1
00:00:00,000 --> 00:00:06,000
 The Dhammatthag, delivered by Venerable Sahaja Ushilananda

2
00:00:06,000 --> 00:00:09,440
 at the Tagata Meditation Center,

3
00:00:09,440 --> 00:00:17,520
 San Jose, California, August 21, 1998.

4
00:00:17,520 --> 00:00:28,760
 Today we come to the very important teaching of the Buddha,

5
00:00:28,760 --> 00:00:33,040
 the doctrine of another.

6
00:00:33,040 --> 00:00:41,110
 The doctrine of another is found only in Buddhism, and so

7
00:00:41,110 --> 00:00:43,800
 this doctrine of another makes Buddhism

8
00:00:43,800 --> 00:00:51,320
 unique among religions because almost all other major

9
00:00:51,320 --> 00:00:56,120
 religions believe in the opposite

10
00:00:56,120 --> 00:01:00,560
 of the doctrine of another.

11
00:01:00,560 --> 00:01:06,930
 And this doctrine of another is a central doctrine in the

12
00:01:06,930 --> 00:01:10,000
 teachings of the Buddha, and

13
00:01:10,000 --> 00:01:17,930
 it is important that we understand another first in theory,

14
00:01:17,930 --> 00:01:21,560
 and then we must understand

15
00:01:21,560 --> 00:01:29,360
 it through practice of Vibhasana Meditation.

16
00:01:29,360 --> 00:01:37,570
 Buddha taught this doctrine of another to his five

17
00:01:37,570 --> 00:01:43,320
 disciples, that was for the first

18
00:01:43,320 --> 00:01:44,320
 time.

19
00:01:44,320 --> 00:01:50,210
 Now, two months after his enlightenment, Buddha taught

20
00:01:50,210 --> 00:01:58,240
 these five disciples his first sermon,

21
00:01:58,240 --> 00:02:05,220
 the summon of the wheel of the Dhamma, and five days later

22
00:02:05,220 --> 00:02:08,080
 he taught to them the another

23
00:02:08,080 --> 00:02:15,520
 lakana-sudha, the characteristic of another.

24
00:02:15,520 --> 00:02:21,790
 And all these five disciples became arrogant after

25
00:02:21,790 --> 00:02:27,720
 listening to this teaching of another.

26
00:02:27,720 --> 00:02:37,500
 In this another lakana-sudha, Buddha declared that the five

27
00:02:37,500 --> 00:02:41,960
 aggregates are another.

28
00:02:41,960 --> 00:02:51,850
 Now, actually he took the five aggregates one by one and

29
00:02:51,850 --> 00:02:58,280
 then said that Rupa or corporeality

30
00:02:58,280 --> 00:03:02,460
 is another, Vedana or feeling is another, Sanya or

31
00:03:02,460 --> 00:03:05,400
 perception is another, Sankara or

32
00:03:05,400 --> 00:03:10,250
 mental formations are another, and Vedana or consciousness

33
00:03:10,250 --> 00:03:11,440
 is another.

34
00:03:11,440 --> 00:03:21,140
 So he taught that all these five aggregates are another,

35
00:03:21,140 --> 00:03:26,280
 and another means not atta.

36
00:03:26,280 --> 00:03:35,230
 The word atta is a compound word, it is compounded as na

37
00:03:35,230 --> 00:03:37,240
 and atta.

38
00:03:37,240 --> 00:03:44,040
 So na means not and atta means atta.

39
00:03:44,040 --> 00:03:58,440
 Now the word atta in Pali has at least two meanings.

40
00:03:58,440 --> 00:04:04,980
 One is like when we use oneself to refer to oneself or

41
00:04:04,980 --> 00:04:08,360
 yourself or himself, we use the

42
00:04:08,360 --> 00:04:09,600
 word atta.

43
00:04:09,600 --> 00:04:14,160
 So that is a reflexive pronoun.

44
00:04:14,160 --> 00:04:22,720
 And the other meaning of atta is a permanent entity

45
00:04:22,720 --> 00:04:29,520
 conceived by most people in the ancient

46
00:04:29,520 --> 00:04:38,200
 days to be the core of all beings and so on.

47
00:04:38,200 --> 00:04:44,310
 So the word atta has at least two meanings, one is just

48
00:04:44,310 --> 00:04:48,320
 self or with a small s, so oneself,

49
00:04:48,320 --> 00:04:56,690
 himself and so on, and the other is self or soul or

50
00:04:56,690 --> 00:05:02,920
 permanent entity believed to be a

51
00:05:02,920 --> 00:05:08,420
 truth by many people and during or before the time of the

52
00:05:08,420 --> 00:05:11,080
 Buddha and during the time

53
00:05:11,080 --> 00:05:16,320
 or even after the time of the Buddha.

54
00:05:16,320 --> 00:05:21,820
 This what atta is compounded with the word na and so first

55
00:05:21,820 --> 00:05:24,240
 there is na atta and that

56
00:05:24,240 --> 00:05:31,900
 na atta is changed into annata following the Pali gram

57
00:05:31,900 --> 00:05:34,520
matical rules.

58
00:05:34,520 --> 00:05:40,600
 So annata means not atta.

59
00:05:40,600 --> 00:05:47,080
 The Pali word annata or by the doctrine of annata Buddha

60
00:05:47,080 --> 00:05:50,600
 negated the atta and what was

61
00:05:50,600 --> 00:05:52,600
 the atta that Buddha negated?

62
00:05:52,600 --> 00:06:02,490
 Now as I said before, in those days people believe in a

63
00:06:02,490 --> 00:06:07,720
 soul or a permanent entity and

64
00:06:07,720 --> 00:06:12,770
 they think that there is something that is permanent and

65
00:06:12,770 --> 00:06:15,720
 that permanent something lives

66
00:06:15,720 --> 00:06:17,760
 in the bodies.

67
00:06:17,760 --> 00:06:23,400
 And then when the physical body becomes old and decrepit

68
00:06:23,400 --> 00:06:26,560
 and dies, it will take another

69
00:06:26,560 --> 00:06:28,880
 body and so on.

70
00:06:28,880 --> 00:06:35,430
 And also they believe that that permanent entity is the

71
00:06:35,430 --> 00:06:38,840
 real one that acts or that does

72
00:06:38,840 --> 00:06:47,840
 actions and that entity is the real one that experiences

73
00:06:47,840 --> 00:06:52,320
 happiness or suffering.

74
00:06:52,320 --> 00:06:59,920
 And also that permanent entity is the Lord.

75
00:06:59,920 --> 00:07:06,710
 It has the power to administer the activities of the body

76
00:07:06,710 --> 00:07:08,240
 and so on.

77
00:07:08,240 --> 00:07:14,560
 And that is what those people believed during that time.

78
00:07:14,560 --> 00:07:23,050
 And so Buddha came and then declared that the five aggreg

79
00:07:23,050 --> 00:07:26,200
ates are not atta.

80
00:07:26,200 --> 00:07:34,760
 So five aggregates are not atta, they conceived to be a

81
00:07:34,760 --> 00:07:38,200
 permanent entity.

82
00:07:38,200 --> 00:07:48,010
 In the another lakana, Buddha said corporeality is not atta

83
00:07:48,010 --> 00:07:50,040
 and so on.

84
00:07:50,040 --> 00:07:56,990
 And Buddha analyzed the whole world into the five aggreg

85
00:07:56,990 --> 00:07:58,120
ates.

86
00:07:58,120 --> 00:08:04,500
 So there is nothing in addition to five aggregates that we

87
00:08:04,500 --> 00:08:06,640
 can call a world.

88
00:08:06,640 --> 00:08:11,760
 So when Buddha said these five aggregates are not atta, it

89
00:08:11,760 --> 00:08:14,360
 amounts to saying that there

90
00:08:14,360 --> 00:08:17,680
 is no atta at all in the world.

91
00:08:17,680 --> 00:08:26,890
 So although Buddha did not say there is no atta, when he

92
00:08:26,890 --> 00:08:31,720
 said these five aggregates are

93
00:08:31,720 --> 00:08:43,400
 not atta, it amounts to saying that there is no atta.

94
00:08:43,400 --> 00:08:50,810
 Now what it is, that is an atta, that is described as an

95
00:08:50,810 --> 00:08:52,040
 atta.

96
00:08:52,040 --> 00:08:58,550
 Now I just told you that Buddha declared the five aggreg

97
00:08:58,550 --> 00:09:01,200
ates to be not atta.

98
00:09:01,200 --> 00:09:07,450
 Now there are three statements which are well known among

99
00:09:07,450 --> 00:09:11,080
 Buddhists and these statements

100
00:09:11,080 --> 00:09:20,170
 are all conditions phenomena are impermanent, all

101
00:09:20,170 --> 00:09:27,280
 conditions phenomena are suffering and

102
00:09:27,280 --> 00:09:35,640
 all things are another.

103
00:09:35,640 --> 00:09:40,320
 So in the first two statements Buddha said all conditions

104
00:09:40,320 --> 00:09:42,880
 phenomena are impermanent and

105
00:09:42,880 --> 00:09:47,020
 suffering but in the third Buddha did not use the word

106
00:09:47,020 --> 00:09:49,360
 conditioned phenomena but he

107
00:09:49,360 --> 00:09:52,200
 used instead the word dharma.

108
00:09:52,200 --> 00:10:02,350
 So according to that statement all dharma are not atta and

109
00:10:02,350 --> 00:10:06,680
 all dharma mean the five

110
00:10:06,680 --> 00:10:11,820
 aggregates and nirvana.

111
00:10:11,820 --> 00:10:19,240
 So we must understand that not only are the five aggregates

112
00:10:19,240 --> 00:10:23,680
 another but also nirvana is.

113
00:10:23,680 --> 00:10:29,390
 So sometimes we might think that since nirvana is the

114
00:10:29,390 --> 00:10:33,320
 object of the world, nirvana must be

115
00:10:33,320 --> 00:10:34,320
 atta.

116
00:10:34,320 --> 00:10:40,740
 But here Buddha said all dhammas are not atta and all dham

117
00:10:40,740 --> 00:10:43,600
mas include nirvana.

118
00:10:43,600 --> 00:10:48,510
 So when we come to another we must understand that all five

119
00:10:48,510 --> 00:10:51,080
 aggregates and also nirvana

120
00:10:51,080 --> 00:10:58,120
 are not atta.

121
00:10:58,120 --> 00:11:04,430
 Now we must understand the third statement that all dhammas

122
00:11:04,430 --> 00:11:07,160
 are another correctly in

123
00:11:07,160 --> 00:11:09,720
 different contexts.

124
00:11:09,720 --> 00:11:15,660
 When it is put out as a general statement we must

125
00:11:15,660 --> 00:11:20,320
 understand all dhammas to me, five

126
00:11:20,320 --> 00:11:22,800
 aggregates and nirvana.

127
00:11:22,800 --> 00:11:28,280
 But when it is in the context of vipassana when all dhammas

128
00:11:28,280 --> 00:11:30,800
 mean the five aggregates

129
00:11:30,800 --> 00:11:38,480
 only and not nirvana because nirvana cannot be taken as an

130
00:11:38,480 --> 00:11:51,760
 object for vipassana meditation.

131
00:11:51,760 --> 00:12:04,010
 In the another lakana soda, Buddha did not say nirvana was

132
00:12:04,010 --> 00:12:06,560
 another.

133
00:12:06,560 --> 00:12:14,840
 Now why did Buddha not mention nirvana as another in that s

134
00:12:14,840 --> 00:12:15,960
oda?

135
00:12:15,960 --> 00:12:22,560
 I think it is because Buddha wanted to direct the five

136
00:12:22,560 --> 00:12:27,600
 disciples to the practice of vipassana

137
00:12:27,600 --> 00:12:29,880
 meditation.

138
00:12:29,880 --> 00:12:35,430
 So for those who practice vipassana meditation only the

139
00:12:35,430 --> 00:12:38,760
 five aggregates can be the object

140
00:12:38,760 --> 00:12:41,640
 of meditation.

141
00:12:41,640 --> 00:12:48,960
 So that is probably because of this that Buddha did not

142
00:12:48,960 --> 00:12:53,880
 mention nirvana as another in the

143
00:12:53,880 --> 00:13:07,920
 another lakana soda.

144
00:13:07,920 --> 00:13:15,710
 Now the meaning of the word anatta, so we know that anatta

145
00:13:15,710 --> 00:13:18,120
 means not ata.

146
00:13:18,120 --> 00:13:27,170
 In the commentaries it is explained as meaning not being

147
00:13:27,170 --> 00:13:32,680
 the core of anything or not having

148
00:13:32,680 --> 00:13:43,040
 a core or not being susceptible to the exercise of power.

149
00:13:43,040 --> 00:13:46,730
 So at least we can say there are two meanings or

150
00:13:46,730 --> 00:13:50,200
 interpretations to understand here.

151
00:13:50,200 --> 00:13:57,020
 Not ata and not ata means no core or having no core and

152
00:13:57,020 --> 00:14:01,560
 also it means we cannot have power

153
00:14:01,560 --> 00:14:12,560
 over this.

154
00:14:12,560 --> 00:14:20,300
 So when Buddha said that rupa or corporeality is anatta, he

155
00:14:20,300 --> 00:14:24,600
 meant that rupa or corporeality

156
00:14:24,600 --> 00:14:33,080
 is not a core or not substantial and also he meant that

157
00:14:33,080 --> 00:14:37,800
 corporeality is the one over

158
00:14:37,800 --> 00:14:42,760
 which we cannot have our power.

159
00:14:42,760 --> 00:14:51,750
 Although both interpretations, that is being no core or

160
00:14:51,750 --> 00:14:56,680
 having no core is accepted by our

161
00:14:56,680 --> 00:15:05,190
 teachers, I think being no core is more appropriate in my

162
00:15:05,190 --> 00:15:07,040
 opinion.

163
00:15:07,040 --> 00:15:15,540
 If we say rupa or corporeality is anatta because it has no

164
00:15:15,540 --> 00:15:20,760
 core, then we can argue that okay,

165
00:15:20,760 --> 00:15:29,440
 rupa has no core but there must be a core somewhere.

166
00:15:29,440 --> 00:15:40,660
 Actually rupa does not possess that core so that kind of

167
00:15:40,660 --> 00:15:45,600
 argument can come and according

168
00:15:45,600 --> 00:15:54,010
 to the teachings of the Buddha that argument is not correct

169
00:15:54,010 --> 00:15:54,600
.

170
00:15:54,600 --> 00:15:59,890
 Now what is it that we call a core here, a substance or

171
00:15:59,890 --> 00:16:01,160
 essence?

172
00:16:01,160 --> 00:16:08,290
 So in the commoner is it is stated that something that is

173
00:16:08,290 --> 00:16:13,800
 wrongly conceived as a self, a permanent

174
00:16:13,800 --> 00:16:15,760
 entity.

175
00:16:15,760 --> 00:16:22,500
 That is what we call core here and also as I said before,

176
00:16:22,500 --> 00:16:25,960
 some permanent entity that

177
00:16:25,960 --> 00:16:34,820
 abides in the bodies of beings and that permanent entity

178
00:16:34,820 --> 00:16:40,120
 lives forever and when the present

179
00:16:40,120 --> 00:16:46,780
 body becomes old and it dies then it transfers itself to

180
00:16:46,780 --> 00:16:54,440
 another new body and so on and also

181
00:16:54,440 --> 00:17:01,390
 the permanent, I mean a core means a doer, that is someone

182
00:17:01,390 --> 00:17:04,880
 who performs the actions.

183
00:17:04,880 --> 00:17:10,110
 Now although we think that we are doing the actions

184
00:17:10,110 --> 00:17:13,960
 according to that opinion, it is the

185
00:17:13,960 --> 00:17:17,880
 ata that is doing the actions and not us.

186
00:17:17,880 --> 00:17:25,400
 So ata is the one that performs the actions and we are just

187
00:17:25,400 --> 00:17:30,160
 the instruments and also whenever

188
00:17:30,160 --> 00:17:38,550
 we experience happiness or suffering it is not us who

189
00:17:38,550 --> 00:17:44,360
 experiences but the ata that experiences

190
00:17:44,360 --> 00:17:53,870
 and we are just the instrument in the ata experiencing

191
00:17:53,870 --> 00:17:58,840
 happiness or suffering.

192
00:17:58,840 --> 00:18:04,080
 And also that ata is his own master.

193
00:18:04,080 --> 00:18:11,500
 So one who is his own master is the most important part of

194
00:18:11,500 --> 00:18:16,640
 the theme and so it is called a core.

195
00:18:16,640 --> 00:18:23,280
 Now ruba is not a core means ruba is not any one of these.

196
00:18:23,280 --> 00:18:29,220
 Ruba is not a self or a soul, ruba is not the abider or not

197
00:18:29,220 --> 00:18:31,880
 the one that lives in the

198
00:18:31,880 --> 00:18:39,080
 body for all the time and ruba is not a doer, ruba is not

199
00:18:39,080 --> 00:18:43,240
 an experiencer and ruba is not

200
00:18:43,240 --> 00:18:49,400
 one who which is its own master.

201
00:18:49,400 --> 00:18:54,680
 And we understand in that way we are said to understand the

202
00:18:54,680 --> 00:18:57,360
 another nature of things.

203
00:18:57,360 --> 00:19:05,250
 Now how do we know that ruba or corporeality for example is

204
00:19:05,250 --> 00:19:09,480
 not self, not an abider, not

205
00:19:09,480 --> 00:19:17,600
 a doer, not an experiencer and not the one who is its own

206
00:19:17,600 --> 00:19:19,320
 master.

207
00:19:19,320 --> 00:19:25,870
 Now the explanation given in the commentaries is first we

208
00:19:25,870 --> 00:19:29,800
 understand that corporeality is

209
00:19:29,800 --> 00:19:34,680
 impermanent and so it is suffering.

210
00:19:34,680 --> 00:19:41,080
 So these two let us say we have understood.

211
00:19:41,080 --> 00:19:52,080
 Now if corporeality is impermanent and it is suffering then

212
00:19:52,080 --> 00:19:57,400
 it must have no core or no

213
00:19:57,400 --> 00:20:04,360
 substance in it because it cannot prevent itself from being

214
00:20:04,360 --> 00:20:07,320
 impermanent and from being

215
00:20:07,320 --> 00:20:08,560
 suffering.

216
00:20:08,560 --> 00:20:15,160
 So this is the argument taught in the commentaries.

217
00:20:15,160 --> 00:20:21,840
 What is impermanent and suffering cannot even prevent its

218
00:20:21,840 --> 00:20:26,800
 own impermanence and being suffering.

219
00:20:26,800 --> 00:20:33,700
 So how could it have the state of a doer or an experiencer

220
00:20:33,700 --> 00:20:35,280
 and so on?

221
00:20:35,280 --> 00:20:40,650
 That means something that is impermanent, it cannot change

222
00:20:40,650 --> 00:20:43,720
 itself into a permanent thing,

223
00:20:43,720 --> 00:20:45,120
 something that is suffering.

224
00:20:45,120 --> 00:20:48,400
 It cannot change itself into happiness.

225
00:20:48,400 --> 00:20:56,650
 So if it cannot prevent itself from becoming impermanent

226
00:20:56,650 --> 00:21:01,040
 and suffering, how can it be a

227
00:21:01,040 --> 00:21:05,120
 master or a doer or an experiencer?

228
00:21:05,120 --> 00:21:12,480
 So what is impermanent and what is suffering is actually

229
00:21:12,480 --> 00:21:16,160
 not ata or anata in the sense

230
00:21:16,160 --> 00:21:26,880
 of having no core or in the sense of being insubstantial.

231
00:21:26,880 --> 00:21:33,750
 Now this takes us to another interpretation of the word an

232
00:21:33,750 --> 00:21:37,720
ata that is being insusceptible

233
00:21:37,720 --> 00:21:43,520
 to exercise of power.

234
00:21:43,520 --> 00:21:50,120
 It simply means it does not follow our wishes.

235
00:21:50,120 --> 00:21:56,780
 Now all these five aggregates are anata because of the word

236
00:21:56,780 --> 00:21:59,280
 said by the Buddha.

237
00:21:59,280 --> 00:22:03,480
 What is dukkha is anata.

238
00:22:03,480 --> 00:22:10,160
 Now this is a criterion Buddha gives us to decide whether

239
00:22:10,160 --> 00:22:13,580
 something is anata or not.

240
00:22:13,580 --> 00:22:18,240
 So if we want to know whether something is anata or not, we

241
00:22:18,240 --> 00:22:20,560
 must first know whether it

242
00:22:20,560 --> 00:22:25,200
 is dukkha or not.

243
00:22:25,200 --> 00:22:29,840
 And what is a criterion for dukkha?

244
00:22:29,840 --> 00:22:39,380
 Now Buddha gave us another criterion and that is what is

245
00:22:39,380 --> 00:22:44,040
 impermanent is dukkha.

246
00:22:44,040 --> 00:22:46,840
 So we have two formulas here.

247
00:22:46,840 --> 00:22:54,650
 What is impermanent is suffering and what is suffering is

248
00:22:54,650 --> 00:22:57,520
 not so or another.

249
00:22:57,520 --> 00:23:08,430
 And what is the criterion for being anetia or being imper

250
00:23:08,430 --> 00:23:10,600
manent?

251
00:23:10,600 --> 00:23:20,520
 To put it plainly disappearing after arising.

252
00:23:20,520 --> 00:23:25,460
 So disappearing after arising is a mark of being imper

253
00:23:25,460 --> 00:23:26,480
manent.

254
00:23:26,480 --> 00:23:31,760
 So whatever arises and disappears is impermanent.

255
00:23:31,760 --> 00:23:36,580
 And whatever is impermanent is dukkha or suffering.

256
00:23:36,580 --> 00:23:41,360
 And whatever is dukkha is another.

257
00:23:41,360 --> 00:23:53,850
 Now we know that rupa or corporeality or feeling or

258
00:23:53,850 --> 00:23:54,560
 perception and so on, so they arise and

259
00:23:54,560 --> 00:24:00,360
 disappear or they disappear after arising.

260
00:24:00,360 --> 00:24:04,410
 So when we see for ourselves that these arise and disappear

261
00:24:04,410 --> 00:24:07,200
, we know that they are impermanent.

262
00:24:07,200 --> 00:24:10,560
 When we know that they are impermanent, we also know that

263
00:24:10,560 --> 00:24:12,960
 they are dukkha, they are suffering.

264
00:24:12,960 --> 00:24:19,360
 And dukkha here means, suffering here means not physically

265
00:24:19,360 --> 00:24:22,320
 painful but here suffering

266
00:24:22,320 --> 00:24:30,120
 means being oppressed by rise and fall.

267
00:24:30,120 --> 00:24:35,640
 So the meaning of dukkha is being suppressed by rise and

268
00:24:35,640 --> 00:24:36,560
 fall.

269
00:24:36,560 --> 00:24:41,250
 That means if anything has arising and disappearing, if

270
00:24:41,250 --> 00:24:44,440
 anything arises and disappears, it is

271
00:24:44,440 --> 00:24:50,390
 called dukkha, suffering, because it is bombarded by or

272
00:24:50,390 --> 00:24:54,640
 afflicted by rising and disappearing.

273
00:24:54,640 --> 00:25:00,260
 Now if you see something as impermanent, you know that you

274
00:25:00,260 --> 00:25:03,360
 cannot turn it into a permanent

275
00:25:03,360 --> 00:25:04,360
 thing.

276
00:25:04,360 --> 00:25:09,600
 And if you see something as dukkha, you cannot turn it into

277
00:25:09,600 --> 00:25:10,600
 sukha.

278
00:25:10,600 --> 00:25:16,240
 So you cannot exercise any power over them and so they are

279
00:25:16,240 --> 00:25:19,160
 not self or they are another.

280
00:25:19,160 --> 00:25:24,560
 So in the sense that they do not follow your wish, that

281
00:25:24,560 --> 00:25:27,720
 means they arise and disappear

282
00:25:27,720 --> 00:25:34,480
 according to their wish and not according to your wish.

283
00:25:34,480 --> 00:25:42,600
 So mind and matter arise and disappear depending on the

284
00:25:42,600 --> 00:25:44,960
 conditions.

285
00:25:44,960 --> 00:25:48,320
 And whether we don't want them to arise or not, when there

286
00:25:48,320 --> 00:25:49,960
 are conditions, they will

287
00:25:49,960 --> 00:25:53,000
 arise and they will disappear.

288
00:25:53,000 --> 00:26:00,480
 So we have no control over their arising and disappearing.

289
00:26:00,480 --> 00:26:07,960
 That is why having no control over them, having no excess

290
00:26:07,960 --> 00:26:12,440
 of power over them is said to be

291
00:26:12,440 --> 00:26:19,320
 a mark of being another.

292
00:26:19,320 --> 00:26:27,060
 So if we want to know whether something is another or not,

293
00:26:27,060 --> 00:26:30,440
 we have to find this dukkha

294
00:26:30,440 --> 00:26:39,060
 mark or this sign and that is not being susceptible to the

295
00:26:39,060 --> 00:26:42,440
 exercise of power.

296
00:26:42,440 --> 00:26:49,070
 So in the commandaries, the characteristic or the mark of

297
00:26:49,070 --> 00:26:52,800
 anatta is given as insusceptibility

298
00:26:52,800 --> 00:27:00,640
 to the exercise of power.

299
00:27:00,640 --> 00:27:06,250
 I think we should also understand that having no core is

300
00:27:06,250 --> 00:27:09,920
 also a characteristic of another.

301
00:27:09,920 --> 00:27:14,740
 So when we want to know whether something is another, we

302
00:27:14,740 --> 00:27:17,240
 may look for one of these two

303
00:27:17,240 --> 00:27:23,760
 marks whether it has a core or it is substantial and also

304
00:27:23,760 --> 00:27:27,680
 whether it acts according to our

305
00:27:27,680 --> 00:27:36,360
 wish or it is out of control by us.

306
00:27:36,360 --> 00:27:44,300
 Most people are unable to see the another nature of things

307
00:27:44,300 --> 00:27:48,360
 because this another nature

308
00:27:48,360 --> 00:27:57,560
 is concealed by compactness.

309
00:27:57,560 --> 00:28:01,950
 We take things to be compact, we take things to be

310
00:28:01,950 --> 00:28:05,280
 substantial and so we fail to see the

311
00:28:05,280 --> 00:28:08,400
 another nature of things.

312
00:28:08,400 --> 00:28:14,500
 We are concealed by compactness means concealed by the

313
00:28:14,500 --> 00:28:17,480
 notion of compactness.

314
00:28:17,480 --> 00:28:25,250
 Now we think that we are a compact thing, the matter is

315
00:28:25,250 --> 00:28:30,040
 compact and mind is also compact

316
00:28:30,040 --> 00:28:40,710
 and so long as we think we are compact, we cannot see the

317
00:28:40,710 --> 00:28:47,080
 insubstantiality of ourselves.

318
00:28:47,080 --> 00:28:54,620
 And compactness or the notion of compactness conceives the

319
00:28:54,620 --> 00:28:59,480
 another nature because we do

320
00:28:59,480 --> 00:29:10,140
 not give attention to resolution into elements, digging

321
00:29:10,140 --> 00:29:15,440
 down different elements.

322
00:29:15,440 --> 00:29:21,720
 So long as we see mind and matter as one compact thing, we

323
00:29:21,720 --> 00:29:26,680
 will not see it's another nature.

324
00:29:26,680 --> 00:29:34,520
 But when we break down the body and mind into elements,

325
00:29:34,520 --> 00:29:40,200
 that is elements that compose them,

326
00:29:40,200 --> 00:29:47,080
 then we will come to see the insubstantiality of things.

327
00:29:47,080 --> 00:29:54,690
 Now most people do not even see mind and matter as two

328
00:29:54,690 --> 00:29:57,800
 separate things.

329
00:29:57,800 --> 00:30:02,040
 Sometimes we think we are just one, our mind and body are

330
00:30:02,040 --> 00:30:03,040
 just one.

331
00:30:03,040 --> 00:30:08,500
 So long as we do not see the separateness of mind and

332
00:30:08,500 --> 00:30:12,080
 matter or in different elements

333
00:30:12,080 --> 00:30:17,670
 that constitute mind and matter, so long as we cannot see

334
00:30:17,670 --> 00:30:20,480
 them, we will not be able to

335
00:30:20,480 --> 00:30:24,360
 break this notion of compactness.

336
00:30:24,360 --> 00:30:28,460
 If we cannot break the notion of compactness, we will not

337
00:30:28,460 --> 00:30:31,120
 see the another nature of things.

338
00:30:31,120 --> 00:30:39,280
 But when we pay attention to what's happening to us or what

339
00:30:39,280 --> 00:30:42,960
's happening in us or when we

340
00:30:42,960 --> 00:30:51,910
 pay attention to the object at the present moment, we will

341
00:30:51,910 --> 00:30:55,680
 not fail to see that what

342
00:30:55,680 --> 00:31:03,630
 we thought to be one compact thing is actually composed of

343
00:31:03,630 --> 00:31:06,360
 small elements.

344
00:31:06,360 --> 00:31:12,720
 What we thought to be one compact thing is actually

345
00:31:12,720 --> 00:31:16,760
 composed of small elements.

346
00:31:16,760 --> 00:31:24,970
 Now if we do not practice meditation, we think that this

347
00:31:24,970 --> 00:31:31,040
 one mind sees, hears, smells, tastes,

348
00:31:31,040 --> 00:31:34,640
 touches and thinks.

349
00:31:34,640 --> 00:31:41,230
 But when you watch your mind, you know that seeing is one,

350
00:31:41,230 --> 00:31:44,400
 you see with one mind and you

351
00:31:44,400 --> 00:31:46,920
 hear with one mind and so on.

352
00:31:46,920 --> 00:31:52,320
 So they are separate things, they are separate elements.

353
00:31:52,320 --> 00:31:58,760
 And also the body, you know, the four elements of earth,

354
00:31:58,760 --> 00:32:01,480
 water, fire and air.

355
00:32:01,480 --> 00:32:09,280
 And so when you are able to see these elements separately,

356
00:32:09,280 --> 00:32:14,120
 you break the notion of compactness

357
00:32:14,120 --> 00:32:16,280
 and compactness.

358
00:32:16,280 --> 00:32:20,700
 So when you can break the notion of compactness, you know

359
00:32:20,700 --> 00:32:23,880
 that what you thought to be substantial

360
00:32:23,880 --> 00:32:27,480
 is really insubstantial.

361
00:32:27,480 --> 00:32:35,160
 And so you see that it is another.

362
00:32:35,160 --> 00:32:42,980
 And when you see the elements separately, one distinct from

363
00:32:42,980 --> 00:32:46,160
 the other and they arise

364
00:32:46,160 --> 00:32:51,380
 and disappear depending on the conditions, you come to the

365
00:32:51,380 --> 00:32:53,760
 realization that there is

366
00:32:53,760 --> 00:32:57,760
 no exercise of power over them.

367
00:32:57,760 --> 00:33:03,920
 For example, seeing mind or seeing consciousness.

368
00:33:03,920 --> 00:33:07,430
 So when there is something to be seen and when it is

369
00:33:07,430 --> 00:33:09,760
 brought into the avenue of your

370
00:33:09,760 --> 00:33:13,800
 eyes, there is the seeing consciousness.

371
00:33:13,800 --> 00:33:18,190
 So you cannot prevent this seeing consciousness from

372
00:33:18,190 --> 00:33:21,880
 arising when there are these conditions.

373
00:33:21,880 --> 00:33:27,220
 However much you don't want this seeing consciousness not

374
00:33:27,220 --> 00:33:30,000
 to arise, it will arise because there

375
00:33:30,000 --> 00:33:32,880
 are conditions for it to arise.

376
00:33:32,880 --> 00:33:37,730
 So there is no exercise of power over the arising of that

377
00:33:37,730 --> 00:33:40,160
 seeing consciousness.

378
00:33:40,160 --> 00:33:44,520
 So it arises depending on the conditions and so long as

379
00:33:44,520 --> 00:33:48,840
 there are conditions, it will arise.

380
00:33:48,840 --> 00:33:55,470
 So you have no power over them and having no power over

381
00:33:55,470 --> 00:33:58,880
 them is one aspect of being

382
00:33:58,880 --> 00:34:03,080
 another.

383
00:34:03,080 --> 00:34:09,400
 So we can really see the impermanent nature of things.

384
00:34:09,400 --> 00:34:16,890
 That is, they are insubstantial, they have no inner core

385
00:34:16,890 --> 00:34:21,320
 and also they are not susceptible

386
00:34:21,320 --> 00:34:27,160
 to exercise of power only when we practice Vipassana

387
00:34:27,160 --> 00:34:31,800
 meditation, only when we pay attention

388
00:34:31,800 --> 00:34:34,280
 to the objects.

389
00:34:34,280 --> 00:34:38,790
 Without the practice of Vipassana meditation, we cannot see

390
00:34:38,790 --> 00:34:41,160
 the another nature clearly.

391
00:34:41,160 --> 00:34:45,980
 We may read books and we may hear or listen to talks and we

392
00:34:45,980 --> 00:34:48,920
 think we understand the another

393
00:34:48,920 --> 00:34:49,920
 nature.

394
00:34:49,920 --> 00:34:55,800
 But that understanding is not our understanding, it is just

395
00:34:55,800 --> 00:34:58,760
 a borrowed understanding.

396
00:34:58,760 --> 00:35:02,760
 Only when we practice Vipassana meditation, only when we

397
00:35:02,760 --> 00:35:05,200
 pay close attention to the objects

398
00:35:05,200 --> 00:35:11,220
 at the present moment can we see the another nature of

399
00:35:11,220 --> 00:35:15,200
 things along with the impermanent

400
00:35:15,200 --> 00:35:18,960
 nature and suffering nature.

401
00:35:18,960 --> 00:35:26,860
 So the nature of another can be understood only through the

402
00:35:26,860 --> 00:35:34,920
 practice of Vipassana meditation.

403
00:35:34,920 --> 00:35:41,550
 Since when we practice Vipassana meditation, we do not see

404
00:35:41,550 --> 00:35:45,200
 anything that is substantial,

405
00:35:45,200 --> 00:35:52,040
 anything that we can exercise power over.

406
00:35:52,040 --> 00:35:59,190
 We come to realization as and that there is just the

407
00:35:59,190 --> 00:36:04,400
 suffering and no person who suffers

408
00:36:04,400 --> 00:36:11,330
 because what we call a person is just a combination of the

409
00:36:11,330 --> 00:36:13,800
 mind and matter.

410
00:36:13,800 --> 00:36:25,720
 And there is no person over and above the mind and matter.

411
00:36:25,720 --> 00:36:30,660
 So through practice of Vipassana meditation, we come to see

412
00:36:30,660 --> 00:36:33,480
 that there is just suffering,

413
00:36:33,480 --> 00:36:38,430
 there is just arising and disappearing and nothing that

414
00:36:38,430 --> 00:36:40,800
 arises and disappears.

415
00:36:40,800 --> 00:36:45,820
 And also when we look at the activities, we see that there

416
00:36:45,820 --> 00:36:48,800
 is just the activity and actually

417
00:36:48,800 --> 00:36:56,160
 no person, say who performs these activities.

418
00:36:56,160 --> 00:37:04,870
 And also it is said that the suggestion of suffering or nir

419
00:37:04,870 --> 00:37:09,320
vana is but there is no person

420
00:37:09,320 --> 00:37:16,400
 who experiences nirvana because there is just mind and

421
00:37:16,400 --> 00:37:19,560
 matter and no person.

422
00:37:19,560 --> 00:37:24,380
 And also it is said that there is the path but nobody who

423
00:37:24,380 --> 00:37:26,380
 walks on that path.

424
00:37:26,380 --> 00:37:33,360
 That means the path means of the eight components of the

425
00:37:33,360 --> 00:37:38,840
 path and apart from these eight components,

426
00:37:38,840 --> 00:37:42,840
 we do not see anything to call a path.

427
00:37:42,840 --> 00:37:50,660
 And so there is just these eight qualities, I mean eight

428
00:37:50,660 --> 00:37:54,760
 mental states and no person,

429
00:37:54,760 --> 00:38:02,370
 nobody who practices these eight mental states because in

430
00:38:02,370 --> 00:38:07,320
 the ultimate analysis, there are

431
00:38:07,320 --> 00:38:13,560
 just mind and body, I mean Nama and Rupa or mind and matter

432
00:38:13,560 --> 00:38:16,520
 and in addition to mind and

433
00:38:16,520 --> 00:38:24,530
 matter, there is nothing we can call a person or an

434
00:38:24,530 --> 00:38:27,080
 individual.

435
00:38:27,080 --> 00:38:34,430
 Seeing another nature of mind and matter or the five aggreg

436
00:38:34,430 --> 00:38:37,160
ates is not the end of the

437
00:38:37,160 --> 00:38:42,190
 practice of Vipassana, actually it is the beginning of Vip

438
00:38:42,190 --> 00:38:43,160
assana.

439
00:38:43,160 --> 00:38:50,580
 So people have to make more effort to go through the higher

440
00:38:50,580 --> 00:38:56,000
 stages of Vipassana practice until

441
00:38:56,000 --> 00:39:02,440
 they reach the stage of realizing the Four Noble Truths.

442
00:39:02,440 --> 00:39:09,800
 But the realization of Four Noble Truths cannot come about

443
00:39:09,800 --> 00:39:14,120
 without the understanding of the

444
00:39:14,120 --> 00:39:19,770
 another nature of mind and matter, actually not only

445
00:39:19,770 --> 00:39:24,120
 another nature but also the impermanent

446
00:39:24,120 --> 00:39:29,950
 nature and suffering nature of mind and matter or the five

447
00:39:29,950 --> 00:39:31,600
 aggregates.

448
00:39:31,600 --> 00:39:37,330
 So it is very important that when we practice Vipassana

449
00:39:37,330 --> 00:39:41,000
 meditation, we see for ourselves

450
00:39:41,000 --> 00:39:45,380
 through our own experience the impermanent nature, the

451
00:39:45,380 --> 00:39:48,000
 suffering nature and the another

452
00:39:48,000 --> 00:39:57,450
 nature of mind and matter so that we can go on this path

453
00:39:57,450 --> 00:40:02,680
 going through the higher stages

454
00:40:02,680 --> 00:40:10,240
 of Vipassana meditation until we reach our goal.

455
00:40:10,240 --> 00:40:20,590
 The three to recapitulate Buddha taught that five aggreg

456
00:40:20,590 --> 00:40:24,360
ates are not atta and since there

457
00:40:24,360 --> 00:40:31,360
 are only five aggregates in the world, it amounts to saying

458
00:40:31,360 --> 00:40:34,240
 that there is no atta.

459
00:40:34,240 --> 00:40:41,010
 And the characteristic of atta is one of the three general

460
00:40:41,010 --> 00:40:45,520
 characteristics of all conditioned

461
00:40:45,520 --> 00:40:51,170
 phenomena and it is important that yogis see the

462
00:40:51,170 --> 00:40:55,680
 characteristic of atta along with the

463
00:40:55,680 --> 00:41:00,480
 characteristic of impermanent and suffering.

464
00:41:00,480 --> 00:41:05,330
 And this seeing the three characteristics of all

465
00:41:05,330 --> 00:41:09,320
 conditioned phenomena will eventually

466
00:41:09,320 --> 00:41:24,720
 lead yogis to the realization of the truth.

467
00:41:24,720 --> 00:41:25,720
 Thank you.

468
00:41:25,720 --> 00:41:25,720
 Thank you.

469
00:41:25,720 --> 00:41:26,720
 Thank you.

470
00:41:26,720 --> 00:41:26,720
 Thank you.

471
00:41:26,720 --> 00:41:27,720
 Thank you.

