1
00:00:00,000 --> 00:00:13,020
 So I thought I'd start off my new YouTube with a video on

2
00:00:13,020 --> 00:00:14,240
 how to meditate.

3
00:00:14,240 --> 00:00:20,520
 That's going to be a how to do it yourself at home follow

4
00:00:20,520 --> 00:00:23,200
 along sort of video.

5
00:00:23,200 --> 00:00:26,280
 The first thing is to explain a little bit about meditation

6
00:00:26,280 --> 00:00:26,360
.

7
00:00:26,360 --> 00:00:29,080
 What does it mean to meditate?

8
00:00:29,080 --> 00:00:35,120
 Well the word meditation is an English word.

9
00:00:35,120 --> 00:00:40,400
 It comes from Latin or it comes from Greek.

10
00:00:40,400 --> 00:00:47,040
 It comes from the same root as the word medication.

11
00:00:47,040 --> 00:00:52,000
 But medication is something which cures the sickness, it

12
00:00:52,000 --> 00:00:54,640
 remedies the sicknesses in the

13
00:00:54,640 --> 00:00:56,880
 body.

14
00:00:56,880 --> 00:01:00,130
 Meditation is something which remedies the sicknesses in

15
00:01:00,130 --> 00:01:00,840
 the mind.

16
00:01:00,840 --> 00:01:05,740
 And so someone might ask well I don't have mental illness,

17
00:01:05,740 --> 00:01:07,600
 I'm not sick in my mind.

18
00:01:07,600 --> 00:01:10,720
 Why should I meditate?

19
00:01:10,720 --> 00:01:15,500
 What we consider mental sickness means any mental upset or

20
00:01:15,500 --> 00:01:16,560
 mental stress.

21
00:01:16,560 --> 00:01:21,920
 So people who are all stressed out if you have a friend who

22
00:01:21,920 --> 00:01:24,640
's got stress or depression

23
00:01:24,640 --> 00:01:32,550
 or sadness or short attention span, ADD, ADHD, and so on,

24
00:01:32,550 --> 00:01:36,200
 hyperactive disorders.

25
00:01:36,200 --> 00:01:38,240
 This is the reason why we meditate.

26
00:01:38,240 --> 00:01:41,680
 We practice to do away with any sort of mental upset even

27
00:01:41,680 --> 00:01:43,680
 if we're an ordinary person but

28
00:01:43,680 --> 00:01:48,310
 we find our lives are not as fulfilling as we wish them to

29
00:01:48,310 --> 00:01:49,520
 be or so on.

30
00:01:49,520 --> 00:01:54,190
 We have many reasons, in fact it's meditation to make the

31
00:01:54,190 --> 00:01:56,880
 mind strong and firm and to make

32
00:01:56,880 --> 00:01:58,920
 the strong and peaceful and happy.

33
00:01:58,920 --> 00:02:05,300
 Since the focus of my YouTube is my channel, is for peace

34
00:02:05,300 --> 00:02:08,760
 and for happiness, I thought

35
00:02:08,760 --> 00:02:13,170
 this would be a good start to teach people how to find

36
00:02:13,170 --> 00:02:15,840
 inner peace and inner happiness

37
00:02:15,840 --> 00:02:19,940
 because this then of course unfolds into outer peace and

38
00:02:19,940 --> 00:02:21,800
 outer happiness when we're all

39
00:02:21,800 --> 00:02:24,000
 inner peaceful and happy.

40
00:02:24,000 --> 00:02:29,320
 This is how we bring about world peace and world happiness.

41
00:02:29,320 --> 00:02:31,720
 So let's get right into it.

42
00:02:31,720 --> 00:02:33,400
 Then we'll talk about meditation.

43
00:02:33,400 --> 00:02:36,960
 Meditation means to cure the sicknesses of the mind.

44
00:02:36,960 --> 00:02:40,160
 Now let's learn how to practice meditation.

45
00:02:40,160 --> 00:02:43,150
 So I'm going to start, this is only going to be part one

46
00:02:43,150 --> 00:02:44,800
 and we'll go slowly through

47
00:02:44,800 --> 00:02:48,190
 this and in my next part I'll teach you more advanced

48
00:02:48,190 --> 00:02:50,400
 meditation and so on and so on.

49
00:02:50,400 --> 00:02:56,630
 But for the first part I'll teach just a brief meditation,

50
00:02:56,630 --> 00:02:59,800
 a very simple meditation.

51
00:02:59,800 --> 00:03:02,450
 What we're going to look at in meditation is we're going to

52
00:03:02,450 --> 00:03:03,600
 look at our body and our

53
00:03:03,600 --> 00:03:07,940
 mind because again real meditation has to be able to cure

54
00:03:07,940 --> 00:03:10,040
 the things which are wrong

55
00:03:10,040 --> 00:03:17,560
 with our minds, wrong with our subconscious and our

56
00:03:17,560 --> 00:03:20,360
 conscious mind.

57
00:03:20,360 --> 00:03:23,040
 So we have to use that as our object.

58
00:03:23,040 --> 00:03:28,350
 We can't do any of these anti-transcendental meditations or

59
00:03:28,350 --> 00:03:30,160
 image free or so on and so

60
00:03:30,160 --> 00:03:34,760
 on, creative visualizations and so on.

61
00:03:34,760 --> 00:03:36,160
 We can't do any of that.

62
00:03:36,160 --> 00:03:38,880
 This won't cure us of the problem because it has nothing to

63
00:03:38,880 --> 00:03:40,080
 do with what is the real

64
00:03:40,080 --> 00:03:44,400
 problem which is up here and down here.

65
00:03:44,400 --> 00:03:46,680
 I'm going to start by teaching sitting meditation.

66
00:03:46,680 --> 00:03:50,490
 Most people might think, "Well, all meditation is sitting,

67
00:03:50,490 --> 00:03:52,240
 what's so special here?"

68
00:03:52,240 --> 00:03:53,240
 Well this isn't so.

69
00:03:53,240 --> 00:03:56,020
 Next I'm going to teach walking meditation, but first

70
00:03:56,020 --> 00:03:57,320
 sitting meditation.

71
00:03:57,320 --> 00:03:59,840
 Sitting meditation, so everyone hold up your right hand.

72
00:03:59,840 --> 00:04:01,760
 Don't worry, no vows.

73
00:04:01,760 --> 00:04:06,400
 We're going to use our right hand and place it on your

74
00:04:06,400 --> 00:04:07,320
 stomach.

75
00:04:07,320 --> 00:04:10,460
 You don't have to get right in there if you place it on top

76
00:04:10,460 --> 00:04:12,600
 of your clothes or your robes.

77
00:04:12,600 --> 00:04:14,480
 That's fine.

78
00:04:14,480 --> 00:04:20,580
 Just so that you can feel your stomach and try to feel the

79
00:04:20,580 --> 00:04:23,560
 breath going into the body

80
00:04:23,560 --> 00:04:26,400
 and the breath leaving the body.

81
00:04:26,400 --> 00:04:32,160
 You can use the body as our object.

82
00:04:32,160 --> 00:04:35,240
 When the breath goes into the body, the stomach will rise.

83
00:04:35,240 --> 00:04:39,120
 Just naturally you don't have to force it.

84
00:04:39,120 --> 00:04:45,760
 You don't have to artificially make it long or deep.

85
00:04:45,760 --> 00:04:49,120
 Only just watch the breath as it naturally goes into the

86
00:04:49,120 --> 00:04:49,720
 body.

87
00:04:49,720 --> 00:04:51,640
 Watch the stomach as it rises.

88
00:04:51,640 --> 00:04:55,390
 When the breath reaches the stomach, the stomach will rise

89
00:04:55,390 --> 00:04:56,360
 naturally.

90
00:04:56,360 --> 00:04:59,660
 When the breath goes out of the body, the stomach will fall

91
00:04:59,660 --> 00:05:00,560
 naturally.

92
00:05:00,560 --> 00:05:05,950
 We're going to use a word to keep our mind fixed on the

93
00:05:05,950 --> 00:05:09,320
 stomach because otherwise our

94
00:05:09,320 --> 00:05:12,550
 minds are going to wander and we're not going to see really

95
00:05:12,550 --> 00:05:14,560
 clearly about this body starting

96
00:05:14,560 --> 00:05:15,560
 with the stomach.

97
00:05:15,560 --> 00:05:17,960
 If we can't see clearly about the body, we're not going to

98
00:05:17,960 --> 00:05:19,120
 be able to see clearly about

99
00:05:19,120 --> 00:05:22,120
 the mind because of course the mind, what is the mind that

100
00:05:22,120 --> 00:05:23,280
 lives in the body?

101
00:05:23,280 --> 00:05:26,550
 We're starting with the stomach, we're going to use this as

102
00:05:26,550 --> 00:05:31,280
 a way to see also the mind.

103
00:05:31,280 --> 00:05:34,650
 What we're going to do when the stomach rises naturally, we

104
00:05:34,650 --> 00:05:36,720
're going to say to ourselves,

105
00:05:36,720 --> 00:05:37,720
 rising.

106
00:05:37,720 --> 00:05:55,280
 You can keep your hand there.

107
00:05:55,280 --> 00:05:57,090
 If it gets to the point where you don't need to use your

108
00:05:57,090 --> 00:05:58,160
 hand and you can feel it even

109
00:05:58,160 --> 00:06:00,890
 without having your hand there, you can take your hand away

110
00:06:00,890 --> 00:06:02,080
 and put your hands on your

111
00:06:02,080 --> 00:06:05,160
 lap just like you see the Buddha do.

112
00:06:05,160 --> 00:06:09,020
 However you like, you can put your hands, you can do this

113
00:06:09,020 --> 00:06:10,920
 in a chair, you can do this

114
00:06:10,920 --> 00:06:15,900
 lying down, lying meditation, rising, falling, however you

115
00:06:15,900 --> 00:06:17,320
 choose to do it.

116
00:06:17,320 --> 00:06:21,790
 We'll do this all together now for a few minutes and then

117
00:06:21,790 --> 00:06:24,400
 we'll fit for today with a brief

118
00:06:24,400 --> 00:06:28,810
 introduction to Vipassana meditation, meditation which

119
00:06:28,810 --> 00:06:31,080
 helps us to see clearly which will be

120
00:06:31,080 --> 00:06:36,250
 a cause for us to endure mental upset or mental illnesses

121
00:06:36,250 --> 00:06:39,320
 which we all carry around with us,

122
00:06:39,320 --> 00:06:40,320
 depression and sadness.

123
00:06:40,320 --> 00:06:43,950
 We're going to work them out slowly but we'll start here

124
00:06:43,950 --> 00:06:46,200
 with a very simple meditation based

125
00:06:46,200 --> 00:06:47,200
 on the stomach.

126
00:06:47,200 --> 00:06:50,080
 We just say in our minds, rising, falling.

127
00:06:50,080 --> 00:06:54,890
 You don't have to say it out loud, please don't say it out

128
00:06:54,890 --> 00:06:55,680
 loud.

129
00:06:55,680 --> 00:07:02,680
 In the mind, the mind of the rising and the falling.

130
00:07:02,680 --> 00:07:29,680
 We'll do that now for a few minutes.

131
00:07:29,680 --> 00:07:58,680
 (Pause)

132
00:07:58,680 --> 00:08:17,960
 Now let's start at Vipassana meditation.

133
00:08:17,960 --> 00:08:21,650
 You may be wondering exactly what this has to do with the

134
00:08:21,650 --> 00:08:23,680
 mind and hearing the mind.

135
00:08:23,680 --> 00:08:29,860
 I just like to say in this episode that before we can start

136
00:08:29,860 --> 00:08:33,240
 to change or cure the problems

137
00:08:33,240 --> 00:08:35,880
 we have to understand the disease.

138
00:08:35,880 --> 00:08:39,750
 Just like in medicine, first you have to take science, you

139
00:08:39,750 --> 00:08:42,120
 have to understand what our organs

140
00:08:42,120 --> 00:08:47,400
 and what our cells and all of these things and how they

141
00:08:47,400 --> 00:08:50,080
 systems work together.

142
00:08:50,080 --> 00:08:53,520
 In the same way, the first thing before we're going to work

143
00:08:53,520 --> 00:08:55,320
 on the mind, first we have to

144
00:08:55,320 --> 00:08:56,320
 understand the mind.

145
00:08:56,320 --> 00:08:59,290
 So in the beginning we're going to look at the body, the

146
00:08:59,290 --> 00:09:00,880
 rising and the falling and the

147
00:09:00,880 --> 00:09:04,020
 mind which says rising and says falling and knows the

148
00:09:04,020 --> 00:09:05,680
 rising and the falling.

149
00:09:05,680 --> 00:09:09,030
 Even in just a few minutes, probably you can already see

150
00:09:09,030 --> 00:09:10,960
 something about your own mind.

151
00:09:10,960 --> 00:09:14,530
 Maybe for many people the mind is wandering and the mind is

152
00:09:14,530 --> 00:09:16,240
 not with the rising and the

153
00:09:16,240 --> 00:09:19,870
 falling, maybe the mind is getting bored or it's getting

154
00:09:19,870 --> 00:09:21,680
 upset or maybe it feels very

155
00:09:21,680 --> 00:09:22,680
 happy and peaceful.

156
00:09:22,680 --> 00:09:26,780
 You can start to see how the mind works and how the body

157
00:09:26,780 --> 00:09:29,120
 works and then we can start to

158
00:09:29,120 --> 00:09:34,070
 work on the problems and see what are the real problems and

159
00:09:34,070 --> 00:09:36,240
 why it is that we as human

160
00:09:36,240 --> 00:09:42,240
 beings still have pain and suffering and sadness and so on.

161
00:09:42,240 --> 00:09:46,030
 Why these things still come up from time to time and we can

162
00:09:46,030 --> 00:09:47,920
 learn to overcome and do away

163
00:09:47,920 --> 00:09:51,520
 with these things and be happy and peaceful all the time.

164
00:09:51,520 --> 00:09:52,760
 That's all for now.

165
00:09:52,760 --> 00:09:54,040
 Thanks for tuning in.

166
00:09:54,040 --> 00:10:10,120
 [

