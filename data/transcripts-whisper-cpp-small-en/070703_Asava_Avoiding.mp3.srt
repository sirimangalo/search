1
00:00:00,000 --> 00:00:15,480
 Today I'll continue the series of talks on the Asavas, the

2
00:00:15,480 --> 00:00:16,720
 developments which cause the

3
00:00:16,720 --> 00:00:26,880
 mind to ferment, to become sour, to go bad.

4
00:00:26,880 --> 00:00:38,160
 Today's topic is on the Pariwadjana Baha Tappa Asavas, the

5
00:00:38,160 --> 00:00:41,400
 Asavati, the Asavas which, or

6
00:00:41,400 --> 00:00:50,150
 the method of removing the Asavas which consists of

7
00:00:50,150 --> 00:00:53,560
 avoiding, doing away with the Asavas through

8
00:00:53,560 --> 00:00:58,640
 the way of getting rid of them, removing them simply by

9
00:00:58,640 --> 00:00:59,680
 avoiding.

10
00:00:59,680 --> 00:01:05,480
 Now this is a, it's important to be clear about this one.

11
00:01:05,480 --> 00:01:13,960
 It's a subject which is very easy to misunderstand.

12
00:01:13,960 --> 00:01:19,000
 And indeed Buddhism is often very much misunderstood as

13
00:01:19,000 --> 00:01:23,080
 being a religion which consists of avoiding

14
00:01:23,080 --> 00:01:33,680
 things like responsibility or involvement or which involves

15
00:01:33,680 --> 00:01:38,600
 running away from reality,

16
00:01:38,600 --> 00:01:48,680
 running away from suffering, running away from problems.

17
00:01:48,680 --> 00:01:53,040
 And so it's important to clear this up first of all.

18
00:01:53,040 --> 00:01:57,130
 I think it's fair to say that Buddhism does to some extent

19
00:01:57,130 --> 00:01:59,760
 advocate running away or avoiding

20
00:01:59,760 --> 00:02:10,630
 or keeping out of what is generally called modern society

21
00:02:10,630 --> 00:02:14,760
 or public society.

22
00:02:14,760 --> 00:02:21,480
 But I think this is a fair statement because I think modern

23
00:02:21,480 --> 00:02:24,920
 society or public society is

24
00:02:24,920 --> 00:02:28,940
 something which is very much misunderstood and there's a

25
00:02:28,940 --> 00:02:31,280
 great bias in our understanding

26
00:02:31,280 --> 00:02:35,080
 of society, of what we call society.

27
00:02:35,080 --> 00:02:43,210
 And we think of society as the right and the proper way to

28
00:02:43,210 --> 00:02:47,680
 organize groups of people.

29
00:02:47,680 --> 00:02:52,880
 And we base this on Aristotle or on Socrates, on Plato and

30
00:02:52,880 --> 00:02:53,720
 so on.

31
00:02:53,720 --> 00:02:58,700
 On these ancient philosophers who we assume were infallible

32
00:02:58,700 --> 00:03:00,960
 and who we assume are being

33
00:03:00,960 --> 00:03:09,520
 followed to the letter.

34
00:03:09,520 --> 00:03:16,100
 And yet we see that the aims and the goals and the fruits

35
00:03:16,100 --> 00:03:19,840
 which come from these systems

36
00:03:19,840 --> 00:03:26,900
 which we call society tend very much on the side of greed

37
00:03:26,900 --> 00:03:30,680
 and on the side of lust, tend

38
00:03:30,680 --> 00:03:41,000
 very much on the side of consumerism in the sense of

39
00:03:41,000 --> 00:03:44,240
 reaching out for, seeking out for

40
00:03:44,240 --> 00:03:49,960
 things which are impermanent, things which are unsatisfying

41
00:03:49,960 --> 00:03:52,240
 and things which are not

42
00:03:52,240 --> 00:03:53,400
 under our control.

43
00:03:53,400 --> 00:03:59,830
 Looking out for things which are subject to dissolution,

44
00:03:59,830 --> 00:04:02,520
 which are ephemeral.

45
00:04:02,520 --> 00:04:07,990
 And so, what we find is that when we reach out and want

46
00:04:07,990 --> 00:04:10,960
 more and more and more, there

47
00:04:10,960 --> 00:04:16,750
 are other beings who also reach out and want more and more

48
00:04:16,750 --> 00:04:18,040
 and more.

49
00:04:18,040 --> 00:04:22,220
 And eventually there's not enough for anyone because our

50
00:04:22,220 --> 00:04:24,480
 wants are far greater than our

51
00:04:24,480 --> 00:04:28,240
 actual needs.

52
00:04:28,240 --> 00:04:31,840
 And so we create what we call society.

53
00:04:31,840 --> 00:04:36,190
 This is of course very much simplified, but it's clear in

54
00:04:36,190 --> 00:04:38,080
 my mind that there's a great

55
00:04:38,080 --> 00:04:39,840
 element of truth in this.

56
00:04:39,840 --> 00:04:44,110
 And it's based on the Lord Buddha's idea of explanation of

57
00:04:44,110 --> 00:04:48,000
 how society arose and so on.

58
00:04:48,000 --> 00:04:54,260
 So we're not satisfied simply by collecting wild roots and

59
00:04:54,260 --> 00:04:57,840
 fruits and wild edibles, which

60
00:04:57,840 --> 00:05:00,090
 at that time would have been plentiful at the time when

61
00:05:00,090 --> 00:05:01,400
 people lived in the forest and

62
00:05:01,400 --> 00:05:04,760
 among the trees.

63
00:05:04,760 --> 00:05:08,340
 But nowadays we see that, we see simply from the words

64
00:05:08,340 --> 00:05:10,600
 where people say going off to live

65
00:05:10,600 --> 00:05:13,440
 in the forest is a kind of running away.

66
00:05:13,440 --> 00:05:18,730
 We see how far we've come from the nature which is our home

67
00:05:18,730 --> 00:05:21,400
, which is our true home.

68
00:05:21,400 --> 00:05:26,800
 Now we think of trees as something like an ornament that

69
00:05:26,800 --> 00:05:30,040
 you put on your front lawn.

70
00:05:30,040 --> 00:05:34,050
 We think of peace and quiet as something you find when you

71
00:05:34,050 --> 00:05:36,240
 close your door or when you

72
00:05:36,240 --> 00:05:48,560
 lie down to sleep in your cement room like a jail cell.

73
00:05:48,560 --> 00:05:59,600
 And this is because of the unsatisfied, the lack of content

74
00:05:59,600 --> 00:06:02,360
ment, being discontent with

75
00:06:02,360 --> 00:06:03,360
 what we have.

76
00:06:03,360 --> 00:06:06,360
 We have little, we're not able to live in.

77
00:06:06,360 --> 00:06:10,120
 Of course Socrates himself was very clear on this fact.

78
00:06:10,120 --> 00:06:13,260
 When he sat down and started to create his idea of society

79
00:06:13,260 --> 00:06:14,860
 was that we would eat things

80
00:06:14,860 --> 00:06:19,520
 like acorns and drink spring water and so on.

81
00:06:19,520 --> 00:06:26,810
 We'd eat foods which were raw and which were bland and so

82
00:06:26,810 --> 00:06:27,680
 on.

83
00:06:27,680 --> 00:06:33,750
 Simple foods which could be easily found from foraging and

84
00:06:33,750 --> 00:06:34,680
 so on.

85
00:06:34,680 --> 00:06:38,730
 But we can see that the lack of contentment, and Socrates

86
00:06:38,730 --> 00:06:41,080
 pointed this out and it was clear

87
00:06:41,080 --> 00:06:44,720
 that no one was going to go for such a society.

88
00:06:44,720 --> 00:06:47,680
 And so then we have to start collecting food.

89
00:06:47,680 --> 00:06:50,160
 And when we collect food and keep food for the next day and

90
00:06:50,160 --> 00:06:51,320
 the next day and the next

91
00:06:51,320 --> 00:06:58,370
 day, when we keep it more and more and more, well we have

92
00:06:58,370 --> 00:07:01,320
 to extend our range.

93
00:07:01,320 --> 00:07:06,280
 And when our range interferes with other people's range, we

94
00:07:06,280 --> 00:07:08,360
 have to set up fences.

95
00:07:08,360 --> 00:07:13,000
 We have to set up territories.

96
00:07:13,000 --> 00:07:18,300
 When setting up territories then we find that our territory

97
00:07:18,300 --> 00:07:21,640
 is not big enough to satisfy

98
00:07:21,640 --> 00:07:27,130
 our desires, our desires become manifold and so we have to

99
00:07:27,130 --> 00:07:29,000
 alter the territory.

100
00:07:29,000 --> 00:07:33,250
 We have to cut down the trees and start specializing in a

101
00:07:33,250 --> 00:07:35,200
 certain type of food.

102
00:07:35,200 --> 00:07:40,790
 And we specialize in something and maybe begin to barter or

103
00:07:40,790 --> 00:07:43,640
 to trade and so on and so on.

104
00:07:43,640 --> 00:07:48,910
 And eventually we have what is now this big mess which we

105
00:07:48,910 --> 00:07:50,600
 call society.

106
00:07:50,600 --> 00:07:53,320
 And it has to do with a great population explosion.

107
00:07:53,320 --> 00:07:56,890
 Of course population explosion, we all know where that

108
00:07:56,890 --> 00:07:57,880
 comes from.

109
00:07:57,880 --> 00:08:02,520
 It comes from something which is considered nowadays to be

110
00:08:02,520 --> 00:08:04,540
 natural and this is the sexual

111
00:08:04,540 --> 00:08:08,810
 intercourse, something which probably wouldn't have been a

112
00:08:08,810 --> 00:08:11,000
 big problem if we could keep our

113
00:08:11,000 --> 00:08:14,520
 minds and our hearts and our eyes and our bodies under

114
00:08:14,520 --> 00:08:15,400
 control.

115
00:08:15,400 --> 00:08:20,580
 But we can see how out of control it becomes and how our

116
00:08:20,580 --> 00:08:23,320
 population explodes.

117
00:08:23,320 --> 00:08:27,460
 And because of course of our keen intellect and our ability

118
00:08:27,460 --> 00:08:29,320
 to get what we want most of

119
00:08:29,320 --> 00:08:34,280
 the time we've come to take over most parts of this earth

120
00:08:34,280 --> 00:08:36,760
 and radically alter the face

121
00:08:36,760 --> 00:08:40,840
 of it.

122
00:08:40,840 --> 00:08:45,730
 We can see how someone who is able to control their bodies

123
00:08:45,730 --> 00:08:48,860
 and not the body and not interested

124
00:08:48,860 --> 00:08:53,810
 in running after objects of the sense is able to live in

125
00:08:53,810 --> 00:08:56,800
 great peace and in a very ancient

126
00:08:56,800 --> 00:09:00,000
 way.

127
00:09:00,000 --> 00:09:04,370
 And so I think in this sense some of the criticisms are

128
00:09:04,370 --> 00:09:07,600
 very much biased and of course rightly

129
00:09:07,600 --> 00:09:11,490
 so they come from people who are very much engrossed,

130
00:09:11,490 --> 00:09:14,080
 involved, very much enchanted by

131
00:09:14,080 --> 00:09:18,550
 society and get involved in political debates and so on and

132
00:09:18,550 --> 00:09:20,680
 create all sorts of systems

133
00:09:20,680 --> 00:09:24,720
 and organizations.

134
00:09:24,720 --> 00:09:29,420
 What we find there are still people in this world who

135
00:09:29,420 --> 00:09:31,840
 refuse to be involved, refuse to

136
00:09:31,840 --> 00:09:37,520
 indulge in the foolish running after, chasing after things

137
00:09:37,520 --> 00:09:40,160
 which are not truly going to

138
00:09:40,160 --> 00:09:43,520
 satisfy and we can see this is when it comes down to this

139
00:09:43,520 --> 00:09:45,400
 is the main goal of society is

140
00:09:45,400 --> 00:09:50,240
 to get all the time what we want as much as possible.

141
00:09:50,240 --> 00:09:54,380
 And of course we try not to interfere with other people

142
00:09:54,380 --> 00:09:55,840
 while doing so.

143
00:09:55,840 --> 00:09:59,560
 But of course the main problem is the wanting and wanting

144
00:09:59,560 --> 00:10:01,680
 is something which easily gets

145
00:10:01,680 --> 00:10:02,680
 out of control.

146
00:10:02,680 --> 00:10:06,080
 When we are free from wanting this or wanting that then we

147
00:10:06,080 --> 00:10:07,880
 don't have to fight or argue

148
00:10:07,880 --> 00:10:12,980
 or interfere with other people's business.

149
00:10:12,980 --> 00:10:16,760
 And so we are able to live in a very natural way.

150
00:10:16,760 --> 00:10:19,340
 So the argument is that living in the forest is certainly

151
00:10:19,340 --> 00:10:20,840
 not running away, it's coming

152
00:10:20,840 --> 00:10:27,840
 back to a natural, a very normal state of existence.

153
00:10:27,840 --> 00:10:33,030
 And another important point which I think is less of a

154
00:10:33,030 --> 00:10:36,200
 criticism but more of a problem

155
00:10:36,200 --> 00:10:42,910
 for meditators, for people who take up the Buddhist path

156
00:10:42,910 --> 00:10:46,040
 and this is running away from

157
00:10:46,040 --> 00:10:47,040
 suffering.

158
00:10:47,040 --> 00:10:51,430
 And I have heard it as a criticism that or as a reason why

159
00:10:51,430 --> 00:10:53,600
 many people have taken up

160
00:10:53,600 --> 00:10:57,690
 the Buddhist path, some people from outside have said it's

161
00:10:57,690 --> 00:10:59,880
 because they've had suffering

162
00:10:59,880 --> 00:11:02,960
 thrown at them for so long that they just want to find a

163
00:11:02,960 --> 00:11:04,600
 way out of it, they want to

164
00:11:04,600 --> 00:11:06,440
 run away from it.

165
00:11:06,440 --> 00:11:09,480
 They don't want to face it anymore.

166
00:11:09,480 --> 00:11:13,400
 And so they take up Buddhism which advocates, so they say

167
00:11:13,400 --> 00:11:15,720
 running away from suffering.

168
00:11:15,720 --> 00:11:19,120
 And Buddhism of course does no such thing.

169
00:11:19,120 --> 00:11:21,670
 Buddhism is for the realization of suffering, it's the

170
00:11:21,670 --> 00:11:23,160
 first noble truth, it's for the

171
00:11:23,160 --> 00:11:27,080
 understanding of suffering and why we suffer.

172
00:11:27,080 --> 00:11:30,230
 And we don't ever have to run away from suffering, what we

173
00:11:30,230 --> 00:11:32,040
 have to do away with is the cause

174
00:11:32,040 --> 00:11:34,760
 of suffering.

175
00:11:34,760 --> 00:11:37,640
 And we have to understand what really is suffering.

176
00:11:37,640 --> 00:11:41,320
 And so we understand that suffering is not just simply a

177
00:11:41,320 --> 00:11:44,000
 painful feeling, it is the attachment

178
00:11:44,000 --> 00:11:47,600
 to the painful feeling which comes from craving, which

179
00:11:47,600 --> 00:11:50,120
 comes from clinging, wanting it to be

180
00:11:50,120 --> 00:11:52,080
 this way, wanting it to be that way, of course when it's

181
00:11:52,080 --> 00:11:53,340
 not the way we want, this is when

182
00:11:53,340 --> 00:11:56,940
 suffering arises.

183
00:11:56,940 --> 00:11:59,450
 But for a person who practices the Buddhist path, as we can

184
00:11:59,450 --> 00:12:03,160
 all see when we practice meditation,

185
00:12:03,160 --> 00:12:05,640
 we actually have to face suffering in a way that an

186
00:12:05,640 --> 00:12:07,640
 ordinary person never would, never

187
00:12:07,640 --> 00:12:10,240
 would dare to, never would be able to.

188
00:12:10,240 --> 00:12:15,740
 When we sit down and close our eyes and the pain comes up,

189
00:12:15,740 --> 00:12:18,640
 and rather than running away

190
00:12:18,640 --> 00:12:24,480
 or shifting our position, we come closer to it, we come to

191
00:12:24,480 --> 00:12:27,240
 see clearer about it, we come

192
00:12:27,240 --> 00:12:29,240
 to focus on it.

193
00:12:29,240 --> 00:12:32,770
 And by focusing and keeping our mind clear, saying to our

194
00:12:32,770 --> 00:12:35,160
self pain, pain as a simple reminder

195
00:12:35,160 --> 00:12:39,150
 that this is only pain, creating a clear thought which

196
00:12:39,150 --> 00:12:42,000
 replaces the thoughts of disliking,

197
00:12:42,000 --> 00:12:46,840
 the thoughts of upset, to the point where the painful

198
00:12:46,840 --> 00:12:49,880
 feeling is seen clearly for what

199
00:12:49,880 --> 00:12:53,920
 it is, comes to register in our minds and in our memories

200
00:12:53,920 --> 00:12:56,200
 as something which is neutral.

201
00:12:56,200 --> 00:13:00,430
 And because of our lack of clinging and our lack of craving

202
00:13:00,430 --> 00:13:02,760
 in regards to the pain, there

203
00:13:02,760 --> 00:13:06,470
 arises no suffering, next time or the next time or the next

204
00:13:06,470 --> 00:13:07,080
 time.

205
00:13:07,080 --> 00:13:10,520
 We find ourselves free from the suffering which would arise

206
00:13:10,520 --> 00:13:11,640
 from the pain or from the

207
00:13:11,640 --> 00:13:13,020
 uncomfortable situation.

208
00:13:13,020 --> 00:13:17,350
 So it's completely due to lack of understanding, due to

209
00:13:17,350 --> 00:13:20,440
 lack of thorough investigation of the

210
00:13:20,440 --> 00:13:23,930
 Buddhist teaching that people would say that Buddhists run

211
00:13:23,930 --> 00:13:25,360
 away from suffering.

212
00:13:25,360 --> 00:13:28,190
 It's got some element of truth to it, but when using this

213
00:13:28,190 --> 00:13:29,700
 as a criticism, they think

214
00:13:29,700 --> 00:13:33,110
 of suffering as things like pain or things like

215
00:13:33,110 --> 00:13:36,520
 uncomfortable situations, which of course

216
00:13:36,520 --> 00:13:39,880
 are very much a part of the Buddhist path.

217
00:13:39,880 --> 00:13:46,780
 And people would continue this by saying, by closing our

218
00:13:46,780 --> 00:13:49,960
 eyes, by going into our room,

219
00:13:49,960 --> 00:13:52,840
 by taking a retreat.

220
00:13:52,840 --> 00:14:00,160
 When you say, you close your eyes, you can't see anything.

221
00:14:00,160 --> 00:14:03,820
 You run away, it's easy to practice when you're running

222
00:14:03,820 --> 00:14:06,720
 away in your room and not confronting

223
00:14:06,720 --> 00:14:08,400
 anyone.

224
00:14:08,400 --> 00:14:10,680
 So how is it that you are confronting uncomfortable

225
00:14:10,680 --> 00:14:12,840
 situations if you're always running away and

226
00:14:12,840 --> 00:14:16,160
 doing what they call retreats?

227
00:14:16,160 --> 00:14:19,440
 And there's an element of truth to this.

228
00:14:19,440 --> 00:14:23,310
 It's true that when you close your eyes you don't see

229
00:14:23,310 --> 00:14:24,560
 everything.

230
00:14:24,560 --> 00:14:30,070
 But it's also true that sometimes when you see too much you

231
00:14:30,070 --> 00:14:31,280
 go blind.

232
00:14:31,280 --> 00:14:35,200
 When you see too much you can't see anything.

233
00:14:35,200 --> 00:14:38,060
 And we know this very well from looking at very powerful

234
00:14:38,060 --> 00:14:38,920
 light sources.

235
00:14:38,920 --> 00:14:41,240
 When we look at the sun we can go blind.

236
00:14:41,240 --> 00:14:44,720
 When someone shines a bright light in your face you go

237
00:14:44,720 --> 00:14:45,400
 blind.

238
00:14:45,400 --> 00:14:50,430
 When I used to go out of my house in the winter into the

239
00:14:50,430 --> 00:14:54,360
 bright, into the snow, six feet deep

240
00:14:54,360 --> 00:15:00,250
 snow, and to jump out the second floor window, you can go

241
00:15:00,250 --> 00:15:02,200
 blind briefly.

242
00:15:02,200 --> 00:15:04,910
 You have to close your eyes and slowly, slowly open your

243
00:15:04,910 --> 00:15:06,680
 eyes because of the blinding light

244
00:15:06,680 --> 00:15:11,760
 reflecting from the ice crystals.

245
00:15:11,760 --> 00:15:16,250
 And so in the same way, not exactly the same, this isn't a

246
00:15:16,250 --> 00:15:18,880
 physical light which we're talking

247
00:15:18,880 --> 00:15:23,880
 about now, but when we open our eyes we see too much.

248
00:15:23,880 --> 00:15:27,440
 Sure, if we could be truly mindful with our eyes open there

249
00:15:27,440 --> 00:15:29,040
'd be no reason to close our

250
00:15:29,040 --> 00:15:30,040
 eyes.

251
00:15:30,040 --> 00:15:34,420
 But when we close our eyes an ordinary person who has in

252
00:15:34,420 --> 00:15:36,920
 practice is able to focus their

253
00:15:36,920 --> 00:15:39,680
 mind and fix their mind on one object.

254
00:15:39,680 --> 00:15:43,070
 If our concentration is not strong and our eyes are open we

255
00:15:43,070 --> 00:15:44,840
'll easily flip from one thing

256
00:15:44,840 --> 00:15:51,280
 to another and not be able to see clearly about anything.

257
00:15:51,280 --> 00:15:59,960
 People have accused Buddhists of being autists.

258
00:15:59,960 --> 00:16:02,280
 Closing our eyes, how can we see the truth?

259
00:16:02,280 --> 00:16:06,930
 Well the problem is we're like infants, we're like children

260
00:16:06,930 --> 00:16:06,960
.

261
00:16:06,960 --> 00:16:11,320
 We close our eyes and yes we go off into our room at first.

262
00:16:11,320 --> 00:16:14,800
 We do this as a practice, as a means of training.

263
00:16:14,800 --> 00:16:17,800
 We say as a training like training wheels.

264
00:16:17,800 --> 00:16:21,560
 You can't blame the child for using training wheels.

265
00:16:21,560 --> 00:16:26,760
 He knows that if he didn't use them he would fall off the

266
00:16:26,760 --> 00:16:27,640
 bike.

267
00:16:27,640 --> 00:16:31,600
 So what we do here is simply a training for our lives.

268
00:16:31,600 --> 00:16:37,230
 It's not running away, it's easing off or making it easier

269
00:16:37,230 --> 00:16:39,840
 for us in the beginning.

270
00:16:39,840 --> 00:16:41,950
 When we practice and practice when we're truly strong

271
00:16:41,950 --> 00:16:43,560
 enough to deal with the things around

272
00:16:43,560 --> 00:16:46,480
 us then we can open our eyes and we can walk around.

273
00:16:46,480 --> 00:16:53,350
 And of course meditators do do this and eventually do leave

274
00:16:53,350 --> 00:16:55,240
 the centers.

275
00:16:55,240 --> 00:16:57,370
 But usually in the end they end up coming back because they

276
00:16:57,370 --> 00:16:59,480
 realize that out there there's

277
00:16:59,480 --> 00:17:02,940
 just a whole bunch of people excited and intoxicated by

278
00:17:02,940 --> 00:17:05,440
 things which are only a source for more

279
00:17:05,440 --> 00:17:06,440
 and more suffering.

280
00:17:06,440 --> 00:17:09,190
 They see that there's, society has come to take, what we

281
00:17:09,190 --> 00:17:10,620
 call society has come to take

282
00:17:10,620 --> 00:17:12,120
 over most of the world.

283
00:17:12,120 --> 00:17:16,980
 And there's very little of the world which lives in a

284
00:17:16,980 --> 00:17:20,600
 wholesome, peaceful, in a natural

285
00:17:20,600 --> 00:17:24,200
 way in this day and age.

286
00:17:24,200 --> 00:17:28,450
 We try to find places to create our own society, a society

287
00:17:28,450 --> 00:17:32,000
 which is based on contentment, which

288
00:17:32,000 --> 00:17:38,720
 is based on renunciation, giving up, letting go.

289
00:17:38,720 --> 00:17:43,270
 So this all is sort of a precursor to the actual teaching

290
00:17:43,270 --> 00:17:44,080
 here.

291
00:17:44,080 --> 00:17:47,070
 The things which the Lord Buddha said and it's very

292
00:17:47,070 --> 00:17:48,720
 interesting to consider.

293
00:17:48,720 --> 00:17:53,380
 The things which the Lord Buddha said are necessary to

294
00:17:53,380 --> 00:17:54,280
 avoid.

295
00:17:54,280 --> 00:18:02,720
 And the first one is things like elephants or wild, or

296
00:18:02,720 --> 00:18:06,920
 snakes or wild animals.

297
00:18:06,920 --> 00:18:13,760
 Any sort of thorn bush or something or like a deep pit or a

298
00:18:13,760 --> 00:18:16,520
 deep river or so on.

299
00:18:16,520 --> 00:18:20,280
 Like when you're walking and you come across a bramble bush

300
00:18:20,280 --> 00:18:22,200
, well you go around it or you

301
00:18:22,200 --> 00:18:25,410
 walk into the forest and you come to a place with thistles

302
00:18:25,410 --> 00:18:27,120
 or something, you have to go

303
00:18:27,120 --> 00:18:29,600
 around it.

304
00:18:29,600 --> 00:18:32,930
 Or you come across a wild elephant, well you don't just

305
00:18:32,930 --> 00:18:35,160
 keep walking and say, "If I die,

306
00:18:35,160 --> 00:18:36,160
 I die."

307
00:18:36,160 --> 00:18:38,600
 You go around it.

308
00:18:38,600 --> 00:18:42,560
 Why?

309
00:18:42,560 --> 00:18:44,400
 You would lose so much, you would lose a great amount of

310
00:18:44,400 --> 00:18:44,840
 benefit.

311
00:18:44,840 --> 00:18:47,820
 You would be doing something which was, when you had the

312
00:18:47,820 --> 00:18:49,960
 choice, you would be doing something

313
00:18:49,960 --> 00:18:53,360
 which is unwise.

314
00:18:53,360 --> 00:18:56,690
 You would choose to do something to destroy your practice,

315
00:18:56,690 --> 00:18:58,240
 to destroy your ability to

316
00:18:58,240 --> 00:18:59,760
 practice.

317
00:18:59,760 --> 00:19:04,260
 Something which would possibly lead to your great injury or

318
00:19:04,260 --> 00:19:05,040
 death.

319
00:19:05,040 --> 00:19:11,090
 And of course not out of fear of death, this is important,

320
00:19:11,090 --> 00:19:13,920
 but out of an understanding

321
00:19:13,920 --> 00:19:16,400
 of what is right and wrong.

322
00:19:16,400 --> 00:19:20,170
 When you have the choice, you do what is the appropriate

323
00:19:20,170 --> 00:19:20,880
 thing.

324
00:19:20,880 --> 00:19:28,220
 What wise people would appreciate or would laud, would say

325
00:19:28,220 --> 00:19:31,080
 is a good thing to do.

326
00:19:31,080 --> 00:19:33,640
 The first thing is things like wild animals, and this can

327
00:19:33,640 --> 00:19:35,120
 also mean places which have wild

328
00:19:35,120 --> 00:19:36,120
 animals.

329
00:19:36,120 --> 00:19:39,960
 Now of course the forest has certain wild animals and this

330
00:19:39,960 --> 00:19:41,220
 can be a danger.

331
00:19:41,220 --> 00:19:45,360
 You can't go to a place to practice where there are wild

332
00:19:45,360 --> 00:19:48,000
 tigers or lions roaming about.

333
00:19:48,000 --> 00:19:51,800
 It wouldn't be wise, it wouldn't be a conducive place.

334
00:19:51,800 --> 00:19:55,670
 And it would lead possibly to more and more defilement to

335
00:19:55,670 --> 00:19:57,160
 arise in the mind.

336
00:19:57,160 --> 00:20:00,590
 You would be sitting in meditation and always afraid or

337
00:20:00,590 --> 00:20:02,680
 worried or always distracted by

338
00:20:02,680 --> 00:20:06,730
 the prospect of being confronted by a wild animal or by

339
00:20:06,730 --> 00:20:09,360
 something which could cause great

340
00:20:09,360 --> 00:20:11,640
 problems.

341
00:20:11,640 --> 00:20:13,800
 And of course mosquitoes don't fall into this category.

342
00:20:13,800 --> 00:20:17,960
 We can't consider mosquitoes to be wild animals.

343
00:20:17,960 --> 00:20:21,770
 Some things like animals which bite and so on are to be

344
00:20:21,770 --> 00:20:24,200
 considered things which we have

345
00:20:24,200 --> 00:20:29,480
 to be patient, and this I talked about last time.

346
00:20:29,480 --> 00:20:32,050
 And by the same token, talking about mosquitoes, we do have

347
00:20:32,050 --> 00:20:33,480
 to be careful about things like

348
00:20:33,480 --> 00:20:34,480
 malaria.

349
00:20:34,480 --> 00:20:37,310
 We can't simply sit and let ourselves be bitten and bitten

350
00:20:37,310 --> 00:20:38,760
 again when we know there might

351
00:20:38,760 --> 00:20:43,690
 be a potential risk of malaria which would compromise our

352
00:20:43,690 --> 00:20:46,080
 practice and compromise our

353
00:20:46,080 --> 00:20:56,240
 ability to see clearly and to finish the practice.

354
00:20:56,240 --> 00:20:57,760
 This is the first part.

355
00:20:57,760 --> 00:21:04,930
 The second part has to do with places and people which are

356
00:21:04,930 --> 00:21:07,320
 unsuitable.

357
00:21:07,320 --> 00:21:14,440
 For instance, places like bars or places of entertainment,

358
00:21:14,440 --> 00:21:17,960
 of merriment, carnivals and

359
00:21:17,960 --> 00:21:26,130
 festivals, places where the recreation, the activities

360
00:21:26,130 --> 00:21:30,360
 which are occurring are involved

361
00:21:30,360 --> 00:21:37,530
 with unwholesomeness, avoiding brothels, avoiding bars,

362
00:21:37,530 --> 00:21:41,960
 avoiding drug fest or places where opium

363
00:21:41,960 --> 00:21:44,920
 dens and so on.

364
00:21:44,920 --> 00:21:50,800
 Nowadays it means avoiding what we call parties.

365
00:21:50,800 --> 00:21:51,920
 Avoiding these things as a practice.

366
00:21:51,920 --> 00:21:56,600
 No, this is not something which everyone has to do.

367
00:21:56,600 --> 00:22:00,310
 If someone, an ordinary person, enjoys those things and

368
00:22:00,310 --> 00:22:02,600
 likes those things, then they will

369
00:22:02,600 --> 00:22:05,640
 of course go and do those things.

370
00:22:05,640 --> 00:22:06,960
 But this is a very objective thing.

371
00:22:06,960 --> 00:22:10,590
 If a person wishes to be free from mental defilement,

372
00:22:10,590 --> 00:22:13,000
 wishes for their mind to be pure,

373
00:22:13,000 --> 00:22:16,000
 then there is a certain necessity to do away with these

374
00:22:16,000 --> 00:22:16,680
 things.

375
00:22:16,680 --> 00:22:24,060
 It's possible that by going into a pub one can stay without

376
00:22:24,060 --> 00:22:27,520
 creating unwholesomeness,

377
00:22:27,520 --> 00:22:34,120
 without oneself becoming intoxicated or taking intoxicating

378
00:22:34,120 --> 00:22:35,160
 drink.

379
00:22:35,160 --> 00:22:39,140
 But it is equally possible that by going in we might fall

380
00:22:39,140 --> 00:22:41,520
 risk to temptation and we would

381
00:22:41,520 --> 00:22:48,640
 be subject to a bad reputation, subject to reprimand,

382
00:22:48,640 --> 00:22:53,000
 subject to being looked down upon

383
00:22:53,000 --> 00:22:57,320
 by people who know better.

384
00:22:57,320 --> 00:23:02,360
 When people are attending wild parties and going to bars

385
00:23:02,360 --> 00:23:04,800
 and so on, then we start to

386
00:23:04,800 --> 00:23:08,330
 lose their interest in taking us as their friend, those

387
00:23:08,330 --> 00:23:10,200
 people who are interested in

388
00:23:10,200 --> 00:23:14,520
 spirituality, interest in peace and so on.

389
00:23:14,520 --> 00:23:18,410
 They aren't able to stay with us and we aren't able to stay

390
00:23:18,410 --> 00:23:20,840
 with them and we find ourselves

391
00:23:20,840 --> 00:23:28,040
 being surrounded by people who are engaged in unwholesome,

392
00:23:28,040 --> 00:23:31,120
 unprofitable things.

393
00:23:31,120 --> 00:23:33,430
 And this is the other part, is the part about people,

394
00:23:33,430 --> 00:23:35,080
 people which are inappropriate.

395
00:23:35,080 --> 00:23:38,670
 Of course it's inappropriate for someone who wants to be

396
00:23:38,670 --> 00:23:40,680
 free from mental defilement,

397
00:23:40,680 --> 00:23:43,900
 to surround themselves with friends who are full of mental

398
00:23:43,900 --> 00:23:46,680
 defilement and who are encouraged,

399
00:23:46,680 --> 00:23:52,050
 who are developing, who are on a path to develop and to

400
00:23:52,050 --> 00:23:55,440
 increase unwholesome states.

401
00:23:55,440 --> 00:23:59,380
 People who are engaged in unwholesome activities, engaged

402
00:23:59,380 --> 00:24:01,800
 in things which bring about more and

403
00:24:01,800 --> 00:24:09,400
 more craving and greed and anger and suffering and upset.

404
00:24:09,400 --> 00:24:15,030
 Those things which create delusion and create confusion and

405
00:24:15,030 --> 00:24:17,320
 darkness in the mind.

406
00:24:17,320 --> 00:24:18,760
 We have to avoid these people.

407
00:24:18,760 --> 00:24:22,520
 This is the Lord Buddha's teaching on what we should avoid.

408
00:24:22,520 --> 00:24:25,460
 And these things, if we didn't avoid these things, if we

409
00:24:25,460 --> 00:24:27,080
 didn't avoid things which were

410
00:24:27,080 --> 00:24:31,900
 a danger to us or things which were a cause for distraction

411
00:24:31,900 --> 00:24:34,440
, then it would be easy for

412
00:24:34,440 --> 00:24:36,040
 defilements to arise in our mind.

413
00:24:36,040 --> 00:24:39,750
 And if we didn't avoid places and people which were unwh

414
00:24:39,750 --> 00:24:41,920
olesome, places and people which

415
00:24:41,920 --> 00:24:46,460
 were unprofitable, which brought us no benefit, which

416
00:24:46,460 --> 00:24:49,480
 brought our minds down and led us down

417
00:24:49,480 --> 00:24:55,600
 the wrong path and led us to greater and greater delusion

418
00:24:55,600 --> 00:24:59,760
 and distraction and defilement, then

419
00:24:59,760 --> 00:25:03,860
 we would clearly never become free from the things which

420
00:25:03,860 --> 00:25:07,440
 make our minds, which would become

421
00:25:07,440 --> 00:25:11,520
 pickled, our minds would go sour, our minds would become

422
00:25:11,520 --> 00:25:14,360
 defiled, our minds would go bad.

423
00:25:14,360 --> 00:25:17,820
 This is a way to stop our minds from going bad and to do

424
00:25:17,820 --> 00:25:19,800
 away with the defilement in

425
00:25:19,800 --> 00:25:24,280
 our mind, to clean up our minds, to clear up our minds, to

426
00:25:24,280 --> 00:25:26,620
 bring about a very true sense

427
00:25:26,620 --> 00:25:30,160
 of clarity of mind.

428
00:25:30,160 --> 00:25:31,390
 And this is the teaching, the Lord Buddha's teaching on Par

429
00:25:31,390 --> 00:25:37,920
i Vad-tsana Pahattata.

430
00:25:37,920 --> 00:25:38,320
 And that's all for today.

431
00:25:38,320 --> 00:25:39,320
 Thank you.

432
00:25:39,320 --> 00:25:49,320
 [BLANK_AUDIO]

