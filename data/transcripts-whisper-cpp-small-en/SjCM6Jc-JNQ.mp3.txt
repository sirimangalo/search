 Hey, good evening everyone.
 One thing that we're often concerned with in Buddhism in
 the practice and teaching of
 the Buddha's teachings is the depth to which we understand,
 appreciate, and attain the
 Dhamma, the truth, the goal.
 There's a concern that our understanding of the Dhamma
 might be shallow and as a result
 our attainment of the Dhamma will be lacking, inadequate,
 limited, not bad or problematic,
 just insufficient or limited.
 For instance, it's a good thing to know about moral precept
s and of course it's a good thing
 to practice them.
 But Sila goes so much deeper, true ethics go so much deeper
 than just keeping rules.
 With concentration, being focused, being understanding the
 Buddha's teaching about being mindful
 and dedicated, Buddhist, dedicated towards good things.
 This is a very good quality, but of course Samadhi goes
 much deeper than just focusing
 or even being mindful in an ordinary sense.
 And of course wisdom, wisdom is where we have the greatest
 concern, the concern that our
 understanding be more than just intellectual understanding.
 This is important.
 If you read a lot or you study a lot, well you come to know
 a lot about the Buddha's
 teaching.
 When you come to meditate you realize all that knowledge is
 nothing.
 It's good, it's good to know these things.
 It's good in a practical sense even, it can help you in
 your life if you have an understanding
 of all things are impermanent, this too will pass.
 Makes you a better person, a happier person, this is a good
 thing.
 But we recognize there's a greater depth to be had than
 just knowledge of the Buddha's
 teaching.
 A deep, true ethics is activity that is mindful, that
 involves mindfulness.
 When you're walking, stepping right, stepping left, it's
 impossible to be unethical when
 you have the mindful, clear awareness of the activity.
 True focus of course then comes from that sort of behavior,
 the body and mind are in
 sync then you become very focused and it means your mind
 actually becomes free from unwholesome
 qualities that you start to see clearly.
 True focus is not just being concentrated but being in
 focus.
 It's that which allows you to see clearly.
 And so true wisdom is seeing clearly.
 It's not intellectual, it doesn't have to be a thought.
 Wisdom is light, it's like shining a light on reality.
 So that's just an introduction.
 What I wanted to talk tonight about was the Buddha's
 teaching on depths.
 The Buddha said there are six qualities.
 This is in the Chakani part of the Guttarnikaya, Arahantav
aga I think.
 Mahantata sutta, Mahantata.
 Mahantata means greatness.
 Vipula, so the Buddha said, "Chahidamihi bhikkhu ei, Chahid
amihi samanagato bhikkhu."
 Bhikkhu, one who has seen the danger and clinging the
 danger and being reborn again and again
 in samsara, who is accomplished in six dhammas, who
 possesses six things.
 Nachira seva, Mahantatang, in no long time will attain to
 greatness.
 Vipulata, Vipula, Vipula comes from Vipula which means deep
, profound.
 Vipulata means depth or profundity.
 So the idea here is attaining to success and greatness in
 the Dhamma.
 Papunati dhammi, so in regards to the teachings, in regards
 to the Dhamma, in regards to the
 truth, reality.
 It's not about right or wrong, it's about greatness or
 ordinary.
 Ordinary people are not bad necessarily, but there's
 nothing great or wonderful about being
 reborn again and again.
 Even evil people who go to hell and so on.
 It's not so much that it's wrong, it's just that it's ignob
le.
 There's nothing great about indulging in sensuality, there
's nothing great about being rich or
 even famous, even being king of the whole world.
 It's not that great in the end.
 Going to heaven, becoming a Brahma, it's getting greater.
 There's a greatness to it, but it's not really great.
 It's not really deep or profound.
 What's profound is breaking free from the cycle, finding
 something that is lasting,
 stable, secure, safe.
 Because even being born a Brahma is not safe or secure, let
 alone being rich or famous
 or indulging in pleasures.
 So when we come to practice meditation, I think this is of
 great interest to us.
 How do we not just succeed, but how do we gain greater
 depth in our practice?
 I think it's a question for all Buddhist practitioners.
 It might be practicing for many years, but it might be
 shallow.
 How do we ensure that our practice goes deeper?
 So these are qualities, like many of these lists of
 qualities, they're a teaching.
 The idea is as you listen to the teaching, you're
 practicing and you're taking these in,
 internalizing and simulating these teachings, applying them
 to yourself,
 using them not just for theory, but as an encouragement and
 direction
 for greater and more profound practice.
 So there are six of them.
 And the Buddha used interesting words.
 If you don't know Pali, it might not be that interesting,
 but I have to explain because it's a little bit poetic.
 So the first one is Aloka Bahulo.
 Aloka is light.
 And so he didn't explain these.
 We have to understand them.
 What did he mean by light?
 And you have to know something about the Buddha's teaching
 to understand,
 but I already gave it away, that light of wisdom.
 Aloka means you have to have wisdom, but rather than say
 wisdom, he said light.
 It's a certain kind of wisdom. It's not seeing things with
 your eyes, of course,
 but it's the feeling of clarity and opening your eyes and
 leaving the darkness behind.
 It's like waking up from a sleep almost.
 When you open your eyes and suddenly realize that you're
 bumping into things
 and why you're in so much pain is because you were blind.
 Aloka Bahulo, one who has greatness of wisdom.
 So in meditation it's important that your focus, your
 attention is on understanding.
 It's not magic where you just repeat words to yourself and
 you become enlightened.
 You want to really try to see and understand.
 Of course, it's not like a book where you can just...
 Well, maybe it kind of is like a book.
 If you want to simile with a book, you have to read it.
 You can't force the knowledge out of the pages.
 You can't pick up the book and stare more intently,
 you stare and the more knowledge you gain.
 Meditation is kind of like reading a book.
 You have to go page by page and the knowledge comes on its
 own.
 It comes in its own time.
 So rather than trying to force or magically make knowledge
 arise
 or even our idea that somehow you can make knowledge arise
 if you analyze,
 if you look closely enough.
 Rather than analyzing the book to get its knowledge, you
 have to read it.
 So in meditation you need...
 You can't create the knowledge, but you have to be intent
 upon it
 and you have to be clear in your mind about the nature of
 things.
 You have to have that as your goal.
 As you watch, you're trying to confront things.
 You have clear theory to back it up that's setting you in
 the right direction,
 what we intend to see in permanence, suffering and not so.
 It's important to be clever in the practice
 and not get caught up in doubts and debates and analysis.
 Often our search for knowledge gets in the way of our
 attainment of wisdom.
 So it's important that we put that aside when we practice
 and focus just on seeing clearly.
 It's like if you want wisdom you have to put aside
 knowledge.
 You have to put aside any kind of analysis you might have.
 Just open your eyes, learn how to open your eyes and how to
 see.
 Light, you need to have great light.
 You have to dispel the darkness.
 Ignore, avoid the darkness which is all kinds of judgments
 and doubts, confusion.
 Don't let yourself get confused or caught up in views.
 Views, of course, very dangerous.
 Sometimes we judge a teaching and that gets in the way of
 appreciating the truth
 or any goodness that might be in it.
 Alok, a bahulu, wisdom, of course, most important.
 If you want depth you have to gain wisdom.
 Your practice can be just tranquility.
 If you're fixed or focused on the pleasant, peaceful
 aspects of meditation, for example,
 you will never get depths of practice.
 Focusing on samatha meditation, focusing on peace or
 happiness or metta meditation, which is good.
 All of which is good. Samatha is also good.
 But there's a lack of depth. Your depth will never come
 until you gain wisdom and understanding,
 until you see clearly.
 Number two, yoga bahulu.
 Yoga is a strange one.
 But I didn't use the word yoga that much.
 But here he uses the word yoga.
 One should have much yoga, be full of yoga.
 So we have to understand the word yoga.
 It's not difficult to understand, but it meant something
 quite different from how we use the word in modern society.
 So in ancient times yoga was a word they used for spiritual
 practice.
 The word yoga means something like a yoke.
 But not the yoke of an egg. It means a yoke as in a piece
 of wood that an ox would put on the back of the ox
 and it allows it to pull the plow.
 So it's like a harness, a yoke.
 And so the idea was you would be dedicated or committed.
 Yoga was this commitment, spiritual commitment.
 That's how I like to translate the word religion.
 This word religion gets such a bad reputation because of
 how it's been used.
 But when it's used in the sense of being religious about
 something,
 in the sense of being dedicated to something intent upon it
,
 I think that's very much what yoga means.
 You'll often, I'm not sure so much how much the Buddha used
 it,
 but in the commentaries you'll often hear this word yoga v
achara again and again.
 We're reading the Melinda Panna.
 And again and again he refers to a meditator as a yoga vach
ara,
 which means someone basically who is committed to yoga,
 committed to a religious spiritual practice,
 someone who is cultivating or is practicing in a spiritual
 way,
 dedicated way.
 Like we're all doing here.
 You're dedicated to your practice.
 So yoga means like dedication, exertion I think it came to
 mean.
 So it probably has implications in regards to effort.
 I could say an important part of yoga is the effort.
 And effort in Buddhism really isn't about pushing yourself
 harder and harder,
 not so much.
 It really is about this intention or exertion.
 And dedication, in a good word.
 And being dedicated to something you try again, try and try
 again,
 never give up.
 It doesn't mean you have to push harder all the time.
 Just when you feel yourself, they say tātā which means sl
acking or flagging.
 You just feel like you're lacking in energy.
 You don't have to force yourself.
 You just have to try.
 And that dedication, and even when you can't walk so you
 stand,
 you can't stand your state.
 You keep trying.
 Yoga bahulō.
 Number three, veda bahulō.
 Now suppose with yoga you'd have to confine it more to the
 effort and dedication side
 because veda is another aspect of religion.
 Veda is this religious feeling, veda, vedana kind of.
 But here it refers to, I think it refers to sanvega,
 the idea of the urgency of the practice.
 If you want to really go deeper with your practice,
 it has to take on a sense of urgency, not just done as a
 hobby or a pastime,
 not even just as a matter of course.
 The Buddha said you should work to eradicate wrong view,
 the view of self, view of me and mine.
 He said you should work like a person with their head on
 fire.
 Suppose you're wearing a turban, I guess, and it was on
 fire.
 You'd be pretty darned intent upon,
 you'd have a sense of urgency in regards to putting out
 that fire.
 The fire of wrong view is much more dangerous.
 We don't know what's going to happen in the future where we
're going.
 If we die without any goodness in our hearts,
 we could be reborn in a very unpleasant place.
 We could even simply be born as an animal,
 which would be quite discouraging.
 It's very hard for an animal to be reborn as a human being.
 Much more common for them to be reborn as the same animal
 again and again and again.
 It's just very difficult to do the good activities that
 cultivate good qualities
 that are required to be born a human.
 So a sense of urgency.
 We're going to get old, we're going to get sick, we're
 going to die.
 Bad things could happen any time.
 We're not ready for them.
 It's very easy to get on the bad, the wrong, the bad path.
 Veda Bahulam.
 Number four, Asantuti Bahulam.
 Asantuti is contentment, which is actually funny enough,
 it's a very good quality in Buddhism.
 The Buddha said you should be santuti.
 It's the greatest blessing to be content.
 And so the Buddha taught both contentment and discontent.
 Contentment, we should be content with food, whatever food
 we get.
 We shouldn't try too hard to get good food, delicious food
 anyway.
 You should be a little concerned perhaps about healthy food
, not eating junk.
 You shouldn't be obsessed with it.
 You should be content with whatever you have.
 Clothing should be content with dwelling.
 We're having a debate over allowing meditators to switch
 rooms
 or whether we have to force them to be in the room.
 I don't think we do.
 I think changing rooms sometimes is important.
 But I think it's true quite often is just because, oh, that
's a nicer room,
 or this room is hot, this room is cold.
 What you have to be careful about, don't get too caught up
 in luxury
 or following your preferences and that sort of thing.
 So contentment is good, contentment with lodging, with
 medicine,
 not being too caught up in being too healthy or worrying
 about your health,
 worrying about pains or this sort of thing.
 But asantuti is of course very important as well
 because it's easy to become complacent.
 And this is in regards to practice, in regards to the Dham
ma.
 Don't be content with your gains.
 Don't rest on your laurels.
 We gain a lot from the meditation practice.
 It should be gaining every day.
 Every time you sit down, in fact, you should be gaining
 something.
 It's easy when you gain something fairly significant
 or what seems significant, you become complacent.
 Even asotapan, asakiragami, there were even anagamis that
 were complacent.
 And the Buddha said, "You guys are on the wrong path."
 He said, "You're not really followers of mine, anagami."
 He didn't probably say it like that, but he said,
 "You're not doing the right thing by being complacent."
 Hanagami is someone who has come very far in the practice.
 And the Buddha still said, "Don't be complacent.
 Don't be content with just that."
 Number five, anikita-duro.
 Anikita means to throw away or to put down, I guess.
 Anikita.
 Don't put down the work.
 Dura. Dura means duty.
 Don't shirk your duties or let them slide.
 It really means don't stop practicing.
 There are two dura, two dutis in Buddhism.
 Gantatura, which is the duty of study.
 And Vipassanandura, which is the duty to see clearly.
 Don't shirk either of these.
 You should study.
 Even just listening to the Dhamma, that's how you study.
 It's a great way to study.
 Reading books can also be useful.
 But most importantly, of course,
 don't put down the practice of mindfulness.
 This means that, of course, in life,
 don't let years go by where you've stopped meditating
 just because you're complacent.
 It could be many reasons.
 It doesn't have to be because you're content or complacent.
 It's easy to get caught up in work or relationships, life,
 travel,
 any of the ten balipoda, the ten impediments to practice.
 Don't let them get in your way.
 Don't put down the practice.
 You read in the Satipatanas with the commentary about on A
um's Round,
 the monks would take every step mindfully.
 And if they took one step without mindfulness, they would
 stop.
 And the monk behind them would know they were unmindful.
 And so it was quite, it kept you quite vigilant, actually,
 because if you stopped, then the people behind you would
 know that you're not mindful.
 So you would have tried your best not to have to stop.
 So the monks behind you wouldn't know or wouldn't criticize
 that well.
 You wouldn't feel ashamed of making the monks behind you
 stop.
 Even on Aum's Round, they went on Aum's Round.
 They would be mindful.
 They would put it down.
 You have to put it down sometimes when you teach.
 Even when you study, perhaps.
 When you're talking with people, you have difficulty when
 you're eating.
 But you shouldn't, you should try your best not to.
 Even when eating, try to be mindful.
 Even when talking, you can, to some extent anyway, be
 mindful.
 The physical act of talking is just a physical act,
 and you can be aware of your lips moving, of the feelings,
 and so on.
 Anikita duro, don't put down the practice,
 not just in terms of days, weeks, months, years, but in
 terms of moments.
 Throughout the day you're here, you have the opportunity to
 take this to its greatest extent.
 Not even sleeping. You know, you sleep less.
 Anikita, you don't ever put it down.
 If you do that, it's not that it's wrong to sleep or
 anything,
 but the greatness that comes from constant and continuous
 exertion, patience,
 and clarity of mind. This is greatness.
 This is where depth comes. You take your practice to a
 deeper level.
 So that's what, that's number five.
 Number six is kusalei su dhamme su uttaricha patareti.
 Number six is kusalei su dhamme su in regards to wholesome
 things, good things.
 Uttaricha patareti. Patareti, coming to or attaining to,
 attaining.
 Uttarim means greater. Uttarim, more and more.
 It's in line with not being content, not being complacent.
 The exertion greatness,
 which is the Buddha's words, has to do with going further
 and further.
 Not just having, not just being content with keeping moral
 precepts,
 but trying to be ethical in all of your movement.
 It's about going deeper and finding concentration,
 about finding wisdom and deeper wisdom.
 It's about not holding on to insights that you gain from
 the practice.
 It's about the fact that practice is always changing, is
 always going deeper.
 It's always going further. There's always more to learn
 until you completely free yourself from suffering.
 It's an activity that's always about, always adapting,
 always changing.
 The benefits and the knowledge and the understanding of the
 practice that you gained even today
 will not perfectly apply tomorrow because you have to go
 further.
 One of the important challenges of meditation is it's not a
 constant thing.
 So like Samatha meditation, where the object's always going
 to be the same
 and your relation to it is just about refining and refining
.
 In insight, there's a refining that goes on, but there's
 also an adapting and a going deeper.
 Going further, our relationship with our neurotic behaviors
, our depression, anxiety, worry, greed,
 we should constantly be coming to it with a fresh
 perspective,
 ready to see new things, ready to go deeper, ready to go
 further.
 So these are the six.
 Mahantatam, Mahantatadam.
 Damas of greatness in depth, waypulata.
 You want your practice to go deeper.
 That's just another way of describing, teaching,
 encouraging for us to dedicate ourselves to the practice.
 So that's the dhamma for tonight.
 Thank you for coming to listen.
 Thank you.
 Thank you.
