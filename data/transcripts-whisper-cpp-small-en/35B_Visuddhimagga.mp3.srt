1
00:00:00,000 --> 00:00:06,000
 The word aggregated, not so good here.

2
00:00:06,000 --> 00:00:15,000
 So we should substitute some other word, like group,

3
00:00:15,000 --> 00:00:19,570
 Syla group, Somadi group, Panya group or something like

4
00:00:19,570 --> 00:00:20,000
 that.

5
00:00:20,000 --> 00:00:25,250
 The Pali word is Khanda, but here Khanda does not mean the

6
00:00:25,250 --> 00:00:27,000
 same thing as Rupa Khanda,

7
00:00:27,000 --> 00:00:30,000
 Rupa Khanda, Vidhana Khanda and so on.

8
00:00:30,000 --> 00:00:33,000
 Here Syla Khanda, Somadi Khanda, Panya Khanda.

9
00:00:33,000 --> 00:00:36,000
 So I think it's a group.

10
00:00:36,000 --> 00:00:39,000
 A group is better word than aggregate here.

11
00:00:39,000 --> 00:00:44,000
 So according to three groups, for the path being selective,

12
00:00:44,000 --> 00:00:48,000
 it's included by the three aggregates which are

13
00:00:48,000 --> 00:00:50,000
 comprehensive,

14
00:00:50,000 --> 00:00:53,560
 as the city is by a kingdom according as it is said and so

15
00:00:53,560 --> 00:00:54,000
 on.

16
00:00:54,000 --> 00:01:04,000
 Now, what is the meaning of the path being selective?

17
00:01:04,000 --> 00:01:07,000
 Say Syla group.

18
00:01:07,000 --> 00:01:14,000
 Syla group means both mundane and supramandane.

19
00:01:14,000 --> 00:01:21,000
 There is mundane Syla and there is supramandane Syla.

20
00:01:21,000 --> 00:01:25,450
 So when we say Syla group, we include both mundane and sup

21
00:01:25,450 --> 00:01:26,000
ramandane.

22
00:01:26,000 --> 00:01:28,000
 So it is more comprehensive.

23
00:01:28,000 --> 00:01:31,000
 But Maga is only supramandane.

24
00:01:31,000 --> 00:01:37,000
 So it is called selective here.

25
00:01:37,000 --> 00:01:42,680
 There may be a better word for the word selective, I don't

26
00:01:42,680 --> 00:01:47,000
 know.

27
00:01:47,000 --> 00:01:57,420
 So the supramandane path is just a part of the whole group

28
00:01:57,420 --> 00:01:59,000
 of Syla.

29
00:01:59,000 --> 00:02:05,000
 So Syla group means mundane and supramandane Syla.

30
00:02:05,000 --> 00:02:07,000
 But Maga means only supramandane.

31
00:02:07,000 --> 00:02:12,000
 So it is less comprehensive.

32
00:02:12,000 --> 00:02:18,000
 As a city is by a kingdom, according as it is said.

33
00:02:18,000 --> 00:02:24,000
 And the quotation is from Isoda and Majima Nikaya.

34
00:02:24,000 --> 00:02:27,480
 The three groups are not included in the Noble Eightfold

35
00:02:27,480 --> 00:02:28,000
 Path,

36
00:02:28,000 --> 00:02:33,010
 Franvi Sagar, but the Noble Eightfold Path is included by

37
00:02:33,010 --> 00:02:34,000
 the three aggregates,

38
00:02:34,000 --> 00:02:37,000
 our three groups.

39
00:02:37,000 --> 00:02:44,710
 That means the three groups are more comprehensive than the

40
00:02:44,710 --> 00:02:46,000
 Noble Eightfold Path.

41
00:02:46,000 --> 00:02:51,620
 So it is not said that these groups are included in the

42
00:02:51,620 --> 00:02:54,000
 Noble Eightfold Path,

43
00:02:54,000 --> 00:03:00,550
 but that the Noble Eightfold Path is included by the three

44
00:03:00,550 --> 00:03:02,000
 aggregates.

45
00:03:02,000 --> 00:03:06,000
 Any right speech, any right action, any right livelihood.

46
00:03:06,000 --> 00:03:11,000
 These are included in the Virgil group.

47
00:03:11,000 --> 00:03:16,000
 So when we divide the eight factors into three groups,

48
00:03:16,000 --> 00:03:24,330
 the right speech, right action and right livelihood fall

49
00:03:24,330 --> 00:03:30,000
 under Virgil, Syla.

50
00:03:30,000 --> 00:03:32,370
 And then any right effort, right mindfulness and right

51
00:03:32,370 --> 00:03:33,000
 concentration.

52
00:03:33,000 --> 00:03:37,000
 These are included in the concentration group.

53
00:03:37,000 --> 00:03:41,000
 So these three are, they belong to the concentration group.

54
00:03:41,000 --> 00:03:44,740
 And the right view and right thinking, they are included in

55
00:03:44,740 --> 00:03:47,000
 the understanding group.

56
00:03:47,000 --> 00:03:52,000
 So Syla, Somari and Panya.

57
00:03:52,000 --> 00:03:54,680
 As to the three meanings, with right effort, concentration

58
00:03:54,680 --> 00:03:56,000
 cannot of its own nature

59
00:03:56,000 --> 00:03:59,590
 cause absorption through unification on the object and so

60
00:03:59,590 --> 00:04:00,000
 on.

61
00:04:00,000 --> 00:04:05,750
 Expleination is very important when you try to explain how

62
00:04:05,750 --> 00:04:11,000
 these factors work during meditation.

63
00:04:11,000 --> 00:04:20,000
 Now, here it is said that concentration alone by its nature

64
00:04:20,000 --> 00:04:24,000
 cannot cause absorption through unification of the object.

65
00:04:24,000 --> 00:04:27,570
 But with energy accomplishing, accomplishing its function

66
00:04:27,570 --> 00:04:28,000
 and so on.

67
00:04:28,000 --> 00:04:36,560
 So that means, Somari when only helped by energy and

68
00:04:36,560 --> 00:04:39,000
 mindfulness,

69
00:04:39,000 --> 00:04:41,000
 will really penetrate.

70
00:04:41,000 --> 00:04:45,090
 So if we want to get Somari and we have to have really, I

71
00:04:45,090 --> 00:04:46,000
 mean effort,

72
00:04:46,000 --> 00:04:52,000
 we must make effort and we must have mindfulness.

73
00:04:52,000 --> 00:04:56,320
 When there is effort and mindfulness, can there be

74
00:04:56,320 --> 00:04:58,000
 concentration?

75
00:04:58,000 --> 00:05:03,000
 So concentration is helped by effort and mindfulness.

76
00:05:03,000 --> 00:05:09,000
 So that is explained in this paragraph.

77
00:05:09,000 --> 00:05:16,600
 And then a simile is given for people going to a garden and

78
00:05:16,600 --> 00:05:21,000
 then feeding the fruit.

79
00:05:21,000 --> 00:05:27,810
 Then paragraph 99, here also Panya is assisted by right

80
00:05:27,810 --> 00:05:30,000
 thinking or Vitha-ga.

81
00:05:30,000 --> 00:05:33,620
 That is why right thinking is included in the group of P

82
00:05:33,620 --> 00:05:34,000
anya.

83
00:05:34,000 --> 00:05:37,010
 So as regards right view and right thinking, understanding

84
00:05:37,010 --> 00:05:38,000
 cannot of its own nature

85
00:05:38,000 --> 00:05:42,000
 define an object as impermanent, painful, not-self.

86
00:05:42,000 --> 00:05:45,960
 But with applied thought, that is Vitha-ga, giving

87
00:05:45,960 --> 00:05:50,000
 assistance by repeatedly hitting the object it can.

88
00:05:50,000 --> 00:05:57,650
 That means the right thought or initial application takes

89
00:05:57,650 --> 00:06:00,000
 the mind to the object,

90
00:06:00,000 --> 00:06:04,000
 mind together with the confidence.

91
00:06:04,000 --> 00:06:09,320
 So only when Vitha-ga takes the mind to the object can

92
00:06:09,320 --> 00:06:13,000
 understanding of Panya penetrate it.

93
00:06:13,000 --> 00:06:17,000
 It is like if you don't take a person to a certain place,

94
00:06:17,000 --> 00:06:20,000
 that person will not know anything about that place.

95
00:06:20,000 --> 00:06:25,000
 So you have to take it, take him there and then he sees it

96
00:06:25,000 --> 00:06:28,000
 and then he knows everything about it.

97
00:06:28,000 --> 00:06:30,620
 If you do not take him there, then he will not know about

98
00:06:30,620 --> 00:06:31,000
 it.

99
00:06:31,000 --> 00:06:35,650
 In the same way, if Vitha-ga or the right thought does not

100
00:06:35,650 --> 00:06:38,000
 take the mind to the object,

101
00:06:38,000 --> 00:06:42,000
 then Panya cannot do anything.

102
00:06:42,000 --> 00:06:46,150
 That is why the Panya is assisted by Vitha-ga or right

103
00:06:46,150 --> 00:06:47,000
 thinking.

104
00:06:47,000 --> 00:06:54,690
 So they are grouped together as understanding group and

105
00:06:54,690 --> 00:07:00,000
 also is similarly given.

106
00:07:00,000 --> 00:07:04,000
 So the part is included by the three aggregates.

107
00:07:04,000 --> 00:07:10,000
 And then paragraph 103, let's do similar and dissimilar.

108
00:07:10,000 --> 00:07:15,460
 It is just something I am playing with the different

109
00:07:15,460 --> 00:07:19,000
 meanings of the noble truths.

110
00:07:19,000 --> 00:07:22,200
 All the truths are similar to each other because they are

111
00:07:22,200 --> 00:07:23,000
 not unreal,

112
00:07:23,000 --> 00:07:27,240
 are vital self and are difficult to penetrate according as

113
00:07:27,240 --> 00:07:28,000
 it is said.

114
00:07:28,000 --> 00:07:31,000
 What do you think Anana, which is more difficult to do,

115
00:07:31,000 --> 00:07:33,000
 more difficult to perform,

116
00:07:33,000 --> 00:07:36,000
 that a man should shoot an arrow through a small keyhole

117
00:07:36,000 --> 00:07:37,000
 from a distance,

118
00:07:37,000 --> 00:07:40,610
 time after time without missing, or that he should

119
00:07:40,610 --> 00:07:43,000
 penetrate the tip of a hair split,

120
00:07:43,000 --> 00:07:47,000
 split a hundred times with the tip of a similar hair.

121
00:07:47,000 --> 00:07:50,290
 And this is more difficult to do, when also more difficult

122
00:07:50,290 --> 00:07:51,000
 to perform,

123
00:07:51,000 --> 00:07:54,070
 that a man should penetrate the tip of a hair split a

124
00:07:54,070 --> 00:07:57,000
 hundred times with the tip of a similar hair.

125
00:07:57,000 --> 00:07:59,730
 They penetrate something more difficult to penetrate than

126
00:07:59,730 --> 00:08:01,000
 that, Ananda,

127
00:08:01,000 --> 00:08:05,000
 who penetrate correctly, thus this is suffering and so on.

128
00:08:05,000 --> 00:08:06,960
 It is more difficult to penetrate the four noble truths

129
00:08:06,960 --> 00:08:12,000
 than to penetrate the tip of a hair split a hundred times

130
00:08:12,000 --> 00:08:15,000
 with another tip of a hair.

131
00:08:15,000 --> 00:08:19,000
 So if you strike the tip of a needle with another needle,

132
00:08:19,000 --> 00:08:20,000
 something like that.

133
00:08:20,000 --> 00:08:22,000
 It is a real challenge.

134
00:08:22,000 --> 00:08:25,000
 That's right.

135
00:08:25,000 --> 00:08:28,420
 And the first two are similar since they are performed and

136
00:08:28,420 --> 00:08:29,000
 so on.

137
00:08:29,000 --> 00:08:32,350
 They are similar in one sense and dissimilar in another

138
00:08:32,350 --> 00:08:33,000
 sense.

139
00:08:33,000 --> 00:08:39,950
 So this is like just shuffling the four noble truths as to

140
00:08:39,950 --> 00:08:43,000
 similarity and dissimilarity.

141
00:08:43,000 --> 00:08:51,000
 Now, next chapter, the dependent origination.

142
00:08:51,000 --> 00:08:56,000
 First, the word, the Pali word.

143
00:08:56,000 --> 00:09:03,090
 So please look at the sheets and I will explain to you and

144
00:09:03,090 --> 00:09:06,000
 then read the book.

145
00:09:06,000 --> 00:09:11,060
 So we cannot do it with the Pali word, so we take the Pali

146
00:09:11,060 --> 00:09:12,000
 words.

147
00:09:12,000 --> 00:09:15,000
 The word is Pateachasamopada.

148
00:09:15,000 --> 00:09:21,320
 Now, in the Bhishulimara, in this book, Pateachasamopada is

149
00:09:21,320 --> 00:09:23,000
 said to mean,

150
00:09:23,000 --> 00:09:26,000
 states that are conditioned.

151
00:09:26,000 --> 00:09:30,000
 That means causes, states that are conditioned.

152
00:09:30,000 --> 00:09:35,000
 Namely, Awijah means ignorance and so on.

153
00:09:35,000 --> 00:09:41,590
 Now, before we explain to you this, I want you to be

154
00:09:41,590 --> 00:09:46,000
 familiar with the formula of the dependent origination.

155
00:09:46,000 --> 00:09:51,800
 Depending upon Awijah, I mean, ignorance, there are

156
00:09:51,800 --> 00:09:53,000
 formations.

157
00:09:53,000 --> 00:09:56,720
 Depending upon formations, there is consciousness and so on

158
00:09:56,720 --> 00:09:57,000
.

159
00:09:57,000 --> 00:10:07,130
 So, ignorance, formations, consciousness, mentality,

160
00:10:07,130 --> 00:10:11,000
 materiality, six bases.

161
00:10:11,000 --> 00:10:15,000
 What else? What next?

162
00:10:16,000 --> 00:10:23,710
 Contact, feeling, craving, cleaning, becoming, birth, old

163
00:10:23,710 --> 00:10:26,000
 age and death.

164
00:10:26,000 --> 00:10:32,000
 So, this sequence you have to be familiar with.

165
00:10:32,000 --> 00:10:37,000
 Now, what does the word Pateachasamopada mean?

166
00:10:37,000 --> 00:10:45,160
 Pateachasamopada means states that are conditioned. That is

167
00:10:45,160 --> 00:10:46,000
 according to this commentary.

168
00:10:46,000 --> 00:10:50,000
 And then there is another word, Pateachasamopana.

169
00:10:50,000 --> 00:10:55,000
 Pateachasamopana means states that are conditioned.

170
00:10:55,000 --> 00:11:02,000
 So, they are results of fruits, namely, Jara, Marana, etc.

171
00:11:02,000 --> 00:11:05,870
 In reality, all the numbers mentioned in the Pateachasamop

172
00:11:05,870 --> 00:11:08,000
ada are both included in both.

173
00:11:08,000 --> 00:11:14,000
 They are both Pateachasamopada and Pateachasamopana.

174
00:11:14,000 --> 00:11:17,200
 Only the first indicates conditions and the second

175
00:11:17,200 --> 00:11:18,000
 conditions.

176
00:11:18,000 --> 00:11:23,100
 Because all those mentioned in the Pateachasamopada are

177
00:11:23,100 --> 00:11:25,000
 interdependent.

178
00:11:25,000 --> 00:11:31,010
 So, first you should understand this and then the paragraph

179
00:11:31,010 --> 00:11:33,000
 4 you may read.

180
00:11:33,000 --> 00:11:37,000
 Then, please go down to the bottom.

181
00:11:37,000 --> 00:11:43,000
 In the word Pateachasamopada, we have the word Opada there.

182
00:11:43,000 --> 00:11:48,000
 So, the word Opada has three meanings.

183
00:11:48,000 --> 00:11:52,000
 One is that which arises.

184
00:11:52,000 --> 00:11:56,000
 The second meaning is that which causes others to arise.

185
00:11:56,000 --> 00:11:59,000
 This is a causative sense.

186
00:11:59,000 --> 00:12:02,000
 And the third meaning is just arising.

187
00:12:02,000 --> 00:12:04,000
 It's an action.

188
00:12:04,000 --> 00:12:06,000
 We call it verbal noun.

189
00:12:06,000 --> 00:12:12,000
 So, the word Opada can mean three meanings.

190
00:12:12,000 --> 00:12:15,990
 But with regard to the word Pateachasamopada, only the two,

191
00:12:15,990 --> 00:12:18,000
 the first two are accepted.

192
00:12:18,000 --> 00:12:24,000
 And the last one is rejected in the Visodimaga.

193
00:12:24,000 --> 00:12:36,470
 And so that rejection you can read in paragraphs 8 through

194
00:12:36,470 --> 00:12:38,000
 13.

195
00:12:38,000 --> 00:12:49,000
 And then the meaning of the word Pateachasamopada.

196
00:12:49,000 --> 00:12:54,000
 There are four meanings given to the word Pateachasamopada.

197
00:12:54,000 --> 00:12:59,390
 So, the first meaning, that which is to be arrived at or

198
00:12:59,390 --> 00:13:02,000
 that which is to be known.

199
00:13:02,000 --> 00:13:05,000
 And it is the meaning of Pateachasamopada here.

200
00:13:05,000 --> 00:13:09,000
 And which arises together and rightly.

201
00:13:09,000 --> 00:13:15,000
 So, I put the colors to correspond.

202
00:13:15,000 --> 00:13:21,000
 So, the word Pateachasamopada is composed of San and Opada.

203
00:13:21,000 --> 00:13:29,000
 And San is made to mean together and rightly here.

204
00:13:29,000 --> 00:13:33,000
 And Opada is made to mean that which arises.

205
00:13:33,000 --> 00:13:39,000
 So, Samopada means that which arises together and rightly.

206
00:13:39,000 --> 00:13:50,000
 And together means not singly, not formation only.

207
00:13:50,000 --> 00:13:53,000
 Not consciousness only.

208
00:13:53,000 --> 00:13:56,990
 Because when consciousness arises, then the major factors

209
00:13:56,990 --> 00:13:58,000
 also arise.

210
00:13:58,000 --> 00:14:04,000
 And at the same time, they arise also material properties.

211
00:14:04,000 --> 00:14:06,000
 So, in that way.

212
00:14:06,000 --> 00:14:10,000
 And together means not singly, not one by one.

213
00:14:10,000 --> 00:14:13,000
 And rightly means not without cause.

214
00:14:13,000 --> 00:14:17,950
 So, they arise, rightly means they arise with their

215
00:14:17,950 --> 00:14:20,000
 respective causes.

216
00:14:20,000 --> 00:14:22,000
 Not without cause.

217
00:14:22,000 --> 00:14:25,630
 So, by this definition the conditioned states are called P

218
00:14:25,630 --> 00:14:27,000
ateachasamopada.

219
00:14:27,000 --> 00:14:32,580
 So, by this definition the results are called Pateachasamop

220
00:14:32,580 --> 00:14:33,000
ada.

221
00:14:33,000 --> 00:14:40,380
 Now, second definition is that which depending upon the

222
00:14:40,380 --> 00:14:42,000
 convergence of conditions

223
00:14:42,000 --> 00:14:47,000
 and that is the meaning of Pateachas, arises together.

224
00:14:47,000 --> 00:14:52,000
 Here, sound has only one meaning together.

225
00:14:52,000 --> 00:14:57,000
 And Opada has meaning that which arises.

226
00:14:57,000 --> 00:15:04,000
 And Pateachas means having depended upon or depending upon.

227
00:15:04,000 --> 00:15:08,320
 So, by this definition to the conditioned states, the

228
00:15:08,320 --> 00:15:13,000
 fruits are meant.

229
00:15:13,000 --> 00:15:18,470
 Now, if you look at the first line, you see that Pateachas

230
00:15:18,470 --> 00:15:21,000
amopada means states that are conditioned.

231
00:15:21,000 --> 00:15:25,300
 But here, the first meaning is again according to the first

232
00:15:25,300 --> 00:15:27,000
 and the second meaning,

233
00:15:27,000 --> 00:15:32,000
 Pateachasamopada means results and not causes.

234
00:15:32,000 --> 00:15:40,170
 But the commentary, Venerable Bho Dha Kosa is very

235
00:15:40,170 --> 00:15:46,000
 persistent in making us accept that

236
00:15:46,000 --> 00:15:52,330
 Pateachasamopada really means the conditions of causes and

237
00:15:52,330 --> 00:15:55,000
 not the effects, not the fruits.

238
00:15:55,000 --> 00:16:00,020
 Then how to explain that according to the definition given

239
00:16:00,020 --> 00:16:01,000
 by himself?

240
00:16:01,000 --> 00:16:05,220
 Number one definition and number two definition means the

241
00:16:05,220 --> 00:16:07,000
 results and not the cause.

242
00:16:07,000 --> 00:16:15,000
 So, he has to conjure up something.

243
00:16:15,000 --> 00:16:18,720
 So, although by definitions one and two, the word Pateachas

244
00:16:18,720 --> 00:16:20,000
amopada means conditioned states,

245
00:16:20,000 --> 00:16:24,000
 we are here to understand it in a figurative sense.

246
00:16:24,000 --> 00:16:27,000
 So, it means states that are conditioned.

247
00:16:27,000 --> 00:16:31,160
 So, actually, we must take that the conditions are meant by

248
00:16:31,160 --> 00:16:33,000
 the word Pateachasamopada,

249
00:16:33,000 --> 00:16:36,000
 even though we follow these two definitions.

250
00:16:36,000 --> 00:16:43,000
 How? That is explained in paragraph 16.

251
00:16:43,000 --> 00:16:54,460
 Sometimes, we, not only we, even in the text, there are

252
00:16:54,460 --> 00:16:57,000
 some sayings which have to be taken figuratively.

253
00:16:57,000 --> 00:17:02,000
 Now, there is one statement in the Dhammabara which says,

254
00:17:02,000 --> 00:17:08,000
 "The appearance of Buddhas is bliss."

255
00:17:08,000 --> 00:17:13,000
 Now, the appearance of Buddha is not bliss, actually.

256
00:17:13,000 --> 00:17:16,000
 It is the cause of bliss.

257
00:17:16,000 --> 00:17:20,000
 Because when Buddhas appear, then people get enlightened

258
00:17:20,000 --> 00:17:23,000
 and so there is bliss for them.

259
00:17:23,000 --> 00:17:27,590
 So, the appearance of Buddha is actually not bliss, but the

260
00:17:27,590 --> 00:17:29,000
 cause of bliss.

261
00:17:29,000 --> 00:17:34,000
 But it is just the appearance of Buddha is bliss.

262
00:17:34,000 --> 00:17:46,040
 Or, in other words, like saying, "Diabetes is sugar,"

263
00:17:46,040 --> 00:17:49,000
 something like that.

264
00:17:49,000 --> 00:17:53,000
 Actually, diabetes is not sugar, but it is caused by sugar.

265
00:17:53,000 --> 00:17:57,000
 But we might say diabetes is sugar, something like that.

266
00:17:57,000 --> 00:17:59,500
 So, here also, although the word Buddhas and Mopada means "

267
00:17:59,500 --> 00:18:04,000
conditions, things or results,"

268
00:18:04,000 --> 00:18:09,660
 we must understand that it means the conditions or causes

269
00:18:09,660 --> 00:18:12,000
 and not the results.

270
00:18:12,000 --> 00:18:17,000
 So, that is explained in paragraph 16.

271
00:18:17,000 --> 00:18:21,000
 Now, the third meaning, that which is to be gone towards

272
00:18:21,000 --> 00:18:26,000
 and which originates, states together.

273
00:18:26,000 --> 00:18:31,080
 Now, here, the word ovara means that which causes to arise,

274
00:18:31,080 --> 00:18:34,000
 that which produces.

275
00:18:34,000 --> 00:18:40,570
 And some means together, which produces things together,

276
00:18:40,570 --> 00:18:44,000
 things that arise together.

277
00:18:44,000 --> 00:18:48,100
 That which is to be gone towards, that is the meaning of

278
00:18:48,100 --> 00:18:50,000
 the word paticca,

279
00:18:50,000 --> 00:18:55,000
 which is to be gone towards really means...

280
00:18:55,000 --> 00:19:09,000
 Now, in Buddhism, the theory of causation is like that.

281
00:19:09,000 --> 00:19:17,000
 On account of many causes, there are many results.

282
00:19:17,000 --> 00:19:20,000
 We will come to that later, not today.

283
00:19:20,000 --> 00:19:25,000
 So, there is not one cause and one effect,

284
00:19:25,000 --> 00:19:29,370
 or not one cause and many effects, or many causes and one

285
00:19:29,370 --> 00:19:30,000
 effect,

286
00:19:30,000 --> 00:19:36,240
 but there is many causes and many effects that is accepted

287
00:19:36,240 --> 00:19:38,000
 by Buddhism.

288
00:19:38,000 --> 00:19:44,000
 So, here, which is to be gone towards means...

289
00:19:44,000 --> 00:19:48,000
 The conditions, two or three conditions meet together.

290
00:19:48,000 --> 00:19:53,050
 They must meet together in order to produce the result,

291
00:19:53,050 --> 00:19:56,000
 like three sticks put together.

292
00:19:56,000 --> 00:19:59,550
 So, they must come together and they must depend on each

293
00:19:59,550 --> 00:20:03,000
 other in order to produce something.

294
00:20:03,000 --> 00:20:08,170
 So, that is why they are called here, which is to be gone

295
00:20:08,170 --> 00:20:09,000
 towards.

296
00:20:09,000 --> 00:20:13,980
 And when they produce sticks, they produce sticks together,

297
00:20:13,980 --> 00:20:18,000
 not one stick only.

298
00:20:18,000 --> 00:20:25,000
 Now, the obvious example is the relinking.

299
00:20:25,000 --> 00:20:28,640
 So, when karma produces its effects, then there is rel

300
00:20:28,640 --> 00:20:30,000
inking, right?

301
00:20:30,000 --> 00:20:32,000
 So, relinking means what?

302
00:20:32,000 --> 00:20:37,690
 At the same moment, there is consciousness, there are

303
00:20:37,690 --> 00:20:43,000
 mental factors and there are matter born of karma.

304
00:20:43,000 --> 00:20:46,000
 So, they arise together.

305
00:20:46,000 --> 00:20:57,210
 So, "Pateja Samobara" means here, that which is to be gone

306
00:20:57,210 --> 00:21:01,000
 towards and which originates sticks together.

307
00:21:01,000 --> 00:21:05,000
 Now, by this definition, what is man?

308
00:21:05,000 --> 00:21:10,760
 Causes, conditions, because they originate or they produce

309
00:21:10,760 --> 00:21:12,000
 other sticks.

310
00:21:12,000 --> 00:21:16,800
 And then, the fourth meaning, that which, depending upon

311
00:21:16,800 --> 00:21:21,000
 one another, it is more or less the same as the third one,

312
00:21:21,000 --> 00:21:25,000
 originates evenly and together.

313
00:21:25,000 --> 00:21:32,000
 Here, Sam is made to mean two things, evenly and together.

314
00:21:32,000 --> 00:21:36,000
 In the book, it is translated as "equally".

315
00:21:36,000 --> 00:21:42,340
 So, evenly means not piecemeal, not one and then next one

316
00:21:42,340 --> 00:21:46,000
 and then next one, but two or three things together.

317
00:21:46,000 --> 00:21:48,000
 So, not piecemeal.

318
00:21:48,000 --> 00:21:52,000
 And together means not one after the other.

319
00:21:52,000 --> 00:22:03,030
 Whether the product or the result is consciousness or

320
00:22:03,030 --> 00:22:07,000
 material property.

321
00:22:07,000 --> 00:22:16,000
 It arises with other things too, right?

322
00:22:16,000 --> 00:22:19,000
 When consciousness arises, there are mental factors too.

323
00:22:19,000 --> 00:22:23,780
 When material properties are produced, they are produced in

324
00:22:23,780 --> 00:22:27,000
 what we call "kalabas", the groups.

325
00:22:27,000 --> 00:22:32,000
 So, they arise in groups and so they are together.

326
00:22:32,000 --> 00:22:36,540
 So, they are not produced just one at a time, but they are

327
00:22:36,540 --> 00:22:37,000
 produced,

328
00:22:37,000 --> 00:22:40,220
 if there are eight material properties, then the eight are

329
00:22:40,220 --> 00:22:42,000
 produced at the same moment.

330
00:22:42,000 --> 00:22:48,610
 So, they are produced evenly and they are produced together

331
00:22:48,610 --> 00:22:49,000
.

332
00:22:49,000 --> 00:22:53,000
 So, by this definition too, the conditions are met.

333
00:22:53,000 --> 00:23:01,830
 So, the definition three and four tell us that the "paditya

334
00:23:01,830 --> 00:23:06,000
 samobata" means conditions, right?

335
00:23:06,000 --> 00:23:16,000
 Here we don't have to have recourse to the figurative usage

336
00:23:16,000 --> 00:23:16,000
.

337
00:23:16,000 --> 00:23:19,790
 So, here directly the word "paditya samobata" means

338
00:23:19,790 --> 00:23:21,000
 conditions.

339
00:23:21,000 --> 00:23:32,460
 Now, in all these definitions, you see that "that", "that",

340
00:23:32,460 --> 00:23:33,000
 "that".

341
00:23:33,000 --> 00:23:39,240
 So, "that" means a group of conditions or group of states

342
00:23:39,240 --> 00:23:42,000
 that are conditioned.

343
00:23:42,000 --> 00:23:47,000
 That is why the singular number is used here.

344
00:23:47,000 --> 00:23:54,000
 Although, actually, it means a multiplicity of conditions

345
00:23:54,000 --> 00:24:00,000
 and also a multiplicity of states that are conditioned.

346
00:24:02,000 --> 00:24:09,000
 Now, what about "paditya samobata" as a doctrine?

347
00:24:09,000 --> 00:24:14,210
 Here "paditya samobata" means not doctrine, but causes or

348
00:24:14,210 --> 00:24:15,000
 defense.

349
00:24:15,000 --> 00:24:19,000
 But we call this doctrine also "paditya samobata".

350
00:24:19,000 --> 00:24:26,000
 So, as a doctrine, what should we call it in English?

351
00:24:30,000 --> 00:24:34,300
 It is translated as dependent origination, dependent

352
00:24:34,300 --> 00:24:38,000
 arising, dependent co-orising.

353
00:24:38,000 --> 00:24:46,980
 Dependent co-orising is, say, the right translation because

354
00:24:46,980 --> 00:24:50,000
 there are, you see, there are two together, right?

355
00:24:50,000 --> 00:24:56,000
 But I have one fear about using co-orising

356
00:24:56,000 --> 00:25:01,390
 because somebody might take that co-orising means cause and

357
00:25:01,390 --> 00:25:04,000
 effect arising together.

358
00:25:04,000 --> 00:25:12,100
 It does not necessarily mean that the cause and effect

359
00:25:12,100 --> 00:25:15,000
 arise together.

360
00:25:15,000 --> 00:25:23,000
 Together here means just the effects arising in any group

361
00:25:23,000 --> 00:25:25,000
 or something like that.

362
00:25:25,000 --> 00:25:30,000
 Not cause and effect arising together, but there is cause

363
00:25:30,000 --> 00:25:34,000
 and effect and effect consist of, say, more than one thing.

364
00:25:34,000 --> 00:25:38,660
 So that is what is meant by "together" in these definitions

365
00:25:38,660 --> 00:25:39,000
.

366
00:25:39,000 --> 00:25:43,620
 But using the term "dependent", doesn't that make it clear

367
00:25:43,620 --> 00:25:45,000
 or doesn't it?

368
00:25:45,000 --> 00:25:49,000
 I don't know. Dependent co-orising.

369
00:25:49,000 --> 00:25:56,990
 If it was just co-orising, then you could see, but isn't

370
00:25:56,990 --> 00:26:01,000
 the cause and effect...

371
00:26:01,000 --> 00:26:08,330
 We often say co-origination rather than co-orising. Is that

372
00:26:08,330 --> 00:26:09,000
 any better?

373
00:26:09,000 --> 00:26:15,400
 I don't think so because, you know, two meanings are given

374
00:26:15,400 --> 00:26:17,000
 to the word "ubhara" here.

375
00:26:17,000 --> 00:26:24,000
 In English we cannot have one word which means two things.

376
00:26:24,000 --> 00:26:29,620
 So we have to choose one or the other arising co-orig

377
00:26:29,620 --> 00:26:31,000
ination.

378
00:26:31,000 --> 00:26:38,560
 Don't we say that clinging arises from craving? Is that not

379
00:26:38,560 --> 00:26:40,000
 a causal relationship?

380
00:26:40,000 --> 00:26:43,000
 You're saying it's not a causal relationship.

381
00:26:43,000 --> 00:26:47,000
 There is a causal relationship between these.

382
00:26:47,000 --> 00:26:56,000
 But sometimes, now, we call them "links", say, "ignorance"

383
00:26:56,000 --> 00:26:57,000
 and "relationship"

384
00:26:57,000 --> 00:27:00,570
 between "ignorance" and "formations", "formations" and "

385
00:27:00,570 --> 00:27:02,000
conscious" and so on.

386
00:27:02,000 --> 00:27:11,560
 Some relations are as producer and produced, but most of

387
00:27:11,560 --> 00:27:18,000
 them are arising together and helping each other.

388
00:27:18,000 --> 00:27:27,490
 So co-orising can mean cause and effect arising together,

389
00:27:27,490 --> 00:27:30,000
 but not always.

390
00:27:30,000 --> 00:27:32,000
 You mean sometimes separately, sometimes?

391
00:27:32,000 --> 00:27:37,990
 That's right, yes, because let's say the relationship

392
00:27:37,990 --> 00:27:43,000
 between mental formations and consciousness.

393
00:27:43,000 --> 00:27:48,090
 Mental formation means karma and consciousness means

394
00:27:48,090 --> 00:27:51,000
 resultant consciousness.

395
00:27:51,000 --> 00:27:55,270
 So they belong to different times. They don't arise

396
00:27:55,270 --> 00:27:59,000
 together, but they are related

397
00:27:59,000 --> 00:28:05,000
 as cause and effect.

398
00:28:05,000 --> 00:28:11,420
 Then consciousness and namarupa, I mean namarupa, mentality

399
00:28:11,420 --> 00:28:13,000
 and materiality,

400
00:28:13,000 --> 00:28:17,620
 then in that case, they arise together. Consciousness and

401
00:28:17,620 --> 00:28:21,000
 mentality and materiality arise together.

402
00:28:21,000 --> 00:28:28,050
 Their relationship is not producer and the produced, but

403
00:28:28,050 --> 00:28:33,000
 just helping each other, supporting each other.

404
00:28:33,000 --> 00:28:39,710
 So that is why it is important to understand the paticca

405
00:28:39,710 --> 00:28:44,000
 samubhara with reference to the patthana.

406
00:28:44,000 --> 00:28:47,830
 That is why the patthana is given in this book. So we'll

407
00:28:47,830 --> 00:28:50,000
 come to this later.

408
00:28:50,000 --> 00:28:57,260
 Only when you understand with reference to patthana do you

409
00:28:57,260 --> 00:28:59,000
 really understand paticca samubhara.

410
00:28:59,000 --> 00:29:03,580
 Otherwise there may be something missing in your knowledge

411
00:29:03,580 --> 00:29:07,000
 of understanding of paticca samubhara.

412
00:29:07,000 --> 00:29:19,380
 Because we think that when there is ignorance, there are

413
00:29:19,380 --> 00:29:21,850
 formations, when there are formations, there is

414
00:29:21,850 --> 00:29:23,000
 consciousness,

415
00:29:23,000 --> 00:29:27,490
 we normally think that one is caused by the other. But it

416
00:29:27,490 --> 00:29:31,000
 is not the case with every of the links.

417
00:29:31,000 --> 00:29:36,160
 Some links are related as cause and effect and some are not

418
00:29:36,160 --> 00:29:42,000
 cause and effect, but just supporting each other.

419
00:29:42,000 --> 00:29:47,430
 Like a group of people doing the same work. So they help

420
00:29:47,430 --> 00:29:52,080
 each other, they support each other and then they do the

421
00:29:52,080 --> 00:29:53,000
 work.

422
00:29:53,000 --> 00:29:58,780
 So those relationships are to be understood with reference

423
00:29:58,780 --> 00:30:02,650
 to patthana, because of relations, the 34th cause of

424
00:30:02,650 --> 00:30:06,000
 relations given in this chapter.

425
00:30:06,000 --> 00:30:17,310
 So this is the bedrock understanding and then we can go to

426
00:30:17,310 --> 00:30:22,000
 the chapter itself.

427
00:30:22,000 --> 00:30:33,000
 Now let me see.

428
00:30:33,000 --> 00:30:39,760
 So in the paragraph 16, something 18 and so on, if it is

429
00:30:39,760 --> 00:30:47,000
 difficult there to understand, just come back here.

430
00:30:47,000 --> 00:30:52,850
 There are so many parenthesis, brackets, and then it is

431
00:30:52,850 --> 00:31:11,000
 difficult to read actually, and then polywars and...

432
00:31:11,000 --> 00:31:14,000
 So we will do it next week.

433
00:31:14,000 --> 00:31:23,350
 We may not read 24 conditions the next week, but you can

434
00:31:23,350 --> 00:31:25,000
 find them.

435
00:31:25,000 --> 00:31:28,000
 Let me see.

436
00:31:28,000 --> 00:31:43,000
 Yeah, 611. So there are 24 conditions.

437
00:31:43,000 --> 00:31:52,590
 So please read about... you will have to read more this

438
00:31:52,590 --> 00:31:54,000
 week.

439
00:31:54,000 --> 00:32:00,000
 So up to the end of the 24 conditions.

440
00:32:00,000 --> 00:32:12,000
 If you read with this, I think it will be a great help.

441
00:32:12,000 --> 00:32:17,050
 And then in the word explanations, there is something like

442
00:32:17,050 --> 00:32:19,000
... what do you call it?

443
00:32:19,000 --> 00:32:30,000
 Etymological acrobatics.

