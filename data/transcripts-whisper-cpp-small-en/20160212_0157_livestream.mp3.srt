1
00:00:00,000 --> 00:00:04,840
 Good evening everyone.

2
00:00:04,840 --> 00:00:10,840
 Broadcasting live, February 11th, 2016.

3
00:00:10,840 --> 00:00:17,240
 I'm actually just coming on to say that I'm not going to be

4
00:00:17,240 --> 00:00:20,200
 broadcasting tonight.

5
00:00:20,200 --> 00:00:23,200
 Well, not substantially.

6
00:00:23,200 --> 00:00:32,190
 Sorry, but tomorrow morning I have an exam and I really

7
00:00:32,190 --> 00:00:36,400
 should study tonight.

8
00:00:36,400 --> 00:00:53,600
 And so, yeah, just a little busy.

9
00:00:53,600 --> 00:00:56,600
 But we have a nice quote today.

10
00:00:56,600 --> 00:01:06,600
 It's an interesting word, talking about limited actions.

11
00:01:06,600 --> 00:01:10,600
 I don't have the source here.

12
00:01:10,600 --> 00:01:18,970
 But if I understand correctly, this verse is on the power

13
00:01:18,970 --> 00:01:20,600
 of love.

14
00:01:20,600 --> 00:01:29,480
 It's from the Jātakas, so it's not... it's not talking

15
00:01:29,480 --> 00:01:33,240
 about deeply Buddhist teachings.

16
00:01:33,240 --> 00:01:38,230
 Like the power of love is great and all, but you could

17
00:01:38,230 --> 00:01:40,800
 argue that it's not essential or

18
00:01:40,800 --> 00:01:44,120
 it's not sufficient.

19
00:01:44,120 --> 00:01:46,560
 Neither necessary nor sufficient.

20
00:01:46,560 --> 00:01:51,560
 Is it helpful for the past?

21
00:01:51,560 --> 00:01:53,560
 For sure.

22
00:01:53,560 --> 00:02:00,870
 And so when one dwells with love in one's heart, it helps

23
00:02:00,870 --> 00:02:04,680
 dispel feelings of guilt.

24
00:02:04,680 --> 00:02:10,880
 It helps dispel feelings of enmity or vengeance.

25
00:02:10,880 --> 00:02:17,250
 It helps us to be aware of the grudges or conflicts in our

26
00:02:17,250 --> 00:02:18,480
 mind.

27
00:02:18,480 --> 00:02:24,130
 So we practice loving kindness sort of surprisingly for our

28
00:02:24,130 --> 00:02:25,680
 own benefit.

29
00:02:25,680 --> 00:02:27,840
 We're not really expecting that by sending love we're going

30
00:02:27,840 --> 00:02:28,840
 to help those people that

31
00:02:28,840 --> 00:02:32,560
 we wish well of, not directly in life.

32
00:02:32,560 --> 00:02:37,640
 Of course, there is a very important benefit for the people

33
00:02:37,640 --> 00:02:39,880
 who we're sending it to, and

34
00:02:39,880 --> 00:02:46,760
 we are going to appreciate them more.

35
00:02:46,760 --> 00:02:58,240
 Be less inclined to fight with them, to conflict with those

36
00:02:58,240 --> 00:03:04,800
 people.

37
00:03:04,800 --> 00:03:08,660
 And so it goes with these types of meditation, like there's

38
00:03:08,660 --> 00:03:11,600
 many different protective meditations.

39
00:03:11,600 --> 00:03:15,210
 But it did spend a lot of time talking about the Brahmo Yā

40
00:03:15,210 --> 00:03:17,520
as, and I think that's important

41
00:03:17,520 --> 00:03:19,480
 not to lose track of.

42
00:03:19,480 --> 00:03:23,500
 It's important not to overemphasize the importance of love,

43
00:03:23,500 --> 00:03:26,320
 but it's also important not to neglect

44
00:03:26,320 --> 00:03:27,920
 the importance of it.

45
00:03:27,920 --> 00:03:32,870
 But as meditation practices, love, compassion, sympathy,

46
00:03:32,870 --> 00:03:36,160
 and equanimity are very important,

47
00:03:36,160 --> 00:03:41,680
 very useful meditation for an aspiring Buddhist.

48
00:03:41,680 --> 00:03:46,330
 Anyway, and there's not that much to say about the quote

49
00:03:46,330 --> 00:03:48,800
 anyway unless I wanted to go on

50
00:03:48,800 --> 00:03:51,400
 about how to cultivate loving kindness.

51
00:03:51,400 --> 00:03:55,160
 I do have videos that talk a little bit about it, but then

52
00:03:55,160 --> 00:03:57,120
 I have that kids video if you

53
00:03:57,120 --> 00:03:59,560
 ever want to learn about how to send love.

54
00:03:59,560 --> 00:04:02,720
 But I'm going to go for tonight.

55
00:04:02,720 --> 00:04:04,920
 So thanks for tuning in.

56
00:04:04,920 --> 00:04:06,760
 Thanks for showing up to meditate.

57
00:04:06,760 --> 00:04:09,760
 Wishing you all good practice.

58
00:04:09,760 --> 00:04:10,760
 Thank you.

