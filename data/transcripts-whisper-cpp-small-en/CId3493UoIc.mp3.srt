1
00:00:00,000 --> 00:00:03,120
 Is there any point in living a life other than a monk's

2
00:00:03,120 --> 00:00:07,310
 life, meditating primarily and trying to achieve nirvana or

3
00:00:07,310 --> 00:00:10,000
 enlightenment for true peace and happiness?

4
00:00:10,000 --> 00:00:16,940
 I mean there's a lot of monks who live pointless lives as

5
00:00:16,940 --> 00:00:17,000
 well, so...

6
00:00:17,000 --> 00:00:24,000
 The point is not to be a monk, but the parenthesis part,

7
00:00:24,000 --> 00:00:26,310
 meditating primarily and trying to achieve nirvana or

8
00:00:26,310 --> 00:00:29,000
 enlightenment for true peace and happiness.

9
00:00:29,000 --> 00:00:32,270
 My teacher said it just comes down to whether you want more

10
00:00:32,270 --> 00:00:34,000
 suffering or less suffering.

11
00:00:34,000 --> 00:00:36,700
 You know, we're all... that's the... what you're saying is

12
00:00:36,700 --> 00:00:39,230
 correct, but some people still want more suffering and some

13
00:00:39,230 --> 00:00:41,840
 people want less suffering so they go quicker or slower

14
00:00:41,840 --> 00:00:43,000
 towards that.

15
00:00:43,000 --> 00:00:56,000
 Is there any point besides meditating?

16
00:00:57,000 --> 00:01:02,040
 No, not really. I mean, no, because the only point that we

17
00:01:02,040 --> 00:01:05,000
 see in Buddhism is nirvana, nirvana.

18
00:01:05,000 --> 00:01:08,660
 So if it's not something we... I mean the other thing is it

19
00:01:08,660 --> 00:01:11,000
's not just about meditating, right?

20
00:01:11,000 --> 00:01:15,110
 There are many other ways you can cultivate wholesome

21
00:01:15,110 --> 00:01:20,450
 states. You can be charitable, you can be moral, you can

22
00:01:20,450 --> 00:01:23,000
 study the Dhamma, listen to the Dhamma.

23
00:01:24,000 --> 00:01:27,510
 These are all things that help to create wholesome states

24
00:01:27,510 --> 00:01:31,550
 of mind and will help to bring your mind together and lead

25
00:01:31,550 --> 00:01:33,000
 you closer to the goal.

