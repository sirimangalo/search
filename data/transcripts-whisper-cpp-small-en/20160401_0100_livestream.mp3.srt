1
00:00:00,000 --> 00:00:10,000
 [silence]

2
00:00:10,000 --> 00:00:20,000
 [silence]

3
00:00:20,000 --> 00:00:30,000
 [silence]

4
00:00:30,000 --> 00:00:40,000
 [silence]

5
00:00:40,000 --> 00:00:50,000
 [silence]

6
00:00:50,000 --> 00:01:00,000
 [silence]

7
00:01:00,000 --> 00:01:03,000
 Good evening everyone.

8
00:01:03,000 --> 00:01:24,000
 Casting live March 31st.

9
00:01:24,000 --> 00:01:35,000
 Today's quote from the Agutarani Gaya.

10
00:01:35,000 --> 00:01:56,000
 Luminous or radiant is this mind.

11
00:01:56,000 --> 00:02:05,890
 It is defiled by visiting defilements or incoming defile

12
00:02:05,890 --> 00:02:07,000
ments

13
00:02:07,000 --> 00:02:09,000
 that come from without.

14
00:02:09,000 --> 00:02:15,000
 It's a good explanation.

15
00:02:15,000 --> 00:02:19,000
 A gantuka is that which comes.

16
00:02:19,000 --> 00:02:22,000
 A gantuka is often used to talk about a guest.

17
00:02:22,000 --> 00:02:33,000
 Monks are called a gantuka when they come.

18
00:02:33,000 --> 00:02:37,000
 I'm not quite sure actually.

19
00:02:37,000 --> 00:03:01,320
 [

20
00:03:01,320 --> 00:03:09,320
 Those who have never looked at their minds,

21
00:03:09,320 --> 00:03:13,320
 never thought about spiritual teachings,

22
00:03:13,320 --> 00:03:19,320
 maybe not even spiritual teachings so much as those who

23
00:03:19,320 --> 00:03:22,320
 never thought

24
00:03:22,320 --> 00:03:26,320
 about the things that the Buddha taught.

25
00:03:26,320 --> 00:03:31,320
 Never looked at their own minds or learned anything, or

26
00:03:31,320 --> 00:03:31,320
 spent their time in

27
00:03:31,320 --> 00:03:33,320
 the world of worldly affairs.

28
00:03:33,320 --> 00:03:41,320
 Someone who has not heard the truth.

29
00:03:41,320 --> 00:03:52,320
 They don't understand this as it is.

30
00:03:52,320 --> 00:04:00,780
 So people think that the mind is, or that their person is

31
00:04:00,780 --> 00:04:02,320
 static.

32
00:04:02,320 --> 00:04:07,710
 We think of our characters, characteristics, our character

33
00:04:07,710 --> 00:04:11,320
 types as fixed.

34
00:04:11,320 --> 00:04:15,320
 So say I'm an angry person or I get angry at this.

35
00:04:15,320 --> 00:04:19,140
 Someone told me just today, really nice person, but she

36
00:04:19,140 --> 00:04:20,320
 said I get.

37
00:04:20,320 --> 00:04:27,320
 I get ticked off at people's crap or something like that.

38
00:04:27,320 --> 00:04:31,810
 It was in the context of a larger statement, but she

39
00:04:31,810 --> 00:04:36,320
 mentioned that in passing.

40
00:04:36,320 --> 00:04:39,320
 She can get frustrated at people.

41
00:04:39,320 --> 00:04:41,320
 We say things like this.

42
00:04:41,320 --> 00:04:44,320
 This is how we talk and this is how we think.

43
00:04:44,320 --> 00:04:50,320
 We think of ourselves as having a certain character type.

44
00:04:50,320 --> 00:04:56,320
 We think of the mind as being inherently idiosyncratic.

45
00:04:56,320 --> 00:04:59,320
 Buddha said no, the mind is luminous.

46
00:04:59,320 --> 00:05:04,320
 The mind is pure basically.

47
00:05:04,320 --> 00:05:09,100
 Funny enough, there are groups that misinterpret this text

48
00:05:09,100 --> 00:05:09,320
 to mean that the

49
00:05:09,320 --> 00:05:14,320
 mind is like a glowing ball. The Dhammakaya movement in

50
00:05:14,320 --> 00:05:14,320
 Thailand,

51
00:05:14,320 --> 00:05:18,620
 I heard the Vai Sabat, I sat with him for five hours while

52
00:05:18,620 --> 00:05:20,320
 he explained to a small group of us

53
00:05:20,320 --> 00:05:24,320
 about what this passage means.

54
00:05:24,320 --> 00:05:28,800
 It's that the mind is a bright light, which is coinc

55
00:05:28,800 --> 00:05:30,320
identally the meditation object

56
00:05:30,320 --> 00:05:35,320
 that they use. They imagine a bright light, a crystal.

57
00:05:35,320 --> 00:05:39,930
 The mind is luminous. So they take it to be a physical

58
00:05:39,930 --> 00:05:42,320
 light that you can see.

59
00:05:42,320 --> 00:05:42,320
 Das

60
00:05:42,320 --> 00:05:45,320
 Dhammata is meant here.

61
00:05:45,320 --> 00:05:55,320
 It is related to kilesa, upakilesa, defilement.

62
00:05:55,320 --> 00:06:00,320
 So that's the first. This is the Ekkanthi Padma.

63
00:06:00,320 --> 00:06:07,320
 Each sutra, you could say, is just a couple of sentences.

64
00:06:07,320 --> 00:06:11,530
 So that first part is the first sutra. It says Padthamam,

65
00:06:11,530 --> 00:06:12,320
 the first.

66
00:06:12,320 --> 00:06:15,320
 This is the first sutra, just that part.

67
00:06:15,320 --> 00:06:22,320
 And then the second sutra is just an inversion of it.

68
00:06:22,320 --> 00:06:32,180
 So he says, Suthavattho Adyasabha kasad jita bhavana ati ud

69
00:06:32,180 --> 00:06:32,320
hami.

70
00:06:32,320 --> 00:06:37,320
 Oh wait, sorry. I didn't finish the first one.

71
00:06:37,320 --> 00:06:41,480
 So an uninstructured ordinary worldling doesn't understand

72
00:06:41,480 --> 00:06:42,320
 this as it is.

73
00:06:42,320 --> 00:06:47,320
 Tasma asuthavattho putudjana sa jita bhavana nati.

74
00:06:47,320 --> 00:06:53,070
 Therefore, for an uninstructured worldling, jita bhavana n

75
00:06:53,070 --> 00:06:59,320
ati, there is no cultivation of the mind.

76
00:06:59,320 --> 00:07:04,280
 They do not cultivate their mindset. That's not a literal

77
00:07:04,280 --> 00:07:05,320
 translation.

78
00:07:05,320 --> 00:07:08,320
 A literal translation is, there is no, and it's in quotes,

79
00:07:08,320 --> 00:07:11,320
 therefore,

80
00:07:11,320 --> 00:07:17,410
 I say, vadami, tasma vadami, therefore I say, asuthavattho,

81
00:07:17,410 --> 00:07:20,320
 for an uninstructured putudjana sa, worldling,

82
00:07:20,320 --> 00:07:27,970
 jita bhavana, the training of the mind, nati, doesn't exist

83
00:07:27,970 --> 00:07:28,320
.

84
00:07:28,320 --> 00:07:31,320
 You all know the word nati means, right?

85
00:07:31,320 --> 00:07:36,320
 Nati is a famous, there's a story behind the word, right?

86
00:07:36,320 --> 00:07:42,840
 Nati jita bhavana, there is no jita bhavana, there is no bh

87
00:07:42,840 --> 00:07:45,320
avana for the jita.

88
00:07:45,320 --> 00:07:47,320
 And then the second one is the inversion.

89
00:07:47,320 --> 00:07:52,320
 So, sutang sutva arihasava kovya tabhutam bhajanati.

90
00:07:52,320 --> 00:07:59,160
 That instructed student of the enlightened ones understands

91
00:07:59,160 --> 00:08:02,320
 it as it is, jita bhutam bhajanati.

92
00:08:02,320 --> 00:08:10,540
 Tasma, therefore, or so, from that, literally, sutavattho a

93
00:08:10,540 --> 00:08:14,320
rihasava kasa, jita bhavana ati.

94
00:08:14,320 --> 00:08:22,590
 For a instructed student of the enlightened ones, there is

95
00:08:22,590 --> 00:08:27,320
 ati, there is jita bhavana,

96
00:08:27,320 --> 00:08:32,060
 the cultivation of the mind, or the development of the mind

97
00:08:32,060 --> 00:08:40,320
, mental development, kita bhavana.

98
00:08:40,320 --> 00:08:43,320
 That's said, and it says duttiyam, which means the second,

99
00:08:43,320 --> 00:08:46,320
 and then it goes on, there's ten in this waga.

100
00:08:46,320 --> 00:08:51,160
 So the nguttar nikai is sorted by wagas, which are like

101
00:08:51,160 --> 00:08:53,320
 chapters, sort of.

102
00:08:53,320 --> 00:08:59,450
 They don't, they're usually chapters are, well, sometimes

103
00:08:59,450 --> 00:09:01,320
 they're sorted by subject.

104
00:09:01,320 --> 00:09:04,320
 Sometimes they're just packed in there.

105
00:09:04,320 --> 00:09:18,130
 Here, the third one is about metajita, the mind of metajita

106
00:09:18,130 --> 00:09:25,320
, achara sangha tama tampi.

107
00:09:25,320 --> 00:09:34,030
 Anyway, it's been a long day. I had an exam this morning on

108
00:09:34,030 --> 00:09:38,320
 peace, which is always a good thing to be tested on.

109
00:09:38,320 --> 00:09:44,700
 But it was actually kind of disappointing. The questions on

110
00:09:44,700 --> 00:09:49,320
 it were surprising.

111
00:09:49,320 --> 00:09:55,690
 The questions were about, well, I guess, it was just kind

112
00:09:55,690 --> 00:09:58,320
 of strange. It wasn't the things that I'd studied.

113
00:09:58,320 --> 00:10:04,430
 We were asked for meetings that had led up to UN resolution

114
00:10:04,430 --> 00:10:08,320
 70.1, 70/1.

115
00:10:08,320 --> 00:10:12,160
 I wasn't paying attention to the names of the meetings and

116
00:10:12,160 --> 00:10:13,320
 some of which.

117
00:10:13,320 --> 00:10:17,530
 I guess I'm a little bit out of my element there because I

118
00:10:17,530 --> 00:10:21,320
'm not really interested in worldly affairs.

119
00:10:21,320 --> 00:10:26,290
 I'm more interested in the content than the stuff that's

120
00:10:26,290 --> 00:10:28,320
 going on, which is a fault of mine.

121
00:10:28,320 --> 00:10:31,670
 I mean, I can never really be a peace worker in the sense

122
00:10:31,670 --> 00:10:33,320
 that they want me to be.

123
00:10:33,320 --> 00:10:37,320
 They want us to be worldly peace workers, I think.

124
00:10:37,320 --> 00:10:41,760
 Getting involved with governments and doing our civic duty,

125
00:10:41,760 --> 00:10:44,320
 I guess, and getting involved.

126
00:10:44,320 --> 00:10:50,320
 Unfortunately, I'm not a citizen. I'm out of society.

127
00:10:50,320 --> 00:10:53,660
 Then we had written an essay about, we had one of three

128
00:10:53,660 --> 00:10:57,320
 topics, and we're talking about happiness.

129
00:10:57,320 --> 00:11:00,320
 Which one did I talk about?

130
00:11:00,320 --> 00:11:11,320
 Bizarre.

131
00:11:11,320 --> 00:11:14,750
 Yeah, I think I'm just wondering if I wrote about the wrong

132
00:11:14,750 --> 00:11:15,320
 topic.

133
00:11:15,320 --> 00:11:22,490
 I wrote about UN resolution 70/1, Sustainable Development

134
00:11:22,490 --> 00:11:25,320
 Report, just because it was easy.

135
00:11:25,320 --> 00:11:30,340
 I started thinking, this resolution doesn't really hit at

136
00:11:30,340 --> 00:11:36,320
 the root of the problem, which is this quote.

137
00:11:36,320 --> 00:11:41,320
 The root of the problem is our minds are defiled.

138
00:11:41,320 --> 00:11:44,520
 There was no call for meditation. How do we solve the world

139
00:11:44,520 --> 00:11:45,320
's problems?

140
00:11:45,320 --> 00:11:49,320
 Meditation wasn't on the list.

141
00:11:49,320 --> 00:11:56,320
 So it was missing something.

142
00:11:56,320 --> 00:11:59,020
 And then I just finished my essay. Why am I talking about

143
00:11:59,020 --> 00:12:01,320
 this? Because I wanted to talk about my essay.

144
00:12:01,320 --> 00:12:04,320
 So I did this essay.

145
00:12:04,320 --> 00:12:11,240
 And I think it's somewhat interesting. It's a little too

146
00:12:11,240 --> 00:12:15,320
 academic for my tastes.

147
00:12:15,320 --> 00:12:20,110
 Meaning, I always find when I write for school, it's a

148
00:12:20,110 --> 00:12:22,320
 little bit artificial.

149
00:12:22,320 --> 00:12:27,200
 You kind of have to make it fit with the course, and then

150
00:12:27,200 --> 00:12:31,320
 don't do any courses on meditation.

151
00:12:31,320 --> 00:12:34,080
 I suppose I somehow play into their hands a little too much

152
00:12:34,080 --> 00:12:34,320
.

153
00:12:34,320 --> 00:12:38,200
 I could probably have done a, found a way to do an essay on

154
00:12:38,200 --> 00:12:43,320
 meditation, but it's East Asian Buddhism.

155
00:12:43,320 --> 00:12:46,320
 I was interested in the Lotus Sutra.

156
00:12:46,320 --> 00:12:58,890
 So I'm interested in making clear what is the Dharma and

157
00:12:58,890 --> 00:13:04,320
 what isn't the Dharma.

158
00:13:04,320 --> 00:13:10,320
 That you can't just get away with.

159
00:13:10,320 --> 00:13:13,650
 I'm putting words in the Buddha's mouth. I was really kind

160
00:13:13,650 --> 00:13:14,320
 of disgusted by the Lotus Sutra,

161
00:13:14,320 --> 00:13:20,920
 for what it blatantly appears to do to the Buddha and to

162
00:13:20,920 --> 00:13:26,320
 Sariputra, and to the Dharma in general.

163
00:13:26,320 --> 00:13:31,300
 How it just throws out the 45 years of the Buddha's

164
00:13:31,300 --> 00:13:35,320
 teaching, and just makes bizarre claims.

165
00:13:35,320 --> 00:13:39,510
 I mean, of course there were lots of texts like this, but

166
00:13:39,510 --> 00:13:41,320
 the Lotus Sutra became famous.

167
00:13:41,320 --> 00:13:46,320
 So I'm looking at why it became famous.

168
00:13:46,320 --> 00:13:53,430
 So why I'm talking about this, because I thought maybe

169
00:13:53,430 --> 00:13:55,320
 somebody out there would like to read it,

170
00:13:55,320 --> 00:14:06,320
 and hopefully look at it for me, and find typos and stuff.

171
00:14:06,320 --> 00:14:13,320
 So, I don't want to put it like that on the internet,

172
00:14:13,320 --> 00:14:16,320
 especially not when it's not finished.

173
00:14:16,320 --> 00:14:19,600
 And because it would just mean getting too many people

174
00:14:19,600 --> 00:14:21,320
 writing comments on it.

175
00:14:21,320 --> 00:14:24,650
 I've done that before, and boy did I get lots and lots of

176
00:14:24,650 --> 00:14:29,320
 comments that I had to just discard.

177
00:14:29,320 --> 00:14:32,850
 So I'm posting it here on the site if you want to go read

178
00:14:32,850 --> 00:14:33,320
 it.

179
00:14:33,320 --> 00:14:37,710
 It's fairly academic, I think, so hopefully it's not a turn

180
00:14:37,710 --> 00:14:38,320
 off.

181
00:14:38,320 --> 00:14:43,680
 But I can write, my writing is good. I know my writing is

182
00:14:43,680 --> 00:14:44,320
 good.

183
00:14:44,320 --> 00:14:48,680
 So if you're interested, if there's anyone out there who

184
00:14:48,680 --> 00:14:51,320
 would go ahead and read through it,

185
00:14:51,320 --> 00:14:56,320
 and at least give me some feedback, programmer.

186
00:14:56,320 --> 00:14:59,460
 We're supposed to do this before we hand our work in, find

187
00:14:59,460 --> 00:15:00,320
 someone who will read it.

188
00:15:00,320 --> 00:15:03,320
 So I got all you guys to read it.

189
00:15:03,320 --> 00:15:06,320
 So anyway, I post a link in case there's someone out there

190
00:15:06,320 --> 00:15:13,320
 who has the time to go through it.

191
00:15:13,320 --> 00:15:17,320
 And that's about it.

192
00:15:17,320 --> 00:15:21,710
 Not much. Yeah, so I just finished that paper tonight. It's

193
00:15:21,710 --> 00:15:23,320
 not a long paper.

194
00:15:23,320 --> 00:15:26,860
 In fact, if it seems terse and it's like, wow, why did you

195
00:15:26,860 --> 00:15:28,320
 write so little?

196
00:15:28,320 --> 00:15:32,320
 And just, it felt like I was skipping from point to point.

197
00:15:32,320 --> 00:15:36,320
 It's because we had a word limit of 2,000 words.

198
00:15:36,320 --> 00:15:42,610
 And the papers that I came out with was 2,200, and that's

199
00:15:42,610 --> 00:15:46,320
 being excruciatingly parsimonious.

200
00:15:46,320 --> 00:15:53,420
 So I could have written it four times as much probably, at

201
00:15:53,420 --> 00:15:55,320
 least twice as much, would have been pretty easy.

202
00:15:55,320 --> 00:15:58,320
 It would have been more comfortable writing twice as much,

203
00:15:58,320 --> 00:16:00,320
 but it would have been more work.

204
00:16:00,320 --> 00:16:05,320
 So I'm kind of thankful that it was only short.

205
00:16:05,320 --> 00:16:12,430
 And he really likes, this professor is really big on conc

206
00:16:12,430 --> 00:16:16,320
ision.

207
00:16:16,320 --> 00:16:21,350
 Because I guess a lot of people just end up with word salad

208
00:16:21,350 --> 00:16:24,320
, and they just,

209
00:16:24,320 --> 00:16:26,760
 they write an introduction that's totally unrelated to

210
00:16:26,760 --> 00:16:30,320
 their paper, and they just blather on about this and that.

211
00:16:30,320 --> 00:16:38,710
 So making it so short forces you to be on track, because

212
00:16:38,710 --> 00:16:40,320
 you have to get through your whole argument.

213
00:16:40,320 --> 00:16:44,140
 So anyway, you don't want to hear this. This is not related

214
00:16:44,140 --> 00:16:44,320
.

215
00:16:44,320 --> 00:16:49,320
 But tonight's verse, getting back to the verse was awesome.

216
00:16:49,320 --> 00:16:54,150
 It's an awesome little verse, and very much related to our

217
00:16:54,150 --> 00:16:56,320
 meditation practice.

218
00:16:56,320 --> 00:17:03,260
 We practice to not change the mind in terms of cultivating

219
00:17:03,260 --> 00:17:06,320
 a new I, a new me, that's Buddhist.

220
00:17:06,320 --> 00:17:11,580
 But in terms of just coming back to a natural state, it's

221
00:17:11,580 --> 00:17:15,320
 very important in Buddhism, in meditation,

222
00:17:15,320 --> 00:17:21,070
 that we're just trying to sort everything out, untie all

223
00:17:21,070 --> 00:17:24,320
 the knots, which is a bigger task than it may sound.

224
00:17:24,320 --> 00:17:26,580
 I mean, it's not like you're just going to go back to the

225
00:17:26,580 --> 00:17:30,320
 ordinary person you were before, or you generally are.

226
00:17:30,320 --> 00:17:34,300
 It is a profound change, but it's only a profound change

227
00:17:34,300 --> 00:17:35,320
 because,

228
00:17:35,320 --> 00:17:38,680
 because really everything that makes us human is an art

229
00:17:38,680 --> 00:17:39,320
ifice.

230
00:17:39,320 --> 00:17:44,320
 The whole being a human is not natural.

231
00:17:44,320 --> 00:17:47,710
 And we get back into this idea of what's natural, but it's

232
00:17:47,710 --> 00:17:50,060
 not natural in the sense that we've made it, we've

233
00:17:50,060 --> 00:17:52,320
 cultivated it.

234
00:17:52,320 --> 00:17:56,320
 We've cultivated it arbitrarily.

235
00:17:56,320 --> 00:17:59,300
 There's nothing particularly, it's not like you look at the

236
00:17:59,300 --> 00:18:01,320
 universe and say, "Oh, that needs humans."

237
00:18:01,320 --> 00:18:05,360
 The perfect representation of the universe, of being in the

238
00:18:05,360 --> 00:18:07,320
 universe, a human being.

239
00:18:07,320 --> 00:18:10,800
 No, I mean, that's what Christianity, Judaism, theistic

240
00:18:10,800 --> 00:18:12,320
 religions tried to do.

241
00:18:12,320 --> 00:18:15,140
 They said, "Well, why are humans here?" "Well, we must be

242
00:18:15,140 --> 00:18:16,320
 the perfect..."

243
00:18:16,320 --> 00:18:21,320
 Somehow they got to that. It's not true.

244
00:18:21,320 --> 00:18:31,320
 Anyone who believes that is grossly deluding themselves.

245
00:18:31,320 --> 00:18:37,320
 But there is something luminous and pure in us all,

246
00:18:37,320 --> 00:18:41,320
 underneath all that garbage, no matter who you are,

247
00:18:41,320 --> 00:18:46,400
 no matter what you've done. Even Devadatta in the end, got

248
00:18:46,400 --> 00:18:50,320
 on the right track.

249
00:18:50,320 --> 00:18:54,320
 Anyway, that's all for tonight.

250
00:18:54,320 --> 00:18:58,440
 Take a rest tomorrow. I'm going to spend some of the day

251
00:18:58,440 --> 00:19:00,320
 going over any comments that people have on my paper,

252
00:19:00,320 --> 00:19:04,840
 if anyone does look at it, and going over it, and then I

253
00:19:04,840 --> 00:19:07,320
 got to submit it before the end of tomorrow.

254
00:19:07,320 --> 00:19:11,320
 And then I'm done. I've got a couple exams, but...

255
00:19:11,320 --> 00:19:14,320
 Oh, and the symposium next week is the Peace Symposium.

256
00:19:14,320 --> 00:19:19,600
 So yeah, this week I'm going to be spending on organizing

257
00:19:19,600 --> 00:19:21,320
 the symposium.

258
00:19:21,320 --> 00:19:25,320
 That's all. Have a good night.

259
00:19:25,320 --> 00:19:29,320
 Oh, yeah, have a good night. No questions tonight.

260
00:19:29,320 --> 00:19:32,560
 Not that anyone ever has them, but if you happen to have

261
00:19:32,560 --> 00:19:36,320
 questions, come back tomorrow.

262
00:19:36,320 --> 00:19:39,320
 Good night.

263
00:19:40,320 --> 00:19:43,320
 Thank you.

264
00:19:44,320 --> 00:19:47,320
 Thank you.

