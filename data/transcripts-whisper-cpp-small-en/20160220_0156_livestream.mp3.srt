1
00:00:00,000 --> 00:00:19,880
 Good evening everyone.

2
00:00:19,880 --> 00:00:40,040
 Tonight's quote is about wealth.

3
00:00:40,040 --> 00:00:49,440
 Many of you probably are aware Buddhism is not

4
00:00:49,440 --> 00:00:52,200
 a wealth-oriented religion.

5
00:00:52,200 --> 00:00:58,960
 It doesn't place much importance on wealth.

6
00:00:58,960 --> 00:01:06,640
 I can't think of any religion that does.

7
00:01:06,640 --> 00:01:15,920
 I think Buddhism especially has some very fairly pointed

8
00:01:15,920 --> 00:01:17,760
 things to say.

9
00:01:17,760 --> 00:01:22,480
 Or at least has a reputation of probably being anti-wealth.

10
00:01:22,480 --> 00:01:33,080
 I think, I mean, monks are supposed to be monks who are the

11
00:01:33,080 --> 00:01:34,720
 spokespeople of the religion,

12
00:01:34,720 --> 00:01:36,840
 are supposed to live in poverty.

13
00:01:36,840 --> 00:01:46,430
 They aren't allowed to touch money or valuable objects like

14
00:01:46,430 --> 00:01:49,560
 gold and jewels.

15
00:01:49,560 --> 00:02:03,900
 They have to wear these robes, they have to eat the food

16
00:02:03,900 --> 00:02:08,000
 that were given.

17
00:02:08,000 --> 00:02:12,960
 The poverty is a big part of Buddhism.

18
00:02:12,960 --> 00:02:20,160
 Buddhism himself said, "Santuti paramang dhan nam."

19
00:02:20,160 --> 00:02:24,000
 Contentment is the greatest wealth.

20
00:02:24,000 --> 00:02:30,080
 That was amazing.

21
00:02:30,080 --> 00:02:38,960
 Oh, I had my timer set, right?

22
00:02:38,960 --> 00:03:08,840
 I ended early.

23
00:03:08,840 --> 00:03:19,720
 Contentment is the greatest form of wealth.

24
00:03:19,720 --> 00:03:29,320
 That's because what wealth is supposed to do is fulfill our

25
00:03:29,320 --> 00:03:38,320
 wants, our needs.

26
00:03:38,320 --> 00:03:43,340
 So ordinary wealth is able to temporarily fulfill our wants

27
00:03:43,340 --> 00:03:50,000
 and our desires and our needs, which

28
00:03:50,000 --> 00:03:59,760
 our needs anyway are, make wealth an important thing.

29
00:03:59,760 --> 00:04:02,510
 So in this quote that we have tonight, the Buddha

30
00:04:02,510 --> 00:04:04,760
 acknowledges that wealth is good for

31
00:04:04,760 --> 00:04:05,760
 you.

32
00:04:05,760 --> 00:04:06,760
 It can make you happy.

33
00:04:06,760 --> 00:04:07,760
 It can make your parents happy.

34
00:04:07,760 --> 00:04:11,110
 It can make your spouse, your children, your servants and

35
00:04:11,110 --> 00:04:12,200
 workers happy.

36
00:04:12,200 --> 00:04:16,960
 Your friends and companions happy.

37
00:04:16,960 --> 00:04:20,920
 And you can use it to give charity or religious donations.

38
00:04:20,920 --> 00:04:32,680
 That's what the quote refers to, but charity in general.

39
00:04:32,680 --> 00:04:40,850
 So obviously poverty is a problem, but the dangers of that

40
00:04:40,850 --> 00:04:45,120
 wealth is that it's temporary.

41
00:04:45,120 --> 00:04:49,400
 It disappears.

42
00:04:49,400 --> 00:04:51,920
 Wealth can be incredibly dangerous, right?

43
00:04:51,920 --> 00:04:55,280
 The more wealthy you are, the more trapped you are by it.

44
00:04:55,280 --> 00:04:56,960
 It can be.

45
00:04:56,960 --> 00:05:01,900
 In many ways, wealthy people are trapped or can be trapped

46
00:05:01,900 --> 00:05:03,480
 by their money.

47
00:05:03,480 --> 00:05:08,090
 There was an interesting story I read about people who win

48
00:05:08,090 --> 00:05:10,680
 the lottery, that apparently

49
00:05:10,680 --> 00:05:13,680
 winning the lottery is one of the worst things that can

50
00:05:13,680 --> 00:05:14,680
 happen to you.

51
00:05:14,680 --> 00:05:18,310
 If you look it up, it's quite interesting what happens to

52
00:05:18,310 --> 00:05:20,280
 people, what the statistics

53
00:05:20,280 --> 00:05:29,310
 are for people who have won the lottery, homicide, suicide,

54
00:05:29,310 --> 00:05:34,480
 a lot of robbery, you lose friends,

55
00:05:34,480 --> 00:05:42,320
 you lose family, lose your life.

56
00:05:42,320 --> 00:05:45,950
 That kind of wealth can be dangerous, but even putting that

57
00:05:45,950 --> 00:05:49,480
 aside, worldly wealth, it

58
00:05:49,480 --> 00:05:54,950
 can be dangerous, though it's necessary to maintain

59
00:05:54,950 --> 00:05:56,600
 livelihood.

60
00:05:56,600 --> 00:05:59,640
 And this is where Buddhism does acknowledge the need for

61
00:05:59,640 --> 00:06:00,200
 wealth.

62
00:06:00,200 --> 00:06:04,920
 I mean, even monks have to have bowls, they have to have

63
00:06:04,920 --> 00:06:07,960
 things, they have to have tools,

64
00:06:07,960 --> 00:06:16,520
 things that allow them to continue their life.

65
00:06:16,520 --> 00:06:22,500
 But beyond that wealth isn't actually able to provide

66
00:06:22,500 --> 00:06:26,320
 contentment, it isn't actually

67
00:06:26,320 --> 00:06:32,480
 able to satisfy, it isn't able to bring happiness.

68
00:06:32,480 --> 00:06:34,840
 So the happiness that it can bring is the ability to live

69
00:06:34,840 --> 00:06:36,200
 one's life without worry,

70
00:06:36,200 --> 00:06:42,960
 without fear, and potentially without having to go hungry,

71
00:06:42,960 --> 00:06:46,000
 without having to be cold or

72
00:06:46,000 --> 00:06:54,150
 hot, without having to suffer through lack of the amenities

73
00:06:54,150 --> 00:06:58,200
 or the necessities of life.

74
00:06:58,200 --> 00:07:02,330
 But as far as fulfilling our wants, our desires, there's no

75
00:07:02,330 --> 00:07:04,520
 wealth, no amount of wealth that

76
00:07:04,520 --> 00:07:07,560
 can do that.

77
00:07:07,560 --> 00:07:15,240
 And so in terms of our wants, this is why contentment is

78
00:07:15,240 --> 00:07:19,260
 the greatest form of wealth.

79
00:07:19,260 --> 00:07:23,880
 Because that's what we're looking for from wealth, right?

80
00:07:23,880 --> 00:07:26,560
 We understand that the problem is we want something.

81
00:07:26,560 --> 00:07:28,360
 This is an issue.

82
00:07:28,360 --> 00:07:32,000
 When you want something, it's actually a problem.

83
00:07:32,000 --> 00:07:36,500
 If you don't get it, to the extent that you want it, it's

84
00:07:36,500 --> 00:07:38,680
 going to cause suffering for

85
00:07:38,680 --> 00:07:40,280
 you until you get it.

86
00:07:40,280 --> 00:07:44,200
 If you want it a lot, it can cause great suffering.

87
00:07:44,200 --> 00:07:46,760
 If you want it a little, well, just a little suffering

88
00:07:46,760 --> 00:07:47,760
 until you get it.

89
00:07:47,760 --> 00:07:51,800
 Of course, once you get it, then you're pleased.

90
00:07:51,800 --> 00:07:55,360
 But that pleasure reinforces the desire.

91
00:07:55,360 --> 00:08:01,480
 And so you want it more, and it becomes more acute.

92
00:08:01,480 --> 00:08:06,480
 And eventually you don't get what you want and you suffer.

93
00:08:06,480 --> 00:08:13,200
 So you're constantly living your life chasing after your

94
00:08:13,200 --> 00:08:14,640
 desires.

95
00:08:14,640 --> 00:08:20,430
 Contentment on the other hand accomplishes the same thing,

96
00:08:20,430 --> 00:08:22,160
 but it does it by going the

97
00:08:22,160 --> 00:08:25,040
 other way, by destroying the wanting.

98
00:08:25,040 --> 00:08:31,430
 When you remove the wanting without reinforcing it, without

99
00:08:31,430 --> 00:08:34,720
 following it, then you weaken

100
00:08:34,720 --> 00:08:37,040
 it.

101
00:08:37,040 --> 00:08:40,000
 When you can find peace without the things you want.

102
00:08:40,000 --> 00:08:43,630
 And you can give up your wants and find happiness without

103
00:08:43,630 --> 00:08:46,320
 needing this or that to make you happy.

104
00:08:46,320 --> 00:08:51,920
 Which is a foreign concept to many of us.

105
00:08:51,920 --> 00:08:56,830
 We're so used to finding happiness in things and

106
00:08:56,830 --> 00:08:58,640
 experiences.

107
00:08:58,640 --> 00:09:04,030
 It becomes kind of religious or a view that we have, our

108
00:09:04,030 --> 00:09:05,200
 belief.

109
00:09:05,200 --> 00:09:11,540
 And if you challenge that, there's a tendency to react

110
00:09:11,540 --> 00:09:14,120
 quite negatively.

111
00:09:14,120 --> 00:09:24,000
 We will get upset, turned off by the things you're saying.

112
00:09:24,000 --> 00:09:28,320
 Find happiness by letting go, by not clinging, by giving up

113
00:09:28,320 --> 00:09:29,560
 your desires.

114
00:09:29,560 --> 00:09:31,840
 Very few people will be interested.

115
00:09:31,840 --> 00:09:34,770
 As I've talked about recently, most of us don't even

116
00:09:34,770 --> 00:09:36,600
 understand that statement.

117
00:09:36,600 --> 00:09:39,000
 We can't even comprehend it.

118
00:09:39,000 --> 00:09:44,800
 We're so fixated on our desires.

119
00:09:44,800 --> 00:09:47,610
 It's hard to even comprehend the idea of being happy

120
00:09:47,610 --> 00:09:48,720
 without...

121
00:09:48,720 --> 00:09:52,150
 Now maybe that's not giving people enough credit, but

122
00:09:52,150 --> 00:09:54,200
 especially all of you here, anyone

123
00:09:54,200 --> 00:09:59,180
 who's watching this, has an understanding, some

124
00:09:59,180 --> 00:10:02,480
 understanding of the problem.

125
00:10:02,480 --> 00:10:05,760
 They've had suffering in their lives.

126
00:10:05,760 --> 00:10:10,970
 And so they can see how our wants, when we want things to

127
00:10:10,970 --> 00:10:13,160
 be a certain way, when we don't

128
00:10:13,160 --> 00:10:16,840
 get that, it's quite suffering, quite a bit of suffering.

129
00:10:16,840 --> 00:10:22,720
 Great suffering.

130
00:10:22,720 --> 00:10:26,360
 And so we turn to meditation, thinking, to find contentment

131
00:10:26,360 --> 00:10:26,640
.

132
00:10:26,640 --> 00:10:28,640
 This is really what it is.

133
00:10:28,640 --> 00:10:32,800
 If this hasn't come to you, well here's an idea, a reason

134
00:10:32,800 --> 00:10:35,720
 why meditation might be interesting.

135
00:10:35,720 --> 00:10:44,960
 Is to find contentment, to be able to live without need.

136
00:10:44,960 --> 00:10:46,560
 And so how does it do this?

137
00:10:46,560 --> 00:10:49,760
 Meditation teaches us objectivity.

138
00:10:49,760 --> 00:10:52,600
 It's like straightening the mind.

139
00:10:52,600 --> 00:10:57,390
 The mind is bent and crooked all out of shape, so you have

140
00:10:57,390 --> 00:11:00,160
 to strike it and straighten it.

141
00:11:00,160 --> 00:11:02,720
 And every moment that you see things as they are, you're

142
00:11:02,720 --> 00:11:04,120
 straightening the mind.

143
00:11:04,120 --> 00:11:06,720
 Not just every moment you're meditating, that's not the

144
00:11:06,720 --> 00:11:07,160
 case.

145
00:11:07,160 --> 00:11:09,970
 But while you're meditating, when you find a moment where

146
00:11:09,970 --> 00:11:11,360
 you just see something as it

147
00:11:11,360 --> 00:11:15,240
 is, when you say rising, and you're just aware of the

148
00:11:15,240 --> 00:11:18,800
 rising, that moment is a pure state.

149
00:11:18,800 --> 00:11:22,400
 And it's straight.

150
00:11:22,400 --> 00:11:29,880
 The mind is straight, is wholesome, is pure.

151
00:11:29,880 --> 00:11:36,520
 When you cultivate this, the cultivation of these states,

152
00:11:36,520 --> 00:11:43,120
 this is the path to contentment.

153
00:11:43,120 --> 00:11:46,330
 Because in that time, in that moment, there's no need for

154
00:11:46,330 --> 00:11:48,160
 anything, there's no want for

155
00:11:48,160 --> 00:11:49,160
 anything.

156
00:11:49,160 --> 00:11:53,680
 When you cultivate these moments after moments consecut

157
00:11:53,680 --> 00:11:56,760
ively, you slowly become content.

158
00:11:56,760 --> 00:12:00,780
 Now you're dealing with so many likes and dislikes, so it's

159
00:12:00,780 --> 00:12:02,520
 not easy and it's not very

160
00:12:02,520 --> 00:12:03,800
 comfortable.

161
00:12:03,800 --> 00:12:05,600
 It's mostly not content.

162
00:12:05,600 --> 00:12:10,040
 But in those moments, you can see the contentment.

163
00:12:10,040 --> 00:12:12,540
 That's what we're fighting for, that's what we're working

164
00:12:12,540 --> 00:12:12,880
 for.

165
00:12:12,880 --> 00:12:17,360
 It takes time, but through practice, this is what you

166
00:12:17,360 --> 00:12:18,640
 cultivate.

167
00:12:18,640 --> 00:12:28,800
 This is the direction that you steer yourself towards.

168
00:12:28,800 --> 00:12:31,310
 So I don't want to talk too much about wealth, I don't like

169
00:12:31,310 --> 00:12:33,000
 talking about money and so on,

170
00:12:33,000 --> 00:12:41,600
 but contentment, that's where it's at.

171
00:12:41,600 --> 00:12:47,200
 So there's your Dhamma bit for tonight.

172
00:12:47,200 --> 00:12:50,660
 Open up to hang out if anybody wants to.

173
00:12:50,660 --> 00:12:54,080
 Come on and ask questions, if you don't have any questions.

174
00:12:54,080 --> 00:12:58,400
 Just say goodnight.

175
00:12:58,400 --> 00:13:03,520
 I met a couple of Sri Lankan people on the street today and

176
00:13:03,520 --> 00:13:06,280
 they're coming tomorrow morning

177
00:13:06,280 --> 00:13:07,280
 to visit.

178
00:13:07,280 --> 00:13:10,280
 It's nice, people in the community.

179
00:13:10,280 --> 00:13:14,560
 There actually are people in this area.

180
00:13:14,560 --> 00:13:17,710
 There's a Sri Lankan monk or Bangladeshi monk in Toronto

181
00:13:17,710 --> 00:13:19,440
 who said he was going to get me

182
00:13:19,440 --> 00:13:24,800
 in touch with the Sri Lankan community here and he never

183
00:13:24,800 --> 00:13:25,560
 did.

184
00:13:25,560 --> 00:13:27,120
 He was very busy though.

185
00:13:27,120 --> 00:13:32,410
 Apparently there's quite a few Sri Lankan people in

186
00:13:32,410 --> 00:13:33,840
 Hamilton.

187
00:13:33,840 --> 00:13:38,840
 There's some Western people coming out to meditate, to

188
00:13:38,840 --> 00:13:39,760
 visit.

189
00:13:39,760 --> 00:13:42,520
 So this place could become a real part of the community.

190
00:13:42,520 --> 00:13:46,960
 Let's see how it goes.

191
00:13:46,960 --> 00:13:51,210
 I gave a meditation booklet to the woman who serves me at

192
00:13:51,210 --> 00:13:52,480
 Tim Hortons.

193
00:13:52,480 --> 00:13:54,040
 Really good group of people at Tim Hortons.

194
00:13:54,040 --> 00:13:55,040
 It's funny.

195
00:13:55,040 --> 00:13:56,840
 I've gotten to know them.

196
00:13:56,840 --> 00:14:02,690
 Rob, when you're echoing me, do you have something else on

197
00:14:02,690 --> 00:14:07,720
 like YouTube or the audio stream?

198
00:14:07,720 --> 00:14:10,920
 Better now.

199
00:14:10,920 --> 00:14:13,960
 Hey Robyn.

200
00:14:13,960 --> 00:14:20,160
 She doesn't hear me I don't think.

201
00:14:20,160 --> 00:14:21,160
 Hello.

202
00:14:21,160 --> 00:14:22,160
 Hello.

203
00:14:22,160 --> 00:14:23,160
 Hi.

204
00:14:23,160 --> 00:14:25,160
 Sorry, it's been so long.

205
00:14:25,160 --> 00:14:31,160
 I forgot that I have to turn the other one off.

206
00:14:31,160 --> 00:14:32,160
 Sorry about that.

207
00:14:32,160 --> 00:14:33,160
 So I had a question.

208
00:14:33,160 --> 00:14:34,160
 Okay.

209
00:14:34,160 --> 00:14:37,640
 So we have a teaching that we hear all the time.

210
00:14:37,640 --> 00:14:39,640
 We heard it pretty recently.

211
00:14:39,640 --> 00:14:44,620
 Let's see, only be seeing, let hearing only be hearing, let

212
00:14:44,620 --> 00:14:46,800
 smelling only be smelling.

213
00:14:46,800 --> 00:14:48,800
 And I mean it sounds good.

214
00:14:48,800 --> 00:14:50,800
 But my mind, yeah.

215
00:14:50,800 --> 00:14:52,800
 I forgot that I had to turn the other one off.

216
00:14:52,800 --> 00:14:54,800
 Sorry about that.

217
00:14:54,800 --> 00:14:56,800
 I had a question.

218
00:14:56,800 --> 00:14:57,800
 Okay, Tim, you're echoing us.

219
00:14:57,800 --> 00:14:59,740
 Please, you have to, if you're going to come on the hangout

220
00:14:59,740 --> 00:15:00,720
, you have to turn the other

221
00:15:00,720 --> 00:15:02,720
 thing off first.

222
00:15:02,720 --> 00:15:07,720
 Let's see, only be seeing.

223
00:15:07,720 --> 00:15:09,720
 Hello?

224
00:15:09,720 --> 00:15:11,720
 Okay.

225
00:15:11,720 --> 00:15:12,720
 Okay.

226
00:15:12,720 --> 00:15:16,720
 So with this teaching, I mean it sounds good.

227
00:15:16,720 --> 00:15:21,650
 But my common sense tells me that we have to judge what

228
00:15:21,650 --> 00:15:24,560
 comes in contact with our senses

229
00:15:24,560 --> 00:15:25,560
 to be safe.

230
00:15:25,560 --> 00:15:30,080
 If you're truly just tasting, tasting but not judging the

231
00:15:30,080 --> 00:15:32,120
 taste, you could be eating

232
00:15:32,120 --> 00:15:39,960
 rancid food. If you're smelling but not recognizing the

233
00:15:39,960 --> 00:15:40,760
 smell, your house could be burning.

234
00:15:40,760 --> 00:15:45,310
 I mean, how does this teaching work in the real world where

235
00:15:45,310 --> 00:15:47,360
 we do have to evaluate our

236
00:15:47,360 --> 00:15:49,360
 surroundings?

237
00:15:49,360 --> 00:15:55,840
 Yeah, I mean, it's not the whole truth.

238
00:15:55,840 --> 00:15:59,710
 It's not like a way of living your life, but it's the

239
00:15:59,710 --> 00:16:02,320
 necessary activity that you need

240
00:16:02,320 --> 00:16:07,200
 to perform during a meditation practice.

241
00:16:07,200 --> 00:16:09,140
 Because if you don't get to the point where seeing is just

242
00:16:09,140 --> 00:16:12,680
 seeing, you'll never get to

243
00:16:12,680 --> 00:16:14,240
 the root.

244
00:16:14,240 --> 00:16:18,390
 But I mean, even an arahand has to think and has to process

245
00:16:18,390 --> 00:16:20,840
, has to talk, has to interact

246
00:16:20,840 --> 00:16:25,200
 with people, has to judge.

247
00:16:25,200 --> 00:16:26,640
 Seeing is not just seeing.

248
00:16:26,640 --> 00:16:32,160
 Seeing is a pit in front of you that you have to go around.

249
00:16:32,160 --> 00:16:34,520
 But you don't have that in mind.

250
00:16:34,520 --> 00:16:39,160
 I mean, it's still seeing.

251
00:16:39,160 --> 00:16:42,400
 You see the pit and you say seeing, seeing.

252
00:16:42,400 --> 00:16:46,690
 And then there's still the thoughts that arise based on the

253
00:16:46,690 --> 00:16:47,320
 pit.

254
00:16:47,320 --> 00:16:48,920
 They arise anyway.

255
00:16:48,920 --> 00:16:56,480
 The point is your mind is straight.

256
00:16:56,480 --> 00:17:02,040
 So those thoughts are free from, "Oh my gosh, it's a pit."

257
00:17:02,040 --> 00:17:06,190
 They can still arise and they still will arise, the

258
00:17:06,190 --> 00:17:07,400
 awareness.

259
00:17:07,400 --> 00:17:11,920
 So it's only part of the picture, right?

260
00:17:11,920 --> 00:17:13,400
 Those thoughts of, "Oh, that's a pit."

261
00:17:13,400 --> 00:17:15,400
 Well, hey, wait, you're not seeing, seeing.

262
00:17:15,400 --> 00:17:17,980
 But when you said seeing is just seeing, when you said

263
00:17:17,980 --> 00:17:19,840
 seeing, seeing, it neutralized the

264
00:17:19,840 --> 00:17:20,840
 experience.

265
00:17:20,840 --> 00:17:24,610
 At least for a moment, you could still get react, but the

266
00:17:24,610 --> 00:17:26,440
 first moment of seeing the

267
00:17:26,440 --> 00:17:32,110
 pit or the tiger or whatever has neutralized the fear and

268
00:17:32,110 --> 00:17:34,400
 the shock and so on.

269
00:17:34,400 --> 00:17:39,390
 I mean, that being said, to some extent, there is a sense

270
00:17:39,390 --> 00:17:41,960
 of having to stay neutral.

271
00:17:41,960 --> 00:17:44,090
 Like of course, if it's rancid food, you know that it's

272
00:17:44,090 --> 00:17:44,680
 rancid food.

273
00:17:44,680 --> 00:17:47,120
 You have no reason to eat it.

274
00:17:47,120 --> 00:17:52,330
 But if it's a tiger and the tiger is going to eat you, well

275
00:17:52,330 --> 00:17:53,680
, you run.

276
00:17:53,680 --> 00:18:00,050
 Once it catches you, seeing, let seeing just be seeing, and

277
00:18:00,050 --> 00:18:02,200
 pain just be pain.

278
00:18:02,200 --> 00:18:06,360
 So you kind of have to go in and out of this?

279
00:18:06,360 --> 00:18:08,760
 Would that be correct?

280
00:18:08,760 --> 00:18:13,920
 Not even.

281
00:18:13,920 --> 00:18:16,290
 It's arguable that, yeah, you'd have to go a little, but it

282
00:18:16,290 --> 00:18:18,680
's not really out of it.

283
00:18:18,680 --> 00:18:19,680
 Because your mind is pure.

284
00:18:19,680 --> 00:18:24,160
 It's like when you walk, do walking meditation, you're only

285
00:18:24,160 --> 00:18:26,560
 saying, "Step right, step left,"

286
00:18:26,560 --> 00:18:28,080
 but you're aware of a lot else.

287
00:18:28,080 --> 00:18:30,080
 You're aware of the shifting of the body.

288
00:18:30,080 --> 00:18:34,120
 You may be aware of stray thoughts, but you don't have to

289
00:18:34,120 --> 00:18:36,080
 note them all as long as you

290
00:18:36,080 --> 00:18:40,700
 keep your mind straight, as long as you keep your mind pure

291
00:18:40,700 --> 00:18:41,000
.

292
00:18:41,000 --> 00:18:47,160
 And so by noting every second or so, you know, frequently,

293
00:18:47,160 --> 00:18:51,360
 your mind will say, "Stay straight."

294
00:18:51,360 --> 00:18:58,470
 So it's important to do this, but it's not everything,

295
00:18:58,470 --> 00:19:02,080
 especially of your life.

296
00:19:02,080 --> 00:19:06,200
 But that being said, when you actually do a meditation, or

297
00:19:06,200 --> 00:19:08,120
 the best way to understand

298
00:19:08,120 --> 00:19:10,730
 it, the easiest way to understand it is during a meditation

299
00:19:10,730 --> 00:19:10,960
.

300
00:19:10,960 --> 00:19:13,900
 If you want to follow it where you can follow it perfectly

301
00:19:13,900 --> 00:19:15,680
 strictly, when you're sitting

302
00:19:15,680 --> 00:19:19,950
 with your eyes closed, well, that's the time where you're

303
00:19:19,950 --> 00:19:22,360
 going to look at these things.

304
00:19:22,360 --> 00:19:26,560
 But all it takes is a moment, right?

305
00:19:26,560 --> 00:19:31,200
 Like I told you this story about this monk who was teaching

306
00:19:31,200 --> 00:19:35,120
, I was watching, and he says,

307
00:19:35,120 --> 00:19:38,120
 "Oh, I'm not going to do that."

308
00:19:38,120 --> 00:19:41,200
 He went into cessation while he was teaching.

309
00:19:41,200 --> 00:19:46,480
 It's probably one of the most impressive things I've ever

310
00:19:46,480 --> 00:19:47,280
 seen.

311
00:19:47,280 --> 00:19:52,120
 So all it was is in that moment for him hearing was just

312
00:19:52,120 --> 00:19:55,360
 hearing as he was teaching it, and

313
00:19:55,360 --> 00:20:00,240
 that's what it means.

314
00:20:00,240 --> 00:20:08,550
 My challenge is just to make it enough of a part of my day

315
00:20:08,550 --> 00:20:12,880
 to ever get to that point.

316
00:20:12,880 --> 00:20:20,270
 One of our recent meditators finished his course, did okay,

317
00:20:20,270 --> 00:20:23,920
 finished it, but was a little

318
00:20:23,920 --> 00:20:26,930
 bit disappointed, I think, with the results, even though I

319
00:20:26,930 --> 00:20:28,400
 was pretty sure he got where

320
00:20:28,400 --> 00:20:32,080
 he was supposed to be.

321
00:20:32,080 --> 00:20:33,620
 You can't tell.

322
00:20:33,620 --> 00:20:37,160
 It's hard for him to tell, it's hard for me to tell.

323
00:20:37,160 --> 00:20:46,520
 But then he emailed me, and he'd gotten stuck on a flight.

324
00:20:46,520 --> 00:20:50,290
 He was on this plane for like nine hours or something on

325
00:20:50,290 --> 00:20:51,360
 the runway.

326
00:20:51,360 --> 00:20:57,720
 It's an insane amount of time sitting on the runway.

327
00:20:57,720 --> 00:21:01,350
 What he described to me, what happened on the runway,

328
00:21:01,350 --> 00:21:03,400
 suddenly he came back and I was

329
00:21:03,400 --> 00:21:08,190
 just listening, and I said, "That sounds exactly like him

330
00:21:08,190 --> 00:21:12,200
 on the plane leaving to go home."

331
00:21:12,200 --> 00:21:13,200
 You never know when.

332
00:21:13,200 --> 00:21:14,600
 You never know when.

333
00:21:14,600 --> 00:21:16,720
 And when you're ready.

334
00:21:16,720 --> 00:21:20,450
 Sometimes you get so stressed during the course, or you get

335
00:21:20,450 --> 00:21:22,120
 stressed during the course and

336
00:21:22,120 --> 00:21:24,160
 it inhibits, it prevents.

337
00:21:24,160 --> 00:21:28,120
 From your worried or near the end it can get stressful, and

338
00:21:28,120 --> 00:21:31,920
 if you let it get to you, if

339
00:21:31,920 --> 00:21:36,240
 you're not mindful, then it just happens when you relax.

340
00:21:36,240 --> 00:21:39,280
 It's like Ananda, right?

341
00:21:39,280 --> 00:21:43,980
 Ananda was doing walking meditation all night, trying to

342
00:21:43,980 --> 00:21:47,120
 become enlightened, and nothing.

343
00:21:47,120 --> 00:21:49,690
 And then it was almost dawn, and he felt like he'd gotten

344
00:21:49,690 --> 00:21:51,520
 nowhere, and he was really stressed

345
00:21:51,520 --> 00:21:52,520
 out.

346
00:21:52,520 --> 00:21:54,630
 And then he thought, and he said, "Oh, I'm pushing too hard

347
00:21:54,630 --> 00:21:54,880
."

348
00:21:54,880 --> 00:22:01,470
 And so he laid down, and before his head touched the pillow

349
00:22:01,470 --> 00:22:03,680
 he was not right.

350
00:22:03,680 --> 00:22:06,240
 Hi, Tom.

351
00:22:06,240 --> 00:22:08,120
 Hi, Montay.

352
00:22:08,120 --> 00:22:11,640
 You have a question?

353
00:22:11,640 --> 00:22:12,640
 No.

354
00:22:12,640 --> 00:22:13,640
 Oh.

355
00:22:13,640 --> 00:22:17,760
 I'll just, I'll just show the flag.

356
00:22:17,760 --> 00:22:19,000
 Alright.

357
00:22:19,000 --> 00:22:21,520
 Well, nice to see you.

358
00:22:21,520 --> 00:22:28,320
 Good to see you.

359
00:22:28,320 --> 00:22:29,320
 Thank you, Montay.

360
00:22:29,320 --> 00:22:30,320
 Alright, welcome.

361
00:22:30,320 --> 00:22:31,320
 Have a good night, everyone.

362
00:22:31,320 --> 00:22:31,320
 Goodbye.

363
00:22:31,320 --> 00:22:33,440
 Bye.

364
00:22:33,440 --> 00:22:35,440
 Bye.

365
00:22:35,440 --> 00:22:37,440
 Bye.

366
00:22:37,440 --> 00:22:39,440
 Bye.

367
00:22:39,440 --> 00:22:41,440
 Bye.

368
00:22:41,440 --> 00:22:43,440
 Bye.

369
00:22:43,440 --> 00:22:45,440
 Bye.

