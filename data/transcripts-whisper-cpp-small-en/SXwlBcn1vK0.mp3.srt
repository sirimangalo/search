1
00:00:00,000 --> 00:00:03,000
 I'm not a Buddhist, but I find meditation to be an

2
00:00:03,000 --> 00:00:05,480
 incredibly useful skill.

3
00:00:05,480 --> 00:00:08,670
 How important do you think traditions of Buddhism are

4
00:00:08,670 --> 00:00:12,520
 outside of meditation?

5
00:00:12,520 --> 00:00:15,440
 I've sort of answered this question.

6
00:00:15,440 --> 00:00:22,280
 I don't know, in various places, probably not clearly.

7
00:00:22,280 --> 00:00:29,840
 You know, they're not intrinsically important.

8
00:00:29,840 --> 00:00:36,850
 I would categorize them as beneficial, potentially

9
00:00:36,850 --> 00:00:42,560
 beneficial, let's even say that.

10
00:00:42,560 --> 00:00:45,920
 You know, they can be potentially deleterious, if used in

11
00:00:45,920 --> 00:00:48,600
 the wrong way, if misunderstood.

12
00:00:48,600 --> 00:00:54,220
 If you believe that the rights and rituals that Buddhists

13
00:00:54,220 --> 00:00:58,040
 perform are intrinsically valuable,

14
00:00:58,040 --> 00:01:02,240
 then you start to have a problem.

15
00:01:02,240 --> 00:01:13,070
 If you think that any Buddhist practice, even giving

16
00:01:13,070 --> 00:01:16,520
 charity or even practicing meditation,

17
00:01:16,520 --> 00:01:21,150
 let's go so far, morality, meditation, if you think any of

18
00:01:21,150 --> 00:01:23,720
 these things are intrinsically

19
00:01:23,720 --> 00:01:27,060
 valuable, then you've got a problem.

20
00:01:27,060 --> 00:01:31,720
 This is, I think, not widely understood, certainly not in

21
00:01:31,720 --> 00:01:33,560
 cultural Buddhism.

22
00:01:33,560 --> 00:01:37,660
 Cultural Buddhists are generally under the misunderstanding

23
00:01:37,660 --> 00:01:40,280
 that these things are intrinsically

24
00:01:40,280 --> 00:01:41,280
 beneficial.

25
00:01:41,280 --> 00:01:43,960
 And people in general in the world are, of whatever

26
00:01:43,960 --> 00:01:45,700
 religion they belong to, or even

27
00:01:45,700 --> 00:01:50,520
 people who are not religious, tend to think that things

28
00:01:50,520 --> 00:01:53,760
 that are not intrinsically valuable

29
00:01:53,760 --> 00:01:56,320
 are intrinsically valuable.

30
00:01:56,320 --> 00:02:02,150
 There's a sutta, the saropa-masutta, I can't remember the m

31
00:02:02,150 --> 00:02:07,040
aha-saropa-masutta or the chula-saropa-masutta

32
00:02:07,040 --> 00:02:09,040
 in the Majjhima Nikaya.

33
00:02:09,040 --> 00:02:21,180
 Anyway, one of them talks about this exact issue, what is

34
00:02:21,180 --> 00:02:28,520
 and what is not truly of benefit.

35
00:02:28,520 --> 00:02:31,690
 This isn't exactly answering your question, but I think it

36
00:02:31,690 --> 00:02:33,640
's important in terms of providing

37
00:02:33,640 --> 00:02:39,530
 a context for the answer that even keeping, you might say

38
00:02:39,530 --> 00:02:42,280
 giving gifts or the idea of

39
00:02:42,280 --> 00:02:48,320
 charity, morality, concentration, tranquility, even vipass

40
00:02:48,320 --> 00:02:51,160
ana insight, the Buddha said these

41
00:02:51,160 --> 00:02:54,490
 are not the core, these are not truly, these are not the

42
00:02:54,490 --> 00:02:57,160
 heartwood, they're not the sara,

43
00:02:57,160 --> 00:03:03,840
 the true essence of Buddhism.

44
00:03:03,840 --> 00:03:08,740
 So if we want to talk about traditions, we can tentatively

45
00:03:08,740 --> 00:03:10,960
 place them, you might say

46
00:03:10,960 --> 00:03:14,540
 below the charity rung on the ladder.

47
00:03:14,540 --> 00:03:20,460
 So you have this rung below charity and that's traditional

48
00:03:20,460 --> 00:03:21,880
 practices.

49
00:03:21,880 --> 00:03:26,900
 This traditional practices or rituals can serve to be a

50
00:03:26,900 --> 00:03:29,800
 framework within which you can

51
00:03:29,800 --> 00:03:34,930
 develop charity, so you have these Buddhist holidays for

52
00:03:34,930 --> 00:03:39,400
 example, this helps develop charity,

53
00:03:39,400 --> 00:03:45,370
 the idea of charity, it also helps to develop respect and

54
00:03:45,370 --> 00:03:48,820
 humility and gratitude towards

55
00:03:48,820 --> 00:03:52,600
 the Buddha I'm thinking, it helps to develop morality, it's

56
00:03:52,600 --> 00:03:54,480
 an opportunity for people to

57
00:03:54,480 --> 00:03:58,550
 cultivate morality, to try to keep the eight precepts, if

58
00:03:58,550 --> 00:04:00,600
 they can't keep them every day

59
00:04:00,600 --> 00:04:03,970
 then they try to keep them on the Buddhist holiday, this is

60
00:04:03,970 --> 00:04:05,680
 a good thing about a Buddhist

61
00:04:05,680 --> 00:04:08,880
 holiday for example.

62
00:04:08,880 --> 00:04:12,560
 It's a good chance to calm their minds, it's a good chance

63
00:04:12,560 --> 00:04:15,040
 to develop insight and it's

64
00:04:15,040 --> 00:04:19,810
 even a good chance to develop that which is of true benefit

65
00:04:19,810 --> 00:04:21,960
, it's even possible that on

66
00:04:21,960 --> 00:04:28,200
 a Buddhist holiday it can help you to develop insight that

67
00:04:28,200 --> 00:04:31,200
 leads to enlightenment.

68
00:04:31,200 --> 00:04:39,380
 So I would place all traditions on this tentative lowest

69
00:04:39,380 --> 00:04:44,320
 rung on the beneficial ladder.

70
00:04:44,320 --> 00:04:50,060
 I just want to finish the explanation that none of these

71
00:04:50,060 --> 00:04:54,080
 things are intrinsically valuable,

72
00:04:54,080 --> 00:04:59,740
 so more so Buddhist traditions, when you say that morality

73
00:04:59,740 --> 00:05:02,360
 and none of these things are

74
00:05:02,360 --> 00:05:06,430
 actually intrinsically beneficial, they're all only for the

75
00:05:06,430 --> 00:05:08,160
 purpose of bringing out that

76
00:05:08,160 --> 00:05:13,760
 which is of true benefit which is enlightenment or nibbana.

77
00:05:13,760 --> 00:05:23,920
 The biggest problem comes when you have traditions

78
00:05:23,920 --> 00:05:27,120
 that don't do anything, that don't help you develop

79
00:05:27,120 --> 00:05:29,400
 morality or concentration or wisdom

80
00:05:29,400 --> 00:05:33,410
 or even charity, so therefore don't lead you closer to the

81
00:05:33,410 --> 00:05:35,600
 goal, some traditions can actually

82
00:05:35,600 --> 00:05:41,440
 lead people farther from the goal, traditions of horoscopes

83
00:05:41,440 --> 00:05:46,600
 and there are many useless traditions.

84
00:05:46,600 --> 00:05:50,050
 For example we have a new well that we dug and they put

85
00:05:50,050 --> 00:05:52,560
 these lamps up around the well,

86
00:05:52,560 --> 00:05:55,160
 I'm not sure why, but it's some kind of tradition and it

87
00:05:55,160 --> 00:05:57,360
 must have to do with angels and so

88
00:05:57,360 --> 00:06:00,200
 on.

89
00:06:00,200 --> 00:06:02,760
 Just an excellent example, you may be able to twist it and

90
00:06:02,760 --> 00:06:04,040
 you may actually be able to

91
00:06:04,040 --> 00:06:07,110
 find something beneficial, but I would say in many cases

92
00:06:07,110 --> 00:06:09,360
 these sorts of traditions, especially

93
00:06:09,360 --> 00:06:12,930
 when they come to be thought of as Buddhist traditions, can

94
00:06:12,930 --> 00:06:14,960
 actually be deleterious, they

95
00:06:14,960 --> 00:06:22,000
 lead to wrong views and wrong practices.

96
00:06:22,000 --> 00:06:26,390
 But even traditions that encourage the development of

97
00:06:26,390 --> 00:06:32,320
 charity, morality, concentration, even wisdom,

98
00:06:32,320 --> 00:06:39,810
 if they are reinforcing the idea that these things have

99
00:06:39,810 --> 00:06:42,320
 intrinsic value then they can

100
00:06:42,320 --> 00:06:45,680
 be somewhat problematic.

101
00:06:45,680 --> 00:06:49,850
 They're not evil, they're not something that you have to sc

102
00:06:49,850 --> 00:06:51,560
old people about, but it's

103
00:06:51,560 --> 00:06:55,190
 something that you might want to politely remind people

104
00:06:55,190 --> 00:06:56,920
 that this isn't the goal.

105
00:06:56,920 --> 00:06:59,750
 What I'm thinking of especially is charity, so people will

106
00:06:59,750 --> 00:07:01,380
 have these traditions of giving

107
00:07:01,380 --> 00:07:03,930
 charity and thinking that this is my Buddhist practices,

108
00:07:03,930 --> 00:07:05,560
 this practice of charity, and they

109
00:07:05,560 --> 00:07:08,440
 never think of what is charity for.

110
00:07:08,440 --> 00:07:11,930
 The farthest they might get is that charity leads me to go

111
00:07:11,930 --> 00:07:13,160
 to heaven and so on.

112
00:07:13,160 --> 00:07:18,040
 They never get to get closer or they don't get much closer

113
00:07:18,040 --> 00:07:20,320
 anyway to the actual goal,

114
00:07:20,320 --> 00:07:24,000
 to that which is of real benefit.

115
00:07:24,000 --> 00:07:29,480
 In short, as I said in the beginning, I don't think that

116
00:07:29,480 --> 00:07:32,680
 you could think of them as being

117
00:07:32,680 --> 00:07:37,160
 important but beneficial.

118
00:07:37,160 --> 00:07:39,920
 I think there's a difference because there are many

119
00:07:39,920 --> 00:07:41,800
 beneficial things that you could

120
00:07:41,800 --> 00:07:45,030
 do that could benefit your practice that I don't think you

121
00:07:45,030 --> 00:07:46,360
 would go so far as to say

122
00:07:46,360 --> 00:07:48,080
 they're important.

123
00:07:48,080 --> 00:07:50,720
 The only things that are truly important are morality,

124
00:07:50,720 --> 00:07:52,540
 concentration, and wisdom because

125
00:07:52,540 --> 00:07:58,240
 those are the things that lead you to release a knowledge

126
00:07:58,240 --> 00:08:00,120
 of freedom.

127
00:08:00,120 --> 00:08:04,050
 Apart from that, there are beneficial things that you can

128
00:08:04,050 --> 00:08:05,400
 engage in and you don't have

129
00:08:05,400 --> 00:08:06,400
 to engage in.

130
00:08:06,400 --> 00:08:07,400
 For some people, they're beneficial.

131
00:08:07,400 --> 00:08:08,520
 For some people, they're useless.

132
00:08:08,520 --> 00:08:11,080
 For some people, they're even deleterious.

133
00:08:11,080 --> 00:08:18,520
 For some people, traditions just get in their way.

134
00:08:18,520 --> 00:08:21,620
 For some people, it's the only opportunity they have for

135
00:08:21,620 --> 00:08:24,480
 the ordinary Sri Lankan villager.

136
00:08:24,480 --> 00:08:27,250
 It's sometimes the only excuse that they have to come and

137
00:08:27,250 --> 00:08:28,480
 practice meditation.

138
00:08:28,480 --> 00:08:30,920
 They have here a day off and they're not expected to work.

139
00:08:30,920 --> 00:08:33,040
 They're not expected to do anything.

140
00:08:33,040 --> 00:08:37,940
 Now finally, they have a chance to practice and cultivate

141
00:08:37,940 --> 00:08:39,160
 good deeds.

142
00:08:39,160 --> 00:08:43,040
 I would say there are various traditions that are like this

143
00:08:43,040 --> 00:08:45,400
 that are a reminder for people,

144
00:08:45,400 --> 00:08:50,890
 like having a schedule, having these things that when, for

145
00:08:50,890 --> 00:08:52,760
 example, before you eat to

146
00:08:52,760 --> 00:08:57,170
 offer food to the Buddha, it's a reminder for the person of

147
00:08:57,170 --> 00:08:59,200
 the Buddha's teaching and

148
00:08:59,200 --> 00:09:03,810
 of the Buddha's greatness and so on, can potentially lead

149
00:09:03,810 --> 00:09:06,520
 people closer to the practice.

150
00:09:06,520 --> 00:09:09,250
 I wouldn't say important in the sense that you're missing

151
00:09:09,250 --> 00:09:10,840
 something, but beneficial in

152
00:09:10,840 --> 00:09:16,440
 the sense that they may possibly help you or encourage you

153
00:09:16,440 --> 00:09:18,480
 in your practice.

154
00:09:18,480 --> 00:09:20,700
 Having a Buddha image, bowing down to the Buddha image once

155
00:09:20,700 --> 00:09:22,240
 in a while, these can be beneficial

156
00:09:22,240 --> 00:09:23,240
 things.

157
00:09:23,240 --> 00:09:24,240
 It can be.

158
00:09:24,240 --> 00:09:28,500
 I've talked about the Buddha image before and how I'm amb

159
00:09:28,500 --> 00:09:30,480
ivalent about it or I have

160
00:09:30,480 --> 00:09:33,740
 two different takes on the Buddha image that it can

161
00:09:33,740 --> 00:09:35,560
 actually be dangerous.

162
00:09:35,560 --> 00:09:38,080
 It can certainly be beneficial.

163
00:09:38,080 --> 00:09:41,820
 I know for me, the Buddha image was a very important

164
00:09:41,820 --> 00:09:44,280
 support when I was first learning

165
00:09:44,280 --> 00:09:48,540
 about Buddhism, when I was first practicing meditation

166
00:09:48,540 --> 00:09:52,800
 because my mind was quite in chaos.

167
00:09:52,800 --> 00:09:55,170
 Seeing the Buddha image at one point in my meditation

168
00:09:55,170 --> 00:09:56,720
 course really brought me back and

169
00:09:56,720 --> 00:09:59,440
 gave me the confidence that I needed to continue my

170
00:09:59,440 --> 00:10:00,180
 practice.

171
00:10:00,180 --> 00:10:01,180
 Was the Buddha image important?

172
00:10:01,180 --> 00:10:05,360
 Actually, I would say for me, it was quite important.

173
00:10:05,360 --> 00:10:08,310
 If I hadn't seen the Buddha image, I probably would have

174
00:10:08,310 --> 00:10:10,080
 still finished the course, but

175
00:10:10,080 --> 00:10:16,630
 it was an important, beneficial experience for me that did

176
00:10:16,630 --> 00:10:19,200
 change my practice.

177
00:10:19,200 --> 00:10:23,160
 Just psychologically gave me the support, reminded me about

178
00:10:23,160 --> 00:10:25,080
 this is not something simple

179
00:10:25,080 --> 00:10:26,080
 that I'm doing.

180
00:10:26,080 --> 00:10:27,320
 It encouraged me.

181
00:10:27,320 --> 00:10:29,890
 I gave myself up to the Buddha and it made me think, "I don

182
00:10:29,890 --> 00:10:31,280
't have to worry anymore.

183
00:10:31,280 --> 00:10:36,880
 It can be as much pain as there will be.

184
00:10:36,880 --> 00:10:38,880
 I don't have to cling to it.

185
00:10:38,880 --> 00:10:40,000
 It's not me, it's not mine.

186
00:10:40,000 --> 00:10:41,000
 It's now the Buddha's.

187
00:10:41,000 --> 00:10:44,280
 It belongs to him now."

188
00:10:44,280 --> 00:10:48,770
 I felt freed from my feelings of guilt and of all the

189
00:10:48,770 --> 00:10:51,760
 things that I had done which were

190
00:10:51,760 --> 00:10:55,600
 immoral and improper.

191
00:10:55,600 --> 00:10:56,680
 I think they can be useful.

192
00:10:56,680 --> 00:10:57,680
 Thank you.

