 Basically what we want to know is what you feel you gained
 from the practice.
 What's different now that you finished the course from your
 feeling before you started the course or before you started
 the course?
 Well, a lot of things changed actually.
 Before I came here, well actually the reason why I came
 here is because I was usually feeling anxious and it wasn't
 sort of like strong, anxious.
 I knew that there was a subtle sense of underlying anxiety
 inside of me.
 And there are bouts of feelings of anger and depression,
 you know, things that people usually feel, but I wasn't
 really okay with it.
 I realized that I wasn't going to live a life where I just
 had to cope with things constantly.
 I wanted to be not to cope with those things. I wanted to
 live and coexist peacefully with my emotions.
 I didn't want to view them as if I was against them or as
 if they were some sort of enemy.
 So, you know, I came here to meditate and one of the major
 things that changed me was that the fact that I couldn't do
 anything.
 Which is basically, I think that was the hardest thing that
 I have ever done, ironically speaking, to do nothing.
 Because at home you have all these things that you can do.
 You go to school, you go to work, you do volunteer, you
 hang out with friends.
 At home you have your computer and basically that takes
 away everything.
 And you have all these sorts of distractions.
 So when you have emotional problems, you don't really have
 the time or have the attention to really scrutinize them in
 a way, to really confront them,
 to see what they really are in their truest nature.
 And so when you come here, that's the reason why it's so
 difficult for me to do nothing.
 It's that I have all these emotions, all of these negative
 thoughts that I never thought, I never knew that they were
 so serious.
 I never knew that they were that extreme, in a sense.
 And so, you know, you spend time here and sometimes they
 come up, sometimes they go.
 But the thing you realize is that you start to adapt to
 this certain kind of environment.
 You adapt to the fact that since there is nothing you can
 do, you might as well be okay with it.
 I think that's the biggest thing that I learned here, is
 that once you have absolutely no distractions,
 nothing can take you away from what's the raw form of
 yourself.
 And you really start to confront who you are as a person.
 You start to realize there's nothing really bad about it.
 I realized I don't know why I was running away from them.
 I honestly don't know why I was trying to distract myself,
 clinging on to positive thoughts, clinging on to positive
 emotions and experiences,
 so that I don't have to experience those negative ones.
 Since I came here, I realized, in meditation, I've sat
 through some of my worst emotions and thoughts ever.
 Just feelings of shame and guilt and sadness and anger and
 extreme panic attacks.
 I wanted to just punch a wall or do something.
 Many times I wanted to run away to home and just distract
 myself again.
 But you can do that, and that's one of the best things.
 But you sit through them eventually, and you realize that I
'm still here.
 That despite the suffering that you go through, despite the
 discomfort and negativity that you experience,
 you're still here, and you take a look around you, reality
 is still here.
 Reality hasn't changed. All that has changed is what's
 inside of you.
 Inside of you, it constantly fluctuates.
 And so you learn to be mindful, and you learn to let go,
 really.
 You realize that your emotions and your thoughts are not
 really that harmful, whether they're good or not.
 They're just natural. They're like nature. They're like the
 weather, as I was thinking the other day.
 Sometimes it rains, sometimes it doesn't.
 It doesn't mean that you have to get upset every single
 time you're in a rain.
 You can just go outside and say, "Oh, it's raining."
 So what? Tomorrow it might be sunny or not? It doesn't
 matter. You just enjoy the moment.
 So that's one of the biggest things that I've ever learned.
 And it made me have this sense of fearlessness, that I
 could go into several situations that I wasn't able to
 before,
 because I was afraid of the thoughts and emotions that
 would generate if I wasn't in those experiences and
 situations.
 But now, since I've spent so many days realizing the fact
 that my emotions fluctuate,
 and the worst way to deal with them is to cling on to
 positive experiences and to avoid negative ones,
 because ultimately you can't really control them.
 You can't control your thoughts. You can't control your
 emotions.
 They're just a part of whatever is happening.
 I don't know whatever really is happening.
 Sometimes I'm moody, sometimes I'm not, sometimes I feel
 anxiety, sometimes I'm not.
 But the content of those things, it's irrelevant. That's
 what I've learned.
 You start to realize there are certain actions that you
 make.
 But the content of those things, they're just sort of an
 illusion that you create for yourself.
 Everyone has stories and histories and dramas about their
 lives, but they're not part of reality.
 That's what I've learned.
 It's like when I was saying that it fluctuates. Whatever's
 inside it fluctuates, it changes,
 but whatever's outside, it's still there.
 And you realize that. And as you become more mindful, you
 start to realize that,
 "Oh, I'm just thinking. I'm just feeling things. It doesn't
 really matter."
 "Oh, it matters because you want to gravitate towards the
 peacefulness."
 But you do so in a way that you just naturally gravitate
 towards peacefulness
 as you don't become involved in the thoughts and emotions,
 especially the contents of them and the details of them.
 And another thing that I learned here was that before I
 arrived here,
 I was thinking to myself, "Well, I'm going on a spiritual
 journey to achieve happiness, to get it."
 I'm so proud of myself for wanting to do so.
 And I came here, and that's one of the biggest hindrances
 that blocked my path for getting,
 or not getting, but just simply being with happiness, being
 with peacefulness.
 It's that I wanted to get it. I wanted to obtain it. It's
 like an item.
 It's like I have my education, I have my family, I have my
 happiness. That's how I viewed it.
 And it doesn't work out that way. It never works out that
 way.
 And happiness is not... I don't think... Once you get on
 the pursuit of happiness,
 happiness is not in your reach anymore.
 I think it's once you start to begin to be open to your
 suffering,
 when you become open to your negativity, that you're not
 afraid anymore.
 We're not trying to avoid these kinds of situations,
 and only try to get these ones, like happiness.
 It's when you embrace all sorts of experiences,
 when you realize that all experiences are just basically
 experiences, they're basically phenomena.
 That's when you experience a sense of peacefulness.
 Because when I was trying to meditate, I was always telling
 myself,
 you have to get to a state, a specific meditative state.
 You have to do that. Then you can experience happiness.
 Because then, if you don't get into meditative state, you
 don't get wisdom.
 If you don't get wisdom, you don't know what's really going
 on.
 You don't realize things that you came here, you wasted
 your time.
 And that was an obstacle. That's why I couldn't do it for
 so long.
 And it's when I realized that I can't control everything
 anymore.
 I can't dominate. I just have to be.
 That you start to realize that peacefulness just arises
 naturally.
 That you're okay. Even if you feel anxiety, even if you
 feel anger, sadness,
 there's a sense of peacefulness in terms of...
 I don't know how to describe it, but it's like you're okay.
 And you're not...
 It's also like a form of detachment from your emotions.
 You just feel it, but you're not actually there to be
 resistant of it.
 You accept it. It's like you accept a friend who's sad.
 You don't try to push her away. You just accept another
 person.
 And I realized this before, it's always easier to accept
 someone else than to accept yourself.
 So, when you become mindful, you view your emotions as
 something that's not part of yourself.
 And I think that's why it's so easy to accept them.
 It's because once they're not part of yourself, you can
 just see them for who they are.
 You can give compassion to them, embrace them, be open to
 them, whatever.
 But it's much more easier to just let go of that personal
 connection,
 but just to realize that they're just natural.
 I mean, we're all human beings. We experience emotions.
 You can't be peaceful all the time.
 And once you accept that, you can be peaceful.
 I think that's the irony behind that.
 But it took me a lot of patience, which is another thing
 that I learned.
 A lot of patience.
 In the first few days, Yu Tai Damo told me that patience is
 your best friend.
 Apparently, I was too impatient to listen to his advice.
 But over time, there's nothing you can do. You have to be
 patient.
 And to be honest, the conditions here are not the best
 conditions.
 I was living in a cave, lots of mosquitoes.
 Sounds like I'm whining, which I am, but because I was
 brought up in modern, more developed countries,
 so I was not used to it.
 But it's because of those annoyances that you start to
 adapt to situations
 that you force yourself to be more patient. Otherwise, you
 can't survive.
 I think naturally, your mind just tries to be more peaceful
.
 And in whatever situation, because that's the way that you
 have to survive.
 And so as a result, you just learn to be okay.
 You learn not to slap yourself in the face over and over
 and over again.
 Because you can see it clearly.
 And you didn't realize that before, because you never had
 the time to observe it.
 But now you have the time to observe it, to actually sit
 down and train your mind,
 sharpen your mind, and let you make your mind see things
 more clearly.
 You start to see links between thoughts and emotions,
 and between things that's going on internally.
 And you can see so clearly that when something happens,
 and when you think about something, it's literally like you
're slapping yourself in the face.
 And in times outside of meditation, you do something bad,
 like you trip on the ground, you literally slap yourself in
 the face.
 Nobody would do that. Why would you do that?
 You might get angry, but people don't know that being angry
 is like hurting yourself.
 It's hurting your own mind. It's like hurting yourself
 physically.
 But I think it's even worse, because hurting your mind, it
's your well-being.
 It's everything that is your existence.
 Everything outside of your mind is just sort of an
 environment.
 And that's how I really start to be okay with my emotions,
 is that you start to realize how to be kind to yourself.
 You start to realize how to just accept things, accept
 yourself.
 Because before coming here, I realized I was very critical
 of my thoughts, very judgmental.
 I would constantly tell myself, "Oh, you shouldn't do this.
 You shouldn't think this. You shouldn't feel this.
 This isn't good. You should feel ashamed. You should feel
 anxious.
 You need to change it. You need to make a drastic change.
 Otherwise, it's bad for you.
 Otherwise, you will lose career opportunities. You will
 lose relationship opportunities.
 You will lose things that society would grant you if your
 personality isn't like this."
 And so that trained me to think in a way that I have to be
 a certain person.
 So I would not accept parts of myself.
 But once you come here, you start to learn that that's how
 dangerous that is to you.
 That you're really unhappy living like that.
 It's really unhappy. It's an imprisonment.
 You're a prisoner to society.
 Society tells you that you need to feel like this.
 And it's basically telling you, "Don't accept yourself."
 You have to be like this. Otherwise, you can't be happy.
 In order to be happy, you have to not accept yourself,
 which is contradictive.
 And I came here and I realized whether or not I think about
 bad things,
 whether or not I feel bad emotions or positive even.
 They just come and they go. They never last forever.
 Sometimes they will feel extreme just anger or extreme
 depression or isolation or loneliness.
 And at that very moment, I will always think that they will
 last forever.
 I will always think that. I will always think, "This is the
 worst emotion. I wish it would just go away."
 I will believe that it doesn't go away. But eventually, it
 always does.
 And I think that's the best opportunity. When you do
 nothing, you realize that it does go away.
 When you do something, you don't realize that. Your
 attention is something else.
 And you're always kept in this illusion that when you have
 bad emotions, it's like a red alert.
 You have to do something. Otherwise, it will just keep
 coming in and destroy everything.
 But with patience and with mindfulness, with the
 opportunity to have no distractions
 but to fully introspect and look at yourself and confront
 yourself,
 you realize that thoughts and emotions, they come and go,
 which is impermanence.
 And sometimes I will feel extremely happy. Sometimes I feel
 like, "This is it. I found my happiness. I'm ready to go.
 I don't need to be here. I know all the answers."
 And the next day, I will feel extremely, just reach the
 other end of the emotional scale.
 I feel just hopelessness, frustration, like everything I've
 learned was lost.
 Sometimes I would think to myself, "This is too difficult
 for me. I will never reach that stage of happiness.
 I will just always be imprisoned by negativity and
 suffering. This is ridiculous."
 I would sleep in a cave with more than, I think, more than
 100 mosquito bites by now
 with the chance of actually getting some diseases and
 sleeping less than eight hours,
 which is actually, it actually isn't that bad by now.
 But at first, it was too much of a challenge for me.
 But you realize whether if it's positive experiences or
 negative experiences, nothing stays forever.
 So why bother trying to do those things that make you feel
 like you've reached a certain stage where you're just fully
 content?
 Because that stage, I don't think that stage ever exists in
 society anyway.
 Society tells you that you do these things, and once you
 reach them, there's this eternal happiness,
 and you just reach it, and then you're just content. That's
 your ticket.
 But that never happens. You do all those things, but
 internally you don't know the truth.
 You still feel this sort of disharmony and this gap and
 this sense of lack.
 And it's when you realize that certain emotions and
 thoughts, they're just not satisfying in a sense.
 Yeah, that's the thing. They're not satisfying.
 So once you stay here for a while, you let go of some
 things,
 because you realize the things that you're clinging onto
 back home.
 I mean, sure, they're good, but they're also using up a lot
 of your attention and energy that are not necessary,
 that are not ultimately conducive to your happiness. They
 don't add any more happiness.
 They don't really make it eternal. In fact, they actually
 add more addiction,
 because the more you experience them, the more you want it,
 because you get used to,
 you know, some of the things that I would do at home or
 back in the West, I would do them,
 and sure, they make me happy, but I would expect them to
 make me happier the next time.
 And it's just a constant cycle, and I would get tired of
 them, and I'd move on.
 And that's kind of like the tug of war of life.
 You know, this happiness, you try to pull it towards you,
 but then it keeps on going away,
 you try to pull it towards you again, it keeps on going
 away.
 You never just sort of relax. You always want something for
 yourself,
 and you're resistant to something else that you don't want.
 Once you come here, you just let go. You just tell yourself
, "Stop playing the game."
 You don't want to play the tug of war anymore, you just...
 whatever.
 And it's not a sense of carelessness or apathy, but it's a
 sense of clarity.
 You're really clear on the true nature of your emotions and
 thoughts.
 And you realize what's good for you, what's good for others
,
 what's bad for you and what's bad for others.
 And that's it.
 So those are the things that I've learned majorly.
 And I really hope that I don't forget them when I go back
 home, which I won't.
 I don't think I will, because a lot of the things that I
 learned,
 it's too major for me to forget. It's too big for me to
 forget.
 I think it's already being internalized inside of me.
 The fact that emotions come and go, thoughts come and go.
 To actually have that experience, it's much more different
 than to read about it in a book.
 Because I knew about it. I knew everything that I know now
 before I came here.
 Everything I'm telling you now, I had the ability to tell
 you back at home before anything happened.
 Because I had read about it. I knew everything that...
 I knew the basics of Buddhism. I knew the basic results
 that you get from meditation.
 I read a lot about philosophy, about society.
 I knew a lot of things about how to live a good life.
 But obviously it's different. Something is different here.
 You get the actual experience.
 To know something intellectually, you don't really know it.
 You just have the information, but you don't internalize it
.
 You don't digest it completely. You can just regurgitate
 just from memory.
 But when you actually experience it, you... it's a sort of
 merge, sort of fusion with that knowledge.
 You embody that knowledge now.
 You're the personification of what you've learned.
 And so, yeah, I mean, I would really urge the people who
 are really interested in Buddhism not to just read about it
.
 Because you can read a million books about Buddhism, know
 every single little thing.
 But I would guarantee you that the results that you get
 from reading it would not be as good as someone who never
 knew anything about Buddhism,
 but just meditated for 20 days or even 10 days and had the
 direct experience of what you read.
 That's the most important thing I've learned.
 Well, one of the most important things I've learned is that
 you have to get direct training.
 You have to get meditation.
 And you have to have a teacher to guide you, which I was
 fortunate enough to be with a very good one.
 And, yeah, that's basically it.
 That's basically the answer that I would give in terms of
 what I've learned and the experiences and the things I've
 gained here.
 So, yeah, thank you for listening in, if you are.
