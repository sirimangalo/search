1
00:00:00,000 --> 00:00:29,000
 Good evening everyone.

2
00:00:29,000 --> 00:00:34,000
 So I'm broadcasting live audio.

3
00:00:34,000 --> 00:00:45,000
 I'm going to do a video tonight.

4
00:00:45,000 --> 00:00:49,120
 The quote is actually one of the Dhammapada quotes that we

5
00:00:49,120 --> 00:00:53,000
've already gone through.

6
00:00:53,000 --> 00:01:01,270
 So if you want to learn about it, you can go to the series

7
00:01:01,270 --> 00:01:04,000
 on the Dhammapada.

8
00:01:04,000 --> 00:01:07,390
 I'm getting closer to exams, so I'm going to try to take

9
00:01:07,390 --> 00:01:11,000
 some time to focus on them.

10
00:01:11,000 --> 00:01:14,000
 And tomorrow we have this story telling things,

11
00:01:14,000 --> 00:01:19,510
 so tonight I have to sort of rehearse and figure out

12
00:01:19,510 --> 00:01:25,000
 exactly what I'm going to say about this story.

13
00:01:25,000 --> 00:01:32,000
 But here we are broadcasting.

14
00:01:32,000 --> 00:01:36,080
 So if anyone has any questions or anything you want me to

15
00:01:36,080 --> 00:01:38,000
 talk about,

16
00:01:38,000 --> 00:01:44,000
 I'm happy to spend a little bit of time talking Dhamma.

17
00:01:44,000 --> 00:01:47,280
 We have a new meditator here tonight, so we have three med

18
00:01:47,280 --> 00:01:51,000
itators, five of us in the house.

19
00:01:51,000 --> 00:01:56,000
 Really, this house is too small.

20
00:01:56,000 --> 00:02:04,000
 We have three bedrooms, we have five people staying here.

21
00:02:04,000 --> 00:02:11,160
 Which is great, and more people applying to come and med

22
00:02:11,160 --> 00:02:12,000
itate,

23
00:02:12,000 --> 00:02:22,000
 still have a backlog of people to confirm and talk about,

24
00:02:22,000 --> 00:02:30,000
 discuss about their potential visit.

25
00:02:30,000 --> 00:02:36,000
 But lots of interest, which is great.

26
00:02:36,000 --> 00:02:43,070
 Anyway, does anyone have any questions or anything you want

27
00:02:43,070 --> 00:02:46,000
 me to talk about?

28
00:02:46,000 --> 00:02:53,000
 Can you even hear me?

29
00:02:53,000 --> 00:03:08,000
 I'm broadcasting. I think you can hear me.

30
00:03:08,000 --> 00:03:29,000
 Hello.

31
00:03:29,000 --> 00:03:37,000
 Maybe everybody's waiting for the video on YouTube.

32
00:03:37,000 --> 00:03:44,000
 Oh, they're here.

33
00:03:44,000 --> 00:03:49,000
 Chat wasn't updating.

34
00:03:49,000 --> 00:03:56,000
 Or there's a delay, I think.

35
00:03:56,000 --> 00:03:58,000
 Yeah, I'm not going to do a video tonight.

36
00:03:58,000 --> 00:04:03,000
 The Dhammapada verse is one I've already talked about.

37
00:04:03,000 --> 00:04:10,790
 It's about Nanda Koppala, the cowherd who gets killed by a

38
00:04:10,790 --> 00:04:11,000
 hunter.

39
00:04:11,000 --> 00:04:14,000
 And they think, "Huh, he didn't deserve that."

40
00:04:14,000 --> 00:04:43,000
 That's his own. Yeah, I don't know.

41
00:04:43,000 --> 00:05:08,000
 Okay.

42
00:05:08,000 --> 00:05:12,010
 There are no questions. I think we're going to end the

43
00:05:12,010 --> 00:05:14,000
 broadcast short tonight.

44
00:05:14,000 --> 00:05:19,000
 And we'll see how it goes tomorrow.

45
00:05:19,000 --> 00:05:36,000
 But I guess that's all. Have a good night, everyone.

