1
00:00:00,000 --> 00:00:16,560
 Okay, good evening everyone.

2
00:00:16,560 --> 00:00:24,880
 Welcome to our evening Dhamma session.

3
00:00:24,880 --> 00:00:37,080
 So, I've been invited by a friend, a friend of mine.

4
00:00:37,080 --> 00:00:43,090
 One of the monks here is putting on a big conference in a

5
00:00:43,090 --> 00:00:46,680
 couple weeks and they needed

6
00:00:46,680 --> 00:00:50,120
 people to give, to speak at it.

7
00:00:50,120 --> 00:00:52,800
 So I'm giving a talk.

8
00:00:52,800 --> 00:00:57,040
 I don't want to make it sound like somehow they sought me

9
00:00:57,040 --> 00:00:58,080
 out for it.

10
00:00:58,080 --> 00:01:04,040
 I think I was just available.

11
00:01:04,040 --> 00:01:15,320
 But one of the talks is going to be on wrong mindfulness

12
00:01:15,320 --> 00:01:18,480
 and the other is going to be on

13
00:01:18,480 --> 00:01:20,600
 Buddhism in a digital age.

14
00:01:20,600 --> 00:01:26,970
 So I thought maybe in the next week or so I can go over

15
00:01:26,970 --> 00:01:30,720
 some of the ideas, test them

16
00:01:30,720 --> 00:01:34,960
 out on you.

17
00:01:34,960 --> 00:01:41,440
 My captive audience.

18
00:01:41,440 --> 00:01:44,720
 So tonight I thought I'd talk about modern Buddhism.

19
00:01:44,720 --> 00:01:50,160
 Buddhism in a digital age.

20
00:01:50,160 --> 00:01:55,720
 Even just generally a modern age.

21
00:01:55,720 --> 00:02:09,560
 There's one fairly clear debate or division in modern

22
00:02:09,560 --> 00:02:13,200
 Buddhism and that's between engagement

23
00:02:13,200 --> 00:02:15,200
 and renunciation.

24
00:02:15,200 --> 00:02:23,120
 I mean I think most Buddhism is renunciant.

25
00:02:23,120 --> 00:02:26,120
 Is on the side of renunciation.

26
00:02:26,120 --> 00:02:28,720
 Leaving society going off in the forest.

27
00:02:28,720 --> 00:02:32,490
 And when you think of Buddhism, I think it's still more

28
00:02:32,490 --> 00:02:34,680
 common to think of the Buddha off

29
00:02:34,680 --> 00:02:38,840
 under a tree in the forest.

30
00:02:38,840 --> 00:02:46,080
 But there are those who struggle with that and argue for a

31
00:02:46,080 --> 00:02:50,680
 Buddhism or feel more comfortable

32
00:02:50,680 --> 00:02:55,480
 practicing a Buddhism that is engaged.

33
00:02:55,480 --> 00:03:02,360
 That stays in the world and works to better the world.

34
00:03:02,360 --> 00:03:14,280
 Works to engage with the world rather than disengage.

35
00:03:14,280 --> 00:03:20,200
 It's a struggle that Buddhists individually go through and

36
00:03:20,200 --> 00:03:23,000
 it's a division between various

37
00:03:23,000 --> 00:03:35,370
 Buddhist institutions, some very social oriented and some

38
00:03:35,370 --> 00:03:41,240
 very reclusive oriented.

39
00:03:41,240 --> 00:03:48,280
 And so this is seen as sort of a modern dilemma I think.

40
00:03:48,280 --> 00:03:53,840
 But it really is actually an ancient dilemma.

41
00:03:53,840 --> 00:03:57,650
 In the time of the Buddha there was this question of

42
00:03:57,650 --> 00:04:00,880
 whether one should be engaged or one should

43
00:04:00,880 --> 00:04:03,880
 be secluded.

44
00:04:03,880 --> 00:04:11,710
 And so you have Devadatta, the Buddhist cousin who made all

45
00:04:11,710 --> 00:04:15,240
 sorts of trouble and did all

46
00:04:15,240 --> 00:04:22,960
 sorts of wicked things.

47
00:04:22,960 --> 00:04:26,670
 He tried to entrap the Buddha by proposing five rules, one

48
00:04:26,670 --> 00:04:28,640
 of which was that monks should

49
00:04:28,640 --> 00:04:32,560
 always live in the forest.

50
00:04:32,560 --> 00:04:35,980
 And he knew that the Buddha wouldn't accept these rules.

51
00:04:35,980 --> 00:04:41,500
 And so by proposing them he wanted to show that the Buddha

52
00:04:41,500 --> 00:04:42,800
 was soft.

53
00:04:42,800 --> 00:04:46,200
 That the Buddha was unfit to be the leader.

54
00:04:46,200 --> 00:04:54,800
 That Devadatta was the true hardcore dedicated leader.

55
00:04:54,800 --> 00:04:56,080
 But the Buddha said of course no.

56
00:04:56,080 --> 00:04:57,440
 He said no monks don't.

57
00:04:57,440 --> 00:05:00,640
 If monks live in a city they should practice Buddhism.

58
00:05:00,640 --> 00:05:07,580
 If monks live in the forest they should also practice the D

59
00:05:07,580 --> 00:05:08,800
hamma.

60
00:05:08,800 --> 00:05:15,800
 So that's what I think.

61
00:05:15,800 --> 00:05:16,800
 [Birds chirping]

