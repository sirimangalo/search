1
00:00:00,000 --> 00:00:04,750
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,750 --> 00:00:06,280
 Today we continue with

3
00:00:06,280 --> 00:00:12,640
 verse 226 which reads as follows.

4
00:00:13,360 --> 00:00:22,040
 sadhā jāgaramananān ohoratānu sikinān nibānān adi

5
00:00:22,040 --> 00:00:29,800
 mutānān atanga canti asavā

6
00:00:31,080 --> 00:00:42,530
 which means for those who are always awake training both

7
00:00:42,530 --> 00:00:46,240
 day and night and

8
00:00:46,240 --> 00:00:52,820
 for those intent on nibāṇa the defilements of the mind

9
00:00:52,820 --> 00:00:53,920
 come to an end.

10
00:00:53,920 --> 00:01:04,580
 This verse was taught in response to a discussion between

11
00:01:04,580 --> 00:01:08,080
 the Buddha and a slave

12
00:01:08,080 --> 00:01:13,680
 woman named Pūṇa, a servant woman or slave. I guess I'm

13
00:01:13,680 --> 00:01:14,840
 not quite sure how you

14
00:01:14,840 --> 00:01:20,200
 should translate it. I don't know that there were, I guess

15
00:01:20,200 --> 00:01:21,360
 there were slaves at

16
00:01:21,360 --> 00:01:27,680
 that time it was more of a indentured servitude perhaps or

17
00:01:27,680 --> 00:01:28,920
 there was

18
00:01:28,920 --> 00:01:33,880
 definitely class problems and so on low-class people were

19
00:01:33,880 --> 00:01:35,320
 they turned

20
00:01:35,320 --> 00:01:38,750
 themselves over as servants and I don't know how it worked

21
00:01:38,750 --> 00:01:41,200
 anyway. It wasn't a

22
00:01:41,200 --> 00:01:51,210
 perfect society by any means but this woman was assigned to

23
00:01:51,210 --> 00:01:52,200
 a great amount of

24
00:01:52,200 --> 00:01:55,840
 work and there was a festival perhaps I don't know she was

25
00:01:55,840 --> 00:01:57,880
 assigned to grind up

26
00:01:57,880 --> 00:02:05,510
 rice into flour or some sort of rice based work and so she

27
00:02:05,510 --> 00:02:07,040
 had to work late

28
00:02:07,040 --> 00:02:12,640
 into the night and it was tiresome and late at night in Raj

29
00:02:12,640 --> 00:02:14,200
agaha Rajagaha has a

30
00:02:14,200 --> 00:02:18,680
 city that is surrounded by mountains so late at night while

31
00:02:18,680 --> 00:02:19,760
 she was working she

32
00:02:19,760 --> 00:02:25,860
 looked over and she saw up on the mountain and Kīcakuta v

33
00:02:25,860 --> 00:02:27,520
ultures peak

34
00:02:27,520 --> 00:02:35,310
 lights along the trail traveling along the path up and down

35
00:02:35,310 --> 00:02:37,600
 the mountain and

36
00:02:37,600 --> 00:02:41,650
 it was the monks after listening to the Buddha teach or

37
00:02:41,650 --> 00:02:43,520
 listening to whoever was

38
00:02:43,520 --> 00:02:49,560
 teaching that day late at night they would return back to

39
00:02:49,560 --> 00:02:53,360
 their their kuti or

40
00:02:53,360 --> 00:02:57,360
 return back to their tent or tree perhaps that they were

41
00:02:57,360 --> 00:02:59,440
 staying under and

42
00:02:59,440 --> 00:03:04,570
 they had a light and go along the way and she thought but

43
00:03:04,570 --> 00:03:05,760
 she didn't know this

44
00:03:05,760 --> 00:03:09,780
 and she thought to herself wow I wonder why they're up so

45
00:03:09,780 --> 00:03:11,120
 late and I'm up

46
00:03:11,120 --> 00:03:16,050
 because I have all this work to do it must be some some

47
00:03:16,050 --> 00:03:17,140
 sickness or something

48
00:03:17,140 --> 00:03:21,950
 wrong must be something wrong I wonder why they're up so

49
00:03:21,950 --> 00:03:23,500
 late and thought

50
00:03:23,500 --> 00:03:27,130
 nothing more of it and then in the morning she got together

51
00:03:27,130 --> 00:03:28,560
 some of the the

52
00:03:28,560 --> 00:03:33,590
 rice dust that was left over from her work and took it up

53
00:03:33,590 --> 00:03:34,920
 in her hand poured

54
00:03:34,920 --> 00:03:41,170
 some water over it and made it into a rice paddy and put it

55
00:03:41,170 --> 00:03:43,480
 in the stove and

56
00:03:43,480 --> 00:03:49,740
 the charcoal to heat it and prepared her meal that way and

57
00:03:49,740 --> 00:03:51,960
 then walked out of her

58
00:03:51,960 --> 00:03:58,040
 place of residence and maybe was going to work I don't know

59
00:03:58,040 --> 00:03:59,200
 and she met the

60
00:03:59,200 --> 00:04:02,970
 Buddha the Buddha was going on alms round and she thought

61
00:04:02,970 --> 00:04:05,920
 to herself I have

62
00:04:05,920 --> 00:04:10,660
 this this offering I could give this to this monk who

63
00:04:10,660 --> 00:04:12,400
 appears to be very very

64
00:04:12,400 --> 00:04:16,160
 much worth supporting I could do that and that would be a

65
00:04:16,160 --> 00:04:17,200
 great thing for me to do

66
00:04:17,200 --> 00:04:21,430
 but there's no way he'd accept it and the Buddha stopped

67
00:04:21,430 --> 00:04:24,520
 and looked at her and so

68
00:04:24,520 --> 00:04:30,280
 she she held out this course you know rice dust really the

69
00:04:30,280 --> 00:04:31,400
 worst food

70
00:04:31,400 --> 00:04:37,530
 lowest grade food you could you could make and the Buddha

71
00:04:37,530 --> 00:04:38,340
 held out his bowl

72
00:04:38,340 --> 00:04:42,200
 she put the food in her in his bowl and thought to herself

73
00:04:42,200 --> 00:04:43,740
 well he's accepted it

74
00:04:43,740 --> 00:04:47,790
 out of kindness to me that's that's very kind of him to to

75
00:04:47,790 --> 00:04:49,260
 accept my offering

76
00:04:49,260 --> 00:04:52,950
 there's no way he's gonna eat it he'll give it to someone

77
00:04:52,950 --> 00:04:53,960
 else and so she

78
00:04:53,960 --> 00:04:58,650
 followed along behind him and watched and the Buddha went

79
00:04:58,650 --> 00:05:00,000
 to a tree stopped

80
00:05:00,000 --> 00:05:03,610
 under the tree sat down and started eating her her rice

81
00:05:03,610 --> 00:05:05,400
 cake and so she went

82
00:05:05,400 --> 00:05:11,760
 up to him and she sat down and waited for him to finish and

83
00:05:11,760 --> 00:05:14,720
 they got to

84
00:05:14,720 --> 00:05:19,790
 talking and she remarked him she said so I'm up I'm up late

85
00:05:19,790 --> 00:05:20,680
 at night because of

86
00:05:20,680 --> 00:05:25,640
 all the work and all the duress I'm under to get this work

87
00:05:25,640 --> 00:05:29,520
 completed what's

88
00:05:29,520 --> 00:05:34,040
 what's up with you there must be something wrong with you

89
00:05:34,040 --> 00:05:34,640
 with the monks

90
00:05:34,640 --> 00:05:39,120
 that they're up so late and the Buddha said well it's true

91
00:05:39,120 --> 00:05:39,600
 that you're up

92
00:05:39,600 --> 00:05:44,680
 because of your stress the stress that you're under but my

93
00:05:44,680 --> 00:05:46,640
 students they are up

94
00:05:46,640 --> 00:05:51,160
 all night because that's there that's the work that they

95
00:05:51,160 --> 00:05:53,240
 are doing because

96
00:05:53,240 --> 00:05:58,480
 they are intent upon refreeing themselves from suffering

97
00:05:58,480 --> 00:05:59,040
 and we taught

98
00:05:59,040 --> 00:06:07,260
 this verse so I think what the story reminds us or teaches

99
00:06:07,260 --> 00:06:08,880
 us the lesson it

100
00:06:08,880 --> 00:06:17,810
 has it's in the juxtaposition between the the worldly exert

101
00:06:17,810 --> 00:06:20,240
ion and the way of

102
00:06:20,240 --> 00:06:26,690
 life of a Buddhist meditator it reminds us that meditation

103
00:06:26,690 --> 00:06:28,880
 isn't just a hobby I

104
00:06:28,880 --> 00:06:35,040
 think a lot of Buddhists of course a lot of people who

105
00:06:35,040 --> 00:06:36,200
 practice Buddhist

106
00:06:36,200 --> 00:06:49,300
 meditation never realize the the potential for practicing

107
00:06:49,300 --> 00:06:51,360
 day and night

108
00:06:51,360 --> 00:06:56,890
 training themselves not only as a as a hobby or as a sort

109
00:06:56,890 --> 00:06:58,720
 of a side exercise

110
00:06:58,720 --> 00:07:03,930
 but as a truly as a way of life as an as a way of exerting

111
00:07:03,930 --> 00:07:06,880
 themselves you know we

112
00:07:06,880 --> 00:07:11,600
 think of meditation differently from work that we do or

113
00:07:11,600 --> 00:07:12,560
 ambitions that we

114
00:07:12,560 --> 00:07:19,840
 have life goals and a life's work it's rare to find someone

115
00:07:19,840 --> 00:07:20,640
 who thinks of

116
00:07:20,640 --> 00:07:26,720
 Buddhism or Buddhist meditation as a life work and so the

117
00:07:26,720 --> 00:07:28,800
 idea of staying awake

118
00:07:28,800 --> 00:07:32,710
 all day and all night and doing something like meditating

119
00:07:32,710 --> 00:07:33,400
 all day and

120
00:07:33,400 --> 00:07:37,870
 all night seems crazy I've suggested that to to some people

121
00:07:37,870 --> 00:07:38,380
 and they just

122
00:07:38,380 --> 00:07:41,610
 thought it was the most crazy thing to suggest that one

123
00:07:41,610 --> 00:07:42,760
 might stay up all night

124
00:07:42,760 --> 00:07:47,840
 meditating and yet we do stay up all night for other

125
00:07:47,840 --> 00:07:49,120
 reasons we'll stay up

126
00:07:49,120 --> 00:07:57,360
 all night partying or studying working we'll even stay up

127
00:07:57,360 --> 00:07:58,200
 all night suffering

128
00:07:58,200 --> 00:08:03,950
 if we're if we're an insomniac or if we have some grievance

129
00:08:03,950 --> 00:08:06,400
 people who have

130
00:08:06,400 --> 00:08:10,170
 lost loved ones might stay up all night so we stay up all

131
00:08:10,170 --> 00:08:11,640
 night for all sorts of

132
00:08:11,640 --> 00:08:20,200
 different reasons pull an all nighter for for our work and

133
00:08:20,200 --> 00:08:22,360
 so the the idea that

134
00:08:22,360 --> 00:08:25,460
 meditation practice practice of Buddhism training in the

135
00:08:25,460 --> 00:08:27,520
 Buddhist teaching should

136
00:08:27,520 --> 00:08:31,980
 be similar is an important idea that we should understand

137
00:08:31,980 --> 00:08:33,640
 and appreciate

138
00:08:34,120 --> 00:08:41,310
 sadhaj jag araman anam for those who are awake always it

139
00:08:41,310 --> 00:08:43,960
 really is a an ideal that

140
00:08:43,960 --> 00:08:50,040
 we strive for in our practice to get to the point where we

141
00:08:50,040 --> 00:08:52,360
 can be awake day and

142
00:08:52,360 --> 00:08:58,720
 night where we can be mindful is of course sleep is sleep

143
00:08:58,720 --> 00:09:00,760
 is a it's a way of

144
00:09:00,760 --> 00:09:06,120
 resetting the mind and if your mind is engaged in

145
00:09:06,120 --> 00:09:08,440
 activities that sully it that

146
00:09:08,440 --> 00:09:14,790
 that increase its stress and tension and wind it up then

147
00:09:14,790 --> 00:09:15,960
 sleep is a way of coming

148
00:09:15,960 --> 00:09:19,780
 back to an ordinary and normal state and so it feels quite

149
00:09:19,780 --> 00:09:20,680
 relieving and so we

150
00:09:20,680 --> 00:09:24,040
 really like sleep but for a meditator it's quite the

151
00:09:24,040 --> 00:09:26,400
 opposite meditation is a

152
00:09:26,400 --> 00:09:31,860
 means of unwinding it's a means of untying winding down the

153
00:09:31,860 --> 00:09:33,120
 mind and and

154
00:09:33,120 --> 00:09:38,240
 sleep actually winds it back up because all of our ordinary

155
00:09:38,240 --> 00:09:40,080
 and familiar habits

156
00:09:40,080 --> 00:09:44,680
 you know reassert themselves through dreams and through

157
00:09:44,680 --> 00:09:45,560
 just through the

158
00:09:45,560 --> 00:09:48,780
 relaxed state of sleep and so we find the progress we've

159
00:09:48,780 --> 00:09:49,760
 gained is often

160
00:09:49,760 --> 00:09:54,870
 reset and so practicing day and night is because of the

161
00:09:54,870 --> 00:09:56,280
 nature of meditation is

162
00:09:56,280 --> 00:10:00,990
 actually a better thing for the mind than for the body even

163
00:10:00,990 --> 00:10:03,400
 then sleep it can

164
00:10:03,400 --> 00:10:12,440
 be that's I think the lesson of the story that we should

165
00:10:12,440 --> 00:10:13,400
 treat meditation as

166
00:10:13,400 --> 00:10:19,540
 something as a life's work something we have to put our

167
00:10:19,540 --> 00:10:21,080
 whole heart in if you

168
00:10:21,080 --> 00:10:24,180
 really want to gain results it's something you have to

169
00:10:24,180 --> 00:10:25,080
 decide it's going

170
00:10:25,080 --> 00:10:30,570
 to be a part of your life not just a hobby or not just an

171
00:10:30,570 --> 00:10:32,880
 escape but something

172
00:10:32,880 --> 00:10:36,230
 that you take as a training and so that's what the verse

173
00:10:36,230 --> 00:10:37,280
 starts to teach

174
00:10:37,280 --> 00:10:41,230
 the verse has four four parts the first is in relation to

175
00:10:41,230 --> 00:10:43,760
 being always awake the

176
00:10:43,760 --> 00:10:47,840
 second is in regards to the training training day and night

177
00:10:47,840 --> 00:10:49,160
 so the Buddha

178
00:10:49,160 --> 00:10:52,820
 taught three types of training or three aspect three parts

179
00:10:52,820 --> 00:10:54,960
 to the training

180
00:10:54,960 --> 00:10:59,390
 the first one is sila this means ethical behavior the

181
00:10:59,390 --> 00:11:01,000
 second is samadhi which

182
00:11:01,000 --> 00:11:05,480
 means focus or concentration and the third is panya which

183
00:11:05,480 --> 00:11:07,840
 means wisdom and

184
00:11:07,840 --> 00:11:10,750
 there's a sense that the first leads to the second the

185
00:11:10,750 --> 00:11:12,200
 second leads to the third

186
00:11:12,200 --> 00:11:15,440
 but also a sense that this is the three aspects of our

187
00:11:15,440 --> 00:11:16,920
 training when we train

188
00:11:16,920 --> 00:11:22,520
 when we practice mindfulness we're training in all three so

189
00:11:22,520 --> 00:11:23,360
 sila the first

190
00:11:23,360 --> 00:11:28,650
 one ethics it refers of course to not breaking precepts so

191
00:11:28,650 --> 00:11:31,680
 we should always be

192
00:11:31,680 --> 00:11:35,000
 keeping ethical precepts that we don't do this and we don't

193
00:11:35,000 --> 00:11:35,960
 do that not just

194
00:11:35,960 --> 00:11:40,020
 sometimes not just when it's convenient I won't kill when

195
00:11:40,020 --> 00:11:41,520
 it's convenient but

196
00:11:41,520 --> 00:11:45,120
 when there are mosquitoes well I'll take a break from that

197
00:11:45,120 --> 00:11:46,040
 you can't do that

198
00:11:46,040 --> 00:11:52,380
 taking a break is especially for ethics it misses the point

199
00:11:52,380 --> 00:11:53,320
 of ethics because

200
00:11:53,320 --> 00:11:58,210
 it's not so much about the body as it is about what it does

201
00:11:58,210 --> 00:11:59,520
 for your mind it

202
00:11:59,520 --> 00:12:02,830
 helps you focus and if you're only ethical when it's

203
00:12:02,830 --> 00:12:03,760
 convenient you're not

204
00:12:03,760 --> 00:12:06,800
 really ethical at all you're not changing the mind in any

205
00:12:06,800 --> 00:12:07,440
 appreciable

206
00:12:07,440 --> 00:12:15,550
 degree but when you are ethical when you do guard the mind

207
00:12:15,550 --> 00:12:17,600
 or guard the body and

208
00:12:17,600 --> 00:12:21,920
 speech with the mind then it does change it does focus your

209
00:12:21,920 --> 00:12:23,920
 mind because the

210
00:12:23,920 --> 00:12:28,420
 avenues by which the mind would get lost and get sidetr

211
00:12:28,420 --> 00:12:31,520
acked are shut off and so

212
00:12:31,520 --> 00:12:35,150
 it involves not just keeping precepts but also guarding our

213
00:12:35,150 --> 00:12:36,200
 behavior when you

214
00:12:36,200 --> 00:12:41,520
 walk ethical walking means being with the walking you know

215
00:12:41,520 --> 00:12:43,000
 being objective as

216
00:12:43,000 --> 00:12:47,810
 you walk ethical speaking means being present when you

217
00:12:47,810 --> 00:12:48,840
 speak being aware of

218
00:12:48,840 --> 00:12:54,610
 what you're saying and being aware of the emotions guarding

219
00:12:54,610 --> 00:12:56,280
 the mind as it

220
00:12:56,280 --> 00:13:05,690
 approaches the physical door and the verbal door the second

221
00:13:05,690 --> 00:13:07,320
 training samadhi

222
00:13:07,320 --> 00:13:10,940
 often is translated as concentration but focus maybe makes

223
00:13:10,940 --> 00:13:12,160
 more sense because

224
00:13:12,160 --> 00:13:16,480
 it's about focusing for not just focusing in but also

225
00:13:16,480 --> 00:13:20,080
 coming into focus as you're

226
00:13:20,080 --> 00:13:23,200
 more ethical your mind will start to write itself samadhi

227
00:13:23,200 --> 00:13:24,600
 has a meaning

228
00:13:24,600 --> 00:13:29,490
 etymologically of balance I think it's like said like the

229
00:13:29,490 --> 00:13:30,960
 word same it's like a

230
00:13:30,960 --> 00:13:36,220
 balance when you're not when you're not un-centered or

231
00:13:36,220 --> 00:13:37,520
 unfocused when you get

232
00:13:37,520 --> 00:13:41,370
 to the point where everything starts to settle right this

233
00:13:41,370 --> 00:13:42,720
 is why tranquility

234
00:13:42,720 --> 00:13:47,040
 meditation is such a useful base for practice because it

235
00:13:47,040 --> 00:13:49,360
 settles the mind but

236
00:13:49,360 --> 00:13:56,150
 mindfulness and mindfulness of experience drives straight

237
00:13:56,150 --> 00:13:57,000
 to the point

238
00:13:57,000 --> 00:14:01,970
 because it straightens out our crookedness it straightens

239
00:14:01,970 --> 00:14:02,800
 out all the

240
00:14:02,800 --> 00:14:06,600
 disruptions it settles all of our disruptions unties all

241
00:14:06,600 --> 00:14:07,680
 the knots in the

242
00:14:07,680 --> 00:14:12,620
 mind because normally when we experience for example pain

243
00:14:12,620 --> 00:14:14,960
 or bad memories or loud

244
00:14:14,960 --> 00:14:22,460
 noises we react you know we're tied to them we're caught up

245
00:14:22,460 --> 00:14:23,120
 by them and this

246
00:14:23,120 --> 00:14:29,040
 disturbs the mind it it unfocuses our mind so as we become

247
00:14:29,040 --> 00:14:29,880
 more mindful our

248
00:14:29,880 --> 00:14:34,040
 mind focuses and stabilizes and that's necessary in order

249
00:14:34,040 --> 00:14:35,440
 to see clearly right

250
00:14:35,440 --> 00:14:39,420
 but just like with a camera in order to see you have to

251
00:14:39,420 --> 00:14:41,320
 focus it just like a

252
00:14:41,320 --> 00:14:47,750
 pool of water if you are a pot of water if if you stir it

253
00:14:47,750 --> 00:14:49,960
 up or if it's boiling

254
00:14:49,960 --> 00:14:52,850
 or something you know you can't see until everything

255
00:14:52,850 --> 00:14:53,840
 settles to the bottom

256
00:14:53,840 --> 00:14:57,440
 and once it settles then you can see what's in all the in

257
00:14:57,440 --> 00:14:58,960
 tide pools when the

258
00:14:58,960 --> 00:15:02,210
 when the ocean tide comes in it gets can get muddy but then

259
00:15:02,210 --> 00:15:04,200
 when it settles then

260
00:15:04,200 --> 00:15:11,350
 you can see so when the mind is riled up when the mind is

261
00:15:11,350 --> 00:15:13,440
 disturbed when the

262
00:15:13,440 --> 00:15:18,310
 mind is caught up by all the emotions and attachments

263
00:15:18,310 --> 00:15:20,480
 reactions that are a part

264
00:15:20,480 --> 00:15:24,040
 of our ordinary non meditative life we can't see anything

265
00:15:24,040 --> 00:15:27,480
 clearly and so the

266
00:15:27,480 --> 00:15:30,760
 just as the first leads to the second the second leads to

267
00:15:30,760 --> 00:15:31,920
 the third which is

268
00:15:31,920 --> 00:15:40,520
 panya panya meaning wisdom and wisdom is in Buddhism not

269
00:15:40,520 --> 00:15:42,480
 very similar to what we

270
00:15:42,480 --> 00:15:46,550
 normally think of as wisdom in non meditative circles when

271
00:15:46,550 --> 00:15:47,440
 we talk about

272
00:15:47,440 --> 00:15:51,100
 wisdom we think of something intellectual but wisdom really

273
00:15:51,100 --> 00:15:51,520
 means

274
00:15:51,520 --> 00:15:56,030
 becoming more familiar with reality it's not about studying

275
00:15:56,030 --> 00:15:57,360
 intellectually

276
00:15:57,360 --> 00:16:01,910
 anything that we might call reality it's about studying

277
00:16:01,910 --> 00:16:03,360
 firsthand so that we

278
00:16:03,360 --> 00:16:07,960
 become more familiar what does that mean it means becoming

279
00:16:07,960 --> 00:16:08,600
 more familiar with

280
00:16:08,600 --> 00:16:14,730
 pain because familiarity removes or at least reduces step

281
00:16:14,730 --> 00:16:17,280
 by step our

282
00:16:17,280 --> 00:16:22,560
 reactivity you know something that is so familiar to us

283
00:16:22,560 --> 00:16:24,160
 doesn't have the same

284
00:16:24,160 --> 00:16:29,180
 power over us something that we are we are constantly just

285
00:16:29,180 --> 00:16:30,400
 reacting to you know

286
00:16:30,400 --> 00:16:35,910
 trying to avoid shying away from and so by focusing on pain

287
00:16:35,910 --> 00:16:37,560
 by focusing on the

288
00:16:37,560 --> 00:16:42,120
 bad memories by focusing on the sound that disturbs us we

289
00:16:42,120 --> 00:16:42,880
 become more familiar

290
00:16:42,880 --> 00:16:46,360
 with it we become more familiar with our reactions and

291
00:16:46,360 --> 00:16:48,640
 whereas a result able to

292
00:16:48,640 --> 00:16:51,370
 see the reactions that are positive and beneficial and

293
00:16:51,370 --> 00:16:52,120
 those that are

294
00:16:52,120 --> 00:16:56,680
 unbeneficial we're able to see firsthand what it is that's

295
00:16:56,680 --> 00:16:57,760
 causing us stress and

296
00:16:57,760 --> 00:17:03,040
 suffering and as it becomes more familiar we become wiser

297
00:17:03,040 --> 00:17:04,600
 it means we know more

298
00:17:04,600 --> 00:17:11,360
 clearly we we understand more clearly the results of our of

299
00:17:11,360 --> 00:17:12,520
 our behaviors the

300
00:17:12,520 --> 00:17:19,150
 results of our reactions and so as a result we change the

301
00:17:19,150 --> 00:17:19,960
 wisdom is about

302
00:17:19,960 --> 00:17:23,760
 change really the process of acquiring wisdom is about

303
00:17:23,760 --> 00:17:25,000
 abandoning all the wrong

304
00:17:25,000 --> 00:17:34,760
 behaviors all the misunderstandings all the self-harming

305
00:17:34,760 --> 00:17:38,960
 tendencies that we have

306
00:17:39,560 --> 00:17:47,210
 Oh horror tanu sikino horror horror a hose is day ratas

307
00:17:47,210 --> 00:17:50,520
 night horror tanu sikino

308
00:17:50,520 --> 00:17:56,880
 nibana nandi mutta nam intent on nibana this then refers to

309
00:17:56,880 --> 00:17:57,920
 the goal so when you

310
00:17:57,920 --> 00:18:01,840
 say we train we train when you talk about meditation as a

311
00:18:01,840 --> 00:18:03,320
 training one of

312
00:18:03,320 --> 00:18:08,130
 the biggest challenges to doing that is understanding why

313
00:18:08,130 --> 00:18:09,680
 you might be doing

314
00:18:09,680 --> 00:18:16,230
 that we understand that that we would party all night

315
00:18:16,230 --> 00:18:16,960
 because of the pleasure

316
00:18:16,960 --> 00:18:21,600
 no the excitement we understand that we would study all

317
00:18:21,600 --> 00:18:22,880
 night because of the

318
00:18:22,880 --> 00:18:26,920
 grades that we might get because of the eventual job that

319
00:18:26,920 --> 00:18:28,560
 we might get we work

320
00:18:28,560 --> 00:18:33,490
 all night because of the money the success the results that

321
00:18:33,490 --> 00:18:34,360
 we can see

322
00:18:34,360 --> 00:18:42,280
 tangible results and often it's quite daunting or

323
00:18:42,280 --> 00:18:46,840
 challenging for meditators

324
00:18:46,840 --> 00:18:49,840
 to

325
00:18:49,840 --> 00:18:55,980
 find reason to commit themselves to meditation to see the

326
00:18:55,980 --> 00:18:57,720
 benefits of the

327
00:18:57,720 --> 00:19:02,880
 meditation which is kind of curious because the benefits

328
00:19:02,880 --> 00:19:04,000
 are very similar to

329
00:19:04,000 --> 00:19:07,390
 any other type of training or work that you might undertake

330
00:19:07,390 --> 00:19:08,480
 you know they're

331
00:19:08,480 --> 00:19:13,920
 very much related to the actual nature of the work if you

332
00:19:13,920 --> 00:19:16,680
 if you study all

333
00:19:16,680 --> 00:19:20,770
 night you know you know more about the subject if you work

334
00:19:20,770 --> 00:19:23,080
 in a factory all

335
00:19:23,080 --> 00:19:31,830
 night you get factory factory products if you party all

336
00:19:31,830 --> 00:19:32,480
 night well you get a

337
00:19:32,480 --> 00:19:36,190
 hangover but no you also get the excitement and the

338
00:19:36,190 --> 00:19:39,320
 pleasure I think with

339
00:19:39,320 --> 00:19:44,670
 meditation we often look too far you know we think of

340
00:19:44,670 --> 00:19:48,880
 results as being some

341
00:19:48,880 --> 00:19:58,360
 spiritual magical mystical high and airy-fairy I don't know

342
00:19:58,360 --> 00:19:59,360
 that's something

343
00:19:59,360 --> 00:20:04,600
 like like up in the up in the sky sort of like there'll be

344
00:20:04,600 --> 00:20:05,960
 a big sign you've

345
00:20:05,960 --> 00:20:11,510
 made it or something when in fact meditation is just a

346
00:20:11,510 --> 00:20:12,720
 training nirbha

347
00:20:12,720 --> 00:20:16,050
 now when we talk about nirbha nights often thought of as

348
00:20:16,050 --> 00:20:17,320
 this very special

349
00:20:17,320 --> 00:20:19,880
 thing that you would really just you know I'm gonna be like

350
00:20:19,880 --> 00:20:21,480
 walking into a

351
00:20:21,480 --> 00:20:26,060
 capital city or a kingdom or walking into heaven it would

352
00:20:26,060 --> 00:20:27,120
 be like arriving at

353
00:20:27,120 --> 00:20:32,400
 the pearly gates of heaven or something but nirbha is the

354
00:20:32,400 --> 00:20:34,400
 result of training

355
00:20:34,400 --> 00:20:38,720
 when you train yourself what happens you become trained

356
00:20:38,720 --> 00:20:41,680
 when you see clearly when

357
00:20:41,680 --> 00:20:48,390
 you observe when you cleanse the mind clear off the the

358
00:20:48,390 --> 00:20:51,240
 lens of perception you

359
00:20:51,240 --> 00:20:58,480
 see more clearly what do you get you get clarity so nirbha

360
00:20:58,480 --> 00:20:59,740
 is is more of a

361
00:20:59,740 --> 00:21:03,550
 function of the mind than anything nirbha means release or

362
00:21:03,550 --> 00:21:05,960
 or freedom and

363
00:21:05,960 --> 00:21:12,450
 often literally means extinguishing extinguishing putting

364
00:21:12,450 --> 00:21:14,880
 out the fires so

365
00:21:14,880 --> 00:21:19,060
 it's directly related to our observation and and

366
00:21:19,060 --> 00:21:20,480
 appreciation and

367
00:21:20,480 --> 00:21:25,680
 cultivation of understanding of the fires the fire is what

368
00:21:25,680 --> 00:21:26,880
 the fires in the

369
00:21:26,880 --> 00:21:32,670
 mind the fire of greed the fire of anger the fire of

370
00:21:32,670 --> 00:21:34,160
 delusion

371
00:21:34,160 --> 00:21:38,390
 nirbha is not some thing that we attain something that we

372
00:21:38,390 --> 00:21:39,600
 get to if we travel

373
00:21:39,600 --> 00:21:44,080
 long and far because you're thinking of it that way as a

374
00:21:44,080 --> 00:21:46,080
 place that you might get

375
00:21:46,080 --> 00:21:49,290
 to the place has nothing to do with the journey right and

376
00:21:49,290 --> 00:21:50,440
 so when we think like

377
00:21:50,440 --> 00:21:54,290
 that we we have no idea that we're getting close to the

378
00:21:54,290 --> 00:21:55,280
 journey right close

379
00:21:55,280 --> 00:21:58,670
 to the destination when someone is driving a car you

380
00:21:58,670 --> 00:21:59,880
 suppose you're

381
00:21:59,880 --> 00:22:02,770
 driving to a city you know until you get to the outskirts

382
00:22:02,770 --> 00:22:03,760
 of the city you don't

383
00:22:03,760 --> 00:22:06,020
 know whether you're going the right way and that's often I

384
00:22:06,020 --> 00:22:07,000
 think our meditators

385
00:22:07,000 --> 00:22:10,750
 feel because they separate the journey from the destination

386
00:22:10,750 --> 00:22:11,760
 and it's not like

387
00:22:11,760 --> 00:22:16,080
 that when you lift weights you're not trying to move the

388
00:22:16,080 --> 00:22:17,320
 weight somewhere so

389
00:22:17,320 --> 00:22:19,620
 that by the end you look and you say boy those weights got

390
00:22:19,620 --> 00:22:20,320
 all the way over

391
00:22:20,320 --> 00:22:24,170
 there or all the way up there I kept lifting them until

392
00:22:24,170 --> 00:22:25,800
 they got to heaven

393
00:22:25,800 --> 00:22:28,960
 absolutely not you lift the weights and lift the weights

394
00:22:28,960 --> 00:22:29,840
 and you turn around you

395
00:22:29,840 --> 00:22:33,430
 say they're still where they started they haven't moved at

396
00:22:33,430 --> 00:22:34,680
 all it's kind of

397
00:22:34,680 --> 00:22:38,370
 like that which is absurd of course because you're not

398
00:22:38,370 --> 00:22:39,320
 lifting weights to

399
00:22:39,320 --> 00:22:44,530
 move them you're lifting weights to strengthen the body and

400
00:22:44,530 --> 00:22:45,320
 likewise you're

401
00:22:45,320 --> 00:22:52,250
 lifting you're cultivating the you're exerting the mind in

402
00:22:52,250 --> 00:22:53,120
 order to strengthen

403
00:22:53,120 --> 00:22:58,190
 it and to polish and to cleanse it you clean you when you

404
00:22:58,190 --> 00:22:59,840
 when you clean the

405
00:22:59,840 --> 00:23:03,230
 house nothing magical happens you just end up with a

406
00:23:03,230 --> 00:23:05,840
 cleaner house when you

407
00:23:05,840 --> 00:23:09,960
 cleanse the mind there's nothing magical that happens often

408
00:23:09,960 --> 00:23:11,840
 meditators are excited

409
00:23:11,840 --> 00:23:16,000
 by sights they'll see bright lights or colors or pictures

410
00:23:16,000 --> 00:23:17,520
 maybe they'll hear

411
00:23:17,520 --> 00:23:21,070
 sounds maybe they'll feel bliss or rapture and they'll

412
00:23:21,070 --> 00:23:21,520
 think well maybe

413
00:23:21,520 --> 00:23:26,210
 that's it and none of those are it what's it is a clarity

414
00:23:26,210 --> 00:23:27,240
 of mind the mind

415
00:23:27,240 --> 00:23:34,360
 becomes more clear sees things closer to as they really are

416
00:23:34,360 --> 00:23:35,400
 the understanding of

417
00:23:35,400 --> 00:23:39,820
 the thing that the things that we we cling to are not worth

418
00:23:39,820 --> 00:23:41,680
 clinging to that

419
00:23:41,680 --> 00:23:45,760
 which we feel is permanent stable lasting is actually not

420
00:23:45,760 --> 00:23:47,160
 so the letting

421
00:23:47,160 --> 00:23:50,450
 go of the things that we cling to that which we think of as

422
00:23:50,450 --> 00:23:51,760
 satisfying this will

423
00:23:51,760 --> 00:23:56,800
 satisfy me we find out it's not and just a general

424
00:23:56,800 --> 00:23:58,280
 realization that there's

425
00:23:58,280 --> 00:24:00,880
 nothing worth there's nothing satisfying those things that

426
00:24:00,880 --> 00:24:01,680
 we crave are not

427
00:24:01,680 --> 00:24:06,150
 not actually going to satisfy us there's no benefit in

428
00:24:06,150 --> 00:24:07,760
 striving for them and

429
00:24:07,760 --> 00:24:12,920
 craving for them that the things that we think of as me and

430
00:24:12,920 --> 00:24:14,280
 mine under my control

431
00:24:14,280 --> 00:24:20,180
 are not me nor mine nor under my control all of which leads

432
00:24:20,180 --> 00:24:22,560
 us to let go needs us

433
00:24:22,560 --> 00:24:28,280
 to free ourselves from our attachments from our stress from

434
00:24:28,280 --> 00:24:30,160
 from our suffering

435
00:24:30,160 --> 00:24:35,950
 a tongue got chant the azoa so someone who trains and who

436
00:24:35,950 --> 00:24:38,480
 realizes who frees

437
00:24:38,480 --> 00:24:43,990
 themselves they will be free from all defilements of the

438
00:24:43,990 --> 00:24:45,920
 mind that's really the

439
00:24:45,920 --> 00:24:48,750
 goal if you want to ask whether you've made progress you

440
00:24:48,750 --> 00:24:50,600
 just have to ask how

441
00:24:50,600 --> 00:24:55,160
 is your mind doing is your mind seeing more clearly try and

442
00:24:55,160 --> 00:24:56,600
 get a sense of what

443
00:24:56,600 --> 00:24:59,740
 it is that you're actually doing when you say to yourself

444
00:24:59,740 --> 00:25:00,680
 pain pain what is

445
00:25:00,680 --> 00:25:04,040
 it what is what are you doing and that might not be clear

446
00:25:04,040 --> 00:25:05,000
 in the beginning but

447
00:25:05,000 --> 00:25:08,520
 it should be fairly clear as you go that what you're doing

448
00:25:08,520 --> 00:25:09,520
 is reminding yourself

449
00:25:09,520 --> 00:25:14,100
 and you're cultivating this capacity to remember things as

450
00:25:14,100 --> 00:25:15,960
 they are to see pain

451
00:25:15,960 --> 00:25:19,640
 as pain in order to say pain you have to know that it's

452
00:25:19,640 --> 00:25:20,840
 pain which we think well

453
00:25:20,840 --> 00:25:24,360
 of course and know it's pain but you don't we do for a

454
00:25:24,360 --> 00:25:25,180
 second and then we're

455
00:25:25,180 --> 00:25:29,390
 gone when you say to yourself pain you're cultivating the

456
00:25:29,390 --> 00:25:31,520
 the perspective

457
00:25:31,520 --> 00:25:34,890
 that pain is pain rather than that's a problem I have to

458
00:25:34,890 --> 00:25:36,040
 fix it what am I gonna

459
00:25:36,040 --> 00:25:40,790
 do take some medicine or get a massage or something so it's

460
00:25:40,790 --> 00:25:42,360
 a change and it

461
00:25:42,360 --> 00:25:46,410
 cultivates objectivity and and a familiarity because it

462
00:25:46,410 --> 00:25:47,160
 brings you

463
00:25:47,160 --> 00:25:50,500
 actually closer to the experience and keeps you with the

464
00:25:50,500 --> 00:25:52,560
 experience so you ask

465
00:25:52,560 --> 00:25:55,570
 you so you see you ask yourself what am I doing what is it

466
00:25:55,570 --> 00:25:56,680
 doing and you see

467
00:25:56,680 --> 00:26:00,390
 that that's what it's doing then you can only guess that

468
00:26:00,390 --> 00:26:02,560
 without doubt the results

469
00:26:02,560 --> 00:26:07,240
 will be similar okay I'm seeing things more clearly as they

470
00:26:07,240 --> 00:26:07,800
 are I'm more

471
00:26:07,800 --> 00:26:10,710
 becoming more familiar with things I'm going to have a

472
00:26:10,710 --> 00:26:11,960
 better perspective of

473
00:26:11,960 --> 00:26:15,760
 things and you can look there you can see there am I less

474
00:26:15,760 --> 00:26:17,200
 reactive towards

475
00:26:17,200 --> 00:26:25,840
 these things am I more objective about my experience more

476
00:26:25,840 --> 00:26:26,660
 peaceful in my

477
00:26:26,660 --> 00:26:30,480
 interactions with things that's where you see results of

478
00:26:30,480 --> 00:26:32,280
 course the the thrust of

479
00:26:32,280 --> 00:26:39,350
 the verse and the story is that really the most likely way

480
00:26:39,350 --> 00:26:40,880
 of attaining any

481
00:26:40,880 --> 00:26:46,400
 real results is again to teach to treat Buddhist practice

482
00:26:46,400 --> 00:26:49,280
 as a life goal not as

483
00:26:49,280 --> 00:26:52,880
 a hobby known as something you do and it's convenient but

484
00:26:52,880 --> 00:26:54,160
 something that you

485
00:26:54,160 --> 00:27:00,120
 engage in day and night sadhā-jāgaramānana for those who

486
00:27:00,120 --> 00:27:04,640
 are always awake so

487
00:27:04,640 --> 00:27:10,000
 that's the Dhammapada for today thank you all for listening

