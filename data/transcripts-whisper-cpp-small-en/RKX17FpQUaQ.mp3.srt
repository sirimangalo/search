1
00:00:00,000 --> 00:00:08,870
 The main goal of meditation, as I've said before, is

2
00:00:08,870 --> 00:00:15,520
 enlightenment or freedom.

3
00:00:15,520 --> 00:00:20,240
 The word enlightenment is an interesting word.

4
00:00:20,240 --> 00:00:21,920
 It's of course an English word.

5
00:00:21,920 --> 00:00:27,290
 It's not exactly the word that we use when we use talking

6
00:00:27,290 --> 00:00:29,600
 to other languages.

7
00:00:29,600 --> 00:00:33,650
 But it's an interesting word because if you look at the

8
00:00:33,650 --> 00:00:35,600
 actual word itself, it could mean

9
00:00:35,600 --> 00:00:42,290
 one of two things, being bright, being removed or taken out

10
00:00:42,290 --> 00:00:45,920
 of the darkness, enlightening

11
00:00:45,920 --> 00:00:50,520
 oneself in terms of gaining wisdom or understanding or

12
00:00:50,520 --> 00:00:54,160
 light, and the other one becoming lighter

13
00:00:54,160 --> 00:00:57,600
 or giving up one's burdens.

14
00:00:57,600 --> 00:01:02,400
 And both of these definitions I think are quite apt for the

15
00:01:02,400 --> 00:01:04,560
 meaning, to explain the

16
00:01:04,560 --> 00:01:08,810
 meaning of the result of meditation, the fruit of

17
00:01:08,810 --> 00:01:12,360
 meditation which we're all wishing to,

18
00:01:12,360 --> 00:01:15,920
 hoping to attain.

19
00:01:15,920 --> 00:01:20,240
 The first one is because we're practicing to gain wisdom.

20
00:01:20,240 --> 00:01:23,600
 We're not practicing just to sit around and feel peaceful

21
00:01:23,600 --> 00:01:25,120
 or to gain a break or a rest

22
00:01:25,120 --> 00:01:28,520
 from our troubles for a while.

23
00:01:28,520 --> 00:01:32,750
 As I've said before, we're looking to really and truly

24
00:01:32,750 --> 00:01:35,480
 understand how our reality works,

25
00:01:35,480 --> 00:01:41,010
 how our mind works, how our physical surroundings work and

26
00:01:41,010 --> 00:01:43,760
 how we interact with them.

27
00:01:43,760 --> 00:01:46,210
 And coming to see things as they are, coming to see the

28
00:01:46,210 --> 00:01:48,000
 truth about our emotions, the truth

29
00:01:48,000 --> 00:01:51,050
 about our obsessions, the truth about our aversions, the

30
00:01:51,050 --> 00:01:52,520
 truth about our fears and our

31
00:01:52,520 --> 00:01:54,560
 worries and our concerns.

32
00:01:54,560 --> 00:01:57,360
 And coming to see the true nature of the things which we

33
00:01:57,360 --> 00:01:59,120
 worry and concern and are afraid

34
00:01:59,120 --> 00:02:02,880
 and depressed and confused about.

35
00:02:02,880 --> 00:02:06,560
 And once we come to see these things clearly, of course, we

36
00:02:06,560 --> 00:02:08,960
're able to approach every experience

37
00:02:08,960 --> 00:02:11,680
 in a much more enlightened manner.

38
00:02:11,680 --> 00:02:15,280
 It's like it feels like, or it appears to the meditator as

39
00:02:15,280 --> 00:02:17,020
 something akin to a person

40
00:02:17,020 --> 00:02:19,980
 coming out of a dark room, where before they were in total

41
00:02:19,980 --> 00:02:21,840
 darkness, they were living their

42
00:02:21,840 --> 00:02:25,330
 lives bumping into things, going around like a person in a

43
00:02:25,330 --> 00:02:28,480
 dark room or a person blindfolded.

44
00:02:28,480 --> 00:02:34,190
 And so they would act like a chicken with its head cut off

45
00:02:34,190 --> 00:02:37,240
 or an animal that has no intelligence

46
00:02:37,240 --> 00:02:39,520
 and no wisdom, no understanding.

47
00:02:39,520 --> 00:02:41,690
 And after they practice meditation, it's like suddenly

48
00:02:41,690 --> 00:02:43,040
 someone turned on the light and they're

49
00:02:43,040 --> 00:02:47,640
 able to see and they're able to avoid dangers and avoid

50
00:02:47,640 --> 00:02:50,900
 suffering because they see the truth

51
00:02:50,900 --> 00:02:51,900
 of the situation.

52
00:02:51,900 --> 00:02:54,760
 They see things as they are and how they work.

53
00:02:54,760 --> 00:02:58,040
 They see their own emotions and so on.

54
00:02:58,040 --> 00:03:03,770
 The other definition, equally apt, but for a totally

55
00:03:03,770 --> 00:03:07,360
 different reason, is the idea of

56
00:03:07,360 --> 00:03:10,040
 giving up one's burdens.

57
00:03:10,040 --> 00:03:14,060
 And they're sort of intertwined, but the idea is here that

58
00:03:14,060 --> 00:03:16,320
 we're giving up our attachments

59
00:03:16,320 --> 00:03:21,170
 or our predilections, our obsessions with things, whereas

60
00:03:21,170 --> 00:03:23,400
 normally we carry around so

61
00:03:23,400 --> 00:03:24,840
 much baggage with us.

62
00:03:24,840 --> 00:03:28,250
 We talk about emotional baggage, but we can also think of

63
00:03:28,250 --> 00:03:30,240
 all of our physical possessions

64
00:03:30,240 --> 00:03:34,410
 and the people and the places and the things which we cling

65
00:03:34,410 --> 00:03:36,160
 to as a kind of baggage or

66
00:03:36,160 --> 00:03:41,320
 a burden that we have to carry around and care for and

67
00:03:41,320 --> 00:03:43,080
 obsess about.

68
00:03:43,080 --> 00:03:46,760
 If we take even just the body, we're spending all of our

69
00:03:46,760 --> 00:03:49,280
 time grooming and cleaning and fussing

70
00:03:49,280 --> 00:03:52,600
 over our body, making sure that it's in perfect health, in

71
00:03:52,600 --> 00:03:54,920
 perfect condition, perfect weight,

72
00:03:54,920 --> 00:03:58,070
 perfect color and so on, even to the point of our hair and

73
00:03:58,070 --> 00:03:59,560
 our clothes and so on.

74
00:03:59,560 --> 00:04:01,640
 We have to decorate ourselves up.

75
00:04:01,640 --> 00:04:02,880
 It becomes a real burden.

76
00:04:02,880 --> 00:04:07,580
 It's something that we have to worry and fret over and

77
00:04:07,580 --> 00:04:09,000
 stress over.

78
00:04:09,000 --> 00:04:10,720
 All of the people around us are the same.

79
00:04:10,720 --> 00:04:14,610
 They become a burden when we are concerned and worried and

80
00:04:14,610 --> 00:04:16,840
 our happiness depends on theirs

81
00:04:16,840 --> 00:04:20,880
 or our happiness depends on their appearance and on their

82
00:04:20,880 --> 00:04:21,840
 behavior.

83
00:04:21,840 --> 00:04:27,830
 Our happiness depends on our friendship with them when we

84
00:04:27,830 --> 00:04:30,740
 are dependent on them acting

85
00:04:30,740 --> 00:04:32,880
 in a certain way and not acting in another way.

86
00:04:32,880 --> 00:04:33,880
 This is a real burden.

87
00:04:33,880 --> 00:04:34,880
 It's a weight on us.

88
00:04:34,880 --> 00:04:38,500
 When we are able to give this up and be without the most

89
00:04:38,500 --> 00:04:41,160
 beautiful body or the most beautiful

90
00:04:41,160 --> 00:04:46,030
 clothes or the greatest friends or without people who are

91
00:04:46,030 --> 00:04:48,080
 pleasing to us, when we are

92
00:04:48,080 --> 00:04:52,280
 able to just be and let things come and go, when our

93
00:04:52,280 --> 00:04:55,200
 possessions are not so important.

94
00:04:55,200 --> 00:04:57,350
 Of course, when we are able to give up our emotional

95
00:04:57,350 --> 00:04:58,840
 baggage as well once we gain wisdom

96
00:04:58,840 --> 00:05:04,500
 and understanding, obviously the other important aspect of

97
00:05:04,500 --> 00:05:07,680
 wisdom, meaning that wisdom is in

98
00:05:07,680 --> 00:05:11,340
 and of itself not truly a valid goal because just because

99
00:05:11,340 --> 00:05:13,240
 you are wise doesn't in and of

100
00:05:13,240 --> 00:05:15,880
 itself mean happiness.

101
00:05:15,880 --> 00:05:19,440
 But it necessitates happiness because it relieves the

102
00:05:19,440 --> 00:05:20,200
 burden.

103
00:05:20,200 --> 00:05:23,310
 After you have wisdom and understanding, you are freed from

104
00:05:23,310 --> 00:05:24,920
 this whole emotional baggage

105
00:05:24,920 --> 00:05:28,000
 that comes from our obsessions and our attachments and

106
00:05:28,000 --> 00:05:28,640
 things.

107
00:05:28,640 --> 00:05:33,000
 We have an understanding of the situations around us.

108
00:05:33,000 --> 00:05:35,080
 We approach things without any emotional baggage.

109
00:05:35,080 --> 00:05:39,080
 We approach things with wisdom, with a clear sense of how

110
00:05:39,080 --> 00:05:41,440
 things are and what things are,

111
00:05:41,440 --> 00:05:46,080
 a clear sense of the nature of the situation.

112
00:05:46,080 --> 00:05:52,150
 So we live our lives free from all of the baggage as we

113
00:05:52,150 --> 00:05:55,060
 become enlightened.

114
00:05:55,060 --> 00:05:57,920
 So I wanted to explain this to sort of give an idea that

115
00:05:57,920 --> 00:05:59,760
 enlightenment is not something

116
00:05:59,760 --> 00:06:03,040
 foreign, it's not something mystical or magical.

117
00:06:03,040 --> 00:06:06,410
 Even the idea of nirvana or nirvana is simply freedom from

118
00:06:06,410 --> 00:06:08,880
 suffering because of enlightenment,

119
00:06:08,880 --> 00:06:13,620
 because one has become freed of one's burdens, because one

120
00:06:13,620 --> 00:06:16,080
 has become wise and has turned

121
00:06:16,080 --> 00:06:21,260
 on the light in their mind, has gained the brilliance of

122
00:06:21,260 --> 00:06:22,320
 wisdom.

123
00:06:22,320 --> 00:06:25,160
 It's something that comes from simply observing, simply

124
00:06:25,160 --> 00:06:27,160
 watching, learning, looking deeper

125
00:06:27,160 --> 00:06:34,060
 and looking in a clear, empirical, experimental way where

126
00:06:34,060 --> 00:06:39,200
 we accept things or acknowledge things

127
00:06:39,200 --> 00:06:40,360
 for what they are.

128
00:06:40,360 --> 00:06:44,010
 So as we explained in meditation, when we feel pain, we

129
00:06:44,010 --> 00:06:45,500
 just know it as pain.

130
00:06:45,500 --> 00:06:47,870
 When we feel the movements of the body, even we see them

131
00:06:47,870 --> 00:06:49,520
 just as the movements of the body.

132
00:06:49,520 --> 00:06:51,900
 When we're thinking, we see it just as thinking.

133
00:06:51,900 --> 00:06:53,960
 When we have emotions, we see them just as emotions.

134
00:06:53,960 --> 00:06:57,110
 When we see, when we hear, when we smell, when we taste,

135
00:06:57,110 --> 00:06:58,880
 when we feel and we think things,

136
00:06:58,880 --> 00:07:01,320
 we see it simply for what it is, as it is.

137
00:07:01,320 --> 00:07:04,790
 And as we look closer and closer, as we see clearer and

138
00:07:04,790 --> 00:07:07,080
 clearer, wisdom can't but arise

139
00:07:07,080 --> 00:07:09,460
 because we're looking at things simply for what they are.

140
00:07:09,460 --> 00:07:13,800
 We have an objective outlook and we are objectively

141
00:07:13,800 --> 00:07:17,840
 studying, analyzing and learning more about

142
00:07:17,840 --> 00:07:23,120
 ourselves, more about the phenomena that arise in our

143
00:07:23,120 --> 00:07:25,600
 everyday experience.

144
00:07:25,600 --> 00:07:28,240
 So I hope this has been of some help, just in sort of an

145
00:07:28,240 --> 00:07:29,760
 addition to the videos on how

146
00:07:29,760 --> 00:07:31,120
 to meditate.

147
00:07:31,120 --> 00:07:35,820
 One more in this series on how and why everyone should

148
00:07:35,820 --> 00:07:38,120
 practice meditation.

149
00:07:38,120 --> 00:07:41,820
 So thanks for tuning in and we'll look forward to more

150
00:07:41,820 --> 00:07:43,560
 videos in the future.

151
00:07:43,560 --> 00:07:43,960
 Thanks, have a good day.

152
00:07:43,960 --> 00:08:00,040
 [

