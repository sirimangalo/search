1
00:00:00,000 --> 00:00:06,000
 Good evening.

2
00:00:06,000 --> 00:00:11,000
 Tonight we are continuing our study of the Dhammapada.

3
00:00:11,000 --> 00:00:20,000
 Looking at verse 2-12, which reads as follows.

4
00:00:20,000 --> 00:00:35,000
 Piyatodayati sokho, piyatodayati bhayam.

5
00:00:35,000 --> 00:00:43,000
 Piyatodvipamuntasa nati sokho kutoh bhayam.

6
00:00:43,000 --> 00:00:53,490
 Which means, "From what is dear, from what is cherished,

7
00:00:53,490 --> 00:00:56,000
 comes sorrow.

8
00:00:56,000 --> 00:01:03,000
 From what is cherished comes fear.

9
00:01:03,000 --> 00:01:13,000
 For one who is freed from holding things from cherishing,

10
00:01:13,000 --> 00:01:17,000
 there is no sorrow whence fear."

11
00:01:17,000 --> 00:01:26,000
 Meaning, how could there be fear?

12
00:01:26,000 --> 00:01:36,800
 So this verse was taught in regards to a man who had lost

13
00:01:36,800 --> 00:01:39,000
 his son.

14
00:01:39,000 --> 00:01:43,000
 Very simple story. There's not much to it.

15
00:01:43,000 --> 00:01:46,470
 The story goes that there was this man in the time of the

16
00:01:46,470 --> 00:01:47,000
 Buddha.

17
00:01:47,000 --> 00:01:49,000
 I think we've had stories like this.

18
00:01:49,000 --> 00:01:54,880
 In fact, the second verse of the Dhammapada is a very

19
00:01:54,880 --> 00:01:57,000
 similar story.

20
00:01:57,000 --> 00:02:03,300
 This man lost his son and just spent all his time wailing

21
00:02:03,300 --> 00:02:10,000
 and moaning and bemoaning his loss.

22
00:02:10,000 --> 00:02:12,000
 Losing a child, no?

23
00:02:12,000 --> 00:02:19,000
 It's one of the things we never wish, or not never wish,

24
00:02:19,000 --> 00:02:24,000
 that it's hard to beat the loss of a child.

25
00:02:24,000 --> 00:02:28,960
 Meaning, the loss of a parent is sad, but something you can

26
00:02:28,960 --> 00:02:30,000
 live with.

27
00:02:30,000 --> 00:02:35,980
 The worst, the most dreadful thing is the loss of one's

28
00:02:35,980 --> 00:02:42,090
 child, which should never happen in the grand scheme of

29
00:02:42,090 --> 00:02:52,000
 things, if there were a grand scheme.

30
00:02:52,000 --> 00:02:56,210
 The Buddha found out about this man and thought to himself,

31
00:02:56,210 --> 00:03:00,510
 "Father, someone who might be able to understand the Dhamma

32
00:03:00,510 --> 00:03:01,000
."

33
00:03:01,000 --> 00:03:05,030
 We hear these stories and it sounds like we Buddhists are

34
00:03:05,030 --> 00:03:09,000
 always keen to find those people who are miserable.

35
00:03:09,000 --> 00:03:14,000
 We think, "Boy, they'd make a good Buddhist."

36
00:03:14,000 --> 00:03:18,930
 It's not really fair, but it becomes suspiciously like that

37
00:03:18,930 --> 00:03:24,000
, where you think religion might be leveled this accusation,

38
00:03:24,000 --> 00:03:29,650
 that religion preys upon those who are desperate or those

39
00:03:29,650 --> 00:03:32,000
 who are vulnerable.

40
00:03:32,000 --> 00:03:35,150
 It certainly is possible a person who is vulnerable can be

41
00:03:35,150 --> 00:03:42,000
 preyed upon, even by people calling themselves religious.

42
00:03:42,000 --> 00:03:47,180
 But that's an uncharitable connection, of course, because

43
00:03:47,180 --> 00:03:54,000
 religion, in most cases, tries to or claims to,

44
00:03:54,000 --> 00:04:00,850
 works towards answering questions that science and society

45
00:04:00,850 --> 00:04:02,000
 can't.

46
00:04:02,000 --> 00:04:05,390
 You bought into society, did everything right, and still

47
00:04:05,390 --> 00:04:07,000
 everything goes wrong.

48
00:04:07,000 --> 00:04:10,000
 What's wrong? So it tries to look beyond.

49
00:04:10,000 --> 00:04:12,000
 It claims that there's something beyond.

50
00:04:12,000 --> 00:04:15,650
 Buddhism as well claims that there's something beyond

51
00:04:15,650 --> 00:04:18,000
 society, even beyond science.

52
00:04:18,000 --> 00:04:22,910
 Science can tell you all sorts of things about the brain

53
00:04:22,910 --> 00:04:24,000
 and the body.

54
00:04:24,000 --> 00:04:34,000
 It can't tell you much about the mind or the psyche.

55
00:04:34,000 --> 00:04:40,000
 So this man was miserable, and he'd done everything right.

56
00:04:40,000 --> 00:04:41,000
 He was, I think, probably quite wealthy,

57
00:04:41,000 --> 00:04:47,680
 and he certainly wasn't destitute, but here he was reduced

58
00:04:47,680 --> 00:04:54,000
 to ruin, not by loss of wealth or loss of status,

59
00:04:54,000 --> 00:04:59,630
 but by a loss of something that he couldn't buy, he couldn

60
00:04:59,630 --> 00:05:07,940
't hold on to with money or power or even intelligence and

61
00:05:07,940 --> 00:05:11,000
 logic and reasoning.

62
00:05:11,000 --> 00:05:16,780
 So it goes beyond that. What do you do when you lose your

63
00:05:16,780 --> 00:05:18,000
 child?

64
00:05:18,000 --> 00:05:23,000
 The Buddha went to him and asked him, "Why are you crying?"

65
00:05:23,000 --> 00:05:28,990
 The man told him, and the Buddha said, "Wise people don't

66
00:05:28,990 --> 00:05:31,000
 act in this way," he said.

67
00:05:31,000 --> 00:05:35,610
 He wasn't critical. I may sound a little harsh when I tell

68
00:05:35,610 --> 00:05:39,000
 it, but he wasn't so harsh.

69
00:05:39,000 --> 00:05:43,230
 He said, "In ancient times, laymen, householder," he would

70
00:05:43,230 --> 00:05:45,000
 call them householder,

71
00:05:45,000 --> 00:05:52,420
 "in ancient times, when a man lost his son, he wouldn't cry

72
00:05:52,420 --> 00:05:53,000
."

73
00:05:53,000 --> 00:05:56,810
 He would think to himself, and here we have a Pali that's

74
00:05:56,810 --> 00:05:59,000
 probably a good one to remember.

75
00:05:59,000 --> 00:06:01,000
 I was just trying to...

76
00:06:01,000 --> 00:06:12,660
 "Marana Dhammang Matan, Bijana Dhammang Bindang," which

77
00:06:12,660 --> 00:06:15,000
 means what is of a nature to die has died.

78
00:06:15,000 --> 00:06:22,000
 What is of a nature to break apart has broken.

79
00:06:22,000 --> 00:06:32,410
 He taught this in reference, and then he taught also the Ur

80
00:06:32,410 --> 00:06:34,000
agataka.

81
00:06:34,000 --> 00:06:39,000
 Uraga means snake. It's the snake jataka.

82
00:06:39,000 --> 00:06:42,990
 It only tangentially has to do with the snake because this

83
00:06:42,990 --> 00:06:45,000
 man loses his son to a snake.

84
00:06:45,000 --> 00:06:47,000
 A snake bites his son.

85
00:06:47,000 --> 00:06:52,100
 I once met a monk in... I lived with a monk in Sri Lanka, a

86
00:06:52,100 --> 00:06:55,000
 very good close friend when I was there,

87
00:06:55,000 --> 00:06:57,000
 just a great monk.

88
00:06:57,000 --> 00:07:00,880
 When he was young, his sister, his younger sister got

89
00:07:00,880 --> 00:07:03,000
 bitten by a cobra and died.

90
00:07:03,000 --> 00:07:08,000
 This kind of thing happens.

91
00:07:08,000 --> 00:07:15,450
 In the Uragataka, this was the bodhisatta who was the

92
00:07:15,450 --> 00:07:17,000
 father.

93
00:07:17,000 --> 00:07:22,120
 The story tells that since he got married and had kids

94
00:07:22,120 --> 00:07:27,000
 through his whole household career as a father,

95
00:07:27,000 --> 00:07:32,000
 as a head of a household, he taught his whole family.

96
00:07:32,000 --> 00:07:39,650
 They taught each other and they talked constantly about

97
00:07:39,650 --> 00:07:41,000
 death.

98
00:07:41,000 --> 00:07:44,000
 They would remind each other of death and talk about it.

99
00:07:44,000 --> 00:07:49,000
 It was an open and common topic of conversation.

100
00:07:49,000 --> 00:07:53,900
 I find it funny how shocking, the shock value of talking

101
00:07:53,900 --> 00:07:55,000
 about death,

102
00:07:55,000 --> 00:07:59,000
 often when we bring it up and talk about how you might die

103
00:07:59,000 --> 00:08:01,000
 and people are disturbed.

104
00:08:01,000 --> 00:08:05,370
 It's kind of funny because of how taboo it almost... not

105
00:08:05,370 --> 00:08:06,000
 taboo,

106
00:08:06,000 --> 00:08:12,670
 but how impolite it is to talk about death in many cultures

107
00:08:12,670 --> 00:08:15,000
, probably most cultures,

108
00:08:15,000 --> 00:08:23,000
 besides really hardcore Buddhist circles.

109
00:08:23,000 --> 00:08:33,000
 This was probably at the time quite unique or exceptional.

110
00:08:33,000 --> 00:08:35,000
 Today, thinking about it, it's exceptional.

111
00:08:35,000 --> 00:08:38,000
 Talking to your kids about death, what a horrible thing.

112
00:08:38,000 --> 00:08:43,000
 My parents never talked to me about death.

113
00:08:43,000 --> 00:08:47,000
 But as a result, when this son died, if we read the story,

114
00:08:47,000 --> 00:08:48,000
 it's a really good jataka,

115
00:08:48,000 --> 00:08:53,560
 one of the ones worth reading, one of the ones especially

116
00:08:53,560 --> 00:08:55,000
 worth reading,

117
00:08:55,000 --> 00:08:57,000
 especially for the verses.

118
00:08:57,000 --> 00:09:00,610
 He says things like this, "If you spill milk, the milk jug

119
00:09:00,610 --> 00:09:01,000
 breaks."

120
00:09:01,000 --> 00:09:06,740
 Or, "If a pot breaks, you don't cry. Crying won't bring it

121
00:09:06,740 --> 00:09:08,000
 back."

122
00:09:08,000 --> 00:09:10,000
 "Bhijana damang bhindang"

123
00:09:10,000 --> 00:09:15,000
 "What is of a nature to break has broken."

124
00:09:15,000 --> 00:09:19,000
 "Life is of a nature to break."

125
00:09:19,000 --> 00:09:22,140
 He taught the uragajataka, told the story of this man and

126
00:09:22,140 --> 00:09:23,000
 how they burnt the son

127
00:09:23,000 --> 00:09:29,000
 and the son's body and this man came along and said,

128
00:09:29,000 --> 00:09:31,000
 "There must be an enemy of yours you're burning."

129
00:09:31,000 --> 00:09:33,000
 He said, "No, no, this is my son."

130
00:09:33,000 --> 00:09:36,400
 He says, "What? How could this be? You're all here very

131
00:09:36,400 --> 00:09:38,000
 peaceful and serene

132
00:09:38,000 --> 00:09:40,000
 and not crying and not upset."

133
00:09:40,000 --> 00:09:45,000
 This man talks to all the family and they all recite verses

134
00:09:45,000 --> 00:09:46,000
 and say,

135
00:09:46,000 --> 00:09:50,540
 "If you call the moon down, wish for the moon and yell at

136
00:09:50,540 --> 00:09:52,000
 the moon to come down

137
00:09:52,000 --> 00:09:59,000
 and it won't come. You can't bring my son back."

138
00:09:59,000 --> 00:10:04,000
 Each verse had a different way of putting it.

139
00:10:04,000 --> 00:10:07,010
 So he told the uragajataka here and then taught this verse,

140
00:10:07,010 --> 00:10:09,000
 a very simple story.

141
00:10:09,000 --> 00:10:12,330
 As a result of teaching the verse, I think the man became a

142
00:10:12,330 --> 00:10:13,000
 sotapanai.

143
00:10:13,000 --> 00:10:17,000
 He was able to gain a new perspective on things.

144
00:10:17,000 --> 00:10:19,370
 So he was someone, the Buddha picked out these people who

145
00:10:19,370 --> 00:10:21,000
 were just right.

146
00:10:21,000 --> 00:10:26,010
 Not like us, we all have to work very, very hard to get

147
00:10:26,010 --> 00:10:27,000
 anywhere.

148
00:10:27,000 --> 00:10:29,560
 The Buddha picked the low-hanging fruit, the people who

149
00:10:29,560 --> 00:10:31,000
 were ready because...

150
00:10:31,000 --> 00:10:34,000
 It's a good example, teaching those people who are ready

151
00:10:34,000 --> 00:10:38,000
 and are interested in learning.

152
00:10:38,000 --> 00:10:42,450
 Instead of trying to encourage people who don't want to

153
00:10:42,450 --> 00:10:45,000
 learn and pull people who don't want to learn,

154
00:10:45,000 --> 00:10:48,290
 often we make that mistake trying to pull in people because

155
00:10:48,290 --> 00:10:52,000
 they're our friends, our family members.

156
00:10:52,000 --> 00:10:57,000
 It's a lot of work for very little benefit.

157
00:10:57,000 --> 00:11:01,000
 Whereas instead if you found the people who wanted to come

158
00:11:01,000 --> 00:11:01,000
 and were...

159
00:11:01,000 --> 00:11:03,610
 Everyone would think they were crazy but they're crazy

160
00:11:03,610 --> 00:11:07,000
 enough to come to a place like this

161
00:11:07,000 --> 00:11:10,310
 to dedicate themselves to walking back and forth and

162
00:11:10,310 --> 00:11:17,000
 sitting very still, repeating mantras in their heads.

163
00:11:17,000 --> 00:11:20,610
 If we focus on those people, we have much better results

164
00:11:20,610 --> 00:11:23,000
 for much less effort and so it's efficient.

165
00:11:23,000 --> 00:11:26,490
 And those people go out and help other people and it

166
00:11:26,490 --> 00:11:28,000
 spreads that way.

167
00:11:28,000 --> 00:11:30,470
 Buddhism has never been a proselytizing religion for that

168
00:11:30,470 --> 00:11:31,000
 reason.

169
00:11:31,000 --> 00:11:34,000
 It's a difficult thing to practice Buddhism.

170
00:11:34,000 --> 00:11:37,010
 We teach those people who are ready, who are interested,

171
00:11:37,010 --> 00:11:42,000
 who are often miserable but most important are sensitive.

172
00:11:42,000 --> 00:11:45,940
 And a person who is on the upswing, who has everything good

173
00:11:45,940 --> 00:11:47,000
 in their lives,

174
00:11:47,000 --> 00:11:50,320
 is often very insensitive to the idea that something might

175
00:11:50,320 --> 00:11:54,000
 go wrong, that something might break.

176
00:11:54,000 --> 00:11:58,140
 Sometimes again the difference between brains even,

177
00:11:58,140 --> 00:12:01,000
 apparently our brains are different which makes sense.

178
00:12:01,000 --> 00:12:07,300
 Meaning some people are much more susceptible to suffering

179
00:12:07,300 --> 00:12:09,000
 than others.

180
00:12:09,000 --> 00:12:12,000
 Something goes wrong, they react more negatively.

181
00:12:12,000 --> 00:12:16,000
 Other people are insulated from it.

182
00:12:16,000 --> 00:12:18,550
 Meaning something bad happens and they're very quick to

183
00:12:18,550 --> 00:12:19,000
 rebound.

184
00:12:19,000 --> 00:12:21,530
 It's just based on how the brain works and of course you

185
00:12:21,530 --> 00:12:23,000
 can train that to some extent.

186
00:12:23,000 --> 00:12:27,330
 But to some extent it's organic, it's the way the brain is

187
00:12:27,330 --> 00:12:29,000
 composed and so on.

188
00:12:29,000 --> 00:12:32,000
 So it appears to the scientist.

189
00:12:32,000 --> 00:12:36,000
 I mean it seems reasonable.

190
00:12:36,000 --> 00:12:42,230
 But so those people for whom it's very hard to see the

191
00:12:42,230 --> 00:12:43,000
 negative, right?

192
00:12:43,000 --> 00:12:46,000
 If something bad happens you forget about it very quickly.

193
00:12:46,000 --> 00:12:48,860
 It's very hard to see the potential for disaster because

194
00:12:48,860 --> 00:12:50,000
 there isn't really.

195
00:12:50,000 --> 00:12:54,000
 You're insulated temporarily of course.

196
00:12:54,000 --> 00:12:58,230
 The bigger picture is really what this verse is, part of

197
00:12:58,230 --> 00:13:01,000
 what this verse is talking about.

198
00:13:01,000 --> 00:13:04,000
 So the verse has three key words.

199
00:13:04,000 --> 00:13:07,000
 The first one is cherished, what is cherished.

200
00:13:07,000 --> 00:13:12,000
 The second one is sadness and the third is fear.

201
00:13:12,000 --> 00:13:18,900
 And the idea that something that is cherished doesn't lead

202
00:13:18,900 --> 00:13:20,000
 to happiness,

203
00:13:20,000 --> 00:13:24,000
 doesn't lead to contentment and bliss and a good life,

204
00:13:24,000 --> 00:13:26,000
 doesn't lead to goodness.

205
00:13:26,000 --> 00:13:31,420
 We kind of have this idea I suppose that the things that we

206
00:13:31,420 --> 00:13:32,000
 like,

207
00:13:32,000 --> 00:13:35,290
 the things that bring us happiness, that happiness is

208
00:13:35,290 --> 00:13:36,000
 goodness.

209
00:13:36,000 --> 00:13:39,150
 We put the cart before the horse whereas in Buddhism

210
00:13:39,150 --> 00:13:41,000
 goodness leads to happiness.

211
00:13:41,000 --> 00:13:44,000
 We seek out happiness in the world.

212
00:13:44,000 --> 00:13:46,000
 In Buddhism it's not that way.

213
00:13:46,000 --> 00:13:48,650
 In Buddhism we see through that and see that seeking out

214
00:13:48,650 --> 00:13:50,000
 happiness isn't good.

215
00:13:50,000 --> 00:13:57,080
 Happiness doesn't lead to goodness, doesn't even lead to

216
00:13:57,080 --> 00:14:00,000
 happiness.

217
00:14:00,000 --> 00:14:03,000
 But we cherish things and this is the idea behind the story

218
00:14:03,000 --> 00:14:07,000
 and this whole chapter of course is

219
00:14:07,000 --> 00:14:10,000
 a chapter on things that are cherished.

220
00:14:10,000 --> 00:14:14,540
 So putting stress, giving it a whole chapter gives us this

221
00:14:14,540 --> 00:14:19,000
 idea that there was a recognition of how important it is,

222
00:14:19,000 --> 00:14:24,210
 this concept of things that we cherish, concept of holding

223
00:14:24,210 --> 00:14:25,000
 things dear,

224
00:14:25,000 --> 00:14:28,440
 which ultimately comes down to what we like and it's this

225
00:14:28,440 --> 00:14:36,000
 idea of craving and clinging and addiction and so on.

226
00:14:36,000 --> 00:14:40,460
 So what I think is interesting about this verse and this

227
00:14:40,460 --> 00:14:43,000
 teaching is what we talked about last week

228
00:14:43,000 --> 00:14:48,090
 and we'll talk about for the... well not next week but for

229
00:14:48,090 --> 00:14:53,000
 quite a few verses for this whole chapter probably.

230
00:14:53,000 --> 00:14:59,210
 But what is interesting is the separation here between the

231
00:14:59,210 --> 00:15:02,000
 distinction between sorrow and fear,

232
00:15:02,000 --> 00:15:05,000
 which I think is interesting.

233
00:15:05,000 --> 00:15:20,350
 So the idea that holding something dear could lead to

234
00:15:20,350 --> 00:15:22,000
 sorrow,

235
00:15:22,000 --> 00:15:32,020
 the clear idea is that when you want, when you like things,

236
00:15:32,020 --> 00:15:33,000
 when there are things that you hold dear,

237
00:15:33,000 --> 00:15:39,050
 you might lose them and if you lose them, that would be

238
00:15:39,050 --> 00:15:41,000
 great suffering.

239
00:15:41,000 --> 00:15:44,130
 And this of course applies to people, it applies to things,

240
00:15:44,130 --> 00:15:46,000
 it's the idea, sort of the concept,

241
00:15:46,000 --> 00:15:48,600
 one of the concepts of suffering in Buddhism that you might

242
00:15:48,600 --> 00:15:52,000
 lose it and if you lose it, that would be suffering.

243
00:15:52,000 --> 00:16:00,060
 But the idea of fear and here fear is the same word that

244
00:16:00,060 --> 00:16:03,000
 they use for danger.

245
00:16:03,000 --> 00:16:06,030
 And it seems a little bit odd because in English of course

246
00:16:06,030 --> 00:16:09,000
 they're two very different words, fear and danger,

247
00:16:09,000 --> 00:16:13,750
 but the idea, the commonality behind them is the

248
00:16:13,750 --> 00:16:21,000
 anticipation or the concern for the potential of suffering.

249
00:16:21,000 --> 00:16:24,530
 When you fear something, you don't fear what is happening

250
00:16:24,530 --> 00:16:25,000
 to you, right?

251
00:16:25,000 --> 00:16:29,220
 If you're in great pain, you're not afraid of the pain, you

252
00:16:29,220 --> 00:16:32,000
're afraid of what might come as a result of the pain.

253
00:16:32,000 --> 00:16:36,000
 You might say it's a disliking or an aversion to a concept.

254
00:16:36,000 --> 00:16:41,100
 So fear isn't actually, I don't think, it's not a real

255
00:16:41,100 --> 00:16:45,000
 thing, it's not a real experience.

256
00:16:45,000 --> 00:16:48,150
 But when we experience fear, what we call something fear,

257
00:16:48,150 --> 00:16:51,000
 what we're experiencing is a concept, a thought.

258
00:16:51,000 --> 00:16:55,000
 Suppose there's a spider and you're afraid of the spider,

259
00:16:55,000 --> 00:16:57,000
 you're not afraid of the spider,

260
00:16:57,000 --> 00:17:00,280
 you're afraid of, you have a concept of it jumping you, it

261
00:17:00,280 --> 00:17:05,000
 attacking you, biting you or crawling on you even.

262
00:17:05,000 --> 00:17:13,000
 And that fear is of the anticipation.

263
00:17:13,000 --> 00:17:16,410
 That's not even quite, it's more complicated because you

264
00:17:16,410 --> 00:17:20,600
 can see a spider on you and just be freaked out by the

265
00:17:20,600 --> 00:17:21,000
 touch,

266
00:17:21,000 --> 00:17:24,240
 but it's still very much associated with the idea that the

267
00:17:24,240 --> 00:17:27,000
 spider might bite you or that sort of thing.

268
00:17:27,000 --> 00:17:32,540
 Anyway, the point is ultimately it comes down to a simple

269
00:17:32,540 --> 00:17:34,000
 mind state.

270
00:17:34,000 --> 00:17:37,990
 But what's interesting about it is that you can have all

271
00:17:37,990 --> 00:17:40,000
 the things that you love,

272
00:17:40,000 --> 00:17:44,710
 you can be surrounded by the people that you love and still

273
00:17:44,710 --> 00:17:46,000
 suffer.

274
00:17:46,000 --> 00:17:48,610
 And suffer not because you've lost something, not because

275
00:17:48,610 --> 00:17:50,000
 you can't get what you want,

276
00:17:50,000 --> 00:17:53,060
 not because you've gotten what you don't want, but simply

277
00:17:53,060 --> 00:17:57,000
 because you could lose what you want.

278
00:17:57,000 --> 00:18:02,150
 And this is something that makes meditation stand out,

279
00:18:02,150 --> 00:18:04,000
 mindfulness stand out,

280
00:18:04,000 --> 00:18:09,550
 because of the insight that it gives into the suffering

281
00:18:09,550 --> 00:18:14,000
 inherent in having things that you cherish.

282
00:18:14,000 --> 00:18:17,210
 That it's not just the sadness that comes from not getting

283
00:18:17,210 --> 00:18:20,000
 what you want, but it's the constant,

284
00:18:20,000 --> 00:18:27,600
 not constant, but ever present, incessant and repeated and

285
00:18:27,600 --> 00:18:33,000
 regular appearance of terror, of fear, of worry.

286
00:18:33,000 --> 00:18:39,000
 The concern that you might lose what you have.

287
00:18:39,000 --> 00:18:43,020
 Sometimes this is why, of course, thinking about death is

288
00:18:43,020 --> 00:18:46,000
 so powerful, why it's important for us to think about death

289
00:18:46,000 --> 00:18:46,000
,

290
00:18:46,000 --> 00:18:48,600
 because death is sort of the biggest thing that we might

291
00:18:48,600 --> 00:18:51,000
 fear, but there's many others.

292
00:18:51,000 --> 00:18:54,400
 Think about all the things that you own. Think about your

293
00:18:54,400 --> 00:18:57,000
 house, your car, think about your family,

294
00:18:57,000 --> 00:19:00,850
 more deeply valued things, your relatives. What if your

295
00:19:00,850 --> 00:19:04,000
 parents died? What if your children died?

296
00:19:04,000 --> 00:19:07,490
 What if your siblings and friends and lovers? What if they

297
00:19:07,490 --> 00:19:09,000
 all died?

298
00:19:09,000 --> 00:19:13,400
 What if you lost your health? What if you got sick? What if

299
00:19:13,400 --> 00:19:15,000
 you got in a car accident?

300
00:19:15,000 --> 00:19:18,710
 What if you lost all your money? I mean, many things,

301
00:19:18,710 --> 00:19:21,000
 anything that we hold dear.

302
00:19:21,000 --> 00:19:24,000
 What if suddenly you were bankrupt, you had no money?

303
00:19:24,000 --> 00:19:27,730
 Or suddenly this house burned down and we were all here,

304
00:19:27,730 --> 00:19:30,000
 regardless of what future aid we might get,

305
00:19:30,000 --> 00:19:37,670
 we were all forced to go and walk out in the cold and the

306
00:19:37,670 --> 00:19:39,000
 snow.

307
00:19:39,000 --> 00:19:42,300
 Bringing up, I mean, not everything, many things that would

308
00:19:42,300 --> 00:19:50,000
 like this, you bring them up to someone and it scares them.

309
00:19:50,000 --> 00:19:56,610
 And this points to something very important, I think, that

310
00:19:56,610 --> 00:20:00,000
 meditation, mindfulness shows us

311
00:20:00,000 --> 00:20:04,740
 the true nature of cherishing, the true nature of holding

312
00:20:04,740 --> 00:20:07,000
 things dear, the state,

313
00:20:07,000 --> 00:20:11,680
 the truth about this state of craving things and clinging

314
00:20:11,680 --> 00:20:14,000
 to things and loving things.

315
00:20:14,000 --> 00:20:20,240
 And that that love is always going to be tainted with fear,

316
00:20:20,240 --> 00:20:22,000
 with danger.

317
00:20:22,000 --> 00:20:27,290
 Not just danger, I think fear is here more relevant because

318
00:20:27,290 --> 00:20:29,000
 someone can be oblivious to the danger.

319
00:20:29,000 --> 00:20:36,340
 But when someone fears losing what they have, it's almost

320
00:20:36,340 --> 00:20:40,000
 as bad or perhaps as bad as actually losing the thing

321
00:20:40,000 --> 00:20:46,000
 because it's constant, it's incessant.

322
00:20:46,000 --> 00:20:52,410
 And so in our meditation we see the not only potential for

323
00:20:52,410 --> 00:20:57,390
 loss, but the very real stress that comes from holding onto

324
00:20:57,390 --> 00:20:58,000
 things.

325
00:20:58,000 --> 00:21:02,000
 Sometimes you want something, you can't get it, you suffer.

326
00:21:02,000 --> 00:21:06,560
 But sometimes you have something, you're afraid you might

327
00:21:06,560 --> 00:21:13,000
 lose it, you have stress over it or worry over keeping it.

328
00:21:13,000 --> 00:21:20,110
 And it goes beyond fear as well, it goes to the simple

329
00:21:20,110 --> 00:21:24,000
 stress involved with wanting things.

330
00:21:24,000 --> 00:21:27,430
 When you want something and then you're concerned about

331
00:21:27,430 --> 00:21:30,770
 getting it or worried about how to get it or have to think

332
00:21:30,770 --> 00:21:32,000
 about how to get it.

333
00:21:32,000 --> 00:21:35,000
 Many, many things that you see in meditation.

334
00:21:35,000 --> 00:21:43,210
 And what I think also bears noting is how ignorant we are

335
00:21:43,210 --> 00:21:47,000
 to this without the practice of mindfulness.

336
00:21:47,000 --> 00:21:54,610
 It almost seems like a silly thing to say that things you

337
00:21:54,610 --> 00:22:00,030
 have you might lose them and that there's this fear

338
00:22:00,030 --> 00:22:05,370
 associated because our perspective of thinking that

339
00:22:05,370 --> 00:22:08,000
 happiness comes from getting what you want.

340
00:22:08,000 --> 00:22:12,100
 Not the Buddhist perspective, the ordinary perspective of

341
00:22:12,100 --> 00:22:15,700
 thinking, if I get what I want, if I work things out in

342
00:22:15,700 --> 00:22:21,150
 life and work hard and get everything okay, then I'll be

343
00:22:21,150 --> 00:22:22,000
 happy.

344
00:22:22,000 --> 00:22:28,000
 This idea has an inherent and implicit assumption.

345
00:22:28,000 --> 00:22:31,510
 So it's something we already know and kind of assume

346
00:22:31,510 --> 00:22:34,880
 generally, I think, that it's precarious, that there is

347
00:22:34,880 --> 00:22:37,000
 always going to be this concern.

348
00:22:37,000 --> 00:22:42,310
 Because part of getting what you want means keeping what

349
00:22:42,310 --> 00:22:45,000
 you want, what you like.

350
00:22:45,000 --> 00:22:49,150
 And keeping what you like involves some stress, some work,

351
00:22:49,150 --> 00:22:51,000
 some worry, some fear.

352
00:22:51,000 --> 00:22:55,000
 You have to protect it.

353
00:22:55,000 --> 00:22:59,140
 And so the point in that, what I mean to say is this kind

354
00:22:59,140 --> 00:23:03,530
 of happiness, the happiness of ordinary society where

355
00:23:03,530 --> 00:23:07,380
 people have houses and they have jobs and they have

356
00:23:07,380 --> 00:23:11,220
 families and so on, is always going to be inferior, is

357
00:23:11,220 --> 00:23:14,000
 always going to be lacking.

358
00:23:14,000 --> 00:23:17,160
 And when people say they're happy in life, it's really

359
00:23:17,160 --> 00:23:20,130
 simply because they haven't really been mindful and their

360
00:23:20,130 --> 00:23:23,330
 lack of mindfulness keeps them oblivious to how much stress

361
00:23:23,330 --> 00:23:29,000
 and suffering is involved, how limited their happiness is.

362
00:23:29,000 --> 00:23:32,270
 No matter how happy you can be, you think, "I'm so happy, I

363
00:23:32,270 --> 00:23:34,000
've got everything I want."

364
00:23:34,000 --> 00:23:37,310
 How limited that is. It's limited by sorrow when you lose,

365
00:23:37,310 --> 00:23:40,590
 when you can't get the things you want, when the things you

366
00:23:40,590 --> 00:23:42,000
 want change or so on.

367
00:23:42,000 --> 00:23:49,160
 But it's also the stress of having to keep worrying about

368
00:23:49,160 --> 00:23:54,000
 keeping, worrying about losing.

369
00:23:54,000 --> 00:24:01,360
 And so the end of the verse, "Nati Soko Kuto Bayang" for

370
00:24:01,360 --> 00:24:06,000
 someone who frees themselves, "Wipa Mutta"

371
00:24:06,000 --> 00:24:09,930
 someone who becomes free from cherishing. It's an odd thing

372
00:24:09,930 --> 00:24:13,230
 to say, right? We don't think of ourselves needing to be

373
00:24:13,230 --> 00:24:14,000
 freed from it.

374
00:24:14,000 --> 00:24:20,000
 We leap head first into cherishing. Yes, cherish.

375
00:24:20,000 --> 00:24:24,830
 Dearly beloved. Our whole culture, most cultures in the

376
00:24:24,830 --> 00:24:28,990
 world are about cherishing. Hold on to the things you

377
00:24:28,990 --> 00:24:30,000
 cherish.

378
00:24:30,000 --> 00:24:36,820
 Find something you love. Do something you love. Yeah, not

379
00:24:36,820 --> 00:24:38,000
 in Buddhism.

380
00:24:38,000 --> 00:24:43,720
 In mindfulness we see differently. We come to... we make

381
00:24:43,720 --> 00:24:48,210
 claims about seeing through that and claims about the

382
00:24:48,210 --> 00:24:50,000
 limited nature of cherishing.

383
00:24:50,000 --> 00:24:57,800
 And more importantly for this part of the verse, the

384
00:24:57,800 --> 00:25:04,880
 constraint and the binding and the servitude involved with

385
00:25:04,880 --> 00:25:06,000
 cherishing things.

386
00:25:06,000 --> 00:25:10,380
 That in fact we are not in charge. In fact, even if we

387
00:25:10,380 --> 00:25:14,000
 wanted to stop cherishing things we couldn't.

388
00:25:14,000 --> 00:25:16,820
 If we wanted to stop holding things dear and suddenly we

389
00:25:16,820 --> 00:25:20,000
 say, "Okay, my son is dead. I don't love him anymore."

390
00:25:20,000 --> 00:25:23,150
 Let's just get rid of that attachment now because it's no

391
00:25:23,150 --> 00:25:25,820
 good to me. Even if we wanted to do that, most people don't

392
00:25:25,820 --> 00:25:26,000
.

393
00:25:26,000 --> 00:25:34,730
 They're quite happy to suffer. Even if we wanted to we

394
00:25:34,730 --> 00:25:36,000
 couldn't.

395
00:25:36,000 --> 00:25:41,460
 We couldn't because we were ignorant, because we don't see

396
00:25:41,460 --> 00:25:47,860
 the stress and suffering. We don't realize the trap that we

397
00:25:47,860 --> 00:25:50,000
're stuck in.

398
00:25:50,000 --> 00:25:54,000
 We don't see clearly.

399
00:25:54,000 --> 00:25:58,020
 So through our practice of meditation here you might find

400
00:25:58,020 --> 00:26:00,000
 yourself letting go of some things. It can be scary.

401
00:26:00,000 --> 00:26:04,510
 It can be scary because you think, "I want to put all those

402
00:26:04,510 --> 00:26:08,000
 things I love. I'm going to lose them."

403
00:26:08,000 --> 00:26:13,470
 But what you will find is not that suddenly you've lost

404
00:26:13,470 --> 00:26:17,380
 things that you love. You'll find yourself gradually

405
00:26:17,380 --> 00:26:18,000
 letting go.

406
00:26:18,000 --> 00:26:26,290
 Gradually opening up to possibilities. Possibilities of

407
00:26:26,290 --> 00:26:30,000
 what we call loss and gain. Getting things you don't want.

408
00:26:30,000 --> 00:26:34,060
 Losing things you do want. But we call them those things

409
00:26:34,060 --> 00:26:38,000
 but it's simply really in reality it's just change.

410
00:26:38,000 --> 00:26:41,550
 You open yourself up to change. Change no longer threatens

411
00:26:41,550 --> 00:26:46,000
 you. It's no longer scary. It's no longer dangerous.

412
00:26:46,000 --> 00:26:55,650
 You become freed. Released from the bondage of having to

413
00:26:55,650 --> 00:26:58,000
 find happiness in things.

414
00:26:58,000 --> 00:27:08,000
 Your happiness, your peace comes from freedom.

415
00:27:08,000 --> 00:27:14,650
 So a good verse. Another one of these verses about cher

416
00:27:14,650 --> 00:27:18,000
ishing. We're going to have many more I think.

417
00:27:18,000 --> 00:27:25,860
 And this verse is verse 212 of 423. There are 423 verses in

418
00:27:25,860 --> 00:27:30,000
 the Dhammapada we've covered. 212.

419
00:27:30,000 --> 00:27:35,610
 Which means we've passed halfway with this verse. So thank

420
00:27:35,610 --> 00:27:39,000
 you all for keeping up and your interest in the Dhammapada

421
00:27:39,000 --> 00:27:40,000
 and interest in the practice.

422
00:27:40,000 --> 00:27:43,940
 Thank you all for being here and practicing. I wish you all

423
00:27:43,940 --> 00:27:46,000
 the best in your practice.

424
00:27:46,000 --> 00:27:50,000
 That's the Dhammapada. Thank you. Have a good night.

