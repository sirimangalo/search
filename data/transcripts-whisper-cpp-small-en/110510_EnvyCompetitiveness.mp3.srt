1
00:00:00,000 --> 00:00:07,480
 Welcome back to Ask a Monk. Today's question is on envy and

2
00:00:07,480 --> 00:00:10,480
 competitiveness.

3
00:00:10,480 --> 00:00:15,130
 When we acknowledge the fact that these states are common

4
00:00:15,130 --> 00:00:17,400
 in the world, how can

5
00:00:17,400 --> 00:00:26,290
 we hope to remedy them? So there are two different answers

6
00:00:26,290 --> 00:00:27,280
 to this question

7
00:00:27,280 --> 00:00:30,370
 depending on which way you look at it. If you're talking

8
00:00:30,370 --> 00:00:31,960
 about remedying it

9
00:00:31,960 --> 00:00:39,050
 within yourself, then it's a much easier question to answer

10
00:00:39,050 --> 00:00:40,000
. If you're

11
00:00:40,000 --> 00:00:44,270
 talking about remedying it in the world, then it requires

12
00:00:44,270 --> 00:00:45,920
 perhaps a little bit of

13
00:00:45,920 --> 00:00:49,420
 extra explanation, but I'll try to go through both of these

14
00:00:49,420 --> 00:00:51,560
. As for remedying

15
00:00:51,560 --> 00:00:56,230
 it in ourselves, the remedy for states like envy and

16
00:00:56,230 --> 00:01:00,000
 competitiveness are a

17
00:01:00,000 --> 00:01:11,360
 reassessment of our idea of what is worth, what has

18
00:01:11,360 --> 00:01:12,800
 intrinsic worth, what

19
00:01:12,800 --> 00:01:24,240
 is beneficial. I think too often we mistake the cause or we

20
00:01:24,240 --> 00:01:27,480
 think of the

21
00:01:27,480 --> 00:01:34,240
 things that we are competing for as intrinsically valuable.

22
00:01:34,240 --> 00:01:35,000
 Even though

23
00:01:35,000 --> 00:01:37,880
 we might acknowledge the fact that envy and competitiveness

24
00:01:37,880 --> 00:01:39,160
 are negative states,

25
00:01:39,160 --> 00:01:48,850
 we limit our attempts to mitigate them or to eradicate them

26
00:01:48,850 --> 00:01:50,360
 to the suppression

27
00:01:50,360 --> 00:02:03,280
 or the limiting of our desires. So we agree that the things

28
00:02:03,280 --> 00:02:03,520
 that we're

29
00:02:03,520 --> 00:02:08,560
 competing for are worthwhile, but because everyone wants

30
00:02:08,560 --> 00:02:10,480
 them, we have to either

31
00:02:10,480 --> 00:02:15,430
 compete for them, which we think is wrong, or we have to

32
00:02:15,430 --> 00:02:17,640
 suppress our desire for

33
00:02:17,640 --> 00:02:20,640
 them and we have to come to some sort of compromise where

34
00:02:20,640 --> 00:02:21,280
 everyone gets

35
00:02:21,280 --> 00:02:30,820
 part of what they would like to what they want. I think

36
00:02:30,820 --> 00:02:34,800
 this is an unfortunate

37
00:02:34,800 --> 00:02:38,910
 state of affairs and it's of course part of our inability

38
00:02:38,910 --> 00:02:41,120
 to see things clearly,

39
00:02:41,120 --> 00:02:46,590
 that we have limited ourselves or our solutions to the

40
00:02:46,590 --> 00:02:48,440
 problems of the world

41
00:02:48,440 --> 00:02:56,400
 by our inability to reevaluate our position or to come out

42
00:02:56,400 --> 00:02:58,160
 of this narrow

43
00:02:58,160 --> 00:03:09,630
 minded and this subjective state of affairs. The Buddha

44
00:03:09,630 --> 00:03:11,240
 said that when

45
00:03:11,240 --> 00:03:16,460
 we cling to something, it's either through our craving for

46
00:03:16,460 --> 00:03:18,160
 sensuality, for

47
00:03:18,160 --> 00:03:23,340
 simple sensations. We want to see beautiful things, we want

48
00:03:23,340 --> 00:03:23,440
 to

49
00:03:23,440 --> 00:03:27,360
 hear beautiful sounds, we want to smell good smells and we

50
00:03:27,360 --> 00:03:28,120
 want to taste

51
00:03:28,120 --> 00:03:33,910
 delicious tastes, we want to feel pleasant sensations and

52
00:03:33,910 --> 00:03:35,360
 we want to have

53
00:03:35,360 --> 00:03:42,920
 pleasant ideas or pleasant thoughts. This is the first way

54
00:03:42,920 --> 00:03:43,920
 we can cling to

55
00:03:43,920 --> 00:03:47,770
 something. The second way we cling to something is our

56
00:03:47,770 --> 00:03:49,280
 desire to be something,

57
00:03:49,280 --> 00:03:53,940
 our desire to attain some abstract notion, wanting to be

58
00:03:53,940 --> 00:03:55,400
 famous, wanting to

59
00:03:55,400 --> 00:04:00,680
 be rich, wanting to be powerful and so on, wanting to be

60
00:04:00,680 --> 00:04:02,400
 something or to have some

61
00:04:02,400 --> 00:04:09,000
 state of affairs arise. The third way is by desiring for

62
00:04:09,000 --> 00:04:09,600
 some state of

63
00:04:09,600 --> 00:04:13,560
 affairs or some state to cease, desiring not to be

64
00:04:13,560 --> 00:04:16,040
 something. I don't want to be

65
00:04:16,040 --> 00:04:22,960
 this, I don't want to be that, not wanting to come in to

66
00:04:22,960 --> 00:04:25,160
 contact with certain

67
00:04:25,160 --> 00:04:33,020
 certain states, not wanting to be poor, not wanting to be

68
00:04:33,020 --> 00:04:35,680
 in this or that state

69
00:04:35,680 --> 00:04:44,160
 or have this or that state of affairs be present. So based

70
00:04:44,160 --> 00:04:45,120
 on these three

71
00:04:45,120 --> 00:04:49,960
 sorts of craving or desire, we come into conflict with

72
00:04:49,960 --> 00:04:51,240
 others because of

73
00:04:51,240 --> 00:04:56,720
 course the other people want the same sorts of things that

74
00:04:56,720 --> 00:04:57,880
 we want or are also

75
00:04:57,880 --> 00:05:01,360
 trying to attain the same sorts of things. We've come to

76
00:05:01,360 --> 00:05:02,560
 accept that certain

77
00:05:02,560 --> 00:05:07,990
 sensations, certain experiences are pleasant. We've come to

78
00:05:07,990 --> 00:05:08,920
 accept that

79
00:05:08,920 --> 00:05:15,600
 certain states of being are preferable, being famous, being

80
00:05:15,600 --> 00:05:16,360
 rich, being

81
00:05:16,360 --> 00:05:19,960
 powerful and so on. We've come to accept that certain

82
00:05:19,960 --> 00:05:20,840
 states of affairs are

83
00:05:20,840 --> 00:05:25,360
 inferior, certain ways of being like living in a cave or

84
00:05:25,360 --> 00:05:26,720
 wearing rags or so

85
00:05:26,720 --> 00:05:35,180
 on are inferior and we want to be free from these states.

86
00:05:35,180 --> 00:05:39,440
 The true way

87
00:05:39,440 --> 00:05:44,390
 for ourselves to overcome these is to see through them, to

88
00:05:44,390 --> 00:05:45,620
 see that the

89
00:05:45,620 --> 00:05:51,980
 sensations that we have, our experiences of the world are

90
00:05:51,980 --> 00:05:53,440
 actually

91
00:05:53,440 --> 00:06:00,280
 objective, they're neither good nor bad and to be able to

92
00:06:00,280 --> 00:06:01,080
 see through this

93
00:06:01,080 --> 00:06:05,690
 partiality that when we see something it's merely seeing,

94
00:06:05,690 --> 00:06:06,880
 when we hear something

95
00:06:06,880 --> 00:06:10,180
 it's merely hearing, when we smell it's merely smelling. We

96
00:06:10,180 --> 00:06:11,320
 have to actually

97
00:06:11,320 --> 00:06:17,200
 overcome this addiction that we have for sensuality. This

98
00:06:17,200 --> 00:06:18,600
 is the first way that

99
00:06:18,600 --> 00:06:23,960
 we overcome these these states. The second way,

100
00:06:23,960 --> 00:06:25,840
 understanding that there is no

101
00:06:25,840 --> 00:06:30,080
 intrinsic benefit in a specific state of affairs, coming to

102
00:06:30,080 --> 00:06:31,360
 understand that being

103
00:06:31,360 --> 00:06:36,350
 rich has no intrinsic value of being powerful, being famous

104
00:06:36,350 --> 00:06:38,760
, being the boss or

105
00:06:38,760 --> 00:06:43,110
 being the head of a company or the head of a department or

106
00:06:43,110 --> 00:06:44,600
 so on and so on,

107
00:06:44,600 --> 00:06:48,950
 having lots of clothes, having a nice house, having a car,

108
00:06:48,950 --> 00:06:50,320
 a beautiful car,

109
00:06:50,320 --> 00:06:55,240
 beautiful family, having lots of children and so on. There

110
00:06:55,240 --> 00:06:55,680
's nothing

111
00:06:55,680 --> 00:07:00,490
 intrinsically positive or negative about these things but

112
00:07:00,490 --> 00:07:01,880
 based on our ability to

113
00:07:01,880 --> 00:07:06,480
 see through them we become in tune with reality and we are

114
00:07:06,480 --> 00:07:08,320
 able to accept

115
00:07:08,320 --> 00:07:12,690
 things as they are. So when other people want certain

116
00:07:12,690 --> 00:07:14,680
 things we have no desire

117
00:07:14,680 --> 00:07:17,070
 for those things and we're able to give them up, we're able

118
00:07:17,070 --> 00:07:18,880
 to let them go and

119
00:07:18,880 --> 00:07:23,480
 this has a lot to do with ego, the ability to give up our

120
00:07:23,480 --> 00:07:25,600
 ego, not having to

121
00:07:25,600 --> 00:07:29,990
 be right, not having to be the winner, to be successful, to

122
00:07:29,990 --> 00:07:32,000
 be victorious, not

123
00:07:32,000 --> 00:07:34,310
 having to fight with people. So when someone wants

124
00:07:34,310 --> 00:07:35,880
 something we don't have

125
00:07:35,880 --> 00:07:41,120
 any need to be seen as the victor or to be something when

126
00:07:41,120 --> 00:07:42,560
 we're able to give it

127
00:07:42,560 --> 00:07:46,650
 up, no matter if people ridicule us or call us a loser or

128
00:07:46,650 --> 00:07:48,040
 call us a bum or so

129
00:07:48,040 --> 00:07:52,810
 on, these words really have no meaning, we have no desire

130
00:07:52,810 --> 00:07:54,800
 for other people's

131
00:07:54,800 --> 00:07:59,570
 praise, we have no desire for other people's approval, we

132
00:07:59,570 --> 00:08:00,720
 have no desire for

133
00:08:00,720 --> 00:08:04,170
 other people's envy or so on. We can see that these things

134
00:08:04,170 --> 00:08:05,280
 have no intrinsic

135
00:08:05,280 --> 00:08:08,400
 value. This also comes about through the practice of

136
00:08:08,400 --> 00:08:09,960
 meditation when you start to

137
00:08:09,960 --> 00:08:13,640
 see when you're able to break reality up into pieces and

138
00:08:13,640 --> 00:08:14,880
 into its ultimate

139
00:08:14,880 --> 00:08:19,130
 components you can see that there is no meaning in being

140
00:08:19,130 --> 00:08:20,520
 rich or famous or

141
00:08:20,520 --> 00:08:24,790
 powerful or successful or so on. Even the praise of other

142
00:08:24,790 --> 00:08:26,240
 people is simply sound

143
00:08:26,240 --> 00:08:30,760
 coming from our coming to our ears and thoughts arising in

144
00:08:30,760 --> 00:08:31,880
 our mind of how

145
00:08:31,880 --> 00:08:38,280
 people love us and esteem us and it has no real lasting

146
00:08:38,280 --> 00:08:39,960
 effect on our true

147
00:08:39,960 --> 00:08:46,880
 peace and happiness and it brings no lasting peace. And the

148
00:08:46,880 --> 00:08:48,640
 same goes with our

149
00:08:48,640 --> 00:08:53,410
 desire for things not to be wanting to be free from state,

150
00:08:53,410 --> 00:08:53,960
 from certain states.

151
00:08:53,960 --> 00:08:58,200
 So when we see that there is nothing intrinsically wrong

152
00:08:58,200 --> 00:09:00,160
 with certain

153
00:09:00,160 --> 00:09:06,660
 states of affairs like living in poverty or living as

154
00:09:06,660 --> 00:09:08,000
 nobody or not having

155
00:09:08,000 --> 00:09:13,880
 education or so on, any sort of state of affairs that is

156
00:09:13,880 --> 00:09:15,960
 undesirable, we come to

157
00:09:15,960 --> 00:09:19,110
 see that it's only undesirable because of our expectations,

158
00:09:19,110 --> 00:09:19,800
 because of our

159
00:09:19,800 --> 00:09:25,880
 partiality and if we're impartial then we're able to live

160
00:09:25,880 --> 00:09:26,640
 with anything

161
00:09:26,640 --> 00:09:30,820
 when there are people in our lives that we'd rather be free

162
00:09:30,820 --> 00:09:31,640
 from when there are

163
00:09:31,640 --> 00:09:35,000
 states of affairs, when we're sick and so on. We don't feel

164
00:09:35,000 --> 00:09:36,080
 disturbed by these

165
00:09:36,080 --> 00:09:39,740
 states or these people who are able to live our lives in

166
00:09:39,740 --> 00:09:41,560
 peace even with

167
00:09:41,560 --> 00:09:47,800
 difficult situations or difficult phenomena, things that

168
00:09:47,800 --> 00:09:49,160
 would cause

169
00:09:49,160 --> 00:09:52,510
 other people suffering and difficult things that are

170
00:09:52,510 --> 00:09:54,400
 accepted by the world to

171
00:09:54,400 --> 00:10:01,240
 be negative states. We come to see rather that it is the

172
00:10:01,240 --> 00:10:02,240
 attachment and the

173
00:10:02,240 --> 00:10:06,100
 partiality, the anger and the greed and the delusion and

174
00:10:06,100 --> 00:10:07,720
 the egotism and so on

175
00:10:07,720 --> 00:10:12,050
 that are truly negative and we desire to get rid of these

176
00:10:12,050 --> 00:10:13,280
 and through getting

177
00:10:13,280 --> 00:10:17,060
 rid of these there's no competition. I suppose some might

178
00:10:17,060 --> 00:10:18,040
 say and I would

179
00:10:18,040 --> 00:10:21,120
 probably agree to some extent that a certain level of

180
00:10:21,120 --> 00:10:22,360
 competition in this

181
00:10:22,360 --> 00:10:26,280
 regard is useful. Competing to get rid of the defilements,

182
00:10:26,280 --> 00:10:27,520
 it comes in with

183
00:10:27,520 --> 00:10:30,300
 meditators. When people are meditating in a group they find

184
00:10:30,300 --> 00:10:32,400
 themselves wanting to

185
00:10:32,400 --> 00:10:35,870
 not necessarily show off but at least keep up with the

186
00:10:35,870 --> 00:10:37,040
 group so you find

187
00:10:37,040 --> 00:10:39,850
 yourself sitting straighter and longer and better and

188
00:10:39,850 --> 00:10:41,280
 easier because of this

189
00:10:41,280 --> 00:10:46,680
 so-called peer pressure. There are other people watching

190
00:10:46,680 --> 00:10:47,800
 you and or

191
00:10:47,800 --> 00:10:50,200
 other people who are going to judge you and so on and so to

192
00:10:50,200 --> 00:10:52,000
 a limited extent it's

193
00:10:52,000 --> 00:10:54,940
 helpful at least in the beginning. I would say in the long

194
00:10:54,940 --> 00:10:55,920
 term even that has

195
00:10:55,920 --> 00:11:02,400
 to be given up and practice should be done alone,

196
00:11:02,400 --> 00:11:06,280
 specifically because of these

197
00:11:06,280 --> 00:11:10,470
 difficulties that when we don't have competition we're lazy

198
00:11:10,470 --> 00:11:10,920
 and so on

199
00:11:10,920 --> 00:11:13,880
 because it's that laziness and that inertia that we're

200
00:11:13,880 --> 00:11:14,960
 trying to overcome

201
00:11:14,960 --> 00:11:18,560
 and if we're always reliant on a group then we'll never

202
00:11:18,560 --> 00:11:20,000
 have a chance to face

203
00:11:20,000 --> 00:11:23,150
 these states and to overcome them even though it may be

204
00:11:23,150 --> 00:11:24,440
 helpful in the beginning

205
00:11:24,440 --> 00:11:30,080
 to have support. Eventually one should be able to go

206
00:11:30,080 --> 00:11:32,440
 directly inside and not have

207
00:11:32,440 --> 00:11:39,850
 one's state of mind be dependent on external phenomena. As

208
00:11:39,850 --> 00:11:42,400
 for eradicating

209
00:11:42,400 --> 00:11:46,930
 these sorts of states in the world, well I guess the simple

210
00:11:46,930 --> 00:11:48,080
 explanation is

211
00:11:48,080 --> 00:11:55,870
 by extension the explanation to people of what is really

212
00:11:55,870 --> 00:11:56,680
 valuable because

213
00:11:56,680 --> 00:12:00,130
 we've come to value things that are useless, that are

214
00:12:00,130 --> 00:12:02,080
 meaningless and we've

215
00:12:02,080 --> 00:12:06,760
 given up our valuing of things that are useful like

216
00:12:06,760 --> 00:12:08,160
 meditation, introspection,

217
00:12:08,160 --> 00:12:13,200
 staying alone and spending time to yourself and so on. We

218
00:12:13,200 --> 00:12:15,800
 esteem people who

219
00:12:15,800 --> 00:12:20,440
 are gregarious, outgoing, who are fun and exciting and so

220
00:12:20,440 --> 00:12:21,880
 on and people who are

221
00:12:21,880 --> 00:12:25,950
 introspective, thoughtful and so on are less esteemed by

222
00:12:25,950 --> 00:12:27,360
 the masses at any rate

223
00:12:27,360 --> 00:12:34,100
 and so I think this this sort of paradigm shift needs to

224
00:12:34,100 --> 00:12:34,960
 take place where

225
00:12:34,960 --> 00:12:41,680
 we become less social and more introspective or at least

226
00:12:41,680 --> 00:12:43,680
 more in tune

227
00:12:43,680 --> 00:12:50,310
 with reality which really does put us as an island where

228
00:12:50,310 --> 00:12:52,080
 even though we might be

229
00:12:52,080 --> 00:12:55,440
 involved with other people our relationships with other

230
00:12:55,440 --> 00:12:56,080
 people are

231
00:12:56,080 --> 00:12:59,800
 conceptual and there's nothing wrong with that sort of

232
00:12:59,800 --> 00:13:00,900
 thing but in an

233
00:13:00,900 --> 00:13:04,580
 ultimate sense we are alone. We were born alone, we'll die

234
00:13:04,580 --> 00:13:06,200
 alone, we live our lives

235
00:13:06,200 --> 00:13:12,040
 alone, our thoughts are our own and so on. To a great

236
00:13:12,040 --> 00:13:14,440
 extent we are solitary beings

237
00:13:14,440 --> 00:13:18,710
 no matter we might be surrounded by loved ones, people who

238
00:13:18,710 --> 00:13:19,720
 love us and so on

239
00:13:19,720 --> 00:13:24,000
 and still be lonely, still feel alone if we're not able to

240
00:13:24,000 --> 00:13:25,400
 understand and to

241
00:13:25,400 --> 00:13:29,360
 come to terms with the nature of our own minds and our own

242
00:13:29,360 --> 00:13:33,480
 reality. So I think

243
00:13:33,480 --> 00:13:36,160
 it's obviously a difficult thing to do and in any way

244
00:13:36,160 --> 00:13:39,040
 changing society but even

245
00:13:39,040 --> 00:13:41,880
 changing society comes from within. As you change yourself

246
00:13:41,880 --> 00:13:42,520
 you become an

247
00:13:42,520 --> 00:13:46,320
 example for others and the things that you say and the

248
00:13:46,320 --> 00:13:48,840
 advice that you give, the

249
00:13:48,840 --> 00:13:54,240
 impression that you give to others is a great support for

250
00:13:54,240 --> 00:13:56,960
 them to also find the

251
00:13:56,960 --> 00:13:59,610
 results and to gain the source of results that you have

252
00:13:59,610 --> 00:14:00,120
 gained through your

253
00:14:00,120 --> 00:14:04,480
 practice. So I hope that helps thanks for the question.

