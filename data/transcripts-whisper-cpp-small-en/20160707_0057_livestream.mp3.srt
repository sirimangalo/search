1
00:00:00,000 --> 00:00:12,000
 Good evening everyone. We're broadcasting live July 6th.

2
00:00:19,000 --> 00:00:32,000
 Today's quote is about suffering. It's all about suffering,

3
00:00:32,000 --> 00:00:32,000
 isn't it?

4
00:00:32,000 --> 00:00:36,000
 It's one of the first things you hear about Buddhism is

5
00:00:36,000 --> 00:00:40,000
 that Buddha focused on suffering.

6
00:00:46,000 --> 00:00:50,600
 People who write about, talk about, or try to explain

7
00:00:50,600 --> 00:01:01,030
 Buddhism will try and defend it, couch it in, find language

8
00:01:01,030 --> 00:01:03,000
 and nuance.

9
00:01:06,000 --> 00:01:12,850
 Which is kind of funny because it's the most important

10
00:01:12,850 --> 00:01:18,000
 aspect of Buddhism, the suffering.

11
00:01:18,000 --> 00:01:25,500
 It's the most important aspect of spiritual practice. It's

12
00:01:25,500 --> 00:01:32,710
 the most important concept or quality or idea in the world,

13
00:01:32,710 --> 00:01:36,000
 in the universe for beings.

14
00:01:37,000 --> 00:01:43,080
 The Buddha was incredible that he was able to so clearly

15
00:01:43,080 --> 00:01:49,000
 identify the problem. It's almost too obvious.

16
00:01:49,000 --> 00:01:57,440
 It almost doesn't even need to be said except that, of

17
00:01:57,440 --> 00:02:02,000
 course, what is the problem, the suffering?

18
00:02:03,000 --> 00:02:11,950
 It turns out that we're very good at avoiding and twisting

19
00:02:11,950 --> 00:02:18,930
 the truth to find some other problem, to not have to deal

20
00:02:18,930 --> 00:02:20,000
 with suffering.

21
00:02:20,000 --> 00:02:25,000
 If I could solve something else, there would be no problem.

22
00:02:26,000 --> 00:02:32,000
 If I could have money, if only I had a job, if only I had a

23
00:02:32,000 --> 00:02:36,000
 girlfriend or a boyfriend.

24
00:02:36,000 --> 00:02:48,210
 The problem is inequality or the problem is lack of respect

25
00:02:48,210 --> 00:02:49,000
.

26
00:02:49,000 --> 00:03:00,110
 The problem is whatever problem I'm facing, whether it be

27
00:03:00,110 --> 00:03:05,000
 external or internal.

28
00:03:10,000 --> 00:03:16,200
 In fact, it's quite simple. The problem is suffering. That

29
00:03:16,200 --> 00:03:19,000
's the problem. That's what we should be focusing on.

30
00:03:19,000 --> 00:03:24,070
 I think it's so depressing to talk about suffering. That's

31
00:03:24,070 --> 00:03:32,000
 how brainwashed we become into avoiding the problem.

32
00:03:33,000 --> 00:03:38,520
 You can't fix the problem because early on in life we get

33
00:03:38,520 --> 00:03:40,000
 suffering.

34
00:03:40,000 --> 00:03:52,000
 We have no way of fixing it. So we learn to avoid, ignore,

35
00:03:52,000 --> 00:03:58,000
 become willfully blind towards suffering.

36
00:03:59,000 --> 00:04:04,000
 We don't have an answer, so we learn to put up with it.

37
00:04:04,000 --> 00:04:14,000
 You often get the view that suffering is a part of life.

38
00:04:14,000 --> 00:04:17,020
 Suffering is something you can't avoid. That's actually

39
00:04:17,020 --> 00:04:18,000
 kind of wise.

40
00:04:18,000 --> 00:04:22,630
 In Buddhism we talk about that because there are aspects of

41
00:04:22,630 --> 00:04:26,000
 suffering that you can't avoid in life.

42
00:04:27,000 --> 00:04:30,000
 You can't avoid getting old, you can't avoid getting sick,

43
00:04:30,000 --> 00:04:36,410
 you can't avoid dying, you can't avoid urinating, defec

44
00:04:36,410 --> 00:04:40,910
ating, eating, hunger, thirst, heat, cold, you can't avoid

45
00:04:40,910 --> 00:04:42,000
 all these things.

46
00:04:42,000 --> 00:04:51,000
 You can be free from suffering.

47
00:04:52,000 --> 00:04:58,000
 The Buddha asks this. The quote is actually more specific.

48
00:04:58,000 --> 00:05:04,640
 These boys were tormenting fish, which as someone noted is

49
00:05:04,640 --> 00:05:09,000
 quite apt for National Fishing Day.

50
00:05:09,000 --> 00:05:13,000
 I don't know if it's in America, but in Canada it's hard.

51
00:05:13,000 --> 00:05:17,000
 People are going around celebrating the catching of fish.

52
00:05:17,000 --> 00:05:20,000
 It's like as though it were ice hockey or something.

53
00:05:21,000 --> 00:05:23,000
 They call it a sport.

54
00:05:23,000 --> 00:05:25,980
 Hunting would be a sport if we gave weapons to the other

55
00:05:25,980 --> 00:05:29,000
 team, right? Then it would be a sport.

56
00:05:29,000 --> 00:05:33,000
 Bloodthirsty sport.

57
00:05:33,000 --> 00:05:43,000
 That would sober people up if the fish had fishing rods.

58
00:05:44,000 --> 00:05:52,000
 They could hook the humans. It became a test of who could

59
00:05:52,000 --> 00:05:52,000
 bait the other team.

60
00:05:52,000 --> 00:05:56,750
 So you wouldn't know that you're going to eat this plate of

61
00:05:56,750 --> 00:05:59,000
 food whether it was bait.

62
00:05:59,000 --> 00:06:04,000
 Wouldn't that be awful?

63
00:06:05,000 --> 00:06:09,030
 It's a way of knowing, just having to take your gut feeling

64
00:06:09,030 --> 00:06:13,520
 whether you should eat this plate of food or not because

65
00:06:13,520 --> 00:06:16,000
 there might be some invisible hook in it.

66
00:06:16,000 --> 00:06:22,880
 Suddenly you're yanked into the water and drowned. It's

67
00:06:22,880 --> 00:06:26,000
 basically it.

68
00:06:33,000 --> 00:06:36,160
 So they were tormenting fish. It looks like they weren't

69
00:06:36,160 --> 00:06:40,370
 even killing them or they weren't going to eat them. It was

70
00:06:40,370 --> 00:06:43,000
 just for fun because kids are like that.

71
00:06:43,000 --> 00:06:49,670
 It says kids are innocent. Kids are not innocent. Kids are

72
00:06:49,670 --> 00:06:52,000
 pure. Kids are not pure.

73
00:06:52,000 --> 00:06:56,720
 We should just try and be a kid. I can't think of anything

74
00:06:56,720 --> 00:06:58,000
 more horrific.

75
00:07:00,000 --> 00:07:03,380
 Kids are horrible. They can be so cruel. But the

76
00:07:03,380 --> 00:07:07,360
 interesting thing is it's not because they're intentionally

77
00:07:07,360 --> 00:07:10,000
 evil. It's because they don't know any better.

78
00:07:10,000 --> 00:07:14,270
 They don't understand the consequences. Kids have forgotten

79
00:07:14,270 --> 00:07:16,000
 all about consequences.

80
00:07:16,000 --> 00:07:24,000
 They're born fresh and let go completely of past memories.

81
00:07:29,000 --> 00:07:31,430
 That's all they can think about is the pleasure that they

82
00:07:31,430 --> 00:07:33,000
 can get wherever they can get it.

83
00:07:33,000 --> 00:07:39,760
 My kids are so happy because they're so simple. They just

84
00:07:39,760 --> 00:07:41,000
 want to be happy.

85
00:07:41,000 --> 00:07:46,700
 They don't realize the consequences of their quest for

86
00:07:46,700 --> 00:07:51,000
 happiness and so they do terrible things.

87
00:07:52,000 --> 00:07:55,520
 If we just torment some fish that will make us happy. It

88
00:07:55,520 --> 00:07:59,000
 doesn't matter what about the fish. As long as we get happy

89
00:07:59,000 --> 00:07:59,000
.

90
00:07:59,000 --> 00:08:04,560
 The Buddha comes out and asks them, tries to point out the

91
00:08:04,560 --> 00:08:06,000
 consequences.

92
00:08:10,000 --> 00:08:16,910
 He does more of this helping us to realize we're not so

93
00:08:16,910 --> 00:08:24,300
 separate from our victims. We're not so separate from each

94
00:08:24,300 --> 00:08:25,000
 other.

95
00:08:26,000 --> 00:08:32,000
 He asks them, what's the Pali?

96
00:08:41,000 --> 00:08:49,940
 Are you afraid, boys, of suffering? Is suffering apiya? Ap

97
00:08:49,940 --> 00:08:58,000
iya means not dear. Undear. Unpleasant to you. Unpleasing.

98
00:08:58,000 --> 00:08:59,000
 Displeasing to you.

99
00:09:04,000 --> 00:09:12,000
 And they said, "Eywang bante baiyama maiyama."

100
00:09:12,000 --> 00:09:15,300
 "Eywang bante baiyama maiyang bante dukasya apiyang no dukk

101
00:09:15,300 --> 00:09:16,000
ang."

102
00:09:16,000 --> 00:09:23,000
 Yes, it is so venerable, sir. We are afraid of suffering.

103
00:09:23,000 --> 00:09:29,000
 Suffering is displeasing to us.

104
00:09:32,000 --> 00:09:35,500
 This is from the Udana. Udana is where the Buddha gave

105
00:09:35,500 --> 00:09:36,000
 inspired.

106
00:09:36,000 --> 00:09:44,000
 Udana is where you proclaim something. It's an udana.

107
00:09:44,000 --> 00:09:49,540
 These are generally short suttas where the Buddha would

108
00:09:49,540 --> 00:09:54,330
 proclaim something. Something sort of quite profound or

109
00:09:54,330 --> 00:09:56,000
 meaningful anyway.

110
00:09:57,000 --> 00:10:03,740
 And so then he gave the udana. "Ata ko bagva eta matang vid

111
00:10:03,740 --> 00:10:07,000
itwa taiyang vilaayang imangudana mudali."

112
00:10:07,000 --> 00:10:13,750
 At that time, the Blessed One gave this exclamation, this

113
00:10:13,750 --> 00:10:16,000
 utterance.

114
00:10:18,000 --> 00:10:22,390
 "Sache baiyata dukasya," if you are afraid of suffering, "S

115
00:10:22,390 --> 00:10:27,000
ache wo dukam apiyam," if suffering is displeasing to you.

116
00:10:27,000 --> 00:10:32,000
 "Makata bapakangamang," don't do evil deeds.

117
00:10:32,000 --> 00:10:42,000
 "Aviva yadiva raho," in public or in private.

118
00:10:45,000 --> 00:10:54,410
 "Sache bapakangamang karisata karotawa," if and if evil

119
00:10:54,410 --> 00:11:01,000
 deeds you will do or do.

120
00:11:05,000 --> 00:11:23,460
 If you are doing or will do, "Navodukam pamutyati upejapi

121
00:11:23,460 --> 00:11:23,830
 allayatam," verses are sometimes difficult because they

122
00:11:23,830 --> 00:11:24,000
 mangle it.

123
00:11:25,000 --> 00:11:34,000
 Basically, you cannot find an escape from suffering.

124
00:11:34,000 --> 00:11:42,000
 "Peychapi allayatam."

125
00:11:42,000 --> 00:11:46,780
 You will not be free from suffering by running away from it

126
00:11:46,780 --> 00:11:49,000
, by escaping.

127
00:11:50,000 --> 00:12:01,550
 "Don't do evil." You don't want to suffer. It's quite

128
00:12:01,550 --> 00:12:04,000
 simple.

129
00:12:04,000 --> 00:12:09,260
 Evil leads to suffering. Suffering is the problem. Don't do

130
00:12:09,260 --> 00:12:10,000
 evil.

131
00:12:11,000 --> 00:12:16,780
 The world is not a complicated place. All of the problems

132
00:12:16,780 --> 00:12:20,000
 of the world can boil down to this.

133
00:12:20,000 --> 00:12:26,240
 Suffering. Wealth inequality isn't the problem, it's

134
00:12:26,240 --> 00:12:27,000
 suffering.

135
00:12:27,000 --> 00:12:31,930
 The climate isn't the problem, the environment isn't the

136
00:12:31,930 --> 00:12:33,000
 problem.

137
00:12:34,000 --> 00:12:37,710
 It isn't even a problem that you don't have any money or

138
00:12:37,710 --> 00:12:41,000
 even any food. The problem is suffering.

139
00:12:41,000 --> 00:12:45,490
 Sometimes you lose your job and you still have food, you

140
00:12:45,490 --> 00:12:49,360
 still have health and you still have a place to stay, but

141
00:12:49,360 --> 00:12:51,000
 it's so much suffering.

142
00:12:51,000 --> 00:12:59,000
 You don't want to have lost your job, so you suffer.

143
00:13:00,000 --> 00:13:05,450
 The cause of suffering is even quite simple. The cause of

144
00:13:05,450 --> 00:13:10,000
 suffering, in essence, is evil.

145
00:13:10,000 --> 00:13:15,630
 It's kind of a tautology that it's evil because it causes

146
00:13:15,630 --> 00:13:18,000
 suffering, it causes suffering because it's evil.

147
00:13:18,000 --> 00:13:22,110
 It's more like it's evil because it causes suffering. So

148
00:13:22,110 --> 00:13:24,000
 the question is, what is evil?

149
00:13:25,000 --> 00:13:29,000
 What is it that causes suffering? This is the key, really.

150
00:13:29,000 --> 00:13:37,460
 The Buddha was equally awesome to be able to identify the

151
00:13:37,460 --> 00:13:40,000
 simple cause.

152
00:13:40,000 --> 00:13:47,070
 The cause of suffering isn't other people or isn't the

153
00:13:47,070 --> 00:13:51,000
 situation you're in.

154
00:13:52,000 --> 00:13:55,790
 The cause of suffering is not any situation, not any

155
00:13:55,790 --> 00:13:57,000
 experience.

156
00:13:57,000 --> 00:14:01,000
 The cause of suffering is our reactions to experience,

157
00:14:01,000 --> 00:14:08,610
 specifically, more directly, our attachment, our desire for

158
00:14:08,610 --> 00:14:11,000
 certain experiences.

159
00:14:12,000 --> 00:14:20,920
 It's our desires that propel us to cultivate expectations,

160
00:14:20,920 --> 00:14:27,280
 hopes, wants, ambitions, and they set us up for

161
00:14:27,280 --> 00:14:29,000
 disappointment.

162
00:14:29,000 --> 00:14:31,580
 You want things to be a certain way and then there another

163
00:14:31,580 --> 00:14:39,000
 way. When you like something, not having it, change.

164
00:14:39,000 --> 00:14:47,460
 When you're partial to certain experiences, other

165
00:14:47,460 --> 00:14:55,000
 experiences, become unpleasant, displeasing, dukkha.

166
00:14:55,000 --> 00:15:00,240
 And then you cultivate diversion towards them. They're not

167
00:15:00,240 --> 00:15:02,000
 what you want.

168
00:15:03,000 --> 00:15:10,000
 The cause of suffering is the cause of suffering.

169
00:15:10,000 --> 00:15:12,750
 The cause of suffering is the cause of suffering. But it

170
00:15:12,750 --> 00:15:15,000
 still doesn't actually get to the point.

171
00:15:15,000 --> 00:15:17,580
 The interesting thing is we say craving is the cause of

172
00:15:17,580 --> 00:15:20,000
 suffering, so we talk a lot about letting go.

173
00:15:20,000 --> 00:15:24,010
 If you can let go, then you'll be free from suffering. But

174
00:15:24,010 --> 00:15:27,000
 it still doesn't tell the whole picture.

175
00:15:28,000 --> 00:15:31,220
 The question is, how do you let go? It's actually quite

176
00:15:31,220 --> 00:15:37,000
 reasonable and fairly well understood in a general sense.

177
00:15:37,000 --> 00:15:40,170
 If you can learn to let go, you'll suffer less. The

178
00:15:40,170 --> 00:15:43,000
 stressing and obsessing question is, how do you let go?

179
00:15:43,000 --> 00:15:48,070
 We haven't completed the picture because the cause of

180
00:15:48,070 --> 00:15:52,000
 craving, you have to ask, what is the cause of craving?

181
00:15:53,000 --> 00:15:55,400
 What is the cause of desire? What is the cause of clinging

182
00:15:55,400 --> 00:15:58,820
 to things? Expecting things, wanting things, needing things

183
00:15:58,820 --> 00:16:00,000
 to be a certain way.

184
00:16:00,000 --> 00:16:07,280
 It's actually, it's not some inherent evil or maliciousness

185
00:16:07,280 --> 00:16:12,000
 on our part. It's simple delusion, simple ignorance.

186
00:16:15,000 --> 00:16:18,630
 You don't understand why there is suffering. There's

187
00:16:18,630 --> 00:16:22,260
 suffering because of nature, because of the way nature

188
00:16:22,260 --> 00:16:23,000
 works.

189
00:16:23,000 --> 00:16:32,000
 It's somewhat random or happen, happenstance, chance.

190
00:16:32,000 --> 00:16:39,000
 Things have turned out the way they have on whim.

191
00:16:40,000 --> 00:16:44,040
 There's no rhyme or reason to the universe. There's no

192
00:16:44,040 --> 00:16:49,440
 meaning behind things. There's a history, a long and

193
00:16:49,440 --> 00:16:52,000
 terrible history.

194
00:16:53,000 --> 00:17:03,820
 But in all aspects, the universe is put together haphazard

195
00:17:03,820 --> 00:17:10,000
ly, by chance, by blindness.

196
00:17:13,000 --> 00:17:18,100
 Through blindness we have created this prison for ourselves

197
00:17:18,100 --> 00:17:23,300
, created this existence that is sometimes good, sometimes

198
00:17:23,300 --> 00:17:24,000
 bad.

199
00:17:24,000 --> 00:17:27,870
 Sometimes we're really great people, we can be really good

200
00:17:27,870 --> 00:17:31,420
 to each other, usually based on knowledge of cause and

201
00:17:31,420 --> 00:17:32,000
 effect.

202
00:17:33,000 --> 00:17:36,290
 But then we forget and we lose sight of the good and we

203
00:17:36,290 --> 00:17:40,430
 become terrible people again. We become addicted to things,

204
00:17:40,430 --> 00:17:42,000
 we become obsessed.

205
00:17:42,000 --> 00:17:47,390
 We don't realize we're doing it, we cultivate all sorts of

206
00:17:47,390 --> 00:17:50,000
 bad habits and we suffer.

207
00:17:50,000 --> 00:17:56,190
 This is what you realize in meditation. What am I doing to

208
00:17:56,190 --> 00:18:00,000
 myself? What have I been doing to myself?

209
00:18:01,000 --> 00:18:04,310
 How could I have been so blind? This is what you realize

210
00:18:04,310 --> 00:18:09,000
 when you start to meditate. You see all your bad habits.

211
00:18:12,000 --> 00:18:20,370
 Totally inept at this life thing, the incredible

212
00:18:20,370 --> 00:18:30,530
 incompetence, our inability to find the truth, to find the

213
00:18:30,530 --> 00:18:37,000
 right, to find the good, to find that which makes us happy.

214
00:18:38,000 --> 00:18:41,430
 Why can't we just be happy? Why can't we find what makes us

215
00:18:41,430 --> 00:18:43,000
 happy? Why do we suffer?

216
00:18:43,000 --> 00:18:48,490
 Blindness, ignorance. When you practice insight meditation,

217
00:18:48,490 --> 00:18:52,160
 that's what happens. You don't let go. That's not what you

218
00:18:52,160 --> 00:18:53,000
're doing.

219
00:18:53,000 --> 00:18:59,010
 Letting go comes from the practice. Why does it come?

220
00:18:59,010 --> 00:19:02,000
 Because you see clearly.

221
00:19:03,000 --> 00:19:06,160
 What you're doing is trying to see things, just see things

222
00:19:06,160 --> 00:19:10,410
 as they are. Open your eyes. Understand what you're doing

223
00:19:10,410 --> 00:19:11,000
 to yourself.

224
00:19:11,000 --> 00:19:15,840
 When you start to see, you start to see what you're doing,

225
00:19:15,840 --> 00:19:18,000
 what the mind is doing.

226
00:19:21,000 --> 00:19:25,660
 You see greed, you see anger, you see delusion, you see

227
00:19:25,660 --> 00:19:30,500
 conceit, you see arrogance, all sorts of evil. Why is it

228
00:19:30,500 --> 00:19:34,000
 evil? You can see it's causing you suffering.

229
00:19:37,000 --> 00:19:43,000
 So anyway, that's a little bit about suffering tonight.

230
00:19:43,000 --> 00:19:46,000
 Because it's so much, so great to talk. Such a great

231
00:19:46,000 --> 00:19:48,000
 subject. It's something we should talk about all the time.

232
00:19:48,000 --> 00:19:52,000
 We should all concentrate our efforts and our focus, our

233
00:19:52,000 --> 00:19:57,380
 attention on suffering. We should. We should stop being

234
00:19:57,380 --> 00:20:00,000
 such a taboo subject. It's the problem.

235
00:20:01,000 --> 00:20:03,400
 It's what we should all be working on. It's what the

236
00:20:03,400 --> 00:20:06,150
 government should be working on. It's what everyone,

237
00:20:06,150 --> 00:20:10,310
 religious should be working on. Forget about God. God's not

238
00:20:10,310 --> 00:20:11,000
 going to help you.

239
00:20:11,000 --> 00:20:20,530
 God's not the problem. Praying is not the problem. Suff

240
00:20:20,530 --> 00:20:24,000
ering, that's the problem.

241
00:20:25,000 --> 00:20:29,400
 Alright, so let's look at some questions here. It would

242
00:20:29,400 --> 00:20:32,050
 really be nice if you could put the question mark before

243
00:20:32,050 --> 00:20:35,000
 the questions. It's pretty easy if you figured out.

244
00:20:35,000 --> 00:20:38,800
 That would make it easier for me to tell which are

245
00:20:38,800 --> 00:20:40,000
 questions.

246
00:20:41,000 --> 00:20:44,900
 Do you always associate the feeling with the thought,

247
00:20:44,900 --> 00:20:49,220
 saying that this is the feeling, this is the feeling? That

248
00:20:49,220 --> 00:20:52,680
's really... You have to be careful to be grammatical in

249
00:20:52,680 --> 00:20:55,810
 your questions. I think it gives me a free pass to skip it

250
00:20:55,810 --> 00:20:58,000
 because I don't know what you're saying.

251
00:21:02,000 --> 00:21:05,630
 This feeling is from this thought. Should there be separate

252
00:21:05,630 --> 00:21:09,310
 things? You only have to worry about what things are. Don't

253
00:21:09,310 --> 00:21:12,320
 focus too much in terms of your practice on cause and

254
00:21:12,320 --> 00:21:16,080
 effect. You'll see cause and effect. You'll see what leads

255
00:21:16,080 --> 00:21:17,000
 to what's fine.

256
00:21:18,000 --> 00:21:22,240
 Your practice should only be this is this. Remember those

257
00:21:22,240 --> 00:21:26,890
 words, this is this. That's your whole practice. Reminding

258
00:21:26,890 --> 00:21:29,000
 yourself, this is this.

259
00:21:29,000 --> 00:21:50,000
 [silence]

260
00:21:51,000 --> 00:21:56,490
 Yeah, those kids videos. If you know of any kids who you

261
00:21:56,490 --> 00:22:02,230
 think might be interested in meditation, there's videos on

262
00:22:02,230 --> 00:22:07,630
 how to meditate for kids. It seemed to work, seemed to be

263
00:22:07,630 --> 00:22:09,000
 beneficial.

264
00:22:09,000 --> 00:22:15,000
 How do we observe, understand mind and mental objects?

265
00:22:18,000 --> 00:22:22,430
 Mental objects is just a translation of Dhamma. Dhamma

266
00:22:22,430 --> 00:22:26,080
 doesn't really translate to mental objects, but that's an

267
00:22:26,080 --> 00:22:28,000
 explanatory translation.

268
00:22:31,000 --> 00:22:34,680
 Well, there's a sixth sense, this is in the context of the

269
00:22:34,680 --> 00:22:38,280
 sixth sense. So seeing, what's the object of seeing is

270
00:22:38,280 --> 00:22:41,770
 light or forms, which the object of hearing is sound,

271
00:22:41,770 --> 00:22:46,020
 smells, tastes, feelings. The object of the mind is

272
00:22:46,020 --> 00:22:51,000
 thoughts or Dhamma. Dhamma just means something mental,

273
00:22:51,000 --> 00:22:56,000
 something in the mind.

274
00:22:57,000 --> 00:23:01,150
 I think that can be also if you see, have visions in the

275
00:23:01,150 --> 00:23:05,280
 mind or if a song is going through your head, then it's

276
00:23:05,280 --> 00:23:09,520
 still I think called Dhamma Rama. Even though it was a

277
00:23:09,520 --> 00:23:13,530
 sight or a sound, because it's not coming from the eyes, it

278
00:23:13,530 --> 00:23:16,000
's considered to be Dhamma Rama.

279
00:23:16,000 --> 00:23:24,000
 [silence]

280
00:23:25,000 --> 00:23:28,180
 Is suffering a direct effect of latching on the artificial

281
00:23:28,180 --> 00:23:31,000
 nature of things rather than their true nature?

282
00:23:36,000 --> 00:23:41,230
 It's not all artificial nature causes suffering, but it's

283
00:23:41,230 --> 00:23:46,550
 certainly necessary. If you see things truly as they are,

284
00:23:46,550 --> 00:23:51,510
 you won't latch onto them and you won't suffer. So it's

285
00:23:51,510 --> 00:23:54,000
 necessary but not sufficient.

286
00:23:57,000 --> 00:24:00,490
 I would say if you see the artificial nature of things, I

287
00:24:00,490 --> 00:24:04,240
 mean even enlightened people see, enlightened beings see

288
00:24:04,240 --> 00:24:08,150
 the artificial nature. They know that this is a camera or a

289
00:24:08,150 --> 00:24:11,990
 computer or this is a monk sitting here. That's artificial

290
00:24:11,990 --> 00:24:17,000
 but they know it. They're not incapable of seeing it.

291
00:24:18,000 --> 00:24:23,050
 But the latching on I guess is the key here. The latching

292
00:24:23,050 --> 00:24:27,700
 onto anything is the problem. It's the latching not the

293
00:24:27,700 --> 00:24:31,990
 artificial. But we only latch because of the artificial.

294
00:24:31,990 --> 00:24:35,430
 You can't latch on when you see something as it is because

295
00:24:35,430 --> 00:24:41,560
 it arises and ceases. It's absurd to think about latching

296
00:24:41,560 --> 00:24:43,000
 onto it.

297
00:24:47,000 --> 00:24:51,040
 What are some good words for noting the transition from

298
00:24:51,040 --> 00:24:54,000
 standing to sitting and vice versa?

299
00:24:55,000 --> 00:25:00,720
 I mean whatever bending, lifting, touching. You should note

300
00:25:00,720 --> 00:25:07,580
 intention. If you can try to catch the intention. Wanting

301
00:25:07,580 --> 00:25:14,190
 to stand, wanting to stand and bending, stretching and so

302
00:25:14,190 --> 00:25:19,000
 on. Wanting to sit, wanting to bend, lowering, touching.

303
00:25:20,000 --> 00:25:22,000
 How many people are there?

304
00:25:22,000 --> 00:25:25,000
 How many people are there?

305
00:25:25,000 --> 00:25:31,050
 Fifty-two viewers. Things are really picking up. I've got

306
00:25:31,050 --> 00:25:36,590
 more and more people coming to do meditation courses. We

307
00:25:36,590 --> 00:25:42,290
 really should think seriously about someday setting up our

308
00:25:42,290 --> 00:25:46,000
 own center. Not renting a house.

309
00:25:47,000 --> 00:25:50,660
 We should think about this. We have lots of people

310
00:25:50,660 --> 00:25:55,270
 interested and we should talk about it. See if there's

311
00:25:55,270 --> 00:25:57,000
 enough support.

312
00:25:57,000 --> 00:26:05,010
 I think because this isn't really stable. I don't know what

313
00:26:05,010 --> 00:26:08,260
 the situation of things is going to be, whether we're

314
00:26:08,260 --> 00:26:11,000
 always going to be able to rent like this.

315
00:26:12,000 --> 00:26:18,000
 And moreover, we're going to be full. We're going to have

316
00:26:18,000 --> 00:26:24,370
 four meditators throughout August and into September. That

317
00:26:24,370 --> 00:26:28,000
 doesn't seem like a lot but it's a full house.

318
00:26:34,000 --> 00:26:40,280
 Anyway, great to see such interest in these things. It's

319
00:26:40,280 --> 00:26:43,000
 great to think that this is somehow useful.

320
00:26:43,000 --> 00:26:48,570
 Which reminds me, I'm probably going to try to set up a

321
00:26:48,570 --> 00:26:51,000
 testimonials page.

322
00:26:54,000 --> 00:26:58,690
 If you've been practicing meditation, I'll probably make an

323
00:26:58,690 --> 00:27:02,000
 announcement once we get it set up again.

324
00:27:02,000 --> 00:27:06,150
 If you've been practicing this meditation and feel like it

325
00:27:06,150 --> 00:27:10,240
's been beneficial to you, if you could send us a testim

326
00:27:10,240 --> 00:27:11,000
onial.

327
00:27:12,000 --> 00:27:15,680
 This means some short words about how the meditation

328
00:27:15,680 --> 00:27:20,000
 benefited you. Send it to info@siri-mongolo.org.

329
00:27:20,000 --> 00:27:24,550
 For now, I'll try to set up a new email address or

330
00:27:24,550 --> 00:27:29,000
 something. But info works. info@siri-mongolo.org.

331
00:27:29,000 --> 00:27:32,940
 And we'll try to collect them, start collecting them and

332
00:27:32,940 --> 00:27:35,000
 put them up on the website.

333
00:27:36,000 --> 00:27:39,620
 I also get from Facebook from time to time and even YouTube

334
00:27:39,620 --> 00:27:42,360
 from time to time. So we'll try to collect them all

335
00:27:42,360 --> 00:27:43,000
 together.

336
00:27:43,000 --> 00:27:49,430
 I think that's useful. It helps people decide whether we're

337
00:27:49,430 --> 00:27:55,530
 legit, whether we're not some fly-by-night operation or

338
00:27:55,530 --> 00:27:58,000
 some cult or something.

339
00:28:05,000 --> 00:28:08,290
 Are some people more like, more attached to aversion than

340
00:28:08,290 --> 00:28:14,000
 to greed? Yes, some people are more inclined towards anger.

341
00:28:14,000 --> 00:28:22,450
 How can we be sure of the accuracy of our notings? Would

342
00:28:22,450 --> 00:28:26,000
 noting falsely not consolidate delusion?

343
00:28:29,000 --> 00:28:33,740
 No, it would come from delusion, I think. It's not that

344
00:28:33,740 --> 00:28:38,000
 difficult. Because if you note something like "cat", "cat",

345
00:28:38,000 --> 00:28:41,090
 it's not really clear and it's not cutting through the

346
00:28:41,090 --> 00:28:42,000
 delusion.

347
00:28:42,000 --> 00:28:50,000
 Not as well. So you have to note realities.

348
00:28:51,000 --> 00:28:53,710
 If you hear something, if you hear something and you say "

349
00:28:53,710 --> 00:28:56,910
seeing", that's problematic, I suppose. Because of how

350
00:28:56,910 --> 00:28:59,000
 twisted that makes your mind.

351
00:28:59,000 --> 00:29:11,450
 All conditioned phenomena or dukkha is still true, even if

352
00:29:11,450 --> 00:29:18,000
 you don't latch onto things.

353
00:29:20,000 --> 00:29:26,230
 Dukkha doesn't mean they're suffering. Dukkha means they're

354
00:29:26,230 --> 00:29:31,000
 basically not worth clinging to.

355
00:29:31,000 --> 00:29:33,510
 I know that's not a direct translation, but it still

356
00:29:33,510 --> 00:29:36,360
 relates to clinging. If you don't cling to something, it

357
00:29:36,360 --> 00:29:38,000
 can't cause you suffering.

358
00:29:42,000 --> 00:29:45,780
 Dukkha means basically not worth clinging to. That's the

359
00:29:45,780 --> 00:29:48,000
 explanatory translation.

360
00:29:48,000 --> 00:29:54,420
 So in Arahant you could say it still has dukkha? No, not

361
00:29:54,420 --> 00:29:58,040
 really. Yes, in a sense. And they would admit that, the

362
00:29:58,040 --> 00:29:59,000
 Buddha would admit that.

363
00:29:59,000 --> 00:30:02,310
 But in an ultimate sense it's not really suffering. They're

364
00:30:02,310 --> 00:30:03,000
 not suffering.

365
00:30:03,000 --> 00:30:09,000
 Dukkha means when you cling to it, it causes you suffering.

366
00:30:09,000 --> 00:30:11,000
 Why? Because it's impermanent.

367
00:30:12,000 --> 00:30:15,310
 You won't meet your expectations. It's unpredictable. You

368
00:30:15,310 --> 00:30:19,170
 don't like it, well it might come. You like it, well it

369
00:30:19,170 --> 00:30:20,000
 might go.

370
00:30:20,000 --> 00:30:26,000
 It's under your control. It's anithya dukkha anata.

371
00:30:26,000 --> 00:30:29,000
 It means these things are not worth clinging to basically.

372
00:30:29,000 --> 00:30:33,020
 Sabe damma na langa bhini vesaya. That's all you need to

373
00:30:33,020 --> 00:30:36,000
 know. No damma is worth clinging to.

374
00:30:37,000 --> 00:30:41,330
 Alright, enough for tonight. Thank you all for tuning in. W

375
00:30:41,330 --> 00:30:45,000
ishing you all good practice. Be well.

376
00:30:46,000 --> 00:30:50,070
 Thank you all for tuning in. Wishing you all good practice.

377
00:30:50,070 --> 00:30:51,000
 Be well.

378
00:30:51,000 --> 00:30:52,000
 Thank you.

379
00:30:52,000 --> 00:30:53,000
 Thank you.

380
00:30:54,000 --> 00:30:55,000
 Thank you.

381
00:30:56,000 --> 00:30:57,000
 Thank you.

382
00:30:58,000 --> 00:30:59,000
 Thank you.

383
00:31:01,000 --> 00:31:02,000
 Thank you.

