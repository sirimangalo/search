1
00:00:00,000 --> 00:00:03,090
 When you are focusing on your breath, should you keep a

2
00:00:03,090 --> 00:00:06,000
 peripheral awareness to your passing thoughts?

3
00:00:06,000 --> 00:00:09,130
 Because I found that if I'm deeply aware of the breath or

4
00:00:09,130 --> 00:00:12,000
 another object, I just notice a thought after it has passed

5
00:00:12,000 --> 00:00:14,000
 and not in the moment.

6
00:00:14,000 --> 00:00:22,710
 Well, no, you shouldn't. The problem isn't that you're too

7
00:00:22,710 --> 00:00:26,990
 focused on the breath. The problem is that your mind still

8
00:00:26,990 --> 00:00:27,000
...

9
00:00:28,000 --> 00:00:31,430
 Well, it's not even a problem, but it's not the ideal mind

10
00:00:31,430 --> 00:00:36,270
 state. Of course, the ideal mind state is freedom from

11
00:00:36,270 --> 00:00:38,000
 thought, even freedom from breath.

12
00:00:38,000 --> 00:00:42,280
 I mean, the ideal mind state is extinguishing when there is

13
00:00:42,280 --> 00:00:44,000
 release or freedom.

14
00:00:46,000 --> 00:00:54,380
 But the problem that you have is that your lack of

15
00:00:54,380 --> 00:01:02,630
 mindfulness is not allowing you to catch the thought when

16
00:01:02,630 --> 00:01:05,000
 it starts.

17
00:01:05,000 --> 00:01:08,990
 It has nothing to do with being too absorbed in the breath.

18
00:01:08,990 --> 00:01:12,500
 When you're watching the breath, if you're mindful, if you

19
00:01:12,500 --> 00:01:14,000
're clearly aware,

20
00:01:15,000 --> 00:01:18,000
 you should be able to see as soon as the thought arises.

21
00:01:18,000 --> 00:01:20,430
 And as soon as the thought arises, you'll be able to

22
00:01:20,430 --> 00:01:22,000
 acknowledge it as thinking.

23
00:01:22,000 --> 00:01:25,630
 It's true that you can't actually be aware of an object as

24
00:01:25,630 --> 00:01:27,000
 it's happening.

25
00:01:27,000 --> 00:01:30,190
 Sorry, you can't actually be mindful of an object as it's

26
00:01:30,190 --> 00:01:33,290
 happening. The object arises and there's the awareness, and

27
00:01:33,290 --> 00:01:35,000
 then there's the mindfulness.

28
00:01:35,000 --> 00:01:38,840
 So you're actually technically not mindful of something at

29
00:01:38,840 --> 00:01:43,100
 the moment that it occurs, but it's so quick. It's thought

30
00:01:43,100 --> 00:01:45,580
 moments. It's one thought moment and the next thought

31
00:01:45,580 --> 00:01:46,000
 moment.

32
00:01:48,000 --> 00:01:52,030
 But the fact that you can't catch it until after it's over

33
00:01:52,030 --> 00:01:55,630
 is a sign that during that thought there was lack of

34
00:01:55,630 --> 00:01:57,000
 mindfulness.

35
00:01:57,000 --> 00:02:02,950
 Each thought moment until you caught it was a mind with

36
00:02:02,950 --> 00:02:05,000
 delusion in it.

37
00:02:05,000 --> 00:02:10,020
 So that being the case, I would suggest that your attention

38
00:02:10,020 --> 00:02:15,000
 on the breath was void of wisdom and void of mindfulness.

39
00:02:15,000 --> 00:02:19,730
 So perhaps I had delusion involved in it. If there is a

40
00:02:19,730 --> 00:02:24,720
 diluted mind state, even watching the breath, there will be

41
00:02:24,720 --> 00:02:29,000
 conceit, there will be view of self, there will be craving.

42
00:02:29,000 --> 00:02:33,690
 As a result, the moment of the thought arising will come

43
00:02:33,690 --> 00:02:38,700
 without awareness of the thought, and that's how you get

44
00:02:38,700 --> 00:02:40,000
 carried away.

45
00:02:40,000 --> 00:02:44,170
 Practically speaking, a beginner will only catch the

46
00:02:44,170 --> 00:02:48,010
 thoughts at the end after you've thought through the whole

47
00:02:48,010 --> 00:02:49,000
 subject.

48
00:02:49,000 --> 00:02:52,250
 An advanced meditator, as they progress, they'll be able to

49
00:02:52,250 --> 00:02:55,000
 catch the thoughts earlier and earlier and earlier.

50
00:02:55,000 --> 00:02:58,760
 And a meditator who is truly in the zone will be able to

51
00:02:58,760 --> 00:03:02,000
 catch the thoughts as soon as they arise.

52
00:03:02,000 --> 00:03:05,090
 So the thought arises in the mind, the meditator is aware

53
00:03:05,090 --> 00:03:08,380
 that the thought arose, just one thought subject, and then

54
00:03:08,380 --> 00:03:09,000
 it ceases.

55
00:03:09,000 --> 00:03:14,000
 And the meditator comes back to the main object.

56
00:03:14,000 --> 00:03:18,980
 So know you shouldn't keep tabs on something else while you

57
00:03:18,980 --> 00:03:22,000
're focusing on the main object.

58
00:03:22,000 --> 00:03:27,230
 And know it isn't caused by the breath, it's caused by

59
00:03:27,230 --> 00:03:33,400
 delusion, which may be indicative of improper observation

60
00:03:33,400 --> 00:03:35,000
 of the breath.

61
00:03:35,000 --> 00:03:39,160
 So starting out from a diluted state, which carries on into

62
00:03:39,160 --> 00:03:43,000
 the thought, allows the thought to sneak in there.

