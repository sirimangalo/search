1
00:00:00,000 --> 00:00:11,880
 Good evening. This is a short video to talk about a program

2
00:00:11,880 --> 00:00:17,000
 that we are planning for January of 2020.

3
00:00:17,000 --> 00:00:23,880
 So some years ago I took a trip down the east coast of the

4
00:00:23,880 --> 00:00:29,430
 United States starting in Massachusetts and went all the

5
00:00:29,430 --> 00:00:30,000
 way down to Florida.

6
00:00:30,000 --> 00:00:34,480
 And along the way I stayed with people who were interested

7
00:00:34,480 --> 00:00:38,200
 in learning about the practice or sharing it with people in

8
00:00:38,200 --> 00:00:39,000
 their community.

9
00:00:39,000 --> 00:00:42,880
 We set this up as a program and invited people to set

10
00:00:42,880 --> 00:00:47,480
 something up in their area along that sort of general route

11
00:00:47,480 --> 00:00:50,000
 from Massachusetts to Florida.

12
00:00:50,000 --> 00:00:53,770
 And it was great. It worked really well. I ended up staying

13
00:00:53,770 --> 00:00:56,490
 at a Catholic monastery. I stayed in a tent some of the

14
00:00:56,490 --> 00:00:57,000
 time.

15
00:00:57,000 --> 00:00:59,990
 I stayed at monasteries a couple of times. But it was all

16
00:00:59,990 --> 00:01:05,540
 people who really had the keen interest for something like

17
00:01:05,540 --> 00:01:08,000
 this to take root.

18
00:01:08,000 --> 00:01:13,440
 Or to plant a seed in their community. So in January I have

19
00:01:13,440 --> 00:01:16,950
 a month and we're thinking of doing the same thing in the

20
00:01:16,950 --> 00:01:19,000
 west coast of the United States.

21
00:01:19,000 --> 00:01:21,820
 So I'm not really sure how it's going to work. One idea we

22
00:01:21,820 --> 00:01:26,000
 had maybe from Arizona up to Seattle or Vancouver even.

23
00:01:26,000 --> 00:01:30,580
 But something similar. Taking buses from place to place and

24
00:01:30,580 --> 00:01:33,000
 staying in tent. I have a tent.

25
00:01:33,000 --> 00:01:37,220
 And maybe going with someone else if there's anyone who

26
00:01:37,220 --> 00:01:41,000
 wants to go with me just to sort of make it easy.

27
00:01:41,000 --> 00:01:44,000
 Safety in numbers or whatever.

28
00:01:44,000 --> 00:01:51,750
 But this is a call today for people. It's an announcement

29
00:01:51,750 --> 00:01:53,000
 of this plan.

30
00:01:53,000 --> 00:01:56,290
 And a call for people who are interested, who live in the

31
00:01:56,290 --> 00:01:58,000
 western of the United States.

32
00:01:58,000 --> 00:02:05,980
 And would like to set something up in their area. To put in

33
00:02:05,980 --> 00:02:11,000
 our plan along this trip. Would like us to visit.

34
00:02:11,000 --> 00:02:15,590
 It might be one day or two days or three days. If you have

35
00:02:15,590 --> 00:02:18,000
 the facilities you can set something up.

36
00:02:18,000 --> 00:02:23,200
 Even just one day or two days. But something to do with

37
00:02:23,200 --> 00:02:24,000
 meditation.

38
00:02:24,000 --> 00:02:28,250
 It's not just going to be Dhamma talks or something like

39
00:02:28,250 --> 00:02:29,000
 that.

40
00:02:29,000 --> 00:02:32,010
 The idea is that we might set up at least a one day course

41
00:02:32,010 --> 00:02:33,000
 in your area.

42
00:02:33,000 --> 00:02:38,000
 One day, two days, whatever you can manage.

43
00:02:38,000 --> 00:02:41,830
 And if you can, we want you to let us know. And we're going

44
00:02:41,830 --> 00:02:47,000
 to assess sort of the potential to see where we can go.

45
00:02:47,000 --> 00:02:51,320
 And which places are feasible and so on. We have a group

46
00:02:51,320 --> 00:02:53,000
 set up already.

47
00:02:53,000 --> 00:02:57,300
 Our volunteers are aware of this and ready for it. And all

48
00:02:57,300 --> 00:03:00,000
 they've done now, they said all now,

49
00:03:00,000 --> 00:03:05,030
 left to do is for me to announce it. And then you can

50
00:03:05,030 --> 00:03:09,000
 contact them and get involved.

51
00:03:09,000 --> 00:03:15,000
 So the way to get involved is to contact us for now.

52
00:03:15,000 --> 00:03:17,830
 The way to get involved is to contact us through our

53
00:03:17,830 --> 00:03:19,000
 Discord server.

54
00:03:19,000 --> 00:03:22,840
 So Discord is this platform. We were on Slack before but we

55
00:03:22,840 --> 00:03:24,000
 moved to Discord.

56
00:03:24,000 --> 00:03:28,000
 It's this platform that allows communities to get together.

57
00:03:28,000 --> 00:03:31,030
 We've been using it for quite a while now for our Dhamma

58
00:03:31,030 --> 00:03:32,000
 study group.

59
00:03:32,000 --> 00:03:35,000
 We meet once a week and we are able to hear each other.

60
00:03:35,000 --> 00:03:38,610
 And we read a book and we talk about it. We read some

61
00:03:38,610 --> 00:03:40,000
 teaching of some sort.

62
00:03:40,000 --> 00:03:43,130
 So you're welcome. If you're interested in joining the

63
00:03:43,130 --> 00:03:44,000
 Discord server anyway,

64
00:03:44,000 --> 00:03:49,000
 I mean, there's greatness to it. It's our community.

65
00:03:49,000 --> 00:03:52,000
 Sort of the heart of our online community now.

66
00:03:52,000 --> 00:03:55,590
 We have a volunteer section for people who are helping the

67
00:03:55,590 --> 00:03:57,000
 organization run.

68
00:03:57,000 --> 00:03:59,000
 We have the Dhamma study group. We just have other channels

69
00:03:59,000 --> 00:04:02,000
 for people who are interested in communicating

70
00:04:02,000 --> 00:04:06,000
 or cultivating this sense of an online community.

71
00:04:06,000 --> 00:04:09,350
 So you're welcome to join anyway. But now we've set up a

72
00:04:09,350 --> 00:04:13,000
 new channel called something like Teaching Tour 2020

73
00:04:13,000 --> 00:04:16,000
 or something like that. And it's for all of January 2020.

74
00:04:16,000 --> 00:04:20,000
 If you're interested in getting involved in this, go there.

75
00:04:20,000 --> 00:04:23,000
 I'll put a link in the description of this video.

76
00:04:23,000 --> 00:04:26,000
 Go there and let us know that you're interested.

77
00:04:26,000 --> 00:04:30,260
 Let us know what sort of things you're thinking that you

78
00:04:30,260 --> 00:04:32,000
 might be able to set up.

79
00:04:32,000 --> 00:04:35,110
 And our volunteers will talk to you about it and we'll

80
00:04:35,110 --> 00:04:37,000
 consider and figure out what's possible

81
00:04:37,000 --> 00:04:40,910
 and work out a plan and then we'll post that plan and let

82
00:04:40,910 --> 00:04:42,000
 everyone know

83
00:04:42,000 --> 00:04:47,000
 and hopefully do some great things just as last time.

84
00:04:47,000 --> 00:04:52,290
 So please do if you're interested, let us know and get

85
00:04:52,290 --> 00:04:53,000
 involved.

86
00:04:53,000 --> 00:04:56,700
 And hope to see you in January if you're able to set

87
00:04:56,700 --> 00:04:58,000
 something up.

88
00:04:58,000 --> 00:05:01,000
 Bye. Have a good night.

