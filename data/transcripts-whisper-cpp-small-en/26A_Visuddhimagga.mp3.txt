 We are on page 420.
 paragraph 46. Up till paragraph 45, the ten kinds of
 success were described and among them the first one.
 Only success by resolve is actually mentioned in the clause
 "Kines of Subnormal Power of Success".
 It is from the text. But two success as transformation and
 three success as the mind-made body are needed in this
 sense as well.
 So in the clause "Kines of Subnormal Power of Subnormal
 Success" we should take not only one but three of the kinds
 of success mentioned above.
 Now the commentator goes on explaining the other clauses in
 the text. "To the kinds of Subnormal Power", that is to the
 components of Subnormal Power or to the departments of Sub
normal Power.
 That means the constituents of Subnormal Power. He directs,
 he inclines his mind.
 When that bhikkhu's consciousness has become the basis for
 direct knowledge in the way already described, he directs
 the preliminary work consciousness with the purpose of att
aining the kinds of Subnormal Power.
 He sends it in the direction of kinds of Subnormal Power
 leading it away from the kasina as its object.
 In order to do a supernormal thing, first he enters into j
hanas and then he makes the fourth jhana. Fourth jhana means
 in fourfold method.
 Fourth jhana is the basis for Subnormal Power or Subnormal
 Knowledge. So bhikkhu's consciousness has become the basis
 for direct knowledge in the way already described.
 He directs the preliminary work consciousness with the
 purpose of attaining the kinds of Subnormal Power.
 So after entering into the fourth jhana, he emerges from
 the fourth jhana and then he does the preparation work for
 the Subnormal Knowledge.
 So preliminary work consciousness means sense fear
 consciousness.
 And then he sends it in the direction of the kinds of Sub
normal Power that means he inclines his mind towards that.
 So inclines make it tan and lean towards the Subnormal
 Power to be attained.
 And he means the bhikkhu who has done the directing of his
 mind in this way, the various, varied of different sources.
 These are word explanations. So they seem to be repeating.
 Kinds of Subnormal Power, departments of Subnormal Power,
 wields the Bali word, alternative form.
 Sometimes it cannot be translated into English or another
 language because the two words are just a variation.
 The second is just a variation of the first word. There is
 no difference in meaning.
 But bhacha nub bhoti and bhacha nub bhavati, they are the
 same.
 In the first word, the 'a' should be short, not long.
 Bhacha nub bhoti.
 And the second word is bhacha nub bhavati.
 The only difference is here, instead of bhoti here, we have
 bhavati.
 It is just a chromatic peculiarity.
 The meaning is that he makes contact with, realizes,
 reaches.
 Now in order to show that variousness, it is said, having
 been one,
 we have to read this because the commentator is going to
 explain one by one the different kinds of Subnormal Powers
 a man may perform.
 So having been one, he becomes many. Having been many, he
 becomes one.
 Actually, this text is not given in full in the Wizard of
 Maga because the commentator takes it that his readers
 already know this passage.
 But we are lucky that the translator gave us a full text
 here.
 That is why it is included in the brackets.
 So having been one, he becomes many. Having been many, he
 becomes one.
 That means he multiplies himself.
 He will create many likenesses of him, a hundred, a
 thousand, and so on.
 He appears and vanishes.
 He goes unhindered through walls, through enclosures,
 through mountains, as though in open space.
 He dives in and out of the earth as though in water.
 He goes on unbroken water as though on earth, walking
 underwater.
 Seated cross-legged, he travels in space like a winged bird
.
 With his hand, he touches and strokes the moon and sun so
 mighty and powerful.
 He wields bodily mastery even as far as the Brahman was.
 This is taken from the discourse Digha Nikaya.
 Then the commentator will explain one by one.
 Having been one, having been normally one before giving
 effect to the supernormal power.
 So a monk is only one, but here he is going to make
 likenesses of himself to create images.
 So he becomes many, wanting to walk with many, or wanting
 to do a recital, or wanting to ask questions with many,
 he becomes a hundred or a thousand.
 How does he do this?
 He accomplishes one, the four planes, two, the four bases
 or roads,
 three, the eight steps and four, the sixteen routes of
 supernormal power.
 And then he resolves with knowledge.
 Here planes means something like stories, I mean one story,
 two stories, high and so on.
 The Paliwat uses Bhumi.
 So Bhumi can mean planes, planes of existence, as well as
 floors in buildings.
 So first floor, second floor and so on.
 And these four planes should be understood as the four jhan
as.
 So four jhanas are said to be the four planes of four
 stages of the supernormal power.
 And four jhanas means according to four full methods.
 For this has been said by the general of the Dhamma, that
 means the elder Saribhoda.
 Saribhoda was said to be, or his other monks called him the
 general of the Dhamma.
 What are the four planes of supernormal power and so on?
 So it is taken from the Bhattisambhida manga.
 And you will find the reference, a few lines down, PS 2205.
 And he reaches supernormal power by becoming light, malle
able and really in body
 after steeping himself in blissful perception and light
 perception
 due to the pervasion of happiness and pervasion of bliss.
 Which is why the first three jhanas should be understood as
 the accessory plane
 since they lead to the obtaining of supernormal power in
 this manner.
 But the full is the natural plane for obtaining supernormal
 power.
 That means he enters into the first jhana and then second j
hana
 and then third jhana and the full jhana.
 So the full jhana is the natural plane for obtaining super
normal power.
 That means full jhana is the basis for supernormal power.
 Without entering into the full jhana and emerging from it,
 nobody can exercise the supernormal powers.
 So the full planes mean the full jhanas, the full material
 jhanas.
 Then the full basis of the road should be understood as the
 full basis of success
 and the Paliwat is a deep barda, road to power.
 Or here roads mean there are causes of.
 What is this? What are the full basis for success?
 Here a bakery develops the basis for success or road to
 power
 that possesses both concentration due to zeal and the will
 to strife.
 He develops the basis for success, road to power,
 that possesses both concentration due to energy and the
 will to strife.
 He develops the basis for success, road to power,
 that possesses both concentration due to natural purity of
 consciousness
 and the will to strife.
 He develops the basis for success, that possesses both
 concentration due to inquiry.
 Inquiry really means panya, knowledge, not just inquiring.
 The Paliwat uses wi mamsa, the Sanskrit word is mi mamsa.
 Mi mamsa means investigation.
 So here in first occasion does not necessarily mean just to
 investigate
 but to investigate and to understand correctly.
 So it is a name for panya, concentration due to inquiry and
 the will to strife.
 These four basis for success lead to the obtaining of super
normal power,
 to the fearlessness due to supernormal power and so on.
 What are these four? Did you get four?
 Now zeal is one thing here. The Paliwat is chanda, that
 means just the will to do.
 You know, chanda has two meanings, just the will to do or
 sometimes it is desire for essential things.
 So here just the will to do, not desire for essential
 things.
 So the first one is chanda, the second one is energy, vidya
.
 And the third is consciousness and the fourth is inquiry of
 panya.
 These four are called ikdibara, roads to power or causes of
 power.
 So wisdom, energy, consciousness and the will to do, a very
 strong will.
 So 8% has to develop these four qualities in order to get
 supernormal power.
 And here the concentration that has zeal as its cause or
 has zeal outstanding,
 that means zeal which is prominent, is concentration due to
 zeal.
 This is the term for concentration obtained by giving
 precedence to zeal consisting in desire to act,
 just the desire to act and not attachment to essential
 things.
 Will formation as endeavor is will to strike. This is a
 word explanation.
 This is a term for the energy of right endeavor accompl
ishing its full function.
 Among the 37 constituents of enlightenment, we have four
 great efforts.
 Right endeavor, it is called right endeavor. So four kinds
 of right endeavor.
 That is trying to get rid of the akusala which has arisen
 in the past
 and then avoiding akusala and getting kusala and developing
 kusala.
 So with regard to akusala, two kinds of endeavor and with
 regard to kusala also two kinds of endeavor.
 Possess is furnished with concentration due to zeal and
 with the four instances of the will to strive.
 Road to power basis for success. The meaning is the total
 of consciousness and its remaining concomitant
 except the concentration and the will which are in the
 sense of resolve.
 The road to basis for the concentration due to zeal and
 will to strive associated with the direct knowledge
 consciousness
 which later are themselves termed power success either by
 treatment as production
 or in the sense of succeeding or by treatment in this way.
 Being succeeded and by its means that they are successful,
 they are enriched, promoted.
 It's difficult to understand this passage.
 Now, this word road to power has two words, I mean road and
 power, in Pali, ad and bhada.
 Now, ad means simply concentration and energy.
 So they are here called ad, success of power.
 And bhada really means their basis.
 Although here it is, yes, basis for success. So their basis
.
 And their basis is what?
 Consciousness and its remaining concomitant. It is not
 accurate here.
 The remaining consciousness and concomitant.
 In order to understand this, you have to understand the
 consciousness called supernormal power consciousness.
 When a person achieves this power, there arises in his mind
 a kind of consciousness.
 So that consciousness is called supernormal power
 consciousness.
 Now, along with that consciousness, there are many mental
 factors.
 So among them, the two, some concentration and energy are
 called ad here, called power.
 And then the consciousness and other remaining concomitants
 are called road here, bhada.
 So by the compound word ad bhada is meant the supernormal
 power consciousness and its concomitant.
 So the meaning is, I think the total is not good here.
 And there is no word for total in the original.
 Consciousness and its concomitants accept concentration and
 the will here means actually energy.
 Which, not chanda, actually it is energy.
 Which, in the sense of resolve, also here in the sense of
 basis, not in the sense of resolve,
 in the sense of basis, the road to the concentration, judic
ies and will to strive associated with the direct knowledge
 consciousness.
 Will to strive.
 Will to strive means energy.
 Energy is a life effort.
 It is explained where ad bhada...
 So will to strive really means striving.
 So what common word is the translation of will to strive?
 The word used here is not simple willia, but chanda...
 There are two compound words.
 Chanda samadhi and bhadhanasankara.
 Chanda samadhi means samadhi caused by chanda.
 And then bhadhanasankara means striving.
 Striving formations, that means willia, I mean energy or
 effort.
 They are explained in paragraph 51.
 Concentration will to strive.
 Will as endeavor is will to strive.
 So it is not chanda here, but it is willia.
 So concentration and energy are called ad here and
 consciousness and other concomitants.
 The remaining concomitants are called road or bhada.
 So ad bhada, road to power means the consciousness, direct
 knowledge consciousness and concomitants.
 Other than concentration and energy.
 This is also just word explanation.
 Or alternatively it is arrived at by means of death.
 Thus that is a road and it is reached is the meaning.
 That means the meaning of arrived at.
 So ad bhada, ad yah bhada, resolution of conform.
 This is a term for zeal etc. according as it is said.
 Here ad bhada is made to mean the four things.
 The same for will to do, energy, consciousness and inquiry
 or paññā.
 Well which is the Hollywood for the consciousness in those
 words?
 A cheetah.
 And then a quotation.
 So next paragraph 54.
 The eight steps should be understood as the eight beginning
 with zeal.
 Now actually there are only four roads to success.
 But here the concentration is combined with each.
 So we have here eight.
 So if a vehicle obtains concentration, obtains unification
 of mind supported by zeal,
 then the zeal is not the concentration, the concentration
 is not the zeal.
 The zeal is one, the concentration is another.
 So we get two here.
 Zeal and concentration.
 And in the next sentence if a vehicle is supported by
 energy we have energy and concentration.
 And then in the next we have consciousness and
 concentration.
 And last we have inquiry and concentration.
 So there are eight steps or eight, like we can call them
 eight causes again.
 So these eight steps also one has to perform.
 Just trying to get into jhana and then developing them.
 Now that the paragraph 55, the sixteen routes, the mind's
 unperturbedness should be understood
 in sixteen modes of what this is said.
 So here the sixteen modes are given.
 What are the sixteen routes of success?
 Undejected consciousness, this is one.
 Unrelated consciousness, this is another.
 Unattracted consciousness, so on and so on.
 So there are sixteen.
 Among them, about the middle of the paragraph there is the
 barriers.
 Consciousness, rate of barriers is not perturbed by the
 barrier of defilement.
 Now here consciousness with barriers means the destructions
 coming in between the moments of samadhi.
 Not the moments of samadhi following one after the other.
 There are some buffers between the moments of samadhi.
 So when he wasn't samadhi is not yet good, then he will
 have these destructions of thoughts
 coming in between the samadhi moments.
 So if it is like that, it is called consciousness with
 barriers.
 And if samadhi is one uninterrupted flow, then it is called
 consciousness with barriers.
 So here consciousness really means consciousness
 accompanied by concentration, concentrated consciousness.
 Now 56, of course this meaning is already established by
 the words when his concentrated mind etc.
 But it is stated again for the purpose of showing that the
 first jhana etc. are the, let us try called three.
 There is no word for three in the original and it doesn't
 fit here because there are four blings not three.
 So the first jhana etc. are the planes of success to super
 normal powers,
 basis of steps of and roots of success to super normal
 powers.
 In order to show that it is mentioned again here.
 The first mentioned method is the one given in the sodas.
 But this is how it is given in the pati samvita maga.
 So there are two kinds of treatment.
 The one is given, what is given in the sodas and the other
 is that given in pati samvita maga.
 Pati samvita maga is said to be later than the sodas.
 But it is ascribed to the Venerable Saripoda.
 If it is true then it may not be so much later than the sod
as.
 And pati samvita maga is something like a commentary.
 Not exactly a commentary but looks something resembling a
 commentary on some teachings in the sodas.
 And Venerable Bodhagosa has very great respect for that
 book, pati samvita maga.
 So whenever he wrote about meditation he always quotes pati
 samvita maga.
 So we have many quotations from pati samvita maga and ryus
odhive maga.
 And today also we will find many.
 So it is stated again for the purpose of writing confusion
 in each of the two instances.
 Now 57. He resolves with knowledge.
 These words appear in paragraph 48.
 When he has accomplished these things consisting of planes,
 bases, steps and routes of success to supernormal power,
 then he attains jhana as the basis for direct knowledge and
 emerges from it.
 Now here the author is describing how one goes about until
 one reaches the direct knowledge consciousness.
 So what is the first thing to do?
 He attains jhana as the basis for direct knowledge and
 emerges from it.
 So first he attains first jhana, then second jhana, then
 third jhana, then fourth jhana.
 And make that basis for the direct knowledge and emerges
 from it.
 Then if he wants to become a hundred he does the
 preliminary work thus,
 "Let me become a hundred, let me become a hundred."
 That is the second stage.
 And that is called a preliminary work.
 And that is done by karma vajra consciousness, not rupa vaj
ra consciousness.
 So entering into jhana and when a person is in the jhana
 then rupa vajra consciousness only arises.
 Then he emerges from jhana and does the preliminary work,
 "Let me become a hundred," or "Let me become a hundred."
 Then he does this with cheda belonging to the central
 sphere.
 After which he again attains jhana, again he enters into jh
ana, into the fourth jhana,
 as basis for direct knowledge, then emerges from it and
 then resolves.
 So make determination then.
 He becomes a hundred simultaneously with the resolving
 consciousness.
 That resolving consciousness really means direct knowledge
 consciousness.
 The same method applies in the case of a thousand and so on
.
 So there are four stages.
 First, the basis jhana and then preliminary work.
 Again, basis jhana.
 And last, direct knowledge.
 So there are four stages.
 If he does not succeed in this way, he should do the
 preliminary work again
 and attain, emerge and resolve a second time.
 For it is said in the sanyuta commentary that it is
 allowable to attain once or twice.
 So he may try once, twice.
 Here in the basic jhana consciousness has to sign as its
 object.
 Sign means a counterpart sign.
 So the object of jhana is counterpart sign.
 So here also the object of basic jhana,
 now basic jhana consciousness is the counterpart sign.
 But the preliminary work consciousness have the hundred as
 their object or thousand as their object.
 Preliminary work is done by kamavajra consciousness.
 So the counterpart sign is not the object of these jhana.
 But if he wants to become a hundred, then a hundred is the
 object.
 If he wants to become a thousand, then a thousand is the
 object.
 And these letter are objects as appearances, not as
 concepts.
 So appearances means visible objects.
 Just as visible objects and not as concepts.
 And resolving consciousness has likewise the hundred as its
 object or the thousand as its object.
 Resolving consciousness really means direct knowledge
 consciousness.
 So it has a hundred or a thousand as its object.
 That arises only, once only, next to change of lineage
 consciousness.
 As in the case of absorption consciousness already
 described.
 It is said that the direct knowledge consciousness arise
 only once and then disappears.
 Jhana consciousness at the first attainment, then it arises
 only once and then disappears.
 And later on when that person wants to spend some time with
 Jhana only, then he enters into that Jhana.
 And at that time Jhana consciousness may, one Jhana
 consciousness may follow the other in succession for say
 one hour, two hours or the whole day.
 Now this direct knowledge consciousness arises only once.
 It is actually a kind of full Jhana consciousness, but not
 the same as full Jhana consciousness.
 But it is recognized as the full Jhana consciousness.
 But the difference is it does not take the sign, the
 counterbar sign as object.
 It takes the hundred appearances or a thousand appearances
 or whatever maybe.
 So that is the difference.
 So is it the same object that you have about Jhana?
 Yeah, same as a preliminary.
 Here the preliminary consciousness takes a hundred, a
 thousand as object.
 So the direct knowledge consciousness also takes that
 object.
 And it is fine materials for your consciousness now
 belonging to the full Jhana, right?
 This page will be important for a bit of students when they
 reach the ninth chapter.
 In the ninth chapter the direct knowledge is treated very
 briefly. So you can take something from here.
 I still, I'm not clear about, it seems like that's a
 concept.
 I'm not understanding the difference between appearances.
 Appearance means, you know, paramata, I mean, ultimate
 reality.
 So what it takes is something like color.
 When we see something, we actually see color. We see with
 our eyes color.
 And then we think we see a form or a shape. And that is, we
 think we see them.
 We see with our manodwara, mentally we see the shape or
 form.
 But what we really see is the visible data.
 So in the same way, what the preliminary consciousness
 takes is the visible data.
 Not the concept as a man or a monk or something like that.
 Normally one, he adverts to himself as many or a hundred or
 a thousand or a hundred thousand.
 Having adverted, he resolves with knowledge, let me be many
.
 He becomes many, like the venerable Chula Panchaka.
 And here he adverts, is said with respect only to the
 preliminary work.
 Having adverted, he resolves with knowledge, is said with
 respect to the knowledge of the direct knowledge.
 So here we call Abhinya. Consequently he adverts to many.
 After that he attains with the last one of the preliminary
 work consciousness.
 Here, I think with the last one can mean at the moment of
 the last preliminary work consciousness.
 So I think we should say after he attains, after that he
 attains after the last one of the preliminary work
 consciousness.
 So there are four or three movements of preliminary work
 consciousness and then there is direct knowledge
 consciousness.
 So not with the last one, but after the last one he attains
 the direct knowledge consciousness.
 After emerging from the attainment he again adverts to, let
 me be many.
 After which he resolves by means of this single
 consciousness belonging to the knowledge of direct
 knowledge.
 So direct knowledge consciousness arises only once,
 which has arisen next to the three or four preparatory
 consciousness that have occurred.
 Preparatory consciousness means preliminary consciousness,
 four movements or three movements.
 And which has the name resolve owing to its making the
 decision.
 So the last one is called resolve. This is how the meaning
 should be understood here.
 So in a thought process for Abhinya, there will be four or
 three kama-vajra movements and then the full jhana.
 And that full jhana is called Abhinya, direct knowledge.
 And then life continuum follows.
 And it is so powerful that just by arising once it can
 create miracles.
 Like the Venerable Chula Pantaka is said in order to point
 to a bodily witness.
 Bodily witness really means direct witness, not hearsay,
 something like that.
 So bodily witness of this multiple state, but that must be
 illustrated by the story.
 So here we have the story. I think I have told this story
 many times.
 Chula Pantaka, so he was very, very dull. He was born very
 dull.
 So he could not even learn the four lines of his, four line
 stanza in how many months? Four months.
 So even it is said that he learned one line for four months
. I mean, one month.
 And then he got that one line. Then he goes to the next
 line.
 So when he goes to the next line, he lost the first one.
 So after the fourth month, he had nothing.
 So his brother said, "You are useless in this dispensation
."
 And he expelled him from the monastery. Now his brother was
 an Arahant, but yeah, he seemed to be angry with him.
 So you are useless in this dispensation.
 And here, actually he was not useless. He was going to be
 an Arahant a few moments later.
 But he was not as wise as the Buddha, his elder brother.
 So his opinion was there. His younger brother was useless
 because he could not even learn four lines of his stanza.
 And if he could not learn, then he won't be able to
 practice.
 Because in order to practice, you have to know something.
 You have to learn something from the teacher.
 That is why he said, "You are useless in this dispensation
."
 But Buddha will turn him into a very useful disciple.
 So at that time, the elder had charge of the allocation of
 meal and so on.
 This story is not difficult to understand.
 And then he became an Arahant.
 The next day, when the others went to take meals at Anantam
, because he was left behind,
 but at that time he was an Arahant.
 And then Buddha wanted to let other people know that he had
 become an Arahant.
 When after offering food,
 at the end of the water offering ceremony, he covered his
 bowl, the Buddha.
 That means pouring water on the hand of a Buddha or the one
 who accepts.
 It is a symbolic, like we do now in the water-dropping
 ceremony.
 And that is done before eating here.
 And in Sri Lanka, they still do it this way.
 So laypeople bring food to the monastery or maybe invite
 them to their houses.
 And they offer food to the monks and one of the monks does
 the ceremony.
 And then they were offered in monks age.
 But in our country, we do after eating.
 So there is a difference of customs.
 But here Buddha did before eating.
 So when the guru was being given out at the end of the
 water offering ceremony,
 he covered his bowl.
 So he did not accept guru.
 Then Jyuga asked what it is, bainable sir.
 There is a bhikkhu at the monastery and so on.
 So there is one bhikkhu left.
 So somebody was sent to take him to the place.
 But at that time, now having multiplied himself up to a
 thousand,
 Bhanchakas sat in the pleasant mango wood until the time
 should be announced.
 So he was waiting.
 He creates himself into a thousand monks.
 And then when the people went there, they saw many monks
 and so they went back to the Buddha and the monastery is
 crowded with bhikkhusas.
 So I do not know which of them the Lord is.
 I mean which of them is Jula Bhantaka is.
 So Buddha said go and catch hold of the hem of the rope of
 the first one you see.
 Tell him the master calls you and bring him here.
 He went and got hold of the rope of the elder himself.
 That means he happened to have taken hold of the elder
 himself,
 not of the created images.
 But even if he took hold of the rope of the images,
 then all images would disappear and only the real Jula Bh
antaka would remain.
 But here the man happened to have taken hold of the rope of
 the elder himself.
 At once all the creations vanished and so on.
 And then, so Jula Bhantaka went there, took the meal
 and then Buddha left him to give a Dhamma talk.
 So when he became an Arhan, he was endowed with all super
normal knowledge
 and all jhanas and so on.
 So he was not able to repeat all what the Buddha had taught
.
 So it is not mentioned in this book, but in the Dhamma,
 it is mentioned that Buddha left him to give a Dhamma talk
 and so he gave a very good Dhamma talk.
 Basing his talk was full of the TV to cut.
 I wish you could now memorize.
 Right.
 So this is the story of Jula Bhantaka. We will have many
 stories today.
 The many who were created there were just like the poss
essor of the supernormal power
 because they were created without particular specification.
 If there is specification, then they will appear as
 specified.
 If there is no specification, then all creations will be
 doing the same as the monks does.
 If he is sitting, they will all be sitting. If he is
 walking, they will all be walking.
 But Jula Bhantaka did not do that way.
 So he had to enter into Jhana and then emerge from it, then
 resolve one,
 and then next, and then do Jhana, and then resolve again
 and again.
 May some be walking, may some be sitting, may some be rec
iting, something like that.
 So he had to be very familiar, very adept at entering into
 Jhana and emerging from it.
 So 68. The same method of explanation applied to the clause
,
 "Having been many, he becomes one." But there is this
 difference.
 After this, Bhagavad-gita has thus created a many full
 state, then he against things.
 There is one only.
