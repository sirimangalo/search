1
00:00:00,000 --> 00:00:05,090
 liking, question and answer. Giving up pleasure. Do we need

2
00:00:05,090 --> 00:00:07,600
 to give up all materialistic pleasure?

3
00:00:07,600 --> 00:00:13,020
 No, you do not have to give up any pleasure. This is really

4
00:00:13,020 --> 00:00:15,680
 key. You have to give up your attachment

5
00:00:15,680 --> 00:00:19,130
 to pleasure and that is the difference here. A person can

6
00:00:19,130 --> 00:00:21,200
 experience pleasure without being

7
00:00:21,200 --> 00:00:24,490
 attached to it. There are many mental and material states

8
00:00:24,490 --> 00:00:26,880
 that do not stem from greed or coexist with

9
00:00:26,880 --> 00:00:30,570
 attachment. Attachment is a sort of stress that brings the

10
00:00:30,570 --> 00:00:32,400
 mind out of its state of peace and

11
00:00:32,400 --> 00:00:36,360
 happiness. If you engage in attachment or addiction,

12
00:00:36,360 --> 00:00:39,360
 wanting, desire, etc, you will only become

13
00:00:39,360 --> 00:00:43,140
 increasingly miserable as you compartmentalize reality into

14
00:00:43,140 --> 00:00:45,280
 good and bad. You will not always

15
00:00:45,280 --> 00:00:49,330
 get what you want for the very reason that you have wants.

16
00:00:49,330 --> 00:00:51,600
 If, on the other hand, you are able

17
00:00:51,600 --> 00:00:54,790
 to accept reality for what it is, then it is not a question

18
00:00:54,790 --> 00:00:57,120
 of whether there is pleasure or pain at

19
00:00:57,120 --> 00:01:00,130
 all. You will be able to live in harmony with the world

20
00:01:00,130 --> 00:01:02,720
 around you and in harmony with reality as it

21
00:01:02,720 --> 00:01:06,120
 is. Accepting change, accepting the good and the bad and

22
00:01:06,120 --> 00:01:08,320
 not labeling things as good or bad.

23
00:01:08,320 --> 00:01:11,950
 It is important to understand that you do not have to give

24
00:01:11,950 --> 00:01:14,400
 up anything at all. The point is that you

25
00:01:14,400 --> 00:01:17,300
 are suffering and you are likely not clearly aware of your

26
00:01:17,300 --> 00:01:20,320
 own suffering. Most people are unable to

27
00:01:20,320 --> 00:01:23,260
 see the extent to which their state of existence is

28
00:01:23,260 --> 00:01:26,240
 conducive to stress and suffering. Ultimately,

29
00:01:26,240 --> 00:01:29,050
 we are all suffering. Once we realize that there is

30
00:01:29,050 --> 00:01:31,680
 suffering, we begin to want to find a way out of

31
00:01:31,680 --> 00:01:34,450
 it. So we start looking and once you start looking in

32
00:01:34,450 --> 00:01:36,880
 earnest, you begin to realize that the cause of

33
00:01:36,880 --> 00:01:39,640
 suffering is really your attachments. Attachments to

34
00:01:39,640 --> 00:01:42,160
 pleasure, attachments to being, and attachments

35
00:01:42,160 --> 00:01:45,660
 to not being. Our attachments are a cause of suffering

36
00:01:45,660 --> 00:01:47,680
 because we are not able to accept

37
00:01:47,680 --> 00:01:51,250
 reality for what it is, not able to accept the experience.

38
00:01:51,250 --> 00:01:53,280
 When you realize the stress that you

39
00:01:53,280 --> 00:01:56,680
 are causing, you let go automatically. If you see clearly

40
00:01:56,680 --> 00:01:58,560
 something is causing you stress,

41
00:01:58,560 --> 00:02:02,200
 there is no thought or intention involved in the response.

42
00:02:02,200 --> 00:02:04,960
 It immediately changes who you are.

43
00:02:04,960 --> 00:02:07,650
 Your behavior changes so that you do not repeat your

44
00:02:07,650 --> 00:02:10,080
 mistake. You would never do something when

45
00:02:10,080 --> 00:02:13,150
 you have verified for yourself that it causes you suffering

46
00:02:13,150 --> 00:02:15,120
. That is the nature of the mind.

47
00:02:15,760 --> 00:02:18,630
 We only act the way we do because we think it is going to

48
00:02:18,630 --> 00:02:20,880
 bring us pleasure, happiness, or some

49
00:02:20,880 --> 00:02:24,450
 sort of benefit. You do not ever have to worry about having

50
00:02:24,450 --> 00:02:26,880
 to give something up, thinking, "Oh,

51
00:02:26,880 --> 00:02:30,140
 I am going to have to give up this or that." The point is

52
00:02:30,140 --> 00:02:32,480
 to look closer and see more clearly and

53
00:02:32,480 --> 00:02:35,650
 give up those things that stop you from seeing clearly. Of

54
00:02:35,650 --> 00:02:37,440
 course, give up drugs and alcohol

55
00:02:37,440 --> 00:02:40,200
 because you cannot see objectively when your mind is

56
00:02:40,200 --> 00:02:42,800
 intoxicated. Give up entertainment to the degree

57
00:02:42,800 --> 00:02:45,960
 that it impedes your clear investigation to reality. If you

58
00:02:45,960 --> 00:02:47,920
 do not give it up, you will not be able to

59
00:02:47,920 --> 00:02:51,420
 undertake scientific inquiry. Once you have done this,

60
00:02:51,420 --> 00:02:54,000
 start to look. Do not take it on faith,

61
00:02:54,000 --> 00:02:57,780
 just start looking. The better you understand reality, the

62
00:02:57,780 --> 00:02:59,600
 more you will give up until finally

63
00:02:59,600 --> 00:03:02,610
 you give up everything as a matter of course. There is no

64
00:03:02,610 --> 00:03:04,960
 forcing, pushing, or pulling involved

65
00:03:04,960 --> 00:03:08,880
 at all. It is wisdom. Once you see things as they are, you

66
00:03:08,880 --> 00:03:11,280
 do not want to cling because it is not a

67
00:03:11,280 --> 00:03:14,680
 belief nor is it a theory. It is understanding,

68
00:03:14,680 --> 00:03:18,160
 understanding that no happiness comes from clinging.

