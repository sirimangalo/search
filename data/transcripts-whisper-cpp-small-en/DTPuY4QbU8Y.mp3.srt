1
00:00:00,000 --> 00:00:05,000
 Welcome everyone to our study of the Dhammapada.

2
00:00:05,000 --> 00:00:10,000
 Tonight we'll be continuing with verse number 42, which

3
00:00:10,000 --> 00:00:12,000
 goes as follows.

4
00:00:12,000 --> 00:00:22,210
 "Dhisodhisangyam tangayira vedivapanavedinam micha panihit

5
00:00:22,210 --> 00:00:29,000
angjitam papyo nam tatukare"

6
00:00:29,000 --> 00:00:36,000
 Which means "Dhisodhisang" "dhiso" means an enemy.

7
00:00:36,000 --> 00:00:40,410
 "Dhisodhisangyam tangayira" what an enemy could do to their

8
00:00:40,410 --> 00:00:44,000
 enemy, what one enemy could do to another.

9
00:00:44,000 --> 00:00:52,000
 "Vedivapanavedinam" or one who is vengeful towards another.

10
00:00:52,000 --> 00:01:00,020
 "Micha pan banyitangjitam" the ill-directed mind, or the

11
00:01:00,020 --> 00:01:03,000
 mind that is set wrongly.

12
00:01:03,000 --> 00:01:14,020
 "Papyo nam tatukare" is of greater evil, does greater evil

13
00:01:14,020 --> 00:01:17,000
 than that.

14
00:01:17,000 --> 00:01:21,590
 Some pretty powerful words where the meaning is what an

15
00:01:21,590 --> 00:01:26,470
 enemy could do to an enemy or someone who is seeking

16
00:01:26,470 --> 00:01:27,000
 vengeance.

17
00:01:27,000 --> 00:01:31,610
 Anyone who could ever seek vengeance upon someone else, wre

18
00:01:31,610 --> 00:01:34,000
ak vengeance upon another person.

19
00:01:34,000 --> 00:01:38,760
 The Buddha said, some pretty strong words, that it's not

20
00:01:38,760 --> 00:01:44,200
 possible basically for them to do as much damage as the ill

21
00:01:44,200 --> 00:01:46,000
-directed mind.

22
00:01:46,000 --> 00:01:49,000
 It's worth listening to, no?

23
00:01:49,000 --> 00:01:53,290
 So, but first we have a story. It's not much of a story,

24
00:01:53,290 --> 00:01:55,000
 this story is quite a short one.

25
00:01:55,000 --> 00:01:59,850
 The story goes that Anattapindika, this great lay disciple

26
00:01:59,850 --> 00:02:05,160
 of the Buddha who donated so much and donated Chitavana,

27
00:02:05,160 --> 00:02:08,000
 this Buddhist monastery for the Buddha,

28
00:02:08,000 --> 00:02:18,610
 had a cattle herder, as I call him, the person who looks

29
00:02:18,610 --> 00:02:22,000
 after his cows.

30
00:02:22,000 --> 00:02:27,600
 And this cattle herder, he from time to time would go with

31
00:02:27,600 --> 00:02:32,000
 Anattapindika to listen to the Buddha teach.

32
00:02:32,000 --> 00:02:38,500
 And he gained great faith in the Buddha and so he invited

33
00:02:38,500 --> 00:02:47,000
 the Buddha to come to his house to receive charity,

34
00:02:47,000 --> 00:02:51,570
 or receiving some kind of gift from him. He thought that,

35
00:02:51,570 --> 00:02:53,000
 wow, this is someone worth supporting.

36
00:02:53,000 --> 00:02:57,970
 So he decided he wanted to provide some requisites to the

37
00:02:57,970 --> 00:02:59,000
 Buddha.

38
00:02:59,000 --> 00:03:04,020
 And the Buddha, seeing that this guy had some strong good

39
00:03:04,020 --> 00:03:09,700
 qualities in him and the potential to understand the Dhamma

40
00:03:09,700 --> 00:03:10,000
,

41
00:03:10,000 --> 00:03:16,370
 accepted the invitation and walked all the way to this man

42
00:03:16,370 --> 00:03:20,000
's, this cattle herder's house.

43
00:03:20,000 --> 00:03:24,270
 And for seven days, as I understand, for seven days he

44
00:03:24,270 --> 00:03:34,780
 spent, he went every day to receive the five products of a

45
00:03:34,780 --> 00:03:35,000
 cow.

46
00:03:35,000 --> 00:03:39,670
 So he got milk and butter and cheese and so on, all of

47
00:03:39,670 --> 00:03:42,000
 these good things that come from the cows.

48
00:03:42,000 --> 00:03:46,470
 So he had these cows that he looked after and he was able

49
00:03:46,470 --> 00:03:50,000
 to provide the Buddha with dairy products.

50
00:03:50,000 --> 00:03:53,700
 And the story goes that he offered these to the Buddha

51
00:03:53,700 --> 00:03:55,000
 seven days in a row.

52
00:03:55,000 --> 00:04:00,580
 And on the seventh day he was taking the Buddha back to, he

53
00:04:00,580 --> 00:04:04,000
 invited the Buddha for seven days,

54
00:04:04,000 --> 00:04:06,210
 and on the seventh day he was going to follow the Buddha

55
00:04:06,210 --> 00:04:10,000
 back to Jethavana and he carried the Buddha's bowl.

56
00:04:10,000 --> 00:04:16,110
 Now they got halfway through the forest on the way back to

57
00:04:16,110 --> 00:04:18,000
 Sawati, I guess.

58
00:04:18,000 --> 00:04:23,580
 And suddenly the Buddha turned around and received his bowl

59
00:04:23,580 --> 00:04:29,310
 from Nanda. Nanda was his name, his gopala, the cattle her

60
00:04:29,310 --> 00:04:30,000
der.

61
00:04:30,000 --> 00:04:34,110
 And he received his bowl from him and said, "Nanda, please

62
00:04:34,110 --> 00:04:40,000
 turn back, don't come any further, we'll go by ourselves."

63
00:04:40,000 --> 00:04:44,330
 And the Buddha turned and walked away and Nanda turned

64
00:04:44,330 --> 00:04:46,000
 around to go home.

65
00:04:46,000 --> 00:04:51,050
 And on his way home, some of the monks were coming later,

66
00:04:51,050 --> 00:04:55,000
 they had left the house later, I guess, after the Buddha.

67
00:04:55,000 --> 00:04:59,060
 And notice that what happened was Nanda turned around and

68
00:04:59,060 --> 00:05:06,000
 as he was walking back through the forest, he got,

69
00:05:06,000 --> 00:05:09,000
 he ran into a hunter, there was a hunter who saw him going

70
00:05:09,000 --> 00:05:11,000
 through the forest and thought he was a deer or something,

71
00:05:11,000 --> 00:05:15,510
 and shot him and killed him, shot him with a bow and arrow

72
00:05:15,510 --> 00:05:17,000
 and killed him.

73
00:05:17,000 --> 00:05:20,760
 And some of the monks noticed this, the hunter realized he

74
00:05:20,760 --> 00:05:23,000
 was wrong and I guess just ran away.

75
00:05:23,000 --> 00:05:28,570
 But some of the monks saw this, that here is a guy who had

76
00:05:28,570 --> 00:05:31,000
 done so much good deeds,

77
00:05:31,000 --> 00:05:35,380
 and it's almost as though the Buddha knew what was going to

78
00:05:35,380 --> 00:05:38,000
 happen and it really seemed strange.

79
00:05:38,000 --> 00:05:41,210
 The reason this story is so well remembered and it's

80
00:05:41,210 --> 00:05:46,000
 actually in the commentary is because it's a curious story.

81
00:05:46,000 --> 00:05:51,260
 Here we have someone that the Buddha spent seven days with

82
00:05:51,260 --> 00:05:55,000
 and it's almost as though the Buddha knew.

83
00:05:55,000 --> 00:05:59,230
 And of course, there was this idea going around that the

84
00:05:59,230 --> 00:06:01,000
 Buddha was omniscient.

85
00:06:01,000 --> 00:06:04,600
 So he wouldn't have sent Nanda back if he didn't know what

86
00:06:04,600 --> 00:06:06,000
 was going to happen.

87
00:06:06,000 --> 00:06:09,480
 And yet he purposefully sent Nanda back, kind of like as

88
00:06:09,480 --> 00:06:13,690
 though he knew that he was going to get shot by this hunter

89
00:06:13,690 --> 00:06:14,000
.

90
00:06:14,000 --> 00:06:18,600
 This is the curious part of the story and what leads to

91
00:06:18,600 --> 00:06:20,000
 this verse.

92
00:06:20,000 --> 00:06:22,950
 And so the monks are kind of like, and they heard that the

93
00:06:22,950 --> 00:06:25,810
 Buddha had sent him back and they went and they're kind of

94
00:06:25,810 --> 00:06:26,000
 like,

95
00:06:26,000 --> 00:06:33,970
 "I don't know how to approach this, but Lord Buddha, you

96
00:06:33,970 --> 00:06:39,700
 sent Nanda back knowing that, did you know that this was

97
00:06:39,700 --> 00:06:41,000
 going to happen?"

98
00:06:41,000 --> 00:06:43,620
 "No, if you hadn't have sent him back, you wouldn't have

99
00:06:43,620 --> 00:06:48,000
 gotten shot by the hunter. So what's up?"

100
00:06:48,000 --> 00:06:53,280
 And the Buddha said, "If I hadn't told whether I told him

101
00:06:53,280 --> 00:06:58,280
 to go or didn't tell him to go, whether he had gone north,

102
00:06:58,280 --> 00:07:00,000
 south, east, west, in any of the eight,

103
00:07:00,000 --> 00:07:03,230
 the four directions or the four minor directions, wherever

104
00:07:03,230 --> 00:07:09,090
 he had gone, there's no way he could have escaped from the

105
00:07:09,090 --> 00:07:16,000
 effects of his bad karma."

106
00:07:16,000 --> 00:07:23,470
 And then the Buddha taught this verse. He said, and as he's

107
00:07:23,470 --> 00:07:27,000
 taught elsewhere, you can't escape this karma.

108
00:07:27,000 --> 00:07:34,130
 The point being that the blame that we might place on an

109
00:07:34,130 --> 00:07:42,310
 enemy or on someone seeking vengeance against us is much

110
00:07:42,310 --> 00:07:43,000
 better placed,

111
00:07:43,000 --> 00:07:48,850
 or is properly placed on our own mind, which has caused us

112
00:07:48,850 --> 00:07:53,040
 to arise in such a situation, caused us to give rise to

113
00:07:53,040 --> 00:07:54,000
 such a situation.

114
00:07:54,000 --> 00:08:04,000
 So it's really an interesting story because it requires,

115
00:08:04,000 --> 00:08:11,130
 for understanding it requires a sort of a profound

116
00:08:11,130 --> 00:08:12,000
 understanding of karma,

117
00:08:12,000 --> 00:08:14,720
 not just on a moment to moment level, but you have to

118
00:08:14,720 --> 00:08:18,000
 somehow see that there's a relationship between this hunter

119
00:08:18,000 --> 00:08:22,000
 killing this cattle herder

120
00:08:22,000 --> 00:08:27,890
 and something that the cattle herder has done in the past,

121
00:08:27,890 --> 00:08:31,000
 probably, to this hunter.

122
00:08:31,000 --> 00:08:34,350
 And the even more curious thing about this is we don't know

123
00:08:34,350 --> 00:08:37,460
 what it was, because as with most of the stories, then the

124
00:08:37,460 --> 00:08:38,000
 Buddha says,

125
00:08:38,000 --> 00:08:42,380
 "In the past, this man did this and this and this." But the

126
00:08:42,380 --> 00:08:45,330
 curious note that we have is at the end of the story, the

127
00:08:45,330 --> 00:08:46,000
 commentary says,

128
00:08:46,000 --> 00:08:49,750
 "But the monks never asked the Buddha what this guy had

129
00:08:49,750 --> 00:08:53,000
 done, so we don't know what the karma was."

130
00:08:53,000 --> 00:08:56,590
 It's actually a unique story in that way, because normally

131
00:08:56,590 --> 00:08:59,720
 the monks go up to the Buddha and ask him, "What did Nanda

132
00:08:59,720 --> 00:09:03,000
 do? What did Nanda Gopala do to deserve that?"

133
00:09:03,000 --> 00:09:08,200
 And in this case, they didn't. So first of all, it talks

134
00:09:08,200 --> 00:09:09,000
 about karma.

135
00:09:09,000 --> 00:09:15,250
 And it's sort of an example, a strong example of how this

136
00:09:15,250 --> 00:09:22,410
 verse comes into play, how this verse, the truth of this

137
00:09:22,410 --> 00:09:27,000
 verse, how this verse has meaning.

138
00:09:27,000 --> 00:09:31,840
 Because the point being that not only will your ill-

139
00:09:31,840 --> 00:09:36,710
directed mind hurt you on a momentary level, like if you get

140
00:09:36,710 --> 00:09:38,000
 angry or in pain,

141
00:09:38,000 --> 00:09:42,000
 or if you cling to things you're stressed out about them.

142
00:09:42,000 --> 00:09:46,550
 But it has profound reaching effects that can lead over

143
00:09:46,550 --> 00:09:50,650
 into the next life and can chase you throughout your

144
00:09:50,650 --> 00:09:56,000
 journey in samsara, because of how they affect the mind.

145
00:09:56,000 --> 00:10:07,670
 So it actually maybe requires a little bit of faith, and

146
00:10:07,670 --> 00:10:14,000
 people will start to question this idea of karma and say,

147
00:10:14,000 --> 00:10:18,390
 "Well, here we're entering into a realm that is outside of

148
00:10:18,390 --> 00:10:22,780
 the core of the Buddhist teaching because it's starting to

149
00:10:22,780 --> 00:10:32,450
 deal with faith and belief in some sort of magical karmic

150
00:10:32,450 --> 00:10:34,000
 potency of acts,

151
00:10:34,000 --> 00:10:38,440
 that when you kill someone you magically get punished for

152
00:10:38,440 --> 00:10:43,440
 it." Or that the things that we have done have some

153
00:10:43,440 --> 00:10:48,000
 connection, unseen connection to things in the past.

154
00:10:48,000 --> 00:10:52,460
 But all that really shows, if you understand how Buddhism,

155
00:10:52,460 --> 00:10:56,760
 how karma works, how rebirth works, all that really shows

156
00:10:56,760 --> 00:11:03,370
 is the profound and intrinsic connection between moments of

157
00:11:03,370 --> 00:11:04,000
 consciousness,

158
00:11:04,000 --> 00:11:12,000
 between our acts and the world around us.

159
00:11:12,000 --> 00:11:15,070
 So like they'll say about the weather, they've come to

160
00:11:15,070 --> 00:11:18,060
 realize this about the weather, that how small things in

161
00:11:18,060 --> 00:11:20,820
 one part of the world can have a profound impact on other

162
00:11:20,820 --> 00:11:22,000
 parts of the world.

163
00:11:22,000 --> 00:11:25,210
 They have this saying which is probably not true, that if a

164
00:11:25,210 --> 00:11:28,520
 butterfly flaps its wings in China there's an earthquake in

165
00:11:28,520 --> 00:11:33,000
 America or something, or a tornado in America.

166
00:11:33,000 --> 00:11:39,120
 The point being that it's very easy for things to change

167
00:11:39,120 --> 00:11:44,920
 and something very small or very specific can have an

168
00:11:44,920 --> 00:11:48,000
 effect on the long term.

169
00:11:48,000 --> 00:11:52,400
 It's not difficult to understand really at all when you

170
00:11:52,400 --> 00:11:56,930
 think about how an act affects your mind and how your mind

171
00:11:56,930 --> 00:11:59,000
 affects your future.

172
00:11:59,000 --> 00:12:03,480
 So if you have done bad deeds in the past, if you've hurt

173
00:12:03,480 --> 00:12:08,250
 other people, or if you've clung to, if you're clinging to

174
00:12:08,250 --> 00:12:12,390
 things, it's going to change your whole attitude towards

175
00:12:12,390 --> 00:12:13,000
 life.

176
00:12:13,000 --> 00:12:16,570
 It's going to affect your mind. It's something you'll think

177
00:12:16,570 --> 00:12:18,000
 about on a daily basis.

178
00:12:18,000 --> 00:12:21,230
 And the Buddha said, "Whatever you think about, that's what

179
00:12:21,230 --> 00:12:24,320
 your mind inclines towards." It's a very obvious sort of

180
00:12:24,320 --> 00:12:25,000
 teaching.

181
00:12:25,000 --> 00:12:27,820
 Something we don't think about because we don't really

182
00:12:27,820 --> 00:12:31,550
 understand karma. We don't really get that our thoughts

183
00:12:31,550 --> 00:12:33,000
 affect our lives.

184
00:12:33,000 --> 00:12:38,970
 And yet they absolutely do. When we're stressed about

185
00:12:38,970 --> 00:12:41,780
 something, it changes all of our interactions with other

186
00:12:41,780 --> 00:12:44,000
 people throughout the life, with the world around us,

187
00:12:44,000 --> 00:12:46,000
 throughout our life.

188
00:12:46,000 --> 00:12:48,960
 It changes even the direction of our lives. People who have

189
00:12:48,960 --> 00:12:52,860
 gone through traumatic experiences, of course it changes

190
00:12:52,860 --> 00:12:54,000
 their whole life.

191
00:12:54,000 --> 00:13:03,730
 It changes their outlook. It changes their potential, their

192
00:13:03,730 --> 00:13:08,000
 possibilities in their lives.

193
00:13:08,000 --> 00:13:11,600
 The things that we do, the things that we engage in, change

194
00:13:11,600 --> 00:13:15,000
 us. People who are addicted to things have to spend money

195
00:13:15,000 --> 00:13:18,570
 and put resources into the things that they want, that they

196
00:13:18,570 --> 00:13:20,000
 might have put elsewhere.

197
00:13:20,000 --> 00:13:25,870
 They have to dedicate time to their addiction, and energy,

198
00:13:25,870 --> 00:13:30,000
 and brain cells, thoughts, and so on.

199
00:13:30,000 --> 00:13:37,030
 And so the effect can actually get bigger over the long

200
00:13:37,030 --> 00:13:41,220
 term, so that the effect that a karma has in the short term

201
00:13:41,220 --> 00:13:45,640
 might be minuscule compared to the effect that it has later

202
00:13:45,640 --> 00:13:48,000
 on in life or even into the next life.

203
00:13:48,000 --> 00:13:51,110
 Because of course as Buddhists we're not thinking of the

204
00:13:51,110 --> 00:13:54,500
 next life as something separate from this life. It's a

205
00:13:54,500 --> 00:13:56,000
 continuation.

206
00:13:56,000 --> 00:14:00,830
 There's a shift at the moment of death, but only because we

207
00:14:00,830 --> 00:14:05,610
've entered into this state of being born and die, this

208
00:14:05,610 --> 00:14:12,440
 human state that is very coarse and requires or associates

209
00:14:12,440 --> 00:14:16,000
 with the physical body.

210
00:14:16,000 --> 00:14:22,330
 Actually at the moment of death nothing happens. Life just

211
00:14:22,330 --> 00:14:27,000
 continues, and our mind just continues.

212
00:14:27,000 --> 00:14:31,340
 Now that's the part that has to do with karma. When we're

213
00:14:31,340 --> 00:14:38,650
 talking about the mind and the potency of the mind, why is

214
00:14:38,650 --> 00:14:43,000
 the mind so potent in creating karma?

215
00:14:43,000 --> 00:14:49,850
 Once we accept the fact that our bad deeds have an effect,

216
00:14:49,850 --> 00:14:56,480
 why would it be that the deeds of others are less potent

217
00:14:56,480 --> 00:14:59,000
 than our own minds?

218
00:14:59,000 --> 00:15:03,310
 And further, why is it the mind that the Buddha is focusing

219
00:15:03,310 --> 00:15:05,000
 on and not the deeds?

220
00:15:05,000 --> 00:15:10,500
 So people are still asking me, why is it that when you do

221
00:15:10,500 --> 00:15:15,000
 something it doesn't have karmic potency?

222
00:15:15,000 --> 00:15:18,320
 We have this idea that karma is some kind of magic because

223
00:15:18,320 --> 00:15:21,930
 we've been brought up generally in the West, and we've been

224
00:15:21,930 --> 00:15:25,480
 brought up to think in terms of God, to think in terms of

225
00:15:25,480 --> 00:15:34,000
 nature as having some kind of watchful or some kind of...

226
00:15:34,000 --> 00:15:42,700
 some kind of power to affect and to intervene with our

227
00:15:42,700 --> 00:15:44,000
 lives.

228
00:15:44,000 --> 00:15:46,990
 So we think when you step on an ant, bad, you've done a bad

229
00:15:46,990 --> 00:15:50,960
 deed, right? Because we're used to sin being something that

230
00:15:50,960 --> 00:15:52,000
 God decides.

231
00:15:52,000 --> 00:15:57,690
 Is it sinful or is it right? And he's watching it like

232
00:15:57,690 --> 00:16:00,000
 Santa Claus.

233
00:16:00,000 --> 00:16:05,740
 But karma is absolutely not that. Karma is the effect that

234
00:16:05,740 --> 00:16:10,310
 something has on your mind, or the effect that something

235
00:16:10,310 --> 00:16:14,770
 has on the choices you make and on your state of mind into

236
00:16:14,770 --> 00:16:16,000
 the future.

237
00:16:16,000 --> 00:16:21,000
 And that which has the biggest, the most profound effect,

238
00:16:21,000 --> 00:16:25,000
 or really the only effect, is your own mind.

239
00:16:25,000 --> 00:16:31,110
 Another person, the harm that an enemy can do to you, or

240
00:16:31,110 --> 00:16:37,290
 someone who wants to take out their vengeance on you, is

241
00:16:37,290 --> 00:16:41,000
 actually absolutely zero.

242
00:16:41,000 --> 00:16:46,960
 The harm that they can perform directly upon you is none.

243
00:16:46,960 --> 00:16:50,990
 It's not even little. It's not a little. It's not something

244
00:16:50,990 --> 00:16:53,000
 you can mitigate. It's zero.

245
00:16:53,000 --> 00:16:57,590
 They can't... no one can hurt you. No one can cause harm to

246
00:16:57,590 --> 00:16:59,000
 another.

247
00:16:59,000 --> 00:17:01,450
 So what the Buddha says is actually an understatement of

248
00:17:01,450 --> 00:17:06,280
 the fact. There's no comparison between the harm that an

249
00:17:06,280 --> 00:17:11,380
 enemy can do to you and the harm that you can do to

250
00:17:11,380 --> 00:17:13,000
 yourself.

251
00:17:13,000 --> 00:17:20,060
 And this is due to the theory, or the Buddhist theory,

252
00:17:20,060 --> 00:17:26,590
 which is accepted and not accepted, not accepted by many of

253
00:17:26,590 --> 00:17:33,000
 you, that the mind is apart from the body.

254
00:17:33,000 --> 00:17:39,100
 That it's not a physical process. You're not locked into

255
00:17:39,100 --> 00:17:42,000
 your environment.

256
00:17:42,000 --> 00:17:46,280
 We react to... the mind reacts to the environment. It

257
00:17:46,280 --> 00:17:53,000
 reacts either in a positive or a negative or a neutral way.

258
00:17:53,000 --> 00:17:56,340
 So when we experience something painful, we generally tend

259
00:17:56,340 --> 00:17:59,670
 to react in a negative way. When we experience something

260
00:17:59,670 --> 00:18:02,000
 pleasant, we react in a positive way.

261
00:18:02,000 --> 00:18:07,630
 The body reacts, but the mind also reacts. And the key to

262
00:18:07,630 --> 00:18:12,850
 understanding and to practicing Buddhism is realizing that

263
00:18:12,850 --> 00:18:16,000
 these two things are separate.

264
00:18:16,000 --> 00:18:20,350
 That the mind need not react in a negative or a positive

265
00:18:20,350 --> 00:18:26,000
 way. The mind can react in an objective way to reality.

266
00:18:26,000 --> 00:18:31,720
 If and when the mind is aware of the pain as just pain,

267
00:18:31,720 --> 00:18:36,000
 then the pain will not harm the mind.

268
00:18:36,000 --> 00:18:40,890
 If when someone yells at us or scolds us and we see it

269
00:18:40,890 --> 00:18:49,720
 simply as hearing, we understand it to be sound arising at

270
00:18:49,720 --> 00:18:51,000
 the ear.

271
00:18:51,000 --> 00:19:01,520
 Then we... and observation proves this. Or if you test this

272
00:19:01,520 --> 00:19:04,950
 hypothesis, if you test it out for yourself, you can see

273
00:19:04,950 --> 00:19:06,000
 this for yourself.

274
00:19:06,000 --> 00:19:11,260
 If you listen to the sound of my voice and just take it for

275
00:19:11,260 --> 00:19:15,000
 sound, hearing, hearing...

276
00:19:15,000 --> 00:19:19,250
 Then you find that any other judgments or thoughts you

277
00:19:19,250 --> 00:19:23,000
 might have about what I'm saying disappear.

278
00:19:23,000 --> 00:19:27,510
 Any boredom or aversion to what I'm saying or any

279
00:19:27,510 --> 00:19:32,820
 attachment or desire or appreciation of what I'm saying

280
00:19:32,820 --> 00:19:34,000
 melts away.

281
00:19:34,000 --> 00:19:37,500
 And you simply know it for sound. And you find that

282
00:19:37,500 --> 00:19:43,000
 suddenly you're in the realm of peace and of contentment.

283
00:19:43,000 --> 00:19:47,610
 You're unshaken by what the person is saying. If they're

284
00:19:47,610 --> 00:19:52,730
 saying something nasty or unpleasant, even if they're

285
00:19:52,730 --> 00:19:58,000
 hitting you or beating you.

286
00:19:58,000 --> 00:20:04,110
 It's actually the only way to be free from suffering is to

287
00:20:04,110 --> 00:20:07,000
 free yourself from within.

288
00:20:07,000 --> 00:20:12,830
 Because you can't be free from enemies, you can't be free

289
00:20:12,830 --> 00:20:18,000
 from harsh speech, you can't be free from the cold and heat

290
00:20:18,000 --> 00:20:22,000
 and hunger and thirst and sickness and old age and death.

291
00:20:22,000 --> 00:20:24,000
 We can't be free from these things.

292
00:20:24,000 --> 00:20:30,240
 So there's only two ways to be free from suffering. It's

293
00:20:30,240 --> 00:20:33,470
 not have any of these things which are ubiquitous, which

294
00:20:33,470 --> 00:20:38,000
 are an intrinsic part of life, or overcome them.

295
00:20:38,000 --> 00:20:42,000
 Be free from them. Free yourself, free your mind from them.

296
00:20:42,000 --> 00:20:44,000
 And this is what the Buddha is referring to.

297
00:20:44,000 --> 00:20:48,870
 This is the key theory that we have in Buddhism. You need

298
00:20:48,870 --> 00:20:55,000
 to accept this in order to practice Buddhism correctly.

299
00:20:55,000 --> 00:20:59,460
 It's key to understand that you can change the way you

300
00:20:59,460 --> 00:21:04,450
 react to reality. You need not react. You can interact

301
00:21:04,450 --> 00:21:07,000
 objectively with reality.

302
00:21:07,000 --> 00:21:12,740
 This is why the Buddha said the mind that is set wrongly,

303
00:21:12,740 --> 00:21:18,530
 the mind that is set in a bad way, is of much greater or

304
00:21:18,530 --> 00:21:24,500
 incomparably greater harm to the individual than anyone

305
00:21:24,500 --> 00:21:28,000
 else could be, than an enemy could be.

306
00:21:28,000 --> 00:21:34,810
 And furthermore, what this means, this means actually more

307
00:21:34,810 --> 00:21:38,680
 than just karma. What the Buddha is talking about here is

308
00:21:38,680 --> 00:21:40,000
 not just someone doing a bad deed.

309
00:21:40,000 --> 00:21:44,060
 What he's talking about is setting the mind in a bad way,

310
00:21:44,060 --> 00:21:48,000
 which is actually even worse than doing a bad deed.

311
00:21:48,000 --> 00:21:51,230
 So up until now we're just talking about bad karma. But

312
00:21:51,230 --> 00:21:54,360
 even worse than bad karma, there's something even worse

313
00:21:54,360 --> 00:21:55,000
 than that.

314
00:21:55,000 --> 00:21:58,750
 If you kill someone, if you steal, if you do bad things, it

315
00:21:58,750 --> 00:22:01,000
's setting your mind in a bad way.

316
00:22:01,000 --> 00:22:09,720
 "Micha panihita" means set or sent in a wrong way. You can

317
00:22:09,720 --> 00:22:13,000
 think it refers to wrong view.

318
00:22:13,000 --> 00:22:18,640
 So there are three kinds of wrong. One is wrong perception,

319
00:22:18,640 --> 00:22:24,060
 another is wrong thought, and the third is wrong view.

320
00:22:24,060 --> 00:22:25,000
 Wrong view is the worst.

321
00:22:25,000 --> 00:22:30,090
 Wrong perception is the least serious. If someone says

322
00:22:30,090 --> 00:22:33,070
 something to you and you get angry right away, that's wrong

323
00:22:33,070 --> 00:22:35,000
 perception. You perceive it as bad.

324
00:22:35,000 --> 00:22:37,530
 But when you start thinking about it, that starts to get

325
00:22:37,530 --> 00:22:40,730
 worse. You think, "Oh, what did that person do? That was

326
00:22:40,730 --> 00:22:45,000
 really mean of them and so on. Really nasty of them."

327
00:22:45,000 --> 00:22:47,640
 That's worse because then you start to think how you're

328
00:22:47,640 --> 00:22:50,000
 going to hurt them and hurt them back and so on.

329
00:22:50,000 --> 00:22:53,490
 But when you have the view, when you set yourself in the

330
00:22:53,490 --> 00:22:57,760
 idea, "That person is evil. I didn't deserve that. They

331
00:22:57,760 --> 00:22:59,000
 deserve to be punished."

332
00:22:59,000 --> 00:23:05,080
 When you have the wrong views. Even as Buddhists, we all

333
00:23:05,080 --> 00:23:09,000
 have these first two. But we need not have the third.

334
00:23:09,000 --> 00:23:12,800
 And as Buddhists, we generally don't because this is what

335
00:23:12,800 --> 00:23:16,460
 we're taught. We're taught that, "No, it's not good for you

336
00:23:16,460 --> 00:23:18,000
 to hurt other people."

337
00:23:18,000 --> 00:23:23,130
 What did someone say? "Treat other people well, not because

338
00:23:23,130 --> 00:23:27,000
 they deserve to be happy, but because you do."

339
00:23:27,000 --> 00:23:30,150
 Which I think is very pertinent. I mean, of course, you can

340
00:23:30,150 --> 00:23:32,450
 think they deserve to be happy, but that's not the real

341
00:23:32,450 --> 00:23:33,000
 reason.

342
00:23:33,000 --> 00:23:36,700
 The real reason is, "Look, if you get angry at them, it's

343
00:23:36,700 --> 00:23:40,320
 not good for you. You don't get anywhere by hurting other

344
00:23:40,320 --> 00:23:43,990
 people. You don't help them, obviously. You don't help

345
00:23:43,990 --> 00:23:45,000
 yourself."

346
00:23:45,000 --> 00:23:48,190
 And it's pertinent because it's how the mind reacts. The

347
00:23:48,190 --> 00:23:51,400
 mind is looking for happiness. It's not really looking for

348
00:23:51,400 --> 00:23:53,000
 happiness in other people.

349
00:23:53,000 --> 00:23:57,280
 We help other people because it makes us happy. If it didn

350
00:23:57,280 --> 00:24:04,890
't make us happy, it wouldn't excite the mind. The mind

351
00:24:04,890 --> 00:24:09,000
 wouldn't be pleased to continue it.

352
00:24:09,000 --> 00:24:14,160
 So if you want to really encourage your mind and really get

353
00:24:14,160 --> 00:24:18,000
 the attention of your heart, you have to put it this way

354
00:24:18,000 --> 00:24:21,250
 and say, "Look, let's not get angry, not because they don't

355
00:24:21,250 --> 00:24:23,690
 deserve it, but because we don't deserve it. We don't

356
00:24:23,690 --> 00:24:29,000
 deserve to suffer from this. So let us stop ourselves."

357
00:24:29,000 --> 00:24:31,300
 Because we have this idea, because we have this

358
00:24:31,300 --> 00:24:34,280
 understanding, and this is the understanding we try to give

359
00:24:34,280 --> 00:24:37,610
 to Buddhists and to meditators, we're free from this

360
00:24:37,610 --> 00:24:41,000
 wrongly directed mind most of the time.

361
00:24:41,000 --> 00:24:45,950
 Still, we have to always be on guard. When we're in

362
00:24:45,950 --> 00:24:50,000
 training, this is something important to understand.

363
00:24:50,000 --> 00:24:55,740
 Guard against your mind, first of all, from having bad

364
00:24:55,740 --> 00:25:01,330
 perception at the very core. You're best off when you can

365
00:25:01,330 --> 00:25:06,300
 just perceive things as they are and not misperceive things

366
00:25:06,300 --> 00:25:11,000
 as good, as bad as me, as mine, as right, as wrong.

367
00:25:11,000 --> 00:25:14,160
 But even when you do, then be careful not to let it come

368
00:25:14,160 --> 00:25:17,780
 into your thoughts. Be aware of the anger, be aware of the

369
00:25:17,780 --> 00:25:21,260
 wanting, be aware of the desire. Don't let your mind start

370
00:25:21,260 --> 00:25:24,190
 thinking about how you're going to get what you want, how

371
00:25:24,190 --> 00:25:27,000
 you're going to chase away what you don't want.

372
00:25:27,000 --> 00:25:30,090
 And if it does come to thoughts, try to be aware of the

373
00:25:30,090 --> 00:25:33,680
 thoughts and don't let them become views and think, "Yeah,

374
00:25:33,680 --> 00:25:37,000
 I deserve that. That's right for me to get that."

375
00:25:37,000 --> 00:25:41,300
 Or, "I don't deserve that. This is wrong and I'm being

376
00:25:41,300 --> 00:25:45,560
 unjustly treated," and so on. Not because they don't

377
00:25:45,560 --> 00:25:49,000
 deserve it, but because you don't deserve it.

378
00:25:49,000 --> 00:25:53,290
 Because we don't want to suffer. So let's stop making

379
00:25:53,290 --> 00:25:56,140
 ourselves suffer. We have to see that these things are

380
00:25:56,140 --> 00:25:57,000
 causing us suffering.

381
00:25:57,000 --> 00:26:00,760
 And the worst is this third one, when you set your mind in

382
00:26:00,760 --> 00:26:03,000
 the wrong way. This is what the Buddha is talking about.

383
00:26:03,000 --> 00:26:06,250
 This is the key to this verse. Something for us all to

384
00:26:06,250 --> 00:26:07,000
 remember.

385
00:26:07,000 --> 00:26:10,570
 Whenever you think your problems are other people and the

386
00:26:10,570 --> 00:26:14,200
 situation in the world around you, never forget what the

387
00:26:14,200 --> 00:26:15,000
 Buddha said.

388
00:26:15,000 --> 00:26:20,590
 "Dhisodhisangyam dangka-gira viri-vapan viri-nam. Mitya-p

389
00:26:20,590 --> 00:26:24,000
ani-hitang-jitam-papi-onam-dattangkari."

390
00:26:24,000 --> 00:26:29,530
 An enemy as an enemy would do to an enemy, or one seeking

391
00:26:29,530 --> 00:26:36,490
 vengeance would do to another. The wrongly directed mind is

392
00:26:36,490 --> 00:26:43,000
 far, far...will do far, far greater evil than any of those.

393
00:26:43,000 --> 00:26:48,590
 So that's the Dhammapada verse for today. Thank you all for

394
00:26:48,590 --> 00:26:52,000
 listening and tune in next time.

