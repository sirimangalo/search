1
00:00:00,000 --> 00:00:03,000
 Hello and welcome back to Ask a Monk.

2
00:00:03,000 --> 00:00:08,840
 Now I'd like to answer a question about pain in meditation.

3
00:00:08,840 --> 00:00:13,190
 When you feel a great amount of pain during your meditation

4
00:00:13,190 --> 00:00:17,240
, is it necessary to sit still

5
00:00:17,240 --> 00:00:20,040
 and ensure that you never move?

6
00:00:20,040 --> 00:00:22,620
 If a person moves, is there something wrong with that?

7
00:00:22,620 --> 00:00:27,880
 Or is the point of meditation to be able to not move?

8
00:00:27,880 --> 00:00:31,240
 Is it wrong practice if you move your body?

9
00:00:31,240 --> 00:00:33,080
 And this is a very simple question with a very simple

10
00:00:33,080 --> 00:00:34,880
 answer, but it actually has some

11
00:00:34,880 --> 00:00:35,880
 profound implications.

12
00:00:35,880 --> 00:00:40,040
 So I'm going to take a little bit of time to answer it in

13
00:00:40,040 --> 00:00:41,360
 some detail.

14
00:00:41,360 --> 00:00:46,720
 The simple answer first is yes indeed you can move.

15
00:00:46,720 --> 00:00:49,760
 There's nothing intrinsically wrong with moving your body.

16
00:00:49,760 --> 00:00:51,640
 You know, we move it all the time.

17
00:00:51,640 --> 00:00:55,250
 The problem here, of course, that we have to understand is

18
00:00:55,250 --> 00:00:56,880
 that moving based on pain

19
00:00:56,880 --> 00:00:59,640
 creates aversion towards the pain.

20
00:00:59,640 --> 00:01:04,360
 Now that's all very well in theory.

21
00:01:04,360 --> 00:01:08,030
 You shouldn't move because if you move you're moving based

22
00:01:08,030 --> 00:01:09,080
 on aversion.

23
00:01:09,080 --> 00:01:12,910
 But it can actually go the other way where you crush your

24
00:01:12,910 --> 00:01:15,080
 aversion to the pain and you

25
00:01:15,080 --> 00:01:17,040
 force yourself to sit.

26
00:01:17,040 --> 00:01:23,850
 In fact creating more tension and more stress because there

27
00:01:23,850 --> 00:01:25,960
's the forcing.

28
00:01:25,960 --> 00:01:31,240
 There's the not complying with nature.

29
00:01:31,240 --> 00:01:35,060
 And in fact sometimes nature dictates that it's necessary

30
00:01:35,060 --> 00:01:35,840
 to move.

31
00:01:35,840 --> 00:01:39,720
 The appropriate reaction, the physical reaction is to move.

32
00:01:39,720 --> 00:01:43,120
 Once you get on in the meditation, this will change.

33
00:01:43,120 --> 00:01:45,240
 First of all there will be less pain.

34
00:01:45,240 --> 00:01:48,710
 Second of all it will be, it will bring less stress and

35
00:01:48,710 --> 00:01:50,080
 tension and less of a desire to

36
00:01:50,080 --> 00:01:51,080
 move.

37
00:01:51,080 --> 00:01:53,710
 The pain will become just another sensation and the mind

38
00:01:53,710 --> 00:01:55,160
 won't say something's wrong or

39
00:01:55,160 --> 00:01:56,380
 feel that something's wrong.

40
00:01:56,380 --> 00:01:59,690
 It will just see it as a sensation and be content with it

41
00:01:59,690 --> 00:02:00,940
 and not upset by it.

42
00:02:00,940 --> 00:02:02,960
 So you won't need to move.

43
00:02:02,960 --> 00:02:06,390
 Now in the beginning, this isn't the case, in the beginning

44
00:02:06,390 --> 00:02:07,960
 it brings more stress and

45
00:02:07,960 --> 00:02:10,540
 tension and really great problems.

46
00:02:10,540 --> 00:02:15,990
 So what we should do is be mindful of it and when it first

47
00:02:15,990 --> 00:02:19,120
 comes we should certainly try

48
00:02:19,120 --> 00:02:22,910
 to be as mindful of it as possible and not move without

49
00:02:22,910 --> 00:02:25,120
 immediately moving our bodies

50
00:02:25,120 --> 00:02:28,410
 because this is a great teacher for us and this is what I'd

51
00:02:28,410 --> 00:02:29,560
 like to get into.

52
00:02:29,560 --> 00:02:35,020
 But the basic technique is to say to ourselves, "Pain, pain

53
00:02:35,020 --> 00:02:38,240
, pain, just quietly, not out loud

54
00:02:38,240 --> 00:02:41,080
 but in the mind, just focusing on the pain and reminding

55
00:02:41,080 --> 00:02:42,840
 ourselves, 'Hey, this is just

56
00:02:42,840 --> 00:02:45,360
 pain, there's nothing wrong with it, it's not a negative

57
00:02:45,360 --> 00:02:46,800
 experience, there's nothing

58
00:02:46,800 --> 00:02:49,240
 intrinsically bad about pain.'

59
00:02:49,240 --> 00:02:52,960
 It's, in terms of the universe, really doesn't care whether

60
00:02:52,960 --> 00:02:54,640
 you're in pain or not and so

61
00:02:54,640 --> 00:02:58,880
 it's not something that is intrinsically negative.

62
00:02:58,880 --> 00:03:01,730
 What is negative is your negative reactions to it, your

63
00:03:01,730 --> 00:03:03,400
 anger, your frustration, your

64
00:03:03,400 --> 00:03:06,420
 thinking that it's bad, that it's a problem, that you have

65
00:03:06,420 --> 00:03:07,920
 to do something about it.

66
00:03:07,920 --> 00:03:10,940
 So when you see it simply as pain and all of that goes away

67
00:03:10,940 --> 00:03:12,680
, this is really the truth,

68
00:03:12,680 --> 00:03:14,520
 it's something that you should try.

69
00:03:14,520 --> 00:03:16,870
 These videos are not meant to just be, "Oh, that sounds

70
00:03:16,870 --> 00:03:18,160
 nice, I agree with that," and

71
00:03:18,160 --> 00:03:21,800
 then you go home or go back to Facebook or whatever.

72
00:03:21,800 --> 00:03:23,600
 These are something you should put into practice.

73
00:03:23,600 --> 00:03:27,550
 So when I say this, try it and find out for yourself

74
00:03:27,550 --> 00:03:31,440
 because it really has wonderful consequences.

75
00:03:31,440 --> 00:03:33,960
 Any kind of pain that you come up with in your life,

76
00:03:33,960 --> 00:03:35,640
 suddenly it's no longer a great

77
00:03:35,640 --> 00:03:38,480
 problem or a great difficulty.

78
00:03:38,480 --> 00:03:41,750
 It's something that you can deal with mindfulness and

79
00:03:41,750 --> 00:03:44,280
 clarity and wisdom and not have to suffer

80
00:03:44,280 --> 00:03:46,280
 from.

81
00:03:46,280 --> 00:03:48,830
 Once that becomes too much, in the beginning that's very

82
00:03:48,830 --> 00:03:50,260
 difficult to do and you're not

83
00:03:50,260 --> 00:03:53,070
 really mindful, you're mostly just pain, pain and you're

84
00:03:53,070 --> 00:03:54,680
 really angry about it anyway.

85
00:03:54,680 --> 00:03:58,270
 So saying pain isn't really acknowledging, it's reinforcing

86
00:03:58,270 --> 00:04:00,440
 the anger and the hate.

87
00:04:00,440 --> 00:04:03,300
 So when it gets to that point where it's overwhelming and

88
00:04:03,300 --> 00:04:05,220
 you feel like you have to move, then

89
00:04:05,220 --> 00:04:06,300
 just move mindfully.

90
00:04:06,300 --> 00:04:09,640
 You can lift your leg when moving your hand, moving, say to

91
00:04:09,640 --> 00:04:11,520
 yourself, moving, placing or

92
00:04:11,520 --> 00:04:15,650
 grasping, lifting, moving, placing or you can just move

93
00:04:15,650 --> 00:04:17,800
 your leg without, if you're sitting

94
00:04:17,800 --> 00:04:22,750
 in this position, just lifting, moving, placing, just do it

95
00:04:22,750 --> 00:04:25,600
 mindfully and it becomes a meditation

96
00:04:25,600 --> 00:04:28,280
 because eventually our whole life should become meditation.

97
00:04:28,280 --> 00:04:32,150
 We should try to be as mindful as we can in our daily lives

98
00:04:32,150 --> 00:04:32,240
.

99
00:04:32,240 --> 00:04:35,280
 When we eat, we're mindful, when we drink, we're mindful.

100
00:04:35,280 --> 00:04:36,600
 Everything we do, we know what we're doing.

101
00:04:36,600 --> 00:04:40,720
 We move our hands, this should be moving, moving, shaking.

102
00:04:40,720 --> 00:04:42,520
 When we talk, we should know that we're talking.

103
00:04:42,520 --> 00:04:43,520
 What is this feeling?

104
00:04:43,520 --> 00:04:45,400
 My lips moving.

105
00:04:45,400 --> 00:04:48,380
 And to that extent, you can be mindful.

106
00:04:48,380 --> 00:04:50,970
 When you're listening, you can say hearing, hearing and

107
00:04:50,970 --> 00:04:52,480
 acknowledging the sound and you'll

108
00:04:52,480 --> 00:04:55,460
 find that you really understand the meaning much better and

109
00:04:55,460 --> 00:04:56,480
 you get less caught up in

110
00:04:56,480 --> 00:04:59,680
 your own emotions and judgments and so on.

111
00:04:59,680 --> 00:05:02,920
 So this is the basic technique.

112
00:05:02,920 --> 00:05:07,710
 Now the theory behind pain and how it's such a good teacher

113
00:05:07,710 --> 00:05:10,040
 is that eventually pain is one

114
00:05:10,040 --> 00:05:16,240
 of the best teachers that we have because pain is something

115
00:05:16,240 --> 00:05:18,440
 that we don't like.

116
00:05:18,440 --> 00:05:20,560
 It's our aversion.

117
00:05:20,560 --> 00:05:24,280
 It's our problem.

118
00:05:24,280 --> 00:05:28,030
 If it weren't for pain, if it weren't for things like pain,

119
00:05:28,030 --> 00:05:29,640
 then we would never need

120
00:05:29,640 --> 00:05:30,640
 to practice meditation.

121
00:05:30,640 --> 00:05:33,680
 We would never think to come and practice meditation.

122
00:05:33,680 --> 00:05:34,680
 Why would we?

123
00:05:34,680 --> 00:05:35,680
 What's the point?

124
00:05:35,680 --> 00:05:38,230
 We're wasting all this time when we can be out enjoying

125
00:05:38,230 --> 00:05:38,680
 life.

126
00:05:38,680 --> 00:05:41,090
 But the problem is that in our life, it's not all fun and

127
00:05:41,090 --> 00:05:41,600
 games.

128
00:05:41,600 --> 00:05:44,510
 And in fact, our clinging leads us to have suffering

129
00:05:44,510 --> 00:05:46,680
 because we don't like certain things.

130
00:05:46,680 --> 00:05:49,320
 We're not content with thinking about the good things.

131
00:05:49,320 --> 00:05:52,270
 And so when a bad thing comes, it makes us angry because it

132
00:05:52,270 --> 00:05:53,600
's not what we want and so

133
00:05:53,600 --> 00:05:54,680
 on.

134
00:05:54,680 --> 00:06:00,690
 So what we're basically doing here is learning how to live

135
00:06:00,690 --> 00:06:03,600
 with it and how to see it for

136
00:06:03,600 --> 00:06:06,310
 what it is and see it simply as another experience, not as

137
00:06:06,310 --> 00:06:08,240
 a bad thing and not seeing other things

138
00:06:08,240 --> 00:06:10,200
 as good things.

139
00:06:10,200 --> 00:06:14,330
 And so the theory here is that most people understand, it

140
00:06:14,330 --> 00:06:17,000
 has to do with how people understand

141
00:06:17,000 --> 00:06:18,800
 suffering.

142
00:06:18,800 --> 00:06:23,050
 An ordinary person understands suffering to be, and

143
00:06:23,050 --> 00:06:25,840
 suffering here is of course the big

144
00:06:25,840 --> 00:06:26,840
 elephant in the room.

145
00:06:26,840 --> 00:06:30,080
 It's what we as Buddhists deal with.

146
00:06:30,080 --> 00:06:33,180
 It's what we talk about, what we teach, and yet always have

147
00:06:33,180 --> 00:06:34,680
 a very difficult, a great

148
00:06:34,680 --> 00:06:36,760
 amount of difficulty talking about.

149
00:06:36,760 --> 00:06:40,090
 But this is because most people understand suffering to be

150
00:06:40,090 --> 00:06:41,600
 the pain and suffering to

151
00:06:41,600 --> 00:06:42,600
 be a feeling.

152
00:06:42,600 --> 00:06:44,360
 This is what is a dukkha vedana.

153
00:06:44,360 --> 00:06:47,440
 Dukkha vedana means dukkha is suffering, vedana is the

154
00:06:47,440 --> 00:06:48,240
 feeling.

155
00:06:48,240 --> 00:06:50,860
 So we understand dukkha to be a vedana, to be a feeling

156
00:06:50,860 --> 00:06:51,720
 that you get.

157
00:06:51,720 --> 00:06:53,760
 You get this feeling, that's dukkha, that's suffering.

158
00:06:53,760 --> 00:06:55,240
 You get that feeling, that's suffering.

159
00:06:55,240 --> 00:06:58,080
 You get another feeling and it's not suffering.

160
00:06:58,080 --> 00:07:03,240
 So we categorize our experiences.

161
00:07:03,240 --> 00:07:06,360
 This one is suffering, this one is not suffering.

162
00:07:06,360 --> 00:07:12,430
 And as a result, our means of overcoming suffering, of

163
00:07:12,430 --> 00:07:16,200
 being free from suffering is to find a

164
00:07:16,200 --> 00:07:19,200
 way to only have these experiences and to never have these

165
00:07:19,200 --> 00:07:20,160
 experiences.

166
00:07:20,160 --> 00:07:22,520
 Doesn't that sound normal?

167
00:07:22,520 --> 00:07:24,760
 That sounds, yeah, that's how we get rid of suffering.

168
00:07:24,760 --> 00:07:26,830
 That's because that's what we're told, that's what we're

169
00:07:26,830 --> 00:07:27,360
 taught.

170
00:07:27,360 --> 00:07:29,040
 And it's totally false.

171
00:07:29,040 --> 00:07:30,240
 It's totally wrong.

172
00:07:30,240 --> 00:07:33,430
 It's the wrong way to deal with suffering, but this is how

173
00:07:33,430 --> 00:07:33,960
 we do.

174
00:07:33,960 --> 00:07:34,960
 Why is it wrong?

175
00:07:34,960 --> 00:07:38,640
 Okay, so you have pain in the leg, you move your leg, and

176
00:07:38,640 --> 00:07:40,800
 then you have pain in the back,

177
00:07:40,800 --> 00:07:42,880
 and so then you have to stretch your back.

178
00:07:42,880 --> 00:07:45,450
 And then you have pain in your head, and so you have to

179
00:07:45,450 --> 00:07:46,840
 take a pill for the pain.

180
00:07:46,840 --> 00:07:49,200
 And you have pain here, pain there, and you have to take

181
00:07:49,200 --> 00:07:50,560
 another pill, or you have to

182
00:07:50,560 --> 00:07:53,320
 get an operation, and so on and so on.

183
00:07:53,320 --> 00:07:56,230
 And all this time, you're developing more and more aversion

184
00:07:56,230 --> 00:07:57,680
 to the pain until eventually

185
00:07:57,680 --> 00:08:00,200
 it becomes totally overwhelming.

186
00:08:00,200 --> 00:08:03,460
 When a person takes a pill for a headache, really what they

187
00:08:03,460 --> 00:08:05,320
're doing is reaffirming their

188
00:08:05,320 --> 00:08:07,080
 aversion towards the pain.

189
00:08:07,080 --> 00:08:09,910
 And as a result, it's going to be worse next time, it's

190
00:08:09,910 --> 00:08:11,720
 going to, yeah, it's going to be

191
00:08:11,720 --> 00:08:13,720
 worse and worse and worse.

192
00:08:13,720 --> 00:08:16,370
 Not because the pain changes, but because our attitude

193
00:08:16,370 --> 00:08:18,000
 towards it is reaffirmed as being

194
00:08:18,000 --> 00:08:21,310
 this is bad, this is bad, this is bad, bad, bad, until it

195
00:08:21,310 --> 00:08:23,200
 becomes totally unbearable.

196
00:08:23,200 --> 00:08:25,360
 These things are not static.

197
00:08:25,360 --> 00:08:26,600
 Craving is not static.

198
00:08:26,600 --> 00:08:27,840
 It builds.

199
00:08:27,840 --> 00:08:29,720
 It turns into a habit.

200
00:08:29,720 --> 00:08:34,730
 It changes our whole vibration in that direction, and it

201
00:08:34,730 --> 00:08:38,160
 changes the world around us as well.

202
00:08:38,160 --> 00:08:41,280
 Anger does as well, aversion does as well.

203
00:08:41,280 --> 00:08:46,390
 We become totally averse to these things and unable to bear

204
00:08:46,390 --> 00:08:46,880
 them.

205
00:08:46,880 --> 00:08:51,620
 So what would we come to see, and the reason why we come to

206
00:08:51,620 --> 00:08:54,040
 practice meditation is the

207
00:08:54,040 --> 00:08:56,770
 second type of suffering, the Buddha said, and the dukkha

208
00:08:56,770 --> 00:08:57,400
 sabhala.

209
00:08:57,400 --> 00:09:01,710
 Sabhala means reality of suffering, that suffering is a

210
00:09:01,710 --> 00:09:02,240
 fact.

211
00:09:02,240 --> 00:09:04,120
 You can't escape it.

212
00:09:04,120 --> 00:09:06,120
 It's reality.

213
00:09:06,120 --> 00:09:08,990
 It's a part of reality, and this is like getting old is a

214
00:09:08,990 --> 00:09:10,880
 part of reality, getting sick is

215
00:09:10,880 --> 00:09:12,920
 a part of reality, dying is a part of reality.

216
00:09:12,920 --> 00:09:21,130
 So these methods of overcoming suffering or these ways of

217
00:09:21,130 --> 00:09:25,480
 avoiding the unpleasant part

218
00:09:25,480 --> 00:09:30,880
 of reality are temporary, are ineffective, because it's

219
00:09:30,880 --> 00:09:33,600
 there, it's a part of life, it's

220
00:09:33,600 --> 00:09:37,210
 not going to go away, you're not going to remove it, you're

221
00:09:37,210 --> 00:09:38,560
 not going to get rid of

222
00:09:38,560 --> 00:09:40,960
 sickness, you're not going to get rid of old age, you're

223
00:09:40,960 --> 00:09:42,280
 not going to get rid of death.

224
00:09:42,280 --> 00:09:44,880
 These things are going to come to you.

225
00:09:44,880 --> 00:09:48,130
 And it's this realization, when we have a situation that we

226
00:09:48,130 --> 00:09:49,640
 can't overcome, people who

227
00:09:49,640 --> 00:09:52,920
 have migraine headaches, they've taken every pill and

228
00:09:52,920 --> 00:09:55,160
 nothing works, people who are mourning

229
00:09:55,160 --> 00:09:57,000
 a person who passed away.

230
00:09:57,000 --> 00:10:00,220
 They can drink or whatever, but nothing works, and they're

231
00:10:00,220 --> 00:10:02,080
 still sad, they're still thinking

232
00:10:02,080 --> 00:10:04,480
 about the person, nothing works.

233
00:10:04,480 --> 00:10:07,300
 When a person gets to this point, this is most often when

234
00:10:07,300 --> 00:10:09,320
 they begin to practice meditation.

235
00:10:09,320 --> 00:10:11,590
 When they get to the point where they realize they're

236
00:10:11,590 --> 00:10:13,020
 totally on the wrong path, when they

237
00:10:13,020 --> 00:10:18,250
 realize that this reaffirmation of greed, anger, delusion

238
00:10:18,250 --> 00:10:20,720
 is totally dragging them down

239
00:10:20,720 --> 00:10:23,870
 on the wrong path, and is not helping their reactions

240
00:10:23,870 --> 00:10:25,960
 towards suffering, their ways of

241
00:10:25,960 --> 00:10:29,000
 dealing with suffering are ineffective.

242
00:10:29,000 --> 00:10:32,000
 This is the second type of suffering, it's a very important

243
00:10:32,000 --> 00:10:33,520
 realization, because that's

244
00:10:33,520 --> 00:10:36,070
 what leads us, that's what leads to the conclusion that we

245
00:10:36,070 --> 00:10:37,240
 have to do something.

246
00:10:37,240 --> 00:10:38,360
 People say, "Why do you meditate?

247
00:10:38,360 --> 00:10:40,480
 What's the point wasting your time?"

248
00:10:40,480 --> 00:10:43,500
 Well, those people have not yet realized, this is what they

249
00:10:43,500 --> 00:10:44,800
 haven't yet realized.

250
00:10:44,800 --> 00:10:47,590
 For a person who has realized that, "You can't really

251
00:10:47,590 --> 00:10:49,320
 escape it, you can say I go out and

252
00:10:49,320 --> 00:10:53,700
 party and I'm happy, I've tried it, and I'm not able to

253
00:10:53,700 --> 00:10:56,160
 escape suffering in the way you

254
00:10:56,160 --> 00:10:58,640
 pretend or you think you're able to."

255
00:10:58,640 --> 00:11:01,880
 My experience is that suffering is a part of life, it's

256
00:11:01,880 --> 00:11:03,620
 something that we either learn

257
00:11:03,620 --> 00:11:08,510
 to deal with, or we suffer more and more and more, it gets

258
00:11:08,510 --> 00:11:11,000
 worse and worse and worse, so

259
00:11:11,000 --> 00:11:14,060
 we find a way, so this is when we come to practice

260
00:11:14,060 --> 00:11:15,120
 meditation.

261
00:11:15,120 --> 00:11:17,890
 When we come to practice meditation, we realize the third

262
00:11:17,890 --> 00:11:20,280
 truth, the third type of suffering,

263
00:11:20,280 --> 00:11:25,560
 the third way of understanding this word, suffering.

264
00:11:25,560 --> 00:11:30,480
 This is that suffering is inherent in all things, just as

265
00:11:30,480 --> 00:11:32,960
 heat is inherent in fire.

266
00:11:32,960 --> 00:11:37,890
 There's no fire that is not hot, fire is hot, that's, well,

267
00:11:37,890 --> 00:11:40,160
 in a conventional sense, fire

268
00:11:40,160 --> 00:11:42,000
 is hot.

269
00:11:42,000 --> 00:11:46,230
 By the same token, all things in the world, anything that

270
00:11:46,230 --> 00:11:48,040
 arises is suffering.

271
00:11:48,040 --> 00:11:52,720
 What we mean by this is just as fire is hot, if you hold on

272
00:11:52,720 --> 00:11:54,800
 to it, it burns you.

273
00:11:54,800 --> 00:11:58,300
 When something arises and you cling to it, it makes you

274
00:11:58,300 --> 00:11:59,040
 suffer.

275
00:11:59,040 --> 00:12:01,200
 Why this happens is basically what I've been saying.

276
00:12:01,200 --> 00:12:03,980
 When it's something good, you cling to it as good, and then

277
00:12:03,980 --> 00:12:04,920
 it disappears.

278
00:12:04,920 --> 00:12:08,170
 When it disappears, you're unhappy, you're looking for it,

279
00:12:08,170 --> 00:12:09,560
 because there's still the

280
00:12:09,560 --> 00:12:12,140
 hunting, there's still the clinging in your mind, and in

281
00:12:12,140 --> 00:12:13,800
 fact you cultivate it and develop

282
00:12:13,800 --> 00:12:16,580
 it and it comes up again and again, I want this, I want

283
00:12:16,580 --> 00:12:18,520
 this, I like this, I like this.

284
00:12:18,520 --> 00:12:23,000
 When it's gone, the craving doesn't go away, the experience

285
00:12:23,000 --> 00:12:24,880
 is gone, and who knows when

286
00:12:24,880 --> 00:12:26,120
 it'll come back.

287
00:12:26,120 --> 00:12:29,230
 If it's an unpleasant experience, the craving, aversion, go

288
00:12:29,230 --> 00:12:30,840
 away, go away, and pushing it

289
00:12:30,840 --> 00:12:34,500
 away with this method or that method makes it worse, and so

290
00:12:34,500 --> 00:12:34,920
 on.

291
00:12:34,920 --> 00:12:38,380
 When we practice meditation, we come to realize that no,

292
00:12:38,380 --> 00:12:40,400
 these things are not really going

293
00:12:40,400 --> 00:12:41,400
 to make me happy.

294
00:12:41,400 --> 00:12:43,240
 Clinging to good things is not going to make me happy.

295
00:12:43,240 --> 00:12:44,240
 Why?

296
00:12:44,240 --> 00:12:45,840
 Because they come and they go.

297
00:12:45,840 --> 00:12:47,480
 They arise and they cease.

298
00:12:47,480 --> 00:12:50,280
 My happiness depends on that, how can I ever hope to be

299
00:12:50,280 --> 00:12:50,880
 happy?

300
00:12:50,880 --> 00:12:54,210
 If say my happiness depends on this person, you come to

301
00:12:54,210 --> 00:12:56,240
 realize that that person is only

302
00:12:56,240 --> 00:12:57,240
 experiences.

303
00:12:57,240 --> 00:12:58,240
 You hear something and that's that person.

304
00:12:58,240 --> 00:13:00,360
 You see something and that's that person.

305
00:13:00,360 --> 00:13:02,480
 You think of something and that's that person.

306
00:13:02,480 --> 00:13:06,490
 But all of those experiences are impermanent and they're

307
00:13:06,490 --> 00:13:07,560
 uncertain.

308
00:13:07,560 --> 00:13:09,380
 You don't know whether they're going to say something good

309
00:13:09,380 --> 00:13:09,760
 to you.

310
00:13:09,760 --> 00:13:12,260
 Maybe someone you love says something bad to you and you

311
00:13:12,260 --> 00:13:13,160
 suffer from it.

312
00:13:13,160 --> 00:13:15,800
 And the realization that it's not sure.

313
00:13:15,800 --> 00:13:18,120
 There's no person there that's wonderful.

314
00:13:18,120 --> 00:13:20,580
 There's a bunch of experiences waiting to happen and those

315
00:13:20,580 --> 00:13:21,720
 experiences will not all

316
00:13:21,720 --> 00:13:23,120
 be pleasant.

317
00:13:23,120 --> 00:13:26,060
 When the person dies, if they're a loved one, then that

318
00:13:26,060 --> 00:13:28,040
 will be a very unpleasant thing.

319
00:13:28,040 --> 00:13:31,160
 This realization we come to through our meditation practice

320
00:13:31,160 --> 00:13:33,160
, even just watching our own body,

321
00:13:33,160 --> 00:13:36,450
 watching the rising and falling on the stomach, coming to

322
00:13:36,450 --> 00:13:38,760
 see that every single thing that

323
00:13:38,760 --> 00:13:40,800
 arises has to pass away.

324
00:13:40,800 --> 00:13:43,080
 It comes and it goes.

325
00:13:43,080 --> 00:13:45,320
 Coming to see that we do it even with our stomach.

326
00:13:45,320 --> 00:13:48,300
 We're rising and falling, oh now it's smooth and then we

327
00:13:48,300 --> 00:13:48,960
 like it.

328
00:13:48,960 --> 00:13:51,150
 And then suddenly it changes and it's rough and

329
00:13:51,150 --> 00:13:53,080
 uncomfortable and we can't really find

330
00:13:53,080 --> 00:13:55,720
 it and we're angry and upset.

331
00:13:55,720 --> 00:13:58,550
 Even this very stupid, simple object of the stomach can

332
00:13:58,550 --> 00:13:59,880
 teach us everything we need to

333
00:13:59,880 --> 00:14:03,720
 know about reality because it shows us our mind.

334
00:14:03,720 --> 00:14:07,260
 It shows us the way we project and the way we relate to

335
00:14:07,260 --> 00:14:09,680
 things and make more out of things

336
00:14:09,680 --> 00:14:12,800
 than they actually are.

337
00:14:12,800 --> 00:14:15,960
 Through the meditation practice we'll come to break down

338
00:14:15,960 --> 00:14:17,760
 people, break down things and

339
00:14:17,760 --> 00:14:24,500
 experiences into individual phenomenon that arise and come

340
00:14:24,500 --> 00:14:25,600
 and go.

341
00:14:25,600 --> 00:14:30,170
 As a result we see that none of them are pleasant or none

342
00:14:30,170 --> 00:14:32,480
 of them are satisfying.

343
00:14:32,480 --> 00:14:34,840
 And this is what it means by suffering, that our happiness

344
00:14:34,840 --> 00:14:36,120
 should never depend on these

345
00:14:36,120 --> 00:14:37,120
 things.

346
00:14:37,120 --> 00:14:40,060
 It depends on a person, that person doesn't exist, it's

347
00:14:40,060 --> 00:14:41,720
 just experiences coming to see

348
00:14:41,720 --> 00:14:45,330
 that our relationship with the person is just through

349
00:14:45,330 --> 00:14:47,680
 experiences which are never going

350
00:14:47,680 --> 00:14:48,680
 to satisfy us.

351
00:14:48,680 --> 00:14:52,260
 The only thing they can possibly do is bring more clinging

352
00:14:52,260 --> 00:14:54,440
 and craving and dissatisfaction

353
00:14:54,440 --> 00:14:57,360
 and the need for more and so on.

354
00:14:57,360 --> 00:15:00,240
 And we will never truly be at peace with ourselves.

355
00:15:00,240 --> 00:15:02,530
 People think that they will but you can look at those

356
00:15:02,530 --> 00:15:04,680
 people and see that they're not really

357
00:15:04,680 --> 00:15:07,600
 truly at peace with themselves.

358
00:15:07,600 --> 00:15:09,730
 And through the meditation we come to see this and so we

359
00:15:09,730 --> 00:15:10,920
 come to gain true peace for

360
00:15:10,920 --> 00:15:11,920
 ourselves.

361
00:15:11,920 --> 00:15:15,210
 As you see these things, you'll see things arising even

362
00:15:15,210 --> 00:15:17,200
 just the stomach coming and going

363
00:15:17,200 --> 00:15:19,890
 and you'll see the clinging and how useless it is to cling

364
00:15:19,890 --> 00:15:21,240
 to it to be this way or that

365
00:15:21,240 --> 00:15:22,520
 way because it's changing.

366
00:15:22,520 --> 00:15:25,090
 You see it coming and going and that it's not under your

367
00:15:25,090 --> 00:15:25,760
 control.

368
00:15:25,760 --> 00:15:28,650
 You see impermanent suffering, non-self, these three

369
00:15:28,650 --> 00:15:29,800
 characteristics.

370
00:15:29,800 --> 00:15:33,360
 As this goes on, the fourth type of suffering or the fourth

371
00:15:33,360 --> 00:15:35,480
 way of understanding suffering

372
00:15:35,480 --> 00:15:37,920
 comes to you and that is as the truth.

373
00:15:37,920 --> 00:15:40,900
 And this is what we call, you hear about the noble truth,

374
00:15:40,900 --> 00:15:42,680
 the noble truth of suffering.

375
00:15:42,680 --> 00:15:46,390
 And this is the realization that we're hoping for, this

376
00:15:46,390 --> 00:15:48,920
 realization that it's not a good,

377
00:15:48,920 --> 00:15:50,720
 it's not satisfying.

378
00:15:50,720 --> 00:15:53,480
 These are not going to make me happy.

379
00:15:53,480 --> 00:15:57,440
 This realization is the fourth type of way of understanding

380
00:15:57,440 --> 00:15:59,240
 suffering, is really the

381
00:15:59,240 --> 00:16:00,920
 consummation of the Buddha's teaching.

382
00:16:00,920 --> 00:16:03,300
 It's just starting to see things, seeing things coming and

383
00:16:03,300 --> 00:16:04,880
 going, oh this isn't satisfying.

384
00:16:04,880 --> 00:16:07,930
 That is, you just realize at some point that this isn't the

385
00:16:07,930 --> 00:16:09,360
 way to find happiness.

386
00:16:09,360 --> 00:16:11,860
 It's not intellectual of course, I'm just putting words to

387
00:16:11,860 --> 00:16:14,000
 it but it's suddenly a boom.

388
00:16:14,000 --> 00:16:18,100
 The mind just says, no and the mind gives up, the mind lets

389
00:16:18,100 --> 00:16:18,640
 go.

390
00:16:18,640 --> 00:16:23,090
 And at that point there's freedom, the mind is released and

391
00:16:23,090 --> 00:16:24,760
 you enter into this state

392
00:16:24,760 --> 00:16:29,440
 of total freedom which really there's no experience at all.

393
00:16:29,440 --> 00:16:31,470
 There's no seeing, hearing, smelling, tasting, feeling or

394
00:16:31,470 --> 00:16:32,160
 even thinking.

395
00:16:32,160 --> 00:16:33,920
 You're not even aware.

396
00:16:33,920 --> 00:16:37,600
 It's kind of like falling asleep but it's total peace.

397
00:16:37,600 --> 00:16:41,560
 And when you come back you realize, wow, I just totally let

398
00:16:41,560 --> 00:16:42,000
 go.

399
00:16:42,000 --> 00:16:48,400
 There was no arising of anything at that point.

400
00:16:48,400 --> 00:16:50,920
 And this is what we call nirvana, this realization.

401
00:16:50,920 --> 00:16:53,940
 And a person starts to realize this and looking around and

402
00:16:53,940 --> 00:16:55,720
 seeing that their happiness can't

403
00:16:55,720 --> 00:16:58,680
 come from anything outside of themselves.

404
00:16:58,680 --> 00:17:01,290
 And so they cultivate this more and more and more and start

405
00:17:01,290 --> 00:17:02,680
 to see the truth more and more

406
00:17:02,680 --> 00:17:06,810
 and more and eventually are able to become totally free

407
00:17:06,810 --> 00:17:08,280
 from suffering.

408
00:17:08,280 --> 00:17:11,910
 So the point being that it's this realization, when you

409
00:17:11,910 --> 00:17:14,000
 talk about nirvana and so on, and

410
00:17:14,000 --> 00:17:16,080
 just see for yourself.

411
00:17:16,080 --> 00:17:18,760
 The Buddhist teaching is to see for yourself.

412
00:17:18,760 --> 00:17:21,420
 But the point which I think everyone can understand is that

413
00:17:21,420 --> 00:17:22,840
 we have to see this reality.

414
00:17:22,840 --> 00:17:25,820
 We have to really grasp this, that we're not going to find

415
00:17:25,820 --> 00:17:27,600
 happiness in the things outside

416
00:17:27,600 --> 00:17:29,360
 of ourselves.

417
00:17:29,360 --> 00:17:35,830
 And that these phenomena that arise, there's no good that

418
00:17:35,830 --> 00:17:39,200
 can come from clinging even in

419
00:17:39,200 --> 00:17:40,800
 terms of aversion.

420
00:17:40,800 --> 00:17:44,200
 There's no good that comes from wanting to escape it or

421
00:17:44,200 --> 00:17:46,280
 wanting to even force yourself

422
00:17:46,280 --> 00:17:48,620
 to sit still in this case.

423
00:17:48,620 --> 00:17:51,060
 So it's a very simple question with a very simple answer,

424
00:17:51,060 --> 00:17:52,920
 but here are the profound implications

425
00:17:52,920 --> 00:17:55,700
 to do with suffering that help us to understand why it is

426
00:17:55,700 --> 00:17:57,440
 that we might want to sit through

427
00:17:57,440 --> 00:17:58,440
 it sometimes.

428
00:17:58,440 --> 00:18:01,860
 And if we can get that through our head then sometimes we

429
00:18:01,860 --> 00:18:03,280
 can sit through it.

430
00:18:03,280 --> 00:18:05,690
 We say, "No, yeah, I want to move, but it's not really a

431
00:18:05,690 --> 00:18:06,320
 big deal.

432
00:18:06,320 --> 00:18:09,200
 It's not going to kill me and I'm going to learn something

433
00:18:09,200 --> 00:18:09,760
 here."

434
00:18:09,760 --> 00:18:12,900
 And as you do that you'll see how your mind works, you'll

435
00:18:12,900 --> 00:18:15,040
 see the craving and the aversion.

436
00:18:15,040 --> 00:18:17,880
 And you'll see how the phenomenon works and how it arises

437
00:18:17,880 --> 00:18:19,440
 and ceases and see that there's

438
00:18:19,440 --> 00:18:22,110
 no good that comes from being angry about it or upset about

439
00:18:22,110 --> 00:18:22,480
 it.

440
00:18:22,480 --> 00:18:25,510
 You don't feel happier, it doesn't solve the problem, it

441
00:18:25,510 --> 00:18:26,880
 creates more aversion.

442
00:18:26,880 --> 00:18:29,210
 Moving your foot doesn't solve the problem, it's a

443
00:18:29,210 --> 00:18:30,880
 temporary solution to kind of ease

444
00:18:30,880 --> 00:18:33,400
 the pressure when it's too much for you.

445
00:18:33,400 --> 00:18:36,650
 But eventually you're just going to say, "Why would I move

446
00:18:36,650 --> 00:18:37,680
 my foot again?

447
00:18:37,680 --> 00:18:39,360
 I've moved my foot ten times already.

448
00:18:39,360 --> 00:18:40,360
 It didn't solve the problem."

449
00:18:40,360 --> 00:18:43,080
 And eventually you give that up and you say, "Well, I'll

450
00:18:43,080 --> 00:18:44,320
 just sit through it."

451
00:18:44,320 --> 00:18:45,970
 That's what happens in meditation when you go to an

452
00:18:45,970 --> 00:18:46,720
 intensive course.

453
00:18:46,720 --> 00:18:49,060
 In the beginning in pain here you say, "Okay, I can fix

454
00:18:49,060 --> 00:18:49,440
 that."

455
00:18:49,440 --> 00:18:51,280
 And you put a pillow under here and then this one and then

456
00:18:51,280 --> 00:18:52,360
 you put a pillow under here and

457
00:18:52,360 --> 00:18:55,400
 then here you put a pillow and here you put a pillow.

458
00:18:55,400 --> 00:18:58,400
 Until eventually you just say, "Oh, it's not working.

459
00:18:58,400 --> 00:19:00,080
 It's not solving the problem."

460
00:19:00,080 --> 00:19:03,170
 And you give up and that's the realization that we're

461
00:19:03,170 --> 00:19:04,240
 striving for.

462
00:19:04,240 --> 00:19:06,680
 You realize that more and more and more.

463
00:19:06,680 --> 00:19:09,630
 You'll realize that that's the truth, that's the truth of

464
00:19:09,630 --> 00:19:11,400
 reality and you'll let go and

465
00:19:11,400 --> 00:19:12,920
 not cling.

466
00:19:12,920 --> 00:19:17,270
 So this is my discussion of the truth of suffering in the

467
00:19:17,270 --> 00:19:19,120
 Buddha's teaching.

468
00:19:19,120 --> 00:19:21,800
 Thanks for tuning in and I wish you all the best that you

469
00:19:21,800 --> 00:19:23,160
're all able to put this into

470
00:19:23,160 --> 00:19:27,130
 practice and try your best to become patient and be able to

471
00:19:27,130 --> 00:19:29,400
 overcome your attachments and

472
00:19:29,400 --> 00:19:33,180
 your immersions and suffering and find true peace,

473
00:19:33,180 --> 00:19:36,280
 happiness and freedom from suffering.

