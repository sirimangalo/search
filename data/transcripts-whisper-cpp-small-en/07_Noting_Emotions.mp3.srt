1
00:00:00,000 --> 00:00:02,080
 Notting emotions.

2
00:00:02,080 --> 00:00:06,360
 In the case of emotions, is it okay to just note feeling

3
00:00:06,360 --> 00:00:09,320
 or is it better to label the specific emotion?

4
00:00:09,320 --> 00:00:12,840
 Emotions are complex entities.

5
00:00:12,840 --> 00:00:16,240
 They generally contain fleeting mental component

6
00:00:16,240 --> 00:00:18,560
 and a more lasting physical effect.

7
00:00:18,560 --> 00:00:22,400
 It is important to separate these two aspects

8
00:00:22,400 --> 00:00:26,320
 in order to clearly see the reality of the experience.

9
00:00:26,320 --> 00:00:31,200
 For example, anxiety exists only momentarily in the mind

10
00:00:31,200 --> 00:00:33,640
 but sometimes appears to persist

11
00:00:33,640 --> 00:00:36,880
 due to the physical reaction triggered by it.

12
00:00:36,880 --> 00:00:40,960
 The actual emotion like anxiety, disliking, liking,

13
00:00:40,960 --> 00:00:44,720
 fear, worry, et cetera, should be noted by name

14
00:00:44,720 --> 00:00:47,720
 for the brief moment that it arises.

15
00:00:47,720 --> 00:00:51,000
 The physical aspect of each should be distinguished

16
00:00:51,000 --> 00:00:55,280
 as being simply feeling or in certain cases, pain,

17
00:00:55,280 --> 00:00:56,800
 seizure or calm.

18
00:00:56,800 --> 00:00:59,560
 (static buzzing)

