1
00:00:00,000 --> 00:00:03,670
 Sir, I have failed to understand why people are born with

2
00:00:03,670 --> 00:00:05,000
 physical disability.

3
00:00:05,000 --> 00:00:09,000
 Are they born to suffer throughout their life?

4
00:00:09,000 --> 00:00:12,000
 But why?

5
00:00:12,000 --> 00:00:14,000
 Why?

6
00:00:14,000 --> 00:00:18,000
 Why is an interesting question as well.

7
00:00:18,000 --> 00:00:21,500
 One answer you can make is that it really doesn't matter

8
00:00:21,500 --> 00:00:22,000
 why.

9
00:00:22,000 --> 00:00:25,720
 I mean, it is the case that they're born with disabilities,

10
00:00:25,720 --> 00:00:26,000
 no?

11
00:00:26,000 --> 00:00:31,070
 The whole thing of karma is interesting and it is important

12
00:00:31,070 --> 00:00:34,000
.

13
00:00:34,000 --> 00:00:38,080
 But you have to understand, and this is something

14
00:00:38,080 --> 00:00:39,000
 interesting here,

15
00:00:39,000 --> 00:00:42,000
 because we always talk about how the Buddha taught karma.

16
00:00:42,000 --> 00:00:45,710
 And then there are the Buddhists who claim that the Buddha

17
00:00:45,710 --> 00:00:47,000
 didn't teach karma,

18
00:00:47,000 --> 00:00:50,780
 but it was just because that was what the people in the

19
00:00:50,780 --> 00:00:53,000
 society around him taught.

20
00:00:53,000 --> 00:00:56,620
 I think that's a silly idea. Of course the Buddha believed

21
00:00:56,620 --> 00:00:58,000
 in karma because of course it happens.

22
00:00:58,000 --> 00:01:00,970
 Because of course from life to life these things carry over

23
00:01:00,970 --> 00:01:01,000
.

24
00:01:01,000 --> 00:01:03,460
 We don't really know how it works. We have the Buddha's

25
00:01:03,460 --> 00:01:04,000
 words on how it works.

26
00:01:04,000 --> 00:01:07,140
 And we have an idea, and the more we practice meditation,

27
00:01:07,140 --> 00:01:09,000
 the clearer that idea becomes.

28
00:01:09,000 --> 00:01:11,920
 Like, "Ooh, if I keep these mind states, there's going to

29
00:01:11,920 --> 00:01:14,000
 be a lot of suffering following it.

30
00:01:14,000 --> 00:01:17,690
 And if I cultivate these mind states and if I cultivate

31
00:01:17,690 --> 00:01:19,000
 these tendencies,

32
00:01:19,000 --> 00:01:22,000
 I'm going to suffer horribly because of these."

33
00:01:22,000 --> 00:01:26,000
 But we don't really know, "This person is born this way

34
00:01:26,000 --> 00:01:26,000
 because of this,

35
00:01:26,000 --> 00:01:28,610
 and this person is born that way. This is what they did in

36
00:01:28,610 --> 00:01:29,000
 the past."

37
00:01:29,000 --> 00:01:32,000
 You have to know exactly how experience works.

38
00:01:32,000 --> 00:01:35,150
 You have to be able to see what's going on in their mind at

39
00:01:35,150 --> 00:01:36,000
 every moment

40
00:01:36,000 --> 00:01:40,000
 and see where that all came from.

41
00:01:40,000 --> 00:01:44,000
 You have to see where their mind was when they died

42
00:01:44,000 --> 00:01:46,350
 and what they had done in their past. It was a very

43
00:01:46,350 --> 00:01:47,000
 difficult thing.

44
00:01:47,000 --> 00:01:53,000
 The Buddha said, "No one can do it except a Buddha."

45
00:01:53,000 --> 00:01:58,000
 But whether the Buddha taught this idea of past life,

46
00:01:58,000 --> 00:02:00,000
 future life, or so on,

47
00:02:00,000 --> 00:02:03,470
 what the Buddha was trying to impress upon people is the

48
00:02:03,470 --> 00:02:05,000
 present moment.

49
00:02:05,000 --> 00:02:08,000
 "Aditya nannuakkamaya, don't go back to the past.

50
00:02:08,000 --> 00:02:11,000
 Nappati gankey anagatang, don't worry about the future."

51
00:02:11,000 --> 00:02:14,000
 Whatever comes up in the present moment, tathatatvi passati

52
00:02:14,000 --> 00:02:14,000
.

53
00:02:14,000 --> 00:02:17,260
 What's in the past is gone. What's in the future is not

54
00:02:17,260 --> 00:02:18,000
 come yet.

55
00:02:18,000 --> 00:02:21,000
 See whatever arises clearly.

56
00:02:21,000 --> 00:02:23,000
 Weep passati is the same word as weep passana.

57
00:02:23,000 --> 00:02:27,100
 It means insight. See with insight that which arises in the

58
00:02:27,100 --> 00:02:28,000
 present moment.

59
00:02:28,000 --> 00:02:40,000
 One way to answer it is to say, "It is what it is."

60
00:02:40,000 --> 00:02:43,240
 The question of why they are born that way is not so

61
00:02:43,240 --> 00:02:44,000
 important.

62
00:02:44,000 --> 00:02:48,450
 The question, the idea of it having some meaning or some

63
00:02:48,450 --> 00:02:52,000
 purpose or it having a why.

64
00:02:52,000 --> 00:02:54,180
 The why is not so important. The question is what you are

65
00:02:54,180 --> 00:02:55,000
 going to do with it.

66
00:02:55,000 --> 00:02:59,000
 What you are going to get out of that life.

67
00:02:59,000 --> 00:03:05,140
 Because in Buddhism we have no idea of, in fact, an

68
00:03:05,140 --> 00:03:08,000
 important thing to mention is this idea of purpose.

69
00:03:08,000 --> 00:03:12,430
 We hear that everything happens for a purpose and there

70
00:03:12,430 --> 00:03:17,000
 must be some meaning that there is a lesson to be learned.

71
00:03:17,000 --> 00:03:21,540
 You learn from these things or these things are given to

72
00:03:21,540 --> 00:03:23,000
 you by a deity.

73
00:03:23,000 --> 00:03:26,370
 Of course in deistic religions or theistic religions there

74
00:03:26,370 --> 00:03:29,990
 is the belief that these conditions were imposed upon us

75
00:03:29,990 --> 00:03:33,000
 for some ulterior purpose.

76
00:03:33,000 --> 00:03:38,640
 We don't have that in Buddhism. You are in charge of your

77
00:03:38,640 --> 00:03:39,000
 destiny.

78
00:03:39,000 --> 00:03:42,750
 You lead yourself to be born as a cripple. However you got

79
00:03:42,750 --> 00:03:44,000
 there, you got there.

80
00:03:44,000 --> 00:03:48,090
 That is where you are right now. A person who is born with

81
00:03:48,090 --> 00:03:51,000
 physical disabilities has no one to blame but themselves.

82
00:03:51,000 --> 00:03:55,220
 I think people have a hard time hearing that and they think

83
00:03:55,220 --> 00:03:59,000
 that they are blaming the victim or so on like that.

84
00:03:59,000 --> 00:04:03,340
 If you think of things scientifically, the universe is what

85
00:04:03,340 --> 00:04:04,000
 it is.

86
00:04:04,000 --> 00:04:08,350
 We have experience. Experience does exist and our whole of

87
00:04:08,350 --> 00:04:11,000
 reality is based on this experience.

88
00:04:11,000 --> 00:04:15,000
 What could our future come from except for our experience?

89
00:04:15,000 --> 00:04:18,550
 Whether it is true that we are blaming the victims and

90
00:04:18,550 --> 00:04:22,000
 victims of crimes and victims of great suffering.

91
00:04:22,000 --> 00:04:25,590
 We blame them saying that it is just their karma. It is not

92
00:04:25,590 --> 00:04:28,450
 because it is also the karma of the person inflicting the

93
00:04:28,450 --> 00:04:29,000
 crimes.

94
00:04:29,000 --> 00:04:33,430
 We get ourselves into these situations. If we didn't act in

95
00:04:33,430 --> 00:04:37,220
 a certain way, we wouldn't find ourselves in these

96
00:04:37,220 --> 00:04:38,000
 situations.

97
00:04:38,000 --> 00:04:43,280
 This isn't fatalistic because the most important thing is

98
00:04:43,280 --> 00:04:47,350
 not how we got here or where we find ourselves or how we

99
00:04:47,350 --> 00:04:48,000
 find ourselves.

100
00:04:48,000 --> 00:04:52,000
 It is what we do with it and where we go from here.

101
00:04:52,000 --> 00:04:55,260
 If you are a crippled person, maybe walking meditation is

102
00:04:55,260 --> 00:04:58,300
 impossible but certainly the development of goodness is

103
00:04:58,300 --> 00:04:59,000
 possible.

104
00:04:59,000 --> 00:05:07,370
 There is the story of Matakundali, this boy who couldn't

105
00:05:07,370 --> 00:05:11,000
 even raise his hands even to pay respect to the Buddha.

106
00:05:11,000 --> 00:05:13,440
 Even to do that much. You don't talk about practicing

107
00:05:13,440 --> 00:05:16,000
 meditation but he had never seen the Buddha before.

108
00:05:16,000 --> 00:05:20,080
 When he saw the Buddha, he had so much rapture in him that

109
00:05:20,080 --> 00:05:23,900
 he just wanted to bow down to the Buddha and pay respect to

110
00:05:23,900 --> 00:05:25,000
 him and learn from him.

111
00:05:25,000 --> 00:05:29,320
 He thought this in his mind, and he damaged the Buddha and

112
00:05:29,320 --> 00:05:32,000
 died. He was very sick.

113
00:05:32,000 --> 00:05:37,200
 As a result of that, he was born in heaven. He was born as

114
00:05:37,200 --> 00:05:38,000
 an angel.

115
00:05:38,000 --> 00:05:40,560
 He was able to come back and listen to the Buddha's

116
00:05:40,560 --> 00:05:43,000
 teaching and practice and become a light.

117
00:05:43,000 --> 00:05:47,580
 Whether you believe these kinds of stories or not, I think

118
00:05:47,580 --> 00:05:50,000
 I'm just crazy to tell them.

119
00:05:50,000 --> 00:05:56,270
 It expresses the point that is verifiable, that goodness

120
00:05:56,270 --> 00:06:00,110
 comes from the mind and can be performed at any moment, in

121
00:06:00,110 --> 00:06:01,000
 any circumstance.

122
00:06:01,000 --> 00:06:10,000
 That's really what we try to do in this life.

123
00:06:10,000 --> 00:06:14,790
 We don't worry about our situation. It's totally empowering

124
00:06:14,790 --> 00:06:16,000
 for everyone.

125
00:06:16,000 --> 00:06:21,780
 For a disabled person, for a person who is in a state of

126
00:06:21,780 --> 00:06:31,000
 suffering, even a person who is in some form of slavery or

127
00:06:31,000 --> 00:06:36,000
 hard labor or who has great debt.

128
00:06:36,000 --> 00:06:40,570
 In the end, they can live their life out to its end and

129
00:06:40,570 --> 00:06:45,220
 become enlightened in a way that leads them closer to

130
00:06:45,220 --> 00:06:47,000
 enlightenment.

131
00:06:47,000 --> 00:06:53,220
 I would like to say that in Europe, people with physical

132
00:06:53,220 --> 00:06:59,360
 disabilities or mental disabilities are not called handic

133
00:06:59,360 --> 00:07:01,000
apped or so.

134
00:07:01,000 --> 00:07:06,000
 They are called now people with special needs.

135
00:07:06,000 --> 00:07:16,760
 I think that is very beautiful to put it because if you

136
00:07:16,760 --> 00:07:27,000
 look at it without...

137
00:07:27,000 --> 00:07:33,890
 I think the suffering that many of those people have

138
00:07:33,890 --> 00:07:41,650
 becomes because they are judged by others who are different

139
00:07:41,650 --> 00:07:42,000
.

140
00:07:42,000 --> 00:07:50,770
 We, with two arms and two legs, moving them as we want,

141
00:07:50,770 --> 00:07:56,360
 judge them and say, "Oh, they don't have that. They are

142
00:07:56,360 --> 00:07:59,000
 different. They are disabled."

143
00:07:59,000 --> 00:08:05,080
 But it's our judgment. They are in and of themselves

144
00:08:05,080 --> 00:08:06,000
 perfect.

145
00:08:06,000 --> 00:08:10,850
 There are people without legs or without arms and they

146
00:08:10,850 --> 00:08:16,000
 still draw pictures and do things.

147
00:08:16,000 --> 00:08:22,250
 They are people with special needs who are very, very happy

148
00:08:22,250 --> 00:08:28,000
, living happy lives, living good lives, not suffering

149
00:08:28,000 --> 00:08:32,000
 because they are not judged by others.

150
00:08:32,000 --> 00:08:36,000
 They are just supported in the way they are.

151
00:08:36,000 --> 00:08:42,110
 If they need to do things that they cannot do because they

152
00:08:42,110 --> 00:08:48,170
 are different, then people help them instead of judging

153
00:08:48,170 --> 00:08:49,000
 them.

154
00:08:49,000 --> 00:09:00,020
 I think we should think more of that, of how we judge them

155
00:09:00,020 --> 00:09:08,300
 and what we do with that to them instead of just being

156
00:09:08,300 --> 00:09:13,000
 natural to them and accept them as they really are

157
00:09:13,000 --> 00:09:21,530
 and help them if help is needed and learn from them that

158
00:09:21,530 --> 00:09:26,830
 life can be very good even if it's not perfect, even if

159
00:09:26,830 --> 00:09:31,000
 body and mind are not perfect if we want it.

160
00:09:31,000 --> 00:09:35,760
 I can ask you a corralary question. What do you think of

161
00:09:35,760 --> 00:09:39,000
 the fact that it's often karmic?

162
00:09:39,000 --> 00:09:43,000
 I want to be very careful here because I don't want to give

163
00:09:43,000 --> 00:09:47,000
 the impression that we should have any kind of judging.

164
00:09:47,000 --> 00:09:50,990
 But what you often see with people who have what you might

165
00:09:50,990 --> 00:09:54,000
 call special needs is not all the time.

166
00:09:54,000 --> 00:09:57,560
 This is why I want to be careful. But you often see that

167
00:09:57,560 --> 00:10:01,560
 they are unable to practice meditation because of some

168
00:10:01,560 --> 00:10:06,000
 intense unwholesomeness that has led them to this state.

169
00:10:06,000 --> 00:10:10,000
 What would you think of that idea?

170
00:10:10,000 --> 00:10:26,000
 That there is something out of whack?

171
00:10:26,000 --> 00:10:35,870
 I think the best part of that would be for a person who has

172
00:10:35,870 --> 00:10:45,940
 these things to yes feel empowered but to not feel overconf

173
00:10:45,940 --> 00:10:51,000
ident and think I'm perfect.

174
00:10:51,000 --> 00:10:55,080
 To give rise to this kind of feeling like wow because of my

175
00:10:55,080 --> 00:10:58,000
 negligence I've come to this situation.

176
00:10:58,000 --> 00:11:00,850
 Which I think would only come to a person who believes in

177
00:11:00,850 --> 00:11:03,590
 karma and past lives and so on but can actually be

178
00:11:03,590 --> 00:11:07,060
 beneficial because it helps you to give rise to this sense

179
00:11:07,060 --> 00:11:11,490
 of urgency and the need to root out whatever it is that has

180
00:11:11,490 --> 00:11:13,000
 brought you to that state.

181
00:11:13,000 --> 00:11:16,960
 So just something to think about not to take too seriously

182
00:11:16,960 --> 00:11:20,750
 because you don't want to look at people and say oh those

183
00:11:20,750 --> 00:11:23,000
 people have bad karma and so on.

184
00:11:23,000 --> 00:11:28,010
 But there is something there. Something that why they weren

185
00:11:28,010 --> 00:11:33,810
't born, why their experience led them to be born in the

186
00:11:33,810 --> 00:11:40,000
 state that is obviously out of whack I think is the term.

187
00:11:40,000 --> 00:11:45,000
 What do you have to say?

188
00:11:45,000 --> 00:11:47,000
 Do you understand what I'm asking?

189
00:11:47,000 --> 00:11:51,000
 I do understand more or less.

190
00:11:51,000 --> 00:12:01,870
 I have one experience a long long ago I was preparing for a

191
00:12:01,870 --> 00:12:11,920
 theater piece where I had to act a disabled girl with I

192
00:12:11,920 --> 00:12:20,000
 think it's down syndrome called so I had to play that.

193
00:12:20,000 --> 00:12:29,180
 And I went to to a daycare center where people with special

194
00:12:29,180 --> 00:12:35,870
 needs spend their days and afterwards in the evening would

195
00:12:35,870 --> 00:12:39,000
 go back to the families.

196
00:12:39,000 --> 00:12:44,840
 And there was there were some people with with the down

197
00:12:44,840 --> 00:12:50,960
 syndrome and for for some month I worked with them but not

198
00:12:50,960 --> 00:12:57,080
 work with them that's wrong I just visited them and spent

199
00:12:57,080 --> 00:13:00,000
 time and we played and so on.

200
00:13:00,000 --> 00:13:09,510
 And they were so beautiful so good so really really much

201
00:13:09,510 --> 00:13:16,000
 better than than they were angels.

202
00:13:16,000 --> 00:13:23,320
 I don't I don't know what to say maybe they they are not

203
00:13:23,320 --> 00:13:30,970
 able to to meditate but they could be still we could have

204
00:13:30,970 --> 00:13:41,000
 very very silent moments together and

205
00:13:41,000 --> 00:13:49,950
 probably there are other forms of mental illness which I

206
00:13:49,950 --> 00:13:56,710
 haven't researched where something of the of the bad past

207
00:13:56,710 --> 00:14:01,000
 karma is left or so I don't know but I don't think that the

208
00:14:01,000 --> 00:14:07,630
 person is really actually taking the the bad into the next

209
00:14:07,630 --> 00:14:08,000
 life.

210
00:14:08,000 --> 00:14:17,000
 It's just maybe.

211
00:14:17,000 --> 00:14:21,310
 I've had meditators who have have disabilities in one way

212
00:14:21,310 --> 00:14:25,240
 or another and I just kind of get the feeling that there is

213
00:14:25,240 --> 00:14:29,550
 something there there can be something something to watch

214
00:14:29,550 --> 00:14:33,370
 out for and I guess the idea would be the only thing I'd

215
00:14:33,370 --> 00:14:37,000
 want to the only time I'd want to bring such a thing up is

216
00:14:37,000 --> 00:14:41,000
 for for all of us know because we all have disabilities.

217
00:14:41,000 --> 00:14:44,440
 We all have things in our life obstacles in our lives and

218
00:14:44,440 --> 00:14:47,810
 this should be you know even if they don't bring it into

219
00:14:47,810 --> 00:14:51,510
 their next life and have negative mind states because of it

220
00:14:51,510 --> 00:14:54,790
 it's still a reason for us to be clear that this sort of

221
00:14:54,790 --> 00:14:57,540
 thing can arise in our experience and if we come to

222
00:14:57,540 --> 00:15:02,000
 understand reality as being based on on what it is you know

223
00:15:02,000 --> 00:15:06,000
 really reality is experience it is it is reality.

224
00:15:06,000 --> 00:15:10,080
 Then we realize that we have to be very careful with this

225
00:15:10,080 --> 00:15:13,930
 like an egg if you if you are not careful it will break to

226
00:15:13,930 --> 00:15:18,150
 be careful with our experience and to be quite quite

227
00:15:18,150 --> 00:15:21,910
 meticulous about the cultivation of certain mind states

228
00:15:21,910 --> 00:15:26,320
 which can have you know if you read in the suit is just the

229
00:15:26,320 --> 00:15:29,250
 smallest thing the other thing that that kind of I would

230
00:15:29,250 --> 00:15:33,440
 say collaborates the idea is when people do suffer they

231
00:15:33,440 --> 00:15:35,000
 often and not always but they often do.

232
00:15:35,000 --> 00:15:39,280
 Become better people because of it because they see the

233
00:15:39,280 --> 00:15:43,910
 potential or because they've dealt with suffering and they

234
00:15:43,910 --> 00:15:48,180
 know how to deal with suffering and they they're they

235
00:15:48,180 --> 00:15:52,360
 become more patient and they become more compassionate

236
00:15:52,360 --> 00:15:56,000
 because they know what it means to suffer.

237
00:15:56,000 --> 00:16:02,070
 My niece for example was as a child had the tendency to to

238
00:16:02,070 --> 00:16:08,340
 be very sick for suddenly it could just appear that she got

239
00:16:08,340 --> 00:16:14,240
 very sick and living in that danger to become to become

240
00:16:14,240 --> 00:16:20,090
 very sick every moment and and get fever so high that it

241
00:16:20,090 --> 00:16:25,000
 really could be life threatening made her a very sick.

242
00:16:25,000 --> 00:16:33,040
 Made her a very very mature person who I'm seeing the lead

243
00:16:33,040 --> 00:16:34,000
 go no.

244
00:16:34,000 --> 00:16:39,070
 So she is 17 now and she is very very special very

245
00:16:39,070 --> 00:16:45,000
 different from other people in it's a very good language.

246
00:16:45,000 --> 00:16:49,000
 So just to reconfirm what you said.

