1
00:00:00,000 --> 00:00:03,300
 How do I deal with constantly seeing cause and effect in

2
00:00:03,300 --> 00:00:05,400
 work when it becomes a distraction?

3
00:00:05,400 --> 00:00:08,680
 We'll focus on the distraction

4
00:00:08,680 --> 00:00:13,540
 When you feel distracted focus on that state of distraction

5
00:00:13,540 --> 00:00:20,020
 This is called Jana it can be this is why we talk about

6
00:00:20,020 --> 00:00:22,820
 what we call the upakillate we pass an upakillasis

7
00:00:22,820 --> 00:00:26,330
 They are the defilements of insight. So there are good

8
00:00:26,330 --> 00:00:26,760
 things

9
00:00:26,760 --> 00:00:29,620
 But they distract you from the practice

10
00:00:29,940 --> 00:00:32,930
 when they distract you should focus on the distraction is

11
00:00:32,930 --> 00:00:34,420
 distracted distracted or

12
00:00:34,420 --> 00:00:37,140
 overwhelmed overwhelmed or

13
00:00:37,140 --> 00:00:40,260
 knowing knowing your thinking thinking or so on and

14
00:00:40,260 --> 00:00:46,240
 And as best you can then come back again to your ordinary

15
00:00:46,240 --> 00:00:47,940
 meditation practice

