1
00:00:00,000 --> 00:00:03,480
 When I come to see something really clearly catching a bad

2
00:00:03,480 --> 00:00:07,200
 thought in the middle of daily activities, for instance, it

3
00:00:07,200 --> 00:00:11,000
 often triggers joy in me. I feel I have insight.

4
00:00:11,000 --> 00:00:13,980
 Do you think this joy is wholesome or is it based on ego?

5
00:00:13,980 --> 00:00:16,000
 How to deal with it?

6
00:00:16,000 --> 00:00:20,530
 Again, the importance or the usefulness of the abhidhamma.

7
00:00:20,530 --> 00:00:25,260
 Joy, somanasa, can be either wholesome or unwholesome. In

8
00:00:25,260 --> 00:00:31,220
 and of itself, it's neither. It can be accompanied by

9
00:00:31,220 --> 00:00:34,000
 wholesomeness or unwholesomeness.

10
00:00:34,000 --> 00:00:38,070
 If you like something and that has a joy associated with it

11
00:00:38,070 --> 00:00:42,490
, then that's unwholesome. If you do something positive,

12
00:00:42,490 --> 00:00:46,960
 like help someone else or cultivate insight, and joy comes

13
00:00:46,960 --> 00:00:53,000
 spontaneously from that, then that's neutral.

14
00:00:53,000 --> 00:00:57,570
 If you do something wholesome and you feel good about that

15
00:00:57,570 --> 00:01:01,930
 wholesomeness and feel joy doing good deeds at the moment

16
00:01:01,930 --> 00:01:05,000
 of doing, then that's wholesome.

17
00:01:05,000 --> 00:01:08,140
 But it's not wholesome because of the feeling. It's not

18
00:01:08,140 --> 00:01:11,880
 wholesome because of the joy. Joy is indeterminate. It's a

19
00:01:11,880 --> 00:01:17,190
 part of the mind that is neither positive or negative. It's

20
00:01:17,190 --> 00:01:20,000
 indeterminate. It's neutral.

21
00:01:20,000 --> 00:01:23,500
 It depends what it's accompanied with. It can come in three

22
00:01:23,500 --> 00:01:29,510
 spots. One, in an unwholesome mind. Two, in a wholesome

23
00:01:29,510 --> 00:01:34,950
 mind. And three, in a mind that is the result of a wholes

24
00:01:34,950 --> 00:01:36,000
ome mind.

25
00:01:36,000 --> 00:01:38,770
 So feeling happy because of something good that you've done

26
00:01:38,770 --> 00:01:39,000
.

27
00:01:39,000 --> 00:01:42,420
 The first one is unwholesome. The second one is wholesome.

28
00:01:42,420 --> 00:01:46,180
 But the third one, the result, is neutral. And in all three

29
00:01:46,180 --> 00:01:50,620
 cases, the quality of the mind is not determined by the joy

30
00:01:50,620 --> 00:01:51,000
.

31
00:01:51,000 --> 00:01:53,580
 So joy is indeterminate. Do you see how useful the abhidham

32
00:01:53,580 --> 00:01:58,000
ma is? I can answer that question without any doubt.

33
00:01:58,000 --> 00:02:09,010
 Whether you believe me or not, this is another thing. De

34
00:02:09,010 --> 00:02:09,280
aling with it is just to say to yourself, "Happy, happy,

35
00:02:09,280 --> 00:02:09,610
 feeling, feeling." If you like it, say, "Liking, liking,"

36
00:02:09,610 --> 00:02:10,000
 and so on. See it for what it is.

