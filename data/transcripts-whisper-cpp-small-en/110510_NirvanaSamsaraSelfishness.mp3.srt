1
00:00:00,000 --> 00:00:04,850
 Hello, welcome back to Ask a Monk. This next question is a

2
00:00:04,850 --> 00:00:07,200
 pretty good example, I think,

3
00:00:07,200 --> 00:00:12,590
 of how not to ask questions on this forum. And that's

4
00:00:12,590 --> 00:00:13,600
 because it's specifically...

5
00:00:13,600 --> 00:00:23,510
 Well, it seems quite clearly to be a presentation of one's

6
00:00:23,510 --> 00:00:24,880
 own philosophy

7
00:00:24,880 --> 00:00:27,360
 in the form of a question, because here we have a premise

8
00:00:27,360 --> 00:00:29,520
 and a question. I'll read it to you.

9
00:00:30,240 --> 00:00:32,790
 I want to ask about non-duality. When one doesn't

10
00:00:32,790 --> 00:00:36,000
 distinguish the two realms of samsara and nirvana,

11
00:00:36,000 --> 00:00:40,060
 then there is no goal. So my question is, is wanting to

12
00:00:40,060 --> 00:00:43,040
 transcend rebirth selfish? But what

13
00:00:43,040 --> 00:00:46,200
 you have here is a complete philosophy that exists in

14
00:00:46,200 --> 00:00:48,640
 Buddhism. In Buddhism there are many

15
00:00:48,640 --> 00:00:53,420
 philosophies and they're not all compatible. This one I

16
00:00:53,420 --> 00:00:55,520
 happen to not agree with. And

17
00:00:57,120 --> 00:01:01,400
 this is a complete presentation of this philosophy. And it

18
00:01:01,400 --> 00:01:04,880
's clearly so because the

19
00:01:04,880 --> 00:01:11,740
 premise and the conclusion that is presented in the premise

20
00:01:11,740 --> 00:01:15,200
, which the implicit conclusion being

21
00:01:15,200 --> 00:01:17,840
 that striving for the end of rebirth is selfish, it has

22
00:01:17,840 --> 00:01:20,080
 nothing to do... they have nothing to do

23
00:01:20,080 --> 00:01:25,240
 with each other unless... except in the greater framework

24
00:01:25,240 --> 00:01:28,080
 of this philosophy, this specific

25
00:01:28,080 --> 00:01:34,460
 philosophy. So it seems clear that the purpose of this post

26
00:01:34,460 --> 00:01:37,040
 is not to ask a question, it's to

27
00:01:37,040 --> 00:01:39,980
 present a philosophy, which is not really appropriate. Now,

28
00:01:39,980 --> 00:01:41,680
 I'm not sure this is the

29
00:01:41,680 --> 00:01:45,010
 case. It may be that the person asking is really interested

30
00:01:45,010 --> 00:01:46,720
 and really thinks that I believe

31
00:01:47,600 --> 00:01:51,980
 that these two things are indistinguishable and somehow

32
00:01:51,980 --> 00:01:56,560
 thinks that that effects... has some

33
00:01:56,560 --> 00:02:00,190
 effect on the question as to whether sending rebirth is

34
00:02:00,190 --> 00:02:05,440
 selfish. I don't see it. So briefly

35
00:02:05,440 --> 00:02:07,670
 I'll go over this, but this isn't the kind of question that

36
00:02:07,670 --> 00:02:09,280
 I'm interested in asking. I would

37
00:02:09,280 --> 00:02:15,240
 like to be answering questions about meditation and about

38
00:02:15,240 --> 00:02:19,680
... that really help you understand the

39
00:02:19,680 --> 00:02:24,200
 Buddha's teaching and understand the meditation practice

40
00:02:24,200 --> 00:02:26,720
 and improve your meditation practice and

41
00:02:26,720 --> 00:02:31,810
 help you along the path. So, but anyway, the premise, why I

42
00:02:31,810 --> 00:02:34,240
 disagree with this philosophy,

43
00:02:36,560 --> 00:02:39,780
 the idea that samsara and nirvana are indistinguishable is

44
00:02:39,780 --> 00:02:43,760
 just a sophism. It's a

45
00:02:43,760 --> 00:02:46,610
 theory, it's a view. It has something to do with reality.

46
00:02:46,610 --> 00:02:48,720
 If you experience samsara, samsara is

47
00:02:48,720 --> 00:02:51,000
 one thing. This is samsara, seeing, the hearing, the

48
00:02:51,000 --> 00:02:53,520
 smelling, the tasting, the feeling, and thinking.

49
00:02:53,520 --> 00:02:58,460
 It's the arising of suffering and the pursuit of suffering.

50
00:02:58,460 --> 00:03:00,400
 Nirvana is the opposite. It's the

51
00:03:00,400 --> 00:03:05,630
 non-arising of suffering, the non-pursuit of suffering. And

52
00:03:05,630 --> 00:03:08,480
 the experience of it is completely

53
00:03:08,480 --> 00:03:11,140
 different. The reason why people are unable to distinguish

54
00:03:11,140 --> 00:03:12,960
 it is because they haven't experienced

55
00:03:12,960 --> 00:03:16,210
 it. They... everything they've experienced is the same.

56
00:03:16,210 --> 00:03:18,480
 They practice meditation and their experiences

57
00:03:18,480 --> 00:03:21,750
 come and go and so they relate that back to samsara and it

58
00:03:21,750 --> 00:03:23,840
 also comes and goes and everything comes

59
00:03:23,840 --> 00:03:26,070
 and goes and they say nothing is different. So they're

60
00:03:26,070 --> 00:03:27,680
 unable to distinguish one from the other,

61
00:03:27,680 --> 00:03:30,470
 which is correct really because everything is

62
00:03:30,470 --> 00:03:34,880
 indistinguishable except nirvana. Everything else

63
00:03:34,880 --> 00:03:38,160
 arises and ceases and this is why the Buddha said that

64
00:03:38,160 --> 00:03:41,680
 nothing's worth clinging to because it

65
00:03:41,680 --> 00:03:48,450
 all arises and ceases. So it's basically equating two oppos

66
00:03:48,450 --> 00:03:52,320
ites or confusing two opposites. It's like

67
00:03:54,320 --> 00:03:56,170
 saying that light and day, night and day are

68
00:03:56,170 --> 00:03:58,240
 indistinguishable. Light and darkness are

69
00:03:58,240 --> 00:04:05,650
 indistinguishable, which, you know, theoretically is... you

70
00:04:05,650 --> 00:04:09,280
 can come up with a theory that says

71
00:04:09,280 --> 00:04:12,000
 that is the case but doesn't affect the reality that light

72
00:04:12,000 --> 00:04:13,760
 is quite different from darkness

73
00:04:13,760 --> 00:04:18,860
 and that's objectively so and nirvana and samsara are

74
00:04:18,860 --> 00:04:22,560
 objectively so as well, no matter what theory

75
00:04:22,560 --> 00:04:25,670
 or philosophy you come up with because you can come up with

76
00:04:25,670 --> 00:04:27,040
 any philosophy you like

77
00:04:27,040 --> 00:04:31,660
 and it doesn't affect... it doesn't have any effect on

78
00:04:31,660 --> 00:04:34,320
 reality. Now, as to the question,

79
00:04:34,320 --> 00:04:39,450
 I'm assuming that the implication here is that somehow that

80
00:04:39,450 --> 00:04:42,000
 the answer... that premise

81
00:04:42,640 --> 00:04:49,350
 implies that or leads to the conclusion, supports the

82
00:04:49,350 --> 00:04:54,720
 conclusion that freedom from or the desire,

83
00:04:54,720 --> 00:04:58,350
 wanting to transcend rebirth is selfish. I'm assuming that

84
00:04:58,350 --> 00:04:59,760
's what's implied here.

85
00:04:59,760 --> 00:05:05,240
 So I can talk about that. Selfishness only really comes

86
00:05:05,240 --> 00:05:08,640
 into play in Buddhism, as I understand it,

87
00:05:10,320 --> 00:05:13,090
 in terms of clinging. So if you cling to something and you

88
00:05:13,090 --> 00:05:14,880
 don't want someone else to have it,

89
00:05:14,880 --> 00:05:18,040
 if you ask me for something and I don't want to give it to

90
00:05:18,040 --> 00:05:19,440
 you, it's only because I'm clinging

91
00:05:19,440 --> 00:05:24,460
 to the object. There's no true selfishness that arises. It

92
00:05:24,460 --> 00:05:27,840
's a clinging. It's a function of the

93
00:05:27,840 --> 00:05:30,930
 mind that wants an object. You don't think about the other

94
00:05:30,930 --> 00:05:33,440
 person. What we call selfishness is

95
00:05:33,440 --> 00:05:38,930
 this absorption in one's attachment. And only by overcoming

96
00:05:38,930 --> 00:05:41,760
 that can you possibly give up

97
00:05:41,760 --> 00:05:44,520
 something that is a benefit to you. And we're able to do

98
00:05:44,520 --> 00:05:46,560
 that as we grow up. It's not because we

99
00:05:46,560 --> 00:05:49,590
 become... well, it could be because we become what we call

100
00:05:49,590 --> 00:05:55,680
 more altruistic, but that is not

101
00:05:55,680 --> 00:06:00,200
 the way of the Buddha. That's not the answer because this

102
00:06:00,200 --> 00:06:02,400
 idea of altruism is just another theory.

103
00:06:02,400 --> 00:06:04,840
 It's another mind game. And there are people who even

104
00:06:04,840 --> 00:06:07,040
 refute this theory that say, "How can you

105
00:06:07,040 --> 00:06:11,080
 possibly work for the benefit of other people?" But the

106
00:06:11,080 --> 00:06:14,240
 Buddha's teaching doesn't go either way.

107
00:06:14,240 --> 00:06:18,830
 There's no idea that another person's benefit is better

108
00:06:18,830 --> 00:06:21,440
 than working for one's own benefit.

109
00:06:21,440 --> 00:06:25,410
 You act in such a way that brings harmony. You act in such

110
00:06:25,410 --> 00:06:27,920
 a way that leads to the least conflict.

111
00:06:27,920 --> 00:06:32,430
 You act in such a way that is going to be most appropriate

112
00:06:32,430 --> 00:06:35,200
 at any given time. And it's not a

113
00:06:35,200 --> 00:06:38,360
 intellectual exercise. You don't have to study the books

114
00:06:38,360 --> 00:06:40,080
 and learn what is most appropriate,

115
00:06:40,080 --> 00:06:43,360
 though that can help for people who don't have the

116
00:06:43,360 --> 00:06:49,200
 experience. You understand the nature of reality

117
00:06:49,200 --> 00:06:52,380
 through your practice and therefore are able to clearly see

118
00:06:52,380 --> 00:06:54,240
 what is of the greatest benefit,

119
00:06:54,240 --> 00:06:56,870
 what is the most appropriate at any given time. And it

120
00:06:56,870 --> 00:06:58,720
 might be acting for your own benefit. It

121
00:06:58,720 --> 00:07:01,970
 might be acting for someone else's benefit. But the point

122
00:07:01,970 --> 00:07:03,840
 is not either/or. The point is

123
00:07:03,840 --> 00:07:10,430
 the appropriateness of the act. So if someone comes and

124
00:07:10,430 --> 00:07:12,160
 asks you for something,

125
00:07:12,160 --> 00:07:15,440
 many things might come into play. It depends on the

126
00:07:15,440 --> 00:07:17,760
 situation. If you ask me for something,

127
00:07:17,760 --> 00:07:20,940
 I might give it to you, even though it might cause me

128
00:07:20,940 --> 00:07:23,280
 suffering. There may be many reasons for that.

129
00:07:23,280 --> 00:07:26,210
 It could be because you've helped me before. It could be

130
00:07:26,210 --> 00:07:28,160
 because I am able to deal with suffering.

131
00:07:28,160 --> 00:07:32,240
 I know that you're not. If I have practiced meditation and

132
00:07:32,240 --> 00:07:34,080
 I am able to be patient and so

133
00:07:34,080 --> 00:07:37,080
 on, and I can see that you're not, I can see that giving to

134
00:07:37,080 --> 00:07:39,200
 you will lead to less suffering overall.

135
00:07:39,200 --> 00:07:42,430
 Because for me, it's only physical suffering for you. It's

136
00:07:42,430 --> 00:07:44,320
 mental and so on. I might not give you

137
00:07:44,320 --> 00:07:47,260
 any of it because I might see that you are not going to use

138
00:07:47,260 --> 00:07:49,040
 it. Or I might see that if you're

139
00:07:49,040 --> 00:07:51,450
 without it, you're going to learn something. You're going

140
00:07:51,450 --> 00:07:52,720
 to learn patience and so on.

141
00:07:54,320 --> 00:07:56,110
 There are many responses. This is talking about an

142
00:07:56,110 --> 00:07:59,600
 enlightened person. If a person is enlightened,

143
00:07:59,600 --> 00:08:02,960
 they will act in this way. They will never rise to the my

144
00:08:02,960 --> 00:08:04,640
 benefit, your benefit,

145
00:08:04,640 --> 00:08:09,240
 because there's no clinging. That's the only time that

146
00:08:09,240 --> 00:08:13,120
 selfishness arises. So the idea that wanting

147
00:08:13,120 --> 00:08:16,970
 to transcend rebirth is selfish really doesn't come, doesn

148
00:08:16,970 --> 00:08:19,360
't, the idea of selfishness doesn't

149
00:08:19,360 --> 00:08:22,270
 come into play. It has nothing to do with anyone else. And

150
00:08:22,270 --> 00:08:24,640
 the only way you could call it selfish

151
00:08:24,640 --> 00:08:28,500
 is if you start playing these mind games and logic and, you

152
00:08:28,500 --> 00:08:30,880
 know, it's an intellectual exercise

153
00:08:30,880 --> 00:08:34,450
 where you say, well, if you did stay around, you could help

154
00:08:34,450 --> 00:08:36,320
 so many people. And therefore,

155
00:08:36,320 --> 00:08:39,860
 it is selfish to, you know, but it's just a theory, right?

156
00:08:39,860 --> 00:08:41,920
 It has nothing to do with the state of mind.

157
00:08:41,920 --> 00:08:44,630
 The person could be totally unselfish. And if anyone asked

158
00:08:44,630 --> 00:08:46,240
 them for something, they would help

159
00:08:46,240 --> 00:08:48,970
 them. But they don't ever think, hmm, why I should stick

160
00:08:48,970 --> 00:08:51,200
 around. Otherwise, it's going to be selfish,

161
00:08:51,200 --> 00:08:55,230
 because there's no clinging. There's no attachment to this

162
00:08:55,230 --> 00:08:58,000
 state or that state. The person simply

163
00:08:58,000 --> 00:09:02,350
 acts as is appropriate in that, in that instance. So the

164
00:09:02,350 --> 00:09:08,240
 realization of freedom from rebirth or the

165
00:09:08,240 --> 00:09:10,810
 attainment of freedom from rebirth is of the greatest

166
00:09:10,810 --> 00:09:12,800
 benefit to a person. If a person wants

167
00:09:12,800 --> 00:09:16,180
 to strive for that, then that's a good thing. It creates

168
00:09:16,180 --> 00:09:20,720
 benefit. If a person decides to extend

169
00:09:20,720 --> 00:09:26,250
 or not work towards the freedom from rebirth and they want

170
00:09:26,250 --> 00:09:28,400
 to be reborn again and again,

171
00:09:28,400 --> 00:09:31,360
 then that's their prerogative. The Buddha never laid down

172
00:09:31,360 --> 00:09:35,040
 any, you know, laws of nature in this

173
00:09:35,040 --> 00:09:37,970
 regard, because there are none. There is simply the cause

174
00:09:37,970 --> 00:09:42,480
 and effect. If a person decides that

175
00:09:42,480 --> 00:09:46,030
 they want to extend their existence and come back again and

176
00:09:46,030 --> 00:09:47,840
 again and be reborn again and again,

177
00:09:47,840 --> 00:09:50,450
 thinking that they're going to help people, then that's

178
00:09:50,450 --> 00:09:52,400
 fine. Or that's their choice. That's

179
00:09:52,400 --> 00:09:56,660
 their path. There's no need for any sort of judgment at all

180
00:09:56,660 --> 00:10:00,560
. If a person decides that they want to

181
00:10:00,560 --> 00:10:04,570
 become free from suffering in this life and not come back

182
00:10:04,570 --> 00:10:06,400
 and be free from rebirth,

183
00:10:07,440 --> 00:10:10,660
 then that's their path as well. There's only the cause and

184
00:10:10,660 --> 00:10:12,960
 effect. So a person who is reborn again

185
00:10:12,960 --> 00:10:15,350
 and again and again will have to come back again and again

186
00:10:15,350 --> 00:10:16,560
 and suffer again and again.

187
00:10:16,560 --> 00:10:21,110
 And, you know, it's questionable as to how much help they

188
00:10:21,110 --> 00:10:24,160
 can possibly be, given that they are

189
00:10:24,160 --> 00:10:26,640
 not yet free from clinging, because a person who is free

190
00:10:26,640 --> 00:10:30,480
 from clinging would not be reborn. And

191
00:10:36,960 --> 00:10:41,030
 the person, actually the person who does become free from

192
00:10:41,030 --> 00:10:45,120
 rebirth, who does transcend rebirth,

193
00:10:45,120 --> 00:10:48,860
 so to speak, is able to help people, because their mind is

194
00:10:48,860 --> 00:10:51,200
 free from clinging, and therefore they are

195
00:10:51,200 --> 00:10:54,940
 able to provide great support and help and great insight to

196
00:10:54,940 --> 00:10:57,600
 people. The Buddha is a good example.

197
00:10:58,560 --> 00:11:04,870
 Now, there is the question of whether the Buddha and

198
00:11:04,870 --> 00:11:08,880
 enlightened beings do come back, and because

199
00:11:08,880 --> 00:11:11,270
 there are even philosophies that believe a person who is

200
00:11:11,270 --> 00:11:13,120
 free from clinging still is reborn,

201
00:11:13,120 --> 00:11:17,090
 but that's not really the question here. So, or it's

202
00:11:17,090 --> 00:11:19,520
 getting a little bit too much into

203
00:11:19,520 --> 00:11:23,430
 other people's philosophies, and I don't want to create

204
00:11:23,430 --> 00:11:26,160
 more confusion than is necessary.

205
00:11:26,160 --> 00:11:29,420
 So the point here is that we work for benefit, we work for

206
00:11:29,420 --> 00:11:32,640
 welfare, we do what's appropriate,

207
00:11:32,640 --> 00:11:37,720
 or we try to, this is our practice, and an enlightened

208
00:11:37,720 --> 00:11:40,960
 being is someone who is perfect

209
00:11:40,960 --> 00:11:46,720
 at it, who doesn't have any thought of self or other, and

210
00:11:46,720 --> 00:11:49,520
 is not subject to suffering. When they

211
00:11:49,520 --> 00:11:54,900
 pass away from this life, they are free from all suffering,

212
00:11:54,900 --> 00:11:59,680
 and there's no more coming back,

213
00:11:59,680 --> 00:12:05,120
 no more arising. So, basically I just wanted to say that

214
00:12:05,120 --> 00:12:13,040
 this is not the sort of question that I'm

215
00:12:13,040 --> 00:12:16,060
 hoping to entertain, and I think probably in the future I'm

216
00:12:16,060 --> 00:12:17,600
 just going to skip over, because I

217
00:12:17,600 --> 00:12:20,090
 got a lot of questions, and I'm going to try to be

218
00:12:20,090 --> 00:12:23,040
 selective. I've been quite random actually

219
00:12:23,040 --> 00:12:25,120
 in choosing which questions to answer, because it's

220
00:12:25,120 --> 00:12:31,840
 difficult to sort them. I've got a lot of

221
00:12:31,840 --> 00:12:35,740
 them, and Google doesn't make it easy by any means. So I'm

222
00:12:35,740 --> 00:12:40,800
 going to try to skip and go directly

223
00:12:40,800 --> 00:12:44,960
 to those ones that I think are going to be useful for

224
00:12:44,960 --> 00:12:48,080
 people. Okay, so anyway thanks for tuning in.

225
00:12:48,080 --> 00:12:51,520
 This has been another episode of Ask a Monk, and I'm

226
00:12:51,520 --> 00:12:54,160
 wishing you all peace, happiness, and freedom

227
00:12:54,160 --> 00:13:03,840
 from suffering.

