1
00:00:00,000 --> 00:00:11,600
 Okay, good evening everyone.

2
00:00:11,600 --> 00:00:22,360
 Welcome to our live broadcast evening Dhamma session.

3
00:00:22,360 --> 00:00:26,280
 So last night we started talking about mindfulness.

4
00:00:26,280 --> 00:00:29,790
 That was to answer the question of what is wrong

5
00:00:29,790 --> 00:00:31,000
 mindfulness,

6
00:00:31,000 --> 00:00:34,160
 but I ended up spending all the time talking about what is

7
00:00:34,160 --> 00:00:35,160
 mindfulness.

8
00:00:35,160 --> 00:00:39,640
 So tonight, I'll briefly talk about how mindfulness goes

9
00:00:39,640 --> 00:00:40,120
 wrong.

10
00:00:40,120 --> 00:00:43,770
 I mean, it should be quite interesting as meditators to

11
00:00:43,770 --> 00:00:55,080
 know how the practice can go wrong.

12
00:00:55,080 --> 00:01:00,370
 The first thing I think to note is that mindfulness is

13
00:01:00,370 --> 00:01:02,320
 never wrong.

14
00:01:02,320 --> 00:01:05,280
 Mindfulness is a sobanajeetaseekha.

15
00:01:05,280 --> 00:01:11,600
 Sadhar, sobanajeetaseekha.

16
00:01:11,600 --> 00:01:17,630
 I can't remember the word, but it's a beautiful quality of

17
00:01:17,630 --> 00:01:19,000
 mind.

18
00:01:19,000 --> 00:01:29,800
 Mindfulness is common to all wholesome states, I think.

19
00:01:29,800 --> 00:01:36,350
 So mindfulness itself can never be wrong and it's always

20
00:01:36,350 --> 00:01:37,640
 useful.

21
00:01:37,640 --> 00:01:43,930
 So when we talk about wrong mindfulness, we're not actually

22
00:01:43,930 --> 00:01:45,280
 talking about mindfulness being

23
00:01:45,280 --> 00:01:46,280
 wrong.

24
00:01:46,280 --> 00:01:49,670
 We're talking more about the practice of mindfulness going

25
00:01:49,670 --> 00:01:50,280
 wrong.

26
00:01:50,280 --> 00:01:54,280
 But furthermore, the practice of mindfulness, it is quite

27
00:01:54,280 --> 00:01:56,480
 difficult for it to go wrong.

28
00:01:56,480 --> 00:02:00,880
 And I want to stress that because with other types of

29
00:02:00,880 --> 00:02:02,760
 meditation, it actually is quite

30
00:02:02,760 --> 00:02:06,880
 easy for it to go wrong.

31
00:02:06,880 --> 00:02:08,400
 I'll talk a little bit about that.

32
00:02:08,400 --> 00:02:12,640
 Maybe I'll wait and talk during the talk about that.

33
00:02:12,640 --> 00:02:16,320
 And mindfulness is so simple.

34
00:02:16,320 --> 00:02:21,520
 The first thing, it's difficult to misunderstand the

35
00:02:21,520 --> 00:02:23,160
 instruction.

36
00:02:23,160 --> 00:02:30,280
 It's difficult to be in doubt about what is being asked.

37
00:02:30,280 --> 00:02:35,600
 And it's difficult to apply mindfulness.

38
00:02:35,600 --> 00:02:45,030
 And it's really impossible to apply mindfulness and go too

39
00:02:45,030 --> 00:02:46,240
 far.

40
00:02:46,240 --> 00:02:48,120
 With other types of meditation, you have to be careful.

41
00:02:48,120 --> 00:02:51,920
 If you go too far, you can get out of your depths.

42
00:02:51,920 --> 00:02:55,650
 But with mindfulness, you don't really ever get out of your

43
00:02:55,650 --> 00:02:56,400
 depths.

44
00:02:56,400 --> 00:02:59,320
 It can seem overwhelming, but it's in a different way.

45
00:02:59,320 --> 00:03:03,260
 The overwhelming nature of it is real.

46
00:03:03,260 --> 00:03:06,360
 It's just your own mind.

47
00:03:06,360 --> 00:03:10,400
 Whereas other types of meditation, you can get lost because

48
00:03:10,400 --> 00:03:12,040
 they're conceptual.

49
00:03:12,040 --> 00:03:13,280
 We'll talk about that.

50
00:03:13,280 --> 00:03:22,600
 The first way mindfulness can go wrong is unmindfulness.

51
00:03:22,600 --> 00:03:29,190
 The four ways mindfulness can go wrong is one, unmind

52
00:03:29,190 --> 00:03:34,840
fulness, two, misdirected mindfulness,

53
00:03:34,840 --> 00:03:47,340
 three, lapsed mindfulness, and four, impotent or ineffect

54
00:03:47,340 --> 00:03:50,480
ual mindfulness.

55
00:03:50,480 --> 00:03:53,880
 It might be the best word.

56
00:03:53,880 --> 00:03:59,280
 Let's see if I can get to this again.

57
00:03:59,280 --> 00:04:01,360
 Unmindfulness is of course the opposite.

