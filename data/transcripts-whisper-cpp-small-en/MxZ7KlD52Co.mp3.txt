 "How do I know I am being mindful enough and practicing
 properly?"
 It's a difficult question to answer because you really have
 to understand where your question
 is coming from.
 I mean you, I mean when you ask this question the important
 thing for the teacher to point
 out is what's giving rise to this question.
 And what's giving rise to it, maybe what's giving rise to
 it, I would venture a guess
 is some kind of doubt or anxiety about your practice, about
 who you are and about your
 status as a meditator and so on.
 It could come from doubts or uncertainty or even can come
 simply from curiosity.
 But all of these are mind states and all of those are what
 you should be focusing on rather
 than asking such questions.
 That's really avoiding the question, isn't it?
 I don't think it's a very good question to ask and I'm not
 criticizing you because it's
 a question that I get a lot but the way to answer it is to
 divert the focus back to where
 it should be and that is on the states of mind when they
 arise rather than worrying
 about how your practice is doing or how mindful you are.
 There's kind of a, there's a general confusion that occurs
 in the meditator's mind because
 they are indoctrinated culturally into the idea that
 meditation is about concentration.
 So they use the word mindfulness but their brain is still
 thinking concentration and
 so they ask questions and this is a very common question,
 am I being mindful enough?
 You can't possibly qualify, qualify?
 There's no quality to mindfulness.
 You either are mindfulness or you are not and you can only
 be mindful one moment at
 a time.
 This is what is confirmed by our use of this word.
 The word is the recognition but that recognition occurs,
 the word mindfulness which really
 means to recognize and to fully grasp the object as it is,
 it occurs for one moment
 and if you're not practicing the next moment then that goes
 away.
 If you're not quick and you're not catching the next one
 then you miss the whole point.
 So the answer here is to try and see it moment to moment.
 Don't worry about your quality of practice.
 Don't worry about your quality of mind.
 People will always come to me, meditators will always come
 to me and say I'm not concentrated
 enough and I will remind them again and again and again
 that it's not really what we're
 looking for.
 You know I didn't ask how concentrated are you, how's your
 practice and then I was like
 oh I'm not very concentrated.
 Well I didn't ask that.
 I mean basically the meaning is that's not how I would
 judge your practice by how concentrated
 you are but when you're not concentrated as the Buddha said
 in the Satipatthana sutta
 you should know when I'm unfocused he knows I'm unfocused,
 when he's focused he knows
 he's focused.
 I think that will help you and I'm happy to not only
 progress but to hopefully avoid what
 may be and I'm assuming likely is doubts about yourself and
 your practice and worrying about
 the practice.
 Okay, glad that helped.
