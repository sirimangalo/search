1
00:00:00,000 --> 00:00:04,170
 Hello and welcome back to Ask a Monk. Today's question is

2
00:00:04,170 --> 00:00:06,320
 about drowsiness. So I

3
00:00:06,320 --> 00:00:11,220
 was asked how to deal with falling asleep when meditating.

4
00:00:11,220 --> 00:00:11,920
 Someone said they

5
00:00:11,920 --> 00:00:14,250
 would like to be able to meditate and they've tried but

6
00:00:14,250 --> 00:00:15,200
 they find themselves

7
00:00:15,200 --> 00:00:18,320
 falling asleep every time. I think I've addressed this

8
00:00:18,320 --> 00:00:20,080
 before but not

9
00:00:20,080 --> 00:00:24,570
 comprehensively so I'd like to do that now, sort of

10
00:00:24,570 --> 00:00:26,560
 referring back to the Buddhist

11
00:00:26,560 --> 00:00:33,520
 teaching. Now the person who asked the question this time

12
00:00:33,520 --> 00:00:34,800
 said that he

13
00:00:34,800 --> 00:00:39,960
 would like, he or she would like to use the meditation for

14
00:00:39,960 --> 00:00:42,360
 the purpose of

15
00:00:42,360 --> 00:00:46,490
 feeling rested. So they want to be able to find rest but

16
00:00:46,490 --> 00:00:47,160
 still be alert.

17
00:00:47,160 --> 00:00:53,000
 And I'd like to caution that that may be the reason why you

18
00:00:53,000 --> 00:00:53,840
're feeling falling

19
00:00:53,840 --> 00:00:56,410
 asleep when you meditate because you've got the wrong

20
00:00:56,410 --> 00:00:57,560
 understanding of what

21
00:00:57,560 --> 00:01:02,400
 meditation should be. If your intention is to simply rest

22
00:01:02,400 --> 00:01:04,280
 then falling asleep

23
00:01:04,280 --> 00:01:09,830
 is probably the best way to do it to an extent. I mean I

24
00:01:09,830 --> 00:01:11,080
 guess if that's the only

25
00:01:11,080 --> 00:01:15,440
 the point is if that's the only intention that you go into

26
00:01:15,440 --> 00:01:15,520
 the

27
00:01:15,520 --> 00:01:20,060
 meditation with and that's the direction that your mind is

28
00:01:20,060 --> 00:01:21,760
 going to go. If you

29
00:01:21,760 --> 00:01:24,690
 sit down and meditate and say okay I want to feel rested

30
00:01:24,690 --> 00:01:25,360
 your mind is not

31
00:01:25,360 --> 00:01:30,260
 going to have to develop energy, it's not going to be awake

32
00:01:30,260 --> 00:01:31,840
 and alert, it's going

33
00:01:31,840 --> 00:01:36,380
 to start to tend towards the only way that it knows how to

34
00:01:36,380 --> 00:01:37,680
 rest and that is to

35
00:01:37,680 --> 00:01:40,710
 fall asleep. There are better ways and meditation I think

36
00:01:40,710 --> 00:01:42,240
 certainly does lead

37
00:01:42,240 --> 00:01:45,640
 to much more of a rested mind state but the mind won't go

38
00:01:45,640 --> 00:01:47,120
 in that direction

39
00:01:47,120 --> 00:01:51,080
 unless your intention in going into the meditation is to

40
00:01:51,080 --> 00:01:51,880
 use it for the

41
00:01:51,880 --> 00:01:55,420
 purpose of meditating and that is to understand reality and

42
00:01:55,420 --> 00:01:56,120
 to be able to

43
00:01:56,120 --> 00:01:59,500
 deal with the difficulties and the problems and to

44
00:01:59,500 --> 00:02:00,560
 understand the

45
00:02:00,560 --> 00:02:05,010
 difficulties and the problems that you have in your mind

46
00:02:05,010 --> 00:02:06,760
 and in your life. If you

47
00:02:06,760 --> 00:02:09,540
 go into the meditation with this mindset, this sort of

48
00:02:09,540 --> 00:02:11,320
 mindset, then your

49
00:02:11,320 --> 00:02:15,240
 mind is more likely to be alert and to be interested and

50
00:02:15,240 --> 00:02:16,480
 you're more likely to

51
00:02:16,480 --> 00:02:18,900
 be able to pay attention, less likely to fall asleep. So

52
00:02:18,900 --> 00:02:20,520
 that's the first bit of

53
00:02:20,520 --> 00:02:24,530
 advice I would give is to try to reevaluate what you expect

54
00:02:24,530 --> 00:02:25,440
 to get out of

55
00:02:25,440 --> 00:02:30,360
 meditation. Resting is certainly not the purpose of

56
00:02:30,360 --> 00:02:32,320
 practicing meditation, not

57
00:02:32,320 --> 00:02:35,840
 the best aim and if that's the aim going into it, it's as I

58
00:02:35,840 --> 00:02:36,580
 said going to

59
00:02:36,580 --> 00:02:39,420
 have these sorts of repercussions causing you to fall

60
00:02:39,420 --> 00:02:42,640
 asleep. Nonetheless, for those

61
00:02:42,640 --> 00:02:45,570
 meditators who have made up their mind and come to

62
00:02:45,570 --> 00:02:47,200
 understand what it is that

63
00:02:47,200 --> 00:02:50,490
 meditation is for and are using it for the purpose of

64
00:02:50,490 --> 00:02:51,920
 coming to understand

65
00:02:51,920 --> 00:02:55,880
 reality as it is, there are times especially when doing

66
00:02:55,880 --> 00:02:56,600
 intensive

67
00:02:56,600 --> 00:02:59,760
 meditation that you find yourself feeling drowsy and even

68
00:02:59,760 --> 00:03:00,640
 falling asleep.

69
00:03:00,640 --> 00:03:04,870
 So how do you deal with this? Well the Buddha gave seven

70
00:03:04,870 --> 00:03:06,680
 specific techniques

71
00:03:06,680 --> 00:03:13,000
 that one could use to overcome drowsiness and there are

72
00:03:13,000 --> 00:03:13,800
 actually seven

73
00:03:13,800 --> 00:03:16,460
 different sort of types of, they deal with seven different

74
00:03:16,460 --> 00:03:17,520
 types of drowsiness

75
00:03:17,520 --> 00:03:21,170
 or at least a few different types of drowsiness. Each one

76
00:03:21,170 --> 00:03:22,000
 is unique in

77
00:03:22,000 --> 00:03:27,450
 its approach and so I'd like to in this case refer directly

78
00:03:27,450 --> 00:03:28,040
 back to this.

79
00:03:28,040 --> 00:03:32,980
 It's the Pachalaayana Sutta or Pachala Sutta or Chappala

80
00:03:32,980 --> 00:03:33,620
 Sutta. It's in the

81
00:03:33,620 --> 00:03:38,960
 Anguttra Nikaya Book of Sevens. So we have seven ways of

82
00:03:38,960 --> 00:03:39,480
 dealing with

83
00:03:39,480 --> 00:03:44,540
 drowsiness. So the first way that the Buddha said is to

84
00:03:44,540 --> 00:03:45,960
 change your object or

85
00:03:45,960 --> 00:03:53,040
 to examine the object of your attention because one of the

86
00:03:53,040 --> 00:03:55,320
 most obvious

87
00:03:55,320 --> 00:03:59,470
 reasons why someone might become drowsy is because their

88
00:03:59,470 --> 00:04:00,080
 mind has

89
00:04:00,080 --> 00:04:03,920
 begun to wander. When you're sitting in meditation you

90
00:04:03,920 --> 00:04:05,200
 begin by focusing on a

91
00:04:05,200 --> 00:04:10,390
 specific object but your mind starts to wander and slowly

92
00:04:10,390 --> 00:04:12,440
 fall into a more

93
00:04:12,440 --> 00:04:17,200
 trance-like state that is bordering on sleep and eventually

94
00:04:17,200 --> 00:04:17,600
 will lead

95
00:04:17,600 --> 00:04:21,820
 one to fall asleep. Now when that is the case it's

96
00:04:21,820 --> 00:04:24,200
 important to think about what

97
00:04:24,200 --> 00:04:29,050
 it was that you were considering when you were feeling drow

98
00:04:29,050 --> 00:04:29,240
sy

99
00:04:29,240 --> 00:04:35,020
 and to change that, to go back to focusing on the original

100
00:04:35,020 --> 00:04:36,000
 object and to

101
00:04:36,000 --> 00:04:41,440
 be careful not to let yourself fall into the reflection on

102
00:04:41,440 --> 00:04:41,960
 these

103
00:04:41,960 --> 00:04:45,250
 speculative thoughts that are going to lead your mind to

104
00:04:45,250 --> 00:04:47,120
 wander. Often it's the

105
00:04:47,120 --> 00:04:49,470
 case that we'll start to remember things that we're worried

106
00:04:49,470 --> 00:04:50,320
 about or concerned

107
00:04:50,320 --> 00:04:53,370
 about and that will lead our minds to wander and speculate

108
00:04:53,370 --> 00:04:54,440
 and eventually get

109
00:04:54,440 --> 00:05:01,760
 tired and fall asleep. So the Buddha said be careful not to

110
00:05:01,760 --> 00:05:02,160
 give those

111
00:05:02,160 --> 00:05:05,740
 sorts of thoughts any ground, whatever thought it was that

112
00:05:05,740 --> 00:05:06,600
 was causing you to

113
00:05:06,600 --> 00:05:16,190
 feel tired to avoid that thought and to not develop that

114
00:05:16,190 --> 00:05:17,400
 train of thought or

115
00:05:17,400 --> 00:05:22,060
 state of mind. Of course the best way to do this is to come

116
00:05:22,060 --> 00:05:22,520
 back to your

117
00:05:22,520 --> 00:05:26,500
 meditation technique and especially to deal with as the

118
00:05:26,500 --> 00:05:27,880
 Buddha said the

119
00:05:27,880 --> 00:05:33,260
 drowsiness as itself instead of allowing the thoughts to

120
00:05:33,260 --> 00:05:34,320
 build the drowsiness

121
00:05:34,320 --> 00:05:37,670
 focus on the drowsiness and look at it. When you do that

122
00:05:37,670 --> 00:05:38,600
 you're letting go

123
00:05:38,600 --> 00:05:41,590
 of the cause of the drowsiness and the drowsiness will

124
00:05:41,590 --> 00:05:43,800
 disappear by itself. Also

125
00:05:43,800 --> 00:05:50,260
 the attention and the alert awareness of the drowsiness is

126
00:05:50,260 --> 00:05:50,720
 the

127
00:05:50,720 --> 00:05:54,160
 opposite and you'll find that the drowsiness as you watch

128
00:05:54,160 --> 00:05:54,800
 it because of the

129
00:05:54,800 --> 00:05:57,870
 change in the mind state the drowsiness itself goes away so

130
00:05:57,870 --> 00:05:59,160
 you can try to focus

131
00:05:59,160 --> 00:06:03,340
 on the drowsiness itself which is is quite useful but most

132
00:06:03,340 --> 00:06:03,880
 important is to

133
00:06:03,880 --> 00:06:07,820
 avoid the thoughts that were causing you to become drowsing

134
00:06:07,820 --> 00:06:10,720
. The second way is to

135
00:06:10,720 --> 00:06:15,330
 if this practical method doesn't work of reverting back to

136
00:06:15,330 --> 00:06:16,160
 the practice and

137
00:06:16,160 --> 00:06:20,680
 adjusting your practice you can go back to the teachings

138
00:06:20,680 --> 00:06:23,080
 that the Buddha taught

139
00:06:23,080 --> 00:06:27,130
 or that your teacher gave us on and to go over them in your

140
00:06:27,130 --> 00:06:30,120
 mind to think about

141
00:06:30,120 --> 00:06:32,970
 for instance the four foundations of mindfulness the body

142
00:06:32,970 --> 00:06:33,880
 the feelings the

143
00:06:33,880 --> 00:06:39,920
 mind the various dhammas of the hindrances of the emotions

144
00:06:39,920 --> 00:06:40,320
 or so on

145
00:06:40,320 --> 00:06:43,230
 focus on what it was that the Buddha taught or what it is

146
00:06:43,230 --> 00:06:43,800
 that your

147
00:06:43,800 --> 00:06:47,710
 meditation teacher taught and go over them also the body

148
00:06:47,710 --> 00:06:48,960
 how do we focus on

149
00:06:48,960 --> 00:06:55,160
 the body think about maybe ways that you are lacking and

150
00:06:55,160 --> 00:06:56,160
 things that you are

151
00:06:56,160 --> 00:06:59,570
 missing in terms of awareness of the body and are you

152
00:06:59,570 --> 00:07:00,920
 actually able to watch

153
00:07:00,920 --> 00:07:03,930
 the movements of the stomach or be aware of the sitting

154
00:07:03,930 --> 00:07:05,880
 position or the breath or

155
00:07:05,880 --> 00:07:09,440
 whatever is your object are you actually aware of the

156
00:07:09,440 --> 00:07:10,760
 feelings when there's a

157
00:07:10,760 --> 00:07:13,890
 painful feeling are you actually paying attention to it for

158
00:07:13,890 --> 00:07:14,640
 instance saying to

159
00:07:14,640 --> 00:07:18,420
 yourself pain pain or if you feel happy or calm are you

160
00:07:18,420 --> 00:07:19,560
 actually paying attention

161
00:07:19,560 --> 00:07:24,050
 or are you letting it drag you down into a state of letharg

162
00:07:24,050 --> 00:07:25,600
y in the state of

163
00:07:25,600 --> 00:07:31,300
 fatigue and drowsiness and the same with the mind and the

164
00:07:31,300 --> 00:07:33,160
 mind of just so refer

165
00:07:33,160 --> 00:07:36,150
 back to the teachings and think about them in your mind

166
00:07:36,150 --> 00:07:38,600
 examine them and and

167
00:07:38,600 --> 00:07:42,100
 relate them back to your practice and and compare your

168
00:07:42,100 --> 00:07:43,180
 practice to the

169
00:07:43,180 --> 00:07:47,600
 teachings and that if you do that first of all just

170
00:07:47,600 --> 00:07:48,720
 thinking about these good

171
00:07:48,720 --> 00:07:51,640
 things will will wake you up and remind you of what you

172
00:07:51,640 --> 00:07:53,480
 should be doing and

173
00:07:53,480 --> 00:07:57,810
 second of all it will allow you to adjust your practice the

174
00:07:57,810 --> 00:07:58,760
 third way is if

175
00:07:58,760 --> 00:08:01,240
 that doesn't work or another way to deal with drowsiness is

176
00:08:01,240 --> 00:08:02,160
 to actually recite

177
00:08:02,160 --> 00:08:07,060
 the teachings and I know from experience that this is quite

178
00:08:07,060 --> 00:08:08,680
 useful for example

179
00:08:08,680 --> 00:08:13,930
 when you're driving I remember when I would be driving

180
00:08:13,930 --> 00:08:15,560
 somewhere at night and

181
00:08:15,560 --> 00:08:19,790
 trying to be mindful watching the road and steering turning

182
00:08:19,790 --> 00:08:21,600
 seeing and emotions

183
00:08:21,600 --> 00:08:26,110
 that arise and so on that I would find myself drifting

184
00:08:26,110 --> 00:08:27,480
 because sometimes there

185
00:08:27,480 --> 00:08:30,550
 would be excess concentration and not enough effort and

186
00:08:30,550 --> 00:08:31,800
 maybe falling asleep

187
00:08:31,800 --> 00:08:34,900
 when that happened I would actually recite the Buddha's

188
00:08:34,900 --> 00:08:36,040
 teachings and we do

189
00:08:36,040 --> 00:08:41,340
 this chant chance that we have of the Buddha's teachings we

190
00:08:41,340 --> 00:08:42,720
 actually recite

191
00:08:42,720 --> 00:08:45,610
 them so there's not them it's not really an intellectual

192
00:08:45,610 --> 00:08:46,640
 exercise but it's

193
00:08:46,640 --> 00:08:50,560
 something akin to singing songs when people turn on the

194
00:08:50,560 --> 00:08:51,960
 radio and sing along

195
00:08:51,960 --> 00:08:55,580
 when they're driving late at night it's something that will

196
00:08:55,580 --> 00:08:57,360
 wake you up but

197
00:08:57,360 --> 00:08:59,240
 also of course because it's the Buddha's teachings it's

198
00:08:59,240 --> 00:08:59,940
 something that will

199
00:08:59,940 --> 00:09:04,440
 invigorate you and give you effort and and give you the

200
00:09:04,440 --> 00:09:06,480
 encouragement that you

201
00:09:06,480 --> 00:09:09,070
 might need thinking about the Buddha thinking about his

202
00:09:09,070 --> 00:09:10,080
 teachings thinking

203
00:09:10,080 --> 00:09:16,660
 about the meditation practice and so on if that still doesn

204
00:09:16,660 --> 00:09:18,360
't work we the the

205
00:09:18,360 --> 00:09:22,520
 fourth method the Buddha said is to start to get physical

206
00:09:22,520 --> 00:09:23,680
 and the Buddha said

207
00:09:23,680 --> 00:09:28,110
 you pull your ears technique of waking you up kind of

208
00:09:28,110 --> 00:09:29,520
 stretching your cranium

209
00:09:29,520 --> 00:09:33,570
 rub your arms rub your body massage yourself and maybe

210
00:09:33,570 --> 00:09:35,280
 stretch a little bit

211
00:09:35,280 --> 00:09:40,960
 to kind of wake you up to get the blood flowing and to give

212
00:09:40,960 --> 00:09:41,520
 yourself a little

213
00:09:41,520 --> 00:09:44,200
 bit of physical energy because that might be the cause your

214
00:09:44,200 --> 00:09:45,360
 body might be

215
00:09:45,360 --> 00:09:51,150
 tired or so on your body might be stiff for instance some

216
00:09:51,150 --> 00:09:52,160
 people might even go

217
00:09:52,160 --> 00:09:55,040
 so far as to practice yoga I think it could be a very

218
00:09:55,040 --> 00:09:56,320
 useful technique to

219
00:09:56,320 --> 00:09:59,080
 waking you up because it's a different technique and

220
00:09:59,080 --> 00:10:01,240
 actually might lead in a

221
00:10:01,240 --> 00:10:04,470
 different direction I wouldn't recommend extensive practice

222
00:10:04,470 --> 00:10:05,760
 of yoga in

223
00:10:05,760 --> 00:10:11,200
 combination with insight meditation but there's certainly

224
00:10:11,200 --> 00:10:12,640
 nothing harmful in

225
00:10:12,640 --> 00:10:19,010
 in practicing it and and certainly in moderation for the

226
00:10:19,010 --> 00:10:20,720
 purposes of building

227
00:10:20,720 --> 00:10:25,420
 the energy necessary to practice meditation but at any rate

228
00:10:25,420 --> 00:10:26,240
 the Buddha

229
00:10:26,240 --> 00:10:31,640
 said massaging and rubbing and pulling your ears and so on

230
00:10:31,640 --> 00:10:33,280
 if that doesn't work

231
00:10:33,280 --> 00:10:36,660
 another way to deal with drowsiness the Buddha said is to

232
00:10:36,660 --> 00:10:38,320
 actually stand up and

233
00:10:38,320 --> 00:10:42,900
 go get some water pour water on your face rub water into

234
00:10:42,900 --> 00:10:44,680
 your eyes and look to

235
00:10:44,680 --> 00:10:47,480
 all directions the Buddha said you know take a look around

236
00:10:47,480 --> 00:10:48,400
 this is a way of kind

237
00:10:48,400 --> 00:10:52,440
 of waking you up and stimulating your mind to sort of get

238
00:10:52,440 --> 00:10:53,680
 rid of this heavy

239
00:10:53,680 --> 00:10:57,520
 state of concentration look in all directions look all

240
00:10:57,520 --> 00:10:58,360
 around you go and

241
00:10:58,360 --> 00:11:02,160
 look around see what's going on look up at the stars he

242
00:11:02,160 --> 00:11:03,960
 said usually drowsiness

243
00:11:03,960 --> 00:11:07,330
 comes at night so go and look up at the sky look up at the

244
00:11:07,330 --> 00:11:08,480
 stars look up at the

245
00:11:08,480 --> 00:11:13,000
 constellations is a way of breaking up this heavy

246
00:11:13,000 --> 00:11:14,480
 concentration giving yourself

247
00:11:14,480 --> 00:11:18,590
 more flexible state of mind to break to throw off the leth

248
00:11:18,590 --> 00:11:20,480
argy the drowsiness

249
00:11:20,480 --> 00:11:24,090
 he said if you do that then it's possible that the drows

250
00:11:24,090 --> 00:11:25,320
iness will will

251
00:11:25,320 --> 00:11:29,240
 disappear and you'll be able to continue with your practice

252
00:11:29,240 --> 00:11:30,880
 you can do walking

253
00:11:30,880 --> 00:11:34,030
 meditation the Buddha said this is a part of this is to

254
00:11:34,030 --> 00:11:35,040
 switch to doing

255
00:11:35,040 --> 00:11:38,560
 walking meditation that's one of the reasons for walking

256
00:11:38,560 --> 00:11:39,960
 meditation as well

257
00:11:39,960 --> 00:11:43,640
 it allows you to develop effort and energy if you sit all

258
00:11:43,640 --> 00:11:44,840
 the time it's easy

259
00:11:44,840 --> 00:11:47,580
 to fall asleep if you do walking as well you'll find that

260
00:11:47,580 --> 00:11:48,680
 when you do the sitting

261
00:11:48,680 --> 00:11:53,690
 you you feel charged afterwards so this is the the fifth

262
00:11:53,690 --> 00:11:55,880
 method the sixth method

263
00:11:55,880 --> 00:12:00,710
 is a specific meditation technique and it's probably not

264
00:12:00,710 --> 00:12:01,640
 applicable for most

265
00:12:01,640 --> 00:12:05,400
 beginner meditators but if you've been practicing

266
00:12:05,400 --> 00:12:07,400
 meditation for a while and I

267
00:12:07,400 --> 00:12:10,890
 suppose even even as a beginner you could attempt it the

268
00:12:10,890 --> 00:12:12,320
 technique is to

269
00:12:12,320 --> 00:12:18,980
 think of day as night to resolve on the meditation or the

270
00:12:18,980 --> 00:12:20,480
 awareness of light

271
00:12:20,480 --> 00:12:24,120
 even though it's dark at night because of course night is

272
00:12:24,120 --> 00:12:25,360
 the darkness is

273
00:12:25,360 --> 00:12:28,260
 something that triggers our recollection of oh now it's

274
00:12:28,260 --> 00:12:28,840
 time to fall

275
00:12:28,840 --> 00:12:32,550
 asleep and causes us to start to feel drowsy our mind

276
00:12:32,550 --> 00:12:34,520
 triggers the drowsiness

277
00:12:34,520 --> 00:12:38,080
 routine and based on the fact that it's dark so you trick

278
00:12:38,080 --> 00:12:39,120
 yourself you tell

279
00:12:39,120 --> 00:12:42,960
 yourself that it's light or you you envision you can close

280
00:12:42,960 --> 00:12:43,960
 with your eyes

281
00:12:43,960 --> 00:12:47,980
 closed you can imagine light or become aware of light I

282
00:12:47,980 --> 00:12:49,080
 know there are monks

283
00:12:49,080 --> 00:12:51,850
 who told me that you should do meditation with a bright

284
00:12:51,850 --> 00:12:53,320
 light on and I

285
00:12:53,320 --> 00:12:56,460
 suppose this could help as well I've tried it I don't it

286
00:12:56,460 --> 00:12:57,520
 doesn't seem that

287
00:12:57,520 --> 00:13:03,470
 useful but I suppose it could be at least to some extent

288
00:13:03,470 --> 00:13:05,080
 but here the point

289
00:13:05,080 --> 00:13:07,740
 is to actually get it in your mind that it's light that

290
00:13:07,740 --> 00:13:09,120
 there is light because

291
00:13:09,120 --> 00:13:12,790
 that will trigger the energy in the mind it's a way of

292
00:13:12,790 --> 00:13:14,920
 stimulating energy he said

293
00:13:14,920 --> 00:13:17,980
 thinking of though it's night to think of it as day and to

294
00:13:17,980 --> 00:13:19,680
 get an idea in your

295
00:13:19,680 --> 00:13:22,460
 mind if you're actually been practicing meditation for a

296
00:13:22,460 --> 00:13:24,080
 while you can actually

297
00:13:24,080 --> 00:13:28,430
 begin to envision envision bright lights some people even

298
00:13:28,430 --> 00:13:29,840
 get very distracted by

299
00:13:29,840 --> 00:13:33,420
 these lights or colors or pictures or so on and we have to

300
00:13:33,420 --> 00:13:34,760
 remind meditators not

301
00:13:34,760 --> 00:13:38,480
 to get off track and to acknowledge them as seeing seeing

302
00:13:38,480 --> 00:13:39,720
 to just remind yourself

303
00:13:39,720 --> 00:13:43,480
 this is a visual stimulus it's not a magical sensation

304
00:13:43,480 --> 00:13:44,720
 magical experience or

305
00:13:44,720 --> 00:13:48,360
 certainly not the path that leads to enlightenment it's the

306
00:13:48,360 --> 00:13:50,240
 wrong path but in

307
00:13:50,240 --> 00:13:53,430
 this case it has a limited benefit of bringing about energy

308
00:13:53,430 --> 00:13:54,400
 and effort it has

309
00:13:54,400 --> 00:13:59,830
 its use even though it's not the path and that's the sixth

310
00:13:59,830 --> 00:14:01,320
 method if that

311
00:14:01,320 --> 00:14:05,160
 doesn't work the Buddha said lie down and the Buddha's way

312
00:14:05,160 --> 00:14:06,240
 of lying down was

313
00:14:06,240 --> 00:14:09,370
 to lie down on what on your side not in your stomach not on

314
00:14:09,370 --> 00:14:10,160
 your back with a

315
00:14:10,160 --> 00:14:14,130
 light on on one side and I'm not sure you can prop your

316
00:14:14,130 --> 00:14:15,760
 head up or you I don't

317
00:14:15,760 --> 00:14:18,260
 think that's mentioned but the way I would often do it is

318
00:14:18,260 --> 00:14:19,200
 to actually prop my

319
00:14:19,200 --> 00:14:24,040
 head up on my elbow and it's a technique that I've I've

320
00:14:24,040 --> 00:14:25,320
 seen other people use and

321
00:14:25,320 --> 00:14:29,760
 monks use and it seems to very much keep you awake if you

322
00:14:29,760 --> 00:14:31,520
 start to fall asleep

323
00:14:31,520 --> 00:14:35,610
 your head starts to fall off your your arm and you wake up

324
00:14:35,610 --> 00:14:36,840
 quite well it's also

325
00:14:36,840 --> 00:14:39,570
 quite painful in the beginning it's something you have to

326
00:14:39,570 --> 00:14:41,160
 develop it's a

327
00:14:41,160 --> 00:14:44,030
 physical technique that you have to develop just like

328
00:14:44,030 --> 00:14:45,280
 sitting cross-legged

329
00:14:45,280 --> 00:14:49,470
 but at any rate lie down on one side and perhaps not pro

330
00:14:49,470 --> 00:14:51,000
pping the head up but at

331
00:14:51,000 --> 00:14:55,120
 at the very least resolving on getting up the Buddha said

332
00:14:55,120 --> 00:14:55,960
 you're lying down and

333
00:14:55,960 --> 00:14:59,080
 you know you know you kind of given up at this point

334
00:14:59,080 --> 00:15:00,520
 because you're feeling like

335
00:15:00,520 --> 00:15:03,870
 you can't overcome the drowsiness so you're going to accept

336
00:15:03,870 --> 00:15:04,800
 that you might

337
00:15:04,800 --> 00:15:09,640
 fall asleep so you lie down and you say I'm going to get up

338
00:15:09,640 --> 00:15:10,800
 in so many minutes

339
00:15:10,800 --> 00:15:13,720
 or so many hours if it's time to sleep you say I'm gonna

340
00:15:13,720 --> 00:15:14,840
 sleep for four hours

341
00:15:14,840 --> 00:15:18,700
 or however long you're going to sleep and I'm going to get

342
00:15:18,700 --> 00:15:19,800
 up at such and such a

343
00:15:19,800 --> 00:15:23,920
 time when you resolve think about before you go to sleep

344
00:15:23,920 --> 00:15:25,520
 think only about that the

345
00:15:25,520 --> 00:15:28,470
 fact that you're going to get up so that when that time

346
00:15:28,470 --> 00:15:30,960
 comes you'll find that if

347
00:15:30,960 --> 00:15:35,090
 you resolve in this way you're actually your mind is able

348
00:15:35,090 --> 00:15:38,360
 to somehow wake you up

349
00:15:38,360 --> 00:15:42,970
 at that time it's amazing how how advanced the mind really

350
00:15:42,970 --> 00:15:44,120
 is that you'll

351
00:15:44,120 --> 00:15:47,240
 find yourself waking up a few minutes before the hour that

352
00:15:47,240 --> 00:15:48,640
 you were going to

353
00:15:48,640 --> 00:15:52,830
 wake up or how many minutes you were going to sleep or so

354
00:15:52,830 --> 00:15:54,600
 on and then the

355
00:15:54,600 --> 00:15:57,890
 Buddha said and then get up he said resolve in your mind I

356
00:15:57,890 --> 00:15:59,560
 will not give

357
00:15:59,560 --> 00:16:02,600
 into drowsiness and I will not become attached to the

358
00:16:02,600 --> 00:16:03,880
 pleasure that comes from

359
00:16:03,880 --> 00:16:08,620
 lying down because one of the things that we miss one of

360
00:16:08,620 --> 00:16:10,480
 the great addictions

361
00:16:10,480 --> 00:16:14,740
 that we miss is the addiction to sleep many people like to

362
00:16:14,740 --> 00:16:16,080
 lie down for many

363
00:16:16,080 --> 00:16:20,600
 many hours and sleep a lot and it's because of the physical

364
00:16:20,600 --> 00:16:21,760
 pleasure that

365
00:16:21,760 --> 00:16:24,790
 that comes from it but the problem with this physical

366
00:16:24,790 --> 00:16:25,920
 pleasure is like all

367
00:16:25,920 --> 00:16:29,340
 physical pleasures it's not permanent it's not lasting and

368
00:16:29,340 --> 00:16:30,720
 it's not the case

369
00:16:30,720 --> 00:16:33,690
 that the more you sleep the more happy you feel you in fact

370
00:16:33,690 --> 00:16:35,240
 feel more depressed

371
00:16:35,240 --> 00:16:41,760
 and more lethargic and less energetic less bright and and

372
00:16:41,760 --> 00:16:43,360
 and you know at

373
00:16:43,360 --> 00:16:48,440
 peace with yourself you feel more distracted less awake and

374
00:16:48,440 --> 00:16:50,880
 less alert so

375
00:16:50,880 --> 00:16:55,150
 you know the Buddha said this is this last one you have to

376
00:16:55,150 --> 00:16:56,960
 you have to make it

377
00:16:56,960 --> 00:16:59,850
 you can't just lie down and say I'm going to go to sleep

378
00:16:59,850 --> 00:17:01,440
 and fall asleep you have

379
00:17:01,440 --> 00:17:04,860
 to be strict about it that I'm going to set myself how long

380
00:17:04,860 --> 00:17:06,280
 I'm going to sleep

381
00:17:06,280 --> 00:17:09,850
 and then when I wake up I'm going to get up and go on with

382
00:17:09,850 --> 00:17:11,520
 my practice the Buddha

383
00:17:11,520 --> 00:17:14,150
 said this is the way that a person should deal with drows

384
00:17:14,150 --> 00:17:15,800
iness so rather

385
00:17:15,800 --> 00:17:18,770
 than give my own thoughts on it or something that's vaguely

386
00:17:18,770 --> 00:17:19,440
 based on the

387
00:17:19,440 --> 00:17:23,440
 Buddha's teaching there you have directly as best as I

388
00:17:23,440 --> 00:17:27,240
 could translate

389
00:17:27,240 --> 00:17:33,060
 and paraphrase and expand upon it the Buddha's teaching on

390
00:17:33,060 --> 00:17:34,320
 how to deal with

391
00:17:34,320 --> 00:17:38,920
 drowsiness so thanks for the question hope that helped

