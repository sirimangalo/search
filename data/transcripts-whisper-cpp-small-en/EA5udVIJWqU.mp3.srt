1
00:00:00,000 --> 00:00:03,920
 OK, good evening, everyone.

2
00:00:03,920 --> 00:00:15,000
 I'm broadcasting live December 1st, 2015.

3
00:00:15,000 --> 00:00:18,800
 So I had two people come up to me this morning

4
00:00:18,800 --> 00:00:24,020
 after all this changing the color of my robe and everything

5
00:00:24,020 --> 00:00:24,280
.

6
00:00:24,280 --> 00:00:28,100
 See, I think before the other color just looked like a

7
00:00:28,100 --> 00:00:28,480
 stunt.

8
00:00:28,480 --> 00:00:32,400
 And now that I've changed to a more reasonable color,

9
00:00:32,400 --> 00:00:36,080
 people are more comfortable coming up and talking to me.

10
00:00:36,080 --> 00:00:37,520
 It was really funny.

11
00:00:37,520 --> 00:00:42,000
 This man, I'm walking in the student center.

12
00:00:42,000 --> 00:00:46,800
 And suddenly there's a hey or hi or something like that.

13
00:00:46,800 --> 00:00:48,440
 And I turn, and sure enough, there's

14
00:00:48,440 --> 00:00:52,280
 someone walking with me.

15
00:00:52,280 --> 00:00:57,560
 And then he just says, you can't dress like that

16
00:00:57,560 --> 00:01:01,880
 and not expect people to come up and ask you questions.

17
00:01:01,880 --> 00:01:02,960
 And I said, why not?

18
00:01:02,960 --> 00:01:08,520
 Why not?

19
00:01:08,520 --> 00:01:11,760
 Leave me alone.

20
00:01:11,760 --> 00:01:15,920
 But he stopped and tried to get me

21
00:01:15,920 --> 00:01:20,520
 to explain to him Buddhism in the middle of this crowded

22
00:01:20,520 --> 00:01:21,040
 place.

23
00:01:21,040 --> 00:01:25,400
 And I said, look, it's not something

24
00:01:25,400 --> 00:01:28,640
 I can easily tell you in this short time.

25
00:01:28,640 --> 00:01:31,080
 But I gave him my booklet on how to meditate.

26
00:01:31,080 --> 00:01:33,480
 And just as I was handing him the booklet,

27
00:01:33,480 --> 00:01:38,160
 this woman comes up and starts talking to me.

28
00:01:38,160 --> 00:01:39,840
 But she was fairly abrupt.

29
00:01:39,840 --> 00:01:46,360
 She said, I thought you were a Hindu monk.

30
00:01:46,360 --> 00:01:47,680
 I'm a practicing Hindu.

31
00:01:47,680 --> 00:01:50,920
 And if you were a Hindu monk, then you just

32
00:01:50,920 --> 00:01:52,120
 circle left it hanging like--

33
00:01:56,480 --> 00:02:00,840
 if you were one of us, I wouldn't be interested.

34
00:02:00,840 --> 00:02:02,440
 Somebody has said to that, right?

35
00:02:02,440 --> 00:02:04,760
 I just said, OK, I'm going to go to class now.

36
00:02:04,760 --> 00:02:05,280
 Bye.

37
00:02:05,280 --> 00:02:14,800
 But I'm slowly converting the smoker section--

38
00:02:14,800 --> 00:02:16,520
 actually, not the smoker section.

39
00:02:16,520 --> 00:02:19,440
 There's friends of friends of friends.

40
00:02:19,440 --> 00:02:21,760
 So Katya is in my class.

41
00:02:21,760 --> 00:02:24,960
 And she started practicing meditation.

42
00:02:24,960 --> 00:02:28,000
 And then her friend Sarah joined us.

43
00:02:28,000 --> 00:02:29,600
 And I taught her how to meditate.

44
00:02:29,600 --> 00:02:35,280
 And then Sarah's friend Natalie joined us.

45
00:02:35,280 --> 00:02:38,320
 So now I've got three this afternoon.

46
00:02:38,320 --> 00:02:42,440
 The three of us-- the four of us meditated together.

47
00:02:42,440 --> 00:02:44,280
 So I'm teaching meditation slowly.

48
00:02:44,280 --> 00:02:48,720
 And then this person in my Latin class came up to me.

49
00:02:48,720 --> 00:02:50,360
 I think she's in my Latin class.

50
00:02:50,360 --> 00:02:53,000
 I knew that it was one of the visiting high school students

51
00:02:53,000 --> 00:02:53,000
.

52
00:02:53,000 --> 00:02:57,400
 Because we had some high school students visiting.

53
00:02:57,400 --> 00:02:59,160
 It was the first time someone came up to me.

54
00:02:59,160 --> 00:03:03,480
 So she said, hey, what's your name?

55
00:03:03,480 --> 00:03:05,760
 My name's Yutto Damo.

56
00:03:05,760 --> 00:03:06,960
 You do.

57
00:03:06,960 --> 00:03:11,080
 You have some videos on YouTube.

58
00:03:11,080 --> 00:03:13,160
 That's why I gave her my card.

59
00:03:13,160 --> 00:03:15,040
 She wants to learn how to meditate.

60
00:03:15,040 --> 00:03:17,520
 [END PLAYBACK]

61
00:03:17,520 --> 00:03:26,320
 What else?

62
00:03:26,320 --> 00:03:34,920
 I guess that's it.

63
00:03:34,920 --> 00:03:41,000
 Oh, the thing in Florida, we just talked about that.

64
00:03:41,000 --> 00:03:46,480
 So we'll talk about that later, I guess.

65
00:03:46,480 --> 00:03:48,760
 We'll announce it.

66
00:03:48,760 --> 00:03:58,880
 So we're going to give gifts to orphans, I think,

67
00:03:58,880 --> 00:04:01,120
 or children who are not as the orphans,

68
00:04:01,120 --> 00:04:05,160
 but foster children or children who are in the home.

69
00:04:05,160 --> 00:04:06,920
 Homeless kids, I guess, maybe.

70
00:04:09,680 --> 00:04:13,200
 So this charity that looks after kids, and not just kids,

71
00:04:13,200 --> 00:04:14,880
 but looks after people who are in trouble.

72
00:04:14,880 --> 00:04:16,840
 So if you're looking for a charity

73
00:04:16,840 --> 00:04:21,200
 to give to this winter, or if you'd just

74
00:04:21,200 --> 00:04:25,640
 like to do a good deed, Robin's going

75
00:04:25,640 --> 00:04:28,080
 to help set up a campaign.

76
00:04:28,080 --> 00:04:30,360
 But what she said, she suggested what we do

77
00:04:30,360 --> 00:04:33,120
 is we combine it with the book campaign.

78
00:04:33,120 --> 00:04:36,440
 So Robin, maybe you want to talk about that for a bit.

79
00:04:36,440 --> 00:04:37,600
 Sure.

80
00:04:37,600 --> 00:04:40,760
 We've been talking about different things

81
00:04:40,760 --> 00:04:43,400
 that would be nice to do, especially this time of year.

82
00:04:43,400 --> 00:04:46,560
 And there's an opportunity to have some of the How

83
00:04:46,560 --> 00:04:51,080
 to Meditate books reprinted, 1,000 more copies reprinted,

84
00:04:51,080 --> 00:04:55,840
 which is great, a great way to spread that information.

85
00:04:55,840 --> 00:04:59,280
 So we'd like to set up a campaign for anyone

86
00:04:59,280 --> 00:05:00,880
 who would like to donate towards this,

87
00:05:00,880 --> 00:05:04,120
 because all the reprinting is done by donation.

88
00:05:04,120 --> 00:05:07,640
 And we'd also like to, as Bante was saying,

89
00:05:07,640 --> 00:05:09,680
 support the children's home in Tampa,

90
00:05:09,680 --> 00:05:14,320
 Florida with doing something nice for the resident children

91
00:05:14,320 --> 00:05:15,920
 at this holiday season.

92
00:05:15,920 --> 00:05:19,400
 So I'll have all of that ready tomorrow

93
00:05:19,400 --> 00:05:20,960
 for anyone who would like to participate

94
00:05:20,960 --> 00:05:23,320
 in both of those programs.

95
00:05:23,320 --> 00:05:25,720
 It would be one campaign for the two.

96
00:05:25,720 --> 00:05:27,800
 The first part would be for what is

97
00:05:27,800 --> 00:05:29,600
 needed for the printing of the books.

98
00:05:29,600 --> 00:05:31,800
 And anything left over after the printing of the books

99
00:05:31,800 --> 00:05:34,450
 will be for the benefit of the children at the children's

100
00:05:34,450 --> 00:05:34,560
 home

101
00:05:34,560 --> 00:05:36,640
 in Tampa, Florida.

102
00:05:36,640 --> 00:05:39,440
 Yeah, it's just that I'll be in Florida.

103
00:05:39,440 --> 00:05:40,840
 And I really want to do something

104
00:05:40,840 --> 00:05:43,320
 on behalf of my family.

105
00:05:43,320 --> 00:05:45,200
 So that's my point.

106
00:05:45,200 --> 00:05:51,440
 But then I thought, well, that'd be a neat thing for other

107
00:05:51,440 --> 00:05:51,720
 people

108
00:05:51,720 --> 00:05:52,220
 to do.

109
00:05:52,220 --> 00:05:54,040
 If you want to do something on behalf of someone

110
00:05:54,040 --> 00:05:58,600
 this holiday season, you can give a gift

111
00:05:58,600 --> 00:06:00,440
 on behalf of someone you know.

112
00:06:00,440 --> 00:06:02,760
 Instead of giving gifts to each other, you can say, well,

113
00:06:02,760 --> 00:06:05,600
 our family, we're going to do something good for others.

114
00:06:05,600 --> 00:06:07,880
 So the money that we're going to spend on gifts for each

115
00:06:07,880 --> 00:06:08,320
 other,

116
00:06:08,320 --> 00:06:12,480
 we're going to spend on gifts for homeless kids.

117
00:06:12,480 --> 00:06:14,320
 And then you've given each other a gift.

118
00:06:14,320 --> 00:06:16,520
 You've done something good on each other's name

119
00:06:16,520 --> 00:06:18,920
 and made each other feel happy, and so on.

120
00:06:18,920 --> 00:06:21,440
 So take part in this.

121
00:06:21,440 --> 00:06:24,400
 Rather than go looking for some charity

122
00:06:24,400 --> 00:06:28,100
 and do it together with us, and when we pool our resources,

123
00:06:28,100 --> 00:06:30,400
 we can really make a difference.

124
00:06:30,400 --> 00:06:32,440
 Definitely.

125
00:06:32,440 --> 00:06:33,800
 We have a great community.

126
00:06:33,800 --> 00:06:40,280
 So understand.

127
00:06:40,280 --> 00:06:47,360
 There's probably more I can remember.

128
00:06:47,360 --> 00:06:54,240
 Got a question?

129
00:06:54,240 --> 00:06:56,720
 We have no questions.

130
00:06:56,720 --> 00:06:58,120
 I have a question, though.

131
00:06:58,120 --> 00:07:00,640
 We didn't post the link to the Hangout.

132
00:07:00,640 --> 00:07:02,360
 Do you see the link to the Hangout?

133
00:07:02,360 --> 00:07:04,760
 No, that's only on your screen.

134
00:07:04,760 --> 00:07:09,280
 Here's the link.

135
00:07:09,280 --> 00:07:11,480
 If someone wants to come on the Hangout and say hello,

136
00:07:11,480 --> 00:07:13,560
 you can come live.

137
00:07:13,560 --> 00:07:16,920
 If you're audacious-- so my Latin teacher said "audax."

138
00:07:16,920 --> 00:07:24,800
 If you're brave.

139
00:07:24,800 --> 00:07:31,760
 [AUDIO OUT]

140
00:07:31,760 --> 00:07:34,200
 Oh, I had a question, Bante.

141
00:07:34,200 --> 00:07:38,560
 We talked a little bit about you potentially restarting

142
00:07:38,560 --> 00:07:43,040
 the Buddhism 101 series.

143
00:07:43,040 --> 00:07:45,640
 Have you given any thought to that?

144
00:07:45,640 --> 00:07:48,080
 Well, yeah.

145
00:07:48,080 --> 00:07:49,600
 If I get more time--

146
00:07:49,600 --> 00:07:50,760
 I mean, I can do it without.

147
00:07:50,760 --> 00:07:52,600
 I don't need-- it's not like I need more time

148
00:07:52,600 --> 00:07:55,720
 to just replace one of the Dhamma Planda with the Buddhism

149
00:07:55,720 --> 00:07:56,200
 101

150
00:07:56,200 --> 00:07:56,720
 video.

151
00:07:56,720 --> 00:08:00,440
 It does take a little bit of thought to get it straight,

152
00:08:00,440 --> 00:08:04,200
 but it's not that big of a deal.

153
00:08:04,200 --> 00:08:10,920
 So you can do that on, say, Wednesdays.

154
00:08:10,920 --> 00:08:11,880
 That would be great.

155
00:08:11,880 --> 00:08:18,600
 We can really expand it and just start

156
00:08:18,600 --> 00:08:21,880
 talking about different topics.

157
00:08:21,880 --> 00:08:22,380
 Yeah.

158
00:08:22,380 --> 00:08:29,560
 I would really like that.

159
00:08:29,560 --> 00:08:33,520
 I kind of miss studying the core Buddhist principles.

160
00:08:33,520 --> 00:08:35,120
 I studied a little bit, but--

161
00:08:35,120 --> 00:08:36,800
 I mean, when you're talking about core principles,

162
00:08:36,800 --> 00:08:38,240
 it's easy to start talking about it,

163
00:08:38,240 --> 00:08:40,280
 but it would be nice to spend a little bit of time

164
00:08:40,280 --> 00:08:42,760
 and make sure I got all the facts straight.

165
00:08:42,760 --> 00:08:47,120
 That does take a little bit of planning.

166
00:08:47,120 --> 00:08:48,000
 So there's a little bit--

167
00:08:48,000 --> 00:08:51,160
 I mean, the Dhamma Planda is one thing.

168
00:08:51,160 --> 00:08:52,640
 I mean, you have to read the story,

169
00:08:52,640 --> 00:08:58,440
 but the teaching isn't in depth.

170
00:08:58,440 --> 00:09:00,480
 I suppose it's not that big of a deal.

171
00:09:00,480 --> 00:09:01,960
 I mean, it's just basic concepts.

172
00:09:01,960 --> 00:09:04,680
 I know all the basic--

173
00:09:04,680 --> 00:09:06,880
 so I need to research, really.

174
00:09:06,880 --> 00:09:13,880
 But it might not last that long because I think

175
00:09:13,880 --> 00:09:16,640
 it's finding the next topic, trying to think, OK,

176
00:09:16,640 --> 00:09:20,800
 what should I teach next?

177
00:09:20,800 --> 00:09:23,680
 See, I can try that.

178
00:09:23,680 --> 00:09:25,720
 Maybe I'll try that tomorrow.

179
00:09:25,720 --> 00:09:27,600
 I do know what I need to do to finish it.

180
00:09:27,600 --> 00:09:31,040
 There were supposed to be five videos, maybe six

181
00:09:31,040 --> 00:09:32,320
 in the series.

182
00:09:32,320 --> 00:09:36,360
 So I know which one comes next.

183
00:09:36,360 --> 00:09:37,720
 I think-- actually, I don't know.

184
00:09:37,720 --> 00:09:41,640
 I don't remember where I ended, but I know the sequence.

185
00:09:41,640 --> 00:09:44,440
 I think I finished morality, right?

186
00:09:44,440 --> 00:09:46,280
 I can check on that.

187
00:09:46,280 --> 00:09:49,800
 The next one-- oh, I think it's a bit difficult.

188
00:09:49,800 --> 00:09:55,320
 See, I was planning on doing it based on the Anupupikata,

189
00:09:55,320 --> 00:09:59,040
 the Buddhist teaching in order.

190
00:09:59,040 --> 00:10:00,560
 And so the next one is supposed to be

191
00:10:00,560 --> 00:10:13,000
 about heaven, which really describes the benefits

192
00:10:13,000 --> 00:10:17,080
 of good deeds.

193
00:10:17,080 --> 00:10:19,760
 Well, so far, Banté, you have generosity, morality,

194
00:10:19,760 --> 00:10:23,360
 one morality, two morality, three and morality four.

195
00:10:23,360 --> 00:10:25,440
 Yeah, that's all about morality.

196
00:10:25,440 --> 00:10:28,520
 So the next will be-- it's supposed to be about heaven.

197
00:10:28,520 --> 00:10:32,960
 And I think that means in regards to the benefits.

198
00:10:32,960 --> 00:10:35,960
 It's referring to the benefits of wholesome karma.

199
00:10:35,960 --> 00:10:45,360
 So I have to do a whole video on heaven, which is--

200
00:10:45,360 --> 00:10:47,760
 how to make it actually about the basics of Buddhism,

201
00:10:47,760 --> 00:10:50,400
 because that's part of the Buddhist orderly teaching.

202
00:10:50,400 --> 00:10:51,520
 So what is meant by that?

203
00:10:51,520 --> 00:10:56,600
 Karma, I guess.

204
00:10:56,600 --> 00:10:57,920
 Really, that's what it's about.

205
00:10:57,920 --> 00:11:00,000
 So the next one should be about karma, I guess.

206
00:11:00,000 --> 00:11:05,680
 Yes, we will be.

207
00:11:05,680 --> 00:11:09,040
 The video-- of course, I already have videos on karma.

208
00:11:09,040 --> 00:11:12,200
 But I'll do a new one, basic, no?

209
00:11:12,200 --> 00:11:13,920
 Basic karma.

210
00:11:13,920 --> 00:11:15,760
 So then I have to think, how do I present it?

211
00:11:15,760 --> 00:11:17,160
 That's the other thing, you see.

212
00:11:17,160 --> 00:11:18,360
 I know the teaching.

213
00:11:18,360 --> 00:11:20,360
 How do I present this to people?

214
00:11:20,360 --> 00:11:21,920
 You have to consider the audience.

215
00:11:21,920 --> 00:11:24,680
 This is 101.

216
00:11:24,680 --> 00:11:27,840
 So let's take a little planning.

217
00:11:27,840 --> 00:11:28,640
 Sure.

218
00:11:28,640 --> 00:11:31,200
 And I've got exams coming up.

219
00:11:31,200 --> 00:11:32,680
 So is this something to start--

220
00:11:32,680 --> 00:11:35,080
 that's better started after the first of the year?

221
00:11:35,080 --> 00:11:39,640
 Maybe after the-- after my exams are over.

222
00:11:39,640 --> 00:11:42,960
 And then I go to Florida.

223
00:11:42,960 --> 00:11:46,400
 So maybe after the first of the year?

224
00:11:46,400 --> 00:11:48,640
 Yeah, except I might be going back to school.

225
00:11:48,640 --> 00:11:49,140
 We'll see.

226
00:11:49,140 --> 00:11:53,360
 I really shouldn't know.

227
00:11:53,360 --> 00:11:54,360
 I should just quit it.

228
00:11:54,360 --> 00:11:56,840
 [VIDEO PLAYBACK]

229
00:12:23,800 --> 00:12:26,280
 Would you do things like in the Buddhism 101,

230
00:12:26,280 --> 00:12:28,000
 would you do the Four Noble Truths

231
00:12:28,000 --> 00:12:30,280
 and the Eightfold Noble Path and all of those?

232
00:12:30,280 --> 00:12:30,780
 Yeah.

233
00:12:30,780 --> 00:12:33,840
 Yeah, I mean, it ends up with the Four Noble Truths.

234
00:12:33,840 --> 00:12:36,360
 That would be the sixth video.

235
00:12:36,360 --> 00:12:37,560
 OK.

236
00:12:37,560 --> 00:12:39,200
 So that's already in there.

237
00:12:39,200 --> 00:12:41,120
 And then in the Four Noble Truths,

238
00:12:41,120 --> 00:12:47,080
 you'd probably deal with each Noble Truth individually.

239
00:12:47,080 --> 00:12:49,520
 So you'd have at least four videos, probably.

240
00:12:49,520 --> 00:12:51,360
 Makes sense.

241
00:12:53,360 --> 00:12:57,280
 And so maybe the fourth video would be eight videos

242
00:12:57,280 --> 00:13:00,080
 or something.

243
00:13:00,080 --> 00:13:02,480
 You could expand it like that.

244
00:13:02,480 --> 00:13:04,080
 That'd be neat.

245
00:13:04,080 --> 00:13:06,400
 Turn it into a fractal.

246
00:13:06,400 --> 00:13:08,560
 [END PLAYBACK]

247
00:13:08,560 --> 00:13:11,520
 [AUDIO OUT]

248
00:13:11,520 --> 00:13:13,960
 [AUDIO OUT]

249
00:13:13,960 --> 00:13:16,400
 [AUDIO OUT]

250
00:13:16,400 --> 00:13:18,840
 [AUDIO OUT]

251
00:13:18,840 --> 00:13:21,280
 [AUDIO OUT]

252
00:13:21,280 --> 00:13:23,720
 [AUDIO OUT]

253
00:13:23,720 --> 00:13:26,160
 [AUDIO OUT]

254
00:13:26,160 --> 00:13:28,600
 [AUDIO OUT]

255
00:13:53,600 --> 00:13:56,600
 [AUDIO OUT]

256
00:13:56,600 --> 00:13:57,880
 It's like the middle of the night.

257
00:13:57,880 --> 00:14:02,200
 Sorry, Bandhi.

258
00:14:02,200 --> 00:14:03,720
 Your screen froze.

259
00:14:03,720 --> 00:14:06,010
 And I think we might have missed part of what you were

260
00:14:06,010 --> 00:14:06,600
 saying.

261
00:14:06,600 --> 00:14:07,440
 Oh, really?

262
00:14:07,440 --> 00:14:10,960
 Yeah, you were frozen for a bit there.

263
00:14:10,960 --> 00:14:13,720
 Maybe it was-- could have been you, though.

264
00:14:13,720 --> 00:14:15,480
 Could have been your connection.

265
00:14:15,480 --> 00:14:16,600
 Maybe.

266
00:14:16,600 --> 00:14:17,920
 Maybe it was mine.

267
00:14:17,920 --> 00:14:22,320
 My internet connection is pretty good to hear.

268
00:14:22,320 --> 00:14:23,120
 OK.

269
00:14:23,120 --> 00:14:25,840
 So I'm surprised to hear that.

270
00:14:25,840 --> 00:14:28,960
 Maybe it was just me.

271
00:14:28,960 --> 00:14:29,960
 Hello, Bandhi.

272
00:14:29,960 --> 00:14:31,440
 This is stupid, but I'm wondering

273
00:14:31,440 --> 00:14:33,720
 if you are familiar with Daniel Ingram

274
00:14:33,720 --> 00:14:35,880
 and mastering the core teachings of the Buddha,

275
00:14:35,880 --> 00:14:39,040
 and if so, your thoughts on his method.

276
00:14:39,040 --> 00:14:39,960
 Yeah, sorry.

277
00:14:39,960 --> 00:14:43,840
 Don't answer your thoughts on questions.

278
00:14:43,840 --> 00:14:44,760
 Not my teaching.

279
00:14:44,760 --> 00:14:48,240
 I don't have thoughts on it.

280
00:14:48,240 --> 00:14:49,080
 Safer that way.

281
00:14:55,160 --> 00:14:57,080
 I know, Bhamara.

282
00:14:57,080 --> 00:14:57,840
 One question.

283
00:14:57,840 --> 00:15:00,640
 Oh, that's Ann?

284
00:15:00,640 --> 00:15:02,320
 Yes.

285
00:15:02,320 --> 00:15:02,800
 Yeah.

286
00:15:02,800 --> 00:15:07,440
 Yeah, I guess, I mean, I should at least say

287
00:15:07,440 --> 00:15:08,880
 that that's not what I teach.

288
00:15:08,880 --> 00:15:12,520
 So he does talk about Mahasya Sayadaw's teaching,

289
00:15:12,520 --> 00:15:16,080
 but it's important to understand our tradition is

290
00:15:16,080 --> 00:15:18,360
 the Mahasya Sayadaw tradition.

291
00:15:18,360 --> 00:15:20,920
 So if you wanted to ask me something

292
00:15:20,920 --> 00:15:22,480
 about the Mahasya Sayadaw tradition,

293
00:15:22,480 --> 00:15:24,840
 I can talk about that.

294
00:15:24,840 --> 00:15:25,680
 Because that's us.

295
00:15:25,680 --> 00:15:34,160
 Should I learn about meditation first or about suttas first

296
00:15:34,160 --> 00:15:34,360
?

297
00:15:34,360 --> 00:15:43,840
 Meditation, I would say.

298
00:15:43,840 --> 00:15:46,600
 I mean, what were the suttas?

299
00:15:46,600 --> 00:15:51,920
 They were talks designed to encourage people in practice.

300
00:15:51,920 --> 00:15:56,080
 So you should read about how to practice.

301
00:15:56,080 --> 00:15:58,160
 Read something that allows you to practice.

302
00:15:58,160 --> 00:16:21,320
 They didn't have books where they studied.

303
00:16:21,320 --> 00:16:23,680
 No.

304
00:16:23,680 --> 00:16:27,120
 The monks did do studying, reciting, and listening

305
00:16:27,120 --> 00:16:28,640
 to talk after talk.

306
00:16:28,640 --> 00:16:30,960
 It was that they listened to many,

307
00:16:30,960 --> 00:16:33,000
 learned lots of different dhamma.

308
00:16:33,000 --> 00:16:36,480
 But it was more once they were into it.

309
00:16:36,480 --> 00:16:41,940
 So meditation should come first, learning about how to

310
00:16:41,940 --> 00:16:42,960
 practice.

311
00:16:42,960 --> 00:16:45,680
 That's also very important because your understanding

312
00:16:45,680 --> 00:16:48,560
 and your appreciation of the Buddha's teaching

313
00:16:48,560 --> 00:16:52,200
 changes dramatically once you start to practice.

314
00:16:52,200 --> 00:16:54,000
 If you haven't been practicing meditation,

315
00:16:54,000 --> 00:16:57,940
 it's often very difficult to understand and to even

316
00:16:57,940 --> 00:16:58,720
 appreciate

317
00:16:58,720 --> 00:17:00,640
 or have any interest in the Buddha's teaching.

318
00:17:00,640 --> 00:17:07,920
 Just as-- I don't know if it's fair to say,

319
00:17:07,920 --> 00:17:15,920
 but ordinary animals aren't able to appreciate things

320
00:17:15,920 --> 00:17:19,280
 like cell phone.

321
00:17:19,280 --> 00:17:23,160
 So there is a sense that there is that ordinary--

322
00:17:23,160 --> 00:17:27,890
 a person who's not meditating is missing some clarity of

323
00:17:27,890 --> 00:17:28,240
 mind

324
00:17:28,240 --> 00:17:31,800
 that allows them to see the value of things

325
00:17:31,800 --> 00:17:35,320
 beyond the material.

326
00:17:35,320 --> 00:17:41,160
 It's not quite fair, but most people--

327
00:17:41,160 --> 00:17:41,760
 I mean, I was.

328
00:17:41,760 --> 00:17:42,720
 I know I was.

329
00:17:42,720 --> 00:17:47,640
 I was unable to really see the value of so many things.

330
00:17:47,640 --> 00:17:52,080
 But when you meditate, it brings clarity.

331
00:17:52,080 --> 00:17:54,080
 And the Buddha's teaching was very interesting to me

332
00:17:54,080 --> 00:17:56,040
 after that.

333
00:17:56,040 --> 00:17:58,200
 I don't think it was something that I was--

334
00:17:58,200 --> 00:17:59,520
 that's all keen on.

335
00:17:59,520 --> 00:18:01,000
 Even when you hear the Buddha's--

336
00:18:01,000 --> 00:18:04,040
 I read the Buddha said, when you walk, just walk.

337
00:18:04,040 --> 00:18:07,240
 When you sit, just sit.

338
00:18:07,240 --> 00:18:08,520
 I didn't really appreciate that.

339
00:18:08,520 --> 00:18:11,440
 It didn't have any meaning to me.

340
00:18:11,440 --> 00:18:13,160
 But then you meditate and say, oh, OK.

341
00:18:13,160 --> 00:18:14,920
 Understand.

342
00:18:14,920 --> 00:18:16,320
 It's meaningful.

343
00:18:16,320 --> 00:18:17,320
 Because you practiced it.

344
00:18:17,320 --> 00:18:25,200
 So I guess-- I mean, the thing is,

345
00:18:25,200 --> 00:18:28,840
 don't study if you're not meditating.

346
00:18:28,840 --> 00:18:30,560
 Don't go and study without meditating.

347
00:18:30,560 --> 00:18:37,720
 That doesn't mean you have to not study and just do med

348
00:18:37,720 --> 00:18:38,840
itating.

349
00:18:38,840 --> 00:18:40,400
 You can do them together.

350
00:18:40,400 --> 00:18:44,520
 Ideally, you've got a teacher who's teaching.

351
00:18:44,520 --> 00:18:48,160
 You don't need to study at all.

352
00:18:48,160 --> 00:18:50,160
 Just follow their instruction.

353
00:18:50,160 --> 00:18:53,640
 [SIDE CONVERSATION]

354
00:18:53,640 --> 00:18:57,120
 [SIDE CONVERSATION]

355
00:18:57,120 --> 00:19:00,600
 [SIDE CONVERSATION]

356
00:19:00,600 --> 00:19:04,080
 [SIDE CONVERSATION]

357
00:19:04,080 --> 00:19:07,560
 [SIDE CONVERSATION]

358
00:19:07,560 --> 00:19:11,040
 [SIDE CONVERSATION]

359
00:19:11,040 --> 00:19:14,520
 [SIDE CONVERSATION]

360
00:19:14,520 --> 00:19:18,000
 [SIDE CONVERSATION]

361
00:19:18,000 --> 00:19:21,480
 [SIDE CONVERSATION]

362
00:19:21,480 --> 00:19:24,960
 [SIDE CONVERSATION]

363
00:19:24,960 --> 00:19:28,440
 [SIDE CONVERSATION]

364
00:19:53,400 --> 00:19:58,480
 OK, well, if there's no more questions, we'll just end it.

365
00:19:58,480 --> 00:20:08,840
 Thank you, Bante.

366
00:20:08,840 --> 00:20:10,080
 Oh, that's what it was.

367
00:20:10,080 --> 00:20:12,080
 I knew there was one more thing.

368
00:20:12,080 --> 00:20:14,040
 And the owner came by.

369
00:20:14,040 --> 00:20:16,320
 Maybe this isn't really a big deal for the broadcast,

370
00:20:16,320 --> 00:20:20,800
 but it's something I thought we should probably talk about.

371
00:20:20,800 --> 00:20:25,000
 He called me last night, or last night,

372
00:20:25,000 --> 00:20:26,760
 and asked if he could bring someone over

373
00:20:26,760 --> 00:20:29,760
 to look at the house, because they want to buy it.

374
00:20:29,760 --> 00:20:41,840
 So he got me thinking, probably we

375
00:20:41,840 --> 00:20:43,920
 should at least be on the lookout

376
00:20:43,920 --> 00:20:46,040
 for potential of other places.

377
00:20:46,040 --> 00:20:47,560
 I mean, I don't know what that means.

378
00:20:47,560 --> 00:20:50,640
 I should have probably asked whether that means we'd

379
00:20:50,640 --> 00:20:55,080
 be able to continue renting it another year.

380
00:20:55,080 --> 00:20:58,320
 But it may be that we don't want this place.

381
00:20:58,320 --> 00:21:00,960
 It's not that big.

382
00:21:00,960 --> 00:21:06,320
 And maybe we could look for something

383
00:21:06,320 --> 00:21:11,840
 that's a little more-- maybe even a little quieter,

384
00:21:11,840 --> 00:21:13,400
 because this is on a busy street.

385
00:21:13,400 --> 00:21:17,920
 Yeah, I think we had looked into it

386
00:21:17,920 --> 00:21:19,960
 before the lease was signed.

387
00:21:19,960 --> 00:21:24,040
 And if the homeowners sold it, you

388
00:21:24,040 --> 00:21:28,120
 do have the right to stay through your lease.

389
00:21:28,120 --> 00:21:31,400
 But maybe you don't want to.

390
00:21:31,400 --> 00:21:33,240
 Yeah, I mean, we'd have the right to stay.

391
00:21:33,240 --> 00:21:36,440
 But at the end of this year, who knows what would happen?

392
00:21:36,440 --> 00:21:39,080
 They have the right to cancel it for the next year.

393
00:21:39,080 --> 00:21:42,040
 We don't have a contract.

394
00:21:42,040 --> 00:21:46,840
 They'd have to just give us two months notice.

395
00:21:46,840 --> 00:21:49,840
 And permanent, I'm really messing with you.

396
00:21:49,840 --> 00:21:51,200
 No, we have a place finally.

397
00:21:51,200 --> 00:21:52,440
 We're even on Google Maps.

398
00:21:52,440 --> 00:21:55,800
 So we have something stable and permanent.

399
00:21:55,800 --> 00:21:57,640
 Last thing.

400
00:21:57,640 --> 00:22:00,080
 No, no such thing.

401
00:22:00,080 --> 00:22:01,840
 No such thing.

402
00:22:01,840 --> 00:22:03,960
 But wherever you end up, there you are.

403
00:22:03,960 --> 00:22:06,600
 And we'll get that place on Google Maps, too.

404
00:22:06,600 --> 00:22:08,360
 Yeah.

405
00:22:08,360 --> 00:22:12,920
 I think I'm sure there's a change of a location thing.

406
00:22:12,920 --> 00:22:15,280
 Oh, definitely.

407
00:22:15,280 --> 00:22:16,600
 OK.

408
00:22:16,600 --> 00:22:17,640
 Good night, everyone.

409
00:22:17,640 --> 00:22:19,080
 Thank you, Robin.

410
00:22:19,080 --> 00:22:20,480
 Thank you, Bante.

411
00:22:20,480 --> 00:22:22,240
 Good night.

