1
00:00:00,000 --> 00:00:01,000
 Good evening.

2
00:00:01,000 --> 00:00:07,000
 We're broadcasting live, January 11th.

3
00:00:07,000 --> 00:00:21,520
 Okay, we won't be doing...

4
00:00:21,520 --> 00:00:30,520
 We won't be doing video tonight, just audio.

5
00:00:30,520 --> 00:00:35,520
 Make's him quite busy.

6
00:00:35,520 --> 00:00:39,520
 Tomorrow we have our first five minute meditation lesson.

7
00:00:39,520 --> 00:00:44,520
 A booth set up, hopefully.

8
00:00:44,520 --> 00:00:50,520
 I just spent the evening pounding grommets into a banner.

9
00:00:50,520 --> 00:00:57,520
 Thanks, Robin, for the grommet kit.

10
00:00:57,520 --> 00:01:05,520
 Learned how to make grommets.

11
00:01:05,520 --> 00:01:11,520
 Today we did our meditation in the atrium, just a few of us

12
00:01:11,520 --> 00:01:11,520
.

13
00:01:11,520 --> 00:01:19,550
 Come together, and I have to make something to give out

14
00:01:19,550 --> 00:01:21,520
 tomorrow.

15
00:01:21,520 --> 00:01:27,520
 Got a bunch of stuff to do.

16
00:01:27,520 --> 00:01:29,520
 And there's all this school work.

17
00:01:29,520 --> 00:01:32,520
 For some reason I'm back in school.

18
00:01:32,520 --> 00:01:41,520
 So, busy stuff here.

19
00:01:41,520 --> 00:01:44,520
 We have a quote tonight about...

20
00:01:44,520 --> 00:01:49,050
 It's one of the many quotes about how to tell the

21
00:01:49,050 --> 00:01:52,520
 difference between the Dhamma and what is not Dhamma.

22
00:01:52,520 --> 00:02:00,520
 It's basically this.

23
00:02:00,520 --> 00:02:09,210
 The point of it is that we shouldn't follow something just

24
00:02:09,210 --> 00:02:14,520
 because it's Buddhist.

25
00:02:14,520 --> 00:02:22,820
 We should be open to questioning and analyzing,

26
00:02:22,820 --> 00:02:25,520
 investigating.

27
00:02:25,520 --> 00:02:26,520
 Because it can change.

28
00:02:26,520 --> 00:02:31,520
 We could hear wrong, we could understand wrong, we could...

29
00:02:31,520 --> 00:02:32,520
 We could be misled.

30
00:02:32,520 --> 00:02:41,520
 Our teachers could be wrong.

31
00:02:41,520 --> 00:02:44,520
 But we know that the Dhamma isn't wrong.

32
00:02:44,520 --> 00:02:47,520
 And we know that by its very definition.

33
00:02:47,520 --> 00:02:55,520
 We might say, like, why is...

34
00:02:55,520 --> 00:03:00,520
 How do we know that all Dhamma is good?

35
00:03:00,520 --> 00:03:02,520
 Well, because that's our definition of it.

36
00:03:02,520 --> 00:03:08,520
 If it weren't good, we wouldn't call it Dhamma.

37
00:03:08,520 --> 00:03:11,890
 If it didn't lead to good, if it didn't lead to turning

38
00:03:11,890 --> 00:03:12,520
 away.

39
00:03:12,520 --> 00:03:21,520
 Turning away means abandoning desire and ambition.

40
00:03:21,520 --> 00:03:25,900
 But turning away from samsara is what comes when you see

41
00:03:25,900 --> 00:03:27,520
 things clearly.

42
00:03:27,520 --> 00:03:32,150
 When you see that things are impermanent, unsatisfying,

43
00:03:32,150 --> 00:03:33,520
 uncontrollable.

44
00:03:33,520 --> 00:03:43,770
 When you lose all interest in them, when you lose all

45
00:03:43,770 --> 00:03:48,520
 desire and volition.

46
00:03:48,520 --> 00:03:51,520
 Also, there's the turning away.

47
00:03:51,520 --> 00:03:55,520
 The fading, fading away.

48
00:03:55,520 --> 00:03:59,300
 Because once you turn away from them, they start to fade

49
00:03:59,300 --> 00:03:59,520
 away.

50
00:03:59,520 --> 00:04:06,290
 All the stress and suffering and chaos and confusion that

51
00:04:06,290 --> 00:04:07,520
 we normally live with.

52
00:04:07,520 --> 00:04:19,520
 All the heat, the fever abates, fades away.

53
00:04:19,520 --> 00:04:25,520
 And nirvana or the unbinding become free.

54
00:04:25,520 --> 00:04:35,970
 We let go. We let go. We're free. When we're free, we say,

55
00:04:35,970 --> 00:04:37,520
 "I'm free."

56
00:04:37,520 --> 00:04:42,520
 "Vimutasming, vimutam nithi, nyanam hoti."

57
00:04:42,520 --> 00:04:52,520
 "No, I'm free."

58
00:04:52,520 --> 00:05:00,260
 So it's by verifying that it leads to these things, that we

59
00:05:00,260 --> 00:05:06,520
 know it's the Dhamma.

60
00:05:06,520 --> 00:05:10,530
 That's important. Buddhism isn't supposed to be about dogma

61
00:05:10,530 --> 00:05:11,520
 or beliefs, you know.

62
00:05:11,520 --> 00:05:19,630
 Most religions have some seemingly arbitrary or specific

63
00:05:19,630 --> 00:05:21,520
 dogma that you have to believe.

64
00:05:21,520 --> 00:05:33,520
 Well, Buddhism is really not supposed to be like that.

65
00:05:33,520 --> 00:05:40,190
 Buddhism is about good. It's about principles that are

66
00:05:40,190 --> 00:05:49,520
 universal and non-esoteric.

67
00:05:49,520 --> 00:05:56,090
 Meaning they should be easily understood and easily ver

68
00:05:56,090 --> 00:05:57,520
ifiable.

69
00:05:57,520 --> 00:06:03,100
 Even nirvana. Nirvana is not some magical, mysterious thing

70
00:06:03,100 --> 00:06:03,520
.

71
00:06:03,520 --> 00:06:08,520
 It's part of nature. It's part of reality.

72
00:06:08,520 --> 00:06:20,520
 Something we can realize for ourselves.

73
00:06:20,520 --> 00:06:24,080
 So we practice. We practice. We don't worry too much about

74
00:06:24,080 --> 00:06:31,980
 results, but we have to verify that what we're doing is

75
00:06:31,980 --> 00:06:33,520
 actually good.

76
00:06:33,520 --> 00:06:37,450
 We have to be clear about it, because it's not difficult to

77
00:06:37,450 --> 00:06:38,520
 understand.

78
00:06:38,520 --> 00:06:41,090
 If you're hurting yourself or hurting others, that's not

79
00:06:41,090 --> 00:06:41,520
 good.

80
00:06:41,520 --> 00:06:46,020
 So if there's anything that does that, then you should stop

81
00:06:46,020 --> 00:06:46,520
 it.

82
00:06:46,520 --> 00:06:49,920
 It's helping you and helping others, then it's good. You

83
00:06:49,920 --> 00:06:52,520
 should continue it.

84
00:06:52,520 --> 00:06:57,520
 That's about it.

85
00:06:57,520 --> 00:07:01,510
 To that end, we have all the rest of the teachings of the

86
00:07:01,510 --> 00:07:04,520
 Buddha, all the various teachings.

87
00:07:04,520 --> 00:07:11,200
 We have to internalize them and understand them, understand

88
00:07:11,200 --> 00:07:12,520
 the benefit of them.

89
00:07:12,520 --> 00:07:16,110
 Not just follow them, because that's what we're supposed to

90
00:07:16,110 --> 00:07:16,520
 do.

91
00:07:16,520 --> 00:07:18,520
 Because it's so easy to get it wrong that way.

92
00:07:18,520 --> 00:07:22,030
 You focus, put emphasis on the wrong thing, because you don

93
00:07:22,030 --> 00:07:23,520
't really understand why you're doing it.

94
00:07:23,520 --> 00:07:27,680
 When we meditate, maybe you think the words are important,

95
00:07:27,680 --> 00:07:32,790
 so you emphasize the words in your mind, and then you start

96
00:07:32,790 --> 00:07:35,520
 saying random words.

97
00:07:35,520 --> 00:07:39,520
 Maybe you think that walking is important to this.

98
00:07:39,520 --> 00:07:43,180
 Maybe it's important to sit really straight or easy to

99
00:07:43,180 --> 00:07:46,920
 emphasize the wrong things if you don't understand exactly

100
00:07:46,920 --> 00:07:48,520
 what the point is.

101
00:07:48,520 --> 00:07:50,590
 It's important for us to study and to see where does the

102
00:07:50,590 --> 00:07:51,520
 goodness come from.

103
00:07:51,520 --> 00:07:54,540
 What is goodness? We should ask. What is the good that

104
00:07:54,540 --> 00:07:55,520
 comes from this?

105
00:07:55,520 --> 00:07:58,610
 Does this help me see clearly? Does this help me learn to

106
00:07:58,610 --> 00:07:59,520
 let go?

107
00:07:59,520 --> 00:08:04,430
 By this, will I free myself from suffering? That's what we

108
00:08:04,430 --> 00:08:06,520
 should have.

109
00:08:06,520 --> 00:08:12,520
 Then we know it's the dumb love.

110
00:08:12,520 --> 00:08:17,520
 I'm going to leave you with that.

111
00:08:17,520 --> 00:08:20,520
 I've got some more work to do this evening.

112
00:08:20,520 --> 00:08:25,240
 Tomorrow we should probably have done the bada, but see how

113
00:08:25,240 --> 00:08:30,350
 busy I get tomorrow with this five-minute meditation lesson

114
00:08:30,350 --> 00:08:30,520
.

115
00:08:30,520 --> 00:08:34,570
 Thanks for tuning in. Thanks for meditating with us,

116
00:08:34,570 --> 00:08:35,520
 everybody.

117
00:08:35,520 --> 00:08:38,520
 I wish you all the best.

118
00:08:39,520 --> 00:08:42,520
 Thank you.

119
00:08:44,520 --> 00:08:46,520
 Thank you.

