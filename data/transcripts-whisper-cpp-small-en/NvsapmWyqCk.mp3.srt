1
00:00:00,000 --> 00:00:06,150
 Okay, good evening everyone. Welcome to our evening Dhamma

2
00:00:06,150 --> 00:00:07,000
 session.

3
00:00:23,000 --> 00:00:38,220
 One very well known description of the Buddha's teaching is

4
00:00:38,220 --> 00:00:40,000
 as the middle way.

5
00:00:40,000 --> 00:00:47,480
 And so all of us are on the middle way, trying to find the

6
00:00:47,480 --> 00:00:49,000
 middle way.

7
00:00:49,000 --> 00:01:03,220
 Sometimes it's described in that way, most famously in

8
00:01:03,220 --> 00:01:09,690
 terms of not torturing yourself, but not becoming indulgent

9
00:01:09,690 --> 00:01:10,000
.

10
00:01:11,000 --> 00:01:15,470
 I think that applies very well to the meditation practice.

11
00:01:15,470 --> 00:01:19,480
 It's something that is an important lesson for us during

12
00:01:19,480 --> 00:01:21,000
 the meditation course.

13
00:01:21,000 --> 00:01:27,200
 Don't torture yourself. Don't force it. Don't even force

14
00:01:27,200 --> 00:01:29,000
 yourself not to force it.

15
00:01:30,000 --> 00:01:40,000
 You realize that forcing it isn't even under your control.

16
00:01:40,000 --> 00:01:43,000
 This isn't something that you can just steamroll over.

17
00:01:43,000 --> 00:01:48,330
 Don't torture yourself. But at the same time don't become

18
00:01:48,330 --> 00:01:49,000
 complacent.

19
00:01:49,000 --> 00:01:53,380
 And so it seems actually it eventually becomes a razor's

20
00:01:53,380 --> 00:01:57,000
 edge, when there's only really one way, there's no leeway.

21
00:01:58,000 --> 00:02:04,420
 If you want to really be in the middle way, anupagama, you

22
00:02:04,420 --> 00:02:10,000
 have to not go at all in either direction.

23
00:02:10,000 --> 00:02:14,840
 So what we're looking for is this state of balance in a

24
00:02:14,840 --> 00:02:17,000
 sense on a razor's edge.

25
00:02:17,000 --> 00:02:24,120
 And balance is a really good way to understand the middle

26
00:02:24,120 --> 00:02:26,000
 way as well.

27
00:02:27,000 --> 00:02:31,770
 Now balance doesn't mean a little bit of indulgence and a

28
00:02:31,770 --> 00:02:34,000
 little bit of torture.

29
00:02:34,000 --> 00:02:39,740
 Balance means to find a state that is free from such things

30
00:02:39,740 --> 00:02:43,380
, like this state that is in either one direction or the

31
00:02:43,380 --> 00:02:44,000
 other.

32
00:02:44,000 --> 00:02:47,000
 It's a very tricky thing.

33
00:02:47,000 --> 00:02:53,000
 But there are many types of balance that are talked about.

34
00:02:54,000 --> 00:02:58,580
 I think many of you have read my booklet where I relate the

35
00:02:58,580 --> 00:03:02,000
 teaching on balancing the faculties.

36
00:03:02,000 --> 00:03:08,640
 So confidence has to balance with wisdom, wisdom has to

37
00:03:08,640 --> 00:03:14,670
 balance with confidence, effort has to balance with

38
00:03:14,670 --> 00:03:17,000
 concentration, and vice versa.

39
00:03:18,000 --> 00:03:22,120
 Mindfulness alone is the one you don't have to balance, you

40
00:03:22,120 --> 00:03:24,000
 don't have to moderate.

41
00:03:24,000 --> 00:03:31,000
 Mindfulness, the more mindfulness you have the better.

42
00:03:31,000 --> 00:03:36,120
 There's no middle way with mindfulness. Mindfulness is the

43
00:03:36,120 --> 00:03:37,000
 middle way.

44
00:03:38,000 --> 00:03:45,430
 Because mindfulness is what allows you to avoid extremes,

45
00:03:45,430 --> 00:03:49,000
 avoid an excess or deficiency.

46
00:03:49,000 --> 00:03:57,360
 We have so many different qualities of mind that the Buddha

47
00:03:57,360 --> 00:03:58,000
 talks about.

48
00:03:58,000 --> 00:04:01,210
 And sometimes you read it and you think, "Well, how do I go

49
00:04:01,210 --> 00:04:03,000
 about developing that one?"

50
00:04:04,000 --> 00:04:09,560
 It's like you read about all the parts of a motor and you

51
00:04:09,560 --> 00:04:12,920
 open the hood of your car and you try to figure out how to

52
00:04:12,920 --> 00:04:14,000
 turn each one on.

53
00:04:14,000 --> 00:04:17,000
 That's not how it works.

54
00:04:17,000 --> 00:04:20,730
 It's important to know all the parts, to be able to fix

55
00:04:20,730 --> 00:04:24,000
 them if they get broken, to clean them and so on.

56
00:04:24,000 --> 00:04:26,730
 But you don't go around starting all the parts of the

57
00:04:26,730 --> 00:04:29,000
 engine, you really just turn the key.

58
00:04:30,000 --> 00:04:33,200
 And so here we have this great vehicle of insight

59
00:04:33,200 --> 00:04:38,340
 meditation and so many adornments like loving kindness and

60
00:04:38,340 --> 00:04:43,000
 compassion and generosity and wisdom, of course.

61
00:04:43,000 --> 00:04:47,800
 But you don't really have to go around turning all those

62
00:04:47,800 --> 00:04:53,000
 things on. Mindfulness is the one. Mindfulness is the key.

63
00:04:59,000 --> 00:05:02,040
 So mindfulness is the one that balances. And you see that

64
00:05:02,040 --> 00:05:05,450
 again in an important set. That's something I wanted to

65
00:05:05,450 --> 00:05:07,000
 really talk about tonight.

66
00:05:07,000 --> 00:05:15,000
 Our local meditators are all doing really well on track.

67
00:05:17,000 --> 00:05:24,090
 So I thought it was also time to talk about the seven bhoj

68
00:05:24,090 --> 00:05:30,800
angas, bho-di-anga, bho-janga, and the factors of

69
00:05:30,800 --> 00:05:32,000
 enlightenment.

70
00:05:32,000 --> 00:05:38,100
 The factors of enlightenment allow us, without going into

71
00:05:38,100 --> 00:05:42,380
 too much detail, because we don't want to get too caught up

72
00:05:42,380 --> 00:05:43,000
 in theory.

73
00:05:44,000 --> 00:05:49,940
 But they can describe to you the path, describe to you the

74
00:05:49,940 --> 00:05:58,000
 progress and the change that comes to pass.

75
00:05:58,000 --> 00:06:07,430
 So they start with sati. And one way to understand them is

76
00:06:07,430 --> 00:06:09,000
 as a progression.

77
00:06:10,000 --> 00:06:14,140
 So it starts with sati, of course, with mindfulness. It

78
00:06:14,140 --> 00:06:16,670
 starts when you begin to practice, when you begin to

79
00:06:16,670 --> 00:06:20,000
 cultivate objective awareness.

80
00:06:20,000 --> 00:06:28,000
 Seeing, hearing, feeling, thinking, liking, disliking.

81
00:06:31,000 --> 00:06:40,140
 When you recognize it, remind yourself it is what it is.

82
00:06:40,140 --> 00:06:42,670
 That's the key that starts you on the path. And that's what

83
00:06:42,670 --> 00:06:44,000
 you've been doing, right?

84
00:06:44,000 --> 00:06:50,800
 So as you do that, number two is dhammavicaya. And this is

85
00:06:50,800 --> 00:06:54,660
 the sort of an investigation or a realization of the dham

86
00:06:54,660 --> 00:06:55,000
mas.

87
00:06:57,000 --> 00:07:02,590
 Which means, as you practice, you come to understand

88
00:07:02,590 --> 00:07:07,130
 yourself. You learn many things about yourself, about

89
00:07:07,130 --> 00:07:08,000
 reality.

90
00:07:08,000 --> 00:07:13,350
 You see clearly your habits, good and bad. And you see

91
00:07:13,350 --> 00:07:20,870
 clearly that the objects which you react to, liking, disl

92
00:07:20,870 --> 00:07:25,000
iking, etc., arrogant, conceited, and so on.

93
00:07:27,000 --> 00:07:30,030
 You see that they're not what you thought. You see that

94
00:07:30,030 --> 00:07:34,410
 whatever you cling to or hold on to is impermanent

95
00:07:34,410 --> 00:07:37,000
 suffering and non-self.

96
00:07:37,000 --> 00:07:32,680
 Dhammavicaya is about seeing three characteristics. It's

97
00:07:32,680 --> 00:07:28,880
 about changing our predilection for stability and our

98
00:07:28,880 --> 00:07:54,000
 misguided notion that we can find stability.

99
00:07:54,000 --> 00:07:57,170
 Experience something pleasant and we cling to it thinking,

100
00:07:57,170 --> 00:08:00,930
 "Oh, this will last forever." Right? We don't think that,

101
00:08:00,930 --> 00:08:03,000
 but that's how we approach it.

102
00:08:03,000 --> 00:08:08,250
 So that when it's gone, we feel very sad. We suffer. It's

103
00:08:08,250 --> 00:08:14,150
 suffering because it changes. And you can't control it. You

104
00:08:14,150 --> 00:08:15,000
're not in charge.

105
00:08:16,000 --> 00:08:22,080
 You'll find meditators when they begin to practice, they'll

106
00:08:22,080 --> 00:08:26,000
 try very much to control their practice.

107
00:08:26,000 --> 00:08:34,240
 But you can't control. So you see non-self, impermanent

108
00:08:34,240 --> 00:08:36,000
 suffering, non-self.

109
00:08:38,000 --> 00:08:40,720
 This is the second step. This is what you've already begun

110
00:08:40,720 --> 00:08:43,540
 as well, right? You've already seen some of this. You're

111
00:08:43,540 --> 00:08:46,000
 watching, you're learning about this as we go.

112
00:08:46,000 --> 00:08:52,000
 As you see that, the third one arises, which is vidya.

113
00:08:53,000 --> 00:08:58,120
 Now vidya here really comes after you start to see the

114
00:08:58,120 --> 00:09:03,130
 truth because in the beginning you're withholding your

115
00:09:03,130 --> 00:09:09,000
 doubt, your sordha, giving the benefit of the doubt.

116
00:09:14,000 --> 00:09:18,000
 And so you're not 100% into it, right? Why would you be?

117
00:09:18,000 --> 00:09:21,140
 But as you practice and you start to see the results, see

118
00:09:21,140 --> 00:09:25,000
 the benefits, see the truth, see the usefulness of this,

119
00:09:25,000 --> 00:09:30,000
 the usefulness of training in equanimity and objectivity.

120
00:09:35,000 --> 00:09:41,970
 And you begin to cultivate, you begin to become energetic,

121
00:09:41,970 --> 00:09:47,000
 enthusiastic, intent upon it, vidya.

122
00:09:50,000 --> 00:09:59,110
 After vidya then there's vidhi. So once you start to really

123
00:09:59,110 --> 00:10:05,820
 put out effort, it becomes powerful, right? Because your

124
00:10:05,820 --> 00:10:08,520
 other habits are getting weaker, because you're not

125
00:10:08,520 --> 00:10:11,820
 engaging in them, and your mind is so engrossed in the

126
00:10:11,820 --> 00:10:15,000
 meditation, then it starts to become a habit.

127
00:10:16,000 --> 00:10:19,920
 And the energy there, the sort of the charge that builds up

128
00:10:19,920 --> 00:10:24,610
, the power that builds up is called vidhi. Vidhi is this

129
00:10:24,610 --> 00:10:32,000
 power, this static charge, if you will, the power of

130
00:10:32,000 --> 00:10:39,000
 inertia. Once you get going, it starts to go by itself.

131
00:10:40,000 --> 00:10:45,000
 So vidhi is of many kinds, of course, there are, I mean vid

132
00:10:45,000 --> 00:10:50,000
hi is just any kind of ecstatic or charged state of mind.

133
00:10:50,000 --> 00:10:55,580
 There's many of them. But here it means being enthusiastic

134
00:10:55,580 --> 00:11:01,000
 and engaged and strong in your practice.

135
00:11:06,000 --> 00:11:11,630
 Once you're engaged in the practice and really set upon it,

136
00:11:11,630 --> 00:11:16,740
 then there arises passati, tranquility. Some of you have

137
00:11:16,740 --> 00:11:19,530
 already talked about how you feel calm sometimes, that's

138
00:11:19,530 --> 00:11:23,000
 good. Don't cling to it, obviously.

139
00:11:24,000 --> 00:11:27,760
 And when you've come to see is that when you cling to it,

140
00:11:27,760 --> 00:11:32,000
 it doesn't make you more calm. For sure.

141
00:11:37,000 --> 00:11:50,600
 But it's good to realize that this is a benefit that you

142
00:11:50,600 --> 00:11:54,260
 calm down. You're not trying to force it, but you start to

143
00:11:54,260 --> 00:11:54,620
 say that by just being aware and objective and allowing the

144
00:11:54,620 --> 00:11:56,440
 distress and the restlessness and so on, and not reacting

145
00:11:56,440 --> 00:12:00,000
 to it, how that in and of itself calms you down.

146
00:12:01,000 --> 00:12:06,230
 You no longer have to be anything, you no longer react to

147
00:12:06,230 --> 00:12:07,000
 things.

148
00:12:07,000 --> 00:12:14,350
 After passadib, there becomes samadhi. So once it's quiet,

149
00:12:14,350 --> 00:12:19,000
 then your mind becomes very focused.

150
00:12:21,000 --> 00:12:24,360
 So you've been very energetic and intent upon it, quiet

151
00:12:24,360 --> 00:12:27,960
 down, and once you quiet down, all of these, starting with

152
00:12:27,960 --> 00:12:32,650
 mindfulness, they work together to focus your attention,

153
00:12:32,650 --> 00:12:36,700
 your mind becomes focused, and when you walk, you're only

154
00:12:36,700 --> 00:12:39,660
 aware of the right foot moving and then the left foot

155
00:12:39,660 --> 00:12:44,000
 moving, or lifting, placing, or lifting heel and so on.

156
00:12:44,000 --> 00:13:02,000
 So there arises samadhi. Your mind is no longer restless.

157
00:13:03,000 --> 00:13:06,100
 After samadhi, then there is upekha. And this is what we're

158
00:13:06,100 --> 00:13:09,100
 aiming for. Those of you, a couple of you, have been to the

159
00:13:09,100 --> 00:13:12,000
 goenka courses. He talks a lot about this, I think.

160
00:13:12,000 --> 00:13:16,390
 But we're not trying to force upekha, right? Key for all of

161
00:13:16,390 --> 00:13:21,590
 this is to not try and cultivate the goal. We have to

162
00:13:21,590 --> 00:13:25,690
 cultivate the practice. And if it's right practice, the

163
00:13:25,690 --> 00:13:28,000
 goal will come by itself.

164
00:13:29,000 --> 00:13:31,880
 So through all of this, you should become more equanimous.

165
00:13:31,880 --> 00:13:35,000
 Don't force that. That's not the goal. That's not the way.

166
00:13:35,000 --> 00:13:44,210
 But it will by itself, you become very much intent upon the

167
00:13:44,210 --> 00:13:47,730
 practice and also as you start to learn more about yourself

168
00:13:47,730 --> 00:13:48,000
.

169
00:13:49,000 --> 00:13:54,860
 You're less inclined to cling to things that can't make you

170
00:13:54,860 --> 00:14:00,280
 happy, can't satisfy you. Things that only lead to

171
00:14:00,280 --> 00:14:04,000
 suffering when you cling to them.

172
00:14:05,000 --> 00:14:10,020
 Upekha is the final one. Once you have upekha, true upekha,

173
00:14:10,020 --> 00:14:14,540
 that's the highest of vipassana. It's from there that you

174
00:14:14,540 --> 00:14:22,760
 enter into nimana. Sankhara upekha. Equanimity about all

175
00:14:22,760 --> 00:14:24,000
 sankharas.

176
00:14:25,000 --> 00:14:28,730
 Just as arising and ceasing. No longer distressed, no

177
00:14:28,730 --> 00:14:33,210
 longer upset, no longer craving or yearning. Content,

178
00:14:33,210 --> 00:14:36,000
 peaceful, focused.

179
00:14:36,000 --> 00:14:39,990
 So there are real progression in that way. But the Buddha

180
00:14:39,990 --> 00:14:43,000
 offers another way of understanding them.

181
00:14:44,000 --> 00:14:49,970
 He says that investigation of dhammas and energy and rapt

182
00:14:49,970 --> 00:14:55,000
ure, these three relate to energy, effort.

183
00:14:55,000 --> 00:14:59,970
 So they're the ones you should cultivate when you're tired

184
00:14:59,970 --> 00:15:02,000
 or when you're lazy.

185
00:15:03,000 --> 00:15:09,700
 You're falling asleep while you cultivate dhammavicaya, v

186
00:15:09,700 --> 00:15:13,000
iriya, viti. Focus on them.

187
00:15:13,000 --> 00:15:20,460
 When you're, on the other three, tranquility or quietude,

188
00:15:20,460 --> 00:15:25,000
 concentration and equanimity.

189
00:15:30,000 --> 00:15:35,540
 These ones are related to concentration. So if you're

190
00:15:35,540 --> 00:15:38,470
 restless, if you're distracted, you should cultivate these

191
00:15:38,470 --> 00:15:39,000
 three.

192
00:15:39,000 --> 00:15:44,690
 But again, we get back to this idea of, well first we get

193
00:15:44,690 --> 00:15:49,800
 back to this idea of balance. You balance these seven

194
00:15:49,800 --> 00:15:52,000
 factors of enlightenment.

195
00:15:52,000 --> 00:16:07,720
 But we get, we come back to this idea of trying to go

196
00:16:07,720 --> 00:16:13,000
 around and start all, tweak all the parts of the engine.

197
00:16:14,000 --> 00:16:18,370
 And maybe that's a good analogy because there is some

198
00:16:18,370 --> 00:16:22,200
 tweaking that can be done, but in meditation practice the

199
00:16:22,200 --> 00:16:25,000
 tweaking itself comes about by mindfulness.

200
00:16:25,000 --> 00:16:28,680
 You know, in a way it's just a way of highlighting the fact

201
00:16:28,680 --> 00:16:31,600
 that mindfulness is in the center. Because this is where

202
00:16:31,600 --> 00:16:32,000
 the Buddha said,

203
00:16:33,000 --> 00:16:37,040
 "Ting chukwang bhikkave sabhati kang wadami." Whereas the

204
00:16:37,040 --> 00:16:42,000
 other six factors of enlightenment are only important to

205
00:16:42,000 --> 00:16:45,000
 cultivate when you're imbalanced.

206
00:16:45,000 --> 00:16:49,000
 Mindfulness is always useful.

207
00:16:52,000 --> 00:16:59,690
 And so by being mindful of the imbalance, that's where

208
00:16:59,690 --> 00:17:03,000
 balance comes from.

209
00:17:03,000 --> 00:17:09,030
 So remember the bhojangas. I think this is really the way

210
00:17:09,030 --> 00:17:13,060
 it's to be done. You know, you're not going to go around

211
00:17:13,060 --> 00:17:14,000
 and tweak the engine.

212
00:17:15,000 --> 00:17:17,750
 You don't go around and say, "Okay, I don't have enough

213
00:17:17,750 --> 00:17:20,820
 effort. I'm going to cultivate effort." And so it could

214
00:17:20,820 --> 00:17:22,000
 magically appear.

215
00:17:22,000 --> 00:17:26,430
 It's important to know the dhamma. What we don't realize is

216
00:17:26,430 --> 00:17:29,960
 that just knowing these things already changes the way you

217
00:17:29,960 --> 00:17:31,000
 look at them.

218
00:17:31,000 --> 00:17:36,420
 Right? If I hadn't taught you how to meditate, how to be

219
00:17:36,420 --> 00:17:39,000
 mindful, you wouldn't know how to look at the mind.

220
00:17:40,000 --> 00:17:42,730
 When we talk about the three characteristics, if you hadn't

221
00:17:42,730 --> 00:17:45,890
 heard of impermanent suffering and non-self, you would have

222
00:17:45,890 --> 00:17:48,000
 thought this was just torture.

223
00:17:48,000 --> 00:17:53,010
 Many people do run away. Well, not many. Some people do run

224
00:17:53,010 --> 00:17:56,000
 away in the early stages of the course.

225
00:17:56,000 --> 00:18:01,880
 Because they can't handle, they don't understand what's

226
00:18:01,880 --> 00:18:05,020
 really going on, and they see impermanent suffering and non

227
00:18:05,020 --> 00:18:07,000
-self and it scares them, bothers them.

228
00:18:09,000 --> 00:18:17,000
 [silence]

229
00:18:17,000 --> 00:18:40,000
 [silence]

230
00:18:41,000 --> 00:18:43,680
 It's important to know the dhamma. It's important to be

231
00:18:43,680 --> 00:18:47,180
 taught it, to be, have it explained to you. Because just

232
00:18:47,180 --> 00:18:49,000
 knowing it changes.

233
00:18:49,000 --> 00:18:52,000
 Changes how you look at it.

234
00:18:52,000 --> 00:18:56,370
 The way I stress that, because what you don't want to do is

235
00:18:56,370 --> 00:18:58,000
 try and control it.

236
00:18:58,000 --> 00:19:01,180
 As I said, you don't have energy, so you run around doing

237
00:19:01,180 --> 00:19:06,000
 jumping jacks or something. You don't force these things.

238
00:19:06,000 --> 00:19:08,000
 You understand them.

239
00:19:09,000 --> 00:19:12,730
 Most importantly, it gives you a focus for your mindfulness

240
00:19:12,730 --> 00:19:13,000
.

241
00:19:13,000 --> 00:19:18,480
 Highlighting all the... Buddha did this. He would highlight

242
00:19:18,480 --> 00:19:20,000
 all the various aspects of experience.

243
00:19:20,000 --> 00:19:24,000
 By highlighting them, it's like shining a light on them.

244
00:19:24,000 --> 00:19:26,610
 And when you hear about this and you reflect, "How is my

245
00:19:26,610 --> 00:19:31,000
 effort? How is my concentration?" Equanimity and so on.

246
00:19:32,000 --> 00:19:38,000
 You see, you have a light shine on, shine shone, shined.

247
00:19:38,000 --> 00:19:49,000
 On the experience, on the nature of reality, on reality.

248
00:19:49,000 --> 00:19:56,840
 And it makes it much more precise and clear when it goes to

249
00:19:56,840 --> 00:19:59,000
 being mindful of it.

250
00:19:59,000 --> 00:20:04,000
 So, remember these seven.

251
00:20:04,000 --> 00:20:11,090
 Mindfulness, sati, dhammavicaya, which means the wisdom

252
00:20:11,090 --> 00:20:15,330
 that comes from it, the realization of impermanence,

253
00:20:15,330 --> 00:20:17,000
 suffering and non-self.

254
00:20:21,000 --> 00:20:22,000
 Mw

255
00:20:22,000 --> 00:20:22,000
 Mw

256
00:20:22,000 --> 00:20:28,000
 Mw

257
00:20:28,000 --> 00:20:36,000
 Mw

258
00:20:36,000 --> 00:20:43,000
 Mw

259
00:20:44,000 --> 00:20:47,000
 That allows you to correct it.

260
00:20:47,000 --> 00:20:51,350
 So, there you go. A little bit of dhamma tonight. Something

261
00:20:51,350 --> 00:20:54,420
 very important. I mean, it's quite simple, but those seven

262
00:20:54,420 --> 00:20:58,000
 things, those are what leads to enlightenment.

263
00:20:58,000 --> 00:21:02,000
 So, thank you all for coming out.

264
00:21:02,000 --> 00:21:07,000
 Keep practicing.

265
00:21:07,000 --> 00:21:33,000
 [silence]

266
00:21:34,000 --> 00:21:37,000
 Oh, we have more questions on the site.

267
00:21:37,000 --> 00:21:41,080
 In relation to the meditation technique on sitting, I

268
00:21:41,080 --> 00:21:44,990
 usually do it with my eyes open as with walking because

269
00:21:44,990 --> 00:21:48,220
 most of the day I'm with my eyes open when I want to relate

270
00:21:48,220 --> 00:21:50,000
 my practice to daily life.

271
00:21:50,000 --> 00:21:54,000
 I wonder how much important is the practice.

272
00:21:54,000 --> 00:21:58,070
 Also, sitting meditation with your eyes closed is the key

273
00:21:58,070 --> 00:22:00,000
 factor of the practice.

274
00:22:01,000 --> 00:22:07,910
 No, but it's a sense door that we can close, and as a

275
00:22:07,910 --> 00:22:12,000
 result it simplifies things.

276
00:22:12,000 --> 00:22:19,000
 So, closing the eyes is actually beneficial in that sense.

277
00:22:19,000 --> 00:22:20,000
 It makes it easier.

278
00:22:20,000 --> 00:22:24,000
 Meditation is hard enough without trying to make it harder.

279
00:22:24,000 --> 00:22:27,350
 I get what you're saying, trying to make it like real life,

280
00:22:27,350 --> 00:22:30,000
 but real life it's very hard to be mindful.

281
00:22:30,000 --> 00:22:34,260
 So, there's nothing wrong with having your eyes open. It's

282
00:22:34,260 --> 00:22:38,420
 just you probably get better, more quicker results with

283
00:22:38,420 --> 00:22:44,990
 your eyes closed because it's easier and it easy allows you

284
00:22:44,990 --> 00:22:50,030
 to, to that extent allows you to focus better, to progress

285
00:22:50,030 --> 00:22:51,000
 quicker.

286
00:22:51,000 --> 00:22:57,000
 [silence]

287
00:22:58,000 --> 00:23:01,000
 What is akasha? Akasa means space, yes.

288
00:23:01,000 --> 00:23:07,910
 Akasa is the world, the world of space. It's one of the

289
00:23:07,910 --> 00:23:13,000
 three worlds. There's akasa loka, the world of space.

290
00:23:13,000 --> 00:23:17,570
 There's sata loka, which is the world of beings. And then

291
00:23:17,570 --> 00:23:21,000
 there's sankara loka, which is the world of formations.

292
00:23:26,000 --> 00:23:33,000
 There's a question about zazen compared to masi.

293
00:23:33,000 --> 00:23:37,590
 Sorry, I don't do such things. I don't really, because I

294
00:23:37,590 --> 00:23:39,260
 don't know anything about what you're talking about. I don

295
00:23:39,260 --> 00:23:41,000
't know much about zen at all.

296
00:23:41,000 --> 00:23:44,000
 And besides what you read in books.

297
00:23:46,000 --> 00:23:48,830
 Every once in a while I get dizzy and when I open my eyes,

298
00:23:48,830 --> 00:23:51,860
 my eyes are shaking. I'm trying to be mindful of the

299
00:23:51,860 --> 00:23:55,570
 shaking and frustration, but it seems to be too intense for

300
00:23:55,570 --> 00:23:57,000
 being a meditator.

301
00:23:57,000 --> 00:24:01,710
 Well, I mean, seems to be too intense. It's just a judgment

302
00:24:01,710 --> 00:24:06,450
. So you can say judging, judging or worried, worried or so

303
00:24:06,450 --> 00:24:07,000
 on.

304
00:24:08,000 --> 00:24:12,710
 So such thing is too intense. Too intense is when you, when

305
00:24:12,710 --> 00:24:14,000
 you give into it.

306
00:24:14,000 --> 00:24:19,210
 And it's good to try to be mindful of the shaking and

307
00:24:19,210 --> 00:24:22,960
 frustration. What you're learning is impermanent suffering,

308
00:24:22,960 --> 00:24:26,000
 non-self impermanent, because it's strange, you know.

309
00:24:27,000 --> 00:24:30,090
 Impermanence is when you see that things can change at any

310
00:24:30,090 --> 00:24:34,470
 time. It wasn't like this now. It's like this. Suffering

311
00:24:34,470 --> 00:24:38,200
 because you see that it's not the way you want it to be. It

312
00:24:38,200 --> 00:24:40,000
 makes you frustrated.

313
00:24:40,000 --> 00:24:45,430
 And non-self. And suffering also that you can't make it the

314
00:24:45,430 --> 00:24:47,000
 way you, you can't fix it.

315
00:24:48,000 --> 00:24:51,900
 It doesn't, it doesn't go according to your plan. Non-self,

316
00:24:51,900 --> 00:24:56,000
 right? You can't, because, because you can't control it.

317
00:24:56,000 --> 00:25:08,450
 Any tips for remaining mindful throughout the day? Thank

318
00:25:08,450 --> 00:25:13,000
 you. I'll become enlightened. That's the best way.

319
00:25:15,000 --> 00:25:20,760
 I'd encourage if you have time to do a meditation course or

320
00:25:20,760 --> 00:25:24,830
 more than one. Intensive courses are really good for

321
00:25:24,830 --> 00:25:28,000
 helping you be more mindful during the day.

322
00:25:28,000 --> 00:25:31,440
 Live in a place where people are mindful, live in a

323
00:25:31,440 --> 00:25:36,610
 monastery. That helps. That kind of thing. Study, study the

324
00:25:36,610 --> 00:25:39,000
 Dhamma, that'll help.

325
00:25:42,000 --> 00:25:44,670
 Okay, so that's all the questions. Thank you all for tuning

326
00:25:44,670 --> 00:25:49,000
 in. Again, I'm not here tomorrow, probably through Sunday,

327
00:25:49,000 --> 00:25:51,000
 because there's this big celebration.

328
00:25:51,000 --> 00:25:54,130
 Tomorrow I'm going to teach, I mentioned this, I'm going to

329
00:25:54,130 --> 00:25:57,000
 teach at University of Toronto. There's a celebration.

330
00:25:57,000 --> 00:26:04,510
 It's a memorial for the head monk's dead mother. And then

331
00:26:04,510 --> 00:26:07,910
 on Sunday there's the Buddhist birthday celebration in

332
00:26:07,910 --> 00:26:11,000
 Mississauga. Big, thousand, thousand people.

333
00:26:11,000 --> 00:26:15,250
 We're going to have a meditation tent there. So, that's all

334
00:26:15,250 --> 00:26:18,000
. Have a good night everyone.

