 I can start it now.
 Okay, the live stream is now live.
 So this is teaching in Second Life.
 This stream is also being recorded to our website
 and broadcast publicly.
 At siri-mongolo.org colon 8000 front slash live.
 Maybe somebody can post the link.
 If anyone has the link.
 No, that's not the link.
 Should be, that should be the link.
 Okay, so we'll just wait. We still got five minutes, four
 minutes before we're
 ready to start.
 But today's talk is going to be about right view.
 And that was a topic suggested by someone on our live
 broadcast
 last night, I think. So maybe what we can do is at the end
 of
 today's talk, if there's anyone here who
 has a suggestion for next week, something they'd
 like to hear about, know more about, they can present it.
 Or you can do that now. We got four minutes if anybody has
 any suggestions
 for next week.
 Otherwise, let me know before we, before I leave.
 We'll talk about it. I mean, a lot of the ideas I'm
 probably just going to shoot
 down because I've already done a talk on them.
 Well, there's many ideas, there's several ideas that
 like last night I had to say, probably not because I've
 done
 several talks on that topic.
 It's the thing about the internet. It's
 in anyone who does public speaking for a living or on a
 regular basis
 is aware of the difference. Whereas before you could say
 the same thing again and again and again,
 it doesn't work anymore because everything's already
 recorded.
 The people you're talking to the second time have already
 heard what you said
 the first time. So it's much more important to say
 something new.
 It's interesting because hearing the same things over and
 over again is actually
 can be a really good thing. But because we're also
 recording, there's often a
 sort of an impetus to say something new instead.
 It's a new dynamic.
 On the other hand, it's a good part of the internet that
 people who want to
 hear the same things over and over again can go back and
 listen to the old talks over again.
 So somebody mentioned "write speech." I'm not sure if I
 could spend a half an hour
 talking about just write speech. I'm not that good of a
 talker.
 Maybe I could, but off the top of my head it seems like, "
Wow, is that really such a
 big topic?" I'm sorry that sounds flippant, but
 technically there's not a lot to talk about
 with write speech. If I had time, I could come up with some
 stories about
 write speech.
 But, you know, as a topic it's the kind of thing you'd talk
 about in the larger
 talk about the Eightfold Noble Path.
 [Silence]
 Virtue. I could talk about virtue. I mean morality is,
 in general, it would make a good talk. People are
 interested because it's not
 just about keeping rules.
 I don't know that I've done many talks specifically on
 morality, but I have
 talked about it.
 Virtue or ethics, however you want to say it. Okay, so it's
 three o'clock.
 Guess we'll just get started then. Welcome everyone.
 If anyone wants to do a video recording, please feel free.
 I'm
 too afraid that my computer will crash for a third time if
 I try it again.
 So I'm not going to do video recording,
 but the audio will be available on our website
 at meditation.siri-mongolo.org/live. I think is where it is
.
 There's a whole archive of live talks.
 Okay, so today's talk is going to be about right view.
 What is right view?
 And how do we cultivate right view? Why is right view
 important? Maybe as well.
 So first, the general sort of an introduction to right view
.
 Right view is the first of the eight factors of the Eight
fold Noble Path.
 It has some preeminence in Buddhism,
 being the core of the training on wisdom.
 One way of looking at right view is, I'll get into this a
 little bit
 later, is that it's the attainment of, or it's the moment
 that leads to the
 attainment of nirvana, of freedom from suffering. So it's
 really the
 key factor in attaining enlightenment. And we talk about
 mindfulness as being
 the key that starts the engine. But right view is,
 I don't know, maybe you'd want to say right view is
 the reaching of your destination. Or if you want to go the
 different kind of
 analogy, it could be when the engine sparks the light.
 So when all the path factors work together, the spark that
 lights the
 gasoline is right view.
 So you don't practice right view necessarily.
 You cultivate it. And near the end of the talk, I'll talk
 about ways in which you
 cultivate it. But so generally what right view is,
 right view is
 a proper way of understanding, looking at the world.
 In context, it's one of the three
 rightnesses that combats the three perversions.
 Perversion not in a terribly pejorative
 sense, or not in the way we use it today, but
 in something being disgusting or perverse.
 But perversion in the sense of distortion.
 So there are three distortions or misunderstandings.
 Well, I'm not even misunderstanding. Wrongnesses, this may
 be good.
 One of them is wrong view. Wrong view is the worst.
 The second is wrong thought. And the third is wrong
 perception. Or if you
 want to say the perversion of view, the perversion of
 thought, and the
 perversion of perception. Perception is probably the
 wrong word. It's just the way we... It's probably okay.
 But I don't think technically it is the right dictionary
 definition word to be
 using. But I'll explain what it means. So right
 perception, or whatever word you want to use, sanya.
 The word we use for sanya, which often just means to
 recognize something,
 is the most visceral and really the hardest and the last
 to do away with. So it means when you see something
 and you react to it as being good or bad. You look at
 something and
 you're attracted to it. And that leads of course to desire,
 to craving, to attachment, so on. Or you look at something
 and you're
 turned off by it. You're upset by it in some way or another
. Maybe with anger,
 maybe with sadness, maybe with fear, maybe with disgust.
 Or you look at something, third case is you look at
 something or you hear
 something or whatever. You experience something.
 And there is an identification with it, delusion based on
 it.
 So you look at something and think that's mine. Or you look
 at something and
 think that's permanent, that's stable. You look at
 something and you
 judge it.
 It gives rise to arrogance. So some kind of misconception,
 generally
 misconception of things as being permanent, satisfying, or
 controllable in
 some way or another.
 And this is what leads us to greed, anger, and delusion.
 But it's just a perception. I mean this is a habit. It
 comes up habitually.
 It's very hard to do away with. This is the last one that
 you do away with.
 More of course than that, and thus easier to do away with,
 is wrong
 thought. So suppose you have a perception of something as
 being
 ugly, or someone is being inferior to you, or someone is
 being
 beautiful, someone is being, something is being attractive.
 Well then you think to yourself, boy that thing is
 attractive, or that thing is
 is going to bring me happiness. That thing is going to
 satisfy me.
 That thing, I can control that thing. So it's a thought
 that comes up. Again,
 these are, these tend to be fairly instinctual or habitual.
 We can't control the thoughts that we have. Some people
 make this mistake.
 I mean because we attach to thoughts as being self,
 my thoughts, we have this predisposition to think
 think of them possessively. We get upset at our thoughts. "
Oh how could I
 think such a horrible thing? I'm a horrible person for
 thinking that
 horrible thing." But that's not the nature of thoughts.
 Thoughts, they can come from a whole variety of reasons.
 Often they're just
 caused by the brain. The brain is the impetus for the
 arising of thought.
 And who knows what's going on in the brain. There could be
 a chemical
 imbalance. There could be any number of things.
 Could have eaten the wrong food. Could have some
 memories mixed up, some wires shorted. You know the brain
 is organic.
 Who knows what's going on? You could have brain damage
 of any sorts. Could have been lack of sleep,
 making you think these things lots of different reasons.
 But the point is that
 they're just thoughts.
 And we often make the mistake of making more of a thought
 than it is. We think
 that's our view. What may not be. Now you can have a
 thought about killing someone and never have any desire,
 you know,
 and understand fully that killing them is wrong.
 But the thought comes up anyway. You may not even have any
 anger towards the
 person. But silly thoughts can come up. You know,
 horrible thoughts can come up. And they're just thoughts.
 But there are those thoughts that do lead to
 more weighty emotions. Like if you think someone is
 terrible or awful, you might, you're more likely to get
 angry at them.
 If you think someone or something is beautiful or
 attractive, you're much
 more likely to become addicted to it, attached to it, to
 desire it.
 And if you think of, you know, with thoughts of delusion,
 like
 I'm better than that person, or I don't deserve that,
 or I deserve that, you're much more likely to identify with
 it and cultivate
 ego, arrogance, conceit, etc. So thoughts are
 also problematic.
 Wrong thoughts. But the worst of the three,
 and fortunately therefore the first to go,
 is wrong view. This is the really dangerous one.
 It's the one that leads us to be born in hell,
 and worse. It's the one that leads us to be stuck
 in samsara. It's what causes us to be born again and again
 in unfortunate states. Because having perceptions and
 thoughts,
 these can change. You know, you can override them with
 right view,
 but wrong view is what breeds wrong thought and wrong
 perception, right?
 If you believe, like I know many people believe that
 beauty is a good thing, and that's probably not something
 that I'm going to
 convince many people, even in this audience,
 against. But that view is going to lead to desire. It's
 going to lead you to be
 attracted to beautiful things. You think, "Well, what's
 wrong with that?"
 This we would consider. It's not that there's no
 condemnation involved.
 Let's just consider that that is going to perpetuate the
 system, and you're
 going to get caught up, and you're going to be really born
 again.
 And you're going to have to deal with all the same things
 that you've dealt
 with in this life. If you have really intense desires,
 coarse desires, as desires tend to become coarser and co
arser,
 and the more you become addicted to them, then there's a
 potential for real
 real delusion and
 well, problem in the future. Addiction, really.
 Potentially suffering is the idea.
 But I mean more clearly, if you have the wrong view that
 says killing is good,
 stealing is good, you know, I believe if you believe that
 killing is right in
 certain instances, then you're much more likely to do
 those sorts of activities. You're much more likely to kill,
 and encourage
 killing, get caught up in killing.
 So that's where right view fits in. If you want an
 understanding, right view is
 your opinion, your outlook. When you take a thought and you
 affirm it in your mind, there's an affirmation, "Yes, that
 is true."
 So it's the difference between a thought. The difference
 between a thought and a
 view is this. A view is something, a view is the
 affirmation of a thought, the agreeing with it. The thought
 can
 come up and you may not agree with it. You may say, "No, no
, that's a silly
 thought. That's a wrong thought." And if you're
 mindful of that, it has no effect on you because you can't
 again control the
 thoughts. But if you say, "Yes, that thought is correct,"
 and it turns out that that thought is not correct, like you
 think
 "beautiful things are going to satisfy me," and you say, "
Yes, that is true,"
 guess what? You've developed wrong view according to
 Buddhism.
 And it's not wrong, and wrong isn't for any of these things
, isn't because
 the Buddha said they're wrong. It isn't because we won't
 like you because
 you keep those views. It's nothing to do with that. Again,
 there's no condemnation. It's wrong in the most objective
 sense. The
 claim is made that it's not true. Wrong means untrue, means
 false.
 So that we can contest that, you can say, "Well, beauty
 actually does
 lead to contentment." Well, okay, that's your view.
 We would say it's wrong view because we would argue that it
's false.
 It's not true that beautiful things can lead to contentment
.
 They don't lead to contentment. That's the contention.
 For example, people say that killing is right.
 Killing, say killing for food. A lot of people have this
 view that
 there was a thing a while back, Mark Zuckerberg decided
 that he's going to
 kill all the animals that he eats. If he eats meat, he's
 going to kill the animals.
 As he said, "If you eat meat, you should kill it."
 That we would say is... Well, there's some wrong view
 involved there.
 I mean, not necessarily, but if he has the view that
 killing in that case is okay,
 which maybe he doesn't even. He might feel really bad about
 it, but can't
 overcome his desire for meat, which is pretty tragic.
 But if you have the view that, many people do have this
 view that
 killing is proper. It's proper to kill. The First Nations
 people in our country,
 as you say, Native Americans,
 they believe that it's part of the cycle.
 You know, when deer are playing the part of the hunted, and
 it's our part of the
 play. I don't know exactly, actually. I'm kind of just
 paraphrasing,
 but they have the sense that hunting and killing is a part
 of the master plan,
 and therefore is a good thing.
 So, these kinds of wrong views. But the point to make in
 general is that
 wrong view is just a view that is untrue, that goes against
 reality. So again, we all have claims that we make about
 reality.
 You can argue that my claims or the Buddha's claims are
 wrong
 or false. The Buddha didn't know what he was talking about,
 but these are the
 claims that we make. So that's generally what right view is
.
 Right view is that view of things that is in line
 perfectly with reality, whatever that may be. Now, of
 course,
 there's argument over what that is, but that's the general
 definition.
 So specifically in Buddhism, what does Buddhism say is
 right view?
 And we can first of all separate it into two categories
 that are still related, but one category is more of a, as
 it relates
 to folk Buddhism or conventional reality.
 And so this is right view in terms of cosmology, in terms
 of
 living one's life, in terms of a life path, which is very
 important.
 It's not to discount the importance of it, but it has to be
 understood that this
 is only conceptual, conventional. And so this is right view
 about things
 like karma. And so again, it's just a claim
 being made that karma is true. Karma really happens.
 You can argue with the claim, you can throw it out.
 But that's the claim. The Buddha said there is the there is
 the deed, there is
 the result of the deed. This is right view. A person who
 says there is no
 deed, there is no result of a deed, fruit of the deed. This
 is wrong view.
 A person who says there is no mother, there is no father,
 in the sense that people who have done good things for you
 don't deserve your respect, don't deserve your gratitude.
 So that there is no mother, father means forget about your
 parents.
 And by extension, anyone else who's helped you, if anyone's
 done good things
 for you, don't worry about it. This is wrong view.
 Conventionally speaking, that's a bad thing. And it's not,
 you know, it's just using conventions to talk about, but in
 ultimate reality, if
 you're not grateful to those people who have helped you,
 in ultimate reality, that's really, there's going to be a
 lot of bad ultimate
 reality coming your way.
 I mean, it's a lot of suffering, to put it more accurately.
 And another part of it is rebirth, which of course goes
 hand in hand with
 karma. People, when they think of karma, they usually think
 of past life karma.
 Even though karma can take effect even in this life,
 it very often only, or is most pronounced in how it affects
 our next
 life. So in this life you can, due to good karma of being
 born as a
 human, for example, you can avoid the results of bad karma.
 You can kill and
 steal and lie and cheat and get away with it.
 But when you die, you don't have any of that protection of
 the physical body, or
 you're at the mercy of your thoughts, you're at the mercy
 of your attachments.
 And so
 our rebirth is very much influenced, very directly
 influenced,
 by our past karma. So rebirth and karma are generally
 connected. It's how we explain the different realms, and
 how one is born
 in the different realms, which is of course one of the big
 questions for
 the just people have. People who do believe in an afterlife
.
 Most deists or theists, well the theists I guess, will say
 that
 God is the one who decides who goes where.
 On Buddhism we say that your your deeds or your habits, you
 really in the end of
 things, decide where you go. The moment of death, all your
 good and
 bad deeds will present themselves, or many of them will
 present themselves, and if
 you cling to one strong enough, it will lead you to be
 reborn.
 And then all the other factors and your habits will come
 into play, and
 you'll form the new existence, either in the womb, or you
'll form it
 by being born fully formed in one of the ethereal realms,
 like the heaven realm,
 or the hell realm, or as a brahma, or whatever, or as a
 ghost, that kind of thing.
 So this is right view. It's important, obviously, because
 of how it affects your life. It's hard to meditate if you
're
 caught up in wrong view. If you believe drinking alcohol is
 a really good thing,
 well, really hard to meditate if you're into drinking lots
 of alcohol.
 If you're killing and stealing and lying and cheating, well
, hard to keep your
 mind calm, if not impossible. So, yeah, knowledge about
 karma, knowledge
 about rebirth tends to be useful in terms of reminding
 you of the importance of meditation. Of course, it extends
 the whole idea of
 karma, but practically speaking, a lot of people
 discard the idea of rebirth. They say the Buddha didn't
 teach it,
 which is a bit ridiculous, but they say things like that.
 They say how Buddhism
 is just about the present life.
 And there isn't, to some extent, that's true. I mean, the
 Buddha did say we
 should focus on the present moment, not the future, not the
 past, but
 conventionally speaking, it's useful to be reminded of the
 fact that
 life is short, life has an end to it, that life is
 uncertain, but death is certain.
 It helps remind us, it helps focus us, it helps push us to
 meditate.
 So that's mundane right view, sort of the conventional. Now
, in an ultimate sense,
 right view can also be can further be subdivided into two
 categories.
 There's mundane and super mundane. So mundane right view in
 this context, or
 let's talk about in general, when we talk about
 ultimate reality or right view of ultimate reality, we're
 talking about
 right view of self, or we're talking about right view of
 the nature of reality.
 So it's much more about existence.
 I think the word is epistemology. I can't remember, there's
 all these words.
 Epistemology, is that what exists, or is it ontology?
 There's these two words.
 Ontology is
 oh no it is ontology. Right, yeah epistemology is about
 knowing
 right? That's the other one.
 So
 Buddhist ontology.
 Yeah, well the idea of understanding what exists,
 this is right view in an ultimate sense. And so this is
 much more related to meditation. You could argue that karma
 is something
 that you learn through meditation as well. I think that's
 an important point
 to make. But this one is more core,
 and it's more important, I guess you could say.
 It's more important, and this is the right view that leads
 to
 enlightenment. But it's on two levels. So the first right
 view is theoretical.
 It's something that you have to learn so people will tell
 you
 about the nature of reality,
 and intellectually you'll start to appreciate it.
 Furthermore as you start to meditate you'll start to see it
,
 and then intellectually you'll say oh yes there's only this
.
 This is the way things are. It's all that which contributes
 to your
 outlook. So our ordinary outlook is in terms of
 things. Like if you pull yourself out of Second Life
 and look around the room, you'll see you should see a
 computer monitor
 or monitor of your laptop, your notebook, laptop, computer.
 You'll probably see a keyboard. If you look down at
 yourself you'll probably see your
 hands and your feet. You'll see things, entities. You'll
 see a
 mouse on your desk,
 walls, lights, whatever's in your room. Maybe you'll see
 the teapot that you
 drink out of, whatever's in your room.
 But none of these things in Buddhism are recognized as
 actually existing.
 So this conventional way of looking at reality
 is actually an illusion. And so right view is coming to see
 that in an ultimate
 sense what exists is not entities but
 experiences. And this takes a little while to learn,
 but intellectually it's important because if you start med
itating
 thinking about things you're already looking at reality
 from
 the wrong point of view. Like when you walk,
 if you think of it as I'm walking, a person walking,
 you're missing what's really going on because what's really
 happening is the
 experiences, the feelings, and of course the thoughts and
 the
 reactions.
 But these things are only experiences. And when you sit and
 the stomach rises if
 you're thinking of yourself breathing or if you think of
 the breath coming into
 your body and the breath going out of your body these are
 just concepts.
 The reality is just the experiences, just the feelings.
 So this is important when a person begins to meditate, this
 is the
 first, actually the first step is attaining right view.
 But it's right view in this this mundane sense
 that has a lot more to do with the intellect than to do
 with,
 than has to do with actual experience. Some experience, but
 it's an experience
 that leads to this intellectual understanding that oh yeah
 these entities are just conceptual. What really exists is
 body and mind really or experience.
 And this continues throughout your practice. As you
 practice it becomes
 strengthened and refined and you start to understand
 suffering, you start to
 understand impermanence, you start to understand non-self.
 And so your intellectual outlook is improved
 by the practice. The more you practice, the more you see,
 and until intellectually you're very very clear
 on the nature of reality.
 And this is where there arises super mundane right view.
 And this is noble right view. If you talk about the noble
 eightfold path,
 the noble eightfold path only actually occurs
 in the moment. But it's that moment when your intellectual
 right view becomes a
 certainty, where it really becomes your view.
 Like you get it. Up until that point you're seeing it
 and you're building a hypothesis, but there comes a moment.
 You see because you'll be meditating through actual
 meditation, you'll
 see the same things over and over again, you'll see the
 same things happening,
 you'll see the same characteristics in everything you
 experience, everything
 impermanent, everything unsatisfying, everything
 uncontrollable. And it'll come again and again until
 finally one thing will present itself. It'll be the last
 thing, it's nothing
 special, but one thing will present itself and will be so
 clearly impermanent
 or clearly unsatisfying or clearly uncontrollable
 that you'll just get it. That's the moment where the mind
 switches, where the
 mind lets go. It's like turning a light switch. And
 whether you want to say turning on a light in terms of
 becoming enlightened
 or turning off a light in terms of the cessation of
 suffering,
 describe it as you will, but it's where the mind is becomes
 freed
 from samsara. That moment is, has all of the eight-fold
 path factors,
 including right view. I mean most importantly right view.
 Everything else is just as a result of this right view that
 lets you see things as there.
 So that's a brief synopsis of right view.
 I was going to go into the practices that lead us to
 cultivate
 right view, but on the other hand I've been talking for
 half an hour and we could also just, if anyone has any
 questions or wants to talk, questions about buddhism,
 questions about
 right view, questions about meditation,
 we could just do that instead because I've talked, I've
 given several talks on
 how to cultivate right view. I think I've done at least one
 on YouTube.
 If I haven't, I mean I'm happy to talk about it.
 I'll put it to you. If anyone has questions go ahead and
 ask them.
 If not, I guess I'll just keep talking.
 Or if not, then let me know that you'd like me to keep
 talking
 and that you're not all away from the keyboard or
 of chatting on Facebook instead of listening.
 Okay, well that first part of the talk's over so
 I can give another talk.
 I'll give the rest of the talk.
 All right, so there are five things in the texts which lead
 to right view.
 This is not my list, it's a list that occurs in the
 tepidaka, I think, and guttarnikaya, Book of Fives, if I'm
 not mistaken.
 And these are in order sutta, sila, sorry,
 sila, sutta, sila, sutta, sagachha, samatha, and vipassana.
 These are the five things that lead to right view.
 So sila means morality.
 The cultivation of morality comes right view.
 And this I already sort of hinted at at least that
 without proper morality you can't really see clearly.
 When a person is immoral their mind is on, is a flame with
 guilt,
 remorse, fear, anxiety, paranoia, delusion,
 just all around messed up. That's why we call it,
 that's why in buddhism we consider it immoral, not for any
 other reason.
 It's immoral because it messes you up, which is kind of
 perverse. I think many
 people look at that and say that's weird. Why doesn't buddh
ism care about what
 happens to the people you hurt, right? Because buddhism has
 the idea that,
 or the claim that, you can't hurt another person.
 If I steal from you, you're only hurt because you get
 upset about it. And so in an ultimate sense
 we hurt ourselves with our evil deeds more than we hurt
 other people.
 So if you hurt an enlightened being you don't hurt them at
 all.
 You can't possibly quote-unquote hurt them.
 You can beat their bodies or you can take away their
 possessions,
 do whatever you want. You can't actually hurt them.
 But you can hurt yourself and you do hurt yourself.
 Which is the interesting thing, as not only
 are they wrong, unwholesome deeds, not only are they wrong,
 but they
 prevent you from seeing how wrong they are.
 It's very dangerous, scary really, because you think
 otherwise, well I can do them
 and if they're wrong I'll know it, right? I'll be able to
 see if it's wrong.
 Yeah, not always, not necessarily, because they
 preoccupy you. If you're preoccupied with guilt or anger,
 arrogance, concede, all the things that come from doing,
 from cultivating deeds based on these emotions. You
 cultivate the deeds based
 on a certain emotion and the emotion increases.
 When those emotions are in the mind they don't foster
 objectivity.
 So when you keep morality, when you cultivate morality, it
 helps you cultivate
 right view, it lets you see clearly. You start to see,
 when you give up alcohol, you start to see what alcohol is
 done to you, because
 now you're sober and your mind is clearer.
 When you stop killing, stop stealing, lying, cheating,
 when you're mindful of these things, you start to see
 more clearly and you'll start to see cause and effect.
 Mostly because your mind is clear, your mind is calm.
 So that's the first important aspect, the idea that without
 ethics it's very hard to see things clearly.
 The second factor that promotes the cultivation of right
 view,
 sutta means listening or hearing. So in the time of the
 Buddha there wasn't,
 they didn't write religious teachings down as far as we're
 aware of. So it was all transmitted orally,
 enchanted and memorized and so on. But what it means,
 this just means hearing from someone, a learning from
 someone else,
 listening to teachings like this. When you listen to
 teachings about right
 view, it helps cultivate right view. It's very important.
 Now there are some people who can learn the truth without
 hearing it.
 These people are very few and far between. There's far more
 people who
 think they can cultivate, they can become enlightened, let
's put it in general words,
 without a teacher than actually can. That's an important
 point that we should
 think about. It's far more common to be a person who
 thinks they can find the truth themselves
 than to actually be someone who can find the truth for
 themselves.
 That's the claim we make. It's very very difficult to do.
 You can try, not as easy as we often think. We can be
 fairly
 arrogant as human beings. I've met people who, you know,
 with the
 best of intentions and well-meaning and not
 bad people by any means, but have deluded themselves into
 thinking that
 they're going to find the truth on their own.
 They're on their own path, quote unquote.
 So yeah, suta, much easier if you find someone who has
 right view.
 Of course the problem is how do you know whether they have
 right view? You're not
 yet having it, but to listen. And the idea being that
 even if you don't know who has right view, the more you
 listen,
 the more you'll be able to compare and contrast and
 put together, piece together the truth, that it actually
 will help
 you. Now, it could confuse you. There could be an
 argument made that if you study too much you can actually
 cultivate wrong views.
 But I think you could argue that, but
 that would be sort of an extreme case. It happens. You know
, people who
 study too much do exist. But for the most part,
 that's not what is meant here. It's not meant that
 we're not referring to study, study, study. We're just
 saying
 learning what is right. When you hear good teachings, for
 the most part, they're
 beneficial. I mean, I guess you just have to say that
 it's not a sure case. Just hearing the truth doesn't mean
 you're going to get
 it. But not having heard the truth is a much
 better chance. There's a much better chance of you not
 getting it.
 So listening is an important part with the
 warning that, you know, don't spend all your time listening
 to the
 Dhamma. Because once you've heard sort of the
 basics, that's really enough. So the point is just to hear
 the truth.
 The third factor, Sagat-cha, means
 conversation, dialogue. So sutta means is one way, it's
 passive.
 Like right now, you're listening to me. Sagat-cha is
 actually
 asking questions or asking for clarification or
 getting into dialogue with someone else, getting into
 dialogue with
 other meditators, talking with teachers and other medit
ators, talking with wise
 people. But this is, it involves talking,
 expressing ourselves. And you could probably argue that
 part of it is just
 being able to formulate our thoughts into words,
 our feelings, our mind states, the way we do for
 when we see a therapist, right? Often therapists' only role
 is to listen.
 And that's enough to allow us to express ourselves
 and sort of organize our thoughts. But I think more often
 it refers to,
 I mean it refers to this, but probably the most common form
 is asking
 questions. And so there's this important idea
 that if you don't ever ask questions, if you don't ever
 ask the questions that you have or ask questions that
 express your doubts,
 that'd be very difficult for you to, or you're not,
 you're going to be harder for you to find answers to them.
 Not impossible, but
 asking questions and getting answers or
 describing your practice to a teacher and asking whether
 you're on the right
 track. That kind of thing can be useful. Describing your
 practice and having it
 critiqued. Like there's the story of Anuruddha
 who came to Sariputta and he was concerned because he said
 I can, my mind is perfectly focused and I can be aware of
 the whole of the
 solar system or the whole of the, you know, the
 whole of the galaxy, maybe the universe, who knows,
 at once. And yet I'm still not able to free myself
 and become an enlightened being.
 And Sariputta said to him, well the fact that you say that
 your mind is
 perfectly concentrated, that's your conceit.
 And the fact that you, or that you say that you
 can observe the whole of the galaxy or the solar system at
 once,
 that's your distraction. And that you say you haven't yet
 attained
 enlightenment, that's your worry. And he said if you do
 away with these
 three things you'll be able to become free from suffering.
 So that's a good example of Sagat Chah.
 Sariputta gave him a very good, you know, redirected him
 very clearly
 in that example.
 So that's Sagat Chah. When we do meditation courses here,
 the meditators will have to meet with the teacher once a
 day.
 Every meditator will meet with the teacher once a day and
 it's not only listening to me talk but asking questions and
 discussing your
 practice and dialogue. And now we're doing this over the
 internet. Some people are taking internet courses with me,
 meeting once a week.
 We have slots for that if anyone's interested.
 Go to our meditation site, meditation.siri-mongolo.org
 and you'll see the meet page. Here you can meet with a
 teacher.
 That's Sagat Chah. Number four, Samatha. Samatha means
 tranquility.
 So it's important that we calm the mind. Our mind has to be
 calm before we can
 understand reality. If you practice
 intense calm,
 it can lead to the jhanas and so that's a kind of jhana sam
adhi.
 It leads to right view in regards to samatha. But more
 importantly is when
 you practice meditation, your mind settles down.
 Not at first, but eventually your mind starts to settle
 down. Your mind at least
 becomes more focused and by being more focused you're able
 to see things clearly.
 So anything you do that calms you down, if you practice
 loving kindness, if you
 practice samatha meditation specifically, this kind of
 thing
 calms you down and that allows you to see some of the
 problems in your mind, some of the bad habits.
 Number five is vipassana. So the cultivation of insight.
 And this is more
 along the lines of what I was talking about earlier when I
 talked about the cultivation of right view. How right view
 progresses in seeing the three characteristics. So vipass
ana means to
 see clearly. It means to see clearly impermanent suffering
 and non-self or
 impermanence, unsatisfactoriness and
 uncontrollability. That things are impermanent, unsatisf
ying and
 uncontrollable. And as you see that clearer and clearer and
 clearer, this is what leads to Arya samaditi, noble right
 view.
 When at the moment that you see something as impermanent or
 as suffering
 or as non-self, in that moment the mind lets go.
 That sees perfectly clearly and that's enough to cause the
 mind to let go.
 So there you go. That briefly is
 sort of a broad general talk about the concept of right
 view.
 Are there any questions?
 Simon has a question.
 Start by setting their minds on that which is beautiful.
 Going through all the
 jhanas and finally letting go of the jhanas as they too
 mostly let go of.
 This form of beauty if you recall the sutta.
 I do recall. Kalayana is probably the word.
 Let me see if I can find it. Which one is that? Mn?
 I guess. Mn? What's the number?
 How did somebody set my cloth?
 Weird.
 Anyone? Anyone? What's the number?
 137 okay.
 Mm-hmm.
 I don't find... let me see.
 Yeah well, Tanisaro has his own ideas about how to
 translate things.
 Near the end of the sutta, the beginning of the sutta, I'm
 just looking at the
 Pali. Let's look at the English. Bhikubodi's translation.
 Okay. Possessed of material forms he has resolved only upon
 the beautiful.
 Possessed of material forms. I see his forms. Not pursuing
 forms internally.
 He forms internal externally.
 764.
 Ayamududdhi Adisa Subantu.
 Adimuddhoati. Subantu. What the heck is that?
 Subantu. I don't think it means what they're saying it
 means.
 Adidisa. I think it's from an earlier suta actually.
 Oh wait.
 Hmm.
 I don't know what subantu means. Subantu eva. Subantu eva.
 Subantu.
 Yeah. I mean I've read this before but never give it much
 thought.
 Bhikubodi gives a note that it has something to do
 with the eight liberations.
 Which I'm assuming means the
 eight jhanas.
 Yeah. Not exactly though. Any other perception or non-per
ception is the
 seventh. So then the eighth is the cessation of
 Sunya-Vedanyani-Rhoda.
 [Music]
 [Music]
 So some of it's been already explained. So let's look up
 this word
 subantu.
 Subantu.
 Tarukandhasuddha.
 Yeah. Suba. Suba asubang.
 Oh asubang I think. It's asubang. It just means subang.
 And some kind of weird sundae. I don't know.
 Okay. So beautiful. That's interesting. Why the
 proceeds resolved only upon the beautiful suba.
 That's really interesting.
 Something to study. Something to look up. I don't actually
 know what's going on
 here with these eight.
 Or how the eight. How the eight. So Bhikubodi says look at
 note
 764. If you got his version of the Majimini
 Kaya which I don't have in front of me.
 I don't know where to find note 764 which sutta that is in.
 But if you look up his middle length discourses it talks
 about the eight
 directions. I guess talks about the eight
 liberations. It seems to me the eight. Yeah well
 something to look up.
 Okay what else do we got here? Someone else with a question
?
 Kanti dana. Kanti dana. Kanti dana maybe.
 What does kanti dana mean?
 Kanti dana. The gift of patience.
 Kanti dana. One who gives
 dana. One who has been given to patience.
 One to whom patience has been given as a gift.
 One to whom patience has been given as a gift. Maybe that's
 what it means.
 If it has taken a person so long to cultivate an even
 interest in finding
 the truth how can we avoid starting all over being
 enchanted in the worldly
 world in our one's next life? Will we keep whatever right
 view? Well we
 go by our habits so if you're cultivating good habits in
 this life
 that's going to keep you closer to Buddhism. If you
 cultivate bad habits
 those bad habits are going to tear you away from it.
 It's really the only way.
 Is that Robin? Is Robin calling herself Kanti dana? Oh hi
 Robin.
 [Silence]
 So yeah.
 We have no reassurance. There's no certainty but the reass
urance is there.
 That cultivation of good things and especially the
 cultivation of Buddhist
 good things keeps you involved with Buddhism.
 Just doing good deeds in general without any relationship
 to Buddhism is
 not as likely to keep you tied to Buddhism but it's still
 likely to keep
 you tied to good people and you know Buddhists have
 goodness because that's what we cultivate.
 Oops that wasn't the right button.
 Here that's right. What we do right now in this moment that
 matters.
 Right. What we do right now in this moment.
 It's the best way to look at things.
 Okay well if that's all then I'm going to say
 goodbye to everyone. Wish you all a good day. Thank you all
 for coming.
 Hope to see you all next week.
 [Silence]
 [Silence]
 [Silence]
 [Silence]
 [Silence]
 [Silence]
