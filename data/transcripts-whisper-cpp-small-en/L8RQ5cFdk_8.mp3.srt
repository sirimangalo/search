1
00:00:00,000 --> 00:00:05,370
 Okay, so welcome everyone to this week's episode of Monk

2
00:00:05,370 --> 00:00:07,000
 Radio. Time for announcements.

3
00:00:07,000 --> 00:00:16,820
 I've got a couple of announcements. One, that I hit two

4
00:00:16,820 --> 00:00:19,000
 million views on YouTube,

5
00:00:19,000 --> 00:00:25,650
 which is an interesting thing to think about, because it

6
00:00:25,650 --> 00:00:28,000
 really isn't that big of a number.

7
00:00:29,000 --> 00:00:33,330
 I think there are videos on YouTube with like 10 million

8
00:00:33,330 --> 00:00:35,000
 views, one video.

9
00:00:35,000 --> 00:00:40,000
 So it really doesn't have that much significance.

10
00:00:40,000 --> 00:00:45,990
 But on the other hand, if you think of two million, I don't

11
00:00:45,990 --> 00:00:48,000
 know, two million video views,

12
00:00:48,000 --> 00:00:51,360
 I mean it says something. It says that people are

13
00:00:51,360 --> 00:00:54,000
 interested in Buddhism, interested in meditation,

14
00:00:55,000 --> 00:01:00,650
 and interested in pornography and masturbation, because

15
00:01:00,650 --> 00:01:03,000
 that's one of my most famous videos.

16
00:01:03,000 --> 00:01:06,000
 It's kind of sad and depressing in a way.

17
00:01:06,000 --> 00:01:10,360
 But the number one video is about meditation. It's not the

18
00:01:10,360 --> 00:01:11,000
 number one video.

19
00:01:11,000 --> 00:01:17,000
 It's the number two video. So, yeah.

20
00:01:18,000 --> 00:01:23,090
 I mean, it's good to laugh about such things. We take them

21
00:01:23,090 --> 00:01:26,000
 too seriously and that causes a lot of the problems that

22
00:01:26,000 --> 00:01:27,000
 they are blamed for.

23
00:01:27,000 --> 00:01:31,340
 It's our stress and our feelings of guilt that cause most

24
00:01:31,340 --> 00:01:33,000
 of the suffering.

25
00:01:33,000 --> 00:01:36,700
 When we overcome that, the problems themselves are not that

26
00:01:36,700 --> 00:01:38,000
 insurmountable.

27
00:01:39,000 --> 00:01:43,520
 I suppose they are difficult. I mean, Buddha said sens

28
00:01:43,520 --> 00:01:47,000
uality is the greatest bind that there is.

29
00:01:47,000 --> 00:01:53,330
 Ropes and thongs and chains are weak in comparison to the

30
00:01:53,330 --> 00:01:56,000
 bond of sensuality.

31
00:01:56,000 --> 00:01:59,720
 So it's not something that you should take lightly for sure

32
00:01:59,720 --> 00:02:00,000
.

33
00:02:00,000 --> 00:02:03,850
 But it's not something you should feel guilty and hit

34
00:02:03,850 --> 00:02:06,000
 yourself over, either.

35
00:02:08,000 --> 00:02:12,000
 So, okay, anyway, on track that was two million views.

36
00:02:12,000 --> 00:02:15,000
 So here's to another two million.

37
00:02:15,000 --> 00:02:20,240
 And hopefully with everyone's help here, we'll have some

38
00:02:20,240 --> 00:02:25,000
 good videos to start us off in that direction this week.

39
00:02:25,000 --> 00:02:33,000
 Another announcement. I mentioned it yesterday, I think,

40
00:02:33,000 --> 00:02:37,000
 that we have a candidate for ordination.

41
00:02:37,000 --> 00:02:41,000
 And the big announcement today, different from yesterday,

42
00:02:41,000 --> 00:02:45,000
 is that he's going to be flying back to Finland,

43
00:02:45,000 --> 00:02:50,580
 probably flying back to Finland in the very near future in

44
00:02:50,580 --> 00:02:55,000
 order to ordain quicker than we thought.

45
00:02:55,000 --> 00:02:59,810
 So possibly ordain with me here in Sri Lanka before I leave

46
00:02:59,810 --> 00:03:03,500
 for Thailand and then go with me to Thailand, which would

47
00:03:03,500 --> 00:03:04,000
 be great.

48
00:03:05,000 --> 00:03:08,050
 It would be great to have a companion. And I think there's

49
00:03:08,050 --> 00:03:10,000
 a Sri Lankan monk joining us as well,

50
00:03:10,000 --> 00:03:13,430
 who wants to go and meditate in the forests of Thailand,

51
00:03:13,430 --> 00:03:15,000
 which would be great.

52
00:03:15,000 --> 00:03:18,000
 I'm wondering why he doesn't meditate in the forest here,

53
00:03:18,000 --> 00:03:22,190
 but I think it's hard to meditate in your own country

54
00:03:22,190 --> 00:03:23,000
 sometimes

55
00:03:23,000 --> 00:03:28,000
 because too many relatives and friends and so on.

56
00:03:28,000 --> 00:03:31,130
 When you go to a foreign country, you have to take it more

57
00:03:31,130 --> 00:03:32,000
 seriously.

58
00:03:34,000 --> 00:03:40,700
 And the other man who's staying here, George, is leaving

59
00:03:40,700 --> 00:03:42,000
 soon.

60
00:03:42,000 --> 00:03:46,760
 His grandmother died and he has to go back for... well, he

61
00:03:46,760 --> 00:03:50,430
 was only going to stay till Friday anyway, but he could be

62
00:03:50,430 --> 00:03:51,000
 going back early.

63
00:03:51,000 --> 00:03:54,480
 But the reason I mention him is because he's an interesting

64
00:03:54,480 --> 00:03:55,000
 case.

65
00:03:55,000 --> 00:03:57,850
 Everyone's running away, having very much difficulty

66
00:03:57,850 --> 00:03:59,000
 staying in the forest.

67
00:04:00,000 --> 00:04:03,500
 He was one of those people, men mainly actually, the women

68
00:04:03,500 --> 00:04:05,000
 don't have such troubles.

69
00:04:05,000 --> 00:04:08,720
 So last week he was quite ready to leave and then he

70
00:04:08,720 --> 00:04:12,590
 suddenly had a breakthrough and now he doesn't want to

71
00:04:12,590 --> 00:04:13,000
 leave.

72
00:04:13,000 --> 00:04:16,930
 So he was asking me just now before I started, "What's he

73
00:04:16,930 --> 00:04:20,730
 going to do? He's not going to be able to finish the course

74
00:04:20,730 --> 00:04:22,000
. How's he going to continue?"

75
00:04:23,000 --> 00:04:26,710
 And I said, "Well, just come back. We can talk over Skype

76
00:04:26,710 --> 00:04:30,000
 or on the internet somehow and get practicing."

77
00:04:30,000 --> 00:04:32,290
 But I said, "Now you've made it. You made it over the

78
00:04:32,290 --> 00:04:35,000
 biggest hurdle, which is the one thing to leave hurdle."

79
00:04:35,000 --> 00:04:40,500
 Once you get over that and really appreciate the meditation

80
00:04:40,500 --> 00:04:43,000
 for what it is, then nothing will stop you.

81
00:04:43,000 --> 00:04:47,840
 Nothing can stand in your way. You just have to, as long as

82
00:04:47,840 --> 00:04:52,000
 you can continue practicing, you've faced the worst of it.

83
00:04:53,000 --> 00:04:57,000
 So those are two announcements, or a few announcements.

84
00:04:57,000 --> 00:05:05,850
 Apart from that, everything is continuing. Sankh Sara is

85
00:05:05,850 --> 00:05:08,000
 still here.

86
00:05:08,000 --> 00:05:11,450
 Anybody have anything they want to say? Any announcements

87
00:05:11,450 --> 00:05:14,000
 that they have that they'd like to share?

88
00:05:14,000 --> 00:05:17,000
 Anybody on our panel?

89
00:05:19,000 --> 00:05:23,000
 Paulina is still planning to join us, I think, in October.

90
00:05:23,000 --> 00:05:25,000
 She may be chasing her mind.

91
00:05:25,000 --> 00:05:28,000
 So there's an announcement. We might have a new...

92
00:05:28,000 --> 00:05:30,000
 Wonderful.

93
00:05:30,000 --> 00:05:34,160
 Ah, yes, that's right. She's already made... Sorry, I'm bad

94
00:05:34,160 --> 00:05:35,000
 memory.

95
00:05:35,000 --> 00:05:39,000
 She's already dedicating herself to it. She's great.

96
00:05:39,000 --> 00:05:43,000
 Marion also, there's another person who was here yesterday.

97
00:05:43,000 --> 00:05:44,000
 She's on her retreat today.

98
00:05:44,000 --> 00:05:47,400
 That's why she's not joining us, and her plan is to come as

99
00:05:47,400 --> 00:05:48,000
 well.

100
00:05:49,000 --> 00:05:51,950
 So lots of good things happening. There's a couple of men

101
00:05:51,950 --> 00:05:53,000
 who...

102
00:05:53,000 --> 00:05:57,080
 Well, we have Laurie, who I mentioned already. We've got

103
00:05:57,080 --> 00:06:01,000
 other people who have expressed interest in ordaining,

104
00:06:01,000 --> 00:06:04,040
 and people who just want to come to meditate in Thailand.

105
00:06:04,040 --> 00:06:06,000
 So lots of good things happening.

106
00:06:06,000 --> 00:06:10,480
 Meechi Jai is here, the Burmese nun. And she said to me,

107
00:06:10,480 --> 00:06:13,000
 she really likes this place.

108
00:06:14,000 --> 00:06:18,990
 She agrees with me on many of the aspects of how great a

109
00:06:18,990 --> 00:06:22,000
 country it is to live in as a monastic.

110
00:06:22,000 --> 00:06:25,220
 And so I think her plan is to come back and bring some

111
00:06:25,220 --> 00:06:28,000
 friends. She has some supporters in Chiang Mai,

112
00:06:28,000 --> 00:06:32,000
 who she wants to bring as well, who want to come.

113
00:06:32,000 --> 00:06:36,940
 So lots of good stuff happening. Just continuing on with

114
00:06:36,940 --> 00:06:39,000
 our work and practice.

115
00:06:39,000 --> 00:06:43,000
 So that's announcements for this week.

