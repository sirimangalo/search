1
00:00:00,000 --> 00:00:04,060
 What is your view on having sex as a Buddhist? Are you, for

2
00:00:04,060 --> 00:00:07,000
 example, allowed to have multiple sexual relationships?

3
00:00:07,000 --> 00:00:10,600
 I put the emphasis wrong. I'm not allowed to have any

4
00:00:10,600 --> 00:00:13,850
 sexual relationships, but "are you" means in general, right

5
00:00:13,850 --> 00:00:14,000
?

6
00:00:14,000 --> 00:00:23,440
 So in general, a lay person, put it in general because sens

7
00:00:23,440 --> 00:00:28,000
ual desire is evil. It's an evil.

8
00:00:28,000 --> 00:00:33,420
 This is Buddhist word evil, no? Because the meaning of the

9
00:00:33,420 --> 00:00:36,000
 word evil is something that is going to lead you to suffer.

10
00:00:36,000 --> 00:00:40,080
 Now, the core of Buddhism is that desire leads you to

11
00:00:40,080 --> 00:00:41,000
 suffer.

12
00:00:41,000 --> 00:00:46,510
 Wanting something leads you to try to get it. When you try

13
00:00:46,510 --> 00:00:51,000
 to get it, it creates... it changes your life.

14
00:00:51,000 --> 00:00:55,130
 We want to get something, so you have to work for it, and

15
00:00:55,130 --> 00:00:59,000
 that changes your whole universe in some small way.

16
00:00:59,000 --> 00:01:02,490
 And that's going to create more stress. It's going to

17
00:01:02,490 --> 00:01:07,000
 create more dissatisfaction and suffering.

18
00:01:07,000 --> 00:01:11,790
 So undeniably, it's a bad thing. But the question is, how

19
00:01:11,790 --> 00:01:13,780
 bad? Is it really going to get in the way of your

20
00:01:13,780 --> 00:01:15,000
 meditation practice?

21
00:01:15,000 --> 00:01:19,950
 No, probably not. Excessive sex or multiple sexual

22
00:01:19,950 --> 00:01:23,000
 relationships can get in you.

23
00:01:23,000 --> 00:01:26,440
 Whether it's with one person or with many people, in fact,

24
00:01:26,440 --> 00:01:30,000
 many people would be more deleterious for your practice.

25
00:01:30,000 --> 00:01:33,950
 It's not a horrible thing in Buddhism to have multiple

26
00:01:33,950 --> 00:01:36,000
 sexual relationships.

27
00:01:36,000 --> 00:01:41,580
 The problem comes where you are deceiving people. This is

28
00:01:41,580 --> 00:01:49,060
 where it gets really bad, and where it gets really evil

29
00:01:49,060 --> 00:01:51,000
 when you start deceiving people.

30
00:01:51,000 --> 00:01:54,380
 And that's where it really cuts your practice and starts

31
00:01:54,380 --> 00:01:57,000
 heading you in the opposite direction.

32
00:01:57,000 --> 00:02:00,050
 It doesn't just slow you down, it starts heading you in the

33
00:02:00,050 --> 00:02:04,000
 wrong direction when you're sleeping around.

34
00:02:04,000 --> 00:02:08,090
 When you have a monogamous relationship and are committed

35
00:02:08,090 --> 00:02:11,000
 to it and then are cheating on that person,

36
00:02:11,000 --> 00:02:16,710
 or someone else is in a monogamous relationship and you're

37
00:02:16,710 --> 00:02:19,000
 sleeping with them.

38
00:02:19,000 --> 00:02:21,000
 This kind of thing, this is where it really gets trouble.

39
00:02:21,000 --> 00:02:25,040
 If you're in a polygamous relationship, this is not a

40
00:02:25,040 --> 00:02:30,000
 corrupt evil in Buddhism.

41
00:02:30,000 --> 00:02:37,620
 I don't know. I've thought about this a lot. You look at it

42
00:02:37,620 --> 00:02:43,000
, it's really what never was seen as such.

43
00:02:43,000 --> 00:02:47,070
 I don't know where it became, but it's kind of this

44
00:02:47,070 --> 00:02:52,360
 Protestantism that turned it into such a horrible thing to

45
00:02:52,360 --> 00:02:56,000
 have a polygamous relationship.

46
00:02:56,000 --> 00:03:01,300
 If anything, it seems to be what God designed us for. One

47
00:03:01,300 --> 00:03:03,000
 man, many women.

48
00:03:03,000 --> 00:03:08,450
 Look at nature. It's not always, but with monkeys, for

49
00:03:08,450 --> 00:03:13,530
 example, chimpanzees, it's very common to see polygamous

50
00:03:13,530 --> 00:03:15,000
 relationships.

51
00:03:15,000 --> 00:03:17,900
 We see it all the time in the forest. One big monkey and he

52
00:03:17,900 --> 00:03:20,000
's going around and he's got all these women.

53
00:03:20,000 --> 00:03:27,220
 The problem isn't that it's a matter of it's in a category

54
00:03:27,220 --> 00:03:31,150
 of its own. It's not in a category. It's just fairly

55
00:03:31,150 --> 00:03:32,000
 extreme.

56
00:03:32,000 --> 00:03:34,740
 The more sex and the more diverse sex you have, the more it

57
00:03:34,740 --> 00:03:37,000
's going to get in the way of your practice.

58
00:03:37,000 --> 00:03:40,210
 It hasn't crossed the line of being immoral in the sense of

59
00:03:40,210 --> 00:03:44,180
 leading you backwards yet. It's not leading you in the

60
00:03:44,180 --> 00:03:45,000
 wrong.

61
00:03:45,000 --> 00:03:51,000
 It is, but it can still be worked out through meditation.

62
00:03:51,000 --> 00:03:54,160
 If you start cheating on people, if you start lying and

63
00:03:54,160 --> 00:03:55,000
 deceiving people,

64
00:03:55,000 --> 00:03:57,990
 breaking up relationships, that makes your meditation very,

65
00:03:57,990 --> 00:04:00,000
 very difficult in a very different way.

66
00:04:00,000 --> 00:04:04,820
 So that's categorically different. This has to be

67
00:04:04,820 --> 00:04:08,000
 understood. But it has to also be understood.

68
00:04:08,000 --> 00:04:11,000
 I don't want you to say that polygamy is fine. Go for it.

69
00:04:11,000 --> 00:04:13,970
 It's really going to get in your way and it's going to make

70
00:04:13,970 --> 00:04:16,000
 you no sex in general.

71
00:04:16,000 --> 00:04:20,010
 But for sure, promiscuity and polygamy will for sure drive

72
00:04:20,010 --> 00:04:25,390
 you crazy. Your mind will become more and more attached to

73
00:04:25,390 --> 00:04:26,000
 it.

74
00:04:26,000 --> 00:04:28,220
 The best thing is to give it up and start looking at the

75
00:04:28,220 --> 00:04:31,270
 sexual desire. Give it up. Be celibate and start looking at

76
00:04:31,270 --> 00:04:32,000
 the celibate.

77
00:04:32,000 --> 00:04:35,150
 Looking at the desire. If you happen to break your celibacy

78
00:04:35,150 --> 00:04:38,000
, just don't become a monk yet. Start looking at it.

79
00:04:38,000 --> 00:04:41,530
 Try to be celibate for a while. See what happens. Watch the

80
00:04:41,530 --> 00:04:44,000
 sexual urges and really learn about them.

81
00:04:44,000 --> 00:04:47,420
 Take it as a laboratory experiment. It's much more

82
00:04:47,420 --> 00:04:50,610
 conducive. There's no reason to be promiscuous. There's no

83
00:04:50,610 --> 00:04:52,000
 reason to be polygamous.

84
00:04:52,000 --> 00:04:56,060
 It doesn't do you any good. It doesn't make you more happy.

85
00:04:56,060 --> 00:04:59,000
 What good is it? It just makes you less satisfied.

86
00:04:59,000 --> 00:05:01,190
 It doesn't make you a better person. It doesn't make you a

87
00:05:01,190 --> 00:05:04,000
 happier person. It doesn't make you more content.

88
00:05:04,000 --> 00:05:09,000
 What good is it? It's better to go the other direction.

