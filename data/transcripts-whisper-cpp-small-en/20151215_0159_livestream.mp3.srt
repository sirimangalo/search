1
00:00:00,000 --> 00:00:07,000
 Okay, hello everyone.

2
00:00:07,000 --> 00:00:11,400
 I'm broadcasting live.

3
00:00:11,400 --> 00:00:12,400
 Live audio.

4
00:00:12,400 --> 00:00:19,600
 Tonight we have a Dhammapada.

5
00:00:19,600 --> 00:00:22,600
 December 14th is the day.

6
00:00:22,600 --> 00:00:34,600
 Let's make sure.

7
00:00:34,600 --> 00:00:36,600
 I think we'll switch this.

8
00:00:36,600 --> 00:00:37,600
 Test.

9
00:00:37,600 --> 00:00:38,600
 Okay.

10
00:00:38,600 --> 00:00:42,600
 I think...

11
00:00:42,600 --> 00:00:47,600
 I think we're recording.

12
00:00:47,600 --> 00:00:52,600
 Test.

13
00:00:52,600 --> 00:00:55,600
 Excuse me.

14
00:00:55,600 --> 00:00:56,600
 Okay.

15
00:00:56,600 --> 00:01:07,600
 So, ready to go.

16
00:01:07,600 --> 00:01:12,600
 Test.

17
00:01:12,600 --> 00:01:15,600
 Hello and welcome back to our study of the Dhammapada.

18
00:01:15,600 --> 00:01:21,850
 Today we continue on with verse number 121, which reads as

19
00:01:21,850 --> 00:01:23,600
 follows.

20
00:01:23,600 --> 00:01:34,590
 "Mawaman neta pappasa namandang agamisati uda bindunipatena

21
00:01:34,590 --> 00:01:46,810
 uda kambopi purati balo purati pappasa tokang tokampi ajin

22
00:01:46,810 --> 00:01:48,600
am."

23
00:01:48,600 --> 00:02:02,430
 Which means one should not look down, one should not think

24
00:02:02,430 --> 00:02:07,600
 little of evil.

25
00:02:07,600 --> 00:02:10,600
 Thinking, "Nawaman namandang agamisati."

26
00:02:10,600 --> 00:02:17,600
 "It will not come, this will not come to me."

27
00:02:17,600 --> 00:02:27,600
 So one should not look down upon, not think little of evil.

28
00:02:27,600 --> 00:02:33,600
 The is thinking evil results won't come of them.

29
00:02:33,600 --> 00:02:38,600
 So in the case of just a small evil, the point.

30
00:02:38,600 --> 00:02:48,600
 Because, "Uda bindunipatena uda kambopi purati."

31
00:02:48,600 --> 00:02:55,600
 Raindrops or water falling one drop at a time.

32
00:02:55,600 --> 00:03:06,600
 Even drops of water are able to fill a water pot.

33
00:03:06,600 --> 00:03:15,600
 Are able to make a flood of water.

34
00:03:15,600 --> 00:03:20,900
 "Uda kumbha" flood of water, or a water jug depending on

35
00:03:20,900 --> 00:03:21,600
 who you ask.

36
00:03:21,600 --> 00:03:28,600
 "Purati" fills, fills the water jug probably.

37
00:03:28,600 --> 00:03:38,600
 In the same way, "Balo purati pappasa" becomes full of evil

38
00:03:38,600 --> 00:03:38,600
.

39
00:03:38,600 --> 00:03:41,600
 "Tokang tokampi ajinam."

40
00:03:41,600 --> 00:03:46,600
 Even though "pi" it is gathered, "Ajinam."

41
00:03:46,600 --> 00:03:48,600
 "Tokang tokang."

42
00:03:48,600 --> 00:03:51,600
 I always remember this, "Tokang tokang."

43
00:03:51,600 --> 00:03:56,600
 "Tokang" means a bit, or a small amount.

44
00:03:56,600 --> 00:04:05,600
 "Tokang tokang" means little by little.

45
00:04:05,600 --> 00:04:10,600
 Little by little one becomes full of evil.

46
00:04:10,600 --> 00:04:19,120
 So this was taught in regards to a certain "biko" monk

47
00:04:19,120 --> 00:04:21,600
 whose name isn't given.

48
00:04:21,600 --> 00:04:29,600
 It was a monk who did something quite minor.

49
00:04:29,600 --> 00:04:36,600
 The act itself wasn't terribly evil.

50
00:04:36,600 --> 00:04:43,600
 You might even say that the act itself was negligible.

51
00:04:43,600 --> 00:04:46,910
 Not something to make a story about, but it's an

52
00:04:46,910 --> 00:04:47,600
 interesting story.

53
00:04:47,600 --> 00:04:49,600
 It's very short.

54
00:04:49,600 --> 00:04:55,600
 So what happened was he didn't take care of his requisites.

55
00:04:55,600 --> 00:04:58,840
 He didn't take care of the furniture and the bedding and

56
00:04:58,840 --> 00:05:01,600
 his belongings in general.

57
00:05:01,600 --> 00:05:05,470
 Sometimes he'd have to bring a chair outside and then he'd

58
00:05:05,470 --> 00:05:06,600
 leave it outside.

59
00:05:06,600 --> 00:05:12,190
 Or he'd take his bedding outside to air it out, his sheets

60
00:05:12,190 --> 00:05:13,600
 or mattress or something.

61
00:05:13,600 --> 00:05:17,600
 And then he'd leave them outside and then get rained on.

62
00:05:17,600 --> 00:05:21,860
 He would leave things unattended or uncared for and the

63
00:05:21,860 --> 00:05:25,600
 mice would eat them or the ants would eat them.

64
00:05:25,600 --> 00:05:28,590
 So he went through a lot of stuff and a lot of the

65
00:05:28,590 --> 00:05:31,940
 belongings, not just of his, but of the sangha, of the

66
00:05:31,940 --> 00:05:34,600
 monastery, were ruined as a result.

67
00:05:34,600 --> 00:05:38,070
 So again, it's just one of those things that shouldn't be

68
00:05:38,070 --> 00:05:41,600
 done, but it's not like he's going to hell for it.

69
00:05:41,600 --> 00:05:42,600
 So the monks admonished him.

70
00:05:42,600 --> 00:05:45,600
 They said, "Look, you shouldn't do this.

71
00:05:45,600 --> 00:05:50,290
 Don't you think you should put your stuff away and take

72
00:05:50,290 --> 00:05:51,600
 care of it?"

73
00:05:51,600 --> 00:05:54,600
 And he would say to them, "You know, it's just a trifling.

74
00:05:54,600 --> 00:05:55,600
 It's not a big deal.

75
00:05:55,600 --> 00:05:59,600
 Really, it's not even really worth worrying about."

76
00:05:59,600 --> 00:06:05,360
 They got more important things like meditation and study

77
00:06:05,360 --> 00:06:06,600
 and so on.

78
00:06:06,600 --> 00:06:09,240
 And so he would do the same thing again and again, never

79
00:06:09,240 --> 00:06:12,600
 learning, never changing.

80
00:06:12,600 --> 00:06:14,960
 And so the monks kind of got fed up with this and they went

81
00:06:14,960 --> 00:06:18,600
 to the Buddha and they said, "Look what's going on."

82
00:06:18,600 --> 00:06:22,480
 And so the Buddha sent him and said, "Is it true that you

83
00:06:22,480 --> 00:06:23,600
're acting this way?

84
00:06:23,600 --> 00:06:26,600
 Is it true that you don't put your belongings away?"

85
00:06:26,600 --> 00:06:31,600
 And when admonished that you don't stop, you keep doing it.

86
00:06:31,600 --> 00:06:34,600
 And so even to the Buddha, he said the same thing.

87
00:06:34,600 --> 00:06:37,600
 He said, "It's such a small thing.

88
00:06:37,600 --> 00:06:43,210
 It's wrong, but it's such a small wrong. Why worry about it

89
00:06:43,210 --> 00:06:43,600
?

90
00:06:43,600 --> 00:06:46,600
 It's not worth worrying about."

91
00:06:46,600 --> 00:06:50,600
 And the Buddha took this as an opportunity to teach.

92
00:06:50,600 --> 00:06:54,800
 So he was concerned, not exactly with the act, although he

93
00:06:54,800 --> 00:06:59,600
 did end up making a rule against leaving stuff out.

94
00:06:59,600 --> 00:07:04,990
 But he seems concerned with the attitude, this attitude of

95
00:07:04,990 --> 00:07:06,600
 making little.

96
00:07:06,600 --> 00:07:11,980
 Of your faults or dismissing small faults, which is

97
00:07:11,980 --> 00:07:16,080
 interesting because there's this idea of being easy going

98
00:07:16,080 --> 00:07:18,600
 and don't sweat the small stuff.

99
00:07:18,600 --> 00:07:22,710
 And there's something to that. Certainly you shouldn't make

100
00:07:22,710 --> 00:07:25,600
 things bigger than they actually are.

101
00:07:25,600 --> 00:07:29,450
 But you can't leave, it's like you can't leave an infection

102
00:07:29,450 --> 00:07:30,600
 unattended.

103
00:07:30,600 --> 00:07:35,600
 As far as evil goes, there's no such thing as little.

104
00:07:35,600 --> 00:07:38,610
 And there's another quote in here that I think we've

105
00:07:38,610 --> 00:07:39,600
 skipped over.

106
00:07:39,600 --> 00:07:42,600
 It comes from another one, and I was trying to remember it.

107
00:07:42,600 --> 00:07:47,600
 It's, "In regards to evil, apakanti nava mannita bang."

108
00:07:47,600 --> 00:07:51,600
 It is not proper to say apakang, it's just a little.

109
00:07:51,600 --> 00:07:55,600
 You should never look upon an evil deed as it's too little.

110
00:07:55,600 --> 00:07:59,760
 So the deed itself I don't think is terribly an evil thing,

111
00:07:59,760 --> 00:08:03,600
 it's negligent to not look after your belongings.

112
00:08:03,600 --> 00:08:07,380
 But the attitude is much more important, and the import of

113
00:08:07,380 --> 00:08:11,090
 this verse is of course far beyond leaving stuff out and so

114
00:08:11,090 --> 00:08:11,600
 on.

115
00:08:11,600 --> 00:08:15,470
 It's about the attitude of making light of something that

116
00:08:15,470 --> 00:08:23,600
 is negligent, making light of anything that is evil.

117
00:08:23,600 --> 00:08:26,760
 And this goes back to what we were talking about in the

118
00:08:26,760 --> 00:08:29,600
 past couple of verses and even earlier verses.

119
00:08:29,600 --> 00:08:38,430
 Like this monk who did whatever he wanted, aided, and was

120
00:08:38,430 --> 00:08:40,600
 sexually active and so on.

121
00:08:40,600 --> 00:08:43,600
 And it seemed pleasant.

122
00:08:43,600 --> 00:08:50,240
 When he wasn't doing what he wanted, he was miserable, and

123
00:08:50,240 --> 00:08:52,600
 he was suffering physically.

124
00:08:52,600 --> 00:08:54,900
 And so then when he started doing whatever he wanted, he

125
00:08:54,900 --> 00:08:56,600
 actually flourished and prospered.

126
00:08:56,600 --> 00:08:59,600
 He was healthier and happier and so on.

127
00:08:59,600 --> 00:09:04,600
 So it looked good, it was like, "Well that's great."

128
00:09:04,600 --> 00:09:07,400
 I think meditators, when they come here, they often fall

129
00:09:07,400 --> 00:09:08,600
 into this kind of doubt.

130
00:09:08,600 --> 00:09:11,530
 They think, "What am I doing here again? If I want to stop

131
00:09:11,530 --> 00:09:13,860
 the suffering, we're talking about ending suffering, why

132
00:09:13,860 --> 00:09:15,600
 don't I just go home?"

133
00:09:15,600 --> 00:09:18,600
 Then there would be no suffering.

134
00:09:18,600 --> 00:09:22,150
 It's actually quite remarkable that we come to meditate at

135
00:09:22,150 --> 00:09:24,800
 all, especially with all the wonderful things out there,

136
00:09:24,800 --> 00:09:31,190
 wonderful ways of finding happiness, finding pleasure

137
00:09:31,190 --> 00:09:32,600
 anyway.

138
00:09:32,600 --> 00:09:36,960
 But for some of us, for those of us who are perhaps more

139
00:09:36,960 --> 00:09:40,600
 introspective and maybe more observant,

140
00:09:40,600 --> 00:09:45,250
 we come to see that it's not actually satisfaction, it's

141
00:09:45,250 --> 00:09:47,600
 not actually happiness.

142
00:09:47,600 --> 00:09:49,950
 And this is concerning, as you watch yourself, filling

143
00:09:49,950 --> 00:09:54,750
 yourself up, not with happiness, but with desire, with

144
00:09:54,750 --> 00:09:58,600
 greed, with attachment, and ultimately with evil.

145
00:09:58,600 --> 00:10:01,600
 Evil meaning those things that cause you suffering.

146
00:10:01,600 --> 00:10:05,050
 So you become more addicted, and that addiction is pretty

147
00:10:05,050 --> 00:10:05,600
 evil.

148
00:10:05,600 --> 00:10:10,600
 In a sense, it brings suffering to you.

149
00:10:10,600 --> 00:10:15,600
 And also anger and frustration and boredom and conflict.

150
00:10:15,600 --> 00:10:18,680
 Just conflict whenever you can't get what you want, or

151
00:10:18,680 --> 00:10:20,600
 someone stands in the way of you getting what you want.

152
00:10:20,600 --> 00:10:30,600
 If you have enough desire, you will create great conflict

153
00:10:30,600 --> 00:10:32,600
 and adversity.

154
00:10:32,600 --> 00:10:35,600
 And you can't go to war over greed in this world.

155
00:10:35,600 --> 00:10:46,600
 Many wars have started just over simple greed.

156
00:10:46,600 --> 00:10:51,600
 And so it takes real introspection to get there.

157
00:10:51,600 --> 00:10:55,000
 But that is the underlying truth, that the evil doesn't go

158
00:10:55,000 --> 00:10:55,600
 away.

159
00:10:55,600 --> 00:11:03,600
 If you cultivate addiction, you become more addicted. If

160
00:11:03,600 --> 00:11:03,600
 you cultivate anger and aversion, you become more set in

161
00:11:03,600 --> 00:11:03,600
 those ways.

162
00:11:03,600 --> 00:11:07,600
 It's how habits are formed. It's how we become who we are.

163
00:11:07,600 --> 00:11:13,850
 We come to meditate and we think that we're surprised at

164
00:11:13,850 --> 00:11:18,240
 how much we think, and how much anger and aversion, and how

165
00:11:18,240 --> 00:11:20,600
 much greed and attachment,

166
00:11:20,600 --> 00:11:23,440
 and all these things that are inside. Where did they all

167
00:11:23,440 --> 00:11:24,600
 come from, we think?

168
00:11:24,600 --> 00:11:28,110
 This is like you have water dripping into the pot, and then

169
00:11:28,110 --> 00:11:31,600
 you look down and you say, "Ah, the pot is quite full."

170
00:11:31,600 --> 00:11:34,210
 And you don't know how did it get full, but actually the

171
00:11:34,210 --> 00:11:35,600
 water was dripping down.

172
00:11:35,600 --> 00:11:40,900
 The mind is the same. When you open it up and look, when

173
00:11:40,900 --> 00:11:43,750
 you actually take a look, that's when you see what you're

174
00:11:43,750 --> 00:11:44,600
 doing to yourself.

175
00:11:44,600 --> 00:11:50,690
 For the most part, human beings are protected from the

176
00:11:50,690 --> 00:11:53,600
 results on a large scale.

177
00:11:53,600 --> 00:11:58,050
 For the most part, we can avoid the suffering that comes

178
00:11:58,050 --> 00:12:02,600
 from not getting what we want or getting what we don't want

179
00:12:02,600 --> 00:12:02,600
.

180
00:12:02,600 --> 00:12:07,010
 We're able to, because we live in kind of a paradise, many

181
00:12:07,010 --> 00:12:11,600
 of us, not all of us, of course, relative to other animals,

182
00:12:11,600 --> 00:12:15,600
 for sure we live in a, many of us live in quite a paradise,

183
00:12:15,600 --> 00:12:17,600
 always able to get food every day.

184
00:12:17,600 --> 00:12:23,140
 I mean, just that is quite remarkable. There's no reason

185
00:12:23,140 --> 00:12:25,970
 why we should be able to get enough food to eat, but we do

186
00:12:25,970 --> 00:12:27,600
 get more than enough food.

187
00:12:27,600 --> 00:12:31,680
 And further than that, we get pleasure and entertainment

188
00:12:31,680 --> 00:12:33,600
 and all of these things.

189
00:12:33,600 --> 00:12:36,980
 And so we aren't able to see what the result is of

190
00:12:36,980 --> 00:12:40,600
 addiction, what the result is of aversion.

191
00:12:40,600 --> 00:12:43,580
 When we don't like something, we have ways of removing it.

192
00:12:43,580 --> 00:12:48,600
 You have pests in your home, you use pesticides.

193
00:12:48,600 --> 00:12:52,800
 We have many, many ways to get rid of the problems. You

194
00:12:52,800 --> 00:12:56,620
 have a headache, take a pill, you have a backache, get a

195
00:12:56,620 --> 00:12:57,600
 massage.

196
00:12:57,600 --> 00:13:01,600
 You feel bored, there's lots of things to entertain you.

197
00:13:01,600 --> 00:13:06,600
 And so little by little, we fill up our pot, not with good,

198
00:13:06,600 --> 00:13:07,600
 but with evil.

199
00:13:07,600 --> 00:13:10,780
 And the next verse, if you could guess, the next verse is

200
00:13:10,780 --> 00:13:12,600
 going to be about doing good.

201
00:13:12,600 --> 00:13:16,930
 You shouldn't, shouldn't, we'll talk about that one next

202
00:13:16,930 --> 00:13:17,600
 time.

203
00:13:17,600 --> 00:13:20,830
 But what I wanted to say especially is meditation changes

204
00:13:20,830 --> 00:13:25,600
 all that. Meditation allows you to see.

205
00:13:25,600 --> 00:13:30,600
 And this is really how we understand karma in Buddhism.

206
00:13:30,600 --> 00:13:33,600
 People talk about karma as being a kind of a belief and it

207
00:13:33,600 --> 00:13:34,600
's really not.

208
00:13:34,600 --> 00:13:38,390
 Most people can't, have to believe it because they aren't

209
00:13:38,390 --> 00:13:39,600
 able to see it.

210
00:13:39,600 --> 00:13:41,960
 But karma is something that you have to see on the moment

211
00:13:41,960 --> 00:13:42,600
ary level.

212
00:13:42,600 --> 00:13:46,120
 A person who practices insight meditation is able to

213
00:13:46,120 --> 00:13:48,600
 understand karma, is able to see it.

214
00:13:48,600 --> 00:13:50,600
 Because you are able to see moment to moment.

215
00:13:50,600 --> 00:13:53,500
 When I cling to things, it just leads to stress, it leads

216
00:13:53,500 --> 00:13:55,600
 to suffering, it leads to busyness.

217
00:13:55,600 --> 00:13:59,330
 It doesn't make me more content, more satisfied, more happy

218
00:13:59,330 --> 00:13:59,600
.

219
00:13:59,600 --> 00:14:02,600
 When I get angry, the same thing, anger is just painful and

220
00:14:02,600 --> 00:14:04,600
 unpleasant, it causes suffering.

221
00:14:04,600 --> 00:14:08,050
 On the other hand, when I cultivate mindfulness, when I

222
00:14:08,050 --> 00:14:11,460
 cultivate clarity in that moment, it's sending out goodness

223
00:14:11,460 --> 00:14:11,600
.

224
00:14:11,600 --> 00:14:17,130
 It's rippling out the effects of it. You can see. You don't

225
00:14:17,130 --> 00:14:18,600
 know where it's going.

226
00:14:18,600 --> 00:14:22,540
 Just like evil, when you become attached, it goes into the

227
00:14:22,540 --> 00:14:24,600
 mix in your brain and it affects your brain and your body.

228
00:14:24,600 --> 00:14:27,950
 You don't really see where it's going until later when it

229
00:14:27,950 --> 00:14:30,370
 gets full and then it overflows and then you see the result

230
00:14:30,370 --> 00:14:30,600
.

231
00:14:30,600 --> 00:14:33,370
 But when you meditate, you can see the source, you can see

232
00:14:33,370 --> 00:14:34,600
 what you're sending out.

233
00:14:34,600 --> 00:14:38,600
 You get to see how your reactions are affecting your brain,

234
00:14:38,600 --> 00:14:41,600
 are affecting your body, are affecting the world around you

235
00:14:41,600 --> 00:14:41,600
,

236
00:14:41,600 --> 00:14:44,600
 the people around you and everything you say and you do.

237
00:14:44,600 --> 00:14:46,600
 When you're mindful, you can see all this.

238
00:14:46,600 --> 00:14:49,340
 It's the great thing about meditation. It's one of the

239
00:14:49,340 --> 00:14:52,600
 first things you learn in the meditation practice.

240
00:14:52,600 --> 00:14:56,930
 Not intellectually, but you see it. You see how the mind

241
00:14:56,930 --> 00:15:01,600
 works. You see how karma works.

242
00:15:01,600 --> 00:15:07,210
 You lose doubt. You lose this doubt about good and evil and

243
00:15:07,210 --> 00:15:08,600
 right and wrong.

244
00:15:08,600 --> 00:15:13,950
 You come to understand that there is a result. There are

245
00:15:13,950 --> 00:15:16,600
 consequences to our action.

246
00:15:16,600 --> 00:15:20,580
 This is important for our meditation. It's an important

247
00:15:20,580 --> 00:15:24,950
 encouragement, I think, especially for those of us who are

248
00:15:24,950 --> 00:15:28,600
 meditating to verify this and to say to ourselves,

249
00:15:28,600 --> 00:15:34,010
 "This is a great thing I'm doing that I'm able to see this,

250
00:15:34,010 --> 00:15:38,920
 that I can adjust and that I can change my habits so that I

251
00:15:38,920 --> 00:15:39,600
 fill my products,

252
00:15:39,600 --> 00:15:45,400
 not with addiction and aversion, but with mindfulness and

253
00:15:45,400 --> 00:15:46,600
 wisdom."

254
00:15:46,600 --> 00:15:54,600
 So that's the Dhammapada short story, wonderful verse.

255
00:15:54,600 --> 00:15:58,600
 Thank you all for tuning in and wishing you all the best.

256
00:15:58,600 --> 00:16:02,600
 Thank you.

257
00:16:02,600 --> 00:16:22,600
 Thank you.

