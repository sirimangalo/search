 doesn't mean we should do away with all laws or rules. It
 means we have to have a better way of
 of actually ensuring that the rules are kept, you know,
 that we're able to build a society where we
 have an order whereby people don't fall into behavior that
 is conducive to conflict,
 either inner conflict or conflict in society.
 And eventually, ideally, a society that encourages people
 in to become wiser, to become better, to
 become more pure, more free, more happy.
 And the final one is having common purity of view. So this
 relates to what I've been talking about
 not only do we need rules, but to have a pure society, to
 have a harmonious society,
 you really do need to some extent a community of a common
 purpose, right? So most of Canada is
 in agreement that providing health care to all people is a
 good thing. And having that right view
 of kindness and support and caring for our society. You
 could say that's an important
 part of creating the harmony. You know, if you look in
 America, there's incredible disharmony.
 America is very much divided down party lines. And probably
 there's some intent behind that,
 the intent to create a divide and conquer divided society.
 I don't know. I don't want to get in too
 much into it. But when you've got such ideology,
 ideological diversity, that's where conflict
 arises. So this recent in Canada, it's come up recently,
 there's been a move to, I mean, I'd say
 it's fairly radical, but this move to try and criminalize
 or criminalize hate. And not just
 hate, but physical and verbal acts of hate. And to apply
 some fairly broad criteria for what we
 understand as hate. So if you deny a person's identity,
 gender identity is the big hot topic now,
 that can be considered hate speech, or an act of a hate
 crime. I mean, if it was done with malicious
 intent, that's not like policing speech, not what everyone
 thinks it is. If you are doing so in a
 dismissive fashion, where you really and truly, anyway, I
 don't know, it's a bit complicated and
 perhaps radical in a sense, but it's caused a great
 division in our society, I think. I mean,
 on the one hand, there's one side, there's people who are
 quite militaristic in their, in their
 push for their own rights, you know, a push for their own,
 what the other side calls agenda,
 the liberal extremist agenda. I mean, in the end, there's,
 there's some real good there,
 trying to, trying to make sure that people feel and have
 this sense of self worth and sense of,
 of grounding in who they are, and that they're not left out
 of the conversation, that they're not
 dismissed as, or, or, or forced to be something they're not
, right? That they can have this space
 to breathe. But it does get quite vegment and angry, which
 is not, it's not conducive to progress and
 harmony. Not when it gets to that level, I think. It can be
 so much better done if it was tempered and
 associated with kindness. On the other side, there's an
 incredibly vile and corrupt form of
 hatred and bigotry that is cloaked in the guise of free
 speech, but really, it's just about
 hating people who are different and people who have
 a different way of looking at the world. So we see this in
 society. I mean, you see this in
 monasteries, the story of, we'll have many stories of monks
 beating on monks and monks attacking
 monks and monks fighting with monks. We have stories going
 back to the Buddha's time, this sort of thing.
 Right now we're talking about view. We have, we have
 stories of monks arguing with monks. We have
 divisions from the time of the Buddha. Right after the
 Buddha passed away, there was right away a
 split and it split and it split and it split. And now we've
 got forms of Buddhism that I don't even
 recognize as anything to do with Buddhism. Very different
 forms. I've got splits among communities.
 I've been in, I was in a monastery once where there were
 three groups of monks. They were had a split
 and in the monastery there were three, three communities
 and they didn't interact with each
 other. They meant they schemed against each other. It was
 quite, quite, well,
 impressive in a way, I guess.
 Having the same view. Now for us as meditators, this means
 really having a view of the four noble
 truths. That gives you the greatest harmony. When you see
 that, then there's no, there's none of this.
 There would never be a question about identity. There would
 never be a question about
 a clique or a group or a division of the Sangha.
 I just wouldn't have any sense or meaning or purpose.
 Not to mention all the anger and greed involved would just
 have disappeared.
 It's important that we're clear and that we, I mean, we
 have these debates in society. It's
 important that we debate. It's important that we discuss.
 It's important that we,
 we listen to each other. It's one thing that being in the
 university has taught me is how to listen,
 how to listen to people who I might disagree with. It
 really gives you interesting perspective on why
 people think things they do and it helps you understand how
 people get to the views that
 they get to, even though you might completely disagree with
 their views. You at least can
 understand what that makes them and where they come from.
 By listening and by talking, we can,
 we can break through and come to an understanding with each
 other. And,
 you know, I mean, there's many, many aspects, many factors
 involved. You need rules, you need
 kindness, you need a desire for truth and the desire for
 harmony. The worst thing,
 the worst enemy to harmony, of course, is the desire for
 conflict.
 If society is keen to hurt and to cause harm and to disrupt
,
 and you never, it doesn't matter how well-intentioned you
 might be or
 how clear you might be in your mind, you're never going to
 succeed in bringing harmony to society.
 And that's a sort of view in itself, right? The view, the
 view that harmony and peace are
 worth working towards, that the view that the true goal
 should be happiness and should be peace,
 that there is no happiness without peace. Nati santi parang
 sukhang.
 Once you have peace, you have happiness.
 So there you go. Those are the Saraniya dhammas, they're d
hammas that we should all
 recollect and they help us think, fondly of each other.
 They help make the community
 a harmonious community. So thank you all for tuning in.
 Have a good night.
 (
 sound effect )
 (
 sound effect )
 ( sound effect )
 ( sound effect )
 ( sound effect )
 ( sound effect )
 My understanding is there's never been a communist
 government.
 Oops, that was the wrong button.
 What does that do?
 A communist government is, I mean, there is a sort of a
 tension between the self and the community.
 I don't think it's proper to, um, yeah, I don't know.
 I think to some extent you have to remember the importance
 of the individual and the importance
 for individual freedom to pursue one's own, one's own
 enlightenment, one's own goals. I mean,
 as a pluralistic society is always going to be a challenge
 because you have different ideas of
 what's good and what's right. So until you get everyone
 with the same right view, very difficult,
 very difficult to find harmony, which was that last point.
 ( sound effect )
 ( sound effect )
 ( sound effect )
 Students criticize faculty. That must be interesting. I
 just had to do my evaluations.
 I wasn't sure if there'd be a lot of criticism or not.
 Um, out of defensiveness. Well, I mean, ego plays a big
 part of that then.
 The whole idea is to, I mean, that's the big problem with
 the university is there's a lot of ego.
 That can be a problem.
 ( sound effect )
 So, I mean, a bit the best advice I think is learning to
 have a thick skin and not,
 I'd say thick skin, but with faculty it's scary.
