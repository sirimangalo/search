1
00:00:00,000 --> 00:00:04,410
 Should the four foundations of mindfulness and the five Jh

2
00:00:04,410 --> 00:00:06,240
ana factors of concentration

3
00:00:06,240 --> 00:00:11,740
 and absorption be learned before Vipassana meditation can

4
00:00:11,740 --> 00:00:13,760
 be correctly used?

5
00:00:13,760 --> 00:00:22,030
 Okay, well there are two paths of practice that should be

6
00:00:22,030 --> 00:00:23,480
 clear.

7
00:00:23,480 --> 00:00:27,480
 The first path of practice is to start with Tranquility

8
00:00:27,480 --> 00:00:29,760
 meditation and then go on with

9
00:00:29,760 --> 00:00:30,760
 Vipassana.

10
00:00:30,760 --> 00:00:37,780
 So the way that works is you cultivate pure concentration

11
00:00:37,780 --> 00:00:42,160
 and or you cultivate tranquility

12
00:00:42,160 --> 00:00:47,010
 and then your wisdom, your knowledge comes and when they

13
00:00:47,010 --> 00:00:49,000
 meet in the peak when you have

14
00:00:49,000 --> 00:00:53,640
 both the two of them then there's the spark and because of

15
00:00:53,640 --> 00:00:59,640
 the focused, the focused knowledge

16
00:00:59,640 --> 00:01:05,200
 or insight there is the attainment of Nibbana.

17
00:01:05,200 --> 00:01:09,130
 Now the other way is to build up both concentration and

18
00:01:09,130 --> 00:01:12,000
 insight together until the last moment

19
00:01:12,000 --> 00:01:14,360
 and there's the spark.

20
00:01:14,360 --> 00:01:19,740
 So a person who practices the five Jhana factors of

21
00:01:19,740 --> 00:01:24,080
 concentration and absorption is someone

22
00:01:24,080 --> 00:01:28,210
 who goes the first way, first tranquilizing the mind then

23
00:01:28,210 --> 00:01:30,400
 cultivating insight based on

24
00:01:30,400 --> 00:01:33,160
 that.

25
00:01:33,160 --> 00:01:39,780
 Now it can be stronger, it's certainly more full because it

26
00:01:39,780 --> 00:01:42,800
 gives you the opportunity

27
00:01:42,800 --> 00:01:45,980
 to explore the conceptual world because the reason it's

28
00:01:45,980 --> 00:01:47,880
 calm without insight is because

29
00:01:47,880 --> 00:01:50,480
 it's focused on the concept and because it allows you, you

30
00:01:50,480 --> 00:01:51,800
're in the conceptual world

31
00:01:51,800 --> 00:01:55,230
 you can play with it and you can actually cultivate magical

32
00:01:55,230 --> 00:01:57,000
 powers like reading people's

33
00:01:57,000 --> 00:02:02,310
 minds, remembering past lives, all sorts of neat phenomena

34
00:02:02,310 --> 00:02:04,560
 that are in the conceptual

35
00:02:04,560 --> 00:02:07,160
 world.

36
00:02:07,160 --> 00:02:12,560
 You can't get that from cultivating the two together, not

37
00:02:12,560 --> 00:02:14,720
 very easily anyway.

38
00:02:14,720 --> 00:02:17,240
 But it seems to take longer.

39
00:02:17,240 --> 00:02:20,840
 The time you're spending in cultivating first concentration

40
00:02:20,840 --> 00:02:22,880
 could have been used in cultivating

41
00:02:22,880 --> 00:02:26,230
 insight so it seems like it probably takes a little bit

42
00:02:26,230 --> 00:02:28,080
 longer because if you just do

43
00:02:28,080 --> 00:02:32,050
 pure insight and concentration you seem to be able to get

44
00:02:32,050 --> 00:02:34,040
 pretty good results in a few

45
00:02:34,040 --> 00:02:38,630
 weeks or at most a month whereas if you do tranquility

46
00:02:38,630 --> 00:02:41,240
 first it can take months to get

47
00:02:41,240 --> 00:02:42,240
 the same result.

48
00:02:42,240 --> 00:02:59,690
 So, the question of whether you have to learn them, now the

49
00:02:59,690 --> 00:03:01,640
 question you hear, your question

50
00:03:01,640 --> 00:03:05,400
 is should they be learned, now I'm not sure whether that

51
00:03:05,400 --> 00:03:07,520
 means study or practice, if it

52
00:03:07,520 --> 00:03:11,670
 just means do you have to study about these things then

53
00:03:11,670 --> 00:03:14,240
 well first of all you don't have

54
00:03:14,240 --> 00:03:20,150
 to practice them first so you don't have to learn how to or

55
00:03:20,150 --> 00:03:23,120
 you don't have to cultivate

56
00:03:23,120 --> 00:03:27,260
 the five jhana factors but first if you just want to do

57
00:03:27,260 --> 00:03:29,120
 them both together.

58
00:03:29,120 --> 00:03:33,830
 So you obviously in that case don't have to study them

59
00:03:33,830 --> 00:03:36,560
 either but if you want to do it

60
00:03:36,560 --> 00:03:40,350
 the samatha first way, the tranquility first way then you

61
00:03:40,350 --> 00:03:42,440
 have to both learn and practice

62
00:03:42,440 --> 00:03:45,900
 them first, study and practice them.

63
00:03:45,900 --> 00:03:50,540
 As for the four foundations of mindfulness, if the question

64
00:03:50,540 --> 00:03:52,520
 is do you have to practice

65
00:03:52,520 --> 00:03:56,780
 them first so you have to undertake to practice them before

66
00:03:56,780 --> 00:03:59,040
 practicing insight meditation

67
00:03:59,040 --> 00:04:02,290
 then this is a confusion about what is meant by insight

68
00:04:02,290 --> 00:04:04,720
 meditation because vipassana insight

69
00:04:04,720 --> 00:04:07,640
 meditation is the practice of the four foundations of

70
00:04:07,640 --> 00:04:10,440
 mindfulness, you practice the four foundations

71
00:04:10,440 --> 00:04:14,730
 of mindfulness to cultivate insight, you can use certain

72
00:04:14,730 --> 00:04:17,200
 aspects of the four foundations

73
00:04:17,200 --> 00:04:22,590
 of mindfulness to cultivate tranquility but in its most

74
00:04:22,590 --> 00:04:25,640
 bare form it's a type of insight

75
00:04:25,640 --> 00:04:27,600
 meditation practice.

76
00:04:27,600 --> 00:04:32,270
 Now the question do you have to study the four foundations

77
00:04:32,270 --> 00:04:34,920
 first, obviously certainly

78
00:04:34,920 --> 00:04:37,800
 you don't have to study them in detail but obviously you

79
00:04:37,800 --> 00:04:39,520
 have to know about them because

80
00:04:39,520 --> 00:04:44,020
 they are the practice that you're undertaking, knowing

81
00:04:44,020 --> 00:04:46,600
 about and understanding how to practice

82
00:04:46,600 --> 00:04:50,320
 each of the four in regards to each of the four foundations

83
00:04:50,320 --> 00:04:52,000
 at least on a basic level

84
00:04:52,000 --> 00:04:56,430
 is essential because without understanding what it is you

85
00:04:56,430 --> 00:04:58,720
're going to practice you can't

86
00:04:58,720 --> 00:05:04,440
 be expected to practice but certainly important to study

87
00:05:04,440 --> 00:05:07,720
 them but the question of whether

88
00:05:07,720 --> 00:05:11,140
 you should practice them first doesn't arise because once

89
00:05:11,140 --> 00:05:13,160
 you begin to practice them that's

90
00:05:13,160 --> 00:05:16,690
 the practice of vipassana, insight meditation unless you're

91
00:05:16,690 --> 00:05:18,520
 focusing on the specific parts

92
00:05:18,520 --> 00:05:22,810
 that can lead to tranquility first like mindfulness of the

93
00:05:22,810 --> 00:05:25,680
 breath or the cemetery contemplations,

94
00:05:25,680 --> 00:05:29,600
 loathsome-ness that kind of thing, the 32 parts of the body

95
00:05:29,600 --> 00:05:30,640
 for example.

96
00:05:30,640 --> 00:05:33,240
 So, thank you.

