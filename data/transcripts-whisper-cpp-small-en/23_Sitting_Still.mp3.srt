1
00:00:00,000 --> 00:00:05,330
 Sitting Meditation Q&A. Sitting still. Is there a way to

2
00:00:05,330 --> 00:00:06,960
 get more motivated while

3
00:00:06,960 --> 00:00:11,380
 meditating? Sometimes I want to meditate for an hour but I

4
00:00:11,380 --> 00:00:13,600
 end up unable to meditate for

5
00:00:13,600 --> 00:00:18,570
 longer than 20 minutes. I think an important part of the

6
00:00:18,570 --> 00:00:21,200
 answer is that meditation is something

7
00:00:21,200 --> 00:00:24,510
 that is momentary. It is something that is to be done at

8
00:00:24,510 --> 00:00:26,960
 every moment. You cannot meditate for

9
00:00:26,960 --> 00:00:31,310
 an hour. You can only meditate for one instant and that

10
00:00:31,310 --> 00:00:35,120
 instant is either meditative or it is not.

11
00:00:35,120 --> 00:00:39,440
 At this moment, are you mindful or are you not? When you

12
00:00:39,440 --> 00:00:42,400
 ask how to get motivated during meditation,

13
00:00:42,400 --> 00:00:46,320
 a good question to ask yourself might be, "How do I best

14
00:00:46,320 --> 00:00:48,960
 make use of this hour so that it is

15
00:00:48,960 --> 00:00:52,710
 generally meditative?" If you approach the problem this way

16
00:00:52,710 --> 00:00:55,440
, you will not worry so much about how

17
00:00:55,440 --> 00:00:59,990
 long you sit or become discouraged by your inability to sit

18
00:00:59,990 --> 00:01:02,240
 in meditation for a long time.

19
00:01:02,240 --> 00:01:07,040
 Our inability to sit for long periods is a part of the

20
00:01:07,040 --> 00:01:10,000
 reason why we meditate in the first place.

21
00:01:10,000 --> 00:01:13,880
 The mental qualities preventing you from sitting for long

22
00:01:13,880 --> 00:01:16,800
 periods should be a main focus of your

23
00:01:16,800 --> 00:01:20,910
 practice. This is something easy to forget because they are

24
00:01:20,910 --> 00:01:23,280
 the same problems present throughout

25
00:01:23,280 --> 00:01:26,300
 our life and we are quick to react to them rather than

26
00:01:26,300 --> 00:01:30,080
 understand them. The problems in our minds

27
00:01:30,080 --> 00:01:33,520
 are a part of who we are, so we meditate in order to

28
00:01:33,520 --> 00:01:37,040
 overcome them rather than avoid them. One trick

29
00:01:37,040 --> 00:01:40,600
 you can use when you want to stop meditating is to ask

30
00:01:40,600 --> 00:01:43,760
 yourself, "What is the problem? What is it

31
00:01:43,760 --> 00:01:47,760
 that is causing me to want to stop sitting here, to get up

32
00:01:47,760 --> 00:01:50,160
 and do something else? What is the

33
00:01:50,160 --> 00:01:53,940
 difference between sitting here and going somewhere else?"

34
00:01:53,940 --> 00:01:56,320
 There could be a good reason, like you have

35
00:01:56,320 --> 00:01:59,900
 some important work to do, or you are going to injure

36
00:01:59,900 --> 00:02:02,800
 yourself if you sit in this position any

37
00:02:02,800 --> 00:02:06,790
 longer, but most likely it is simply because of the reasons

38
00:02:06,790 --> 00:02:09,760
 why we are meditating in the first place,

39
00:02:09,760 --> 00:02:14,890
 desire, aversion, delusion, etc. These problematic mind

40
00:02:14,890 --> 00:02:18,080
 states come up in meditation and present a

41
00:02:18,080 --> 00:02:22,010
 chance to work them out. Meditation gives us a chance to

42
00:02:22,010 --> 00:02:24,400
 examine our minds and to see what is

43
00:02:24,400 --> 00:02:28,660
 causing us stress and suffering, what is of benefit and

44
00:02:28,660 --> 00:02:31,920
 what is not. Understanding this can help

45
00:02:31,920 --> 00:02:35,970
 improve motivation as well. Other ways of creating

46
00:02:35,970 --> 00:02:39,200
 motivation include positive reminders of the

47
00:02:39,200 --> 00:02:43,970
 benefits of meditation, the benefits to yourself, the peace

48
00:02:43,970 --> 00:02:45,920
 and clarity of mind that comes from

49
00:02:45,920 --> 00:02:50,090
 meditation, and how much you can help other people as well.

50
00:02:50,090 --> 00:02:52,560
 You can think of friends and family who

51
00:02:52,560 --> 00:02:55,880
 are stressed and suffering that you would be able to help

52
00:02:55,880 --> 00:02:58,480
 by learning this incredible technique of

53
00:02:58,480 --> 00:03:02,310
 stress reduction. Conversely, you can motivate yourself

54
00:03:02,310 --> 00:03:04,960
 using negative reminders of what happens

55
00:03:04,960 --> 00:03:08,760
 if we do not meditate. The unwholesome mind states that

56
00:03:08,760 --> 00:03:11,840
 consume our mind, the hurt we cause towards

57
00:03:11,840 --> 00:03:15,800
 ourselves, the stress, the addiction, and the suffering

58
00:03:15,800 --> 00:03:18,320
 that we bring to the world around us.

59
00:03:18,320 --> 00:03:21,550
 You can remind yourself of the dangers that come from

60
00:03:21,550 --> 00:03:24,080
 allowing these states to persist

61
00:03:24,080 --> 00:03:28,300
 over the long term, the danger of not being able to cope

62
00:03:28,300 --> 00:03:32,080
 with misfortune, sickness, and old age.

63
00:03:32,080 --> 00:03:35,490
 When you are not able to deal with simple pains and

64
00:03:35,490 --> 00:03:38,480
 stresses from sitting still for more than 20

65
00:03:38,480 --> 00:03:42,540
 minutes, what happens if you become invalid and have to

66
00:03:42,540 --> 00:03:45,120
 stay in bed for days? How will you be

67
00:03:45,120 --> 00:03:49,070
 able to cope with that? This sort of reminder can help us

68
00:03:49,070 --> 00:03:51,920
 appreciate the benefits of meditation.

