1
00:00:00,000 --> 00:00:07,460
 Okay, good evening everyone.

2
00:00:07,460 --> 00:00:17,320
 Back for more questions and answers.

3
00:00:17,320 --> 00:00:26,710
 So tonight's question is on the healing power of meditation

4
00:00:26,710 --> 00:00:26,860
.

5
00:00:26,860 --> 00:00:35,840
 The common question, the common theme is the question of

6
00:00:35,840 --> 00:00:41,400
 whether meditation can heal x,

7
00:00:41,400 --> 00:00:46,480
 y or z, can do x, y or z for us.

8
00:00:46,480 --> 00:00:59,080
 Can meditation do this, can meditation do that?

9
00:00:59,080 --> 00:01:03,600
 This question actually specifically asks whether, and it's

10
00:01:03,600 --> 00:01:07,320
 a good example question, whether meditation

11
00:01:07,320 --> 00:01:13,610
 and mindfulness can cure the damage to the brain and the

12
00:01:13,610 --> 00:01:17,320
 mind caused by psychedelics.

13
00:01:17,320 --> 00:01:19,200
 It's an interesting question.

14
00:01:19,200 --> 00:01:23,090
 I'm not sure whether this person has heard me talk about my

15
00:01:23,090 --> 00:01:25,160
 experience with psychedelics

16
00:01:25,160 --> 00:01:34,580
 because I think my brain was somewhat damaged by psychedel

17
00:01:34,580 --> 00:01:36,320
ics temporarily, permanently,

18
00:01:36,320 --> 00:01:39,120
 I don't know.

19
00:01:39,120 --> 00:01:43,600
 I've done psychedelics before and there have been no

20
00:01:43,600 --> 00:01:46,760
 problems, but something in this one

21
00:01:46,760 --> 00:01:50,080
 really changed the way my brain sort of functioned.

22
00:01:50,080 --> 00:01:51,760
 There was something wrong.

23
00:01:51,760 --> 00:01:57,750
 Like I felt like I had been pulled away from the experient

24
00:01:57,750 --> 00:02:00,800
ial world and there was sort

25
00:02:00,800 --> 00:02:08,690
 of a lapse or a disconnect and it was acutely felt every

26
00:02:08,690 --> 00:02:12,880
 time I then did drugs, usually

27
00:02:12,880 --> 00:02:13,880
 cannabis.

28
00:02:13,880 --> 00:02:26,280
 That's cannabis, cannabis is marijuana, right?

29
00:02:26,280 --> 00:02:28,200
 I would feel this again, this disconnect.

30
00:02:28,200 --> 00:02:32,360
 I wasn't really there.

31
00:02:32,360 --> 00:02:35,240
 No cannabis and moderation does not have phenomenal

32
00:02:35,240 --> 00:02:36,080
 potential.

33
00:02:36,080 --> 00:02:39,560
 You people in chat, you're listening to the dhamma now.

34
00:02:39,560 --> 00:02:44,520
 If you're not interested in listening, go elsewhere.

35
00:02:44,520 --> 00:02:45,520
 Not so many comments.

36
00:02:45,520 --> 00:02:49,720
 I don't need a running commentary, please.

37
00:02:49,720 --> 00:02:53,120
 Thank you.

38
00:02:53,120 --> 00:03:06,800
 It turned me off of drugs, really.

39
00:03:06,800 --> 00:03:09,640
 Not to say that drugs do that, drugs are not all the same,

40
00:03:09,640 --> 00:03:11,360
 but I couldn't do them anymore.

41
00:03:11,360 --> 00:03:16,720
 I couldn't get stoned anymore.

42
00:03:16,720 --> 00:03:20,640
 I think I was still into alcohol for a while or whatever.

43
00:03:20,640 --> 00:03:28,120
 I empathize with the potential problems to the brain.

44
00:03:28,120 --> 00:03:31,790
 I mean, there have been studies done on how alcohol,

45
00:03:31,790 --> 00:03:34,240
 caffeine and marijuana do degrade

46
00:03:34,240 --> 00:03:35,240
 the brain.

47
00:03:35,240 --> 00:03:36,240
 I don't know.

48
00:03:36,240 --> 00:03:37,840
 I mean, it's studies I've seen.

49
00:03:37,840 --> 00:03:39,680
 True, false, I don't know.

50
00:03:39,680 --> 00:03:43,560
 Science is always learning new things, but there appears to

51
00:03:43,560 --> 00:03:45,040
 be the potential for some

52
00:03:45,040 --> 00:03:49,160
 connection to actual brain damage just by drinking lots of

53
00:03:49,160 --> 00:03:51,160
 coffee or alcohol or taking

54
00:03:51,160 --> 00:03:56,480
 lots of marijuana.

55
00:03:56,480 --> 00:03:58,960
 It's more extreme, psychedelics, I guess.

56
00:03:58,960 --> 00:04:01,440
 I mean, apparently I'm not the only one who had this sort

57
00:04:01,440 --> 00:04:02,960
 of experience where you really

58
00:04:02,960 --> 00:04:05,840
 feel like you've damaged your brain in some way.

59
00:04:05,840 --> 00:04:08,840
 Regardless, this isn't to talk about drugs.

60
00:04:08,840 --> 00:04:11,760
 If you people are, all these chats are talking about

61
00:04:11,760 --> 00:04:13,040
 cannabis and so on.

62
00:04:13,040 --> 00:04:14,040
 I'm not interested.

63
00:04:14,040 --> 00:04:15,040
 Please go elsewhere.

64
00:04:15,040 --> 00:04:19,280
 It's not what this talk is about.

65
00:04:19,280 --> 00:04:21,480
 This talk is about the healing power of meditation.

66
00:04:21,480 --> 00:04:26,480
 If you suffer brain damage, I mean, a really good question

67
00:04:26,480 --> 00:04:28,840
 would be, what if you have a

68
00:04:28,840 --> 00:04:33,810
 partial lobotomy or maybe you get into an accident and have

69
00:04:33,810 --> 00:04:36,040
 brain trauma or maybe you

70
00:04:36,040 --> 00:04:48,920
 ... what you ... well, for some reason or another, your

71
00:04:48,920 --> 00:04:50,200
 brain gets damaged.

72
00:04:50,200 --> 00:04:53,160
 Maybe you're sniffing gasoline, right?

73
00:04:53,160 --> 00:05:00,480
 That'll do it.

74
00:05:00,480 --> 00:05:02,410
 So there's this question of whether meditation can heal

75
00:05:02,410 --> 00:05:02,760
 this.

76
00:05:02,760 --> 00:05:05,650
 I think of all the questions of can meditation do this and

77
00:05:05,650 --> 00:05:07,280
 can meditation do that, this one

78
00:05:07,280 --> 00:05:11,880
 has a good potential to be answered in the positive, right?

79
00:05:11,880 --> 00:05:20,570
 Because meditation deals with the mind, which is very

80
00:05:20,570 --> 00:05:25,360
 closely related to the brain.

81
00:05:25,360 --> 00:05:30,420
 So the healing power of meditation might then extend to

82
00:05:30,420 --> 00:05:33,080
 this sort of brain damage.

83
00:05:33,080 --> 00:05:36,020
 Now the question was asking whether the damage to the mind

84
00:05:36,020 --> 00:05:36,960
 can be healed.

85
00:05:36,960 --> 00:05:39,680
 That's actually a completely different question.

86
00:05:39,680 --> 00:05:43,680
 First, you have to ask, can the mind be damaged?

87
00:05:43,680 --> 00:05:46,120
 What does it mean to have mind damage?

88
00:05:46,120 --> 00:05:49,160
 Something I've actually never been asked before.

89
00:05:49,160 --> 00:05:50,160
 So let's separate these out.

90
00:05:50,160 --> 00:05:53,120
 First, let's do away with that one.

91
00:05:53,120 --> 00:05:59,550
 Mind damage can only mean the cultivation of bad habits,

92
00:05:59,550 --> 00:06:00,680
 right?

93
00:06:00,680 --> 00:06:03,720
 And bad habits can be pretty extreme.

94
00:06:03,720 --> 00:06:08,560
 If you do something even once that's quite extreme, then it

95
00:06:08,560 --> 00:06:10,920
 can affect the mind in quite

96
00:06:10,920 --> 00:06:12,600
 an extreme way.

97
00:06:12,600 --> 00:06:16,520
 The mind can also be affected by physical changes.

98
00:06:16,520 --> 00:06:22,160
 So if the brain were to undergo some severe trauma or so on

99
00:06:22,160 --> 00:06:25,120
, then the potential result

100
00:06:25,120 --> 00:06:29,330
 to the mind might be quite extreme based on the person's

101
00:06:29,330 --> 00:06:31,920
 reactivity, you know, you're

102
00:06:31,920 --> 00:06:35,950
 reacting to it and the inability to adapt to change, you

103
00:06:35,950 --> 00:06:38,160
 know, the expectation and so

104
00:06:38,160 --> 00:06:39,520
 on.

105
00:06:39,520 --> 00:06:41,520
 The mind is organic as well.

106
00:06:41,520 --> 00:06:47,080
 So it can be affected by the physical.

107
00:06:47,080 --> 00:06:50,040
 But it's not damaged per se.

108
00:06:50,040 --> 00:06:55,960
 It's just caught up in habits and expectations is the big

109
00:06:55,960 --> 00:06:56,760
 one.

110
00:06:56,760 --> 00:07:00,310
 And those expectations are not met because of the changes

111
00:07:00,310 --> 00:07:01,560
 to the physical.

112
00:07:01,560 --> 00:07:05,650
 So that being healed, that absolutely can be healed through

113
00:07:05,650 --> 00:07:06,760
 meditation.

114
00:07:06,760 --> 00:07:12,190
 I mean, one of the important aspects of this question is

115
00:07:12,190 --> 00:07:15,160
 whether or not you mean in this

116
00:07:15,160 --> 00:07:17,720
 life or whether you mean in future lives.

117
00:07:17,720 --> 00:07:22,580
 I mean, for the mind, healing is ongoing and it can start

118
00:07:22,580 --> 00:07:25,520
 as soon as you undertake training

119
00:07:25,520 --> 00:07:28,960
 in meditation, for example.

120
00:07:28,960 --> 00:07:33,260
 And you know, it continues in this life and it continues

121
00:07:33,260 --> 00:07:35,000
 into the next life.

122
00:07:35,000 --> 00:07:39,820
 It's possible that there are certain parameters under which

123
00:07:39,820 --> 00:07:41,600
 it's not possible.

124
00:07:41,600 --> 00:07:44,320
 It's possible that it might be not possible for a person to

125
00:07:44,320 --> 00:07:45,720
 become enlightened in this

126
00:07:45,720 --> 00:07:47,120
 life.

127
00:07:47,120 --> 00:07:50,470
 So for a person to free themselves from bad habits, it's

128
00:07:50,470 --> 00:07:52,560
 possible to be in such a situation.

129
00:07:52,560 --> 00:07:54,760
 I could think of maybe a lobotomy.

130
00:07:54,760 --> 00:07:55,760
 I don't know.

131
00:07:55,760 --> 00:07:58,680
 I don't have any evidence or background for this.

132
00:07:58,680 --> 00:08:03,070
 If a person has electroshock therapy or lobotomy or

133
00:08:03,070 --> 00:08:06,840
 something, it may, I wouldn't say render

134
00:08:06,840 --> 00:08:10,420
 the mind incapable of becoming enlightened, but it will

135
00:08:10,420 --> 00:08:12,480
 make it very difficult or it will

136
00:08:12,480 --> 00:08:17,280
 render it so difficult that the mind just is incapable,

137
00:08:17,280 --> 00:08:19,840
 incapable of changing its bad

138
00:08:19,840 --> 00:08:23,890
 habits, of changing its reactions, its interactions with

139
00:08:23,890 --> 00:08:25,040
 the physical.

140
00:08:25,040 --> 00:08:32,090
 I mean, changing to the extent of healing, of becoming

141
00:08:32,090 --> 00:08:35,440
 enlightened, really.

142
00:08:35,440 --> 00:08:42,760
 The damage to the brain is in whole of the realm.

143
00:08:42,760 --> 00:08:47,800
 Brain damage can affect the mind, but only involves the

144
00:08:47,800 --> 00:08:50,920
 stimuli that the mind is fed.

145
00:08:50,920 --> 00:08:53,840
 So it is a different category.

146
00:08:53,840 --> 00:08:57,060
 So having dealt with the mind, I mean, the most important

147
00:08:57,060 --> 00:08:58,720
 thing to understand about the

148
00:08:58,720 --> 00:09:04,060
 mind is it deals with reactions, interactions, and habits

149
00:09:04,060 --> 00:09:07,160
 of interactions that are affected

150
00:09:07,160 --> 00:09:09,120
 by the physical.

151
00:09:09,120 --> 00:09:12,610
 When we turn to the physical, we're in this realm of asking

152
00:09:12,610 --> 00:09:14,560
 this question, can meditation

153
00:09:14,560 --> 00:09:18,080
 do X, Y, and Z?

154
00:09:18,080 --> 00:09:20,080
 And it's actually two different questions when we ask

155
00:09:20,080 --> 00:09:21,520
 whether meditation can do something

156
00:09:21,520 --> 00:09:23,280
 and whether mindfulness can do something.

157
00:09:23,280 --> 00:09:26,430
 And an important part of this question that, from my

158
00:09:26,430 --> 00:09:28,760
 perspective, is a clarification of

159
00:09:28,760 --> 00:09:36,090
 what mindfulness does, what mindfulness meditation is for,

160
00:09:36,090 --> 00:09:38,840
 what it is meant to do.

161
00:09:38,840 --> 00:09:41,920
 Now, it's certainly possible that you might sit down and

162
00:09:41,920 --> 00:09:43,960
 practice meditation and mindfulness

163
00:09:43,960 --> 00:09:46,600
 and you suddenly have all sorts of strange things happen.

164
00:09:46,600 --> 00:09:49,800
 Maybe you remember your past life, maybe you start floating

165
00:09:49,800 --> 00:09:50,560
 in the air.

166
00:09:50,560 --> 00:09:52,040
 Certainly this has happened to people.

167
00:09:52,040 --> 00:09:56,400
 Not very common, but apparently yes.

168
00:09:56,400 --> 00:10:00,040
 Many people, this is relatively common, where they'll hear

169
00:10:00,040 --> 00:10:01,760
 things that aren't there.

170
00:10:01,760 --> 00:10:03,870
 And there's a speculation that they're hearing things far

171
00:10:03,870 --> 00:10:04,240
 away.

172
00:10:04,240 --> 00:10:07,920
 Some people even claim to hear things far away, see things

173
00:10:07,920 --> 00:10:11,000
 far away, leave their body.

174
00:10:11,000 --> 00:10:12,000
 That's a common one.

175
00:10:12,000 --> 00:10:14,760
 Someone will leave their body and look down at themselves.

176
00:10:14,760 --> 00:10:17,990
 They'll be looking down, "Oh, there's me sitting in

177
00:10:17,990 --> 00:10:19,120
 meditation."

178
00:10:19,120 --> 00:10:20,440
 Yes, you hear.

179
00:10:20,440 --> 00:10:23,400
 True, false, some people are probably quite skeptical.

180
00:10:23,400 --> 00:10:26,840
 But these are the kinds of things that can happen.

181
00:10:26,840 --> 00:10:30,320
 Could mindfulness meditation do many, many things?

182
00:10:30,320 --> 00:10:32,680
 Sure.

183
00:10:32,680 --> 00:10:36,140
 It's important to understand that none of those things are

184
00:10:36,140 --> 00:10:38,000
 what mindfulness meditation

185
00:10:38,000 --> 00:10:39,000
 is for.

186
00:10:39,000 --> 00:10:41,040
 And you might say, "Well, that's not a problem."

187
00:10:41,040 --> 00:10:43,860
 It's interesting to know that there are byproducts.

188
00:10:43,860 --> 00:10:47,570
 But the other thing is, the desire for those things to

189
00:10:47,570 --> 00:10:50,240
 happen prevents you from practicing

190
00:10:50,240 --> 00:10:52,620
 mindfulness.

191
00:10:52,620 --> 00:10:56,070
 Even if you desire something like remembering past lives,

192
00:10:56,070 --> 00:10:57,800
 any time you want to remember

193
00:10:57,800 --> 00:11:01,280
 your past life, you're no longer practicing mindfulness.

194
00:11:01,280 --> 00:11:03,480
 You're now engaging in a desire for something.

195
00:11:03,480 --> 00:11:05,860
 You might even think it's a wholesome desire or a wholesome

196
00:11:05,860 --> 00:11:07,160
 intention, but it's no longer

197
00:11:07,160 --> 00:11:08,960
 mindfulness.

198
00:11:08,960 --> 00:11:12,580
 And so if you desire for, if your intention, this is the

199
00:11:12,580 --> 00:11:13,340
 point.

200
00:11:13,340 --> 00:11:16,740
 If your intention in practicing mindfulness meditation has

201
00:11:16,740 --> 00:11:17,800
 anything to do with any of

202
00:11:17,800 --> 00:11:21,120
 those things, healing the body, healing the brain, and

203
00:11:21,120 --> 00:11:23,160
 healing your cold, healing your

204
00:11:23,160 --> 00:11:27,840
 eyesight, making it more beautiful or something like that,

205
00:11:27,840 --> 00:11:30,680
 anything, even let's say wholesome

206
00:11:30,680 --> 00:11:34,140
 things like maybe making me healthier or so on, then it

207
00:11:34,140 --> 00:11:36,060
 detracts from your ability to

208
00:11:36,060 --> 00:11:37,880
 practice mindfulness meditation.

209
00:11:37,880 --> 00:11:41,200
 It actually makes it no longer mindfulness.

210
00:11:41,200 --> 00:11:44,630
 It's an important point to keep in mind that when you ask

211
00:11:44,630 --> 00:11:46,880
 these questions, the real problem

212
00:11:46,880 --> 00:11:49,320
 is that you want for those things to happen.

213
00:11:49,320 --> 00:11:54,150
 And the asking of the question puts you at an important

214
00:11:54,150 --> 00:11:57,040
 dissonance in terms of mindfulness

215
00:11:57,040 --> 00:11:59,680
 practice because if that's what's bringing you to

216
00:11:59,680 --> 00:12:02,140
 meditation, to mindfulness meditation,

217
00:12:02,140 --> 00:12:05,940
 you're not going to be practicing mindfulness meditation

218
00:12:05,940 --> 00:12:08,080
 until you realize that it's getting

219
00:12:08,080 --> 00:12:11,260
 in the way of not just you practicing this meditation that

220
00:12:11,260 --> 00:12:12,800
 I teach, but it's getting

221
00:12:12,800 --> 00:12:15,840
 in the way of you finding freedom from suffering.

222
00:12:15,840 --> 00:12:20,960
 Your desire for freedom from brain damage even is going to

223
00:12:20,960 --> 00:12:23,480
 get in the way of you finding

224
00:12:23,480 --> 00:12:24,680
 freedom from suffering, right?

225
00:12:24,680 --> 00:12:28,760
 So suppose brain damage, I got really sick in Sri Lanka and

226
00:12:28,760 --> 00:12:30,480
 I had the feeling that it

227
00:12:30,480 --> 00:12:32,320
 changed how my brain works.

228
00:12:32,320 --> 00:12:35,280
 And so some memory isn't quite as good as it used to be.

229
00:12:35,280 --> 00:12:38,480
 I'm also just getting older, so it could be that.

230
00:12:38,480 --> 00:12:43,160
 But if I want for that to change, if I have this intention,

231
00:12:43,160 --> 00:12:45,480
 and this is often the thing,

232
00:12:45,480 --> 00:12:47,930
 you have a desire to remember better, there's even claims

233
00:12:47,930 --> 00:12:49,620
 that mindfulness meditation improves

234
00:12:49,620 --> 00:12:50,620
 your memory.

235
00:12:50,620 --> 00:12:51,620
 I think for some people it does.

236
00:12:51,620 --> 00:12:55,130
 I've also met other meditators, mindfulness meditators who

237
00:12:55,130 --> 00:12:57,100
 don't have very good memory,

238
00:12:57,100 --> 00:13:01,960
 even though their mindfulness is quite good, it seems.

239
00:13:01,960 --> 00:13:04,560
 And I think it does depend to some extent on the capacity

240
00:13:04,560 --> 00:13:06,120
 of the brain, the functioning

241
00:13:06,120 --> 00:13:16,000
 of the brain and so on.

242
00:13:16,000 --> 00:13:24,770
 And so there's this relationship and the mind can in some

243
00:13:24,770 --> 00:13:29,560
 cases heal the brain, there might

244
00:13:29,560 --> 00:13:34,680
 be healing that goes on, but the desire for it, the

245
00:13:34,680 --> 00:13:38,440
 intention, the concern even for the

246
00:13:38,440 --> 00:13:43,060
 healing of the brain is a problem.

247
00:13:43,060 --> 00:13:48,000
 And so really not as the most important point, because it's

248
00:13:48,000 --> 00:13:50,160
 not so interesting to me, or

249
00:13:50,160 --> 00:13:53,660
 it's not something that I wish to make interesting, but I

250
00:13:53,660 --> 00:13:56,360
 think there are other types of meditation

251
00:13:56,360 --> 00:14:01,560
 that potentially have the express benefit of healing the

252
00:14:01,560 --> 00:14:04,480
 brain, let's say, or healing

253
00:14:04,480 --> 00:14:07,710
 the body, or helping you remember past lives, obviously,

254
00:14:07,710 --> 00:14:09,760
 helping you float through the air,

255
00:14:09,760 --> 00:14:11,720
 helping you leave your body.

256
00:14:11,720 --> 00:14:15,230
 Lots of teachings, even in Buddhism, outside of Buddhism,

257
00:14:15,230 --> 00:14:17,160
 by Buddhists, you'll hear them

258
00:14:17,160 --> 00:14:19,600
 talk about these things, so many different kinds.

259
00:14:19,600 --> 00:14:21,240
 And I think that's all valid.

260
00:14:21,240 --> 00:14:24,470
 I mean, science would reject a lot of it, like let's say I

261
00:14:24,470 --> 00:14:26,400
 say, I can give you a meditation

262
00:14:26,400 --> 00:14:29,550
 that's going to cure your cancer, scientists would say that

263
00:14:29,550 --> 00:14:31,320
's both ridiculous and reckless

264
00:14:31,320 --> 00:14:33,400
 to suggest such a thing.

265
00:14:33,400 --> 00:14:36,780
 That doesn't really have any bearing on me saying it's

266
00:14:36,780 --> 00:14:37,600
 possible.

267
00:14:37,600 --> 00:14:41,750
 It's possible as far as I know, anything's possible as far

268
00:14:41,750 --> 00:14:44,240
 as I know, but partially because

269
00:14:44,240 --> 00:14:46,720
 I just don't understand science.

270
00:14:46,720 --> 00:14:49,870
 I'm not a material scientist to the extent that I can tell

271
00:14:49,870 --> 00:14:51,680
 you what's physically possible

272
00:14:51,680 --> 00:14:54,920
 and what's physically impossible.

273
00:14:54,920 --> 00:14:58,560
 Could meditation regrow a lost limb?

274
00:14:58,560 --> 00:15:00,880
 I think yes, it could.

275
00:15:00,880 --> 00:15:03,580
 Science would, of course, many of you even here are

276
00:15:03,580 --> 00:15:05,840
 probably saying that's ridiculous,

277
00:15:05,840 --> 00:15:07,800
 it's impossible.

278
00:15:07,800 --> 00:15:10,160
 But that's not really what I mean by possible.

279
00:15:10,160 --> 00:15:14,270
 It's possible in the sense that I don't have proof and I'm

280
00:15:14,270 --> 00:15:16,840
 not really interested in finding

281
00:15:16,840 --> 00:15:20,160
 proof, it's not in my realm of what I can prove to say yes

282
00:15:20,160 --> 00:15:21,880
 or no, it's just something

283
00:15:21,880 --> 00:15:22,880
 out there.

284
00:15:22,880 --> 00:15:26,500
 The more important point is that, again, when you are

285
00:15:26,500 --> 00:15:28,720
 practicing those meditations, it's

286
00:15:28,720 --> 00:15:30,560
 to your detriment.

287
00:15:30,560 --> 00:15:33,790
 It's generally to your detriment to be practicing

288
00:15:33,790 --> 00:15:36,680
 meditation to heal this or to heal that because

289
00:15:36,680 --> 00:15:40,910
 it's creating clinging, because it's creating distraction,

290
00:15:40,910 --> 00:15:43,080
 because it's reinforcing your

291
00:15:43,080 --> 00:15:46,800
 attachment to things like health, to things like the proper

292
00:15:46,800 --> 00:15:48,960
 functioning of the brain even.

293
00:15:48,960 --> 00:15:51,430
 If you want to remember better, it's going to get in the

294
00:15:51,430 --> 00:15:52,920
 way of your meditation because

295
00:15:52,920 --> 00:15:55,630
 you become attached to remembering and it's always

296
00:15:55,630 --> 00:15:58,000
 temporary and when it goes away, etc.,

297
00:15:58,000 --> 00:16:02,680
 etc., you're going to be upset and so on.

298
00:16:02,680 --> 00:16:06,550
 The point of this and the point of me answering this

299
00:16:06,550 --> 00:16:09,400
 question that I want to get across is

300
00:16:09,400 --> 00:16:19,400
 that mindfulness requires letting go.

301
00:16:19,400 --> 00:16:22,640
 That mindfulness meditation, the four foundations of

302
00:16:22,640 --> 00:16:26,280
 mindfulness for the purpose of seeing insight,

303
00:16:26,280 --> 00:16:29,430
 requires you really to not have any ambition, any desire,

304
00:16:29,430 --> 00:16:31,680
 even the desire to become enlightened.

305
00:16:31,680 --> 00:16:32,680
 It's going to get in your way.

306
00:16:32,680 --> 00:16:36,040
 It has to be about being here and now.

307
00:16:36,040 --> 00:16:39,480
 People say, "Well, then what about wanting to meditate?"

308
00:16:39,480 --> 00:16:43,530
 It's really not a proper question or a proper concern

309
00:16:43,530 --> 00:16:46,760
 because of the nature of mindfulness.

310
00:16:46,760 --> 00:16:49,120
 Mindfulness is actually nothing.

311
00:16:49,120 --> 00:16:51,020
 It's stopping everything.

312
00:16:51,020 --> 00:16:52,960
 Are you here and now?

313
00:16:52,960 --> 00:16:55,480
 It's not a cult because it's nothing.

314
00:16:55,480 --> 00:17:02,080
 Me sitting here now, when I can just sit here now without

315
00:17:02,080 --> 00:17:06,280
 belief, without dogma, without

316
00:17:06,280 --> 00:17:09,840
 any kind of ambition or desire, that's enlightenment.

317
00:17:09,840 --> 00:17:12,880
 When a person can do that, it's not anything.

318
00:17:12,880 --> 00:17:17,880
 It's not even a religion really.

319
00:17:17,880 --> 00:17:23,080
 It's the religion of losing your religion.

320
00:17:23,080 --> 00:17:27,250
 So in the beginning, there's many desires and I think the

321
00:17:27,250 --> 00:17:29,040
 best we can do and what's

322
00:17:29,040 --> 00:17:34,160
 important here is to stop with extraneous desires.

323
00:17:34,160 --> 00:17:38,190
 If you desire to become enlightened, as I said, it's going

324
00:17:38,190 --> 00:17:39,640
 to get in your way.

325
00:17:39,640 --> 00:17:44,140
 But it's better than desiring for meditation to straighten

326
00:17:44,140 --> 00:17:46,520
 your teeth or fix your eyesight

327
00:17:46,520 --> 00:17:52,080
 or even fix your brain, your memory.

328
00:17:52,080 --> 00:17:54,320
 I hope that was useful.

329
00:17:54,320 --> 00:17:56,880
 I think we might turn off these comments.

330
00:17:56,880 --> 00:17:58,960
 This is the internet.

331
00:17:58,960 --> 00:18:00,720
 It's a scary place.

332
00:18:00,720 --> 00:18:03,240
 I think it's distracting.

333
00:18:03,240 --> 00:18:07,240
 I appreciate those of you who are listening keenly, but I

334
00:18:07,240 --> 00:18:09,440
 really would like it to be like

335
00:18:09,440 --> 00:18:10,440
 that.

336
00:18:10,440 --> 00:18:13,760
 I know the novel thing about live streaming is you can

337
00:18:13,760 --> 00:18:15,760
 interact with a broadcaster.

338
00:18:15,760 --> 00:18:19,640
 It's not really what this is about.

339
00:18:19,640 --> 00:18:22,390
 Maybe we could have a stream where I do interact with the

340
00:18:22,390 --> 00:18:23,120
 comments.

341
00:18:23,120 --> 00:18:24,800
 I do talk to people in the comments.

342
00:18:24,800 --> 00:18:28,500
 The problem with that, of course, is we get these sorts of

343
00:18:28,500 --> 00:18:29,400
 comments.

344
00:18:29,400 --> 00:18:33,800
 Here, I'll scroll through them.

345
00:18:33,800 --> 00:18:38,720
 Sounds like a cult.

346
00:18:38,720 --> 00:18:40,320
 A lot of these are good.

347
00:18:40,320 --> 00:18:44,360
 No, I have a really good audience.

348
00:18:44,360 --> 00:18:47,680
 It's really incredible.

349
00:18:47,680 --> 00:18:49,680
 There's someone asking me.

350
00:18:49,680 --> 00:18:52,800
 Someone called Truth Is Within asking me when I'll be back

351
00:18:52,800 --> 00:18:54,680
 in Ontario, calling me Noah.

352
00:18:54,680 --> 00:18:57,680
 That's interesting.

353
00:18:57,680 --> 00:19:04,920
 I think you've got some very good comments here, but the

354
00:19:04,920 --> 00:19:07,160
 point stands.

355
00:19:07,160 --> 00:19:10,920
 Listening to the dhamma should be about listening.

356
00:19:10,920 --> 00:19:13,110
 Maybe we'll have times where I turn off the comments and

357
00:19:13,110 --> 00:19:14,280
 then we'll have streams where

358
00:19:14,280 --> 00:19:19,280
 I have the comments on and we can have a chat.

359
00:19:19,280 --> 00:19:23,080
 A little chat with the internet.

360
00:19:23,080 --> 00:19:25,880
 What could possibly go wrong?

361
00:19:25,880 --> 00:19:28,120
 Anyway, thank you all for tuning in.

362
00:19:28,120 --> 00:19:29,120
 74 people watching.

363
00:19:29,120 --> 00:19:30,120
 That's great.

364
00:19:30,120 --> 00:19:30,120
 I wish you all the best.

365
00:19:30,120 --> 00:19:32,120
 [END]

366
00:19:32,120 --> 00:19:34,120
 1

