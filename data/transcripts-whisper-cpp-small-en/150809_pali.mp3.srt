1
00:00:00,000 --> 00:00:07,680
 Okay, so let's get started today. We're starting on Ba-Lung

2
00:00:07,680 --> 00:00:09,360
, not Se-Wei. Yeah, we're doing the

3
00:00:09,360 --> 00:00:13,350
 rest of the questions at the end of the, not questions,

4
00:00:13,350 --> 00:00:14,620
 rest of the sample

5
00:00:14,620 --> 00:00:19,380
 sentences at the end of the text.

6
00:00:24,100 --> 00:00:30,880
 So number 18, if you scroll right down to the bottom, Ba-L

7
00:00:30,880 --> 00:00:36,000
ung, Ba-Lung, not Se-Wei.

8
00:00:36,000 --> 00:00:42,480
 Can anyone tell me what that means?

9
00:00:42,480 --> 00:00:48,920
 Ba-Lung is young or child, right?

10
00:00:48,920 --> 00:00:52,720
 Ba-Lung means a fool.

11
00:00:54,720 --> 00:00:59,580
 That's in our say or not, Je-Bola, no.

12
00:00:59,580 --> 00:01:09,660
 And nigh is not? Yes. So the fool's not doing something?

13
00:01:09,660 --> 00:01:14,100
 What case is the fool in?

14
00:01:19,780 --> 00:01:26,860
 2-1. So is that the subject of the sentence?

15
00:01:26,860 --> 00:01:33,060
 The object of the sentence. Yes.

16
00:01:33,060 --> 00:01:40,980
 Where's the subject?

17
00:01:40,980 --> 00:01:46,260
 The subject must be Se-Wei.

18
00:01:46,260 --> 00:01:49,460
 Must be?

19
00:01:49,780 --> 00:01:54,910
 Well, the fool is the object, so I'm assuming Se-Wei must

20
00:01:54,910 --> 00:01:57,020
 be the subject now.

21
00:01:57,020 --> 00:02:02,490
 Well, most or many Pali sentences don't even have a subject

22
00:02:02,490 --> 00:02:02,860
.

23
00:02:02,860 --> 00:02:07,910
 It's implicit in the verb. Se-Wei is actually a verb. E-Ya

24
00:02:07,910 --> 00:02:08,500
 is the

25
00:02:08,500 --> 00:02:14,420
 co-optitive ending, third person singular.

26
00:02:14,420 --> 00:02:22,420
 You know, you can plug these into the digital Pali reader

27
00:02:22,420 --> 00:02:23,060
 if you have it.

28
00:02:23,060 --> 00:02:29,510
 You can go to the text pad, TP on the bottom, and paste it

29
00:02:29,510 --> 00:02:31,260
 in, and then click

30
00:02:31,260 --> 00:02:38,120
 analyze. Yeah, except it's not don't, because don't would

31
00:02:38,120 --> 00:02:39,700
 be second person.

32
00:02:39,700 --> 00:02:44,140
 They would be talking to second person. Se-Wei-Ya is not

33
00:02:44,140 --> 00:02:45,780
 second person. So it's

34
00:02:45,780 --> 00:02:50,990
 some third person advice. He, she, or it, or in this case

35
00:02:50,990 --> 00:02:53,300
 one, one should not.

36
00:02:53,300 --> 00:02:56,340
 Another reason for leaving out the subject is because you

37
00:02:56,340 --> 00:02:57,300
're talking about

38
00:02:57,300 --> 00:03:05,780
 the general third person. One should not associate. So this

39
00:03:05,780 --> 00:03:06,860
 is like a Se-Wuna.

40
00:03:06,860 --> 00:03:12,460
 Se-Wuna is the, Se-Wuna is the noun. Se-Wuna means associ

41
00:03:12,460 --> 00:03:14,300
ating, association.

42
00:03:14,300 --> 00:03:23,510
 Se-Wuna means non-association. Se-Wuna is the root. Se-Wat-

43
00:03:23,510 --> 00:03:24,700
Se-Wati.

44
00:03:24,700 --> 00:03:29,980
 Se-Wei-Ya. Se-Wuna.

45
00:03:29,980 --> 00:03:38,940
 Pandita-Meva-Bhajatu.

46
00:03:38,940 --> 00:03:54,700
 Any ideas? Pandita is the vice.

47
00:03:56,620 --> 00:04:07,420
 Bhajatu. Bhajatu comes from Bhajana.

48
00:04:07,420 --> 00:04:11,250
 Bhaj, it's sort of to do with partaking. So, bunjati, it's,

49
00:04:11,250 --> 00:04:14,700
 it's similar, but it means to

50
00:04:14,700 --> 00:04:22,380
 get involved with, or to be associated with, similarly.

51
00:04:24,540 --> 00:04:29,980
 To be close to, to partake in a sense of friendship.

52
00:04:29,980 --> 00:04:35,440
 I see. So it's similar to Se-Wei-Ya or Se-Wuna. Yeah, the

53
00:04:35,440 --> 00:04:37,980
 meaning is similar.

54
00:04:37,980 --> 00:04:41,660
 So then associate with the wise.

55
00:04:41,660 --> 00:04:47,820
 What form is bhajatu?

56
00:04:47,820 --> 00:04:52,780
 Bhajatu.

57
00:04:52,780 --> 00:05:01,340
 One-One.

58
00:05:01,340 --> 00:05:08,780
 Was it first person, second person, third person?

59
00:05:08,780 --> 00:05:19,740
 Naga says first. That sounds good.

60
00:05:19,740 --> 00:05:26,510
 Well, actually, one-one would be third person, because in

61
00:05:26,510 --> 00:05:28,380
 Pali, the third person is, they call

62
00:05:28,380 --> 00:05:32,560
 it the first person. What we call the third person, they

63
00:05:32,560 --> 00:05:34,540
 call the first person.

64
00:05:35,340 --> 00:05:37,500
 Patama police.

65
00:05:37,500 --> 00:05:51,500
 And what tense is it?

66
00:05:51,500 --> 00:05:52,460
 Bhajatu.

67
00:05:52,460 --> 00:05:53,420
 Bhajatu.

68
00:05:53,420 --> 00:05:54,380
 Bhajatu.

69
00:05:54,380 --> 00:05:55,340
 Bhajatu.

70
00:05:55,340 --> 00:05:56,300
 Bhajatu.

71
00:05:56,300 --> 00:05:57,260
 Bhajatu.

72
00:05:57,260 --> 00:05:58,220
 Bhajatu.

73
00:05:58,220 --> 00:05:59,180
 Bhajatu.

74
00:05:59,180 --> 00:06:00,140
 Bhajatu.

75
00:06:00,140 --> 00:06:01,100
 Bhajatu.

76
00:06:01,100 --> 00:06:02,060
 Bhajatu.

77
00:06:02,060 --> 00:06:03,020
 Bhajatu.

78
00:06:03,020 --> 00:06:03,980
 Bhajatu.

79
00:06:03,980 --> 00:06:04,940
 Bhajatu.

80
00:06:04,940 --> 00:06:05,900
 Bhajatu.

81
00:06:05,900 --> 00:06:06,860
 Bhajatu.

82
00:06:35,260 --> 00:06:37,980
 Anyone? Is anyone trying these?

83
00:06:37,980 --> 00:06:43,680
 I guess some of you are, no? I guess no one did any over

84
00:06:43,680 --> 00:06:44,700
 the week.

85
00:06:44,700 --> 00:06:50,860
 I looked at them, but I'm not, you know, what I have is not

86
00:06:50,860 --> 00:06:54,220
 very clear.

87
00:07:04,380 --> 00:07:11,320
 Okay, well, Bhajatu, the root is bhaj, and you add an 'a'

88
00:07:11,320 --> 00:07:12,620
 and 'du'.

89
00:07:12,620 --> 00:07:17,880
 Bhajatu is the stem, and then you add 'du' on the end. So

90
00:07:17,880 --> 00:07:19,660
 where do we see 'du'?

91
00:07:19,660 --> 00:07:24,620
 What form would 'du' be? Like, why did you guys say it was

92
00:07:24,620 --> 00:07:26,700
 a one-one? One-one of what? What tense?

93
00:07:26,700 --> 00:07:33,260
 There's an imperative form with a 'du' ending.

94
00:07:33,900 --> 00:07:36,110
 There is indeed. In fact, that's what you should always

95
00:07:36,110 --> 00:07:37,740
 think of, because 'du' always,

96
00:07:37,740 --> 00:07:42,440
 if you see 'du' on the end of a verb or 'attu' especially,

97
00:07:42,440 --> 00:07:44,540
 right away it's imperative one-one,

98
00:07:44,540 --> 00:07:50,380
 third person singular, which means may he, she, or it do

99
00:07:50,380 --> 00:07:52,380
 something.

100
00:07:52,380 --> 00:07:58,560
 In this case, should. It's more not may, but should. One

101
00:07:58,560 --> 00:08:00,620
 should. Again, like say,

102
00:08:00,620 --> 00:08:02,140
 waya is also a third person singular, but what bhajatu is

103
00:08:02,140 --> 00:08:05,340
 also third person singular,

104
00:08:05,340 --> 00:08:09,040
 say waya is satami, which we call the optative, and bhajatu

105
00:08:09,040 --> 00:08:11,420
 is panchami, which we call the imperative.

106
00:08:11,420 --> 00:08:17,330
 So this sentence means, you have to take the 'a' into

107
00:08:17,330 --> 00:08:19,340
 account, 'a' means

108
00:08:19,340 --> 00:08:26,480
 indeed, or verily, it's emphatic in some way, indeed one

109
00:08:26,480 --> 00:08:29,180
 should, or rather one should,

110
00:08:29,180 --> 00:08:31,740
 you could say, I suppose, but it's more like indeed one

111
00:08:31,740 --> 00:08:36,860
 should keep companionship with wise people,

112
00:08:36,860 --> 00:08:41,060
 with a wise man. So if someone meets a wise person, they

113
00:08:41,060 --> 00:08:43,340
 should keep company with them.

114
00:08:43,340 --> 00:08:50,140
 The next sentence is a relative-co-relative. You got the y

115
00:08:50,140 --> 00:08:53,900
aa and da. The kyt system,

116
00:08:54,460 --> 00:08:57,060
 k is for questions, but yaa and da are for relative-co-

117
00:08:57,060 --> 00:08:59,500
relative. So the first one is,

118
00:08:59,500 --> 00:09:05,490
 in English, we would say the wt. So yang is the ws and t is

119
00:09:05,490 --> 00:09:09,980
, and dang is the t. So yang means what,

120
00:09:09,980 --> 00:09:15,730
 and dang means that. So it would go in this form, what so

121
00:09:15,730 --> 00:09:21,420
 and so does that you should do.

122
00:09:21,420 --> 00:09:27,250
 In fact, that's exactly what this sentence means. What yang

123
00:09:27,250 --> 00:09:30,460
 is the object of the sentence. So

124
00:09:30,460 --> 00:09:37,560
 what a dira, wise man, may do. In this case, this is satt

125
00:09:37,560 --> 00:09:39,740
ami, but it's not in the sense of should,

126
00:09:39,740 --> 00:09:44,640
 it should, in the sense of may, in the sense whatever they

127
00:09:44,640 --> 00:09:46,940
 should do, whatever they may do,

128
00:09:48,140 --> 00:09:52,240
 that's why it's called opti-tive. Whatever they should do,

129
00:09:52,240 --> 00:09:52,940
 that you should do.

130
00:09:52,940 --> 00:10:00,020
 That you should do. Karasu is second person, singular, and

131
00:10:00,020 --> 00:10:01,740
 again imperative, but it's in

132
00:10:01,740 --> 00:10:04,990
 a regular form because kara is an irregular verb. So it's

133
00:10:04,990 --> 00:10:07,820
 all going to look weird. But if you click

134
00:10:07,820 --> 00:10:11,240
 on karasu in this digital poly-reader, and then you click

135
00:10:11,240 --> 00:10:13,340
 on the little c that comes up beside the

136
00:10:13,340 --> 00:10:17,410
 word, beside the definition, you scroll down and you can

137
00:10:17,410 --> 00:10:20,220
 see that it's a 2-1 imperative.

138
00:10:20,220 --> 00:10:22,940
 Oh, that's cool.

139
00:10:22,940 --> 00:10:34,540
 Yang diro kare ya, whatever a wise person should do,

140
00:10:35,420 --> 00:10:40,760
 dang karasu, that you should do. So we start the sentence

141
00:10:40,760 --> 00:10:43,740
 with what, but what is the object of the

142
00:10:43,740 --> 00:10:48,180
 sentence? But in order to have it make sense, we put it

143
00:10:48,180 --> 00:10:51,340
 first because the emphasis is on the what and

144
00:10:51,340 --> 00:10:55,720
 the that. Yang is what, dang is that. This is a very simple

145
00:10:55,720 --> 00:10:58,700
 relative, correlative sentence and

146
00:10:58,700 --> 00:11:10,380
 they get a lot more difficult than this. The next one.

147
00:11:10,380 --> 00:11:16,580
 The next one's difficult because ubatjaga is a very, very

148
00:11:16,580 --> 00:11:21,820
 irregular verb. It means to pass by.

149
00:11:21,820 --> 00:11:35,660
 And in this form, it means it's imperative. So it means may

150
00:11:35,660 --> 00:11:39,500
 it pass you by or it should pass you by.

151
00:11:48,940 --> 00:11:52,780
 And the negative of the imperative uses ma instead of na.

152
00:11:52,780 --> 00:11:55,900
 All other verb forms use na,

153
00:11:55,900 --> 00:12:01,170
 but the imperative uses ma. So it's just like na, but it's

154
00:12:01,170 --> 00:12:03,500
 the negative. You should not or they

155
00:12:03,500 --> 00:12:08,810
 should not. It should not. May it not in this case, may it

156
00:12:08,810 --> 00:12:16,300
 not. So may it not pass you by.

157
00:12:18,300 --> 00:12:20,060
 And what is the subject of the sentence?

158
00:12:20,060 --> 00:12:31,260
 Kanou. And what does kana mean? Moment. So the moment and

159
00:12:31,260 --> 00:12:34,940
 then wo is a short form of the

160
00:12:34,940 --> 00:12:41,720
 second person plural, personal pronoun, you. So it's you

161
00:12:41,720 --> 00:12:46,700
 plural and it's in this case a two one,

162
00:12:46,700 --> 00:12:53,100
 a two two sorry two two form of of or you.

163
00:12:53,100 --> 00:13:02,220
 So woe is this the object of the sentence means you all.

164
00:13:12,460 --> 00:13:16,410
 Woe and no and they these are very common. They is the

165
00:13:16,410 --> 00:13:19,100
 common plural short form plural for third

166
00:13:19,100 --> 00:13:26,140
 person and or also second person singular. Woe is is third

167
00:13:26,140 --> 00:13:30,540
 person plural singular third person plural

168
00:13:30,540 --> 00:13:35,950
 and no is first person plural. May is is first person

169
00:13:35,950 --> 00:13:38,300
 singular. So for the for the for the

170
00:13:38,300 --> 00:13:41,980
 pronouns they're all shortened to may and no. That's the

171
00:13:41,980 --> 00:13:46,060
 first person forms and day and woe.

172
00:13:46,060 --> 00:13:52,900
 That's the second person second person forms third person.

173
00:13:52,900 --> 00:13:55,500
 Third person doesn't really have that

174
00:13:55,500 --> 00:13:58,460
 right. So it's first and second person pronouns have

175
00:13:58,460 --> 00:14:04,780
 shortened forms. May and no and day and woe.

176
00:14:04,780 --> 00:14:24,540
 So what does this sentence mean?

177
00:14:34,060 --> 00:14:37,470
 May you not let the moment I think I have their tense wrong

178
00:14:37,470 --> 00:14:39,100
 but not let the moment pass you by.

179
00:14:39,100 --> 00:14:50,140
 What's the subject again? Kano which means moment. So in

180
00:14:50,140 --> 00:14:52,220
 this case it's made a subject

181
00:14:52,220 --> 00:14:56,360
 when you translate this verb. May the moment not pass you

182
00:14:56,360 --> 00:14:57,820
 by. That's right.

183
00:14:57,820 --> 00:15:07,580
 So

184
00:15:25,900 --> 00:15:30,570
 okay this next one is interesting. Kana kana tita is one

185
00:15:30,570 --> 00:15:32,860
 whose moment is in the past.

186
00:15:32,860 --> 00:15:38,710
 One for whom the moment has passed. It's an interesting

187
00:15:38,710 --> 00:15:39,660
 word I didn't

188
00:15:39,660 --> 00:15:40,860
 when they thought about that one.

189
00:15:40,860 --> 00:15:47,900
 What does this sentence mean?

190
00:15:47,900 --> 00:16:01,980
 Is that Soh Kanti bhante? Soh Chanti.

191
00:16:07,980 --> 00:16:14,470
 Sorrow. Soh Chanti one whose moment has passed away will g

192
00:16:14,470 --> 00:16:17,340
rieve or something like that.

193
00:16:17,340 --> 00:16:20,060
 What form is Soh Chanti?

194
00:16:20,060 --> 00:16:20,140
 Soh Chanti.

195
00:16:20,140 --> 00:16:20,220
 Soh Chanti.

196
00:16:20,220 --> 00:16:24,300
 Soh Chanti.

197
00:16:24,300 --> 00:16:28,380
 Soh Chanti.

198
00:16:28,380 --> 00:16:32,460
 Soh Chanti.

199
00:16:32,460 --> 00:16:36,540
 Soh Chanti.

200
00:16:36,540 --> 00:16:40,620
 Soh Chanti.

201
00:16:40,620 --> 00:16:44,700
 Soh Chanti.

202
00:16:44,700 --> 00:16:48,780
 Soh Chanti.

203
00:16:48,780 --> 00:16:52,860
 Soh Chanti.

204
00:16:52,860 --> 00:16:56,940
 Soh Chanti.

205
00:16:56,940 --> 00:17:01,020
 Soh Chanti.

206
00:17:01,020 --> 00:17:05,100
 Soh Chanti.

207
00:17:05,100 --> 00:17:09,180
 Soh Chanti.

208
00:17:09,180 --> 00:17:13,260
 Soh Chanti.

209
00:17:13,260 --> 00:17:17,340
 Soh Chanti.

210
00:17:17,340 --> 00:17:23,420
 Soh Chanti.

211
00:17:23,500 --> 00:17:27,500
 Soh Chanti.

212
00:17:27,500 --> 00:17:31,580
 Soh Chanti.

213
00:17:31,580 --> 00:17:35,660
 Soh Chanti.

214
00:17:35,660 --> 00:17:39,740
 Soh Chanti.

215
00:17:39,740 --> 00:17:43,820
 Soh Chanti.

216
00:17:43,820 --> 00:17:47,900
 Soh Chanti.

217
00:17:47,900 --> 00:17:51,980
 Soh Chanti.

218
00:17:51,980 --> 00:17:56,060
 Soh Chanti.

219
00:17:56,060 --> 00:18:00,140
 Soh Chanti.

220
00:18:23,660 --> 00:18:27,740
 No one knows what Soh Chanti, what form it is.

221
00:18:27,740 --> 00:18:48,860
 Present plural third person? Yes. So it means they sorrow.

222
00:18:48,860 --> 00:18:54,460
 Who are the they in this case?

223
00:18:54,460 --> 00:18:56,620
 Kanadita.

224
00:18:56,620 --> 00:19:04,030
 Yes. What form is Kanadita in? That ending doesn't look

225
00:19:04,030 --> 00:19:04,780
 familiar at all.

226
00:19:04,780 --> 00:19:07,980
 Oh.

227
00:19:07,980 --> 00:19:16,620
 Are you saying one of the paradigms ends with ITA?

228
00:19:18,380 --> 00:19:24,210
 Oh what is this word? Kana means means moment. What's the

229
00:19:24,210 --> 00:19:25,980
 rest of it?

230
00:19:25,980 --> 00:19:42,280
 If you use the digital Pali reader and you look at that

231
00:19:42,280 --> 00:19:43,980
 little white box below the word,

232
00:19:47,420 --> 00:19:49,260
 you can separate it into two parts.

233
00:19:49,260 --> 00:19:59,100
 The second option is Kana and Adi, Adiita. And Adiita means

234
00:19:59,100 --> 00:19:59,660
 past.

235
00:19:59,660 --> 00:20:05,480
 So the word here is Kana, Kanadita. That's the stem word.

236
00:20:05,480 --> 00:20:07,020
 So it's not an ITA ending.

237
00:20:07,020 --> 00:20:09,660
 Oh I see.

238
00:20:09,660 --> 00:20:14,840
 So according to digital Pali reader, Kana is either a

239
00:20:14,840 --> 00:20:16,700
 moment, a minute, or an opportunity.

240
00:20:16,700 --> 00:20:19,500
 So yeah. And what is Adiita?

241
00:20:19,500 --> 00:20:35,420
 Missed opportunity. The cost.

242
00:20:35,420 --> 00:20:38,220
 Adiita means past.

243
00:20:40,300 --> 00:20:46,140
 So this looks like it means opportunities that are in the

244
00:20:46,140 --> 00:20:47,820
 past. Or yeah, opportunities that

245
00:20:47,820 --> 00:20:54,820
 are in the past. But this kind of compound, it turns it

246
00:20:54,820 --> 00:21:01,580
 into an ownership where it means one who

247
00:21:01,580 --> 00:21:06,650
 has the thing that it's describing. So one whose moment is

248
00:21:06,650 --> 00:21:09,500
 in the past. One whose moment has passed

249
00:21:09,500 --> 00:21:17,110
 them by. One whose moment has already gone. And so it'll be

250
00:21:17,110 --> 00:21:21,020
 masculine in this case. And it's

251
00:21:21,020 --> 00:21:26,000
 masculine plural. This is actually a one-two form. Just

252
00:21:26,000 --> 00:21:30,700
 like Purisa. Purisa is Puriso Purisa.

253
00:21:32,060 --> 00:21:38,570
 One-two is Purisa. So this would be conjugated as Kana-tito

254
00:21:38,570 --> 00:21:43,660
, Kana-tita, Kana-tita, Kana-tite,

255
00:21:43,660 --> 00:21:48,360
 Kana-tite-na, and so on. If you click on the conjugation

256
00:21:48,360 --> 00:21:49,180
 you'll see it.

257
00:21:49,180 --> 00:21:58,300
 It'll give right now it's giving me the conjugation of Adi

258
00:21:58,300 --> 00:21:59,100
ita.

259
00:21:59,100 --> 00:22:05,420
 Adiita, Adiita, Adi...

260
00:22:05,420 --> 00:22:12,540
 Yeah. Adiita, Adiita. It's got the vocative in the wrong

261
00:22:12,540 --> 00:22:17,420
 place. That's interesting.

262
00:22:17,420 --> 00:22:21,740
 So

263
00:22:21,740 --> 00:22:24,060
 you

264
00:22:24,060 --> 00:22:26,380
 you

265
00:22:52,060 --> 00:22:57,340
 so this means people whose moments in the past indeed will

266
00:22:57,340 --> 00:22:58,220
 uh indeed grieve

267
00:22:58,220 --> 00:23:04,840
 means people who let the moment pass them by indeed grieve

268
00:23:04,840 --> 00:23:06,700
 indeed the he is sort of like indeed

269
00:23:06,700 --> 00:23:09,580
 and so chanting they grieve

270
00:23:09,580 --> 00:23:17,230
 the next one's really tough but we've already had this

271
00:23:17,230 --> 00:23:18,460
 example

272
00:23:19,820 --> 00:23:22,420
 well maybe you didn't it's in the book so that's why he's

273
00:23:22,420 --> 00:23:23,020
 giving it

274
00:23:23,020 --> 00:23:26,850
 Dana I think it is above it is in the textbook Dana means

275
00:23:26,850 --> 00:23:31,500
 by by them but aha is difficult because aha is

276
00:23:31,500 --> 00:23:36,620
 past past tense paroka I think

277
00:23:36,620 --> 00:23:42,140
 and so again first person a third person singular

278
00:23:44,380 --> 00:23:50,310
 is the object subject of the sentence means one who is in

279
00:23:50,310 --> 00:23:51,580
 the past one who has lived in

280
00:23:51,580 --> 00:24:02,550
 the ancient times and Dana here is specific meaning it

281
00:24:02,550 --> 00:24:08,060
 means therefore or it's Dana means by that

282
00:24:08,060 --> 00:24:13,750
 literally from that no no by that or with that but here

283
00:24:13,750 --> 00:24:16,700
 means like because of that

284
00:24:16,700 --> 00:24:24,030
 therefore the Purana the old the ancient one has said that

285
00:24:24,030 --> 00:24:28,140
's what this means so the this is what

286
00:24:28,140 --> 00:24:30,590
 you'll see in the commentary is when you when in the wisud

287
00:24:30,590 --> 00:24:32,380
imaga that we're studying he uses this

288
00:24:32,380 --> 00:24:35,750
 kind of phrase quite often they said something and then he

289
00:24:35,750 --> 00:24:37,980
 said for that reason it was said by the

290
00:24:37,980 --> 00:24:41,260
 ancient usually it's plural but here it's singular

291
00:24:41,260 --> 00:24:47,270
 Ponte where is the implication of of said aha is an

292
00:24:47,270 --> 00:24:50,940
 irregular verb it's an irregular past tense of

293
00:24:50,940 --> 00:24:59,700
 what I think right no uh it's very irregular yeah that says

294
00:24:59,700 --> 00:25:01,420
 here exclamation of surprise

295
00:25:01,420 --> 00:25:03,820
 no that's not what it means

296
00:25:04,780 --> 00:25:06,780
 so

297
00:25:06,780 --> 00:25:12,990
 maybe I have the wrong form up here yeah scroll down to the

298
00:25:12,990 --> 00:25:14,380
 ten plus aha

299
00:25:14,380 --> 00:25:21,900
 okay thank you yeah so that's why that little box is there

300
00:25:21,900 --> 00:25:25,820
 because it's gonna get can't

301
00:25:25,820 --> 00:25:30,830
 doesn't know can't interpret it like we can this is ten

302
00:25:30,830 --> 00:25:33,900
 Dana the is gone plus aha which is a

303
00:25:33,900 --> 00:25:43,180
 irregular form of regular form uh what's the actual verb

304
00:25:43,180 --> 00:25:51,140
 yeah I guess it's it's sort of its own verb anyway you have

305
00:25:51,140 --> 00:25:53,660
 to learn about these irregular forms

306
00:25:55,260 --> 00:25:55,260
 you

307
00:25:55,260 --> 00:26:15,260
 okay the next one

308
00:26:15,260 --> 00:26:25,260
 one

309
00:26:30,620 --> 00:26:42,980
 bahu jana multitude of jana cloud and jana means a person

310
00:26:42,980 --> 00:26:46,860
 so jana means people okay

311
00:26:46,860 --> 00:26:53,500
 bahu is like multiple many here

312
00:26:58,620 --> 00:27:00,620
 many much anything like that

313
00:27:00,620 --> 00:27:09,340
 it has here

314
00:27:09,340 --> 00:27:18,460
 sunny party gathered

315
00:27:18,460 --> 00:27:24,700
 yeah inksu is third person plural past tense

316
00:27:24,700 --> 00:27:34,700
 yeah

317
00:27:34,700 --> 00:27:37,020
 in the sense that something like have gathered

318
00:27:37,020 --> 00:27:48,700
 so many people have gathered

319
00:27:48,700 --> 00:27:53,500
 here

320
00:27:53,500 --> 00:27:54,000
 you

321
00:27:56,000 --> 00:27:56,500
 you

322
00:27:58,500 --> 00:27:59,000
 you

323
00:28:01,000 --> 00:28:01,500
 you

324
00:28:03,500 --> 00:28:04,000
 you

325
00:28:06,000 --> 00:28:06,500
 you

326
00:28:08,500 --> 00:28:09,000
 you

327
00:28:11,000 --> 00:28:11,500
 you

328
00:28:13,500 --> 00:28:14,000
 you

329
00:28:24,760 --> 00:28:25,260
 you

330
00:28:25,260 --> 00:28:25,760
 you

331
00:28:26,760 --> 00:28:27,260
 you

332
00:28:27,260 --> 00:28:27,760
 you

333
00:28:27,760 --> 00:28:28,260
 you

334
00:28:29,260 --> 00:28:29,760
 you

335
00:28:29,760 --> 00:28:30,260
 you

336
00:28:30,260 --> 00:28:30,760
 you

337
00:28:31,760 --> 00:28:32,260
 you

338
00:28:38,260 --> 00:28:38,760
 you

339
00:28:38,760 --> 00:28:39,260
 you

340
00:28:41,260 --> 00:28:41,760
 you

341
00:28:41,760 --> 00:28:43,760
 you

342
00:28:47,760 --> 00:28:49,760
 you

343
00:28:49,760 --> 00:28:51,760
 you

344
00:28:51,760 --> 00:28:53,760
 you

345
00:28:53,760 --> 00:28:55,760
 you

346
00:28:55,760 --> 00:28:57,760
 you

347
00:28:59,760 --> 00:28:59,760
 you

348
00:28:59,760 --> 00:29:21,760
 this right next one I'm not clear on people

349
00:29:21,760 --> 00:29:23,760
 you

350
00:29:49,760 --> 00:30:01,760
 let me see if I've got a typo here or not

351
00:30:01,760 --> 00:30:12,320
 yeah that should be a long night

352
00:30:16,960 --> 00:30:20,960
 bye naka have a good day

353
00:30:22,960 --> 00:30:23,460
 you

354
00:30:23,460 --> 00:30:23,960
 you

355
00:30:23,960 --> 00:30:25,960
 you

356
00:30:27,960 --> 00:30:27,960
 you

357
00:30:27,960 --> 00:30:29,960
 you

358
00:30:29,960 --> 00:30:31,960
 you

359
00:30:31,960 --> 00:30:33,960
 you

360
00:30:33,960 --> 00:30:35,960
 you

361
00:30:35,960 --> 00:30:37,960
 you

362
00:30:37,960 --> 00:30:39,960
 you

363
00:30:39,960 --> 00:30:41,960
 you

364
00:30:41,960 --> 00:30:43,960
 you

365
00:30:45,960 --> 00:30:45,960
 you

366
00:30:45,960 --> 00:30:45,960
 you

367
00:30:45,960 --> 00:30:46,460
 you

368
00:30:46,460 --> 00:30:46,460
 you

369
00:30:46,460 --> 00:30:46,960
 you

370
00:30:46,960 --> 00:30:47,460
 you

371
00:30:47,460 --> 00:30:47,960
 you

372
00:30:47,960 --> 00:30:48,460
 you

373
00:30:48,460 --> 00:30:48,960
 you

374
00:30:48,960 --> 00:30:49,460
 you

375
00:30:49,460 --> 00:30:49,960
 you

376
00:30:49,960 --> 00:30:49,960
 you

377
00:30:49,960 --> 00:30:50,460
 you

378
00:30:50,460 --> 00:30:50,960
 you

379
00:30:50,960 --> 00:30:50,960
 you

380
00:30:50,960 --> 00:30:51,460
 you

381
00:30:51,460 --> 00:30:51,960
 you

382
00:30:51,960 --> 00:30:51,960
 you

383
00:30:51,960 --> 00:30:52,460
 you

384
00:30:52,460 --> 00:30:52,960
 you

385
00:30:52,960 --> 00:30:53,460
 you

386
00:30:53,460 --> 00:30:53,960
 you

387
00:30:53,960 --> 00:30:54,460
 you

388
00:30:54,460 --> 00:30:54,960
 you

389
00:30:54,960 --> 00:30:55,460
 you

390
00:30:55,460 --> 00:30:55,960
 you

391
00:30:55,960 --> 00:30:56,460
 you

392
00:30:56,460 --> 00:30:56,960
 you

393
00:30:56,960 --> 00:30:57,460
 you

394
00:30:57,460 --> 00:30:57,960
 you

395
00:30:57,960 --> 00:30:58,460
 you

396
00:30:58,460 --> 00:30:58,960
 you

397
00:30:58,960 --> 00:30:59,460
 you

398
00:30:59,460 --> 00:30:59,960
 you

399
00:30:59,960 --> 00:31:00,460
 you

400
00:31:00,460 --> 00:31:00,960
 you

401
00:31:00,960 --> 00:31:01,460
 you

402
00:31:01,460 --> 00:31:01,960
 you

403
00:31:01,960 --> 00:31:02,460
 you

404
00:31:02,460 --> 00:31:02,960
 you

405
00:31:02,960 --> 00:31:03,460
 you

406
00:31:03,460 --> 00:31:03,960
 you

407
00:31:03,960 --> 00:31:04,460
 you

408
00:31:04,460 --> 00:31:04,960
 you

409
00:31:04,960 --> 00:31:05,460
 you

410
00:31:05,460 --> 00:31:05,960
 you

411
00:31:05,960 --> 00:31:06,460
 you

412
00:31:06,460 --> 00:31:06,960
 you

413
00:31:06,960 --> 00:31:07,460
 you

414
00:31:07,460 --> 00:31:07,960
 you

415
00:31:07,960 --> 00:31:08,460
 you

416
00:31:08,460 --> 00:31:08,960
 you

417
00:31:08,960 --> 00:31:09,460
 you

418
00:31:09,460 --> 00:31:09,960
 you

419
00:31:09,960 --> 00:31:10,460
 you

420
00:31:10,460 --> 00:31:10,960
 you

421
00:31:10,960 --> 00:31:11,460
 you

422
00:31:11,460 --> 00:31:11,960
 you

423
00:31:11,960 --> 00:31:12,460
 you

424
00:31:12,460 --> 00:31:12,460
 you

425
00:31:12,460 --> 00:31:12,960
 you

426
00:31:12,960 --> 00:31:13,460
 you

427
00:31:13,460 --> 00:31:13,960
 you

428
00:31:13,960 --> 00:31:14,460
 you

429
00:31:16,460 --> 00:31:16,960
 you

430
00:31:18,960 --> 00:31:19,460
 you

431
00:31:21,460 --> 00:31:21,960
 you

432
00:31:23,960 --> 00:31:24,460
 you

433
00:31:26,460 --> 00:31:26,960
 you

434
00:31:28,960 --> 00:31:29,460
 you

435
00:31:29,460 --> 00:31:29,960
 you

436
00:31:29,960 --> 00:31:30,460
 you

437
00:31:30,460 --> 00:31:59,460
 yeah

438
00:31:59,460 --> 00:32:01,960
 why king is in this case why

439
00:32:01,960 --> 00:32:02,960
 why king is in this case why

440
00:32:02,960 --> 00:32:03,960
 why king is in this case why

441
00:32:03,960 --> 00:32:04,960
 why king is in this case why

442
00:32:04,960 --> 00:32:05,460
 do you

443
00:32:05,460 --> 00:32:05,960
 do you

444
00:32:05,960 --> 00:32:06,460
 do you

445
00:32:06,460 --> 00:32:06,960
 do you

446
00:32:06,960 --> 00:32:07,460
 yeah

447
00:32:07,460 --> 00:32:07,960
 do you

448
00:32:07,960 --> 00:32:08,460
 yeah

449
00:32:08,460 --> 00:32:08,960
 why do you stand there

450
00:32:08,960 --> 00:32:09,460
 as though

451
00:32:09,460 --> 00:32:09,460
 as though

452
00:32:09,460 --> 00:32:09,960
 as though

453
00:32:09,960 --> 00:32:10,460
 as though

454
00:32:10,460 --> 00:32:10,960
 as though

455
00:32:10,960 --> 00:32:11,460
 as though

456
00:32:11,460 --> 00:32:11,960
 as though

457
00:32:11,960 --> 00:32:11,960
 as though

458
00:32:11,960 --> 00:32:11,960
 as though

459
00:32:11,960 --> 00:32:12,460
 as though

460
00:32:12,460 --> 00:32:12,960
 as though

461
00:32:12,960 --> 00:32:13,460
 as though

462
00:32:13,460 --> 00:32:13,960
 as though

463
00:32:13,960 --> 00:32:13,960
 as though

464
00:32:13,960 --> 00:32:13,960
 as though

465
00:32:13,960 --> 00:32:14,460
 as though

466
00:32:14,460 --> 00:32:14,460
 as though

467
00:32:14,460 --> 00:32:14,960
 as though

468
00:32:14,960 --> 00:32:15,460
 as though

469
00:32:15,460 --> 00:32:15,960
 as though

470
00:32:15,960 --> 00:32:16,460
 as though

471
00:32:16,460 --> 00:32:16,960
 as though

472
00:32:16,960 --> 00:32:17,460
 as though

473
00:32:17,460 --> 00:32:17,960
 as though

474
00:32:17,960 --> 00:32:18,460
 as though

475
00:32:18,460 --> 00:32:18,960
 as though

476
00:32:18,960 --> 00:32:19,460
 as though

477
00:32:19,460 --> 00:32:19,960
 as though

478
00:32:19,960 --> 00:32:20,460
 as though

479
00:32:20,460 --> 00:32:20,960
 as though

480
00:32:20,960 --> 00:32:21,460
 as though

481
00:32:21,460 --> 00:32:21,960
 as though

482
00:32:21,960 --> 00:32:22,460
 as though

483
00:32:22,460 --> 00:32:22,960
 as though

484
00:32:22,960 --> 00:32:23,460
 as though

485
00:32:23,460 --> 00:32:23,960
 as though

486
00:32:23,960 --> 00:32:24,460
 as though

487
00:32:24,460 --> 00:32:24,960
 as though

488
00:32:26,960 --> 00:32:27,460
 as though

489
00:32:27,460 --> 00:32:27,960
 as though

490
00:32:27,960 --> 00:32:28,460
 as though

491
00:32:30,460 --> 00:32:30,960
 as though

492
00:32:30,960 --> 00:32:30,960
 as though

493
00:32:30,960 --> 00:32:31,460
 as though

494
00:32:31,460 --> 00:32:31,960
 as though

495
00:32:33,960 --> 00:32:34,460
 as though

496
00:32:36,460 --> 00:32:36,960
 as though

497
00:32:36,960 --> 00:32:37,460
 as though

498
00:32:37,460 --> 00:32:37,960
 as though

499
00:32:39,960 --> 00:32:40,460
 as though

500
00:32:42,460 --> 00:32:42,960
 as though

501
00:32:42,960 --> 00:32:43,460
 as though

502
00:32:43,460 --> 00:32:43,960
 as though

503
00:32:43,960 --> 00:32:44,460
 as though

504
00:32:44,460 --> 00:32:44,460
 as though

505
00:32:44,460 --> 00:32:44,960
 as though

506
00:32:44,960 --> 00:32:45,460
 as though

507
00:32:45,460 --> 00:32:45,960
 as though

508
00:32:45,960 --> 00:32:46,460
 as though

509
00:32:46,460 --> 00:32:46,960
 as though

510
00:32:46,960 --> 00:32:47,460
 as though

511
00:32:47,460 --> 00:32:47,960
 as though

512
00:32:47,960 --> 00:32:48,460
 as though

513
00:32:48,460 --> 00:32:48,960
 as though

514
00:32:48,960 --> 00:32:49,460
 as though

515
00:32:49,460 --> 00:32:49,960
 as though

516
00:32:49,960 --> 00:32:50,460
 as though

517
00:32:50,460 --> 00:32:50,460
 as though

518
00:32:50,460 --> 00:32:50,960
 as though

519
00:32:50,960 --> 00:32:51,460
 as though

520
00:32:51,460 --> 00:32:51,960
 as though

521
00:32:51,960 --> 00:32:52,460
 as though

522
00:32:52,460 --> 00:32:52,460
 as though

523
00:32:52,460 --> 00:32:52,960
 as though

524
00:32:52,960 --> 00:32:52,960
 as though

525
00:32:52,960 --> 00:32:53,460
 as though

526
00:32:53,460 --> 00:32:53,960
 as though

527
00:32:53,960 --> 00:32:54,460
 as though

528
00:32:54,460 --> 00:32:54,960
 as though

529
00:32:54,960 --> 00:32:55,460
 as though

530
00:32:55,460 --> 00:32:55,960
 as though

531
00:32:55,960 --> 00:32:56,460
 as though

532
00:32:56,460 --> 00:32:56,960
 as though

533
00:32:56,960 --> 00:32:57,460
 as though

534
00:32:57,460 --> 00:32:57,960
 as though

535
00:32:57,960 --> 00:32:58,460
 as though

536
00:32:58,460 --> 00:32:58,960
 as though

537
00:32:58,960 --> 00:32:59,460
 as though

538
00:32:59,460 --> 00:32:59,960
 as though

539
00:32:59,960 --> 00:33:00,460
 as though

540
00:33:00,460 --> 00:33:00,960
 as though

541
00:33:00,960 --> 00:33:01,460
 as though

542
00:33:01,460 --> 00:33:01,960
 as though

543
00:33:01,960 --> 00:33:02,460
 as though

544
00:33:02,460 --> 00:33:02,960
 as though

545
00:33:02,960 --> 00:33:03,460
 as though

546
00:33:03,460 --> 00:33:03,960
 as though

547
00:33:03,960 --> 00:33:04,460
 as though

548
00:33:04,460 --> 00:33:04,960
 as though

549
00:33:04,960 --> 00:33:05,460
 as though

550
00:33:05,460 --> 00:33:05,960
 as though

551
00:33:05,960 --> 00:33:06,460
 as though

552
00:33:06,460 --> 00:33:06,960
 as though

553
00:33:06,960 --> 00:33:07,460
 as though

554
00:33:07,460 --> 00:33:07,960
 as though

555
00:33:07,960 --> 00:33:08,460
 as though

556
00:33:08,460 --> 00:33:08,960
 as though

557
00:33:08,960 --> 00:33:09,460
 as though

558
00:33:09,460 --> 00:33:09,960
 as though

559
00:33:09,960 --> 00:33:10,460
 as though

560
00:33:10,460 --> 00:33:10,460
 as though

561
00:33:10,460 --> 00:33:10,960
 as though

562
00:33:10,960 --> 00:33:11,460
 as though

563
00:33:11,460 --> 00:33:11,960
 as though

564
00:33:11,960 --> 00:33:12,460
 as though

565
00:33:12,460 --> 00:33:12,960
 as though

566
00:33:12,960 --> 00:33:13,460
 as though

567
00:33:13,460 --> 00:33:13,960
 as though

568
00:33:13,960 --> 00:33:14,460
 as though

569
00:33:14,460 --> 00:33:14,960
 as though

570
00:33:14,960 --> 00:33:15,460
 as though

571
00:33:15,460 --> 00:33:15,960
 as though

572
00:33:15,960 --> 00:33:16,460
 as though

573
00:33:16,460 --> 00:33:16,960
 as though

574
00:33:16,960 --> 00:33:17,460
 as though

575
00:33:17,460 --> 00:33:17,960
 as though

576
00:33:17,960 --> 00:33:17,960
 as though

577
00:33:17,960 --> 00:33:18,460
 as though

578
00:33:18,460 --> 00:33:18,960
 as though

579
00:33:18,960 --> 00:33:19,460
 as though

580
00:33:19,460 --> 00:33:19,960
 as though

581
00:33:19,960 --> 00:33:20,460
 as though

582
00:33:20,460 --> 00:33:20,960
 as though

583
00:33:20,960 --> 00:33:21,460
 as though

584
00:33:21,460 --> 00:33:21,960
 as though

585
00:33:21,960 --> 00:33:22,460
 as though

586
00:33:22,460 --> 00:33:22,960
 as though

587
00:33:22,960 --> 00:33:23,460
 as though

588
00:33:23,460 --> 00:33:23,960
 as though

589
00:33:23,960 --> 00:33:24,460
 as though

590
00:33:24,460 --> 00:33:24,960
 as though

591
00:33:24,960 --> 00:33:25,460
 as though

592
00:33:25,460 --> 00:33:25,960
 as though

593
00:33:25,960 --> 00:33:26,460
 as though

594
00:33:26,460 --> 00:33:26,960
 as though

595
00:33:26,960 --> 00:33:27,460
 as though

596
00:33:27,460 --> 00:33:27,960
 as though

597
00:33:27,960 --> 00:33:28,460
 as though

598
00:33:28,460 --> 00:33:28,460
 as though

599
00:33:28,460 --> 00:33:28,960
 as though

600
00:33:28,960 --> 00:33:29,460
 as though

601
00:33:29,460 --> 00:33:29,960
 as though

602
00:33:29,960 --> 00:33:30,460
 as though

603
00:33:30,460 --> 00:33:30,460
 as though

604
00:33:30,460 --> 00:33:30,960
 as though

605
00:33:30,960 --> 00:33:31,460
 as though

606
00:33:31,460 --> 00:33:31,960
 as though

607
00:33:31,960 --> 00:33:32,460
 as though

608
00:33:32,460 --> 00:33:32,960
 as though

609
00:33:32,960 --> 00:33:33,460
 as though

610
00:33:33,460 --> 00:33:33,960
 as though

611
00:33:33,960 --> 00:33:34,460
 as though

612
00:33:34,460 --> 00:33:34,960
 as though

613
00:33:34,960 --> 00:33:35,460
 as though

614
00:33:35,460 --> 00:33:35,960
 as though

615
00:33:35,960 --> 00:33:36,460
 as though

616
00:33:36,460 --> 00:33:36,960
 as though

617
00:33:36,960 --> 00:33:37,460
 as though

618
00:33:37,460 --> 00:33:37,960
 as though

619
00:33:37,960 --> 00:33:38,460
 as though

620
00:33:38,460 --> 00:33:38,960
 as though

621
00:33:38,960 --> 00:33:39,460
 as though

622
00:33:39,460 --> 00:33:39,960
 as though

623
00:33:39,960 --> 00:33:40,460
 as though

624
00:33:40,460 --> 00:33:40,960
 as though

625
00:33:40,960 --> 00:33:41,460
 as though

626
00:33:41,460 --> 00:33:41,960
 as though

627
00:33:41,960 --> 00:33:42,460
 as though

628
00:33:42,460 --> 00:33:42,960
 as though

629
00:33:42,960 --> 00:33:43,460
 as though

630
00:33:43,460 --> 00:33:43,960
 as though

631
00:33:43,960 --> 00:33:44,460
 as though

632
00:33:44,460 --> 00:33:44,960
 as though

633
00:33:44,960 --> 00:33:45,460
 as though

634
00:33:45,460 --> 00:33:45,960
 as though

635
00:33:45,960 --> 00:33:46,460
 as though

636
00:33:46,460 --> 00:33:46,960
 as though

637
00:33:46,960 --> 00:33:47,460
 as though

638
00:33:47,460 --> 00:33:47,960
 as though

639
00:33:47,960 --> 00:33:48,460
 as though

640
00:33:49,460 --> 00:33:49,960
 as though

641
00:33:49,960 --> 00:33:50,460
 as though

642
00:33:50,460 --> 00:33:50,960
 as though

643
00:33:50,960 --> 00:33:51,460
 as though

644
00:33:51,460 --> 00:33:51,960
 as though

645
00:33:51,960 --> 00:33:52,460
 as though

646
00:33:52,460 --> 00:33:52,960
 as though

647
00:33:52,960 --> 00:33:53,460
 as though

648
00:33:53,460 --> 00:33:53,960
 as though

649
00:33:53,960 --> 00:33:54,460
 as though

650
00:33:54,460 --> 00:33:54,960
 as though

651
00:33:54,960 --> 00:33:55,460
 as though

652
00:33:55,460 --> 00:33:55,960
 as though

653
00:33:55,960 --> 00:33:56,460
 as though

654
00:33:56,460 --> 00:33:56,960
 as though

655
00:33:56,960 --> 00:33:57,460
 as though

656
00:33:57,460 --> 00:33:57,960
 as though

657
00:33:57,960 --> 00:33:58,460
 as though

658
00:33:58,460 --> 00:33:58,960
 as though

659
00:33:58,960 --> 00:33:59,460
 as though

660
00:33:59,460 --> 00:33:59,960
 as though

661
00:33:59,960 --> 00:34:00,460
 as though

662
00:34:00,460 --> 00:34:00,960
 as though

663
00:34:00,960 --> 00:34:01,460
 as though

664
00:34:01,460 --> 00:34:01,960
 as though

665
00:34:01,960 --> 00:34:02,460
 as though

666
00:34:02,460 --> 00:34:02,960
 as though

667
00:34:02,960 --> 00:34:03,460
 as though

668
00:34:03,460 --> 00:34:03,960
 as though

669
00:34:03,960 --> 00:34:04,460
 as though

670
00:34:04,460 --> 00:34:04,960
 as though

671
00:34:04,960 --> 00:34:05,460
 as though

672
00:34:05,460 --> 00:34:05,960
 as though

673
00:34:05,960 --> 00:34:06,460
 as though

674
00:34:06,460 --> 00:34:06,960
 as though

675
00:34:06,960 --> 00:34:07,460
 as though

676
00:34:07,460 --> 00:34:07,960
 as though

677
00:34:07,960 --> 00:34:08,460
 as though

678
00:34:08,460 --> 00:34:08,960
 as though

679
00:34:08,960 --> 00:34:09,460
 as though

680
00:34:09,460 --> 00:34:09,960
 as though

681
00:34:09,960 --> 00:34:10,460
 as though

682
00:34:10,460 --> 00:34:10,960
 as though

683
00:34:10,960 --> 00:34:10,960
 as though

684
00:34:10,960 --> 00:34:11,460
 as though

685
00:34:11,460 --> 00:34:11,960
 as though

686
00:34:11,960 --> 00:34:12,460
 as though

687
00:34:12,460 --> 00:34:12,960
 as though

