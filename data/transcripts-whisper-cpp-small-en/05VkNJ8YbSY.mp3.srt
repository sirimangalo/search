1
00:00:00,000 --> 00:00:04,800
 I heard in Tibetan Buddhism the soul will remain in Bardo

2
00:00:04,800 --> 00:00:06,000
 after death.

3
00:00:06,000 --> 00:00:11,990
 In what form or manner is this entity after it detaches

4
00:00:11,990 --> 00:00:18,000
 from the body at death perceives the physical reality?

5
00:00:18,000 --> 00:00:24,760
 Yeah, I don't get that grammar is not so perfect. I mean,

6
00:00:24,760 --> 00:00:29,860
 we couldn't criticize that, but it's hard to understand

7
00:00:29,860 --> 00:00:31,000
 exactly what's being asked there.

8
00:00:31,000 --> 00:00:38,610
 Perceived? At what manner is it perceived, or what manner

9
00:00:38,610 --> 00:00:42,000
 does it perceive the physical reality?

10
00:00:42,000 --> 00:00:48,250
 Well, first of all, I'm not a Tibetan Buddhist, so I'm not

11
00:00:48,250 --> 00:00:52,690
 going to comment exactly, and it may not be the exact

12
00:00:52,690 --> 00:00:54,000
 answer to the question.

13
00:00:54,000 --> 00:00:58,930
 But I don't actually think that Tibetan Buddhists believe

14
00:00:58,930 --> 00:01:03,350
 in a soul. I'm not positive on that, but I would highly

15
00:01:03,350 --> 00:01:08,660
 doubt it that the Tibetan Buddhists do believe such a thing

16
00:01:08,660 --> 00:01:09,000
.

17
00:01:09,000 --> 00:01:15,890
 What they believe is the being continues, or the existence

18
00:01:15,890 --> 00:01:21,000
 continues, in what they call the Bardo.

19
00:01:21,000 --> 00:01:23,760
 Now, maybe they do have the idea that there is a soul there

20
00:01:23,760 --> 00:01:26,990
. I'm sure some people, of course, many Buddhists do have

21
00:01:26,990 --> 00:01:30,000
 attachment to the soul until you become a sotapan.

22
00:01:30,000 --> 00:01:35,830
 You still have the potential to cling to sakryaditi, cling

23
00:01:35,830 --> 00:01:39,990
 to self. But the truth of what's going on there is there is

24
00:01:39,990 --> 00:01:41,000
 experience.

25
00:01:41,000 --> 00:01:43,540
 There is the arising and ceasing of seeing, hearing,

26
00:01:43,540 --> 00:01:46,000
 smelling, taste, and feeling, and thinking.

27
00:01:46,000 --> 00:01:50,250
 In Theravada Buddhism, we might call that a ghost. And I

28
00:01:50,250 --> 00:01:53,560
 know in Tibetan Buddhism they say 49 days is the time that

29
00:01:53,560 --> 00:01:55,000
 they stay in the Bardo.

30
00:01:55,000 --> 00:02:00,120
 I think in Theravada Buddhism we're not so clear on the 49

31
00:02:00,120 --> 00:02:03,000
 days, but it may be the truth.

32
00:02:03,000 --> 00:02:07,040
 People have actually verified this, that on the 50th day

33
00:02:07,040 --> 00:02:11,000
 the ghost comes and says goodbye or something like that.

34
00:02:11,000 --> 00:02:14,260
 We would just say it's a ghost. The point is that it's not

35
00:02:14,260 --> 00:02:20,000
 in between. It's not an antara bhava in an ultimate sense.

36
00:02:20,000 --> 00:02:24,210
 It's not between existences. It is an existence in and of

37
00:02:24,210 --> 00:02:25,000
 itself.

38
00:02:25,000 --> 00:02:31,860
 If a person who dies, if the experience continues on

39
00:02:31,860 --> 00:02:39,530
 locally, or even remotely or in whatever location, without

40
00:02:39,530 --> 00:02:44,000
 the arising of a coarse material body,

41
00:02:44,000 --> 00:02:48,330
 we consider that to be a bhava in and of itself. It's an

42
00:02:48,330 --> 00:02:53,000
 existence. The existence of the being has continued.

43
00:02:53,000 --> 00:02:58,940
 And how long it lasts, it might last a short time, it might

44
00:02:58,940 --> 00:03:01,000
 last a long time.

45
00:03:01,000 --> 00:03:03,860
 Usually I would say depending on the karma. If the person

46
00:03:03,860 --> 00:03:07,650
 has done the karma, to lead them to be a ghost for a long

47
00:03:07,650 --> 00:03:08,000
 time,

48
00:03:08,000 --> 00:03:16,000
 you might stay in that state for longer than 49 days.

49
00:03:16,000 --> 00:03:20,950
 The Tibetans have interesting ideas about what that being

50
00:03:20,950 --> 00:03:22,000
 perceives.

51
00:03:22,000 --> 00:03:26,200
 When they are getting ready to go on to the next life, they

52
00:03:26,200 --> 00:03:28,000
 will perceive all sorts of crazy stuff.

53
00:03:28,000 --> 00:03:38,060
 Technically they will perceive one of three things. Up

54
00:03:38,060 --> 00:03:39,650
 until this point they would perceive reality kind of like

55
00:03:39,650 --> 00:03:40,000
 an angel.

56
00:03:40,000 --> 00:03:44,410
 They would perceive people maybe flying around, floating

57
00:03:44,410 --> 00:03:46,790
 around. They would see the people crying over their dead

58
00:03:46,790 --> 00:03:47,000
 body.

59
00:03:47,000 --> 00:03:50,150
 They would be unable to communicate with these people. Or

60
00:03:50,150 --> 00:03:54,390
 they might go here or go there based on their attachment to

61
00:03:54,390 --> 00:03:56,000
 their past life.

62
00:03:56,000 --> 00:03:59,220
 But then at the moment when they are going to move on, one

63
00:03:59,220 --> 00:04:01,000
 of three things will come to them.

64
00:04:01,000 --> 00:04:04,280
 And this of course can also come at the moment of death in

65
00:04:04,280 --> 00:04:05,000
 the body.

66
00:04:05,000 --> 00:04:08,350
 So without actually leaving the body, a person can go

67
00:04:08,350 --> 00:04:13,000
 directly, apparently can go directly to heaven or hell or

68
00:04:13,000 --> 00:04:13,000
 wherever,

69
00:04:13,000 --> 00:04:17,300
 based on the arising of three things. So whether it's still

70
00:04:17,300 --> 00:04:20,000
 in the body or whether it's after having left the body,

71
00:04:20,000 --> 00:04:24,140
 there will be one of three experiences. One, an experience

72
00:04:24,140 --> 00:04:27,000
 or a remembrance of the karma that one has done.

73
00:04:27,000 --> 00:04:32,060
 I did a talk a while back. I should probably do a video. I

74
00:04:32,060 --> 00:04:35,720
 don't know that I've ever done a video on the 12 types of

75
00:04:35,720 --> 00:04:36,000
 karma.

76
00:04:36,000 --> 00:04:51,120
 But there are four types that are pertinent here. There is

77
00:04:51,120 --> 00:04:52,000
 karma that is niyat, the garukam, the kamma that is sure.

78
00:04:52,000 --> 00:04:55,020
 After you've done this karma, that's what's going to lead

79
00:04:55,020 --> 00:04:56,000
 to the rebirth.

80
00:04:56,000 --> 00:05:00,070
 So if you've killed your father, killed your mother, killed

81
00:05:00,070 --> 00:05:04,650
 an arahant, hurt the Buddha, or caused a schism in the San

82
00:05:04,650 --> 00:05:05,000
gha.

83
00:05:05,000 --> 00:05:09,740
 If you've done any of these things, then that's what's

84
00:05:09,740 --> 00:05:13,000
 going to come to you. You're going to remember the karma.

85
00:05:13,000 --> 00:05:21,000
 Or you're going to remember a symbol of the karma.

86
00:05:21,000 --> 00:05:26,240
 So there's kamalimita, then there's kamalimita. So a symbol

87
00:05:26,240 --> 00:05:27,000
 of the kamalimita.

88
00:05:27,000 --> 00:05:29,760
 You might not remember the actual act. Think of the actual

89
00:05:29,760 --> 00:05:32,650
 act. But instead you'll think of something that symbolizes

90
00:05:32,650 --> 00:05:33,000
 it.

91
00:05:33,000 --> 00:05:37,440
 So if you killed someone, you might see a knife, or you

92
00:05:37,440 --> 00:05:40,000
 might see blood, or so on.

93
00:05:40,000 --> 00:05:45,010
 If you've done this, you'll see something that reminds you

94
00:05:45,010 --> 00:05:48,000
 of it, or that is symbolic of it.

95
00:05:48,000 --> 00:05:53,860
 Or third, you might see gatimimita. You might see an image

96
00:05:53,860 --> 00:05:55,000
 of the way that you're going to go.

97
00:05:55,000 --> 00:05:58,490
 So some people actually feel like they're flying up to

98
00:05:58,490 --> 00:06:03,000
 heaven, or they're taking a trip. Kids who were reborn,

99
00:06:03,000 --> 00:06:05,790
 kids who remember their past lives, will say they remember

100
00:06:05,790 --> 00:06:08,000
 traveling to the house that they were born in.

101
00:06:08,000 --> 00:06:11,520
 And they're standing outside of this house, and suddenly

102
00:06:11,520 --> 00:06:15,000
 they're born in the womb of the woman inside the house.

103
00:06:15,000 --> 00:06:18,990
 This is called gatimimita. So the person sees the

104
00:06:18,990 --> 00:06:24,000
 destination, a vision or a dream of the destination.

105
00:06:24,000 --> 00:06:28,310
 So if it's garukama, if you've done any really, really bad

106
00:06:28,310 --> 00:06:31,000
 deeds, it'll be based on this.

107
00:06:31,000 --> 00:06:37,130
 If you did something particularly potent at the moment of

108
00:06:37,130 --> 00:06:40,000
 death, then you're more likely to remember.

109
00:06:40,000 --> 00:06:42,950
 But you haven't done a garukama, then you're likely to

110
00:06:42,950 --> 00:06:44,000
 remember that.

111
00:06:44,000 --> 00:06:46,510
 If there's nothing you've done particularly at the moment

112
00:06:46,510 --> 00:06:50,000
 of death, and you haven't performed garukama,

113
00:06:50,000 --> 00:06:54,000
 then you'll remember some karma that you did repeatedly.

114
00:06:54,000 --> 00:06:59,000
 That was a habitual, a china kamma.

115
00:06:59,000 --> 00:07:03,390
 Something that, so if you were a thief and you stole,

116
00:07:03,390 --> 00:07:05,000
 continue, you stole regularly,

117
00:07:05,000 --> 00:07:09,160
 or if you were a murderer, a hunter, or a butcher, so on,

118
00:07:09,160 --> 00:07:11,940
 then this is what would come back to you as one of these

119
00:07:11,940 --> 00:07:13,000
 three things.

120
00:07:13,000 --> 00:07:19,130
 One of these three things would be based on that karma. And

121
00:07:19,130 --> 00:07:21,430
 if not, then it would be just some sort of karma that you

122
00:07:21,430 --> 00:07:22,000
 performed,

123
00:07:22,000 --> 00:07:27,000
 meaning some experience that had an effect on your mind

124
00:07:27,000 --> 00:07:35,000
 that caused you to attach to it like it or dislike it,

125
00:07:35,000 --> 00:07:39,630
 and so was some sort of karma in the mental sense. So that

126
00:07:39,630 --> 00:07:42,000
's the last perception.

127
00:07:42,000 --> 00:07:47,000
 And at that moment, and Tibetan Buddhism, as I understand,

128
00:07:47,000 --> 00:07:49,000
 they talk about this as well,

129
00:07:49,000 --> 00:07:53,600
 how there will be an image or a vision come up, and you'll

130
00:07:53,600 --> 00:07:55,000
 be attracted to that.

131
00:07:55,000 --> 00:07:58,720
 And if you're not mindful at that moment, you'll get sucked

132
00:07:58,720 --> 00:08:01,000
 into it and be reborn based on it.

133
00:08:01,000 --> 00:08:07,860
 So that's the process. Now as far as the existence up to

134
00:08:07,860 --> 00:08:09,000
 that time, well, as I said,

135
00:08:09,000 --> 00:08:12,800
 it's either the moment of death passing on or else at the

136
00:08:12,800 --> 00:08:17,270
 moment of death there's an out-of-body experience, like the

137
00:08:17,270 --> 00:08:18,000
 bardo,

138
00:08:18,000 --> 00:08:23,000
 and then eventual continuing on to the next life.

