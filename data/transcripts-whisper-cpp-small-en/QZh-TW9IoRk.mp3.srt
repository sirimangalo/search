1
00:00:00,000 --> 00:00:06,400
 need each other, you can't just work on morality and...

2
00:00:06,400 --> 00:00:07,400
 In isolation?

3
00:00:07,400 --> 00:00:08,800
 The other ones fall apart.

4
00:00:08,800 --> 00:00:12,080
 Well, that's really important, I think, and it points to

5
00:00:12,080 --> 00:00:13,800
 what true morality is.

6
00:00:13,800 --> 00:00:19,880
 Someone had a comment on my video for children, because it

7
00:00:19,880 --> 00:00:21,700
 was just teaching meditation with

8
00:00:21,700 --> 00:00:25,900
 no context of morality or even wisdom, as he said.

9
00:00:25,900 --> 00:00:30,560
 But I think that's really missing the point of morality and

10
00:00:30,560 --> 00:00:33,900
 wisdom and how they come together.

11
00:00:33,900 --> 00:00:37,510
 True morality isn't intellectual, it doesn't come from

12
00:00:37,510 --> 00:00:39,600
 taking on precepts or...

13
00:00:39,600 --> 00:00:43,600
 Really, it doesn't come from working.

14
00:00:43,600 --> 00:00:45,960
 It comes from enlightenment.

15
00:00:45,960 --> 00:00:49,080
 Morality comes through meditation.

16
00:00:49,080 --> 00:00:51,840
 The beginning of meditation is morality.

17
00:00:51,840 --> 00:00:53,200
 That's true morality.

18
00:00:53,200 --> 00:00:57,560
 Morality isn't taking precepts, it isn't even refusing to

19
00:00:57,560 --> 00:00:59,760
 do something when you have the

20
00:00:59,760 --> 00:01:01,120
 opportunity to do it.

21
00:01:01,120 --> 00:01:06,080
 It's not, at least not in the sense of saying, "I'm not

22
00:01:06,080 --> 00:01:07,320
 going to kill when the opportunity

23
00:01:07,320 --> 00:01:08,960
 comes to kill."

24
00:01:08,960 --> 00:01:10,040
 It's much deeper than that.

25
00:01:10,040 --> 00:01:16,120
 True morality is a mind state that refrains.

26
00:01:16,120 --> 00:01:22,600
 So this applies to all states that become distracted.

27
00:01:22,600 --> 00:01:26,520
 Morality is the development of concentration directly.

28
00:01:26,520 --> 00:01:29,440
 It's the focusing of the mind.

29
00:01:29,440 --> 00:01:33,460
 And that's why morality is said to lead to concentration.

30
00:01:33,460 --> 00:01:36,360
 Things that are immoral, they're all immoral.

31
00:01:36,360 --> 00:01:38,430
 As a result, there's so many immoral things out there that

32
00:01:38,430 --> 00:01:39,560
 we don't generally consider

33
00:01:39,560 --> 00:01:40,560
 immoral.

34
00:01:40,560 --> 00:01:43,680
 But they're immoral because they are diffusing the mind.

35
00:01:43,680 --> 00:01:46,560
 They're creating chaos in the mind.

36
00:01:46,560 --> 00:01:49,340
 Anything that makes the mind more chaotic is, for that

37
00:01:49,340 --> 00:01:50,500
 reason, immoral.

38
00:01:50,500 --> 00:01:53,590
 And that, therefore, is the definition of immorality in a

39
00:01:53,590 --> 00:01:54,520
 Buddhist sense.

40
00:01:54,520 --> 00:01:58,720
 It's totally utilitarian.

41
00:01:58,720 --> 00:02:01,420
 So something is immoral because of its effect on one's mind

42
00:02:01,420 --> 00:02:01,640
.

43
00:02:01,640 --> 00:02:06,590
 But it's also absolute because any number of things,

44
00:02:06,590 --> 00:02:09,640
 anything that we would normally

45
00:02:09,640 --> 00:02:12,520
 consider immoral, has that effect on the mind.

46
00:02:12,520 --> 00:02:14,400
 Killing has that effect on the mind.

47
00:02:14,400 --> 00:02:16,040
 Stealing has.

48
00:02:16,040 --> 00:02:20,160
 Useless speech has that effect on the mind.

49
00:02:20,160 --> 00:02:23,480
 It dulls the mind and it diffuses the mind.

50
00:02:23,480 --> 00:02:29,320
 It creates chaos and disorder in the mind.

51
00:02:29,320 --> 00:02:32,030
 So in that sense, working on morality and isolation is a

52
00:02:32,030 --> 00:02:33,920
 misunderstanding of what morality

53
00:02:33,920 --> 00:02:34,920
 is.

54
00:02:34,920 --> 00:02:36,560
 Morality is meditation.

55
00:02:36,560 --> 00:02:39,350
 When you sit down to practice meditation, that's when true

56
00:02:39,350 --> 00:02:40,400
 morality arises.

57
00:02:40,400 --> 00:02:42,000
 Yeah, you should keep precepts.

58
00:02:42,000 --> 00:02:43,600
 You should know what is right and what is wrong.

59
00:02:43,600 --> 00:02:47,040
 But that's only a part of the framework that is going to

60
00:02:47,040 --> 00:02:49,280
 allow you to approach meditation

61
00:02:49,280 --> 00:02:50,720
 in the right way.

62
00:02:50,720 --> 00:02:53,970
 Once you know what sorts of things are immoral, then you

63
00:02:53,970 --> 00:02:55,560
 know the sorts of things that are

64
00:02:55,560 --> 00:02:59,370
 going to create diffusion in the mind, that are going to

65
00:02:59,370 --> 00:03:01,080
 cause distraction and mental

66
00:03:01,080 --> 00:03:04,160
 upset.

67
00:03:04,160 --> 00:03:10,240
 That's all because the precepts are just a guide.

68
00:03:10,240 --> 00:03:12,480
 They aren't anything real.

69
00:03:12,480 --> 00:03:14,880
 They're just concepts.

70
00:03:14,880 --> 00:03:19,700
 I mean, given that there's no beings, killing isn't, there

71
00:03:19,700 --> 00:03:22,000
's nothing wrong with killing

72
00:03:22,000 --> 00:03:24,240
 because you can't kill someone.

73
00:03:24,240 --> 00:03:25,240
 There's no being.

74
00:03:25,240 --> 00:03:26,800
 There's no one to kill.

75
00:03:26,800 --> 00:03:29,760
 This is one way of, this is a view in the Buddhist time.

76
00:03:29,760 --> 00:03:31,400
 When someone kills, there is no killing.

77
00:03:31,400 --> 00:03:35,420
 It's just the knife goes through parts, atoms, which was

78
00:03:35,420 --> 00:03:37,600
 what this one teacher said.

79
00:03:37,600 --> 00:03:41,480
 The Buddha denied this because of the nature of the mind.

80
00:03:41,480 --> 00:03:42,600
 The mind is involved.

81
00:03:42,600 --> 00:03:44,720
 Yeah, the person.

82
00:03:44,720 --> 00:03:47,360
 What's wrong with killing isn't that the person dies.

83
00:03:47,360 --> 00:03:51,650
 What I'm killing is that you've created a horrible

84
00:03:51,650 --> 00:03:56,520
 intention in your mind and that totally disrupts

85
00:03:56,520 --> 00:03:58,760
 your own experience.

86
00:03:58,760 --> 00:04:02,150
 It's a very powerful thing to kill someone, but not because

87
00:04:02,150 --> 00:04:03,640
 of what it does to them and

88
00:04:03,640 --> 00:04:06,760
 because of what it does to you.

89
00:04:06,760 --> 00:04:08,280
 It goes beyond what we realize.

90
00:04:08,280 --> 00:04:12,280
 It's only when you meditate, if you've killed before, once

91
00:04:12,280 --> 00:04:14,080
 you meditate, you'll know what

92
00:04:14,080 --> 00:04:25,160
 I'm talking about because you really do feel it.

93
00:04:25,160 --> 00:04:31,070
 To get back to the original comment of how difficult it

94
00:04:31,070 --> 00:04:34,200
 might be to be moral without

95
00:04:34,200 --> 00:04:38,370
 meditation, be careful that you're not trying to be moral

96
00:04:38,370 --> 00:04:41,440
 too hard because true moral, good

97
00:04:41,440 --> 00:04:46,180
 to try, in some sense to keep the precepts, but much better

98
00:04:46,180 --> 00:04:48,160
 to use them as a guide and

99
00:04:48,160 --> 00:04:51,530
 really gain true realization that these things are wrong so

100
00:04:51,530 --> 00:04:53,280
 that they just won't arise.

101
00:04:53,280 --> 00:04:55,400
 You just won't have wrong speech.

102
00:04:55,400 --> 00:04:56,600
 You just won't lie.

103
00:04:56,600 --> 00:05:00,280
 You won't have harsh speech, say bad things about it.

104
00:05:00,280 --> 00:05:04,210
 You won't have divisive speech trying to break people apart

105
00:05:04,210 --> 00:05:05,080
 and so on.

106
00:05:05,080 --> 00:05:11,320
 You give up these kinds of behaviors naturally.

107
00:05:11,320 --> 00:05:14,280
 And that's where it's tied into concentration and wisdom

108
00:05:14,280 --> 00:05:16,240
 because wisdom actually leads back

109
00:05:16,240 --> 00:05:17,240
 to morality.

110
00:05:17,240 --> 00:05:18,240
 It's a circle.

111
00:05:18,240 --> 00:05:21,000
 Once you have wisdom, that's what brings true morality.

112
00:05:21,000 --> 00:05:22,000
 Okay?

