 Welcome everyone.
 We're up and broadcasting.
 Again for meditation Q&A session, which means we're here to
 answer questions about meditation.
 Specifically, the meditation in our tradition.
 So if you're new to this tradition, recommend that you read
 the booklet that we have on
 how to meditate.
 If you're looking to do a practice course in our tradition,
 you can do an at-home course
 right now.
 You sign up on our website.
 The link to this is all in the description to this video.
 It's all free.
 We don't charge for anything.
 Today is the full moon, the second full moon in October, I
 think.
 It's a blue moon.
 Not that that really means anything.
 But the full moon is traditionally a time of increased
 practice and study and teaching
 in Buddhism.
 So this morning I was invited to take part in an online
 meditation broadcast by the Tourism
 Authority of Thailand, which I guess is a big deal.
 They're a very big organization in Thailand.
 Part of the government, I think.
 Anyway they were very kind and very happy to have me.
 It was a great thing and people came from all over the
 world.
 Some of our people came.
 Some of their people came.
 I was able to give 10 minute instruction on meditation,
 just brief.
 They talked about how international it was, how people came
 from all over the world.
 That's really very true about all the things that we seem
 to do.
 We are very international.
 There's nothing deep or profound about that fact, but it
 does make you think about the
 impact that we and people like us have on the world, the
 impact the Buddha had on the
 world.
 There's a claim that Buddhism has results that you can see
 here and now.
 Wonder what the results of Buddhism have been for the world
.
 We talk about the impact that certain inventions had.
 What was the impact of the wheel or fire?
 What was the impact of electricity?
 What's the impact of mindfulness?
 The first is mindfulness, mindfulness in the world.
 We have a world where we got to believe that to some extent
 mindfulness is more valued
 than it was before the Buddha came.
 That there's a stream or there are threads of interest and
 understanding about seeing
 reality clearly as it is.
 That's the first benefit of mindfulness is mindfulness
 itself, that we have a better
 sense of proper and improper, better sense of the
 nature of our experiences.
 We have a better grasp on the nature of things, the quality
 of our experiences, the quality
 of our reactions, the quality of our interactions.
 So we make better decisions because of the Buddha's
 teaching.
 Second is happiness.
 We got to think that there's some, to some extent, some of
 the happiness in the world
 is caused by the influence of the Buddha's teaching.
 Of course there may be other reasons unrelated to Buddhism,
 but one of the great benefits
 of mindfulness is happiness.
 You live a life with greater happiness than you maybe even
 ever thought you were capable
 of before finding meditation.
 You may have felt a drift overwhelmed by mental illness, by
 bad habits of mind.
 And through the practice many people have found happiness.
 The third benefit is the benefit that comes not just in
 this life, but the benefit to
 our stream of consciousness from life to life, meaning some
 of the benefits that came even
 in the Buddha's time, we might still be feeling as beings
 who have been born again and forgotten
 ever meeting the Buddha or ever learning his teachings, but
 with a sense of ethics and
 morality, a sense of wholesome and goodness, wholesomeness
 and goodness, a sense of clarity
 and understanding.
 One of the benefits of Buddhism is the depth of its effect
 on the mind.
 And so even though you may not become enlightened in this
 life, the quality, qualities of mind
 that you have developed through meditation, even when it's
 challenging, even when, especially
 when it challenges you, they continue on life after life.
 That's a great benefit, something we can probably see even
 today, that we don't know who has
 practiced or who has been influenced by the Buddha,
 probably much of the interest in things
 like ethics and meditation, some of the philosophical
 traditions that came later might have been
 influenced by the Buddha.
 And the fourth is the ultimate peace that comes from the
 Buddha's teaching.
 You've got to imagine that some of the peace in the world,
 some of the peace that's still
 left, whatever's left of peace in this world, when you meet
 people who are peaceful, when
 you live in situations where there's no conflict and you
 find yourself alone in the forest,
 some of the peace in the world is because of people who
 would otherwise have been violent
 and encouraged and incited violence and caused the world to
 burn even more than it's burning
 now.
 Some of that peace that's left over, some of it must be
 because of the Buddha, because
 that's the final benefit of mindfulness, peace.
 Even just the experience, the attainment of nibbāna, bāri
-nibbāna when someone passes
 away and doesn't come back, they leave behind nothing but
 peace, like what the Buddha left
 behind was peace.
 Freedom from any danger, freedom from any stress, never to
 cause disharmony in the world
 again.
 So this is what we're doing in the world.
 We may never see, we may never be able to pinpoint how we
've changed the world and ultimately
 the changes are going to eventually make way for future
 things.
 But we see the benefits in ourselves and we continue in
 this way precisely because we
 see the positivity of it, we see the positive qualities in
 ourselves, in our experience
 and in the world around us.
 So as usual, we will enter into our question and answer
 phase portion of this session.
 So I'd ask from here on, there'll be no more chatting in
 the chat box.
 If you have a question, post a question.
 If it's not a question, I'll just close your eyes and med
itate on it.
 And I'm ready when you are, Chris.
 Okay, let's begin.
 Can doing mindful activities such as a mindful shower,
 mindful yoga, mindful exercise, etc.
 have as much benefits as meditation if one finds it hard to
 sit for more than 10 minutes?
 So the best way to deal with, the best thing to do when one
 finds something hard is to
 practice it.
 If you want something not to be hard anymore, you should
 practice it.
 And so to some extent, that's the whole point of meditation
 that we should work on those
 things that we find challenging.
 So if you do something that's easier, you'll get less
 benefit because you're less challenged
 and therefore there's less training involved.
 Those things that you call mindful are just names.
 If you're going to do the same thing that you do in formal
 meditation, which here that
 would be according to the booklet that we have, then you'll
 find that just as hard,
 if not harder to do in the shower while doing yoga, while
 exercising, etc.
 But it's quite common for people to call things mindful,
 but it's one thing to call it mindful.
 It's another to actually be doing the exact same sort of
 things that you're expected to
 do when sitting.
 That being said, it may be easier because, well, it's
 probably easier because of that,
 because you're feeling less confined by the practice than
 because there's a diversity
 that keeps you interested and so on.
 And that's the whole point of doing the walking is because
 there's not, walking and sitting
 is because there's not the diversity.
 And so it forces you to confront things like anxiety, rest
lessness, fatigue, liking, disliking,
 boredom, even.
 So the challenge is good for you, basically.
 Now, the thing is, it's always going to be hard in the
 beginning.
 Just don't be discouraged by that.
 And certainly you have to change that perspective of
 thinking, it's hard, therefore I should
 do something easier.
 If you start to challenge that and confront those things
 that are hard, you'll find much
 more benefit and you'll find they get easier, of course.
 What would be the best practice for one who couldn't
 concentrate properly because of debilitating
 brain damage?
 Well, we're not too concerned with concentration.
 There's no properly concentrate.
 Properly concentrating is to note something.
 If you can recognize something as it is, I mean, if you
 have enough mindfulness to type
 out that sentence, you're okay to practice.
 So just being able to recognize things, that's all that's
 required.
 The best practice, I would still say, is according to the
 booklet, according to this teaching.
 I mean, you're just asking me, this is the tradition I
 recommend.
 Is it possible to note the sensations of parts of the body,
 even if they are not prominent?
 For example, the arms, elbows, hands, mouth, eyes.
 Hands hides in the whole body, so I find it beneficial.
 The problem with this is that you're looking for something.
 In looking, you're cultivating the habit of looking.
 You're cultivating the habit of trying to fix as well.
 You see this concept of resistance hiding.
 That's just a concept, but you get into that idea of ferre
ting out and discovering resistance,
 which we're not really into.
 Why we focus on what's prominent is because that's what's
 there and that's choiceless.
 You're not making a decision, you're not trying to
 arbitrarily or intentionally find something.
 So we pick some banal exercise, like watching the stomach,
 watching the foot, but there's
 nothing special about those.
 It's just something easy, something simple, something
 repetitive.
 So finding it beneficial, probably you find it easier.
 It's more challenging to have to focus on the same thing
 over and over again because
 it's dull and banal.
 But that's the whole point, just to give your mind a chance
 to complain.
 I'm not really sure what you're looking for, but I wouldn't
 go looking.
 How do we verify if we are doing sattī or samma sattī?
 Samma sattī is the last moment of consciousness before nir
vana.
 There's nothing different from that and other kinds of satt
ī, it's just perfect at that
 point.
 All the eight factors come together and you call them samma
.
 Sati means to remember.
 So in the context of the preliminary path, before you reach
 the Eightfold Noble Path,
 it involves mindfulness of the present, remembering the
 present.
 It's not actually mindfulness, that's not the best
 translation, but it means to remember.
 So if remembering the past, remembering things you have to
 do in the future, that's sattī,
 but it's not according to the practice.
 Remembrance of the present, that's sattī.
 When you experience something and then you remember it for
 what it is, rather than getting
 lost in reaction, that's sattī.
 How do you know that you're doing that?
 To that extent, in that sense, it's not really something
 you have to ask, whether is this
 right or is this wrong?
 It's right if it's like that, if it's reminding you or
 remembering something just as it is,
 that's sattī.
 So how you can tell that that's right sattī, or how you
 can tell that it's a good thing
 to be doing, because it is right sattī, but how do you
 know, how do you, can you confirm
 that?
 You can confirm that because of the results, of course.
 When you do that, you'll find that you're less reactionary,
 you're better able to see
 your reactions, everything becomes clearer, you're much
 more present, and you start to
 learn about how your mind and your body work together.
 Just the clarity, really, because that's what mindfulness
 leads to, it leads to vipassana,
 sattī leads to vipassana.
 Vipassana means seeing clearly.
 Can the touch points be implemented in other postures, such
 as lying or standing?
 Yes.
 I have achieved success in an exam.
 However, I haven't been able to come to terms with it, and
 now I'm concerned about the upcoming
 exam.
 It feels like a vicious loop.
 Meditation isn't helping.
 Help.
 So, really, the question is, what do you expect meditation
 to, how do you expect meditation
 to help, in what way?
 Because if it's not helping, then either A, you're not
 doing it right, or B, it's not
 supposed to help with that, in the way that you think it is
.
 What meditation should do is, it should initially, it
 shouldn't do anything besides helping you
 to see your qualities of mind, to see experiences as they
 are.
 Now, seeing experiences as they are seems like a pointless
 task, because it doesn't
 change anything, right?
 When you're stressed, you just know that you're stressed,
 okay?
 So, I know that I'm stressed now.
 How does that help?
 But it actually is that which helps the most.
 It's the only thing that helps on a very real and basic
 level, because it's clarity.
 It's vision.
 It's seeing things as they are.
 When you see that things are stressful, when you see that
 things cause you experiences
 and reactions cause you stress, that's all it takes to
 change your mind, to change your
 habits.
 And the more clearly you see things, that's causing you
 stress and suffering, the weaker
 and the weaker is your attachment and your inclination
 towards those things, until your
 mind completely lets go of them.
 That's all it takes.
 So meditation only helps you to see things clearly and not
 react.
 See things clearly and cut off the chain of delusion and
 reaction.
 It won't help in any other way.
 It helps you be more patient and it requires you to be
 patient.
 You can't just do meditation and then think suddenly
 everything's going to be perfect.
 That's not how it works.
 You have to do meditation as a means of letting go of
 expectations for things to be perfect
 or this way or that way.
 Letting go of your ambitions and so on.
 So for things like exams, there's lots of worry, stress,
 fatigue, headaches, that sort
 of thing.
 It's not going to make any of that go away, but it's going
 to help you see it more clearly.
 Once you see it more clearly, you will see it become better
.
 It will go away.
 It will go away because you stop feeling it.
 But you need to have that ability to understand that there
's a disconnect.
 Meditation doesn't directly make things better.
 It helps you see what you're doing wrong.
 That's what meditation does.
 The doing things right comes through the knowledge of what
 is the wrong way, basically.
 So it's a two-step process.
 It's not meditate, poof, everything's good.
 It's meditate, oh my God, meditation shows me how bad
 everything is and then poof, everything's
 good.
 Why?
 Because you know what's bad.
 You need that patience to see the second step.
 Does even the rising and falling of the abdomen need to
 stop being perceived for cessation
 / nirvana to happen?
 When cessation happens, does the noting continue?
 Is one aware?
 So for cessation to occur right before the cessation, no,
 there has to be the perception
 of something because otherwise it would be cessation itself
.
 Cessation in terms of nirvana means cessation, so there
 would be no arising.
 There's no arising.
 So awareness isn't arising.
 Awareness is something that arises.
 Noting is something that arises.
 So none of those things would exist there.
 Thirty-five minutes into vipassana, my leg really started
 to hurt.
 Despite inquiring on the nature of the pain, I got
 extremely light-headed, anxious and nauseous,
 so I had to stop.
 Any reason for this?
 I don't know if you've read our booklet and what you mean
 by vipassana, but I'd recommend
 if you're interested, in my opinion, to read the booklet.
 It might give you some insight in a different way to look
 at it.
 But we're not into investigating, what did you say, the
 investigating the nature of the
 pain, inquiring on the nature of the pain.
 That's not what we're interested in.
 We're just trying to remind ourselves that it's just pain
 so that we can see it just
 as pain.
 It seems, anytime I say that, I know that it sounds like I
'm saying something useless
 or meaningless.
 It's like an absurd thing to do.
 Like stating the obvious, basically.
 "Yes, I know it's pain.
 How can I get rid of it?"
 That's the problem.
 The problem is you've forgotten.
 You've lost track of it as just pain.
 If you were to stay with it as just pain, there would be no
 need to find an answer, to find
 a solution, because there's no problem.
 There's only pain.
 So when we say to ourselves, "Pain, pain," we teach
 ourselves a new way to look at it
 as just pain.
 When you stay with it, that's mindfulness.
 When you really just see something as it is.
 That leads to clarity of vision.
 It leads to a new perspective where everything is just seen
 as it is.
 This clarity where your whole experience is just one of
 presence.
 So same when you feel lightheaded, anxious, nauseous.
 None of those should make you stop, because they're all
 just more experiences that you
 can note.
 If you feel anxious, you say anxious, anxious.
 If you feel lightheaded, you might say feeling, feeling or
 lightheaded.
 If you're dizzy, maybe.
 If you feel nauseous, you just say nauseous, nauseous.
 If there's disliking, you've known that as well.
 It might be that the way you're practicing is not according
 to the...
 It sounds like it's not.
 That's probably a reason for all the negative reactions.
 But for all those things, you just note them as they are.
 How do we note the cessation of consciousness?
 You don't.
 Well, you don't note in that way.
 Noting is not like that.
 Noting is noting as something.
 Now, you can be aware of the cessation and you're
 constantly aware of it, because it's
 kind of indirect, but you notice that the consciousness is
 arising and ceasing.
 Like you'll be thinking one thing and then suddenly you're
 thinking something else or
 so on.
 As you progress in the meditation, you'll see arising and
 ceasing.
 But we don't note ceasing or arising.
 We just note the thing.
 Like if you're thinking or aware, if you're aware, then you
 might say aware.
 And then you'll see ceasing as well.
 What is the difference between noting the belly, the nost
rils, or breathing through
 the ears?
 Well, basically I tell you to note at the belly.
 It's not technically any difference.
 I've never heard of breathing through the ears, but I'm not
 sure if this person is just
 being funny or what.
 Maybe referring to the sound of breathing.
 Is that a thing?
 Not one I've practiced, Bontu.
 How do you practice yoga mindfully?
 Focus on the breath, body sensation, how to know what to
 focus on when we could focus
 on so many things.
 I don't teach mindful yoga, so I don't know.
 But based on the fundamentals, you note the movements of
 the body, you note the postures
 of the body.
 If you're standing, walking, sitting, lying, you note that.
 When you move or bend or stretch or push or pull, you note
 all that.
 When you feel tension or pain, you note all that.
 When you feel liking or disliking, happy, unhappy, and so
 on.
 Calm, you note all of that.
 How to know what to focus on?
 Well, when you're not doing formal meditation, you can
 focus on pretty much anything.
 One good base is the body again.
 So focus on the posture of the body, walking, standing,
 sitting, or lying.
 Remember the movements of the body as well.
 If something distracts you from that, note it.
 When it sits gone, go back to the body.
 How to deal with regrets from the past?
 Just noting fear, sadness, regrets, worry, shame.
 Yeah, all of those.
 Also the thinking, note that as well.
 Any visuals as well, you can note that.
 I mean, it's going to take time.
 It's probably a very strong, many regrets are very strong
 and deeply ingrained.
 So don't be too discouraged when they don't immediately
 disappear.
 It takes time.
 It takes skill.
 It takes work.
 Noting the breath is preventing me from correctly seeing
 what thoughts and feelings I have.
 It's as if the noting of the breath was there above the
 feelings and thoughts I have.
 Is that good practice?
 So I don't teach noting the breath per se, but if you're
 talking about noting the stomach
 rising and falling, it really shouldn't get in the way.
 Breath might get in the way because it's conceptual, like
 breath going in, breath going out is just
 an idea.
 The truth is the feelings, right?
 And if you focus on the feelings, either at the stomach or
 at the nose, those feelings
 arise and cease and they'll be replaced by things like
 thoughts.
 You can't have two of them at once.
 One can only be aware of one object at a time, otherwise it
's what would it mean to be aware
 of two things at once?
 It's just a misconception because it's very quick and very
 chaotic.
 Any suggestions on how to stay mindful during daily life
 and not just when meditating?
 Practice, practice, practice.
 There's not much more to it than that.
 Try and do some formal meditation every day.
 That's a support for your daily practice.
 But it can take a long time.
 It's great that you're asking the question because it's a
 sign that you're interested.
 Just keep it up.
 It's hard.
 Hard things are good to practice.
 What to do if you find out your parents have been lying to
 you for a long time?
 If you simply forgive, what kind of meditation should I use
?
 Well forgive in this perspective means let go of the anger
 and any kind of sadness or
 feelings of betrayal and so on.
 That's all that's required as far as forgiving.
 You don't have to particularly say anything or take any
 view of forgiveness.
 You just have to let go of the anger.
 When you have no anger then you just view it as it is.
 You have wisdom about it.
 These people did this thing and that was bad.
 Obviously it's worse for them because it's very perverse to
 lie to someone.
 And then you have no problem with any kind of forgiveness.
 There are ways to reaffirm that of course.
 You can forgive, like actually verbally say, "I forgive
 these people."
 You can ask their forgiveness and then forgive them and
 that sort of thing.
 But ultimately just let go of the negative states.
 There's a difference between forgiving and forgetting.
 It doesn't mean you have any connection with those people.
 If someone is a bad person and continues with bad behavior
 it's better for both of you probably
 to have limited contact or to limit your contact based on
 that.
 I've stopped and started practicing many times.
 It is particularly hard when agitated and in pain.
 Several years into practice the sense of agitation and pain
 became more intense.
 Any advice?
 I don't know if you've considered doing, I don't know if
 you're doing our tradition,
 but assuming you are, I don't know if you've done the at-
home course.
 If you've done that already well you're probably at a stage
 where you're just having to slog
 through the bad habits that you have.
 These are just habits.
 There's no fixing them.
 When things become more intense that's not a bad sign.
 It's not anything at all.
 It's just a sign that they're still there.
 It's telling you how strong they are, how powerful they are
.
 Once you start to see them more objectively and see more
 objectively in general there's
 less agitation.
 Pain on the other hand is different.
 Pain is not something you're ever going to be free from.
 The freedom comes from not reacting, not getting upset
 about the pain.
 So don't be discouraged at all if there's intense pain as
 long as it's physical.
 If it's mental pain well then yeah that's related to disl
iking.
 As you become more objective, more at peace, less judgment
al, you'll find less reactionary.
 You'll find that settles down.
 I noticed that defilements such as paranoia started arising
 again after a long period
 of not occurring.
 Is this a sign that my practice has fallen or just that
 there are still roots present
 in me?
 Still roots present in you.
 Again we're so complicated and our problems are so much
 more deep rooted than we think
 that.
 Now if you've done a lot of intensive practice in the right
 way it would be discouraging
 to have really bad habits come back but you're always going
 to see the same bad habits coming
 back because that's what they are, they're habitual.
 In fact even once you're enlightened there's going to be
 shadows and echoes of those habits.
 Even Arahants had shadows and echoes of their bad habits.
 Just no more defilements, none of the things like paranoia.
 But even suppose someone with that problem were to become
 enlightened they might still
 have thoughts that were related to the paranoia but the
 thoughts just wouldn't make them paranoid.
 There would be no fear, there would just be the thought and
 then they'd say "Oh here comes
 that thought again."
 That's even still possible.
 So key in your practice is going to be separating the
 thoughts from the actual fear.
 Just because you have a thought that says "Oh my God I'm
 going to suffer in this way or
 that they're after me" or so on.
 Just because you have that thought doesn't mean you have
 any emotion based on it.
 Thoughts are also not under our control and they come in
 very strange ways.
 So just note the thoughts and let it go and separate it
 from the fear, note that as well.
 A lot of mental issues, the solution relates to separating
 them into pieces because when
 you make them monolithic they're not real anymore, you're
 not actually focusing on the
 reality.
 You're focusing on multiple realities and turning them into
 a boogeyman, a monster.
 Deconstruction is key.
 When we note seeing, do we keep in memory that this seeing
 is due to contact between
 eye and object?
 No we do not.
 We do not make any intellectual idea of it at all.
 "Dit te di tama tang bhavi sitti" - "Let seeing just be
 seeing."
 That really is all there is to it.
 The problem is that we make more of things than they
 actually are.
 "Nanimitta gahi nanubyanjana gahi" - "Don't cling to the
 details or the particulars.
 Let it just be seeing."
 Anything else related to that, you're going to see it as a
 result.
 You're going to see clearly about contact and so on.
 Anything that you need to see you'll see because it's there
.
 It doesn't require you to go looking for it.
 Like when you see a tiger you also see it stripes.
 You don't have to go looking for them.
 "What role does a philosophical contemplation play in a
 wholesome meditation practice?"
 Well, it depends what kind.
 That's called "jinta mayapanya".
 It's worldly.
 It's not a replacement for practice.
 In terms of supporting the practice, it can be useful.
 That being said, 99% of the time it's too much.
 It's not useful.
 You really don't need anything that we might recognize as
 philosophical.
 In fact, the only real thinking you need to do is putting
 what you've read and heard in
 a way that allows you to practice it.
 It's the step between hearing and practicing.
 You need to hear.
 You hear me tell you how to practice.
 Then you can't just take that hearing and practice it.
 You have to think about it.
 Thinking about it really literally only means, "Oh, okay.
 This is how I walk.
 All right.
 I understand that now."
 Then go and do it.
 It really isn't what we would recognize as philosophical.
 The same goes with the teachings on the Four Noble Truths
 and so on.
 We think about them far too much.
 You really don't need to do much thinking at all.
 It's the practicing that leads to wisdom and the wisdom isn
't thinking either.
 Wisdom isn't like, "Oh, I had this great deep thought."
 That's not wisdom.
 Wisdom means seeing clearly and seeing perfectly, fully.
 When you fully see something, see the truth really.
 It's not intellectual.
 How do we fabricate and note equanimity?
 Well, if you're asking for tips on how to fabricate equanim
ity, don't do that.
 If you're asking how equanimity comes into being, that's
 not really important.
 I'm not going to get into it.
 How do you note equanimity as equanimity?
 If you're calm, you would say calm, calm.
 If you feel neutral, you can say feeling, feeling, or
 neutral, neutral.
 Sorry.
 Did I get cut off there?
 Not to me anyway.
 I find my awareness is more like a series of returns to the
 present moment rather than
 continuous awareness.
 Is this incorrect practice?
 Yeah, that's right.
 Coming back to the present moment again and again, moment
 after moment, and that's because
 there are other habits that are competing.
 So after some time, you might find yourself able to feel
 more like staying with the present
 moment, but for sure in the beginning, it feels always like
 coming back.
 But even then, it's still arising and ceasing.
 It's going to be again and again applying the practice.
 It's quite energy intensive, having to do it again and
 again.
 But as a result, it trains you.
 It builds up energy.
 It makes you stronger and more energetic.
 It's a positive quality of mind.
 Is a moment where the mind knows clearly an object, a
 moment of nibbana?
 No.
 Knowing isn't arising.
 Knowing is something that arises.
 One is unarism, the state of non-arising.
 Have we done away with all the meditation questions?
 I have.
 Yeah, it actually looks like we're in a second tier.
 I think I might end it early today.
 It's been a long day.
 I've already answered a bunch of questions in the morning.
 I'm feeling a little bit out of it.
 So I think let's stop it there then.
 If there are no other urgent, or we'll stop if there are
 any last urgent ones, we can
 answer them otherwise.
 Thank you all for coming.
 Thank you both.
 And we'll try and be back on Wednesday again if I think we
've got our technical issues
 sorted out.
 Have a good day, everyone.
 Sadhu.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
