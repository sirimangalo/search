1
00:00:00,000 --> 00:00:13,000
 Very good evening everyone.

2
00:00:13,000 --> 00:00:29,880
 Broadcasting Live March 10th.

3
00:00:29,880 --> 00:00:58,880
 Today's quote is one of these quotes about the right time.

4
00:00:58,880 --> 00:01:09,350
 How do you find the right time? How do you find a good time

5
00:01:09,350 --> 00:01:16,880
? How do you find a happy time?

6
00:01:16,880 --> 00:01:28,880
 We look for happy times. We look forward to them. We work

7
00:01:28,880 --> 00:01:28,880
 towards them.

8
00:01:28,880 --> 00:01:36,600
 Even meditation, we have the idea that it's going to lead

9
00:01:36,600 --> 00:01:42,880
 us to happy, happier times in the future.

10
00:01:42,880 --> 00:01:51,880
 Meditation should lead us to happiness, right?

11
00:01:51,880 --> 00:01:55,030
 But if you know anything about Buddhism, the Buddhist

12
00:01:55,030 --> 00:01:59,390
 teaching, you know the sort of ideas that he presents in

13
00:01:59,390 --> 00:02:02,880
 regards to time.

14
00:02:02,880 --> 00:02:10,260
 That looking for a happy time, thinking of a specific time

15
00:02:10,260 --> 00:02:14,880
 or place or situation as being happy,

16
00:02:14,880 --> 00:02:20,700
 that's the wrong way to go about it. Wrong way to go about

17
00:02:20,700 --> 00:02:23,880
 finding happiness.

18
00:02:23,880 --> 00:02:44,340
 And so the Buddha says, "Yea bhikkhu ei satta pubhana samay

19
00:02:44,340 --> 00:02:45,160
ankaya na sutjalitang karanthi, vajaya sutjalitang karanthi,

20
00:02:45,160 --> 00:02:45,880
 manasa sutjalitam karanthi."

21
00:02:45,880 --> 00:02:58,170
 Whenever monks, oh monks, oh bhikkhus, whatever being

22
00:02:58,170 --> 00:03:08,240
 performs, when a being performs bodily good conduct with

23
00:03:08,240 --> 00:03:08,880
 the body,

24
00:03:08,880 --> 00:03:17,470
 good conduct with speech, and good conduct of the mind in

25
00:03:17,470 --> 00:03:23,880
 the morning, when one does that in the morning,

26
00:03:23,880 --> 00:03:38,490
 "supubhana ho bhikkhu ei te santana." That's a good morning

27
00:03:38,490 --> 00:03:41,880
 monks. It's a good morning for those beings.

28
00:03:41,880 --> 00:03:48,800
 "Yea bhikkhu ei satta madjan hi ka samayankaya." Whatever

29
00:03:48,800 --> 00:03:56,880
 beings in the middle of the day behave well with body,

30
00:03:56,880 --> 00:03:57,880
 behave well with speech,

31
00:03:57,880 --> 00:04:04,330
 behave well with mind. "Sumadjanikho bhikkhu ei te santana

32
00:04:04,330 --> 00:04:10,880
." That's a good midday for those beings.

33
00:04:10,880 --> 00:04:23,540
 The same with evening. Very important teaching. Somewhat

34
00:04:23,540 --> 00:04:36,880
 simple, I know. But important still. Important.

35
00:04:36,880 --> 00:04:43,370
 Everyone tells us about Buddhist approach to happiness in a

36
00:04:43,370 --> 00:04:52,370
 good time. That a good moment, "sunakatang sumangalang supa

37
00:04:52,370 --> 00:04:55,880
 bhataang sudhi tang."

38
00:04:55,880 --> 00:05:10,040
 It is a good season. It is a good blessing. It's a good

39
00:05:10,040 --> 00:05:12,880
 moment.

40
00:05:12,880 --> 00:05:25,880
 "Suputitang"

41
00:05:25,880 --> 00:05:33,100
 "Sukanoh sumo utto jasui tang brahma caisu." When one lives

42
00:05:33,100 --> 00:05:38,630
 the holy life well, just for a moment, that is a good

43
00:05:38,630 --> 00:05:39,880
 moment.

44
00:05:39,880 --> 00:05:46,880
 Actually, that's not quite what this says, is it?

45
00:05:46,880 --> 00:05:57,680
 Good behavior. A good state. Happiness is not to be found

46
00:05:57,680 --> 00:05:59,880
 in time or place.

47
00:05:59,880 --> 00:06:05,890
 Or not particular to time or place. Not because of the

48
00:06:05,890 --> 00:06:08,880
 specific time or place.

49
00:06:08,880 --> 00:06:15,880
 Happiness is to be found in every time and every place.

50
00:06:15,880 --> 00:06:22,880
 Or not. Or found in no place.

51
00:06:22,880 --> 00:06:30,410
 Dependent only on one's own state. If one behaves badly

52
00:06:30,410 --> 00:06:34,820
 with body, badly in speech, badly in mind, that's not a

53
00:06:34,820 --> 00:06:40,880
 good time. No matter when it is.

54
00:06:40,880 --> 00:06:52,360
 But the right moment, a happy time, happiness can be found

55
00:06:52,360 --> 00:06:57,880
 anywhere at any time. Peace.

56
00:06:57,880 --> 00:07:07,880
 When the mind is at peacefulness, when the body is properly

57
00:07:07,880 --> 00:07:15,880
 behaved, when killing or stealing or cheating,

58
00:07:15,880 --> 00:07:23,050
 when the speech is proper, not lying or hurting others with

59
00:07:23,050 --> 00:07:26,880
 speech or using useless speech,

60
00:07:26,880 --> 00:07:36,880
 and when the mind is well-directed, mindful, pure, clear,

61
00:07:36,880 --> 00:07:44,350
 and your mind is directed in the right way, aware of the

62
00:07:44,350 --> 00:07:50,880
 body, aware of speech, aware of the thoughts,

63
00:07:50,880 --> 00:07:55,720
 clear in the thoughts with no anger, with no greed, with no

64
00:07:55,720 --> 00:07:56,880
 delusion.

65
00:07:56,880 --> 00:08:01,780
 That moment, that's where happiness is. That's where

66
00:08:01,780 --> 00:08:04,880
 goodness is. That's the right moment.

67
00:08:04,880 --> 00:08:09,250
 The moment comes anywhere. You find it anywhere. You don't

68
00:08:09,250 --> 00:08:09,880
 need to go far.

69
00:08:09,880 --> 00:08:13,330
 All this ambition and this striving that we have for

70
00:08:13,330 --> 00:08:17,880
 finding happiness, it's all misdirected, misguided.

71
00:08:17,880 --> 00:08:30,880
 Find happiness here and now, wherever here and now is.

72
00:08:30,880 --> 00:08:43,100
 Te atalandha sukita virulham dasasane. They have gained

73
00:08:43,100 --> 00:08:46,880
 something useful.

74
00:08:46,880 --> 00:08:55,800
 And they are happy. Te atalandha. This is actually a

75
00:08:55,800 --> 00:08:57,880
 blessing, I think.

76
00:08:57,880 --> 00:09:00,860
 It's a blessing that we do and I can't remember exactly how

77
00:09:00,860 --> 00:09:01,880
 it translates.

78
00:09:01,880 --> 00:09:07,880
 Vrulha buddhasas naimi, they prosper in the buddhasasana.

79
00:09:07,880 --> 00:09:09,880
 This is a chant that we do quite often.

80
00:09:09,880 --> 00:09:17,380
 Sunakatang, sunangalang, sutapatang, sututitang, sukanosam

81
00:09:17,380 --> 00:09:18,880
otoh, jasugitang, brahmajarisu,

82
00:09:18,880 --> 00:09:26,490
 patakinanga, aikamanga, ajakamanga, patakinanga, patakin

83
00:09:26,490 --> 00:09:26,880
anga nukamanga,

84
00:09:26,880 --> 00:09:35,880
 vaneedite patakine, patakinani kattva, davandate patakine.

85
00:09:37,880 --> 00:09:42,770
 This is pubanha sutta. It's an important sutta because of

86
00:09:42,770 --> 00:09:44,880
 what it says at the top.

87
00:09:44,880 --> 00:09:50,880
 Morning, afternoon, early evening. It doesn't matter when,

88
00:09:50,880 --> 00:09:53,880
 it doesn't matter where.

89
00:09:53,880 --> 00:10:00,880
 Happiness, goodness, the happy day.

90
00:10:00,880 --> 00:10:03,400
 It's quite simple. It comes from the body, comes from

91
00:10:03,400 --> 00:10:05,880
 speech, comes from the mind.

92
00:10:05,880 --> 00:10:11,500
 So that's the demo for tonight. Thank you all for tuning in

93
00:10:11,500 --> 00:10:11,880
.

94
00:10:11,880 --> 00:10:17,830
 I guess we can go, if anybody has questions, I'm going to

95
00:10:17,830 --> 00:10:22,880
 post the hangout.

96
00:10:22,880 --> 00:10:32,180
 So you can click on the link. If you've got questions, join

97
00:10:32,180 --> 00:10:34,880
 the hangout.

98
00:10:34,880 --> 00:10:40,880
 And if there are no questions, that's all for tonight.

99
00:10:40,880 --> 00:11:04,880
 [silence]

100
00:11:04,880 --> 00:11:14,880
 I did a good evening. Larry.

101
00:11:14,880 --> 00:11:16,880
 How are you?

102
00:11:16,880 --> 00:11:23,880
 I'm good. Thank you. We're having some pretty heavy weather

103
00:11:23,880 --> 00:11:27,880
 down here in Mississippi today and tonight.

104
00:11:27,880 --> 00:11:36,020
 I don't expect to keep internet. In a little while we're

105
00:11:36,020 --> 00:11:37,880
 probably going to lose internet.

106
00:11:37,880 --> 00:11:40,880
 But other than that, doing good.

107
00:11:40,880 --> 00:11:44,880
 You have some kind of hurricane?

108
00:11:44,880 --> 00:11:52,580
 Just a big front system moving across the southern, western

109
00:11:52,580 --> 00:11:56,880
 part of the south, deep south.

110
00:11:56,880 --> 00:12:02,530
 It seems to be kind of regular here, low winds, and often

111
00:12:02,530 --> 00:12:07,140
 out of rain, and flash flood warnings, and tornado watches,

112
00:12:07,140 --> 00:12:09,880
 and that sort of thing.

113
00:12:09,880 --> 00:12:16,880
 I attributed to climate change.

114
00:12:16,880 --> 00:12:19,490
 But good to see you. Good evening. Hope you're having a

115
00:12:19,490 --> 00:12:19,880
 good day.

116
00:12:19,880 --> 00:12:22,880
 Yeah, you too. Thanks.

117
00:12:22,880 --> 00:12:25,880
 No question, I guess.

118
00:12:25,880 --> 00:12:29,880
 Yeah. Looks like no one else can come on.

119
00:12:29,880 --> 00:12:38,300
 I'm curious. Walking meditation, and doing half-walk and

120
00:12:38,300 --> 00:12:42,730
 half-sitting, so if I want to sit for 45 minutes or an hour

121
00:12:42,730 --> 00:12:47,880
, then I need to walk for 45 minutes or an hour.

122
00:12:47,880 --> 00:12:58,910
 And I wonder what the Theravada forest tradition view is on

123
00:12:58,910 --> 00:12:59,880
 walking meditate.

124
00:12:59,880 --> 00:13:07,120
 Using one of these labyrinths that you see, it's a walking

125
00:13:07,120 --> 00:13:14,050
 pattern that's typically out of doors, a large walking

126
00:13:14,050 --> 00:13:18,830
 pattern that you do, kind of weave from the outside, follow

127
00:13:18,830 --> 00:13:19,880
 this path,

128
00:13:19,880 --> 00:13:23,960
 intricately, until you get to the middle, and then you turn

129
00:13:23,960 --> 00:13:28,540
 around and walk back out. It's not one that you get lost in

130
00:13:28,540 --> 00:13:28,880
.

131
00:13:28,880 --> 00:13:35,990
 It's one that you kind of meditatively walk slowly along,

132
00:13:35,990 --> 00:13:42,630
 rather than as an option to just walking in a straight line

133
00:13:42,630 --> 00:13:47,110
, say 10 or 15 feet, walking, turn around, come back, go

134
00:13:47,110 --> 00:13:48,880
 back and forth.

135
00:13:48,880 --> 00:13:52,880
 And it's just a curiosity question.

136
00:13:52,880 --> 00:13:59,880
 Why would you walk in a different way?

137
00:13:59,880 --> 00:14:07,220
 Well, it's a way of having, it appears, I really don't know

138
00:14:07,220 --> 00:14:15,450
 that much about them, but say an area that's maybe 20, 30,

139
00:14:15,450 --> 00:14:19,880
 40 feet across the conference,

140
00:14:19,880 --> 00:14:27,370
 and you start at a point on the outside, and it's like a

141
00:14:27,370 --> 00:14:34,340
 little defined path that goes around, comes back around on

142
00:14:34,340 --> 00:14:34,880
 itself,

143
00:14:34,880 --> 00:14:40,200
 and you eventually work to the middle of the pattern, at

144
00:14:40,200 --> 00:14:45,010
 which point you turn around and follow that same path, a

145
00:14:45,010 --> 00:14:47,880
 little trail back out.

146
00:14:47,880 --> 00:14:53,790
 It's kind of a novel looking thing, and I didn't know if it

147
00:14:53,790 --> 00:15:03,880
 was something that's contrary, evidently a number of faiths

148
00:15:03,880 --> 00:15:10,880
 or religions make use of it around the world.

149
00:15:10,880 --> 00:15:16,880
 Presumably, some Buddhist and some Christian, Orthodox

150
00:15:16,880 --> 00:15:20,880
 Christian churches and such as that.

151
00:15:20,880 --> 00:15:26,880
 So it's really just a curiosity thing.

152
00:15:26,880 --> 00:15:28,880
 Never heard of it.

153
00:15:28,880 --> 00:15:36,880
 Okay. I don't have a picture of one handy.

154
00:15:36,880 --> 00:15:47,200
 And I do want to ask about the walking meditation

155
00:15:47,200 --> 00:15:50,880
 instruction.

156
00:15:50,880 --> 00:15:55,840
 You instructed me to pick my foot up for the next step,

157
00:15:55,840 --> 00:16:00,910
 pick it straight up, take a step and place it so that I'm

158
00:16:00,910 --> 00:16:06,880
 lifting, placing, lifting, placing, lifting, placing.

159
00:16:06,880 --> 00:16:13,350
 And to lift my foot, flat footed straight up, and then go

160
00:16:13,350 --> 00:16:19,800
 forward with it and place it straight down is causing me to

161
00:16:19,800 --> 00:16:22,880
 take some pretty small steps.

162
00:16:22,880 --> 00:16:30,220
 And it doesn't bother me, but it makes the walking process

163
00:16:30,220 --> 00:16:36,590
 much more tedious, and it's requiring a little more

164
00:16:36,590 --> 00:16:43,880
 attention to the mechanics of the walking process.

165
00:16:43,880 --> 00:16:52,210
 I'm just wondering if that's what's intended, and I think I

166
00:16:52,210 --> 00:16:54,880
'm doing it right.

167
00:16:54,880 --> 00:16:59,520
 Well, watch the video. You can see how I do it in the video

168
00:16:59,520 --> 00:16:59,880
.

169
00:16:59,880 --> 00:17:06,310
 I lost the video. I was able to watch it that one time,

170
00:17:06,310 --> 00:17:12,400
 about six of the eight seconds, and then it didn't continue

171
00:17:12,400 --> 00:17:14,880
 and I don't have it.

172
00:17:14,880 --> 00:17:17,880
 It'll be in the Hangout.

173
00:17:17,880 --> 00:17:18,880
 Okay.

174
00:17:18,880 --> 00:17:21,880
 In our Hangout list.

175
00:17:21,880 --> 00:17:23,880
 It'll be a Lincoln Hangout?

176
00:17:23,880 --> 00:17:24,880
 Yeah.

177
00:17:24,880 --> 00:17:28,880
 Okay. Super.

178
00:17:28,880 --> 00:17:36,880
 And also, go ahead.

179
00:17:36,880 --> 00:17:39,880
 And then one other question.

180
00:17:39,880 --> 00:17:43,880
 You're saying during the

181
00:17:43,880 --> 00:17:51,020
 Daima talk in the evenings that some are watching it on

182
00:17:51,020 --> 00:17:52,880
 YouTube.

183
00:17:52,880 --> 00:17:57,830
 Is the live stream, live broadcast that's available on the

184
00:17:57,830 --> 00:18:04,330
 Meditation Plus site, is it also available as a YouTube

185
00:18:04,330 --> 00:18:05,880
 visual?

186
00:18:05,880 --> 00:18:07,880
 This one is.

187
00:18:07,880 --> 00:18:08,880
 Okay.

188
00:18:08,880 --> 00:18:10,880
 They aren't all.

189
00:18:10,880 --> 00:18:16,880
 Okay. And how would I reach that?

190
00:18:16,880 --> 00:18:21,880
 Go to the YouTube, my YouTube channel.

191
00:18:21,880 --> 00:18:24,880
 Okay. Okay.

192
00:18:24,880 --> 00:18:29,870
 I evidently didn't do something right because I went to it,

193
00:18:29,870 --> 00:18:37,880
 but all I saw available were previous episodes.

194
00:18:37,880 --> 00:18:41,310
 Yeah, I'm not that familiar with it, actually. I'm just

195
00:18:41,310 --> 00:18:42,880
 pretty sure that it works.

196
00:18:42,880 --> 00:18:45,880
 There's not always a video.

197
00:18:45,880 --> 00:18:46,880
 Okay.

198
00:18:46,880 --> 00:18:49,880
 I don't always record video.

199
00:18:49,880 --> 00:18:55,880
 Okay. No big deal. I was just curious about that too.

200
00:18:55,880 --> 00:18:57,880
 Okay.

201
00:18:57,880 --> 00:18:58,880
 Well, I know you're busy.

202
00:18:58,880 --> 00:19:02,980
 So, I won't. I look like I'm the only one here, so I won't

203
00:19:02,980 --> 00:19:03,880
 hold you up.

204
00:19:03,880 --> 00:19:04,880
 All right.

205
00:19:04,880 --> 00:19:06,880
 Take care. Take care. Practice on. Thank you very much.

206
00:19:06,880 --> 00:19:07,880
 Thank you.

207
00:19:07,880 --> 00:19:08,880
 Good night.

208
00:19:08,880 --> 00:19:09,880
 Good night.

209
00:19:11,880 --> 00:19:12,880
 Thank you.

