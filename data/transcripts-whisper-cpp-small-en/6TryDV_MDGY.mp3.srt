1
00:00:00,000 --> 00:00:10,880
 Okay, trying again.

2
00:00:10,880 --> 00:00:17,480
 Looks good.

3
00:00:17,480 --> 00:00:25,870
 So tonight I was thinking about a verse, a Dhammapada verse

4
00:00:25,870 --> 00:00:31,840
, I think, a Rau Gaya Paramala

5
00:00:31,840 --> 00:00:33,280
 Bhah.

6
00:00:33,280 --> 00:00:40,690
 Freedom from sickness, freedom from illness is the greatest

7
00:00:40,690 --> 00:00:41,760
 gain.

8
00:00:41,760 --> 00:00:49,980
 Bhan tu ti paramang dhanang, contentment is the greatest

9
00:00:49,980 --> 00:00:51,040
 wealth.

10
00:00:51,040 --> 00:01:03,550
 We sasa paramang yati, paramang yati, the familiarity is

11
00:01:03,550 --> 00:01:08,280
 the greatest relation.

12
00:01:08,280 --> 00:01:20,560
 Nibaanang paramang sukang, Nibaan is the highest happiness.

13
00:01:20,560 --> 00:01:25,560
 I'm thinking about this verse.

14
00:01:25,560 --> 00:01:29,200
 You can really get a sense of the Buddha saying this,

15
00:01:29,200 --> 00:01:32,000
 looking out on the community of monks

16
00:01:32,000 --> 00:01:37,990
 living in the forest, reflecting on all that he'd been

17
00:01:37,990 --> 00:01:42,640
 through to become a Buddha and expressing

18
00:01:42,640 --> 00:01:54,200
 his, expressing his perception, his outlook on the world in

19
00:01:54,200 --> 00:01:59,720
 this way.

20
00:01:59,720 --> 00:02:04,440
 Rau Gaya means not having Roga, Roga means that which ruch

21
00:02:04,440 --> 00:02:07,000
ati, that which disturbs you,

22
00:02:07,000 --> 00:02:15,160
 afflicts you, affliction.

23
00:02:15,160 --> 00:02:25,120
 The greatest gain, the greatest possession.

24
00:02:25,120 --> 00:02:32,840
 Greatest gain actually, laba means gain.

25
00:02:32,840 --> 00:02:37,370
 Ask anyone who's physically sick, right, physically ill, do

26
00:02:37,370 --> 00:02:38,800
 they want money?

27
00:02:38,800 --> 00:02:39,800
 No.

28
00:02:39,800 --> 00:02:50,720
 Fame, glory, mansion, power.

29
00:02:50,720 --> 00:02:54,250
 All the riches in all the world won't save you from

30
00:02:54,250 --> 00:02:55,240
 sickness.

31
00:02:55,240 --> 00:02:57,400
 Four kinds of sickness in Buddhism.

32
00:02:57,400 --> 00:03:04,540
 There's sickness that comes from food, sickness that comes

33
00:03:04,540 --> 00:03:07,560
 from the environment, sickness

34
00:03:07,560 --> 00:03:16,490
 that comes from karma, and sickness that comes from the

35
00:03:16,490 --> 00:03:17,880
 mind.

36
00:03:17,880 --> 00:03:20,590
 So actually it's quite likely that the Buddha wasn't

37
00:03:20,590 --> 00:03:22,540
 thinking of physical illness when he

38
00:03:22,540 --> 00:03:23,540
 said this.

39
00:03:23,540 --> 00:03:28,510
 I mean, I think we can all agree that if you're physically

40
00:03:28,510 --> 00:03:31,960
 unwell, all the material possessions

41
00:03:31,960 --> 00:03:42,900
 in the world are not going to compare to the gain of

42
00:03:42,900 --> 00:03:47,000
 becoming free from that sickness.

43
00:03:47,000 --> 00:03:51,280
 And a lot of money and a lot of effort just to free

44
00:03:51,280 --> 00:03:53,880
 ourselves from illness.

45
00:03:53,880 --> 00:03:56,930
 Everything else becomes meaningless because you can't of

46
00:03:56,930 --> 00:03:58,740
 course enjoy any of the pleasures

47
00:03:58,740 --> 00:04:02,200
 or riches or gains, accomplishments in life if you're

48
00:04:02,200 --> 00:04:03,480
 physically ill.

49
00:04:03,480 --> 00:04:09,060
 But I don't think that was exactly what the Buddha was

50
00:04:09,060 --> 00:04:11,040
 talking about.

51
00:04:11,040 --> 00:04:13,200
 It actually isn't the greatest gain.

52
00:04:13,200 --> 00:04:18,650
 Not in the Buddhist sense because you have to understand

53
00:04:18,650 --> 00:04:21,280
 sickness of the mind and to

54
00:04:21,280 --> 00:04:25,920
 understand the full range of sickness.

55
00:04:25,920 --> 00:04:31,280
 A person can be perfectly healthy in the body but if their

56
00:04:31,280 --> 00:04:34,960
 mind is ill, the physical body,

57
00:04:34,960 --> 00:04:40,120
 physical health does them no good either.

58
00:04:40,120 --> 00:04:48,180
 So sickness from food, sickness from the environment, these

59
00:04:48,180 --> 00:04:50,400
 are physical.

60
00:04:50,400 --> 00:04:54,810
 Sickness from karma, while that can be physical, can be

61
00:04:54,810 --> 00:04:55,720
 mental.

62
00:04:55,720 --> 00:04:59,750
 But sometimes we're born with diseases, with illnesses,

63
00:04:59,750 --> 00:05:01,040
 physical illnesses.

64
00:05:01,040 --> 00:05:08,080
 Some people are born deficient in some way in the body.

65
00:05:08,080 --> 00:05:15,080
 Some people are born with all kinds of illness.

66
00:05:15,080 --> 00:05:20,980
 Some people die, some humans die shortly after childbirth

67
00:05:20,980 --> 00:05:23,680
 because they're so ill.

68
00:05:23,680 --> 00:05:25,360
 This can be because of karma.

69
00:05:25,360 --> 00:05:29,800
 I mean sickness because of food is quite obvious.

70
00:05:29,800 --> 00:05:33,180
 You eat too much, you eat the wrong kinds of food, this is

71
00:05:33,180 --> 00:05:34,200
 quite clear.

72
00:05:34,200 --> 00:05:40,550
 And the environment could be anything from bacteria,

73
00:05:40,550 --> 00:05:45,680
 viruses, infections, radiation perhaps,

74
00:05:45,680 --> 00:05:46,680
 chemicals.

75
00:05:46,680 --> 00:05:52,040
 Now everything's got these terrible chemicals in it making

76
00:05:52,040 --> 00:05:53,400
 us all sick.

77
00:05:53,400 --> 00:05:58,920
 But karma, karma, that one's not so clear.

78
00:05:58,920 --> 00:06:01,660
 Karma is the Buddhist answer to why we're born certain ways

79
00:06:01,660 --> 00:06:01,880
.

80
00:06:01,880 --> 00:06:06,520
 In fact, not even karma, not just karma, right?

81
00:06:06,520 --> 00:06:10,170
 Because while the environment can have a factor, it can

82
00:06:10,170 --> 00:06:11,200
 play a part.

83
00:06:11,200 --> 00:06:16,430
 Karma certainly plays a part but it's much more complicated

84
00:06:16,430 --> 00:06:17,640
 than that.

85
00:06:17,640 --> 00:06:22,020
 Sometimes we perform certain karmas and as a byproduct bad

86
00:06:22,020 --> 00:06:24,040
 or good things happen that

87
00:06:24,040 --> 00:06:28,380
 are totally, I mean things that we like but totally had

88
00:06:28,380 --> 00:06:30,920
 nothing to do with the karma.

89
00:06:30,920 --> 00:06:31,920
 Right?

90
00:06:31,920 --> 00:06:38,080
 Like perhaps you did some very bad things and as a result

91
00:06:38,080 --> 00:06:40,880
 you have this bad karma in

92
00:06:40,880 --> 00:06:47,510
 your life but as a result like let's say a person who is

93
00:06:47,510 --> 00:06:50,920
 very poor and so they have to

94
00:06:50,920 --> 00:06:55,350
 go and live off in the countryside and then there's a fire

95
00:06:55,350 --> 00:06:58,120
 that burns down the city, right?

96
00:06:58,120 --> 00:07:02,200
 I mean just the point being that there are circumstances or

97
00:07:02,200 --> 00:07:04,120
 results that don't always

98
00:07:04,120 --> 00:07:05,120
 have to do with karma.

99
00:07:05,120 --> 00:07:09,040
 I'm just trying to say not everything is karma.

100
00:07:09,040 --> 00:07:11,710
 This person who was very poor because they were maybe very

101
00:07:11,710 --> 00:07:13,200
 stingy so he'd go, "Oh look,

102
00:07:13,200 --> 00:07:15,080
 it's a very bad thing."

103
00:07:15,080 --> 00:07:20,310
 But actually they got quite lucky because it meant that

104
00:07:20,310 --> 00:07:22,880
 they didn't meet with the same

105
00:07:22,880 --> 00:07:26,440
 fate or rich people who as a result of being rich someone

106
00:07:26,440 --> 00:07:28,080
 comes and steals stuff from them.

107
00:07:28,080 --> 00:07:32,240
 It doesn't have to be past karma.

108
00:07:32,240 --> 00:07:37,560
 You can do all sorts of good karma to become rich and then

109
00:07:37,560 --> 00:07:40,520
 the vicissitudes of life that

110
00:07:40,520 --> 00:07:43,200
 may have nothing to do with karma just take it all away.

111
00:07:43,200 --> 00:07:45,200
 It can happen.

112
00:07:45,200 --> 00:07:49,360
 It's quite complicated is the point.

113
00:07:49,360 --> 00:07:56,070
 But karma certainly plays a part in this life and from life

114
00:07:56,070 --> 00:07:59,160
 to life as an extension.

115
00:07:59,160 --> 00:08:04,210
 But I think we all get a sense that it's the mental illness

116
00:08:04,210 --> 00:08:07,060
 that the Buddha was really thinking

117
00:08:07,060 --> 00:08:12,280
 of or that's most important here.

118
00:08:12,280 --> 00:08:18,380
 The greatest gain and it's a gain you see because we're all

119
00:08:18,380 --> 00:08:20,560
 sick in the mind until

120
00:08:20,560 --> 00:08:24,800
 we become free from that sickness.

121
00:08:24,800 --> 00:08:27,720
 Till we gain that greatest gain.

122
00:08:27,720 --> 00:08:37,280
 We're becoming free from greed, free from anger, free from

123
00:08:37,280 --> 00:08:43,360
 arrogance, conceit, free from attachment

124
00:08:43,360 --> 00:08:50,870
 and addiction, free from views and opinions and beliefs and

125
00:08:50,870 --> 00:08:54,040
 free from the things that

126
00:08:54,040 --> 00:08:59,160
 keep us stressed and suffering in our minds.

127
00:08:59,160 --> 00:09:03,470
 Like depression is a mental illness or anxiety but

128
00:09:03,470 --> 00:09:06,320
 arrogance and conceit they are also mental

129
00:09:06,320 --> 00:09:08,020
 illnesses.

130
00:09:08,020 --> 00:09:12,040
 Addiction is a mental illness.

131
00:09:12,040 --> 00:09:22,680
 Anger is a mental illness.

132
00:09:22,680 --> 00:09:24,800
 So that's the first part of the verse.

133
00:09:24,800 --> 00:09:25,800
 Freedom from illness.

134
00:09:25,800 --> 00:09:28,120
 What's the greatest gain?

135
00:09:28,120 --> 00:09:30,720
 Becoming free from this sickness.

136
00:09:30,720 --> 00:09:33,280
 Someone was telling me this is like a hospital.

137
00:09:33,280 --> 00:09:37,040
 We're all like sick patients here.

138
00:09:37,040 --> 00:09:39,040
 It's quite true.

139
00:09:39,040 --> 00:09:43,380
 The real problem is all those people out there who should

140
00:09:43,380 --> 00:09:46,040
 be in the hospital and are running

141
00:09:46,040 --> 00:09:49,700
 free contaminating each other, spreading their sickness to

142
00:09:49,700 --> 00:09:50,560
 each other.

143
00:09:50,560 --> 00:09:52,400
 Really, right?

144
00:09:52,400 --> 00:09:53,400
 There's no exaggeration.

145
00:09:53,400 --> 00:09:56,940
 There are sick people out there doing sick things and

146
00:09:56,940 --> 00:09:58,560
 spreading sickness.

147
00:09:58,560 --> 00:10:05,880
 They really should find a hospital.

148
00:10:05,880 --> 00:10:11,680
 Find a doctor like the Buddha.

149
00:10:11,680 --> 00:10:13,920
 Santuti Paramangdhanang.

150
00:10:13,920 --> 00:10:20,830
 In the Buddha living off in the forest, content with no lux

151
00:10:20,830 --> 00:10:24,200
uries, no possessions.

152
00:10:24,200 --> 00:10:27,920
 Monks aren't even allowed to touch money.

153
00:10:27,920 --> 00:10:32,760
 The greatest possession is contentment.

154
00:10:32,760 --> 00:10:35,240
 Looking out at the people, these people content.

155
00:10:35,240 --> 00:10:37,200
 How content are you?

156
00:10:37,200 --> 00:10:41,080
 It might feel quite discontent in the beginning.

157
00:10:41,080 --> 00:10:44,560
 But how great is it to be able to be here?

158
00:10:44,560 --> 00:10:48,750
 How powerful is it to not have to go and seek out

159
00:10:48,750 --> 00:10:51,760
 entertainment or diversion?

160
00:10:51,760 --> 00:11:00,400
 How great it is that you can come and be at peace?

161
00:11:00,400 --> 00:11:03,520
 You can see how challenging it is, right?

162
00:11:03,520 --> 00:11:09,120
 We're quite poor in our desires for things.

163
00:11:09,120 --> 00:11:12,200
 People in society are very poor because they're always

164
00:11:12,200 --> 00:11:12,920
 wanting.

165
00:11:12,920 --> 00:11:17,360
 They never have all that they want.

166
00:11:17,360 --> 00:11:20,870
 And you come here and you realize how poor we are because

167
00:11:20,870 --> 00:11:23,640
 we don't have this contentment.

168
00:11:23,640 --> 00:11:30,400
 And we sit and we're displeased, we're unsatisfied.

169
00:11:30,400 --> 00:11:36,040
 We want something, anything, right?

170
00:11:36,040 --> 00:11:39,600
 We're lacking.

171
00:11:39,600 --> 00:11:43,560
 We don't have all that we want.

172
00:11:43,560 --> 00:11:46,350
 And so the Buddha looked out on all the arahants sitting

173
00:11:46,350 --> 00:11:48,280
 around him and he thought about his

174
00:11:48,280 --> 00:11:51,360
 own trials.

175
00:11:51,360 --> 00:11:55,630
 Now he had become content and he said, "This is the

176
00:11:55,630 --> 00:11:58,680
 greatest, the greatest possession."

177
00:11:58,680 --> 00:12:00,600
 If you're content, you don't need anything, right?

178
00:12:00,600 --> 00:12:02,960
 You never want for anything.

179
00:12:02,960 --> 00:12:08,760
 There's no question that that is the greatest possession.

180
00:12:08,760 --> 00:12:11,770
 There's no other possession that can make you satisfied

181
00:12:11,770 --> 00:12:13,000
 like contentment.

182
00:12:13,000 --> 00:12:16,000
 It's the second part.

183
00:12:16,000 --> 00:12:21,200
 The third part of the verse, "We sahsapaarmanyati."

184
00:12:21,200 --> 00:12:25,240
 Now I thought about this one and it was kind of strange.

185
00:12:25,240 --> 00:12:27,760
 I think, "Well, what's so important about that?"

186
00:12:27,760 --> 00:12:34,070
 But then you think about where the Buddha was when he said

187
00:12:34,070 --> 00:12:35,080
 this.

188
00:12:35,080 --> 00:12:39,700
 Far, far away from his father and his mother and his family

189
00:12:39,700 --> 00:12:42,400
, but surrounded by people like

190
00:12:42,400 --> 00:12:44,880
 us.

191
00:12:44,880 --> 00:12:53,600
 Meditators, monastics, spiritual beings.

192
00:12:53,600 --> 00:12:57,010
 We sahsapa means, sorry I didn't translate, we sahsapa

193
00:12:57,010 --> 00:12:59,080
 means familiarity, but it has a

194
00:12:59,080 --> 00:13:03,040
 sense of the idea.

195
00:13:03,040 --> 00:13:10,650
 We sahsapa relates to if I have certain possessions and I

196
00:13:10,650 --> 00:13:14,440
 say to you, "Hey, you're welcome to

197
00:13:14,440 --> 00:13:15,440
 take anything you want.

198
00:13:15,440 --> 00:13:17,440
 Don't have to ask me."

199
00:13:17,440 --> 00:13:21,030
 I don't remember what the word is if there is a better word

200
00:13:21,030 --> 00:13:22,800
 than familiarity, but it

201
00:13:22,800 --> 00:13:27,810
 means this sense of taking each other for granted or being

202
00:13:27,810 --> 00:13:30,200
 able to take each other for

203
00:13:30,200 --> 00:13:36,800
 granted and taking it for granted.

204
00:13:36,800 --> 00:13:42,270
 What's mine is yours, what's yours is mine, that kind of

205
00:13:42,270 --> 00:13:43,240
 thing.

206
00:13:43,240 --> 00:13:47,200
 So it's like when we talk about dhamma brothers and sisters

207
00:13:47,200 --> 00:13:49,200
, our dhamma relatives.

208
00:13:49,200 --> 00:13:56,800
 Nyatidham, they say in Thai, dhamma is dhamma, nyati, so d

209
00:13:56,800 --> 00:14:01,440
harmma relatives, dharmma family.

210
00:14:01,440 --> 00:14:04,440
 And so all of you even here in Second Life, all of you on

211
00:14:04,440 --> 00:14:06,400
 YouTube were part of an extended

212
00:14:06,400 --> 00:14:07,400
 family.

213
00:14:07,400 --> 00:14:16,110
 Although some of us are like any family at different stages

214
00:14:16,110 --> 00:14:19,840
 of maturity, but we're all

215
00:14:19,840 --> 00:14:26,600
 family and the greatest, the greatest family is this.

216
00:14:26,600 --> 00:14:31,770
 When we're in harmony, I can depend upon all of you to not

217
00:14:31,770 --> 00:14:34,200
 steal my things or break my

218
00:14:34,200 --> 00:14:38,200
 things or to hurt me or to lie to me or to cheat me.

219
00:14:38,200 --> 00:14:43,180
 The most wonderful thing about being a Buddhist monk is

220
00:14:43,180 --> 00:14:46,400
 that for the most part you're surrounded

221
00:14:46,400 --> 00:14:48,000
 by good people.

222
00:14:48,000 --> 00:14:50,220
 I'm always shocked when I find out someone was lying to me

223
00:14:50,220 --> 00:14:51,280
 because people don't lie

224
00:14:51,280 --> 00:14:55,520
 to me that often, not that I know, but I think generally

225
00:14:55,520 --> 00:14:57,520
 lie to me a lot less than they do

226
00:14:57,520 --> 00:14:59,520
 to others.

227
00:14:59,520 --> 00:15:00,880
 And I can depend upon that.

228
00:15:00,880 --> 00:15:02,600
 That's so great.

229
00:15:02,600 --> 00:15:03,960
 That's the most wonderful.

230
00:15:03,960 --> 00:15:05,520
 I can't really depend on my family.

231
00:15:05,520 --> 00:15:11,040
 I don't know if they'll lie to me or not.

232
00:15:11,040 --> 00:15:14,830
 But being we are born into certain families, we're born

233
00:15:14,830 --> 00:15:17,280
 into a certain family with certain

234
00:15:17,280 --> 00:15:19,280
 relatives.

235
00:15:19,280 --> 00:15:25,840
 But those aren't the greatest relatives.

236
00:15:25,840 --> 00:15:30,540
 The greatest of all relations doesn't have to do with blood

237
00:15:30,540 --> 00:15:30,880
.

238
00:15:30,880 --> 00:15:39,400
 It has to do with familiarity and compatibility.

239
00:15:39,400 --> 00:15:43,400
 So he was expressing his appreciation of his community.

240
00:15:43,400 --> 00:15:47,180
 So we take a moment to also appreciate our community here,

241
00:15:47,180 --> 00:15:49,280
 how great it is to be surrounded

242
00:15:49,280 --> 00:15:52,360
 by such good people.

243
00:15:52,360 --> 00:16:01,760
 And the fourth part we get to the core of the issue.

244
00:16:01,760 --> 00:16:10,760
 Freedom from suffering is the highest happiness.

245
00:16:10,760 --> 00:16:14,360
 Nibana is a scary thing, I think.

246
00:16:14,360 --> 00:16:20,240
 Anything strange and unfamiliar is scary.

247
00:16:20,240 --> 00:16:23,510
 But there's a special dread, I think, associated with nib

248
00:16:23,510 --> 00:16:25,640
ana because it threatens everything

249
00:16:25,640 --> 00:16:28,480
 we hold dear.

250
00:16:28,480 --> 00:16:30,640
 Purposefully, right?

251
00:16:30,640 --> 00:16:35,970
 Nibana if you learn anything about it, it means you let go

252
00:16:35,970 --> 00:16:37,760
 of everything.

253
00:16:37,760 --> 00:16:41,760
 Nibana is no attachment.

254
00:16:41,760 --> 00:16:46,600
 None of the things you love, none of the things you want.

255
00:16:46,600 --> 00:16:47,720
 It's none of them.

256
00:16:47,720 --> 00:16:57,040
 None of the things you love.

257
00:16:57,040 --> 00:16:58,240
 So it's actually quite stressful.

258
00:16:58,240 --> 00:17:01,400
 Nibana is a very stressful thing for us.

259
00:17:01,400 --> 00:17:02,920
 Until you realize it, of course.

260
00:17:02,920 --> 00:17:06,050
 Well, until you start to see the things that you're holding

261
00:17:06,050 --> 00:17:07,400
 onto and saying, "Hey, wait

262
00:17:07,400 --> 00:17:08,400
 a minute.

263
00:17:08,400 --> 00:17:10,520
 I don't want to let go of this."

264
00:17:10,520 --> 00:17:13,100
 You realize that all those things are not worth clinging to

265
00:17:13,100 --> 00:17:13,320
.

266
00:17:13,320 --> 00:17:17,270
 All of those things are impermanent, unsatisfying,

267
00:17:17,270 --> 00:17:24,960
 uncontrollable, meaningless, purposeless, useless.

268
00:17:24,960 --> 00:17:25,960
 And you see this.

269
00:17:25,960 --> 00:17:29,800
 This is what you're starting to see or have started to have

270
00:17:29,800 --> 00:17:30,800
 come to see.

271
00:17:30,800 --> 00:17:36,680
 Yes, there's no real meaning or purpose or benefit to these

272
00:17:36,680 --> 00:17:37,800
 things.

273
00:17:37,800 --> 00:17:43,320
 Purpose can't be found in a risen phenomena.

274
00:17:43,320 --> 00:17:46,640
 It can be found in Nibana, which is true peace.

275
00:17:46,640 --> 00:17:48,880
 So there's a question asked that I've repeated.

276
00:17:48,880 --> 00:17:52,400
 Some of you have sure heard me say, "How can Nibana be

277
00:17:52,400 --> 00:17:55,280
 happiness when there is no feeling

278
00:17:55,280 --> 00:17:56,280
 in Nibana?"

279
00:17:56,280 --> 00:17:59,810
 And the answer, Sariputta gave an answer and said, "It's

280
00:17:59,810 --> 00:18:01,520
 precisely because there is no

281
00:18:01,520 --> 00:18:04,440
 feeling that Nibana is happiness."

282
00:18:04,440 --> 00:18:11,210
 It's a very difficult truth to understand because we're

283
00:18:11,210 --> 00:18:15,320
 very much addicted to feeling.

284
00:18:15,320 --> 00:18:17,600
 Happy feeling is good.

285
00:18:17,600 --> 00:18:20,240
 Calm feeling is good.

286
00:18:20,240 --> 00:18:21,520
 Painful feeling is bad.

287
00:18:21,520 --> 00:18:25,320
 This is how we think.

288
00:18:25,320 --> 00:18:28,320
 Until we start to see that even happy feeling, calm feeling

289
00:18:28,320 --> 00:18:30,440
, they're impermanent, unsatisfying,

290
00:18:30,440 --> 00:18:35,840
 uncontrollable.

291
00:18:35,840 --> 00:18:37,320
 Only then we start to let them go.

292
00:18:37,320 --> 00:18:39,640
 We start to see this isn't real happiness.

293
00:18:39,640 --> 00:18:41,680
 This is stress.

294
00:18:41,680 --> 00:18:46,100
 My mind is no more at peace than it was before I got this

295
00:18:46,100 --> 00:18:47,200
 feeling.

296
00:18:47,200 --> 00:18:50,040
 I'm no more satisfied than before.

297
00:18:50,040 --> 00:18:53,240
 If anything, I just wanted more.

298
00:18:53,240 --> 00:19:01,720
 If anything, I'm just attached to it more.

299
00:19:01,720 --> 00:19:03,710
 That's when we start to turn and incline and say, "Maybe

300
00:19:03,710 --> 00:19:05,480
 there's something better than

301
00:19:05,480 --> 00:19:07,080
 feeling.

302
00:19:07,080 --> 00:19:13,600
 Maybe there's something better than a risen phenomena."

303
00:19:13,600 --> 00:19:14,600
 There's a natural inclination.

304
00:19:14,600 --> 00:19:18,200
 I mean, there's no brainwashing going on here.

305
00:19:18,200 --> 00:19:19,200
 You'll see it for yourself.

306
00:19:19,200 --> 00:19:21,600
 I don't have to tell you.

307
00:19:21,600 --> 00:19:25,320
 It's irrefutable.

308
00:19:25,320 --> 00:19:31,680
 Once you realize that, then one comes to nirvana.

309
00:19:31,680 --> 00:19:33,680
 Once you've seen it, there's no question.

310
00:19:33,680 --> 00:19:40,760
 Nirvana is the highest happiness.

311
00:19:40,760 --> 00:19:42,880
 That was what I was thinking about tonight.

312
00:19:42,880 --> 00:19:45,520
 We'll take that as our dhamma for tonight.

313
00:19:45,520 --> 00:19:47,080
 Thank you all for coming out.

314
00:19:47,080 --> 00:19:48,560
 Have a good night.

315
00:19:48,560 --> 00:19:49,560
 Thank you.

316
00:19:49,560 --> 00:19:50,560
 Thank you.

317
00:19:50,560 --> 00:19:51,560
 Thank you.

318
00:19:51,560 --> 00:19:52,560
 Thank you.

319
00:19:52,560 --> 00:19:53,560
 Thank you.

320
00:19:53,560 --> 00:19:54,560
 Thank you.

321
00:19:54,560 --> 00:19:55,560
 Thank you.

322
00:19:55,560 --> 00:19:56,560
 Thank you.

323
00:19:56,560 --> 00:19:57,560
 Thank you.

324
00:19:57,560 --> 00:19:58,560
 Thank you.

325
00:19:58,560 --> 00:19:59,560
 Thank you.

326
00:19:59,560 --> 00:20:00,560
 Thank you.

327
00:20:00,560 --> 00:20:01,560
 Thank you.

328
00:20:01,560 --> 00:20:02,560
 Thank you.

329
00:20:02,560 --> 00:20:03,560
 Thank you.

330
00:20:03,560 --> 00:20:04,560
 Thank you.

331
00:20:04,560 --> 00:20:05,560
 Thank you.

332
00:20:05,560 --> 00:20:06,560
 Thank you.

333
00:20:06,560 --> 00:20:07,560
 Thank you.

334
00:20:07,560 --> 00:20:08,560
 Thank you.

335
00:20:08,560 --> 00:20:09,560
 Thank you.

336
00:20:09,560 --> 00:20:10,560
 Thank you.

337
00:20:10,560 --> 00:20:11,560
 Thank you.

338
00:20:11,560 --> 00:20:12,560
 Thank you.

339
00:20:12,560 --> 00:20:13,560
 Thank you.

340
00:20:13,560 --> 00:20:14,560
 Thank you.

341
00:20:14,560 --> 00:20:15,560
 Thank you.

342
00:20:15,560 --> 00:20:16,560
 Thank you.

343
00:20:16,560 --> 00:20:17,560
 Thank you.

344
00:20:17,560 --> 00:20:18,560
 Thank you.

345
00:20:18,560 --> 00:20:19,560
 Thank you.

346
00:20:19,560 --> 00:20:20,560
 Thank you.

347
00:20:20,560 --> 00:20:21,560
 Thank you.

348
00:20:21,560 --> 00:20:22,560
 Thank you.

349
00:20:22,560 --> 00:20:23,560
 Thank you.

350
00:20:23,560 --> 00:20:24,560
 Thank you.

351
00:20:24,560 --> 00:20:25,560
 Thank you.

352
00:20:25,560 --> 00:20:26,560
 Thank you.

353
00:20:26,560 --> 00:20:27,560
 Thank you.

354
00:20:27,560 --> 00:20:28,560
 Thank you.

355
00:20:28,560 --> 00:20:29,560
 Thank you.

356
00:20:29,560 --> 00:20:30,560
 Thank you.

357
00:20:30,560 --> 00:20:31,560
 Thank you.

358
00:20:31,560 --> 00:20:32,560
 Thank you.

359
00:20:32,560 --> 00:20:33,560
 Thank you.

360
00:20:33,560 --> 00:20:34,560
 Thank you.

361
00:20:34,560 --> 00:20:35,560
 Thank you.

362
00:20:35,560 --> 00:20:36,560
 Thank you.

363
00:20:36,560 --> 00:20:37,560
 Thank you.

364
00:20:37,560 --> 00:20:38,560
 Thank you.

365
00:20:38,560 --> 00:20:39,560
 Thank you.

366
00:20:39,560 --> 00:20:40,560
 Thank you.

367
00:20:40,560 --> 00:20:41,560
 Thank you.

368
00:20:41,560 --> 00:20:42,560
 Thank you.

369
00:20:42,560 --> 00:20:43,560
 Thank you.

370
00:20:43,560 --> 00:20:44,560
 Thank you.

371
00:20:44,560 --> 00:20:45,560
 Thank you.

372
00:20:45,560 --> 00:20:46,560
 Thank you.

373
00:20:46,560 --> 00:20:47,560
 Thank you.

374
00:20:47,560 --> 00:20:48,560
 Thank you.

375
00:20:48,560 --> 00:20:49,560
 Thank you.

376
00:20:49,560 --> 00:20:50,560
 Thank you.

377
00:20:50,560 --> 00:20:51,560
 Thank you.

378
00:20:51,560 --> 00:20:52,560
 Thank you.

379
00:20:52,560 --> 00:20:53,560
 Thank you.

380
00:20:53,560 --> 00:20:54,560
 Thank you.

381
00:20:54,560 --> 00:20:55,560
 Thank you.

382
00:20:55,560 --> 00:20:56,560
 Thank you.

383
00:20:56,560 --> 00:20:57,560
 Thank you.

384
00:20:57,560 --> 00:20:58,560
 Thank you.

385
00:20:58,560 --> 00:20:59,560
 Thank you.

386
00:20:59,560 --> 00:21:00,560
 Thank you.

387
00:21:00,560 --> 00:21:01,560
 Thank you.

388
00:21:01,560 --> 00:21:02,560
 Thank you.

389
00:21:02,560 --> 00:21:03,560
 Thank you.

390
00:21:03,560 --> 00:21:04,560
 Thank you.

391
00:21:04,560 --> 00:21:05,560
 Thank you.

392
00:21:05,560 --> 00:21:06,560
 Thank you.

393
00:21:06,560 --> 00:21:07,560
 Thank you.

394
00:21:07,560 --> 00:21:08,560
 Thank you.

395
00:21:08,560 --> 00:21:09,560
 Thank you.

396
00:21:09,560 --> 00:21:10,560
 Thank you.

397
00:21:10,560 --> 00:21:11,560
 Thank you.

398
00:21:11,560 --> 00:21:12,560
 Thank you.

399
00:21:12,560 --> 00:21:13,560
 Thank you.

400
00:21:13,560 --> 00:21:14,560
 Thank you.

401
00:21:14,560 --> 00:21:15,560
 Thank you.

402
00:21:15,560 --> 00:21:16,560
 Thank you.

403
00:21:16,560 --> 00:21:17,560
 Thank you.

404
00:21:17,560 --> 00:21:18,560
 Thank you.

405
00:21:18,560 --> 00:21:19,560
 Thank you.

406
00:21:19,560 --> 00:21:20,560
 Thank you.

407
00:21:20,560 --> 00:21:21,560
 Thank you.

408
00:21:21,560 --> 00:21:22,560
 Thank you.

409
00:21:22,560 --> 00:21:23,560
 Thank you.

410
00:21:23,560 --> 00:21:24,560
 Thank you.

411
00:21:24,560 --> 00:21:25,560
 Thank you.

412
00:21:25,560 --> 00:21:26,560
 Thank you.

413
00:21:26,560 --> 00:21:27,560
 Thank you.

414
00:21:27,560 --> 00:21:28,560
 Thank you.

415
00:21:28,560 --> 00:21:29,560
 Thank you.

416
00:21:29,560 --> 00:21:30,560
 Thank you.

417
00:21:30,560 --> 00:21:31,560
 Thank you.

418
00:21:31,560 --> 00:21:32,560
 Thank you.

419
00:21:32,560 --> 00:21:33,560
 Thank you.

420
00:21:33,560 --> 00:21:34,560
 Thank you.

421
00:21:34,560 --> 00:21:35,560
 Thank you.

422
00:21:35,560 --> 00:21:36,560
 Thank you.

423
00:21:36,560 --> 00:21:37,560
 Thank you.

424
00:21:37,560 --> 00:21:38,560
 Thank you.

425
00:21:38,560 --> 00:21:39,560
 Thank you.

426
00:21:39,560 --> 00:21:40,560
 Thank you.

