1
00:00:00,000 --> 00:00:05,000
 Namat

2
00:00:05,000 --> 00:00:10,000
 Ratanat Dayasa

3
00:00:10,000 --> 00:00:15,000
 Welcome everyone to today's

4
00:00:15,000 --> 00:00:20,000
 online Dhamma talk

5
00:00:20,000 --> 00:00:25,000
 Today I'm recording

6
00:00:25,000 --> 00:00:30,000
 not only live on the internet but also broadcasting into

7
00:00:30,000 --> 00:00:30,000
 our new

8
00:00:30,000 --> 00:00:35,000
 virtual reality

9
00:00:35,000 --> 00:00:40,000
 platform. Virtual reality is

10
00:00:40,000 --> 00:00:45,000
 interesting. At first it seemed like a fairly silly idea

11
00:00:45,000 --> 00:00:50,000
 especially the idea of say sitting in meditation in virtual

12
00:00:50,000 --> 00:00:50,000
 reality

13
00:00:50,000 --> 00:00:55,000
 but

14
00:00:55,000 --> 00:01:00,000
 I think as a means to gather people together

15
00:01:00,000 --> 00:01:05,000
 and give people a vision of some sort of community

16
00:01:05,000 --> 00:01:10,000
 which were often lacking as Buddhists

17
00:01:10,000 --> 00:01:15,000
 in the modern era being scattered across the globe

18
00:01:15,000 --> 00:01:20,000
 I think it has quite a bit of potential and it seems to

19
00:01:20,000 --> 00:01:20,000
 have proved

20
00:01:20,000 --> 00:01:25,000
 itself in my experiences using

21
00:01:25,000 --> 00:01:30,000
 the virtual realities which have been set up

22
00:01:30,000 --> 00:01:35,000
 like second life and so on.

23
00:01:35,000 --> 00:01:39,430
 But the great thing about what we set up now is that we're

24
00:01:39,430 --> 00:01:40,000
 going to make a

25
00:01:40,000 --> 00:01:45,000
 fully and completely Buddhist virtual reality

26
00:01:45,000 --> 00:01:50,000
 and I think this has an even greater potential so that when

27
00:01:50,000 --> 00:01:50,000
 people want to

28
00:01:50,000 --> 00:01:55,000
 connect with a Buddhist community they know exactly where

29
00:01:55,000 --> 00:01:55,000
 to go

30
00:01:55,000 --> 00:02:00,000
 and so I hope that starting in the new year we'll be able

31
00:02:00,000 --> 00:02:00,000
 to have that

32
00:02:00,000 --> 00:02:05,000
 up and running. But for those of you who are not

33
00:02:05,000 --> 00:02:09,100
 on virtual reality and my talks are going to still be

34
00:02:09,100 --> 00:02:10,000
 broadcast over the

35
00:02:10,000 --> 00:02:15,000
 internet on my website and for those of you who aren't able

36
00:02:15,000 --> 00:02:15,000
 to

37
00:02:15,000 --> 00:02:20,000
 listen in there I'm going to record them and having

38
00:02:20,000 --> 00:02:20,000
 recorded them

39
00:02:20,000 --> 00:02:25,000
 you can listen to them at your leisure also downloaded from

40
00:02:25,000 --> 00:02:25,000
 the website.

41
00:02:25,000 --> 00:02:30,000
 So today I thought I would start something a little bit

42
00:02:30,000 --> 00:02:35,000
 different. I've been giving talks and

43
00:02:35,000 --> 00:02:40,000
 doing sutta study courses and even telling

44
00:02:40,000 --> 00:02:45,000
 stories. I thought I would do something a little bit

45
00:02:45,000 --> 00:02:45,000
 similar to

46
00:02:45,000 --> 00:02:50,000
 the sutta study except now we won't be studying sutta

47
00:02:50,000 --> 00:02:55,000
 a discourse we'll be studying verses and these are the

48
00:02:55,000 --> 00:03:00,000
 verses of the Dhammapada. The Dhammapada is a

49
00:03:00,000 --> 00:03:05,000
 classic Buddhist text which contains some of the

50
00:03:05,000 --> 00:03:10,000
 most pithy statements of the Lord Buddha

51
00:03:10,000 --> 00:03:15,000
 those statements which were thought to have profound

52
00:03:15,000 --> 00:03:15,000
 meaning.

53
00:03:15,000 --> 00:03:20,000
 And so I'm going to start today with the first two verses

54
00:03:20,000 --> 00:03:25,000
 of the Dhammapada and the first set of verses in the Dham

55
00:03:25,000 --> 00:03:25,000
mapada are in pairs

56
00:03:25,000 --> 00:03:30,000
 so that's why we have two here today. And these verses

57
00:03:30,000 --> 00:03:35,000
 with the Pali are as follows.

58
00:03:35,000 --> 00:03:40,000
 This is the first verse.

59
00:03:40,000 --> 00:03:45,000
 And it's translated as

60
00:03:45,000 --> 00:03:50,000
 mind is the forerunner of all states

61
00:03:50,000 --> 00:03:55,000
 all Dhammas. Mind is their chief

62
00:03:55,000 --> 00:04:00,000
 and they are mind made by the Lord

63
00:04:00,000 --> 00:04:05,000
 mind is that they have mind as their chief and they are

64
00:04:05,000 --> 00:04:05,000
 mind made.

65
00:04:05,000 --> 00:04:10,000
 If one speaks or acts with a wicked mind

66
00:04:10,000 --> 00:04:15,000
 then suffering follows one just as the wheel follows the ho

67
00:04:15,000 --> 00:04:15,000
of of the ox

68
00:04:15,000 --> 00:04:20,000
 that pulls it. So this is a statement of the Lord Buddha

69
00:04:20,000 --> 00:04:20,000
 that

70
00:04:20,000 --> 00:04:25,000
 the mind is the most important in everything that we do

71
00:04:25,000 --> 00:04:30,000
 in the production of karma. The second verse is almost the

72
00:04:30,000 --> 00:04:30,000
 same

73
00:04:30,000 --> 00:04:35,000
 it just ends

74
00:04:35,000 --> 00:04:40,000
 like a shadow that never leaves

75
00:04:40,000 --> 00:04:45,000
 when one acts or speaks with a pure heart, a bright heart

76
00:04:45,000 --> 00:04:50,000
 and the light of the Lord is the light of the Lord.

77
00:04:50,000 --> 00:04:55,000
 When one acts or speaks with a pure heart, a bright heart

78
00:04:55,000 --> 00:05:00,000
 a bright mind, happiness

79
00:05:00,000 --> 00:05:05,000
 follows one just as a shadow that never leaves.

80
00:05:05,000 --> 00:05:08,960
 So whether it's good or evil these two verses are

81
00:05:08,960 --> 00:05:10,000
 explaining that it's the mind

82
00:05:10,000 --> 00:05:15,000
 that qualifies a action.

83
00:05:15,000 --> 00:05:19,250
 And this was a profound statement. It's profound even today

84
00:05:19,250 --> 00:05:20,000
 but in the Buddha's time it was

85
00:05:20,000 --> 00:05:24,650
 quite profound on a religious level because the doctrine of

86
00:05:24,650 --> 00:05:25,000
 karma of course was

87
00:05:25,000 --> 00:05:30,000
 a hotly debated issue and

88
00:05:30,000 --> 00:05:35,000
 mostly it was debated over which actions of speech

89
00:05:35,000 --> 00:05:40,000
 or of speech or

90
00:05:40,000 --> 00:05:45,000
 body were going to be most potent and were going to have

91
00:05:45,000 --> 00:05:50,000
 an effect, were going to give results. There was no thought

92
00:05:50,000 --> 00:05:55,000
 that somehow it should be the mind that

93
00:05:55,000 --> 00:05:59,300
 was most important. For most people it was simply karma was

94
00:05:59,300 --> 00:06:00,000
 a ritual.

95
00:06:00,000 --> 00:06:05,000
 Karma actually means action. So as I've said before and if

96
00:06:05,000 --> 00:06:05,000
 you want to look at it one way

97
00:06:05,000 --> 00:06:10,000
 the Buddha didn't teach the doctrine of karma

98
00:06:10,000 --> 00:06:15,000
 he taught against it. He thought that it wasn't the actions

99
00:06:15,000 --> 00:06:15,000
 which were most important.

100
00:06:15,000 --> 00:06:20,000
 It was the

101
00:06:20,000 --> 00:06:23,990
 mind behind them and this is what he's saying in these two

102
00:06:23,990 --> 00:06:25,000
 verses.

103
00:06:25,000 --> 00:06:30,000
 And so this radically changed

104
00:06:30,000 --> 00:06:35,000
 how people looked at this idea of karma. People having in

105
00:06:35,000 --> 00:06:35,000
 the Vedas

106
00:06:35,000 --> 00:06:40,000
 and even the Upanishads it was considered to be that

107
00:06:40,000 --> 00:06:40,000
 certain

108
00:06:40,000 --> 00:06:44,560
 actions, yoga for instance, there were certain asanas that

109
00:06:44,560 --> 00:06:45,000
 were powerful and

110
00:06:45,000 --> 00:06:50,000
 so on. Certain mantras that had power to them.

111
00:06:50,000 --> 00:06:55,000
 And there's a funny story that I always tell about how this

112
00:06:55,000 --> 00:06:55,000
 monkey was in

113
00:06:55,000 --> 00:07:00,000
 Singapore giving a Dhamma talk and he was asked

114
00:07:00,000 --> 00:07:05,000
 "Is it enough to chant the name,

115
00:07:05,000 --> 00:07:10,000
 is it enough to chant Namo Amitavada to go to the Pure Land

116
00:07:10,000 --> 00:07:10,000
?

117
00:07:10,000 --> 00:07:14,620
 Is that enough to go to the Pure Land or is that enough to

118
00:07:14,620 --> 00:07:15,000
 be a good Buddhist?"

119
00:07:15,000 --> 00:07:20,000
 And he gave a story first about this mother and her son

120
00:07:20,000 --> 00:07:20,000
 that I've given

121
00:07:20,000 --> 00:07:25,000
 the story before. But then he said there's no text

122
00:07:25,000 --> 00:07:30,000
 in the Mahayana which says that if you chant the Namo Amit

123
00:07:30,000 --> 00:07:30,000
avada

124
00:07:30,000 --> 00:07:35,000
 you're going to be reborn in the Pure Land. There's not one

125
00:07:35,000 --> 00:07:35,000
 text.

126
00:07:35,000 --> 00:07:39,000
 This is what he said. Now I'm not familiar with these texts

127
00:07:39,000 --> 00:07:40,000
 so I'm going to go by his authority.

128
00:07:40,000 --> 00:07:45,000
 He said there may be some people who try to say that this

129
00:07:45,000 --> 00:07:45,000
 is the case but there's no

130
00:07:45,000 --> 00:07:50,000
 core Mahayana text that says this. The Buddha never said

131
00:07:50,000 --> 00:07:50,000
 that.

132
00:07:50,000 --> 00:07:55,000
 He said, and the Buddha said

133
00:07:55,000 --> 00:07:59,000
 chanting, the Buddha never said that if you chant Namo

134
00:07:59,000 --> 00:08:03,330
 Amitavada you're going to be reborn in the Pure Land. What

135
00:08:03,330 --> 00:08:04,000
 he did say is if you chant Namo

136
00:08:04,000 --> 00:08:08,510
 Amitavada with a pure mind you'll be reborn in the Pure

137
00:08:08,510 --> 00:08:09,000
 Land.

138
00:08:09,000 --> 00:08:14,000
 And he said it also goes to

139
00:08:14,000 --> 00:08:19,000
 it also follows from that or it's also true that if you

140
00:08:19,000 --> 00:08:19,000
 chant

141
00:08:19,000 --> 00:08:24,000
 Pola with a pure mind you'll be reborn in the Pure Land.

142
00:08:24,000 --> 00:08:29,000
 This was a hit with the Singaporean audience.

143
00:08:29,000 --> 00:08:34,000
 But it's also very indicative of the sort of

144
00:08:34,000 --> 00:08:39,000
 way that Buddhists look at karma. There's no special words

145
00:08:39,000 --> 00:08:39,000
 that we can

146
00:08:39,000 --> 00:08:44,000
 use or special actions that we can hold on to that are

147
00:08:44,000 --> 00:08:44,000
 going to somehow

148
00:08:44,000 --> 00:08:48,000
 bring us to peace and happiness and freedom from suffering

149
00:08:48,000 --> 00:08:48,000
 since all of these things are

150
00:08:48,000 --> 00:08:54,000
 qualities of the mind. So the stories behind these verses

151
00:08:54,000 --> 00:08:59,000
 are quite good examples as well. The first verse

152
00:08:59,000 --> 00:09:04,000
 about when you do a bad deed it follows you. It's a story

153
00:09:04,000 --> 00:09:04,000
 of a blind

154
00:09:04,000 --> 00:09:09,000
 elder who had become enlightened at the same time, same

155
00:09:09,000 --> 00:09:09,000
 moment

156
00:09:09,000 --> 00:09:14,000
 as going blind. So he was practicing so strenuously and he

157
00:09:14,000 --> 00:09:14,000
 actually needed

158
00:09:14,000 --> 00:09:19,000
 some rest to lie down and rest his eyes. And because he

159
00:09:19,000 --> 00:09:19,000
 didn't rest

160
00:09:19,000 --> 00:09:24,000
 because he didn't lie down he went blind but as he was

161
00:09:24,000 --> 00:09:24,000
 practicing

162
00:09:24,000 --> 00:09:29,000
 he realized that his eyes were dying, his eyes were

163
00:09:29,000 --> 00:09:29,000
 becoming

164
00:09:29,000 --> 00:09:33,460
 destroyed through his practice that he actually was able to

165
00:09:33,460 --> 00:09:34,000
 give up his attachment to

166
00:09:34,000 --> 00:09:38,080
 everything and able to let go and become free from

167
00:09:38,080 --> 00:09:39,000
 suffering.

168
00:09:39,000 --> 00:09:44,000
 So for this reason he was called "Chakubala" - one who

169
00:09:44,000 --> 00:09:44,000
 protects his eyes

170
00:09:44,000 --> 00:09:49,000
 which is quite a funny play on words because the truth is

171
00:09:49,000 --> 00:09:55,000
 he let his eyes become destroyed. But the eye that he

172
00:09:55,000 --> 00:10:01,290
 protected was the eye of Dhamma. So his inner eye, his

173
00:10:01,290 --> 00:10:02,000
 inner understanding,

174
00:10:02,000 --> 00:10:07,000
 his inner vision was protected and he was most

175
00:10:07,000 --> 00:10:13,000
 resolute in his protection of that vision.

176
00:10:13,000 --> 00:10:18,000
 So he was given the name "Chakubala" and after he was

177
00:10:18,000 --> 00:10:18,000
 enlightened one night he was

178
00:10:18,000 --> 00:10:23,000
 doing walking meditation late at night or in the early

179
00:10:23,000 --> 00:10:23,000
 morning

180
00:10:23,000 --> 00:10:27,000
 and it had rained the night before, it had rained all night

181
00:10:27,000 --> 00:10:28,000
 and as the rain was letting up

182
00:10:28,000 --> 00:10:32,850
 there were these insects, those little flying insects, term

183
00:10:32,850 --> 00:10:33,000
ites or something

184
00:10:33,000 --> 00:10:39,740
 that fly around and end up giving up their wings and just

185
00:10:39,740 --> 00:10:41,000
 sort of flailing about on the ground

186
00:10:41,000 --> 00:10:44,400
 in the puddles and so on. And as he was doing walking

187
00:10:44,400 --> 00:10:46,000
 meditation he ended up stepping on

188
00:10:46,000 --> 00:10:50,210
 many of these insects without knowing it. And in the

189
00:10:50,210 --> 00:10:52,000
 morning when the other monks came around

190
00:10:52,000 --> 00:10:57,570
 they asked the monks in the monastery, they said "Who was

191
00:10:57,570 --> 00:10:59,000
 doing walking meditation in this area?"

192
00:10:59,000 --> 00:11:02,700
 And they said it was "Chakubala" and when they found that

193
00:11:02,700 --> 00:11:04,000
 out they were shocked and they said

194
00:11:04,000 --> 00:11:09,000
 "This monk has been committing murder, has been killing

195
00:11:09,000 --> 00:11:10,000
 these living beings."

196
00:11:10,000 --> 00:11:17,000
 And they went to see the Buddha and the Buddha said to them

197
00:11:17,000 --> 00:11:17,000
, he said

198
00:11:17,000 --> 00:11:22,000
 "My son Dakubala is totally innocent of this, he didn't

199
00:11:22,000 --> 00:11:23,000
 know that he was doing,

200
00:11:23,000 --> 00:11:25,460
 he didn't know what was, he didn't know that those insects

201
00:11:25,460 --> 00:11:27,000
 were dying, he had no clue.

202
00:11:27,000 --> 00:11:31,170
 Had he known he wouldn't have done it." And then he gave

203
00:11:31,170 --> 00:11:35,000
 this verse that mind is what makes something

204
00:11:35,000 --> 00:11:38,320
 an evil act. If one acts or speaks with an evil mind this

205
00:11:38,320 --> 00:11:40,000
 is when suffering follows,

206
00:11:40,000 --> 00:11:44,510
 this is when it's considered a bad karma. And the second

207
00:11:44,510 --> 00:11:46,000
 verse is the opposite.

208
00:11:46,000 --> 00:11:51,630
 So the first verse indicates that no matter how evil a deed

209
00:11:51,630 --> 00:11:53,000
 might appear,

210
00:11:53,000 --> 00:11:58,450
 if it doesn't have a mind, if it doesn't come from one's

211
00:11:58,450 --> 00:11:59,000
 intentions,

212
00:11:59,000 --> 00:12:05,000
 if it isn't intended then it has no fruit, it can't bear

213
00:12:05,000 --> 00:12:09,000
 any fruit in the person's mind

214
00:12:09,000 --> 00:12:13,000
 and so therefore it's not considered a bad deed.

215
00:12:13,000 --> 00:12:19,910
 And this also is something that we can use to explain how

216
00:12:19,910 --> 00:12:26,000
 we can approach the idea of killing or murder in Buddhism.

217
00:12:26,000 --> 00:12:29,580
 So when we understand that, when we know that there is a

218
00:12:29,580 --> 00:12:31,000
 living being present

219
00:12:31,000 --> 00:12:35,430
 and when we actively make effort to try to end its life and

220
00:12:35,430 --> 00:12:38,000
 then we succeed,

221
00:12:38,000 --> 00:12:40,260
 this is when it's considered murder. But when suppose we're

222
00:12:40,260 --> 00:12:41,000
 walking down the street

223
00:12:41,000 --> 00:12:44,000
 and we step on an ant, this is what many people ask.

224
00:12:44,000 --> 00:12:46,730
 If you didn't know that the ant was there then it's not

225
00:12:46,730 --> 00:12:48,000
 considered murder.

226
00:12:48,000 --> 00:12:51,420
 If you had no clue that it died then you could die, you can

227
00:12:51,420 --> 00:12:52,000
 pass away,

228
00:12:52,000 --> 00:12:54,570
 you could live your life and be totally at ease with

229
00:12:54,570 --> 00:12:55,000
 yourself

230
00:12:55,000 --> 00:12:59,000
 and it will never bring any unwholesome consequence to you

231
00:12:59,000 --> 00:13:00,000
 because you hadn't the clue,

232
00:13:00,000 --> 00:13:03,510
 you hadn't the intention. You could have been totally

233
00:13:03,510 --> 00:13:07,000
 mindful as this monk was walking back and forth

234
00:13:07,000 --> 00:13:11,080
 and still the being has to die because of its own karma,

235
00:13:11,080 --> 00:13:15,000
 because of the nature of the universe or its way in life.

236
00:13:15,000 --> 00:13:21,760
 And for instance for things like vegetarianism when people

237
00:13:21,760 --> 00:13:23,000
 ask why Buddhists can eat meat

238
00:13:23,000 --> 00:13:28,210
 and in Tibet they eat meat, in the Theravada countries they

239
00:13:28,210 --> 00:13:31,000
 eat meat, why they can do this.

240
00:13:31,000 --> 00:13:35,810
 Well this goes back to our understanding of what is a good

241
00:13:35,810 --> 00:13:37,000
 and bad karma,

242
00:13:37,000 --> 00:13:41,650
 what is an active karma and what is an act which is karm

243
00:13:41,650 --> 00:13:43,000
ically inactive.

244
00:13:43,000 --> 00:13:46,000
 And again it has to do with our intention.

245
00:13:46,000 --> 00:13:49,730
 When you eat something that's already dead there's no

246
00:13:49,730 --> 00:13:51,000
 intention to kill,

247
00:13:51,000 --> 00:13:54,000
 there's no intention to hurt other living beings.

248
00:13:54,000 --> 00:14:01,130
 And this is kind of a signature of Buddhism across the

249
00:14:01,130 --> 00:14:04,000
 board I think is this idea that

250
00:14:04,000 --> 00:14:09,700
 Buddhist practice is not an intellectual practice, you can

251
00:14:09,700 --> 00:14:13,150
't intellectualize it and think about it and expect to get

252
00:14:13,150 --> 00:14:14,000
 some result.

253
00:14:14,000 --> 00:14:19,170
 You can't say I'm going to be moral by intellectualizing or

254
00:14:19,170 --> 00:14:22,750
 I'm going to develop morality through on an intellectual

255
00:14:22,750 --> 00:14:23,000
 basis,

256
00:14:23,000 --> 00:14:25,000
 it has to come from the heart.

257
00:14:25,000 --> 00:14:29,000
 It comes from an understanding that this is a living being

258
00:14:29,000 --> 00:14:38,650
 and this heartfelt desire to see that being free from

259
00:14:38,650 --> 00:14:39,000
 suffering

260
00:14:39,000 --> 00:14:44,650
 and to wish for them to not fall into the kind of suffering

261
00:14:44,650 --> 00:14:48,000
 that comes about from murder.

262
00:14:48,000 --> 00:14:53,470
 When you eat something that's already dead, this certainly

263
00:14:53,470 --> 00:14:54,000
 isn't the case,

264
00:14:54,000 --> 00:14:57,830
 the mind can be completely free from guilt, free from worry

265
00:14:57,830 --> 00:15:01,000
, free from anger, free from greed,

266
00:15:01,000 --> 00:15:08,000
 in the same way as if one is to eat other inanimate food

267
00:15:08,000 --> 00:15:09,000
 stuffs.

268
00:15:09,000 --> 00:15:15,910
 For instance, eating pesticide-ridden carrots or vegetables

269
00:15:15,910 --> 00:15:18,000
 or fruits or using all sorts of things

270
00:15:18,000 --> 00:15:24,450
 which may have on an intellectual level may have ethically

271
00:15:24,450 --> 00:15:27,000
 questionable origins,

272
00:15:27,000 --> 00:15:30,100
 using all sorts of things that if you were to intellectual

273
00:15:30,100 --> 00:15:32,000
ize it and look on a societal level,

274
00:15:32,000 --> 00:15:35,000
 these things may not be ethically sound to use.

275
00:15:35,000 --> 00:15:38,900
 In fact, many of the things we use are ethically

276
00:15:38,900 --> 00:15:40,000
 questionable.

277
00:15:40,000 --> 00:15:43,390
 Coffee, for instance, simply drinking coffee is an eth

278
00:15:43,390 --> 00:15:47,000
ically questionable act on an intellectual level,

279
00:15:47,000 --> 00:15:50,000
 but Buddhist morality doesn't cover this.

280
00:15:50,000 --> 00:15:52,000
 Buddhist morality isn't concerned with this.

281
00:15:52,000 --> 00:15:56,020
 We're concerned only with the state of one's heart when one

282
00:15:56,020 --> 00:15:57,000
 does something.

283
00:15:57,000 --> 00:16:01,440
 And if one acts or speaks with a pure heart, as we're going

284
00:16:01,440 --> 00:16:04,000
 to see in the next verse,

285
00:16:04,000 --> 00:16:08,000
 then no matter how small it is, it brings happiness.

286
00:16:08,000 --> 00:16:13,250
 No matter how small the physical action, simply the good

287
00:16:13,250 --> 00:16:19,000
 intention in the mind nullifies any physical act.

288
00:16:19,000 --> 00:16:22,360
 So people who are superstitious and think that doing this

289
00:16:22,360 --> 00:16:24,000
 or that act, simply by the ritual act,

290
00:16:24,000 --> 00:16:28,590
 it's going to bring bad luck, for instance, walking under a

291
00:16:28,590 --> 00:16:30,000
 ladder or so on.

292
00:16:30,000 --> 00:16:34,680
 This the Lord Buddha was totally against and totally denied

293
00:16:34,680 --> 00:16:35,000
.

294
00:16:35,000 --> 00:16:37,000
 So what's the second verse about?

295
00:16:37,000 --> 00:16:42,170
 The second verse, the story behind it is this boy who was

296
00:16:42,170 --> 00:16:46,000
 very ill and his father wouldn't call a doctor in

297
00:16:46,000 --> 00:16:49,480
 because his father was a millionaire who was very, very

298
00:16:49,480 --> 00:16:50,000
 stingy.

299
00:16:50,000 --> 00:16:52,620
 And so instead of calling in a doctor, he went to the

300
00:16:52,620 --> 00:16:54,000
 doctor and asked him,

301
00:16:54,000 --> 00:16:57,360
 asked the doctors what sort of medicine they would

302
00:16:57,360 --> 00:17:00,000
 prescribe for such and such an illness.

303
00:17:00,000 --> 00:17:02,450
 And when the doctor said this or that medicine, so he went

304
00:17:02,450 --> 00:17:05,000
 and found the medicine himself and tried to administer it

305
00:17:05,000 --> 00:17:07,000
 to his son.

306
00:17:07,000 --> 00:17:11,350
 As a result of his bumbling efforts, the son got worse to

307
00:17:11,350 --> 00:17:14,000
 the point where the son was about to die,

308
00:17:14,000 --> 00:17:17,350
 at which point the father realized that he had to call a

309
00:17:17,350 --> 00:17:22,000
 doctor, but it was too late and the boy was on his deathbed

310
00:17:22,000 --> 00:17:22,000
.

311
00:17:22,000 --> 00:17:28,000
 And so the father, seeing that the boy was on his deathbed,

312
00:17:28,000 --> 00:17:30,000
 what did he do rather than console him

313
00:17:30,000 --> 00:17:32,000
 and keep him in the quiet indoors of the house?

314
00:17:32,000 --> 00:17:38,080
 He thought to himself, "Oh, dear, now if people come to see

315
00:17:38,080 --> 00:17:43,590
 him, to say goodbye to him and to try to console him and

316
00:17:43,590 --> 00:17:44,000
 comfort him,

317
00:17:44,000 --> 00:17:46,500
 they're going to come into that mansion and see all of my

318
00:17:46,500 --> 00:17:50,000
 treasure and then they're going to be angry and jealous

319
00:17:50,000 --> 00:17:53,000
 and want a share of my treasure."

320
00:17:53,000 --> 00:17:58,320
 So he took the boy and had his bed moved outside of the

321
00:17:58,320 --> 00:18:00,000
 house so that if people came to visit,

322
00:18:00,000 --> 00:18:04,070
 they wouldn't be able to come in the house and see all of

323
00:18:04,070 --> 00:18:07,000
 his treasures, all of his wealth.

324
00:18:07,000 --> 00:18:12,860
 And so this was kind of a terrible thing for a father to do

325
00:18:12,860 --> 00:18:17,000
 to his son, but it ended up being a great luck for the boy,

326
00:18:17,000 --> 00:18:20,570
 because on that morning the Lord Buddha was going on alms

327
00:18:20,570 --> 00:18:25,080
 round and he went on alms round past the millionaire's

328
00:18:25,080 --> 00:18:26,000
 mansion.

329
00:18:26,000 --> 00:18:29,750
 And the boy was dying, lying on his deathbed and he was

330
00:18:29,750 --> 00:18:34,000
 looking in the direction that the Buddha was walking.

331
00:18:34,000 --> 00:18:36,770
 And the Buddha stopped and looked at him and when the boy

332
00:18:36,770 --> 00:18:42,210
 looked up at the Buddha and was just so full of awe and

333
00:18:42,210 --> 00:18:43,000
 inspiration,

334
00:18:43,000 --> 00:18:49,220
 simply seeing the calm and peaceful countenance of the Lord

335
00:18:49,220 --> 00:18:50,000
 Buddha,

336
00:18:50,000 --> 00:18:54,930
 he thought to himself, "Wow, I've never had the chance to

337
00:18:54,930 --> 00:18:58,000
 see such a beautiful, such a wonderful person."

338
00:18:58,000 --> 00:19:02,220
 And so he tried to lift up his hands to his chest and pay

339
00:19:02,220 --> 00:19:05,000
 respect to the Buddha, but even that he couldn't do.

340
00:19:05,000 --> 00:19:09,150
 He was so weak and so close to death that all he could do

341
00:19:09,150 --> 00:19:12,000
 was in his mind make this intention,

342
00:19:12,000 --> 00:19:15,160
 trying to lift his hands up to his chest and pay respect to

343
00:19:15,160 --> 00:19:16,000
 the Buddha,

344
00:19:16,000 --> 00:19:19,230
 but his mind was so full of happiness and joy just at

345
00:19:19,230 --> 00:19:26,230
 seeing such a beautiful, such a perfect, such an advanced,

346
00:19:26,230 --> 00:19:29,000
 spiritually enlightened being

347
00:19:29,000 --> 00:19:34,600
 that when he died, he was born in heaven. He was born as an

348
00:19:34,600 --> 00:19:35,000
 angel.

349
00:19:35,000 --> 00:19:40,780
 And then the story goes that he came down and talked with

350
00:19:40,780 --> 00:19:43,000
 his father and talked with the Buddha

351
00:19:43,000 --> 00:19:53,100
 and showed that just by making this determination in his

352
00:19:53,100 --> 00:19:56,000
 mind and creating this state of clarity,

353
00:19:56,000 --> 00:19:59,500
 this state of peace, this state of happiness and joy in his

354
00:19:59,500 --> 00:20:04,000
 mind, he was able to be reborn in the heavenly world.

355
00:20:04,000 --> 00:20:06,600
 And so the monks asked, "How is this possible? How is it

356
00:20:06,600 --> 00:20:10,000
 possible that by, you know, without doing anything,

357
00:20:10,000 --> 00:20:14,830
 simply by making this determination in his mind, by wanting

358
00:20:14,830 --> 00:20:16,000
 to pay respect,

359
00:20:16,000 --> 00:20:20,230
 by wanting to do something to show his admiration for the

360
00:20:20,230 --> 00:20:24,000
 Buddha, he was born in heaven."

361
00:20:24,000 --> 00:20:30,030
 And this is when the Lord Buddha gave this one, this verse,

362
00:20:30,030 --> 00:20:32,000
 which is,

363
00:20:32,000 --> 00:20:36,000
 "Manasa ji pa sannena pa sati wa karo ti wa"

364
00:20:36,000 --> 00:20:40,050
 When one acts with a pure mind or speaks with a pure mind,

365
00:20:40,050 --> 00:20:48,000
 happiness follows like a shadow that never leaves.

366
00:20:48,000 --> 00:20:54,070
 So what we're going to see throughout all of these verses,

367
00:20:54,070 --> 00:20:55,660
 and what we should see throughout all of the Buddha's

368
00:20:55,660 --> 00:20:56,000
 teaching,

369
00:20:56,000 --> 00:21:01,860
 is that the idea of purifying one's mind holds predominance

370
00:21:01,860 --> 00:21:04,000
 over everything else.

371
00:21:04,000 --> 00:21:06,980
 That we can do or we can say good deeds. We can try to help

372
00:21:06,980 --> 00:21:08,000
 other people.

373
00:21:08,000 --> 00:21:13,280
 We can build ourselves up or make ourselves look like this

374
00:21:13,280 --> 00:21:16,000
 or that, or make ourselves out to be this or that,

375
00:21:16,000 --> 00:21:20,000
 or do whatever we want externally.

376
00:21:20,000 --> 00:21:30,310
 And it won't nearly have a fraction of the effect of simply

377
00:21:30,310 --> 00:21:33,000
 sitting down, closing one's eyes,

378
00:21:33,000 --> 00:21:36,990
 and trying to look and see and understand one's own mind

379
00:21:36,990 --> 00:21:41,000
 and the way one's heart works,

380
00:21:41,000 --> 00:21:45,000
 and the good and the bad things that exist in our minds.

381
00:21:45,000 --> 00:21:49,670
 Trying to develop our minds to keep our minds from straying

382
00:21:49,670 --> 00:21:54,500
 off into worry and doubt and anger and fear and craving and

383
00:21:54,500 --> 00:21:56,000
 addiction

384
00:21:56,000 --> 00:22:00,720
 and ego and all of these unwholesome, unpleasant,

385
00:22:00,720 --> 00:22:04,000
 undesirable mind states.

386
00:22:04,000 --> 00:22:08,180
 And trying to develop in our mind states of peace and

387
00:22:08,180 --> 00:22:11,000
 tranquility, states of clarity.

388
00:22:11,000 --> 00:22:15,190
 And trying to come to understand how the mind works to give

389
00:22:15,190 --> 00:22:18,000
 rise to these unwholesome states.

390
00:22:18,000 --> 00:22:22,210
 What is it that we're not seeing clearly? What is it that

391
00:22:22,210 --> 00:22:27,000
 is going on to create these unwholesome mind states?

392
00:22:27,000 --> 00:22:31,430
 Coming to look at the phenomena that arise, whether it be

393
00:22:31,430 --> 00:22:35,000
 things that we see, things that we hear,

394
00:22:35,000 --> 00:22:39,520
 whatever phenomena arise at the six senses, coming to see

395
00:22:39,520 --> 00:22:43,470
 them clearly so that they don't give rise to these unwholes

396
00:22:43,470 --> 00:22:45,000
ome mind states.

397
00:22:45,000 --> 00:22:49,120
 So meditation for this reason has the greatest predominance

398
00:22:49,120 --> 00:22:50,000
 in Buddhism,

399
00:22:50,000 --> 00:22:54,550
 to sitting down, closing our eyes, or during any action we

400
00:22:54,550 --> 00:22:56,000
 do during the day.

401
00:22:56,000 --> 00:23:01,000
 Meditation doesn't have to be just a sitting exercise.

402
00:23:01,000 --> 00:23:06,980
 While we're focusing on the mind, we can use the body as

403
00:23:06,980 --> 00:23:11,420
 our intermediary, because the mind is what's doing the

404
00:23:11,420 --> 00:23:12,000
 looking.

405
00:23:12,000 --> 00:23:16,740
 So if the mind starts to focus on the body, it becomes

406
00:23:16,740 --> 00:23:20,850
 clear, the mind's relationship with the objects becomes

407
00:23:20,850 --> 00:23:22,000
 clear by itself.

408
00:23:22,000 --> 00:23:26,930
 We simply make effort in the mind to focus on the body and

409
00:23:26,930 --> 00:23:30,000
 then we can see how the mind works.

410
00:23:30,000 --> 00:23:34,280
 When we try to focus on the body, then we see the mind

411
00:23:34,280 --> 00:23:36,000
 wandering away.

412
00:23:36,000 --> 00:23:41,510
 We try to keep the mind straight, the mind starts to get

413
00:23:41,510 --> 00:23:44,000
 all bent out of shape.

414
00:23:44,000 --> 00:23:47,810
 When we try to keep the mind in the present moment, it goes

415
00:23:47,810 --> 00:23:53,000
 back to the past or goes ahead to the future.

416
00:23:53,000 --> 00:23:57,790
 When we come to see how our mind is, we come to see all of

417
00:23:57,790 --> 00:24:02,000
 the anger, how our mind becomes angry at certain things

418
00:24:02,000 --> 00:24:03,000
 that we don't like.

419
00:24:03,000 --> 00:24:05,000
 We're able to see the arising of the anger.

420
00:24:05,000 --> 00:24:10,640
 We're able to watch the state or the experience that gives

421
00:24:10,640 --> 00:24:13,000
 rise to the emotion.

422
00:24:13,000 --> 00:24:17,810
 When we have wanting or addiction, we're able to see the

423
00:24:17,810 --> 00:24:21,000
 experience that gives rise to it.

424
00:24:21,000 --> 00:24:23,400
 When we see things as they are, we see that they're coming

425
00:24:23,400 --> 00:24:24,000
 and going.

426
00:24:24,000 --> 00:24:27,000
 We don't give rise to this liking or this disliking.

427
00:24:27,000 --> 00:24:30,610
 We look at the experience of seeing or hearing or smelling

428
00:24:30,610 --> 00:24:34,530
 or tasting or feeling of thinking and we see it for what it

429
00:24:34,530 --> 00:24:35,000
 is.

430
00:24:35,000 --> 00:24:40,020
 When the mind becomes pure, everything we do with a pure

431
00:24:40,020 --> 00:24:44,000
 mind can be considered to be a good karma.

432
00:24:44,000 --> 00:24:48,120
 Everything we do with an impure mind, no matter how good we

433
00:24:48,120 --> 00:24:50,000
 might think it to be.

434
00:24:50,000 --> 00:24:53,240
 If the mind is impure at the moment that we're doing it, it

435
00:24:53,240 --> 00:24:58,370
's considered to be a bad karma and it will bring unwholes

436
00:24:58,370 --> 00:25:01,000
ome, unpleasant results.

437
00:25:01,000 --> 00:25:05,000
 This is something that we see often when we do good deeds.

438
00:25:05,000 --> 00:25:07,000
 We try to help people.

439
00:25:07,000 --> 00:25:11,000
 You can see it often either in yourself or in other people.

440
00:25:11,000 --> 00:25:13,370
 We're trying to do these good deeds and we're setting out

441
00:25:13,370 --> 00:25:14,000
 to...

442
00:25:14,000 --> 00:25:18,910
 We have this intention to help people, whether it be to

443
00:25:18,910 --> 00:25:24,520
 give to charity or set up a charity or even to teach and to

444
00:25:24,520 --> 00:25:26,000
 spread Buddhism.

445
00:25:26,000 --> 00:25:31,390
 Then we find ourselves getting angry or getting conceited

446
00:25:31,390 --> 00:25:36,560
 or getting even greedy or attached to gain or fame or so on

447
00:25:36,560 --> 00:25:37,000
.

448
00:25:37,000 --> 00:25:40,500
 It turns out to be a very unwholesome act or at least a

449
00:25:40,500 --> 00:25:44,720
 very mixed act where there is some wholesomeness and there

450
00:25:44,720 --> 00:25:48,000
's also a great deal of unwholesomeness.

451
00:25:48,000 --> 00:25:53,010
 As I've said before, we can look at ourselves as like a

452
00:25:53,010 --> 00:26:00,860
 spring, a mountain spring flowing down to the rivers and

453
00:26:00,860 --> 00:26:03,000
 valleys below.

454
00:26:03,000 --> 00:26:09,320
 If the spring is impure, if it's impure at the source, then

455
00:26:09,320 --> 00:26:12,670
 no matter how strong it is or no matter how far the water

456
00:26:12,670 --> 00:26:16,720
 goes, even all the way to reach the ocean, it's going to be

457
00:26:16,720 --> 00:26:20,000
 impure all the way down the way.

458
00:26:20,000 --> 00:26:23,160
 Whatever animals come to try to drink from it, they won't

459
00:26:23,160 --> 00:26:26,150
 be able to drink, they won't be able to get any nourishment

460
00:26:26,150 --> 00:26:31,000
, any refreshment from it because of its impurity.

461
00:26:31,000 --> 00:26:34,000
 They will become sick and suffer as a result.

462
00:26:34,000 --> 00:26:38,440
 The same goes for all of our actions. If our actions are

463
00:26:38,440 --> 00:26:41,380
 impure in the heart and we say to ourselves, "I'm going to

464
00:26:41,380 --> 00:26:43,620
 go out and do this, I'm going to go out and do that, help

465
00:26:43,620 --> 00:26:45,000
 people in this way or that way,"

466
00:26:45,000 --> 00:26:50,150
 we find that we only end up creating more suffering because

467
00:26:50,150 --> 00:26:52,000
 our minds are impure.

468
00:26:52,000 --> 00:26:55,770
 Because in our minds we still have anger, we still have

469
00:26:55,770 --> 00:26:59,000
 greed, we still have resentment and so on.

470
00:26:59,000 --> 00:27:02,040
 But if we work hard to purify our minds, to get rid of

471
00:27:02,040 --> 00:27:06,860
 these unwholesome states, to come to see clearly about

472
00:27:06,860 --> 00:27:09,000
 things as they are,

473
00:27:09,000 --> 00:27:14,070
 then no matter how weak or how small the spring is, any

474
00:27:14,070 --> 00:27:19,730
 animal that drinks therefrom can gain refreshment, can gain

475
00:27:19,730 --> 00:27:23,830
 satisfaction because of the purity of the water, because of

476
00:27:23,830 --> 00:27:26,000
 the purity of the source.

477
00:27:26,000 --> 00:27:29,320
 And so the same with our actions. When our minds are pure,

478
00:27:29,320 --> 00:27:32,920
 everything that we do, everything that we say, it doesn't

479
00:27:32,920 --> 00:27:36,090
 really matter what we do or what we say or how we try to

480
00:27:36,090 --> 00:27:37,000
 help people.

481
00:27:37,000 --> 00:27:41,920
 Simply our being present has an effect on other people and

482
00:27:41,920 --> 00:27:45,510
 they're able to take us as an example, they're able to

483
00:27:45,510 --> 00:27:49,000
 learn from us, they're able to gain from us.

484
00:27:49,000 --> 00:27:52,810
 People want to be around us, want to associate and be

485
00:27:52,810 --> 00:27:55,000
 friends and learn from us.

486
00:27:55,000 --> 00:28:00,980
 The more we are able to purify our minds, people want to

487
00:28:00,980 --> 00:28:06,000
 associate with us and gain benefit from us.

488
00:28:06,000 --> 00:28:07,730
 So this is a very core teaching of the Lord Buddha and this

489
00:28:07,730 --> 00:28:13,770
 is the very beginning of the Dhamma Bhada, the teaching on

490
00:28:13,770 --> 00:28:18,710
 the mind, the importance of the mind and how the mind comes

491
00:28:18,710 --> 00:28:21,740
 first, how everything comes from the mind, whether it be

492
00:28:21,740 --> 00:28:25,000
 good or bad deeds, whether it be good things or bad things,

493
00:28:25,000 --> 00:28:27,000
 it all comes from the mind.

494
00:28:27,000 --> 00:28:30,100
 So that's the Dhamma that I would like to give on this

495
00:28:30,100 --> 00:28:34,570
 occasion. That's the talk for today. This is our first talk

496
00:28:34,570 --> 00:28:39,130
 broadcasting in Buddhaverse. Here I am sitting in the amph

497
00:28:39,130 --> 00:28:42,000
itheatre with two listeners.

498
00:28:42,000 --> 00:28:48,660
 So we're going to take a snapshot here. We can post this on

499
00:28:48,660 --> 00:28:50,000
 the internet.

