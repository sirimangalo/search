1
00:00:00,000 --> 00:00:05,000
 other useful meditation practices. You said in your Ask a M

2
00:00:05,000 --> 00:00:07,360
ok series, "For people caught up in lust,

3
00:00:07,360 --> 00:00:10,620
 it is good for them to contemplate the unpleasant aspects

4
00:00:10,620 --> 00:00:13,920
 of the body. What is the proper way to do

5
00:00:13,920 --> 00:00:18,440
 that?" I did mention this, and I think I mentioned as well

6
00:00:18,440 --> 00:00:21,360
 that for people caught up in anger,

7
00:00:21,360 --> 00:00:24,770
 it is good to practice friendliness, metta. Both belong to

8
00:00:24,770 --> 00:00:26,960
 a set of four meditations

9
00:00:26,960 --> 00:00:31,990
 called the chatur arakha, kamathana. Arakha means God, so

10
00:00:31,990 --> 00:00:34,240
 these four meditations are meant to guard

11
00:00:34,240 --> 00:00:37,320
 the person's mind or they are something that protects you

12
00:00:37,320 --> 00:00:38,560
 during the time that you are

13
00:00:38,560 --> 00:00:42,300
 practicing vipassana meditation to see clearly. They are a

14
00:00:42,300 --> 00:00:44,480
 sort of protection that will help keep

15
00:00:44,480 --> 00:00:47,570
 the mind in a condition where it is able to see clearly,

16
00:00:47,570 --> 00:00:49,680
 stopping it from falling into great

17
00:00:49,680 --> 00:00:54,200
 states of lust, anger and so on. The four kamathana med

18
00:00:54,200 --> 00:00:57,600
itations are mindfulness of the buddha,

19
00:00:57,600 --> 00:01:01,200
 mindfulness of the absence of beauty in the body, friend

20
00:01:01,200 --> 00:01:03,600
liness and death. Each has different

21
00:01:03,600 --> 00:01:06,840
 qualities and although none of them are necessary to attain

22
00:01:06,840 --> 00:01:09,200
 enlightenment, they can be of practical

23
00:01:09,200 --> 00:01:12,590
 value and so are recommended as generally supportive

24
00:01:12,590 --> 00:01:15,120
 meditation practices as a complement

25
00:01:15,120 --> 00:01:19,690
 to the practice of vipassana meditation. Mindfulness of the

26
00:01:19,690 --> 00:01:21,360
 buddha is most practical

27
00:01:21,360 --> 00:01:24,590
 for Buddhists, people who consider the buddha and his

28
00:01:24,590 --> 00:01:27,120
 teachings as their guide, as a means of

29
00:01:27,120 --> 00:01:30,610
 bolstering our courage to undertake the difficult path

30
00:01:30,610 --> 00:01:33,440
 towards purity and enlightenment. We practice

31
00:01:33,440 --> 00:01:37,130
 recollecting the buddha as an example to us, someone who

32
00:01:37,130 --> 00:01:39,280
 was the perfect teacher, free from

33
00:01:39,280 --> 00:01:43,200
 defilement and delusion, perfect in knowledge and conduct.

34
00:01:43,200 --> 00:01:45,520
 We reflect on the characteristics of the

35
00:01:45,520 --> 00:01:49,750
 buddha, for instance that he had great wisdom, we hear his

36
00:01:49,750 --> 00:01:52,880
 teachings and gain great faith reaffirming

37
00:01:52,880 --> 00:01:56,930
 in our minds that yes indeed he is one who had great wisdom

38
00:01:56,930 --> 00:01:59,360
 and so we should follow his teachings

39
00:01:59,360 --> 00:02:03,480
 because they will certainly lead to the result he proclaims

40
00:02:03,480 --> 00:02:05,760
. We reflect on the great purity of the

41
00:02:05,760 --> 00:02:09,630
 buddha, both he and his teachings are incredibly pure, free

42
00:02:09,630 --> 00:02:12,800
 of dogma or superstition. The buddha

43
00:02:12,800 --> 00:02:17,010
 was pure in that he simply gave something to the world that

44
00:02:17,010 --> 00:02:19,440
 was of use to all beings as opposed to

45
00:02:19,440 --> 00:02:23,220
 looking for students or trying to become famous. We reflect

46
00:02:23,220 --> 00:02:25,600
 on his great compassion, deciding to

47
00:02:25,600 --> 00:02:28,760
 teach for the whole of his enlightened life. He could have

48
00:02:28,760 --> 00:02:30,640
 sat in the forest and meditated and

49
00:02:30,640 --> 00:02:34,540
 passed away undisturbed but when he was asked to teach he

50
00:02:34,540 --> 00:02:36,720
 gave up his own peaceful abiding to

51
00:02:36,720 --> 00:02:40,350
 bring peace to others. There are many qualities of the budd

52
00:02:40,350 --> 00:02:42,320
ha that we recite and reflect upon

53
00:02:42,320 --> 00:02:45,350
 and although this is obviously something that is most

54
00:02:45,350 --> 00:02:47,760
 useful for the buddhist, I would encourage

55
00:02:47,760 --> 00:02:51,340
 everyone to study about the qualities of the buddha. One

56
00:02:51,340 --> 00:02:53,440
 simple way to do this is to recite

57
00:02:53,440 --> 00:02:57,030
 the Pali verse recollecting his virtues and consider each

58
00:02:57,030 --> 00:02:59,120
 one individually. So we say

59
00:02:59,760 --> 00:03:04,130
 vidyā-charaṇa-sampanu which means endow with knowledge

60
00:03:04,130 --> 00:03:06,560
 and conduct and reflect on the buddhist's

61
00:03:06,560 --> 00:03:10,540
 greatness of knowledge and conduct and so on. The second

62
00:03:10,540 --> 00:03:13,360
 meditation is the one you asked about,

63
00:03:13,360 --> 00:03:16,940
 mindfulness of the unpleasant or literally not beautiful

64
00:03:16,940 --> 00:03:19,040
 nature of the body. The view that the

65
00:03:19,040 --> 00:03:21,920
 body is something beautiful is wrong. There is nothing

66
00:03:21,920 --> 00:03:24,560
 intrinsically beautiful about the body.

67
00:03:24,560 --> 00:03:27,920
 It could be argued that there is nothing intrinsically ugly

68
00:03:27,920 --> 00:03:29,200
 about the body either

69
00:03:29,200 --> 00:03:31,760
 but in comparison to the perception of the body as

70
00:03:31,760 --> 00:03:34,400
 beautiful is generally due to not paying close

71
00:03:34,400 --> 00:03:38,090
 attention to reality whereas the perception of the body as

72
00:03:38,090 --> 00:03:40,400
 ugly is generally as a result of paying

73
00:03:40,400 --> 00:03:44,690
 attention. If you compare the body to gold or diamonds or a

74
00:03:44,690 --> 00:03:46,960
 flower the human body isn't very

75
00:03:46,960 --> 00:03:50,500
 high on any objective scale of beauty. This is made clear

76
00:03:50,500 --> 00:03:52,960
 by the practice of reviewing the parts of

77
00:03:52,960 --> 00:03:56,810
 the body. Starting with the hair on your head its nature is

78
00:03:56,810 --> 00:03:58,960
 like grass that has been planted

79
00:03:58,960 --> 00:04:02,350
 in this skull and if you do not wash it smells and looks

80
00:04:02,350 --> 00:04:05,120
 repulsive. Focusing your attention on hair

81
00:04:05,120 --> 00:04:08,560
 as a meditation object you quickly come to see that hair is

82
00:04:08,560 --> 00:04:11,040
 not the attractive thing we ordinarily

83
00:04:11,040 --> 00:04:14,760
 perceive it as. We often perceive a person's hair as

84
00:04:14,760 --> 00:04:17,840
 beautiful but if a strand of hair falls into

85
00:04:17,840 --> 00:04:21,950
 our food we might become nauseous. When hair is cut off it

86
00:04:21,950 --> 00:04:24,800
 looks unappealing in clumps on the floor.

87
00:04:24,800 --> 00:04:27,500
 People who keep their hair after cutting it are often met

88
00:04:27,500 --> 00:04:30,160
 with revulsion. We go through all the

89
00:04:30,160 --> 00:04:35,010
 various parts of the body as a reflection. Head hair, body

90
00:04:35,010 --> 00:04:38,800
 hair, nails, teeth, skin, flesh, blood,

91
00:04:38,800 --> 00:04:44,740
 bones, bone marrow, feces, urine, liver, spleen, heart and

92
00:04:44,740 --> 00:04:46,960
 so on. Traditionally one would learn

93
00:04:46,960 --> 00:04:52,710
 and recite the Pali names for each Kesa, Loma, Naka, Danta,

94
00:04:52,710 --> 00:04:57,760
 Tako, Mamsam, Haru, Ati, Atimindjam etc.

95
00:04:57,760 --> 00:05:01,800
 Through all 32 parts of the body then break them up into

96
00:05:01,800 --> 00:05:04,640
 groups or reflect on them individually

97
00:05:04,640 --> 00:05:08,960
 saying Kesa, Kesa, Kesa and just repeat that word over and

98
00:05:08,960 --> 00:05:11,920
 over focusing on the concept of head hair

99
00:05:11,920 --> 00:05:15,600
 in much the same way as we focus on experience in Vipassana

100
00:05:15,600 --> 00:05:18,640
 meditation. Except in this meditation

101
00:05:18,640 --> 00:05:22,720
 one focuses on a concept, the concept of head hair. You do

102
00:05:22,720 --> 00:05:24,480
 not have to convince yourself that

103
00:05:24,480 --> 00:05:27,670
 the body is lonesome. You just reflect mindfully about the

104
00:05:27,670 --> 00:05:29,360
 parts of the body and slowly the

105
00:05:29,360 --> 00:05:32,720
 delusion of beauty fades as you become more familiar with

106
00:05:32,720 --> 00:05:34,480
 the true nature of the body.

107
00:05:34,480 --> 00:05:37,640
 In some cases it can happen that one does become repulsed

108
00:05:37,640 --> 00:05:40,000
 as a result of this sort of practice

109
00:05:40,000 --> 00:05:42,940
 so it is best practiced by one who has great lust for

110
00:05:42,940 --> 00:05:45,360
 physical beauty to help free them of

111
00:05:45,360 --> 00:05:49,820
 that misperception. The third meditation is friendliness,

112
00:05:49,820 --> 00:05:51,760
 metta which is often used as a

113
00:05:51,760 --> 00:05:55,300
 means of dedicating the goodness gained through cultivating

114
00:05:55,300 --> 00:05:57,200
 mindfulness to those we care for.

115
00:05:57,200 --> 00:06:00,660
 After a formal meditation session you can channel the

116
00:06:00,660 --> 00:06:02,560
 strength and clarity of mind

117
00:06:02,560 --> 00:06:05,990
 gained from meditation to express appreciation for all

118
00:06:05,990 --> 00:06:08,480
 beings and to commit yourself to overcoming

119
00:06:08,480 --> 00:06:12,080
 any conflict you might have with others. This practice can

120
00:06:12,080 --> 00:06:14,000
 be of great benefit to the practice

121
00:06:14,000 --> 00:06:17,230
 of Vipassana meditation helping to straighten out the

122
00:06:17,230 --> 00:06:19,680
 crookedness we have in our minds. There are

123
00:06:19,680 --> 00:06:23,180
 many ways to practice metta. You can extend goodwill

124
00:06:23,180 --> 00:06:25,600
 consecutively in terms of proximity,

125
00:06:25,600 --> 00:06:28,340
 wishing first for your parents to be happy then for

126
00:06:28,340 --> 00:06:31,280
 relatives, family, people nearby, people in

127
00:06:31,280 --> 00:06:35,160
 your city, country, the whole world and finally the whole

128
00:06:35,160 --> 00:06:37,760
 universe. Even a few minutes of this

129
00:06:37,760 --> 00:06:40,970
 practice after finishing with Vipassana meditation is

130
00:06:40,970 --> 00:06:43,440
 useful for overcoming anger, strengthening

131
00:06:43,440 --> 00:06:46,980
 relationships with others and straightening the mind. The

132
00:06:46,980 --> 00:06:49,200
 fourth meditation is mindfulness of

133
00:06:49,200 --> 00:06:52,850
 death which is useful for cultivating a sense of urgency

134
00:06:52,850 --> 00:06:55,600
 reminding us that we cannot just go on

135
00:06:55,600 --> 00:06:58,970
 with our lives as though they were no tomorrow. Eventually

136
00:06:58,970 --> 00:07:00,960
 there comes a day of reckoning when

137
00:07:00,960 --> 00:07:04,820
 our physical body dies. At the time of one's death they say

138
00:07:04,820 --> 00:07:06,960
 your whole life flashes before

139
00:07:06,960 --> 00:07:10,320
 your eyes and you will cling to whatever has the strongest

140
00:07:10,320 --> 00:07:12,480
 emotional hold on your mind,

141
00:07:12,480 --> 00:07:16,040
 leading you to be born again based on that experience. If

142
00:07:16,040 --> 00:07:18,000
 it is a bad experience your next

143
00:07:18,000 --> 00:07:21,450
 life will be unpleasant. Remembering this possibility is an

144
00:07:21,450 --> 00:07:23,280
 important practice to stop us

145
00:07:23,280 --> 00:07:27,090
 from being negligent. There are many ways to cultivate

146
00:07:27,090 --> 00:07:29,520
 mindfulness of death. The traditional

147
00:07:29,520 --> 00:07:33,590
 method is to simply say to yourself something like life is

148
00:07:33,590 --> 00:07:36,720
 uncertain, death is certain or all

149
00:07:36,720 --> 00:07:40,250
 beings have to die and I too will have to die one day.

150
00:07:40,250 --> 00:07:43,120
 There is a story of the bodhisattva teaching

151
00:07:43,120 --> 00:07:46,720
 his whole family mindfulness of death so they were never

152
00:07:46,720 --> 00:07:49,600
 complacent and when his son died no one in

153
00:07:49,600 --> 00:07:53,360
 the family was consumed by suffering at the loss. You can

154
00:07:53,360 --> 00:07:56,160
 and should make use of all these meditations

155
00:07:56,160 --> 00:07:59,810
 from time to time as appropriate. They are all beneficial

156
00:07:59,810 --> 00:08:02,240
 and can be used together with Vipassana

157
00:08:02,240 --> 00:08:07,120
 meditation to great benefit.

