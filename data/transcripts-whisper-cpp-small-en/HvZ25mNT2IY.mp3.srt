1
00:00:00,000 --> 00:00:02,450
 How does the attainment of the higher paths such as

2
00:00:02,450 --> 00:00:06,880
 becoming a Sakatakami and the fruits differ from the

3
00:00:06,880 --> 00:00:08,480
 attainment of stream entry?

4
00:00:08,480 --> 00:00:13,120
 They differ based on the defilements that they cut

5
00:00:13,120 --> 00:00:16,640
 and it is difficult really to

6
00:00:16,640 --> 00:00:19,880
 to

7
00:00:19,880 --> 00:00:23,360
 To match practice and theory

8
00:00:23,360 --> 00:00:28,200
 Because in theory it seems to be a very definite

9
00:00:28,880 --> 00:00:32,390
 Boom like a signpost thing where you can now see now you're

10
00:00:32,390 --> 00:00:34,680
 a Sakitakami now you're an Anakami

11
00:00:34,680 --> 00:00:37,940
 That that is totally different from the practice like you

12
00:00:37,940 --> 00:00:40,060
're practicing practicing practice and boom suddenly you're

13
00:00:40,060 --> 00:00:40,680
 a Sotapanan

14
00:00:40,680 --> 00:00:45,520
 Or let's not say Sotapanan that is actually quite quite a

15
00:00:45,520 --> 00:00:46,760
 profound shift

16
00:00:46,760 --> 00:00:49,880
 But to a second from a Sotapanan to a Sakitakami for

17
00:00:49,880 --> 00:00:52,240
 example in practice

18
00:00:52,880 --> 00:00:56,370
 I would say the safest thing both from our practical

19
00:00:56,370 --> 00:00:59,360
 experience and still keeping with the theory is

20
00:00:59,360 --> 00:01:03,840
 It's simply a

21
00:01:03,840 --> 00:01:08,150
 Marker like a marker on a thermostat that when you when you

22
00:01:08,150 --> 00:01:09,600
 get to a certain level

23
00:01:09,600 --> 00:01:12,530
 When the temperature gets to a certain level you can see

24
00:01:12,530 --> 00:01:13,880
 that it's past that line

25
00:01:13,880 --> 00:01:18,680
 Or a gas tank you know because you're emptying all the def

26
00:01:18,680 --> 00:01:20,920
ilements out so when it gets to a certain line you can say

27
00:01:20,920 --> 00:01:21,080
 now

28
00:01:21,080 --> 00:01:23,950
 It's gone three quarters of a tank now. It's half a tank

29
00:01:23,950 --> 00:01:25,800
 and those quarter of a tank

30
00:01:25,800 --> 00:01:30,040
 Because in between Sotapanan Sakitakami there are still the

31
00:01:30,040 --> 00:01:31,040
 realizations of

32
00:01:31,040 --> 00:01:33,800
 nimana

33
00:01:33,800 --> 00:01:37,140
 One practices again and again and realizes attains the

34
00:01:37,140 --> 00:01:39,480
 fruit of the first path again

35
00:01:39,480 --> 00:01:44,010
 One can never realize the the the path of the Sotapani my

36
00:01:44,010 --> 00:01:46,960
 sort of the Patea Magga again

37
00:01:46,960 --> 00:01:48,840
 because

38
00:01:48,840 --> 00:01:51,990
 That's just a technical name for the first realization of n

39
00:01:51,990 --> 00:01:55,760
imana the next realization and subsequent realizations are

40
00:01:55,760 --> 00:01:59,000
 Pala Pala Nyan

41
00:01:59,000 --> 00:02:03,850
 And so you can continue to go back to Pala Nyan which will

42
00:02:03,850 --> 00:02:06,000
 continue to weaken the defilements

43
00:02:06,000 --> 00:02:09,200
 but until one reaches a

44
00:02:09,200 --> 00:02:11,880
 certain

45
00:02:11,880 --> 00:02:16,360
 Unverifiable state you can this year the Buddha a certain

46
00:02:17,280 --> 00:02:19,280
 recognizable state where

47
00:02:19,280 --> 00:02:25,760
 There's not enough defilements left to cause you to be born

48
00:02:25,760 --> 00:02:27,440
 more than once

49
00:02:27,440 --> 00:02:30,680
 As a human being or as an angel

50
00:02:30,680 --> 00:02:34,330
 Then you can say you're a Sakitakami, but it's very

51
00:02:34,330 --> 00:02:36,830
 difficult if you're not a Buddha. How could you know what

52
00:02:36,830 --> 00:02:37,300
 that line is?

53
00:02:37,300 --> 00:02:40,720
 An anagami will be more easy to see because you get to the

54
00:02:40,720 --> 00:02:43,880
 point where you have eradicated all

55
00:02:47,040 --> 00:02:49,640
 Ra Kamara Ga and patika all

56
00:02:49,640 --> 00:02:52,400
 greed and over all

57
00:02:52,400 --> 00:02:56,240
 Lust and and aversion

58
00:02:56,240 --> 00:03:02,000
 and our hand is is I would say even easier to see because

59
00:03:02,000 --> 00:03:05,200
 At the realization of our head. There is no

60
00:03:05,200 --> 00:03:08,600
 There's then no defilements whatsoever and

61
00:03:08,600 --> 00:03:12,520
 One realizes this one one comes out of the Pala Nyan

62
00:03:13,400 --> 00:03:15,820
 One is able to see what defilements are left because the

63
00:03:15,820 --> 00:03:19,040
 mind is very pure at that point like clear water

64
00:03:19,040 --> 00:03:22,000
 And one is able to see clearly what defilements are left

65
00:03:22,000 --> 00:03:23,520
 what defilements are gone

66
00:03:23,520 --> 00:03:29,480
 So what remains to be done

67
00:03:29,480 --> 00:03:32,880
 But the actual realization is the same

68
00:03:32,880 --> 00:03:36,420
 so you if you want to forget about all four of these things

69
00:03:36,420 --> 00:03:39,400
 you just say the realization of the the

70
00:03:40,880 --> 00:03:42,960
 subsequent and and continue no

71
00:03:42,960 --> 00:03:47,320
 Repeated

72
00:03:47,320 --> 00:03:49,880
 Realization of Nibbana cuts off more and more defilements

73
00:03:49,880 --> 00:03:52,830
 and you just have these markers until they're all gone, and

74
00:03:52,830 --> 00:03:53,840
 then it's our hot

75
00:03:53,840 --> 00:03:55,840
 you

