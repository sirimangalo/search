1
00:00:00,000 --> 00:00:07,840
 I have anxiety panic attacks at least one time per day.

2
00:00:07,840 --> 00:00:09,600
 How can I calm down?

3
00:00:09,600 --> 00:00:13,480
 I mean if I think on what's happening the panic becomes

4
00:00:13,480 --> 00:00:14,240
 worse.

5
00:00:14,240 --> 00:00:15,440
 Help.

6
00:00:15,440 --> 00:00:21,140
 Well this is one that I answer a lot in various forms but

7
00:00:21,140 --> 00:00:24,760
 it's especially good to answer these

8
00:00:24,760 --> 00:00:29,640
 questions because this is a real life important question.

9
00:00:29,640 --> 00:00:35,460
 It's not like what would happen if everyone became Buddhist

10
00:00:35,460 --> 00:00:37,160
 and so on and so on.

11
00:00:37,160 --> 00:00:38,560
 Because it's actually pertinent.

12
00:00:38,560 --> 00:00:40,760
 It's actually an important question.

13
00:00:40,760 --> 00:00:44,480
 It's not such an important question to know what would

14
00:00:44,480 --> 00:00:49,600
 happen if X, not to really, not

15
00:00:49,600 --> 00:00:54,260
 to poke fun too much but this is an important question

16
00:00:54,260 --> 00:00:57,400
 because you have anxiety and that's

17
00:00:57,400 --> 00:01:04,560
 causing real problems in your life.

18
00:01:04,560 --> 00:01:09,840
 So you have to ask yourself is the anxiety really a problem

19
00:01:09,840 --> 00:01:10,200
?

20
00:01:10,200 --> 00:01:14,640
 Is the problem really the anxiety?

21
00:01:14,640 --> 00:01:17,800
 Because if it is then clearly looking at it is not the

22
00:01:17,800 --> 00:01:18,840
 answer right?

23
00:01:18,840 --> 00:01:21,760
 Because when you look at it it gets stronger.

24
00:01:21,760 --> 00:01:23,560
 It becomes clearer.

25
00:01:23,560 --> 00:01:28,520
 You get even more focused on the anxiety.

26
00:01:28,520 --> 00:01:31,750
 But let's step back a second and really ask ourselves what

27
00:01:31,750 --> 00:01:33,560
 happens when you're anxious?

28
00:01:33,560 --> 00:01:37,360
 What does it do?

29
00:01:37,360 --> 00:01:42,170
 Probably it tenses up the stomach, tenses up the head, t

30
00:01:42,170 --> 00:01:44,920
enses up the shoulders and makes

31
00:01:44,920 --> 00:01:57,290
 you feel really bad and prevents you from interacting with

32
00:01:57,290 --> 00:01:59,040
 other people,

33
00:01:59,040 --> 00:02:00,640
 from carrying out your life.

34
00:02:00,640 --> 00:02:04,530
 It might even lead you to do something desperate, hurt

35
00:02:04,530 --> 00:02:06,920
 yourself, hurt other people.

36
00:02:06,920 --> 00:02:15,600
 It will prevent you from living an ordinary life.

37
00:02:15,600 --> 00:02:18,400
 But what is actually going on there?

38
00:02:18,400 --> 00:02:31,140
 When you have the anxiety, what is it that is preventing

39
00:02:31,140 --> 00:02:33,320
 you from, what is it that is

40
00:02:33,320 --> 00:02:38,160
 actually preventing you from carrying out your life in an

41
00:02:38,160 --> 00:02:39,960
 ordinary manner?

42
00:02:39,960 --> 00:02:51,930
 When you look at the anxiety you'll see that actually it

43
00:02:51,930 --> 00:02:54,680
 really isn't objectively a very

44
00:02:54,680 --> 00:02:58,000
 big problem at all.

45
00:02:58,000 --> 00:03:04,490
 Anxiety is a state of mind that causes physical reactions,

46
00:03:04,490 --> 00:03:06,720
 tensing and so on.

47
00:03:06,720 --> 00:03:08,400
 But it really stops there.

48
00:03:08,400 --> 00:03:12,800
 A simple anxiety, panic attack stops there.

49
00:03:12,800 --> 00:03:15,520
 It has effects in the mind, it has effects in the body.

50
00:03:15,520 --> 00:03:25,010
 But unless we react to it in a negative way, it doesn't

51
00:03:25,010 --> 00:03:31,880
 have a massive impact or a significant

52
00:03:31,880 --> 00:03:33,600
 impact on our lives.

53
00:03:33,600 --> 00:03:37,360
 It doesn't have to have a significant impact on our lives

54
00:03:37,360 --> 00:03:38,280
 in any way.

55
00:03:38,280 --> 00:03:43,260
 I used to have very strong panic attacks when I had to give

56
00:03:43,260 --> 00:03:45,560
 a talk, when I had to get up

57
00:03:45,560 --> 00:03:53,800
 on stage or anything like that.

58
00:03:53,800 --> 00:03:58,340
 This was an issue as a Buddhist monk because people think

59
00:03:58,340 --> 00:04:00,600
 you're in robes and they must

60
00:04:00,600 --> 00:04:04,080
 know something so they want you to give a talk.

61
00:04:04,080 --> 00:04:07,820
 So you get up on stage and sometimes in front of hundreds

62
00:04:07,820 --> 00:04:09,920
 or I don't know hundreds of people

63
00:04:09,920 --> 00:04:16,760
 anyway and it can create a real panic attack.

64
00:04:16,760 --> 00:04:21,070
 That is a clear example of how people who don't have a

65
00:04:21,070 --> 00:04:23,440
 strong tendency to panic can

66
00:04:23,440 --> 00:04:25,360
 really panic.

67
00:04:25,360 --> 00:04:29,330
 And what I found through the meditation was, the hardest

68
00:04:29,330 --> 00:04:31,640
 part of it was that noting didn't

69
00:04:31,640 --> 00:04:36,530
 help, meditation didn't help because I would acknowledge it

70
00:04:36,530 --> 00:04:39,160
, I would say to myself, afraid,

71
00:04:39,160 --> 00:04:42,040
 afraid or feeling, feeling the tension in the stomach and

72
00:04:42,040 --> 00:04:44,880
 so on and it wouldn't go away.

73
00:04:44,880 --> 00:04:49,160
 Sometimes it would get even worse.

74
00:04:49,160 --> 00:04:55,040
 But what I found was that by doing it anyway, by noting

75
00:04:55,040 --> 00:04:58,520
 anyway, regardless of whether it

76
00:04:58,520 --> 00:05:02,000
 was going away, by trying and trying and trying and really

77
00:05:02,000 --> 00:05:04,000
 putting my heart into the noting

78
00:05:04,000 --> 00:05:10,290
 and every moment to be aware of what's going on, that the

79
00:05:10,290 --> 00:05:14,400
 panic, the panic attack in general

80
00:05:14,400 --> 00:05:18,070
 didn't get in my way, surprisingly enough, didn't get in my

81
00:05:18,070 --> 00:05:19,720
 way in giving the talk.

82
00:05:19,720 --> 00:05:23,110
 And sometimes I actually gave a better talk than when I was

83
00:05:23,110 --> 00:05:23,880
 relaxed.

84
00:05:23,880 --> 00:05:28,090
 I've seen motivational speakers because when I was young I

85
00:05:28,090 --> 00:05:30,520
 would go to all these leadership

86
00:05:30,520 --> 00:05:32,840
 conferences and they'd have motivational speakers.

87
00:05:32,840 --> 00:05:37,650
 I saw one motivational speaker twice and it's something I

88
00:05:37,650 --> 00:05:39,720
'll always remember.

89
00:05:39,720 --> 00:05:45,360
 The first time you gave the speech, I didn't know him or I

90
00:05:45,360 --> 00:05:48,320
 wasn't in close contact with

91
00:05:48,320 --> 00:05:50,800
 him, but it was a big deal.

92
00:05:50,800 --> 00:05:54,920
 I'd never been to this leadership conference before.

93
00:05:54,920 --> 00:05:57,530
 This was his first time being invited and so it seemed like

94
00:05:57,530 --> 00:05:58,720
 he took it very seriously

95
00:05:58,720 --> 00:06:00,120
 and it was a brilliant talk.

96
00:06:00,120 --> 00:06:03,880
 It was really something that inspired us all.

97
00:06:03,880 --> 00:06:06,920
 The second time he came back to give a talk again and I was

98
00:06:06,920 --> 00:06:08,480
, at that point, it was my

99
00:06:08,480 --> 00:06:12,430
 second time or third time or whatever and I was one of the

100
00:06:12,430 --> 00:06:14,760
 organizers and so I was talking

101
00:06:14,760 --> 00:06:17,830
 with him and I was really impressed by this guy so I was

102
00:06:17,830 --> 00:06:20,200
 sitting with him and he was,

103
00:06:20,200 --> 00:06:24,310
 he was so, seemed so high on his last appearance that he

104
00:06:24,310 --> 00:06:27,040
 got so, he got overconfident and he

105
00:06:27,040 --> 00:06:28,890
 was sitting there telling me about how you just get up

106
00:06:28,890 --> 00:06:29,840
 there and say, you just get up

107
00:06:29,840 --> 00:06:32,550
 there and talk, you know, it doesn't matter what you say,

108
00:06:32,550 --> 00:06:33,880
 it always comes out right if

109
00:06:33,880 --> 00:06:36,600
 you just let yourself talk and so on.

110
00:06:36,600 --> 00:06:39,200
 And he got up and he bombed totally.

111
00:06:39,200 --> 00:06:44,800
 He really fell flat because he was expecting people to just

112
00:06:44,800 --> 00:06:47,720
, he was expecting a positive

113
00:06:47,720 --> 00:06:48,920
 reaction.

114
00:06:48,920 --> 00:06:51,410
 And so he wasn't trying, he wasn't putting any effort into

115
00:06:51,410 --> 00:06:52,720
 it, he wasn't being mindful,

116
00:06:52,720 --> 00:06:53,720
 you might say.

117
00:06:53,720 --> 00:06:57,080
 Of course, he didn't have a great knowledge of mindfulness.

118
00:06:57,080 --> 00:07:00,930
 When you're worried, when you have stress, it's often

119
00:07:00,930 --> 00:07:03,080
 easier to be mindful than when

120
00:07:03,080 --> 00:07:05,200
 you're happy, right?

121
00:07:05,200 --> 00:07:07,900
 This is why people come to Buddhism when they're suffering

122
00:07:07,900 --> 00:07:09,480
 because they really feel like they

123
00:07:09,480 --> 00:07:13,440
 need an answer.

124
00:07:13,440 --> 00:07:22,500
 And so, the surprising thing led me to, or helped me to

125
00:07:22,500 --> 00:07:24,600
 understand what I've come to

126
00:07:24,600 --> 00:07:28,160
 understand through, in general through the meditation

127
00:07:28,160 --> 00:07:30,440
 practice is that the biggest problem

128
00:07:30,440 --> 00:07:34,660
 is not the experiences, even be they inherently negative

129
00:07:34,660 --> 00:07:37,320
 experiences, it's our tendency to

130
00:07:37,320 --> 00:07:43,280
 react to them, react negatively to a negative experience.

131
00:07:43,280 --> 00:07:47,400
 So the truth of it is, when you focus on the anxiety, or

132
00:07:47,400 --> 00:07:49,720
 not just focus on it, when you're

133
00:07:49,720 --> 00:07:54,790
 recognized the anxiety as anxiety, the anxiety itself

134
00:07:54,790 --> 00:07:56,240
 disappears.

135
00:07:56,240 --> 00:08:00,760
 The anxious mind state is replaced by a mindful mind state.

136
00:08:00,760 --> 00:08:04,140
 What doesn't disappear and what fools you into thinking

137
00:08:04,140 --> 00:08:05,880
 that you're still anxious is

138
00:08:05,880 --> 00:08:14,910
 the physical results of it, the physical manifestations of

139
00:08:14,910 --> 00:08:20,160
 it, because they are not directly connected

140
00:08:20,160 --> 00:08:21,960
 to one state of mind.

141
00:08:21,960 --> 00:08:25,530
 It's like you've already set the ball rolling and now you

142
00:08:25,530 --> 00:08:27,640
're feeling the effects of it.

143
00:08:27,640 --> 00:08:30,510
 Now you're feeling the effects of the anxiety in the

144
00:08:30,510 --> 00:08:33,200
 stomach, in the head, in the shoulders,

145
00:08:33,200 --> 00:08:38,560
 everything all tense, and that's what doesn't go away.

146
00:08:38,560 --> 00:08:41,350
 The problem is we don't see that, we lose our confidence in

147
00:08:41,350 --> 00:08:42,680
 the meditation practice

148
00:08:42,680 --> 00:08:45,080
 and we give it up and we feel anxious again.

149
00:08:45,080 --> 00:08:47,800
 We get worried about it, we say, "It's not going away."

150
00:08:47,800 --> 00:08:50,740
 And then we get anxious again, it's really, this isn't the,

151
00:08:50,740 --> 00:08:52,200
 I don't have an answer, I

152
00:08:52,200 --> 00:08:56,160
 don't know what to do, I still don't have a solution.

153
00:08:56,160 --> 00:08:58,590
 And so it makes us anxious again, the anxiety comes up

154
00:08:58,590 --> 00:09:00,120
 again and we try to be mindful of

155
00:09:00,120 --> 00:09:04,010
 it again and we find that yes, the effects of it are still

156
00:09:04,010 --> 00:09:05,800
 there and so we think the

157
00:09:05,800 --> 00:09:12,920
 anxiety is still there and so on and so on.

158
00:09:12,920 --> 00:09:23,330
 If we are observant, we will be able to notice the

159
00:09:23,330 --> 00:09:27,440
 difference and you should try to notice

160
00:09:27,440 --> 00:09:29,000
 the difference.

161
00:09:29,000 --> 00:09:31,520
 Even that you need this, you need a teacher to point it out

162
00:09:31,520 --> 00:09:32,840
 to you that the physical and

163
00:09:32,840 --> 00:09:34,880
 mental are two different things.

164
00:09:34,880 --> 00:09:37,840
 It's the first stage of knowledge that a person comes to,

165
00:09:37,840 --> 00:09:39,880
 the difference between the physical

166
00:09:39,880 --> 00:09:41,880
 and the mental and the separation between them two.

167
00:09:41,880 --> 00:09:44,270
 And in fact, that's what you're realizing, that's what this

168
00:09:44,270 --> 00:09:45,100
 is showing to you.

169
00:09:45,100 --> 00:09:46,880
 It's showing you the difference between the physical and

170
00:09:46,880 --> 00:09:48,560
 mental because when you say,

171
00:09:48,560 --> 00:09:52,280
 "Anxious, anxious or worried, worried," the worry is

172
00:09:52,280 --> 00:09:53,480
 already gone.

173
00:09:53,480 --> 00:09:57,000
 At that moment you're not worried, you're focused and

174
00:09:57,000 --> 00:09:58,960
 mindful and clearly aware.

175
00:09:58,960 --> 00:10:02,600
 The physical is not affected by that.

176
00:10:02,600 --> 00:10:05,920
 The physical will continue to arise.

177
00:10:05,920 --> 00:10:09,240
 The physical is something separate from this.

178
00:10:09,240 --> 00:10:13,850
 It is something that has already been affected by the past

179
00:10:13,850 --> 00:10:14,880
 anxiety.

180
00:10:14,880 --> 00:10:17,920
 What you should do at that point when you still feel the

181
00:10:17,920 --> 00:10:19,560
 effects of the anxiousness

182
00:10:19,560 --> 00:10:22,180
 is you should see them as physical and note them as well,

183
00:10:22,180 --> 00:10:23,640
 the feeling in the stomach,

184
00:10:23,640 --> 00:10:25,600
 feeling, feeling tense, tense.

185
00:10:25,600 --> 00:10:28,540
 If you react to it, disliking it, you should focus on this

186
00:10:28,540 --> 00:10:30,720
 new mind state that has arisen,

187
00:10:30,720 --> 00:10:31,720
 disliking, disliking.

188
00:10:31,720 --> 00:10:33,280
 But this is what's going on.

189
00:10:33,280 --> 00:10:37,540
 An anxiety attack is not just anxiousness, this entity that

190
00:10:37,540 --> 00:10:39,400
 is there continuously.

191
00:10:39,400 --> 00:10:43,050
 It's a whole bunch of things going on from moment to moment

192
00:10:43,050 --> 00:10:43,360
.

193
00:10:43,360 --> 00:10:46,750
 Anxious, physical feeling, disliking the physical feeling,

194
00:10:46,750 --> 00:10:49,160
 anxious again and so on and so on.

195
00:10:49,160 --> 00:10:52,640
 There are so many different things involved in it.

196
00:10:52,640 --> 00:10:55,700
 There can be ego, the attachment to it, "I'm anxious,"

197
00:10:55,700 --> 00:11:00,640
 though there can be the desire to

198
00:11:00,640 --> 00:11:06,920
 be confident and impress other people and so on.

199
00:11:06,920 --> 00:11:08,760
 But certainly the meditation is working.

200
00:11:08,760 --> 00:11:13,760
 The meditation is helping you to see that these things are

201
00:11:13,760 --> 00:11:16,160
 not under your control.

202
00:11:16,160 --> 00:11:19,630
 Whether the physical nor the mental is subject to your

203
00:11:19,630 --> 00:11:21,960
 control, the more you cling to it,

204
00:11:21,960 --> 00:11:27,130
 the more you try to fix it or try to avoid it, try to

205
00:11:27,130 --> 00:11:31,200
 change it, try to make things better,

206
00:11:31,200 --> 00:11:35,510
 the more suffering, stress and disappointment you'll create

207
00:11:35,510 --> 00:11:37,620
 for yourself, the bigger the

208
00:11:37,620 --> 00:11:40,880
 problem will become.

209
00:11:40,880 --> 00:11:43,990
 When the anxiety comes, it is correct to say to yourself, "

210
00:11:43,990 --> 00:11:45,880
Anxious, anxious," to focus on

211
00:11:45,880 --> 00:11:51,380
 it, to be mindful of it, to let yourself "be anxious,"

212
00:11:51,380 --> 00:11:54,200
 because the anxiety is not what's

213
00:11:54,200 --> 00:11:55,200
 the problem.

214
00:11:55,200 --> 00:11:59,540
 The problem is your reaction to generally physical states

215
00:11:59,540 --> 00:12:02,120
 that are caused by the anxiety.

216
00:12:02,120 --> 00:12:03,120
 Anxiety is just a moment.

217
00:12:03,120 --> 00:12:06,320
 It's something that occurs in the moment and then is gone.

218
00:12:06,320 --> 00:12:09,040
 At the moment when you're mindful, it's already gone.

219
00:12:09,040 --> 00:12:11,990
 All that's left is the physical experiences, which should

220
00:12:11,990 --> 00:12:13,000
 be noted as well.

221
00:12:13,000 --> 00:12:16,160
 If you can be thorough with this and note everything, note

222
00:12:16,160 --> 00:12:17,720
 the anxiety when it's there,

223
00:12:17,720 --> 00:12:21,290
 note the physical sensations, note the disliking, going

224
00:12:21,290 --> 00:12:23,880
 back and forth and be thorough and complete

225
00:12:23,880 --> 00:12:29,050
 in your noting, you'll begin to get the hang of it and you

226
00:12:29,050 --> 00:12:31,800
 will realize that it doesn't

227
00:12:31,800 --> 00:12:33,560
 matter what arises.

228
00:12:33,560 --> 00:12:37,600
 Nothing has the power to hurt you until you react to it.

229
00:12:37,600 --> 00:12:45,170
 Not even negative mind states can hurt you until you react

230
00:12:45,170 --> 00:12:46,680
 to them.

231
00:12:46,680 --> 00:12:48,930
 Just be a little more patient with it and you'll see for

232
00:12:48,930 --> 00:12:50,160
 yourself all the things that

233
00:12:50,160 --> 00:12:52,040
 I'm explaining to you.

234
00:12:52,040 --> 00:12:55,780
 If you're a little more patient with the anxiety, noting it

235
00:12:55,780 --> 00:12:57,720
 even though the anxiety doesn't

236
00:12:57,720 --> 00:13:01,160
 seem to go away and noting more over the physical

237
00:13:01,160 --> 00:13:04,440
 sensations, the disliking of the anxiety,

238
00:13:04,440 --> 00:13:08,300
 and so on and so on, every physical and mental experience

239
00:13:08,300 --> 00:13:10,640
 that arises from moment to moment,

240
00:13:10,640 --> 00:13:14,310
 you will see and you'll be able to break these things apart

241
00:13:14,310 --> 00:13:16,220
 and you'll cease to be disturbed

242
00:13:16,220 --> 00:13:18,770
 by them because you'll see that they don't have any power

243
00:13:18,770 --> 00:13:20,080
 over you and that it doesn't

244
00:13:20,080 --> 00:13:21,400
 really matter what occurs.

245
00:13:21,400 --> 00:13:24,020
 It doesn't matter whether you get afraid, it doesn't matter

246
00:13:24,020 --> 00:13:25,280
 whether you're anxious,

247
00:13:25,280 --> 00:13:28,220
 it doesn't even matter whether it inhibits your ability to

248
00:13:28,220 --> 00:13:29,240
 perform in life.

249
00:13:29,240 --> 00:13:33,850
 It doesn't matter if people think you're a fool and useless

250
00:13:33,850 --> 00:13:35,560
 and incompetent.

251
00:13:35,560 --> 00:13:37,360
 It doesn't really matter.

252
00:13:37,360 --> 00:13:39,320
 It doesn't matter if you're thrown out on the street.

253
00:13:39,320 --> 00:13:42,360
 It doesn't matter if you die.

254
00:13:42,360 --> 00:13:46,210
 When you come to this realization, this is what really over

255
00:13:46,210 --> 00:13:47,400
comes anxiety.

256
00:13:47,400 --> 00:13:51,000
 I mean, I guess I had real anxiety problems even as a monk

257
00:13:51,000 --> 00:13:53,200
 and it's something that through

258
00:13:53,200 --> 00:13:59,480
 practice really is really easy to solve actually.

259
00:13:59,480 --> 00:14:03,480
 I think anxiety and fear are really easy ones to solve.

260
00:14:03,480 --> 00:14:07,640
 They may not be eradicated quickly, but the solution comes

261
00:14:07,640 --> 00:14:10,000
 fairly quickly when you realize

262
00:14:10,000 --> 00:14:11,480
 that it doesn't really matter.

263
00:14:11,480 --> 00:14:13,040
 Yeah, you can be anxious.

264
00:14:13,040 --> 00:14:16,320
 Yeah, you can be afraid.

265
00:14:16,320 --> 00:14:21,160
 These are the, of course, acceptance, which is the opposite

266
00:14:21,160 --> 00:14:22,360
 of anxiety.

267
00:14:22,360 --> 00:14:26,640
 It destroys them, I think.

268
00:14:26,640 --> 00:14:31,480
 Anybody want to chime in there?

269
00:14:31,480 --> 00:14:39,240
 Marianne have something to add?

270
00:14:39,240 --> 00:14:43,270
 I was just going to say how you answered this problem and

271
00:14:43,270 --> 00:14:45,760
 with the emphasis on patience and

272
00:14:45,760 --> 00:14:51,780
 realizing that all these petty little feelings that we have

273
00:14:51,780 --> 00:14:55,000
 are just not that important.

274
00:14:55,000 --> 00:14:59,880
 I was thinking of you when I said getting thrown out on the

275
00:14:59,880 --> 00:15:00,880
 street.

276
00:15:00,880 --> 00:15:02,560
 I was like, "No."

