 Good evening.
 So we're looking at the Dhammapada again tonight,
 continuing with verse 193, which reads as follows.
 "Dulabho purisa janyu nasu sambhata jayati yataso jayati de
 ro dankulansukkame dati."
 Which means a thoroughbred man is hard to find, hard to get
, hard to find.
 They are not born everywhere.
 "Dulabho yataso jayati de ro, in a place where such a wise
 person is born, that family finds great happiness."
 So there's not much of a story. This is another one of An
anda's questions.
 Ananda didn't become enlightened until after the Buddha
 passed away.
 So you sometimes wonder whether these questions were a part
 of that.
 I think that's perhaps a little unfair, but he seems to be
 very much interested in gaining all the theoretical
 knowledge he can while the Buddha is still around.
 And I don't think, you can necessarily chastise him for it,
 I don't think the Buddha ever did.
 The Buddha seemed to understand that Ananda wouldn't become
 enlightened, but that his goal and his intent, his path,
 because not all Buddhist practitioners have the same path,
 his path would be one of theoretical study.
 And so he had many questions just to make sure he
 understood and that he knew, not just understood but knew,
 and had in his mind all of these teachings, apparently had
 a very good memory.
 There are people in the world, you hear about them, they
 have these photographic memories or something.
 Apparently Ananda had a very good memory, so he could
 remember anything, but he had to hear it.
 So he asked the Buddha many questions, and his question
 here was, "Where do you find a thoroughbred human?"
 He said, "We know about thoroughbred horses and elephants,
 and they're actually pretty easy to find.
 You can find them anywhere, but where can you find a
 thoroughbred human?"
 So the story says the Buddha took it to mean that Ananda
 was talking about a Buddha.
 So there's two ways we can look at this verse. From the
 story perspective, we're talking about the Buddha.
 Where do you find a Buddha? And it's not easy to find a
 Buddha. It takes a long, long time.
 It's not just two or three days and then you get a Buddha.
 A Buddha is something that requires uncountable lengths of
 time,
 many multiple uncountable lengths of time for a person to
 become a Buddha.
 They're very rare. They don't just arise up anywhere.
 So he talked a little bit about the conditions that are
 required for the arising of a Buddha,
 what sort of family they'll be born into and that sort of
 thing.
 But from the perspective of the verse, there's another
 aspect. Yes, the verse says that it's hard to find a Buddha
,
 but it also says how great or how hard it is to find a
 thoroughbred person.
 It also talks about how great it is for their family and
 just how great it is and how much happiness comes from a
 person who is thoroughbred.
 And it makes us think of the Buddha's teaching on thorough
bred humans.
 And it seems pretty clear from that teaching that the
 Buddha didn't reserve this teaching only for Buddhas, this
 designation.
 So just because someone isn't a Buddha doesn't mean they
 can't be thoroughbred.
 So there's two things I want to talk about here.
 The first one, coming off the story perspective, is a
 little bit of humility
 because it's easy to think that you're special.
 And in Buddhism, this manifests itself as people thinking
 that they're qualified to become a Buddha.
 And I think, I'm certainly not a person to put someone in
 their place and say, "You can't become a Buddha."
 But I think the general consensus in our tradition is that
 a lot more people want to become Buddhas than will ever
 actually attain Buddhahood.
 So Kondanya was, no, Kondanya, what's his name?
 Kachayana, I think. I think Kachayana, one of the Buddha's
 chief disciples, had made a vow to become a Buddha.
 It wasn't him, it was someone.
 And when he came to this life and saw the Buddha, he
 thought to himself, "Oh boy, that's just too pure and too
 perfect.
 I'm not capable of that." And he gave it up and became a
 monk.
 But a lot of people just have the idea to become a Buddha.
 And I think it can be based on this sort of conceit of
 thinking that you're special.
 Thinking that you have some special quality that makes you
 better than everyone else.
 And so when I read this first, one of the things I think of
 is it's a reminder that I'm not a Buddha.
 I'm not this kind of a thoroughbred. I'm not anything
 special.
 I think that's potentially useful.
 Of course you have to be careful not to be too hard on
 yourself or think of yourself as bad or evil or corrupt or
 just useless or pointless.
 But on the other hand, we have to find freedom.
 We have to, to some extent, see how worthless and useless
 we are, our bodies and our minds.
 Because part of letting go is seeing how useless it all is.
 Part of becoming a Buddha even is to realize how useless
 the person who is the Buddha actually is.
 Meaning in an ultimate sense, the person, the being, the
 identity,
 and even the physical and mental manifestations are in and
 of themselves of no value.
 They have nothing that you should cherish or hold on to.
 So reminding ourselves that we are just ordinary and that
 our minds are chaotic and unwieldy,
 reminding ourselves that we are not something special can
 be quite useful.
 It's useful to come down to earth and to not hold yourself
 up over others or to think of yourself as something special
.
 If you want to become a Buddha, you have to let go of that.
 If you want to become enlightened, you also have to let go
 of that.
 But on the other side, it's important to be encouraged and
 it's important to remember the greatness of the Buddhist
 teaching,
 of the practice, the greatness of mindfulness, the
 greatness of purity of mind,
 the greatness of greatness, the greatness of goodness.
 And so we talk about what is the difference between a
 thoroughbred and just an ordinary human being,
 an untrainable sort of, if you relate it to animals.
 You have these animals that are thoroughbred, like a horse
 or a dog.
 I don't have much familiarity with elephants, thoroughbred
 elephants, but purebred elephants.
 Purebred dogs are somewhat exceptional. They have an
 intelligence that's, I guess they say, bring bread into
 them,
 but you could also think of it as something to do with the
 human connection and how humans are born as animals and
 animals are born as humans.
 They have a strong connection to being human or to just
 intelligence or high-mindedness.
 And so they're very easy to train.
 And so we make the simile, the analogy with human beings,
 comparison to human beings.
 What sort of human being is easy to train? What sort of a
 human being, when they put their mind to it, can achieve
 greatness?
 And the Buddha talked about, in particular, in relation to
 the path, he talked about a horse,
 a horse that knows the way to go, a horse that is
 responsive.
 So there are different kinds of horse and different levels
 of capacity of horses.
 The rider pulls up the goat, the stick, and they just have
 to show it in the right direction, and the horse knows the
 horse knows to go.
 The second type of horse, you actually have to touch them.
 That would be a very special horse. We didn't even have to
 touch them.
 They knew right away, go left, go right, go faster, go
 slower.
 But the second one, you have to touch them. And when they
 feel the touch, not a painful touch, but just a touch,
 and they know which way to go, to speed up, to slow down.
 Third type of horse, you actually have to make it hurt. It
 doesn't hurt, the horse won't go.
 I don't know, it sounds kind of cruel. I'm not giving
 directions on how to deal with horses.
 I think probably horse riding has some bad karma associated
 with it.
 With similes you can talk about these sorts of things, like
 hunters, and so on, that sort of thing.
 But so, you hit it until it hurts. When it hurts, then the
 horse knows to go.
 The fourth type of horse, you really have to get to the
 bone.
 You really have to like, really make it injured, or bruised
 even.
 You bruise it, if you make it really hurt, then the horse
 will go.
 Go this way, go that way, and so on.
 I hope I'm not getting these wrong, it's been a long time,
 but I think that's the fourth one, you really have to hit
 to the bone, I think.
 Yeah, that's what it is.
 Of course, the fifth type of horse, you can hit them until
 they die, and you hit them until your goat breaks, or the
 stick breaks,
 or the horse falls down, and it will never go the right way
.
 So that rather violent list of horses, how does that relate
 to humans, how does that relate to us, to meditators?
 It's nothing like the, I'm not going to hit anyone with a
 stick, this isn't that sort of teaching.
 No, the stick here is death and suffering.
 Some people, just when they hear about it, if you remember
 the Buddha, the bodhisattva, when he heard about,
 well actually that's not true, but they had never heard of,
 the legend goes that he had never heard of old age sickness
 or death.
 But the first type of person anyway, just when they hear
 about it, they actually tried to not let him know about it,
 that's how the legend goes, I don't know if it's actually
 true, but that's what they say.
 But when you hear about it, hear about people dying, like
 in a tsunami or an earthquake,
 or you hear about murders, these mass murders now that we
 hear about people just going in and shooting people,
 because they're different colored skin, or because they're
 gay, or because they're different religion, or whatever.
 And you hear about that, you hear about, not about the evil
 of the killing, but about the death and the suffering,
 and you think, wow, that really is the nature of life, that
's in the realm of possibility.
 I am not at all prepared to deal with such reality, which
 means I'm out of touch with reality, I am not safe,
 I am not where I should be, I have not attained what needs
 to be attained.
 Some people think like this, and they really get a sense of
 urgency and work and become enlightened
 and learn the way to free themselves from the danger of old
 age sickness and death.
 I'm suffering just from hearing about it. That's like the
 horse that you don't even have to hit them with it.
 It hasn't hit them at all.
 The second type of person they have to see someone, or they
 have to actually come in contact with someone
 who is suffering and sick and old or dying or dead, they
 have to actually see it, or come in contact with it in some
 way.
 And when they do, then they realize this could happen to me
, and it's just this is the nature of reality.
 Suffering is a part of life. Here I have been negligent,
 engaging in mindless, meaningless pleasures and so on.
 The third type of person, it has to be someone close to
 them. When their parents die, or relatives or friends or
 loved ones,
 when they get old, sick and die, then they see, like I said
 before, it's like the armies of Mara have attacked your
 fortress
 and the walls are crumbling, it's like you see the people
 dying around you.
 It's like an army has Mahasthaya Nida Matruna, who have
 been attacked by the great armies of Mara, the great armies
 of death.
 And the fourth type of person, well, they don't get it even
 when people around them are dying.
 They have to themselves be old, sick or dying, have to be
 in great pain or suffering, and then they realize it, then
 they realize something needs to be done.
 So it's quite desperate at that point, but they are still
 considered to be trainable, right?
 Because they realize that something needs to be done, then
 they go and do it.
 They take up the practice of training their mind, purifying
 their mind, cleansing their mind of all the clinging and
 attachment that might cause them suffering.
 And they're able to free themselves.
 The fifth type of human being, what's that one? Well, they
 get old, sick and die, and even on their deathbed, they're
 still drinking and smoking or cursing people or clinging to
 things,
 still up into their last moment, their last breath.
 They die in a bad way. Even when they're dying, they don't
 realize.
 And part of it is just our culture, you know, we're taught
 ways to be.
 If you had only one day to live, what would you do? Go on a
 party, you know?
 Do all these skydive, you know? Go traveling, see things,
 see all the things outside of yourself,
 none of which will prepare you and make you better equipped
 to deal with this part of life that is death.
 None of it will lead to greater things.
 So someone who does, someone who takes up the practice, for
 one of these reasons, is considered to be a thoroughbred,
 purebred.
 It's a sign of greatness. So it's something to be proud of
 and encouraged by.
 Just not to let it go to your head, I think.
 The other reminder that we're not Buddha, that we're just,
 we're actually rejects at this point, is quite humbling.
 If we had been great people, we would have probably become
 enlightened when the Buddha was here, but we were around
 back then, probably humans,
 maybe animals, who knows, we could have been anywhere, but
 we missed it, we missed our opportunity and here we are.
 We're like the beggars outside of the palace walls taking
 scraps, 2500 years later.
 And the other part of that is how great it is, right?
 So the greatness and the benefit and the happiness, the
 Buddha, sukham, sukhamedati, leads to great happiness when
 there is such a person.
 It's happiness for their family, happiness for their
 society.
 I think this is something that is a good reminder and would
 be a great thing for people to realize, for families to
 realize,
 when they are hard on their children or pushing their
 children into making money or finding worldly success or
 stability,
 getting good grades and how they are disappointed in their
 kids if they don't do well in school or if they change
 their religion or if they happen to be homosexual or if
 they,
 any number of things, that transgender is a big thing now,
 many much hate there.
 How disappointed people are because of things like that.
 Anyway, I'm not getting into all the many types of
 disappointment that parents have and all of the
 expectations for worldly success and stability and rect
itude,
 moral rectitude in various ways, religion and sexuality and
 so on.
 All of these things that lead parents to denounce their
 children and throw them out.
 And all of the things that parents look for in their
 children and push their children into, getting a good job
 and making money and getting married.
 For many cultures, marriage is a very important thing. You
 must get married, that's a measure of success.
 And if only people realized what was a true greatness and
 what would really be a truly great thing for your children
 to do would be for them to practice and free themselves
 from suffering,
 to purify their minds and to train themselves, to realize
 that all of that useless, meaningless stuff is useless and
 meaningless.
 That is of no consequence. And all the energy people put
 into pushing their children and forcing their children to
 find success is all wasted and misguided.
 And there's so much greatness that comes not from money or
 worldly success but from a child or a person who follows
 the path and finds their own way to free themselves from
 all clinging and craving and anger and conceit and ego and
 so on.
 How much greatness comes not just to them but to their
 family and to their society.
 They become a morally upstanding citizen, they become
 someone who is able to give advice, someone who guides
 others and supports others in their own spiritual practice,
 someone who is harmless,
 who is not cruel or unkind, someone who is generous and
 wise. It's such a great thing to have in society that this
 is just a reminder of what are the important things in life
 and what are the important qualities that we should aspire
 for in ourselves and aspire for in the people around us.
 Hard to find, but wherever you do find such a person, such
 a wise person, great happiness comes to everyone.
 So that's the verse. That's the Dhammapada for tonight.
 Thank you all for listening.
