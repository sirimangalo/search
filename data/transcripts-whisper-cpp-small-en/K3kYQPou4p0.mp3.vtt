WEBVTT

00:00:00.000 --> 00:00:05.360
 Okay, next question comes from Minor Players.

00:00:05.360 --> 00:00:10.560
 "Can you help me better understand karma and reincarnation?

00:00:10.560 --> 00:00:14.360
 What helped you when you first began your studies?

00:00:14.360 --> 00:00:17.830
 I know you have mentioned meditation as a tool, but so far

00:00:17.830 --> 00:00:20.200
 no insights, for me at least.

00:00:20.200 --> 00:00:22.560
 Perhaps a suggested reading authored by a Westerner."

00:00:22.560 --> 00:00:24.120
 No.

00:00:24.120 --> 00:00:25.320
 Meditate.

00:00:25.320 --> 00:00:29.960
 Meditate, meditate, meditate, meditate.

00:00:29.960 --> 00:00:32.640
 Okay.

00:00:32.640 --> 00:00:36.920
 Meditation does help you to understand both karma and

00:00:36.920 --> 00:00:39.240
 rebirth, but I think the problem

00:00:39.240 --> 00:00:42.120
 in most cases is a misunderstanding of karma and a

00:00:42.120 --> 00:00:44.160
 misunderstanding of rebirth.

00:00:44.160 --> 00:00:48.650
 We think of it like we think of everything as some entity

00:00:48.650 --> 00:00:52.080
 that clearly says karma, cause,

00:00:52.080 --> 00:00:58.680
 effect, and we think of rebirth in terms of this life, past

00:00:58.680 --> 00:00:59.400
 life.

00:00:59.400 --> 00:01:05.360
 How meditation helps you to understand karma is because

00:01:05.360 --> 00:01:07.720
 everything we do is karmic.

00:01:07.720 --> 00:01:11.140
 Every moment of our existence is karmic, is cause and

00:01:11.140 --> 00:01:11.920
 effect.

00:01:11.920 --> 00:01:14.800
 We want to walk, therefore we walk.

00:01:14.800 --> 00:01:16.840
 When you're doing walking meditation you can see this.

00:01:16.840 --> 00:01:19.260
 You can see that first there comes the desire to move the

00:01:19.260 --> 00:01:20.880
 foot, then there comes the moving

00:01:20.880 --> 00:01:24.640
 of the foot.

00:01:24.640 --> 00:01:28.320
 And even just a few days of meditating you should be able

00:01:28.320 --> 00:01:30.320
 to see that when you give rise

00:01:30.320 --> 00:01:33.830
 to something karmic, say anger or greed, then there's an

00:01:33.830 --> 00:01:35.240
 immediate result.

00:01:35.240 --> 00:01:39.020
 And this is where all of our feelings of guilt and you

00:01:39.020 --> 00:01:42.160
 could say stress and worry and suffering

00:01:42.160 --> 00:01:43.800
 and addiction come from.

00:01:43.800 --> 00:01:46.440
 All of these things are caused by karma.

00:01:46.440 --> 00:01:49.180
 The fact that we don't get what we want is caused by our

00:01:49.180 --> 00:01:49.920
 wanting.

00:01:49.920 --> 00:01:52.440
 That's karma.

00:01:52.440 --> 00:01:56.010
 And the idea of, you know, I kill you, then you come back

00:01:56.010 --> 00:01:57.840
 and kill me is really just an

00:01:57.840 --> 00:02:01.720
 extrapolation of that.

00:02:01.720 --> 00:02:06.880
 And it's not anything special or mysterious.

00:02:06.880 --> 00:02:10.330
 It's simply the ripples that we send out into the universe

00:02:10.330 --> 00:02:12.240
 changing the nature of our mind

00:02:12.240 --> 00:02:14.840
 and the universe around us.

00:02:14.840 --> 00:02:20.690
 How rebirth works is, as I've said in one of my reality

00:02:20.690 --> 00:02:24.360
 videos, it's not that Buddhists

00:02:24.360 --> 00:02:27.680
 believe in rebirth, we just don't believe in death.

00:02:27.680 --> 00:02:30.800
 And that's because there's no reason to believe in death

00:02:30.800 --> 00:02:33.280
 except for someone who has no clinging,

00:02:33.280 --> 00:02:35.960
 but that's another issue.

00:02:35.960 --> 00:02:41.950
 For most of us, rebirth is a continuation of birth, of

00:02:41.950 --> 00:02:45.280
 existence, of the arising of

00:02:45.280 --> 00:02:47.480
 phenomena.

00:02:47.480 --> 00:02:51.720
 Things arise because of a cause.

00:02:51.720 --> 00:02:55.040
 When we get angry a lot, we create the world around us.

00:02:55.040 --> 00:02:58.080
 When we want a lot, we build things.

00:02:58.080 --> 00:03:05.620
 We get involved in projects and work and we open a store or

00:03:05.620 --> 00:03:08.760
 we start a business.

00:03:08.760 --> 00:03:14.040
 We study things, we develop ourselves, we create existence.

00:03:14.040 --> 00:03:15.640
 And we change the universe around us.

00:03:15.640 --> 00:03:20.150
 Death is sort of like a wave in that, a ripple in that,

00:03:20.150 --> 00:03:22.960
 when there's the building up in this

00:03:22.960 --> 00:03:25.600
 life and the crashing down and the building up and the

00:03:25.600 --> 00:03:26.560
 crashing down.

00:03:26.560 --> 00:03:30.970
 But it's a continuation and our mind continues from one

00:03:30.970 --> 00:03:32.760
 moment to the next.

00:03:32.760 --> 00:03:36.170
 It's not a belief in anything, neither karma nor rebirth is

00:03:36.170 --> 00:03:37.720
 a belief in something.

00:03:37.720 --> 00:03:45.450
 It's a giving up of all of our ideas and beliefs of the

00:03:45.450 --> 00:03:49.600
 termination of life and of luck and

00:03:49.600 --> 00:03:52.740
 of chance and of magic and so on for a very simple

00:03:52.740 --> 00:03:54.760
 understanding of reality that things

00:03:54.760 --> 00:03:59.080
 arise in sequence and when this arises, that arises.

00:03:59.080 --> 00:04:01.760
 With the arising of this, there's arising of that.

00:04:01.760 --> 00:04:03.300
 When this ceases, that ceases.

00:04:03.300 --> 00:04:06.580
 With the non arising of this, there's the non arising of

00:04:06.580 --> 00:04:07.120
 that.

00:04:07.120 --> 00:04:11.360
 You can't have suffering without defilement.

00:04:11.360 --> 00:04:15.540
 If the mind is free from defilement, then there arises no

00:04:15.540 --> 00:04:16.680
 suffering.

00:04:16.680 --> 00:04:22.440
 There arises no result.

00:04:22.440 --> 00:04:25.460
 You can't not get what you want when you have no wanting,

00:04:25.460 --> 00:04:26.360
 for example.

00:04:26.360 --> 00:04:31.540
 So karma is the giving rise to the wanting and the result

00:04:31.540 --> 00:04:33.640
 that comes from it.

00:04:33.640 --> 00:04:36.270
 And I think probably you're already seeing that in

00:04:36.270 --> 00:04:38.120
 meditation if you are meditating.

00:04:38.120 --> 00:04:42.020
 You may just be looking for some insight based on a concept

00:04:42.020 --> 00:04:44.400
 or an idea of karma and rebirth,

00:04:44.400 --> 00:04:45.960
 which I would do away with.

00:04:45.960 --> 00:04:50.480
 I would say you can ask yourself, "Do you think doing bad

00:04:50.480 --> 00:04:52.400
 deeds has a good result?"

00:04:52.400 --> 00:04:56.030
 If the answer is yes, then okay, you're still lacking in

00:04:56.030 --> 00:04:57.840
 meditation experience, the experience

00:04:57.840 --> 00:04:58.840
 of reality.

00:04:58.840 --> 00:05:01.850
 You ask yourself, "Do you think a good deed has a bad

00:05:01.850 --> 00:05:02.560
 result?"

00:05:02.560 --> 00:05:07.260
 If the answer is still yes, then you're still lacking in

00:05:07.260 --> 00:05:09.440
 meditation practice.

00:05:09.440 --> 00:05:12.250
 And I would say continue in the meditation practice until

00:05:12.250 --> 00:05:13.720
 you can see that actually good

00:05:13.720 --> 00:05:19.920
 deeds, meaning good states of mind, lead to good actions.

00:05:19.920 --> 00:05:24.030
 Good actions lead to good results, lead to happiness, peace

00:05:24.030 --> 00:05:26.080
, harmony, and freedom from

00:05:26.080 --> 00:05:27.080
 suffering.

00:05:27.080 --> 00:05:30.270
 Bad deeds, on the other hand, bad mind states of greed, of

00:05:30.270 --> 00:05:32.160
 anger, of delusion, conceit,

00:05:32.160 --> 00:05:37.800
 arrogance and so on, lead to bad deeds which lead in turn

00:05:37.800 --> 00:05:42.040
 to bad results, suffering, conflict,

00:05:42.040 --> 00:05:48.720
 friction, entanglement, and further and further becoming.

00:05:48.720 --> 00:05:52.200
 I hope that that kind of answered your question.

00:05:52.200 --> 00:05:55.800
 I would say don't worry so much about theory and certainly

00:05:55.800 --> 00:05:59.720
 don't read books about the subject.

00:05:59.720 --> 00:06:01.360
 I would practice meditation.

00:06:01.360 --> 00:06:04.680
 Okay, okay, maybe I'll give you a couple of links in the

00:06:04.680 --> 00:06:05.400
 answer.

00:06:05.400 --> 00:06:07.860
 Check out the answer to your question, maybe the

00:06:07.860 --> 00:06:10.000
 description to this video as well, and

00:06:10.000 --> 00:06:13.890
 I'll try to give a couple of links just to give an overview

00:06:13.890 --> 00:06:15.280
 of karma anyway.

00:06:15.280 --> 00:06:19.440
 Death I think is, unless you're going to practice med

00:06:19.440 --> 00:06:22.520
itations that allow you to remember things

00:06:22.520 --> 00:06:27.920
 that far in the past, I would say just understand that we

00:06:27.920 --> 00:06:30.560
 don't believe in death.

00:06:30.560 --> 00:06:32.640
 That's the understanding of rebirth.

00:06:32.640 --> 00:06:36.080
 There's no reason to think of death because our experience,

00:06:36.080 --> 00:06:37.800
 especially in meditation,

00:06:37.800 --> 00:06:41.270
 is one of continued arising and that's all that happens at

00:06:41.270 --> 00:06:43.160
 the moment of physical death.

00:06:43.160 --> 00:06:46.240
 It continues.

