1
00:00:00,000 --> 00:00:03,630
 A question, to extend the question about sleeping, I get

2
00:00:03,630 --> 00:00:04,880
 tired when meditating.

3
00:00:04,880 --> 00:00:10,500
 Is it better to sleep when I'm not sleepy than meditate?

4
00:00:10,500 --> 00:00:12,880
 Sleepiness is actually a great object of meditation.

5
00:00:12,880 --> 00:00:18,190
 It's something that is overlooked, I think, by many medit

6
00:00:18,190 --> 00:00:19,120
ators.

7
00:00:19,120 --> 00:00:23,250
 Yeah, sometimes you have to just fall asleep, but the best

8
00:00:23,250 --> 00:00:25,860
 thing is for that to come naturally.

9
00:00:25,860 --> 00:00:28,380
 If you fall asleep, you fall asleep.

10
00:00:28,380 --> 00:00:31,890
 When you're sleeping, first before you go to sleep, try to

11
00:00:31,890 --> 00:00:33,760
 focus on the sleepiness,

12
00:00:33,760 --> 00:00:38,240
 focus on the tired feeling.

13
00:00:38,240 --> 00:00:40,730
 It's difficult in daily life when you're not in a

14
00:00:40,730 --> 00:00:42,840
 meditation center because you really

15
00:00:42,840 --> 00:00:47,920
 are exhausted a lot of the time from work and just thinking

16
00:00:47,920 --> 00:00:50,800
 a lot and so much activity.

17
00:00:50,800 --> 00:00:53,430
 You can try it, focus on the tiredness, and you'll find

18
00:00:53,430 --> 00:00:56,200
 that sometimes it gives you energy.

19
00:00:56,200 --> 00:00:59,110
 Sometimes you're able to burst the bubble, so to speak, and

20
00:00:59,110 --> 00:01:00,320
 it just disappears.

21
00:01:00,320 --> 00:01:03,160
 Suddenly you're not tired anymore.

22
00:01:03,160 --> 00:01:05,320
 And you'll find you have suddenly a lot of energy and you

23
00:01:05,320 --> 00:01:06,440
're able to do a really good

24
00:01:06,440 --> 00:01:08,440
 meditation center.

25
00:01:08,440 --> 00:01:09,440
 Drowsiness is a hindrance.

26
00:01:09,440 --> 00:01:12,320
 It's something that drags you down and stops you from med

27
00:01:12,320 --> 00:01:13,040
itating.

28
00:01:13,040 --> 00:01:16,850
 If you're able to pierce it and overcome it, you'll find

29
00:01:16,850 --> 00:01:18,640
 that suddenly your mind is clear

30
00:01:18,640 --> 00:01:22,870
 and your practice goes a lot better and meditation can be

31
00:01:22,870 --> 00:01:25,600
 quite fruitful after that point.

32
00:01:25,600 --> 00:01:32,130
 So we should be careful not to forget about the state of

33
00:01:32,130 --> 00:01:36,960
 mind that is fatigue or drowsiness.

34
00:01:36,960 --> 00:01:39,560
 We shouldn't forget that it's a part of the meditation.

35
00:01:39,560 --> 00:01:43,720
 When it comes up, it's a valid object of observation.

36
00:01:43,720 --> 00:01:48,520
 It's a good one too.

37
00:01:48,520 --> 00:01:49,720
 There's two ways.

38
00:01:49,720 --> 00:01:52,570
 One way is you'll get a lot of energy by focusing on it, by

39
00:01:52,570 --> 00:01:54,880
 acknowledging it, for example, tired,

40
00:01:54,880 --> 00:01:58,400
 and the other is you'll just fall asleep.

41
00:01:58,400 --> 00:02:01,940
 If you're really good, it'll be one or the other, and

42
00:02:01,940 --> 00:02:03,600
 neither is a problem.

43
00:02:03,600 --> 00:02:06,560
 The problem with sleeping first and then meditating is that

44
00:02:06,560 --> 00:02:09,120
 sleep drains your awareness and your

45
00:02:09,120 --> 00:02:10,120
 mindfulness.

46
00:02:10,120 --> 00:02:12,460
 We try to have meditators sleep less and less, and

47
00:02:12,460 --> 00:02:14,520
 sometimes they don't even sleep at all.

48
00:02:14,520 --> 00:02:19,840
 They'll just do meditation.

49
00:02:19,840 --> 00:02:22,000
 Sleep is an uncontrolled state.

50
00:02:22,000 --> 00:02:24,160
 It's actually just a construct.

51
00:02:24,160 --> 00:02:26,760
 Sleep doesn't have any ultimate reality.

52
00:02:26,760 --> 00:02:29,520
 We think of sleep as being a necessary part of reality, but

53
00:02:29,520 --> 00:02:31,920
 it's only a conditioned state

54
00:02:31,920 --> 00:02:38,800
 that we've developed as human beings or as I suppose most

55
00:02:38,800 --> 00:02:42,240
 animals have developed.

56
00:02:42,240 --> 00:02:44,240
 It's not a part of ultimate reality.

57
00:02:44,240 --> 00:02:47,400
 It's nothing intrinsic in the mind.

58
00:02:47,400 --> 00:02:56,300
 It's just an aspect of this period of our existence that we

59
00:02:56,300 --> 00:02:59,640
 have a very coarse body and it falls

60
00:02:59,640 --> 00:03:07,160
 asleep sometimes.

61
00:03:07,160 --> 00:03:10,200
 During the time that we're asleep, the mind is in a

62
00:03:10,200 --> 00:03:13,120
 specific state that's not very wholesome.

63
00:03:13,120 --> 00:03:19,600
 It's very free to do what it wants and to make assumptions

64
00:03:19,600 --> 00:03:22,280
 and conclusions.

65
00:03:22,280 --> 00:03:25,800
 People will commit dastardly deeds in their dreams that can

66
00:03:25,800 --> 00:03:26,480
 happen.

67
00:03:26,480 --> 00:03:29,960
 You can do all sorts of bad things.

68
00:03:29,960 --> 00:03:33,250
 You can do good things, but sometimes you fight and kill

69
00:03:33,250 --> 00:03:34,760
 and so on depending on the

70
00:03:34,760 --> 00:03:39,040
 state of mind.

71
00:03:39,040 --> 00:03:43,840
 It usually develops unwholesomeness when you sleep.

72
00:03:43,840 --> 00:03:45,840
 Most people develop unwholesomeness.

73
00:03:45,840 --> 00:03:48,930
 When they wake up, the mind is not fresh, the mind is not

74
00:03:48,930 --> 00:03:49,480
 clear.

75
00:03:49,480 --> 00:03:51,920
 It's clouded and diluted.

76
00:03:51,920 --> 00:03:52,920
 You can feel that.

77
00:03:52,920 --> 00:03:55,350
 If you're in a meditation center, you can feel that after

78
00:03:55,350 --> 00:03:57,520
 you sleep you don't feel better.

79
00:03:57,520 --> 00:04:04,840
 You feel actually less sharp and less clear.

80
00:04:04,840 --> 00:04:06,000
 You have to sleep.

81
00:04:06,000 --> 00:04:09,640
 As human beings, we do have to sleep.

82
00:04:09,640 --> 00:04:12,370
 That's the important point here, to try to sleep mindfully

83
00:04:12,370 --> 00:04:13,200
 before you sleep.

84
00:04:13,200 --> 00:04:16,590
 Do meditation, whether it's lying meditation, saying to

85
00:04:16,590 --> 00:04:18,720
 yourself, "Rising, falling," or

86
00:04:18,720 --> 00:04:22,760
 whether it's some sitting meditation before you lie down.

87
00:04:22,760 --> 00:04:26,840
 That'll help your sleep to be a lot more quiet and a lot

88
00:04:26,840 --> 00:04:27,840
 clearer.

89
00:04:27,840 --> 00:04:29,600
 You find you don't dream so much.

90
00:04:29,600 --> 00:04:33,660
 Your mind is not so distracted when you sleep and you wake

91
00:04:33,660 --> 00:04:35,600
 up a lot more refreshed.

92
00:04:35,600 --> 00:04:40,420
 You can actually find that sleep is much more beneficial

93
00:04:40,420 --> 00:04:42,760
 and it's not as much of a hindrance

94
00:04:42,760 --> 00:04:43,560
 to your meditation.

95
00:04:43,560 --> 00:04:44,560
 Thank you.

