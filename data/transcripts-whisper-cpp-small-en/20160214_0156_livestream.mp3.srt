1
00:00:00,000 --> 00:00:17,000
 [ Silence ]

2
00:00:17,000 --> 00:00:19,740
 >> Okay. Good evening, everyone.

3
00:00:19,740 --> 00:00:27,400
 [ Silence ]

4
00:00:27,900 --> 00:00:34,900
 I'm broadcasting live February 13th.

5
00:00:34,900 --> 00:00:40,400
 [ Silence ]

6
00:00:40,400 --> 00:00:41,400
 Oops.

7
00:00:41,400 --> 00:00:53,900
 [ Silence ]

8
00:00:53,900 --> 00:00:54,900
 Sorry.

9
00:00:55,400 --> 00:00:59,400
 [ Silence ]

10
00:00:59,400 --> 00:01:03,900
 Okay. So I'm going to post this Hangout.

11
00:01:03,900 --> 00:01:06,900
 Actually, that's not.

12
00:01:06,900 --> 00:01:07,900
 We'll post it later.

13
00:01:07,900 --> 00:01:17,400
 But joining the Hangout, the idea was for people who,

14
00:01:17,400 --> 00:01:19,900
 if you have questions.

15
00:01:24,400 --> 00:01:27,840
 If you have a question, you can join the Hangout and ask it

16
00:01:27,840 --> 00:01:28,400
.

17
00:01:28,400 --> 00:01:38,400
 I got someone sent me a note saying, complaining about,

18
00:01:38,400 --> 00:01:47,400
 somehow being dismissive of Larry, I think, last night.

19
00:01:47,400 --> 00:01:53,400
 So I apologize if that was the case, but probably was,

20
00:01:53,900 --> 00:01:58,900
 in some way, the case because I was kind of hoping to

21
00:01:58,900 --> 00:02:01,900
 encourage,

22
00:02:01,900 --> 00:02:07,900
 not like a panel like we had set up in the past,

23
00:02:07,900 --> 00:02:09,900
 but just people asking questions.

24
00:02:09,900 --> 00:02:15,900
 So if you don't have a question, the idea is come on the

25
00:02:15,900 --> 00:02:15,900
 Hangout

26
00:02:15,900 --> 00:02:16,900
 for a question.

27
00:02:16,900 --> 00:02:18,900
 If you don't have a question, just listen at home.

28
00:02:19,400 --> 00:02:27,400
 It doesn't have to be authoritarian or anything,

29
00:02:27,400 --> 00:02:35,400
 but I think it's better to just keep that for questions.

30
00:02:35,400 --> 00:02:47,120
 And I apologize if it seems somewhat cold or harsh or

31
00:02:47,120 --> 00:02:48,400
 whatever.

32
00:02:48,400 --> 00:03:00,000
 What's the thing when you teach, when you put yourself out

33
00:03:00,000 --> 00:03:00,400
 there,

34
00:03:00,400 --> 00:03:05,400
 you open yourself up for criticism.

35
00:03:05,400 --> 00:03:09,080
 It's a reason why a lot of people, I think, are afraid of

36
00:03:09,080 --> 00:03:09,400
 teaching,

37
00:03:09,400 --> 00:03:17,400
 are afraid of putting themselves out there in a thick skin.

38
00:03:17,400 --> 00:03:23,120
 You also get good feedback, positive feedback, but you get

39
00:03:23,120 --> 00:03:24,400
 negative feedback,

40
00:03:24,400 --> 00:03:29,090
 and you'll make mistakes, and people will call you out on

41
00:03:29,090 --> 00:03:29,400
 them.

42
00:03:29,400 --> 00:03:33,920
 If you hide away in your room, you don't have to face the

43
00:03:33,920 --> 00:03:35,400
 criticism.

44
00:03:35,400 --> 00:03:42,400
 So tonight we have a quote about gold.

45
00:03:42,900 --> 00:03:52,900
 Gold is of two kinds, unrefined and refined.

46
00:03:52,900 --> 00:03:59,110
 So we come into the meditation unrefined, like unrefined

47
00:03:59,110 --> 00:03:59,900
 gold,

48
00:03:59,900 --> 00:04:04,400
 with all sorts of defilements.

49
00:04:04,900 --> 00:04:13,400
 It has iron, copper, tin, lead, and silver.

50
00:04:13,400 --> 00:04:17,040
 These are five other types of metal that are mixed in with

51
00:04:17,040 --> 00:04:17,400
 gold

52
00:04:17,400 --> 00:04:24,870
 and keep it from being pure, pliant, and keep it from being

53
00:04:24,870 --> 00:04:26,400
 workable.

54
00:04:26,900 --> 00:04:35,450
 You can't do much with ordinary gold, or you need to purify

55
00:04:35,450 --> 00:04:36,400
 it.

56
00:04:36,400 --> 00:04:48,400
 In the same way, the mind is defiled by five impurities,

57
00:04:48,400 --> 00:04:54,400
 and these are the five hindrances.

58
00:04:54,900 --> 00:05:02,640
 When I first started meditating, there was this British ex-

59
00:05:02,640 --> 00:05:06,400
meditator,

60
00:05:06,400 --> 00:05:12,660
 who some years before that made a cartoon of the five hindr

61
00:05:12,660 --> 00:05:13,400
ances.

62
00:05:13,400 --> 00:05:16,400
 When I first got there, they were handing out these pages

63
00:05:16,400 --> 00:05:19,730
 with the cartoons of the five hindrances and cartoons of

64
00:05:19,730 --> 00:05:22,400
 the five faculties,

65
00:05:22,900 --> 00:05:31,040
 being confidence, effort, mindfulness, concentration, and

66
00:05:31,040 --> 00:05:32,400
 wisdom.

67
00:05:32,400 --> 00:05:39,400
 They're quite useful cartoons, quite useful to have those

68
00:05:39,400 --> 00:05:43,400
 that have a visual reminder of them.

69
00:05:43,400 --> 00:05:47,880
 I don't know if they still give them out, or if they still

70
00:05:47,880 --> 00:05:49,400
 even have them.

71
00:05:49,900 --> 00:05:53,400
 When I was working there, we used to photocopied them.

72
00:05:53,400 --> 00:06:03,400
 The five hindrances are the first set of dhammas

73
00:06:03,400 --> 00:06:06,400
 that we have to look at in the meditation practice.

74
00:06:06,400 --> 00:06:10,400
 This is under that ketchal category of dhammas,

75
00:06:10,900 --> 00:06:17,510
 which never had a successful explanation of how to

76
00:06:17,510 --> 00:06:20,400
 translate dhamma in that context.

77
00:06:20,400 --> 00:06:23,400
 It seems like it means more than one thing,

78
00:06:23,400 --> 00:06:26,400
 something that you couldn't really translate,

79
00:06:26,400 --> 00:06:32,400
 you just have to understand it as a category of things.

80
00:06:32,400 --> 00:06:38,400
 But the first category is the hindrances,

81
00:06:38,900 --> 00:06:44,400
 which makes it seem like a real meditation teaching.

82
00:06:44,400 --> 00:06:47,400
 The dhammas in the Satipatthana Sutta

83
00:06:47,400 --> 00:06:52,900
 refer to those things that you have to understand or you

84
00:06:52,900 --> 00:06:54,400
 have to know about,

85
00:06:54,400 --> 00:07:01,400
 or that you encounter on your progress, on your path.

86
00:07:01,400 --> 00:07:07,400
 So as you're mindful of reality, the objective reality,

87
00:07:07,900 --> 00:07:11,900
 this is the body and the mind, ordinary reality that's not

88
00:07:11,900 --> 00:07:16,400
 caught up in goodness or evil,

89
00:07:16,400 --> 00:07:19,400
 not ethically charged.

90
00:07:19,400 --> 00:07:22,400
 You're going to react.

91
00:07:22,400 --> 00:07:27,060
 When you experience through the senses, you're going to

92
00:07:27,060 --> 00:07:27,400
 react,

93
00:07:27,400 --> 00:07:30,400
 see things, hear things.

94
00:07:30,400 --> 00:07:33,610
 When you think things, you're going to react to your

95
00:07:33,610 --> 00:07:34,400
 thoughts.

96
00:07:34,900 --> 00:07:39,580
 Physical sensations of pleasure and pain, you're going to

97
00:07:39,580 --> 00:07:40,400
 react.

98
00:07:40,400 --> 00:07:44,400
 And this is the five hindrances.

99
00:07:44,400 --> 00:07:48,400
 So as you're practicing, the five hindrances will come up,

100
00:07:48,400 --> 00:07:51,400
 mainly as reaction, in reaction to experiences.

101
00:07:51,400 --> 00:07:56,150
 They're hindrances because they get in the way of your

102
00:07:56,150 --> 00:07:57,400
 progress.

103
00:07:57,400 --> 00:08:01,400
 You'll like certain things, dislike certain things.

104
00:08:01,900 --> 00:08:05,400
 I'm used to using simplified versions of these.

105
00:08:05,400 --> 00:08:09,400
 The five hindrances, the technical names of them are

106
00:08:09,400 --> 00:08:13,400
 kamachanda, meaning sense desire.

107
00:08:13,400 --> 00:08:15,400
 So he's got them here.

108
00:08:15,400 --> 00:08:18,400
 Sense, desire, ill will.

109
00:08:18,400 --> 00:08:22,400
 And you have sloth and torpor.

110
00:08:22,400 --> 00:08:25,800
 Sloth and torpor are two words that we just don't use

111
00:08:25,800 --> 00:08:26,400
 anymore,

112
00:08:26,400 --> 00:08:29,400
 so it's a shame that we're still using sloth and torpor.

113
00:08:29,900 --> 00:08:32,400
 It's because there's two words.

114
00:08:32,400 --> 00:08:40,960
 Basically laziness, inertia, muddled, unwieldiness of the

115
00:08:40,960 --> 00:08:42,400
 mind.

116
00:08:42,400 --> 00:08:48,400
 And then restlessness and worry, that's a good definition.

117
00:08:48,400 --> 00:08:51,960
 Restlessness and worry are put together, but they're

118
00:08:51,960 --> 00:08:53,400
 actually two different things.

119
00:08:53,400 --> 00:08:56,400
 Restlessness is when the mind is not focused,

120
00:08:56,400 --> 00:08:58,400
 when you're thinking lots of different things.

121
00:08:58,900 --> 00:09:01,400
 Worry is a little bit different.

122
00:09:01,400 --> 00:09:05,400
 Worry is a sort of a fear or anxiety.

123
00:09:05,400 --> 00:09:10,650
 Concern about, I mean the technical descriptions are

124
00:09:10,650 --> 00:09:15,400
 concerned about things that you've done in the past,

125
00:09:15,400 --> 00:09:19,400
 bad deeds that you've done, worried about consequences,

126
00:09:19,400 --> 00:09:21,400
 that kind of thing.

127
00:09:21,400 --> 00:09:28,400
 And tina, no, vichiki-cha which means doubt.

128
00:09:30,400 --> 00:09:33,710
 The five hindrances are one of the first things that we

129
00:09:33,710 --> 00:09:35,900
 teach meditators.

130
00:09:35,900 --> 00:09:37,900
 They're always going to be important.

131
00:09:37,900 --> 00:09:41,330
 They're what you focus on, or they're a big part of your

132
00:09:41,330 --> 00:09:41,900
 focus,

133
00:09:41,900 --> 00:09:45,460
 and they always have to be in your mind from beginning to

134
00:09:45,460 --> 00:09:46,900
 end of the course.

135
00:09:46,900 --> 00:09:51,050
 If you don't understand the five hindrances, if you don't

136
00:09:51,050 --> 00:09:51,900
 get them,

137
00:09:51,900 --> 00:09:54,900
 they'll destroy you in the practice.

138
00:09:55,400 --> 00:09:58,600
 As it gets harder, as it gets more intensive in a

139
00:09:58,600 --> 00:09:59,900
 meditation course,

140
00:09:59,900 --> 00:10:04,520
 not practicing in daily life, of course they come into play

141
00:10:04,520 --> 00:10:04,900
 there,

142
00:10:04,900 --> 00:10:07,870
 but during a meditation course they really make or break

143
00:10:07,870 --> 00:10:08,900
 your practice.

144
00:10:08,900 --> 00:10:11,960
 As soon as you let them get to you and you forget to be

145
00:10:11,960 --> 00:10:12,900
 mindful of them,

146
00:10:12,900 --> 00:10:17,400
 or if you don't work hard to learn how to be mindful of

147
00:10:17,400 --> 00:10:18,900
 them and how to overcome them,

148
00:10:18,900 --> 00:10:21,900
 if you don't become comfortable dealing with them,

149
00:10:22,400 --> 00:10:31,900
 then they'll start to weasel their way into your mind,

150
00:10:31,900 --> 00:10:41,900
 and they'll start eating away at your confidence,

151
00:10:41,900 --> 00:10:45,900
 eating away at your resolve, eating away at your effort,

152
00:10:45,900 --> 00:10:49,900
 and they'll drag you down and they'll make you...

153
00:10:51,400 --> 00:10:56,900
 they'll make you discouraged, and they can cause you to

154
00:10:56,900 --> 00:10:57,900
 fail.

155
00:10:57,900 --> 00:11:01,340
 Which should be really encouraging, because that's all that

156
00:11:01,340 --> 00:11:02,900
 is going to make you fail.

157
00:11:02,900 --> 00:11:06,900
 We say that it gets tough, or it is difficult to meditate,

158
00:11:06,900 --> 00:11:09,900
 or there are times where you feel like you can't meditate,

159
00:11:09,900 --> 00:11:13,900
 or you're just overwhelmed, and you don't know what to do,

160
00:11:13,900 --> 00:11:18,900
 but it's not the situation that makes it impossible.

161
00:11:19,400 --> 00:11:24,110
 There's no situation in the course that makes it impossible

162
00:11:24,110 --> 00:11:24,900
 to meditate.

163
00:11:24,900 --> 00:11:30,670
 It's only your own mind, your own emotions, your own

164
00:11:30,670 --> 00:11:32,900
 reactions to things.

165
00:11:32,900 --> 00:11:38,900
 And so it's actually quite easy to figure out the solution

166
00:11:38,900 --> 00:11:40,900
 to the problems that come up,

167
00:11:40,900 --> 00:11:44,900
 not only actually in meditation, but in our lives.

168
00:11:45,400 --> 00:11:48,900
 It's quite encouraging for figuring out a succeeded life.

169
00:11:48,900 --> 00:11:54,510
 Not easy, but at least you have a clear picture of what you

170
00:11:54,510 --> 00:11:56,900
 need to do.

171
00:11:56,900 --> 00:11:59,900
 And let's just deal with the five hundredths.

172
00:11:59,900 --> 00:12:03,210
 They're really the only things standing between us and

173
00:12:03,210 --> 00:12:04,900
 success in anything,

174
00:12:04,900 --> 00:12:08,750
 because they destroy the minds... they destroy the

175
00:12:08,750 --> 00:12:09,900
 faculties of the mind.

176
00:12:09,900 --> 00:12:13,900
 So they destroy your confidence, they destroy your effort,

177
00:12:14,400 --> 00:12:19,070
 they destroy your mindfulness, your concentration and your

178
00:12:19,070 --> 00:12:19,900
 wisdom.

179
00:12:19,900 --> 00:12:24,290
 They distract you from these things, they keep you from

180
00:12:24,290 --> 00:12:25,900
 developing them.

181
00:12:25,900 --> 00:12:31,900
 They keep them from... these faculties from growing strong,

182
00:12:31,900 --> 00:12:36,900
 from working together to free the mind.

183
00:12:41,400 --> 00:12:46,900
 So for that reason, these are very important dumbness.

184
00:12:46,900 --> 00:12:50,900
 Meditation, in our tradition we simplify them,

185
00:12:50,900 --> 00:12:54,900
 liking, disliking, drowsiness, distraction, doubt.

186
00:12:54,900 --> 00:12:57,600
 I'm the one who came up with those words, I don't know that

187
00:12:57,600 --> 00:12:58,900
 anyone else is using those.

188
00:12:58,900 --> 00:13:02,900
 Why I use them is because it's liking and then four D's,

189
00:13:02,900 --> 00:13:05,900
 liking, disliking, drowsiness, distraction, doubt.

190
00:13:05,900 --> 00:13:08,900
 It's easy to say, it's easy to remember.

191
00:13:09,400 --> 00:13:12,590
 But it's very similar to how a Jandong, my teacher, teaches

192
00:13:12,590 --> 00:13:13,900
 them in Thai.

193
00:13:13,900 --> 00:13:18,280
 가나, 마차, 나, 보, 보, 소살, 차, 마차, 보, �

194
00:13:18,280 --> 00:13:18,900
�, 소살.

195
00:13:18,900 --> 00:13:21,900
 As you see, it gives them very simple words.

196
00:13:21,900 --> 00:13:24,640
 Even if you don't know what those mean, you can hear that

197
00:13:24,640 --> 00:13:25,900
 they're very easy to say.

198
00:13:25,900 --> 00:13:29,900
 가, 마차, 마 means no, and 아 means like.

199
00:13:29,900 --> 00:13:34,900
 아, 마차, 보 is just tired or drowsy.

200
00:13:35,400 --> 00:13:41,900
 보 means 보살, 보살 is distracted or flustered.

201
00:13:41,900 --> 00:13:45,900
 And 소살 is doubt.

202
00:13:45,900 --> 00:13:51,900
 We need these simple words because we're on the front lines

203
00:13:51,900 --> 00:13:51,900
.

204
00:13:51,900 --> 00:13:54,900
 We need to be able to use these as weapons.

205
00:13:54,900 --> 00:13:57,900
 We need to have a weapon to combat these.

206
00:13:57,900 --> 00:14:00,900
 So it's like knowing your enemy.

207
00:14:01,400 --> 00:14:05,900
 We have to know a way of getting to them.

208
00:14:05,900 --> 00:14:10,110
 If you have these words like "central desire, ill will" and

209
00:14:10,110 --> 00:14:11,900
 so on, "sloth and lacing",

210
00:14:11,900 --> 00:14:15,900
 it doesn't really work, practically speaking.

211
00:14:15,900 --> 00:14:20,900
 It's important that we memorize these.

212
00:14:20,900 --> 00:14:27,070
 When people come to practice in 전형, 아장형 will

213
00:14:27,070 --> 00:14:28,900
 remind them of these.

214
00:14:29,400 --> 00:14:31,900
 When it starts with the four foundations of mindfulness,

215
00:14:31,900 --> 00:14:34,630
 you have to memorize the four 새힕형, 가에에, 나지�

216
00:14:34,630 --> 00:14:35,900
��, 나지형.

217
00:14:35,900 --> 00:14:39,190
 And then under 나지형 you have to memorize the five hind

218
00:14:39,190 --> 00:14:39,900
rances.

219
00:14:39,900 --> 00:14:42,900
 전형, 마장형, and it will have you repeat after him.

220
00:14:42,900 --> 00:14:47,640
 전형, 마장형, 마장형, 보아, 보아, 보살, 보�

221
00:14:47,640 --> 00:14:48,900
�, 보살.

222
00:14:48,900 --> 00:14:51,900
 And then you have to say it yourself.

223
00:14:51,900 --> 00:14:56,900
 They're that important.

224
00:14:57,400 --> 00:15:01,900
 As you can see, the Buddha says,

225
00:15:01,900 --> 00:15:05,900
 "When you give up these five debasements,

226
00:15:05,900 --> 00:15:08,900
 these five defilements,

227
00:15:08,900 --> 00:15:18,390
 one can then direct the mind to the realization by psychic

228
00:15:18,390 --> 00:15:18,900
 knowledge

229
00:15:18,900 --> 00:15:21,900
 of whatever can be realized by psychic knowledge.

230
00:15:21,900 --> 00:15:24,900
 You can see it directly, whatever its range might be."

231
00:15:25,400 --> 00:15:27,900
 So when you're talking about psychic powers,

232
00:15:27,900 --> 00:15:30,900
 magical powers, it's possible to gain these

233
00:15:30,900 --> 00:15:33,900
 when your mind is free from hindrance,

234
00:15:33,900 --> 00:15:35,900
 but it's also possible.

235
00:15:35,900 --> 00:15:41,900
 The sixth of the abhinya of the psychic powers

236
00:15:41,900 --> 00:15:46,900
 is the destruction of defilement.

237
00:15:46,900 --> 00:15:50,900
 So the highest psychic power is the power of wisdom

238
00:15:50,900 --> 00:15:53,900
 of overcoming the defilement.

239
00:15:54,400 --> 00:15:56,900
 That's the dhamma for tonight.

240
00:15:56,900 --> 00:16:01,900
 Now I'm going to post the link to the hangout,

241
00:16:01,900 --> 00:16:05,840
 but it's only really if you want to come on and ask

242
00:16:05,840 --> 00:16:06,900
 questions.

243
00:16:06,900 --> 00:16:15,900
 So if you have a question, come on the hangout.

244
00:16:15,900 --> 00:16:18,900
 Don't be shy.

245
00:16:22,400 --> 00:16:24,900
 Come and ask.

246
00:16:24,900 --> 00:16:27,900
 This is our new way of doing things.

247
00:16:27,900 --> 00:16:32,060
 You've got to actually be brave enough to come on and ask

248
00:16:32,060 --> 00:16:32,900
 your question.

249
00:16:32,900 --> 00:16:36,900
 You also need a mic in that case, which I guess is a bit of

250
00:16:36,900 --> 00:16:37,900
 a thing.

251
00:16:37,900 --> 00:16:41,900
 Some people might not have a mic.

252
00:16:41,900 --> 00:16:54,400
 [silence]

253
00:16:54,400 --> 00:16:56,400
 Huh.

254
00:16:56,400 --> 00:16:59,400
 This person has posted this several times.

255
00:16:59,400 --> 00:17:02,400
 I guess I never actually answered it.

256
00:17:02,400 --> 00:17:06,400
 Someone's asking about whether they should

257
00:17:06,400 --> 00:17:10,400
 learn more from normal how-to videos.

258
00:17:10,900 --> 00:17:13,130
 If you're 17 years old, don't worry about the children's

259
00:17:13,130 --> 00:17:13,400
 videos.

260
00:17:13,400 --> 00:17:16,000
 I mean, adults have liked them. Didn't I answer this

261
00:17:16,000 --> 00:17:16,400
 already?

262
00:17:16,400 --> 00:17:23,400
 Adults have found those videos useful as well,

263
00:17:23,400 --> 00:17:25,640
 and they are kind of useful because they simplify

264
00:17:25,640 --> 00:17:26,400
 everything.

265
00:17:26,400 --> 00:17:29,400
 But I recommend my booklet.

266
00:17:29,400 --> 00:17:32,400
 If you haven't read the booklet on how to meditate,

267
00:17:32,400 --> 00:17:35,400
 that's where you should start.

268
00:17:35,900 --> 00:17:42,400
 Meditate's videos are just about non-insight things.

269
00:17:42,400 --> 00:17:54,400
 So, no questions?

270
00:17:54,400 --> 00:17:58,400
 No questions that I'm going to hand out.

271
00:17:58,400 --> 00:18:14,900
 [silence]

272
00:18:14,900 --> 00:18:19,900
 Alright. Have a good night, everyone.

273
00:18:19,900 --> 00:18:22,900
 See you all tomorrow.

274
00:18:23,400 --> 00:18:31,900
 [silence]

275
00:18:31,900 --> 00:18:37,900
 [silence]

