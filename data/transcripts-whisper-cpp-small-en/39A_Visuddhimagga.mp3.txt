 I hope that one day you can come here and teach to us. This
 is really rare.
 Alas, that I probably can only come in to meet at you,
 because my life unfortunately has gotten too busy.
 But it's always a great pleasure for you to come in.
 I'm going to be listening to your tapes, so thank you.
 [Coughing]
 Is he staying?
 He's going in that right now.
 He's going.
 Now, a little about the book, "Wizard of Maga".
 This book is a handbook for meditating monks.
 It was written by a monk for the use of monks who practice
 meditation.
 And this book was written on the scheme of three steps
 towards spiritual development.
 The first step is morality, and second step is
 concentration, and the third step, wisdom.
 So we have gone through chapters on morality, concentration
, and some chapters on wisdom too.
 So the first and second chapters deal with morality, and
 then the other chapters until the thirteenth chapter deal
 with
 what is called concentration.
 So in these chapters, the forty subjects of sub-matter
 meditation or tranquility meditation are treated.
 And then with the fourteenth chapter, we come into the
 realm of what is called understanding of wisdom.
 And these chapters, chapter fourteen to chapter seventeen,
 are in preparation for vipassana meditation.
 So before we practice vipassana meditation, according to
 this book, we should understand
 something about the aggregate, sense basis, faculties,
 noble truths, and the dependent origination.
 So in order to go further from the eighteenth chapter, we
 will have to be familiar with at least the chapters
 from chapter fourteen to chapter seventeen, because though
 the subjects treated in those chapters
 will be brought to bear upon what will be said in these
 chapters.
 And with the eighteenth chapter begins the real vipassana
 meditation.
 So the other chapters, the preceding chapters are just for
 preparation, like giving you information about these aggreg
ates
 and so on, which you will contemplate on, or which you will
 see through the practice of vipassana meditation.
 And vipassana meditation is described with reference to
 what is called purity, different stages of purity.
 And there are seven stages of purity described here.
 If you look at photograph one, then you see the reference
 back to chapter fourteen.
 That there are first two purities, purity of morals and
 purity of mind.
 So purification of virtue and purification of consciousness
.
 Purification of virtue means keeping the precepts, not
 breaking them.
 And purification of consciousness means the practice of sam
atha meditation
 until one gets the neighborhood concentration and also the
 absorption concentration.
 So these are called purification of consciousness.
 Here consciousness really means samadhi concentration.
 And these two purifications are called the roots of, root
 purification, because they are like roots.
 On these two roots the other purifications will be built.
 So the other purifications are what?
 Paragraph two. Purification of view, purification by
 overcoming doubt, purification by knowledge and vision
 of what is part and what is not part, purification by
 knowledge and vision of the way,
 and purification by knowledge and vision.
 They are called trunk purifications.
 So altogether there are seven purifications.
 So the first two purifications are dealt with in the
 chapters from 1 to 13.
 And then from 14 to 17 chapters are in preparation for the
 Vipassana meditation.
 And now this chapter deals with purification of view.
 So this chapter is a description of purification of view.
 Purification of view really means having right view with
 regard to aggregate basis and so on.
 So let's say a person practices Vipassana meditation.
 According, if you follow the order in this book, he will
 first practice samadhi meditation and try to get jhanas.
 Or he may not practice samadhi meditation at all, but go
 right to Vipassana.
 So first the definition defining of mentality, materiality.
 So defining of mentality, materiality is explained in this
 chapter.
 And defining of mentality, materiality really means seeing
 during meditation mind and matter clearly.
 So seeing mind not mixed with matter and matter not mixed
 with mind.
 So seeing mind clearly and matter clearly.
 And that is here called defining of mentality and material
ity.
 So in order to understand, in order to define mentality and
 materiality,
 we need to understand what is mind and what is matter
 according to the Buddha's teachings.
 And they are described in the chapters preceding chapters.
 Chapter 14.
 What's in our mind, consciousness and mental state, our
 mental factors are called mind.
 And matter is, as you know, the physical thing, physical
 properties in our bodies as well as outside things like
 tables, chairs, trees and so on.
 So first, the paragraph 3 and following describe how a
 person whose vehicle is serenity
 or who practices samadhi meditation first define mind and
 matter and matter, mentality and materiality.
 And here, one who wants to accomplish this, if firstly, his
 vehicle is serenity.
 That means he practices samadhi meditation first and gets j
hanas.
 So for him, he should emerge from any fine material or imm
aterial jhanas.
 So you know there are eight jhanas, four material and four
 immaterial jhanas.
 But accepting the base consisting of neither perception nor
 nonperception,
 that is the highest of the immaterial jhanas, it is so
 subtle that it is very difficult for one
 who first defines mind and matter to be able to define
 clearly.
 So it is accepted.
 So we have all the seven jhanas here.
 So emerging from any of the fine material or immaterial j
hanas,
 except the base consisting of neither perception nor nonper
ception,
 and he should discern according to characteristic function,
 etc.
 The jhana factors consisting of applied thought and so on.
 So first he entered into the jhanas he has attained, and
 then he emerges from the jhana.
 And then take the factors of jhana, like initial
 application, sustained application and so on.
 So take the factors of jhana as the object of Vipassana
 meditation,
 or take the other mental factors which accompany the jhana
 as the object of Vipassana meditation,
 and he tries to see their characteristic function,
 manifestation and so on.
 Now whenever we study a thing in Abhidhamma, we are to
 understand it with reference to four or three aspects.
 The first is characteristic, the second is function, and
 the third is manifestation,
 or the mode of manifestation, and the fourth is proximate
 cause.
 So he should contemplate on the factors of jhana or the
 other mental factors
 accompanying the factors of jhana with reference to their
 characteristic function and mode of manifestation.
 When he has done so, all that should be defined as
 mentality in the sense of bending because of its bending
 onto the object.
 So he contemplates on, or he makes himself aware of, these
 jhana factors and other mental states
 and taking maybe one by one and trying to understand the
 characteristic and so on of these mental states.
 And then he defines that this is mentality, this is mind,
 this belongs to mind, this is mental.
 And then later on he will go to the matter.
 So first he defines them as mental, as nama and bali.
 The bali-wat nama, the literal meaning of the bali-wat nama
 is bending, bending towards.
 So nama is that which bends towards the object.
 So when you pay close attention to the object during
 meditation, you will come to be aware that
 mind is hitting the object or going towards the object.
 So you develop on a noise outside and you take the noise as
 an object and then you may have some other thoughts.
 And so your mind is going to that object and then to
 another object and another object and so on.
 So mind is that which bends towards the object and that is
 why it is called nama in bali.
 After defining mentality, he defines materiality or matter.
 So the fourth paragraph describes that.
 So then just as a man by following a snake that he has seen
 in his house finds his abode,
 so this meditator scrutinizes that mentality.
 He seeks to find out what its occurrence is supported by
 and he sees that it is supported by the matter of heart.
 Now according to the teachings in the abitama, every type
 of consciousness must have a physical basis.
 So seeing consciousness has the eye as a physical basis,
 and hearing consciousness has the eye as a physical basis,
 and then there are some other types of consciousness which
 have heart as a physical basis.
 So first he dwells upon the mind and then following the
 mind,
 he discovers or seeks to find out by what this mind is
 supported.
 And then he comes to realize that the heart is the support
 of the factors of jhana
 and then those concomitant with the factors of jhana.
 After that he describes as materiality, the primary
 elements which are the heart support,
 and the remaining derived kinds of materiality that are the
 elements as their support.
 Now there is a heart base, and heart base is a dependent
 type of matter,
 and heart base depends upon the other great primaries.
 You know there are four great primaries or four great
 elements, earth element, water element, fire element and
 air element.
 So the heart base, very small particle of matter, the heart
 base depends upon the four great elements.
 So he discovers these four great elements also,
 and he defines all that as materiality because it is
 smallest steps by coal etc.
 That means it changes by heat, coal, by hunger, by thirst,
 by bites of insects and so on.
 Now this is the literal meaning of the word 'ruba'. You see
 the word 'ruba' in paragraph four.
 So 'ruba' is so called because it is smallest steps or it
 changes.
 It changes with climate, it changes with hunger, it changes
 with thirst and so on.
 So he defines the base of that consciousness as 'ruba', as
 matter.
 So a person whose vehicle is samatha or serenity or tranqu
ility meditation
 defines first the mental things, the factors of jhana and
 its concomitant.
 And then he tries to find the base of these mental states
 and then he finds the matter which is the base of these
 mental states.
 And then he defines it as 'ruba'.
 So during meditation he first dwells on the mental things
 and then from those mental things he goes to material
 things.
 So first he defines a nama and then he defines 'ruba'.
 That is for a person who has serenity meditation as a
 vehicle.
 That means who first practices serenity meditation and gets
 jhana.
 Now there are people who do not practice samatha meditation
 but just practice pure vipassana meditation.
 For them, since they have no jhanas to dwell upon, there is
 another matter.
 So paragraph 5 and following paragraph describe his way of
 contemplating, his way of defining the mentality and
 materiality.
 And also a person whose serenity can follow this method too
 because it is up to him whether to begin with the mentality
 first and materiality later
 or to take materiality first and mentality later.
 But for a person whose vehicle is vipassana, he must begin
 with materiality first and then go to mentality.
 Because materiality or matter is easier to see for him than
 the mental states or types of consciousness.
 So one whose vehicle is pure inside or that same, a faucet.
 That means the person whose vehicle is serenity.
 This sounds the four elements in brief or in detail in one
 of the various ways given in the chapter on the definition
 of the four elements.
 Now you have to go back to chapter 11.
 So this is the method of defining materiality and mentality
.
 It is described in how many ways?
 It is described by way of four elements and then by way of
 eighteen elements and then by way of twelve sense basis, by
 way of aggregates and then by way of very brief defining.
 So different ways are given here. So the first is by way of
 the four great elements.
 Actually the heading which we have on page 679, the
 definition based on the four primaries should be here.
 Should be below paragraph 4.
 So only from there the definition based on the four
 primaries begins.
 And then we have starting with materiality.
 So here he first dwells on the materiality and not on the
 mind first.
 When the elements have become clear in their correct
 essential characteristics firstly in the case of head hair,
 originator, gamma, they have become plain.
 Ten instances of materiality with the body that it does.
 The four elements, color, odor, flavor, nutritive essence
 and life and body sensitivity.
 Now you need to be acquainted with the chapters preceding
 it to fully understand this.
 When describing the material properties, they are described
 as being caused by, we find the causes here, they are
 caused by karma, caused by consciousness, caused by
 temperature and caused by nutrition.
 And then these material properties are treated in groups.
 Say a group of 8, a group of 9, a group of 10 and so on.
 And these groups are actually not found in the text, but in
 the common trees we find these and also in the manual of a
 pyramid, we find material properties treated in groups.
 So according to these groups, the other is here explaining.
 So if you are not familiar with these passages, it may be
 difficult to understand or maybe confusing.
 Here, first, although it is said here that it is according
 to the full grid elements, we must understand that when
 describing the defining of full grid elements, the odor
 described is with reference to 32 parts of the body.
 So we have to go back to 32 parts of the body and remember
 which part is caused by which.
 And so there are 32 parts of the body here.
 Head hairs, body hairs, nails, deep skin and so on, until
 urine, water element.
 Now, in the book it said, head hair originated by gamma.
 Now, head hair is originated by gamma and it is also
 originated by consciousness, originated by temperature and
 originated by nutrition.
 It is caused by all full causes.
 So for the head hair caused by gamma, there become plain 10
 instances of materiality with the body dagger dust.
 The full elements, color, odor, flavor, nutritive essence
 and light and body sensitivity.
 And because the sex dagger is present there too, there are
 another 10.
 That is the same 9 with sex instead of body sensitivity.
 And since they opted with nutritive essence as a long name,
 that is the full elements and color, odor, flavor and
 nutritive essence originated by nutrition, nutriment.
 And that originated by temperature and that originated by
 consciousness are present there too.
 There are another 24.
 So there is a total of 44 instances of materiality in the
 case of each of the 24 bodily parts of full full orig
ination.
 Now, if you look at the causes, the bottom of the page, the
 gorge, dung, pus, urine, they are temperature originated.
 They are caused by temperature, caused by utu.
 And then tears, sweat, spittles, snuts, they are caused by
 temperature and consciousness.
 So they are caused by two causes.
 And fire, they digest.
 There are four kinds of fire, fire element.
 And the last one is the fire that digest what we eat, drink
 and so on.
 So that fire is karma originated.
 If you have a good stomach, that means you had a good karma
.
 If you have a good digestion.
 And then in and out breath.
 In breath and out breath are consciousness originated.
 Because breathing is present only in those beings that have
 consciousness.
 And the rest, the remaining all, are full full origination.
 So they are caused by all four causes.
 And then we take head hair and then since head hair is not
 in the above four groups, it is caused by four full causes.
 And for head hair caused by karma, there are a number of
 material properties.
 And then for head hair caused by consciousness and number
 of properties.
 And then head hair caused by temperature.
 And for head hair caused by new to men, a number of
 material properties.
 And so all together it is identical company.
 Forty four instances of materiality in the case of the
 twenty four bodily parts of full full origination.
 So they are all together forty four.
 But in the case of four memories, red tears, petal and
 start, which are originated by temperature and
 consciousness.
 There are sixteen instances materiality with the two octets
 with nutritive essence as eight in each.
 Now, they are what we call inseparables.
 You may remember that one. Inseparables.
 Those material properties which cannot be physically
 separated.
 There are always these eight.
 Even in the smallest particle of an atom, according to Obit
ama, there are these eight material properties.
 The four great elements, the third element, the fire
 element, the water element, the fire element, the air
 element.
 And then colour, odour, flavour and nutritive essence.
 So these eight are here called octets with nutritive
 essence as eight.
 It's not so long in Bali, but if you translate it into
 English, it becomes a long name.
 Octets with nutritive essence as eight.
 So in Bali it is Ojatamaka. Five syllables.
 So in the case of the four, namely, gorge, tongue, pus and
 urine, which are originated by temperature, eight instances
 of materiality.
 Since they are caused by temperature only, there are only
 eight material properties.
 They complain in each with the octet with nutritive essence
 as eight, the same inseparables.
 And this in the first place is a method in the case of the
 32 bodily aspects.
 But there are ten more aspects that become clear when those
 32 aspects have become clear.
 And as regards to these firstly, nine instances of material
ity, that is the octet with nutritive essence as eight plus
 life,
 becomes plain in the case of karma, bone part of heat that
 digests what is eaten, etc.
 That means for the fourth of the four fire elements, there
 are nine.
 Eight inseparables and then one life principle. So there
 are nine there.
 Eight inseparables plus life principle. We call it life
 principle, jivita.
 And likewise, nine instances of materiality that is octet
 with nutritive essence as eight plus sound.
 Here sound in the case of consciousness, bone part of air
 consisting in breath and outbreath.
 In breath and outbreath, there are nine material properties
.
 Eight inseparables plus sound. And then 33 instances of
 materiality that is karma, bone, life and net.
 And the three octets with nutritive essence as eight in the
 case of each of the remaining eight parts that are of full,
 full origination.
 There are the eight separables plus life principle that is
 called life and net.
 And then three octets with nutritive essence as eight, that
 is the eight inseparables.
 So one, nine and three, eight inseparables, nine plus,
 twenty-four becomes thirty-three.
 If you cannot calm them, don't worry.
 In actual practice, you need not see all these.
 Like mentioned here, it is very difficult.
 Even for those who have studied abhidhamma, it is not so
 easy to see them clearly when they practice meditation.
 They may see just a few of them, not all of them.
 But here everything is mentioned in detail.
 But it does not mean that when you practice vipassana
 meditation, you must see all of these mentioned here.
 So what is important in real practice is to be able to see
 in your mind clearly what you observe.
 Say you are making notes or observing sometimes mental
 things and sometimes material things.
 For example, when you are concentrating on the breath, then
 you are concentrating on the matter.
 Because breath is air and air is matter.
 When you are concentrating on your thoughts, then it is on
 the mind you are dwelling on.
 So you pay attention to them and then you try to see them
 clearly,
 the breath as your thoughts or your mental state as your
 mental state.
 So when you can see them clearly without being mixed with
 any other thing,
 then you are said to have got the knowledge of designing
 mind and matter.
 So you need not go through all these,
 checking the head hair and then trying to find out how many
 material properties are there in connection with that head
 hair and so on.
 But if you can, then it will be good for you to dwell on
 these and see them clearly.
 But as I said, the most important thing is first to see
 them clearly and then to know that mind is the one that
 goes to the object.
 And Rupa, the matter is one that does not cognize.
 So if you see just that, then you are said to have got that
 knowledge of mind and matter,
 a knowledge of designing, a knowledge of discriminating
 mind and matter.
 And this knowledge of designing mind and matter comes not
 at the beginning of your practice.
 Before you get to that stage, you have to get enough
 concentration.
 So in the beginning, what you are doing is trying to get
 concentration.
 So after you get concentration, after your mind can be on
 the object without being disturbed by mental hindrances,
 then you will begin to see things clearly.
 Now sometimes people, meditators think that, "Oh, I can see
 clearly. I don't need too much concentration."
 The moment I sit and watch myself, I can see them clearly.
 But later on when they come to see really clearly, they say
, "Oh, what I said I saw clearly was nothing."
 Now it is real, it is only now that I see clearly something
 like this.
 So if you cannot follow all these things, don't worry.
 So first, he developed the material things in different
 ways.
 So after defining the ruba, defining the matter, he tries
 to define the mind or the mentality.
 So paragraph 8 describes that.
 "Taking all these together under the characteristic of
 being molested, he sees them as materiality.
 When he has discerned materiality thus, the immaterial
 states become plain to him in accordance with the sense
 doors,
 that is to say, the 81 kinds of money, consciousness, and
 so on."
 And here the different types of consciousness are mentioned
.
 And if you remember the chapters on consciousness, you will
 easily understand this.
 And if you don't remember, don't worry.
 So in practice, say sometimes you see something and you are
 aware of that seeing consciousness.
 You hear something and you are aware of that hearing
 consciousness.
 So when you are mindful of seeing consciousness or hearing
 consciousness, then you are seeing mentality.
 And you also, you will not fail to see that the
 consciousness is that which just bends towards the object.
 That is why it is called nama and pala.
 So when watching or when dwelling upon types of
 consciousness, you do not dwell upon the supermandane types
 of consciousness.
 Because supermandane types of consciousness and also mental
 factors and others, whatever is supermandane is not the
 domain of vipassana.
 In vipassana, you take only the mundane things as objects,
 simply because you have not experienced yourself, the super
mandane state.
 And what you have not experienced, you cannot see clearly.
 So in vipassana meditation, we are concerned only with what
 is mundane and not supermandane.
 So the supermamaning types of consciousness are excluded
 from the types of consciousness which are the object of vip
assana meditation.
 So all these types of consciousness and mental states you
 define as mentality.
 So you define materiality first and then you define
 mentality.
 So this is by way of the four great elements with reference
 to actually 42 kinds of elements.
 And then next paragraph 9, the definition based on the 18
 elements.
 Here also you have to understand first the 18 elements
 which are described in the previous chapters.
 And element of the eye, element of ear and so on.
 So with regard to this also the way of defining is, now
 when we see the eye element, we do not mean the eyeball.
 What we mean is the sensitivity, sensitive part in the eye.
 So the part of the eye where the image strikes.
 So that sensitive area or sensitive part of the eye is what
 is called the eye here.
 So the eyeball is not the eye element here.
 So here instead of taking the piece of flesh, very occasion
 with white and black circles,
 having lantern, breath and faucet in the eye socket with
 the string of sinew,
 which the wall turns and eye, he defines as eye element.
 The eye sensitivity of the guy described among the kinds of
 derived materiality in the description of the application.
 But in chapter 14, so he defines that as eye element, not
 the whole eyeball.
 So something in the eye, maybe some place in the retina
 where the image strikes and through which we see things.
 And he does not define as eye element the remaining
 instances of materiality, which total 53 and so on.
 I will not burden you with finding out where the 53 or 43
 or 45.
 And then the next is based on the 12th basis.
 There are 12th sense basis.
 If you understand the 18 elements, then 12th basis also you
 understand.
 It is another way of describing the realities.
 One Buddha taught in different ways and sometimes he taught
 Nama and Rupa in 18 elements,
 and sometimes in 12th basis, and sometimes 5 aggregates and
 so on.
 So following these different ways of treating what we call
 the ultimate reality,
 we have different methods here.
 So we can use any of these methods to contemplate on the
 mind and matter.
 So this paragraph 12 is a definition based on the 12th
 basis.
 Here also, eye, ear base and so on.
 The eye base is the same as the eye element.
 The eye base is the eye base and not the eyeball and others
.
 So here he defines as eye with the sensitivity only,
 leaving out the 53 remaining instances of materiality
 and the way described for the eye element and so on.
 And then the next paragraph is definition based on the
 aggregates.
 Now it is a little brief because you have to deal with only
 5 aggregates instead of 12 or 18 and described earlier.
 So here another defines it more briefly than that by means
 of the aggregates.
 Now you know the 5 aggregates.
 What is the first of the 5 aggregates?
 Ruba.
 Right, yes, Ruba, copper, reality, Ruba.
 So that Ruba is divided into 27 or 28 material properties,
 the 4 primary elements and then 24 depending on it.
 And they are kava, ora and so on.
 Now there are 28 material properties and then among them
 only the first 18 or here 17 are suitable for comprehension
.
 That is suitable for meditating on.
 Although there are 28 material properties, the last 10 are
 not suitable for comprehension, not suitable for meditation
.
 Simply because actually they are just the modes or some
 aspects of the first 18.
 So the first 18 are the real material properties.
 That is why they are called Ruba, Ruba.
 So the real Ruba.
 But in this book only 17 are meant, right?
 17 instances of material that are consisting of the 4
 primary elements and so on.
 That is because it leaves out the heart base.
 So the others are not suitable for contemplation.
 And they are bodily intimations, verbal intimations, the
 space element and the lightness, malleability,
 wilderness, growth, continuity, aging and impermanence of
 materiality.
 So they are mentioned in the chapter on 5 aggregates.
 So if we take material properties as 27 then the first 17.
 If we take as 28 then the first 18 are suitable for
 comprehension and the last 10 are not.
