1
00:00:00,000 --> 00:00:06,360
 Hi, I'm just making a video today to give update on the

2
00:00:06,360 --> 00:00:10,280
 situation with our DVD on

3
00:00:10,280 --> 00:00:14,140
 how to meditate or the double DVD set on how and why to med

4
00:00:14,140 --> 00:00:16,140
itate. Up until

5
00:00:16,140 --> 00:00:19,360
 recently I had to make it clear to people that we were

6
00:00:19,360 --> 00:00:20,120
 trying to recoup

7
00:00:20,120 --> 00:00:23,770
 some of the costs that it took to make the DVD which of

8
00:00:23,770 --> 00:00:24,900
 course had nothing

9
00:00:24,900 --> 00:00:30,360
 really to do with me and it was basically a way of

10
00:00:30,360 --> 00:00:33,640
 recovering the costs

11
00:00:33,640 --> 00:00:37,640
 incurred by the producers of the video. As it turns out the

12
00:00:37,640 --> 00:00:38,800
 producers of the

13
00:00:38,800 --> 00:00:45,460
 video have expressed their willingness to do away with any

14
00:00:45,460 --> 00:00:47,160
 fees, any

15
00:00:47,160 --> 00:00:50,750
 reimbursement that they might have gotten in the process of

16
00:00:50,750 --> 00:00:51,480
 producing the

17
00:00:51,480 --> 00:00:55,270
 videos so effectively we can offer the video for free and

18
00:00:55,270 --> 00:00:56,760
 as I said before the

19
00:00:56,760 --> 00:01:00,280
 money has nothing to do with me. I myself don't handle

20
00:01:00,280 --> 00:01:01,600
 money but I felt that it

21
00:01:01,600 --> 00:01:06,210
 was necessary to let that be known and now I'm more than

22
00:01:06,210 --> 00:01:08,320
 happy to say that our

23
00:01:08,320 --> 00:01:13,900
 videos are now totally free. We still have the cost of

24
00:01:13,900 --> 00:01:15,800
 shipping the videos of

25
00:01:15,800 --> 00:01:18,700
 course and that can cost between two and five dollars.

26
00:01:18,700 --> 00:01:19,840
 Again this has nothing to

27
00:01:19,840 --> 00:01:23,470
 do with me but it has to be paid for somewhere so often my

28
00:01:23,470 --> 00:01:24,280
 students or

29
00:01:24,280 --> 00:01:27,600
 friends or people who request the videos will offer

30
00:01:27,600 --> 00:01:29,000
 donations which of course

31
00:01:29,000 --> 00:01:32,950
 you're welcome to do but again let me make it clear that

32
00:01:32,950 --> 00:01:33,960
 the videos are free

33
00:01:33,960 --> 00:01:38,500
 we're fully willing and able to ship the video to anywhere

34
00:01:38,500 --> 00:01:40,200
 in the world. It might

35
00:01:40,200 --> 00:01:45,320
 take a little while to get to you but I should think that

36
00:01:45,320 --> 00:01:46,200
 you know free is free

37
00:01:46,200 --> 00:01:50,350
 so please don't be shy and do contact us and let us know if

38
00:01:50,350 --> 00:01:51,640
 you'd like one of the

39
00:01:51,640 --> 00:01:57,560
 DVD sets. Here it is this is what it looks like fully

40
00:01:57,560 --> 00:01:58,760
 professionally done

41
00:01:58,760 --> 00:02:02,960
 professionally produced professionally made so thanks

42
00:02:02,960 --> 00:02:04,320
 everyone who's given

43
00:02:04,320 --> 00:02:07,990
 their positive feedback and thank you just for requesting

44
00:02:07,990 --> 00:02:09,520
 the videos and it

45
00:02:09,520 --> 00:02:12,850
 makes us feel good to be able to spread the meditation

46
00:02:12,850 --> 00:02:15,240
 teachings in this way so

47
00:02:15,240 --> 00:02:19,060
 thanks again for tuning in and hope to hear from you all in

48
00:02:19,060 --> 00:02:20,880
 the future. Thank

49
00:02:20,880 --> 00:02:23,800
 you have a good day.

