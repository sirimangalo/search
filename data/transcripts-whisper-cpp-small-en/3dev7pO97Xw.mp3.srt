1
00:00:00,000 --> 00:00:04,060
 For me, when concentration is good, phenomena appear in a

2
00:00:04,060 --> 00:00:06,500
 certain flux centered in the heart region.

3
00:00:06,500 --> 00:00:10,500
 Any advice on avoiding delusion and attachment around this?

4
00:00:10,500 --> 00:00:13,450
 For example, the mind sometimes produces images to

5
00:00:13,450 --> 00:00:16,720
 represent the experience, since it's a very spatial

6
00:00:16,720 --> 00:00:17,500
 experience.

7
00:00:17,500 --> 00:00:25,570
 Well, studying the abhidhamma could be one fairly crass

8
00:00:25,570 --> 00:00:26,500
 answer.

9
00:00:28,500 --> 00:00:32,420
 To the point is just to break things down into what really

10
00:00:32,420 --> 00:00:33,000
 exists.

11
00:00:33,000 --> 00:00:36,000
 So when you see something, they're seeing.

12
00:00:36,000 --> 00:00:40,270
 And the point is to get to the point where an image is just

13
00:00:40,270 --> 00:00:41,000
 an image.

14
00:00:41,000 --> 00:00:48,000
 And the way to avoid delusion is to stop the associations.

15
00:00:48,000 --> 00:00:51,000
 That this means this and that means that.

16
00:00:51,000 --> 00:00:55,480
 Like, the heart region is one where, for example, and I don

17
00:00:55,480 --> 00:00:56,500
't know if it's your case,

18
00:00:56,500 --> 00:00:59,790
 people will start to feel tension or so on, or the heart

19
00:00:59,790 --> 00:01:02,500
 beating irregularly, and they'll become afraid.

20
00:01:02,500 --> 00:01:04,960
 They'll think, "Oh, I'm going to die. I'm going to have a

21
00:01:04,960 --> 00:01:06,500
 heart attack," or so on.

22
00:01:06,500 --> 00:01:15,000
 The point is to avoid any sort of projection in this way.

23
00:01:15,000 --> 00:01:17,500
 Other people, they'll have an experience.

24
00:01:17,500 --> 00:01:20,500
 For example, here, this is the third eye region.

25
00:01:20,500 --> 00:01:22,500
 They'll say, "Oh, my third eye is opening up."

26
00:01:22,500 --> 00:01:24,730
 And I get a lot of this because people actually believe in

27
00:01:24,730 --> 00:01:25,500
 the third eye.

28
00:01:25,500 --> 00:01:29,080
 They'll say, "I had a third eye opening experience," and so

29
00:01:29,080 --> 00:01:29,500
 on.

30
00:01:29,500 --> 00:01:33,500
 And it's total...

31
00:01:33,500 --> 00:01:36,050
 Well, it's valid in one sense. I mean, they're having some

32
00:01:36,050 --> 00:01:36,500
 experience.

33
00:01:36,500 --> 00:01:40,500
 But there's the proliferation, the idea of the chakras.

34
00:01:40,500 --> 00:01:43,500
 We don't teach chakras because that's an extrapolation.

35
00:01:43,500 --> 00:01:44,500
 The chakras don't exist.

36
00:01:44,500 --> 00:01:48,830
 There may be some activity arise in a certain region, but

37
00:01:48,830 --> 00:01:50,500
 that's just activity arising.

38
00:01:50,500 --> 00:01:54,280
 It's a feeling, or it's a sight, or it's a sound, or

39
00:01:54,280 --> 00:01:55,500
 whatever it is.

40
00:01:55,500 --> 00:01:58,160
 That's why, if you read the Buddha's teaching, it actually

41
00:01:58,160 --> 00:02:00,500
 seems boring and dull and dry

42
00:02:00,500 --> 00:02:04,260
 because it's trying to bring us to the point where we can

43
00:02:04,260 --> 00:02:05,500
 see what really is there,

44
00:02:05,500 --> 00:02:08,350
 and that's just seeing, hearing, smelling, tasting, feeling

45
00:02:08,350 --> 00:02:08,500
, and thinking.

46
00:02:08,500 --> 00:02:10,500
 That's the truth of reality.

47
00:02:10,500 --> 00:02:14,010
 The best way to avoid delusion is to focus on things like

48
00:02:14,010 --> 00:02:17,500
 this, the five aggregates, the six senses,

49
00:02:18,500 --> 00:02:21,500
 and the four sati pathanas.

50
00:02:21,500 --> 00:02:24,500
 No.

