 Can you talk about right concentration? Right concentration
 is any concentration,
 any, there's a quote of the Buddha, it's any wholesome,
 beneficial, oneness, single-pointedness
 of mind that is free from the five hindrances which are
 liking, disliking, drowsiness,
 distraction and doubt. If the mind is free from those then
 this is considered to be right
 concentration. Now as I talked about yesterday in terms of
 the Eightfold Noble Path true right
 concentration is only that one moment or two moments or
 whatever it's the moment no it's one
 moment where the mind is leaving samsara or has entered
 into nibbana for the first time. But we
 get to that point by abandoning the five hindrances. So at
 the moment of the maga nyana,
 then the knowledge of the path, maga means path, it's the
 moment when the Eightfold Noble Path
 comes into full effect and at that moment we are said to
 have right concentration and in the moments
 that follow which are the moments of palanyana where the
 person stays in nibbana and experiences
 nibbanaic freedom. That's right concentration. Simple
 answer, I'm not gonna go, let's not go
 any further than that, keep it simple.
