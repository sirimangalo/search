1
00:00:00,000 --> 00:00:03,000
 [water splashing]

2
00:00:03,000 --> 00:00:06,000
 [music]

3
00:00:08,000 --> 00:00:11,000
 [water splashing]

4
00:00:13,000 --> 00:00:15,000
 [water splashing]

5
00:00:15,000 --> 00:00:18,000
 [water splashing]

6
00:00:18,000 --> 00:00:35,000
 [water splashing]

7
00:00:37,000 --> 00:00:40,000
 [music]

8
00:00:42,000 --> 00:00:45,000
 [music]

9
00:00:45,000 --> 00:00:49,000
 [music]

10
00:00:49,000 --> 00:00:59,000
 [music]

11
00:00:59,000 --> 00:01:07,000
 [music]

12
00:01:07,000 --> 00:01:10,000
 [music]

13
00:01:10,000 --> 00:01:17,000
 Ladies and gentlemen, now before we start to practice,

14
00:01:17,000 --> 00:01:20,000
 we have to pay respect to the Buddha first.

15
00:01:20,000 --> 00:01:24,000
 Any of you, you just watching or you can follow me, just

16
00:01:24,000 --> 00:01:24,000
 matter.

17
00:01:24,000 --> 00:01:29,000
 [singing]

18
00:01:31,000 --> 00:01:34,000
 [singing]

19
00:01:34,000 --> 00:01:38,000
 [singing]

20
00:01:38,000 --> 00:01:44,000
 [singing]

21
00:01:44,000 --> 00:01:48,000
 [singing]

22
00:01:48,000 --> 00:01:56,000
 [singing]

23
00:01:57,000 --> 00:02:00,000
 [singing]

24
00:02:00,000 --> 00:02:08,000
 [singing]

25
00:02:08,000 --> 00:02:12,000
 [singing]

26
00:02:12,000 --> 00:02:16,000
 [singing]

27
00:02:16,000 --> 00:02:20,000
 [singing]

28
00:02:20,000 --> 00:02:24,000
 [singing]

29
00:02:25,000 --> 00:02:28,000
 [singing]

30
00:02:28,000 --> 00:02:37,580
 First of all, I would like to let you know that please pay

31
00:02:37,580 --> 00:02:38,000
 attention.

32
00:02:38,000 --> 00:02:43,000
 Usually before we start to do anything,

33
00:02:43,000 --> 00:02:46,090
 it is Thai tradition or Thai culture or it's a Buddhist

34
00:02:46,090 --> 00:02:47,000
 tradition.

35
00:02:47,000 --> 00:02:51,000
 This is the Gallants of Flowers.

36
00:02:51,000 --> 00:02:55,000
 Usually every time before we start teaching,

37
00:02:55,000 --> 00:02:59,000
 the Lord Buddha is our headmaster.

38
00:02:59,000 --> 00:03:02,000
 We respect him as our father.

39
00:03:02,000 --> 00:03:07,400
 So before we start to do anything, we have to respect him

40
00:03:07,400 --> 00:03:08,000
 first.

41
00:03:08,000 --> 00:03:12,780
 That is the reason why I have to pay homage to the Buddha

42
00:03:12,780 --> 00:03:13,000
 first.

43
00:03:13,000 --> 00:03:17,000
 And then this is what it is, what it means.

44
00:03:18,000 --> 00:03:23,000
 Usually Thai people, before they start to learn meditation,

45
00:03:23,000 --> 00:03:27,000
 they have to pay respect to the master or teacher first.

46
00:03:27,000 --> 00:03:34,000
 But for all of you, maybe it is not necessary for you to do

47
00:03:34,000 --> 00:03:41,000
 because you just come in here and you are not understanding

48
00:03:41,000 --> 00:03:43,000
 what does it mean,

49
00:03:43,000 --> 00:03:44,000
 why you have to do.

50
00:03:44,000 --> 00:03:47,180
 I just let you know that this is Thai tradition, Thai

51
00:03:47,180 --> 00:03:48,000
 culture.

52
00:03:48,000 --> 00:03:56,000
 Before we start to learn meditation, we have to take this

53
00:03:56,000 --> 00:03:58,000
 to the master

54
00:03:58,000 --> 00:04:01,000
 and the master has to give them the precept first.

55
00:04:01,000 --> 00:04:06,000
 Maybe you don't understand about the precepts.

56
00:04:06,000 --> 00:04:09,330
 If I tell you that the precepts, it is a little bit

57
00:04:09,330 --> 00:04:12,000
 difficult for you to understand

58
00:04:12,000 --> 00:04:14,000
 what does it mean, precepts.

59
00:04:14,000 --> 00:04:20,200
 But if I tell you that five rules are easy for you to

60
00:04:20,200 --> 00:04:21,000
 understand,

61
00:04:21,000 --> 00:04:27,350
 five rules or five precepts, they are number one, no

62
00:04:27,350 --> 00:04:29,000
 killing,

63
00:04:29,000 --> 00:04:34,440
 number two, no stealing, number three, not committing

64
00:04:34,440 --> 00:04:36,000
 sexual adultery,

65
00:04:36,000 --> 00:04:42,550
 number four, not care-line, number five, no drinking

66
00:04:42,550 --> 00:04:43,000
 alcohol.

67
00:04:43,000 --> 00:04:47,530
 These are five precepts or five rules that people or their

68
00:04:47,530 --> 00:04:49,000
 person like you are

69
00:04:49,000 --> 00:04:51,000
 should have to keep it.

70
00:04:51,000 --> 00:04:58,000
 You can imagine that if the population of the world takes

71
00:04:58,000 --> 00:05:01,000
 only five rules or five precepts,

72
00:05:01,000 --> 00:05:03,000
 this will be peaceful, right?

73
00:05:03,000 --> 00:05:09,240
 But it is impossible because of this world, many, many

74
00:05:09,240 --> 00:05:12,000
 people, many kind of people.

75
00:05:12,000 --> 00:05:16,000
 The people in this world come from different levels,

76
00:05:16,000 --> 00:05:19,000
 different positions, different situations.

77
00:05:19,000 --> 00:05:25,980
 Over here today, you are coming here to learn about

78
00:05:25,980 --> 00:05:30,000
 Buddhist meditation, yes or no?

79
00:05:31,000 --> 00:05:33,000
 This is the reason you are coming here, right?

80
00:05:33,000 --> 00:05:37,050
 And you would like to know some of, did you read some about

81
00:05:37,050 --> 00:05:39,000
 the book, about Buddhism?

82
00:05:39,000 --> 00:05:41,000
 You read some?

83
00:05:41,000 --> 00:05:44,000
 Somebody read the book about Buddhism?

84
00:05:44,000 --> 00:05:46,000
 Never?

85
00:05:46,000 --> 00:05:49,000
 You did or you never read?

86
00:05:49,000 --> 00:05:54,750
 Anyhow, I would like to let you know that the doctrine of

87
00:05:54,750 --> 00:05:56,000
 the Buddha,

88
00:05:56,000 --> 00:06:02,000
 there are 84,000 articles, 84,000 main articles,

89
00:06:02,000 --> 00:06:10,000
 but it is impossible for me to tell you all of them now.

90
00:06:10,000 --> 00:06:16,390
 I just let you know that the doctrine of the Buddha, there

91
00:06:16,390 --> 00:06:18,000
 are 84 main articles,

92
00:06:18,000 --> 00:06:21,000
 but we can summarize only three.

93
00:06:21,000 --> 00:06:26,100
 Number one, you are not doing your will or you are not

94
00:06:26,100 --> 00:06:29,000
 doing the bad things, number one.

95
00:06:29,000 --> 00:06:34,040
 And number two, you have to do the good things or good

96
00:06:34,040 --> 00:06:35,000
 deeds.

97
00:06:35,000 --> 00:06:39,000
 Number three, purify your mind.

98
00:06:39,000 --> 00:06:43,000
 These are the summarizations of the doctrine of the Buddha.

99
00:06:44,000 --> 00:06:50,600
 You are coming here, you try to know more about, number

100
00:06:50,600 --> 00:06:53,000
 three, purify your mind.

101
00:06:53,000 --> 00:06:59,450
 If I tell you that you are not doing your will or bad

102
00:06:59,450 --> 00:07:01,000
 things, you can understand.

103
00:07:01,000 --> 00:07:05,000
 It's easy for you to understand. What does it mean?

104
00:07:05,000 --> 00:07:08,970
 For example, like you are not killing, you are not stealing

105
00:07:08,970 --> 00:07:09,000
,

106
00:07:09,000 --> 00:07:12,320
 you are not doing sexual, comedic, or drug-free, that is

107
00:07:12,320 --> 00:07:13,000
 bad things.

108
00:07:13,000 --> 00:07:16,600
 Opposite, if you are not doing that, you are doing good

109
00:07:16,600 --> 00:07:17,000
 things.

110
00:07:17,000 --> 00:07:23,000
 But purify your mind. You cannot just read it.

111
00:07:23,000 --> 00:07:27,610
 You have to practice. Only one in the world, that is

112
00:07:27,610 --> 00:07:29,000
 meditation.

113
00:07:29,000 --> 00:07:34,630
 So there is a reason all of you are coming here to learn

114
00:07:34,630 --> 00:07:35,000
 more,

115
00:07:35,000 --> 00:07:38,000
 how to clean and clear your mind.

116
00:07:38,000 --> 00:07:43,630
 Because when you practice meditation, it means you come to

117
00:07:43,630 --> 00:07:46,000
 clean and clear your mind.

118
00:07:46,000 --> 00:07:50,790
 Clean and clear your mind from what? From great hatred,

119
00:07:50,790 --> 00:07:52,000
 delusion.

120
00:07:52,000 --> 00:07:57,000
 I think you understand great hatred, delusion.

121
00:07:57,000 --> 00:08:01,000
 Because all of these kill the people.

122
00:08:01,000 --> 00:08:04,000
 The people kill each other because of this.

123
00:08:05,000 --> 00:08:12,610
 So if you can quit or if you can overcome all of this, it

124
00:08:12,610 --> 00:08:15,000
 means your mind purify.

125
00:08:15,000 --> 00:08:21,000
 How many kinds of meditation in the Buddhism?

126
00:08:21,000 --> 00:08:25,910
 There are many, many kinds of techniques in the Buddhist

127
00:08:25,910 --> 00:08:27,000
 meditation.

128
00:08:27,000 --> 00:08:32,110
 But we can tell that, we can say that there are only two

129
00:08:32,110 --> 00:08:34,000
 million kinds of Buddhist meditation,

130
00:08:34,000 --> 00:08:36,000
 only two million kinds.

131
00:08:36,000 --> 00:08:41,000
 Number one, samatha or tranquility meditation.

132
00:08:41,000 --> 00:08:46,000
 Number one, samatha or tranquility meditation.

133
00:08:46,000 --> 00:08:52,000
 Number two, vibhavasana or inside meditation.

134
00:08:52,000 --> 00:08:55,000
 These are two million kinds.

135
00:08:55,000 --> 00:09:02,490
 Over here, we are teaching vibhavasana or inside meditation

136
00:09:02,490 --> 00:09:03,000
.

137
00:09:04,000 --> 00:09:07,260
 Even though vibhavasana or inside meditation, many, many

138
00:09:07,260 --> 00:09:09,000
 kinds of techniques.

139
00:09:09,000 --> 00:09:14,180
 But the techniques, what we are teaching here, namely four

140
00:09:14,180 --> 00:09:17,000
 foundations of mindfulness.

141
00:09:17,000 --> 00:09:21,500
 Four foundations of mindfulness is the name of the vibhavas

142
00:09:21,500 --> 00:09:24,000
ana what we are teaching here.

143
00:09:24,000 --> 00:09:28,000
 I will try to explain to you step by step.

144
00:09:28,000 --> 00:09:33,690
 Four foundations of mindfulness, it means before you start

145
00:09:33,690 --> 00:09:36,000
 to practice meditation,

146
00:09:36,000 --> 00:09:41,850
 you have to try to keep your mind empty, make your mind

147
00:09:41,850 --> 00:09:43,000
 empty.

148
00:09:43,000 --> 00:09:47,000
 It means you don't want to think of past harm.

149
00:09:47,000 --> 00:09:51,000
 You don't want to think of future harm.

150
00:09:51,000 --> 00:09:55,980
 You try to keep your mindfulness at the present moment to

151
00:09:55,980 --> 00:09:57,000
 moment.

152
00:09:57,000 --> 00:09:59,000
 Anything you understand?

153
00:09:59,000 --> 00:10:02,000
 You don't want to think of past harm.

154
00:10:02,000 --> 00:10:07,000
 It means you don't want to think of what you did before.

155
00:10:07,000 --> 00:10:10,300
 And you don't want to think of what you are going to do

156
00:10:10,300 --> 00:10:12,000
 tomorrow or after this.

157
00:10:12,000 --> 00:10:15,360
 Just try to keep your mindfulness at the present moment to

158
00:10:15,360 --> 00:10:16,000
 moment.

159
00:10:16,000 --> 00:10:20,000
 All of you, you fix your eyes on me now, right?

160
00:10:20,000 --> 00:10:23,330
 Because you would like to know, you will pay attention to

161
00:10:23,330 --> 00:10:26,000
 know what I am talking, what I am saying.

162
00:10:26,000 --> 00:10:29,840
 This moment you try to understand what I am saying, what I

163
00:10:29,840 --> 00:10:31,000
 am talking about.

164
00:10:31,000 --> 00:10:36,270
 But if you are not paying attention, if you look around,

165
00:10:36,270 --> 00:10:38,000
 look somewhere,

166
00:10:38,000 --> 00:10:40,000
 you cannot have good concentration.

167
00:10:40,000 --> 00:10:43,060
 And then you cannot understand what I am talking about,

168
00:10:43,060 --> 00:10:46,000
 what I am saying, right?

169
00:10:46,000 --> 00:10:50,000
 So please try to pay attention.

170
00:10:50,000 --> 00:10:56,000
 I tried to explain to you about Buddhist meditation clearly

171
00:10:56,000 --> 00:10:56,000
.

172
00:10:56,000 --> 00:11:02,000
 I told you that the meditation, what we are teaching here,

173
00:11:02,000 --> 00:11:07,000
 namely, for foundations of mindfulness, it means,

174
00:11:07,000 --> 00:11:12,990
 number one, you have to pay attention and keep your

175
00:11:12,990 --> 00:11:17,000
 mindfulness on a bodily action.

176
00:11:17,000 --> 00:11:21,520
 It means every time when you are standing, you know you are

177
00:11:21,520 --> 00:11:22,000
 standing.

178
00:11:22,000 --> 00:11:25,000
 When you are sitting, you know you are sitting.

179
00:11:25,000 --> 00:11:28,920
 When you are walking or while you are walking, you know you

180
00:11:28,920 --> 00:11:30,000
 are walking.

181
00:11:30,000 --> 00:11:32,000
 This is bodily action.

182
00:11:32,000 --> 00:11:36,560
 And number two, contemplation of your feelings or

183
00:11:36,560 --> 00:11:38,000
 sensations.

184
00:11:38,000 --> 00:11:43,000
 It means while you are sitting meditation, especially,

185
00:11:43,000 --> 00:11:46,930
 like now, all of you are sitting cross-legged, someone

186
00:11:46,930 --> 00:11:48,000
 sideways.

187
00:11:48,000 --> 00:11:53,050
 But sometimes you feel your legs pain, sometimes you feel

188
00:11:53,050 --> 00:11:54,000
 your legs numb,

189
00:11:54,000 --> 00:11:57,000
 sometimes you feel your back pain.

190
00:11:57,000 --> 00:12:00,000
 So this is sensations.

191
00:12:00,000 --> 00:12:05,110
 So if any kind of sensations happen to you, you try to

192
00:12:05,110 --> 00:12:10,000
 notice contemplation of your feelings or sensations.

193
00:12:10,000 --> 00:12:13,000
 Number three, contemplation of your mind.

194
00:12:13,000 --> 00:12:17,850
 It means while you are sitting meditation or walking

195
00:12:17,850 --> 00:12:19,000
 meditation,

196
00:12:19,000 --> 00:12:24,420
 sometimes you are thinking of something or maybe you are

197
00:12:24,420 --> 00:12:28,000
 thinking of many things in five minutes or ten minutes.

198
00:12:28,000 --> 00:12:33,280
 If you are thinking of something or anything, please try to

199
00:12:33,280 --> 00:12:34,000
 notice.

200
00:12:34,000 --> 00:12:37,000
 Thinking, thinking, thinking.

201
00:12:37,000 --> 00:12:41,000
 And number four, contemplation of your mind objects.

202
00:12:41,000 --> 00:12:43,000
 Mind objects, what does it mean?

203
00:12:43,000 --> 00:12:48,910
 Mind objects, it means we talk about five kinds of hind

204
00:12:48,910 --> 00:12:50,000
rance.

205
00:12:50,000 --> 00:12:53,000
 Number one, sense desires.

206
00:12:53,000 --> 00:12:57,000
 Desires, many kinds of desires.

207
00:12:57,000 --> 00:13:00,450
 For example, like you are on, you need money, you need a

208
00:13:00,450 --> 00:13:02,000
 good job, you need food frames,

209
00:13:02,000 --> 00:13:05,000
 you need a car, you need a house.

210
00:13:05,000 --> 00:13:10,000
 This is a desire. These are desires.

211
00:13:10,000 --> 00:13:13,000
 And number two, anger or angry.

212
00:13:13,000 --> 00:13:17,000
 Number three, sleepy, lazy, dropper.

213
00:13:17,000 --> 00:13:21,000
 Number four, distracting or distraction.

214
00:13:21,000 --> 00:13:24,000
 Number five, doubt.

215
00:13:24,000 --> 00:13:28,910
 All of this happens to everybody, more or less, but it will

216
00:13:28,910 --> 00:13:31,000
 be happening to everybody.

217
00:13:31,000 --> 00:13:35,000
 Male, female, every nationality is same.

218
00:13:35,000 --> 00:13:43,000
 So if all of this happens to you, namely five hindrance,

219
00:13:43,000 --> 00:13:47,000
 if it happens to you, you just try to notice.

220
00:13:47,000 --> 00:13:51,810
 And please remember that while you are practicing

221
00:13:51,810 --> 00:13:53,000
 meditation,

222
00:13:53,000 --> 00:13:56,550
 this kind of technique will show you how to walk, how to

223
00:13:56,550 --> 00:13:57,000
 sit.

224
00:13:57,000 --> 00:14:02,000
 You have to know walking meditation and sitting meditation.

225
00:14:02,000 --> 00:14:06,000
 I will show you first before we start to do.

226
00:14:06,000 --> 00:14:11,740
 And I will try to explain to you when or while you are

227
00:14:11,740 --> 00:14:14,000
 walking what you should do,

228
00:14:14,000 --> 00:14:16,000
 while you are sitting what you should do.

229
00:14:16,000 --> 00:14:23,600
 I told you that meditation practice, you try to pay

230
00:14:23,600 --> 00:14:25,000
 attention,

231
00:14:25,000 --> 00:14:29,000
 try to understand what you are doing.

232
00:14:29,000 --> 00:14:34,290
 So before you start to practice, you have to try to pay

233
00:14:34,290 --> 00:14:35,000
 attention

234
00:14:35,000 --> 00:14:39,000
 and fix your mind inside your body.

235
00:14:39,000 --> 00:14:44,290
 Please remember that when you are walking or while you are

236
00:14:44,290 --> 00:14:45,000
 walking,

237
00:14:45,000 --> 00:14:49,000
 you fix your mind on your feet.

238
00:14:49,000 --> 00:14:52,000
 Fix your mind on your feet. It means you pay attention.

239
00:14:52,000 --> 00:14:55,000
 When you are standing, you know you are standing.

240
00:14:55,000 --> 00:15:00,000
 You say inside your mind, standing, standing, standing

241
00:15:00,000 --> 00:15:01,000
 three times.

242
00:15:01,000 --> 00:15:04,000
 It means you try to understand you are standing now.

243
00:15:04,000 --> 00:15:09,390
 And then when you start to walk, every moment you are

244
00:15:09,390 --> 00:15:11,000
 sleeping, you will be aware.

245
00:15:11,000 --> 00:15:16,010
 Awareness, mindfulness, it is very, very important for med

246
00:15:16,010 --> 00:15:17,000
itators.

247
00:15:17,000 --> 00:15:22,000
 Mindfulness, it means while every step you are sleeping,

248
00:15:22,000 --> 00:15:26,200
 when your foot is lifting like this, lifting, you know that

249
00:15:26,200 --> 00:15:27,000
 lifting.

250
00:15:27,000 --> 00:15:31,000
 When your foot is dropping, you say "ressing".

251
00:15:31,000 --> 00:15:34,000
 When your foot is dropping, you say "dropping".

252
00:15:34,000 --> 00:15:38,000
 I will show you again. This is walking meditation.

253
00:15:38,000 --> 00:15:43,770
 Please remember that while you are walking, you don't want

254
00:15:43,770 --> 00:15:46,000
 to close your eyes.

255
00:15:46,000 --> 00:15:51,000
 Keep your eyes open and then walk in.

256
00:15:51,000 --> 00:15:55,000
 But fix your mind on your feet.

257
00:15:55,000 --> 00:15:58,000
 Every step you are stepping, you will be aware.

258
00:15:58,000 --> 00:16:01,000
 That is walking meditation.

259
00:16:01,000 --> 00:16:04,000
 And when you are walking, you to close your hands.

260
00:16:04,000 --> 00:16:09,540
 Because if you let your hand free, it is difficult to have

261
00:16:09,540 --> 00:16:11,000
 good concentration.

262
00:16:11,000 --> 00:16:15,000
 So you to close your hands and walk in normally.

263
00:16:15,000 --> 00:16:19,000
 When you practice meditation, you try to relax.

264
00:16:19,000 --> 00:16:22,250
 You don't want to force yourself. You don't want to press

265
00:16:22,250 --> 00:16:23,000
 yourself.

266
00:16:23,000 --> 00:16:28,000
 Just try to walk in normally. Remember.

267
00:16:28,000 --> 00:16:34,000
 And then after you finish walking, you will sit in.

268
00:16:34,000 --> 00:16:38,000
 Cross legs like this, like that, cross legs.

269
00:16:38,000 --> 00:16:41,000
 And then you put your hands like this first.

270
00:16:41,000 --> 00:16:45,000
 Move your hand, hand by hand, like this. Moving, moving,

271
00:16:45,000 --> 00:16:45,000
 moving.

272
00:16:45,000 --> 00:16:48,000
 Turning, turning, turning, lowering, touching.

273
00:16:48,000 --> 00:16:52,000
 Moving, moving, turning, turning, lowering, touching.

274
00:16:52,000 --> 00:16:56,000
 Like this. After that, keep your body straight.

275
00:16:56,000 --> 00:17:00,470
 Straight. Because if your body is not straight, it is

276
00:17:00,470 --> 00:17:02,000
 difficult when you are breathing.

277
00:17:02,000 --> 00:17:06,090
 Because when you are sitting in meditation, you have to fix

278
00:17:06,090 --> 00:17:08,000
 your mind on the abdomen.

279
00:17:08,000 --> 00:17:12,730
 When you are breathing in, your abdomen is rising, you just

280
00:17:12,730 --> 00:17:14,000
 not rising.

281
00:17:14,000 --> 00:17:17,990
 But when you are breathing out, your abdomen is falling,

282
00:17:17,990 --> 00:17:20,000
 you just not falling.

283
00:17:20,000 --> 00:17:25,000
 But this moment, maybe you are thinking of something.

284
00:17:25,000 --> 00:17:30,000
 If you are thinking of something, you have to not suddenly.

285
00:17:30,000 --> 00:17:34,000
 Not thinking, thinking, thinking, three times, or three

286
00:17:34,000 --> 00:17:34,000
 times.

287
00:17:34,000 --> 00:17:37,000
 And then come back to your abdomen.

288
00:17:37,000 --> 00:17:41,000
 Please remember that while you are sitting in meditation,

289
00:17:41,000 --> 00:17:45,000
 you have to fix your mind on the abdomen only.

290
00:17:45,000 --> 00:17:48,120
 If you are thinking of something, you are not thinking,

291
00:17:48,120 --> 00:17:50,000
 thinking, thinking three times.

292
00:17:50,000 --> 00:17:52,910
 If you are hearing something, you know that hearing,

293
00:17:52,910 --> 00:17:54,000
 hearing, hearing.

294
00:17:54,000 --> 00:17:56,000
 Then come back to your abdomen.

295
00:17:56,000 --> 00:18:01,000
 Try to sit in quiet as long as possible.

296
00:18:01,000 --> 00:18:07,000
 So after this, we can show you how to walk, how to sit.

297
00:18:07,000 --> 00:18:14,110
 And please remember, please remember that meditation

298
00:18:14,110 --> 00:18:15,000
 practice,

299
00:18:15,000 --> 00:18:22,000
 anyone can practice anywhere, any day, any time.

300
00:18:22,000 --> 00:18:29,000
 Remember, anyone can practice anywhere, any day, any time.

301
00:18:29,000 --> 00:18:34,190
 And maybe you are wondering that, what are the purposes of

302
00:18:34,190 --> 00:18:36,000
 Buddhist meditation?

303
00:18:36,000 --> 00:18:39,480
 If you practice meditation, what do you get, what do you

304
00:18:39,480 --> 00:18:40,000
 read,

305
00:18:40,000 --> 00:18:44,000
 what are the results of Buddhist meditation?

306
00:18:44,000 --> 00:18:50,770
 I can tell you that there are five purposes of practicing

307
00:18:50,770 --> 00:18:52,000
 meditation.

308
00:18:52,000 --> 00:18:55,000
 Number one, purify your mind.

309
00:18:55,000 --> 00:19:01,000
 Number two, get rid of sorrows and lamentations.

310
00:19:01,000 --> 00:19:07,000
 Number three, get rid of physical and mental suffering.

311
00:19:07,000 --> 00:19:12,000
 Number four, understand the truth of life.

312
00:19:12,000 --> 00:19:18,000
 Number five, extinguish suffering and attain your water.

313
00:19:18,000 --> 00:19:21,000
 I explain to you step by step.

314
00:19:21,000 --> 00:19:25,000
 Number one, purify your mind, what does it mean?

315
00:19:25,000 --> 00:19:29,550
 It means when you practice meditation, even one minute,

316
00:19:29,550 --> 00:19:31,000
 five minutes, ten minutes,

317
00:19:31,000 --> 00:19:57,000
 that moment will be purify from free, healthier delusion.

318
00:19:57,000 --> 00:20:01,000
 Number five, it means when you practice meditation,

319
00:20:01,000 --> 00:20:06,990
 you can understand yourself that every moment your body

320
00:20:06,990 --> 00:20:09,000
 always changes.

321
00:20:09,000 --> 00:20:13,400
 Everything impermanent is always changed, even your

322
00:20:13,400 --> 00:20:14,000
 breathing.

323
00:20:14,000 --> 00:20:18,000
 You are breathing every moment but never smoothly.

324
00:20:18,000 --> 00:20:21,670
 Sometimes it's slow, sometimes it's fast, sometimes it's

325
00:20:21,670 --> 00:20:25,000
 deep, sometimes it's shallow, for example.

326
00:20:25,000 --> 00:20:30,000
 So it's always changed. Even your body, you always change.

327
00:20:30,000 --> 00:20:35,570
 Your hands always change. It's longer and longer every

328
00:20:35,570 --> 00:20:37,000
 moment but you don't see that.

329
00:20:37,000 --> 00:20:42,880
 You can see that. Your skin, your face, everything changes

330
00:20:42,880 --> 00:20:44,000
 all the time.

331
00:20:44,000 --> 00:20:48,460
 But you don't know. But if you practice meditation, you can

332
00:20:48,460 --> 00:20:50,000
 see that.

333
00:20:50,000 --> 00:20:52,410
 When you're breathing in, breathing out, it's always

334
00:20:52,410 --> 00:20:53,000
 changed.

335
00:20:53,000 --> 00:20:57,350
 Sometimes it's fast, sometimes it's slow, sometimes it's

336
00:20:57,350 --> 00:20:59,000
 deep, for example.

337
00:20:59,000 --> 00:21:05,000
 Number five, extinguish suffering and attain your water.

338
00:21:05,000 --> 00:21:12,000
 And sometimes you feel you have disruption.

339
00:21:12,000 --> 00:21:15,000
 If you have disruption, it means you're thinking too much,

340
00:21:15,000 --> 00:21:16,000
 thinking of many things.

341
00:21:16,000 --> 00:21:19,000
 You try to catch your thought as soon as possible.

342
00:21:19,000 --> 00:21:23,000
 If you have doubt, you try to catch your thought.

343
00:21:23,000 --> 00:21:27,000
 And note that doubt, doubt, doubt.

344
00:21:27,000 --> 00:21:32,000
 All of this you have to try to understand.

345
00:21:32,000 --> 00:21:36,680
 Now I give you instructions and explaining to you about 30

346
00:21:36,680 --> 00:21:38,000
 minutes already.

347
00:21:38,000 --> 00:21:45,390
 And then now we start to walk, start to walk about 15

348
00:21:45,390 --> 00:21:48,000
 minutes and then sitting, okay?

349
00:21:48,000 --> 00:21:49,000
 Okay?

