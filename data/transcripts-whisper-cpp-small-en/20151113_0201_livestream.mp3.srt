1
00:00:00,000 --> 00:00:02,000
 [ [

2
00:00:02,000 --> 00:00:02,000
 [

3
00:00:02,000 --> 00:00:03,000
 [

4
00:00:03,000 --> 00:00:05,000
 []

5
00:00:05,000 --> 00:00:08,000
 Okay, good evening everyone.

6
00:00:08,000 --> 00:00:12,000
 We're broadcasting live, hopefully,

7
00:00:12,000 --> 00:00:16,000
 November 12th, 2015.

8
00:00:16,000 --> 00:00:21,000
 I'm trying out something new here.

9
00:00:21,000 --> 00:00:22,000
 [

10
00:00:22,000 --> 00:00:24,000
 [

11
00:00:24,000 --> 00:00:25,000
 []

12
00:00:25,000 --> 00:00:26,000
 []

13
00:00:26,000 --> 00:00:27,000
 []

14
00:00:27,000 --> 00:00:28,000
 []

15
00:00:28,000 --> 00:00:29,000
 []

16
00:00:29,000 --> 00:00:30,000
 []

17
00:00:30,000 --> 00:00:31,000
 []

18
00:00:31,000 --> 00:00:36,000
 So, just a word.

19
00:00:36,000 --> 00:00:44,000
 Tomorrow we have a special event with the Buddhism

20
00:00:44,000 --> 00:00:45,000
 Association,

21
00:00:45,000 --> 00:00:48,000
 so I won't be broadcasting tomorrow.

22
00:00:48,000 --> 00:00:50,000
 That is cancelled.

23
00:00:50,000 --> 00:00:53,000
 And then Saturday is a big day as well.

24
00:00:53,000 --> 00:00:55,000
 But Saturday broadcast should go ahead.

25
00:00:55,000 --> 00:00:58,000
 I mean, just an ordinary broadcast.

26
00:00:58,000 --> 00:01:01,000
 But we're skipping a Dhammapada.

27
00:01:01,000 --> 00:01:04,000
 So the next Dhammapada will be Monday.

28
00:01:04,000 --> 00:01:12,000
 I suppose we could do a Dhammapada on Saturday,

29
00:01:12,000 --> 00:01:16,000
 but I think we'll just skip it.

30
00:01:18,000 --> 00:01:22,000
 Which means we have to wait for this extra special next one

31
00:01:22,000 --> 00:01:22,000
.

32
00:01:22,000 --> 00:01:25,000
 The next one's extra special.

33
00:01:25,000 --> 00:01:33,000
 I don't know how this new mic is working out.

34
00:01:33,000 --> 00:01:35,000
 Hopefully it's okay.

35
00:01:35,000 --> 00:01:42,000
 So tonight we will take questions.

36
00:01:42,000 --> 00:01:45,000
 If anyone has any questions.

37
00:01:47,000 --> 00:01:49,000
 The mic sounds fine, Bante.

38
00:01:49,000 --> 00:01:53,000
 It's just picking up a little bit of noise from

39
00:01:53,000 --> 00:01:55,000
 and just rustling around a little bit.

40
00:01:55,000 --> 00:01:57,000
 But it sounds good.

41
00:01:57,000 --> 00:01:58,000
 Okay.

42
00:01:58,000 --> 00:02:00,000
 I won't wrestle.

43
00:02:00,000 --> 00:02:03,000
 Just a very quiet...

44
00:02:03,000 --> 00:02:08,000
 So I have sort of a question.

45
00:02:08,000 --> 00:02:11,000
 A friend of mine, and I think she's watching, if she is.

46
00:02:11,000 --> 00:02:12,000
 Hi, Sally.

47
00:02:12,000 --> 00:02:15,000
 A friend of mine had asked if her daughter could attend

48
00:02:15,000 --> 00:02:17,000
 a Buddhist service with me.

49
00:02:17,000 --> 00:02:19,000
 And while that would have been great,

50
00:02:19,000 --> 00:02:22,000
 I would have really enjoyed bringing her.

51
00:02:22,000 --> 00:02:27,000
 There was only one, and she didn't have the availability

52
00:02:27,000 --> 00:02:28,000
 on that day.

53
00:02:28,000 --> 00:02:30,000
 It's for homework assignment.

54
00:02:30,000 --> 00:02:33,000
 So I was wondering if you could talk in general about...

55
00:02:33,000 --> 00:02:38,000
 The idea of her homework assignment was to attend a service

56
00:02:38,000 --> 00:02:41,000
 that was something outside of her own religion.

57
00:02:41,000 --> 00:02:43,000
 It's for a religion education class.

58
00:02:43,000 --> 00:02:45,000
 So I was wondering if you could talk a little bit about

59
00:02:45,000 --> 00:02:48,000
 what Buddhist services involve,

60
00:02:48,000 --> 00:02:53,000
 what the requirements are, the expectations for followers

61
00:02:53,000 --> 00:02:54,000
 of Buddhism.

62
00:02:54,000 --> 00:02:56,000
 It's considerably different from, let's say,

63
00:02:56,000 --> 00:02:58,000
 a Christian perspective with a real requirement

64
00:02:58,000 --> 00:03:01,000
 to be there weekly and so forth.

65
00:03:01,000 --> 00:03:05,000
 Well, maybe you can talk about...

66
00:03:08,000 --> 00:03:12,000
 I'm not sure exactly what the idea is.

67
00:03:12,000 --> 00:03:15,000
 We don't have any requirements in Buddhism.

68
00:03:15,000 --> 00:03:19,000
 We teach cause and effect.

69
00:03:19,000 --> 00:03:21,000
 If you do this, this will happen.

70
00:03:21,000 --> 00:03:26,000
 So we have theories and claims about reality,

71
00:03:26,000 --> 00:03:30,000
 but we don't have many obligations on people.

72
00:03:30,000 --> 00:03:34,470
 We have some "shoulds," but they're more or less

73
00:03:34,470 --> 00:03:35,000
 conditional.

74
00:03:35,000 --> 00:03:39,000
 If you want this, then you should do this.

75
00:03:39,000 --> 00:03:43,130
 If you want to be happy, then you should be a good person,

76
00:03:43,130 --> 00:03:44,000
 for example.

77
00:03:44,000 --> 00:03:53,000
 But there's never any sense of a institution in Buddhism,

78
00:03:53,000 --> 00:03:56,000
 except for the monastic institution,

79
00:03:56,000 --> 00:03:57,000
 which is a whole other thing,

80
00:03:57,000 --> 00:04:06,000
 but that's actually still quite practical.

81
00:04:06,000 --> 00:04:11,000
 So I'm not sure quite what I can talk about.

82
00:04:11,000 --> 00:04:14,860
 That's a big part of it, that there is no actual

83
00:04:14,860 --> 00:04:16,000
 requirement

84
00:04:16,000 --> 00:04:19,000
 to attend on a regular basis,

85
00:04:19,000 --> 00:04:23,000
 but people still do go to monasteries and temples

86
00:04:26,000 --> 00:04:29,000
 when they want to, I guess.

87
00:04:29,000 --> 00:04:34,000
 Yeah, they go to find teachers.

88
00:04:34,000 --> 00:04:38,000
 They go to the monastery to find a monk,

89
00:04:38,000 --> 00:04:42,720
 because you figure that a monk is a good choice for a

90
00:04:42,720 --> 00:04:44,000
 teacher,

91
00:04:44,000 --> 00:04:49,000
 and most likely has something good to teach.

92
00:04:49,000 --> 00:04:52,000
 And then you build up a relationship,

93
00:04:52,000 --> 00:04:59,680
 and then you know whether they have something good to teach

94
00:04:59,680 --> 00:05:00,000
.

95
00:05:00,000 --> 00:05:05,000
 And so you go back, and maybe you ask them questions

96
00:05:05,000 --> 00:05:11,840
 and get advice and clarify teachings and that kind of thing

97
00:05:11,840 --> 00:05:12,000
.

98
00:05:12,000 --> 00:05:18,000
 But there's also the "lupasata."

99
00:05:18,000 --> 00:05:20,000
 Sorry, I'm wrestling.

100
00:05:22,000 --> 00:05:24,000
 Also what?

101
00:05:24,000 --> 00:05:26,000
 The "lupasata."

102
00:05:26,000 --> 00:05:30,000
 Right.

103
00:05:30,000 --> 00:05:32,000
 Yeah, so there is something.

104
00:05:32,000 --> 00:05:39,000
 People ask the Buddha how they should keep the holy days,

105
00:05:39,000 --> 00:05:44,960
 because in India there was tradition of keeping the lunar

106
00:05:44,960 --> 00:05:46,000
 cycle.

107
00:05:47,000 --> 00:05:51,430
 It's a holy day, so they would take the eighth day and the

108
00:05:51,430 --> 00:05:52,000
 fifteenth day.

109
00:05:52,000 --> 00:05:55,620
 Because if you know, the moon goes in a cycle of thirty

110
00:05:55,620 --> 00:05:57,000
 days, approximately.

111
00:05:57,000 --> 00:06:00,000
 And so every fifteen days you get a new moon,

112
00:06:00,000 --> 00:06:03,000
 every fifteen days you get a lunar cycle.

113
00:06:03,000 --> 00:06:06,000
 After fifteen days you get the new moon,

114
00:06:06,000 --> 00:06:09,000
 and then another fifteen days you get the full moon.

115
00:06:09,000 --> 00:06:13,000
 And then halfway in between you get the half moon.

116
00:06:13,000 --> 00:06:17,000
 So they had four holy days in India,

117
00:06:17,000 --> 00:06:21,000
 that people kind of, whatever teaching they followed,

118
00:06:21,000 --> 00:06:24,000
 that was kind of a day that they considered to be holy.

119
00:06:24,000 --> 00:06:27,000
 Special day.

120
00:06:27,000 --> 00:06:30,000
 And so the Buddha said that people should keep them

121
00:06:30,000 --> 00:06:34,000
 by taking the eight precepts,

122
00:06:34,000 --> 00:06:38,000
 because that's the kind of thing an arahant lived by.

123
00:06:38,000 --> 00:06:43,000
 And so it was a way of mimicking or emulating the arahants

124
00:06:43,000 --> 00:06:45,000
 on those days,

125
00:06:45,000 --> 00:06:49,230
 by keeping the eight precepts, which is not killing, not

126
00:06:49,230 --> 00:06:50,000
 stealing,

127
00:06:50,000 --> 00:06:54,000
 not having any sexual or romantic activity,

128
00:06:54,000 --> 00:07:00,000
 not lying, not taking drugs or alcohol,

129
00:07:00,000 --> 00:07:05,000
 not sleeping on luxurious beds.

130
00:07:06,000 --> 00:07:08,000
 I'm sorry, I'm a bit tired.

131
00:07:08,000 --> 00:07:11,000
 Not eating outside of the wrong time,

132
00:07:11,000 --> 00:07:20,000
 not wearing beautification or engaging in entertainment,

133
00:07:20,000 --> 00:07:24,000
 and not sleeping on luxurious beds.

134
00:07:24,000 --> 00:07:34,440
 And they might also take it as an opportunity to practice

135
00:07:34,440 --> 00:07:35,000
 meditation.

136
00:07:36,000 --> 00:07:39,000
 More so than the rest of the week.

137
00:07:39,000 --> 00:07:50,000
 So rather than being weekly on a Sunday,

138
00:07:50,000 --> 00:07:53,000
 it would vary based on the moon cycles.

139
00:07:53,000 --> 00:07:57,000
 Yeah, I mean they didn't have a solar week back then.

140
00:07:57,000 --> 00:07:59,000
 Solar week? How do you call it?

141
00:07:59,000 --> 00:08:02,000
 They didn't have a seven day week the way we have it.

142
00:08:02,000 --> 00:08:05,000
 Lunar week? Or would it be solar week?

143
00:08:05,000 --> 00:08:08,420
 I don't know what we call ours, but they didn't have what

144
00:08:08,420 --> 00:08:09,000
 we have.

145
00:08:09,000 --> 00:08:11,000
 They had a lunar week.

146
00:08:11,000 --> 00:08:17,000
 So a fortnight would be based on the lunar cycle.

147
00:08:17,000 --> 00:08:30,000
 Thank you, Bante.

148
00:08:30,000 --> 00:08:35,000
 Another question.

149
00:08:35,000 --> 00:08:39,000
 What does an animal have to do to be born as a human?

150
00:08:39,000 --> 00:08:42,000
 They have to keep the five per cent.

151
00:08:42,000 --> 00:08:49,710
 Mainly. I mean, it's how something is born as something

152
00:08:49,710 --> 00:08:50,000
 else is,

153
00:08:50,000 --> 00:08:53,000
 if you know anything about karma, there's no science.

154
00:08:53,000 --> 00:08:57,000
 I mean, there's no easy way to pinpoint it.

155
00:08:57,000 --> 00:09:00,000
 It's like asking what happens,

156
00:09:00,000 --> 00:09:03,000
 what do you have to do to create an earthquake?

157
00:09:03,000 --> 00:09:05,000
 What do you have to do to create a tornado?

158
00:09:05,000 --> 00:09:07,000
 It's highly complex.

159
00:09:07,000 --> 00:09:12,000
 Nobody knows how to predict tornadoes or even earthquakes,

160
00:09:12,000 --> 00:09:15,000
 because it's very complex.

161
00:09:15,000 --> 00:09:18,550
 But how to predict where someone's going to be born is far

162
00:09:18,550 --> 00:09:20,000
 more complex.

163
00:09:20,000 --> 00:09:23,000
 So...

164
00:09:24,000 --> 00:09:27,820
 There's a lot to... I mean, it comes down to the last

165
00:09:27,820 --> 00:09:28,000
 moment,

166
00:09:28,000 --> 00:09:30,000
 what their mind is like in the last moment.

167
00:09:30,000 --> 00:09:32,000
 But if they're not keeping five per cent,

168
00:09:32,000 --> 00:09:35,000
 it's very difficult for them to be born as human.

169
00:09:35,000 --> 00:09:40,000
 It comes down to that last moment.

170
00:09:40,000 --> 00:09:44,450
 That would be extremely difficult for an animal that was a

171
00:09:44,450 --> 00:09:45,000
 carnivore,

172
00:09:45,000 --> 00:09:46,000
 I would think.

173
00:09:46,000 --> 00:09:48,000
 Yep.

174
00:09:53,000 --> 00:09:55,630
 It's not a matter of being difficult, because they wouldn't

175
00:09:55,630 --> 00:09:56,000
 have any idea,

176
00:09:56,000 --> 00:09:57,000
 but either.

177
00:09:57,000 --> 00:09:59,440
 They wouldn't have any idea, "Oh, let me be born as a human

178
00:09:59,440 --> 00:10:00,000
 being."

179
00:10:00,000 --> 00:10:02,000
 For the most part, they would be very happy,

180
00:10:02,000 --> 00:10:08,140
 living as they were and breeding with other animals of

181
00:10:08,140 --> 00:10:09,000
 their species

182
00:10:09,000 --> 00:10:13,000
 and being born again and again as the same type of animal.

183
00:10:13,000 --> 00:10:17,000
 It would be very rare, not a very common thing,

184
00:10:17,000 --> 00:10:22,000
 or it would not be common to have the cause to be born as a

185
00:10:22,000 --> 00:10:23,000
 human.

186
00:10:23,000 --> 00:10:34,000
 It's not about the quote, right? We have a quote.

187
00:10:34,000 --> 00:10:37,000
 Is there a quote of any interest?

188
00:10:42,000 --> 00:10:46,390
 "It is just as if a man travelling in a forest should come

189
00:10:46,390 --> 00:10:47,000
 across an ancient road,

190
00:10:47,000 --> 00:10:51,000
 an ancient path traversed by people in former times.

191
00:10:51,000 --> 00:10:54,000
 Proceeding along it, he comes to an ancient city,

192
00:10:54,000 --> 00:10:57,000
 an old royal citadel lived in by people in former times

193
00:10:57,000 --> 00:10:59,000
 with parks and graves,

194
00:10:59,000 --> 00:11:03,680
 oak groves, water tanks and walls, a truly delightful place

195
00:11:03,680 --> 00:11:04,000
."

196
00:11:04,000 --> 00:11:07,410
 I suppose that this man should tell of his discovery to the

197
00:11:07,410 --> 00:11:08,000
 capable

198
00:11:08,000 --> 00:11:10,410
 of a womanist saying, "Sire, you should know that I have

199
00:11:10,410 --> 00:11:12,000
 discovered an ancient city.

200
00:11:12,000 --> 00:11:14,000
 You should restore that place."

201
00:11:14,000 --> 00:11:17,290
 Then suppose that ancient city was restored so that it

202
00:11:17,290 --> 00:11:18,000
 became prosperous,

203
00:11:18,000 --> 00:11:21,920
 flourishing, opulence and was filled with folk and it grew

204
00:11:21,920 --> 00:11:23,000
 and expanded.

205
00:11:23,000 --> 00:11:26,680
 "In the same way, I have seen an ancient road, an ancient

206
00:11:26,680 --> 00:11:27,000
 path,

207
00:11:27,000 --> 00:11:30,990
 traversed by the fully enlightened Buddha's of former times

208
00:11:30,990 --> 00:11:31,000
.

209
00:11:31,000 --> 00:11:35,000
 And what is that path? It is the Noble Eightfold Path."

210
00:11:35,000 --> 00:11:45,000
 And as a result, he cleared the path and led people down

211
00:11:45,000 --> 00:11:48,000
 the path to the great city

212
00:11:48,000 --> 00:11:52,670
 and many people followed him and entered into the great

213
00:11:52,670 --> 00:11:55,000
 city and dwelt there.

214
00:11:55,000 --> 00:12:01,000
 It's a nice way of putting it.

215
00:12:01,000 --> 00:12:06,000
 The idea of the practice being a path is very important.

216
00:12:06,000 --> 00:12:19,700
 The idea that we are going somewhere to a destination we've

217
00:12:19,700 --> 00:12:21,000
 never been in.

218
00:12:21,000 --> 00:12:27,990
 And so the idea of needing a guide is important to be clear

219
00:12:27,990 --> 00:12:29,000
 on.

220
00:12:29,000 --> 00:12:33,730
 People who think they can just find the way themselves have

221
00:12:33,730 --> 00:12:36,000
 to think of a person

222
00:12:36,000 --> 00:12:39,300
 who has never been down a path and never been to the

223
00:12:39,300 --> 00:12:40,000
 destination.

224
00:12:40,000 --> 00:12:43,000
 How would they know when they are getting closer?

225
00:12:43,000 --> 00:12:46,000
 How would they know when they are on the right path?

226
00:12:46,000 --> 00:12:48,690
 In the most part they wouldn't and very easily get off the

227
00:12:48,690 --> 00:12:50,000
 path,

228
00:12:50,000 --> 00:12:52,000
 fall into the wrong path.

229
00:12:52,000 --> 00:12:55,000
 So having a guide is important.

230
00:12:55,000 --> 00:12:58,990
 I bring this up to people sometimes when they talk about

231
00:12:58,990 --> 00:13:03,000
 just trying to figure things out themselves,

232
00:13:03,000 --> 00:13:06,000
 find their own way.

233
00:13:06,000 --> 00:13:09,290
 There's no reason to think you should be able to find your

234
00:13:09,290 --> 00:13:10,000
 own way.

235
00:13:10,000 --> 00:13:14,000
 It's quite a gamble.

236
00:13:21,000 --> 00:13:25,000
 And like a path, it's something that you have to travel,

237
00:13:25,000 --> 00:13:26,000
 you have to do.

238
00:13:26,000 --> 00:13:29,000
 There's no, it's not just going to come to you.

239
00:13:29,000 --> 00:13:31,180
 You're not just going to sit still for a while and

240
00:13:31,180 --> 00:13:36,000
 enlightenment is going to hit you in the back of the head.

241
00:13:36,000 --> 00:13:40,000
 You have to walk the path.

242
00:13:40,000 --> 00:13:46,000
 If you don't walk the path, you'll never reach the goal.

243
00:13:47,000 --> 00:13:51,480
 Pante, would you just explain briefly what the eightfold

244
00:13:51,480 --> 00:13:52,000
 path is?

245
00:13:52,000 --> 00:13:57,000
 I don't know that my friend understands what that is.

246
00:13:57,000 --> 00:14:00,000
 You tell me.

247
00:14:00,000 --> 00:14:02,000
 Well, it's...

248
00:14:02,000 --> 00:14:04,000
 I'll correct you.

249
00:14:04,000 --> 00:14:07,000
 Okay, that sounds good then.

250
00:14:07,000 --> 00:14:11,660
 It's what Buddhists try to do, living through their daily

251
00:14:11,660 --> 00:14:12,000
 life,

252
00:14:12,000 --> 00:14:17,000
 eight different areas where you try to be very mindful

253
00:14:17,000 --> 00:14:23,000
 and to work towards just being better in each area.

254
00:14:23,000 --> 00:14:28,000
 The first being to have the right intentions.

255
00:14:28,000 --> 00:14:31,000
 Right view is the first one.

256
00:14:31,000 --> 00:14:33,000
 I'm sorry, what was that?

257
00:14:33,000 --> 00:14:35,000
 Right view is the first one.

258
00:14:35,000 --> 00:14:38,210
 Oh, I'm sorry. Okay, so right view, so understanding what

259
00:14:38,210 --> 00:14:39,000
 the teachings are

260
00:14:39,000 --> 00:14:44,000
 and right intentions, not having greed or anger

261
00:14:44,000 --> 00:14:49,000
 or delusion as your motivation, having the right motivation

262
00:14:49,000 --> 00:14:49,000
.

263
00:14:49,000 --> 00:14:52,000
 Right action.

264
00:14:52,000 --> 00:14:54,000
 Speech first.

265
00:14:54,000 --> 00:14:57,000
 Oh, I'm sorry. I always give them the wrong order.

266
00:14:57,000 --> 00:15:04,000
 Right speech, not being harsh with people, not lying

267
00:15:04,000 --> 00:15:09,520
 or spreading rumors or saying the wrong thing, saying mean

268
00:15:09,520 --> 00:15:10,000
 things.

269
00:15:10,000 --> 00:15:16,000
 Right action, not killing people or sexual misconduct.

270
00:15:16,000 --> 00:15:18,000
 That's a big one.

271
00:15:18,000 --> 00:15:22,000
 Not stealing, things like that.

272
00:15:22,000 --> 00:15:27,000
 Right livelihood.

273
00:15:27,000 --> 00:15:30,000
 There are certain jobs that Buddhists shouldn't have.

274
00:15:30,000 --> 00:15:35,000
 That involve killing or selling poisons and

275
00:15:35,000 --> 00:15:40,000
 jobs that involve harm to being so anything that would

276
00:15:40,000 --> 00:15:41,000
 involve killing

277
00:15:41,000 --> 00:15:47,000
 like being a butcher or a fisherman or selling poisons.

278
00:15:47,000 --> 00:15:52,000
 And I think there are some other ones and anything that

279
00:15:52,000 --> 00:15:53,000
 would involve

280
00:15:53,000 --> 00:15:57,000
 human trafficking or slavery or anything like that.

281
00:15:59,000 --> 00:16:04,000
 And after right livelihood, right concentration.

282
00:16:04,000 --> 00:16:05,000
 Effort.

283
00:16:05,000 --> 00:16:08,000
 Oh, I'm sorry. Right effort.

284
00:16:08,000 --> 00:16:13,000
 Right concentration, which is mindful.

285
00:16:13,000 --> 00:16:16,000
 I'm sorry. I'm really making a mess of this.

286
00:16:16,000 --> 00:16:18,000
 Concentration is the last one.

287
00:16:18,000 --> 00:16:20,000
 Okay, so right effort.

288
00:16:20,000 --> 00:16:25,330
 Putting a proper wholesome effort into learning it and

289
00:16:25,330 --> 00:16:27,000
 practicing all of this.

290
00:16:27,000 --> 00:16:35,000
 I'm sorry, I forgot the one after effort.

291
00:16:35,000 --> 00:16:37,000
 Right effort, then right mindfulness.

292
00:16:37,000 --> 00:16:42,000
 Then right mindfulness, being mindful of what you're doing.

293
00:16:42,000 --> 00:16:46,000
 Right concentration, which is involved with meditation.

294
00:16:46,000 --> 00:16:50,000
 And then.

295
00:16:53,000 --> 00:16:58,410
 I'm sorry, just because I'm on the spot. I'm totally

296
00:16:58,410 --> 00:16:59,000
 forgetting everything.

297
00:16:59,000 --> 00:17:01,000
 What's after right concentration?

298
00:17:01,000 --> 00:17:05,500
 Well, it's the one dealing with insight, but right

299
00:17:05,500 --> 00:17:06,000
 mindfulness.

300
00:17:06,000 --> 00:17:09,250
 No, no, actually, that was a trick question. Right

301
00:17:09,250 --> 00:17:11,000
 concentration is number eight.

302
00:17:11,000 --> 00:17:12,000
 Okay.

303
00:17:12,000 --> 00:17:14,000
 So you got eight. That was eight.

304
00:17:14,000 --> 00:17:20,210
 But actually, after right concentration comes Samanjana,

305
00:17:20,210 --> 00:17:22,000
 which is number nine.

306
00:17:23,000 --> 00:17:25,000
 Which is right knowledge.

307
00:17:25,000 --> 00:17:31,170
 Which is just to get it to see the truth and to understand

308
00:17:31,170 --> 00:17:35,000
 that which allows you to let go.

309
00:17:35,000 --> 00:17:40,000
 To see, to know the truth.

310
00:17:40,000 --> 00:17:43,000
 But nothing is worth clinging to.

311
00:17:43,000 --> 00:17:47,020
 Just having this epiphany, not really an epiphany, but this

312
00:17:47,020 --> 00:17:50,000
 conclusion that comes from the observation

313
00:17:51,000 --> 00:17:52,000
 of reality.

314
00:17:52,000 --> 00:17:58,000
 And after Samanjana and Samanimuti, this right freedom,

315
00:17:58,000 --> 00:18:00,000
 right liberation.

316
00:18:00,000 --> 00:18:09,080
 So having seen the truth, you're freed from the shackles of

317
00:18:09,080 --> 00:18:11,000
 delusion.

318
00:18:11,000 --> 00:18:15,000
 And you don't cling to things anymore.

319
00:18:15,000 --> 00:18:18,000
 You don't do things that cause yourself harm.

320
00:18:19,000 --> 00:18:22,000
 You don't do things that create suffering.

321
00:18:22,000 --> 00:18:31,000
 Thank you, Bhante.

322
00:18:31,000 --> 00:18:33,000
 Thank you.

323
00:18:33,000 --> 00:18:36,000
 I think I need to practice up on my eightfold path.

324
00:18:36,000 --> 00:18:38,000
 Good.

325
00:18:38,000 --> 00:18:44,270
 I have read about unconscious beings, asana-sattva, from

326
00:18:44,270 --> 00:18:46,000
 one of the realms of existence.

327
00:18:47,000 --> 00:18:49,000
 Do we know much about these beings?

328
00:18:49,000 --> 00:18:52,000
 I don't know much about those beings.

329
00:18:52,000 --> 00:18:57,000
 I think some people do, but they are a bit of a mystery.

330
00:18:57,000 --> 00:19:04,330
 My teacher said, "There has to be some awareness or

331
00:19:04,330 --> 00:19:06,000
 something."

332
00:19:06,000 --> 00:19:10,000
 He figures there has to be something.

333
00:19:11,000 --> 00:19:17,630
 Asana-sattva means they don't have any mind, but he said

334
00:19:17,630 --> 00:19:19,000
 there has to be mind.

335
00:19:19,000 --> 00:19:22,730
 I'm befuddled as well if there's no mind how it can

336
00:19:22,730 --> 00:19:24,000
 continue on.

337
00:19:24,000 --> 00:19:28,410
 The Abhidhamma scholars will tell you it's because of this

338
00:19:28,410 --> 00:19:31,600
 or that, but practically speaking it doesn't make much

339
00:19:31,600 --> 00:19:32,000
 sense.

340
00:19:34,000 --> 00:19:42,000
 [Silence]

341
00:19:42,000 --> 00:19:45,300
 Would that be different than a person who is unconscious in

342
00:19:45,300 --> 00:19:48,040
 a human realm, just for example being in a coma or

343
00:19:48,040 --> 00:19:49,000
 something?

344
00:19:49,000 --> 00:19:53,490
 I don't know what a coma is like, but I think it's supposed

345
00:19:53,490 --> 00:19:55,000
 to be different.

346
00:19:55,000 --> 00:19:59,000
 It's being born without a mind. It's a good idea.

347
00:20:01,000 --> 00:20:06,010
 But it doesn't say asana means non-recipient, not aware of

348
00:20:06,010 --> 00:20:07,000
 anything.

349
00:20:07,000 --> 00:20:10,000
 So maybe it's being born without senses.

350
00:20:10,000 --> 00:20:13,590
 It's a Brahma realm, I think. It's considered to be one of

351
00:20:13,590 --> 00:20:15,000
 the Brahma realms.

352
00:20:15,000 --> 00:20:22,000
 How does right view differ from right knowledge?

353
00:20:27,000 --> 00:20:30,000
 Robin, how does right view differ from right knowledge?

354
00:20:30,000 --> 00:20:34,000
 Oh, right knowledge.

355
00:20:34,000 --> 00:20:38,000
 Right view...

356
00:20:49,000 --> 00:20:55,500
 Well, if you get technical, all of the eightfold path

357
00:20:55,500 --> 00:21:01,430
 factors together, when they are perfect, that is right

358
00:21:01,430 --> 00:21:03,000
 knowledge.

359
00:21:03,000 --> 00:21:08,350
 So the ninth one is just a way of saying, talking about

360
00:21:08,350 --> 00:21:13,440
 that moment when they come together and when your view

361
00:21:13,440 --> 00:21:15,000
 becomes knowledge.

362
00:21:18,000 --> 00:21:21,330
 So up until that point, it's kind of becoming clearer to

363
00:21:21,330 --> 00:21:25,560
 you that that's the case, but at the moment that it becomes

364
00:21:25,560 --> 00:21:28,000
 knowledge that someone has.

365
00:21:28,000 --> 00:21:32,000
 I mean, it's the Buddha's teaching, right?

366
00:21:32,000 --> 00:21:35,480
 Many of the Buddha's teachings are teachings. They're meant

367
00:21:35,480 --> 00:21:36,000
 to teach you.

368
00:21:36,000 --> 00:21:39,150
 So the person listening is supposed to learn something. It

369
00:21:39,150 --> 00:21:44,150
's not meant to be putting things in... putting pegs in

370
00:21:44,150 --> 00:21:45,000
 holes.

371
00:21:46,000 --> 00:21:50,490
 It's not meant to be theoretical. So there's a license

372
00:21:50,490 --> 00:21:52,000
 being taken here.

373
00:21:52,000 --> 00:21:55,060
 But that's what it means when the eightfold path factors

374
00:21:55,060 --> 00:21:59,080
 become... when you practice the eightfold path factors,

375
00:21:59,080 --> 00:22:01,760
 when they become mature, then there's the moment of Samyana

376
00:22:01,760 --> 00:22:02,000
.

377
00:22:02,000 --> 00:22:07,000
 And with right knowledge, then there's right freedom.

378
00:22:07,000 --> 00:22:12,000
 So it's not actually different.

379
00:22:13,000 --> 00:22:15,230
 Except that when you could say when right view has the

380
00:22:15,230 --> 00:22:18,000
 other seven path factors together and they're all purified,

381
00:22:18,000 --> 00:22:23,000
 then when they're all perfect, then there is Samyana.

382
00:22:23,000 --> 00:22:30,000
 Banté, may you speak on the stillness in the practice of

383
00:22:30,000 --> 00:22:33,000
 meditation? Thank you.

384
00:22:35,000 --> 00:22:40,390
 It's called Pasadhi. It's one of the potential defilements

385
00:22:40,390 --> 00:22:44,030
 of insight because people who become still get attached to

386
00:22:44,030 --> 00:22:47,510
 the stillness and think that they've attained something

387
00:22:47,510 --> 00:22:49,000
 special and they follow it.

388
00:22:49,000 --> 00:22:52,600
 They think that's the path. And since it's not the path,

389
00:22:52,600 --> 00:22:54,000
 they get off the path.

390
00:22:54,000 --> 00:22:57,730
 So when you're still, you should acknowledge to yourself

391
00:22:57,730 --> 00:23:00,000
 still, still, quiet, quiet.

392
00:23:04,000 --> 00:23:08,180
 It's not a bad thing, but it's something that can get in

393
00:23:08,180 --> 00:23:12,000
 the way of seeing clearly, if you're not mindful of it.

394
00:23:12,000 --> 00:23:25,000
 So tomorrow is the campfire at McMaster?

395
00:23:25,000 --> 00:23:27,000
 Yeah.

396
00:23:27,000 --> 00:23:28,000
 That sounds nice.

397
00:23:28,000 --> 00:23:30,450
 Buddhist campfire. Yeah, maybe we'll have someone take

398
00:23:30,450 --> 00:23:31,000
 pictures.

399
00:23:31,000 --> 00:23:33,000
 We should.

400
00:23:33,000 --> 00:23:35,000
 Someone should take pictures.

401
00:23:35,000 --> 00:23:40,000
 If you can take pictures in the dark.

402
00:23:40,000 --> 00:23:43,000
 If you have a good camera, you can.

403
00:23:43,000 --> 00:23:56,000
 So is that it for our questions?

404
00:23:56,000 --> 00:23:59,000
 We are all caught up on questions.

405
00:24:02,000 --> 00:24:05,000
 Thank you, everyone, for coming out.

406
00:24:05,000 --> 00:24:09,000
 Thanks, Robin, for your help and for your teachings.

407
00:24:09,000 --> 00:24:13,000
 Thank you, Mom. Thank you for straightening me out.

408
00:24:13,000 --> 00:24:16,000
 And good night, everyone.

409
00:24:16,000 --> 00:24:17,000
 Good night.

410
00:24:18,000 --> 00:24:19,000
 Thank you.

411
00:24:21,000 --> 00:24:22,000
 Thank you.

