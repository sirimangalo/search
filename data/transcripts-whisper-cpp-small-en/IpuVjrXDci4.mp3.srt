1
00:00:00,000 --> 00:00:03,680
 I wonder if you could talk about depression. Is it an

2
00:00:03,680 --> 00:00:07,600
 energy? I understand anger is an energy, but depression

3
00:00:07,600 --> 00:00:11,260
 Do you think it is an illness energy or mindset? Hope you

4
00:00:11,260 --> 00:00:12,260
 don't mind me asking

5
00:00:12,260 --> 00:00:19,960
 Anger being an energy. No anger isn't an energy anger. Okay

6
00:00:19,960 --> 00:00:23,560
 Buddhist Buddhist

7
00:00:23,560 --> 00:00:26,360
 Theory breaks things down

8
00:00:26,720 --> 00:00:29,400
 they even said that the in our tradition they said that the

9
00:00:29,400 --> 00:00:29,920
 Buddha was a

10
00:00:29,920 --> 00:00:33,000
 We budge a wad in someone who?

11
00:00:33,000 --> 00:00:37,010
 Splits things up or dissects things breaks things up into

12
00:00:37,010 --> 00:00:40,720
 its constituent parts. That's what best described the

13
00:00:40,720 --> 00:00:41,040
 Buddha

14
00:00:41,040 --> 00:00:44,270
 it's an interesting interesting quote, but that ended up

15
00:00:44,270 --> 00:00:44,680
 being the

16
00:00:44,680 --> 00:00:48,580
 The Orthodox interpretation of the Buddha's teaching was

17
00:00:48,580 --> 00:00:51,080
 that the Buddha broke things up instead of allowing for

18
00:00:51,080 --> 00:00:53,560
 entities to exist and

19
00:00:53,560 --> 00:00:55,800
 so we do this with

20
00:00:56,040 --> 00:00:58,650
 Emotions and I think it's very important to do it with

21
00:00:58,650 --> 00:01:00,080
 emotions. I agree with this

22
00:01:00,080 --> 00:01:03,160
 interpretation

23
00:01:03,160 --> 00:01:06,030
 Anger at its very essence is simply disliking. It's a

24
00:01:06,030 --> 00:01:06,620
 judgment

25
00:01:06,620 --> 00:01:10,320
 The state of being angry

26
00:01:10,320 --> 00:01:13,080
 Doesn't just contain that judgment it contains

27
00:01:13,080 --> 00:01:16,060
 judgment after judgment after judgment moment after moment

28
00:01:16,060 --> 00:01:17,560
 after moment of judgment and

29
00:01:17,560 --> 00:01:21,500
 The results of those judgments which is a lot of energy a

30
00:01:21,500 --> 00:01:24,440
 lot of stress a lot of pain

31
00:01:25,080 --> 00:01:27,840
 Heat in the body it can be associated with headaches in the

32
00:01:27,840 --> 00:01:28,160
 head

33
00:01:28,160 --> 00:01:32,250
 Shaking in the body red in the face blood pumping heart

34
00:01:32,250 --> 00:01:32,960
 pumping

35
00:01:32,960 --> 00:01:37,240
 but these are all results and and

36
00:01:37,240 --> 00:01:40,760
 Habits that have become a part of the system of what we

37
00:01:40,760 --> 00:01:41,720
 call the body

38
00:01:41,720 --> 00:01:47,320
 What we call the body is simply the habits of the mind that

39
00:01:47,320 --> 00:01:48,080
 have become

40
00:01:48,080 --> 00:01:52,000
 Lifetime after lifetime become what we call human

41
00:01:52,000 --> 00:01:54,000
 So

42
00:01:54,000 --> 00:01:59,600
 That's about anger now depression is a little bit

43
00:01:59,600 --> 00:02:03,390
 More intricate but it can still be broken down into its

44
00:02:03,390 --> 00:02:04,360
 constituent parts

45
00:02:04,360 --> 00:02:07,200
 And the only way to deal with it is to break it down into

46
00:02:07,200 --> 00:02:09,200
 its constituent parts. We talked about this

47
00:02:09,200 --> 00:02:11,350
 I've talked about this before in regards to addiction

48
00:02:11,350 --> 00:02:12,680
 pornography. We were talking about

49
00:02:12,680 --> 00:02:16,220
 how you can break it down in regards to the

50
00:02:16,220 --> 00:02:19,870
 Looking at something beautiful or attractive and then

51
00:02:19,870 --> 00:02:21,160
 feeling good about it

52
00:02:21,760 --> 00:02:24,720
 And then want and then as a result wanting that feeling or

53
00:02:24,720 --> 00:02:27,000
 a tad craving that clean that feeling

54
00:02:27,000 --> 00:02:29,870
 Once you crave the feeling then you act for it again. So if

55
00:02:29,870 --> 00:02:30,720
 you break it up into these

56
00:02:30,720 --> 00:02:35,550
 These or you you act to bring it about again when you act

57
00:02:35,550 --> 00:02:36,300
 to bring it out again

58
00:02:36,300 --> 00:02:38,280
 You see it again when you see it again

59
00:02:38,280 --> 00:02:40,800
 Then there's the good feeling and then there's more liking

60
00:02:40,800 --> 00:02:42,800
 and you have this feedback loop

61
00:02:42,800 --> 00:02:45,380
 Once you break it up into these parts the seeing the

62
00:02:45,380 --> 00:02:46,620
 feeling and the liking

63
00:02:47,720 --> 00:02:50,420
 Then you can go back and forth between these two and these

64
00:02:50,420 --> 00:02:52,120
 three and you can come to see that

65
00:02:52,120 --> 00:02:54,940
 see what you're doing to yourself and what's causing the

66
00:02:54,940 --> 00:02:58,420
 suffering and and see that there's actually no entity there

67
00:02:58,420 --> 00:02:58,800
's actually

68
00:02:58,800 --> 00:03:02,720
 nothing to it the seeing is is meaningless the

69
00:03:02,720 --> 00:03:06,320
 Pleasant feeling is also meaningless and the craving is

70
00:03:06,320 --> 00:03:10,190
 also meaningless. There's no reason for one to lead to the

71
00:03:10,190 --> 00:03:10,680
 other

72
00:03:10,680 --> 00:03:13,700
 Or one to necessitate the other they have there's no

73
00:03:13,700 --> 00:03:14,280
 logical

74
00:03:14,280 --> 00:03:17,000
 relationship so

75
00:03:17,480 --> 00:03:20,010
 With but with depression you can break it up in a similar

76
00:03:20,010 --> 00:03:21,800
 way. So with depression you have and

77
00:03:21,800 --> 00:03:25,710
 I'm not an expert on this but thinking back to to my

78
00:03:25,710 --> 00:03:27,520
 depression you have of course anger

79
00:03:27,520 --> 00:03:31,560
 The base of depression is still anger. It's a it's what we

80
00:03:31,560 --> 00:03:35,080
 call anger meaning the state of disliking the partiality

81
00:03:35,080 --> 00:03:37,800
 Because you're not happy about something

82
00:03:37,800 --> 00:03:40,000
 It can often be a thought

83
00:03:40,000 --> 00:03:45,400
 And it's generally a reoccurring thought

84
00:03:47,200 --> 00:03:49,200
 And

85
00:03:49,200 --> 00:03:51,200
 It's a perception

86
00:03:51,200 --> 00:03:54,360
 It could be a perception based on your current situation

87
00:03:54,360 --> 00:03:57,510
 It can be a memory based on something that is passed. It

88
00:03:57,510 --> 00:04:00,900
 can be something about the future that looks bleak and and

89
00:04:00,900 --> 00:04:04,000
 unappealing and so on

90
00:04:04,000 --> 00:04:08,550
 But then one one thing that I think binds together these

91
00:04:08,550 --> 00:04:12,800
 emotions that makes us call them depression is the

92
00:04:13,800 --> 00:04:18,160
 habit how it becomes habitual it's a

93
00:04:18,160 --> 00:04:23,060
 simmering sort of anger that becomes this habit where we

94
00:04:23,060 --> 00:04:26,600
 we turn inward on ourselves and

95
00:04:26,600 --> 00:04:33,030
 React to the anger we don't like something we're not happy

96
00:04:33,030 --> 00:04:35,560
 about something and we react by

97
00:04:35,560 --> 00:04:39,860
 closing down by repressing by mulling by

98
00:04:39,860 --> 00:04:42,520
 fuming so

99
00:04:43,160 --> 00:04:46,050
 The difference with angers with different kinds of anger

100
00:04:46,050 --> 00:04:49,240
 isn't the disliking it isn't the the anger itself. It's how

101
00:04:49,240 --> 00:04:50,320
 we deal with it and

102
00:04:50,320 --> 00:04:53,480
 Or it's how we react to it

103
00:04:53,480 --> 00:04:56,330
 Some people react to bad things by getting furious about

104
00:04:56,330 --> 00:04:56,640
 them

105
00:04:56,640 --> 00:04:59,520
 Some people react to anger by getting depressed about them

106
00:04:59,520 --> 00:05:02,920
 the furious means that person has the tendency to fight

107
00:05:02,920 --> 00:05:06,320
 the depression means that person has the tendency to

108
00:05:06,320 --> 00:05:08,960
 retract within themselves and and

109
00:05:08,960 --> 00:05:12,240
 Go back and obsess over them

110
00:05:12,880 --> 00:05:14,560
 it's

111
00:05:14,560 --> 00:05:16,560
 quite intricate but

112
00:05:16,560 --> 00:05:20,620
 Must be dealt with by breaking it down into its constituent

113
00:05:20,620 --> 00:05:23,680
 part. So first there is the disliking and

114
00:05:23,680 --> 00:05:27,680
 Then there is the pain or the the sadness

115
00:05:27,680 --> 00:05:31,200
 depression is often related with sadness and

116
00:05:31,200 --> 00:05:37,440
 This brooding sort of feeling

117
00:05:39,680 --> 00:05:43,360
 but you have to be able to catch the the the disliking and

118
00:05:43,360 --> 00:05:46,730
 Then catch the feeling and then catch the thought you have

119
00:05:46,730 --> 00:05:49,440
 to be able to break it up into these three parts

120
00:05:49,440 --> 00:05:51,960
 There's whatever it is that's making you depressed

121
00:05:51,960 --> 00:05:54,960
 Find that and it could be many things

122
00:05:54,960 --> 00:05:57,680
 For many people it is a number of things

123
00:05:57,680 --> 00:06:00,380
 But whenever one of those things comes up you say thinking

124
00:06:00,380 --> 00:06:02,320
 thinking or remembering remembering or so

125
00:06:02,320 --> 00:06:05,600
 Or if it's seeing or hearing that you see something that

126
00:06:05,600 --> 00:06:08,350
 makes you depressed and seeing it again and again makes you

127
00:06:08,350 --> 00:06:09,000
 depressed

128
00:06:09,640 --> 00:06:12,640
 so say to yourself seeing seeing when then there's the

129
00:06:12,640 --> 00:06:17,150
 Unhappiness you say unhappy or pain pain or so on or

130
00:06:17,150 --> 00:06:22,360
 sadness and and when you dislike it disliking disliking

131
00:06:22,360 --> 00:06:26,480
 That's the way with everything so I don't think it helps to

132
00:06:26,480 --> 00:06:29,000
 think of whether it's an energy or

133
00:06:29,000 --> 00:06:32,680
 illness or a mindset it's it's a

134
00:06:32,680 --> 00:06:37,720
 Conglomeration that becomes habitual of various things

135
00:06:38,840 --> 00:06:41,370
 One important thing that you could look up is the teaching

136
00:06:41,370 --> 00:06:44,000
 on the Buddha of paticca samupada

137
00:06:44,000 --> 00:06:48,090
 Which is basically and in what I was explaining in brief

138
00:06:48,090 --> 00:06:48,240
 that

139
00:06:48,240 --> 00:06:51,680
 passa sala yatanat patya passo

140
00:06:51,680 --> 00:06:56,320
 The six senses lead to contact passo patya veda nawedana

141
00:06:56,320 --> 00:07:00,410
 is cause the contact with the senses seeing hearing

142
00:07:00,410 --> 00:07:04,040
 smelling tasting feeling thinking leads to

143
00:07:04,960 --> 00:07:07,720
 Feelings happy feelings and happy feelings neutral feelings

144
00:07:07,720 --> 00:07:10,800
 veda nawedana patya tanha so craving

145
00:07:10,800 --> 00:07:14,480
 Wanting something to be wanting something to not be which

146
00:07:14,480 --> 00:07:15,780
 would be anger

147
00:07:15,780 --> 00:07:18,920
 comes from

148
00:07:18,920 --> 00:07:21,320
 Comes from the feelings so

149
00:07:21,320 --> 00:07:24,360
 When you be it when you break them up in this way and when

150
00:07:24,360 --> 00:07:25,920
 you get rid of your ignorance about this

151
00:07:25,920 --> 00:07:28,350
 You come to see what you're doing and you come to see how

152
00:07:28,350 --> 00:07:29,880
 it's leading only to suffering

153
00:07:31,360 --> 00:07:34,490
 That you're able to change the habit because the habits are

154
00:07:34,490 --> 00:07:35,000
 based on

155
00:07:35,000 --> 00:07:38,150
 Misunderstanding you know it's it's because you think that

156
00:07:38,150 --> 00:07:39,600
 it's going to make you happy

157
00:07:39,600 --> 00:07:43,480
 It's because you think that it's the right way or because

158
00:07:43,480 --> 00:07:44,040
 of a habit

159
00:07:44,040 --> 00:07:48,940
 Because you are conditioned to act in that way because you

160
00:07:48,940 --> 00:07:50,610
're not realizing you don't see what you're doing to

161
00:07:50,610 --> 00:07:51,920
 yourself that you do it

162
00:07:51,920 --> 00:07:54,370
 Once you see what you're doing to yourself. You don't you

163
00:07:54,370 --> 00:07:56,160
 stop doing that you stop hurting yourself

164
00:07:56,160 --> 00:07:58,160
 so

