 Good evening everyone.
 Broadcasting live December 12th, 2015.
 Maybe a short broadcast tonight.
 I'm realizing that I have quite a bit of studying left to
 do for the next two exams.
 I haven't taken these classes probably as seriously as I
 should have.
 I had to do a lot of reading.
 Probably not going to do that well on either of these exams
.
 What subjects, Pante?
 Linguistics and philosophy.
 Monday we're having a study session, so two of my
 classmates are coming over.
 We're going to sit down for the linguistics exam.
 I'm not really worried about it because that's a first year
 class.
 I don't think it really matters either way as long as I
 pass it, which I will.
 But somehow I was talking with another student about this
 for the Latin exam.
 I was talking to someone about how it feels kind of wrong
 to not do your best at something.
 It's strange.
 There's still an intention to get 100% in everything.
 I don't get 100% while a lesson has to be learned to try to
 get 100% next time,
 which is a difficult position to be in.
 We'll see if I get 100% in Latin.
 Wouldn't that be funny?
 I had 100% going into the exam.
 Probably got a 95.
 So Latin was last week's?
 There was a bonus question.
 I asked if there was going to be a bonus section because he
 puts bonus on all the quizzes,
 and they're actually quite generous.
 So I actually on each of my quizzes I got over 100% because
 of the bonus.
 I think average was like 103%.
 So there was bonus on the exam, but the bonus was actually
 quite cheap.
 Out of 200 marks, it was a 3-mark bonus question.
 But why I bring it up, and this is actually, it is related.
 It was an interesting quote, interesting probably to many
 of us,
 because he brought up this quote that had come up recently
 in the media, a Latin quote.
 And in regards to one of the biggest stories of the past
 month,
 if anyone knows what that quote was, because it's actually
 quite a famous quote.
 I had, you know, not being a news person, I hadn't seen it,
 but he brought up a picture,
 and it's now on the Wikipedia page for that quote.
 The quote is "Floc duat n'ec morge tour."
 It doesn't ring any bells in it, unless you're really
 keeping up with the news,
 or unless you happen to be from France, because it's a very
 famous quote in France.
 "Floc duat" means "tossed about the fluctuate," right?
 "Floc duat" means "to be shaken or disturbed."
 It's using the imagery of a boat.
 "Floc duat" means "shaken about," "tossed about" by the way
.
 "Nec" means "and not."
 And "merge tour."
 "Merge tour" means "merge tour" maybe.
 "Merge" is not "merge," it's from "submerge."
 So "merge tour" means "sank."
 So a boat, a successful ship, is one that is tossed about
 but not submerged, not sunk.
 I just barely remembered him bringing up and forgot totally
 what it was about,
 but I kind of guessed, and actually I think I got it right.
 But tossed about and not submerged.
 I think it's more like tossed about and not sunk, but
 basically that.
 Anyway, it was used, it's been used for many occasions in
 France to talk about how they are "resilient."
 It's a quote about resiliency, but they also used it for
 the Paris bombings
 that show that they weren't going to be defeated, weren't
 going to let this defeat them.
 It's a quote about resiliency, which I think is a very good
 quote from Buddhism, from Buddhist practice.
 And it's a very good Buddhist response to things like
 terrorist attacks
 and the difficulties in life.
 Shaken, yes, never sunk.
 There's always another chance. You always get another
 chance.
 We have the Buddha as our example of that, the Buddha who
 took four uncountable eons
 and 100,000 countable eons, great eons, to become a Buddha.
 All the time it took him, all the mistakes he had to make
 to get there.
 And I think that our one little life is not really worth
 all the concern we give it.
 Like there's that Zen book that my mother got and showed to
 me,
 "Don't sweat the small stuff, and it's all small stuff."
 No questions tonight?
 I don't think so.
 We have here a quote about an acrobat, right?
 Yes, I like that quote.
 It says "protect", but protect is a poor translation.
 It is a literal translation, but it means guard. Guard
 would probably be better.
 Watch out for raka, but raka, guard is better.
 I will...
 He does use guard. He watched and guarded by himself for
 the first part,
 but then in the second part he says, "No master."
 But then he switches to protect, which is weird. Guard.
 How do we guard ourselves?
 We practice the four foundations of mindfulness,
 and by protecting ourselves we also protect others.
 This is another interesting part of this.
 Not only is it about everyone doing their own duty,
 and being good people, but it addresses the idea that when
 you do that you also help others.
 Why? Because of the leading to patience, forbearance, harm
lessness, love and compassion.
 By practicing mindfulness, by practicing for one's own
 betterment, one protects others.
 This is in contrast to those people who think the way to be
 a good person,
 the way to do the right thing is to help others.
 It's not really.
 There's a person who helps themselves, who's naturally
 inclined to help others.
 It's not what should be the focus, right?
 If your focus is on helping others, you neglect the source.
 If your focus is on helping yourself, then you radiate
 goodness in all things that you do.
 If we don't have any questions, we can just call it a night
 and say hello.
 Thank you all for meditating with us.
 Tomorrow we have meetings and a mursudimaga.
 I may skip some of the meetings.
 Maybe I'll do my studying while you guys are having the
 meeting, and I'll listen.
 I can study and be there at the same time, I think.
 Yeah, we can just kind of call out to you if we need your
 input on something.
 Sure.
 That sounds good.
 Okay, then. Good night, everyone. Thank you, Robin, for
 joining me.
 Thank you, Bante.
 Good night.
 Thank you.
