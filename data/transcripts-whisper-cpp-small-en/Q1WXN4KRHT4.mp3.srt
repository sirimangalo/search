1
00:00:00,000 --> 00:00:04,000
 I dreamt about my great-grandmother dying and going into a

2
00:00:04,000 --> 00:00:05,280
 light and moaning.

3
00:00:05,280 --> 00:00:09,030
 I had no idea she was dying as I was sleeping and I only

4
00:00:09,030 --> 00:00:10,080
 found out she had

5
00:00:10,080 --> 00:00:13,570
 died a week after I had the dream. Does Buddhism have a

6
00:00:13,570 --> 00:00:17,000
 view on things like this?

7
00:00:18,760 --> 00:00:30,590
 Not really. Remember Buddhism is primarily a practice. So

8
00:00:30,590 --> 00:00:31,600
 ask yourself is

9
00:00:31,600 --> 00:00:37,760
 this practical? No it's not. It fits in with the idea of

10
00:00:37,760 --> 00:00:39,040
 cultivating magical

11
00:00:39,040 --> 00:00:43,320
 powers and so insofar as Buddhism deals with the

12
00:00:43,320 --> 00:00:45,880
 cultivation of magical powers

13
00:00:45,880 --> 00:00:51,640
 it is related. Buddhism has a view on this. It's lumped in

14
00:00:51,640 --> 00:00:53,400
 there with

15
00:00:53,400 --> 00:00:57,230
 supernatural occurrences which can be cultivated. So you

16
00:00:57,230 --> 00:00:58,880
 can have more than

17
00:00:58,880 --> 00:01:03,800
 just this accidental or circumstantial experience. You can

18
00:01:03,800 --> 00:01:05,640
 actually watch people

19
00:01:05,640 --> 00:01:14,330
 die and be born and so on. You can contact spirits, ghosts,

20
00:01:14,330 --> 00:01:17,160
 angels, people far away

21
00:01:17,160 --> 00:01:23,930
 that kind of thing through the cultivation of magical

22
00:01:23,930 --> 00:01:24,560
 powers which are

23
00:01:24,560 --> 00:01:30,850
 not essential and not really not an essential part of the

24
00:01:30,850 --> 00:01:31,840
 path anyway.

25
00:01:31,840 --> 00:01:39,720
 They're a byproduct, a positive byproduct in a worldly

26
00:01:39,720 --> 00:01:46,570
 sense of the practice and have in the end no clear purpose.

27
00:01:46,570 --> 00:01:48,200
 The best they could

28
00:01:48,200 --> 00:01:53,320
 do is help to open up one's mind to the idea of the

29
00:01:53,320 --> 00:01:56,480
 importance of the mind and

30
00:01:56,480 --> 00:02:00,390
 the idea of the survival of the mind after the physical

31
00:02:00,390 --> 00:02:01,360
 death, that kind of

32
00:02:01,360 --> 00:02:09,600
 thing. But I have some thoughts on this. It's especially

33
00:02:09,600 --> 00:02:10,880
 because it's

34
00:02:10,880 --> 00:02:15,360
 prevalent, it's common. It's hard to find people who haven

35
00:02:15,360 --> 00:02:16,400
't heard such stories.

36
00:02:16,400 --> 00:02:20,330
 Many people have themselves experienced these sorts of

37
00:02:20,330 --> 00:02:21,440
 things and when

38
00:02:21,440 --> 00:02:26,830
 they're told it's whatever, some kind of confabulation or

39
00:02:26,830 --> 00:02:27,800
 wishful

40
00:02:27,800 --> 00:02:32,960
 thinking or so on, it just doesn't... I was even told when

41
00:02:32,960 --> 00:02:34,040
 I was talking about my

42
00:02:34,040 --> 00:02:35,980
 experience someone just said, "Well you're lying," or "I

43
00:02:35,980 --> 00:02:37,680
 think you're lying." Which

44
00:02:37,680 --> 00:02:41,160
 doesn't help me at all because I know I'm not lying. And

45
00:02:41,160 --> 00:02:42,440
 then when you have

46
00:02:42,440 --> 00:02:46,950
 thousands or hundreds and thousands of these stories, near-

47
00:02:46,950 --> 00:02:48,240
death experiences,

48
00:02:48,240 --> 00:02:53,320
 experiences of people who have died or dying, it's common

49
00:02:53,320 --> 00:02:54,800
 and quite common

50
00:02:54,800 --> 00:02:57,560
 for it to be the case where the person who's having the

51
00:02:57,560 --> 00:02:59,440
 experience had no

52
00:02:59,440 --> 00:03:01,940
 idea that the person was dying, didn't even know the person

53
00:03:01,940 --> 00:03:03,240
 was sick in some

54
00:03:03,240 --> 00:03:09,680
 cases. Lots of strange things of this sort. And to just

55
00:03:09,680 --> 00:03:10,720
 disregard them I

56
00:03:10,720 --> 00:03:20,080
 think is a symptom of closed-mindedness, bigotry and scient

57
00:03:20,080 --> 00:03:20,680
ism.

58
00:03:20,680 --> 00:03:34,240
 It means clinging to that which is known, that which is

59
00:03:34,240 --> 00:03:35,400
 believed in the

60
00:03:35,400 --> 00:03:39,330
 sense, that which is accepted as known. Or the clinging to

61
00:03:39,330 --> 00:03:40,840
 accepted theories and

62
00:03:40,840 --> 00:03:44,200
 ideas of like materialism, for the most part. The idea that

63
00:03:44,200 --> 00:03:44,680
 everything is

64
00:03:44,680 --> 00:03:49,000
 physical and therefore these sorts of things can't happen.

65
00:03:49,000 --> 00:03:50,280
 That's it. But

66
00:03:50,280 --> 00:03:54,200
 Buddhism not really because it's not that practical.

