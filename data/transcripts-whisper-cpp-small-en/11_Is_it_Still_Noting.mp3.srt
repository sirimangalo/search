1
00:00:00,000 --> 00:00:02,000
 Is it still noting?

2
00:00:02,000 --> 00:00:05,920
 Question. The practice of noting as I understand it

3
00:00:05,920 --> 00:00:10,640
 involves thought vocalizing in language a noted phenomenon.

4
00:00:10,640 --> 00:00:14,370
 Is there also a noting without vocalizing the phenomenon in

5
00:00:14,370 --> 00:00:18,430
 thought or is the thought vocalization intrinsic to the

6
00:00:18,430 --> 00:00:19,200
 practice?

7
00:00:19,200 --> 00:00:23,700
 Ask another way if one observes the phenomenon but bypasses

8
00:00:23,700 --> 00:00:28,170
 the mental enunciation in language of what is observed,

9
00:00:28,170 --> 00:00:30,480
 does that qualify as noting?

10
00:00:30,480 --> 00:00:35,420
 Answer. I think there is a bit of a misunderstanding about

11
00:00:35,420 --> 00:00:38,080
 what is going on in your mind generally.

12
00:00:38,080 --> 00:00:42,290
 Thinking is an intrinsic part of our mental activity. We

13
00:00:42,290 --> 00:00:44,080
 are always thinking.

14
00:00:44,080 --> 00:00:48,140
 The practice of meditation is not the creation of a verbal

15
00:00:48,140 --> 00:00:51,360
 word. You are not verbalizing anything.

16
00:00:51,360 --> 00:00:55,020
 Rather you are thinking. But the thought is a clear thought

17
00:00:55,020 --> 00:00:59,360
. The thought has a one-to-one correlation with reality.

18
00:00:59,360 --> 00:01:03,380
 For example, when you see something, it is not me, it is

19
00:01:03,380 --> 00:01:06,850
 not mine, it is not all of the other things that you might

20
00:01:06,850 --> 00:01:07,520
 think of.

21
00:01:07,520 --> 00:01:11,710
 Rather it is just seeing. I know there are many people,

22
00:01:11,710 --> 00:01:16,000
 even experienced meditators, who ask this question or who

23
00:01:16,000 --> 00:01:19,760
 argue that you are better off to not use the noting.

24
00:01:19,760 --> 00:01:23,730
 And that is fine. Different interpretations and different

25
00:01:23,730 --> 00:01:26,320
 practices are perfectly acceptable.

26
00:01:26,320 --> 00:01:30,140
 You could become enlightened without practicing noting and

27
00:01:30,140 --> 00:01:33,650
 certainly there are different techniques of practice that

28
00:01:33,650 --> 00:01:35,360
 lead to enlightenment.

29
00:01:35,360 --> 00:01:39,410
 All I can say is that in my experience, the benefits of

30
00:01:39,410 --> 00:01:43,600
 noting should become obvious if you give it a chance.

31
00:01:43,600 --> 00:01:47,950
 One problem I see is that based on our natural inclination

32
00:01:47,950 --> 00:01:52,110
 to doubt things combined with our natural disinclination

33
00:01:52,110 --> 00:01:53,760
 towards objectivity,

34
00:01:53,760 --> 00:01:58,120
 once we start seeing reality as it is, the mind revolts and

35
00:01:58,120 --> 00:02:01,280
 rejects it because it feels unnatural.

36
00:02:01,280 --> 00:02:05,510
 I often hear it expressed that noting practice feels

37
00:02:05,510 --> 00:02:09,200
 unnatural. But ask yourself, "What is natural?"

38
00:02:09,200 --> 00:02:13,580
 Natural is following your defilements. Natural is thinking.

39
00:02:13,580 --> 00:02:17,890
 This is me, this is mine, this is what I am. I am walking,

40
00:02:17,890 --> 00:02:18,960
 I am sitting.

41
00:02:18,960 --> 00:02:23,340
 Natural is the belief that there is an entity, that things

42
00:02:23,340 --> 00:02:24,960
 are I, me and mine.

43
00:02:24,960 --> 00:02:28,670
 A commentary to the Satipatthana Sutta poses and answers

44
00:02:28,670 --> 00:02:32,400
 the question of what makes the awareness of a meditator

45
00:02:32,400 --> 00:02:35,040
 distinct from ordinary awareness.

46
00:02:35,040 --> 00:02:38,440
 It says that even ordinary animals know when they are

47
00:02:38,440 --> 00:02:42,360
 moving, but their awareness is clouded by delusion and

48
00:02:42,360 --> 00:02:44,000
 conception of soul.

49
00:02:44,000 --> 00:02:47,740
 The awareness that we are trying to cultivate is a very

50
00:02:47,740 --> 00:02:49,920
 specific type of awareness.

51
00:02:49,920 --> 00:02:54,010
 In the Satipatthana Sutta, it describes the meditator state

52
00:02:54,010 --> 00:02:56,240
 as possessing patissati mataya,

53
00:02:56,240 --> 00:03:00,230
 which means with just a state of remembrance of the object

54
00:03:00,230 --> 00:03:01,040
 as it is.

55
00:03:01,040 --> 00:03:04,820
 The awareness that we are trying to evoke is a one-to-one

56
00:03:04,820 --> 00:03:08,160
 recognition of each experience just as it is.

57
00:03:08,160 --> 00:03:12,240
 Such recognition is not a natural state of human beings.

58
00:03:12,240 --> 00:03:15,660
 Our natural state of observation does not contain this one-

59
00:03:15,660 --> 00:03:18,240
to-one remembrance or mindfulness.

60
00:03:18,240 --> 00:03:20,950
 Even though you may believe that you know walking as

61
00:03:20,950 --> 00:03:25,320
 walking, or sitting as sitting, there is usually much more

62
00:03:25,320 --> 00:03:26,960
 going on in the mind.

63
00:03:26,960 --> 00:03:32,390
 Our ordinary inclination is to view our experiences as me,

64
00:03:32,390 --> 00:03:34,320
 as mine, as self.

65
00:03:34,320 --> 00:03:38,620
 Unless our meditation practice forces us to see reality as

66
00:03:38,620 --> 00:03:41,360
 the moment of experience that it is,

67
00:03:41,360 --> 00:03:46,070
 we will inevitably become attached even to our practice,

68
00:03:46,070 --> 00:03:49,840
 conceiving of it as self or belonging to self.

69
00:03:49,840 --> 00:03:54,020
 The fact that noting is unnatural, going against our

70
00:03:54,020 --> 00:03:58,080
 natural inclination, is the actual reason for its use.

71
00:03:58,080 --> 00:04:01,800
 Its purpose is to change our natural inclination to

72
00:04:01,800 --> 00:04:06,160
 interpret and extrapolate our experiences in favor of

73
00:04:06,160 --> 00:04:08,560
 seeing things just as they are.

74
00:04:08,560 --> 00:04:11,820
 Such a change takes considerable efforts more than

75
00:04:11,820 --> 00:04:16,110
 newcomers to meditation might realize, because how set we

76
00:04:16,110 --> 00:04:18,400
 are in our natural ways.

77
00:04:18,400 --> 00:04:22,820
 Meditation forces us to change our entire outlook, far

78
00:04:22,820 --> 00:04:25,760
 beyond what we might expect going in.

