1
00:00:00,000 --> 00:00:04,200
 I'm very prominent in the United States right now.

2
00:00:04,200 --> 00:00:09,120
 Bonta, do you recommend lay Buddhists or people who are

3
00:00:09,120 --> 00:00:11,720
 trying to move towards a more meditative

4
00:00:11,720 --> 00:00:18,140
 lifestyle not to become involved and too concerned with

5
00:00:18,140 --> 00:00:19,600
 politics?

6
00:00:19,600 --> 00:00:23,800
 Well, I don't think there's a difficult answer.

7
00:00:23,800 --> 00:00:25,520
 Of course.

8
00:00:25,520 --> 00:00:27,120
 Don't get involved with anything.

9
00:00:27,120 --> 00:00:30,680
 I mean, do you think politics has some benefit for your

10
00:00:30,680 --> 00:00:31,800
 meditation?

11
00:00:31,800 --> 00:00:34,440
 I mean, I guess...

12
00:00:34,440 --> 00:00:39,340
 I would say outside of their meditative sections, I guess

13
00:00:39,340 --> 00:00:42,560
 he's talking probably life in general

14
00:00:42,560 --> 00:00:46,800
 during the rest of your day or whatever.

15
00:00:46,800 --> 00:00:48,480
 Is it going to get in the way with your meditation?

16
00:00:48,480 --> 00:00:52,820
 No, I mean, in some sense, reading the news and knowing

17
00:00:52,820 --> 00:00:55,080
 what's going on is useful.

18
00:00:55,080 --> 00:01:01,440
 If you think of it as a functional or a duty that you have

19
00:01:01,440 --> 00:01:04,840
 to keep informed so that you're

20
00:01:04,840 --> 00:01:08,710
 able to respond and interact and know what people are

21
00:01:08,710 --> 00:01:11,400
 talking about when they talk about

22
00:01:11,400 --> 00:01:16,960
 the president or so on, they can actually be useful.

23
00:01:16,960 --> 00:01:21,430
 The point is useful on a functional level for someone who's

24
00:01:21,430 --> 00:01:23,360
 dwelling in society.

25
00:01:23,360 --> 00:01:26,260
 If you're trying to move towards a more meditative

26
00:01:26,260 --> 00:01:28,120
 lifestyle, then of course.

27
00:01:28,120 --> 00:01:33,360
 Maybe the question could be how important is it to move

28
00:01:33,360 --> 00:01:34,960
 away from it?

29
00:01:34,960 --> 00:01:38,930
 Because obviously giving up more and more things is a great

30
00:01:38,930 --> 00:01:40,960
 benefit, focusing more and

31
00:01:40,960 --> 00:01:42,960
 more on meditation.

32
00:01:42,960 --> 00:01:46,680
 Where is it on the scale of giving up politics?

33
00:01:46,680 --> 00:01:49,600
 Giving up interest in politics?

34
00:01:49,600 --> 00:01:52,280
 I think that depends how involved you are in politics.

35
00:01:52,280 --> 00:01:56,590
 If you're running someone's campaign or out campaigning for

36
00:01:56,590 --> 00:01:58,680
 someone, then yeah, probably

37
00:01:58,680 --> 00:02:02,320
 it's something that you should make a decision on.

38
00:02:02,320 --> 00:02:08,120
 If it's just watching the polls and looking and seeing who

39
00:02:08,120 --> 00:02:11,360
's winning and so on, then it's

40
00:02:11,360 --> 00:02:17,610
 just an obsession, which a lot of people seem to have, that

41
00:02:17,610 --> 00:02:20,960
 among many obsessions you have

42
00:02:20,960 --> 00:02:21,960
 to see.

43
00:02:21,960 --> 00:02:23,760
 I think the last one that I have where I've given up

44
00:02:23,760 --> 00:02:25,200
 everything but politics, well then

45
00:02:25,200 --> 00:02:29,030
 maybe it's next to go, but I wouldn't worry too much about

46
00:02:29,030 --> 00:02:29,480
 it.

47
00:02:29,480 --> 00:02:33,060
 No, I guess there's more to the question and a better

48
00:02:33,060 --> 00:02:35,800
 answer is talking about how important

49
00:02:35,800 --> 00:02:39,510
 is politics and what place does politics play, because it

50
00:02:39,510 --> 00:02:41,440
 really doesn't play that big of

51
00:02:41,440 --> 00:02:42,440
 a part.

52
00:02:42,440 --> 00:02:51,240
 It doesn't affect your happiness to any great degree.

53
00:02:51,240 --> 00:02:56,280
 I don't know.

54
00:02:56,280 --> 00:02:59,310
 If your country elects a government that is corrupt and so

55
00:02:59,310 --> 00:03:01,000
 on, that's a really, really

56
00:03:01,000 --> 00:03:03,840
 bad thing.

57
00:03:03,840 --> 00:03:07,230
 Kind of sly as a Buddhist how we can get around this, and I

58
00:03:07,230 --> 00:03:09,200
 think it's totally valid, is you

59
00:03:09,200 --> 00:03:14,990
 have to ask yourself why do countries elect corrupt leaders

60
00:03:14,990 --> 00:03:15,120
?

61
00:03:15,120 --> 00:03:16,120
 Because there's a very simple answer.

62
00:03:16,120 --> 00:03:18,550
 It's because the people are corrupt, because the people are

63
00:03:18,550 --> 00:03:19,200
 corrupted.

64
00:03:19,200 --> 00:03:24,400
 The people are easily influenced by liars.

65
00:03:24,400 --> 00:03:27,080
 People are not stupid.

66
00:03:27,080 --> 00:03:30,040
 Most of the politicians seem to think that they are,

67
00:03:30,040 --> 00:03:32,000
 because they put out these ads and

68
00:03:32,000 --> 00:03:39,200
 arguments that are really ... We watch Hollywood.

69
00:03:39,200 --> 00:03:42,480
 People watch Hollywood movies.

70
00:03:42,480 --> 00:03:45,330
 They know what a good actor is, what someone who really

71
00:03:45,330 --> 00:03:47,080
 feels what they're saying, and

72
00:03:47,080 --> 00:03:48,640
 politicians don't.

73
00:03:48,640 --> 00:03:58,000
 They're totally rehashing old lines and so on.

74
00:03:58,000 --> 00:04:03,540
 What I mean to say is, if the people of a country are med

75
00:04:03,540 --> 00:04:07,240
itative, that just goes straight

76
00:04:07,240 --> 00:04:11,990
 to the point, but are pure, are clear in their minds and so

77
00:04:11,990 --> 00:04:14,440
 on, then there's no way that

78
00:04:14,440 --> 00:04:17,200
 they would elect the wrong guy, so to speak.

79
00:04:17,200 --> 00:04:20,690
 They would elect someone who is kind and who has a clear

80
00:04:20,690 --> 00:04:22,840
 mind and who is intelligent and

81
00:04:22,840 --> 00:04:26,040
 who's helping people and so on.

82
00:04:26,040 --> 00:04:30,800
 In a democracy, that's what would happen.

83
00:04:30,800 --> 00:04:37,220
 The problem is that people are selfish and bigoted and full

84
00:04:37,220 --> 00:04:40,080
 of views and opinions and

85
00:04:40,080 --> 00:04:43,880
 have these horrible ideas about what is proper.

86
00:04:43,880 --> 00:04:44,880
 Where do we have to start?

87
00:04:44,880 --> 00:04:46,360
 It's not getting involved in politics.

88
00:04:46,360 --> 00:04:51,080
 It's about changing these people's minds, waking them up.

89
00:04:51,080 --> 00:04:54,680
 That's totally Buddhism.

90
00:04:54,680 --> 00:04:59,380
 If the question is, "Should I be concerned and involved in

91
00:04:59,380 --> 00:05:01,800
 politics for the reason that

92
00:05:01,800 --> 00:05:07,550
 if I'm not, it might lead me to have to live in a country

93
00:05:07,550 --> 00:05:10,880
 with a dictator or someone who

94
00:05:10,880 --> 00:05:17,630
 is totally wreaking havoc on the country's social structure

95
00:05:17,630 --> 00:05:20,360
," then no, you don't have

96
00:05:20,360 --> 00:05:21,360
 to.

97
00:05:21,360 --> 00:05:23,940
 You have to help to change people, because the more people

98
00:05:23,940 --> 00:05:25,280
 you change, the more people

99
00:05:25,280 --> 00:05:27,680
 you're going to have who vote for the right guy or at least

100
00:05:27,680 --> 00:05:29,360
 don't vote for the wrong guy.

101
00:05:29,360 --> 00:05:37,100
 Don't vote for someone who is going to, who is whatever,

102
00:05:37,100 --> 00:05:40,600
 who is corrupt and evil.

103
00:05:40,600 --> 00:05:44,220
 So if you can change people, you change so much in a

104
00:05:44,220 --> 00:05:47,400
 society, you change the entertainment.

105
00:05:47,400 --> 00:05:52,400
 When you change people's attitudes, suddenly the

106
00:05:52,400 --> 00:05:56,440
 entertainment starts to reflect that as

107
00:05:56,440 --> 00:05:59,760
 an example.

108
00:05:59,760 --> 00:06:02,600
 It's a good example because entertainment affects everyone.

109
00:06:02,600 --> 00:06:04,520
 It affects politicians.

110
00:06:04,520 --> 00:06:05,640
 They mirror themselves.

111
00:06:05,640 --> 00:06:08,120
 They want to be like the movie stars.

112
00:06:08,120 --> 00:06:11,460
 I don't know that entertainment would be the perfect

113
00:06:11,460 --> 00:06:14,040
 category, but I'm thinking of America

114
00:06:14,040 --> 00:06:15,440
 now.

115
00:06:15,440 --> 00:06:18,560
 If you can change the entertainment in America, and maybe

116
00:06:18,560 --> 00:06:20,120
 it's just because America is so

117
00:06:20,120 --> 00:06:25,360
 fixated on entertainment, then you can change everything.

118
00:06:25,360 --> 00:06:29,110
 You can change politics, you can change society, you can

119
00:06:29,110 --> 00:06:30,400
 change peoples.

120
00:06:30,400 --> 00:06:36,640
 You can change the whole—that's a scary thing, I suppose.

121
00:06:36,640 --> 00:06:40,080
 Because everyone wants to be like the movie stars.

122
00:06:40,080 --> 00:06:46,310
 Anyway, maybe that's going over the top, but the point is

123
00:06:46,310 --> 00:06:49,440
 sound, I think, that you help

124
00:06:49,440 --> 00:06:52,050
 the people, change the people, change people's thoughts,

125
00:06:52,050 --> 00:06:53,560
 change the direction, give them

126
00:06:53,560 --> 00:06:58,170
 new ideas, good ideas, and you'll change the political

127
00:06:58,170 --> 00:07:00,840
 system much, much better than you

128
00:07:00,840 --> 00:07:05,000
 could by voting or even campaigning for someone who is in

129
00:07:05,000 --> 00:07:05,960
 general.

130
00:07:05,960 --> 00:07:09,960
 There's no such thing as a pure politician, I don't think,

131
00:07:09,960 --> 00:07:11,840
 or it's very hard to find.

132
00:07:11,840 --> 00:07:15,540
 Everyone gets corrupted by it and has to compromise

133
00:07:15,540 --> 00:07:18,240
 themselves by the nature of the system, by

134
00:07:18,240 --> 00:07:24,040
 the nature of the fact that corruption is built into it in

135
00:07:24,040 --> 00:07:25,160
 general.

136
00:07:25,160 --> 00:07:29,150
 The only way to change that is to change people's minds,

137
00:07:29,150 --> 00:07:31,720
 change the people around you, change—start

138
00:07:31,720 --> 00:07:35,800
 making YouTube videos where you tell people good things,

139
00:07:35,800 --> 00:07:37,800
 teach people good stuff.

140
00:07:37,800 --> 00:07:40,280
 Yeah, self-congratulatory.

