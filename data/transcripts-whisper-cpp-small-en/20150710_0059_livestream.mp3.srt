1
00:00:00,000 --> 00:00:07,000
 Okay, good evening.

2
00:00:07,000 --> 00:00:12,420
 Broadcasting live from Stomach Creek Ontario, July 9th,

3
00:00:12,420 --> 00:00:15,000
 2015.

4
00:00:15,000 --> 00:00:21,000
 So we're following along with this book, "Buddha Vaacana"

5
00:00:21,000 --> 00:00:30,510
 or "Words of Buddha" by Venerable Dhammika. 365 days of D

6
00:00:30,510 --> 00:00:31,000
hamma.

7
00:00:31,000 --> 00:00:38,000
 And so we're on number 190, which is July 9th.

8
00:00:38,000 --> 00:00:41,000
 And it has to do with sickness.

9
00:00:41,000 --> 00:00:46,000
 Mental sickness and physical sickness.

10
00:00:46,000 --> 00:00:50,070
 There's lots of people in the world who are without

11
00:00:50,070 --> 00:00:52,000
 physical sickness.

12
00:00:52,000 --> 00:00:55,780
 There are lots of people who think that they are free from

13
00:00:55,780 --> 00:00:57,000
 mental sickness.

14
00:00:57,000 --> 00:01:01,350
 According to the Buddha, it's rare to find someone who

15
00:01:01,350 --> 00:01:05,000
 actually is free from mental sickness.

16
00:01:05,000 --> 00:01:10,000
 Mental sickness means the different... it means...

17
00:01:10,000 --> 00:01:15,000
 The point is it means different things to different people.

18
00:01:15,000 --> 00:01:25,270
 Most people think of mental illness as extreme states of

19
00:01:25,270 --> 00:01:30,000
 abnormality

20
00:01:30,000 --> 00:01:35,000
 that prevent one from functioning in society.

21
00:01:35,000 --> 00:01:39,000
 That seriously hamper one's ability to function.

22
00:01:39,000 --> 00:01:54,180
 This isn't any Buddha's scope on what is and isn't mental

23
00:01:54,180 --> 00:01:59,000
 illness to the Buddha.

24
00:01:59,000 --> 00:02:10,000
 Mental illness is anything but any wrongness of mind.

25
00:02:10,000 --> 00:02:15,000
 So a person who is selfish, selfishness is mental illness.

26
00:02:15,000 --> 00:02:21,450
 A person who is arrogant, that arrogance is a mental

27
00:02:21,450 --> 00:02:23,000
 illness.

28
00:02:23,000 --> 00:02:27,470
 A person who is addicted to something, that addiction is a

29
00:02:27,470 --> 00:02:29,000
 mental illness.

30
00:02:29,000 --> 00:02:40,000
 I've had people take issue to this kind of outlook.

31
00:02:40,000 --> 00:02:44,250
 There's the idea that calling something an illness is

32
00:02:44,250 --> 00:02:48,000
 putting a stigma or a negative connotation,

33
00:02:48,000 --> 00:02:56,780
 but I don't think that's really a valid argument because it

34
00:02:56,780 --> 00:02:59,000
's true.

35
00:02:59,000 --> 00:03:04,000
 It's a fact that these things are problems.

36
00:03:04,000 --> 00:03:09,000
 Calling them illnesses just calls them what they are.

37
00:03:09,000 --> 00:03:22,890
 You know, cold is an illness. It's not terminal, but

38
00:03:22,890 --> 00:03:28,000
 anything that affects the body.

39
00:03:28,000 --> 00:03:30,500
 It's called illness. So we always say the same thing about

40
00:03:30,500 --> 00:03:32,000
 the mind, the mind.

41
00:03:32,000 --> 00:03:38,000
 Anything that affects the mind negatively is an illness.

42
00:03:38,000 --> 00:03:41,000
 So people think that they're free from mental illness.

43
00:03:41,000 --> 00:03:45,000
 Most people think they're good people.

44
00:03:45,000 --> 00:03:54,000
 This is mainly due to our low level of mindfulness.

45
00:03:54,000 --> 00:04:11,000
 We aren't able to see the finer defilements in our minds.

46
00:04:11,000 --> 00:04:15,000
 We aren't able to notice.

47
00:04:15,000 --> 00:04:23,000
 We don't know our own mind.

48
00:04:23,000 --> 00:04:29,000
 Our understanding of our mind is worth it, of course.

49
00:04:29,000 --> 00:04:31,000
 And we're quick to forget.

50
00:04:31,000 --> 00:04:42,610
 We're quick to excuse our failings or suppress the

51
00:04:42,610 --> 00:04:47,000
 recognition of our wrongs and so on.

52
00:04:47,000 --> 00:04:54,150
 So we aren't all that cognizant of our faults and the state

53
00:04:54,150 --> 00:04:56,000
 of our mind.

54
00:04:56,000 --> 00:05:00,600
 The Buddha actually decides this passage, which is actually

55
00:05:00,600 --> 00:05:03,000
 the words of the Buddha.

56
00:05:03,000 --> 00:05:15,660
 The Buddha further delimited illness as coming from four

57
00:05:15,660 --> 00:05:18,000
 root causes.

58
00:05:18,000 --> 00:05:23,000
 So we have four causes of illness.

59
00:05:23,000 --> 00:05:26,000
 Illness can be caused by food.

60
00:05:26,000 --> 00:05:29,000
 Illness can be caused by weather.

61
00:05:29,000 --> 00:05:32,000
 Illness can be caused by karma.

62
00:05:32,000 --> 00:05:35,000
 Illness can be caused by the mind.

63
00:05:35,000 --> 00:05:44,000
 Any one of these four can be a cause for sickness.

64
00:05:44,000 --> 00:05:51,000
 These four are considered to be the root causes of illness.

65
00:05:51,000 --> 00:05:53,000
 And they should be taken seriously.

66
00:05:53,000 --> 00:06:04,000
 The Buddha also said, quite famously among those who know,

67
00:06:04,000 --> 00:06:08,000
 the Buddha said, "Aro Gaya Paramah Laba."

68
00:06:08,000 --> 00:06:15,000
 Freedom from illness is the greatest laba, the game, the

69
00:06:15,000 --> 00:06:19,000
 greatest game,

70
00:06:19,000 --> 00:06:25,000
 the greatest possession he makes.

71
00:06:25,000 --> 00:06:30,000
 So it's a serious subject and even physically speaking,

72
00:06:30,000 --> 00:06:34,130
 we shouldn't discount the benefits of being free from

73
00:06:34,130 --> 00:06:46,000
 illness, from physical illness.

74
00:06:46,000 --> 00:06:52,000
 If our body is not in tip-top shape, it's very difficult

75
00:06:52,000 --> 00:06:57,000
 for us to do good for ourselves,

76
00:06:57,000 --> 00:07:01,000
 let alone others.

77
00:07:01,000 --> 00:07:09,930
 But it happens that people ignore the mental illness

78
00:07:09,930 --> 00:07:11,000
 entirely

79
00:07:11,000 --> 00:07:16,000
 and focus solely on physical well-being.

80
00:07:16,000 --> 00:07:21,980
 So there are people who are obsessed with diets and

81
00:07:21,980 --> 00:07:24,000
 physical regimen,

82
00:07:24,000 --> 00:07:28,980
 physical exercise, and whose minds are a mess, who are

83
00:07:28,980 --> 00:07:32,000
 arrogant and conceited and proud,

84
00:07:32,000 --> 00:07:41,000
 or who are filled with self-hatred at their image,

85
00:07:41,000 --> 00:07:46,810
 trying to constantly fix their image, work on their image

86
00:07:46,810 --> 00:07:49,000
 out of low self-esteem.

87
00:07:49,000 --> 00:07:52,000
 And the two kinds of people, people with high self-esteem

88
00:07:52,000 --> 00:07:54,000
 and people with low self-esteem,

89
00:07:54,000 --> 00:07:56,000
 both are very dangerous.

90
00:07:56,000 --> 00:08:19,000
 Destructive goes to self and others.

91
00:08:19,000 --> 00:08:23,080
 Until we become proud of our physical health, people can

92
00:08:23,080 --> 00:08:25,000
 think that life is good,

93
00:08:25,000 --> 00:08:29,250
 there's no need to practice meditation because nothing's

94
00:08:29,250 --> 00:08:30,000
 wrong.

95
00:08:30,000 --> 00:08:34,000
 I have everything I could ever want and my life is good,

96
00:08:34,000 --> 00:08:36,000
 this kind of thing.

97
00:08:36,000 --> 00:08:58,240
 And so they are just inclined to practice meditation very

98
00:08:58,240 --> 00:09:02,000
 short.

99
00:09:02,000 --> 00:09:06,000
 So we have to look at physical and mental illness.

100
00:09:06,000 --> 00:09:10,000
 When we look at these four types, four causes of illness,

101
00:09:10,000 --> 00:09:16,000
 we can get a clear sense of what is meant by illness.

102
00:09:16,000 --> 00:09:21,000
 So we have from food, illness can come from our diet,

103
00:09:21,000 --> 00:09:23,000
 improper diet.

104
00:09:23,000 --> 00:09:25,000
 So diet is important.

105
00:09:25,000 --> 00:09:30,000
 Buddha seems to have accepted that. He praised once,

106
00:09:30,000 --> 00:09:34,000
 praised a monk for actually separating out his arms food

107
00:09:34,000 --> 00:09:39,000
 into what was healthy and what was unhealthless.

108
00:09:39,000 --> 00:09:42,000
 It was sort of a back-ended compliment.

109
00:09:42,000 --> 00:09:47,770
 He actually used it as a chance to talk about this monk's

110
00:09:47,770 --> 00:09:49,000
 past lives

111
00:09:49,000 --> 00:09:53,240
 when he knew the difference between what was good and what

112
00:09:53,240 --> 00:09:55,000
 was bad for his mind.

113
00:09:55,000 --> 00:10:01,170
 So again we have the comparison, the contrast between

114
00:10:01,170 --> 00:10:04,000
 physical and mental illness.

115
00:10:04,000 --> 00:10:07,850
 The monks were complaining about this monk who was

116
00:10:07,850 --> 00:10:11,000
 concerned about his physical health.

117
00:10:11,000 --> 00:10:13,000
 They said, "Hey, what's he doing?"

118
00:10:13,000 --> 00:10:18,330
 The Buddha said, "Well, this monk is just like that. It's

119
00:10:18,330 --> 00:10:19,000
 good for him.

120
00:10:19,000 --> 00:10:24,170
 It's good for him that he knows what is to his benefit and

121
00:10:24,170 --> 00:10:25,000
 to his detriment

122
00:10:25,000 --> 00:10:30,260
 because in the past he also knew and he knew it was good

123
00:10:30,260 --> 00:10:32,000
 for his mind and his body."

124
00:10:32,000 --> 00:10:35,000
 So it's this story that the Buddha told.

125
00:10:36,000 --> 00:11:01,580
 [

126
00:11:01,580 --> 00:11:10,650
 A man who came to do a course with us wanted to live off

127
00:11:10,650 --> 00:11:12,580
 fruit for his whole course.

128
00:11:12,580 --> 00:11:17,900
 Probably not a good idea, but not really against the rules.

129
00:11:17,900 --> 00:11:20,580
 As long as you're eating.]

130
00:11:20,580 --> 00:11:24,580
 Tried to discourage him, but he wouldn't be discouraged.

131
00:11:24,580 --> 00:11:32,080
 And near the end of the course he came to me and he had spr

132
00:11:32,080 --> 00:11:34,580
ained something.

133
00:11:34,580 --> 00:11:38,260
 He had sprained his leg and he realized that he'd gone two

134
00:11:38,260 --> 00:11:41,580
 weeks or something with only eating fruit.

135
00:11:41,580 --> 00:11:45,630
 Because of the walking meditation that we do he wasn't able

136
00:11:45,630 --> 00:11:46,580
 to continue.

137
00:11:46,580 --> 00:11:52,440
 And he decided, "Well, maybe it's a good idea if I actually

138
00:11:52,440 --> 00:11:54,580
 need something."

139
00:11:54,580 --> 00:11:59,580
 So meditation, you have to take care of your body.

140
00:11:59,580 --> 00:12:05,580
 It won't let you continue if you're not in tip-top shape,

141
00:12:05,580 --> 00:12:07,580
 good shape.

142
00:12:07,580 --> 00:12:17,580
 [The Buddha is a good idea.]

143
00:12:17,580 --> 00:12:20,760
 Also if you eat too much of course it makes you tired and

144
00:12:20,760 --> 00:12:22,580
 makes you difficult to practice.

145
00:12:22,580 --> 00:12:27,730
 Of course this is only physical. It's conducive to smooth

146
00:12:27,730 --> 00:12:30,580
 practice, but not necessary.

147
00:12:30,580 --> 00:12:33,610
 People who are sick, people who are crippled, people with

148
00:12:33,610 --> 00:12:36,580
 any kind of disability can still practice.

149
00:12:36,580 --> 00:12:39,580
 It's not to say that they can.

150
00:12:39,580 --> 00:12:43,760
 But an ideal situation is to be able to walk and to sit, to

151
00:12:43,760 --> 00:12:51,580
 have a comfortable, good digestion and all this.

152
00:12:51,580 --> 00:12:56,580
 These are the types of sickness that come from food.

153
00:12:56,580 --> 00:13:06,580
 This is like heart disease and diabetes.

154
00:13:06,580 --> 00:13:09,770
 It makes meditation difficult, makes spiritual practice

155
00:13:09,770 --> 00:13:10,580
 difficult.

156
00:13:10,580 --> 00:13:32,580
 [The Buddha is a good idea.]

157
00:13:32,580 --> 00:13:48,580
 You may have sickness that comes from

158
00:13:48,580 --> 00:14:11,600
 the environment, viruses, radiation, food poisoning,

159
00:14:11,600 --> 00:14:14,580
 certain types of cancer,

160
00:14:14,580 --> 00:14:24,140
 sunburn, this kind of thing, poison ivy, all that is

161
00:14:24,140 --> 00:14:27,580
 environmental.

162
00:14:27,580 --> 00:14:30,950
 You also have to be careful. The Buddha is clear that you

163
00:14:30,950 --> 00:14:33,580
 don't just ignore your environment and let yourself suffer.

164
00:14:33,580 --> 00:14:40,070
 You have to take care of your body and part of this is

165
00:14:40,070 --> 00:14:43,580
 avoiding dangerous situations,

166
00:14:43,580 --> 00:14:54,580
 dangerous to the body, problematic to the body.

167
00:14:54,580 --> 00:15:05,580
 These two are physical, food and environment.

168
00:15:05,580 --> 00:15:09,630
 Now the other two, these are the ones that are more mental,

169
00:15:09,630 --> 00:15:13,580
 karma, sickness that comes from karma.

170
00:15:13,580 --> 00:15:16,580
 Karma, of course, is mental.

171
00:15:16,580 --> 00:15:21,760
 The good and the bad deeds, in this case, bad deeds that we

172
00:15:21,760 --> 00:15:22,580
've done,

173
00:15:22,580 --> 00:15:28,940
 will lead us to get sick and will cause sickness in the

174
00:15:28,940 --> 00:15:30,580
 body.

175
00:15:30,580 --> 00:15:40,580
 They lead us to be negligent.

176
00:15:40,580 --> 00:15:44,700
 They lead us to be reborn with sickness of people who are

177
00:15:44,700 --> 00:15:46,580
 born with disabilities.

178
00:15:46,580 --> 00:15:51,590
 This is often this idea of people deserving to be crippled

179
00:15:51,590 --> 00:15:53,580
 or deserving to be blind

180
00:15:53,580 --> 00:15:56,580
 or deserving of this or that, the idea of cancer.

181
00:15:56,580 --> 00:16:02,130
 It's often met with a great amount of disgust and

182
00:16:02,130 --> 00:16:03,580
 opposition,

183
00:16:03,580 --> 00:16:07,580
 thinking this is a horrible thing to say to someone.

184
00:16:07,580 --> 00:16:14,580
 Karma was never promoted as being a positive thing or

185
00:16:14,580 --> 00:16:15,580
 something that

186
00:16:15,580 --> 00:16:19,580
 people would leap at, thinking, "Oh, what a great system."

187
00:16:19,580 --> 00:16:21,580
 No, karma is scary and awful.

188
00:16:21,580 --> 00:16:26,510
 It's awful that people have to suffer for their good and

189
00:16:26,510 --> 00:16:27,580
 bad deeds,

190
00:16:27,580 --> 00:16:32,580
 which is not a good thing.

191
00:16:32,580 --> 00:16:38,030
 But it's whatever else you say about it, it is just and

192
00:16:38,030 --> 00:16:40,580
 provides meaning,

193
00:16:40,580 --> 00:16:50,580
 provides answers to why, people who wonder why.

194
00:16:50,580 --> 00:16:54,580
 Well, if there's a logical reason, you know?

195
00:16:54,580 --> 00:17:04,580
 We don't ask why a convict goes to prison.

196
00:17:04,580 --> 00:17:16,720
 We don't ask why people who hurt others wind up cultivating

197
00:17:16,720 --> 00:17:17,580
 enemies

198
00:17:17,580 --> 00:17:24,580
 and facing retribution.

199
00:17:24,580 --> 00:17:28,580
 We understand that there's cause and effect.

200
00:17:28,580 --> 00:17:31,580
 So karma is just part of that.

201
00:17:31,580 --> 00:17:35,860
 If we can accept that our sicknesses are often caused by

202
00:17:35,860 --> 00:17:36,580
 karma,

203
00:17:36,580 --> 00:17:41,790
 cancer, a lot of unexplainable cancer seems to be likely k

204
00:17:41,790 --> 00:17:44,580
armically based.

205
00:17:44,580 --> 00:17:48,580
 People who are born with disabilities,

206
00:17:48,580 --> 00:17:52,130
 I think it's safe to assume that a lot of this is karma, if

207
00:17:52,130 --> 00:17:56,580
 not all of it.

208
00:17:56,580 --> 00:18:02,410
 So it gives us a good background of why we are the way we

209
00:18:02,410 --> 00:18:02,580
 are

210
00:18:02,580 --> 00:18:19,580
 and why others are the way they are.

211
00:18:19,580 --> 00:18:23,580
 And the fourth one is sickness that comes from the mind.

212
00:18:23,580 --> 00:18:25,580
 So the mind can cause sickness.

213
00:18:25,580 --> 00:18:29,580
 This is the one that we're focusing most on in meditation,

214
00:18:29,580 --> 00:18:32,910
 although the karma one you could say is a big part of our

215
00:18:32,910 --> 00:18:33,580
 focus.

216
00:18:33,580 --> 00:18:37,580
 We're more concerned with the state of mind.

217
00:18:37,580 --> 00:18:44,150
 Mind simply having certain states of mind makes the body

218
00:18:44,150 --> 00:18:44,580
 sick.

219
00:18:44,580 --> 00:18:48,580
 When we're angry, our bodies become inefficient, greedy.

220
00:18:48,580 --> 00:18:54,580
 We neglect our bodies, deluded, our bodies stiffen up.

221
00:18:54,580 --> 00:18:58,580
 We have a hard time functioning.

222
00:18:58,580 --> 00:19:01,840
 The state of mind is a cause, or a correct cause for

223
00:19:01,840 --> 00:19:02,580
 illness.

224
00:19:02,580 --> 00:19:09,930
 You might say this is karmic, but karma is more focused on

225
00:19:09,930 --> 00:19:10,580
 acts

226
00:19:10,580 --> 00:19:15,580
 that we perform with body and speech, or even thought.

227
00:19:15,580 --> 00:19:20,800
 But the actual mental state, the anger and the greed and

228
00:19:20,800 --> 00:19:22,580
 the delusion in our mind

229
00:19:22,580 --> 00:19:26,580
 is cause, physical sickness.

230
00:19:26,580 --> 00:19:30,580
 And they are of themselves sickness.

231
00:19:30,580 --> 00:19:35,220
 These are what the Buddha was talking about in this verse

232
00:19:35,220 --> 00:19:35,580
 from the book

233
00:19:35,580 --> 00:19:39,580
 that we're going through, mental illness.

234
00:19:39,580 --> 00:19:42,580
 It's hard to find people who are free from this,

235
00:19:42,580 --> 00:19:48,580
 because when we come right down to it, mental illness is

236
00:19:48,580 --> 00:19:52,580
 any state of upset in the mind.

237
00:19:52,580 --> 00:19:58,580
 It's only a matter of degrees.

238
00:19:58,580 --> 00:20:02,570
 Some of it to such a severe degree, of course, that it is

239
00:20:02,570 --> 00:20:02,580
 organic

240
00:20:02,580 --> 00:20:07,200
 in the sense that the brain is damaged, and we aren't able

241
00:20:07,200 --> 00:20:08,580
 to fix it in this life.

242
00:20:08,580 --> 00:20:12,580
 But that's not categorically different.

243
00:20:12,580 --> 00:20:15,580
 It doesn't have to be considered categorically different

244
00:20:15,580 --> 00:20:19,580
 from milder forms of illness.

245
00:20:19,580 --> 00:20:23,530
 It's just so extreme that it's manifested itself in the

246
00:20:23,530 --> 00:20:26,580
 physical realm.

247
00:20:26,580 --> 00:20:32,580
 All illness can be cured over time.

248
00:20:32,580 --> 00:20:39,580
 It just often takes more than one lifetime.

249
00:20:39,580 --> 00:20:42,960
 Only those who are free from the violence, that's the

250
00:20:42,960 --> 00:20:43,580
 Buddha's end.

251
00:20:43,580 --> 00:20:46,580
 And those are the people who are able to free themselves,

252
00:20:46,580 --> 00:20:50,580
 able to be free from mental illness.

253
00:20:50,580 --> 00:20:54,580
 So that's what we aim for.

254
00:20:54,580 --> 00:20:58,580
 Just some food for thought.

255
00:20:58,580 --> 00:21:03,580
 And mental illness.

256
00:21:03,580 --> 00:21:05,580
 Mental and physical illness.

257
00:21:05,580 --> 00:21:07,580
 So thank you for tuning in.

258
00:21:07,580 --> 00:21:10,580
 Wishing you all good practice.

259
00:21:10,580 --> 00:21:13,580
 Stay mindful.

