 Okay, good evening everyone. Broadcasting live, October 11
th, 2015. We have another Dhammapada,
 so you might as well keep going with these. Although we
 could do Dhammapada like every
 other day and do something else, do the quotes. Our quote
 is also from the Dhammapada.
 Well, then we better stick to the Dhammapada, stick to the
 order then, right?
 We have a long quote today. Test, test, test, test, test.
 Hello and welcome back to our study of the Dhammapada.
 Today we continue on with first
 number 83, which reads as follows.
 Which means,
 good people are everywhere in every instance, on every
 occasion renouncing, giving up, giving
 up. They don't mutter or blather or chatter as though they
 were enamored by sensuality,
 in love with love or lusting after lustful things, desirous
 of the desirables.
 Sukeṇa puta atavadu keṇa, when touched by either
 happiness or suffering.
 No ucāvacyān bandhita dasayanti, they don't exhibit the
 wise, don't exhibit highs or lows.
 So ordinary people when touched by sukha or dukha,
 happiness or suffering, they are affected,
 they are elated, they rejoice about their good fortune,
 they become somehow dependent
 on it. They step into it and they come to expect it.
 When touched by suffering then they lament and bemoan their
 state.
 But asapurisa is ever chajanti, is ever renouncing, letting
 go.
 So that's the verse.
 It's in regards to a story about 500 monks and 500 beggars.
 So in the time of the Buddha, after the Buddha was recently
 enlightened, he was staying in
 Vairanjā. He lived there for the rains. He had been
 invited to stay in Vairanjā if you read
 the beginning of the Vinayat.
 teinakopanayana-samayana-buddho-bhakava-vairanjāyāng vih
arati
 The Buddha was dwelling in Vairanjā. This Brahman invited
 him to stay there and then
 forgot about him.
 So the Buddhist and his 500 monks dwelt in great hardship.
 They only were able to survive
 because of some horse traders who gave them horse food,
 gave them oats, something like
 oats, some kind of grain. And they were able to live off
 this grain.
 But it was great hardship. So they went on alms, but I
 guess didn't get much. And this
 Brahman who invited them to this area where they weren't
 able to get alms, totally forgot
 about them until the end of the rains.
 But it says here that there were a bunch of beggars living
 with them. By the kindness
 of the monks, 500 eaters of refuse lived within the
 monastery enclosure.
 Or it doesn't even say that, but what it does say is that I
 get the point is these beggars
 were before, so they weren't living. I was wrong. They
 weren't living with the Buddha
 at Vairanjha. But in Vairanjha, the monks, these 500 monks
 were all at least Dottapana,
 maybe all Arhants. And they were all living with the Buddha
 and they were content with
 their poor fare. They were at peace, even in great hardship
.
 Later on, they came to great wealth and prosperity, not
 wealth in terms of money, but they were
 very well cared for. They moved to Sawati and they lived
 there. But they were still
 the same. They were still at peace. So whether they lived
 in hardship or whether they lived
 in great comfort, they were the same. And then you had
 these 500 beggars who appeared
 after there was great wealth and because of their own
 hardship, decided that they would
 hang out at the monastery. And so through the kindness of
 the monks, these 500 beggars
 lived off the leftover food because everyone was always
 bringing food to the monks and
 coming to hear the monks teach and supporting the monks in
 different ways. And so there
 was lots of leftovers and so these beggars would come. And
 it says they would eat the
 choice food left over by the monks and then lie down and
 sleep. And then when they got
 up from their sleep, they would be full of energy and they
 would go and wrestle. They
 would shout and jump and wrestle and play, misbehave both
 within and without the monastery.
 They did nothing but misbehave. And so the monks were
 discussing this and they noted
 the difference because ostensibly a part of the problem for
 these beggars was the opulence.
 Once they got great food, they got strength and energy and
 they just started acting rambunctious.
 Whereas the monks, whether they were in hardship or in op
ulence or affluence, were unchanged.
 And so the Buddha, for that reason, he told a Jataka story
 about horses, which is actually
 quite interesting because it points out that even it's a
 story about some donkeys and horses
 and these thoroughbred horses were fed, it was an accident
 or something, they were fed
 wine of some sort or some kind of alcohol and were
 unaffected by it. But these donkeys
 who were hanging out with the thoroughbred horses drank the
 same wine and started braying
 and carrying on. The point being that even alcohol, even
 though we rail against it as
 being a terrible, terrible thing, all it has the power to
 do is remove your inhibitions.
 So if you don't have any harmful intentions in your mind,
 it doesn't actually hurt you.
 Unfortunately for most of us, that includes delusion, it
 includes ego and so on. So if
 you have none of that, then you're fine. But anyway, he
 makes the point that there's a
 difference between people. For an ordinary person, it's
 actually an interesting point
 because for ordinary people, it's sometimes the other way
 around. Sometimes when things
 are fine, people can be quite moral and ethical, right?
 When you don't need to steal, when
 you don't need to kill, when there's no adversity, when
 there's no danger. But when the going
 gets tough, many people will abandon their ethics, abandon
 their goodness, their generosity.
 For other people, it's the other way around. In hardship,
 they are forced to behave themselves.
 They have to act in such a way that other people respect
 them and so on. But when things
 are good, they get lazy. Or for monastics, this is very
 much the case. When things are
 rough, you're forced to be mindful because there's lots of
 physical pain and physical
 discomfort and hunger and thirst and heat and cold that you
 can't escape. But when you
 have the pleasure and the luxury, then it's very easy to
 get lazy. It's very easy to become
 indulgent and so on. It's actually quite an impressive
 thing, impressive feat that these
 monks having been in such hardship, going to such opulence
 and not being affected by
 it. But the point still stands that it works both ways and
 it seems more to be about change
 than anything. That after a while you become sort of stable
 in your situation. And if things
 don't change, you become, you may become complacent
 thinking that everything is okay, but then
 when things change, you see what you couldn't see because
 of the stability. Part of it points
 out the nature of impermanence or the importance of imper
manence, the harm, the danger of change
 to one's psyche. And also the usefulness of impermanence
 for that very reason. When things
 change you're able to better see your defilement when you
 have the rug pulled out from under
 you. When everything's going stable, whether it's difficult
, if it's difficult, you plot
 along. If it's good, then you feel calm and at peace. But
 when things change, you can
 be quite upset. A person who is living in opulence, if they
 fall into hardship, would have a very
 hard time coping mentally with it. So it goes both ways and
 this is continuing this theme
 in this verse. We've seen this recently quite a bit. We
 talk about how wise people touched
 by either happiness or unhappiness. Happiness or suffering
 show no change.
 Na uchah wach, na uchah wachah dasayanti. Uchah means high
 and awachah means low. So they
 experience no highs or lows. Which is interesting. People
 often think of that as being a terribly
 dull and uninteresting state. Certainly not something to be
 strived for. The highs and
 the lows are the spice of life, they would say. It gives
 life meaning, I guess. It gives
 meaning to your happiness. But it's actually quite the
 opposite. We find people who are
 stuck on happiness and suffering, that these are the ones
 who are like zombies. They can
 be very depressed and colorless in their constant aversion
 to suffering or in their constant
 obsession with happiness. A zombie is someone who is
 constantly seeking out pleasure. It's
 by seeking out pleasure that you become this zombie that
 consumes and consumes. Or you're
 a zombie working just to survive in great hardship. Just to
 cope with suffering. A person who
 has had great loss becomes a zombie. A person who exhibits
 neither highs or lows is actually
 quite at peace. Has quite a calm and uplifted mind, a light
 mind. A mind that is flexible.
 A mind that is kind and compassionate. Because they aren't
 caught up in their own emotions.
 So they're able to approach life with great zest and vigor
 and clarity and goodness as
 well. So their intentions to help others and that kind of
 thing are pronounced. Their happiness
 and their peace are much pronounced. So they're anything
 but a zombie. A zombie is the one
 who is caught up in likes and dislikes. They become tired
 and they get into this trance-like
 state, zombie-like state of consuming pleasure and coping
 with suffering. It's a warning
 for all of us not to become complacent. It's easy to think
 that you're moral and ethical.
 If you don't look closely, it's easy to think, "Well, I'm
 not a bad person. I'm living fine."
 But so much of that is often dependent on our pleasure. And
 when you start to meditate,
 you start to see, "Ah, yes, actually I'm quite intoxicated
 by this pleasure. And many of
 the things that I thought were harmless are actually
 building me up to take a fall. Because
 when I'm not able to indulge in sensual pleasures anymore,
 I'm going to get angry and upset and
 frustrated." It's what leads us to fight and quarrel and
 manipulate others, our indulgence
 in sensuality. It's really the crux of it. Our reactions to
 pleasant experiences and
 our reactions to unpleasant experiences. This about sums up
 all of the problems that we
 have, the problems of the mind that we're working to
 overcome. There's a third one that's not
 really explicitly mentioned here, and that's delusion. We
 consider these to be the three
 evils in the mind, the three things that lead us to suffer.
 But they're not equal. Happiness
 and unhappiness lead to greed and anger, liking and disl
iking, our reactions to these things.
 These are, you could say, the surface problem, but the
 underlying problem. The third one
 is the underlying problem. Our delusion is how we approach
 these things. The reason we
 get angry about unpleasant things, the reason we become
 attached to pleasant things, is
 because of our delusion, because of our ignorance, our
 perception that this one's going to make
 me happy. This one's something that I... The way to be
 happy is to avoid it. If I chase
 after this one, I'll be happy. If I push this one away, I
'll also be happy. Not realizing
 that the more we push this one away, the more unhappy we'll
 become. The more we cling to
 this one, the more needy we will become, and so the more
 unhappy we will become. And shameful
 we'll become. If you ask yourself, "Who would you rather
 have as a friend? Who would you
 rather associate with? Someone who wants a lot, or someone
 who doesn't have... Who has
 great wanting, or someone who has little wanting? Who has
 great desires, strong, strong desires,
 or someone who has simple desires, or is free from desire?"
 I don't know. Some people might
 say they like people who have great desires, but if you
 think about it, when people have
 great desires, then they end up wanting a lot from you.
 Friends who have great desires
 can be great, great burdens. Friends who are hard to please
, or friends who have great...
 People who have great aversion to suffering. So this is the
 sort of situation we have here
 where part of the implication here is that these guys are
 acting improper, or acting
 in an unsuitable way, and having eaten this food that was
 meant for the monks, and sort
 of living in close quarters with these spiritual people,
 they're acting like ordinary individuals.
 You get that a lot in monasteries, when they get big in
 Thailand, you'd see this a lot,
 where the workers in the monastery is not so very moral. I
 was talking to one of the
 workers about cockroaches, and I think she pulled out a can
 of cockroach spray and was
 trying to hand it to me. I remember going, we went on this
 parade once, the monks, we
 were up in this boat, they had us up on this float with a
 boat, it was like a boat, and
 people were putting food in the boat. It was a means of
 supporting, I don't know actually
 what the food was going to be for, but it was a parade with
 monks, which was kind of
 nice. But then the board of directors for the monastery was
 surrounding it, and going
 on with us, and then they pulled out alcohol and started
 drinking whiskey, and all around
 us they were holding onto the edges of the boat and riding
 the float. It's a shame really,
 that sort of thing, that living so close to such goodness
 and such spiritual teachings,
 that they don't bother to put them into practice. And part
 of it is this problem of having things
 too good, when things are good, people may tend to misbe
have. It's part of the reason
 why monks are encouraged to go off into the forest and live
 in simplest, simple accommodations,
 to live some more or less in hardship, because it does, it
 can keep you honest, it forces
 you to be mindful. In terms of the monastic life hardship
 is quite often important for
 spiritual development, because it's very easy to get lazy,
 as we can see here. Anyway, so
 a simple teaching, the verse itself has some interesting
 points, the idea that what makes
 a good person is their practice of renunciation everywhere,
 whether they're happy or unhappy,
 and they never blather on. Lapa yanti, it's an interesting
 word, they chatter. The implication
 is that people who are intoxicated with sensuality will bl
ather on about it, something we should
 catch ourselves in talking about food or clothing or music
 or art or this kind of thing. But
 one who is wise when touched by other happiness or sorrow
 is at peace, is content. Their happiness,
 their peace, their contentment is not dependent. Anamisa su
ka is a happiness that is not based on
 an object, doesn't require this or that, it isn't affected,
 it isn't disturbed, it isn't
 disturbed and it isn't dependent on external sense,
 external experiences or phenomena. This is why
 meditation is so important. It's important to understand
 change and to become familiar with
 change and to see that change is a part of life so that
 changes don't affect us, so that our
 happiness can be our peace of mind, can be undisturbed by
 change. Anyway, that's the teaching
 of this Dhammapada verse. That's all for now. Thank you for
 tuning in. Keep practicing and be well.
 Do you have any questions? I'm sure we do, Bante. Sorry, I
 got caught up in the story and didn't
 check the questions. Hello, Bante. There aren't any
 teachers or retreats close to where I live.
 I'm doing three hours a day of meditation. I know you said
 that meditation can be risky if done
 intensively and without a teacher, but I'm thinking I can
 do more as time goes by and my
 mind becomes stronger. I'm thinking about building my
 practice up to eight hours a day over the years.
 Is this a good approach? If your practice is proper, the
 reason for having a teacher is to
 keep you on the right path. I mean, sure, you can say you
're practicing eight hours, but what
 exactly are you doing for eight hours? I mean,
 theoretically, if you're using my booklet, I would
 think you're practicing as I deem to be correct, but there
 are nuances and there are subtleties to
 it that are easy to miss. Very, very easy to miss without a
 teacher. So definitely with eight hours,
 doing eight hours a day, you'd want some contact with the
 teacher, I would think, unless you're a
 special individual, which most people do think they're a
 bit special. So be careful of that as
 well. And just in case you weren't aware, if you click on
 the the meet button, Bhante actually has
 one on one reporting time with meditators from this group,
 and there are several slots left. So
 what do you think of the people of the hardcore Dharma
 movement like Daniel Ingram, who says that
 an Arahant can still have lust and aversion to certain
 states? I've heard that he's since
 recanted his claim to be an Arahant. So I mean, I don't
 think about them that much. So again,
 it's one of these situations where it's not something I
 think about and I don't really want
 to comment on other groups. Sorry. I feel that taste is
 very close to feeling. So when I'm tasting
 something, I say feeling, feeling. Is that right practice?
 No, there are different sense. I mean,
 for most of us, there are different sense. Taste is a taste
, feeling is a feeling. Another question
 on the heart in the left hand column of the meditator list,
 and that is when people are showing
 that they're sending metha after their meditation, you just
 click on your check and it'll turn into
 a heart. Or if you're on the Android app, you just long
 click the start button and it'll turn your
 it'll turn it into a heart by your name. Is the mind
 strength developed by Samatha different
 compared to one developed by Vipassana? Yeah, I mean, Vip
assana ultimately leads to Nibana. So
 it is, that's a stable strength of mind, permanent, you
 could say. Whereas with Samatha, it's impermanent.
 No, turn Robin up. I'm wondering, you know, there should be
 a way to pipe you into this mic as well.
 I have to figure that out. I found out what happened
 yesterday when I, my mic sounded really
 terrible. It was actually unplugged. I didn't realize
 someone in my family had unplugged it.
 So it was, I was just going through the webcam mic. So
 sorry about that. I know it kind of messed
 up the audio yesterday. Is the point to fully accept all
 experiences without judgment? The
 point is to become free from suffering, but eventually you
 get to a state where you fully
 accept all experiences without judgment. And, you know, it
's poor wording, and I'm not comfortable
 with the word accept because, or without judgment, because
 they're vague words in English. So if you
 accept something, that means you can, it can mean, can be
 taken to mean that you consider it to be
 proper. You consider it to be acceptable, right? And if you
 have no judgment, that means also that
 you don't see that which is harmful as being harmful. You
 don't judge that what is harmful
 as being harmful. You see, it can mean that. So you might
 say is the point, the point is to fully
 experience, not fully accept, to experience experiences as
 they are without wrong judgment.
 So when you experience something as impermanent, then that
's that thing is actually something to
 be discarded, not to be accepted. So once you're fully equ
animous towards things, you see them as
 they are. You don't get upset or elated by them, but you
 end up rejecting them as useless. You
 don't accept them and say, well, this is how I want to live
 my whole, this is what I want to happen in
 the future, or this is fine with me in the future. You
 actually give them up and discard them. So
 there's that sense of discarding the those things that are
 impermanent, unsatisfying and uncontrollable.
 So I don't like to say it's accept things or to not judge
 them. Because to some extent, there is
 a judgment that goes on. The judgment is these are useless.
 These are suffering. These are are
 without benefit to me and throwing them out, abandoning
 them.
 During meditation, I am having recollections of moments
 from my life that I have not thought
 about in many years, forgotten moments and places from
 childhood. Is this progress or just distraction?
 It's a sign of progress that in and of itself isn't
 progress. But it's a sign that you are
 you have stronger concentration than before, you have a
 clearer mind than before. And so
 these things are coming up. But it's still, it still can be
 a distraction. So you have to note
 it as thinking thinking or if you have feelings about it,
 liking or disliking or worry or fear,
 or so on, you have to acknowledge it.
 You've spoken about the relative wholesomeness of various
 occupations.
 Is working in aged care to be considered wholesome?
 Yep, I would say mostly. I mean, look, if you're working
 with old with old people, and you're mean
 to them, that's not wholesome. If you're working with old
 people, and you're caring for them, and
 you're kind to them, and you're humble, and you're, you're
 servile, I guess, is a word maybe people
 don't want to hear, but to some extent, you know, you're
 working for them to, to like, being a tool
 for them without any ego or attachment or anger or, or, you
 know, oppression, oppression, because
 sometimes in those situations, old people are treated as,
 you know, prisoners, and you're the
 jailer, so you're in charge kind of thing. So, you know,
 can be actually quite unwholesome. Part of it
 is, is the issue of medication, my grandmother lived in an
 old age home, and they just medicated
 her to almost a death. When we finally found out, I wasn't,
 you know, I wasn't involved with the
 decision making, but she was put in this old age home,
 which in and of itself was probably a mistake,
 but then they just, they just neglected her. And by the
 time it was, we found out she was,
 she was addicted to these drugs, and she couldn't go off
 them without being in terrible, terrible pain.
 So I'm not convinced that old age homes are a good thing,
 and be nicer if
 old people were taken care of by their families, but in
 cases where it's necessary for them to
 find someone to take care of them, if you're, if you have
 good intentions, you know, it can be a
 great thing. It's a great opportunity to do a good deed,
 even if you're working in the restaurant
 industry, if you're serving people food, and you're humble
 and, and servile, I think, I don't know
 better word, but something like that, in the sense of
 consider yourself working for them, and having
 no ego or anything, then it can be quite wholesome as well,
 because you're bringing in, you're helping
 people, you're doing a service for people. These kind of
 things can be wholesome. If you sell things
 that are useful for people, and you sell them at good
 prices, you know, good prices for people,
 not cheating anyone, that can be wholesome. But definitely
 looking after people, old people,
 young people, any type of person, if you're helping them
 out, seems like a quite a wholesome job to me.
 You can also have, require great patience. That's a good
 aspect of it.
 While doing Menta towards my enemies, I experienced sort of
 delight in,
 in that they received the consequences of their karma. How
 can I avoid that?
 Well, you don't have to avoid it, you have to acknowledge
 it. That's where we pass in as more
 useful. If you're practicing Menta, you would go back to
 someone you loved, and slowly try to send
 Menta to them. You, you, yeah, you could avoid it. You can
 avoid it by going back to a neutral person,
 or a person you love, and focusing on them first, and then
 slowly going back to the person who you
 don't like. But in Vipassana, it's more important that you
 practice Vipassana to try and
 let go of those feelings by saying, liking, liking, or, you
 know,
 underlying anger or hatred you have towards them.
 Is there an online library of Buddhist texts?
 Oh, probably. We have one that I kind of copied from
 someone else. It's on static.surymongalo.org/pdf.
 There's a page with a whole bunch of PDFs, but most of them
 are not that interesting to me.
 I just copied them over there. There's, some of them are
 probably interesting.
 [
 Silence ]
 Monté, can you give an example of the element of cohesion?
 Um, yeah, you don't experience cohesion, apparently, but
 cohesion is what keeps things
 together. It's the aspect of physical reality that keeps
 things together.
 So like glue, or static, actually, static attraction.
 If I meditate after the nightly YouTube talks, will my
 question mark symbol be green the next night?
 No, it's just taking you from the list here, which I think
 is two or three hours.
 The list below, if you're on this list, then you're green.
 If you're not on this list,
 then you're orange. It doesn't really matter if you're
 orange, we still answer your questions, but
 if we click on your username and don't see that you've done
 any meditation
 in your meditation log, then we might say, "Ah, this is not
 a meditator."
 [ Silence ]
 Now, don't worry if you're yellow sometimes, orange
 sometimes. It's the psychology of the internet,
 right? You can really play with people's minds. You have to
 acknowledge that feeling.
 If you feel guilty or bad or upset because you're not green
,
 then you should acknowledge that. Good practice.
 Somewhere in here, and I had spotted it earlier, was an
 interesting question. If you go somewhere
 and practice meditation locally, but you can't necessarily
 log in, should you log that in
 afterwards to show that you meditated? You could
 potentially spend like you have on Saturdays,
 you now have a day of meditation. Someone could be med
itating for maybe six hours,
 but maybe it's not necessarily convenient to log in during
 a live event like that.
 I didn't want to make too much out of the logs because, you
 know, for reasons like that,
 because I mean, I don't log all my meditation, but I wouldn
't worry about it. You know, there's
 no reason to do that. We're not giving out awards for the
 amount of meditation you've done.
 For that reason, we haven't paid too much attention to it.
 On the other hand, we would
 like to eventually get to the point where we have apps for
 people's phones, both Android and iOS,
 so that you could log in. That would be nice because then
 we'd be able to keep track of
 your meditation. We don't have a sense, "Oh, here's another
 person meditating."
 It's a support for our community because when we see that
 you're on the list, then it's another
 person to show that we're a large group and that we're not
 meditating alone, that kind of thing.
 Yeah, I use the Android app frequently and I've done it a
 couple of times at live events, but
 you do have to be careful to go into the settings and turn
 the tone off because
 you definitely don't want to disturb other people with the
 bird sound or something like that.
 Should we be careful to think that we experience cessation?
 I don't understand. Not to think?
 Careful about such thoughts?
 Careful about assuming that we experience cessation, maybe?
 Yeah, I mean, that makes sense. It's easy to overestimate
 your experiences.
 My teacher said, "Look and see how much greed, anger, and
 delusion you have. If you have less,
 then, well, you're doing well. If you have more, well, but
 it should be less over time."
 As an amateur musician, it's not clear for me how creating
 music
 can be thought of as bringing suffering. Could you please
 explain?
 It's something you like, you know, it's an addiction. It
 stimulates the brain.
 And if you look at the science of it, stimulating the brain
 is a path of diminishing returns.
 So you end up needing more as time goes on to get the same
 pleasure. And so that's why we try to
 listen to different types of music because we try to
 stimulate the brain in a new way
 so that we can get the same pleasure. But it's building up
 these pathways. It's strengthening
 the pathways of desire and the pathways of, well, sense
 gratification. And it's the same reason why
 drugs cause us suffering in the end because you can never
 get enough. You will always wind up in
 a place where you feel bored or you feel the need. As human
 beings, we have the luxury of getting what
 we want, but put a person who likes making music in a
 position where they're unable to make music
 and they'll be unhappy. You know, we don't get that so much
 because we are, for the most part,
 able to do what we want. But tell a musician that they have
 to work a nine to five job,
 give up their music, you know, they'll be unhappy. It's a
 very minor thing. I mean, music isn't going
 to send you to hell or anything, but it does make you
 stressed out when you can't get it,
 like anything else. And so you won't see it, but there'll
 be subtle, more subtle manifestations of
 that where you'll feel bored and you couldn't, if you were
 really in tune with your mind or someone
 who was really in tune with their mind, could see that that
 was a cause of addiction to music.
 But it's again, it's a very small thing. It's not like
 music is really, really bad.
 But like anything, it's certainly not the way to find
 freedom from suffering.
 Could you explain how to send Metta after meditating? Do
 you recommend doing this after
 every session? Yeah, I did a video for kids on that. They
 used to look at that video.
 First, I would send it to yourself. I'd be happy. And then
 the people in the room,
 this is how I do it. It's one of the ways. It's more
 comfortable for me. I used to do it based
 on people I love and then people neutral and then people
 who I don't know and so on. But that's too,
 I don't know, too messy for me. I always think about the
 people in the room next to me
 and then in the building and then in going gradually to the
 town, the city, the neighborhood,
 the city, country and the world. That seems to work a lot
 better. Simple.
 It's probably better as you're becoming more
 equanimous too, toward not necessarily putting people in
 the categories of those I love and those
 I dislike and all. Yeah.
 What would be the point of recording video while one med
itates?
 I don't know. What would be the point?
 It was, if the question is regarding the other day when we
 were meditating online together,
 we were meditating online together. Well, that wasn't
 recorded. I mean,
 yeah, it was just the point wasn't to record. The point was
 to broadcast.
 I think you had a busy day and you were still during your
 meditation session when the broadcast
 started. So you finished it out. Yeah. I mean, the point
 was to broadcast meditation. I mean,
 that's nice actually to do live meditation sittings where
 you can see each other meditating.
 I did that in Sri Lanka for a while. When Google Hangouts
 came out new,
 they would do a non on air hangout and invite people in my
 circles to meditate.
 And so we'd see each other meditating up to 10 people.
 So back to the music, we're getting ready to move to an
 office with an open floor plan. And they're
 giving us all this information on how to get along with
 people when there's 100 people in one room,
 basically. And one of the things is because it's
 distracting and so forth, they kind of recommend
 that you wear headphones or earbuds and listen to some
 music to keep you calm. Probably not going
 to do that. Yeah, I mean, if you want to take an extreme
 case as far as listening to music goes,
 a lot of the young kids who are listening to their music
 all the time tend to be quite
 unpleasant towards their parents. It's not totally because
 of the music, but it's definitely a part
 of it. I've seen it where they always got these earphones
 and when you tell them to take it out,
 then they sit there and they bounce their knee and they're
 uncomfortable. It's an addiction.
 It's part of the addiction. It's not huge, but it can
 become quite a big part of one's
 addiction framework. We have all these different means as
 humans that we can all the different
 things that can make us happy. Food, sex, music, drugs,
 alcohol, television, games.
 Then we go from one to the other and we're very good at it.
 We're able to live a life of such
 pleasure, ignoring the fact that there are other people
 screaming in pain, dying of horrible deaths
 and being tortured and raped and burned alive and this and
 that. That all could happen to us at any
 moment. It could happen to us. We could get run over by a
 car. There was this woman who was run
 over by a car and then dragged screaming under the car for
 blocks. It was in Toronto many years ago
 and the old woman who ran her over had never found out
 about it. They never told the woman
 who was driving the car. Hopefully they revoked her license
, I don't know.
 It's terrible. Yeah, it can happen to you. Listening to
 music isn't going to help you then.
 In fact, the point is that it makes you more susceptible to
 pain, more susceptible to discomfort.
 Okay, enough. That's all for tonight. Thank you all for
 tuning in.
 Thanks, Robin, for taking part. Tomorrow we have no Robin.
 We need volunteers for tomorrow.
 Thanks to Fano, who was saying he could join us or if there
's anybody else who would like to join
 the hangout and give me a ring, let me know. Monday and
 Tuesday it will be just me.
 Thank you, Bante. Thank you. Good night, everyone.
 Bye.
 Bye.
 Bye.
 Bye.
 Bye.
 Bye.
 Bye.
 Bye.
 Bye.
 Bye.
