1
00:00:00,000 --> 00:00:10,000
 [birds chirping]

2
00:00:10,000 --> 00:00:20,020
 [birds chirping]

3
00:00:20,020 --> 00:00:35,020
 [birds chirping]

4
00:00:35,020 --> 00:00:45,020
 Well, good evening everyone. Welcome back to evening Dhamma

5
00:00:45,020 --> 00:00:45,020
.

6
00:00:45,020 --> 00:00:52,020
 Tonight we are ending the Sābhasa Vasūta.

7
00:00:52,020 --> 00:01:01,020
 And we've recently been over the topic of this section,

8
00:01:01,020 --> 00:01:05,620
 so I'm not going to go into great detail, but it's the kind

9
00:01:05,620 --> 00:01:07,020
 of thing that it's okay to go over.

10
00:01:07,020 --> 00:01:12,340
 For tonight we're looking at the taints or defilements to

11
00:01:12,340 --> 00:01:19,020
 be abandoned by developing or cultivating Bhāvanā.

12
00:01:19,020 --> 00:01:24,020
 Bhāvanā is a very important word in Buddhism.

13
00:01:24,020 --> 00:01:33,020
 Bhāva comes from "bu", "bu" means existence or to be being

14
00:01:33,020 --> 00:01:33,020
.

15
00:01:33,020 --> 00:01:37,580
 It's a simple word. If you talk about something existing,

16
00:01:37,580 --> 00:01:40,020
 you say it exists, Bhāvati.

17
00:01:40,020 --> 00:01:49,820
 Bhāva is something coming into being, it's existing or

18
00:01:49,820 --> 00:01:52,020
 being.

19
00:01:52,020 --> 00:02:04,020
 But Bhāvanā means causing something to be.

20
00:02:04,020 --> 00:02:07,440
 It's an important word because it describes Buddhist

21
00:02:07,440 --> 00:02:09,020
 practice quite well.

22
00:02:09,020 --> 00:02:21,020
 Buddhist practice isn't about relaxing or running away.

23
00:02:21,020 --> 00:02:30,020
 Buddhism is very much about developing and cultivating.

24
00:02:30,020 --> 00:02:38,940
 So meditation, this is why I have to repeatedly get the

25
00:02:38,940 --> 00:02:40,020
 door.

26
00:02:40,020 --> 00:02:58,020
 Why is someone barging into our house?

27
00:02:58,020 --> 00:03:01,020
 This is new.

28
00:03:01,020 --> 00:03:06,790
 So the idea is to become something, to cause things to come

29
00:03:06,790 --> 00:03:11,020
 into being and specific things.

30
00:03:11,020 --> 00:03:21,020
 When we practice meditation we are not just...

31
00:03:21,020 --> 00:03:23,020
 What's the problem?

32
00:03:23,020 --> 00:03:29,150
 I think he was trying to figure out what kind of place this

33
00:03:29,150 --> 00:03:30,020
 was.

34
00:03:30,020 --> 00:03:33,900
 I guess now that I've got the speakers on people can hear

35
00:03:33,900 --> 00:03:35,020
 us outside.

36
00:03:35,020 --> 00:03:43,020
 That's interesting.

37
00:03:43,020 --> 00:03:46,020
 We're trying to cultivate certain qualities.

38
00:03:46,020 --> 00:03:49,830
 So meditation isn't just about closing your eyes and blank

39
00:03:49,830 --> 00:03:51,020
ing your mind.

40
00:03:51,020 --> 00:03:54,020
 There's quite a bit more to meditation than people think.

41
00:03:54,020 --> 00:03:59,770
 It's not even just a question about it being about relaxing

42
00:03:59,770 --> 00:04:00,020
.

43
00:04:00,020 --> 00:04:06,020
 Meditation isn't exactly about one single thing.

44
00:04:06,020 --> 00:04:11,690
 There are ways of summing it up, but there's quite a bit

45
00:04:11,690 --> 00:04:14,020
 involved with meditation.

46
00:04:14,020 --> 00:04:22,130
 Many qualities of mind, many facets to the quality of mind

47
00:04:22,130 --> 00:04:35,020
 that we're trying to cultivate.

48
00:04:35,020 --> 00:04:43,870
 Meditation and Buddhist practice in general involves

49
00:04:43,870 --> 00:04:50,020
 producing, causing to come into being,

50
00:04:50,020 --> 00:04:52,020
 many qualities of mind.

51
00:04:52,020 --> 00:04:59,990
 We're talking about qualities of mind that were weak or non

52
00:04:59,990 --> 00:05:03,020
-existent before.

53
00:05:03,020 --> 00:05:06,020
 We're not talking about...

54
00:05:06,020 --> 00:05:09,020
 It's not physical.

55
00:05:09,020 --> 00:05:12,020
 We're not talking about cultivating anything physical.

56
00:05:12,020 --> 00:05:16,100
 Buddhism isn't about building monasteries or shaving off

57
00:05:16,100 --> 00:05:18,020
 your head, your hair,

58
00:05:18,020 --> 00:05:22,020
 putting on special clothes.

59
00:05:22,020 --> 00:05:28,310
 It's not about getting to the point where you can sit in

60
00:05:28,310 --> 00:05:31,020
 full lotus posture.

61
00:05:31,020 --> 00:05:37,020
 What we're cultivating here is qualities of mind.

62
00:05:37,020 --> 00:05:42,020
 That's something I think takes a little bit of patience,

63
00:05:42,020 --> 00:05:48,020
 but should be a focus when we have the question of,

64
00:05:48,020 --> 00:05:51,020
 "What are we getting out of this practice?"

65
00:05:51,020 --> 00:05:57,020
 Our focus is too often on, "How happy is it making me?"

66
00:05:57,020 --> 00:06:09,520
 Which is fine and good, but it smacks of potential impat

67
00:06:09,520 --> 00:06:15,020
ience and addiction to pleasure.

68
00:06:15,020 --> 00:06:21,020
 If I say to you meditation is supposed to make you happy,

69
00:06:21,020 --> 00:06:26,500
 we tend to ignore all the work that has to be done and

70
00:06:26,500 --> 00:06:31,020
 focus very much on that happiness.

71
00:06:31,020 --> 00:06:38,950
 When in fact it's our obsession with happiness that makes

72
00:06:38,950 --> 00:06:41,020
 us unhappy.

73
00:06:41,020 --> 00:06:44,330
 If you want to talk about success and progress in the

74
00:06:44,330 --> 00:06:45,020
 practice,

75
00:06:45,020 --> 00:06:48,020
 you have to ask about your state of mind.

76
00:06:48,020 --> 00:06:52,020
 Happiness isn't something that's easy to come by.

77
00:06:52,020 --> 00:06:58,200
 Happiness requires a strong and healthy and pure mind, a

78
00:06:58,200 --> 00:07:02,020
 well-developed mind.

79
00:07:02,020 --> 00:07:08,020
 The Buddha said, "Abhavitang jitang rago samativijit."

80
00:07:08,020 --> 00:07:18,020
 Just like rain penetrates a poorly thatched roof,

81
00:07:18,020 --> 00:07:27,100
 so too the undeveloped mind, abhavitang jitang, allows the

82
00:07:27,100 --> 00:07:29,020
 defilements in.

83
00:07:29,020 --> 00:07:33,110
 Defilements meaning those things that get us caught up in

84
00:07:33,110 --> 00:07:37,020
 addiction and aversion and suffering.

85
00:07:37,020 --> 00:07:40,830
 We suffer because our minds are undeveloped, not because we

86
00:07:40,830 --> 00:07:42,020
're working too hard,

87
00:07:42,020 --> 00:07:45,490
 but because we're not really working hard enough or we're

88
00:07:45,490 --> 00:07:51,020
 not working in the right way.

89
00:07:51,020 --> 00:07:53,320
 If you want to be happy, there are many qualities that you

90
00:07:53,320 --> 00:07:54,020
 have to develop.

91
00:07:54,020 --> 00:08:02,020
 Most importantly, mindfulness, and this is the first one.

92
00:08:02,020 --> 00:08:06,510
 Mindfulness always is the first. It's very important that

93
00:08:06,510 --> 00:08:09,020
 it be placed at the beginning.

94
00:08:09,020 --> 00:08:14,820
 It's the one the Buddha said is always useful when you

95
00:08:14,820 --> 00:08:17,020
 refer to the bhojangas.

96
00:08:17,020 --> 00:08:21,320
 What we're supposed to be developing are the seven factors

97
00:08:21,320 --> 00:08:23,020
 of enlightenment.

98
00:08:23,020 --> 00:08:29,650
 Bhojanga, bodhianga, 'anga' is just a word that means

99
00:08:29,650 --> 00:08:35,020
 member. Bodhi means enlightenment.

100
00:08:35,020 --> 00:08:41,020
 Bodhi is that moment when everything comes together,

101
00:08:41,020 --> 00:08:48,020
 when one is perfect in all the many factors and qualities,

102
00:08:48,020 --> 00:08:54,270
 faculties and qualities of mind that come together to

103
00:08:54,270 --> 00:09:00,020
 create enlightenment. That's bodhi.

104
00:09:00,020 --> 00:09:04,160
 So these seven factors are the seven things that we

105
00:09:04,160 --> 00:09:05,020
 cultivate.

106
00:09:05,020 --> 00:09:10,110
 And having cultivated, I realize bodhi, I realize

107
00:09:10,110 --> 00:09:12,020
 enlightenment.

108
00:09:12,020 --> 00:09:17,020
 Sati is the first. We talk a lot about sati and

109
00:09:17,020 --> 00:09:21,020
 of course this is how we come to see. This is, as we talked

110
00:09:21,020 --> 00:09:24,020
 about last time, how we abandon

111
00:09:24,020 --> 00:09:30,460
 the defilements temporarily and with the pure mind that

112
00:09:30,460 --> 00:09:34,020
 comes from being mindful,

113
00:09:34,020 --> 00:09:40,020
 we're able to abandon and free ourselves.

114
00:09:40,020 --> 00:09:55,020
 See clearly and through seeing clearly free ourselves.

115
00:09:55,020 --> 00:09:58,020
 So once we've developed sati then there comes dhammavitsaya

116
00:09:58,020 --> 00:09:58,020
,

117
00:09:58,020 --> 00:10:04,050
 which means in regards to the dhammas, the understanding

118
00:10:04,050 --> 00:10:05,020
 that comes about,

119
00:10:05,020 --> 00:10:09,550
 the shedding the light. Mindfulness is like a very powerful

120
00:10:09,550 --> 00:10:14,020
 light and dhammavitsaya is where you analyze.

121
00:10:14,020 --> 00:10:19,180
 Not intellectually but you start to react really, or not

122
00:10:19,180 --> 00:10:26,020
 react but taste, taste your reactions.

123
00:10:26,020 --> 00:10:30,020
 Dhammavitsaya is where you start to discriminate,

124
00:10:30,020 --> 00:10:34,170
 where you really start to be able to judge properly. We

125
00:10:34,170 --> 00:10:37,020
 talk a lot about how judgement is a real problem,

126
00:10:37,020 --> 00:10:41,770
 but it's only wrong judgement that's a problem or ignorant

127
00:10:41,770 --> 00:10:43,020
 judgement.

128
00:10:43,020 --> 00:10:47,030
 Once you start to be able to see things clearly, you start

129
00:10:47,030 --> 00:10:50,020
 to be able to judge things properly

130
00:10:50,020 --> 00:10:57,020
 and undeniably discriminate between good and bad.

131
00:10:57,020 --> 00:11:02,970
 This is undeniably bad. Why? Well, I've seen it with

132
00:11:02,970 --> 00:11:05,020
 perfect clarity.

133
00:11:05,020 --> 00:11:07,610
 That's what we do in the practice. It's like picking out

134
00:11:07,610 --> 00:11:09,020
 the bad stuff and throwing it away

135
00:11:09,020 --> 00:11:12,020
 and sorting everything into good and bad.

136
00:11:12,020 --> 00:11:16,240
 Not because someone told you so or because you think so or

137
00:11:16,240 --> 00:11:18,020
 because you feel so,

138
00:11:18,020 --> 00:11:26,020
 because you know so and you see.

139
00:11:26,020 --> 00:11:36,020
 Once you do that, to develop, you need to develop effort.

140
00:11:36,020 --> 00:11:41,090
 So you can have mindfulness and through mindfulness comes

141
00:11:41,090 --> 00:11:43,020
 this discrimination.

142
00:11:43,020 --> 00:11:46,770
 But for it to succeed you need effort. The third one is

143
00:11:46,770 --> 00:11:48,020
 effort.

144
00:11:48,020 --> 00:11:51,020
 This means the first two are really the practice.

145
00:11:51,020 --> 00:11:56,580
 For it to be successful you need to put effort in. You can

146
00:11:56,580 --> 00:11:59,020
't just do it once and give up.

147
00:11:59,020 --> 00:12:04,290
 Sit down for 10 minutes and that was that. Now I've med

148
00:12:04,290 --> 00:12:07,020
itated, I'm a meditator.

149
00:12:07,020 --> 00:12:09,860
 Something that you have to do with the Buddha said, "Satyac

150
00:12:09,860 --> 00:12:11,020
charya wasena"

151
00:12:11,020 --> 00:12:15,160
 is to get into the habit or the activity of this continuous

152
00:12:15,160 --> 00:12:20,020
 satyaccharya,

153
00:12:20,020 --> 00:12:25,020
 this continuous mindfulness and investigation.

154
00:12:25,020 --> 00:12:28,230
 It means when you're eating, eating should become a

155
00:12:28,230 --> 00:12:29,020
 meditation.

156
00:12:29,020 --> 00:12:33,500
 When you're walking around, sitting down, lying down to

157
00:12:33,500 --> 00:12:34,020
 sleep.

158
00:12:34,020 --> 00:12:39,020
 When you're in the shower, when you're on the toilet.

159
00:12:39,020 --> 00:12:43,610
 Everything you do, every moment. There's a moment when you

160
00:12:43,610 --> 00:12:47,020
 can be cultivating these qualities.

161
00:12:47,020 --> 00:12:50,020
 It takes effort.

162
00:12:50,020 --> 00:12:54,020
 But once you cultivate effort and the effort comes,

163
00:12:54,020 --> 00:12:56,530
 another thing about it is once you're mindful and you start

164
00:12:56,530 --> 00:12:58,020
 to see things clearly,

165
00:12:58,020 --> 00:13:00,020
 you free up a lot of effort.

166
00:13:00,020 --> 00:13:04,160
 When we talk about being tired, we think of being tired as

167
00:13:04,160 --> 00:13:06,020
 because you've worked too hard,

168
00:13:06,020 --> 00:13:10,450
 it's oftentimes not even a physical tired, it's a mental

169
00:13:10,450 --> 00:13:12,020
 stiffness.

170
00:13:12,020 --> 00:13:16,020
 It's like a tension of the mind really.

171
00:13:16,020 --> 00:13:19,390
 And once you're mindful, you suddenly feel all this energy

172
00:13:19,390 --> 00:13:20,020
 come back,

173
00:13:20,020 --> 00:13:25,860
 gradually feel all this energy come back as you start to

174
00:13:25,860 --> 00:13:28,020
 let go and ease up.

175
00:13:28,020 --> 00:13:33,020
 Your mind becomes more focused and more alert.

176
00:13:33,020 --> 00:13:40,130
 And you gain more energy and then the final of the energy

177
00:13:40,130 --> 00:13:44,020
 side is rapture.

178
00:13:44,020 --> 00:13:49,750
 And so once you become energetic, then you start to become

179
00:13:49,750 --> 00:13:53,020
...it becomes habitual.

180
00:13:53,020 --> 00:13:58,020
 So rapture is this state of gaining momentum really,

181
00:13:58,020 --> 00:14:02,020
 becoming powerful.

182
00:14:02,020 --> 00:14:09,020
 Through putting out effort, it becomes stronger and more...

183
00:14:09,020 --> 00:14:12,020
 It's like inertia really.

184
00:14:12,020 --> 00:14:15,250
 When it starts to become...you get in the groove, it

185
00:14:15,250 --> 00:14:21,020
 becomes habitual and easier really.

186
00:14:21,020 --> 00:14:28,020
 More efficient. Rapture refers to anything you get caught

187
00:14:28,020 --> 00:14:28,020
 up in really,

188
00:14:28,020 --> 00:14:31,020
 that's why we use the word rapture.

189
00:14:31,020 --> 00:14:34,020
 So it's easy to get caught up in many different things.

190
00:14:34,020 --> 00:14:37,740
 Here you start to get caught up in mindfulness and that's a

191
00:14:37,740 --> 00:14:39,020
 really good sign.

192
00:14:39,020 --> 00:14:42,310
 Once it becomes second nature where you're just mindful

193
00:14:42,310 --> 00:14:45,020
 constantly throughout the day,

194
00:14:45,020 --> 00:14:49,260
 or it becomes very much a part of who you are, that's rapt

195
00:14:49,260 --> 00:14:50,020
ure.

196
00:14:50,020 --> 00:14:54,660
 You become enraptured by mindfulness. It's sort of what a

197
00:14:54,660 --> 00:14:57,020
 great thing.

198
00:14:57,020 --> 00:15:00,380
 So those ones, well mindfulness is in the center but the

199
00:15:00,380 --> 00:15:03,020
 other three are on the effort side.

200
00:15:03,020 --> 00:15:06,800
 And then we have on the other side of things, your mind

201
00:15:06,800 --> 00:15:10,020
 becomes calmer, so basadiyant.

202
00:15:10,020 --> 00:15:14,100
 Your mind becomes more tranquil, samadhi, your mind becomes

203
00:15:14,100 --> 00:15:15,020
 more focused.

204
00:15:15,020 --> 00:15:19,520
 And upeka, your mind...this is the most important one, your

205
00:15:19,520 --> 00:15:23,020
 mind becomes more equanimous.

206
00:15:23,020 --> 00:15:28,590
 And once you've completely tranquilized the mind and the

207
00:15:28,590 --> 00:15:29,020
 body,

208
00:15:29,020 --> 00:15:37,320
 to the point where you're able to be still and you feel

209
00:15:37,320 --> 00:15:40,020
 very sorted out,

210
00:15:40,020 --> 00:15:45,730
 every experience comes orderly. It's still chaos but it's

211
00:15:45,730 --> 00:15:47,020
 manageable chaos.

212
00:15:47,020 --> 00:15:53,310
 And there's a quietude to it, a stillness, there's no more

213
00:15:53,310 --> 00:15:56,020
 defilements really.

214
00:15:56,020 --> 00:16:00,120
 Samadhi means you're focused, you have this perfect clarity

215
00:16:00,120 --> 00:16:03,020
 of focus on each object as it arises.

216
00:16:03,020 --> 00:16:10,020
 And upeka means you have no judging.

217
00:16:10,020 --> 00:16:13,020
 These are the qualities that we try to develop.

218
00:16:13,020 --> 00:16:16,290
 We put mindfulness in the center, the other ones are on the

219
00:16:16,290 --> 00:16:18,020
 side of effort and concentration.

220
00:16:18,020 --> 00:16:22,020
 So if you have a lot of effort, then you have to be clear

221
00:16:22,020 --> 00:16:23,020
 that you're...

222
00:16:23,020 --> 00:16:27,200
 a lot of energy, over energy, you're distracted and

223
00:16:27,200 --> 00:16:28,020
 restless.

224
00:16:28,020 --> 00:16:31,830
 Then you have to be clear about that and start to focus on

225
00:16:31,830 --> 00:16:33,020
 the other three.

226
00:16:33,020 --> 00:16:38,020
 Calming your mind down, focusing a bit better,

227
00:16:38,020 --> 00:16:44,160
 trying to be less jumpy and less judgmental or reactionary,

228
00:16:44,160 --> 00:16:46,020
 more equanimous.

229
00:16:46,020 --> 00:16:48,510
 I mean just acknowledging those things helps you focus on

230
00:16:48,510 --> 00:16:52,020
...focus your mindfulness really.

231
00:16:52,020 --> 00:16:54,410
 Mindfulness is the tool, you don't have to get too

232
00:16:54,410 --> 00:16:57,020
 distracted by this idea of balancing things.

233
00:16:57,020 --> 00:17:00,690
 It's more of an intellectual thing for you to step back and

234
00:17:00,690 --> 00:17:03,020
 say, "Aha, well that's where I'm obviously not being

235
00:17:03,020 --> 00:17:07,000
 mindful or it's harder for me to be mindful because I'm out

236
00:17:07,000 --> 00:17:09,020
 of balance."

237
00:17:09,020 --> 00:17:13,010
 And so knowing this helps you to focus your mindfulness and

238
00:17:13,010 --> 00:17:19,140
 make you focus your efforts to be more mindful of those

239
00:17:19,140 --> 00:17:23,020
 things that are important.

240
00:17:23,020 --> 00:17:27,020
 So this one, this one really pairs with the first one.

241
00:17:27,020 --> 00:17:32,020
 Remember we had the first one was seeing in this sutta.

242
00:17:32,020 --> 00:17:36,020
 So seeing is our practice, we're trying to see the truth.

243
00:17:36,020 --> 00:17:39,940
 It's a very lofty sounding goal, but it's quite simple, it

244
00:17:39,940 --> 00:17:42,020
 is lofty, but it's simple.

245
00:17:42,020 --> 00:17:45,810
 We're trying to understand what's going on, why are we

246
00:17:45,810 --> 00:17:47,020
 suffering?

247
00:17:47,020 --> 00:17:52,020
 Trying to understand our minds, understand our experience,

248
00:17:52,020 --> 00:17:59,300
 understand reality as a mundane thing, as a process of

249
00:17:59,300 --> 00:18:05,020
 experience that causes us so much stress and suffering,

250
00:18:05,020 --> 00:18:13,020
 and to change that, to fix that really.

251
00:18:13,020 --> 00:18:17,410
 Fix that through seeing, through seeing the problem,

252
00:18:17,410 --> 00:18:21,750
 understanding the problem, and naturally inclining or

253
00:18:21,750 --> 00:18:25,020
 changing the way we incline our minds.

254
00:18:25,020 --> 00:18:30,740
 And so the other five that we've been through up to today

255
00:18:30,740 --> 00:18:34,020
 are more supportive practices.

256
00:18:34,020 --> 00:18:40,700
 And indeed, each one of those supportive practices cuts off

257
00:18:40,700 --> 00:18:43,020
 a part of the problem.

258
00:18:43,020 --> 00:18:49,800
 And these asavas, which means these sort of threads that

259
00:18:49,800 --> 00:18:59,340
 are getting us off balance, that are leading to stress and

260
00:18:59,340 --> 00:19:02,020
 suffering really.

261
00:19:02,020 --> 00:19:05,420
 But the main practice is still seeing, and bhavana is just

262
00:19:05,420 --> 00:19:08,510
 bringing it back home, that as you start to see and start

263
00:19:08,510 --> 00:19:09,020
 practice,

264
00:19:09,020 --> 00:19:12,020
 seeing there are qualities that you are cultivating.

265
00:19:12,020 --> 00:19:16,030
 So bhavana is the qualities, it's the same with the satipat

266
00:19:16,030 --> 00:19:17,020
thana sutta, right?

267
00:19:17,020 --> 00:19:19,960
 The Buddha goes through the satipatthana sutta, and then at

268
00:19:19,960 --> 00:19:23,020
 the very end, well, before getting into the truths,

269
00:19:23,020 --> 00:19:27,880
 he talks about the factors of enlightenment, what you're

270
00:19:27,880 --> 00:19:31,020
 developing through the practice.

271
00:19:31,020 --> 00:19:34,020
 And so this is this section, these seven things,

272
00:19:34,020 --> 00:19:42,020
 mindfulness, investigation, effort, rapture, tranquility,

273
00:19:42,020 --> 00:19:44,020
 concentration, and equanimity.

274
00:19:44,020 --> 00:19:52,020
 These are the seven factors of enlightenment.

275
00:19:52,020 --> 00:20:00,140
 It says something interesting about each of them that I

276
00:20:00,140 --> 00:20:03,970
 didn't mention. It's viveka nisitang, viraag nisitang, niro

277
00:20:03,970 --> 00:20:05,020
d nisitang, wasagaparinamang, naming,

278
00:20:05,020 --> 00:20:16,860
 which means each one of these factors is based on seclusion

279
00:20:16,860 --> 00:20:27,020
, dispassion, cessation, and as giving up relinquishment as

280
00:20:27,020 --> 00:20:33,020
 its ending,

281
00:20:33,020 --> 00:20:37,020
 as its result.

282
00:20:37,020 --> 00:20:40,960
 So the point, the stress here is on the fact that these are

283
00:20:40,960 --> 00:20:47,280
 the qualities that lead to seclusion or are associated with

284
00:20:47,280 --> 00:20:53,020
 seclusion, dispassion, and cessation.

285
00:20:53,020 --> 00:20:58,350
 These are the qualities of mind. These are them. These are

286
00:20:58,350 --> 00:21:04,170
 the way to true seclusion in the sense of being secluded

287
00:21:04,170 --> 00:21:06,020
 from the defilement,

288
00:21:06,020 --> 00:21:11,610
 where your mind is away from all that heat and stress and

289
00:21:11,610 --> 00:21:17,410
 fever of defilement, dispassion and cessation, the

290
00:21:17,410 --> 00:21:20,020
 cessation of suffering,

291
00:21:20,020 --> 00:21:23,510
 and have letting go as their end. So we talk about letting

292
00:21:23,510 --> 00:21:26,020
 go a lot in Buddhism. How does one let go?

293
00:21:26,020 --> 00:21:35,020
 Letting go is the result, parinaming. It's the ending.

294
00:21:35,020 --> 00:21:41,760
 It comes from all of these. It comes when you're mindful

295
00:21:41,760 --> 00:21:47,630
 and when you develop all the qualities that lead to

296
00:21:47,630 --> 00:21:50,020
 enlightenment.

297
00:21:50,020 --> 00:21:55,250
 And so in conclusion, the Buddha says, when one has removed

298
00:21:55,250 --> 00:22:00,080
 all of these defilements, the ones through seeing that

299
00:22:00,080 --> 00:22:03,020
 should be abandoned by seeing,

300
00:22:03,020 --> 00:22:09,820
 the ones through using, through enduring, through avoiding,

301
00:22:09,820 --> 00:22:16,020
 through removing, and through developing,

302
00:22:16,020 --> 00:22:21,130
 then one is called a bhikkhu who dwells restrained with the

303
00:22:21,130 --> 00:22:23,020
 restraint of all the taints.

304
00:22:23,020 --> 00:22:27,030
 It's not quite the language I would use. It's a difficult

305
00:22:27,030 --> 00:22:28,020
 language.

306
00:22:28,020 --> 00:22:37,020
 Dwells restrained. Dwells restrained.

307
00:22:37,020 --> 00:22:42,630
 Having restrained all of the... Yeah, restrained is not the

308
00:22:42,630 --> 00:22:45,020
 best word.

309
00:22:45,020 --> 00:22:52,020
 Sabhasova sammara sambhutto. Having...

310
00:22:52,020 --> 00:22:56,020
 Having...

311
00:22:56,020 --> 00:23:02,020
 Restrained them, I guess.

312
00:23:02,020 --> 00:23:08,100
 So the asavva, again, it's kind of this metaphor of streams

313
00:23:08,100 --> 00:23:16,020
 or this imagery of these things that get us lost.

314
00:23:16,020 --> 00:23:23,020
 It's like a leak. So it means you've plugged all the leaks.

315
00:23:23,020 --> 00:23:27,750
 Our mind ordinarily is at peace. Why can't we just be here

316
00:23:27,750 --> 00:23:29,020
 and now?

317
00:23:29,020 --> 00:23:34,710
 Ordinary reality as we describe it and as we think of it is

318
00:23:34,710 --> 00:23:36,020
 really pure.

319
00:23:36,020 --> 00:23:41,790
 But it's the asavva that get us impure, that get us caught

320
00:23:41,790 --> 00:23:46,020
 up in busyness and stress and friction.

321
00:23:46,020 --> 00:23:50,020
 They're the poison.

322
00:23:50,020 --> 00:23:58,480
 And so once one has done this, one has plugged up all the

323
00:23:58,480 --> 00:24:00,020
 leaks.

324
00:24:00,020 --> 00:24:04,020
 Achyecit anhang, one has cut off craving.

325
00:24:04,020 --> 00:24:14,500
 We watay sanghyojanang, one has untied, we watay, I don't

326
00:24:14,500 --> 00:24:16,020
 know.

327
00:24:16,020 --> 00:24:20,020
 What does bhikkubodi say?

328
00:24:20,020 --> 00:24:25,020
 Flung off the fetters.

329
00:24:25,020 --> 00:24:31,020
 Samma mana bhisamaya antamakasi dukasa.

330
00:24:31,020 --> 00:24:34,820
 With the complete penetration of conceit, he has made the

331
00:24:34,820 --> 00:24:36,020
 end of suffering.

332
00:24:36,020 --> 00:24:42,020
 Made an end of suffering.

333
00:24:42,020 --> 00:24:46,360
 So again, what's great about this sutta is it gives a

334
00:24:46,360 --> 00:24:51,020
 really comprehensive look at the practice.

335
00:24:51,020 --> 00:24:53,570
 I mean, there's much more that could be said about each one

336
00:24:53,570 --> 00:24:57,350
 of these and there's many details and many ways of

337
00:24:57,350 --> 00:25:00,020
 implementing all the aspects of the practice.

338
00:25:00,020 --> 00:25:06,020
 But this gives a good summary of Buddhist practice really.

339
00:25:06,020 --> 00:25:13,540
 And it comes back, starts at seeing and comes back to

340
00:25:13,540 --> 00:25:16,020
 cultivating.

341
00:25:16,020 --> 00:25:20,210
 It really gives us some good pointers and good reminders on

342
00:25:20,210 --> 00:25:24,360
 how to keep our practice going and keep it going in the

343
00:25:24,360 --> 00:25:26,020
 right direction.

344
00:25:26,020 --> 00:25:32,020
 So that's the sabassa of a sutta.

345
00:25:32,020 --> 00:25:39,020
 And that's the Dhamma for tonight.

346
00:25:39,020 --> 00:25:45,020
 We can see if there are questions.

347
00:25:45,020 --> 00:25:48,970
 Is it useful to expose ourselves to situations we are a

348
00:25:48,970 --> 00:25:52,020
verse to in order to learn about them?

349
00:25:52,020 --> 00:25:54,380
 For example, I have an aversion to reading so it would be

350
00:25:54,380 --> 00:25:57,120
 useful for me to try to read something and study my

351
00:25:57,120 --> 00:26:01,020
 reactions using the noting technique.

352
00:26:01,020 --> 00:26:03,020
 I think I just answered this one.

353
00:26:03,020 --> 00:26:08,670
 I mean, it's not really a good idea to intentionally evoke

354
00:26:08,670 --> 00:26:15,540
 defilements because then you're cultivating that triggering

355
00:26:15,540 --> 00:26:17,020
 activity.

356
00:26:17,020 --> 00:26:23,470
 I mean, part of what we're trying to learn is neutrality or

357
00:26:23,470 --> 00:26:25,020
 objectivity.

358
00:26:25,020 --> 00:26:29,520
 And if you're triggering it, there's the real danger of

359
00:26:29,520 --> 00:26:34,020
 getting impatient or ambitious about the practice.

360
00:26:34,020 --> 00:26:48,160
 Hey, if I do this, is it useful? Like this question of is

361
00:26:48,160 --> 00:26:48,540
 it useful? It's potentially ambitious where instead you

362
00:26:48,540 --> 00:26:49,020
 should just live your life and then add mindfulness.

363
00:26:49,020 --> 00:26:53,020
 Let the changes come naturally.

364
00:26:53,020 --> 00:26:56,630
 I mean, instead of going out of your way to read a book

365
00:26:56,630 --> 00:27:00,780
 when you actually naturally are in a position where reading

366
00:27:00,780 --> 00:27:03,020
 is the appropriate thing to do,

367
00:27:03,020 --> 00:27:06,020
 then study it.

368
00:27:06,020 --> 00:27:09,720
 It's more natural that way. There's a problem with sort of

369
00:27:09,720 --> 00:27:16,220
 artificially evoking defilements except in the most

370
00:27:16,220 --> 00:27:19,020
 innocuous sorts of ways like doing sitting meditation,

371
00:27:19,020 --> 00:27:23,250
 walking meditation potentially evokes quite a few

372
00:27:23,250 --> 00:27:25,020
 problematic mind states.

373
00:27:25,020 --> 00:27:30,570
 It's quite benign, I think, in the sense of it's just

374
00:27:30,570 --> 00:27:35,020
 walking and it's just sitting, right?

375
00:27:35,020 --> 00:27:41,020
 But going out of your way, I think, is problematic.

376
00:27:41,020 --> 00:27:48,090
 Who is Sonam Mahathira that Sayadaw mentions on page 87,

377
00:27:48,090 --> 00:27:51,020
 the manual to insight?

378
00:27:51,020 --> 00:27:56,020
 There are two Sonas, but the main one is about effort.

379
00:27:56,020 --> 00:28:04,020
 He was a person who had too much push too hard.

380
00:28:04,020 --> 00:28:09,020
 So I think this is where the simile of the three strings

381
00:28:09,020 --> 00:28:11,020
 comes in, right?

382
00:28:11,020 --> 00:28:22,020
 You have overzealousness, is this?

383
00:28:22,020 --> 00:28:26,020
 The heroic efforts made by Sonam Mahathira.

384
00:28:26,020 --> 00:28:28,020
 This is different, right?

385
00:28:28,020 --> 00:28:34,020
 Thinking about teacher reflecting on the attributes,

386
00:28:34,020 --> 00:28:37,020
 considering the heroic efforts made by Sona.

387
00:28:37,020 --> 00:28:42,850
 So Sona was so, if you want to know about any of these

388
00:28:42,850 --> 00:28:47,420
 sorts of people, the dictionary of poly proper names is a

389
00:28:47,420 --> 00:28:49,020
 really good resource.

390
00:28:49,020 --> 00:28:51,560
 You can look up Sona there. It'll give you all the

391
00:28:51,560 --> 00:28:52,020
 information.

392
00:28:52,020 --> 00:28:58,210
 But he was so delicate that there were hairs apparently

393
00:28:58,210 --> 00:29:02,020
 growing on the bottoms of his feet.

394
00:29:02,020 --> 00:29:04,460
 And the king, I think, even asked to look at his feet

395
00:29:04,460 --> 00:29:07,360
 because he was astonished that someone could possibly have

396
00:29:07,360 --> 00:29:08,020
 hair there.

397
00:29:08,020 --> 00:29:11,220
 But he had these fine hairs on the bottom of his feet. He

398
00:29:11,220 --> 00:29:14,020
 was so delicate, apparently.

399
00:29:14,020 --> 00:29:19,940
 And so when he became a monk, he did walking meditation and

400
00:29:19,940 --> 00:29:23,020
 his feet started bleeding.

401
00:29:23,020 --> 00:29:25,760
 I'm hoping I'm getting the story right, but his feet

402
00:29:25,760 --> 00:29:30,020
 started bleeding, but he persisted.

403
00:29:30,020 --> 00:29:35,290
 And it was because of him I think the Buddha eventually

404
00:29:35,290 --> 00:29:39,020
 allowed the monks to wear sandals.

405
00:29:39,020 --> 00:29:43,020
 Monks would go barefoot before that.

406
00:29:43,020 --> 00:29:50,020
 But at one point it was, I think it was an abhrama.

407
00:29:50,020 --> 00:29:52,350
 I don't think it was the Buddha. The simile of these three

408
00:29:52,350 --> 00:29:53,020
 strings.

409
00:29:53,020 --> 00:29:55,810
 If one is too, if a string is too tight, no it may have

410
00:29:55,810 --> 00:29:57,020
 been the Buddha.

411
00:29:57,020 --> 00:30:00,800
 If a string is too tight, it snaps. If a string is too

412
00:30:00,800 --> 00:30:02,020
 loose, it doesn't play.

413
00:30:02,020 --> 00:30:06,980
 The string has to be perfectly tuned. And that's how your

414
00:30:06,980 --> 00:30:08,020
 effort should be.

415
00:30:08,020 --> 00:30:11,020
 You shouldn't just push too hard.

416
00:30:11,020 --> 00:30:14,550
 So he worked really hard and that's a good thing, but in

417
00:30:14,550 --> 00:30:20,020
 the end he worked too hard.

418
00:30:20,020 --> 00:30:25,250
 But I recommend looking up the dictionary of Pali proper

419
00:30:25,250 --> 00:30:30,020
 names because I may have butchered some of that.

420
00:30:30,020 --> 00:30:40,020
 Could you go over the Anatta Lakhana Sutta next? Sure.

421
00:30:40,020 --> 00:30:46,020
 I'll do that maybe Saturday.

422
00:30:46,020 --> 00:30:50,020
 I want to become a monk. How can I go ahead?

423
00:30:50,020 --> 00:30:51,820
 I'm not going to answer questions about how to become a

424
00:30:51,820 --> 00:30:55,020
 monk because I think it's misguided.

425
00:30:55,020 --> 00:31:03,150
 No, it's problematic because people ordain, there's too

426
00:31:03,150 --> 00:31:04,020
 much.

427
00:31:04,020 --> 00:31:17,020
 It's often the case that people put too much attention and

428
00:31:17,020 --> 00:31:17,020
 attention to ordination.

429
00:31:17,020 --> 00:31:20,130
 And that actually takes away from people's attention to

430
00:31:20,130 --> 00:31:21,020
 meditation.

431
00:31:21,020 --> 00:31:24,790
 It's seen as sort of a quick fix and that causes problems

432
00:31:24,790 --> 00:31:26,020
 from onisteries.

433
00:31:26,020 --> 00:31:32,020
 So I shy away from focusing on that aspect of Buddhism.

434
00:31:32,020 --> 00:31:38,060
 Ordination, it's certainly possible, but it should be

435
00:31:38,060 --> 00:31:43,020
 related to your meditation practice.

436
00:31:43,020 --> 00:31:45,630
 And in that case it'll come naturally. You'll be at a

437
00:31:45,630 --> 00:31:54,020
 center and the opportunity will arise and you just take it.

438
00:31:54,020 --> 00:31:56,570
 I mean, part of the problem is that it's hard to find a

439
00:31:56,570 --> 00:31:58,020
 center where it's a good place to ordain.

440
00:31:58,020 --> 00:32:03,730
 And it's hard for me to give a recommendation because there

441
00:32:03,730 --> 00:32:06,020
 are many different,

442
00:32:06,020 --> 00:32:12,360
 lots of obstacles to becoming a monk and for those who have

443
00:32:12,360 --> 00:32:16,020
 become a monk in modern times.

444
00:32:16,020 --> 00:32:19,330
 I come from a Jewish family. My family is worried that one

445
00:32:19,330 --> 00:32:23,020
 day I might get the idea of becoming a monk.

446
00:32:23,020 --> 00:32:26,470
 I say for years I slowly transform to a fundamentalist in

447
00:32:26,470 --> 00:32:28,020
 my views to Buddhism.

448
00:32:28,020 --> 00:32:31,020
 How could I deal with it with its ignorance?

449
00:32:31,020 --> 00:32:36,920
 It's very difficult to talk to people who have no idea of D

450
00:32:36,920 --> 00:32:38,020
hamma.

451
00:32:38,020 --> 00:32:42,020
 Well, patience and practice on your own.

452
00:32:42,020 --> 00:32:46,260
 And once you become very comfortable in the practice,

453
00:32:46,260 --> 00:32:51,550
 people can't help but over time see that it's benefiting

454
00:32:51,550 --> 00:32:52,020
 you.

455
00:32:52,020 --> 00:32:58,010
 I mean, no, they can, but it becomes a matter of disassoci

456
00:32:58,010 --> 00:33:04,760
ating yourself with such people or bringing them over to

457
00:33:04,760 --> 00:33:06,020
 your side

458
00:33:06,020 --> 00:33:18,020
 where they accept the fact that it's a good thing.

459
00:33:18,020 --> 00:33:20,500
 I mean, I think, sorry if that doesn't really answer your

460
00:33:20,500 --> 00:33:23,020
 question, but it's not really an easily answered question.

461
00:33:23,020 --> 00:33:28,080
 You can't just fix people's ignorance. You can't just solve

462
00:33:28,080 --> 00:33:31,020
 their concerns.

463
00:33:31,020 --> 00:33:36,300
 It could very well be Sariputta's mother was scolding him

464
00:33:36,300 --> 00:33:38,020
 to the very end.

465
00:33:38,020 --> 00:33:43,670
 Sariputta, who was of great wisdom, took until he was dying

466
00:33:43,670 --> 00:33:49,020
 for his mother to see the goodness of what he was doing.

467
00:33:49,020 --> 00:34:10,370
 There was another question about being a monk and I just

468
00:34:10,370 --> 00:34:16,020
 deleted it.

469
00:34:16,020 --> 00:34:19,980
 Yeah, I'm not really going to answer that. Again, I don't

470
00:34:19,980 --> 00:34:23,020
 want people to fixate on this idea of being a monk.

471
00:34:23,020 --> 00:34:25,850
 I know it's very romantic and everyone wants, there's many

472
00:34:25,850 --> 00:34:27,020
 questions about it.

473
00:34:27,020 --> 00:34:34,390
 It's quite an interest and that's a good thing, but maybe

474
00:34:34,390 --> 00:34:36,020
 it's not such a good thing.

475
00:34:36,020 --> 00:34:40,730
 I mean, I think I would discourage that and I would

476
00:34:40,730 --> 00:34:45,020
 encourage people to focus more on meditation

477
00:34:45,020 --> 00:34:52,080
 because becoming a monk, if you have to ask me, you're

478
00:34:52,080 --> 00:34:56,020
 probably not there yet.

479
00:34:56,020 --> 00:35:00,060
 The only time it should be asked is when you're in a

480
00:35:00,060 --> 00:35:04,020
 monastery and you're talking to your monastic teacher

481
00:35:04,020 --> 00:35:10,830
 and you're just looking to further your meditation practice

482
00:35:10,830 --> 00:35:15,020
 that you're already undertaking.

483
00:35:15,020 --> 00:35:19,840
 If you're asking me on the internet, I would say stick to

484
00:35:19,840 --> 00:35:22,020
 your meditation for now

485
00:35:22,020 --> 00:35:28,020
 or find a place to go and do a meditation course.

486
00:35:28,020 --> 00:35:34,650
 It's much more practical, especially in modern times, to

487
00:35:34,650 --> 00:35:37,020
 focus in that way.

488
00:35:37,020 --> 00:35:41,020
 So, that's all for tonight. Thank you all for tuning in.

489
00:35:41,020 --> 00:35:48,020
 Have a good night.

490
00:35:49,020 --> 00:35:54,020
 [birds chirping]

491
00:35:56,020 --> 00:36:01,020
 [birds chirping]

492
00:36:02,020 --> 00:36:07,020
 [birds chirping]

493
00:36:08,020 --> 00:36:13,020
 [birds chirping]

494
00:36:15,020 --> 00:36:20,020
 [birds chirping]

495
00:36:21,020 --> 00:36:26,020
 [birds chirping]

