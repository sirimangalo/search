1
00:00:00,000 --> 00:00:07,360
 The mind still, mostly on the mind.

2
00:00:07,360 --> 00:00:10,360
 It's the one most important thing.

3
00:00:10,360 --> 00:00:18,560
 When you practice meditation you get to taste the mind.

4
00:00:18,560 --> 00:00:19,560
 No?

5
00:00:19,560 --> 00:00:23,870
 All the things that you're tasting while you're here, you

6
00:00:23,870 --> 00:00:27,280
 get to taste the mind.

7
00:00:27,280 --> 00:00:29,870
 Many people say there's that saying, "No, the mind is a

8
00:00:29,870 --> 00:00:31,200
 terrible thing to taste."

9
00:00:31,200 --> 00:00:33,920
 Or is that waste?

10
00:00:33,920 --> 00:00:35,920
 I'm not sure.

11
00:00:35,920 --> 00:00:40,080
 There is a saying, "The mind is a terrible thing to taste."

12
00:00:40,080 --> 00:00:42,880
 It can be, no?

13
00:00:42,880 --> 00:00:51,600
 When you have to deal with what's going on in the mind.

14
00:00:51,600 --> 00:00:58,150
 But from the sounds of it, at this point, if you think back

15
00:00:58,150 --> 00:00:58,960
, then you can see how the

16
00:00:58,960 --> 00:01:02,000
 mind can be a terrible thing to taste.

17
00:01:02,000 --> 00:01:05,320
 But now, now that was it.

18
00:01:05,320 --> 00:01:11,590
 After you've done the foundation course and you've done

19
00:01:11,590 --> 00:01:14,000
 some retreats, you can see the

20
00:01:14,000 --> 00:01:15,000
 difference.

21
00:01:15,000 --> 00:01:18,200
 If anyone asks, "Is meditation of any value?"

22
00:01:18,200 --> 00:01:19,200
 Because you might forget.

23
00:01:19,200 --> 00:01:20,600
 This is the nature of the mind.

24
00:01:20,600 --> 00:01:23,600
 We forget what we've done in the past.

25
00:01:23,600 --> 00:01:29,520
 So you have to keep this in mind and you have to do this.

26
00:01:29,520 --> 00:01:30,680
 Now you have proof.

27
00:01:30,680 --> 00:01:33,640
 And don't forget this.

28
00:01:33,640 --> 00:01:35,890
 Meditation is valuable because it's now brought you to the

29
00:01:35,890 --> 00:01:37,120
 point where the mind isn't such

30
00:01:37,120 --> 00:01:41,400
 a bad thing to taste.

31
00:01:41,400 --> 00:01:42,400
 Why is this?

32
00:01:42,400 --> 00:01:47,050
 Why is it that actually the mind isn't, when you dig, dig,

33
00:01:47,050 --> 00:01:49,520
 dig, dig, and go very, very

34
00:01:49,520 --> 00:01:53,760
 deep that eventually you come to the mind is not such a bad

35
00:01:53,760 --> 00:01:55,720
 thing to experience or to

36
00:01:55,720 --> 00:01:56,720
 be involved in.

37
00:01:56,720 --> 00:01:59,770
 This is because the Buddha said, this is what we're getting

38
00:01:59,770 --> 00:02:04,640
 into today, "Bhabhasaramidang

39
00:02:04,640 --> 00:02:08,240
 bhikkhu vijita."

40
00:02:08,240 --> 00:02:12,760
 Radiant, oh bhikkhus, is the mind.

41
00:02:12,760 --> 00:02:16,400
 Radiant is the mind.

42
00:02:16,400 --> 00:02:19,460
 So people think, you might get the idea anyway, that the

43
00:02:19,460 --> 00:02:21,480
 Buddha thought that our mind is a

44
00:02:21,480 --> 00:02:23,920
 bad thing.

45
00:02:23,920 --> 00:02:29,120
 The mind is a rotten thing that we have to throw away.

46
00:02:29,120 --> 00:02:31,840
 Because he often said that about the body.

47
00:02:31,840 --> 00:02:35,600
 Because the body is something that we have to throw away.

48
00:02:35,600 --> 00:02:41,970
 Before long the body will lie on the earth, the Buddha said

49
00:02:41,970 --> 00:02:42,400
.

50
00:02:42,400 --> 00:02:47,720
 "Niratang vakalinkalang."

51
00:02:47,720 --> 00:02:52,190
 Just like a charred log, as useless as a charred piece of

52
00:02:52,190 --> 00:02:52,920
 wood.

53
00:02:52,920 --> 00:03:00,080
 This body that we cling to, my body.

54
00:03:00,080 --> 00:03:03,890
 Actually all that's happening is it's clinging to us and

55
00:03:03,890 --> 00:03:06,200
 eventually it stops clinging.

56
00:03:06,200 --> 00:03:08,080
 And we have to let it go.

57
00:03:08,080 --> 00:03:09,080
 Some people can't.

58
00:03:09,080 --> 00:03:12,400
 We hear about ghosts knowing graveyards.

59
00:03:12,400 --> 00:03:14,740
 Because once in a while they can't let go and they're

60
00:03:14,740 --> 00:03:15,760
 clinging so much.

61
00:03:15,760 --> 00:03:18,040
 But the body we have to let it go.

62
00:03:18,040 --> 00:03:21,520
 It's something that is actually rotten inside or it's rip

63
00:03:21,520 --> 00:03:23,320
ening and eventually it's going

64
00:03:23,320 --> 00:03:26,000
 to go rotten and fall apart.

65
00:03:26,000 --> 00:03:29,620
 But the mind on the other hand, the Buddha said deep down

66
00:03:29,620 --> 00:03:32,160
 in the mind, the mind is beautiful.

67
00:03:32,160 --> 00:03:33,160
 It's radiant.

68
00:03:33,160 --> 00:03:34,160
 "Bhabhasaramidam bhikkhu vijita."

69
00:03:34,160 --> 00:03:41,600
 So it's not that our mind is defiled.

70
00:03:41,600 --> 00:03:47,080
 Our mind is nothing bad about our mind but what's wrong?

71
00:03:47,080 --> 00:03:59,760
 He says, "Tanchakoh aag kantukhe upakilise hi."

72
00:03:59,760 --> 00:04:00,760
 Upakilitang.

73
00:04:00,760 --> 00:04:10,550
 It becomes defiled, the mind becomes defiled by visiting a

74
00:04:10,550 --> 00:04:16,360
 gantuka, visiting defilements

75
00:04:16,360 --> 00:04:26,360
 or defilements that just pass through.

76
00:04:26,360 --> 00:04:33,640
 And then he says that an ordinary person doesn't see this.

77
00:04:33,640 --> 00:04:35,280
 Doesn't see this as it is.

78
00:04:35,280 --> 00:04:37,040
 So they might think there's something wrong with the mind.

79
00:04:37,040 --> 00:04:41,970
 When an ordinary person has depression or anger or greed,

80
00:04:41,970 --> 00:04:44,680
 it can drive them crazy because

81
00:04:44,680 --> 00:04:46,440
 they think that this is a part of them.

82
00:04:46,440 --> 00:04:50,130
 This is why people take medication trying to change the

83
00:04:50,130 --> 00:04:50,760
 mind.

84
00:04:50,760 --> 00:04:55,030
 They take these medications and it attacks the brain but it

85
00:04:55,030 --> 00:04:57,080
 doesn't get to the mind.

86
00:04:57,080 --> 00:05:00,400
 But because they don't see that actually the mind is bright

87
00:05:00,400 --> 00:05:02,440
, the Buddha said it's for that

88
00:05:02,440 --> 00:05:11,660
 reason that I say there is no, there is no jitabhavana,

89
00:05:11,660 --> 00:05:16,360
 there is no development of mind

90
00:05:16,360 --> 00:05:20,000
 for an ordinary person, for most people.

91
00:05:20,000 --> 00:05:23,920
 Most people are unable to develop their mind because they

92
00:05:23,920 --> 00:05:25,880
 can't see that deep down the

93
00:05:25,880 --> 00:05:37,080
 mind is radiant.

94
00:05:37,080 --> 00:05:40,830
 But a person who has considered carefully and looked into

95
00:05:40,830 --> 00:05:42,560
 the wise things, he says,

96
00:05:42,560 --> 00:05:45,560
 this person is able to see that the mind is beautiful, the

97
00:05:45,560 --> 00:05:47,200
 mind is radiant because they're

98
00:05:47,200 --> 00:05:52,320
 able to practice meditation.

99
00:05:52,320 --> 00:05:56,490
 Then the Buddha goes on, he says in fact, he gives an

100
00:05:56,490 --> 00:05:58,840
 example of the mind of loving

101
00:05:58,840 --> 00:06:00,960
 kindness.

102
00:06:00,960 --> 00:06:27,400
 The mind of loving kindness for example, if a monk or

103
00:06:27,400 --> 00:06:30,320
 anyone who takes on the life of

104
00:06:30,320 --> 00:06:38,280
 a meditator or to practice the mind of loving kindness for

105
00:06:38,280 --> 00:06:43,200
 example, even for an instant.

106
00:06:43,200 --> 00:06:48,840
 So after you practice your meditation you send love to the

107
00:06:48,840 --> 00:06:51,520
 other meditators and your

108
00:06:51,520 --> 00:07:01,070
 parents and your friends and your enemies and to all beings

109
00:07:01,070 --> 00:07:01,640
.

110
00:07:01,640 --> 00:07:05,080
 If you do it even just for a moment, just take a moment to

111
00:07:05,080 --> 00:07:06,000
 send love.

112
00:07:06,000 --> 00:07:13,660
 The Buddha said this person, this bhikkhu, their meditation

113
00:07:13,660 --> 00:07:15,760
 is not in vain.

114
00:07:15,760 --> 00:07:23,790
 They have arita jhani, jhano, arita jhano, they are a

115
00:07:23,790 --> 00:07:28,400
 person who practices jhana not

116
00:07:28,400 --> 00:07:34,160
 in vain, not without purpose, practices meditation.

117
00:07:34,160 --> 00:07:39,560
 We harapli, they dwell meditating with some purpose.

118
00:07:39,560 --> 00:07:46,160
 They are a person who follows the teaching of the Buddha, s

119
00:07:46,160 --> 00:07:50,200
attu sasana karo, who undertakes

120
00:07:50,200 --> 00:07:55,450
 the teaching of the Buddha, the teaching of the teacher,

121
00:07:55,450 --> 00:07:57,480
 this is the Buddha.

122
00:07:57,480 --> 00:08:07,340
 "Ovada pati karo," someone who undertakes the advice, the

123
00:08:07,340 --> 00:08:18,120
 exhortation of the Buddha.

124
00:08:18,120 --> 00:08:19,120
 And so on.

125
00:08:19,120 --> 00:08:22,640
 And then he says, "So what do you think of, or what could

126
00:08:22,640 --> 00:08:24,760
 you say about when someone really

127
00:08:24,760 --> 00:08:39,000
 develops it, develops it extensively?"

128
00:08:39,000 --> 00:08:45,330
 Final thing he has to say is that all akusala dhamma and

129
00:08:45,330 --> 00:08:48,560
 all kusala dhamma, there he goes

130
00:08:48,560 --> 00:08:51,440
 on as well, but we'll stop there.

131
00:08:51,440 --> 00:08:55,880
 All akusala dhamma and akusala dhamma, they have the mind

132
00:08:55,880 --> 00:08:58,560
 as their manopubang, manopubang

133
00:08:58,560 --> 00:09:01,160
 nama.

134
00:09:01,160 --> 00:09:04,160
 They all have the mind as their forerunner.

135
00:09:04,160 --> 00:09:09,200
 The mind comes before all kusala and all kusala.

136
00:09:09,200 --> 00:09:12,140
 So you can put this all together, this is actually the same

137
00:09:12,140 --> 00:09:13,720
 teaching, it goes in order.

138
00:09:13,720 --> 00:09:15,680
 We're talking about the mind.

139
00:09:15,680 --> 00:09:17,680
 Our mind is beautiful.

140
00:09:17,680 --> 00:09:21,840
 Our mind is not something that we should throw away or

141
00:09:21,840 --> 00:09:24,560
 discard or try to run away from.

142
00:09:24,560 --> 00:09:27,890
 The reason why it's so difficult to face the mind is that

143
00:09:27,890 --> 00:09:30,760
 the mind is covered over in defilement.

144
00:09:30,760 --> 00:09:35,170
 It's like spinning around like a top and throwing out all

145
00:09:35,170 --> 00:09:36,960
 these defilements.

146
00:09:36,960 --> 00:09:41,610
 In the beginning that's hard to see, but at this point you

147
00:09:41,610 --> 00:09:43,760
 should be able to see that

148
00:09:43,760 --> 00:09:48,560
 actually the problem isn't the mind, the problem is the def

149
00:09:48,560 --> 00:09:49,720
ilements.

150
00:09:49,720 --> 00:09:53,940
 Actually you can feel good about yourself, you can feel

151
00:09:53,940 --> 00:09:56,120
 good about the mind and focus

152
00:09:56,120 --> 00:09:59,810
 your life on the mind, on keeping your mind free from these

153
00:09:59,810 --> 00:10:01,000
 defilements.

154
00:10:01,000 --> 00:10:04,330
 It's like you have to set up a guard because you have this

155
00:10:04,330 --> 00:10:06,160
 wonderful treasure of a mind

156
00:10:06,160 --> 00:10:08,320
 that can do so much good for you.

157
00:10:08,320 --> 00:10:15,350
 But if it gets taken over by the defilements it will become

158
00:10:15,350 --> 00:10:17,920
 your worst enemy.

159
00:10:17,920 --> 00:10:23,050
 This is why he says that the development of the mind is of

160
00:10:23,050 --> 00:10:25,200
 great benefit, even for just

161
00:10:25,200 --> 00:10:29,720
 a moment you do a great thing.

162
00:10:29,720 --> 00:10:32,170
 I think what's important about this teaching, about a

163
00:10:32,170 --> 00:10:33,760
 moment, is to help us get over the

164
00:10:33,760 --> 00:10:38,670
 idea of how long we're practicing, even hour or day or week

165
00:10:38,670 --> 00:10:41,200
 or month or years, and just

166
00:10:41,200 --> 00:10:43,480
 focus on the moment.

167
00:10:43,480 --> 00:10:47,280
 Not think about how long it's going to take or how long we

168
00:10:47,280 --> 00:10:49,280
 have to sit and meditate for

169
00:10:49,280 --> 00:10:52,400
 or how long we have to stay here.

170
00:10:52,400 --> 00:10:54,440
 Think about just this moment.

171
00:10:54,440 --> 00:10:56,930
 Don't think about, "Oh, in the past I was not a good medit

172
00:10:56,930 --> 00:10:58,160
ator or I didn't meditate

173
00:10:58,160 --> 00:11:04,280
 or I did a lot of bad things in the past."

174
00:11:04,280 --> 00:11:07,480
 Only think of this one moment and you can be proud of

175
00:11:07,480 --> 00:11:09,400
 yourself in that one moment.

176
00:11:09,400 --> 00:11:11,560
 You can feel good about yourself.

177
00:11:11,560 --> 00:11:15,180
 You can feel good about your mind and confidence about what

178
00:11:15,180 --> 00:11:16,200
 you've done.

179
00:11:16,200 --> 00:11:19,780
 Because when you work in moments then you're actually truly

180
00:11:19,780 --> 00:11:21,520
 developing good things.

181
00:11:21,520 --> 00:11:23,520
 You're truly developing goodness.

182
00:11:23,520 --> 00:11:26,400
 You can only develop goodness in a moment.

183
00:11:26,400 --> 00:11:29,880
 You can't develop goodness an hour from now or an hour ago.

184
00:11:29,880 --> 00:11:31,800
 Even a moment from now you can't develop.

185
00:11:31,800 --> 00:11:34,480
 You can only develop it now.

186
00:11:34,480 --> 00:11:36,920
 That's only one moment of goodness.

187
00:11:36,920 --> 00:11:41,640
 The next moment maybe your mind changes and you're into

188
00:11:41,640 --> 00:11:44,400
 delusion or attachment or anger

189
00:11:44,400 --> 00:11:49,640
 or hatred or so on.

190
00:11:49,640 --> 00:11:52,060
 This is a very important teaching to always think in terms

191
00:11:52,060 --> 00:11:52,760
 of moments.

192
00:11:52,760 --> 00:11:56,120
 This is why the Buddha said, "Even for a moment.

193
00:11:56,120 --> 00:11:57,120
 We should think like that.

194
00:11:57,120 --> 00:12:00,680
 Then we should count the moments, not count the hours."

195
00:12:00,680 --> 00:12:03,450
 Never count the hours of practice that you've done in the

196
00:12:03,450 --> 00:12:05,240
 weeks or the months or the years.

197
00:12:05,240 --> 00:12:06,240
 Count the moments.

198
00:12:06,240 --> 00:12:10,720
 Then you think, "Ooh, I've done a lot of meditation.

199
00:12:10,720 --> 00:12:14,000
 I can't even count how much meditation I've done."

200
00:12:14,000 --> 00:12:16,320
 Or maybe you think, "Mmm, I haven't even done many moments.

201
00:12:16,320 --> 00:12:20,760
 I've done many hours but most of those hours were nodding

202
00:12:20,760 --> 00:12:24,200
 off or thinking about other things."

203
00:12:24,200 --> 00:12:27,600
 You can see where the real meditation is.

204
00:12:27,600 --> 00:12:37,440
 The Buddha said it's the mind that is defiled.

205
00:12:37,440 --> 00:12:41,910
 All akusna dhamma, all anhosam dhammas, all the bad things

206
00:12:41,910 --> 00:12:43,520
 we say, it all comes from when

207
00:12:43,520 --> 00:12:46,560
 these defilements take over the mind.

208
00:12:46,560 --> 00:12:48,920
 You can't have akusna dhamma without the mind.

209
00:12:48,920 --> 00:12:51,930
 You can't have anhosam-ness without the judgments of the

210
00:12:51,930 --> 00:12:52,480
 mind.

211
00:12:52,480 --> 00:12:58,600
 And as the mind falls into liking and disliking, clings to

212
00:12:58,600 --> 00:13:04,480
 this, or becomes averse to that,

213
00:13:04,480 --> 00:13:14,000
 it leads us around in so much, so much delusion.

214
00:13:14,000 --> 00:13:20,340
 So our practice is to find the pure mind inside, which is

215
00:13:20,340 --> 00:13:24,360
 something for us to think about.

216
00:13:24,360 --> 00:13:26,800
 Is that actually the mind is not the problem?

217
00:13:26,800 --> 00:13:28,400
 We can see the mind.

218
00:13:28,400 --> 00:13:34,720
 And this is why it works for us to simply see things as

219
00:13:34,720 --> 00:13:36,800
 they are, because there's nothing

220
00:13:36,800 --> 00:13:39,080
 wrong with things.

221
00:13:39,080 --> 00:13:41,200
 There's nothing wrong with the world around us.

222
00:13:41,200 --> 00:13:44,800
 There's nothing wrong with our mind.

223
00:13:44,800 --> 00:13:49,410
 What's wrong is the judgments and the defilements that

224
00:13:49,410 --> 00:13:51,200
 cover the mind up.

225
00:13:51,200 --> 00:13:54,020
 When you can see things as they are, and just thinking,

226
00:13:54,020 --> 00:13:56,320
 thinking, or seeing, seeing, hearing,

227
00:13:56,320 --> 00:14:00,200
 smelling, suddenly the mind is pure again.

228
00:14:00,200 --> 00:14:05,200
 You think the mind is inherently dirty, it's not true.

229
00:14:05,200 --> 00:14:10,290
 At the moment when you see seeing, you connect with the

230
00:14:10,290 --> 00:14:11,600
 pure mind.

231
00:14:11,600 --> 00:14:15,360
 Your mind is pure, clear, you know seeing.

232
00:14:15,360 --> 00:14:19,720
 When you lift your foot, you know you're lifting the foot,

233
00:14:19,720 --> 00:14:22,120
 this is lifting, you have a clear

234
00:14:22,120 --> 00:14:23,120
 mind.

235
00:14:23,120 --> 00:14:28,080
 At that moment there's no defilements in the mind.

236
00:14:28,080 --> 00:14:33,790
 When you know the movement, when you know the object, when

237
00:14:33,790 --> 00:14:36,440
 you know the stomach, and

238
00:14:36,440 --> 00:14:40,600
 you know this is rising, just knowing this is rising, the

239
00:14:40,600 --> 00:14:41,800
 mind is pure.

240
00:14:41,800 --> 00:14:44,530
 So we're not trying to create anything, we're trying to get

241
00:14:44,530 --> 00:14:45,280
 rid of stuff.

242
00:14:45,280 --> 00:14:49,730
 So that in the end all that's left is our awareness of

243
00:14:49,730 --> 00:14:51,680
 things as they are.

244
00:14:51,680 --> 00:14:55,880
 This is this is this.

245
00:14:55,880 --> 00:15:01,290
 Certainly isn't something easy, but it's something we

246
00:15:01,290 --> 00:15:05,240
 should feel confident that deep down inside

247
00:15:05,240 --> 00:15:08,240
 we're perfectly pure.

248
00:15:08,240 --> 00:15:13,960
 You just have to find that perfectly pure, perfect pure.

249
00:15:13,960 --> 00:15:18,120
 Get rid of all the impurity that's covering it up.

250
00:15:18,120 --> 00:15:21,120
 Digging down to find the buried treasure.

251
00:15:21,120 --> 00:15:25,710
 So just another short teaching, something for us to think

252
00:15:25,710 --> 00:15:28,120
 about as we go through the,

253
00:15:28,120 --> 00:15:31,800
 we try to just go through the Buddhist teaching and then

254
00:15:31,800 --> 00:15:34,040
 you'll have an idea of what the Buddhist

255
00:15:34,040 --> 00:15:35,040
 teach.

256
00:15:35,040 --> 00:15:38,390
 I think even from just this brief few teachings that we've

257
00:15:38,390 --> 00:15:40,360
 gone through, you can see how much

258
00:15:40,360 --> 00:15:45,160
 emphasis the Buddha placed on meditation and on the mind.

259
00:15:45,160 --> 00:15:49,750
 So he didn't say studying or helping other people or so on,

260
00:15:49,750 --> 00:15:52,000
 he said developing the mind

261
00:15:52,000 --> 00:15:57,750
 even for just a moment that's what makes your monastic life

262
00:15:57,750 --> 00:16:00,960
, the life of a monk worthwhile.

263
00:16:00,960 --> 00:16:04,680
 Not to mention the life of all beings.

264
00:16:04,680 --> 00:16:07,560
 So this is for us to develop.

265
00:16:07,560 --> 00:16:10,200
 It's encouragement that we're on the right path and we're

266
00:16:10,200 --> 00:16:11,720
 doing as the Buddha said and

267
00:16:11,720 --> 00:16:15,510
 just now he said this is someone who follows his exhort

268
00:16:15,510 --> 00:16:18,400
ation, this is someone who undertakes

269
00:16:18,400 --> 00:16:26,400
 his teaching, who performs the instruction of the Buddha.

270
00:16:26,400 --> 00:16:31,820
 So we're practicing Buddhism here and we have the Buddha to

271
00:16:31,820 --> 00:16:33,120
 back us up.

272
00:16:33,120 --> 00:16:37,170
 So without further ado we'll move on and get on with our

273
00:16:37,170 --> 00:16:38,400
 meditation.

274
00:16:38,400 --> 00:16:42,480
 So we can all go back and do mindful frustration walking on

275
00:16:42,480 --> 00:16:43,400
 through.

