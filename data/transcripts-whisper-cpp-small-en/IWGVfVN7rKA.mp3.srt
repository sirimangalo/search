1
00:00:00,000 --> 00:00:04,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,000 --> 00:00:13,000
 Today we continue with verse 225, which reads as follows.

3
00:00:15,000 --> 00:00:22,000
 Ahing saka ye monayo nityanga yena sanguta

4
00:00:22,000 --> 00:00:30,000
 te yanti ajutang tanang yatagantva nasulchari

5
00:00:30,000 --> 00:00:34,000
 which means

6
00:00:34,000 --> 00:00:45,000
 non-violent those sages ever restrained in body.

7
00:00:45,000 --> 00:00:53,000
 They go to a place they never fall away from

8
00:00:53,000 --> 00:01:00,000
 where having gone one suffers not, one sorrows not.

9
00:01:00,000 --> 00:01:14,000
 This verse was apparently taught in response to a question

10
00:01:14,000 --> 00:01:24,650
 that relates to a story about the Buddha when he was living

11
00:01:24,650 --> 00:01:28,000
 near Saketa.

12
00:01:28,000 --> 00:01:34,000
 The story goes that there was a Brahmin living in the area.

13
00:01:34,000 --> 00:01:38,000
 And when the Buddha went on alms around this Brahmin,

14
00:01:38,000 --> 00:01:44,000
 saw the Buddha walking and rushed up to him and said,

15
00:01:44,000 --> 00:01:53,000
 "My son, my son, what are you doing? Where have you been?

16
00:01:53,000 --> 00:01:59,160
 Where have you been? Why have you not come home? Why have

17
00:01:59,160 --> 00:02:03,000
 you not come to see us?"

18
00:02:03,000 --> 00:02:08,320
 And he invited the Buddha to his house and to his wife and

19
00:02:08,320 --> 00:02:10,000
 his wife said,

20
00:02:10,000 --> 00:02:16,000
 the Brahmin woman said, "My son, where have you been?

21
00:02:16,000 --> 00:02:19,000
 Why have you not come to visit?"

22
00:02:19,000 --> 00:02:23,000
 And they called out all of their children and said, "Come,

23
00:02:23,000 --> 00:02:23,000
 come pay respect.

24
00:02:23,000 --> 00:02:30,460
 Your brother is a rishi, is an ascetic. Come and pay

25
00:02:30,460 --> 00:02:34,000
 respect to your brother."

26
00:02:34,000 --> 00:02:40,490
 And the Buddha was silent and he waited through it

27
00:02:40,490 --> 00:02:41,000
 patiently

28
00:02:41,000 --> 00:02:45,000
 and they asked the Buddha, they said,

29
00:02:45,000 --> 00:02:48,490
 "Our son, please, you must come to eat at our house every

30
00:02:48,490 --> 00:02:49,000
 day.

31
00:02:49,000 --> 00:02:53,200
 Why would you go anywhere else when you're in your home,

32
00:02:53,200 --> 00:02:58,000
 when you're near us, your parents?"

33
00:02:58,000 --> 00:03:03,830
 And the Buddha said, "Oh, it's not our way to go to the

34
00:03:03,830 --> 00:03:06,000
 same place regularly."

35
00:03:06,000 --> 00:03:14,130
 Buddha's way is to support and commune with so many, with

36
00:03:14,130 --> 00:03:20,000
 all sorts of different people by moving, by traveling.

37
00:03:20,000 --> 00:03:23,340
 And they said, "Well, then please send us monks, send us

38
00:03:23,340 --> 00:03:24,000
 your followers,

39
00:03:24,000 --> 00:03:29,330
 send us other followers of your teaching, other monks, we

40
00:03:29,330 --> 00:03:33,000
'll take care for them."

41
00:03:33,000 --> 00:03:37,160
 And they did this and whenever the Buddha was free, he

42
00:03:37,160 --> 00:03:38,000
 would go to their house

43
00:03:38,000 --> 00:03:41,610
 and again and again they listened to his teachings and they

44
00:03:41,610 --> 00:03:44,000
 came to the monastery,

45
00:03:44,000 --> 00:03:50,000
 always calling him our son.

46
00:03:50,000 --> 00:03:52,000
 And they became anagamis.

47
00:03:52,000 --> 00:03:56,000
 Throughout this time, they practiced the Buddha's teaching

48
00:03:56,000 --> 00:03:58,000
 and listened to his teaching

49
00:03:58,000 --> 00:04:04,570
 and became devout followers and were successful in becoming

50
00:04:04,570 --> 00:04:07,000
 anagami, the non-returner, meaning

51
00:04:07,000 --> 00:04:10,000
 when they passed away they would never be reborn again.

52
00:04:10,000 --> 00:04:15,730
 They had given up all greed, all anger, all sensual greed

53
00:04:15,730 --> 00:04:20,000
 anyway and all anger.

54
00:04:20,000 --> 00:04:23,000
 They were very pure.

55
00:04:23,000 --> 00:04:26,940
 And so the monks took this up as a topic of conversation

56
00:04:26,940 --> 00:04:30,000
 one day asking, "What's going on here?"

57
00:04:30,000 --> 00:04:40,790
 Not only do these Brahmin people erroneously label the

58
00:04:40,790 --> 00:04:42,000
 Buddha as their son,

59
00:04:42,000 --> 00:04:45,000
 the Buddha doesn't correct them.

60
00:04:45,000 --> 00:04:51,840
 They know that the Buddha has a father and a mother, Sudhod

61
00:04:51,840 --> 00:04:54,000
ana and Mahapajapati,

62
00:04:54,000 --> 00:04:58,000
 they were his parents.

63
00:04:58,000 --> 00:05:03,550
 And the Buddha doesn't bother to correct them, it's quite

64
00:05:03,550 --> 00:05:06,000
 exceptional.

65
00:05:06,000 --> 00:05:08,760
 And the Buddha heard them and he said, "Oh, you want to

66
00:05:08,760 --> 00:05:12,000
 know the reason why they call me their son?"

67
00:05:12,000 --> 00:05:16,000
 And he told a story of the past in Jataka and he said,

68
00:05:16,000 --> 00:05:24,650
 "In 500 lifetimes, in 500 births, both of these two lovely

69
00:05:24,650 --> 00:05:27,000
 people were my parents.

70
00:05:27,000 --> 00:05:31,000
 In 500 births, they were my aunt and uncle.

71
00:05:31,000 --> 00:05:37,000
 And in 500 births, they were my grandparents."

72
00:05:37,000 --> 00:05:42,420
 So again and again and again we were related and as a

73
00:05:42,420 --> 00:05:44,000
 result this perception,

74
00:05:44,000 --> 00:05:52,000
 this recognition has stuck with them.

75
00:05:52,000 --> 00:05:56,170
 From that time on, the story goes on to say that they spent

76
00:05:56,170 --> 00:05:58,000
 the rains with the Buddha,

77
00:05:58,000 --> 00:06:00,610
 that the Buddha relied upon them and for the, during the

78
00:06:00,610 --> 00:06:02,000
 three months of the rainy season,

79
00:06:02,000 --> 00:06:07,010
 he went ever and again to their house and through his

80
00:06:07,010 --> 00:06:10,000
 guidance they became Arahant

81
00:06:10,000 --> 00:06:12,000
 and passed away.

82
00:06:12,000 --> 00:06:15,320
 And when they passed away, the monks asked, they asked the

83
00:06:15,320 --> 00:06:16,000
 Buddha,

84
00:06:16,000 --> 00:06:19,000
 "Where did they go, Venerable Sir?"

85
00:06:19,000 --> 00:06:22,490
 Not realizing that they had become Arahant, never to be

86
00:06:22,490 --> 00:06:24,000
 born again.

87
00:06:24,000 --> 00:06:30,200
 They said, "Oh, people, sages like them, special

88
00:06:30,200 --> 00:06:33,000
 individuals, quite exceptional,

89
00:06:33,000 --> 00:06:35,800
 are never born again. They go to a place, where did they go

90
00:06:35,800 --> 00:06:36,000
?

91
00:06:36,000 --> 00:06:40,000
 They go to a place that you'll never fall away from."

92
00:06:40,000 --> 00:06:46,000
 And then he taught this verse.

93
00:06:46,000 --> 00:06:49,000
 So the lesson of the story, if there is one, I think,

94
00:06:49,000 --> 00:06:53,050
 especially relating to meditation, is in regards to

95
00:06:53,050 --> 00:06:54,000
 perception

96
00:06:54,000 --> 00:07:01,470
 and in regards to concepts, specifically concepts of

97
00:07:01,470 --> 00:07:04,000
 relationship.

98
00:07:04,000 --> 00:07:11,000
 The story reminds us that our relationships are conceptual

99
00:07:11,000 --> 00:07:15,000
 based on our perceptions.

100
00:07:15,000 --> 00:07:19,380
 Many of us, I think, can relate not necessarily to the

101
00:07:19,380 --> 00:07:21,000
 specific perception

102
00:07:21,000 --> 00:07:26,050
 of these Brahman couple, but to this general type of

103
00:07:26,050 --> 00:07:27,000
 perception

104
00:07:27,000 --> 00:07:31,000
 where you recognize someone who you've never met before.

105
00:07:31,000 --> 00:07:35,000
 They're in sharper focus than everyone else

106
00:07:35,000 --> 00:07:40,610
 and your connection to them is quick and strong and lasting

107
00:07:40,610 --> 00:07:41,000
.

108
00:07:41,000 --> 00:07:45,000
 We see this with people we just met for the first time,

109
00:07:45,000 --> 00:07:49,000
 and it might be good, it might be bad, it might be negative

110
00:07:49,000 --> 00:07:52,000
 in the sense that we become enemies, rivals.

111
00:07:52,000 --> 00:07:57,000
 It can be just as sharp as with friends.

112
00:07:57,000 --> 00:08:02,000
 But we generally don't look at family the same way,

113
00:08:02,000 --> 00:08:08,720
 and this story gives us a reminder that even our family

114
00:08:08,720 --> 00:08:10,000
 members

115
00:08:10,000 --> 00:08:13,000
 are no less conceptual than our friends,

116
00:08:13,000 --> 00:08:16,000
 the relationships with our friends and so on.

117
00:08:16,000 --> 00:08:21,200
 Our loved ones, our lovers, our romantic, involving

118
00:08:21,200 --> 00:08:23,000
 partners.

119
00:08:23,000 --> 00:08:28,000
 Who is to say that this couple is any less the Buddhist

120
00:08:28,000 --> 00:08:28,000
 parents

121
00:08:28,000 --> 00:08:39,000
 than Sudodana or Mahapachapati or Mahamaya?

122
00:08:39,000 --> 00:08:44,000
 Just because their relationship with the Buddha is older,

123
00:08:44,000 --> 00:08:50,250
 his past has changed, in the end it all comes down to our

124
00:08:50,250 --> 00:08:51,000
 perceptions.

125
00:08:51,000 --> 00:08:55,000
 So if there's any lesson to be had for a meditator,

126
00:08:55,000 --> 00:09:01,780
 it's to remind us that our reification of things is just

127
00:09:01,780 --> 00:09:03,000
 conceptual

128
00:09:03,000 --> 00:09:09,000
 and helping us to see that our perception of everything,

129
00:09:09,000 --> 00:09:13,490
 of people and places and things and our attachment to

130
00:09:13,490 --> 00:09:14,000
 things

131
00:09:14,000 --> 00:09:20,000
 is based on this cultivation of a perception that builds up

132
00:09:20,000 --> 00:09:20,000
 over time

133
00:09:20,000 --> 00:09:22,000
 and with attention.

134
00:09:22,000 --> 00:09:25,000
 The more attention and the more focus we put on something,

135
00:09:25,000 --> 00:09:29,000
 if it's a person, then on our relationship,

136
00:09:29,000 --> 00:09:33,000
 the stronger that perception becomes.

137
00:09:33,000 --> 00:09:37,000
 Lifetime after lifetime after lifetime it builds up

138
00:09:37,000 --> 00:09:41,000
 and that's the only reason why we are the way we are

139
00:09:41,000 --> 00:09:44,000
 and why we have the personality we have.

140
00:09:44,000 --> 00:09:47,620
 Not because we were born with it, it's much deeper than

141
00:09:47,620 --> 00:09:48,000
 that.

142
00:09:48,000 --> 00:09:51,000
 It's because we cultivate it and we've cultivated it

143
00:09:51,000 --> 00:09:54,000
 and continue cultivating it.

144
00:09:54,000 --> 00:10:00,170
 And so meditation is very much a practice of refining and

145
00:10:00,170 --> 00:10:01,000
 adjusting those habits,

146
00:10:01,000 --> 00:10:04,000
 straightening out our personalities.

147
00:10:04,000 --> 00:10:07,330
 We don't have to lose our personalities, just straighten

148
00:10:07,330 --> 00:10:09,000
 them out.

149
00:10:09,000 --> 00:10:11,850
 And what we lose, what we remove are the ones that we see

150
00:10:11,850 --> 00:10:13,000
 clearly for ourselves

151
00:10:13,000 --> 00:10:19,000
 without any reliance on others at all,

152
00:10:19,000 --> 00:10:27,000
 as being harmful and stressful and a cause for suffering.

153
00:10:27,000 --> 00:10:31,450
 But the deeper lesson of course comes from the verse as

154
00:10:31,450 --> 00:10:32,000
 usual.

155
00:10:32,000 --> 00:10:37,000
 And the lesson I think, the main lesson from the verse

156
00:10:37,000 --> 00:10:44,000
 relates again to this difference between worldly pursuits

157
00:10:44,000 --> 00:10:50,000
 and spiritual or holy religious pursuits.

158
00:10:50,000 --> 00:10:54,000
 Because it relates to violence,

159
00:10:54,000 --> 00:11:01,970
 it relates to activity, unrestrained activity with the body

160
00:11:01,970 --> 00:11:02,000
.

161
00:11:02,000 --> 00:11:07,700
 You know, acting on impulse, acting on ambition, acting on

162
00:11:07,700 --> 00:11:10,000
 desire.

163
00:11:10,000 --> 00:11:14,000
 It's the way of the world. It's a way that gets you what

164
00:11:14,000 --> 00:11:14,000
 you want.

165
00:11:14,000 --> 00:11:21,000
 Very often anger, violence,

166
00:11:21,000 --> 00:11:23,870
 and by violence not just physical violence, verbal violence

167
00:11:23,870 --> 00:11:26,000
, verbal conflict,

168
00:11:26,000 --> 00:11:28,000
 very often gets you what you want.

169
00:11:28,000 --> 00:11:33,280
 Verbal manipulation, physical acts of manipulation, of

170
00:11:33,280 --> 00:11:36,000
 coercion, of threat,

171
00:11:36,000 --> 00:11:42,000
 of intimidation, very often gets people what they want.

172
00:11:42,000 --> 00:11:45,000
 And so the way of the world is one way.

173
00:11:45,000 --> 00:11:49,000
 The way of the dhamma is very different.

174
00:11:49,000 --> 00:11:52,000
 And it's in stark contrast.

175
00:11:52,000 --> 00:11:55,570
 Because the way of the world always, whether it relates to

176
00:11:55,570 --> 00:11:56,000
 anger and violence

177
00:11:56,000 --> 00:12:02,540
 or whether it relates to greed, which also has to do with

178
00:12:02,540 --> 00:12:04,000
 violence,

179
00:12:04,000 --> 00:12:10,350
 is ultimately about the reliance on certain situations to

180
00:12:10,350 --> 00:12:13,000
 make you happy.

181
00:12:13,000 --> 00:12:16,000
 We get what we want. We accomplish what we desire.

182
00:12:16,000 --> 00:12:23,000
 We become proud and attached and identify with things.

183
00:12:23,000 --> 00:12:27,000
 More and more we become dependent and reliant on them.

184
00:12:27,000 --> 00:12:30,000
 We suffer from the anger of the violence.

185
00:12:30,000 --> 00:12:33,000
 We suffer from the greed.

186
00:12:33,000 --> 00:12:39,000
 The desire which stresses our taxes, our system.

187
00:12:39,000 --> 00:12:43,750
 We suffer from the worry and the fear of other people's

188
00:12:43,750 --> 00:12:44,000
 violence.

189
00:12:44,000 --> 00:12:48,000
 We suffer from and their greed,

190
00:12:48,000 --> 00:12:50,980
 the concern for losing what we have from having it

191
00:12:50,980 --> 00:12:52,000
 threatened.

192
00:12:52,000 --> 00:12:55,560
 The more violent you are, whether it be through greed or

193
00:12:55,560 --> 00:12:56,000
 anger,

194
00:12:56,000 --> 00:13:01,150
 the more concerned you become, the more susceptible you

195
00:13:01,150 --> 00:13:02,000
 become.

196
00:13:02,000 --> 00:13:06,000
 Because you become familiar with loss, the concept of loss.

197
00:13:06,000 --> 00:13:14,000
 Only someone who has something to lose suffers from loss.

198
00:13:14,000 --> 00:13:18,000
 Only someone who is attached to the things that they have

199
00:13:18,000 --> 00:13:21,670
 or the things they might get can suffer from losing or not

200
00:13:21,670 --> 00:13:23,000
 getting what they want,

201
00:13:23,000 --> 00:13:28,000
 what they like, what they want, what they need.

202
00:13:28,000 --> 00:13:32,000
 And we suffer when it is taken away, when we are conquered.

203
00:13:32,000 --> 00:13:37,300
 The conqueror who is conquered suffers more than the one

204
00:13:37,300 --> 00:13:41,000
 who conquers not.

205
00:13:41,000 --> 00:13:50,580
 We suffer when we are taken advantage of when we lose the

206
00:13:50,580 --> 00:13:54,000
 things we want to others.

207
00:13:54,000 --> 00:13:58,000
 We, the manipulator, are manipulated.

208
00:13:58,000 --> 00:14:02,000
 The greedy one has things taken from them.

209
00:14:02,000 --> 00:14:03,000
 This is how we suffer.

210
00:14:03,000 --> 00:14:11,000
 So it's constantly in a state of uncertainty.

211
00:14:11,000 --> 00:14:13,150
 And this is the essence of the Buddhist teaching on

212
00:14:13,150 --> 00:14:14,000
 uncertainty,

213
00:14:14,000 --> 00:14:18,000
 on anicca, impermanence.

214
00:14:18,000 --> 00:14:23,820
 Or someone who relies upon this king of the hill, king of

215
00:14:23,820 --> 00:14:26,000
 the castle mentality

216
00:14:26,000 --> 00:14:30,000
 where you can rise to the top.

217
00:14:30,000 --> 00:14:37,000
 Someone who rises to the top has that much further to fall.

218
00:14:37,000 --> 00:14:44,000
 It's that much more susceptible to loss.

219
00:14:44,000 --> 00:14:51,990
 And so it seems that the person who wins, who is victorious

220
00:14:51,990 --> 00:14:52,000
,

221
00:14:52,000 --> 00:14:55,000
 that they've got it made.

222
00:14:55,000 --> 00:15:01,000
 There is no way, there is no instance,

223
00:15:01,000 --> 00:15:07,000
 there is no possibility for that to be lasting, permanent,

224
00:15:07,000 --> 00:15:08,000
 stable,

225
00:15:08,000 --> 00:15:16,000
 to be any sort of everlasting goal.

226
00:15:16,000 --> 00:15:22,050
 Even putting aside death, the ultimate end to any

227
00:15:22,050 --> 00:15:25,000
 accomplishment,

228
00:15:25,000 --> 00:15:29,000
 any worldly accomplishment.

229
00:15:29,000 --> 00:15:35,000
 The simple reliance on victory, the simple frame of mind

230
00:15:35,000 --> 00:15:41,000
 is one that is unbalanced, requires this,

231
00:15:41,000 --> 00:15:47,000
 is constantly stressed, constantly in a state of suffering.

232
00:15:47,000 --> 00:15:52,000
 A person who gets what they want feels so happy

233
00:15:52,000 --> 00:15:55,000
 and thinks they are so happy.

234
00:15:55,000 --> 00:16:01,000
 They are so full of greed and anger and delusion,

235
00:16:01,000 --> 00:16:06,000
 they can't see while they are suffering.

236
00:16:06,000 --> 00:16:09,830
 They can't see how tense and stressed and perverted their

237
00:16:09,830 --> 00:16:10,000
 mind is,

238
00:16:10,000 --> 00:16:12,000
 how perverse the mind is.

239
00:16:12,000 --> 00:16:16,190
 A person who is perverse misses the fact that that per

240
00:16:16,190 --> 00:16:20,000
version is a cause for suffering.

241
00:16:20,000 --> 00:16:24,950
 So we have this example of, worldly example of police

242
00:16:24,950 --> 00:16:26,000
 brutality.

243
00:16:26,000 --> 00:16:30,790
 And we have on the other side the example of protesters

244
00:16:30,790 --> 00:16:33,000
 being angry and violent

245
00:16:33,000 --> 00:16:47,000
 and not to really get involved and not to equiv--

246
00:16:47,000 --> 00:16:50,000
 not to create an equivalence,

247
00:16:50,000 --> 00:16:55,320
 because clearly there is something to be-- to stand up and

248
00:16:55,320 --> 00:16:56,000
 say something about.

249
00:16:56,000 --> 00:16:59,000
 Clearly there is something wrong with the system.

250
00:16:59,000 --> 00:17:02,350
 The system is the system, the society that we have built

251
00:17:02,350 --> 00:17:06,000
 for ourselves is based on greed.

252
00:17:06,000 --> 00:17:10,070
 There is a problem with what might be called police

253
00:17:10,070 --> 00:17:13,000
 brutality, there's no question.

254
00:17:13,000 --> 00:17:17,760
 And that's an issue that is worth standing up for and

255
00:17:17,760 --> 00:17:20,000
 speaking out about.

256
00:17:20,000 --> 00:17:24,810
 That being said, take stepping back and looking from the

257
00:17:24,810 --> 00:17:28,000
 point of view of all

258
00:17:28,000 --> 00:17:33,000
 who might wish for good things.

259
00:17:33,000 --> 00:17:39,000
 No good things come from anger, from violence,

260
00:17:39,000 --> 00:17:42,490
 whether it be on the side of the police who think they're

261
00:17:42,490 --> 00:17:45,000
 so good getting their way,

262
00:17:45,000 --> 00:17:49,060
 they have the power, not realizing how torturous it must be

263
00:17:49,060 --> 00:17:54,000
 to be such a violent and cruel individual.

264
00:17:54,000 --> 00:17:57,000
 Fearful, even.

265
00:17:57,000 --> 00:18:00,010
 How much--there's apparently a greater amount of trauma

266
00:18:00,010 --> 00:18:02,000
 because they're under pressure,

267
00:18:02,000 --> 00:18:06,780
 peer pressure from other police officers, from their

268
00:18:06,780 --> 00:18:09,000
 superiors and so on.

269
00:18:09,000 --> 00:18:13,410
 And on the other side of the protesters who have such a

270
00:18:13,410 --> 00:18:16,000
 noble aspiration to end racism,

271
00:18:16,000 --> 00:18:20,950
 to end discrimination, to end cruelty and brutality, to end

272
00:18:20,950 --> 00:18:24,000
 evil states of mind.

273
00:18:24,000 --> 00:18:26,890
 Ultimately, they may not phrase it, they may not realize it

274
00:18:26,890 --> 00:18:27,000
,

275
00:18:27,000 --> 00:18:32,000
 ultimately it comes down to evil states of mind.

276
00:18:32,000 --> 00:18:35,120
 Without the evil states of mind, the unwholesome, the

277
00:18:35,120 --> 00:18:38,000
 states of mind that cause stress and suffering,

278
00:18:38,000 --> 00:18:42,340
 without those mind states there would be no tension or

279
00:18:42,340 --> 00:18:46,000
 there would be no conflict.

280
00:18:46,000 --> 00:18:50,560
 And so as a result, we, itself, are feeling to increase and

281
00:18:50,560 --> 00:18:54,000
 to cultivate anger.

282
00:18:54,000 --> 00:18:58,100
 It may get us what we want, it may even be the right

283
00:18:58,100 --> 00:18:59,000
 solution,

284
00:18:59,000 --> 00:19:02,650
 it may even be the solution in the sense that on a worldly

285
00:19:02,650 --> 00:19:06,000
 level it does change things for the better.

286
00:19:06,000 --> 00:19:11,300
 But as a rule, and for our own spiritual health, for our

287
00:19:11,300 --> 00:19:13,000
 own mental health and well-being,

288
00:19:13,000 --> 00:19:18,050
 which ultimately is what it's all about, for our own

289
00:19:18,050 --> 00:19:21,000
 spiritual mental well-being,

290
00:19:21,000 --> 00:19:26,000
 we can't succumb to anger and violence.

291
00:19:26,000 --> 00:19:30,120
 Ultimately, it's self-defeating because if the goal is to

292
00:19:30,120 --> 00:19:36,000
 be achieved with anger and violence,

293
00:19:36,000 --> 00:19:39,000
 then what kind of a goal is it? What is the result?

294
00:19:39,000 --> 00:19:44,040
 If that's the outcome, then we haven't really fixed

295
00:19:44,040 --> 00:19:45,000
 anything.

296
00:19:45,000 --> 00:19:54,000
 It's not a world worth living in, even if it does change.

297
00:19:54,000 --> 00:20:00,720
 So that's on the side of, well, that's the disadvantages to

298
00:20:00,720 --> 00:20:01,000
 that.

299
00:20:01,000 --> 00:20:07,960
 And in opposition, we have the way of the Dhamma, one who

300
00:20:07,960 --> 00:20:10,000
 is nonviolent,

301
00:20:10,000 --> 00:20:14,720
 one who is controlled, "kaayana samhuta," which means

302
00:20:14,720 --> 00:20:16,000
 controlled in body,

303
00:20:16,000 --> 00:20:21,110
 but the commentary says it doesn't just relate to body, it

304
00:20:21,110 --> 00:20:24,000
's actually body, speech and mind, of course.

305
00:20:24,000 --> 00:20:31,000
 The Buddha wasn't just praising physical restraint.

306
00:20:31,000 --> 00:20:38,680
 "kaaya" often just means in the being, so it refers to all

307
00:20:38,680 --> 00:20:40,000
 three.

308
00:20:40,000 --> 00:20:43,000
 This way is very different.

309
00:20:43,000 --> 00:20:45,710
 We might put up an equivalence and say, "Well, what about

310
00:20:45,710 --> 00:20:53,000
 you? Aren't you attached to your way as well?"

311
00:20:53,000 --> 00:20:56,550
 We don't want to think that there might be one way, one

312
00:20:56,550 --> 00:21:00,180
 thing better than another, one way of life better than

313
00:21:00,180 --> 00:21:01,000
 another,

314
00:21:01,000 --> 00:21:05,870
 meaning we quite often put up these false equivalences

315
00:21:05,870 --> 00:21:08,000
 between different things.

316
00:21:08,000 --> 00:21:13,710
 So we have to point out the stark contrast between these

317
00:21:13,710 --> 00:21:15,000
 two ways.

318
00:21:15,000 --> 00:21:19,850
 One is always inclining, always reaching, and the other is

319
00:21:19,850 --> 00:21:23,000
 never inclining and never reaching,

320
00:21:23,000 --> 00:21:27,070
 never pushing and pulling. The Buddha said, "Never coming

321
00:21:27,070 --> 00:21:35,000
 or going, for there is no coming and no going."

322
00:21:35,000 --> 00:21:43,000
 And so the issue of impermanence, of instability is solved.

323
00:21:43,000 --> 00:21:50,070
 The second part of the verse talks about permanent, "acch

324
00:21:50,070 --> 00:21:53,000
uta," "chutas," falling away.

325
00:21:53,000 --> 00:21:57,000
 "Acchuta," never falling away.

326
00:21:57,000 --> 00:22:00,290
 You can never fall away from it because it's at the bottom

327
00:22:00,290 --> 00:22:04,000
 already, it's at the base already.

328
00:22:04,000 --> 00:22:10,730
 If something desirable comes, no matter how desirable, if

329
00:22:10,730 --> 00:22:14,000
 something desirable goes,

330
00:22:14,000 --> 00:22:19,000
 if something undesirable comes, it makes no difference.

331
00:22:19,000 --> 00:22:25,070
 It is a way of being, a way of relating to reality that is

332
00:22:25,070 --> 00:22:28,000
 inherently, by its very nature,

333
00:22:28,000 --> 00:22:35,580
 without any cause for doubt or uncertainty, stable,

334
00:22:35,580 --> 00:22:44,000
 consistent, unshakable, invincible.

335
00:22:44,000 --> 00:22:48,630
 The question as to whether it or not, at such a state as

336
00:22:48,630 --> 00:22:51,000
 possible or really exists,

337
00:22:51,000 --> 00:22:55,710
 well, that's the question that Buddhism seeks to answer and

338
00:22:55,710 --> 00:22:57,000
 to demonstrate.

339
00:22:57,000 --> 00:22:59,000
 And that's where meditation comes in.

340
00:22:59,000 --> 00:23:03,380
 Our practice of mindfulness, the question that often arises

341
00:23:03,380 --> 00:23:05,000
 in people,

342
00:23:05,000 --> 00:23:09,650
 why are we repeating to ourselves something like pain, pain

343
00:23:09,650 --> 00:23:13,000
, or for angry, say, angry, angry,

344
00:23:13,000 --> 00:23:16,090
 if we want something wanting. Why do we say to ourselves, "

345
00:23:16,090 --> 00:23:18,000
See, see, why are we doing this?

346
00:23:18,000 --> 00:23:23,750
 Why are we reminding ourselves about these mundane

347
00:23:23,750 --> 00:23:26,000
 realities?"

348
00:23:26,000 --> 00:23:29,930
 And this is the answer. We're trying to cultivate a state

349
00:23:29,930 --> 00:23:32,000
 of objectivity

350
00:23:32,000 --> 00:23:36,130
 where we relate to things just as they are, because our

351
00:23:36,130 --> 00:23:37,000
 actions,

352
00:23:37,000 --> 00:23:40,000
 just like the actions of anyone who gets angry and violent

353
00:23:40,000 --> 00:23:48,000
 or greedy and covetous and ambitious and so on,

354
00:23:48,000 --> 00:23:55,000
 our habits have power and influence. They change us.

355
00:23:55,000 --> 00:24:01,990
 And if we cultivate this state of objectivity where we

356
00:24:01,990 --> 00:24:04,000
 remember things,

357
00:24:04,000 --> 00:24:08,310
 where we remember, "Oh, yeah, I'm not going to forget that

358
00:24:08,310 --> 00:24:11,000
 that's all, that's just pain."

359
00:24:11,000 --> 00:24:14,340
 Because if you forget that that's just pain, you will get

360
00:24:14,340 --> 00:24:16,000
 upset about it.

361
00:24:16,000 --> 00:24:19,100
 When you see delicious food, for example, I'm not going to

362
00:24:19,100 --> 00:24:21,000
 forget that that's seeing.

363
00:24:21,000 --> 00:24:23,470
 Because if I forget, I will get caught up in all these

364
00:24:23,470 --> 00:24:24,000
 concepts,

365
00:24:24,000 --> 00:24:29,120
 "Oh, I know what that tastes like, and that's something I

366
00:24:29,120 --> 00:24:30,000
 like."

367
00:24:30,000 --> 00:24:33,540
 When you're angry, I'm not going to forget that that's

368
00:24:33,540 --> 00:24:34,000
 anger.

369
00:24:34,000 --> 00:24:40,170
 Because if I forget, if I lose track of the experience, I

370
00:24:40,170 --> 00:24:45,000
 will get caught up by the anger, by the greed.

371
00:24:45,000 --> 00:24:49,630
 I will become violent. I will become covetous. I will be

372
00:24:49,630 --> 00:24:51,000
 manipulative.

373
00:24:51,000 --> 00:24:54,850
 I will do all sorts of things to get what I want and to

374
00:24:54,850 --> 00:25:00,000
 remove what I don't want and so on.

375
00:25:00,000 --> 00:25:05,000
 This is what it means to be restrained, "sambhuta."

376
00:25:05,000 --> 00:25:09,110
 It doesn't mean you have to force yourself in any way,

377
00:25:09,110 --> 00:25:10,000
 really.

378
00:25:10,000 --> 00:25:15,000
 It means you have to keep the mind with the experience,

379
00:25:15,000 --> 00:25:19,000
 keep the mind in the reality of the experience,

380
00:25:19,000 --> 00:25:27,530
 not getting caught up in our reactions and our perceptions

381
00:25:27,530 --> 00:25:31,000
 of things.

382
00:25:31,000 --> 00:25:37,130
 So when the Buddha talks about this "acchutatana," "acchut

383
00:25:37,130 --> 00:25:41,000
atanam yatagandhva nasocchare,"

384
00:25:41,000 --> 00:25:48,720
 where sorrow is not, he's talking about, at its base, this

385
00:25:48,720 --> 00:25:54,000
 state, this way of life, way of being,

386
00:25:54,000 --> 00:26:00,000
 that one comes to, through training and through practice,

387
00:26:00,000 --> 00:26:04,000
 of non-reactivity, of non-judgment,

388
00:26:04,000 --> 00:26:10,000
 of simple, true and pure experience of reality as it is,

389
00:26:10,000 --> 00:26:13,000
 experience of this, of what we are all experiencing,

390
00:26:13,000 --> 00:26:20,180
 but experiencing it purely and truly and without judgment

391
00:26:20,180 --> 00:26:22,000
 or reactivity.

392
00:26:22,000 --> 00:26:27,000
 Because that is the way of life, the way of practice.

393
00:26:27,000 --> 00:26:37,610
 That is the way that carries on to eternity, that has no

394
00:26:37,610 --> 00:26:40,000
 falling back.

395
00:26:40,000 --> 00:26:48,000
 There is no tension in it. There is no opposition.

396
00:26:48,000 --> 00:26:53,570
 When you strive and strive and strive to get something, you

397
00:26:53,570 --> 00:26:56,000
 build up and build up this attachment to it.

398
00:26:56,000 --> 00:27:02,810
 When you live your life in objectivity, in clear awareness

399
00:27:02,810 --> 00:27:05,000
 of things as they are,

400
00:27:05,000 --> 00:27:10,000
 there is no build-up like that. There is a shedding off,

401
00:27:10,000 --> 00:27:11,000
 there is a freeing.

402
00:27:11,000 --> 00:27:15,340
 The result, rather than a building up and a building up of

403
00:27:15,340 --> 00:27:19,000
 some formation that is going to topple down,

404
00:27:19,000 --> 00:27:25,440
 there is the deconstruction and there is a reduction in

405
00:27:25,440 --> 00:27:28,000
 needs and attachment.

406
00:27:28,000 --> 00:27:35,980
 The ultimate destination, the ultimate result is freedom

407
00:27:35,980 --> 00:27:38,000
 from suffering.

408
00:27:38,000 --> 00:27:41,900
 In a way, very different from any conception of freedom

409
00:27:41,900 --> 00:27:47,000
 from suffering that we might have in a worldly sense,

410
00:27:47,000 --> 00:27:50,570
 because it is independent. It has nothing to do with some

411
00:27:50,570 --> 00:27:54,000
 sorrow or our experiences of this or that.

412
00:27:54,000 --> 00:28:02,000
 It is free from any of the uncertainty and the impermanence

413
00:28:02,000 --> 00:28:09,000
, the unpredictability of samsara of the world.

414
00:28:09,000 --> 00:28:19,120
 There is no fear of sorrow from one who has gone to this

415
00:28:19,120 --> 00:28:21,000
 place.

416
00:28:21,000 --> 00:28:24,520
 That is the Dhammapandha for today. Thank you all for

417
00:28:24,520 --> 00:28:25,000
 listening.

