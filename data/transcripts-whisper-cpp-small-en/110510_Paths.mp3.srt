1
00:00:00,000 --> 00:00:05,080
 Hello and welcome back to Ask a Monk.

2
00:00:05,080 --> 00:00:12,150
 Today I'll be starting again my series on Buddhism,

3
00:00:12,150 --> 00:00:15,440
 Meditation in the Master of Life,

4
00:00:15,440 --> 00:00:20,960
 answering questions that people have over YouTube and the

5
00:00:20,960 --> 00:00:23,520
 internet in general.

6
00:00:23,520 --> 00:00:32,960
 So today the question comes in regards to choosing a path.

7
00:00:32,960 --> 00:00:41,310
 The questioner asked about whether one can pick and choose

8
00:00:41,310 --> 00:00:43,980
 from the various religious

9
00:00:43,980 --> 00:00:48,520
 traditions based on one's interest.

10
00:00:48,520 --> 00:00:53,330
 One is interested in this tradition, that tradition, and

11
00:00:53,330 --> 00:00:55,920
 many different traditions.

12
00:00:55,920 --> 00:01:02,510
 Is it possible or what is the result, what sort of result

13
00:01:02,510 --> 00:01:05,920
 can one expect from picking

14
00:01:05,920 --> 00:01:10,770
 and choosing the things that one finds useful from various

15
00:01:10,770 --> 00:01:13,200
 religious traditions?

16
00:01:13,200 --> 00:01:19,440
 That really is the point in regards to results because the

17
00:01:19,440 --> 00:01:23,580
 meaning of the word path is something

18
00:01:23,580 --> 00:01:27,320
 that leads to a goal.

19
00:01:27,320 --> 00:01:33,700
 I think it's important to be clear that we have a goal in

20
00:01:33,700 --> 00:01:34,800
 mind.

21
00:01:34,800 --> 00:01:40,970
 Now if you have a specific goal in mind, then there's

22
00:01:40,970 --> 00:01:44,120
 really no problem in picking and choosing

23
00:01:44,120 --> 00:01:48,030
 if you think this teaching or that teaching is going to

24
00:01:48,030 --> 00:01:50,200
 help you towards that goal.

25
00:01:50,200 --> 00:01:54,340
 So it's really going to depend on what sort of a goal you

26
00:01:54,340 --> 00:01:55,080
 have.

27
00:01:55,080 --> 00:02:02,600
 Now the problem comes where there are many people who don't

28
00:02:02,600 --> 00:02:05,920
 have a goal and they might

29
00:02:05,920 --> 00:02:08,750
 pick and choose simply based on their interest, simply

30
00:02:08,750 --> 00:02:10,600
 based on what appeals to them at the

31
00:02:10,600 --> 00:02:19,360
 time and as a result have no particular goal in mind.

32
00:02:19,360 --> 00:02:26,760
 They in fact might claim that there is no objective truth

33
00:02:26,760 --> 00:02:31,400
 or objective reality or objective

34
00:02:31,400 --> 00:02:34,920
 goal.

35
00:02:34,920 --> 00:02:43,090
 They might take as their belief or their view that reality

36
00:02:43,090 --> 00:02:46,880
 is what it is and it's eternal

37
00:02:46,880 --> 00:02:53,580
 and all we have to do is to keep going, do what comes to us

38
00:02:53,580 --> 00:02:56,440
 at the time and that's going

39
00:02:56,440 --> 00:03:03,490
 to lead us ever and ever again to a similar or different or

40
00:03:03,490 --> 00:03:06,960
 constantly changing state

41
00:03:06,960 --> 00:03:11,160
 of existence.

42
00:03:11,160 --> 00:03:16,140
 I think that whether it comes up as a view in one's mind, I

43
00:03:16,140 --> 00:03:20,360
 think that sort of idea is

44
00:03:20,360 --> 00:03:23,090
 prevalent among people who pick and choose from various

45
00:03:23,090 --> 00:03:24,480
 religious traditions.

46
00:03:24,480 --> 00:03:31,480
 They don't have a particular goal or path in mind.

47
00:03:31,480 --> 00:03:35,100
 They're just doing what comes to them at the time and so it

48
00:03:35,100 --> 00:03:37,040
 may be laziness, it may just

49
00:03:37,040 --> 00:03:45,000
 be not realizing the importance or not seeing any

50
00:03:45,000 --> 00:03:49,720
 importance in finding a path or changing

51
00:03:49,720 --> 00:03:52,400
 our state.

52
00:03:52,400 --> 00:03:57,400
 Some people think that our state as it is is fine.

53
00:03:57,400 --> 00:04:03,240
 The problem with this idea, the idea of this continuous

54
00:04:03,240 --> 00:04:07,400
 cycle or eternal existence continuing

55
00:04:07,400 --> 00:04:15,290
 on and just taking what comes to you at the time is that

56
00:04:15,290 --> 00:04:21,640
 then you put yourself in a position

57
00:04:21,640 --> 00:04:27,210
 to have to accept the suffering and the unhappiness that

58
00:04:27,210 --> 00:04:28,960
 exists in life.

59
00:04:28,960 --> 00:04:33,310
 Now by that I don't simply mean the suffering that we're

60
00:04:33,310 --> 00:04:36,360
 experiencing now because obviously

61
00:04:36,360 --> 00:04:39,200
 for most of us it's a mixed bag.

62
00:04:39,200 --> 00:04:42,920
 There's some happiness and some suffering but if your

63
00:04:42,920 --> 00:04:45,320
 understanding is that existence

64
00:04:45,320 --> 00:04:49,780
 and reality is eternal and we just continue on and on as it

65
00:04:49,780 --> 00:04:51,840
 is, then you have to look

66
00:04:51,840 --> 00:04:56,040
 around you and see the sorts of lives that are possible.

67
00:04:56,040 --> 00:04:58,780
 It's clear that it's possible to experience a great amount

68
00:04:58,780 --> 00:05:00,120
 of happiness but it's also

69
00:05:00,120 --> 00:05:04,000
 possible to experience a great amount of sorrow, a great

70
00:05:04,000 --> 00:05:05,640
 amount of suffering.

71
00:05:05,640 --> 00:05:14,760
 By great amount of suffering, I mean torture and worse.

72
00:05:14,760 --> 00:05:20,140
 If you think that there is some value in this experience,

73
00:05:20,140 --> 00:05:22,920
 it's important to take a very

74
00:05:22,920 --> 00:05:27,390
 close look and not just take this as a view because clearly

75
00:05:27,390 --> 00:05:29,440
 even in our one life we've

76
00:05:29,440 --> 00:05:33,280
 gone through a lot, most of us, just to grow up, just to

77
00:05:33,280 --> 00:05:35,600
 get to the point where we're more

78
00:05:35,600 --> 00:05:36,600
 or less stable.

79
00:05:36,600 --> 00:05:40,230
 If we look back at all the suffering and all the lessons we

80
00:05:40,230 --> 00:05:42,560
 had to learn and then ask yourself

81
00:05:42,560 --> 00:05:45,050
 whether you want to learn these lessons and then forget

82
00:05:45,050 --> 00:05:46,520
 them and then have to learn them

83
00:05:46,520 --> 00:05:54,250
 again and forget them on and on and on with no end in sight

84
00:05:54,250 --> 00:05:54,840
.

85
00:05:54,840 --> 00:05:59,590
 I think most of us, if we're honest with ourselves, we

86
00:05:59,590 --> 00:06:03,840
 would prefer a state of development and

87
00:06:03,840 --> 00:06:07,400
 I think in that sense it's fine to pick and choose the

88
00:06:07,400 --> 00:06:09,120
 things that you think are going

89
00:06:09,120 --> 00:06:13,130
 to help you as long as you have an idea of what is going to

90
00:06:13,130 --> 00:06:15,760
 be helpful for one's development.

91
00:06:15,760 --> 00:06:22,360
 Now the reasons why one might not simply pick a specific

92
00:06:22,360 --> 00:06:25,920
 path, it's clear I think why there

93
00:06:25,920 --> 00:06:30,080
 are many people who would rather not pick this religious

94
00:06:30,080 --> 00:06:32,720
 tradition, that religious tradition

95
00:06:32,720 --> 00:06:36,240
 and so on.

96
00:06:36,240 --> 00:06:41,360
 I think for some people it's not wanting to follow the

97
00:06:41,360 --> 00:06:45,640
 crowd, we have this idea of individuality

98
00:06:45,640 --> 00:06:49,360
 and we want to be special and we want to have our own way

99
00:06:49,360 --> 00:06:51,600
 and it's an ego trip of sorts.

100
00:06:51,600 --> 00:06:54,540
 But I think for other people it's quite discouraging to see

101
00:06:54,540 --> 00:06:56,680
 all of these different paths all claiming

102
00:06:56,680 --> 00:07:03,570
 to have the soul, the one and only way out of suffering,

103
00:07:03,570 --> 00:07:07,720
 the one and only way to salvation,

104
00:07:07,720 --> 00:07:14,400
 to freedom, to everlasting happiness, to development.

105
00:07:14,400 --> 00:07:21,590
 And so as a result we think better for me to take the good

106
00:07:21,590 --> 00:07:25,680
 and not take something simply

107
00:07:25,680 --> 00:07:28,230
 because it belongs to this religious tradition or that

108
00:07:28,230 --> 00:07:30,200
 religious tradition but take it because

109
00:07:30,200 --> 00:07:36,060
 it seems after some introspection and even some

110
00:07:36,060 --> 00:07:41,680
 experimentation that it seems to be helpful,

111
00:07:41,680 --> 00:07:43,680
 take those things that are helpful.

112
00:07:43,680 --> 00:07:47,480
 Now this has its positives and its negatives.

113
00:07:47,480 --> 00:07:53,830
 The negatives are first of all that your potential or

114
00:07:53,830 --> 00:07:58,840
 mistake is greater because all of the various

115
00:07:58,840 --> 00:08:02,900
 spiritual and religious traditions have been tried and

116
00:08:02,900 --> 00:08:05,120
 tested and they do all lead to a

117
00:08:05,120 --> 00:08:07,560
 fairly specific result.

118
00:08:07,560 --> 00:08:12,510
 So at least to an extent you'd be better off to look and

119
00:08:12,510 --> 00:08:15,360
 see where a specific tradition

120
00:08:15,360 --> 00:08:18,770
 leads, a specific practice leads because it could be that

121
00:08:18,770 --> 00:08:20,280
 there are two traditions that

122
00:08:20,280 --> 00:08:25,450
 are both leading to the same place and there may be more

123
00:08:25,450 --> 00:08:28,640
 than one tradition, one path,

124
00:08:28,640 --> 00:08:34,040
 they may actually be the same path.

125
00:08:34,040 --> 00:08:37,370
 And in this sense it's important, I think one should never

126
00:08:37,370 --> 00:08:39,000
 look at something in terms

127
00:08:39,000 --> 00:08:41,570
 of a broad label like this is Christianity and therefore it

128
00:08:41,570 --> 00:08:42,760
's going to lead me in this

129
00:08:42,760 --> 00:08:45,240
 direction, this is Buddhism and therefore it's going to

130
00:08:45,240 --> 00:08:46,560
 lead me in this direction and

131
00:08:46,560 --> 00:08:48,880
 so on and so on.

132
00:08:48,880 --> 00:08:52,030
 It's important to look at the various teachers and the

133
00:08:52,030 --> 00:08:54,200
 various traditions because even in

134
00:08:54,200 --> 00:08:58,370
 Buddhism for example there are many different ways and

135
00:08:58,370 --> 00:09:01,080
 ostensibly they lead in different

136
00:09:01,080 --> 00:09:04,400
 directions to an extent.

137
00:09:04,400 --> 00:09:11,520
 So I think rather than picking and choosing at random or

138
00:09:11,520 --> 00:09:15,680
 based on one's own limited knowledge,

139
00:09:15,680 --> 00:09:19,240
 not having practiced any of these paths, one may be better

140
00:09:19,240 --> 00:09:21,120
 off to look at the sort of people

141
00:09:21,120 --> 00:09:27,390
 who practice a specific path and try out that path for a

142
00:09:27,390 --> 00:09:28,520
 while.

143
00:09:28,520 --> 00:09:34,230
 To not worry about collecting the things from various paths

144
00:09:34,230 --> 00:09:36,800
 but to look at the paths and

145
00:09:36,800 --> 00:09:42,360
 see where they lead and to develop a specific path.

146
00:09:42,360 --> 00:09:46,130
 I would prefer that over picking and choosing and hoping to

147
00:09:46,130 --> 00:09:48,080
 cobble together some sort of

148
00:09:48,080 --> 00:09:51,930
 path that's going to lead you somewhere because the problem

149
00:09:51,930 --> 00:09:54,320
 is as I said you've got no experience

150
00:09:54,320 --> 00:09:55,320
 with it.

151
00:09:55,320 --> 00:10:00,040
 You've got to cobble together this vehicle and you start

152
00:10:00,040 --> 00:10:02,440
 going with it and after some

153
00:10:02,440 --> 00:10:05,910
 time you might realize that it's not taking you, it's not

154
00:10:05,910 --> 00:10:07,760
 well made and it's not taking

155
00:10:07,760 --> 00:10:09,280
 you in the direction that you need to go.

156
00:10:09,280 --> 00:10:12,740
 But if you have this vehicle you see other people taking

157
00:10:12,740 --> 00:10:14,620
 this vehicle and the results

158
00:10:14,620 --> 00:10:17,120
 that they get are clear.

159
00:10:17,120 --> 00:10:31,630
 Then you'd be better off to in some sense to follow after

160
00:10:31,630 --> 00:10:32,960
 them.

161
00:10:32,960 --> 00:10:35,200
 And so you can do either.

162
00:10:35,200 --> 00:10:38,290
 You can choose to cobble together your own vehicle to make

163
00:10:38,290 --> 00:10:40,400
 up your own path and basically

164
00:10:40,400 --> 00:10:43,260
 starting a new religion or a new religious tradition saying

165
00:10:43,260 --> 00:10:44,440
 that this is my path and

166
00:10:44,440 --> 00:10:49,200
 it's leading there.

167
00:10:49,200 --> 00:10:53,240
 And hope for the best and probably the odds are against you

168
00:10:53,240 --> 00:10:55,360
 unless you're a fairly special

169
00:10:55,360 --> 00:10:59,840
 person or start looking at religious traditions.

170
00:10:59,840 --> 00:11:02,680
 I mean what we're talking about here is vehicles, vehicles

171
00:11:02,680 --> 00:11:05,280
 that lead us in a certain direction

172
00:11:05,280 --> 00:11:11,760
 and they don't all lead in the same direction.

173
00:11:11,760 --> 00:11:20,790
 So if you pick one and take it where it's going to lead you

174
00:11:20,790 --> 00:11:25,200
 or use it for that specific

175
00:11:25,200 --> 00:11:29,000
 result then why do you need to take something from another

176
00:11:29,000 --> 00:11:29,800
 vehicle?

177
00:11:29,800 --> 00:11:34,540
 I mean not to say that these things are not good but there

178
00:11:34,540 --> 00:11:37,000
 are reasons why people have

179
00:11:37,000 --> 00:11:42,430
 come to a specific set of practices, a specific set of

180
00:11:42,430 --> 00:11:46,000
 views, beliefs and practices.

181
00:11:46,000 --> 00:11:51,640
 So I would say it's expedient to pick a specific practice

182
00:11:51,640 --> 00:11:54,840
 because it saves you a lot of the

183
00:11:54,840 --> 00:11:56,920
 guesswork, a lot of the experimentation.

184
00:11:56,920 --> 00:12:00,880
 But if you're really fed up and you don't see that any one

185
00:12:00,880 --> 00:12:02,680
 vehicle, any one path is

186
00:12:02,680 --> 00:12:06,240
 leading you in the direction that you're looking to go then

187
00:12:06,240 --> 00:12:08,000
 by all means I think you'll have

188
00:12:08,000 --> 00:12:13,800
 to pick and choose and start your own way.

189
00:12:13,800 --> 00:12:18,970
 And the only thing is that I would caution against this

190
00:12:18,970 --> 00:12:21,840
 lack of direction where people

191
00:12:21,840 --> 00:12:24,770
 put these things together sort of as an ego trip or kind of

192
00:12:24,770 --> 00:12:26,360
 thinking that this is my path

193
00:12:26,360 --> 00:12:29,330
 and then everyone looks at you and you can feel good about

194
00:12:29,330 --> 00:12:32,440
 yourself because you've got

195
00:12:32,440 --> 00:12:34,440
 your way and so on.

196
00:12:34,440 --> 00:12:39,550
 I think there's a lot of people like this who in the end

197
00:12:39,550 --> 00:12:42,320
 wind up going nowhere or not

198
00:12:42,320 --> 00:12:46,200
 having any clear direction or any clear goal in mind.

199
00:12:46,200 --> 00:12:48,480
 So I hope that helps.

200
00:12:48,480 --> 00:12:54,120
 I would recommend that you at least try the path that the

201
00:12:54,120 --> 00:12:57,160
 Buddha laid down and that has

202
00:12:57,160 --> 00:13:00,240
 been followed by millions of people up into the present day

203
00:13:00,240 --> 00:13:01,720
 and to see where that leads

204
00:13:01,720 --> 00:13:07,260
 you because as far as I can see it's a path that does lead

205
00:13:07,260 --> 00:13:09,960
 where it says it leads and

206
00:13:09,960 --> 00:13:13,640
 can clearly be seen to lead where it says it leads as

207
00:13:13,640 --> 00:13:16,120
 opposed to maybe as another path

208
00:13:16,120 --> 00:13:21,160
 that says it leads somewhere but requires you to take it on

209
00:13:21,160 --> 00:13:23,440
 faith that it does so that

210
00:13:23,440 --> 00:13:28,470
 actually the Buddha's teaching does lead to development

211
00:13:28,470 --> 00:13:31,200
 that one can clearly verify.

212
00:13:31,200 --> 00:13:37,520
 And lead to ultimate freedom from suffering and happiness.

213
00:13:37,520 --> 00:13:38,840
 So hope that helps.

214
00:13:38,840 --> 00:13:39,840
 Thanks for the question.

215
00:13:39,840 --> 00:13:40,840
 All the best.

216
00:13:40,840 --> 00:13:59,120
 [

