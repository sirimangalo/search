 So when I read this passage, I couldn't understand it.
 And so I looked for an explanation in the commentary, but
 the commentary didn't say
 anything about this sentence.
 I was dissatisfied with the commentary because we always
 expect the commentary to explain
 such points and at this place it's silent.
 And what was sub-commentary saying?
 So it gives a lot of dukkha.
 And so I wanted to give dukkha to you too.
 So you have to think about it until next class.
 Okay, next question.
 Do plants, trees, rocks and animals have karma?
 Maybe due to many of you it is a silly question, but there
 may be some people, hopefully not
 among you, who may think that there is karma to plants,
 trees and so on.
 So this question is put here.
 So according to Thiravada Buddhism, plants, trees and rocks
 are not living beings.
 They are not regarded as living beings, although in some
 way plants may be said to have life.
 But that is not life taught in Abhidhamma.
 So plants, trees and rocks are not living beings and as
 such they have no karma.
 Although they have no karma, they are nevertheless
 dependent on other material conditions like
 moisture, temperature and nutrition and so on.
 So although they have no karma, they depend on conditions
 to arise and to exist.
 So they are also dependent on some conditions, although
 they have no karma.
 So the causal relationship between inanimate things will be
 explained with reference to
 patthana.
 Now animals are living beings.
 We have all five aggregates as we human beings do and so
 they are living beings.
 As living beings, they have karma.
 Animals acquire karma or accumulate karma and animals are
 born as animals as a result
 of Akusela karma in the past.
 So animals have karma and they do or they perform karma.
 But plants, trees, rocks and other inanimate things have no
 karma.
 Now there are some statements in Risuddhi Maga regarding
 karma and results and they are
 noteworthy.
 So we should note them.
 Now while karma and results thus causally maintain their
 round, as seed and tree succeed
 in turn, no first beginning can be shown.
 Now karma and results follow each other and there is karma
 and then there is the result
 of karma.
 Now see there is kusela or Akusela karma and as a result of
 kusela and Akusela karma there
 is rebirth as say in four woeful states or in human beings
 or in celestial beings.
 And during that existence beings acquire new karma and then
 there is the result of that
 karma in the future.
 And so karma and its results go on and on and on like that,
 like a seed and the tree.
 So there is a seed and then it grows into a tree and then
 the tree gives seed and then
 seed in the tree and so on.
 Or we can say the hand and the egg, right?
 Go on and on and on.
 So there is no first beginning.
 No first beginning can be shown.
 We can go back but however far we go back we will never
 reach the beginning.
 So there is no first beginning of karma and results.
 So karma and results, that is the rolling on of karma and
 results in this round of rebirths.
 And not in the future round of births can they be shown not
 to occur.
 And in the future they will surely arise because when there
 is karma at present then there
 will be result of karma in the future.
 Now in Sankhya philosophy which is one of the six classical
 Hindu philosophies, it
 is believed that the result already exists in the cause but
 in an unmanifest form.
 So when it becomes manifest it becomes the result.
 So according to that philosophy the result is already in
 the cause.
 So the cause becomes mature and when it reaches the highest
 stage of maturity it becomes the
 result.
 So in that philosophy karma or cause and result are same.
 But Buddhism does not accept that.
 In Buddhism it is thought that karma is one thing and
 result is another.
 So here Vishuddhi Maga said, "There is no karma in result
 nor does result exist in karma."
 So there is no karma in result and no result in karma.
 They are just two different things.
 Though they are white of one another, though they are
 different from one another, there
 is no fruit without the karma.
 But there can be no result without the karma.
 So they are related as cause and effect.
 But cause is one thing and effect is another.
 They are not identical.
 As fire does not exist inside the sun, it can't go down,
 nor yet outside them.
 But it's brought to be by means of components or by means
 of conditions.
 Now when there is sunshine and there is cow dung or small
 pieces of wood or pieces of
 paper and you put a magnifying glass between them and
 concentrate the rays on the pieces
 of paper or cow dung, the fire is produced.
 So here it says, "Fire does not exist inside the sun,
 inside the gem or inside cow dung."
 Because if there is, if fire exists in the sun, I mean the
 rays of the sun or the gem
 or cow dung, they will be burning.
 They will always be burning.
 But fire is not in them.
 And so they are not burning when they are put separately.
 But when they are put together, then there is fire.
 So fire does not exist in the sun or in the gem or in the
 cow dung.
 Then where does it exist?
 Outside there?
 We do not say, we cannot say that fire exists somewhere
 apart from these three things.
 But fire is brought to be, fire is produced by means of the
 components, by means of these
 conditions.
 The conditions here means the sun, a gem or a magnifying
 glass and cow dung.
 So when these three are put together, then there is fire.
 But fire does not exist in the sun, gem and cow dung.
 And without these three things, fire cannot be produced.
 So in the same way, there is no result in the comma and no
 comma in the result.
 Results be found within the comma nor without.
 Not that the comma still persists.
 In the result, it is produced.
 So there is no result found in the comma or outside the
 comma or without the comma.
 And the comma still persists, nor does the comma still
 persist in the result.
 So comma does not persist in the result.
 That means comma is not in the result, it has produced.
 The comma of its fruit is white.
 So comma is white of its fruit.
 Comma is white of the result.
 No fruit exists yet in comma and there is no fruit in the
 comma.
 And still the fruit is born from it.
 The result is born from comma, wholly depending on the
 comma.
 So wholly depending on the, totally depending on the comma,
 the result is produced.
 For here there is no God or Brahma, creator of the round of
 rebirth.
 So this round of rebirth is not the creation of a God or a
 Brahma, but the unnatural outcome
 of the comma.
 So comma and its results, the phenomena alone flow on.
 So they roll on and on and on, cause and component, their
 condition.
 So their condition is by their cause and their condition by
 their condition, by the conditions.
 So dependent upon the cause and conditions, these mental
 and physical phenomena flow on
 and on and on.
 And in this flow on of mental and physical phenomena, which
 is called the round of rebirth,
 there is no God or no Brahman who creates this round of
 rebirth.
 So just mental and physical phenomena dependent upon the
 conditions flowing on and on.
 Some benefits of understanding the law of comma.
 So we, as Buddhists we understand the law of comma
 following the teachings of the Buddha.
 And this understanding of law of comma has many benefits
 and here are some of them.
 So it satisfies our curiosity about inequality among beings
.
 So we want to know why beings are different, why beings are
 not equal, although they are
 equal as beings, as human beings or animals, as animals.
 And this inequality among beings is satisfactorily
 explained by the law of comma.
 The understanding of law of comma satisfies our curiosity
 about inequality among beings.
 It is no wonder that understanding of law of comma has
 converted many people to Buddhism.
 And understanding of law of comma gives us hope.
 There is always hope that we can be better.
 We are not to be satisfied with what condition we are in,
 but we can always improve our condition
 by doing good comma.
 So it gives us the understanding of the law of comma gives
 us hope.
 So we are never hopeless.
 And the understanding of law of comma can give us
 consolation in times of misfortune.
 Now people meet with misfortune many times in their lives,
 loss of wealth, loss of business,
 loss of job, loss of friends, loss of loved ones.
 And when they meet with such misfortunes, those who do not
 understand the law of comma
 cannot be consoled.
 They might even become very dejected and they might even
 take their own lives.
 But those who understand the law of comma can understand
 the misfortune because this
 misfortune comes as a result of what they did in the past.
 And so there is no other person to blame but themselves.
 And so people can be consoled with the understanding of law
 of comma.
 And it gives us strength to meet the ups and downs of life
 without agitation.
 Now when we live in life, there are ups and downs in our
 lives.
 We cannot avoid meeting success, meeting failure in our
 lives.
 So whether we meet with success or failure, we can stand
 firm.
 We can meet these situations without much agitation.
 And it is important that we be without agitation when we
 meet with success or failure.
 Because now when people meet with success, they are elated
 and when they meet with failure,
 they are dejected.
 And so their lives become miserable.
 But if we understand the law of comma, we can take them as
 they come without much agitation.
 It teaches us to realize our potential to create our own
 future.
 Now we get the results of our own comma.
 So we can create our own future.
 We have the potential to create our own future if we
 understand the law of comma.
 And not only realizing the potential, but we can use this
 potential to make our future
 lives better than the present ones.
 So it teaches us that we beings have this potential to
 create our own future.
 And we need not look to other external powers or deities
 for our own future.
 Actually we alone can create our future.
 And the understanding of law of comma teaches individual
 responsibility.
 Since the results we experience are caused by our own comma
, we alone are responsible
 for our enjoyment or our suffering.
 So since we are responsible for our own happiness or
 suffering, we are not to blame others for
 our conditions.
 So it teaches that we are responsible for ourselves.
 And if we want to be better in the future, we must do or we
 must acquire good comma.
 So we become responsible persons if we understand the law
 of comma.
 And it teaches us to avoid harming other beings and to be
 compassionate to other beings.
 Now if we harm other beings, we will suffer the painful
 consequences of our comma.
 So if we do not want to suffer ourselves, then we do not
 inflict suffering on others.
 And so it helps us to avoid harming or injuring the other
 beings.
 And it teaches us to be compassionate to other beings.
 Because by being compassionate to other beings, we develop
 good comma which will give good
 results in the future.
 So understanding the law of comma can help us to make the
 society more humane and more
 peaceful and more harmonious.
 There may be some more benefits of understanding the law of
 comma.
 So I want you to make additions to this list yourselves.
 So we have now studied comma in general, what comma is and
 what we can do about comma and
 so on.
 Next time we study the different kinds of comma.
 You asked me on Sunday.
 [ Laughter ]
