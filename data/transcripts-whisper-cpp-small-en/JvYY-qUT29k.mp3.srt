1
00:00:00,000 --> 00:00:02,000
 Okay, starting.

2
00:00:02,000 --> 00:00:05,950
 "I feel confused. Is it okay not to feel the body in

3
00:00:05,950 --> 00:00:07,000
 meditation?

4
00:00:07,000 --> 00:00:10,000
 For example, often I can't tell where my eyes are looking.

5
00:00:10,000 --> 00:00:14,270
 Later bodily feelings seem to fade. I only register the

6
00:00:14,270 --> 00:00:17,000
 breath. Is that okay?"

7
00:00:17,000 --> 00:00:23,000
 Yeah, in fact, I'm probably just going to answer this.

8
00:00:23,000 --> 00:00:26,000
 I don't know if anyone else has anything to add,

9
00:00:26,000 --> 00:00:30,180
 it's actually considered to be one of the stages of,

10
00:00:30,180 --> 00:00:32,000
 potentially one of the stages of knowledge.

11
00:00:32,000 --> 00:00:35,000
 The point being that the body doesn't exist.

12
00:00:35,000 --> 00:00:37,000
 The body was never there in the first place.

13
00:00:37,000 --> 00:00:39,770
 And there comes a point in your practice where you realize

14
00:00:39,770 --> 00:00:40,000
 that,

15
00:00:40,000 --> 00:00:44,000
 and as a result you say, "Oh, my body disappeared."

16
00:00:44,000 --> 00:00:46,300
 Well, the body wasn't there in the first place. I mean,

17
00:00:46,300 --> 00:00:47,000
 think about it.

18
00:00:47,000 --> 00:00:50,000
 Without experiencing it, where does the body,

19
00:00:50,000 --> 00:00:52,990
 where does the idea of the body exist at all? It exists in

20
00:00:52,990 --> 00:00:54,000
 the mind.

21
00:00:54,000 --> 00:00:59,000
 So our continued awareness of the body is totally mental.

22
00:00:59,000 --> 00:01:02,000
 It comes from an experience here, an experience there,

23
00:01:02,000 --> 00:01:06,000
 that we put together in your mind and say, "Oh, my body."

24
00:01:06,000 --> 00:01:10,160
 But really, when you close your eyes, why should your body

25
00:01:10,160 --> 00:01:12,000
 be there at all?

26
00:01:12,000 --> 00:01:14,000
 You don't see it anymore.

27
00:01:14,000 --> 00:01:16,380
 And if you're sitting quite still, you don't feel it

28
00:01:16,380 --> 00:01:17,000
 anymore.

29
00:01:17,000 --> 00:01:20,000
 It only exists as a concept in the mind.

30
00:01:20,000 --> 00:01:22,000
 And there comes a point in your meditation

31
00:01:22,000 --> 00:01:26,000
 where you break through that and you start to

32
00:01:26,000 --> 00:01:30,000
 experience or observe only ultimate reality,

33
00:01:30,000 --> 00:01:35,000
 at which point body has no place and place no part.

34
00:01:35,000 --> 00:01:38,090
 So potentially what you're describing is actually a very

35
00:01:38,090 --> 00:01:39,000
 good thing.

36
00:01:39,000 --> 00:01:43,000
 It's a realization of the nature of reality,

37
00:01:43,000 --> 00:01:48,000
 giving up the observation of concepts, like body.

38
00:01:48,000 --> 00:01:54,990
 Yeah, I figured it would be kind of silent after that.

39
00:01:54,990 --> 00:01:57,000
 Anyone?

