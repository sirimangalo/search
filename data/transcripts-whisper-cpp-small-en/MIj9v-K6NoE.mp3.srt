1
00:00:00,000 --> 00:00:07,000
 This is our new DVD on how to meditate.

2
00:00:07,000 --> 00:00:14,000
 So you can choose to look at all the lessons as one,

3
00:00:14,000 --> 00:00:20,000
 or you can pick one of the lessons to look at individually.

4
00:00:20,000 --> 00:00:27,000
 Or you can look at the about.

5
00:00:28,000 --> 00:00:28,000
 [

6
00:00:28,000 --> 00:00:28,000
 [

7
00:00:28,000 --> 00:00:29,000
 [

8
00:00:29,000 --> 00:00:30,000
 [

9
00:00:30,000 --> 00:00:31,000
 [

10
00:00:31,000 --> 00:00:32,000
 [

11
00:00:32,000 --> 00:00:33,000
 ]]

12
00:00:33,000 --> 00:00:39,500
 Hi, and welcome to this series of videos on how to meditate

13
00:00:39,500 --> 00:00:40,000
.

14
00:00:40,000 --> 00:00:43,220
 In this the first video, I'll be talking about what is the

15
00:00:43,220 --> 00:00:44,000
 meaning.

16
00:00:44,000 --> 00:00:49,000
 In the next video, I'll be explaining how it is that we

17
00:00:49,000 --> 00:00:53,000
 fall, rising, falling, ourselves.

18
00:00:53,000 --> 00:00:55,890
 When we do the walking meditation, especially because it is

19
00:00:55,890 --> 00:00:57,000
 slow and repetitious,

20
00:00:57,000 --> 00:00:59,000
 the body is given a chance.

21
00:00:59,000 --> 00:01:02,000
 During the time that we are doing the walking meditation,

22
00:01:02,000 --> 00:01:02,000
 we are the same.

23
00:01:02,000 --> 00:01:05,360
 We should try to keep our mind bound to an individual or an

24
00:01:05,360 --> 00:01:07,000
 entity of any sort.

25
00:01:07,000 --> 00:01:11,360
 It is a way of paying respect to the practice which we are

26
00:01:11,360 --> 00:01:12,000
 about to undergo.

27
00:01:12,000 --> 00:01:14,000
 Thank you.

28
00:01:14,000 --> 00:01:21,630
 Hi, in this video, I'll be talking about some of the ways

29
00:01:21,630 --> 00:01:22,000
 in which we would come to act.

30
00:01:22,000 --> 00:01:26,750
 Because they are still, say to ourselves, we can say, "I'll

31
00:01:26,750 --> 00:01:28,000
 see all of those this way."

32
00:01:28,000 --> 00:01:31,400
 I would like to thank you all for watching, taking the time

33
00:01:31,400 --> 00:01:33,000
 to watch these videos.

34
00:01:33,000 --> 00:01:37,680
 I really, truly, sincerely hope that these videos will come

35
00:01:37,680 --> 00:01:38,000
.

36
00:01:40,000 --> 00:01:42,000
 Thank you.

37
00:01:42,000 --> 00:02:02,000
 (Music)

38
00:02:02,000 --> 00:02:09,000
 He said that we should always strive to...

39
00:02:09,000 --> 00:02:12,390
 ...behind the ordinary, everyday life, to leave behind

40
00:02:12,390 --> 00:02:14,000
 pretty much everything that makes us...

41
00:02:14,000 --> 00:02:17,000
 ...its a monk, I think.

42
00:02:17,000 --> 00:02:19,840
 And I mean, that's what's important, most of the things

43
00:02:19,840 --> 00:02:21,000
 that get a state of being,

44
00:02:21,000 --> 00:02:24,000
 which is in the end, really...

45
00:02:24,000 --> 00:02:27,000
 ...and they're just about nothing.

46
00:02:27,000 --> 00:02:29,000
 It's just being.

47
00:02:29,000 --> 00:02:30,000
 That's...

48
00:02:30,000 --> 00:02:33,000
 For me, that's what it means for people.

49
00:02:33,000 --> 00:02:38,000
 (Music)

