1
00:00:00,000 --> 00:00:03,800
 Since I teach in an inner-city school, I create karma with

2
00:00:03,800 --> 00:00:04,600
 students that have

3
00:00:04,600 --> 00:00:07,840
 social emotional problems. It seems like some students

4
00:00:07,840 --> 00:00:09,040
 negative karma are so

5
00:00:09,040 --> 00:00:11,920
 severe that they are self-destructive. Is it better to

6
00:00:11,920 --> 00:00:13,560
 create karma with wealthy,

7
00:00:13,560 --> 00:00:16,710
 highly intelligent and nice people so that in my next life

8
00:00:16,710 --> 00:00:17,600
 I will be better

9
00:00:17,600 --> 00:00:20,500
 associated with people that have these characteristics

10
00:00:20,500 --> 00:00:21,640
 rather than with people

11
00:00:21,640 --> 00:00:28,600
 that have few virtues to have a better rebirth or better

12
00:00:28,600 --> 00:00:37,920
 karma? I think it's

13
00:00:37,920 --> 00:00:41,700
 kind of a loaded question because I did answer something

14
00:00:41,700 --> 00:00:42,480
 along these lines

15
00:00:42,480 --> 00:00:51,770
 before about how it's better to help people who are of

16
00:00:51,770 --> 00:00:55,720
 sound mind. I assume

17
00:00:55,720 --> 00:00:59,450
 that there's at least a part of this is an interest in that

18
00:00:59,450 --> 00:01:01,800
 topic. The

19
00:01:01,800 --> 00:01:12,680
 point that I hope was made clear in that speech was that it

20
00:01:12,680 --> 00:01:13,200
 has nothing to

21
00:01:13,200 --> 00:01:18,620
 do with your own benefit. It has to do with the benefit

22
00:01:18,620 --> 00:01:19,800
 that you're going to

23
00:01:19,800 --> 00:01:29,240
 bring to others because you can bring benefit to people who

24
00:01:29,240 --> 00:01:31,320
 have difficult

25
00:01:31,320 --> 00:01:37,340
 minds. As I said, I taught in the jail. I taught meditation

26
00:01:37,340 --> 00:01:40,120
 in the jail.

27
00:01:40,120 --> 00:01:44,780
 Teaching in the jail gave me a lot of the same feeling as

28
00:01:44,780 --> 00:01:47,000
 teaching in second life

29
00:01:47,000 --> 00:01:51,320
 when I taught in this virtual reality world. In both places

30
00:01:51,320 --> 00:01:52,320
 I got the same

31
00:01:52,320 --> 00:01:55,820
 feeling that these people are not really serious about this

32
00:01:55,820 --> 00:01:57,000
. As I said, this one

33
00:01:57,000 --> 00:02:01,650
 prisoner, he asked me, he wanted to learn how to fly, how

34
00:02:01,650 --> 00:02:03,640
 to leave his body. He's

35
00:02:03,640 --> 00:02:08,330
 in jail. What could be better to be able to leave his body

36
00:02:08,330 --> 00:02:08,920
 and leave the

37
00:02:08,920 --> 00:02:16,060
 prison and go flying around the city? You can help those

38
00:02:16,060 --> 00:02:18,240
 people. Theoretically,

39
00:02:18,240 --> 00:02:22,960
 you can help anyone. Part of the answer, I think, is to not

40
00:02:22,960 --> 00:02:23,800
 be averse to

41
00:02:23,800 --> 00:02:27,040
 helping the people who you're confronted with. The point

42
00:02:27,040 --> 00:02:28,440
 was, if you're going

43
00:02:28,440 --> 00:02:32,960
 to dedicate your life to helping, to bringing benefit to

44
00:02:32,960 --> 00:02:34,240
 others, or if you're

45
00:02:34,240 --> 00:02:40,580
 going to dedicate a portion of your life to helping others,

46
00:02:40,580 --> 00:02:42,760
 you should try to

47
00:02:42,760 --> 00:02:47,790
 find a way so that your help will be most efficient. From

48
00:02:47,790 --> 00:02:48,400
 my point of view,

49
00:02:48,400 --> 00:02:51,460
 the most efficient thing to do is to teach people how to

50
00:02:51,460 --> 00:02:52,360
 deal with these

51
00:02:52,360 --> 00:02:55,820
 people, or it's more efficient to teach people who are

52
00:02:55,820 --> 00:02:57,760
 going to then be teachers.

53
00:02:57,760 --> 00:03:02,190
 It's more efficient for me to teach people who come here

54
00:03:02,190 --> 00:03:03,080
 how to become

55
00:03:03,080 --> 00:03:09,410
 leaders and spiritual guides than it would be for me to go

56
00:03:09,410 --> 00:03:10,520
 around as they

57
00:03:10,520 --> 00:03:16,850
 might do, teaching people who are not spiritual or dealing

58
00:03:16,850 --> 00:03:17,760
 with interacting

59
00:03:17,760 --> 00:03:23,920
 with people who are not, who are maybe at a lesser level of

60
00:03:23,920 --> 00:03:24,400
 interest.

61
00:03:24,400 --> 00:03:29,330
 It would be better to be that sort of person who's a

62
00:03:29,330 --> 00:03:30,000
 teacher and a

63
00:03:30,000 --> 00:03:33,900
 guide than to be someone who is further on down, who is

64
00:03:33,900 --> 00:03:35,320
 just helping people's

65
00:03:35,320 --> 00:03:38,800
 material benefit. For example, a person who is a spiritual

66
00:03:38,800 --> 00:03:40,920
 guide for

67
00:03:40,920 --> 00:03:47,300
 teaching people how to be good lawyers or good doctors or

68
00:03:47,300 --> 00:03:48,040
 good secular or school

69
00:03:48,040 --> 00:03:52,370
 teachers. Better to be that person than to be the person

70
00:03:52,370 --> 00:03:53,000
 who is the school

71
00:03:53,000 --> 00:03:57,410
 teacher who is teaching secular subjects, subjects that

72
00:03:57,410 --> 00:04:00,440
 have some benefit but have

73
00:04:00,440 --> 00:04:03,580
 far less benefit obviously than the spiritual side of being

74
00:04:03,580 --> 00:04:04,320
 able to teach

75
00:04:04,320 --> 00:04:07,850
 meditation. So I would answer in two ways. The first one is

76
00:04:07,850 --> 00:04:08,560
 that if you're with

77
00:04:08,560 --> 00:04:11,440
 these people, if this is your job and this is where you are

78
00:04:11,440 --> 00:04:12,360
 in life, then

79
00:04:12,360 --> 00:04:15,200
 certainly don't avoid them and don't think, "Oh boy, I wish

80
00:04:15,200 --> 00:04:16,080
 I was with people

81
00:04:16,080 --> 00:04:21,760
 who were rich and handsome or whatever, who had good karma

82
00:04:21,760 --> 00:04:22,720
 and

83
00:04:22,720 --> 00:04:29,560
 lots of who gave me a good rebirth or so on." But on the

84
00:04:29,560 --> 00:04:31,160
 other hand, you

85
00:04:31,160 --> 00:04:38,210
 sometimes have to question whether your life is the best

86
00:04:38,210 --> 00:04:40,040
 use, whether your

87
00:04:40,040 --> 00:04:43,230
 path at this time is the best use of your resources for

88
00:04:43,230 --> 00:04:44,360
 both yourself and

89
00:04:44,360 --> 00:04:48,260
 others. That's always a judgment call because eventually,

90
00:04:48,260 --> 00:04:48,680
 of course, the

91
00:04:48,680 --> 00:04:52,040
 answer is no, the best life is to go off and become a monk

92
00:04:52,040 --> 00:04:53,040
 and live in

93
00:04:53,040 --> 00:04:58,280
 a cave. So it's totally up to you how far you want to go

94
00:04:58,280 --> 00:05:00,760
 with that. There is the

95
00:05:00,760 --> 00:05:04,790
 story of this Sotapana who then became the wife of a hunter

96
00:05:04,790 --> 00:05:05,800
, so she lived as a

97
00:05:05,800 --> 00:05:12,280
 hunter's wife cleaning the guts off of his traps. So

98
00:05:12,280 --> 00:05:12,720
 certainly you

99
00:05:12,720 --> 00:05:16,660
 can live in any position and you shouldn't have to worry

100
00:05:16,660 --> 00:05:17,080
 about where you

101
00:05:17,080 --> 00:05:22,760
 find yourself. But the point of that was just in regards to

102
00:05:22,760 --> 00:05:24,780
 having the

103
00:05:24,780 --> 00:05:29,760
 most benefit. From my point of view, it comes from teaching

104
00:05:29,760 --> 00:05:30,520
 people how to teach

105
00:05:30,520 --> 00:05:36,640
 meditation really or at least teaching meditation. I have

106
00:05:36,640 --> 00:05:40,480
 two things to say to

107
00:05:40,480 --> 00:05:49,700
 this. The first thing is you should, when you are with them

108
00:05:49,700 --> 00:05:53,280
, try not to become

109
00:05:53,280 --> 00:06:05,000
 like them, like with unwholesome karma and destructive, but

110
00:06:05,000 --> 00:06:05,920
 you should try to

111
00:06:05,920 --> 00:06:14,280
 make them more like you are. You should try to teach them

112
00:06:14,280 --> 00:06:16,200
 the virtues

113
00:06:16,200 --> 00:06:20,880
 when they have none and to be not destructive when they are

114
00:06:20,880 --> 00:06:21,680
 destructive.

115
00:06:21,680 --> 00:06:27,660
 That's the one thing I wanted to say. The other thing is

116
00:06:27,660 --> 00:06:30,400
 that your karma brought

117
00:06:30,400 --> 00:06:35,650
 you there maybe. There are always more reasons to get

118
00:06:35,650 --> 00:06:36,720
 somewhere or to

119
00:06:36,720 --> 00:06:40,730
 experience something than just only karma. But let's say

120
00:06:40,730 --> 00:06:41,960
 your karma got you

121
00:06:41,960 --> 00:06:52,350
 there, then that's one thing. But what you are doing there,

122
00:06:52,350 --> 00:06:53,880
 what you are doing with

123
00:06:53,880 --> 00:07:00,680
 them, is what creates your future karma. So if you can

124
00:07:00,680 --> 00:07:02,200
 teach them in a wholesome

125
00:07:02,200 --> 00:07:06,830
 way, when you can make them better people, when you don't

126
00:07:06,830 --> 00:07:08,920
 get angry, when you don't

127
00:07:08,920 --> 00:07:14,880
 do unwholesome things while you're teaching them, then it's

128
00:07:14,880 --> 00:07:16,280
 absolutely not

129
00:07:16,280 --> 00:07:24,680
 important if you teach intelligent or high-class people or

130
00:07:24,680 --> 00:07:27,400
 low-class people.

131
00:07:27,400 --> 00:07:36,010
 It's just important what you do, how wholesome your mind is

132
00:07:36,010 --> 00:07:40,680
 at that moment. I

133
00:07:40,680 --> 00:07:47,090
 could have explained better but... It's fine. It's getting

134
00:07:47,090 --> 00:07:48,480
 late.

