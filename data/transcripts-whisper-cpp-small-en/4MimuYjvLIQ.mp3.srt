1
00:00:00,000 --> 00:00:07,600
 Good evening everyone.

2
00:00:07,600 --> 00:00:12,600
 Broadcasting Live, October 13th, 2015.

3
00:00:12,600 --> 00:00:29,480
 I'm going to go with another Dhammapada video tonight.

4
00:00:29,480 --> 00:00:36,480
 Testing, testing, testing, testing.

5
00:00:36,480 --> 00:00:48,880
 Hello and welcome back to our study of the Dhammapada.

6
00:00:48,880 --> 00:00:55,940
 Today we continue with verses 85 and 86, which read as

7
00:00:55,940 --> 00:00:58,520
 follows.

8
00:00:58,520 --> 00:01:06,810
 Apakati Manu Seeso, yejana par gamino, ata yang itta apajat

9
00:01:06,810 --> 00:01:09,520
ir meinwan dahoti,

10
00:01:09,520 --> 00:01:18,150
 yejko samadakati dami dhammanu atino, tejana par meisanti m

11
00:01:18,150 --> 00:01:24,120
achudhi yang surutta.

12
00:01:24,120 --> 00:01:30,790
 These verses are a common chant in Thailand, so quite

13
00:01:30,790 --> 00:01:31,520
 familiar.

14
00:01:31,520 --> 00:01:34,720
 These are the next ones in the chapter.

15
00:01:34,720 --> 00:01:45,320
 So these two mean, apakati manu seeso, few are they among

16
00:01:45,320 --> 00:01:45,320
 humans,

17
00:01:45,320 --> 00:01:50,720
 yejana par gamino, who go to the farther shore,

18
00:01:50,720 --> 00:01:54,720
 ata yang itta apajatir meinwan dahoti,

19
00:01:54,720 --> 00:02:01,720
 here these other humans, other people,

20
00:02:01,720 --> 00:02:08,520
 tiramey waludhavuti, simply run up and down the shore.

21
00:02:08,520 --> 00:02:22,520
 yejko samadakati, but those who, samadakati dami,

22
00:02:22,520 --> 00:02:27,720
 in regards to the well-taught dhamma, dhammanu atino,

23
00:02:27,720 --> 00:02:39,120
 are those who go according to the dhamma, tejana par meis

24
00:02:39,120 --> 00:02:39,120
anti.

25
00:02:39,120 --> 00:02:45,320
 These people go to the farther shore,

26
00:02:45,320 --> 00:02:49,120
 machudhi yang surutta.

27
00:02:49,120 --> 00:02:57,640
 The-- these cross, or these cross to the other side.

28
00:02:57,640 --> 00:03:01,520
 Machudhi yang to the king of the kingdom of death,

29
00:03:01,520 --> 00:03:04,960
 which is hard to cross, which is very hard to cross.

30
00:03:04,960 --> 00:03:14,080
 So we have some imagery here, imagery of crossing an ocean.

31
00:03:14,080 --> 00:03:18,400
 And now most people just run up and down the shore.

32
00:03:18,400 --> 00:03:21,800
 Tiram is this shore.

33
00:03:21,800 --> 00:03:24,120
 Anudavati is running up and down.

34
00:03:24,120 --> 00:03:32,000
 So this was given in regards to another very short story.

35
00:03:32,000 --> 00:03:35,920
 It's rather more of a remark that the Buddha gave in

36
00:03:35,920 --> 00:03:36,480
 regards

37
00:03:36,480 --> 00:03:41,830
 to the habits of those who would come and listen to the d

38
00:03:41,830 --> 00:03:42,120
hamma.

39
00:03:42,120 --> 00:03:46,880
 It seems that there was a specific group of people

40
00:03:46,880 --> 00:03:50,080
 who came to listen to the Buddha's teaching.

41
00:03:50,080 --> 00:03:54,710
 They had done good deeds, or they had supported the

42
00:03:54,710 --> 00:03:55,320
 monastery.

43
00:03:55,320 --> 00:03:58,720
 And so they came and gave alms to the monks

44
00:03:58,720 --> 00:04:02,480
 and then stuck around intending to listen to the dhamma all

45
00:04:02,480 --> 00:04:02,800
 night.

46
00:04:02,800 --> 00:04:06,420
 There would be teachings of the dhamma where the monks

47
00:04:06,420 --> 00:04:07,080
 would take turns

48
00:04:07,080 --> 00:04:10,660
 giving talks all night, or maybe even one monk would talk

49
00:04:10,660 --> 00:04:10,960
 all night,

50
00:04:10,960 --> 00:04:12,200
 probably the former.

51
00:04:12,200 --> 00:04:15,040
 Probably it would be several monks taking turns.

52
00:04:15,040 --> 00:04:21,800
 We once did this in Thailand, something like this.

53
00:04:21,800 --> 00:04:28,040
 There were some ancient Buddha statues that were stolen.

54
00:04:28,040 --> 00:04:30,760
 They were actually kept behind bars.

55
00:04:30,760 --> 00:04:31,800
 They were quite valuable.

56
00:04:31,800 --> 00:04:33,360
 They were kept behind bars.

57
00:04:33,360 --> 00:04:35,080
 And someone actually cut through the bars,

58
00:04:35,080 --> 00:04:38,920
 or bent the bars or something to steal these Buddha images.

59
00:04:38,920 --> 00:04:42,400
 Cut through the bars, I think, to steal the Buddha images.

60
00:04:42,400 --> 00:04:48,250
 And somewhat miraculously, one of them was found at the

61
00:04:48,250 --> 00:04:55,440
 bottom of one of the rivers,

62
00:04:55,440 --> 00:04:58,880
 I guess, like a small river, when it dried up or something

63
00:04:58,880 --> 00:04:59,800
 in the dry season.

64
00:04:59,800 --> 00:05:01,320
 It was found.

65
00:05:01,320 --> 00:05:07,520
 And so they pulled it up and brought it back and had an all

66
00:05:07,520 --> 00:05:08,880
 night ceremony.

67
00:05:08,880 --> 00:05:10,680
 It was something like this.

68
00:05:10,680 --> 00:05:14,200
 I can't-- the details are imprecise.

69
00:05:14,200 --> 00:05:17,280
 I maybe never even paid too much attention to the details.

70
00:05:17,280 --> 00:05:19,120
 But it was something like that.

71
00:05:19,120 --> 00:05:23,440
 And so we had an all night ceremony to honor the Buddha,

72
00:05:23,440 --> 00:05:27,980
 or maybe to reestablish its sanctity or something, its hol

73
00:05:27,980 --> 00:05:28,760
iness.

74
00:05:28,760 --> 00:05:31,280
 It was a very cultural sort of thing that I didn't pay too

75
00:05:31,280 --> 00:05:32,000
 much attention to.

76
00:05:32,000 --> 00:05:34,550
 It may have been broken or something, and there was a

77
00:05:34,550 --> 00:05:35,640
 fixing that had to go on.

78
00:05:35,640 --> 00:05:40,310
 And at the end of fixing it, you have to re-bless it or

79
00:05:40,310 --> 00:05:41,760
 something.

80
00:05:41,760 --> 00:05:44,230
 But it was kind of considered a miracle that they got it

81
00:05:44,230 --> 00:05:44,680
 back.

82
00:05:44,680 --> 00:05:46,320
 So it was a big deal.

83
00:05:46,320 --> 00:05:49,040
 And all of the monks, I was a part of it.

84
00:05:49,040 --> 00:05:52,840
 We would take turns chanting or giving talks or that kind

85
00:05:52,840 --> 00:05:54,760
 of thing all night

86
00:05:54,760 --> 00:05:59,760
 until the sun rose so that the spirit of this still goes on

87
00:05:59,760 --> 00:06:01,960
, even today.

88
00:06:01,960 --> 00:06:05,320
 This idea of an all night dhamma thing.

89
00:06:05,320 --> 00:06:06,160
 But it was sort of--

90
00:06:06,160 --> 00:06:08,080
 I think it was Ajahn Dang that put it together.

91
00:06:08,080 --> 00:06:10,280
 I don't know how often it happens.

92
00:06:10,280 --> 00:06:12,730
 I know in Burma they do all night chantings and all night

93
00:06:12,730 --> 00:06:14,160
 teachings,

94
00:06:14,160 --> 00:06:16,480
 from what I hear.

95
00:06:16,480 --> 00:06:21,080
 Anyway, the situation here was all of them

96
00:06:21,080 --> 00:06:23,400
 were trying to listen to the dhamma, but none of them

97
00:06:23,400 --> 00:06:25,360
 lasted.

98
00:06:25,360 --> 00:06:28,720
 Or maybe not none of them, but many of them were unable to

99
00:06:28,720 --> 00:06:29,280
 last.

100
00:06:31,240 --> 00:06:31,720
 Right.

101
00:06:31,720 --> 00:06:32,240
 None of them.

102
00:06:32,240 --> 00:06:35,320
 They were unable to listen to the dhamma all night.

103
00:06:35,320 --> 00:06:43,370
 Some of them were overcome by lust, sexual passion, and

104
00:06:43,370 --> 00:06:44,960
 went home.

105
00:06:44,960 --> 00:06:53,280
 Some of them were overcome by anger, boredom, maybe.

106
00:06:53,280 --> 00:06:59,280
 The anger is of a very different kind, many different kinds

107
00:06:59,280 --> 00:06:59,280
. It can be boredom.

108
00:06:59,280 --> 00:07:02,280
 It can be dislike of the dhamma teachings.

109
00:07:02,280 --> 00:07:09,390
 It can be self-hatred at the idea that every time you hear

110
00:07:09,390 --> 00:07:12,280
 something that hits too close to home,

111
00:07:12,280 --> 00:07:15,280
 maybe they're talking about things that are unwholesome,

112
00:07:15,280 --> 00:07:18,280
 and you realize that you engage in some of those,

113
00:07:18,280 --> 00:07:21,240
 and so you get angry at yourself and feel guilty in all

114
00:07:21,240 --> 00:07:22,280
 these things.

115
00:07:22,280 --> 00:07:24,630
 So being overcome with that, they weren't able to stay, and

116
00:07:24,630 --> 00:07:26,280
 they went home.

117
00:07:26,280 --> 00:07:36,280
 And some of them were overcome by mana, conceit.

118
00:07:36,280 --> 00:07:42,280
 So that would be the getting, feeling self-righteous

119
00:07:42,280 --> 00:07:46,280
 and displeased by the dhamma, but not an angry thing,

120
00:07:46,280 --> 00:07:50,280
 just sort of feeling like they knew better than the monks

121
00:07:50,280 --> 00:07:53,280
 or these monks don't know what they're talking about,

122
00:07:53,280 --> 00:07:56,280
 being attached to views and that kind of thing.

123
00:07:56,280 --> 00:07:59,830
 Some of them got caught up with "tinamida," so they got

124
00:07:59,830 --> 00:08:02,280
 tired, that's a common one.

125
00:08:02,280 --> 00:08:05,960
 And were nodding off and said, "I realize that they had to

126
00:08:05,960 --> 00:08:07,280
 go home is better."

127
00:08:07,280 --> 00:08:18,280
 And so they went home in the middle of the teaching.

128
00:08:18,280 --> 00:08:28,280
 And so they went off.

129
00:08:28,280 --> 00:08:32,280
 The next day, the monks gathered and commented on this,

130
00:08:32,280 --> 00:08:37,860
 and just were sort of remarking on how difficult it is for

131
00:08:37,860 --> 00:08:40,280
 people to listen to the dhamma

132
00:08:40,280 --> 00:08:46,660
 and how people aren't all that keen to take the opportunity

133
00:08:46,660 --> 00:08:49,280
 to listen to the dhamma.

134
00:08:49,280 --> 00:08:52,130
 And the Buddha said, "Monks for the most," the Buddha came

135
00:08:52,130 --> 00:08:52,280
 in

136
00:08:52,280 --> 00:08:54,920
 and asked them what they were talking about, and when they

137
00:08:54,920 --> 00:08:55,280
 told him,

138
00:08:55,280 --> 00:09:01,280
 he remarked that this is common for people.

139
00:09:01,280 --> 00:09:07,280
 He said, "For the most part, ye bu yena bhavani sita.

140
00:09:07,280 --> 00:09:12,640
 For the most part, they are dependent upon, or they are

141
00:09:12,640 --> 00:09:14,280
 attached to existence,

142
00:09:14,280 --> 00:09:17,280
 bhava, becoming.

143
00:09:17,280 --> 00:09:26,280
 Bhavaisu eva langa, we hananti, they dwell stuck,

144
00:09:26,280 --> 00:09:31,280
 stuck in bhava, stuck in becoming."

145
00:09:31,280 --> 00:09:34,280
 Meaning here, "bhava" is a sort of a neutral term,

146
00:09:34,280 --> 00:09:38,400
 that sort of blanket statement for anything that leads to

147
00:09:38,400 --> 00:09:40,280
 further becoming.

148
00:09:40,280 --> 00:09:44,650
 So when you want something, on a very basic level, it leads

149
00:09:44,650 --> 00:09:45,280
 to becoming here,

150
00:09:45,280 --> 00:09:51,250
 and now you give rise to something new, which is the plan

151
00:09:51,250 --> 00:09:53,280
 to get what you want.

152
00:09:53,280 --> 00:09:58,180
 You want to eat, and so suddenly the plan to make food

153
00:09:58,180 --> 00:09:59,280
 arises.

154
00:09:59,280 --> 00:10:04,280
 Maybe you want to watch a movie, so you plan to go out,

155
00:10:04,280 --> 00:10:10,280
 I guess people don't go to the video store anymore.

156
00:10:10,280 --> 00:10:15,280
 Whatever you want, you want to go to Thailand,

157
00:10:15,280 --> 00:10:20,280
 and so you give rise to something, this.

158
00:10:20,280 --> 00:10:26,830
 You give rise to further becoming something that's not just

159
00:10:26,830 --> 00:10:27,280
 functional,

160
00:10:27,280 --> 00:10:34,410
 but it's actually a desire base, a whole new set of

161
00:10:34,410 --> 00:10:36,280
 variables.

162
00:10:36,280 --> 00:10:40,280
 Or if you're angry, you give rise to something new as well,

163
00:10:40,280 --> 00:10:43,780
 the very least a headache, but you can also have anger give

164
00:10:43,780 --> 00:10:45,280
 rise to argument,

165
00:10:45,280 --> 00:10:50,280
 friction, conflict, war, even.

166
00:10:50,280 --> 00:10:53,320
 Of course, war can be caused by greed, and often it's

167
00:10:53,320 --> 00:10:55,280
 caused simply by greed.

168
00:10:55,280 --> 00:11:03,270
 Concede, you give rise to this competition or this sense of

169
00:11:03,270 --> 00:11:04,280
 this conflict

170
00:11:04,280 --> 00:11:10,280
 based on power struggle and desire for dominance,

171
00:11:10,280 --> 00:11:11,280
 oppression,

172
00:11:11,280 --> 00:11:15,280
 this kind of thing, oppressing others, belittling others,

173
00:11:15,280 --> 00:11:16,280
 that kind of thing.

174
00:11:16,280 --> 00:11:22,880
 So you create something new, or you'd simply create a new

175
00:11:22,880 --> 00:11:24,280
 plan for yourself.

176
00:11:24,280 --> 00:11:29,830
 I deserve to be king, so I'm going to fight my way to

177
00:11:29,830 --> 00:11:31,280
 become king.

178
00:11:31,280 --> 00:11:36,280
 I deserve this or that, or I don't deserve this or that,

179
00:11:36,280 --> 00:11:42,280
 and so you strive accordingly.

180
00:11:42,280 --> 00:11:48,240
 Sometimes it's just out of laziness, this gives rise to

181
00:11:48,240 --> 00:11:48,280
 sleep,

182
00:11:48,280 --> 00:11:52,440
 but it also gives rise to sickness, it gives rise to

183
00:11:52,440 --> 00:11:54,280
 conflict and problems

184
00:11:54,280 --> 00:11:59,280
 when you don't act according to your duties, where you just

185
00:11:59,280 --> 00:11:59,280
 consume

186
00:11:59,280 --> 00:12:04,280
 but don't produce, and as a result people become upset

187
00:12:04,280 --> 00:12:12,280
 because you're simply a consumer, a mooch, lazy, etc.

188
00:12:12,280 --> 00:12:16,280
 And so on. So this is the idea of becoming.

189
00:12:16,280 --> 00:12:18,280
 It's really anything that leads to...

190
00:12:18,280 --> 00:12:22,280
 So there was a question recently about what's wrong with...

191
00:12:22,280 --> 00:12:25,660
 There's always questions about what's wrong with seeking

192
00:12:25,660 --> 00:12:27,280
 out pleasure,

193
00:12:27,280 --> 00:12:31,280
 ordinary pleasure, because you do get some.

194
00:12:31,280 --> 00:12:33,280
 And it's really this...

195
00:12:33,280 --> 00:12:42,180
 These two verses point to the mindset or the outlook of the

196
00:12:42,180 --> 00:12:43,280
 Buddha

197
00:12:43,280 --> 00:12:47,280
 and of Buddhists in general, is that it's not enough

198
00:12:47,280 --> 00:12:50,730
 and it's not sustainable because the more you want, as I

199
00:12:50,730 --> 00:12:51,280
 said,

200
00:12:51,280 --> 00:12:55,110
 the more you suffer, the less you get, or the less

201
00:12:55,110 --> 00:12:57,280
 satisfied you are.

202
00:12:57,280 --> 00:13:00,280
 In fact, it's the wrong way, it's unsustainable.

203
00:13:00,280 --> 00:13:03,280
 You find yourself bouncing back and forth, or for a time,

204
00:13:03,280 --> 00:13:06,760
 you're able to balance things before you fall one way or

205
00:13:06,760 --> 00:13:07,280
 the other

206
00:13:07,280 --> 00:13:10,280
 and then it's back and forth again.

207
00:13:10,280 --> 00:13:14,270
 And it's building up disappointment, building up attachment

208
00:13:14,270 --> 00:13:16,280
 and stress

209
00:13:16,280 --> 00:13:25,280
 or it's working for a time to either repress,

210
00:13:25,280 --> 00:13:29,280
 often it's simply to repress our desires.

211
00:13:29,280 --> 00:13:32,280
 We fluctuate back and forth and go through life,

212
00:13:32,280 --> 00:13:35,280
 or we go through lives again and again and again,

213
00:13:35,280 --> 00:13:39,280
 or born old sick die, born old sick die.

214
00:13:41,280 --> 00:13:47,800
 So this is sort of the outlook that the Buddha's basing

215
00:13:47,800 --> 00:13:49,280
 this on.

216
00:13:49,280 --> 00:13:52,280
 These two verses are actually...

217
00:13:52,280 --> 00:13:56,440
 One thing that stands out for me, besides the obvious

218
00:13:56,440 --> 00:13:57,280
 symbolism

219
00:13:57,280 --> 00:14:04,640
 or imagery which is quite powerful, is sort of congrat

220
00:14:04,640 --> 00:14:06,280
ulatory.

221
00:14:06,280 --> 00:14:11,280
 It seemed like a good opportunity to congratulate

222
00:14:11,280 --> 00:14:15,280
 all of the people involved with this community,

223
00:14:15,280 --> 00:14:19,820
 all of the listeners, all of you who are listening to the D

224
00:14:19,820 --> 00:14:20,280
hamma,

225
00:14:20,280 --> 00:14:23,280
 taking the time, some of you, every day to...

226
00:14:23,280 --> 00:14:26,280
 It's not something I never thought would happen, I thought,

227
00:14:26,280 --> 00:14:28,280
 "Okay, once a week maybe."

228
00:14:28,280 --> 00:14:32,160
 But some people actually come out every day to listen to

229
00:14:32,160 --> 00:14:33,280
 the Dhamma.

230
00:14:33,280 --> 00:14:38,280
 You take the time to come online and sit still for an hour,

231
00:14:38,280 --> 00:14:42,280
 half an hour, an hour, even to ask questions,

232
00:14:42,280 --> 00:14:47,520
 so to get involved, to perform this act of good karma of

233
00:14:47,520 --> 00:14:48,280
 asking questions.

234
00:14:48,280 --> 00:14:54,280
 And many of you are very respectful and so being respectful

235
00:14:54,280 --> 00:14:57,280
 and all sorts of good karma going on.

236
00:14:57,280 --> 00:15:02,280
 And that's something to be proud of

237
00:15:02,280 --> 00:15:05,280
 because even in the context of the story,

238
00:15:05,280 --> 00:15:09,280
 many people...

239
00:15:09,280 --> 00:15:11,280
 I mean, in the context of the story,

240
00:15:11,280 --> 00:15:13,960
 these people are still to be congratulated because they

241
00:15:13,960 --> 00:15:14,280
 tried.

242
00:15:14,280 --> 00:15:18,280
 And it's not like they didn't hear some of the Dhamma,

243
00:15:18,280 --> 00:15:20,550
 it's just they tried to do something beyond their

244
00:15:20,550 --> 00:15:21,280
 capabilities.

245
00:15:21,280 --> 00:15:28,280
 But all of the people here should be congratulated as well

246
00:15:28,280 --> 00:15:30,280
 for having the good intentions.

247
00:15:30,280 --> 00:15:35,280
 Many people don't even think about crossing.

248
00:15:35,280 --> 00:15:40,560
 Or if they think about crossing, they never do anything to

249
00:15:40,560 --> 00:15:41,280
 try to cross.

250
00:15:41,280 --> 00:15:43,280
 They would never come and meditate.

251
00:15:43,280 --> 00:15:46,280
 If you look at the list of meditators that we have,

252
00:15:46,280 --> 00:15:49,280
 you can see how many we have tonight.

253
00:15:49,280 --> 00:15:54,280
 We've got actually a fairly small list today.

254
00:15:54,280 --> 00:15:57,970
 Uh-oh, and a bunch of orange people, maybe I spoke too soon

255
00:15:57,970 --> 00:15:58,280
.

256
00:15:58,280 --> 00:16:00,280
 What happened today?

257
00:16:00,280 --> 00:16:02,280
 Well, it's still...

258
00:16:02,280 --> 00:16:06,870
 We have many, many people and we have people meditating on

259
00:16:06,870 --> 00:16:07,280
 this side

260
00:16:07,280 --> 00:16:09,280
 around the clock.

261
00:16:09,280 --> 00:16:14,280
 People coming to listen to the Dhamma every day.

262
00:16:14,280 --> 00:16:20,280
 So this is a sign of at least wanting to cross.

263
00:16:20,280 --> 00:16:23,090
 Maybe some people just listen to the talks or watch my

264
00:16:23,090 --> 00:16:24,280
 YouTube videos

265
00:16:24,280 --> 00:16:29,280
 and maybe dip their foot in the water but don't ever cross.

266
00:16:29,280 --> 00:16:32,280
 But at the very least, there's some thought.

267
00:16:32,280 --> 00:16:34,280
 And this is the first step.

268
00:16:34,280 --> 00:16:36,280
 The first step is thinking about it,

269
00:16:36,280 --> 00:16:38,280
 having the...

270
00:16:38,280 --> 00:16:45,340
 giving rise to the intention or the desire to better

271
00:16:45,340 --> 00:16:46,280
 oneself

272
00:16:46,280 --> 00:16:49,280
 as opposed to just running up and down the shore.

273
00:16:49,280 --> 00:16:52,280
 Because that's what it's like.

274
00:16:52,280 --> 00:16:55,280
 That's what the Buddha likens most of our activity to,

275
00:16:55,280 --> 00:16:58,280
 just running up and down the shore.

276
00:16:58,280 --> 00:17:02,280
 Sometimes running up and down out of desire,

277
00:17:02,280 --> 00:17:08,250
 sometimes running up and down out of anger, fear, worry,

278
00:17:08,250 --> 00:17:09,280
 conceit.

279
00:17:09,280 --> 00:17:13,280
 We have many, many ways of running around in circles.

280
00:17:19,280 --> 00:17:22,280
 Running around in circles in the kingdom of death.

281
00:17:22,280 --> 00:17:25,280
 This is where we find ourselves being born again and again,

282
00:17:25,280 --> 00:17:27,280
 dying again and again.

283
00:17:27,280 --> 00:17:32,280
 Learning and forgetting.

284
00:17:32,280 --> 00:17:42,280
 Not really learning from our mistakes.

285
00:17:42,280 --> 00:17:46,280
 Our intention here, our practice here, is to break that,

286
00:17:46,280 --> 00:17:51,280
 to cross over, to go beyond, to go beyond death,

287
00:17:51,280 --> 00:17:56,280
 to figure the system out, to come to understand the system

288
00:17:56,280 --> 00:17:59,280
 and to understand becoming,

289
00:17:59,280 --> 00:18:01,280
 and as a result not be dependent upon it.

290
00:18:01,280 --> 00:18:04,280
 One way of explaining why we're dependent upon it is

291
00:18:04,280 --> 00:18:07,280
 because we don't understand it.

292
00:18:07,280 --> 00:18:11,280
 We're dependent upon it because it runs us.

293
00:18:11,280 --> 00:18:12,280
 We don't run it.

294
00:18:12,280 --> 00:18:17,280
 We don't run it.

295
00:18:17,280 --> 00:18:19,280
 We don't run it.

296
00:18:19,280 --> 00:18:21,280
 We don't run it.

297
00:18:21,280 --> 00:18:23,280
 We don't run it.

298
00:18:23,280 --> 00:18:25,280
 We don't run it.

299
00:18:25,280 --> 00:18:27,280
 We don't run it.

300
00:18:27,280 --> 00:18:29,280
 We don't run it.

301
00:18:29,280 --> 00:18:31,280
 We don't run it.

302
00:18:31,280 --> 00:18:33,280
 We don't run it.

303
00:18:33,280 --> 00:18:35,280
 We don't run it.

304
00:18:35,280 --> 00:18:37,280
 We don't run it.

305
00:18:37,280 --> 00:18:39,280
 We don't run it.

306
00:18:39,280 --> 00:18:40,280
 We don't run it.

307
00:18:40,280 --> 00:18:42,280
 We don't run it.

308
00:18:42,280 --> 00:18:43,280
 We don't run it.

309
00:18:43,280 --> 00:18:44,280
 We don't run it.

310
00:18:44,280 --> 00:18:45,280
 We don't run it.

311
00:18:45,280 --> 00:18:46,280
 We don't run it.

312
00:18:46,280 --> 00:18:47,280
 We don't run it.

313
00:18:47,280 --> 00:18:48,280
 We don't run it.

314
00:18:48,280 --> 00:18:49,280
 We don't run it.

315
00:18:49,280 --> 00:18:50,280
 We don't run it.

316
00:18:50,280 --> 00:18:51,280
 We don't run it.

317
00:18:51,280 --> 00:18:52,280
 We don't run it.

318
00:18:52,280 --> 00:18:53,280
 We don't run it.

319
00:18:53,280 --> 00:18:54,280
 We don't run it.

320
00:18:54,280 --> 00:18:55,280
 We don't run it.

321
00:18:55,280 --> 00:18:56,280
 We don't run it.

322
00:18:56,280 --> 00:18:57,280
 We don't run it.

323
00:18:57,280 --> 00:18:58,280
 We don't run it.

324
00:18:58,280 --> 00:18:59,280
 We don't run it.

325
00:18:59,280 --> 00:19:00,280
 We don't run it.

326
00:19:00,280 --> 00:19:01,280
 We don't run it.

327
00:19:01,280 --> 00:19:02,280
 We don't run it.

328
00:19:02,280 --> 00:19:03,280
 We don't run it.

329
00:19:03,280 --> 00:19:04,280
 We don't run it.

330
00:19:04,280 --> 00:19:05,280
 We don't run it.

331
00:19:05,280 --> 00:19:06,280
 We don't run it.

332
00:19:06,280 --> 00:19:07,280
 We don't run it.

333
00:19:07,280 --> 00:19:08,280
 We don't run it.

334
00:19:08,280 --> 00:19:12,870
 It's rather a poetic verse, a simple story, simple verse

335
00:19:12,870 --> 00:19:14,920
 really, simple meaning.

336
00:19:14,920 --> 00:19:24,080
 It's kind of hard teaching.

337
00:19:24,080 --> 00:19:26,610
 It lays bare the fact that most of what we do is just

338
00:19:26,610 --> 00:19:28,360
 running up and down the shore.

339
00:19:28,360 --> 00:19:32,920
 It doesn't really accomplish anything in the end.

340
00:19:32,920 --> 00:19:35,250
 But we should, on the other hand, it should be an

341
00:19:35,250 --> 00:19:37,320
 encouraging teaching because whatever

342
00:19:37,320 --> 00:19:41,270
 the good things that we do, this is our coming closer to be

343
00:19:41,270 --> 00:19:43,360
 one of those few people who is

344
00:19:43,360 --> 00:19:48,130
 actually able to cross over, actually able to rise above

345
00:19:48,130 --> 00:19:50,560
 and free themselves from the

346
00:19:50,560 --> 00:19:54,760
 wheel of suffering, from the kingdom of death.

347
00:19:54,760 --> 00:19:56,680
 So good work, everyone.

348
00:19:56,680 --> 00:20:00,870
 Thank you all for tuning in and for supporting these

349
00:20:00,870 --> 00:20:04,120
 teachings with your practice, for practicing

350
00:20:04,120 --> 00:20:08,040
 and continuing on this practice.

351
00:20:08,040 --> 00:20:10,320
 So that's the Dhammapada teaching for tonight.

352
00:20:10,320 --> 00:20:12,240
 Thank you all for tuning in.

353
00:20:12,240 --> 00:20:13,520
 Keep practicing and be well.

354
00:20:13,520 --> 00:20:36,800
 Stefanos is with me again tonight.

355
00:20:36,800 --> 00:20:37,800
 Hello.

356
00:20:37,800 --> 00:20:43,800
 Thank you, Roman's place.

357
00:20:43,800 --> 00:20:46,240
 Do you have any questions?

358
00:20:46,240 --> 00:20:47,960
 Yes.

359
00:20:47,960 --> 00:20:52,300
 First question is, the cat charity I do some volunteer work

360
00:20:52,300 --> 00:20:54,000
 for are running a raffle to

361
00:20:54,000 --> 00:20:55,920
 raise cash.

362
00:20:55,920 --> 00:20:58,650
 You say that gambling is not in line with the teachings,

363
00:20:58,650 --> 00:21:00,080
 but if the money raised goes

364
00:21:00,080 --> 00:21:04,450
 towards helping running something good and makes everyone

365
00:21:04,450 --> 00:21:06,480
 feel good, would that make

366
00:21:06,480 --> 00:21:08,280
 a difference?

367
00:21:08,280 --> 00:21:09,280
 I think so.

368
00:21:09,280 --> 00:21:13,590
 I mean, no one's going to come out saying, feeling bad

369
00:21:13,590 --> 00:21:16,240
 about having donated, right?

370
00:21:16,240 --> 00:21:20,240
 No one's going to come out saying, if only I had, you know,

371
00:21:20,240 --> 00:21:22,200
 it's not, it's not exactly

372
00:21:22,200 --> 00:21:23,280
 the same as gambling.

373
00:21:23,280 --> 00:21:24,280
 It's not perfect.

374
00:21:24,280 --> 00:21:28,060
 And I wouldn't say you're scot free from all the wholesomen

375
00:21:28,060 --> 00:21:29,600
ess and gambling because you're

376
00:21:29,600 --> 00:21:32,440
 encouraging greed and people still.

377
00:21:32,440 --> 00:21:33,960
 It's like a white and black karma.

378
00:21:33,960 --> 00:21:36,330
 It's definitely a good example of a white and black karma

379
00:21:36,330 --> 00:21:38,640
 because people are doing it out

380
00:21:38,640 --> 00:21:45,000
 of desire to help, but also out of the desire to win.

381
00:21:45,000 --> 00:21:47,130
 It's you know, for most people, I don't think it's such a

382
00:21:47,130 --> 00:21:48,120
 big deal that they win.

383
00:21:48,120 --> 00:21:51,440
 They're just, it's a way of encouraging people, encouraging

384
00:21:51,440 --> 00:21:53,560
 the people's intention, but you're

385
00:21:53,560 --> 00:21:56,190
 encouraging it with, with a little bit of greed or

386
00:21:56,190 --> 00:21:58,080
 excitement for, Oh, what if I did

387
00:21:58,080 --> 00:22:00,080
 when wouldn't that be kind of neat?

388
00:22:00,080 --> 00:22:03,550
 I don't think most people buy raffle tickets thinking, yeah

389
00:22:03,550 --> 00:22:05,680
, I really want to win them,

390
00:22:05,680 --> 00:22:06,680
 but I don't know really.

391
00:22:06,680 --> 00:22:09,870
 I think many people just do it to support the charity,

392
00:22:09,870 --> 00:22:11,320
 which is a good part.

393
00:22:11,320 --> 00:22:15,870
 So it's not, not how I would do things and how I would

394
00:22:15,870 --> 00:22:18,800
 support a charity, but, or try

395
00:22:18,800 --> 00:22:22,160
 to get us charity supported.

396
00:22:22,160 --> 00:22:27,280
 Hello, Bonté.

397
00:22:27,280 --> 00:22:29,720
 When I am talking to someone in my thoughts while med

398
00:22:29,720 --> 00:22:31,600
itating, should I acknowledge this

399
00:22:31,600 --> 00:22:34,280
 as talking or thinking?

400
00:22:34,280 --> 00:22:37,400
 Thanks.

401
00:22:37,400 --> 00:22:39,320
 When you were talking to someone?

402
00:22:39,320 --> 00:22:43,380
 I think when they're talking to someone in their head, I

403
00:22:43,380 --> 00:22:46,160
 think like going over a conversation

404
00:22:46,160 --> 00:22:47,160
 or something.

405
00:22:47,160 --> 00:22:48,160
 I don't know.

406
00:22:48,160 --> 00:22:51,120
 Yeah, I would still say thinking, right?

407
00:22:51,120 --> 00:22:52,880
 Going over a conversation is more thinking.

408
00:22:52,880 --> 00:22:54,600
 If you want, you can say talking again.

409
00:22:54,600 --> 00:22:57,400
 And the word is not the most important thing.

410
00:22:57,400 --> 00:23:00,460
 Which word you pick, it's just something that keeps you

411
00:23:00,460 --> 00:23:01,300
 objective.

412
00:23:01,300 --> 00:23:04,930
 So it's not damn that person who I talked with or why I

413
00:23:04,930 --> 00:23:07,360
 really love that person, really

414
00:23:07,360 --> 00:23:10,760
 attracted to that person, et cetera, et cetera.

415
00:23:10,760 --> 00:23:13,520
 Keeps you, whatever keeps you objective.

416
00:23:13,520 --> 00:23:15,440
 So if it's talking, just say talking, talking.

417
00:23:15,440 --> 00:23:19,520
 And you'll see that, Oh, it's actually just a thought.

418
00:23:19,520 --> 00:23:23,680
 Your thinking is better because you'll see that it's just a

419
00:23:23,680 --> 00:23:24,560
 thought.

420
00:23:24,560 --> 00:23:27,730
 Or if they mean when they're actually talking to someone,

421
00:23:27,730 --> 00:23:29,920
 what should they do in their thoughts?

422
00:23:29,920 --> 00:23:31,520
 Like how should they be mindful?

423
00:23:31,520 --> 00:23:33,760
 They might mean that.

424
00:23:33,760 --> 00:23:37,760
 Mindful of your lips moving, feeling, feeling.

425
00:23:37,760 --> 00:23:41,740
 The mindful of the feelings in the body, in the emotions,

426
00:23:41,740 --> 00:23:43,960
 the thoughts, the intentions

427
00:23:43,960 --> 00:23:46,920
 to speak.

428
00:23:46,920 --> 00:23:47,920
 Your mind is pretty quick.

429
00:23:47,920 --> 00:23:58,000
 So you can do this all and still continue to talk.

430
00:23:58,000 --> 00:24:02,300
 When I have emotions, for example, anger, I know anger and

431
00:24:02,300 --> 00:24:04,400
 then I just know feeling,

432
00:24:04,400 --> 00:24:07,350
 feeling, because it seems that there isn't anger, but just

433
00:24:07,350 --> 00:24:08,160
 sensations.

434
00:24:08,160 --> 00:24:11,410
 Is this correct noting or should I keep noting anger

435
00:24:11,410 --> 00:24:12,720
 because it was anger?

436
00:24:12,720 --> 00:24:13,720
 That's awesome.

437
00:24:13,720 --> 00:24:15,520
 Yeah, no, that's very clear.

438
00:24:15,520 --> 00:24:16,880
 All of the emotions are like that.

439
00:24:16,880 --> 00:24:20,640
 They have physical counterparts, physical manifestations

440
00:24:20,640 --> 00:24:22,200
 and physical effects.

441
00:24:22,200 --> 00:24:25,370
 And they are distinct from the anger and the greed and the

442
00:24:25,370 --> 00:24:27,760
 worry and the fear and et cetera,

443
00:24:27,760 --> 00:24:28,760
 et cetera.

444
00:24:28,760 --> 00:24:31,760
 So acknowledge them separate.

445
00:24:31,760 --> 00:24:35,400
 Absolutely.

446
00:24:35,400 --> 00:24:39,640
 Sometimes during sitting meditation, I get very distracted

447
00:24:39,640 --> 00:24:41,160
 because I am tired.

448
00:24:41,160 --> 00:24:43,200
 I start to fall asleep.

449
00:24:43,200 --> 00:24:46,400
 I find that I can focus better if my eyes are open.

450
00:24:46,400 --> 00:24:51,550
 Does this take away from the point of meditation or is this

451
00:24:51,550 --> 00:24:52,400
 okay?

452
00:24:52,400 --> 00:24:55,880
 It's okay.

453
00:24:55,880 --> 00:24:56,880
 It's not wrong.

454
00:24:56,880 --> 00:24:59,850
 It's better to have your eyes closed when you can, but you

455
00:24:59,850 --> 00:25:01,200
 find it keeps you awake.

456
00:25:01,200 --> 00:25:06,040
 It's really not wrong to open your eyes.

457
00:25:06,040 --> 00:25:09,930
 It's better, of course, to overcome the drowsiness and keep

458
00:25:09,930 --> 00:25:12,040
 meditating, but you can do it either

459
00:25:12,040 --> 00:25:13,040
 way.

460
00:25:13,040 --> 00:25:14,680
 I mean, your mind shouldn't be with your eyes anyway.

461
00:25:14,680 --> 00:25:17,680
 It should be with whatever your object is.

462
00:25:17,680 --> 00:25:18,680
 So with the stomach.

463
00:25:18,680 --> 00:25:22,360
 In fact, in the end, it's probably just a trick.

464
00:25:22,360 --> 00:25:26,410
 And it's not, I mean, it's not really the cause of your d

465
00:25:26,410 --> 00:25:27,560
rowsiness.

466
00:25:27,560 --> 00:25:33,010
 Once you get equally focused with your eyes open and you're

467
00:25:33,010 --> 00:25:36,240
 still going to get drowsiness.

468
00:25:36,240 --> 00:25:48,530
 Better solutions probably to do walking meditation, I would

469
00:25:48,530 --> 00:25:50,480
 think.

470
00:25:50,480 --> 00:25:53,970
 How long do you recommend a student practice your teachings

471
00:25:53,970 --> 00:25:55,440
 on meditation according to

472
00:25:55,440 --> 00:25:58,930
 your booklet before undertaking the formal study with you

473
00:25:58,930 --> 00:26:00,720
 over the scheduled one on one

474
00:26:00,720 --> 00:26:04,080
 meetings via the internet?

475
00:26:04,080 --> 00:26:05,080
 Thank you.

476
00:26:05,080 --> 00:26:08,320
 Take a week.

477
00:26:08,320 --> 00:26:12,850
 If you've done a week of at least an hour a day, you know,

478
00:26:12,850 --> 00:26:14,960
 two, if you can at least

479
00:26:14,960 --> 00:26:26,520
 an hour, one hour a day, then, you know, I'm happy to help.

480
00:26:26,520 --> 00:26:29,120
 What's meta supposed to feel like?

481
00:26:29,120 --> 00:26:32,200
 It kind of bothers me that I can't feel much meta, but I'm

482
00:26:32,200 --> 00:26:33,840
 thinking I might have the wrong

483
00:26:33,840 --> 00:26:35,480
 idea about what it is.

484
00:26:35,480 --> 00:26:37,160
 Is this supposed to be sentimental?

485
00:26:37,160 --> 00:26:38,680
 Kind of, yeah.

486
00:26:38,680 --> 00:26:42,920
 I wouldn't worry too much about it.

487
00:26:42,920 --> 00:26:46,370
 Meta is something that comes naturally as you practice

488
00:26:46,370 --> 00:26:47,920
 insight meditation.

489
00:26:47,920 --> 00:26:51,240
 We concern more about insight.

490
00:26:51,240 --> 00:26:55,760
 Meta is secondary.

491
00:26:55,760 --> 00:27:00,460
 But if you really want to experience it, focus on people

492
00:27:00,460 --> 00:27:02,760
 who you know you have that love

493
00:27:02,760 --> 00:27:07,000
 for, like family members maybe, or good friends.

494
00:27:07,000 --> 00:27:13,320
 It's a sense of friendliness, wishing for them to be happy.

495
00:27:13,320 --> 00:27:23,280
 Good intentions toward them.

496
00:27:23,280 --> 00:27:27,870
 Does the process of samsara, death and rebirth, occur on

497
00:27:27,870 --> 00:27:31,360
 other planets or in other solar systems?

498
00:27:31,360 --> 00:27:35,280
 I have no idea.

499
00:27:35,280 --> 00:27:36,280
 Sorry.

500
00:27:36,280 --> 00:27:39,280
 And that's it.

501
00:27:39,280 --> 00:27:40,280
 Okay.

502
00:27:40,280 --> 00:27:45,600
 I think we'll stop then.

503
00:27:45,600 --> 00:27:56,120
 Oh, wait, maybe there is one way.

504
00:27:56,120 --> 00:28:01,440
 Can you tell us the best way to help feed you currently?

505
00:28:01,440 --> 00:28:03,240
 Is it through YouCaring site?

506
00:28:03,240 --> 00:28:08,950
 No, the YouCaring site is a project set up by Robin to help

507
00:28:08,950 --> 00:28:11,200
 support the monthly expenses

508
00:28:11,200 --> 00:28:16,040
 of just having a house, having a place.

509
00:28:16,040 --> 00:28:23,200
 So that's for things like rent and utilities.

510
00:28:23,200 --> 00:28:25,600
 That's something fairly specific.

511
00:28:25,600 --> 00:28:31,970
 Although it goes, that's the nonprofit organization that is

512
00:28:31,970 --> 00:28:35,240
 run by a group of volunteers.

513
00:28:35,240 --> 00:28:36,240
 So it's general.

514
00:28:36,240 --> 00:28:39,870
 I mean, it can be used for various things, but it isn't

515
00:28:39,870 --> 00:28:42,000
 used to support me actually.

516
00:28:42,000 --> 00:28:45,870
 Except insofar as I live here and depend very much on this

517
00:28:45,870 --> 00:28:48,160
 place and the bills and so on.

518
00:28:48,160 --> 00:28:52,080
 I mean, it's really a big thing.

519
00:28:52,080 --> 00:28:54,160
 But yeah, Fernando has probably the right link.

520
00:28:54,160 --> 00:28:55,960
 That's probably your best bet.

521
00:28:55,960 --> 00:28:57,800
 I will say that I don't...

522
00:28:57,800 --> 00:29:01,400
 Food is really not an issue at this point, though it's

523
00:29:01,400 --> 00:29:03,560
 often the one thing that people

524
00:29:03,560 --> 00:29:05,040
 would like to help with.

525
00:29:05,040 --> 00:29:06,800
 It's not an easy thing.

526
00:29:06,800 --> 00:29:13,850
 I mean, ideally, if you have a house somewhere nearby, they

527
00:29:13,850 --> 00:29:17,200
 can come for alms one morning.

528
00:29:17,200 --> 00:29:20,560
 Or even not so close.

529
00:29:20,560 --> 00:29:24,680
 I could find time to travel throughout Ontario.

530
00:29:24,680 --> 00:29:30,760
 Suppose going to America for alms is a bit much.

531
00:29:30,760 --> 00:29:36,410
 Although, outside of the rains, I can take time to go and

532
00:29:36,410 --> 00:29:38,640
 stay in someone's backyard

533
00:29:38,640 --> 00:29:44,920
 during the hot season.

534
00:29:44,920 --> 00:29:49,040
 But then it would be more like a trip than an alms giving.

535
00:29:49,040 --> 00:29:52,320
 But barring that, there's ways of...

536
00:29:52,320 --> 00:29:54,120
 Well, they've got to go to the food support.

537
00:29:54,120 --> 00:29:57,280
 That's what we've figured out so far.

538
00:29:57,280 --> 00:30:00,450
 And that's Tina is doing most of the organizing there, I

539
00:30:00,450 --> 00:30:00,960
 think.

540
00:30:00,960 --> 00:30:06,960
 But there's several people involved with that.

541
00:30:06,960 --> 00:30:12,440
 Okay, so that's all for today.

542
00:30:12,440 --> 00:30:14,320
 Thank you all for tuning in.

543
00:30:14,320 --> 00:30:15,320
 Thank you.

544
00:30:15,320 --> 00:30:17,320
 Thanks, Fernando, for taking part.

545
00:30:17,320 --> 00:30:20,640
 Have a good night, everyone.

546
00:30:20,640 --> 00:30:21,640
 Thank you.

547
00:30:21,640 --> 00:30:31,640
 [BLANK_AUDIO]

