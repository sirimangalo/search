 Good evening everyone.
 Broadcasting Live, April 29th, 2016.
 Today's quote is about giving up. It's about our inability
 to give up.
 It's from the Majima Nikaya, the Majima Nikaya number 66,
 the Pali, the Luck,
 the Kiko-pama-sutta, the Simile of the Quail. So the Buddha
 admonished the monks to eat once a day.
 And the story goes that some of the monks were not happy
 with that. They said, "We can't abide by that."
 And so the Buddha said, "Well, then at least eat one
 portion of food today."
 Meaning go for alms, get your food, and eat some in the
 morning and some for lunch, and that's it.
 And the monks said, "Some monks said, 'We can't abide by
 that either.'"
 And so the Buddha left. He said, "I can't live with such
 monks."
 This sutta is sort of reflecting on that, Udayin. He comes
 to the Buddha and he admits that it was hard for him at
 first to accept the idea of eating only in the morning,
 only once.
 But once he realized the benefits, he was actually quite
 happy with it.
 And so he comes and he appreciates, he says, let's look at
 the Pali,
 Bhaunang Watanum Bhagavadukadhamanam Apahata.
 The Buddha is one who has relinquished, has relieved us of
 many things that cause suffering.
 He has Bhaunang Sukadhamanam Apahata.
 He is one who brings us many dhammas that bring happiness.
 Umbahunang Akusala Nang Dhamma Nang Apahata. He is the
 bringer of, he is the reliever, the remover of many dhammas
 that are unwholesome.
 And he is the conveyor of many dhammas that are wholesome.
 He thought to himself and he came to see the Buddha and he
 repeated this to the Buddha. He said, it occurred to me
 that the Buddha has done so much for us.
 Teaching us even these small things, things that seem a tr
ifle, things that seem somewhat irrelevant.
 Why should we have to give up eating? What's the difference
 between eating in the morning and the evening?
 What's the difference between eating throughout the day?
 If you've never undertaken the training much, they hadn't.
 It seems somewhat trivial, inconsequential, things like
 that.
 I remember when I was back in the day, in Thailand a lot of
 monks, a lot of the rules have become quite lax.
 There are monks who, well most monks will have soya milk in
 the evening.
 And it was things like that. Most of us wouldn't eat in the
 evening but a lot of monks, myself included,
 would get soya milk, tetrapax and almsraun, sometimes five,
 ten of them.
 And we'd keep them and we'd drink them in the evening.
 And then I started reading the vinaya and realized that
 this was against the rules.
 I remember how hard it was to give that up at all.
 I'm not sure if I can do that but I remember just this suta
 brings that to mind because
 I remember being with another monk and explaining to a new
 monk and explaining to him
 it wasn't according to the rules.
 So in the evening we were sitting around thinking of soya
 milk and trying to find something that we were allowed.
 And so we had some tea at first but then we started to
 think, you know, tea is only allowed if it's a medicine.
 I remember giving these little things, you know, these
 little things that are so intrinsic to life,
 like not eating in the evening, it's just such a core part
 of our life that in fact when you do give it up,
 it changes something.
 It's one of those things that changes your life.
 You know, it sounds dramatic, it's not a dramatic change
 but it does do something because it's so core, you know.
 These things that are so intrinsic a part of our lives.
 Even they seem inconsequential, you give them up and
 suddenly because there's so
 so much a part of your daily routine that they change a lot
.
 Well we think of change, sweeping changes like moving to
 another country or becoming a monk or something.
 When in fact it's really the little things, it's about how
 you live your life.
 It's about what you're doing in your day, you know.
 Just living your life differently.
 And so in many ways it's about shaving off these little
 things, it's about some gradual change.
 And that's what the sutta talks about, it talks about these
 little things.
 And so the simile of the quail is actually two similes.
 And so our quote is just a brief snippet from the sutta.
 And there's again the Buddha does both sides, he says, "
Some foolish people,
 when advised to give something small, even small,
 they say, 'What is this small and insignificant matter?
 Why is he putting so much stress on little things?'"
 And the Buddha says, "For such a person, even that little
 thing becomes a real bond for them,
 a real fetter, strong, solid and stout, because of their
 view,
 because of their inability to see their ignorance,
 inability to see the problem with it,
 when the claim is that there is a problem with it."
 And this is really common, you know, this is something that
 we face
 trying to teach Buddhism.
 I had an argument with someone in New York about affection.
 I said, I've met someone and they were asking if I ever
 gave relationship advice.
 And I said, "I don't really often give relationship advice.
 It's something I don't find myself doing."
 I was thinking about it.
 And I said, "Well, sometimes I have people come to me and
 say,
 well, they have a hard time being affectionate."
 And this person was surprised and said, "Well, I would
 think meditation would make you more affectionate."
 And we argued about it.
 So when, I mean, so many things when we talk about giving
 them up,
 about changing the way we live our lives,
 they're met with resistance and even outright rejection.
 It's hard for people to accept and in fact, they will
 outright reject them.
 So they might want to practice meditation, but they'll
 reject the idea of,
 for example, giving up affection, which, you know, actually
 is fine.
 There are many Buddhists who continue to be affectionate.
 I would say though, that through the practice of meditation
,
 I would argue you do become less affectionate, more
 peaceful,
 you know, more content and less clingy and so on.
 This is an example.
 And so for that person, you know, these things that are
 actually quite simple to give up,
 I mean small things as well, especially like giving up
 eating in the evening,
 they become very strong.
 And so he likens this to a rotten vine.
 And he said, "Suppose you have this rotten vine
 and there's a quail that's caught up in the vine.
 And suppose someone came up and said, "Oh, well look at
 that rotten vine.
 That quail should have no problem getting up.
 For that quail, that's not a real fetter."
 But as would they be speaking rightly?
 No.
 No, they would not be speaking rightly because of that qu
ail,
 because of how weak that little quail is, the bird.
 Even that rotten vine, that rotten, dried up rotten creeper
,
 is like iron.
 They said in the same way, a monk who is weak,
 for a monk who is weak, these things, these silly things,
 you know,
 would turn out to be actually quite easy to give up and
 easy to live without.
 And actually the giving up and the doing without leads to
 peace and freedom from suffering,
 leads to happiness.
 But for a weak person, the attachment makes it a strong
 bind, like steel.
 Like the Buddha said, leather and even iron and steel and
 chains,
 these are not strong binds.
 These are weak.
 When you compare them to the bonds of lust and desire,
 and even aversion and delusion, these are the strong bind.
 Which is funny because they're actually not.
 They're actually quite easy to do without and easy.
 But if you're bound to them, they're like, they're stronger
 than steel.
 But as soon as you look and examine and as soon as you're
 mindful,
 it's like they dissolve like a mirage.
 So they weren't even there.
 And so the Buddha said then there's another person
 who doesn't disparage me and crumble and complain when I
 tell.
 And he said it's like an elephant, a great elephant.
 And the great elephant has a rotten creeper tied around its
 neck,
 gets caught up in this rotten creeper.
 If someone were to say, oh, that's a really strong bind,
 that elephant's going to be in trouble.
 Would they be speaking rightly?
 No, they wouldn't.
 For an elephant, even the strongest bind is of little
 consequence,
 let alone a rotten one.
 So much is just a matter of our state of mind.
 Do we want to be free?
 Are we actually seeking to be free?
 We say we want to be happy.
 Are we actually working to become happy?
 Or are we just sitting around waiting for happiness to come
 to us?
 Are we working?
 Are we doing what it takes to strengthen our minds,
 to create the strength and the presence and the mindfulness
 to be happy?
 Or are we just sucking dry the bare bones
 or the bare remains of whatever happiness we have,
 whatever pleasure we can find, like a dog gnawing at a
 bloody bone,
 never satisfied.
 So that's the quote this evening, a little bit of dhamma
 for us all.
 We have some chatter in the chat box.
 Well...
 Hmm...
 How much longer do we have to be able to donate to the
 Rogue Fund?
 It's a good question.
 I would say, I don't know, I have to ask Robin.
 Robin's here.
 Robin's like God for us.
 She does, she dictates so much.
 We have to all praise Robin and thank her.
 We have to garawat.
 We have to...
 And I appreciate her importance because she does a lot for
 us.
 We do have to...
 Well, ordering the robes isn't a big deal.
 In fact, probably we could fudge it.
 Although, the money has to probably go to Thailand at some
 point.
 So we're atheists so we can think of each other as God.
 Now the Buddha said, parents are your God,
 parents are gods for their children, that kind of thing.
 But Robin has much in common with God for many of us, me
 included.
 I don't know if that's weird to say, but it's pretty weird.
 But it just being in terms of someone who does so much for
 us.
 It's quite helpful.
 It's supportive.
 Yeah, so probably before the order gets placed whenever
 that is.
 But there also has to be a money transfer to Thailand, I
 think.
 So maybe even before that.
 The money transfer doesn't take long, right?
 No, I guess the order can be placed before she has the
 money, I think.
 I don't know.
 Robin, you know more about this, I think, than me.
 We're in contact.
 We got a really good person in Thailand as well.
 She's kind of like our Thai goddess.
 She's our God in Thailand.
 God because we depend upon her.
 So as far as Thailand goes, she is Thailand.
 When we need something done in Thailand,
 so Tanya in Bangkok, she takes care of everything.
 Hey, we got 37 people watching.
 That's great.
 You know, to do this every day and have so many people
 still tuning in.
 I've seen a spike.
 I just look now for the first time in a while at YouTube
 analytics.
 It's got this graph of views and it's really gone up in the
 past.
 I don't know.
 The past recently, whatever that is.
 But also I've seen some fairly negative comments, which is,
 you know, the internet, I guess,
 is like that.
 You're always going to have to be fairly thick skinned
 because you get the great comments,
 right?
 You know, you've changed their lives and they're so great
 and I appreciate so much.
 And then you get people who will be opposite.
 Just a shame really, but that's the world.
 It's actually kind of informative.
 It reminds us that the world has both good and evil.
 For those who despair, there's no reason to despair because
 there's so many good things
 in the world.
 But at the same time, we have this reminder that the world
 has these negative aspects
 to it.
 There's this phenomenon where when you post something on
 the internet, you'll get a hundred
 positive comments or a thousand positive comments.
 But if you get one negative comment, you forget about all
 the positive ones.
 If you get a thousand, you're like, oh, that's nice.
 But as soon as you get one negative, like the internet
 hates me.
 The internet's a fairly good, if you look at it objectively
, it's a fairly good measure
 of humanity.
 It's an incredible thing, not the internet.
 What a world we live in.
 Anyway, no questions?
 No questions.
 What is Simon getting an error?
 No.
 I get an error on the link.
 Huh.
 Oh, you missed something, I think.
 Yeah, there's a dot.
 That period on the end doesn't work.
 You put a period on the end of the link and our site picks
 it up as a part of the link.
 So just delete the period on the end or copy and paste the
 link without the period.
 Circular.
 Yeah, the Baddi to Samupada is an interesting study.
 And this is where a lot of people argue.
 There's argument about this.
 So some people look at that.
 What that means is that in this life, you've got the body
 and the mind.
 Name and form is a really bad translation.
 Nama Rupa.
 Rupa is physical.
 Nama is mental.
 So the mental and the physical create consciousness.
 Nama Rupa is one thing.
 It's the body and mind complex.
 It creates consciousness.
 So the Nama would be the mind and the Rupa is like the
 seeing, sorry, the eye, the light
 and the eye.
 When these come together, there's a consciousness.
 There's an experience of consciousness.
 But that consciousness then gives rise to Nama Rupa.
 And so it goes, so the consciousness is what is aware of
 the physical and the mental.
 So when you're aware, you're aware of the seeing.
 So it goes moment to moment.
 And that's in this life.
 That explanation of the teachers of Baddi is the one life
 explanation.
 So many people believe that's the correct interpretation,
 but it's not the only explanation.
 If in other places, the Buddha says it goes backwards.
 You know, vinyana, pañjaya, Nama Rupa.
 But vinyana comes from, which is consciousness,
 consciousness comes from Sankara.
 And Sankaras are, well, they're also Nama Rupa, but they're
 in the past.
 The good and bad deeds that we've done is up until the
 moment of death in a past life
 where we're born again.
 So people talk about this past life, the three life version
 of paticca samuppada.
 But the Nama Rupa, pañjaya, vinyana, vinyana, pañjaya, N
ama Rupa, that's circular indeed,
 because in every moment it's happening in the cycle.
 The sequence occurs again and again and again.
 I mean, and again, Nama Rupa, pañjaya, vinyana is just a
 gross simplification of what's actually
 happening.
 If you want the orthodox understanding of what's really
 happening, you should learn
 Abhidhamma, read Bhikkhu Bodhi's comprehensive translation
 and explanation on the Abhidhammata
 Sangha, the comprehensive manual of Abhidhamma.
 I really want to adopt Buddhism, but coming from a very
 Catholic family, I'm pretty worried.
 Do you have any advice?
 Read my booklet.
 The booklet should be at the top.
 Is it linked at the top still?
 Yes.
 At the top of this page, there's the word near the top of
 the page.
 It says this booklet.
 Go there, read that.
 If you want, I can send you a copy.
 I can send a copy, right?
 If you send me your mailing address.
 And then start to learn how to be mindful of worry.
 So Buddhism is all about seeing things as they are.
 If you're worried, then focus on the worry and say worried.
 You would actually meditate on the worry.
 And then things like, because when you're meditating,
 things like Buddhist and Catholic,
 they don't exist.
 So you're able to free yourself from worry, which is really
 the Buddhist practice.
 But definitely read my booklet if you can.
 It sounds kind of weird saying that, like I'm self-prom
oting.
 But I don't think the book is incredibly well written or
 anything.
 It's just trying to put in text a lot of the things that I
 was taught and learned and heard
 over and over again.
 And I thought, wow, this should just go in a book.
 They have books in Thailand.
 I'm not going to lie about these, but I think it's a good
 order the way it's written down.
 It's actually quite backwards.
 The last chapter in the book talks about sila, morality,
 which is the first thing.
 But it doesn't have to be.
 And when you're talking to someone who's new to Buddhism,
 new to meditation, it doesn't
 need to be the first thing.
 They're interested in meditation.
 So you teach them meditation.
 The details come later about what you have to do to sustain
 your meditation.
 So we work from inside out trying to explain to people.
 Because if you start with too many details, they have a
 hard time finding the core and
 seeing the bigger picture.
 But if you start, what is the core, then everything else
 builds on that.
 And morality at the outside can be the last thing you teach
.
 Otherwise people get turned off.
 They don't understand why morality, why precepts, why the
 focus on all these rules.
 He realized destroyed his birth, et cetera.
 How would someone know his birth is destroyed when you know
 you didn't choose to be born?
 Why can't you be forced to be born again, just like this
 time?
 Well, the Buddha knew.
 The Buddha said to have had profound understanding of
 really everything.
 So there's no question.
 It's kind of whether you believe that the Buddha actually
 had that kind of knowledge,
 which is--
 I know it's a bit of a cop-out for me to say that, but his
 knowledge was just so profound.
 But I don't think that's necessary.
 Because an Arahant, which is someone who becomes
 enlightened following the Buddha, also knows
 destroyed his birth, kinajati, destroyed his birth.
 They have freed themselves, and they're clear in their mind
 that they have freed themselves
 from all desire, all attachment.
 And without any attachment, this is what you see in the
 meditation.
 You're learning the nature of reality.
 And what you're seeing is how things work.
 Once you see how things work, there's no doubt in your mind
 of where your mind rests in that.
 And that when your mind is completely freed from ignorance
 and delusion, and thereby greed
 and anger, that you should ever be born again.
 There's no doubt that this could ever happen.
 It's something we can't fathom because we're not there yet.
 If you're not there, you haven't experienced this for
 yourself.
 You've experienced that clarity.
 You still have this-- the mind is a mystery, right?
 But the claim being made is that the mind need not stay a
 mystery, that you can unravel
 the mysteries of the mind.
 Mystery is a reality.
 You can come to an understanding of reality that is
 complete.
 So there's no being forced to be born again.
 You yourself give rise to birth because of desire in a word
.
 I want to jump into Buddhism.
 Where is a good place to start?
 So read my booklet.
 That's the first.
 If you read anything by Mahasi Sayadaw, that's great.
 And then come and do a course.
 We have online courses here, so you can do a meditation
 course with me online.
 You meet once a week, and you do an hour at least of
 meditation a day.
 I don't know.
 I guess I'm kind of skirting it because probably the answer
 you want is what books should I
 read, what text should I read.
 So on that side, don't do any of that without meditating.
 And I would argue, because you came to me, I came to us,
 meditate the way we meditate.
 So meditate this way according to this booklet.
 Put aside any other meditation you've practiced.
 And then while you're doing that, throughout your life,
 start to read the teachings of
 the Buddha.
 Start to read what we call the tepidaka.
 You can read the translations by Bhikkhu Bodhi.
 They're very good translations from wisdom publications of
 the middle length discourses
 and the long discourses.
 Those two are really good.
 And then there's two more.
 There's the connected discourses and the numerical disc
ourses, which are significantly larger
 and more diverse.
 How do we change our bad habits without forcing ourselves
 over pressing?
 We see them clearly.
 Once you see that they're bad habits, you'll give them up
 naturally.
 So if you haven't read my booklet, I encourage you to do
 that.
 If you have, start meditating and you'll start to see your
 bad habits more clearly.
 Once you see them clearly, they'll change.
 And again, it's like this quote today.
 It's piece by piece, layer by layer, bit by bit.
 It's gradual.
 It's not, you go to do a course and suddenly poof, you're
 free or whoops, I just lost on
 my desires.
 You slowly change.
 You start to see things clear.
 Well, Al, I'm happy to help.
 That's interesting.
 As you're reading, if you have questions, feel free to post
 them here.
 Try to be here every night, most nights.
 Yeah, and we got a good group here.
 People helpfully posting links and stuff.
 So this is really a great little site.
 I mean, it's ugly, I know, but if I can coordinate, maybe
 next month, we can start working on
 transferring this to a more proper system, proper, what do
 you call proper framework,
 so that this website will be more professional.
 Because we had someone really working on it and he's kind
 of put it on hold, but he's
 still in the wings waiting to help.
 How can meditation be used to free oneself from guilt?
 Well, ultimately, because you stop doing things that make
 you feel guilty, but also because
 you learn to be more objective, you learn to be more
 understanding and less judgmental,
 because guilt ultimately comes from judgment, right?
 You judge yourself and self judgment.
 And you become to be more accepting, not accepting, but
 more, what's the word, not accepting,
 but what was the word I just used?
 To be more objective anyway, less judgmental.
 Because you see that judgment is cause of suffering, right?
 And you see that it's not really you that's doing this,
 these are just habits, they're
 like echoes, echoes of bad karma.
 Bad things you've done in the past have created these
 echoes and these ripples and reverberation.
 These waves that come into the future have spilled over
 into the present.
 And so you become more tolerant, that's the word.
 And you learn to tolerate your bad habits and your bad.
 I mean, maybe that it's difficult because it's not like you
've come to say, "Oh, well,
 that's just who I am."
 No, you stop reacting and you stop getting caught up in
 your bad habits.
 And as a result, they become weaker when you stop feeding
 them.
 Feeding them either by saying, "Yes, these are good," or by
 saying, "No, these are bad.
 I'm such a bad person."
 You know, getting angry, getting upset.
 As you just watch them with dispassion and watch them
 objectively, they start to shrivel
 up and die because they have no fuel.
 We got a bunch of questions tonight.
 When does the weekly meditation meet?
 There's no weekly meditation meet, there's slots.
 If you click on meet up at the top of the page, you'll see
 we've got slots.
 And some people have taken slots and read through the
 directions there.
 We meet using video conference online.
 Yeah.
 An enlightened person may not experience mental suffering
 anymore, but still experiences physical
 suffering.
 Still hurts, no?
 You know, you really should practice meditation in this
 tradition because it would probably
 answer a lot of these questions, well, some of these
 questions for you.
 That's why I strongly suggest that if you're reading the
 Buddha's teaching, you should
 also be meditating.
 Don't go out and read all those books until you've started
 on a meditation practice because
 they're supposed to be meditation teachings.
 Physical suffering is technically suffering, but it's only
 unpleasant if you find it, so
 if you get displeased by it.
 And yeah, physical suffering is inevitable as long as you
 have a body.
 For anyone practicing meditation in a busy college
 lifestyle, well, start with 15 minutes
 a day, 20 minutes a day.
 Anyone can do that.
 Do it in between studying.
 Study some, then do sitting meditation or walking
 meditation.
 So that I do, so that I used to do when I was first
 starting out.
 And when you have break, when you have a break, you'll have
 summer break or winter break or
 fall break or whatever break.
 Do some meditation then, and then you ramp it up a bit.
 But do some every day.
 There's no excuse.
 We have 24 hours in the day.
 Anyone can do 20 minutes, and then if you do 20, ramp it up
 to 30.
 To do a course, we want you to do an hour a day.
 So that's the challenge.
 Can you do a half an hour in the morning, a half an hour in
 the evening?
 If you can, then you can do a meditation course.
 It's a start.
 By the end of the course, we try to have you up to two
 hours a day.
 So just if you're interested in that.
 God is teaching on prosperity and by Bhikkhu, Bhasnagoda, R
ahu, that I've never heard of.
 His translation seemed rare.
 Are you sure that's the right word?
 For a translation to be rare, it means it's hard to find.
 Either that or the implication could be that it's not like
 other translations.
 Because maybe the word.
 Robes just keep going up, right?
 This is for up to $2,500 for robes, which is how many robes
?
 That's about 20 cents.
 Last year or the year before, we raised, we got over 100,
 right?
 No, not 100.
 100 times 100.
 Yeah.
 But over 100.
 When someone was saying he probably has a jantung, probably
 has a warehouse full of robes.
 He may, but you've ever seen pictures of these ceremonies?
 Like on his birthday every year, he gives out how many sets
?
 It must be over 1,000 sets of robes.
 And just like it'll be 1,000, maybe not 1,000, but hundreds
.
 Probably 1,000 sets of robes and gifts, just big
 extravagant gifts on his birthday.
 It's a huge giving affair.
 Ananda got 500 robes from the king.
 The king gave 500 robe sets to one of his people and said,
 go and distribute these to
 the monks.
 And the guy was, I think, lazy.
 So he just went and gave them all to Ananda.
 I think he really appreciated Ananda.
 So he just gave them all to Ananda.
 And the king found out about this and he was really upset.
 He went to Ananda and said, what kind of a monk are you
 that you accept 500 sets of robes?
 How in the world could you possibly use those?
 And he said, well, I gave them all to other monks.
 Monks whose robes were tattered.
 We give them out to them.
 He said, well, what would they do with their old robes?
 He said, well, they cut them up and they make them into the
 lower robe, which is smaller.
 And they said, well, what do they do with their own lower
 robes?
 Oh, they turn them into bathing cloths.
 And what do they do with their own bathing cloths?
 And they use them for this until finally they break up
 little patches into thread.
 And then they use that thread to sew their robes.
 What would they do with their old robe?
 Their old thread.
 They mix it in with clay and they make bricks out of it to
 build the kutti.
 They won't go to waste, I guarantee.
 Giving robes to someone like Ajahn Tong, there's no going
 to waste.
 He won't wear maybe any of them, but they won't go to waste
.
 It's a great way to support him and his teachings.
 He has hundreds of centers around Thailand.
 There were not hundreds.
 There are lots of centers around Thailand that he supports.
 And monks by the hundreds, at least.
 Hmm.
 I was involved in the Iraq war, became a drug addict, med
itating for many years, tapering
 off the pain medication.
 Meditation even helped me in the war.
 Sometimes the pain of the full body withdrawal is agonizing
.
 No, I shouldn't beat myself up over this.
 And the advice or anecdotes, yeah, absolutely.
 I mean, it probably is a very valuable experience.
 You know, it's not about succeeding, which is probably
 something you're aware of.
 You're not going to win and you're going to fail more than
 you win.
 But failure doesn't.
 Failure is just a failure to break out of it.
 That can happen again and again.
 You don't have to concern yourself so much with that.
 You know, like when you scream or when the pain is
 overwhelming and you go back to the
 drugs or something.
 You just have to chip away at it.
 You have to set your mind in the right direction.
 It's like this quote.
 These things are only strong bind.
 The real strong bind is the view and the opinion.
 Where is your mind state?
 Your mindset.
 Is your mind set on freeing yourself or is it set on
 reacting?
 Set on clinging.
 And so it's not going to be perfect.
 You're not going to be able to meditate like a Buddha or
 something.
 Meditation is a lot like a war zone.
 I've said this many times.
 You can have all the theory in the world and all the
 training in the world.
 But when you get to the war zone, it's a war zone.
 All the rules go out the window or many rules.
 You have to improvise.
 Meditation is like that.
 You sit there and all the theory is fine, but my mind is
 something totally different.
 Your mind is real.
 And so then you really have to actualize it.
 And so it's going to be messed up.
 It's not going to be clear cut.
 It's going to be easy.
 If you've read my booklet, please, please.
 Well, if you haven't read my booklet, please do.
 And use that technique.
 If you have pain, just say to yourself, "Pain.
 Remind yourself this is pain.
 That's all it is.
 It's not bad.
 It's not a problem."
 And teach yourself that.
 And over time, it really can't help.
 Pain is a great teacher.
 There are monks who...
 There was this monk who broke his legs.
 He was going to be killed.
 These guys wanted to kill him because they had been hired
 to get rid of him for an inheritance
 because he was the heir to all this money.
 So he wanted to kill him.
 And he said, "Oh, no.
 Come back."
 He said, "Look, I'm not done meditating.
 Come back in the morning.
 You can kill me then."
 And they refused.
 They said, "Yeah, right.
 You'll run away."
 And so he picked up a rock and he broke both of his legs.
 And he said, "There.
 Does that reassure you that I won't run?"
 So they left him alone for the night.
 And he meditated on the pain and became an arahant.
 And he meditated on the excruciating pain.
 The Buddha practiced many meditations, even holding his
 breath.
 The holding his breath was wrong meditation.
 The difference between us and the Buddha is we don't have
 to do all the wrong ways because
 we have the Buddha to teach us the right way.
 Perhaps for increasing the quality of meditation along with
 the length.
 Yeah, concentrating.
 What we're trying to see in meditation is impermanent
 suffering and non-self.
 We're trying to see that we can't control things.
 And so that part of it is going to be letting go.
 And as you start to let go and loosen up, you'll find that
 ironically or paradoxically,
 it becomes easier to concentrate.
 It becomes natural.
 You trying to force yourself to concentrate is antithetical
 to your purpose because it's
 ignorant.
 It's ignorant of the fact that you're seeing that you can't
 do it.
 You can't increase the quality of meditation.
 You have to stop trying in this sense and just start seeing
.
 And as you see, things will improve naturally.
 With what kind of mind or thoughts should we donate?
 There's lots of different minds with which you can donate.
 The best one is to think that it will support you in your
 practice ultimately.
 The condensed version of Buddha gives several examples of
 people.
 Some people give because other people give and they say, "
Well, why is people think this
 is a good thing?
 Why is people saying giving is good?"
 But the best reason, some people give thinking, "This will
 be a support in my future life.
 I'll be born in heaven as a result of this," that kind of
 thing.
 "I'll be rich because I'm giving."
 Some people give to others thinking they'll get it in
 return.
 This person will like me that kind of thing.
 The best is thinking this will be a support for the mind.
 This will make it easier to progress spiritually.
 We give with that idea.
 We give that this makes me a better person kind of thing.
 Not quite like that.
 Giving improves.
 Giving is a support for the path.
 That's the best.
 That's the purest.
 Okay.
 That's all I'm going to say.
 Good night.
 Thank you all.
 That was a lot of good questions.
 But is it Friday?
 Is it because it's Friday night?
 There's so many people, so many questions.
 Maybe it's picking up.
 If you have more questions, come on back tomorrow.
 We'll be here.
 We should be here same time, same place.
 Have a good night, everyone.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
 Thank you.
