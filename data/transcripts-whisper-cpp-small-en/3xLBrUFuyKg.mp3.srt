1
00:00:00,000 --> 00:00:05,600
 Hi. So in this video I will be talking about some of the

2
00:00:05,600 --> 00:00:07,920
 important qualities of meditation,

3
00:00:07,920 --> 00:00:10,330
 which we need to keep in mind during the time that we do

4
00:00:10,330 --> 00:00:12,280
 the walking or the sitting meditation.

5
00:00:12,280 --> 00:00:14,390
 So it's not enough for us to say that we're doing the

6
00:00:14,390 --> 00:00:15,440
 walking, we're doing the sitting

7
00:00:15,440 --> 00:00:19,320
 on a regular basis, and expect that we should all receive

8
00:00:19,320 --> 00:00:21,840
 the same benefits from the practice

9
00:00:21,840 --> 00:00:24,750
 no matter what's going on in our minds or how we're

10
00:00:24,750 --> 00:00:27,040
 carrying out the practice. Our practice

11
00:00:27,040 --> 00:00:30,940
 has to have certain important qualities to it in order for

12
00:00:30,940 --> 00:00:33,240
 us to consider that our practice

13
00:00:33,240 --> 00:00:37,010
 is a success, in order for us to really get something out

14
00:00:37,010 --> 00:00:39,360
 of it. The first important quality

15
00:00:39,360 --> 00:00:41,760
 of meditation practice is that we have to make the

16
00:00:41,760 --> 00:00:43,640
 acknowledgement in the present moment.

17
00:00:43,640 --> 00:00:46,790
 Our mind should always be in the here and now. We cannot

18
00:00:46,790 --> 00:00:48,240
 let our minds fall into the

19
00:00:48,240 --> 00:00:51,510
 past or go ahead into the future. We shouldn't think about

20
00:00:51,510 --> 00:00:53,240
 how many more minutes are left

21
00:00:53,240 --> 00:00:55,800
 in the practice or how many minutes we've practiced or so

22
00:00:55,800 --> 00:00:57,180
 on. Our mind should always

23
00:00:57,180 --> 00:01:01,580
 be in the present moment. We should also be continuously in

24
00:01:01,580 --> 00:01:05,040
 the present moment. So not

25
00:01:05,040 --> 00:01:08,710
 only practicing this moment or this moment, but that we are

26
00:01:08,710 --> 00:01:10,200
 practicing continuously from

27
00:01:10,200 --> 00:01:13,760
 one moment to the next. At the moment when we finish the

28
00:01:13,760 --> 00:01:15,440
 walking meditation, we should

29
00:01:15,440 --> 00:01:19,290
 try to continue our awareness and our acknowledgement of

30
00:01:19,290 --> 00:01:22,760
 the present moment into the sitting meditation.

31
00:01:22,760 --> 00:01:25,380
 Once we finish the sitting meditation, when we're going to

32
00:01:25,380 --> 00:01:26,880
 get up and go on with our lives,

33
00:01:26,880 --> 00:01:30,730
 we should try to continue into our lives, carry on

34
00:01:30,730 --> 00:01:32,760
 continuously. During the time that

35
00:01:32,760 --> 00:01:34,810
 we're doing the walking meditation, we're the same. We

36
00:01:34,810 --> 00:01:35,880
 should try to keep our minds

37
00:01:35,880 --> 00:01:39,020
 in the present moment through the whole of the practice.

38
00:01:39,020 --> 00:01:40,600
 When we do the sitting, trying

39
00:01:40,600 --> 00:01:43,850
 to keep it on the rising and falling again and again

40
00:01:43,850 --> 00:01:45,800
 continuously. Just like the rain

41
00:01:45,800 --> 00:01:49,330
 falling, it is said that meditation practice is like rain

42
00:01:49,330 --> 00:01:51,520
 falling. Every moment that we're

43
00:01:51,520 --> 00:01:54,880
 aware is like one drop of rain. And though it may not seem

44
00:01:54,880 --> 00:01:56,600
 like that much, once we're

45
00:01:56,600 --> 00:02:00,500
 mindful again and again and aware again and again, just

46
00:02:00,500 --> 00:02:02,760
 like rain falling, it can flood

47
00:02:02,760 --> 00:02:05,900
 and create a huge flood or fill up a lake. In the same way

48
00:02:05,900 --> 00:02:07,760
 our awareness again and again

49
00:02:07,760 --> 00:02:11,330
 in one moment after one moment can create very strong

50
00:02:11,330 --> 00:02:13,520
 states of concentration based

51
00:02:13,520 --> 00:02:17,440
 on the present moment so that we're very clearly aware. And

52
00:02:17,440 --> 00:02:19,620
 we create a great understanding

53
00:02:19,620 --> 00:02:25,590
 and clarity of mind in regards to reality. Another

54
00:02:25,590 --> 00:02:28,280
 important point is the actual acknowledgement

55
00:02:28,280 --> 00:02:32,370
 or the quality of the acknowledgement. We have to first of

56
00:02:32,370 --> 00:02:33,800
 all have a certain amount

57
00:02:33,800 --> 00:02:36,620
 of effort. It takes effort for us to do this again and

58
00:02:36,620 --> 00:02:38,440
 again and we have to acknowledge

59
00:02:38,440 --> 00:02:41,730
 this. And we can't just let ourselves say rising, falling,

60
00:02:41,730 --> 00:02:43,240
 rising, falling and let the

61
00:02:43,240 --> 00:02:46,730
 mind drift. We have to push the mind and keep the mind with

62
00:02:46,730 --> 00:02:48,560
 the present moment, with the

63
00:02:48,560 --> 00:02:51,560
 object as it arises, whatever object that is. So if it's

64
00:02:51,560 --> 00:02:53,040
 the rising and the falling

65
00:02:53,040 --> 00:02:56,430
 and the beginning, staying with it again and again, sending

66
00:02:56,430 --> 00:02:58,200
 the mind out instead of keeping

67
00:02:58,200 --> 00:03:01,080
 the mind up here or at the mouth, sending the mind out to

68
00:03:01,080 --> 00:03:02,600
 the object again and again

69
00:03:02,600 --> 00:03:06,340
 and again and keeping the mind very, sort of a strong state

70
00:03:06,340 --> 00:03:08,000
 of mind, keeping our mind

71
00:03:08,000 --> 00:03:14,690
 strong and focused on the present moment. Once we do this

72
00:03:14,690 --> 00:03:15,840
 then we'll begin to know,

73
00:03:15,840 --> 00:03:17,900
 become aware of the object. And this is another important

74
00:03:17,900 --> 00:03:19,880
 thing that we're actually aware

75
00:03:19,880 --> 00:03:22,440
 of the object as it is occurring. So that we're not just

76
00:03:22,440 --> 00:03:23,760
 saying rising, falling, but

77
00:03:23,760 --> 00:03:27,550
 once we push our minds to the object that we actually know

78
00:03:27,550 --> 00:03:29,360
 from the beginning to the

79
00:03:29,360 --> 00:03:33,140
 end of the motion. If it's pain that we actually are aware

80
00:03:33,140 --> 00:03:35,620
 of the pain. If it's thoughts that

81
00:03:35,620 --> 00:03:39,440
 we're actually aware of the thought. Once we become aware

82
00:03:39,440 --> 00:03:40,360
 of the thought then we can

83
00:03:40,360 --> 00:03:42,870
 make the acknowledgement. And this is of course the most

84
00:03:42,870 --> 00:03:44,300
 important part is that actually we

85
00:03:44,300 --> 00:03:48,400
 grasp and we fix the mind on that awareness, not letting it

86
00:03:48,400 --> 00:03:50,320
 continue on into liking or

87
00:03:50,320 --> 00:03:55,990
 disliking or creating all sorts of mental activity or

88
00:03:55,990 --> 00:03:59,320
 judgments about the object. The

89
00:03:59,320 --> 00:04:03,710
 final important fundamental quality of practice is called

90
00:04:03,710 --> 00:04:07,120
 balancing the faculties. So it's

91
00:04:07,120 --> 00:04:10,370
 recognized that all people have certain faculties in their

92
00:04:10,370 --> 00:04:12,380
 mind. These are qualities of mind

93
00:04:12,380 --> 00:04:17,760
 which are very beneficial. Altogether there are five. These

94
00:04:17,760 --> 00:04:20,440
 are the confidence, effort,

95
00:04:20,440 --> 00:04:24,490
 mindfulness, concentration and wisdom. And these have to,

96
00:04:24,490 --> 00:04:26,360
 we say these have to be balanced

97
00:04:26,360 --> 00:04:30,000
 with each other. For instance some people might have strong

98
00:04:30,000 --> 00:04:31,400
 confidence but they may

99
00:04:31,400 --> 00:04:35,380
 have low wisdom. This means that they have what we call

100
00:04:35,380 --> 00:04:38,000
 blind faith. They believe things

101
00:04:38,000 --> 00:04:42,510
 simply because of this is what they've been told or because

102
00:04:42,510 --> 00:04:44,720
 of some desire to believe.

103
00:04:44,720 --> 00:04:49,060
 Not because of any rational understanding. Some people have

104
00:04:49,060 --> 00:04:51,040
 strong wisdom but low faith

105
00:04:51,040 --> 00:04:55,270
 so they doubt everything. And they refuse to believe

106
00:04:55,270 --> 00:04:57,080
 anything even from a respected

107
00:04:57,080 --> 00:04:59,670
 authority or so on. Or they refuse to believe anything

108
00:04:59,670 --> 00:05:01,400
 because they themselves have never

109
00:05:01,400 --> 00:05:05,530
 experienced it. Because they've never undertaken the

110
00:05:05,530 --> 00:05:07,720
 meditation practice. Some people have

111
00:05:07,720 --> 00:05:12,290
 strong effort but weak concentration. In this case the mind

112
00:05:12,290 --> 00:05:14,600
 becomes distracted where the

113
00:05:14,600 --> 00:05:18,100
 mind is not focused. It can't focus on anything. Some

114
00:05:18,100 --> 00:05:20,560
 people have strong concentration but

115
00:05:20,560 --> 00:05:25,290
 weak effort. And this what makes people lazy or drowsy

116
00:05:25,290 --> 00:05:28,160
 keeps us from sending the mind on

117
00:05:28,160 --> 00:05:31,720
 and on and on keeping up with the present moment. And we

118
00:05:31,720 --> 00:05:33,760
 find ourselves getting drowsy

119
00:05:33,760 --> 00:05:38,450
 and falling asleep. The way we balance these we balance

120
00:05:38,450 --> 00:05:42,920
 confidence with wisdom and effort

121
00:05:42,920 --> 00:05:46,410
 with concentration. We use mindfulness. We use this clear

122
00:05:46,410 --> 00:05:48,200
 thought. The effort sending

123
00:05:48,200 --> 00:05:51,340
 the mind to the object, the awareness of the object, the

124
00:05:51,340 --> 00:05:53,000
 focusing on the object and the

125
00:05:53,000 --> 00:05:55,890
 clear knowing of the object. These are all of the mental

126
00:05:55,890 --> 00:05:58,040
 faculties put into one. Confidence,

127
00:05:58,040 --> 00:06:03,180
 effort, mindfulness, concentration and wisdom. Once we've

128
00:06:03,180 --> 00:06:05,840
 balanced the four outside faculties

129
00:06:05,840 --> 00:06:08,880
 with the faculty of mindfulness or the clear awareness they

130
00:06:08,880 --> 00:06:10,360
 will work together and they

131
00:06:10,360 --> 00:06:13,020
 will create a very strong state of mind where we have

132
00:06:13,020 --> 00:06:14,960
 strong confidence, we have strong

133
00:06:14,960 --> 00:06:19,120
 effort. We are clearly mindful and we have strong

134
00:06:19,120 --> 00:06:22,080
 concentration and we have very sure

135
00:06:22,080 --> 00:06:25,250
 wisdom because we see for ourselves and we understand for

136
00:06:25,250 --> 00:06:26,800
 ourselves without the need

137
00:06:26,800 --> 00:06:31,010
 to rely on someone else. At that time this is when the mind

138
00:06:31,010 --> 00:06:32,920
 is able to overcome states

139
00:06:32,920 --> 00:06:36,890
 of suffering, overcome states of depression, is able to

140
00:06:36,890 --> 00:06:39,080
 like a strong man is able to bend

141
00:06:39,080 --> 00:06:44,010
 an iron bar. We also can bend and shape and mold our minds

142
00:06:44,010 --> 00:06:46,420
 and bring our minds back to

143
00:06:46,420 --> 00:06:50,300
 a state of peace and happiness overcoming all sorts of

144
00:06:50,300 --> 00:06:53,080
 unpleasant states. This is a

145
00:06:53,080 --> 00:06:57,300
 basic understanding of some of the fundamental qualities of

146
00:06:57,300 --> 00:06:59,480
 meditation which we need to keep

147
00:06:59,480 --> 00:07:02,850
 in mind. We need to be in the present moment and practice

148
00:07:02,850 --> 00:07:04,840
 continuously. We need to make

149
00:07:04,840 --> 00:07:08,400
 the clear thought and we can't just say to ourselves rising

150
00:07:08,400 --> 00:07:10,420
, falling, we have to actually

151
00:07:10,420 --> 00:07:14,290
 know what's going on in the present moment and we have to

152
00:07:14,290 --> 00:07:16,200
 balance the faculties. This

153
00:07:16,200 --> 00:07:20,630
 is another in the series on how to meditate and this is

154
00:07:20,630 --> 00:07:23,160
 sort of an addition to the actual

155
00:07:23,160 --> 00:07:27,580
 technique of meditation which is meant to give a better,

156
00:07:27,580 --> 00:07:29,820
 stronger quality and a greater

157
00:07:29,820 --> 00:07:34,030
 benefit to the meditation practice. So I hope that through

158
00:07:34,030 --> 00:07:36,180
 this you are able to find peace

159
00:07:36,180 --> 00:07:40,000
 and happiness and freedom from suffering. Thank you again

160
00:07:40,000 --> 00:07:41,120
 for tuning in and all the

161
00:07:41,120 --> 00:07:41,400
 best.

162
00:07:41,400 --> 00:07:43,400
 Thank you.

