1
00:00:00,000 --> 00:00:04,880
 Noting in the head. Question. The instructions say that the

2
00:00:04,880 --> 00:00:08,320
 mantra should not be at the mouth or in the head.

3
00:00:08,320 --> 00:00:11,640
 Whenever I note something, it appears in my head as a

4
00:00:11,640 --> 00:00:16,840
 thought. When I am tired, I am thinking tired, tired, tired

5
00:00:16,840 --> 00:00:19,600
. Is this important?

6
00:00:19,600 --> 00:00:23,990
 Answer. When you get better at noting that thought is with

7
00:00:23,990 --> 00:00:27,200
 the object, the mantra being at the head is usually a

8
00:00:27,200 --> 00:00:28,960
 product of being accustomed to

9
00:00:28,960 --> 00:00:32,960
 conceptual thought. When given this exercise, the untrained

10
00:00:32,960 --> 00:00:36,810
 mind's only way of implementing it is to create it as a

11
00:00:36,810 --> 00:00:38,560
 thought in the head.

12
00:00:38,560 --> 00:00:42,440
 When I started, I was actually seeing the words like they

13
00:00:42,440 --> 00:00:46,230
 were pasted onto the inside of my forehead. I was getting

14
00:00:46,230 --> 00:00:47,840
 headaches due to this.

15
00:00:47,840 --> 00:00:51,220
 Part of why people think it is in the head is because in

16
00:00:51,220 --> 00:00:54,640
 the beginning, they often get tension in the head from

17
00:00:54,640 --> 00:00:57,520
 forcing themselves to be with the object.

18
00:00:57,520 --> 00:01:01,140
 So they confuse that with the actual acknowledgement. They

19
00:01:01,140 --> 00:01:04,800
 think, "I am acknowledging in my head," but they are not.

20
00:01:04,800 --> 00:01:08,500
 Their head is just giving rise to tension as a result of

21
00:01:08,500 --> 00:01:12,320
 the person's tense, forceful, controlling mind state.

22
00:01:12,320 --> 00:01:16,150
 This is just a matter of a lack of proficiency. Once you

23
00:01:16,150 --> 00:01:19,360
 become proficient in the practice, the thoughts themselves

24
00:01:19,360 --> 00:01:23,200
 become natural and a part of the experience.

25
00:01:23,200 --> 00:01:27,540
 They are just experiencing reality as it is, as through the

26
00:01:27,540 --> 00:01:30,960
 words it says in the foot of the stomach itself.

27
00:01:30,960 --> 00:01:34,140
 The only time it should be in the head is when you are

28
00:01:34,140 --> 00:01:39,280
 acknowledging pain, aching, tension, or feelings in the

29
00:01:39,280 --> 00:01:39,600
 head.

