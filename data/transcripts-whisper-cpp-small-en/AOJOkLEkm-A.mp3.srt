1
00:00:00,000 --> 00:00:14,720
 Good evening everyone.

2
00:00:14,720 --> 00:00:25,840
 Broadcasting Live, February 12th.

3
00:00:25,840 --> 00:00:47,200
 We are broadcasting a few minutes early.

4
00:00:47,200 --> 00:01:04,960
 Today's quote is about the senses.

5
00:01:04,960 --> 00:01:06,360
 It's a good quote.

6
00:01:06,360 --> 00:01:09,720
 The translation as always is...

7
00:01:09,720 --> 00:01:12,360
 The translation is really fine.

8
00:01:12,360 --> 00:01:16,120
 It's just being someone who knows Pali, you gotta wonder

9
00:01:16,120 --> 00:01:16,840
 what he means by

10
00:01:16,840 --> 00:01:17,840
 unruffled.

11
00:01:17,840 --> 00:01:43,520
 Ajatang ambya seka sukha pati samyam.

12
00:01:43,520 --> 00:01:48,520
 Unsullied.

13
00:01:48,520 --> 00:01:54,080
 I don't know about the whole unruffled thing.

14
00:01:54,080 --> 00:01:55,080
 Happiness of being unruffled.

15
00:01:55,080 --> 00:01:57,760
 I think it's the happiness that is unsullied.

16
00:01:57,760 --> 00:01:59,960
 It's an unsullied happiness.

17
00:01:59,960 --> 00:02:03,920
 That's what Bhikkhu Bodhi translates.

18
00:02:03,920 --> 00:02:06,160
 So we'll go with Bhikkhu Bodhi's translation.

19
00:02:06,160 --> 00:02:13,390
 When seeing a form with the eye he does not grasp but it's

20
00:02:13,390 --> 00:02:14,880
 signs and features.

21
00:02:14,880 --> 00:02:19,880
 This is an important quote that I often bring up.

22
00:02:19,880 --> 00:02:31,760
 Nanimita gahi nana nubhianjna gahi.

23
00:02:31,760 --> 00:02:57,880
 So what does this mean?

24
00:02:57,880 --> 00:03:04,930
 It's actually a criticism that we get from time to time

25
00:03:04,930 --> 00:03:07,120
 about using a mantra, using a

26
00:03:07,120 --> 00:03:13,200
 word to remind ourselves of the experience because it feels

27
00:03:13,200 --> 00:03:14,060
 like you're not really getting

28
00:03:14,060 --> 00:03:17,080
 deep into the experience.

29
00:03:17,080 --> 00:03:21,560
 But when you see something and you see seeing, seeing it,

30
00:03:21,560 --> 00:03:23,080
 it actually stops you from getting

31
00:03:23,080 --> 00:03:28,640
 deep into it, from actually experiencing it.

32
00:03:28,640 --> 00:03:31,000
 And it's a criticism by other Buddhists who think that,

33
00:03:31,000 --> 00:03:32,640
 well, you can't really understand

34
00:03:32,640 --> 00:03:38,860
 something unless you get deep into it and the words get in

35
00:03:38,860 --> 00:03:39,720
 the way.

36
00:03:39,720 --> 00:03:42,960
 The words are designed to get in the way and that's based

37
00:03:42,960 --> 00:03:44,880
 on what the Buddha is saying

38
00:03:44,880 --> 00:03:48,320
 here in the Majimuni Caya.

39
00:03:48,320 --> 00:03:53,110
 So Nanimita gahi, when seeing a form with the eye, one

40
00:03:53,110 --> 00:03:55,120
 doesn't grasp at the sign.

41
00:03:55,120 --> 00:03:57,400
 And I've talked about this before.

42
00:03:57,400 --> 00:04:03,600
 We had this a couple of days ago, I think.

43
00:04:03,600 --> 00:04:10,400
 So Nanimita is a characteristic or a sign is a good word.

44
00:04:10,400 --> 00:04:12,880
 It's just we don't use it in English really.

45
00:04:12,880 --> 00:04:17,590
 But when you see a human being, as I said, how you know it

46
00:04:17,590 --> 00:04:19,520
's a man or it's a woman is

47
00:04:19,520 --> 00:04:23,810
 by the sign of a man or the sign of a woman, the hint or

48
00:04:23,810 --> 00:04:26,280
 the characteristic, something

49
00:04:26,280 --> 00:04:31,730
 that creates or induces recognition, that's a man, that's a

50
00:04:31,730 --> 00:04:32,600
 woman.

51
00:04:32,600 --> 00:04:36,690
 When you see an apple, that which tells you it's an apple

52
00:04:36,690 --> 00:04:38,240
 and not an orange.

53
00:04:38,240 --> 00:04:44,760
 And the moment you get that it's an apple, that's a Nanim

54
00:04:44,760 --> 00:04:45,320
ita.

55
00:04:45,320 --> 00:04:54,560
 And beyond that, there is the sign of beauty and the sign

56
00:04:54,560 --> 00:04:57,200
 of ugliness.

57
00:04:57,200 --> 00:05:00,760
 And so there's the sign of that which is desirable.

58
00:05:00,760 --> 00:05:05,170
 And when you grasp that, when you get yourself to the point

59
00:05:05,170 --> 00:05:08,200
 of seeing something as beautiful,

60
00:05:08,200 --> 00:05:11,640
 then desire will arise.

61
00:05:11,640 --> 00:05:15,440
 So this is what we want to avoid.

62
00:05:15,440 --> 00:05:18,330
 When you see something, you see a person and you say to

63
00:05:18,330 --> 00:05:20,200
 yourself, seeing, seeing.

64
00:05:20,200 --> 00:05:25,120
 The person doesn't get to the point of a person.

65
00:05:25,120 --> 00:05:31,880
 It doesn't go further to the point of beautiful or ugly.

66
00:05:31,880 --> 00:05:35,620
 Someone you like or dislike when you say seeing, seeing,

67
00:05:35,620 --> 00:05:38,160
 that part of the chain doesn't arise.

68
00:05:38,160 --> 00:05:43,160
 Because you've broken the chain.

69
00:05:43,160 --> 00:05:50,160
 Anubyanjana, it's another aspect of this.

70
00:05:50,160 --> 00:05:51,680
 Anubyanjana means the particulars.

71
00:05:51,680 --> 00:05:54,440
 It's really saying the same thing in two different ways.

72
00:05:54,440 --> 00:05:57,360
 But Anubyanjana means particulars.

73
00:05:57,360 --> 00:05:58,840
 So we don't want to know the particulars.

74
00:05:58,840 --> 00:06:00,320
 Isn't that interesting?

75
00:06:00,320 --> 00:06:02,560
 Because the particulars are where the danger comes.

76
00:06:02,560 --> 00:06:09,580
 So the Buddha says next, "Syatva Dikaranamenang, Chakundray

77
00:06:09,580 --> 00:06:11,680
ang Asamutang."

78
00:06:11,680 --> 00:06:19,400
 Having not, when the eye faculty is not guarded, Asamutang,

79
00:06:19,400 --> 00:06:25,960
 Mihurantang dwelling with the eye

80
00:06:25,960 --> 00:06:37,560
 unguarded, one dwells with the mind unguarded.

81
00:06:37,560 --> 00:06:42,150
 Evil unwholesome states of covetousness and grief might

82
00:06:42,150 --> 00:06:43,400
 invade him.

83
00:06:43,400 --> 00:06:44,960
 It's the key.

84
00:06:44,960 --> 00:06:46,400
 This is where likes and dislikes come.

85
00:06:46,400 --> 00:06:53,200
 This is where pleasure and displeasure come from.

86
00:06:53,200 --> 00:06:56,420
 This is where addiction and aversion, these habits that we

87
00:06:56,420 --> 00:06:58,200
 cultivate that make us greedy,

88
00:06:58,200 --> 00:07:02,940
 that make us angry, that create frustration and boredom,

89
00:07:02,940 --> 00:07:05,560
 that create fear and that create

90
00:07:05,560 --> 00:07:11,180
 worry, that create addiction, create withdrawal,

91
00:07:11,180 --> 00:07:15,960
 disappointment, not getting what you want,

92
00:07:15,960 --> 00:07:19,680
 getting what you don't want, all this trouble that we have

93
00:07:19,680 --> 00:07:21,760
 in our lives, it's all created

94
00:07:21,760 --> 00:07:28,920
 by grasping at signs in particular.

95
00:07:28,920 --> 00:07:31,680
 So when we try, when you see something, we try to say to

96
00:07:31,680 --> 00:07:33,560
 ourselves, see, remind ourselves

97
00:07:33,560 --> 00:07:41,430
 at seeing, otherwise it becomes beautiful or ugly, and

98
00:07:41,430 --> 00:07:44,280
 sounds at the ear.

99
00:07:44,280 --> 00:07:47,970
 The problem is that we're taught in society to look for the

100
00:07:47,970 --> 00:07:50,160
 good sounds and the good signs.

101
00:07:50,160 --> 00:07:57,330
 It's actually this kind of talk is highly unwelcome for

102
00:07:57,330 --> 00:07:59,440
 most people.

103
00:07:59,440 --> 00:08:01,690
 Now to talk about beauty as though there was something

104
00:08:01,690 --> 00:08:03,280
 wrong with that, as though we should

105
00:08:03,280 --> 00:08:07,840
 see things look beyond beauty, see beautiful things without

106
00:08:07,840 --> 00:08:10,000
 noticing how beautiful they

107
00:08:10,000 --> 00:08:13,160
 are.

108
00:08:13,160 --> 00:08:19,940
 The spiritual teachers or spiritual practitioners often

109
00:08:19,940 --> 00:08:22,480
 make this mistake.

110
00:08:22,480 --> 00:08:25,430
 They think this whole idea of stopping to smell the roses

111
00:08:25,430 --> 00:08:27,640
 as though there was something beneficial

112
00:08:27,640 --> 00:08:35,560
 about smelling roses.

113
00:08:35,560 --> 00:08:40,960
 Maybe it's surprising, I guess it speaks to the age group,

114
00:08:40,960 --> 00:08:43,680
 but I would say somewhere around

115
00:08:43,680 --> 00:08:48,510
 50% of the undergraduate students at the university have

116
00:08:48,510 --> 00:08:51,440
 these ear muds in their ears, like in

117
00:08:51,440 --> 00:08:53,160
 between classes all the time.

118
00:08:53,160 --> 00:08:57,120
 They come to do meditation, today I did a five minute

119
00:08:57,120 --> 00:08:59,600
 meditation all day, so people would

120
00:08:59,600 --> 00:09:04,160
 come and I'd give them a five minute meditation lesson.

121
00:09:04,160 --> 00:09:06,920
 And most of them, most of them had to take the earbud out

122
00:09:06,920 --> 00:09:08,400
 of their ear before they sat

123
00:09:08,400 --> 00:09:09,400
 down.

124
00:09:09,400 --> 00:09:13,400
 It's really a thing.

125
00:09:13,400 --> 00:09:16,430
 We can't live in the world, it's bizarre for those of us

126
00:09:16,430 --> 00:09:18,100
 who walk down the street and are

127
00:09:18,100 --> 00:09:24,320
 in the world hearing the ordinary sounds.

128
00:09:24,320 --> 00:09:31,000
 One of my classmates said she, what did she say?

129
00:09:31,000 --> 00:09:34,560
 Something about singing in the shower.

130
00:09:34,560 --> 00:09:35,560
 You sing in the shower?

131
00:09:35,560 --> 00:09:36,880
 I said, oh yeah, I have to have music.

132
00:09:36,880 --> 00:09:41,320
 They said, you have music in the bathroom?

133
00:09:41,320 --> 00:09:45,640
 It's apparently a thing.

134
00:09:45,640 --> 00:09:48,000
 It was like, yes, I think music in the bathroom is

135
00:09:48,000 --> 00:09:49,240
 important for a...

136
00:09:49,240 --> 00:09:50,280
 Sorry, I shouldn't...

137
00:09:50,280 --> 00:09:52,120
 She may end up watching this.

138
00:09:52,120 --> 00:09:56,400
 She's a really good person.

139
00:09:56,400 --> 00:09:57,400
 We're working together.

140
00:09:57,400 --> 00:10:00,660
 She's the person who's helping me work on the Peace Sym

141
00:10:00,660 --> 00:10:02,960
posium, which I haven't mentioned,

142
00:10:02,960 --> 00:10:03,960
 I don't think.

143
00:10:03,960 --> 00:10:07,840
 We're doing a Peace Symposium at McMaster.

144
00:10:07,840 --> 00:10:10,970
 People have been asking me, I don't really know what the

145
00:10:10,970 --> 00:10:12,880
 word symposium means, but sort

146
00:10:12,880 --> 00:10:14,520
 of like a peace fair.

147
00:10:14,520 --> 00:10:17,640
 And in my mind, it is actually, I don't think that's where

148
00:10:17,640 --> 00:10:19,600
 the direction that the Peace Studies

149
00:10:19,600 --> 00:10:24,430
 program wants us to go, but I want it to be a healing

150
00:10:24,430 --> 00:10:28,080
 experience where people come together

151
00:10:28,080 --> 00:10:29,080
 and...

152
00:10:29,080 --> 00:10:33,400
 I mean like a peace, an active peace process where people

153
00:10:33,400 --> 00:10:35,920
 come and they leave knowing more

154
00:10:35,920 --> 00:10:41,120
 about peace, having learned skills about cultivating peace.

155
00:10:41,120 --> 00:10:45,440
 Anyway, sorry, a little bit off track there.

156
00:10:45,440 --> 00:10:50,600
 But it's a thing, no?

157
00:10:50,600 --> 00:10:54,840
 We want to hear beautiful music, beautiful sounds.

158
00:10:54,840 --> 00:10:59,640
 A lot of people ask whether you can meditate to music,

159
00:10:59,640 --> 00:11:03,160
 which it shows this misunderstanding

160
00:11:03,160 --> 00:11:04,160
 about spirituality.

161
00:11:04,160 --> 00:11:07,160
 I mean, from a Buddhist point of view, anyway.

162
00:11:07,160 --> 00:11:10,790
 Of course, a lot of people would say the Buddhists have got

163
00:11:10,790 --> 00:11:13,920
 it wrong, so to each their own.

164
00:11:13,920 --> 00:11:21,240
 Smells, we want to smell good things.

165
00:11:21,240 --> 00:11:26,100
 It's the nature of wanting to experience good things that

166
00:11:26,100 --> 00:11:28,600
 you cultivate partiality.

167
00:11:28,600 --> 00:11:29,600
 It's like a pendulum.

168
00:11:29,600 --> 00:11:34,320
 If you pull it one way, you're creating the...

169
00:11:34,320 --> 00:11:39,600
 With the potential, potential energy.

170
00:11:39,600 --> 00:11:43,390
 The more you pull, the more potential until finally you let

171
00:11:43,390 --> 00:11:45,200
 go and it will swing back.

172
00:11:45,200 --> 00:11:47,400
 You create the opposite.

173
00:11:47,400 --> 00:11:53,680
 You create the aversion with the attraction and become more

174
00:11:53,680 --> 00:11:57,400
 irritable, more dissatisfied,

175
00:11:57,400 --> 00:12:07,200
 more prone to disappointment, more prone to anger.

176
00:12:07,200 --> 00:12:10,000
 This is why people fight.

177
00:12:10,000 --> 00:12:12,180
 People who are steeped in sensual pleasure have so much

178
00:12:12,180 --> 00:12:13,440
 sensual pleasure, end up being

179
00:12:13,440 --> 00:12:15,480
 the ones who fight the most.

180
00:12:15,480 --> 00:12:19,340
 Fight with each other, bicker with each other, argue with

181
00:12:19,340 --> 00:12:20,320
 each other.

182
00:12:20,320 --> 00:12:25,160
 So much conflict comes from sensuality.

183
00:12:25,160 --> 00:12:29,720
 Comes from that which is supposed to make us happy.

184
00:12:29,720 --> 00:12:32,080
 We go to war over our happiness.

185
00:12:32,080 --> 00:12:37,160
 It's the truth of it.

186
00:12:37,160 --> 00:12:42,760
 So when we smell good smells, we want to smell the roses.

187
00:12:42,760 --> 00:12:51,720
 We don't want to smell bad smells.

188
00:12:51,720 --> 00:12:55,290
 And it seems so ingrained in the experience where you think

189
00:12:55,290 --> 00:12:56,960
 a bad smell is a bad smell.

190
00:12:56,960 --> 00:13:00,040
 It's bad.

191
00:13:00,040 --> 00:13:04,600
 This is the first myth that we have to dispel.

192
00:13:04,600 --> 00:13:10,280
 The bad is a product of the particulars of the smell.

193
00:13:10,280 --> 00:13:15,480
 If it's just smelling, if you say smelling, smelling, it's

194
00:13:15,480 --> 00:13:16,720
 just smell.

195
00:13:16,720 --> 00:13:17,720
 Same goes with taste.

196
00:13:17,720 --> 00:13:23,440
 Good taste, bad taste.

197
00:13:23,440 --> 00:13:25,600
 It's easy to get caught up in tastes.

198
00:13:25,600 --> 00:13:30,050
 But it's also very easy if you know how to free yourself,

199
00:13:30,050 --> 00:13:32,360
 to avoid that whole world of

200
00:13:32,360 --> 00:13:37,000
 life, addiction, and aversion.

201
00:13:37,000 --> 00:13:40,360
 If you say to yourself, tasting, tasting.

202
00:13:40,360 --> 00:13:43,960
 All of this, seeing, seeing with sensuality, with sexuality

203
00:13:43,960 --> 00:13:46,120
, with romance, seeing a beautiful

204
00:13:46,120 --> 00:13:51,640
 person, seeing, seeing, it's just seeing.

205
00:13:51,640 --> 00:13:52,640
 Cut it off totally.

206
00:13:52,640 --> 00:13:59,480
 First you have to be diligent, of course.

207
00:13:59,480 --> 00:14:02,550
 You've done something that just, you can break off with one

208
00:14:02,550 --> 00:14:03,080
 goal.

209
00:14:03,080 --> 00:14:05,080
 It's a habit that you build.

210
00:14:05,080 --> 00:14:07,590
 Eventually it becomes a habit to see things just as they

211
00:14:07,590 --> 00:14:07,960
 are.

212
00:14:07,960 --> 00:14:10,800
 And that's the habit we want to cultivate.

213
00:14:10,800 --> 00:14:20,880
 When you can do that, you move towards letting go.

214
00:14:20,880 --> 00:14:23,840
 So a really good quote.

215
00:14:23,840 --> 00:14:28,240
 Not so much more to say.

216
00:14:28,240 --> 00:14:33,830
 But we could talk about this happiness that is unsullied or

217
00:14:33,830 --> 00:14:35,720
 unadulterated.

218
00:14:35,720 --> 00:14:38,690
 So it's the difference between happiness that is based on

219
00:14:38,690 --> 00:14:40,640
 partiality because it's dependent

220
00:14:40,640 --> 00:14:46,800
 on x and not y or not x.

221
00:14:46,800 --> 00:14:53,550
 Opposed to the happiness that is free from reliance or

222
00:14:53,550 --> 00:14:57,320
 dependence on experience.

223
00:14:57,320 --> 00:15:00,680
 Happiness that comes from being free from letting go.

224
00:15:00,680 --> 00:15:02,720
 From freedom from stress.

225
00:15:02,720 --> 00:15:09,560
 Freedom from the concern about the experience.

226
00:15:09,560 --> 00:15:13,440
 Anyway.

227
00:15:13,440 --> 00:15:17,800
 So that's our Dhamma for today.

228
00:15:17,800 --> 00:15:19,720
 Nobody joined me on the hangout today.

229
00:15:19,720 --> 00:15:22,480
 Must be because last night I didn't have one.

230
00:15:22,480 --> 00:15:24,040
 You can go ahead.

231
00:15:24,040 --> 00:15:27,680
 Oh, I know because I didn't post the hangout.

232
00:15:27,680 --> 00:15:29,080
 You can't join me.

233
00:15:29,080 --> 00:15:30,080
 Right.

234
00:15:30,080 --> 00:15:32,720
 There's a step missing here.

235
00:15:32,720 --> 00:15:33,720
 Sorry.

236
00:15:33,720 --> 00:15:34,720
 There.

237
00:15:34,720 --> 00:15:37,720
 There's the hangout.

238
00:15:37,720 --> 00:15:43,600
 Is there anybody around who wants to join?

239
00:15:43,600 --> 00:15:45,120
 Only join if you want to ask a question.

240
00:15:45,120 --> 00:15:48,200
 If you have a question, join me.

241
00:15:48,200 --> 00:15:52,200
 Ask it in the hangout.

242
00:15:52,200 --> 00:15:55,200
 Okay.

243
00:15:55,200 --> 00:15:58,200
 Okay.

244
00:15:58,200 --> 00:16:01,200
 Okay.

245
00:16:01,200 --> 00:16:05,200
 Okay.

246
00:16:05,200 --> 00:16:09,200
 Okay.

247
00:16:09,200 --> 00:16:14,200
 Okay.

248
00:16:14,200 --> 00:16:25,200
 Larry, do you have a question?

249
00:16:25,200 --> 00:16:26,200
 No.

250
00:16:26,200 --> 00:16:30,200
 I've been listening to the Dhamma talk.

251
00:16:30,200 --> 00:16:37,200
 And I just noticed that the link to this call in showed up.

252
00:16:37,200 --> 00:16:39,200
 So I clicked on it.

253
00:16:39,200 --> 00:16:43,200
 I've been wondering if I should have seen the link earlier.

254
00:16:43,200 --> 00:16:44,200
 Yeah.

255
00:16:44,200 --> 00:16:47,200
 I forgot to post it.

256
00:16:47,200 --> 00:16:56,820
 And I presume that a prior link to a previous call would

257
00:16:56,820 --> 00:16:59,200
 not work for this call.

258
00:16:59,200 --> 00:17:00,200
 No.

259
00:17:00,200 --> 00:17:01,200
 Okay.

260
00:17:01,200 --> 00:17:05,200
 Everyone is uniquely coded or whatever for the internet.

261
00:17:05,200 --> 00:17:06,200
 Okay.

262
00:17:06,200 --> 00:17:11,440
 So I listened to your talk and clicked on the link to the

263
00:17:11,440 --> 00:17:17,200
 call in as soon as I saw it.

264
00:17:17,200 --> 00:17:20,200
 But I do have a question.

265
00:17:20,200 --> 00:17:27,200
 You sparked a question in me.

266
00:17:27,200 --> 00:17:34,860
 When we're just essentially noting seeing, seeing, smelling

267
00:17:34,860 --> 00:17:36,200
, hearing.

268
00:17:36,200 --> 00:17:41,200
 And of course those are based on the sense doors.

269
00:17:41,200 --> 00:17:46,200
 The eyes, ears, nose, mouth, touching.

270
00:17:46,200 --> 00:17:54,200
 So is it inappropriate to just say something like sensing?

271
00:17:54,200 --> 00:18:01,740
 And try to tie the noting to the specific sense door is

272
00:18:01,740 --> 00:18:07,200
 there's evidently value in that.

273
00:18:07,200 --> 00:18:10,200
 And I'm just speculating.

274
00:18:10,200 --> 00:18:11,200
 Right.

275
00:18:11,200 --> 00:18:13,200
 I mean the word isn't that important.

276
00:18:13,200 --> 00:18:17,200
 Whatever brings you close to the experience.

277
00:18:17,200 --> 00:18:19,200
 But sensing to me is too abstract.

278
00:18:19,200 --> 00:18:22,200
 It's not likely to bring you close to the experience.

279
00:18:22,200 --> 00:18:25,200
 I mean the bear experience.

280
00:18:25,200 --> 00:18:26,200
 Right.

281
00:18:26,200 --> 00:18:29,200
 It's a bit abstract.

282
00:18:29,200 --> 00:18:33,200
 You don't sense something, hear something.

283
00:18:33,200 --> 00:18:35,200
 Good.

284
00:18:35,200 --> 00:18:38,200
 I appreciate the clarification.

285
00:18:38,200 --> 00:18:39,200
 I've wondered that.

286
00:18:39,200 --> 00:18:45,150
 And like you say, just say, try to boil it down to the bear

287
00:18:45,150 --> 00:18:47,200
 essential sensing.

288
00:18:47,200 --> 00:18:53,200
 It isn't very satisfying.

289
00:18:53,200 --> 00:18:58,770
 Notionally or intellectually it's not a very satisfying

290
00:18:58,770 --> 00:19:00,200
 term to use.

291
00:19:00,200 --> 00:19:08,200
 But yeah, I appreciate that.

292
00:19:08,200 --> 00:19:10,750
 One thing I did want to talk about when I got your

293
00:19:10,750 --> 00:19:12,200
 attention everybody.

294
00:19:12,200 --> 00:19:18,760
 Tomorrow I'm having a weekly, we're starting doing these

295
00:19:18,760 --> 00:19:25,200
 weekly talks in Second Life at the Buddha Center.

296
00:19:25,200 --> 00:19:28,200
 So if you know what Second Life is and you know where the

297
00:19:28,200 --> 00:19:31,200
 Buddha Center is, it can come out.

298
00:19:31,200 --> 00:19:35,320
 That's at 3pm or you can just come to the meditation site,

299
00:19:35,320 --> 00:19:37,200
 meditation.siri-mongolo.org

300
00:19:37,200 --> 00:19:42,200
 and listen in because I'll be simulcasting.

301
00:19:42,200 --> 00:19:45,200
 Is that a word?

302
00:19:45,200 --> 00:19:51,350
 Simultaneously broadcasting audio there so you can listen

303
00:19:51,350 --> 00:19:52,200
 to the audio.

304
00:19:52,200 --> 00:19:55,200
 I don't know what I'm talking about yet.

305
00:19:55,200 --> 00:19:59,890
 Anybody have anything they want me to give a talk on

306
00:19:59,890 --> 00:20:01,200
 tomorrow?

307
00:20:01,200 --> 00:20:05,610
 If you come on the Hangout and give me a topic, I'll

308
00:20:05,610 --> 00:20:07,200
 consider it.

309
00:20:07,200 --> 00:20:10,200
 Can you give a talk on dependent origination?

310
00:20:10,200 --> 00:20:13,930
 I actually gave one in Second Life on dependent origination

311
00:20:13,930 --> 00:20:14,200
.

312
00:20:14,200 --> 00:20:18,200
 I've given several on dependent origination.

313
00:20:18,200 --> 00:20:24,200
 You can look it up.

314
00:20:24,200 --> 00:20:27,370
 I don't know if they're any good, but I think there's one

315
00:20:27,370 --> 00:20:29,200
 on practical dependent origination.

316
00:20:29,200 --> 00:20:33,580
 Not Second Life, but there's one that's called practical

317
00:20:33,580 --> 00:20:35,200
 dependent origination.

318
00:20:35,200 --> 00:20:38,200
 It seems to me. Maybe it's one I did in Sri Lanka.

319
00:20:38,200 --> 00:20:42,850
 I think it's one I did on the Buddhist television in Sri

320
00:20:42,850 --> 00:20:43,200
 Lanka.

321
00:20:43,200 --> 00:20:48,450
 I remember seeing a YouTube that you had published quite

322
00:20:48,450 --> 00:20:49,200
 some back.

323
00:20:49,200 --> 00:20:54,090
 I think you were in Sri Lanka somewhere overseas about

324
00:20:54,090 --> 00:20:58,200
 dependent origination.

325
00:20:58,200 --> 00:21:04,200
 I have done a couple of these.

326
00:21:04,200 --> 00:21:10,200
 Another thing, can you talk about right view?

327
00:21:10,200 --> 00:21:17,200
 Sure.

328
00:21:17,200 --> 00:21:20,200
 I have to think about that.

329
00:21:20,200 --> 00:21:23,200
 There's different ways of talking about right view.

330
00:21:23,200 --> 00:21:30,200
 Different ways of looking at it.

331
00:21:30,200 --> 00:21:37,200
 Would the different ways of talking about be associated

332
00:21:37,200 --> 00:21:43,200
 with different sects of Buddhism?

333
00:21:43,200 --> 00:21:54,440
 Like the forced tradition or this or that, the way they

334
00:21:54,440 --> 00:21:55,200
 define the Noble Eightfold Path?

335
00:21:55,200 --> 00:21:58,200
 Is that what you're saying?

336
00:21:58,200 --> 00:22:02,440
 The different ways of talking about right view is because

337
00:22:02,440 --> 00:22:05,200
 the Buddha talked about it in different ways.

338
00:22:05,200 --> 00:22:14,200
 There's basically two kinds, mundane and noble.

339
00:22:14,200 --> 00:22:19,200
 Mundane right view is useful, but it's not enough.

340
00:22:19,200 --> 00:22:37,200
 Mundane right view has to do with the law of karma. Mostly

341
00:22:37,200 --> 00:22:37,200
 the law of karma.

342
00:22:37,200 --> 00:22:43,200
 I'll take a look at it, see?

343
00:22:43,200 --> 00:22:49,740
 There is right view of non-self. There is right view of the

344
00:22:49,740 --> 00:22:55,200
 truth of suffering or the Four Noble Truths.

345
00:22:55,200 --> 00:23:01,200
 There is right view in terms of karma.

346
00:23:01,200 --> 00:23:07,740
 Those are three that I can think of. They're not

347
00:23:07,740 --> 00:23:16,020
 exclusively different, but three ways of approaching right

348
00:23:16,020 --> 00:23:17,200
 view.

349
00:23:17,200 --> 00:23:21,200
 Thank you, that sounds like a good talk.

350
00:23:21,200 --> 00:23:26,200
 You're welcome.

351
00:23:26,200 --> 00:23:29,200
 Bobby, did you have a question? Is that why you're here?

352
00:23:29,200 --> 00:23:36,870
 I was just going to ask about the Four Nutriments in the

353
00:23:36,870 --> 00:23:39,200
 Samadhi Tisuta,

354
00:23:39,200 --> 00:23:45,200
 how they relate to dependent origination.

355
00:23:45,200 --> 00:23:56,540
 I believe that that sutta talks about Pachchachas Samupada

356
00:23:56,540 --> 00:23:58,200
 as well as the Four Nutriments and the Four Noble Truths.

357
00:23:58,200 --> 00:24:01,200
 Did you say the Samadhi Tisuta?

358
00:24:01,200 --> 00:24:05,200
 Yeah.

359
00:24:05,200 --> 00:24:08,200
 What are the Four Nutriments?

360
00:24:08,200 --> 00:24:11,200
 What are the Four Nutriments?

361
00:24:11,200 --> 00:24:19,520
 It was food, so physical food, and then mental volition, so

362
00:24:19,520 --> 00:24:23,200
 sankhara, and then contact.

363
00:24:23,200 --> 00:24:31,470
 Manosanthi, it's not actually Manosanthi, it's actually

364
00:24:31,470 --> 00:24:34,200
 attention.

365
00:24:34,200 --> 00:24:39,490
 And then there is Pasahara and Minyanahara, consciousness,

366
00:24:39,490 --> 00:24:40,200
 right?

367
00:24:40,200 --> 00:24:42,200
 Yeah.

368
00:24:42,200 --> 00:24:44,360
 I didn't realize that came from the Samadhi Tisuta. It

369
00:24:44,360 --> 00:24:46,200
 shows what a scholar I am.

370
00:24:46,200 --> 00:24:52,200
 Which one is Samadhi Tijimani Kaya number 12?

371
00:24:52,200 --> 00:24:56,200
 I think it's 9. Yeah, number 9.

372
00:24:56,200 --> 00:24:59,200
 Yep.

373
00:24:59,200 --> 00:25:02,200
 Sutra by Sari Puta.

374
00:25:02,200 --> 00:25:12,200
 Yeah.

375
00:25:12,200 --> 00:25:16,400
 The arriving of craving, their causes, right? I mean this

376
00:25:16,400 --> 00:25:18,050
 whole sutra, sutta is about different ways of expressing

377
00:25:18,050 --> 00:25:19,200
 the Four Noble Truths.

378
00:25:19,200 --> 00:25:21,200
 Yeah.

379
00:25:21,200 --> 00:25:25,960
 I think it says in the commentary that he is 24 different

380
00:25:25,960 --> 00:25:29,200
 ways, or maybe he even says something.

381
00:25:29,200 --> 00:25:33,200
 In 24 different ways, or some number of ways.

382
00:25:33,200 --> 00:25:35,680
 Most number of times anyone has gone through the Four Noble

383
00:25:35,680 --> 00:25:37,200
 Truths or something like that.

384
00:25:37,200 --> 00:25:42,500
 Yeah, especially as you always relate the topic that he is

385
00:25:42,500 --> 00:25:46,200
 talking about back to the Noble Truth.

386
00:25:46,200 --> 00:25:50,520
 I mean, the Bhattichya Samupada is basically the Four Noble

387
00:25:50,520 --> 00:25:55,200
 Truths, kind of the Four Noble Truths expanded upon.

388
00:25:55,200 --> 00:26:00,440
 But even it, even Bhattichya Samupada is still a cond

389
00:26:00,440 --> 00:26:03,200
ensation of the Mahapatana.

390
00:26:03,200 --> 00:26:09,680
 If you really want to know about causality, there are 24 bh

391
00:26:09,680 --> 00:26:13,200
ajya, 24 conditionalities.

392
00:26:13,200 --> 00:26:16,200
 And it's much more complicated than just the line.

393
00:26:16,200 --> 00:26:18,340
 I mean, Bhattichya Samupada sounds like a line, right? It

394
00:26:18,340 --> 00:26:20,200
 sounds like it's in a chain.

395
00:26:20,200 --> 00:26:26,200
 But it's not really. It's quite complicated.

396
00:26:26,200 --> 00:26:28,870
 I mean, people try to think of it as simple as just, and

397
00:26:28,870 --> 00:26:31,490
 some people say Bhattichya Samupada only relates to one

398
00:26:31,490 --> 00:26:34,200
 moment, it's only this life.

399
00:26:34,200 --> 00:26:37,560
 Nothing to do with past lives, nothing to do with future

400
00:26:37,560 --> 00:26:38,200
 lives.

401
00:26:38,200 --> 00:26:46,420
 Bizarrely, they try to say that there's no Bhattichya Samup

402
00:26:46,420 --> 00:26:51,200
ada doesn't talk about past lives.

403
00:26:51,200 --> 00:26:56,090
 But it's quite, you know, the first Bhattichya Samkara is

404
00:26:56,090 --> 00:26:57,200
 past lives.

405
00:26:57,200 --> 00:27:03,340
 It's just a way of expressing how we are born again and

406
00:27:03,340 --> 00:27:06,200
 again based on ignorance.

407
00:27:06,200 --> 00:27:11,470
 But ignorance is also involved with tanha. So we say, "tan

408
00:27:11,470 --> 00:27:13,200
ha bhatti, waidana bhattiya tanha."

409
00:27:13,200 --> 00:27:17,810
 Well, waidana only gives rise to tanha if there's a bhichya

410
00:27:17,810 --> 00:27:18,200
.

411
00:27:18,200 --> 00:27:26,200
 But waidana doesn't require a bhichya, not in this life.

412
00:27:26,200 --> 00:27:31,060
 So you experience waidana even without a bhichya, but not

413
00:27:31,060 --> 00:27:32,200
 originally.

414
00:27:32,200 --> 00:27:38,190
 Originally you needed a bhichya to be born. So it's not

415
00:27:38,190 --> 00:27:40,200
 linear.

416
00:27:40,200 --> 00:27:45,640
 So if you really want to learn about it, you should read

417
00:27:45,640 --> 00:27:49,200
 the matikha of the Maha Bhattana.

418
00:27:49,200 --> 00:27:52,420
 There's a really good chant that I actually put up on my

419
00:27:52,420 --> 00:27:53,200
 website.

420
00:27:53,200 --> 00:27:57,560
 It makes me think I was Burmese in the past life because

421
00:27:57,560 --> 00:28:02,200
 the first time I heard this chant, I just struck by it.

422
00:28:02,200 --> 00:28:04,760
 I've never been struck by chanting before, but it was

423
00:28:04,760 --> 00:28:06,200
 something so...

424
00:28:06,200 --> 00:28:09,200
 It just resonated with me. And these monks from Burma were

425
00:28:09,200 --> 00:28:15,230
 visiting Thailand and they went around this jaidya in Doy S

426
00:28:15,230 --> 00:28:16,200
uteib.

427
00:28:16,200 --> 00:28:20,900
 And I just had to know what that was, so later I asked them

428
00:28:20,900 --> 00:28:28,480
, "Would a Sri Lankan monk, a Burmese monk, explain it to me

429
00:28:28,480 --> 00:28:29,200
?"

430
00:28:29,200 --> 00:28:36,200
 And he said, "Oh, that's the Maha Bhattana."

431
00:28:36,200 --> 00:28:41,740
 So, hetupachayoti hetupachayoti kannan daman dang smutanan

432
00:28:41,740 --> 00:28:45,200
 jarmpa nang hetupachayena bachayo.

433
00:28:45,200 --> 00:28:47,200
 Something like that.

434
00:28:47,200 --> 00:28:52,310
 So that's the first one, hetupachayo, and something is its

435
00:28:52,310 --> 00:28:53,200
 cause.

436
00:28:53,200 --> 00:29:01,200
 So a cause of...

437
00:29:01,200 --> 00:29:18,200
 Something is a cause of form together with its dhammas.

438
00:29:18,200 --> 00:29:21,020
 I can't translate this. I used to know how this was

439
00:29:21,020 --> 00:29:22,200
 translated.

440
00:29:22,200 --> 00:29:25,380
 Something is a cause when it's... I mean, this is just very

441
00:29:25,380 --> 00:29:30,720
, very basic, very, very brief going through how something

442
00:29:30,720 --> 00:29:31,200
 is a cause of something else.

443
00:29:31,200 --> 00:29:36,200
 You have to read the translation.

444
00:29:36,200 --> 00:29:39,200
 I'll definitely check that out.

445
00:29:39,200 --> 00:29:47,160
 There is a translation of the Maha Bhattana, at least

446
00:29:47,160 --> 00:29:51,200
 abbreviated, I think.

447
00:29:51,200 --> 00:29:55,200
 Anyway, that's all for tonight.

448
00:29:55,200 --> 00:29:59,200
 Then, if there's no questions, I'm going to head off.

449
00:29:59,200 --> 00:30:02,200
 Soti?

450
00:30:02,200 --> 00:30:04,200
 Monty?

451
00:30:04,200 --> 00:30:09,200
 Soti is for hello. When we say goodbye, it's God who...

452
00:30:09,200 --> 00:30:11,200
 God who?

453
00:30:11,200 --> 00:30:14,700
 I tried to sort that out, and I wasn't successful the other

454
00:30:14,700 --> 00:30:15,200
 day.

455
00:30:15,200 --> 00:30:20,480
 Honestly, it's probably okay, just the tradition. It's only

456
00:30:20,480 --> 00:30:21,200
 the tradition I know.

457
00:30:21,200 --> 00:30:24,200
 It's Soti is to say hello. Sadhu is when you finish.

458
00:30:24,200 --> 00:30:26,200
 Sadhu.

459
00:30:26,200 --> 00:30:29,200
 Sadhu, Monty.

460
00:30:29,200 --> 00:30:30,200
 Good night.

461
00:30:30,200 --> 00:30:31,200
 Good night, all.

462
00:30:31,200 --> 00:30:32,200
 Thank you very much.

463
00:30:34,200 --> 00:30:36,200
 Thank you.

