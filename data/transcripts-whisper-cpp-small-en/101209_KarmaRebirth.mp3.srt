1
00:00:00,000 --> 00:00:05,760
 All right then, welcome everyone. Today's talk will be

2
00:00:05,760 --> 00:00:06,880
 about two important

3
00:00:06,880 --> 00:00:12,160
 concepts in the Buddha's teaching. These are karma and

4
00:00:12,160 --> 00:00:14,640
 rebirth. Now I say these

5
00:00:14,640 --> 00:00:19,970
 are important concepts and I think this is a debated

6
00:00:19,970 --> 00:00:22,480
 statement. Many people seem

7
00:00:22,480 --> 00:00:24,990
 to think that karma and rebirth have nothing to do with the

8
00:00:24,990 --> 00:00:25,480
 Buddha's

9
00:00:25,480 --> 00:00:30,640
 teaching, most especially the teaching on rebirth. There's

10
00:00:30,640 --> 00:00:32,320
 a lot of belief that

11
00:00:32,320 --> 00:00:38,310
 the Buddha taught only for one life and nothing to do with

12
00:00:38,310 --> 00:00:40,320
 the idea of rebirth.

13
00:00:40,320 --> 00:00:46,510
 Really these sorts of debates I think center around our

14
00:00:46,510 --> 00:00:48,880
 attachment to views,

15
00:00:48,880 --> 00:00:59,660
 our opinions and views of what the Buddha taught rather

16
00:00:59,660 --> 00:01:01,640
 than actual

17
00:01:01,640 --> 00:01:05,290
 understanding of reality. Because when you practice the

18
00:01:05,290 --> 00:01:06,560
 Buddha's teaching it

19
00:01:06,560 --> 00:01:14,960
 really makes no difference as to what is one's opinion or

20
00:01:14,960 --> 00:01:16,600
 the idea of what it

21
00:01:16,600 --> 00:01:24,520
 was that the Buddha taught or so on. Whether the Buddha

22
00:01:24,520 --> 00:01:25,000
 taught about

23
00:01:25,000 --> 00:01:33,840
 rebirth, about past lives or future lives, there's no

24
00:01:33,840 --> 00:01:34,440
 argument that

25
00:01:34,440 --> 00:01:37,880
 arises because through the practice you come to see for

26
00:01:37,880 --> 00:01:39,960
 yourself the reality of

27
00:01:39,960 --> 00:01:47,180
 things. What we come to see when we practice the Buddha's

28
00:01:47,180 --> 00:01:48,400
 teaching and

29
00:01:48,400 --> 00:01:54,280
 understand the reality of things is that really in a way

30
00:01:54,280 --> 00:01:55,840
 the Buddha didn't teach

31
00:01:55,840 --> 00:02:04,200
 karma and rebirth in their standard form.

32
00:02:04,640 --> 00:02:12,050
 Because the word karma means action and the doctrine of

33
00:02:12,050 --> 00:02:16,000
 karma means the belief

34
00:02:16,000 --> 00:02:30,680
 that every action that we do has an effect on our happiness

35
00:02:30,680 --> 00:02:30,920
 and

36
00:02:30,920 --> 00:02:36,920
 our unhappiness. Either that or certain actions when

37
00:02:36,920 --> 00:02:39,640
 performed have an effect on

38
00:02:39,640 --> 00:02:48,480
 our happiness and our unhappiness in and of themselves.

39
00:02:48,480 --> 00:02:52,440
 This was the

40
00:02:52,440 --> 00:02:56,730
 doctrine that was understood to be the truth by many people

41
00:02:56,730 --> 00:02:57,880
 before the time of

42
00:02:57,880 --> 00:03:14,800
 the Buddha. It was taught by the Brahmins, the priests. The

43
00:03:14,800 --> 00:03:15,120
 reason it

44
00:03:15,120 --> 00:03:17,790
 was taught by them it seems is because this was a very

45
00:03:17,790 --> 00:03:18,920
 profitable thing to

46
00:03:18,920 --> 00:03:25,360
 teach when you teach people that certain ritual actions are

47
00:03:25,360 --> 00:03:25,560
 to their

48
00:03:25,560 --> 00:03:33,160
 benefit for either warding off evil or bringing blessings

49
00:03:33,160 --> 00:03:37,240
 and so on. Those

50
00:03:37,240 --> 00:03:43,040
 actions were actions that required a priest to perform.

51
00:03:43,040 --> 00:03:44,080
 Then it was a great

52
00:03:44,080 --> 00:03:48,610
 way to make your living. This evolved into an entire

53
00:03:48,610 --> 00:03:50,720
 doctrine based on

54
00:03:50,720 --> 00:03:59,740
 these sacrifices that the Brahmins did. It was also prior

55
00:03:59,740 --> 00:04:01,160
 to the establishment of

56
00:04:01,160 --> 00:04:07,400
 the priestly caste or group it seems it was a part of the

57
00:04:07,400 --> 00:04:09,080
 nomadic culture of the

58
00:04:09,080 --> 00:04:12,170
 Aryans, these people who came in and slaughtered all the

59
00:04:12,170 --> 00:04:14,920
 natives in India.

60
00:04:14,920 --> 00:04:18,350
 When they won the battle they would always offer something

61
00:04:18,350 --> 00:04:19,400
 to their God, to

62
00:04:19,400 --> 00:04:27,000
 Indra. You can see this of course in all nomadic or native

63
00:04:27,000 --> 00:04:30,120
 tribal

64
00:04:30,120 --> 00:04:37,560
 cultures. This idea of sacrifice you can see it in Judaism

65
00:04:37,560 --> 00:04:38,680
 in the early

66
00:04:38,680 --> 00:04:46,670
 Old Testament, the Hebrew Bible. This idea that some

67
00:04:46,670 --> 00:04:50,240
 certain activities had

68
00:04:50,240 --> 00:04:52,820
 benefit and the Buddha taught against this. This was the

69
00:04:52,820 --> 00:04:53,560
 teaching that the

70
00:04:53,560 --> 00:05:03,200
 Buddha had to replace because obviously it's not for those

71
00:05:03,200 --> 00:05:03,800
 of us who have

72
00:05:03,800 --> 00:05:08,950
 an intellectual or a scientific understanding of reality.

73
00:05:08,950 --> 00:05:10,000
 We can see and

74
00:05:10,000 --> 00:05:13,680
 we can verify for ourselves through experimentation,

75
00:05:13,680 --> 00:05:15,880
 through research, through

76
00:05:15,880 --> 00:05:19,620
 empirical observation that this isn't the truth, that these

77
00:05:19,620 --> 00:05:20,680
 actions don't

78
00:05:20,680 --> 00:05:28,960
 actually have any intrinsic benefit. With the Buddha taught

79
00:05:28,960 --> 00:05:29,720
 as karma

80
00:05:29,720 --> 00:05:32,940
 and he just borrowed the word he really didn't teach karma

81
00:05:32,940 --> 00:05:33,880
 at all he taught

82
00:05:33,880 --> 00:05:41,400
 intention, and he said that what these people are talking

83
00:05:41,400 --> 00:05:42,160
 about with the word

84
00:05:42,160 --> 00:05:47,860
 karma is actually the truth of it is that it is the job it

85
00:05:47,860 --> 00:05:49,320
 is the role of

86
00:05:49,320 --> 00:05:58,140
 of one's intention, one's volition. When one has wholesome

87
00:05:58,140 --> 00:06:00,920
 intentions it

88
00:06:00,920 --> 00:06:05,620
 doesn't matter what one does, if one's mind has wisdom in

89
00:06:05,620 --> 00:06:07,240
 it or has

90
00:06:07,240 --> 00:06:15,960
 the desire to sacrifice or love in it or so on, then

91
00:06:15,960 --> 00:06:16,760
 everything one does

92
00:06:16,760 --> 00:06:20,120
 with that mind, based on that mind, the result of that mind

93
00:06:20,120 --> 00:06:21,800
, the result of that

94
00:06:21,800 --> 00:06:29,560
 karma, that mental action will inevitably be positive. This

95
00:06:29,560 --> 00:06:29,800
 is the

96
00:06:29,800 --> 00:06:35,930
 Buddha's doctrine of karma. It's a very simple doctrine and

97
00:06:35,930 --> 00:06:38,680
 it's very scientific

98
00:06:38,680 --> 00:06:43,160
 because in understanding both karma and rebirth we have to

99
00:06:43,160 --> 00:06:46,600
 understand the

100
00:06:46,600 --> 00:06:49,410
 nature of reality according to the Buddha, according to the

101
00:06:49,410 --> 00:06:49,920
 Buddha's

102
00:06:49,920 --> 00:06:54,510
 teaching. That the Buddha again didn't talk about reality

103
00:06:54,510 --> 00:06:55,400
 as something out

104
00:06:55,400 --> 00:07:02,300
 there. He talked about two kinds of reality, this

105
00:07:02,300 --> 00:07:03,840
 conceptual reality that we

106
00:07:03,840 --> 00:07:07,040
 talk about the universe, he might even talk about second

107
00:07:07,040 --> 00:07:08,400
 life as a conceptual

108
00:07:08,400 --> 00:07:11,110
 reality in the same way that the world around us is a

109
00:07:11,110 --> 00:07:12,560
 conceptual reality. So we

110
00:07:12,560 --> 00:07:16,370
 can talk about the Deer Park, we can talk about the the

111
00:07:16,370 --> 00:07:18,120
 hall, the meditation hall

112
00:07:18,120 --> 00:07:21,540
 up there, the store, the hot spring, the mountains on, but

113
00:07:21,540 --> 00:07:22,760
 these are all just

114
00:07:22,760 --> 00:07:28,360
 concepts, they don't really exist, but we use them to refer

115
00:07:28,360 --> 00:07:30,680
 these places to

116
00:07:30,680 --> 00:07:37,080
 other people. And we do the same with the world around us,

117
00:07:37,080 --> 00:07:38,920
 we refer to countries

118
00:07:38,920 --> 00:07:46,360
 and cities and places, this three-dimensional reality that

119
00:07:46,360 --> 00:07:46,680
 is out

120
00:07:46,680 --> 00:07:51,900
 there and has actually been shown to be only merely

121
00:07:51,900 --> 00:07:54,920
 conceptual by modern

122
00:07:54,920 --> 00:07:59,800
 physics, that reality can only be thought of in terms of

123
00:07:59,800 --> 00:08:01,480
 events, in terms

124
00:08:01,480 --> 00:08:07,940
 of experience. And this is very much the Buddha's teaching

125
00:08:07,940 --> 00:08:09,480
 that all

126
00:08:09,480 --> 00:08:15,670
 of reality is dependent on the mind. There's the physical

127
00:08:15,670 --> 00:08:16,720
 side of reality but

128
00:08:16,720 --> 00:08:22,510
 it is only encompassed by experience. The ultimate reality

129
00:08:22,510 --> 00:08:23,260
 of it is the

130
00:08:23,260 --> 00:08:27,720
 experience of it, when we experience the physical reality

131
00:08:27,720 --> 00:08:28,720
 around us seeing and

132
00:08:28,720 --> 00:08:31,760
 hearing and smelling it, tasting it, feeling it, thinking

133
00:08:31,760 --> 00:08:32,320
 it, this is the this

134
00:08:32,320 --> 00:08:34,880
 is what's real.

135
00:08:34,880 --> 00:08:56,000
 And so our experience of reality will define the results

136
00:08:56,000 --> 00:08:57,000
 that we receive. If we

137
00:08:57,000 --> 00:09:02,520
 choose to direct our minds in a certain direction, that's

138
00:09:02,520 --> 00:09:04,080
 the reality that or the

139
00:09:04,080 --> 00:09:08,760
 reality that arises will be based on that action, on that

140
00:09:08,760 --> 00:09:11,680
 intention. When we

141
00:09:11,680 --> 00:09:14,570
 talk about karma, it's not something magical or mystical

142
00:09:14,570 --> 00:09:15,640
 where someone kills

143
00:09:15,640 --> 00:09:17,990
 someone and then you sit around waiting for them to be hit

144
00:09:17,990 --> 00:09:18,680
 by lightning or

145
00:09:18,680 --> 00:09:25,000
 something, or even sit around waiting for them to go or

146
00:09:25,000 --> 00:09:26,760
 smug in the fact that

147
00:09:26,760 --> 00:09:32,470
 they're going to go to hell or so on. It's actually not not

148
00:09:32,470 --> 00:09:35,520
 a cut-and-dry doctrine

149
00:09:35,520 --> 00:09:39,940
 in that way. You can't say that if a person does all sorts

150
00:09:39,940 --> 00:09:40,520
 of bad things

151
00:09:40,520 --> 00:09:44,640
 they're necessarily going to go to hell. That's unscient

152
00:09:44,640 --> 00:09:47,560
ific because it's a

153
00:09:47,560 --> 00:09:53,730
 simplistic sort of magical pseudoscientific understanding

154
00:09:53,730 --> 00:09:54,160
 or belief.

155
00:09:54,160 --> 00:09:58,990
 This idea that certain things automatically lead you to

156
00:09:58,990 --> 00:09:59,760
 certain

157
00:09:59,760 --> 00:10:05,760
 results and that's unscientific. We can say that certain

158
00:10:05,760 --> 00:10:07,920
 intentions incline in

159
00:10:07,920 --> 00:10:10,950
 a certain direction but other than that, unless we can

160
00:10:10,950 --> 00:10:12,040
 somehow take into account

161
00:10:12,040 --> 00:10:17,840
 all the other factors involved, our intention and our past

162
00:10:17,840 --> 00:10:18,640
 intentions,

163
00:10:18,640 --> 00:10:25,520
 the direction we're heading and so on, then we can't really

164
00:10:25,520 --> 00:10:25,760
 make

165
00:10:25,760 --> 00:10:29,440
 those kinds of claims as to what a certain act is going to

166
00:10:29,440 --> 00:10:29,880
 affect.

167
00:10:29,880 --> 00:10:34,560
 It's going to have it's like weather. You can look at a

168
00:10:34,560 --> 00:10:35,240
 certain weather and

169
00:10:35,240 --> 00:10:37,880
 pattern and say, "Oh this is going to create a different

170
00:10:37,880 --> 00:10:39,160
 weather pattern." And

171
00:10:39,160 --> 00:10:42,700
 then the results come out completely different because

172
00:10:42,700 --> 00:10:43,680
 there are so many

173
00:10:43,680 --> 00:10:47,210
 different factors involved that you can't possibly take

174
00:10:47,210 --> 00:10:48,280
 into account. You can

175
00:10:48,280 --> 00:10:54,050
 only offer probabilities unless you have some super

176
00:10:54,050 --> 00:10:56,680
 advanced understanding of all

177
00:10:56,680 --> 00:11:01,870
 of the different variables and factors involved in the

178
00:11:01,870 --> 00:11:03,040
 process.

179
00:11:03,040 --> 00:11:11,280
 The teaching of karma really is that basically, in a basic

180
00:11:11,280 --> 00:11:12,000
 understanding,

181
00:11:12,000 --> 00:11:20,000
 is that bad things have a bad effect on our lives. That's

182
00:11:20,000 --> 00:11:20,720
 why they're bad.

183
00:11:20,720 --> 00:11:24,800
 Certain things, certain intentions will have a bad result

184
00:11:24,800 --> 00:11:27,000
 or will incline

185
00:11:27,000 --> 00:11:32,160
 towards a bad result. Certain things will incline towards a

186
00:11:32,160 --> 00:11:33,120
 good result. Will it

187
00:11:33,120 --> 00:11:39,660
 lead to our benefit? And so if we understand karma in this

188
00:11:39,660 --> 00:11:40,560
 way, we can

189
00:11:40,560 --> 00:11:42,730
 really understand the details of the doctrine. We can

190
00:11:42,730 --> 00:11:43,800
 understand how it works.

191
00:11:43,800 --> 00:11:46,880
 We can see that it's actually not a cut-and-dry thing where

192
00:11:46,880 --> 00:11:48,480
 all we can say

193
00:11:48,480 --> 00:11:51,650
 is that certain things are unadvisable because they're

194
00:11:51,650 --> 00:11:52,920
 dangerous. They have the

195
00:11:52,920 --> 00:12:02,400
 potential to lead to our suffering. So first on a

196
00:12:02,400 --> 00:12:06,210
 phenomenological or an experiential level, there are four

197
00:12:06,210 --> 00:12:07,280
 kinds of karma.

198
00:12:07,280 --> 00:12:11,620
 This is based on our every moment of the experience, every

199
00:12:11,620 --> 00:12:13,080
 karma that we perform

200
00:12:13,080 --> 00:12:18,430
 when we get angry, when we have greed in our minds and so

201
00:12:18,430 --> 00:12:20,040
 on, when we give rise to

202
00:12:20,040 --> 00:12:26,600
 these unwholesome states. They will have four results. One

203
00:12:26,600 --> 00:12:29,600
 kind will

204
00:12:29,600 --> 00:12:35,880
 actually give rise to a specific result. So certain acts

205
00:12:35,880 --> 00:12:37,160
 that we perform

206
00:12:37,160 --> 00:12:41,360
 will give a result. Sometimes when you do something bad or

207
00:12:41,360 --> 00:12:41,600
 you do

208
00:12:41,600 --> 00:12:45,120
 something good, right away you experience the result.

209
00:12:45,120 --> 00:12:46,800
 Actually this is generally

210
00:12:46,800 --> 00:12:52,700
 the case that most of the things we do have a immediate

211
00:12:52,700 --> 00:12:54,600
 result and it's just

212
00:12:54,600 --> 00:12:57,130
 that we don't see it or we don't understand the

213
00:12:57,130 --> 00:12:58,720
 relationship between the

214
00:12:58,720 --> 00:13:01,700
 two. For instance, when you're generous with someone, you

215
00:13:01,700 --> 00:13:02,160
 give them

216
00:13:02,160 --> 00:13:06,090
 something or you offer your time to them or your kind words

217
00:13:06,090 --> 00:13:07,680
 or so on, you'll find

218
00:13:07,680 --> 00:13:10,070
 that right away you feel good about yourself. You feel calm

219
00:13:10,070 --> 00:13:10,920
, you feel happy,

220
00:13:10,920 --> 00:13:15,290
 you feel proud in a sense or you feel good about what you

221
00:13:15,290 --> 00:13:17,360
've done. That's

222
00:13:17,360 --> 00:13:24,720
 the direct result that comes from the good karma. But

223
00:13:24,720 --> 00:13:25,760
 certain times

224
00:13:25,760 --> 00:13:29,960
 we'll do something and we're expecting a result of a

225
00:13:29,960 --> 00:13:31,120
 certain sort and

226
00:13:31,120 --> 00:13:35,310
 we don't get that result. In fact, sometimes it isn't the

227
00:13:35,310 --> 00:13:36,320
 case where we give

228
00:13:36,320 --> 00:13:39,510
 something to someone. Sometimes we can give or be generous

229
00:13:39,510 --> 00:13:40,640
 toward someone or do

230
00:13:40,640 --> 00:13:45,040
 something good in one of the various ways and get a bad

231
00:13:45,040 --> 00:13:48,000
 result. We give

232
00:13:48,000 --> 00:13:54,960
 something to someone and they become upset because of it.

233
00:13:54,960 --> 00:13:55,360
 Maybe we

234
00:13:55,360 --> 00:14:00,560
 don't give them what they wanted or you give a person on

235
00:14:00,560 --> 00:14:00,680
 the

236
00:14:00,680 --> 00:14:04,120
 street, you give them a dollar bill and they get angry and

237
00:14:04,120 --> 00:14:04,520
 they wanted

238
00:14:04,520 --> 00:14:09,000
 more. Maybe you give them at one dollar and then all of a

239
00:14:09,000 --> 00:14:09,480
 sudden their

240
00:14:09,480 --> 00:14:15,060
 friend comes and wants another dollar or so on and you feel

241
00:14:15,060 --> 00:14:16,520
 upset by it.

242
00:14:16,520 --> 00:14:27,560
 So only certain types of karma will give a direct result.

243
00:14:27,560 --> 00:14:28,440
 Other types of karma

244
00:14:28,440 --> 00:14:36,600
 have the effect of supporting the arising of results. So

245
00:14:36,600 --> 00:14:37,880
 sometimes we'll

246
00:14:37,880 --> 00:14:41,330
 do something and it doesn't give us a good result right

247
00:14:41,330 --> 00:14:42,120
 away or it doesn't

248
00:14:42,120 --> 00:14:47,220
 give us a bad result right away. But as we perform the same

249
00:14:47,220 --> 00:14:48,400
 action again and

250
00:14:48,400 --> 00:14:50,930
 again or repeatedly perform wholesome or unwholesome

251
00:14:50,930 --> 00:14:53,480
 actions, the accumulation of

252
00:14:53,480 --> 00:15:00,080
 the evil deeds or the good deeds leads to the result of the

253
00:15:00,080 --> 00:15:00,080
 action, leads to the

254
00:15:00,080 --> 00:15:04,870
 bad result or the good result. This is called supportive

255
00:15:04,870 --> 00:15:07,000
 karma. Sometimes it's

256
00:15:07,000 --> 00:15:11,940
 required to do the same act again and again or to perform

257
00:15:11,940 --> 00:15:14,280
 multiple acts in

258
00:15:14,280 --> 00:15:19,560
 order to bring about a result. If supporting factors aren't

259
00:15:19,560 --> 00:15:20,560
 present then

260
00:15:20,560 --> 00:15:26,750
 sometimes our good deeds won't come to good results. So the

261
00:15:26,750 --> 00:15:27,000
 point is

262
00:15:27,000 --> 00:15:31,200
 that we shouldn't feel upset when good deeds don't always

263
00:15:31,200 --> 00:15:31,640
 lead to good

264
00:15:31,640 --> 00:15:40,640
 results or also not be negligent when our bad deeds don't

265
00:15:40,640 --> 00:15:41,440
 always lead to bad

266
00:15:41,440 --> 00:15:46,000
 results. Sometimes they're just waiting for the

267
00:15:46,000 --> 00:15:50,520
 accumulation of bad deeds.

268
00:15:50,520 --> 00:15:58,370
 Sometimes all it takes is one straw that broke the camel's

269
00:15:58,370 --> 00:15:59,080
 back.

270
00:15:59,080 --> 00:16:01,080
 [Music]

