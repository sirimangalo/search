1
00:00:00,000 --> 00:00:02,000
 Note every phenomenon.

2
00:00:02,000 --> 00:00:07,200
 Is it necessary to note every phenomenon that occurs during

3
00:00:07,200 --> 00:00:08,720
 walking meditation?

4
00:00:08,720 --> 00:00:13,880
 No, given the dynamic nature of walking meditation,

5
00:00:13,880 --> 00:00:17,850
 it can actually be a hindrance to try to note everything

6
00:00:17,850 --> 00:00:19,240
 while in motion.

7
00:00:19,240 --> 00:00:23,840
 A general rule of thumb is to ignore small disturbances

8
00:00:23,840 --> 00:00:25,580
 like fleeting thoughts,

9
00:00:25,580 --> 00:00:28,480
 sounds, or sensations.

10
00:00:29,000 --> 00:00:32,480
 Simply bring the mind back to focus on the foot without

11
00:00:32,480 --> 00:00:34,960
 interrupting the movements of the body.

12
00:00:34,960 --> 00:00:36,960
 If the disturbance is

13
00:00:36,960 --> 00:00:40,840
 significant enough to keep the mind away from the foot,

14
00:00:40,840 --> 00:00:43,600
 then stop, bring the feet together,

15
00:00:43,600 --> 00:00:45,960
 noting,

16
00:00:45,960 --> 00:00:47,280
 stopping,

17
00:00:47,280 --> 00:00:51,560
 stopping, and note the phenomenon in the standing position

18
00:00:52,120 --> 00:00:56,680
 until it either goes away or the mind loses interest and

19
00:00:56,680 --> 00:00:59,200
 then resume the walking as normal.

20
00:00:59,200 --> 00:01:01,200
 (

