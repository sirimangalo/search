1
00:00:00,000 --> 00:00:04,000
 Hello everyone, just an update video here.

2
00:00:04,000 --> 00:00:08,000
 Apologies that I haven't put any videos up lately.

3
00:00:08,000 --> 00:00:12,000
 I've been busy with other things. One of which is a

4
00:00:12,000 --> 00:00:16,000
 potential

5
00:00:16,000 --> 00:00:20,000
 monastery. It looks like

6
00:00:20,000 --> 00:00:24,000
 maybe in the near future we'll have a forest

7
00:00:24,000 --> 00:00:28,000
 monastery up and kicking anyway.

8
00:00:28,000 --> 00:00:32,000
 Starting up. So more on that hopefully in the near future

9
00:00:32,000 --> 00:00:36,000
 but that's sort of what's been keeping me busy. In the

10
00:00:36,000 --> 00:00:36,000
 meantime

11
00:00:36,000 --> 00:00:40,000
 I've uploaded the

12
00:00:40,000 --> 00:00:44,000
 how to meditate videos in iPod

13
00:00:44,000 --> 00:00:48,000
 format or in smaller format

14
00:00:48,000 --> 00:00:52,000
 suitable for mobile devices.

15
00:00:52,000 --> 00:00:56,000
 You can download those from our website. The link is down

16
00:00:56,000 --> 00:00:56,000
 there

17
00:00:56,000 --> 00:01:00,000
 in the description. Please let me know

18
00:01:00,000 --> 00:01:03,410
 I don't have such a device so I can't say whether they're

19
00:01:03,410 --> 00:01:04,000
 working or not.

20
00:01:04,000 --> 00:01:08,000
 Let me know if they work otherwise I'll

21
00:01:08,000 --> 00:01:12,000
 upload them again in a different format.

22
00:01:12,000 --> 00:01:16,000
 And yeah, so

23
00:01:16,000 --> 00:01:20,000
 that's all. I'll look for another video hopefully

24
00:01:20,000 --> 00:01:22,830
 in the near future. I'll try to get back to answering

25
00:01:22,830 --> 00:01:24,000
 people's questions. I know there's

26
00:01:24,000 --> 00:01:28,000
 still quite a few that are unanswered.

27
00:01:28,000 --> 00:01:32,000
 Thanks to everyone for tuning in and

28
00:01:32,000 --> 00:01:36,000
 leaving your feedback both good and bad. Happy to get it.

29
00:01:36,000 --> 00:01:40,000
 And keep meditating. All the best.

