1
00:00:00,000 --> 00:00:17,540
 This morning I'll continue with a series of talks on the

2
00:00:17,540 --> 00:00:20,000
 greatest blessings which exist

3
00:00:20,000 --> 00:00:21,000
 in the world.

4
00:00:21,000 --> 00:00:26,000
 I'll stop by the way.

5
00:00:26,000 --> 00:00:33,660
 The next set in order is Araṭi-Virati-pāpāma-japāmā-j

6
00:00:33,660 --> 00:00:34,000
asam-mamā.

7
00:00:34,000 --> 00:00:40,000
 Apāmā-to-cattāmī-tsvait-yītā-mā-gālamatamam.

8
00:00:40,000 --> 00:00:51,150
 We'll be continuing to talk about the Lord Buddha-tādha to

9
00:00:51,150 --> 00:00:55,200
 be things which bring personal

10
00:00:55,200 --> 00:01:02,460
 development which lead one to progress and to develop in

11
00:01:02,460 --> 00:01:04,360
 one's life.

12
00:01:04,360 --> 00:01:13,760
 And here we have a new set of three blessings, three things

13
00:01:13,760 --> 00:01:17,000
 which lead one to develop and

14
00:01:17,000 --> 00:01:22,680
 to progress in one's life.

15
00:01:24,680 --> 00:01:37,680
 One to avoid and abstain from all evil.

16
00:01:37,680 --> 00:01:57,680
 One to avoid and abstain from all evil.

17
00:01:57,680 --> 00:02:09,680
 One to avoid and abstain from all evil.

18
00:02:09,680 --> 00:02:17,680
 One to avoid and abstain from all evil.

19
00:02:17,680 --> 00:02:25,900
 In regards to the dhammas, all dhammas, eta-māṅkālamat

20
00:02:25,900 --> 00:02:29,120
amam, these are things which lead one to development

21
00:02:29,120 --> 00:02:37,680
 and prosperity in one's life.

22
00:02:37,680 --> 00:02:50,270
 So ara-ti-vira-ti-pāpā means to abstain from those things

23
00:02:50,270 --> 00:02:55,200
 which are unwholesome, which

24
00:02:55,200 --> 00:03:03,280
 lead to suffering, those things which are considered to be

25
00:03:03,280 --> 00:03:07,240
 evil in the highest sense.

26
00:03:07,240 --> 00:03:11,080
 As for evil, there are three kinds of evil.

27
00:03:11,080 --> 00:03:16,450
 There's evil by way of body, evil by way of speech, and

28
00:03:16,450 --> 00:03:18,720
 evil by way of mind.

29
00:03:18,720 --> 00:03:24,630
 When a person does evil deeds, it's either by way of one of

30
00:03:24,630 --> 00:03:26,960
 these three doors.

31
00:03:26,960 --> 00:03:33,140
 By way of body, we have killing and stealing and sexual

32
00:03:33,140 --> 00:03:35,000
 misconduct.

33
00:03:35,000 --> 00:03:40,390
 By way of speech, we have lying and divisive speech, harsh

34
00:03:40,390 --> 00:03:43,760
 speech, useless speech, frivolous

35
00:03:43,760 --> 00:03:47,280
 speech.

36
00:03:47,280 --> 00:04:00,560
 By way of mind, we have greed, anger and delusion.

37
00:04:00,560 --> 00:04:04,870
 Having what other people have, being hateful towards other

38
00:04:04,870 --> 00:04:06,960
 people and having wrong view

39
00:04:06,960 --> 00:04:14,920
 or wrong understanding about the world.

40
00:04:14,920 --> 00:04:21,080
 Lord Buddha said, "Aka-tang du-ka-tang-si-yur."

41
00:04:21,080 --> 00:04:25,680
 One should not even do any evil.

42
00:04:25,680 --> 00:04:29,600
 It's just not worth doing.

43
00:04:29,600 --> 00:04:35,130
 A small evil deed is something which we should avoid and

44
00:04:35,130 --> 00:04:38,120
 abstain from in all times.

45
00:04:38,120 --> 00:04:41,680
 We avoid and abstain from evil deeds, there are three ways.

46
00:04:41,680 --> 00:04:43,800
 The first way is to keep morality.

47
00:04:43,800 --> 00:04:50,960
 We make a promise to ourselves not to kill, not to steal

48
00:04:50,960 --> 00:04:52,600
 and so on.

49
00:04:52,600 --> 00:04:58,170
 This is the way to stop the bad deeds of body and speech,

50
00:04:58,170 --> 00:05:01,080
 but the bad deeds of mind are

51
00:05:01,080 --> 00:05:05,480
 much more difficult to root out.

52
00:05:05,480 --> 00:05:10,760
 We practice morality, we keep precepts, we make a promise

53
00:05:10,760 --> 00:05:13,320
 not to kill, not to steal,

54
00:05:13,320 --> 00:05:18,440
 not to commit adultery, not to use wrong speech.

55
00:05:18,440 --> 00:05:23,800
 The five precepts are the eight precepts, the ten precepts

56
00:05:23,800 --> 00:05:26,880
 are even 227 precepts.

57
00:05:26,880 --> 00:05:35,200
 This is considered to be morality.

58
00:05:35,200 --> 00:05:38,660
 The second way we practice meditation, we develop our minds

59
00:05:38,660 --> 00:05:40,040
 and we sit and practice

60
00:05:40,040 --> 00:05:48,840
 rising and falling, watch the breath.

61
00:05:48,840 --> 00:05:54,380
 Our mind develops concentration and focus and is able to do

62
00:05:54,380 --> 00:05:58,160
 away with the bad deeds of mind

63
00:05:58,160 --> 00:06:04,040
 so that our minds become pure.

64
00:06:04,040 --> 00:06:11,500
 We sit and simply acknowledge the simple things which arise

65
00:06:11,500 --> 00:06:13,720
 and cease, we acknowledge the

66
00:06:13,720 --> 00:06:17,630
 pains and the aching which arise and cease in the body

67
00:06:17,630 --> 00:06:19,840
 which come and go and without

68
00:06:19,840 --> 00:06:28,920
 attaching to anything we just watch the truth around us.

69
00:06:28,920 --> 00:06:33,670
 When we do this all of the bad things in the mind, all

70
00:06:33,670 --> 00:06:36,960
 greed and anger and confusion disappear

71
00:06:36,960 --> 00:06:43,760
 temporarily.

72
00:06:43,760 --> 00:06:48,230
 But even though once we practice meditation all of the bad

73
00:06:48,230 --> 00:06:50,640
 things disappear, if we stop

74
00:06:50,640 --> 00:06:56,330
 there and stop at the practice of meditation, our minds

75
00:06:56,330 --> 00:06:59,440
 might be clear and calm at that

76
00:06:59,440 --> 00:07:08,280
 time but they will not be clean in the highest sense.

77
00:07:08,280 --> 00:07:11,100
 So once we sit down and practice meditation, the Lord

78
00:07:11,100 --> 00:07:12,920
 Buddha taught us we need vipa-sana,

79
00:07:12,920 --> 00:07:17,420
 we need to see clearly, we need to come to acknowledge

80
00:07:17,420 --> 00:07:20,440
 everything which arises and ceases

81
00:07:20,440 --> 00:07:23,450
 and after our minds become peaceful and calm we have to pay

82
00:07:23,450 --> 00:07:24,640
 attention to everything which

83
00:07:24,640 --> 00:07:30,410
 arises, even calm feelings we pay attention and acknowledge

84
00:07:30,410 --> 00:07:32,600
 calm, calm, calm.

85
00:07:32,600 --> 00:07:34,750
 When we are thinking about this or about that we

86
00:07:34,750 --> 00:07:36,760
 acknowledge thinking and bring our minds

87
00:07:36,760 --> 00:07:40,800
 always back to the present home, to the rising and the

88
00:07:40,800 --> 00:07:43,720
 falling, to the movements of the foot

89
00:07:43,720 --> 00:07:48,320
 and so on.

90
00:07:48,320 --> 00:07:53,250
 When we practice in this way, watching present reality we

91
00:07:53,250 --> 00:07:56,000
 come to gain wisdom and this can

92
00:07:56,000 --> 00:08:02,240
 destroy all tendency to give rise to defilements.

93
00:08:02,240 --> 00:08:05,170
 Not only do the defilements disappear, the bad things in

94
00:08:05,170 --> 00:08:06,840
 our minds and all the bad deeds

95
00:08:06,840 --> 00:08:09,640
 of body and speech, they disappear.

96
00:08:09,640 --> 00:08:13,120
 But also any tendency for them to arise again, any

97
00:08:13,120 --> 00:08:17,480
 potential is rooted out because of wisdom.

98
00:08:17,480 --> 00:08:23,440
 We know for ourselves what is the truth.

99
00:08:23,440 --> 00:08:25,950
 There is no possibility for any of these bad things to

100
00:08:25,950 --> 00:08:26,760
 arise again.

101
00:08:26,760 --> 00:08:32,260
 Even if we stop practicing, the truth stays with us and be

102
00:08:32,260 --> 00:08:34,600
 with us for all times.

103
00:08:34,600 --> 00:08:40,250
 The Lord Buddha taught us this is something auspicious in

104
00:08:40,250 --> 00:08:42,480
 the highest sense.

105
00:08:42,480 --> 00:08:45,960
 It leads to development, it leads to prosperity.

106
00:08:45,960 --> 00:08:51,350
 When we don't do evil deeds we don't have enemies, we don't

107
00:08:51,350 --> 00:08:54,280
 have fear of the repercussions

108
00:08:54,280 --> 00:08:56,600
 of our bad deeds.

109
00:08:56,600 --> 00:08:59,680
 Our minds are peaceful and calm, we don't have guilt, any

110
00:08:59,680 --> 00:09:01,200
 guilty feelings for the bad

111
00:09:01,200 --> 00:09:03,200
 things we've done.

112
00:09:03,200 --> 00:09:09,900
 When we refrain from evil deeds it's a, on whatever level

113
00:09:09,900 --> 00:09:14,600
 we refrain, it's a cause for

114
00:09:14,600 --> 00:09:19,820
 greater and further development, greater and further

115
00:09:19,820 --> 00:09:21,720
 spiritual gain.

116
00:09:21,720 --> 00:09:25,240
 This is something we should set ourselves in, abstain from

117
00:09:25,240 --> 00:09:27,120
 bad deeds of body, abstain from

118
00:09:27,120 --> 00:09:31,720
 bad deeds of speech, abstain from bad deeds of mind.

119
00:09:31,720 --> 00:09:35,200
 This is the first one.

120
00:09:35,200 --> 00:09:43,950
 "Vajrajpaana jasanyamo" to give up intoxication, alcohol

121
00:09:43,950 --> 00:09:47,760
 and drugs, narcotics, things which

122
00:09:47,760 --> 00:09:57,760
 lead the mind to a drunken, unmindful stain.

123
00:09:57,760 --> 00:10:02,310
 Many people ask why is it so important to give up alcohol,

124
00:10:02,310 --> 00:10:05,000
 to give up drugs, and narcotics

125
00:10:05,000 --> 00:10:07,200
 which lead to intoxication?

126
00:10:07,200 --> 00:10:11,780
 When we come to practice meditation why is it so important

127
00:10:11,780 --> 00:10:13,960
 that we live our lives free

128
00:10:13,960 --> 00:10:19,200
 from these things?

129
00:10:19,200 --> 00:10:23,770
 Most important factor in the development of the mind is

130
00:10:23,770 --> 00:10:27,200
 mindfulness, what we call mindfulness,

131
00:10:27,200 --> 00:10:29,880
 the word sati.

132
00:10:29,880 --> 00:10:33,470
 Sati means to be aware of what's happening, to remind

133
00:10:33,470 --> 00:10:35,560
 yourself all the time what's going

134
00:10:35,560 --> 00:10:38,600
 on around you, to bring yourself to the present moment.

135
00:10:38,600 --> 00:10:42,600
 When we acknowledge rising and falling this is mindfulness.

136
00:10:42,600 --> 00:10:47,840
 This is knowing clearly what's happening.

137
00:10:47,840 --> 00:10:52,990
 When we take drugs or intoxicants or alcohol or any of

138
00:10:52,990 --> 00:10:56,440
 these substances which cloud the

139
00:10:56,440 --> 00:11:02,680
 mind it has the opposite effect of mindfulness.

140
00:11:02,680 --> 00:11:07,360
 Even some substances bring great clarity of mind.

141
00:11:07,360 --> 00:11:09,720
 They can never bring mindfulness.

142
00:11:09,720 --> 00:11:14,640
 In fact they take away the mindfulness of the mind.

143
00:11:14,640 --> 00:11:18,900
 Mindfulness is like the ultimate sobriety, the ultimate

144
00:11:18,900 --> 00:11:21,360
 state of sobriety, being sober

145
00:11:21,360 --> 00:11:24,440
 in the ultimate sense.

146
00:11:24,440 --> 00:11:28,890
 Our minds are not intoxicated by anything whatsoever, not

147
00:11:28,890 --> 00:11:30,600
 intoxicated by sights, by

148
00:11:30,600 --> 00:11:37,000
 sounds, by smells, by taste, by feelings or by thoughts.

149
00:11:37,000 --> 00:11:41,060
 When we take intoxicating substances the purpose of taking

150
00:11:41,060 --> 00:11:43,240
 them and the result from taking

151
00:11:43,240 --> 00:11:49,120
 them is the intoxication of the mind.

152
00:11:49,120 --> 00:11:54,970
 Some sort of happy, blissful state which is unable to

153
00:11:54,970 --> 00:11:58,760
 comprehend the truth of suffering

154
00:11:58,760 --> 00:12:05,800
 or the cause of suffering, unable to see them.

155
00:12:05,800 --> 00:12:12,270
 The disadvantages of clinging, the disadvantages of holding

156
00:12:12,270 --> 00:12:15,680
 on to the eye, the ear, the nose,

157
00:12:15,680 --> 00:12:24,400
 the tongue, the body and the heart, unable to be mindful.

158
00:12:24,400 --> 00:12:31,400
 So we have to start as a base for our practice.

159
00:12:31,400 --> 00:12:36,060
 The abstention from alcohol and narcotics is a base for the

160
00:12:36,060 --> 00:12:38,840
 practice of Vipassana meditation.

161
00:12:38,840 --> 00:12:46,600
 Without it it's impossible to develop meditation in any

162
00:12:46,600 --> 00:12:48,640
 real sense.

163
00:12:48,640 --> 00:12:53,840
 Mindfulness is being sober both in body and in mind.

164
00:12:53,840 --> 00:12:59,710
 If we're not committed to sobriety we can never hope to

165
00:12:59,710 --> 00:13:03,480
 develop in Vipassana meditation

166
00:13:03,480 --> 00:13:10,680
 because our minds will always be clouded and unclear.

167
00:13:10,680 --> 00:13:17,240
 This is also a great auspiciousness.

168
00:13:17,240 --> 00:13:22,630
 You can tell whether someone is going to develop or whether

169
00:13:22,630 --> 00:13:25,560
 they are committed to giving up

170
00:13:25,560 --> 00:13:28,040
 intoxication enough.

171
00:13:28,040 --> 00:13:33,030
 Someone is still indulging in intoxication and intoxicants

172
00:13:33,030 --> 00:13:35,280
 in this way or that way.

173
00:13:35,280 --> 00:13:38,400
 Even not in terms of substances but in terms of the things

174
00:13:38,400 --> 00:13:40,280
 around them, sights and sounds

175
00:13:40,280 --> 00:13:44,200
 and smells and tastes and feelings and thoughts.

176
00:13:44,200 --> 00:13:47,950
 If they're still intoxicated by any one of these things you

177
00:13:47,950 --> 00:13:49,640
 can be sure they're not going

178
00:13:49,640 --> 00:13:53,880
 to develop or it will be difficult for them to develop.

179
00:13:53,880 --> 00:13:57,090
 If you talk to people who have given these things up it's

180
00:13:57,090 --> 00:13:58,880
 quite sure that these people

181
00:13:58,880 --> 00:14:03,560
 will have prosperity and progress in their lives.

182
00:14:03,560 --> 00:14:07,430
 So this is considered a great auspiciousness, something

183
00:14:07,430 --> 00:14:10,040
 which will certainly lead to goodness

184
00:14:10,040 --> 00:14:18,320
 in the future, a blessing in the highest sense.

185
00:14:18,320 --> 00:14:25,340
 Appamadho jatamisu, the third one, the third blessing today

186
00:14:25,340 --> 00:14:28,680
 is in regards to all dhammas,

187
00:14:28,680 --> 00:14:32,320
 all good things and all bad things.

188
00:14:32,320 --> 00:14:37,230
 To have appamada, appamada means the opposite of negligence

189
00:14:37,230 --> 00:14:39,800
, not being negligent, being

190
00:14:39,800 --> 00:14:49,720
 vigilant, being diligent, being hateful, being mindful,

191
00:14:49,720 --> 00:14:54,120
 being aware at all times.

192
00:14:54,120 --> 00:14:57,770
 In regards to dhammas there are two kinds of dhamma, two

193
00:14:57,770 --> 00:14:59,120
 kinds of reality.

194
00:14:59,120 --> 00:15:03,880
 One is good things, one is bad things.

195
00:15:03,880 --> 00:15:11,480
 Good things means all wholesome or beneficial realities.

196
00:15:11,480 --> 00:15:17,450
 Bad things means all unwholesome, all unbeneficial, all un

197
00:15:17,450 --> 00:15:21,240
helpful realities, all things which

198
00:15:21,240 --> 00:15:25,960
 lead to suffering.

199
00:15:25,960 --> 00:15:30,350
 The Lord Buddha taught we should never be negligent in

200
00:15:30,350 --> 00:15:33,800
 regards to these things, never be forgetful,

201
00:15:33,800 --> 00:15:39,880
 never simply let our minds follow after good things or

202
00:15:39,880 --> 00:15:43,080
 follow after bad things.

203
00:15:43,080 --> 00:15:46,790
 In regards to all wholesome and unwholesome things we

204
00:15:46,790 --> 00:15:48,800
 should not be negligent.

205
00:15:48,800 --> 00:15:51,560
 Whatever good things exist in the world means whatever

206
00:15:51,560 --> 00:15:54,080
 wholesome things exist, whatever beneficial

207
00:15:54,080 --> 00:15:59,100
 things, beneficial qualities of mind there are, we should

208
00:15:59,100 --> 00:16:00,920
 develop these things.

209
00:16:00,920 --> 00:16:09,170
 For instance, mindfulness, concentration, effort,

210
00:16:09,170 --> 00:16:12,840
 confidence, wisdom and so on.

211
00:16:12,840 --> 00:16:16,640
 We should not be negligent in developing these things.

212
00:16:16,640 --> 00:16:20,080
 We should at every chance that we get we should try to

213
00:16:20,080 --> 00:16:22,560
 develop these things in our mind.

214
00:16:22,560 --> 00:16:32,740
 We should continue to practice meditation with unending

215
00:16:32,740 --> 00:16:38,520
 effort and concentration and focus

216
00:16:38,520 --> 00:16:41,360
 and confidence and so on.

217
00:16:41,360 --> 00:16:46,450
 We can develop wisdom and mindfulness, become free from

218
00:16:46,450 --> 00:16:47,760
 suffering.

219
00:16:47,760 --> 00:16:51,690
 We should not be negligent in regards to unwholesome things

220
00:16:51,690 --> 00:16:52,480
 either.

221
00:16:52,480 --> 00:16:55,180
 Whatever bad things exist in the mind we should not let

222
00:16:55,180 --> 00:16:56,880
 them simply let them continue and

223
00:16:56,880 --> 00:17:00,560
 persist in our minds.

224
00:17:00,560 --> 00:17:05,490
 We should work our hardest to root them out, to take them

225
00:17:05,490 --> 00:17:07,160
 out of the mind.

226
00:17:07,160 --> 00:17:10,690
 We should make a determination that we will not let these

227
00:17:10,690 --> 00:17:12,520
 things stay in our minds.

228
00:17:12,520 --> 00:17:17,650
 We will continue to practice mindfulness until these things

229
00:17:17,650 --> 00:17:20,440
 have left the mind, until these

230
00:17:20,440 --> 00:17:24,790
 things will no longer come back into the mind, until we

231
00:17:24,790 --> 00:17:27,320
 attain the state of no return so

232
00:17:27,320 --> 00:17:32,080
 that all bad things will never return into our mind.

233
00:17:32,080 --> 00:17:44,370
 No more greed or anger or delusion, stinginess or jealousy,

234
00:17:44,370 --> 00:17:50,880
 conceit or deception or so on.

235
00:17:50,880 --> 00:17:54,050
 Lord Buddha said we should not be negligent in regards to

236
00:17:54,050 --> 00:17:55,840
 good things and bad things.

237
00:17:55,840 --> 00:18:00,690
 Whatever is good we should develop it, keep it in our minds

238
00:18:00,690 --> 00:18:01,000
.

239
00:18:01,000 --> 00:18:04,390
 Whatever is bad we should throw it away and keep it out of

240
00:18:04,390 --> 00:18:06,240
 our minds, keep it away from

241
00:18:06,240 --> 00:18:09,600
 our mind.

242
00:18:09,600 --> 00:18:12,940
 This is a great auspiciousness that leads to happiness and

243
00:18:12,940 --> 00:18:14,560
 peace because the cause of

244
00:18:14,560 --> 00:18:17,920
 all suffering is unwholesome.

245
00:18:17,920 --> 00:18:20,770
 Whatever suffering there is in the world in our lives it

246
00:18:20,770 --> 00:18:22,560
 comes from the unwholesomeness

247
00:18:22,560 --> 00:18:25,570
 of the bad things we have in our minds in the present

248
00:18:25,570 --> 00:18:27,600
 moment, the bad things we've done

249
00:18:27,600 --> 00:18:32,760
 in the past because of our unwholesomeness.

250
00:18:32,760 --> 00:18:35,760
 Whatever happiness there is it comes from wholesomeness,

251
00:18:35,760 --> 00:18:37,280
 from doing good deeds, from

252
00:18:37,280 --> 00:18:42,160
 having a clear mind, a clean mind.

253
00:18:42,160 --> 00:18:47,280
 This is the truth which the Lord Buddha taught.

254
00:18:47,280 --> 00:18:55,000
 When we practice mindfulness in this way we become someone

255
00:18:55,000 --> 00:18:59,160
 free from unwholesomeness with

256
00:18:59,160 --> 00:19:03,920
 a mind full of goodness.

257
00:19:03,920 --> 00:19:08,530
 We become someone who is not negligent in regards to dharma

258
00:19:08,530 --> 00:19:10,760
, in regards to reality.

259
00:19:10,760 --> 00:19:18,380
 It comes to act in accordance with reality, in accordance

260
00:19:18,380 --> 00:19:20,800
 with the truth.

261
00:19:20,800 --> 00:19:22,950
 This is considered a great auspiciousness, it leads to

262
00:19:22,950 --> 00:19:26,640
 happiness and freedom from suffering.

263
00:19:26,640 --> 00:19:32,850
 Today we've gone through three of the greatest auspicious

264
00:19:32,850 --> 00:19:36,840
 blessings which exist in an ultimate

265
00:19:36,840 --> 00:19:41,600
 sense, "Eta Mang, Gala Muthal Mang".

266
00:19:41,600 --> 00:19:41,920
 We'll stop there for today.

267
00:19:41,920 --> 00:19:45,640
 (Chanting)

