1
00:00:00,000 --> 00:00:10,120
 So today is Asalahabhucha, the full moon of Asalah.

2
00:00:10,120 --> 00:00:14,990
 And it's the time when the Lord Buddha had traveled from

3
00:00:14,990 --> 00:00:18,720
 Bodhagaya, which is the place

4
00:00:18,720 --> 00:00:25,010
 where the Lord Buddha became enlightened, all the way to

5
00:00:25,010 --> 00:00:29,880
 the Isipatana, Deer Park, just

6
00:00:29,880 --> 00:00:36,480
 outside of Varanasi, Varanasi as we chant them.

7
00:00:36,480 --> 00:00:39,090
 And he went this distance because he was looking for

8
00:00:39,090 --> 00:00:41,200
 someone who would be able to understand

9
00:00:41,200 --> 00:00:43,600
 his teaching.

10
00:00:43,600 --> 00:00:48,250
 At that time, he figured that most people wouldn't be able

11
00:00:48,250 --> 00:00:50,200
 to understand this very

12
00:00:50,200 --> 00:00:54,320
 difficult to understand thing which he had discovered.

13
00:00:54,320 --> 00:00:58,180
 And so in the beginning he thought not to teach until it

14
00:00:58,180 --> 00:01:00,220
 came to him that there would

15
00:01:00,220 --> 00:01:07,640
 be people who understood the teaching, who could understand

16
00:01:07,640 --> 00:01:09,680
 the teaching.

17
00:01:09,680 --> 00:01:14,330
 And first he thought of his old teachers who had taught him

18
00:01:14,330 --> 00:01:17,200
 tranquility meditation, concentration

19
00:01:17,200 --> 00:01:25,080
 meditation, meditation based on conceptual objects.

20
00:01:25,080 --> 00:01:29,960
 But then he realized that they had passed away already and

21
00:01:29,960 --> 00:01:31,960
 that they had gone on to

22
00:01:31,960 --> 00:01:34,900
 be born in the Brahma realms where of course they couldn't

23
00:01:34,900 --> 00:01:36,880
 practice insight meditation.

24
00:01:36,880 --> 00:01:44,610
 They couldn't practice to understand about suffering and

25
00:01:44,610 --> 00:01:48,160
 the cause of suffering.

26
00:01:48,160 --> 00:01:51,250
 And so then he thought of these five monks who had been

27
00:01:51,250 --> 00:01:53,240
 practicing asceticism with him

28
00:01:53,240 --> 00:01:55,640
 for six years.

29
00:01:55,640 --> 00:02:00,650
 And through his mental power he was able to determine that

30
00:02:00,650 --> 00:02:03,320
 they were in Isipatana, Deer

31
00:02:03,320 --> 00:02:08,810
 Park, still on the wrong path of torturing themselves, self

32
00:02:08,810 --> 00:02:10,580
-motification.

33
00:02:10,580 --> 00:02:11,960
 And so he traveled by foot.

34
00:02:11,960 --> 00:02:22,350
 He, I believe it was 120 miles or something, it's a very

35
00:02:22,350 --> 00:02:25,400
 long distance.

36
00:02:25,400 --> 00:02:30,210
 When he arrived there, the five ascetics, they made a

37
00:02:30,210 --> 00:02:33,240
 determination among themselves

38
00:02:33,240 --> 00:02:39,610
 that they would not receive their old teacher with any sort

39
00:02:39,610 --> 00:02:42,440
 of measure of respect.

40
00:02:42,440 --> 00:02:46,850
 Because what had happened was the Lord Buddha had given up

41
00:02:46,850 --> 00:02:49,040
 austerity in favor of a more

42
00:02:49,040 --> 00:02:52,880
 moderate way of practice.

43
00:02:52,880 --> 00:02:57,410
 And basically what that means is he just gave up any

44
00:02:57,410 --> 00:03:00,560
 attachment to such practices.

45
00:03:00,560 --> 00:03:05,600
 The practices of not eating, the practices of torturing

46
00:03:05,600 --> 00:03:08,320
 oneself in various ways where

47
00:03:08,320 --> 00:03:15,470
 they would sit over a fire or hang upside down or hold one

48
00:03:15,470 --> 00:03:18,560
 arm up for years or so on,

49
00:03:18,560 --> 00:03:22,160
 expose themselves to cold, expose themselves to heat,

50
00:03:22,160 --> 00:03:24,360
 expose themselves to sharp thorns

51
00:03:24,360 --> 00:03:27,600
 and so on.

52
00:03:27,600 --> 00:03:30,420
 And he had given all of this up and a part of it was going

53
00:03:30,420 --> 00:03:31,840
 back to eating because he

54
00:03:31,840 --> 00:03:36,800
 realized that in order to continue practicing on the long

55
00:03:36,800 --> 00:03:39,480
 term, not eating is of course

56
00:03:39,480 --> 00:03:41,480
 not a viable alternative.

57
00:03:41,480 --> 00:03:46,780
 It leads directly to death, which of course is not a very

58
00:03:46,780 --> 00:03:48,440
 useful thing.

59
00:03:48,440 --> 00:03:53,050
 And so the Lord Buddha started eating again and this upset

60
00:03:53,050 --> 00:03:55,440
 these ascetics because they

61
00:03:55,440 --> 00:03:59,080
 thought this was leading back to the indulgent life.

62
00:03:59,080 --> 00:04:01,080
 And for most people it probably would have.

63
00:04:01,080 --> 00:04:04,790
 But most people, for all of them who couldn't control

64
00:04:04,790 --> 00:04:07,680
 themselves, going back to eating would

65
00:04:07,680 --> 00:04:11,020
 have of course allowed their defilements, the kilesa, which

66
00:04:11,020 --> 00:04:12,400
 still existed in them to

67
00:04:12,400 --> 00:04:17,690
 arise again. And without any tools, any practice to allow

68
00:04:17,690 --> 00:04:20,680
 them to control those or to remove

69
00:04:20,680 --> 00:04:24,470
 those defilements, they would have very easily gone back to

70
00:04:24,470 --> 00:04:25,560
 the home life.

71
00:04:25,560 --> 00:04:28,620
 They may have very easily become attracted to objects of

72
00:04:28,620 --> 00:04:30,200
 the sense and given up their

73
00:04:30,200 --> 00:04:39,400
 interest in the holy life or in the ascetic life.

74
00:04:39,400 --> 00:04:43,190
 And so we can see the only way that these ascetics were

75
00:04:43,190 --> 00:04:45,400
 able to keep there and the only

76
00:04:45,400 --> 00:04:48,970
 way many people are able to keep their religious life is

77
00:04:48,970 --> 00:04:51,520
 simply by suppressing or repressing

78
00:04:51,520 --> 00:04:53,650
 their desires. And they do this in various ways by tort

79
00:04:53,650 --> 00:04:55,720
uring themselves.

80
00:04:55,720 --> 00:04:59,580
 Underpinnance or often, I mean of course the easiest way to

81
00:04:59,580 --> 00:05:01,440
 do it was with tranquility

82
00:05:01,440 --> 00:05:06,820
 meditation or concentration meditation, which these men may

83
00:05:06,820 --> 00:05:09,640
 or may not have been practicing.

84
00:05:09,640 --> 00:05:12,390
 But at any rate, their way was to torture themselves, to

85
00:05:12,390 --> 00:05:13,960
 keep themselves in a constant

86
00:05:13,960 --> 00:05:18,150
 state of pain so that they wouldn't be able to think of

87
00:05:18,150 --> 00:05:19,280
 pleasure.

88
00:05:19,280 --> 00:05:24,640
 Some thought to somehow suppress these feelings.

89
00:05:24,640 --> 00:05:27,990
 But anyway, when the Lord Buddha came, they were very upset

90
00:05:27,990 --> 00:05:29,440
 and they didn't want to have

91
00:05:29,440 --> 00:05:33,800
 anything to do with him until they saw how mindful he was.

92
00:05:33,800 --> 00:05:37,870
 And we can see how this affects the way people look at us

93
00:05:37,870 --> 00:05:40,440
 and relate to us even to this day.

94
00:05:40,440 --> 00:05:43,200
 You can realize that when we're mindful, people are much

95
00:05:43,200 --> 00:05:45,200
 more willing to listen to us and

96
00:05:45,200 --> 00:05:49,530
 pay attention to us and treat us kindly and with respect as

97
00:05:49,530 --> 00:05:51,880
 opposed to when we're unmindful

98
00:05:51,880 --> 00:05:56,480
 and distracted. We can see we're actually causing suffering

99
00:05:56,480 --> 00:05:57,440
 for other people just by

100
00:05:57,440 --> 00:06:00,350
 being around them and they're not comfortable or happy

101
00:06:00,350 --> 00:06:01,440
 being around us.

102
00:06:01,440 --> 00:06:04,740
 But in this case, the Lord Buddha was so mindful and

103
00:06:04,740 --> 00:06:07,200
 clearly aware that they couldn't help

104
00:06:07,200 --> 00:06:12,710
 but undertake all of the tokens of respect, like taking his

105
00:06:12,710 --> 00:06:16,080
 robe, taking his bowl, preparing

106
00:06:16,080 --> 00:06:21,160
 a seed, standing up to greet him.

107
00:06:21,160 --> 00:06:25,310
 When they sat down, the Lord Buddha tried to instruct them

108
00:06:25,310 --> 00:06:27,080
 and explain to them that

109
00:06:27,080 --> 00:06:30,370
 he had become enlightened. And they said, "How in the world

110
00:06:30,370 --> 00:06:33,480
 could you have become enlightened?

111
00:06:33,480 --> 00:06:36,840
 You hadn't become enlightened when you were undertaking the

112
00:06:36,840 --> 00:06:38,440
 ascetic practices. How could

113
00:06:38,440 --> 00:06:41,920
 you become enlightened when you've given them up?"

114
00:06:41,920 --> 00:06:45,240
 And the Lord Buddha said again, "I've listened to me. I

115
00:06:45,240 --> 00:06:47,800
 really have become enlightened."

116
00:06:47,800 --> 00:06:51,660
 And for three times he told them, three times they derided

117
00:06:51,660 --> 00:06:54,200
 him, they chided him, they tried

118
00:06:54,200 --> 00:07:01,250
 to poke fun at him, or they denied the possibility. The

119
00:07:01,250 --> 00:07:02,960
 Lord Buddha then asked them if he had

120
00:07:02,960 --> 00:07:07,200
 ever said this before to them, if he had ever said such a

121
00:07:07,200 --> 00:07:08,880
 thing before. I mean, was this

122
00:07:08,880 --> 00:07:12,100
 the kind of thing that he would do if he played these kind

123
00:07:12,100 --> 00:07:14,640
 of practical jokes on them or overestimate

124
00:07:14,640 --> 00:07:17,600
 himself? And they realized at that point, it was a turning

125
00:07:17,600 --> 00:07:18,480
 point. They realized that,

126
00:07:18,480 --> 00:07:21,390
 no, the Lord Buddha would never have, or their teacher

127
00:07:21,390 --> 00:07:23,360
 would, you know, he'd never done this

128
00:07:23,360 --> 00:07:28,430
 before. He wasn't accustomed to playing jokes or lying or

129
00:07:28,430 --> 00:07:31,240
 tricking or overestimating himself.

130
00:07:31,240 --> 00:07:35,350
 In fact, he had always been, of course, a very straight and

131
00:07:35,350 --> 00:07:37,160
 honest sort of person. So

132
00:07:37,160 --> 00:07:39,460
 at this point they changed, and they were then, from then

133
00:07:39,460 --> 00:07:40,680
 on, willing to listen to what

134
00:07:40,680 --> 00:07:43,530
 he had to say. And that's when he started the Dhammacakapu

135
00:07:43,530 --> 00:07:45,400
watana sutta, which is the

136
00:07:45,400 --> 00:07:50,150
 first discourse of the Lord Buddha. Dhammacakapuwatana

137
00:07:50,150 --> 00:07:52,400
 means turning. "Chakka" is the word.

138
00:07:52,480 --> 00:07:56,350
 "Chakka" is the wheel, and "dhamma" is the teaching or the

139
00:07:56,350 --> 00:07:58,640
 truth. So this is the turning

140
00:07:58,640 --> 00:08:01,900
 wheel of the "dhamma," the turning wheel of the "dhamma."

141
00:08:01,900 --> 00:08:03,520
 This is what this discourse

142
00:08:03,520 --> 00:08:09,160
 is about. And it's a fairly simple teaching. The beginning

143
00:08:09,160 --> 00:08:12,520
 is a fairly widely cited teaching.

144
00:08:12,520 --> 00:23:57,590
 It's this talk about the "dwei mei picuwei anta." There are

145
00:23:57,590 --> 00:08:20,080
 these two extremes, "anta"

146
00:08:22,080 --> 00:08:25,230
 means end, or it actually is the cognate, I think, of the

147
00:08:25,230 --> 00:08:26,960
 English word end. And if you

148
00:08:26,960 --> 00:08:31,280
 look at it, the word looks very similar, "anta" and end.

149
00:08:31,280 --> 00:08:33,960
 And I believe it's the same word.

150
00:08:33,960 --> 00:08:39,260
 But here we're looking at ends, we mean extremes. So if we

151
00:08:39,260 --> 00:08:42,880
 have a stick, the stick has two ends.

152
00:08:42,880 --> 00:08:46,510
 And so here we're talking about the two extremes of the

153
00:08:46,510 --> 00:08:49,120
 stick, or the two extremes of the holy

154
00:08:49,120 --> 00:08:52,880
 life in this case. And of course these two extremes are

155
00:08:52,880 --> 00:08:55,040
 torturing oneself and sensual

156
00:08:55,040 --> 00:08:59,550
 indulgence. Now, the Lord Buddha didn't often come back to

157
00:08:59,550 --> 00:09:01,280
 this teaching. So it's important

158
00:09:01,280 --> 00:09:04,560
 to understand that actually we're not dealing with an

159
00:09:04,560 --> 00:09:06,800
 extremely core teaching of the Lord

160
00:09:06,800 --> 00:09:10,510
 Buddha. It's not that this is a core principle of Buddhism.

161
00:09:10,510 --> 00:09:12,320
 And it's often, why I say that

162
00:09:12,320 --> 00:09:15,810
 is because it's often misused. They say, "Well, the Buddha

163
00:09:15,810 --> 00:09:17,520
 taught the middle way." And it's

164
00:09:17,520 --> 00:09:21,470
 true, he did, and he repeats it several times in the tibet

165
00:09:21,470 --> 00:09:23,840
ica. And he uses it in different

166
00:09:23,840 --> 00:09:28,130
 ways. Sometimes it's not saying yes and not saying no, but

167
00:09:28,130 --> 00:09:30,080
 giving up the whole idea of

168
00:09:30,080 --> 00:09:35,720
 the question in a specific case. But here, of course, it's

169
00:09:35,720 --> 00:09:37,080
 clear why he's saying this

170
00:09:37,080 --> 00:09:39,900
 because he's talking to people who are torturing themselves

171
00:09:39,900 --> 00:09:44,120
. So first he says that, yes, clearly

172
00:09:44,240 --> 00:09:46,520
 sensual pleasure is not a good thing. And he does this to

173
00:09:46,520 --> 00:09:47,960
 reaffirm the fact that he hasn't

174
00:09:47,960 --> 00:09:50,800
 gone back to indulgence and sensual pleasure. Because of

175
00:09:50,800 --> 00:09:52,480
 course, indulgence and sensual

176
00:09:52,480 --> 00:09:55,620
 pleasure is something which leads to addiction, which leads

177
00:09:55,620 --> 00:09:57,200
 to attachment, which leads to

178
00:09:57,200 --> 00:10:01,460
 all sorts of things like acquiring and giving birth to

179
00:10:01,460 --> 00:10:04,200
 children and debt and all sorts of

180
00:10:10,640 --> 00:10:16,710
 attachments, all sorts of things that bind, all sorts of

181
00:10:16,710 --> 00:10:17,640
 ties. But then he goes on to

182
00:10:17,640 --> 00:10:23,060
 say, "Well, then the other side of the coin is that tort

183
00:10:23,060 --> 00:10:26,400
uring oneself is also useless.

184
00:10:26,400 --> 00:10:28,490
 It's clear that we can torture ourselves until we die and

185
00:10:28,490 --> 00:10:30,040
 we still be nothing from it."

186
00:10:30,040 --> 00:10:34,470
 He said it was duko, something which causes suffering. An

187
00:10:34,470 --> 00:10:36,840
ali yo, he know it's something

188
00:10:36,840 --> 00:10:40,760
 which is inferior. No, sorry, it's duko. The other one is h

189
00:10:40,760 --> 00:10:42,440
ino. This one is duko, which

190
00:10:42,440 --> 00:10:46,050
 means it's painful. Anali yo, it's not noble. There's

191
00:10:46,050 --> 00:10:48,680
 nothing noble about torturing yourself.

192
00:10:48,680 --> 00:10:53,740
 So he was giving a fairly harsh criticism of this sort of

193
00:10:53,740 --> 00:10:55,680
 practice as well. And then

194
00:10:55,680 --> 00:11:00,970
 he goes on to explain the middle way. And this is the core

195
00:11:00,970 --> 00:11:03,840
 of Buddhism. It's sometimes

196
00:11:03,840 --> 00:11:07,080
 called the middle way, but it's most often called the eight

197
00:11:07,080 --> 00:11:08,800
fold noble way. It's a noble

198
00:11:08,800 --> 00:11:13,070
 path. It's a path which leads one to become noble, which

199
00:11:13,070 --> 00:11:15,800
 leads one to become enlightened.

200
00:11:15,800 --> 00:11:20,640
 And so this is the eightfold noble path. Then he goes on to

201
00:11:20,640 --> 00:11:24,200
 explain the Four Noble Truths.

202
00:11:24,200 --> 00:11:28,460
 And he just sort of breaks into it. It's not really

203
00:11:28,460 --> 00:11:31,200
 explained very clearly, but he's

204
00:11:33,720 --> 00:11:37,340
 basically trying to, from this point on, here he's got them

205
00:11:37,340 --> 00:11:39,680
. He's explained to them about

206
00:11:39,680 --> 00:11:43,340
 what is the right path that they should be following. And

207
00:11:43,340 --> 00:11:44,240
 then once they understand that

208
00:11:44,240 --> 00:11:47,250
 this is actually a pretty profound thing, torturing oneself

209
00:11:47,250 --> 00:11:48,680
 is actually a pretty simple

210
00:11:48,680 --> 00:11:51,590
 and kind of stupid thing to do. But here we have something

211
00:11:51,590 --> 00:11:52,680
 which is quite profound. It's

212
00:11:52,680 --> 00:11:57,120
 got right view, right thought, right speech, right action,

213
00:11:57,120 --> 00:11:59,680
 right livelihood, right effort,

214
00:12:00,200 --> 00:12:03,220
 right mindfulness and right concentration. It's not

215
00:12:03,220 --> 00:12:05,280
 something that's easily grasped.

216
00:12:05,280 --> 00:12:07,990
 So now he's got them and then he starts to go on to the

217
00:12:07,990 --> 00:12:10,480
 Four Noble Truths. And of course

218
00:12:10,480 --> 00:12:14,680
 the Four Noble Truths are dukha, the truth of suffering, d

219
00:12:14,680 --> 00:12:17,480
ukasamutaya, the cause of suffering,

220
00:12:17,480 --> 00:12:21,790
 dukanirodha, which is the cessation of suffering, and dukan

221
00:12:21,790 --> 00:12:24,480
irodha bati gamini bati bata, which

222
00:12:24,480 --> 00:12:28,730
 is actually the path which leads the cessation of suffering

223
00:12:28,730 --> 00:12:31,080
, which is actually the hateful

224
00:12:31,080 --> 00:12:35,720
 noble path which he just discussed. And each of these Four

225
00:12:35,720 --> 00:12:38,080
 Noble Truths is given in some

226
00:12:38,080 --> 00:12:44,400
 short explanation is given for each of them. So the truth

227
00:12:44,400 --> 00:12:45,960
 of suffering is that birth is

228
00:12:45,960 --> 00:12:49,980
 suffering, chaatipi dukha, jalapi dukha, old age is

229
00:12:49,980 --> 00:12:52,960
 suffering, malanampi dukha, death is

230
00:12:53,160 --> 00:12:57,030
 suffering, soka pari deva dukha domana subaya sapi dukha,

231
00:12:57,030 --> 00:13:00,160
 sorrow, lamentation, pain, distress,

232
00:13:00,160 --> 00:13:04,920
 and despair are suffering. Api hei hisampayoko dukho, the

233
00:13:04,920 --> 00:13:07,160
 association with things disliked,

234
00:13:07,160 --> 00:13:12,580
 dukhe dukho, pia hi vipayogo dukho, the separation from

235
00:13:12,580 --> 00:13:16,760
 things which is like, stress is suffering,

236
00:13:22,320 --> 00:13:26,730
 that which is not getting what one wants is suffering, and

237
00:13:26,730 --> 00:13:28,920
 in brief the five aggregates

238
00:13:28,920 --> 00:13:34,970
 of clinging are suffering. And this last part is the

239
00:13:34,970 --> 00:13:35,920
 beginning of meditation practice, when

240
00:13:35,920 --> 00:13:39,720
 we talk about the five aggregates. And so he may have

241
00:13:39,720 --> 00:13:42,200
 explained at this time what these

242
00:13:42,200 --> 00:13:46,310
 five were and we don't have it recorded here. It may have

243
00:13:46,310 --> 00:13:48,800
 just been understood by them that

244
00:13:48,800 --> 00:13:51,740
 these five aggregates existed. But it seems likely, more

245
00:13:51,740 --> 00:13:53,680
 likely that the Lord Buddha explained

246
00:13:53,680 --> 00:13:56,750
 some of this out because the five aggregates of course were

247
00:13:56,750 --> 00:13:58,400
 not, as far as we understand,

248
00:13:58,400 --> 00:14:01,600
 they were not known before the time of the Lord Buddha. It

249
00:14:01,600 --> 00:14:03,240
 took the Lord Buddha to explain

250
00:14:03,240 --> 00:14:06,180
 these five things. This is one of the things we give the

251
00:14:06,180 --> 00:14:07,880
 Lord Buddha great credit for,

252
00:14:07,880 --> 00:14:14,880
 being able to separate the being out into five parts. And

253
00:14:14,880 --> 00:14:14,880
 so then we have, you know,

254
00:14:16,520 --> 00:14:20,290
 what is, this is the truth of suffering is actually pretty

255
00:14:20,290 --> 00:14:22,220
 much everything that arises

256
00:14:22,220 --> 00:14:25,390
 can be said to be the cause, can be said to be suffering.

257
00:14:25,390 --> 00:14:26,780
 And what it means by suffering

258
00:14:26,780 --> 00:14:30,330
 is something which causes us, causes us suffering when we

259
00:14:30,330 --> 00:14:32,600
 hold on to it, when we cling to it.

260
00:14:32,600 --> 00:14:37,370
 And this is what it means, upadhanakandha. Upadhanha means

261
00:14:37,370 --> 00:14:39,600
 clinging, and kandha is the

262
00:14:39,600 --> 00:14:43,760
 aggregates. So some people often accuse Buddhism of saying

263
00:14:43,760 --> 00:14:46,600
 that life is suffering, for instance.

264
00:14:46,600 --> 00:14:49,690
 And of course the Lord Buddha never once said that life is

265
00:14:49,690 --> 00:14:51,480
 suffering. Life doesn't have

266
00:14:51,480 --> 00:14:54,650
 to be suffering. But anything that we cling to, that thing

267
00:14:54,650 --> 00:14:56,200
 is only a cause of suffering

268
00:14:56,200 --> 00:14:59,840
 for us. It's a painful thing. Just like a fire is something

269
00:14:59,840 --> 00:15:01,280
 that can be very painful

270
00:15:01,280 --> 00:15:04,700
 if you touch it, if you grasp it, if you hold on to it.

271
00:15:04,700 --> 00:15:07,200
 Something that is very hot, something

272
00:15:07,200 --> 00:15:10,540
 that is very sharp. If you hold on to it, it can give you

273
00:15:10,540 --> 00:15:12,400
 great pain and suffering.

274
00:15:12,400 --> 00:15:15,770
 Of course if you don't hold on to it, if you don't cling to

275
00:15:15,770 --> 00:15:18,000
 it, then it can cause you suffering.

276
00:15:18,000 --> 00:15:22,240
 It cannot cause you suffering. So this is the first noble

277
00:15:22,240 --> 00:15:23,760
 truth. The second one is what

278
00:15:23,760 --> 00:15:27,510
 is the cause of suffering? And the cause of suffering is,

279
00:15:27,510 --> 00:15:29,480
 of course, this clinging. Or

280
00:15:29,480 --> 00:15:31,980
 it's actually, let's be more clear, the cause of suffering,

281
00:15:31,980 --> 00:15:33,440
 the Lord Buddha said, is what

282
00:15:33,440 --> 00:15:37,050
 is the cause of clinging? And actually clinging itself has

283
00:15:37,050 --> 00:15:39,160
 a cause, and that cause is craving

284
00:15:39,160 --> 00:15:43,620
 or wanting, desiring something. And so these things are,

285
00:15:43,620 --> 00:15:45,000
 some people maybe think that these

286
00:15:45,000 --> 00:15:50,060
 two things are the same. And in terms of the abhidhamma,

287
00:15:50,060 --> 00:15:52,000
 they're very similar. But craving

288
00:15:52,000 --> 00:15:56,520
 is a very small sort of wanting. When you see something and

289
00:15:56,520 --> 00:15:58,240
 you want it or you like

290
00:15:58,240 --> 00:16:03,050
 it, it then leads to what we might say in English,

291
00:16:03,050 --> 00:16:05,240
 addiction. So the addiction is what

292
00:16:05,240 --> 00:16:09,280
 we mean by upadana. When we like something, we may not be

293
00:16:09,280 --> 00:16:10,880
 addicted to it, but once we

294
00:16:10,880 --> 00:16:13,700
 get it again and again and again and we reinforce this

295
00:16:13,700 --> 00:16:15,960
 liking, that's what leads to a clinging

296
00:16:15,960 --> 00:16:20,110
 and this state of not being able to let go and the needing

297
00:16:20,110 --> 00:16:22,480
 of something to keep us happy.

298
00:16:22,480 --> 00:16:26,980
 When we don't get it, of course, we're very unhappy. And

299
00:16:26,980 --> 00:16:29,120
 this is why this is the cause

300
00:16:29,120 --> 00:16:31,600
 of suffering. The third noble truth is the cessation of

301
00:16:31,600 --> 00:16:33,720
 suffering. And what is the cessation

302
00:16:33,720 --> 00:16:40,720
 of suffering? Yayang tanha. Yayang tanha. Where do we have

303
00:16:40,720 --> 00:16:40,720
 it? Yota sa yoyo. Yoyo, yoyo,

304
00:16:40,720 --> 00:16:47,720
 yoyo, yoyo. The cessation without any remainder of tanha,

305
00:16:47,720 --> 00:16:47,720
 of craving. This is the cessation

306
00:17:07,160 --> 00:17:10,680
 of suffering. The remainderless fading and cessation, ren

307
00:17:10,680 --> 00:17:13,440
unciation, relinquishment, release,

308
00:17:13,440 --> 00:17:18,330
 and letting go of that very craving. And so this is the

309
00:17:18,330 --> 00:17:20,440
 goal of insight meditation, is

310
00:17:20,440 --> 00:17:24,520
 to let go of the craving. When we see impermanence, when we

311
00:17:24,520 --> 00:17:27,040
 see suffering, when we see non-self

312
00:17:27,040 --> 00:17:31,580
 in the things which we contemplate, this leads us to let go

313
00:17:31,580 --> 00:17:34,040
. It leads us to give up and to

314
00:17:34,840 --> 00:17:39,480
 be able to be free from our attachment, free from suffering

315
00:17:39,480 --> 00:17:40,720
. And how do we do that? And

316
00:17:40,720 --> 00:17:42,940
 then, of course, the fourth noble truth, we come back again

317
00:17:42,940 --> 00:17:44,080
 to the middle way, which is

318
00:17:44,080 --> 00:17:48,900
 again the eightfold noble path. And Mahasya Sayyada, he

319
00:17:48,900 --> 00:17:50,760
 comments that probably the Lord

320
00:17:50,760 --> 00:17:55,500
 Buddha gave more detail in regards to the eightfold noble

321
00:17:55,500 --> 00:17:57,760
 path, just as he did in the

322
00:18:00,920 --> 00:18:04,950
 vatyvatana sutta, where we have two versions of it. Now,

323
00:18:04,950 --> 00:18:06,760
 the Dhamma Jacopawattana sutta,

324
00:18:06,760 --> 00:18:09,390
 we only have one version, but it's also possible that the

325
00:18:09,390 --> 00:18:11,000
 Lord Buddha actually gave it more

326
00:18:11,000 --> 00:18:14,180
 detailed explanation, and all we have is a sort of

327
00:18:14,180 --> 00:18:16,600
 streamlined version, which is very

328
00:18:16,600 --> 00:18:23,390
 easy for chanting. So after explaining the four noble

329
00:18:23,390 --> 00:18:23,600
 truths, then the Lord Buddha goes

330
00:18:25,320 --> 00:18:32,320
 on to explain how he came to understand these noble truths.

331
00:18:32,320 --> 00:18:32,320
 And before he understood these

332
00:18:32,320 --> 00:18:41,040
 noble truths, he would never have said he was enlightened.

333
00:18:41,040 --> 00:18:41,040
 "Ne wa dawahang bikawe."

334
00:18:41,040 --> 00:18:47,490
 Neither did I say that I was enlightened. For as long as I

335
00:18:47,490 --> 00:18:50,440
 didn't understand these

336
00:18:52,560 --> 00:18:56,910
 four noble truths, for so long, up until that point, I

337
00:18:56,910 --> 00:18:59,560
 never said that I was a Buddha. But

338
00:18:59,560 --> 00:19:04,080
 once I understood these four noble truths, from then on,

339
00:19:04,080 --> 00:19:06,320
 then I said that I was indeed

340
00:19:06,320 --> 00:19:12,670
 a Buddha, fully enlightened. And then he goes on to say, "

341
00:19:12,670 --> 00:19:13,320
What is the benefit of this?

342
00:19:13,320 --> 00:19:17,020
 Yana-nacyapana-meyta-sanangudapadi." "The knowledge and

343
00:19:17,020 --> 00:19:20,080
 vision arose in me." "Akupa-meyimudti."

344
00:19:21,040 --> 00:19:25,050
 "My release is unshakable." "I am antima-chati." "This is

345
00:19:25,050 --> 00:19:28,040
 my last birth." "Nati dani puna

346
00:19:28,040 --> 00:19:34,130
 pawoti." "There is no further becoming." This is what he

347
00:19:34,130 --> 00:19:35,040
 said. And the result of the

348
00:19:35,040 --> 00:19:41,040
 teaching was that Anya-kundanya came to understand what we

349
00:19:41,040 --> 00:19:44,360
 call the "I" of Dhamma, and this

350
00:19:45,720 --> 00:19:50,890
 is of course the most important phrase in the practice of

351
00:19:50,890 --> 00:19:52,720
 Buddhism, is that "yankinci-somutaya

352
00:19:52,720 --> 00:20:04,160
 dhamma-ang sapantang-ni rota-dhammanti." Whatever dhammas,

353
00:20:04,160 --> 00:20:04,600
 whatever is the subject or has as

354
00:20:04,600 --> 00:20:11,080
 its nature arising, whatever is of the nature to arise, all

355
00:20:11,080 --> 00:20:14,240
 of that, every such thing, whatever

356
00:20:15,160 --> 00:20:19,460
 every such thing is of the nature to cease. And as Ajahn P

357
00:20:19,460 --> 00:20:22,160
ramod said in Minnesota, he

358
00:20:22,160 --> 00:20:27,150
 was explaining this, he said, "You know, it seems very

359
00:20:27,150 --> 00:20:30,360
 simple, it's very simple and basic

360
00:20:30,360 --> 00:20:33,810
 teaching. Everything that arises has to cease, but we don't

361
00:20:33,810 --> 00:20:35,680
 really realize this. We don't

362
00:20:35,680 --> 00:20:38,620
 really see this with our hearts. We can accept it

363
00:20:38,620 --> 00:20:41,260
 intellectually very easy. It's very easy

364
00:20:41,260 --> 00:20:45,670
 to accept. But the truth is we don't really see this about

365
00:20:45,670 --> 00:20:48,160
 things. When something comes

366
00:20:48,160 --> 00:20:50,450
 up we hold on to it, and then when it disappears we're

367
00:20:50,450 --> 00:20:52,280
 upset. Well clearly this means that

368
00:20:52,280 --> 00:20:56,540
 we don't understand yet that everything has to cease. It's

369
00:20:56,540 --> 00:20:58,320
 like we have this kid inside

370
00:20:58,320 --> 00:21:02,670
 of us who is just learning and we already know in our mind

371
00:21:02,670 --> 00:21:04,720
 all of these things, but

372
00:21:04,720 --> 00:21:08,570
 our heart is like a child. It's something like this that we

373
00:21:08,570 --> 00:21:10,320
 really don't understand

374
00:21:10,320 --> 00:21:13,420
 what we think we understand. And so when we practice

375
00:21:13,420 --> 00:21:14,640
 meditation we realize that we're

376
00:21:14,640 --> 00:21:18,170
 actually clinging to things which are impermanent. And the

377
00:21:18,170 --> 00:21:20,320
 more we practice the more we're able

378
00:21:20,320 --> 00:21:22,720
 to see that these things, you know, there's no reason to

379
00:21:22,720 --> 00:21:24,200
 get upset about them because they're

380
00:21:24,200 --> 00:21:27,600
 just going to cease. They're just going to fade away. But

381
00:21:27,600 --> 00:21:30,040
 we can't just say this to ourselves

382
00:21:30,040 --> 00:21:32,720
 and say, "Yeah, I know, of course, I understand that." As

383
00:21:32,720 --> 00:21:34,360
 long as we're still suffering it's

384
00:21:34,360 --> 00:21:36,780
 a sign that we still don't understand that because we're

385
00:21:36,780 --> 00:21:38,120
 still holding on to things,

386
00:21:38,120 --> 00:21:41,150
 we have understanding that they have to change and they

387
00:21:41,150 --> 00:21:43,280
 have to disappear. And even though

388
00:21:43,280 --> 00:21:47,330
 we may get what we want, that we can't have what we want

389
00:21:47,330 --> 00:21:49,080
 all the time. And so this was

390
00:21:49,080 --> 00:21:51,910
 considered the point where Kuntanya understood the Dhamma.

391
00:21:51,910 --> 00:21:53,320
 It's called the realization of

392
00:21:53,320 --> 00:21:57,730
 Sodhapanna. And as we understand it in Abhidhamma it means

393
00:21:57,730 --> 00:22:00,320
 that he actually entered into the

394
00:22:00,320 --> 00:22:04,350
 realization of Nibbana, or complete cessation, where there

395
00:22:04,350 --> 00:22:06,840
 was no arising. There was no thinking.

396
00:22:06,840 --> 00:22:09,750
 There was none of the five aggregates. There was no… Well

397
00:22:09,750 --> 00:22:11,200
, the body would have still

398
00:22:11,200 --> 00:22:15,370
 been there, but there was no realization or perception of

399
00:22:15,370 --> 00:22:18,000
 the body. There was no feeling.

400
00:22:18,000 --> 00:22:22,100
 There was no memories arising, no thoughts arising, and no

401
00:22:22,100 --> 00:22:24,920
 consciousness perceiving anything.

402
00:22:24,920 --> 00:22:28,170
 There was only… If you may say that there was the

403
00:22:28,170 --> 00:22:30,760
 perception of Nibbana, but of course

404
00:22:30,760 --> 00:22:36,280
 this is very much like a non-perception or a non-precipient

405
00:22:36,280 --> 00:22:39,080
 state. But we have to be

406
00:22:39,080 --> 00:22:41,450
 careful. We think of it… Actually, we understand it to

407
00:22:41,450 --> 00:22:42,960
 just to mean that the mind was free

408
00:22:42,960 --> 00:22:46,450
 at that moment. Just like the mind was not… The mind is

409
00:22:46,450 --> 00:22:48,600
 something which grasps and which

410
00:22:48,600 --> 00:22:51,970
 holds on to things at all times. Here there was no grasping

411
00:22:51,970 --> 00:22:53,600
 and no holding on. So the mind

412
00:22:53,600 --> 00:23:00,200
 was like a bird flying free and not holding on to the tree.

413
00:23:00,200 --> 00:23:01,720
 Just for that moment, that

414
00:23:01,720 --> 00:23:07,110
 realization was enough to change Kondanya's life. From that

415
00:23:07,110 --> 00:23:09,560
 point on, he was able to let

416
00:23:09,560 --> 00:23:15,560
 go, able to be free, at least to some extent. Then of

417
00:23:15,560 --> 00:23:16,860
 course for the next five days they

418
00:23:16,860 --> 00:23:21,300
 went on to continue the practice which the Lord Buddha had

419
00:23:21,300 --> 00:23:23,520
 taught until all of the five

420
00:23:23,520 --> 00:23:27,920
 disciples became perfectly… Or became arahants, became

421
00:23:27,920 --> 00:23:31,600
 completely free from the kilesa, the

422
00:23:31,600 --> 00:23:34,900
 greed and anger and delusion, the defilements which exist

423
00:23:34,900 --> 00:23:36,560
 inside themselves. So probably

424
00:23:36,560 --> 00:23:39,230
 for the next day we'll be going over some of the other

425
00:23:39,230 --> 00:23:40,800
 teachings of the Lord Buddha

426
00:23:40,800 --> 00:23:44,800
 and on the fifth day we will chant the Anattalaka Nisuddha.

427
00:23:44,800 --> 00:23:47,780
 But for today, this is a summary

428
00:23:47,780 --> 00:23:52,020
 of the Dhamma Jacopawattana Sutta in brief. And now we can

429
00:23:52,020 --> 00:23:54,240
 start our meditation. First

430
00:23:54,240 --> 00:23:56,240
 we do mindful postulation and walking in the city.

431
00:23:56,240 --> 00:24:22,820
 [

