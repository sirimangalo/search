1
00:00:00,000 --> 00:00:24,520
 [Silence]

2
00:00:24,520 --> 00:00:38,270
 Good evening. Broadcasting live from on October 5th 2015.

3
00:00:38,270 --> 00:00:40,720
 So last night we did a

4
00:00:40,720 --> 00:00:44,960
 Dhammapada video. The intention is to do another one today.

5
00:00:44,960 --> 00:00:46,880
 Last night the audio

6
00:00:46,880 --> 00:00:52,500
 was really messed up. There it was terrible terrible static

7
00:00:52,500 --> 00:00:54,000
. It's one of the

8
00:00:54,000 --> 00:00:58,310
 connections. Somehow this very expensive microphone set has

9
00:00:58,310 --> 00:00:59,480
 really kind of

10
00:00:59,480 --> 00:01:05,200
 crappy connections. So it was actually like the socket. I

11
00:01:05,200 --> 00:01:07,480
 don't know. Anyway more or

12
00:01:07,480 --> 00:01:13,630
 less unusable. So I had to use the online audio which is

13
00:01:13,630 --> 00:01:15,520
 not very good quality

14
00:01:15,520 --> 00:01:19,530
 either. So trying something new today and that's to use

15
00:01:19,530 --> 00:01:23,520
 this big mic for the audio.

16
00:01:23,520 --> 00:01:32,480
 We got the camera plugged into the mixer board thing. I'm

17
00:01:32,480 --> 00:01:33,600
 gonna do a Dhammapada

18
00:01:33,600 --> 00:01:44,880
 video okay. Test test. I think it's too loud. Test test

19
00:01:44,880 --> 00:01:52,160
 test test. Test.

20
00:01:52,160 --> 00:01:58,760
 Okay. Never want it too loud when you record because you

21
00:01:58,760 --> 00:02:02,360
 can always increase

22
00:02:02,360 --> 00:02:07,080
 the volume later. But with you anyway you don't want to

23
00:02:07,080 --> 00:02:14,760
 hear that. Ready? Good evening.

24
00:02:14,760 --> 00:02:19,210
 Welcome back to our study of the Dhammapada. Today we

25
00:02:19,210 --> 00:02:20,400
 continue on with

26
00:02:20,400 --> 00:02:26,280
 verse number 78 which reads as follows.

27
00:02:26,280 --> 00:02:31,120
 Nabhaje papa ke mitae Nabhaje puri sadhame

28
00:02:31,120 --> 00:02:41,120
 pajeta kalyanai mitae pajeta puri sutami. Which means don't

29
00:02:41,120 --> 00:02:42,480
 one should not

30
00:02:42,480 --> 00:02:49,870
 associate with an evil friend. One should not associate

31
00:02:49,870 --> 00:02:52,920
 with a low person. One should

32
00:02:52,920 --> 00:02:59,400
 associate with a kalyanai mitae, a beautiful friend. One

33
00:02:59,400 --> 00:03:01,880
 should associate with a

34
00:03:01,880 --> 00:03:11,430
 puri sutama. Utama is highest with the highest person. This

35
00:03:11,430 --> 00:03:13,480
 verse we're told was

36
00:03:13,480 --> 00:03:20,480
 taught in regards to channa. Channa is one of the more

37
00:03:20,480 --> 00:03:21,720
 famous figures in the

38
00:03:21,720 --> 00:03:26,380
 Buddha story. Every story that tells every story that you

39
00:03:26,380 --> 00:03:29,040
 hear about how our

40
00:03:29,040 --> 00:03:36,430
 great and glorious and wonderful and awesome leader became

41
00:03:36,430 --> 00:03:39,200
 a Buddha includes

42
00:03:39,200 --> 00:03:46,040
 channa. Have you ever seen the little Buddha with Kyanu

43
00:03:46,040 --> 00:03:48,960
 Reeves? It has

44
00:03:48,960 --> 00:03:54,120
 channa. Channa. Why are those men, why is that man lying

45
00:03:54,120 --> 00:03:56,320
 there? What is wrong with

46
00:03:56,320 --> 00:04:02,320
 those people? They're sick my lord. Channa was there with

47
00:04:02,320 --> 00:04:04,360
 him from childhood.

48
00:04:04,360 --> 00:04:09,080
 Channa was there when he saw the four sites, the four great

49
00:04:09,080 --> 00:04:10,800
 sites. He saw an

50
00:04:10,800 --> 00:04:16,600
 old person, a sick person, a dead person and then he saw a

51
00:04:16,600 --> 00:04:18,640
 reckless. Who is that

52
00:04:18,640 --> 00:04:23,760
 man? He is a reckless. He has gone forth from the home life

53
00:04:23,760 --> 00:04:24,680
 in order to find

54
00:04:24,680 --> 00:04:29,680
 freedom from suffering, freedom from death. That is what I

55
00:04:29,680 --> 00:04:32,480
 must do. Have you ever watched

56
00:04:32,480 --> 00:04:39,320
 Little Buddha or what's another story?

57
00:04:39,320 --> 00:04:48,680
 Well they all got this story and then channa went with him

58
00:04:48,680 --> 00:04:52,200
 when he left but

59
00:04:52,200 --> 00:04:57,340
 then he told channa to turn back and channa turned back and

60
00:04:57,340 --> 00:04:59,520
 brought his he

61
00:04:59,520 --> 00:05:05,280
 said to channa you must take take Kantaka, his horse and my

62
00:05:05,280 --> 00:05:06,440
 jewels. So all

63
00:05:06,440 --> 00:05:11,710
 of his royal jewels and earrings and whatever he crown,

64
00:05:11,710 --> 00:05:12,360
 whatever he was

65
00:05:12,360 --> 00:05:15,850
 wearing, probably not a crown but whatever jewels he was

66
00:05:15,850 --> 00:05:16,760
 wearing bring

67
00:05:16,760 --> 00:05:21,200
 these back my royal accoutrements because they have to go

68
00:05:21,200 --> 00:05:22,000
 back to my family.

69
00:05:22,000 --> 00:05:28,760
 I don't want them to get lost and so channa went a little

70
00:05:28,760 --> 00:05:30,720
 way. I hope I'm not

71
00:05:30,720 --> 00:05:34,400
 mixing this up with somebody else's story. Yeah no, channa

72
00:05:34,400 --> 00:05:35,120
 went back but

73
00:05:35,120 --> 00:05:39,210
 Kantaka died of a broken heart, his horse. His horse who

74
00:05:39,210 --> 00:05:40,240
 had been born with him.

75
00:05:40,240 --> 00:05:44,720
 The horse was born on the same day as the bodhisatta. I

76
00:05:44,720 --> 00:05:45,680
 think channa was as well

77
00:05:45,680 --> 00:05:49,410
 actually. There are five things as part of one of the exams

78
00:05:49,410 --> 00:05:50,560
 we have to take in

79
00:05:50,560 --> 00:05:53,060
 the first Dhamma exams. There are like five things that

80
00:05:53,060 --> 00:05:55,080
 were born on the same day as

81
00:05:55,080 --> 00:06:02,560
 the bodhisatta. The tree, the bodhi tree was planted on the

82
00:06:02,560 --> 00:06:04,280
 same day as the

83
00:06:04,280 --> 00:06:11,240
 bodhisatta was born. I think Yasodara, his wife to be,

84
00:06:11,240 --> 00:06:14,200
 maybe Ananda,

85
00:06:14,720 --> 00:06:18,950
 Kantaka the horse and I don't know it, maybe channa, I'm

86
00:06:18,950 --> 00:06:20,440
 grasping, I don't quite

87
00:06:20,440 --> 00:06:23,830
 remember. I know the horse and the bodhi tree for sure and

88
00:06:23,830 --> 00:06:25,440
 the horse died of a

89
00:06:25,440 --> 00:06:30,420
 broken heart because it thought that it would never see its

90
00:06:30,420 --> 00:06:33,480
 master again and it

91
00:06:33,480 --> 00:06:37,240
 was reborn. Kantaka was reborn as an angel I believe and

92
00:06:37,240 --> 00:06:38,800
 there's some story about

93
00:06:38,800 --> 00:06:45,150
 him that I can't remember either. But channa went away and

94
00:06:45,150 --> 00:06:47,200
 he thought I can't

95
00:06:47,200 --> 00:06:49,620
 bring these jewels back. If I go back without the bodhis

96
00:06:49,620 --> 00:06:50,840
atta, without the

97
00:06:50,840 --> 00:06:55,440
 prince, they're gonna think I did something to him. They're

98
00:06:55,440 --> 00:06:56,040
 gonna

99
00:06:56,040 --> 00:07:02,690
 accuse me and so he hung the jewels up on a tree and went

100
00:07:02,690 --> 00:07:03,560
 and became an

101
00:07:03,560 --> 00:07:07,280
 ascetic himself, lived in the forest. I think that's the

102
00:07:07,280 --> 00:07:09,160
 story. It's easy to get

103
00:07:09,160 --> 00:07:12,510
 these mixed up, I don't pay too much attention but I think

104
00:07:12,510 --> 00:07:13,160
 that's channa's

105
00:07:13,160 --> 00:07:16,600
 story. How he came to become a monk, I can't remember that.

106
00:07:16,600 --> 00:07:17,440
 Eventually he came

107
00:07:17,440 --> 00:07:22,990
 around to become a bhikkhu so he ordained under the Buddha

108
00:07:22,990 --> 00:07:23,560
 and I imagine

109
00:07:23,560 --> 00:07:27,880
 by that time Sariputta and Mughalana were already the two

110
00:07:27,880 --> 00:07:29,040
 chief disciples and

111
00:07:29,040 --> 00:07:34,460
 channa as close again we have one of these stories of being

112
00:07:34,460 --> 00:07:34,880
 close to

113
00:07:34,880 --> 00:07:39,980
 perfection and having no part in it. So he was so close to

114
00:07:39,980 --> 00:07:41,140
 the Buddha, perfect

115
00:07:41,140 --> 00:07:46,920
 and perfectly enlightened Buddha. And what did he do? He

116
00:07:46,920 --> 00:07:48,080
 went around mocking

117
00:07:48,080 --> 00:07:51,190
 the two chief disciples and saying I was there with him

118
00:07:51,190 --> 00:07:52,480
 from the very beginning

119
00:07:52,480 --> 00:07:58,530
 and I never ever lost track of the of the Buddha. I was

120
00:07:58,530 --> 00:08:00,680
 there when he left home, I

121
00:08:00,680 --> 00:08:04,270
 was there when he saw the four sites but these two, these

122
00:08:04,270 --> 00:08:05,960
 two go around saying I'm

123
00:08:05,960 --> 00:08:09,540
 the chief disciple. It actually says that, it's like he m

124
00:08:09,540 --> 00:08:11,200
ocks them. I'm the

125
00:08:11,200 --> 00:08:19,100
 chief disciple. Can you imagine right? For Sariputta and M

126
00:08:19,100 --> 00:08:20,140
ughalana. And he

127
00:08:20,140 --> 00:08:24,470
 would say this out loud to people. You go around thinking

128
00:08:24,470 --> 00:08:31,160
 they're all, all that. And

129
00:08:31,160 --> 00:08:35,960
 word got back to the Buddha of course and the Buddha called

130
00:08:35,960 --> 00:08:37,120
 channa up and

131
00:08:37,120 --> 00:08:42,400
 asked him if this was true and channa kind of got shamed

132
00:08:42,400 --> 00:08:44,260
 and quieted down for

133
00:08:44,260 --> 00:08:51,260
 a while. Stop saying such terrible things. But then he

134
00:08:51,260 --> 00:08:52,680
 started up again and he

135
00:08:52,680 --> 00:08:55,650
 started up again and the Buddha called him back and he

136
00:08:55,650 --> 00:08:57,000
 started up again and the

137
00:08:57,000 --> 00:08:58,840
 Buddha called him back the third time and the third time

138
00:08:58,840 --> 00:08:59,640
 the Buddha said look

139
00:08:59,640 --> 00:09:02,970
 channa. Sariputta and Mughalana couldn't be your best

140
00:09:02,970 --> 00:09:05,640
 friends. They could help you

141
00:09:05,640 --> 00:09:08,990
 so, so much. They could do such great things for you if you

142
00:09:08,990 --> 00:09:09,800
'd be their friends

143
00:09:09,800 --> 00:09:13,720
 because they are the, they are among the highest of beings.

144
00:09:13,720 --> 00:09:16,480
 They are true Kalyanamitta,

145
00:09:16,480 --> 00:09:19,920
 true good friends. You should not disparage them. You

146
00:09:19,920 --> 00:09:20,840
 should not think

147
00:09:20,840 --> 00:09:24,820
 low, think little of them, belittle them, mock them. This

148
00:09:24,820 --> 00:09:25,720
 is all to your

149
00:09:25,720 --> 00:09:32,710
 detriment. And he didn't listen but the Buddha said, so

150
00:09:32,710 --> 00:09:34,280
 before, when he's giving

151
00:09:34,280 --> 00:09:36,930
 him this lecture this is when he said the verse, Nabhaje, b

152
00:09:36,930 --> 00:09:38,560
apakimite. Don't

153
00:09:38,560 --> 00:09:43,200
 associate with those bad friends of yours. Don't associate

154
00:09:43,200 --> 00:09:43,880
 with low people.

155
00:09:43,880 --> 00:09:47,380
 Associate with good friends like Sariputta. Associate with

156
00:09:47,380 --> 00:09:47,880
 the highest

157
00:09:47,880 --> 00:09:53,480
 people like Mughalana and Sariputta. But he didn't listen

158
00:09:53,480 --> 00:09:57,240
 and the story goes on.

159
00:09:57,240 --> 00:10:02,210
 The Buddha said to Ananda, to said to the monks, he's not

160
00:10:02,210 --> 00:10:03,640
 gonna, during the time I'm

161
00:10:03,640 --> 00:10:08,460
 alive, as long as I'm alive he's never going to be, he will

162
00:10:08,460 --> 00:10:09,680
 never be humbled.

163
00:10:09,680 --> 00:10:13,360
 But once I pass away he will be humbled. And so they had,

164
00:10:13,360 --> 00:10:16,120
 Ananda asked him, this is

165
00:10:16,120 --> 00:10:22,640
 a prelude to the Parinibhana Sut, Mahaparinibhana Sut where

166
00:10:22,640 --> 00:10:23,360
 Ananda actually

167
00:10:23,360 --> 00:10:25,960
 asks the Buddha, what are we supposed to do with Channa? It

168
00:10:25,960 --> 00:10:26,840
's like he remembered

169
00:10:26,840 --> 00:10:29,410
 this part of the story. The Buddha, the Buddha, he

170
00:10:29,410 --> 00:10:30,820
 remembered that the Buddha had

171
00:10:30,820 --> 00:10:34,550
 once said that after he passed away Channa wouldn't be hum

172
00:10:34,550 --> 00:10:35,600
bled. So he said, what do

173
00:10:35,600 --> 00:10:39,340
 we do to humble him? The Buddha said give him the highest,

174
00:10:39,340 --> 00:10:41,240
 the ultimate punishment,

175
00:10:41,240 --> 00:10:46,350
 the brahma danda. Danda means stick literally but it seems

176
00:10:46,350 --> 00:10:46,640
 to mean

177
00:10:46,640 --> 00:10:50,750
 punishment. And brahma means of course highest, the God.

178
00:10:50,750 --> 00:10:52,880
 Punishment of God sort

179
00:10:52,880 --> 00:10:56,530
 of. But it means the punishment of the highest or the

180
00:10:56,530 --> 00:10:57,520
 highest punishment. The

181
00:10:57,520 --> 00:11:01,090
 ultimate punishment. What do you think the ultimate

182
00:11:01,090 --> 00:11:05,600
 punishment is? So they asked

183
00:11:05,600 --> 00:11:07,710
 him, well what is that ultimate punishment? What is the bra

184
00:11:07,710 --> 00:11:08,680
hma danda?

185
00:11:08,680 --> 00:11:13,710
 The Buddha said, let him say what he wants but no one

186
00:11:13,710 --> 00:11:16,040
 should consort with him,

187
00:11:16,040 --> 00:11:22,080
 talk to him, teach him, admonish him, get involved with him

188
00:11:22,080 --> 00:11:23,760
 in any way. Let no one

189
00:11:23,760 --> 00:11:29,370
 teach him, let no one help him. Let no one try and make him

190
00:11:29,370 --> 00:11:32,160
 a better person. That

191
00:11:32,160 --> 00:11:39,440
 right there, it's like killing him. It's like, as the

192
00:11:39,440 --> 00:11:40,600
 Buddha mentioned this kind

193
00:11:40,600 --> 00:11:45,920
 of thing to a prince once. I think it was Prince Ambaya, I

194
00:11:45,920 --> 00:11:46,560
 can't remember.

195
00:11:46,560 --> 00:11:55,120
 Veganaka maybe. And he said, if I teach people, I teach my

196
00:11:55,120 --> 00:11:55,680
 disciples,

197
00:11:55,680 --> 00:11:58,320
 if I give them a good teaching, tell them this is good, do

198
00:11:58,320 --> 00:11:59,840
 this, and they don't

199
00:11:59,840 --> 00:12:02,450
 listen, then I give them a hard teaching, I say don't do

200
00:12:02,450 --> 00:12:04,680
 this, that's bad. And if

201
00:12:04,680 --> 00:12:09,480
 they still don't listen, then I kill them. And he said,

202
00:12:09,480 --> 00:12:10,080
 what would you mean you

203
00:12:10,080 --> 00:12:14,560
 kill them? None of us, I don't teach them, I don't help

204
00:12:14,560 --> 00:12:15,960
 them, and all of my

205
00:12:15,960 --> 00:12:22,680
 disciples also cease helping and teaching them. The prince

206
00:12:22,680 --> 00:12:23,800
 said, indeed

207
00:12:23,800 --> 00:12:28,490
 that's as though killing them. So it's just the ultimate

208
00:12:28,490 --> 00:12:31,000
 punishment. And after

209
00:12:31,000 --> 00:12:33,680
 the Buddha passed away, indeed Ananda went with a bunch of

210
00:12:33,680 --> 00:12:35,040
 monks. He told

211
00:12:35,040 --> 00:12:37,140
 Mahakasapa that this is what the Buddha had said at the

212
00:12:37,140 --> 00:12:38,480
 first council. He told

213
00:12:38,480 --> 00:12:44,530
 Mahakasapa. And Mahakasapa said, well then you go and best

214
00:12:44,530 --> 00:12:46,280
ow this, bestow the

215
00:12:46,280 --> 00:12:53,660
 Brahmadanda on Channa. They went to Channa and Channa was

216
00:12:53,660 --> 00:12:55,120
 shaken by it and

217
00:12:55,120 --> 00:13:02,780
 eventually humbled. So that's our story. How does this

218
00:13:02,780 --> 00:13:05,200
 relate to our practice?

219
00:13:05,200 --> 00:13:07,940
 Well it's got this very simple teaching, but if we look at

220
00:13:07,940 --> 00:13:09,120
 the story we can find

221
00:13:09,120 --> 00:13:14,920
 a couple of other points. First is the point of pride.

222
00:13:14,920 --> 00:13:18,320
 Pride in stature. I've

223
00:13:18,320 --> 00:13:22,490
 met people in Buddhist teachers actually who are very proud

224
00:13:22,490 --> 00:13:23,120
 of their

225
00:13:23,120 --> 00:13:28,690
 seniority. And I think it's very important to point out how

226
00:13:28,690 --> 00:13:29,360
 senior they

227
00:13:29,360 --> 00:13:34,590
 are. I was practicing since this time and this time. I'm a

228
00:13:34,590 --> 00:13:35,680
 senior student

229
00:13:35,680 --> 00:13:40,610
 of this person and that person. I guess though it really

230
00:13:40,610 --> 00:13:42,480
 matters. As though

231
00:13:42,480 --> 00:13:50,040
 that says something. It's funny that Channa would be like

232
00:13:50,040 --> 00:13:50,560
 that. Being so

233
00:13:50,560 --> 00:13:53,910
 close to the Buddha and still didn't understand. There's

234
00:13:53,910 --> 00:13:54,400
 something to be

235
00:13:54,400 --> 00:14:01,400
 said about seniority. The Buddha said that's the easiest

236
00:14:01,400 --> 00:14:03,120
 way to set up a

237
00:14:03,120 --> 00:14:06,760
 system. If you have a monastic system, seniority makes

238
00:14:06,760 --> 00:14:09,600
 things a lot easier. But

239
00:14:09,600 --> 00:14:12,720
 that only really applies to an institution. In an

240
00:14:12,720 --> 00:14:13,600
 institution,

241
00:14:13,600 --> 00:14:19,020
 seniority makes sense because how are you going to judge

242
00:14:19,020 --> 00:14:21,600
 merit? And if merit

243
00:14:21,600 --> 00:14:26,310
 leads to institutional rank, you can see where the problem

244
00:14:26,310 --> 00:14:27,840
 lies. People pretending

245
00:14:27,840 --> 00:14:33,120
 to have merit or people getting greedy about cultivating

246
00:14:33,120 --> 00:14:35,200
 their

247
00:14:35,200 --> 00:14:41,280
 practice simply to gain stature and that kind of thing.

248
00:14:41,280 --> 00:14:44,460
 So in that case going by seniority makes sense. But to say

249
00:14:44,460 --> 00:14:45,280
 that somehow you're

250
00:14:45,280 --> 00:14:47,750
 better than someone or to hold yourself up because of

251
00:14:47,750 --> 00:14:49,520
 seniority is really

252
00:14:49,520 --> 00:14:54,000
 ridiculous. Functionally it makes sense. It stops a lot of

253
00:14:54,000 --> 00:14:56,480
 the ego.

254
00:14:56,480 --> 00:15:01,280
 But it was funny I talked to

255
00:15:01,280 --> 00:15:07,280
 I was staying with a monk once and he wasn't

256
00:15:07,280 --> 00:15:14,560
 the greatest of monks. We got in a feud and at one point he

257
00:15:14,560 --> 00:15:14,720
 was

258
00:15:14,720 --> 00:15:20,800
 saying, "Oh this monk is so much junior to me."

259
00:15:20,800 --> 00:15:23,680
 I'm not sure if that was. Anyway there was something and I

260
00:15:23,680 --> 00:15:24,320
 went to see one of

261
00:15:24,320 --> 00:15:30,480
 the big monks and I was saying, "You know this is

262
00:15:30,480 --> 00:15:35,160
 what he's saying." And he said, "Signority is of two. There

263
00:15:35,160 --> 00:15:35,760
's many kinds

264
00:15:35,760 --> 00:15:40,190
 of seniority. One kind of seniority is seniority of ability

265
00:15:40,190 --> 00:15:41,520
."

266
00:15:43,520 --> 00:15:46,970
 But the problem there is how do you decide who has the

267
00:15:46,970 --> 00:15:48,080
 ability.

268
00:15:48,080 --> 00:15:53,650
 Still it makes a lot more sense. You have to take both into

269
00:15:53,650 --> 00:15:54,480
 account.

270
00:15:54,480 --> 00:15:59,150
 And in fact you should never. If Moggallana and Sariputta

271
00:15:59,150 --> 00:15:59,680
 were in that

272
00:15:59,680 --> 00:16:03,160
 position they would have never thought, "Who is this chanag

273
00:16:03,160 --> 00:16:03,760
ai who the Buddha

274
00:16:03,760 --> 00:16:08,720
 holds up so highly?" One should never hold oneself above

275
00:16:08,720 --> 00:16:10,240
 others.

276
00:16:10,240 --> 00:16:14,510
 So this idea of pride is really important. We should never

277
00:16:14,510 --> 00:16:15,360
 think of ourselves as

278
00:16:15,360 --> 00:16:18,320
 advanced meditators, "Oh I've been practicing for so many

279
00:16:18,320 --> 00:16:18,880
 years."

280
00:16:18,880 --> 00:16:21,450
 It's usually what we say when we meet other people, "I've

281
00:16:21,450 --> 00:16:22,320
 been practicing for

282
00:16:22,320 --> 00:16:26,400
 so many years." It's really kind of unwholesome you know.

283
00:16:26,400 --> 00:16:30,240
 Because it's kind of boasting in the sense. Like you say it

284
00:16:30,240 --> 00:16:30,640
 because you

285
00:16:30,640 --> 00:16:33,920
 want people to be impressed. Which is really a bad thing.

286
00:16:33,920 --> 00:16:34,240
 It doesn't

287
00:16:34,240 --> 00:16:37,730
 really work very well right? Because who wants to hear that

288
00:16:37,730 --> 00:16:39,520
, "Ooh wow

289
00:16:39,520 --> 00:16:44,000
 you're better than me." It's a good way to create animosity

290
00:16:44,000 --> 00:16:44,960
 in fact.

291
00:16:44,960 --> 00:16:50,480
 Jealousy. That's a good test for us on the other hand.

292
00:16:50,480 --> 00:16:53,200
 To be humble.

293
00:16:53,200 --> 00:17:02,240
 At least to recognize our jealousy, to recognize our ego.

294
00:17:02,240 --> 00:17:05,680
 Another thing we don't want to do is be falsely humble.

295
00:17:05,680 --> 00:17:09,600
 To pretend to be humble or to say to disparage ourselves

296
00:17:09,600 --> 00:17:14,560
 in order to appear humble or in order to cultivate humility

297
00:17:14,560 --> 00:17:15,520
. It's not really

298
00:17:15,520 --> 00:17:19,810
 something you cultivate. It's funny you know. Wholesome

299
00:17:19,810 --> 00:17:20,160
 qualities

300
00:17:20,160 --> 00:17:23,440
 for the most part aren't something you should cultivate.

301
00:17:23,440 --> 00:17:26,430
 They're something that should come when your mind is

302
00:17:26,430 --> 00:17:27,840
 purified. It's like you

303
00:17:27,840 --> 00:17:31,280
 purify the soil and everything grows. All the good things

304
00:17:31,280 --> 00:17:35,680
 grow naturally. So humility isn't something you can

305
00:17:35,680 --> 00:17:38,800
 cultivate. Really the only thing we should be cultivating

306
00:17:38,800 --> 00:17:39,760
 is mindfulness. The

307
00:17:39,760 --> 00:17:43,010
 Buddha put it in a special category. It's the only one that

308
00:17:43,010 --> 00:17:44,240
's always useful.

309
00:17:44,240 --> 00:17:49,470
 It's the only one that you should always be focused on.

310
00:17:49,470 --> 00:17:50,000
 Because

311
00:17:50,000 --> 00:17:53,120
 through the practice of mindfulness you'll see your ego.

312
00:17:53,120 --> 00:17:56,320
 You'll see your jealousy. You'll see these qualities.

313
00:17:56,320 --> 00:17:59,520
 If Chana had just been mindful he would have seen

314
00:17:59,520 --> 00:18:04,160
 that this wasn't helping him. It wasn't making him happier.

315
00:18:04,160 --> 00:18:08,800
 But that's really the only way to do it.

316
00:18:08,800 --> 00:18:14,960
 What else? Well there's not too much here. That the big one

317
00:18:14,960 --> 00:18:20,800
 is in regards to association with good people.

318
00:18:20,800 --> 00:18:26,400
 All of us would would kill. Not kill. Would. With this

319
00:18:26,400 --> 00:18:28,160
 expression we would

320
00:18:28,160 --> 00:18:32,500
 give a lot to have the opportunity to be that close to the

321
00:18:32,500 --> 00:18:33,200
 Buddha.

322
00:18:33,200 --> 00:18:36,650
 That close to the two disciples. Two chief disciples let

323
00:18:36,650 --> 00:18:37,920
 alone the Buddha.

324
00:18:37,920 --> 00:18:40,840
 Do you imagine having the access to a teacher like Sariput

325
00:18:40,840 --> 00:18:43,120
ta or Moggalama?

326
00:18:43,120 --> 00:18:49,040
 And here he is going around disparaging them.

327
00:18:49,040 --> 00:18:54,830
 Saying bad things about them. Who do they think they are?

328
00:18:54,830 --> 00:18:56,400
 These newcomers.

329
00:18:56,400 --> 00:18:59,680
 The monks were like that as well. They wondered why

330
00:18:59,680 --> 00:19:05,000
 Kondanya wasn't made the chief disciple. The Buddha said it

331
00:19:05,000 --> 00:19:06,320
's because of

332
00:19:06,320 --> 00:19:11,920
 a difference of determination. Determination is the wish

333
00:19:11,920 --> 00:19:12,560
 you make.

334
00:19:12,560 --> 00:19:20,800
 Atitana means what you fix on. What your goal is basically.

335
00:19:20,800 --> 00:19:23,840
 So Kondanya's goal was to become the first one to realize

336
00:19:23,840 --> 00:19:24,240
 the Buddha's

337
00:19:24,240 --> 00:19:27,040
 teaching. And there's a story about him that I think we'll

338
00:19:27,040 --> 00:19:27,920
 get to if we haven't

339
00:19:27,920 --> 00:19:34,240
 already. Sariputta and Moggalana made the

340
00:19:34,240 --> 00:19:38,090
 determination. It was their long-standing wish. They weren

341
00:19:38,090 --> 00:19:38,960
't newcomers.

342
00:19:38,960 --> 00:19:43,040
 They have been born and died with the Buddha so many lif

343
00:19:43,040 --> 00:19:43,520
etimes.

344
00:19:43,520 --> 00:19:46,480
 If you read the jataka's, the number of jataka's that

345
00:19:46,480 --> 00:19:48,480
 included Sariputta,

346
00:19:48,480 --> 00:19:51,840
 more than Moggalana I think, but also Moggalana.

347
00:19:51,840 --> 00:19:58,240
 They were with him from long, long ago.

348
00:19:58,240 --> 00:20:03,540
 Because when you associate with unpleasant people, how hard

349
00:20:03,540 --> 00:20:04,080
 is it to be

350
00:20:04,080 --> 00:20:07,760
 mindful? How easy it is to get caught up in

351
00:20:07,760 --> 00:20:11,680
 unwholesomeness? To acquire the qualities of these

352
00:20:11,680 --> 00:20:14,640
 unwholesome people.

353
00:20:14,640 --> 00:20:19,200
 Whereas in a way, all you have to do if you're

354
00:20:19,200 --> 00:20:24,500
 open and you appreciate wise people, all you have to do is

355
00:20:24,500 --> 00:20:24,720
 be

356
00:20:24,720 --> 00:20:27,680
 around them. And through your openness and your

357
00:20:27,680 --> 00:20:32,720
 appreciation for them, you will only stand to gain.

358
00:20:32,720 --> 00:20:37,120
 You will only stand to benefit, to progress,

359
00:20:37,120 --> 00:20:41,200
 to better yourself, to emulate them, and to follow their

360
00:20:41,200 --> 00:20:43,680
 example.

361
00:20:44,960 --> 00:20:51,040
 So simple teaching, simple story. That's the Dhammapada for

362
00:20:51,040 --> 00:20:52,400
 today.

363
00:20:52,400 --> 00:20:56,640
 Thank you all for tuning in and keep practicing

364
00:20:56,640 --> 00:21:01,440
 and be well and good night.

365
00:21:01,440 --> 00:21:11,120
 Okay, so that's the Dhammapada.

366
00:21:11,680 --> 00:21:16,800
 We are of course still live.

367
00:21:16,800 --> 00:21:20,380
 We have some questions. Let's not do too many questions

368
00:21:20,380 --> 00:21:22,960
 tonight because

369
00:21:22,960 --> 00:21:26,660
 I was fighting all night last night with a failed hard

370
00:21:26,660 --> 00:21:27,200
 drive

371
00:21:27,200 --> 00:21:30,560
 and then today I was fighting with bureaucracy.

372
00:21:30,560 --> 00:21:36,170
 Not fighting, but just losing energy. We had our first

373
00:21:36,170 --> 00:21:38,480
 study group today

374
00:21:38,480 --> 00:21:44,720
 at McMaster University and a total of one person shot up.

375
00:21:44,720 --> 00:21:48,090
 And I was that one person, so I don't think the study group

376
00:21:48,090 --> 00:21:50,720
 is happening.

377
00:21:50,720 --> 00:21:53,300
 Everyone's, someone brought it up and I was skeptical and

378
00:21:53,300 --> 00:21:54,240
 several people were

379
00:21:54,240 --> 00:21:57,360
 skeptical. So someone called for a show of hands

380
00:21:57,360 --> 00:22:00,460
 who would be interested in study group and several people

381
00:22:00,460 --> 00:22:00,880
 put their

382
00:22:00,880 --> 00:22:04,720
 hand up. But someone said, you know, we're studying

383
00:22:04,720 --> 00:22:09,680
 so much at university. Don't know how much more studying we

384
00:22:09,680 --> 00:22:09,920
 can

385
00:22:09,920 --> 00:22:12,160
 add.

386
00:22:12,160 --> 00:22:17,040
 And I think that's probably what happened here.

387
00:22:17,040 --> 00:22:20,140
 As interested as people are, there's a lot of studying

388
00:22:20,140 --> 00:22:22,080
 going on already.

389
00:22:22,080 --> 00:22:27,390
 People are pretty busy. There was one man yesterday came to

390
00:22:27,390 --> 00:22:28,160
 the peace

391
00:22:28,160 --> 00:22:31,360
 festival with me. He's coming next week. He'll be coming

392
00:22:31,360 --> 00:22:34,080
 for a few days.

393
00:22:35,200 --> 00:22:40,240
 McMaster student during the reading week, so

394
00:22:40,240 --> 00:22:46,160
 we will be having a course for those who come.

395
00:22:46,160 --> 00:22:50,480
 So why don't we take a few questions then?

396
00:22:50,480 --> 00:22:53,170
 Sure. Is there any difference between dealing with

397
00:22:53,170 --> 00:22:54,400
 attachment for a person

398
00:22:54,400 --> 00:23:00,260
 and attachment to a situation? No, they're pretty much the

399
00:23:00,260 --> 00:23:01,120
 same.

400
00:23:02,320 --> 00:23:06,320
 When I do walking meditation, I do inclining, taking off,

401
00:23:06,320 --> 00:23:11,560
 lifting, going down, placing. Is that okay? It's fine. It's

402
00:23:11,560 --> 00:23:12,240
 a little bit of an

403
00:23:12,240 --> 00:23:15,920
 advanced technique. Normally we run you through the steps

404
00:23:15,920 --> 00:23:16,400
 starting at

405
00:23:16,400 --> 00:23:21,360
 the first step and going up in order. So it's not how we do

406
00:23:21,360 --> 00:23:22,560
 a course.

407
00:23:22,560 --> 00:23:29,040
 But you know, it's not wrong.

408
00:23:30,560 --> 00:23:33,600
 While meditating today, I felt the ambitious of power.

409
00:23:33,600 --> 00:23:38,290
 To be mindful was just beyond my hands. Do you have any

410
00:23:38,290 --> 00:23:39,280
 advice?

411
00:23:39,280 --> 00:23:48,320
 I don't understand the question.

412
00:23:48,320 --> 00:23:54,880
 Ambitious of power. Would that be like wanting power?

413
00:23:54,880 --> 00:23:57,920
 Sounds like wanting power.

414
00:23:59,920 --> 00:24:03,210
 Couldn't stay mindful. Oh, it was just beyond my hands in

415
00:24:03,210 --> 00:24:04,000
 the sense I couldn't

416
00:24:04,000 --> 00:24:08,960
 be mindful. Well, feeling, feeling, the very least I

417
00:24:08,960 --> 00:24:12,400
 understand that. You can just say feeling, feeling,

418
00:24:12,400 --> 00:24:16,320
 knowing, knowing. Sometimes that's all the best you can do

419
00:24:16,320 --> 00:24:17,200
 if it's not clear

420
00:24:17,200 --> 00:24:21,440
 exactly what the experience is. Because we don't

421
00:24:21,440 --> 00:24:24,960
 have to grasp at the particulars.

422
00:24:27,600 --> 00:24:31,370
 Don't grasp, but the details are the particulars of the

423
00:24:31,370 --> 00:24:32,480
 experience.

424
00:24:32,480 --> 00:24:37,030
 What do you think of Dzogchen techniques such as looking

425
00:24:37,030 --> 00:24:38,560
 for the seer?

426
00:24:38,560 --> 00:24:42,790
 I don't think about Dongchen techniques. What is the best

427
00:24:42,790 --> 00:24:43,600
 meditation for

428
00:24:43,600 --> 00:24:47,280
 concentration? Can you briefly touch on it? Thank you, B

429
00:24:47,280 --> 00:24:49,120
ante.

430
00:24:49,120 --> 00:24:53,840
 I don't teach meditations like that.

431
00:24:55,280 --> 00:24:59,120
 As I am meditating and have quieted my monkey mind,

432
00:24:59,120 --> 00:25:01,590
 do I try to look into my attachments or allow those

433
00:25:01,590 --> 00:25:03,920
 thoughts to come naturally?

434
00:25:03,920 --> 00:25:06,630
 Allow them to come naturally. If you haven't read my

435
00:25:06,630 --> 00:25:07,680
 booklet, I'd recommend

436
00:25:07,680 --> 00:25:11,330
 reading my booklet on how to meditate. It gives you a way

437
00:25:11,330 --> 00:25:13,440
 of approaching

438
00:25:13,440 --> 00:25:16,160
 experiences.

439
00:25:16,160 --> 00:25:22,050
 Darwin says, "Thank you, Bante. I love the Dhammapada

440
00:25:22,050 --> 00:25:23,760
 videos every night now and you

441
00:25:23,760 --> 00:25:27,920
 only have one year left to finish them all."

442
00:25:27,920 --> 00:25:32,640
 That's true. You go through much quicker with one every

443
00:25:32,640 --> 00:25:34,320
 night.

444
00:25:34,320 --> 00:25:37,130
 Has meditation helped you become a better student? Any

445
00:25:37,130 --> 00:25:38,160
 differences from your

446
00:25:38,160 --> 00:25:42,160
 earlier years in college? Yeah, definitely. You have to

447
00:25:42,160 --> 00:25:42,560
 understand

448
00:25:42,560 --> 00:25:47,520
 when I went to college the first time, half my time was

449
00:25:47,520 --> 00:25:52,240
 spent drinking alcohol, chasing after, well, chasing things

450
00:25:52,240 --> 00:25:53,440
 that probably weren't

451
00:25:53,440 --> 00:26:00,960
 going to make me happy. And getting involved in many, many

452
00:26:00,960 --> 00:26:04,000
 extracurriculars.

453
00:26:04,000 --> 00:26:07,770
 I wasn't an evil person. I was actually interested in the

454
00:26:07,770 --> 00:26:09,040
 environment and

455
00:26:09,040 --> 00:26:12,960
 student politics and that kind of thing, but

456
00:26:12,960 --> 00:26:15,840
 didn't do very well. I didn't do bad. I got, think, an A+

457
00:26:15,840 --> 00:26:17,920
 in calculus.

458
00:26:17,920 --> 00:26:21,840
 I think I got a B in biology.

459
00:26:21,840 --> 00:26:26,550
 But I got A's mostly, I think. But when I first went back

460
00:26:26,550 --> 00:26:27,840
 to school,

461
00:26:27,840 --> 00:26:31,520
 after having practiced meditation, I got

462
00:26:31,520 --> 00:26:37,680
 nine A+s, I think, out of the year. Nine A+s, an A, and an

463
00:26:37,680 --> 00:26:38,320
 A-.

464
00:26:38,320 --> 00:26:41,600
 I think something like that.

465
00:26:41,600 --> 00:26:47,010
 So yeah, certainly helps your practice. I'm probably set to

466
00:26:47,010 --> 00:26:47,360
 get all

467
00:26:47,360 --> 00:26:50,320
 straight A's, but I'm not doing full time. But, you know,

468
00:26:50,320 --> 00:26:50,960
 like I'm

469
00:26:50,960 --> 00:26:56,460
 using the Latin quizzes. So it definitely helps your memory

470
00:26:56,460 --> 00:26:57,280
.

471
00:26:57,280 --> 00:27:00,570
 And I'm getting older too, so there's that. It's not as

472
00:27:00,570 --> 00:27:03,680
 easy as it was before.

473
00:27:03,680 --> 00:27:10,400
 When noting how important is it to be specific,

474
00:27:10,400 --> 00:27:13,840
 I note thinking to refer to all kinds of mental activities

475
00:27:13,840 --> 00:27:15,200
 such as fantasizing

476
00:27:15,200 --> 00:27:20,880
 and remembering past events, etc. Is specificity important?

477
00:27:20,880 --> 00:27:23,360
 No.

478
00:27:23,360 --> 00:27:29,280
 No, that's fine. I mean Mahasya Sayadha does,

479
00:27:29,280 --> 00:27:32,960
 in certain cases, talk about specificity, specifying.

480
00:27:32,960 --> 00:27:39,520
 So you can, based on his advice. But we don't generally.

481
00:27:39,520 --> 00:27:44,000
 I think in his book he even talked about, like, if you're

482
00:27:44,000 --> 00:27:48,480
 imagining visiting or arguing, say arguing, arguing.

483
00:27:48,480 --> 00:27:53,340
 Yeah, it's a bit of an anomaly there. I'm not so sure, but,

484
00:27:53,340 --> 00:27:55,120
 you know,

485
00:27:55,120 --> 00:27:58,000
 who am I to argue?

486
00:27:58,000 --> 00:28:05,120
 I'll cut up on questions. Right, enough then.

487
00:28:05,120 --> 00:28:08,690
 Good night. Thank you all for showing up. Lots of viewers

488
00:28:08,690 --> 00:28:11,120
 on YouTube, 44 viewers.

489
00:28:11,120 --> 00:28:14,000
 That's great. Thank you all for tuning in. Have a good

490
00:28:14,000 --> 00:28:15,200
 night. Thank you, Robin, for

491
00:28:15,200 --> 00:28:22,480
 helping. Thank you, Bante. Good night.

