 Okay, we're live.
 So welcome to our sutta study tonight.
 Tonight we're beginning to study the Maha-hati Padopa-masut
ta.
 In Chimunikaya number 28.
 We should get this one done in two nights, I think.
 So as usual we'll be reading the Pali, chanting the Pali
 first and then reading and discussing a bit the English.
 So without further ado, we'll get started.
 Namo tasa, maruatu, waratu, samah sambuddhasa.
 Iva me zyutangi kansa mayam magawa, savatiyam viharati jita
 vaneyana tappindika sahara me.
 Tatrako ayasmat sahriputto viku, amanthese, auso vika vitti
, auso dikote viku, ayasmat sahriputta sahpatya sousum.
 Ayasmat sahriputto itandavuja, sayyata piaouso yani gani ji
, jangana bharnana nangpada jatani,
 sabhani tani hati pade samodhana nangpada janti, hati pada
 nangdhi sanga maga yati, yaridhama handa dina.
 Iva me wakaua, uso yegi jigusala dhamma, sabhite chatuso a
riya sache su sanga nangpada janti,
 kata me su chatuso takye ariya sache dukha samudhay ariya s
ache, dukha niro jay ariya sache, dukha niro dagaminiya,
 pati pada yayaya ariya sache, kataman javuso dukhang ariya
 sache, jati vidukha jala vidukha,
 marnam vidukhan su kaparide vadukha johana su paya sahpiduk
ha, yam pi chanda davati sampidukhang,
 samkitena panchupadana kandadukha, kata me javuso panchupad
ana kandas,
 saiyati dangrupadana kandu vidanupadana kandu, sanyupadana
 kandu,
 kata me javuso rupupadana kandu, jatari mata maha bhutan,
 nijatun anchama bhutanam ubadayarupam katama javuso jatar
oma bhutan,
 pattavi dhatu apodhatu tejodhatu vayodhatu katama javuso p
attavi dhatu,
 pattavi dhatu siya anjatika siya bhaira katama javuso jat
ika pattavi dhatu,
 yamajatam bhajatam kakalang karigatam ubadinam saiyati dang
,
 kisa loma nakadantata jomamsan naru aak piyatiminam wakam
 adayam,
 yakanang gi loma kam bhihakam bhapasan,
 antang antagunam gudari yam karizam,
 yam wapay nayan yam bhihin shiyan jatam,
 pachatam kakalang karigatam bhavadinam,
 ayam guchata uso ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yajabahira battavi dhatu battavi dhatu,
 yag jivakopana ajatika battavi dhati,
 yag jivakopana ajatika battavi dhati,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 yag jivakopana ajatika battavi dhatu,
 kariyati idangudana masanthi,
 dasa jeya wuso bhikunu yawambudam,
 aryusaro yawandam and arusaro,
 yawansankang and arusaro,
 ubeka kusanisitana santati,
 sutana sangvichati, sangvigana bhajati,
 alabha vatami, navatami,
 labhaduladam vatami, navatami su ladan,
 yasame yawambudam, aryusaro,
 yawandam vam vanusaro,
 yawansankang, aryusaro,
 ubeka kusanisitana santati,
 tseyatapi auso bhansanisa sasram,
 iswa sangvijapti,
 sangvigana bhajati,
 iwa mihi wakolavusota sachibigunu,
 yawambudam anusarato,
 yawandam anusarato,
 iwa mihi wakolavusota sasram,
 ubeka kusanisitana santati,
 sutana sangvichati,
 sangvigana bhajati,
 alabha vatami, navatami,
 labhaduladam vatami,
 navatami su ladan,
 yasame yawambudam anusarato,
 yawandam anusarato,
 yawansankang anusarato,
 ubeka kusanisitana santati,
 tseyatapi ausobigunu,
 yawambudam anusarato,
 yawandam anusarato,
 yawamsankang anusarato,
 ubeka kusanisitana santati,
 sutana atamanahoti,
 eta vatabhiko ausobigunu,
 bhagukatamodhi,
 katamachavuso apodhatu,
 abodhatu,
 siya vanchatika,
 siya vahira,
 katamachavuso achatika,
 labhudhatu,
 yawanganchatam vachatam,
 bhagukatam vachatam vachatam,
 siya vidam vidam,
 siham pum polo hitam,
 siddho meidho asu asadki,
 o singanika,
 lasika mutam yam vahbananyam,
 kintjiyajatam bachatam,
 bhagukatam vachatam,
 aiyangvuchata uso achatika apodhatu,
 yawanganchatika apodhatu,
 yawanganchatika apodhatu,
 abodhatu,
 lwisa,
 nadeyukam bhamanyisotam asmina meisotati,
 hiwamitangyatabutam samma panya yadatabang,
 hiwamitangyatabutam samma panya yadisvah,
 abodha yuya min dati,
 abodha yuya titang vidajiti,
 utikaso asu sammayo yam vahira,
 o dhatupakupati,
 sagamam piwati,
 nigamam piwati,
 nagaram piwati,
 chanapadam piwati,
 chanapadabadi sampiwati,
 utikaso asu sammayo yam vahira,
 samudayo chanasaktikani piy,
 udagani yogachanti,
 tiyojana satikani bhidagani yogachanti,
 tiyojana satikani bhidagani yogachanti,
 tiyojana satikani yogachanti,
 panchayojana satikani bhidagani yogachanti,
 tiyojana satikani bhidagani yogachanti,
 satayojana satikani bhidagani yogachanti,
 utikaso asu sammayo yam vahira,
 samudayo satatalam piwdagang santati,
 chatalam piwdagang santati,
 panchayajalam piwdagang santati,
 chalduratalam piwdagang santati,
 titalam piwdagang santati,
 vialam piwdagang santati,
 talamatam piwdagang santati,
 utikaso asu sammayo yam vahira,
 samudayo satapuri sampriyudagang santati,
 japuri sampriyudagang santati,
 manjapuri sampriyudagang santati,
 chattu pori sampriyudagang santati,
 tipuri sampriyudagang santati,
 tipuri sampriyudagang santati,
 pori sammata mata mepudagang santati,
 potikaso asu sammayo yam vahira,
 samudayo satapuri sampriyudagang santati,
 khatimatam piwdagang santati,
 janukamatam piwdagang santati,
 gopakamatam piwdagang santati,
 otikaso asu sammayo yam vahira,
 samudayo satapuri sampriyudagang santati,
 vialam piwdagang santati,
 dasatani namah asu vahira yam vodagu yam,
 tawamahalika yani cata panyayasati,
 kayatamata panyayasati,
 vayadamata panyayasati,
 vibharina madamata vibhanyayasati,
 kimpanimasamatatakasahayasvatanupadinasah,
 antiwamamantiwahasmidiwah,
 atkawasanotiwitahotipe,
 dasachayahuso bhikunowiwamudam gunusarato,
 ewandamamunusarato, ewansankamunusarato,
 obeika kusala isidasantati,
 sutena atamanotity,
 tawatapri konko auso bhikunowabagatamoti.
 Okay, we'll stop there.
 More that way.
 Hmm, that was difficult, I can't think.
 I think the English would be a little easier,
 but this is a difficult sutta, actually.
 It's actually, this one doesn't feature the Buddha at all.
 It's Sariputta who gives this discourse.
 He's one of the few.
 He has more, he's the one who has more suttas in his name,
 to his name than anyone besides the Buddha,
 honestly, because he's the Buddha's sagi,
 the Buddha's right-hand man.
 But you'll notice that there's a different flavor to them.
 Sariputta was quite analytical,
 so he's very much into explaining the Buddha's teachings
 in an analytical fashion.
 And it's very much in the flavor of what you find in the Ab
hidhamma,
 or the Pattisambhida manga, or even the Visuddhi manga,
 sort of what it's based on,
 sort of taking the Buddha's teaching and extrapolating upon
 it
 and systematizing it.
 Let's, okay, let's get started then.
 Thus have I heard, on one occasion,
 the Blessed One was living at Sawati in Jade's Grove,
 and not at Pindikas Park.
 There the venerable Sariputta addressed the bhikkhus' thus.
 Friends, bhikkhus, friend, they replied.
 The venerable Sariputta said this.
 They noticed, they're still using the word friends for each
 other.
 This is how they addressed each other during the time of
 the Buddha.
 Once the Buddha passed away,
 then they would address the senior,
 the juniors would address the seniors as bhantae,
 and only the seniors would address the juniors as friends.
 Friends, just as a footprint of any living being that walks
 can be placed within an elephant's footprint,
 and so the elephant's footprint is declared the chief of
 them
 because of its great size.
 So too, all wholesome states can be included in the four
 noble truths.
 And what for?
 In the noble truth of suffering,
 in the noble truth of the origin of suffering,
 in the noble truth of the cessation of suffering,
 and in the noble truth of the way leading to the cessation
 of suffering.
 Okay, so the first simile of the elephant's footprint was
 that
 when you see an elephant's footprint,
 you can, there's the idea that you can know the,
 know what kind of an elephant it is by its footprint,
 and the Buddha said that's not the case.
 But that's the shorter discourse,
 the minor discourse on the simile of the elephant's
 footprint.
 Here's the same object, but a totally different simile.
 The four noble truths are the chief in regards to all
 wholesome states,
 or it's not even all wholesome states.
 In regards to wholesomeness, or the cultivation of wholes
omeness,
 when you talk about cultivation of what is good,
 it all fits within the four noble truths.
 So it's not just about the cultivation,
 it's not just about good wholesome states,
 but anything to do with the subject,
 anything to do with what's actually useful and beneficial,
 falls under the categories of the four noble truths.
 It's a profound statement, it's a bold statement,
 but since they deal directly with happiness and suffering,
 if you're of the opinion that happiness and suffering are
 what's truly important,
 truly useful discussion, then it makes perfect sense.
 So what he's going to do now is he's going to show how that
 works,
 how actually he's going to give an example of how the four
 noble truths cascade
 into other sets of Dhamma.
 So he's not going to explain the four noble truths in
 detail,
 he's going to pick one piece of the four noble truths
 and show a smaller part of the Dhamma that fits in with the
 four noble truths,
 and then he's going to show a part of that Dhamma,
 and a part of that Dhamma that actually cascades into
 several layers.
 Here we have the four noble truths, in that standard we
 know what those are.
 And what is the noble truth of suffering?
 Birth is suffering, aging is suffering, death is suffering,
 sorrow, lamentation, pain, grief, and despair are suffering
,
 not to obtain what one wants is suffering.
 In short, the five aggregates affected by clinging are
 suffering.
 So we do kind of separate these into two categories, I
 would say.
 Everything else and the last one.
 Everything else is much more theoretical and abstract and
 conceptual.
 But when we talk about, when we talk about,
 the real definition of suffering is this, because
 everything else really fits into that.
 He's giving examples in the first set.
 So what we really do have to focus on is this last one.
 It's more difficult to understand, you see,
 but these ones make it easier to understand what we're
 talking about,
 birth being suffering, aging being suffering.
 This one's very easy to understand.
 This one might be difficult, how is birth suffering,
 but we can, there's different ways of explaining that it's
 not.
 The point is that stuff that we understand to be suffering,
 it's all in here.
 But this one, it's really making another point that really,
 anything that you can cling to, anything that arises,
 is part of the truth of suffering, because it either is
 painful
 or it's hard to maintain, or it's a cause for clinging,
 which leads to suffering when it's gone,
 which is a cause for addiction and dissatisfaction.
 So what he's going, so instead of, he's not going to go
 through all four of the normal truths.
 I think at the end he might, I can't remember,
 but he's showing in a cascading way, right?
 So then he takes this last one, and the next paragraph he's
 going to just pick it.
 He's just going to talk about it.
 So what about, so you pick the four noble truths,
 and pick the first one, and you can fit the five aggregate,
 or you can fit all the kinds of suffering in there.
 Right, the idea of suffering is inside,
 and inside of that are the five aggregates of clinging,
 and he's going to explain that.
 And what are the five aggregates affected by clinging?
 They are the material form aggregate affected by clinging,
 the feeling aggregate, the perception aggregate,
 the formation is aggregate,
 and the consciousness aggregate affected by clinging.
 There's no word affected. He's just inserting it in there.
 It's not in the Paladi.
 The Upadhanakandas just means the clinging aggregate,
 the five clinging aggregates.
 So that's how you want to relate these two words up to the
 translator.
 The five aggregates of clinging are often what they're
 called,
 but I guess affected does kind of get the vague,
 get it across in a vague sort of general sense
 that it has something to do with clinging.
 The five clung to aggregates,
 maybe possible, the five sorts of things that one can cling
 to,
 or the five things related to clinging, right?
 But really I think it's the five clung to
 is probably the best five aggregates that are clung to.
 Or the five sets of clings that can occur
 based on five different types of objects.
 So aggregates of clinging kind of works
 because we're actually talking about,
 no that's not true, it's the five aggregates that are clung
 to.
 Because clinging itself is not the truth of suffering.
 The aggregates themselves are,
 but they're things that are clung to.
 And they're also the basis of experience,
 the basis of reality in Buddhism,
 or this five aggregates.
 I think we've talked about this before anyway.
 So then, in those five,
 so we're thinking in terms of the simile of footprints,
 inside this latest footprint of the five aggregates,
 take the first aggregate and explain it,
 smaller footprint.
 And what is the material form aggregate affected by
 clinging?
 It is the four grade elements,
 and the material form derived from the four grade elements.
 And what are the four grade elements?
 They are the earth element, the water element,
 the fire element, and the air element.
 Okay.
 It may seem kind of weird that,
 in the beginning we talked about how this is all to do with
 wholesomeness.
 And we start to wonder when he talks like this,
 what this has to do with wholesomeness,
 what this has to do with good things.
 How is it useful?
 How is what we're talking about here useful?
 The Four
 Great Elements.
 The four great elements, the Mahabhuta,
 the word isn't quite element.
 The four great primaries maybe, or great things that exist.
 Bhuta just means something that exists, or something that
 is evident.
 The four great evidence might be a T on the end,
 evidence, things that are evident.
 Elements is if it was dhatu, but it does mean the four
 great elements,
 but the word isn't elements, the word is Bhuta, it's not d
hatu.
 And the other set is the material form derived from the
 four great elements,
 or great primaries.
 And those are like space,
 because space only arises based on the elements.
 It's what's in between stuff.
 If there's no stuff, there's no in between.
 And all the qualities of matter,
 like malleability or stiffness,
 there's the femininity, masculinity,
 there's many, many different derived forms.
 So based on the formation of matter,
 like in science we would say talk about maybe shininess or
 wetness
 or heat,
 heat is already one of the four great elements.
 I think movement, things like movement and so on,
 I don't understand,
 I think it's like 20 of them or something.
 So and then he takes that, and he focuses on the first one,
 and says what are the four great elements.
 And again, the four elements are not what we would normally
 think of them
 in the West as being actual dirt,
 actual water, actual fire, actual air.
 It's not the only meaning here.
 This is the external fire, the external earth, the external
 water.
 Well that is the conceptual idea that we have of them,
 but it also has to do with hardness, cohesion, heat,
 and stiffness, more pressure,
 the characteristics of these elements experientially,
 which is really the most important part of the description,
 because it's what is practical for creating the arising of
 insight into experience.
 Anyway, so he's going to talk about the earth element as
 being both internal and external.
 What friends is the earth element?
 The earth element may be either internal or external.
 What is the internal earth element?
 Whatever internally belonging to oneself is solid, solid
ified, and clung to.
 That is head hairs, body hairs, nails, teeth, skin, flesh,
 sinews, bones, bone marrow,
 kidneys, heart, liver, diaphragm, spleen, lungs, intestines
,
 mesentery, contents of the stomach, feces, or whatever else
 internally.
 Belonging to oneself is solid, solidified, and clung to.
 This is called the internal earth element.
 Now both the internal earth element and the external earth
 element are simply earth elements,
 and that should be seen as it actually is with proper
 wisdom thus.
 This is not mine. This I am not. This is not myself.
 When one sees it thus as it actually is with proper wisdom,
 one becomes disenchanted with the earth element
 and makes the mind dispassionate toward the earth element.
 Okay, this is... now we're getting somewhere with this
 whole wholesomeness,
 because this is part of who we are, and it's the sort of
 thing that is clung to.
 The point is, he talks about all the internal earth, but
 whatever is clung to.
 All the solid stuff that is clung to, whatever is about us.
 And actually, not so much this stuff. This is okay.
 I mean, this is the conceptually clung to stuff.
 We cling to our hair, we cling to our nails, our teeth,
 skin.
 We don't cling to much of the rest of the stuff. Not
 directly.
 But it's included in here for completion's sake.
 A lot of this stuff is useful for tranquility meditation,
 allowing us to let go of the attachment of the body.
 And we see how disgusting some of this stuff is,
 and we realize that the body is not really full of sugar
 and spice and all things nice.
 It's gross.
 I'm an equal opportunity.
 Whatever else internally belonging to oneself is solid, so
 I can clung to.
 So the point is, being solid and the hardness of it, or the
 softness of it,
 or the feeling of hardness when your bones touch each other
,
 or when you touch the floor, or so on, on the hardness in
 the body.
 When your teeth touch each other, or when you touch your
 nails, or so on.
 This is all hardness.
 So it's the internal earth.
 The point is that we cling to it. We cling to the
 sensations.
 It's a small part of the sort of things that we cling to.
 And the conception is, it makes up the body that we cling
 to,
 like clinging to the beauty of our hair, and nails, and
 teeth, and so on, and skin.
 Clinging to these things.
 What he's saying is, they're all based on the earth element
, even the external stuff.
 Now, external stuff would be the earth around us, the cap
illae earth, and matter actually.
 No, it would be all the hardness and inherent matter around
 us.
 The point is that when we cling to it, it causes a
 suffering.
 Remember, relating back to the Four Noble Truths,
 you cling to the body, the physical, or any kind of
 experience.
 It leads us to suffer either causing us pain, or creating
 difficulty in maintaining it.
 You build up your house, you have to maintain the earth
 element, the hardness of it,
 or else it falls apart, falls down, cracks, and so on.
 Or it causes suffering when it changes, when it breaks.
 So when one sees that one cannot control these things, when
 one looks at it,
 and sees that one cannot find true contentment with the
 earth element,
 with things made up of the earth element, because they're
 actually just earth element.
 Hair is just earth element, body, nails, teeth, skin, all
 the good stuff, all the bad stuff,
 it's all just earth, it's not really me, it's not mine, it
's not an entity.
 It's just an experience that arises and sees.
 One becomes seeing this, one becomes disenchanted with the
 earth element,
 and makes the mind dispassionate towards the earth element.
 Now there comes a time when the water element is disturbed,
 and then the external earth element vanishes.
 When this external earth element, great as it is, is seen
 to be impermanent,
 subject to destruction, disappearance, and change,
 what of this body, which is clung to by craving and lasts
 but a while?
 There can be no considering that as I or mine or I am.
 Here's a bit of rhetoric, quite useful, it's like,
 you know, if the earth, if the great earth,
 so I guess this is sort of Buddhist physics, no?
 Ancient is Indian cosmology.
 The city Magadad's talk about these in quite detail about
 the four ways,
 the three ways, right? Water, fire, wind,
 the three ways that the universe can be destroyed is kind
 of interesting,
 I wonder how it would relate to, or whether it would relate
 to modern cosmological theories.
 But even the earth element, the earth, means all heavenly
 bodies,
 and the great as they are, or the whole earth that we live
 on, great as it is,
 is eventually going to just vanish, disappear, be obliter
ated.
 Great, as great as it is.
 What of this body? This body, how can we cling to this body
 as being something lasting?
 So then, if others abuse, revile, scold, and harass a bhikk
hu,
 who has seen this element as it actually is, he understands
 thus,
 "This painful feeling born of your contact has arisen in me
.
 That is dependence, not independence."
 Dependent on what? Dependent on contact.
 Then he sees that contact is impermanent, that feeling is
 impermanent,
 that perception is impermanent, that formations are imper
manent,
 and that consciousness is impermanent.
 And his mind, having made an element its objective support,
 enters into that new objective support,
 and acquires confidence, steadiness, and resolution.
 The point of this is that, based on his study of earth,
 this occurs.
 This occurs when you study reality.
 It seems actually kind of unrelated to his study of the
 earth element.
 But just the earth element is actually everything to do
 with experience,
 including the earth element as an example.
 If you take the earth element as an example,
 whereas before when others would yell at them because they
--he would get angry and upset,
 now because of watching reality and seeing reality as it is
,
 when he is harassed, when he is hurt, when he is attacked,
 he sees it in a new way, which is really--
 this is actually just a description of how he sees it.
 It's an explanation of how he sees it.
 It's not a theoretical thing that you have to study this.
 It's interesting to study, and for us to understand this,
 it's actually just a description.
 Whereas normally you think, "Oh, that jerk who reviled me
 abused me."
 Now they think, "Oh, here's this ear contact that has risen
 in me."
 And he sees that it's dependent, and he sees that it has
 nothing to do with the person.
 It's just a causal chain.
 And seeing it as impermanence, seeing it as not me, not
 mine, not self,
 then becomes unattached to it and acquires confidence,
 steadiness, and resolution.
 I've got a big, long explanation here. I don't have to put
 it to that.
 Anyway, it causes the mind to settle down.
 The mind is no longer jumping around chasing after, for
 example, the earth element.
 Are we into the water element? No. Yeah, the earth element.
 And furthermore...
 Now if others attack that bikkun waves that are unwished
 for,
 undesired and disagreeable, by contact with fish, clods,
 fists, clods, sticks, or knives, he understands thus,
 "This body is of such a... This body is of such a nature
 that contact with fists, clods, sticks, and knives is a
 sealant."
 But this has been said by the Blessed One in his advice on
 the simile of the saw.
 He goes, "Even if bandits were to sever you savagely limb
 by limb with the two-handled saw,
 he who gave rise to a mind of hate towards them would not
 be carrying out my teaching.
 So tireless energy shall be aroused in me, and unremitting
 mindfulness established.
 My body shall be tranquil and untroubled, my mind
 concentrated and unified.
 And now let contact with fists, clods, sticks, and knives a
-sail this body."
 Really inappropriate. For this teaching of the Buddhist is
 being practiced by me.
 So sorry.
 So even if attacked, as above it was, if there's verbal
 abuse,
 but if there's also physical abuse, it's really actually a
 description of the benefits of meditation.
 Highly beneficial in what it does to you.
 Reactions to people's abuse is changed.
 We're able to see, recognize things as not a person, as not
 conceptual.
 It's just reality, which is such a relief to look at things
 as they are.
 It seems so awful actually. When ordinary people hear this,
 they think,
 "Oh, how awful." It's like ignoring the person.
 It's like the person didn't even exist. And that's the
 point.
 It makes it so difficult to understand the dumbness.
 You think of it in terms of entities, people.
 And actually it's just experiences.
 Breaking it apart is such a relief.
 No more anger, no more hatred. There's more vengeance.
 Even if people beat you, and of course it goes back to the
 simile of the saw,
 which we studied earlier, the Buddha demands this of us.
 And Rama, recollecting that, he works hard. He or she works
 hard and stays in trouble.
 When that bhikkhu thus recollects the Buddha, the Dhamma,
 and the Sangha,
 if equanimity supported by the wholesome does not become
 established in Him,
 then he arouses a sense of urgency thus, "It is a loss for
 me. It is no gain for me.
 It is bad for me. It is no good for me."
 That when I thus recollect the Buddha, the Dhamma, and the
 Sangha,
 equanimity supported by the wholesome does not become
 established in me.
 Just as when a daughter-in-law sees her father-in-law,
 she arouses a sense of urgency to please him.
 So too, when that bhikkhu thus recollects the Buddha, the D
hamma, and the Sangha,
 if equanimity supported by the wholesome does not become
 established in him,
 then he arouses a sense of urgency.
 But if, when he recollects the Buddha, the Dhamma, and the
 Sangha,
 equanimity supported by the wholesome becomes established
 in him,
 then he is satisfied with it.
 At that point, friends, much has been done by that bhikkhu.
 So this threw me off before the...
 He's not actually recollecting the Buddha, the Dhamma, and
 the Sangha.
 He's recollecting the...
 He's recollecting the element.
 So I'm not quite sure what he's doing here,
 but the commentary says that recollection of the Buddha is
...
 Right?
 Recollection of the Buddha having given this advice.
 Recollection of the Dhamma is this teaching that he gives.
 And recollection of the Sangha is this recollection of
 monks who do this thing,
 who keep their mind tranquil.
 He who does not give rise to a mind of hate.
 But I'm more inclined to think that it's just a general...
 general sort of non sequitur in the sense of just reminding
 the monks,
 because I'm going to do this in a talk where you describe
 all of this as actually...
 relating to, if not the Buddha, then the Dhamma, and if not
 the Dhamma and the Sangha.
 And I put more emphasis on the Dhamma here,
 because the Buddha really is focusing on the Sangha in this
...
 this is just the teaching or reality.
 But in some way it's recollection of the Buddha, the Dhamma
, the Sangha.
 Recollection of the teaching.
 Recollection of the Buddhist religion, of Buddhism.
 It can also be that while he's telling this talk, the monks
 are getting bored, you see,
 because if there's ordinary world things in the audience,
 sometimes they get bored.
 And so they may have been all letting their minds wander
 and getting caught up in unwholesome thoughts.
 And so he pulls them back and he says so...
 and by mentioning Buddha, Dhamma, and the Sangha,
 it has a way of doing that, reminding people that they're
 not interested in this long discourse
 about the aggregate or the element.
 He pulls them back by reminding them of the Buddha.
 He's kind of done that above.
 You get the sense that it could have been,
 this was totally out of left field.
 It could have been that someone was starting to get...
 or some monks were starting to wander, and so he goes back
 to this.
 But actually, for these ones, it's more likely that what he
's doing is
 making it more practical for them.
 So these are kind of there.
 But this one's kind of...
 Anyway, to me, it was just my own...
 probably my own lack of wisdom.
 But Sri Puddas is just brilliant.
 And the thing about the suttas is they're given to an
 audience,
 and you can't ever forget that.
 And it can be at times that they were given particularly
 for that certain audience,
 or there's a hint of that in them.
 You can't deny that that could be the case in certain
 audiences.
 So he's talking about a sense of urgency, and it's still
 very much.
 It's not like the total non-secret of the whole passage
 itself.
 He's talking about what happens when you realize this,
 the impermanence of all things, there arises this equanim
ity,
 which is the height of our meditation practice.
 This point much has been done by him.
 So this is the description of the...
 Earth element is going to do the same with the water
 elements.
 We'll finish up with the water element today.
 What friends is the water elements?
 The water elements may be either internal or external.
 What is the internal water elements?
 Whatever internally belonging to oneself is water, watery,
 and klung-tu.
 That is bile, phlegm, pus, blood, sweat, fat, tears, grease
, spittle,
 snot, oil of the joints, urine, or whatever else internally
 belonging to oneself
 is water, watery, and klung-tu.
 This is called the internal water elements.
 Now both the internal water elements and the external water
 elements
 are simply water elements,
 and that should be seen as it actually is with proper
 wisdom thus.
 This is not mine, this I am not, this is not myself.
 When one sees it thus as it actually is with proper wisdom,
 one becomes disenchanted with the water elements
 and makes the mind dispassionate toward the water elements.
 Really the point is, remember we're talking about
 everything fitting in
 one footprint of the Four Noble Truths.
 And as I said already, we're dealing with very, very small
 elements.
 Remember this is only one quarter of the first of the five
 aggregates.
 The water element, the Earth element was the first quarter,
 this is the second quarter.
 The second quarter of one part of the first aggregate,
 which is one quarter of the Four Noble Truths.
 So we've got a very small part of the puzzle here.
 And so what Sariputta is implying here,
 and what we see down in the Abhidhamma
 is actually going through each and every one of the many
 parts.
 And so if you multiply it all out, you're dealing with many
, many, many parts
 and a huge, huge number of things that fit in the Four
 Noble Truths.
 And he's just giving a glimpse, just giving a taste of that
 here.
 So it's not like the water element is terribly important,
 it's not like it's imperative that you study these things.
 But we do in some small way cling to these things,
 and they can be a very useful object of meditation.
 Because in the other side we cling to them as being part of
 the body,
 which we consider to be beautiful.
 So studying these things allows us to let go of that,
 seeing them as being.
 Not so wonderful after all.
 But again, in terms of experience,
 we're just talking about all the cohesion in the body,
 all the fluid in the body, what is fluidity?
 Fluid, stuff that is fluidy, watery, and clung to.
 Most important, there's anything in the body,
 and then later, or the mind, that it's clung to.
 And again, same thing as before.
 He's going to talk about the water element, the external
 water.
 Now there comes a time when the external water element is
 disturbed.
 It carries away villages, towns, cities, districts, and
 countries.
 There comes a time when the waters and the great oceans
 sink down
 100 leagues, 200 leagues, 300 leagues, 400, 500, 600, 700
 leagues.
 There comes a time when the waters and the great oceans
 stand
 seven palms deep, six palms deep, two palms deep, only a
 palm deep.
 There comes a time when the waters and the great oceans
 stand
 seven fathoms deep, six fathoms deep, two fathoms deep,
 only a fathom deep.
 There comes a time when the waters and the great oceans
 stand half a fathom deep, only waist deep, only knee deep,
 only ankle deep.
 There comes a time when the waters and the great oceans
 are not enough to wet even the joint of a finger.
 When even this external water element, great as it is,
 is seen to be impermanent, subject to destruction,
 disappearance, and change.
 What of this body, which is clung to by craving and lasts
 but a while?
 There can be no considering that as I or mine or I am.
 Yeah, not much to say there.
 The water element, same as the Earth, eventually just
 evaporates.
 I mean, it's not actually the water element,
 but the external water outside of the body,
 the actual conceptual water that we see in the world around
 us.
 Even that great, great physical thing eventually is
 disintegrated.
 So what of this body, what of this body,
 which is clung to by craving and lasts but a while?
 So same thing, and then same thing with the rest of it.
 When people attack you, you can think in terms of the water
 element as well,
 and think in terms of the elements as being impermanent and
 suffering.
 So then, if others abuse, revile, scold, and harass Abiku,
 we see this element as it actually is.
 He understands thus.
 At that point too, friends, much has been done by Abiku.
 That's all for today.
 So thank you all for tuning in.
 Have a good night.
