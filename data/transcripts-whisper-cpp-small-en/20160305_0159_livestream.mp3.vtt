WEBVTT

00:00:00.000 --> 00:00:14.000
 Good evening everyone.

00:00:14.000 --> 00:00:31.000
 Broadcasting line March 4th.

00:00:31.000 --> 00:00:38.000
 Today's quote is from the Jataka.

00:00:38.000 --> 00:00:47.000
 It's a short quote. I'm going to read it off in full.

00:00:47.000 --> 00:00:52.000
 I don't know the following from the minute.

00:00:52.000 --> 00:00:57.000
 "He who puts aside his wants in order to do the right thing

00:00:57.000 --> 00:00:57.000
,

00:00:57.000 --> 00:01:00.000
 even though it would be difficult,

00:01:00.000 --> 00:01:12.000
 like a patient drinking medicine, later he will rejoice."

00:01:12.000 --> 00:01:22.000
 So yeah, sometimes doing the right thing hurts.

00:01:22.000 --> 00:01:26.000
 Sometimes you want something that you know the right thing

00:01:26.000 --> 00:01:28.000
 isn't it?

00:01:28.000 --> 00:01:31.000
 Just to not take it.

00:01:31.000 --> 00:01:35.710
 Do something, you know, that when I'm not doing it, it's

00:01:35.710 --> 00:01:43.000
 probably the better thing.

00:01:43.000 --> 00:01:52.000
 This is a really important teaching, I think.

00:01:52.000 --> 00:01:58.000
 Because it's hard to go against your desires.

00:01:58.000 --> 00:02:01.000
 You're not equipped to.

00:02:01.000 --> 00:02:13.000
 Our brains are designed to accommodate addiction,

00:02:13.000 --> 00:02:16.000
 accommodate our desires.

00:02:16.000 --> 00:02:20.660
 And the no impulse gets weaker and weaker as you become

00:02:20.660 --> 00:02:22.000
 more addicted.

00:02:22.000 --> 00:02:26.000
 So the no system, whatever that is.

00:02:26.000 --> 00:02:29.010
 The system that says no is quite different from the system

00:02:29.010 --> 00:02:33.000
 that says yes.

00:02:33.000 --> 00:02:36.000
 And so you can only say no for so long,

00:02:36.000 --> 00:02:42.000
 and it takes effort and it's taxing to say no.

00:02:42.000 --> 00:02:50.000
 You prevent yourself from following your desires.

00:02:50.000 --> 00:02:55.000
 That's not the important part of this first.

00:02:55.000 --> 00:02:58.950
 The important part of the teaching is not to deny your

00:02:58.950 --> 00:03:00.000
 desires.

00:03:00.000 --> 00:03:06.660
 It's about the happiness that comes from what's right, from

00:03:06.660 --> 00:03:09.000
 knowing what's right.

00:03:09.000 --> 00:03:13.000
 Just like the happiness that comes from taking medicine.

00:03:13.000 --> 00:03:15.810
 The only reason we take medicine is not because of how it

00:03:15.810 --> 00:03:19.000
 tastes or because of what makes us feel,

00:03:19.000 --> 00:03:24.000
 but it's because it cures us of our illness.

00:03:24.000 --> 00:03:36.350
 And that knowledge, that reassurance, confidence in our

00:03:36.350 --> 00:03:37.000
 mind,

00:03:37.000 --> 00:03:41.000
 that is strong enough.

00:03:41.000 --> 00:03:47.000
 That is strong enough to overcome the desire.

00:03:47.000 --> 00:03:52.870
 You want something and that's a hard argument to fight

00:03:52.870 --> 00:03:54.000
 against.

00:03:54.000 --> 00:03:57.000
 I want it.

00:03:57.000 --> 00:03:59.000
 I want that.

00:03:59.000 --> 00:04:01.000
 That thing makes me happy.

00:04:01.000 --> 00:04:03.000
 It's a hard argument to fight.

00:04:03.000 --> 00:04:12.000
 You can't just say no, no, bad for you.

00:04:12.000 --> 00:04:15.000
 But it's the only way.

00:04:15.000 --> 00:04:18.320
 It's understandable if you don't find something better than

00:04:18.320 --> 00:04:19.000
 the sense of desire.

00:04:19.000 --> 00:04:22.000
 It's understandable that you cling to them.

00:04:22.000 --> 00:04:27.000
 When you find something better...

00:04:27.000 --> 00:04:39.000
 When you find something more powerful,

00:04:39.000 --> 00:04:49.000
 this can be enough to cause you to let go of your desires.

00:04:49.000 --> 00:04:52.000
 This is why giving is better than receiving.

00:04:52.000 --> 00:04:54.000
 It really is.

00:04:54.000 --> 00:04:57.000
 It really feels better to give than to receive.

00:04:57.000 --> 00:05:04.000
 And this is how one conquers miserliness within oneself.

00:05:04.000 --> 00:05:10.000
 Stinginess, practice giving, because it will feel good.

00:05:10.000 --> 00:05:13.000
 It will feel better than getting.

00:05:13.000 --> 00:05:19.040
 If you do it enough, it's actually a way to overcome sting

00:05:19.040 --> 00:05:21.000
iness, to give.

00:05:21.000 --> 00:05:24.000
 Practice giving.

00:05:24.000 --> 00:05:31.000
 It actually works because it feels better.

00:05:31.000 --> 00:05:34.000
 Breaking precepts is always nice.

00:05:34.000 --> 00:05:39.940
 We like to kill and steal and lie and cheat when we get

00:05:39.940 --> 00:05:42.000
 away with it.

00:05:42.000 --> 00:05:45.660
 Normally the only way we do away with it is we prevent

00:05:45.660 --> 00:05:49.000
 ourselves from breaking precepts.

00:05:49.000 --> 00:05:54.000
 Being immoral is by telling us how bad ourselves are.

00:05:54.000 --> 00:05:57.000
 Bad being immoral.

00:05:57.000 --> 00:05:59.000
 That again doesn't really...

00:05:59.000 --> 00:06:02.000
 It's not uplifting.

00:06:02.000 --> 00:06:04.000
 It's not a substitute.

00:06:04.000 --> 00:06:07.000
 You can't live your life just saying everything is bad.

00:06:07.000 --> 00:06:12.000
 This is bad, that's bad.

00:06:12.000 --> 00:06:16.000
 But when you do keep the precepts, when you keep the rules,

00:06:16.000 --> 00:06:20.000
 when you look at it as uplifting,

00:06:20.000 --> 00:06:30.000
 when you feel the power of being a good person,

00:06:30.000 --> 00:06:35.000
 it's this power that actually...

00:06:35.000 --> 00:06:39.000
 The power of goodness, the power of being moral,

00:06:39.000 --> 00:06:41.550
 of knowing that you're an ethical person, you're not

00:06:41.550 --> 00:06:44.000
 stealing, you're not killing.

00:06:44.000 --> 00:06:46.000
 That power is uplifting.

00:06:46.000 --> 00:06:50.630
 You sit and you think about how you've given freedom from

00:06:50.630 --> 00:06:53.000
 fear to all beings.

00:06:53.000 --> 00:07:00.000
 Abhayadhana, the gift of fearlessness.

00:07:00.000 --> 00:07:05.000
 No one needs to be afraid of you.

00:07:05.000 --> 00:07:11.520
 It's just with the precepts, and it's a gift, and it feels

00:07:11.520 --> 00:07:13.000
 good.

00:07:13.000 --> 00:07:25.000
 [Coughing]

00:07:25.000 --> 00:07:33.000
 The same goes with green, with anger, even with delusion,

00:07:33.000 --> 00:07:34.000
 with ignorance.

00:07:34.000 --> 00:07:37.000
 They say ignorance is bliss.

00:07:37.000 --> 00:07:39.000
 So it's nice to just live your life and not care,

00:07:39.000 --> 00:07:43.390
 and not carry the world on your shoulders, or think about

00:07:43.390 --> 00:07:44.000
 death,

00:07:44.000 --> 00:07:47.030
 or think about getting sick or old age, or think about

00:07:47.030 --> 00:07:48.000
 suffering,

00:07:48.000 --> 00:07:51.000
 think about the world's problems.

00:07:51.000 --> 00:07:54.000
 It's blissful to not think about these things.

00:07:54.000 --> 00:07:57.640
 It's really a horrible philosophy because your problems don

00:07:57.640 --> 00:07:58.000
't go away

00:07:58.000 --> 00:08:00.000
 just because you're ignorant of them.

00:08:00.000 --> 00:08:04.210
 And so that bliss is not sustainable, it's not rational, it

00:08:04.210 --> 00:08:06.000
's not reasonable.

00:08:06.000 --> 00:08:09.000
 But the opposite seems even worse, to have to live your

00:08:09.000 --> 00:08:09.000
 life

00:08:09.000 --> 00:08:13.000
 and worry about these things. What good does that do?

00:08:13.000 --> 00:08:16.000
 What good does it do to concern yourself with the problems

00:08:16.000 --> 00:08:17.000
 of life,

00:08:17.000 --> 00:08:21.000
 the problems of the world?

00:08:21.000 --> 00:08:29.000
 It just makes you stressed and sick and upset.

00:08:29.000 --> 00:08:31.440
 So ignorance is better, but the only thing better than

00:08:31.440 --> 00:08:34.000
 ignorance is knowledge,

00:08:34.000 --> 00:08:37.670
 and not the kind of knowledge, not knowledge of your

00:08:37.670 --> 00:08:38.000
 problems,

00:08:38.000 --> 00:08:42.000
 but understanding of your problems.

00:08:42.000 --> 00:08:44.870
 Maybe, sir, maybe knowledge is a bad word, but

00:08:44.870 --> 00:08:46.000
 understanding.

00:08:46.000 --> 00:08:51.000
 So ignorance is bliss, but because knowledge is suffering,

00:08:51.000 --> 00:08:58.000
 but understanding, that's peace.

00:08:58.000 --> 00:09:03.070
 Because you understand your problems, they cease to be

00:09:03.070 --> 00:09:04.000
 problems.

00:09:04.000 --> 00:09:09.000
 And then thinking about them is not stressful.

00:09:09.000 --> 00:09:13.000
 Then they have no power over you.

00:09:13.000 --> 00:09:18.000
 When they do come to pass, when they do rear their heads,

00:09:18.000 --> 00:09:23.000
 they have no power over you because you understand them.

00:09:23.000 --> 00:09:27.000
 You know their true nature.

00:09:27.000 --> 00:09:38.330
 They're not shocked or upset by them, but surprised by them

00:09:38.330 --> 00:09:39.000
.

00:09:39.000 --> 00:09:45.000
 And so there's wisdom, wisdom that comes through meditation

00:09:45.000 --> 00:09:45.000
,

00:09:45.000 --> 00:09:54.000
 through understanding, through experience.

00:09:54.000 --> 00:09:58.940
 This is more powerful than ignorance, better than ignorance

00:09:58.940 --> 00:09:59.000
.

00:09:59.000 --> 00:10:04.000
 The simile with the medicine,

00:10:04.000 --> 00:10:09.000
 a person who steeps themselves in greed, anger and delusion

00:10:09.000 --> 00:10:09.000
,

00:10:09.000 --> 00:10:14.160
 follows their desires, even when it means not doing the

00:10:14.160 --> 00:10:16.000
 right thing.

00:10:16.000 --> 00:10:19.000
 It's like a person who doesn't take their medicine,

00:10:19.000 --> 00:10:22.000
 a sick person who doesn't take their medicine,

00:10:22.000 --> 00:10:25.680
 who doesn't take their medicine, who doesn't take their

00:10:25.680 --> 00:10:26.000
 medicine,

00:10:26.000 --> 00:10:29.690
 who doesn't take their medicine, who doesn't take their

00:10:29.690 --> 00:10:30.000
 medicine,

00:10:30.000 --> 00:10:33.000
 who doesn't take their medicine,

00:10:33.000 --> 00:10:36.670
 who doesn't take their medicine, who doesn't take their

00:10:36.670 --> 00:10:37.000
 medicine,

00:10:37.000 --> 00:10:40.630
 who doesn't take their medicine, who doesn't take their

00:10:40.630 --> 00:10:41.000
 medicine,

00:10:41.000 --> 00:10:44.380
 who doesn't take their medicine, who doesn't take their

00:10:44.380 --> 00:10:45.000
 medicine,

00:10:45.000 --> 00:10:48.000
 who doesn't take their medicine, who doesn't take their

00:10:48.000 --> 00:10:48.000
 medicine,

00:10:48.000 --> 00:10:52.000
 because sometimes you have to do what is unpleasant.

00:10:52.000 --> 00:10:55.300
 You have to go against your desires, you have to go against

00:10:55.300 --> 00:10:56.000
 your anger,

00:10:56.000 --> 00:10:59.000
 be patient when other people make you upset,

00:10:59.000 --> 00:11:02.000
 when things make you upset, be patient.

00:11:02.000 --> 00:11:08.000
 Bear with unpleasantness.

00:11:08.000 --> 00:11:13.020
 Sometimes you have to wake yourself up, slap yourself in

00:11:13.020 --> 00:11:15.000
 the face,

00:11:15.000 --> 00:11:18.000
 face your problems, learn about your problems,

00:11:18.000 --> 00:11:21.000
 understand your problems, rather than run away from them,

00:11:21.000 --> 00:11:24.000
 or ignore them, or pretend they don't exist,

00:11:24.000 --> 00:11:27.000
 or drown yourself in drugs and alcohol,

00:11:27.000 --> 00:11:33.000
 so that you don't have to think about them.

00:11:33.000 --> 00:11:43.000
 If you can actually understand them.

00:11:43.000 --> 00:11:50.000
 If you can cultivate wisdom about them.

00:11:50.000 --> 00:11:57.200
 Then you rejoice, then you truly, truly find true peace and

00:11:57.200 --> 00:12:00.000
 happiness.

00:12:00.000 --> 00:12:08.000
 There is lasting, substantial, meaningful.

00:12:08.000 --> 00:12:16.000
 Interesting little quote.

00:12:16.000 --> 00:12:22.000
 I'm quite busy this weekend, I'm still not overly sicker,

00:12:22.000 --> 00:12:30.000
 so tomorrow I'm canceling second time.

00:12:30.000 --> 00:12:37.000
 I'll see you about more nine o'clock broadcasts.

00:12:37.000 --> 00:12:47.230
 I'm behind in my schoolwork, so I'll get catched up in the

00:12:47.230 --> 00:12:50.000
 next week.

00:12:50.000 --> 00:12:54.000
 Thank you all for tuning in, and good night.

