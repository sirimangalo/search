 your study. Peter asked what is the formal way to greet a
 monk in Sri Lanka
 and is it different from other countries? Is high bhante
 acceptable?
 I think high bhante you can say when you're familiar. I
 would possibly call
 say high bhante if I meet bhante you tadamo somewhere here
 in the monastery.
 If it's an official occasion or a monk you don't know don't
 say high bhante I would say rather
 say namasa khan bhante or... That's Thai you know. No, isn
't it? Is it?
 Okay then don't say that. High corruption of the word nam
askara. It would be namaste no?
 Namaste bhante. I would say that's that would be but nam
aste is also no good because that's
 Sanskrit. Well I mean sorry to jump in but yeah that's fine
. Would you say high father
 to a priest or high brother to a Christian monk? I don't
 think people normally would if they
 have respect for the person they would I would think at
 least say hello father or something like
 that because I mean bhante is not a nickname it's a title
 of respect it's like high venerable I
 guess you could say it but it's you know I mean it gets
 ridiculous if you like say
 hi your majesty or something like that you don't really for
 a king or something but
 it's not on that level but because of the the fact that bh
ante is a title of respect I think
 high bhante is a little bit too informal. It differs from
 country to country as we
 experienced just now in Thailand you would say namaskara or
 namaskara and
 and you would in most countries I think put your hands
 together and
 don't look straight into the face in the beginning later in
 the conversation yes but
 in the beginning have the head a little bit lowered or the
 even a little bit bent or so
 if it's an important monk the the more important the monk
 the deeper the bow
 and things like that. Yeah I mean here in Sri Lanka they
 they touch their hands to your feet
 if people do that when they respect you and we do that when
 we respect the senior monk
 we touch we touch our head actually to the feet of the
 elder and people do that for their parents
 or I learned here in Sri Lanka from the bhikkhunis who have
 stayed in Sri Lanka before me that we do
 like people do to the queen of England I don't know how is
 that how that is called but
 yeah probably like and with the palms together so
 that would the the appropriate greeting for a bhikkhuni to
 greet a higher monk. We were talking
 about the word bhante because I'm still pretty adamant that
 it's masculine but that's really
 a grammatical question they'll often say ay-ay-ay or you
 could say bad day I think but no one
 would have no one uses it so but I think you could looking
 at because it is used there's two two
 angels talking together and one says calls the other bhante
 the female calls the male bhante
 and the male turns around and calls the female bad day
 which they're actually pretty much the
 same word just one's masculine one's feminine it's not not
 so clear they are not asking for
 okay
