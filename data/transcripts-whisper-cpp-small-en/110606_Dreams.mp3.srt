1
00:00:00,000 --> 00:00:03,860
 Hello, welcome back to Ask a Monk. Today I will be

2
00:00:03,860 --> 00:00:05,480
 answering the question as to whether

3
00:00:05,480 --> 00:00:11,050
 dreams have karmic potency. So whether they have the

4
00:00:11,050 --> 00:00:17,840
 ability to give rise to ethical states,

5
00:00:17,840 --> 00:00:20,810
 do they have ethical implications in the sense that can

6
00:00:20,810 --> 00:00:22,480
 they affect our lives and can they

7
00:00:22,480 --> 00:00:26,080
 affect the world around us? Is it possible to perform

8
00:00:26,080 --> 00:00:28,480
 Buddhist karma, you know, to make

9
00:00:28,480 --> 00:00:33,650
 bad karma while you're asleep? And the answer I think is

10
00:00:33,650 --> 00:00:38,360
 quite clearly yes. And it's important

11
00:00:38,360 --> 00:00:41,840
 that we understand what we mean by karma, and this sort of

12
00:00:41,840 --> 00:00:43,640
 helps us to understand what

13
00:00:43,640 --> 00:00:47,860
 we mean by karma in Buddhism. Karma isn't an action that

14
00:00:47,860 --> 00:00:49,720
 you do with the body or speech,

15
00:00:49,720 --> 00:00:52,360
 it's something that arises in the mind. When you intend to

16
00:00:52,360 --> 00:00:53,920
 do something, that's karma.

17
00:00:53,920 --> 00:00:57,280
 So the only reason to say killing is wrong or stealing is

18
00:00:57,280 --> 00:00:59,200
 wrong, or as we call bad karma,

19
00:00:59,200 --> 00:01:03,320
 is because of the effect that such things have on our mind

20
00:01:03,320 --> 00:01:04,880
 and the mental aspect of

21
00:01:04,880 --> 00:01:11,060
 the act, that your mind is building up the tendency to be

22
00:01:11,060 --> 00:01:14,440
 angry or greedy or corrupt.

23
00:01:14,440 --> 00:01:20,970
 So in that sense, because dreams tend to be ethically

24
00:01:20,970 --> 00:01:25,120
 charged, so we give rise to these

25
00:01:25,120 --> 00:01:29,340
 same states in our mind of greed and anger and delusion,

26
00:01:29,340 --> 00:01:31,720
 they can all arise in our mind.

27
00:01:31,720 --> 00:01:34,610
 Quite clearly there are ethical implications. Now, I want

28
00:01:34,610 --> 00:01:36,640
 to talk about a few things here,

29
00:01:36,640 --> 00:01:41,600
 so I'm going to try to keep them in a clear order. The

30
00:01:41,600 --> 00:01:43,760
 first thing I should talk about

31
00:01:43,760 --> 00:01:49,510
 is what we understand dreams to be in Buddhism. So what are

32
00:01:49,510 --> 00:01:53,600
 we talking about? We have a description

33
00:01:53,600 --> 00:01:57,440
 of the different kinds of dreams in one of the old Buddhist

34
00:01:57,440 --> 00:01:59,520
 texts, the questions of King

35
00:01:59,520 --> 00:02:04,130
 Melinda or the Melinda Panha. In that text, it's mentioned

36
00:02:04,130 --> 00:02:05,640
 that there are six types of

37
00:02:05,640 --> 00:02:10,010
 dreams, and so this will help us to understand what exactly

38
00:02:10,010 --> 00:02:12,200
 we mean by dreams and exactly

39
00:02:12,200 --> 00:02:18,720
 how relevant they are to our practice and so on. Because I

40
00:02:18,720 --> 00:02:19,360
 also want to talk about how

41
00:02:19,360 --> 00:02:23,160
 we should approach dreams and what it means in terms of our

42
00:02:23,160 --> 00:02:24,160
 practice.

43
00:02:24,160 --> 00:02:29,510
 So there are six kinds. The first three are physical, wind,

44
00:02:29,510 --> 00:02:31,920
 bile, phlegm, and this is

45
00:02:31,920 --> 00:02:36,010
 just based on the medical terminology that they would use

46
00:02:36,010 --> 00:02:37,960
 in the time of the Buddha or

47
00:02:37,960 --> 00:02:41,260
 in ancient India. So basically it means a disruption in the

48
00:02:41,260 --> 00:02:44,000
 body. There's something,

49
00:02:44,000 --> 00:02:47,760
 maybe you eat bad food or maybe your body is out of order,

50
00:02:47,760 --> 00:02:49,640
 you have sickness or so on,

51
00:02:49,640 --> 00:02:56,500
 that can easily give rise to dreams. And the other three

52
00:02:56,500 --> 00:03:00,160
 are some external influence, which

53
00:03:00,160 --> 00:03:03,490
 could be a person around you, it could be spirits, it could

54
00:03:03,490 --> 00:03:05,520
 be angels. I think what

55
00:03:05,520 --> 00:03:07,680
 they're talking about is angels, but actually if you think

56
00:03:07,680 --> 00:03:08,840
 about it, we don't even have

57
00:03:08,840 --> 00:03:11,850
 to go so far. If someone starts whispering in your ear

58
00:03:11,850 --> 00:03:13,560
 where you're sleeping or if you

59
00:03:13,560 --> 00:03:16,250
 hear noises, if you've ever had it that something's going

60
00:03:16,250 --> 00:03:18,000
 on around you and you start dreaming

61
00:03:18,000 --> 00:03:22,050
 about that something based on the sound that you hear or if

62
00:03:22,050 --> 00:03:23,960
 it gets hot that can affect

63
00:03:23,960 --> 00:03:29,400
 your state of mind and so on, it can cause you to dream.

64
00:03:29,400 --> 00:03:33,470
 The fifth is something from the past and the sixth is

65
00:03:33,470 --> 00:03:36,480
 something from the future. This is

66
00:03:36,480 --> 00:03:42,080
 where it gets interesting. So some people have visions of

67
00:03:42,080 --> 00:03:44,520
 things in their dreams, can

68
00:03:44,520 --> 00:03:47,350
 be from the past in this life, sometimes people are

69
00:03:47,350 --> 00:03:49,640
 suspicious that it's something from their

70
00:03:49,640 --> 00:03:53,180
 past lives, but let's just say something in the past in

71
00:03:53,180 --> 00:03:55,120
 this life because that's really

72
00:03:55,120 --> 00:03:59,060
 obvious. Memories that we've had, things that we keep in

73
00:03:59,060 --> 00:04:01,560
 our minds can cause us to dream.

74
00:04:01,560 --> 00:04:08,070
 But the sixth one is considered to be important and it's

75
00:04:08,070 --> 00:04:11,440
 worth noting that only the sixth is

76
00:04:11,440 --> 00:04:16,060
 considered to be important in terms of actually paying some

77
00:04:16,060 --> 00:04:18,560
 kind of attention to it. The sixth

78
00:04:18,560 --> 00:04:22,250
 kind is something from the future and some people may not

79
00:04:22,250 --> 00:04:24,080
 even believe this, but other

80
00:04:24,080 --> 00:04:27,320
 people will be emphatic that this sort of thing happens to

81
00:04:27,320 --> 00:04:28,880
 them where they'll dream

82
00:04:28,880 --> 00:04:33,040
 something and then it happens. Suddenly it comes in their

83
00:04:33,040 --> 00:04:35,040
 life, people can even have

84
00:04:35,040 --> 00:04:39,480
 this in meditation. But it's possible that something from

85
00:04:39,480 --> 00:04:41,800
 the future can cause a dream,

86
00:04:41,800 --> 00:04:44,830
 something that's about to happen. And you can understand

87
00:04:44,830 --> 00:04:46,200
 this in different ways. It

88
00:04:46,200 --> 00:04:50,410
 could be simply because of the power of what's going to

89
00:04:50,410 --> 00:04:53,120
 happen because we consider that the

90
00:04:53,120 --> 00:04:58,410
 universe is much more than we can see and experience

91
00:04:58,410 --> 00:05:00,080
 normally. So there are a lot of

92
00:05:00,080 --> 00:05:05,620
 forces that are at work potentially and those forces can be

93
00:05:05,620 --> 00:05:08,560
 experienced if you know about

94
00:05:08,560 --> 00:05:12,050
 how animals are able to predict the weather and are able to

95
00:05:12,050 --> 00:05:13,800
 predict disasters. Like before

96
00:05:13,800 --> 00:05:18,820
 the tsunami in Asia, they say that the elephants all ran in

97
00:05:18,820 --> 00:05:21,560
 land like they knew it was going

98
00:05:21,560 --> 00:05:29,020
 to come. And so on, just kind of the forces of nature that

99
00:05:29,020 --> 00:05:31,240
 we're not able to sense in

100
00:05:31,240 --> 00:05:35,840
 our normal state. Because actually when you're sleeping,

101
00:05:35,840 --> 00:05:37,400
 let's talk a little bit about what

102
00:05:37,400 --> 00:05:41,840
 exactly dreams are. So these are the six sources of dreams.

103
00:05:41,840 --> 00:05:42,880
 But what's happening in the dream

104
00:05:42,880 --> 00:05:46,750
 state is your mind is not asleep and is not awake. This is

105
00:05:46,750 --> 00:05:48,560
 what we know in science as

106
00:05:48,560 --> 00:05:52,340
 well. But from a meditative point of view, there's still a

107
00:05:52,340 --> 00:05:55,480
 great amount of concentration,

108
00:05:55,480 --> 00:06:00,180
 but there's no mindfulness. So you're not really in control

109
00:06:00,180 --> 00:06:02,080
 obviously for most of us

110
00:06:02,080 --> 00:06:04,890
 unless you train yourself because there are actually people

111
00:06:04,890 --> 00:06:06,560
 who train themselves in dreams.

112
00:06:06,560 --> 00:06:08,680
 And I'm not going to teach you to do this. This isn't what

113
00:06:08,680 --> 00:06:10,720
 I teach. I just want to help

114
00:06:10,720 --> 00:06:14,220
 people to understand this so we can move on and continue

115
00:06:14,220 --> 00:06:16,280
 with our meditation because this

116
00:06:16,280 --> 00:06:20,850
 will interrupt your life and people do wonder, well, what

117
00:06:20,850 --> 00:06:23,520
 is the significance of dreams?

118
00:06:23,520 --> 00:06:25,970
 Most dreams are not significant because as I said, you're

119
00:06:25,970 --> 00:06:27,160
 in this state and you can be

120
00:06:27,160 --> 00:06:30,070
 easily influenced by things because you don't have the

121
00:06:30,070 --> 00:06:32,280
 ability to judge, to discriminate,

122
00:06:32,280 --> 00:06:38,460
 and to catch yourself. And so you easily slip into illusion

123
00:06:38,460 --> 00:06:41,200
 and fantasy. But there are certain

124
00:06:41,200 --> 00:06:45,260
 dreams that are affected by the future. And this is what

125
00:06:45,260 --> 00:06:47,320
 leads people I think to this

126
00:06:47,320 --> 00:06:49,770
 and the dreams about the past, which is what leads people

127
00:06:49,770 --> 00:06:51,160
 to put significance in dreams.

128
00:06:51,160 --> 00:06:54,540
 So they might experience a dream that they feel has some

129
00:06:54,540 --> 00:06:56,480
 significance and that should

130
00:06:56,480 --> 00:06:59,820
 be explored and has some meaning and importance based on

131
00:06:59,820 --> 00:07:01,920
 the past and something they might

132
00:07:01,920 --> 00:07:07,150
 be repressing and so on. Or they might feel that they have

133
00:07:07,150 --> 00:07:09,760
 some dream and then it sort

134
00:07:09,760 --> 00:07:12,590
 of comes true in the future. And so as a result, they feel

135
00:07:12,590 --> 00:07:15,840
 that dreams have great significance.

136
00:07:15,840 --> 00:07:21,110
 Psychotherapists and Freud and Jung put great emphasis on

137
00:07:21,110 --> 00:07:24,560
 dreams and the importance of them.

138
00:07:24,560 --> 00:07:27,070
 I want to say that we shouldn't put great importance on our

139
00:07:27,070 --> 00:07:28,440
 dreams. And this comes back

140
00:07:28,440 --> 00:07:31,810
 to the idea of whether you can create karma. As I said,

141
00:07:31,810 --> 00:07:34,120
 there are karmic states that arise.

142
00:07:34,120 --> 00:07:42,360
 And so you are potentially creating more karma. But the

143
00:07:42,360 --> 00:07:44,120
 point is that you can't really control

144
00:07:44,120 --> 00:07:47,550
 that. You can't say, "I'm not going to create karma." You

145
00:07:47,550 --> 00:07:49,200
 can't say, "Stop." You can't

146
00:07:49,200 --> 00:07:52,740
 stop yourself from giving rise to fear or fear for

147
00:07:52,740 --> 00:07:55,520
 nightmares or anger or attachment and

148
00:07:55,520 --> 00:08:01,370
 lust and so on. So it's much more useful. What I think

149
00:08:01,370 --> 00:08:04,440
 dreams are really useful for

150
00:08:04,440 --> 00:08:10,020
 is for understanding our mind state. And we should never

151
00:08:10,020 --> 00:08:12,640
 take the whole of the dream as

152
00:08:12,640 --> 00:08:16,360
 somehow significant, even if it means something for the

153
00:08:16,360 --> 00:08:18,640
 future or it has the potential to be

154
00:08:18,640 --> 00:08:28,050
 a prediction or foretelling the future. Or even if it comes

155
00:08:28,050 --> 00:08:29,880
 from some repressed state

156
00:08:29,880 --> 00:08:32,800
 that we have in the past, the point is that you're in a

157
00:08:32,800 --> 00:08:35,160
 state where the mind is not connected.

158
00:08:35,160 --> 00:08:39,310
 It's not functioning in a logical manner. So it's going to

159
00:08:39,310 --> 00:08:41,200
 make connections between

160
00:08:41,200 --> 00:08:44,140
 things that otherwise don't have connections. So maybe part

161
00:08:44,140 --> 00:08:46,800
 of it comes from the fact that

162
00:08:46,800 --> 00:08:49,610
 you had some traumatic experience in the past, but part of

163
00:08:49,610 --> 00:08:52,200
 it just comes from your mind,

164
00:08:52,200 --> 00:08:56,510
 the chaos in the mind and the nature of the mind to dream

165
00:08:56,510 --> 00:08:58,880
 and to imagine. Likewise, you

166
00:08:58,880 --> 00:09:01,840
 might remember to think of something in the future or you

167
00:09:01,840 --> 00:09:03,280
 might have the ability to see

168
00:09:03,280 --> 00:09:05,430
 what's going to happen in the future, but it becomes

169
00:09:05,430 --> 00:09:07,760
 distorted. It is distorted by the

170
00:09:07,760 --> 00:09:10,930
 mind. It's the same as when you go in a hallucinogenic drug

171
00:09:10,930 --> 00:09:14,160
 and you might have visions of angels,

172
00:09:14,160 --> 00:09:19,550
 deities and all sorts of spiritual things. So you think

173
00:09:19,550 --> 00:09:21,640
 that this is the truth and you're

174
00:09:21,640 --> 00:09:26,640
 seeing something that we can't normally see, but it's also

175
00:09:26,640 --> 00:09:29,280
 the brain that is creating,

176
00:09:29,280 --> 00:09:32,490
 is imagining, is hallucinating. So whether part of it is

177
00:09:32,490 --> 00:09:34,720
 real and whether it is sending

178
00:09:34,720 --> 00:09:37,170
 the mind off into a dimension that can see things that we

179
00:09:37,170 --> 00:09:39,880
 otherwise couldn't see, it's

180
00:09:39,880 --> 00:09:42,940
 also the brain and it's also doing all sorts of crazy

181
00:09:42,940 --> 00:09:45,480
 things. I think if you look carefully,

182
00:09:45,480 --> 00:09:47,480
 you'll see that this is true with dreams and that they

183
00:09:47,480 --> 00:09:51,400
 shouldn't be trusted. Also, I think,

184
00:09:51,400 --> 00:09:53,740
 in the sense of whether you can create karma or the karma

185
00:09:53,740 --> 00:09:57,720
 that you're creating, because

186
00:09:57,720 --> 00:10:01,420
 for one thing, the karma is not going to be strong because

187
00:10:01,420 --> 00:10:03,160
 you don't really intend to

188
00:10:03,160 --> 00:10:06,440
 do this or that in your dreams. You just kind of get angry

189
00:10:06,440 --> 00:10:08,340
 or worried or afraid in the case

190
00:10:08,340 --> 00:10:11,940
 of nightmares or have lust in the case of lustful dreams,

191
00:10:11,940 --> 00:10:13,600
 but you don't really have

192
00:10:13,600 --> 00:10:18,190
 this strong intention because your mind is not clear. Your

193
00:10:18,190 --> 00:10:20,240
 mind doesn't have the will

194
00:10:20,240 --> 00:10:25,880
 or the awareness or the mindfulness which is able to make

195
00:10:25,880 --> 00:10:28,320
 decisions clearly. So it's

196
00:10:28,320 --> 00:10:34,300
 going to be weak karma for one thing and also for another

197
00:10:34,300 --> 00:10:37,520
 thing, you're all mixed. There's

198
00:10:37,520 --> 00:10:41,230
 many states that are mixed up and as I said, connections

199
00:10:41,230 --> 00:10:43,340
 can be made in an illogical manner

200
00:10:43,340 --> 00:10:47,800
 and therefore you have all these crazy dreams. So what we

201
00:10:47,800 --> 00:10:49,660
 should use dreams for is to be

202
00:10:49,660 --> 00:10:52,110
 able to judge our mind state because what's good about them

203
00:10:52,110 --> 00:10:53,540
 is you don't have this control.

204
00:10:53,540 --> 00:10:57,790
 You aren't covering things up. Because there's no control,

205
00:10:57,790 --> 00:10:59,680
 you're able to see many things

206
00:10:59,680 --> 00:11:03,180
 about yourself that you wouldn't otherwise see and this isn

207
00:11:03,180 --> 00:11:04,920
't the content of the dream,

208
00:11:04,920 --> 00:11:07,580
 but it's the quality of the dream. So if you have a dream

209
00:11:07,580 --> 00:11:08,960
 that's based on anger, if you

210
00:11:08,960 --> 00:11:11,380
 have a dream that's based on fear, nightmares, if you have

211
00:11:11,380 --> 00:11:12,700
 a dream that's based on lust or

212
00:11:12,700 --> 00:11:15,700
 so on, this shows you what's in your mind and often in a

213
00:11:15,700 --> 00:11:17,640
 way that you otherwise wouldn't

214
00:11:17,640 --> 00:11:20,300
 be able to see. So people who are otherwise calm and

215
00:11:20,300 --> 00:11:22,220
 controlled might find themselves suddenly

216
00:11:22,220 --> 00:11:24,590
 having nightmares and so they think something is wrong.

217
00:11:24,590 --> 00:11:26,320
 Well the truth is this is you seeing

218
00:11:26,320 --> 00:11:29,890
 something deep down that you're repressing because we're

219
00:11:29,890 --> 00:11:31,440
 only able to repress things

220
00:11:31,440 --> 00:11:34,760
 in our daily life by force of will, by actually some sort

221
00:11:34,760 --> 00:11:39,400
 of mindfulness and the ability to

222
00:11:39,400 --> 00:11:41,990
 choose not to follow, which we don't have when we're

223
00:11:41,990 --> 00:11:44,160
 dreaming, when we're asleep. So

224
00:11:44,160 --> 00:11:47,030
 in that sense I would say that really the only thing that

225
00:11:47,030 --> 00:11:48,480
 we should do with dreams is

226
00:11:48,480 --> 00:11:51,640
 use them to see where we have work to do and to help us to

227
00:11:51,640 --> 00:11:53,800
 understand how our mind works.

228
00:11:53,800 --> 00:11:57,320
 Don't take the content of the dream as important. This is a

229
00:11:57,320 --> 00:11:59,480
 big mistake because you don't know

230
00:11:59,480 --> 00:12:02,050
 where it's coming from. It could be coming simply because

231
00:12:02,050 --> 00:12:03,240
 you had bad food, it could

232
00:12:03,240 --> 00:12:05,940
 be coming from the way your body, the way your brain is

233
00:12:05,940 --> 00:12:07,680
 functioning, the chemicals and

234
00:12:07,680 --> 00:12:11,930
 whatever substances you've been taking, the different foods

235
00:12:11,930 --> 00:12:13,560
 that don't react well and

236
00:12:13,560 --> 00:12:16,500
 so on. It could be coming partly from your past, it could

237
00:12:16,500 --> 00:12:18,000
 be coming from the future,

238
00:12:18,000 --> 00:12:21,200
 it could be coming from external influences, sounds that

239
00:12:21,200 --> 00:12:22,960
 you hear or even angels. This

240
00:12:22,960 --> 00:12:26,090
 is what they say. There could be spirits around and because

241
00:12:26,090 --> 00:12:28,480
 you have strong concentration,

242
00:12:28,480 --> 00:12:32,950
 you're put into a state where you might be able to, similar

243
00:12:32,950 --> 00:12:35,040
 to meditation, where people

244
00:12:35,040 --> 00:12:38,070
 practice meditation, go into great states of concentration

245
00:12:38,070 --> 00:12:39,440
 and are able to see and hear

246
00:12:39,440 --> 00:12:43,100
 and experience things that we can't otherwise. So it can

247
00:12:43,100 --> 00:12:44,360
 come from all sorts of different

248
00:12:44,360 --> 00:12:48,200
 sources and it's not nearly as reliable as a meditation,

249
00:12:48,200 --> 00:12:50,080
 for example, in any way. If

250
00:12:50,080 --> 00:12:53,310
 you want to predict the future, go and practice meditation.

251
00:12:53,310 --> 00:12:55,000
 If you want to understand about

252
00:12:55,000 --> 00:12:57,950
 your past and what sort of things you're repressing, you

253
00:12:57,950 --> 00:12:59,960
 practice meditation. If you want to see

254
00:12:59,960 --> 00:13:03,420
 angels and spirits and so on, practice meditation. There

255
00:13:03,420 --> 00:13:05,760
 are different meditations for this.

256
00:13:05,760 --> 00:13:09,370
 But as I said, when you're asleep, when you're dreaming,

257
00:13:09,370 --> 00:13:11,380
 your dreams will show you something

258
00:13:11,380 --> 00:13:14,490
 about yourself, about how your mind works. Because as I'm

259
00:13:14,490 --> 00:13:17,000
 told, a perfectly enlightened

260
00:13:17,000 --> 00:13:20,870
 person, a person who has done away with all of their mental

261
00:13:20,870 --> 00:13:22,860
 defilements won't dream. When

262
00:13:22,860 --> 00:13:28,620
 they sleep, they sleep soundly and they sleep mindfully. So

263
00:13:28,620 --> 00:13:31,400
 actually, there's some sort of

264
00:13:31,400 --> 00:13:33,960
 clarity of mind during the sleep and when they wake up,

265
00:13:33,960 --> 00:13:37,080
 they're fully refreshed and rested.

266
00:13:37,080 --> 00:13:40,750
 But irregardless, it's just important that like anything,

267
00:13:40,750 --> 00:13:42,360
 we don't follow them and we

268
00:13:42,360 --> 00:13:45,960
 don't project on them. A dream is what it is. You had that

269
00:13:45,960 --> 00:13:47,680
 experience in your dream.

270
00:13:47,680 --> 00:13:50,880
 It doesn't mean that it's going to happen in the future or

271
00:13:50,880 --> 00:13:52,680
 that somehow it has some special

272
00:13:52,680 --> 00:13:56,430
 significance or someone's trying to tell you something or

273
00:13:56,430 --> 00:13:57,840
 so on. Even if it's an angel

274
00:13:57,840 --> 00:14:00,410
 or a spirit trying to tell you something or even your mind

275
00:14:00,410 --> 00:14:01,880
 trying to tell you something,

276
00:14:01,880 --> 00:14:08,600
 that doesn't mean you should follow it. Because the source

277
00:14:08,600 --> 00:14:11,840
 of the dream may still be full

278
00:14:11,840 --> 00:14:15,630
 of delusion and misunderstanding as well and lead you on

279
00:14:15,630 --> 00:14:17,160
 the wrong path. So dreams can

280
00:14:17,160 --> 00:14:20,360
 be karmic and that's maybe the last thing that I would say

281
00:14:20,360 --> 00:14:21,700
 is that this should be a

282
00:14:21,700 --> 00:14:24,780
 caution for us that we can't always control our minds in

283
00:14:24,780 --> 00:14:26,560
 the states like dream but even

284
00:14:26,560 --> 00:14:31,290
 in our waking states, dreams are just another reason for us

285
00:14:31,290 --> 00:14:33,520
 to work on our minds because

286
00:14:33,520 --> 00:14:39,030
 otherwise when we fall asleep, we're going to dream. What

287
00:14:39,030 --> 00:14:40,480
 we mean by creating karma even

288
00:14:40,480 --> 00:14:43,590
 when we dream is that we're building up the tendency to act

289
00:14:43,590 --> 00:14:44,880
 in that way. So the dreams

290
00:14:44,880 --> 00:14:47,940
 are reinforcing these emotions, reinforcing our fear,

291
00:14:47,940 --> 00:14:50,080
 reinforcing our stress, reinforcing

292
00:14:50,080 --> 00:14:53,320
 our anger, reinforcing our lust, reinforcing the states

293
00:14:53,320 --> 00:14:57,160
 that we're trying to do away with.

294
00:14:57,160 --> 00:15:00,900
 They can anyway. You can also have positive dreams where

295
00:15:00,900 --> 00:15:03,040
 you have love and kindness towards

296
00:15:03,040 --> 00:15:05,980
 other beings as possible. But generally you'll see the

297
00:15:05,980 --> 00:15:07,520
 things you're clinging to when you

298
00:15:07,520 --> 00:15:10,690
 dream and so it's an example of what we have to get rid of

299
00:15:10,690 --> 00:15:13,080
 and it's also a reason for us

300
00:15:13,080 --> 00:15:17,000
 to in a sense be concerned and take our meditation

301
00:15:17,000 --> 00:15:21,080
 seriously because otherwise this is the opposite

302
00:15:21,080 --> 00:15:24,610
 of meditating where you're not mindful. You will develop

303
00:15:24,610 --> 00:15:27,960
 these states and they will become

304
00:15:27,960 --> 00:15:31,000
 more pronounced. So I hope that helps and answers the

305
00:15:31,000 --> 00:15:32,200
 question. Thank you for tuning

306
00:15:32,200 --> 00:15:36,640
 in once again. This has been another episode of Ask a Monk.

307
00:15:36,640 --> 00:15:37,600
 This is the first time using

308
00:15:37,600 --> 00:15:40,330
 this new microphone. So I've got a microphone. I hope it

309
00:15:40,330 --> 00:15:42,120
 works. I can sit further away from

310
00:15:42,120 --> 00:15:47,720
 the camera and let you see more than just my face. So

311
00:15:47,720 --> 00:15:48,920
 thanks for tuning in all the best.

