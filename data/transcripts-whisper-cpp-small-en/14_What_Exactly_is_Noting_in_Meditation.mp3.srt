1
00:00:00,000 --> 00:00:04,000
 What exactly is noting in meditation?

2
00:00:04,000 --> 00:00:08,600
 Question, what exactly is noting in meditation?

3
00:00:08,600 --> 00:00:13,040
 Answer, noting is the straightening of the mind in regards

4
00:00:13,040 --> 00:00:14,200
 to the object.

5
00:00:14,200 --> 00:00:18,410
 Noting is reminding yourself of the essence and reality of

6
00:00:18,410 --> 00:00:19,900
 the experience,

7
00:00:19,900 --> 00:00:23,970
 as opposed to seeing one's experiences as entities with

8
00:00:23,970 --> 00:00:26,600
 qualities that make one react

9
00:00:26,600 --> 00:00:32,900
 with liking, disliking, or interpretation as me, mine, etc.

10
00:00:32,900 --> 00:00:38,100
 Noting is for the purpose of experiencing reality as it is.

11
00:00:38,100 --> 00:00:42,250
 By reminding yourself what it is, the word sati is

12
00:00:42,250 --> 00:00:44,500
 translated as mindfulness,

13
00:00:44,500 --> 00:00:49,410
 but the word sati means remembrance, reminding, reminded

14
00:00:49,410 --> 00:00:49,800
ness,

15
00:00:49,800 --> 00:00:53,940
 recollectedness of the ability to recognize something for

16
00:00:53,940 --> 00:00:54,900
 what it is.

17
00:00:54,900 --> 00:00:58,800
 Noting is the creation in the mind of a recognition,

18
00:00:58,800 --> 00:01:02,900
 which is why the word you choose is not so important.

19
00:01:02,900 --> 00:01:07,110
 The word itself is an act of recognizing the object as

20
00:01:07,110 --> 00:01:10,900
 close as possible to what it actually is.

