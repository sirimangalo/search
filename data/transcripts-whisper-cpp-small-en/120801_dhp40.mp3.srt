1
00:00:00,000 --> 00:00:05,320
 Okay, hello and welcome to our study of the Dhammapada.

2
00:00:05,320 --> 00:00:09,340
 Today we continue with verse number 40, which reads as

3
00:00:09,340 --> 00:00:10,640
 follows.

4
00:00:10,640 --> 00:00:18,030
 "Kumbhupamangka yamimangviditwa nagarupamang tittamidangpap

5
00:00:18,030 --> 00:00:22,560
etwa yodhaytamarangpanyaudhayna

6
00:00:22,560 --> 00:00:30,960
 chitancha arakke aniway sanosya," which means, "Kumbhupam

7
00:00:30,960 --> 00:00:33,600
angka yamimangviditwa having seen

8
00:00:33,600 --> 00:00:40,230
 or having known the body to be like a pot, nagarupamang t

9
00:00:40,230 --> 00:00:45,040
ittamidangpapetwa having established

10
00:00:45,040 --> 00:00:54,200
 or fortified this mind like a city.

11
00:00:54,200 --> 00:01:00,460
 Yodhaytamarangpanyaudhayna one should or do you, may you

12
00:01:00,460 --> 00:01:05,400
 fight with Mara using the weapon

13
00:01:05,400 --> 00:01:06,400
 of wisdom.

14
00:01:06,400 --> 00:01:27,380
 "Kumbhupamangka yamimangpapetwa yodhaytamarangpapetwa

15
00:01:27,380 --> 00:01:28,400
 having been

16
00:01:28,400 --> 00:01:47,640
 in

17
00:01:47,640 --> 00:02:11,720
 the

18
00:02:11,720 --> 00:02:40,760
 world

19
00:02:40,760 --> 00:03:03,240
 and

20
00:03:03,240 --> 00:03:29,320
 the

21
00:03:29,320 --> 00:03:42,520
 world

22
00:03:42,520 --> 00:04:11,840
 and

23
00:04:11,840 --> 00:04:31,120
 the world.

24
00:04:31,120 --> 00:04:54,000
 When

25
00:04:54,000 --> 00:05:21,920
 the world

26
00:05:21,920 --> 00:05:49,840
 is

27
00:05:49,840 --> 00:06:18,960
 in

28
00:06:18,960 --> 00:06:41,560
 the world,

29
00:06:41,560 --> 00:07:09,480
 the world is

30
00:07:09,480 --> 00:07:38,440
 in

31
00:07:41,440 --> 00:08:08,080
 the world.

32
00:08:08,080 --> 00:08:24,000
 The world is in the world.

33
00:08:24,000 --> 00:08:50,960
 The world is in the world.

34
00:08:50,960 --> 00:09:13,920
 The world is in the world.

35
00:09:13,920 --> 00:09:34,880
 The world is in the world.

36
00:09:34,880 --> 00:09:55,840
 The world is in the world.

37
00:09:55,840 --> 00:10:16,800
 The world is in the world.

38
00:10:16,800 --> 00:10:37,760
 The world is in the world.

39
00:10:37,760 --> 00:10:58,720
 The world is in the world.

40
00:10:58,720 --> 00:11:19,680
 The world is in the world.

41
00:11:19,680 --> 00:11:40,640
 The world is in the world.

42
00:11:40,640 --> 00:12:01,600
 The world is in the world.

43
00:12:01,600 --> 00:12:22,560
 The world is in the world.

44
00:12:22,560 --> 00:12:43,520
 The world is in the world.

45
00:12:43,520 --> 00:13:04,480
 The world is in the world.

46
00:13:04,480 --> 00:13:25,440
 The world is in the world.

47
00:13:25,440 --> 00:13:46,400
 The world is in the world.

48
00:13:46,400 --> 00:14:07,360
 The world is in the world.

49
00:14:07,360 --> 00:14:28,320
 The world is in the world.

50
00:14:28,320 --> 00:14:49,280
 The world is in the world.

51
00:14:49,280 --> 00:15:10,240
 The world is in the world.

52
00:15:10,240 --> 00:15:31,200
 The world is in the world.

53
00:15:31,200 --> 00:15:52,160
 The world is in the world.

54
00:15:52,160 --> 00:16:13,120
 The world is in the world.

55
00:16:13,120 --> 00:16:34,080
 The world is in the world.

56
00:16:34,080 --> 00:16:55,040
 The world is in the world.

57
00:16:55,040 --> 00:17:16,000
 The world is in the world.

58
00:17:16,000 --> 00:17:36,960
 The world is in the world.

59
00:17:36,960 --> 00:17:57,920
 The world is in the world.

60
00:17:57,920 --> 00:18:18,880
 The world is in the world.

61
00:18:18,880 --> 00:18:39,840
 The world is in the world.

62
00:18:39,840 --> 00:19:00,800
 The world is in the world.

63
00:19:00,800 --> 00:19:21,760
 The world is in the world.

64
00:19:21,760 --> 00:19:42,720
 The world is in the world.

65
00:19:42,720 --> 00:20:03,680
 The world is in the world.

66
00:20:03,680 --> 00:20:24,640
 The world is in the world.

67
00:20:24,640 --> 00:20:45,600
 The world is in the world.

68
00:20:45,600 --> 00:21:06,560
 The world is in the world.

69
00:21:06,560 --> 00:21:27,520
 The world is in the world.

70
00:21:27,520 --> 00:21:48,480
 The world is in the world.

71
00:21:48,480 --> 00:22:09,440
 The world is in the world.

72
00:22:09,440 --> 00:22:30,400
 The world is in the world.

73
00:22:30,400 --> 00:22:51,360
 The world is in the world.

74
00:22:51,360 --> 00:23:12,320
 The world is in the world.

75
00:23:12,320 --> 00:23:33,280
 The world is in the world.

76
00:23:33,280 --> 00:23:54,240
 The world is in the world.

77
00:23:54,240 --> 00:24:15,200
 The world is in the world.

78
00:24:15,200 --> 00:24:36,160
 The world is in the world.

79
00:24:36,160 --> 00:24:57,120
 The world is in the world.

80
00:24:57,120 --> 00:25:18,080
 The world is in the world.

81
00:25:18,080 --> 00:25:39,040
 The world is in the world.

82
00:25:39,040 --> 00:26:00,000
 The world is in the world.

83
00:26:00,000 --> 00:26:20,960
 The world is in the world.

84
00:26:20,960 --> 00:26:41,920
 The world is in the world.

85
00:26:41,920 --> 00:27:02,880
 The world is in the world.

86
00:27:02,880 --> 00:27:23,840
 The world is in the world.

87
00:27:23,840 --> 00:27:44,800
 The world is in the world.

88
00:27:44,800 --> 00:28:05,760
 The world is in the world.

89
00:28:05,760 --> 00:28:26,720
 The world is in the world.

90
00:28:26,720 --> 00:28:47,680
 The world is in the world.

91
00:28:47,680 --> 00:29:08,640
 The world is in the world.

92
00:29:08,640 --> 00:29:29,600
 The world is in the world.

93
00:29:29,600 --> 00:29:50,560
 The world is in the world.

94
00:29:50,560 --> 00:30:11,520
 The world is in the world.

95
00:30:11,520 --> 00:30:32,480
 The world is in the world.

96
00:30:32,480 --> 00:30:53,440
 The world is in the world.

97
00:30:53,440 --> 00:31:14,400
 The world is in the world.

98
00:31:14,400 --> 00:31:35,360
 The world is in the world.

99
00:31:35,360 --> 00:31:56,320
 The world is in the world.

100
00:31:56,320 --> 00:32:17,280
 The world is in the world.

101
00:32:17,280 --> 00:32:38,240
 The world is in the world.

102
00:32:38,240 --> 00:32:59,200
 The world is in the world.

103
00:32:59,200 --> 00:33:20,160
 The world is in the world.

104
00:33:20,160 --> 00:33:41,120
 The world is in the world.

105
00:33:41,120 --> 00:34:02,080
 The world is in the world.

106
00:34:02,080 --> 00:34:23,040
 The world is in the world.

107
00:34:23,040 --> 00:34:44,000
 The world is in the world.

108
00:34:44,000 --> 00:35:04,960
 The world is in the world.

109
00:35:04,960 --> 00:35:25,920
 The world is in the world.

110
00:35:25,920 --> 00:35:54,880
 The world is in the world.

111
00:35:54,880 --> 00:35:59,840
 The world is in the world.

