1
00:00:00,000 --> 00:00:11,750
 Good evening everyone. We're broadcasting live. How's the

2
00:00:11,750 --> 00:00:18,240
 sound? Is that too loud? Someone

3
00:00:18,240 --> 00:00:23,310
 said it was too quiet last night so I'm trying something

4
00:00:23,310 --> 00:00:29,560
 louder. Is it distorted? I can't

5
00:00:29,560 --> 00:00:31,560
 tell.

6
00:00:31,560 --> 00:00:58,850
 So tonight's quote is a really good one. It's about

7
00:00:58,850 --> 00:00:59,560
 treatment.

8
00:00:59,560 --> 00:01:09,930
 Nati ki cha pe ti tang bhi aadim nado so so ti ki cha ke.

9
00:01:09,930 --> 00:01:13,560
 You can see that's poetry.

10
00:01:13,560 --> 00:01:26,670
 Just as with a sick person who knows they are sick. Right?

11
00:01:26,670 --> 00:01:33,560
 Vijaymani. Just a second.

12
00:01:33,560 --> 00:01:41,310
 Anawijo mani is when there is a tikitjaka which is a

13
00:01:41,310 --> 00:01:45,560
 physician. Someone who treats sickness

14
00:01:45,560 --> 00:01:51,900
 from the verb tikitjati. Just as with a sick person when a

15
00:01:51,900 --> 00:01:55,560
 physician or a doctor is there.

16
00:01:55,560 --> 00:02:05,910
 Nati ki cha pe ti tang bhi aadim. But he doesn't have the

17
00:02:05,910 --> 00:02:11,560
 doctor cure him. Nado so so ti ki cha ke.

18
00:02:11,560 --> 00:02:27,920
 It is not the fault of that doctor. Ei wang even so ki lei

19
00:02:27,920 --> 00:02:28,560
 sambiadhi tukito pari pilito.

20
00:02:28,560 --> 00:02:33,210
 For one, for those who are sick with the defilements, with

21
00:02:33,210 --> 00:02:38,560
 greed, with anger, with delusion, who

22
00:02:38,560 --> 00:02:46,720
 suffer, who are feverish. Naga wei sati tang aa chariyang,

23
00:02:46,720 --> 00:02:48,560
 who don't seek out a teacher.

24
00:02:48,560 --> 00:02:56,280
 Nado so so vi nayake. It is not the fault of the one who

25
00:02:56,280 --> 00:03:00,560
 would lead them out. The vi nayaka.

26
00:03:00,560 --> 00:03:08,560
 The one who would free them from defilement.

27
00:03:08,560 --> 00:03:15,320
 This is from the jataka. I'm not going to go through the j

28
00:03:15,320 --> 00:03:18,560
ataka. In fact, it's part of a larger quote.

29
00:03:20,560 --> 00:03:28,740
 But not important. It's important here is the... This is a

30
00:03:28,740 --> 00:03:34,560
 good quote. We sometimes put too much of an

31
00:03:34,560 --> 00:03:38,780
 emphasis on a teacher. Someone came recently to see me and

32
00:03:38,780 --> 00:03:42,560
 said he was looking for a good teacher.

33
00:03:42,560 --> 00:03:44,740
 And I said to him, "Well, that's not the most important

34
00:03:44,740 --> 00:03:49,560
 thing actually. The most important is the good teaching."

35
00:03:49,560 --> 00:03:52,220
 Can you say it's important to have a good teacher? Yes, it

36
00:03:52,220 --> 00:03:55,560
's important. But it's much more important to have a

37
00:03:55,560 --> 00:03:59,560
 good teaching. The real focus should be on the teaching.

38
00:03:59,560 --> 00:04:01,560
 Because if you have a good teacher teaching,

39
00:04:01,560 --> 00:04:05,870
 you're the wrong thing. It's even more dangerous. Because

40
00:04:05,870 --> 00:04:07,560
 they're so good at teaching something wrong

41
00:04:07,560 --> 00:04:13,560
 that you go the wrong way even quicker.

42
00:04:13,560 --> 00:04:18,180
 And in the end, especially in this teaching, a teacher can

43
00:04:18,180 --> 00:04:19,560
 only go so far.

44
00:04:19,560 --> 00:04:25,190
 I get questions. People ask me questions, send me questions

45
00:04:25,190 --> 00:04:27,560
. Some questions you just can't answer.

46
00:04:27,560 --> 00:04:31,560
 Many questions. Many questions people are asking.

47
00:04:31,560 --> 00:04:36,380
 Questions how to solve their problems. Questions what does

48
00:04:36,380 --> 00:04:38,560
... Someone was saying, "What is it like to perceive

49
00:04:38,560 --> 00:04:43,150
 nirvāṇa or what is it like... No, what is the perception

50
00:04:43,150 --> 00:04:46,560
 of a sotāpana? How can you answer that?"

51
00:04:46,560 --> 00:04:50,210
 You could. You can answer to some extent. But I look at

52
00:04:50,210 --> 00:04:53,560
 these questions and think, "No, this isn't to come from a

53
00:04:53,560 --> 00:05:00,250
 teacher. The answers have to come from practice." The fact

54
00:05:00,250 --> 00:05:03,560
 that we're asking questions is sometimes a sign that

55
00:05:03,560 --> 00:05:12,220
 we're not really practicing. What this is leading to is

56
00:05:12,220 --> 00:05:15,560
 that the teacher only shows the way.

57
00:05:15,560 --> 00:05:20,460
 Akkata rota taagata. Even at the taagata, the Buddha is

58
00:05:20,460 --> 00:05:24,560
 just one who shows the way, who points the way.

59
00:05:24,560 --> 00:05:38,330
 "Tum hei hi kit chang" is up to you. The work must be done

60
00:05:38,330 --> 00:05:39,560
 by you.

61
00:05:39,560 --> 00:05:43,270
 So we have to see this in meditation. It's not going to be

62
00:05:43,270 --> 00:05:44,560
 fun. It's not going to be easy.

63
00:05:44,560 --> 00:05:48,580
 The teacher doesn't have any answers or tricks or means.

64
00:05:48,580 --> 00:05:50,560
 The best a teacher can do is encourage you.

65
00:05:50,560 --> 00:05:54,580
 And that's good. It's good to get encouragement. But in the

66
00:05:54,580 --> 00:05:56,560
 end, even that is a crutch.

67
00:05:56,560 --> 00:06:00,020
 Even that is something you have to outgrow. You're

68
00:06:00,020 --> 00:06:04,560
 completely alone in this practice. It'll make you cry.

69
00:06:04,560 --> 00:06:08,490
 You know it's good if it's bringing you to tears with

70
00:06:08,490 --> 00:06:12,560
 frustration and just being overwhelmed by the enormity of

71
00:06:12,560 --> 00:06:19,140
 the problem that you have in your mind. Blood, sweat and

72
00:06:19,140 --> 00:06:22,560
 tears. It really, it's not even a difficult thing.

73
00:06:22,560 --> 00:06:26,360
 All you're doing is walking and sitting, right? It's not

74
00:06:26,360 --> 00:06:29,560
 like that should be difficult. But that's the point.

75
00:06:29,560 --> 00:06:33,150
 It's all about you. It's about your sickness. If you're

76
00:06:33,150 --> 00:06:36,560
 sick, walking and sitting is painful.

77
00:06:36,560 --> 00:06:41,680
 When you're sick with defilement, it's equally so. You're

78
00:06:41,680 --> 00:06:42,560
 sick with greed, with anger, with delusion.

79
00:06:42,560 --> 00:06:47,780
 Just sitting still is problematic. It's difficult. It's

80
00:06:47,780 --> 00:06:48,560
 unpleasant.

81
00:07:00,560 --> 00:07:05,500
 So the doctor is there. The doctor is telling you what to

82
00:07:05,500 --> 00:07:09,560
 do if you don't do it. If you don't practice.

83
00:07:09,560 --> 00:07:15,710
 If you don't cure your sickness. "Nado soso tikichake" is

84
00:07:15,710 --> 00:07:17,560
 not the fault of that doctor.

85
00:07:17,560 --> 00:07:25,160
 That's not actually what this quote says. It says if you

86
00:07:25,160 --> 00:07:26,560
 don't seek out the teacher.

87
00:07:26,560 --> 00:07:31,130
 So it is actually putting emphasis on the teacher. I kind

88
00:07:31,130 --> 00:07:33,560
 of misinterpreted it.

89
00:07:33,560 --> 00:07:38,880
 So there's even a bigger fault would be if you didn't seek

90
00:07:38,880 --> 00:07:41,560
 out a teacher. Absolutely.

91
00:07:41,560 --> 00:07:46,310
 If the teacher is present and if you don't listen to them,

92
00:07:46,310 --> 00:07:50,560
 that's more important. You don't follow their teachings.

93
00:07:50,560 --> 00:07:54,480
 But, right, so actually what this quote says is for those

94
00:07:54,480 --> 00:07:58,560
 people who don't ever think the common minute.

95
00:07:58,560 --> 00:08:03,690
 People in the world who are suffering from stress and

96
00:08:03,690 --> 00:08:06,560
 anxiety and where do they turn for a solution?

97
00:08:06,560 --> 00:08:11,860
 They turn to pills. They turn to drugs and alcohol. They

98
00:08:11,860 --> 00:08:15,560
 turn to entertainment. They turn to diversion.

99
00:08:15,560 --> 00:08:20,420
 They turn to work or something, anything to avoid having to

100
00:08:20,420 --> 00:08:22,560
 deal with the problem.

101
00:08:22,560 --> 00:08:27,560
 They hear about meditation. They hear about the Buddha.

102
00:08:27,560 --> 00:08:30,560
 They hear about Buddhist teachers and so on.

103
00:08:30,560 --> 00:08:37,940
 They don't understand the use. They can't. Sometimes don't

104
00:08:37,940 --> 00:08:40,560
 even know that they're sick.

105
00:08:44,560 --> 00:08:47,560
 I think there's more to this than that.

106
00:08:47,560 --> 00:08:51,560
 In the context of a monastery, it makes good sense.

107
00:08:51,560 --> 00:08:54,750
 We've got monks in the monastery who never go to see the

108
00:08:54,750 --> 00:08:55,560
 teacher.

109
00:08:55,560 --> 00:09:00,150
 They never go to ask for advice. They never go to undertake

110
00:09:00,150 --> 00:09:02,560
 a meditation course.

111
00:09:02,560 --> 00:09:09,420
 I think the deeper meaning or the deeper point has to be

112
00:09:09,420 --> 00:09:13,560
 made. It's not enough to go and see the teacher.

113
00:09:13,560 --> 00:09:21,060
 It's not enough to listen to the Buddha's teaching. If you

114
00:09:21,060 --> 00:09:23,560
 don't put it into practice.

115
00:09:23,560 --> 00:09:28,740
 The corollary there is that it's going to be tough. It's

116
00:09:28,740 --> 00:09:30,560
 not something someone else can do for you.

117
00:09:30,560 --> 00:09:34,200
 The teacher can't help you much. There's not much that can

118
00:09:34,200 --> 00:09:34,560
 help you.

119
00:09:34,560 --> 00:09:40,260
 There's not much that you can use as a trick to make it

120
00:09:40,260 --> 00:09:41,560
 easier.

121
00:09:41,560 --> 00:09:45,560
 In fact, just trying to make it easier is a problem.

122
00:09:45,560 --> 00:09:52,160
 Trying to make it more pleasant, more stable, more

123
00:09:52,160 --> 00:09:55,560
 agreeable just makes it harder in the end.

124
00:09:55,560 --> 00:10:00,800
 Makes it less effective. What we're looking for is

125
00:10:00,800 --> 00:10:02,560
 something called anulomika kanti.

126
00:10:02,560 --> 00:10:11,560
 Kanti means patience, forbearance, patience really.

127
00:10:11,560 --> 00:10:18,560
 Anulomika means Loma is the grain, Anu means with the grain

128
00:10:18,560 --> 00:10:18,560
.

129
00:10:18,560 --> 00:10:27,720
 So patience that accords with reality but also with the

130
00:10:27,720 --> 00:10:28,560
 path.

131
00:10:28,560 --> 00:10:34,560
 What it means is it's in line with the truth.

132
00:10:34,560 --> 00:10:39,560
 I guess would be the best way to put it.

133
00:10:39,560 --> 00:10:42,560
 Meaning when you experience something you have patience.

134
00:10:42,560 --> 00:10:51,560
 Your patience is what brings you in line with the truth.

135
00:10:51,560 --> 00:10:55,560
 Because without that patience you have judgment.

136
00:10:55,560 --> 00:10:58,420
 Rather than seeing things as they are, you think only of

137
00:10:58,420 --> 00:11:02,560
 what they should be, what you'd like them to be.

138
00:11:02,560 --> 00:11:10,040
 The change you wish they were, wish you could enact how you

139
00:11:10,040 --> 00:11:12,560
'd rather they were.

140
00:11:12,560 --> 00:11:17,430
 So what you want, what you don't want, your urges, your

141
00:11:17,430 --> 00:11:20,560
 desires, your needs, that kind of thing.

142
00:11:20,560 --> 00:11:23,560
 Rather than just seeing things as they are.

143
00:11:23,560 --> 00:11:29,560
 So we need patience. That's the biggest one.

144
00:11:29,560 --> 00:11:35,560
 And it comes with practice. It comes with taking the

145
00:11:35,560 --> 00:11:40,560
 regimen that is given by the teacher.

146
00:11:40,560 --> 00:11:43,630
 So that's my take on the quote. And actually it's not quite

147
00:11:43,630 --> 00:11:45,560
 what the quote says. It's from the Jataka.

148
00:11:45,560 --> 00:11:49,560
 So it's one of these past life stories.

149
00:11:49,560 --> 00:11:52,470
 And there's definitely that aspect. How many people don't

150
00:11:52,470 --> 00:11:54,560
 even think to come and do a meditation course?

151
00:11:54,560 --> 00:11:59,260
 We would never think. Here we've put up, I've put up these

152
00:11:59,260 --> 00:12:01,560
 online meditation courses.

153
00:12:01,560 --> 00:12:06,560
 And they're mostly empty these days.

154
00:12:06,560 --> 00:12:09,360
 And I don't know how well they're received. I'm not giving

155
00:12:09,360 --> 00:12:12,560
 that much on them. I just give you the next exercise.

156
00:12:12,560 --> 00:12:15,210
 But it puts the onus on the meditator. You have to do the

157
00:12:15,210 --> 00:12:17,560
 work. I'm not going to give you much.

158
00:12:17,560 --> 00:12:22,590
 I'll give you the next exercise. Either you do it or you

159
00:12:22,590 --> 00:12:23,560
 don't.

160
00:12:23,560 --> 00:12:27,530
 But we don't see a lot of people these days even coming to

161
00:12:27,530 --> 00:12:28,560
 meditate.

162
00:12:28,560 --> 00:12:35,650
 Relatively speaking, it's not as hugely popular as one

163
00:12:35,650 --> 00:12:37,560
 would hope.

164
00:12:37,560 --> 00:12:41,740
 We're very good at going to the doctor for our physical

165
00:12:41,740 --> 00:12:42,560
 ailments.

166
00:12:42,560 --> 00:12:47,190
 We're not so concerned about our mental ailments. We go to

167
00:12:47,190 --> 00:12:50,560
 a physical doctor for them to fix our brains.

168
00:12:50,560 --> 00:12:53,560
 Thinking that the brain is the problem.

169
00:12:53,560 --> 00:12:58,640
 When is the brain? The brain is just a bunch of fat and

170
00:12:58,640 --> 00:13:01,560
 cells. There's nothing in the brain.

171
00:13:01,560 --> 00:13:11,310
 Just a mindless organ. Dump more chemicals in it. It just

172
00:13:11,310 --> 00:13:12,560
 mixes up.

173
00:13:12,560 --> 00:13:26,560
 It mixes up with the chemicals. It's not the answer.

174
00:13:26,560 --> 00:13:34,080
 Fewer those who are even stirred by things that are

175
00:13:34,080 --> 00:13:35,560
 stirring.

176
00:13:35,560 --> 00:13:40,190
 Who are moved by things that are moving. Who are upset by

177
00:13:40,190 --> 00:13:44,560
 things that they should be agitated by.

178
00:13:44,560 --> 00:13:49,830
 But even fewer are those people who once they've been moved

179
00:13:49,830 --> 00:13:52,560
 to action actually do act.

180
00:13:52,560 --> 00:13:58,020
 Actually do go and find a teacher and listen carefully and

181
00:13:58,020 --> 00:14:01,560
 appreciate and retain the teaching.

182
00:14:01,560 --> 00:14:06,560
 And then actually put it into practice.

183
00:14:06,560 --> 00:14:10,560
 That's where that's really curious. It's quite simple.

184
00:14:10,560 --> 00:14:15,810
 So anyway that's the quote. Some things to think about. Don

185
00:14:15,810 --> 00:14:19,560
't neglect going to see teachers.

186
00:14:19,560 --> 00:14:26,060
 But my addition is don't place too much emphasis on the

187
00:14:26,060 --> 00:14:31,560
 teacher. Do the work. That's all there is to it.

188
00:14:31,560 --> 00:14:39,010
 A lot of people just like to listen to talks and read books

189
00:14:39,010 --> 00:14:43,560
. It's very hollow. Very empty in the end.

190
00:14:43,560 --> 00:14:49,560
 It doesn't fill you up with wisdom.

191
00:14:49,560 --> 00:14:54,560
 Okay so we have questions.

192
00:14:54,560 --> 00:14:59,230
 Is there a difference between karma and superstition making

193
00:14:59,230 --> 00:15:00,560
 assumptions?

194
00:15:00,560 --> 00:15:03,390
 It seems a lot of time people can use the law of universe,

195
00:15:03,390 --> 00:15:08,290
 karma, past actions to do evils and make excuses for their

196
00:15:08,290 --> 00:15:10,560
 own actions.

197
00:15:10,560 --> 00:15:14,610
 You made me do this or you used to be like this so it's

198
00:15:14,610 --> 00:15:15,560
 your fault.

199
00:15:15,560 --> 00:15:19,560
 I'm doing my best to make your life a living hell.

200
00:15:19,560 --> 00:15:24,560
 Yeah well there's lots of misunderstandings about karma.

201
00:15:24,560 --> 00:15:24,560
 People have all sorts of ideas.

202
00:15:24,560 --> 00:15:28,560
 But you're getting on to something which is important.

203
00:15:28,560 --> 00:15:32,860
 Is it anyone who thinks they know cause and effect, the

204
00:15:32,860 --> 00:15:37,590
 nature of karma is deluding themselves unless they're a

205
00:15:37,590 --> 00:15:38,560
 Buddha.

206
00:15:38,560 --> 00:15:42,850
 It's very complicated. So no one can say this is the reason

207
00:15:42,850 --> 00:15:44,560
 why you're like this.

208
00:15:44,560 --> 00:15:49,910
 We get a general sense and that general sense holds up but

209
00:15:49,910 --> 00:15:51,560
 it's not sure.

210
00:15:51,560 --> 00:15:54,560
 We can still only guess and estimate.

211
00:15:54,560 --> 00:15:59,490
 If someone is born sick, well there's often a mental reason

212
00:15:59,490 --> 00:16:00,560
 for that.

213
00:16:00,560 --> 00:16:05,560
 The mind that came into the womb was messed up.

214
00:16:05,560 --> 00:16:15,920
 It got messed up. It messed up the fetus and not only that

215
00:16:15,920 --> 00:16:16,560
 but it was drawn to a potentially problematic situation.

216
00:16:16,560 --> 00:16:19,560
 It all came together wrong which is karma.

217
00:16:19,560 --> 00:16:25,560
 So we have this general idea that this is the way things go

218
00:16:25,560 --> 00:16:25,560
.

219
00:16:25,560 --> 00:16:29,340
 But to actually understand how it works, it's not even

220
00:16:29,340 --> 00:16:32,560
 necessary to understand how the intricacies of it go.

221
00:16:32,560 --> 00:16:36,560
 What is the cause of this? What is the cause of that?

222
00:16:36,560 --> 00:16:41,610
 What's important about karma is to see how your mind states

223
00:16:41,610 --> 00:16:47,560
 affect the next moment, affect your mind, change your mind.

224
00:16:47,560 --> 00:16:51,600
 When you get greedy, what is that like? When you're angry,

225
00:16:51,600 --> 00:16:53,560
 what's the result of these things?

226
00:16:53,560 --> 00:16:56,560
 When you're mindful, what's the result of that?

227
00:16:56,560 --> 00:17:00,580
 To understand that they change you. Our good and bad deeds

228
00:17:00,580 --> 00:17:05,680
 change us. They're habitual. They make their mark. They

229
00:17:05,680 --> 00:17:11,560
 leave their mark on us.

230
00:17:11,560 --> 00:17:18,540
 Karma is important. The general sort of folk understanding

231
00:17:18,540 --> 00:17:25,000
 of karma is useful to understand evil leads to evil, good

232
00:17:25,000 --> 00:17:26,560
 leads to good.

233
00:17:26,560 --> 00:17:30,560
 But it's very general and vague.

234
00:17:30,560 --> 00:17:34,150
 As meditators, what's important is this momentary

235
00:17:34,150 --> 00:17:38,310
 understanding when you watch your body and mind and you see

236
00:17:38,310 --> 00:17:49,620
 how they interact and how they change based on your actions

237
00:17:49,620 --> 00:17:50,560
.

238
00:17:50,560 --> 00:17:54,970
 During sitting meditation, there might occur a loss of any

239
00:17:54,970 --> 00:17:59,690
 definitively clear thoughts. There's awareness of tactile

240
00:17:59,690 --> 00:18:02,560
 sensations and various ambient sounds,

241
00:18:02,560 --> 00:18:11,740
 that a chaotic stream of extremely brief images becomes

242
00:18:11,740 --> 00:18:21,130
 necessary to arbitrarily return to the clear thoughts of

243
00:18:21,130 --> 00:18:24,560
 the rising phone.

244
00:18:24,560 --> 00:18:33,760
 It can be physical. It can be mental. You're not always

245
00:18:33,760 --> 00:18:36,560
 going to be able to be mindful clearly.

246
00:18:36,560 --> 00:18:41,320
 For our purposes, the only thing we have to concern

247
00:18:41,320 --> 00:18:46,560
 ourselves with is once we realize that it's happened,

248
00:18:46,560 --> 00:18:55,460
 when we realize that it was days there, or when we realize

249
00:18:55,460 --> 00:18:56,560
 that my mind is cloudy,

250
00:18:56,560 --> 00:19:04,560
 is to say to yourself, feeling, feeling, knowing, cloudy,

251
00:19:04,560 --> 00:19:12,560
 cloudy, even, unclear, confused.

252
00:19:12,560 --> 00:19:17,830
 That creates clarity as you do that. So don't just return

253
00:19:17,830 --> 00:19:20,560
 to the rising and falling of the abdomen.

254
00:19:20,560 --> 00:19:25,540
 Try and be aware of your mind state. In doing that, you'll

255
00:19:25,540 --> 00:19:29,560
 create clarity based on it.

256
00:19:29,560 --> 00:19:34,500
 But what's causing them? Those kind of questions, again, it

257
00:19:34,500 --> 00:19:37,560
's like trying to understand karma.

258
00:19:37,560 --> 00:19:42,560
 It is what it is, basically. From a Buddhist point of view,

259
00:19:42,560 --> 00:19:46,010
 we're not concerned about some kind of explanation of what

260
00:19:46,010 --> 00:19:46,560
 it is.

261
00:19:46,560 --> 00:19:50,240
 It's an experience. It's impermanent. It's unsatisfying. It

262
00:19:50,240 --> 00:19:55,560
's uncontrollable. That's all we need to know about it.

263
00:19:55,560 --> 00:20:01,820
 And so by cultivating this remembrance, reminding yourself

264
00:20:01,820 --> 00:20:10,560
 it is what it is, you'll get that clarity of mind.

265
00:20:10,560 --> 00:20:13,920
 Any thoughts on explaining memory since things are imper

266
00:20:13,920 --> 00:20:18,560
manent? Where and how does the memory of them remain?

267
00:20:18,560 --> 00:20:25,560
 Nothing remains. Everything arises and memory is like an

268
00:20:25,560 --> 00:20:28,560
 echo. It's a very complicated echo.

269
00:20:28,560 --> 00:20:39,560
 It's like the ripples, the changes that something makes.

270
00:20:39,560 --> 00:20:46,700
 When you apply the mind to that, there's the imprint in the

271
00:20:46,700 --> 00:20:49,560
 universe, really. It's all one thing.

272
00:20:49,560 --> 00:20:54,240
 The universe is one thing. So when something occurs, it

273
00:20:54,240 --> 00:20:56,560
 changes everything else.

274
00:20:56,560 --> 00:21:02,230
 And you can see the echo or the imprint of it. Much of this

275
00:21:02,230 --> 00:21:03,560
 goes on in the brain.

276
00:21:03,560 --> 00:21:07,280
 The brain is imprinted by experience. The brain doesn't

277
00:21:07,280 --> 00:21:09,560
 store memories. The brain is just cells.

278
00:21:09,560 --> 00:21:15,560
 It's proteins and fats and blood. That's what the brain is.

279
00:21:15,560 --> 00:21:22,240
 But it's able to store changes, an echo or an imprint of

280
00:21:22,240 --> 00:21:27,560
 the experience that the mind then accesses.

281
00:21:27,560 --> 00:21:32,170
 And it creates a new experience, which is like the old

282
00:21:32,170 --> 00:21:37,560
 experience. That's how memory occurs.

283
00:21:37,560 --> 00:21:45,840
 Basically just how you think it occurs, something special

284
00:21:45,840 --> 00:21:47,560
 about it.

285
00:21:47,560 --> 00:21:51,690
 But as far as impermanent, so if you're talking about from

286
00:21:51,690 --> 00:21:55,210
 a momentary point of view, everything is, you know, it's

287
00:21:55,210 --> 00:21:56,560
 still just cause and effect.

288
00:21:56,560 --> 00:22:02,470
 It's one thing after another. And it's complicated enough

289
00:22:02,470 --> 00:22:07,700
 to store as memories, meaning to give a new experience that

290
00:22:07,700 --> 00:22:08,560
 is like an old experience.

291
00:22:08,560 --> 00:22:12,560
 So one experience causes an experience that is like that.

292
00:22:12,560 --> 00:22:19,030
 And then also the knowledge, this is like that, which is s

293
00:22:19,030 --> 00:22:20,560
anyam.

294
00:22:20,560 --> 00:22:24,570
 But again, knowledge of how things work is not so useful.

295
00:22:24,570 --> 00:22:26,730
 What's important is that they do what they're seeing, how

296
00:22:26,730 --> 00:22:35,170
 they're seeing the way that they work, not understanding

297
00:22:35,170 --> 00:22:42,560
 why they work that way.

298
00:22:42,560 --> 00:22:47,320
 I can't wrap my head around the scientific certainty of the

299
00:22:47,320 --> 00:22:49,560
 idea of reincarnation.

300
00:22:49,560 --> 00:22:53,210
 Scientific certainty of the death of the universe, it is

301
00:22:53,210 --> 00:22:56,560
 once all the stars burn out and all energy is lost.

302
00:22:56,560 --> 00:23:03,170
 Well, all energy is not lost, but energy is just a concept.

303
00:23:03,170 --> 00:23:09,260
 It's just a word that we use to explain, well, something

304
00:23:09,260 --> 00:23:13,560
 that we're experiencing actually.

305
00:23:13,560 --> 00:23:21,560
 Some orderliness, some aspect of the nature of reality.

306
00:23:21,560 --> 00:23:24,600
 But, you know, I've talked about rebirth a lot. It's not

307
00:23:24,600 --> 00:23:27,350
 that we believe in reincarnation, it's that we don't

308
00:23:27,350 --> 00:23:28,560
 believe in death.

309
00:23:28,560 --> 00:23:34,710
 Death is just a concept. When you die, the mind and the

310
00:23:34,710 --> 00:23:41,560
 body experience doesn't cease, experience just continues.

311
00:23:41,560 --> 00:23:44,740
 There's no scientific certainty of it until you see it for

312
00:23:44,740 --> 00:23:48,260
 yourself. When you're reborn, then you'll have certainty of

313
00:23:48,260 --> 00:23:51,560
 it, I mean, until you forget it, until you lose it.

314
00:23:51,560 --> 00:23:55,790
 Because you don't have any of the echoes reminding you. It

315
00:23:55,790 --> 00:23:59,560
's very hard to remember past lives.

316
00:23:59,560 --> 00:24:05,380
 For humans anyway, because we're caught up in these cycles

317
00:24:05,380 --> 00:24:10,560
 of birth, not like an angel or something or a god.

318
00:24:10,560 --> 00:24:17,440
 So it's more smooth, the transition. For us, the new brain

319
00:24:17,440 --> 00:24:19,560
 is quite new.

320
00:24:19,560 --> 00:24:24,430
 So all the old imprints that create echoes of past lives

321
00:24:24,430 --> 00:24:25,560
 are gone.

322
00:24:25,560 --> 00:24:28,740
 It's not about certainty, it's about understanding the

323
00:24:28,740 --> 00:24:32,690
 nature, it's about arguing, it's basically logic that the

324
00:24:32,690 --> 00:24:38,640
 idea that there's death is what's uncertain, there's no

325
00:24:38,640 --> 00:24:41,560
 real good evidence for death.

326
00:24:41,560 --> 00:24:46,770
 Except, obviously it appears that the body has stopped

327
00:24:46,770 --> 00:24:52,180
 working, but from a point of view of experience, the body

328
00:24:52,180 --> 00:24:55,560
 didn't exist in the first place.

329
00:24:55,560 --> 00:25:00,400
 There's only experiences, and they continue on. The only

330
00:25:00,400 --> 00:25:04,560
 time they die is if you attain nirvana.

331
00:25:04,560 --> 00:25:18,680
 I'm glad some people are appreciating the online course. It

332
00:25:18,680 --> 00:25:22,820
's a shame not more people join up, but maybe once I get

333
00:25:22,820 --> 00:25:26,560
 back from Thailand and Asia.

334
00:25:26,560 --> 00:25:30,790
 So this Saturday we have the big Buddha celebration, and

335
00:25:30,790 --> 00:25:35,200
 then next week I'm off to Thailand and Sri Lanka, and maybe

336
00:25:35,200 --> 00:25:36,560
 Taiwan actually.

337
00:25:36,560 --> 00:25:40,460
 Looks like I may possibly take a trip to Taiwan, but

338
00:25:40,460 --> 00:25:42,560
 nothing's certain yet.

339
00:25:42,560 --> 00:25:49,060
 I'll be back July 1st I think, and then we'll start. No,

340
00:25:49,060 --> 00:25:53,560
 before July 1st, May 29th maybe.

341
00:25:54,560 --> 00:25:56,560
 Anyway.

342
00:25:56,560 --> 00:26:01,770
 So, that's all for tonight then. Thank you all for tuning

343
00:26:01,770 --> 00:26:04,560
 in. I wish you all the best.

344
00:26:04,560 --> 00:26:06,560
 Thank you.

345
00:26:07,560 --> 00:26:09,560
 Thank you.

346
00:26:10,560 --> 00:26:12,560
 Thank you.

