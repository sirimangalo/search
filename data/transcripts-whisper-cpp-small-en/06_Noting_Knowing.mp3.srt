1
00:00:00,000 --> 00:00:05,290
 Noting, knowing, question. What should one do if one cannot

2
00:00:05,290 --> 00:00:07,320
 find a name for an arisen

3
00:00:07,320 --> 00:00:14,280
 phenomenon? Answer. In most cases, noting either knowing,

4
00:00:14,280 --> 00:00:17,420
 knowing, or feeling, feeling

5
00:00:17,420 --> 00:00:21,080
 will be accurate enough to create objective awareness of

6
00:00:21,080 --> 00:00:22,880
 any phenomena that is not as

7
00:00:22,880 --> 00:00:29,240
 easily categorizable as seeing or hearing, etc.

8
00:00:29,240 --> 00:00:33,480
 Question. What should one do when a phenomena is so brief?

9
00:00:33,480 --> 00:00:35,280
 One is only aware of it after

10
00:00:35,280 --> 00:00:40,330
 it has already disappeared? Answer. At that moment there is

11
00:00:40,330 --> 00:00:42,000
 a knowledge of the disappearance

12
00:00:42,000 --> 00:00:46,450
 of the phenomena. Noting, knowing, knowing is enough to

13
00:00:46,450 --> 00:00:49,240
 prevent any uncertainty or judgment

14
00:00:49,240 --> 00:00:51,440
 about the object that has already ceased.

15
00:00:51,440 --> 00:00:52,440
 Question.

16
00:00:52,440 --> 00:00:53,440
 Question.

17
00:00:53,440 --> 00:00:53,440
 Answer.

18
00:00:53,440 --> 00:00:54,440
 Answer.

