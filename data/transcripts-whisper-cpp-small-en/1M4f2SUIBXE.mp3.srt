1
00:00:00,000 --> 00:00:09,760
 Okay, can we recollect on Anicaduka and Anata during our

2
00:00:09,760 --> 00:00:14,000
 meditation, for example, when pain appears?

3
00:00:14,000 --> 00:00:22,350
 You can, but it's not vipassana. It's in Thai they call it

4
00:00:22,350 --> 00:00:27,000
 vipassan nuk, because nuk means thinking.

5
00:00:27,000 --> 00:00:31,560
 Recollecting on impermanent suffering and non-self. You

6
00:00:31,560 --> 00:00:35,120
 actually do see some instances of this in the Tapitaka,

7
00:00:35,120 --> 00:00:39,710
 where the Buddha has you recollect of something as being

8
00:00:39,710 --> 00:00:41,000
 impermanent.

9
00:00:41,000 --> 00:00:49,150
 But I think, I don't want to say absolutely not, but the

10
00:00:49,150 --> 00:00:57,000
 general, I think we have to interpret that carefully,

11
00:00:57,000 --> 00:01:00,270
 because the observation that we get from meditation is that

12
00:01:00,270 --> 00:01:04,160
 when people do that, they don't have actual experiences of

13
00:01:04,160 --> 00:01:07,000
 impermanent suffering and non-self.

14
00:01:07,000 --> 00:01:10,700
 The truth of Anicaduka and Anata is that you see them in

15
00:01:10,700 --> 00:01:13,670
 every moment. Everything is impermanent suffering and non-

16
00:01:13,670 --> 00:01:17,000
self. They don't really exist as entities.

17
00:01:17,000 --> 00:01:21,160
 They are the result, they are the realization that comes as

18
00:01:21,160 --> 00:01:24,000
 a result of seeing things as they are.

19
00:01:24,000 --> 00:01:29,460
 And if you notice, they are all really the absence of

20
00:01:29,460 --> 00:01:34,730
 something. It's destroying the illusion of permanent

21
00:01:34,730 --> 00:01:40,000
 stability, satisfaction and controllability.

22
00:01:40,000 --> 00:01:43,260
 So when you see, for example, you watch the stomach rising,

23
00:01:43,260 --> 00:01:46,510
 at the moment of seeing rising, you're seeing impermanent

24
00:01:46,510 --> 00:01:50,250
 suffering and non-self. You're seeing something that is

25
00:01:50,250 --> 00:01:54,000
 inherently impermanent suffering and non-self.

26
00:01:54,000 --> 00:01:59,980
 And so the wisdom that we're looking for is as a result of

27
00:01:59,980 --> 00:02:06,090
 breaking away from our delusions, our ideas that these

28
00:02:06,090 --> 00:02:12,580
 things are permanent, that these things are satisfying,

29
00:02:12,580 --> 00:02:17,000
 that these things are controllable.

30
00:02:17,000 --> 00:02:20,980
 So you'll see something very simple, that the rising and

31
00:02:20,980 --> 00:02:24,930
 falling is going of its own accord, that it's changing,

32
00:02:24,930 --> 00:02:29,000
 that it's unsatisfying, that it's uncontrollable.

33
00:02:29,000 --> 00:02:33,540
 You see these things and it will jar with your expectations

34
00:02:33,540 --> 00:02:38,170
, it will jar with your intentions, your desires to make it

35
00:02:38,170 --> 00:02:42,320
 stable, your desires to make it satisfying, your desires to

36
00:02:42,320 --> 00:02:44,000
 make it controllable.

37
00:02:44,000 --> 00:02:49,540
 And it's that jarring that is the awakening that occurs,

38
00:02:49,540 --> 00:02:55,530
 the change in the mind, the realization, seeing things that

39
00:02:55,530 --> 00:02:58,000
 you never saw before.

40
00:02:58,000 --> 00:03:02,720
 It has to come by itself and it really should come from

41
00:03:02,720 --> 00:03:08,460
 intensive meditation practice, where you dedicate yourself

42
00:03:08,460 --> 00:03:13,330
 to seeing things from moment to moment as impermanent

43
00:03:13,330 --> 00:03:16,000
 suffering and non-self.

44
00:03:16,000 --> 00:03:19,970
 Basically, vipassana is the result of the meditation, it's

45
00:03:19,970 --> 00:03:22,000
 not the practice that we do.

46
00:03:22,000 --> 00:03:24,360
 So everyone is always complaining about vipassana

47
00:03:24,360 --> 00:03:27,120
 meditation, saying, "Where did the Buddha teach vipassana

48
00:03:27,120 --> 00:03:30,000
 meditation?" You can't practice vipassana.

49
00:03:30,000 --> 00:03:33,150
 And they're right, we don't call it vipassana meditation

50
00:03:33,150 --> 00:03:36,180
 because we want people to practice vipassana and we want

51
00:03:36,180 --> 00:03:38,000
 people to practice insight.

52
00:03:38,000 --> 00:03:41,520
 We call it vipassana meditation, insight meditation because

53
00:03:41,520 --> 00:03:44,000
 that's what we hope people to get from it.

54
00:03:44,000 --> 00:03:47,160
 Vipassana meditation is actually the practice of the four

55
00:03:47,160 --> 00:03:49,000
 foundations of mindfulness.

56
00:03:49,000 --> 00:03:51,790
 So if you look at the four foundations of mindfulness, the

57
00:03:51,790 --> 00:03:54,890
 satipatthana sutta, you won't see the Buddha telling you to

58
00:03:54,890 --> 00:03:58,000
 contemplate on impermanent suffering and non-self.

59
00:03:58,000 --> 00:04:02,460
 You'll see him telling you to contemplate on things as they

60
00:04:02,460 --> 00:04:04,000
 are, which is very common.

61
00:04:04,000 --> 00:04:07,760
 You hear the Buddha talk about yata bhutanyana, seeing

62
00:04:07,760 --> 00:04:13,000
 things, knowing and seeing things as they are.

63
00:04:13,000 --> 00:04:19,800
 And that is impermanent suffering and non-self, that once

64
00:04:19,800 --> 00:04:24,010
 you can see things clearly as they are, you will see imper

65
00:04:24,010 --> 00:04:26,000
manent suffering and non-self.

66
00:04:26,000 --> 00:04:28,540
 You'll see that you can't control things. You'll see that

67
00:04:28,540 --> 00:04:30,000
 things don't go according to your wishes.

68
00:04:30,000 --> 00:04:33,080
 You'll see that clinging to them causes your suffering and

69
00:04:33,080 --> 00:04:34,000
 so on and so on.

70
00:04:34,000 --> 00:04:38,340
 And you'll slowly, slowly give up and let go of the things

71
00:04:38,340 --> 00:04:40,000
 that you cling to.

72
00:04:40,000 --> 00:04:45,000
 And that is as a result of seeing them clearly as they are,

73
00:04:45,000 --> 00:04:48,000
 of seeing that they're not what you expected them to be.

74
00:04:48,000 --> 00:04:51,390
 They're not permanent. They're not satisfying. They're not

75
00:04:51,390 --> 00:04:52,000
 controllable.

76
00:04:52,000 --> 00:04:55,670
 So vipassana is the result, not the practice. Just as with

77
00:04:55,670 --> 00:04:58,000
 samanta meditation, you don't practice tranquility.

78
00:04:58,000 --> 00:05:01,530
 You practice some meditation technique in order to bring

79
00:05:01,530 --> 00:05:03,000
 about tranquility.

80
00:05:03,000 --> 00:05:05,000
 These are the two different kinds of meditation.

81
00:05:05,000 --> 00:05:09,000
 It's not people say, "How can you split them up easily?"

82
00:05:09,000 --> 00:05:11,000
 One leads to one thing, one leads to another.

83
00:05:11,000 --> 00:05:14,000
 Certain meditations don't lead to insight.

84
00:05:14,000 --> 00:05:18,590
 Certain meditations don't directly lead to tranquility in a

85
00:05:18,590 --> 00:05:20,000
 worldly sense.

86
00:05:20,000 --> 00:05:24,320
 They might only lead to the cessation experience of

87
00:05:24,320 --> 00:05:28,870
 realizing nirvana, which is of course the greatest tranqu

88
00:05:28,870 --> 00:05:30,000
ility of all.

89
00:05:30,000 --> 00:05:34,000
 So that's the separation. Does that make sense?

90
00:05:36,000 --> 00:05:39,890
 Does that answer your question? I wouldn't worry too much

91
00:05:39,890 --> 00:05:42,000
 about vipassana.

92
00:05:42,000 --> 00:05:46,000
 Worry more about satipatthana. That's the practice.

