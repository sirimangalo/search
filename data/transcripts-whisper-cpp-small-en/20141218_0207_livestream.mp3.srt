1
00:00:00,000 --> 00:00:07,000
 Okay, good morning.

2
00:00:07,000 --> 00:00:19,640
 Broadcasting live.

3
00:00:19,640 --> 00:00:26,680
 Tomorrow I'm off to the airport, near the airport.

4
00:00:26,680 --> 00:00:33,680
 I'll spend tomorrow, last day in Bangkok, and then early

5
00:00:33,680 --> 00:00:33,680
 morning Saturday to Sri Lanka.

6
00:00:33,680 --> 00:00:48,680
 I'm told that the internet is good in Sri Lanka.

7
00:00:48,680 --> 00:00:52,440
 I might be able to even do monk radio this weekend.

8
00:00:52,440 --> 00:00:59,440
 We'll see. Going to be busy.

9
00:00:59,440 --> 00:01:03,480
 Looks like there's a whole schedule set up for Sri Lanka.

10
00:01:03,480 --> 00:01:10,480
 Teaching every day. Almost every day.

11
00:01:10,480 --> 00:01:19,560
 Meditation is just like any other work that we undertake.

12
00:01:19,840 --> 00:01:26,840
 It's got a lot in common with training the body.

13
00:01:26,840 --> 00:01:34,320
 And that's how it should be seen.

14
00:01:34,320 --> 00:01:41,320
 If you want real results from meditation, lasting results.

15
00:01:41,320 --> 00:01:48,320
 Meditation, lasting results.

16
00:01:48,320 --> 00:01:57,440
 It's like the difference between getting drunk or taking

17
00:01:57,440 --> 00:01:57,440
 drugs to please the body.

18
00:01:57,440 --> 00:02:09,120
 To make you feel good.

19
00:02:09,120 --> 00:02:14,120
 Versus training the body to really make you good.

20
00:02:14,120 --> 00:02:21,120
 Make it work good.

21
00:02:21,120 --> 00:02:27,590
 Healing the body of its sicknesses and cultivating strength

22
00:02:27,590 --> 00:02:29,600
 and abilities.

23
00:02:29,600 --> 00:02:36,600
 Meditation is also a lot like other mental trainings.

24
00:02:36,600 --> 00:02:56,600
 The training to be a lawyer or an accountant.

25
00:02:58,600 --> 00:03:05,600
 Or a doctor.

26
00:03:05,600 --> 00:03:12,600
 Training in chess or training in mathematics.

27
00:03:12,600 --> 00:03:20,600
 Sciences. Engineering.

28
00:03:20,600 --> 00:03:25,600
 It's similar. It's using the same building blocks.

29
00:03:25,600 --> 00:03:30,600
 So the strategies all have to be the same.

30
00:03:30,600 --> 00:03:37,160
 And there's one strategy that stands out as sort of the key

31
00:03:37,160 --> 00:03:37,600
 to success.

32
00:03:37,600 --> 00:03:48,600
 The key to achievement, to progress.

33
00:03:49,600 --> 00:03:55,600
 It's called the path to idhi.

34
00:03:55,600 --> 00:03:58,600
 Or the things that lead to idhi.

35
00:03:58,600 --> 00:04:05,600
 Idhi meaning power or strength or accomplishment.

36
00:04:05,600 --> 00:04:11,600
 Success.

37
00:04:11,600 --> 00:04:16,970
 Things that lead to success in any sphere, not just in

38
00:04:16,970 --> 00:04:18,600
 meditation.

39
00:04:18,600 --> 00:04:22,600
 Useful for us to remember.

40
00:04:22,600 --> 00:04:26,600
 Really all you need to succeed. Four things.

41
00:04:26,600 --> 00:04:30,600
 Four general principles.

42
00:04:30,600 --> 00:04:36,600
 Called the four idhi pata.

43
00:04:36,600 --> 00:04:41,600
 If you don't have these you can't succeed. If you have them

44
00:04:41,600 --> 00:04:41,600
 succeed.

45
00:04:41,600 --> 00:04:46,600
 They are essential.

46
00:04:46,600 --> 00:04:54,600
 And in order we have chanda.

47
00:04:54,600 --> 00:05:04,600
 Which means somewhere between contentment with the activity

48
00:05:04,600 --> 00:05:08,600
 and interest in the activity.

49
00:05:08,600 --> 00:05:14,540
 Almost desire for the activity. Inclination towards the

50
00:05:14,540 --> 00:05:15,600
 activity.

51
00:05:15,600 --> 00:05:22,600
 Interest. Interest is maybe the best here.

52
00:05:22,600 --> 00:05:26,150
 Somewhere between interest and contentment. Being content

53
00:05:26,150 --> 00:05:27,600
 doing it.

54
00:05:27,600 --> 00:05:30,600
 It's not quite desire right? If you want to do something

55
00:05:30,600 --> 00:05:33,600
 that's not necessarily helpful.

56
00:05:33,600 --> 00:05:38,600
 It can actually get in your way because you're too eager.

57
00:05:38,600 --> 00:05:42,600
 It can actually be too keen.

58
00:05:42,600 --> 00:05:46,540
 But maybe that's the word. Keen. Being keen about something

59
00:05:46,540 --> 00:05:46,600
.

60
00:05:46,600 --> 00:05:50,600
 You do have to be keen.

61
00:05:50,600 --> 00:05:53,980
 It's not exactly desire but for some reason or other you're

62
00:05:53,980 --> 00:05:55,600
 keen on doing the activity.

63
00:05:55,600 --> 00:06:01,570
 In some cases it's keen because it brings you pleasure like

64
00:06:01,570 --> 00:06:06,600
 keen on eating food or keen on romance or sexuality.

65
00:06:06,600 --> 00:06:12,600
 Keen on drugs and alcohol. Keen on candy.

66
00:06:12,600 --> 00:06:16,600
 But it doesn't have to be that. You can be keen on working.

67
00:06:16,600 --> 00:06:20,550
 Keen on helping other people. Keen on cultivating goodness

68
00:06:20,550 --> 00:06:21,600
 in yourself.

69
00:06:21,600 --> 00:06:26,600
 Keen on anything.

70
00:06:26,600 --> 00:06:32,260
 But if you're not keen about something, success is most

71
00:06:32,260 --> 00:06:35,600
 generally outside of your grasp.

72
00:06:35,600 --> 00:06:40,010
 Very difficult to achieve success when you are not keen on

73
00:06:40,010 --> 00:06:41,600
 what you're doing.

74
00:06:41,600 --> 00:06:47,600
 It's possible to succeed. It's just going against the grain

75
00:06:47,600 --> 00:06:48,600
.

76
00:06:48,600 --> 00:06:53,600
 It's like the oil in your motor.

77
00:06:53,600 --> 00:07:03,600
 Without it it's hard on the system.

78
00:07:03,600 --> 00:07:06,900
 Because you don't want to do what you do. Meditation of

79
00:07:06,900 --> 00:07:07,600
 course is the worst with this.

80
00:07:07,600 --> 00:07:12,060
 If you don't want to meditate it becomes a chore and you

81
00:07:12,060 --> 00:07:13,600
 have a problem.

82
00:07:13,600 --> 00:07:18,700
 This is a problem, obviously a very common problem. It's

83
00:07:18,700 --> 00:07:20,600
 easy to get to the point especially in insight meditation

84
00:07:20,600 --> 00:07:21,600
 where you're dealing with suffering.

85
00:07:21,600 --> 00:07:26,420
 Where you don't really want to meditate anymore. You're not

86
00:07:26,420 --> 00:07:30,600
 happy. You're not comfortable doing it anymore.

87
00:07:30,600 --> 00:07:34,050
 But the neat thing about insight meditation is that that

88
00:07:34,050 --> 00:07:35,600
 can become a meditation.

89
00:07:35,600 --> 00:07:40,770
 If you're clever, if you really know what you're doing, if

90
00:07:40,770 --> 00:07:44,700
 you really understand the basic principles of insight

91
00:07:44,700 --> 00:07:45,600
 meditation,

92
00:07:45,600 --> 00:07:50,600
 then it's not a barrier because that's a meditation.

93
00:07:50,600 --> 00:07:53,890
 When you meditate on the disliking, you meditate on the

94
00:07:53,890 --> 00:08:02,600
 boredom and the frustration, the pain and the suffering.

95
00:08:02,600 --> 00:08:05,930
 When you find there's no excuse, you don't need excuses not

96
00:08:05,930 --> 00:08:16,860
 to do it. You don't have any reason for your disliking for

97
00:08:16,860 --> 00:08:19,600
 your aversion.

98
00:08:19,600 --> 00:08:27,600
 When you find this doesn't really get in the way.

99
00:08:27,600 --> 00:08:31,920
 If I'm content with it, it's very hard to want to meditate

100
00:08:31,920 --> 00:08:35,600
 after a while because it's not something you can cling to.

101
00:08:35,600 --> 00:08:41,850
 Meditation is an activity. Insight meditation is not

102
00:08:41,850 --> 00:08:45,600
 something you can cling to.

103
00:08:45,600 --> 00:08:48,540
 You can't think about meditation if you really understand

104
00:08:48,540 --> 00:08:52,290
 it in practicing. You can't think about it and say, "Boy, I

105
00:08:52,290 --> 00:08:53,600
 want to do that."

106
00:08:53,600 --> 00:09:00,040
 You can't feel attraction towards it once you understand it

107
00:09:00,040 --> 00:09:06,600
 because there's no object of attraction.

108
00:09:06,600 --> 00:09:09,780
 There's only the consideration of those things that are

109
00:09:09,780 --> 00:09:10,600
 undesirable.

110
00:09:10,600 --> 00:09:16,600
 When you think about them, your mind, of course, recoils.

111
00:09:16,600 --> 00:09:21,470
 And yet the interest comes. The more you practice, the more

112
00:09:21,470 --> 00:09:24,600
 interested you are in practicing.

113
00:09:24,600 --> 00:09:37,330
 It's not even practicing so much as returning to a state of

114
00:09:37,330 --> 00:09:47,780
 clarity of mind, cleaning away the garbage, ceasing to

115
00:09:47,780 --> 00:09:48,600
 engage in the cultivation of unwholesomeness,

116
00:09:48,600 --> 00:09:52,160
 ceasing to engage in evil and in that which causes

117
00:09:52,160 --> 00:09:54,600
 suffering for you and others.

118
00:09:54,600 --> 00:10:21,600
 [Pause]

119
00:10:21,600 --> 00:10:24,960
 So anyway, something for us to keep in mind that we need j

120
00:10:24,960 --> 00:10:25,600
andha.

121
00:10:25,600 --> 00:10:31,600
 You need to be interested and keen on what you're doing.

122
00:10:31,600 --> 00:10:34,600
 Keen on meditation. So something to be cultivated.

123
00:10:34,600 --> 00:10:39,140
 So as a sign, if you're not keen on it, don't just dismiss

124
00:10:39,140 --> 00:10:41,600
 that and keep going and plugging away anyway.

125
00:10:41,600 --> 00:10:48,940
 Don't drudge through it. You have to actually work through

126
00:10:48,940 --> 00:10:53,440
 that, the drudgery, the dissatisfaction with the meditation

127
00:10:53,440 --> 00:10:53,600
.

128
00:10:53,600 --> 00:10:57,490
 Meditate on that. It's important because you won't get

129
00:10:57,490 --> 00:11:01,700
 anywhere without being keen about it, without reading

130
00:11:01,700 --> 00:11:05,600
 yourself without aversion.

131
00:11:05,600 --> 00:11:11,790
 You'll just be going against the grain all the time. Number

132
00:11:11,790 --> 00:11:14,600
 two is effort.

133
00:11:14,600 --> 00:11:18,150
 You need effort in order to succeed. This is of course true

134
00:11:18,150 --> 00:11:22,960
 in the world. If you don't work hard, you don't get

135
00:11:22,960 --> 00:11:24,600
 anywhere.

136
00:11:24,600 --> 00:11:29,130
 In the Dhamma as well, if you don't work at it, you don't

137
00:11:29,130 --> 00:11:33,850
 get anywhere. You need the effort to rid yourself of unwh

138
00:11:33,850 --> 00:11:35,600
olesome states.

139
00:11:35,600 --> 00:11:39,620
 You need the effort to prevent wholesome states from

140
00:11:39,620 --> 00:11:40,600
 arising.

141
00:11:40,600 --> 00:11:46,620
 The effort to cultivate wholesome states and the effort to

142
00:11:46,620 --> 00:11:49,930
 maintain and protect wholesome states that have already

143
00:11:49,930 --> 00:11:50,600
 risen.

144
00:11:50,600 --> 00:11:58,600
 These are the four right efforts in Buddhism.

145
00:11:58,600 --> 00:12:04,490
 In a way, it's kind of a different sort of effort from what

146
00:12:04,490 --> 00:12:08,600
 we would normally understand by the term.

147
00:12:08,600 --> 00:12:11,210
 We think effort is just pushing harder and harder. That's

148
00:12:11,210 --> 00:12:13,600
 not really what it means.

149
00:12:13,600 --> 00:12:18,850
 It means working, certainly, but working in a specific way,

150
00:12:18,850 --> 00:12:26,430
 working to maintain wholesomeness and stay away from unwh

151
00:12:26,430 --> 00:12:30,600
olesome states.

152
00:12:30,600 --> 00:12:33,690
 That's all. It doesn't mean pushing hard. It doesn't mean

153
00:12:33,690 --> 00:12:36,600
 meditating hours and hours and hours.

154
00:12:36,600 --> 00:12:40,120
 Of course, meditating hours and hours and hours can be a

155
00:12:40,120 --> 00:12:43,600
 good thing, usually a good thing if you're doing it.

156
00:12:43,600 --> 00:12:48,600
 If it's done properly, it's a wonderful thing.

157
00:12:48,600 --> 00:12:51,030
 But it has to be accompanied by right effort, not wrong

158
00:12:51,030 --> 00:12:55,460
 effort. You can't just push and push and push yourself. You

159
00:12:55,460 --> 00:12:59,600
'll go crazy.

160
00:12:59,600 --> 00:13:10,230
 Right effort is a specific type of effort. The effort to be

161
00:13:10,230 --> 00:13:18,600
 pure of mind, effort taken to be clear in mind.

162
00:13:18,600 --> 00:13:23,110
 And you need that. If you don't have effort, you won't

163
00:13:23,110 --> 00:13:27,930
 really be mindful, really meditate. You can walk and sit

164
00:13:27,930 --> 00:13:31,600
 for hours and hours and it won't do you any good.

165
00:13:31,600 --> 00:13:34,600
 You don't need to put up effort.

166
00:13:34,600 --> 00:13:37,600
 Of course, you just need effort just to actually meditate.

167
00:13:37,600 --> 00:13:40,600
 It takes effort to get up early in the morning.

168
00:13:40,600 --> 00:13:44,340
 It takes effort to sit up straight. It takes effort to walk

169
00:13:44,340 --> 00:13:48,600
 back and forth without stopping. It does take effort.

170
00:13:48,600 --> 00:13:56,600
 If you don't have it, that's going to be a hindrance.

171
00:13:56,600 --> 00:14:01,930
 So as with anything, this is part of our success, how to

172
00:14:01,930 --> 00:14:03,600
 find success.

173
00:14:03,600 --> 00:14:09,620
 The third intibada is called "kitta". "Kitta" means "mind".

174
00:14:09,620 --> 00:14:12,370
 So it's actually a curious usage of the word "mind", but it

175
00:14:12,370 --> 00:14:15,600
 means keeping in mind.

176
00:14:15,600 --> 00:14:21,660
 If you want to succeed at anything, the third quality that

177
00:14:21,660 --> 00:14:25,600
 you need is to keep it in mind.

178
00:14:25,600 --> 00:14:32,450
 To not lose sight of the goal, to not lose sight of the

179
00:14:32,450 --> 00:14:33,600
 path.

180
00:14:33,600 --> 00:14:37,600
 To not forget your mission.

181
00:14:37,600 --> 00:14:41,460
 To not forget your goals, to not lose track of them, lose

182
00:14:41,460 --> 00:14:42,600
 sight of them.

183
00:14:42,600 --> 00:14:50,110
 To not get sidetracked by other things, not get distracted

184
00:14:50,110 --> 00:14:52,600
 by other things.

185
00:14:52,600 --> 00:14:57,910
 And this is of course nowhere more important than in

186
00:14:57,910 --> 00:15:02,600
 meditation, where the object is the mind itself.

187
00:15:02,600 --> 00:15:10,600
 The object is reality. It's the practice of the mind.

188
00:15:10,600 --> 00:15:19,530
 It involves the mind. So keeping the mind on the task is

189
00:15:19,530 --> 00:15:22,600
 the whole thing.

190
00:15:22,600 --> 00:15:24,850
 Keeping it in mind takes on a whole new meaning in

191
00:15:24,850 --> 00:15:28,600
 meditation, because that's the essence of the meditation.

192
00:15:28,600 --> 00:15:33,670
 When your mind isn't on the meditation, you can't progress

193
00:15:33,670 --> 00:15:35,600
 at all whatsoever.

194
00:15:35,600 --> 00:15:40,230
 Perhaps the most crucial of the four, because it is the

195
00:15:40,230 --> 00:15:41,600
 practice itself.

196
00:15:41,600 --> 00:15:45,950
 The practice is keeping your mind on it. In the world, the

197
00:15:45,950 --> 00:15:48,600
 affairs take your mind off of it.

198
00:15:48,600 --> 00:15:52,190
 Sometimes it's fine because it's running by itself. Your

199
00:15:52,190 --> 00:15:55,600
 business will continue.

200
00:15:55,600 --> 00:16:01,000
 Even your physical activities, you can be lifting weights

201
00:16:01,000 --> 00:16:03,600
 and let your mind wander.

202
00:16:03,600 --> 00:16:07,600
 You just have to keep your mind on it when it's necessary.

203
00:16:07,600 --> 00:16:13,630
 Meditation isn't like that. Meditation obviously requires

204
00:16:13,630 --> 00:16:17,600
 this to a much greater degree.

205
00:16:17,600 --> 00:16:21,680
 But it still involves keeping your mind on the fact that it

206
00:16:21,680 --> 00:16:23,600
's time to do sitting meditation,

207
00:16:23,600 --> 00:16:35,600
 time to do walking and sitting, time to...

208
00:16:35,600 --> 00:16:38,700
 To be mindful in all your activities, when you're eating,

209
00:16:38,700 --> 00:16:40,600
 reminding yourself that you're remembering that

210
00:16:40,600 --> 00:16:44,110
 you should be mindful when you eat, chewing, chewing,

211
00:16:44,110 --> 00:16:46,600
 swallowing, tasting.

212
00:16:46,600 --> 00:16:48,600
 When you're showering, when you're washing your clothes,

213
00:16:48,600 --> 00:16:50,600
 whatever you're doing.

214
00:16:50,600 --> 00:16:54,340
 The ability to remember, to remind yourself, "Ah, I should

215
00:16:54,340 --> 00:16:55,600
 be mindful of this."

216
00:16:55,600 --> 00:16:59,600
 Remind yourself and keep yourself in the present moment.

217
00:16:59,600 --> 00:17:05,570
 This is what is meant by "chitta". Without this, meditation

218
00:17:05,570 --> 00:17:10,600
 will never succeed 100%.

219
00:17:10,600 --> 00:17:19,870
 Number three. Number four is called "vimangsa". "Vi" means

220
00:17:19,870 --> 00:17:27,600
 "out" or "divided".

221
00:17:27,600 --> 00:17:34,600
 "Out" is better. "Mangsa".

222
00:17:34,600 --> 00:17:41,600
 I'm not even sure where that comes from. "Vimangsa" means "

223
00:17:41,600 --> 00:17:41,600
the ability to discriminate".

224
00:17:41,600 --> 00:17:44,600
 It's another word for "wisdom".

225
00:17:44,600 --> 00:17:48,600
 But discrimination is what is used.

226
00:17:48,600 --> 00:17:51,600
 That's the aspect of wisdom that's important.

227
00:17:51,600 --> 00:17:56,120
 It's the ability not just to keep your mind on something,

228
00:17:56,120 --> 00:18:01,600
 but to be clever and figure out ways

229
00:18:01,600 --> 00:18:12,600
 and refine your attention, refine and adjust your activity.

230
00:18:12,600 --> 00:18:17,840
 So obviously quite important in all activities where

231
00:18:17,840 --> 00:18:19,600
 success is the goal,

232
00:18:19,600 --> 00:18:24,600
 where we're trying to succeed at something.

233
00:18:24,600 --> 00:18:28,600
 It's not just enough to... Well, it's true in some spheres.

234
00:18:28,600 --> 00:18:32,600
 It might just be enough to plod along, but in most cases

235
00:18:32,600 --> 00:18:35,600
 you need some.

236
00:18:35,600 --> 00:18:39,130
 It wouldn't be enough just to have the first three without

237
00:18:39,130 --> 00:18:40,600
 having this wisdom

238
00:18:40,600 --> 00:18:46,150
 to be able to adjust and discriminate and refine your

239
00:18:46,150 --> 00:18:47,600
 activity.

240
00:18:47,600 --> 00:18:52,470
 So at work, finding better ways to do things, making the

241
00:18:52,470 --> 00:18:55,600
 right decisions in business,

242
00:18:55,600 --> 00:19:03,600
 in family, in society, in school, being clever at studying,

243
00:19:03,600 --> 00:19:08,830
 being clever at choosing the right topics and having the

244
00:19:08,830 --> 00:19:13,600
 right tools.

245
00:19:13,600 --> 00:19:18,600
 In general, just being able to be efficient,

246
00:19:18,600 --> 00:19:23,390
 having the wisdom to see when you're being inefficient and

247
00:19:23,390 --> 00:19:24,600
 so on,

248
00:19:24,600 --> 00:19:32,600
 to see when you're actually on the wrong path

249
00:19:32,600 --> 00:19:38,010
 or practicing incorrectly, working incorrectly, working

250
00:19:38,010 --> 00:19:38,600
 improperly.

251
00:19:38,600 --> 00:19:41,600
 To see a better way.

252
00:19:41,600 --> 00:19:44,260
 To have wisdom, to not just plod along, but to do it with

253
00:19:44,260 --> 00:19:48,600
 wisdom so you do it right.

254
00:19:48,600 --> 00:19:55,600
 You're able to adjust, you're flexible, it takes wisdom.

255
00:19:55,600 --> 00:19:59,970
 In meditation this is also obviously a very important

256
00:19:59,970 --> 00:20:03,600
 aspect of the practice of the path.

257
00:20:03,600 --> 00:20:07,770
 That you can't just do walking and sitting, walking and

258
00:20:07,770 --> 00:20:08,600
 sitting.

259
00:20:08,600 --> 00:20:11,600
 You think you're going to become enlightened.

260
00:20:11,600 --> 00:20:19,230
 You need to be clear about the object. Your mind has to be

261
00:20:19,230 --> 00:20:19,600
 clear.

262
00:20:19,600 --> 00:20:26,250
 Your mind has to be on the right path, in the right way, in

263
00:20:26,250 --> 00:20:31,600
 the right place.

264
00:20:31,600 --> 00:20:35,290
 So when the stomach is rising, your mind has to be aware of

265
00:20:35,290 --> 00:20:36,600
 the stomach rising.

266
00:20:36,600 --> 00:20:45,590
 It shouldn't really need to be said, but your mind has to

267
00:20:45,590 --> 00:20:47,600
 be there.

268
00:20:47,600 --> 00:20:51,600
 That's jita. Your mind has to be there,

269
00:20:51,600 --> 00:20:56,600
 but you have to be clear about the nature of your practice.

270
00:20:56,600 --> 00:21:03,600
 You have to be able to adjust your practice.

271
00:21:03,600 --> 00:21:06,600
 You have to be able to see when you're doing things right

272
00:21:06,600 --> 00:21:07,600
 and doing things wrong.

273
00:21:07,600 --> 00:21:11,600
 You have to be able to.

274
00:21:11,600 --> 00:21:14,950
 For example, when you're walking and you find yourself very

275
00:21:14,950 --> 00:21:15,600
 distracted,

276
00:21:15,600 --> 00:21:17,600
 you have to be discriminated.

277
00:21:17,600 --> 00:21:20,100
 You have to be able to see that walking isn't helping you

278
00:21:20,100 --> 00:21:20,600
 right now.

279
00:21:20,600 --> 00:21:23,600
 So you sit down.

280
00:21:23,600 --> 00:21:26,600
 If you're sitting down and you feel very tired,

281
00:21:26,600 --> 00:21:34,600
 you're browsing off, then you have to discriminate.

282
00:21:34,600 --> 00:21:42,600
 You have to be clever and decide to walk instead.

283
00:21:42,600 --> 00:21:47,600
 You have to find ways like Ananda when he was...

284
00:21:47,600 --> 00:21:51,600
 When he became enlightened, he was doing walking all night,

285
00:21:51,600 --> 00:21:55,600
 and he didn't get anywhere.

286
00:21:55,600 --> 00:21:59,600
 At the last moment, he decided,

287
00:21:59,600 --> 00:22:03,240
 he realized that he'd probably better lie down because he

288
00:22:03,240 --> 00:22:04,600
 had too much effort.

289
00:22:04,600 --> 00:22:08,600
 So he lay down, and right away he became enlightened.

290
00:22:08,600 --> 00:22:10,600
 This kind of thing. It actually can happen.

291
00:22:10,600 --> 00:22:15,600
 It's quite possible that you just adjust something.

292
00:22:15,600 --> 00:22:18,600
 You just have the wisdom to adjust your practice,

293
00:22:18,600 --> 00:22:21,600
 to be able to catch the things that you're not noting.

294
00:22:21,600 --> 00:22:25,600
 You realize that when you're struggling to have the wisdom

295
00:22:25,600 --> 00:22:29,600
 to catch what it is that's causing you problems,

296
00:22:29,600 --> 00:22:32,600
 to catch what it is that's holding you back,

297
00:22:32,600 --> 00:22:36,600
 you can't just force yourself to practice properly.

298
00:22:36,600 --> 00:22:38,600
 You can't force the practice to succeed.

299
00:22:38,600 --> 00:22:41,600
 It's not like any other work in that way.

300
00:22:41,600 --> 00:22:43,600
 You need wisdom.

301
00:22:43,600 --> 00:22:47,600
 You have to find the way through wisdom.

302
00:22:47,600 --> 00:22:55,600
 Panyaya parisujjati. You become pure through wisdom.

303
00:22:55,600 --> 00:22:59,600
 I mean to discriminate is very important.

304
00:22:59,600 --> 00:23:03,600
 Not just walk sit, walk sit, walk sit for hours an hour.

305
00:23:03,600 --> 00:23:08,070
 You can do it for lifetimes and not get anywhere if you're

306
00:23:08,070 --> 00:23:09,600
 not doing it properly.

307
00:23:09,600 --> 00:23:13,600
 Your mind isn't in the right place and you're not adjusting

308
00:23:13,600 --> 00:23:13,600
,

309
00:23:13,600 --> 00:23:18,600
 constantly adjusting your practice, constantly flexible.

310
00:23:18,600 --> 00:23:24,600
 This is most apparent in regards to the layers of practice

311
00:23:24,600 --> 00:23:28,600
 and the types of experience in the way that a meditator

312
00:23:28,600 --> 00:23:29,600
 figures out,

313
00:23:29,600 --> 00:23:33,220
 in a sense, figures out an experience or figures out an eng

314
00:23:33,220 --> 00:23:34,600
ram,

315
00:23:34,600 --> 00:23:41,520
 a type of mental activity, figures out how to be mindful of

316
00:23:41,520 --> 00:23:42,600
 something.

317
00:23:42,600 --> 00:23:45,830
 And then they stick to that and they try to do that again

318
00:23:45,830 --> 00:23:46,600
 and again and again.

319
00:23:46,600 --> 00:23:50,970
 And that's not how meditation works because every moment is

320
00:23:50,970 --> 00:23:52,600
 going to be different.

321
00:23:52,600 --> 00:23:56,600
 You're always changing. Even your changing is change.

322
00:23:56,600 --> 00:24:00,600
 Even your changing is change. You have to remember that.

323
00:24:00,600 --> 00:24:04,600
 It's never going to be predictable.

324
00:24:04,600 --> 00:24:07,600
 That's the truth of impermanence.

325
00:24:07,600 --> 00:24:11,000
 Not just that it comes again and again and again, but that

326
00:24:11,000 --> 00:24:12,600
 it's always changing.

327
00:24:12,600 --> 00:24:16,600
 And if you're just trying to use the old ways,

328
00:24:16,600 --> 00:24:21,600
 without having wimangsa, without being able to break apart

329
00:24:21,600 --> 00:24:25,320
 and see the truth behind the current experience, understand

330
00:24:25,320 --> 00:24:26,600
 the current experience,

331
00:24:26,600 --> 00:24:33,230
 you have a very hard time doing the same thing over and

332
00:24:33,230 --> 00:24:33,600
 over,

333
00:24:33,600 --> 00:24:40,590
 over-impanition and ability to understand the current

334
00:24:40,590 --> 00:24:42,600
 experience,

335
00:24:42,600 --> 00:24:44,600
 in all its uniqueness.

336
00:24:44,600 --> 00:24:46,600
 So there you go. Those are the four idhipada.

337
00:24:46,600 --> 00:24:50,300
 They're very useful, something for us to think about during

338
00:24:50,300 --> 00:24:51,600
 our practice.

339
00:24:51,600 --> 00:24:58,600
 Keep in mind, so that our practice may succeed.

340
00:24:58,600 --> 00:25:01,600
 Thank you all for tuning in and may all of your practice,

341
00:25:01,600 --> 00:25:05,600
 everyone's practice succeed. Be well.

