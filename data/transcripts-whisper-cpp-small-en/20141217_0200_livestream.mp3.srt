1
00:00:00,000 --> 00:00:14,640
 Okay, good morning. I'm broadcasting here from Bangkok. And

2
00:00:14,640 --> 00:00:20,880
 I have Christine here. Christine

3
00:00:20,880 --> 00:00:28,640
 is here listening. So I have a guest. Looks like we got a

4
00:00:28,640 --> 00:00:32,760
 few people listening. Good morning,

5
00:00:32,760 --> 00:00:46,700
 everyone. Good evening. We were talking about something. I

6
00:00:46,700 --> 00:00:48,480
 was sort of debating something

7
00:00:48,480 --> 00:00:54,830
 with a Thai student of mine here, one of the owners of this

8
00:00:54,830 --> 00:01:02,280
 land that I'm staying in. We

9
00:01:02,280 --> 00:01:19,390
 were talking about an enlightenment and the Dhamma and Nib

10
00:01:19,390 --> 00:01:24,320
ana. I thought it was interesting,

11
00:01:24,320 --> 00:01:31,830
 something worth sharing. Because it's a bigger issue than

12
00:01:31,830 --> 00:01:36,520
 that. I had a similar debate or even

13
00:01:36,520 --> 00:01:48,140
 argument with another friend of mine who was from India

14
00:01:48,140 --> 00:01:54,520
 claiming that one of his teachers was most

15
00:01:54,520 --> 00:02:02,390
 definitely an Arahant. One of his Hindu teachers was most

16
00:02:02,390 --> 00:02:07,880
 definitely an Arahant. And so this student

17
00:02:07,880 --> 00:02:16,240
 of mine here, supporter of mine here, was talking about a

18
00:02:16,240 --> 00:02:20,640
 monk who I've heard some things about that

19
00:02:20,640 --> 00:02:28,100
 didn't make me very impressed with. But it seems she's of

20
00:02:28,100 --> 00:02:34,160
 the impression that he's on the right track

21
00:02:34,160 --> 00:02:39,530
 or he was passed away now. And we got in talking to about

22
00:02:39,530 --> 00:02:42,680
 other monks in Thailand who have claimed

23
00:02:42,680 --> 00:02:46,210
 to be Arahants and how some of that and I was saying how

24
00:02:46,210 --> 00:02:48,560
 even some of them seem to have views

25
00:02:48,560 --> 00:02:56,050
 that were suspicious. Of course we don't know exactly what

26
00:02:56,050 --> 00:03:00,720
 sort of views another person holds.

27
00:03:00,720 --> 00:03:04,240
 We can only go by their words which can be taken out of

28
00:03:04,240 --> 00:03:05,560
 context and can be

29
00:03:05,560 --> 00:03:17,460
 meant in a way different than we actually saw them. We

30
00:03:17,460 --> 00:03:23,480
 understood them. But we have to be able to

31
00:03:23,480 --> 00:03:30,370
 separate out goodness and enlightenment. I think that's

32
00:03:30,370 --> 00:03:33,640
 really the point that makes me want to

33
00:03:33,640 --> 00:03:38,130
 bring it up here today. The difference between goodness and

34
00:03:38,130 --> 00:03:45,320
 enlightenment. They're not the same

35
00:03:45,320 --> 00:03:49,660
 thing. And so by goodness it's just a word I'm trying to

36
00:03:49,660 --> 00:03:58,160
 use in a general sense to refer to good

37
00:03:58,160 --> 00:04:02,490
 qualities of mind. So when a person has wisdom we consider

38
00:04:02,490 --> 00:04:05,200
 this to be goodness. When a person is

39
00:04:05,200 --> 00:04:13,420
 generous we consider this to be good. Because this is

40
00:04:13,420 --> 00:04:22,120
 different from enlightenment. We have

41
00:04:22,120 --> 00:04:29,660
 to understand that enlightenment and nibbana are really

42
00:04:29,660 --> 00:04:41,680
 beyond that. And we risk the mistake of

43
00:04:45,680 --> 00:04:52,180
 thinking that we understand what it means to be enlightened

44
00:04:52,180 --> 00:04:54,760
. Unless you're enlightened already

45
00:04:54,760 --> 00:04:59,630
 it's not something that can be understood fully. Not even

46
00:04:59,630 --> 00:05:05,960
 really and generally. Something that you

47
00:05:05,960 --> 00:05:08,200
 might even say has to be taken on faith but that's not

48
00:05:08,200 --> 00:05:10,080
 really a good word because it shouldn't be your

49
00:05:10,080 --> 00:05:13,270
 focus anyway. Enlightenment shouldn't be our focus and that

50
00:05:13,270 --> 00:05:15,400
's the problem. When you say this person

51
00:05:15,400 --> 00:05:17,560
 was an arahant if you're not an arahant that's not

52
00:05:17,560 --> 00:05:19,640
 something you should say because how would you

53
00:05:19,640 --> 00:05:23,850
 know? What does that mean to you? If you're not an arahant

54
00:05:23,850 --> 00:05:26,080
 it's not like I know this person's a

55
00:05:26,080 --> 00:05:28,780
 doctor even though I'm not a doctor. I don't have to be a

56
00:05:28,780 --> 00:05:31,280
 doctor to know a person's a doctor. It's

57
00:05:31,280 --> 00:05:36,080
 not like that. Being an arahant is having a state of mind

58
00:05:36,080 --> 00:05:39,800
 that is unfathomable too. It's because it's

59
00:05:39,800 --> 00:05:46,720
 to do with the rejection of ignorance. An arahant has done

60
00:05:46,720 --> 00:05:49,840
 away with the clouds that surround our

61
00:05:49,840 --> 00:05:54,270
 minds. An ordinary person's mind is clouded. So for that

62
00:05:54,270 --> 00:05:57,480
 ordinary person to then talk about someone

63
00:05:57,480 --> 00:06:04,930
 who has realized the mistakes, who has overcome the faults

64
00:06:04,930 --> 00:06:09,400
 of one's own faults, it's unfathomable.

65
00:06:10,520 --> 00:06:14,400
 You see? The reason that we aren't enlightened is because

66
00:06:14,400 --> 00:06:17,320
 we're all mixed up in our mind. To

67
00:06:17,320 --> 00:06:22,090
 even have any grasp whatsoever of a person who's given that

68
00:06:22,090 --> 00:06:25,040
 up is not possible. So that's the

69
00:06:25,040 --> 00:06:31,260
 danger here. When a teacher is teaching good things that's

70
00:06:31,260 --> 00:06:35,400
 great. That's worth. We should all

71
00:06:35,400 --> 00:06:40,660
 rejoice in that because even unenlightened beings can teach

72
00:06:40,660 --> 00:06:43,000
 and can recognize good things,

73
00:06:43,000 --> 00:06:48,030
 can recognize goodness. That's the point. You can recognize

74
00:06:48,030 --> 00:06:50,880
 goodness. That's all we can do is we can

75
00:06:50,880 --> 00:06:53,580
 recognize this person has goodness, that person has

76
00:06:53,580 --> 00:06:58,960
 goodness. What that person says is good. If

77
00:06:58,960 --> 00:07:02,530
 you're an average person you can generally tell the

78
00:07:02,530 --> 00:07:05,800
 difference between good and evil. Many of us

79
00:07:05,800 --> 00:07:11,540
 are unable to give up evil and even see the fault in the

80
00:07:11,540 --> 00:07:16,000
 see clearly the fault in evil, but we can

81
00:07:16,000 --> 00:07:19,840
 generally tell the difference even though we're often vague

82
00:07:19,840 --> 00:07:23,880
 on why evil is such a bad thing until

83
00:07:23,880 --> 00:07:32,010
 you become enlightened. But it's comprehendable. We can

84
00:07:32,010 --> 00:07:39,760
 understand goodness. Enlightenment is a

85
00:07:39,760 --> 00:07:47,280
 whole other thing. So when someone says that this person is

86
00:07:47,280 --> 00:07:50,400
 really think this person was a fully

87
00:07:50,400 --> 00:07:55,200
 enlightened being like it actually this same topic came up

88
00:07:55,200 --> 00:07:58,240
 in regards to Christ and apparently

89
00:07:58,240 --> 00:08:00,710
 there's on the internet there's a quote that has the Dalai

90
00:08:00,710 --> 00:08:04,280
 Lama saying that Christ was a fully

91
00:08:04,280 --> 00:08:11,530
 enlightened being. And I don't know if it was taken out of

92
00:08:11,530 --> 00:08:16,360
 context or if it was misquoted or whatnot.

93
00:08:16,360 --> 00:08:23,010
 But all of this you know when someone says such and such a

94
00:08:23,010 --> 00:08:31,280
 person was an Arahant or in the case of

95
00:08:31,280 --> 00:08:34,880
 this supporter here in Thailand she was talking about how

96
00:08:34,880 --> 00:08:37,000
 this because I brought up I said well

97
00:08:37,000 --> 00:08:40,380
 this teacher he isn't this the same guy who claims to bring

98
00:08:40,380 --> 00:08:43,320
 people on tours of Nibbana like he takes

99
00:08:43,320 --> 00:08:46,730
 his students to go and see Nibbana. Actually they with

100
00:08:46,730 --> 00:08:49,120
 their minds they go in and look and see what

101
00:08:49,120 --> 00:08:54,500
 it's like. And so she was she was coaching it and saying

102
00:08:54,500 --> 00:08:57,560
 you know you have to listen to it in

103
00:08:57,560 --> 00:09:03,270
 context maybe and she said I haven't heard him say that but

104
00:09:03,270 --> 00:09:08,760
 it's a bit suspicious. But the point being

105
00:09:08,760 --> 00:09:18,630
 anyone who talks about these things we have to call into

106
00:09:18,630 --> 00:09:25,480
 question their ability to I mean now I

107
00:09:25,480 --> 00:09:29,930
 guess that's not quite fair but often I guess all I can say

108
00:09:29,930 --> 00:09:32,760
 is often what it does is question this

109
00:09:32,760 --> 00:09:37,380
 own this person who's speaking question their own question

110
00:09:37,380 --> 00:09:40,720
 calls into question their level of

111
00:09:40,720 --> 00:09:54,050
 realization. Anyway the point is to understand the

112
00:09:54,050 --> 00:10:07,120
 difference and to careful be to understand

113
00:10:07,120 --> 00:10:12,750
 the depth and the profundity of what we're talking about.

114
00:10:12,750 --> 00:10:18,880
 Being enlightened doesn't just mean being

115
00:10:18,880 --> 00:10:25,720
 a good person or a good speaker. It isn't even just about

116
00:10:25,720 --> 00:10:29,880
 wisdom it's about a specific type of

117
00:10:29,880 --> 00:10:40,110
 wisdom that shakes the foundation of reality. Makes a crack

118
00:10:40,110 --> 00:10:48,760
 in samsara, a crack in the shell

119
00:10:48,760 --> 00:10:55,200
 that keeps us in this egg we call samsara. This is how the

120
00:10:55,200 --> 00:10:59,560
 Buddha put it he said it's like he was

121
00:10:59,560 --> 00:11:03,080
 the first one to crack through this shell that's what makes

122
00:11:03,080 --> 00:11:05,200
 him different that's all that makes him

123
00:11:05,200 --> 00:11:09,010
 different he was just the first one everybody else just

124
00:11:09,010 --> 00:11:11,720
 came after him but it was like cracking out

125
00:11:11,720 --> 00:11:22,840
 of a shell once you when you see nirvana when you see nir

126
00:11:22,840 --> 00:11:33,720
vana it's it's irreversible it's nothing to

127
00:11:33,720 --> 00:11:38,900
 do with the shell with the egg anymore so goodness is not

128
00:11:38,900 --> 00:11:45,240
 any anything in samsara is not nirvana now

129
00:11:45,240 --> 00:11:54,910
 the it's true that it's definitely the case that the

130
00:11:54,910 --> 00:12:04,160
 realization of nirvana and enlightenment indeed

131
00:12:04,160 --> 00:12:08,150
 does may lead to goodness there is definitely a

132
00:12:08,150 --> 00:12:13,640
 relationship but the two are not equal and so we

133
00:12:13,640 --> 00:12:17,570
 not being able to tell the level of another person's

134
00:12:17,570 --> 00:12:21,520
 enlightenment it's easy to too easy

135
00:12:21,520 --> 00:12:28,020
 to jump to conclusions and to suggest that someone is

136
00:12:28,020 --> 00:12:32,000
 enlightened because of the manifestations

137
00:12:32,000 --> 00:12:39,080
 because you see them as such a good person I'm not the

138
00:12:39,080 --> 00:12:43,400
 point is not to criticize but neither

139
00:12:43,400 --> 00:12:47,700
 but the point is neither should we jump to conclusions

140
00:12:47,700 --> 00:12:50,360
 about enlightenment or nirvana

141
00:12:50,360 --> 00:13:05,250
 and so the question always comes up about how do you find

142
00:13:05,250 --> 00:13:07,200
 then an enlightened teacher if you can't

143
00:13:07,200 --> 00:13:11,810
 discern for yourself whether a person is enlightened and I

144
00:13:11,810 --> 00:13:17,400
 reassure you that it's you really cannot there

145
00:13:17,400 --> 00:13:23,680
 is very little hope to be able to tell whether a person is

146
00:13:23,680 --> 00:13:26,120
 enlightened unless you're enlightened

147
00:13:26,120 --> 00:13:28,440
 yourself right because you don't really understand what it

148
00:13:28,440 --> 00:13:28,720
 means

149
00:13:40,000 --> 00:13:44,150
 when people want to know how do I how do I find a teacher

150
00:13:44,150 --> 00:13:47,600
 and so this was the question that was

151
00:13:47,600 --> 00:13:50,260
 posed even in the time of the Buddha and this is where this

152
00:13:50,260 --> 00:13:54,960
 great kalamasuta came from it all comes

153
00:13:54,960 --> 00:14:03,740
 down to really step-by-step step-by-step realization what

154
00:14:03,740 --> 00:14:08,160
 you know for yourself the Buddha said what

155
00:14:08,160 --> 00:14:11,430
 you know for yourself to be right and what you know for

156
00:14:11,430 --> 00:14:13,880
 yourself to be wrong what you can see

157
00:14:13,880 --> 00:14:20,630
 clearly as right and wrong and that may not be anything

158
00:14:20,630 --> 00:14:24,880
 terribly profound but as I said you can

159
00:14:24,880 --> 00:14:28,290
 see for example we can see what is good and what is bad and

160
00:14:28,290 --> 00:14:30,720
 those are like rungs on the ladder that

161
00:14:30,720 --> 00:14:35,450
 are going to lead us to enlightenment so when you can see

162
00:14:35,450 --> 00:14:38,760
 this is good and this is bad I mean for

163
00:14:38,760 --> 00:14:42,640
 the most part that's not the biggest problem the biggest

164
00:14:42,640 --> 00:14:45,120
 problem is how do I the biggest problem

165
00:14:45,120 --> 00:14:55,120
 is acting accordingly I know this is bad can I stop doing

166
00:14:55,120 --> 00:15:01,240
 it I know this is good can I ensure that

167
00:15:01,240 --> 00:15:03,730
 I'm going to do it and that's really the problem is keeping

168
00:15:03,730 --> 00:15:07,200
 ourselves from doing bad things that's

169
00:15:07,200 --> 00:15:10,330
 the first problem anyway keeping ourselves from doing bad

170
00:15:10,330 --> 00:15:12,720
 things and encouraging ourselves to do

171
00:15:12,720 --> 00:15:16,370
 good things doesn't mean that that we all know what's right

172
00:15:16,370 --> 00:15:18,640
 and what's wrong that's that also

173
00:15:18,640 --> 00:15:23,140
 isn't the case you do there is work that has to be done to

174
00:15:23,140 --> 00:15:25,960
 understand that but all of it the meaning

175
00:15:25,960 --> 00:15:32,170
 is all of it is comprehensible to an ordinary individual if

176
00:15:32,170 --> 00:15:35,280
 one is looking to a figure out

177
00:15:35,280 --> 00:15:41,020
 what is right and what is wrong and be figure out how to

178
00:15:41,020 --> 00:15:44,920
 act accordingly how to set oneself

179
00:15:44,920 --> 00:15:49,480
 and what is good and how to prevent oneself from doing what

180
00:15:49,480 --> 00:15:51,640
 is bad to set oneself in the right

181
00:15:51,640 --> 00:15:54,830
 course and understand what is the right course these two

182
00:15:54,830 --> 00:16:00,680
 things if one has this intention then

183
00:16:00,680 --> 00:16:05,730
 the potential is there to step by step make one's way to

184
00:16:05,730 --> 00:16:11,680
 enlightenment and so the Buddha encouraged

185
00:16:11,680 --> 00:16:23,130
 the encouraged one to use one's own ability and one's own

186
00:16:23,130 --> 00:16:33,120
 mind and one's own wisdom in investigating

187
00:16:33,120 --> 00:16:40,320
 the truth in this way because honest the thing is even if

188
00:16:40,320 --> 00:16:44,400
 you did know that someone was enlightened

189
00:16:44,400 --> 00:16:48,410
 suppose we had this faculty this ability to know whether

190
00:16:48,410 --> 00:16:52,120
 someone was enlightened or they had

191
00:16:52,120 --> 00:16:55,830
 suppose there was a sign over their heads you know everyone

192
00:16:55,830 --> 00:16:57,920
 who's an arahant would have this

193
00:16:57,920 --> 00:17:05,270
 this may be a halo yeah and it was just you know and you

194
00:17:05,270 --> 00:17:08,800
 could somehow be totally sure that that

195
00:17:08,800 --> 00:17:12,960
 halo meant the person was an arahant that that's exactly

196
00:17:12,960 --> 00:17:15,360
 what it meant even if that were the case

197
00:17:15,360 --> 00:17:18,180
 and then you said to yourself so that means I have to

198
00:17:18,180 --> 00:17:20,880
 believe everything they say and and then

199
00:17:20,880 --> 00:17:23,890
 that's not even true but let's say Buddha you know it makes

200
00:17:23,890 --> 00:17:25,920
 them a fully enlightened Buddha and

201
00:17:25,920 --> 00:17:29,280
 there were Buddhas all over the place and you just had to

202
00:17:29,280 --> 00:17:33,200
 listen to them right and because

203
00:17:33,200 --> 00:17:40,540
 they're not even an arahant can be wrong about things they

204
00:17:40,540 --> 00:17:44,760
 can they can still still make mistakes

205
00:17:44,760 --> 00:17:51,860
 they can't have delusion but they can still have ignorance

206
00:17:51,860 --> 00:17:56,480
 about certain things and misspeak and

207
00:17:56,480 --> 00:17:59,730
 so on I guess no the very least they can there are things

208
00:17:59,730 --> 00:18:01,520
 they don't know so they still have

209
00:18:01,520 --> 00:18:06,360
 ignorance but a Buddha doesn't have that so but the point

210
00:18:06,360 --> 00:18:11,640
 here even if that were the case and they

211
00:18:11,640 --> 00:18:17,200
 tell you you must do such and such even that is a very

212
00:18:17,200 --> 00:18:21,800
 limited value to us there's a relatively

213
00:18:21,800 --> 00:18:24,920
 limited value to us because if you look at all of the

214
00:18:24,920 --> 00:18:28,960
 people who did meet the Buddha and how many

215
00:18:28,960 --> 00:18:32,150
 of them didn't become enlightened as a result that's

216
00:18:32,150 --> 00:18:34,840
 because their own minds were not directed

217
00:18:34,840 --> 00:18:38,700
 in the right way that knowledge you have to understand the

218
00:18:38,700 --> 00:18:40,640
 difference between two types

219
00:18:40,640 --> 00:18:43,040
 these two types of knowledge knowledge from someone else is

220
00:18:43,040 --> 00:18:45,720
 not true knowledge knowledge

221
00:18:45,720 --> 00:18:50,190
 that you've received from an outside source you don't know

222
00:18:50,190 --> 00:18:53,360
 that that you haven't understood

223
00:18:53,360 --> 00:18:58,040
 that for yourself relatively speaking it's very limited if

224
00:18:58,040 --> 00:19:03,080
 you have faith in the person then it

225
00:19:03,080 --> 00:19:06,780
 will lead you to practice to understand for yourself

226
00:19:06,780 --> 00:19:09,380
 potentially but that's all it can do

227
00:19:09,380 --> 00:19:15,280
 it can never be a replacement for your own understanding so

228
00:19:15,280 --> 00:19:19,840
 so it's not it's not it's not

229
00:19:19,840 --> 00:19:22,200
 worth as to spare discouraging of being discouraged about

230
00:19:22,200 --> 00:19:23,640
 the fact that we'll never know who's

231
00:19:23,640 --> 00:19:27,510
 enlightened and who is not that's not really it wouldn't be

232
00:19:27,510 --> 00:19:29,680
 wouldn't be any better if you could

233
00:19:29,680 --> 00:19:32,320
 it wouldn't be much it would be better but it wouldn't be

234
00:19:32,320 --> 00:19:34,280
 wouldn't solve the problem if you could

235
00:19:34,280 --> 00:19:39,110
 you still have to figure it out for yourself so yes you

236
00:19:39,110 --> 00:19:42,320
 have to be discerning and you have to

237
00:19:42,320 --> 00:19:45,530
 listen to what teachers have to say and discriminate

238
00:19:45,530 --> 00:19:49,100
 between different teachers this teacher says this

239
00:19:49,100 --> 00:19:52,010
 that teacher says that you have to be able to see the

240
00:19:52,010 --> 00:19:54,540
 goodness in people if a teacher has lots of

241
00:19:54,540 --> 00:19:58,640
 things that are questionable then you have to question them

242
00:19:58,640 --> 00:20:00,620
 and you have to maybe shy away from

243
00:20:00,620 --> 00:20:09,300
 that teacher and you have to begin to build up views and

244
00:20:09,300 --> 00:20:14,200
 and ideas to some extent in order to

245
00:20:14,200 --> 00:20:17,470
 protect yourself against bad teachings so for example we

246
00:20:17,470 --> 00:20:20,320
 have the Buddhist teaching on what is

247
00:20:20,320 --> 00:20:23,960
 nibbana and what is enlightenment and so when we hear about

248
00:20:23,960 --> 00:20:26,200
 someone talk someone taking their

249
00:20:26,200 --> 00:20:29,890
 students on a trip to see nibbana or we hear about a

250
00:20:29,890 --> 00:20:33,040
 teacher who claims to talk claims to be able to

251
00:20:33,040 --> 00:20:36,520
 talk with the Buddha who's passed away or talk with our

252
00:20:36,520 --> 00:20:42,280
 hunts who have passed away then we we

253
00:20:42,280 --> 00:20:45,860
 have to question that person we question that person's

254
00:20:45,860 --> 00:20:48,160
 authenticity we tend I mean this was

255
00:20:48,160 --> 00:20:52,600
 part of this argument this argument was that no matter how

256
00:20:52,600 --> 00:20:54,760
 good that teacher is no matter how

257
00:20:54,760 --> 00:20:58,310
 wise or so on that that in and of itself tells me that they

258
00:20:58,310 --> 00:21:00,800
 have they don't understand enlightenment

259
00:21:00,800 --> 00:21:04,390
 they don't understand nibbana and that's all it's it's a

260
00:21:04,390 --> 00:21:07,440
 difference between goodness and enlightenment

261
00:21:07,440 --> 00:21:10,050
 they can still be good and that's it's it's difficult to

262
00:21:10,050 --> 00:21:11,680
 argue because they say but that

263
00:21:11,680 --> 00:21:14,090
 person is so good they must be enlightened and that person

264
00:21:14,090 --> 00:21:16,080
 is such a great teacher and and has

265
00:21:16,080 --> 00:21:20,210
 all these magical powers and so on and when they died their

266
00:21:20,210 --> 00:21:22,600
 their bones became crystal and all

267
00:21:22,600 --> 00:21:26,410
 this all these things you know ways of telling that someone

268
00:21:26,410 --> 00:21:28,680
 is an Arahant and it's not they're

269
00:21:28,680 --> 00:21:32,600
 not really that's not how you tell if someone you can't

270
00:21:32,600 --> 00:21:37,480
 tell if someone is an Arahant unless you

271
00:21:37,480 --> 00:21:47,470
 are as well even then I wouldn't want to speculate but but

272
00:21:47,470 --> 00:21:54,440
 you can discriminate and so when you start

273
00:21:54,440 --> 00:21:59,170
 to practice and understand then the more you practice and

274
00:21:59,170 --> 00:22:01,840
 the more you understand the better

275
00:22:01,840 --> 00:22:06,550
 you're able are to sort out things sort things out and so

276
00:22:06,550 --> 00:22:09,080
 that in this example if you have someone

277
00:22:09,080 --> 00:22:11,920
 who is claiming to talk to a Buddha who has passed away

278
00:22:11,920 --> 00:22:16,400
 here's a good example well having

279
00:22:16,400 --> 00:22:20,720
 practice for some time you you understand what is logical

280
00:22:20,720 --> 00:22:24,880
 and reasonable about reality you understand

281
00:22:24,880 --> 00:22:30,950
 about death and and how the mind doesn't really die it just

282
00:22:30,950 --> 00:22:34,840
 keeps going and then you understand

283
00:22:34,840 --> 00:22:38,990
 perhaps intellectually about nirvana you know suppose you

284
00:22:38,990 --> 00:22:41,800
 haven't seen nirvana for yourself yet

285
00:22:41,800 --> 00:22:45,460
 but you understand intellectually that it really is

286
00:22:45,460 --> 00:22:52,360
 something that doesn't arise is outside of

287
00:22:52,360 --> 00:22:56,520
 samsara and so on and so you understand that that's what

288
00:22:56,520 --> 00:22:59,400
 makes a Buddha so special or an Arahant so

289
00:22:59,400 --> 00:23:02,970
 special is that they don't come back they aren't born again

290
00:23:02,970 --> 00:23:07,480
 there are no more Sankaras and then so

291
00:23:07,480 --> 00:23:09,900
 you say well if there are no more Sankaras then how could

292
00:23:09,900 --> 00:23:11,600
 you possibly talk to an enlightened

293
00:23:11,600 --> 00:23:15,030
 being who's passed away what would there be left that would

294
00:23:15,030 --> 00:23:19,920
 allow you to talk with them so that's

295
00:23:19,920 --> 00:23:23,300
 an example of how you would then you then have to say to

296
00:23:23,300 --> 00:23:25,680
 yourself why is it that this enlightened

297
00:23:25,680 --> 00:23:29,900
 being is teaching this crazy is claiming this crazy thing

298
00:23:29,900 --> 00:23:32,400
 and and and you say how can that be but he

299
00:23:32,400 --> 00:23:35,780
 must be enlightened and you have to realize it's he might

300
00:23:35,780 --> 00:23:39,320
 just be a very very good person maybe he's

301
00:23:39,320 --> 00:23:44,690
 like the bodhisattva the bodhisattva was very wise but

302
00:23:44,690 --> 00:23:49,120
 never became enlightened not until the very

303
00:23:49,120 --> 00:23:56,750
 end four uncountable eons later plus 100,000 great eons

304
00:23:56,750 --> 00:24:00,280
 long long time when he was very wise

305
00:24:00,280 --> 00:24:04,700
 did a lot of good things and helped a lot of people and

306
00:24:04,700 --> 00:24:06,720
 probably a lot of people said that

307
00:24:06,720 --> 00:24:16,160
 about him is that he was enlightened but he wasn't anyway I

308
00:24:16,160 --> 00:24:20,920
 didn't I've been sitting here chatting

309
00:24:20,920 --> 00:24:24,440
 with Christine for a while so I just had this thought ratt

310
00:24:24,440 --> 00:24:26,680
ling around in my head for some days

311
00:24:26,680 --> 00:24:31,230
 now so I thought that would be an interesting Dhamma to

312
00:24:31,230 --> 00:24:34,480
 give today nothing really serious as

313
00:24:34,480 --> 00:24:39,300
 far as a talk but hope that's been interesting and wish for

314
00:24:39,300 --> 00:24:41,600
 all of us for all of you who are

315
00:24:41,600 --> 00:24:48,980
 listening to be able to discern the good from the bad and

316
00:24:48,980 --> 00:24:53,800
 to step-by-step climb the rungs of this

317
00:24:53,800 --> 00:24:57,450
 ladder to realize nirvana for yourselves then you won't

318
00:24:57,450 --> 00:25:01,280
 have to have any doubt about it be well

