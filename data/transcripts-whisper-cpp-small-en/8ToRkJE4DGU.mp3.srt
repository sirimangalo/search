1
00:00:00,000 --> 00:00:08,630
 Hello, so this is the second in my series on why everyone

2
00:00:08,630 --> 00:00:10,920
 should practice meditation.

3
00:00:10,920 --> 00:00:14,080
 And this is going to give the number four reason, the top

4
00:00:14,080 --> 00:00:15,640
 five reasons why everyone

5
00:00:15,640 --> 00:00:16,800
 should meditate.

6
00:00:16,800 --> 00:00:21,210
 The number four reason is because meditation is something

7
00:00:21,210 --> 00:00:23,480
 which allows us to do away with

8
00:00:23,480 --> 00:00:28,880
 sorrow, lamentation and despair.

9
00:00:28,880 --> 00:00:34,750
 This includes all kinds of depression and all sorts of

10
00:00:34,750 --> 00:00:37,880
 mourning and sadness and any

11
00:00:37,880 --> 00:00:39,280
 kind of upset in the mind.

12
00:00:39,280 --> 00:00:43,950
 All of our mental conditions which for the most part we are

13
00:00:43,950 --> 00:00:46,080
 unable to do away with.

14
00:00:46,080 --> 00:00:50,320
 We try to find a way out through medication.

15
00:00:50,320 --> 00:00:54,090
 We think of it as perhaps an imbalance in the mind or we

16
00:00:54,090 --> 00:00:56,240
 try to do away with it through

17
00:00:56,240 --> 00:01:01,140
 drugs, through alcohol, through entertainment, through

18
00:01:01,140 --> 00:01:02,360
 diversion.

19
00:01:02,360 --> 00:01:07,450
 And so we find ourselves really covering over the cause or

20
00:01:07,450 --> 00:01:10,200
 covering over the experience

21
00:01:10,200 --> 00:01:14,020
 of the suffering, the experience of the unpleasantness of

22
00:01:14,020 --> 00:01:15,240
 the situation.

23
00:01:15,240 --> 00:01:17,390
 And as a result we are never really able to do away with

24
00:01:17,390 --> 00:01:18,440
 the unpleasantness.

25
00:01:18,440 --> 00:01:26,180
 We are never really able to fully overcome these difficult

26
00:01:26,180 --> 00:01:28,480
 experiences.

27
00:01:28,480 --> 00:01:30,970
 The practice of meditation at the time when we practice and

28
00:01:30,970 --> 00:01:32,080
 our mind is clear and our

29
00:01:32,080 --> 00:01:36,130
 mind is fully aware of the situation as it's occurring and

30
00:01:36,130 --> 00:01:38,120
 aware of the reality around

31
00:01:38,120 --> 00:01:39,120
 us.

32
00:01:39,120 --> 00:01:42,010
 We come to see that really all of the things which are

33
00:01:42,010 --> 00:01:44,040
 upsetting us, whether they be loss

34
00:01:44,040 --> 00:01:49,160
 or whether they be lack of achievement, not able to attain

35
00:01:49,160 --> 00:01:51,400
 the things that we want to

36
00:01:51,400 --> 00:01:54,620
 attain, not able to get the things that we want to get or

37
00:01:54,620 --> 00:01:56,280
 being confronted by things

38
00:01:56,280 --> 00:01:59,720
 which are unpleasant, being confronted by things which are

39
00:01:59,720 --> 00:02:01,200
 very difficult to bear or

40
00:02:01,200 --> 00:02:06,600
 just overall feelings of sadness and depression.

41
00:02:06,600 --> 00:02:09,390
 Actually these things are very easy to overcome at the time

42
00:02:09,390 --> 00:02:10,420
 when we practice.

43
00:02:10,420 --> 00:02:17,400
 These things, we can see that they have really no basis in

44
00:02:17,400 --> 00:02:18,920
 reality.

45
00:02:18,920 --> 00:02:22,560
 They take all sorts of conceptual objects.

46
00:02:22,560 --> 00:02:26,790
 So it's either a person who's passed away or who's left or

47
00:02:26,790 --> 00:02:28,800
 who's gone who is no longer

48
00:02:28,800 --> 00:02:32,530
 with us or it's a thing or it's a concept of some sort,

49
00:02:32,530 --> 00:02:34,640
 something which we create in

50
00:02:34,640 --> 00:02:37,060
 our mind and actually has nothing to do with the reality

51
00:02:37,060 --> 00:02:37,680
 around us.

52
00:02:37,680 --> 00:02:42,110
 Now at the time when we are mindful, we can see this and we

53
00:02:42,110 --> 00:02:44,280
 can bring our minds back to

54
00:02:44,280 --> 00:02:47,570
 the present moment and we're able very quickly to do away

55
00:02:47,570 --> 00:02:48,840
 with these things.

56
00:02:48,840 --> 00:02:53,640
 People are able to do with great terrible states of stress

57
00:02:53,640 --> 00:02:55,680
 and depression in a very

58
00:02:55,680 --> 00:02:56,680
 short time.

59
00:02:56,680 --> 00:03:02,070
 In fact, these are things which take very little time to do

60
00:03:02,070 --> 00:03:04,760
 away with states of sadness,

61
00:03:04,760 --> 00:03:10,150
 of mourning, of depression, of stress, or anxiety, fear,

62
00:03:10,150 --> 00:03:11,040
 worry.

63
00:03:11,040 --> 00:03:14,910
 Even insomnia, many people come to practice meditation who

64
00:03:14,910 --> 00:03:16,960
 are unable to sleep adequately

65
00:03:16,960 --> 00:03:20,880
 or feel like they're not able to sleep as much or as sound

66
00:03:20,880 --> 00:03:22,680
ly as they would like.

67
00:03:22,680 --> 00:03:25,260
 They find that through the practice of meditation, very

68
00:03:25,260 --> 00:03:26,960
 quickly they're able to overcome this

69
00:03:26,960 --> 00:03:31,420
 and their mind relaxes and as a result they're able to

70
00:03:31,420 --> 00:03:34,360
 sleep or they're able to relax.

71
00:03:34,360 --> 00:03:38,280
 They're able to live their lives peaceful and happily.

72
00:03:38,280 --> 00:03:41,240
 This is something to keep in mind if there's anyone out

73
00:03:41,240 --> 00:03:43,040
 there who knows someone who has

74
00:03:43,040 --> 00:03:46,000
 a condition where maybe they're taking medication for

75
00:03:46,000 --> 00:03:48,800
 trying to cure it through a physical means.

76
00:03:48,800 --> 00:03:52,810
 Actually, there is a cure, a mental cure, using the mind as

77
00:03:52,810 --> 00:03:54,880
 the cure, using the training

78
00:03:54,880 --> 00:04:01,410
 of the mind, trying to bring about a cure on a mental basis

79
00:04:01,410 --> 00:04:01,920
.

80
00:04:01,920 --> 00:04:04,640
 This is the practice of meditation.

81
00:04:04,640 --> 00:04:07,750
 If you come and you start to use this clear thought and

82
00:04:07,750 --> 00:04:09,920
 bring your mind back to the present

83
00:04:09,920 --> 00:04:12,320
 reality, you start to see that all of these things really

84
00:04:12,320 --> 00:04:13,640
 have no meaning, have no basis

85
00:04:13,640 --> 00:04:17,450
 in reality, in the reality that we're experiencing right

86
00:04:17,450 --> 00:04:18,000
 now.

87
00:04:18,000 --> 00:04:20,870
 It's often something from the past or something in the

88
00:04:20,870 --> 00:04:22,800
 future and really has nothing to do

89
00:04:22,800 --> 00:04:27,170
 with reality, which is the phenomena which arise and which

90
00:04:27,170 --> 00:04:29,840
 are experienced at every moment.

91
00:04:29,840 --> 00:04:31,120
 This is number four.

92
00:04:31,120 --> 00:04:32,880
 The number four reason why everyone should practice

93
00:04:32,880 --> 00:04:33,440
 meditation.

94
00:04:33,440 --> 00:04:37,050
 Thanks for tuning in and look forward to seeing number

95
00:04:37,050 --> 00:04:39,320
 three in the near or far future.

96
00:04:39,320 --> 00:04:39,320
 Thank you.

97
00:04:39,320 --> 00:04:40,320
 [END]

