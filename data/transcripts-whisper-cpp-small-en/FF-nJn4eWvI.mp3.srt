1
00:00:00,000 --> 00:00:05,810
 Welcome back to Ask a Monk. Next question comes from Elek

2
00:00:05,810 --> 00:00:10,920
 Maham, who asks, "I am

3
00:00:10,920 --> 00:00:14,380
 wanting to learn to transcend my suffering and overcome my

4
00:00:14,380 --> 00:00:16,160
 suffering, but

5
00:00:16,160 --> 00:00:19,820
 I also wish to positively affect change in the world around

6
00:00:19,820 --> 00:00:21,220
 me. How can I

7
00:00:21,220 --> 00:00:24,890
 reconcile the idea of accepting this reality with my desire

8
00:00:24,890 --> 00:00:25,760
 to make the world

9
00:00:25,760 --> 00:00:34,040
 better for all?" Good question. There's a few things that

10
00:00:34,040 --> 00:00:35,920
 you have to keep in

11
00:00:35,920 --> 00:00:40,090
 mind in this sort of situation, which I think is the

12
00:00:40,090 --> 00:00:41,480
 situation that we all

13
00:00:41,480 --> 00:00:45,240
 should be in, the desire to help ourselves and the desire

14
00:00:45,240 --> 00:00:46,080
 to help others.

15
00:00:46,080 --> 00:00:53,090
 The first is knowing your place in the world. I think for

16
00:00:53,090 --> 00:00:55,840
 anyone who expects to

17
00:00:55,840 --> 00:01:02,600
 change the world in its entirety to make the world a

18
00:01:02,600 --> 00:01:03,600
 peaceful place,

19
00:01:03,600 --> 00:01:07,490
 I think obviously, and I hope there's not many people in

20
00:01:07,490 --> 00:01:09,560
 that sort of delusion,

21
00:01:09,560 --> 00:01:15,070
 I think that there's no hope of ever making the world a

22
00:01:15,070 --> 00:01:16,920
 peaceful place. I mean,

23
00:01:16,920 --> 00:01:20,370
 to put it all into context, that eventually this world is

24
00:01:20,370 --> 00:01:22,480
 going to,

25
00:01:22,480 --> 00:01:27,070
 the planet is going to fly into the Sun or be burnt to a

26
00:01:27,070 --> 00:01:29,120
 crisp by the

27
00:01:29,120 --> 00:01:36,870
 heat of the Sun. So there's no hope. I mean, we can help

28
00:01:36,870 --> 00:01:37,360
 the world and

29
00:01:37,360 --> 00:01:41,020
 we can help other people, but in the end we have to give up

30
00:01:41,020 --> 00:01:42,640
 and we have to let go.

31
00:01:42,640 --> 00:01:53,720
 The key to, you know, finding a balance is in knowing what

32
00:01:53,720 --> 00:01:54,200
 you can

33
00:01:54,200 --> 00:01:58,030
 do and accepting the things that you can't do. So we want

34
00:01:58,030 --> 00:02:00,080
 to be able to help

35
00:02:00,080 --> 00:02:02,380
 people, then we understand that we can help the people

36
00:02:02,380 --> 00:02:03,400
 around us. We can help

37
00:02:03,400 --> 00:02:08,630
 our family, we can help our friends, we can affect change

38
00:02:08,630 --> 00:02:11,640
 in our sphere of

39
00:02:11,640 --> 00:02:16,380
 influence and we can change and expand that sphere of

40
00:02:16,380 --> 00:02:19,000
 influence, but in the end

41
00:02:19,000 --> 00:02:25,610
 it's always going to be limited. And well, I think that it

42
00:02:25,610 --> 00:02:26,680
's important to,

43
00:02:26,680 --> 00:02:30,760
 it's important part of who we are to want to help others. I

44
00:02:30,760 --> 00:02:31,600
 think that's the

45
00:02:31,600 --> 00:02:38,600
 key is that it should be simply a part of who we are. If

46
00:02:38,600 --> 00:02:41,480
 someone has ambition to

47
00:02:41,480 --> 00:02:45,840
 change the world and that's their goal, then I think that

48
00:02:45,840 --> 00:02:47,200
 ambition is actually

49
00:02:47,200 --> 00:02:52,000
 going to get in the way of their successful advancement as

50
00:02:52,000 --> 00:02:52,880
 a human being

51
00:02:52,880 --> 00:02:58,960
 because they're going to always be frustrated and upset by

52
00:02:58,960 --> 00:03:01,520
 their

53
00:03:01,520 --> 00:03:04,810
 by not being able to affect the source of change that they

54
00:03:04,810 --> 00:03:07,120
 want to affect.

55
00:03:07,120 --> 00:03:12,290
 I think the real answer to your question is that by

56
00:03:12,290 --> 00:03:14,320
 changing yourself and by

57
00:03:14,320 --> 00:03:20,310
 learning to accept and let go, you become a better person

58
00:03:20,310 --> 00:03:23,200
 and as a better person

59
00:03:23,200 --> 00:03:26,470
 you affect the change in the world around you that you

60
00:03:26,470 --> 00:03:29,280
 would like to see.

61
00:03:29,280 --> 00:03:32,180
 I think it was Gandhi who said that to be the change that

62
00:03:32,180 --> 00:03:33,360
 you would like to see in

63
00:03:33,360 --> 00:03:41,340
 the world. It's wrong for us to go about thinking that we

64
00:03:41,340 --> 00:03:41,720
're going to

65
00:03:41,720 --> 00:03:46,520
 change other people or change the world when we ourselves

66
00:03:46,520 --> 00:03:49,400
 are full of things

67
00:03:49,400 --> 00:03:51,950
 that should be changed, are full of negative state,

68
00:03:51,950 --> 00:03:54,240
 negative mind states and

69
00:03:54,240 --> 00:04:01,290
 negative emotions and so on. If we can't change those then

70
00:04:01,290 --> 00:04:02,640
 our acts in the world,

71
00:04:02,640 --> 00:04:06,600
 our sphere of influence, the being who we are which is very

72
00:04:06,600 --> 00:04:06,800
 much

73
00:04:06,800 --> 00:04:10,730
 connected with the world around us is going to have a

74
00:04:10,730 --> 00:04:12,040
 negative impact on the

75
00:04:12,040 --> 00:04:14,550
 world. It's going to have a negative impact on our minds

76
00:04:14,550 --> 00:04:15,200
 and it's a

77
00:04:15,200 --> 00:04:18,420
 negative impact on the minds of the beings around us. If

78
00:04:18,420 --> 00:04:20,720
 our mind is pure as

79
00:04:20,720 --> 00:04:29,160
 we work to purify our minds, we will naturally affect the

80
00:04:29,160 --> 00:04:31,040
 beings and the

81
00:04:31,040 --> 00:04:37,440
 world around us whether we're even in the jungle, seemingly

82
00:04:37,440 --> 00:04:43,840
 removed from the world. If a person is dedicated to

83
00:04:43,840 --> 00:04:45,120
 clearing and cleaning

84
00:04:45,120 --> 00:04:48,920
 their mind, they can't help but have an effect on the world

85
00:04:48,920 --> 00:04:50,560
 at large and

86
00:04:50,560 --> 00:04:55,980
 they have it in their own sphere of influence. So not

87
00:04:55,980 --> 00:04:57,520
 everyone will be living

88
00:04:57,520 --> 00:04:59,510
 in the forest. For some people that's their sphere of

89
00:04:59,510 --> 00:05:00,400
 influence, that their

90
00:05:00,400 --> 00:05:05,910
 life naturally gravitates towards seclusion. For other

91
00:05:05,910 --> 00:05:07,480
 people it naturally

92
00:05:07,480 --> 00:05:11,200
 gravitates towards society. The important thing is that

93
00:05:11,200 --> 00:05:12,120
 whatever

94
00:05:12,120 --> 00:05:19,820
 life you live, that you should work to purify your sphere

95
00:05:19,820 --> 00:05:24,960
 of existence and that

96
00:05:24,960 --> 00:05:27,420
 includes your own mind and that includes helping other

97
00:05:27,420 --> 00:05:29,160
 people. It should be a part

98
00:05:29,160 --> 00:05:33,900
 of who you are and in that sense I don't think there's any

99
00:05:33,900 --> 00:05:36,520
 conflict at all between

100
00:05:36,520 --> 00:05:38,860
 helping yourself and helping other people. The Buddha said

101
00:05:38,860 --> 00:05:39,400
 when you help

102
00:05:39,400 --> 00:05:42,400
 yourself you help others. When you help others you help

103
00:05:42,400 --> 00:05:43,320
 yourself and it should

104
00:05:43,320 --> 00:05:47,480
 be that way. You should never put the welfare of other

105
00:05:47,480 --> 00:05:48,840
 people in

106
00:05:48,840 --> 00:05:52,280
 front of your own welfare, thinking that you'll help other

107
00:05:52,280 --> 00:05:52,800
 people in

108
00:05:52,800 --> 00:05:59,000
 some way while sacrificing your own mental clarity because

109
00:05:59,000 --> 00:05:59,720
 that's

110
00:05:59,720 --> 00:06:03,990
 the point is that you're going to only create tension and

111
00:06:03,990 --> 00:06:05,240
 stress. You see all

112
00:06:05,240 --> 00:06:09,740
 these organizations that I used to get involved in that are

113
00:06:09,740 --> 00:06:10,840
 trying to affect

114
00:06:10,840 --> 00:06:13,530
 change in the world and in third world countries and

115
00:06:13,530 --> 00:06:14,880
 politically and so on

116
00:06:14,880 --> 00:06:19,950
 trying to help people. The people are in many ways stressed

117
00:06:19,950 --> 00:06:21,040
 and miserable and

118
00:06:21,040 --> 00:06:26,630
 get angry and fight and so on. There's a lot of suffering

119
00:06:26,630 --> 00:06:27,720
 involved because

120
00:06:27,720 --> 00:06:32,820
 they're putting the world in front of themselves and as we

121
00:06:32,820 --> 00:06:33,720
 can see in the

122
00:06:33,720 --> 00:06:38,230
 world today it's not working. The good that they're doing

123
00:06:38,230 --> 00:06:38,640
 is not

124
00:06:38,640 --> 00:06:43,040
 having any lasting impact. If you're able to build

125
00:06:43,040 --> 00:06:46,000
 hospitals or feed the poor or

126
00:06:46,000 --> 00:06:51,850
 eradicate this disease or that disease it's like trying to

127
00:06:51,850 --> 00:06:52,160
 stem

128
00:06:52,160 --> 00:06:57,360
 the flow of a river with your bare hands. You're not going

129
00:06:57,360 --> 00:06:57,760
 to get very

130
00:06:57,760 --> 00:06:59,870
 far. You can see that we're not getting very far. There's

131
00:06:59,870 --> 00:07:01,360
 no one in the world who's

132
00:07:01,360 --> 00:07:07,080
 really going to... it's impossible to really make the world

133
00:07:07,080 --> 00:07:07,840
 a perfect place.

134
00:07:07,840 --> 00:07:12,920
 I can tell you living here, I'm here in nature and I'm

135
00:07:12,920 --> 00:07:14,880
 surrounded by murderers

136
00:07:14,880 --> 00:07:20,790
 and violence all the time. The ants are killing this or

137
00:07:20,790 --> 00:07:21,840
 that and the

138
00:07:21,840 --> 00:07:31,270
 lizards and the monkeys. The universe is very much out of

139
00:07:31,270 --> 00:07:32,360
 our control really.

140
00:07:32,360 --> 00:07:40,520
 This is why I say we should work to purify our sphere of

141
00:07:40,520 --> 00:07:41,360
 existence, our

142
00:07:41,360 --> 00:07:46,480
 sphere of influence. To go beyond that is to do something

143
00:07:46,480 --> 00:07:47,120
 that's futile,

144
00:07:47,120 --> 00:07:49,970
 something that's not going to have the results that we hope

145
00:07:49,970 --> 00:07:51,680
 for. It takes us out

146
00:07:51,680 --> 00:07:55,860
 of balance and therefore leads to stress and suffering both

147
00:07:55,860 --> 00:07:57,800
 for ourselves and for

148
00:07:57,800 --> 00:08:01,360
 other people, even though we might have limited success in

149
00:08:01,360 --> 00:08:03,800
 terms of improving

150
00:08:03,800 --> 00:08:10,040
 the states of existence for various people for a temporary

151
00:08:10,040 --> 00:08:12,360
 period of time.

152
00:08:12,360 --> 00:08:16,540
 Helping people should really be a part of our spiritual

153
00:08:16,540 --> 00:08:18,680
 development. It should...

154
00:08:18,680 --> 00:08:22,520
 there should be no difference between helping ourselves and

155
00:08:22,520 --> 00:08:22,880
 helping others.

156
00:08:22,880 --> 00:08:28,920
 They should both be the improvement of the world, the world

157
00:08:28,920 --> 00:08:29,800
 that we live in,

158
00:08:29,800 --> 00:08:34,100
 which is our sphere of existence. I hope that helps and is

159
00:08:34,100 --> 00:08:36,560
 not too convoluted.

160
00:08:36,560 --> 00:08:41,040
 Thanks for the question. All the best.

