1
00:00:00,000 --> 00:00:04,920
 Hello and welcome back to our study of the Dhammapandana.

2
00:00:04,920 --> 00:00:10,360
 Today we continue on with verse 148 which reads as follows.

3
00:00:10,360 --> 00:00:17,920
 Parijinam middang ru pang, rogani lang babangguraang, bhij

4
00:00:17,920 --> 00:00:26,360
ati puti sandhe ho, maranante ye jivitang.

5
00:00:26,360 --> 00:00:34,280
 Which means, parijinam middang ru pang, this body is old,

6
00:00:34,280 --> 00:00:38,520
 is aged, completely worn out.

7
00:00:38,520 --> 00:00:45,760
 Noga nirang, a nest of disease, sickness.

8
00:00:45,760 --> 00:00:53,160
 Babangguraang, it is subject to, where it is weak, it is

9
00:00:53,160 --> 00:00:58,920
 subject to destruction, it's fragile.

10
00:00:58,920 --> 00:01:14,000
 And bhijati puti sandhe ho, this mass of filth, breaks.

11
00:01:14,000 --> 00:01:18,360
 This one is a reference to the idea of a pot filled with,

12
00:01:18,360 --> 00:01:20,480
 the idea of the body being a

13
00:01:20,480 --> 00:01:25,920
 pot full of urine and feces and when you die it breaks.

14
00:01:25,920 --> 00:01:30,830
 Or a pot that has many holes, it's leaky, it's cracked and

15
00:01:30,830 --> 00:01:31,920
 it leaks.

16
00:01:31,920 --> 00:01:34,680
 That's what the body is like.

17
00:01:34,680 --> 00:01:39,680
 And when you die it breaks.

18
00:01:39,680 --> 00:01:45,200
 Maranante ye jivitang, for death is the end of life.

19
00:01:45,200 --> 00:01:50,560
 Maranante ye jivitang.

20
00:01:50,560 --> 00:01:56,860
 All the very pleasant or upbeat verse by the useful one

21
00:01:56,860 --> 00:01:58,280
 that was.

22
00:01:58,280 --> 00:02:08,240
 So the story goes, this is the story of the nun uttara.

23
00:02:08,240 --> 00:02:12,490
 I'm not sure which uttara, it seems there were several utt

24
00:02:12,490 --> 00:02:13,480
ara nuns.

25
00:02:13,480 --> 00:02:20,260
 But uttara was committed, she was quite a committed or

26
00:02:20,260 --> 00:02:24,680
 devoted, reckless bhikkhuni.

27
00:02:24,680 --> 00:02:28,910
 And for all of her life she went on aamzraand until she was

28
00:02:28,910 --> 00:02:30,760
 120 years old.

29
00:02:30,760 --> 00:02:35,680
 Aamzara is an interesting thing, you start to appreciate

30
00:02:35,680 --> 00:02:37,880
 food in a whole new way and

31
00:02:37,880 --> 00:02:39,440
 people give it to you.

32
00:02:39,440 --> 00:02:44,500
 They often give you what's left over or they give to you

33
00:02:44,500 --> 00:02:47,200
 what is most dear to them.

34
00:02:47,200 --> 00:02:50,720
 But having to go out into the village really makes you

35
00:02:50,720 --> 00:02:52,720
 mindful of the need to eat.

36
00:02:52,720 --> 00:02:56,400
 How it's the one thing that we can't escape, it's this one

37
00:02:56,400 --> 00:02:58,200
 sickness that we have, this

38
00:02:58,200 --> 00:03:03,080
 need to eat, we're addicted to food.

39
00:03:03,080 --> 00:03:09,620
 This body requires constant and repeated nourishment day

40
00:03:09,620 --> 00:03:12,240
 after day after day.

41
00:03:12,240 --> 00:03:14,790
 And so you sort of look at food in a whole new way, not as

42
00:03:14,790 --> 00:03:16,360
 this thing that I must have,

43
00:03:16,360 --> 00:03:19,630
 but this thing that I may get and if I get it means my life

44
00:03:19,630 --> 00:03:21,400
 will continue and if I don't

45
00:03:21,400 --> 00:03:24,040
 get it my life may end.

46
00:03:24,040 --> 00:03:27,200
 You become somewhat liberated.

47
00:03:27,200 --> 00:03:31,280
 Aamzara is liberating, I mean it's incredibly liberating

48
00:03:31,280 --> 00:03:33,040
 through its humility.

49
00:03:33,040 --> 00:03:34,040
 It's almost as though you're begging.

50
00:03:34,040 --> 00:03:38,510
 Of course we're not allowed to beg but you would walk

51
00:03:38,510 --> 00:03:41,480
 through the village and it's wonderful

52
00:03:41,480 --> 00:03:47,770
 because people are delighted to offer food to monks, not

53
00:03:47,770 --> 00:03:51,000
 just in Asia but also in North

54
00:03:51,000 --> 00:04:00,320
 America and Europe and throughout the world really.

55
00:04:00,320 --> 00:04:04,720
 So I would take that as the basis of this story because the

56
00:04:04,720 --> 00:04:06,680
 story goes that there's

57
00:04:06,680 --> 00:04:14,010
 none, 120 years old, having practiced Aamzara her whole

58
00:04:14,010 --> 00:04:15,040
 life.

59
00:04:15,040 --> 00:04:19,580
 One time she saw a particular monk walking on Aamzara and

60
00:04:19,580 --> 00:04:21,920
 it doesn't say anything about

61
00:04:21,920 --> 00:04:22,920
 this monk.

62
00:04:22,920 --> 00:04:28,260
 It says a certain monk but perhaps he was enlightened,

63
00:04:28,260 --> 00:04:31,600
 perhaps he appeared enlightened

64
00:04:31,600 --> 00:04:37,810
 or appeared somehow worthy of respect or perhaps not,

65
00:04:37,810 --> 00:04:42,400
 perhaps it was a means of humbling him.

66
00:04:42,400 --> 00:04:44,520
 She gave him all of his food and she asked permission if

67
00:04:44,520 --> 00:04:45,680
 she could give him his food

68
00:04:45,680 --> 00:04:48,160
 and she gave him all of her food.

69
00:04:48,160 --> 00:04:51,720
 I don't think I've ever done that before, I've given some

70
00:04:51,720 --> 00:04:52,640
 of my food.

71
00:04:52,640 --> 00:04:56,140
 Often on Aamzara you'll meet another monk or a nun and

72
00:04:56,140 --> 00:04:58,240
 especially the nuns I would often

73
00:04:58,240 --> 00:05:01,570
 give them food because in Asia it's not always sure that

74
00:05:01,570 --> 00:05:03,560
 they're going to get food the same

75
00:05:03,560 --> 00:05:04,560
 as the monks.

76
00:05:04,560 --> 00:05:07,220
 Of course honestly I think I would have realized that they

77
00:05:07,220 --> 00:05:08,920
 often get more because people are

78
00:05:08,920 --> 00:05:15,120
 so appreciative of the rarity of the nun.

79
00:05:15,120 --> 00:05:18,260
 Anyway she gave him all of her food and had none for

80
00:05:18,260 --> 00:05:19,040
 herself.

81
00:05:19,040 --> 00:05:21,670
 I think another thing you could say is at 120 she wasn't

82
00:05:21,670 --> 00:05:23,040
 really all that concerned with

83
00:05:23,040 --> 00:05:26,720
 living on.

84
00:05:26,720 --> 00:05:29,800
 Whatever her reason she did and went without food for that

85
00:05:29,800 --> 00:05:30,240
 day.

86
00:05:30,240 --> 00:05:34,100
 The next day she met the monk again and did the same thing

87
00:05:34,100 --> 00:05:36,040
 and so on for a third day.

88
00:05:36,040 --> 00:05:40,050
 I wish when she started to become quite weak it was an

89
00:05:40,050 --> 00:05:42,720
 interesting choice that she made

90
00:05:42,720 --> 00:05:46,920
 of practice, probably not a wise one in the end.

91
00:05:46,920 --> 00:05:49,560
 On the fourth day she met the Buddha.

92
00:05:49,560 --> 00:05:54,980
 Perhaps the Buddha was interested in sort of maybe jolting

93
00:05:54,980 --> 00:05:58,560
 her back into proper practice.

94
00:05:58,560 --> 00:06:01,600
 I'm not sure.

95
00:06:01,600 --> 00:06:06,290
 When she met the teacher I guess it was crowded it says and

96
00:06:06,290 --> 00:06:08,720
 so she stepped back to let the

97
00:06:08,720 --> 00:06:14,010
 teacher go by and when she stepped back she stepped on her

98
00:06:14,010 --> 00:06:17,520
 robe and tripped and fell sprawling

99
00:06:17,520 --> 00:06:20,360
 on the ground.

100
00:06:20,360 --> 00:06:23,720
 The Buddha is an interesting way about him.

101
00:06:23,720 --> 00:06:31,120
 Obviously quite a unique mannerism.

102
00:06:31,120 --> 00:06:33,780
 Very much in terms of letting people deal with their karma

103
00:06:33,780 --> 00:06:35,320
 it doesn't appear that he actually

104
00:06:35,320 --> 00:06:39,560
 tried to help the woman up or get someone to help her up.

105
00:06:39,560 --> 00:06:41,400
 It was a learning experience for her.

106
00:06:41,400 --> 00:06:44,480
 She could get up on her own I guess.

107
00:06:44,480 --> 00:06:47,360
 What he did is he walked over and he gave her a teaching.

108
00:06:47,360 --> 00:06:50,640
 This was a teaching for her.

109
00:06:50,640 --> 00:06:51,640
 He didn't criticize her.

110
00:06:51,640 --> 00:06:54,360
 He didn't say, "Hey, why are you so weak?

111
00:06:54,360 --> 00:06:55,360
 Why are you not eating?"

112
00:06:55,360 --> 00:06:58,240
 That kind of thing.

113
00:06:58,240 --> 00:07:00,190
 He gave her a teaching that was actually somewhat apropos

114
00:07:00,190 --> 00:07:01,640
 because it wasn't really wrong what

115
00:07:01,640 --> 00:07:03,920
 she was doing.

116
00:07:03,920 --> 00:07:06,930
 It's something that could easily be realized in her

117
00:07:06,930 --> 00:07:07,800
 situation.

118
00:07:07,800 --> 00:07:10,760
 This is the teaching he gave her about her body.

119
00:07:10,760 --> 00:07:14,020
 Look at this body that you've made very weak and that is

120
00:07:14,020 --> 00:07:15,680
 weak by its very nature.

121
00:07:15,680 --> 00:07:19,200
 It's so fragile.

122
00:07:19,200 --> 00:07:23,210
 Perhaps it was even a reminder that this body is not

123
00:07:23,210 --> 00:07:26,280
 something that you can take lightly.

124
00:07:26,280 --> 00:07:29,000
 You can't just abuse it.

125
00:07:29,000 --> 00:07:34,400
 Anyway, he taught her this verse.

126
00:07:34,400 --> 00:07:38,180
 As a result of the verse, apparently she became a surtapan

127
00:07:38,180 --> 00:07:38,680
er.

128
00:07:38,680 --> 00:07:41,720
 Many beings around were able to gain benefits.

129
00:07:41,720 --> 00:07:45,560
 "Sātika dhamma dhi sana ho sī."

130
00:07:45,560 --> 00:07:49,640
 The teaching was a great benefit to many.

131
00:07:49,640 --> 00:07:54,160
 It's a great benefit to us I think.

132
00:07:54,160 --> 00:08:00,280
 All of us are quite young here in Hamilton.

133
00:08:00,280 --> 00:08:02,100
 Everybody in second life of course looks young and

134
00:08:02,100 --> 00:08:03,840
 beautiful, but we don't really know the

135
00:08:03,840 --> 00:08:04,840
 truth.

136
00:08:04,840 --> 00:08:09,800
 But it doesn't matter.

137
00:08:09,800 --> 00:08:12,800
 The commentary says about this "pabangurang."

138
00:08:12,800 --> 00:08:13,800
 No.

139
00:08:13,800 --> 00:08:15,800
 Yeah, "pabangurang."

140
00:08:15,800 --> 00:08:20,260
 "Pabangurang" means fragile or subject to "pabanjati,"

141
00:08:20,260 --> 00:08:23,760
 which means breakage or destruction.

142
00:08:23,760 --> 00:08:26,880
 Easily broken.

143
00:08:26,880 --> 00:08:36,040
 It says about this word that it means even a young person

144
00:08:36,040 --> 00:08:38,080
 is frail.

145
00:08:38,080 --> 00:08:43,360
 Even young people are fragile.

146
00:08:43,360 --> 00:08:47,150
 So the lesson for us I think, there's so much that can be

147
00:08:47,150 --> 00:08:49,360
 said, but to start at the beginning

148
00:08:49,360 --> 00:08:54,710
 is we think of all the ways we look at the body as being

149
00:08:54,710 --> 00:08:57,240
 powerful, healthy.

150
00:08:57,240 --> 00:08:59,120
 Some people even think of the body as a temple.

151
00:08:59,120 --> 00:09:03,880
 We have many ways of looking at the body.

152
00:09:03,880 --> 00:09:08,730
 Most of us just have a concept of it being our body,

153
00:09:08,730 --> 00:09:12,800
 subject to our whim, capable, capable

154
00:09:12,800 --> 00:09:13,800
 of so much.

155
00:09:13,800 --> 00:09:17,340
 We may not consciously think of it while this body is

156
00:09:17,340 --> 00:09:19,840
 capable of so much, but we take for

157
00:09:19,840 --> 00:09:23,970
 granted that the body is, we can do anything we want with

158
00:09:23,970 --> 00:09:24,960
 the body.

159
00:09:24,960 --> 00:09:27,200
 We take our legs for granted that we can walk.

160
00:09:27,200 --> 00:09:30,320
 We take our arms and our hands for granted that we can

161
00:09:30,320 --> 00:09:32,520
 manipulate all the many things,

162
00:09:32,520 --> 00:09:38,400
 anything that we like.

163
00:09:38,400 --> 00:09:41,970
 We take our eyes for granted that we can see our ears, that

164
00:09:41,970 --> 00:09:42,960
 we can hear.

165
00:09:42,960 --> 00:09:44,320
 We take our teeth for granted.

166
00:09:44,320 --> 00:09:47,640
 We take everything for granted.

167
00:09:47,640 --> 00:09:50,630
 You can see where I'm going with this that it's quite

168
00:09:50,630 --> 00:09:51,400
 misleading.

169
00:09:51,400 --> 00:09:54,530
 It's one of those many things, the body is one of many

170
00:09:54,530 --> 00:09:56,640
 things that we become incredibly

171
00:09:56,640 --> 00:10:04,000
 attached to, to our detriment, to our eventual detriment.

172
00:10:04,000 --> 00:10:06,890
 People think that old age, the suffering of old age is a

173
00:10:06,890 --> 00:10:08,440
 part of life and we just have

174
00:10:08,440 --> 00:10:09,440
 to bear with it.

175
00:10:09,440 --> 00:10:12,880
 Really the truth is we set ourselves up for it.

176
00:10:12,880 --> 00:10:16,990
 We set ourselves up for a great amount of fear and worry

177
00:10:16,990 --> 00:10:19,400
 and anxiety about growing old,

178
00:10:19,400 --> 00:10:23,400
 about getting sick, and about dying.

179
00:10:23,400 --> 00:10:28,390
 How many people are afraid of death, afraid of their own

180
00:10:28,390 --> 00:10:31,520
 death, or repulsed by the thought

181
00:10:31,520 --> 00:10:34,920
 of dying because of how intoxicated they are.

182
00:10:34,920 --> 00:10:41,670
 The body is such a great producer of chemicals as well and

183
00:10:41,670 --> 00:10:43,320
 pleasure.

184
00:10:43,320 --> 00:10:47,140
 When we run and jump and play and sing and dance, so many

185
00:10:47,140 --> 00:10:49,520
 wonderful hormones in the brain

186
00:10:49,520 --> 00:10:54,040
 become activated and give us such wonderful pleasure.

187
00:10:54,040 --> 00:11:00,600
 So we become intoxicated by this.

188
00:11:00,600 --> 00:11:04,810
 To our detriment, because we don't become happier, we don't

189
00:11:04,810 --> 00:11:06,960
 start to live happier lives,

190
00:11:06,960 --> 00:11:10,720
 we just get more and more addicted.

191
00:11:10,720 --> 00:11:17,160
 We don't smile more, we don't feel more at peace.

192
00:11:17,160 --> 00:11:19,040
 These things come from somewhere else.

193
00:11:19,040 --> 00:11:21,420
 These things come from wisdom and understanding and in fact

194
00:11:21,420 --> 00:11:22,040
 letting go.

195
00:11:22,040 --> 00:11:25,390
 That's why you see old people often quite at peace because

196
00:11:25,390 --> 00:11:27,080
 they've had to come to terms

197
00:11:27,080 --> 00:11:31,810
 and they've had to realize, "I can't get all this pleasure

198
00:11:31,810 --> 00:11:33,360
 that I wanted."

199
00:11:33,360 --> 00:11:35,950
 Old people, once they realize they can't have sexual

200
00:11:35,950 --> 00:11:37,720
 intercourse anymore, once they realize

201
00:11:37,720 --> 00:11:41,790
 they can't dance anymore, once all of these things fade

202
00:11:41,790 --> 00:11:46,160
 away, they become much more peaceful,

203
00:11:46,160 --> 00:11:49,220
 perhaps not happier because there still will be residual

204
00:11:49,220 --> 00:11:50,920
 addiction and they'll find other

205
00:11:50,920 --> 00:11:58,050
 ways to find pleasure but in general they have to come to

206
00:11:58,050 --> 00:12:01,840
 terms, which is a good thing

207
00:12:01,840 --> 00:12:04,440
 because as I said they tend to be more peaceful.

208
00:12:04,440 --> 00:12:07,120
 So a person who wants to find peace, we go through our

209
00:12:07,120 --> 00:12:09,240
 lives agitated, stressed, thinking

210
00:12:09,240 --> 00:12:13,020
 we're happy, thinking the body is bringing us happy, but

211
00:12:13,020 --> 00:12:15,240
 with such stress and suffering.

212
00:12:15,240 --> 00:12:19,200
 Whereas if we took this kind of teaching, even now, even as

213
00:12:19,200 --> 00:12:20,920
 young people, and we saw

214
00:12:20,920 --> 00:12:25,070
 the body for what it is, a nest of disease, a nest of

215
00:12:25,070 --> 00:12:28,040
 sickness, a place where sickness

216
00:12:28,040 --> 00:12:29,640
 grows, right?

217
00:12:29,640 --> 00:12:31,840
 It really is a nest.

218
00:12:31,840 --> 00:12:35,320
 These bacteria and viruses, they just love our body.

219
00:12:35,320 --> 00:12:37,440
 It's a breeding ground for them.

220
00:12:37,440 --> 00:12:40,280
 "Oh, look at this wonderful nest that has been put here.

221
00:12:40,280 --> 00:12:43,840
 It's just walking around touching things and picking up.

222
00:12:43,840 --> 00:12:46,120
 It makes me up, puts me in its mouth.

223
00:12:46,120 --> 00:12:52,880
 Oh, wonderful place to live and grow."

224
00:12:52,880 --> 00:12:56,040
 To our benefit, seeing this is not a pessimistic teaching.

225
00:12:56,040 --> 00:12:57,760
 It's not meant to make you feel awful.

226
00:12:57,760 --> 00:13:02,600
 It's in fact meant to free you from addiction to the body.

227
00:13:02,600 --> 00:13:07,120
 The body is such a curious thing, right?

228
00:13:07,120 --> 00:13:11,870
 Scientists have convinced us that it's just sort of by

229
00:13:11,870 --> 00:13:14,720
 natural selection, right?

230
00:13:14,720 --> 00:13:16,880
 We would argue.

231
00:13:16,880 --> 00:13:24,170
 Science has never been well able to take into account the

232
00:13:24,170 --> 00:13:25,320
 mind.

233
00:13:25,320 --> 00:13:29,650
 Just as I was preparing for this talk, one of my Sri Lankan

234
00:13:29,650 --> 00:13:31,360
 friends in Florida sent me

235
00:13:31,360 --> 00:13:34,860
 a quote we'd been arguing about science and whether science

236
00:13:34,860 --> 00:13:36,560
 actually says anything about

237
00:13:36,560 --> 00:13:37,560
 the nature of the world.

238
00:13:37,560 --> 00:13:42,640
 He sent me a Bohr quote, a quote of Niels Bohr.

239
00:13:42,640 --> 00:13:43,640
 Bohr is wonderful.

240
00:13:43,640 --> 00:13:47,280
 He says, "Science doesn't have physics."

241
00:13:47,280 --> 00:13:51,800
 He says, "There is no quantum world.

242
00:13:51,800 --> 00:13:55,840
 Physics doesn't attempt to explain the nature of reality.

243
00:13:55,840 --> 00:14:00,400
 It just describes our experience, really.

244
00:14:00,400 --> 00:14:07,680
 Our experience of it, our encounters with it, or whatever."

245
00:14:07,680 --> 00:14:11,370
 Why I bring that up is because it calls into question the

246
00:14:11,370 --> 00:14:13,400
 idea that the body even exists

247
00:14:13,400 --> 00:14:18,170
 or that this idea of natural selection is even the right

248
00:14:18,170 --> 00:14:20,640
 way of looking at things.

249
00:14:20,640 --> 00:14:22,520
 Certainly from a Buddhist point of view, we look at it

250
00:14:22,520 --> 00:14:23,360
 quite differently.

251
00:14:23,360 --> 00:14:25,400
 It's the mind playing a large part.

252
00:14:25,400 --> 00:14:27,600
 The mind did this, right?

253
00:14:27,600 --> 00:14:32,290
 We did this to ourselves, which sort of makes you look at

254
00:14:32,290 --> 00:14:34,360
 it in a different way.

255
00:14:34,360 --> 00:14:36,330
 When you look at the body as something we've done to

256
00:14:36,330 --> 00:14:38,040
 something, you say, "Oh, Jesus, this

257
00:14:38,040 --> 00:14:41,760
 is the best I could have done."

258
00:14:41,760 --> 00:14:47,840
 No titanium bones or the skin that was a little tougher.

259
00:14:47,840 --> 00:14:55,800
 Maybe this facial hair that could have done without that.

260
00:14:55,800 --> 00:14:57,760
 How much we would have done better.

261
00:14:57,760 --> 00:14:59,960
 Teeth, make my teeth a little stronger.

262
00:14:59,960 --> 00:15:02,520
 Sugar, what's up with this?

263
00:15:02,520 --> 00:15:05,280
 Sugar rots my teeth.

264
00:15:05,280 --> 00:15:07,280
 How puny.

265
00:15:07,280 --> 00:15:09,440
 Bacteria.

266
00:15:09,440 --> 00:15:12,020
 Bacteria get on your teeth and they eat the sugar and they

267
00:15:12,020 --> 00:15:12,440
 rot.

268
00:15:12,440 --> 00:15:13,440
 They corrode.

269
00:15:13,440 --> 00:15:16,520
 They corrode the enamel of your teeth.

270
00:15:16,520 --> 00:15:20,800
 What a worthless, poorly made instrument.

271
00:15:20,800 --> 00:15:26,080
 It gets old, sick and dies.

272
00:15:26,080 --> 00:15:29,320
 There are machines.

273
00:15:29,320 --> 00:15:33,360
 Machines that we make now will last longer than this body.

274
00:15:33,360 --> 00:15:38,120
 But the body is a manifestation of all that we are.

275
00:15:38,120 --> 00:15:40,280
 It's pretty pitiful, really.

276
00:15:40,280 --> 00:15:41,960
 This is all we are.

277
00:15:41,960 --> 00:15:44,760
 We don't think like this, but we should.

278
00:15:44,760 --> 00:15:48,040
 Couldn't I have done better?

279
00:15:48,040 --> 00:15:49,040
 You can.

280
00:15:49,040 --> 00:15:50,600
 Angels do better.

281
00:15:50,600 --> 00:15:51,600
 Gods do better.

282
00:15:51,600 --> 00:15:53,320
 Wow, their bodies must be wonderful.

283
00:15:53,320 --> 00:16:00,960
 They can always become an angel or a god.

284
00:16:00,960 --> 00:16:04,370
 These are different ways we can, there's so much that we

285
00:16:04,370 --> 00:16:06,160
 can learn from this idea, this

286
00:16:06,160 --> 00:16:08,400
 way of looking at the world.

287
00:16:08,400 --> 00:16:12,040
 They talk about the nature of the body for a long time.

288
00:16:12,040 --> 00:16:14,100
 But to delve right into how it's useful for us as medit

289
00:16:14,100 --> 00:16:15,520
ators, I think that's the most

290
00:16:15,520 --> 00:16:16,520
 important.

291
00:16:16,520 --> 00:16:22,120
 I think this is sort of a good description of the sort of

292
00:16:22,120 --> 00:16:25,480
 thing that you'll come to understand

293
00:16:25,480 --> 00:16:26,480
 through meditation.

294
00:16:26,480 --> 00:16:30,560
 You see, that the body, the body is how we contact, the

295
00:16:30,560 --> 00:16:33,600
 body is how we experience sensations,

296
00:16:33,600 --> 00:16:38,520
 the body is responsible for much of our pleasure and pain,

297
00:16:38,520 --> 00:16:39,400
 right?

298
00:16:39,400 --> 00:16:45,190
 Well, for all of the physical pleasure and physical pain

299
00:16:45,190 --> 00:16:47,600
 that we experience.

300
00:16:47,600 --> 00:16:52,300
 We come to see that the body is something we have to carry

301
00:16:52,300 --> 00:16:53,920
 around with us.

302
00:16:53,920 --> 00:16:56,330
 When you meditate, it would be nice if we could meditate

303
00:16:56,330 --> 00:16:57,840
 and leave our bodies, right?

304
00:16:57,840 --> 00:17:01,320
 Do some transcendental meditation.

305
00:17:01,320 --> 00:17:03,590
 The more you meditate when you're here doing a meditation

306
00:17:03,590 --> 00:17:05,400
 course, you start to realize,

307
00:17:05,400 --> 00:17:07,600
 "This body isn't something useful.

308
00:17:07,600 --> 00:17:10,080
 It's something that I have to care for and take care of.

309
00:17:10,080 --> 00:17:12,000
 All I want to do is meditate and look.

310
00:17:12,000 --> 00:17:13,000
 I have to go and eat.

311
00:17:13,000 --> 00:17:14,000
 I have to sleep.

312
00:17:14,000 --> 00:17:15,280
 I have to adjust my position.

313
00:17:15,280 --> 00:17:17,280
 I can't just sit.

314
00:17:17,280 --> 00:17:20,040
 I have to get up and walk.

315
00:17:20,040 --> 00:17:21,040
 I can't walk forever.

316
00:17:21,040 --> 00:17:22,040
 I have to sit down."

317
00:17:22,040 --> 00:17:25,520
 I itch.

318
00:17:25,520 --> 00:17:26,520
 It gets hot.

319
00:17:26,520 --> 00:17:27,520
 It gets cold.

320
00:17:27,520 --> 00:17:28,520
 I urinate.

321
00:17:28,520 --> 00:17:29,520
 I defecate.

322
00:17:29,520 --> 00:17:31,520
 Sometimes I get sick.

323
00:17:31,520 --> 00:17:39,170
 Never mind if you fall down and break a limb or get cut,

324
00:17:39,170 --> 00:17:43,800
 get bruised, poke an eye out.

325
00:17:43,800 --> 00:17:49,920
 So many things can happen to this body.

326
00:17:49,920 --> 00:17:52,280
 So as meditators, this sort of thing comes up and I think

327
00:17:52,280 --> 00:17:53,480
 talking about it is always

328
00:17:53,480 --> 00:17:55,780
 talking about the things that we might experience in

329
00:17:55,780 --> 00:17:57,960
 meditation or should experience in meditation

330
00:17:57,960 --> 00:17:59,680
 is quite useful.

331
00:17:59,680 --> 00:18:02,130
 Because when we do realize them, it helps reaffirm what we

332
00:18:02,130 --> 00:18:03,400
're experiencing and clarify

333
00:18:03,400 --> 00:18:04,400
 it for us.

334
00:18:04,400 --> 00:18:08,920
 "Yeah, that is what I'm seeing."

335
00:18:08,920 --> 00:18:12,060
 Realizing that this body is not quite what I thought it was

336
00:18:12,060 --> 00:18:12,320
.

337
00:18:12,320 --> 00:18:15,720
 Realizing how silly we were to think of how wonderful this

338
00:18:15,720 --> 00:18:16,440
 body is.

339
00:18:16,440 --> 00:18:17,440
 It's so silly.

340
00:18:17,440 --> 00:18:22,960
 And you want to know how silly it is.

341
00:18:22,960 --> 00:18:25,880
 Dogs think the same things about their bodies.

342
00:18:25,880 --> 00:18:29,320
 They look at our bodies and say, "That's ugly."

343
00:18:29,320 --> 00:18:30,320
 But their own bodies.

344
00:18:30,320 --> 00:18:31,320
 Wow, those are beautiful.

345
00:18:31,320 --> 00:18:36,320
 And bodies of other dogs.

346
00:18:36,320 --> 00:18:38,320
 Rats.

347
00:18:38,320 --> 00:18:40,320
 Bugs.

348
00:18:40,320 --> 00:18:41,880
 Insects.

349
00:18:41,880 --> 00:18:43,540
 Insects will look at each other and think, "Wow, you're

350
00:18:43,540 --> 00:18:44,080
 beautiful."

351
00:18:44,080 --> 00:18:46,160
 They'll look at their bodies.

352
00:18:46,160 --> 00:18:48,660
 If you ever know birds, birds are wonderful in this way,

353
00:18:48,660 --> 00:18:50,320
 always preening their feathers.

354
00:18:50,320 --> 00:18:53,880
 I think they have to do it to fly as well.

355
00:18:53,880 --> 00:18:59,080
 But it's pretty clear that they're in their vein.

356
00:18:59,080 --> 00:19:03,000
 How attached they are to their bodies.

357
00:19:03,000 --> 00:19:06,080
 Flee infested bodies.

358
00:19:06,080 --> 00:19:10,080
 Smelly, dirty bodies.

359
00:19:10,080 --> 00:19:13,920
 Yeah, we don't have to be negative.

360
00:19:13,920 --> 00:19:14,920
 That is so negative.

361
00:19:14,920 --> 00:19:17,240
 In fact, that's all conceptual.

362
00:19:17,240 --> 00:19:20,400
 Body is even all conceptual.

363
00:19:20,400 --> 00:19:24,140
 On a deep level in our meditation practice, this teaching

364
00:19:24,140 --> 00:19:26,040
 has to be taken on a momentary

365
00:19:26,040 --> 00:19:27,480
 level.

366
00:19:27,480 --> 00:19:31,040
 So the real destruction of the body is every moment.

367
00:19:31,040 --> 00:19:34,720
 When you extend your arm, that arises.

368
00:19:34,720 --> 00:19:36,720
 When you stop, it ceases.

369
00:19:36,720 --> 00:19:39,640
 When you flex your arm, you're born and die.

370
00:19:39,640 --> 00:19:43,680
 That is what we mean by body.

371
00:19:43,680 --> 00:19:47,520
 It arises and it ceases in that moment.

372
00:19:47,520 --> 00:19:52,280
 When you feel pain, physical pain, that's an experience

373
00:19:52,280 --> 00:19:54,520
 that arises and ceases.

374
00:19:54,520 --> 00:19:58,580
 So we come to this sort of thing that Bohr came to, that

375
00:19:58,580 --> 00:20:00,880
 this quantum physicist came

376
00:20:00,880 --> 00:20:04,080
 to, the world doesn't really exist.

377
00:20:04,080 --> 00:20:12,720
 All that we can say about the world is what we experience.

378
00:20:12,720 --> 00:20:15,240
 So we have to work on both these levels.

379
00:20:15,240 --> 00:20:19,550
 We have to work on the conceptual level to help us see

380
00:20:19,550 --> 00:20:23,200
 through our attachments to concepts.

381
00:20:23,200 --> 00:20:30,150
 Once we see that the body is not a source of refuge, we see

382
00:20:30,150 --> 00:20:34,440
 that it's subject to dissolution

383
00:20:34,440 --> 00:20:35,440
 and impermanence.

384
00:20:35,440 --> 00:20:39,670
 When we see deeper that it doesn't actually even exist,

385
00:20:39,670 --> 00:20:41,960
 that we die every moment, the

386
00:20:41,960 --> 00:20:47,200
 body is born and dies with every experience.

387
00:20:47,200 --> 00:20:48,560
 Then we really let go of the body.

388
00:20:48,560 --> 00:20:52,080
 I mean, that's where the greatest peace comes from.

389
00:20:52,080 --> 00:20:54,760
 Because when you see the body as not even existing, it's

390
00:20:54,760 --> 00:20:56,480
 really just experiences arising

391
00:20:56,480 --> 00:20:59,600
 and ceasing.

392
00:20:59,600 --> 00:21:01,800
 Then old age sickness and death can't come to you.

393
00:21:01,800 --> 00:21:05,850
 And that's a real good reason to, it gives us impetus to

394
00:21:05,850 --> 00:21:06,920
 practice.

395
00:21:06,920 --> 00:21:09,540
 When we realize that our attachment to the body is a real

396
00:21:09,540 --> 00:21:10,960
 problem, we're going to die

397
00:21:10,960 --> 00:21:11,960
 soon.

398
00:21:11,960 --> 00:21:15,350
 We're going to get old, we're going to get sick and we're

399
00:21:15,350 --> 00:21:16,400
 going to die.

400
00:21:16,400 --> 00:21:18,970
 Much to our benefit if we can practice meditation and learn

401
00:21:18,970 --> 00:21:20,320
 to let go of it and learn to see

402
00:21:20,320 --> 00:21:25,950
 through it, learn to see this truth that the body doesn't

403
00:21:25,950 --> 00:21:27,440
 even exist.

404
00:21:27,440 --> 00:21:31,060
 Then when it gets old, sick and dies, you don't lose

405
00:21:31,060 --> 00:21:32,040
 anything.

406
00:21:32,040 --> 00:21:33,960
 People say Buddhism is nihilistic.

407
00:21:33,960 --> 00:21:36,440
 Buddhism is denialistic, I suppose.

408
00:21:36,440 --> 00:21:40,810
 We deny the existence of so many things that most people

409
00:21:40,810 --> 00:21:43,440
 take for granted as existing.

410
00:21:43,440 --> 00:21:46,100
 I think that Bohr quote is more apt than he perhaps

411
00:21:46,100 --> 00:21:48,200
 understood, although I think he was

412
00:21:48,200 --> 00:21:55,280
 a rather wise individual in that it's really only

413
00:21:55,280 --> 00:21:59,360
 experience that exists.

414
00:21:59,360 --> 00:22:02,760
 So there you go.

415
00:22:02,760 --> 00:22:04,520
 That's the Dhammapada for this evening.

416
00:22:04,520 --> 00:22:07,960
 That's our Dhammapada talk for today.

417
00:22:07,960 --> 00:22:11,360
 Wishing you all good practice and a peaceful, happy life.

418
00:22:11,360 --> 00:22:12,360
 Thank you.

