WEBVTT

00:00:00.000 --> 00:00:05.230
 Something the Buddha said that we should often remind

00:00:05.230 --> 00:00:08.300
 ourselves of is one of those pithi

00:00:08.300 --> 00:00:17.390
 sayings that last long after the person who said them is

00:00:17.390 --> 00:00:18.880
 gone.

00:00:18.880 --> 00:00:21.510
 One such thing, and I can't say whether it was the Buddha

00:00:21.510 --> 00:00:24.240
 who first said it, it was probably

00:00:24.240 --> 00:00:30.100
 the sort of thing that you hear from many people, but may

00:00:30.100 --> 00:00:32.960
 well have been one of those

00:00:32.960 --> 00:00:39.160
 sayings that came from the Buddha originally is "Kanno hu

00:00:39.160 --> 00:00:42.560
 ma upatyaka."

00:00:42.560 --> 00:00:45.760
 Don't let the moment pass you by.

00:00:45.760 --> 00:00:56.480
 "Ganno hu ma upatyaka" means moment, "woe" means you, "woe"

00:00:56.480 --> 00:00:58.560
 here is accusative.

00:00:58.560 --> 00:01:07.240
 "Ma upatyaka" means don't let the moment not overcome you

00:01:07.240 --> 00:01:11.360
 or go beyond you.

00:01:11.360 --> 00:01:21.250
 May the moment not pass you by, don't let the moment pass

00:01:21.250 --> 00:01:23.040
 you by.

00:01:23.040 --> 00:01:33.840
 Time moves really quickly, life moves quickly.

00:01:33.840 --> 00:01:39.670
 When we look back, it's frightening how quickly it's all

00:01:39.670 --> 00:01:40.600
 gone.

00:01:40.600 --> 00:01:46.840
 Those people getting nostalgic, thinking about where their

00:01:46.840 --> 00:01:52.200
 lives have gone, disappeared in

00:01:52.200 --> 00:01:59.040
 the infinity of the past.

00:01:59.040 --> 00:02:06.000
 How quickly the days and weeks and months and years go by.

00:02:06.000 --> 00:02:19.840
 And even that is not the least of it, that's not the end of

00:02:19.840 --> 00:02:21.960
 it.

00:02:21.960 --> 00:02:28.030
 Even our own life, our whole life is just a moment, a brief

00:02:28.030 --> 00:02:30.000
 moment in time.

00:02:30.000 --> 00:02:48.490
 There was a story of an angel, a couple who were in heaven

00:02:48.490 --> 00:02:52.360
 and the wife passed away from

00:02:52.360 --> 00:02:58.360
 heaven and was born as a human being in the morning, one

00:02:58.360 --> 00:03:02.840
 fine morning they were out, frolicking

00:03:02.840 --> 00:03:05.510
 and enjoying themselves in heaven and the wife suddenly

00:03:05.510 --> 00:03:06.160
 passed away.

00:03:06.160 --> 00:03:09.650
 I can't remember the whole story, it's one of these famous

00:03:09.650 --> 00:03:11.000
 Buddhist stories.

00:03:11.000 --> 00:03:15.480
 My memory is so bad.

00:03:15.480 --> 00:03:26.000
 And so the woman was born as a young girl and grew up and

00:03:26.000 --> 00:03:27.400
 remembered her past life as an

00:03:27.400 --> 00:03:42.720
 angel and realized that she had lost her glory in heaven.

00:03:42.720 --> 00:03:47.160
 And so she worked all her life to try and do good deeds.

00:03:47.160 --> 00:03:51.070
 She got married, had children and she was about 60 or 70 I

00:03:51.070 --> 00:03:52.980
 think when she finally passed

00:03:52.980 --> 00:03:59.280
 away.

00:03:59.280 --> 00:04:04.430
 And she was born right away in heaven again, except now it

00:04:04.430 --> 00:04:07.200
 was the afternoon, it was the

00:04:07.200 --> 00:04:08.920
 afternoon of the same day.

00:04:08.920 --> 00:04:13.810
 So the husband sees her and says, "Oh, where did you go

00:04:13.810 --> 00:04:15.480
 this morning?"

00:04:15.480 --> 00:04:19.080
 And here she had spent a whole lifetime down on earth.

00:04:19.080 --> 00:04:24.820
 That's how it is, that's what they say, the difference

00:04:24.820 --> 00:04:27.600
 between heaven and earth and of

00:04:27.600 --> 00:04:37.090
 course the different levels of heaven and so on are yet

00:04:37.090 --> 00:04:42.640
 longer and longer in length.

00:04:42.640 --> 00:04:47.760
 One human lifetime is just a day in the lower heavens and

00:04:47.760 --> 00:04:50.280
 in the higher heavens, one day

00:04:50.280 --> 00:04:58.270
 in those heavens is only a day in the higher heavens and so

00:04:58.270 --> 00:04:59.160
 on.

00:04:59.160 --> 00:05:03.320
 More and more insanely long.

00:05:03.320 --> 00:05:08.140
 In the Brahma world, in the God realms, they forget that

00:05:08.140 --> 00:05:10.760
 they were born in these realms

00:05:10.760 --> 00:05:13.080
 and they think they're eternal.

00:05:13.080 --> 00:05:14.880
 It's so long.

00:05:14.880 --> 00:05:21.840
 They just have no ability to recollect back that far.

00:05:21.840 --> 00:05:25.400
 So they think that they're eternal.

00:05:25.400 --> 00:05:29.800
 Very easy to think you're eternal when you live so long.

00:05:29.800 --> 00:05:35.200
 Very difficult to see impermanent suffering and non-self.

00:05:35.200 --> 00:05:36.200
 Impossible they say.

00:05:36.200 --> 00:05:40.000
 In these realms it's impossible to realize the Dhamma

00:05:40.000 --> 00:05:43.960
 because there's no ability to comprehend

00:05:43.960 --> 00:05:48.760
 the three characteristics or something like that.

00:05:48.760 --> 00:05:53.920
 The point being, time moves pretty quickly.

00:05:53.920 --> 00:05:59.710
 It moves so quickly that you think it actually really

00:05:59.710 --> 00:06:01.640
 stands still.

00:06:01.640 --> 00:06:02.640
 It's like a paradox.

00:06:02.640 --> 00:06:06.440
 It gets so fast that you think it's really meaningless.

00:06:06.440 --> 00:06:11.070
 You think how long we've lived our lives and it's so

00:06:11.070 --> 00:06:13.800
 pointless, it's so nothing, such

00:06:13.800 --> 00:06:27.040
 a blink in the stream of time.

00:06:27.040 --> 00:06:32.510
 And given that time is eternal, you think, well, actually

00:06:32.510 --> 00:06:36.200
 it doesn't matter whether I

00:06:36.200 --> 00:06:38.970
 catch this moment at all because there's more moments after

00:06:38.970 --> 00:06:39.280
 it.

00:06:39.280 --> 00:06:51.160
 If it's eternal then there's no reason to concern ourselves

00:06:51.160 --> 00:06:51.360
.

00:06:51.360 --> 00:06:54.440
 Which is kind of true.

00:06:54.440 --> 00:06:58.360
 You don't have to practice meditation.

00:06:58.360 --> 00:07:02.920
 You don't have to cultivate good deeds.

00:07:02.920 --> 00:07:06.250
 This woman, she did great deeds and was born in heaven and

00:07:06.250 --> 00:07:07.240
 then lost it all.

00:07:07.240 --> 00:07:11.680
 And then she had to spend her whole life just to get it all

00:07:11.680 --> 00:07:12.520
 again.

00:07:12.520 --> 00:07:16.280
 Her whole life was so much work and effort just to get back

00:07:16.280 --> 00:07:17.720
 in the afternoon.

00:07:17.720 --> 00:07:21.040
 So it was like nothing to her husband.

00:07:21.040 --> 00:07:24.830
 I looked for her during the day and then in the afternoon

00:07:24.830 --> 00:07:26.040
 she came back.

00:07:26.040 --> 00:07:27.440
 Where did you go this evening?

00:07:27.440 --> 00:07:28.440
 I thought it was nothing.

00:07:28.440 --> 00:07:29.440
 Where did you go this morning?

00:07:29.440 --> 00:07:31.960
 So she had only been gone a short time.

00:07:31.960 --> 00:07:39.980
 Her whole life was not meaningless but really, really very

00:07:39.980 --> 00:07:42.640
 insignificant.

00:07:42.640 --> 00:07:48.890
 And of course even having done all those good deeds, her

00:07:48.890 --> 00:07:53.560
 stay in heaven would not be permanent.

00:07:53.560 --> 00:07:56.690
 But there are some reasons to argue against this idea that

00:07:56.690 --> 00:07:58.200
 we should take things easy

00:07:58.200 --> 00:08:07.310
 and let things go and not concern ourselves about making

00:08:07.310 --> 00:08:09.640
 use of the time that we have.

00:08:09.640 --> 00:08:13.610
 The first one is that we're born as human beings which is

00:08:13.610 --> 00:08:17.320
 not an overly common occurrence.

00:08:17.320 --> 00:08:23.200
 Very difficult for ordinary animals to be born as humans.

00:08:23.200 --> 00:08:27.720
 Much easier for them to go round and round.

00:08:27.720 --> 00:08:29.720
 Being born an animal is not difficult.

00:08:29.720 --> 00:08:34.690
 It doesn't take brains, it doesn't take goodness, it doesn

00:08:34.690 --> 00:08:37.280
't take anything, any sort of special

00:08:37.280 --> 00:08:38.280
 quality.

00:08:38.280 --> 00:08:43.160
 All you have to do is be stupid.

00:08:43.160 --> 00:08:47.350
 If you have no intelligence, no wisdom, if you don't care

00:08:47.350 --> 00:08:48.880
 about good things, it's very

00:08:48.880 --> 00:08:53.880
 easy to be born as an animal.

00:08:53.880 --> 00:09:00.280
 But to be born as a human is a very special type of animal.

00:09:00.280 --> 00:09:06.340
 Humans can go against their own genes, go against their own

00:09:06.340 --> 00:09:08.120
 programming.

00:09:08.120 --> 00:09:15.640
 Humans have the ability that ordinary animals rarely if

00:09:15.640 --> 00:09:18.520
 ever exhibit.

00:09:18.520 --> 00:09:29.960
 And that is to reprogram themselves, to question their own

00:09:29.960 --> 00:09:41.560
 intention and to reprogram their

00:09:41.560 --> 00:09:45.320
 habits and their behaviors.

00:09:45.320 --> 00:09:50.140
 Humans have the potential to become totally free from bad

00:09:50.140 --> 00:09:52.760
 habits, from unwholesome mind

00:09:52.760 --> 00:09:53.760
 states.

00:09:53.760 --> 00:10:03.040
 The ordinary animals don't have this.

00:10:03.040 --> 00:10:05.760
 So who knows where we're going to go in the next life.

00:10:05.760 --> 00:10:11.340
 If we do good deeds we'll maybe go to heaven or maybe be

00:10:11.340 --> 00:10:14.680
 born again as a human being but

00:10:14.680 --> 00:10:17.480
 who knows.

00:10:17.480 --> 00:10:24.040
 If we also do bad deeds, if we're lazy and careless and don

00:10:24.040 --> 00:10:26.840
't concern ourselves about

00:10:26.840 --> 00:10:29.800
 doing good deeds, let our bad deeds show as well.

00:10:29.800 --> 00:10:33.520
 And who knows where we'll go.

00:10:33.520 --> 00:10:40.270
 We might wind up in great suffering and get caught up in

00:10:40.270 --> 00:10:41.880
 the trap.

00:10:41.880 --> 00:10:46.870
 Much again is all fine and good because maybe billions and

00:10:46.870 --> 00:10:49.680
 billions and billions and trillions

00:10:49.680 --> 00:10:53.020
 of years from now or lifetimes from now we might have

00:10:53.020 --> 00:10:55.160
 another chance to be born a human

00:10:55.160 --> 00:10:56.160
 being.

00:10:56.160 --> 00:10:57.160
 So it's all good.

00:10:57.160 --> 00:11:01.720
 That's how you want to live.

00:11:01.720 --> 00:11:05.850
 But given the choice it does make sense that we should

00:11:05.850 --> 00:11:08.680
 capitalize on the opportunity that

00:11:08.680 --> 00:11:15.280
 we have to be born a human being.

00:11:15.280 --> 00:11:19.400
 The second is not only we're born a human being but we're

00:11:19.400 --> 00:11:21.160
 born in a time when Buddhism

00:11:21.160 --> 00:11:24.920
 is to be found in the world.

00:11:24.920 --> 00:11:28.040
 We take this for granted because Buddhism is very easy to

00:11:28.040 --> 00:11:28.400
 do.

00:11:28.400 --> 00:11:31.280
 We take it for granted because we're good at taking things

00:11:31.280 --> 00:11:32.080
 for granted.

00:11:32.080 --> 00:11:33.720
 We take for granted whatever we have.

00:11:33.720 --> 00:11:36.180
 This is why the world is such a mess.

00:11:36.180 --> 00:11:39.820
 We have such great luxury and opulence in the human realm

00:11:39.820 --> 00:11:42.000
 that we've taken it for granted

00:11:42.000 --> 00:11:44.160
 and now it's disappearing.

00:11:44.160 --> 00:11:46.520
 The trees are disappearing.

00:11:46.520 --> 00:11:49.200
 The weather's getting bad.

00:11:49.200 --> 00:11:50.920
 The air is becoming poisonous.

00:11:50.920 --> 00:11:55.400
 The water is poisonous.

00:11:55.400 --> 00:11:57.080
 This is what we're taking for granted.

00:11:57.080 --> 00:12:04.480
 This is the animal side of us, this parasitic nature of

00:12:04.480 --> 00:12:09.000
 existence that you see in animals

00:12:09.000 --> 00:12:10.000
 as well.

00:12:10.000 --> 00:12:11.490
 The only reason animals don't do this is because they're

00:12:11.490 --> 00:12:12.120
 not smart enough.

00:12:12.120 --> 00:12:16.520
 They don't have the brains to take over the way we do.

00:12:16.520 --> 00:12:20.400
 It's not that they're more conscious of it.

00:12:20.400 --> 00:12:26.230
 In fact, if you see what animals do do when they have the

00:12:26.230 --> 00:12:29.480
 chance, how prone they are to

00:12:29.480 --> 00:12:33.860
 overrunning and destroying and wreaking havoc on the

00:12:33.860 --> 00:12:37.160
 environment, the only reason they can

00:12:37.160 --> 00:12:42.440
 is that they don't have the same intellectual capabilities

00:12:42.440 --> 00:12:43.880
 as humans do.

00:12:43.880 --> 00:12:47.480
 We take a lot for granted and so we lose it.

00:12:47.480 --> 00:12:51.960
 We take it for granted so we don't care for it.

00:12:51.960 --> 00:12:55.320
 We do the same with the Buddhist teaching.

00:12:55.320 --> 00:12:58.530
 We take it for granted and so we don't care for it and

00:12:58.530 --> 00:13:00.520
 slowly it's wasting away.

00:13:00.520 --> 00:13:05.840
 Now, often all people care about our very simple teachings

00:13:05.840 --> 00:13:07.960
 of the Buddha or stories

00:13:07.960 --> 00:13:13.830
 or maybe only interested in it philosophically or

00:13:13.830 --> 00:13:16.240
 intellectually.

00:13:16.240 --> 00:13:19.200
 In many Buddhist countries they're only interested in memor

00:13:19.200 --> 00:13:23.440
izing it so they can teach it to others.

00:13:23.440 --> 00:13:26.520
 But we're not caring for it.

00:13:26.520 --> 00:13:28.000
 Or we're less caring for it.

00:13:28.000 --> 00:13:30.200
 And of course we've always been this way.

00:13:30.200 --> 00:13:34.750
 Even in the Buddhist time, people didn't really care for

00:13:34.750 --> 00:13:38.640
 the Dhamma the way they should have

00:13:38.640 --> 00:13:43.800
 and so it deteriorated.

00:13:43.800 --> 00:13:47.310
 But the point here is it's still here and there still are

00:13:47.310 --> 00:13:49.640
 people caring for the Dhamma,

00:13:49.640 --> 00:13:54.660
 practicing it, concerning themselves with cultivating the D

00:13:54.660 --> 00:14:00.400
hamma within them and becoming

00:14:00.400 --> 00:14:02.720
 practitioners of the Dhamma.

00:14:02.720 --> 00:14:08.240
 And so we're all fortunate enough to have this greatness,

00:14:08.240 --> 00:14:10.480
 this goodness, this truth

00:14:10.480 --> 00:14:15.920
 in the world, this teaching in the world.

00:14:15.920 --> 00:14:16.920
 And it's not going to last.

00:14:16.920 --> 00:14:19.640
 Who knows how much longer it will last?

00:14:19.640 --> 00:14:25.700
 The story goes that we've got another 2500 years which is

00:14:25.700 --> 00:14:28.520
 of course very little time.

00:14:28.520 --> 00:14:31.910
 Especially considering the rest of it might be spent as a

00:14:31.910 --> 00:14:33.560
 dog or a horse or a dung beetle.

00:14:33.560 --> 00:14:36.680
 We don't know where we're going next.

00:14:36.680 --> 00:14:40.970
 But now we have it and if we don't capitalize on this

00:14:40.970 --> 00:14:44.040
 opportunity that we've got, it's

00:14:44.040 --> 00:14:47.790
 going to be over sooner than we can think, sooner than we

00:14:47.790 --> 00:14:48.560
 know it.

00:14:48.560 --> 00:14:52.280
 This is why I'm always encouraging people to ordain.

00:14:52.280 --> 00:14:55.800
 I mean it seems like a no brainer to me.

00:14:55.800 --> 00:14:59.090
 It's not like I'm saying everyone should ordain or it's the

00:14:59.090 --> 00:15:00.880
 way of the world to ordain.

00:15:00.880 --> 00:15:04.980
 But look we've got this very special opportunity, this is

00:15:04.980 --> 00:15:09.240
 like a once in a bazillion year opportunity.

00:15:09.240 --> 00:15:11.470
 Who knows when we'll get this chance again to be born a

00:15:11.470 --> 00:15:12.800
 human being in the time of the

00:15:12.800 --> 00:15:14.800
 Munda.

00:15:14.800 --> 00:15:19.280
 Come, let's put our heart into this.

00:15:19.280 --> 00:15:23.490
 Let's work together and try to purify our minds, try to get

00:15:23.490 --> 00:15:26.080
 rid of these evil, unwholesome,

00:15:26.080 --> 00:15:31.080
 unskillful tendencies that exist inside of us.

00:15:31.080 --> 00:15:32.640
 Let's work on this.

00:15:32.640 --> 00:15:37.080
 Let's put our hearts into it.

00:15:37.080 --> 00:15:42.200
 It's the best chance we have.

00:15:42.200 --> 00:15:45.600
 Later on if we didn't, who knows what comes next.

00:15:45.600 --> 00:15:50.440
 But now we have this.

00:15:50.440 --> 00:15:54.240
 The Buddha's teaching here is something that is perfect and

00:15:54.240 --> 00:15:56.240
 pure and really and truly leads

00:15:56.240 --> 00:16:02.120
 to peace, happiness and freedom from suffering.

00:16:02.120 --> 00:16:08.720
 So this is the second reason.

00:16:08.720 --> 00:16:18.430
 The third reason is because we are actually, because we

00:16:18.430 --> 00:16:19.440
 actually have the opportunity,

00:16:19.440 --> 00:16:25.320
 we have the chance to practice the Buddha's teaching.

00:16:25.320 --> 00:16:33.210
 So in many cases there is no opportunity even for people

00:16:33.210 --> 00:16:35.040
 who, well for all the beings who

00:16:35.040 --> 00:16:39.050
 are born in the world, very few of them are engaged

00:16:39.050 --> 00:16:42.400
 actively in the practice of the Buddha's

00:16:42.400 --> 00:16:44.400
 teaching.

00:16:44.400 --> 00:16:48.730
 So many people are caught up in other religions, for

00:16:48.730 --> 00:16:51.800
 example, which we would consider to be

00:16:51.800 --> 00:16:56.080
 wrong view because they are caught up in views and beliefs.

00:16:56.080 --> 00:17:00.700
 They're caught up in, they're dedicated to beliefs like if

00:17:00.700 --> 00:17:02.640
 you believe in X you will

00:17:02.640 --> 00:17:09.640
 go to heaven, if you don't believe in X you'll go to hell,

00:17:09.640 --> 00:17:13.120
 if you do these rituals you will

00:17:13.120 --> 00:17:16.060
 be free for eternity and so on.

00:17:16.060 --> 00:17:19.090
 So many people are of the belief that when you die there's

00:17:19.090 --> 00:17:19.800
 nothing.

00:17:19.800 --> 00:17:23.680
 When you die the mind stops.

00:17:23.680 --> 00:17:29.850
 All of these we would consider to be wrong view because

00:17:29.850 --> 00:17:33.360
 they're based on speculation

00:17:33.360 --> 00:17:41.440
 or faith or brainwashing in many cases.

00:17:41.440 --> 00:17:47.750
 So it's rare to find people who get or appreciate the idea

00:17:47.750 --> 00:17:52.120
 of not just scientific investigation

00:17:52.120 --> 00:17:56.510
 but personal scientific investigation, investigation of

00:17:56.510 --> 00:17:59.200
 your own reality, not just reality on an

00:17:59.200 --> 00:18:03.380
 impersonal level but your own existence, your own

00:18:03.380 --> 00:18:04.720
 experience.

00:18:04.720 --> 00:18:12.680
 It's very rare to find, but it's rare to find the

00:18:12.680 --> 00:18:18.240
 opportunity even for those people who

00:18:18.240 --> 00:18:19.240
 want to.

00:18:19.240 --> 00:18:22.760
 So first of all it's the opportunity of having your mind

00:18:22.760 --> 00:18:25.480
 set on it, second of all the opportunity

00:18:25.480 --> 00:18:33.390
 of being physically located or physically situated in all

00:18:33.390 --> 00:18:36.840
 aspects such that you can

00:18:36.840 --> 00:18:47.880
 take advantage of this right view, this good intention to

00:18:47.880 --> 00:18:52.460
 practice the dhamma.

00:18:52.460 --> 00:19:00.240
 So here we are well situated, we have the texts, we have

00:19:00.240 --> 00:19:04.640
 the thoughts in our mind, the intentions

00:19:04.640 --> 00:19:09.470
 in our mind, we even have what looks to be a community of

00:19:09.470 --> 00:19:12.520
 meditators, people interested

00:19:12.520 --> 00:19:13.520
 in meditation.

00:19:13.520 --> 00:19:18.270
 So we have here daily the chance to at least do some basic

00:19:18.270 --> 00:19:20.940
 group meditation and practice

00:19:20.940 --> 00:19:31.720
 and study and dedication to the dhamma.

00:19:31.720 --> 00:19:38.630
 So there's another reason to take this moment seriously is

00:19:38.630 --> 00:19:41.240
 that we have not only is the

00:19:41.240 --> 00:19:45.190
 Buddha's teaching in the world but it's here for us, it's

00:19:45.190 --> 00:19:46.480
 right with us.

00:19:46.480 --> 00:19:51.660
 All we have to do is start practicing, all of the factors

00:19:51.660 --> 00:19:53.200
 are in place.

00:19:53.200 --> 00:20:01.950
 We're not starving, we're not in debt or slaves to, we're

00:20:01.950 --> 00:20:08.680
 not enslaved to anyone and so on.

00:20:08.680 --> 00:20:13.480
 We're not in danger of our lives or our health, we have all

00:20:13.480 --> 00:20:16.400
 these qualities and then moreover

00:20:16.400 --> 00:20:18.720
 we have a meditation group.

00:20:18.720 --> 00:20:22.910
 We have a group of people who are of like mind, we have a

00:20:22.910 --> 00:20:25.640
 place and we have the opportunity

00:20:25.640 --> 00:20:27.640
 to practice.

00:20:27.640 --> 00:20:32.780
 This is the third reason, the fourth reason is that well we

00:20:32.780 --> 00:20:35.440
've taken the opportunity and

00:20:35.440 --> 00:20:42.750
 here we are right, ready to practice, ready to do this

00:20:42.750 --> 00:20:44.560
 practice.

00:20:44.560 --> 00:20:49.080
 So the fourth reason is, it's not exactly a reason but it's

00:20:49.080 --> 00:20:51.080
 the fourth is how great this

00:20:51.080 --> 00:20:54.380
 moment is, why this moment is special is because we've

00:20:54.380 --> 00:20:56.800
 actually not only got the opportunity

00:20:56.800 --> 00:20:58.920
 but we've taken it.

00:20:58.920 --> 00:21:02.650
 So many people have the opportunity to come here tonight

00:21:02.650 --> 00:21:04.920
 for example and many people don't

00:21:04.920 --> 00:21:08.270
 but certainly there are people who know about it and have

00:21:08.270 --> 00:21:10.240
 for some reason rather decided

00:21:10.240 --> 00:21:13.320
 not to come.

00:21:13.320 --> 00:21:20.750
 And there will always be this, we will always be, at times

00:21:20.750 --> 00:21:25.120
 we will be lazy or get sidetracked

00:21:25.120 --> 00:21:31.480
 by other concerns or other attachments, other desires that

00:21:31.480 --> 00:21:34.680
 make us want to do other things.

00:21:34.680 --> 00:21:38.960
 But here we've taken this opportunity, we're here together.

00:21:38.960 --> 00:21:45.590
 So all that we have left is to make use of it and to

00:21:45.590 --> 00:21:47.520
 practice.

00:21:47.520 --> 00:21:50.480
 So this is what we're doing, I mean goodness is not just,

00:21:50.480 --> 00:21:52.080
 the Buddhist teaching is not

00:21:52.080 --> 00:21:55.690
 just the practice of meditation, all that we've done

00:21:55.690 --> 00:21:57.960
 tonight is dedication to the Buddhist

00:21:57.960 --> 00:22:03.710
 teaching, it's something that we should be happy about or

00:22:03.710 --> 00:22:07.680
 glad about or encouraged by

00:22:07.680 --> 00:22:09.920
 the fact that we've taken this opportunity.

00:22:09.920 --> 00:22:14.560
 Here we are, we come in and we don't say bad things to each

00:22:14.560 --> 00:22:16.960
 other, we don't do bad things,

00:22:16.960 --> 00:22:19.160
 we don't even think about thoughts.

00:22:19.160 --> 00:22:23.610
 We come in and we're respectful and we pay respect to the

00:22:23.610 --> 00:22:24.560
 Buddha.

00:22:24.560 --> 00:22:32.640
 We offer incense, candles, flowers to the Buddha, we make

00:22:32.640 --> 00:22:36.400
 offerings to the Buddha out

00:22:36.400 --> 00:22:40.870
 of respect and even from that very act it's a very special

00:22:40.870 --> 00:22:43.680
 thing, just thinking of someone

00:22:43.680 --> 00:22:48.350
 who is so pure and perfect, it puts our mind towards the

00:22:48.350 --> 00:22:51.720
 idea of being pure and perfect.

00:22:51.720 --> 00:22:55.840
 This is why the Buddha said "pujja, japujja, nyan" when you

00:22:55.840 --> 00:22:57.280
 pay respect or homage to those

00:22:57.280 --> 00:23:03.320
 who are worthy of homage, this is a great blessing.

00:23:03.320 --> 00:23:07.700
 Because whoever you pay homage to, this is the sort of

00:23:07.700 --> 00:23:09.960
 person that you become.

00:23:09.960 --> 00:23:13.080
 Whoever you put up as your role model, of course, this is

00:23:13.080 --> 00:23:14.840
 who you wind up being more and more

00:23:14.840 --> 00:23:17.520
 like.

00:23:17.520 --> 00:23:20.690
 When we think about the Buddha, when we pay respect to the

00:23:20.690 --> 00:23:22.360
 Buddha, this is putting him

00:23:22.360 --> 00:23:25.000
 up as the ideal.

00:23:25.000 --> 00:23:28.100
 Whoever this person was, we've never met him but we know

00:23:28.100 --> 00:23:29.840
 this is our ideal, a person who

00:23:29.840 --> 00:23:35.560
 is free from defilement, a person who is perfectly wise, a

00:23:35.560 --> 00:23:38.840
 person who has a great compassion

00:23:38.840 --> 00:23:43.000
 to help other beings.

00:23:43.000 --> 00:23:47.040
 This is the ideal that we maybe don't strive completely for

00:23:47.040 --> 00:23:48.760
 but we work as close as we

00:23:48.760 --> 00:23:57.600
 can to become a disciple of this great being.

00:23:57.600 --> 00:23:59.840
 And then we begin chanting, even the chanting.

00:23:59.840 --> 00:24:03.280
 Some people maybe wonder why we do chanting.

00:24:03.280 --> 00:24:06.280
 It may be difficult, especially if you don't understand

00:24:06.280 --> 00:24:07.480
 what's being said.

00:24:07.480 --> 00:24:10.330
 When you don't understand the chanting, you often think it

00:24:10.330 --> 00:24:11.840
's maybe just some ritual and

00:24:11.840 --> 00:24:14.120
 we don't know why we're doing it.

00:24:14.120 --> 00:24:17.240
 Maybe it seems like it has no point for some people.

00:24:17.240 --> 00:24:19.000
 Maybe that's a reason why they don't think to come.

00:24:19.000 --> 00:24:23.720
 They think "oh, chanting is not really useful."

00:24:23.720 --> 00:24:25.520
 But then you think these are the words of the Buddha.

00:24:25.520 --> 00:24:30.190
 So when you hear "gano voma upatagā" even just hearing

00:24:30.190 --> 00:24:31.280
 that, that's the words of the

00:24:31.280 --> 00:24:32.280
 Buddha.

00:24:32.280 --> 00:24:37.160
 It's a great blessing, especially when you know this is the

00:24:37.160 --> 00:24:39.140
 words of the Buddha.

00:24:39.140 --> 00:24:41.610
 Even if you don't know what's being said, there are these

00:24:41.610 --> 00:24:43.160
 stories of a frog, for example,

00:24:43.160 --> 00:24:46.090
 who was killed while he was listening to Buddha's teaching

00:24:46.090 --> 00:24:48.600
 or these bats that fell from the

00:24:48.600 --> 00:24:54.100
 roof of a cave while they were listening to monks chanting

00:24:54.100 --> 00:24:56.000
 the abhidhamma.

00:24:56.000 --> 00:25:00.140
 These cases of beings whose minds became pure, not knowing

00:25:00.140 --> 00:25:02.280
 what this was, but knowing that

00:25:02.280 --> 00:25:06.970
 it was somehow holy or somehow special and becoming gladd

00:25:06.970 --> 00:25:09.680
ened and pacified by the words,

00:25:09.680 --> 00:25:11.680
 by the speech.

00:25:11.680 --> 00:25:15.050
 There's something very powerful to the Buddha's teaching,

00:25:15.050 --> 00:25:17.840
 especially when it's in Pali, in

00:25:17.840 --> 00:25:25.540
 the language which was at least very close to what the

00:25:25.540 --> 00:25:29.480
 Buddha actually spoke.

00:25:29.480 --> 00:25:35.720
 So this is something that helps to purify and to pacify our

00:25:35.720 --> 00:25:38.480
 mind and to help give rise

00:25:38.480 --> 00:25:41.920
 to gladness and peace in the mind.

00:25:41.920 --> 00:25:44.760
 It's very useful for meditation.

00:25:44.760 --> 00:25:49.380
 And then there's the chanting of the Buddha's teaching,

00:25:49.380 --> 00:25:51.920
 especially when you do know what's

00:25:51.920 --> 00:25:53.920
 being said.

00:25:53.920 --> 00:25:56.560
 So by memorizing the Buddha's teaching, you always have

00:25:56.560 --> 00:25:59.760
 this in your mind, "pooja, chapooja,

00:25:59.760 --> 00:26:03.980
 niyana," this idea of paying homage to those who are worthy

00:26:03.980 --> 00:26:04.960
 of homage.

00:26:04.960 --> 00:26:06.640
 And so many other, how many blessings?

00:26:06.640 --> 00:26:15.640
 Thirty-seven or something, many, many blessings.

00:26:15.640 --> 00:26:23.640
 Thirty-eight, I can't remember.

00:26:23.640 --> 00:26:28.650
 Association, not associating with fools, association with

00:26:28.650 --> 00:26:34.880
 the wise, this is a great, great blessing.

00:26:34.880 --> 00:26:35.880
 And so on and so on.

00:26:35.880 --> 00:26:39.930
 So many, just the Mangala Suta alone is an incredible

00:26:39.930 --> 00:26:42.520
 teaching for, my teacher said,

00:26:42.520 --> 00:26:45.770
 if society, if the world at large knew about these

00:26:45.770 --> 00:26:48.400
 blessings and followed and practiced

00:26:48.400 --> 00:26:55.880
 just these basic principles of life, there would be no

00:26:55.880 --> 00:26:59.200
 conflict in the world.

00:26:59.200 --> 00:27:03.840
 Just this one Suta is very, very powerful.

00:27:03.840 --> 00:27:07.480
 So many things in there, maybe not everything, but so many

00:27:07.480 --> 00:27:08.240
 things.

00:27:08.240 --> 00:27:11.720
 We can see the Buddha's wisdom just in this Suta alone.

00:27:11.720 --> 00:27:13.720
 That's why we chant it so often.

00:27:13.720 --> 00:27:16.560
 It's a good one to remember.

00:27:16.560 --> 00:27:20.080
 If you can't remember it, you go by the first letter.

00:27:20.080 --> 00:27:21.080
 This is how I do it.

00:27:21.080 --> 00:27:22.080
 This is how I can remember it.

00:27:22.080 --> 00:27:39.040
 A, pa, ba, ma, da, a, ga, ka, da, pa, e, e, e, e, e, e, e,

00:27:39.040 --> 00:27:39.040
 e, e, e, e, e, e, e, e, e,

00:27:39.040 --> 00:27:39.640
 e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e,

00:27:39.640 --> 00:27:40.040
 e, e, e, e, e, e, e, e, e, e,

00:27:40.040 --> 00:27:40.650
 e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e,

00:27:40.650 --> 00:27:41.040
 e, e, e, e, e, e, e, e, e,

00:27:41.040 --> 00:27:50.830
 e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e,

00:27:50.830 --> 00:27:55.000
 e, e, e, e, e, e, e, e,

00:27:55.000 --> 00:28:01.800
 e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e,

00:28:01.800 --> 00:28:05.000
 e, e, e, e, e, e, e, e,

00:28:05.000 --> 00:28:09.640
 e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e,

00:28:09.640 --> 00:28:11.000
 e, e, e, e, e, e, e,

00:28:12.000 --> 00:28:18.000
 e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e,

00:28:18.000 --> 00:28:19.000
 e, e, e, e, e, e, e, e,

00:28:19.000 --> 00:28:25.890
 e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e,

00:28:25.890 --> 00:28:28.000
 e, e, e, e, e, e, e, e,

00:28:34.000 --> 00:28:46.000
 e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e,

00:28:46.000 --> 00:28:50.000
 e, e, e, e, e, e, e, e,

00:30:21.000 --> 00:30:37.000
 e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e, e,

00:30:37.000 --> 00:30:37.000
 e, e, e, e, e, e, e, e, e,

