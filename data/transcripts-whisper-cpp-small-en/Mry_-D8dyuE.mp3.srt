1
00:00:00,000 --> 00:00:10,000
 Hello everyone, I'm back with more questions and answers.

2
00:00:10,000 --> 00:00:14,240
 So today's question, you can go and read it if you like, if

3
00:00:14,240 --> 00:00:19,980
 you're watching this live.

4
00:00:19,980 --> 00:00:26,220
 It's actually a rather complicated question, but I find two

5
00:00:26,220 --> 00:00:27,900
 points in it.

6
00:00:27,900 --> 00:00:33,780
 It's about the noting practice or the use of what I call

7
00:00:33,780 --> 00:00:36,480
 mantra, mindfulness mantra

8
00:00:36,480 --> 00:00:45,480
 of sorts, using a word or making a note.

9
00:00:45,480 --> 00:00:51,280
 And so the first part is about different ways of noting.

10
00:00:51,280 --> 00:00:54,000
 This person has their own way of noting, I think.

11
00:00:54,000 --> 00:00:58,840
 I don't quite understand.

12
00:00:58,840 --> 00:01:01,450
 It's not easy to tell exactly what this person is doing,

13
00:01:01,450 --> 00:01:03,760
 how they're meditating, even quite

14
00:01:03,760 --> 00:01:05,440
 what they're asking over the internet.

15
00:01:05,440 --> 00:01:09,360
 So I'm just going to talk generally.

16
00:01:09,360 --> 00:01:14,050
 First about that, and second is about whether noting can

17
00:01:14,050 --> 00:01:15,680
 cause clinging.

18
00:01:15,680 --> 00:01:17,480
 This is a common question, the second one.

19
00:01:17,480 --> 00:01:24,040
 But it's two distinct points, though related.

20
00:01:24,040 --> 00:01:28,840
 I think I can answer them sort of together.

21
00:01:28,840 --> 00:01:35,450
 So this person talks about noting experiences as the five

22
00:01:35,450 --> 00:01:37,280
 aggregates.

23
00:01:37,280 --> 00:01:42,040
 So this one is the first aggregate, this one is form, is

24
00:01:42,040 --> 00:01:43,240
 physical.

25
00:01:43,240 --> 00:01:47,160
 This one is feeling, this one is perception.

26
00:01:47,160 --> 00:01:55,160
 This one is thought, this one is consciousness.

27
00:01:55,160 --> 00:01:58,270
 And so there's a question, I'm not even sure that they're

28
00:01:58,270 --> 00:02:00,400
 asking this question, but I have

29
00:02:00,400 --> 00:02:03,510
 a question that I want to answer is whether this is a valid

30
00:02:03,510 --> 00:02:05,400
 understanding of the technique

31
00:02:05,400 --> 00:02:08,900
 that we do or even a valid meditation at all.

32
00:02:08,900 --> 00:02:12,570
 So when we talk about meditation, and this is a question I

33
00:02:12,570 --> 00:02:14,360
 get, sort of question I get,

34
00:02:14,360 --> 00:02:18,850
 is what is meditation or is X meditation, is this thing

35
00:02:18,850 --> 00:02:22,560
 that I do meditation or so on?

36
00:02:22,560 --> 00:02:26,040
 Meditation is some kind of mental practice.

37
00:02:26,040 --> 00:02:28,690
 It's where you do something with your mind, cultivating

38
00:02:28,690 --> 00:02:30,320
 habits, cultivating or dealing

39
00:02:30,320 --> 00:02:31,320
 with habits.

40
00:02:31,320 --> 00:02:35,440
 It can be about breaking down habits.

41
00:02:35,440 --> 00:02:39,390
 And so when you have an experience, there's many different

42
00:02:39,390 --> 00:02:40,800
 things you can do.

43
00:02:40,800 --> 00:02:44,800
 You can think about it.

44
00:02:44,800 --> 00:02:46,880
 You can react to it.

45
00:02:46,880 --> 00:02:52,720
 You can try and suppress it or you can try and find a way

46
00:02:52,720 --> 00:02:54,360
 to avoid it.

47
00:02:54,360 --> 00:03:04,980
 You can go with it or engage with it, try and encourage it

48
00:03:04,980 --> 00:03:08,040
 to continue.

49
00:03:08,040 --> 00:03:09,440
 You can enjoy it.

50
00:03:09,440 --> 00:03:16,560
 You can ignore it.

51
00:03:16,560 --> 00:03:20,310
 And so what happens with some types of meditation or

52
00:03:20,310 --> 00:03:24,400
 practice that appear to be meditation is

53
00:03:24,400 --> 00:03:29,890
 they become a meditation in the Western sense where you

54
00:03:29,890 --> 00:03:32,800
 mull or ponder something.

55
00:03:32,800 --> 00:03:35,120
 And I think this is what this person is talking about.

56
00:03:35,120 --> 00:03:39,420
 And so I don't consider it to be a valid sort of meditation

57
00:03:39,420 --> 00:03:42,360
 of the sort that we're doing.

58
00:03:42,360 --> 00:03:45,480
 If you sit and when you have an experience, suppose you

59
00:03:45,480 --> 00:03:47,320
 feel pain and you contemplate

60
00:03:47,320 --> 00:03:49,890
 it from a Buddhist perspective, you think about it, "Hey,

61
00:03:49,890 --> 00:03:51,000
 this is impermanent.

62
00:03:51,000 --> 00:03:52,880
 Hey, this is suffering.

63
00:03:52,880 --> 00:03:54,160
 Hey, this is non-self."

64
00:03:54,160 --> 00:03:58,280
 Or you think, "Oh, this is just pain."

65
00:03:58,280 --> 00:04:02,160
 And you try and tell yourself something about it.

66
00:04:02,160 --> 00:04:05,260
 Well, not this is just pain actually, that would be a

67
00:04:05,260 --> 00:04:06,600
 little better I think.

68
00:04:06,600 --> 00:04:09,120
 But so I'll explain why.

69
00:04:09,120 --> 00:04:12,290
 But if you make it out to be more than it is, you create

70
00:04:12,290 --> 00:04:14,240
 abstract thought about it.

71
00:04:14,240 --> 00:04:19,200
 You're not actually cultivating mindfulness.

72
00:04:19,200 --> 00:04:24,920
 You're evoking states of thought, states of contemplation.

73
00:04:24,920 --> 00:04:27,980
 So you're actually not paying attention to the experience,

74
00:04:27,980 --> 00:04:29,320
 which turns out to be quite

75
00:04:29,320 --> 00:04:33,800
 important from a meditative or a mindfulness perspective.

76
00:04:33,800 --> 00:04:36,510
 The difference between that and saying to yourself, "This

77
00:04:36,510 --> 00:04:38,400
 is pain," or even just pain,

78
00:04:38,400 --> 00:04:41,510
 pain, or if it's a thought saying, "This is a thought," or

79
00:04:41,510 --> 00:04:43,120
 thinking, is you're actually

80
00:04:43,120 --> 00:04:49,270
 reminding yourself something very fundamental and basic

81
00:04:49,270 --> 00:04:51,960
 about the experience.

82
00:04:51,960 --> 00:04:56,640
 And what it evokes is a free understanding, a free

83
00:04:56,640 --> 00:05:00,400
 consciousness, a consciousness that

84
00:05:00,400 --> 00:05:04,650
 is free from any kind of judgment, any kind of abstraction,

85
00:05:04,650 --> 00:05:06,880
 any kind of diversification,

86
00:05:06,880 --> 00:05:11,040
 making more out of the experience than it actually is.

87
00:05:11,040 --> 00:05:17,850
 So it's actually quite a special interaction in that it's

88
00:05:17,850 --> 00:05:21,200
 meant to, it's designed to,

89
00:05:21,200 --> 00:05:26,160
 and it has the effect of creating an objective state of

90
00:05:26,160 --> 00:05:29,280
 mind where you simply experience

91
00:05:29,280 --> 00:05:31,400
 the object as it is.

92
00:05:31,400 --> 00:05:36,220
 And we talk about things like patience and equanimity, even

93
00:05:36,220 --> 00:05:38,360
 peace and tranquility.

94
00:05:38,360 --> 00:05:44,320
 They're all wrapped up in this idea of not doing anything.

95
00:05:44,320 --> 00:05:49,690
 So what we're trying to do with that is to find that state

96
00:05:49,690 --> 00:05:52,200
 where we don't create, where

97
00:05:52,200 --> 00:05:55,920
 we don't proliferate, where we don't make anything out of

98
00:05:55,920 --> 00:05:56,360
 it.

99
00:05:56,360 --> 00:05:57,360
 It's called bhava.

100
00:05:57,360 --> 00:06:03,950
 Bhava, if you're familiar with Buddhist theory, is the link

101
00:06:03,950 --> 00:06:09,520
 in the chain that leads to suffering

102
00:06:09,520 --> 00:06:11,200
 when you make something out of something.

103
00:06:11,200 --> 00:06:14,320
 So if someone says a simple worldly example, if someone

104
00:06:14,320 --> 00:06:16,080
 says something to you like you're

105
00:06:16,080 --> 00:06:18,730
 a jerk, you're a dummy, you're this, you're that, if

106
00:06:18,730 --> 00:06:20,480
 someone says you're fat or you're

107
00:06:20,480 --> 00:06:24,080
 thin or so on, someone says something, maybe even something

108
00:06:24,080 --> 00:06:26,080
 innocent to you and you make

109
00:06:26,080 --> 00:06:29,810
 something out of it, you get upset about it, you've created

110
00:06:29,810 --> 00:06:30,480
 bhava.

111
00:06:30,480 --> 00:06:34,640
 Bhava means being or becoming, you make something, you

112
00:06:34,640 --> 00:06:36,900
 cause something to become.

113
00:06:36,900 --> 00:06:38,400
 You make a deal out of it, right?

114
00:06:38,400 --> 00:06:42,360
 When you make a big deal out of something or any kind of

115
00:06:42,360 --> 00:06:43,000
 deal.

116
00:06:43,000 --> 00:06:47,600
 So the use of the mantra, a mantra when it's tautological

117
00:06:47,600 --> 00:06:50,080
 in the sense of it's not saying

118
00:06:50,080 --> 00:06:55,570
 anything new about the experience, it's just reminding you

119
00:06:55,570 --> 00:06:58,680
 in the way of trying to experience

120
00:06:58,680 --> 00:07:03,040
 the thing simply for what it is.

121
00:07:03,040 --> 00:07:06,480
 It's unique in that what it evokes again is nothing really.

122
00:07:06,480 --> 00:07:10,520
 It's an absence and it should feel like that.

123
00:07:10,520 --> 00:07:13,280
 It should feel like you've got a respite.

124
00:07:13,280 --> 00:07:19,870
 You've been given us a moment that is free from any of this

125
00:07:19,870 --> 00:07:23,200
 thinking or contemplating

126
00:07:23,200 --> 00:07:26,280
 or diversifying.

127
00:07:26,280 --> 00:07:29,670
 So the first part of this question, this person who talks

128
00:07:29,670 --> 00:07:31,760
 about applying Buddhist theory to

129
00:07:31,760 --> 00:07:36,050
 the experience and trying to, a common one that wasn't

130
00:07:36,050 --> 00:07:38,680
 mentioned is saying to yourself,

131
00:07:38,680 --> 00:07:39,680
 "Hey, that's impermanent."

132
00:07:39,680 --> 00:07:43,730
 If you start to contemplate it as impermanent, it's a poor

133
00:07:43,730 --> 00:07:46,160
 translation or it's a misleading

134
00:07:46,160 --> 00:07:49,850
 translation when we read texts where it says one contem

135
00:07:49,850 --> 00:07:52,440
plates something as impermanent.

136
00:07:52,440 --> 00:07:57,250
 It's generally better or more literal to say sees or

137
00:07:57,250 --> 00:08:00,480
 understands or knows the thing as

138
00:08:00,480 --> 00:08:03,360
 impermanent.

139
00:08:03,360 --> 00:08:07,520
 And that gets into our, sort of the second part.

140
00:08:07,520 --> 00:08:11,360
 No, but I'll just say a few more things about what else

141
00:08:11,360 --> 00:08:12,120
 comes.

142
00:08:12,120 --> 00:08:16,740
 So I say when you note you're really doing nothing or you

143
00:08:16,740 --> 00:08:18,940
're creating nothing.

144
00:08:18,940 --> 00:08:21,990
 The result is a state that is free, but there's much more

145
00:08:21,990 --> 00:08:23,040
 to it than that.

146
00:08:23,040 --> 00:08:27,000
 It's not just simply nothing.

147
00:08:27,000 --> 00:08:30,240
 First of all, you're creating this objectivity, but second

148
00:08:30,240 --> 00:08:32,200
 of all, you're creating a habit.

149
00:08:32,200 --> 00:08:35,780
 You're creating a habitual awareness or a habitual

150
00:08:35,780 --> 00:08:38,200
 interaction with experience.

151
00:08:38,200 --> 00:08:44,490
 So without meditating, a person who never has any idea to

152
00:08:44,490 --> 00:08:48,560
 practice meditation is constantly

153
00:08:48,560 --> 00:08:51,960
 engaging with experiences habitually.

154
00:08:51,960 --> 00:08:55,580
 We think it, and this person even mentions it, I think in

155
00:08:55,580 --> 00:08:57,560
 their question about how when

156
00:08:57,560 --> 00:09:00,760
 you experience something immediately you react to it.

157
00:09:00,760 --> 00:09:03,940
 It's like he explains, or this person explains it as being

158
00:09:03,940 --> 00:09:05,640
 bound up in the experience.

159
00:09:05,640 --> 00:09:07,400
 There's liking and disliking.

160
00:09:07,400 --> 00:09:09,680
 It's just a part of the experience.

161
00:09:09,680 --> 00:09:12,080
 It's so immediate.

162
00:09:12,080 --> 00:09:17,320
 So we would see this as habit, our habits that we develop

163
00:09:17,320 --> 00:09:20,240
 throughout our life in a kind of

164
00:09:20,240 --> 00:09:24,160
 a similar way to the way we develop habits in meditation.

165
00:09:24,160 --> 00:09:28,050
 But the habits in meditation, especially mindfulness

166
00:09:28,050 --> 00:09:31,440
 meditation, are much more simple and singular.

167
00:09:31,440 --> 00:09:38,360
 And so by saying to ourselves pain, pain or thinking,

168
00:09:38,360 --> 00:09:44,160
 thinking and evoking habits of experiencing

169
00:09:44,160 --> 00:09:51,880
 things just as they are, it flows into our ordinary lives.

170
00:09:51,880 --> 00:09:55,960
 And so we'll find ourselves walking down the street and

171
00:09:55,960 --> 00:09:58,960
 experiencing things without stress,

172
00:09:58,960 --> 00:10:02,680
 having thoughts, having interactions with people.

173
00:10:02,680 --> 00:10:05,570
 We'll find ourselves at work in situations where we would

174
00:10:05,570 --> 00:10:07,080
 be stressed, where we would

175
00:10:07,080 --> 00:10:11,240
 be upset, in family situations where we would be reactive

176
00:10:11,240 --> 00:10:14,120
 or reactionary, and finding ourselves

177
00:10:14,120 --> 00:10:16,880
 far less reactionary because of the habits that we're

178
00:10:16,880 --> 00:10:17,760
 developing.

179
00:10:17,760 --> 00:10:20,960
 It's not magic and it's not quick.

180
00:10:20,960 --> 00:10:25,410
 It's not a quick fix because we're dealing with old habits

181
00:10:25,410 --> 00:10:27,520
 that are years and years and

182
00:10:27,520 --> 00:10:31,840
 even lifetimes, perhaps old.

183
00:10:31,840 --> 00:10:35,280
 But it happens quite clearly.

184
00:10:35,280 --> 00:10:39,710
 And the evidence you can see for yourself, the Buddha said,

185
00:10:39,710 --> 00:10:41,800
 his teaching was something

186
00:10:41,800 --> 00:10:43,600
 you can see for this very reason.

187
00:10:43,600 --> 00:10:44,800
 It's not magic.

188
00:10:44,800 --> 00:10:50,230
 It's not talking about God or heaven or spirits or magical

189
00:10:50,230 --> 00:10:51,320
 powers.

190
00:10:51,320 --> 00:10:55,650
 It's talking about simple principles of cultivating habits

191
00:10:55,650 --> 00:10:57,600
 and their effect on them.

192
00:10:57,600 --> 00:11:02,320
 The third thing it does is it breaks down bad habits.

193
00:11:02,320 --> 00:11:05,660
 And that's, you know, as a part of building up good habits,

194
00:11:05,660 --> 00:11:07,280
 is that all these reactions

195
00:11:07,280 --> 00:11:08,700
 don't have the opportunity.

196
00:11:08,700 --> 00:11:15,540
 So when you do nothing, we decrease and weaken our capacity

197
00:11:15,540 --> 00:11:20,040
, our potential to react to things.

198
00:11:20,040 --> 00:11:24,020
 The fourth thing it does is it helps you to see more

199
00:11:24,020 --> 00:11:26,520
 clearly your experience.

200
00:11:26,520 --> 00:11:29,200
 So you'll see clearly how bad habits are bad.

201
00:11:29,200 --> 00:11:32,660
 You'll see how bad anger is and greed is and the conceit

202
00:11:32,660 --> 00:11:34,840
 and arrogance and all that worry

203
00:11:34,840 --> 00:11:39,680
 and restlessness and distraction and so on.

204
00:11:39,680 --> 00:11:42,920
 But you'll also see more clearly the things that you like,

205
00:11:42,920 --> 00:11:44,600
 the things that you normally

206
00:11:44,600 --> 00:11:47,920
 like, the things that you normally dislike.

207
00:11:47,920 --> 00:12:00,450
 You'll see clearly your thoughts and your rationalization

208
00:12:00,450 --> 00:12:04,200
 or, you know, these investigations

209
00:12:04,200 --> 00:12:05,200
 that go on in the mind.

210
00:12:05,200 --> 00:12:07,960
 You'll see them clearly as well.

211
00:12:07,960 --> 00:12:11,370
 And the great thing about that is that's where you see

212
00:12:11,370 --> 00:12:14,160
 impermanent suffering and non-self.

213
00:12:14,160 --> 00:12:18,600
 You'll see that the things that you clung to or held onto

214
00:12:18,600 --> 00:12:21,240
 or wanted are not worth wanting

215
00:12:21,240 --> 00:12:25,370
 because the stability you saw in them, the satisfaction you

216
00:12:25,370 --> 00:12:27,120
 saw in them, the control

217
00:12:27,120 --> 00:12:32,680
 that you thought you had over them, goes all to pot.

218
00:12:32,680 --> 00:12:34,720
 It turns out to be an illusion.

219
00:12:34,720 --> 00:12:38,420
 It turns out to be something you cooked up in your

220
00:12:38,420 --> 00:12:39,640
 ignorance.

221
00:12:39,640 --> 00:12:43,640
 And when you look more clearly at the experiences, you see

222
00:12:43,640 --> 00:12:47,680
 that it's all unpredictable, inconstant,

223
00:12:47,680 --> 00:12:49,520
 impermanent.

224
00:12:49,520 --> 00:12:55,400
 It's unsatisfying and it's uncontrollable and there's no

225
00:12:55,400 --> 00:12:58,480
 thing that you can control.

226
00:12:58,480 --> 00:13:02,470
 The nature of things is not about things that are possess

227
00:13:02,470 --> 00:13:04,360
able or controllable.

228
00:13:04,360 --> 00:13:10,820
 So you see the chaos in your mind and in your body and so

229
00:13:10,820 --> 00:13:11,640
 on.

230
00:13:11,640 --> 00:13:15,180
 So these are the things that happen when you are mindful,

231
00:13:15,180 --> 00:13:17,120
 when you just say to yourself,

232
00:13:17,120 --> 00:13:19,800
 "Pain, pain, thinking, thinking."

233
00:13:19,800 --> 00:13:28,160
 So the question of whether noting could lead to clinging,

234
00:13:28,160 --> 00:13:31,920
 it's often when someone first

235
00:13:31,920 --> 00:13:36,630
 hears about this idea and we mention things like saying to

236
00:13:36,630 --> 00:13:39,400
 yourself, "Angry, angry."

237
00:13:39,400 --> 00:13:42,170
 It's often a question where the person asks, "Wouldn't that

238
00:13:42,170 --> 00:13:43,600
 just make the anger worse or

239
00:13:43,600 --> 00:13:47,320
 wouldn't that just make me want the thing or crave the

240
00:13:47,320 --> 00:13:48,480
 thing more?"

241
00:13:48,480 --> 00:13:54,640
 And I suppose it's reasonable or it's unsurprising that

242
00:13:54,640 --> 00:13:58,920
 people will ask this, but for someone

243
00:13:58,920 --> 00:14:05,080
 who's done meditation, it's hard to understand sort of how

244
00:14:05,080 --> 00:14:08,400
 someone could think that or how

245
00:14:08,400 --> 00:14:09,560
 that could be possible.

246
00:14:09,560 --> 00:14:15,280
 It's like you might as well think that the sun might rise

247
00:14:15,280 --> 00:14:18,240
 in the west of something.

248
00:14:18,240 --> 00:14:21,540
 But to break it down intellectually, I'm going to give a

249
00:14:21,540 --> 00:14:23,200
 talk here and I'll talk a little

250
00:14:23,200 --> 00:14:26,240
 bit intellectually, just why it isn't so.

251
00:14:26,240 --> 00:14:28,080
 Because if you practice meditation, you'll see that it isn

252
00:14:28,080 --> 00:14:28,400
't so.

253
00:14:28,400 --> 00:14:33,160
 If you practice saying to yourself, "Pain, angry, angry, or

254
00:14:33,160 --> 00:14:35,800
 liking, liking," or whatever,

255
00:14:35,800 --> 00:14:40,150
 if you say to yourself, "Pain, pain," why it doesn't lead

256
00:14:40,150 --> 00:14:41,520
 to bad things.

257
00:14:41,520 --> 00:14:47,270
 We'll talk about why this is, goes back to what I've just

258
00:14:47,270 --> 00:14:49,680
 been talking about.

259
00:14:49,680 --> 00:14:56,000
 But more particularly is talking about what it is that

260
00:14:56,000 --> 00:14:58,600
 leads to clinging.

261
00:14:58,600 --> 00:15:00,800
 Why is it that we cling to things?

262
00:15:00,800 --> 00:15:11,440
 When we have something pleasant, why do we want it?

263
00:15:11,440 --> 00:15:15,200
 What is it that leads us to want more?

264
00:15:15,200 --> 00:15:20,400
 When we have anger, what is it that leads to more anger?

265
00:15:20,400 --> 00:15:25,450
 So it's first worth noting that we can't actually be

266
00:15:25,450 --> 00:15:28,840
 mindful in the present moment.

267
00:15:28,840 --> 00:15:33,790
 And this is especially the case with anger and greed, for

268
00:15:33,790 --> 00:15:37,080
 example, or unwholesome states.

269
00:15:37,080 --> 00:15:40,550
 So when you have an experience of thought, for example, the

270
00:15:40,550 --> 00:15:42,100
 thought has to come first

271
00:15:42,100 --> 00:15:43,840
 and then you have to be mindful of it.

272
00:15:43,840 --> 00:15:46,680
 You have to remind yourself that was thinking.

273
00:15:46,680 --> 00:15:49,620
 So it's actually being mindful of something that happened

274
00:15:49,620 --> 00:15:50,360
 in the past.

275
00:15:50,360 --> 00:15:53,070
 That's important because we have to understand what's going

276
00:15:53,070 --> 00:15:54,800
 on here when we get angry, or

277
00:15:54,800 --> 00:15:58,790
 when we have clinging, or so on, or when we have an

278
00:15:58,790 --> 00:16:01,760
 experience and then we cling to it.

279
00:16:01,760 --> 00:16:10,600
 What is the process by which that occurs?

280
00:16:10,600 --> 00:16:16,070
 So when we have anger, for example, anger and mindfulness

281
00:16:16,070 --> 00:16:18,640
 can't exist in the same mind

282
00:16:18,640 --> 00:16:19,640
 state.

283
00:16:19,640 --> 00:16:25,920
 So what we're actually being aware of is the fact that we

284
00:16:25,920 --> 00:16:27,640
 were angry.

285
00:16:27,640 --> 00:16:30,880
 It's an acknowledgement of the fact that we're angry,

286
00:16:30,880 --> 00:16:33,140
 saying there was anger, or there was

287
00:16:33,140 --> 00:16:36,220
 anger and then saying to ourselves that was anger.

288
00:16:36,220 --> 00:16:45,350
 So how that's different from escalating, or why that would

289
00:16:45,350 --> 00:16:49,580
 in no way escalate is because

290
00:16:49,580 --> 00:16:55,340
 escalating is when you say that something is this or that.

291
00:16:55,340 --> 00:16:58,940
 When you have anger and you say that's bad, or when the

292
00:16:58,940 --> 00:17:01,080
 anger leads to a headache, or

293
00:17:01,080 --> 00:17:05,260
 tension, or pain, or sadness, or so on, and you react to

294
00:17:05,260 --> 00:17:05,920
 that.

295
00:17:05,920 --> 00:17:08,140
 You say that's bad.

296
00:17:08,140 --> 00:17:13,910
 So if someone says something to you and you get angry, and

297
00:17:13,910 --> 00:17:16,800
 the anger comes and then you

298
00:17:16,800 --> 00:17:19,570
 think about what they said, you remember again what they

299
00:17:19,570 --> 00:17:23,000
 said, and this happens very quickly.

300
00:17:23,000 --> 00:17:27,000
 You'll say, "Did you just say you're angry?"

301
00:17:27,000 --> 00:17:30,680
 And angrily you'll say to them or to yourself, "Did you

302
00:17:30,680 --> 00:17:33,040
 just say to me that I'm this or I'm

303
00:17:33,040 --> 00:17:34,040
 that?"

304
00:17:34,040 --> 00:17:37,330
 And that makes you more angry, because you're evoking

305
00:17:37,330 --> 00:17:39,680
 another experience after the anger,

306
00:17:39,680 --> 00:17:42,160
 and you're getting angry about that.

307
00:17:42,160 --> 00:17:47,720
 And you're building up based all on habits.

308
00:17:47,720 --> 00:17:51,080
 You're escalating it into more and more anger.

309
00:17:51,080 --> 00:17:54,800
 The same goes with, this is similar to how I talked about

310
00:17:54,800 --> 00:17:56,920
 anxiety, when you're anxious

311
00:17:56,920 --> 00:17:59,490
 and then there's the physical feelings that come from

312
00:17:59,490 --> 00:18:01,480
 anxiety and they make you more anxious.

313
00:18:01,480 --> 00:18:04,270
 You get anxious because, "Oh, I'm anxious," and you can

314
00:18:04,270 --> 00:18:05,880
 feel it, and it makes you more

315
00:18:05,880 --> 00:18:09,990
 anxious and you can have a panic attack if it gets worse

316
00:18:09,990 --> 00:18:11,060
 and worse.

317
00:18:11,060 --> 00:18:15,100
 When you say to yourself, for example, "Angry, angry," or

318
00:18:15,100 --> 00:18:17,400
 when you say to yourself, "Pain,

319
00:18:17,400 --> 00:18:21,290
 pain," why it doesn't lead to bad things, why the anger

320
00:18:21,290 --> 00:18:23,280
 doesn't lead to more anger,

321
00:18:23,280 --> 00:18:26,130
 is because you're saying, "This is this," rather than

322
00:18:26,130 --> 00:18:27,880
 saying, "This is bad," and creating

323
00:18:27,880 --> 00:18:30,880
 a reaction, you're cutting the chain.

324
00:18:30,880 --> 00:18:34,320
 So, it's quite simple and quite obvious.

325
00:18:34,320 --> 00:18:41,440
 I think the question is based on our ordinary use of words.

326
00:18:41,440 --> 00:18:44,720
 In an ordinary sense, we might say, "I'm so angry," but we

327
00:18:44,720 --> 00:18:46,460
 don't mean it as an acceptance

328
00:18:46,460 --> 00:18:48,320
 and an understanding that I am angry.

329
00:18:48,320 --> 00:18:53,940
 We mean it as, "I'm getting angry," or, "I want to be more

330
00:18:53,940 --> 00:18:55,080
 angry."

331
00:18:55,080 --> 00:18:58,720
 When you say, "I'm so angry," you're actually building more

332
00:18:58,720 --> 00:19:00,360
 anger in the ordinary sense

333
00:19:00,360 --> 00:19:06,600
 because you're not just being objective about it.

334
00:19:06,600 --> 00:19:10,590
 So I don't know, a couple of interesting ideas about the

335
00:19:10,590 --> 00:19:13,480
 noting technique, about mindfulness

336
00:19:13,480 --> 00:19:16,080
 in general.

337
00:19:16,080 --> 00:19:21,970
 I think I take this approach because I think it's important

338
00:19:21,970 --> 00:19:24,680
 to talk about this process

339
00:19:24,680 --> 00:19:30,080
 of noting explicitly in detail.

340
00:19:30,080 --> 00:19:33,740
 Sometimes, because sometimes I make videos about Buddhism

341
00:19:33,740 --> 00:19:35,760
 and about meditation, talking

342
00:19:35,760 --> 00:19:40,620
 about concepts and ideas without explicitly talking about

343
00:19:40,620 --> 00:19:42,160
 the technique.

344
00:19:42,160 --> 00:19:45,680
 It seems like sometimes people miss that.

345
00:19:45,680 --> 00:19:49,560
 I've talked to people who are interested in my videos who

346
00:19:49,560 --> 00:19:51,520
 have found them helpful, but

347
00:19:51,520 --> 00:19:56,120
 don't yet understand how it is we practice.

348
00:19:56,120 --> 00:19:58,800
 So it stays on this intellectual level.

349
00:19:58,800 --> 00:20:03,390
 This may have been a case with this person where they were

350
00:20:03,390 --> 00:20:07,600
 engaged in analyzing the experiences,

351
00:20:07,600 --> 00:20:10,240
 which is different from being mindful of them.

352
00:20:10,240 --> 00:20:11,800
 So there you go.

353
00:20:11,800 --> 00:20:13,520
 Here's the video for tonight.

354
00:20:13,520 --> 00:20:15,040
 Thank you all for tuning in.

