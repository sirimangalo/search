1
00:00:00,000 --> 00:00:05,640
 Hi everyone, welcome back to Ask a Monk.

2
00:00:05,640 --> 00:00:09,090
 I'm getting a lot of different questions here, and I'm

3
00:00:09,090 --> 00:00:10,320
 probably not going to be able to answer

4
00:00:10,320 --> 00:00:12,520
 all of them, I'll try my best.

5
00:00:12,520 --> 00:00:17,350
 But some of the questions I get reoccur or are related, and

6
00:00:17,350 --> 00:00:19,800
 so I'm going to try to answer

7
00:00:19,800 --> 00:00:22,000
 some of them in groups.

8
00:00:22,000 --> 00:00:25,610
 The first group that I often get, and I've got from a long

9
00:00:25,610 --> 00:00:27,600
 time and I've always avoided

10
00:00:27,600 --> 00:00:33,190
 answering, is about monks life and about my life in

11
00:00:33,190 --> 00:00:34,680
 specific.

12
00:00:34,680 --> 00:00:38,430
 And honestly I don't think my life is a particularly good

13
00:00:38,430 --> 00:00:41,080
 example of what a monk should be.

14
00:00:41,080 --> 00:00:46,970
 I've had an interesting and tumultuous monks life, although

15
00:00:46,970 --> 00:00:49,440
 it might be indicative of the

16
00:00:49,440 --> 00:00:54,710
 times and exemplary or a good example of what you have to

17
00:00:54,710 --> 00:00:57,640
 put up with in becoming a monk.

18
00:00:57,640 --> 00:01:03,280
 But becoming a monk is basically giving up all of the

19
00:01:03,280 --> 00:01:06,760
 external attachments that we have

20
00:01:06,760 --> 00:01:10,440
 in favor of a more protected lifestyle.

21
00:01:10,440 --> 00:01:14,600
 And by protected I really mean protected from oneself.

22
00:01:14,600 --> 00:01:22,920
 So a lot of the temptations in an external sense are gone.

23
00:01:22,920 --> 00:01:27,010
 We're surrounded by the Buddha's teaching, we're surrounded

24
00:01:27,010 --> 00:01:29,360
 by meditators, we're surrounded

25
00:01:29,360 --> 00:01:33,320
 by other people doing the same sort of thing or who are

26
00:01:33,320 --> 00:01:36,000
 interested in mental development.

27
00:01:36,000 --> 00:01:39,300
 And so we have a much greater chance of developing

28
00:01:39,300 --> 00:01:42,200
 ourselves in the meditation practice.

29
00:01:42,200 --> 00:01:46,800
 Well at the same time a lot of the things that would tempt

30
00:01:46,800 --> 00:01:49,520
 us to get caught up in addiction

31
00:01:49,520 --> 00:01:54,220
 and get caught up in other ways and in wasting our time are

32
00:01:54,220 --> 00:01:54,960
 gone.

33
00:01:54,960 --> 00:01:59,070
 Of course it's not perfect and it certainly is an external

34
00:01:59,070 --> 00:02:00,560
 aspect of our life.

35
00:02:00,560 --> 00:02:03,550
 There's nothing to say that a monk is immediately going to

36
00:02:03,550 --> 00:02:05,360
 become enlightened or that a lay

37
00:02:05,360 --> 00:02:08,840
 person, an ordinary person can't become enlightened.

38
00:02:08,840 --> 00:02:10,080
 But it certainly helps.

39
00:02:10,080 --> 00:02:13,970
 The reason I became a monk is specifically that, that I

40
00:02:13,970 --> 00:02:16,320
 figured it would protect me from

41
00:02:16,320 --> 00:02:21,040
 a lot of the bad habits and bad tendencies that I had.

42
00:02:21,040 --> 00:02:22,740
 And indeed it has.

43
00:02:22,740 --> 00:02:28,890
 It's been a really good aid for me to allow me to grow

44
00:02:28,890 --> 00:02:32,680
 without getting off track.

45
00:02:32,680 --> 00:02:36,140
 As far as becoming a monk, it can be quite difficult

46
00:02:36,140 --> 00:02:37,120
 nowadays.

47
00:02:37,120 --> 00:02:40,800
 It's hard to find a place that's going to accept you to ord

48
00:02:40,800 --> 00:02:42,920
ain for more than a short period

49
00:02:42,920 --> 00:02:44,640
 of time.

50
00:02:44,640 --> 00:02:49,880
 I myself am very much keen on having people ordain.

51
00:02:49,880 --> 00:02:54,610
 Simply because I don't see much of a difference between the

52
00:02:54,610 --> 00:02:57,520
 two, ordinary life and the monk's

53
00:02:57,520 --> 00:03:00,760
 life since it's simply an externality.

54
00:03:00,760 --> 00:03:03,840
 I mean a person who comes to meditate with me and doesn't

55
00:03:03,840 --> 00:03:05,360
 become a monk and a person

56
00:03:05,360 --> 00:03:07,880
 who comes to become a monk, I don't see the difference.

57
00:03:07,880 --> 00:03:09,720
 I don't differentiate the two.

58
00:03:09,720 --> 00:03:12,120
 If you come to meditate, you come to meditate.

59
00:03:12,120 --> 00:03:14,580
 The reason you become a monk, in my mind, is because you

60
00:03:14,580 --> 00:03:15,960
 want to dedicate yourself to

61
00:03:15,960 --> 00:03:21,200
 long term meditation and following this path.

62
00:03:21,200 --> 00:03:25,450
 So I'm keen to have people ordain, you know, once I become

63
00:03:25,450 --> 00:03:27,920
 eligible to ordain, people are

64
00:03:27,920 --> 00:03:32,540
 going to try to ordain our students as novices first and

65
00:03:32,540 --> 00:03:35,320
 give them the basic precepts.

66
00:03:35,320 --> 00:03:41,750
 The basic precepts of being a monk are chastity and poverty

67
00:03:41,750 --> 00:03:42,160
.

68
00:03:42,160 --> 00:03:49,240
 And the other one is the abstinence from entertainment.

69
00:03:49,240 --> 00:03:51,760
 But the big ones are chastity and poverty.

70
00:03:51,760 --> 00:03:55,330
 So the difference between a meditator and a monk, the monk

71
00:03:55,330 --> 00:03:56,840
 has to give up money.

72
00:03:56,840 --> 00:04:01,960
 The monk has to dedicate themselves to the life of poverty.

73
00:04:01,960 --> 00:04:05,240
 So you're wearing only one set of robes and when I wash

74
00:04:05,240 --> 00:04:07,480
 this one, I wear my second robe.

75
00:04:07,480 --> 00:04:10,800
 You have three robes altogether.

76
00:04:10,800 --> 00:04:14,960
 And not keeping food, not touching money and so on.

77
00:04:14,960 --> 00:04:17,720
 So using things which are of benefit.

78
00:04:17,720 --> 00:04:22,700
 I mean I even go so far as using obviously a computer, but

79
00:04:22,700 --> 00:04:24,840
 not keeping things and not

80
00:04:24,840 --> 00:04:29,320
 clinging to things and not living luxuriously.

81
00:04:29,320 --> 00:04:30,520
 So you sleep on the floor.

82
00:04:30,520 --> 00:04:34,090
 But you know, a lot of this is very similar to how medit

83
00:04:34,090 --> 00:04:35,080
ators live.

84
00:04:35,080 --> 00:04:39,040
 The difference is a meditator is short term and for a monk

85
00:04:39,040 --> 00:04:40,400
 it's long term.

86
00:04:40,400 --> 00:04:44,170
 As far as ceremonies and preparing and so on and so on, I

87
00:04:44,170 --> 00:04:46,680
 mean that's really just technical

88
00:04:46,680 --> 00:04:47,680
 details.

89
00:04:47,680 --> 00:04:50,160
 The ordination of a monk is not a ceremony.

90
00:04:50,160 --> 00:04:51,960
 It shouldn't be considered to be a ceremony.

91
00:04:51,960 --> 00:04:53,240
 It's an act.

92
00:04:53,240 --> 00:04:56,680
 It's a formal act of the Buddhist monks.

93
00:04:56,680 --> 00:05:00,120
 It's like the inauguration of a president or so on or of a

94
00:05:00,120 --> 00:05:01,600
 member of Congress.

95
00:05:01,600 --> 00:05:05,750
 You have to go through a certain process, screening process

96
00:05:05,750 --> 00:05:07,920
 and a confirmation process.

97
00:05:07,920 --> 00:05:11,920
 And once you've gone through that, then you can ordain.

98
00:05:11,920 --> 00:05:14,970
 It shouldn't be considered a ritual, a ceremony or some

99
00:05:14,970 --> 00:05:16,920
 kind of religious observance.

100
00:05:16,920 --> 00:05:22,610
 It's joining a club, so to speak, or joining a society and

101
00:05:22,610 --> 00:05:25,400
 you have to be screened and so

102
00:05:25,400 --> 00:05:26,400
 on.

103
00:05:26,400 --> 00:05:28,880
 Okay, so those are some thoughts on the monk's life.

104
00:05:28,880 --> 00:05:33,720
 One last thing that I'd like to do is give you a link to a

105
00:05:33,720 --> 00:05:36,440
 very good, something that's

106
00:05:36,440 --> 00:05:38,880
 much better than anything I could say on the monk's life.

107
00:05:38,880 --> 00:05:42,800
 It's very poetic and very to the point.

108
00:05:42,800 --> 00:05:46,010
 It's something that I always like to read again and again,

109
00:05:46,010 --> 00:05:47,440
 written by a novice monk

110
00:05:47,440 --> 00:05:54,400
 actually who ordained when he was old and wrote this in

111
00:05:54,400 --> 00:05:58,680
 defense of the monastic life.

112
00:05:58,680 --> 00:06:03,680
 Okay, so there's a link in the description of this video.

113
00:06:03,680 --> 00:06:06,800
 Please follow that link if you're interested in

114
00:06:06,800 --> 00:06:09,160
 understanding the real nature of going

115
00:06:09,160 --> 00:06:12,300
 forth of leaving behind the home life, which is what we do

116
00:06:12,300 --> 00:06:13,360
 when we ordain.

117
00:06:13,360 --> 00:06:14,880
 Okay, so thanks for the questions.

118
00:06:14,880 --> 00:06:17,940
 I'll try to get to some more of them tonight and in the

119
00:06:17,940 --> 00:06:19,040
 next few days.

120
00:06:19,040 --> 00:06:22,840
 Of course, I'm busy in real life, but I'll do my best.

