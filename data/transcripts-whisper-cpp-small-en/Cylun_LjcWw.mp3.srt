1
00:00:00,000 --> 00:00:07,000
 Okay, good evening everyone. Welcome to our live broadcast.

2
00:00:07,000 --> 00:00:12,320
 Broadcasting live in Second

3
00:00:12,320 --> 00:00:20,400
 Life, live on our website via audio and recording for

4
00:00:20,400 --> 00:00:24,400
 upload to YouTube. And of course live

5
00:00:24,400 --> 00:00:31,420
 here in Hamilton to a live studio audience. We're over full

6
00:00:31,420 --> 00:00:34,720
. We now have seven people

7
00:00:34,720 --> 00:00:39,510
 staying in this house. No, do we? We have six people right

8
00:00:39,510 --> 00:00:41,440
 now and tonight we'll have

9
00:00:41,440 --> 00:00:51,100
 a seventh supposed to be coming. We're over full. Just can

10
00:00:51,100 --> 00:00:55,040
't turn people away. Just can't

11
00:00:55,040 --> 00:01:02,240
 keep people. They're breaking down the doors to get in.

12
00:01:02,240 --> 00:01:05,000
 That's how passionate people become.

13
00:01:05,000 --> 00:01:08,850
 Religion is such a powerful thing. No, the word religion

14
00:01:08,850 --> 00:01:10,720
 has such a bad reputation or

15
00:01:10,720 --> 00:01:14,340
 such a specific reputation that makes a lot of people

16
00:01:14,340 --> 00:01:16,720
 uneasy when they hear the word.

17
00:01:16,720 --> 00:01:21,630
 So I think it's important to reform the word. Religion

18
00:01:21,630 --> 00:01:24,720
 means taking things seriously. So

19
00:01:24,720 --> 00:01:28,790
 tonight's talk is about someone who, well not actually so

20
00:01:28,790 --> 00:01:30,800
 much about him hopefully,

21
00:01:30,800 --> 00:01:38,600
 but it involves a discussion with someone who took Buddhism

22
00:01:38,600 --> 00:01:42,280
 quite seriously, took the

23
00:01:42,280 --> 00:01:47,270
 Buddhist teachings quite seriously. And it stands out as

24
00:01:47,270 --> 00:01:49,640
 someone who is remarkable in

25
00:01:49,640 --> 00:01:56,810
 terms of taking the Buddhist teachings seriously. He was

26
00:01:56,810 --> 00:02:01,240
 designated by the Buddha as foremost

27
00:02:01,240 --> 00:02:05,400
 of the Buddhist disciples who went forth, who left the home

28
00:02:05,400 --> 00:02:08,360
 life out of faith, out of

29
00:02:08,360 --> 00:02:15,860
 faith. So who had a religious sense of religiosity. His

30
00:02:15,860 --> 00:02:20,920
 name was Ratapala. You know the story of

31
00:02:20,920 --> 00:02:30,360
 Ratapala. He comes in in Jiminikaya, Suta 82. It's called

32
00:02:30,360 --> 00:02:34,240
 Ratapala Suta. So the story

33
00:02:34,240 --> 00:02:38,800
 goes Ratapala heard the Buddhist teaching and realized that

34
00:02:38,800 --> 00:02:40,800
 it was quite difficult for him

35
00:02:40,800 --> 00:02:45,050
 to practice it while living at home. He thought to himself,

36
00:02:45,050 --> 00:02:47,000
 "Wow, this sort of teaching isn't

37
00:02:47,000 --> 00:02:50,990
 the kind of thing you can do surrounded by sensuality and

38
00:02:50,990 --> 00:02:52,800
 caught up by daily affairs

39
00:02:52,800 --> 00:02:58,430
 of the 'real world.'" It's not the real world, but what

40
00:02:58,430 --> 00:03:00,440
 people would call the real

41
00:03:00,440 --> 00:03:06,800
 world. The ordinary mundane contrived artificial world of

42
00:03:06,800 --> 00:03:10,000
 society that we've put together to

43
00:03:10,000 --> 00:03:15,840
 help us achieve our goals of sensual pleasure. Not so

44
00:03:15,840 --> 00:03:20,120
 useful for becoming enlightened, too

45
00:03:20,120 --> 00:03:28,390
 busy, too caught up in defilement. So he decided he wanted

46
00:03:28,390 --> 00:03:29,760
 to go forth. He wanted to become

47
00:03:29,760 --> 00:03:35,180
 a monk, but he asked the Buddha to ordain him and the Buddh

48
00:03:35,180 --> 00:03:37,560
ists asked him, "Do you have

49
00:03:37,560 --> 00:03:41,580
 your parents' permission?" He said, "No, I don't have my

50
00:03:41,580 --> 00:03:43,620
 parents' permission." The

51
00:03:43,620 --> 00:03:46,070
 Buddha said, "Well, I don't ordain people who don't have

52
00:03:46,070 --> 00:03:47,440
 their parents' permission."

53
00:03:47,440 --> 00:03:50,650
 So he had to go back to get his parents' permission. To

54
00:03:50,650 --> 00:03:54,920
 make a long story short, his parents didn't

55
00:03:54,920 --> 00:04:00,000
 give permission and he lay down on the floor. He pleaded

56
00:04:00,000 --> 00:04:03,480
 and begged with them, but eventually

57
00:04:03,480 --> 00:04:06,810
 he lay down on the floor and said, "I'm not going to eat or

58
00:04:06,810 --> 00:04:08,640
 drink or do anything. I'm

59
00:04:08,640 --> 00:04:15,420
 not going to get up off this floor until you allow me to

60
00:04:15,420 --> 00:04:18,080
 ordain." So they waited him out

61
00:04:18,080 --> 00:04:20,920
 for a while, but then he made good on his threat and he

62
00:04:20,920 --> 00:04:23,000
 just lay there and started starving

63
00:04:23,000 --> 00:04:27,320
 to death. They called his friends to try and convince him.

64
00:04:27,320 --> 00:04:28,600
 His friends came over, talked

65
00:04:28,600 --> 00:04:31,890
 to him and they went and talked to his parents and said, "

66
00:04:31,890 --> 00:04:33,840
Look, he's pretty serious about

67
00:04:33,840 --> 00:04:37,050
 this. How about this? If you let him ordain, at least you

68
00:04:37,050 --> 00:04:38,760
'll get to see him alive. You

69
00:04:38,760 --> 00:04:42,270
 can go visit him, but if you don't let him ordain, he's

70
00:04:42,270 --> 00:04:44,360
 going to die. Then you won't

71
00:04:44,360 --> 00:04:50,860
 see him. Then he'll be gone. So let him ordain." The story

72
00:04:50,860 --> 00:04:52,480
 goes on and on. Tonight I didn't

73
00:04:52,480 --> 00:04:57,590
 want to talk so much about his story, as interesting and as

74
00:04:57,590 --> 00:04:59,960
 inspiring as it is. Later on in his

75
00:04:59,960 --> 00:05:03,940
 life he was living, I can't remember where he was living,

76
00:05:03,940 --> 00:05:06,000
 but he went somewhere and met

77
00:05:06,000 --> 00:05:16,330
 with a king. The king asks him, "Let's see if I can find

78
00:05:16,330 --> 00:05:18,880
 him. Korabia, yeah. Okay. I

79
00:05:18,880 --> 00:05:23,220
 was going to say Korabia was the name of this king or the

80
00:05:23,220 --> 00:05:25,840
 name of the place where the king

81
00:05:25,840 --> 00:05:32,640
 lived. I think he was actually the name of the place." The

82
00:05:32,640 --> 00:05:37,280
 king came to see him and he

83
00:05:37,280 --> 00:05:41,460
 said to Ratapala, he said, "I know people who leave the

84
00:05:41,460 --> 00:05:43,680
 home life and go to the forest

85
00:05:43,680 --> 00:05:47,480
 to do their religious thing and they do it for one of four

86
00:05:47,480 --> 00:05:49,680
 reasons. They do it because

87
00:05:49,680 --> 00:05:57,090
 they've gotten old. They do it because they've gotten sick.

88
00:05:57,090 --> 00:06:01,160
 They do it because they've lost

89
00:06:01,160 --> 00:06:08,530
 wealth or they do it because they've lost their relatives.

90
00:06:08,530 --> 00:06:11,880
 So someone who is old can

91
00:06:11,880 --> 00:06:20,930
 no longer work and is feeble and can't find any sort of sol

92
00:06:20,930 --> 00:06:26,120
ace or status or work or activity

93
00:06:26,120 --> 00:06:29,640
 in the world of young people and so they go off into the

94
00:06:29,640 --> 00:06:31,680
 forest and become a religious

95
00:06:31,680 --> 00:06:35,290
 person. This actually happens even in Buddhism. Old people

96
00:06:35,290 --> 00:06:36,880
 become monks and it's somewhat

97
00:06:36,880 --> 00:06:40,820
 troublesome because they often get sick. That's the second

98
00:06:40,820 --> 00:06:41,840
 one is people get sick and then

99
00:06:41,840 --> 00:06:44,460
 they want to go off into the forest while there's not much

100
00:06:44,460 --> 00:06:45,840
 they could do in India at

101
00:06:45,840 --> 00:06:49,590
 the time. Nowadays we just put them in old age homes and

102
00:06:49,590 --> 00:06:50,880
 forget about them. But same

103
00:06:50,880 --> 00:06:54,240
 sort of thing. They didn't have old age homes. They just

104
00:06:54,240 --> 00:06:56,160
 sent them off into the forest to

105
00:06:56,160 --> 00:07:05,650
 live or to die. Or someone who loses wealth and become

106
00:07:05,650 --> 00:07:07,280
 impoverished and have to become

107
00:07:07,280 --> 00:07:12,730
 a beggar and have to live under a tree or in a ditch or

108
00:07:12,730 --> 00:07:16,520
 something. Also common then. Also

109
00:07:16,520 --> 00:07:23,880
 happens now. Or loss of relatives. Relatives who maybe took

110
00:07:23,880 --> 00:07:26,000
 care of the person or maybe

111
00:07:26,000 --> 00:07:30,210
 just out of grief they would realize the suffering of life

112
00:07:30,210 --> 00:07:33,600
 and leave the world. And he said,

113
00:07:33,600 --> 00:07:36,330
 "But you, you're young, you're healthy, you come from a

114
00:07:36,330 --> 00:07:37,840
 good family and they're all still

115
00:07:37,840 --> 00:07:44,120
 alive. What was it that caused you to leave home? Why did

116
00:07:44,120 --> 00:07:47,480
 you leave behind the world?"

117
00:07:47,480 --> 00:07:50,470
 It's a good question. With all the wonderful things that we

118
00:07:50,470 --> 00:07:51,800
 have in the world, why do we

119
00:07:51,800 --> 00:07:57,060
 leave it? Why do we come here? Why do you come here to

120
00:07:57,060 --> 00:07:59,400
 torture yourself? Even just to

121
00:07:59,400 --> 00:08:04,370
 meditate people will go to the monasteries, temples,

122
00:08:04,370 --> 00:08:08,040
 churches when something bad happens.

123
00:08:08,040 --> 00:08:13,600
 So it's remarkable to see someone who has their whole life

124
00:08:13,600 --> 00:08:16,240
 ahead of them, couldn't be

125
00:08:16,240 --> 00:08:19,550
 capable of so much in the world. This is why Ratapala's

126
00:08:19,550 --> 00:08:21,480
 parents wouldn't allow him. They

127
00:08:21,480 --> 00:08:25,620
 were quite upset that he wanted to throw away his wonderful

128
00:08:25,620 --> 00:08:28,080
 future. All the wonderful good

129
00:08:28,080 --> 00:08:33,170
 things he could do, right? They wanted to do this useless

130
00:08:33,170 --> 00:08:36,000
 thing of becoming a reckless,

131
00:08:36,000 --> 00:08:45,080
 becoming a Buddhist monk. How useless they thought. And Rat

132
00:08:45,080 --> 00:08:47,640
apala's answer is well known

133
00:08:47,640 --> 00:08:51,990
 in the Buddha's dispensation. He claims that it comes from

134
00:08:51,990 --> 00:08:54,120
 the Buddha. Now I'm not sure

135
00:08:54,120 --> 00:08:57,010
 if we actually have somewhere where the Buddha said this.

136
00:08:57,010 --> 00:08:58,920
 We very well may have. But Ratapala

137
00:08:58,920 --> 00:09:02,100
 is the one who is known for giving it. He says it comes

138
00:09:02,100 --> 00:09:03,800
 from the Buddha. He said there are

139
00:09:03,800 --> 00:09:06,090
 these four dhamma-dheisa. And dhamma-dheisa comes from dhis

140
00:09:06,090 --> 00:09:12,240
a. Dhisa means to indicate.

141
00:09:12,240 --> 00:09:18,720
 Dika. Disa, it's the same. So the things the Buddha pointed

142
00:09:18,720 --> 00:09:20,120
 out. Four dhamma-dheisa, four

143
00:09:20,120 --> 00:09:28,160
 indicators. Dhamma indicators. And when these were pointed

144
00:09:28,160 --> 00:09:31,600
 out to me, I realized that I

145
00:09:31,600 --> 00:09:38,320
 had to leave. I had to do something. I think these are

146
00:09:38,320 --> 00:09:40,400
 quite useful, not just for encouraging

147
00:09:40,400 --> 00:09:43,870
 people to take up meditation or take up Buddhism, but also

148
00:09:43,870 --> 00:09:46,040
 to encourage us in our practice and

149
00:09:46,040 --> 00:09:50,050
 to remind us why we're here when we get discouraged. And to

150
00:09:50,050 --> 00:09:52,520
 sort of focus our attention on what's

151
00:09:52,520 --> 00:10:00,020
 really important, clarify and keep us on track. So the

152
00:10:00,020 --> 00:10:07,520
 first is upaniyati loko adhu. The world

153
00:10:07,520 --> 00:10:14,190
 is uncertain. The world is unstable. So all these good

154
00:10:14,190 --> 00:10:18,080
 things, I mean just the fact that

155
00:10:18,080 --> 00:10:20,980
 those losses exist. You don't need to experience the loss

156
00:10:20,980 --> 00:10:22,680
 to know that that's a part of life,

157
00:10:22,680 --> 00:10:28,680
 to know that it's a danger. And to start to question, what

158
00:10:28,680 --> 00:10:31,480
 am I doing here? Wasting my

159
00:10:31,480 --> 00:10:36,500
 time when I certainly won't be prepared for old age, for

160
00:10:36,500 --> 00:10:43,000
 sickness, for loss, for death.

161
00:10:43,000 --> 00:10:48,290
 The uncertainty of life, all the things that we hold dear,

162
00:10:48,290 --> 00:10:51,040
 even our own selves, even our

163
00:10:51,040 --> 00:10:58,420
 own families, our own situation, everything about us is

164
00:10:58,420 --> 00:11:11,640
 unpredictable, chaotic. That's

165
00:11:11,640 --> 00:11:15,140
 the first reason why one would go off and become a monk or

166
00:11:15,140 --> 00:11:17,440
 go off and practice meditation.

167
00:11:17,440 --> 00:11:32,660
 The second one is ata no loko anabisaro. This world has no

168
00:11:32,660 --> 00:11:38,040
 refuge, no master or guardian

169
00:11:38,040 --> 00:11:42,690
 protector. And the king questions him on all these. He says

170
00:11:42,690 --> 00:11:45,280
, "Well, what do you mean? It

171
00:11:45,280 --> 00:11:50,030
 seems very constant. Here I'm the king and life is very

172
00:11:50,030 --> 00:11:54,520
 constant for me." They say, "Well,

173
00:11:54,520 --> 00:11:58,320
 do you remember when you were young and you could shoot an

174
00:11:58,320 --> 00:12:00,400
 arrow or ride a horse? Can you

175
00:12:00,400 --> 00:12:04,860
 do that now? Oh, impermanence, you see. Old age comes to us

176
00:12:04,860 --> 00:12:06,920
 all seeing this and knowing

177
00:12:06,920 --> 00:12:10,580
 that this is the case. Go forth." And then he says, "Well,

178
00:12:10,580 --> 00:12:12,360
 what about this? No protector.

179
00:12:12,360 --> 00:12:15,300
 What do you mean? I've got elephants, I've got warriors, I

180
00:12:15,300 --> 00:12:16,880
've got lots and lots of refuge

181
00:12:16,880 --> 00:12:19,550
 and protectors." He says, "Well, what if you get sick?

182
00:12:19,550 --> 00:12:21,360
 Suppose you get really sick, are

183
00:12:21,360 --> 00:12:24,910
 your elephants going to protect you from the sickness? Is

184
00:12:24,910 --> 00:12:27,120
 your family, your warriors, going

185
00:12:27,120 --> 00:12:30,620
 to stand around you and keep the sickness away? No, no,

186
00:12:30,620 --> 00:12:32,520
 there's nobody who can protect

187
00:12:32,520 --> 00:12:38,470
 you." Right? And when we're young, this is one of the big

188
00:12:38,470 --> 00:12:40,520
 shatterings of youth, the realization

189
00:12:40,520 --> 00:12:44,740
 that our parents can't protect us from suffering. It's a

190
00:12:44,740 --> 00:12:47,680
 shock of growing up. The realization

191
00:12:47,680 --> 00:12:59,840
 that suffering is vulnerable to suffering and not protected

192
00:12:59,840 --> 00:13:00,960
 from it, we're not sheltered

193
00:13:00,960 --> 00:13:10,880
 from it. There's nothing that can shelter us from it.

194
00:13:10,880 --> 00:13:17,760
 Number three, "Asakoloko sabangbahaya

195
00:13:17,760 --> 00:13:25,580
 gamani yanti." This world is not an owner. It has no

196
00:13:25,580 --> 00:13:31,440
 ownership. It's nothing of its own.

197
00:13:31,440 --> 00:13:36,080
 It goes having abandoned everything. The king said, "Well,

198
00:13:36,080 --> 00:13:38,160
 what do you mean? I can go with

199
00:13:38,160 --> 00:13:43,570
 everything full jewels and we have so much that is ours,

200
00:13:43,570 --> 00:13:46,640
 right? We've got our cars and

201
00:13:46,640 --> 00:13:51,540
 our houses and our families and our friends and our iPhones

202
00:13:51,540 --> 00:13:54,720
 and our computers and everything.

203
00:13:54,720 --> 00:14:00,220
 We've got so much, it's all ours. We go with it. No, you

204
00:14:00,220 --> 00:14:03,600
 don't go with any of it." He says,

205
00:14:03,600 --> 00:14:06,160
 "Do you know whether you're going to be king in the next

206
00:14:06,160 --> 00:14:07,760
 life? Do you know whether you're

207
00:14:07,760 --> 00:14:10,580
 going to be human in the next life? Do you even enjoy the

208
00:14:10,580 --> 00:14:12,240
 riches? Maybe you'll be born

209
00:14:12,240 --> 00:14:18,580
 a hungry ghost guarding your own treasure or maybe you'll

210
00:14:18,580 --> 00:14:22,240
 be born an ant or a termite

211
00:14:22,240 --> 00:14:30,540
 living in the wood wall of the palace. We're not just in

212
00:14:30,540 --> 00:14:37,120
 the next life. All of our possessions,

213
00:14:37,120 --> 00:14:41,090
 right? We can't hold on to them. We can't say, "Let this be

214
00:14:41,090 --> 00:14:43,600
 mine forever. Let this car or this

215
00:14:43,600 --> 00:14:50,290
 house or this iPhone, let it be mine." Even our friends and

216
00:14:50,290 --> 00:14:53,440
 family and even our own bodies

217
00:14:53,440 --> 00:14:57,390
 don't really belong to us. We're borrowing them and not

218
00:14:57,390 --> 00:15:00,080
 even borrowing them because we don't know

219
00:15:00,080 --> 00:15:06,200
 when they're going to be taken back. It's like we've stolen

220
00:15:06,200 --> 00:15:07,680
 them and we're just waiting for the

221
00:15:07,680 --> 00:15:12,410
 police to come and arrest us. Wait until we get caught. It

222
00:15:12,410 --> 00:15:14,720
's like we're living on stolen time.

223
00:15:14,720 --> 00:15:18,840
 So we got to get as much pleasure and use out of it, joy

224
00:15:18,840 --> 00:15:21,040
ride in this body that we have and then

225
00:15:21,040 --> 00:15:28,380
 death comes and catches us and wham takes it away. We don't

226
00:15:28,380 --> 00:15:32,000
 know when that's going to be. We're just

227
00:15:32,000 --> 00:15:37,600
 trying to run away. We're on the lam. We're on the run.

228
00:15:37,600 --> 00:15:43,840
 Death is on our heels. That's a good analogy.

229
00:15:48,400 --> 00:15:53,360
 And number four, Uno Loco Atito Tanha Dasu.

230
00:15:53,360 --> 00:16:05,520
 Una. Una means the same. That's where the word want and una

231
00:16:05,520 --> 00:16:08,320
 is from the same place.

232
00:16:08,320 --> 00:16:14,800
 Una means wanting. This world is wanting, is lacking.

233
00:16:18,080 --> 00:16:20,960
 Atita, unsatisfied.

234
00:16:20,960 --> 00:16:30,400
 Tanha Dasu, slave of tanha, of craving.

235
00:16:30,400 --> 00:16:37,740
 And he says, "What do you mean? I can satisfy myself

236
00:16:37,740 --> 00:16:38,960
 anytime.

237
00:16:38,960 --> 00:16:44,450
 I get whatever I want. I'm always satisfied. I eat and then

238
00:16:44,450 --> 00:16:45,760
 I'm satisfied.

239
00:16:45,760 --> 00:16:52,220
 And the Buddha says, "Well, what if you heard about another

240
00:16:52,220 --> 00:16:52,960
 kingdom that was

241
00:16:52,960 --> 00:16:57,820
 weak but had lots of resources and they told you that it

242
00:16:57,820 --> 00:16:59,520
 was ready right for the taking?

243
00:16:59,520 --> 00:17:03,440
 What would you do? Oh, I'd conquer it. And what about

244
00:17:03,440 --> 00:17:05,360
 another one and another one?

245
00:17:05,360 --> 00:17:12,310
 You never have enough." The Buddha said, "It could rain

246
00:17:12,310 --> 00:17:13,200
 gold.

247
00:17:14,560 --> 00:17:16,880
 It could rain gold or precious jewels.

248
00:17:16,880 --> 00:17:23,800
 And the shower of them would never be enough for one being

249
00:17:23,800 --> 00:17:24,480
."

250
00:17:24,480 --> 00:17:28,580
 It's the nature of craving. The very nature of craving is

251
00:17:28,580 --> 00:17:34,160
 that it's habitual. It's ever increasing.

252
00:17:34,160 --> 00:17:42,760
 It's self-perpetuating or self-enforcing or it feeds itself

253
00:17:42,760 --> 00:17:44,480
. It becomes stronger the more

254
00:17:44,480 --> 00:17:49,870
 you engage in it. It's a very scary thing. We're always

255
00:17:49,870 --> 00:17:51,040
 unsatisfied.

256
00:17:51,040 --> 00:17:57,200
 One that we can never be satisfied.

257
00:17:57,200 --> 00:18:04,960
 Seeing this many people leave home when they realize how

258
00:18:04,960 --> 00:18:07,360
 unsatisfied they are,

259
00:18:07,360 --> 00:18:09,620
 how they can eat and eat and all they're gaining is more

260
00:18:09,620 --> 00:18:10,480
 and more craving.

261
00:18:12,000 --> 00:18:14,730
 It's one of the big things you realize here is how attached

262
00:18:14,730 --> 00:18:17,040
 we are to diversion,

263
00:18:17,040 --> 00:18:21,910
 how unable we are just to be with ourselves, just to have

264
00:18:21,910 --> 00:18:25,360
 the patience to be.

265
00:18:25,360 --> 00:18:28,630
 It's such a simple thing. You think, "Well, that's easy."

266
00:18:28,630 --> 00:18:32,080
 But isn't it ridiculous how we can't just be?

267
00:18:33,920 --> 00:18:42,800
 We can't be ourselves. We have to do, make, create, get. It

268
00:18:42,800 --> 00:18:46,240
's quite surprising because

269
00:18:46,240 --> 00:18:50,100
 I think we generally have this idea that we can just be

270
00:18:50,100 --> 00:18:54,480
 that we always are. But we're not good at

271
00:18:54,480 --> 00:19:01,020
 "are-ing." We're not good at being. We have to do. We have

272
00:19:01,020 --> 00:19:04,720
 to get so much craving and aversion.

273
00:19:04,720 --> 00:19:10,000
 There's so many inbuilt reactions to everything.

274
00:19:10,000 --> 00:19:15,570
 So realizing this, realizing all four of these things, this

275
00:19:15,570 --> 00:19:16,800
 is what leads one to

276
00:19:18,640 --> 00:19:22,770
 go forth to become a monk, to practice meditation. This is

277
00:19:22,770 --> 00:19:25,760
 the encouragement, the reminder to all of us

278
00:19:25,760 --> 00:19:33,440
 that we're here for a reason, we're here to figure this out

279
00:19:33,440 --> 00:19:35,040
, to solve this problem,

280
00:19:35,040 --> 00:19:40,350
 the problem of impermanence and let the world, any goal we

281
00:19:40,350 --> 00:19:45,200
 might attain, any object we might acquire,

282
00:19:46,880 --> 00:19:51,590
 anything we might become will always be unstable, uncertain

283
00:19:51,590 --> 00:19:53,360
, impermanent.

284
00:19:53,360 --> 00:19:59,470
 That there's nothing that can keep us from the vicissitudes

285
00:19:59,470 --> 00:20:01,680
 of life. There's no protection.

286
00:20:01,680 --> 00:20:04,280
 There's no refuge. There's nowhere we can go. Nothing we

287
00:20:04,280 --> 00:20:10,480
 can do to avoid the vicissitudes of life.

288
00:20:15,520 --> 00:20:21,320
 There's that all of our possessions can't protect us, all

289
00:20:21,320 --> 00:20:21,680
 of our

290
00:20:21,680 --> 00:20:27,520
 friends and family. We leave them all behind.

291
00:20:27,520 --> 00:20:31,010
 That all the things we hold dear and all the things we

292
00:20:31,010 --> 00:20:32,880
 crave and love and cling to

293
00:20:32,880 --> 00:20:37,790
 are not ours. They'll be gone. We'll be gone from them soon

294
00:20:37,790 --> 00:20:38,400
 enough.

295
00:20:42,720 --> 00:20:46,670
 And finally that craving, we are slaves of craving, slaves

296
00:20:46,670 --> 00:20:47,520
 of desire.

297
00:20:47,520 --> 00:20:51,870
 We are not masters of our desire. Our desires are not our

298
00:20:51,870 --> 00:20:53,840
 possession that we can control.

299
00:20:53,840 --> 00:20:58,200
 They are the master. We are the slave. Very scary reality

300
00:20:58,200 --> 00:21:00,240
 and these scary realities

301
00:21:00,240 --> 00:21:04,790
 encourage us because there is something that can protect us

302
00:21:04,790 --> 00:21:07,040
. There is a refuge and that's the

303
00:21:07,040 --> 00:21:12,190
 dumbness of the truth, reality, seeing things as they are.

304
00:21:12,190 --> 00:21:15,200
 Once we learn how to just be

305
00:21:15,200 --> 00:21:22,700
 when we become content, we become stable, we become

306
00:21:22,700 --> 00:21:24,800
 invincible.

307
00:21:24,800 --> 00:21:29,670
 We know there's no coming or going and we free ourselves

308
00:21:29,670 --> 00:21:31,440
 from suffering.

309
00:21:35,680 --> 00:21:38,670
 So there you go. There's the Dhamma for tonight. Fine

310
00:21:38,670 --> 00:21:42,320
 teaching handed on by Ratapala, the

311
00:21:42,320 --> 00:21:46,340
 Buddhist monk with the greatest faith and the greatest

312
00:21:46,340 --> 00:21:49,360
 confidence in going forth.

313
00:21:49,360 --> 00:21:53,330
 No doubt in his mind he would have done anything. He would

314
00:21:53,330 --> 00:21:56,800
 have died just to do what we're doing.

315
00:21:59,120 --> 00:22:05,560
 It's a good example of religiosity and how wonderful and

316
00:22:05,560 --> 00:22:07,600
 powerful it can be.

317
00:22:07,600 --> 00:22:13,750
 So there you go. Thank you for tuning in. Have a good night

318
00:22:13,750 --> 00:22:13,760
.

