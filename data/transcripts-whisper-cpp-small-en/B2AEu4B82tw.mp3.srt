1
00:00:00,000 --> 00:00:07,000
 >> Good evening.

2
00:00:07,000 --> 00:00:25,000
 [ Silence ]

3
00:00:25,000 --> 00:00:34,000
 Good evening.

4
00:00:34,000 --> 00:00:39,000
 So we're starting up again. I've been away for most of the

5
00:00:39,000 --> 00:00:52,000
 month, the past month.

6
00:00:52,000 --> 00:00:59,000
 So tonight we have two new meditators staying here.

7
00:00:59,000 --> 00:01:25,000
 And as usual, it's a chance to -- an opportunity to --

8
00:01:25,000 --> 00:01:39,000
 go over some of the basics. So I thought it might be a --

9
00:01:39,000 --> 00:01:46,000
 a good idea to do something fairly traditional.

10
00:01:46,000 --> 00:01:56,740
 And so what I'm going to do actually is give you the Pali

11
00:01:56,740 --> 00:01:58,000
 words

12
00:01:58,000 --> 00:02:02,000
 that describe what it is that we do.

13
00:02:02,000 --> 00:02:13,460
 So it's just really a reminder of what it is that we're

14
00:02:13,460 --> 00:02:18,000
 here to practice.

15
00:02:18,000 --> 00:02:21,600
 And so there are -- there are only six words. It's not a

16
00:02:21,600 --> 00:02:24,000
 lot, but we recommend

17
00:02:24,000 --> 00:02:33,000
 that you remember these six words in your practice.

18
00:02:33,000 --> 00:02:39,220
 Of course, it helps if you understand Pali, but some of

19
00:02:39,220 --> 00:02:44,000
 them are quite familiar words.

20
00:02:44,000 --> 00:02:48,000
 So if I can have you guys repeat after me, okay?

21
00:02:48,000 --> 00:02:49,580
 I don't have them written out here. Maybe in the future I

22
00:02:49,580 --> 00:02:51,000
'll have them written out.

23
00:02:51,000 --> 00:02:54,580
 But just repeat if you can humor me with this, and I'll

24
00:02:54,580 --> 00:02:56,000
 explain what they are.

25
00:02:56,000 --> 00:03:03,000
 So the first word is "vipassana." Can you say that?

26
00:03:03,000 --> 00:03:10,000
 So it's more with a "w" than a "vipassana."

27
00:03:10,000 --> 00:03:21,000
 The second one is "sati patana."

28
00:03:21,000 --> 00:03:43,000
 "Kaya vedana chitta dhamma vipassana satipatana kaya vedana

29
00:03:43,000 --> 00:03:50,000
 chitta dhamma vipassana satipatana"

30
00:03:50,000 --> 00:04:01,000
 "Kaya vedana chitta dhamma."

31
00:04:01,000 --> 00:04:05,190
 So my teacher used to make everyone -- or still does make

32
00:04:05,190 --> 00:04:08,000
 everyone -- memorize these.

33
00:04:08,000 --> 00:04:13,640
 This is the memorization portion of the learning for new

34
00:04:13,640 --> 00:04:15,000
 students.

35
00:04:15,000 --> 00:04:19,850
 Your students, you come here and you're like kindergarten

36
00:04:19,850 --> 00:04:21,000
 students.

37
00:04:21,000 --> 00:04:26,710
 Now I think both of you have background and some basis in

38
00:04:26,710 --> 00:04:30,000
 meditation, but it doesn't matter.

39
00:04:30,000 --> 00:04:36,000
 We put you in kindergarten anyway.

40
00:04:36,000 --> 00:04:40,540
 And that's fine. Learning basics again and again is always

41
00:04:40,540 --> 00:04:41,000
 good.

42
00:04:41,000 --> 00:04:47,390
 Because actually these six words describe a complete

43
00:04:47,390 --> 00:04:49,000
 practice.

44
00:04:49,000 --> 00:04:55,400
 They aren't just the basic beginner steps that you start

45
00:04:55,400 --> 00:05:04,000
 with and then move on to something else.

46
00:05:04,000 --> 00:05:07,660
 So I brought up my teacher because one thing interesting

47
00:05:07,660 --> 00:05:10,000
 that he always says is just memorizing them like that.

48
00:05:10,000 --> 00:05:13,000
 It's a very good thing.

49
00:05:13,000 --> 00:05:21,670
 It's interesting we skip over this quality of just being

50
00:05:21,670 --> 00:05:23,000
 here.

51
00:05:23,000 --> 00:05:29,610
 So much appreciation to you too for being here because just

52
00:05:29,610 --> 00:05:36,000
 being here has the great intention or the great aspiration.

53
00:05:36,000 --> 00:05:42,000
 That's very much worthy of appreciation.

54
00:05:42,000 --> 00:05:49,000
 Your intentions on coming here are wonderful.

55
00:05:49,000 --> 00:05:54,400
 And on memorizing, there's a similar quality to memorizing

56
00:05:54,400 --> 00:05:58,000
 these teachings because you've made it.

57
00:05:58,000 --> 00:06:03,000
 You've made it to the practice.

58
00:06:03,000 --> 00:06:10,000
 And you've now come in contact with the practice.

59
00:06:10,000 --> 00:06:16,110
 Even though you don't understand these six words, just

60
00:06:16,110 --> 00:06:26,250
 hearing them and reciting them is enough to be reborn in

61
00:06:26,250 --> 00:06:31,000
 heaven just for your good intentions.

62
00:06:31,000 --> 00:06:35,580
 If you don't believe that sort of thing, it's important to

63
00:06:35,580 --> 00:06:39,480
 understand that that's a big part of what we're doing is

64
00:06:39,480 --> 00:06:42,000
 cultivating good intentions.

65
00:06:42,000 --> 00:06:49,210
 And our good thoughts, our good positive states of mind, or

66
00:06:49,210 --> 00:06:54,790
 even just our appreciation of the Dhamma, our willingness

67
00:06:54,790 --> 00:06:57,000
 to learn the Dhammas.

68
00:06:57,000 --> 00:07:02,000
 These are important qualities to cultivate.

69
00:07:02,000 --> 00:07:04,510
 Of course, it's not enough to become enlightened, so you

70
00:07:04,510 --> 00:07:07,000
 have to actually understand what these words mean.

71
00:07:07,000 --> 00:07:12,000
 Vipassana starting at the top.

72
00:07:12,000 --> 00:07:17,280
 Vipassana is often translated as insight. I don't think

73
00:07:17,280 --> 00:07:21,000
 that's the best translation of it.

74
00:07:21,000 --> 00:07:26,880
 Vipassana means seeing, or sight perhaps, but seeing is

75
00:07:26,880 --> 00:07:28,000
 closer.

76
00:07:28,000 --> 00:07:38,370
 And vip means clearly, could mean into, more likely means

77
00:07:38,370 --> 00:07:44,000
 clearly, seeing clearly.

78
00:07:44,000 --> 00:07:51,010
 And why that's important is because it captures quite well

79
00:07:51,010 --> 00:07:56,000
 the essence of what we're trying to do.

80
00:07:56,000 --> 00:07:59,970
 Vipassana is a word that describes why we're doing what we

81
00:07:59,970 --> 00:08:01,000
're doing.

82
00:08:01,000 --> 00:08:04,550
 It's not the part of this that represents what we're

83
00:08:04,550 --> 00:08:06,000
 actually doing.

84
00:08:06,000 --> 00:08:11,040
 But it's even just the momentary goal that we try to

85
00:08:11,040 --> 00:08:17,950
 accomplish through the rest of this, through the actual

86
00:08:17,950 --> 00:08:20,000
 practice.

87
00:08:20,000 --> 00:08:22,000
 Seeing clearly.

88
00:08:22,000 --> 00:08:25,950
 Keep that in mind because what are some other goals you

89
00:08:25,950 --> 00:08:31,380
 might have from practicing meditation to feel calm, to find

90
00:08:31,380 --> 00:08:37,230
 peace, to stop this or that, or to change this or that, to

91
00:08:37,230 --> 00:08:43,000
 fix this or that, to become this or that.

92
00:08:43,000 --> 00:08:48,060
 And many of those goals are part of general Buddhist

93
00:08:48,060 --> 00:08:53,000
 description of what we're aiming for, right?

94
00:08:53,000 --> 00:08:57,000
 What is the goal of Buddhism? Well, peace, sure. Freedom

95
00:08:57,000 --> 00:09:01,000
 from suffering, absolutely.

96
00:09:01,000 --> 00:09:08,260
 But it's not central to, it doesn't centrally describe the

97
00:09:08,260 --> 00:09:13,000
 immediate outcome of the practice.

98
00:09:13,000 --> 00:09:16,520
 Sort of like the state of mind we're trying to achieve,

99
00:09:16,520 --> 00:09:20,160
 which then leads to so many positive things like peace,

100
00:09:20,160 --> 00:09:23,000
 happiness and freedom from suffering.

101
00:09:23,000 --> 00:09:28,800
 Even letting go. You talk about, okay, your practice is for

102
00:09:28,800 --> 00:09:32,000
 letting go, but how do you let go?

103
00:09:32,000 --> 00:09:36,000
 There's many theories on how to let go.

104
00:09:36,000 --> 00:09:39,990
 The most common one is just listening to people when they

105
00:09:39,990 --> 00:09:42,000
 tell you, just let it go.

106
00:09:42,000 --> 00:09:47,030
 Someone doesn't let it go. They seem to be saying, on

107
00:09:47,030 --> 00:09:52,410
 hearing this, gain the power to just suddenly let go of the

108
00:09:52,410 --> 00:09:55,000
 things that you cling to.

109
00:09:55,000 --> 00:10:01,000
 Or it's funny because it's not really how it works.

110
00:10:01,000 --> 00:10:11,520
 Another one is forcing yourself to let go. Working hard and

111
00:10:11,520 --> 00:10:12,000
 letting go.

112
00:10:12,000 --> 00:10:17,030
 Working hard isn't part of what we do. For sure you can see

113
00:10:17,030 --> 00:10:19,000
 it's hard work.

114
00:10:19,000 --> 00:10:26,510
 In some sense the hard work is in not working, is in not

115
00:10:26,510 --> 00:10:31,000
 pushing, not controlling.

116
00:10:31,000 --> 00:10:34,050
 When you force yourself to let go, it's not an act of

117
00:10:34,050 --> 00:10:39,000
 letting go. It doesn't work that way either.

118
00:10:39,000 --> 00:10:44,930
 So even letting go comes from seeing clearly. The premise

119
00:10:44,930 --> 00:10:51,990
 is that the way we see things now is what causes us to

120
00:10:51,990 --> 00:10:53,000
 cling.

121
00:10:53,000 --> 00:10:59,420
 What we don't see, our lack of clear vision, a lack of

122
00:10:59,420 --> 00:11:02,760
 clarity in our relationship with our experiences, is what

123
00:11:02,760 --> 00:11:05,000
 leads us to cling to them.

124
00:11:05,000 --> 00:11:15,000
 Try to fix them, try to control them, try to possess them,

125
00:11:15,000 --> 00:11:19,000
 to sustain them and so on.

126
00:11:19,000 --> 00:11:24,280
 Why we react to things, why we cause our self-suffering and

127
00:11:24,280 --> 00:11:27,000
 cause other people suffering.

128
00:11:27,000 --> 00:11:31,260
 We think it's a good idea. It's based on our vision and our

129
00:11:31,260 --> 00:11:35,540
 understanding of the situation, which is often no vision at

130
00:11:35,540 --> 00:11:36,000
 all.

131
00:11:36,000 --> 00:11:48,470
 It's often just a habitual feeling or conception of what's

132
00:11:48,470 --> 00:11:53,000
 the right response, what's a good response.

133
00:11:53,000 --> 00:11:58,150
 Getting angry is good when people do things that you don't

134
00:11:58,150 --> 00:11:59,000
 like.

135
00:11:59,000 --> 00:12:06,670
 Getting worried is a good response for bad thoughts or

136
00:12:06,670 --> 00:12:11,000
 concerns about the future.

137
00:12:11,000 --> 00:12:20,010
 Getting depressed is a good response to loss and failure

138
00:12:20,010 --> 00:12:22,000
 and so on.

139
00:12:22,000 --> 00:12:31,870
 Mourning and sadness is a good response to loss and death

140
00:12:31,870 --> 00:12:34,000
 and so on.

141
00:12:34,000 --> 00:12:40,440
 But craving something or striving to obtain things is a

142
00:12:40,440 --> 00:12:45,000
 good response to pleasure and so on.

143
00:12:45,000 --> 00:12:53,000
 And why this is, is because we fail to see three things.

144
00:12:53,000 --> 00:12:54,000
 And so this is what we pass in a, in summary,

145
00:12:54,000 --> 00:12:58,000
 so there's many things we could see clearly about.

146
00:12:58,000 --> 00:13:02,000
 The three things we need to see clearly about are one that

147
00:13:02,000 --> 00:13:06,000
 everything inside of ourselves and in the world around us

148
00:13:06,000 --> 00:13:08,000
 is impermanent.

149
00:13:08,000 --> 00:13:14,860
 Overcome this concept that some part of our experiences or

150
00:13:14,860 --> 00:13:20,000
 something exists that last forever, that is predictable,

151
00:13:20,000 --> 00:13:23,000
 that is sustainable.

152
00:13:23,000 --> 00:13:28,610
 You know, our pleasure, our pain. To see clearly, oh, it

153
00:13:28,610 --> 00:13:31,000
 doesn't last more than a moment.

154
00:13:31,000 --> 00:13:39,050
 The actual experiences arise and cease. So anything that we

155
00:13:39,050 --> 00:13:42,000
 might cling to or react to is just our own imagination.

156
00:13:42,000 --> 00:13:46,240
 That's good or bad or something, you know. And

157
00:13:46,240 --> 00:13:55,000
 unpredictable as well. So our reactions are unhelpful.

158
00:13:55,000 --> 00:14:01,290
 Wanting something to stay, wanting something to go and so

159
00:14:01,290 --> 00:14:11,000
 on. It's not, we can't predict. It's unstable.

160
00:14:11,000 --> 00:14:18,080
 The second thing is that inside of ourselves and in the

161
00:14:18,080 --> 00:14:24,000
 world around us everything is unsatisfying.

162
00:14:24,000 --> 00:14:28,930
 Things that we cling to cannot satisfy us. Things that we

163
00:14:28,930 --> 00:14:33,000
 don't like, we can't fix them.

164
00:14:33,000 --> 00:14:39,000
 Because they're unstable, they're unpredictable.

165
00:14:39,000 --> 00:14:46,480
 Suffering, this is often called, what it means is that you

166
00:14:46,480 --> 00:14:51,000
 suffer when you get caught up in it.

167
00:14:51,000 --> 00:14:56,010
 You suffer if you try to fix it, try to control it, try to

168
00:14:56,010 --> 00:14:59,000
 keep it, try to get rid of it.

169
00:14:59,000 --> 00:15:03,560
 It's called suffering because it's like a hot fire. A hot

170
00:15:03,560 --> 00:15:06,000
 fire is very painful.

171
00:15:06,000 --> 00:15:08,490
 But it's not painful if you're over here and it's over

172
00:15:08,490 --> 00:15:11,390
 there. It's painful when you jump in it, when you get

173
00:15:11,390 --> 00:15:12,000
 caught up in it.

174
00:15:12,000 --> 00:15:17,000
 So experience, we get caught up in experience.

175
00:15:17,000 --> 00:15:21,900
 And all it takes is getting involved in it, having some

176
00:15:21,900 --> 00:15:25,000
 kind of desire in regards to it.

177
00:15:25,000 --> 00:15:30,510
 Some kind of intention, I'll get caught up in this. That

178
00:15:30,510 --> 00:15:34,000
 causes stress and suffering.

179
00:15:34,000 --> 00:15:37,770
 So seeing that about everything, every part of our

180
00:15:37,770 --> 00:15:39,000
 experience.

181
00:15:39,000 --> 00:15:42,500
 It's not worth clinging to, it's not worth getting caught

182
00:15:42,500 --> 00:15:45,000
 up in, it's just the cause of stress.

183
00:15:45,000 --> 00:15:51,000
 Happiness can't come from the objects of experience.

184
00:15:51,000 --> 00:15:55,970
 And the third is that everything inside of ourselves and in

185
00:15:55,970 --> 00:15:59,000
 the world around us is non-self.

186
00:15:59,000 --> 00:16:04,000
 Non-self. We're taught everything.

187
00:16:04,000 --> 00:16:07,180
 Our ordinary way of looking at things is that there's

188
00:16:07,180 --> 00:16:09,000
 people and places and things.

189
00:16:09,000 --> 00:16:12,340
 And when you talk about a person, a person has a self,

190
00:16:12,340 --> 00:16:13,000
 right?

191
00:16:13,000 --> 00:16:18,830
 But there's no person in experience that the reality of

192
00:16:18,830 --> 00:16:24,330
 that person is experiences of seeing and hearing and

193
00:16:24,330 --> 00:16:26,000
 conceptualizing.

194
00:16:26,000 --> 00:16:32,000
 All of which are impermanent, are momentary.

195
00:16:32,000 --> 00:16:37,000
 We hold onto things as self-beings. It's me, it's mine.

196
00:16:37,000 --> 00:16:43,000
 We hold onto people. This is you and my concept of you.

197
00:16:43,000 --> 00:16:48,000
 Why when people change do we suffer? It's self.

198
00:16:48,000 --> 00:16:57,000
 You used to be this. Why can't you be more this?

199
00:16:57,000 --> 00:17:01,000
 Because things are impermanent we can't control them.

200
00:17:01,000 --> 00:17:03,000
 People we can't control.

201
00:17:03,000 --> 00:17:08,000
 The reality is there is no self.

202
00:17:08,000 --> 00:17:12,720
 There is no self involved in a person or a place or a thing

203
00:17:12,720 --> 00:17:16,000
, an experience.

204
00:17:16,000 --> 00:17:20,000
 All we have is momentary experiences.

205
00:17:20,000 --> 00:17:22,070
 It doesn't mean there's anything, when you say that it's

206
00:17:22,070 --> 00:17:24,000
 not like there's anything lacking.

207
00:17:24,000 --> 00:17:28,170
 The person is still there. But in that person it's not

208
00:17:28,170 --> 00:17:30,000
 quite how we see it.

209
00:17:30,000 --> 00:17:36,470
 A person is just a whole bunch of experiences that are

210
00:17:36,470 --> 00:17:38,000
 creating new experiences,

211
00:17:38,000 --> 00:17:43,000
 a whole bunch of phenomena that are creating new phenomena.

212
00:17:43,000 --> 00:17:48,220
 There's nothing, there's no entity in there, there's no

213
00:17:48,220 --> 00:17:51,000
 core, no soul there.

214
00:17:51,000 --> 00:17:55,430
 That belies the reality, what you see when you practice of

215
00:17:55,430 --> 00:17:59,000
 everything, changing even our own minds.

216
00:17:59,000 --> 00:18:03,000
 I don't really have a soul or a self in that sense.

217
00:18:03,000 --> 00:18:08,050
 Who I am is constantly changing. You'll see that about

218
00:18:08,050 --> 00:18:09,000
 yourself.

219
00:18:09,000 --> 00:18:13,000
 But you'll see that about everything, that there is no core

220
00:18:13,000 --> 00:18:21,000
 and no control over these things.

221
00:18:21,000 --> 00:18:23,320
 So that's vipassana. You can think in general, it's just

222
00:18:23,320 --> 00:18:25,000
 you're going to see more clearly

223
00:18:25,000 --> 00:18:31,000
 and will lead you to cling less, suffer less.

224
00:18:31,000 --> 00:18:34,000
 So that's the first word. The other words are all related.

225
00:18:34,000 --> 00:18:36,000
 The satipatana.

226
00:18:36,000 --> 00:18:40,000
 Sati is a word that we should be familiar with.

227
00:18:40,000 --> 00:18:42,000
 If we're not, we should make ourselves familiar.

228
00:18:42,000 --> 00:18:45,000
 Sati is the word that we translate as mindfulness.

229
00:18:45,000 --> 00:18:50,000
 Sati means to remember. That's the best way to translate it

230
00:18:50,000 --> 00:18:51,000
 and describe what it means.

231
00:18:51,000 --> 00:18:57,000
 It means the capacity, the ability to remember.

232
00:18:57,000 --> 00:19:03,000
 So we're cultivating this ability to remember.

233
00:19:03,000 --> 00:19:10,850
 Bhatana is just a word that means a locus or a means or a

234
00:19:10,850 --> 00:19:15,000
 base on which to develop sati.

235
00:19:15,000 --> 00:19:17,000
 And that's what these other four words are.

236
00:19:17,000 --> 00:19:23,050
 If you don't remember the first two words, just remember

237
00:19:23,050 --> 00:19:26,000
 the four later words.

238
00:19:26,000 --> 00:19:28,280
 You can remember them in English as well, but it's

239
00:19:28,280 --> 00:19:33,000
 important to stress that these four are

240
00:19:33,000 --> 00:19:36,960
 sort of the framework with which we're working in our

241
00:19:36,960 --> 00:19:38,000
 practice.

242
00:19:38,000 --> 00:19:41,670
 So sati means to remember. It means to remember not the

243
00:19:41,670 --> 00:19:44,000
 past or the future.

244
00:19:44,000 --> 00:19:47,130
 It means to remember the present and not just the present,

245
00:19:47,130 --> 00:19:52,000
 but to remember the experiential present.

246
00:19:52,000 --> 00:19:59,000
 So when you're in pain, to remember the pain, to remember

247
00:19:59,000 --> 00:20:06,000
 and to stay with and to be with the pain,

248
00:20:06,000 --> 00:20:09,000
 to remember that it's just pain.

249
00:20:09,000 --> 00:20:13,000
 When you see something, to remember that it's just seeing.

250
00:20:13,000 --> 00:20:14,000
 When you think something,

251
00:20:14,000 --> 00:20:17,000
 to remember that it's just thinking.

252
00:20:17,000 --> 00:20:20,000
 This is the language that the Buddha used.

253
00:20:20,000 --> 00:20:25,000
 Ditet di tama tambhavi sati. Let's seeing just be seeing.

254
00:20:25,000 --> 00:20:32,000
 Just be seeing.

255
00:20:32,000 --> 00:20:35,400
 We try to change the way we look at our experiences, and

256
00:20:35,400 --> 00:20:38,000
 this is how we cultivate vipassana.

257
00:20:38,000 --> 00:20:42,560
 Our ordinary way of relating to experiences is reacting to

258
00:20:42,560 --> 00:20:43,000
 them,

259
00:20:43,000 --> 00:20:50,680
 creating thoughts that are sort of pre-prejudiced to the

260
00:20:50,680 --> 00:20:52,000
 point.

261
00:20:52,000 --> 00:20:55,280
 So when we have pain, we're prejudiced against it, which

262
00:20:55,280 --> 00:20:56,000
 means we have a habit of reacting

263
00:20:56,000 --> 00:21:00,330
 in certain ways of seeing it in a certain way of a

264
00:21:00,330 --> 00:21:03,000
 narrative about the pain,

265
00:21:03,000 --> 00:21:07,390
 things we already know about the pain. We know that it's

266
00:21:07,390 --> 00:21:10,000
 bad. It's a problem.

267
00:21:10,000 --> 00:21:13,000
 It's a danger even.

268
00:21:13,000 --> 00:21:17,260
 Thoughts, we know that thoughts are bad or a danger or a

269
00:21:17,260 --> 00:21:18,000
 problem.

270
00:21:18,000 --> 00:21:22,940
 But we know these things and it's not knowledge, it's based

271
00:21:22,940 --> 00:21:26,000
 on our ignorance.

272
00:21:26,000 --> 00:21:29,240
 And we try to change that. We try to show ourselves that

273
00:21:29,240 --> 00:21:30,000
 pain is just pain,

274
00:21:30,000 --> 00:21:33,790
 thoughts are just thoughts and so on. We try to cultivate

275
00:21:33,790 --> 00:21:35,000
 this remembrance of things

276
00:21:35,000 --> 00:21:39,000
 just as they are. And so the way we do that, of course, is

277
00:21:39,000 --> 00:21:39,000
 saying to ourselves,

278
00:21:39,000 --> 00:21:44,600
 pain, pain. That's cultivating sati. When you say to

279
00:21:44,600 --> 00:21:46,000
 yourself, pain, pain,

280
00:21:46,000 --> 00:21:49,000
 you're bringing your mind, you're reminding yourself,

281
00:21:49,000 --> 00:21:52,000
 helping yourself remember

282
00:21:52,000 --> 00:21:57,300
 the experience as pain. It's just pain. It's not bad, it's

283
00:21:57,300 --> 00:21:59,000
 not good, it's not me,

284
00:21:59,000 --> 00:22:07,000
 it's not mine. It's not a problem.

285
00:22:07,000 --> 00:22:11,590
 And you start to see it's actually not worth getting upset

286
00:22:11,590 --> 00:22:12,000
 about.

287
00:22:12,000 --> 00:22:19,000
 There's no benefit that comes from that.

288
00:22:19,000 --> 00:22:24,250
 So that's sati. And we have the four sati pataṇa, the

289
00:22:24,250 --> 00:22:26,000
 four bases of developing sati,

290
00:22:26,000 --> 00:22:31,000
 which are kāya, vedana, chitadam. So kāya is the body.

291
00:22:31,000 --> 00:22:33,680
 We start by focusing on the body. This is why we use the

292
00:22:33,680 --> 00:22:38,000
 stomach as a basic object.

293
00:22:38,000 --> 00:22:42,250
 When the stomach rises, saying to yourself, rising, when

294
00:22:42,250 --> 00:22:44,000
 the stomach falls, falling,

295
00:22:44,000 --> 00:22:48,720
 there's nothing special about it. The body, in fact, it's

296
00:22:48,720 --> 00:22:53,000
 the best object to start with

297
00:22:53,000 --> 00:22:58,650
 and to take as a base because it's always there. It's much

298
00:22:58,650 --> 00:23:01,000
 less situational

299
00:23:01,000 --> 00:23:04,780
 than the rest of the three. You're always going to have the

300
00:23:04,780 --> 00:23:05,000
 body.

301
00:23:05,000 --> 00:23:08,270
 You can even just say, if you can't find the breath, you

302
00:23:08,270 --> 00:23:11,000
 can just say sitting, sitting.

303
00:23:11,000 --> 00:23:14,990
 And you'll find that that brings you back, reminds you,

304
00:23:14,990 --> 00:23:17,000
 helps you remember

305
00:23:17,000 --> 00:23:27,000
 the present moment. You can try that now. Close your eyes.

306
00:23:27,000 --> 00:23:29,470
 I say try it now because sitting here listening to me is

307
00:23:29,470 --> 00:23:30,000
 one thing,

308
00:23:30,000 --> 00:23:33,550
 but you're just listening intellectually, and what's going

309
00:23:33,550 --> 00:23:34,000
 on in your mind,

310
00:23:34,000 --> 00:23:39,850
 you're judging it, liking, disliking. So there's a

311
00:23:39,850 --> 00:23:41,000
 disconnect.

312
00:23:41,000 --> 00:23:51,980
 So to connect it, let's take listening to the Dhamma as a

313
00:23:51,980 --> 00:23:57,000
 meditation practice.

314
00:23:57,000 --> 00:23:59,220
 If you want, you can put your hand on your stomach in the

315
00:23:59,220 --> 00:24:02,000
 beginning if you've never done this.

316
00:24:02,000 --> 00:24:09,960
 Otherwise, just focus on the rising, falling, and say

317
00:24:09,960 --> 00:24:13,000
 rising, falling.

318
00:24:13,000 --> 00:24:15,000
 The other one, of course, is the walking meditation.

319
00:24:15,000 --> 00:24:24,000
 When you walk and say stepping right, stepping left,

320
00:24:24,000 --> 00:24:29,000
 it's important when you walk to separate the movements.

321
00:24:29,000 --> 00:24:34,350
 When the movement begins, you start saying step, bing, and

322
00:24:34,350 --> 00:24:35,000
 then right

323
00:24:35,000 --> 00:24:38,440
 when you put the foot down. And then you're not doing both

324
00:24:38,440 --> 00:24:42,000
 steps at once in a flow or anything.

325
00:24:42,000 --> 00:24:51,550
 We want to be with the experience, and that means one

326
00:24:51,550 --> 00:24:55,000
 experience at a time.

327
00:24:55,000 --> 00:25:01,000
 Body's a great way to learn how to see clearly.

328
00:25:01,000 --> 00:25:04,210
 It's a great object, but it's something we're probably not

329
00:25:04,210 --> 00:25:12,000
 immediately going to react to.

330
00:25:12,000 --> 00:25:25,310
 So it's relatively simple and easy to begin with and to

331
00:25:25,310 --> 00:25:34,000
 begin to learn how to be mindful.

332
00:25:34,000 --> 00:25:44,000
 The second is vedana, so vedana is translated as feelings.

333
00:25:44,000 --> 00:25:50,120
 But it's just referring to a specific category or type of

334
00:25:50,120 --> 00:25:52,000
 feeling.

335
00:25:52,000 --> 00:25:56,000
 Happy, unhappy, and neutral.

336
00:25:56,000 --> 00:26:03,410
 Everything we experience, we have one of these, mostly

337
00:26:03,410 --> 00:26:05,000
 neutral.

338
00:26:05,000 --> 00:26:08,870
 But sometimes there will be pain or displeasure. Sometimes

339
00:26:08,870 --> 00:26:11,000
 there will be pleasure.

340
00:26:11,000 --> 00:26:17,000
 Sometimes there's a specific neutral feeling of calm.

341
00:26:17,000 --> 00:26:19,000
 And these we should all note as well.

342
00:26:19,000 --> 00:26:24,660
 So as we're focusing on the body, we pay attention to other

343
00:26:24,660 --> 00:26:26,000
 experiences when they come up.

344
00:26:26,000 --> 00:26:28,940
 If you feel pain, forget about the body. Focus on the

345
00:26:28,940 --> 00:26:30,000
 feelings.

346
00:26:30,000 --> 00:26:36,000
 Pain, pain, pain. This helps you see the pain as pain,

347
00:26:36,000 --> 00:26:40,000
 helps you see it more clearly.

348
00:26:40,000 --> 00:26:44,030
 It helps you understand your reactions to the pain and to

349
00:26:44,030 --> 00:26:48,000
 change your reactions,

350
00:26:48,000 --> 00:26:51,310
 many of which are just based on ignorance and poor

351
00:26:51,310 --> 00:26:54,570
 understanding, poor appreciation of the reality of the

352
00:26:54,570 --> 00:26:56,000
 situation.

353
00:26:56,000 --> 00:27:02,360
 Pain isn't a problem. And understanding that is a very

354
00:27:02,360 --> 00:27:04,000
 powerful thing.

355
00:27:04,000 --> 00:27:09,000
 When you feel happy, saying to yourself, "Happy, happy."

356
00:27:09,000 --> 00:27:13,000
 When you feel calm, saying, "Calm, calm."

357
00:27:13,000 --> 00:27:19,250
 Just stay with it until it goes away. Once it's gone, come

358
00:27:19,250 --> 00:27:22,000
 back again to the body.

359
00:27:22,000 --> 00:27:27,000
 The third one is jitta. Jitta means the mind.

360
00:27:27,000 --> 00:27:33,000
 So there are many mind states.

361
00:27:33,000 --> 00:27:36,000
 But most importantly is the thinking.

362
00:27:36,000 --> 00:27:41,390
 So in this category we focus mostly on the fact that you're

363
00:27:41,390 --> 00:27:46,000
 thinking, the idea, the state of thinking.

364
00:27:46,000 --> 00:27:50,000
 You start thinking about your experiences.

365
00:27:50,000 --> 00:27:53,000
 You have pain and then you think about it.

366
00:27:53,000 --> 00:27:58,000
 You have a memory and then you think about it.

367
00:27:58,000 --> 00:28:04,000
 Even just sounds, you might start thinking about them.

368
00:28:04,000 --> 00:28:08,000
 You have some idea, start thinking about it.

369
00:28:08,000 --> 00:28:12,000
 I'm not trying to stop thinking.

370
00:28:12,000 --> 00:28:16,000
 That's a prejudice that people come to the practice with.

371
00:28:16,000 --> 00:28:19,550
 Thinking is part of the practice. Pain is part of the

372
00:28:19,550 --> 00:28:20,000
 practice.

373
00:28:20,000 --> 00:28:26,560
 Happiness is part of the practice. They're not good or bad

374
00:28:26,560 --> 00:28:29,000
 or extra.

375
00:28:29,000 --> 00:28:33,180
 So when you think, you're also taking it as an object. When

376
00:28:33,180 --> 00:28:35,460
 you remember and realize that you're thinking, say to

377
00:28:35,460 --> 00:28:39,000
 yourself, "Thinking, thinking."

378
00:28:39,000 --> 00:28:43,000
 It helps to remind you, not get rid of the thought exactly,

379
00:28:43,000 --> 00:28:46,000
 but to remind you it's just thinking.

380
00:28:46,000 --> 00:28:52,000
 Past, future, good, bad, doesn't matter.

381
00:28:52,000 --> 00:28:57,460
 Helps you see impermanence, helps you see the nature of

382
00:28:57,460 --> 00:29:01,000
 experience, nature of reality.

383
00:29:01,000 --> 00:29:05,990
 Thoughts aren't also a problem or a good or bad about them.

384
00:29:05,990 --> 00:29:15,650
 It's our reactions to them that create this idea of good or

385
00:29:15,650 --> 00:29:17,000
 bad.

386
00:29:17,000 --> 00:29:24,090
 And finally, Dhamma is the hardest of these six words to

387
00:29:24,090 --> 00:29:26,000
 translate.

388
00:29:26,000 --> 00:29:30,010
 It's easy to get a sense of it. It's not an esoteric word

389
00:29:30,010 --> 00:29:31,000
 exactly.

390
00:29:31,000 --> 00:29:37,270
 It's the teaching of the Buddha or the things the Buddha

391
00:29:37,270 --> 00:29:42,000
 learned, so kind of reality, truth.

392
00:29:42,000 --> 00:29:48,350
 But the point is, I think, that when we practice, when we

393
00:29:48,350 --> 00:29:58,000
 try to be mindful, we're engaging in a path.

394
00:29:58,000 --> 00:30:05,150
 We're engaging in a practice to progress and to learn and

395
00:30:05,150 --> 00:30:15,000
 to cultivate an understanding about certain things and to

396
00:30:15,000 --> 00:30:19,000
 do away with, reject and abandon certain things.

397
00:30:19,000 --> 00:30:23,830
 The first group is those that we're going to abandon. Under

398
00:30:23,830 --> 00:30:29,000
 Dhammas there are several groups.

399
00:30:29,000 --> 00:30:33,000
 The first one is the hindrances. These are the ones to

400
00:30:33,000 --> 00:30:34,000
 abandon.

401
00:30:34,000 --> 00:30:39,060
 So as we're practicing mindfulness, there's more that's

402
00:30:39,060 --> 00:30:40,000
 going to arise.

403
00:30:40,000 --> 00:30:45,690
 We're going to react to our experiences. And this causes

404
00:30:45,690 --> 00:30:49,000
 stress, this causes trouble.

405
00:30:49,000 --> 00:30:53,860
 It certainly disturbs our practice. It gets in the way of

406
00:30:53,860 --> 00:30:56,000
 us seeing clearly.

407
00:30:56,000 --> 00:31:00,000
 So these five, we often have people memorize these as well.

408
00:31:00,000 --> 00:31:01,000
 You can just remember them.

409
00:31:01,000 --> 00:31:09,660
 Simple words to refer to them. Liking, disliking, drows

410
00:31:09,660 --> 00:31:15,000
iness, distraction, and doubt.

411
00:31:15,000 --> 00:31:18,080
 And you can think of some of these as just headings, like

412
00:31:18,080 --> 00:31:20,000
 under disliking there's many.

413
00:31:20,000 --> 00:31:26,000
 But with liking there's liking and wanting.

414
00:31:26,000 --> 00:31:29,910
 And we call these hindrances, so they are things we want to

415
00:31:29,910 --> 00:31:31,000
 do away with.

416
00:31:31,000 --> 00:31:36,650
 But more practically they're things that as you become more

417
00:31:36,650 --> 00:31:40,000
 mindful you abandon naturally.

418
00:31:40,000 --> 00:31:44,000
 So we're not going to judge them either.

419
00:31:44,000 --> 00:31:47,000
 When you like something you say liking, liking.

420
00:31:47,000 --> 00:31:51,320
 It's important to have an attitude of not trying to get rid

421
00:31:51,320 --> 00:31:53,000
 of the experience.

422
00:31:53,000 --> 00:31:56,000
 So say to yourself, liking, liking.

423
00:31:56,000 --> 00:32:03,000
 If you dislike something say disliking, disliking.

424
00:32:03,000 --> 00:32:10,440
 Or frustrated, angry, worry, frustrated, angry, angry,

425
00:32:10,440 --> 00:32:12,000
 bored, sad, depressed.

426
00:32:12,000 --> 00:32:16,000
 Those all come under disliking.

427
00:32:16,000 --> 00:32:21,000
 But find a word you can say to yourself, bored, bored.

428
00:32:21,000 --> 00:32:23,000
 I think bored is a good one.

429
00:32:23,000 --> 00:32:28,310
 Sounds kind of funny to say it thinking, wouldn't I get

430
00:32:28,310 --> 00:32:32,000
 more bored if I did that?

431
00:32:32,000 --> 00:32:38,070
 So it's important when you use this technique, the mantra,

432
00:32:38,070 --> 00:32:39,000
 that you're objective about it.

433
00:32:39,000 --> 00:32:45,000
 You're not adding some venom to it.

434
00:32:45,000 --> 00:32:47,000
 Like angry, angry.

435
00:32:47,000 --> 00:32:49,770
 If you're saying it angrily it's probably not going to help

436
00:32:49,770 --> 00:32:50,000
.

437
00:32:50,000 --> 00:32:53,210
 But if you're sincerely trying to appreciate the fact that

438
00:32:53,210 --> 00:32:54,000
 you're angry,

439
00:32:54,000 --> 00:32:57,610
 you'll find the anger goes away quite quickly because that

440
00:32:57,610 --> 00:33:04,000
's a very different attitude from actually being angry.

441
00:33:04,000 --> 00:33:08,790
 And so it evokes different qualities, not angry qualities,

442
00:33:08,790 --> 00:33:19,000
 but mindful and wise and clear states of mind.

443
00:33:19,000 --> 00:33:24,550
 If you're tired or drowsy, say to yourself, tired, tired, d

444
00:33:24,550 --> 00:33:26,000
rowsy, drowsy.

445
00:33:26,000 --> 00:33:30,150
 If you're distracted or worried say distracted, distracted,

446
00:33:30,150 --> 00:33:34,000
 or worried, worried, restless, restless.

447
00:33:34,000 --> 00:33:37,000
 Distracted is a good one if you're thinking a lot.

448
00:33:37,000 --> 00:33:40,000
 You don't know how to say thinking, thinking all the time.

449
00:33:40,000 --> 00:33:45,000
 If it's a lot, say to yourself, distracted, distracted.

450
00:33:45,000 --> 00:33:49,090
 And doubt or confusion if you have doubt about yourself or

451
00:33:49,090 --> 00:33:53,000
 practice, doubt about me or anything.

452
00:33:53,000 --> 00:33:56,000
 Just say doubting, doubting.

453
00:33:56,000 --> 00:34:03,000
 If you're confused, say confused, confused.

454
00:34:03,000 --> 00:34:05,670
 With all these, stay with them until they go away and then

455
00:34:05,670 --> 00:34:10,000
 when they're gone just come back again to the body.

456
00:34:10,000 --> 00:34:18,000
 And that's the four satipatanas.

457
00:34:18,000 --> 00:34:21,470
 The other thing about the dhamma that I'll mention is the

458
00:34:21,470 --> 00:34:22,000
 senses.

459
00:34:22,000 --> 00:34:25,890
 But we go into those in detail as you progress in the

460
00:34:25,890 --> 00:34:27,000
 practice.

461
00:34:27,000 --> 00:34:31,460
 So just remember when you start, if you're just starting

462
00:34:31,460 --> 00:34:35,000
 out, an important one is seeing.

463
00:34:35,000 --> 00:34:38,030
 If you have your eyes closed and you see lights or colors,

464
00:34:38,030 --> 00:34:41,000
 pictures, just say to yourself, seeing, seeing.

465
00:34:41,000 --> 00:34:44,000
 Don't get detached to it.

466
00:34:44,000 --> 00:34:49,000
 Try not to get excited and think, oh, this is a good sign

467
00:34:49,000 --> 00:34:51,730
 or maybe this is a bad sign or get afraid of it or

468
00:34:51,730 --> 00:34:53,000
 something.

469
00:34:53,000 --> 00:34:56,000
 It's just seeing.

470
00:34:56,000 --> 00:35:01,690
 It happens in meditation that you start hallucinating kind

471
00:35:01,690 --> 00:35:03,000
 of in a way.

472
00:35:03,000 --> 00:35:08,440
 So for some people, so don't sit there waiting for it to

473
00:35:08,440 --> 00:35:09,000
 happen.

474
00:35:09,000 --> 00:35:12,000
 But if you do see, just say seeing, seeing and let it go.

475
00:35:12,000 --> 00:35:15,000
 It's just an experience.

476
00:35:15,000 --> 00:35:18,000
 Okay, so those are the basics of the practice.

477
00:35:18,000 --> 00:35:25,000
 Vipassana satipatana, kaya vidana jita dhamma.

478
00:35:25,000 --> 00:35:27,510
 If you guys have questions now, we can talk a little bit

479
00:35:27,510 --> 00:35:28,000
 more.

480
00:35:28,000 --> 00:35:32,000
 Just make sure everything's okay on the first day.

481
00:35:32,000 --> 00:35:34,000
 But that's the dhamma for tonight.

482
00:35:34,000 --> 00:35:37,540
 And thank you, Internet, for listening, watching and tuning

483
00:35:37,540 --> 00:35:39,000
 in and practicing as well.

484
00:35:39,000 --> 00:35:45,450
 I'll try to get back into answering questions as well from

485
00:35:45,450 --> 00:35:47,000
 the Internet.

486
00:35:47,000 --> 00:35:52,000
 Now that I'm back, have a good night.

487
00:35:53,000 --> 00:35:56,000
 Thank you.

488
00:35:58,000 --> 00:36:00,000
 Thank you.

489
00:36:01,000 --> 00:36:03,000
 Thank you.

