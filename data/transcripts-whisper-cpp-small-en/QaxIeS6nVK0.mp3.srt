1
00:00:00,000 --> 00:00:16,440
 Good evening everyone.

2
00:00:16,440 --> 00:00:24,640
 Welcome to another evening.

3
00:00:24,640 --> 00:00:26,640
 Another day.

4
00:00:26,640 --> 00:00:36,360
 I just keep coming, no?

5
00:00:36,360 --> 00:00:40,040
 Yesterday is gone.

6
00:00:40,040 --> 00:00:44,040
 Today comes.

7
00:00:44,040 --> 00:00:55,920
 I bet at the end of today there will be another day.

8
00:00:55,920 --> 00:01:00,800
 The term for the universe in Buddhism is samsara.

9
00:01:00,800 --> 00:01:05,240
 Sanghsara.

10
00:01:05,240 --> 00:01:12,720
 Sanghsara means to wander on.

11
00:01:12,720 --> 00:01:21,880
 Is there a problem with my sound?

12
00:01:21,880 --> 00:01:28,880
 To wander on.

13
00:01:28,880 --> 00:01:40,080
 Chaturnanga ryasajjanang yathabhutang adasana.

14
00:01:40,080 --> 00:01:57,190
 Through not seeing the four noble truths, samsaritang digam

15
00:01:57,190 --> 00:01:57,360
atanang dasutasvevajadis.

16
00:01:57,360 --> 00:02:03,600
 Through not seeing these four noble truths, samsaritang dig

17
00:02:03,600 --> 00:02:04,760
amatanang.

18
00:02:04,760 --> 00:02:12,750
 There is this wandering on for a long time, to put it

19
00:02:12,750 --> 00:02:14,520
 mildly.

20
00:02:14,520 --> 00:02:18,680
 Diga long.

21
00:02:18,680 --> 00:02:29,360
 This long wandering dasutasvevajadis from birth to birth.

22
00:02:29,360 --> 00:02:31,360
 Being born.

23
00:02:31,360 --> 00:02:33,840
 Dying here we're born there.

24
00:02:33,840 --> 00:02:42,080
 Dying from there we're born somewhere else.

25
00:02:42,080 --> 00:02:53,600
 This is the reality that we're faced with.

26
00:02:53,600 --> 00:03:07,160
 Unfortunately, privileged only to a very short memory.

27
00:03:07,160 --> 00:03:11,420
 And so we're unable to see truths like the four noble

28
00:03:11,420 --> 00:03:12,360
 truths.

29
00:03:12,360 --> 00:03:23,160
 We don't have this science, this knowledge.

30
00:03:23,160 --> 00:03:25,810
 And so it's not merely by chance that the Buddha's

31
00:03:25,810 --> 00:03:29,680
 enlightenment came after seeing the

32
00:03:29,680 --> 00:03:31,520
 rounds of rebirth.

33
00:03:31,520 --> 00:03:42,480
 Through understanding the repetitive and infinite

34
00:03:42,480 --> 00:03:48,000
 repetition of rebirth.

35
00:03:48,000 --> 00:03:56,470
 But he saw where our ambitions lead us, where our defile

36
00:03:56,470 --> 00:03:59,760
ments drag us, where our addictions

37
00:03:59,760 --> 00:04:08,680
 keep us tied to the wheel of samsara.

38
00:04:08,680 --> 00:04:10,520
 Like a dog chasing its tail.

39
00:04:10,520 --> 00:04:12,520
 That's what we are.

40
00:04:12,520 --> 00:04:15,000
 It's quite depressing really.

41
00:04:15,000 --> 00:04:18,920
 This, oh this existence.

42
00:04:18,920 --> 00:04:24,840
 It's not how we want to look at existence, right?

43
00:04:24,840 --> 00:04:27,360
 Life is sacred.

44
00:04:27,360 --> 00:04:29,120
 There is great joy to be had.

45
00:04:29,120 --> 00:04:32,840
 This is what we want to see.

46
00:04:32,840 --> 00:04:37,460
 That kind of teaching generally goes over quite a bit

47
00:04:37,460 --> 00:04:40,920
 better than the depressing teachings

48
00:04:40,920 --> 00:04:46,440
 of repetition.

49
00:04:46,440 --> 00:04:57,800
 But the interesting reality is our acceptance of suffering.

50
00:04:57,800 --> 00:05:02,970
 The four noble truths having all to do with suffering and

51
00:05:02,970 --> 00:05:05,520
 clinging and addiction.

52
00:05:05,520 --> 00:05:09,000
 Our acceptance and our understanding of suffering is

53
00:05:09,000 --> 00:05:10,880
 actually the only thing that we can do

54
00:05:10,880 --> 00:05:16,760
 that brings true happiness.

55
00:05:16,760 --> 00:05:20,120
 Through thoroughly understanding.

56
00:05:20,120 --> 00:05:24,640
 You see because we're not actually happy.

57
00:05:24,640 --> 00:05:29,520
 We can't actually find happiness.

58
00:05:29,520 --> 00:05:34,830
 You think that this round of rebirth is capable of

59
00:05:34,830 --> 00:05:38,480
 satisfying you that if you get what you

60
00:05:38,480 --> 00:05:43,380
 want, if you have what you want, if you set up your life in

61
00:05:43,380 --> 00:05:45,800
 such a way as to provide for

62
00:05:45,800 --> 00:05:51,200
 all of your desires you can somehow be satisfied.

63
00:05:51,200 --> 00:05:57,760
 We're not actually happy.

64
00:05:57,760 --> 00:06:01,580
 There's this philosophical question about whether it would

65
00:06:01,580 --> 00:06:03,520
 be better to be a pig satisfied

66
00:06:03,520 --> 00:06:09,520
 in its style or Socrates.

67
00:06:09,520 --> 00:06:16,300
 Who, you know, for philosophers is one of the wisest men in

68
00:06:16,300 --> 00:06:17,720
 history.

69
00:06:17,720 --> 00:06:23,750
 Meaning to be a wise person but suffer and be made to drink

70
00:06:23,750 --> 00:06:24,920
 poison.

71
00:06:24,920 --> 00:06:28,900
 Now the interesting thing of course about Socrates is that

72
00:06:28,900 --> 00:06:31,000
 he was at peace when he died.

73
00:06:31,000 --> 00:06:35,880
 He was in the books anyway.

74
00:06:35,880 --> 00:06:45,960
 But the real point is that you, wisdom itself, true wisdom,

75
00:06:45,960 --> 00:06:47,080
 which doesn't just mean knowledge

76
00:06:47,080 --> 00:06:52,000
 but it means the knowledge of which knowledge?

77
00:06:52,000 --> 00:07:00,750
 The knowledge to differentiate between knowledges which is

78
00:07:00,750 --> 00:07:05,120
 useful, which is useless.

79
00:07:05,120 --> 00:07:09,680
 True knowledge is the true cause of happiness.

80
00:07:09,680 --> 00:07:11,840
 That the pig can never be truly happy.

81
00:07:11,840 --> 00:07:16,460
 You might look at the pig and say, "Oh that pig looks happy

82
00:07:16,460 --> 00:07:16,920
."

83
00:07:16,920 --> 00:07:23,450
 And the same goes for humans. You look around you and the

84
00:07:23,450 --> 00:07:23,520
 human world appears to be full

85
00:07:23,520 --> 00:07:26,320
 of happiness, right?

86
00:07:26,320 --> 00:07:34,140
 Everywhere you turn, open up an advertisement full of

87
00:07:34,140 --> 00:07:35,720
 smiles.

88
00:07:35,720 --> 00:07:42,680
 Turn on the television, watch a commercial, smiles.

89
00:07:42,680 --> 00:07:46,550
 Most of our favorite television shows are they used to be

90
00:07:46,550 --> 00:07:48,360
 anyway full of smiles.

91
00:07:48,360 --> 00:07:54,430
 Problems that could be solved and villains that could be

92
00:07:54,430 --> 00:07:56,120
 vanquished, heroes that would

93
00:07:56,120 --> 00:08:05,440
 live happily ever after.

94
00:08:05,440 --> 00:08:10,560
 This is the story that we tell ourselves.

95
00:08:10,560 --> 00:08:12,560
 And if it were true then all would be fine.

96
00:08:12,560 --> 00:08:16,160
 It would be good for us to be positive about the world.

97
00:08:16,160 --> 00:08:20,320
 Unfortunately it's not true.

98
00:08:20,320 --> 00:08:33,560
 Even those people who will swear up and down that they're

99
00:08:33,560 --> 00:08:35,040
 happy.

100
00:08:35,040 --> 00:08:38,280
 Would be shocked if they ever tried to practice meditation

101
00:08:38,280 --> 00:08:40,040
 to realize how truly unhappy they

102
00:08:40,040 --> 00:08:42,600
 actually are.

103
00:08:42,600 --> 00:08:46,060
 How truly unsatisfied and unable to simply be with their

104
00:08:46,060 --> 00:08:47,920
 own mind, to taste their own

105
00:08:47,920 --> 00:08:52,440
 mind.

106
00:08:52,440 --> 00:08:53,800
 Meditation really is the true test, right?

107
00:08:53,800 --> 00:08:56,320
 If you're so happy, let's test.

108
00:08:56,320 --> 00:09:01,460
 We'll sit here together quietly and we'll measure our

109
00:09:01,460 --> 00:09:02,880
 happiness.

110
00:09:02,880 --> 00:09:09,040
 We'll measure our minds.

111
00:09:09,040 --> 00:09:16,450
 Put ourselves in the petri dish and we'll examine our state

112
00:09:16,450 --> 00:09:17,880
 of mind.

113
00:09:17,880 --> 00:09:22,060
 And we'll see that those people who claim to be so happy

114
00:09:22,060 --> 00:09:24,800
 are full of stress and dissatisfaction

115
00:09:24,800 --> 00:09:30,320
 and clinging and their happiness is very much dependent on

116
00:09:30,320 --> 00:09:32,720
 people, places and things.

117
00:09:32,720 --> 00:09:35,440
 Specific people, places and things.

118
00:09:35,440 --> 00:09:39,200
 And experiences.

119
00:09:39,200 --> 00:10:00,680
 Without those they can't be pleased, let alone happy.

120
00:10:00,680 --> 00:10:05,580
 The Buddhism doesn't make grandoise claims about religious

121
00:10:05,580 --> 00:10:07,480
 truths or anything.

122
00:10:07,480 --> 00:10:15,700
 It's not about proselytizing or ranting about the evil of

123
00:10:15,700 --> 00:10:19,400
 defilements and so on.

124
00:10:19,400 --> 00:10:21,560
 Buddhism doesn't create a whole new religion.

125
00:10:21,560 --> 00:10:22,680
 That's not really the idea.

126
00:10:22,680 --> 00:10:28,040
 The Buddha was, he called himself a vibhajavadi.

127
00:10:28,040 --> 00:10:33,000
 Someone who just looks at things and tells it like it is.

128
00:10:33,000 --> 00:10:38,100
 He's able to see through the, or see in detail, just like

129
00:10:38,100 --> 00:10:40,280
 physical scientists or material

130
00:10:40,280 --> 00:10:45,110
 scientists are able to split the atom and find subatomic

131
00:10:45,110 --> 00:10:47,120
 particles and so on.

132
00:10:47,120 --> 00:10:52,440
 The Buddha was able to do the same with the mind.

133
00:10:52,440 --> 00:10:58,920
 So the Buddha didn't create something.

134
00:10:58,920 --> 00:11:01,560
 Buddhism isn't like this.

135
00:11:01,560 --> 00:11:05,710
 The Buddhism I practice anyway doesn't have a heaven with a

136
00:11:05,710 --> 00:11:07,600
 Buddha God in it granting

137
00:11:07,600 --> 00:11:11,440
 all your wishes.

138
00:11:11,440 --> 00:11:15,900
 Buddhism is just describing reality and pointing out some

139
00:11:15,900 --> 00:11:19,080
 aspects of reality that are not terribly

140
00:11:19,080 --> 00:11:23,800
 obvious or clear to us.

141
00:11:23,800 --> 00:11:27,510
 It's such a wonderful thing to take someone through a

142
00:11:27,510 --> 00:11:30,020
 meditation course because you see

143
00:11:30,020 --> 00:11:33,160
 their eyes opening day by day.

144
00:11:33,160 --> 00:11:38,410
 You see them swallowing this bitter pill of truth and they

145
00:11:38,410 --> 00:11:41,080
're finally honest with themselves.

146
00:11:41,080 --> 00:11:45,630
 I mean isn't that the greatest thing when you're finally

147
00:11:45,630 --> 00:11:47,760
 honest with yourself?

148
00:11:47,760 --> 00:11:51,840
 When you stop fooling yourself and you stop pretending to

149
00:11:51,840 --> 00:11:54,240
 be something, when you finally

150
00:11:54,240 --> 00:12:07,200
 have the opportunity to just, to just better yourself.

151
00:12:07,200 --> 00:12:11,470
 And you see all your ego and all your tricks and all your

152
00:12:11,470 --> 00:12:13,480
 mental constructs that you've

153
00:12:13,480 --> 00:12:19,960
 created over your life.

154
00:12:19,960 --> 00:12:23,640
 And through the practice you free yourself.

155
00:12:23,640 --> 00:12:29,120
 It's quite simple.

156
00:12:29,120 --> 00:12:35,680
 So tonight's topic then is samsara.

157
00:12:35,680 --> 00:12:42,830
 Seeing through the delusion of samsara, the narrative that

158
00:12:42,830 --> 00:12:46,560
 we story that we tell ourselves

159
00:12:46,560 --> 00:12:56,910
 about how great the world is and how we can find all of,

160
00:12:56,910 --> 00:12:58,200
 find the objects of all of our

161
00:12:58,200 --> 00:13:00,760
 desires.

162
00:13:00,760 --> 00:13:09,580
 If only we try harder, if only we cling more, cling tighter

163
00:13:09,580 --> 00:13:13,360
 to the things we love, strive

164
00:13:13,360 --> 00:13:28,360
 after them.

165
00:13:28,360 --> 00:13:30,120
 Meditation isn't about going somewhere.

166
00:13:30,120 --> 00:13:32,040
 It's the opposite of this wandering on.

167
00:13:32,040 --> 00:13:37,000
 It's the opposite of samsara stopping.

168
00:13:37,000 --> 00:13:40,340
 You know the story of Anguly Mala or this man who, if you

169
00:13:40,340 --> 00:13:42,160
 ever watched the Thai movie

170
00:13:42,160 --> 00:13:44,800
 it's a really good rendition.

171
00:13:44,800 --> 00:13:49,600
 The subtitles are terrible but at one point I was trying to

172
00:13:49,600 --> 00:13:52,040
 redo the subtitles but never

173
00:13:52,040 --> 00:13:54,560
 got finished.

174
00:13:54,560 --> 00:14:01,620
 But in the Thai version, which is an adaptation, he thinks

175
00:14:01,620 --> 00:14:05,320
 if he kills all these people he'll

176
00:14:05,320 --> 00:14:10,660
 become the supreme being and become one with God kind of

177
00:14:10,660 --> 00:14:11,640
 thing.

178
00:14:11,640 --> 00:14:14,300
 He hears this voice and thinks that Brahma is telling him

179
00:14:14,300 --> 00:14:15,680
 to kill all these people.

180
00:14:15,680 --> 00:14:19,600
 So he kills them thinking he's liberating them.

181
00:14:19,600 --> 00:14:20,600
 And it evolves.

182
00:14:20,600 --> 00:14:22,770
 In the beginning he's killing evil people and then in the

183
00:14:22,770 --> 00:14:23,960
 end he realizes everyone has

184
00:14:23,960 --> 00:14:28,960
 evil in them and he just starts killing everyone.

185
00:14:28,960 --> 00:14:35,850
 Until finally he meets the Buddha and the Buddha contrives

186
00:14:35,850 --> 00:14:38,880
 through magical power to

187
00:14:38,880 --> 00:14:42,910
 not be caught and Anguly Mala chasing after him, running

188
00:14:42,910 --> 00:14:44,920
 through the forest, yells out

189
00:14:44,920 --> 00:14:46,920
 stop stop.

190
00:14:46,920 --> 00:14:58,480
 And the Buddha says, "Rau yut laiau Anguly Mala"

191
00:14:58,480 --> 00:14:59,480
 We've already stopped.

192
00:14:59,480 --> 00:15:03,480
 I've already stopped Anguly Mala.

193
00:15:03,480 --> 00:15:13,560
 "Tur laiau tikun yut tur"

194
00:15:13,560 --> 00:15:18,920
 It's you who should stop.

195
00:15:18,920 --> 00:15:23,800
 The meditation is about stopping.

196
00:15:23,800 --> 00:15:27,560
 When you create karma again and again, hopefully not by

197
00:15:27,560 --> 00:15:30,320
 killing human beings, but all the karma

198
00:15:30,320 --> 00:15:35,610
 that we create, this is the wandering on, this is the

199
00:15:35,610 --> 00:15:38,440
 proliferation becoming.

200
00:15:38,440 --> 00:15:46,060
 Every ambition you embark upon adds to your journey, adds

201
00:15:46,060 --> 00:15:51,000
 length to your journey, lengthens,

202
00:15:51,000 --> 00:15:59,720
 you're wandering on, perpetuates the wandering the cycle.

203
00:15:59,720 --> 00:16:02,020
 And so we're not doing anything special in meditation, we

204
00:16:02,020 --> 00:16:02,960
're just stopping.

205
00:16:02,960 --> 00:16:08,630
 You know, let's stop and at least take stock of where we're

206
00:16:08,630 --> 00:16:09,760
 headed.

207
00:16:09,760 --> 00:16:14,490
 Try and learn a little bit about the reasons why we do

208
00:16:14,490 --> 00:16:17,960
 things, the motivations behind our

209
00:16:17,960 --> 00:16:25,640
 ambitions and better ourselves and purify our intentions

210
00:16:25,640 --> 00:16:27,200
 and so on.

211
00:16:27,200 --> 00:16:32,480
 Buddhism is an incredibly noble, the noble path.

212
00:16:32,480 --> 00:16:37,640
 The nobility of this, the grace and the perfection.

213
00:16:37,640 --> 00:16:41,280
 This is why people make these statues.

214
00:16:41,280 --> 00:16:46,100
 How many human beings can say they have statues, this grand

215
00:16:46,100 --> 00:16:47,920
oise made for them?

216
00:16:47,920 --> 00:16:52,120
 The only other person I can think of, let me see.

217
00:16:52,120 --> 00:16:55,400
 I mean, Jesus is the other one, right?

218
00:16:55,400 --> 00:17:00,320
 Even Jesus doesn't have such a, and this is the outspring,

219
00:17:00,320 --> 00:17:02,760
 the outpouring of such faith

220
00:17:02,760 --> 00:17:06,880
 that people get by hearing and learning and appreciating.

221
00:17:06,880 --> 00:17:10,530
 Like if you've ever seen in Buddhist societies, when people

222
00:17:10,530 --> 00:17:13,960
 come to meditation, they just,

223
00:17:13,960 --> 00:17:18,360
 whether they be rich or poor, how it changes their life and

224
00:17:18,360 --> 00:17:20,680
 how much faith and confidence

225
00:17:20,680 --> 00:17:25,290
 they get, even here in the West, you know, how many people

226
00:17:25,290 --> 00:17:27,240
 come to the meditation and

227
00:17:27,240 --> 00:17:36,160
 just are in awe of the greatness and the purity of this

228
00:17:36,160 --> 00:17:37,640
 path.

229
00:17:37,640 --> 00:17:42,810
 So on that note, I'd like to express my appreciation first

230
00:17:42,810 --> 00:17:45,800
 and foremost for those meditators who

231
00:17:45,800 --> 00:17:51,660
 make it all the way here and undertake intensive meditation

232
00:17:51,660 --> 00:17:53,000
 practice.

233
00:17:53,000 --> 00:17:58,250
 It's you who are truly coming to a stop, making a stand,

234
00:17:58,250 --> 00:18:01,360
 establishing yourself well in the

235
00:18:01,360 --> 00:18:04,400
 present moment.

236
00:18:04,400 --> 00:18:07,320
 Tonight we have another meditator on his way.

237
00:18:07,320 --> 00:18:11,600
 He called me from the Toronto airport, coming from America.

238
00:18:11,600 --> 00:18:17,360
 He's kind of, kind of, well, he's been trying to, he's

239
00:18:17,360 --> 00:18:20,800
 never left his country before and

240
00:18:20,800 --> 00:18:27,910
 he's rather young, I think, but incredibly intent on

241
00:18:27,910 --> 00:18:30,120
 getting here.

242
00:18:30,120 --> 00:18:38,560
 He's in Canada somewhere on his way.

243
00:18:38,560 --> 00:18:41,510
 And then to all of you who come out to listen to the Dhamma

244
00:18:41,510 --> 00:18:43,200
, listening to the Dhamma is

245
00:18:43,200 --> 00:18:48,160
 something unique, uniquely special in the world.

246
00:18:48,160 --> 00:18:54,090
 This opportunity to learn not just another philosophy or

247
00:18:54,090 --> 00:18:57,960
 another one more way of understanding

248
00:18:57,960 --> 00:19:07,410
 the world, but to learn the way to understand the world,

249
00:19:07,410 --> 00:19:12,520
 the way to see through our delusions

250
00:19:12,520 --> 00:19:18,320
 to free ourselves from all suffering.

251
00:19:18,320 --> 00:19:26,680
 So much appreciation to you all.

252
00:19:26,680 --> 00:19:28,120
 And that's all I have to say.

253
00:19:28,120 --> 00:19:29,120
 Thank you for coming out.

254
00:19:29,120 --> 00:19:34,280
 If you have any questions, I'm happy to answer.

255
00:19:34,280 --> 00:19:35,280
 And the bell rings.

256
00:19:35,280 --> 00:19:37,200
 That means we've got a visitor.

257
00:19:37,200 --> 00:19:42,120
 Each one of you guys let him in and I'll take over.

258
00:19:42,120 --> 00:19:44,720
 Perfect timing.

259
00:19:44,720 --> 00:19:49,920
 This is probably the young guy.

260
00:19:49,920 --> 00:19:50,920
 It's okay.

261
00:19:50,920 --> 00:19:53,920
 Let the man that you can, you guys can go ahead.

262
00:19:53,920 --> 00:19:57,400
 Hi, go ahead.

263
00:19:57,400 --> 00:19:59,720
 Alah, you can go ahead.

264
00:19:59,720 --> 00:20:07,040
 You don't have to.

265
00:20:07,040 --> 00:20:10,040
 He made it.

266
00:20:10,040 --> 00:20:14,120
 He's here.

267
00:20:14,120 --> 00:20:15,640
 Javan.

268
00:20:15,640 --> 00:20:18,160
 How do you spell it?

269
00:20:18,160 --> 00:20:20,160
 How do you pronounce your name?

270
00:20:20,160 --> 00:20:21,160
 Javan.

271
00:20:21,160 --> 00:20:22,160
 Javan.

272
00:20:22,160 --> 00:20:24,160
 Come on in.

273
00:20:24,160 --> 00:20:25,160
 Have a seat for a second.

274
00:20:25,160 --> 00:20:27,160
 I'm just answering questions and welcome.

275
00:20:27,160 --> 00:20:34,320
 Glad you made it.

276
00:20:34,320 --> 00:20:35,320
 Any questions?

277
00:20:35,320 --> 00:20:43,120
 If there's no questions, I'm going to introduce our new med

278
00:20:43,120 --> 00:20:46,360
itator to the center.

279
00:20:46,360 --> 00:20:48,820
 Didn't Angali Malakil because he wanted to offer a garland

280
00:20:48,820 --> 00:20:50,120
 of fingers to his teacher?

281
00:20:50,120 --> 00:20:51,280
 No, no, that's not it.

282
00:20:51,280 --> 00:20:56,730
 No, the original text is, I think with the commentaries, is

283
00:20:56,730 --> 00:21:02,520
 that his teacher, he was

284
00:21:02,520 --> 00:21:03,520
 his favorite.

285
00:21:03,520 --> 00:21:06,200
 He was the teacher's pet.

286
00:21:06,200 --> 00:21:10,620
 The other students were jealous and they contrived to turn

287
00:21:10,620 --> 00:21:13,360
 the teacher against Angali Mala.

288
00:21:13,360 --> 00:21:17,530
 They convinced the teacher that Angali Mala was having an

289
00:21:17,530 --> 00:21:20,760
 affair with the teacher's wife.

290
00:21:20,760 --> 00:21:27,880
 This is how the Thai version sticks to this more or less.

291
00:21:27,880 --> 00:21:30,420
 He gets caught in a sort of a, it's a little bit more

292
00:21:30,420 --> 00:21:31,360
 complicated.

293
00:21:31,360 --> 00:21:34,880
 It's actually, it's really well done, the Thai version,

294
00:21:34,880 --> 00:21:36,760
 except for all the killing.

295
00:21:36,760 --> 00:21:39,980
 It's just full of, most of the movie is just about blood

296
00:21:39,980 --> 00:21:41,860
 and gore, but the actual story

297
00:21:41,860 --> 00:21:44,200
 behind it is really well done.

298
00:21:44,200 --> 00:21:48,470
 But sticking to the original story, so they convinced this

299
00:21:48,470 --> 00:21:49,040
 guy.

300
00:21:49,040 --> 00:21:52,730
 The teacher gets convinced and wants to, as a result,

301
00:21:52,730 --> 00:21:54,400
 destroy Angali Mala.

302
00:21:54,400 --> 00:21:57,580
 He says to Angali Mala that in order to graduate, he has to

303
00:21:57,580 --> 00:21:59,840
, something like in order to graduate,

304
00:21:59,840 --> 00:22:03,210
 he has to bring him a thousand, or he has to kill a

305
00:22:03,210 --> 00:22:04,720
 thousand people.

306
00:22:04,720 --> 00:22:08,720
 Angali, the finger thing, is just in order to count them.

307
00:22:08,720 --> 00:22:12,280
 How's he going to count a hundred?

308
00:22:12,280 --> 00:22:17,420
 Well he cuts the thumb maybe off of each, off one hand of

309
00:22:17,420 --> 00:22:19,560
 each person he kills, and

310
00:22:19,560 --> 00:22:23,000
 he makes a necklace so he can count.

311
00:22:23,000 --> 00:22:25,840
 That's how he can count to a thousand.

312
00:22:25,840 --> 00:22:28,400
 So he becomes known as Angali Mala.

313
00:22:28,400 --> 00:22:31,640
 Now the actual texts are quite sparse.

314
00:22:31,640 --> 00:22:34,500
 They don't have a lot of, they don't have any of this

315
00:22:34,500 --> 00:22:35,480
 commentary.

316
00:22:35,480 --> 00:22:38,670
 It's just, there's a story of a bandit called Angali Mala

317
00:22:38,670 --> 00:22:42,240
 who cuts people's fingers off.

318
00:22:42,240 --> 00:22:45,840
 But, commentaries have all sorts of stories about him.

319
00:22:45,840 --> 00:22:49,910
 And then there's a sutta giving a story after when Angali

320
00:22:49,910 --> 00:22:51,680
 Mala is already a monk.

321
00:22:51,680 --> 00:22:56,240
 I guess that's the Angali Mala sutta, right?

322
00:22:56,240 --> 00:22:59,560
 No, it's, yeah.

323
00:22:59,560 --> 00:23:07,280
 So the garland of fingers wasn't the gift to his teacher.

324
00:23:07,280 --> 00:23:23,840
 It was just a count.

325
00:23:23,840 --> 00:23:25,820
 Now the whole killing people because he wanted to send them

326
00:23:25,820 --> 00:23:27,920
 to heaven, that was the Thai version.

327
00:23:27,920 --> 00:23:28,920
 Really well done.

328
00:23:28,920 --> 00:23:32,180
 It's really, I mean it's not canonical, but it's a really

329
00:23:32,180 --> 00:23:33,120
 good story.

330
00:23:33,120 --> 00:23:38,590
 I don't know if you've ever seen the short video I did with

331
00:23:38,590 --> 00:23:40,120
 clips of it.

332
00:23:40,120 --> 00:23:46,230
 There's a really good section that I put together and put

333
00:23:46,230 --> 00:23:49,400
 on YouTube a long time ago.

334
00:23:49,400 --> 00:23:53,370
 Take a look at it and get a sort of a taste of what the

335
00:23:53,370 --> 00:23:54,840
 movie's like.

336
00:23:54,840 --> 00:23:59,830
 There was a lot of controversy surrounding the film because

337
00:23:59,830 --> 00:24:02,200
 it wasn't, it wasn't quite

338
00:24:02,200 --> 00:24:05,990
 canonical and there was a bit of a romance between Angali

339
00:24:05,990 --> 00:24:07,440
 Mala and the wife.

340
00:24:07,440 --> 00:24:12,930
 She ends up following him into the jungle and they escape

341
00:24:12,930 --> 00:24:14,280
 together.

342
00:24:14,280 --> 00:24:18,010
 But they never, it's very careful not to actually have them

343
00:24:18,010 --> 00:24:20,160
 be in a romantic relationship.

344
00:24:20,160 --> 00:24:34,800
 And he eventually becomes a monk of course.

345
00:24:34,800 --> 00:24:37,610
 Okay I'll let you all watch that video and I'm going to

346
00:24:37,610 --> 00:24:43,120
 sneak out the back door and go

347
00:24:43,120 --> 00:24:44,640
 meet my new meditator.

348
00:24:44,640 --> 00:24:47,520
 So have a good night everyone.

349
00:24:47,520 --> 00:24:48,520
 Bye.

350
00:24:48,520 --> 00:24:49,520
 Bye.

