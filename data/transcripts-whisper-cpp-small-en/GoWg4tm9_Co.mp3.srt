1
00:00:00,000 --> 00:00:07,520
 Okay, so starting with our announcements. The first

2
00:00:07,520 --> 00:00:09,400
 announcement, as I said, is we

3
00:00:09,400 --> 00:00:13,170
 have... I've started posting chapters from the book as I

4
00:00:13,170 --> 00:00:15,680
 edit them, so you all get

5
00:00:15,680 --> 00:00:20,520
 to see how slowly I'm editing them. And they're not really

6
00:00:20,520 --> 00:00:20,680
 well

7
00:00:20,680 --> 00:00:23,210
 edited, but I figured they're good enough to put up on the

8
00:00:23,210 --> 00:00:25,240
 web blog, so there's a

9
00:00:25,240 --> 00:00:31,500
 page now at my web blog that has just a couple of chapters

10
00:00:31,500 --> 00:00:32,240
 from the book. It's

11
00:00:32,240 --> 00:00:37,840
 the page under the text teachings called Lessons in Pract

12
00:00:37,840 --> 00:00:40,800
ical Buddhism. And so if

13
00:00:40,800 --> 00:00:44,360
 you go there, you'll see there's a couple of talks that I

14
00:00:44,360 --> 00:00:45,560
 just actually picked at

15
00:00:45,560 --> 00:00:49,210
 random. I was just clicking and started editing, so it's

16
00:00:49,210 --> 00:00:51,160
 not like those ones are

17
00:00:51,160 --> 00:00:54,440
 particularly special. I'm just gonna go through them at

18
00:00:54,440 --> 00:00:56,320
 random and edit them.

19
00:00:56,320 --> 00:01:01,600
 So you can go check that out and give your feedback. And

20
00:01:01,600 --> 00:01:02,160
 you're still people...

21
00:01:02,160 --> 00:01:08,860
 if you're still interested in transcribing one, the more

22
00:01:08,860 --> 00:01:09,280
 the merrier

23
00:01:09,280 --> 00:01:14,800
 we'll see how many we get around to, and whether I actually

24
00:01:14,800 --> 00:01:15,720
 end up

25
00:01:15,720 --> 00:01:21,150
 putting them all in the book. Second announcement, that's

26
00:01:21,150 --> 00:01:21,400
 Palanya Niyaki, you

27
00:01:21,400 --> 00:01:28,850
 can give the announcement. Our Meditator has finished her

28
00:01:28,850 --> 00:01:31,720
 course today, and

29
00:01:31,720 --> 00:01:37,390
 hopefully in near future she will be our next monastic

30
00:01:37,390 --> 00:01:39,640
 resident here in our

31
00:01:39,640 --> 00:01:46,980
 monastery. I was just saying that the future is unsure. We

32
00:01:46,980 --> 00:01:47,080
 can't

33
00:01:47,080 --> 00:01:50,420
 say that she's going to be a monk or so on, but it looks

34
00:01:50,420 --> 00:01:52,000
 quite promising. She

35
00:01:52,000 --> 00:01:54,980
 came off of her course, and one of the first things she

36
00:01:54,980 --> 00:01:57,000
 asked was, "When is

37
00:01:57,000 --> 00:02:01,400
 the soonest I'll be able to our day?" It wasn't actually

38
00:02:01,400 --> 00:02:03,040
 that she was like

39
00:02:03,040 --> 00:02:06,840
 champing at the bit or something, it was just for practical

40
00:02:06,840 --> 00:02:07,160
 reasons,

41
00:02:07,160 --> 00:02:10,530
 because she has to tell her family and so on, but just the

42
00:02:10,530 --> 00:02:11,560
 sort of matter-of-fact

43
00:02:11,560 --> 00:02:15,530
 way she said it was kind of encouraging. There wasn't this

44
00:02:15,530 --> 00:02:16,760
 "should I your day and

45
00:02:16,760 --> 00:02:23,040
 shouldn't I your day?" It was like it seemed to be quite a

46
00:02:23,040 --> 00:02:25,000
 said-and-done fact.

47
00:02:25,000 --> 00:02:32,600
 So she will be becoming a novice probably at the end of the

48
00:02:32,600 --> 00:02:34,000
 month,

49
00:02:34,000 --> 00:02:39,650
 but it's the 18th day or something, so maybe in 12 days, 10

50
00:02:39,650 --> 00:02:41,120
, 11 days she'll be

51
00:02:41,120 --> 00:02:51,180
 able to ordain. Then you'll get pictures of our newest mon

52
00:02:51,180 --> 00:02:51,640
astic

53
00:02:51,640 --> 00:02:56,050
 resident, and then as far as ordaining as a monk, we'll

54
00:02:56,050 --> 00:02:56,600
 have to see how

55
00:02:56,600 --> 00:02:59,490
 that's going to go, because of course we need a community

56
00:02:59,490 --> 00:03:01,400
 to ordain them, so that

57
00:03:01,400 --> 00:03:07,270
 might take some time to arrange. Okay, so that's another

58
00:03:07,270 --> 00:03:08,360
 announcement. Any other

59
00:03:08,360 --> 00:03:10,920
 announcements?

60
00:03:10,920 --> 00:03:19,840
 That's not an announcement. Okay, that's enough.

