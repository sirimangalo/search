1
00:00:00,000 --> 00:00:26,380
 [

2
00:00:26,380 --> 00:00:48,080
 What's the news today? Any interesting news?]

3
00:00:48,080 --> 00:00:51,080
 I didn't watch too much news today. I was out most of the

4
00:00:51,080 --> 00:00:55,120
 day getting things done. All

5
00:00:55,120 --> 00:01:11,560
 the coverage here is still on the shootings in California.

6
00:01:11,560 --> 00:01:16,980
 Getting a lot of calls. I've gotten calls from people today

7
00:01:16,980 --> 00:01:19,240
 and yesterday. Hey, I found

8
00:01:19,240 --> 00:01:26,980
 you on Meetup. Meetup is really actually a big thing. Oh,

9
00:01:26,980 --> 00:01:31,680
 tomorrow I think I'd better

10
00:01:31,680 --> 00:01:37,400
 skip the Visuddhi Maga class. I better just cancel it, I

11
00:01:37,400 --> 00:01:41,440
 guess, because there's an interfaith

12
00:01:41,440 --> 00:01:49,770
 peace meeting. Meeting of the Interfaith Peace Group in

13
00:01:49,770 --> 00:01:53,000
 Hamilton. One of the leaders is an

14
00:01:53,000 --> 00:01:57,810
 ex-McMaster professor. Her husband was one of my professors

15
00:01:57,810 --> 00:01:59,760
 16 years ago. I've just

16
00:01:59,760 --> 00:02:04,170
 reconnected with him. I'm on their email list. There was

17
00:02:04,170 --> 00:02:06,200
 one a few weeks ago and I didn't

18
00:02:06,200 --> 00:02:14,220
 go. But tomorrow I think someone's coming maybe to pick me

19
00:02:14,220 --> 00:02:17,640
 up here on their way. I should

20
00:02:17,640 --> 00:02:21,920
 probably go. Sure, I'll send out a message to cancel the

21
00:02:21,920 --> 00:02:24,480
 class for tomorrow. But I think

22
00:02:24,480 --> 00:02:29,960
 I'll be able to make it for the two meetings. The thing is

23
00:02:29,960 --> 00:02:33,880
 at 2.30. I'll make it for the

24
00:02:33,880 --> 00:02:37,230
 two meetings. Maybe I have to leave the second one a bit

25
00:02:37,230 --> 00:02:39,760
 early. I don't know. That sounds

26
00:02:39,760 --> 00:02:50,160
 good. Next week I have exams. One exam. And then I have to

27
00:02:50,160 --> 00:02:51,360
 start studying for the other

28
00:02:51,360 --> 00:03:01,410
 two. So I have to read Descartes, Locke and Spinoza. Anyway

29
00:03:01,410 --> 00:03:06,240
, so that is good to read about.

30
00:03:06,240 --> 00:03:11,470
 Sort of. I mean, some of the things these guys believe are

31
00:03:11,470 --> 00:03:13,840
 kind of weird. But it's good

32
00:03:13,840 --> 00:03:20,180
 to know. It gives you language, learning the way Western

33
00:03:20,180 --> 00:03:23,680
 philosophers thought and spoke

34
00:03:23,680 --> 00:03:27,390
 and the things they discussed. It allows us to explain

35
00:03:27,390 --> 00:03:29,680
 Buddhism. And many of them were

36
00:03:29,680 --> 00:03:33,020
 influenced. Not many of them. Some of them were influenced

37
00:03:33,020 --> 00:03:34,680
 by Buddhism. So some of them

38
00:03:34,680 --> 00:03:37,830
 have actually interesting ways of describing and explaining

39
00:03:37,830 --> 00:03:39,320
 things that we also describe

40
00:03:39,320 --> 00:03:46,210
 and explain. But in a Western context. So there's benefit

41
00:03:46,210 --> 00:03:48,720
 there. Who's the most modern philosopher

42
00:03:48,720 --> 00:03:51,640
 that you're studying? Well, it's early modern philosophy.

43
00:03:51,640 --> 00:03:54,920
 So these three are the cusp of

44
00:03:54,920 --> 00:03:59,760
 early modern thought when people started to question the

45
00:03:59,760 --> 00:04:02,160
 church and started to try to

46
00:04:02,160 --> 00:04:12,040
 find ways of knowing that were outside of the Bible says.

47
00:04:12,040 --> 00:04:12,920
 So they mostly believed in

48
00:04:12,920 --> 00:04:18,560
 God still, but they started to question many things.

49
00:04:18,560 --> 00:04:21,840
 Started to figure out how to know

50
00:04:21,840 --> 00:04:32,010
 how to learn. Probably wouldn't take the class again if I

51
00:04:32,010 --> 00:04:35,960
 had to do it over. Is there a part

52
00:04:35,960 --> 00:04:40,640
 two with more modern? There's more philosophy courses I can

53
00:04:40,640 --> 00:04:43,480
 take kind of turned off of philosophy

54
00:04:43,480 --> 00:04:51,980
 courses, though. Too much too speculative really for me.

55
00:04:51,980 --> 00:04:56,240
 Because Buddhist one of there's a

56
00:04:56,240 --> 00:05:00,000
 woman of Chinese ethnicity, I guess you could say she's

57
00:05:00,000 --> 00:05:02,520
 Canadian, but her parents, I think

58
00:05:02,520 --> 00:05:06,520
 are from China in my class. And I asked her what she

59
00:05:06,520 --> 00:05:09,400
 thought of the class. And she I think

60
00:05:09,400 --> 00:05:13,820
 she said she was she kind of give a disgusted noise. And we

61
00:05:13,820 --> 00:05:16,160
 were talking about I asked her

62
00:05:16,160 --> 00:05:20,540
 she's in Eastern philosophy, Chinese philosophy. And I said

63
00:05:20,540 --> 00:05:24,160
, Yeah, I think I've had enough

64
00:05:24,160 --> 00:05:27,650
 with Western philosophy. Maybe I should be probably should

65
00:05:27,650 --> 00:05:29,600
 have taken that one instead.

66
00:05:29,600 --> 00:05:36,960
 Not sure why I didn't. Maybe it's a night class or

67
00:05:36,960 --> 00:05:42,560
 something. I think I'll probably stick

68
00:05:42,560 --> 00:05:48,370
 with it for another semester because stopping in January

69
00:05:48,370 --> 00:05:51,440
 seems a bit troublesome. With the

70
00:05:51,440 --> 00:05:54,050
 whole I mean, they're paid, they've paid paid for my

71
00:05:54,050 --> 00:05:55,960
 tuition, the government paid for my

72
00:05:55,960 --> 00:05:59,390
 tuition for the whole year. I'm not sure how they look at

73
00:05:59,390 --> 00:06:03,480
 it if I dropped out. It gets complicated,

74
00:06:03,480 --> 00:06:09,030
 I think. I think so. Have you signed up for classes for

75
00:06:09,030 --> 00:06:11,320
 next semester? Is it too early?

76
00:06:11,320 --> 00:06:14,170
 I have, but I'm probably going to change it. I'm probably

77
00:06:14,170 --> 00:06:15,720
 going to take the second half

78
00:06:15,720 --> 00:06:23,060
 of Latin. Because it is useful. It's the base of English,

79
00:06:23,060 --> 00:06:25,760
 it's one of the bases of English.

80
00:06:25,760 --> 00:06:29,070
 But also because it's pretty easy. I'm currently getting I

81
00:06:29,070 --> 00:06:33,040
 think 103% in the class. I've got

82
00:06:33,040 --> 00:06:37,280
 over 100% in each of the quizzes. So it's it's it's I wasn

83
00:06:37,280 --> 00:06:39,360
't going to take the second half.

84
00:06:39,360 --> 00:06:43,260
 But then now I thought, Well, it's so easy, you know, it's

85
00:06:43,260 --> 00:06:45,360
 one less thing I have to really

86
00:06:45,360 --> 00:06:50,200
 expend a lot of energy. They don't have poly or Sanskrit.

87
00:06:50,200 --> 00:06:51,160
 They have Sanskrit, but I was

88
00:06:51,160 --> 00:06:54,650
 afraid to take it because my Sanskrit is very rusty. And

89
00:06:54,650 --> 00:06:57,520
 this was going to be my first goal.

90
00:06:57,520 --> 00:07:01,300
 So I can't take it now in January, I'd have to start it

91
00:07:01,300 --> 00:07:03,840
 next semester next September.

92
00:07:03,840 --> 00:07:07,660
 Probably should have taken it in retrospect. But I was

93
00:07:07,660 --> 00:07:10,440
 cautious. Also, I was I was thinking

94
00:07:10,440 --> 00:07:16,450
 I'd be in Stony Creek. So yeah. Now it's good to be

95
00:07:16,450 --> 00:07:19,120
 cautious your first semester back. I

96
00:07:19,120 --> 00:07:25,660
 want to get overwhelmed. So looking back, I could take it

97
00:07:25,660 --> 00:07:27,800
 fourth year Sanskrit. It's

98
00:07:27,800 --> 00:07:33,140
 really hardcore. I might not be good to take it because I

99
00:07:33,140 --> 00:07:37,000
 can't remember much of Sanskrit

100
00:07:37,000 --> 00:07:45,580
 grammar. I'd be really rusty. They don't have anything more

101
00:07:45,580 --> 00:07:47,920
 introductory than fourth year.

102
00:07:47,920 --> 00:07:51,360
 Well, they it's strange how they made it. The first year,

103
00:07:51,360 --> 00:07:54,280
 the first class is third year.

104
00:07:54,280 --> 00:07:57,100
 And the second class is fourth year, there's no first or

105
00:07:57,100 --> 00:07:58,720
 second year. I think the point

106
00:07:58,720 --> 00:08:02,160
 being that it's more more designed, they don't want first

107
00:08:02,160 --> 00:08:04,320
 years taking it. I think they did

108
00:08:04,320 --> 00:08:07,880
 that. Because half the people end up failing the course. Or

109
00:08:07,880 --> 00:08:09,520
 when I took it, I think half

110
00:08:09,520 --> 00:08:15,280
 the people dropped out or failed it. And that's when it was

111
00:08:15,280 --> 00:08:18,120
 set as a third year class. So

112
00:08:18,120 --> 00:08:22,720
 I think the point is, if they made it a first year class,

113
00:08:22,720 --> 00:08:27,440
 it's just a recipe for disaster.

114
00:08:27,440 --> 00:08:30,850
 Yeah. And make it a third year class, I guess there's a lot

115
00:08:30,850 --> 00:08:32,000
 of prerequisite, I think you

116
00:08:32,000 --> 00:08:41,600
 might have to be an upper year to take it or something.

117
00:08:41,600 --> 00:08:46,100
 You ready for ready for a question, Bhante? Sure. How does

118
00:08:46,100 --> 00:08:47,400
 one note wanting to not do

119
00:08:47,400 --> 00:08:50,980
 something as in trying to suppress a behavior? Can I say

120
00:08:50,980 --> 00:08:53,640
 not wanting?

121
00:08:53,640 --> 00:09:08,560
 Sure. There was once this nun, Thai woman who I think she

122
00:09:08,560 --> 00:09:10,560
 wasn't a nun in the beginning.

123
00:09:10,560 --> 00:09:17,370
 I thought she's very good at ordaining people. She was bul

124
00:09:17,370 --> 00:09:20,480
imic, I think. And so she she she

125
00:09:20,480 --> 00:09:25,720
 couldn't keep food down, she would throw it. And maybe bul

126
00:09:25,720 --> 00:09:27,200
imic, or maybe she just couldn't

127
00:09:27,200 --> 00:09:32,730
 keep it down. I don't know, maybe it was a sickness. And so

128
00:09:32,730 --> 00:09:33,680
 she explained this to him

129
00:09:33,680 --> 00:09:38,920
 and he had her noting, you know, a Tia, no, Tia, no,

130
00:09:38,920 --> 00:09:43,240
 throwing up, throwing up, vomiting,

131
00:09:43,240 --> 00:09:50,920
 and she started she was practicing, but it was really

132
00:09:50,920 --> 00:09:54,560
 difficult for her. And so she came

133
00:09:54,560 --> 00:10:04,400
 back one day and said, and Jan, I want to die, she said.

134
00:10:04,400 --> 00:10:07,000
 She said, I can't take it.

135
00:10:07,000 --> 00:10:14,430
 She said, I want to die. Or it was something like, I'm not,

136
00:10:14,430 --> 00:10:16,200
 I think it started off, I'm

137
00:10:16,200 --> 00:10:22,120
 not, is it my why, which means I can't take it, basically.

138
00:10:22,120 --> 00:10:25,320
 And he said, he said, I can't

139
00:10:25,320 --> 00:10:28,440
 take it, just say to yourself, can't take it, can't take it

140
00:10:28,440 --> 00:10:30,360
. And he said, and I want

141
00:10:30,360 --> 00:10:34,630
 to kill myself, I want to die, wanting to die, wanting to

142
00:10:34,630 --> 00:10:37,160
 die. And just, I don't want to

143
00:10:37,160 --> 00:10:41,980
 live, not wanting to live. No, he really doesn't get face

144
00:10:41,980 --> 00:10:47,800
 does he? He gave her no quarter. And

145
00:10:47,800 --> 00:10:51,360
 finally she started laughing and she broke down. Like, like

146
00:10:51,360 --> 00:10:54,200
 she, she let go a little bit,

147
00:10:54,200 --> 00:10:59,940
 you know, and started laughing. And he started laughing

148
00:10:59,940 --> 00:11:01,960
 with her. And I think then she became

149
00:11:01,960 --> 00:11:10,230
 a nun, she ended up becoming a nun. She, she really was

150
00:11:10,230 --> 00:11:15,560
 able to overcome it. That that

151
00:11:15,560 --> 00:11:19,170
 that's, it was kind of being facetious, I think, I mean,

152
00:11:19,170 --> 00:11:21,160
 you really shouldn't say, I

153
00:11:21,160 --> 00:11:23,350
 don't want to live, not wanting to live, not wanting to

154
00:11:23,350 --> 00:11:24,840
 live, it should be a little more

155
00:11:24,840 --> 00:11:28,920
 specific than that, like you're upset or sad or depressed,

156
00:11:28,920 --> 00:11:33,320
 that's a bit better. But you

157
00:11:33,320 --> 00:11:38,390
 see how sometimes that's the best way to when people are

158
00:11:38,390 --> 00:11:41,400
 stubborn, especially, you don't

159
00:11:41,400 --> 00:11:43,130
 want to live, well, they just say not wanting to live in

160
00:11:43,130 --> 00:11:47,400
 that one. It's kind of this kind

161
00:11:47,400 --> 00:11:50,520
 of question. It's, it's very much about semantics. It's not

162
00:11:50,520 --> 00:11:52,840
 really a real question. It's not

163
00:11:52,840 --> 00:11:56,850
 really an important one. I'm sorry, I don't mean to be too

164
00:11:56,850 --> 00:11:59,480
 critical. But I think if you

165
00:11:59,480 --> 00:12:02,620
 look closer, you'll see that there's, there's realities

166
00:12:02,620 --> 00:12:04,360
 that are a bit more specific and

167
00:12:04,360 --> 00:12:08,390
 real than whether you want something or you don't want it.

168
00:12:08,390 --> 00:12:11,000
 It's all just words. What's

169
00:12:11,000 --> 00:12:13,200
 the state of mind is the question. What's going on in the

170
00:12:13,200 --> 00:12:14,520
 mind? That's the question.

171
00:12:15,640 --> 00:12:19,270
 If it's, if you want something, if you want, or if you have

172
00:12:19,270 --> 00:12:21,560
 desire arise, then it's desire.

173
00:12:21,560 --> 00:12:24,000
 If you have aversion arise, then it's aversion, a very

174
00:12:24,000 --> 00:12:25,880
 distinct mind states, and you can tell

175
00:12:25,880 --> 00:12:28,390
 which one. And often they're mixed up with each other,

176
00:12:28,390 --> 00:12:30,280
 there's desire, then there's aversion,

177
00:12:30,280 --> 00:12:36,280
 and there's delusion. There's lots of states.

178
00:12:36,280 --> 00:12:42,860
 A follow up comment from the questioner. It says it's, who

179
00:12:42,860 --> 00:12:44,600
 says it's clearly wanting,

180
00:12:44,600 --> 00:12:47,800
 but he's not sure if he needs to emphasize the negation.

181
00:12:47,800 --> 00:12:52,200
 I think you have to say to yourself thinking, thinking,

182
00:12:52,200 --> 00:12:56,410
 wondering, wondering, doubting, doubting sounds more, more

183
00:12:56,410 --> 00:12:57,400
 important.

184
00:12:57,400 --> 00:13:08,400
 I have a 10 year old niece who always asks me questions

185
00:13:08,400 --> 00:13:12,920
 about Buddhism. I was thinking about

186
00:13:12,920 --> 00:13:15,630
 getting her a book on Jataka stories. I think you have

187
00:13:15,630 --> 00:13:18,440
 experience teaching kids. What do you think?

188
00:13:18,440 --> 00:13:21,480
 Is it age appropriate? Can you recommend any additions?

189
00:13:21,480 --> 00:13:25,240
 There are Jatakas for children by

190
00:13:25,240 --> 00:13:30,460
 Ken and Wissako Kawasaki. You find them on budanet.net. J

191
00:13:30,460 --> 00:13:34,040
ataka stories for kids.

192
00:13:34,040 --> 00:13:38,210
 Most of them are pretty good. They're retold in a way that

193
00:13:38,210 --> 00:13:42,520
 kids find helpful. Someone once

194
00:13:42,520 --> 00:13:47,270
 sent me a book for children. Jataka stories retold. There

195
00:13:47,270 --> 00:13:48,760
 was a monk. It's actually,

196
00:13:48,760 --> 00:13:52,290
 you can buy this book apparently. It's like Buddhist bed

197
00:13:52,290 --> 00:13:55,000
time stories or something.

198
00:13:55,000 --> 00:14:06,490
 But yeah, the one by Ken and Wissako Kawasaki is probably a

199
00:14:06,490 --> 00:14:07,960
 good bet.

200
00:14:07,960 --> 00:14:17,000
 If you're looking for that.

201
00:14:17,000 --> 00:14:22,790
 Another thing is maybe show her my DVD on how to meditate

202
00:14:22,790 --> 00:14:24,120
 for kids.

203
00:14:24,120 --> 00:14:28,880
 There's some good lessons in there on different ways of med

204
00:14:28,880 --> 00:14:29,480
itating.

205
00:14:29,480 --> 00:14:31,830
 Opening kids' eyes up, getting them an idea of what

206
00:14:31,830 --> 00:14:32,680
 meditation is.

207
00:14:34,360 --> 00:14:41,180
 It's not just insight meditation, but meditation primer for

208
00:14:41,180 --> 00:14:43,480
 kids.

209
00:14:43,480 --> 00:14:49,100
 Four videos. That's really worth it. I think it's good. I

210
00:14:49,100 --> 00:14:52,440
've seen good results with it for

211
00:14:52,440 --> 00:14:56,140
 a three and a five year old kid. They were able to

212
00:14:56,140 --> 00:14:57,320
 understand it.

213
00:14:57,320 --> 00:15:01,650
 Or just my book on how to meditate. If she's 11, she can

214
00:15:01,650 --> 00:15:02,440
 already start to

215
00:15:03,160 --> 00:15:10,200
 learn how to meditate from that booklet probably.

216
00:15:10,200 --> 00:15:18,590
 If an abbot dies or leaves some monastery, how is the next

217
00:15:18,590 --> 00:15:21,480
 abbot selected?

218
00:15:26,440 --> 00:15:33,240
 Abbots aren't a part of Buddhism. We don't have abbots in

219
00:15:33,240 --> 00:15:34,920
 the Vinaya.

220
00:15:34,920 --> 00:15:39,590
 So that's totally dependent on the country. It has nothing

221
00:15:39,590 --> 00:15:41,400
 to do with Buddhism directly.

222
00:15:41,400 --> 00:15:44,060
 Many monks have complained about that. Well, not many monks

223
00:15:44,060 --> 00:15:45,640
. Some monks have complained about that.

224
00:15:45,640 --> 00:15:50,460
 That abbots in most countries have too much power. It's not

225
00:15:50,460 --> 00:15:52,840
 according to the Vinaya at all.

226
00:15:54,120 --> 00:15:57,900
 According to the Vinaya, the Sangha has to be in charge,

227
00:15:57,900 --> 00:15:59,000
 not for one person.

228
00:15:59,000 --> 00:16:03,640
 Is an abbot different than a head monk? There's no such

229
00:16:03,640 --> 00:16:04,840
 thing as a head monk or an

230
00:16:04,840 --> 00:16:12,780
 abbot. That term doesn't exist. It goes by seniority always

231
00:16:12,780 --> 00:16:14,280
. So if a more senior monk

232
00:16:14,280 --> 00:16:19,940
 comes, he's more senior. But it goes by seniority in some

233
00:16:19,940 --> 00:16:22,760
 cases. For decision making,

234
00:16:22,760 --> 00:16:26,040
 it has to go by the Sangha.

235
00:16:26,040 --> 00:16:38,330
 And even still, you know, there's not... Well, if you're

236
00:16:38,330 --> 00:16:39,800
 staying in a place where there's not

237
00:16:39,800 --> 00:16:42,460
 four monks, then you can't have a Sangha. So then it's just

238
00:16:42,460 --> 00:16:44,040
, you know, you try to get along and

239
00:16:44,040 --> 00:16:47,320
 try to be respectful to your seniors, that kind of thing.

240
00:16:47,320 --> 00:16:59,720
 [silence]

241
00:16:59,720 --> 00:17:02,070
 Well, we hear those terms all the time, head monk and abbot

242
00:17:02,070 --> 00:17:02,520
. So

243
00:17:02,520 --> 00:17:06,580
 it's just something that just kind of came to be, but it's

244
00:17:06,580 --> 00:17:08,360
 not really supposed to be.

245
00:17:08,360 --> 00:17:14,280
 Not really. Yeah, we have to be careful. It can be a big

246
00:17:14,280 --> 00:17:18,360
 ego trip in the head monk.

247
00:17:18,360 --> 00:17:23,720
 [silence]

248
00:17:23,720 --> 00:17:26,800
 Seniority with monks. Is it years as a monk or years of

249
00:17:26,800 --> 00:17:27,400
 life?

250
00:17:27,400 --> 00:17:34,040
 It's moments as a monk. So I was ordained with 18 monks and

251
00:17:34,040 --> 00:17:36,760
 I think I was the second to last.

252
00:17:38,440 --> 00:17:40,440
 So all the others were senior to me.

253
00:17:40,440 --> 00:17:45,960
 Actually, I'm not sure if that's true because we are

254
00:17:45,960 --> 00:17:48,200
 ordained in three,

255
00:17:48,200 --> 00:17:50,280
 so I don't know how that works. I think it still works

256
00:17:50,280 --> 00:17:52,040
 according to eight, according to...

257
00:17:52,040 --> 00:17:57,880
 There's an order, but we are we ordained three at a time.

258
00:17:57,880 --> 00:18:01,390
 So I'm not sure how that quite works. Anyway, none of those

259
00:18:01,390 --> 00:18:03,480
... I'm the only one left.

260
00:18:05,240 --> 00:18:08,520
 So then you're the senior? Well, they're all disrobed.

261
00:18:08,520 --> 00:18:13,180
 Yes. But yeah, you have to... So when you meet a monk, you

262
00:18:13,180 --> 00:18:14,440
 have to ask when they were ordained,

263
00:18:14,440 --> 00:18:17,780
 and then sometimes you have to calculate. Usually you say,

264
00:18:17,780 --> 00:18:19,240
 "How many reigns are you?"

265
00:18:19,240 --> 00:18:23,320
 And then if it's an equal number, then you have to ask, "

266
00:18:23,320 --> 00:18:24,680
When were they? Were you ordained?"

267
00:18:24,680 --> 00:18:31,810
 I'm older than most monks my age because I wasn't... Most

268
00:18:31,810 --> 00:18:33,960
 monks, at least in Thailand,

269
00:18:33,960 --> 00:18:37,300
 because most Thai monks ordain before the reigns, but I

270
00:18:37,300 --> 00:18:40,120
 ordained for the king's birthday.

271
00:18:40,120 --> 00:18:43,240
 "Oh, today's my birthday." No, I ordained yesterday.

272
00:18:43,240 --> 00:18:45,800
 December 4th, I missed my birthday.

273
00:18:45,800 --> 00:18:49,960
 Congratulations, Arif. Stop thinking about it.

274
00:18:49,960 --> 00:18:51,880
 So that's... I was trying to think, "How many is that?"

275
00:18:51,880 --> 00:18:55,640
 That's 14 now, 14 years a monk, yesterday,

276
00:18:55,640 --> 00:18:58,040
 because I ordained for the king's birthday, which is today,

277
00:18:58,040 --> 00:19:00,480
 but we ordained the day before because on the king's

278
00:19:00,480 --> 00:19:02,520
 birthday, there's too much going on.

279
00:19:02,520 --> 00:19:08,680
 So yeah, my birthday was yesterday.

280
00:19:08,680 --> 00:19:12,920
 Well, happy belated birthday, Bhante. Thank you.

281
00:19:12,920 --> 00:19:19,640
 Do you have a sangha in Hamilton?

282
00:19:19,640 --> 00:19:27,000
 Well, there's... In Stony Creek, there's at least three,

283
00:19:27,000 --> 00:19:28,120
 sometimes four monks,

284
00:19:28,120 --> 00:19:33,600
 so together with them, we can form a sangha. Then there's a

285
00:19:33,600 --> 00:19:34,520
 Laotian monk,

286
00:19:34,520 --> 00:19:38,010
 and there's a bunch more Laotian monks and Sri Lankan monks

287
00:19:38,010 --> 00:19:39,240
 in Toronto area.

288
00:19:39,240 --> 00:19:43,080
 There's lots of monks around if we wanted to form a sangha,

289
00:19:43,080 --> 00:19:45,400
 but I'm alone, so I don't have a sangha here.

290
00:19:45,400 --> 00:19:52,360
 Do any of them get together to recite the Kathimaka?

291
00:19:52,360 --> 00:19:55,400
 Not that I know of.

292
00:19:56,520 --> 00:19:58,520
 [silence]

293
00:19:58,520 --> 00:20:06,900
 Simon tried to give you the cake emote in the meditation

294
00:20:06,900 --> 00:20:11,720
 chat, but it's not working.

295
00:20:11,720 --> 00:20:12,280
 Doesn't work?

296
00:20:12,280 --> 00:20:15,760
 No, I had noticed that too. I was trying to do that for

297
00:20:15,760 --> 00:20:17,880
 someone's birthday at one point.

298
00:20:17,880 --> 00:20:20,840
 Bet I can fix that.

299
00:20:20,840 --> 00:20:27,880
 [silence]

300
00:20:27,880 --> 00:20:29,000
 Looks like a cake to you.

301
00:20:29,000 --> 00:20:36,520
 Must just not be showing it for some reason on the strange

302
00:20:36,520 --> 00:20:36,600
...

303
00:20:36,600 --> 00:20:47,720
 Nutty cakes, indeed.

304
00:20:47,800 --> 00:20:57,800
 [silence]

305
00:20:57,800 --> 00:21:07,880
 [silence]

306
00:21:07,880 --> 00:21:17,880
 [silence]

307
00:21:17,880 --> 00:21:27,960
 [silence]

308
00:21:27,960 --> 00:21:37,960
 [silence]

309
00:21:37,960 --> 00:21:50,040
 [silence]

310
00:21:50,040 --> 00:22:00,040
 [silence]

311
00:22:00,040 --> 00:22:16,120
 [silence]

312
00:22:16,120 --> 00:22:18,120
 Let's find that symbol.

313
00:22:18,120 --> 00:22:30,200
 [silence]

314
00:22:30,200 --> 00:22:42,200
 [silence]

315
00:22:42,200 --> 00:22:58,280
 [silence]

316
00:22:58,280 --> 00:23:02,280
 Hmm. How strange.

317
00:23:06,280 --> 00:23:12,360
 It should be...it should work, actually. Looks like...

318
00:23:12,360 --> 00:23:22,360
 [silence]

319
00:23:22,360 --> 00:23:27,670
 It's funny looking at a programming code. It's all gibber

320
00:23:27,670 --> 00:23:28,360
ish.

321
00:23:28,360 --> 00:23:31,320
 [silence]

322
00:23:31,320 --> 00:23:33,320
 Computers are crazy things.

323
00:23:33,320 --> 00:23:39,320
 Yes, they are. We have some fairly inappropriate emoticons

324
00:23:39,320 --> 00:23:43,320
 for our meditation chat panel. There's like a cocktail and

325
00:23:43,320 --> 00:23:43,320
...

326
00:23:43,320 --> 00:23:45,320
 Uh-oh. We can get rid of them.

327
00:23:45,320 --> 00:23:47,320
 [chuckle]

328
00:23:47,320 --> 00:23:51,750
 Some of them, I can't quite see what they are. Maybe a

329
00:23:51,750 --> 00:23:55,320
 pizza? Oh, okay, pizza. Pizza's fine.

330
00:23:55,320 --> 00:24:15,320
 [silence]

331
00:24:15,320 --> 00:24:27,360
 Lots of angry faces. Devil. Envy. We tend not to use those.

332
00:24:27,360 --> 00:24:29,320
 I'm pretty happy bunch here.

333
00:24:29,320 --> 00:24:32,690
 Okay, I'm gonna try something. You have to tell me if it

334
00:24:32,690 --> 00:24:33,320
 works.

335
00:24:33,320 --> 00:24:57,320
 [silence]

336
00:24:57,320 --> 00:25:19,320
 [silence]

337
00:25:19,320 --> 00:25:27,140
 No, no, that won't work. Oh, I wonder if...no, I don't know

338
00:25:27,140 --> 00:25:34,360
. I don't know what's happening. I'm too dumb for this. I

339
00:25:34,360 --> 00:25:39,320
 don't really know what's going on.

340
00:25:39,320 --> 00:25:46,580
 Replace that. Oh, wait, it's insertSmiley that we want.

341
00:25:46,580 --> 00:25:52,040
 This is the problem. Message, val, val plus s, insertSmiley

342
00:25:52,040 --> 00:25:52,320
.

343
00:25:52,320 --> 00:26:01,320
 Okay, so each SmileyBox, I have to find the SmileyBox.

344
00:26:01,320 --> 00:26:08,970
 There's a populateSmiley's thing, right? Where we populate

345
00:26:08,970 --> 00:26:10,320
 the box.

346
00:26:10,320 --> 00:26:14,140
 Is this done on WordPress as well, Bunty? No, this is just

347
00:26:14,140 --> 00:26:21,290
 something I put together. Probably not something we want to

348
00:26:21,290 --> 00:26:24,320
 work on in a live forum.

349
00:26:24,320 --> 00:26:32,670
 Everyone's trying the kink. All right, I'll figure it out.

350
00:26:32,670 --> 00:26:34,900
 Can you work on it and also answer a question at the same

351
00:26:34,900 --> 00:26:35,320
 time?

352
00:26:35,320 --> 00:26:38,810
 I shouldn't do two things at once. What's the question? Is

353
00:26:38,810 --> 00:26:43,320
 intense fear common in meditation?

354
00:26:43,320 --> 00:26:48,050
 Any question that asks whether something's common is really

355
00:26:48,050 --> 00:26:53,220
 not really...you can't really answer that. There's no such

356
00:26:53,220 --> 00:26:58,320
 thing as common. Everyone has different experiences.

357
00:26:58,320 --> 00:27:02,720
 What would it matter if it was common? What does that mean?

358
00:27:02,720 --> 00:27:05,320
 It doesn't really mean anything.

359
00:27:05,320 --> 00:27:10,160
 Maybe that's another way of saying, "This is happening to

360
00:27:10,160 --> 00:27:13,320
 me. Am I doing something wrong?"

361
00:27:13,320 --> 00:27:17,790
 Even asking whether you're doing something wrong is a

362
00:27:17,790 --> 00:27:22,160
 misunderstanding generally, because if you're being mindful

363
00:27:22,160 --> 00:27:23,320
, then you're doing it right.

364
00:27:23,320 --> 00:27:25,740
 If you're not being mindful, then it doesn't matter what

365
00:27:25,740 --> 00:27:26,320
 happens.

366
00:27:26,320 --> 00:27:31,370
 Doing it wrong doesn't have anything to do with what

367
00:27:31,370 --> 00:27:36,320
 happens. It's how you react to what happens.

368
00:27:36,320 --> 00:27:40,780
 That is a good point. If you're doing it right, you'd think

369
00:27:40,780 --> 00:27:44,320
, "Why is this fear coming up in me? I'm meditating."

370
00:27:44,320 --> 00:28:00,250
 But that's really it. Meditation often helps you to see

371
00:28:00,250 --> 00:28:10,320
 things that you don't want to see.

372
00:28:10,320 --> 00:28:21,320
 Bhandi, can we meditate to help others be mindful?

373
00:28:21,320 --> 00:28:25,240
 Not directly. I mean, if you're mindful, it encourages

374
00:28:25,240 --> 00:28:29,320
 other people, and it provides a good example for them.

375
00:28:29,320 --> 00:28:34,630
 But I don't really see how it would be possible to...there

376
00:28:34,630 --> 00:28:37,320
's certainly nothing about meditating to help.

377
00:28:37,320 --> 00:28:50,320
 Then why would that be something you think is possible?

378
00:28:50,320 --> 00:28:55,490
 It may be helping someone meditating to give a wish that

379
00:28:55,490 --> 00:28:58,320
 someone could be mindful.

380
00:28:58,320 --> 00:29:01,610
 Maybe sending metta, sending a wish that someone can be

381
00:29:01,610 --> 00:29:05,320
 mindful. Is that possible?

382
00:29:05,320 --> 00:29:13,320
 Well, if wishes were fishes, then we'd have a fry.

383
00:29:13,320 --> 00:29:15,320
 Must be a Canadian saying.

384
00:29:15,320 --> 00:29:20,320
 That's an American saying. I was told that by an American.

385
00:29:20,320 --> 00:29:25,240
 I don't know. It could be. I'm sure I haven't heard all the

386
00:29:25,240 --> 00:29:26,320
 sayings.

387
00:29:26,320 --> 00:29:29,320
 Okay, you know what's happening? This has nothing to do

388
00:29:29,320 --> 00:29:32,320
 with the smileys. The smileys are being put correctly.

389
00:29:32,320 --> 00:29:36,380
 So I can turn all those things into cakes. I just have to

390
00:29:36,380 --> 00:29:40,320
 figure out something's going on somewhere else.

391
00:29:40,320 --> 00:29:47,320
 And we're formatting the chat improperly.

392
00:29:47,320 --> 00:29:50,900
 Okay, so for our meditation group, we have smileys that

393
00:29:50,900 --> 00:29:56,320
 indicate swearing, punching, angry, and the devil.

394
00:29:56,320 --> 00:30:11,320
 I think we need some new smileys. Bombarding.

395
00:30:11,320 --> 00:30:19,960
 Okay, you know what I'm going to do? I'm going to send my

396
00:30:19,960 --> 00:30:23,320
 phone.

397
00:30:23,320 --> 00:30:28,180
 This is good. You're all going to get a bunch of alerts now

398
00:30:28,180 --> 00:30:29,320
 that are going to make you think it's going crazy.

399
00:30:29,320 --> 00:30:36,080
 It's going to go crazy for a bit just because I have to

400
00:30:36,080 --> 00:30:40,320
 test and see what's going on.

401
00:30:40,320 --> 00:30:54,320
 I mean, just don't refresh your browser.

402
00:30:54,320 --> 00:31:18,320
 Okay.

403
00:31:18,320 --> 00:31:44,320
 Okay.

404
00:31:44,320 --> 00:32:08,320
 Okay.

405
00:32:08,320 --> 00:32:17,320
 The hard way.

406
00:32:17,320 --> 00:32:26,320
 There's too many.

407
00:32:26,320 --> 00:32:33,050
 Okay, it is removing that thing from... Is the cake the

408
00:32:33,050 --> 00:32:37,290
 only one not working? Did anyone notice anything else not

409
00:32:37,290 --> 00:32:38,320
 working?

410
00:32:38,320 --> 00:33:07,320
 I think that's the only one I've come across.

411
00:33:07,320 --> 00:33:25,320
 Oh, well.

412
00:33:25,320 --> 00:33:31,440
 It's beyond me. No cake. Not the cake. It's a good lesson

413
00:33:31,440 --> 00:33:36,390
 for us all. Let me remove that. We want to remove the mart

414
00:33:36,390 --> 00:33:37,320
ini.

415
00:33:37,320 --> 00:33:41,090
 Well, there's a bunch of them. There's like punching and

416
00:33:41,090 --> 00:33:44,320
 swearing and hitting and puking and...

417
00:33:44,320 --> 00:33:49,320
 Puking is okay. Puking is part of the meditation process.

418
00:33:49,320 --> 00:33:52,750
 That's true. I'm not sure that anyone's ever used that in

419
00:33:52,750 --> 00:33:56,020
 Shaco. But it is kind of cool. It does take the alt

420
00:33:56,020 --> 00:33:57,320
 characters.

421
00:33:57,320 --> 00:34:00,650
 I hadn't realized that or hadn't thought about that in a

422
00:34:00,650 --> 00:34:01,320
 while.

423
00:34:01,320 --> 00:34:07,890
 We got faces and then we got drink. Probably the drink one

424
00:34:07,890 --> 00:34:12,320
 with... Is it a d with parenthesis? Is that the one?

425
00:34:12,320 --> 00:34:15,320
 It looks like double parenthesis d.

426
00:34:15,320 --> 00:34:22,320
 Yeah, okay. Remove that. Ninja. Can we keep the ninja?

427
00:34:22,320 --> 00:34:28,320
 Ninja's kind of cool. Who's Toivo?

428
00:34:28,320 --> 00:34:32,320
 And Heidi.

429
00:34:32,320 --> 00:34:35,270
 We got rid of the finger, the bandit, the drunk, smoking,

430
00:34:35,270 --> 00:34:39,320
 beer and mooning. Those ones are gone.

431
00:34:39,320 --> 00:34:48,320
 Those I've deleted already. Pizza, coffee, phone...

432
00:34:48,320 --> 00:34:54,320
 There's punching, right diagonal from the green one.

433
00:34:54,320 --> 00:34:57,320
 What's the code for that?

434
00:34:57,320 --> 00:35:00,320
 Double parenthesis punch.

435
00:35:00,320 --> 00:35:02,320
 Punch?

436
00:35:02,320 --> 00:35:03,320
 Punch.

437
00:35:03,320 --> 00:35:06,320
 I don't see that.

438
00:35:06,320 --> 00:35:09,320
 Must have missed it. Maybe it's up here.

439
00:35:09,320 --> 00:35:16,320
 I see puke and giggle clap rolling on the floor laughing.

440
00:35:16,320 --> 00:35:19,490
 Fubar, swear, swearing. Are we going to give up the

441
00:35:19,490 --> 00:35:20,320
 swearing one?

442
00:35:20,320 --> 00:35:23,320
 I don't think anyone's ever used that.

443
00:35:23,320 --> 00:35:28,320
 Swearing and Fubar will get rid of punch. Okay, I found it.

444
00:35:28,320 --> 00:35:31,320
 Face palm, fingers crossed.

445
00:35:31,320 --> 00:35:35,320
 Got the cash money sign there.

446
00:35:35,320 --> 00:35:38,320
 You sometimes have to talk about money.

447
00:35:38,320 --> 00:35:44,320
 True.

448
00:35:44,320 --> 00:35:47,320
 Anyway, we're kind of just wasting time, aren't we?

449
00:35:47,320 --> 00:35:51,320
 Well, I think there was another question. I'm sorry.

450
00:35:51,320 --> 00:35:52,320
 Okay.

451
00:35:52,320 --> 00:35:55,320
 Falling down on my duties here.

452
00:35:55,320 --> 00:35:58,980
 When meeting someone for the first time, what question do

453
00:35:58,980 --> 00:36:02,410
 you find most helpful to develop an understanding between

454
00:36:02,410 --> 00:36:04,320
 each other?

455
00:36:04,320 --> 00:36:11,320
 I don't think so.

456
00:36:11,320 --> 00:36:14,610
 That's an interesting question, I don't think of such

457
00:36:14,610 --> 00:36:18,320
 things. I don't worry about such things.

458
00:36:18,320 --> 00:36:21,590
 I think you should be mindful when you meet people. That's

459
00:36:21,590 --> 00:36:22,320
 more important.

460
00:36:22,320 --> 00:36:25,540
 Could you imagine if you had one question that you asked

461
00:36:25,540 --> 00:36:29,640
 everybody, wouldn't it come off really like canned, you

462
00:36:29,640 --> 00:36:30,320
 know?

463
00:36:30,320 --> 00:36:32,320
 Like a pickup line.

464
00:36:32,320 --> 00:36:36,320
 Yeah. Oh, you must say that to all the...

465
00:36:36,320 --> 00:36:45,320
 You must say that to all the Buddhists.

466
00:36:45,320 --> 00:36:47,780
 I don't have a list of questions that I have. I have a list

467
00:36:47,780 --> 00:36:49,320
 of questions that I ask meditators.

468
00:36:49,320 --> 00:36:53,320
 I've got some fairly rote questions there.

469
00:36:53,320 --> 00:36:56,540
 When I meet people, I try to be as natural as I can. I

470
00:36:56,540 --> 00:37:03,780
 think being natural is important. Not having an agenda, not

471
00:37:03,780 --> 00:37:04,320
 having anything behind, you know?

472
00:37:04,320 --> 00:37:09,870
 Not coming as a Buddhist, even. Coming at people from the

473
00:37:09,870 --> 00:37:14,320
 point of view of a human being, you know?

474
00:37:14,320 --> 00:37:20,520
 As natural as possible. So that's what we're aiming for, is

475
00:37:20,520 --> 00:37:22,320
 nature beyond nature.

476
00:37:22,320 --> 00:37:26,920
 Because nature, as we know it, is all messed up. It's not

477
00:37:26,920 --> 00:37:28,320
 really natural.

478
00:37:28,320 --> 00:37:32,410
 But the nature that we understand is like rape is natural

479
00:37:32,410 --> 00:37:34,320
 and murder is natural.

480
00:37:34,320 --> 00:37:37,880
 These are all part of the natural world, but they're very

481
00:37:37,880 --> 00:37:39,320
 unnatural things.

482
00:37:39,320 --> 00:37:44,320
 So through meditation, we become quite natural in a sense.

483
00:37:44,320 --> 00:37:50,320
 In a sense of at ease and without pretense or agenda,

484
00:37:50,320 --> 00:37:53,320
 without artifice of any sort.

485
00:37:53,320 --> 00:38:11,320
 [Silence]

486
00:38:11,320 --> 00:38:14,320
 I think we should just say good night.

487
00:38:14,320 --> 00:38:16,320
 Thank you, Bante.

488
00:38:16,320 --> 00:38:19,320
 Thank you, Robin. Thanks, everyone. Have a good night.

489
00:38:19,320 --> 00:38:22,320
 Good night.

490
00:38:22,320 --> 00:38:26,320
 Oh, we had that...

491
00:38:26,320 --> 00:38:29,630
 We have a quote, right? So we ended the broadcast. That's

492
00:38:29,630 --> 00:38:33,840
 what we're supposed to do, right? We're supposed to look at

493
00:38:33,840 --> 00:38:35,320
 that quote.

494
00:38:35,320 --> 00:38:41,970
 Well, we have to remember. Next time we have to remember to

495
00:38:41,970 --> 00:38:44,320
 look at the quote.

496
00:38:44,320 --> 00:38:49,320
 And I'll try to make videos.

497
00:38:49,320 --> 00:38:53,400
 I'll try to maybe add a video. Maybe Saturday night we can

498
00:38:53,400 --> 00:38:54,320
 have a video.

499
00:38:54,320 --> 00:38:59,000
 I'm recording. I'll start to do Buddhism 101 again. Maybe I

500
00:38:59,000 --> 00:39:01,320
 can do that Saturday.

501
00:39:01,320 --> 00:39:05,320
 Let's see. Good night.

502
00:39:05,320 --> 00:39:07,320
 Signing off.

