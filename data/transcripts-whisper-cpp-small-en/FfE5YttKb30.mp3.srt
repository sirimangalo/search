1
00:00:00,000 --> 00:00:05,000
 How does one take the middle way in regards, for example,

2
00:00:05,000 --> 00:00:06,000
 to food?

3
00:00:06,000 --> 00:00:10,250
 We should eat enough to have energy for all day, all our

4
00:00:10,250 --> 00:00:12,000
 daily duties,

5
00:00:12,000 --> 00:00:15,820
 but when we are not sure what is enough, we should rather

6
00:00:15,820 --> 00:00:16,000
 eat more than less

7
00:00:16,000 --> 00:00:19,380
 because negative effects from restraining are more

8
00:00:19,380 --> 00:00:20,000
 destructive

9
00:00:20,000 --> 00:00:25,290
 than negative effects from indulging ourself. Is this right

10
00:00:25,290 --> 00:00:26,000
?

11
00:00:26,000 --> 00:00:37,260
 Well, I think it depends on what you are doing, because

12
00:00:37,260 --> 00:00:40,000
 obviously you are still living in the world.

13
00:00:40,000 --> 00:00:43,610
 I think that would make it easier to judge how much you

14
00:00:43,610 --> 00:00:45,000
 need, really,

15
00:00:45,000 --> 00:00:48,520
 because if you are doing that, it is not like every time

16
00:00:48,520 --> 00:00:49,000
 you eat,

17
00:00:49,000 --> 00:00:52,000
 you have to sit down and have this whole meal.

18
00:00:52,000 --> 00:00:58,000
 You should eat small things at various times instead.

19
00:00:58,000 --> 00:01:02,370
 I think that would make it a lot easier to regulate how

20
00:01:02,370 --> 00:01:04,000
 much you take in

21
00:01:04,000 --> 00:01:09,210
 and see how much you really need, unless you are keeping

22
00:01:09,210 --> 00:01:10,000
 eight precepts

23
00:01:10,000 --> 00:01:13,000
 than you have to eat before noon.

24
00:01:13,000 --> 00:01:20,720
 But, I don't know, indulging our overly restraining can be

25
00:01:20,720 --> 00:01:21,000
 bad.

26
00:01:21,000 --> 00:01:25,000
 I think it is going to be a matter of experimentation,

27
00:01:25,000 --> 00:01:26,000
 really.

28
00:01:26,000 --> 00:01:31,000
 You just have to realize how much energy you are using

29
00:01:31,000 --> 00:01:34,000
 during the day and eat accordingly.

30
00:01:34,000 --> 00:01:41,000
 Simple enough.

31
00:01:41,000 --> 00:01:48,720
 I had the same thought that when you are working, you have

32
00:01:48,720 --> 00:01:53,000
 another need for calories

33
00:01:53,000 --> 00:01:57,310
 or for nutriments than when you are just sitting and med

34
00:01:57,310 --> 00:01:58,000
itating.

35
00:01:58,000 --> 00:02:02,200
 So, as a layperson, you need more food and you need

36
00:02:02,200 --> 00:02:05,000
 eventually a third meal.

37
00:02:05,000 --> 00:02:10,750
 When you are a meditator here on a retreat, one meal could

38
00:02:10,750 --> 00:02:14,000
 be enough or two meals.

39
00:02:14,000 --> 00:02:19,800
 When you are on eight precepts and you have to work and you

40
00:02:19,800 --> 00:02:21,000
 feel hungry,

41
00:02:21,000 --> 00:02:26,350
 then something like a juice or so in the afternoon helps to

42
00:02:26,350 --> 00:02:28,000
 overcome that hunger

43
00:02:28,000 --> 00:02:34,000
 or that feeling of getting dizzy or getting nervous.

44
00:02:34,000 --> 00:02:38,000
 Before I ordained, I thought I need three meals.

45
00:02:38,000 --> 00:02:42,000
 I need to eat a lot.

46
00:02:42,000 --> 00:02:47,290
 I found out that I am fine with one meal when I am in

47
00:02:47,290 --> 00:02:49,000
 meditation retreat

48
00:02:49,000 --> 00:02:55,000
 and two meals when I am doing duties.

49
00:02:55,000 --> 00:03:01,780
 So, it is happening in the mind as well that you think you

50
00:03:01,780 --> 00:03:04,000
 really need it.

51
00:03:04,000 --> 00:03:07,580
 You may have a little bit of hunger in the evening when you

52
00:03:07,580 --> 00:03:12,000
 start to get four or two meals,

53
00:03:12,000 --> 00:03:20,000
 but after a while you will find that this is nothing bad.

54
00:03:20,000 --> 00:03:28,000
 For sure.

55
00:03:28,000 --> 00:03:32,000
 I have a couple of things to say, sort of corallaries,

56
00:03:32,000 --> 00:03:35,000
 and I think the question has already been answered.

57
00:03:35,000 --> 00:03:42,000
 First of all, to make clear that there are negative effects

58
00:03:42,000 --> 00:03:43,000
 from indulging,

59
00:03:43,000 --> 00:03:46,000
 that may not be clear.

60
00:03:46,000 --> 00:03:51,000
 Food is a lot more dangerous than we make it out to be,

61
00:03:51,000 --> 00:03:55,250
 or than we actually realize, because a lot of food will

62
00:03:55,250 --> 00:03:57,000
 have an effect on your brain chemistry as well,

63
00:03:57,000 --> 00:04:01,000
 a lot of rich food and fatty food and so on.

64
00:04:01,000 --> 00:04:08,200
 Obviously, you have negative effects from restraining in

65
00:04:08,200 --> 00:04:11,000
 terms of the weakness that comes from it.

66
00:04:11,000 --> 00:04:16,000
 Now, if you are living an ordinary life, an ordinary life,

67
00:04:16,000 --> 00:04:21,000
 if you are living a life in the human world, human society,

68
00:04:21,000 --> 00:04:25,840
 then you need to do work, you need to think, you need to

69
00:04:25,840 --> 00:04:27,000
 act, you need to speak.

70
00:04:27,000 --> 00:04:32,000
 So, not having enough food can be dangerous in that regard,

71
00:04:32,000 --> 00:04:39,630
 but in terms of our spirituality, it's really more, I would

72
00:04:39,630 --> 00:04:41,000
 say, the other way,

73
00:04:41,000 --> 00:04:46,000
 that indulging is actually a lot more dangerous.

74
00:04:46,000 --> 00:04:50,190
 It leads to lust, or it supports lust to get stronger in

75
00:04:50,190 --> 00:04:51,000
 the mind,

76
00:04:51,000 --> 00:04:55,000
 it supports laziness to get stronger in the mind.

77
00:04:55,000 --> 00:04:57,000
 It doesn't create these things in the mind,

78
00:04:57,000 --> 00:05:01,560
 but because of the brain chemistry and the processes in the

79
00:05:01,560 --> 00:05:02,000
 body,

80
00:05:02,000 --> 00:05:06,400
 and the blood flow and so on, required to digest food and

81
00:05:06,400 --> 00:05:10,000
 the chemicals that are added to the body,

82
00:05:10,000 --> 00:05:13,000
 these feelings become stronger.

83
00:05:13,000 --> 00:05:15,640
 This is why in the Buddha's time, everyone was starving

84
00:05:15,640 --> 00:05:16,000
 themselves,

85
00:05:16,000 --> 00:05:18,730
 because when they starved themselves, or all the spiritual

86
00:05:18,730 --> 00:05:19,000
 people,

87
00:05:19,000 --> 00:05:21,580
 because when they starved themselves, these feelings just

88
00:05:21,580 --> 00:05:22,000
 went away totally,

89
00:05:22,000 --> 00:05:25,100
 and they thought they were enlightened, wow, I don't get

90
00:05:25,100 --> 00:05:26,000
 greedy at all,

91
00:05:26,000 --> 00:05:30,790
 because there's no chemicals left to have lust arise, for

92
00:05:30,790 --> 00:05:34,000
 the hormones to work.

93
00:05:34,000 --> 00:05:36,000
 That's not the way out of suffering,

94
00:05:36,000 --> 00:05:40,510
 but it points very much to the problem with going the other

95
00:05:40,510 --> 00:05:41,000
 way,

96
00:05:41,000 --> 00:05:46,490
 is that you're developing an incredible amount of these

97
00:05:46,490 --> 00:05:47,000
 states.

98
00:05:47,000 --> 00:05:51,000
 Now, the other thing I wanted to say is,

99
00:05:51,000 --> 00:05:57,900
 and it relates to the necessity to eat food in life and

100
00:05:57,900 --> 00:05:59,000
 society,

101
00:05:59,000 --> 00:06:03,000
 is that the question is, what is the food for?

102
00:06:03,000 --> 00:06:09,020
 This is a question that especially monks and meditators are

103
00:06:09,020 --> 00:06:12,000
 not very clear on and often miss,

104
00:06:12,000 --> 00:06:19,000
 so people think they will see the effects of eating less

105
00:06:19,000 --> 00:06:20,000
 than they're used to

106
00:06:20,000 --> 00:06:23,000
 in terms of maybe their wounds don't heal as quickly,

107
00:06:23,000 --> 00:06:26,880
 maybe they don't have as much energy, they can see their

108
00:06:26,880 --> 00:06:28,000
 muscles atrophying,

109
00:06:28,000 --> 00:06:31,790
 their muscles getting smaller, maybe they're losing the

110
00:06:31,790 --> 00:06:32,000
 color,

111
00:06:32,000 --> 00:06:36,000
 and the beauty in their face is getting thin,

112
00:06:36,000 --> 00:06:39,000
 and they can see the bones sticking out of their shoulders.

113
00:06:39,000 --> 00:06:45,700
 A million other things are excuses that people come up with

114
00:06:45,700 --> 00:06:47,000
 to eat more.

115
00:06:47,000 --> 00:06:52,280
 This is what constantly I was getting in Los Angeles, for

116
00:06:52,280 --> 00:06:53,000
 example,

117
00:06:53,000 --> 00:06:55,750
 you're too thin, you have to eat more, you're not eating

118
00:06:55,750 --> 00:06:57,000
 enough, and so on.

119
00:06:57,000 --> 00:06:59,000
 The question is, enough for what?

120
00:06:59,000 --> 00:07:03,610
 Because the Buddha's idea of eating enough is enough to

121
00:07:03,610 --> 00:07:06,000
 live, enough to survive.

122
00:07:06,000 --> 00:07:09,270
 If the food that you're eating is not enough for you to

123
00:07:09,270 --> 00:07:10,000
 survive on

124
00:07:10,000 --> 00:07:15,000
 and not enough for you to carry out your duties correctly,

125
00:07:15,000 --> 00:07:16,000
 then it's not enough.

126
00:07:16,000 --> 00:07:18,490
 But if the food you eat is enough for you to carry out your

127
00:07:18,490 --> 00:07:19,000
 duties,

128
00:07:19,000 --> 00:07:22,970
 if with the food you eat you can walk back and forth five

129
00:07:22,970 --> 00:07:24,000
 hours a day

130
00:07:24,000 --> 00:07:27,330
 and do five hours of walking meditation, then I would say

131
00:07:27,330 --> 00:07:29,000
 that's enough food.

132
00:07:29,000 --> 00:07:31,400
 And that's really not a lot of food because you're walking

133
00:07:31,400 --> 00:07:33,000
 quite slowly.

134
00:07:33,000 --> 00:07:35,000
 So you do five hours of walking, five hours of sitting,

135
00:07:35,000 --> 00:07:38,570
 or six hours of walking, six hours of sitting, that's

136
00:07:38,570 --> 00:07:40,000
 enough to live.

137
00:07:40,000 --> 00:07:43,550
 Anything that you have to do apart from that, if you have

138
00:07:43,550 --> 00:07:44,000
 to teach,

139
00:07:44,000 --> 00:07:47,000
 if you have to work in the monastery, if you have to then

140
00:07:47,000 --> 00:07:48,000
 go out and work

141
00:07:48,000 --> 00:07:55,000
 in daily, in society, then you add on to that extent.

142
00:07:55,000 --> 00:08:00,000
 You know, can, with this food, can I live, can I survive?

143
00:08:00,000 --> 00:08:02,000
 Because that's really the point.

144
00:08:02,000 --> 00:08:09,280
 The monk and the monastic is the ideal or the perfect

145
00:08:09,280 --> 00:08:10,000
 example

146
00:08:10,000 --> 00:08:13,000
 of the bare minimum of requirements.

147
00:08:13,000 --> 00:08:16,000
 We have, we eat because we need the food.

148
00:08:16,000 --> 00:08:18,450
 We wear clothes because we need something to cover our

149
00:08:18,450 --> 00:08:19,000
 bodies.

150
00:08:19,000 --> 00:08:21,990
 We have shelter because we need something to protect

151
00:08:21,990 --> 00:08:23,000
 against the rain

152
00:08:23,000 --> 00:08:28,000
 or also to give us solitude and so on.

153
00:08:28,000 --> 00:08:34,160
 And we take medicine so that we don't suffer from horrible,

154
00:08:34,160 --> 00:08:35,000
 painful feelings

155
00:08:35,000 --> 00:08:39,000
 that are going to leave us or drive us crazy even,

156
00:08:39,000 --> 00:08:44,080
 that are going to hurt our meditation practice or disrupt

157
00:08:44,080 --> 00:08:47,000
 our meditation practice.

158
00:08:47,000 --> 00:08:52,990
 So, point being, as Palanjani also said, we need far less

159
00:08:52,990 --> 00:08:55,000
 than we think that we need.

160
00:08:55,000 --> 00:09:00,540
 And when you really think about what do I need this food

161
00:09:00,540 --> 00:09:01,000
 for,

162
00:09:01,000 --> 00:09:06,000
 you realize that you really need a lot less than you think.

163
00:09:06,000 --> 00:09:11,000
 And the point is that we, rather than using this as a guide

164
00:09:11,000 --> 00:09:11,000
,

165
00:09:11,000 --> 00:09:15,260
 we always will use things like hunger as a guide or partial

166
00:09:15,260 --> 00:09:17,000
ity as a guide.

167
00:09:17,000 --> 00:09:25,930
 I know I only need maybe some crackers and cheese and that

168
00:09:25,930 --> 00:09:27,000
's enough,

169
00:09:27,000 --> 00:09:32,000
 but chips are better, so I'll have chips instead.

170
00:09:32,000 --> 00:09:36,000
 Or I know that I've had enough food, but I'm still hungry,

171
00:09:36,000 --> 00:09:41,160
 so I'll go out and eat more and we'll eat until we're not

172
00:09:41,160 --> 00:09:42,000
 hungry.

173
00:09:42,000 --> 00:09:44,000
 This is our measure.

174
00:09:44,000 --> 00:09:48,770
 I had a friend when I was younger and he became a biologist

175
00:09:48,770 --> 00:09:49,000
,

176
00:09:49,000 --> 00:09:51,000
 became a veterinarian actually,

177
00:09:51,000 --> 00:09:55,000
 and he once told me that the way this gland works in the

178
00:09:55,000 --> 00:09:55,000
 brain,

179
00:09:55,000 --> 00:09:58,000
 the hunger gland works, it's always firing.

180
00:09:58,000 --> 00:10:02,020
 It's nature is to fire and say, "I'm hungry, I'm hungry, I

181
00:10:02,020 --> 00:10:03,000
'm hungry, I'm hungry," constantly.

182
00:10:03,000 --> 00:10:06,640
 That's its ordinary state and it's only by putting a whole

183
00:10:06,640 --> 00:10:09,000
 bunch of food to make it shut up.

184
00:10:09,000 --> 00:10:15,000
 Somehow it gets something and then it switches off.

185
00:10:15,000 --> 00:10:18,170
 And then when the food disappears, it turns back on and

186
00:10:18,170 --> 00:10:19,000
 starts saying,

187
00:10:19,000 --> 00:10:22,000
 "I'm hungry, I'm hungry, I'm hungry."

188
00:10:22,000 --> 00:10:24,540
 It doesn't necessarily have anything to do with your level

189
00:10:24,540 --> 00:10:25,000
 of health

190
00:10:25,000 --> 00:10:27,890
 or your physical requirements or even how much food you

191
00:10:27,890 --> 00:10:29,000
 have in your stomach.

192
00:10:29,000 --> 00:10:32,000
 You can have enough food in your stomach to survive,

193
00:10:32,000 --> 00:10:34,430
 and yet it hasn't hit this gland yet, and so it's still

194
00:10:34,430 --> 00:10:38,000
 saying, "Eat more, eat more, eat more. I'm hungry."

195
00:10:38,000 --> 00:10:43,050
 Something we don't realize, hunger is not an indication

196
00:10:43,050 --> 00:10:45,000
 that we have to eat more.

197
00:10:45,000 --> 00:10:48,400
 Sorry, it's not a perfect indication that we have to eat

198
00:10:48,400 --> 00:10:49,000
 more.

199
00:10:49,000 --> 00:10:52,000
 It's also something that has evolved biologically.

200
00:10:52,000 --> 00:10:55,340
 It's not a spiritual gland. This gland is not our spiritual

201
00:10:55,340 --> 00:10:56,000
 friend.

202
00:10:56,000 --> 00:11:01,870
 If anything, it's a product produced by Mara in the

203
00:11:01,870 --> 00:11:04,000
 factories of Mara

204
00:11:04,000 --> 00:11:08,130
 because it tends to tell us we have to eat more and more

205
00:11:08,130 --> 00:11:09,000
 and get fat and fat

206
00:11:09,000 --> 00:11:12,390
 and save up for the winter or save up for the times when we

207
00:11:12,390 --> 00:11:13,000
 have no food.

208
00:11:13,000 --> 00:11:17,000
 How evolution has led us to have this gland.

209
00:11:17,000 --> 00:11:21,000
 It has nothing to do with any sort of spiritual development

210
00:11:21,000 --> 00:11:21,000
.

211
00:11:21,000 --> 00:11:26,220
 A big point in learning how to find the middle way in

212
00:11:26,220 --> 00:11:27,000
 regards,

213
00:11:27,000 --> 00:11:33,090
 as the example you give food, is to stop listening to

214
00:11:33,090 --> 00:11:34,000
 hunger

215
00:11:34,000 --> 00:11:38,000
 and just be rational and understand how much food you need.

216
00:11:38,000 --> 00:11:42,600
 Okay, yeah, sure, when you're starving, have some food, but

217
00:11:42,600 --> 00:11:46,000
 don't use the hunger as a guide.

218
00:11:46,000 --> 00:11:51,000
 This is apparent when you come to practice meditation

219
00:11:51,000 --> 00:11:51,000
 because you find you've eaten enough,

220
00:11:51,000 --> 00:11:52,790
 you don't need more, and yet in the evening you're still

221
00:11:52,790 --> 00:11:55,000
 going, "I'm hungry, I'm hungry, I'm hungry."

222
00:11:55,000 --> 00:11:59,330
 I've had meditators tell me, "In the evening I was so

223
00:11:59,330 --> 00:12:00,000
 hungry,"

224
00:12:00,000 --> 00:12:02,390
 they'll complain about it the first days and you tell them,

225
00:12:02,390 --> 00:12:03,000
 "Say hungry, hungry."

226
00:12:03,000 --> 00:12:06,000
 And they're, "Come on, I'm hungry, I need more food."

227
00:12:06,000 --> 00:12:09,210
 And then they'll finally try it and they'll come back and

228
00:12:09,210 --> 00:12:11,000
 tell you that it's amazing.

229
00:12:11,000 --> 00:12:14,560
 "Last night I was so hungry, I thought I need some food, I

230
00:12:14,560 --> 00:12:16,000
'm going to die here, I can't stay here another day."

231
00:12:16,000 --> 00:12:20,000
 And so, but finally I said to myself, "Hungry, hungry."

232
00:12:20,000 --> 00:12:23,000
 "Woke up in the morning, I wasn't hungry at all."

233
00:12:23,000 --> 00:12:25,050
 And they're able to see that actually it's just their body

234
00:12:25,050 --> 00:12:28,000
 playing games with them because they're used to eating.

235
00:12:28,000 --> 00:12:33,220
 And because they're used to eating, the mind says, "It's

236
00:12:33,220 --> 00:12:36,000
 time for dinner. Where's my dinner?"

237
00:12:36,000 --> 00:12:41,000
 And the gland starts working.

