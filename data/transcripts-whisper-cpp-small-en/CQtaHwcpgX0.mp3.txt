 Hello, welcome back to Ask a Monk. Now I have two questions
 that were asked together.
 The first question is whether after realizing nirvana, nir
vana, which means freedom or release or emancipation or
 however you want to translate it,
 is there anything left to do or is nirvana it? I mean, is
 that all you have to do? That's the first question.
 So I'll answer this question first. The answer is easy. Nir
vana is the last thing that we ever have to do.
 Once you realize nirvana, there's nothing left that from a
 Buddhist point of view you need to do.
 The point is at that point you're free from suffering. So
 what does this mean?
 Nirvana is the cessation of suffering. This is what the
 definition of it is. It's possible for a person to, not
 easy, but it's possible for a human being to realize
 freedom from suffering,
 to come to be, even temporarily, to be free from the
 arising of stress, the arising of any impermanent
 phenomenon, that at that point the mind is perfectly free.
 It's possible to enter or to touch that state, to
 experience or to see or to realize that state without being
 free from suffering, without being that way forever.
 This is the first thing to point out. And this person we
 have names for them, the sotapan, someone who has entered
 the stream.
 Because though they still have to suffer and they still
 have more to do, they've already cracked the shell, so to
 speak, and it's only a matter of time before they become
 totally free from suffering.
 So this is one understanding of nirvana. It's an experience
 that someone has.
 And at that point, one's life starts to go in the direction
 towards freedom from suffering.
 But what we mean by nirvana or parinirvana or so on is the
 complete freedom from suffering.
 So though a person might still live, they have no more
 greed, no more anger, no more delusion. These things can't
 arise.
 There's perfect understanding of reality, of the
 experiences around oneself.
 When you see, hear, smell, taste, feel, think, there's only
 the awareness of the object. There's no attachment to it.
 So we say this is nirvana. This comes from realizing this
 state of ultimate freedom.
 When you realize it again and again, you start to see that
 there's nothing that lasts, that all of the experiences
 that we have simply arise and cease.
 And there's nothing lasting, there's nothing permanent,
 there's nothing stable. And so one loses all of one's
 attachment.
 At that point, one isn't subject to future rebirth. There's
 this physical reality that we've somehow built up that has
 to last out its lifetime.
 And at the end of that cycle, that revolution, at death
 there's nothing. So at death there's no more arising.
 And at that point there's freedom from suffering, freedom
 from impermanence, from birth, from having to come back
 again.
 So I think this is clear. The answer from a Buddhist point
 of view, doctrinal point of view, is quite clear.
 After that there's nothing left to do. The Buddha said, "K
attan karuniyam." "Done is what needs to be done."
 There's nothing left to do. There's nothing more here.
 But I think an important point to make that is often missed
 is that you don't somehow fall into this state.
 This is the ultimate, the samam bonam, the final
 emancipation. It comes to someone who has had enough, who
 doesn't see any benefit in coming back.
 It doesn't see any benefit in recreating this human state
 or any state, again and again and again.
 Most of us are not there. Most of us think that there's
 still something to be done. We want this, we want that.
 We've been taught that it's good to have a partner, to have
 a family, to have a job, to help people and so on.
 And we think that there's somehow some benefit in this.
 On the other hand, we see that in ourselves there are
 certain attachments that we have that are causing our
 suffering.
 So this is where we're at. We're at the point where we want
 to hold on to certain things, but we want to let go of
 other certain things.
 So this just shows that we're somewhere along the path, we
're somewhere between a useless person and an enlightened
 person.
 And it's up to us which way we're going to go.
 If we take the side of letting go, we don't have to let go
 of those things we don't want to let go of.
 We let go of those things that we want to let go of. And
 this is how the path works.
 People who are afraid of things like nirvana, afraid of
 letting go, and afraid of practicing meditation, because
 they think if I practice meditation,
 I'm going to let go of all these things I love, and I love
 them, and I want those things, I don't want to let go of
 them.
 And the truth is, if you want it, if it's something you're
 holding on to, you're never going to let it go, that's the
 point.
 But what you have to realize is that the more you
 understand, the more mature you become, the more you
 realize that you're not benefiting anyone or anything by
 yourself or others,
 by holding on to anything, by clinging to others, by
 wanting things to be a certain way.
 It's not of a benefit to you, and it's not of a benefit to
 other people. That true happiness doesn't come from these
 things.
 People, when they hear about not coming back, they think, "
Well, the great thing about Buddhism is that there is coming
 back."
 I remember talking with a Catholic woman, and she said, "Oh
, it's so great to hear about how in Buddhism you've got a
 chance to come back."
 Because, of course, in the religion she had been brought up
 with, that's it. When you die, it's either heaven or hell
 forever.
 But she was thinking, "Wow, you know, to be able to come
 back and have another chance."
 So for Westerners, I think this is common. We think, "Oh,
 it's great. I'll be able to come back and try again like
 that movie Groundhog Day, until you get it right."
 And that's really the point, is until you get it right, you
're going to keep coming back.
 But whether this should be looked at, seen as a good thing
 or not, is debatable.
 I mean, ask yourself, don't ask yourself, "How are you
 feeling now?"
 Because most people think, "Well, I'm okay, and I've got
 plans. If I can just this and this and this, there's a
 chance that I'll be stable and happy and living a good life
."
 But then, don't look at that. Ask yourself what it's taken
 to get even to the point that you're at now.
 And think of all the lessons you've had to learn.
 And if it's true that you come back again and again and
 again, then you're going to have to learn all of those.
 If you don't learn anything now and change anything about
 this state, if you don't somehow increase your level of
 maturity in a way that you've never done before in all your
 lives,
 then you're just going to have to learn these lessons again
 and go through all of the suffering that we had to go
 through as children, as teens, as young adults, as adults,
 and so on.
 And wait until you get old, sick, and die. I mean, see how
 that's going to be.
 And then, don't stop there. Look at the people around you
 and in the world around us.
 Who's to say, "In next life, you won't be one of the people
 who suffer terribly in this life?"
 So, it's not as simple a thing as some people might think.
 They think, "Oh, great. I'll come back. I'll get to do this
 again, and I'll do this differently, and I'll learn more."
 So, no. It's not like that. If you're not careful, some lif
etimes down the road, it's very possible that you'll end up
 in a terrible situation,
 a situation of intense suffering. Why do people suffer in
 such horrible ways that they do? And who's to say, "You won
't end up like them?"
 So, there is some, we can see some benefit in at least
 bringing ourselves somewhat out of this state,
 to a state where we don't lose all of our memories every 50
 to 100 years, where we're able to keep them and to continue
 developing.
 That's, at the very least, a good thing to do. And so, the
 development of the mind shouldn't scare people.
 The practice of Buddhism shouldn't be a scary thing that
 you're going to let go of things you hold on to.
 Buddhism is about learning. It's about coming to grow, is
 growing up and coming to learn things and understand more
 about reality.
 It's not to brainwash us or to force some kind of detached
 state. It's to help us to learn what's causing us suffering
.
 Because all of us can tell if we're not terribly blind, we
 can verify that there is a lot of suffering in this world.
 And the truth of the Buddha's teaching is that this
 suffering is unnecessary. We don't have to suffer in this
 way.
 The reason we do is because we don't understand, and
 because we don't understand, we therefore do things that
 cause us suffering and that cause other people suffering.
 So, slowly we learn how to overcome those things, how to be
 free from them.
 And eventually we learn how to be free from all suffering,
 how to never cause suffering for others or for ourselves,
 how to act in such a way that is not contradictory to our
 wishes, where we wish to be happy, we wish to live in peace
,
 and yet we find ourselves acting and speaking and thinking
 in ways that cause us suffering and cause disharmony in the
 world around us.
 So this is the, I think, sort of a detailed answer of why
 freedom would be the last thing.
 Because once you have no clinging, once you have no
 attachment and no delusion, you will not cause any
 suffering for yourself.
 And as a result, you will not have to come back and there's
 no more creating, no more forming, no more building.
 You realize that there's nothing worth clinging to, that
 true happiness doesn't come from the objects of the sense
 or any of the objects that are inside of us or in the world
 around us.
 True happiness comes from freedom and you come to see that
 this freedom is the ultimate happiness.
 So, that's the answer to that question.
 The second question, I'm going to have to deal with it here
 so this video is going to be a little bit longer.
 The second question is whether karma has physical results
 only or affects the mind as well.
 And, you know, these questions are actually fairly easy
 because they're just simple doctrinal questions.
 But they're interesting as well.
 Karma, first we have to understand that karma is not a
 thing.
 It doesn't exist in the world. You can't show me karma.
 Karma means action.
 So, technically speaking, you can show me an action when,
 you know, show me the karma of killing.
 So, you can kill something. There's karma for you.
 But the problem is that we come to take karma as this
 substance that exists, like something you're carrying
 around like a weight over your shoulder or that's somehow
 tattooed on your skin or something like that.
 That you carry around and you're just waiting for it to
 drop on your head or something.
 And that's not the case. Karma is a law.
 It's a law that says that there is an effect to our actions
.
 And not just our actions, actually to our intentions, to
 our state of mind.
 That the mind is able to affect the world around us.
 It's not a doctrine of determinism.
 You know, if it were this substance or this thing that you
 could measure, then there might be some determinism.
 But it doesn't say that. Karma just says that it doesn't
 say either way that there is a free will or determinism.
 That's not the point of karma. Karma says that when there
 is action, when you do kill or steal or lie.
 When in your mind you develop these unwholesome tendencies
 to these ideas for the creation of disharmony and suffering
 and stress.
 Then there isn't the effect. The effect is that stress is
 created.
 When you have the intention to help people as well. There's
 another sort of stress created.
 It's not an unpleasant stress, but it's a stress of sorts
 that creates pleasure.
 It's a vibration. It's like a vibration when you want to
 help people.
 When you have the intention to do good things. To give, to
 help, to support, to teach, to even to just be kind.
 When you study, when you learn, when you practice
 meditation, all of these things have an effect.
 They change who we are. It's important to understand that
 aspect of karma.
 The short answer to your question then is that karma, our
 actions will affect both the body and the mind.
 Technically speaking, karma gives rise to experiences.
 It gives rise to seeing, hearing, smelling, tasting,
 feeling, thinking.
 This is the technical explanation that it's going to give
 rise to sensual experience in the future.
 If you do something and you intend to do something, that's
 going to change the experiences that you have.
 You're not going to see the same things as if you hadn't
 done that.
 If you kill someone, they're going to see the inside of a
 jail and so on.
 Hearing, smelling, tasting, feeling, thinking.
 From a technical point of view, that's what happens.
 There are different experiences that you have.
 Those experiences are both physical and mental.
 All it means is that we are affecting our destiny.
 Whether this is based on free will or determinism isn't
 really the question.
 The point is that when we do do something, there is a
 result.
 So rather than worrying about such questions as whether it
's free will or determinism
 or what does karma affect, is it a physical or a mental
 thing, we should train ourselves
 to give up the unwholesome tendencies that cause us
 suffering.
 In the end, the interesting thing is that you give up both
 kinds of karma
 because you have no intention to create happiness either,
 to create happiness in the sense of happy experiences.
 You come to see that even pleasurable experiences are
 temporary.
 Even if you create harmony in the world, even if I were
 able to, with my great karma,
 create peace and harmony in the world, with through some
 great act, a minor series of acts, it's temporary.
 And unless there's wisdom and understanding that allows
 people to let go,
 they're just going to ruin it when I'm gone.
 For instance, the Buddha's teaching, when the Buddha was
 around, there was a lot of good in India.
 It lasted for some time and then India went back and now it
's an ordinary country again.
 But for some time in India, it was a special place.
 It was very Buddhist and there was very little killing and
 so on, from what we understand.
 Just an example, but the point is that it's impermanent.
 And no matter what good things you do, even the Buddha's
 teaching is impermanent.
 It's not going to last forever.
 And this is the teaching of a perfectly enlightened Buddha,
 as we understand.
 So, most important is to become free and to become free
 from karma as well,
 and to not have any attachments that things should turn out
 in a certain way.
 Not to be expecting or have expectations about the future,
 to simply be content and in tune with reality, to see
 reality for what it is,
 and to be at peace with that, and to not cling and to not
 want and to not hope and care and worry and so on.
 But to live one's life in a dynamic way where you can
 accept and react and respond appropriately to every
 situation.
 It doesn't mean that you live in one place and do nothing,
 sit around and do nothing.
 It means that you are able to live dynamically.
 You're not a stick in the mud.
 And when it's time to move, can't move.
 Your mind is able to respond appropriately to all
 experiences and react without attachment.
 So, fairly detailed answers to your questions.
 I hope they did hit the mark, at least to some extent.
 So, thanks for the questions. All the best.
