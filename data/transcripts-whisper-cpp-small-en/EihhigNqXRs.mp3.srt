1
00:00:00,000 --> 00:00:06,600
 Hi, so next question comes from Stilson.

2
00:00:06,600 --> 00:00:09,000
 Does contemplation of the universe and the strong

3
00:00:09,000 --> 00:00:11,220
 possibility of alien life relate to

4
00:00:11,220 --> 00:00:13,840
 awareness of mind and meditation?

5
00:00:13,840 --> 00:00:16,600
 I have often had a clear thought when I realized that the

6
00:00:16,600 --> 00:00:18,720
 events of Earth are irrelevant compared

7
00:00:18,720 --> 00:00:23,120
 to the scale of the universe.

8
00:00:23,120 --> 00:00:30,900
 In short, no, and to explain, there are lots of things that

9
00:00:30,900 --> 00:00:35,880
 can help your meditation practice.

10
00:00:35,880 --> 00:00:40,670
 For instance, as a completely unrelated example, for people

11
00:00:40,670 --> 00:00:42,760
 who are caught up in lust, it's

12
00:00:42,760 --> 00:00:45,990
 good for them to contemplate the loathsome side of the body

13
00:00:45,990 --> 00:00:47,760
, the negative, the fact that

14
00:00:47,760 --> 00:00:53,000
 the body smells, the body becomes greasy and so on, and to

15
00:00:53,000 --> 00:00:54,720
 contemplate the various parts

16
00:00:54,720 --> 00:00:59,960
 of the body, to see that what we're attracted to is

17
00:00:59,960 --> 00:01:03,900
 actually just a physical process.

18
00:01:03,900 --> 00:01:09,510
 For people who are very angry as a rule, it's good for them

19
00:01:09,510 --> 00:01:12,440
 to think of other beings with

20
00:01:12,440 --> 00:01:16,090
 love and to project loving kindness towards all beings,

21
00:01:16,090 --> 00:01:18,200
 especially towards the people that

22
00:01:18,200 --> 00:01:19,920
 they have problems with.

23
00:01:19,920 --> 00:01:24,540
 These are generally useful meditation practices which can

24
00:01:24,540 --> 00:01:27,200
 aid in the meditation practice on

25
00:01:27,200 --> 00:01:30,240
 a preliminary level, but they don't have to do with insight

26
00:01:30,240 --> 00:01:30,500
.

27
00:01:30,500 --> 00:01:33,720
 They don't bring about real insight, and neither does

28
00:01:33,720 --> 00:01:36,640
 something like realizing the insignificance

29
00:01:36,640 --> 00:01:39,000
 of your actions and so on and so on.

30
00:01:39,000 --> 00:01:42,480
 They have a benefit, but they aren't a replacement for

31
00:01:42,480 --> 00:01:44,280
 meditation themselves.

32
00:01:44,280 --> 00:01:49,720
 They don't play an integral part in the meditation.

33
00:01:49,720 --> 00:01:55,770
 There's innumerable things you could do that mental

34
00:01:55,770 --> 00:02:01,560
 exercises you could undertake that would

35
00:02:01,560 --> 00:02:04,640
 support your meditation because of their very nature in

36
00:02:04,640 --> 00:02:06,660
 terms of reducing the ego, in terms

37
00:02:06,660 --> 00:02:11,360
 of putting things in perspective and so on.

38
00:02:11,360 --> 00:02:14,280
 They're all useful and they're all recommendable, but it's

39
00:02:14,280 --> 00:02:15,920
 important not to mix them up and

40
00:02:15,920 --> 00:02:18,710
 to think, "Well, that's meditation, and now you're on the

41
00:02:18,710 --> 00:02:21,280
 right path and that's enough."

42
00:02:21,280 --> 00:02:23,320
 Meditation deals with the next level.

43
00:02:23,320 --> 00:02:26,550
 It deals with things on a phenomenological level where you

44
00:02:26,550 --> 00:02:28,120
're dealing with experience

45
00:02:28,120 --> 00:02:31,010
 as it comes and goes, and that can only occur inside

46
00:02:31,010 --> 00:02:31,840
 yourself.

47
00:02:31,840 --> 00:02:34,240
 It can only occur wherever the mind is.

48
00:02:34,240 --> 00:02:37,170
 When the mind is focused at the eye, wisdom has to arise at

49
00:02:37,170 --> 00:02:38,560
 the eye, when it's at the

50
00:02:38,560 --> 00:02:41,060
 ear, when it's at the nose.

51
00:02:41,060 --> 00:02:44,810
 Whatever sense that you're aware of at that moment, that's

52
00:02:44,810 --> 00:02:46,400
 where wisdom arises.

53
00:02:46,400 --> 00:02:49,640
 At the moment where you're contemplating this, you've got

54
00:02:49,640 --> 00:02:51,520
 one level of wisdom, something

55
00:02:51,520 --> 00:02:53,440
 that seems to you to be wise.

56
00:02:53,440 --> 00:02:56,680
 It certainly is a wise thought, but meditation goes to the

57
00:02:56,680 --> 00:02:58,360
 next step and has you examine

58
00:02:58,360 --> 00:03:01,120
 that thought itself and say, "Hmm, what's involved with

59
00:03:01,120 --> 00:03:01,880
 this thought?"

60
00:03:01,880 --> 00:03:04,960
 You come to see that even in that thought, there's quite

61
00:03:04,960 --> 00:03:06,560
 likely a sort of attachment

62
00:03:06,560 --> 00:03:09,120
 where you think, "Oh, that's good," and you're very proud

63
00:03:09,120 --> 00:03:10,640
 of yourself and you feel very good

64
00:03:10,640 --> 00:03:14,670
 about it and it makes you feel happy, it makes you feel at

65
00:03:14,670 --> 00:03:15,380
 peace.

66
00:03:15,380 --> 00:03:17,690
 Meditation is examining these things as well and seeing, "

67
00:03:17,690 --> 00:03:18,880
Oh, I'm attached to this.

68
00:03:18,880 --> 00:03:23,550
 Oh, I have ... This is making me think about that," and so

69
00:03:23,550 --> 00:03:25,720
 on and so on, and to break those

70
00:03:25,720 --> 00:03:29,440
 apart and to come to let go of even that attachment.

71
00:03:29,440 --> 00:03:32,590
 The Buddha said, "We should not hold on to good things,

72
00:03:32,590 --> 00:03:34,760
 good mind stains, good thoughts,

73
00:03:34,760 --> 00:03:40,040
 good ideas, much more so than we should not hold on to bad

74
00:03:40,040 --> 00:03:41,280
 things."

75
00:03:41,280 --> 00:03:44,260
 He said, "Even the good ones, we shouldn't hold on to, so

76
00:03:44,260 --> 00:03:45,640
 how much more should we let

77
00:03:45,640 --> 00:03:48,760
 go of bad things?"

78
00:03:48,760 --> 00:03:53,920
 The point is that we're expected or our practice is to not

79
00:03:53,920 --> 00:03:56,440
 hold on to anything, even a good

80
00:03:56,440 --> 00:04:01,340
 idea because it had its benefit and it served its purpose

81
00:04:01,340 --> 00:04:03,200
 and now it's gone.

82
00:04:03,200 --> 00:04:07,280
 Even those ideas, they can be beneficial, but no, they're

83
00:04:07,280 --> 00:04:09,000
 not a part of meditation,

84
00:04:09,000 --> 00:04:11,660
 per se.

85
00:04:11,660 --> 00:04:15,110
 It's important to keep a clear line between what is

86
00:04:15,110 --> 00:04:18,120
 meditation practice, what is empirical

87
00:04:18,120 --> 00:04:19,120
 understanding.

88
00:04:19,120 --> 00:04:22,100
 I guess this is another way of explaining it, that that's

89
00:04:22,100 --> 00:04:23,120
 not empirical.

90
00:04:23,120 --> 00:04:25,690
 It's a theory that you have, the idea that ... Or it's

91
00:04:25,690 --> 00:04:27,120
 equivalent to a theory where you

92
00:04:27,120 --> 00:04:31,040
 say, "There's alien life and well, theoretically that means

93
00:04:31,040 --> 00:04:32,920
 that I'm insignificant," and so

94
00:04:32,920 --> 00:04:35,920
 on, but it's not the same as actually examining reality and

95
00:04:35,920 --> 00:04:37,320
 seeing ... It's not that you're

96
00:04:37,320 --> 00:04:39,880
 insignificant, it's that you don't exist.

97
00:04:39,880 --> 00:04:43,080
 The "you," the "I," doesn't exist and that's what comes

98
00:04:43,080 --> 00:04:45,320
 from direct meditation practice.

99
00:04:45,320 --> 00:04:47,800
 Okay, hope that helps to clear things up.

100
00:04:47,800 --> 00:04:48,600
 Thanks for the question.

