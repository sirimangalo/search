1
00:00:00,000 --> 00:00:16,750
 This morning I continued talking about the things which

2
00:00:16,750 --> 00:00:20,320
 lead to success and prosperity

3
00:00:20,320 --> 00:00:21,320
 in ultimate sense.

4
00:00:21,320 --> 00:00:28,960
 We call them the highest blessings.

5
00:00:28,960 --> 00:00:48,280
 Today we continue on with kara wo, translated as respect or

6
00:00:48,280 --> 00:00:51,840
 homage.

7
00:00:51,840 --> 00:01:01,400
 Today we talk about respect for the practice of apamada.

8
00:01:01,400 --> 00:01:05,240
 Apamada is a difficult word to translate to English,

9
00:01:05,240 --> 00:01:08,760
 although the meaning is clear.

10
00:01:08,760 --> 00:01:14,760
 Apamada means negligence.

11
00:01:14,760 --> 00:01:25,560
 Apamada means the opposite of negligence or non-negligence.

12
00:01:25,560 --> 00:01:33,760
 We translate it as vigilance or diligence or heedfulness.

13
00:01:33,760 --> 00:01:46,760
 The meaning of apamada is that we should be mindful at all

14
00:01:46,760 --> 00:01:49,080
 times.

15
00:01:49,080 --> 00:01:56,960
 Not only be mindful at all times, altogether four things.

16
00:01:56,960 --> 00:02:06,640
 We should be free from anger or ill will towards other

17
00:02:06,640 --> 00:02:08,640
 beings.

18
00:02:08,640 --> 00:02:12,120
 We should be always mindful.

19
00:02:12,120 --> 00:02:16,680
 We should be equanimous or level-headed.

20
00:02:16,680 --> 00:02:26,150
 Our mind should be unmoved by good things, things which

21
00:02:26,150 --> 00:02:27,840
 arise.

22
00:02:27,840 --> 00:02:36,190
 We should stay equanimous in the face of whatever might

23
00:02:36,190 --> 00:02:37,720
 arise.

24
00:02:37,720 --> 00:02:41,360
 Composed and our mind is composed of whatever arises.

25
00:02:41,360 --> 00:02:47,870
 When good things arise, we might instead get greedy or

26
00:02:47,870 --> 00:02:50,360
 attached to them.

27
00:02:50,360 --> 00:02:54,080
 Instead we set ourselves on being mindful.

28
00:02:54,080 --> 00:03:02,880
 Don't let greed come and take over our mind.

29
00:03:02,880 --> 00:03:09,660
 When anger might arise, we may see or hear or smell how bad

30
00:03:09,660 --> 00:03:11,720
 things arise.

31
00:03:11,720 --> 00:03:16,840
 Instead of letting anger arise, we may be mindful.

32
00:03:16,840 --> 00:03:21,320
 It needs to be internally composed or level-headed,

33
00:03:21,320 --> 00:03:29,520
 balanced in the face of the thing of dust.

34
00:03:29,520 --> 00:03:40,120
 We need to train ourselves to become free from craving.

35
00:03:40,120 --> 00:03:50,200
 This is what the Lord Buddha called apamada or vigilance or

36
00:03:50,200 --> 00:03:53,160
 heedfulness.

37
00:03:53,160 --> 00:03:57,580
 So respect for this means that we should practice

38
00:03:57,580 --> 00:04:05,840
 respecting Lord Buddha's teaching on apamada.

39
00:04:05,840 --> 00:04:09,440
 In fact the last words of the Lord Buddha before he passed

40
00:04:09,440 --> 00:04:10,720
 away.

41
00:04:10,720 --> 00:04:21,720
 Vayadhamma sankara apamadina sampadita.

42
00:04:21,720 --> 00:04:33,320
 Everything which arises is of the nature to pass away.

43
00:04:33,320 --> 00:04:38,840
 For this reason we should strive on, work hard to fulfill

44
00:04:38,840 --> 00:04:41,640
 the teaching of apamada, fulfill

45
00:04:41,640 --> 00:04:54,280
 the teaching of vigilance.

46
00:04:54,280 --> 00:04:59,140
 So when we practice meditation we should not think that

47
00:04:59,140 --> 00:05:04,960
 there is also some occasion for

48
00:05:04,960 --> 00:05:12,080
 taking a vacation from our practice.

49
00:05:12,080 --> 00:05:17,460
 Maybe in the morning we practice, but in the afternoon we

50
00:05:17,460 --> 00:05:20,240
 let our minds wander and fall

51
00:05:20,240 --> 00:05:24,200
 into greed or fall into anger.

52
00:05:24,200 --> 00:05:32,090
 Let's work hard to cut off the wandering mind, cut off the

53
00:05:32,090 --> 00:05:36,680
 mind which attaches to external

54
00:05:36,680 --> 00:05:39,800
 objects or internal objects.

55
00:05:39,800 --> 00:05:46,680
 It was like a simile yesterday of the man carrying a pot of

56
00:05:46,680 --> 00:05:47,640
 oil.

57
00:05:47,640 --> 00:05:54,790
 I've already talked more about apamada, so today I just

58
00:05:54,790 --> 00:05:58,640
 talked briefly about it in this

59
00:05:58,640 --> 00:05:59,640
 one.

60
00:05:59,640 --> 00:06:07,800
 But to give another story, we'll talk about apamada, angul

61
00:06:07,800 --> 00:06:10,760
imal, angulimal.

62
00:06:10,760 --> 00:06:16,310
 The most negligent person, the most powerful person that

63
00:06:16,310 --> 00:06:19,240
 existed in the time of the Lord

64
00:06:19,240 --> 00:06:20,240
 Buddha.

65
00:06:20,240 --> 00:06:22,160
 He killed all together 999 people.

66
00:06:22,160 --> 00:06:30,840
 His plan was to kill 1,000 people.

67
00:06:30,840 --> 00:06:36,570
 His plan was to kill his mother who would be the 1000th

68
00:06:36,570 --> 00:06:37,640
 victim.

69
00:06:37,640 --> 00:06:40,910
 The Lord Buddha knew that this was going to happen and of

70
00:06:40,910 --> 00:06:42,840
 course if he killed his mother

71
00:06:42,840 --> 00:06:49,210
 he would never chance to become free from suffering in this

72
00:06:49,210 --> 00:06:50,160
 life.

73
00:06:50,160 --> 00:06:57,640
 So the Lord Buddha went to visit Angulimala and so many

74
00:06:57,640 --> 00:07:00,720
 people protested.

75
00:07:00,720 --> 00:07:12,710
 Saying that if he went to Angulimala he would instead be

76
00:07:12,710 --> 00:07:15,920
 the victim.

77
00:07:15,920 --> 00:07:20,930
 In the end Angulimala became a student of the Lord Buddha,

78
00:07:20,930 --> 00:07:23,280
 became a monk, cut off his

79
00:07:23,280 --> 00:07:30,080
 hair and beard, put on the orange robes.

80
00:07:30,080 --> 00:07:31,080
 In the end it became an arahant.

81
00:07:31,080 --> 00:07:37,810
 The Lord Buddha said, "Yopupay pamachitvapatason pumachiti,

82
00:07:37,810 --> 00:07:41,960
 somanglokangvapasiti, abhamutova

83
00:07:41,960 --> 00:07:43,960
 sandhima."

84
00:07:43,960 --> 00:07:51,980
 It means though in the past someone was negligent, later on

85
00:07:51,980 --> 00:07:56,400
 they're no longer negligent.

86
00:07:56,400 --> 00:08:00,720
 And later on they become someone who is not negligent.

87
00:08:00,720 --> 00:08:04,160
 They light up the whole world just like the moon which

88
00:08:04,160 --> 00:08:06,400
 lights up the world when it comes

89
00:08:06,400 --> 00:08:11,120
 up from behind a cloud.

90
00:08:11,120 --> 00:08:21,960
 This is the teaching on Appamanda.

91
00:08:21,960 --> 00:08:23,280
 Means we should always be mindful.

92
00:08:23,280 --> 00:08:28,040
 We should not be negligent in our practice or negligent in

93
00:08:28,040 --> 00:08:30,480
 our training of our minds.

94
00:08:30,480 --> 00:08:37,770
 We should work very hard to fight against our natural

95
00:08:37,770 --> 00:08:41,600
 tendencies to greed or to anger.

96
00:08:41,600 --> 00:08:48,860
 That we can develop the tendency to not be greedy, to not

97
00:08:48,860 --> 00:08:50,480
 be angry.

98
00:08:50,480 --> 00:08:58,290
 The last one in the series on respect is respect for

99
00:08:58,290 --> 00:09:00,960
 hospitality.

100
00:09:00,960 --> 00:09:09,440
 That we should receive guests.

101
00:09:09,440 --> 00:09:17,120
 We should be able to receive other people.

102
00:09:17,120 --> 00:09:20,960
 Now receiving other people is in two ways.

103
00:09:20,960 --> 00:09:25,420
 One we should be ready to receive other people in terms of

104
00:09:25,420 --> 00:09:27,880
 giving them the jects that they

105
00:09:27,880 --> 00:09:28,880
 might need.

106
00:09:28,880 --> 00:09:37,720
 Food and shelter and clothing and medicine.

107
00:09:37,720 --> 00:09:43,250
 We should also receive hospitality in terms of giving

108
00:09:43,250 --> 00:09:46,920
 guests or visitors giving them the

109
00:09:46,920 --> 00:09:51,280
 dhamma.

110
00:09:51,280 --> 00:09:55,080
 We should always remember that whenever people come to

111
00:09:55,080 --> 00:09:55,960
 visit us.

112
00:09:55,960 --> 00:09:57,680
 What we can give to them is two things.

113
00:09:57,680 --> 00:10:03,080
 We can give them items that they might need and we can give

114
00:10:03,080 --> 00:10:05,000
 them the dhamma.

115
00:10:05,000 --> 00:10:11,890
 Giving them the dhamma doesn't mean we should sit around

116
00:10:11,890 --> 00:10:14,840
 and talk with them or try to teach

117
00:10:14,840 --> 00:10:20,680
 them this or teach them that.

118
00:10:20,680 --> 00:10:23,680
 Means we have to encourage them to go and practice.

119
00:10:23,680 --> 00:10:32,810
 encourage them to carry out the practice of Vipassana

120
00:10:32,810 --> 00:10:35,520
 meditation.

121
00:10:35,520 --> 00:10:40,800
 In fact this is an important point in receiving visitors.

122
00:10:40,800 --> 00:10:43,840
 We have to be careful not to think that we have to find out

123
00:10:43,840 --> 00:10:45,280
 all sorts of information

124
00:10:45,280 --> 00:10:54,700
 about them or sit around and talk with them for a long time

125
00:10:54,700 --> 00:10:55,160
.

126
00:10:55,160 --> 00:10:58,200
 Visit with them.

127
00:10:58,200 --> 00:11:01,570
 When visitors come we have to encourage them quickly to go

128
00:11:01,570 --> 00:11:03,560
 and practice because they don't

129
00:11:03,560 --> 00:11:12,720
 have so much time and we also don't have so much time.

130
00:11:12,720 --> 00:11:18,160
 We don't know when is going to be the end of our time here.

131
00:11:18,160 --> 00:11:22,300
 No one knows whether death will come tomorrow.

132
00:11:22,300 --> 00:11:29,640
 We waste our time talking or visiting.

133
00:11:29,640 --> 00:11:33,400
 This is not the way to receive guests.

134
00:11:33,400 --> 00:11:36,870
 Receive people who come to us, whoever, wherever we go,

135
00:11:36,870 --> 00:11:40,360
 when we go home or when we stay here.

136
00:11:40,360 --> 00:11:45,270
 We have to receive guests by putting them to practice right

137
00:11:45,270 --> 00:11:46,040
 away.

138
00:11:46,040 --> 00:11:50,020
 Giving them the opportunity to practice, not wasting their

139
00:11:50,020 --> 00:11:51,800
 time visiting or chatting or

140
00:11:51,800 --> 00:11:53,140
 so on.

141
00:11:53,140 --> 00:11:55,340
 Give them the things that they need to practice and then

142
00:11:55,340 --> 00:11:56,320
 give them the time.

143
00:11:56,320 --> 00:11:59,440
 Means teach them how to practice.

144
00:11:59,440 --> 00:12:03,170
 Once they know how to practice then send them off right

145
00:12:03,170 --> 00:12:04,680
 away to practice.

146
00:12:04,680 --> 00:12:11,680
 No time we have for them to practice.

147
00:12:11,680 --> 00:12:19,950
 Then we can really be said to have been hospitable in the

148
00:12:19,950 --> 00:12:22,680
 highest sense.

149
00:12:22,680 --> 00:12:27,320
 This is the end of respect, having respect.

150
00:12:27,320 --> 00:12:30,530
 All in all we have to have a respectful attitude towards

151
00:12:30,530 --> 00:12:32,320
 these six things, the Buddha, the

152
00:12:32,320 --> 00:12:41,680
 Dhamma, the Sangha, the training, not being negligent and

153
00:12:41,680 --> 00:12:45,400
 then receiving guests.

154
00:12:45,400 --> 00:12:48,400
 Next we go on, the next blessing is Nivat Oja.

155
00:12:48,400 --> 00:12:52,560
 Nivat Oja means something like humility.

156
00:12:52,560 --> 00:12:58,170
 It literally means to not be puffed up, to not be full of

157
00:12:58,170 --> 00:13:00,880
 hot air, to not be full of

158
00:13:00,880 --> 00:13:01,880
 oneself.

159
00:13:01,880 --> 00:13:10,040
 To not think highly of oneself and put other people down.

160
00:13:10,040 --> 00:13:15,640
 Or even also not to think low of oneself.

161
00:13:15,640 --> 00:13:21,480
 It's not to hold on to oneself.

162
00:13:21,480 --> 00:13:25,520
 Not to hold on to the idea of self that I am this, I am

163
00:13:25,520 --> 00:13:26,320
 that.

164
00:13:26,320 --> 00:13:28,400
 This is me, that is me.

165
00:13:28,400 --> 00:13:31,640
 Me, this is mine.

166
00:13:31,640 --> 00:13:37,320
 This is under my control.

167
00:13:37,320 --> 00:13:44,520
 Nivat Oja means to be humble.

168
00:13:44,520 --> 00:13:45,520
 Like Sariputta.

169
00:13:45,520 --> 00:13:55,400
 Sariputta is a very good example of humility.

170
00:13:55,400 --> 00:14:06,280
 Sariputta likened himself to an outcast.

171
00:14:06,280 --> 00:14:12,100
 In the time of the Buddha there were people who were out

172
00:14:12,100 --> 00:14:15,640
cast from Hindu society and they

173
00:14:15,640 --> 00:14:17,720
 had to hit a stick against the ground.

174
00:14:17,720 --> 00:14:21,560
 They had to carry a walking stick and beat it against the

175
00:14:21,560 --> 00:14:23,600
 ground to make noise wherever

176
00:14:23,600 --> 00:14:27,160
 they went.

177
00:14:27,160 --> 00:14:33,580
 So that the higher class people, the Brahmins and the Khaty

178
00:14:33,580 --> 00:14:37,040
as and other classes would know

179
00:14:37,040 --> 00:14:44,180
 that they were coming and would be able to avoid their

180
00:14:44,180 --> 00:14:45,560
 touch.

181
00:14:45,560 --> 00:14:50,120
 And Sariputta likened himself to one of these people.

182
00:14:50,120 --> 00:14:56,490
 He said as far as how I look at myself it's the same, that

183
00:14:56,490 --> 00:14:59,920
 I am some low class or outcast

184
00:14:59,920 --> 00:15:04,000
 person.

185
00:15:04,000 --> 00:15:10,700
 Because it happened that when he was walking by a young

186
00:15:10,700 --> 00:15:14,200
 monk, his robe touched the monk

187
00:15:14,200 --> 00:15:16,360
 as he was walking.

188
00:15:16,360 --> 00:15:20,640
 And the monk spread it around that Sariputta had hit him.

189
00:15:20,640 --> 00:15:26,800
 Sariputta had hit this monk.

190
00:15:26,800 --> 00:15:30,440
 And so Sariputta said for someone who was attached to

191
00:15:30,440 --> 00:15:32,880
 themselves then maybe they could

192
00:15:32,880 --> 00:15:33,880
 commit such a deed.

193
00:15:33,880 --> 00:15:38,280
 But for me he said I am like an outcast.

194
00:15:38,280 --> 00:15:44,880
 Or he said I am like the earth.

195
00:15:44,880 --> 00:15:48,660
 People may do what they want to the earth but the earth

196
00:15:48,660 --> 00:15:50,200
 never complains.

197
00:15:50,200 --> 00:15:53,040
 If we want to be fully humble we have to be like the earth.

198
00:15:53,040 --> 00:15:59,150
 The Lord Buddha said we have to be like the water, we have

199
00:15:59,150 --> 00:16:02,080
 to be like the air, we have

200
00:16:02,080 --> 00:16:07,200
 to be like the earth.

201
00:16:07,200 --> 00:16:15,350
 Like the air means because like anyone can come and write

202
00:16:15,350 --> 00:16:19,080
 on the earth or attack the

203
00:16:19,080 --> 00:16:23,430
 air, sorry, can come and attack the air and write in the

204
00:16:23,430 --> 00:16:24,720
 air and so on.

205
00:16:24,720 --> 00:16:28,320
 And nothing stays.

206
00:16:28,320 --> 00:16:32,810
 Just in the same way someone who is humble, whether someone

207
00:16:32,810 --> 00:16:34,720
 says bad things to them or

208
00:16:34,720 --> 00:16:38,480
 does bad things to them, it doesn't stay with the person

209
00:16:38,480 --> 00:16:40,920
 who is humble, who is not attached

210
00:16:40,920 --> 00:16:42,960
 to self.

211
00:16:42,960 --> 00:16:46,610
 They see that these things are only body and mind, sound or

212
00:16:46,610 --> 00:16:48,240
 sight or smell or taste or

213
00:16:48,240 --> 00:16:50,120
 feelings or thoughts.

214
00:16:50,120 --> 00:16:51,840
 And they come and they go.

215
00:16:51,840 --> 00:16:59,040
 And one doesn't hold on to them.

216
00:16:59,040 --> 00:17:03,050
 One should be like the water in the same way that when you

217
00:17:03,050 --> 00:17:05,560
 write on the water or when people

218
00:17:05,560 --> 00:17:08,500
 can urinate in the water or defecate in the water or throw

219
00:17:08,500 --> 00:17:10,240
 things in the water, the water

220
00:17:10,240 --> 00:17:12,800
 never complains.

221
00:17:12,800 --> 00:17:15,440
 Water accepts it all.

222
00:17:15,440 --> 00:17:21,020
 People write on the water or cut the water or hit the water

223
00:17:21,020 --> 00:17:23,360
 and never complain.

224
00:17:23,360 --> 00:17:24,760
 And the same with the earth.

225
00:17:24,760 --> 00:17:28,530
 People do what they want with the earth, they dig the earth

226
00:17:28,530 --> 00:17:30,800
, they cut the earth, they spoil

227
00:17:30,800 --> 00:17:32,960
 the earth and the earth never complains.

228
00:17:32,960 --> 00:17:38,120
 And we said this is how we should be, this is the teaching

229
00:17:38,120 --> 00:17:40,640
 of Niva the whole child.

230
00:17:40,640 --> 00:17:43,130
 Very important for meditators, we have to learn to let go

231
00:17:43,130 --> 00:17:43,960
 of ourselves.

232
00:17:43,960 --> 00:17:46,830
 It's not always to think about the bad things that people

233
00:17:46,830 --> 00:17:48,360
 did to us, no matter what they

234
00:17:48,360 --> 00:17:50,360
 did to us.

235
00:17:50,360 --> 00:17:55,320
 They didn't do it to anyone, no one did anything.

236
00:17:55,320 --> 00:17:57,960
 These things come and go, they arise.

237
00:17:57,960 --> 00:18:01,880
 They have to let go.

238
00:18:01,880 --> 00:18:03,880
 Let go in the mind.

239
00:18:03,880 --> 00:18:08,020
 These were only things that happened in the past, they

240
00:18:08,020 --> 00:18:10,440
 happened because of causes and

241
00:18:10,440 --> 00:18:15,440
 conditions.

242
00:18:15,440 --> 00:18:18,600
 All that's left now is our own thought about these things.

243
00:18:18,600 --> 00:18:27,850
 We just let go of our own thought and we don't go back to

244
00:18:27,850 --> 00:18:30,040
 it at all.

245
00:18:30,040 --> 00:18:33,920
 Next we have Santuti.

246
00:18:33,920 --> 00:18:39,040
 Santuti means contentment.

247
00:18:39,040 --> 00:18:43,300
 This is a great blessing but in Buddhism we need to have

248
00:18:43,300 --> 00:18:45,760
 both Santuti and Asantuti.

249
00:18:45,760 --> 00:18:48,440
 Both of them have been praised by the Buddha.

250
00:18:48,440 --> 00:18:55,980
 Contentment and discontent have both been praised by the

251
00:18:55,980 --> 00:18:57,440
 Buddha.

252
00:18:57,440 --> 00:19:03,410
 Contentment means we have to be content with what are requ

253
00:19:03,410 --> 00:19:05,320
isites we have.

254
00:19:05,320 --> 00:19:08,020
 Whatever clothing we have, we have to be content with it.

255
00:19:08,020 --> 00:19:12,950
 This is why monks and nuns wear rag robes, they wear robes

256
00:19:12,950 --> 00:19:15,880
 made of pieces of cloth stitched

257
00:19:15,880 --> 00:19:20,560
 together.

258
00:19:20,560 --> 00:19:27,960
 Contentment with whatever clothing we get.

259
00:19:27,960 --> 00:19:31,000
 Contentment with food we have to be content with.

260
00:19:31,000 --> 00:19:36,440
 Whatever alms food we get.

261
00:19:36,440 --> 00:19:39,590
 Contentment with shelter, whatever lodging we get we have

262
00:19:39,590 --> 00:19:40,920
 to be content with.

263
00:19:40,920 --> 00:19:48,000
 Not worrying that it's too hot or too cold or too noisy.

264
00:19:48,000 --> 00:19:54,680
 This and that, too small.

265
00:19:54,680 --> 00:20:01,560
 We should be content with whatever lodging we have.

266
00:20:01,560 --> 00:20:08,920
 With medicines we have, not to worry about finding this

267
00:20:08,920 --> 00:20:12,920
 medicine or that medicine.

268
00:20:12,920 --> 00:20:21,280
 We should be content to take whatever medicine we get.

269
00:20:21,280 --> 00:20:23,740
 Meaning not to be worried about sicknesses which might

270
00:20:23,740 --> 00:20:25,240
 arise in the body, always trying

271
00:20:25,240 --> 00:20:35,400
 to fix the body with this or that medicine.

272
00:20:35,400 --> 00:20:38,560
 But asantuti means we have to be discontent.

273
00:20:38,560 --> 00:20:45,130
 Discontent means discontent with whatever attainment which

274
00:20:45,130 --> 00:20:48,740
 we have gained in the practice.

275
00:20:48,740 --> 00:20:51,490
 We have to always think to ourselves that there is a higher

276
00:20:51,490 --> 00:20:52,680
 state which we have yet

277
00:20:52,680 --> 00:20:55,680
 to gain.

278
00:20:55,680 --> 00:21:04,870
 That though we have heard or we know about the attainment

279
00:21:04,870 --> 00:21:08,080
 of arahant, the attainment

280
00:21:08,080 --> 00:21:11,630
 of freedom from suffering, we still haven't reached the

281
00:21:11,630 --> 00:21:13,680
 attainment of freedom from suffering.

282
00:21:13,680 --> 00:21:15,820
 We still have greed, we still have anger, we still have

283
00:21:15,820 --> 00:21:17,040
 delusion and these things can

284
00:21:17,040 --> 00:21:18,720
 still arise in the future.

285
00:21:18,720 --> 00:21:24,120
 If we are not careful they will come back again and again

286
00:21:24,120 --> 00:21:25,640
 to haunt us.

287
00:21:25,640 --> 00:21:28,280
 So we should not be content with the level of practice

288
00:21:28,280 --> 00:21:29,680
 which we have attained.

289
00:21:29,680 --> 00:21:33,440
 We should never rest content.

290
00:21:33,440 --> 00:21:35,520
 This is one thing we should never be content with.

291
00:21:35,520 --> 00:21:38,640
 It's the Lord Buddha's teaching.

292
00:21:38,640 --> 00:21:43,150
 If we rest content with the spiritual attainment we have,

293
00:21:43,150 --> 00:21:45,720
 it's most likely that more bad and

294
00:21:45,720 --> 00:21:49,280
 evil states will come into our hearts.

295
00:21:49,280 --> 00:21:54,840
 This is being negligent, neglecting our duties.

296
00:21:54,840 --> 00:21:59,940
 And so more greed and anger and delusion will arise and

297
00:21:59,940 --> 00:22:02,640
 cover over our minds again.

298
00:22:02,640 --> 00:22:09,400
 We should not be content with the small attainment, not

299
00:22:09,400 --> 00:22:14,320
 even be content with, in the scriptures

300
00:22:14,320 --> 00:22:18,520
 they say, not even, the Lord Buddha said, not even be

301
00:22:18,520 --> 00:22:20,640
 content with panakami.

302
00:22:20,640 --> 00:22:23,800
 Even though we've cut off greed and anger we should not be

303
00:22:23,800 --> 00:22:25,080
 content with that until we

304
00:22:25,080 --> 00:22:32,720
 have cut off delusion.

305
00:22:32,720 --> 00:22:45,000
 This is the teaching on Samtuti.

306
00:22:45,000 --> 00:22:52,000
 Next we have Kathan, Kathan yuta, kalayanan hamasavanam.

307
00:22:52,000 --> 00:22:56,000
 I think I'll save those for tomorrow.

308
00:22:56,000 --> 00:22:58,840
 Time for morning chanting.

309
00:22:58,840 --> 00:23:03,720
 Please continue to practice.

310
00:23:03,720 --> 00:23:07,480
 This morning I guess we have an ordination.

311
00:23:07,480 --> 00:23:09,480
 Congratulations.

312
00:23:09,480 --> 00:23:12,960
 You can go ahead, they'll probably start chanting now.

