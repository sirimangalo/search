1
00:00:00,000 --> 00:00:04,990
 It says, "We know that in the children's development

2
00:00:04,990 --> 00:00:09,920
 process fairy tales is very important, but

3
00:00:09,920 --> 00:00:13,160
 at the same time it creates an illusion for them.

4
00:00:13,160 --> 00:00:17,920
 What's your advice and what would be good to teach them?"

5
00:00:17,920 --> 00:00:24,160
 Yeah, I don't think the truth of a story is necessary for

6
00:00:24,160 --> 00:00:27,880
 it to be useful, beneficial.

7
00:00:27,880 --> 00:00:30,710
 A story itself has to be, you have to ask the question, "

8
00:00:30,710 --> 00:00:32,360
What good is a story itself?"

9
00:00:32,360 --> 00:00:35,330
 Like, if I tell you a true story or I tell you a story that

10
00:00:35,330 --> 00:00:37,600
 never happened, I don't think

11
00:00:37,600 --> 00:00:41,680
 that is a salient feature or an important feature of the

12
00:00:41,680 --> 00:00:43,800
 story or the usefulness.

13
00:00:43,800 --> 00:00:47,520
 The question is, "To what benefit are stories?"

14
00:00:47,520 --> 00:00:51,580
 And you say that we know that fairy tales are very

15
00:00:51,580 --> 00:00:52,840
 important.

16
00:00:52,840 --> 00:00:58,400
 And I'm going to cede to your knowledge on that.

17
00:00:58,400 --> 00:01:00,080
 It makes sense to me.

18
00:01:00,080 --> 00:01:03,150
 It's not something I, I don't have children personally, so

19
00:01:03,150 --> 00:01:06,520
 I've never really thought about

20
00:01:06,520 --> 00:01:09,560
 how stories benefit.

21
00:01:09,560 --> 00:01:13,080
 But you have to look more at the content of the story.

22
00:01:13,080 --> 00:01:18,970
 So if you tell a child a true story that, you know, corrupt

23
00:01:18,970 --> 00:01:22,080
s them, you know, tell them all

24
00:01:22,080 --> 00:01:25,350
 about these horrible people and maybe even glorify it a

25
00:01:25,350 --> 00:01:27,280
 little bit and talk about how,

26
00:01:27,280 --> 00:01:34,650
 you know, some stories, some stories you read are just,

27
00:01:34,650 --> 00:01:38,680
 just of no use, of no benefit to

28
00:01:38,680 --> 00:01:43,320
 people whether they be true or not.

29
00:01:43,320 --> 00:01:49,470
 I'm thinking now of stories that aren't true, of novels and

30
00:01:49,470 --> 00:01:52,040
 so on that talk about the true

31
00:01:52,040 --> 00:01:54,040
 truth, actually.

32
00:01:54,040 --> 00:01:56,840
 I mean, it's an interesting question.

33
00:01:56,840 --> 00:02:04,350
 So stories that are made up but are based on the way things

34
00:02:04,350 --> 00:02:06,000
 actually go.

35
00:02:06,000 --> 00:02:09,850
 So they talk about corruption and they show people being

36
00:02:09,850 --> 00:02:11,680
 corrupt and they show this as

37
00:02:11,680 --> 00:02:14,360
 being the state of the world.

38
00:02:14,360 --> 00:02:16,520
 And so there's truth in that, right?

39
00:02:16,520 --> 00:02:18,360
 It's really talking about the true state.

40
00:02:18,360 --> 00:02:21,570
 Like people can go into slums and research and write a

41
00:02:21,570 --> 00:02:23,520
 story about how horrible life

42
00:02:23,520 --> 00:02:25,400
 there can be.

43
00:02:25,400 --> 00:02:29,980
 But the question of in what way and in what context and to

44
00:02:29,980 --> 00:02:32,400
 what audience that's useful

45
00:02:32,400 --> 00:02:39,350
 is something, I think, quite interesting and the most

46
00:02:39,350 --> 00:02:42,400
 important question.

47
00:02:42,400 --> 00:02:46,720
 So I think it's generally understood that moral, stories

48
00:02:46,720 --> 00:02:49,280
 with moral are very important,

49
00:02:49,280 --> 00:02:51,760
 stories that teach you something.

50
00:02:51,760 --> 00:02:56,720
 But what I wanted to say about the point I was making about

51
00:02:56,720 --> 00:02:59,440
 the stories about the horrors

52
00:02:59,440 --> 00:03:03,880
 of life is I think stories that motivate are important,

53
00:03:03,880 --> 00:03:07,280
 stories that encourage are important.

54
00:03:07,280 --> 00:03:10,250
 So if your story is just teaching us, like they talk about

55
00:03:10,250 --> 00:03:11,800
 this catharsis, so a lot of

56
00:03:11,800 --> 00:03:13,960
 Shakespearean stuff, right?

57
00:03:13,960 --> 00:03:16,640
 These stories that end just horribly, right?

58
00:03:16,640 --> 00:03:17,800
 And I can never understand it.

59
00:03:17,800 --> 00:03:21,670
 And I think I'm leaning towards a justification that or a

60
00:03:21,670 --> 00:03:24,080
 belief that these sort of stories

61
00:03:24,080 --> 00:03:26,890
 are not justified, even for bringing about a catharsis,

62
00:03:26,890 --> 00:03:28,560
 which of course is not a Buddhist

63
00:03:28,560 --> 00:03:31,520
 concept, a purging of emotions, right?

64
00:03:31,520 --> 00:03:36,480
 So it's a relief to see such horrible things.

65
00:03:36,480 --> 00:03:40,800
 Because to me, it leads to despair and depression and the

66
00:03:40,800 --> 00:03:43,400
 feeling that there is no way out,

67
00:03:43,400 --> 00:03:48,320
 which a lot of stories do.

68
00:03:48,320 --> 00:03:53,480
 So stories that are, I think, encouraging are important.

69
00:03:53,480 --> 00:03:55,520
 Now they have to, of course, encourage in the right way.

70
00:03:55,520 --> 00:03:59,680
 The other side is these fairy tale happily ever after

71
00:03:59,680 --> 00:04:02,800
 stories, which are probably equally

72
00:04:02,800 --> 00:04:07,920
 harmful, I would say, to children.

73
00:04:07,920 --> 00:04:11,440
 The idea, you know, and this has been brought up, I think,

74
00:04:11,440 --> 00:04:13,600
 in sociology circles or psychology

75
00:04:13,600 --> 00:04:17,550
 circles where they they they've pointed this as a real

76
00:04:17,550 --> 00:04:20,240
 problem where, for example, the

77
00:04:20,240 --> 00:04:26,430
 princess waits for her prints or and so this leads women to

78
00:04:26,430 --> 00:04:29,440
 feel like they are just objects

79
00:04:29,440 --> 00:04:32,960
 waiting to be plucked off of a tree or so on.

80
00:04:32,960 --> 00:04:36,450
 But in general, the idea that at the end of the story,

81
00:04:36,450 --> 00:04:38,840
 Prince Charming or whatever, you

82
00:04:38,840 --> 00:04:41,680
 know, the man, the women, the guy gets the girl, the girl

83
00:04:41,680 --> 00:04:43,200
 gets the guy, and they live

84
00:04:43,200 --> 00:04:44,240
 happily ever after.

85
00:04:44,240 --> 00:04:49,060
 This is also problematic because it, you know, and stories

86
00:04:49,060 --> 00:04:51,400
 like it that lead us closer to

87
00:04:51,400 --> 00:04:57,950
 the wheel lead us or remind us or encourage us or teach us

88
00:04:57,950 --> 00:05:01,200
 the rightness of the wheel

89
00:05:01,200 --> 00:05:07,050
 of samsara in terms of following the system, succeeding in

90
00:05:07,050 --> 00:05:10,600
 life, meeting with all the trials

91
00:05:10,600 --> 00:05:16,650
 and tribulations of life and conquering them by becoming a

92
00:05:16,650 --> 00:05:19,840
 successful whatever or, you

93
00:05:19,840 --> 00:05:25,240
 know, defeating the enemies and so on leads people to leads

94
00:05:25,240 --> 00:05:28,360
 people in the wrong direction.

95
00:05:28,360 --> 00:05:32,600
 So the question of which story sort of stories are useful

96
00:05:32,600 --> 00:05:35,840
 is a really interesting one.

97
00:05:35,840 --> 00:05:39,880
 Which stories as a Buddhist would you recommend?

98
00:05:39,880 --> 00:05:42,920
 So first of all, your question is about illusion.

99
00:05:42,920 --> 00:05:45,360
 I don't think the illusion is the problem.

100
00:05:45,360 --> 00:05:46,840
 The moral is the problem.

101
00:05:46,840 --> 00:05:53,100
 The what the story is teaching is the problem or is the

102
00:05:53,100 --> 00:05:54,720
 question.

103
00:05:54,720 --> 00:05:57,340
 And so I think the answer from a Buddhist point of view is

104
00:05:57,340 --> 00:05:58,720
 quite simplest to teach of

105
00:05:58,720 --> 00:06:00,040
 course Buddhist principles.

106
00:06:00,040 --> 00:06:01,200
 And this is what Christians do.

107
00:06:01,200 --> 00:06:02,960
 This is what all religions do.

108
00:06:02,960 --> 00:06:09,310
 They teach, they have fairy tales that teach certain morals

109
00:06:09,310 --> 00:06:11,640
 in their religion.

110
00:06:11,640 --> 00:06:15,790
 And so for example, the Jatakas are actually a really good

111
00:06:15,790 --> 00:06:17,040
 tool for this.

112
00:06:17,040 --> 00:06:20,110
 I printed up some of the, some retellings of the Jatakas

113
00:06:20,110 --> 00:06:21,880
 that are on the internet, the

114
00:06:21,880 --> 00:06:26,490
 past life stories of the Buddha and shared them and shared

115
00:06:26,490 --> 00:06:28,560
 them or gave them to these

116
00:06:28,560 --> 00:06:32,160
 novices and had them, we sat around and read them.

117
00:06:32,160 --> 00:06:38,340
 These Vietnamese Cambodian, mostly Vietnamese and Cambodian

118
00:06:38,340 --> 00:06:41,160
 kids who ordained as novices

119
00:06:41,160 --> 00:06:42,160
 for the summer.

120
00:06:42,160 --> 00:06:43,500
 And these were pretty bad kids.

121
00:06:43,500 --> 00:06:45,920
 Some of them were gangsters.

122
00:06:45,920 --> 00:06:49,790
 Most of them were hooked on, like most kids hooked on PS2

123
00:06:49,790 --> 00:06:53,760
 or whatever it was at the time.

124
00:06:53,760 --> 00:06:54,760
 I don't know.

125
00:06:54,760 --> 00:06:55,760
 They had this PlayStation.

126
00:06:55,760 --> 00:07:00,600
 I think that, yeah, they had a PlayStation.

127
00:07:00,600 --> 00:07:04,480
 So I ended up, they were all staying in this one room and

128
00:07:04,480 --> 00:07:07,360
 just lying around playing PlayStation.

129
00:07:07,360 --> 00:07:10,430
 And so this is what they were doing as novices, which of

130
00:07:10,430 --> 00:07:12,560
 course is not allowed, but that's

131
00:07:12,560 --> 00:07:13,560
 how it goes.

132
00:07:13,560 --> 00:07:14,760
 So in the beginning, I wasn't my monastery.

133
00:07:14,760 --> 00:07:15,760
 I was just a visitor.

134
00:07:15,760 --> 00:07:18,830
 And so for the first few days, I just let it be and said,

135
00:07:18,830 --> 00:07:20,360
 it's not my business.

136
00:07:20,360 --> 00:07:21,720
 But after a while, it got horrible.

137
00:07:21,720 --> 00:07:24,980
 They were lying around swearing and cursing and really

138
00:07:24,980 --> 00:07:27,040
 making a nuisance of themselves.

139
00:07:27,040 --> 00:07:29,880
 So I went into the kitchen and found the fuse box and

140
00:07:29,880 --> 00:07:31,860
 turned off their electricity.

141
00:07:31,860 --> 00:07:35,590
 So that turned off all the fans and of course turned off

142
00:07:35,590 --> 00:07:37,240
 their PlayStation.

143
00:07:37,240 --> 00:07:40,550
 And they were lying in the middle of summer in this

144
00:07:40,550 --> 00:07:44,120
 basement room suffering all of a sudden.

145
00:07:44,120 --> 00:07:46,310
 Of course, they had no clue what had happened and they didn

146
00:07:46,310 --> 00:07:47,640
't know where the fuse box was.

147
00:07:47,640 --> 00:07:48,800
 So then it was a lot of fun.

148
00:07:48,800 --> 00:07:51,240
 And then I was able to really talk to them.

149
00:07:51,240 --> 00:07:56,520
 And I read with them these jatakas.

150
00:07:56,520 --> 00:07:59,560
 It didn't have a big effect.

151
00:07:59,560 --> 00:08:04,900
 But I think because they weren't that interested and after

152
00:08:04,900 --> 00:08:07,200
 a while, no, because other people

153
00:08:07,200 --> 00:08:09,040
 had more of an effect.

154
00:08:09,040 --> 00:08:10,960
 It was much more hands-on.

155
00:08:10,960 --> 00:08:18,640
 I ended up taking them on a camping trip.

156
00:08:18,640 --> 00:08:22,380
 We ended up turning from the jatakas to the Majima Nikaya,

157
00:08:22,380 --> 00:08:24,480
 the Balapan-Dita Sut, I think.

158
00:08:24,480 --> 00:08:26,580
 And because they were asking suddenly when I think they

159
00:08:26,580 --> 00:08:27,840
 read in one of the jatakas about

160
00:08:27,840 --> 00:08:30,600
 hell and they were asking about hell.

161
00:08:30,600 --> 00:08:33,900
 And so, oh, then I got into the, as Larry, you were telling

162
00:08:33,900 --> 00:08:36,040
 about being drawn and quartered.

163
00:08:36,040 --> 00:08:38,840
 There's descriptions of what hell is like.

164
00:08:38,840 --> 00:08:41,610
 And drawn and quartered is actually pretty tame in

165
00:08:41,610 --> 00:08:43,200
 comparison with some of the tortures

166
00:08:43,200 --> 00:08:44,840
 that they talk about in the Majima Nikaya.

167
00:08:44,840 --> 00:08:46,880
 And so this really hit the gangster kids.

168
00:08:46,880 --> 00:08:49,970
 There were these two gangster kids who really, really were

169
00:08:49,970 --> 00:08:51,620
 moved by the whole experience

170
00:08:51,620 --> 00:08:55,500
 of ordaining and my torturing them with these stories and

171
00:08:55,500 --> 00:08:57,800
 then talking about ghosts in the

172
00:08:57,800 --> 00:08:59,520
 monastery and so on.

173
00:08:59,520 --> 00:09:01,920
 I was just relating other people telling me ghost stories

174
00:09:01,920 --> 00:09:03,160
 that they'd seen ghosts in the

175
00:09:03,160 --> 00:09:04,160
 monastery.

176
00:09:04,160 --> 00:09:08,410
 So, one night I was telling them what I had heard with the

177
00:09:08,410 --> 00:09:10,360
 intent to really freak them

178
00:09:10,360 --> 00:09:13,160
 out because they really needed to be freaked out.

179
00:09:13,160 --> 00:09:16,920
 And then in the morning the abbot came down and scolded me

180
00:09:16,920 --> 00:09:18,880
 and said, "Were you telling

181
00:09:18,880 --> 00:09:19,880
 them ghost stories last night?"

182
00:09:19,880 --> 00:09:22,360
 I said, "Yeah, I was just telling them what I had heard."

183
00:09:22,360 --> 00:09:26,600
 He said, "Well, last night I had 10 novices come to my room

184
00:09:26,600 --> 00:09:29,040
 and refuse to sleep anywhere,

185
00:09:29,040 --> 00:09:36,420
 refused to leave and ended up sleeping on the floor in his

186
00:09:36,420 --> 00:09:37,560
 room."

187
00:09:37,560 --> 00:09:38,760
 So I think that's good examples.

188
00:09:38,760 --> 00:09:41,200
 I mean, ghost stories can work for certain kids.

189
00:09:41,200 --> 00:09:46,260
 I mean, these were kids who really had no, many of them

190
00:09:46,260 --> 00:09:47,320
 anyway.

191
00:09:47,320 --> 00:09:48,320
 Some of them were good.

192
00:09:48,320 --> 00:09:52,890
 Many of them had no strong parental guidance and so it was

193
00:09:52,890 --> 00:09:55,400
 necessary to shock them with

194
00:09:55,400 --> 00:09:56,400
 things like that.

195
00:09:56,400 --> 00:10:00,470
 For good kids, man, the jatakas can be a wonderful

196
00:10:00,470 --> 00:10:01,560
 experience.

197
00:10:01,560 --> 00:10:04,900
 It's a shame that most of them are still not, well, they're

198
00:10:04,900 --> 00:10:06,840
 still mostly not translated.

199
00:10:06,840 --> 00:10:10,550
 They're translated in old, old English, which kids would

200
00:10:10,550 --> 00:10:11,800
 not understand.

201
00:10:11,800 --> 00:10:16,050
 But there are some retelling, retold jatakas, which are

202
00:10:16,050 --> 00:10:18,400
 lots of fun and I think would be

203
00:10:18,400 --> 00:10:19,400
 really good for kids.

204
00:10:19,400 --> 00:10:20,880
 I gave them to my brothers once.

205
00:10:20,880 --> 00:10:24,400
 I was kind of in my proselytizing phase.

206
00:10:24,400 --> 00:10:26,800
 I was new to Buddhism and I wanted everyone to become Buddh

207
00:10:26,800 --> 00:10:27,920
ists so I gave them away.

208
00:10:27,920 --> 00:10:29,960
 I printed them up and gave them to my brothers.

209
00:10:29,960 --> 00:10:32,160
 I didn't think they ever read them.

210
00:10:32,160 --> 00:10:37,510
 But I would recommend the jatakas specifically, that if you

211
00:10:37,510 --> 00:10:40,040
 want to tell fairy tales.

212
00:10:40,040 --> 00:10:45,060
 Another good thing is on the internet, there's this one guy

213
00:10:45,060 --> 00:10:47,880
 on YouTube who actually has some

214
00:10:47,880 --> 00:10:50,200
 of the jatakas in cartoon form.

215
00:10:50,200 --> 00:10:52,140
 I don't know where he got them, whether he made them or not

216
00:10:52,140 --> 00:10:53,500
, but it looks like he's making

217
00:10:53,500 --> 00:10:57,760
 or someone's making in India all sorts of cartoons, mostly

218
00:10:57,760 --> 00:10:59,800
 Hindu but some Buddhist.

219
00:10:59,800 --> 00:11:02,150
 So if you look up jataka on YouTube, I bet you'll find his

220
00:11:02,150 --> 00:11:03,280
 stuff and you'll probably

221
00:11:03,280 --> 00:11:07,200
 find some other stuff.

222
00:11:07,200 --> 00:11:11,000
 Get the book, this book of BuddhaNet and print it up and

223
00:11:11,000 --> 00:11:13,360
 read the jatakas to your kids.

224
00:11:13,360 --> 00:11:15,400
 Read these stories to your kids because they're wonderful.

225
00:11:15,400 --> 00:11:18,080
 Some of them are just excellent.

226
00:11:18,080 --> 00:11:21,730
 If I had been read this stuff when I was a kid, I can't

227
00:11:21,730 --> 00:11:24,020
 imagine how different my life

228
00:11:24,020 --> 00:11:28,250
 would have been to have this kind of moral teaching and to

229
00:11:28,250 --> 00:11:30,960
 have heard something so beautiful

230
00:11:30,960 --> 00:11:36,220
 and so pure because that's really what's lacking in most of

231
00:11:36,220 --> 00:11:43,220
 our fairy tales and our stories

232
00:11:43,220 --> 00:11:46,880
 and our entertainment.

233
00:11:46,880 --> 00:11:54,150
 People nowadays can put together brilliant entertainment

234
00:11:54,150 --> 00:11:57,640
 but tend to lack in any moral,

235
00:11:57,640 --> 00:12:04,200
 don't think to put a moral in the movie or the book or the

236
00:12:04,200 --> 00:12:05,440
 story.

237
00:12:05,440 --> 00:12:07,960
 Disney did some good stuff, I think later on.

238
00:12:07,960 --> 00:12:11,070
 The early stuff was probably pretty horrible, was pretty

239
00:12:11,070 --> 00:12:12,440
 horrible for morals.

240
00:12:12,440 --> 00:12:18,010
 But I don't know, I mean I haven't watched Disney in a long

241
00:12:18,010 --> 00:12:20,600
 time but Mulan, I think that

242
00:12:20,600 --> 00:12:25,100
 was kind of, not in a Buddhist sense but that was a good

243
00:12:25,100 --> 00:12:27,680
 one for seeing the equality of

244
00:12:27,680 --> 00:12:28,680
 gender equality.

245
00:12:28,680 --> 00:12:30,440
 I'm trying to think now.

246
00:12:30,440 --> 00:12:35,550
 I know Disney got better, I think they did eventually have,

247
00:12:35,550 --> 00:12:37,960
 what is it, Lion King, not

248
00:12:37,960 --> 00:12:44,230
 totally Buddhist but some idea of letting go of the past,

249
00:12:44,230 --> 00:12:47,480
 that one scene where he's told

250
00:12:47,480 --> 00:12:49,160
 to let go of the past.

251
00:12:49,160 --> 00:12:52,810
 He says, "Yeah, yeah, I don't want to go back because of

252
00:12:52,810 --> 00:12:54,440
 how bad the past was."

253
00:12:54,440 --> 00:12:57,220
 And this monkey hits him in the head and he says, "Oh, that

254
00:12:57,220 --> 00:12:57,880
 hurts."

255
00:12:57,880 --> 00:12:58,880
 He says, "What?

256
00:12:58,880 --> 00:12:59,880
 What do you mean it hurts?

257
00:12:59,880 --> 00:13:00,880
 It's in the past."

258
00:13:00,880 --> 00:13:02,560
 I said, "Oh no, I'm getting it all mixed up."

259
00:13:02,560 --> 00:13:04,120
 He says, "I don't want to go back to the past."

260
00:13:04,120 --> 00:13:08,760
 I said, "It's interesting, it's actually a weird lesson,

261
00:13:08,760 --> 00:13:09,840
 isn't it?"

262
00:13:09,840 --> 00:13:11,960
 So he's given up the past, he says, "I don't want to deal

263
00:13:11,960 --> 00:13:13,120
 with the past, I don't want to

264
00:13:13,120 --> 00:13:14,120
 worry about it."

265
00:13:14,120 --> 00:13:16,040
 And then he hits him and he says, "What?

266
00:13:16,040 --> 00:13:17,040
 It's in the past."

267
00:13:17,040 --> 00:13:20,480
 And then he realizes, "Ah, the past is important."

268
00:13:20,480 --> 00:13:22,520
 Which is weird, it's not really Buddhist.

269
00:13:22,520 --> 00:13:26,440
 Anyway, I just mean to give his examples of stories that do

270
00:13:26,440 --> 00:13:28,440
 have morals, whether they're

271
00:13:28,440 --> 00:13:29,880
 Buddhist morals or not.

272
00:13:29,880 --> 00:13:35,640
 And for the most part, look at all of these fairy tales.

273
00:13:35,640 --> 00:13:40,510
 We did a project in high school on nursery rhymes and fairy

274
00:13:40,510 --> 00:13:41,240
 tales.

275
00:13:41,240 --> 00:13:46,680
 And they're really weird, like Humpty Dumpty, for example.

276
00:13:46,680 --> 00:13:51,700
 Little Jack Horner, I think is actually supposed to have

277
00:13:51,700 --> 00:13:54,680
 stolen this deed or something.

278
00:13:54,680 --> 00:13:59,520
 These weren't actually children's stories.

279
00:13:59,520 --> 00:14:01,920
 And Georgie Porgy, I mean, what are these things?

280
00:14:01,920 --> 00:14:04,760
 This is the stuff that, I mean, this is the kind of stuff

281
00:14:04,760 --> 00:14:06,280
 that kids like because it's

282
00:14:06,280 --> 00:14:08,480
 kind of naughty.

283
00:14:08,480 --> 00:14:12,660
 But, you know, and what is this, "Rockabye baby off tree

284
00:14:12,660 --> 00:14:13,440
 tops?"

285
00:14:13,440 --> 00:14:18,440
 I mean, horrific.

286
00:14:18,440 --> 00:14:19,440
 Horrific stuff.

287
00:14:19,440 --> 00:14:20,440
 It's just useless and meaningless.

288
00:14:20,440 --> 00:14:23,820
 So supplanting this with some really cool Buddhist stuff

289
00:14:23,820 --> 00:14:24,960
 would be great.

290
00:14:24,960 --> 00:14:26,520
 So recommend the Jatakas.

