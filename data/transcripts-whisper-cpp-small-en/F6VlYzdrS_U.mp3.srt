1
00:00:00,000 --> 00:00:27,240
 Good evening, everyone.

2
00:00:27,240 --> 00:00:31,800
 We practice meditation.

3
00:00:31,800 --> 00:00:40,800
 We practice mindfulness.

4
00:00:40,800 --> 00:00:56,320
 We practice with the purpose of strengthening our minds.

5
00:00:56,320 --> 00:01:10,990
 We don't practice to escape or to change, not to change our

6
00:01:10,990 --> 00:01:17,120
 experiences anyway.

7
00:01:17,120 --> 00:01:20,110
 You don't come here thinking, "I'll go to the meditation

8
00:01:20,110 --> 00:01:21,560
 center so I won't have to deal

9
00:01:21,560 --> 00:01:24,480
 with my real-life problems."

10
00:01:24,480 --> 00:01:27,400
 I hope you don't.

11
00:01:27,400 --> 00:01:29,880
 I need a vacation.

12
00:01:29,880 --> 00:01:39,800
 I'll go and stay in a room in a basement somewhere.

13
00:01:39,800 --> 00:01:48,640
 We come here to become strong.

14
00:01:48,640 --> 00:02:04,750
 It's important that we understand the weakness in defile

15
00:02:04,750 --> 00:02:11,400
ment and the weakness in evil.

16
00:02:11,400 --> 00:02:16,030
 This is strength, not because it's goodness, but because

17
00:02:16,030 --> 00:02:19,120
 that's the definition of goodness.

18
00:02:19,120 --> 00:02:28,080
 It's a part of the definition of goodness, is strength.

19
00:02:28,080 --> 00:02:32,210
 It's a characteristic of certain mind states that really

20
00:02:32,210 --> 00:02:38,080
 deserve the title of goodness.

21
00:02:38,080 --> 00:02:41,680
 There's a strength to them.

22
00:02:41,680 --> 00:02:52,570
 The reason why we denounce and reject certain other states,

23
00:02:52,570 --> 00:02:57,520
 greed, anger, delusion, it's

24
00:02:57,520 --> 00:03:00,680
 because they're weak.

25
00:03:00,680 --> 00:03:03,680
 They're vulnerable, fragile.

26
00:03:03,680 --> 00:03:13,070
 If you're a person easily angered, you're going to react,

27
00:03:13,070 --> 00:03:16,560
 easily disturbed.

28
00:03:16,560 --> 00:03:17,560
 What's wrong with anger?

29
00:03:17,560 --> 00:03:19,320
 Well, we say anger is evil.

30
00:03:19,320 --> 00:03:21,040
 What's really evil about it?

31
00:03:21,040 --> 00:03:24,290
 The worst thing about anger is you poke someone who's angry

32
00:03:24,290 --> 00:03:25,920
 and they're disturbed.

33
00:03:25,920 --> 00:03:30,480
 They suffer or are greed.

34
00:03:30,480 --> 00:03:36,050
 If you like things, if you want things, you're vulnerable,

35
00:03:36,050 --> 00:03:39,600
 you're weak, easily manipulated.

36
00:03:39,600 --> 00:03:41,000
 How do you manipulate someone?

37
00:03:41,000 --> 00:03:44,870
 Well, there are different ways, but one of the easiest ways

38
00:03:44,870 --> 00:03:46,560
 is take advantage of their

39
00:03:46,560 --> 00:03:52,000
 addictions, their likes, their dislikes.

40
00:03:52,000 --> 00:03:58,710
 It's much harder to manipulate someone who has no likes or

41
00:03:58,710 --> 00:04:02,360
 dislikes, who is not moved.

42
00:04:02,360 --> 00:04:03,360
 This is what the Buddha said.

43
00:04:03,360 --> 00:04:06,360
 He said, "Putasalokadhami chitanya-sanakampati."

44
00:04:06,360 --> 00:04:25,840
 There was the sort of person who went touched by the lokad

45
00:04:25,840 --> 00:04:29,560
hamma, the ways of the world.

46
00:04:29,560 --> 00:04:37,400
 Chitanya-sanakampati, the mind of such a person, such a

47
00:04:37,400 --> 00:04:41,800
 person, the mind of a person who is

48
00:04:41,800 --> 00:04:43,800
 not shaken.

49
00:04:43,800 --> 00:04:52,040
 Nakampati is like this, you wave her.

50
00:04:52,040 --> 00:04:59,420
 Asokang, because their mind is not shaken by the viciss

51
00:04:59,420 --> 00:05:03,680
itudes of life, it changes the

52
00:05:03,680 --> 00:05:09,960
 ways of the world.

53
00:05:09,960 --> 00:05:28,680
 Their un-sad, un-mournful, unregretful, unregretting.

54
00:05:28,680 --> 00:05:29,390
 Weerajang, stainless, pure, kaimang, safe, safe, truly safe

55
00:05:29,390 --> 00:05:44,320
, etamang, garamutamang.

56
00:05:44,320 --> 00:05:48,120
 This is the highest blessing.

57
00:05:48,120 --> 00:06:01,760
 This is an ultimate blessing, a real and true blessing.

58
00:06:01,760 --> 00:06:04,320
 So I thought it would be useful for us to talk a little bit

59
00:06:04,320 --> 00:06:05,680
 about, I think I've talked

60
00:06:05,680 --> 00:06:13,040
 about these before, but about the lokadhamma, the things in

61
00:06:13,040 --> 00:06:17,000
 the world that disturb us, that

62
00:06:17,000 --> 00:06:22,310
 cause us to wave her, to, in our minds, cause us to become

63
00:06:22,310 --> 00:06:23,280
 upset.

64
00:06:23,280 --> 00:06:32,720
 What are they?

65
00:06:32,720 --> 00:06:39,160
 Gain and gain and loss.

66
00:06:39,160 --> 00:06:42,380
 We're disturbed and upset by these things, disturbed

67
00:06:42,380 --> 00:06:44,480
 positively in the sense of wanting

68
00:06:44,480 --> 00:06:52,450
 gain, negatively in the sense of fearing and reacting

69
00:06:52,450 --> 00:06:55,800
 negatively to loss.

70
00:06:55,800 --> 00:07:03,710
 So much of what we do in the world is for gain, to get this

71
00:07:03,710 --> 00:07:07,660
, get that, get money.

72
00:07:07,660 --> 00:07:15,520
 Money is really just a placeholder, isn't it?

73
00:07:15,520 --> 00:07:16,520
 I mean, money is not a good in and of itself.

74
00:07:16,520 --> 00:07:18,520
 I argued with my philosophy professor about this last year.

75
00:07:18,520 --> 00:07:23,480
 He was saying money isn't the people, because I think it

76
00:07:23,480 --> 00:07:25,240
 was, who was it?

77
00:07:25,240 --> 00:07:30,810
 Aristotle says something about people who crave after money

78
00:07:30,810 --> 00:07:33,600
, who see money as the ultimate

79
00:07:33,600 --> 00:07:34,600
 good.

80
00:07:34,600 --> 00:07:38,270
 I said, well, isn't it, isn't it possible that someone

81
00:07:38,270 --> 00:07:40,200
 could just really like money

82
00:07:40,200 --> 00:07:46,450
 because it's true, you know, as ridiculous as it seems,

83
00:07:46,450 --> 00:07:47,880
 people are just happy to have

84
00:07:47,880 --> 00:07:50,880
 lots of money.

85
00:07:50,880 --> 00:07:56,620
 I mean, if you think about it logically, well, it is just

86
00:07:56,620 --> 00:07:59,840
 paper, but we get very proud.

87
00:07:59,840 --> 00:08:01,940
 It's gotten to the point where money no longer is a

88
00:08:01,940 --> 00:08:02,760
 placeholder.

89
00:08:02,760 --> 00:08:07,320
 Money is, I mean, it represents so much.

90
00:08:07,320 --> 00:08:13,000
 It represents power, represents getting so many things.

91
00:08:13,000 --> 00:08:16,200
 You can get what you want if you have money.

92
00:08:16,200 --> 00:08:33,040
 So money itself becomes something that you want to get.

93
00:08:33,040 --> 00:08:38,120
 Wanting to get possessions, a car, a house, so many things.

94
00:08:38,120 --> 00:08:41,440
 But it's really deeper than that, isn't it?

95
00:08:41,440 --> 00:08:48,320
 Wanting to get food, craving food.

96
00:08:48,320 --> 00:08:56,440
 Very, very simple desire.

97
00:08:56,440 --> 00:08:58,320
 Wanting food that you don't have.

98
00:08:58,320 --> 00:09:01,690
 Coming to a meditation center where all the food is, I don

99
00:09:01,690 --> 00:09:03,480
't know, I think Javan makes

100
00:09:03,480 --> 00:09:04,480
 good food.

101
00:09:04,480 --> 00:09:08,690
 He disparages food, but maybe it's not haute couture or

102
00:09:08,690 --> 00:09:15,560
 certainly we have, right now we

103
00:09:15,560 --> 00:09:20,920
 have a Vietnamese student and where are you from?

104
00:09:20,920 --> 00:09:22,760
 South America?

105
00:09:22,760 --> 00:09:23,760
 Central America?

106
00:09:23,760 --> 00:09:24,760
 Mexico.

107
00:09:24,760 --> 00:09:26,760
 A Mexican student.

108
00:09:26,760 --> 00:09:34,440
 We don't have any Mexican food, I don't think.

109
00:09:34,440 --> 00:09:39,760
 Maybe some Vietnamese food.

110
00:09:39,760 --> 00:09:47,080
 Wanting it, wanting this, wanting that, the things we want.

111
00:09:47,080 --> 00:09:50,400
 Disturbed by it, no?

112
00:09:50,400 --> 00:10:00,520
 How disturbed the mind becomes, how weak the mind is

113
00:10:00,520 --> 00:10:04,320
 because of wanting.

114
00:10:04,320 --> 00:10:08,340
 Becomes an obsession where you think as soon as I get it I

115
00:10:08,340 --> 00:10:10,240
'll be happy, right?

116
00:10:10,240 --> 00:10:18,080
 It's okay to want because once I get it then I'll be happy.

117
00:10:18,080 --> 00:10:23,810
 If you don't challenge yourself, you'll never see the, it

118
00:10:23,810 --> 00:10:26,640
 may never, not never, but you

119
00:10:26,640 --> 00:10:33,100
 may take time for you to actually see how precarious the

120
00:10:33,100 --> 00:10:35,160
 situation is.

121
00:10:35,160 --> 00:10:40,140
 As long as you can keep getting what you want, maybe temper

122
00:10:40,140 --> 00:10:42,880
ing your desires sometimes, I

123
00:10:42,880 --> 00:10:50,610
 never see the problem with it until one day bam, lost and

124
00:10:50,610 --> 00:10:53,160
 disturbed by loss.

125
00:10:53,160 --> 00:10:56,050
 Disturbed by gain because you can't just sit here when you

126
00:10:56,050 --> 00:10:57,640
 want something, you have to

127
00:10:57,640 --> 00:11:00,080
 go and get it.

128
00:11:00,080 --> 00:11:06,290
 When you do sit and meditate, you're disturbed constantly

129
00:11:06,290 --> 00:11:08,600
 by things you want.

130
00:11:08,600 --> 00:11:16,470
 Gain, loss, disturbed by not getting, or by losing the

131
00:11:16,470 --> 00:11:21,000
 things we want and not being able

132
00:11:21,000 --> 00:11:24,040
 to get them.

133
00:11:24,040 --> 00:11:28,280
 We're afraid of losing money.

134
00:11:28,280 --> 00:11:32,160
 We're afraid of losing possessions.

135
00:11:32,160 --> 00:11:36,600
 We're devastated by loss.

136
00:11:36,600 --> 00:11:49,080
 Loss of wealth, loss of health, loss of relatives, friends.

137
00:11:49,080 --> 00:12:00,920
 We put our, put all of our eggs in one basket, I guess.

138
00:12:00,920 --> 00:12:05,520
 Put our eggs in the basket of the world.

139
00:12:05,520 --> 00:12:13,960
 We put all of our faith and all of our, we wager all of our

140
00:12:13,960 --> 00:12:19,000
 happiness on externalities.

141
00:12:19,000 --> 00:12:24,400
 Our happiness depends upon things which are not dependable.

142
00:12:24,400 --> 00:12:27,080
 The world is not dependable.

143
00:12:27,080 --> 00:12:31,970
 A big part of what the Buddha tried to teach, what he saw

144
00:12:31,970 --> 00:12:34,720
 and what he focused on and what

145
00:12:34,720 --> 00:12:41,820
 he tried to impress upon his students, impermanence,

146
00:12:41,820 --> 00:12:46,600
 uncertainty, that the nature of the world

147
00:12:46,600 --> 00:12:52,160
 is a full spectrum as long as there's only part of what's

148
00:12:52,160 --> 00:12:55,160
 possible that is acceptable

149
00:12:55,160 --> 00:12:56,160
 to you.

150
00:12:56,160 --> 00:13:03,860
 You're always going to be weak, vulnerable, until you can

151
00:13:03,860 --> 00:13:07,200
 be at peace and at ease with

152
00:13:07,200 --> 00:13:12,600
 the full spectrum of reality, whatever happens.

153
00:13:12,600 --> 00:13:17,160
 Unshaken, right?

154
00:13:17,160 --> 00:13:31,520
 You'll never truly be at peace until you come to that.

155
00:13:31,520 --> 00:13:34,200
 Those are the first two Lokadamas.

156
00:13:34,200 --> 00:13:40,660
 The second one is, at one extreme we have fame, and the

157
00:13:40,660 --> 00:13:44,280
 other one we have, what's the

158
00:13:44,280 --> 00:13:45,280
 opposite of fame?

159
00:13:45,280 --> 00:13:54,820
 It's something about, I guess, obscurity is perhaps the

160
00:13:54,820 --> 00:13:56,840
 best one.

161
00:13:56,840 --> 00:13:59,080
 But it's misleading to say obscurity.

162
00:13:59,080 --> 00:14:02,830
 It really means not having any friends, not having anyone

163
00:14:02,830 --> 00:14:03,680
 know you.

164
00:14:03,680 --> 00:14:09,560
 Not having any connections.

165
00:14:09,560 --> 00:14:10,560
 Being alone.

166
00:14:10,560 --> 00:14:19,200
 How dis-- Anyone who knows your name, to be alone in the

167
00:14:19,200 --> 00:14:20,000
 world.

168
00:14:20,000 --> 00:14:24,520
 We're disturbed by fame, by how many friends we have, how

169
00:14:24,520 --> 00:14:26,960
 many people know who we are.

170
00:14:26,960 --> 00:14:37,800
 We're moved by our desire for relations, our desire to be

171
00:14:37,800 --> 00:14:43,080
 well known, our desire to be

172
00:14:43,080 --> 00:14:44,080
 famous.

173
00:14:44,080 --> 00:14:51,220
 On many levels, it refers simply to having friends, having

174
00:14:51,220 --> 00:14:55,520
 associates and people surrounding

175
00:14:55,520 --> 00:14:56,760
 you.

176
00:14:56,760 --> 00:15:01,500
 It can be quite disturbing to people when they don't have

177
00:15:01,500 --> 00:15:04,040
 any friends or when no one knows

178
00:15:04,040 --> 00:15:07,320
 them, knows who they are.

179
00:15:07,320 --> 00:15:09,970
 Being a stranger in a strange land when you travel to

180
00:15:09,970 --> 00:15:12,080
 another country and really just feeling

181
00:15:12,080 --> 00:15:13,240
 alone.

182
00:15:13,240 --> 00:15:16,750
 If you've been surrounded by people who you know, you never

183
00:15:16,750 --> 00:15:18,400
 realize this until you go to

184
00:15:18,400 --> 00:15:21,120
 another country and realize, "I'm all alone.

185
00:15:21,120 --> 00:15:26,520
 I don't have anyone here."

186
00:15:26,520 --> 00:15:31,660
 But it gets worse when people really want to be famous,

187
00:15:31,660 --> 00:15:32,640
 right?

188
00:15:32,640 --> 00:15:36,880
 And how fickle it can be.

189
00:15:36,880 --> 00:15:46,800
 You can be famous one day and no one knows who you are the

190
00:15:46,800 --> 00:15:48,480
 next.

191
00:15:48,480 --> 00:15:52,560
 The most devastating I think is feeling friendless, feeling

192
00:15:52,560 --> 00:15:55,120
 like you have no one, no one to support

193
00:15:55,120 --> 00:15:58,560
 you, no one who cares.

194
00:15:58,560 --> 00:16:02,500
 I think we feel this acutely as meditators.

195
00:16:02,500 --> 00:16:06,800
 Sometimes those people who I talk to who really become keen

196
00:16:06,800 --> 00:16:09,520
ly interested in Buddhism and find

197
00:16:09,520 --> 00:16:14,360
 there's no one around.

198
00:16:14,360 --> 00:16:18,470
 I always think back to this one woman who emailed me and we

199
00:16:18,470 --> 00:16:20,440
 got into a discussion.

200
00:16:20,440 --> 00:16:25,480
 She was living in a house and they literally wouldn't let

201
00:16:25,480 --> 00:16:27,080
 her meditate.

202
00:16:27,080 --> 00:16:34,120
 Evil is satanic and so on.

203
00:16:34,120 --> 00:16:37,480
 I don't think that's a unique situation.

204
00:16:37,480 --> 00:16:42,130
 Being alone in the practice of the Dhamma can be quite

205
00:16:42,130 --> 00:16:43,560
 disturbing.

206
00:16:43,560 --> 00:16:44,560
 It shouldn't be.

207
00:16:44,560 --> 00:16:47,560
 I mean, these shouldn't be.

208
00:16:47,560 --> 00:16:50,720
 The problem isn't the experiences, you see.

209
00:16:50,720 --> 00:16:54,520
 Gain and loss are not the problem.

210
00:16:54,520 --> 00:16:59,360
 The problem is being disturbed by them, having lots of

211
00:16:59,360 --> 00:17:01,760
 friends, being alone.

212
00:17:01,760 --> 00:17:06,160
 You practice meditation for a while and you'll just be

213
00:17:06,160 --> 00:17:09,240
 perfectly at ease and prefer for the

214
00:17:09,240 --> 00:17:14,400
 most part to be alone, which is quite amazing really.

215
00:17:14,400 --> 00:17:17,600
 I think it's quite a surprise for people who just needed to

216
00:17:17,600 --> 00:17:19,240
 be around others and needed

217
00:17:19,240 --> 00:17:30,670
 the support and connection to just feel incredibly

218
00:17:30,670 --> 00:17:38,160
 comfortable just being alone.

219
00:17:38,160 --> 00:17:48,050
 The third set is associated, but this is perhaps more

220
00:17:48,050 --> 00:17:53,360
 poignant, more pointed.

221
00:17:53,360 --> 00:17:59,200
 Praise and blame.

222
00:17:59,200 --> 00:18:15,840
 This one certainly is a cause for great disturbance.

223
00:18:15,840 --> 00:18:22,260
 We talk about wavering, how people are disturbed, how we

224
00:18:22,260 --> 00:18:26,440
 are disturbed by praise and blame.

225
00:18:26,440 --> 00:18:31,150
 It's a good one to talk about because it's a good way to

226
00:18:31,150 --> 00:18:34,120
 test yourself when you're praised

227
00:18:34,120 --> 00:18:37,880
 and when you're blamed.

228
00:18:37,880 --> 00:18:51,280
 It's always been quite interesting for me being on YouTube

229
00:18:51,280 --> 00:18:51,880
 and having

230
00:18:51,880 --> 00:18:55,110
 YouTube comments where people will tell me I've changed

231
00:18:55,110 --> 00:18:56,680
 their life and they think I'm

232
00:18:56,680 --> 00:18:59,200
 just the cat's meow.

233
00:18:59,200 --> 00:19:02,750
 The next day, the next hour, you'll even get a message

234
00:19:02,750 --> 00:19:04,680
 saying, "I'm a hustler.

235
00:19:04,680 --> 00:19:05,680
 I'm a fraud.

236
00:19:05,680 --> 00:19:11,560
 I must be selling snake oil or something."

237
00:19:11,560 --> 00:19:16,360
 Many different kinds of opinion, but that's nothing really.

238
00:19:16,360 --> 00:19:21,080
 I don't have to deal with half the things that people out

239
00:19:21,080 --> 00:19:22,200
 there do.

240
00:19:22,200 --> 00:19:26,930
 Daily people can be confronted by other people telling them

241
00:19:26,930 --> 00:19:29,560
 you're useless, you're ugly,

242
00:19:29,560 --> 00:19:35,550
 you're dumb, you're hopeless, and not in the good way that

243
00:19:35,550 --> 00:19:39,360
 we talked about in the bad way.

244
00:19:39,360 --> 00:19:40,360
 You're a loser.

245
00:19:40,360 --> 00:19:45,360
 You're a bum.

246
00:19:45,360 --> 00:19:50,110
 Someone came recently and told me that her daughter was

247
00:19:50,110 --> 00:19:52,040
 told she was brown.

248
00:19:52,040 --> 00:19:54,760
 She shouldn't show her legs.

249
00:19:54,760 --> 00:19:57,480
 She shouldn't wear shorts because people would be turned

250
00:19:57,480 --> 00:19:58,680
 off by her brown skin.

251
00:19:58,680 --> 00:20:09,440
 Apparently, that actually happens still in this world.

252
00:20:09,440 --> 00:20:11,920
 How devastated we become, right?

253
00:20:11,920 --> 00:20:15,660
 Someone calls you fat and you're maybe a little bigger than

254
00:20:15,660 --> 00:20:16,720
 most people.

255
00:20:16,720 --> 00:20:20,160
 Maybe you have more fat than on your body than most people.

256
00:20:20,160 --> 00:20:22,080
 It can be quite hurtful.

257
00:20:22,080 --> 00:20:29,850
 There's this big movement now, people getting very angry at

258
00:20:29,850 --> 00:20:33,600
 how sensitive people are.

259
00:20:33,600 --> 00:20:36,040
 This is an interesting discussion.

260
00:20:36,040 --> 00:20:41,240
 There's truth to it, the fact that we are oversensitive.

261
00:20:41,240 --> 00:20:53,320
 If you identify as a female and people call you he because,

262
00:20:53,320 --> 00:20:55,800
 well, you happen to have Y

263
00:20:55,800 --> 00:21:03,160
 chromosomes, you get very upset about that.

264
00:21:03,160 --> 00:21:07,240
 They would say, "Stop being such a snowflake."

265
00:21:07,240 --> 00:21:09,720
 This is the word they use.

266
00:21:09,720 --> 00:21:16,460
 It becomes quite violent and these people can be quite

267
00:21:16,460 --> 00:21:22,000
 vitriolic and hurtful.

268
00:21:22,000 --> 00:21:26,190
 There is a point there that sensitivity is you've only got

269
00:21:26,190 --> 00:21:28,320
 yourself to blame if you're

270
00:21:28,320 --> 00:21:29,320
 sensitive.

271
00:21:29,320 --> 00:21:37,380
 Again, trying to get everyone to say the right thing, use

272
00:21:37,380 --> 00:21:40,160
 the right words.

273
00:21:40,160 --> 00:21:43,000
 Black people are going to be called horrible things.

274
00:21:43,000 --> 00:21:44,760
 This word that no one wants to say.

275
00:21:44,760 --> 00:21:47,240
 You're going to have that.

276
00:21:47,240 --> 00:21:53,080
 Mexican people, Asian people, we're all called.

277
00:21:53,080 --> 00:21:54,080
 I get called white.

278
00:21:54,080 --> 00:21:57,760
 I don't know, that's not nearly as harmful as hurtful, but

279
00:21:57,760 --> 00:21:59,520
 people certainly mean it in

280
00:21:59,520 --> 00:22:00,520
 a derogatory sense.

281
00:22:00,520 --> 00:22:03,160
 They mean it in a harmful sense.

282
00:22:03,160 --> 00:22:06,560
 You can't be a real Buddhist because you're white.

283
00:22:06,560 --> 00:22:20,800
 I was arrested in California constantly on me.

284
00:22:20,800 --> 00:22:25,160
 One Hispanic man said to me, he said, "If you had Asian

285
00:22:25,160 --> 00:22:27,560
 skin, if you looked Asian, they

286
00:22:27,560 --> 00:22:35,040
 wouldn't bother you."

287
00:22:35,040 --> 00:22:39,980
 We get ... Obviously, I'm in a place of privilege,

288
00:22:39,980 --> 00:22:41,320
 especially in this country.

289
00:22:41,320 --> 00:22:47,360
 I get far less of it than people of other.

290
00:22:47,360 --> 00:22:48,360
 I'm not fat.

291
00:22:48,360 --> 00:22:49,360
 I'm not short.

292
00:22:49,360 --> 00:22:54,080
 I'm not a lot of things that people get bullied for.

293
00:22:54,080 --> 00:23:00,680
 But, we have to ... In Buddhism, this is our practice.

294
00:23:00,680 --> 00:23:05,680
 What I mean to say is that it's all wrong, of course.

295
00:23:05,680 --> 00:23:09,410
 Anyone who says, "Honestly, I agree that if someone wants

296
00:23:09,410 --> 00:23:11,280
 to be called she, her, he,

297
00:23:11,280 --> 00:23:15,440
 him, or something else entirely, then go for it."

298
00:23:15,440 --> 00:23:18,200
 The power, fine.

299
00:23:18,200 --> 00:23:25,280
 It should be our responsibility to accommodate them.

300
00:23:25,280 --> 00:23:29,800
 It's not like this is something that puts us out.

301
00:23:29,800 --> 00:23:36,700
 It's wrong to be angry and hateful towards such people who

302
00:23:36,700 --> 00:23:39,120
 have this identity.

303
00:23:39,120 --> 00:23:47,510
 But, whatever side of the fence of that debate you fall on,

304
00:23:47,510 --> 00:23:51,360
 wherever you fall, in the end,

305
00:23:51,360 --> 00:23:56,270
 we only have ourselves to blame because we can't control

306
00:23:56,270 --> 00:23:57,840
 other people.

307
00:23:57,840 --> 00:23:58,840
 We're vulnerable.

308
00:23:58,840 --> 00:24:00,680
 We're sensitive.

309
00:24:00,680 --> 00:24:05,560
 We're weak.

310
00:24:05,560 --> 00:24:10,640
 It's not a put down or an insult.

311
00:24:10,640 --> 00:24:13,370
 It's totally understandable because, for the most part, we

312
00:24:13,370 --> 00:24:14,800
 don't have any way of becoming

313
00:24:14,800 --> 00:24:15,800
 strong.

314
00:24:15,800 --> 00:24:23,640
 We don't have any way of resisting.

315
00:24:23,640 --> 00:24:25,880
 You can't blame the victim.

316
00:24:25,880 --> 00:24:31,560
 I mean, actually, ultimately, you can.

317
00:24:31,560 --> 00:24:35,270
 But, rather than exactly the victim, you blame the

318
00:24:35,270 --> 00:24:38,200
 situation that they're in, the state that

319
00:24:38,200 --> 00:24:39,200
 they're in.

320
00:24:39,200 --> 00:24:41,040
 We blame the state that we're in.

321
00:24:41,040 --> 00:24:43,800
 We're all in wretched state.

322
00:24:43,800 --> 00:24:45,440
 We're all vulnerable.

323
00:24:45,440 --> 00:24:48,880
 We're all weak.

324
00:24:48,880 --> 00:24:50,400
 We're not invincible.

325
00:24:50,400 --> 00:24:53,760
 It's ultimately our fault.

326
00:24:53,760 --> 00:24:55,800
 We get put into these situations.

327
00:24:55,800 --> 00:24:59,120
 I am blaming the victim, aren't I?

328
00:24:59,120 --> 00:25:00,120
 No.

329
00:25:00,120 --> 00:25:02,450
 I mean, the reason we get put in these situations is really

330
00:25:02,450 --> 00:25:03,800
 because we're the bullies.

331
00:25:03,800 --> 00:25:05,040
 We've been the bullies.

332
00:25:05,040 --> 00:25:06,040
 We bully.

333
00:25:06,040 --> 00:25:12,480
 And because we bully, we get caught up in loops of revenge.

334
00:25:12,480 --> 00:25:18,660
 We get caught up in-- we live in this world because of how

335
00:25:18,660 --> 00:25:20,600
 we've become.

336
00:25:20,600 --> 00:25:25,740
 The reason a person perhaps strongly identifies as a female

337
00:25:25,740 --> 00:25:28,680
, even though they're in the body

338
00:25:28,680 --> 00:25:31,590
 of a male, or strongly identifies as a male, even though

339
00:25:31,590 --> 00:25:34,560
 they're in the body of a female.

340
00:25:34,560 --> 00:25:38,950
 It has much to do with this going back and forth, not being

341
00:25:38,950 --> 00:25:40,880
 able to decide from life

342
00:25:40,880 --> 00:25:45,440
 to life, which is probably most of us.

343
00:25:45,440 --> 00:25:47,910
 Going back and forth from being male and female and not

344
00:25:47,910 --> 00:25:49,600
 deciding being a male and thinking,

345
00:25:49,600 --> 00:25:53,540
 oh, it'd be nice to be a female, being a female, back and

346
00:25:53,540 --> 00:25:54,400
 forth.

347
00:25:54,400 --> 00:25:57,280
 I met a-- and there's this woman.

348
00:25:57,280 --> 00:26:00,900
 I don't know if she still works there, but works at this

349
00:26:00,900 --> 00:26:02,480
 meditation center.

350
00:26:02,480 --> 00:26:05,920
 And she was really helpful and really nice and kind.

351
00:26:05,920 --> 00:26:06,920
 And we worked well together.

352
00:26:06,920 --> 00:26:08,320
 And I was teaching meditation.

353
00:26:08,320 --> 00:26:11,850
 And she was-- every time I went to this place, or quite

354
00:26:11,850 --> 00:26:15,960
 often, she would invite me to teach.

355
00:26:15,960 --> 00:26:19,440
 I went to another section in Bangkok and what Mahatad had.

356
00:26:19,440 --> 00:26:25,360
 And I went to another section talking to the monk about,

357
00:26:25,360 --> 00:26:26,200
 yeah.

358
00:26:26,200 --> 00:26:28,320
 Or she came to see them or something.

359
00:26:28,320 --> 00:26:32,080
 Came to see my teacher when he was there, I think.

360
00:26:32,080 --> 00:26:34,280
 When she left, he said, yeah, you know that woman?

361
00:26:34,280 --> 00:26:37,240
 I said, yeah, I was teaching there.

362
00:26:37,240 --> 00:26:38,240
 And she was helping me.

363
00:26:38,240 --> 00:26:42,520
 And we used to be monks together, he said.

364
00:26:42,520 --> 00:26:47,720
 I had no idea.

365
00:26:47,720 --> 00:26:53,480
 But this is how we find ourselves in these situations, why

366
00:26:53,480 --> 00:26:55,400
 we find ourselves.

367
00:26:55,400 --> 00:27:00,250
 I'm living in Asia as a white person, subject to a lot of

368
00:27:00,250 --> 00:27:01,760
 discrimination.

369
00:27:01,760 --> 00:27:04,880
 So it's not that I don't know these situations.

370
00:27:04,880 --> 00:27:12,590
 And we put ourselves in-- we get into these situations

371
00:27:12,590 --> 00:27:18,880
 where we're discriminated against.

372
00:27:18,880 --> 00:27:24,520
 People say nasty things to us.

373
00:27:24,520 --> 00:27:26,280
 That's not really what I wanted to say.

374
00:27:26,280 --> 00:27:29,230
 Because the whole idea of karma and how we got to where we

375
00:27:29,230 --> 00:27:31,040
 are is not the most important.

376
00:27:31,040 --> 00:27:37,000
 What's most important is how we react to it.

377
00:27:37,000 --> 00:27:39,720
 So the strength-- it's a good example.

378
00:27:39,720 --> 00:27:45,100
 It's a good thing to bring this up because strength and inv

379
00:27:45,100 --> 00:27:47,800
ulnerability doesn't come

380
00:27:47,800 --> 00:27:50,840
 from changing your karma.

381
00:27:50,840 --> 00:27:55,170
 Just really, the takeaway from that should just be that

382
00:27:55,170 --> 00:27:57,680
 there should be no doubt or no

383
00:27:57,680 --> 00:28:01,760
 confusion about why we're in the situation we're in.

384
00:28:01,760 --> 00:28:04,380
 But the answer isn't to change it.

385
00:28:04,380 --> 00:28:07,550
 So the answer is not to say, OK, well, if I'm really nice

386
00:28:07,550 --> 00:28:09,200
 to people and really humble

387
00:28:09,200 --> 00:28:14,200
 and really patient and so on, everyone will be nice to me.

388
00:28:14,200 --> 00:28:16,040
 And that's the answer, right?

389
00:28:16,040 --> 00:28:17,040
 Praise.

390
00:28:17,040 --> 00:28:18,160
 I'll just get praise.

391
00:28:18,160 --> 00:28:20,560
 I'll do whatever I can to just be praised.

392
00:28:20,560 --> 00:28:22,960
 It's a very dangerous road, right?

393
00:28:22,960 --> 00:28:30,240
 If you're constantly seeking praise, that's even worse.

394
00:28:30,240 --> 00:28:35,400
 The worst, of course, it sets you up for disappointment.

395
00:28:35,400 --> 00:28:37,360
 People don't praise you.

396
00:28:37,360 --> 00:28:42,800
 Even you don't get blamed for anything or insulted.

397
00:28:42,800 --> 00:28:44,920
 Just not have you craving the praise.

398
00:28:44,920 --> 00:28:50,090
 I mean, it's a great test because we're praised and blamed

399
00:28:50,090 --> 00:28:57,800
 and we're not praised and not blamed.

400
00:28:57,800 --> 00:29:05,110
 How we treat, how we react, how we respond is quite

401
00:29:05,110 --> 00:29:07,200
 important.

402
00:29:07,200 --> 00:29:11,600
 So our meditation, I mean, this is all important.

403
00:29:11,600 --> 00:29:14,510
 It's important discussion for our meditation because this

404
00:29:14,510 --> 00:29:16,000
 is what we're trying to do.

405
00:29:16,000 --> 00:29:18,440
 We're not trying to change our situation.

406
00:29:18,440 --> 00:29:21,970
 We're trying to become strong when we like something or

407
00:29:21,970 --> 00:29:24,000
 when we dislike something, when

408
00:29:24,000 --> 00:29:30,800
 we're moved and upset by the world.

409
00:29:30,800 --> 00:29:34,760
 The final pair, of course, is the most basic.

410
00:29:34,760 --> 00:29:39,000
 And that's, it really relates to the rest of them, I think.

411
00:29:39,000 --> 00:29:44,400
 It's happiness and unhappiness or pleasure and pain.

412
00:29:44,400 --> 00:29:50,960
 Pleasure and pain is probably the best way to understand it

413
00:29:50,960 --> 00:29:51,160
.

414
00:29:51,160 --> 00:29:52,160
 Pleasure.

415
00:29:52,160 --> 00:29:55,080
 We get pleasure out of all of the other things, right?

416
00:29:55,080 --> 00:29:57,200
 Praise brings us pleasure.

417
00:29:57,200 --> 00:30:02,480
 Friends, fame brings us pleasure.

418
00:30:02,480 --> 00:30:03,640
 Gain brings us pleasure.

419
00:30:03,640 --> 00:30:08,080
 But there's just pleasure, basic pleasure as well.

420
00:30:08,080 --> 00:30:14,310
 Soft seats give us pleasure, nice beds, good food, food

421
00:30:14,310 --> 00:30:18,240
 itself, just food and nourishment

422
00:30:18,240 --> 00:30:20,800
 gives us this warm and full feeling.

423
00:30:20,800 --> 00:30:28,290
 It certainly takes away the pain and anguish of being

424
00:30:28,290 --> 00:30:29,880
 hungry.

425
00:30:29,880 --> 00:30:33,320
 Pleasure seekers.

426
00:30:33,320 --> 00:30:34,520
 It's really not a deep teaching.

427
00:30:34,520 --> 00:30:38,640
 I mean, it shouldn't be hard to understand, but it is.

428
00:30:38,640 --> 00:30:41,400
 It is because we're taught something quite different.

429
00:30:41,400 --> 00:30:46,920
 But it shouldn't be hard to understand that pleasure

430
00:30:46,920 --> 00:30:50,680
 seeking isn't a viable means to find

431
00:30:50,680 --> 00:30:52,720
 happiness.

432
00:30:52,720 --> 00:30:58,990
 Always getting what you want is not something anyone ever

433
00:30:58,990 --> 00:31:01,680
 recommends, right?

434
00:31:01,680 --> 00:31:07,630
 So we tend instead to live in mediocrity or in some kind of

435
00:31:07,630 --> 00:31:10,520
 liminal state where we're

436
00:31:10,520 --> 00:31:12,160
 neither happy nor unhappy.

437
00:31:12,160 --> 00:31:15,320
 We're not at peace with ourselves.

438
00:31:15,320 --> 00:31:22,070
 We're able to sustain a low grade happiness for the most

439
00:31:22,070 --> 00:31:25,200
 part by not trying to be too

440
00:31:25,200 --> 00:31:29,000
 happy.

441
00:31:29,000 --> 00:31:32,620
 Usually as Buddhists, we can, you see a lot of Buddhists

442
00:31:32,620 --> 00:31:35,160
 who live simple lives understanding

443
00:31:35,160 --> 00:31:39,220
 that getting caught up in too much pleasure is going to

444
00:31:39,220 --> 00:31:40,920
 lead to suffering.

445
00:31:40,920 --> 00:31:45,150
 And so they engage in pleasurable activities, but they don

446
00:31:45,150 --> 00:31:47,240
't obsess about them and they

447
00:31:47,240 --> 00:31:52,050
 have an awareness, a mindfulness of how dangerous it can be

448
00:31:52,050 --> 00:31:54,840
 to obsess about pleasure and seek

449
00:31:54,840 --> 00:31:57,840
 it out.

450
00:31:57,840 --> 00:32:05,000
 But they're still vulnerable.

451
00:32:05,000 --> 00:32:07,240
 We're still vulnerable.

452
00:32:07,240 --> 00:32:14,600
 It's a sort of a pleasure and I'll bear with some pain.

453
00:32:14,600 --> 00:32:16,840
 It's not really a solution though.

454
00:32:16,840 --> 00:32:17,840
 It's not strong.

455
00:32:17,840 --> 00:32:19,360
 It's still weak.

456
00:32:19,360 --> 00:32:25,420
 You're still moved by pleasure and pain.

457
00:32:25,420 --> 00:32:26,520
 Most of us are like this.

458
00:32:26,520 --> 00:32:30,970
 Most of us are not addicts in the strong sense of the word,

459
00:32:30,970 --> 00:32:33,520
 but we're still moved by pain.

460
00:32:33,520 --> 00:32:35,120
 We don't like it.

461
00:32:35,120 --> 00:32:36,120
 We're weak.

462
00:32:36,120 --> 00:32:39,520
 We're susceptible to pain and pleasure as well.

463
00:32:39,520 --> 00:32:43,160
 Hey, who doesn't want to feel pleasure, right?

464
00:32:43,160 --> 00:32:44,360
 Weakness.

465
00:32:44,360 --> 00:32:45,360
 This is weakness.

466
00:32:45,360 --> 00:32:48,520
 It literally is.

467
00:32:48,520 --> 00:32:54,340
 It's not trying to be strict or mean about it, but

468
00:32:54,340 --> 00:32:59,720
 literally in a literal sense is weakness.

469
00:32:59,720 --> 00:33:02,960
 That's the point.

470
00:33:02,960 --> 00:33:05,120
 That it would be stronger.

471
00:33:05,120 --> 00:33:11,360
 We would be much stronger if we were at peace with pleasure

472
00:33:11,360 --> 00:33:14,440
 and pain, unmoved by it.

473
00:33:14,440 --> 00:33:20,440
 When pleasure came, it was just another experience.

474
00:33:20,440 --> 00:33:22,840
 When pain comes, it's just another experience.

475
00:33:22,840 --> 00:33:28,380
 When our happiness, our peace, our tranquility, our

476
00:33:28,380 --> 00:33:32,800
 stability of mind, it's not dependent.

477
00:33:32,800 --> 00:33:37,200
 It's not influenced by our experiences.

478
00:33:37,200 --> 00:33:44,600
 It's the only way because experiences are unpredictable.

479
00:33:44,600 --> 00:33:49,640
 You can predict and you can control for short periods, but

480
00:33:49,640 --> 00:33:51,040
 not forever.

481
00:33:51,040 --> 00:33:58,240
 It's not a solution.

482
00:33:58,240 --> 00:34:02,080
 This should help to understand a little clearer about why

483
00:34:02,080 --> 00:34:04,280
 we're doing what we're doing.

484
00:34:04,280 --> 00:34:08,160
 Why are we sitting, repeating to ourselves, rising, falling

485
00:34:08,160 --> 00:34:08,160
?

486
00:34:08,160 --> 00:34:11,050
 I think we overlook how powerful just that noting to

487
00:34:11,050 --> 00:34:13,080
 yourself, the stomach rising and

488
00:34:13,080 --> 00:34:14,960
 falling can be.

489
00:34:14,960 --> 00:34:20,910
 It's an exercise in strength, in invincibility, where you

490
00:34:20,910 --> 00:34:25,040
 experience something without reacting

491
00:34:25,040 --> 00:34:32,060
 to it, without judging it, without any kind of labels or

492
00:34:32,060 --> 00:34:36,160
 value judgment of good or bad.

493
00:34:36,160 --> 00:34:37,480
 You want to be at peace.

494
00:34:37,480 --> 00:34:42,680
 You want to be safe, invincible, strong.

495
00:34:42,680 --> 00:34:44,160
 This is the way to go.

496
00:34:44,160 --> 00:34:50,100
 Learn how to see things as they are, how to live your life,

497
00:34:50,100 --> 00:34:52,640
 how to be alive and not be

498
00:34:52,640 --> 00:35:00,220
 bogged down by judging, judging everything moment after

499
00:35:00,220 --> 00:35:03,560
 moment after moment.

500
00:35:03,560 --> 00:35:08,400
 Judge everything.

501
00:35:08,400 --> 00:35:13,800
 So the lokadamas are always something useful to go over and

502
00:35:13,800 --> 00:35:16,600
 the over the general teaching

503
00:35:16,600 --> 00:35:23,640
 of the weakness, how they are a test for us.

504
00:35:23,640 --> 00:35:28,130
 They test us constantly and you can judge and you can

505
00:35:28,130 --> 00:35:30,800
 measure your strength against

506
00:35:30,800 --> 00:35:34,880
 these things, against the vicissitudes of life.

507
00:35:34,880 --> 00:35:37,520
 You want to know what it means to be enlightened?

508
00:35:37,520 --> 00:35:39,480
 How enlightened am I?

509
00:35:39,480 --> 00:35:43,820
 I am as enlightened as I can withstand the vicissitudes of

510
00:35:43,820 --> 00:35:46,000
 life without being moved by

511
00:35:46,000 --> 00:35:54,520
 them, without being upset for or against them.

512
00:35:54,520 --> 00:35:58,800
 So that's the dhamma for tonight.

513
00:35:58,800 --> 00:36:00,480
 Thank you all for tuning in.

514
00:36:00,480 --> 00:36:02,560
 Have a good night.

515
00:36:02,560 --> 00:36:03,560
 Thank you.

516
00:36:03,560 --> 00:36:19,640
 [

