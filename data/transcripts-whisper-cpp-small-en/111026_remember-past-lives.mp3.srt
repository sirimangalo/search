1
00:00:00,000 --> 00:00:04,000
 Why don't we remember our past lives?

2
00:00:04,000 --> 00:00:10,520
 Let's give the simple answer first. The simple answer is

3
00:00:10,520 --> 00:00:14,040
 because you don't have the power to remember things that

4
00:00:14,040 --> 00:00:15,000
 far back.

5
00:00:15,000 --> 00:00:18,700
 It's just that simple. We have this idea, "Well, I was an

6
00:00:18,700 --> 00:00:21,000
 adult and I should remember all those things."

7
00:00:21,000 --> 00:00:26,700
 But this was before you were this fetus in your mother's

8
00:00:26,700 --> 00:00:31,000
 womb, which you also don't remember.

9
00:00:31,000 --> 00:00:33,960
 Everyone can verify for themselves they were in their

10
00:00:33,960 --> 00:00:45,000
 mother's womb. It's not really. There's ultrasounds and...

11
00:00:45,000 --> 00:00:49,970
 There's baby pictures. I have pictures of my birth. I was

12
00:00:49,970 --> 00:00:53,830
 born in our house at home, not in a hospital, and we have

13
00:00:53,830 --> 00:00:55,000
 pictures of it all.

14
00:00:55,000 --> 00:00:57,610
 So I'm pretty clear that I came out of my mother's womb,

15
00:00:57,610 --> 00:01:03,000
 but I don't remember it. No recollection whatsoever.

16
00:01:03,000 --> 00:01:12,980
 So that's the short answer, is that it's just too far back.

17
00:01:12,980 --> 00:01:17,000
 It's before all that. So the long answer is... or... the

18
00:01:17,000 --> 00:01:17,000
 long answer...

19
00:01:17,000 --> 00:01:22,000
 The long answer is that you can remember your past lives.

20
00:01:22,000 --> 00:01:25,310
 You just have to work at it, just like with any kind of

21
00:01:25,310 --> 00:01:26,000
 memory.

22
00:01:26,000 --> 00:01:28,410
 Now, we don't think of actually working and remembering

23
00:01:28,410 --> 00:01:31,000
 things because our minds don't work very well in general.

24
00:01:31,000 --> 00:01:33,970
 When your mind is untrained, when you haven't tried to

25
00:01:33,970 --> 00:01:38,000
 train your mind, you don't even think it's possible.

26
00:01:38,000 --> 00:01:42,650
 I think memory is something that comes by itself, right? P

27
00:01:42,650 --> 00:01:45,220
ops on at times, but you can actually train your mind, and

28
00:01:45,220 --> 00:01:47,000
 there are people who do it.

29
00:01:47,000 --> 00:01:51,730
 We all heard of these people with photographic memories or

30
00:01:51,730 --> 00:01:55,000
 people who memorize telephone books.

31
00:01:55,000 --> 00:01:59,720
 They train themselves by memorizing telephone books,

32
00:01:59,720 --> 00:02:02,000
 developing this memory.

33
00:02:02,000 --> 00:02:04,340
 Monks do it as well. They memorize all of the Buddha's

34
00:02:04,340 --> 00:02:09,000
 teaching. 45 volumes or 40-some volumes.

35
00:02:09,000 --> 00:02:13,400
 They brag about this, and we hear about these monks who

36
00:02:13,400 --> 00:02:15,000
 memorize it all.

37
00:02:15,000 --> 00:02:18,170
 And then I talked to a monk about it, and he said, "Oh no,

38
00:02:18,170 --> 00:02:20,430
 they never really remember it all. That's just what they

39
00:02:20,430 --> 00:02:21,000
 say."

40
00:02:21,000 --> 00:02:26,000
 He said, "You couldn't remember it all, but you get good.

41
00:02:26,000 --> 00:02:31,180
 You know where everything is, because the point is just to

42
00:02:31,180 --> 00:02:33,000
 be able to pass the exams.

43
00:02:33,000 --> 00:02:37,190
 And the examiner can't ask you everything, so you just have

44
00:02:37,190 --> 00:02:40,960
 to know what he's going to ask you, and you have to know

45
00:02:40,960 --> 00:02:45,260
 where everything is, and basically get the gist of the

46
00:02:45,260 --> 00:02:47,000
 entire tipidaka."

47
00:02:47,000 --> 00:02:51,860
 Anyway, so they do this, and that's still quite an

48
00:02:51,860 --> 00:02:55,000
 incredible accomplishment.

49
00:02:55,000 --> 00:02:58,910
 So the mind can be trained in memory, but it can also be

50
00:02:58,910 --> 00:03:03,000
 trained to remember things that you've forgotten.

51
00:03:03,000 --> 00:03:07,740
 It can be trained to go backwards, but you have to work at

52
00:03:07,740 --> 00:03:10,000
 it really, really hard.

53
00:03:10,000 --> 00:03:13,000
 And the work is in your meditation.

54
00:03:13,000 --> 00:03:16,950
 When you practice samatha meditation, you develop yourself,

55
00:03:16,950 --> 00:03:23,000
 as I was saying, switching objects, then switching jhanas,

56
00:03:23,000 --> 00:03:24,000
 then switching objects and jhanas.

57
00:03:24,000 --> 00:03:30,900
 So you'd enter into the first five jhanas with the earth k

58
00:03:30,900 --> 00:03:34,240
asina, and then you'd enter in the first five jhanas with

59
00:03:34,240 --> 00:03:37,910
 the air kasina, and then the earth element, air element,

60
00:03:37,910 --> 00:03:40,000
 water element, fire element.

61
00:03:40,000 --> 00:03:44,370
 And then you'd switch. You'd go first jhana, third jhana,

62
00:03:44,370 --> 00:03:46,000
 fifth jhana, or something like that.

63
00:03:46,000 --> 00:03:49,360
 Then you'd switch objects, first jhana, earth element,

64
00:03:49,360 --> 00:03:52,000
 first jhana, air element, first jhana.

65
00:03:52,000 --> 00:03:57,560
 And you'd get... you're actually playing this game, and it

66
00:03:57,560 --> 00:04:01,000
's scientific, the development, like check, check, check,

67
00:04:01,000 --> 00:04:02,000
 and going back and forth.

68
00:04:02,000 --> 00:04:09,530
 To the point that your mind is incredibly sharp and able to

69
00:04:09,530 --> 00:04:16,000
 direct itself in any direction that it chooses.

70
00:04:16,000 --> 00:04:23,640
 So you're able to develop these states of intense tranqu

71
00:04:23,640 --> 00:04:25,000
ility.

72
00:04:25,000 --> 00:04:31,110
 And then what you do is you sit down, and you think of what

73
00:04:31,110 --> 00:04:35,000
 you just did before you sat down.

74
00:04:35,000 --> 00:04:39,000
 And then you think about what you just did before that.

75
00:04:39,000 --> 00:04:42,110
 And then you think about what happened before that, before

76
00:04:42,110 --> 00:04:44,000
 that, and you go back like this.

77
00:04:44,000 --> 00:04:47,540
 As soon as you get stuck and can't remember anything, can't

78
00:04:47,540 --> 00:04:51,450
 remember something, you stop, and you develop the jhanas

79
00:04:51,450 --> 00:04:52,000
 again.

80
00:04:52,000 --> 00:04:54,710
 You develop your meditation, entering into the earth, kas

81
00:04:54,710 --> 00:04:57,230
ina, the earth element, air element, fire element, water

82
00:04:57,230 --> 00:04:58,000
 element.

83
00:04:58,000 --> 00:05:00,530
 And there's these methods. It's in the visuddhi maga you

84
00:05:00,530 --> 00:05:02,000
 can read about exactly.

85
00:05:02,000 --> 00:05:04,740
 I don't remember exactly what they do, but it's something

86
00:05:04,740 --> 00:05:07,700
 about going from one to the other, and then skipping, and

87
00:05:07,700 --> 00:05:12,000
 then going backwards and forwards and so on.

88
00:05:12,000 --> 00:05:14,760
 And you do that again, and then you come back and try and

89
00:05:14,760 --> 00:05:16,000
 start all over again.

90
00:05:16,000 --> 00:05:18,880
 Sit down, remember the first thing you did before you sat

91
00:05:18,880 --> 00:05:21,000
 down, and the first thing before that.

92
00:05:21,000 --> 00:05:28,680
 And with work, eventually you can go back days, months,

93
00:05:28,680 --> 00:05:30,000
 years.

94
00:05:30,000 --> 00:05:34,030
 Yeah, it's kind of like regressive hypnosis, except it's

95
00:05:34,030 --> 00:05:36,000
 incredibly systematic.

96
00:05:36,000 --> 00:05:39,830
 It's been systematized to the point where it's no longer

97
00:05:39,830 --> 00:05:41,000
 just chance.

98
00:05:41,000 --> 00:05:44,210
 And I know people do. I've heard of one man who took the

99
00:05:44,210 --> 00:05:47,180
 lazy route, and he was actually able to do this, and

100
00:05:47,180 --> 00:05:49,430
 actually able to remember something that seemed like past

101
00:05:49,430 --> 00:05:50,000
 life.

102
00:05:50,000 --> 00:05:54,080
 But he did sort of just a hypnosis, where he didn't go one

103
00:05:54,080 --> 00:05:57,000
 moment by one moment by one moment.

104
00:05:57,000 --> 00:06:00,300
 But technically speaking, that's what you should do. If you

105
00:06:00,300 --> 00:06:03,510
 really want to be clear about it, you have to be clear that

106
00:06:03,510 --> 00:06:06,880
 it's going in sequence, and remember back, back, back, back

107
00:06:06,880 --> 00:06:09,000
, back, all the way to your birth.

108
00:06:09,000 --> 00:06:13,560
 And then into the womb, to the point where you can remember

109
00:06:13,560 --> 00:06:16,910
 what was going on in the womb, all the way back to the

110
00:06:16,910 --> 00:06:18,000
 point of conception.

111
00:06:18,000 --> 00:06:21,000
 Now at the point of conception, it's much more difficult,

112
00:06:21,000 --> 00:06:27,000
 and this is why regressive hypnosis takes several sessions.

113
00:06:27,000 --> 00:06:31,830
 But as the texts say, eventually you can break through that

114
00:06:31,830 --> 00:06:35,460
, and once you've broken through to a past life, it gets

115
00:06:35,460 --> 00:06:38,700
 easier, and then you can go back further and further from

116
00:06:38,700 --> 00:06:40,000
 one life to the next, and so on.

117
00:06:40,000 --> 00:06:46,090
 Because the familiarity is there. The technique has been

118
00:06:46,090 --> 00:06:52,000
 learned, and the mind becomes familiar with the technique.

119
00:06:52,000 --> 00:06:55,360
 So there's the long answer. You can remember your past

120
00:06:55,360 --> 00:06:58,000
 lives. I mean, we don't remember them now.

121
00:06:58,000 --> 00:07:00,610
 It's an easy answer, because we can't remember that far

122
00:07:00,610 --> 00:07:04,000
 back, but that's just because of the state of our minds.

123
00:07:04,000 --> 00:07:07,440
 Because there's so much information that we're inundated

124
00:07:07,440 --> 00:07:11,000
 with, that we can barely remember what we did last week.

125
00:07:11,000 --> 00:07:14,690
 I can barely remember what I did this morning. But if you

126
00:07:14,690 --> 00:07:18,400
 think about it, you can develop it, and you can bring it

127
00:07:18,400 --> 00:07:19,000
 back.

128
00:07:19,000 --> 00:07:23,000
 So...

