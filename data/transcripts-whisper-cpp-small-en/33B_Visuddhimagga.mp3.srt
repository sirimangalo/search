1
00:00:00,000 --> 00:00:05,000
 So, I faculty, E faculty, no staying in Soha. So this is

2
00:00:05,000 --> 00:00:07,000
 the order given in Visodhi Maga.

3
00:00:07,000 --> 00:00:11,570
 It is a little different, the order is a little different

4
00:00:11,570 --> 00:00:14,120
 from that given in the manual of

5
00:00:14,120 --> 00:00:21,120
 Abhidhamma. And then the comedy that explains the words in

6
00:00:21,120 --> 00:00:21,120
 Pali, Chaku, Soha and Soha,

7
00:00:26,160 --> 00:00:33,160
 but they are referred back to the basis. But the last three

8
00:00:33,160 --> 00:00:33,160
, so please look at the last

9
00:00:33,160 --> 00:00:39,680
 three on the sheet, the fourth column, I share something

10
00:00:39,680 --> 00:00:42,280
 and final knowledge, faculty, final

11
00:00:42,280 --> 00:00:50,820
 lower faculty and so on. So we have understanding faculty,

12
00:00:50,820 --> 00:00:52,640
 I share etcetera faculty, final knowledge

13
00:00:54,160 --> 00:00:59,390
 faculty and final lower faculty. Now, the last three are

14
00:00:59,390 --> 00:01:01,160
 also understanding or panya.

15
00:01:01,160 --> 00:01:10,470
 So understanding is also panya and the last three are also

16
00:01:10,470 --> 00:01:12,880
 panya. And the first one I

17
00:01:12,880 --> 00:01:20,360
 share etcetera is panya at the moment of first Maga. So the

18
00:01:20,360 --> 00:01:20,360
 first one I share is panya at

19
00:01:21,000 --> 00:01:25,650
 the moment of first Maga. And final knowledge faculty means

20
00:01:25,650 --> 00:01:28,000
 panya at the moment of first

21
00:01:28,000 --> 00:01:34,220
 fruit and then second fruit, third fruit and third fruit

22
00:01:34,220 --> 00:01:38,080
 and fourth part. And final newer

23
00:01:46,520 --> 00:01:50,130
 faculty is the last or the fourth fruit consciousness

24
00:01:50,130 --> 00:01:53,520
 arising with the fourth fruit consciousness.

25
00:01:53,520 --> 00:02:03,560
 So the first one is first Maga, first part. The middle one

26
00:02:03,560 --> 00:02:08,880
 is the sixth in between, right?

27
00:02:11,960 --> 00:02:16,620
 And the last one is the fourth fruit. So they are given a

28
00:02:16,620 --> 00:02:18,960
 different name here, each is I

29
00:02:18,960 --> 00:02:28,810
 share etcetera, faculty, final knowledge faculty and final

30
00:02:28,810 --> 00:02:35,040
 lower faculty. So they are explained

31
00:02:35,040 --> 00:02:39,600
 in paragraph three. And then the what? Indria, Indria whose

32
00:02:39,600 --> 00:02:42,040
 English translation is faculty

33
00:02:42,040 --> 00:02:57,560
 is explained in paragraph four and paragraph five. Now, the

34
00:02:57,560 --> 00:02:57,560
 when the first fruit is the

35
00:02:57,560 --> 00:03:02,090
 word Indria, the common data just followed the aphorism

36
00:03:02,090 --> 00:03:04,560
 given in the Sanskrit grammar

37
00:03:04,560 --> 00:03:21,540
 by Banini. You can find the aphorism at the bottom of the

38
00:03:21,540 --> 00:03:27,080
 page 560. And the second one

39
00:03:27,640 --> 00:03:31,220
 there you see compare Banini 5293 Indriyan, Indralingam,

40
00:03:31,220 --> 00:03:34,640
 Indradristam, Indratustam, Indratatamitiwara.

41
00:03:34,640 --> 00:03:42,100
 So this is the aphorism found in the Sanskrit grammar of

42
00:03:42,100 --> 00:03:46,920
 Banini. And when explaining the

43
00:03:51,520 --> 00:03:54,660
 word Indria here, the common data or the Buddha goes to

44
00:03:54,660 --> 00:03:58,520
 follow this aphorism. Only the last

45
00:03:58,520 --> 00:04:07,880
 one is not taken, Indradatam. And the word Indria, there is

46
00:04:07,880 --> 00:04:07,880
 the word Indra, right? So

47
00:04:15,080 --> 00:04:19,690
 it is explained here that the word Indra or Indra means God

48
00:04:19,690 --> 00:04:22,080
 or Bedkama, Kusola or Aakuswakama.

49
00:04:22,080 --> 00:04:28,870
 Or it means the Buddha. So if you understand this, then you

50
00:04:28,870 --> 00:04:31,520
 will understand all. So in

51
00:04:31,520 --> 00:04:38,400
 the word Indria, we have the word Indra, Inda or Indra. And

52
00:04:38,400 --> 00:04:41,040
 Inda is explained here to me

53
00:04:43,120 --> 00:04:48,130
 two things. One is what? Kusola or Aakuswakama. And the

54
00:04:48,130 --> 00:04:50,120
 other is the Buddha. And then there

55
00:04:50,120 --> 00:05:00,260
 is the Ya, right? Or Iya in the word Indria. So the word,

56
00:05:00,260 --> 00:05:01,440
 the suffix Iya is explained to

57
00:05:01,440 --> 00:05:10,740
 me many things. That is what here. And the mark prepared by

58
00:05:10,740 --> 00:05:11,960
 the Buddha. And the mark

59
00:05:12,960 --> 00:05:17,400
 prepared by the Buddha. And then taught by, seen by, fost

60
00:05:17,400 --> 00:05:19,960
ered by. Now when it is the

61
00:05:19,960 --> 00:05:24,460
 mark or when we take the Iya to mean mark, then the word

62
00:05:24,460 --> 00:05:27,200
 Indira means karma. The mark

63
00:05:27,200 --> 00:05:34,130
 of karma is called faculty here. Because by looking at

64
00:05:34,130 --> 00:05:38,200
 everyone's eyes, you can see he

65
00:05:38,400 --> 00:05:42,720
 had good karma in the past or he had bad karma in the past.

66
00:05:42,720 --> 00:05:44,840
 Because good karma creates good

67
00:05:44,840 --> 00:05:49,400
 eye sensitivity and bad karma creates bad eye sensitivity.

68
00:05:49,400 --> 00:05:51,840
 Some people don't have to

69
00:05:51,840 --> 00:05:55,720
 use eye glasses all their lives. But some people have to

70
00:05:55,720 --> 00:05:57,840
 use from childhood. So that

71
00:05:57,840 --> 00:06:01,570
 depends on their karma. Because the eye sensitivity is the

72
00:06:01,570 --> 00:06:04,280
 result of karma. So we all had bad

73
00:06:04,280 --> 00:06:07,910
 karma in the past. As long as the eye sensitivity is

74
00:06:07,910 --> 00:06:11,280
 concerned. Because we have to use glasses.

75
00:06:11,280 --> 00:06:15,940
 But you had good karma. So we know good or bad karma from

76
00:06:15,940 --> 00:06:18,280
 the Iya and so on. So they are

77
00:06:18,280 --> 00:06:25,400
 called mark of the good or bad karma. But here it is

78
00:06:25,400 --> 00:06:29,880
 translated as mark of a ruler.

79
00:06:30,040 --> 00:06:34,330
 Ruler here means good or bad karma. The second meaning is

80
00:06:34,330 --> 00:06:37,040
 what? Prepared by karma. That means

81
00:06:37,040 --> 00:06:44,360
 made by or caused by karma. Ruler here means karma also.

82
00:06:44,360 --> 00:06:44,360
 Now the third meaning is being

83
00:06:44,360 --> 00:06:52,900
 taught by. So taught by, it is not taught by karma but

84
00:06:52,900 --> 00:06:57,940
 taught by Buddha. So the third

85
00:06:57,940 --> 00:07:02,840
 meaning is taught by, that which is taught by the Buddha.

86
00:07:02,840 --> 00:07:04,940
 And the fourth meaning is what?

87
00:07:04,940 --> 00:07:09,870
 Seen by. Again seen by the Buddha. And then the last

88
00:07:09,870 --> 00:07:12,400
 meaning is for start by. So for start

89
00:07:12,400 --> 00:07:17,440
 by the Buddha. That means ruler in his cultivation of

90
00:07:17,440 --> 00:07:20,960
 domain and some in his cultivation of development.

91
00:07:25,040 --> 00:07:29,530
 The cultivation of domain simply means he takes them as

92
00:07:29,530 --> 00:07:32,040
 object. And cultivation of development

93
00:07:32,040 --> 00:07:41,930
 means he develops them. So Buddha cultivated, these third

94
00:07:41,930 --> 00:07:42,960
 qualities were cultivated by the

95
00:07:52,480 --> 00:07:56,650
 Buddha. That is what they are called, say cultivated by the

96
00:07:56,650 --> 00:07:59,480
 Buddha. I mean in the Riya.

97
00:07:59,480 --> 00:08:04,320
 How did the Buddha cultivate them? Buddha cultivated them

98
00:08:04,320 --> 00:08:06,600
 in two ways. One is taking

99
00:08:06,600 --> 00:08:10,830
 them as object. That means when Buddha take Nibbana as

100
00:08:10,830 --> 00:08:13,600
 object and entered into attainment,

101
00:08:18,600 --> 00:08:23,150
 that means he is fostering Nibbana by way of taking it as

102
00:08:23,150 --> 00:08:25,600
 object. And at other times

103
00:08:25,600 --> 00:08:32,930
 he may be practicing or he may be in some other attainment.

104
00:08:32,930 --> 00:08:35,800
 And so in that case he is

105
00:08:35,800 --> 00:08:41,440
 cultivating, he is in the cultivation of development. So

106
00:08:41,440 --> 00:08:42,880
 Buddha, Buddha reacts or acts towards gamma

107
00:08:45,640 --> 00:08:51,180
 in two ways. Taking as object and also making them happen

108
00:08:51,180 --> 00:08:52,640
 in his mind. So for start by a

109
00:08:52,640 --> 00:08:57,550
 ruler. That means for start by the Buddha. So there are

110
00:08:57,550 --> 00:08:59,960
 already how many meanings actually.

111
00:08:59,960 --> 00:09:05,250
 Five meanings. The first meaning is what? Mark of karma.

112
00:09:05,250 --> 00:09:06,960
 The second meaning is prepared

113
00:09:06,960 --> 00:09:11,790
 by karma. The third meaning is taught by the Buddha. The

114
00:09:11,790 --> 00:09:14,240
 fourth meaning is seen by the

115
00:09:14,640 --> 00:09:21,640
 Buddha. The fifth meaning is for start by the Buddha. Now D

116
00:09:21,640 --> 00:09:21,640
 should be B right? In about

117
00:09:21,640 --> 00:09:31,760
 the middle of the paragraph five. Oh A B A D B C E. A D D C

118
00:09:31,760 --> 00:09:31,760
 E. A D D D C E. A D D C

119
00:09:31,760 --> 00:09:38,760
 E. No. I think he is following the sequence given there in

120
00:09:38,760 --> 00:09:38,760
 paragraph four. A B C and so

121
00:09:57,920 --> 00:10:04,920
 on. So his is also correct. So being the mark of a ruler,

122
00:10:04,920 --> 00:10:04,920
 having been prepared by a ruler.

123
00:10:04,920 --> 00:10:15,560
 That means by karma. And taught by a ruler, by the Buddha,

124
00:10:15,560 --> 00:10:20,080
 seen by the Buddha and first

125
00:10:21,560 --> 00:10:25,310
 at the Buddha. Right? And then the others are not difficult

126
00:10:25,310 --> 00:10:27,680
 to understand. Say characteristic

127
00:10:27,680 --> 00:10:34,250
 and so on and as to the order. And then as divided and und

128
00:10:34,250 --> 00:10:34,680
ivided. Barograph nine. Here

129
00:10:34,680 --> 00:10:40,280
 there is only division of the life faculty. Only life

130
00:10:40,280 --> 00:10:43,960
 faculty has division. The others

131
00:10:43,960 --> 00:10:47,960
 are only one. For that is too full as the material life

132
00:10:47,960 --> 00:10:50,320
 faculty and the immaterial life

133
00:10:50,320 --> 00:10:57,320
 faculty. You know there are two kinds of jivitas. Nama jiv

134
00:10:57,320 --> 00:10:57,320
ita and Rupa jivita. The mental jivita

135
00:10:57,320 --> 00:11:03,400
 and physical jivita. They are of two kinds and the others

136
00:11:03,400 --> 00:11:06,480
 are only one kind. There is

137
00:11:06,480 --> 00:11:12,290
 no division of the others. And then their function. Their

138
00:11:12,290 --> 00:11:13,960
 function is actually, since

139
00:11:19,400 --> 00:11:22,770
 they are called faculties or indriya. That means they are

140
00:11:22,770 --> 00:11:24,960
 predominant. So they make others

141
00:11:24,960 --> 00:11:29,370
 follow their wish. That is what is meant by the word indri

142
00:11:29,370 --> 00:11:31,960
ya. Indriya means making other

143
00:11:31,960 --> 00:11:37,490
 people follow his wish. That is indriya. Exercising his

144
00:11:37,490 --> 00:11:39,440
 wish over others. So here the I faculties

145
00:11:44,520 --> 00:11:48,720
 function is to cause by its own keenness, slowness etc. The

146
00:11:48,720 --> 00:11:51,200
 occurrence of I consciousness and

147
00:11:51,200 --> 00:11:56,110
 associated states etc. In a mode parallel to its own. That

148
00:11:56,110 --> 00:11:58,200
 means making them follow

149
00:11:58,200 --> 00:12:06,200
 its own mode. That means when you have a good I-sensitivity

150
00:12:06,200 --> 00:12:06,200
 you have good I-consciousness.

151
00:12:08,400 --> 00:12:12,690
 When you have bad I-sensitivity you have bad I-conscious

152
00:12:12,690 --> 00:12:15,400
ness. So the keenness or I don't

153
00:12:15,400 --> 00:12:21,260
 need slowness it is good here. It should be dullness. So

154
00:12:21,260 --> 00:12:24,040
 keenness and dullness etc. So

155
00:12:24,040 --> 00:12:31,650
 the keenness and dullness of the I-consciousness is

156
00:12:31,650 --> 00:12:37,640
 governed by the keenness and dullness of

157
00:12:38,640 --> 00:12:42,020
 I-sensitivity. That is why I-sensitivity has something like

158
00:12:42,020 --> 00:12:44,640
 authority over I-consciousness.

159
00:12:44,640 --> 00:12:49,340
 It is how it functions. So the meaning of indriya or

160
00:12:49,340 --> 00:12:51,640
 faculty is to exercise authority

161
00:12:51,640 --> 00:12:59,160
 over. Then to go down. The different functions are

162
00:12:59,160 --> 00:13:01,220
 mentioned in the

163
00:13:01,220 --> 00:13:08,220
 I-consciousness and dullness. So about the last four lines.

164
00:13:08,220 --> 00:13:08,220
 That of the final knowledge

165
00:13:08,220 --> 00:13:17,050
 faculty is both to attenuate and abandon respectively last

166
00:13:17,050 --> 00:13:20,820
 ill etc. and to subject co-neascent

167
00:13:20,820 --> 00:13:29,000
 states to its own mastery. Now final knowledge means the

168
00:13:29,000 --> 00:13:30,300
 knowledge between

169
00:13:31,260 --> 00:13:35,270
 the first maga and the last fruit. First maga, first fruit,

170
00:13:35,270 --> 00:13:38,260
 second maga, second fruit, fruit

171
00:13:38,260 --> 00:13:44,070
 and so on. So the first maga and the understanding commuted

172
00:13:44,070 --> 00:13:47,700
 when the first part is called I-shell

173
00:13:47,700 --> 00:13:59,500
 and so on. Then the knowledge or understanding of the

174
00:13:59,500 --> 00:14:06,500
 second maga is called final knowledge. So the final

175
00:14:06,500 --> 00:14:06,500
 knowledge faculty is both to attenuate

176
00:14:06,500 --> 00:14:16,490
 and abandon respectively last ill will etc. Attenuate

177
00:14:16,490 --> 00:14:21,940
 refers to the second part because

178
00:14:29,020 --> 00:14:33,450
 the second part does not eradicate any more mental defile

179
00:14:33,450 --> 00:14:36,020
ments but it makes the remaining

180
00:14:36,020 --> 00:14:40,750
 mental defilements weaker and weaker. So it attenuates. So

181
00:14:40,750 --> 00:14:43,500
 attenuate is for second maga

182
00:14:43,500 --> 00:14:48,470
 and abandon is for third and fourth maga because third maga

183
00:14:48,470 --> 00:14:51,220
 abandons sensual desire and ill

184
00:14:56,700 --> 00:15:01,090
 will altogether and then the fourth maga eradicates the

185
00:15:01,090 --> 00:15:03,700
 remaining mental defilements. So attenuate

186
00:15:03,700 --> 00:15:12,160
 refers to second maga, abandon refers to third and fourth

187
00:15:12,160 --> 00:15:13,660
 magas. That of the final knower

188
00:15:13,660 --> 00:15:19,260
 that is the last one. Faculty is both to abandon and never

189
00:15:19,260 --> 00:15:22,300
 in all functions because it has

190
00:15:22,300 --> 00:15:28,270
 already done its own duty and so there is no more to be

191
00:15:28,270 --> 00:15:29,300
 done. And as to playing that

192
00:15:29,300 --> 00:15:36,150
 means which faculty belong to which plane. So the I-ear,

193
00:15:36,150 --> 00:15:39,700
 nose, tongue, body and so on

194
00:15:39,700 --> 00:15:47,170
 belong to sense fear, gamma, etc. because they are all rub

195
00:15:47,170 --> 00:15:49,620
as. The mind faculty, life

196
00:15:49,740 --> 00:15:52,910
 faculty and equanimity faculty and the faculties of faith,

197
00:15:52,910 --> 00:15:54,820
 energy, mentalness, concentration

198
00:15:54,820 --> 00:15:58,430
 and understanding are included in the four planes. That

199
00:15:58,430 --> 00:16:00,220
 means they belong to all four

200
00:16:00,220 --> 00:16:05,870
 planes, karma vajra, ruba vajra, aruba vajra and glokutra

201
00:16:05,870 --> 00:16:07,220
 because mind faculty, life faculty

202
00:16:07,220 --> 00:16:18,460
 and so on are, mind faculty, life faculty and so on are,

203
00:16:18,460 --> 00:16:18,460
 mind faculty, life faculty

204
00:16:18,460 --> 00:16:25,460
 and so on. Mind faculty is the consciousness and life

205
00:16:25,460 --> 00:16:25,460
 faculty and others are mental states

206
00:16:25,460 --> 00:16:33,900
 or cheetah seekers. So they are included in all four planes

207
00:16:33,900 --> 00:16:35,940
. The joy faculty is included

208
00:16:35,940 --> 00:16:39,970
 in three planes, soma nasa, namely sense fear, find

209
00:16:39,970 --> 00:16:42,940
 material fear and supramanding, not immaterial

210
00:16:45,980 --> 00:16:51,260
 fear. The last three are supramanding only. That is the

211
00:16:51,260 --> 00:16:52,980
 last three means I shall come

212
00:16:52,980 --> 00:17:00,510
 to know and so on. This is how the exposition should be

213
00:17:00,510 --> 00:17:03,340
 known here as to play. So according

214
00:17:03,340 --> 00:17:08,390
 to this we understand that I faculty belongs to sense fear,

215
00:17:08,390 --> 00:17:10,900
 ear faculty belongs to sense

216
00:17:12,380 --> 00:17:16,170
 fear and so on and mind faculty belongs to four planes and

217
00:17:16,170 --> 00:17:17,580
 so on. So four planes means

218
00:17:17,580 --> 00:17:21,620
 four spheres, right? Karma vajra, ruba vajra, I mean find

219
00:17:21,620 --> 00:17:23,540
 material sphere, aruba vajra,

220
00:17:23,540 --> 00:17:28,070
 immaterial sphere and glokutra is also called as fear, not

221
00:17:28,070 --> 00:17:30,540
 included here. It comprises only

222
00:17:30,540 --> 00:17:38,460
 of three, right? Karma vajra, ruba vajra and aruba vajra.

223
00:17:38,460 --> 00:17:43,090
 Now if you look at the chart, I mean this sheet, let us go

224
00:17:43,090 --> 00:17:45,460
 through the basis elements

225
00:17:45,460 --> 00:17:50,110
 and faculties because in order to please its listeners or

226
00:17:50,110 --> 00:17:52,860
 susceptibility of the listener,

227
00:17:52,860 --> 00:18:01,720
 the voter taught in different ways, one and the same thing

228
00:18:01,720 --> 00:18:06,180
 is called a base, an element,

229
00:18:07,580 --> 00:18:14,580
 a frugal dn or so on. So here the, I have colored them. So

230
00:18:14,580 --> 00:18:14,580
 I base, I element and I frugal

231
00:18:14,580 --> 00:18:22,950
 dn are one and the same thing. They are actually eye

232
00:18:22,950 --> 00:18:25,660
 sensitivity.

233
00:18:25,660 --> 00:18:30,490
 Do the same thing? Yeah, they mean the same thing. So the

234
00:18:30,490 --> 00:18:32,660
 same color.

235
00:18:33,420 --> 00:18:37,760
 So sometimes it is called eye base, sometimes eye element,

236
00:18:37,760 --> 00:18:40,420
 sometimes eye frugality, but the

237
00:18:40,420 --> 00:18:43,970
 same thing. So ear, nose, tongue, body base, ear, nose,

238
00:18:43,970 --> 00:18:46,140
 tongue, body element, ear, nose,

239
00:18:46,140 --> 00:18:51,100
 tongue, body, frugality, same thing. And then let us go to

240
00:18:51,100 --> 00:18:53,140
 visible data base. Visible data

241
00:18:53,140 --> 00:18:58,310
 base, visible data element, same thing. Sound, auto, flavor

242
00:18:58,310 --> 00:19:00,260
, tangible data base and tangible

243
00:19:00,260 --> 00:19:05,240
 data element, they mean the same thing, but mind base, it

244
00:19:05,240 --> 00:19:07,260
 is a little different.

245
00:19:07,260 --> 00:19:14,660
 Now mind base means all jaders, right? So in the elements,

246
00:19:14,660 --> 00:19:14,660
 mind base is divided into

247
00:19:14,660 --> 00:19:26,020
 h, mind element and then eye consists as element and so on,

248
00:19:26,020 --> 00:19:27,940
 right? So mind base means all jaders,

249
00:19:27,940 --> 00:19:34,940
 right? Oh, I mean seven, not eight, seven, seven, it is

250
00:19:34,940 --> 00:19:34,940
 called vinyanada. Seven conscious,

251
00:19:34,940 --> 00:19:50,380
 they are called seven consciousness elements. So mind base

252
00:19:50,380 --> 00:19:53,580
 is divided into seven elements

253
00:19:53,580 --> 00:19:59,250
 here, mind element, eye consciousness element and so on. So

254
00:19:59,250 --> 00:20:00,580
 mind element means again the

255
00:20:00,580 --> 00:20:07,250
 three in my chart number 28, 18 and 25. So these are mind

256
00:20:07,250 --> 00:20:09,340
 elements, right? 18, I mean

257
00:20:09,340 --> 00:20:19,650
 28, 18 and 25. And then eye consciousness element means in

258
00:20:19,650 --> 00:20:21,100
 the chart number 28, 18 and

259
00:20:21,740 --> 00:20:25,360
 the chart number 13 and 20. Ear consciousness element 14

260
00:20:25,360 --> 00:20:28,740
 and 21. Nose 15 and 22. Tongue 16

261
00:20:28,740 --> 00:20:37,930
 and 23. Body consciousness element 17 and 24. All the

262
00:20:37,930 --> 00:20:41,980
 others are mind consciousness

263
00:20:41,980 --> 00:20:50,220
 element.

264
00:20:50,220 --> 00:20:55,140
 And among the faculties, mind faculties and mind base are

265
00:20:55,140 --> 00:20:57,220
 the same. So mind base means

266
00:20:57,220 --> 00:21:04,470
 all consciousness and mind faculties also means all

267
00:21:04,470 --> 00:21:08,300
 consciousness. Now we come to Dhamma,

268
00:21:08,300 --> 00:21:13,300
 mental data base. I want to call it Dhamma base. So Dhamma

269
00:21:13,300 --> 00:21:15,700
 base and Dhamma element are

270
00:21:16,660 --> 00:21:23,660
 the same. But in faculties, they are mentioned in different

271
00:21:23,660 --> 00:21:23,660
 ways. So femininity, faculty,

272
00:21:23,660 --> 00:21:30,040
 masculinity, life faculty and pleasure means sukha. Pain

273
00:21:30,040 --> 00:21:33,660
 means dukha. Joy means somanasa.

274
00:21:33,660 --> 00:21:41,910
 Grief means domanasa, mental. Equanimity means upika. The

275
00:21:41,910 --> 00:21:45,020
 first pleasure and pain are bodily,

276
00:21:46,140 --> 00:21:50,090
 pleasure and bodily pain. Dry grief and equanimity are

277
00:21:50,090 --> 00:21:53,140
 mental. And then faith, faculty, that

278
00:21:53,140 --> 00:21:57,390
 means just faith, energy, mindfulness, concentration and

279
00:21:57,390 --> 00:22:00,140
 understanding. And then there are the

280
00:22:00,140 --> 00:22:03,780
 other three kinds of understanding also. So they all belong

281
00:22:03,780 --> 00:22:06,180
 to what? Dhamma element or

282
00:22:06,180 --> 00:22:12,540
 Dhamma base. No color. You may color a red or any other

283
00:22:12,540 --> 00:22:13,180
 color. So you may color a red

284
00:22:13,700 --> 00:22:18,420
 or any other color. Or you can just leave them as they are.

285
00:22:18,420 --> 00:22:20,700
 So femininity belongs to

286
00:22:20,700 --> 00:22:28,010
 what element? Dhamma element. What base? Dhamma base and so

287
00:22:28,010 --> 00:22:31,620
 on. So this way you have a clear

288
00:22:31,620 --> 00:37:52,860
 vision of bases, elements and faculties. So when you read

289
00:37:52,860 --> 00:22:40,820
 the Wizard of America again,

290
00:22:42,180 --> 00:22:49,180
 please have it ready and refer to it. Okay. We can go a

291
00:22:49,180 --> 00:22:49,180
 little into the truths. Satcha.

292
00:22:49,180 --> 00:23:03,620
 So the Pali word Satcha is translated as truth here. And

293
00:23:03,620 --> 00:23:10,660
 then meaning will come later. So

294
00:23:11,660 --> 00:23:14,760
 here in S2 class, the meanings of the truth of suffering,

295
00:23:14,760 --> 00:23:16,460
 et cetera, analyze as full in

296
00:23:16,460 --> 00:23:20,330
 each case that are real, not unreal, not otherwise, and

297
00:23:20,330 --> 00:23:23,100
 must be penetrated by those penetrating

298
00:23:23,100 --> 00:23:26,590
 suffering, et cetera, according as it is said. So that I

299
00:23:26,590 --> 00:23:30,100
 said will be four meanings each of

300
00:23:30,100 --> 00:23:37,540
 the truth. So suffering's meaning is oppressing, being

301
00:23:37,540 --> 00:23:37,540
 formed, burning, being formed, burning,

302
00:23:37,540 --> 00:23:43,610
 and changing. So these four meanings belong to the first

303
00:23:43,610 --> 00:23:44,540
 truth. And then to the second

304
00:23:44,540 --> 00:23:57,420
 truth, what? Accumulating soul's bondage and impending. And

305
00:23:57,420 --> 00:23:57,420
 then the third truth meaning

306
00:24:03,700 --> 00:24:07,880
 escape, seclusion, being unformed, and deadlessness. And

307
00:24:07,880 --> 00:24:10,700
 the fourth truth meaning is outlet, cause,

308
00:24:10,700 --> 00:24:20,630
 seeing, and predominant. So these are the four meanings of

309
00:24:20,630 --> 00:24:23,180
 the pattern. Likewise, suffering's

310
00:24:29,140 --> 00:24:32,730
 meaning of oppression, oppressing meaning of being formed,

311
00:24:32,730 --> 00:24:34,580
 meaning of burning, meaning

312
00:24:34,580 --> 00:24:38,810
 of change, its meaning of penetration too, and so on. So

313
00:24:38,810 --> 00:24:39,860
 suffering, et cetera, should

314
00:24:39,860 --> 00:24:43,890
 be understood according to the four meanings analyzed in

315
00:24:43,890 --> 00:24:46,860
 each case. So they are the meanings

316
00:24:46,860 --> 00:24:54,520
 of the first four meanings of Doha. When you look at Doha,

317
00:24:54,520 --> 00:24:56,140
 when you concentrate on Doha,

318
00:24:56,140 --> 00:25:00,560
 you see that it is oppressing, and it is being formed, it

319
00:25:00,560 --> 00:25:03,140
 is burning, or it burns us, and

320
00:25:03,140 --> 00:25:10,380
 it is also changing, and so on. And then as to derivation,

321
00:25:10,380 --> 00:25:12,460
 and derivation by character,

322
00:25:12,460 --> 00:25:16,440
 et cetera. So the derivation of the word Dukkha, Samudya,

323
00:25:16,440 --> 00:25:19,460
 Nilodha, and Nilodha, Ghamini, Baddibhida

324
00:25:20,900 --> 00:25:26,100
 are given in paragraphs 16, 17, 18, and 19. And they may be

325
00:25:26,100 --> 00:25:27,900
 fanciful, but if you take

326
00:25:27,900 --> 00:25:40,070
 the light in, fighting about the words, I think it will be

327
00:25:40,070 --> 00:25:43,220
 interesting. So Dukkha is

328
00:25:45,580 --> 00:25:50,690
 said to be coming from do and ka, right? And do is bed, and

329
00:25:50,690 --> 00:25:52,580
 ka is what it call? Dukkha,

330
00:25:52,580 --> 00:26:05,300
 what do is bed within the sense of vial, bed of vial, and

331
00:26:05,300 --> 00:26:10,420
 then ka means empty. So something

332
00:26:12,940 --> 00:26:17,950
 which is bed or something which is vial and empty is called

333
00:26:17,950 --> 00:26:19,940
 Dukkha. And the first two

334
00:26:19,940 --> 00:26:23,360
 is vial because it is the haunt of many dangers. And it is

335
00:26:23,360 --> 00:26:25,420
 empty because it is devoid of the

336
00:26:25,420 --> 00:26:29,660
 lustingness, beauty, pleasure, and self, conceived by rush

337
00:26:29,660 --> 00:26:32,420
 people. So it is called Dukkha, bedness,

338
00:26:32,420 --> 00:26:36,810
 suffering, pain, because of vialness and emptiness. Now, if

339
00:26:36,810 --> 00:26:39,620
 we do not go further and come to

340
00:26:40,620 --> 00:26:44,600
 further and apply it to sukha, it is all right. But if we

341
00:26:44,600 --> 00:26:47,620
 try to explain sukha in this way,

342
00:26:47,620 --> 00:26:53,890
 we run into difficulties. Because the word sukha happiness,

343
00:26:53,890 --> 00:26:56,540
 right? Su ka, let us say

344
00:26:56,540 --> 00:27:01,430
 su means good. But ka, if ka is to mean devoid of lusting

345
00:27:01,430 --> 00:27:04,660
ness, devoid of beauty, devoid of

346
00:27:05,780 --> 00:27:10,690
 pleasure, devoid of self, then what about nibana? Nibana is

347
00:27:10,690 --> 00:27:12,780
 called the highest sukha.

348
00:27:12,780 --> 00:27:19,410
 Nibana ever lasts forever. Nibana could be said to be

349
00:27:19,410 --> 00:27:23,420
 beauty, and nibana is pleasure.

350
00:27:23,420 --> 00:27:29,100
 Only nibana is not self. So if we apply similarly the

351
00:27:29,100 --> 00:27:31,860
 explanation given here to sukha, then

352
00:27:33,860 --> 00:27:39,640
 we run into trouble. So it is just an explanation of the

353
00:27:39,640 --> 00:27:40,860
 word.

354
00:27:40,860 --> 00:27:46,630
 And samudya has three parts. Sam, ut, ayak. So sam has a

355
00:27:46,630 --> 00:27:49,380
 meaning of concourse or coming

356
00:27:49,380 --> 00:27:34,200
 together. And ut or u means rising up, coming up. So to up.

357
00:27:34,200 --> 00:28:00,140
 So to up. So to up. So to up.

358
00:28:01,140 --> 00:28:05,060
 And then ayak denotes a reason. So coming together, rising

359
00:28:05,060 --> 00:28:08,140
 up, reason. That is the meaning of

360
00:28:08,140 --> 00:28:15,110
 the word samudya, the second noble truth, origin of

361
00:28:15,110 --> 00:28:19,860
 suffering. And this second truth

362
00:28:19,860 --> 00:28:24,200
 is the reason for the arising of suffering when combined

363
00:28:24,200 --> 00:28:26,860
 with remaining conditions. So

364
00:28:26,860 --> 00:28:32,420
 reason, reason for the arising when combined with remaining

365
00:28:32,420 --> 00:28:33,860
 conditions. When it comes together

366
00:28:33,860 --> 00:28:42,700
 with other conditions. That means it is not the only

367
00:28:42,700 --> 00:28:45,980
 condition of dukkha. But it is a

368
00:28:45,980 --> 00:28:48,560
 prominent condition of dukkha because there are other

369
00:28:48,560 --> 00:28:51,420
 conditions too like ignorance. So

370
00:28:51,420 --> 00:28:54,990
 without ignoring the fact that there is no craving. So

371
00:28:54,990 --> 00:28:56,940
 craving is said to be here, the

372
00:28:56,940 --> 00:29:02,130
 origin of dukkha. But it does not mean that craving is the

373
00:29:02,130 --> 00:29:03,940
 only origin of dukkha. But

374
00:29:03,940 --> 00:29:07,430
 it is a prominent one. That is why it is called the origin

375
00:29:07,430 --> 00:29:10,940
 of dukkha. But the others like

376
00:29:10,940 --> 00:29:17,550
 ignorance is also the origin of dukkha. So when it comes

377
00:29:17,550 --> 00:29:18,500
 into combination with other

378
00:29:19,860 --> 00:29:24,530
 conditions it produces dukkha. So it is the reason of cause

379
00:29:24,530 --> 00:29:26,860
 for the arising of suffering

380
00:29:26,860 --> 00:29:32,200
 when combined with the remaining conditions. And then the

381
00:29:32,200 --> 00:29:35,100
 next one is nirodha, the third

382
00:29:35,100 --> 00:29:40,600
 noble truth, nibhana. But here nirodha is divided into nih

383
00:29:40,600 --> 00:29:42,700
 and rodha. And nih means

384
00:29:45,660 --> 00:29:50,490
 no absence. And rodha means imprisoned. Roda really means

385
00:29:50,490 --> 00:29:52,660
 restriction. So when you are

386
00:29:52,660 --> 00:29:58,420
 imprisoned you are restricted. So nibhana is opposite of

387
00:29:58,420 --> 00:29:59,820
 that, no restriction, freedom.

388
00:29:59,820 --> 00:30:06,010
 So nibhana is called dukkha nirodha. But it is just

389
00:30:06,010 --> 00:30:11,140
 translated as the cessation of suffering.

390
00:30:14,780 --> 00:30:19,490
 And then nirodha gamini padeebada. So that is the way

391
00:30:19,490 --> 00:30:21,780
 leading to cessation. The fourth

392
00:30:21,780 --> 00:30:27,670
 noble truth is called dukkha nirodha gamini padeebada. So

393
00:30:27,670 --> 00:30:30,900
 the way leading to the cessation

394
00:30:30,900 --> 00:30:37,900
 of suffering. And then the what, the pani word ariya satcha

395
00:30:37,900 --> 00:30:37,900
 is explained in four ways.

396
00:30:42,660 --> 00:30:49,250
 Paragraph 20, 21 and 22. Now according to paragraph 21, the

397
00:30:49,250 --> 00:30:49,660
 ariya, let us say ariya

398
00:30:49,660 --> 00:30:57,350
 satcha, ariya truth. Ariya truth means truth to be

399
00:30:57,350 --> 00:31:00,580
 penetrated by ariya. Ariya penetratable

400
00:31:00,580 --> 00:31:11,540
 truth becomes ariya truth. That is paragraph 20.

401
00:31:13,340 --> 00:31:17,810
 According to paragraph 21, the first part of paragraph 21,

402
00:31:17,810 --> 00:31:20,340
 ariya truth means the truth

403
00:31:20,340 --> 00:31:25,620
 of the ariya and the ariya means the Buddha, the truth of

404
00:31:25,620 --> 00:31:28,700
 the Buddha. Ariya s truth, a

405
00:31:28,700 --> 00:31:33,940
 Buddha s truth. And the third one, middle of the paragraph

406
00:31:33,940 --> 00:31:36,700
 21, beginning with all or

407
00:31:37,060 --> 00:31:41,840
 alternatively, so according to that passage, ariya, ariya

408
00:31:41,840 --> 00:31:44,060
 truth means truth that makes people

409
00:31:44,060 --> 00:31:52,170
 into ariyas. That means when you penetrate the truth you

410
00:31:52,170 --> 00:31:56,580
 become an ariya. So the truth

411
00:31:56,580 --> 00:32:02,010
 that makes people into ariyas. And the fourth one is

412
00:32:02,010 --> 00:32:03,580
 paragraph 22. That means real truth.

413
00:32:05,940 --> 00:32:09,790
 Ariya satcha, the Pali word ariya satcha means real truth.

414
00:32:09,790 --> 00:32:11,660
 The truth which is real, which

415
00:32:11,660 --> 00:32:16,630
 is not unreal and so on. So the word ariya satcha is

416
00:32:16,630 --> 00:32:18,660
 explained in four ways. It is usually

417
00:32:18,660 --> 00:32:28,830
 translated as noble truth. But noble truth means here

418
00:32:28,830 --> 00:32:29,180
 according to paragraph 22, noble

419
00:32:29,180 --> 00:32:33,990
 truth means true, the true truth or real truth. And then as

420
00:32:33,990 --> 00:32:36,180
 the division by character

421
00:32:36,180 --> 00:32:54,320
 etcetera and so on. And then paragraph 24 as to meaning,

422
00:32:54,320 --> 00:32:55,340
 tracing out of the meaning,

423
00:32:56,100 --> 00:32:59,530
 as to meaning firstly, what is the meaning of truth. Now

424
00:32:59,530 --> 00:33:01,380
 the word satcha, truth is here

425
00:33:01,380 --> 00:33:05,620
 explained. And in this explanation, the last line of the

426
00:33:05,620 --> 00:33:08,380
 page, with the eye of understanding

427
00:33:08,380 --> 00:33:13,010
 is not misleading like an illusion. Misleading really means

428
00:33:13,010 --> 00:33:15,380
 untrue. It is not untrue like

429
00:33:15,380 --> 00:33:19,550
 an illusion. The word illusion is the translation of the

430
00:33:19,550 --> 00:33:22,380
 Pali word maiya, m-a-y-a, which can

431
00:33:23,340 --> 00:33:27,830
 mean magic. So magic is untrue. So when you go to, magic

432
00:33:27,830 --> 00:33:30,340
 shows, they show you things that

433
00:33:30,340 --> 00:33:37,250
 are untrue but you think they are true. So like magic. De

434
00:33:37,250 --> 00:33:40,820
ceptive like a mirror or undiscoverable

435
00:33:40,820 --> 00:33:45,940
 like the self or the sectarian and so on. And then the

436
00:33:45,940 --> 00:33:48,420
 paragraph 25, there is no pain

437
00:33:48,660 --> 00:33:52,540
 but affliction and not, that is not pain afflicts. The

438
00:33:52,540 --> 00:33:55,260
 meaning is there is no dukha which does

439
00:33:55,260 --> 00:34:01,230
 not afflict. And there is nothing other than dukha that aff

440
00:34:01,230 --> 00:34:02,260
licts. Something like that.

441
00:34:02,260 --> 00:34:08,150
 And then paragraph 26, tracing out the meaning. Here the

442
00:34:08,150 --> 00:34:10,740
 meaning of the word satcha, I mean

443
00:34:16,380 --> 00:34:20,810
 means of the word satcha given in this paragraph. So satcha

444
00:34:20,810 --> 00:34:23,380
 can mean what? The first one. Verbal

445
00:34:23,380 --> 00:34:32,020
 truth. So verbal truth, saying something true is also

446
00:34:32,020 --> 00:34:34,740
 called satcha. And then the next one

447
00:34:34,740 --> 00:34:41,560
 is abstinence from lying is also called satcha. And the

448
00:34:41,560 --> 00:34:43,060
 third one is what? Truth as views.

449
00:34:44,540 --> 00:34:48,700
 So view is also called truth. And in such passages as truth

450
00:34:48,700 --> 00:34:51,540
 is one, there is no sagann,

451
00:34:51,540 --> 00:34:56,440
 it is truth in the ultimate sense, both nirvana and the

452
00:34:56,440 --> 00:34:59,760
 part. And then in such passages as

453
00:34:59,760 --> 00:35:03,640
 of the four truths, how many are profitable, it is noble

454
00:35:03,640 --> 00:35:06,100
 truth, the four truth. And here

455
00:35:06,100 --> 00:35:09,560
 too it is proper as noble truth. This is how the exposition

456
00:35:09,560 --> 00:35:11,140
 should be understood as to

457
00:35:11,140 --> 00:35:15,350
 tracing out the meaning. So sometimes the commentary has

458
00:35:15,350 --> 00:35:18,140
 the habit of giving, showing

459
00:35:18,140 --> 00:35:23,630
 us many meanings of a word. And then at the end it said

460
00:35:23,630 --> 00:35:26,780
 among these meanings and this

461
00:35:26,780 --> 00:35:32,390
 is proper here, that is proper here. That means giving the

462
00:35:32,390 --> 00:35:35,140
 meanings the word can denote and

463
00:35:37,620 --> 00:35:44,620
 then choosing the one appropriate here. So here also satcha

464
00:35:44,620 --> 00:35:44,620
 can mean verbal truth, it

465
00:35:44,620 --> 00:35:51,490
 can mean abstinence from lying and so on. But here what is

466
00:35:51,490 --> 00:35:54,860
 proper is just noble truth.

467
00:35:54,860 --> 00:36:03,480
 And then S2 37, paragraph 27, S2 neither less nor more,

468
00:36:03,480 --> 00:36:06,740
 that is no more than satcha. And

469
00:36:07,900 --> 00:36:12,130
 no fewer than four and no more than four noble truths. And

470
00:36:12,130 --> 00:36:14,900
 the first quotation cannot be

471
00:36:14,900 --> 00:36:20,540
 traced. But second quotation is from Sanyota Nigayak. And

472
00:36:20,540 --> 00:36:23,620
 then S2 the order, I think they

473
00:36:23,620 --> 00:36:30,140
 are not difficult. And then S2 expounding bird and so on.

474
00:36:30,140 --> 00:36:33,020
 Now here from here comes the

475
00:36:34,740 --> 00:36:40,370
 detailed exposition of the four noble truths. Now the noble

476
00:36:40,370 --> 00:36:41,740
 truths are described by the

477
00:36:41,740 --> 00:36:47,700
 Buddha as, see the first one is noble truth of suffering,

478
00:36:47,700 --> 00:36:50,860
 right? So it is expounded as

479
00:36:50,860 --> 00:36:54,250
 bird is suffering, aging is suffering, death is suffering,

480
00:36:54,250 --> 00:36:56,380
 sorrow, lamentation, pain, grief

481
00:36:56,380 --> 00:36:59,660
 and despair are suffering, association with the unloved is

482
00:36:59,660 --> 00:37:01,340
 suffering, separation from

483
00:37:01,340 --> 00:37:03,680
 the loved is suffering and not to get what one wants is

484
00:37:03,680 --> 00:37:05,180
 suffering. In short the five

485
00:37:05,180 --> 00:37:09,200
 aggregates has objects of clinging and suffering and so on.

486
00:37:09,200 --> 00:37:11,100
 So these explanations you find

487
00:37:11,100 --> 00:37:16,520
 in the first sermon, it is setting in motion of the wheel

488
00:37:16,520 --> 00:37:18,100
 of tama also. And then he explains

489
00:37:18,100 --> 00:37:23,830
 them in detail. So we will not come to the end of this

490
00:37:23,830 --> 00:37:26,700
 explanation tonight. I think,

491
00:37:30,220 --> 00:37:37,220
 I will stop here.

492
00:37:37,220 --> 00:37:37,240
 (

