1
00:00:00,000 --> 00:00:06,000
 [silence]

2
00:00:06,000 --> 00:00:12,000
 [silence]

3
00:00:12,000 --> 00:00:18,000
 [silence]

4
00:00:18,000 --> 00:00:24,000
 [silence]

5
00:00:24,000 --> 00:00:30,000
 [silence]

6
00:00:30,000 --> 00:00:36,000
 [silence]

7
00:00:36,000 --> 00:00:42,000
 [silence]

8
00:00:42,000 --> 00:00:48,000
 [silence]

9
00:00:48,000 --> 00:00:54,000
 [silence]

10
00:00:54,000 --> 00:01:00,000
 [silence]

11
00:01:00,000 --> 00:01:02,000
 [silence]

12
00:01:02,000 --> 00:01:06,000
 Okay, good evening everyone. Hopefully we've got everything

13
00:01:06,000 --> 00:01:07,000
 working now.

14
00:01:07,000 --> 00:01:12,000
 [silence]

15
00:01:12,000 --> 00:01:18,000
 [silence]

16
00:01:18,000 --> 00:01:24,000
 [silence]

17
00:01:24,000 --> 00:01:28,000
 Welcome back after a few days away.

18
00:01:28,000 --> 00:01:33,000
 [silence]

19
00:01:33,000 --> 00:01:36,000
 Meditators are diligently at work.

20
00:01:36,000 --> 00:01:40,000
 [silence]

21
00:01:40,000 --> 00:01:43,000
 Coming to the end of the practice.

22
00:01:43,000 --> 00:01:49,000
 The big finale.

23
00:01:49,000 --> 00:01:53,000
 It's a time when

24
00:01:53,000 --> 00:01:57,000
 it's useful to look back on where we've come and

25
00:01:57,000 --> 00:02:01,000
 get a sense therefore, thereby, of where we're going.

26
00:02:01,000 --> 00:02:07,000
 [silence]

27
00:02:07,000 --> 00:02:11,000
 And useful, I think, for everyone else to get a sense of

28
00:02:11,000 --> 00:02:15,670
 what it is we're going through when we do the meditation

29
00:02:15,670 --> 00:02:16,000
 course.

30
00:02:16,000 --> 00:02:19,000
 [silence]

31
00:02:19,000 --> 00:02:23,000
 Get a sense of the path that we're practicing.

32
00:02:23,000 --> 00:02:26,710
 So I thought I'd do that in brief. Go through that with you

33
00:02:26,710 --> 00:02:27,000
.

34
00:02:27,000 --> 00:02:31,000
 Also just as encouragement, how far you've come,

35
00:02:31,000 --> 00:02:35,000
 what great things you've done.

36
00:02:35,000 --> 00:02:39,070
 It's my job to give you encouragement. So maybe you think,

37
00:02:39,070 --> 00:02:40,000
 well, I'm just saying that, but

38
00:02:40,000 --> 00:02:43,000
 really it's true.

39
00:02:43,000 --> 00:02:48,000
 I mean, the power that you can feel and that you can see

40
00:02:48,000 --> 00:02:52,000
 that you gained from the meditation practice, that doesn't

41
00:02:52,000 --> 00:02:55,430
 come from nowhere. It comes from all the hard work you've

42
00:02:55,430 --> 00:02:56,000
 done.

43
00:02:56,000 --> 00:03:01,000
 First in morality, the first step on the path

44
00:03:01,000 --> 00:03:08,000
 is to regulate your actions and your speech.

45
00:03:08,000 --> 00:03:14,000
 Just by not engaging in all sorts of distracting and

46
00:03:14,000 --> 00:03:20,000
 what we call unwholesome activities, those activities which

47
00:03:20,000 --> 00:03:28,000
 distract the mind, bring the mind out of a state of peace.

48
00:03:28,000 --> 00:03:35,000
 Those activities that involve desire, addiction, aversion,

49
00:03:35,000 --> 00:03:40,000
 conceit, arrogance, views.

50
00:03:40,000 --> 00:03:47,000
 [silence]

51
00:03:47,000 --> 00:03:50,230
 It's quite eye-opening when you go out into the world after

52
00:03:50,230 --> 00:03:53,000
 doing a meditation course and you realize

53
00:03:53,000 --> 00:03:59,000
 just how constantly we're bombarded by

54
00:03:59,000 --> 00:04:04,000
 challenging situations and

55
00:04:04,000 --> 00:04:08,600
 bombarded by other people's defilements and stimuli that

56
00:04:08,600 --> 00:04:09,000
 encourage our own

57
00:04:09,000 --> 00:04:14,000
 defilements.

58
00:04:14,000 --> 00:04:17,360
 It's great work that you're doing to come here and just to

59
00:04:17,360 --> 00:04:19,000
 stay here, to put yourself in a position

60
00:04:19,000 --> 00:04:24,000
 where you're not engaging in all those things.

61
00:04:24,000 --> 00:04:30,000
 And moreover, to regulate your activities so that it's as

62
00:04:30,000 --> 00:04:37,000
 clear and as peaceful and as focused as possible.

63
00:04:37,000 --> 00:04:41,670
 This is why we talk about running. Better not to go for a

64
00:04:41,670 --> 00:04:43,000
 run while you're here.

65
00:04:43,000 --> 00:04:49,000
 But so many things, so many distractions we've done away

66
00:04:49,000 --> 00:04:50,000
 with.

67
00:04:50,000 --> 00:04:56,750
 Just by maintaining this routine of walking and sitting

68
00:04:56,750 --> 00:04:59,000
 throughout the day.

69
00:04:59,000 --> 00:05:03,510
 So we call morality, and this is the Buddhist sense of

70
00:05:03,510 --> 00:05:04,000
 morality.

71
00:05:04,000 --> 00:05:08,000
 It's not about killing and stealing, not exactly.

72
00:05:08,000 --> 00:05:12,000
 And there are many things we can say are immoral.

73
00:05:12,000 --> 00:05:17,000
 But morality really comes down to the focus of the mind,

74
00:05:17,000 --> 00:05:21,000
 through the focus of the body.

75
00:05:21,000 --> 00:05:32,000
 By acting and speaking in such ways that we remove the

76
00:05:32,000 --> 00:05:37,000
 opportunity or the

77
00:05:37,000 --> 00:05:47,000
 impulse to do unwholesome actions.

78
00:05:47,000 --> 00:05:51,000
 So that's the first step because it focuses your mind.

79
00:05:51,000 --> 00:05:55,000
 So the second step is this focus that you gain.

80
00:05:55,000 --> 00:05:59,000
 It's what you're doing every moment that your mind falls.

81
00:05:59,000 --> 00:06:04,160
 You're cultivating this focus. Focusing your attention on

82
00:06:04,160 --> 00:06:06,000
 that moment.

83
00:06:06,000 --> 00:06:11,000
 One moment after one moment.

84
00:06:11,000 --> 00:06:15,800
 And as it becomes habitual, as you become comfortable with

85
00:06:15,800 --> 00:06:19,000
 it or accustomed to it,

86
00:06:19,000 --> 00:06:24,000
 it gains a power.

87
00:06:24,000 --> 00:06:29,000
 There's a strength in repetition.

88
00:06:29,000 --> 00:06:33,000
 The more and more often you repeat something,

89
00:06:33,000 --> 00:06:40,450
 the more it becomes a habit, the more it becomes a part of

90
00:06:40,450 --> 00:06:41,000
 you.

91
00:06:41,000 --> 00:06:45,000
 And the more powerful it becomes.

92
00:06:45,000 --> 00:06:47,820
 So you gain this momentum, and this is what we call

93
00:06:47,820 --> 00:06:49,000
 concentration.

94
00:06:49,000 --> 00:06:52,460
 You might not feel concentrated because your mind is still

95
00:06:52,460 --> 00:06:53,000
 trying to play tricks on you,

96
00:06:53,000 --> 00:06:57,000
 and there's lots of distractions.

97
00:06:57,000 --> 00:07:00,430
 But you have a power. You have a true concentration in the

98
00:07:00,430 --> 00:07:02,000
 sense that you're able to stay present.

99
00:07:02,000 --> 00:07:09,000
 It's like riding on a bull, this rodeo bull,

100
00:07:09,000 --> 00:07:14,000
 the challenge is to stay on.

101
00:07:14,000 --> 00:07:19,820
 And so it feels like you're flailing wildly, but you're

102
00:07:19,820 --> 00:07:23,000
 staying on the bull.

103
00:07:23,000 --> 00:07:30,880
 And that's reality. Reality is not a peaceful, still forest

104
00:07:30,880 --> 00:07:32,000
 bull.

105
00:07:32,000 --> 00:07:38,260
 Reality is inconstant, unpredictable. It's unsatisfying, it

106
00:07:38,260 --> 00:07:49,000
's stressful, it's uncontrollable.

107
00:07:49,000 --> 00:07:52,160
 And so it's not about what we experience, it's about how we

108
00:07:52,160 --> 00:07:53,000
 react to it.

109
00:07:53,000 --> 00:07:57,000
 Our concentration is about not reacting to our experiences,

110
00:07:57,000 --> 00:08:03,880
 learning how to be focused naturally on whatever we

111
00:08:03,880 --> 00:08:06,000
 experience.

112
00:08:06,000 --> 00:08:10,000
 After focus comes this whole course of wisdom,

113
00:08:10,000 --> 00:08:12,450
 and that's what you've really been getting out of this

114
00:08:12,450 --> 00:08:13,000
 course.

115
00:08:13,000 --> 00:08:16,980
 Once you become focused, you start to see things about

116
00:08:16,980 --> 00:08:18,000
 yourself.

117
00:08:18,000 --> 00:08:20,610
 First you start to see who you are, what does it mean by

118
00:08:20,610 --> 00:08:21,000
 the self,

119
00:08:21,000 --> 00:08:25,000
 what do we mean when we talk about myself?

120
00:08:25,000 --> 00:08:29,900
 You start to see what's really inside, what you're really

121
00:08:29,900 --> 00:08:31,000
 made of.

122
00:08:31,000 --> 00:08:35,000
 You see that there's physical and there's mental.

123
00:08:35,000 --> 00:08:40,000
 And really what we're made up of is experiences.

124
00:08:40,000 --> 00:08:43,420
 We have experiences of seeing and hearing and smelling and

125
00:08:43,420 --> 00:08:45,000
 tasting and feeling and thinking,

126
00:08:45,000 --> 00:08:57,420
 physical and mental realities coming together to form

127
00:08:57,420 --> 00:09:01,000
 experience.

128
00:09:01,000 --> 00:09:04,000
 We start to see how the experiences work together,

129
00:09:04,000 --> 00:09:06,000
 how some experiences lead to other experiences.

130
00:09:06,000 --> 00:09:10,000
 An experience of pain leads you to get upset,

131
00:09:10,000 --> 00:09:18,000
 and the experience of upset leads you to suffer.

132
00:09:18,000 --> 00:09:23,000
 You see how mindfulness leads to calm, leads to clarity,

133
00:09:23,000 --> 00:09:27,000
 leads to purity.

134
00:09:27,000 --> 00:09:32,000
 You see how something desirable leads to wanting,

135
00:09:32,000 --> 00:09:41,000
 which leads to clinging, which leads to suffering.

136
00:09:41,000 --> 00:09:44,750
 And you start to see really in general that nothing is

137
00:09:44,750 --> 00:09:47,000
 really worth clinging to.

138
00:09:47,000 --> 00:09:51,830
 And this is the slow and steady realization that you come

139
00:09:51,830 --> 00:09:52,000
 to,

140
00:09:52,000 --> 00:09:55,460
 clearer and clearer that you've really come to at this

141
00:09:55,460 --> 00:09:57,000
 point in the course.

142
00:09:57,000 --> 00:09:59,000
 You've seen everything.

143
00:09:59,000 --> 00:10:02,000
 Your mind has done all its tricks.

144
00:10:02,000 --> 00:10:08,000
 Well, it's done most of its major tricks anyway.

145
00:10:08,000 --> 00:10:12,080
 And you're really starting to see that none of it's worth

146
00:10:12,080 --> 00:10:13,000
 clinging to,

147
00:10:13,000 --> 00:10:21,000
 trying to fix, trying to control, not worth it.

148
00:10:21,000 --> 00:10:24,160
 And so at this point it's really just about refining your

149
00:10:24,160 --> 00:10:25,000
 practice.

150
00:10:25,000 --> 00:10:29,040
 All of these stages of knowledge that you've come through

151
00:10:29,040 --> 00:10:30,000
 means you've come to see this.

152
00:10:30,000 --> 00:10:32,820
 The things you thought were stable, satisfying, controll

153
00:10:32,820 --> 00:10:34,000
able,

154
00:10:34,000 --> 00:10:43,000
 are unstable, unpredictable, unsatisfying, uncontrollable.

155
00:10:43,000 --> 00:10:45,630
 It's not worth even trying to control them, trying to own

156
00:10:45,630 --> 00:10:46,000
 them,

157
00:10:46,000 --> 00:10:51,000
 trying to be them.

158
00:10:51,000 --> 00:11:00,000
 And your mind pulls back and you become equanimous.

159
00:11:00,000 --> 00:11:05,090
 Once you get to this point, well, now you can see where we

160
00:11:05,090 --> 00:11:06,000
 are,

161
00:11:06,000 --> 00:11:14,000
 where we're at, as for where we're going.

162
00:11:14,000 --> 00:11:19,740
 We talk about samsara and how the cause of suffering is our

163
00:11:19,740 --> 00:11:21,000
 craving.

164
00:11:21,000 --> 00:11:24,500
 Well, the cause of our clinging to samsara, to clinging to

165
00:11:24,500 --> 00:11:28,000
 all of this, is craving.

166
00:11:28,000 --> 00:11:31,730
 And once you become equanimous, once you let go, the mind

167
00:11:31,730 --> 00:11:33,000
 really lets go.

168
00:11:33,000 --> 00:11:37,000
 So this experience of nirvana or nirvana is really just

169
00:11:37,000 --> 00:11:39,000
 when the mind is not clinging anymore.

170
00:11:39,000 --> 00:11:45,000
 It's not reaching anymore. It's not seeking anymore.

171
00:11:45,000 --> 00:11:47,340
 At the end of that seeking there's the cessation of

172
00:11:47,340 --> 00:11:53,000
 suffering, there's an experience of nirvana.

173
00:11:53,000 --> 00:11:55,000
 That's where we're going.

174
00:11:55,000 --> 00:11:57,360
 Now nirvana is something you can experience just for a

175
00:11:57,360 --> 00:12:00,000
 short time, a few moments even,

176
00:12:00,000 --> 00:12:03,000
 but it really changes your perspective.

177
00:12:03,000 --> 00:12:07,120
 It's not magic, but it is a kind of a magic, or it seems

178
00:12:07,120 --> 00:12:08,000
 magical.

179
00:12:08,000 --> 00:12:12,000
 Because once you see that, then you know what true peace is

180
00:12:12,000 --> 00:12:12,000
.

181
00:12:12,000 --> 00:12:15,370
 And all this other happiness and pleasure that we seek in

182
00:12:15,370 --> 00:12:22,000
 the world just doesn't seem meaningful anymore.

183
00:12:22,000 --> 00:12:25,790
 At this point you realize that you're at a mistake to cling

184
00:12:25,790 --> 00:12:32,000
, but once you see nirvana, it's not intellectual anymore.

185
00:12:32,000 --> 00:12:37,810
 Your mind is really clear, you know in your heart, no doubt

186
00:12:37,810 --> 00:12:39,000
 in your mind that

187
00:12:39,000 --> 00:12:46,880
 'sabe damana langa bhini way saaya', no dama indeed, is

188
00:12:46,880 --> 00:12:50,000
 worth clinging to.

189
00:12:50,000 --> 00:12:54,000
 So that's the path in brief.

190
00:12:54,000 --> 00:12:58,000
 A little bit of encouragement.

191
00:12:58,000 --> 00:13:03,230
 I'll try to come back every night and give you a little bit

192
00:13:03,230 --> 00:13:05,000
 of encouragement.

193
00:13:05,000 --> 00:13:08,990
 But thanks for coming out and appreciation for all your

194
00:13:08,990 --> 00:13:10,000
 practice.

195
00:13:10,000 --> 00:13:13,000
 You can go back and meditate.

196
00:13:13,000 --> 00:13:28,000
 [silence]

197
00:13:28,000 --> 00:13:31,000
 We have some questions on the website.

198
00:13:31,000 --> 00:13:38,000
 [silence]

199
00:13:38,000 --> 00:13:42,220
 So, recommended the middle-length discourses and the great

200
00:13:42,220 --> 00:13:43,000
 disciples.

201
00:13:43,000 --> 00:13:46,880
 Recommend reading the others, yes, the long discourses,

202
00:13:46,880 --> 00:13:49,000
 numerical discourses, yes.

203
00:13:49,000 --> 00:13:53,000
 Recommend all of those.

204
00:13:53,000 --> 00:13:57,950
 But they're a little harder to get into, so the middle-

205
00:13:57,950 --> 00:14:02,000
length discourses is easier to get into.

206
00:14:02,000 --> 00:14:06,690
 But once you've finished it, the long discourses is fairly

207
00:14:06,690 --> 00:14:07,000
 easy.

208
00:14:07,000 --> 00:14:10,000
 It's just they're quite a bit longer.

209
00:14:10,000 --> 00:14:13,330
 The numerical discourses, I don't expect most people will

210
00:14:13,330 --> 00:14:15,000
 read through it entirely.

211
00:14:15,000 --> 00:14:18,910
 There's a lot of repetition and it's a lot of lists and it

212
00:14:18,910 --> 00:14:20,000
's very big.

213
00:14:20,000 --> 00:14:23,700
 The, what you call anthology discourses, I'm not sure what

214
00:14:23,700 --> 00:14:24,000
 that is,

215
00:14:24,000 --> 00:14:27,310
 but it's probably the sanyutani ka, the connected disc

216
00:14:27,310 --> 00:14:28,000
ourses.

217
00:14:28,000 --> 00:14:34,000
 Connected discourses is just huge and a lot of repetition.

218
00:14:34,000 --> 00:14:39,000
 But Bhikkhu Bodhi does a good job of keeping that down.

219
00:14:39,000 --> 00:14:45,030
 And I definitely recommend it all. It just takes time to

220
00:14:45,030 --> 00:14:46,000
 study.

221
00:14:46,000 --> 00:14:51,000
 [silence]

222
00:14:51,000 --> 00:14:55,590
 I think that's really about all I have on the shelf. That's

223
00:14:55,590 --> 00:14:57,000
 interesting.

224
00:14:57,000 --> 00:15:00,000
 [silence]

225
00:15:00,000 --> 00:15:04,000
 Okay, I spoke about teaching as being efficient.

226
00:15:04,000 --> 00:15:07,590
 And the question is, how does, there's no wanting other

227
00:15:07,590 --> 00:15:09,000
 people to be happy?

228
00:15:09,000 --> 00:15:12,000
 No, there's no wanting other people to be happy.

229
00:15:12,000 --> 00:15:15,000
 How does compassion relate to this?

230
00:15:15,000 --> 00:15:17,000
 Compassion is a state of mind.

231
00:15:17,000 --> 00:15:19,000
 When we talk about the Buddha's compassion,

232
00:15:19,000 --> 00:15:22,300
 we're talking about the compassion that led him to become a

233
00:15:22,300 --> 00:15:23,000
 Buddha.

234
00:15:23,000 --> 00:15:28,000
 But once he became a Buddha,

235
00:15:28,000 --> 00:15:33,000
 the compassion didn't drive him anymore.

236
00:15:33,000 --> 00:15:40,000
 And actually true compassion doesn't really drive you.

237
00:15:40,000 --> 00:15:45,000
 True compassion is what allows you to help people.

238
00:15:45,000 --> 00:15:48,000
 But it's not that you're compassionate,

239
00:15:48,000 --> 00:15:51,060
 so you go around trying to change everyone, trying to teach

240
00:15:51,060 --> 00:15:52,000
 everyone.

241
00:15:52,000 --> 00:15:55,000
 So when the Buddha was invited to teach,

242
00:15:55,000 --> 00:16:00,000
 it was compassion that made him say,

243
00:16:00,000 --> 00:16:02,000
 but it's a functional compassion.

244
00:16:02,000 --> 00:16:04,440
 I mean, it's just a part of his enlightenment. It's very

245
00:16:04,440 --> 00:16:05,000
 natural.

246
00:16:05,000 --> 00:16:09,000
 Compassion is maybe just a word that describes that aspect

247
00:16:09,000 --> 00:16:10,000
 of being enlightened

248
00:16:10,000 --> 00:16:13,000
 that leads one to not be cruel.

249
00:16:13,000 --> 00:16:16,120
 Because that's what compassion is. It's really just not

250
00:16:16,120 --> 00:16:17,000
 being cruel.

251
00:16:17,000 --> 00:16:23,000
 When someone is suffering,

252
00:16:23,000 --> 00:16:25,000
 you act in such a way as to free them.

253
00:16:25,000 --> 00:16:29,000
 When they approach you and they want help,

254
00:16:29,000 --> 00:16:33,000
 you do what you can to help them based on their wish.

255
00:16:33,000 --> 00:16:35,000
 Now the question you have is,

256
00:16:35,000 --> 00:16:37,200
 what about the Buddha telling all these arahants to go

257
00:16:37,200 --> 00:16:38,000
 forth and teach?

258
00:16:38,000 --> 00:16:40,000
 I mean, the only reason he did that

259
00:16:40,000 --> 00:16:43,000
 is because he made a promise to spread the teaching.

260
00:16:43,000 --> 00:16:47,000
 So it's still based on this idea that people are asking,

261
00:16:47,000 --> 00:16:50,000
 people are looking for it.

262
00:16:50,000 --> 00:16:52,000
 It has to be that way, I mean, for two reasons.

263
00:16:52,000 --> 00:16:55,790
 First of all, because it's the nature of an enlightened

264
00:16:55,790 --> 00:16:57,000
 being, not the clings.

265
00:16:57,000 --> 00:16:59,000
 So you have no desire for anything.

266
00:16:59,000 --> 00:17:02,000
 But also because if you desire to teach,

267
00:17:02,000 --> 00:17:05,000
 it doesn't really desire to spread the dumb.

268
00:17:05,000 --> 00:17:09,000
 It's problematic.

269
00:17:09,000 --> 00:17:18,860
 There's not a sincerity of reception, you know, as there is

270
00:17:18,860 --> 00:17:19,000
.

271
00:17:19,000 --> 00:17:25,980
 Yesterday I was teaching, I was actually quite good at the

272
00:17:25,980 --> 00:17:26,000
 waste act celebration.

273
00:17:26,000 --> 00:17:30,000
 It was quite, people were quite appreciative.

274
00:17:30,000 --> 00:17:32,550
 But you know, there's always people who seem to be taking

275
00:17:32,550 --> 00:17:33,000
 it for granted,

276
00:17:33,000 --> 00:17:36,000
 like as though I want to be there and they're like,

277
00:17:36,000 --> 00:17:39,000
 "Okay, so you want to teach me how to meditate?"

278
00:17:39,000 --> 00:17:42,220
 Or something like, "Are you going to teach me how to med

279
00:17:42,220 --> 00:17:43,000
itate?"

280
00:17:43,000 --> 00:17:46,000
 "Well, if you want me to."

281
00:17:46,000 --> 00:17:54,000
 I guess I put myself out there yesterday.

282
00:17:54,000 --> 00:17:56,570
 But people seem to think that it's somehow something I want

283
00:17:56,570 --> 00:17:57,000
 to do.

284
00:17:57,000 --> 00:18:00,000
 It's not really something I'm, I mean, I'm not talking,

285
00:18:00,000 --> 00:18:02,000
 I shouldn't talk too much about myself,

286
00:18:02,000 --> 00:18:06,000
 but the wanting isn't a thing.

287
00:18:06,000 --> 00:18:11,000
 It's that people are looking for it.

288
00:18:11,000 --> 00:18:13,000
 And as I said, that it's efficient.

289
00:18:13,000 --> 00:18:17,980
 It's just a great way to live your life and very helpful

290
00:18:17,980 --> 00:18:21,000
 for your own practice.

291
00:18:21,000 --> 00:18:23,000
 Explain that bad habits are dangerous

292
00:18:23,000 --> 00:18:27,000
 because they increase the tendency to like or dislike.

293
00:18:27,000 --> 00:18:30,640
 More functional habits, cleaning the counter when it's

294
00:18:30,640 --> 00:18:31,000
 dirty,

295
00:18:31,000 --> 00:18:34,000
 is there any danger connected to those habits?

296
00:18:34,000 --> 00:18:37,000
 No, I mean, not theoretically,

297
00:18:37,000 --> 00:18:40,270
 but practically most of our functional habits are

298
00:18:40,270 --> 00:18:43,000
 associated with likes and dislikes.

299
00:18:43,000 --> 00:18:45,000
 And those are the habits.

300
00:18:45,000 --> 00:18:47,000
 Those are the aspects of the habit that we want to change.

301
00:18:47,000 --> 00:18:51,210
 So a habit, as you're thinking of it, is many, many

302
00:18:51,210 --> 00:18:52,000
 different habits

303
00:18:52,000 --> 00:18:56,000
 as we would think of them in Buddhism.

304
00:18:56,000 --> 00:18:59,320
 So the habit of cleaning may have habitual anger associated

305
00:18:59,320 --> 00:19:00,000
 with it.

306
00:19:00,000 --> 00:19:02,000
 Why do I have to clean?

307
00:19:02,000 --> 00:19:04,000
 Or this is smelly or this is dirty or so on.

308
00:19:04,000 --> 00:19:07,000
 Boredom maybe.

309
00:19:07,000 --> 00:19:10,000
 Or maybe there's a liking of it.

310
00:19:10,000 --> 00:19:13,000
 You like the cleanliness of the place.

311
00:19:13,000 --> 00:19:17,000
 You like the idea of having a clean house and so on.

312
00:19:17,000 --> 00:19:21,000
 And so those become habitual and they build.

313
00:19:21,000 --> 00:19:23,000
 Those are the bad habits.

314
00:19:23,000 --> 00:19:24,580
 But the habits you're talking about are a little bit more

315
00:19:24,580 --> 00:19:25,000
 complicated,

316
00:19:25,000 --> 00:19:27,000
 they're like complex habits.

317
00:19:27,000 --> 00:19:28,000
 So they can change.

318
00:19:28,000 --> 00:19:32,000
 You can purify that so that you still clean the counter.

319
00:19:32,000 --> 00:19:37,700
 But you do it free from the habit of disliking or liking or

320
00:19:37,700 --> 00:19:38,000
 so on,

321
00:19:38,000 --> 00:19:43,000
 or attachment of any sort.

322
00:19:43,000 --> 00:19:48,000
 Is it fair to say that Upadana requires dithyvipalasa?

323
00:19:48,000 --> 00:19:50,000
 Oh, I don't know where you're going.

324
00:19:50,000 --> 00:19:54,000
 Sankadis are not practical questions.

325
00:19:54,000 --> 00:19:56,000
 I don't have an answer.

326
00:19:56,000 --> 00:19:58,210
 I need an Abhidhamma scholar to give you the answers to

327
00:19:58,210 --> 00:20:00,000
 these questions.

328
00:20:00,000 --> 00:20:08,000
 I'm going to click on the link.

329
00:20:08,000 --> 00:20:19,000
 Click and close all these answered questions.

330
00:20:19,000 --> 00:20:22,590
 When searching, you can't find any information regarding

331
00:20:22,590 --> 00:20:23,000
 mudras.

332
00:20:23,000 --> 00:20:27,000
 Well, we don't teach mudras in my tradition.

333
00:20:27,000 --> 00:20:30,110
 Avijjapa jeyasankara means that all kamas are caused by

334
00:20:30,110 --> 00:20:31,000
 ignorance.

335
00:20:31,000 --> 00:20:32,000
 Yes.

336
00:20:32,000 --> 00:20:34,000
 Including the kusala kamas, yes.

337
00:20:34,000 --> 00:20:37,000
 How do you explain dhiyetu kakama?

338
00:20:37,000 --> 00:20:38,000
 I don't know.

339
00:20:38,000 --> 00:20:41,000
 You have to talk to an Abhidhamma scholar again.

340
00:20:41,000 --> 00:20:49,880
 But in a general practical sense, an arahant has no karmic

341
00:20:49,880 --> 00:20:52,000
 potency.

342
00:20:52,000 --> 00:20:54,680
 The things that they do and they have a special kind of

343
00:20:54,680 --> 00:20:55,000
 mind

344
00:20:55,000 --> 00:20:58,000
 that is just purely functional.

345
00:20:58,000 --> 00:21:01,000
 I'm doing this because it's...

346
00:21:01,000 --> 00:21:04,000
 Really just because I'm doing it, honestly.

347
00:21:04,000 --> 00:21:09,000
 But the thing is, you can't have that mind with evil deeds.

348
00:21:09,000 --> 00:21:11,000
 Because they're too complicated.

349
00:21:11,000 --> 00:21:15,000
 They're too perverse.

350
00:21:15,000 --> 00:21:17,000
 That's the theory.

351
00:21:17,000 --> 00:21:20,000
 It's all fairly technical.

352
00:21:20,000 --> 00:21:24,000
 Okay, well, I answered most of them.

353
00:21:24,000 --> 00:21:26,000
 I deleted a couple.

354
00:21:26,000 --> 00:21:30,000
 That's our questions for tonight.

355
00:21:30,000 --> 00:21:32,000
 So thank you all for coming out.

356
00:21:32,000 --> 00:21:34,000
 I'll see you all, I guess tomorrow.

357
00:21:34,000 --> 00:21:36,630
 I was thinking of maybe cutting down the days that I give

358
00:21:36,630 --> 00:21:37,000
 talks,

359
00:21:37,000 --> 00:21:41,000
 but just give short talks, no?

360
00:21:41,000 --> 00:21:44,000
 Okay, have a good night, everyone.

