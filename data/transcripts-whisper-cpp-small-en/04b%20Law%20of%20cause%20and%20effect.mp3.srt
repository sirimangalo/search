1
00:00:00,000 --> 00:00:09,840
 So when I read this passage, I couldn't understand it.

2
00:00:09,840 --> 00:00:15,780
 And so I looked for an explanation in the commentary, but

3
00:00:15,780 --> 00:00:17,920
 the commentary didn't say

4
00:00:17,920 --> 00:00:20,080
 anything about this sentence.

5
00:00:20,080 --> 00:00:24,540
 I was dissatisfied with the commentary because we always

6
00:00:24,540 --> 00:00:27,120
 expect the commentary to explain

7
00:00:27,120 --> 00:00:32,160
 such points and at this place it's silent.

8
00:00:32,160 --> 00:00:35,840
 And what was sub-commentary saying?

9
00:00:35,840 --> 00:00:39,360
 So it gives a lot of dukkha.

10
00:00:39,360 --> 00:00:43,320
 And so I wanted to give dukkha to you too.

11
00:00:43,320 --> 00:00:48,240
 So you have to think about it until next class.

12
00:00:48,240 --> 00:00:53,160
 Okay, next question.

13
00:00:53,160 --> 00:00:57,240
 Do plants, trees, rocks and animals have karma?

14
00:00:57,240 --> 00:01:03,180
 Maybe due to many of you it is a silly question, but there

15
00:01:03,180 --> 00:01:07,640
 may be some people, hopefully not

16
00:01:07,640 --> 00:01:13,030
 among you, who may think that there is karma to plants,

17
00:01:13,030 --> 00:01:14,820
 trees and so on.

18
00:01:14,820 --> 00:01:17,060
 So this question is put here.

19
00:01:17,060 --> 00:01:20,270
 So according to Thiravada Buddhism, plants, trees and rocks

20
00:01:20,270 --> 00:01:21,640
 are not living beings.

21
00:01:21,640 --> 00:01:26,330
 They are not regarded as living beings, although in some

22
00:01:26,330 --> 00:01:29,480
 way plants may be said to have life.

23
00:01:29,480 --> 00:01:34,000
 But that is not life taught in Abhidhamma.

24
00:01:34,000 --> 00:01:40,910
 So plants, trees and rocks are not living beings and as

25
00:01:40,910 --> 00:01:43,960
 such they have no karma.

26
00:01:43,960 --> 00:01:47,570
 Although they have no karma, they are nevertheless

27
00:01:47,570 --> 00:01:50,760
 dependent on other material conditions like

28
00:01:50,760 --> 00:01:54,320
 moisture, temperature and nutrition and so on.

29
00:01:54,320 --> 00:02:04,280
 So although they have no karma, they depend on conditions

30
00:02:04,280 --> 00:02:08,120
 to arise and to exist.

31
00:02:08,120 --> 00:02:14,600
 So they are also dependent on some conditions, although

32
00:02:14,600 --> 00:02:17,080
 they have no karma.

33
00:02:17,080 --> 00:02:28,130
 So the causal relationship between inanimate things will be

34
00:02:28,130 --> 00:02:33,400
 explained with reference to

35
00:02:33,400 --> 00:02:38,440
 patthana.

36
00:02:38,440 --> 00:02:43,200
 Now animals are living beings.

37
00:02:43,200 --> 00:02:48,010
 We have all five aggregates as we human beings do and so

38
00:02:48,010 --> 00:02:50,440
 they are living beings.

39
00:02:50,440 --> 00:02:54,720
 As living beings, they have karma.

40
00:02:54,720 --> 00:03:00,900
 Animals acquire karma or accumulate karma and animals are

41
00:03:00,900 --> 00:03:03,480
 born as animals as a result

42
00:03:03,480 --> 00:03:06,800
 of Akusela karma in the past.

43
00:03:06,800 --> 00:03:15,920
 So animals have karma and they do or they perform karma.

44
00:03:15,920 --> 00:03:29,020
 But plants, trees, rocks and other inanimate things have no

45
00:03:29,020 --> 00:03:30,000
 karma.

46
00:03:30,000 --> 00:03:37,210
 Now there are some statements in Risuddhi Maga regarding

47
00:03:37,210 --> 00:03:37,480
 karma and results and they are

48
00:03:37,480 --> 00:03:38,480
 noteworthy.

49
00:03:38,480 --> 00:03:46,360
 So we should note them.

50
00:03:46,360 --> 00:03:50,880
 Now while karma and results thus causally maintain their

51
00:03:50,880 --> 00:03:53,520
 round, as seed and tree succeed

52
00:03:53,520 --> 00:03:58,160
 in turn, no first beginning can be shown.

53
00:03:58,160 --> 00:04:02,290
 Now karma and results follow each other and there is karma

54
00:04:02,290 --> 00:04:04,320
 and then there is the result

55
00:04:04,320 --> 00:04:05,320
 of karma.

56
00:04:05,320 --> 00:04:11,290
 Now see there is kusela or Akusela karma and as a result of

57
00:04:11,290 --> 00:04:14,160
 kusela and Akusela karma there

58
00:04:14,160 --> 00:04:21,250
 is rebirth as say in four woeful states or in human beings

59
00:04:21,250 --> 00:04:24,560
 or in celestial beings.

60
00:04:24,560 --> 00:04:30,680
 And during that existence beings acquire new karma and then

61
00:04:30,680 --> 00:04:33,440
 there is the result of that

62
00:04:33,440 --> 00:04:34,600
 karma in the future.

63
00:04:34,600 --> 00:04:39,450
 And so karma and its results go on and on and on like that,

64
00:04:39,450 --> 00:04:41,560
 like a seed and the tree.

65
00:04:41,560 --> 00:04:44,970
 So there is a seed and then it grows into a tree and then

66
00:04:44,970 --> 00:04:46,560
 the tree gives seed and then

67
00:04:46,560 --> 00:04:48,360
 seed in the tree and so on.

68
00:04:48,360 --> 00:04:52,040
 Or we can say the hand and the egg, right?

69
00:04:52,040 --> 00:04:53,560
 Go on and on and on.

70
00:04:53,560 --> 00:04:56,960
 So there is no first beginning.

71
00:04:56,960 --> 00:05:01,800
 No first beginning can be shown.

72
00:05:01,800 --> 00:05:09,790
 We can go back but however far we go back we will never

73
00:05:09,790 --> 00:05:13,280
 reach the beginning.

74
00:05:13,280 --> 00:05:18,320
 So there is no first beginning of karma and results.

75
00:05:18,320 --> 00:05:23,490
 So karma and results, that is the rolling on of karma and

76
00:05:23,490 --> 00:05:26,560
 results in this round of rebirths.

77
00:05:26,560 --> 00:05:30,150
 And not in the future round of births can they be shown not

78
00:05:30,150 --> 00:05:31,000
 to occur.

79
00:05:31,000 --> 00:05:37,920
 And in the future they will surely arise because when there

80
00:05:37,920 --> 00:05:41,160
 is karma at present then there

81
00:05:41,160 --> 00:05:43,920
 will be result of karma in the future.

82
00:05:43,920 --> 00:05:57,050
 Now in Sankhya philosophy which is one of the six classical

83
00:05:57,050 --> 00:06:01,800
 Hindu philosophies, it

84
00:06:01,800 --> 00:06:09,300
 is believed that the result already exists in the cause but

85
00:06:09,300 --> 00:06:12,560
 in an unmanifest form.

86
00:06:12,560 --> 00:06:17,120
 So when it becomes manifest it becomes the result.

87
00:06:17,120 --> 00:06:22,740
 So according to that philosophy the result is already in

88
00:06:22,740 --> 00:06:24,080
 the cause.

89
00:06:24,080 --> 00:06:31,520
 So the cause becomes mature and when it reaches the highest

90
00:06:31,520 --> 00:06:35,720
 stage of maturity it becomes the

91
00:06:35,720 --> 00:06:37,080
 result.

92
00:06:37,080 --> 00:06:45,360
 So in that philosophy karma or cause and result are same.

93
00:06:45,360 --> 00:06:48,640
 But Buddhism does not accept that.

94
00:06:48,640 --> 00:06:51,780
 In Buddhism it is thought that karma is one thing and

95
00:06:51,780 --> 00:06:53,200
 result is another.

96
00:06:53,200 --> 00:06:59,250
 So here Vishuddhi Maga said, "There is no karma in result

97
00:06:59,250 --> 00:07:03,040
 nor does result exist in karma."

98
00:07:03,040 --> 00:07:07,480
 So there is no karma in result and no result in karma.

99
00:07:07,480 --> 00:07:10,480
 They are just two different things.

100
00:07:10,480 --> 00:07:15,580
 Though they are white of one another, though they are

101
00:07:15,580 --> 00:07:18,520
 different from one another, there

102
00:07:18,520 --> 00:07:21,280
 is no fruit without the karma.

103
00:07:21,280 --> 00:07:25,640
 But there can be no result without the karma.

104
00:07:25,640 --> 00:07:29,080
 So they are related as cause and effect.

105
00:07:29,080 --> 00:07:32,240
 But cause is one thing and effect is another.

106
00:07:32,240 --> 00:07:37,400
 They are not identical.

107
00:07:37,400 --> 00:07:43,260
 As fire does not exist inside the sun, it can't go down,

108
00:07:43,260 --> 00:07:45,800
 nor yet outside them.

109
00:07:45,800 --> 00:07:51,060
 But it's brought to be by means of components or by means

110
00:07:51,060 --> 00:07:52,840
 of conditions.

111
00:07:52,840 --> 00:08:00,920
 Now when there is sunshine and there is cow dung or small

112
00:08:00,920 --> 00:08:04,880
 pieces of wood or pieces of

113
00:08:04,880 --> 00:08:10,340
 paper and you put a magnifying glass between them and

114
00:08:10,340 --> 00:08:14,040
 concentrate the rays on the pieces

115
00:08:14,040 --> 00:08:19,560
 of paper or cow dung, the fire is produced.

116
00:08:19,560 --> 00:08:26,310
 So here it says, "Fire does not exist inside the sun,

117
00:08:26,310 --> 00:08:30,520
 inside the gem or inside cow dung."

118
00:08:30,520 --> 00:08:35,730
 Because if there is, if fire exists in the sun, I mean the

119
00:08:35,730 --> 00:08:38,000
 rays of the sun or the gem

120
00:08:38,000 --> 00:08:40,880
 or cow dung, they will be burning.

121
00:08:40,880 --> 00:08:44,080
 They will always be burning.

122
00:08:44,080 --> 00:08:47,040
 But fire is not in them.

123
00:08:47,040 --> 00:08:51,560
 And so they are not burning when they are put separately.

124
00:08:51,560 --> 00:08:56,480
 But when they are put together, then there is fire.

125
00:08:56,480 --> 00:09:02,780
 So fire does not exist in the sun or in the gem or in the

126
00:09:02,780 --> 00:09:04,400
 cow dung.

127
00:09:04,400 --> 00:09:05,840
 Then where does it exist?

128
00:09:05,840 --> 00:09:07,960
 Outside there?

129
00:09:07,960 --> 00:09:12,320
 We do not say, we cannot say that fire exists somewhere

130
00:09:12,320 --> 00:09:15,080
 apart from these three things.

131
00:09:15,080 --> 00:09:19,910
 But fire is brought to be, fire is produced by means of the

132
00:09:19,910 --> 00:09:22,040
 components, by means of these

133
00:09:22,040 --> 00:09:23,040
 conditions.

134
00:09:23,040 --> 00:09:27,530
 The conditions here means the sun, a gem or a magnifying

135
00:09:27,530 --> 00:09:28,840
 glass and cow dung.

136
00:09:28,840 --> 00:09:33,680
 So when these three are put together, then there is fire.

137
00:09:33,680 --> 00:09:38,440
 But fire does not exist in the sun, gem and cow dung.

138
00:09:38,440 --> 00:09:43,080
 And without these three things, fire cannot be produced.

139
00:09:43,080 --> 00:09:49,230
 So in the same way, there is no result in the comma and no

140
00:09:49,230 --> 00:09:51,840
 comma in the result.

141
00:09:51,840 --> 00:09:56,040
 Results be found within the comma nor without.

142
00:09:56,040 --> 00:09:58,360
 Not that the comma still persists.

143
00:09:58,360 --> 00:10:01,440
 In the result, it is produced.

144
00:10:01,440 --> 00:10:08,740
 So there is no result found in the comma or outside the

145
00:10:08,740 --> 00:10:12,560
 comma or without the comma.

146
00:10:12,560 --> 00:10:16,830
 And the comma still persists, nor does the comma still

147
00:10:16,830 --> 00:10:18,280
 persist in the result.

148
00:10:18,280 --> 00:10:22,140
 So comma does not persist in the result.

149
00:10:22,140 --> 00:10:25,760
 That means comma is not in the result, it has produced.

150
00:10:25,760 --> 00:10:28,820
 The comma of its fruit is white.

151
00:10:28,820 --> 00:10:31,720
 So comma is white of its fruit.

152
00:10:31,720 --> 00:10:34,440
 Comma is white of the result.

153
00:10:34,440 --> 00:10:38,080
 No fruit exists yet in comma and there is no fruit in the

154
00:10:38,080 --> 00:10:38,840
 comma.

155
00:10:38,840 --> 00:10:43,560
 And still the fruit is born from it.

156
00:10:43,560 --> 00:10:49,000
 The result is born from comma, wholly depending on the

157
00:10:49,000 --> 00:10:49,840
 comma.

158
00:10:49,840 --> 00:10:57,370
 So wholly depending on the, totally depending on the comma,

159
00:10:57,370 --> 00:11:00,640
 the result is produced.

160
00:11:00,640 --> 00:11:04,960
 For here there is no God or Brahma, creator of the round of

161
00:11:04,960 --> 00:11:05,920
 rebirth.

162
00:11:05,920 --> 00:11:09,800
 So this round of rebirth is not the creation of a God or a

163
00:11:09,800 --> 00:11:12,520
 Brahma, but the unnatural outcome

164
00:11:12,520 --> 00:11:14,440
 of the comma.

165
00:11:14,440 --> 00:11:23,320
 So comma and its results, the phenomena alone flow on.

166
00:11:23,320 --> 00:11:27,730
 So they roll on and on and on, cause and component, their

167
00:11:27,730 --> 00:11:28,920
 condition.

168
00:11:28,920 --> 00:11:32,740
 So their condition is by their cause and their condition by

169
00:11:32,740 --> 00:11:35,360
 their condition, by the conditions.

170
00:11:35,360 --> 00:11:40,070
 So dependent upon the cause and conditions, these mental

171
00:11:40,070 --> 00:11:42,800
 and physical phenomena flow on

172
00:11:42,800 --> 00:11:44,600
 and on and on.

173
00:11:44,600 --> 00:11:49,980
 And in this flow on of mental and physical phenomena, which

174
00:11:49,980 --> 00:11:54,120
 is called the round of rebirth,

175
00:11:54,120 --> 00:12:01,990
 there is no God or no Brahman who creates this round of

176
00:12:01,990 --> 00:12:03,840
 rebirth.

177
00:12:03,840 --> 00:12:14,150
 So just mental and physical phenomena dependent upon the

178
00:12:14,150 --> 00:12:19,480
 conditions flowing on and on.

179
00:12:19,480 --> 00:12:22,600
 Some benefits of understanding the law of comma.

180
00:12:22,600 --> 00:12:26,840
 So we, as Buddhists we understand the law of comma

181
00:12:26,840 --> 00:12:30,200
 following the teachings of the Buddha.

182
00:12:30,200 --> 00:12:34,750
 And this understanding of law of comma has many benefits

183
00:12:34,750 --> 00:12:37,040
 and here are some of them.

184
00:12:37,040 --> 00:12:42,180
 So it satisfies our curiosity about inequality among beings

185
00:12:42,180 --> 00:12:42,440
.

186
00:12:42,440 --> 00:12:46,840
 So we want to know why beings are different, why beings are

187
00:12:46,840 --> 00:12:49,120
 not equal, although they are

188
00:12:49,120 --> 00:12:55,000
 equal as beings, as human beings or animals, as animals.

189
00:12:55,000 --> 00:13:04,730
 And this inequality among beings is satisfactorily

190
00:13:04,730 --> 00:13:10,800
 explained by the law of comma.

191
00:13:10,800 --> 00:13:16,890
 The understanding of law of comma satisfies our curiosity

192
00:13:16,890 --> 00:13:20,120
 about inequality among beings.

193
00:13:20,120 --> 00:13:31,620
 It is no wonder that understanding of law of comma has

194
00:13:31,620 --> 00:13:39,320
 converted many people to Buddhism.

195
00:13:39,320 --> 00:13:43,080
 And understanding of law of comma gives us hope.

196
00:13:43,080 --> 00:13:54,360
 There is always hope that we can be better.

197
00:13:54,360 --> 00:14:04,400
 We are not to be satisfied with what condition we are in,

198
00:14:04,400 --> 00:14:08,200
 but we can always improve our condition

199
00:14:08,200 --> 00:14:12,320
 by doing good comma.

200
00:14:12,320 --> 00:14:17,310
 So it gives us the understanding of the law of comma gives

201
00:14:17,310 --> 00:14:18,320
 us hope.

202
00:14:18,320 --> 00:14:23,120
 So we are never hopeless.

203
00:14:23,120 --> 00:14:26,860
 And the understanding of law of comma can give us

204
00:14:26,860 --> 00:14:29,400
 consolation in times of misfortune.

205
00:14:29,400 --> 00:14:35,550
 Now people meet with misfortune many times in their lives,

206
00:14:35,550 --> 00:14:39,280
 loss of wealth, loss of business,

207
00:14:39,280 --> 00:14:48,240
 loss of job, loss of friends, loss of loved ones.

208
00:14:48,240 --> 00:14:51,450
 And when they meet with such misfortunes, those who do not

209
00:14:51,450 --> 00:14:53,080
 understand the law of comma

210
00:14:53,080 --> 00:14:56,640
 cannot be consoled.

211
00:14:56,640 --> 00:15:04,250
 They might even become very dejected and they might even

212
00:15:04,250 --> 00:15:07,560
 take their own lives.

213
00:15:07,560 --> 00:15:10,950
 But those who understand the law of comma can understand

214
00:15:10,950 --> 00:15:12,400
 the misfortune because this

215
00:15:12,400 --> 00:15:20,160
 misfortune comes as a result of what they did in the past.

216
00:15:20,160 --> 00:15:26,160
 And so there is no other person to blame but themselves.

217
00:15:26,160 --> 00:15:36,320
 And so people can be consoled with the understanding of law

218
00:15:36,320 --> 00:15:38,480
 of comma.

219
00:15:38,480 --> 00:15:42,160
 And it gives us strength to meet the ups and downs of life

220
00:15:42,160 --> 00:15:43,720
 without agitation.

221
00:15:43,720 --> 00:15:47,640
 Now when we live in life, there are ups and downs in our

222
00:15:47,640 --> 00:15:48,440
 lives.

223
00:15:48,440 --> 00:15:55,900
 We cannot avoid meeting success, meeting failure in our

224
00:15:55,900 --> 00:15:57,200
 lives.

225
00:15:57,200 --> 00:16:03,160
 So whether we meet with success or failure, we can stand

226
00:16:03,160 --> 00:16:04,120
 firm.

227
00:16:04,120 --> 00:16:12,500
 We can meet these situations without much agitation.

228
00:16:12,500 --> 00:16:21,390
 And it is important that we be without agitation when we

229
00:16:21,390 --> 00:16:26,080
 meet with success or failure.

230
00:16:26,080 --> 00:16:31,910
 Because now when people meet with success, they are elated

231
00:16:31,910 --> 00:16:34,840
 and when they meet with failure,

232
00:16:34,840 --> 00:16:36,760
 they are dejected.

233
00:16:36,760 --> 00:16:41,360
 And so their lives become miserable.

234
00:16:41,360 --> 00:16:50,700
 But if we understand the law of comma, we can take them as

235
00:16:50,700 --> 00:16:56,880
 they come without much agitation.

236
00:16:56,880 --> 00:17:02,840
 It teaches us to realize our potential to create our own

237
00:17:02,840 --> 00:17:04,080
 future.

238
00:17:04,080 --> 00:17:10,660
 Now we get the results of our own comma.

239
00:17:10,660 --> 00:17:18,300
 So we can create our own future.

240
00:17:18,300 --> 00:17:23,780
 We have the potential to create our own future if we

241
00:17:23,780 --> 00:17:27,280
 understand the law of comma.

242
00:17:27,280 --> 00:17:37,220
 And not only realizing the potential, but we can use this

243
00:17:37,220 --> 00:17:42,320
 potential to make our future

244
00:17:42,320 --> 00:17:46,400
 lives better than the present ones.

245
00:17:46,400 --> 00:17:53,060
 So it teaches us that we beings have this potential to

246
00:17:53,060 --> 00:17:56,320
 create our own future.

247
00:17:56,320 --> 00:18:07,130
 And we need not look to other external powers or deities

248
00:18:07,130 --> 00:18:11,240
 for our own future.

249
00:18:11,240 --> 00:18:18,920
 Actually we alone can create our future.

250
00:18:18,920 --> 00:18:27,290
 And the understanding of law of comma teaches individual

251
00:18:27,290 --> 00:18:30,320
 responsibility.

252
00:18:30,320 --> 00:18:39,090
 Since the results we experience are caused by our own comma

253
00:18:39,090 --> 00:18:43,280
, we alone are responsible

254
00:18:43,280 --> 00:18:51,080
 for our enjoyment or our suffering.

255
00:18:51,080 --> 00:18:59,650
 So since we are responsible for our own happiness or

256
00:18:59,650 --> 00:19:07,080
 suffering, we are not to blame others for

257
00:19:07,080 --> 00:19:09,080
 our conditions.

258
00:19:09,080 --> 00:19:15,840
 So it teaches that we are responsible for ourselves.

259
00:19:15,840 --> 00:19:24,420
 And if we want to be better in the future, we must do or we

260
00:19:24,420 --> 00:19:28,320
 must acquire good comma.

261
00:19:28,320 --> 00:19:39,880
 So we become responsible persons if we understand the law

262
00:19:39,880 --> 00:19:42,360
 of comma.

263
00:19:42,360 --> 00:19:46,830
 And it teaches us to avoid harming other beings and to be

264
00:19:46,830 --> 00:19:49,160
 compassionate to other beings.

265
00:19:49,160 --> 00:19:57,140
 Now if we harm other beings, we will suffer the painful

266
00:19:57,140 --> 00:20:01,560
 consequences of our comma.

267
00:20:01,560 --> 00:20:06,110
 So if we do not want to suffer ourselves, then we do not

268
00:20:06,110 --> 00:20:08,840
 inflict suffering on others.

269
00:20:08,840 --> 00:20:15,870
 And so it helps us to avoid harming or injuring the other

270
00:20:15,870 --> 00:20:17,320
 beings.

271
00:20:17,320 --> 00:20:21,720
 And it teaches us to be compassionate to other beings.

272
00:20:21,720 --> 00:20:29,940
 Because by being compassionate to other beings, we develop

273
00:20:29,940 --> 00:20:34,120
 good comma which will give good

274
00:20:34,120 --> 00:20:37,040
 results in the future.

275
00:20:37,040 --> 00:20:45,800
 So understanding the law of comma can help us to make the

276
00:20:45,800 --> 00:20:50,480
 society more humane and more

277
00:20:50,480 --> 00:20:54,160
 peaceful and more harmonious.

278
00:20:54,160 --> 00:21:00,590
 There may be some more benefits of understanding the law of

279
00:21:00,590 --> 00:21:01,720
 comma.

280
00:21:01,720 --> 00:21:09,840
 So I want you to make additions to this list yourselves.

281
00:21:09,840 --> 00:21:17,310
 So we have now studied comma in general, what comma is and

282
00:21:17,310 --> 00:21:21,160
 what we can do about comma and

283
00:21:21,160 --> 00:21:23,880
 so on.

284
00:21:23,880 --> 00:21:32,680
 Next time we study the different kinds of comma.

285
00:21:32,680 --> 00:21:33,640
 You asked me on Sunday.

286
00:21:33,640 --> 00:21:34,640
 [ Laughter ]

