1
00:00:00,000 --> 00:00:27,000
 [ Silence ]

2
00:00:27,500 --> 00:00:32,500
 Okay. Good evening everyone.

3
00:00:32,500 --> 00:00:37,000
 Broadcasting live, I think.

4
00:00:37,000 --> 00:00:43,000
 Yeah, broadcasting live.

5
00:00:43,000 --> 00:00:50,000
 Today we're doing Dhammapada.

6
00:00:50,000 --> 00:00:53,000
 So...

7
00:00:56,000 --> 00:00:59,000
 You all can listen and...

8
00:00:59,000 --> 00:01:03,000
 fill your video up.

9
00:01:03,000 --> 00:01:12,500
 On YouTube.

10
00:01:12,500 --> 00:01:22,500
 [ Silence ]

11
00:01:22,500 --> 00:01:51,500
 [ Silence ]

12
00:01:52,000 --> 00:01:55,770
 Okay. So I'm just going to go ahead and start a little

13
00:01:55,770 --> 00:01:57,000
 early here.

14
00:01:57,000 --> 00:02:00,000
 I think.

15
00:02:01,000 --> 00:02:09,000
 [ Silence ]

16
00:02:09,000 --> 00:02:19,000
 [ Silence ]

17
00:02:21,000 --> 00:02:29,000
 [ Silence ]

18
00:02:29,000 --> 00:02:39,000
 [ Silence ]

19
00:02:39,000 --> 00:03:04,000
 [ Silence ]

20
00:03:04,000 --> 00:03:07,500
 Hello and welcome back to our study of the Dhammapada.

21
00:03:08,000 --> 00:03:12,000
 Today we continue on with verse number 127.

22
00:03:12,000 --> 00:03:15,000
 Which reads as follows.

23
00:03:15,000 --> 00:03:37,000
 [ Silence ]

24
00:03:37,000 --> 00:03:39,500
 Which means...

25
00:03:39,500 --> 00:03:44,500
 [ Silence ]

26
00:03:44,500 --> 00:03:55,500
 Not up in the sky or in the middle of the ocean.

27
00:03:55,500 --> 00:04:02,500
 [ Silence ]

28
00:04:03,000 --> 00:04:11,580
 Dhammapada not having entered into a cave, a cavern in a

29
00:04:11,580 --> 00:04:14,990
 mountain, entered into the middle of a mountain, center of

30
00:04:14,990 --> 00:04:16,500
 a mountain.

31
00:04:16,500 --> 00:04:24,500
 [ Silence ]

32
00:04:25,000 --> 00:04:33,270
 It cannot be found such a place on this earth. One cannot

33
00:04:33,270 --> 00:04:34,500
 find a place on this earth. On the earth.

34
00:04:34,500 --> 00:04:41,500
 [ Silence ]

35
00:04:41,500 --> 00:04:48,310
 One might be free from evil karma, evil deeds. There's

36
00:04:48,310 --> 00:04:49,500
 nowhere.

37
00:04:49,500 --> 00:04:53,500
 Not up in the sky, not under the ocean.

38
00:04:53,500 --> 00:05:01,000
 Not in the deepest, darkest cave. There's nowhere on earth.

39
00:05:01,000 --> 00:05:03,000
 That's the quote.

40
00:05:03,000 --> 00:05:11,000
 So an interesting story. Actually three stories. That goes

41
00:05:11,000 --> 00:05:13,000
 with this verse.

42
00:05:14,000 --> 00:05:21,200
 It seems there were three groups of monks. And so we have

43
00:05:21,200 --> 00:05:23,500
 three stories.

44
00:05:23,500 --> 00:05:27,500
 The Buddha was living in Jetavana but...

45
00:05:27,500 --> 00:05:32,870
 A first group of monks set out to meet the Buddha and on

46
00:05:32,870 --> 00:05:37,250
 their way they entered a village, a certain village for al

47
00:05:37,250 --> 00:05:37,500
ms.

48
00:05:37,500 --> 00:05:45,270
 And while they were sitting or after they had eaten, or

49
00:05:45,270 --> 00:05:49,000
 while they were waiting for food,

50
00:05:49,000 --> 00:05:55,040
 someone who was cooking food in the morning made their fire

51
00:05:55,040 --> 00:06:04,000
 too hot and suddenly the flame burst up and lit the satch,

52
00:06:04,500 --> 00:06:09,640
 the roof of their hut, and the satch went flying up in a

53
00:06:09,640 --> 00:06:14,000
 ball of flame or floating through the air,

54
00:06:14,000 --> 00:06:17,000
 being carried away by the wind.

55
00:06:17,000 --> 00:06:23,350
 And at that moment a crow was flying and so this tuft of s

56
00:06:23,350 --> 00:06:30,000
atch caught the crow and immediately burst into flames.

57
00:06:30,500 --> 00:06:34,020
 It was on fire, so the crow burnt to a crisp, fell to the

58
00:06:34,020 --> 00:06:38,000
 ground, dead, right in front of the monks.

59
00:06:38,000 --> 00:06:43,770
 And they thought it was kind of remarkable because just one

60
00:06:43,770 --> 00:06:48,880
 tuft of satch flew up and caught the crow squarely as it

61
00:06:48,880 --> 00:06:52,000
 was flying by.

62
00:06:53,000 --> 00:06:57,930
 And so they wondered to themselves, sort of monk talk,

63
00:06:57,930 --> 00:07:02,860
 wonder what the karma of this bird was, that this would

64
00:07:02,860 --> 00:07:03,500
 happen.

65
00:07:03,500 --> 00:07:06,860
 And they said, "Who would know besides the Buddha?" So when

66
00:07:06,860 --> 00:07:08,500
 we get there we should ask him.

67
00:07:08,500 --> 00:07:15,500
 And so they continued on their way. That's the first group.

68
00:07:17,500 --> 00:07:21,580
 The second group of monks also on their way to see the

69
00:07:21,580 --> 00:07:23,010
 Buddha. I mean it seems like probably this was a thing

70
00:07:23,010 --> 00:07:25,960
 where monks would come to the Buddha with these sorts of

71
00:07:25,960 --> 00:07:26,500
 questions.

72
00:07:26,500 --> 00:07:29,110
 So I think we shouldn't be surprised that there were three

73
00:07:29,110 --> 00:07:30,000
 groups of them.

74
00:07:30,000 --> 00:07:34,280
 The second group, on their way to see the Buddha, they took

75
00:07:34,280 --> 00:07:39,000
 a boat across the ocean somehow.

76
00:07:39,000 --> 00:07:41,880
 Not sure where they were, maybe they were in Burma, or

77
00:07:41,880 --> 00:07:45,970
 Burma's attached, maybe Thailand, maybe they were in Sri

78
00:07:45,970 --> 00:07:47,500
 Lanka, who knows?

79
00:07:47,500 --> 00:07:53,970
 Probably not. It's interesting to think of them being on a

80
00:07:53,970 --> 00:07:59,800
 ship because there's not much record of monks outside of

81
00:07:59,800 --> 00:08:00,500
 India.

82
00:08:00,500 --> 00:08:07,980
 But I'm really not clear about that sort of thing. But it

83
00:08:07,980 --> 00:08:12,740
 happened that in the middle of the ocean their boat stopped

84
00:08:12,740 --> 00:08:18,000
, the wind stopped, and they couldn't sail anymore.

85
00:08:18,000 --> 00:08:22,570
 And this was a thing for sailors that if the wind stopped

86
00:08:22,570 --> 00:08:27,330
 for a long period of time they would get superstitious and

87
00:08:27,330 --> 00:08:32,060
 they would think somebody on board, their karma is not

88
00:08:32,060 --> 00:08:34,500
 allowing us to continue.

89
00:08:34,500 --> 00:08:38,220
 And so they'd throw that person overboard. If they could

90
00:08:38,220 --> 00:08:40,890
 figure out who it was, and the way they did it was they

91
00:08:40,890 --> 00:08:42,500
 found the scientific method.

92
00:08:42,500 --> 00:08:47,090
 They'd draw lots, and whoever drew the shortest straw would

93
00:08:47,090 --> 00:08:49,000
 get thrown overboard.

94
00:08:49,000 --> 00:08:52,000
 Seems reasonable, no?

95
00:08:52,000 --> 00:08:56,060
 So they did this, and they thought, "Well whoever gets the

96
00:08:56,060 --> 00:09:00,150
 shortest straw gets thrown over, that's just the way karma

97
00:09:00,150 --> 00:09:01,000
 works."

98
00:09:01,000 --> 00:09:03,000
 I guess.

99
00:09:05,000 --> 00:09:10,300
 Except, lo and behold, the captain's wife drew the lot. Now

100
00:09:10,300 --> 00:09:14,680
 the captain's wife was loved by everyone. She was young,

101
00:09:14,680 --> 00:09:18,500
 she was pretty, she was kind, she was great.

102
00:09:18,500 --> 00:09:21,940
 Just an all-around, not the sort of person you want to

103
00:09:21,940 --> 00:09:24,500
 throw overboard, to say the least.

104
00:09:24,500 --> 00:09:27,400
 Especially since she was the captain's wife, and so

105
00:09:27,400 --> 00:09:30,500
 everyone agreed they couldn't throw her overboard.

106
00:09:30,500 --> 00:09:34,500
 So they said, "Well we'll draw lots again."

107
00:09:34,500 --> 00:09:41,290
 Again they drew lots, and again, for a second time, the

108
00:09:41,290 --> 00:09:46,500
 captain's wife drew the shortest straw.

109
00:09:46,500 --> 00:09:49,620
 And they still couldn't, they said, "There's no way we can

110
00:09:49,620 --> 00:09:50,500
't do this."

111
00:09:50,500 --> 00:09:56,140
 And so a third time they drew straws, and a third time the

112
00:09:56,140 --> 00:10:00,500
 captain's wife drew the shortest straw.

113
00:10:00,500 --> 00:10:06,650
 So they went to the captain and they said, "This is what

114
00:10:06,650 --> 00:10:11,700
 happened three times, it's got to be her, she's the one

115
00:10:11,700 --> 00:10:16,500
 with bad luck, we have to throw her overboard."

116
00:10:16,500 --> 00:10:19,990
 And so they grabbed her, and the captain said, "Well then,

117
00:10:19,990 --> 00:10:23,820
 yes, I guess that's how it has to go, scientifically shown

118
00:10:23,820 --> 00:10:27,500
 that she's to blame for the wind."

119
00:10:27,500 --> 00:10:33,500
 And so they said, "Throw overboard."

120
00:10:33,500 --> 00:10:38,040
 And as they started throwing overboard, she started

121
00:10:38,040 --> 00:10:40,500
 screaming, reasonably.

122
00:10:40,500 --> 00:10:44,050
 She doesn't seem to be that confident in the scientific

123
00:10:44,050 --> 00:10:48,250
 method that they used, but she certainly doesn't seem to

124
00:10:48,250 --> 00:10:50,500
 have wanted to be thrown overboard.

125
00:10:50,500 --> 00:10:56,620
 So interestingly, the captain has them take her jewels away

126
00:10:56,620 --> 00:11:00,500
, says, "Well there's no need to."

127
00:11:00,500 --> 00:11:03,760
 When he heard this he saw, and he said, "Oh there's no need

128
00:11:03,760 --> 00:11:05,500
 for her jewels to go away."

129
00:11:05,500 --> 00:11:08,680
 So he took her jewels and had them wrap her up in a cloth

130
00:11:08,680 --> 00:11:10,500
 and tie a rope around her neck,

131
00:11:10,500 --> 00:11:14,920
 so that she couldn't, or wrap her up in a cloth so that she

132
00:11:14,920 --> 00:11:19,710
 wouldn't scream, and then tie a rope around her neck and

133
00:11:19,710 --> 00:11:27,590
 tie it to a big heavy pot of sand so that they wouldn't see

134
00:11:27,590 --> 00:11:28,500
 her.

135
00:11:28,500 --> 00:11:35,530
 So that he wouldn't have to see her, because he was fond of

136
00:11:35,530 --> 00:11:36,500
 her.

137
00:11:36,500 --> 00:11:40,660
 So they threw overboard, and as soon as she hit the ocean,

138
00:11:40,660 --> 00:11:44,890
 sharks and turtles and fish and so on, ate her tore her to

139
00:11:44,890 --> 00:11:46,500
 bits and she died.

140
00:11:46,500 --> 00:11:49,480
 And the monks on board were watching this, and of course

141
00:11:49,480 --> 00:11:51,500
 didn't really have a say in it all.

142
00:11:51,500 --> 00:11:56,350
 But they were shocked as well. They couldn't believe that

143
00:11:56,350 --> 00:11:58,500
 this sort of thing could happen,

144
00:11:58,500 --> 00:12:02,140
 and that she couldn't really be at the mercy of these

145
00:12:02,140 --> 00:12:06,500
 people, because it was a bit of a coincidence that she drew

146
00:12:06,500 --> 00:12:08,500
 the short straw three times.

147
00:12:08,500 --> 00:12:13,110
 That's quite, unless there were only like a few people on

148
00:12:13,110 --> 00:12:16,500
 board, that was quite a coincidence.

149
00:12:16,500 --> 00:12:24,030
 And so they said, "I wonder what karma she did to deserve

150
00:12:24,030 --> 00:12:27,500
 such a horrible fate?"

151
00:12:27,500 --> 00:12:35,530
 And likewise they said, "Well, let's ask the Buddha and

152
00:12:35,530 --> 00:12:37,500
 find out."

153
00:12:37,500 --> 00:12:40,910
 It's important to point out as we go along, because I'm

154
00:12:40,910 --> 00:12:43,500
 sure the question coming up in people's minds is,

155
00:12:43,500 --> 00:12:45,820
 "Well, what about the people who did that to her?" I mean,

156
00:12:45,820 --> 00:12:48,500
 it's not karma, it's those people.

157
00:12:48,500 --> 00:12:52,630
 And it has to be mentioned that karma isn't like one thing

158
00:12:52,630 --> 00:12:56,920
 in the past, you blame things in your past life. That's not

159
00:12:56,920 --> 00:12:57,500
 true at all.

160
00:12:57,500 --> 00:13:01,870
 Those people who threw that woman into the ocean did a very

161
00:13:01,870 --> 00:13:04,500
, very bad thing. There's no question about it.

162
00:13:04,500 --> 00:13:08,500
 That was an evil deed. Buddhism doesn't condone that.

163
00:13:08,500 --> 00:13:16,530
 But how she got herself in that situation, you see, where

164
00:13:16,530 --> 00:13:25,160
 the likelihood of her being subject to that, because these

165
00:13:25,160 --> 00:13:29,500
 people were not doing it out of hate for her.

166
00:13:29,500 --> 00:13:35,210
 They were doing it out of ignorance and superstition, but

167
00:13:35,210 --> 00:13:39,500
 they didn't just randomly pick someone.

168
00:13:39,500 --> 00:13:43,380
 So how did she get herself in that situation? The theory is

169
00:13:43,380 --> 00:13:48,300
 that there's more behind it. How our life comes to these

170
00:13:48,300 --> 00:13:49,500
 points.

171
00:13:49,500 --> 00:13:56,500
 Anyway, that was the second one.

172
00:13:56,500 --> 00:14:02,790
 The third story, there were seven monks who likewise set

173
00:14:02,790 --> 00:14:09,360
 out to see the Buddha, and on their way, they came to a

174
00:14:09,360 --> 00:14:13,500
 certain monastery and they asked to stay the night.

175
00:14:13,500 --> 00:14:18,410
 And the seven of them were invited to stay in a special

176
00:14:18,410 --> 00:14:23,560
 cave in the side of the mountain that was designated for

177
00:14:23,560 --> 00:14:25,500
 visiting monks.

178
00:14:25,500 --> 00:14:29,680
 So they went there, and they settled down, and they fell

179
00:14:29,680 --> 00:14:31,500
 asleep for the night.

180
00:14:31,500 --> 00:14:35,540
 During the night, a huge boulder, it says the size of a pag

181
00:14:35,540 --> 00:14:40,490
oda, which would be very, very large, fell down the mountain

182
00:14:40,490 --> 00:14:45,630
 and covered the entrance to the cave where they were

183
00:14:45,630 --> 00:14:46,500
 staying.

184
00:14:46,500 --> 00:14:55,710
 Just out of the blue, blocking their exit, making it

185
00:14:55,710 --> 00:15:01,500
 impossible for them to get out.

186
00:15:01,500 --> 00:15:05,500
 When the resident monks found out what happened, they said,

187
00:15:05,500 --> 00:15:08,880
 "We've got to move that rock. There's monks trapped in

188
00:15:08,880 --> 00:15:09,500
 there."

189
00:15:09,500 --> 00:15:15,040
 And so they gathered men, strong people from all around the

190
00:15:15,040 --> 00:15:20,790
 countryside, and they worked tirelessly for seven days to

191
00:15:20,790 --> 00:15:24,500
 remove this rock, but the rock wouldn't budge.

192
00:15:24,500 --> 00:15:28,720
 Until finally, on the seventh day, after seven days, the

193
00:15:28,720 --> 00:15:32,860
 rock moved as though it had never been stuck there. The

194
00:15:32,860 --> 00:15:35,500
 rock moved very easily.

195
00:15:35,500 --> 00:15:40,190
 It even says that it moved by itself, away from the

196
00:15:40,190 --> 00:15:49,500
 entrance. It just suddenly became dislodged.

197
00:15:49,500 --> 00:15:53,770
 And so these seven monks had spent seven days without food,

198
00:15:53,770 --> 00:15:56,500
 without water, were almost dead.

199
00:15:56,500 --> 00:16:00,830
 And yet when they came out, they were able to get water and

200
00:16:00,830 --> 00:16:05,300
 food and survive. But they said to themselves, "I wonder

201
00:16:05,300 --> 00:16:06,500
 what we did."

202
00:16:06,500 --> 00:16:10,090
 It seems a very strange sort of thing to happen. I wonder

203
00:16:10,090 --> 00:16:12,500
 if this is a cause of past karma.

204
00:16:12,500 --> 00:16:15,500
 And so likewise, they decided to ask the Buddha.

205
00:16:15,500 --> 00:16:19,680
 So these three groups of monks met up, and this is the

206
00:16:19,680 --> 00:16:24,690
 story. And then the Buddha tells three stories about their

207
00:16:24,690 --> 00:16:25,500
 pasts.

208
00:16:25,500 --> 00:16:28,340
 The first, the past of the crow, they go to see the Buddha,

209
00:16:28,340 --> 00:16:36,550
 and the Buddha says, "The crow is suffering for past deeds

210
00:16:36,550 --> 00:16:37,500
."

211
00:16:37,500 --> 00:16:41,750
 And it seems like the story is kind of suggesting that it's

212
00:16:41,750 --> 00:16:45,640
 not just one past deed, but it's sort of a habit of bad

213
00:16:45,640 --> 00:16:46,500
 deeds.

214
00:16:46,500 --> 00:16:51,780
 But he gives examples. So he says, "For a long time ago,

215
00:16:51,780 --> 00:16:56,480
 the crow was a farmer, and he had an ox, and he was trying

216
00:16:56,480 --> 00:16:58,500
 to get this ox to do work for him.

217
00:16:58,500 --> 00:17:05,540
 But try as he might, he couldn't tame the ox. He'd get it

218
00:17:05,540 --> 00:17:08,300
 to work, and then it would work for a little bit, and then

219
00:17:08,300 --> 00:17:10,500
 it would go lie down.

220
00:17:10,500 --> 00:17:15,810
 And then he'd get it to move, and it wouldn't move. This ox

221
00:17:15,810 --> 00:17:17,500
 was just terribly, terribly stubborn.

222
00:17:17,500 --> 00:17:21,580
 And so he got increasingly more and more angry, until he

223
00:17:21,580 --> 00:17:28,170
 finally got angry enough that the ox just lay down, that he

224
00:17:28,170 --> 00:17:31,500
 just covered it up in straw and lit it on fire.

225
00:17:31,500 --> 00:17:36,590
 And the Buddha said, "Because of that, he will be cruel. He

226
00:17:36,590 --> 00:17:38,500
 will be, and he was born in hell, actually.

227
00:17:38,500 --> 00:17:41,930
 And after being born in hell, he was born back in the human

228
00:17:41,930 --> 00:17:45,500
 realm, and he was born back in the animal realm as a crow,

229
00:17:45,500 --> 00:17:52,500
 and still suffering from it to this day."

230
00:17:52,500 --> 00:17:58,240
 In fact, it says he was seven times in succession, reborn

231
00:17:58,240 --> 00:17:59,500
 as a crow.

232
00:17:59,500 --> 00:18:04,010
 And then we have the story of the woman on the boat. This

233
00:18:04,010 --> 00:18:09,460
 woman in the past, she was a woman who lived in Benares,

234
00:18:09,460 --> 00:18:12,500
 Varanasi as it's known now.

235
00:18:12,500 --> 00:18:17,930
 And she had a dog. She was a housewife, and so she did all

236
00:18:17,930 --> 00:18:22,220
 these chores, but there was a dog in the house that would

237
00:18:22,220 --> 00:18:24,500
 follow her around everywhere.

238
00:18:24,500 --> 00:18:29,910
 And for some reason, people would tease her because this

239
00:18:29,910 --> 00:18:35,630
 dog was following her like a shadow, and very, very much,

240
00:18:35,630 --> 00:18:38,500
 very, very affectionate.

241
00:18:38,500 --> 00:18:41,860
 Actually, like normal dogs are, but people were joking

242
00:18:41,860 --> 00:18:44,610
 about it, because I guess it wasn't a big thing for women

243
00:18:44,610 --> 00:18:46,500
 to have dogs following them around.

244
00:18:46,500 --> 00:18:51,400
 In fact, it was a common thing for hunters to have dogs, as

245
00:18:51,400 --> 00:18:54,500
 we learned in our previous story.

246
00:18:54,500 --> 00:18:59,840
 So these young men joked about it and said, "Oh, here comes

247
00:18:59,840 --> 00:19:05,920
 the hunter with their dog. We're going to have meat to eat

248
00:19:05,920 --> 00:19:07,500
 tonight."

249
00:19:07,500 --> 00:19:11,630
 There would be meat coming, just joking about her looking

250
00:19:11,630 --> 00:19:15,500
 like a hunter, having this big dog go along with it.

251
00:19:15,500 --> 00:19:22,000
 And this woman was, I guess, of a cruel event, and so

252
00:19:22,000 --> 00:19:26,760
 getting angry and feeling embarrassed and ashamed, she

253
00:19:26,760 --> 00:19:30,500
 picked up a stick and beat the dog almost to death.

254
00:19:30,500 --> 00:19:34,560
 But dogs, being the way they are, have a funny resiliency

255
00:19:34,560 --> 00:19:39,920
 to these sorts of things, and so the dog was actually unm

256
00:19:39,920 --> 00:19:44,500
oved and was still very much in love with this woman.

257
00:19:44,500 --> 00:19:49,450
 It turns out, actually, the commentary says that this dog

258
00:19:49,450 --> 00:19:54,760
 used to be her husband, and so that was a reason, was

259
00:19:54,760 --> 00:19:58,500
 recently her husband in one of her recent births.

260
00:19:58,500 --> 00:20:02,670
 And so, even though it's impossible to find, they say, you

261
00:20:02,670 --> 00:20:05,650
 can't find someone who hasn't been your husband, your wife,

262
00:20:05,650 --> 00:20:09,500
 your son, your daughter, your mother, your father, but

263
00:20:09,500 --> 00:20:14,650
 recent births, there tends to still be some sort of

264
00:20:14,650 --> 00:20:23,500
 affinity or enmity in cases when there was enmity before.

265
00:20:23,500 --> 00:20:28,990
 And so she beat this dog, and it still came back, and so

266
00:20:28,990 --> 00:20:35,500
 she was increasingly angry, irrationally angry at this dog.

267
00:20:35,500 --> 00:20:41,480
 And so, lo and behold, when it came close, she picked up a

268
00:20:41,480 --> 00:20:46,500
 rope and she made a loop and waited for the dog.

269
00:20:46,500 --> 00:20:50,120
 And when the dog came close, she wrapped the loop around

270
00:20:50,120 --> 00:20:53,570
 the dog and tied it to a pot full of sand and threw the pot

271
00:20:53,570 --> 00:20:57,170
 of sand into this big pool, and it rolled down into the

272
00:20:57,170 --> 00:21:02,390
 pool, and the dog was pulled and it was dragged after it

273
00:21:02,390 --> 00:21:05,500
 into the pool and it drowned.

274
00:21:05,500 --> 00:21:12,200
 And that was the karma that caused her to be thrown

275
00:21:12,200 --> 00:21:19,500
 overboard, as well as be spent many years in hell.

276
00:21:19,500 --> 00:21:38,650
 Okay, that's story number two. Story number three, he tells

277
00:21:38,650 --> 00:21:38,970
, these eumunks also have done bad things in the past, and

278
00:21:38,970 --> 00:21:39,270
 so at one time you were cowherds, and you came upon this

279
00:21:39,270 --> 00:21:42,750
 huge lizard, and I guess it was something that they would

280
00:21:42,750 --> 00:21:45,500
 like to eat, people would like to eat,

281
00:21:45,500 --> 00:21:49,500
 and so they ran after it, trying to catch this big lizard,

282
00:21:49,500 --> 00:21:54,500
 but it ran into an ant hill that had seven holes.

283
00:21:54,500 --> 00:21:58,140
 For some reason, seven is a big number. I think it probably

284
00:21:58,140 --> 00:22:03,020
 just means there were a bunch of holes, and so they plugged

285
00:22:03,020 --> 00:22:06,500
 up all these holes,

286
00:22:06,500 --> 00:22:09,120
 and then they, you know, thinking that they could catch it

287
00:22:09,120 --> 00:22:11,670
 at one of the holes, but then they said, you know, we just

288
00:22:11,670 --> 00:22:14,130
 don't have time for this, we'll plug up all the holes and

289
00:22:14,130 --> 00:22:16,600
 we'll come back tomorrow, and we'll catch this lizard,

290
00:22:16,600 --> 00:22:19,180
 because there's now no way out of this big, I guess a term

291
00:22:19,180 --> 00:22:22,500
ite mound or something, something where lizards,

292
00:22:22,500 --> 00:22:26,000
 I don't know, something that lizards like to stay in. Big

293
00:22:26,000 --> 00:22:28,500
 lizard though.

294
00:22:28,500 --> 00:22:30,880
 So we'll come back tomorrow and we'll catch him. So they

295
00:22:30,880 --> 00:22:34,060
 went home, but then they forgot all about it, and so for

296
00:22:34,060 --> 00:22:37,610
 seven days they went and bought their business tending cows

297
00:22:37,610 --> 00:22:39,660
 elsewhere, but then on the seventh day they came back and

298
00:22:39,660 --> 00:22:43,130
 they were tending cows, their cows, and they saw the ant

299
00:22:43,130 --> 00:22:46,640
 till again, and they realized, oh, I wonder what happened

300
00:22:46,640 --> 00:22:47,500
 to the lizard.

301
00:22:47,500 --> 00:22:52,050
 And so they opened up the holes and the lizard, at this

302
00:22:52,050 --> 00:22:57,200
 point, starved and dehydrated, not afraid of its, for its

303
00:22:57,200 --> 00:23:02,510
 life at all, and it's basically at the end of its tether,

304
00:23:02,510 --> 00:23:03,500
 had to come out.

305
00:23:03,500 --> 00:23:08,200
 And so came out and they, they said to themselves, they

306
00:23:08,200 --> 00:23:13,200
 felt, they took pity on it, they said, oh, let's not kill

307
00:23:13,200 --> 00:23:17,500
 it, this poor thing, we tortured it terribly.

308
00:23:17,500 --> 00:23:20,620
 So they nursed it and they actually brought it back to life

309
00:23:20,620 --> 00:23:23,430
, and he said, the Buddha said, you know, see, because of

310
00:23:23,430 --> 00:23:26,330
 that you were able to escape, because you came back for

311
00:23:26,330 --> 00:23:30,500
 this lizard, if not, that would have been it for you.

312
00:23:30,500 --> 00:23:34,980
 I think these stories are interesting, whether you believe

313
00:23:34,980 --> 00:23:39,110
 them or not, but they give some idea of the nature of karma

314
00:23:39,110 --> 00:23:42,990
 according to Buddhism and some of the ways, they're just

315
00:23:42,990 --> 00:23:43,500
 examples.

316
00:23:43,500 --> 00:23:46,290
 Doesn't mean they're not law, like this has to be like this

317
00:23:46,290 --> 00:23:51,270
, but apparently the way things sometimes turn out, like our

318
00:23:51,270 --> 00:23:55,760
 past needs influence, both in this life and the next, they

319
00:23:55,760 --> 00:23:58,500
 influence the things that happen to us.

320
00:23:58,500 --> 00:24:01,740
 And then they said, but is it really that, is it really

321
00:24:01,740 --> 00:24:04,980
 that way that you can't escape your karma? Isn't there

322
00:24:04,980 --> 00:24:08,500
 somewhere you can go to escape it? Couldn't you run away?

323
00:24:08,500 --> 00:24:10,500
 And the Buddha said, no, you couldn't run away.

324
00:24:10,500 --> 00:24:13,580
 There's no place on earth that you can go to run away from

325
00:24:13,580 --> 00:24:19,500
 your karma. There's another jataka that talks about this.

326
00:24:19,500 --> 00:24:24,960
 There's a goat that this Brahman, the goat talks to him and

327
00:24:24,960 --> 00:24:29,510
 the goat says, you know, it's this crying, laughing jantaka

328
00:24:29,510 --> 00:24:32,860
 where he cries and then he laughs, or he laughs and then he

329
00:24:32,860 --> 00:24:33,500
 cries.

330
00:24:33,500 --> 00:24:37,520
 He's about to be killed and the goat starts laughing and

331
00:24:37,520 --> 00:24:40,500
 then he says, why are you laughing?

332
00:24:40,500 --> 00:24:45,060
 He said, because this is my last life, I was a bra. I'm now

333
00:24:45,060 --> 00:24:49,500
, this is the last life that I have to be born as a goat.

334
00:24:49,500 --> 00:24:53,500
 For 500 years, I've been a sacrificial goat. This is it.

335
00:24:53,500 --> 00:24:56,140
 And then he starts crying and the guys, the brahmin brahmin

336
00:24:56,140 --> 00:24:59,820
 says, why are you crying? And he said, because I'm thinking

337
00:24:59,820 --> 00:25:00,500
 of you.

338
00:25:00,500 --> 00:25:04,470
 Why I was a goat for 500 years, being sacrificed, having my

339
00:25:04,470 --> 00:25:07,620
 head cut off is because before that I was a brahmin just

340
00:25:07,620 --> 00:25:09,500
 like you who killed goats.

341
00:25:09,500 --> 00:25:13,160
 So I know this is where you are going to go. And the brah

342
00:25:13,160 --> 00:25:17,700
min said, oh, then I'll protect you. I won't let them kill

343
00:25:17,700 --> 00:25:18,500
 you.

344
00:25:18,500 --> 00:25:21,040
 And he said, there's nothing you can do. There's no way you

345
00:25:21,040 --> 00:25:24,110
 can stop it. And sure enough, the brahmin tried to protect

346
00:25:24,110 --> 00:25:26,500
 him and made sure nobody came near him.

347
00:25:26,500 --> 00:25:31,410
 But a rock fell actually on this goat. There was some

348
00:25:31,410 --> 00:25:36,500
 really strange coincidence. He ended up dying.

349
00:25:36,500 --> 00:25:41,750
 Karma is like that. You see this, you see potentially these

350
00:25:41,750 --> 00:25:45,500
 sorts of things in the world. Very strange things happened.

351
00:25:45,500 --> 00:25:51,500
 There was a woman once, the wife of a top Monsanto exec.

352
00:25:51,500 --> 00:25:56,850
 Not that that means anything, but it's interesting. Walking

353
00:25:56,850 --> 00:26:00,550
 down the road, I read this in the paper some years ago,

354
00:26:00,550 --> 00:26:06,500
 walking down the road and was suddenly hit by a car and

355
00:26:06,500 --> 00:26:12,110
 pulled under the car and dragged screaming for several

356
00:26:12,110 --> 00:26:15,500
 blocks before she died.

357
00:26:15,500 --> 00:26:21,830
 Dragged under the car. Turns out the woman who was driving

358
00:26:21,830 --> 00:26:25,620
 the car was an old lady who could barely see above the dash

359
00:26:25,620 --> 00:26:29,200
 and had no idea what she'd done and probably to this day

360
00:26:29,200 --> 00:26:31,500
 has never been told what she did.

361
00:26:31,500 --> 00:26:37,700
 The story said they hadn't told her. So it wasn't a bad

362
00:26:37,700 --> 00:26:42,200
 karma. She didn't have the intention to kill. Probably some

363
00:26:42,200 --> 00:26:44,800
 bad karma involved with driving when you shouldn't be

364
00:26:44,800 --> 00:26:45,500
 driving.

365
00:26:45,500 --> 00:26:51,150
 But that's a bit different. But it's the kind of sort of, I

366
00:26:51,150 --> 00:26:54,510
 mean, we have no idea why that happened. Some modern people

367
00:26:54,510 --> 00:26:56,500
 would say it's just a coincidence.

368
00:26:56,500 --> 00:27:05,160
 But it's interesting to look and see. I mean, if you think

369
00:27:05,160 --> 00:27:13,480
 in terms of sort of cause and effect, the problem I think

370
00:27:13,480 --> 00:27:16,500
 is that people focus too much on physical cause and effect

371
00:27:16,500 --> 00:27:21,790
 and they call the mind, there's this term epiphenomenon,

372
00:27:21,790 --> 00:27:25,500
 that the mind is at best just a byproduct that is not a

373
00:27:25,500 --> 00:27:25,500
 thing.

374
00:27:25,500 --> 00:27:29,590
 It's just a byproduct that is ineffectual, that has no

375
00:27:29,590 --> 00:27:33,960
 consequence, that the mind can't affect the body, can't

376
00:27:33,960 --> 00:27:35,500
 affect reality.

377
00:27:35,500 --> 00:27:40,180
 So mind is just this thing that happens sort of like just a

378
00:27:40,180 --> 00:27:44,500
 byproduct, a side product that is meaningless.

379
00:27:44,500 --> 00:27:50,800
 But if you think of the mind as being powerful, as being

380
00:27:50,800 --> 00:27:57,380
 potent, then it makes sense to think that such a powerful

381
00:27:57,380 --> 00:28:03,680
 experience should have some cause, like physical things don

382
00:28:03,680 --> 00:28:06,500
't just happen coincidentally.

383
00:28:06,500 --> 00:28:14,270
 An explosion takes gunpowder. A supernova takes a lot of

384
00:28:14,270 --> 00:28:15,500
 energy.

385
00:28:15,500 --> 00:28:20,910
 And so the idea that these experiences of being dragged

386
00:28:20,910 --> 00:28:26,080
 under a car should take some, should not just happen

387
00:28:26,080 --> 00:28:27,500
 randomly.

388
00:28:27,500 --> 00:28:29,330
 I think there's something to that. I think there's

389
00:28:29,330 --> 00:28:32,200
 something that we're missing often. When we say it's just

390
00:28:32,200 --> 00:28:33,500
 random, it's just coincidence.

391
00:28:33,500 --> 00:28:42,730
 I think there's an argument, even not from meditation or so

392
00:28:42,730 --> 00:28:46,220
 on, that it would take some kind of structure, some kind of

393
00:28:46,220 --> 00:28:47,500
 cause and effect.

394
00:28:47,500 --> 00:28:51,890
 But for meditative purposes, I mean, this is of great

395
00:28:51,890 --> 00:28:53,500
 importance to us.

396
00:28:53,500 --> 00:29:02,500
 The idea that our intentions, our minds have consequences.

397
00:29:02,500 --> 00:29:07,260
 This is something that moves people to meditate, moves med

398
00:29:07,260 --> 00:29:10,500
itators not to do unwholesome needs.

399
00:29:10,500 --> 00:29:13,610
 Meditation for this reason will change your life because

400
00:29:13,610 --> 00:29:16,980
 you start to see how powerful and how poisonous the mind

401
00:29:16,980 --> 00:29:22,500
 can be, how harmful the mind can be when misdirected,

402
00:29:22,500 --> 00:29:26,990
 how dangerous it is. You can see these things building. You

403
00:29:26,990 --> 00:29:30,500
 can see how poisonous.

404
00:29:30,500 --> 00:29:34,480
 You can imagine what it's like to do these things and you

405
00:29:34,480 --> 00:29:37,500
 can remember the things that you've done.

406
00:29:37,500 --> 00:29:44,740
 And without any, without any, without any prompting, like

407
00:29:44,740 --> 00:29:46,500
 anyone telling you it's wrong or it's bad,

408
00:29:46,500 --> 00:29:49,860
 you just start to feel really bad about the things that you

409
00:29:49,860 --> 00:29:51,500
've done, bad things you've done.

410
00:29:51,500 --> 00:29:53,810
 It doesn't take someone to tell you that's bad karma or

411
00:29:53,810 --> 00:29:54,500
 something.

412
00:29:54,500 --> 00:29:58,860
 That happens as well. People feel guilty because they're

413
00:29:58,860 --> 00:30:00,500
 told that things are bad.

414
00:30:00,500 --> 00:30:05,500
 But you just can't abide by it because it's so powerful.

415
00:30:05,500 --> 00:30:08,500
 This is where you start to feel the power of these things.

416
00:30:08,500 --> 00:30:11,500
 That's karmic and it's built up inside of you.

417
00:30:11,500 --> 00:30:15,450
 That's why our life flashes before our eyes. Our life doesn

418
00:30:15,450 --> 00:30:18,500
't really flash before our eyes when we die.

419
00:30:18,500 --> 00:30:23,350
 The things that have had an impact on our mind, that's

420
00:30:23,350 --> 00:30:29,500
 karma. Those things flash before our eyes.

421
00:30:29,500 --> 00:30:35,760
 So this has importance in our practice and to remind us and

422
00:30:35,760 --> 00:30:39,500
 to reinforce the things that we see during our practice,

423
00:30:39,500 --> 00:30:43,330
 but also to encourage people to meditate, to learn about

424
00:30:43,330 --> 00:30:44,500
 these things.

425
00:30:44,500 --> 00:30:48,610
 If you want to become a better person, look at your mind,

426
00:30:48,610 --> 00:30:50,500
 learn about your mind.

427
00:30:50,500 --> 00:30:55,500
 These things occur throughout our lives.

428
00:30:55,500 --> 00:30:58,970
 People do evil things, evil things happen to people who don

429
00:30:58,970 --> 00:31:00,500
't seem to deserve them,

430
00:31:00,500 --> 00:31:13,500
 who don't seem to ever have done anything to deserve them.

431
00:31:13,500 --> 00:31:18,650
 But through meditation we see that there actually is this

432
00:31:18,650 --> 00:31:21,500
 cause and effect relationship.

433
00:31:21,500 --> 00:31:26,290
 And it's quite reasonable to suggest that it continues into

434
00:31:26,290 --> 00:31:27,500
 the future.

435
00:31:27,500 --> 00:31:30,390
 It's quite reasonable to think whether you have evidence or

436
00:31:30,390 --> 00:31:31,500
 proof of it or not.

437
00:31:31,500 --> 00:31:34,120
 It's quite reasonable to think that it's going to have an

438
00:31:34,120 --> 00:31:35,500
 effect on your next life.

439
00:31:35,500 --> 00:31:40,680
 It's going to have an effect on your choices of rebirth, it

440
00:31:40,680 --> 00:31:44,900
's going to have an effect on how people react to you in the

441
00:31:44,900 --> 00:31:45,500
 future.

442
00:31:45,500 --> 00:31:49,030
 That things don't go away. The key to this verse is that

443
00:31:49,030 --> 00:31:50,500
 they don't just go away.

444
00:31:50,500 --> 00:31:53,720
 You can't outrun it. The only way to outrun it would be to

445
00:31:53,720 --> 00:31:56,500
 become an arahant, an enlightened being.

446
00:31:56,500 --> 00:32:00,750
 Before it's going to catch up with you and if you die as an

447
00:32:00,750 --> 00:32:04,500
 arahant, then the future karma doesn't have an opportunity.

448
00:32:04,500 --> 00:32:13,000
 To bear fruit. Anyway, so some stories about karma, some

449
00:32:13,000 --> 00:32:14,500
 ideas of karma.

450
00:32:14,500 --> 00:32:18,050
 That's the Dhammapandha for tonight. Thank you all for

451
00:32:18,050 --> 00:32:32,500
 tuning in. Wish you all good practice.

452
00:32:32,500 --> 00:32:34,500
 God bless.

453
00:32:34,500 --> 00:32:36,500
 (

454
00:32:36,500 --> 00:32:51,500
 Pause)

