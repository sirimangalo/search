1
00:00:00,000 --> 00:00:05,420
 Here's a good one. Thank you for your erratic question.

2
00:00:05,420 --> 00:00:06,480
 Question is, it's not

3
00:00:06,480 --> 00:00:10,390
 better to suffer in future lives, it's not better to suffer

4
00:00:10,390 --> 00:00:11,200
 in future lives

5
00:00:11,200 --> 00:00:15,440
 instead of reaching enlightenment and not live anymore. I

6
00:00:15,440 --> 00:00:20,000
 hear you. It's quite

7
00:00:20,000 --> 00:00:26,480
 clear and I think this is on a lot of people's minds. There

8
00:00:26,480 --> 00:00:29,760
 was a queen once in

9
00:00:29,760 --> 00:00:35,480
 the Buddha's time and she became an Arahant and the Buddha

10
00:00:35,480 --> 00:00:36,440
 said to the king,

11
00:00:36,440 --> 00:00:42,880
 "Well either you let her become a bhikkhuni or she's going

12
00:00:42,880 --> 00:00:43,040
 to

13
00:00:43,040 --> 00:00:48,090
 pass away in within seven days because there's no way she

14
00:00:48,090 --> 00:00:49,080
 could survive,

15
00:00:49,080 --> 00:00:56,400
 there's no way she could continue. It's not really possible

16
00:00:56,400 --> 00:00:58,380
." So she would

17
00:00:58,380 --> 00:01:00,990
 pass away and he was like, "Oh no, then ordain her, ordain

18
00:01:00,990 --> 00:01:02,060
 her right away."

19
00:01:02,060 --> 00:01:05,520
 Because he couldn't bear to have her disappear, bear to

20
00:01:05,520 --> 00:01:08,000
 have her gone.

21
00:01:08,000 --> 00:01:17,760
 He said, "Enough of this talk of parinibhana." The question

22
00:01:17,760 --> 00:01:18,720
, I'm sorry,

23
00:01:18,720 --> 00:01:24,450
 it's, I'm sorry to find it funny, but it really answers

24
00:01:24,450 --> 00:01:27,180
 itself. What you're

25
00:01:27,180 --> 00:01:34,070
 saying is you want to suffer in future lives. So there's

26
00:01:34,070 --> 00:01:35,960
 nothing really to worry

27
00:01:35,960 --> 00:01:39,800
 about because you have no chance. With that mind state

28
00:01:39,800 --> 00:01:41,960
 remaining, you have no

29
00:01:41,960 --> 00:01:47,910
 chance of not living anymore. You will have to come back

30
00:01:47,910 --> 00:01:49,200
 because there is

31
00:01:49,200 --> 00:01:55,400
 still this cause for future rebirth. At the moment of death

32
00:01:55,400 --> 00:01:57,000
 you don't say, "Okay,

33
00:01:57,000 --> 00:02:00,690
 enough." You say, "More, more, more. What about, what about

34
00:02:00,690 --> 00:02:05,640
 this? What about that?" So

35
00:02:05,640 --> 00:02:09,790
 don't, you don't have to wrestle with this one. You don't

36
00:02:09,790 --> 00:02:11,040
 have to think, "Hmm,

37
00:02:11,040 --> 00:02:14,430
 should I practice Buddhism because if I practice Buddhism

38
00:02:14,430 --> 00:02:15,240
 maybe I won't live

39
00:02:15,240 --> 00:02:19,300
 anymore." It's not possible. The only way that you could

40
00:02:19,300 --> 00:02:20,720
 not come back and live

41
00:02:20,720 --> 00:02:25,690
 again to suffer more and more is if you decided for

42
00:02:25,690 --> 00:02:28,280
 yourself through what I

43
00:02:28,280 --> 00:02:31,690
 would understand to be an incredible realization of the

44
00:02:31,690 --> 00:02:33,240
 truth, but you know,

45
00:02:33,240 --> 00:02:38,950
 depends who you ask, that there were no reason to come back

46
00:02:38,950 --> 00:02:40,600
. You will come to

47
00:02:40,600 --> 00:02:46,560
 realize that there was no benefit and you come to realize

48
00:02:46,560 --> 00:02:48,080
 that in fact there

49
00:02:48,080 --> 00:02:51,090
 is no one living at all. There is only actually these

50
00:02:51,090 --> 00:02:52,760
 experiences, none of which

51
00:02:52,760 --> 00:02:58,000
 are of any benefit to you or of any benefit in general,

52
00:02:58,000 --> 00:02:59,440
 none of which have no

53
00:02:59,440 --> 00:03:05,040
 intrinsic value. And when you realize that you're not born

54
00:03:05,040 --> 00:03:05,520
 anymore,

55
00:03:05,520 --> 00:03:12,400
 it's just, it just happens. So for yeah, for as long as you

56
00:03:12,400 --> 00:03:13,040
, you still think that

57
00:03:13,040 --> 00:03:18,520
 there's some benefit to existence, don't worry about it.

58
00:03:18,520 --> 00:03:20,120
 You got lots of, lots of

59
00:03:20,120 --> 00:03:22,420
 time to come back again and again, which a lot of people

60
00:03:22,420 --> 00:03:23,720
 like about Buddhism. They

61
00:03:23,720 --> 00:03:26,000
 think, "Well great, I'll just come back again and again and

62
00:03:26,000 --> 00:03:27,520
 again and eventually

63
00:03:27,520 --> 00:03:32,730
 I'll learn everything and become, become enlightened, all

64
00:03:32,730 --> 00:03:34,280
 in good time, but first

65
00:03:34,280 --> 00:03:38,080
 let me explore this kind of this novel idea that I can

66
00:03:38,080 --> 00:03:39,760
 actually, you know, do

67
00:03:39,760 --> 00:03:43,840
 good deeds and go to heaven and so on, which is fine. You

68
00:03:43,840 --> 00:03:45,480
 know, it's a, it's a

69
00:03:45,480 --> 00:03:48,420
 kind of unshaky ground because you never know where you're

70
00:03:48,420 --> 00:03:50,120
 going to go and maybe

71
00:03:50,120 --> 00:03:53,880
 this life, next life you go to heaven, but once you forget

72
00:03:53,880 --> 00:03:55,120
 all of the stuff that

73
00:03:55,120 --> 00:04:00,220
 you've learned in, in regards to goodness and badness, you

74
00:04:00,220 --> 00:04:00,960
 might come back as one

75
00:04:00,960 --> 00:04:07,140
 of those evil people and go to hell as well. So yeah, but

76
00:04:07,140 --> 00:04:07,960
 really that's the

77
00:04:07,960 --> 00:04:12,970
 point is that, to answer your question directly, it's

78
00:04:12,970 --> 00:04:13,680
 because there's no,

79
00:04:13,680 --> 00:04:18,920
 there's nothing good about any piece of existence. It's

80
00:04:18,920 --> 00:04:19,240
 actually a

81
00:04:19,240 --> 00:04:23,880
 delusion that we have. It's not understanding reality as it

82
00:04:23,880 --> 00:04:26,200
 is, but for

83
00:04:26,200 --> 00:04:29,280
 as long as you understand reality to have some intrinsic

84
00:04:29,280 --> 00:04:30,400
 benefit or realities,

85
00:04:30,400 --> 00:04:39,200
 as long as you understand experience, seeing, hearing,

86
00:04:39,200 --> 00:04:40,680
 smelling, tasting, feeling,

87
00:04:40,680 --> 00:04:43,780
 thinking, to have some intrinsic value, you come back and

88
00:04:43,780 --> 00:04:44,880
 there's no need to

89
00:04:44,880 --> 00:04:48,510
 worry about that. The, the other thing I'd say is that it's

90
00:04:48,510 --> 00:04:49,960
 kind of like, it kind of

91
00:04:49,960 --> 00:04:55,640
 like works like pulling the thread of a sweater, because

92
00:04:55,640 --> 00:04:56,200
 you got this loose

93
00:04:56,200 --> 00:04:58,280
 thread and you say, well that's no good, I got to get rid

94
00:04:58,280 --> 00:04:59,960
 of that, but as you pull

95
00:04:59,960 --> 00:05:03,970
 it you realize that there's, it's attached to so much more

96
00:05:03,970 --> 00:05:04,760
 and then you

97
00:05:04,760 --> 00:05:06,800
 keep pulling and pulling and pulling and eventually you're

98
00:05:06,800 --> 00:05:09,360
 left without a sweater.

99
00:05:09,360 --> 00:05:12,950
 Suffering is really in that vein. We start, we come to

100
00:05:12,950 --> 00:05:14,280
 practice not because we

101
00:05:14,280 --> 00:05:16,230
 want to become enlightened, but because we got a lot of

102
00:05:16,230 --> 00:05:18,280
 problems. So we figure,

103
00:05:18,280 --> 00:05:22,840
 get rid of those problems and life will be fine. Then I won

104
00:05:22,840 --> 00:05:23,480
't need to come

105
00:05:23,480 --> 00:05:25,680
 admitted, then I won't have any interest in admitted, that

106
00:05:25,680 --> 00:05:27,080
's enough. Let me get

107
00:05:27,080 --> 00:05:32,170
 that far and then quit. Problem is, as you explore these

108
00:05:32,170 --> 00:05:35,960
 questions or these problems,

109
00:05:35,960 --> 00:05:38,290
 deeper and deeper and until you get to the root of the

110
00:05:38,290 --> 00:05:39,440
 problem, you find that

111
00:05:39,440 --> 00:05:43,120
 the root is actually the root of who you are. It's the root

112
00:05:43,120 --> 00:05:44,520
 of your identification

113
00:05:44,520 --> 00:05:50,160
 with, with existence. And once that's gone, then you have

114
00:05:50,160 --> 00:05:52,200
 no interest either way.

115
00:05:52,200 --> 00:05:56,050
 There's, there's no interest in, in coming back as this or

116
00:05:56,050 --> 00:05:58,040
 that. There's no aversion

117
00:05:58,040 --> 00:06:04,830
 to existing. And so there's just peace. And then at the end

118
00:06:04,830 --> 00:06:08,840
 there's freedom. So I

119
00:06:08,840 --> 00:06:12,160
 hope that answers your question.

