1
00:00:00,000 --> 00:00:05,000
 Okay, good evening everyone.

2
00:00:05,000 --> 00:00:10,000
 Welcome to our evening dhamma.

3
00:00:10,000 --> 00:00:17,160
 I'm going to take a moment here to ask everyone how the

4
00:00:17,160 --> 00:00:18,000
 audio is.

5
00:00:18,000 --> 00:00:23,000
 We've got audio in Second Life.

6
00:00:23,000 --> 00:00:27,000
 We've got audio on YouTube.

7
00:00:27,000 --> 00:00:29,000
 We've got local audio.

8
00:00:29,000 --> 00:00:34,000
 Can you hear me in the back there?

9
00:00:34,000 --> 00:00:36,000
 I'm nodding, okay.

10
00:00:36,000 --> 00:00:38,000
 Locally, I'm okay.

11
00:00:38,000 --> 00:00:40,000
 I've got to get some speakers in this room.

12
00:00:40,000 --> 00:00:45,000
 I can set up a microphone.

13
00:00:45,000 --> 00:00:48,000
 See YouTube. Can YouTube hear me?

14
00:00:48,000 --> 00:00:54,000
 I'm not going to get any death threats from YouTube for not

15
00:00:54,000 --> 00:00:56,000
 having sufficient audio.

16
00:00:56,000 --> 00:01:05,000
 Okay, good. YouTube is good. Louder than last night.

17
00:01:05,000 --> 00:01:07,660
 All right. Well, we'll see if there's any comments by the

18
00:01:07,660 --> 00:01:08,000
 end.

19
00:01:08,000 --> 00:01:16,000
 We'll adjust it again.

20
00:01:16,000 --> 00:01:29,810
 So today we're talking about purification. Purification by

21
00:01:29,810 --> 00:01:35,000
 overcoming doubt.

22
00:01:35,000 --> 00:01:40,000
 Which if you understand it is the next logical step from

23
00:01:40,000 --> 00:01:45,000
 after purification of view.

24
00:01:45,000 --> 00:01:50,000
 And purification of view, what we talked about last night,

25
00:01:50,000 --> 00:01:57,000
 is changing the way we look at the world.

26
00:01:57,000 --> 00:02:02,000
 So moving from a concept or entity-based three-dimensional,

27
00:02:02,000 --> 00:02:07,000
 impersonal view of reality to an experiential base.

28
00:02:07,000 --> 00:02:13,720
 We begin to look at reality from a point of view of here

29
00:02:13,720 --> 00:02:16,000
 now and first person.

30
00:02:16,000 --> 00:02:21,000
 What am I experiencing? What is the experience here and now

31
00:02:21,000 --> 00:02:21,000
?

32
00:02:21,000 --> 00:02:27,000
 That's the basis for the reality.

33
00:02:27,000 --> 00:02:31,350
 So purification by overcoming doubt isn't really about

34
00:02:31,350 --> 00:02:33,000
 doubt about that.

35
00:02:33,000 --> 00:02:37,000
 It's more about what reality is made up of.

36
00:02:37,000 --> 00:02:42,150
 It's more in regards to the way we're going to approach

37
00:02:42,150 --> 00:02:43,000
 reality

38
00:02:43,000 --> 00:02:58,000
 or the consequences of the nature of reality.

39
00:02:58,000 --> 00:03:05,000
 See what it means, the implications.

40
00:03:05,000 --> 00:03:18,000
 So doubt is a quality of mind that we live with ordinarily.

41
00:03:18,000 --> 00:03:26,380
 We live with a doubt constantly about what should I do with

42
00:03:26,380 --> 00:03:27,000
 my life.

43
00:03:27,000 --> 00:03:30,000
 What is the right way to go?

44
00:03:30,000 --> 00:03:42,420
 We have doubts about the nature of our lives, about our

45
00:03:42,420 --> 00:03:43,000
 situations.

46
00:03:43,000 --> 00:03:46,000
 We have doubts about what we should do.

47
00:03:46,000 --> 00:03:51,260
 We have doubts about the nature of the world. How does the

48
00:03:51,260 --> 00:03:54,000
 world work?

49
00:03:54,000 --> 00:03:58,000
 A lot of science was created to help answer these questions

50
00:03:58,000 --> 00:03:58,000
,

51
00:03:58,000 --> 00:04:06,320
 help us find a better way to live, help us understand the

52
00:04:06,320 --> 00:04:09,000
 universe

53
00:04:09,000 --> 00:04:12,000
 so that we might live a better way.

54
00:04:12,000 --> 00:04:14,000
 It's why religion was created as well.

55
00:04:14,000 --> 00:04:20,000
 Religion and science, as we understand these words,

56
00:04:20,000 --> 00:04:25,000
 they both tried to provide us with answers.

57
00:04:25,000 --> 00:04:30,750
 Though what we call religion has often been more about

58
00:04:30,750 --> 00:04:32,000
 believing

59
00:04:32,000 --> 00:04:36,000
 without much basis in evidence.

60
00:04:36,000 --> 00:04:41,000
 Science has been much more about evidence.

61
00:04:41,000 --> 00:04:44,540
 They both tried to give us answers to help us overcome

62
00:04:44,540 --> 00:04:45,000
 doubt.

63
00:04:45,000 --> 00:04:48,000
 And there's quite a bit of overlap, in fact,

64
00:04:48,000 --> 00:04:51,000
 if you think about it from a phenomenological point of view

65
00:04:51,000 --> 00:04:51,000
,

66
00:04:51,000 --> 00:04:56,000
 to believe something or to know it.

67
00:04:56,000 --> 00:05:13,000
 The point is it provides you with an answer.

68
00:05:13,000 --> 00:05:15,000
 Science provides us with answers based on evidence,

69
00:05:15,000 --> 00:05:25,000
 and that's very good at overcoming doubt conventionally.

70
00:05:25,000 --> 00:05:28,000
 If you rely on science, if you believe in science,

71
00:05:28,000 --> 00:05:31,000
 if you put your faith in science,

72
00:05:31,000 --> 00:05:36,000
 it's very good at overcoming our doubt rationally.

73
00:05:36,000 --> 00:05:40,000
 Very good at helping us to find ways of living our lives.

74
00:05:40,000 --> 00:05:43,000
 And there's so much we take for granted about the world

75
00:05:43,000 --> 00:05:52,000
 that science has discovered to be true.

76
00:05:52,000 --> 00:05:58,000
 So science begins to assuage our doubts

77
00:05:58,000 --> 00:06:02,000
 and help us free ourselves from doubt,

78
00:06:02,000 --> 00:06:06,000
 but it doesn't go the full distance.

79
00:06:06,000 --> 00:06:07,000
 I mean, for two reasons.

80
00:06:07,000 --> 00:06:09,000
 First, because science doesn't know everything.

81
00:06:09,000 --> 00:06:11,690
 Scientists haven't figured out all the answers to all the

82
00:06:11,690 --> 00:06:12,000
 questions,

83
00:06:12,000 --> 00:06:19,000
 but also because the answers are not our answers.

84
00:06:19,000 --> 00:06:24,000
 Where science breaks down is it can't help you come to know

85
00:06:24,000 --> 00:06:28,000
 the truth of your reality.

86
00:06:28,000 --> 00:06:30,000
 So we talked about things like depression.

87
00:06:30,000 --> 00:06:34,190
 Science can help you understand the physical aspects of

88
00:06:34,190 --> 00:06:35,000
 depression,

89
00:06:35,000 --> 00:06:38,000
 but it can't help you overcome depression

90
00:06:38,000 --> 00:06:40,000
 because it can't help you understand.

91
00:06:40,000 --> 00:06:46,030
 I mean, it can help, but it can't bring you to understand

92
00:06:46,030 --> 00:06:47,000
 your own depression

93
00:06:47,000 --> 00:06:49,560
 and understand your depression to the extent that you free

94
00:06:49,560 --> 00:06:50,000
 yourself from it

95
00:06:50,000 --> 00:06:55,000
 or anxiety or fear.

96
00:06:55,000 --> 00:06:58,000
 Science can't tell you how to live your life,

97
00:06:58,000 --> 00:07:00,000
 can't tell you how to react to situations,

98
00:07:00,000 --> 00:07:03,000
 it can't teach you wisdom.

99
00:07:03,000 --> 00:07:05,000
 It's the difference again between wisdom and intelligence.

100
00:07:05,000 --> 00:07:09,000
 So religion tries to provide this wisdom,

101
00:07:09,000 --> 00:07:17,000
 tries to offer things like ethics.

102
00:07:17,000 --> 00:07:21,160
 And so our biggest doubts are usually in regards to

103
00:07:21,160 --> 00:07:22,000
 religion.

104
00:07:22,000 --> 00:07:25,000
 They're in regards to our own, what we call spirituality,

105
00:07:25,000 --> 00:07:30,000
 but what really turns out to be much more how we interact

106
00:07:30,000 --> 00:07:33,000
 in a very mundane way with the universe.

107
00:07:33,000 --> 00:07:37,250
 We talk about belief in God as though it's some sort of

108
00:07:37,250 --> 00:07:38,000
 divine

109
00:07:38,000 --> 00:07:41,560
 or abstract thing, but it's really just a means for us to

110
00:07:41,560 --> 00:07:42,000
 cope.

111
00:07:42,000 --> 00:07:48,000
 God is a coping mechanism, more or less.

112
00:07:48,000 --> 00:07:49,000
 How do I mean that?

113
00:07:49,000 --> 00:07:52,000
 We use God to reassure ourselves.

114
00:07:52,000 --> 00:07:54,000
 God has a plan for you.

115
00:07:54,000 --> 00:07:58,000
 No matter how hard things get, just remember God loves you.

116
00:07:58,000 --> 00:08:03,000
 Just remember God is in heaven waiting for you.

117
00:08:03,000 --> 00:08:05,000
 So from a psychological point of view,

118
00:08:05,000 --> 00:08:12,270
 which really is a much better founded in reality than

119
00:08:12,270 --> 00:08:14,000
 religion,

120
00:08:14,000 --> 00:08:20,000
 God and things like God are just tools that we use to help

121
00:08:20,000 --> 00:08:26,000
 us solve our problems,

122
00:08:26,000 --> 00:08:30,000
 help us cope with our difficulties.

123
00:08:30,000 --> 00:08:34,000
 Most of religion is like that, ritual for example.

124
00:08:34,000 --> 00:08:38,500
 You see Buddhists do a lot of rituals, chanting, ringing

125
00:08:38,500 --> 00:08:43,000
 bells, bowing.

126
00:08:43,000 --> 00:08:45,000
 Psychologically these are useful things.

127
00:08:45,000 --> 00:08:50,000
 They help promote positive mind states.

128
00:08:50,000 --> 00:08:54,000
 But none of these things is really enough.

129
00:08:54,000 --> 00:08:55,000
 Enough for what?

130
00:08:55,000 --> 00:09:01,000
 Enough to allow us to completely overcome doubt.

131
00:09:01,000 --> 00:09:05,000
 And so this next stage is where the meditator begins to

132
00:09:05,000 --> 00:09:10,000
 actually understand

133
00:09:10,000 --> 00:09:14,160
 not only the nature of reality, or not only the building

134
00:09:14,160 --> 00:09:15,000
 blocks of reality,

135
00:09:15,000 --> 00:09:21,570
 but how it works, how the machine works, not just what it's

136
00:09:21,570 --> 00:09:25,000
 made up of, how it works.

137
00:09:25,000 --> 00:09:28,550
 And so the first stage is the meditator now begins to not

138
00:09:28,550 --> 00:09:30,000
 only look at body and mind,

139
00:09:30,000 --> 00:09:32,000
 remember what we talked about last night,

140
00:09:32,000 --> 00:09:38,000
 but begins to understand how body and mind work together.

141
00:09:38,000 --> 00:09:43,000
 So the overcoming of doubt here has to do very much with

142
00:09:43,000 --> 00:09:45,000
 how reality really works,

143
00:09:45,000 --> 00:09:51,000
 how our problems come to be.

144
00:09:51,000 --> 00:09:54,060
 I mean the big part of our problem is we don't even

145
00:09:54,060 --> 00:09:56,000
 understand our problems.

146
00:09:56,000 --> 00:09:58,000
 I'm anxious, where does it come from?

147
00:09:58,000 --> 00:09:59,000
 What does it mean?

148
00:09:59,000 --> 00:10:00,000
 I'm depressed.

149
00:10:00,000 --> 00:10:02,000
 What is it?

150
00:10:02,000 --> 00:10:04,000
 And you go to the doctor and they tell you,

151
00:10:04,000 --> 00:10:09,000
 "Oh, it's because your brain is this and this and this."

152
00:10:09,000 --> 00:10:11,000
 Which again is useful to a point.

153
00:10:11,000 --> 00:10:15,000
 It doesn't really, I mean it leads us on the wrong path,

154
00:10:15,000 --> 00:10:18,560
 I think in some sense because it's too much of reliance on

155
00:10:18,560 --> 00:10:20,000
 that as the answer.

156
00:10:20,000 --> 00:10:24,000
 It's not wrong, the brain is doing certain things.

157
00:10:24,000 --> 00:10:30,040
 But it's wrong in the sense that that answer isn't going to

158
00:10:30,040 --> 00:10:33,000
 free us from depression.

159
00:10:33,000 --> 00:10:38,610
 It's the wrong answer because it's the wrong type of answer

160
00:10:38,610 --> 00:10:39,000
.

161
00:10:39,000 --> 00:10:42,000
 The right type of answer is one that we see for ourselves.

162
00:10:42,000 --> 00:10:44,000
 "Oh yes, look at me creating my depression.

163
00:10:44,000 --> 00:10:48,000
 Look at the depression caused by this and this and this."

164
00:10:48,000 --> 00:10:53,000
 The meditator begins to see cause and effect.

165
00:10:53,000 --> 00:10:55,000
 It's called bhajayaparigahanyana.

166
00:10:55,000 --> 00:10:59,000
 It's the second of the sixteen stages of knowledge.

167
00:10:59,000 --> 00:11:03,000
 It's hard to see cause and effect.

168
00:11:03,000 --> 00:11:05,000
 Again, these knowledges are not something intellectual.

169
00:11:05,000 --> 00:11:09,000
 I wouldn't even really recommend researching them too much.

170
00:11:09,000 --> 00:11:11,520
 Anyone who gets caught up in these ends up just hurting

171
00:11:11,520 --> 00:11:12,000
 themselves

172
00:11:12,000 --> 00:11:14,000
 in terms of the practice.

173
00:11:14,000 --> 00:11:16,000
 If you get too caught up in learning about the knowledges

174
00:11:16,000 --> 00:11:19,000
 and thinking about the knowledges

175
00:11:19,000 --> 00:11:21,980
 and the kind of thing teachers use to assess their students

176
00:11:21,980 --> 00:11:22,000
.

177
00:11:22,000 --> 00:11:25,000
 But they're not the most important thing

178
00:11:25,000 --> 00:11:29,000
 and they can even be detrimental if you know too much.

179
00:11:29,000 --> 00:11:31,470
 Again, at this point we're very much into results of the

180
00:11:31,470 --> 00:11:32,000
 practice.

181
00:11:32,000 --> 00:11:35,000
 This is useful for those of you who are meditating.

182
00:11:35,000 --> 00:11:38,000
 It's useful for those of you who haven't in a sense

183
00:11:38,000 --> 00:11:41,000
 to give you a sense of where you're going.

184
00:11:41,000 --> 00:11:45,000
 It helps build confidence and direction.

185
00:11:45,000 --> 00:11:49,000
 It's certainly not a replacement for actual practice.

186
00:11:49,000 --> 00:11:51,310
 It's most useful for those people who are already

187
00:11:51,310 --> 00:11:52,000
 experiencing these things

188
00:11:52,000 --> 00:11:59,000
 so they can confirm and focus their energies

189
00:11:59,000 --> 00:12:06,000
 based on Buddhism, what is not the path.

190
00:12:06,000 --> 00:12:11,000
 In summary, this is an understanding of karma.

191
00:12:11,000 --> 00:12:13,000
 We talk about karma in Buddhism.

192
00:12:13,000 --> 00:12:15,000
 What we really mean by karma in Buddhism,

193
00:12:15,000 --> 00:12:22,000
 is our intentions or our bent, our volition in the mind.

194
00:12:22,000 --> 00:12:25,400
 At this point the meditator starts to understand what vol

195
00:12:25,400 --> 00:12:26,000
ition means.

196
00:12:26,000 --> 00:12:30,430
 When you have a desire for something, well it leads to a

197
00:12:30,430 --> 00:12:31,000
 result.

198
00:12:31,000 --> 00:12:34,380
 You learn what that result is. You don't have to believe

199
00:12:34,380 --> 00:12:35,000
 anyone.

200
00:12:35,000 --> 00:12:37,580
 When you have this sort of mind state it leads to this

201
00:12:37,580 --> 00:12:38,000
 result.

202
00:12:38,000 --> 00:12:41,990
 The meditator begins to reflect upon all of their past vol

203
00:12:41,990 --> 00:12:43,000
itions as well.

204
00:12:43,000 --> 00:12:47,000
 They start to see how this led to this and that led to that

205
00:12:47,000 --> 00:12:48,000
.

206
00:12:48,000 --> 00:12:52,600
 Most importantly they see here and now how this leads to

207
00:12:52,600 --> 00:12:55,000
 this, that leads to that.

208
00:12:55,000 --> 00:12:59,130
 It's very simple. The meditator begins to see the mechanics

209
00:12:59,130 --> 00:13:00,000
 of reality.

210
00:13:00,000 --> 00:13:04,410
 It's quite obvious. The meditator, once they've passed the

211
00:13:04,410 --> 00:13:06,000
 first stage,

212
00:13:06,000 --> 00:13:09,000
 they'll enter into this stage.

213
00:13:09,000 --> 00:13:14,000
 The next thing that overcoming doubt has to do with is,

214
00:13:14,000 --> 00:13:17,000
 "What would we do in regards to that?"

215
00:13:17,000 --> 00:13:20,760
 "Hey, so this leads to this, that leads to that. What does

216
00:13:20,760 --> 00:13:22,000
 it all mean?"

217
00:13:22,000 --> 00:13:27,000
 Or, "What do I do about this?"

218
00:13:27,000 --> 00:13:30,000
 "What's the answer?"

219
00:13:30,000 --> 00:13:34,000
 To some extent of course the answer is just to be mindful.

220
00:13:34,000 --> 00:13:38,000
 But there's a deeper understanding that happens here.

221
00:13:38,000 --> 00:13:42,220
 And there's a wrestling with oneself as one wrestles with

222
00:13:42,220 --> 00:13:51,000
 the implications of the nature of reality.

223
00:13:51,000 --> 00:13:55,000
 One sees how craving is a cause for suffering.

224
00:13:55,000 --> 00:14:01,570
 One begins to see this at this point and one wrestles with

225
00:14:01,570 --> 00:14:03,000
 that.

226
00:14:03,000 --> 00:14:08,720
 In the sense that the meditation is showing one some fairly

227
00:14:08,720 --> 00:14:11,000
 uncomfortable truths.

228
00:14:11,000 --> 00:14:15,820
 And so the wrestling has to do with understanding that this

229
00:14:15,820 --> 00:14:19,000
 isn't a product of the meditation.

230
00:14:19,000 --> 00:14:21,530
 The meditator's first instinct is, "Well, something's wrong

231
00:14:21,530 --> 00:14:24,230
 with the meditation because craving doesn't lead to

232
00:14:24,230 --> 00:14:25,000
 suffering.

233
00:14:25,000 --> 00:14:28,000
 When I want things, I get them.

234
00:14:28,000 --> 00:14:32,000
 The problem here is I'm not getting what I want."

235
00:14:32,000 --> 00:14:36,000
 The meditator begins to see a lot of suffering, stress.

236
00:14:36,000 --> 00:14:38,000
 It's quite stressful at this point.

237
00:14:38,000 --> 00:14:40,950
 It's one of the stages of the meditation that could be

238
00:14:40,950 --> 00:14:42,000
 quite stressful.

239
00:14:42,000 --> 00:14:50,700
 One is forced to choose between following one's desires or

240
00:14:50,700 --> 00:14:53,000
 letting them go.

241
00:14:53,000 --> 00:14:58,840
 I mean both seem to be potential ways to free oneself from

242
00:14:58,840 --> 00:15:00,000
 suffering.

243
00:15:00,000 --> 00:15:02,000
 You want something.

244
00:15:02,000 --> 00:15:06,030
 Your two options are get what you want or give up the

245
00:15:06,030 --> 00:15:07,000
 wanting.

246
00:15:07,000 --> 00:15:10,700
 Either one frees you from the wanting and frees you from

247
00:15:10,700 --> 00:15:12,000
 the suffering.

248
00:15:12,000 --> 00:15:18,240
 And so the meditator begins to see, begins to, I mean they

249
00:15:18,240 --> 00:15:20,000
 have this doubt.

250
00:15:20,000 --> 00:15:23,090
 And it actually can cause some meditators early on in the

251
00:15:23,090 --> 00:15:26,000
 course to leave or to consider leaving.

252
00:15:26,000 --> 00:15:30,280
 It's one of the most difficult parts of the course, just

253
00:15:30,280 --> 00:15:31,000
 psychologically,

254
00:15:31,000 --> 00:15:35,130
 because the meditator is wrestling with this newfound

255
00:15:35,130 --> 00:15:36,000
 knowledge

256
00:15:36,000 --> 00:15:39,410
 that the things that we cling to are not satisfying, they

257
00:15:39,410 --> 00:15:42,000
're not stable or predictable,

258
00:15:42,000 --> 00:15:44,000
 and they're not controllable.

259
00:15:44,000 --> 00:15:49,000
 So one begins to see the three characteristics.

260
00:15:49,000 --> 00:15:52,770
 One's first taste of the three characteristics of imper

261
00:15:52,770 --> 00:15:59,000
manent suffering, non-self, are a challenge.

262
00:15:59,000 --> 00:16:02,000
 So there's a lot of doubt in this stage as a result.

263
00:16:02,000 --> 00:16:05,510
 So it's called purification by overcoming doubt because the

264
00:16:05,510 --> 00:16:09,000
 meditator who successfully navigates this

265
00:16:09,000 --> 00:16:14,120
 begins to gain a real understanding and reassurance of how

266
00:16:14,120 --> 00:16:15,000
 nature really works,

267
00:16:15,000 --> 00:16:19,000
 how reality really works.

268
00:16:19,000 --> 00:16:21,000
 It starts to realize, oh this is the problem.

269
00:16:21,000 --> 00:16:24,600
 Not that I'm not getting what I want, but that I want in

270
00:16:24,600 --> 00:16:26,000
 the first place.

271
00:16:26,000 --> 00:16:31,280
 If I were content, if I were at peace, if I didn't react to

272
00:16:31,280 --> 00:16:33,000
 things, I wouldn't suffer.

273
00:16:33,000 --> 00:16:37,780
 Why? Because those things that I'm reacting to are imper

274
00:16:37,780 --> 00:16:39,000
manent suffering and non-self.

275
00:16:39,000 --> 00:16:42,690
 They can't possibly satisfy me. They're impermanent in that

276
00:16:42,690 --> 00:16:47,000
 they're uncertain, unstable.

277
00:16:47,000 --> 00:16:49,320
 They're suffering in a sense they're unsatisfying, and if

278
00:16:49,320 --> 00:16:50,000
 you cling to them,

279
00:16:50,000 --> 00:16:53,480
 you're just going to suffer because of course they're imper

280
00:16:53,480 --> 00:16:54,000
manent.

281
00:16:54,000 --> 00:16:56,760
 And they're out of our control. They're not me, they're not

282
00:16:56,760 --> 00:16:59,000
 mine, they're not the self,

283
00:16:59,000 --> 00:17:02,000
 myself, they're not any self.

284
00:17:02,000 --> 00:17:10,050
 They're experiences that arise and cease and that are based

285
00:17:10,050 --> 00:17:14,000
 on causes and conditions.

286
00:17:14,000 --> 00:17:17,710
 So the meditator who starts to accept this, it really is a

287
00:17:17,710 --> 00:17:19,000
 liberating moment.

288
00:17:19,000 --> 00:17:22,890
 I mean the next stages that we'll talk about a little bit

289
00:17:22,890 --> 00:17:24,000
 more next time

290
00:17:24,000 --> 00:17:30,730
 are much more peaceful and pleasant, reassuring to the med

291
00:17:30,730 --> 00:17:32,000
itator.

292
00:17:32,000 --> 00:17:36,000
 Once they make this shift, it's really another shift from

293
00:17:36,000 --> 00:17:39,000
 doubting and uncertainty and wavering

294
00:17:39,000 --> 00:17:45,800
 and a lot of wrong ideas about the nature of reality to a

295
00:17:45,800 --> 00:17:47,000
 certainty,

296
00:17:47,000 --> 00:17:50,060
 to seeing how reality really works and understanding why we

297
00:17:50,060 --> 00:17:51,000
 were doubting in the first place,

298
00:17:51,000 --> 00:17:54,570
 why we were unsure about our lives and what to do, where to

299
00:17:54,570 --> 00:17:55,000
 go.

300
00:17:55,000 --> 00:17:58,000
 We had it all wrong.

301
00:17:58,000 --> 00:18:04,060
 We start to see what's really going on, how reality really

302
00:18:04,060 --> 00:18:05,000
 works,

303
00:18:05,000 --> 00:18:07,000
 frees you from doubt.

304
00:18:07,000 --> 00:18:12,040
 When you're free from doubt, there's a certainty, there's a

305
00:18:12,040 --> 00:18:14,000
 composure of mind

306
00:18:14,000 --> 00:18:19,350
 and that's what propels one into the more pleasant states,

307
00:18:19,350 --> 00:18:24,000
 the more composed and energetic states.

308
00:18:24,000 --> 00:18:26,000
 So it's an encouragement.

309
00:18:26,000 --> 00:18:28,890
 In the beginning stages of the practice, and it's difficult

310
00:18:28,890 --> 00:18:29,000
,

311
00:18:29,000 --> 00:18:31,990
 you're probably going through this where you start to see

312
00:18:31,990 --> 00:18:33,000
 the three characteristics

313
00:18:33,000 --> 00:18:36,000
 for the first time and it's not pleasant and you think,

314
00:18:36,000 --> 00:18:41,000
 "Oh, this meditation is changing reality.

315
00:18:41,000 --> 00:18:45,260
 Reality isn't like this. It must be a meditation that makes

316
00:18:45,260 --> 00:18:47,000
 things suddenly impermanent,

317
00:18:47,000 --> 00:18:49,000
 unsatisfying, uncontrollable.

318
00:18:49,000 --> 00:18:53,000
 I better go back to my other reality where things are

319
00:18:53,000 --> 00:18:56,000
 pleasant, satisfying and controllable."

320
00:18:56,000 --> 00:19:00,640
 Until you realize, "Oh, no. This has nothing to do with

321
00:19:00,640 --> 00:19:02,000
 meditation at all.

322
00:19:02,000 --> 00:19:06,000
 This is the nature of reality, whether I'm here or not.

323
00:19:06,000 --> 00:19:10,000
 You can't escape it by going home."

324
00:19:10,000 --> 00:19:15,000
 And at that point, one is really in a good way.

325
00:19:15,000 --> 00:19:17,000
 At that point, they begin to practice vipassana.

326
00:19:17,000 --> 00:19:20,000
 Here we're not yet in vipassana.

327
00:19:20,000 --> 00:19:24,000
 You're not yet in the stages of insight.

328
00:19:24,000 --> 00:19:27,260
 Insight isn't said to have arisen until the next pur

329
00:19:27,260 --> 00:19:28,000
ification.

330
00:19:28,000 --> 00:19:31,470
 Even though there is insight, it's not really considered v

331
00:19:31,470 --> 00:19:32,000
ipassana

332
00:19:32,000 --> 00:19:36,840
 because one is still struggling to learn the nature of

333
00:19:36,840 --> 00:19:38,000
 reality.

334
00:19:38,000 --> 00:19:41,000
 At this point, we're just learning how the tools work.

335
00:19:41,000 --> 00:19:45,000
 You're a carpenter.

336
00:19:45,000 --> 00:19:49,360
 Before you go and carve up some wood or build a cabinet or

337
00:19:49,360 --> 00:19:51,000
 something,

338
00:19:51,000 --> 00:19:53,000
 you have to learn about your tools.

339
00:19:53,000 --> 00:19:57,150
 This is a chisel, this is a hammer, this is this, this is

340
00:19:57,150 --> 00:19:58,000
 that.

341
00:19:58,000 --> 00:20:01,200
 That's what we're doing now, learning how reality works,

342
00:20:01,200 --> 00:20:04,000
 learning what it's all about.

343
00:20:04,000 --> 00:20:08,000
 Once you do that, then the doubt disappears. It means you

344
00:20:08,000 --> 00:20:08,000
 get this composure of mind.

345
00:20:08,000 --> 00:20:14,000
 That's when you're able to use the mind as your tool.

346
00:20:14,000 --> 00:20:20,370
 The mind has become a useful tool that you can apply and

347
00:20:20,370 --> 00:20:22,000
 begin to address

348
00:20:22,000 --> 00:20:31,000
 and overcome suffering and stress and difficulty.

349
00:20:31,000 --> 00:20:39,000
 So, here we are, purification number four already.

350
00:20:39,000 --> 00:20:44,000
 That's the demo for tonight. Thank you all for coming out.

351
00:20:46,000 --> 00:21:12,000
 [silence]

352
00:21:12,000 --> 00:21:17,990
 The site again is not loading. There was some problem with

353
00:21:17,990 --> 00:21:19,000
 it last night

354
00:21:19,000 --> 00:21:25,000
 and again tonight it's not loading.

355
00:21:25,000 --> 00:21:34,000
 [silence]

356
00:21:34,000 --> 00:21:42,580
 I'm performing a TLS handshake to meditation.serimongolo.

357
00:21:42,580 --> 00:21:44,000
org.

358
00:21:44,000 --> 00:21:55,000
 It's good to know my browser at least has good manners.

359
00:21:55,000 --> 00:22:02,870
 The server is not accepting the handshake though. Oh, here

360
00:22:02,870 --> 00:22:04,000
 we go.

361
00:22:04,000 --> 00:22:21,000
 [silence]

362
00:22:21,000 --> 00:22:24,000
 Everything should be working. We also have an audio stream.

363
00:22:24,000 --> 00:22:27,000
 I don't know if anyone is actually listening to it.

364
00:22:27,000 --> 00:22:30,000
 Hopefully it's also working well.

365
00:22:30,000 --> 00:22:56,000
 [silence]

366
00:22:56,000 --> 00:22:59,500
 There must be some bottleneck. Everyone trying to access

367
00:22:59,500 --> 00:23:00,000
 the site all at once

368
00:23:00,000 --> 00:23:05,000
 is causing it to not work, which I really think we...

369
00:23:05,000 --> 00:23:12,000
 I know our IT people are overworked and underpaid,

370
00:23:12,000 --> 00:23:21,000
 but I really think we should assess whether there's

371
00:23:21,000 --> 00:23:21,000
 something

372
00:23:21,000 --> 00:23:27,510
 fundamentally wrong with our setup because if it keeps

373
00:23:27,510 --> 00:23:28,000
 stopping

374
00:23:28,000 --> 00:23:31,000
 and we have to reset it, it really is a sign.

375
00:23:31,000 --> 00:23:36,520
 It's clearly only when, or it seems to be only when, we do

376
00:23:36,520 --> 00:23:38,000
 this session.

377
00:23:38,000 --> 00:23:47,760
 It seems to be clearly a case of too many people trying to

378
00:23:47,760 --> 00:23:49,000
 access it once,

379
00:23:49,000 --> 00:23:52,000
 which means we need a better service, I think.

380
00:23:52,000 --> 00:24:00,980
 Something is... there's some bottleneck that has to be

381
00:24:00,980 --> 00:24:04,000
 addressed.

382
00:24:04,000 --> 00:24:08,190
 All right. Well, another excuse for me not to answer

383
00:24:08,190 --> 00:24:09,000
 questions.

384
00:24:09,000 --> 00:24:13,000
 Thank you all for tuning in. Have a good night.

385
00:24:14,000 --> 00:24:21,000
 [silence]

