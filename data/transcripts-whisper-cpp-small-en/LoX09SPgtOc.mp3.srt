1
00:00:00,000 --> 00:00:12,000
 [birds chirping]

2
00:00:12,000 --> 00:00:22,000
 [birds chirping]

3
00:00:22,000 --> 00:00:32,000
 [birds chirping]

4
00:00:32,000 --> 00:00:42,000
 [birds chirping]

5
00:00:42,000 --> 00:00:52,000
 [birds chirping]

6
00:00:52,000 --> 00:01:02,000
 [birds chirping]

7
00:01:02,000 --> 00:01:12,000
 [birds chirping]

8
00:01:12,000 --> 00:01:22,000
 [birds chirping]

9
00:01:22,000 --> 00:01:32,000
 [birds chirping]

10
00:01:32,000 --> 00:01:42,000
 [birds chirping]

11
00:01:42,000 --> 00:01:47,000
 So here I am in Sri Lanka, my first destination.

12
00:01:47,000 --> 00:01:49,990
 This is the monastery I'll be staying at for the next few

13
00:01:49,990 --> 00:01:50,000
 days

14
00:01:50,000 --> 00:01:56,000
 while we have this conference on Buddhism.

15
00:01:56,000 --> 00:02:00,000
 The thing I'd like to share today, I was thinking

16
00:02:00,000 --> 00:02:03,520
 something I learned over the past two weeks in Thailand

17
00:02:03,520 --> 00:02:05,000
 while I was meditating.

18
00:02:05,000 --> 00:02:10,380
 And it didn't come...the clarity of it came, or the

19
00:02:10,380 --> 00:02:11,000
 inspiration for it,

20
00:02:11,000 --> 00:02:16,000
 came not from the meditation, but from actually a deviation

21
00:02:16,000 --> 00:02:19,000
 or a diversion from the meditation.

22
00:02:19,000 --> 00:02:25,000
 There's a monk who's a relative of my teacher,

23
00:02:25,000 --> 00:02:29,000
 his nephew or great nephew or whatever,

24
00:02:29,000 --> 00:02:34,580
 and he disrobed while I was there, which is kind of

25
00:02:34,580 --> 00:02:36,000
 disappointing.

26
00:02:36,000 --> 00:02:40,000
 But he's an interesting monk.

27
00:02:40,000 --> 00:02:45,040
 And he said something that really struck me, that really

28
00:02:45,040 --> 00:02:47,000
 surprised me actually.

29
00:02:47,000 --> 00:02:55,560
 He had the understanding that the point of the Buddha's

30
00:02:55,560 --> 00:02:56,000
 teaching,

31
00:02:56,000 --> 00:02:58,000
 the point of Vipassana, of insight meditation,

32
00:02:58,000 --> 00:03:05,330
 is to suppress or cover up the defilements of greed, of

33
00:03:05,330 --> 00:03:08,000
 anger and delusion

34
00:03:08,000 --> 00:03:16,000
 to the point that they decompose, basically, is what he was

35
00:03:16,000 --> 00:03:16,000
 saying.

36
00:03:16,000 --> 00:03:22,000
 They rot, and when they rot, then they disappear.

37
00:03:22,000 --> 00:03:27,470
 And I immediately argued with him, but at the time it wasn

38
00:03:27,470 --> 00:03:29,000
't clear in my mind

39
00:03:29,000 --> 00:03:32,000
 what exactly he was saying and what importance it had.

40
00:03:32,000 --> 00:03:35,700
 But when I went back to meditate, I'm putting it together

41
00:03:35,700 --> 00:03:37,000
 with my meditation,

42
00:03:37,000 --> 00:03:47,550
 and the fact that it seems so clear that meditation is not

43
00:03:47,550 --> 00:03:48,000
 that,

44
00:03:48,000 --> 00:03:50,000
 or insight meditation is not that.

45
00:03:50,000 --> 00:03:53,710
 The insight meditation is actually taking the lid off the

46
00:03:53,710 --> 00:03:55,000
 pot, so to speak,

47
00:03:55,000 --> 00:04:00,000
 of our rotting mind, our mind that the Buddha said is,

48
00:04:00,000 --> 00:04:02,000
 he called the defilements asava.

49
00:04:02,000 --> 00:04:06,000
 Asava means rotten, and they rot the mind.

50
00:04:06,000 --> 00:04:10,000
 They cause the mind to become pickled, actually,

51
00:04:10,000 --> 00:04:14,000
 or I don't know what the right word would be.

52
00:04:14,000 --> 00:04:18,000
 And so they don't disappear, they rot the mind.

53
00:04:18,000 --> 00:04:23,000
 They're in their rotting, causing the mind to be rotten.

54
00:04:23,000 --> 00:04:25,000
 That's not a good thing.

55
00:04:25,000 --> 00:04:29,990
 It's not a compost pile where we're talking about ourselves

56
00:04:29,990 --> 00:04:31,000
 and our minds,

57
00:04:31,000 --> 00:04:39,000
 something that is important to purify and to make pure.

58
00:04:39,000 --> 00:04:44,000
 And so what I thought, what I was thinking of this morning,

59
00:04:44,000 --> 00:04:46,000
 I was going to talk about this, and I thought,

60
00:04:46,000 --> 00:04:48,000
 what is this in terms of the Buddha's teaching?

61
00:04:48,000 --> 00:04:52,080
 And then I realized this is what the Buddha meant by the

62
00:04:52,080 --> 00:04:53,000
 middle way.

63
00:04:53,000 --> 00:04:57,000
 And this is the profundity that we don't often realize.

64
00:04:57,000 --> 00:05:02,680
 We hear the Buddha said, don't go to the extreme of sensual

65
00:05:02,680 --> 00:05:03,000
 attachment

66
00:05:03,000 --> 00:05:06,000
 to sensual pleasures where you're following after them.

67
00:05:06,000 --> 00:05:09,000
 So we say, okay, fine, I won't be a prince, and so on.

68
00:05:09,000 --> 00:05:13,000
 I won't dedicate my life to sensuality.

69
00:05:13,000 --> 00:05:15,000
 And then don't torture yourself.

70
00:05:15,000 --> 00:05:19,000
 So we say, okay, so I won't go off and go naked and stand

71
00:05:19,000 --> 00:05:20,000
 in the sun

72
00:05:20,000 --> 00:05:25,040
 and not eat, and all these ascetic practices that they had

73
00:05:25,040 --> 00:05:26,000
 in India.

74
00:05:26,000 --> 00:05:29,000
 But it's so much more immediate than that.

75
00:05:29,000 --> 00:05:34,000
 That's not what is meant by these two extremes.

76
00:05:34,000 --> 00:05:37,000
 The two extremes are how we approach reality.

77
00:05:37,000 --> 00:05:40,450
 The addiction to sensual pleasures is when the emotions

78
00:05:40,450 --> 00:05:41,000
 come up,

79
00:05:41,000 --> 00:05:44,640
 whether it's greed or anger or delusion, we follow after

80
00:05:44,640 --> 00:05:45,000
 them.

81
00:05:45,000 --> 00:05:49,000
 When we want something, we chase after it.

82
00:05:49,000 --> 00:05:52,000
 When you have desire for something, you go for it.

83
00:05:52,000 --> 00:05:56,000
 You say, okay, I want it, therefore I should get it.

84
00:05:56,000 --> 00:05:58,000
 I should chase after it.

85
00:05:58,000 --> 00:06:03,440
 The addiction to or attachment to torturing yourself is so

86
00:06:03,440 --> 00:06:05,000
 perfectly clear.

87
00:06:05,000 --> 00:06:07,000
 And this is exactly what this monk was doing.

88
00:06:07,000 --> 00:06:09,690
 It's what most of us do, especially when we hear about

89
00:06:09,690 --> 00:06:10,000
 Buddhism.

90
00:06:10,000 --> 00:06:15,000
 We hear the Buddha taught, and greed is bad, anger is bad,

91
00:06:15,000 --> 00:06:16,000
 delusion is bad.

92
00:06:16,000 --> 00:06:20,990
 And we get this idea that the Buddha was condemning people

93
00:06:20,990 --> 00:06:21,000
 who had these things.

94
00:06:21,000 --> 00:06:24,000
 Contemming the person for giving rise to these things.

95
00:06:24,000 --> 00:06:27,670
 And we say, okay, so in that case, the point is to suppress

96
00:06:27,670 --> 00:06:29,000
 them, to stop them from coming.

97
00:06:29,000 --> 00:06:33,000
 When they come up, that's bad, that's sinful, that's evil.

98
00:06:33,000 --> 00:06:35,000
 But really, this is not how the Buddha taught.

99
00:06:35,000 --> 00:06:37,000
 The Buddha taught us to understand these things.

100
00:06:37,000 --> 00:06:41,170
 He taught us to understand greed, to understand anger, to

101
00:06:41,170 --> 00:06:43,000
 understand delusion.

102
00:06:43,000 --> 00:06:45,600
 And we don't get this because we haven't reached the middle

103
00:06:45,600 --> 00:06:46,000
 way.

104
00:06:46,000 --> 00:06:48,520
 We think if you let them come up, you're going to follow

105
00:06:48,520 --> 00:06:49,000
 them.

106
00:06:49,000 --> 00:06:51,000
 There's only two ways.

107
00:06:51,000 --> 00:06:53,000
 You either follow them or you suppress them.

108
00:06:53,000 --> 00:06:54,000
 But this is the point.

109
00:06:54,000 --> 00:06:56,000
 The Buddha said there's a third way.

110
00:06:56,000 --> 00:06:58,000
 When they come up, you don't have to follow them.

111
00:06:58,000 --> 00:07:00,000
 But you don't have to suppress them.

112
00:07:00,000 --> 00:07:02,000
 They come up, we say, it's a problem.

113
00:07:02,000 --> 00:07:04,000
 If I don't suppress it, I'm going to follow it.

114
00:07:04,000 --> 00:07:10,180
 I'm going to become greedy or I'm going to chase after and

115
00:07:10,180 --> 00:07:12,000
 become addicted to these things.

116
00:07:12,000 --> 00:07:14,000
 But it's not the case.

117
00:07:14,000 --> 00:07:17,440
 So the true vipassana, if you ask me from my understanding

118
00:07:17,440 --> 00:07:19,000
 of the Buddha's teaching

119
00:07:19,000 --> 00:07:23,130
 and from my own practice, is that these things come up and

120
00:07:23,130 --> 00:07:24,000
 they can be really scary

121
00:07:24,000 --> 00:07:27,000
 because we think this is bad, this shouldn't be happening.

122
00:07:27,000 --> 00:07:33,000
 You want something, your lust comes up or you hate someone.

123
00:07:33,000 --> 00:07:35,600
 Some people find when they meditate, suddenly they're full

124
00:07:35,600 --> 00:07:36,000
 of hate.

125
00:07:36,000 --> 00:07:37,000
 They hate everyone.

126
00:07:37,000 --> 00:07:39,000
 They hate everything that comes up.

127
00:07:39,000 --> 00:07:41,000
 They're angry at everything.

128
00:07:41,000 --> 00:07:43,000
 But these are the emotions that exist in our minds.

129
00:07:43,000 --> 00:07:46,000
 Normally we would feel guilty about them and suppress them

130
00:07:46,000 --> 00:07:46,000
 and say,

131
00:07:46,000 --> 00:07:48,000
 "That's bad. That's sinful. That's wrong."

132
00:07:48,000 --> 00:07:51,000
 And they just rot there. They don't go away.

133
00:07:51,000 --> 00:07:54,000
 They pickle our mind.

134
00:07:54,000 --> 00:08:00,000
 They cause the mind to become rotten and corrupted.

135
00:08:00,000 --> 00:08:08,000
 So this is the scary truth of the Buddha's teaching.

136
00:08:08,000 --> 00:08:10,000
 You have to let these things come up.

137
00:08:10,000 --> 00:08:12,750
 You have to be able to accept the evil that's inside of you

138
00:08:12,750 --> 00:08:13,000
.

139
00:08:13,000 --> 00:08:15,000
 We say it's evil.

140
00:08:15,000 --> 00:08:18,230
 The reason we say it's evil is because it causes you

141
00:08:18,230 --> 00:08:19,000
 suffering.

142
00:08:19,000 --> 00:08:22,290
 But that doesn't mean suppressing it is going to answer

143
00:08:22,290 --> 00:08:23,000
 things.

144
00:08:23,000 --> 00:08:25,000
 It doesn't mean you can deny the existence.

145
00:08:25,000 --> 00:08:27,560
 You have to accept the fact that inside of me there are

146
00:08:27,560 --> 00:08:29,000
 things that are,

147
00:08:29,000 --> 00:08:31,210
 if you don't like the word evil, they're unwholesome or

148
00:08:31,210 --> 00:08:33,000
 they're unskillful

149
00:08:33,000 --> 00:08:37,470
 or they hurt us and they hurt other people and we should

150
00:08:37,470 --> 00:08:38,000
 get rid of them.

151
00:08:38,000 --> 00:08:41,000
 They're not going to go away just by covering them up.

152
00:08:41,000 --> 00:08:47,000
 This is what really hit me talking to this monk and through

153
00:08:47,000 --> 00:08:49,000
 the meditation practice

154
00:08:49,000 --> 00:08:54,000
 is that they're not going to disappear by themselves.

155
00:08:54,000 --> 00:08:58,000
 They have to come up and you have to accept them

156
00:08:58,000 --> 00:09:02,600
 in the sense of accepting that they're there and take them

157
00:09:02,600 --> 00:09:03,000
 apart,

158
00:09:03,000 --> 00:09:06,910
 pull them apart piece by piece and see them for what they

159
00:09:06,910 --> 00:09:07,000
 are.

160
00:09:07,000 --> 00:09:10,000
 Because in fact all that they are is a knot.

161
00:09:10,000 --> 00:09:15,000
 You look at this and you say it's a knot.

162
00:09:15,000 --> 00:09:17,000
 It's tangled up. My hands are tangled up.

163
00:09:17,000 --> 00:09:20,230
 But really when you pull it apart there's no tangle. It's

164
00:09:20,230 --> 00:09:21,000
 disappeared.

165
00:09:21,000 --> 00:09:25,000
 When you untie the knot all you have is a straight row.

166
00:09:25,000 --> 00:09:29,000
 The knot is in fact a non-entity.

167
00:09:29,000 --> 00:09:32,000
 And this is a very important part of what the Buddha meant

168
00:09:32,000 --> 00:09:35,000
 by anatta of non-self.

169
00:09:35,000 --> 00:09:38,000
 It's not only that we don't have a self.

170
00:09:38,000 --> 00:09:43,290
 The point is that every phenomenon that arises is a non-

171
00:09:43,290 --> 00:09:45,000
entity.

172
00:09:45,000 --> 00:09:47,000
 It's not something that exists in and of itself

173
00:09:47,000 --> 00:09:51,380
 and is going to be in our mind forever or that we have to

174
00:09:51,380 --> 00:09:53,000
 attack or fight.

175
00:09:53,000 --> 00:09:57,000
 Once we see it we see that it's nothing. It's immaterial.

176
00:09:57,000 --> 00:10:01,000
 It arises and it ceases and it has causes and conditions.

177
00:10:01,000 --> 00:10:05,000
 If we don't give rise to the cause the result won't come.

178
00:10:05,000 --> 00:10:07,000
 So this is my thought for the day.

179
00:10:07,000 --> 00:10:09,000
 I think it's a very important teaching.

180
00:10:09,000 --> 00:10:12,180
 Very important from the point of view of vipassana

181
00:10:12,180 --> 00:10:13,000
 meditation

182
00:10:13,000 --> 00:10:16,080
 and even more so because it's the first thing that the

183
00:10:16,080 --> 00:10:17,000
 Buddha taught.

184
00:10:17,000 --> 00:10:20,400
 When he turned the wheel of the Dhamma at Saranath for the

185
00:10:20,400 --> 00:10:21,000
 five monks,

186
00:10:21,000 --> 00:10:24,000
 these first five people who were supposed to be the first

187
00:10:24,000 --> 00:10:27,000
 five people to understand the Buddha's teaching.

188
00:10:27,000 --> 00:10:29,000
 This is the first thing he taught.

189
00:10:29,000 --> 00:10:32,000
 "Vikove anta bhachidena nasevitabha"

190
00:10:32,000 --> 00:10:37,000
 There are these two extremes that no one who has left

191
00:10:37,000 --> 00:10:38,000
 behind

192
00:10:38,000 --> 00:10:42,600
 or who has gone forth in search of freedom should follow

193
00:10:42,600 --> 00:10:43,000
 after.

194
00:10:43,000 --> 00:10:48,610
 This is the addiction to sensuality or in the sense of

195
00:10:48,610 --> 00:10:53,000
 chasing after the phenomena that arise

196
00:10:53,000 --> 00:10:59,000
 and the suppression of them or the torturing of themselves.

197
00:10:59,000 --> 00:11:06,000
 The suppressing and repressing one's desire.

198
00:11:06,000 --> 00:11:09,790
 There's an addiction to torturing yourself but this is how

199
00:11:09,790 --> 00:11:11,000
 we torture ourselves.

200
00:11:11,000 --> 00:11:14,270
 We feel guilty, we hate ourselves, we cause ourselves

201
00:11:14,270 --> 00:11:15,000
 suffering

202
00:11:15,000 --> 00:11:19,130
 thinking that somehow you don't have to stop eating or

203
00:11:19,130 --> 00:11:20,000
 stand on one leg

204
00:11:20,000 --> 00:11:23,940
 or all these crazy things that they did in the Buddhist

205
00:11:23,940 --> 00:11:25,000
 time in India

206
00:11:25,000 --> 00:11:27,000
 in order to torture yourself.

207
00:11:27,000 --> 00:11:30,000
 Just by hating yourself, by feeling guilty about yourself.

208
00:11:30,000 --> 00:11:33,200
 That is directly a form of torture and that's exactly what

209
00:11:33,200 --> 00:11:34,000
 it's meant here.

210
00:11:34,000 --> 00:11:37,000
 This is exactly what you experience in meditation.

211
00:11:37,000 --> 00:11:39,000
 You go between these two extremes.

212
00:11:39,000 --> 00:11:42,000
 One moment you're chasing after them, this is great, I love

213
00:11:42,000 --> 00:11:42,000
 it.

214
00:11:42,000 --> 00:11:45,000
 The next moment you're castigating yourself for it.

215
00:11:45,000 --> 00:11:49,690
 This is evil, this is bad, I'm a bad person and anger and

216
00:11:49,690 --> 00:11:51,000
 hatred arises

217
00:11:51,000 --> 00:11:53,000
 and you're torturing yourself.

218
00:11:53,000 --> 00:11:55,000
 So thanks for tuning in, this is Sri Lanka.

