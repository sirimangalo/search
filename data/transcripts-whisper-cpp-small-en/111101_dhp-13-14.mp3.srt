1
00:00:00,000 --> 00:00:05,000
 Okay, so today we continue our study of the Dhammapada.

2
00:00:05,000 --> 00:00:12,000
 Continuing on with verses 13 and 14.

3
00:00:12,000 --> 00:00:14,000
 Watch, read as follows.

4
00:00:16,000 --> 00:00:19,000
 Yatha Agaran Duchanang

5
00:00:19,000 --> 00:00:22,000
 Vuthi

6
00:00:22,000 --> 00:00:25,000
 Vuthi Sammati Vijati

7
00:00:25,000 --> 00:00:28,000
 Ivaanabhavitamjitang

8
00:00:28,000 --> 00:00:32,000
 Raghos Sammati Vijati

9
00:00:32,000 --> 00:00:38,000
 Yatha Agaran Sukshanang

10
00:00:38,000 --> 00:00:43,000
 Vuthi Na Sammati Vijati

11
00:00:43,000 --> 00:00:47,000
 Ivaanabhavitamjitang

12
00:00:47,000 --> 00:00:51,000
 Raghos Sammati Vijati

13
00:00:51,000 --> 00:00:55,000
 And the meaning is as follows.

14
00:00:55,000 --> 00:01:01,000
 Yatha Agaran, just as a house or a building,

15
00:01:01,000 --> 00:01:06,000
 Duchanang that is poorly thatched

16
00:01:06,000 --> 00:01:11,000
 or a roof, just as a roof that is poorly thatched,

17
00:01:11,000 --> 00:01:15,000
 the rain is able to penetrate.

18
00:01:15,000 --> 00:01:19,000
 Vuthi Sammati Vijati

19
00:01:19,000 --> 00:01:22,000
 In the same way an untrained mind,

20
00:01:22,000 --> 00:01:25,000
 Ivaanabhavitamjitang

21
00:01:25,000 --> 00:01:28,000
 Raghos Sammati Vijati

22
00:01:28,000 --> 00:01:33,000
 The lust is able to penetrate.

23
00:01:33,000 --> 00:01:41,000
 And number 14, just as in regards to a well trained mind,

24
00:01:41,000 --> 00:01:45,000
 sorry, in regards to a well thatched hut,

25
00:01:45,000 --> 00:01:47,000
 Vuthi Na Sammati Vijati

26
00:01:47,000 --> 00:01:50,000
 Rain is not able to penetrate.

27
00:01:50,000 --> 00:01:54,000
 In the same way a well developed mind,

28
00:01:54,000 --> 00:01:58,000
 lust is not able to penetrate.

29
00:01:58,000 --> 00:02:03,000
 So, quite useful for meditators.

30
00:02:03,000 --> 00:02:05,000
 This is a meditation teaching.

31
00:02:05,000 --> 00:02:08,000
 He was taught in regards to the Buddha's cousin,

32
00:02:08,000 --> 00:02:11,000
 the Buddha's cousin Nanda.

33
00:02:11,000 --> 00:02:15,350
 When the Buddha went, after Siddhartha Gautama became

34
00:02:15,350 --> 00:02:16,000
 enlightened,

35
00:02:16,000 --> 00:02:22,000
 he went back to his home of Kapilavatu

36
00:02:22,000 --> 00:02:24,000
 and he taught all of his relatives.

37
00:02:24,000 --> 00:02:27,000
 And so he taught his ex-wife, he taught his son,

38
00:02:27,000 --> 00:02:31,880
 he taught his father, and they all were able to understand

39
00:02:31,880 --> 00:02:33,000
 the teachings.

40
00:02:33,000 --> 00:02:37,000
 But there was one person who, it seems the Buddha

41
00:02:37,000 --> 00:02:38,000
 understood,

42
00:02:38,000 --> 00:02:40,000
 he had the ability to penetrate the teachings,

43
00:02:40,000 --> 00:02:43,000
 but was kind of stuck in a bit of a rut.

44
00:02:43,000 --> 00:02:46,130
 And that is that he was set to get married to the most

45
00:02:46,130 --> 00:02:49,000
 beautiful woman in the country.

46
00:02:49,000 --> 00:02:53,000
 Janapadaka Yani, which means the beauty of the land.

47
00:02:53,000 --> 00:02:56,760
 So in the area where they lived, she must have been the

48
00:02:56,760 --> 00:02:58,000
 prize, Muthi.

49
00:02:58,000 --> 00:03:00,000
 So she was well known for her Muthi,

50
00:03:00,000 --> 00:03:04,000
 and Nanda had become infatuated with his beauty.

51
00:03:04,000 --> 00:03:07,000
 And so like ordinary human beings,

52
00:03:07,000 --> 00:03:11,430
 he felt that that was going to bring him happiness to marry

53
00:03:11,430 --> 00:03:12,000
 this woman

54
00:03:12,000 --> 00:03:19,000
 and to live a life of sensual pleasure.

55
00:03:19,000 --> 00:03:23,520
 But Buddhists, as Buddhists, we always throw monkey w

56
00:03:23,520 --> 00:03:24,000
renches into these sorts of things.

57
00:03:24,000 --> 00:03:29,000
 We always put a reign on people's parade.

58
00:03:29,000 --> 00:03:33,000
 And the Buddha was certainly our leader in this regard.

59
00:03:33,000 --> 00:03:36,000
 So on the day when they were to be married,

60
00:03:36,000 --> 00:03:39,000
 or during the marriage preparations,

61
00:03:39,000 --> 00:03:43,000
 they invited the Buddha to come to receive alms.

62
00:03:43,000 --> 00:03:49,050
 And when the Buddha finished his alms, he handed the bowl

63
00:03:49,050 --> 00:03:50,000
 to Nanda.

64
00:03:50,000 --> 00:03:53,840
 When the Buddha finished his meal and had given a talk on

65
00:03:53,840 --> 00:03:56,000
 the teaching,

66
00:03:56,000 --> 00:03:58,860
 he gave his bowl to Nanda and he started walking back to

67
00:03:58,860 --> 00:04:00,000
 the monastery.

68
00:04:00,000 --> 00:04:03,000
 And so when he got to the top of the stairs,

69
00:04:03,000 --> 00:04:05,710
 Nanda thought, "Well, I guess he'll take his bowl back now

70
00:04:05,710 --> 00:04:06,000
."

71
00:04:06,000 --> 00:04:09,670
 But he didn't say, "Excuse me, sir, can you take your bowl

72
00:04:09,670 --> 00:04:10,000
 back?"

73
00:04:10,000 --> 00:04:13,800
 Because he felt that that would be rude. And he had a lot

74
00:04:13,800 --> 00:04:15,000
 of respect for the Buddha.

75
00:04:15,000 --> 00:04:20,200
 And he never had great respect for the Buddha's teaching as

76
00:04:20,200 --> 00:04:21,000
 well.

77
00:04:21,000 --> 00:04:23,000
 But the Buddha didn't take back his bowl.

78
00:04:23,000 --> 00:04:25,000
 So they got to the bottom of the stairs and Nanda thought,

79
00:04:25,000 --> 00:04:27,510
 "Oh, now he'll take back his bowl." And the Buddha didn't

80
00:04:27,510 --> 00:04:28,000
 take back his bowl.

81
00:04:28,000 --> 00:04:30,440
 Then he got to the door of the palace and he thought, "Now

82
00:04:30,440 --> 00:04:31,000
 he'll take his bowl."

83
00:04:31,000 --> 00:04:33,800
 And they got to the door of the city and now he kept

84
00:04:33,800 --> 00:04:35,000
 following after him

85
00:04:35,000 --> 00:04:39,020
 with his bowl thinking, "Why doesn't the Buddha take back

86
00:04:39,020 --> 00:04:40,000
 his bowl?"

87
00:04:40,000 --> 00:04:43,000
 And so people saw this and they started passing it around

88
00:04:43,000 --> 00:04:48,000
 and they sent that word back to his bride-to-be saying,

89
00:04:48,000 --> 00:04:52,680
 "It looks like this. Ammana Gautama has decided to capture

90
00:04:52,680 --> 00:04:54,000
 your husband,

91
00:04:54,000 --> 00:04:56,450
 your future husband, and he's going to spoil your whole

92
00:04:56,450 --> 00:04:57,000
 plans."

93
00:04:57,000 --> 00:05:01,000
 And so she ran after them and she called out to him,

94
00:05:01,000 --> 00:05:04,430
 "Oh, please, please, please, please, please, Sir, hurry

95
00:05:04,430 --> 00:05:05,000
 back soon."

96
00:05:05,000 --> 00:05:09,000
 In the most beautiful voice she could muster.

97
00:05:09,000 --> 00:05:12,000
 And so this was, of course, tearing at his heart

98
00:05:12,000 --> 00:05:16,000
 because he was very much in love with her.

99
00:05:16,000 --> 00:05:20,000
 And making Buddha look like a bit of a meanie, no?

100
00:05:20,000 --> 00:05:24,000
 And so the Buddha, when he got back to the monastery,

101
00:05:24,000 --> 00:05:27,000
 Nanda still thought, "Okay, well, when's he going to turn?"

102
00:05:27,000 --> 00:05:30,000
 He thought right all the way back to the Buddha's kutti.

103
00:05:30,000 --> 00:05:32,000
 And the Buddha turned to Nanda and said,

104
00:05:32,000 --> 00:05:36,000
 "So, Nanda, would you like to become a monk?"

105
00:05:36,000 --> 00:05:39,050
 And Nanda's thinking in his heart, "No, no, no, no, no, I

106
00:05:39,050 --> 00:05:40,000
 wouldn't like to."

107
00:05:40,000 --> 00:05:42,000
 But he had so much respect for the Buddha,

108
00:05:42,000 --> 00:05:45,300
 as people in this situation often are, when confronted by a

109
00:05:45,300 --> 00:05:47,000
 person of authority.

110
00:05:47,000 --> 00:05:51,490
 And he had been so used to, of course, to going according

111
00:05:51,490 --> 00:05:52,000
 to the flow,

112
00:05:52,000 --> 00:05:56,000
 because he was a prince and he was living in luxury

113
00:05:56,000 --> 00:05:59,570
 and you just kind of go along with things and everything is

114
00:05:59,570 --> 00:06:00,000
 wonderful

115
00:06:00,000 --> 00:06:03,000
 because you're a prince.

116
00:06:03,000 --> 00:06:07,000
 So right away he said, "Sure, I'll become a monk."

117
00:06:07,000 --> 00:06:13,160
 But in his heart he was so mortified that he would have to

118
00:06:13,160 --> 00:06:15,000
 become a monk.

119
00:06:15,000 --> 00:06:17,000
 And so the Buddha said, "Okay."

120
00:06:17,000 --> 00:06:19,190
 And he called the other monks over and they ordained him,

121
00:06:19,190 --> 00:06:20,000
 right?

122
00:06:20,000 --> 00:06:22,000
 And then there.

123
00:06:22,000 --> 00:06:27,000
 And then the Buddha took him back, I believe, to Rajagaha.

124
00:06:27,000 --> 00:06:34,000
 And in that time Nanda started getting quite perturbed

125
00:06:34,000 --> 00:06:37,000
 and it was quite miserable as a monk, of course,

126
00:06:37,000 --> 00:06:40,000
 because he was always thinking about his bride to be,

127
00:06:40,000 --> 00:06:42,000
 who he was desperately in love with.

128
00:06:42,000 --> 00:06:44,000
 And he thought, "This is my soulmate.

129
00:06:44,000 --> 00:06:47,000
 This is the one person who means anything to me.

130
00:06:47,000 --> 00:06:49,000
 And here the Buddha has taken this from me."

131
00:06:49,000 --> 00:06:52,000
 He wasn't upset at the Buddha, but he was so miserable

132
00:06:52,000 --> 00:06:54,000
 because he was always thinking about the pleasure

133
00:06:54,000 --> 00:07:01,000
 and the great love that he had for her.

134
00:07:01,000 --> 00:07:04,000
 And so he said to his fellow monks, finally he had enough

135
00:07:04,000 --> 00:07:06,000
 and he said to them, he said, "I've given up.

136
00:07:06,000 --> 00:07:09,000
 I'm going to disrobe and become a lay person again.

137
00:07:09,000 --> 00:07:11,000
 I can't take this anymore.

138
00:07:11,000 --> 00:07:14,000
 This isn't my role in life. This isn't my path.

139
00:07:14,000 --> 00:07:17,620
 I'm in love with this woman and she means everything to me

140
00:07:17,620 --> 00:07:18,000
."

141
00:07:18,000 --> 00:07:20,000
 Of course, this is how we always justify it, right?

142
00:07:20,000 --> 00:07:23,000
 When we have central pleasure, we always claim that there's

143
00:07:23,000 --> 00:07:24,000
 a person

144
00:07:24,000 --> 00:07:26,000
 who has some importance.

145
00:07:26,000 --> 00:07:28,000
 We never get right down to it and say,

146
00:07:28,000 --> 00:07:30,680
 "I just think she's beautiful and I want to see her and

147
00:07:30,680 --> 00:07:31,000
 touch her

148
00:07:31,000 --> 00:07:35,000
 and receive pleasure from her."

149
00:07:35,000 --> 00:07:39,430
 We say she has great meaning and she's my soulmate and so

150
00:07:39,430 --> 00:07:40,000
 on.

151
00:07:40,000 --> 00:07:42,000
 This is how we always feel.

152
00:07:42,000 --> 00:07:44,000
 What happened? So they brought him to the Buddha

153
00:07:44,000 --> 00:07:50,440
 and they said, "Lord Buddha, this Nanda here, he says he's

154
00:07:50,440 --> 00:07:52,000
 going to disrobe."

155
00:07:52,000 --> 00:07:56,600
 And the Buddha takes Nanda. He said, "Okay, I'll look after

156
00:07:56,600 --> 00:07:57,000
 this."

157
00:07:57,000 --> 00:08:03,000
 He took Nanda by his arm and brought him magically.

158
00:08:03,000 --> 00:08:09,000
 First he brought him to this burnt out forest.

159
00:08:09,000 --> 00:08:12,000
 It was a forest fire and it was totally burnt down.

160
00:08:12,000 --> 00:08:16,000
 All that was left was this she-monkey, this female monkey

161
00:08:16,000 --> 00:08:19,540
 who was clinging to a stump of a tree that she probably

162
00:08:19,540 --> 00:08:20,000
 used to live in.

163
00:08:20,000 --> 00:08:24,000
 It was like the last standing burnt out tree.

164
00:08:24,000 --> 00:08:30,190
 Her tail had been burnt off and her ears had been burnt off

165
00:08:30,190 --> 00:08:31,000
.

166
00:08:31,000 --> 00:08:35,970
 What else? Her tail and her ears and her fur had been sing

167
00:08:35,970 --> 00:08:37,000
ed and so on.

168
00:08:37,000 --> 00:08:41,000
 There she was clinging in great suffering and pain

169
00:08:41,000 --> 00:08:45,000
 and she was just looking like a total miserable sight.

170
00:08:45,000 --> 00:08:47,000
 The Buddha said, "You see that monkey over there?"

171
00:08:47,000 --> 00:08:49,000
 He said, "Yes, yes, I see that monkey."

172
00:08:49,000 --> 00:08:52,000
 He didn't quite understand what the point was.

173
00:08:52,000 --> 00:08:55,000
 This female monkey there.

174
00:08:55,000 --> 00:08:57,000
 Then he said, "Okay, now let's go."

175
00:08:57,000 --> 00:09:00,000
 He took him magically up to the heavens.

176
00:09:00,000 --> 00:09:03,000
 He took him to the Dabhatingsa heaven, I think.

177
00:09:03,000 --> 00:09:08,000
 There he showed him 500 dove-footed nymphs

178
00:09:08,000 --> 00:09:11,000
 I think, I'm not sure what the poly is,

179
00:09:11,000 --> 00:09:13,000
 but something about their feet being pink,

180
00:09:13,000 --> 00:09:19,000
 but just beautiful nymphs, of course angels.

181
00:09:19,000 --> 00:09:23,230
 The thing about nymphs, of course, is that they're so much

182
00:09:23,230 --> 00:09:24,000
 more beautiful,

183
00:09:24,000 --> 00:09:27,000
 essentially attractive than any human being.

184
00:09:27,000 --> 00:09:31,000
 It's hard to imagine, it's not like just beautiful women.

185
00:09:31,000 --> 00:09:35,000
 These are people with great merit

186
00:09:35,000 --> 00:09:39,120
 and so they have become very beautiful as a result of their

187
00:09:39,120 --> 00:09:41,000
 beauty of mind.

188
00:09:41,000 --> 00:09:45,000
 The physical has come to reflect that

189
00:09:45,000 --> 00:09:49,000
 beyond anything most of us have ever come across.

190
00:09:49,000 --> 00:09:52,000
 Nanda was quite blown away

191
00:09:52,000 --> 00:09:56,180
 and seeing these 500 nymphs playing around or doing their

192
00:09:56,180 --> 00:09:58,000
 thing.

193
00:09:58,000 --> 00:10:01,000
 The Buddha said, "So tell me Nanda,

194
00:10:01,000 --> 00:10:05,000
 which one is more beautiful, your wife, Janapa Dakhalyaami,

195
00:10:05,000 --> 00:10:08,000
 or these 500 dove-footed nymphs?"

196
00:10:08,000 --> 00:10:11,710
 Nanda, who by this time had kind of forgotten about his

197
00:10:11,710 --> 00:10:12,000
 wife,

198
00:10:12,000 --> 00:10:16,000
 he said, "Venerable sir,

199
00:10:16,000 --> 00:10:23,000
 if you compare my wife and that monkey that we saw,

200
00:10:23,000 --> 00:10:27,000
 my wife, these dove-footed nymphs are

201
00:10:27,000 --> 00:10:32,000
 as much more beautiful than my wife as my wife is more

202
00:10:32,000 --> 00:10:35,000
 beautiful than that monkey."

203
00:10:35,000 --> 00:10:40,000
 And the Buddha said, "Then I tell you what, Nanda,

204
00:10:40,000 --> 00:10:44,000
 if you stay as a monk and you try your best

205
00:10:44,000 --> 00:10:47,000
 to follow my instruction and to live the holy life,

206
00:10:47,000 --> 00:10:51,000
 I promise you these 500 nymphs will be yours,

207
00:10:51,000 --> 00:10:53,000
 will be your entourage,

208
00:10:53,000 --> 00:11:00,360
 you will be born into this state to frolic around with the

209
00:11:00,360 --> 00:11:02,000
 nymphs."

210
00:11:02,000 --> 00:11:06,300
 And Nanda, he wasn't, I guess, at this point a very deep

211
00:11:06,300 --> 00:11:08,000
 individual because

212
00:11:08,000 --> 00:11:13,230
 well, like most of us, he gets caught up with emotions or

213
00:11:13,230 --> 00:11:15,000
 with lust, no?

214
00:11:15,000 --> 00:11:20,000
 And this is because his mind is untrained.

215
00:11:20,000 --> 00:11:27,000
 And so his mind just wiped out the memory of his bride to

216
00:11:27,000 --> 00:11:27,000
 be her,

217
00:11:27,000 --> 00:11:31,000
 or his soon to be wife,

218
00:11:31,000 --> 00:11:35,000
 and was overjoyed at this time.

219
00:11:35,000 --> 00:11:39,000
 He said, "Well, in that case, I'll work really hard."

220
00:11:39,000 --> 00:11:41,680
 Because suddenly he had seen something that most human

221
00:11:41,680 --> 00:11:43,000
 beings never see,

222
00:11:43,000 --> 00:11:50,000
 this something that he thought his wife was on the top.

223
00:11:50,000 --> 00:11:52,550
 And here he's seeing something that in his mind is much

224
00:11:52,550 --> 00:11:53,000
 greater,

225
00:11:53,000 --> 00:11:57,680
 because the central attraction would have been much greater

226
00:11:57,680 --> 00:11:58,000
.

227
00:11:58,000 --> 00:12:04,000
 The perfection of these beings would have attracted him.

228
00:12:04,000 --> 00:12:08,550
 And so he became quite excited and he became much more

229
00:12:08,550 --> 00:12:09,000
 interested

230
00:12:09,000 --> 00:12:11,650
 in living the holy life because he realized that even in

231
00:12:11,650 --> 00:12:12,000
 India,

232
00:12:12,000 --> 00:12:15,000
 even before the Buddha, they believed this,

233
00:12:15,000 --> 00:12:20,000
 that through the practice of austerity or whatever,

234
00:12:20,000 --> 00:12:24,610
 you could go to heaven and enjoy great sensuality in heaven

235
00:12:24,610 --> 00:12:25,000
.

236
00:12:25,000 --> 00:12:28,000
 So he thought, "Wow, it must be true.

237
00:12:28,000 --> 00:12:30,300
 This must really be the result of practicing the Buddhist

238
00:12:30,300 --> 00:12:31,000
 teachings."

239
00:12:31,000 --> 00:12:33,000
 So he was really excited.

240
00:12:33,000 --> 00:12:36,000
 And it just shows how fickle the mind is really.

241
00:12:36,000 --> 00:12:40,070
 The whole idea of soulmates is kind of thrown out the

242
00:12:40,070 --> 00:12:41,000
 window.

243
00:12:41,000 --> 00:12:45,000
 And so from that point on, he practiced quite diligently.

244
00:12:45,000 --> 00:12:48,000
 And the Buddha kind of helped him along, as I understand,

245
00:12:48,000 --> 00:12:50,770
 because the Buddha let it be known what their agreement was

246
00:12:50,770 --> 00:12:51,000
.

247
00:12:51,000 --> 00:12:53,000
 And so all the other monks came to know that

248
00:12:53,000 --> 00:13:00,000
 Nanda is only practicing because he wants to have

249
00:13:00,000 --> 00:13:04,100
 intercourse with a bunch of sensual, a bunch of celestial n

250
00:13:04,100 --> 00:13:05,000
ymphs.

251
00:13:05,000 --> 00:13:08,000
 And so they called him a hireling from that point on.

252
00:13:08,000 --> 00:13:11,000
 They said, "Oh, Nanda, there goes Nanda the hireling.

253
00:13:11,000 --> 00:13:14,000
 Oh, hello, Nanda the hireling."

254
00:13:14,000 --> 00:13:17,000
 Because it seems that Nanda can be bought with a price

255
00:13:17,000 --> 00:13:22,000
 or can be bribed.

256
00:13:22,000 --> 00:13:25,000
 So they were scorning him and so on.

257
00:13:25,000 --> 00:13:28,000
 And so he felt quite ashamed about that.

258
00:13:28,000 --> 00:13:30,000
 But nonetheless, he worked really hard because he felt like

259
00:13:30,000 --> 00:13:33,000
 he was now both in line with the Buddha

260
00:13:33,000 --> 00:13:35,000
 and in line with his desires.

261
00:13:35,000 --> 00:13:40,000
 Because he had done what most people do when this situation

262
00:13:40,000 --> 00:13:40,000
 comes.

263
00:13:40,000 --> 00:13:44,000
 He had made a new decision that these were his soulmates.

264
00:13:44,000 --> 00:13:49,000
 Or this was his destiny to be with these ones.

265
00:13:49,000 --> 00:13:50,000
 And so he worked very hard.

266
00:13:50,000 --> 00:13:54,000
 And as a result of working very hard, slowly, slowly,

267
00:13:54,000 --> 00:13:55,000
 his mind changed.

268
00:13:55,000 --> 00:13:57,000
 His mind calmed down.

269
00:13:57,000 --> 00:13:59,000
 His mind became relaxed.

270
00:13:59,000 --> 00:14:02,000
 His mind became clear.

271
00:14:02,000 --> 00:14:06,000
 And he was able to see the arising and ceasing phenomena.

272
00:14:06,000 --> 00:14:09,000
 And he was able to follow the Buddha's teaching

273
00:14:09,000 --> 00:14:12,770
 and understand the five aggregates and able to break them

274
00:14:12,770 --> 00:14:13,000
 up.

275
00:14:13,000 --> 00:14:18,000
 And to the point that he began to lose his desires

276
00:14:18,000 --> 00:14:20,000
 and lose his lust.

277
00:14:20,000 --> 00:14:23,630
 And that he was able to see that really whether it's his

278
00:14:23,630 --> 00:14:24,000
 wife

279
00:14:24,000 --> 00:14:26,000
 or whether it's these celestial nymphs,

280
00:14:26,000 --> 00:14:29,000
 it's all just nama and group, I know.

281
00:14:29,000 --> 00:14:33,000
 It's all just experience, momentary experience that arises

282
00:14:33,000 --> 00:14:33,000
 and ceases.

283
00:14:33,000 --> 00:14:37,210
 He came to say that this is impermanent, unsatisfying and

284
00:14:37,210 --> 00:14:38,000
 uncontrollable.

285
00:14:38,000 --> 00:14:43,000
 And as a result he gave it up and became an Arahant.

286
00:14:43,000 --> 00:14:46,000
 He gained a realization and his mind let go.

287
00:14:46,000 --> 00:14:50,560
 And he entered into Nibbana and was able to become an Arah

288
00:14:50,560 --> 00:14:51,000
ant.

289
00:14:51,000 --> 00:14:53,000
 Now at the moment when he became an Arahant,

290
00:14:53,000 --> 00:14:55,000
 he went back to see the Buddha.

291
00:14:55,000 --> 00:14:57,000
 And he said to the Buddha,

292
00:14:57,000 --> 00:15:00,000
 "You know that bargain we had."

293
00:15:00,000 --> 00:15:03,000
 He wouldn't have been ashamed, but he said,

294
00:15:03,000 --> 00:15:05,000
 "That bargain that we have, Venerable Sir,

295
00:15:05,000 --> 00:15:10,000
 I release you from that bargain."

296
00:15:10,000 --> 00:15:11,000
 And the Buddha said,

297
00:15:11,000 --> 00:15:13,840
 "From the moment that you became an Arahant, I was released

298
00:15:13,840 --> 00:15:15,000
 from the bargain."

299
00:15:15,000 --> 00:15:18,000
 This is the story of Nanda.

300
00:15:18,000 --> 00:15:21,000
 And then the monks heard about,

301
00:15:21,000 --> 00:15:26,000
 or the monks as usual, they would tease him

302
00:15:26,000 --> 00:15:31,000
 and they said, "So, Nanda, still thinking about some nymphs

303
00:15:31,000 --> 00:15:31,000
,

304
00:15:31,000 --> 00:15:35,730
 or what are the nymphs for today? What's the goal for today

305
00:15:35,730 --> 00:15:36,000
?

306
00:15:36,000 --> 00:15:38,690
 Still thinking about your wife, still thinking about those

307
00:15:38,690 --> 00:15:39,000
 nymphs?"

308
00:15:39,000 --> 00:15:43,210
 And he said, "No, before my mind was untrained, so I

309
00:15:43,210 --> 00:15:44,000
 thought about them,

310
00:15:44,000 --> 00:15:48,520
 but now my mind is trained and so I don't think about them

311
00:15:48,520 --> 00:15:49,000
."

312
00:15:49,000 --> 00:15:52,000
 And they listened to this and they thought, "Listen to him

313
00:15:52,000 --> 00:15:52,000
."

314
00:15:52,000 --> 00:15:55,000
 Because this is a serious offense if you lie about

315
00:15:55,000 --> 00:15:55,000
 something like this,

316
00:15:55,000 --> 00:15:57,000
 pretending to be enlightened.

317
00:15:57,000 --> 00:15:59,000
 So they went to see the Buddha and they said,

318
00:15:59,000 --> 00:16:01,860
 "You should hear what Nanda is saying now, he's pretending

319
00:16:01,860 --> 00:16:03,000
 to be enlightened.

320
00:16:03,000 --> 00:16:06,000
 Maybe he thinks he'll get better nymphs that way."

321
00:16:06,000 --> 00:16:10,000
 And the Buddha said, "My son is correct.

322
00:16:10,000 --> 00:16:13,380
 Before, when his mind was untrained, it was like a roof, so

323
00:16:13,380 --> 00:16:16,000
 that was ill-fatched.

324
00:16:16,000 --> 00:16:20,120
 And now that his mind is trained, it is like a roof that is

325
00:16:20,120 --> 00:16:22,000
 well-fatched."

326
00:16:22,000 --> 00:16:26,630
 And so he told this verse, he said, "Just as a poorly that

327
00:16:26,630 --> 00:16:27,000
ched roof

328
00:16:27,000 --> 00:16:31,400
 is penetrated by the rain, so an untrained mind is

329
00:16:31,400 --> 00:16:34,000
 penetrated by lust."

330
00:16:34,000 --> 00:16:39,180
 I think part of what helped him, which is an interesting

331
00:16:39,180 --> 00:16:41,000
 aspect of lust,

332
00:16:41,000 --> 00:16:44,870
 would have been the realization that his mind was really

333
00:16:44,870 --> 00:16:47,000
 just playing tricks on him.

334
00:16:47,000 --> 00:16:49,000
 Because this is borne out in reality,

335
00:16:49,000 --> 00:16:52,000
 people who think that they have met their soul match,

336
00:16:52,000 --> 00:16:56,570
 or that they get the idea of self, and they think, "I like

337
00:16:56,570 --> 00:16:58,000
 this, I like that."

338
00:16:58,000 --> 00:17:03,000
 Or, "This is my preference, my wife, my lover, and so on,

339
00:17:03,000 --> 00:17:06,000
 my soulmate."

340
00:17:06,000 --> 00:17:12,000
 They can, on a heartbeat, turn and fall for something else.

341
00:17:12,000 --> 00:17:15,170
 And the realization that this is the nature of the mind,

342
00:17:15,170 --> 00:17:17,000
 that it jumps like a fire.

343
00:17:17,000 --> 00:17:23,000
 As the Buddha said, "Nati raga samaghi."

344
00:17:23,000 --> 00:17:28,310
 There is no fire like raga, like lust, because it jumps

345
00:17:28,310 --> 00:17:30,000
 from one object to another.

346
00:17:30,000 --> 00:17:34,130
 Fire, you can contain it. You can contain most fires by

347
00:17:34,130 --> 00:17:37,000
 digging pits, by digging trenches.

348
00:17:37,000 --> 00:17:41,650
 Fire won't jump to a rock, it won't jump to water, but lust

349
00:17:41,650 --> 00:17:43,000
 jumps to anything.

350
00:17:43,000 --> 00:17:46,680
 We can lust after just about anything. This is why when you

351
00:17:46,680 --> 00:17:48,000
're born in a different realm,

352
00:17:48,000 --> 00:17:50,800
 you begin to lust. If you're born a dog, you lust after

353
00:17:50,800 --> 00:17:51,000
 dogs.

354
00:17:51,000 --> 00:17:56,000
 If you're born a dang beetle, you lust after dang beetles.

355
00:17:56,000 --> 00:18:00,280
 So what is the teaching here for us as meditators? This is

356
00:18:00,280 --> 00:18:01,000
 the story part.

357
00:18:01,000 --> 00:18:05,600
 As for what this can do for us, as we come to see our minds

358
00:18:05,600 --> 00:18:07,000
 in this way,

359
00:18:07,000 --> 00:18:11,260
 we come to see our minds as having holes like the hole in a

360
00:18:11,260 --> 00:18:12,000
 roof.

361
00:18:12,000 --> 00:18:17,420
 Our minds are permeable. And so our job as meditators is to

362
00:18:17,420 --> 00:18:21,000
 patch the holes, to patch up the holes.

363
00:18:21,000 --> 00:18:25,000
 And the way we do this, by developing our minds, is that

364
00:18:25,000 --> 00:18:27,000
 the roof has to be patched,

365
00:18:27,000 --> 00:18:32,160
 has to be well covered over. So to the mind has to be well

366
00:18:32,160 --> 00:18:33,000
 sealed.

367
00:18:33,000 --> 00:18:37,600
 The way to seal the mind is in three ways, the development

368
00:18:37,600 --> 00:18:39,000
 of the mind, or the development.

369
00:18:39,000 --> 00:18:42,550
 First is the development of morality. And that's why we're

370
00:18:42,550 --> 00:18:43,000
 starting off here,

371
00:18:43,000 --> 00:18:45,230
 because at the beginning you don't have concentration, you

372
00:18:45,230 --> 00:18:47,000
 just have your morality.

373
00:18:47,000 --> 00:18:51,000
 What is your morality? You've stopped so many things.

374
00:18:51,000 --> 00:18:54,070
 You've stopped connecting with the world. And also you've

375
00:18:54,070 --> 00:18:56,000
 stopped letting your mind wander.

376
00:18:56,000 --> 00:18:58,260
 So when you see something, instead of looking at it, you're

377
00:18:58,260 --> 00:18:59,000
 saying to yourself,

378
00:18:59,000 --> 00:19:05,450
 seeing, seeing, that's morality. The narrowing your

379
00:19:05,450 --> 00:19:09,000
 activity, your mental activity,

380
00:19:09,000 --> 00:19:13,000
 because that's what's going to bring concentration.

381
00:19:13,000 --> 00:19:16,000
 Normally when you walk you're also thinking, and you have

382
00:19:16,000 --> 00:19:20,000
 no morality or no control over the mind.

383
00:19:20,000 --> 00:19:24,740
 And now you're in some way trying to control, restrict

384
00:19:24,740 --> 00:19:27,000
 yourself, morality.

385
00:19:27,000 --> 00:19:31,000
 So when you walk you say walking, walking.

386
00:19:31,000 --> 00:19:33,470
 And even more so when you're practicing meditation, you're

387
00:19:33,470 --> 00:19:36,000
 severely restricting your mind

388
00:19:36,000 --> 00:19:39,280
 just to bear awareness, when you hear a sound hearing, when

389
00:19:39,280 --> 00:19:43,000
 you see, see, when you feel pain, pain, pain.

390
00:19:43,000 --> 00:19:52,450
 Not letting your mind dwell or develop or make more

391
00:19:52,450 --> 00:19:55,000
 proliferate.

392
00:19:55,000 --> 00:19:58,860
 So that develops concentration from morality. Morality is

393
00:19:58,860 --> 00:20:00,000
 the first layer.

394
00:20:00,000 --> 00:20:04,400
 And morality is good because it prevents you from getting

395
00:20:04,400 --> 00:20:09,000
 caught up in distraction,

396
00:20:09,000 --> 00:20:14,000
 doing many things that would hurt your meditation.

397
00:20:14,000 --> 00:20:17,200
 But the next level, as you continue on and continue on, you

398
00:20:17,200 --> 00:20:19,000
'll develop concentration.

399
00:20:19,000 --> 00:20:24,950
 Concentration is the second way to patch up our minds, to

400
00:20:24,950 --> 00:20:27,000
 seal up our minds.

401
00:20:27,000 --> 00:20:30,050
 Once you develop concentration then your mind won't even

402
00:20:30,050 --> 00:20:32,000
 give rise to liking or disliking.

403
00:20:32,000 --> 00:20:36,520
 Once you're able to see the object, when you watch the

404
00:20:36,520 --> 00:20:38,000
 rising and you watch the falling

405
00:20:38,000 --> 00:20:40,200
 and you know that it's rising. At the moment when you know

406
00:20:40,200 --> 00:20:43,000
 that it's rising your mind is perfectly clear.

407
00:20:43,000 --> 00:20:49,140
 It's perfectly dry. The lusty desires can't come in. The

408
00:20:49,140 --> 00:20:50,000
 fire can't come in.

409
00:20:50,000 --> 00:20:55,880
 It can't drive you off to go and chase after celestial n

410
00:20:55,880 --> 00:20:57,000
ymphs.

411
00:20:57,000 --> 00:21:01,420
 The point being that these things are all impermanent and

412
00:21:01,420 --> 00:21:03,000
 they're all relative.

413
00:21:03,000 --> 00:21:06,500
 They don't really have the ability to satisfy. They'll

414
00:21:06,500 --> 00:21:08,000
 create pleasure for some time.

415
00:21:08,000 --> 00:21:10,420
 And even being born in heaven is not something that is

416
00:21:10,420 --> 00:21:11,000
 permanent.

417
00:21:11,000 --> 00:21:17,330
 Because it has a power and it has a force and that force

418
00:21:17,330 --> 00:21:20,000
 arises and ceases.

419
00:21:20,000 --> 00:21:23,470
 And so when you develop concentration your mind will not

420
00:21:23,470 --> 00:21:25,000
 give rise to these lusts.

421
00:21:25,000 --> 00:21:27,700
 You'll just see things as they are. Once you see things as

422
00:21:27,700 --> 00:21:30,000
 they are, wisdom will begin to arise.

423
00:21:30,000 --> 00:21:32,660
 And the question is why if concentration already stops

424
00:21:32,660 --> 00:21:34,000
 these things from arising,

425
00:21:34,000 --> 00:21:37,000
 why isn't that wisdom necessary?

426
00:21:37,000 --> 00:21:41,180
 This concentration, just like any roof, it's temporary. As

427
00:21:41,180 --> 00:21:47,000
 long as you keep patching it up, it will protect you.

428
00:21:47,000 --> 00:21:50,190
 But when you leave it alone, if you leave your roof alone,

429
00:21:50,190 --> 00:21:54,000
 eventually the elements will penetrate it.

430
00:21:54,000 --> 00:21:58,000
 The mind is the same when you stop practicing meditation.

431
00:21:58,000 --> 00:22:00,000
 When you leave it alone, all of it will come back.

432
00:22:00,000 --> 00:22:03,440
 So this is a good way of protecting the mind that many

433
00:22:03,440 --> 00:22:07,000
 people in the world use, but it's not the ultimate method

434
00:22:07,000 --> 00:22:08,000
 of protecting the mind.

435
00:22:08,000 --> 00:22:11,000
 The ultimate one is when you develop wisdom.

436
00:22:11,000 --> 00:22:13,380
 As you're looking and you're seeing things clearly, you

437
00:22:13,380 --> 00:22:16,600
 begin to penetrate the characteristics and the nature of

438
00:22:16,600 --> 00:22:19,000
 things, the nature of reality.

439
00:22:19,000 --> 00:22:25,090
 Our reality, even right here and now, sitting here, is one

440
00:22:25,090 --> 00:22:29,000
 of phenomena arising and ceasing.

441
00:22:29,000 --> 00:22:32,760
 Sometimes we're seeing, sometimes we're hearing, smelling,

442
00:22:32,760 --> 00:22:35,000
 tasting, feeling, thinking.

443
00:22:35,000 --> 00:22:40,090
 We have our emotions and all of these things arising and ce

444
00:22:40,090 --> 00:22:41,000
asing.

445
00:22:41,000 --> 00:22:43,640
 We begin to see this and we are able to break them apart

446
00:22:43,640 --> 00:22:47,000
 and see them from a taro, one by one by one.

447
00:22:47,000 --> 00:22:50,690
 And we realize that there is no, this idea of self, this

448
00:22:50,690 --> 00:22:54,870
 idea of me being able to experience pleasure or become this

449
00:22:54,870 --> 00:22:56,000
 or become that.

450
00:22:56,000 --> 00:23:00,790
 It's really just a delusion. The attraction towards some

451
00:23:00,790 --> 00:23:04,880
 being or something is just a mind state that arises and it

452
00:23:04,880 --> 00:23:07,000
 arises and it ceases.

453
00:23:07,000 --> 00:23:12,000
 You begin to see the progression of mind states.

454
00:23:12,000 --> 00:23:14,480
 When you give rise to lust, what happens? It creates

455
00:23:14,480 --> 00:23:17,300
 attachment and the attachment leads to disappointment when

456
00:23:17,300 --> 00:23:19,000
 you don't get what you want.

457
00:23:19,000 --> 00:23:22,820
 When you dislike something, it leads to anger and

458
00:23:22,820 --> 00:23:26,000
 frustration and directly suffering.

459
00:23:26,000 --> 00:23:28,560
 When you see this, you start to lose interest. You start to

460
00:23:28,560 --> 00:23:29,000
 give up.

461
00:23:29,000 --> 00:23:32,160
 You start to realize that nature doesn't admit to liking or

462
00:23:32,160 --> 00:23:36,000
 disliking. It doesn't admit to good or bad.

463
00:23:36,000 --> 00:23:39,230
 The liking and the disliking comes from creating ideas

464
00:23:39,230 --> 00:23:41,000
 about it and ideas of self.

465
00:23:41,000 --> 00:23:45,400
 That this is my experience and wanting that experience

466
00:23:45,400 --> 00:23:47,000
 again for yourself.

467
00:23:47,000 --> 00:23:51,200
 Once you just see it from what it is, your mind doesn't

468
00:23:51,200 --> 00:23:54,360
 have anything to cling to. It doesn't have any interest in

469
00:23:54,360 --> 00:23:55,000
 anything.

470
00:23:55,000 --> 00:23:58,140
 It doesn't say, "I want to be that. I want to be this." It

471
00:23:58,140 --> 00:24:00,000
's able to just experience.

472
00:24:00,000 --> 00:24:03,110
 Once you do that, once you get to that point where you see

473
00:24:03,110 --> 00:24:06,590
 things as they are, because there's nothing positive or

474
00:24:06,590 --> 00:24:09,000
 negative in anything, really.

475
00:24:09,000 --> 00:24:12,610
 You think this is good or you think this is bad. It's all a

476
00:24:12,610 --> 00:24:15,000
 concept that arises in your mind.

477
00:24:15,000 --> 00:24:18,810
 When you see through that, then it doesn't matter what

478
00:24:18,810 --> 00:24:20,000
 comes to you.

479
00:24:20,000 --> 00:24:23,870
 This is the ultimate protection because nothing can

480
00:24:23,870 --> 00:24:25,000
 penetrate.

481
00:24:25,000 --> 00:24:29,120
 The reason why we have give rise to likes and dislikes, we

482
00:24:29,120 --> 00:24:33,000
 give rise to lust in the first place, is through ignorance.

483
00:24:33,000 --> 00:24:37,770
 Ignorance is the default, not knowing. Everything in the

484
00:24:37,770 --> 00:24:39,000
 universe comes together.

485
00:24:39,000 --> 00:24:41,770
 It doesn't come together out of knowledge. It just comes

486
00:24:41,770 --> 00:24:44,000
 together and arises and develops.

487
00:24:44,000 --> 00:24:48,000
 Wisdom is something that isn't inherent in the universe.

488
00:24:48,000 --> 00:24:51,510
 Wisdom has to come afterwards. It has to come as a result

489
00:24:51,510 --> 00:24:54,000
 of all of this coming up.

490
00:24:54,000 --> 00:25:01,630
 The systematic observation and eventual understanding, once

491
00:25:01,630 --> 00:25:06,000
 you understand, then none of it can affect you.

492
00:25:06,000 --> 00:25:09,000
 Then you don't give rise to likes for this time.

493
00:25:09,000 --> 00:25:13,180
 Then your mind is perfectly well-fetched. That is the

494
00:25:13,180 --> 00:25:14,000
 development that we're trying for here.

495
00:25:14,000 --> 00:25:17,000
 We're trying to just come to understand things.

496
00:25:17,000 --> 00:25:21,310
 It's not intellectual. It's by repeated observation and

497
00:25:21,310 --> 00:25:23,000
 direct experience.

498
00:25:23,000 --> 00:25:28,000
 Once you see it again and again and again for what it is,

499
00:25:28,000 --> 00:25:30,000
 your mind will change.

500
00:25:30,000 --> 00:25:32,980
 Your way of approaching things will change. You'll have no

501
00:25:32,980 --> 00:25:35,000
 desire to chase after things.

502
00:25:35,000 --> 00:25:37,320
 You're knowing that when you chase after it, you know what

503
00:25:37,320 --> 00:25:39,000
 it leads to because you've seen that.

504
00:25:39,000 --> 00:25:41,560
 You have no desire to be angry or upset at things or

505
00:25:41,560 --> 00:25:46,120
 frustrated about things because you know what frustration

506
00:25:46,120 --> 00:25:47,000
 leads to.

507
00:25:47,000 --> 00:25:49,000
 Furthermore, you see these things for what they are.

508
00:25:49,000 --> 00:25:52,180
 You don't think of this as bad or this as good. When

509
00:25:52,180 --> 00:25:55,000
 something happens, you don't have any judgment for it.

510
00:25:55,000 --> 00:25:58,000
 You see that it happens and you let it be.

511
00:25:58,000 --> 00:26:01,260
 This is the teaching for today. This is really what we're

512
00:26:01,260 --> 00:26:02,000
 trying for.

513
00:26:02,000 --> 00:26:06,000
 We start with morality, bringing our mind in.

514
00:26:06,000 --> 00:26:09,210
 Then we develop a concentration which allows us to see

515
00:26:09,210 --> 00:26:10,000
 clearly.

516
00:26:10,000 --> 00:26:15,000
 Once we see clearly, our minds will be well-fatched.

517
00:26:15,000 --> 00:26:17,710
 Fatched so well, in fact, the word well is an

518
00:26:17,710 --> 00:26:19,000
 understatement.

519
00:26:19,000 --> 00:26:26,000
 They will be totally waterproof or lustproof in this case.

520
00:26:26,000 --> 00:26:30,040
 This is the teaching on the Dhammapada, based on verses 13

521
00:26:30,040 --> 00:26:31,000
 and 14.

522
00:26:31,000 --> 00:26:36,000
 Thanks for tuning in and back to meditation for you.

