1
00:00:00,000 --> 00:00:05,240
 Hello and welcome again to our study of the Dhammapada.

2
00:00:05,240 --> 00:00:10,800
 Today we go on to verses number 26 and 27 which read as

3
00:00:10,800 --> 00:00:12,240
 follows.

4
00:00:12,240 --> 00:00:19,000
 "Pama dam anu yun janthi, bala dum me dinho janah,

5
00:00:19,000 --> 00:00:26,000
 apama dansha me dhavi, dhanang se tangva rakhati,

6
00:00:26,000 --> 00:00:33,000
 mapa mama dam anu yun jeta, maka merati santavang,

7
00:00:33,000 --> 00:00:42,000
 apamatohi ca yantoh, papo dvi pulang sukham."

8
00:00:42,000 --> 00:00:46,000
 So two verses again on the practice of meditation.

9
00:00:46,000 --> 00:00:50,040
 The Appamada Vaga is going to be mostly about the practice

10
00:00:50,040 --> 00:00:51,000
 of meditation

11
00:00:51,000 --> 00:00:54,000
 because the word Appamada we translate as heedfulness

12
00:00:54,000 --> 00:00:57,180
 but it's really the core of what is meant by meditation or

13
00:00:57,180 --> 00:01:00,000
 meditation practice in Buddhism.

14
00:01:00,000 --> 00:01:04,000
 It's translated directly by the Buddha as mindfulness.

15
00:01:04,000 --> 00:01:08,170
 It says the meaning of the word Appamada is to be mindful

16
00:01:08,170 --> 00:01:09,000
 all the time,

17
00:01:09,000 --> 00:01:12,440
 to never be without mindfulness or without sati, without

18
00:01:12,440 --> 00:01:15,000
 remembrance or recollection.

19
00:01:15,000 --> 00:01:19,230
 So the direct meaning of these verses, "Pama dam anu yun

20
00:01:19,230 --> 00:01:20,000
 janthi,"

21
00:01:20,000 --> 00:01:28,000
 they indulge or they partake of heedlessness or negligence,

22
00:01:28,000 --> 00:01:33,000
 the fools and the unwise people.

23
00:01:33,000 --> 00:01:44,500
 But those who are wise, made of the wise, partake in Appam

24
00:01:44,500 --> 00:01:47,000
ada, partake in heedfulness

25
00:01:47,000 --> 00:01:54,280
 and guard it, "vakati dhanang satang," like a high or a

26
00:01:54,280 --> 00:01:57,000
 special treasure.

27
00:01:57,000 --> 00:02:03,760
 So the wise guard mindfulness or heedfulness as a great

28
00:02:03,760 --> 00:02:05,000
 treasure.

29
00:02:05,000 --> 00:02:10,000
 Then the Buddha says, "Mama pama dam anu yun jeta,"

30
00:02:10,000 --> 00:02:15,000
 don't engage in heedlessness, in negligence.

31
00:02:15,000 --> 00:02:21,000
 "Mama kama rati santavang," don't partake or indulge in,

32
00:02:21,000 --> 00:02:28,000
 or don't become intoxicated by sensuality.

33
00:02:28,000 --> 00:02:34,910
 Because those who, only those who are heedful and med

34
00:02:34,910 --> 00:02:36,000
itating,

35
00:02:36,000 --> 00:02:42,610
 attain great happiness or attain a profound happiness, "pap

36
00:02:42,610 --> 00:02:45,000
uti vipulang sukan,"

37
00:02:45,000 --> 00:02:48,000
 a profound happiness is attained.

38
00:02:48,000 --> 00:02:52,000
 So this is a very important teaching to understand,

39
00:02:52,000 --> 00:02:55,000
 and it's something that really cuts right to the heart of

40
00:02:55,000 --> 00:02:58,000
 our attachments in the worldly sphere.

41
00:02:58,000 --> 00:03:02,010
 Really good story, very short story, but the story goes

42
00:03:02,010 --> 00:03:04,000
 that in Sawati,

43
00:03:04,000 --> 00:03:10,560
 there was a holiday, and it kind of seems like a strange

44
00:03:10,560 --> 00:03:11,000
 holiday,

45
00:03:11,000 --> 00:03:13,000
 but there are parallels even in modern times.

46
00:03:13,000 --> 00:03:18,000
 It was a holiday where anything went, anything was allowed.

47
00:03:18,000 --> 00:03:22,990
 So no longer you had, there was no sense of respect for

48
00:03:22,990 --> 00:03:24,000
 anyone.

49
00:03:24,000 --> 00:03:28,170
 The people who partook in this holiday would abandon all

50
00:03:28,170 --> 00:03:29,000
 sense of respect

51
00:03:29,000 --> 00:03:34,000
 for mother, father, for boss, for teacher, and so on,

52
00:03:34,000 --> 00:03:37,000
 and they would go from house to house, probably drunk,

53
00:03:37,000 --> 00:03:42,000
 and abuse and revile everyone, you pig, you cow, you goat,

54
00:03:42,000 --> 00:03:45,000
 calling them all sorts of horrible names.

55
00:03:45,000 --> 00:03:48,210
 And the game was that if you wanted them to go away, you

56
00:03:48,210 --> 00:03:50,000
 had to give them stuff.

57
00:03:50,000 --> 00:03:52,400
 So it was like trick or treat, basically, like Halloween,

58
00:03:52,400 --> 00:03:54,000
 except they really did it,

59
00:03:54,000 --> 00:03:56,000
 and they were really mean and nasty.

60
00:03:56,000 --> 00:03:59,480
 They would stay there until you give them some money, or

61
00:03:59,480 --> 00:04:03,000
 some wine, or whatever, food,

62
00:04:03,000 --> 00:04:06,000
 and then they would go away.

63
00:04:06,000 --> 00:04:09,590
 And so the thing was that in Sawati, there was a great

64
00:04:09,590 --> 00:04:11,000
 proportion of people

65
00:04:11,000 --> 00:04:14,590
 who were enlightened, or Sotapan or Sakitagami, at some

66
00:04:14,590 --> 00:04:16,000
 stage of enlightenment,

67
00:04:16,000 --> 00:04:19,000
 so they obviously wouldn't partake in such a festival.

68
00:04:19,000 --> 00:04:22,000
 And so they had a horrible time for seven days,

69
00:04:22,000 --> 00:04:25,000
 and they asked the Buddha not to come to Sawati,

70
00:04:25,000 --> 00:04:26,720
 because if the Buddha went there, they would revile the

71
00:04:26,720 --> 00:04:27,000
 Buddha,

72
00:04:27,000 --> 00:04:29,000
 and that would be very bad karma for them.

73
00:04:29,000 --> 00:04:32,000
 So they said to the Buddha, "Please stay in Jetha one,

74
00:04:32,000 --> 00:04:34,510
 and we'll bring you food every day." So they brought food

75
00:04:34,510 --> 00:04:35,000
 for seven days.

76
00:04:35,000 --> 00:04:38,000
 And then after the seventh day, they came to see the Buddha

77
00:04:38,000 --> 00:04:38,000
 and said,

78
00:04:38,000 --> 00:04:41,700
 "Oh, Venerable Sir, we stayed so awful for these seven days

79
00:04:41,700 --> 00:04:42,000
.

80
00:04:42,000 --> 00:04:45,840
 It was so, so, so tiring for us to have to put up with this

81
00:04:45,840 --> 00:04:47,000
 silliness.

82
00:04:47,000 --> 00:04:49,000
 Why are people like this?

83
00:04:49,000 --> 00:04:54,000
 Why do people act in such a way?

84
00:04:54,000 --> 00:04:57,000
 Isn't it crazy that they do?" And the Buddha said, "Oh yes,

85
00:04:57,000 --> 00:05:02,000
 this is the difference between those who are enlightened

86
00:05:02,000 --> 00:05:04,000
 or those who have understood the truth,

87
00:05:04,000 --> 00:05:07,000
 and those who are still mired in delusion."

88
00:05:07,000 --> 00:05:10,000
 And so the first verse here, and then he told these verses,

89
00:05:10,000 --> 00:05:15,000
 and the first verse explains the difference between the two

90
00:05:15,000 --> 00:05:15,000
,

91
00:05:15,000 --> 00:05:19,310
 that an ordinary person, a person who has no idea about

92
00:05:19,310 --> 00:05:20,000
 what is right

93
00:05:20,000 --> 00:05:23,000
 and what is wrong and has never contemplated these things

94
00:05:23,000 --> 00:05:26,000
 or investigated what might be the truth,

95
00:05:26,000 --> 00:05:31,000
 what the things that they value are the most base

96
00:05:31,000 --> 00:05:39,120
 and useless qualities, for instance, negligence or drunken

97
00:05:39,120 --> 00:05:40,000
ness.

98
00:05:40,000 --> 00:05:43,000
 So an ordinary person and people in ordinary life

99
00:05:43,000 --> 00:05:48,000
 will actually, the things that they will cherish

100
00:05:48,000 --> 00:05:50,000
 are the times when they were the least mindful

101
00:05:50,000 --> 00:05:53,000
 and the least clear in their mind.

102
00:05:53,000 --> 00:05:56,000
 So when you think in lay life,

103
00:05:56,000 --> 00:06:00,000
 people who haven't practiced meditation,

104
00:06:00,000 --> 00:06:01,000
 the kind of things that they cherish,

105
00:06:01,000 --> 00:06:03,000
 they cherish those times when they were drunk

106
00:06:03,000 --> 00:06:05,100
 or when they did something silly and they were having a

107
00:06:05,100 --> 00:06:06,000
 great time

108
00:06:06,000 --> 00:06:09,000
 and doing all sorts of crazy things when they were young

109
00:06:09,000 --> 00:06:10,000
 and crazy

110
00:06:10,000 --> 00:06:12,000
 and how wonderful it was and so on.

111
00:06:12,000 --> 00:06:16,000
 They cherish these times when they do the craziest things.

112
00:06:16,000 --> 00:06:19,000
 And so people who, for instance, in this festival,

113
00:06:19,000 --> 00:06:21,000
 they would always think back to,

114
00:06:21,000 --> 00:06:22,770
 "Oh, wasn't it a wonderful time where we could say whatever

115
00:06:22,770 --> 00:06:23,000
 we want?

116
00:06:23,000 --> 00:06:25,000
 Didn't we have a great time then?"

117
00:06:25,000 --> 00:06:28,000
 And they make it out to be this wonderful thing.

118
00:06:28,000 --> 00:06:33,000
 And a person who has actually taken the time to see

119
00:06:33,000 --> 00:06:35,000
 the consequences of our actions

120
00:06:35,000 --> 00:06:39,000
 and the consequences of every individual mind state

121
00:06:39,000 --> 00:06:42,280
 is totally the opposite and actually cherishes as a

122
00:06:42,280 --> 00:06:43,000
 treasure

123
00:06:43,000 --> 00:06:45,420
 those times when they are cogent, those times when they are

124
00:06:45,420 --> 00:06:46,000
 aware.

125
00:06:46,000 --> 00:06:49,000
 And one of the big reasons for this, I think,

126
00:06:49,000 --> 00:06:52,000
 is that a person who has no idea or no skill

127
00:06:52,000 --> 00:06:58,000
 in understanding experience will have a very difficult time

128
00:06:58,000 --> 00:07:00,000
 just being with reality.

129
00:07:00,000 --> 00:07:02,000
 Reality is the worst thing for them,

130
00:07:02,000 --> 00:07:04,530
 the last thing they want to focus on, because it's

131
00:07:04,530 --> 00:07:05,000
 unpleasant,

132
00:07:05,000 --> 00:07:06,000
 it's difficult to deal with.

133
00:07:06,000 --> 00:07:09,000
 So pain comes up in their, in tears in their eyes,

134
00:07:09,000 --> 00:07:13,000
 and when someone's saying things to them that they don't

135
00:07:13,000 --> 00:07:13,000
 like

136
00:07:13,000 --> 00:07:16,000
 and they get angry when they have to do work

137
00:07:16,000 --> 00:07:20,450
 and then they become frustrated or bored or depressed or so

138
00:07:20,450 --> 00:07:21,000
 on,

139
00:07:21,000 --> 00:07:25,000
 the last thing they want to do is to focus on reality.

140
00:07:25,000 --> 00:07:34,000
 And so the whole goal or the point in Buddhism,

141
00:07:34,000 --> 00:07:37,000
 in the Buddha's teaching, is to come back to reality

142
00:07:37,000 --> 00:07:44,000
 and to find a way to be happy at all times in any situation

143
00:07:44,000 --> 00:07:44,000
,

144
00:07:44,000 --> 00:07:50,000
 not have our happiness depend on some specific circumstance

145
00:07:50,000 --> 00:07:56,000
 and not have it require some specific situation.

146
00:07:56,000 --> 00:07:59,000
 Because when your happiness depends on something,

147
00:07:59,000 --> 00:08:02,000
 it becomes an addiction and it becomes a partiality

148
00:08:02,000 --> 00:08:06,000
 where you aren't able to accept the rest of reality

149
00:08:06,000 --> 00:08:10,650
 because you're thinking about or wanting or striving after

150
00:08:10,650 --> 00:08:13,000
 a certain experience.

151
00:08:13,000 --> 00:08:19,770
 And the experience has to become further and more and more

152
00:08:19,770 --> 00:08:21,000
 extravagant

153
00:08:21,000 --> 00:08:23,000
 in order to satisfy the cravings,

154
00:08:23,000 --> 00:08:29,000
 because our pleasure stimulus isn't a static system.

155
00:08:29,000 --> 00:08:32,000
 When you get what you want, you want more of it.

156
00:08:32,000 --> 00:08:34,000
 When you get more of it, you want even more.

157
00:08:34,000 --> 00:08:37,000
 And it builds and builds and builds until you,

158
00:08:37,000 --> 00:08:40,030
 the only way you can be happy is to have some crazy

159
00:08:40,030 --> 00:08:41,000
 festival like this

160
00:08:41,000 --> 00:08:44,160
 where you go and do something because you're repressing

161
00:08:44,160 --> 00:08:45,000
 these desires,

162
00:08:45,000 --> 00:08:49,000
 you're repressing your greed, your anger and so on.

163
00:08:49,000 --> 00:08:55,000
 I heard about this one society, this one group of people

164
00:08:55,000 --> 00:09:02,000
 where they will engage in group sex,

165
00:09:02,000 --> 00:09:07,040
 they will get all together and have a big orgy just like

166
00:09:07,040 --> 00:09:08,000
 animals.

167
00:09:08,000 --> 00:09:12,000
 And for them, I mean this may sound coarse and rude to say,

168
00:09:12,000 --> 00:09:14,000
 but it's actually a philosophy that they have that,

169
00:09:14,000 --> 00:09:17,000
 well if this is a way to find pleasure,

170
00:09:17,000 --> 00:09:22,120
 then you should strive to indulge in it to the most extreme

171
00:09:22,120 --> 00:09:24,000
 extent possible.

172
00:09:24,000 --> 00:09:26,000
 And so it's a philosophy that they have.

173
00:09:26,000 --> 00:09:29,000
 And the idea being that as long as you can get it,

174
00:09:29,000 --> 00:09:31,720
 then what's wrong with it because you can be happy all the

175
00:09:31,720 --> 00:09:32,000
 time.

176
00:09:32,000 --> 00:09:34,000
 This is the idea.

177
00:09:34,000 --> 00:09:36,660
 So this is for people who don't see what they're doing to

178
00:09:36,660 --> 00:09:37,000
 their mind.

179
00:09:37,000 --> 00:09:39,000
 They think it's somehow static.

180
00:09:39,000 --> 00:09:41,000
 Well, if I want it any time, I can get it.

181
00:09:41,000 --> 00:09:42,850
 And then if I don't want it, I can come back and not have

182
00:09:42,850 --> 00:09:43,000
 it.

183
00:09:43,000 --> 00:09:46,000
 Without seeing the effect that it's having on your mind

184
00:09:46,000 --> 00:09:50,000
 and on the pleasure stimulus systems in the brain and so on

185
00:09:50,000 --> 00:09:50,000
,

186
00:09:50,000 --> 00:09:53,000
 and how actually you're creating an addiction

187
00:09:53,000 --> 00:09:55,000
 and you're creating an intense partiality.

188
00:09:55,000 --> 00:09:57,000
 This is an example of an extreme state.

189
00:09:57,000 --> 00:09:59,640
 There are societies where they don't allow this sort of

190
00:09:59,640 --> 00:10:00,000
 thing

191
00:10:00,000 --> 00:10:08,330
 and where you have to be totally formal and polite and

192
00:10:08,330 --> 00:10:10,000
 proper at all times.

193
00:10:10,000 --> 00:10:13,000
 Conservative, ethical and so on.

194
00:10:13,000 --> 00:10:20,000
 And any kind of showing of emotion is considered bad form.

195
00:10:20,000 --> 00:10:24,000
 But then they still have these desires

196
00:10:24,000 --> 00:10:26,000
 and they repress them and repress them and repress them.

197
00:10:26,000 --> 00:10:28,000
 And then they have these festivals.

198
00:10:28,000 --> 00:10:32,000
 So I've heard places where everything has to be proper

199
00:10:32,000 --> 00:10:36,000
 and everyone has a role and a hierarchy and so on.

200
00:10:36,000 --> 00:10:38,200
 But then they have these parties where they get drunk and

201
00:10:38,200 --> 00:10:39,000
 anything goes.

202
00:10:39,000 --> 00:10:42,800
 And like I heard about the boss getting together with

203
00:10:42,800 --> 00:10:45,000
 having crazy things.

204
00:10:45,000 --> 00:10:47,000
 So these are the things that I hear in the world.

205
00:10:47,000 --> 00:10:49,000
 And these things really do go on.

206
00:10:49,000 --> 00:10:53,660
 I mean, it's actually kind of embarrassing to talk about

207
00:10:53,660 --> 00:10:54,000
 these things.

208
00:10:54,000 --> 00:10:56,000
 But this is what's really happening in the world.

209
00:10:56,000 --> 00:11:00,000
 People are so lost that they actually think these things

210
00:11:00,000 --> 00:11:04,000
 are a cause for happiness.

211
00:11:04,000 --> 00:11:09,430
 And so there are scientists and researchers who have

212
00:11:09,430 --> 00:11:10,000
 studied this

213
00:11:10,000 --> 00:11:13,410
 and see that it really goes step by step by step until

214
00:11:13,410 --> 00:11:16,000
 finally it becomes such a perverse thing

215
00:11:16,000 --> 00:11:20,620
 where you're involving these groups or even, they were

216
00:11:20,620 --> 00:11:25,000
 saying on this one article that I read,

217
00:11:25,000 --> 00:11:29,000
 it eventually gets to with animals and so on.

218
00:11:29,000 --> 00:11:32,000
 This is where you find your pleasure.

219
00:11:32,000 --> 00:11:34,600
 And so this is the kind of thing that we would have seen in

220
00:11:34,600 --> 00:11:35,000
 Sawati.

221
00:11:35,000 --> 00:11:38,000
 Of course, it just talks about reviling people.

222
00:11:38,000 --> 00:11:43,430
 But for sure they had other festivals in regards to sens

223
00:11:43,430 --> 00:11:44,000
uality.

224
00:11:44,000 --> 00:11:47,000
 And this is where the whole Gama Sutra came up and so on.

225
00:11:47,000 --> 00:11:49,890
 And this is how people in the world think that they can

226
00:11:49,890 --> 00:11:51,000
 find happiness.

227
00:11:51,000 --> 00:11:55,000
 So there are two ways that we understand this.

228
00:11:55,000 --> 00:11:57,630
 For someone who practices meditation, it's obvious that

229
00:11:57,630 --> 00:11:59,000
 these things don't lead to happiness.

230
00:11:59,000 --> 00:12:02,060
 You engage in them, you indulge in them, and you find that

231
00:12:02,060 --> 00:12:04,000
 you're just back where you started from.

232
00:12:04,000 --> 00:12:09,510
 You're still unhappy inside. You're still unsatisfied even

233
00:12:09,510 --> 00:12:12,000
 with these things.

234
00:12:12,000 --> 00:12:17,760
 But really the idea is that we're trying to find happiness

235
00:12:17,760 --> 00:12:19,000
 here and now.

236
00:12:19,000 --> 00:12:22,210
 Even if you can say that, "Well, I may be unhappy here, but

237
00:12:22,210 --> 00:12:25,000
 when I get what I want, at that time I'm happy.

238
00:12:25,000 --> 00:12:27,300
 So what's wrong with it? I'm getting a happiness. At the

239
00:12:27,300 --> 00:12:29,000
 time when I get it, there's pleasure."

240
00:12:29,000 --> 00:12:33,860
 But the point is that the whole fact that you, the bare

241
00:12:33,860 --> 00:12:38,000
 fact that you want that is a sign that you're not happy.

242
00:12:38,000 --> 00:12:40,780
 If you are happy, you wouldn't need to strive for something

243
00:12:40,780 --> 00:12:43,000
. You wouldn't need to go for something.

244
00:12:43,000 --> 00:12:44,460
 You wouldn't need to build something. You wouldn't need to

245
00:12:44,460 --> 00:12:45,000
 create something.

246
00:12:45,000 --> 00:12:49,000
 You wouldn't need to attain something or get something.

247
00:12:49,000 --> 00:12:53,790
 So the radical task that we have is to find happiness

248
00:12:53,790 --> 00:12:57,000
 without getting that, without getting anything,

249
00:12:57,000 --> 00:13:00,980
 to find happiness here and now. And it's actually really a

250
00:13:00,980 --> 00:13:02,000
 simple thing.

251
00:13:02,000 --> 00:13:04,310
 And if you think about it, it's how it should be, that we

252
00:13:04,310 --> 00:13:06,000
 should find happiness here and now.

253
00:13:06,000 --> 00:13:09,000
 Why should our happiness depend on something over there?

254
00:13:09,000 --> 00:13:12,000
 Why can't our happiness be here and now?

255
00:13:12,000 --> 00:13:16,050
 It's a very valid question, but it's one that we never ask

256
00:13:16,050 --> 00:13:18,000
 and we always overlook.

257
00:13:18,000 --> 00:13:20,270
 We think, obviously, if you want happiness, you have to

258
00:13:20,270 --> 00:13:23,000
 strive for it. You have to go for it.

259
00:13:23,000 --> 00:13:25,580
 Well, the truth is that even to find happiness here and now

260
00:13:25,580 --> 00:13:27,000
, you have to strive for it.

261
00:13:27,000 --> 00:13:32,130
 And so this is why actually often people will find that the

262
00:13:32,130 --> 00:13:35,000
 practice of meditation is something that is uncomfortable.

263
00:13:35,000 --> 00:13:38,940
 Unpleasant, right? The point being, because you always want

264
00:13:38,940 --> 00:13:40,000
 to go out for something.

265
00:13:40,000 --> 00:13:42,560
 You want to go after that and you're trying to train

266
00:13:42,560 --> 00:13:44,000
 yourself out of that.

267
00:13:44,000 --> 00:13:47,130
 So sometimes you stop yourself and you say no and then you

268
00:13:47,130 --> 00:13:49,000
 feel unhappy.

269
00:13:49,000 --> 00:13:51,830
 Or sometimes you go after it and then you feel guilty or so

270
00:13:51,830 --> 00:13:52,000
 on.

271
00:13:52,000 --> 00:13:57,580
 Until you finally are able to change your mind and straight

272
00:13:57,580 --> 00:14:02,000
en your mind so that you're able to see the goal,

273
00:14:02,000 --> 00:14:08,790
 to see the object of your desire and to simply see it for

274
00:14:08,790 --> 00:14:10,000
 what it is.

275
00:14:10,000 --> 00:14:14,320
 And to be able to live with the perception of something

276
00:14:14,320 --> 00:14:17,000
 without having to chase after it.

277
00:14:17,000 --> 00:14:19,770
 So when you think of something that you normally would like

278
00:14:19,770 --> 00:14:21,000
, you're just thinking of it.

279
00:14:21,000 --> 00:14:25,610
 When you hear a sound that you find pleasant, you just hear

280
00:14:25,610 --> 00:14:26,000
 the sound.

281
00:14:26,000 --> 00:14:29,000
 When you see something beautiful, you just see it.

282
00:14:29,000 --> 00:14:33,000
 And you're able to experience happiness in that moment

283
00:14:33,000 --> 00:14:36,700
 without having your happiness be only when you get that

284
00:14:36,700 --> 00:14:37,000
 object.

285
00:14:37,000 --> 00:14:41,000
 When you find yourself at peace here and now.

286
00:14:41,000 --> 00:14:43,000
 But it's something that you have to strive for.

287
00:14:43,000 --> 00:14:45,000
 So we can say that there are these two strivings.

288
00:14:45,000 --> 00:14:48,360
 This is what the Buddha said, people who strive after

289
00:14:48,360 --> 00:14:52,000
 pleasure and trying to find some object of happiness.

290
00:14:52,000 --> 00:14:54,580
 And people who strive to be free from that, to find

291
00:14:54,580 --> 00:14:56,000
 happiness here and now.

292
00:14:56,000 --> 00:14:57,000
 It's really two different paths.

293
00:14:57,000 --> 00:15:00,130
 It's not that we're striving to achieve something or get

294
00:15:00,130 --> 00:15:01,000
 something.

295
00:15:01,000 --> 00:15:05,820
 We're trying to stop striving, to stop achieving, to stop

296
00:15:05,820 --> 00:15:07,000
 chasing after.

297
00:15:07,000 --> 00:15:08,000
 It's also very difficult.

298
00:15:08,000 --> 00:15:11,430
 So these are the two paths and this is what the Buddha lays

299
00:15:11,430 --> 00:15:12,000
 out here.

300
00:15:12,000 --> 00:15:16,000
 That for ordinary people they will just let themselves go.

301
00:15:16,000 --> 00:15:19,890
 Drink alcohol and take drugs and engage in all sorts of

302
00:15:19,890 --> 00:15:23,000
 foolishness, enjoying the happiness.

303
00:15:23,000 --> 00:15:26,250
 But they don't see how it's changing their minds and they

304
00:15:26,250 --> 00:15:29,000
 don't see how it's building these mindsets.

305
00:15:29,000 --> 00:15:30,000
 We're not static beings.

306
00:15:30,000 --> 00:15:33,110
 This is, another thing is, this shows how important the

307
00:15:33,110 --> 00:15:35,000
 Buddha's teaching of karma is.

308
00:15:35,000 --> 00:15:38,330
 And it goes a lot deeper than we actually think because I

309
00:15:38,330 --> 00:15:40,000
 don't have karma, you don't have karma.

310
00:15:40,000 --> 00:15:44,000
 Karma is the truth of reality. There only is karma.

311
00:15:44,000 --> 00:15:47,000
 We create ourselves out of karma.

312
00:15:47,000 --> 00:15:49,000
 The whole of our being is created out of karma.

313
00:15:49,000 --> 00:15:52,000
 The whole of our life is created out of karma.

314
00:15:52,000 --> 00:15:55,640
 There's no "I". There's only the actions, the things that

315
00:15:55,640 --> 00:15:56,000
 we do.

316
00:15:56,000 --> 00:16:00,000
 So everything that we do creates us.

317
00:16:00,000 --> 00:16:02,760
 The reason why we get angry is because we've built up anger

318
00:16:02,760 --> 00:16:03,000
.

319
00:16:03,000 --> 00:16:05,660
 The reason why we have greed and want things is because we

320
00:16:05,660 --> 00:16:07,000
 build up greed and wanting.

321
00:16:07,000 --> 00:16:09,000
 Year after year, that's all we are.

322
00:16:09,000 --> 00:16:12,600
 We're these accumulation or these conglomeration,

323
00:16:12,600 --> 00:16:16,000
 aggregation of mindstings, both positive and negative.

324
00:16:16,000 --> 00:16:19,260
 Why we have love and kindness is because we've developed

325
00:16:19,260 --> 00:16:20,000
 things.

326
00:16:20,000 --> 00:16:22,940
 Because we've come to see the truth. Because we've come to

327
00:16:22,940 --> 00:16:25,000
 see how horrible it is to suffer.

328
00:16:25,000 --> 00:16:29,000
 And so we don't want to see other beings suffer and so on.

329
00:16:29,000 --> 00:16:32,790
 We are what we do. We are according to the things that we

330
00:16:32,790 --> 00:16:36,000
 do and the qualities that we build up.

331
00:16:36,000 --> 00:16:39,000
 So it's important to understand the difference here.

332
00:16:39,000 --> 00:16:42,990
 Because I think all of us can see in our own lives and in

333
00:16:42,990 --> 00:16:46,000
 our own past this attachment to negligence,

334
00:16:46,000 --> 00:16:50,000
 this attachment to having fun and enjoying life and so on.

335
00:16:50,000 --> 00:16:53,640
 Without seeing how it's creating addiction, how it's

336
00:16:53,640 --> 00:16:56,000
 actually feeding our dissatisfaction.

337
00:16:56,000 --> 00:16:59,000
 So it's making it harder for us to just be here and now.

338
00:16:59,000 --> 00:17:02,430
 When we come back to be just here and now, we see how

339
00:17:02,430 --> 00:17:06,000
 radically opposed these two paths are.

340
00:17:06,000 --> 00:17:08,440
 Being, just being here and now is something totally

341
00:17:08,440 --> 00:17:11,000
 different and completely opposed.

342
00:17:11,000 --> 00:17:14,180
 In the sense that the more we strive to achieve happiness

343
00:17:14,180 --> 00:17:16,000
 and strive to achieve pleasure,

344
00:17:16,000 --> 00:17:19,270
 the harder it is for us to find happiness and peace here

345
00:17:19,270 --> 00:17:20,000
 and now.

346
00:17:20,000 --> 00:17:22,460
 The more we find peace and happiness here and now, the less

347
00:17:22,460 --> 00:17:25,000
 we want, the less we need of anything.

348
00:17:25,000 --> 00:17:28,160
 If you think just where we are right here, right now, when

349
00:17:28,160 --> 00:17:31,000
 we find happiness here, everything else disappears.

350
00:17:31,000 --> 00:17:35,290
 Suddenly the whole world, all of who we are, our lives, our

351
00:17:35,290 --> 00:17:39,000
 friends, our family, all of our thoughts,

352
00:17:39,000 --> 00:17:43,920
 all of our ambitions have just disappeared, vanished in

353
00:17:43,920 --> 00:17:45,000
 thin air.

354
00:17:45,000 --> 00:17:47,000
 That's the first verse.

355
00:17:47,000 --> 00:17:50,400
 The second verse is an injunction by the Buddha that once

356
00:17:50,400 --> 00:17:52,000
 you see this, if you're a wise person,

357
00:17:52,000 --> 00:17:55,000
 you should not engage in negligence.

358
00:17:55,000 --> 00:17:57,890
 Don't engage in it because it will make it harder for you

359
00:17:57,890 --> 00:17:59,000
 to find happiness.

360
00:17:59,000 --> 00:18:02,140
 It will make you less happy, less satisfied, less at peace

361
00:18:02,140 --> 00:18:03,000
 with yourself.

362
00:18:03,000 --> 00:18:10,000
 Don't get caught up in delight in sensuality.

363
00:18:10,000 --> 00:18:12,000
 This is what the Buddha says.

364
00:18:12,000 --> 00:18:17,000
 Sensuality is really the worst of them.

365
00:18:17,000 --> 00:18:19,000
 It's the most coarse of our desires.

366
00:18:19,000 --> 00:18:22,520
 We may have ambitions or so on, but our desire for sens

367
00:18:22,520 --> 00:18:26,460
uality, sights and sounds and smells and tastes and feelings

368
00:18:26,460 --> 00:18:27,000
,

369
00:18:27,000 --> 00:18:31,110
 is the worst because it involves the body and it involves

370
00:18:31,110 --> 00:18:33,000
 chemicals and hormones and so on

371
00:18:33,000 --> 00:18:36,770
 and dopamine in the brain and so on, and this addiction

372
00:18:36,770 --> 00:18:37,000
 cycle,

373
00:18:37,000 --> 00:18:43,000
 which, as I said, is actually creating what we call sankha,

374
00:18:43,000 --> 00:18:43,000
 right?

375
00:18:43,000 --> 00:18:46,000
 It's forming our reality.

376
00:18:46,000 --> 00:18:49,000
 It's changing who we are from the very core.

377
00:18:49,000 --> 00:18:51,000
 It's not us doing this.

378
00:18:51,000 --> 00:18:53,000
 This doing becomes us.

379
00:18:53,000 --> 00:18:56,000
 This is who we become.

380
00:18:56,000 --> 00:19:00,000
 And he says, "Why should you follow what I say?

381
00:19:00,000 --> 00:19:03,030
 Why should you not be negligent and why should you not

382
00:19:03,030 --> 00:19:05,000
 indulge in delight and sensuality?"

383
00:19:05,000 --> 00:19:10,540
 He says, "Because it's only the apamato, only by being heed

384
00:19:10,540 --> 00:19:14,000
ful, gianto and meditating.

385
00:19:14,000 --> 00:19:20,030
 Does one attain bapoti, vipulang sukang, the profoundest

386
00:19:20,030 --> 00:19:21,000
 happiness?"

387
00:19:21,000 --> 00:19:25,000
 So the happiness here is the happiness of peace.

388
00:19:25,000 --> 00:19:28,000
 The Buddha said, "Nati santi, vapang sukang."

389
00:19:28,000 --> 00:19:30,000
 There is no happiness without peace.

390
00:19:30,000 --> 00:19:33,150
 The happinesses that we call happiness, the pleasures in

391
00:19:33,150 --> 00:19:34,000
 the world,

392
00:19:34,000 --> 00:19:38,960
 the things that we say are our happiness or our way to find

393
00:19:38,960 --> 00:19:40,000
 true satisfaction,

394
00:19:40,000 --> 00:19:43,000
 are not real happiness because they're impermanent.

395
00:19:43,000 --> 00:19:46,000
 They change. They're dependent.

396
00:19:46,000 --> 00:19:50,000
 They require some attainment that is also impenement.

397
00:19:50,000 --> 00:19:54,000
 They require an object and they create partiality.

398
00:19:54,000 --> 00:19:57,020
 They create their opposite which is suffering and

399
00:19:57,020 --> 00:19:58,000
 dissatisfaction.

400
00:19:58,000 --> 00:20:02,680
 And so the more one indulges, the more one goes back and

401
00:20:02,680 --> 00:20:03,000
 forth

402
00:20:03,000 --> 00:20:06,000
 between one and the other, pleasure and pain.

403
00:20:06,000 --> 00:20:09,460
 And when it's never at peace, one is never able to just be

404
00:20:09,460 --> 00:20:11,000
 here and now.

405
00:20:11,000 --> 00:20:15,150
 One's life is always about seeking after this, running away

406
00:20:15,150 --> 00:20:16,000
 from that,

407
00:20:16,000 --> 00:20:19,200
 seeking, running, seeking, running, being pushed and pulled

408
00:20:19,200 --> 00:20:20,000
 back and forth.

409
00:20:20,000 --> 00:20:22,000
 And one can never just be here and now.

410
00:20:22,000 --> 00:20:29,870
 One never has the ability or the safety, the stability of

411
00:20:29,870 --> 00:20:31,000
 just being here and now.

412
00:20:31,000 --> 00:20:34,310
 They can build up some kind of pleasant state of being for

413
00:20:34,310 --> 00:20:35,000
 some time

414
00:20:35,000 --> 00:20:38,000
 and then see it washed away in the end.

415
00:20:38,000 --> 00:20:40,810
 At the very end, when they die, they will be in a state of

416
00:20:40,810 --> 00:20:41,000
 loss

417
00:20:41,000 --> 00:20:44,270
 because of their attachments, because of the things that

418
00:20:44,270 --> 00:20:46,000
 they still are clinging to.

419
00:20:46,000 --> 00:20:48,920
 So the profound happiness that the Buddha is talking about

420
00:20:48,920 --> 00:20:50,000
 is peace here and now.

421
00:20:50,000 --> 00:20:53,650
 Being able to be in here and now without any clinging or

422
00:20:53,650 --> 00:20:55,000
 any attachment.

423
00:20:55,000 --> 00:20:57,000
 So that's the teaching today.

424
00:20:57,000 --> 00:21:00,000
 That's Dhammapada verses 26 and 27.

425
00:21:00,000 --> 00:21:02,000
 Thank you for tuning in.

426
00:21:02,000 --> 00:21:04,350
 I wish you all to find peace, happiness and freedom from

427
00:21:04,350 --> 00:21:05,000
 suffering.

428
00:21:05,000 --> 00:21:07,000
 Thank you.

