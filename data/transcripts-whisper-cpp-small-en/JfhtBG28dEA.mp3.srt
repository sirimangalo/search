1
00:00:00,000 --> 00:00:06,360
 Okay, next question comes from Ubasika Sujata.

2
00:00:06,360 --> 00:00:10,340
 Can you explain briefly how is the understanding of sunyata

3
00:00:10,340 --> 00:00:14,560
 in Theravada Buddhism?

4
00:00:14,560 --> 00:00:17,360
 Sunyata means emptiness.

5
00:00:17,360 --> 00:00:23,830
 Emptiness is a word that is, I'd like to say, thrown around

6
00:00:23,830 --> 00:00:25,720
 a lot in Buddhism.

7
00:00:25,720 --> 00:00:30,570
 And I really believe that's an appropriate way of putting

8
00:00:30,570 --> 00:00:33,080
 it because a lot of our understanding

9
00:00:33,080 --> 00:00:37,800
 of sunyata is quite superficial.

10
00:00:37,800 --> 00:00:41,290
 We understand sunyata to be an experience where you'll be

11
00:00:41,290 --> 00:00:42,800
 sitting in meditation and

12
00:00:42,800 --> 00:00:47,180
 suddenly everything goes blank or it suddenly is very empty

13
00:00:47,180 --> 00:00:47,480
.

14
00:00:47,480 --> 00:00:52,090
 You feel very peaceful, very calm, there's no suffering

15
00:00:52,090 --> 00:00:53,760
 arising and so on.

16
00:00:53,760 --> 00:00:56,360
 And people say that therefore you've come to see emptiness.

17
00:00:56,360 --> 00:00:59,720
 And that's not the case, that's not true at all.

18
00:00:59,720 --> 00:01:05,860
 This feeling of emptiness, of nothingness or vastness or so

19
00:01:05,860 --> 00:01:08,840
 on is a very well-documented

20
00:01:08,840 --> 00:01:11,480
 Buddhist meditation experience.

21
00:01:11,480 --> 00:01:15,950
 And it can actually be a hindrance to true development of

22
00:01:15,950 --> 00:01:18,440
 insight because for this very

23
00:01:18,440 --> 00:01:20,990
 reason it leads you to believe that you've become

24
00:01:20,990 --> 00:01:22,960
 enlightened, that you've attained some

25
00:01:22,960 --> 00:01:27,440
 sort of a special experience and that therefore there's

26
00:01:27,440 --> 00:01:29,680
 nothing left for you to do.

27
00:01:29,680 --> 00:01:35,360
 Which of course isn't the case, it's a formed state, it has

28
00:01:35,360 --> 00:01:38,560
 causes, it's based on the concentration

29
00:01:38,560 --> 00:01:40,400
 that you gain in meditation.

30
00:01:40,400 --> 00:01:45,060
 It comes about because of the concentration and it lasts as

31
00:01:45,060 --> 00:01:47,880
 long as the power of concentration

32
00:01:47,880 --> 00:01:49,240
 is still there.

33
00:01:49,240 --> 00:01:52,310
 So you can build it, you can develop it, but it doesn't

34
00:01:52,310 --> 00:01:54,960
 lead you to a permanent and lasting

35
00:01:54,960 --> 00:01:56,120
 peace and happiness.

36
00:01:56,120 --> 00:01:58,860
 Because of course that can only be gained through

37
00:01:58,860 --> 00:02:01,160
 realization, through understanding

38
00:02:01,160 --> 00:02:06,800
 the objective reality in front of you.

39
00:02:06,800 --> 00:02:13,980
 So I would say 90% of our experience of what we term to be

40
00:02:13,980 --> 00:02:17,160
 emptiness is actually nothing

41
00:02:17,160 --> 00:02:18,160
 of the sort.

42
00:02:18,160 --> 00:02:22,880
 It's just a temporary state which is impermanent, which is

43
00:02:22,880 --> 00:02:25,880
 unsatisfying, which is not under our

44
00:02:25,880 --> 00:02:30,600
 control and does not belong to the self.

45
00:02:30,600 --> 00:02:33,810
 And actually these three characteristics are what we're

46
00:02:33,810 --> 00:02:35,760
 really trying to gain in Buddhism.

47
00:02:35,760 --> 00:02:43,280
 It's not any special state of bliss or quietude of mind or

48
00:02:43,280 --> 00:02:44,480
 so on.

49
00:02:44,480 --> 00:02:47,370
 They're trying to come to understand things as they are in

50
00:02:47,370 --> 00:02:49,000
 terms of these three things.

51
00:02:49,000 --> 00:02:51,520
 And this is where emptiness really plays a part.

52
00:02:51,520 --> 00:02:57,040
 Because impermanence, suffering or what we call the

53
00:02:57,040 --> 00:03:01,000
 unsatisfactory nature of all things,

54
00:03:01,000 --> 00:03:05,240
 that nothing can satisfy you and non-self or the fact that

55
00:03:05,240 --> 00:03:07,700
 nothing is under your control.

56
00:03:07,700 --> 00:03:12,880
 Nothing really is subject to your domination.

57
00:03:12,880 --> 00:03:19,130
 These are the three ways of becoming free from suffering,

58
00:03:19,130 --> 00:03:22,160
 of gaining real realization

59
00:03:22,160 --> 00:03:24,800
 of the truth of reality.

60
00:03:24,800 --> 00:03:28,030
 And the realization that comes from each of these three has

61
00:03:28,030 --> 00:03:29,140
 a specific term.

62
00:03:29,140 --> 00:03:32,470
 So the realization of enlightenment can come through one of

63
00:03:32,470 --> 00:03:33,320
 three ways.

64
00:03:33,320 --> 00:03:36,380
 One is through impermanence, the other is through suffering

65
00:03:36,380 --> 00:03:38,240
, the third is through non-self.

66
00:03:38,240 --> 00:03:42,830
 The gaining of realization through impermanence is called

67
00:03:42,830 --> 00:03:45,680
 animita, manga, the path through

68
00:03:45,680 --> 00:03:51,040
 seeing the signlessness, nature of phenomena, that things

69
00:03:51,040 --> 00:03:53,520
 change without warning.

70
00:03:53,520 --> 00:03:58,000
 There's no seeing that everything comes and goes without

71
00:03:58,000 --> 00:04:00,600
 any previous warning, without

72
00:04:00,600 --> 00:04:01,600
 any sign.

73
00:04:01,600 --> 00:04:03,240
 It can change in a heartbeat.

74
00:04:03,240 --> 00:04:09,410
 And we see this in our lives, our lives change very rapidly

75
00:04:09,410 --> 00:04:11,400
 in many cases.

76
00:04:11,400 --> 00:04:13,570
 You'll be living your life in a certain way and suddenly

77
00:04:13,570 --> 00:04:14,920
 everything changes, suddenly

78
00:04:14,920 --> 00:04:18,240
 you lose something in a heartbeat.

79
00:04:18,240 --> 00:04:21,630
 In meditation we see this acutely, that everything is

80
00:04:21,630 --> 00:04:24,640
 changing and things can change in a heartbeat.

81
00:04:24,640 --> 00:04:28,380
 When we see this, this can be a cause for the mind to let

82
00:04:28,380 --> 00:04:30,240
 go for realization of the

83
00:04:30,240 --> 00:04:31,240
 truth.

84
00:04:31,240 --> 00:04:33,960
 And the realization through suffering is to see that

85
00:04:33,960 --> 00:04:35,720
 nothing is worth clinging to.

86
00:04:35,720 --> 00:04:40,540
 It's called apani-hitamaga, the path through seeing the, or

87
00:04:40,540 --> 00:04:43,040
 through non-desiring, or through

88
00:04:43,040 --> 00:04:49,680
 seeing the non-desirability of phenomena.

89
00:04:49,680 --> 00:04:51,520
 When you see that things are changing all the time, you

90
00:04:51,520 --> 00:04:53,720
 start to see that they're unsatisfying.

91
00:04:53,720 --> 00:04:57,120
 So some people see this aspect of the stress involved with

92
00:04:57,120 --> 00:04:59,120
 clinging, the stress involved

93
00:04:59,120 --> 00:05:05,260
 with waiting, with expecting, with trying to chase after a

94
00:05:05,260 --> 00:05:08,000
 certain mode of existence

95
00:05:08,000 --> 00:05:12,650
 and suffering thereby because it's impermanent, it's not

96
00:05:12,650 --> 00:05:14,720
 sure, it's not stable.

97
00:05:14,720 --> 00:05:16,880
 This is the second path, the second path to realization.

98
00:05:16,880 --> 00:05:20,140
 The third path to realization is through non-self, is

99
00:05:20,140 --> 00:05:22,440
 seeing the uncontrollability of things

100
00:05:22,440 --> 00:05:26,210
 and this is termed sunyatamaga, which is the path through

101
00:05:26,210 --> 00:05:27,240
 emptiness.

102
00:05:27,240 --> 00:05:31,040
 And so the real meaning of emptiness is not a feeling of

103
00:05:31,040 --> 00:05:33,200
 everything is empty or there's

104
00:05:33,200 --> 00:05:36,030
 nothing or so much is, you know, it's a fine meditation

105
00:05:36,030 --> 00:05:37,600
 state but it's not realization

106
00:05:37,600 --> 00:05:39,080
 of the truth.

107
00:05:39,080 --> 00:05:41,420
 Emptiness is the realization that everything, every

108
00:05:41,420 --> 00:05:43,520
 experience is empty and it's empty of

109
00:05:43,520 --> 00:05:44,520
 self.

110
00:05:44,520 --> 00:05:50,560
 It's empty of being, it's not an entity in and of itself.

111
00:05:50,560 --> 00:05:53,640
 It comes and it goes, it arises and it ceases.

112
00:05:53,640 --> 00:05:58,240
 There's no solidity to it.

113
00:05:58,240 --> 00:06:02,890
 Everything is ephemeral, everything changes and does so

114
00:06:02,890 --> 00:06:05,120
 outside of the control of any

115
00:06:05,120 --> 00:06:06,640
 creator, any self.

116
00:06:06,640 --> 00:06:10,350
 So the mind is able to intervene, is able to alter the

117
00:06:10,350 --> 00:06:12,440
 course but it's not able to turn

118
00:06:12,440 --> 00:06:16,070
 things off or to make a leap from one state of being to

119
00:06:16,070 --> 00:06:19,320
 another to say, "I'm like this,"

120
00:06:19,320 --> 00:06:21,800
 and suddenly change to be like that.

121
00:06:21,800 --> 00:06:25,380
 The mind is only able to affect things in terms of its

122
00:06:25,380 --> 00:06:27,400
 judgment and in terms of its

123
00:06:27,400 --> 00:06:31,320
 desires when we want things to be a certain way.

124
00:06:31,320 --> 00:06:33,970
 There's a certain power to that that leads us in that

125
00:06:33,970 --> 00:06:34,840
 direction.

126
00:06:34,840 --> 00:06:37,680
 But as we can see in the meditation, things like pain, we

127
00:06:37,680 --> 00:06:38,980
 can't just turn it off.

128
00:06:38,980 --> 00:06:41,900
 Things like the rising and the falling of the abdomen, we

129
00:06:41,900 --> 00:06:43,320
 can't force it to be smooth

130
00:06:43,320 --> 00:06:44,320
 all the time.

131
00:06:44,320 --> 00:06:46,760
 And in fact, the more we try to force it, we see the more

132
00:06:46,760 --> 00:06:48,080
 we suffer as with the pain,

133
00:06:48,080 --> 00:06:51,260
 as with our thoughts, trying to control our minds, trying

134
00:06:51,260 --> 00:06:52,760
 to control our emotions.

135
00:06:52,760 --> 00:06:55,330
 The more we try to control things, we see the more

136
00:06:55,330 --> 00:06:56,560
 suffering we have.

137
00:06:56,560 --> 00:07:00,380
 Once you start to see things as coming and going on their

138
00:07:00,380 --> 00:07:03,120
 own, rising when they want,

139
00:07:03,120 --> 00:07:05,950
 ceasing when they want, the rising and falling of the

140
00:07:05,950 --> 00:07:07,400
 abdomen, going of its own accord, not

141
00:07:07,400 --> 00:07:11,620
 according to our wishes, this is the realization of Sunyath

142
00:07:11,620 --> 00:07:12,200
ara.

143
00:07:12,200 --> 00:07:15,000
 There really is no self involved.

144
00:07:15,000 --> 00:07:21,450
 There is no control involved that they are empty of any

145
00:07:21,450 --> 00:07:22,040
 being.

146
00:07:22,040 --> 00:07:24,040
 This is the third path.

147
00:07:24,040 --> 00:07:28,750
 This is where emptiness plays a part in the meditation

148
00:07:28,750 --> 00:07:30,040
 practice.

149
00:07:30,040 --> 00:07:33,840
 On a more theoretical level, emptiness can be used to

150
00:07:33,840 --> 00:07:36,080
 describe all three paths and in

151
00:07:36,080 --> 00:07:41,640
 fact, all aspects of reality, that all of reality is empty

152
00:07:41,640 --> 00:07:43,240
 of four things.

153
00:07:43,240 --> 00:07:49,520
 And this is the meaning of kandha, which means aggregate.

154
00:07:49,520 --> 00:07:53,760
 So aggregate meaning reality is made up of different parts.

155
00:07:53,760 --> 00:07:59,040
 There's the form aggregate, which is physical.

156
00:07:59,040 --> 00:08:01,870
 There's the feeling aggregate, which is the part of reality

157
00:08:01,870 --> 00:08:03,240
 that is the pain, happiness

158
00:08:03,240 --> 00:08:05,520
 and calm.

159
00:08:05,520 --> 00:08:09,030
 There is the recognition aggregate, the part of reality

160
00:08:09,030 --> 00:08:11,400
 that remembers things, recognizes

161
00:08:11,400 --> 00:08:13,520
 things for being this and that.

162
00:08:13,520 --> 00:08:17,260
 There is the thought part, not the thought, the judgment

163
00:08:17,260 --> 00:08:19,240
 part, what we think of things

164
00:08:19,240 --> 00:08:22,360
 when we remember something, when we recognize something,

165
00:08:22,360 --> 00:08:24,040
 how we react to it, what we think

166
00:08:24,040 --> 00:08:25,360
 of it.

167
00:08:25,360 --> 00:08:27,940
 And then there's the consciousness aggregate, which is the

168
00:08:27,940 --> 00:08:29,320
 part of reality that knows, that

169
00:08:29,320 --> 00:08:30,480
 is aware.

170
00:08:30,480 --> 00:08:34,630
 Right now that you're aware of me speaking to you, the

171
00:08:34,630 --> 00:08:37,080
 sound and the sight and so on.

172
00:08:37,080 --> 00:08:44,710
 All of these five aggregates, the meaning of the word

173
00:08:44,710 --> 00:08:49,680
 aggregate or kandha means, means

174
00:08:49,680 --> 00:08:55,600
 all of these aggregates are empty of desirability, empty of

175
00:08:55,600 --> 00:08:58,960
 permanence, empty of self and so

176
00:08:58,960 --> 00:08:59,960
 on.

177
00:08:59,960 --> 00:09:04,220
 They're empty of any, meaning every part of reality is

178
00:09:04,220 --> 00:09:06,640
 empty of any reason to cling to

179
00:09:06,640 --> 00:09:10,710
 it or any benefit from clinging to it and saying, "This is

180
00:09:10,710 --> 00:09:12,440
 going to make me happy when

181
00:09:12,440 --> 00:09:16,280
 it comes and I'm going to be happy when this goes."

182
00:09:16,280 --> 00:09:20,780
 So the dependency on things that are impermanent, unsatisf

183
00:09:20,780 --> 00:09:22,960
ying and uncontrollable.

184
00:09:22,960 --> 00:09:27,240
 That from my understanding is how emptiness plays a part in

185
00:09:27,240 --> 00:09:29,480
 the teachings of the Buddha.

186
00:09:29,480 --> 00:09:30,560
 So thanks for the question.

187
00:09:30,560 --> 00:09:31,560
 Thank you.

