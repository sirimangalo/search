1
00:00:00,000 --> 00:00:02,000
 I don't know why he made this mistake.

2
00:00:02,000 --> 00:00:04,880
 So some states should be in front.

3
00:00:04,880 --> 00:00:13,880
 Okay, then...

4
00:00:13,880 --> 00:00:19,120
 A long quotation from the Patissam Bida.

5
00:00:19,120 --> 00:00:25,560
 Now let's go to...

6
00:00:54,240 --> 00:00:29,640
 The beginning, middle, and end of the entire outbreath body

7
00:00:29,640 --> 00:00:57,280
.

8
00:00:57,280 --> 00:00:59,280
 Thus he trains.

9
00:00:59,280 --> 00:01:03,320
 Making them known, making them plain in this way.

10
00:01:03,320 --> 00:01:06,890
 He both breathes in and breathes out with consciousness

11
00:01:06,890 --> 00:01:08,960
 associated with knowledge.

12
00:01:08,960 --> 00:01:13,720
 Understanding. That is why he trains thus.

13
00:01:13,720 --> 00:01:16,720
 I shall breathe in, I shall breathe out.

14
00:01:16,720 --> 00:01:20,800
 Yes.

15
00:01:20,800 --> 00:01:23,400
 Now...

16
00:01:23,400 --> 00:01:27,900
 First, the English translation is experiencing the whole

17
00:01:27,900 --> 00:01:28,280
 body.

18
00:01:28,280 --> 00:01:33,320
 But the explanation given in the commentary is not

19
00:01:33,320 --> 00:01:36,840
 experiencing, but making clear, making known, making plain.

20
00:01:36,840 --> 00:01:39,880
 That means trying to see the breath clearly.

21
00:01:39,880 --> 00:01:45,000
 The word, the Pali word,

22
00:01:45,000 --> 00:01:48,160
 Patissam Bida, can mean to experience.

23
00:01:48,480 --> 00:01:54,640
 But here it is explained as meaning to make clear, to make

24
00:01:54,640 --> 00:01:56,280
 evident.

25
00:01:56,280 --> 00:02:06,200
 I'll come to that.

26
00:02:06,200 --> 00:02:11,000
 First, about experiencing, the word experiencing.

27
00:02:11,000 --> 00:02:14,200
 So making clear or making plain.

28
00:02:16,440 --> 00:02:18,440
 The whole breath body.

29
00:02:18,440 --> 00:02:21,680
 Now, we come to the breath body.

30
00:02:21,680 --> 00:02:24,680
 The Pali word is just Gaya.

31
00:02:24,680 --> 00:02:28,400
 Gaya means just body.

32
00:02:28,400 --> 00:02:32,960
 Now,

33
00:02:32,960 --> 00:02:35,520
 the commentary explained it as

34
00:02:35,520 --> 00:02:38,160
 In breath body or breath body.

35
00:02:38,160 --> 00:02:41,560
 Body here means not the whole body, but just the breath.

36
00:02:41,560 --> 00:02:44,520
 The breath is here called body.

37
00:02:45,120 --> 00:02:48,400
 Now, the Pali word Gaya means group.

38
00:02:48,400 --> 00:02:52,000
 Our body is a group of different parts.

39
00:02:52,000 --> 00:02:54,760
 So the breath is also

40
00:02:54,760 --> 00:02:57,840
 a group of

41
00:02:57,840 --> 00:03:00,320
 particles,

42
00:03:00,320 --> 00:03:03,000
 small particles of matter.

43
00:03:03,000 --> 00:03:05,240
 So the breath is also called Gaya.

44
00:03:05,240 --> 00:03:10,560
 So here, by the word Gaya, it means the breath body, not

45
00:03:10,560 --> 00:03:11,600
 the whole body.

46
00:03:11,600 --> 00:03:14,280
 So in

47
00:03:16,680 --> 00:03:18,680
 the commentary explained this way,

48
00:03:18,680 --> 00:03:22,520
 and the commentary, this commentary is based upon

49
00:03:22,520 --> 00:03:26,080
 the Bati Sambida Maga just mentioned.

50
00:03:26,080 --> 00:03:30,160
 So in the Bati Sambida Maga also it is explained as meaning

51
00:03:30,160 --> 00:03:34,920
 breath body, in breath and out breath.

52
00:03:34,920 --> 00:03:39,400
 But now, there are people who

53
00:03:39,400 --> 00:03:44,360
 who said it must mean the whole body.

54
00:03:45,960 --> 00:03:47,960
 And

55
00:03:47,960 --> 00:03:52,240
 they sweep the body, they look through the body or all over

56
00:03:52,240 --> 00:03:54,400
 the body for sensations.

57
00:03:54,400 --> 00:03:57,080
 Now,

58
00:03:57,080 --> 00:04:01,520
 observing these sensations in the body is not against Vip

59
00:04:01,520 --> 00:04:02,320
assana,

60
00:04:02,320 --> 00:04:05,000
 not against the teachings of the Buddha.

61
00:04:05,000 --> 00:04:08,040
 But if you see that

62
00:04:08,840 --> 00:04:11,910
 observing these sensations going all through the body is

63
00:04:11,910 --> 00:04:14,480
 according to this passage, then you are wrong.

64
00:04:14,480 --> 00:04:19,760
 Because this passage has to do with breathing meditation.

65
00:04:19,760 --> 00:04:24,440
 Breathing meditation means meditation taking breathing as

66
00:04:24,440 --> 00:04:25,320
 an object.

67
00:04:25,320 --> 00:04:29,390
 If you look for sensation, if you observe sensation, you

68
00:04:29,390 --> 00:04:31,440
 are no longer watching the breath.

69
00:04:31,440 --> 00:04:34,920
 So it is no longer a breathing meditation,

70
00:04:34,920 --> 00:04:38,280
 mindfulness of breathing meditation.

71
00:04:38,280 --> 00:04:40,280
 It becomes other kind of

72
00:04:40,280 --> 00:04:42,640
 contemplation on the body.

73
00:04:42,640 --> 00:04:45,600
 Or it may be even sensations, I mean

74
00:04:45,600 --> 00:04:48,600
 contemplation on feelings.

75
00:04:48,600 --> 00:04:53,390
 So in practice you can do the sweeping of the body if you

76
00:04:53,390 --> 00:04:55,280
 want to, if you have

77
00:04:55,280 --> 00:04:58,360
 change,

78
00:04:58,360 --> 00:05:00,680
 a sudden degree of

79
00:05:00,680 --> 00:05:06,880
 concentration, you can watch the sensations in the body.

80
00:05:06,880 --> 00:05:08,880
 It is not against Vipassana.

81
00:05:08,880 --> 00:05:12,720
 But it is not according to this passage.

82
00:05:12,720 --> 00:05:19,480
 Because this passage is shown with regard to breathing

83
00:05:19,480 --> 00:05:20,600
 meditation.

84
00:05:20,600 --> 00:05:24,440
 And this passage is more Samatha meditation.

85
00:05:24,440 --> 00:05:26,440
 Yes, it is the Samatha meditation.

86
00:05:26,440 --> 00:05:30,440
 It should be Vipassana meditation.

87
00:05:30,440 --> 00:05:36,960
 But I still, given that, I still find it hard to think of

88
00:05:36,960 --> 00:05:40,440
 breathing meditation as Samatha rather than Vipassana.

89
00:05:40,440 --> 00:05:43,040
 Because

90
00:05:43,040 --> 00:05:46,400
 the object is changing.

91
00:05:46,400 --> 00:05:48,840
 No, in breathing,

92
00:05:48,840 --> 00:05:52,990
 in Samatha meditation, breathing meditation as Samatha

93
00:05:52,990 --> 00:05:55,000
 meditation, the object doesn't change.

94
00:05:55,000 --> 00:05:57,000
 Because it's just the tip of the nose.

95
00:05:59,000 --> 00:06:02,560
 You keep the mind at the tip of the nose.

96
00:06:02,560 --> 00:06:06,440
 But the object of meditation is breath.

97
00:06:06,440 --> 00:06:09,000
 In breath and out breath.

98
00:06:09,000 --> 00:06:13,380
 So you don't pay attention to other objects. And then you

99
00:06:13,380 --> 00:06:15,480
 try to keep your mind on the object. That's why

100
00:06:15,480 --> 00:06:20,510
 next week we will find that you have to make counts in one,

101
00:06:20,510 --> 00:06:23,000
 out one, in two, out two and so on.

102
00:06:23,000 --> 00:06:26,320
 So you take the breath as object only.

103
00:06:27,240 --> 00:06:29,240
 Only the breath as object.

104
00:06:29,240 --> 00:06:31,240
 So it is Samatha meditation.

105
00:06:31,240 --> 00:06:32,520
 So,

106
00:06:32,520 --> 00:06:34,360
 so

107
00:06:34,360 --> 00:06:36,360
 to be aware of the body at all,

108
00:06:36,360 --> 00:06:40,360
 it would have to release Vipassana.

109
00:06:40,360 --> 00:06:43,200
 That's right. Yes.

110
00:06:43,200 --> 00:06:52,390
 Sorry, wouldn't you divide it at the beginning, the first

111
00:06:52,390 --> 00:06:56,320
 four just so that is the first foundation of mind?

112
00:06:56,320 --> 00:06:59,320
 Yeah, the second foundation of feeling.

113
00:06:59,320 --> 00:07:01,320
 Feeling, yeah.

114
00:07:01,320 --> 00:07:05,060
 So this has something to do with the question I made with

115
00:07:05,060 --> 00:07:06,320
 this passage.

116
00:07:06,320 --> 00:07:11,320
 When you talk about the body?

117
00:07:11,320 --> 00:07:15,290
 Yeah, when you talk about the body, it means the breath

118
00:07:15,290 --> 00:07:15,320
 body.

119
00:07:15,320 --> 00:07:18,320
 The breath, not the whole body.

120
00:07:18,320 --> 00:07:20,320
 And also,

121
00:07:20,320 --> 00:07:25,350
 here, seeing or being aware of the beginning, middle and

122
00:07:25,350 --> 00:07:27,320
 end of the breath.

123
00:07:27,320 --> 00:07:31,450
 That means keeping your mind here and when it goes in and

124
00:07:31,450 --> 00:07:32,320
 goes out,

125
00:07:32,320 --> 00:07:33,320
 plus,

126
00:07:33,320 --> 00:07:37,430
 plus this point, and you try to see this is beginning,

127
00:07:37,430 --> 00:07:41,320
 middle and beginning, middle and like that.

128
00:07:41,320 --> 00:07:46,320
 So we cannot say that this passage means the whole body.

129
00:07:46,320 --> 00:07:49,320
 And in this passage, the Kaya means the whole body.

130
00:07:50,320 --> 00:08:00,320
 [

131
00:08:00,320 --> 00:08:03,900
 Only the beginning may be clear, and to another, the middle

132
00:08:03,900 --> 00:08:06,320
 may be clear, or the end may be clear.

133
00:08:06,320 --> 00:08:11,320
 But to yet another, all stages may be clear.

134
00:08:11,320 --> 00:08:13,320
 So we should be like that person.

135
00:08:13,320 --> 00:08:17,320
 That is what is explained in paragraph 172.

136
00:08:17,320 --> 00:08:22,380
 And in 173 he says, "He trains, he strives, he endeavors in

137
00:08:22,380 --> 00:08:23,320
 this way."

138
00:08:23,320 --> 00:08:25,320
 Or else, there is a strain here,

139
00:08:25,320 --> 00:08:28,500
 in one such as this is training in the higher body, his

140
00:08:28,500 --> 00:08:29,320
 consciousness,

141
00:08:29,320 --> 00:08:32,320
 now, here, his concentration, not consciousness.

142
00:08:32,320 --> 00:08:36,320
 His concentration is training in higher consciousness.

143
00:08:36,320 --> 00:08:41,320
 And his understanding is training in higher understanding.

144
00:08:41,320 --> 00:08:45,320
 So he trains in, repeats, develops, repeatedly practices

145
00:08:45,320 --> 00:08:46,320
 these three kinds of training

146
00:08:46,320 --> 00:08:49,720
 on that object by means of that mindfulness, by means of

147
00:08:49,720 --> 00:08:50,320
 that attention.

148
00:08:50,320 --> 00:08:52,320
 That is how the meaning should be regarded here.

149
00:08:52,320 --> 00:08:58,990
 You know the three trainings, actually three trainings are

150
00:08:58,990 --> 00:09:03,320
 morality, concentration and wisdom of panya.

151
00:09:03,320 --> 00:09:04,320
 They are called three trainings.

152
00:09:04,320 --> 00:09:08,320
 Here, they are called three higher trainings.

153
00:09:08,320 --> 00:09:14,380
 They are the same as the other one, just a little bit

154
00:09:14,380 --> 00:09:15,320
 different.

155
00:09:15,320 --> 00:09:25,320
 So here, virtue, I mean, the party marker of restraint is

156
00:09:25,320 --> 00:09:27,320
 called higher virtue here,

157
00:09:27,320 --> 00:09:29,320
 training in the higher virtue.

158
00:09:29,320 --> 00:09:33,800
 And the jhanas are called the training in higher

159
00:09:33,800 --> 00:09:35,320
 consciousness.

160
00:09:35,320 --> 00:09:39,110
 And the enlightenment is called training in higher

161
00:09:39,110 --> 00:09:41,320
 understanding or wisdom.

162
00:09:42,320 --> 00:09:46,690
 So training in higher consciousness means attainment of j

163
00:09:46,690 --> 00:09:51,320
hanas and other, we call, samapadis.

164
00:09:51,320 --> 00:10:03,750
 So in this method, Buddha said, he trains, he trains and so

165
00:10:03,750 --> 00:10:04,320
 on.

166
00:10:04,320 --> 00:10:09,390
 So here, in the first part of the system, that is numbers

167
00:10:09,390 --> 00:10:10,320
 one and two,

168
00:10:10,320 --> 00:10:13,000
 he should only breathe in and breathe out and not do

169
00:10:13,000 --> 00:10:14,320
 anything else at all.

170
00:10:14,320 --> 00:10:17,400
 And it is only afterwards that he should apply himself to

171
00:10:17,400 --> 00:10:18,320
 the arousing of knowledge and so on.

172
00:10:18,320 --> 00:10:21,760
 Consequently, the present tense is used here in the first

173
00:10:21,760 --> 00:10:22,320
 text.

174
00:10:22,320 --> 00:10:26,320
 He knows I breathe in, he knows I breathe out.

175
00:10:26,320 --> 00:10:30,790
 So just he is to know that he is breathing in and breathing

176
00:10:30,790 --> 00:10:33,320
 out, breathing in and breathing out, just that.

177
00:10:33,320 --> 00:10:37,580
 But in this method, he must apply himself or he must train

178
00:10:37,580 --> 00:10:38,320
 himself.

179
00:10:39,320 --> 00:10:44,740
 He must make effort, special effort, to make clear the

180
00:10:44,740 --> 00:10:48,750
 beginning, the middle and end of each in-breath and out-b

181
00:10:48,750 --> 00:10:49,320
reath.

182
00:10:49,320 --> 00:10:52,320
 That is why here the future tense is used.

183
00:10:52,320 --> 00:10:55,320
 I shall breathe in, I shall breathe out.

184
00:10:55,320 --> 00:11:05,100
 So that means there is some effort involved here, not just

185
00:11:05,100 --> 00:11:08,320
 being mindful of in-breath and out-breath.

186
00:11:09,320 --> 00:11:14,130
 In addition to being mindful of in-breath and out-breath,

187
00:11:14,130 --> 00:11:19,750
 he has to know, to clearly see the beginning, the middle

188
00:11:19,750 --> 00:11:22,320
 and end of the breath.

189
00:11:30,320 --> 00:11:34,990
 The next is four. I shall breathe in, I shall breathe out,

190
00:11:34,990 --> 00:11:38,320
 tranquilizing the bodily formations.

191
00:11:38,320 --> 00:11:44,230
 Tranquilizing the bodily formations does not mean that you

192
00:11:44,230 --> 00:11:49,320
 must deliberately tranquilize the breath.

193
00:11:49,320 --> 00:11:54,010
 You must deliberately make the breath subtler. You cannot

194
00:11:54,010 --> 00:11:55,320
 do that actually.

195
00:11:57,320 --> 00:12:03,150
 What is meant is, just pay attention to the breath and as

196
00:12:03,150 --> 00:12:08,840
 your mind calms down, then the breath will become more and

197
00:12:08,840 --> 00:12:10,320
 more subtle.

198
00:12:10,320 --> 00:12:12,320
 That is what is meant here.

199
00:12:12,320 --> 00:12:17,790
 So you cannot make them subtle by just breathing very

200
00:12:17,790 --> 00:12:21,320
 softly or something like that.

201
00:12:21,320 --> 00:12:25,640
 But because it will come with practice, you don't have to

202
00:12:25,640 --> 00:12:26,320
 do it.

203
00:12:26,320 --> 00:12:31,720
 Now, bodily formation. The word "bodily formation" is used

204
00:12:31,720 --> 00:12:34,320
 here. And you know what that means?

205
00:12:34,320 --> 00:12:37,320
 What is bodily formation?

206
00:12:37,320 --> 00:12:39,320
 The breath.

207
00:12:39,320 --> 00:12:41,570
 The breath. Only the breath is called here "bodily

208
00:12:41,570 --> 00:12:42,320
 formation".

209
00:12:44,320 --> 00:12:51,060
 That is why it is difficult to correctly understand in some

210
00:12:51,060 --> 00:12:55,320
 places. Because the same word is used in different meaning.

211
00:12:55,320 --> 00:13:00,410
 Now, the Pali word is "Gaya Sankara". I think you are

212
00:13:00,410 --> 00:13:03,320
 familiar with the word "Sankara".

213
00:13:03,320 --> 00:13:08,320
 So, Gaya Sankara is translated as "bodily formation".

214
00:13:09,320 --> 00:13:15,390
 I don't know what "formation" means. The act of forming,

215
00:13:15,390 --> 00:13:18,320
 something which is formed, both?

216
00:13:18,320 --> 00:13:22,320
 Something that is formed.

217
00:13:22,320 --> 00:13:23,320
 Something that is formed.

218
00:13:23,320 --> 00:13:28,320
 But it could also mean the formation of a circle.

219
00:13:28,320 --> 00:13:32,320
 Yes, yes. So here it means something that is formed.

220
00:13:32,320 --> 00:13:39,840
 So, "bodily formation" means "bodily formed". That means

221
00:13:39,840 --> 00:13:45,320
 the breath is said to be caused by mind.

222
00:13:45,320 --> 00:13:49,320
 There are four causes of material properties.

223
00:13:49,320 --> 00:13:56,320
 Gamma, mind, climate and food.

224
00:13:57,320 --> 00:14:02,320
 So, mind is one of the causes of material properties.

225
00:14:02,320 --> 00:14:05,970
 Now, the breathing in and breathing out is said to be

226
00:14:05,970 --> 00:14:07,320
 caused by mind.

227
00:14:07,320 --> 00:14:11,160
 Although it is caused by mind, the breathing in and out is

228
00:14:11,160 --> 00:14:16,320
 caused by mind, it needs the physical body to arise.

229
00:14:16,320 --> 00:14:19,320
 If there is no body, there can be no breathing at all.

230
00:14:20,320 --> 00:14:25,790
 So, it is described as "formed by the body". That means

231
00:14:25,790 --> 00:14:27,320
 formed with the help of the body.

232
00:14:27,320 --> 00:14:33,010
 So, I think in the footnote, "bodily formation" the in-b

233
00:14:33,010 --> 00:14:34,320
reath and out-breath,

234
00:14:34,320 --> 00:14:38,990
 for although it is consciousness originated, although it is

235
00:14:38,990 --> 00:14:41,320
 caused by consciousness, mind,

236
00:14:41,320 --> 00:14:44,110
 it is nevertheless called "bodily formation" since its

237
00:14:44,110 --> 00:14:48,320
 existence is bound up with the not karma-bound body.

238
00:14:49,320 --> 00:14:52,530
 Bound up with the physical body, not kamma-bound, not

239
00:14:52,530 --> 00:14:54,320
 necessarily kamma-bound.

240
00:14:54,320 --> 00:15:02,320
 So, physical body and it is formed with that as the means.

241
00:15:02,320 --> 00:15:05,790
 But if there is no physical body, there can be no breath at

242
00:15:05,790 --> 00:15:06,320
 all.

243
00:15:06,320 --> 00:15:11,860
 So, it is said to be formed by, or made by, or maybe

244
00:15:11,860 --> 00:15:14,320
 conditioned by the physical body.

245
00:15:14,320 --> 00:15:16,320
 So, it is called "bodily formation".

246
00:15:18,320 --> 00:15:21,320
 Is there a body then that is not kamma-bound?

247
00:15:21,320 --> 00:15:23,320
 Pardon?

248
00:15:23,320 --> 00:15:25,320
 Is there any kind of body that is not kamma-bound?

249
00:15:25,320 --> 00:15:33,320
 There are material properties which are caused by kamma.

250
00:15:33,320 --> 00:15:38,450
 For example, the sensitivity in the eye, sensitivity in the

251
00:15:38,450 --> 00:15:40,320
 ear and so on, they are caused by kamma.

252
00:15:43,320 --> 00:15:49,070
 And voice, I mean, sound of voice, is caused by

253
00:15:49,070 --> 00:15:54,320
 consciousness of mind, as well as by some other cause.

254
00:15:54,320 --> 00:16:05,310
 When a rock hits against something, then there is noise,

255
00:16:05,310 --> 00:16:05,320
 right?

256
00:16:05,320 --> 00:16:08,320
 That is caused by, not caused by mind.

257
00:16:08,320 --> 00:16:11,320
 But when I speak, then my voice is caused by mind.

258
00:16:12,320 --> 00:16:16,320
 So, there are four causes of material properties.

259
00:16:16,320 --> 00:16:20,980
 And since breath is a material property, it must have a

260
00:16:20,980 --> 00:16:21,320
 cause.

261
00:16:21,320 --> 00:16:25,320
 According to Abhirama, it is caused by consciousness.

262
00:16:25,320 --> 00:16:27,320
 So, it is called consciousness originator.

263
00:16:27,320 --> 00:16:30,630
 Although it is caused by consciousness, it is not called

264
00:16:30,630 --> 00:16:33,320
 consciousness formation here,

265
00:16:33,320 --> 00:16:35,320
 but it is called bodily formation.

266
00:16:35,320 --> 00:16:40,010
 Because it has to depend on the body, physical body, for

267
00:16:40,010 --> 00:16:41,320
 its arising.

268
00:16:41,320 --> 00:16:44,950
 So, it is not necessarily kamma-bound body, but it is

269
00:16:44,950 --> 00:16:46,320
 physical body.

270
00:16:46,320 --> 00:17:00,560
 And so, when before we practice meditation, or if we do not

271
00:17:00,560 --> 00:17:02,320
 practice meditation,

272
00:17:02,320 --> 00:17:05,320
 our breathing is said to be gross.

273
00:17:05,320 --> 00:17:08,870
 But when we sit down and practice meditation and try to

274
00:17:08,870 --> 00:17:09,320
 discern,

275
00:17:09,320 --> 00:17:15,820
 discern the breathing, then it becomes more and more subtle

276
00:17:15,820 --> 00:17:16,320
.

277
00:17:16,320 --> 00:17:26,130
 It will become so subtle that he has to investigate whether

278
00:17:26,130 --> 00:17:28,320
 they exist or not.

279
00:17:28,320 --> 00:17:32,890
 Sometimes yogis are alarmed, afraid, because they said they

280
00:17:32,890 --> 00:17:35,320
 had stopped breathing at all.

281
00:17:35,320 --> 00:17:42,830
 And they wondered whether they are living at all, whether

282
00:17:42,830 --> 00:17:44,320
 they are alive at all.

283
00:17:44,320 --> 00:17:52,380
 So, the breathing is different from other objects of

284
00:17:52,380 --> 00:17:54,320
 meditation, like kasina,

285
00:17:54,320 --> 00:17:59,320
 kasina discs, or dead bodies, and so on.

286
00:17:59,320 --> 00:18:05,940
 Other objects become clearer with the development of

287
00:18:05,940 --> 00:18:08,320
 concentration.

288
00:18:08,320 --> 00:18:12,870
 The better your concentration becomes, the clearer these

289
00:18:12,870 --> 00:18:13,320
 objects,

290
00:18:13,320 --> 00:18:16,320
 or these images of these objects become.

291
00:18:16,320 --> 00:18:19,320
 But it is opposite with the breath.

292
00:18:19,320 --> 00:18:22,990
 The better you get concentration, the more subtle it

293
00:18:22,990 --> 00:18:23,320
 becomes,

294
00:18:23,320 --> 00:18:26,320
 and the more difficult it is to see.

295
00:18:26,320 --> 00:18:35,090
 So you have to apply effort and understanding so that you

296
00:18:35,090 --> 00:18:36,320
 do not lose it.

297
00:18:36,320 --> 00:18:41,790
 So it will become so subtle that you wonder whether you are

298
00:18:41,790 --> 00:18:42,320
 breathing at all,

299
00:18:42,320 --> 00:18:46,320
 or whether you have stopped breathing.

300
00:18:46,320 --> 00:18:52,320
 So, it will reach to such a state.

301
00:18:54,320 --> 00:18:57,740
 Now, why is that? Because previously, at the time, when he

302
00:18:57,740 --> 00:18:58,320
 has still not discerned,

303
00:18:58,320 --> 00:19:04,610
 that means when he does not practice meditation, there is

304
00:19:04,610 --> 00:19:08,320
 no concern in him, no reaction.

305
00:19:08,320 --> 00:19:11,320
 Do you see the words? paragraph 178.

306
00:19:11,320 --> 00:19:17,320
 No reaction really means no adverting, no thinking of that.

307
00:19:17,320 --> 00:19:20,500
 No attention, no reviewing to the effect that I am

308
00:19:20,500 --> 00:19:22,320
 progressively tranquilizing each,

309
00:19:22,320 --> 00:19:24,320
 each closer bodily formation.

310
00:19:24,320 --> 00:19:26,320
 But when he has discerned, there is.

311
00:19:26,320 --> 00:19:29,320
 So his bodily formation at the time when he has discerned

312
00:19:29,320 --> 00:19:30,320
 is subtle,

313
00:19:30,320 --> 00:19:35,320
 in comparison with that at the time when he has not.

314
00:19:35,320 --> 00:19:40,710
 So the breathing becomes subtle, more and more subtle, with

315
00:19:40,710 --> 00:19:42,320
 the growth of concentration.

316
00:19:42,320 --> 00:19:46,130
 And you do not have to make them subtle. You cannot make

317
00:19:46,130 --> 00:19:47,320
 them subtle at all.

318
00:19:47,320 --> 00:19:53,490
 But you just keep your mind on the breath and try to

319
00:19:53,490 --> 00:19:56,320
 discern it thoroughly,

320
00:19:56,320 --> 00:20:01,320
 to see clearly the beginning, the middle and the end of it.

321
00:20:01,320 --> 00:20:05,220
 And then when you reach the next stage, they will become

322
00:20:05,220 --> 00:20:06,320
 very subtle.

323
00:20:06,320 --> 00:20:09,610
 So you don't have to make them subtle, but they will become

324
00:20:09,610 --> 00:20:10,320
 subtle.

325
00:20:10,320 --> 00:20:13,320
 So you have to train yourself.

326
00:20:13,320 --> 00:20:17,320
 I shall breathe in, tranquilizing the bodily formation,

327
00:20:17,320 --> 00:20:21,190
 but not really tranquilizing them, but just they become

328
00:20:21,190 --> 00:20:29,320
 tranquil, they become subtle.

329
00:20:29,320 --> 00:20:43,320
 And then in paragraph 179, the relative subtlety is given.

330
00:20:43,320 --> 00:20:48,320
 In discerning, the meditation subject formation is close,

331
00:20:48,320 --> 00:20:52,320
 and it is subtle by comparison in the first journal access.

332
00:20:52,320 --> 00:21:00,430
 That means when you first practice meditation, the breath

333
00:21:00,430 --> 00:21:01,320
 is close.

334
00:21:01,320 --> 00:21:04,830
 When you reach to the excess concentration, it becomes

335
00:21:04,830 --> 00:21:06,320
 subtle.

336
00:21:06,320 --> 00:21:11,880
 And then by comparison, it is close at that stage of excess

337
00:21:11,880 --> 00:21:13,320
 concentration,

338
00:21:13,320 --> 00:21:17,320
 but it is subtle at the stage of first jhana.

339
00:21:17,320 --> 00:21:20,760
 And then in the first jhana and the second jhana, it is

340
00:21:20,760 --> 00:21:21,320
 close.

341
00:21:21,320 --> 00:21:24,320
 And in the second jhana, it is subtle and so on.

342
00:21:24,320 --> 00:21:30,320
 So grossness and subtlety are here compared with the jhana,

343
00:21:30,320 --> 00:21:37,320
 and then the neighborhood of jhana, and then next jhana.

344
00:21:37,320 --> 00:21:45,320
 So in the beginning, it is close.

345
00:21:45,320 --> 00:21:47,920
 And when you reach the neighborhood concentration, it is

346
00:21:47,920 --> 00:21:49,320
 subtle.

347
00:21:49,320 --> 00:21:53,320
 And then at the neighborhood concentration, it is close.

348
00:21:53,320 --> 00:21:55,320
 At the first jhana, it is subtle.

349
00:21:55,320 --> 00:21:57,320
 And at the first jhana, it is close.

350
00:21:57,320 --> 00:22:01,320
 It is subtle in the neighborhood of second jhana.

351
00:22:01,320 --> 00:22:04,320
 And then it is subtle in the second jhana and so on.

352
00:22:04,320 --> 00:22:12,520
 So subtleness is described with the development of

353
00:22:12,520 --> 00:22:15,320
 meditation or concentration.

354
00:22:15,320 --> 00:22:18,320
 And there are two opinions.

355
00:22:18,320 --> 00:22:23,890
 The first one is the opinion of the Diga and Samyutta recit

356
00:22:23,890 --> 00:22:24,320
ers.

357
00:22:24,320 --> 00:22:28,320
 And the second is the Majjhima reciters.

358
00:22:28,320 --> 00:22:32,320
 But just a little bit different.

359
00:22:32,320 --> 00:22:38,320
 Now, you know there are nikayas or collections,

360
00:22:38,320 --> 00:22:44,320
 collections of long sodas, collection of medium sodas,

361
00:22:44,320 --> 00:22:48,050
 and collection of, let's say, miscellaneous sodas, small,

362
00:22:48,050 --> 00:22:51,320
 small sodas, kindred sodas.

363
00:22:51,320 --> 00:23:02,650
 Now, there are monks who made a special study of one

364
00:23:02,650 --> 00:23:07,320
 collection.

365
00:23:07,320 --> 00:23:12,300
 And they have some opinion that may be different from those

366
00:23:12,300 --> 00:23:13,320
 who specialize

367
00:23:13,320 --> 00:23:16,320
 in another collection and so on.

368
00:23:16,320 --> 00:23:20,930
 So there is difference between these teachers, these recit

369
00:23:20,930 --> 00:23:21,320
ers.

370
00:23:21,320 --> 00:23:25,320
 So Diga and Samyutta reciters think this way,

371
00:23:25,320 --> 00:23:30,320
 and the Majjhima reciters think the other way.

372
00:23:30,320 --> 00:23:34,520
 But in the case of insight, that was in the case of Samyut

373
00:23:34,520 --> 00:23:35,320
ta meditation.

374
00:23:35,320 --> 00:23:39,320
 But in the insight meditation, the bodily formation

375
00:23:39,320 --> 00:23:39,320
 occurring at the time

376
00:23:39,320 --> 00:23:42,750
 of not descending is gross, and in descending the primary

377
00:23:42,750 --> 00:23:43,320
 elements,

378
00:23:43,320 --> 00:23:46,320
 it is by comparison subtle and so on.

379
00:23:46,320 --> 00:23:50,200
 So this paragraph describes the development in Vipassana

380
00:23:50,200 --> 00:23:51,320
 meditation.

381
00:23:51,320 --> 00:23:56,430
 It is gross in the preceding stage, and then subtle in

382
00:23:56,430 --> 00:24:04,320
 succeeding stage and so on.

383
00:24:04,320 --> 00:24:23,320
 And then a long quotation from the Paticam Vida Maga.

384
00:24:23,320 --> 00:24:31,420
 So actually we have come to the end of the first four

385
00:24:31,420 --> 00:24:33,320
 methods.

386
00:24:33,320 --> 00:24:36,320
 So you remember the first four methods.

387
00:24:36,320 --> 00:24:42,320
 What is one?

388
00:24:42,320 --> 00:24:48,320
 You know, you breathe in long when you breathe long,

389
00:24:48,320 --> 00:24:53,320
 and you breathe out long when you breathe out long.

390
00:24:53,320 --> 00:25:03,320
 So long breaths, short breaths, and then what?

391
00:25:03,320 --> 00:25:11,030
 Making clear the whole breath body, and then tranquilizing

392
00:25:11,030 --> 00:25:12,320
 the breath body.

393
00:25:12,320 --> 00:25:17,320
 So these four methods, these pertain to Samyutta meditation

394
00:25:17,320 --> 00:25:17,320
,

395
00:25:17,320 --> 00:25:22,320
 and so they will be explained later.

396
00:25:22,320 --> 00:25:31,320
 And the other groups of four have to do with jhanas

397
00:25:31,320 --> 00:25:36,930
 and also with the other types of foundations of mindfulness

398
00:25:36,930 --> 00:25:37,320
.

399
00:25:37,320 --> 00:25:39,320
 They will be explained later.

400
00:25:39,320 --> 00:25:47,320
 But it's still Samyutta meditation.

401
00:25:47,320 --> 00:25:55,320
 They can be Samyutta, and they can be Vipassana.

402
00:25:55,320 --> 00:25:59,320
 Depending on how you practice.

403
00:25:59,320 --> 00:26:04,320
 You practice on the breath and you get jhana,

404
00:26:04,320 --> 00:26:09,320
 and then you dwell on happiness or PT there.

405
00:26:09,320 --> 00:26:16,260
 And if you observe PT as impermanent, then you can feel Vip

406
00:26:16,260 --> 00:26:17,320
assana.

407
00:26:17,320 --> 00:26:28,320
 It will be described later.

408
00:26:28,320 --> 00:26:35,390
 Now, for the bodily formations, meaning breathing in and

409
00:26:35,390 --> 00:26:36,320
 breathing out,

410
00:26:36,320 --> 00:26:44,320
 please see paragraph 181.

411
00:26:44,320 --> 00:26:50,320
 There, the text from Pateesham Vida Amaga is quoted.

412
00:26:50,320 --> 00:26:55,320
 And in the text, "I shall breathe in, shall breathe out,

413
00:26:55,320 --> 00:26:58,850
 tranquilizing the bodily formation." What are the bodily

414
00:26:58,850 --> 00:26:59,320
 formations?

415
00:26:59,320 --> 00:27:04,510
 Long in-breaths, out-breaths, experiencing the whole body,

416
00:27:04,510 --> 00:27:06,320
 belong to the body.

417
00:27:06,320 --> 00:27:10,130
 These beings being bound up with the body are bodily

418
00:27:10,130 --> 00:27:11,320
 formations.

419
00:27:11,320 --> 00:27:15,320
 This is the explanation of the word "body reformations."

420
00:27:15,320 --> 00:27:19,460
 They are called "body reformations" because they are bound

421
00:27:19,460 --> 00:27:21,320
 up with the body.

422
00:27:21,320 --> 00:27:27,320
 They belong to the body. Without body, they cannot arise.

423
00:27:27,320 --> 00:27:47,320
 So, bodily formations mean in-breaths and out-breaths.

424
00:27:47,320 --> 00:27:53,320
 Did you read the footnotes too?

425
00:27:53,320 --> 00:27:55,320
 Some are difficult to understand.

426
00:27:55,320 --> 00:28:01,320
 Very difficult, right?

427
00:28:01,320 --> 00:28:04,320
 I cannot go through them in detail.

428
00:28:04,320 --> 00:28:11,680
 But on page 294, in the footnote, about one, two, three,

429
00:28:11,680 --> 00:28:14,320
 four, five, six, seven lines,

430
00:28:14,320 --> 00:28:19,320
 that is, "What is man is this?"

431
00:28:19,320 --> 00:28:24,320
 The contemplation of the body as an "and so on and so on."

432
00:28:24,320 --> 00:28:31,320
 It's very difficult to understand that translation.

433
00:28:31,320 --> 00:28:33,320
 "What is man is this?"

434
00:28:33,320 --> 00:28:36,880
 The contemplation of the body as "and" in-breaths and out-b

435
00:28:36,880 --> 00:28:37,320
reaths body

436
00:28:37,320 --> 00:28:41,790
 as stated "and" of the physical body, that is, its material

437
00:28:41,790 --> 00:28:42,320
 support,

438
00:28:42,320 --> 00:28:45,630
 which is not contemplation of permanence, etc., in a body

439
00:28:45,630 --> 00:28:54,900
 whose individual essence is impermanent, etc., and so on

440
00:28:54,900 --> 00:28:59,320
 and so on.

441
00:28:59,320 --> 00:29:03,320
 And maybe new translation.

442
00:29:03,320 --> 00:29:12,170
 So, if you are interested, you can ask a copy from my go-to

443
00:29:12,170 --> 00:29:12,320
-go.

444
00:29:12,320 --> 00:29:34,320
 I'll throw out some questions, too.

445
00:29:34,320 --> 00:29:37,320
 I think it's a little better.

446
00:29:37,320 --> 00:29:51,320
 It's laid out better, isn't it?

447
00:29:51,320 --> 00:29:55,320
 Nice and great out.

448
00:29:55,320 --> 00:30:09,320
 Okay, then. Next week from paragraph 186.

449
00:30:09,320 --> 00:30:13,110
 So, once again, next week will be the last week until we

450
00:30:13,110 --> 00:30:16,320
 start receiving again in July.

451
00:30:16,320 --> 00:30:21,320
 Next? I can see.

452
00:30:21,320 --> 00:30:22,320
 Oh, no.

453
00:30:22,320 --> 00:30:23,320
 Two weeks.

454
00:30:23,320 --> 00:30:25,320
 We have two more weeks.

455
00:30:25,320 --> 00:30:27,320
 Oh, yeah.

456
00:30:27,320 --> 00:30:34,320
 Two more weeks.

457
00:30:34,320 --> 00:30:39,320
 So, page 299 to 311.

458
00:30:39,320 --> 00:30:47,320
 Not many at this time.

459
00:30:47,320 --> 00:30:51,550
 So, next class is going to be the method of development of

460
00:30:51,550 --> 00:30:52,320
 the French.

461
00:30:52,320 --> 00:30:54,320
 The method of development.

462
00:30:54,320 --> 00:30:55,320
 The first one.

463
00:30:55,320 --> 00:31:00,170
 Yeah, and then the last class will be on what comment we

464
00:31:00,170 --> 00:31:04,320
 continue, third treated, detract.

465
00:31:04,320 --> 00:31:15,320
 The page 311, paragraph 231, to the end of the chapter.

466
00:31:15,320 --> 00:31:26,320
 So, two weeks more.

467
00:31:27,320 --> 00:31:28,320
 Okay.

468
00:31:30,320 --> 00:31:31,320
 Okay.

469
00:31:33,320 --> 00:31:34,320
 Okay.

470
00:31:36,320 --> 00:31:37,320
 Okay.

471
00:31:39,320 --> 00:31:40,320
 Okay.

472
00:31:42,320 --> 00:31:43,320
 Okay.

473
00:31:45,320 --> 00:31:46,320
 Okay.

474
00:31:48,320 --> 00:31:49,320
 Okay.

475
00:31:51,320 --> 00:31:52,320
 Okay.

476
00:31:54,320 --> 00:31:55,320
 Okay.

477
00:31:57,320 --> 00:31:58,320
 Okay.

478
00:32:00,320 --> 00:32:01,320
 Okay.

479
00:32:02,320 --> 00:32:03,320
 Okay.

480
00:32:03,320 --> 00:32:04,320
 Okay.

481
00:32:04,320 --> 00:32:05,320
 Okay.

482
00:32:05,320 --> 00:32:06,320
 Okay.

483
00:32:06,320 --> 00:32:07,320
 Okay.

484
00:32:07,320 --> 00:32:08,320
 Okay.

485
00:32:08,320 --> 00:32:09,320
 Okay.

486
00:32:09,320 --> 00:32:10,320
 Okay.

487
00:32:10,320 --> 00:32:11,320
 Okay.

488
00:32:11,320 --> 00:32:12,320
 Okay.

489
00:32:12,320 --> 00:32:13,320
 Okay.

490
00:32:13,320 --> 00:32:14,320
 Okay.

491
00:32:14,320 --> 00:32:15,320
 Okay.

492
00:32:15,320 --> 00:32:16,320
 Okay.

493
00:32:16,320 --> 00:32:17,320
 Okay.

494
00:32:17,320 --> 00:32:18,320
 Okay.

495
00:32:18,320 --> 00:32:19,320
 Okay.

496
00:32:19,320 --> 00:32:20,320
 Okay.

497
00:32:20,320 --> 00:32:21,320
 Okay.

498
00:32:21,320 --> 00:32:22,320
 Okay.

499
00:32:22,320 --> 00:32:23,320
 Okay.

500
00:32:23,320 --> 00:32:24,320
 Okay.

501
00:32:24,320 --> 00:32:25,320
 Okay.

