1
00:00:00,000 --> 00:00:14,070
 Good evening everyone. We're broadcasting live May 27th.

2
00:00:14,070 --> 00:00:16,040
 Today's quote is about the

3
00:00:16,040 --> 00:00:26,250
 Buddha about how radiant and clear is his complexion. Just

4
00:00:26,250 --> 00:00:28,980
 as a trinket of red

5
00:00:28,980 --> 00:00:36,130
 gold shines and glitters so too the good Godama senses are

6
00:00:36,130 --> 00:00:38,480
 calmed and his

7
00:00:38,480 --> 00:00:50,860
 complexion is clear and radiant. So in and of itself it's a

8
00:00:50,860 --> 00:00:52,440
 praise of the Buddha

9
00:00:52,440 --> 00:01:01,190
 and the most obvious benefit of this quote is to encourage

10
00:01:01,190 --> 00:01:03,520
 reverence for the

11
00:01:03,520 --> 00:01:09,250
 Buddha which is good in and of itself. It's good to revere

12
00:01:09,250 --> 00:01:11,040
 great people. It's

13
00:01:11,040 --> 00:01:14,610
 good to respect them. It's good even you know to

14
00:01:14,610 --> 00:01:16,880
 potentially bow down before the

15
00:01:16,880 --> 00:01:19,700
 Buddha which is a common thing for Buddhists to do. These

16
00:01:19,700 --> 00:01:20,720
 are good things.

17
00:01:20,720 --> 00:01:28,560
 They cultivate humility, respect, reverence, appreciation.

18
00:01:28,560 --> 00:01:32,320
 They help you take the

19
00:01:32,320 --> 00:01:37,710
 practice, the teaching seriously when you think about the

20
00:01:37,710 --> 00:01:38,720
 Buddha and how

21
00:01:38,720 --> 00:01:45,450
 wonderful he was. It's a good thing. Good for your practice

22
00:01:45,450 --> 00:01:48,080
. I think what's more

23
00:01:48,080 --> 00:01:55,720
 interesting is why. Why is the Buddha so radiant? What is

24
00:01:55,720 --> 00:01:57,560
 it about a Buddha as

25
00:01:57,560 --> 00:02:02,680
 opposed to another religious teacher that we would argue or

26
00:02:02,680 --> 00:02:04,680
 say a claim makes

27
00:02:04,680 --> 00:02:09,350
 him more radiant or what makes him special, what makes him

28
00:02:09,350 --> 00:02:10,800
 radiant more than

29
00:02:10,800 --> 00:02:14,720
 an ordinary person and that we find in the Buddha's

30
00:02:14,720 --> 00:02:15,560
 teaching and that's more

31
00:02:15,560 --> 00:02:18,060
 interesting because then it is something that we can

32
00:02:18,060 --> 00:02:21,240
 actually emulate. We emulate

33
00:02:21,240 --> 00:02:31,120
 the practices and behaviors, the mind states that need one

34
00:02:31,120 --> 00:02:36,800
 to be radiant. That's

35
00:02:36,800 --> 00:02:41,640
 of more benefit, of deeper benefit than simply praising the

36
00:02:41,640 --> 00:02:42,200
 Buddha you know

37
00:02:42,200 --> 00:02:45,340
 because of course that's quite limited. Some people that's

38
00:02:45,340 --> 00:02:47,080
 all they do is rather

39
00:02:47,080 --> 00:02:50,830
 than working to better themselves they praise people who

40
00:02:50,830 --> 00:02:52,080
 are better than themselves.

41
00:02:52,080 --> 00:02:56,390
 I mean not even in religion this is a common thing in the

42
00:02:56,390 --> 00:02:58,600
 world. People use it

43
00:02:58,600 --> 00:03:02,140
 as a defense mechanism when someone is great, when someone

44
00:03:02,140 --> 00:03:03,160
 is exceptional at

45
00:03:03,160 --> 00:03:08,330
 something. They use it as a defense mechanism to praise

46
00:03:08,330 --> 00:03:11,600
 them. It is a way of

47
00:03:11,600 --> 00:03:16,600
 avoiding actually emulating them or keeping up with them.

48
00:03:16,600 --> 00:03:17,600
 Oh you're such a

49
00:03:17,600 --> 00:03:24,120
 good person. Well why? Why that means you're not going to

50
00:03:24,120 --> 00:03:26,320
 become a good person.

51
00:03:26,320 --> 00:03:31,150
 Yeah and that's actually the thing. Oh you're so nice, so

52
00:03:31,150 --> 00:03:33,520
 wonderful. Basically

53
00:03:33,520 --> 00:03:41,200
 saying I have no intention of becoming such a nice person.

54
00:03:41,200 --> 00:03:43,160
 I find it remarkable.

55
00:03:43,160 --> 00:03:47,230
 Sometimes we maybe wish we could be like that person but I

56
00:03:47,230 --> 00:03:49,280
'm just the idea of

57
00:03:49,280 --> 00:03:53,560
 praise is limited and limiting and can be potentially

58
00:03:53,560 --> 00:03:55,400
 limiting if we rely upon

59
00:03:55,400 --> 00:03:58,650
 praise of the Buddha, we worship the Buddha, he's so great

60
00:03:58,650 --> 00:03:59,880
 and then people say

61
00:03:59,880 --> 00:04:03,710
 things like you can't possibly see nirvana yourself.

62
00:04:03,710 --> 00:04:04,640
 Nirvana is not something

63
00:04:04,640 --> 00:04:08,950
 a human being can or I had one man tell me human beings can

64
00:04:08,950 --> 00:04:12,160
't attain nirvana. He

65
00:04:12,160 --> 00:04:17,560
 said it's like no what did he say? He said oh you you

66
00:04:17,560 --> 00:04:22,680
 Westerners you

67
00:04:22,680 --> 00:04:28,440
 look at the moon and you think to go to the moon and I said

68
00:04:28,440 --> 00:04:29,520
 well we did

69
00:04:29,520 --> 00:04:46,640
 actually go to the moon. The idea is to emulate, not simply

70
00:04:46,640 --> 00:04:49,360
 revere, the Buddha,

71
00:04:49,360 --> 00:04:53,720
 emulate the enlightened ones. And the Buddha said if you

72
00:04:53,720 --> 00:04:54,640
 want to respect the

73
00:04:54,640 --> 00:04:58,070
 Buddha what's the greatest respect is to practice his

74
00:04:58,070 --> 00:04:59,680
 teachings. So what are these

75
00:04:59,680 --> 00:05:04,630
 great teachings that lead one to be radiant? The one that's

76
00:05:04,630 --> 00:05:04,840
 most

77
00:05:04,840 --> 00:05:10,110
 directly related to radiance and to this clear complexion

78
00:05:10,110 --> 00:05:11,480
 is staying in the

79
00:05:11,480 --> 00:05:15,920
 present moment. Of course that describes very much the core

80
00:05:15,920 --> 00:05:17,320
 practice of insight

81
00:05:17,320 --> 00:05:22,980
 meditation but this description of being rooted in the

82
00:05:22,980 --> 00:05:26,480
 present moment and he uses

83
00:05:26,480 --> 00:05:36,670
 language relating to plants and how they're rooted in the

84
00:05:36,670 --> 00:05:37,960
 ground. So he

85
00:05:37,960 --> 00:05:42,860
 talks about grass. When you cut grass it gets cut off from

86
00:05:42,860 --> 00:05:44,480
 its source of energy

87
00:05:44,480 --> 00:05:54,450
 and withers up and dies. And likewise this is I'm very apt

88
00:05:54,450 --> 00:05:55,480
 analogy because

89
00:05:55,480 --> 00:05:59,120
 likewise the mind that's caught up in the past or the

90
00:05:59,120 --> 00:06:01,080
 future gets cut off from

91
00:06:01,080 --> 00:06:05,520
 reality and loses its energy and it withers up,

92
00:06:05,520 --> 00:06:19,180
 dries up. So people who are who are of poor complexion in

93
00:06:19,180 --> 00:06:21,760
 sense of aged and

94
00:06:21,760 --> 00:06:27,080
 wrinkled and so on but you know even beyond normal. People

95
00:06:27,080 --> 00:06:30,400
 whose bodies become

96
00:06:30,400 --> 00:06:36,000
 sick and and and chronically unhealthy. It's for many

97
00:06:36,000 --> 00:06:39,960
 causes but one cause is

98
00:06:39,960 --> 00:06:47,870
 not being present. The Buddha was asked why how is it that

99
00:06:47,870 --> 00:06:48,840
 these monks who only

100
00:06:48,840 --> 00:06:52,220
 eat one meal a day how can they be radiant? How can they

101
00:06:52,220 --> 00:06:54,160
 look so so healthy?

102
00:06:54,160 --> 00:06:59,000
 So young if they're not eating full meals sometimes they

103
00:06:59,000 --> 00:07:01,720
 don't get much to eat how

104
00:07:01,720 --> 00:07:06,150
 can they still look so radiant? That is what the Buddha

105
00:07:06,150 --> 00:07:08,120
 said. He said it's because

106
00:07:08,120 --> 00:07:12,800
 they don't for the past they do not mourn nor for the

107
00:07:12,800 --> 00:07:14,900
 future we they take the

108
00:07:14,900 --> 00:07:20,810
 present as it comes and thus their color keep. That's from

109
00:07:20,810 --> 00:07:21,720
 the jataka.

110
00:07:21,720 --> 00:07:30,820
 So everything that we experience occurs in the present

111
00:07:30,820 --> 00:07:32,880
 moment you don't have to

112
00:07:32,880 --> 00:07:38,520
 go looking for it you just have to remind yourself that you

113
00:07:38,520 --> 00:07:40,200
're here now. When

114
00:07:40,200 --> 00:07:42,590
 you think about the past remind yourself it's just a

115
00:07:42,590 --> 00:07:44,840
 thought and it's present. You

116
00:07:44,840 --> 00:07:47,700
 worry about the future remind yourself this is worry and

117
00:07:47,700 --> 00:07:49,320
 this is thought. This

118
00:07:49,320 --> 00:07:53,100
 is planning. This is remembering. Even remembering and

119
00:07:53,100 --> 00:07:54,220
 planning they all happen

120
00:07:54,220 --> 00:07:58,240
 here and now to remind yourself otherwise you create the

121
00:07:58,240 --> 00:07:58,900
 concept of

122
00:07:58,900 --> 00:08:02,870
 future and past and you're lost. You're in the realm of

123
00:08:02,870 --> 00:08:04,700
 concepts. You're no longer in

124
00:08:04,700 --> 00:08:08,820
 the realm of reality. You create a new universe. It's not

125
00:08:08,820 --> 00:08:10,780
 here and now. It's

126
00:08:10,780 --> 00:08:18,700
 completely illusory and you can feel the difference. You

127
00:08:18,700 --> 00:08:19,440
 might argue well that's

128
00:08:19,440 --> 00:08:27,400
 useful and so on. Argue as you may. We suffer from living

129
00:08:27,400 --> 00:08:28,240
 in the past and the

130
00:08:28,240 --> 00:08:32,810
 future. You can feel it viscerally. You lose energy you're

131
00:08:32,810 --> 00:08:34,060
 weighted down you're

132
00:08:34,060 --> 00:08:43,000
 tired you get sick. All of our desires and aversions they

133
00:08:43,000 --> 00:08:44,020
 come from this realm of

134
00:08:44,020 --> 00:08:50,440
 concepts. Reality is not clingable. If you're living in

135
00:08:50,440 --> 00:08:53,120
 reality there's no

136
00:08:53,120 --> 00:09:05,380
 there's no quality to it that is appealing or displeasing.

137
00:09:14,500 --> 00:09:22,560
 So a little bit of so we live in the present moment. This

138
00:09:22,560 --> 00:09:23,500
 is what we try to do

139
00:09:23,500 --> 00:09:27,070
 when we meditate. We try to just be here now plain and

140
00:09:27,070 --> 00:09:28,780
 simple. Not judge it not

141
00:09:28,780 --> 00:09:34,860
 react to it. It's not easy. So it's building a new habit.

142
00:09:34,860 --> 00:09:37,180
 So it takes training.

143
00:09:37,180 --> 00:09:43,620
 It takes practice. But that's what we do and then we shine

144
00:09:43,620 --> 00:09:45,060
 as well and become

145
00:09:45,060 --> 00:09:55,120
 more radiant. So a little bit of dumdah for today.

146
00:09:55,120 --> 00:09:58,260
 Everybody's waving at me.

147
00:09:59,140 --> 00:10:05,910
 Okay we got a couple of questions here. Do you have to see

148
00:10:05,910 --> 00:10:07,900
 impermanent suffering

149
00:10:07,900 --> 00:10:10,740
 a non-self moment to moment or does simply knowing and

150
00:10:10,740 --> 00:10:11,420
 understanding

151
00:10:11,420 --> 00:10:16,640
 impermanent suffering a non-self moment to moment satisfy

152
00:10:16,640 --> 00:10:17,860
 the practice.

153
00:10:20,340 --> 00:10:27,570
 Knowing and seeing are the same. Sounds like you're maybe

154
00:10:27,570 --> 00:10:29,260
 referring to

155
00:10:29,260 --> 00:10:34,460
 intellectual knowledge theoretical knowledge like hey I was

156
00:10:34,460 --> 00:10:36,260
 I was happy

157
00:10:36,260 --> 00:10:39,600
 yesterday and I'm not happy today that's impermanent or

158
00:10:39,600 --> 00:10:40,860
 something like that.

159
00:10:40,860 --> 00:10:45,740
 Knowing and seeing or knowing comes from seeing. When you

160
00:10:45,740 --> 00:10:48,760
 see you know. You know

161
00:10:48,760 --> 00:10:53,360
 because you see and it's not something you have to do it's

162
00:10:53,360 --> 00:10:53,860
 something that

163
00:10:53,860 --> 00:10:59,940
 happens. What you have to do is be present and be only

164
00:10:59,940 --> 00:11:00,740
 present and you'll

165
00:11:00,740 --> 00:11:03,900
 see things are rising and ceasing. They're impermanent

166
00:11:03,900 --> 00:11:04,740
 means they're not

167
00:11:04,740 --> 00:11:10,640
 stable. They're dukkha which means they're not satisfying.

168
00:11:10,640 --> 00:11:11,100
 Because they're

169
00:11:11,100 --> 00:11:14,660
 unstable they can't you can't claim that that's a cause of

170
00:11:14,660 --> 00:11:16,660
 happiness. Like that's

171
00:11:16,660 --> 00:11:21,250
 going to satisfy you and you can't control them. They don't

172
00:11:21,250 --> 00:11:21,980
 belong to you.

173
00:11:21,980 --> 00:11:25,780
 They don't have any entity of their own. They're ephemeral.

174
00:11:25,780 --> 00:11:26,780
 They arise and they

175
00:11:26,780 --> 00:11:33,960
 cease. Once you understand and know suffering impermanence

176
00:11:33,960 --> 00:11:34,820
 and non-self what

177
00:11:34,820 --> 00:11:38,420
 is the next part of the practice meditation. It only takes

178
00:11:38,420 --> 00:11:39,520
 a moment. If you

179
00:11:39,520 --> 00:11:44,320
 have one moment of pure understanding of impermanence or

180
00:11:44,320 --> 00:11:45,860
 suffering or non-self

181
00:11:45,860 --> 00:11:50,060
 one or the other you'll see it clearly. So clearly the next

182
00:11:50,060 --> 00:11:51,740
 moment is the

183
00:11:51,740 --> 00:11:58,460
 cessation. Nivana. So there isn't really a next step. The

184
00:11:58,460 --> 00:12:01,900
 next step happens by itself.

185
00:12:01,900 --> 00:12:08,840
 Is it natural for fear to arise at times one sees non-

186
00:12:08,840 --> 00:12:10,540
control?

187
00:12:13,460 --> 00:12:19,120
 Absolutely yes. It's called bhayanyana. Not exactly. Bhay

188
00:12:19,120 --> 00:12:21,540
anyana is you see the

189
00:12:21,540 --> 00:12:25,290
 fierceness. The fear that comes from it is problem. Fear is

190
00:12:25,290 --> 00:12:26,820
 based on anger.

191
00:12:26,820 --> 00:12:32,900
 Dosa. You have to acknowledge it. Afraid afraid. But it's

192
00:12:32,900 --> 00:12:34,260
 of course common once

193
00:12:34,260 --> 00:12:38,730
 you see how how terrifying it is. How unstable everything

194
00:12:38,730 --> 00:12:39,740
 is. It's like having

195
00:12:39,740 --> 00:12:44,060
 the rug pulled out from under you're having your whole

196
00:12:44,060 --> 00:12:45,620
 foundation shook.

197
00:12:45,620 --> 00:12:49,680
 Because we think of our lives as I can control this and

198
00:12:49,680 --> 00:12:51,740
 then we realize I can't

199
00:12:51,740 --> 00:12:56,280
 control it. That's scary. But the knowledge isn't afraid.

200
00:12:56,280 --> 00:12:57,020
 It's not it's not

201
00:12:57,020 --> 00:13:00,400
 important to be afraid. It's not good to be afraid. It's

202
00:13:00,400 --> 00:13:02,060
 just a reaction that we

203
00:13:02,060 --> 00:13:10,780
 have to work on and overcome. When an unenlightened person

204
00:13:10,780 --> 00:13:11,940
 notes seeing as

205
00:13:11,940 --> 00:13:15,910
 seeing hearing is hearing is it still karma? Yes. It's kus

206
00:13:15,910 --> 00:13:17,980
ala karma. Kusala

207
00:13:17,980 --> 00:13:23,340
 kamba. It's jnana sampayuta as well. So it's associated

208
00:13:23,340 --> 00:13:26,780
 with knowledge. That's

209
00:13:26,780 --> 00:13:31,100
 karma and the best of the best sort.

210
00:13:31,100 --> 00:13:39,920
 Karma that gets you closer to seeing the truth and letting

211
00:13:39,920 --> 00:13:41,580
 go. Realizing nirvana.

212
00:13:41,580 --> 00:13:48,740
 What is the role of prayer in spiritual practice? If you're

213
00:13:48,740 --> 00:13:49,700
 praying to another

214
00:13:49,700 --> 00:13:53,930
 being that's delusion. That's what role it has. But if you

215
00:13:53,930 --> 00:13:54,900
're making a

216
00:13:54,900 --> 00:13:57,660
 determination for something may this happen, may that

217
00:13:57,660 --> 00:13:59,820
 happen. That's a good way

218
00:13:59,820 --> 00:14:04,700
 of setting yourself. And it's also it actually changes the

219
00:14:04,700 --> 00:14:06,260
 universe. It changes

220
00:14:06,260 --> 00:14:10,980
 your mind. It changes your surroundings. If you have a very

221
00:14:10,980 --> 00:14:11,940
 strong mind

222
00:14:11,940 --> 00:14:15,990
 you can actually change things with your determination. May

223
00:14:15,990 --> 00:14:17,620
 it be thus, may it be

224
00:14:17,620 --> 00:14:21,430
 thus. May it not be this. You can actually affect the

225
00:14:21,430 --> 00:14:23,980
 universe. This is how some

226
00:14:23,980 --> 00:14:28,830
 meditators, some meditation practices they actually

227
00:14:28,830 --> 00:14:30,180
 practice to manifest things

228
00:14:30,180 --> 00:14:34,180
 like may I be rich, may I be this or that. Of course we don

229
00:14:34,180 --> 00:14:36,220
't do that but they seem

230
00:14:36,220 --> 00:14:40,490
 to have some results in getting what they want through the

231
00:14:40,490 --> 00:14:41,140
 power of the mind,

232
00:14:41,140 --> 00:14:44,750
 the power of determination. So making a wish, may I be

233
00:14:44,750 --> 00:14:46,060
 happy, may other beings be

234
00:14:46,060 --> 00:14:49,430
 happy, my parents be happy. It can really affect them. It

235
00:14:49,430 --> 00:14:51,100
 can change reality.

236
00:14:51,100 --> 00:14:54,200
 You have a strong enough determination. So we call it

237
00:14:54,200 --> 00:15:02,100
 determination at itana. If

238
00:15:02,100 --> 00:15:04,800
 you hear a voice clearly is it important to note all the

239
00:15:04,800 --> 00:15:05,820
 change in the voice

240
00:15:05,820 --> 00:15:12,020
 happening in that particular moment? No, just the hearing,

241
00:15:12,020 --> 00:15:14,300
 hearing. You don't have to

242
00:15:14,300 --> 00:15:22,100
 worry about the particulars. You'll see all the particulars

243
00:15:22,100 --> 00:15:23,500
 anyway but what you

244
00:15:23,500 --> 00:15:26,060
 have to remind yourself is that that's just hearing. It's

245
00:15:26,060 --> 00:15:27,620
 nothing else. It's not

246
00:15:27,620 --> 00:15:31,900
 good, it's not bad, it's not me, it's not mine.

247
00:15:31,900 --> 00:15:52,660
 A good number of people tonight, most of you are green

248
00:15:52,660 --> 00:15:55,020
 which is good. Green means

249
00:15:55,020 --> 00:16:02,540
 you've meditated recently. Appreciate that.

250
00:16:02,540 --> 00:16:31,720
 You're welcome. Okay. No other questions? We will stop

251
00:16:31,720 --> 00:16:31,900
 there.

252
00:16:33,580 --> 00:16:35,990
 Tomorrow I'm not, I don't think I'll be broadcasting

253
00:16:35,990 --> 00:16:38,180
 tomorrow. It's a long day

254
00:16:38,180 --> 00:16:42,130
 tomorrow with this big thing in Mississauga. So I'll just

255
00:16:42,130 --> 00:16:43,180
 take a break.

256
00:16:43,180 --> 00:16:48,940
 We'll see. I don't think so. Have a good night everyone.

257
00:16:48,940 --> 00:16:54,940
 [Silence]

