 [BLANK_AUDIO]
 [SOUND]
 [BLANK_AUDIO]
 Right, so now we are, I think it's all working.
 So we've got a Google Hangout up as well,
 if people want to come on and ask questions.
 [BLANK_AUDIO]
 Hey, if you just wanna watch,
 you can watch from my YouTube channel, I think.
 [BLANK_AUDIO]
 So today's quote is about mindfulness of breathing.
 [BLANK_AUDIO]
 And the Buddha praised mindfulness of breathing time and
 time again.
 [BLANK_AUDIO]
 It's considered to be the gold standard in meditation
 practices in Buddhism.
 [BLANK_AUDIO]
 And so there's a lot written about it.
 There's a lot of debate about it, discussion about it.
 [BLANK_AUDIO]
 People have their own ideas about it.
 [BLANK_AUDIO]
 Lots and lots of different ideas about it.
 [BLANK_AUDIO]
 Some of which include the fact that it has to be done at
 the nose.
 I've had a monk tell me, I explained to him that I do
 mindfulness of breathing,
 watching the stomach, or I do a meditation watching the
 stomach rising and falling.
 And he looked very serious and concerned.
 And he said, through a translator actually, it was kind of
 interesting.
 He told his translator to tell him that he has to be
 careful with that,
 because it's not the Buddha's teaching.
 [BLANK_AUDIO]
 And he had to be at the nose.
 [BLANK_AUDIO]
 In Thailand, they've got lots of interesting ideas about
 mindfulness of breathing.
 They mix it with mindfulness of the Buddha usually.
 So when the breath goes in, they say budd.
 When it goes out, they say dho, buddho, buddho.
 And as a result, they start to get the idea that the breath
 or
 the awareness of the breath is somehow the Buddha.
 In the sense of Buddha meaning one who knows.
 So if you read some of these books, they talk about that
 being Buddha,
 that is Buddha, that knowing mind.
 Somehow that that's equivalent to Nibbana or something, it
's ridiculous.
 [BLANK_AUDIO]
 But the Buddha had specific teachings on Anapanasati.
 Some people say it's only used for Jhana.
 Other people say that you can use it for Vipassana.
 Some people say, Buddhaghosa apparently said that it's,
 well,
 he kind of insinuates that it's only for the Buddha and
 highly special individuals.
 That it's very difficult to practice Anapanasati.
 He may be right, it may be quite difficult to become,
 his argument is that it becomes more and more refined as
 you practice it.
 But he was talking about a specific type of Anapanasati
 that's used to enter into
 the Jhanas and had the idea that eventually through Anapan
asati you start to see
 a light and that's the nimitta that you use to enter the J
hanas.
 Which is kind of interesting because the breath being,
 so the breath being based on ultimate reality.
 If you focus on the reality of say the stomach or the
 feeling here,
 you can't enter into Jhana obviously because it's imper
manent suffering and
 non-self, it's not something stable that you can absorb in.
 So there has to be some conceptual object like the breath
 or
 something that you would focus on.
 But the Buddha had, there's one passage that's really
 interesting,
 not this quote actually, there's a much better quote.
 This one is sort of practical explaining how to teach
 different ways and
 there's lots of different ways you can watch the breath,
 noticing it's long, noticing it's short, which is actually
 a part of what we do.
 When you watch the stomach, noticing that it's long and
 noticing that it's short is
 a part of seeing both the reality of the experience, being
 impermanent,
 suffering and non-self, and also the reality of your mind
 recognizing this is long,
 recognizing this is short because something is only long
 and short,
 relatively speaking, so it's only the mind that decides.
 And so that's an experience that arises in the mind.
 So seeing all of that is a part of the Vipassana practice.
 But there's calming the mind using breath,
 there's seeing impermanence using the breath,
 the chānupasi rehārati, contemplating release or being
 released by focusing on the breath.
 Lots of different aspects.
 But the quote that I wanted to bring up was where the
 Buddha praises Annapana Sati,
 so he can get an idea of what he thought of it.
 He says, "Santho jeeva panito jā."
 It is both peaceful santha, right?
 Santo.
 It's peaceful and panita, panito jā, which means subtle.
 Santo jeeva panito jā.
 So it's clearly something that calms the mind down.
 That's, you know, even in Vipassana we talk about how it's
 not really designed to calm you down, but
 you can definitely see that having the object of the
 stomach to come back to is calming,
 it's reassuring, it's a base.
 When you're dealing with the craziness of the mind, having
 the stomach to go back to is
 having the breath to go back to.
 I mean, the breath is always there, something you can rely
 on.
 And it's something that can become very peaceful.
 Santo.
 It's very subtle.
 And subtle, it can get subtle.
 I mean, that's more to do with samatha, where you focus on
 the subtle sensations at the nose or whatever.
 Asejnaka.
 This is an interesting one, and this is...
 Asejnaka.
 Asejnaka means mixed.
 So asejnaka means unmixed, unadulterated.
 Somehow it might be translated.
 But it's interesting to use this word to rebut people,
 especially people, you know,
 meditators from the Thai tradition who seem to think you
 can mix Buddha and mindfulness
 with the Buddha and mindfulness with the breath.
 I would argue that this word could be used against that,
 along with the experiences that
 come from mixing them as well.
 But, you know, I shouldn't be too critical to each their
 own.
 But if you talk to monks in Sri Lanka and tell them that
 that's what they're doing in Thailand,
 they just shake their heads, where did they get that idea?
 Mixing two meditations like that.
 Asejnaka means it's unmixed.
 So the breath is breath.
 It's pure, right?
 There's no baggage associated with it.
 I mean, for most people.
 And when you focus on it, it's such a pure object.
 There's no bias in the sense of it being useful for some
 people and not useful for others.
 It being only useful for this type of person.
 Asejnaka, it's unadulterated.
 There's no baggage associated with it for most people.
 Asejnaka suko jawiharo, it's a dwelling in happiness.
 So again, much more to do with the tranquility side of
 mindfulness of breath.
 Where you're focusing on the concept of the breath going in
 and going out, it becomes very calming.
 But no matter what, whether it's vipassana or samatha,
 focusing on the breath is both peaceful and pleasant.
 Happy.
 Now, in vipassana, the problem comes that you eventually
 have to deal with when it's unpleasant.
 You have to deal with the tension in the stomach,
 or you have to deal with the fact that it's not under your
 control.
 It can be quite uncomfortable.
 But by focusing on it, by using it, by being objective,
 which it helps you to become objective because it's such an
 objective object,
 happiness comes regardless of the discomfort.
 So it's important to separate happiness and comfort,
 happiness and pleasure.
 Suko jawiharo doesn't have to mean it's pleasant, but it's
 peaceful and
 it's almost pleasant, but it's pleasant through unpleasant
ness.
 It's important to understand because
 vipassana meditation can be quite unpleasant.
 People think they're doing it wrong or that the meditation
 is wrong.
 Because they see impermanence, I think, changing, chaotic.
 They see suffering and it's unpleasant, it's unsatisfying,
 it's unamenable to your wishes.
 It's unsatisfying, I guess, for lack of a better word.
 And it's uncontrollable.
 So you can't force the breath and you find yourself trying
 to force it.
 Suffering more is a result.
 So if you see that, you might get discouraged and think
 this isn't happy.
 But there's such happiness that comes when you learn to be
 objective about it,
 brings happiness.
 But I think mostly this is referring to how calming it is
 in the beginning.
 If you focus on the concept of the breath, it can be quite
 calming
 and pleasant.
 But the most important aspect is the final characteristics.
 We got santho, jeva, panito, ca, suko, jui, asejana, ca, su
ko, jui, haro.
 I think there's another one that I'm forgetting.
 But the final one is, upanupanay, akusa, papakey akusa le
 damme.
 Whatever evil dammas, papakey, akusa le, and wholesome evil
 dammas have arisen.
 Antara dappeti, mupasameti, it causes mindfulness of
 breathing,
 destroys them basically, causes them to go away, causes
 them to cease to be tranquilized, neutralized.
 It really forces you, I mean this is much more on the vip
assana side.
 It really forces you to give up because you can't control
 it.
 You can't control the breath. It forces you to let go.
 It helps you to let go because it gives you something pure.
 There's no greed attached to it. There's no anger attached
 to it.
 It gives you something pure to focus on and let all your
 anger and all your greed just pour out.
 It's just like water washing it off.
 I think the Buddha likens it to rain, rain actually.
 Just like in the hot season, when you get one season where
 when it rains, it's cool and refreshing.
 And it also washes away all your defilement.
 It is not to be underestimated because you think that it
 was rising from that.
 That's just a simple exercise. You can become enlightened
 just watching your stomach rise and
 fall. You see all three characteristics very clearly.
 It's impermanent suffering and non-self. It's all right
 there.
 And we're forced to let go of defilement. Any defilement
 you have for anything in the world,
 it will show up. It's a cloth and you're wiping your hands
 and all the stains
 show up on the cloth and it wipes away all the stains.
 So anyway, some little speech in praise of Anapanasati.
 Which is very, you know, it's very possible to,
 very reasonable just to suggest that what we practice is a
 form of Anapanasati.
 It's also, you can also describe it as mindfulness of the
 aggregates or the elements because it's that as well.
 Okay, so do we, I've got a whole posse here. Hey guys.
 Some people joined the hangout. Do any of you guys have m
ics?
 I can hear you. Hi Larry.
 Hey, this is my first visit. Oh welcome.
 I've just started trying to get people to join the hangout.
 So I figure if people really want to ask,
 if they're willing to come on here, it means they really
 need to ask their question badly.
 So we'll cull or we'll separate the wheat from the chaff
 this way.
 If you're just idly typing it in, it's a sign that you don
't really need an answer.
 I mean the problem is lots of questions and repeat
 questions more.
 So, and this is more real, you know, right? This is
 actually, we're now a community of
 four of us. We're a Sangha, you could say. And I've got two
 guys sitting here in my room listening.
 So, anybody got any questions or are you just here to say
 hi?
 I'm just here to say hi and get a point with the process.
 If I guess I was thinking that this might be just a Dhamma
 talk,
 and then I question the next opportunity. So I'm not
 prepared.
 That's fine. Yeah, I mean I haven't announced that or
 anything. I, you know,
 I just gave like 10 minutes. So that's our short Dhamma
 talk for the day.
 But then we can stop there. But, you know, we can talk for
 a few minutes anyway.
 You guys are live on the internet in case you didn't
 realize it.
 So we've got 23 viewers.
 And talk.
 I have a question about that.
 Go for it.
 Is it, is it, is it important not to move while you're med
itating with a
 seated meditation?
 It's important to learn while you're meditating. I mean, it
's important that we understand what
 we're doing because we're not trying to get into some
 ecstatic state or trance state.
 And we pass into, we're trying to learn about our minds and
 about our bodies.
 So there's not so many shoulds and shouldn'ts, musts and
 must nots.
 You just have to ask yourself logically, you know, what
 happens when I, for example, when I move.
 So if you're moving without being mindful, then it's a
 moment of delusion or it's a moment of,
 you know, following your habits.
 Moreover, the question is, why are you moving?
 Right. So the cause of your moving is potentially a problem
.
 So we move because we're uncomfortable.
 Uncomfortable means disliking.
 Disliking means anger.
 Anger is one of the defilements.
 So acting on it is problematic.
 It's cultivating the habit of aversion.
 Something bad happens, you find a way to escape it.
 That's a big reason why we move.
 Another reason why we move is we get tired and maybe lazy.
 And so we start to slouch.
 And so again, you might want to address that if you're
 tired or so on, to say to yourself, tired and tired.
 I do follow.
 But that being said, it can be that the pain is just too
 intense and you have no choice,
 you're going to move.
 So I guess the two parts, you note whatever it is that
 making you want to move,
 you say angry and sorry, disliking, disliking or tired.
 But when you do move, that's not the answer to the short
 answer to the question.
 No, it's not wrong.
 I mean, we don't really have wrong and right.
 It's best if you can be free from the need to move for,
 if you can free yourself from the aversion to the pain.
 But there's no intrinsic reason why moving should be wrong
 because you can be mindful of it.
 So absolutely, when you need to move, when you feel like,
 okay, enough, I have to back off,
 it's just too intense.
 Absolutely move.
 But the technique, the proper technique would be to say to
 yourself, wanting to move, wanting to move,
 just a couple of times, noting the intention.
 Sometimes you do that and then you don't have to move.
 But probably you still want to move.
 Lift your hand up.
 So you're moving your foot, you say lifting, lifting,
 moving,
 and you grab the leg grabbing or holding or touching, then
 lifting, moving, lowering,
 releasing and so on.
 So just being mindful.
 That's fine.
 Who's this guy in the middle?
 90 Sim.
 What does 90 Sim?
 Can you hear me?
 I can hear you.
 Oh, who are you?
 So I'm Simon.
 Oh, hi Simon.
 Okay.
 Hey, nice to meet you.
 Nice to meet you.
 You don't have a webcam, I guess.
 No, I'm sorry.
 I just got the mic for now.
 Did you record today's Second Life Talk?
 Yep, we recorded it at Hollow Hill because I kind of messed
 up the time,
 so I wasn't there for the last session.
 So we just listened.
 When actually just 10 minutes after I finished there,
 I just kind of messed up the time.
 So you didn't record the video?
 Yeah, I made a video, but it was not recorded at the D-ap
art.
 It was recorded at another place where we just sat down and
 meditated and listened to.
 Oh, I see.
 Oh, okay.
 At that place actually?
 I don't know if you've been there?
 No.
 No, I haven't been on Second Life much.
 I was thinking you all have places on Second Life, so there
's no point.
 But at one point, we had set up an open Sim.
 If you know open Sim,
 you can set up your own Second Life for free.
 You just need a server.
 Huh?
 Yes, so that's also an open and published space as well,
 where we work today.
 But like with open Sim, you have it on your own server.
 You can have up to...
 You guys can go ahead.
 You guys go meditate.
 You don't need to listen to this.
 This is more general stuff.
 You can have as many people on at once as you like.
 You can have as many shapes and stuff as you like.
 It's all free.
 Tends to be less laggy, depending on your server, than
 Second Life,
 because Second Life is...
 Or it used to be.
 Second Life used to be pretty problematic.
 You can do a lot more on it.
 And the thing is, it would be just our group.
 You wouldn't have...
 I mean, that's good and bad,
 but the good side is it would just be for this group.
 The way to get everybody kind of in the same room together,
 sitting down.
 Yeah, it's fake, but...
 Virtual.
 There's potential for good there, I think.
 It was very interesting listening to the talk today, if you
 could.
 I have a question.
 Yep.
 Second Life, I'm not clear.
 I've never participated in any of those kinds of
 applications or activities,
 so I'm really ignorant of what it's all about.
 I'm getting the impression, but I am curious.
 I'm getting the impression that perhaps it's kind of a way
 to give the virtual Sangha opportunity
 to have some more interface or activity outside of the
 meditation,
 Sir Mango-O side.
 Yeah, I mean, it's a place...
 What people will say good about it is it's a chance to meet
 other people,
 meet other Buddhists, talk with them, meditate with them,
 encourage each other.
 People really like it.
 It's kind of like a game, I think.
 Kind of...
 You know, it's got its good and it's bad.
 The bad is it's kind of like a game.
 So it's easy to get caught up in it.
 Yeah, it's been good the past couple of weeks.
 I've had really good audiences, 20 some people, which is
 per second.
 Pretty good.
 Does it encompass download talent teaching or is it kind of
 a peer-to-peer sharing
 It's virtual reality.
 So you actually see cartoon characters of each other
 sitting in a deer park.
 If you look on YouTube, you can see I've recorded, if you
 Google me,
 "Yutadamo Second Life," you'll probably find my videos that
 I did in Second Life.
 Lots of good talks back in the day.
 Very good.
 They're works.
 Yeah, I guess I shouldn't be saying they're good, but I
 think they were, you know,
 I was really keen on it at the time and doing and preparing
 for them and stuff.
 Bonthe, I listened to those probably more than anything
 else.
 Yeah, I listened to them repeatedly.
 Well, they're really good talks.
 That's good to hear.
 I have another question.
 I have registered for a teacher meet at this week, only one
 morning.
 I think perhaps Friday there was an available slide.
 This would be my first occasion.
 So is it pretty much just earbuds, audio vision, like, hang
 out right now?
 It's very much like what you're doing right now.
 You have to call me is the only difference.
 So you can do that using my Gmail address.
 You can also, on the meditation site, on the page where you
 signed up for it,
 if you're there at the right time, a little button will
 show up.
 And that should do it for you.
 But if you can't do it that way, you can go to hangouts.
google.com.
 And there's a link to that as well, I think on the page,
 right?
 Yeah, you can there's links to Google Hangout.
 Go there and find me using my Gmail address.
 I presume that it's particularly an opportunity.
 I've been meditating on my own down here in South
 Mississippi,
 pretty well isolated from Buddhism for about three years.
 Basis reading and YouTube videos, watched a lot of your
 videos,
 a number of authors, and just kind of grouping my way
 through the process.
 This will be my first opportunity to actually tell somebody
 who understands what I'm up to
 and ask, how can I do it better?
 What should I do differently?
 I'm open to criticism, et cetera.
 Well, what a lot of people don't realize who've gotten
 involved or read my booklet or whatever,
 is that there's more than just the first step.
 Like what you read in my booklet or in my videos is the
 first day of a meditation course.
 There's it gets a lot more involved in that.
 And there's a lot more exercises to give you.
 But can't really do that just by putting them out on the
 internet, because people will,
 there's a potential to go too far too fast on your own,
 that kind of thing.
 You're probably not, but we don't really do it.
 It's just following tradition.
 You have to get the exercises from me.
 So that's what these are for.
 You have to meditate at least an hour a day to be
 considered for it.
 Otherwise, I won't talk to you.
 I'll say go back next week.
 I'd talk to you and then I give you a new exercise.
 If you have questions, you ask them.
 If I have questions, I'll ask them just to make sure you're
 on track, give you the new exercise.
 So we go through what would be the equivalent of like 90%
 of a foundation course if you were
 coming to stay here and doing like many hours a day.
 And so you'll do like one hour a day to start.
 And with the expectation that you should have the
 expectation that you're eventually getting
 up to two hours a day.
 So that's what you should be preparing for.
 Start at one hour, get up to two hours.
 That sort of, I've just made that up on my own, but that
 seems reasonable.
 And then we meet once a week, as you can see, and you do in
 one week,
 what you would be doing in a day.
 As a result, it takes about 15, 18 weeks to finish that
 course.
 And then to actually finish the foundation course, you'd
 still have to come here.
 Like right now, there's one man who did that, finished the
 online course, Patrick.
 And now he's come here for 10 days and he'll finish within
 10 days, the foundation course.
 Okay, so it is, I wasn't clear on that, so I appreciate it.
 There's expectation that I would make arrangements for a
 weekly call in.
 Yes.
 Good.
 Okay.
 Good to.
 Yeah.
 Once you've got the slot, it's yours every week until we
 finish the process or until you decide
 you don't want to do it anymore, then you take yourself off
 the list.
 Okay.
 My life schedule can vary a little bit with sometimes
 having grandbaby responsibilities,
 things like that.
 So I didn't realize that I was needing to commit to that
 particular time slot each week.
 Is that all throughable?
 Yeah, you can change it as you like.
 I mean, you still want to give yourself a week of practice
 in between meetings,
 but yeah, you can choose.
 As long as there's a slot open, just take a new slot.
 Okay.
 Or if you can't make it and there's no slot, just send me
 an email and we can work something out.
 Okay.
 Potentially.
 Okay.
 Super.
 Thanks for all that information.
 Great.
 Appreciate that.
 Yeah.
 Anybody else?
 Otherwise, I'm going to head off.
 Good night.
 Good night, everyone.
 Good night.
 Good night.
 Thank you.
 Thanks for tuning in.
 Thank you.
 Good night.
 Okay.
