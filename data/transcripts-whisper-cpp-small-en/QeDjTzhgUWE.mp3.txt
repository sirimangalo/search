 There's a question whether in this life or a future life to
 be free from suffering one must rid oneself of desire.
 Is one required to be a monk to be enlightened?
 No, not explicitly required.
 Somebody even said that even though one is decked out in
 jewels and wearing a royal attire,
 if they become free from desire and free from defilement,
 they should be considered a monk.
 I believe if I'm not wrong it was Santati, this minister of
 the king,
 who the Buddha told this verse about because Santati was a
 minister to some king, Pasenadi, maybe,
 and he was very clever and was able to defeat an enemy or
 quell an uprising in a border village.
 The king, as a reward to him, allowed him to live as a king
 for seven days,
 not to be the king but to live the luxury and to have as
 much luxury as the king had for seven days.
 For seven days he lived in complete luxury and sensuality.
 He was even drinking alcohol and sporting himself with
 women and prostitutes.
 He had this one dancing girl who was in his employment,
 and he was so in love with her or in lust with her,
 that he would just love to sit there and drink, get really
 drunk and watch her dance.
 I guess there was another thing at the time that, similar
 to now, as I understand nowadays,
 is women had to be really thin in order to be attractive or
 something.
 I don't know if that's still the case, but I did hear this
 whole thing about anorexia,
 women think they have to be thinner and thinner.
 If you come to our monastery, you don't really have a
 choice, you just get thinner and thinner.
 So she was abstaining from food because she wanted to be "
beautiful."
 She got so bad that when she got up on stage to dance on
 the seventh day,
 she had some nerve broke or something broke inside of her.
 There's this story, the same thing happened to a monk
 apparently in the Buddhist time.
 She died, just suddenly, boom, died right there on the
 stage.
 He's looking at her and he's drunk.
 He got so upset about this that he went to find the Buddha,
 all decked out in his jewels and his robes and so on.
 He was so sobered up by the affair that in the time it took
 him to walk to the Buddha,
 he no longer was drunk or something like that because he
 got to the Buddha
 and the Buddha taught him and he became an arahant, if I
 remember correctly.
 The story is so... I'm not so good with my stories, but
 either he became an arahant...
 I think he became an arahant and then he passed away or
 something like that.
 There was one man, if it wasn't Santati, who passed away.
 And then they were all asking him, "Well, what is he? Is he
 a Brahmin or is he a shaman or is he a monk?"
 What is he? And the Buddha said, "Even whether they are in
 these royal clothes and bedecked in jewels,
 if they practice correctly, they're considered a Brahmin,
 they're considered a Samhain, something like that."
 But for sure the theory is sound, that one can become
 enlightened without becoming a monk.
 Now, the orthodox theory is that... or doctrine, is that if
 they don't ordain, they will pass away.
 That day or within seven days, I think within seven days is
 the orthodox doctrine.
 And it seems to be the case that either they became monks
 or else they passed away.
 And the question, of course, is whether that means a full
 ordination or just means living the home life.
 Anything to add? No.
 Nothing to add to that.
