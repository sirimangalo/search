1
00:00:00,000 --> 00:00:15,040
 [ Silence ]

2
00:00:15,040 --> 00:00:21,920
 >> Hi. So this is in answer to the second of the questions,

3
00:00:21,920 --> 00:00:25,060
 I believe, or maybe I've gotten them in the wrong order.

4
00:00:25,060 --> 00:00:28,060
 But the other question is what do you love?

5
00:00:29,420 --> 00:00:36,040
 Yeah. Well, I mean, off the top of my head,

6
00:00:36,040 --> 00:00:41,610
 I think it's important to point out that one practice that

7
00:00:41,610 --> 00:00:42,040
 monks

8
00:00:42,040 --> 00:00:47,440
 or Buddhists are very set on is the practice

9
00:00:47,440 --> 00:00:55,420
 of unlimited loving kindness or love for all beings.

10
00:00:55,420 --> 00:00:58,480
 So I practice that every day and I really, I think if I

11
00:00:58,480 --> 00:00:58,800
 were

12
00:00:58,800 --> 00:01:02,400
 to answer it in ordinary terms, I would say I really love

13
00:01:02,400 --> 00:01:02,700
 people.

14
00:01:02,700 --> 00:01:04,740
 I love beings.

15
00:01:04,740 --> 00:01:06,060
 I really do.

16
00:01:06,060 --> 00:01:07,720
 I really think it's, you know,

17
00:01:07,720 --> 00:01:09,840
 through the meditation practice that's something I've come

18
00:01:09,840 --> 00:01:16,160
 to really appreciate is people's essence or the essence

19
00:01:16,160 --> 00:01:17,940
 of beings, even the smallest beings.

20
00:01:17,940 --> 00:01:19,860
 I mean, I love animals.

21
00:01:19,860 --> 00:01:23,320
 I love babies.

22
00:01:23,320 --> 00:01:24,280
 I love old people.

23
00:01:24,680 --> 00:01:30,540
 This feeling of love is something that really comes

24
00:01:30,540 --> 00:01:34,820
 from meditation and I think other meditators would agree

25
00:01:34,820 --> 00:01:38,920
 with me that this is something that we can all agree

26
00:01:38,920 --> 00:01:44,520
 that is very lovable or is very wonderful or brings forth a

27
00:01:44,520 --> 00:01:44,720
 great

28
00:01:44,720 --> 00:01:45,260
 amount of love.

29
00:01:45,260 --> 00:01:49,040
 Being more specific, I just wanted to show you a few things

30
00:01:49,040 --> 00:01:50,460
 that I love here.

31
00:01:50,960 --> 00:01:57,960
 So we're going to zoom in, let's see, and I'm going to,

32
00:01:57,960 --> 00:02:03,680
 first of all, the first thing I love is my parents.

33
00:02:03,680 --> 00:02:05,920
 This is my father.

34
00:02:05,920 --> 00:02:09,760
 I asked him to send me a picture of himself.

35
00:02:09,760 --> 00:02:11,360
 I realized I don't have any pictures of him,

36
00:02:11,360 --> 00:02:13,480
 so he sent me this.

37
00:02:13,480 --> 00:02:17,080
 It's a picture of my father with a guitar.

38
00:02:19,000 --> 00:02:23,160
 This is my mother, another person I love.

39
00:02:23,160 --> 00:02:26,360
 My parents have been really good to me.

40
00:02:26,360 --> 00:02:31,760
 They are two people who I think weren't for them.

41
00:02:31,760 --> 00:02:33,960
 I wouldn't be here today, of course, goes without saying,

42
00:02:33,960 --> 00:02:37,280
 but no, really, you know, just the great things

43
00:02:37,280 --> 00:02:39,780
 that they've done for me, bringing me up,

44
00:02:39,780 --> 00:02:41,760
 giving me all the things that I need.

45
00:02:41,760 --> 00:02:48,200
 You know, sometimes when I wanted to do something,

46
00:02:48,200 --> 00:02:52,520
 go on a trip or get involved in some organization,

47
00:02:52,520 --> 00:02:56,040
 my parents were always there for me to support me,

48
00:02:56,040 --> 00:03:00,820
 both monetarily and emotionally.

49
00:03:00,820 --> 00:03:04,480
 It was a little more difficult when I decided I wanted

50
00:03:04,480 --> 00:03:06,360
 to become a Buddhist and a Buddhist monk.

51
00:03:06,360 --> 00:03:09,680
 Once I decided that I wanted to shave my head and put on

52
00:03:09,680 --> 00:03:10,080
 robes

53
00:03:10,080 --> 00:03:14,260
 and walk around barefoot, it was a little more difficult

54
00:03:14,260 --> 00:03:18,180
 for them to, well, let's say a lot more difficult

55
00:03:18,180 --> 00:03:20,440
 for them to accept and be supportive of.

56
00:03:20,440 --> 00:03:25,540
 But I think recently or in the past while,

57
00:03:25,540 --> 00:03:26,860
 they've become much more supportive.

58
00:03:26,860 --> 00:03:32,340
 And, you know, now I think I really have to thank them

59
00:03:32,340 --> 00:03:35,550
 for just being so understanding and open-minded about this

60
00:03:35,550 --> 00:03:35,840
 all.

61
00:03:35,840 --> 00:03:37,500
 There's my father again.

62
00:03:37,500 --> 00:03:40,580
 He's actually a lawyer.

63
00:03:40,580 --> 00:03:45,060
 He's not a musician by trade, but, well, no.

64
00:03:45,060 --> 00:03:48,020
 And, yeah, there's a close-up of my father.

65
00:03:48,020 --> 00:03:49,540
 Okay? So that's two things that I love.

66
00:03:49,540 --> 00:03:55,600
 A third thing is my teacher is something or someone

67
00:03:55,600 --> 00:03:57,340
 that I really, really love.

68
00:03:57,340 --> 00:03:59,300
 And I'm going to just show you some pictures here of him,

69
00:03:59,300 --> 00:04:04,620
 but I'm going to also play an audio talk just

70
00:04:04,620 --> 00:04:08,220
 so you can hear why my teacher is so lovable.

71
00:04:08,220 --> 00:04:09,960
 You can hear how lovable he is.

72
00:04:09,960 --> 00:04:11,660
 Let's see if it's going to start.

73
00:04:12,560 --> 00:04:28,560
 [ Foreign Language ]

74
00:04:29,560 --> 00:04:57,560
 [ Foreign Language ]

75
00:04:57,560 --> 00:05:06,560
 [ Foreign Language ]

76
00:05:08,560 --> 00:05:27,560
 [ Foreign Language ]

77
00:05:28,560 --> 00:05:38,560
 [ Foreign Language ]

78
00:05:38,560 --> 00:05:48,560
 [ Foreign Language ]

79
00:05:48,560 --> 00:05:58,560
 [ Foreign Language ]

80
00:05:59,560 --> 00:06:27,560
 [ Foreign Language ]

81
00:06:27,560 --> 00:06:38,560
 [ Foreign Language ]

82
00:06:38,560 --> 00:06:47,560
 [ Foreign Language ]

83
00:06:47,560 --> 00:06:56,560
 [ Foreign Language ]

84
00:06:56,560 --> 00:07:05,560
 [ Foreign Language ]

85
00:07:07,560 --> 00:07:32,560
 [ Foreign Language ]

86
00:07:33,060 --> 00:07:35,760
 Okay. So there's just a few of these many images.

87
00:07:35,760 --> 00:07:38,400
 And that's just a talk he gave in Thai.

88
00:07:38,400 --> 00:07:42,910
 That's really good talk, but I don't suppose any of that

89
00:07:42,910 --> 00:07:43,100
 was,

90
00:07:43,100 --> 00:07:48,220
 for most people, that is not understandable.

91
00:07:48,220 --> 00:07:50,120
 He's talking about doing good deeds.

92
00:07:50,120 --> 00:07:52,400
 I just wanted you to hear his voice

93
00:07:52,400 --> 00:08:00,000
 because he has a very soft, very distinguished voice.

94
00:08:01,360 --> 00:08:05,080
 And he was talking about the New Year's resolution

95
00:08:05,080 --> 00:08:07,320
 of the Thai government many years ago to do

96
00:08:07,320 --> 00:08:11,240
 for the whole country to set themselves in good deeds.

97
00:08:11,240 --> 00:08:15,790
 And it was, he said that's in line with the Buddhist

98
00:08:15,790 --> 00:08:16,120
 teaching

99
00:08:16,120 --> 00:08:20,640
 that says that we should set our minds, it is the minds

100
00:08:20,640 --> 00:08:22,980
 which is most important, that we should set our minds

101
00:08:22,980 --> 00:08:23,880
 on doing good deeds.

102
00:08:24,800 --> 00:08:31,520
 That if our mind, if someone does a bad deed, it hurts them

103
00:08:31,520 --> 00:08:31,520
.

104
00:08:31,520 --> 00:08:33,740
 And it hurts them in this life.

105
00:08:33,740 --> 00:08:37,160
 But if their, when their mind is set on doing bad deeds,

106
00:08:37,160 --> 00:08:40,880
 it's something that carries with them their whole life

107
00:08:40,880 --> 00:08:42,300
 and even into the next life.

108
00:08:42,300 --> 00:08:46,140
 So he said it's most important

109
00:08:46,140 --> 00:08:48,420
 that we set our minds on goodness.

110
00:08:48,420 --> 00:08:50,080
 We set our minds on doing good things.

111
00:08:51,320 --> 00:08:57,200
 That we set our minds away from doing evil deeds.

112
00:08:57,200 --> 00:09:01,720
 Okay, I think that's all.

113
00:09:01,720 --> 00:09:07,040
 So there's three things that I love.

114
00:09:07,040 --> 00:09:10,320
 The final thing that I would say I love is I love being a

115
00:09:10,320 --> 00:09:10,620
 monk.

116
00:09:10,620 --> 00:09:14,080
 And I hope that's clear from the videos

117
00:09:14,080 --> 00:09:15,680
 that I've been recording.

118
00:09:16,160 --> 00:09:22,040
 This is the one thing that I could ever think of doing

119
00:09:22,040 --> 00:09:22,640
 in my life.

120
00:09:22,640 --> 00:09:25,600
 I couldn't ask for anything more and I couldn't ask

121
00:09:25,600 --> 00:09:26,840
 to be doing it something better.

122
00:09:26,840 --> 00:09:30,880
 There's nothing else in the world that I want to be more

123
00:09:30,880 --> 00:09:32,040
 than what I am right now.

124
00:09:32,040 --> 00:09:35,120
 And that's a monk as far as externally.

125
00:09:35,120 --> 00:09:39,920
 If I could think of one situation that I'd like to be

126
00:09:39,920 --> 00:09:42,120
 in it's the situation of the monk's life.

127
00:09:42,120 --> 00:09:44,260
 And so this is one thing that I really love.

128
00:09:44,260 --> 00:09:45,320
 I love being a monk.

129
00:09:45,320 --> 00:09:49,080
 I love the structure and the rigor and the lifestyle.

130
00:09:49,080 --> 00:09:51,300
 And the fact that I'm able to help people.

131
00:09:51,300 --> 00:09:53,240
 I'm able to give something to people that's useful.

132
00:09:53,240 --> 00:09:56,560
 Okay, so that's the answer to my answer to the question,

133
00:09:56,560 --> 00:09:57,580
 what do you love?

