1
00:00:00,000 --> 00:00:03,000
 Dear Panthe, I am seeing 20s.

2
00:00:03,000 --> 00:00:04,000
 No, not that one.

3
00:00:04,000 --> 00:00:07,000
 Can there be desire?

4
00:00:07,000 --> 00:00:11,540
 Oh, okay. I don't see anything selected, so I just picked

5
00:00:11,540 --> 00:00:12,000
 the top one.

6
00:00:12,000 --> 00:00:15,000
 Okay. Can there be desire that isn't harmful?

7
00:00:15,000 --> 00:00:22,080
 It's semantics. I can't really answer this question,

8
00:00:22,080 --> 00:00:24,000
 because you have to tell me what you mean by desire.

9
00:00:24,000 --> 00:00:30,030
 I don't think there can, because I define desire as

10
00:00:30,030 --> 00:00:31,000
 actually wanting something.

11
00:00:31,000 --> 00:00:34,000
 Even that is ambiguous.

12
00:00:34,000 --> 00:00:38,810
 There are very few cases where the words wanting or desire

13
00:00:38,810 --> 00:00:45,270
 could be used that I wouldn't call a case of craving and

14
00:00:45,270 --> 00:00:46,000
 therefore harm.

15
00:00:48,000 --> 00:00:52,800
 But those few cases do exist, like a person who desires to

16
00:00:52,800 --> 00:00:57,610
 be free from suffering, a person who wants to practice

17
00:00:57,610 --> 00:00:59,000
 meditation.

18
00:00:59,000 --> 00:01:06,560
 They're used in a, I believe the word is, a semantic sense,

19
00:01:06,560 --> 00:01:11,000
 not a literal sense, figurative sense, maybe.

20
00:01:13,000 --> 00:01:17,940
 In certain cases, a person can express those desires and

21
00:01:17,940 --> 00:01:23,000
 wants without actually wanting or desiring anything.

22
00:01:23,000 --> 00:01:26,000
 It's a figure of speech. So I want to practice meditation.

23
00:01:26,000 --> 00:01:32,000
 It may not be that I actually want that experience,

24
00:01:32,000 --> 00:01:36,840
 but it's a way of saying that now I'm going to practice

25
00:01:36,840 --> 00:01:40,170
 meditation through, because I have the wisdom to know that

26
00:01:40,170 --> 00:01:42,000
 meditation is the right thing to do at this point.

27
00:01:44,000 --> 00:01:48,030
 But I don't consider that to be desire. So I would consider

28
00:01:48,030 --> 00:01:51,000
 every desire as harmful.

29
00:01:51,000 --> 00:01:53,410
 But there are big arguments about this that are really just

30
00:01:53,410 --> 00:01:55,000
 semantics, so it's kind of silly.

