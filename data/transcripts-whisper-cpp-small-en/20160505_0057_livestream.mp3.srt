1
00:00:00,000 --> 00:00:06,000
 Good evening everyone.

2
00:00:06,000 --> 00:00:12,760
 Tonight we are broadcasting live, but we are going to try

3
00:00:12,760 --> 00:00:15,000
 DemiPada again.

4
00:00:15,000 --> 00:00:18,000
 It's time 1.30.

5
00:00:18,000 --> 00:00:24,500
 It's actually almost the same as 1.29, so not too much to

6
00:00:24,500 --> 00:00:27,000
 say that we're going in order.

7
00:00:27,000 --> 00:00:37,000
 Hello and welcome back to our study of DemiPada.

8
00:00:37,000 --> 00:00:42,980
 Today we continue on with verse 1.30, which reads as

9
00:00:42,980 --> 00:00:44,000
 follows.

10
00:00:44,000 --> 00:00:54,800
 "Sambhaita santi tandasa sambhaita jivitam vyam atanam upam

11
00:00:54,800 --> 00:01:00,000
ang kattwa naha neya nagatim."

12
00:01:00,000 --> 00:01:05,000
 Which is almost the same as the last one.

13
00:01:05,000 --> 00:01:15,960
 It means "all tremble at the rod." What is different is "j

14
00:01:15,960 --> 00:01:18,000
ivitang vyam life is dear sabheisam to all."

15
00:01:18,000 --> 00:01:23,000
 Life is dear to all.

16
00:01:23,000 --> 00:01:25,000
 And then the same as the last one.

17
00:01:25,000 --> 00:01:34,340
 "Sambhaita santi tandasa nang upamang kattwa naha nayam nag

18
00:01:34,340 --> 00:01:36,000
atim."

19
00:01:36,000 --> 00:01:40,910
 And the story actually is said to be almost the same as the

20
00:01:40,910 --> 00:01:42,000
 last one.

21
00:01:42,000 --> 00:01:45,000
 The Buddha laid down two rules.

22
00:01:45,000 --> 00:01:50,680
 And so in the last story, it's about how he laid down the

23
00:01:50,680 --> 00:01:54,000
 rule against hitting other monks.

24
00:01:54,000 --> 00:01:59,000
 A group of six monks who were infamous for their escapades,

25
00:01:59,000 --> 00:02:06,320
 breaking a lot of rules and being the instigation for

26
00:02:06,320 --> 00:02:10,000
 establishing many rules.

27
00:02:10,000 --> 00:02:19,000
 They hit a group of 17 monks who had taken up residence.

28
00:02:19,000 --> 00:02:22,000
 So this one doesn't say what actually happened,

29
00:02:22,000 --> 00:02:25,840
 but for some reason or other they were angry at these satar

30
00:02:25,840 --> 00:02:27,000
asa waghi bhikum,

31
00:02:27,000 --> 00:02:31,000
 a group of 17 monks.

32
00:02:31,000 --> 00:02:34,930
 But in this case, it's interesting, the English translation

33
00:02:34,930 --> 00:02:36,000
 I think gets it wrong.

34
00:02:36,000 --> 00:02:40,000
 The English translation says, "they hit them."

35
00:02:40,000 --> 00:02:45,140
 And then this group of 17 monks held up their fists in

36
00:02:45,140 --> 00:02:47,000
 response to scare them away.

37
00:02:47,000 --> 00:02:50,000
 But I don't think that's what the Pali actually says.

38
00:02:50,000 --> 00:02:54,630
 I can't quite get it for sure, but it certainly doesn't

39
00:02:54,630 --> 00:02:57,000
 make it clear that it was in response.

40
00:02:57,000 --> 00:03:00,940
 It appears to say that just like in the last one where they

41
00:03:00,940 --> 00:03:04,000
 actually hit the group of 17,

42
00:03:04,000 --> 00:03:09,340
 in this one the group of six monks just held up their fists

43
00:03:09,340 --> 00:03:11,000
 to scare them.

44
00:03:11,000 --> 00:03:14,000
 And again the monks screamed.

45
00:03:14,000 --> 00:03:17,140
 And again the Buddha heard and asked, "What's that scream

46
00:03:17,140 --> 00:03:18,000
 all about?"

47
00:03:18,000 --> 00:03:22,000
 And someone told him.

48
00:03:22,000 --> 00:03:28,000
 And he said, "Nabhikvayi to bhata yabhikuna ayam."

49
00:03:28,000 --> 00:03:31,000
 "Evan katabam."

50
00:03:31,000 --> 00:03:37,000
 From here on monks, this is not to be done by monks.

51
00:03:37,000 --> 00:03:43,000
 Whoever does it has broken rule.

52
00:03:43,000 --> 00:03:48,000
 And then he explains it. He says,

53
00:03:48,000 --> 00:04:02,000
 "Just as I, so others, tremble at the rod."

54
00:04:08,000 --> 00:04:13,810
 "Just as to me, and just as to me, so to others, life is

55
00:04:13,810 --> 00:04:15,000
 dear."

56
00:04:15,000 --> 00:04:23,030
 "Knowing this, Nyatva, Evan Nyatva. Knowing this, Iti Nyat

57
00:04:23,030 --> 00:04:24,000
va."

58
00:04:24,000 --> 00:04:29,000
 "Paro na parhita bhu naga khati tabhu."

59
00:04:29,000 --> 00:04:34,000
 "Knowing this one should not strike or kill him."

60
00:04:34,000 --> 00:04:41,830
 And so this is sort of the, as I already said, this is sort

61
00:04:41,830 --> 00:04:45,000
 of the Buddhist version of the Golden Rule

62
00:04:45,000 --> 00:04:48,000
 of doing unto others as they would do unto you.

63
00:04:48,000 --> 00:04:52,650
 But there's two interesting points that I can talk about

64
00:04:52,650 --> 00:04:54,000
 for this one.

65
00:04:54,000 --> 00:04:58,000
 The first is that they haven't actually done anything.

66
00:04:58,000 --> 00:05:01,000
 They didn't actually harm the other monks.

67
00:05:01,000 --> 00:05:09,400
 And so often we place a lot of emphasis on the action, you

68
00:05:09,400 --> 00:05:12,000
 know, when you actually harm someone.

69
00:05:12,000 --> 00:05:18,000
 And it's not any deeper esoteric teaching.

70
00:05:18,000 --> 00:05:22,930
 But it's interesting to remind ourselves that it's not

71
00:05:22,930 --> 00:05:25,000
 actually the acts itself.

72
00:05:25,000 --> 00:05:29,250
 It's our own viciousness in the mind when you raise your

73
00:05:29,250 --> 00:05:34,000
 hand against someone, even just raising your hand.

74
00:05:34,000 --> 00:05:39,250
 You know, a lot of abuse is not even physical. It's

75
00:05:39,250 --> 00:05:42,000
 emotional. It's the fear.

76
00:05:42,000 --> 00:05:50,000
 And oftentimes it's the fear itself that is more harmful,

77
00:05:50,000 --> 00:05:58,290
 that the actual bruises, the actual scars heal much easier,

78
00:05:58,290 --> 00:06:04,000
 much more completely than the mental scars.

79
00:06:04,000 --> 00:06:06,830
 You hear stories about parents who tell their kids to go

80
00:06:06,830 --> 00:06:09,000
 get them their belt, right?

81
00:06:09,000 --> 00:06:11,000
 "Go get me my belt."

82
00:06:11,000 --> 00:06:17,600
 Can you imagine knowing, like when you know what the

83
00:06:17,600 --> 00:06:19,000
 meaning is, you know,

84
00:06:19,000 --> 00:06:23,710
 you have to actually go and bring them the weapon they're

85
00:06:23,710 --> 00:06:32,000
 going to use against you, the trauma.

86
00:06:32,000 --> 00:06:37,070
 So that's one thing, remembering, especially for meditation

87
00:06:37,070 --> 00:06:38,000
 purposes,

88
00:06:38,000 --> 00:06:43,000
 remembering to focus on our mind states, our intentions.

89
00:06:43,000 --> 00:06:47,660
 It doesn't matter whether you actually kill or harm someone

90
00:06:47,660 --> 00:06:48,000
.

91
00:06:48,000 --> 00:06:54,000
 It's the viciousness, the cruelty, the lack of compassion,

92
00:06:54,000 --> 00:07:01,000
 the slavery to anger, you know,

93
00:07:01,000 --> 00:07:04,980
 getting lost in our own anger so we can't even see what we

94
00:07:04,980 --> 00:07:06,000
're doing.

95
00:07:06,000 --> 00:07:10,980
 And that's the second thing to point out is that this isn't

96
00:07:10,980 --> 00:07:14,000
 just an intellectual exercise where we say,

97
00:07:14,000 --> 00:07:17,560
 "Yes, I don't want to be harmed and therefore it makes

98
00:07:17,560 --> 00:07:19,000
 sense that I shouldn't harm others."

99
00:07:19,000 --> 00:07:21,000
 It's not like that.

100
00:07:21,000 --> 00:07:25,120
 The very fact that you don't want to be harmed yourself

101
00:07:25,120 --> 00:07:29,000
 makes it perverse for you to harm others.

102
00:07:29,000 --> 00:07:34,720
 And that's what makes it, or that's one way of explaining

103
00:07:34,720 --> 00:07:38,000
 what makes it karmically active.

104
00:07:38,000 --> 00:07:41,770
 That when you harm others it comes back to harm you and

105
00:07:41,770 --> 00:07:44,000
 your subject yourself to harm

106
00:07:44,000 --> 00:07:48,000
 because you know in your mind that this is wrong.

107
00:07:48,000 --> 00:07:52,540
 There's a sense that this is an evil thing and by evil I

108
00:07:52,540 --> 00:07:56,000
 just mean something you wouldn't want to happen to you.

109
00:07:56,000 --> 00:08:02,010
 And because you know that, when you do it to others it

110
00:08:02,010 --> 00:08:10,000
 leaves a scar on your mind.

111
00:08:10,000 --> 00:08:16,280
 It perverts your very reality, you know, because doing

112
00:08:16,280 --> 00:08:19,000
 things that you want to happen to you is easy

113
00:08:19,000 --> 00:08:22,000
 because there's a sense of the goodness of them.

114
00:08:22,000 --> 00:08:25,000
 This is a good thing to do so you do it for someone else.

115
00:08:25,000 --> 00:08:27,000
 Why? Because it's a good thing.

116
00:08:27,000 --> 00:08:30,000
 You don't have to feel the other person's happiness.

117
00:08:30,000 --> 00:08:32,000
 You know that this is something that leads to happiness.

118
00:08:32,000 --> 00:08:35,000
 So when you do it for them it makes sense.

119
00:08:35,000 --> 00:08:38,880
 To do something that causes harm to someone else actually

120
00:08:38,880 --> 00:08:43,000
 is perverse because you know the harm in it.

121
00:08:43,000 --> 00:08:45,520
 You know that this is an evil thing and by evil again this

122
00:08:45,520 --> 00:08:49,000
 is a suffering thing, a thing that leads to suffering.

123
00:08:49,000 --> 00:08:53,730
 So anyone who says that you should try and maximize your

124
00:08:53,730 --> 00:08:57,000
 own happiness even at the expense of others

125
00:08:57,000 --> 00:09:01,920
 or who argues that anyone who says look out for yourself

126
00:09:01,920 --> 00:09:04,000
 first is being selfish

127
00:09:04,000 --> 00:09:06,700
 because then they go and harm others doesn't really

128
00:09:06,700 --> 00:09:08,000
 understand how it works.

129
00:09:08,000 --> 00:09:12,000
 You can't be selfish and work for your own benefit.

130
00:09:12,000 --> 00:09:16,000
 Anyone who is selfish is not working for their own benefit

131
00:09:16,000 --> 00:09:19,000
 because they're doing things that they know are wrong.

132
00:09:19,000 --> 00:09:23,000
 And again wrong simply means things that lead to suffering.

133
00:09:23,000 --> 00:09:27,010
 It's wrong because it leads to suffering and there's no

134
00:09:27,010 --> 00:09:31,300
 distinction that can be made between the suffering for

135
00:09:31,300 --> 00:09:32,000
 oneself

136
00:09:32,000 --> 00:09:35,380
 or the suffering for another because in your mind there's

137
00:09:35,380 --> 00:09:38,980
 an awareness, there's an understanding that this is a cause

138
00:09:38,980 --> 00:09:40,000
 for suffering.

139
00:09:40,000 --> 00:09:44,000
 There is that.

140
00:09:44,000 --> 00:09:51,980
 So it necessarily involves anger and it involves an wholes

141
00:09:51,980 --> 00:09:54,000
ome mind state

142
00:09:54,000 --> 00:09:59,000
 and leads you to be susceptible to harm yourself.

143
00:09:59,000 --> 00:10:09,050
 When you harm others, I mean the practical fallout is that

144
00:10:09,050 --> 00:10:16,000
 you'll be afraid and you'll put yourself in a situation

145
00:10:16,000 --> 00:10:21,000
 where you're paranoid about retribution.

146
00:10:21,000 --> 00:10:24,000
 When you die it will be an obsession in your mind.

147
00:10:24,000 --> 00:10:27,440
 It's the kind of thing that comes back to haunt you when

148
00:10:27,440 --> 00:10:30,000
 you harm the harm you've done to others.

149
00:10:30,000 --> 00:10:34,250
 And so when you die you end up with that as part of your

150
00:10:34,250 --> 00:10:38,390
 rebirth, with that as part of who you are, potentially

151
00:10:38,390 --> 00:10:39,000
 leading you to hell even

152
00:10:39,000 --> 00:10:43,990
 but more likely just to lead you to a situation, an abusive

153
00:10:43,990 --> 00:10:45,000
 situation.

154
00:10:45,000 --> 00:10:52,180
 Often this is what leads to these cycles of retribution

155
00:10:52,180 --> 00:10:56,550
 where one person is, one person harms another, then they're

156
00:10:56,550 --> 00:10:57,000
 both reborn

157
00:10:57,000 --> 00:10:59,940
 and the other person, one person is the aggressor, the

158
00:10:59,940 --> 00:11:03,000
 other is the victim and it cycles.

159
00:11:03,000 --> 00:11:05,640
 This kind of thing can happen because of the psychology of

160
00:11:05,640 --> 00:11:06,000
 it.

161
00:11:06,000 --> 00:11:11,000
 You harm others and you pervert your own situation.

162
00:11:11,000 --> 00:11:16,000
 There's a sense that that is how karma works.

163
00:11:16,000 --> 00:11:21,780
 It's the kind of thing that we look into, that we discover

164
00:11:21,780 --> 00:11:30,000
 through our practice, how these things affect our mind.

165
00:11:30,000 --> 00:11:32,340
 And so it's not just a pretty saying, "Yes, don't harm

166
00:11:32,340 --> 00:11:35,000
 others because you wouldn't want that to happen to you."

167
00:11:35,000 --> 00:11:40,300
 There actually is some depth to it, that in fact that's

168
00:11:40,300 --> 00:11:45,000
 very much an intrinsic part of how the mind works.

169
00:11:45,000 --> 00:11:49,300
 So, I don't have too much to talk about because it's very

170
00:11:49,300 --> 00:11:53,000
 similar but we're continuing on.

171
00:11:53,000 --> 00:11:58,000
 We've started the 10th chapter, this is verse number 2.

172
00:11:58,000 --> 00:12:05,000
 So we're on next to number 131. Thank you all for tuning in

173
00:12:05,000 --> 00:12:05,000
. I wish you all the best.

174
00:12:05,000 --> 00:12:34,000
 [silence]

175
00:12:34,000 --> 00:12:39,590
 Does anyone have any questions? Let me pull up the

176
00:12:39,590 --> 00:12:44,000
 meditation page.

177
00:12:44,000 --> 00:13:04,000
 We're just doing audio tonight.

178
00:13:04,000 --> 00:13:19,000
 [silence]

179
00:13:19,000 --> 00:13:22,120
 The Americans are going to move to Canada after the

180
00:13:22,120 --> 00:13:23,000
 election.

181
00:13:23,000 --> 00:13:27,000
 Yeah, it looks kind of scary.

182
00:13:27,000 --> 00:13:30,430
 But you know, America's been a scary place for quite some

183
00:13:30,430 --> 00:13:31,000
 time.

184
00:13:31,000 --> 00:13:35,000
 America has a very bad history if you believe what they say

185
00:13:35,000 --> 00:13:39,000
 and it's not really hard to believe.

186
00:13:39,000 --> 00:13:43,930
 Well, it's pretty clear what the US has done to Central

187
00:13:43,930 --> 00:13:51,000
 America, South and Central America.

188
00:13:51,000 --> 00:13:54,900
 Yeah, what they've done around the world. What they've done

189
00:13:54,900 --> 00:13:56,000
 to the Middle East.

190
00:13:56,000 --> 00:14:02,160
 Why the Middle Eastern people really dislike Americans a

191
00:14:02,160 --> 00:14:03,000
 lot.

192
00:14:03,000 --> 00:14:10,190
 They have some reason to dislike your country. So you're

193
00:14:10,190 --> 00:14:16,000
 all welcome in Canada.

194
00:14:16,000 --> 00:14:22,000
 I mean, from my perspective anyway.

195
00:14:22,000 --> 00:14:26,040
 But I wouldn't be too concerned. It's not like it's going

196
00:14:26,040 --> 00:14:29,000
 to get really, really bad right away.

197
00:14:29,000 --> 00:14:38,050
 I mean, what's great about America is there are people who

198
00:14:38,050 --> 00:14:44,000
 are young and have a new mindset

199
00:14:44,000 --> 00:14:55,000
 and appear to be rather empathic, empathetic, sympathetic.

200
00:14:55,000 --> 00:14:59,160
 At the very least appear to be fairly global in their

201
00:14:59,160 --> 00:15:07,000
 thinking in terms of unprejudiced.

202
00:15:07,000 --> 00:15:14,000
 Anyway, real question. Does consciousness correlate with

203
00:15:14,000 --> 00:15:14,000
 defilement?

204
00:15:14,000 --> 00:15:16,460
 Does a person's thoughts, speech and conduct are thoroughly

205
00:15:16,460 --> 00:15:18,000
 defiled? Is that person thoroughly unconscious?

206
00:15:18,000 --> 00:15:26,000
 Person's thoughts, speech and conduct are more conscious?

207
00:15:26,000 --> 00:15:31,860
 No, there's no sense of that. It depends, I guess, what you

208
00:15:31,860 --> 00:15:33,000
 mean by conscious.

209
00:15:33,000 --> 00:15:39,970
 By that you mean mindful. That's usually the word we use to

210
00:15:39,970 --> 00:15:42,000
 describe someone.

211
00:15:42,000 --> 00:15:48,860
 But I suppose, but it's kind of more poetic way, or what's

212
00:15:48,860 --> 00:15:52,000
 the word, figurative?

213
00:15:52,000 --> 00:15:56,480
 Not literally, or in an ultimate sense, it's more of a

214
00:15:56,480 --> 00:15:59,000
 conventional way of speaking.

215
00:15:59,000 --> 00:16:02,420
 Like the Buddha said, the mindful are as though already

216
00:16:02,420 --> 00:16:03,000
 dead.

217
00:16:03,000 --> 00:16:05,000
 That's really the truth. If you've never meditated before,

218
00:16:05,000 --> 00:16:08,000
 if you've never really practiced being mindful,

219
00:16:08,000 --> 00:16:11,000
 it's like going through life asleep.

220
00:16:11,000 --> 00:16:14,400
 When you first start to meditate, it's like waking up. It's

221
00:16:14,400 --> 00:16:18,000
 like a splash of cold water on your face.

222
00:16:18,000 --> 00:16:21,640
 So the quality of consciousness, from a technical point of

223
00:16:21,640 --> 00:16:23,000
 view, it's not that you're less conscious,

224
00:16:23,000 --> 00:16:29,980
 it's that the quality of the consciousness is fairly low

225
00:16:29,980 --> 00:16:32,000
 for someone who has not meditated.

226
00:16:32,000 --> 00:16:37,250
 It's fairly weak, you can say, but it's not less conscious,

227
00:16:37,250 --> 00:16:39,000
 technically speaking,

228
00:16:39,000 --> 00:16:44,250
 though in one manner of speaking it is, I think, proper to

229
00:16:44,250 --> 00:16:45,000
 say.

230
00:16:45,000 --> 00:16:52,000
 It's possible to say it that way.

231
00:16:52,000 --> 00:16:54,000
 It's like spiritually conscious.

232
00:16:54,000 --> 00:16:57,710
 Just generally, I think it's fine to say less conscious,

233
00:16:57,710 --> 00:16:59,000
 more conscious.

234
00:16:59,000 --> 00:17:02,780
 That's how we use the word in English, conscious of what

235
00:17:02,780 --> 00:17:04,000
 you're doing.

236
00:17:04,000 --> 00:17:07,240
 Think of a drunk person. A drunk person is not very

237
00:17:07,240 --> 00:17:10,000
 conscious of what they're doing, they're conscious.

238
00:17:10,000 --> 00:17:13,460
 And from a Buddhist perspective, consciousness is still

239
00:17:13,460 --> 00:17:15,000
 present.

240
00:17:15,000 --> 00:17:21,650
 They may be less conscious. They're certainly less

241
00:17:21,650 --> 00:17:23,000
 conscientious.

242
00:17:23,000 --> 00:17:28,000
 Sangha has a question.

243
00:17:28,000 --> 00:17:32,790
 Can we say that tanha is the liking of the sensation, as up

244
00:17:32,790 --> 00:17:37,000
adhana is wanting it back?

245
00:17:37,000 --> 00:17:42,000
 No, tanha and upadhana are technically the same thing.

246
00:17:42,000 --> 00:17:50,000
 Tanha is just craving and upadhana is stronger craving.

247
00:17:50,000 --> 00:17:58,400
 But this is why paticca-sumupada really gets too much focus

248
00:17:58,400 --> 00:17:59,000
.

249
00:17:59,000 --> 00:18:06,020
 In one sense, people look to it as sort of like the be-all

250
00:18:06,020 --> 00:18:09,000
 end-all of doctrine when it's not.

251
00:18:09,000 --> 00:18:15,000
 It's fairly conventional in the way it presents things.

252
00:18:15,000 --> 00:18:18,640
 It's deep and it's profound and it's important, but it's

253
00:18:18,640 --> 00:18:21,000
 not the ultimate explanation.

254
00:18:21,000 --> 00:18:24,000
 The mahāpatāna is.

255
00:18:24,000 --> 00:18:30,000
 That explanation takes like 400 cartloads of books.

256
00:18:30,000 --> 00:18:35,630
 But what the Buddha is saying here is that craving leads to

257
00:18:35,630 --> 00:18:37,000
 clinging.

258
00:18:37,000 --> 00:18:41,000
 When you want something, you cling to it.

259
00:18:41,000 --> 00:18:44,170
 But technically, the mind is doing the same thing whether

260
00:18:44,170 --> 00:18:46,000
 it wants or whether it clings.

261
00:18:46,000 --> 00:18:49,000
 So it's a conventional manner of speech.

262
00:18:49,000 --> 00:18:52,320
 There's no difference between dhanā and upadhana, from my

263
00:18:52,320 --> 00:18:54,000
 understanding, except,

264
00:18:54,000 --> 00:18:57,210
 and I'm pretty sure this is how the commentary explains it,

265
00:18:57,210 --> 00:18:59,000
 except in terms of the intensity.

266
00:18:59,000 --> 00:19:01,000
 Upadhana just is more intense.

267
00:19:01,000 --> 00:19:04,330
 But conventionally, when we talk about it, we talk about

268
00:19:04,330 --> 00:19:05,000
 craving and clinging.

269
00:19:05,000 --> 00:19:08,000
 When you crave something, you'll cling to it.

270
00:19:08,000 --> 00:19:10,880
 But the mind that craves and the mind that clings are

271
00:19:10,880 --> 00:19:13,000
 actually technically the same.

272
00:19:13,000 --> 00:19:16,000
 It's the same mind.

273
00:19:16,000 --> 00:19:21,100
 So vedicca-sumupāda is fairly conventional in its

274
00:19:21,100 --> 00:19:23,000
 presentation.

275
00:19:23,000 --> 00:19:26,260
 Much of it is ultimate and it's talking about ultimate

276
00:19:26,260 --> 00:19:27,000
 realities,

277
00:19:27,000 --> 00:19:31,000
 but it's not the ultimate explanation.

278
00:19:31,000 --> 00:19:33,000
 The abhidhamma is.

279
00:19:33,000 --> 00:19:36,000
 That's what the abhidhamma is for.

280
00:19:36,000 --> 00:19:49,000
 Anyway, so are we.

281
00:19:49,000 --> 00:19:53,000
 We all caught up.

282
00:19:53,000 --> 00:19:56,000
 Not too many questions.

283
00:19:56,000 --> 00:19:58,000
 What am I doing these days?

284
00:19:58,000 --> 00:20:01,000
 We have a couple of meditators here.

285
00:20:01,000 --> 00:20:04,340
 I mean, Michael is doing a course, and then Mark from

286
00:20:04,340 --> 00:20:05,000
 Germany,

287
00:20:05,000 --> 00:20:09,000
 and Elaine, a local woman, has been coming by.

288
00:20:09,000 --> 00:20:13,000
 She's coming by again tomorrow morning.

289
00:20:13,000 --> 00:20:18,360
 She's been quite interested, and she calls me on the phone

290
00:20:18,360 --> 00:20:19,000
 every so often,

291
00:20:19,000 --> 00:20:22,000
 which is great. It's nice to have.

292
00:20:22,000 --> 00:20:29,000
 To be able to help, she's going through some stress.

293
00:20:30,000 --> 00:20:37,000
 I'm studying Sanskrit, but I'm hoping to...

294
00:20:37,000 --> 00:20:40,000
 I've got a Sanskrit class in the fall, I think,

295
00:20:40,000 --> 00:20:45,000
 so I've got to review my 13-year-old knowledge.

296
00:20:45,000 --> 00:20:50,000
 How long ago did I study Sanskrit many years ago?

297
00:20:52,000 --> 00:20:58,360
 I'm hoping to work with some people to make this, to redo

298
00:20:58,360 --> 00:21:01,000
 our meditation.sarimangalho.org.

299
00:21:01,000 --> 00:21:03,000
 Hopefully we can do that soon.

300
00:21:03,000 --> 00:21:06,000
 This month, this month would be the time.

301
00:21:06,000 --> 00:21:09,000
 We can also work on it over the summer.

302
00:21:09,000 --> 00:21:13,000
 I'm also going back to New York.

303
00:21:13,000 --> 00:21:16,000
 We're going to visit Bhikkhu Bodhi.

304
00:21:16,000 --> 00:21:20,160
 I've been corralled into trying to persuade him to come to

305
00:21:20,160 --> 00:21:22,000
 Canada to give a talk.

306
00:21:22,000 --> 00:21:24,000
 Let's see how that goes.

307
00:21:24,000 --> 00:21:27,330
 I've never met him, actually, so it would be nice to go

308
00:21:27,330 --> 00:21:28,000
 visit.

309
00:21:28,000 --> 00:21:43,000
 Oh, you have a question.

310
00:21:43,000 --> 00:21:46,000
 "Crossing into Canada from the States."

311
00:21:46,000 --> 00:21:49,000
 I don't have a passport.

312
00:21:49,000 --> 00:21:52,000
 You need a passport, I think.

313
00:21:52,000 --> 00:21:57,000
 Pretty sure you can get one.

314
00:21:57,000 --> 00:22:01,230
 It might be difficult, but I think it's your legal right to

315
00:22:01,230 --> 00:22:02,000
 get one.

316
00:22:02,000 --> 00:22:05,000
 I don't know. I would assume.

317
00:22:05,000 --> 00:22:12,000
 Yeah, in Carmel.

318
00:22:13,000 --> 00:22:16,190
 There's a monk here who wants to invite him to give a talk,

319
00:22:16,190 --> 00:22:18,000
 and he wants my help.

320
00:22:18,000 --> 00:22:21,000
 He invited him once, and he refused.

321
00:22:21,000 --> 00:22:23,000
 I said, "Well, you should go see him."

322
00:22:23,000 --> 00:22:25,000
 He said, "Okay, well, you come with me."

323
00:22:25,000 --> 00:22:28,000
 It looks like I'm going with him.

324
00:22:28,000 --> 00:22:31,000
 He just called me from Winnipeg. He's a really nice guy.

325
00:22:31,000 --> 00:22:33,000
 Good friend.

326
00:22:33,000 --> 00:22:38,000
 I think he follows the Lotus Sutra, unfortunately.

327
00:22:38,000 --> 00:22:42,000
 It's a little bit disconcerting, because as it turns out,

328
00:22:42,000 --> 00:22:43,000
 our understanding of Buddhism is quite different.

329
00:22:43,000 --> 00:22:46,000
 But it doesn't mean we can't be friends.

330
00:23:06,000 --> 00:23:09,000
 Anyway, I guess that's all for tonight.

331
00:23:09,000 --> 00:23:12,000
 Have a good night, everyone.

332
00:23:13,000 --> 00:23:15,000
 Thank you.

333
00:23:16,000 --> 00:23:17,000
 [

