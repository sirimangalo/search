1
00:00:00,000 --> 00:00:04,840
 If we go to a meditation course, e.g. in Sri Lanka, do we

2
00:00:04,840 --> 00:00:07,000
 meditate 24/7?

3
00:00:07,000 --> 00:00:11,000
 No.

4
00:00:11,000 --> 00:00:15,000
 No, I had an anecdote about this one.

5
00:00:15,000 --> 00:00:20,000
 I was teaching... I was a substitute teacher

6
00:00:20,000 --> 00:00:27,000
 in a meditation center in Chiang Mai once.

7
00:00:27,000 --> 00:00:31,990
 I got the chance to sub for the head monk who teaches

8
00:00:31,990 --> 00:00:33,000
 foreigners.

9
00:00:33,000 --> 00:00:41,220
 And I found out that his way of teaching was based on

10
00:00:41,220 --> 00:00:44,000
 another teacher's method

11
00:00:44,000 --> 00:00:48,000
 under our head teacher who taught that...

12
00:00:48,000 --> 00:00:52,730
 this monk taught that you should do a certain number of

13
00:00:52,730 --> 00:00:54,000
 hours a day.

14
00:00:56,000 --> 00:00:58,000
 So in the beginning it would be eight hours,

15
00:00:58,000 --> 00:01:01,000
 and in the beginning I think it would be like six hours,

16
00:01:01,000 --> 00:01:03,000
 six hours of meditation a day.

17
00:01:03,000 --> 00:01:07,120
 Then the next day or after a couple of days he'd say, "

18
00:01:07,120 --> 00:01:08,000
Today two more hours."

19
00:01:08,000 --> 00:01:11,000
 So today eight hours of meditation.

20
00:01:11,000 --> 00:01:16,000
 And they were instructed that every day they should count

21
00:01:16,000 --> 00:01:19,000
 how many hours they actually meditated

22
00:01:19,000 --> 00:01:23,000
 and report to the teacher how many hours they meditated.

23
00:01:23,000 --> 00:01:27,000
 So when I got there I found all these foreign meditators...

24
00:01:27,000 --> 00:01:31,000
 I have to say, totally stressed out

25
00:01:31,000 --> 00:01:36,000
 and either feeling proud of themselves because they made

26
00:01:36,000 --> 00:01:37,000
 the number of hours

27
00:01:37,000 --> 00:01:39,550
 that they were supposed to do, whether it was eight, ten,

28
00:01:39,550 --> 00:01:42,000
 twelve, fourteen, sixteen, whatever,

29
00:01:42,000 --> 00:01:48,610
 or else feeling depressed and upset because they didn't

30
00:01:48,610 --> 00:01:52,000
 make the formal meditation.

31
00:01:53,000 --> 00:01:56,000
 They didn't make the number of hours of formal meditation.

32
00:01:56,000 --> 00:01:59,960
 So they were supposed to do eight hours of formal

33
00:01:59,960 --> 00:02:01,000
 meditation

34
00:02:01,000 --> 00:02:04,000
 and they only did seven or they only did six or so on.

35
00:02:04,000 --> 00:02:09,000
 So at first the assistant was asking me,

36
00:02:09,000 --> 00:02:10,960
 "How many hours are you going to give them? How many hours

37
00:02:10,960 --> 00:02:12,000
 are you going to give them?"

38
00:02:12,000 --> 00:02:14,000
 Because she knew that you're supposed to give them hours,

39
00:02:14,000 --> 00:02:22,000
 and she was afraid that I was changing the teaching format.

40
00:02:22,000 --> 00:02:24,730
 So she asked me, "How many hours are you going to give them

41
00:02:24,730 --> 00:02:25,000
 today?"

42
00:02:25,000 --> 00:02:28,000
 This guy had done eight hours, I think.

43
00:02:28,000 --> 00:02:32,000
 And he said to me, "Today I was supposed to do eight hours

44
00:02:32,000 --> 00:02:33,000
 and I did eight hours."

45
00:02:33,000 --> 00:02:37,000
 And he was clearly stressed about this.

46
00:02:37,000 --> 00:02:39,000
 This was not easy for him.

47
00:02:39,000 --> 00:02:42,000
 Eight hours isn't so difficult,

48
00:02:42,000 --> 00:02:45,000
 but he was having a lot of trouble.

49
00:02:45,000 --> 00:02:49,000
 So I looked him in the eye and I said, "Very good.

50
00:02:49,000 --> 00:02:53,000
 Today I want you to do eighteen hours of meditation."

51
00:02:53,000 --> 00:02:58,000
 And his eyes got so wide.

52
00:02:58,000 --> 00:03:01,840
 And his stress level, his blood pressure must have gone

53
00:03:01,840 --> 00:03:03,000
 through the roof.

54
00:03:03,000 --> 00:03:08,000
 And I smiled at him.

55
00:03:09,000 --> 00:03:13,000
 And I said to him, "Here's what I want you to do.

56
00:03:13,000 --> 00:03:19,130
 When you get up in the morning, I want you to sleep not

57
00:03:19,130 --> 00:03:20,000
 more than six hours.

58
00:03:20,000 --> 00:03:23,880
 And as soon as you get up in the morning, I want you to say

59
00:03:23,880 --> 00:03:24,000
,

60
00:03:24,000 --> 00:03:28,550
 'Lying, lying, maybe rising, falling, if you notice the

61
00:03:28,550 --> 00:03:29,000
 stomach.'

62
00:03:29,000 --> 00:03:33,000
 When you sit up, I want you to say, 'Sitting, sitting.'

63
00:03:33,000 --> 00:03:36,000
 When you want to sit up, you can say, 'Wanting, wanting.'

64
00:03:36,000 --> 00:03:39,210
 When you stand up, you can say, 'Wanting to stand, wanting

65
00:03:39,210 --> 00:03:40,000
 to stand.'

66
00:03:40,000 --> 00:03:43,000
 When you go to the washroom, walking, walking.

67
00:03:43,000 --> 00:03:45,000
 When you brush your teeth, brushing, brushing.

68
00:03:45,000 --> 00:03:48,000
 When you eat your food, chewing, chewing.

69
00:03:48,000 --> 00:03:52,000
 I want you to be mindful like this throughout the day.

70
00:03:52,000 --> 00:03:58,000
 Practice as much form of meditation as you can comfortably.

71
00:03:58,000 --> 00:04:01,670
 But don't tell me how many hours a day of form of

72
00:04:01,670 --> 00:04:03,000
 meditation you did.

73
00:04:04,000 --> 00:04:08,000
 I want you to be mindful from the moment you wake up

74
00:04:08,000 --> 00:04:11,000
 to the moment you fall asleep 18 hours later.

75
00:04:11,000 --> 00:04:14,950
 And it doesn't mean you're always mindful, but it means you

76
00:04:14,950 --> 00:04:16,000
're practicing.

77
00:04:16,000 --> 00:04:20,790
 So that's meditation. No, you're trying to be mindful for

78
00:04:20,790 --> 00:04:22,000
 18 hours.

79
00:04:22,000 --> 00:04:25,000
 And so you have six hours of break.

80
00:04:25,000 --> 00:04:28,180
 For six hours you can sleep, and during that time you're

81
00:04:28,180 --> 00:04:29,000
 not meditating.

82
00:04:29,000 --> 00:04:32,000
 And he was actually quite relieved by this.

83
00:04:32,000 --> 00:04:34,000
 It was something quite doable.

84
00:04:34,000 --> 00:04:39,000
 The other thing it did was, when I arrived there,

85
00:04:39,000 --> 00:04:41,500
 I had been there for some while before, and I was watching

86
00:04:41,500 --> 00:04:42,000
 the form of meditators,

87
00:04:42,000 --> 00:04:44,000
 and they weren't meditating.

88
00:04:44,000 --> 00:04:47,170
 They would do their power meditation, one hour walking, one

89
00:04:47,170 --> 00:04:48,000
 hour sitting.

90
00:04:48,000 --> 00:04:51,000
 But when they weren't doing the form of meditation,

91
00:04:51,000 --> 00:04:58,000
 they would sit around chatting, or they would wander around

92
00:04:58,000 --> 00:05:00,000
 and look at the...

93
00:05:01,000 --> 00:05:06,760
 the flora and the fauna, and watch the novices playing

94
00:05:06,760 --> 00:05:08,000
 games with each other,

95
00:05:08,000 --> 00:05:11,000
 and driving the tractors and so on.

96
00:05:11,000 --> 00:05:13,000
 The novices driving the tractors.

97
00:05:13,000 --> 00:05:17,000
 But they weren't being mindful.

98
00:05:17,000 --> 00:05:20,260
 And so I just kicked, but when I got them to do this 18-

99
00:05:20,260 --> 00:05:21,000
hour thing,

100
00:05:21,000 --> 00:05:26,510
 suddenly it was, you know, the assistant who looked after

101
00:05:26,510 --> 00:05:27,000
 the foreigners,

102
00:05:27,000 --> 00:05:31,200
 he said, "Yeah, suddenly, they're not causing trouble

103
00:05:31,200 --> 00:05:32,000
 anymore.

104
00:05:32,000 --> 00:05:35,680
 I don't have to watch them all the time, because they're

105
00:05:35,680 --> 00:05:37,000
 too busy trying to be mindful.

106
00:05:37,000 --> 00:05:39,000
 They don't have any time to talk."

107
00:05:39,000 --> 00:05:43,000
 And that was part of, I think it was part of my instruction

108
00:05:43,000 --> 00:05:43,000
,

109
00:05:43,000 --> 00:05:46,000
 is that whatever you do, you're trying to be mindful.

110
00:05:46,000 --> 00:05:48,700
 So this means not talking with your friends during that

111
00:05:48,700 --> 00:05:50,000
 time that you're eating,

112
00:05:50,000 --> 00:05:52,220
 or even just sitting around trying to stay to yourself, and

113
00:05:52,220 --> 00:05:53,000
 trying to...

114
00:05:54,000 --> 00:05:58,360
 So I coached them on day-to-day mindfulness, or moment-to-

115
00:05:58,360 --> 00:05:59,000
moment mindfulness.

116
00:05:59,000 --> 00:06:02,000
 That's much more important.

117
00:06:02,000 --> 00:06:08,800
 It's much more important how much of your waking hours you

118
00:06:08,800 --> 00:06:11,000
're mindful of.

119
00:06:11,000 --> 00:06:15,000
 That doesn't really answer your question.

120
00:06:15,000 --> 00:06:19,150
 Your question was whether it ever gets to 24/7, or the

121
00:06:19,150 --> 00:06:20,000
 answer I have to give is that

122
00:06:20,000 --> 00:06:24,000
 sometimes you do practice this way, 24 hours a day.

123
00:06:24,000 --> 00:06:26,000
 I don't know about the seven part.

124
00:06:26,000 --> 00:06:30,010
 Maybe for a few days you can do 24 hours, where you're

125
00:06:30,010 --> 00:06:31,000
 trying to be mindful.

126
00:06:31,000 --> 00:06:34,860
 You can sleep, get down to four hours sleep, or not sleep

127
00:06:34,860 --> 00:06:36,000
 at all.

128
00:06:36,000 --> 00:06:39,000
 It depends on the meditator.

129
00:06:39,000 --> 00:06:42,190
 As you go on, it gets a lot easier, and you get less

130
00:06:42,190 --> 00:06:44,000
 interested in things like sleep.

131
00:06:44,000 --> 00:06:47,000
 There are monks that will go without sleeping for days,

132
00:06:47,000 --> 00:06:49,000
 weeks, months on end.

133
00:06:51,000 --> 00:06:53,560
 Because they're practicing meditation, sometimes you fall

134
00:06:53,560 --> 00:06:55,000
 asleep sitting as well.

135
00:06:55,000 --> 00:06:58,650
 If you practice all night, you may not be awake all night,

136
00:06:58,650 --> 00:07:01,000
 but you're trying, so you can count it.

137
00:07:01,000 --> 00:07:03,960
 So you sit there and you fall asleep for half an hour, an

138
00:07:03,960 --> 00:07:05,000
 hour, or so on.

139
00:07:05,000 --> 00:07:07,000
 But you can consider that trying, no?

140
00:07:07,000 --> 00:07:09,000
 It's a part of your meditative experience.

141
00:07:09,000 --> 00:07:13,800
 You're learning about your inability to control your mind

142
00:07:13,800 --> 00:07:15,000
 and your body.

143
00:07:15,000 --> 00:07:17,000
 It just shuts off by itself.

144
00:07:19,000 --> 00:07:21,800
 So I think that should give you some fairly good background

145
00:07:21,800 --> 00:07:26,240
 on how we, information on how we approach this sort of

146
00:07:26,240 --> 00:07:31,000
 question of how much you have to practice meditation.

147
00:07:31,000 --> 00:07:34,000
 I hope that answered your question.

