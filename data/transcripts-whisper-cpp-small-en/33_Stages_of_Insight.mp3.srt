1
00:00:00,000 --> 00:00:02,000
 Phages of insight

2
00:00:02,000 --> 00:00:07,700
 Question can you explain what an insight is in regards to

3
00:00:07,700 --> 00:00:08,540
 meditation?

4
00:00:08,540 --> 00:00:13,080
 Is it a thought that arises that causes you to let go or

5
00:00:13,080 --> 00:00:15,180
 does it occur at a deeper level?

6
00:00:15,180 --> 00:00:20,140
 Also when a person reaches the Sotapana state of mind, is

7
00:00:20,140 --> 00:00:22,480
 that a distinct event in time?

8
00:00:22,480 --> 00:00:25,280
 Answer what is an insight?

9
00:00:25,640 --> 00:00:29,990
 There are two kinds of insights. The first kind of insight

10
00:00:29,990 --> 00:00:34,120
 is at the intellectual level where one has an epiphany or

11
00:00:34,120 --> 00:00:39,160
 intellectual realization. The second kind of insight is a

12
00:00:39,160 --> 00:00:40,000
 meditative

13
00:00:40,000 --> 00:00:45,740
 experience of reality for what it is, a realization based

14
00:00:45,740 --> 00:00:46,200
 on

15
00:00:46,200 --> 00:00:50,520
 meditation practice. It is an experience not an

16
00:00:50,520 --> 00:00:51,880
 intellectual thought.

17
00:00:51,880 --> 00:00:55,480
 There is no need for thoughts because one sees things as

18
00:00:55,480 --> 00:00:57,720
 they are. There are 16

19
00:00:57,720 --> 00:01:02,380
 stages of insights that one realizes in the practice of

20
00:01:02,380 --> 00:01:04,600
 mindfulness meditation.

21
00:01:04,600 --> 00:01:09,240
 If one practices intensively for long enough, it can be

22
00:01:09,240 --> 00:01:11,320
 expected to go through all

23
00:01:11,320 --> 00:01:13,800
 16 of these in order.

24
00:01:13,800 --> 00:01:19,140
 One, the first stage is called Namarupa Parichadanyana.

25
00:01:19,800 --> 00:01:24,530
 insight into the separation between body and mind. At this

26
00:01:24,530 --> 00:01:27,960
 stage, a meditator sees that their being is

27
00:01:27,960 --> 00:01:32,680
 separated into two parts, one being physical and the other

28
00:01:32,680 --> 00:01:36,440
 mental. One sees that there is no self or soul.

29
00:01:36,440 --> 00:01:40,760
 Only these two realities of the physical and mental

30
00:01:40,760 --> 00:01:44,520
 phenomena that arise and sees every moment.

31
00:01:44,520 --> 00:01:47,200
 Two, the second stage,

32
00:01:47,640 --> 00:01:52,470
 Pachaya Parigahanyana is seeing the nature of the physical

33
00:01:52,470 --> 00:01:55,640
 and mental phenomena and how they work together

34
00:01:55,640 --> 00:01:58,200
 based on cause and effect.

35
00:01:58,200 --> 00:02:03,960
 Three, the third stage is called Samasananyana. This is

36
00:02:03,960 --> 00:02:06,920
 translated as knowledge of comprehension.

37
00:02:06,920 --> 00:02:10,360
 It is where one begins to see the three

38
00:02:10,360 --> 00:02:14,760
 characteristics inherent in every arisen phenomenon,

39
00:02:15,380 --> 00:02:18,880
 permanence, suffering and non-self. Why nothing in the

40
00:02:18,880 --> 00:02:22,660
 world is worth clinging to. The things that we thought were

41
00:02:22,660 --> 00:02:23,000
 permanent

42
00:02:23,000 --> 00:02:26,920
 are impermanent. The things that we thought were satisfying

43
00:02:26,920 --> 00:02:30,360
 cannot satisfy us because they are impermanent.

44
00:02:30,360 --> 00:02:34,920
 Further, they are not under our control and we cannot force

45
00:02:34,920 --> 00:02:38,920
 reality to be this way or the way we wish it to be.

46
00:02:39,560 --> 00:02:43,930
 Four, the fourth stage is Utayabaya Nyanah, knowledge of

47
00:02:43,930 --> 00:02:46,840
 arising and seizing. At this stage,

48
00:02:46,840 --> 00:02:51,880
 one is able to clearly break reality into moments of

49
00:02:51,880 --> 00:02:52,520
 experience

50
00:02:52,520 --> 00:02:57,660
 that arise and seize. Rather than seeing things as good or

51
00:02:57,660 --> 00:03:02,480
 bad, one simply sees them as arising and seizing incess

52
00:03:02,480 --> 00:03:03,000
antly.

53
00:03:03,640 --> 00:03:07,880
 Five, the fifth stage is the knowledge of cessation,

54
00:03:07,880 --> 00:03:12,040
 realizing that everything that arises must cease.

55
00:03:12,040 --> 00:03:16,800
 Focusing on cessation impresses upon the meditator the

56
00:03:16,800 --> 00:03:19,000
 reality of impermanence.

57
00:03:19,000 --> 00:03:24,590
 Six, the sixth stage is Bayanyana, knowledge of danger or

58
00:03:24,590 --> 00:03:28,680
 fearsomeness. At this stage, one begins to realize that

59
00:03:28,680 --> 00:03:31,240
 there is a problem with clinging to the

60
00:03:31,240 --> 00:03:33,060
 impermanent.

61
00:03:33,060 --> 00:03:38,220
 Seven, the seventh stage is Adinawa Nyanah, knowledge of

62
00:03:38,220 --> 00:03:40,680
 the disadvantages of clinging.

63
00:03:40,680 --> 00:03:45,650
 This is a direct result of experiencing the problem in the

64
00:03:45,650 --> 00:03:47,240
 previous stage.

65
00:03:47,240 --> 00:03:52,100
 Here, one comes to the conclusion that clinging is to blame

66
00:03:52,100 --> 00:03:54,680
 for our precarious situation.

67
00:03:54,680 --> 00:04:01,580
 Eight, the eighth stage is Nibidha Nyanah, knowledge of dis

68
00:04:01,580 --> 00:04:02,120
enchantment

69
00:04:02,840 --> 00:04:07,900
 At this stage, one becomes disenchanted with reality,

70
00:04:07,900 --> 00:04:11,160
 turning away from clinging to experiences,

71
00:04:11,160 --> 00:04:14,810
 realizing that there is nothing in the world which is

72
00:04:14,810 --> 00:04:19,240
 really unique, exceptional or special in any way.

73
00:04:19,240 --> 00:04:24,320
 One begins to see that in the end it is all just seeing,

74
00:04:24,320 --> 00:04:28,760
 hearing, smelling, tasting, feeling or thinking.

75
00:04:29,000 --> 00:04:34,290
 One begins to turn away from a risen phenomenon, realizing

76
00:04:34,290 --> 00:04:39,240
 that happiness does not lie in anything that arises.

77
00:04:39,240 --> 00:04:45,410
 Nine, the ninth stage is Munchittu Kamyatanyana, knowledge

78
00:04:45,410 --> 00:04:48,120
 of desire for release or freedom.

79
00:04:48,120 --> 00:04:53,170
 At this stage, the meditator begins to actively move away

80
00:04:53,170 --> 00:04:55,240
 from a risen experience,

81
00:04:55,720 --> 00:04:59,940
 no longer looking into external objects for peace and

82
00:04:59,940 --> 00:05:01,240
 happiness.

83
00:05:01,240 --> 00:05:07,560
 Ten, the tenth stage is Patisankha Nyanah, it involves on

84
00:05:07,560 --> 00:05:10,280
eness, objective introspection

85
00:05:10,280 --> 00:05:15,600
 based on the earlier realizations that freed the meditator

86
00:05:15,600 --> 00:05:17,240
 from active bias.

87
00:05:17,240 --> 00:05:21,790
 Patisankha means going over everything again, this time

88
00:05:21,790 --> 00:05:24,040
 with an unbiased perspective,

89
00:05:24,040 --> 00:05:28,010
 trying to see where exactly you are clinging rather than

90
00:05:28,010 --> 00:05:30,120
 where you can find happiness.

91
00:05:30,120 --> 00:05:36,030
 Eleventh, the eleventh stage is Sankarupa Kanyana,

92
00:05:36,030 --> 00:05:38,040
 knowledge of equanimity.

93
00:05:38,040 --> 00:05:42,740
 As your perception changes, instead of trying to find

94
00:05:42,740 --> 00:05:44,920
 happiness in experiences,

95
00:05:44,920 --> 00:05:49,160
 your mind begins to feel equanimous in regard to all a

96
00:05:49,160 --> 00:05:50,680
 risen phenomenon.

97
00:05:50,680 --> 00:05:54,930
 The mind in this stage is finally tuned and alert,

98
00:05:54,930 --> 00:05:59,320
 interacting with experience without reacting to it.

99
00:05:59,320 --> 00:06:02,440
 This is the pinnacle of mundane insight.

100
00:06:02,440 --> 00:06:08,910
 Twelve, after Sankarupa Kanyana, there arises several know

101
00:06:08,910 --> 00:06:10,760
ledges in succession.

102
00:06:10,760 --> 00:06:15,500
 The next one is Anuloma Nyanah, where the mind reaches its

103
00:06:15,500 --> 00:06:16,520
 finest point.

104
00:06:16,520 --> 00:06:20,680
 It is the consummation of insight where the mind is so

105
00:06:20,680 --> 00:06:21,800
 finely tuned

106
00:06:21,800 --> 00:06:25,560
 that you are able to see things exactly as they are.

107
00:06:25,560 --> 00:06:29,080
 This is the moment of clarity that we are aiming for.

108
00:06:29,080 --> 00:06:33,160
 Anuloma Nyanah means a mind that goes with the grain of

109
00:06:33,160 --> 00:06:36,280
 truth as opposed to going against the grain.

110
00:06:36,280 --> 00:06:40,120
 It means being finally in tune with the way things are.

111
00:06:40,120 --> 00:06:43,400
 This knowledge is just a brief moment in time.

112
00:06:44,280 --> 00:06:49,240
 Thirteen, the next knowledge is called Gochabh Nyanah,

113
00:06:49,240 --> 00:06:51,960
 wherein one changes one's lineage.

114
00:06:51,960 --> 00:06:56,280
 Changes of lineage means changing one's state of mind from

115
00:06:56,280 --> 00:06:58,680
 being an ordinary human being

116
00:06:58,680 --> 00:07:02,520
 to being one who has realized the ultimate truth.

117
00:07:02,520 --> 00:07:07,460
 This stage marks the division between the last moment of An

118
00:07:07,460 --> 00:07:10,200
uloma Nyanah and the moment after.

119
00:07:11,080 --> 00:07:15,320
 Fourteen, the next moment is called Maga Nyanah,

120
00:07:15,320 --> 00:07:20,070
 attainment of the path where one's mind goes inward rather

121
00:07:20,070 --> 00:07:22,440
 than following a path outward.

122
00:07:22,440 --> 00:07:26,770
 Maga Nyanah is the path inward, the first moment of freedom

123
00:07:26,770 --> 00:07:28,120
 from suffering.

124
00:07:28,120 --> 00:07:33,320
 Fifteen, the moments that follow are called Vaalanyana,

125
00:07:33,320 --> 00:07:37,390
 realizing of the fruits of the path, they have the same

126
00:07:37,390 --> 00:07:39,560
 nature as the first moment

127
00:07:39,560 --> 00:07:43,380
 but are called fruits because the first moment has already

128
00:07:43,380 --> 00:07:45,160
 brought about freedom.

129
00:07:45,160 --> 00:07:51,160
 Sixteen, the final stage is called Pachawekana Nyanah,

130
00:07:51,160 --> 00:07:54,760
 wherein one reflects upon the previous stages.

131
00:07:54,760 --> 00:07:59,270
 After emerging from the realization of Nibbana, one

132
00:07:59,270 --> 00:08:01,080
 reflects on the results,

133
00:08:01,080 --> 00:08:05,370
 seeing what has changed in the mind as a result of that

134
00:08:05,370 --> 00:08:06,440
 realizing.

135
00:08:07,000 --> 00:08:12,240
 Sotapana is attained at the moment of realizing the four

136
00:08:12,240 --> 00:08:15,160
teenth stage of insight, Maga Nyanah,

137
00:08:15,160 --> 00:08:19,150
 for the first time. Once one attains that stage, one is

138
00:08:19,150 --> 00:08:22,440
 said to be the first type of enlightenment.

139
00:08:22,440 --> 00:08:27,040
 The name for this is Sotapi Maga, realization of the path

140
00:08:27,040 --> 00:08:28,360
 of Sotapana.

141
00:08:28,360 --> 00:08:32,520
 The next moment will be attainment of Vaalanyana.

142
00:08:32,520 --> 00:08:37,680
 One becomes the second type of holy person called Sotapati

143
00:08:37,680 --> 00:08:40,200
 Phala or fruition of Sotapana.

144
00:08:40,200 --> 00:08:45,650
 From that moment on, one is considered to be a Sotapana,

145
00:08:45,650 --> 00:08:47,560
 one who has entered the stream.

146
00:08:47,560 --> 00:08:52,000
 The meaning is that these realizations have fundamentally

147
00:08:52,000 --> 00:08:55,000
 changed the person's outlook on reality.

148
00:08:55,000 --> 00:08:59,300
 There is no going back to an ordinary state of being for

149
00:08:59,300 --> 00:09:00,280
 that person.

150
00:09:00,280 --> 00:09:04,280
 They are considered to have entered the stream to freedom

151
00:09:04,280 --> 00:09:05,320
 from suffering

152
00:09:05,320 --> 00:09:10,490
 and will be born a maximum of seven more lifetimes in sams

153
00:09:10,490 --> 00:09:10,920
ara.

