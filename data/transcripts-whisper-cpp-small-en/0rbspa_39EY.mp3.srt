1
00:00:00,000 --> 00:00:09,030
 Okay, so today we have Ian, he's here from the south of the

2
00:00:09,030 --> 00:00:10,000
 border, no?

3
00:00:10,000 --> 00:00:11,000
 You're from America?

4
00:00:11,000 --> 00:00:12,000
 Virginia Beach.

5
00:00:12,000 --> 00:00:13,000
 You have to turn on the mic.

6
00:00:13,000 --> 00:00:14,000
 Did you turn the switch?

7
00:00:14,000 --> 00:00:15,000
 Okay.

8
00:00:15,000 --> 00:00:17,000
 Virginia Beach, USA.

9
00:00:17,000 --> 00:00:20,000
 Tell us about yourself.

10
00:00:20,000 --> 00:00:21,000
 Tell us about...

11
00:00:21,000 --> 00:00:23,000
 You don't need to know about me.

12
00:00:23,000 --> 00:00:25,000
 You don't have to give up personal information.

13
00:00:25,000 --> 00:00:31,240
 What we want to know is before the practice, before this

14
00:00:31,240 --> 00:00:35,000
 course, and after this course,

15
00:00:35,000 --> 00:00:40,000
 what has changed?

16
00:00:40,000 --> 00:00:46,370
 Before, I want to say I knew these things, but it was only

17
00:00:46,370 --> 00:00:48,000
 an intellect.

18
00:00:48,000 --> 00:00:51,320
 I was telling my friends, like, "Oh, you know, you're

19
00:00:51,320 --> 00:00:52,000
 suffering.

20
00:00:52,000 --> 00:00:53,000
 This is why you're suffering.

21
00:00:53,000 --> 00:00:55,000
 This is why we all suffer."

22
00:00:55,000 --> 00:01:01,050
 But after spending the time here and going step by step,

23
00:01:01,050 --> 00:01:04,000
 moment by moment, you begin

24
00:01:04,000 --> 00:01:10,000
 to know exactly why it is.

25
00:01:10,000 --> 00:01:13,820
 It's something that you can only know through direct

26
00:01:13,820 --> 00:01:15,000
 experience.

27
00:01:15,000 --> 00:01:24,000
 Words can't really explain. Language is insufficient.

28
00:01:24,000 --> 00:01:27,000
 How do you feel now?

29
00:01:27,000 --> 00:01:30,000
 What's your feeling today?

30
00:01:30,000 --> 00:01:31,000
 Been today?

31
00:01:31,000 --> 00:01:33,000
 Overwhelmed?

32
00:01:33,000 --> 00:01:40,000
 Very calm.

33
00:01:40,000 --> 00:01:42,000
 I feel like I can handle a lot more.

34
00:01:42,000 --> 00:01:44,000
 Strong.

35
00:01:44,000 --> 00:01:47,000
 Invincible on some might say.

36
00:01:47,000 --> 00:01:49,000
 No, maybe not invincible.

37
00:01:49,000 --> 00:01:52,000
 You also see one of the common things.

38
00:01:52,000 --> 00:01:55,610
 I'm not trying to put words in your mouth, but just to ask

39
00:01:55,610 --> 00:01:57,000
 about it.

40
00:01:57,000 --> 00:02:01,430
 It's also common to have a better picture of what you have

41
00:02:01,430 --> 00:02:03,000
 yet to...

42
00:02:03,000 --> 00:02:05,000
 the work you have yet to do.

43
00:02:05,000 --> 00:02:06,000
 Definitely.

44
00:02:06,000 --> 00:02:11,150
 It's been said before, but the iceberg, you know, what I

45
00:02:11,150 --> 00:02:13,000
 thought I had to do was...

46
00:02:13,000 --> 00:02:14,000
 just the tip.

47
00:02:14,000 --> 00:02:17,000
 And there's a lot more.

48
00:02:17,000 --> 00:02:20,000
 I see more work to be done.

49
00:02:20,000 --> 00:02:24,590
 You know, today I was getting my ticket and I was having to

50
00:02:24,590 --> 00:02:26,000
 get my password.

51
00:02:26,000 --> 00:02:28,000
 I was getting frustrated with that.

52
00:02:28,000 --> 00:02:32,000
 I was like, "Oh my goodness, it's still here.

53
00:02:32,000 --> 00:02:42,000
 Still things that need to be uprooted and understood."

54
00:02:42,000 --> 00:02:45,000
 Okay, well thank you, Ian.

55
00:02:45,000 --> 00:02:56,000
 Ian's actually already...

56
00:02:56,000 --> 00:03:05,000
 He's going to do the second course, though.

57
00:03:05,000 --> 00:03:09,000
 Expert camera skills.

58
00:03:09,000 --> 00:03:15,000
 This is...

59
00:03:15,000 --> 00:03:22,000
 All right.

60
00:03:22,000 --> 00:03:26,660
 Ian's sticking around, already asking about the instructor

61
00:03:26,660 --> 00:03:27,000
 course.

62
00:03:27,000 --> 00:03:32,230
 So we may actually have a candidate, first candidate here

63
00:03:32,230 --> 00:03:33,000
 in Canada.

64
00:03:33,000 --> 00:03:37,810
 I haven't taught an instructor course in meditation in a

65
00:03:37,810 --> 00:03:39,000
 long time.

66
00:03:39,000 --> 00:03:42,000
 The idea of being an instructor is to...

67
00:03:42,000 --> 00:03:44,000
 well, talk about that some other time.

68
00:03:44,000 --> 00:03:47,000
 It's not like you suddenly become a teacher,

69
00:03:47,000 --> 00:03:52,330
 but you learn the technical skills necessary to lead

70
00:03:52,330 --> 00:03:56,000
 someone through basic practice.

71
00:03:56,000 --> 00:04:00,810
 Anyway, tonight I wanted to talk about this, the benefits

72
00:04:00,810 --> 00:04:02,000
 of the practice.

73
00:04:02,000 --> 00:04:09,000
 More specifically, why we practice.

74
00:04:09,000 --> 00:04:11,000
 It's a bit of a trick question, actually.

75
00:04:11,000 --> 00:04:13,000
 I'm not actually...

76
00:04:13,000 --> 00:04:19,000
 I want to distinguish between benefits and reasons.

77
00:04:19,000 --> 00:04:23,000
 Maybe not even reasons. It's not quite the right word.

78
00:04:23,000 --> 00:04:27,000
 Because we have this problem.

79
00:04:27,000 --> 00:04:34,380
 Buddhism is about freeing ourself from desire, and yet isn

80
00:04:34,380 --> 00:04:38,000
't that a desire?

81
00:04:38,000 --> 00:04:42,000
 I mean, to some extent it's the problem with semantics.

82
00:04:42,000 --> 00:04:45,000
 But to some extent it's really not.

83
00:04:45,000 --> 00:04:48,000
 It really is an apt question.

84
00:04:48,000 --> 00:04:52,000
 Isn't the desire to be free from desire also a desire?

85
00:04:52,000 --> 00:04:58,000
 I would say, yes, it is. And therefore it's problematic.

86
00:04:58,000 --> 00:05:02,000
 So we have this dilemma.

87
00:05:02,000 --> 00:05:05,000
 We can talk about the benefits of the practice.

88
00:05:05,000 --> 00:05:07,000
 There's lots of them.

89
00:05:07,000 --> 00:05:10,740
 And they're great. They're great for encouraging meditators

90
00:05:10,740 --> 00:05:11,000
.

91
00:05:11,000 --> 00:05:14,000
 That's what they're good for.

92
00:05:14,000 --> 00:05:18,660
 So we have the five reasons why the Buddha taught Satipat

93
00:05:18,660 --> 00:05:19,000
ana.

94
00:05:19,000 --> 00:05:21,780
 He said, "What does Satipatana, what is the practice of Sat

95
00:05:21,780 --> 00:05:27,000
i, of mindfulness, what does it lead to?"

96
00:05:27,000 --> 00:05:31,000
 It leads to purity. Your mind becomes more pure.

97
00:05:31,000 --> 00:05:38,000
 Satanam wisudiya, wisudiya. Satanam wisudiya.

98
00:05:38,000 --> 00:05:45,000
 It allows you to go beyond mental illness.

99
00:05:45,000 --> 00:05:50,370
 Sorrow, lamentation, despair, depression, anxiety, all

100
00:05:50,370 --> 00:05:56,000
 these problems that we have.

101
00:05:56,000 --> 00:06:00,700
 It helps you overcome suffering, physical suffering and

102
00:06:00,700 --> 00:06:02,000
 mental suffering.

103
00:06:02,000 --> 00:06:04,910
 And it leads you to the right path. It leads you to the

104
00:06:04,910 --> 00:06:10,000
 goal, to freedom, to nibhana.

105
00:06:10,000 --> 00:06:12,310
 So we have all of these. These are great reasons to

106
00:06:12,310 --> 00:06:13,000
 practice.

107
00:06:13,000 --> 00:06:15,880
 Who wouldn't want a pure mind? Who wouldn't want to be able

108
00:06:15,880 --> 00:06:17,000
 to say that their mind is pure?

109
00:06:17,000 --> 00:06:21,000
 You don't hear people saying, "Oh, I'm so glad I'm such a

110
00:06:21,000 --> 00:06:23,000
 corrupt individual."

111
00:06:23,000 --> 00:06:25,000
 I don't know. Maybe such people do exist.

112
00:06:25,000 --> 00:06:30,000
 I don't meet such people. I'm blessed to meet mostly people

113
00:06:30,000 --> 00:06:35,000
 who are very much interested in a pure mind.

114
00:06:35,000 --> 00:06:39,000
 And so on. Being free from suffering sounds great.

115
00:06:39,000 --> 00:06:43,580
 But the problem is when you cling to it, when you even

116
00:06:43,580 --> 00:06:47,000
 strive for it, you get stuck.

117
00:06:47,000 --> 00:06:49,330
 Because you're clinging, and clinging is the cause of

118
00:06:49,330 --> 00:06:50,000
 suffering.

119
00:06:50,000 --> 00:06:56,000
 You're craving, and craving is the cause of suffering.

120
00:06:56,000 --> 00:06:59,000
 Ajahn Tong always talks about four benefits.

121
00:06:59,000 --> 00:07:01,020
 These are good four things to tell those of you who have

122
00:07:01,020 --> 00:07:02,000
 finished the course.

123
00:07:02,000 --> 00:07:05,000
 So listen up, Ian, this is for you.

124
00:07:05,000 --> 00:07:09,170
 There are four things that you get from the practice, from

125
00:07:09,170 --> 00:07:10,000
 doing, let's say, the foundation course,

126
00:07:10,000 --> 00:07:14,000
 or a foundation course in insight meditation.

127
00:07:14,000 --> 00:07:18,080
 The first is mindfulness. Mindfulness is actually a goal.

128
00:07:18,080 --> 00:07:21,000
 It's a great, wonderful outcome.

129
00:07:21,000 --> 00:07:23,000
 This is what I meant by invincible.

130
00:07:23,000 --> 00:07:28,410
 No, you're not invincible, but when you're mindful, that

131
00:07:28,410 --> 00:07:32,000
 mind state is an invincible mind state.

132
00:07:32,000 --> 00:07:36,000
 It's an incredibly powerful tool.

133
00:07:36,000 --> 00:07:40,660
 Because suddenly there's no you, there's nothing to be hurt

134
00:07:40,660 --> 00:07:41,000
.

135
00:07:41,000 --> 00:07:47,000
 There's no problem.

136
00:07:47,000 --> 00:07:55,080
 Problems disappear. There's only events, there's only

137
00:07:55,080 --> 00:07:58,000
 experiences.

138
00:07:58,000 --> 00:08:02,500
 You know, if I were giving a talk in real life, we wouldn't

139
00:08:02,500 --> 00:08:05,000
 allow for all this chatter.

140
00:08:05,000 --> 00:08:09,750
 In Sri Lanka, they're really all about asking questions

141
00:08:09,750 --> 00:08:12,000
 during the Dhamma talk.

142
00:08:12,000 --> 00:08:14,780
 But save the chatter for after, please. I don't mind, but

143
00:08:14,780 --> 00:08:18,000
 it's got to be distracting for the audience.

144
00:08:18,000 --> 00:08:22,000
 Sorry, on second life, they're chattering.

145
00:08:22,000 --> 00:08:25,000
 It's all wholesome. But let's save the talk for afterwards.

146
00:08:25,000 --> 00:08:28,750
 There's a time, "Kali na Dhamma Savanang", there's a time

147
00:08:28,750 --> 00:08:30,000
 to listen to the Dhamma.

148
00:08:30,000 --> 00:08:33,010
 "Kali na Dhamma Sagatya", there's a time to talk about,

149
00:08:33,010 --> 00:08:47,000
 discuss the Dhamma, discussion after.

150
00:08:47,000 --> 00:08:50,340
 So sati, mindfulness is the first benefit. It's actually a

151
00:08:50,340 --> 00:08:51,000
 benefit of the practice.

152
00:08:51,000 --> 00:08:55,000
 Why? Because you have this wonderful tool.

153
00:08:55,000 --> 00:09:00,420
 You can practice it throughout your life. Any problems that

154
00:09:00,420 --> 00:09:06,000
 come up, you can find challenges, conflicts, anything.

155
00:09:06,000 --> 00:09:13,400
 Work, study, relationships, physical suffering, mental

156
00:09:13,400 --> 00:09:16,000
 suffering, loss.

157
00:09:16,000 --> 00:09:18,940
 Mindfulness is this wonderful tool that you get walking

158
00:09:18,940 --> 00:09:20,000
 down the street.

159
00:09:20,000 --> 00:09:22,870
 What do you do? Walking, walking, and this tool that's

160
00:09:22,870 --> 00:09:24,000
 applicable everywhere.

161
00:09:24,000 --> 00:09:29,000
 Brushing your teeth is brushing, brushing.

162
00:09:29,000 --> 00:09:35,000
 The second one, in sort of a corollary, is happiness.

163
00:09:35,000 --> 00:09:38,000
 As a result of being mindful, you find happiness.

164
00:09:38,000 --> 00:09:42,000
 Ajahn Tong talks about this as heaven, going to heaven.

165
00:09:42,000 --> 00:09:46,910
 But that's just one form of happiness. Heaven is of course

166
00:09:46,910 --> 00:09:48,000
 a great happiness.

167
00:09:48,000 --> 00:09:51,000
 But it's more general than that.

168
00:09:51,000 --> 00:09:54,260
 There's so many happinesses that come from letting go, from

169
00:09:54,260 --> 00:10:03,000
 being free, from seeing clearly, from the pure mind.

170
00:10:03,000 --> 00:10:10,860
 The third is, they say upanisaya, which means you plant a

171
00:10:10,860 --> 00:10:12,000
 seed.

172
00:10:12,000 --> 00:10:16,000
 Upanisaya means different things.

173
00:10:16,000 --> 00:10:22,860
 In Thai, they talk about it as being what you bring into

174
00:10:22,860 --> 00:10:29,000
 your next life, what you carry on with you.

175
00:10:29,000 --> 00:10:34,000
 So, I describe this as a start.

176
00:10:34,000 --> 00:10:39,000
 The third benefit is you get a start on the path.

177
00:10:39,000 --> 00:10:42,250
 You're no longer a beginner meditator, you're no longer a

178
00:10:42,250 --> 00:10:49,000
 neophyte, you're no longer a luddite, a muggle.

179
00:10:49,000 --> 00:10:53,000
 Isn't this word muggle?

180
00:10:53,000 --> 00:10:58,150
 I don't know, Harry Potter has this big thing written. Is

181
00:10:58,150 --> 00:11:00,000
 it muggle? Is that the word?

182
00:11:00,000 --> 00:11:05,000
 A muggle is the right word, isn't it?

183
00:11:05,000 --> 00:11:10,000
 Muggles are, I'm told, they're non-magic folk.

184
00:11:10,000 --> 00:11:14,150
 And then there's whatever the opposite of muggle is, I don

185
00:11:14,150 --> 00:11:15,000
't know.

186
00:11:15,000 --> 00:11:20,100
 You're no longer one of those, you're no longer in the out

187
00:11:20,100 --> 00:11:21,000
 crowd.

188
00:11:21,000 --> 00:11:25,010
 It's not about belonging to a crowd, it's about having

189
00:11:25,010 --> 00:11:27,000
 something wonderful.

190
00:11:27,000 --> 00:11:32,000
 You can go and practice on your own.

191
00:11:32,000 --> 00:11:35,000
 You can continue on, find other meditation groups.

192
00:11:35,000 --> 00:11:41,000
 You are now a veteran, a graduate.

193
00:11:41,000 --> 00:11:43,000
 You've got a start.

194
00:11:43,000 --> 00:11:45,000
 And you take that with you.

195
00:11:45,000 --> 00:11:48,000
 Upanisaya, as you take it with you into your next life,

196
00:11:48,000 --> 00:11:50,000
 everything here, you forget people, places,

197
00:11:50,000 --> 00:11:53,000
 and so on.

198
00:11:53,000 --> 00:11:58,110
 But mindfulness is different, it changes your core, it

199
00:11:58,110 --> 00:11:59,000
 changes who you are.

200
00:11:59,000 --> 00:12:09,760
 It's a very deep, seated change to very basic habits of our

201
00:12:09,760 --> 00:12:11,000
 being.

202
00:12:11,000 --> 00:12:17,150
 So it changes the whole trajectory of our journey in sams

203
00:12:17,150 --> 00:12:18,000
ara.

204
00:12:18,000 --> 00:12:21,000
 And the fourth is a finish.

205
00:12:21,000 --> 00:12:24,320
 Now it's possible through the basic course that you become

206
00:12:24,320 --> 00:12:27,000
 a sotapana, this is always possible.

207
00:12:27,000 --> 00:12:32,240
 It's possible that you, through many years of practice, you

208
00:12:32,240 --> 00:12:35,500
 become a sotapana, sakatagami, anagami, arahant, it's all

209
00:12:35,500 --> 00:12:36,000
 there.

210
00:12:36,000 --> 00:12:38,000
 It's all possible.

211
00:12:38,000 --> 00:12:43,020
 It's just a question of your effort and your merit and

212
00:12:43,020 --> 00:12:47,000
 perfections, your goodness and your perfections.

213
00:12:47,000 --> 00:12:52,940
 How much goodness you have, how much stored up purity you

214
00:12:52,940 --> 00:13:00,720
 already have, just waiting to be used, put to use, and

215
00:13:00,720 --> 00:13:06,000
 actualized in the meditation practice.

216
00:13:06,000 --> 00:13:09,000
 You can become enlightened through this practice.

217
00:13:09,000 --> 00:13:14,000
 So, for very good reasons of practice.

218
00:13:14,000 --> 00:13:17,420
 But again, the problem, what if you sit there saying, "Am I

219
00:13:17,420 --> 00:13:21,100
 a sotapana yet? How do I become a sotapana? When am I going

220
00:13:21,100 --> 00:13:23,000
 to become a sotapana?

221
00:13:23,000 --> 00:13:28,000
 I want so much to become a sotapana."

222
00:13:28,000 --> 00:13:32,000
 That's a bad idea.

223
00:13:32,000 --> 00:13:35,450
 As our meditators know, when they get to the end of the

224
00:13:35,450 --> 00:13:40,000
 course, we have to make these resolves and

225
00:13:40,000 --> 00:13:46,800
 the resolve, the resolution can be such a hindrance to the

226
00:13:46,800 --> 00:13:51,000
 practice because you're waiting for it to happen,

227
00:13:51,000 --> 00:13:55,000
 expecting for something special to happen.

228
00:13:55,000 --> 00:14:02,000
 It destroys your practice.

229
00:14:02,000 --> 00:14:09,570
 So it's more than just a quandary. It speaks to the very

230
00:14:09,570 --> 00:14:14,000
 nature of samsara versus the nature of nimbana.

231
00:14:14,000 --> 00:14:18,000
 This is where this problem comes from.

232
00:14:18,000 --> 00:14:25,530
 Samsara is cause and effect. Everything in every part of

233
00:14:25,530 --> 00:14:34,740
 this universe that we know is conditionally formed, sankara

234
00:14:34,740 --> 00:14:38,000
, sankatta.

235
00:14:38,000 --> 00:14:45,710
 Everything that is conditioned, everything that is cause

236
00:14:45,710 --> 00:14:49,000
 and effect is samsara.

237
00:14:49,000 --> 00:14:54,000
 Nimbana is unconditioned. You can't bring about nimbana.

238
00:14:54,000 --> 00:14:58,000
 You can't cause it to evoke it, cause it to arise.

239
00:14:58,000 --> 00:15:03,000
 It doesn't arise. There is no arising of nimbana.

240
00:15:03,000 --> 00:15:08,700
 It is permanent. Permanence may be the wrong word, but it

241
00:15:08,700 --> 00:15:09,000
 is stable.

242
00:15:09,000 --> 00:15:17,030
 It is stable and dissatisfying. It is eternal, undying,

243
00:15:17,030 --> 00:15:20,000
 unchanging.

244
00:15:20,000 --> 00:15:25,550
 If it's unchanging, how could you cause it to come, cause

245
00:15:25,550 --> 00:15:27,000
 it to arise?

246
00:15:27,000 --> 00:15:33,880
 Because nimbana is not cause and effect, the way we

247
00:15:33,880 --> 00:15:40,250
 approach it and the way we gradually work to bring

248
00:15:40,250 --> 00:15:45,000
 ourselves closer to this, to freedom,

249
00:15:45,000 --> 00:15:48,540
 has to be quite different. It can't be cause and effect.

250
00:15:48,540 --> 00:15:53,260
 You can't be working towards some goal. Working towards

251
00:15:53,260 --> 00:15:57,000
 goals, that's samsara.

252
00:15:57,000 --> 00:15:59,270
 This is why this emphasis on the present moment, it's a

253
00:15:59,270 --> 00:16:02,000
 very good reason why we have to focus on the present moment

254
00:16:02,000 --> 00:16:02,000
,

255
00:16:02,000 --> 00:16:06,950
 because we can't be about cause and effect. We can't be

256
00:16:06,950 --> 00:16:09,000
 striving for some goal.

257
00:16:09,000 --> 00:16:12,540
 Even though we often use those words, when you practice it

258
00:16:12,540 --> 00:16:14,000
 can't be about that.

259
00:16:14,000 --> 00:16:22,370
 It has to be putting down the burden, giving up, giving up

260
00:16:22,370 --> 00:16:25,000
 our ambitions.

261
00:16:25,000 --> 00:16:31,540
 So when we look to our practice, we have to be much more

262
00:16:31,540 --> 00:16:38,730
 about the quality of the practice, the purity, the goodness

263
00:16:38,730 --> 00:16:45,000
, rather than the happiness, the peace.

264
00:16:45,000 --> 00:16:50,370
 All of the benefits that we talk about, even listening to

265
00:16:50,370 --> 00:16:55,000
 meditators describe the outcome of the practice,

266
00:16:55,000 --> 00:16:59,610
 has great encouragement and reassurance to remove your

267
00:16:59,610 --> 00:17:00,000
 doubts.

268
00:17:00,000 --> 00:17:03,120
 Yes, this is a good path, but it won't help you in the

269
00:17:03,120 --> 00:17:07,320
 practice. You can't think, "Am I there yet? Okay, what do I

270
00:17:07,320 --> 00:17:09,000
 have to do to become like that person?"

271
00:17:09,000 --> 00:17:16,810
 That's not how it works. And you get so caught up, hung up,

272
00:17:16,810 --> 00:17:22,000
 held up, held back, when you try to look for results.

273
00:17:22,000 --> 00:17:25,890
 "Am I getting anything out of this practice?" It's a few

274
00:17:25,890 --> 00:17:27,000
 doubts.

275
00:17:27,000 --> 00:17:29,920
 It's not that you couldn't see, it's that it wouldn't be

276
00:17:29,920 --> 00:17:31,000
 useful if you did.

277
00:17:31,000 --> 00:17:34,000
 When you're doing that, you're no longer meditating.

278
00:17:34,000 --> 00:17:38,280
 So be about quality, quality in the present moment. It's

279
00:17:38,280 --> 00:17:42,000
 like, put your nose to the grindstone, don't ever look up.

280
00:17:42,000 --> 00:17:46,000
 Do your work, your bank account fills up by itself.

281
00:17:46,000 --> 00:17:51,000
 The Dhamma checks are what do you call it? Auto deposit?

282
00:17:51,000 --> 00:17:53,000
 What's the word?

283
00:17:53,000 --> 00:17:58,000
 You get paid automatically. No need to receive a check.

284
00:17:58,000 --> 00:18:03,650
 Your bank account, your Dhamma bank fills up by itself. You

285
00:18:03,650 --> 00:18:06,000
 just do the work.

286
00:18:06,000 --> 00:18:11,000
 Direct deposit, yeah, that's it.

287
00:18:11,000 --> 00:18:15,970
 Alright. So, a little bit of talk about benefits

288
00:18:15,970 --> 00:18:17,000
 nonetheless.

289
00:18:17,000 --> 00:18:20,160
 It's always good to talk about because it's a reassurance,

290
00:18:20,160 --> 00:18:22,930
 but a reminder and a warning that this is not how we should

291
00:18:22,930 --> 00:18:24,000
 approach our practice.

292
00:18:24,000 --> 00:18:28,620
 Practice should always be about the practice itself. Why we

293
00:18:28,620 --> 00:18:30,000
 practice?

294
00:18:30,000 --> 00:18:33,890
 Why we practice is because not practicing is a cause of

295
00:18:33,890 --> 00:18:35,000
 suffering.

296
00:18:35,000 --> 00:18:38,000
 Why we practice is because we have ignorance.

297
00:18:38,000 --> 00:18:45,000
 Why we practice is because there's a problem.

298
00:18:45,000 --> 00:18:51,440
 Why we practice is it's very much about the rightness and

299
00:18:51,440 --> 00:18:57,100
 the propriety of it, the goodness of it, rather than we do

300
00:18:57,100 --> 00:18:59,000
 it for x-wall.

301
00:18:59,000 --> 00:19:04,000
 We can't always be thinking about this.

302
00:19:04,000 --> 00:19:08,580
 We have to give up the idea of motivation and being

303
00:19:08,580 --> 00:19:10,000
 motivated.

304
00:19:10,000 --> 00:19:16,000
 Our motivation, if we use the word, should be for purity,

305
00:19:16,000 --> 00:19:24,510
 for quality, for rightness, goodness here and now, and let

306
00:19:24,510 --> 00:19:26,000
 the results come.

307
00:19:26,000 --> 00:19:30,000
 It's like this whole God thing, right?

308
00:19:30,000 --> 00:19:34,310
 People say you have to do x or else God will be angry at

309
00:19:34,310 --> 00:19:35,000
 you.

310
00:19:35,000 --> 00:19:40,000
 If you do x, God will be pleased.

311
00:19:40,000 --> 00:19:43,090
 I just think, if God's going to be angry at me, isn't that

312
00:19:43,090 --> 00:19:44,000
 his problem?

313
00:19:44,000 --> 00:19:48,580
 If God's going to be happy for me, is this really a carrot

314
00:19:48,580 --> 00:19:51,000
 in the stick sort of thing?

315
00:19:51,000 --> 00:19:54,000
 What an awful way to live.

316
00:19:54,000 --> 00:20:00,000
 Just another reason to never believe in any God.

317
00:20:00,000 --> 00:20:05,000
 I think we've covered just about everything.

318
00:20:05,000 --> 00:20:07,860
 That's the time of our tonight. Thank you all for tuning in

319
00:20:07,860 --> 00:20:09,000
. Have a good night.

