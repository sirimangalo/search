1
00:00:00,000 --> 00:00:05,360
 A boy asked how do I know that I'm progressing in the

2
00:00:05,360 --> 00:00:07,000
 meditation process?

3
00:00:07,000 --> 00:00:22,100
 It's I think very easy. You will feel the difference, you

4
00:00:22,100 --> 00:00:28,000
 will feel changed more or less and you will feel ongoing

5
00:00:28,000 --> 00:00:29,000
 changes.

6
00:00:29,000 --> 00:00:37,880
 Sometimes it might feel that you are regressive, that you

7
00:00:37,880 --> 00:00:43,040
 don't advance so much anymore and you don't make any

8
00:00:43,040 --> 00:00:44,000
 progress.

9
00:00:44,000 --> 00:00:49,540
 But in general you will know that, oh, this defilement is

10
00:00:49,540 --> 00:00:55,000
 gone and that defilement is weakened.

11
00:00:55,000 --> 00:00:59,210
 Sometimes it goes in larger steps, sometimes in smaller

12
00:00:59,210 --> 00:01:00,000
 steps.

13
00:01:00,000 --> 00:01:17,690
 But when you had an insight that was really a deep insight,

14
00:01:17,690 --> 00:01:22,000
 then you will certainly know the change.

15
00:01:22,000 --> 00:01:30,000
 And you will certainly know that you had made a progress.

16
00:01:30,000 --> 00:01:38,730
 One thing that I always try to claim about the practice is

17
00:01:38,730 --> 00:01:41,000
 that everything you sit down you should learn something

18
00:01:41,000 --> 00:01:43,000
 about yourself.

19
00:01:43,000 --> 00:01:46,000
 Or in general you might say about reality.

20
00:01:46,000 --> 00:01:49,760
 So if you're practicing and practicing and never learning

21
00:01:49,760 --> 00:01:53,000
 anything, no matter how small, then you can say that,

22
00:01:53,000 --> 00:01:55,540
 well I would say that maybe you're doing something wrong

23
00:01:55,540 --> 00:02:01,280
 because it should be that every time you sit down to med

24
00:02:01,280 --> 00:02:04,000
itate you should learn something.

25
00:02:04,000 --> 00:02:13,590
 The things that we try to learn just to give an idea of

26
00:02:13,590 --> 00:02:18,000
 what we mean by progress. In brief, the things that we

27
00:02:18,000 --> 00:02:18,000
 intend to understand,

28
00:02:18,000 --> 00:02:21,020
 or the knowledge that we intend to gain from the practice,

29
00:02:21,020 --> 00:02:23,000
 there are four things.

30
00:02:23,000 --> 00:02:30,960
 There is the, or it's three things I think, knowledge of

31
00:02:30,960 --> 00:02:32,000
 body and mind.

32
00:02:32,000 --> 00:02:34,900
 So the first thing that you should gain from the practice

33
00:02:34,900 --> 00:02:41,070
 is an understanding of the reality inside of your

34
00:02:41,070 --> 00:02:44,000
 experience, inside and in the world around you.

35
00:02:44,000 --> 00:02:47,310
 So realizing that reality is made up of seeing, hearing,

36
00:02:47,310 --> 00:02:50,000
 smelling, tasting, feeling and thinking,

37
00:02:50,000 --> 00:02:53,940
 and it's made up of physical and mental experiences,

38
00:02:53,940 --> 00:02:56,000
 moments of experience.

39
00:02:56,000 --> 00:03:01,280
 The second thing is that you come to see impermanence,

40
00:03:01,280 --> 00:03:04,000
 suffering and non-self.

41
00:03:04,000 --> 00:03:09,300
 So you'll come to weaken your grasp on these things. This

42
00:03:09,300 --> 00:03:12,000
 is what leads the defilements to get weaker.

43
00:03:12,000 --> 00:03:15,090
 You'll see that the things that you cling to are not worth

44
00:03:15,090 --> 00:03:16,000
 clinging to.

45
00:03:16,000 --> 00:03:19,010
 They're impermanent, they're unsatisfying and they're

46
00:03:19,010 --> 00:03:20,000
 uncontrollable.

47
00:03:20,000 --> 00:03:25,170
 And finally, you'll come to see nirvana, which really you

48
00:03:25,170 --> 00:03:27,000
 can break it up.

49
00:03:27,000 --> 00:03:30,620
 So by saying there's four things, it's because there's the

50
00:03:30,620 --> 00:03:33,000
 path and then there's the result.

51
00:03:33,000 --> 00:03:39,550
 So the path is the weakening of the defilements or the erad

52
00:03:39,550 --> 00:03:44,730
ication of certain defilements in the case of reaching the

53
00:03:44,730 --> 00:03:49,000
 path of Sota Pandasakita Kami and the Kami Raha.

54
00:03:49,000 --> 00:03:52,690
 And the fourth is the fruition. But really it's the same

55
00:03:52,690 --> 00:03:53,000
 thing.

56
00:03:53,000 --> 00:03:57,700
 The fruition and the path take the same object and that is

57
00:03:57,700 --> 00:04:01,000
 nirvana or cessation of suffering.

58
00:04:01,000 --> 00:04:07,090
 So in the beginning of the practice, until you've realized,

59
00:04:07,090 --> 00:04:10,000
 once you've realized the third and the fourth,

60
00:04:10,000 --> 00:04:13,270
 knowledge is this kind of question doesn't really have the

61
00:04:13,270 --> 00:04:18,000
 same weight because you already know just the goal.

62
00:04:18,000 --> 00:04:23,040
 So your only work is to attain it again and again and to

63
00:04:23,040 --> 00:04:27,000
 weaken the defilements more and more.

64
00:04:27,000 --> 00:04:30,230
 But in the meantime, you should be looking to understand

65
00:04:30,230 --> 00:04:34,630
 your mind in terms of or your experience in terms of body

66
00:04:34,630 --> 00:04:37,000
 and mind and to see things as they are.

67
00:04:37,000 --> 00:04:40,540
 The more you can see that the things that you cling to are

68
00:04:40,540 --> 00:04:43,490
 not worth clinging to and how your clinging is leading to

69
00:04:43,490 --> 00:04:46,000
 suffering and so on.

70
00:04:46,000 --> 00:04:52,000
 That's the sign of more of progress in the practice.

71
00:04:52,000 --> 00:04:58,000
 Okay.

