1
00:00:00,000 --> 00:00:10,960
 Today I thought I would give a retelling of either two or

2
00:00:10,960 --> 00:00:16,000
 three of the Jataka stories.

3
00:00:16,000 --> 00:00:22,080
 Because I think these stories are quite beneficial as alleg

4
00:00:22,080 --> 00:00:29,000
ories, you could say, or as good examples or things to keep

5
00:00:29,000 --> 00:00:29,000
 in mind.

6
00:00:29,000 --> 00:00:33,520
 They're like the Aesop's fables, which we can always call

7
00:00:33,520 --> 00:00:39,230
 to mind and use as a reminder of some principle or some

8
00:00:39,230 --> 00:00:40,000
 moral.

9
00:00:40,000 --> 00:00:44,000
 But what we find about the Jataka tales is actually there.

10
00:00:44,000 --> 00:00:49,820
 They can be quite profound and often go far beyond the

11
00:00:49,820 --> 00:00:51,000
 stories of Aesop,

12
00:00:51,000 --> 00:00:57,410
 which were often cute or practical, like how to get by in

13
00:00:57,410 --> 00:00:59,000
 the world.

14
00:00:59,000 --> 00:01:04,090
 These ones are much more on the level of virtue and

15
00:01:04,090 --> 00:01:07,000
 spiritual advancement.

16
00:01:07,000 --> 00:01:10,490
 So while they have the same sort of flavor as Aesop, they

17
00:01:10,490 --> 00:01:16,000
 go a lot deeper in their treatment of morality and virtue,

18
00:01:16,000 --> 00:01:23,190
 and development of the mind and development of the

19
00:01:23,190 --> 00:01:26,000
 spiritual path.

20
00:01:26,000 --> 00:01:29,760
 So I have three here, and I'm not sure if I'll get through

21
00:01:29,760 --> 00:01:33,000
 them all in the time that I've allotted myself,

22
00:01:33,000 --> 00:01:38,000
 but I'll just go until time's up and then I'll stop.

23
00:01:38,000 --> 00:01:43,590
 So the first one is one that is quite well known among

24
00:01:43,590 --> 00:01:47,000
 Buddhist circles. It's the Nigrodamiga Jataka,

25
00:01:47,000 --> 00:01:51,710
 which is the banyan deer, I believe it's translated as Ming

26
00:01:51,710 --> 00:01:59,000
a means deer. Jataka is the past life or the birth.

27
00:01:59,000 --> 00:02:03,830
 A certain birth when the Buddha was born, the Bodhisattva

28
00:02:03,830 --> 00:02:09,040
 was born as a deer of a Nigrodha type, and I guess this is

29
00:02:09,040 --> 00:02:11,000
 a banyan deer.

30
00:02:11,000 --> 00:02:16,340
 So the story starts, keep only with the banyan deer, and

31
00:02:16,340 --> 00:02:20,000
 this is the moral which is going to come at the end.

32
00:02:20,000 --> 00:02:24,770
 So the jataka's are made up of both prose and verse, and

33
00:02:24,770 --> 00:02:28,810
 the verse is what was kept as strictly recited and

34
00:02:28,810 --> 00:02:30,000
 remembered,

35
00:02:30,000 --> 00:02:34,470
 and the prose was perhaps modified or even added to later

36
00:02:34,470 --> 00:02:35,000
 on.

37
00:02:35,000 --> 00:02:41,170
 Who knows, some of it could be exactly word for word, but

38
00:02:41,170 --> 00:02:48,000
 it seems likely that it was not recited

39
00:02:48,000 --> 00:02:52,810
 as strictly as the verses, and so it was just made up based

40
00:02:52,810 --> 00:02:57,000
 on the verses, or reconstructed based on the verses,

41
00:02:57,000 --> 00:03:00,810
 because the verses hold the core of the story. So this

42
00:03:00,810 --> 00:03:05,280
 story has one verse, and the verse is keep only with the b

43
00:03:05,280 --> 00:03:07,000
anyan deer and so on.

44
00:03:07,000 --> 00:03:10,560
 This story was told by the master, the Buddha, well in Jeth

45
00:03:10,560 --> 00:03:15,000
awana, about the mother of the elder named Prince Kasapa.

46
00:03:15,000 --> 00:03:18,280
 The daughter, we learn, of a wealthy merchant of Rajagaha

47
00:03:18,280 --> 00:03:22,080
 was deeply rooted in goodness and scorned all temporal

48
00:03:22,080 --> 00:03:23,000
 things.

49
00:03:23,000 --> 00:03:26,500
 She had reached her final existence and within her breast,

50
00:03:26,500 --> 00:03:30,340
 like a lamp in a pitcher, glowed her sure hope of winning a

51
00:03:30,340 --> 00:03:31,000
rahantship,

52
00:03:31,000 --> 00:03:35,140
 which was full enlightenment. As soon as she reached

53
00:03:35,140 --> 00:03:38,000
 knowledge of herself, as soon as she reached came of age,

54
00:03:38,000 --> 00:03:41,960
 she took no joy in a worldly life, but yearned to renounce

55
00:03:41,960 --> 00:03:43,000
 the world.

56
00:03:43,000 --> 00:03:46,550
 With this aim she said to her mother and father, "My dear

57
00:03:46,550 --> 00:03:50,000
 parents, my heart takes no joy in a worldly life.

58
00:03:50,000 --> 00:03:53,430
 Fain would I embrace the saving doctrine of the Buddha. S

59
00:03:53,430 --> 00:03:56,000
uffer me to take the vows."

60
00:03:56,000 --> 00:03:59,660
 "What, my dear? Ours is a very wealthy family and you are

61
00:03:59,660 --> 00:04:04,000
 our only daughter. You cannot take the vows."

62
00:04:04,000 --> 00:04:06,000
 This means become a nun.

63
00:04:06,000 --> 00:04:08,830
 Having failed to win her parents consent, though she asked

64
00:04:08,830 --> 00:04:12,000
 them again and again, she thought to herself,

65
00:04:12,000 --> 00:04:15,060
 "Be it so then, when I am married into another family, I

66
00:04:15,060 --> 00:04:18,000
 will gain my husband's consent and take the vows."

67
00:04:18,000 --> 00:04:22,140
 And when, being grown up, she entered another family, she

68
00:04:22,140 --> 00:04:25,190
 proved a devoted wife and lived a life of goodness and

69
00:04:25,190 --> 00:04:26,000
 virtue in her new home.

70
00:04:26,000 --> 00:04:30,570
 Now it came to pass that she conceived of a child, though

71
00:04:30,570 --> 00:04:32,000
 she knew it not.

72
00:04:32,000 --> 00:04:35,910
 There was a festival proclaimed in that city and everybody

73
00:04:35,910 --> 00:04:38,840
 kept holiday, the city being decked like a city of the gods

74
00:04:38,840 --> 00:04:39,000
.

75
00:04:39,000 --> 00:04:42,510
 But she, even at the height of the festival, neither an

76
00:04:42,510 --> 00:04:46,380
ointed herself nor put on any finery, going about in her

77
00:04:46,380 --> 00:04:48,000
 every day attire.

78
00:04:48,000 --> 00:04:51,740
 So her husband said to her, "My dear wife, everybody is in

79
00:04:51,740 --> 00:04:55,000
 holiday making, but you do not put on your bravery."

80
00:04:55,000 --> 00:04:59,200
 "My lord and master," she replied, "the body is filled with

81
00:04:59,200 --> 00:05:02,250
 two and thirty component parts. Wherefore should it be

82
00:05:02,250 --> 00:05:03,000
 adorned?"

83
00:05:03,000 --> 00:05:06,920
 This bodily frame is not of angelic or archangelic mould;

84
00:05:06,920 --> 00:05:11,000
 it is not made of gold, jewels, or yellow sandalwood.

85
00:05:11,000 --> 00:05:15,440
 It takes not its birth from the womb of a lotus flower,

86
00:05:15,440 --> 00:05:18,660
 white or red or blue. It is not filled with any immortal b

87
00:05:18,660 --> 00:05:19,000
alsam.

88
00:05:19,000 --> 00:05:25,030
 Nay, it is bread of corruption, and born of mortal parents.

89
00:05:25,030 --> 00:05:27,910
 The qualities that mark it are the wearing and wasting away

90
00:05:27,910 --> 00:05:31,000
, the decay and destruction of them merely transient.

91
00:05:31,000 --> 00:05:35,570
 It is faded to swell a graveyard, and it is devoted to lust

92
00:05:35,570 --> 00:05:39,320
s. It is the source of sorrow and the occasion of lament

93
00:05:39,320 --> 00:05:40,000
ation.

94
00:05:40,000 --> 00:05:44,260
 It is the abode of all diseases and the repository of the

95
00:05:44,260 --> 00:05:48,000
 workings of karma. Foul within, it is always excreting.

96
00:05:48,000 --> 00:05:52,820
 Yea, as all the world can see, its end is death, passing to

97
00:05:52,820 --> 00:05:58,000
 the charnel house, there to be the dwelling place of worms.

98
00:05:58,000 --> 00:06:01,420
 What should I achieve, my bridegroom, by tricking out this

99
00:06:01,420 --> 00:06:06,060
 body? Would not its adornment be like decorating the

100
00:06:06,060 --> 00:06:10,000
 outside of a clothes-stool, of a toilet?

101
00:06:10,000 --> 00:06:14,180
 My dear wife rejoined the young merchant. If you regard

102
00:06:14,180 --> 00:06:19,000
 this body as so sinful, why don't you become a nun?

103
00:06:19,000 --> 00:06:22,220
 If I am accepted, my husband, I will take the vows this

104
00:06:22,220 --> 00:06:26,460
 very day." "Very good," said he, "I will get you admitted

105
00:06:26,460 --> 00:06:28,000
 into the order."

106
00:06:28,000 --> 00:06:31,810
 And after he had shown lavish bounty and hospitality to the

107
00:06:31,810 --> 00:06:35,400
 order of monks, he escorted her with a large following to

108
00:06:35,400 --> 00:06:38,000
 the nunnery and had her admitted as a sister.

109
00:06:38,000 --> 00:06:41,950
 But of the following of Devadatta, who was the arch-enemy

110
00:06:41,950 --> 00:06:46,000
 of the Buddha and bent on creating a schism in the Sangha,

111
00:06:46,000 --> 00:06:48,850
 great was her joy at the fulfillment of her desire to

112
00:06:48,850 --> 00:06:50,000
 become a sister.

113
00:06:50,000 --> 00:06:54,150
 As her time for giving birth grew near, the sisters,

114
00:06:54,150 --> 00:06:57,950
 noticing the change in her person, the swelling in her

115
00:06:57,950 --> 00:07:01,460
 hands and feet and her increased sighs, said, "Lady, you

116
00:07:01,460 --> 00:07:05,000
 seem about to become a mother. What does it mean?"

117
00:07:05,000 --> 00:07:09,210
 "I cannot tell, ladies. I only know I have led a virtuous

118
00:07:09,210 --> 00:07:10,000
 life."

119
00:07:10,000 --> 00:07:13,640
 So the sisters brought her before Devadatta, saying, "Lord,

120
00:07:13,640 --> 00:07:17,560
 this young gentlewoman who was admitted a sister, with the

121
00:07:17,560 --> 00:07:21,400
 reluctant consent of her husband, has now proved to be with

122
00:07:21,400 --> 00:07:22,000
 child.

123
00:07:22,000 --> 00:07:25,960
 But whether this dates from before her admission to the

124
00:07:25,960 --> 00:07:30,000
 order or not, we cannot say. What are we to do now?"

125
00:07:30,000 --> 00:07:34,060
 Not being a Buddha and not having any charity, love or pity

126
00:07:34,060 --> 00:07:36,000
, Devadatta thought thus,

127
00:07:36,000 --> 00:07:39,430
 "It will be a damaging report to get abroad that one of my

128
00:07:39,430 --> 00:07:49,000
 sisters is with child, and that I condone the offence for

129
00:07:49,000 --> 00:07:51,470
 the sexual act was an offence entailing expulsion from the

130
00:07:51,470 --> 00:07:52,000
 order."

131
00:07:52,000 --> 00:07:56,150
 "My course is clear. I must expel this woman from the order

132
00:07:56,150 --> 00:08:00,230
. Without any inquiry, starting forward as if to thrust

133
00:08:00,230 --> 00:08:05,480
 aside a mast of stone, he set away and expelled this woman

134
00:08:05,480 --> 00:08:06,000
."

135
00:08:06,000 --> 00:08:09,980
 Receiving this answer, they arose and with reverent sal

136
00:08:09,980 --> 00:08:13,000
utation withdrew to their own nunnery.

137
00:08:13,000 --> 00:08:16,340
 But the girl said to those sisters, "Ladies, Devadatta the

138
00:08:16,340 --> 00:08:19,620
 elder is not the Buddha. My vows were not taken under Devad

139
00:08:19,620 --> 00:08:23,000
atta, but under the Buddha, the foremost of the world.

140
00:08:23,000 --> 00:08:27,180
 Rob me not of the vocation I won so heartily, but take me

141
00:08:27,180 --> 00:08:30,000
 before the master at Jaitavana."

142
00:08:30,000 --> 00:08:33,620
 So they set out with her for Jaitavana, and journeying over

143
00:08:33,620 --> 00:08:37,530
 the 45 leagues thither from Rajagaha, came in due course to

144
00:08:37,530 --> 00:08:40,840
 their destination, where with reverent salutation to the

145
00:08:40,840 --> 00:08:43,000
 master they laid the matter before him.

146
00:08:43,000 --> 00:08:47,460
 Thought the master, the Buddha, "I'll be at the child who

147
00:08:47,460 --> 00:08:50,630
 was conceived while she was still of the laity, yet it will

148
00:08:50,630 --> 00:08:54,000
 give the heretics an occasion to say that the ascetic Gaut

149
00:08:54,000 --> 00:08:57,000
ama has taken a sister expelled by Devadatta."

150
00:08:57,000 --> 00:09:01,120
 Therefore, to cut short such talk, this case must be heard

151
00:09:01,120 --> 00:09:04,000
 in the presence of the king and his court.

152
00:09:04,000 --> 00:09:08,120
 So on the morrow he sent for Pasenadi, king of Kosala, the

153
00:09:08,120 --> 00:09:12,520
 elder and the younger Anattapindika, the lady Wissaka, the

154
00:09:12,520 --> 00:09:16,000
 great lay disciple, and other well-known personages.

155
00:09:16,000 --> 00:09:19,620
 And in the evening, when the four classes of the faithful

156
00:09:19,620 --> 00:09:23,370
 followers were all assembled, the monks, the nuns, the lay

157
00:09:23,370 --> 00:09:25,000
men and the laywomen,

158
00:09:25,000 --> 00:09:28,890
 he said to the elder Upali, who was the expert in the

159
00:09:28,890 --> 00:09:31,000
 discipline of the monks,

160
00:09:31,000 --> 00:09:34,080
 "Go and clear up this matter of the young sister in the

161
00:09:34,080 --> 00:09:37,000
 presence of the four groups of my disciples."

162
00:09:37,000 --> 00:09:41,000
 "It shall be done," referenced Sirs, the elder.

163
00:09:41,000 --> 00:09:44,520
 And forth to the assembly he went, and there, seating

164
00:09:44,520 --> 00:09:47,830
 himself in his place, he called up Wissaka, the lay

165
00:09:47,830 --> 00:09:51,620
 disciple, inside of the king, and placed the conduct of the

166
00:09:51,620 --> 00:09:53,000
 inquiry in her hands, saying,

167
00:09:53,000 --> 00:09:57,730
 "First ascertain the precise day of the precise month on

168
00:09:57,730 --> 00:10:01,640
 which this girl joined the order," Wissaka, "and thence

169
00:10:01,640 --> 00:10:05,000
 compute whether she conceived before or since that date."

170
00:10:05,000 --> 00:10:09,590
 Accordingly, the lady had a curtain put up as a screen,

171
00:10:09,590 --> 00:10:13,000
 behind which she retired with the girl.

172
00:10:13,000 --> 00:10:22,000
 "Spektatis manibus pedibus umbilico ipso ventra puele,"

173
00:10:22,000 --> 00:10:25,000
 whatever that means, the lady found on comparing the days

174
00:10:25,000 --> 00:10:28,400
 and the months, that the conception had taken place before

175
00:10:28,400 --> 00:10:30,000
 the girl had become a sister.

176
00:10:30,000 --> 00:10:33,610
 This she reported to the elder, who proclaimed the sister

177
00:10:33,610 --> 00:10:36,000
 innocent before all the assembly.

178
00:10:36,000 --> 00:10:40,590
 And she, now that her innocence was established, reverently

179
00:10:40,590 --> 00:10:44,560
 saluted the order and the madr, and with his sisters

180
00:10:44,560 --> 00:10:47,000
 returned to her own nunnery.

181
00:10:47,000 --> 00:10:50,930
 When her time was come, she bore the sun, strong in spirit,

182
00:10:50,930 --> 00:10:54,780
 for whom she had prayed at the feet of the Buddha Padumutt

183
00:10:54,780 --> 00:10:56,000
ara, ages ago.

184
00:10:56,000 --> 00:10:58,850
 One day, when the king was passing by the nunnery, he heard

185
00:10:58,850 --> 00:11:01,650
 the cry of an infant, and asked his courtiers what it meant

186
00:11:01,650 --> 00:11:02,000
.

187
00:11:02,000 --> 00:11:05,520
 They, knowing the facts, told his majesty that the cry came

188
00:11:05,520 --> 00:11:09,000
 from the child to which the young sister had given birth.

189
00:11:09,000 --> 00:11:12,610
 "Sirs," said the king, "the care of children is a clog on

190
00:11:12,610 --> 00:11:16,630
 sisters in their religious life. Let us take charge of him

191
00:11:16,630 --> 00:11:17,000
."

192
00:11:17,000 --> 00:11:20,080
 So the infant was handed over by the king's command to the

193
00:11:20,080 --> 00:11:23,000
 ladies of his family, and brought up as a prince.

194
00:11:23,000 --> 00:11:26,770
 When the day came for him to be named, he was called Kasapa

195
00:11:26,770 --> 00:11:30,020
, but was known as Prince Kasapa because he was brought up

196
00:11:30,020 --> 00:11:31,000
 like a prince.

197
00:11:31,000 --> 00:11:33,970
 At the age of seven, he was admitted a novice under the

198
00:11:33,970 --> 00:11:37,000
 master, and a full brother when he was old enough.

199
00:11:37,000 --> 00:11:40,140
 As time went on, he waxed famous among the expounders of

200
00:11:40,140 --> 00:11:41,000
 the truth.

201
00:11:41,000 --> 00:11:45,120
 So the master gave him precedence, saying, "Brethren, the

202
00:11:45,120 --> 00:11:49,000
 first in eloquence among my disciples is Prince Kasapa."

203
00:11:49,000 --> 00:11:53,500
 Afterwards, by virtue of the Vamika sutta, he won a rahans

204
00:11:53,500 --> 00:11:54,000
hi.

205
00:11:54,000 --> 00:11:58,140
 So too, his mother, the sister, grew to clear vision and

206
00:11:58,140 --> 00:12:00,000
 won the supreme fruit.

207
00:12:00,000 --> 00:12:03,490
 Prince Kasapa, the elder, shone in the faith of the Buddha,

208
00:12:03,490 --> 00:12:06,000
 even as the full moon in the mid-heaven.

209
00:12:06,000 --> 00:12:09,700
 Now one day in the afternoon, when the tathagata, on return

210
00:12:09,700 --> 00:12:13,000
 from his alms round, had addressed the brethren,

211
00:12:13,000 --> 00:12:17,230
 he passed into his perfumed chamber. At the close of his

212
00:12:17,230 --> 00:12:20,270
 address, the brethren spent the day time either in their

213
00:12:20,270 --> 00:12:21,000
 night quarters

214
00:12:21,000 --> 00:12:23,760
 or in their day quarters till it was evening, when they

215
00:12:23,760 --> 00:12:27,000
 assembled in the Hall of Truth and spoke as follows.

216
00:12:27,000 --> 00:12:31,240
 Brethren, devadatta, because he was not a Buddha, and

217
00:12:31,240 --> 00:12:34,000
 because he had no charity, love, or pity,

218
00:12:34,000 --> 00:12:37,620
 was nigh being the ruin of the elder Prince Kasapa and his

219
00:12:37,620 --> 00:12:39,000
 Reverend Mother.

220
00:12:39,000 --> 00:12:41,690
 But the all-enlightened Buddha, being the Lord of Truth and

221
00:12:41,690 --> 00:12:46,120
 being perfect in charity, love, and pity, has proved their

222
00:12:46,120 --> 00:12:47,000
 salvation.

223
00:12:47,000 --> 00:12:51,240
 And as they sat there telling the praises of the Buddha, he

224
00:12:51,240 --> 00:12:54,000
 entered the hall with all the grace of a Buddha

225
00:12:54,000 --> 00:12:57,520
 and asked, as he took his seed, what was it they were

226
00:12:57,520 --> 00:13:00,000
 talking of as they sat together?

227
00:13:00,000 --> 00:13:05,020
 "Of your own virtues, venerable sir," said they, and told

228
00:13:05,020 --> 00:13:06,000
 him all.

229
00:13:06,000 --> 00:13:08,910
 "This is not the first time, brethren," said he, "that the

230
00:13:08,910 --> 00:13:12,000
 tathagata has proved the salvation and refuge of these two.

231
00:13:12,000 --> 00:13:16,000
 He was the same to them in the past also."

232
00:13:16,000 --> 00:13:20,280
 Then, on the brethren asking him to explain this to them,

233
00:13:20,280 --> 00:13:24,000
 he revealed what rebirth had hidden from them.

234
00:13:24,000 --> 00:13:27,000
 Once upon a time when Brahmadatta was reigning in Benares,

235
00:13:27,000 --> 00:13:31,000
 the bodhisattva was born a deer.

236
00:13:31,000 --> 00:13:35,170
 At his birth he was golden of hue, his eyes were like round

237
00:13:35,170 --> 00:13:39,000
 jewels, the sheen of his horns were as of silver,

238
00:13:39,000 --> 00:13:42,760
 his mouth was red as a bunch of scarlet cloth, his foreho

239
00:13:42,760 --> 00:13:45,000
oves were as though lacquered,

240
00:13:45,000 --> 00:13:48,510
 his tail was like the yaks, and he was as big as a young fo

241
00:13:48,510 --> 00:13:49,000
al.

242
00:13:49,000 --> 00:13:52,980
 Attended by five hundred deer he dwelt in the forest under

243
00:13:52,980 --> 00:13:55,000
 the name of King Banyan deer,

244
00:13:55,000 --> 00:13:58,310
 and hard by him dwelt another deer, also with an attendant

245
00:13:58,310 --> 00:14:02,000
 herd of five hundred deer, who was named Branch deer,

246
00:14:02,000 --> 00:14:07,000
 and was as golden of hue as the bodhisattva.

247
00:14:07,000 --> 00:14:10,440
 In those days the King of Benares was passionately fond of

248
00:14:10,440 --> 00:14:14,000
 hunting, and always had meat at every meal.

249
00:14:14,000 --> 00:14:17,500
 Every day he mustered the whole of his subjects, townsfolk

250
00:14:17,500 --> 00:14:21,210
 and countryfolk alike, to the detriment of their business,

251
00:14:21,210 --> 00:14:23,000
 and went hunting.

252
00:14:23,000 --> 00:14:26,400
 Thought his people, "This King of ours stops at all our

253
00:14:26,400 --> 00:14:27,000
 work.

254
00:14:27,000 --> 00:14:29,450
 Suppose we were to sow food and supply water for the deer

255
00:14:29,450 --> 00:14:32,000
 in his own plessence,

256
00:14:32,000 --> 00:14:35,600
 and having driven in a number of deer to bar them in and

257
00:14:35,600 --> 00:14:38,000
 deliver them over to the King."

258
00:14:38,000 --> 00:14:41,070
 So they sowed in the plessence grass for the deer to eat,

259
00:14:41,070 --> 00:14:45,080
 and supplied water for them to drink, and opened the gate

260
00:14:45,080 --> 00:14:46,000
 wide.

261
00:14:46,000 --> 00:14:49,270
 Then they called out the townsfolk and set out into the

262
00:14:49,270 --> 00:14:52,820
 forest, armed with sticks and all manners of weapons to

263
00:14:52,820 --> 00:14:54,000
 find the deer.

264
00:14:54,000 --> 00:14:57,180
 They surrounded about a league of forest in order to catch

265
00:14:57,180 --> 00:14:59,000
 the deer within their circle,

266
00:14:59,000 --> 00:15:02,080
 and in so doing surrounded the haunt of the Banyan and

267
00:15:02,080 --> 00:15:03,000
 Branch deer.

268
00:15:03,000 --> 00:15:05,860
 As soon as they perceived the deer they proceeded to beat

269
00:15:05,860 --> 00:15:09,810
 the trees, bushes, and ground with their sticks, till they

270
00:15:09,810 --> 00:15:12,000
 drove the herds out of their lairs.

271
00:15:12,000 --> 00:15:15,360
 Then they rattled their swords and spears and bows with so

272
00:15:15,360 --> 00:15:18,790
 great a din that they drove all the deer into the plessence

273
00:15:18,790 --> 00:15:21,000
, and shot the gate.

274
00:15:21,000 --> 00:15:25,770
 Then they went to the King and said, "Sire, you put a stop

275
00:15:25,770 --> 00:15:28,000
 to our work by always going hunting,

276
00:15:28,000 --> 00:15:31,140
 so we have driven deer enough from the forest to fill your

277
00:15:31,140 --> 00:15:37,000
 plessence. Henceforth feed on them."

278
00:15:37,000 --> 00:15:41,210
 Hereupon the King betook himself to the plessence, and in

279
00:15:41,210 --> 00:15:44,380
 looking over the herd saw among them two golden deer, to

280
00:15:44,380 --> 00:15:49,000
 whom he granted immunity.

281
00:15:49,000 --> 00:15:52,290
 Sometimes he would go of his own accord and shoot a deer to

282
00:15:52,290 --> 00:15:56,000
 bring home; sometimes the cook would go and shoot one.

283
00:15:56,000 --> 00:15:59,000
 At first sight of the bow the deer would dash off trembling

284
00:15:59,000 --> 00:16:02,010
 for their lives, but after receiving two or three wounds

285
00:16:02,010 --> 00:16:05,000
 they grew weary and faint and were slain.

286
00:16:05,000 --> 00:16:07,960
 The herd of deer told this to the bodhisatta who sent for

287
00:16:07,960 --> 00:16:11,340
 Branch and said, "Friend, the deer are being destroyed in

288
00:16:11,340 --> 00:16:15,280
 great numbers, and though they cannot escape death, at

289
00:16:15,280 --> 00:16:18,000
 least let them not be heedlessly wounded.

290
00:16:18,000 --> 00:16:22,170
 Let the deer go to the block by turns, one day one from my

291
00:16:22,170 --> 00:16:25,000
 herd, and next day one from yours.

292
00:16:25,000 --> 00:16:30,870
 The deer on whom the lot falls to go, to the place of

293
00:16:30,870 --> 00:16:37,000
 execution, and lie down with its head on your block, must

294
00:16:37,000 --> 00:16:37,000
 do so.

295
00:16:37,000 --> 00:16:40,790
 In this wise the deer will escape wounding. The other

296
00:16:40,790 --> 00:16:44,340
 agreed, and thenceforth the deer, whose turn it was, used

297
00:16:44,340 --> 00:16:48,000
 to go and lie down with its neck ready on the block.

298
00:16:48,000 --> 00:16:52,500
 The cook used to go and carry off only the victim which

299
00:16:52,500 --> 00:16:54,000
 awaited them.

300
00:16:54,000 --> 00:16:57,290
 Now one day the lot fell on a pregnant doe of the herd of

301
00:16:57,290 --> 00:17:01,880
 Branch, and she went to Branch and said, "Lord, I am with

302
00:17:01,880 --> 00:17:03,000
 young.

303
00:17:03,000 --> 00:17:07,170
 When I have brought forth my little one, there will be two

304
00:17:07,170 --> 00:17:11,300
 of us to take our turn. Order me to be passed over this

305
00:17:11,300 --> 00:17:12,000
 turn."

306
00:17:12,000 --> 00:17:16,080
 "No, I cannot make your turn another," said he. "You must

307
00:17:16,080 --> 00:17:20,000
 bear the consequences of your own fortune. Be gone."

308
00:17:20,000 --> 00:17:23,910
 Finding no favor with him, the doe went on to the bodhis

309
00:17:23,910 --> 00:17:28,990
atta and told him her story, and he answered, "Very well.

310
00:17:28,990 --> 00:17:35,000
 You go away, and I will see that the turn passes over you."

311
00:17:35,000 --> 00:17:39,520
 And therewithal he went himself to the place of execution,

312
00:17:39,520 --> 00:17:43,000
 and lay down with his own head on the block.

313
00:17:43,000 --> 00:17:46,500
 Cried the cook on seeing him, "Why? Here is the king of the

314
00:17:46,500 --> 00:17:50,000
 deer who was granted immunity. What does this mean?"

315
00:17:50,000 --> 00:17:53,000
 And off he ran to tell the king.

316
00:17:53,000 --> 00:17:56,800
 The moment he heard of it, the king mounted his chariot and

317
00:17:56,800 --> 00:17:59,000
 arrived with a large following.

318
00:17:59,000 --> 00:18:02,740
 "My friend, the king of the deer," he said, on beholding

319
00:18:02,740 --> 00:18:05,960
 the bodhisatta, "did I not promise you your life? How comes

320
00:18:05,960 --> 00:18:09,000
 it that you are lying here?"

321
00:18:09,000 --> 00:18:12,920
 Sire the bodhisatta replied, "There came to me a doe, big

322
00:18:12,920 --> 00:18:17,000
 with young, who prayed me to let her turn fall on another.

323
00:18:17,000 --> 00:18:20,990
 And as I could not pass the doom of one on to another, I,

324
00:18:20,990 --> 00:18:24,540
 laying down my own life for her and taking her doom on

325
00:18:24,540 --> 00:18:27,000
 myself, have laid me down here.

326
00:18:27,000 --> 00:18:31,590
 Think not that there is anything behind this, Your Majesty

327
00:18:31,590 --> 00:18:32,000
."

328
00:18:32,000 --> 00:18:38,130
 "My Lord, the golden king of the dearest," said the king, "

329
00:18:38,130 --> 00:18:44,580
Never yet saw I, even among men, one so abounding in charity

330
00:18:44,580 --> 00:18:47,000
, love and pity as you.

331
00:18:47,000 --> 00:18:52,630
 Therefore I am pleased with you. Arise, I spare the lives

332
00:18:52,630 --> 00:18:55,000
 of both you and of her.

333
00:18:55,000 --> 00:18:59,530
 Though two be spared, what shall the rest do, O king of men

334
00:18:59,530 --> 00:19:00,000
?"

335
00:19:00,000 --> 00:19:03,000
 "I spare them their lives too, my Lord."

336
00:19:03,000 --> 00:19:06,370
 "Sire, only the deer in your pleasant will thus have gained

337
00:19:06,370 --> 00:19:09,000
 immunity. What shall all the rest do?"

338
00:19:09,000 --> 00:19:12,000
 "Their lives too, I spare, my Lord."

339
00:19:12,000 --> 00:19:15,160
 "Sire, deer will thus be safe. What would have but the rest

340
00:19:15,160 --> 00:19:18,000
 of the four-footed creatures? What will they do?"

341
00:19:18,000 --> 00:19:21,000
 "I spare them their lives too."

342
00:19:21,000 --> 00:19:23,740
 "Sire, four-footed creatures will thus be safe. What will

343
00:19:23,740 --> 00:19:25,000
 the flocks of birds do?"

344
00:19:25,000 --> 00:19:28,000
 "They too shall be spared."

345
00:19:28,000 --> 00:19:32,120
 "Sire, birds will thus be safe, but what will the fishes do

346
00:19:32,120 --> 00:19:34,000
 who live in the water?"

347
00:19:34,000 --> 00:19:38,000
 "I spare their lives also."

348
00:19:38,000 --> 00:19:41,160
 After thus interceding with the king for the lives of all

349
00:19:41,160 --> 00:19:44,220
 creatures, the great being arose, establishing the king in

350
00:19:44,220 --> 00:19:46,000
 the five precepts, saying,

351
00:19:46,000 --> 00:19:49,300
 "Walk in righteousness, great king. Walk in righteousness

352
00:19:49,300 --> 00:19:53,000
 and justice towards parents, children, townsmen, and

353
00:19:53,000 --> 00:19:54,000
 country folk,

354
00:19:54,000 --> 00:19:57,440
 so that when this earthly body is dissolved you may enter

355
00:19:57,440 --> 00:19:59,000
 the bliss of heaven."

356
00:19:59,000 --> 00:20:02,150
 Thus with the grace and charm that marks a Buddha did he

357
00:20:02,150 --> 00:20:04,000
 teach the truth to the king.

358
00:20:04,000 --> 00:20:07,010
 A few days he tarried in the pleasants for the king's

359
00:20:07,010 --> 00:20:09,820
 instruction, and then with his attendant heard he passed

360
00:20:09,820 --> 00:20:12,000
 into the forest again.

361
00:20:12,000 --> 00:20:15,250
 And that doe brought forth a fawn fair as the opening bud

362
00:20:15,250 --> 00:20:19,000
 of the lotus, who used to play about with the branch-deer.

363
00:20:19,000 --> 00:20:22,040
 Seeing this his mother said to him, "My child, don't go

364
00:20:22,040 --> 00:20:25,350
 about with him. Only go about with the herd of the banyan

365
00:20:25,350 --> 00:20:26,000
 deer."

366
00:20:26,000 --> 00:20:30,510
 And by way of exhortation she repeated the stanza which

367
00:20:30,510 --> 00:20:32,000
 falls in this birth,

368
00:20:32,000 --> 00:20:35,380
 "Keep only to the banyan deer and shun the branch-deer's

369
00:20:35,380 --> 00:20:39,430
 herd. More welcome far as death, my child, in banyan's

370
00:20:39,430 --> 00:20:40,000
 company,

371
00:20:40,000 --> 00:20:44,000
 than in the ampless term of life with branch."

372
00:20:44,000 --> 00:20:49,210
 Then swore the deer, now in the enjoyment of immunity, used

373
00:20:49,210 --> 00:20:50,000
 to eat men's crops,

374
00:20:50,000 --> 00:20:53,510
 and the men remembering the immunity granted to them did

375
00:20:53,510 --> 00:20:56,000
 not dare to hit the deer or drive them away.

376
00:20:56,000 --> 00:20:58,590
 So they assembled in the king's courtyard and laid the

377
00:20:58,590 --> 00:21:00,000
 matter before the king.

378
00:21:00,000 --> 00:21:03,430
 Said he, "When the banyan deer won my favour I promised him

379
00:21:03,430 --> 00:21:07,430
 a boon. I will forgo my kingdom rather than my promise. Be

380
00:21:07,430 --> 00:21:09,000
 gone.

381
00:21:09,000 --> 00:21:12,000
 Not a man in my kingdom may harm the deer."

382
00:21:12,000 --> 00:21:14,930
 But when this came to the ears of the banyan deer he called

383
00:21:14,930 --> 00:21:16,000
 his herd together and said,

384
00:21:16,000 --> 00:21:19,000
 "Henceforth you shall not eat the crops of others."

385
00:21:19,000 --> 00:21:22,500
 At having thus forbidden them he sent a message to the men

386
00:21:22,500 --> 00:21:23,000
 saying,

387
00:21:23,000 --> 00:21:27,130
 "From this day forward let no husbandman fence his field,

388
00:21:27,130 --> 00:21:31,000
 but merely indicate it with leaves tied up around it."

389
00:21:31,000 --> 00:21:34,280
 And so we here began a plan of tying up leaves to indicate

390
00:21:34,280 --> 00:21:35,000
 the fields,

391
00:21:35,000 --> 00:21:38,990
 and never was a deer known to trespass on a field so marked

392
00:21:38,990 --> 00:21:42,000
, for thus they had been instructed by the bodhisatta.

393
00:21:42,000 --> 00:21:46,310
 Thus did the bodhisatta exhort the deer of his herd, and

394
00:21:46,310 --> 00:21:48,000
 thus did he act all his life long,

395
00:21:48,000 --> 00:21:52,160
 and at the close of a life long passed away with them to

396
00:21:52,160 --> 00:21:55,000
 fare according to his desserts.

397
00:21:55,000 --> 00:21:59,270
 The king too abode by the bodhisattvas' teaching, and after

398
00:21:59,270 --> 00:22:01,830
 a life spent in good works, passed away to fare according

399
00:22:01,830 --> 00:22:03,000
 to his desserts.

400
00:22:03,000 --> 00:22:06,500
 At the close of this lesson when the master had repeated

401
00:22:06,500 --> 00:22:10,000
 that, as now, in bygone days also,

402
00:22:10,000 --> 00:22:13,080
 he had been the salvation of the pair, he preached the four

403
00:22:13,080 --> 00:22:14,000
 noble truths.

404
00:22:14,000 --> 00:22:16,920
 He then showed the connection linking together the two

405
00:22:16,920 --> 00:22:18,000
 stories he had told,

406
00:22:18,000 --> 00:22:21,280
 and identified the birth by saying, "Devadatta was the

407
00:22:21,280 --> 00:22:24,990
 branch deer of those days, and his followers now were the

408
00:22:24,990 --> 00:22:26,000
 deers heard.

409
00:22:26,000 --> 00:22:31,040
 The nun was the doe, and Prince Casaba was her offspring,

410
00:22:31,040 --> 00:22:36,000
 Ananda was the king, and I myself was the banyan deer."

