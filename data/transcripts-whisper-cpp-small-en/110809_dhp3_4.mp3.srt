1
00:00:00,000 --> 00:00:04,620
 Hello, welcome back to this series of videos on the Dhammap

2
00:00:04,620 --> 00:00:07,000
ada. Today I will continue with

3
00:00:07,000 --> 00:00:13,260
 verses 3 and 4. But first I'd like to make a correction to

4
00:00:13,260 --> 00:00:14,680
 the first video about the

5
00:00:14,680 --> 00:00:19,840
 first verse wherein I said that the reason the Buddha gave

6
00:00:19,840 --> 00:00:22,280
 this teaching was because

7
00:00:23,440 --> 00:00:28,200
 of these monks killing all these insects unknowingly and

8
00:00:28,200 --> 00:00:30,440
 then the monks were saying that he had

9
00:00:30,440 --> 00:00:35,110
 done a bad deed and the Buddha said that he indeed didn't

10
00:00:35,110 --> 00:00:37,960
 and he explained that suffering

11
00:00:37,960 --> 00:00:43,370
 only follows from bad intentions. But actually going over

12
00:00:43,370 --> 00:00:44,960
 it again I realized that actually

13
00:00:44,960 --> 00:00:50,310
 specifically the Buddha gave the teaching in regards to the

14
00:00:50,310 --> 00:00:52,640
 reason why this monk went

15
00:00:52,640 --> 00:00:56,880
 blind in the first place which was because in a past life

16
00:00:56,880 --> 00:00:58,920
 he had out of revenge for a

17
00:00:58,920 --> 00:01:03,020
 woman he had treated. He was a doctor who had given this

18
00:01:03,020 --> 00:01:05,160
 treatment to a woman and out

19
00:01:05,160 --> 00:01:08,930
 of revenge for her trying to cheat him he gave her

20
00:01:08,930 --> 00:01:11,760
 something that would make her blind

21
00:01:11,760 --> 00:01:14,660
 and as a result of that bad deed it came back to him. It

22
00:01:14,660 --> 00:01:16,400
 was stuck in his mind because he

23
00:01:16,400 --> 00:01:19,350
 must have felt very guilty. As a doctor he was a good

24
00:01:19,350 --> 00:01:21,320
 doctor and feeling guilty about

25
00:01:21,320 --> 00:01:25,030
 it and it's something that he naturally would feel guilty

26
00:01:25,030 --> 00:01:27,320
 about. So it eventually caught

27
00:01:27,320 --> 00:01:32,150
 up to him and he realized what a horrible thing he had done

28
00:01:32,150 --> 00:01:34,320
 and it conditioned this

29
00:01:34,320 --> 00:01:41,760
 life for him to have to go blind. So that's the origin.

30
00:01:41,760 --> 00:01:41,760
 Then after that the Buddha said

31
00:01:41,760 --> 00:01:47,720
 "Oh you see, you can never escape your bad deeds." But it

32
00:01:47,720 --> 00:01:49,120
 does go both ways. Obviously

33
00:01:49,120 --> 00:01:54,130
 there is reference to the fact that this Chakupala wasn't

34
00:01:54,130 --> 00:01:56,120
 guilty for the killing that he had

35
00:01:56,120 --> 00:02:00,750
 done or for the death of the insects but he was on the

36
00:02:00,750 --> 00:02:03,920
 other hand guilty for something

37
00:02:03,920 --> 00:02:09,220
 that he intended to do to hurt this woman and so he got the

38
00:02:09,220 --> 00:02:10,920
 results of it. But I'm

39
00:02:10,920 --> 00:02:14,180
 not going into these stories so much. I just wanted to

40
00:02:14,180 --> 00:02:16,400
 correct something that I had said.

41
00:02:16,400 --> 00:02:20,080
 I'm going to try to stick to a very simple version of the

42
00:02:20,080 --> 00:02:22,400
 stories and place more emphasis

43
00:02:22,400 --> 00:02:25,160
 on the meaning of the verses and how they apply to our

44
00:02:25,160 --> 00:02:26,760
 lives because I think that is

45
00:02:26,760 --> 00:02:30,250
 of more importance. So this next story is the same. It's a

46
00:02:30,250 --> 00:02:31,840
 very simple story but it's

47
00:02:31,840 --> 00:02:37,460
 extended and I'm going to avoid the extension. So verses 3

48
00:02:37,460 --> 00:02:38,840
 and 4. Number 3 goes "Ako ji mung

49
00:02:41,200 --> 00:02:48,200
 a wadi man a jini man aha sime yajatang upanayhanti weirang

50
00:02:48,200 --> 00:02:48,200
te sangna sammati." And verse 4 almost

51
00:02:48,200 --> 00:02:55,730
 the same. "Ako ji man a wadi man a jini man aha sime yajat

52
00:02:55,730 --> 00:03:02,120
ang upanayhanti weirangte supasamati."

53
00:03:07,560 --> 00:03:14,560
 Which means "Ako ji man a wadi man a jini man aha sime."

54
00:03:14,560 --> 00:03:14,560
 This is a quote he or she or

55
00:03:14,560 --> 00:03:21,140
 this person scolded me, they hurt me, they beat me, they

56
00:03:21,140 --> 00:03:24,560
 defeated me, they destroyed

57
00:03:24,560 --> 00:03:31,640
 me. A person who clings or grasps on or is bound, binds

58
00:03:31,640 --> 00:03:35,400
 themselves to the body of the

59
00:03:35,400 --> 00:03:40,350
 body of the body of the body of the body of the body of the

60
00:03:40,350 --> 00:03:42,400
 body of the body of the

61
00:03:42,400 --> 00:03:47,050
 body of the body of the body of the body of the body of the

62
00:03:47,050 --> 00:03:49,400
 body of the body of the body

63
00:03:49,400 --> 00:03:54,490
 of the body of the body of the body of the body of the body

64
00:03:54,490 --> 00:03:56,400
 of the body of the body

65
00:03:56,400 --> 00:04:01,330
 of the body of the body of the body of the body of the body

66
00:04:01,330 --> 00:04:03,400
 of the body of the body

67
00:04:03,400 --> 00:04:08,350
 of the body of the body of the body of the body of the body

68
00:04:08,350 --> 00:04:10,400
 of the body of the body of

69
00:04:10,400 --> 00:04:14,750
 the body of the body of the body of the body of the body of

70
00:04:14,750 --> 00:04:17,400
 the body of the body of the

71
00:04:17,400 --> 00:04:22,400
 body of the body of the body of the body of the body of the

72
00:04:22,400 --> 00:04:24,400
 body of the body of the body

73
00:04:24,400 --> 00:04:27,940
 of the body of the body of the body of the body of the body

74
00:04:27,940 --> 00:04:29,400
 of the body of the body of

75
00:04:29,400 --> 00:04:32,750
 the body of the body of the body of the body of the body of

76
00:04:32,750 --> 00:04:34,400
 the body of the body of the

77
00:04:34,400 --> 00:04:38,650
 body of the body of the body of the body of the body of the

78
00:04:38,650 --> 00:04:40,400
 body of the body of the body

79
00:04:40,400 --> 00:04:43,850
 of the body of the body of the body of the body of the body

80
00:04:43,850 --> 00:04:45,400
 of the body of the body

81
00:04:45,400 --> 00:04:48,850
 of the body of the body of the body of the body of the body

82
00:04:48,850 --> 00:04:50,400
 of the body of the body of

83
00:04:50,400 --> 00:04:53,300
 the body of the body of the body of the body of the body of

84
00:04:53,300 --> 00:04:55,400
 the body of the body of the

85
00:04:55,400 --> 00:04:59,040
 body of the body of the body of the body of the body of the

86
00:04:59,040 --> 00:05:00,400
 body of the body of the body

87
00:05:00,400 --> 00:05:04,640
 of the body of the body of the body of the body of the body

88
00:05:04,640 --> 00:05:06,400
 of the body of the body of

89
00:05:06,400 --> 00:05:10,000
 the body of the body of the body of the body of the body of

90
00:05:10,000 --> 00:05:11,400
 the body of the body of the

91
00:05:11,400 --> 00:05:14,850
 body of the body of the body of the body of the body of the

92
00:05:14,850 --> 00:05:16,400
 body of the body of the body

93
00:05:16,400 --> 00:05:19,850
 of the body of the body of the body of the body of the body

94
00:05:19,850 --> 00:05:21,400
 of the body of the body of

95
00:05:21,400 --> 00:05:24,850
 the body of the body of the body of the body of the body of

96
00:05:24,850 --> 00:05:26,400
 the body of the body of the

97
00:05:26,400 --> 00:05:30,050
 body of the body of the body of the body of the body of the

98
00:05:30,050 --> 00:05:31,400
 body of the body of the body

99
00:05:31,400 --> 00:05:34,850
 of the body of the body of the body of the body of the body

100
00:05:34,850 --> 00:05:36,400
 of the body of the body of

101
00:05:36,400 --> 00:05:39,990
 the body of the body of the body of the body of the body of

102
00:05:39,990 --> 00:05:41,400
 the body of the body of the

103
00:05:41,400 --> 00:05:44,850
 body of the body of the body of the body of the body of the

104
00:05:44,850 --> 00:05:46,400
 body of the body of the body

105
00:05:51,400 --> 00:05:55,300
 of the body of the body of the body of the body of the body

106
00:05:55,300 --> 00:05:56,400
 of the body of the body of

107
00:05:56,400 --> 00:05:59,990
 the body of the body of the body of the body of the body of

108
00:05:59,990 --> 00:06:02,400
 the body of the body of the

109
00:06:02,400 --> 00:06:06,200
 body of the body of the body of the body of the body of the

110
00:06:06,200 --> 00:06:07,400
 body of the body of the body

111
00:06:10,400 --> 00:06:34,740
 of the body of the body of the body of the body of the body

112
00:06:34,740 --> 00:06:35,400
 of the body of the body of

113
00:06:35,400 --> 00:07:00,400
 the body of the body of the body of the body of the body of

114
00:07:00,400 --> 00:07:00,400
 the body of the body of the

115
00:07:01,400 --> 00:07:17,100
 body of the body of the body of the body of the body of the

116
00:07:17,100 --> 00:07:25,400
 body of the body of the body

117
00:07:25,400 --> 00:07:49,740
 of the body of the body of the body of the body of the body

118
00:07:49,740 --> 00:07:50,400
 of the body of the body of

119
00:07:50,400 --> 00:08:14,750
 the body of the body of the body of the body of the body of

120
00:08:14,750 --> 00:08:15,400
 the body of the body of the

121
00:08:15,400 --> 00:08:40,400
 body of the body of the body of the body of the body of the

122
00:08:40,400 --> 00:08:40,400
 body of the body of the body

123
00:08:40,400 --> 00:09:05,400
 of the body of the body of the body of the body of the body

124
00:09:05,400 --> 00:09:05,400
 of the body of the body of

125
00:09:05,400 --> 00:09:30,400
 the body of the body of the body of the body of the body of

126
00:09:30,400 --> 00:09:30,400
 the body of the body of the

127
00:09:31,400 --> 00:09:55,630
 body of the body of the body of the body of the body of the

128
00:09:55,630 --> 00:09:56,400
 body of the body of the body

129
00:09:56,400 --> 00:10:20,740
 of the body of the body of the body of the body of the body

130
00:10:20,740 --> 00:10:21,400
 of the body of the body of

131
00:10:21,400 --> 00:10:39,100
 the body of the body of the body of the body of the body of

132
00:10:39,100 --> 00:10:46,400
 the body of the body of the

133
00:10:46,400 --> 00:11:11,400
 body of the body of the body of the body of the body of the

134
00:11:11,400 --> 00:11:11,400
 body of the body of the body

135
00:11:16,400 --> 00:11:41,400
 of the body of the body of the body of the body of the body

136
00:11:41,400 --> 00:11:41,400
 of the body of the body of

137
00:11:42,400 --> 00:12:06,620
 the body of the body of the body of the body of the body of

138
00:12:06,620 --> 00:12:07,400
 the body of the body of the

139
00:14:30,400 --> 00:14:55,400
 body of the body of the body of the body of the body of the

140
00:14:55,400 --> 00:14:55,400
 body of the body of the body

141
00:14:55,400 --> 00:15:19,650
 of the body of the body of the body of the body of the body

142
00:15:19,650 --> 00:15:20,400
 of the body of the body of

143
00:15:20,400 --> 00:15:32,600
 the body of the body of the body of the body of the body of

144
00:15:32,600 --> 00:15:38,400
 the body of the body of the

145
00:15:38,400 --> 00:15:43,000
 body of the body of the body of the body of the body of the

146
00:15:43,000 --> 00:15:45,400
 body of the body of the body

147
00:16:01,400 --> 00:16:26,400
 of the body of the body of the body of the body of the body

148
00:16:26,400 --> 00:16:26,400
 of the body of the body of

149
00:16:26,400 --> 00:16:50,720
 the body of the body of the body of the body of the body of

150
00:16:50,720 --> 00:16:51,400
 the body of the body of the

151
00:16:59,400 --> 00:17:24,400
 body of the body of the body of the body of the body of the

152
00:17:24,400 --> 00:17:24,400
 body of the body of the body of the body

153
00:18:01,400 --> 00:18:13,250
 of the body of the body of the body of the body of the body

154
00:18:13,250 --> 00:18:19,400
 of the body of the body of the body of the

