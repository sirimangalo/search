1
00:00:00,000 --> 00:00:05,000
 Many people interpreted that to me in Samhata, I have been

2
00:00:05,000 --> 00:00:06,000
 personal, right?

3
00:00:06,000 --> 00:00:15,980
 Because stopping, they explained to me, keeping the mind

4
00:00:15,980 --> 00:00:18,000
 still on the object

5
00:00:18,000 --> 00:00:23,900
 and seeing the three characteristics of seeing the

6
00:00:23,900 --> 00:00:26,000
 individual essence of things.

7
00:00:27,000 --> 00:00:35,000
 If it means relent fishing, then...

8
00:00:35,000 --> 00:00:42,000
 Wipassana can also be called relent fishing.

9
00:00:42,000 --> 00:00:51,000
 Then we have no Samhata in this case.

10
00:00:52,000 --> 00:00:59,810
 Because this last tetra deals with Samhata only, pure Wip

11
00:00:59,810 --> 00:01:01,000
assana, not Samhata.

12
00:01:01,000 --> 00:01:07,000
 So the four tetras, the first one deals with what?

13
00:01:07,000 --> 00:01:14,690
 Samhata meditation. Because the commandery explains the

14
00:01:14,690 --> 00:01:18,000
 four attaining of jhanas.

15
00:01:19,000 --> 00:01:23,000
 Only after attaining jhanas one can change to Wipassana.

16
00:01:23,000 --> 00:01:32,000
 Then the second and the third deal with what?

17
00:01:32,000 --> 00:01:41,000
 Both Samhata and Wipassana. Because they partake of jhana.

18
00:01:41,000 --> 00:01:47,000
 And the fifth, only Wipassana. So there is no jhana here.

19
00:01:48,000 --> 00:01:54,000
 So the fourth deals only with Wipassana. And this fourth

20
00:01:54,000 --> 00:01:54,000
 deals with what?

21
00:01:54,000 --> 00:02:00,520
 The fourth of the foundations of mindfulness, Dhamma and W

22
00:02:00,520 --> 00:02:02,000
ipassana.

23
00:02:02,000 --> 00:02:06,780
 So fourth tetra, the first deals with the contemplation of

24
00:02:06,780 --> 00:02:09,000
 bodies, again on feeling,

25
00:02:09,000 --> 00:02:13,000
 but on consciousness and the fourth on Dhamma.

26
00:02:14,000 --> 00:02:19,000
 And this fourth deals with pure insight, Wipassana only.

27
00:02:19,000 --> 00:02:23,520
 But the other three deal with Samhata and, and both Samhata

28
00:02:23,520 --> 00:02:25,000
 and Wipassana.

29
00:02:25,000 --> 00:02:30,000
 But it is not necessary to go to the jhana school.

30
00:02:30,000 --> 00:02:34,000
 That's right. It is not necessary.

31
00:02:35,000 --> 00:02:45,000
 And many modern, modern authors tend to, what do you call,

32
00:02:45,000 --> 00:02:49,000
 de-emphasize jhanas.

33
00:02:49,000 --> 00:02:54,410
 And one author says that since jhanas are not mentioned in

34
00:02:54,410 --> 00:02:57,000
 these shoulders, they are not important.

35
00:03:00,000 --> 00:03:04,830
 But when we study a shoulder, then we have to follow the

36
00:03:04,830 --> 00:03:06,000
 tradition.

37
00:03:06,000 --> 00:03:13,000
 So tradition or the commentaries explain these four tetras

38
00:03:13,000 --> 00:03:17,000
 as dealing with Samhata and Wipassana,

39
00:03:17,000 --> 00:03:21,000
 and jhana and Wipassana.

40
00:03:21,000 --> 00:03:25,130
 And four jhanas are mentioned in the Mahasati Patana

41
00:03:25,130 --> 00:03:26,000
 shoulder.

42
00:03:27,000 --> 00:03:30,130
 Although they are not mentioned in the Anabhanasati

43
00:03:30,130 --> 00:03:31,000
 shoulder.

44
00:03:31,000 --> 00:03:37,380
 So there are two shoulders in the whole of Pali Kena that

45
00:03:37,380 --> 00:03:44,890
 deal with breathing meditation and other mindfulness

46
00:03:44,890 --> 00:03:46,000
 meditation.

47
00:03:46,000 --> 00:03:52,000
 So in the Anabhanasati shoulder, no jhanas are mentioned.

48
00:03:53,000 --> 00:03:57,000
 These passages are from that shoulder.

49
00:03:57,000 --> 00:04:01,870
 And so these passages are explained with reference to jhana

50
00:04:01,870 --> 00:04:06,000
 and Wipassana in the ancient commentaries.

51
00:04:06,000 --> 00:04:12,750
 So you cannot say that you do not find jhanas in these two

52
00:04:12,750 --> 00:04:14,000
 shoulders.

53
00:04:15,000 --> 00:04:19,710
 In the Anabhanasati shoulder, jhanas are not mentioned

54
00:04:19,710 --> 00:04:23,000
 explicitly, but they are implied.

55
00:04:23,000 --> 00:04:28,030
 But in the Mahasati Patana shoulder, jhanas are mentioned

56
00:04:28,030 --> 00:04:36,000
 when Buddha defined the Noble Eightfold Path.

57
00:04:37,000 --> 00:04:44,190
 So we cannot say that jhanas are put into the kenon later.

58
00:04:44,190 --> 00:04:48,000
 We have no proof of that.

59
00:04:48,000 --> 00:04:54,480
 We may carelessly say that anything which we don't like is

60
00:04:54,480 --> 00:04:57,000
 an interpolation.

61
00:04:57,000 --> 00:05:00,000
 It's added by monks or something like that.

62
00:05:01,000 --> 00:05:11,670
 But there can be no, what to call, no definite proof that a

63
00:05:11,670 --> 00:05:19,000
 portion of the shoulder is put into it later. Who knows?

64
00:05:19,000 --> 00:05:25,270
 So it is important to study such shoulders with the help of

65
00:05:25,270 --> 00:05:28,000
 the ancient commentaries.

66
00:05:29,000 --> 00:05:34,460
 And it's safe to follow them rather than going away from

67
00:05:34,460 --> 00:05:44,610
 them and interpreting in a way one likes and not following

68
00:05:44,610 --> 00:05:47,000
 the ancient tradition.

69
00:05:47,000 --> 00:05:53,000
 Now conclusion.

70
00:05:53,000 --> 00:06:12,850
 So in the footnotes on page 315, through substitution of

71
00:06:12,850 --> 00:06:17,000
 opposite qualities and so on, there is some explanation

72
00:06:17,000 --> 00:06:22,000
 about the abandonment of the

73
00:06:22,000 --> 00:06:26,000
 the relevant question of giving up.

74
00:06:26,000 --> 00:06:30,690
 About the middle of the footnote. And giving up in this way

75
00:06:30,690 --> 00:06:34,000
 is in the form of inducing non-occurrence.

76
00:06:34,000 --> 00:06:40,580
 Now when we say that, let us say, the path, maga, erad

77
00:06:40,580 --> 00:06:47,250
icates defilements, we do not mean that it really eradicates

78
00:06:47,250 --> 00:06:49,000
 defilements.

79
00:06:50,000 --> 00:06:55,020
 What it meant is it doesn't let the defilements to come up

80
00:06:55,020 --> 00:06:58,000
 again. So non-occurrence.

81
00:06:58,000 --> 00:07:03,210
 Because the present defilements cannot be destroyed because

82
00:07:03,210 --> 00:07:06,000
 they are already in our minds.

83
00:07:06,000 --> 00:07:09,410
 And the paths are already paths and we don't have to do

84
00:07:09,410 --> 00:07:12,000
 anything about them. They are already gone.

85
00:07:12,000 --> 00:07:15,000
 And the future things are not yet come.

86
00:07:16,000 --> 00:07:20,970
 So what the maga or the path eradicates is not the present

87
00:07:20,970 --> 00:07:25,000
 or not the past or not the future defilements.

88
00:07:25,000 --> 00:07:34,490
 But the eradication of the maga rendered them, what do you

89
00:07:34,490 --> 00:07:35,000
 call that?

90
00:07:35,000 --> 00:07:37,000
 Inoperative.

91
00:07:37,000 --> 00:07:42,000
 Inoperative or non-arising. So they will not arise again.

92
00:07:43,000 --> 00:07:47,000
 So it is like destroying the potential of things.

93
00:07:47,000 --> 00:07:53,670
 So it talks about it here. And this subject will be picked

94
00:07:53,670 --> 00:07:57,000
 up later towards the end of the book.

95
00:08:02,000 --> 00:08:08,930
 And paragraph 239, second line, we have the words clear

96
00:08:08,930 --> 00:08:12,000
 vision and deliverance.

97
00:08:12,000 --> 00:08:19,610
 Here clear vision means path, maga. And deliverance means

98
00:08:19,610 --> 00:08:22,000
 fruition, pala.

99
00:08:22,000 --> 00:08:40,390
 Okay then. And then the benefits of this kind of meditation

100
00:08:40,390 --> 00:08:41,000
.

101
00:08:42,000 --> 00:08:53,090
 All of them come from the same sutta, anapanasic sutta. And

102
00:08:53,090 --> 00:08:54,380
 then the story of how a monk who was an arahant and who

103
00:08:54,380 --> 00:08:56,560
 practiced anap, mindfulness of breathing meditation and who

104
00:08:56,560 --> 00:08:58,000
 became an arahant,

105
00:08:59,000 --> 00:09:05,700
 how he knew when he would die. And then he asked his

106
00:09:05,700 --> 00:09:13,320
 companions how they had seen an arahant attain nirvana and

107
00:09:13,320 --> 00:09:16,000
 some say in a sitting position.

108
00:09:17,000 --> 00:09:23,030
 And he said, I will show you. He was going to die. He knew

109
00:09:23,030 --> 00:09:28,000
 the very moment he was going to die.

110
00:09:28,000 --> 00:09:34,740
 And so I will die or I will end the nirvana walking. So he

111
00:09:34,740 --> 00:09:42,000
 made a line in the ambulatory.

112
00:09:43,000 --> 00:09:47,320
 And then he said, I will go to the end of this space. And

113
00:09:47,320 --> 00:09:53,000
 then when I come back and cross the line, I will die then.

114
00:09:53,000 --> 00:10:01,640
 And he did die as he said. So those who practice anapanasic

115
00:10:01,640 --> 00:10:07,910
 meditation can even know when their life force will be

116
00:10:07,910 --> 00:10:12,000
 stopped or when they are going to die.

117
00:10:12,000 --> 00:10:20,000
 So this is also the benefits of anapanasic meditation.

118
00:10:20,000 --> 00:10:29,190
 And in paragraph 241, there is the explanation of three

119
00:10:29,190 --> 00:10:34,180
 kinds of finals, the final in becoming, final in jhana and

120
00:10:34,180 --> 00:10:35,000
 final in death.

121
00:10:36,000 --> 00:10:36,020
 And in paragraph

122
00:10:41,000 --> 00:10:44,750
 that is why there are final ones in jhana. Those that arise

123
00:10:44,750 --> 00:10:48,430
 along with the sixteenth consciousness preceding the death

124
00:10:48,430 --> 00:10:49,000
 consciousness,

125
00:10:49,000 --> 00:10:52,000
 cease together with the death consciousness.

126
00:10:54,000 --> 00:11:00,330
 Now, there are four kinds of material properties, four

127
00:11:00,330 --> 00:11:06,950
 kinds of matter, those caused by karma, those caused by c

128
00:11:06,950 --> 00:11:10,780
itta or mind, those caused by climate and those caused by

129
00:11:10,780 --> 00:11:11,000
 food.

130
00:11:12,000 --> 00:11:19,180
 Those caused by karma must, must disappear at the same

131
00:11:19,180 --> 00:11:24,000
 moment as the death consciousness.

132
00:11:26,000 --> 00:11:32,240
 So at the last, last half moment of death consciousness,

133
00:11:32,240 --> 00:11:38,530
 they must also disappear because the material properties

134
00:11:38,530 --> 00:11:45,980
 caused by mind cannot, cannot live after the moment of

135
00:11:45,980 --> 00:11:47,000
 death.

136
00:11:48,000 --> 00:12:00,100
 So, the, what we call life, or what is translated as vital

137
00:12:00,100 --> 00:12:08,000
 principle of vital life means death, that karma born

138
00:12:08,000 --> 00:12:13,000
 quality in the material properties.

139
00:12:14,000 --> 00:12:17,010
 So death, karma born quality in the material properties

140
00:12:17,010 --> 00:12:20,360
 must die with the death consciousness, must disappear with

141
00:12:20,360 --> 00:12:22,000
 the death consciousness.

142
00:12:22,000 --> 00:12:27,910
 Now, the life of a material property is seventeen times

143
00:12:27,910 --> 00:12:32,000
 that of the life of a consciousness.

144
00:12:32,000 --> 00:12:38,580
 Consciousness lasts for only one big moment or three small

145
00:12:38,580 --> 00:12:40,000
 moments.

146
00:12:41,000 --> 00:12:51,000
 So, the material property lasts for seventeen big moments,

147
00:12:51,000 --> 00:12:52,000
 seventeen big thought moments.

148
00:12:53,000 --> 00:12:59,910
 Since the vital principle must dissolve or disappear at the

149
00:12:59,910 --> 00:13:06,610
 last sub-moment of death consciousness, it must have arisen

150
00:13:06,610 --> 00:13:09,000
 how many moments before?

151
00:13:09,000 --> 00:13:13,710
 Sixteen moments before. So sixteen plus death moment,

152
00:13:13,710 --> 00:13:15,000
 seventeen moments.

153
00:13:16,000 --> 00:13:19,660
 So the end of the seventeenth moment, and the life

154
00:13:19,660 --> 00:13:25,200
 principle must disappear. That is why the preceding, the

155
00:13:25,200 --> 00:13:29,000
 sixteenth consciousness preceding the death consciousness.

156
00:13:29,000 --> 00:13:34,560
 If you want to read more about it, you can read it in the

157
00:13:34,560 --> 00:13:40,000
 sixth chapter of the manual of Abhidhamma.

158
00:13:41,000 --> 00:13:49,360
 When, when a different material properties finally, finally

159
00:13:49,360 --> 00:13:52,000
 disappear in a life.

160
00:13:52,000 --> 00:14:04,000
 So now we go to recollection of peace.

161
00:14:09,000 --> 00:14:13,000
 Recollection of peace really means recollection of Nibbana.

162
00:14:13,000 --> 00:14:16,210
 So one who wants to develop the recollection of peace

163
00:14:16,210 --> 00:14:19,880
 mentioned next to mindfulness of breathing should go into

164
00:14:19,880 --> 00:14:22,520
 solitary retreat and recollect these special qualities of

165
00:14:22,520 --> 00:14:23,000
 Nibbana.

166
00:14:23,000 --> 00:14:27,000
 In other words, the stealing of all suffering as follows.

167
00:14:27,000 --> 00:14:31,070
 Because in so far as there are dhammas, whether formed or

168
00:14:31,070 --> 00:14:35,000
 unformed, that means conditioned or unconditioned,

169
00:14:36,000 --> 00:14:39,570
 feeding away is pronounced the best of them. So feeding

170
00:14:39,570 --> 00:14:41,000
 away is the best of them.

171
00:14:41,000 --> 00:14:44,340
 That is to say, the disillusionment of vanity, the

172
00:14:44,340 --> 00:14:48,000
 elimination of thirst, the abolition of reliance,

173
00:14:48,000 --> 00:14:50,930
 the termination of the realm, the destruction of freedom,

174
00:14:50,930 --> 00:14:53,000
 feeding away, cessation, Nibbana.

175
00:14:53,000 --> 00:14:56,000
 This is what will be explained.

176
00:14:56,000 --> 00:15:01,000
 Here in so far as means as many as.

177
00:15:02,000 --> 00:15:06,000
 Dhammas means individual essences.

178
00:15:06,000 --> 00:15:10,760
 Now, there is a footnote about dhamma and a long, long

179
00:15:10,760 --> 00:15:12,000
 footnote here.

180
00:15:12,000 --> 00:15:17,000
 The footnote gives me headache.

181
00:15:17,000 --> 00:15:23,260
 First, I have to find the passages in the commentaries and

182
00:15:23,260 --> 00:15:26,000
 then check with them the translation.

183
00:15:27,000 --> 00:15:30,360
 And then, sometimes it's very difficult to translate them

184
00:15:30,360 --> 00:15:32,000
 the way they are written.

185
00:15:32,000 --> 00:15:43,990
 And one problem is English is not my mother tongue, so it's

186
00:15:43,990 --> 00:15:46,000
 not so easy.

187
00:15:46,000 --> 00:15:52,000
 Now, dhamma.

188
00:15:54,000 --> 00:16:03,000
 Dhamma comes from the root dha, that means to hold.

189
00:16:03,000 --> 00:16:11,680
 So, in such passages as dhammas, that are concepts, even a

190
00:16:11,680 --> 00:16:15,000
 non-entity is thus called a dhamma.

191
00:16:15,000 --> 00:16:20,270
 Now, what dhamma can mean? Anything in the world, including

192
00:16:20,270 --> 00:16:21,000
 Nibbana.

193
00:16:23,000 --> 00:16:26,000
 Even the concepts are called dhammas.

194
00:16:26,000 --> 00:16:32,410
 Why are they called dhammas? Because they are born and

195
00:16:32,410 --> 00:16:35,000
 affirmed by knowledge.

196
00:16:35,000 --> 00:16:52,000
 They are carried or they are upheld, something like that.

197
00:16:52,000 --> 00:16:57,600
 That kind of dhamma is excluded by his saying dhammas means

198
00:16:57,600 --> 00:17:00,000
 individual essence.

199
00:17:00,000 --> 00:17:04,000
 So, when it is said that dhamma means individual essence,

200
00:17:04,000 --> 00:17:13,050
 then the other wanted to exclude concepts from being called

201
00:17:13,050 --> 00:17:14,000
 dhamma here.

202
00:17:15,000 --> 00:17:20,140
 So, here dhamma means those that have individual essence

203
00:17:20,140 --> 00:17:22,000
 and not concepts.

204
00:17:22,000 --> 00:17:29,120
 Now, that kind of dhamma is excluded by his saying dhammas

205
00:17:29,120 --> 00:17:31,000
 means individual essence.

206
00:17:31,000 --> 00:17:37,810
 The act of becoming, which constitutes existingness in the

207
00:17:37,810 --> 00:17:40,000
 ultimate sense is essence.

208
00:17:41,000 --> 00:17:46,000
 It is with essence, thus it is an individual essence.

209
00:17:46,000 --> 00:17:54,040
 Now, the Dali word sabhawa, this word is also important

210
00:17:54,040 --> 00:17:55,000
 word.

211
00:17:58,000 --> 00:18:05,940
 Here, sabhawa is defined, first bhawa is defined. Bhawa

212
00:18:05,940 --> 00:18:11,000
 means becoming. The act of becoming is bhawa.

213
00:18:11,000 --> 00:18:18,420
 And something which is with this act of becoming is called

214
00:18:18,420 --> 00:18:20,000
 sabhawa.

215
00:18:21,000 --> 00:18:26,900
 The act of becoming means existingness in the ultimate

216
00:18:26,900 --> 00:18:28,000
 sense.

217
00:18:28,000 --> 00:18:32,110
 That means having the three moments of existence, arising,

218
00:18:32,110 --> 00:18:34,000
 aging and disappearing.

219
00:18:34,000 --> 00:18:45,000
 Such things are called sabhawa because they are with bhawa.

220
00:18:46,000 --> 00:18:50,630
 In this word, sabh means with and bhawa means the act of

221
00:18:50,630 --> 00:18:54,000
 becoming, that is, let us say existence.

222
00:18:54,000 --> 00:19:00,000
 So, which has their own existence? They are called sabhawa.

223
00:19:00,000 --> 00:19:07,850
 In other places, or in the footnote itself, sabhawa is

224
00:19:07,850 --> 00:19:12,000
 explained in another way.

225
00:19:14,000 --> 00:19:20,670
 There, sabh means one's own, and bhawa means essence or

226
00:19:20,670 --> 00:19:23,000
 something like that.

227
00:19:23,000 --> 00:19:28,000
 So, sabhawa means one's own essence or one's own nature.

228
00:19:28,000 --> 00:19:36,000
 Or it means common essence or common nature.

229
00:19:38,000 --> 00:19:43,590
 So, sabhawa can mean different things in different contexts

230
00:19:43,590 --> 00:19:44,000
.

231
00:19:44,000 --> 00:19:50,060
 If it is used as a substantive, then it means something

232
00:19:50,060 --> 00:19:58,230
 that has its own becoming or that has existence in the

233
00:19:58,230 --> 00:19:59,000
 ultimate sense.

234
00:19:59,000 --> 00:20:02,000
 Something that has existence.

235
00:20:03,000 --> 00:20:09,420
 In other places, it may mean just one's individual nature

236
00:20:09,420 --> 00:20:16,000
 or individual essence or common essence, common nature.

237
00:20:16,000 --> 00:20:21,000
 There are two kinds of nature, individual and common.

238
00:20:26,000 --> 00:20:32,030
 Suppose I am a bhameesh. So, bhameeshness is my individual

239
00:20:32,030 --> 00:20:33,000
 essence.

240
00:20:33,000 --> 00:20:37,480
 And I am a human being, and this is my common essence,

241
00:20:37,480 --> 00:20:40,000
 common with the other beings.

242
00:20:40,000 --> 00:20:45,000
 So, something like that.

243
00:20:45,000 --> 00:20:50,000
 So, sabhawa can mean these things.

244
00:20:51,000 --> 00:20:56,000
 [Pause]

245
00:20:56,000 --> 00:21:02,210
 Here, so, we should translate, not individual essences,

246
00:21:02,210 --> 00:21:07,000
 some things which have individual essence.

247
00:21:07,000 --> 00:21:11,980
 Here, not essence. What is essence? It is an aspect now,

248
00:21:11,980 --> 00:21:17,000
 right? Essence. It doesn't mean a substantive thing.

249
00:21:19,000 --> 00:21:23,520
 No. So, something which has individual essence or some

250
00:21:23,520 --> 00:21:27,360
 things which have individual essence are called dhammas

251
00:21:27,360 --> 00:21:28,000
 here.

252
00:21:28,000 --> 00:21:31,060
 And whether formed or unformed and so on, they are whether

253
00:21:31,060 --> 00:21:33,000
 conditioned or unconditioned.

254
00:21:33,000 --> 00:21:39,000
 And the long footnote.

255
00:21:48,000 --> 00:21:52,180
 So, individual essence consisting in, say, hardness as that

256
00:21:52,180 --> 00:21:55,410
 of the earth or touching as that of contact is not common

257
00:21:55,410 --> 00:21:56,000
 to all dhammas.

258
00:21:56,000 --> 00:21:59,500
 The generality is the individual essence common to all

259
00:21:59,500 --> 00:22:02,000
 consisting in, in common and so on.

260
00:22:02,000 --> 00:22:10,740
 So, suppose we have earth element, right? Earth element has

261
00:22:10,740 --> 00:22:16,000
 individual essence and general or common essence.

262
00:22:17,000 --> 00:22:20,000
 So, this individual essence is hardness or softness.

263
00:22:20,000 --> 00:22:25,060
 So, hardness or softness is the individual essence of earth

264
00:22:25,060 --> 00:22:30,000
 element, not shared by other elements and dhammas.

265
00:22:30,000 --> 00:22:32,000
 But it is impermanent.

266
00:22:32,000 --> 00:22:39,500
 So, it's impermanent is the common or general essence of it

267
00:22:39,500 --> 00:22:40,000
.

268
00:22:41,000 --> 00:22:45,940
 We can have the specific or individual essence and general

269
00:22:45,940 --> 00:22:47,000
 essence.

270
00:22:47,000 --> 00:23:03,000
 And with regard to time in the footnote, the quote "moolat

271
00:23:03,000 --> 00:23:03,000
iga"

272
00:23:03,000 --> 00:23:13,010
 "Though time is determined by the kind of consciousness

273
00:23:13,010 --> 00:23:18,000
 about the middle of the page."

274
00:23:18,000 --> 00:23:22,650
 For example, as specified in the first paragraph of the d

275
00:23:22,650 --> 00:23:24,000
hammasanghami

276
00:23:25,000 --> 00:23:30,620
 "The non-existence as to individual essence, yet as the non

277
00:23:30,620 --> 00:23:36,170
-entity before and after the movement in which those who

278
00:23:36,170 --> 00:23:39,210
 miss and then who present dhammasoka, it is called the

279
00:23:39,210 --> 00:23:41,000
 container adhikarana.

280
00:23:41,000 --> 00:23:47,560
 It is perceived symbolized only as a state of receptacle ad

281
00:23:47,560 --> 00:23:49,000
harabhava."

282
00:23:51,000 --> 00:24:01,000
 I wonder whether he understood it.

283
00:24:01,000 --> 00:24:10,900
 Now, first, we have to have a little knowledge of the dham

284
00:24:10,900 --> 00:24:12,000
ma.

285
00:24:13,000 --> 00:24:19,300
 So, in the adhebhiram I just said, on such an occasion or

286
00:24:19,300 --> 00:24:25,000
 at such a time, let us say, the first kusala data arises.

287
00:24:25,000 --> 00:24:31,540
 Now, at such a time, the words in English, the words, right

288
00:24:31,540 --> 00:24:33,000
? In Bali one word.

289
00:24:34,000 --> 00:24:42,370
 The words are put in the locative case, x, on, or in the

290
00:24:42,370 --> 00:24:45,000
 locative case.

291
00:24:45,000 --> 00:24:56,610
 Now, why is here, let me say, the Pali word is used is sam

292
00:24:56,610 --> 00:24:57,000
aya.

293
00:24:58,000 --> 00:25:04,270
 Samaya can mean time, or it can mean location. So, let us

294
00:25:04,270 --> 00:25:05,000
 take time, it means time.

295
00:25:05,000 --> 00:25:11,800
 So, why is time the location of consciousness? So, it is

296
00:25:11,800 --> 00:25:14,000
 going to explain this.

297
00:25:14,000 --> 00:25:20,150
 Receptacle and container just means location. So, time is

298
00:25:20,150 --> 00:25:24,000
 not existent according to a bhidama.

299
00:25:26,000 --> 00:25:33,530
 Though time is determined by the kind of consciousness and

300
00:25:33,530 --> 00:25:39,000
 is non-existent as to individual essence,

301
00:25:39,000 --> 00:25:43,410
 according to reality, time has no existence. So, time is

302
00:25:43,410 --> 00:25:44,000
 non-existent.

303
00:25:44,000 --> 00:25:48,910
 But it is determined by the cheda, that means the time when

304
00:25:48,910 --> 00:25:51,000
 a given cheda arises.

305
00:25:52,000 --> 00:25:56,880
 So, time is determined by the cheda. Yet, as the non-entity

306
00:25:56,880 --> 00:26:00,000
, the following are not so good.

307
00:26:00,000 --> 00:26:07,940
 So, why can time be the location of cheda? Time is non-

308
00:26:07,940 --> 00:26:09,000
existent.

309
00:26:09,000 --> 00:26:14,000
 And how can it be the location of cheda?

310
00:26:15,000 --> 00:26:24,450
 When a consciousness arises, before it arises there is

311
00:26:24,450 --> 00:26:25,000
 nothing.

312
00:26:25,000 --> 00:26:29,000
 And after it dissolves there is nothing.

313
00:26:29,000 --> 00:26:35,670
 But when it is in the, during the three phases of existence

314
00:26:35,670 --> 00:26:38,000
, it is called existing.

315
00:26:39,000 --> 00:26:45,000
 So, at that time the consciousness is in existence.

316
00:26:45,000 --> 00:26:51,560
 So, time is said to be a location or receptacle of the

317
00:26:51,560 --> 00:26:54,000
 consciousness.

318
00:26:54,000 --> 00:27:00,350
 Actually, there is no time. Time is not existence or not

319
00:27:00,350 --> 00:27:03,000
 existing according to ultimate reality.

320
00:27:04,000 --> 00:27:09,770
 Although it is non-existent, it is said to be the location

321
00:27:09,770 --> 00:27:11,000
 of cheda.

322
00:27:11,000 --> 00:27:17,000
 Because before the arising of cheda there is no cheda.

323
00:27:17,000 --> 00:27:20,000
 And after the dissolution of cheda there is no cheda.

324
00:27:20,000 --> 00:27:25,000
 So, that is why time is said to be a location for cheda.

325
00:27:25,000 --> 00:27:32,000
 And Buddha said at such a time, the first cheda arises in

326
00:27:32,000 --> 00:27:33,000
 support.

327
00:27:34,000 --> 00:27:37,000
 But the same is true for space also?

328
00:27:37,000 --> 00:27:41,280
 Space, space is also mentioned here. Space and time are

329
00:27:41,280 --> 00:27:45,000
 said to be non-existent according to abidama.

330
00:27:45,000 --> 00:27:52,000
 So, the translation itself is not correct here.

331
00:27:52,000 --> 00:28:00,000
 You know, you need to be very familiar with the language.

332
00:28:01,000 --> 00:28:07,460
 Because there are words that look the same, but you have to

333
00:28:07,460 --> 00:28:10,000
 understand differently.

334
00:28:10,000 --> 00:28:15,500
 So, bhava and abhava, the word abhava is misunderstood here

335
00:28:15,500 --> 00:28:16,000
.

336
00:28:16,000 --> 00:28:22,470
 He translated it as non-entity and it gives the word abhava

337
00:28:22,470 --> 00:28:23,000
.

338
00:28:23,000 --> 00:28:28,000
 But what it means is just non-existent.

339
00:28:28,000 --> 00:28:38,000
 So, the time can be the location or receptacle of cheda.

340
00:28:38,000 --> 00:28:50,980
 Because before the arising or after the dissolution of ched

341
00:28:50,980 --> 00:28:56,000
a and its concomitants, they do not exist.

342
00:28:57,000 --> 00:29:01,000
 So, it is like a reference, right?

343
00:29:01,000 --> 00:29:08,000
 The cheda arises at this time or at that time.

344
00:29:08,000 --> 00:29:16,000
 So, somehow...

345
00:29:25,000 --> 00:29:31,010
 Now, and then in the text itself, explanation of different

346
00:29:31,010 --> 00:29:32,000
 words.

347
00:29:32,000 --> 00:29:41,000
 Now, fading away and disillusionment of vanity and so on.

348
00:29:41,000 --> 00:29:48,700
 And in the explanation of these, the words "oncoming to it"

349
00:29:48,700 --> 00:29:50,000
 are used.

350
00:29:51,000 --> 00:29:55,910
 "Oncoming to it" they are relinquished or they fade away

351
00:29:55,910 --> 00:29:57,000
 and so on.

352
00:29:57,000 --> 00:30:02,000
 So, "oncoming to it" really means on account of it.

353
00:30:02,000 --> 00:30:08,000
 Or, "on taking it as object".

354
00:30:08,000 --> 00:30:15,000
 Now, fading away is not nibbana.

355
00:30:16,000 --> 00:30:20,650
 But nibbana is described as with the words which can mean

356
00:30:20,650 --> 00:30:22,000
 fading away.

357
00:30:22,000 --> 00:30:27,000
 The word "bali" "wi raga" is used here and otherwise too.

358
00:30:27,000 --> 00:30:33,000
 So, "wi raga" is simply translated as "fading away".

359
00:30:33,000 --> 00:30:37,940
 But the act of fading away is not nibbana. Nibbana is not

360
00:30:37,940 --> 00:30:39,000
 fading away.

361
00:30:39,000 --> 00:30:41,000
 Nibbana is something.

362
00:30:44,000 --> 00:30:48,000
 Nibbana, although it has no existence, it is.

363
00:30:48,000 --> 00:30:53,000
 So nibbana is something.

364
00:30:53,000 --> 00:31:02,400
 And when "maka chitta" arises, it eradicates mental

365
00:31:02,400 --> 00:31:04,000
 refinement.

366
00:31:04,000 --> 00:31:08,000
 So, it makes mental refinement fade away.

367
00:31:09,000 --> 00:31:14,000
 But it arises only when it takes nibbana as object.

368
00:31:14,000 --> 00:31:19,000
 If it does not take nibbana as object, it cannot arise.

369
00:31:19,000 --> 00:31:23,540
 So nibbana is a condition for the "maka chitta", part

370
00:31:23,540 --> 00:31:26,000
 consciousness to arise.

371
00:31:26,000 --> 00:31:35,670
 So nibbana is said to be instrumental in making different

372
00:31:35,670 --> 00:31:38,000
 means fade away.

373
00:31:38,000 --> 00:31:40,000
 By "tat", by "maka".

374
00:31:40,000 --> 00:31:44,000
 So it is to be understood in this way, for all these words.

375
00:31:44,000 --> 00:31:50,000
 Not just fading away and then termination of the wrong.

376
00:31:50,000 --> 00:31:56,000
 And what are the other words? Extinction of something.

377
00:31:56,000 --> 00:32:03,130
 Whatever I just said, it means nibbana is instrumental in

378
00:32:03,130 --> 00:32:06,000
 extinction of suffering, something like that.

379
00:32:06,000 --> 00:32:07,000
 And then...

380
00:32:07,000 --> 00:32:31,000
 Right.

381
00:32:31,000 --> 00:32:49,000
 [Pause]

382
00:32:50,000 --> 00:33:01,000
 So, they are there to be understood in that sense. That is,

383
00:33:01,000 --> 00:33:01,000
 just fading, not just fading away is nibbana.

384
00:33:01,000 --> 00:33:08,670
 But nibbana is something which is instrumental in making

385
00:33:08,670 --> 00:33:13,000
 defilements fade away by manga.

386
00:33:15,000 --> 00:33:23,000
 And there is one... one... let's see...

387
00:33:23,000 --> 00:33:34,000
 Something like...

388
00:33:34,000 --> 00:33:42,000
 Change of lineage.

389
00:33:43,000 --> 00:33:46,000
 Do you see the word "change of lineage" somewhere?

390
00:33:46,000 --> 00:33:56,000
 Middle of the footnote.

391
00:33:56,000 --> 00:34:00,000
 Middle of the footnote? On page 318.

392
00:34:00,000 --> 00:34:02,000
 318.

393
00:34:05,000 --> 00:34:10,520
 Now, what the order... I mean, the order of the sub-command

394
00:34:10,520 --> 00:34:16,000
 we were saying is that change of lineage...

395
00:34:16,000 --> 00:34:22,180
 that the precedes... immediately precedes the movement of

396
00:34:22,180 --> 00:34:23,000
 path.

397
00:34:23,000 --> 00:34:28,000
 And that data is called change of lineage.

398
00:34:28,000 --> 00:34:33,000
 And that data takes nibbana as object. Right?

399
00:34:34,000 --> 00:34:39,360
 So nibbana, which is to be realized by... or which is to be

400
00:34:39,360 --> 00:34:43,400
 seen by the movement of change of lineage, must be one that

401
00:34:43,400 --> 00:34:45,000
 has the profundity

402
00:34:45,000 --> 00:34:48,720
 surpassing the nature of belonging to three periods of time

403
00:34:48,720 --> 00:34:49,000
.

404
00:34:49,000 --> 00:34:53,660
 nibbana is timeless. Nibbana does not belong to the present

405
00:34:53,660 --> 00:34:55,000
 or first or future.

406
00:34:59,000 --> 00:35:05,010
 So, in order for nibbana to be realized by change of

407
00:35:05,010 --> 00:35:09,000
 lineage, it must also... it must be... it must have the...

408
00:35:09,000 --> 00:35:15,240
 or it must... it must not belong to any of the three

409
00:35:15,240 --> 00:35:19,000
 periods. That is what is meant there.

410
00:35:19,000 --> 00:35:30,000
 [Silence]

411
00:35:30,000 --> 00:35:34,000
 Okay, now we come to the end of...

412
00:35:35,000 --> 00:36:04,000
 [Silence]

413
00:36:04,000 --> 00:36:09,270
 So you can pick up some positive words from this and say

414
00:36:09,270 --> 00:36:13,000
 that nibbana is also a positive state.

415
00:36:13,000 --> 00:36:23,670
 And then, benefits of the practice of contemplation or nibb

416
00:36:23,670 --> 00:36:27,000
ana or contemplation on peace.

417
00:36:28,000 --> 00:36:36,000
 Okay, this comes to the end of this chapter. Each chapter.

418
00:36:36,000 --> 00:36:44,260
 So, it is the end of this class and we have a break for

419
00:36:44,260 --> 00:36:47,000
 about a month.

420
00:36:47,000 --> 00:36:50,000
 Almost a month.

421
00:36:50,000 --> 00:36:53,960
 On the first Thursday in July, we'll meet back here at the

422
00:36:53,960 --> 00:36:55,000
 first day.

423
00:36:56,000 --> 00:36:58,000
 [Laughter]

424
00:36:58,000 --> 00:37:01,000
 We'll be good on the photograph of our house.

425
00:37:01,000 --> 00:37:05,000
 Oh, yes. Yeah. Interesting stuff.

426
00:37:05,000 --> 00:37:08,000
 [Laughter]

427
00:37:08,000 --> 00:37:10,000
 Okay.

428
00:37:10,000 --> 00:37:14,000
 Thank you very much once again.

429
00:37:14,000 --> 00:37:16,000
 Thank you.

430
00:37:17,000 --> 00:37:39,000
 [Silence]

431
00:37:40,000 --> 00:37:50,000
 [Silence]

432
00:37:50,000 --> 00:38:01,000
 [Silence]

433
00:38:02,000 --> 00:38:20,000
 [Silence]

