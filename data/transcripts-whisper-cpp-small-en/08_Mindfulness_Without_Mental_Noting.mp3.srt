1
00:00:00,000 --> 00:00:03,120
 Mindfulness without mental noting

2
00:00:03,120 --> 00:00:07,800
 Question. I have been practicing mindfulness without mental

3
00:00:07,800 --> 00:00:12,240
 noting and focusing on the sensations, thoughts, etc.

4
00:00:12,240 --> 00:00:15,660
 For me, it is easier to be mindful this way.

5
00:00:15,660 --> 00:00:18,220
 What is your opinion on this?

6
00:00:18,220 --> 00:00:22,800
 Answer. Nobody wants to note. Nobody wants to put words in

7
00:00:22,800 --> 00:00:26,300
 their head. Why? Because it is difficult.

8
00:00:26,300 --> 00:00:29,950
 You have to ask yourself, what is the purpose of meditation

9
00:00:29,950 --> 00:00:30,240
?

10
00:00:30,240 --> 00:00:33,440
 Is the purpose of meditation to make things easy?

11
00:00:33,440 --> 00:00:36,720
 Or is the purpose in meditation to overcome what is

12
00:00:36,720 --> 00:00:37,640
 difficult?

13
00:00:37,640 --> 00:00:41,410
 Some people make the observation that noting makes it more

14
00:00:41,410 --> 00:00:44,880
 difficult to recognize the object for what it is

15
00:00:44,880 --> 00:00:48,910
 because noting forces you to see impermanence, suffering,

16
00:00:48,910 --> 00:00:50,080
 and non-self.

17
00:00:50,080 --> 00:00:53,880
 It forces you to see that things are not as you would like

18
00:00:53,880 --> 00:00:54,280
 them.

19
00:00:54,280 --> 00:00:57,930
 It forces you to see your mind's inclination to control

20
00:00:57,930 --> 00:01:00,920
 things and then be unable to control them.

21
00:01:00,920 --> 00:01:04,640
 Because of the regimented nature of this meditative

22
00:01:04,640 --> 00:01:08,360
 practice, you are not able to have this smooth mindfulness

23
00:01:08,360 --> 00:01:11,160
 which I would just call concentration.

24
00:01:11,160 --> 00:01:14,440
 The word mindfulness can be used so loosely as to become

25
00:01:14,440 --> 00:01:15,960
 rather meaningless.

26
00:01:15,960 --> 00:01:19,620
 It is not a good translation of the word sati which is what

27
00:01:19,620 --> 00:01:21,080
 we are practicing.

28
00:01:21,080 --> 00:01:24,890
 The word sati means specifically the recognition of

29
00:01:24,890 --> 00:01:26,440
 something as it is.

30
00:01:26,440 --> 00:01:29,480
 It is caused by something called tirasana.

31
00:01:29,480 --> 00:01:31,960
 Sanana means the recognition of something.

32
00:01:31,960 --> 00:01:36,930
 This is this, this is that, and tira means firm, strong, or

33
00:01:36,930 --> 00:01:38,040
 fortified.

34
00:01:38,040 --> 00:01:41,870
 Sati means fortifying the recognition of the object for

35
00:01:41,870 --> 00:01:45,480
 what it is and this is why we use the noting technique.

36
00:01:45,480 --> 00:01:49,500
 The problem is that we do not like it. Noting breaks up the

37
00:01:49,500 --> 00:01:52,280
 continuity of the mind's inclinations.

38
00:01:52,280 --> 00:01:56,150
 The stream of habitual clinging to conceit, craving, and to

39
00:01:56,150 --> 00:01:58,520
 the ideas of self are disrupted.

40
00:01:58,520 --> 00:02:02,290
 The whole idea that something is easier for me implies a

41
00:02:02,290 --> 00:02:05,320
 preference and possibly an identification

42
00:02:05,320 --> 00:02:09,580
 with the experience that it is me meditating. It is me

43
00:02:09,580 --> 00:02:11,320
 being mindful.

44
00:02:11,320 --> 00:02:14,720
 The practice of noting results in you feeling like you are

45
00:02:14,720 --> 00:02:16,360
 forcing the experience.

46
00:02:16,360 --> 00:02:20,280
 The experience feels jarring, chaotic, and uncontrollable

47
00:02:20,280 --> 00:02:22,680
 which is the actual reality of it.

48
00:02:22,680 --> 00:02:26,360
 Noting forces you to see that noting itself is not

49
00:02:26,360 --> 00:02:27,720
 uncomfortable.

50
00:02:27,720 --> 00:02:31,040
 It is the discord between the way we would like things to

51
00:02:31,040 --> 00:02:34,440
 be and the way things really are that feels unpleasant.

52
00:02:34,440 --> 00:02:38,000
 Once you come to see things as they are, moment to moment

53
00:02:38,000 --> 00:02:39,000
 and that they do not

54
00:02:39,000 --> 00:02:42,920
 carry on from one moment to the next, then noting becomes a

55
00:02:42,920 --> 00:02:44,600
 very easy thing to do.

56
00:02:44,600 --> 00:02:48,120
 Catching things up in your attention one by one becomes

57
00:02:48,120 --> 00:02:52,120
 natural because you are able to see and accept reality

58
00:02:52,120 --> 00:02:57,070
 just as it is without continuous entities only moment to

59
00:02:57,070 --> 00:02:59,240
 moment experiences.

60
00:02:59,240 --> 00:03:02,930
 Our ordinary perception is of things that continue from

61
00:03:02,930 --> 00:03:04,120
 moment to moment

62
00:03:04,120 --> 00:03:07,530
 because the mind does not find such things in practice. It

63
00:03:07,530 --> 00:03:10,930
 becomes quite disturbed by the simple observation of

64
00:03:10,930 --> 00:03:12,920
 moments of experience.

65
00:03:12,920 --> 00:03:16,570
 Instead of perceiving the momentary experiences for what

66
00:03:16,570 --> 00:03:20,200
 they are, it attempts to create stability by force

67
00:03:20,200 --> 00:03:23,640
 and it may feel like the noting itself is a use of force

68
00:03:23,640 --> 00:03:27,890
 but in actuality it is just our misguided desire for

69
00:03:27,890 --> 00:03:29,800
 stability and control.

70
00:03:29,800 --> 00:03:34,040
 In my opinion, practice without noting is not truly sati.

71
00:03:34,040 --> 00:03:37,640
 Why? Because the word sati means to remind yourself.

72
00:03:37,640 --> 00:03:41,760
 Sati means the firm recognition that comes from reminding

73
00:03:41,760 --> 00:03:44,520
 yourself of the object for what it is.

74
00:03:44,520 --> 00:03:49,640
 The Buddha said "gacango wa gacchami ti pajanati" which

75
00:03:49,640 --> 00:03:52,600
 means "when walking one knows I am walking".

76
00:03:52,600 --> 00:03:56,420
 Gacchami means walking without emphasizing the "I" or the "

77
00:03:56,420 --> 00:03:57,000
me".

78
00:03:57,000 --> 00:04:00,570
 It is the simplest way to focus one's attention on the

79
00:04:00,570 --> 00:04:05,270
 object. Janati means "one knows" and the power prefix means

80
00:04:05,270 --> 00:04:05,720
 "fully"

81
00:04:05,720 --> 00:04:09,720
 which implies an exceptional sort of knowing beyond the

82
00:04:09,720 --> 00:04:12,760
 ordinary sense that it is me who is walking.

83
00:04:12,760 --> 00:04:16,450
 Instead of noting, I try to use the word mantra to describe

84
00:04:16,450 --> 00:04:19,960
 this practice. Mantra is the word that we already know.

85
00:04:19,960 --> 00:04:23,790
 Mantra meditation is a very ancient practice as a widely

86
00:04:23,790 --> 00:04:27,620
 accepted means of focusing the mind's attention on an

87
00:04:27,620 --> 00:04:28,520
 object.

88
00:04:28,520 --> 00:04:32,460
 Ordinarily, however, the object of mantra is a concept

89
00:04:32,460 --> 00:04:36,920
 because concepts are stable, satisfying and controllable.

90
00:04:36,920 --> 00:04:41,480
 In mindfulness meditation, our focus is on experiential

91
00:04:41,480 --> 00:04:45,320
 reality and because experiences are neither stable,

92
00:04:45,320 --> 00:04:49,080
 satisfying nor controllable, it can be a source of some

93
00:04:49,080 --> 00:04:52,680
 distress to the mind seeking out those qualities.

94
00:04:52,680 --> 00:04:56,550
 The fact that you experience impermanence, suffering and

95
00:04:56,550 --> 00:04:59,990
 non-self when applying a mantra to things that are imper

96
00:04:59,990 --> 00:05:00,520
manent,

97
00:05:00,520 --> 00:05:04,140
 suffering and non-self is a sign that you are practicing

98
00:05:04,140 --> 00:05:05,160
 correctly.

99
00:05:05,160 --> 00:05:08,010
 Feeling uncomfortable as a result of seeing these

100
00:05:08,010 --> 00:05:11,800
 characteristics of reality is an important part of the

101
00:05:11,800 --> 00:05:14,520
 training to take you out of your comfort zone.

102
00:05:14,520 --> 00:05:18,210
 Helping you to release your dependency on things that are

103
00:05:18,210 --> 00:05:19,720
 really not dependable.

104
00:05:19,720 --> 00:05:23,320
 From our point of view, it is forcing you to look at things

105
00:05:23,320 --> 00:05:24,440
 objectively.

106
00:05:24,440 --> 00:05:28,870
 If you are interested in seeing reality clearly as it is, I

107
00:05:28,870 --> 00:05:33,320
 can only recommend that you go practice noting the objects.

108
00:05:33,320 --> 00:05:36,610
 If you prefer something more peaceful, calm and soothing

109
00:05:36,610 --> 00:05:41,160
 that takes you away from ordinary reality, then you may

110
00:05:41,160 --> 00:05:44,040
 have to look elsewhere.

