1
00:00:00,000 --> 00:00:03,840
 Hi, welcome back to Ask a Monk. Today's question comes from

2
00:00:03,840 --> 00:00:07,000
 Mikkel Art, who asks, "How quickly

3
00:00:07,000 --> 00:00:10,140
 should I switch between the different things, feelings,

4
00:00:10,140 --> 00:00:11,900
 breathing and so on, that calls

5
00:00:11,900 --> 00:00:15,580
 for my attention when meditating? In your How to Meditate

6
00:00:15,580 --> 00:00:16,720
 videos, you are doing it quite

7
00:00:16,720 --> 00:00:21,480
 slowly but is this only for demonstration purposes?"

8
00:00:21,480 --> 00:00:27,350
 Um, no, I don't think it is just for demonstration purposes

9
00:00:27,350 --> 00:00:28,480
. It's not important how I do this

10
00:00:29,480 --> 00:00:32,090
 how many times you say the word or that you can catch

11
00:00:32,090 --> 00:00:35,040
 everything. It's not important that

12
00:00:35,040 --> 00:00:37,740
 everything that comes up you have a label for it. What's

13
00:00:37,740 --> 00:00:39,120
 important is that your mind

14
00:00:39,120 --> 00:00:43,350
 is in this place, your mind is in this zone, your mind is

15
00:00:43,350 --> 00:00:45,800
 clearly aware of things and this

16
00:00:45,800 --> 00:00:49,800
 word that we use is just to pull the mind back to that

17
00:00:49,800 --> 00:00:52,520
 reality, it's to fix the mind,

18
00:00:52,520 --> 00:00:57,300
 to straighten the mind and keep the mind straight and aware

19
00:00:57,300 --> 00:00:59,520
 of things as they are. You only

20
00:00:59,520 --> 00:01:02,420
 need to do, to acknowledge maybe one time every second and

21
00:01:02,420 --> 00:01:03,920
 it's not a rhythmic thing,

22
00:01:03,920 --> 00:01:08,690
 it's kind of just comfortable at a comfortable pace. So

23
00:01:08,690 --> 00:01:10,920
 when you feel pain, just say to yourself,

24
00:01:10,920 --> 00:01:14,900
 "Pain, pain, pain," and it should be comfortable. There

25
00:01:14,900 --> 00:01:17,520
 should be no feeling that you're saying

26
00:01:17,520 --> 00:01:20,440
 it quickly or you're saying it slowly. It should be just

27
00:01:20,440 --> 00:01:22,280
 saying it. If you get the idea

28
00:01:22,280 --> 00:01:24,280
 that you're saying it quickly then yeah, probably it's too

29
00:01:24,280 --> 00:01:25,440
 quick. If you get the idea that you're

30
00:01:25,440 --> 00:01:28,660
 saying it slowly, probably it's too slow. It should be so

31
00:01:28,660 --> 00:01:29,920
 natural that you don't even

32
00:01:29,920 --> 00:01:34,370
 notice the speed, that there's no sense of slower or fast.

33
00:01:34,370 --> 00:01:36,920
 It should be neutral. It should

34
00:01:36,920 --> 00:01:41,000
 be just saying it when you feel, when you're thinking, just

35
00:01:41,000 --> 00:01:43,360
 thinking, thinking, thinking,

36
00:01:43,360 --> 00:01:46,640
 just as you would normally speak. When you speak to someone

37
00:01:46,640 --> 00:01:48,920
, you speak at a certain rhythm.

38
00:01:48,920 --> 00:01:52,400
 The acknowledgement should be done in that manner. As far

39
00:01:52,400 --> 00:01:54,320
 as switching from object to

40
00:01:54,320 --> 00:01:58,620
 object, this is also not productive in the meditation

41
00:01:58,620 --> 00:02:01,320
 practice. It's not productive to

42
00:02:01,320 --> 00:02:04,050
 try to go, "Okay, now I'm hearing. Okay, now I'm seeing.

43
00:02:04,050 --> 00:02:05,720
 Okay, now..." Pick one thing

44
00:02:05,720 --> 00:02:10,220
 and focus on it. Look at it clearly and just use it as a

45
00:02:10,220 --> 00:02:12,720
 tool to train your mind to see

46
00:02:12,720 --> 00:02:15,930
 things for what they are. It doesn't matter which thing you

47
00:02:15,930 --> 00:02:16,800
 use, whatever is there in

48
00:02:16,800 --> 00:02:20,380
 front of you, whatever is clearest, whatever your mind

49
00:02:20,380 --> 00:02:22,340
 takes as the object. Suppose now

50
00:02:22,340 --> 00:02:24,950
 I'm listening to the sounds outside, then I just focus on

51
00:02:24,950 --> 00:02:26,280
 the sounds. I don't have to

52
00:02:26,280 --> 00:02:29,580
 pay attention to everything else. When you're walking with

53
00:02:29,580 --> 00:02:31,100
 your foot, you also feel your

54
00:02:31,100 --> 00:02:33,480
 knee moving, your leg straightening, the tension and the

55
00:02:33,480 --> 00:02:35,040
 movement in the back, the shifting

56
00:02:35,040 --> 00:02:38,250
 and so on. But you don't have to acknowledge all of those

57
00:02:38,250 --> 00:02:39,720
 things. The reason that you are

58
00:02:39,720 --> 00:02:42,670
 aware of all of those is because you're saying, "Step being

59
00:02:42,670 --> 00:02:44,760
 right," or "Lifting your foot,

60
00:02:44,760 --> 00:02:47,680
 being placing," or so on. Because you're focusing on the

61
00:02:47,680 --> 00:02:49,080
 foot, you then are aware of

62
00:02:49,080 --> 00:02:52,390
 everything as it arises quite clearly. You don't have to

63
00:02:52,390 --> 00:02:53,880
 catch everything as long as

64
00:02:53,880 --> 00:02:58,190
 you're keeping your mind in this mode, bringing your mind

65
00:02:58,190 --> 00:03:00,880
 back again and again until finally

66
00:03:00,880 --> 00:03:04,790
 the mind starts to look at things naturally in this way

67
00:03:04,790 --> 00:03:07,000
 because that's what's going to

68
00:03:07,000 --> 00:03:10,980
 happen. Once you develop this to a great extent, you'll

69
00:03:10,980 --> 00:03:12,560
 find that it just comes naturally.

70
00:03:12,560 --> 00:03:15,010
 You're just aware of things when you move moving, when you

71
00:03:15,010 --> 00:03:16,320
 brush your teeth brushing,

72
00:03:16,320 --> 00:03:21,080
 when you eat your food chewing, scooping, chewing,

73
00:03:21,080 --> 00:03:23,320
 swallowing and so on. You don't need

74
00:03:23,320 --> 00:03:27,270
 to use the words. The word is the training that we do in

75
00:03:27,270 --> 00:03:28,920
 meditation. If you keep training

76
00:03:28,920 --> 00:03:32,500
 yourself and training, you'll get to such a high level that

77
00:03:32,500 --> 00:03:34,120
 your mind just lets go and

78
00:03:34,120 --> 00:03:37,430
 you're able to see things clearly. It's such an extreme

79
00:03:37,430 --> 00:03:40,240
 letting go that the mind actually

80
00:03:40,240 --> 00:03:44,120
 leaves behind the experiential world and enters into a

81
00:03:44,120 --> 00:03:47,240
 state of nirvana through this practice.

82
00:03:47,240 --> 00:03:51,670
 The state that we're aiming for is not this constant

83
00:03:51,670 --> 00:03:54,840
 labeling, but the labeling is what

84
00:03:54,840 --> 00:03:59,040
 increases our awareness. If we can use the labeling again

85
00:03:59,040 --> 00:04:01,560
 and again and again, and clearing

86
00:04:01,560 --> 00:04:05,180
 our mind to a higher and higher degree, we can eventually

87
00:04:05,180 --> 00:04:07,160
 be able to let go of everything

88
00:04:07,160 --> 00:04:14,160
 and so that our mind is able to experience true freedom.

89
00:04:14,160 --> 00:04:14,160
 Not quickly, just try to do

90
00:04:14,160 --> 00:04:19,330
 it naturally. That's the short answer. Thanks for the

91
00:04:19,330 --> 00:04:20,640
 question. Keep practicing.

