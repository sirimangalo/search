1
00:00:00,000 --> 00:00:05,200
 Okay, good evening.

2
00:00:05,200 --> 00:00:10,500
 Welcome back to our study of the Dhammapada.

3
00:00:10,500 --> 00:00:19,200
 Today we look at verse 202, which reads as follows.

4
00:00:19,200 --> 00:00:34,800
 "Nati raga sama agi, nati dosasama gali, nati khandhasama d

5
00:00:34,800 --> 00:00:42,120
ukha, nati santi paransukha."

6
00:00:42,120 --> 00:00:51,600
 Which means there is no fire like passion.

7
00:00:51,600 --> 00:01:00,120
 There is no misfortune like anger.

8
00:01:00,120 --> 00:01:08,600
 There is no suffering like the aggregates.

9
00:01:08,600 --> 00:01:14,000
 There is no happiness apart from peace.

10
00:01:14,000 --> 00:01:21,640
 This is a very popular verse, I guess they all are.

11
00:01:21,640 --> 00:01:23,720
 Now this is a very powerful verse.

12
00:01:23,720 --> 00:01:27,880
 "Nati santi paransukha" is very well known.

13
00:01:27,880 --> 00:01:37,640
 But it's quite a powerful verse.

14
00:01:37,640 --> 00:01:44,440
 The story is again quite a simple story, not much to it.

15
00:01:44,440 --> 00:01:48,360
 But it has a lesson for us.

16
00:01:48,360 --> 00:01:58,280
 There was a young man and young woman who were betrothed

17
00:01:58,280 --> 00:02:02,160
 and married and were brought

18
00:02:02,160 --> 00:02:13,360
 together to give alms to the Buddha and all the monks.

19
00:02:13,360 --> 00:02:23,260
 And the young woman set about to provide food and drink for

20
00:02:23,260 --> 00:02:25,600
 the monks.

21
00:02:25,600 --> 00:02:30,500
 But the young man, when he saw the young woman, when he was

22
00:02:30,500 --> 00:02:33,160
 just looking at her, watching

23
00:02:33,160 --> 00:02:40,840
 her go about her activities, he was suddenly seized with

24
00:02:40,840 --> 00:02:44,440
 great desire, great lust.

25
00:02:44,440 --> 00:02:49,010
 And forgot all about his duties and didn't help her in any

26
00:02:49,010 --> 00:02:51,400
 way with preparing the food

27
00:02:51,400 --> 00:02:53,120
 or offering the food.

28
00:02:53,120 --> 00:03:03,050
 He just stood there watching her and the lust filled his

29
00:03:03,050 --> 00:03:04,600
 mind.

30
00:03:04,600 --> 00:03:09,680
 And it filled his mind to such an extent that he completely

31
00:03:09,680 --> 00:03:12,200
 forgot where he was in a very

32
00:03:12,200 --> 00:03:23,200
 sort of sombre, well, reverential atmosphere, religious

33
00:03:23,200 --> 00:03:25,240
 atmosphere.

34
00:03:25,240 --> 00:03:32,610
 It's like when you're in church, forgot where he was and

35
00:03:32,610 --> 00:03:35,680
 reached out his hand, or hands

36
00:03:35,680 --> 00:03:39,200
 maybe, he was going to grab her.

37
00:03:39,200 --> 00:03:40,880
 And it doesn't say what he was planning to do.

38
00:03:40,880 --> 00:03:43,880
 I don't know that he even had any plans.

39
00:03:43,880 --> 00:03:54,480
 Maybe just, well, anyway, no need to go there.

40
00:03:54,480 --> 00:03:58,100
 And as he did so, the Buddha realized what was happening

41
00:03:58,100 --> 00:03:59,440
 and stopped him.

42
00:03:59,440 --> 00:04:07,860
 They say the Buddha caused him to not see his wife or his

43
00:04:07,860 --> 00:04:10,200
 betrothed.

44
00:04:10,200 --> 00:04:15,570
 And when he couldn't see his wife, he turned and looked at

45
00:04:15,570 --> 00:04:18,080
 the Buddha and sobered up a

46
00:04:18,080 --> 00:04:27,960
 little maybe and the Buddha said to him, "This is a fire.

47
00:04:27,960 --> 00:04:32,960
 You are caught up in a fire, a wildfire."

48
00:04:32,960 --> 00:04:41,920
 And then he taught this verse.

49
00:04:41,920 --> 00:04:47,280
 So the story is one of any number of stories that exempl

50
00:04:47,280 --> 00:04:50,480
ifies this verse quite well.

51
00:04:50,480 --> 00:04:54,230
 And the lesson I think is one and the same in the story and

52
00:04:54,230 --> 00:04:55,400
 in the verse.

53
00:04:55,400 --> 00:04:59,280
 They go along very, they go together very well.

54
00:04:59,280 --> 00:05:03,590
 This verse provides a very good reflection, I think, for

55
00:05:03,590 --> 00:05:04,560
 all of us.

56
00:05:04,560 --> 00:05:13,800
 It's a good reflection on craving and desire in general.

57
00:05:13,800 --> 00:05:20,800
 When you want anything, how much of our lives is caught up

58
00:05:20,800 --> 00:05:23,800
 in desire, passion?

59
00:05:23,800 --> 00:05:28,420
 And how like a fire is it that it burns everything and

60
00:05:28,420 --> 00:05:31,560
 destroys everything the Buddha said.

61
00:05:31,560 --> 00:05:35,520
 There's no fire like passion.

62
00:05:35,520 --> 00:05:37,600
 But it's very much like any other fire.

63
00:05:37,600 --> 00:05:42,480
 It's just so far, far more dangerous and destructive.

64
00:05:42,480 --> 00:05:47,480
 It's because of passion that we steal and kill.

65
00:05:47,480 --> 00:05:55,520
 It's because of passion that rape is committed.

66
00:05:55,520 --> 00:06:01,060
 It's because of passion that we destroy friendships and

67
00:06:01,060 --> 00:06:09,640
 relationships committing adultery and

68
00:06:09,640 --> 00:06:15,000
 impropriety giving up any sense of what might be right or

69
00:06:15,000 --> 00:06:16,080
 proper.

70
00:06:16,080 --> 00:06:24,920
 And putting a propriety aside, what is right.

71
00:06:24,920 --> 00:06:25,920
 It makes us forget.

72
00:06:25,920 --> 00:06:27,960
 It destroys so much in the mind.

73
00:06:27,960 --> 00:06:29,520
 It obliterates it.

74
00:06:29,520 --> 00:06:32,460
 It's like if you imagine that during this ceremony with all

75
00:06:32,460 --> 00:06:33,720
 the monks and the Buddha

76
00:06:33,720 --> 00:06:36,670
 and this two, suppose they were both offering quite

77
00:06:36,670 --> 00:06:38,680
 respectfully and then a fire broke

78
00:06:38,680 --> 00:06:41,320
 out.

79
00:06:41,320 --> 00:06:51,960
 It would destroy the whole ceremony, the whole procession

80
00:06:51,960 --> 00:06:54,440
 of events.

81
00:06:54,440 --> 00:06:56,120
 Well craving does that as well.

82
00:06:56,120 --> 00:07:03,640
 It breaks the goodness, the ethics, the wholesomeness.

83
00:07:03,640 --> 00:07:13,240
 It destroys propriety and goodness and all other things.

84
00:07:13,240 --> 00:07:16,640
 It steamrolls them all in favor of its desires.

85
00:07:16,640 --> 00:07:20,500
 When we want something, we'll just take it.

86
00:07:20,500 --> 00:07:22,800
 And like any fire, it grows.

87
00:07:22,800 --> 00:07:26,580
 Fire doesn't say, "Okay, you fed me and now I'm full and

88
00:07:26,580 --> 00:07:27,360
 that's enough."

89
00:07:27,360 --> 00:07:32,200
 No fire and craving like it is not something you can

90
00:07:32,200 --> 00:07:33,440
 satisfy.

91
00:07:33,440 --> 00:07:36,440
 Fire the more it gets, the more it wants.

92
00:07:36,440 --> 00:07:40,840
 The more it gets, the more it takes.

93
00:07:40,840 --> 00:07:42,240
 Craving is the same.

94
00:07:42,240 --> 00:07:46,440
 Passion is the same.

95
00:07:46,440 --> 00:07:52,280
 You become more and more invested in the fuel.

96
00:07:52,280 --> 00:07:58,680
 Whatever it is that fuels your passion on, you want it more

97
00:07:58,680 --> 00:07:58,760
.

98
00:07:58,760 --> 00:08:04,040
 And more intensely, such that you'll do more to get it.

99
00:08:04,040 --> 00:08:06,840
 You need to get more of it.

100
00:08:06,840 --> 00:08:11,640
 More intense.

101
00:08:11,640 --> 00:08:17,130
 Until you find yourself forgetting all of the good, all of

102
00:08:17,130 --> 00:08:20,480
 what's good and right.

103
00:08:20,480 --> 00:08:21,480
 Just forgetting really.

104
00:08:21,480 --> 00:08:22,480
 It's obliterated.

105
00:08:22,480 --> 00:08:30,720
 And your search for satisfaction.

106
00:08:30,720 --> 00:08:33,150
 Not even thinking that you're going to be satisfied or not

107
00:08:33,150 --> 00:08:34,320
 going to be satisfied.

108
00:08:34,320 --> 00:08:39,120
 You're just blind and caught up in the fire of it.

109
00:08:39,120 --> 00:08:41,480
 It's out of control.

110
00:08:41,480 --> 00:08:42,560
 That's what addiction is.

111
00:08:42,560 --> 00:08:48,760
 No addiction or anything.

112
00:08:48,760 --> 00:08:56,640
 It inflames the mind.

113
00:08:56,640 --> 00:08:58,880
 And it leads to dosa.

114
00:08:58,880 --> 00:09:01,080
 It leads to anger.

115
00:09:01,080 --> 00:09:08,200
 Why the Buddha mentions anger as being nati dosa samakali.

116
00:09:08,200 --> 00:09:10,200
 There's no misfortune like anger.

117
00:09:10,200 --> 00:09:15,580
 Because the real problem with passion is that it leads you

118
00:09:15,580 --> 00:09:18,560
 to want and want more intensely

119
00:09:18,560 --> 00:09:23,440
 and at the same time makes you vulnerable to displeasure

120
00:09:23,440 --> 00:09:25,800
 when you don't get what you

121
00:09:25,800 --> 00:09:28,800
 want.

122
00:09:28,800 --> 00:09:34,030
 It leads to anger when you don't get what you want or anger

123
00:09:34,030 --> 00:09:38,160
 when you're obstructed.

124
00:09:38,160 --> 00:09:46,360
 When you're given things other than what you want.

125
00:09:46,360 --> 00:09:49,330
 Dosa is considered, the Buddha does something quite

126
00:09:49,330 --> 00:09:51,440
 interesting to say that it's Kali.

127
00:09:51,440 --> 00:09:56,170
 Kali is often used to describe misfortune like an unlucky

128
00:09:56,170 --> 00:09:57,920
 throw of the dice.

129
00:09:57,920 --> 00:10:05,450
 If you throw some dice and you get a bad roll, no bad luck,

130
00:10:05,450 --> 00:10:07,320
 it's Kali.

131
00:10:07,320 --> 00:10:09,520
 It's also used to mean evil in general.

132
00:10:09,520 --> 00:10:11,970
 It's used to describe evil so it doesn't necessarily

133
00:10:11,970 --> 00:10:13,480
 literally mean misfortune.

134
00:10:13,480 --> 00:10:15,560
 But it's interesting that they're put together.

135
00:10:15,560 --> 00:10:19,680
 Kali is not a word that you hear quite often.

136
00:10:19,680 --> 00:10:25,600
 So I think the sense of misfortune is meant to be retained

137
00:10:25,600 --> 00:10:26,680
 there.

138
00:10:26,680 --> 00:10:31,230
 It's a misfortune in the same way that if you throw the

139
00:10:31,230 --> 00:10:34,560
 dice and if you throw them wrong,

140
00:10:34,560 --> 00:10:35,560
 bad things happen.

141
00:10:35,560 --> 00:10:36,560
 You lose your money.

142
00:10:36,560 --> 00:10:37,560
 You lose.

143
00:10:37,560 --> 00:10:39,240
 You lose out.

144
00:10:39,240 --> 00:10:45,160
 Anger causes you to lose everything.

145
00:10:45,160 --> 00:10:47,360
 Everything burns it all out.

146
00:10:47,360 --> 00:10:53,600
 But as long as the fire is going, there's a fire.

147
00:10:53,600 --> 00:10:54,600
 There's something.

148
00:10:54,600 --> 00:10:58,490
 You're too caught up to realize what's gone and then when

149
00:10:58,490 --> 00:11:00,120
 the fire burns out, when the

150
00:11:00,120 --> 00:11:06,600
 fuel runs out, you're left with the ashes and the ashes are

151
00:11:06,600 --> 00:11:08,320
 like anger.

152
00:11:08,320 --> 00:11:11,740
 You can no longer get what you want and you're left with

153
00:11:11,740 --> 00:11:13,400
 the, with just ashes.

154
00:11:13,400 --> 00:11:16,920
 Anger is like ashes.

155
00:11:16,920 --> 00:11:25,460
 And when you get angry, it's a misfortune in the sense that

156
00:11:25,460 --> 00:11:28,840
 everything you do leads

157
00:11:28,840 --> 00:11:34,560
 to suffering.

158
00:11:34,560 --> 00:11:39,600
 But it's unlike any other kind of misfortune because if you

159
00:11:39,600 --> 00:11:42,220
 have bad luck, maybe you, nine

160
00:11:42,220 --> 00:11:45,810
 out of ten times, you throw the dice and you get the wrong

161
00:11:45,810 --> 00:11:47,760
 role and you lose a lot of your

162
00:11:47,760 --> 00:11:48,760
 money.

163
00:11:48,760 --> 00:11:51,370
 But you still have a chance to make a right role, no matter

164
00:11:51,370 --> 00:11:52,680
 how bad luck, bad your luck

165
00:11:52,680 --> 00:11:53,680
 is.

166
00:11:53,680 --> 00:11:55,920
 But with anger, it's not like that.

167
00:11:55,920 --> 00:12:01,080
 Anger can never lead to a good result.

168
00:12:01,080 --> 00:12:05,490
 When people fight, husbands and wives and partners fight

169
00:12:05,490 --> 00:12:06,980
 with each other.

170
00:12:06,980 --> 00:12:09,830
 When parents fight with children, when friends fight with

171
00:12:09,830 --> 00:12:11,720
 friends or enemies fight over things

172
00:12:11,720 --> 00:12:16,650
 that they want, we fight out of greed or even just fighting

173
00:12:16,650 --> 00:12:19,040
 out of hatred, fighting out

174
00:12:19,040 --> 00:12:24,920
 of delusion and we destroy all good.

175
00:12:24,920 --> 00:12:26,840
 We lose everything.

176
00:12:26,840 --> 00:12:30,680
 You lose your friendships, you lose your relationships.

177
00:12:30,680 --> 00:12:36,160
 You even lose your life and health.

178
00:12:36,160 --> 00:12:40,320
 It's a great misfortune.

179
00:12:40,320 --> 00:12:45,880
 The greatest, there's no misfortune like anger.

180
00:12:45,880 --> 00:12:52,330
 Nati khanda samadukha is a reminder in the same vein, it's

181
00:12:52,330 --> 00:12:55,360
 in the same reflection, reflecting

182
00:12:55,360 --> 00:13:01,840
 on desire, passion.

183
00:13:01,840 --> 00:13:10,620
 It's remembering about how the objects of our desire are

184
00:13:10,620 --> 00:13:15,760
 ultimately just experience.

185
00:13:15,760 --> 00:13:19,850
 Khanda, the aggregates refers to that which makes up an

186
00:13:19,850 --> 00:13:21,080
 experience.

187
00:13:21,080 --> 00:13:25,290
 When you see or hear or smell or taste or feel or think,

188
00:13:25,290 --> 00:13:27,480
 every moment of that there's

189
00:13:27,480 --> 00:13:32,720
 an experience, there's the five aggregates.

190
00:13:32,720 --> 00:13:35,680
 Form is the physical aspect.

191
00:13:35,680 --> 00:13:41,090
 Feeling is the pleasure or pain or neutral feeling

192
00:13:41,090 --> 00:13:43,200
 associated with it.

193
00:13:43,200 --> 00:13:48,440
 Sannya is the recognition of what you see or hear.

194
00:13:48,440 --> 00:13:50,680
 Sankara is the reaction to it.

195
00:13:50,680 --> 00:13:52,960
 Mnyana is the awareness of it.

196
00:13:52,960 --> 00:13:58,120
 All of that is those five things come together or arise at

197
00:13:58,120 --> 00:14:00,120
 every experience.

198
00:14:00,120 --> 00:14:08,760
 What it means is that that's all there is.

199
00:14:08,760 --> 00:14:15,110
 The teaching on why this is dukkha is quite, it's profound

200
00:14:15,110 --> 00:14:18,080
 and radical and it's an important

201
00:14:18,080 --> 00:14:21,390
 part of the Buddhist teaching, the core part of the

202
00:14:21,390 --> 00:14:22,920
 Buddhist teaching.

203
00:14:22,920 --> 00:14:28,590
 Because what this man is lusting after in his wife is not

204
00:14:28,590 --> 00:14:31,880
 actually his wife, it's the

205
00:14:31,880 --> 00:14:35,640
 five aggregates, it's the experiences.

206
00:14:35,640 --> 00:14:40,900
 And through his not being able to see that, he's coming to

207
00:14:40,900 --> 00:14:42,800
 great suffering.

208
00:14:42,800 --> 00:14:50,440
 He's obliterating all the good and so on.

209
00:14:50,440 --> 00:14:55,410
 But the teaching that the five aggregates are, there's no

210
00:14:55,410 --> 00:14:57,680
 suffering like them, is a

211
00:14:57,680 --> 00:14:59,680
 much deeper sort of teaching.

212
00:14:59,680 --> 00:15:05,200
 It shows what he's lacking, what he's unable to see.

213
00:15:05,200 --> 00:15:10,910
 He's only able to see that suffering comes from not getting

214
00:15:10,910 --> 00:15:13,360
 what he wants, not that the

215
00:15:13,360 --> 00:15:17,700
 system is unable to grant him what he wants because it's

216
00:15:17,700 --> 00:15:20,400
 only made up of moments of experience.

217
00:15:20,400 --> 00:15:29,790
 Ordinary suffering, we think of suffering dukkha-vedana

218
00:15:29,790 --> 00:15:32,560
 when we get a painful feeling

219
00:15:32,560 --> 00:15:35,920
 or any feeling that's not the one we want.

220
00:15:35,920 --> 00:15:41,810
 We don't get a pleasant feeling, we're bored and frustrated

221
00:15:41,810 --> 00:15:41,840
.

222
00:15:41,840 --> 00:15:45,160
 We get painful feelings doubly so.

223
00:15:45,160 --> 00:15:47,720
 Not only did we not get what we wanted, we got what we didn

224
00:15:47,720 --> 00:15:48,280
't want.

225
00:15:48,280 --> 00:15:55,600
 So we become doubly frustrated.

226
00:15:55,600 --> 00:16:01,600
 Pain becomes our enemy.

227
00:16:01,600 --> 00:16:07,050
 As we understand suffering as being a way that which we don

228
00:16:07,050 --> 00:16:09,240
't want and we try to avoid

229
00:16:09,240 --> 00:16:13,440
 it.

230
00:16:13,440 --> 00:16:17,830
 We come to see that in reality there's some suffering that

231
00:16:17,830 --> 00:16:19,120
 we can't avoid.

232
00:16:19,120 --> 00:16:21,640
 We can't possibly avoid suffering in life.

233
00:16:21,640 --> 00:16:24,360
 It's going to come to us in various forms.

234
00:16:24,360 --> 00:16:27,360
 We see dukkha-sabhava, that's what leads people to come to

235
00:16:27,360 --> 00:16:28,640
 practice meditation.

236
00:16:28,640 --> 00:16:35,440
 We see suffering more deeply.

237
00:16:35,440 --> 00:16:38,920
 And when they start to practice meditation, because they've

238
00:16:38,920 --> 00:16:40,800
 seen they can't escape suffering,

239
00:16:40,800 --> 00:16:43,990
 they start to realize that it's a characteristic of reality

240
00:16:43,990 --> 00:16:44,240
.

241
00:16:44,240 --> 00:16:48,680
 It's called dukkha-lakkana.

242
00:16:48,680 --> 00:16:51,960
 This understanding of suffering helps the person to let go.

243
00:16:51,960 --> 00:16:58,570
 As they start to see, "Mmm, it's not suffering as a concept

244
00:16:58,570 --> 00:17:04,000
, as a reality, as a thing, as

245
00:17:04,000 --> 00:17:08,040
 a subject, as a topic."

246
00:17:08,040 --> 00:17:12,070
 Clinging is not something that you can avoid because it's

247
00:17:12,070 --> 00:17:14,080
 inherent in everything.

248
00:17:14,080 --> 00:17:18,560
 Meaning it comes from clinging.

249
00:17:18,560 --> 00:17:24,420
 It doesn't matter what you cling to, it doesn't matter what

250
00:17:24,420 --> 00:17:26,200
 you crave for.

251
00:17:26,200 --> 00:17:29,370
 Seeing things as providing you satisfaction, thinking that

252
00:17:29,370 --> 00:17:32,760
 they're going to make you happy

253
00:17:32,760 --> 00:17:33,760
 is the problem.

254
00:17:33,760 --> 00:17:39,000
 It's what leads us to suffer.

255
00:17:39,000 --> 00:17:42,140
 And as you see that more and more clearly, you come to the

256
00:17:42,140 --> 00:17:43,520
 moment where you see that

257
00:17:43,520 --> 00:17:51,820
 nati khanda samadukha, you see that the aggregates

258
00:17:51,820 --> 00:17:55,040
 experience itself.

259
00:17:55,040 --> 00:17:58,010
 It's a different kind of understanding because, why I said

260
00:17:58,010 --> 00:17:59,720
 radical is because it sounds very

261
00:17:59,720 --> 00:18:03,520
 awful to think that experience is inherently suffering.

262
00:18:03,520 --> 00:18:06,480
 That's not really what it means, not exactly.

263
00:18:06,480 --> 00:18:08,600
 It's on a whole different level.

264
00:18:08,600 --> 00:18:12,350
 This one really, I think, you can take the Buddha's words

265
00:18:12,350 --> 00:18:14,520
 literally, it's not like any

266
00:18:14,520 --> 00:18:15,520
 other suffering.

267
00:18:15,520 --> 00:18:18,300
 Other kinds of suffering is, "Okay, that suffering will

268
00:18:18,300 --> 00:18:19,400
 stay away from it."

269
00:18:19,400 --> 00:18:23,680
 But the suffering of the aggregates is not like that.

270
00:18:23,680 --> 00:18:26,320
 It's in a sense a noble sort of suffering.

271
00:18:26,320 --> 00:18:29,140
 And you remember that understanding suffering is what we're

272
00:18:29,140 --> 00:18:30,560
 trying to do as Buddhists.

273
00:18:30,560 --> 00:18:32,760
 We're not trying to run away from it, we're trying to

274
00:18:32,760 --> 00:18:34,360
 understand it, mainly because while

275
00:18:34,360 --> 00:18:38,160
 it's suffering, it's bad.

276
00:18:38,160 --> 00:18:40,900
 But more deeply, we're trying to understand rather than, we

277
00:18:40,900 --> 00:18:42,160
're trying to change the way

278
00:18:42,160 --> 00:18:47,190
 we look at suffering rather than trying to run away from it

279
00:18:47,190 --> 00:18:47,240
.

280
00:18:47,240 --> 00:18:48,880
 There's freedom from suffering, right?

281
00:18:48,880 --> 00:18:52,770
 If you think it's just in painful feelings, you just run

282
00:18:52,770 --> 00:18:54,720
 away from the feelings.

283
00:18:54,720 --> 00:18:58,410
 If you think it's a part of reality, well then you go and

284
00:18:58,410 --> 00:19:00,280
 practice to change the way

285
00:19:00,280 --> 00:19:01,280
 you understand reality.

286
00:19:01,280 --> 00:19:06,560
 If it's part of all experiences, then you look deeper at

287
00:19:06,560 --> 00:19:09,080
 why that is and you start to

288
00:19:09,080 --> 00:19:11,920
 realize it's not actually the experiences.

289
00:19:11,920 --> 00:19:16,300
 To say that their suffering is not quite accurate, but they

290
00:19:16,300 --> 00:19:17,920
 can't satisfy you.

291
00:19:17,920 --> 00:19:21,170
 The suffering comes when you cling to anything, when you

292
00:19:21,170 --> 00:19:22,640
 cling to experience.

293
00:19:22,640 --> 00:19:26,330
 And in fact, seeing experience for what it is, is what

294
00:19:26,330 --> 00:19:28,560
 releases you from suffering.

295
00:19:28,560 --> 00:19:32,170
 So by paying attention to the aggregates, you come to

296
00:19:32,170 --> 00:19:34,760
 understand things much more clearly.

297
00:19:34,760 --> 00:19:39,160
 And it's sort of the highest form of suffering because it's

298
00:19:39,160 --> 00:19:41,240
 seeing that truth, the truth

299
00:19:41,240 --> 00:19:48,120
 of suffering, the truth of not being able to satisfy.

300
00:19:48,120 --> 00:19:54,320
 That causes you to let go and free yourself, become free

301
00:19:54,320 --> 00:19:56,520
 from suffering.

302
00:19:56,520 --> 00:19:59,920
 Attain or experience a state that is free from any kind of

303
00:19:59,920 --> 00:20:02,160
 stress or any, that is completely

304
00:20:02,160 --> 00:20:06,360
 peace, let's put it that way.

305
00:20:06,360 --> 00:20:09,900
 Which leads to the fourth part, Nati Santi Prang Sukang,

306
00:20:09,900 --> 00:20:11,480
 the most famous part of the

307
00:20:11,480 --> 00:20:12,480
 verse.

308
00:20:12,480 --> 00:20:17,880
 There is no happiness apart from peace, which is different

309
00:20:17,880 --> 00:20:19,640
 from the others.

310
00:20:19,640 --> 00:20:21,120
 It's a different formula.

311
00:20:21,120 --> 00:20:25,170
 Here we're not saying peace is unlike any other kind of

312
00:20:25,170 --> 00:20:26,280
 happiness.

313
00:20:26,280 --> 00:20:30,800
 The Buddha is actually saying there is no other happiness.

314
00:20:30,800 --> 00:20:32,760
 Anything else that you thought was happiness.

315
00:20:32,760 --> 00:20:38,190
 The point is, I think happiness itself is a bit of a red

316
00:20:38,190 --> 00:20:39,200
 herring.

317
00:20:39,200 --> 00:20:42,420
 It's a bit misleading.

318
00:20:42,420 --> 00:20:46,160
 The problem with happiness is you have to like it.

319
00:20:46,160 --> 00:20:50,510
 The problem with any other type of happiness is that it's

320
00:20:50,510 --> 00:20:52,560
 not really happiness because

321
00:20:52,560 --> 00:20:55,120
 you have to like it.

322
00:20:55,120 --> 00:20:59,090
 If you said it's okay to be happy but just don't like it,

323
00:20:59,090 --> 00:21:01,040
 it defeats the whole purpose

324
00:21:01,040 --> 00:21:03,760
 of happiness.

325
00:21:03,760 --> 00:21:06,240
 You don't like it, you can't really call it happiness.

326
00:21:06,240 --> 00:21:08,480
 That's how our formulation goes.

327
00:21:08,480 --> 00:21:10,560
 It's happiness because I like it.

328
00:21:10,560 --> 00:21:18,600
 And that's why we say happiness is subjective.

329
00:21:18,600 --> 00:21:21,440
 What makes one person happy doesn't make another person

330
00:21:21,440 --> 00:21:22,000
 happy.

331
00:21:22,000 --> 00:21:23,040
 Why?

332
00:21:23,040 --> 00:21:28,630
 Because one person likes it, another person doesn't like it

333
00:21:28,630 --> 00:21:28,680
.

334
00:21:28,680 --> 00:21:31,560
 So there are feelings that anyone could say, okay, we all

335
00:21:31,560 --> 00:21:33,120
 have this feeling and we could

336
00:21:33,120 --> 00:21:34,680
 say that's a happy feeling.

337
00:21:34,680 --> 00:21:39,960
 Without liking, it's not really something you would seek

338
00:21:39,960 --> 00:21:40,680
 out.

339
00:21:40,680 --> 00:21:42,680
 It's only because we like that feeling.

340
00:21:42,680 --> 00:21:46,000
 So even in meditation, we want to experience good feelings.

341
00:21:46,000 --> 00:21:51,120
 We like them and so we seek them out.

342
00:21:51,120 --> 00:21:52,120
 But there is an exception.

343
00:21:52,120 --> 00:21:55,400
 I think peace provides that exception.

344
00:21:55,400 --> 00:21:59,950
 You don't have to like peace or even seek it out for it to

345
00:21:59,950 --> 00:22:01,560
 be worthwhile.

346
00:22:01,560 --> 00:22:04,040
 It's a better kind of happiness.

347
00:22:04,040 --> 00:22:07,960
 The Buddha said the only kind of happiness is peace doesn't

348
00:22:07,960 --> 00:22:09,800
 require liking, it isn't

349
00:22:09,800 --> 00:22:14,100
 involved with liking, it doesn't even involve with feelings

350
00:22:14,100 --> 00:22:15,320
 necessarily.

351
00:22:15,320 --> 00:22:17,640
 Peace is like freedom.

352
00:22:17,640 --> 00:22:19,860
 You don't need something to be peaceful, you don't need

353
00:22:19,860 --> 00:22:20,880
 something to be free, you

354
00:22:20,880 --> 00:22:24,880
 just need to get rid of all the stuff that's getting you

355
00:22:24,880 --> 00:22:28,960
 caught up and causing you to be

356
00:22:28,960 --> 00:22:29,960
 un-peaceful.

357
00:22:29,960 --> 00:22:37,940
 So this is a good reflection, something when you're caught

358
00:22:37,940 --> 00:22:41,400
 up in the fire of passion and

359
00:22:41,400 --> 00:22:44,800
 you really are out of control.

360
00:22:44,800 --> 00:22:47,150
 Remind yourself that this is a fire, you're destroying

361
00:22:47,150 --> 00:22:48,600
 yourself, you're destroying the

362
00:22:48,600 --> 00:22:50,600
 mind.

363
00:22:50,600 --> 00:22:54,430
 You're cultivating, you're growing nothing, you're not

364
00:22:54,430 --> 00:22:56,440
 growing happiness, you're growing

365
00:22:56,440 --> 00:22:59,440
 nothing but anger.

366
00:22:59,440 --> 00:23:02,970
 And you're caught up in things that can't possibly satisfy

367
00:23:02,970 --> 00:23:04,680
 you because ultimately it's

368
00:23:04,680 --> 00:23:09,880
 just the aggregates which are unsatisfying.

369
00:23:09,880 --> 00:23:12,450
 The only thing that's truly you can say is satisfying is

370
00:23:12,450 --> 00:23:12,960
 peace.

371
00:23:12,960 --> 00:23:17,520
 So it doesn't require any sort of liking to be peaceful.

372
00:23:17,520 --> 00:23:21,500
 So a very good verse, something we should all maybe even

373
00:23:21,500 --> 00:23:23,800
 memorize, this would be a good

374
00:23:23,800 --> 00:23:28,600
 one to memorize.

375
00:23:28,600 --> 00:23:31,920
 And certainly a good one to reflect on.

376
00:23:31,920 --> 00:23:32,920
 That's the demo for tonight.

377
00:23:32,920 --> 00:23:33,920
 Thank you all for listening.

378
00:23:33,920 --> 00:23:34,920
 Thank you.

379
00:23:34,920 --> 00:23:51,000
 [

