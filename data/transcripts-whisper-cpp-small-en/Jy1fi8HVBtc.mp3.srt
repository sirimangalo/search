1
00:00:00,000 --> 00:00:07,000
 Good evening everyone.

2
00:00:07,000 --> 00:00:15,000
 Welcome to our evening session.

3
00:00:15,000 --> 00:00:28,000
 We all gather together locally or on the internet

4
00:00:28,000 --> 00:00:34,420
 to listen to the Dhamma, to discuss the Dhamma, to practice

5
00:00:34,420 --> 00:00:37,000
 the Dhamma.

6
00:00:37,000 --> 00:00:44,340
 Today I got word of a... I got word just yesterday, last

7
00:00:44,340 --> 00:00:48,000
 night I think, about a talk.

8
00:00:48,000 --> 00:00:52,000
 I was being held at McMaster University.

9
00:00:52,000 --> 00:00:55,000
 Something about Buddhism and Myanmar.

10
00:00:55,000 --> 00:00:58,230
 When I got there it was a pleasant surprise. They were

11
00:00:58,230 --> 00:00:59,000
 talking about our tradition.

12
00:00:59,000 --> 00:01:03,000
 They were talking about our meditation tradition.

13
00:01:03,000 --> 00:01:09,990
 There were others involved, but much of it was based around

14
00:01:09,990 --> 00:01:14,000
 the practice of mindfulness,

15
00:01:14,000 --> 00:01:22,000
 of insight based on the four foundations of mindfulness,

16
00:01:22,000 --> 00:01:26,160
 which in a scholarly setting it was quite incredible to

17
00:01:26,160 --> 00:01:27,000
 have people coming from

18
00:01:27,000 --> 00:01:32,420
 the University of Toronto and the speaker came from the

19
00:01:32,420 --> 00:01:39,000
 University of Virginia, I think.

20
00:01:39,000 --> 00:01:43,300
 It's quite encouraging and the talk was a lecture. I don't

21
00:01:43,300 --> 00:01:46,000
 know what you call it, the speaker.

22
00:01:46,000 --> 00:01:50,450
 It was quite encouraging as well. He talked a lot about how

23
00:01:50,450 --> 00:01:52,000
 mindfulness has evolved,

24
00:01:52,000 --> 00:01:57,000
 how practice has evolved as it's moved over time,

25
00:01:57,000 --> 00:02:02,550
 and the use of the word mindfulness, how it's become such a

26
00:02:02,550 --> 00:02:04,000
 catch word.

27
00:02:04,000 --> 00:02:20,700
 There are issues and questions of authenticity and keeping

28
00:02:20,700 --> 00:02:22,000
 with the tradition,

29
00:02:22,000 --> 00:02:28,000
 keeping with the original teachings.

30
00:02:28,000 --> 00:02:32,920
 The idea that the word mindfulness has come to mean

31
00:02:32,920 --> 00:02:36,000
 something different over time.

32
00:02:36,000 --> 00:02:40,670
 As a result, meditation practice has changed and people's

33
00:02:40,670 --> 00:02:41,000
 ideas of what it means

34
00:02:41,000 --> 00:02:44,000
 to practice the Buddha's teaching has changed.

35
00:02:44,000 --> 00:02:49,250
 Nonetheless, to hear people and to see a picture of Mahasya

36
00:02:49,250 --> 00:02:51,000
 Sayadaw on the board

37
00:02:51,000 --> 00:02:59,420
 was quite heartwarming even just to hear people talk about

38
00:02:59,420 --> 00:03:00,000
 him

39
00:03:00,000 --> 00:03:04,290
 and to talk about the use of insight meditation, the

40
00:03:04,290 --> 00:03:08,000
 practice of insight meditation,

41
00:03:08,000 --> 00:03:15,000
 to have a chance to talk about the discussion afterwards

42
00:03:15,000 --> 00:03:21,000
 got onto the topic of what is mindfulness.

43
00:03:21,000 --> 00:03:26,310
 One of the members of the audience was quite bold in his

44
00:03:26,310 --> 00:03:30,000
 conception that

45
00:03:30,000 --> 00:03:33,440
 mindfulness isn't good or bad. Mindfulness just fixes your

46
00:03:33,440 --> 00:03:36,000
 attention on an object or something,

47
00:03:36,000 --> 00:03:41,000
 reinforces... I'm not actually quite clear what is...

48
00:03:41,000 --> 00:03:44,760
 I don't think I quite agree with it, but the question came

49
00:03:44,760 --> 00:03:45,000
 up whether...

50
00:03:45,000 --> 00:03:53,140
 The idea was that a sniper, a killer, a soldier can be

51
00:03:53,140 --> 00:03:58,000
 quite mindful in what they're doing.

52
00:03:58,000 --> 00:04:04,140
 A cat can be quite... the cat idea was that it was quite

53
00:04:04,140 --> 00:04:05,000
 focused.

54
00:04:05,000 --> 00:04:09,750
 And yet if you know anything about the abhidhamma, these

55
00:04:09,750 --> 00:04:12,000
 are wholesome mind states.

56
00:04:12,000 --> 00:04:17,000
 So we have this idea that concentration is wholesome,

57
00:04:17,000 --> 00:04:18,000
 mindfulness is wholesome,

58
00:04:18,000 --> 00:04:21,400
 confidence is wholesome. So what about these people who are

59
00:04:21,400 --> 00:04:24,000
 confident, these evil...

60
00:04:24,000 --> 00:04:28,000
 people who are confident about their evil?

61
00:04:28,000 --> 00:04:37,650
 People who are confident in scamming, manipulating, in opp

62
00:04:37,650 --> 00:04:43,000
ressing others.

63
00:04:43,000 --> 00:04:48,250
 And so I had to give a sort of a bit of a correction, I

64
00:04:48,250 --> 00:04:51,000
 thought, to explain.

65
00:04:51,000 --> 00:04:52,560
 It's something that's interesting to us because it's an

66
00:04:52,560 --> 00:04:55,000
 interesting question of karma.

67
00:04:55,000 --> 00:04:59,000
 What is good karma and bad karma?

68
00:04:59,000 --> 00:05:03,000
 How can you say something's good karma or bad karma?

69
00:05:03,000 --> 00:05:11,000
 It's quite complicated.

70
00:05:11,000 --> 00:05:18,510
 It actually sounds like some sort of magic, something very

71
00:05:18,510 --> 00:05:21,000
 magical or spiritual.

72
00:05:21,000 --> 00:05:24,360
 When you kill, it sounds nice actually, when you kill it's

73
00:05:24,360 --> 00:05:26,000
 bad karma and bad things are going to happen to you,

74
00:05:26,000 --> 00:05:29,000
 you're going to get retribution.

75
00:05:29,000 --> 00:05:35,000
 And so it takes on this sort of mystical sort of air.

76
00:05:35,000 --> 00:05:41,000
 This idea that there's some karmic power or force.

77
00:05:41,000 --> 00:05:43,000
 But it's not exactly how it works.

78
00:05:43,000 --> 00:05:49,210
 Karma, with much of the Buddhist teachings, is really to be

79
00:05:49,210 --> 00:05:52,000
 understood as a meditator,

80
00:05:52,000 --> 00:05:57,000
 as someone practicing mindfulness.

81
00:05:57,000 --> 00:06:02,820
 Karma was this concept that was around before the Buddha

82
00:06:02,820 --> 00:06:05,000
 became enlightened,

83
00:06:05,000 --> 00:06:06,000
 but they had the wrong idea of it.

84
00:06:06,000 --> 00:06:11,000
 So he was just explaining to them what is actually potent.

85
00:06:11,000 --> 00:06:17,000
 And so what is actually potent is intention.

86
00:06:17,000 --> 00:06:20,020
 And so at first you think, well, if you intend to kill,

87
00:06:20,020 --> 00:06:21,000
 then killing's bad,

88
00:06:21,000 --> 00:06:30,000
 but it's not even that simple.

89
00:06:30,000 --> 00:06:34,260
 Because every moment, if you think about it, when you

90
00:06:34,260 --> 00:06:37,000
 intend to kill someone, that's one intention.

91
00:06:37,000 --> 00:06:41,000
 But the next moment you intend to pick up a knife.

92
00:06:41,000 --> 00:06:44,840
 The next moment you intend to go and find the person you're

93
00:06:44,840 --> 00:06:46,000
 going to kill.

94
00:06:46,000 --> 00:06:49,000
 And every moment there's an intention.

95
00:06:49,000 --> 00:06:54,110
 It's not even exactly intention, it's your volition or your

96
00:06:54,110 --> 00:06:55,000
 bent,

97
00:06:55,000 --> 00:07:00,000
 your inclination at that moment, your state of mind really.

98
00:07:00,000 --> 00:07:08,000
 It's your chitana.

99
00:07:08,000 --> 00:07:11,660
 And so I was saying, well, my understanding from the Abhid

100
00:07:11,660 --> 00:07:12,000
hamma,

101
00:07:12,000 --> 00:07:15,280
 I didn't want to sort of preach as a meditation teacher,

102
00:07:15,280 --> 00:07:19,000
 but I can preach to all of you.

103
00:07:19,000 --> 00:07:24,570
 So as we understand it as meditators, karma is every moment

104
00:07:24,570 --> 00:07:25,000
.

105
00:07:25,000 --> 00:07:28,480
 So if you say a killer has mindfulness, well, they don't

106
00:07:28,480 --> 00:07:29,000
 have mindfulness

107
00:07:29,000 --> 00:07:31,000
 at the moment when they want to kill.

108
00:07:31,000 --> 00:07:33,000
 At that moment there's no mindfulness.

109
00:07:33,000 --> 00:07:36,000
 They are. I'm actually not sure.

110
00:07:36,000 --> 00:07:39,000
 There's no wholesomeness.

111
00:07:39,000 --> 00:07:41,000
 It's funny now, my Abhidhamma is not clear,

112
00:07:41,000 --> 00:07:44,800
 but they were saying that mindfulness is also always wholes

113
00:07:44,800 --> 00:07:46,000
ome according to the Abhidhamma.

114
00:07:46,000 --> 00:07:47,000
 I'm not so sure.

115
00:07:47,000 --> 00:07:52,000
 Look at this thing, I'm going to get myself in trouble.

116
00:07:52,000 --> 00:07:54,000
 It doesn't really matter.

117
00:07:54,000 --> 00:07:57,000
 We're so interested in the technicalities, not right now.

118
00:07:57,000 --> 00:08:03,000
 Please forgive me for being imperfect.

119
00:08:03,000 --> 00:08:10,000
 But the point being, no action is hard to find an action

120
00:08:10,000 --> 00:08:12,000
 that's entirely wholesome or unwholesome

121
00:08:12,000 --> 00:08:16,000
 because wholesome really is referring to the individual,

122
00:08:16,000 --> 00:08:21,550
 that which brings you happiness, peace, goodness, or bring

123
00:08:21,550 --> 00:08:23,000
 good things to you.

124
00:08:23,000 --> 00:08:26,000
 In other words, things that bring success.

125
00:08:26,000 --> 00:08:31,000
 So a person's ability to successfully shoot someone else,

126
00:08:31,000 --> 00:08:34,000
 it actually requires moments of wholesomeness,

127
00:08:34,000 --> 00:08:41,280
 moments where you are focused and concentrated, present,

128
00:08:41,280 --> 00:08:43,000
 confident.

129
00:08:43,000 --> 00:08:46,000
 The problem is there's so much overarching

130
00:08:46,000 --> 00:08:53,000
 and so many moments of very strong ignorance, hatred,

131
00:08:53,000 --> 00:08:57,000
 disregard for people for life.

132
00:08:57,000 --> 00:09:00,400
 And this is true because we're not always thinking about

133
00:09:00,400 --> 00:09:01,000
 killing a person.

134
00:09:01,000 --> 00:09:08,000
 Often we're thinking about lifting up our gun and so on.

135
00:09:08,000 --> 00:09:12,400
 It'd be mostly unwholesome, but there will still be moments

136
00:09:12,400 --> 00:09:14,000
 of wholesomeness.

137
00:09:14,000 --> 00:09:19,000
 An easier example might be when you do something good,

138
00:09:19,000 --> 00:09:22,000
 when you give a gift to someone,

139
00:09:22,000 --> 00:09:24,000
 you say, "Well, that's wholesome, right?"

140
00:09:24,000 --> 00:09:26,000
 You say, "Giving is good."

141
00:09:26,000 --> 00:09:32,000
 Not exactly. Not necessarily. Not entirely.

142
00:09:32,000 --> 00:09:35,000
 I remember when we used to give gifts to Ajahn Tong,

143
00:09:35,000 --> 00:09:39,000
 everyone was so worried. I had to do it just right.

144
00:09:39,000 --> 00:09:42,640
 I started thinking, "You know, this worry is not wholesome

145
00:09:42,640 --> 00:09:43,000
."

146
00:09:43,000 --> 00:09:45,800
 Getting all this stuff, sitting there, we have to sit

147
00:09:45,800 --> 00:09:47,000
 around and wait for Ajahn Tong to arrive,

148
00:09:47,000 --> 00:09:52,460
 and he's always late, kind of grumpy. And you think, "Well,

149
00:09:52,460 --> 00:09:54,000
 that's not wholesome."

150
00:09:54,000 --> 00:09:57,000
 It's much more complicated, you see.

151
00:09:57,000 --> 00:09:59,000
 Meditation, when people say, "Yes, I'm going to meditate."

152
00:09:59,000 --> 00:10:03,000
 Oh, good.

153
00:10:03,000 --> 00:10:04,480
 There's a lot of unwholesomeness that comes up from

154
00:10:04,480 --> 00:10:05,000
 meditation.

155
00:10:05,000 --> 00:10:08,000
 You see, it's not something to be afraid of.

156
00:10:08,000 --> 00:10:10,480
 It's not like, "Oh, well, I better not go and meditate or I

157
00:10:10,480 --> 00:10:13,000
'll get angry."

158
00:10:13,000 --> 00:10:18,390
 It's not magic. It's not a demon that you have to be afraid

159
00:10:18,390 --> 00:10:19,000
 of.

160
00:10:19,000 --> 00:10:24,000
 It's science. It's really psychology.

161
00:10:24,000 --> 00:10:28,000
 There are things that tear your mind apart, that remove

162
00:10:28,000 --> 00:10:30,000
 your mind's ability to function,

163
00:10:30,000 --> 00:10:40,820
 that debilitate, that decapacitate, that make you incapac

164
00:10:40,820 --> 00:10:42,000
itate you.

165
00:10:42,000 --> 00:10:53,000
 Weaken the mind, cripple the mind, shrink the mind.

166
00:10:53,000 --> 00:10:55,000
 But those are just moments.

167
00:10:55,000 --> 00:10:57,580
 And in meditation, we're very much interested in studying

168
00:10:57,580 --> 00:10:58,000
 this.

169
00:10:58,000 --> 00:11:01,000
 We don't have to be afraid of it.

170
00:11:01,000 --> 00:11:08,000
 It's like you're studying diseases or you're studying pain.

171
00:11:08,000 --> 00:11:12,660
 We want to understand it. So we're willing to allow

172
00:11:12,660 --> 00:11:13,000
 ourselves,

173
00:11:13,000 --> 00:11:16,490
 give ourselves the room to be angry, give ourselves the

174
00:11:16,490 --> 00:11:19,000
 space to commit unwholesome karma,

175
00:11:19,000 --> 00:11:20,000
 even as meditators.

176
00:11:20,000 --> 00:11:22,000
 Meditation isn't entirely wholesome.

177
00:11:22,000 --> 00:11:25,060
 There's a lot of unwholesomeness in insight meditation,

178
00:11:25,060 --> 00:11:27,000
 getting angry.

179
00:11:27,000 --> 00:11:30,000
 This is why people are skeptical about it sometimes.

180
00:11:30,000 --> 00:11:33,000
 And they think you have to do what the Buddha said.

181
00:11:33,000 --> 00:11:38,380
 The Buddha was inclined to have his meditators because it's

182
00:11:38,380 --> 00:11:41,000
 much more wholesome to do samatha first,

183
00:11:41,000 --> 00:11:44,240
 focus your mind and separate your mind away from unwholes

184
00:11:44,240 --> 00:11:45,000
omeness.

185
00:11:45,000 --> 00:11:49,340
 It takes a lot of time and a lot of effort and a lot of

186
00:11:49,340 --> 00:11:52,000
 ideal conditions.

187
00:11:52,000 --> 00:11:55,190
 Nowadays it's much more difficult for people whose minds

188
00:11:55,190 --> 00:11:56,000
 are consumed.

189
00:11:56,000 --> 00:11:58,890
 And it's so much ignorance that we're not even able to tell

190
00:11:58,890 --> 00:12:00,000
 that these are bad,

191
00:12:00,000 --> 00:12:05,000
 these things are bad. It's much quicker and simpler for us

192
00:12:05,000 --> 00:12:07,000
 to just muddle through it

193
00:12:07,000 --> 00:12:13,000
 and see directly, "Oh yeah, causing myself harm."

194
00:12:13,000 --> 00:12:26,200
 So good and bad are momentary. It's quite scary actually if

195
00:12:26,200 --> 00:12:27,000
 you think about it.

196
00:12:27,000 --> 00:12:32,040
 Our whole day, every day, throughout our day, we're doing

197
00:12:32,040 --> 00:12:37,000
 countless, countless unwholesome deeds.

198
00:12:37,000 --> 00:12:41,000
 We're doing lots of wholesome deeds as well hopefully.

199
00:12:41,000 --> 00:12:43,000
 But those deeds are just momentary.

200
00:12:43,000 --> 00:12:47,360
 It means we have moments of wholesomeness and unwholesomen

201
00:12:47,360 --> 00:12:48,000
ess.

202
00:12:48,000 --> 00:12:53,210
 Karma is only really scary when it becomes a habit, ajnakam

203
00:12:53,210 --> 00:12:55,000
 it's called,

204
00:12:55,000 --> 00:12:58,050
 or when it's extreme when we let it get to the point where

205
00:12:58,050 --> 00:12:59,000
 we do something,

206
00:12:59,000 --> 00:13:04,070
 we have a moment that's just so unforgivable, like killing

207
00:13:04,070 --> 00:13:05,000
 your parents

208
00:13:05,000 --> 00:13:13,090
 or hurting a Buddha, that kind of thing, dropping a rock on

209
00:13:13,090 --> 00:13:14,000
 the Buddha.

210
00:13:14,000 --> 00:13:19,650
 But that's when it's only really, you know, karma is not

211
00:13:19,650 --> 00:13:21,000
 something to be afraid of,

212
00:13:21,000 --> 00:13:25,000
 something to understand, something to go beyond really.

213
00:13:25,000 --> 00:13:28,000
 It's also not something to get caught up in, to think,

214
00:13:28,000 --> 00:13:32,900
 "I'm going to do lots of good karma and that'll make me

215
00:13:32,900 --> 00:13:34,000
 happy."

216
00:13:34,000 --> 00:13:39,000
 We have to go beyond karma by studying it.

217
00:13:39,000 --> 00:13:42,000
 And this is, so it is everywhere.

218
00:13:42,000 --> 00:13:44,540
 It's in everything we do. When we give, there's good karma,

219
00:13:44,540 --> 00:13:45,000
 there's bad karma.

220
00:13:45,000 --> 00:13:48,350
 When we kill, there's mostly bad karma, but there's still

221
00:13:48,350 --> 00:13:50,000
 going to be some moments potentially

222
00:13:50,000 --> 00:14:00,000
 of wholesomeness.

223
00:14:00,000 --> 00:14:02,000
 There could still be moments of wholesomeness.

224
00:14:02,000 --> 00:14:05,540
 I don't want to get in trouble here with these radical

225
00:14:05,540 --> 00:14:06,000
 ideas,

226
00:14:06,000 --> 00:14:14,000
 but it seems to me that there would be moments.

227
00:14:14,000 --> 00:14:17,640
 But the point is that only a meditator can truly understand

228
00:14:17,640 --> 00:14:18,000
 this.

229
00:14:18,000 --> 00:14:21,210
 When you're meditating, you're able to see that which

230
00:14:21,210 --> 00:14:22,000
 causes you suffering,

231
00:14:22,000 --> 00:14:24,000
 that which causes you peace.

232
00:14:24,000 --> 00:14:29,000
 I remember when I was hunting, when I was a teenager,

233
00:14:29,000 --> 00:14:34,000
 and I was sitting up in a tree with a crow with a bow.

234
00:14:34,000 --> 00:14:38,000
 It was the most awful thing to think of now.

235
00:14:38,000 --> 00:14:42,000
 And I had to sit for a long time and wait.

236
00:14:42,000 --> 00:14:46,000
 And it ended up being quite a spiritual experience.

237
00:14:46,000 --> 00:14:51,000
 This bird came and landed really right above my head.

238
00:14:51,000 --> 00:14:55,240
 I heard listening to the birds floating by, watching the

239
00:14:55,240 --> 00:14:56,000
 sunset,

240
00:14:56,000 --> 00:15:00,360
 watching it get dark, just sitting alone in the forest up

241
00:15:00,360 --> 00:15:02,000
 in the tree,

242
00:15:02,000 --> 00:15:04,000
 and it started to get dark.

243
00:15:04,000 --> 00:15:07,000
 No, it didn't quite get, before it got dark,

244
00:15:07,000 --> 00:15:12,000
 these deer came out into the clearing, and I was, "Ah, I'm

245
00:15:12,000 --> 00:15:12,000
 going to get ready."

246
00:15:12,000 --> 00:15:16,000
 Now is the time. But they turned out they were female,

247
00:15:16,000 --> 00:15:20,000
 a female and young, and you're not allowed to kill them.

248
00:15:20,000 --> 00:15:22,000
 You're going to need a special permit.

249
00:15:22,000 --> 00:15:24,000
 So, okay, I knew I was...

250
00:15:24,000 --> 00:15:27,480
 And they came right up and they started eating from the

251
00:15:27,480 --> 00:15:28,000
 tree, right?

252
00:15:28,000 --> 00:15:30,000
 And I was sitting here and I was sitting here,

253
00:15:30,000 --> 00:15:33,000
 and I was looking into the eyes of this female deer.

254
00:15:33,000 --> 00:15:36,480
 And she looked at me, and I looked at her, and she's

255
00:15:36,480 --> 00:15:39,000
 chewing on me.

256
00:15:39,000 --> 00:15:42,000
 It was awful to think back that that was the sort of thing

257
00:15:42,000 --> 00:15:43,000
 I was interested in,

258
00:15:43,000 --> 00:15:49,000
 but just to point out, karma is not so simple.

259
00:15:49,000 --> 00:15:54,000
 You think of Anguly Mala and all of his intent to kill,

260
00:15:54,000 --> 00:15:58,010
 and then he ended up becoming an arahant, not because of

261
00:15:58,010 --> 00:15:59,000
 all the killing,

262
00:15:59,000 --> 00:16:04,390
 but because during that time he came to understand

263
00:16:04,390 --> 00:16:06,000
 suffering.

264
00:16:06,000 --> 00:16:09,000
 You could even argue that through doing evil deeds,

265
00:16:09,000 --> 00:16:11,000
 a person realizes how awful they are.

266
00:16:11,000 --> 00:16:13,000
 Sometimes it takes doing an evil deed.

267
00:16:13,000 --> 00:16:16,000
 Sometimes it takes someone to be addicted to drugs

268
00:16:16,000 --> 00:16:18,000
 in order to know that drugs are wrong.

269
00:16:18,000 --> 00:16:20,720
 I don't want to give the impression that there's nothing

270
00:16:20,720 --> 00:16:21,000
 wrong with evil,

271
00:16:21,000 --> 00:16:25,000
 and this is a good thing to do, but it's complicated.

272
00:16:25,000 --> 00:16:28,000
 The Buddha said himself, "Understanding karma?

273
00:16:28,000 --> 00:16:32,000
 Only a Buddha can really understand it."

274
00:16:32,000 --> 00:16:36,420
 But even the understanding that we have is that it's not

275
00:16:36,420 --> 00:16:38,000
 something easy to understand.

276
00:16:38,000 --> 00:16:41,000
 So I thought it would be good to talk about it,

277
00:16:41,000 --> 00:16:44,400
 because I want to impress upon you that this is a good way

278
00:16:44,400 --> 00:16:45,000
 of describing

279
00:16:45,000 --> 00:16:48,000
 what we're doing in the practice. We're sorting it out.

280
00:16:48,000 --> 00:16:52,720
 We're coming to see our habits, the karma that we've built

281
00:16:52,720 --> 00:16:54,000
 up as habits,

282
00:16:54,000 --> 00:16:57,000
 and realizing that some of it's not useful, not beneficial.

283
00:16:57,000 --> 00:17:01,000
 Some of it's outright harmful.

284
00:17:01,000 --> 00:17:03,200
 It's not even about asking whether this is a good deed or

285
00:17:03,200 --> 00:17:05,000
 that is a good deed.

286
00:17:05,000 --> 00:17:08,000
 It's about understanding mind states

287
00:17:08,000 --> 00:17:11,000
 and seeing for ourselves what's a good mind state,

288
00:17:11,000 --> 00:17:14,000
 what's a bad mind state.

289
00:17:14,000 --> 00:17:16,000
 You can see clearly because this one leads to suffering,

290
00:17:16,000 --> 00:17:19,000
 this one leads to happiness.

291
00:17:19,000 --> 00:17:23,000
 It's undeniable and it doesn't change.

292
00:17:23,000 --> 00:17:26,000
 You can ever get angry and then say, "Oh, that was fun."

293
00:17:26,000 --> 00:17:30,000
 I'm angry and look at how much happiness that brought me.

294
00:17:30,000 --> 00:17:33,000
 Greed will never make you satisfied.

295
00:17:33,000 --> 00:17:41,000
 It has a very distinct nature that doesn't change.

296
00:17:41,000 --> 00:17:44,000
 That's the law of karma. That's what we mean.

297
00:17:44,000 --> 00:17:49,340
 It's a law. It might as well be a law because it doesn't

298
00:17:49,340 --> 00:17:51,000
 ever change.

299
00:17:51,000 --> 00:17:56,380
 Anyway, so in meditation, just being mindful, watching and

300
00:17:56,380 --> 00:17:57,000
 seeing,

301
00:17:57,000 --> 00:18:00,000
 we see three things.

302
00:18:00,000 --> 00:18:10,000
 We see the defilement, we see the karma, we see the result.

303
00:18:10,000 --> 00:18:13,000
 This is the wheel of karma. You've never heard of this.

304
00:18:13,000 --> 00:18:24,000
 There's kilesa kamvibhaka, or jaitana kamvibhaka.

305
00:18:24,000 --> 00:18:28,640
 You have the unwholesome mind state that leads you to do

306
00:18:28,640 --> 00:18:29,000
 things,

307
00:18:29,000 --> 00:18:32,000
 leads you to kill, leads you to steal,

308
00:18:32,000 --> 00:18:37,000
 leads you even to think bad thoughts.

309
00:18:37,000 --> 00:18:41,000
 Then there's the result.

310
00:18:41,000 --> 00:18:45,200
 We see in meditation how different mind states bring

311
00:18:45,200 --> 00:18:46,000
 different results.

312
00:18:46,000 --> 00:18:50,600
 It's chaotic. It's not like it's going to show it in some

313
00:18:50,600 --> 00:18:52,000
 chart or something.

314
00:18:52,000 --> 00:18:55,000
 It's not going to be able to chart it.

315
00:18:55,000 --> 00:19:00,000
 But you'll get a sense. You'll start to see thinking a lot.

316
00:19:00,000 --> 00:19:05,000
 Thinking a lot gives me a headache. There's delusion,

317
00:19:05,000 --> 00:19:08,000
 frustration.

318
00:19:08,000 --> 00:19:12,910
 Frustration makes me tired, makes me feel hot and sick

319
00:19:12,910 --> 00:19:14,000
 inside.

320
00:19:14,000 --> 00:19:20,000
 Greed makes me feel kind of dirty or greasy or unpleasant.

321
00:19:20,000 --> 00:19:26,000
 Makes me feel agitated, makes me feel hot as well.

322
00:19:26,000 --> 00:19:35,000
 It burns us up inside.

323
00:19:35,000 --> 00:19:41,000
 And we start to naturally incline away from bad karma,

324
00:19:41,000 --> 00:19:48,000
 but also away from trying to make good karma.

325
00:19:48,000 --> 00:19:51,790
 As we start to understand karma more and more, we start to

326
00:19:51,790 --> 00:19:53,000
 lose our ambitions.

327
00:19:53,000 --> 00:19:58,000
 We start to lose the need, the drive to do this or that.

328
00:19:58,000 --> 00:20:01,000
 But we become more content and more resolved.

329
00:20:01,000 --> 00:20:05,000
 It's not that we lose our effort or we become lazy.

330
00:20:05,000 --> 00:20:13,000
 We become more intent on being.

331
00:20:13,000 --> 00:20:18,000
 Not becoming, not just being.

332
00:20:18,000 --> 00:20:23,000
 I'm not even being, but in the sense of stopping,

333
00:20:23,000 --> 00:20:33,000
 staying put and become very interested and focused.

334
00:20:33,000 --> 00:20:36,850
 All of our energy goes into not doing anything, into

335
00:20:36,850 --> 00:20:42,000
 stopping, into staying.

336
00:20:42,000 --> 00:20:47,000
 Into being, you know, just...being isn't the right word,

337
00:20:47,000 --> 00:20:52,420
 but what it means is not, not being, not striving, not

338
00:20:52,420 --> 00:20:54,000
 reaching.

339
00:20:54,000 --> 00:21:02,000
 In a sense, striving to, striving to stop striving.

340
00:21:02,000 --> 00:21:07,000
 Striving to stop karma.

341
00:21:07,000 --> 00:21:12,960
 Anyway, some scattered thoughts, interesting, very

342
00:21:12,960 --> 00:21:15,000
 interesting topic.

343
00:21:15,000 --> 00:21:18,950
 And again, just want to think about how wonderful it is

344
00:21:18,950 --> 00:21:22,000
 that meditation is taking off

345
00:21:22,000 --> 00:21:26,000
 or has taken off and is really a part of the world.

346
00:21:26,000 --> 00:21:34,020
 You know, we think of the world as being full of problems,

347
00:21:34,020 --> 00:21:36,000
 full of dangers,

348
00:21:36,000 --> 00:21:42,000
 full of unwholesomeness really.

349
00:21:42,000 --> 00:21:47,360
 There's a lot of, I guess for lack of a better word, there

350
00:21:47,360 --> 00:21:49,000
's a lot of evil in the world,

351
00:21:49,000 --> 00:21:51,000
 if you look at it that way.

352
00:21:51,000 --> 00:21:55,000
 I don't think it's so useful to dwell upon the evil.

353
00:21:55,000 --> 00:22:00,670
 You know, there's a lot of talk about activism and getting

354
00:22:00,670 --> 00:22:02,000
 involved.

355
00:22:02,000 --> 00:22:05,630
 I'm not going to say anything, I'm not going to criticize

356
00:22:05,630 --> 00:22:11,000
 it, but I think there's room for arguably the, you know,

357
00:22:11,000 --> 00:22:16,000
 the idea of focusing on the good, like Pollyanna sort of,

358
00:22:16,000 --> 00:22:19,000
 if you ever read the book Pollyanna.

359
00:22:19,000 --> 00:22:22,000
 Or was it a book? Yeah.

360
00:22:22,000 --> 00:22:25,910
 You know, sometimes if you focus on the good it becomes

361
00:22:25,910 --> 00:22:27,000
 your universe.

362
00:22:27,000 --> 00:22:33,460
 Buddhism, I think there's room to talk about this sort of

363
00:22:33,460 --> 00:22:35,000
 attitude.

364
00:22:35,000 --> 00:22:38,510
 If we think about all the meditation that goes on and we

365
00:22:38,510 --> 00:22:41,000
 gain confidence and encouragement from that

366
00:22:41,000 --> 00:22:43,490
 and we work and we focus all of our efforts, not on

367
00:22:43,490 --> 00:22:48,000
 changing the world or correcting people's wrong views,

368
00:22:48,000 --> 00:22:53,120
 but work to support people and to support the practice of

369
00:22:53,120 --> 00:22:56,000
 meditation, to encourage others to meditate.

370
00:22:56,000 --> 00:23:00,000
 Encourage the cultivation of right view.

371
00:23:00,000 --> 00:23:07,440
 I think this is what has had this great and lasting impact,

372
00:23:07,440 --> 00:23:09,000
 bringing goodness to people.

373
00:23:09,000 --> 00:23:13,260
 You know, it's never going to fix the world, but it's

374
00:23:13,260 --> 00:23:18,000
 certainly a good way to live.

375
00:23:18,000 --> 00:23:22,800
 I think that's the point. We can't fix the world, we can't

376
00:23:22,800 --> 00:23:26,000
 change the world, but we can live well.

377
00:23:26,000 --> 00:23:30,000
 We can set ourselves on what's right.

378
00:23:30,000 --> 00:23:34,000
 We can set ourselves on the right path.

379
00:23:34,000 --> 00:23:41,000
 And we can have our lives be an outpouring of goodness.

380
00:23:41,000 --> 00:23:44,290
 It is through many things, but really the core of it is we

381
00:23:44,290 --> 00:23:46,000
 do it through meditation.

382
00:23:46,000 --> 00:23:52,480
 So it's awesome to see so many people interested here and

383
00:23:52,480 --> 00:23:56,000
 interested around the world, it sounds like,

384
00:23:56,000 --> 00:24:00,110
 from what we hear. We hear reports more and more of people

385
00:24:00,110 --> 00:24:04,000
 interested in meditation and mindfulness.

386
00:24:04,000 --> 00:24:08,630
 And there you go. That's the Dhamma for tonight. Thank you

387
00:24:08,630 --> 00:24:10,000
 all for tuning in.

