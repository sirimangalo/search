1
00:00:00,000 --> 00:00:04,160
 Hello and welcome back to Ask a Monk. Today's question is

2
00:00:04,160 --> 00:00:06,280
 about drowsiness. So I

3
00:00:06,280 --> 00:00:11,230
 was asked how to deal with falling asleep when meditating.

4
00:00:11,230 --> 00:00:11,880
 Someone said they

5
00:00:11,880 --> 00:00:14,190
 would like to be able to meditate and they've tried but

6
00:00:14,190 --> 00:00:15,160
 they find themselves

7
00:00:15,160 --> 00:00:18,330
 falling asleep every time. I think I've addressed this

8
00:00:18,330 --> 00:00:20,080
 before but not

9
00:00:20,080 --> 00:00:24,560
 comprehensively so I'd like to do that now, sort of

10
00:00:24,560 --> 00:00:26,480
 referring back to the Buddha's

11
00:00:26,480 --> 00:00:32,730
 teaching. Now the the person who asked the question this

12
00:00:32,730 --> 00:00:34,880
 time said that he would

13
00:00:34,880 --> 00:00:40,540
 like he or she would like to use the meditation for the

14
00:00:40,540 --> 00:00:43,320
 purpose of feeling

15
00:00:43,320 --> 00:00:46,720
 rested. So they want to be able to find rest but still be

16
00:00:46,720 --> 00:00:48,600
 alert and I'd like to

17
00:00:48,600 --> 00:00:53,580
 caution that that may be the reason why you're feeling

18
00:00:53,580 --> 00:00:54,360
 falling asleep when you

19
00:00:54,360 --> 00:00:57,500
 meditate because you've got the wrong understanding of what

20
00:00:57,500 --> 00:00:58,600
 meditation should

21
00:00:58,600 --> 00:01:04,340
 be. If your intention is to simply rest then falling asleep

22
00:01:04,340 --> 00:01:05,680
 is probably the best

23
00:01:05,680 --> 00:01:10,880
 way to do it to an extent. I mean I guess if that's the

24
00:01:10,880 --> 00:01:12,680
 only the point is

25
00:01:12,680 --> 00:01:15,480
 if that's the only intention that you go into the

26
00:01:15,480 --> 00:01:17,320
 meditation with and that's the

27
00:01:17,320 --> 00:01:21,440
 direction that your mind is going to go. If you sit down

28
00:01:21,440 --> 00:01:23,180
 and meditate and say okay

29
00:01:23,180 --> 00:01:25,900
 I want to feel rested your mind is not going to have to

30
00:01:25,900 --> 00:01:27,260
 develop energy it's not

31
00:01:27,260 --> 00:01:33,940
 going to be be awake and alert it's going to start to tend

32
00:01:33,940 --> 00:01:35,660
 towards the only

33
00:01:35,660 --> 00:01:38,820
 way that it knows how to rest and that is to fall asleep.

34
00:01:38,820 --> 00:01:39,440
 There are better ways

35
00:01:39,440 --> 00:01:43,020
 and meditation I think certainly does lead to much more of

36
00:01:43,020 --> 00:01:44,240
 a rested mind state

37
00:01:44,240 --> 00:01:48,530
 but the mind won't go in that direction unless you your

38
00:01:48,530 --> 00:01:49,760
 intention in going into

39
00:01:49,760 --> 00:01:52,800
 the meditation is to use it for the purpose of meditating

40
00:01:52,800 --> 00:01:53,600
 and that is to

41
00:01:53,600 --> 00:01:57,090
 understand reality and to be able to deal with the

42
00:01:57,090 --> 00:01:58,040
 difficulties and the

43
00:01:58,040 --> 00:02:01,120
 problems and to understand the difficulties and the

44
00:02:01,120 --> 00:02:02,200
 problems that you

45
00:02:02,200 --> 00:02:06,390
 have in your mind and in your life. If you go into the

46
00:02:06,390 --> 00:02:08,000
 meditation with this

47
00:02:08,000 --> 00:02:12,160
 mindset that this sort of mindset then your mind is more

48
00:02:12,160 --> 00:02:14,080
 likely to be alert and

49
00:02:14,080 --> 00:02:16,870
 to be interested and you're more likely to be able to pay

50
00:02:16,870 --> 00:02:17,880
 attention less likely

51
00:02:17,880 --> 00:02:21,200
 to fall asleep. So that's the first bit of advice I would

52
00:02:21,200 --> 00:02:21,880
 give is to try to

53
00:02:21,880 --> 00:02:26,740
 reevaluate what you expect to get out of meditation. Rest

54
00:02:26,740 --> 00:02:28,680
ing is certainly not the

55
00:02:28,680 --> 00:02:33,920
 the purpose of practicing meditation not the best aim and

56
00:02:33,920 --> 00:02:34,600
 if that's the

57
00:02:34,600 --> 00:02:37,430
 aim going into it it's as I said going to have these sorts

58
00:02:37,430 --> 00:02:38,440
 of repercussions

59
00:02:38,440 --> 00:02:42,720
 causing you to fall asleep. Nonetheless for those medit

60
00:02:42,720 --> 00:02:45,040
ators who have made up

61
00:02:45,040 --> 00:02:47,340
 their mind and come to understand what it is that

62
00:02:47,340 --> 00:02:48,680
 meditation is for and are

63
00:02:48,680 --> 00:02:52,860
 using it for the purpose of coming to understand reality as

64
00:02:52,860 --> 00:02:54,240
 it is there are

65
00:02:54,240 --> 00:02:57,530
 times especially when doing intensive meditation that you

66
00:02:57,530 --> 00:02:58,200
 find yourself

67
00:02:58,200 --> 00:03:01,850
 feeling drowsy and even falling asleep. So how do you deal

68
00:03:01,850 --> 00:03:03,860
 with this? Well the

69
00:03:03,860 --> 00:03:08,720
 Buddha gave seven specific techniques that one could use to

70
00:03:08,720 --> 00:03:10,400
 overcome drowsiness

71
00:03:10,400 --> 00:03:15,390
 and there are actually seven different sort of types of

72
00:03:15,390 --> 00:03:16,160
 they deal with seven

73
00:03:16,160 --> 00:03:19,050
 different types of drowsiness or at least a few different

74
00:03:19,050 --> 00:03:20,000
 types of drowsiness

75
00:03:20,000 --> 00:03:24,650
 each one is is unique in its its approach and so I'd like

76
00:03:24,650 --> 00:03:25,840
 to in this case

77
00:03:25,840 --> 00:03:30,950
 refer directly back to this it's the Pachalaayana Sutta or

78
00:03:30,950 --> 00:03:32,360
 Pachala Sutta or

79
00:03:32,360 --> 00:03:35,750
 Chappala Sutta it's in the Anguttra Nikaya book of sevens

80
00:03:35,750 --> 00:03:37,400
 so we have seven

81
00:03:37,400 --> 00:03:42,800
 seven ways of dealing with drowsiness. So the first way

82
00:03:42,800 --> 00:03:44,440
 that the Buddha said is to

83
00:03:44,440 --> 00:03:49,370
 change your object or to examine the object of your

84
00:03:49,370 --> 00:03:51,800
 attention because one of

85
00:03:51,800 --> 00:03:58,320
 the most obvious way obvious reasons why someone might

86
00:03:58,320 --> 00:03:59,440
 become drowsy is because

87
00:03:59,440 --> 00:04:02,650
 their mind has begun to wander. When you're sitting in

88
00:04:02,650 --> 00:04:04,280
 meditation you begin

89
00:04:04,280 --> 00:04:08,040
 by focusing on a specific object but your mind starts to

90
00:04:08,040 --> 00:04:09,440
 wander and slowly

91
00:04:09,440 --> 00:04:15,210
 fall into a more trance like state that that is bordering

92
00:04:15,210 --> 00:04:16,520
 on sleep and

93
00:04:16,520 --> 00:04:21,070
 eventually will lead one to fall asleep. Now when that is

94
00:04:21,070 --> 00:04:22,680
 the case it's important

95
00:04:22,680 --> 00:04:26,950
 to think about what it was that you were you were

96
00:04:26,950 --> 00:04:28,120
 considering when you were when

97
00:04:28,120 --> 00:04:33,260
 you were feeling drowsy and to change that to go back to

98
00:04:33,260 --> 00:04:34,840
 focusing on the the

99
00:04:34,840 --> 00:04:39,010
 original object and to be careful not to let yourself fall

100
00:04:39,010 --> 00:04:40,720
 into the reflection on

101
00:04:40,720 --> 00:04:44,880
 these unspeculate speculative thoughts that are going to

102
00:04:44,880 --> 00:04:45,520
 lead your mind to

103
00:04:45,520 --> 00:04:48,380
 wander. Often it's the case that we'll start to remember

104
00:04:48,380 --> 00:04:49,120
 things that we're

105
00:04:49,120 --> 00:04:51,690
 worried about or concerned about and that will lead our

106
00:04:51,690 --> 00:04:52,640
 minds to wander and

107
00:04:52,640 --> 00:04:57,910
 speculate and eventually get tired and and fall into fall

108
00:04:57,910 --> 00:05:00,600
 asleep. So the Buddha

109
00:05:00,600 --> 00:05:03,350
 said be careful not to give those sorts of thoughts any

110
00:05:03,350 --> 00:05:05,200
 ground whatever thought

111
00:05:05,200 --> 00:05:10,160
 it was that was causing you to feel tired to avoid that

112
00:05:10,160 --> 00:05:11,520
 that thought and not

113
00:05:11,520 --> 00:05:18,170
 think might not develop that that train of thought or state

114
00:05:18,170 --> 00:05:20,440
 of mind. Of course

115
00:05:20,440 --> 00:05:23,660
 the best way to do this is to come back to your meditation

116
00:05:23,660 --> 00:05:24,560
 technique and

117
00:05:24,560 --> 00:05:28,880
 especially to deal with as the Buddha said the the drows

118
00:05:28,880 --> 00:05:30,680
iness as itself

119
00:05:30,680 --> 00:05:34,250
 instead of allowing the thoughts to build the drowsiness

120
00:05:34,250 --> 00:05:35,040
 focus on the

121
00:05:35,040 --> 00:05:37,910
 drowsiness and look at it when you do that you're you're

122
00:05:37,910 --> 00:05:38,880
 letting go of the

123
00:05:38,880 --> 00:05:42,150
 cause of the drowsiness and the drowsiness will disappear

124
00:05:42,150 --> 00:05:42,800
 by itself.

125
00:05:42,800 --> 00:05:49,630
 Also the the attention and the alert awareness of the drows

126
00:05:49,630 --> 00:05:50,680
iness is the

127
00:05:50,680 --> 00:05:53,920
 opposite and you'll find that the drowsiness as you watch

128
00:05:53,920 --> 00:05:54,600
 it because of

129
00:05:54,600 --> 00:05:57,360
 the change in the mind state the drowsiness itself goes

130
00:05:57,360 --> 00:05:58,660
 away so you can

131
00:05:58,660 --> 00:06:02,070
 try to focus on the drowsiness itself which is is quite

132
00:06:02,070 --> 00:06:03,160
 useful but most

133
00:06:03,160 --> 00:06:06,020
 important is to avoid the the thoughts that were causing

134
00:06:06,020 --> 00:06:08,080
 you to become drowsing.

135
00:06:08,080 --> 00:06:13,640
 The second way is to if if this practical method doesn't

136
00:06:13,640 --> 00:06:15,320
 work of reverting back to

137
00:06:15,320 --> 00:06:19,140
 the practice and adjusting your practice you can go back to

138
00:06:19,140 --> 00:06:20,880
 the teachings that

139
00:06:20,880 --> 00:06:25,050
 the Buddha taught or that your teacher gave us on and to go

140
00:06:25,050 --> 00:06:26,000
 over them in your

141
00:06:26,000 --> 00:06:31,800
 mind to to think about for instance the four foundations of

142
00:06:31,800 --> 00:06:32,760
 mindfulness the body

143
00:06:32,760 --> 00:06:38,680
 the feelings the mind the various dhammas the hindrances of

144
00:06:38,680 --> 00:06:39,720
 the emotions

145
00:06:39,720 --> 00:06:42,680
 or so on focus on what it was that the Buddha taught or

146
00:06:42,680 --> 00:06:43,720
 what it is that your

147
00:06:43,720 --> 00:06:47,630
 meditation teacher taught and go over them also the body

148
00:06:47,630 --> 00:06:48,880
 how do we focus on

149
00:06:48,880 --> 00:06:55,080
 the body think about maybe ways that you are lacking and

150
00:06:55,080 --> 00:06:56,160
 things that you are

151
00:06:56,160 --> 00:06:59,470
 missing in terms of awareness of the body and are you

152
00:06:59,470 --> 00:07:00,840
 actually able to watch

153
00:07:00,840 --> 00:07:03,850
 the movements of the stomach or be aware of the sitting

154
00:07:03,850 --> 00:07:05,800
 position or the breath or

155
00:07:05,800 --> 00:07:09,360
 whatever is your object are you actually aware of the

156
00:07:09,360 --> 00:07:10,680
 feelings when there's a

157
00:07:10,680 --> 00:07:13,760
 painful feeling are you actually paying attention to it for

158
00:07:13,760 --> 00:07:14,560
 instance saying to

159
00:07:14,560 --> 00:07:18,360
 yourself pain pain or if you feel happy or calm are you

160
00:07:18,360 --> 00:07:19,480
 actually paying attention

161
00:07:19,480 --> 00:07:23,970
 or are you letting it drag you down into a state of letharg

162
00:07:23,970 --> 00:07:25,560
y in the state of

163
00:07:25,560 --> 00:07:30,600
 fatigue and and drowsiness and the same with the mind and

164
00:07:30,600 --> 00:07:32,520
 the mind of just so

165
00:07:32,520 --> 00:07:35,430
 refer back to the teachings and think about them in your

166
00:07:35,430 --> 00:07:37,320
 mind examine them and

167
00:07:37,320 --> 00:07:41,850
 and relate them back to your practice and and compare your

168
00:07:41,850 --> 00:07:43,120
 practice to the

169
00:07:43,120 --> 00:07:47,540
 teachings and that if you do that first of all just

170
00:07:47,540 --> 00:07:48,640
 thinking about these good

171
00:07:48,640 --> 00:07:51,780
 things will will wake you up and remind you of what you

172
00:07:51,780 --> 00:07:53,360
 should be doing and

173
00:07:53,360 --> 00:07:57,470
 second of all it will allow you to adjust your practice the

174
00:07:57,470 --> 00:07:58,680
 third way is if

175
00:07:58,680 --> 00:08:01,180
 that doesn't work or another way to deal with drowsiness is

176
00:08:01,180 --> 00:08:02,080
 to actually recite

177
00:08:02,080 --> 00:08:07,170
 the teachings and I know from experience that this is quite

178
00:08:07,170 --> 00:08:08,600
 useful for example

179
00:08:08,600 --> 00:08:13,870
 when you're driving I remember when I would be driving

180
00:08:13,870 --> 00:08:15,480
 somewhere at night and

181
00:08:15,480 --> 00:08:19,780
 trying to be mindful watching the road and steering turning

182
00:08:19,780 --> 00:08:21,520
 seeing and emotions

183
00:08:21,520 --> 00:08:26,070
 that arise and so on that I would find myself drifting

184
00:08:26,070 --> 00:08:27,440
 because sometimes there

185
00:08:27,440 --> 00:08:30,930
 would be excess concentration and not enough effort and

186
00:08:30,930 --> 00:08:31,760
 maybe falling asleep

187
00:08:31,760 --> 00:08:34,810
 when that happened I would actually recite the Buddha's

188
00:08:34,810 --> 00:08:35,960
 teachings and we do

189
00:08:35,960 --> 00:08:41,160
 this chant chance that we have of the Buddha's teachings we

190
00:08:41,160 --> 00:08:42,600
 actually recite

191
00:08:42,600 --> 00:08:45,470
 them so there's not it's not really an intellectual

192
00:08:45,470 --> 00:08:47,000
 exercise but it's something

193
00:08:47,000 --> 00:08:50,940
 akin to singing songs when people turn on the radio and

194
00:08:50,940 --> 00:08:52,440
 sing along when they're

195
00:08:52,440 --> 00:08:56,280
 driving late at night it's something that will wake you up

196
00:08:56,280 --> 00:08:57,960
 but also of course

197
00:08:57,960 --> 00:08:59,920
 because it's the Buddha's teachings it's something that

198
00:08:59,920 --> 00:09:00,800
 will invigorate you and

199
00:09:00,800 --> 00:09:05,890
 give you effort and and give you the encouragement that you

200
00:09:05,890 --> 00:09:06,840
 might need

201
00:09:06,840 --> 00:09:09,520
 thinking about the Buddha thinking about his teachings

202
00:09:09,520 --> 00:09:12,280
 thinking about the

203
00:09:12,280 --> 00:09:17,480
 meditation practice and so on if that still doesn't work we

204
00:09:17,480 --> 00:09:18,520
 the the fourth

205
00:09:18,520 --> 00:09:22,620
 method the Buddha said is to start to get physical and the

206
00:09:22,620 --> 00:09:24,040
 Buddha said use you

207
00:09:24,040 --> 00:09:28,000
 pull your ears technique of waking you up kind of

208
00:09:28,000 --> 00:09:30,440
 stretching your cranium rub

209
00:09:30,440 --> 00:09:33,880
 your arms rub your body massage yourself and maybe stretch

210
00:09:33,880 --> 00:09:36,520
 a little bit to kind

211
00:09:36,520 --> 00:09:40,880
 of wake you up to get the blood flowing and to give

212
00:09:40,880 --> 00:09:41,680
 yourself a little bit of

213
00:09:41,680 --> 00:09:44,710
 physical energy because that might be the cause your body

214
00:09:44,710 --> 00:09:46,960
 might be tired or so

215
00:09:46,960 --> 00:09:51,680
 on your body might be stiff for instance some people might

216
00:09:51,680 --> 00:09:52,720
 even go so far as to

217
00:09:52,720 --> 00:09:55,780
 practice yoga I think it could be a very useful technique

218
00:09:55,780 --> 00:09:56,920
 to waking you up

219
00:09:56,920 --> 00:10:00,670
 because it's a different technique and actually might lead

220
00:10:00,670 --> 00:10:01,560
 in a different

221
00:10:01,560 --> 00:10:05,430
 direction I wouldn't recommend extensive practice of yoga

222
00:10:05,430 --> 00:10:08,040
 in combination with

223
00:10:08,040 --> 00:10:14,000
 insight meditation but there's certainly nothing harmful in

224
00:10:14,000 --> 00:10:16,760
 in practicing it and

225
00:10:16,760 --> 00:10:20,340
 and certainly in moderation for the purposes of building

226
00:10:20,340 --> 00:10:21,040
 the energy

227
00:10:21,040 --> 00:10:27,120
 necessary to practice meditation but at any rate the Buddha

228
00:10:27,120 --> 00:10:29,160
 said massaging and

229
00:10:29,160 --> 00:10:33,090
 rubbing and pulling your ears and so on if that doesn't

230
00:10:33,090 --> 00:10:34,320
 work another way to deal

231
00:10:34,320 --> 00:10:38,030
 with drowsiness the Buddha said is to actually stand up and

232
00:10:38,030 --> 00:10:39,000
 go get some water

233
00:10:39,000 --> 00:10:43,940
 pour water on your face rub water into your eyes and look

234
00:10:43,940 --> 00:10:45,640
 to all directions the

235
00:10:45,640 --> 00:10:48,240
 Buddha said take a look around this is a way of kind of

236
00:10:48,240 --> 00:10:49,720
 waking you up and

237
00:10:49,720 --> 00:10:53,890
 stimulating your mind to sort of get rid of this heavy

238
00:10:53,890 --> 00:10:55,680
 state of concentration

239
00:10:55,680 --> 00:10:58,590
 look in all directions look all around you go and look

240
00:10:58,590 --> 00:10:59,600
 around see what's going

241
00:10:59,600 --> 00:11:03,930
 on look up at the stars he said usually drowsiness comes at

242
00:11:03,930 --> 00:11:05,040
 night so go and look

243
00:11:05,040 --> 00:11:08,950
 up at the sky look up at the stars look up at the constell

244
00:11:08,950 --> 00:11:11,240
ations is a way of

245
00:11:11,240 --> 00:11:14,560
 breaking up this heavy concentration giving yourself a more

246
00:11:14,560 --> 00:11:15,600
 flexible state of

247
00:11:15,600 --> 00:11:20,870
 mind to break to throw off the lethargy the drowsiness he

248
00:11:20,870 --> 00:11:22,240
 said if you do that

249
00:11:22,240 --> 00:11:25,810
 then it's possible that the drowsiness will will disappear

250
00:11:25,810 --> 00:11:26,560
 and you'll be able to

251
00:11:26,560 --> 00:11:31,250
 continue with your practice you can do walking meditation

252
00:11:31,250 --> 00:11:32,560
 the Buddha said this

253
00:11:32,560 --> 00:11:36,120
 is a part of this is to switch to doing walking meditation

254
00:11:36,120 --> 00:11:37,200
 that's one of the

255
00:11:37,200 --> 00:11:40,720
 reasons for walking meditation as well it allows you to

256
00:11:40,720 --> 00:11:42,000
 develop effort and

257
00:11:42,000 --> 00:11:45,690
 energy if you sit all the time it's easy to fall asleep if

258
00:11:45,690 --> 00:11:46,760
 you do walking as well

259
00:11:46,760 --> 00:11:49,640
 you'll find that when you do the sitting you you feel

260
00:11:49,640 --> 00:11:52,720
 charged afterwards so this

261
00:11:52,720 --> 00:11:58,040
 is the the fifth method the sixth method is a specific

262
00:11:58,040 --> 00:11:59,880
 meditation technique and

263
00:11:59,880 --> 00:12:03,610
 it's probably not applicable for most beginner meditators

264
00:12:03,610 --> 00:12:04,800
 but if you've been

265
00:12:04,800 --> 00:12:07,930
 practicing meditation for a while and I suppose even even

266
00:12:07,930 --> 00:12:08,920
 as a beginner you

267
00:12:08,920 --> 00:12:14,000
 could attempt it the technique is to think of day as night

268
00:12:14,000 --> 00:12:17,400
 to resolve on the

269
00:12:17,400 --> 00:12:21,610
 meditation or the awareness of light even though it's dark

270
00:12:21,610 --> 00:12:23,000
 at night because of

271
00:12:23,000 --> 00:12:26,300
 course night is the darkness is something that triggers our

272
00:12:26,300 --> 00:12:27,000
 recollection

273
00:12:27,000 --> 00:12:30,760
 of oh now it's time to fall asleep and causes us to start

274
00:12:30,760 --> 00:12:31,960
 to feel drowsy our

275
00:12:31,960 --> 00:12:36,640
 mind triggers the drowsiness routine and based on the fact

276
00:12:36,640 --> 00:12:37,840
 that it's dark so you

277
00:12:37,840 --> 00:12:41,230
 trick yourself you tell yourself that it's light or you you

278
00:12:41,230 --> 00:12:42,880
 envision you can

279
00:12:42,880 --> 00:12:46,670
 close with your eyes closed you can imagine light or become

280
00:12:46,670 --> 00:12:47,880
 aware of light I

281
00:12:47,880 --> 00:12:50,240
 know there are monks who told me that you should do

282
00:12:50,240 --> 00:12:51,640
 meditation with a bright

283
00:12:51,640 --> 00:12:55,700
 light on and I suppose this could help as well I've tried

284
00:12:55,700 --> 00:12:56,360
 it I don't it doesn't

285
00:12:56,360 --> 00:13:03,820
 seem that useful but I suppose it could be at least to some

286
00:13:03,820 --> 00:13:04,800
 extent but here the

287
00:13:04,800 --> 00:13:07,500
 point is to actually get it in your mind that it's light

288
00:13:07,500 --> 00:13:08,520
 that there is light

289
00:13:08,520 --> 00:13:12,390
 because that will trigger the energy in the mind it's a way

290
00:13:12,390 --> 00:13:13,520
 of stimulating

291
00:13:13,520 --> 00:13:17,260
 energy he said thinking of though it's night to think of it

292
00:13:17,260 --> 00:13:18,600
 as day and to get

293
00:13:18,600 --> 00:13:21,680
 an idea in your mind if you're actually been practicing

294
00:13:21,680 --> 00:13:22,560
 meditation for a while

295
00:13:22,560 --> 00:13:27,790
 you can actually begin to envision envision bright lights

296
00:13:27,790 --> 00:13:28,560
 some people even

297
00:13:28,560 --> 00:13:32,020
 get very distracted by these lights or colors or pictures

298
00:13:32,020 --> 00:13:33,520
 or so on and we have

299
00:13:33,520 --> 00:13:36,130
 to remind meditators not to get off track and to

300
00:13:36,130 --> 00:13:37,680
 acknowledge them as seeing

301
00:13:37,680 --> 00:13:41,510
 seeing to just remind yourself this is a visual stimulus it

302
00:13:41,510 --> 00:13:42,680
's not a magical

303
00:13:42,680 --> 00:13:46,460
 sensation magical experience or certainly not the path that

304
00:13:46,460 --> 00:13:46,900
 leads to

305
00:13:46,900 --> 00:13:51,120
 enlightenment it's the wrong path but in this case it has a

306
00:13:51,120 --> 00:13:52,240
 limited benefit of

307
00:13:52,240 --> 00:13:56,340
 bringing about energy and effort it has its use even though

308
00:13:56,340 --> 00:13:57,960
 it's not the path

309
00:13:57,960 --> 00:14:02,550
 and that's the sixth method if that doesn't work the Buddha

310
00:14:02,550 --> 00:14:03,120
 said lie down

311
00:14:03,120 --> 00:14:07,120
 and the Buddha's way of lying down was to lie down on what

312
00:14:07,120 --> 00:14:08,640
 on your side not in

313
00:14:08,640 --> 00:14:11,690
 your stomach not on your back with the light on the one

314
00:14:11,690 --> 00:14:12,720
 side and I'm not sure

315
00:14:12,720 --> 00:14:15,980
 you can prop your head up or you I don't think that's

316
00:14:15,980 --> 00:14:17,400
 mentioned but the way I

317
00:14:17,400 --> 00:14:20,930
 would often do it is to actually prop my head up on my

318
00:14:20,930 --> 00:14:23,280
 elbow and it's a technique

319
00:14:23,280 --> 00:14:27,670
 that I've I've seen other people use and monks use and it

320
00:14:27,670 --> 00:14:29,920
 seems to very much

321
00:14:29,920 --> 00:14:32,410
 keep you awake if you start to fall asleep your head starts

322
00:14:32,410 --> 00:14:33,120
 to fall off your

323
00:14:33,120 --> 00:14:37,520
 your arm and you wake up quite well it's also quite painful

324
00:14:37,520 --> 00:14:38,200
 in the beginning it's

325
00:14:38,200 --> 00:14:41,730
 something you have to develop it's a physical technique

326
00:14:41,730 --> 00:14:42,620
 that you have to

327
00:14:42,620 --> 00:14:47,160
 develop just like sitting cross-legged but at any rate lie

328
00:14:47,160 --> 00:14:48,600
 down on one side and

329
00:14:48,600 --> 00:14:53,110
 perhaps not propping the head up but at the very least

330
00:14:53,110 --> 00:14:54,320
 resolving on getting up

331
00:14:54,320 --> 00:14:57,210
 the Buddha said you're lying down and you know you know you

332
00:14:57,210 --> 00:14:58,120
're kind of given

333
00:14:58,120 --> 00:15:00,920
 up at this point because you're feeling like you can't

334
00:15:00,920 --> 00:15:01,960
 overcome the drowsiness

335
00:15:01,960 --> 00:15:05,850
 so you're going to accept that you might fall asleep so you

336
00:15:05,850 --> 00:15:08,120
 lie down and you say

337
00:15:08,120 --> 00:15:12,240
 I'm going to get up in so many minutes or so many hours if

338
00:15:12,240 --> 00:15:13,080
 it's time to sleep

339
00:15:13,080 --> 00:15:16,170
 you say I'm gonna sleep for four hours or however long you

340
00:15:16,170 --> 00:15:17,120
're going to sleep

341
00:15:17,120 --> 00:15:20,850
 and I'm going to get up at such and such a time when you

342
00:15:20,850 --> 00:15:22,720
 resolve think about

343
00:15:22,720 --> 00:15:25,670
 before you go to sleep think only about that the fact that

344
00:15:25,670 --> 00:15:26,520
 you're going to get

345
00:15:26,520 --> 00:15:30,880
 up so that when that time comes you'll find that if you

346
00:15:30,880 --> 00:15:32,160
 resolve in this way

347
00:15:32,160 --> 00:15:38,370
 you're actually your mind is able to somehow wake you up at

348
00:15:38,370 --> 00:15:39,720
 that time it's

349
00:15:39,720 --> 00:15:43,990
 amazing how how advanced the mind really is that you'll

350
00:15:43,990 --> 00:15:45,320
 find yourself waking up a

351
00:15:45,320 --> 00:15:49,320
 few minutes before the hour that you were going to wake up

352
00:15:49,320 --> 00:15:50,760
 or how many

353
00:15:50,760 --> 00:15:54,440
 minutes you were going to sleep or so on and then the

354
00:15:54,440 --> 00:15:55,360
 Buddha said and then get

355
00:15:55,360 --> 00:16:00,060
 up he said resolve in your mind I will not give into drows

356
00:16:00,060 --> 00:16:01,040
iness and I will not

357
00:16:01,040 --> 00:16:04,600
 become attached to the pleasure that comes from lying down

358
00:16:04,600 --> 00:16:06,480
 because one of the

359
00:16:06,480 --> 00:16:10,640
 things that we miss one of the great addictions that we

360
00:16:10,640 --> 00:16:12,480
 miss is the addiction

361
00:16:12,480 --> 00:16:16,670
 to sleep many people like to lie down for many many hours

362
00:16:16,670 --> 00:16:18,280
 and sleep a lot and

363
00:16:18,280 --> 00:16:22,760
 it's because of the physical pleasure that comes from it

364
00:16:22,760 --> 00:16:24,120
 but the problem with

365
00:16:24,120 --> 00:16:26,980
 this physical pleasure is like all physical pleasures it's

366
00:16:26,980 --> 00:16:27,760
 not permanent

367
00:16:27,760 --> 00:16:31,050
 it's not lasting and it's not the case that the more you

368
00:16:31,050 --> 00:16:32,400
 sleep the more happy

369
00:16:32,400 --> 00:16:37,420
 you feel you in fact feel more depressed and more lethargic

370
00:16:37,420 --> 00:16:39,200
 and less energetic

371
00:16:39,200 --> 00:16:45,620
 less bright and and and you know at peace with yourself you

372
00:16:45,620 --> 00:16:46,000
 feel more

373
00:16:46,000 --> 00:16:51,730
 distracted less awake and less alert so you know the Buddha

374
00:16:51,730 --> 00:16:53,480
 said this is this

375
00:16:53,480 --> 00:16:57,880
 last one you have to you have to make it you can't just lie

376
00:16:57,880 --> 00:16:59,280
 down and say I'm going

377
00:16:59,280 --> 00:17:03,050
 to go to sleep and fall asleep you have to be strict about

378
00:17:03,050 --> 00:17:04,000
 it that I'm going to

379
00:17:04,000 --> 00:17:07,040
 set myself how long I'm going to sleep and then when I wake

380
00:17:07,040 --> 00:17:08,080
 up I'm going to get

381
00:17:08,080 --> 00:17:12,140
 up and go on with my practice the Buddha said this is the

382
00:17:12,140 --> 00:17:13,440
 way that a person should

383
00:17:13,440 --> 00:17:17,120
 deal with drowsiness so rather than give my own thoughts on

384
00:17:17,120 --> 00:17:17,920
 it or something

385
00:17:17,920 --> 00:17:20,860
 that's vaguely based on the Buddha's teaching there you

386
00:17:20,860 --> 00:17:23,040
 have directly as best

387
00:17:23,040 --> 00:17:31,240
 as I could translate and paraphrase and expand upon it the

388
00:17:31,240 --> 00:17:32,840
 Buddha's teaching on

389
00:17:32,840 --> 00:17:36,560
 how to deal with drowsiness so thanks for the question hope

390
00:17:36,560 --> 00:17:38,880
 that helped

