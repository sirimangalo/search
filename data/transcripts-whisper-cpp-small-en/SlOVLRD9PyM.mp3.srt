1
00:00:00,000 --> 00:00:02,000
 Okay.

2
00:00:02,000 --> 00:00:07,190
 All the following is proven as life-extending or being very

3
00:00:07,190 --> 00:00:10,500
 healthy. One, doing sports or physical exercise.

4
00:00:10,500 --> 00:00:12,880
 Two, sex.

5
00:00:12,880 --> 00:00:14,880
 Three, eating many times a day.

6
00:00:14,880 --> 00:00:17,360
 Four, sleeping at least six hours a night.

7
00:00:17,360 --> 00:00:20,930
 What does some considers all these ideas bad? How do you

8
00:00:20,930 --> 00:00:21,600
 explain this?

9
00:00:21,600 --> 00:00:24,480
 Very good question.

10
00:00:24,480 --> 00:00:26,280
 Hmm.

11
00:00:26,280 --> 00:00:28,280
 It's a tough one.

12
00:00:30,200 --> 00:00:35,300
 Okay, well, let's start by countering some of these claims.

13
00:00:35,300 --> 00:00:39,600
 Okay.

14
00:00:39,600 --> 00:00:43,930
 Sports and physical exercise. A lot of the reason why it's

15
00:00:43,930 --> 00:00:46,020
 considered to be healthy is

16
00:00:46,020 --> 00:00:49,460
 because people eat many times a day, for example.

17
00:00:49,460 --> 00:00:54,320
 And eat, well, let's just say eat too much and start there.

18
00:00:54,320 --> 00:00:57,000
 Hmm?

19
00:00:57,000 --> 00:01:05,160
 Also, because their bodies operate, tend to operate sub-

20
00:01:05,160 --> 00:01:06,180
optimally.

21
00:01:06,180 --> 00:01:12,200
 Hmm? And so that's going to lead to the, the, the,

22
00:01:12,200 --> 00:01:17,040
 because of the mind. You know, the mind will cause tension

23
00:01:17,040 --> 00:01:17,720
 in the body,

24
00:01:17,720 --> 00:01:22,680
 will cause

25
00:01:24,800 --> 00:01:28,780
 overworking of the systems in the body due to hormone

26
00:01:28,780 --> 00:01:29,760
 releases, you know,

27
00:01:29,760 --> 00:01:32,480
 based on sex, for example.

28
00:01:32,480 --> 00:01:37,080
 But just any kind of desire or anger or frustration.

29
00:01:37,080 --> 00:01:40,160
 This is going to

30
00:01:40,160 --> 00:01:43,900
 disrupt the bodily systems or overtax them and so it

31
00:01:43,900 --> 00:01:46,760
 actually can, you know,

32
00:01:46,760 --> 00:01:51,760
 lead to buildup of toxins, that kind of thing.

33
00:01:51,760 --> 00:02:02,280
 And so hence you need, there's the need to, to exercise.

34
00:02:02,280 --> 00:02:07,440
 Do sports and do exercise. This helps people.

35
00:02:07,440 --> 00:02:10,520
 But see,

36
00:02:10,520 --> 00:02:14,000
 all of these studies for all of these things are dealing

37
00:02:14,000 --> 00:02:16,560
 most likely with people who don't meditate or,

38
00:02:16,560 --> 00:02:20,040
 you know, maybe do a little bit of meditation, but not

39
00:02:20,040 --> 00:02:20,880
 seriously.

40
00:02:20,880 --> 00:02:23,560
 You know, you're dealing with ordinary people who consider

41
00:02:23,560 --> 00:02:25,960
 sports to be their meditation.

42
00:02:25,960 --> 00:02:29,220
 So it's helpful for them. It's a way of relaxing. Well,

43
00:02:29,220 --> 00:02:29,840
 good for them.

44
00:02:29,840 --> 00:02:33,840
 If you practice meditation, especially walking meditation,

45
00:02:33,840 --> 00:02:34,280
 it's,

46
00:02:34,280 --> 00:02:38,250
 you'd have to do a study of those people and to see whether

47
00:02:38,250 --> 00:02:38,840
 sports,

48
00:02:38,840 --> 00:02:41,670
 you know, whether they were actually lacking something and

49
00:02:41,670 --> 00:02:44,440
 I think you'd find that no indeed they're not. I mean,

50
00:02:44,440 --> 00:02:48,120
 ridiculous because consider all these, consider,

51
00:02:49,360 --> 00:02:52,620
 you know, the, the, the sort of the stereotypical Asian

52
00:02:52,620 --> 00:02:56,640
 person living on the mountain who lives to be 90 or 100,

53
00:02:56,640 --> 00:02:57,960
 who never does any sports.

54
00:02:57,960 --> 00:03:02,140
 I guess in some cases they're doing a lot of physical

55
00:03:02,140 --> 00:03:06,280
 activity, but in many cases not. In many cases, it's just

56
00:03:06,280 --> 00:03:07,760
 peaceful living, you know.

57
00:03:07,760 --> 00:03:10,480
 Peaceful living changes so much.

58
00:03:10,480 --> 00:03:14,760
 That's one thing. So, so anyway, talking about sports and

59
00:03:14,760 --> 00:03:17,040
 physical exercise, I think you can challenge that

60
00:03:17,680 --> 00:03:21,550
 based on the meditative lifestyle, that there's really no

61
00:03:21,550 --> 00:03:22,080
 reason to,

62
00:03:22,080 --> 00:03:26,030
 to require your muscles to be bigger or your cardiovascular

63
00:03:26,030 --> 00:03:28,360
 system to be, to be

64
00:03:28,360 --> 00:03:30,960
 worked out.

65
00:03:30,960 --> 00:03:33,440
 If your mind is,

66
00:03:33,440 --> 00:03:39,580
 is in tune with the body, you will find through meditation

67
00:03:39,580 --> 00:03:40,560
 that your body

68
00:03:40,560 --> 00:03:44,560
 functions far more efficiently through meditation.

69
00:03:44,560 --> 00:03:46,820
 You can feel in the meditation practice that a lot of

70
00:03:46,820 --> 00:03:47,600
 toxins are being

71
00:03:47,600 --> 00:03:51,400
 flushed out.

72
00:03:51,400 --> 00:03:55,360
 Okay, sex.

73
00:03:55,360 --> 00:04:00,440
 Sex. Well, besides the fact that it's a physical activity,

74
00:04:00,440 --> 00:04:06,420
 I don't know what to say here because you're, you're, you

75
00:04:06,420 --> 00:04:08,280
 know, you study the addiction

76
00:04:08,280 --> 00:04:11,200
 centers in the brain. You know, what you're talking about,

77
00:04:11,200 --> 00:04:13,440
 I guess, is the release of

78
00:04:15,440 --> 00:04:19,000
 endorphins, whatever, you know, there's a lot of dopamine,

79
00:04:19,000 --> 00:04:20,160
 I'm sure.

80
00:04:20,160 --> 00:04:24,820
 Oxytocin, I don't know. I mean, sure there's a lot. Yeah,

81
00:04:24,820 --> 00:04:29,400
 which one? Dopamine is desire and oxytocin is pleasure.

82
00:04:29,400 --> 00:04:32,400
 I don't know. I'm sure it's all caught up in there, but you

83
00:04:32,400 --> 00:04:35,600
 have a serious problem with all of this. Whatever benefit,

84
00:04:35,600 --> 00:15:28,050
 I don't know that there's any physical benefit to, to the

85
00:15:28,050 --> 00:15:28,050
 excretion of dopamine or the, you know, the production of

86
00:15:28,050 --> 00:04:42,760
 dopamine oxytocin.

87
00:04:43,240 --> 00:04:46,610
 Whatever these chemicals are, they, I don't, I mean, I'm

88
00:04:46,610 --> 00:04:48,600
 not a chemist or a biologist,

89
00:04:48,600 --> 00:04:50,760
 but I do know,

90
00:04:50,760 --> 00:04:53,760
 just from listening and of course from practice,

91
00:04:53,760 --> 00:04:57,220
 that you've got a serious problem here because you're

92
00:04:57,220 --> 00:04:59,960
 cultivating addiction. These systems

93
00:04:59,960 --> 00:05:04,070
 seriously affect the brain in a bad way. They cultivate

94
00:05:04,070 --> 00:05:08,720
 what we call the addiction cycle. It's studied, you know.

95
00:05:10,560 --> 00:05:13,360
 Sex leads you to desire more sex and

96
00:05:13,360 --> 00:05:21,800
 actually makes you less satisfied, you know, more impatient

97
00:05:21,800 --> 00:05:21,800
.

98
00:05:21,800 --> 00:05:28,160
 This is not theoretical. You can study how this addiction

99
00:05:28,160 --> 00:05:31,000
 cycle works. It leads to addiction. There's no question.

100
00:05:31,000 --> 00:05:34,280
 What I think is probably not well

101
00:05:34,280 --> 00:05:37,000
 researched is what addiction does to you.

102
00:05:38,160 --> 00:05:40,840
 And or we don't realize it's like, okay, so I get addicted

103
00:05:40,840 --> 00:05:43,500
 to sex. It's fine. You know, I'm happy to have an active

104
00:05:43,500 --> 00:05:43,960
 sex life.

105
00:05:43,960 --> 00:05:45,600
 But

106
00:05:45,600 --> 00:05:48,140
 what does the question is, that's not the question, the

107
00:05:48,140 --> 00:05:50,280
 question is what does this addiction cycle do to you?

108
00:05:50,280 --> 00:05:52,560
 Does it have any negative effects? And I think absolutely

109
00:05:52,560 --> 00:05:54,840
 it does. It leads to the potential for greater anger and

110
00:05:54,840 --> 00:05:55,400
 frustration,

111
00:05:55,400 --> 00:05:58,000
 depression,

112
00:05:58,000 --> 00:06:00,000
 boredom and

113
00:06:00,000 --> 00:06:03,200
 dissatisfaction, inability to,

114
00:06:04,120 --> 00:06:06,210
 well, I mean these are all saying the same thing, but

115
00:06:06,210 --> 00:06:08,600
 basically the inability to stay at peace with yourself.

116
00:06:08,600 --> 00:06:15,960
 So the idea that sex is,

117
00:06:15,960 --> 00:06:22,030
 what are you saying about these things? Life-extending or

118
00:06:22,030 --> 00:06:23,680
 being very healthy.

119
00:06:23,680 --> 00:06:31,160
 Okay, well, let's get back to the life-extending thing.

120
00:06:33,280 --> 00:06:35,690
 Yeah, okay. No, let's not. We have to include it there

121
00:06:35,690 --> 00:06:37,960
 because life-extending is even from a Buddhist point of

122
00:06:37,960 --> 00:06:38,200
 view.

123
00:06:38,200 --> 00:06:40,200
 That's a good thing, good to live longer.

124
00:06:40,200 --> 00:06:45,600
 At the expense of

125
00:06:45,600 --> 00:06:52,440
 your, well, as again, I would say the the benefits are

126
00:06:52,440 --> 00:06:56,000
 purely based on the fact that the person has too much

127
00:06:56,000 --> 00:06:57,800
 stress in their lives and

128
00:06:59,000 --> 00:07:02,300
 this is their only outlet. So absolutely a person who is

129
00:07:02,300 --> 00:07:04,280
 suppressing their sexual urges and

130
00:07:04,280 --> 00:07:10,080
 sitting around as a couch potato all the time, this is

131
00:07:10,080 --> 00:07:12,440
 unhealthy and so that

132
00:07:12,440 --> 00:07:17,470
 release is the only way to physically cope with that. You

133
00:07:17,470 --> 00:07:19,680
 know, it's just a bouncing back and forth,

134
00:07:19,680 --> 00:07:22,720
 but I would say definitely a person who is able to

135
00:07:22,720 --> 00:07:26,560
 overcome those things is

136
00:07:28,000 --> 00:07:31,930
 better off. I would argue will actually have the potential

137
00:07:31,930 --> 00:07:34,080
 to live longer, but

138
00:07:34,080 --> 00:07:36,160
 moreover,

139
00:07:36,160 --> 00:07:40,720
 avoids the huge, huge problem of addiction. Sports and

140
00:07:40,720 --> 00:07:43,960
 exercise is the same. I mean sports can be quite addictive.

141
00:07:43,960 --> 00:07:47,410
 We have, and if you don't believe you don't, if you think

142
00:07:47,410 --> 00:07:48,240
 that's absurd,

143
00:07:48,240 --> 00:07:52,020
 people who come and meditate can prove it. Anyone, any

144
00:07:52,020 --> 00:07:54,260
 sports person who comes to meditate has a very difficult

145
00:07:54,260 --> 00:07:55,320
 time sitting still.

146
00:07:55,320 --> 00:07:57,560
 They're not able to because

147
00:07:58,040 --> 00:08:00,040
 they have an addiction and

148
00:08:00,040 --> 00:08:03,540
 that's going to spill over in your life. It's going to make

149
00:08:03,540 --> 00:08:03,760
 you

150
00:08:03,760 --> 00:08:06,840
 jumpy. It's going to make you,

151
00:08:06,840 --> 00:08:09,640
 you know, somehow require something,

152
00:08:09,640 --> 00:08:13,120
 which means that when you're in a position to not be able

153
00:08:13,120 --> 00:08:13,360
 to

154
00:08:13,360 --> 00:08:17,740
 indulge in your addiction, you're going to be frustrated.

155
00:08:17,740 --> 00:08:20,040
 You know, as you get older, if you ever end up being bed

156
00:08:20,040 --> 00:08:22,880
ridden, it's going to be frustrating. It's going to make you

157
00:08:23,520 --> 00:08:26,980
 depressed and so on. Definitely this happens. Sports,

158
00:08:26,980 --> 00:08:30,210
 people who suffer injuries in sports, they become depressed

159
00:08:30,210 --> 00:08:30,360
 and

160
00:08:30,360 --> 00:08:33,240
 frustrated and turn to

161
00:08:33,240 --> 00:08:35,680
 alcohol in extreme cases, but

162
00:08:35,680 --> 00:08:40,480
 it's a problem. So

163
00:08:40,480 --> 00:08:46,210
 which gets to sort of the other part of this answer is that

164
00:08:46,210 --> 00:08:48,560
 Buddhism isn't so concerned with the body.

165
00:08:48,560 --> 00:08:51,580
 Living long, good thing. Living long with,

166
00:08:52,400 --> 00:08:56,450
 by cultivating addiction, bad thing. You know, if as you,

167
00:08:56,450 --> 00:08:58,600
 if living a hundred years

168
00:08:58,600 --> 00:09:03,450
 means you can cultivate more addiction, then better to die

169
00:09:03,450 --> 00:09:03,800
 early.

170
00:09:03,800 --> 00:09:06,870
 You know, it's a horrible thing to say, but you're not

171
00:09:06,870 --> 00:09:08,640
 doing anything good by living.

172
00:09:08,640 --> 00:09:13,200
 So as the Buddha said,

173
00:09:13,200 --> 00:09:17,200
 (speaking in foreign language)

174
00:09:17,200 --> 00:09:27,480
 That's not quite the correct grammar.

175
00:09:27,480 --> 00:09:32,190
 Better to live, better it is to live one day. Better than

176
00:09:32,190 --> 00:09:33,760
 to live a hundred years,

177
00:09:33,760 --> 00:09:36,840
 not seeing the truth.

178
00:09:36,840 --> 00:09:40,610
 Not seeing arising and ceasing. Not seeing the nature of

179
00:09:40,610 --> 00:09:40,960
 things.

180
00:09:42,760 --> 00:09:46,160
 Better to live one day, having seen this truth.

181
00:09:46,160 --> 00:09:50,000
 So

182
00:09:50,000 --> 00:09:54,610
 that's the, the second, more, probably the more deep part

183
00:09:54,610 --> 00:09:55,320
 of this answer.

184
00:09:55,320 --> 00:09:58,200
 But I don't think these claims should be

185
00:09:58,200 --> 00:10:02,000
 gone unchall, should be left unchallenged. So number two.

186
00:10:02,000 --> 00:10:04,120
 Number three, eating many times a day.

187
00:10:04,120 --> 00:10:07,080
 I've heard this and I've had doctors tell me,

188
00:10:08,120 --> 00:10:11,230
 even a Thai doctor told me, he said, "I don't want to say

189
00:10:11,230 --> 00:10:12,880
 bad things about monks."

190
00:10:12,880 --> 00:10:15,220
 And then he basically went on to say how bad it is that

191
00:10:15,220 --> 00:10:16,760
 monks only eat in the morning.

192
00:10:16,760 --> 00:10:23,320
 Again, I want to say that it has to do with people's

193
00:10:23,320 --> 00:10:28,160
 their

194
00:10:28,160 --> 00:10:30,120
 lifestyles.

195
00:10:30,120 --> 00:10:33,400
 But I guess I'd have to look deeper into the science of it.

196
00:10:33,400 --> 00:10:37,350
 I want to say that eating once a day is great. It feels

197
00:10:37,350 --> 00:10:38,040
 regular.

198
00:10:38,040 --> 00:10:41,320
 Eating once a day is just one meal, for example.

199
00:10:41,320 --> 00:10:47,370
 My guess, my hypothesis is that the digestive cycle works

200
00:10:47,370 --> 00:10:50,400
 on a 24-hour, something like a 24-hour cycle anyway.

201
00:10:50,400 --> 00:10:52,400
 You fully digest food.

202
00:10:52,400 --> 00:10:55,430
 Maybe that's a myth, but as I understood it was a 24-hour

203
00:10:55,430 --> 00:10:55,760
 thing.

204
00:10:55,760 --> 00:10:59,180
 So you get into a very regular cycle and your bowel

205
00:10:59,180 --> 00:11:01,840
 movements are regular.

206
00:11:02,440 --> 00:11:04,440
 Everything seems quite

207
00:11:04,440 --> 00:11:11,320
 efficient, eating only one meal a day.

208
00:11:11,320 --> 00:11:17,520
 But again, the much bigger problem is, could you imagine

209
00:11:17,520 --> 00:11:18,160
 eating

210
00:11:18,160 --> 00:11:20,360
 10 meals a day, for example?

211
00:11:20,360 --> 00:11:23,380
 I don't know how many meals they say is good, at least four

212
00:11:23,380 --> 00:11:26,400
 or six maybe, eating throughout the day.

213
00:11:26,400 --> 00:11:29,000
 Great, so it's good for your body. What are you, a cow?

214
00:11:29,000 --> 00:11:33,930
 I mean, is this how you want to live your life, live to eat

215
00:11:33,930 --> 00:11:34,040
?

216
00:11:34,040 --> 00:11:37,080
 You know, that, you know, problem.

217
00:11:37,080 --> 00:11:39,560
 Problem is addiction.

218
00:11:39,560 --> 00:11:42,760
 If you eat many times a day, who cares how good it is?

219
00:11:42,760 --> 00:11:46,280
 It's sort of this, you know, this stereotypical

220
00:11:46,280 --> 00:11:50,880
 the cynic who looks at the health nut.

221
00:11:50,880 --> 00:11:53,640
 A person who

222
00:11:53,640 --> 00:11:57,070
 obsesses over their food and is all into everything as this

223
00:11:57,070 --> 00:11:59,550
, this, this. I mean, I'm pretty particular personally,

224
00:11:59,550 --> 00:12:00,120
 knowing

225
00:12:00,120 --> 00:12:04,580
 not to use trans fatty acids and not to, you know, trying

226
00:12:04,580 --> 00:12:06,160
 to stay away from fried foods.

227
00:12:06,160 --> 00:12:10,520
 You know, red meat, you know, meat in general, you know,

228
00:12:10,520 --> 00:12:12,520
 trying to stay away from too much sugar,

229
00:12:12,520 --> 00:12:14,920
 etc., etc.

230
00:12:14,920 --> 00:12:17,440
 Chemicals, you know, not wanting to eat stuff if it's

231
00:12:17,440 --> 00:12:19,640
 pumped full of chemicals. These I think,

232
00:12:20,680 --> 00:12:24,680
 I mean, it feels kind of like a healthy, you know, sort of

233
00:12:24,680 --> 00:12:25,480
 discrimination.

234
00:12:25,480 --> 00:12:28,720
 But people can go extreme and they live their lives, you

235
00:12:28,720 --> 00:12:28,840
 know,

236
00:12:28,840 --> 00:12:31,640
 about their food, obsessing over their food. And

237
00:12:31,640 --> 00:12:35,000
 see some of these people and they have their raw grains and

238
00:12:35,000 --> 00:12:35,640
 nuts and it's,

239
00:12:35,640 --> 00:12:38,950
 it's just, it's heaven for them, you know. And this is an

240
00:12:38,950 --> 00:12:41,400
 obsession. It feels good, but it's an addiction.

241
00:12:41,400 --> 00:12:47,000
 It doesn't, it's not necessarily that, but

242
00:12:47,000 --> 00:12:49,640
 simply eating many times a day is

243
00:12:50,200 --> 00:12:52,600
 encouraging your addiction. So again, we're much more

244
00:12:52,600 --> 00:12:53,720
 concerned with the mind.

245
00:12:53,720 --> 00:12:57,080
 You're going to die, but if you die full of

246
00:12:57,080 --> 00:12:59,880
 lust and greed for food,

247
00:12:59,880 --> 00:13:03,480
 you might be born, reborn as a cow or a pig, pig, you know.

248
00:13:03,480 --> 00:13:05,160
 Look at how pigs eat. They'll eat anything.

249
00:13:05,160 --> 00:13:09,780
 Because they're so caught up in food, getting caught up in

250
00:13:09,780 --> 00:13:12,120
 food very dangerous mentally, spiritually.

251
00:13:12,120 --> 00:13:14,920
 Okay.

252
00:13:14,920 --> 00:13:17,320
 Number four, sleeping at least at six hours a night. Now,

253
00:13:17,320 --> 00:13:18,040
 that's the easiest.

254
00:13:18,120 --> 00:13:20,570
 Obviously that has much to do with the state of mind. A

255
00:13:20,570 --> 00:13:23,110
 person who is highly stressed out probably needs about

256
00:13:23,110 --> 00:13:24,680
 eight hours of sleep, you know.

257
00:13:24,680 --> 00:13:27,560
 Maybe even more.

258
00:13:27,560 --> 00:13:30,590
 A person who isn't stressed out. I've got a meditator

259
00:13:30,590 --> 00:13:31,320
 downstairs

260
00:13:31,320 --> 00:13:36,040
 who I told to sleep four hours a night, you know. We have

261
00:13:36,040 --> 00:13:38,630
 meditators who don't sleep, who practice all day and all

262
00:13:38,630 --> 00:13:39,160
 night.

263
00:13:39,160 --> 00:13:44,310
 There was a monk, there's monks do this for months, you

264
00:13:44,310 --> 00:13:44,760
 know.

265
00:13:44,760 --> 00:13:47,720
 There's a story, the first verse of the Dhammapada.

266
00:13:49,240 --> 00:13:51,630
 The background story is about a monk who did this for three

267
00:13:51,630 --> 00:13:52,440
 months.

268
00:13:52,440 --> 00:13:55,080
 And

269
00:13:55,080 --> 00:13:59,560
 Apropo, he became blind as a result of his practice. He

270
00:13:59,560 --> 00:14:03,720
 destroyed his eyes. So he really hurt his body.

271
00:14:03,720 --> 00:14:07,800
 But at the same time he became an arahant. So they called

272
00:14:07,800 --> 00:14:09,000
 him "Chakubala".

273
00:14:09,000 --> 00:14:13,790
 "Chaku" means eye, bala is guardian. He's one who guarded

274
00:14:13,790 --> 00:14:14,840
 his eyes.

275
00:14:15,720 --> 00:14:20,130
 It's a play on words because there's two kinds of eyes. The

276
00:14:20,130 --> 00:14:22,840
 eye of Dhamma and the physical eye.

277
00:14:22,840 --> 00:14:27,400
 So he protected the important vision. He gained

278
00:14:27,400 --> 00:14:30,940
 the vision that was most important and became blind

279
00:14:30,940 --> 00:14:31,320
 physically.

280
00:14:31,320 --> 00:14:36,840
 He dispelled his spiritual blindness at the same moment

281
00:14:36,840 --> 00:14:41,800
 as losing his physical vision.

282
00:14:41,800 --> 00:14:45,080
 So

283
00:14:46,280 --> 00:14:48,980
 again, two parts to the answer. One, it's certainly, you

284
00:14:48,980 --> 00:14:52,470
 know, these kinds of studies are dealing with ordinary

285
00:14:52,470 --> 00:14:55,720
 people who aren't doing intensive meditation.

286
00:14:55,720 --> 00:15:00,440
 But also not such a big deal to

287
00:15:00,440 --> 00:15:03,080
 neglect

288
00:15:03,080 --> 00:15:06,350
 the body in favor of the mind to some extent. And of course

289
00:15:06,350 --> 00:15:08,200
 you rely on the body. It's no good

290
00:15:08,200 --> 00:15:11,160
 dying.

291
00:15:11,160 --> 00:15:13,960
 But certainly better to die

292
00:15:14,660 --> 00:15:16,660
 than to be mindful than to live

293
00:15:16,660 --> 00:15:19,600
 corrupt.

294
00:15:19,600 --> 00:15:22,320
 So thank you. Good question, but

295
00:15:22,320 --> 00:15:28,000
 we'll have, maybe it's a bit of a disagreement. So anyway.

