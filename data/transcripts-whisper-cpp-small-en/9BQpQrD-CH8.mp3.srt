1
00:00:00,000 --> 00:00:06,420
 Hey everyone, I'm going to try the idea of uploading short

2
00:00:06,420 --> 00:00:09,720
 videos where I talk to you about something that is

3
00:00:09,720 --> 00:00:13,000
 happening in my life or something that I think is important

4
00:00:13,000 --> 00:00:15,000
 that I want to let you know about.

5
00:00:15,000 --> 00:00:18,250
 So, kind of like video logging. I've done it before. I'm

6
00:00:18,250 --> 00:00:22,000
 going to try it again and see how it goes.

7
00:00:22,000 --> 00:00:26,970
 So tonight I wanted to talk to you about two things, just

8
00:00:26,970 --> 00:00:30,000
 for those of you who weren't aware.

9
00:00:30,000 --> 00:00:36,580
 The first is that, based on the request of one of my

10
00:00:36,580 --> 00:00:44,290
 followers, I've found out that there is a YouTube feature

11
00:00:44,290 --> 00:00:51,840
 that now allows other people to submit subtitles for your

12
00:00:51,840 --> 00:00:53,000
 videos.

13
00:00:53,000 --> 00:00:56,590
 So people have asked before about submitting subtitles,

14
00:00:56,590 --> 00:01:00,230
 translations in other languages, and as far as I know, you

15
00:01:00,230 --> 00:01:02,000
 can now do that on my videos.

16
00:01:02,000 --> 00:01:08,000
 So, somehow there is somewhere a means for you to do that.

17
00:01:08,000 --> 00:01:10,530
 If you're aware of these things or you know how they work,

18
00:01:10,530 --> 00:01:12,000
 you can now go ahead and do it.

19
00:01:12,000 --> 00:01:14,670
 So just to let you know, if you wanted to make subtitles, I

20
00:01:14,670 --> 00:01:17,640
 think it's now possible. Let me know if it's not. We'll try

21
00:01:17,640 --> 00:01:19,000
 to figure it out.

22
00:01:19,000 --> 00:01:22,220
 That's one thing. So, anybody who wants to submit them,

23
00:01:22,220 --> 00:01:25,500
 please do. It's a great thing to be able to share these

24
00:01:25,500 --> 00:01:27,560
 things if you think that they're good teachings to share

25
00:01:27,560 --> 00:01:31,000
 them with other people who don't speak English.

26
00:01:31,000 --> 00:01:36,490
 And the other thing is, along the same lines, translating

27
00:01:36,490 --> 00:01:42,860
 my booklet, "How to Meditate", which is located at htm.siri

28
00:01:42,860 --> 00:01:45,000
-mongolo.org.

29
00:01:45,000 --> 00:01:49,370
 It's got its own page, its own website there. And so

30
00:01:49,370 --> 00:01:52,850
 translating it into languages that it isn't already

31
00:01:52,850 --> 00:01:54,000
 translated into.

32
00:01:54,000 --> 00:01:57,740
 We've translated it into something like 18 different

33
00:01:57,740 --> 00:02:01,000
 languages, but we're always looking for more.

34
00:02:01,000 --> 00:02:05,250
 My teacher said he wanted us to get 110 languages, I think,

35
00:02:05,250 --> 00:02:07,000
 or something like that.

36
00:02:07,000 --> 00:02:10,460
 So please do. If there's a language that hasn't been

37
00:02:10,460 --> 00:02:14,310
 translated and you'd like to do that, great work. Please go

38
00:02:14,310 --> 00:02:15,000
 ahead and do that.

39
00:02:15,000 --> 00:02:18,080
 The final thing is something that I've been meaning to let

40
00:02:18,080 --> 00:02:20,920
 people know. I've been letting people know, but I haven't

41
00:02:20,920 --> 00:02:22,000
 done a video about it.

42
00:02:22,000 --> 00:02:25,260
 So here's a video that talks specifically about it. As we

43
00:02:25,260 --> 00:02:28,000
've started to do online meditation courses.

44
00:02:28,000 --> 00:02:32,790
 So I'm meeting with meditators, four different meditators,

45
00:02:32,790 --> 00:02:36,000
 up to four different meditators a day.

46
00:02:36,000 --> 00:02:41,000
 Actually, six on Saturday. And we meet one on one.

47
00:02:41,000 --> 00:02:44,660
 And this is for people who are undertaking daily practice

48
00:02:44,660 --> 00:02:46,000
 at least an hour a day.

49
00:02:46,000 --> 00:02:49,260
 And we try to get up to two hours a day and would like to

50
00:02:49,260 --> 00:02:54,010
 go through a more comprehensive practice than is found in

51
00:02:54,010 --> 00:02:56,000
 the simple booklet on "How to Meditate",

52
00:02:56,000 --> 00:02:59,940
 which is really just about foundation practice. So if you

53
00:02:59,940 --> 00:03:07,000
're interested, you can go to meditation.serimongolo.org.

54
00:03:07,000 --> 00:03:09,980
 Sign up, register. You just enter your username and

55
00:03:09,980 --> 00:03:12,000
 password, then click register.

56
00:03:12,000 --> 00:03:14,840
 And then there's a meet page and you click there and you

57
00:03:14,840 --> 00:03:18,300
 can choose a slot. You just click on the slot and then you

58
00:03:18,300 --> 00:03:19,000
're set.

59
00:03:19,000 --> 00:03:23,240
 You need to know how to use Hangouts, Google Hangouts. And

60
00:03:23,240 --> 00:03:26,640
 you need to be familiar with the Hangouts interface at hang

61
00:03:26,640 --> 00:03:28,000
outs.google.com.

62
00:03:28,000 --> 00:03:32,640
 But there's a little bit of explanation about that on the

63
00:03:32,640 --> 00:03:34,000
 page itself.

64
00:03:34,000 --> 00:03:39,680
 And you need a webcam and a microphone, preferably a

65
00:03:39,680 --> 00:03:44,000
 headset. But, you know, all that's negotiable.

66
00:03:44,000 --> 00:03:47,870
 And then we just meet once a week and you practice at least

67
00:03:47,870 --> 00:03:50,000
 an hour a day. And every week I give you a new exercise.

68
00:03:50,000 --> 00:03:53,470
 So anyone who's interested in that, please do take

69
00:03:53,470 --> 00:03:55,000
 advantage of that.

70
00:03:55,000 --> 00:03:57,940
 There are limited slots available, but as people finish the

71
00:03:57,940 --> 00:04:03,000
 course, new slots become available. So always check back.

72
00:04:03,000 --> 00:04:06,320
 And okay, so that's it. Those are some things that I

73
00:04:06,320 --> 00:04:08,000
 thought you should know about.

74
00:04:08,000 --> 00:04:11,630
 And I think I'll do this whenever I think of something that

75
00:04:11,630 --> 00:04:14,000
 I think you all should know about. I'll just make a video

76
00:04:14,000 --> 00:04:14,000
 about it.

77
00:04:14,000 --> 00:04:18,140
 So I hope that's useful and helpful. And thank you all for

78
00:04:18,140 --> 00:04:20,000
 being helpful in that regard.

79
00:04:20,000 --> 00:04:24,000
 I wish you all good practice. Be well.

