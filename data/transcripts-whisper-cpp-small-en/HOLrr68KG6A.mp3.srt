1
00:00:00,000 --> 00:00:05,820
 In intellectual terms, I cannot grasp the implementation of

2
00:00:05,820 --> 00:00:08,000
 Buddhist philosophy to motivation.

3
00:00:08,000 --> 00:00:12,030
 What is wrong with the claim that successful practice would

4
00:00:12,030 --> 00:00:14,000
 ultimately lead to being a vegetable?

5
00:00:14,000 --> 00:00:17,000
 Poof, you're a vegetable.

6
00:00:17,000 --> 00:00:20,000
 Depends what type.

7
00:00:20,000 --> 00:00:22,000
 Carrot.

8
00:00:26,000 --> 00:00:30,050
 It means ultimately lead you to just sit in a puddle of

9
00:00:30,050 --> 00:00:34,000
 your own urine and feces and die.

10
00:00:34,000 --> 00:00:39,460
 But the Buddha almost did just that, didn't he? Not the

11
00:00:39,460 --> 00:00:41,000
 urine and feces part.

12
00:00:41,000 --> 00:00:46,000
 The Buddha was ready to just pass away.

13
00:00:46,000 --> 00:00:50,790
 I mean, the first point I think is that not everything

14
00:00:50,790 --> 00:00:54,000
 comes from karmic volition.

15
00:00:54,000 --> 00:00:59,000
 When you let go, you don't stop acting like a human being.

16
00:00:59,000 --> 00:01:02,000
 You are a human being.

17
00:01:02,000 --> 00:01:05,310
 I always like to refer to quantum physics because it's just

18
00:01:05,310 --> 00:01:06,000
 so Western.

19
00:01:06,000 --> 00:01:10,000
 A lot of these questions are just so Western.

20
00:01:10,000 --> 00:01:13,730
 Maybe even not quantum physics. Let's just think of a

21
00:01:13,730 --> 00:01:17,000
 theory of reality where it's mostly physical.

22
00:01:17,000 --> 00:01:23,070
 Where the mind has a very limited potential to interact

23
00:01:23,070 --> 00:01:25,000
 with the reality.

24
00:01:25,000 --> 00:01:29,660
 It does have an ability to interact and change, but not

25
00:01:29,660 --> 00:01:32,000
 ultimate free will in the sense of saying,

26
00:01:32,000 --> 00:01:35,000
 "Poof, you're a vegetable."

27
00:01:38,000 --> 00:01:47,220
 Even when the mind stops actively changing, even if the

28
00:01:47,220 --> 00:01:50,000
 mind were to stop actively changing the physical reality,

29
00:01:50,000 --> 00:01:53,000
 much of it would still continue.

30
00:01:53,000 --> 00:01:57,100
 There would still be lots of walking and lots of even

31
00:01:57,100 --> 00:01:59,000
 talking, for example.

32
00:01:59,000 --> 00:02:04,710
 There would still be the ordinary activities that were

33
00:02:04,710 --> 00:02:09,000
 habitual and appropriate at the time.

34
00:02:09,000 --> 00:02:12,240
 An enlightened being doesn't stop acting, doesn't stop

35
00:02:12,240 --> 00:02:16,000
 thinking, doesn't even necessarily stop acting mentally.

36
00:02:16,000 --> 00:02:18,220
 They don't stop acting mentally in terms of changing the

37
00:02:18,220 --> 00:02:19,000
 physical reality,

38
00:02:19,000 --> 00:02:33,620
 but they stop forcing it. They stop creating volitional

39
00:02:33,620 --> 00:02:37,000
 tendencies like desires.

40
00:02:37,000 --> 00:02:41,090
 They can still have intention. The intention to do this,

41
00:02:41,090 --> 00:02:43,000
 the intention to do that.

42
00:02:43,000 --> 00:02:46,590
 Because that's all, you could say, habitual, but I think

43
00:02:46,590 --> 00:02:48,000
 better it's appropriate.

44
00:02:48,000 --> 00:02:51,040
 An enlightened being is someone who does things that are

45
00:02:51,040 --> 00:02:52,000
 appropriate.

46
00:02:52,000 --> 00:02:57,040
 The key here, I think, is you're in intellectual terms,

47
00:02:57,040 --> 00:02:58,000
 because you're thinking in intellectual terms.

48
00:02:58,000 --> 00:03:01,500
 It's not intellectual, it's real. It just happens. You let

49
00:03:01,500 --> 00:03:03,000
 go and you're free.

50
00:03:03,000 --> 00:03:09,230
 When you use terms like motivation and vegetable, Buddhist

51
00:03:09,230 --> 00:03:10,000
 philosophy,

52
00:03:10,000 --> 00:03:17,000
 you're creating motivation, you're creating this idea of

53
00:03:17,000 --> 00:03:19,000
 being motivated

54
00:03:19,000 --> 00:03:22,000
 and it creates dichotomies or trichotomy.

55
00:03:22,000 --> 00:03:25,760
 It sets you up for either this or that, either motivated or

56
00:03:25,760 --> 00:03:29,000
 not, either successful or not.

57
00:03:29,000 --> 00:03:31,530
 Which is really not the case. A person who sees clearly

58
00:03:31,530 --> 00:03:34,000
 lets go and when they let go they're free.

59
00:03:34,000 --> 00:03:36,650
 That has nothing to do with whether or not they do this or

60
00:03:36,650 --> 00:03:38,000
 do that.

61
00:03:38,000 --> 00:03:41,000
 They can be highly motivated people to an extent.

62
00:03:41,000 --> 00:03:46,770
 Or something akin to motivation, I would say, dedicated and

63
00:03:46,770 --> 00:03:49,000
 energetic people.

64
00:03:49,000 --> 00:03:51,300
 Because they have these qualities in their mind, not

65
00:03:51,300 --> 00:03:53,000
 because they want them to be there

66
00:03:53,000 --> 00:03:58,000
 or because they need them or they actively seek them out.

67
00:03:58,000 --> 00:04:03,000
 But because it's appropriate, it's proper, it's natural.

68
00:04:03,000 --> 00:04:08,000
 Buddhism is about coming back to nature, which in Arahant

69
00:04:08,000 --> 00:04:10,000
 you could say is the most natural human being or being.

