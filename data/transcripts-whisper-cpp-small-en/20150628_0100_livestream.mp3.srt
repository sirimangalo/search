1
00:00:00,000 --> 00:00:07,000
 Okay, we're connected.

2
00:00:07,000 --> 00:00:13,000
 Good evening, everyone.

3
00:00:13,000 --> 00:00:20,000
 Broadcasting live from Stony Creek, Ontario.

4
00:00:20,000 --> 00:00:29,000
 I don't know if anyone's actually listening tonight, but

5
00:00:29,000 --> 00:00:33,000
 we have these recorded, so

6
00:00:33,000 --> 00:00:42,000
 assume that there may be people listening.

7
00:00:42,000 --> 00:00:46,000
 We talk a lot about meditation being difficult.

8
00:00:46,000 --> 00:00:50,000
 There's no denying the fact that for most of us,

9
00:00:50,000 --> 00:00:52,000
 meditation is difficult.

10
00:00:52,000 --> 00:00:56,000
 It should be the most difficult thing we've ever done,

11
00:00:56,000 --> 00:01:00,000
 because if we'd ever done something like this before,

12
00:01:00,000 --> 00:01:03,000
 we'd have no need to do it again.

13
00:01:03,000 --> 00:01:07,000
 It's not something you could have ever done before.

14
00:01:07,000 --> 00:01:09,000
 Maybe that's not true.

15
00:01:09,000 --> 00:01:12,000
 There's a certain stage that you can get to.

16
00:01:12,000 --> 00:01:17,000
 You can practice some of the way and then drop off

17
00:01:17,000 --> 00:01:19,000
 and leave the meditation behind.

18
00:01:19,000 --> 00:01:25,000
 So that's possible that we've done some before.

19
00:01:25,000 --> 00:01:28,000
 But compared to other things we do in this life,

20
00:01:28,000 --> 00:01:34,000
 the difficulty of meditation, it's surprising.

21
00:01:34,000 --> 00:01:39,000
 The wonderful thing about meditation is how deep it goes,

22
00:01:39,000 --> 00:01:43,000
 how deep the changes are.

23
00:01:43,000 --> 00:01:50,000
 It's something that surprises you.

24
00:01:50,000 --> 00:01:52,000
 It's not like anything else.

25
00:01:52,000 --> 00:01:55,930
 Think of any description you can use to what meditation

26
00:01:55,930 --> 00:01:56,000
 does for you.

27
00:01:56,000 --> 00:02:00,080
 It will ultimately fall short because it's teaching you

28
00:02:00,080 --> 00:02:02,000
 something new.

29
00:02:02,000 --> 00:02:09,000
 It's reaching down to the very depths of our being.

30
00:02:09,000 --> 00:02:15,000
 It's like when a computer loses its power supply

31
00:02:15,000 --> 00:02:19,000
 or when you mess with a computer's power supply.

32
00:02:19,000 --> 00:02:22,000
 You can't go any deeper than that.

33
00:02:22,000 --> 00:02:27,000
 It's the ultimate.

34
00:02:27,000 --> 00:02:30,000
 It's at the core of who we are.

35
00:02:30,000 --> 00:02:33,000
 It takes away that core.

36
00:02:33,000 --> 00:02:39,000
 It's like an atom bomb, the splitting of an atom.

37
00:02:39,000 --> 00:02:42,000
 You split the atom of a being.

38
00:02:42,000 --> 00:02:50,000
 The point is it's profound, difficult, surprising.

39
00:02:50,000 --> 00:02:53,240
 But the thing about difficulty, the difficulty of

40
00:02:53,240 --> 00:02:54,000
 meditation,

41
00:02:54,000 --> 00:03:00,000
 it's a unique sort of difficulty as well.

42
00:03:00,000 --> 00:03:03,150
 In fact, I don't know that it's fair to say that the

43
00:03:03,150 --> 00:03:08,000
 meditation itself is difficult.

44
00:03:08,000 --> 00:03:12,000
 I guess in some way it's fair.

45
00:03:12,000 --> 00:03:14,570
 When we say meditation is difficult, what we're usually

46
00:03:14,570 --> 00:03:19,000
 talking about is

47
00:03:19,000 --> 00:03:28,890
 the stress and suffering that is associated with inside

48
00:03:28,890 --> 00:03:33,000
 meditation.

49
00:03:33,000 --> 00:03:38,020
 Tranquility meditation is difficult, but it's difficult in

50
00:03:38,020 --> 00:03:39,000
 a different way.

51
00:03:39,000 --> 00:03:42,000
 It's difficult in terms of the challenge.

52
00:03:42,000 --> 00:03:47,000
 It just takes a bit of work to cultivate tranquility.

53
00:03:47,000 --> 00:03:52,180
 But inside meditation is difficult when you're doing it

54
00:03:52,180 --> 00:03:53,000
 well.

55
00:03:53,000 --> 00:03:58,090
 When you're doing it well, it's difficult because it shows

56
00:03:58,090 --> 00:03:59,000
 you

57
00:03:59,000 --> 00:04:03,000
 what you're doing wrong.

58
00:04:03,000 --> 00:04:06,030
 So much of the difficulty of inside meditation isn't

59
00:04:06,030 --> 00:04:08,000
 actually the meditation.

60
00:04:08,000 --> 00:04:16,000
 It's that we're...

61
00:04:16,000 --> 00:04:28,000
 It's the problems with our mind that we're seeing.

62
00:04:28,000 --> 00:04:31,680
 Through the practice of meditation, we see what we're doing

63
00:04:31,680 --> 00:04:32,000
 wrong.

64
00:04:32,000 --> 00:04:37,810
 So the problem with the suffering that comes when we med

65
00:04:37,810 --> 00:04:38,000
itate

66
00:04:38,000 --> 00:04:42,000
 is not caused by the meditation.

67
00:04:42,000 --> 00:04:48,000
 Inside meditation is designed to help you see the problem.

68
00:04:48,000 --> 00:04:52,000
 It doesn't cause the problem. It doesn't cause suffering.

69
00:04:52,000 --> 00:04:55,000
 It allows us to see it.

70
00:04:55,000 --> 00:04:58,000
 It puts us in a position where we can just watch

71
00:04:58,000 --> 00:05:04,000
 all the ways that we cause ourselves suffering.

72
00:05:04,000 --> 00:05:10,000
 That's what inside meditation is for.

73
00:05:10,000 --> 00:05:12,450
 So in a way you can't say that it's the meditation that's

74
00:05:12,450 --> 00:05:13,000
 difficult.

75
00:05:13,000 --> 00:05:15,000
 It's us that's difficult.

76
00:05:15,000 --> 00:05:21,000
 Meditation is actually the process of fixing the problem,

77
00:05:21,000 --> 00:05:28,000
 fixing the difficulty, making things easier.

78
00:05:28,000 --> 00:05:34,000
 The Buddha is often compared to a doctor.

79
00:05:34,000 --> 00:05:37,000
 You could think of the Buddha as like a mechanic as well,

80
00:05:37,000 --> 00:05:46,000
 or Buddhism as like fixing an automobile or something.

81
00:05:46,000 --> 00:05:49,000
 When you go in and you look around, you see the problem.

82
00:05:49,000 --> 00:05:53,750
 When a doctor goes in or when a mechanic goes in, they see

83
00:05:53,750 --> 00:05:57,000
 the problem.

84
00:05:57,000 --> 00:06:02,180
 And so there's some difficulty involved in surgery or

85
00:06:02,180 --> 00:06:03,000
 mechanics,

86
00:06:03,000 --> 00:06:07,000
 to say the least, but it's about fixing things,

87
00:06:07,000 --> 00:06:16,000
 about fixing problems, curing the problem.

88
00:06:16,000 --> 00:06:19,810
 Meditation feels like this. It feels very difficult at

89
00:06:19,810 --> 00:06:20,000
 times.

90
00:06:20,000 --> 00:06:28,000
 Doing it again and again. Sometimes we don't want to do it.

91
00:06:28,000 --> 00:06:32,990
 Our aversion to meditation, to inside meditation especially

92
00:06:32,990 --> 00:06:33,000
,

93
00:06:33,000 --> 00:06:35,000
 has nothing to do with the meditation itself,

94
00:06:35,000 --> 00:06:38,000
 because the meditation is wonderful.

95
00:06:38,000 --> 00:06:42,670
 The thing about insight meditation is you can't cling to it

96
00:06:42,670 --> 00:06:43,000
.

97
00:06:43,000 --> 00:06:50,000
 There's nothing to hold on to.

98
00:06:50,000 --> 00:06:53,000
 With any other exercise you can hold on to something,

99
00:06:53,000 --> 00:06:57,230
 hold on to a goal like a diet, and then suddenly I'll look

100
00:06:57,230 --> 00:06:58,000
 more beautiful,

101
00:06:58,000 --> 00:07:01,640
 so you hold on to that, or I'll exercise and that'll make

102
00:07:01,640 --> 00:07:02,000
 me stronger,

103
00:07:02,000 --> 00:07:05,000
 and so you hold on to that.

104
00:07:05,000 --> 00:07:09,000
 Even tranquility meditation, I'll feel calm.

105
00:07:09,000 --> 00:07:11,940
 Most people meditate because they have a goal that they

106
00:07:11,940 --> 00:07:13,000
 want to feel calm.

107
00:07:13,000 --> 00:07:16,000
 That's why eventually a meditator wants to run away.

108
00:07:16,000 --> 00:07:20,130
 In insight meditation there comes a time when you just want

109
00:07:20,130 --> 00:07:21,000
 to stop,

110
00:07:21,000 --> 00:07:25,000
 because you can't hold on anymore.

111
00:07:25,000 --> 00:07:29,000
 You start to freak out and there's nothing to hold on to.

112
00:07:29,000 --> 00:07:32,000
 You get the calm and that doesn't satisfy you.

113
00:07:32,000 --> 00:07:36,210
 You can feel happy or you learn things and it just doesn't

114
00:07:36,210 --> 00:07:38,000
 satisfy you.

115
00:07:38,000 --> 00:07:44,000
 Your mind is looking for satisfaction.

116
00:07:44,000 --> 00:07:59,000
 You can't find it anywhere.

117
00:07:59,000 --> 00:08:08,000
 In insight meditation is difficult, difficult because it

118
00:08:08,000 --> 00:08:08,000
 shows us things

119
00:08:08,000 --> 00:08:11,000
 we otherwise don't want to see.

120
00:08:11,000 --> 00:08:14,300
 In that sense it's not really difficult at all because all

121
00:08:14,300 --> 00:08:15,000
 of those things

122
00:08:15,000 --> 00:08:18,630
 are already present in our lives and could come up at any

123
00:08:18,630 --> 00:08:19,000
 time.

124
00:08:19,000 --> 00:08:21,000
 They're already there.

125
00:08:21,000 --> 00:08:24,000
 What we're talking about is the difficulties of life.

126
00:08:24,000 --> 00:08:27,450
 The problem is the reason as we come to meditate is often

127
00:08:27,450 --> 00:08:28,000
 to escape.

128
00:08:28,000 --> 00:08:34,000
 These problems which is really the root of the problem.

129
00:08:34,000 --> 00:08:41,000
 It's reacting to things.

130
00:08:41,000 --> 00:08:49,000
 The meditation teacher's job is often not entirely honest.

131
00:08:49,000 --> 00:08:52,000
 It actually doesn't really matter how honest you are

132
00:08:52,000 --> 00:08:54,000
 because in the beginning

133
00:08:54,000 --> 00:08:55,000
 it's hard to get through.

134
00:08:55,000 --> 00:08:57,000
 You don't really get through to the meditator.

135
00:08:57,000 --> 00:08:59,000
 You can tell them how difficult it's going to be.

136
00:08:59,000 --> 00:09:05,000
 They don't realize the sense in which it's difficult.

137
00:09:05,000 --> 00:09:08,230
 Meditation is not difficult. We're difficult from the

138
00:09:08,230 --> 00:09:09,000
 problem.

139
00:09:09,000 --> 00:09:13,000
 But until you meditate you can't really understand that.

140
00:09:13,000 --> 00:09:18,000
 You can't really understand the depth of what that means.

141
00:09:18,000 --> 00:09:22,000
 When you meditate you start to see.

142
00:09:22,000 --> 00:09:25,640
 The difficulty in meditation is really that you can't cling

143
00:09:25,640 --> 00:09:26,000
 to it.

144
00:09:26,000 --> 00:09:30,000
 So you practice and you feel really good that you've gotten

145
00:09:30,000 --> 00:09:30,000
.

146
00:09:30,000 --> 00:09:33,000
 You feel really confident that you've gotten somewhere.

147
00:09:33,000 --> 00:09:36,000
 But anytime you think about it you can't hold on to it and

148
00:09:36,000 --> 00:09:36,000
 say,

149
00:09:36,000 --> 00:09:39,000
 "Yeah, I'm going to go and do that."

150
00:09:39,000 --> 00:09:42,000
 There's no desire that can arise.

151
00:09:42,000 --> 00:09:46,000
 You can in the beginning if you have ideas about goals and

152
00:09:46,000 --> 00:09:46,000
 so on.

153
00:09:46,000 --> 00:09:54,000
 But true meditation, true insight, you can't cling to it.

154
00:09:54,000 --> 00:09:58,000
 It's very tricky in that way.

155
00:09:58,000 --> 00:10:00,000
 But we shouldn't despair.

156
00:10:00,000 --> 00:10:03,420
 Either way we shouldn't despair because meditation brings

157
00:10:03,420 --> 00:10:04,000
 up things

158
00:10:04,000 --> 00:10:06,000
 that we're not able to deal with.

159
00:10:06,000 --> 00:10:09,350
 Or we shouldn't despair that meditation is difficult, is a

160
00:10:09,350 --> 00:10:12,000
 challenge or whatever.

161
00:10:12,000 --> 00:10:14,460
 Why shouldn't we despair? We shouldn't despair because we

162
00:10:14,460 --> 00:10:15,000
've already done

163
00:10:15,000 --> 00:10:20,000
 very difficult things to get where we are today.

164
00:10:20,000 --> 00:10:26,000
 We're beings who have survived, who have succeeded through

165
00:10:26,000 --> 00:10:28,000
 great difficulty,

166
00:10:28,000 --> 00:10:31,000
 which is a good sign.

167
00:10:31,000 --> 00:10:34,270
 If we're looking for encouragement, something good about

168
00:10:34,270 --> 00:10:35,000
 ourselves,

169
00:10:35,000 --> 00:10:39,540
 we should look here and realize the difficulty that we've

170
00:10:39,540 --> 00:10:41,000
 already faced.

171
00:10:41,000 --> 00:10:46,000
 We face difficulty just in being born a human being.

172
00:10:46,000 --> 00:10:50,000
 No matter who we are or the bad things we've done or how

173
00:10:50,000 --> 00:10:50,000
 guilty we feel

174
00:10:50,000 --> 00:10:53,670
 about how useless or lazy we are, no matter how deep those

175
00:10:53,670 --> 00:10:55,000
 feelings can go.

176
00:10:55,000 --> 00:10:58,000
 We've got to admit we did something right.

177
00:10:58,000 --> 00:11:03,870
 It has to be accepted that we're born as a being that's not

178
00:11:03,870 --> 00:11:07,000
 easy to be born.

179
00:11:07,000 --> 00:11:11,120
 Whether you believe in past lives or not, it's something

180
00:11:11,120 --> 00:11:12,000
 special.

181
00:11:12,000 --> 00:11:16,000
 There's something special about us.

182
00:11:16,000 --> 00:11:18,600
 Suppose you want to believe that it was just chance or not

183
00:11:18,600 --> 00:11:20,000
 chance,

184
00:11:20,000 --> 00:11:25,000
 but some totally meaningless reason, natural selection,

185
00:11:25,000 --> 00:11:30,880
 however you put it, or no reason at all I was born a human

186
00:11:30,880 --> 00:11:32,000
 being.

187
00:11:32,000 --> 00:11:44,000
 Something more interesting, more complex, more powerful

188
00:11:44,000 --> 00:11:48,000
 than being born as an ordinary animal.

189
00:11:48,000 --> 00:11:54,270
 In Buddhism we believe that that was something good that we

190
00:11:54,270 --> 00:11:55,000
 did,

191
00:11:55,000 --> 00:12:00,000
 something powerful that we did in the past.

192
00:12:00,000 --> 00:12:02,000
 So if you believe that we've done that.

193
00:12:02,000 --> 00:12:04,000
 Even if you don't believe that one,

194
00:12:04,000 --> 00:12:07,920
 we've done something very difficult to live our lives

195
00:12:07,920 --> 00:12:09,000
 successfully,

196
00:12:09,000 --> 00:12:17,000
 to not die yet,

197
00:12:17,000 --> 00:12:26,000
 to not lose a limb, to learn to speak, to learn to read,

198
00:12:26,000 --> 00:12:31,000
 to learn to get a job and work and all these things.

199
00:12:31,000 --> 00:12:34,000
 We've made a lot of mistakes, surely,

200
00:12:34,000 --> 00:12:38,000
 but we've done good things to get where we are.

201
00:12:38,000 --> 00:12:46,410
 We've managed to survive, managed to learn enough to

202
00:12:46,410 --> 00:12:52,000
 continue our lives.

203
00:12:52,000 --> 00:12:59,200
 Beyond that we have gained the opportunity to meet with

204
00:12:59,200 --> 00:13:00,000
 spiritual teachings,

205
00:13:00,000 --> 00:13:03,000
 with not just spiritual teachings but Buddhist teachings.

206
00:13:03,000 --> 00:13:06,000
 I know this because you're listening right now.

207
00:13:06,000 --> 00:13:09,620
 I consider this to be a Buddhist teaching, the Buddha

208
00:13:09,620 --> 00:13:10,000
 taught.

209
00:13:10,000 --> 00:13:16,000
 "Kit-chang-sadhama-savanam" or "desanam"

210
00:13:16,000 --> 00:13:22,000
 It's hard to obtain the teaching of the Dhamma.

211
00:13:22,000 --> 00:13:24,000
 It's not an easy thing to meet with.

212
00:13:24,000 --> 00:13:33,000
 "Kit-chou-manus" or "Kit-cho"

213
00:13:33,000 --> 00:13:39,970
 Difficult, I can't remember the poly, difficult to obtain a

214
00:13:39,970 --> 00:13:42,000
 human birth.

215
00:13:42,000 --> 00:13:55,000
 "Kit-chou-machan-jivitam" Difficult is a human life.

216
00:13:55,000 --> 00:13:58,000
 All these teachings of the Buddha,

217
00:13:58,000 --> 00:14:02,000
 so all this teaching of the Buddha, difficult to find.

218
00:14:02,000 --> 00:14:05,680
 You've got good luck at the least if you believe it's all

219
00:14:05,680 --> 00:14:07,000
 luck or whatever.

220
00:14:07,000 --> 00:14:12,000
 But no, we don't believe that.

221
00:14:12,000 --> 00:14:19,000
 Something praiseworthy.

222
00:14:19,000 --> 00:14:21,590
 We've done a difficult thing to meet with the Buddhist

223
00:14:21,590 --> 00:14:25,000
 teaching as a human being.

224
00:14:25,000 --> 00:14:29,450
 And finally we've done a very difficult thing by taking the

225
00:14:29,450 --> 00:14:30,000
 opportunity

226
00:14:30,000 --> 00:14:33,890
 to listen to the Buddhist teaching and study the Buddhist

227
00:14:33,890 --> 00:14:35,000
 teaching.

228
00:14:35,000 --> 00:14:42,000
 I was just today in Calla, the east, north of Toronto.

229
00:14:42,000 --> 00:14:50,000
 They had an ordination and establishing of the boundaries

230
00:14:50,000 --> 00:14:54,000
 for an ordination hall.

231
00:14:54,000 --> 00:15:06,000
 It's quite neat to see.

232
00:15:06,000 --> 00:15:11,300
 It's quite interesting we have these people who have met

233
00:15:11,300 --> 00:15:16,000
 with the Buddhist teaching.

234
00:15:16,000 --> 00:15:20,110
 Sometimes you look and you see people who are even born

235
00:15:20,110 --> 00:15:22,000
 Buddhists.

236
00:15:22,000 --> 00:15:26,000
 Some of them there obviously knew some things,

237
00:15:26,000 --> 00:15:30,400
 but it was also clear that some of the kids there who were

238
00:15:30,400 --> 00:15:32,000
 ordained,

239
00:15:32,000 --> 00:15:35,000
 still hadn't met with the Buddhist teaching,

240
00:15:35,000 --> 00:15:38,000
 still hadn't listened, still hadn't heard,

241
00:15:38,000 --> 00:15:41,360
 and certainly hadn't come to the point where they were

242
00:15:41,360 --> 00:15:43,000
 actually practicing.

243
00:15:43,000 --> 00:15:46,900
 So they had these novices that were sitting during one of

244
00:15:46,900 --> 00:15:48,000
 the ceremonies.

245
00:15:48,000 --> 00:15:52,710
 They were sitting or waiting around and they were sitting

246
00:15:52,710 --> 00:15:55,000
 talking about Zorro,

247
00:15:55,000 --> 00:15:57,000
 someone killing Zorro with a knife.

248
00:15:57,000 --> 00:15:59,660
 I'm still not sure whether it was a game they were talking

249
00:15:59,660 --> 00:16:00,000
 about

250
00:16:00,000 --> 00:16:04,000
 or a movie or some imagination, I don't know.

251
00:16:04,000 --> 00:16:07,000
 But it was certainly quite off topic.

252
00:16:07,000 --> 00:16:09,000
 Killing Zorro with a knife.

253
00:16:09,000 --> 00:16:13,000
 That was how someone they knew killed Zorro with a knife.

254
00:16:13,000 --> 00:16:15,000
 It was a video game of sorts.

255
00:16:15,000 --> 00:16:20,000
 Anyway, these sorts of things and just watching it.

256
00:16:20,000 --> 00:16:23,680
 Even if you meet with the Buddhist teaching with all of

257
00:16:23,680 --> 00:16:24,000
 them,

258
00:16:24,000 --> 00:16:28,200
 in this time the whole world really has the opportunity to

259
00:16:28,200 --> 00:16:29,000
 practice,

260
00:16:29,000 --> 00:16:33,000
 not the whole world, but throughout the world there are

261
00:16:33,000 --> 00:16:35,000
 opportunities to practice.

262
00:16:35,000 --> 00:16:39,470
 The Buddhist teaching and how many people don't ever take

263
00:16:39,470 --> 00:16:40,000
 it.

264
00:16:40,000 --> 00:16:47,000
 Buddha said it's difficult to find teachings.

265
00:16:47,000 --> 00:16:50,000
 Then he said it's difficult for people even to want to.

266
00:16:50,000 --> 00:16:55,000
 He said few are those people who actually want to practice.

267
00:16:55,000 --> 00:16:58,000
 But then he said fewer still.

268
00:16:58,000 --> 00:17:05,000
 Few among those people who actually want to practice.

269
00:17:05,000 --> 00:17:09,000
 Are those whoever get around to practicing?

270
00:17:10,000 --> 00:17:14,000
 So we've done all of this.

271
00:17:14,000 --> 00:17:17,000
 That's something.

272
00:17:17,000 --> 00:17:23,000
 So we shouldn't despair.

273
00:17:23,000 --> 00:17:26,990
 We shouldn't take the difficulty that we encounter in the

274
00:17:26,990 --> 00:17:29,000
 practice too seriously.

275
00:17:29,000 --> 00:17:38,000
 We should try to encourage ourselves in this way.

276
00:17:39,000 --> 00:17:44,000
 We appreciate ourselves and our potential.

277
00:17:44,000 --> 00:17:49,000
 Meditation isn't all that hard.

278
00:17:49,000 --> 00:17:54,680
 It's the most difficult thing we've ever done, but in the

279
00:17:54,680 --> 00:17:58,000
 end it's us that's the problem.

280
00:17:58,000 --> 00:18:04,630
 If we can see that and just get rid of that whole ego thing

281
00:18:04,630 --> 00:18:05,000
,

282
00:18:05,000 --> 00:18:08,000
 then meditation isn't so difficult.

283
00:18:08,000 --> 00:18:11,000
 It actually gets easier the more you practice.

284
00:18:11,000 --> 00:18:16,000
 Easier, more peaceful, happier, all of these things come.

285
00:18:16,000 --> 00:18:26,000
 My teacher said it's like eating sugar cane.

286
00:18:26,000 --> 00:18:31,000
 If you take the whole sugar cane,

287
00:18:31,000 --> 00:18:35,260
 the bottom is where all the sweet is and the top is not so

288
00:18:35,260 --> 00:18:36,000
 sweet.

289
00:18:36,000 --> 00:18:42,000
 So they said that ordinary pleasures and pursuits

290
00:18:42,000 --> 00:18:46,000
 are like eating sugar cane from the bottom up.

291
00:18:46,000 --> 00:18:50,630
 It starts off very sweet, but it gets less and less as you

292
00:18:50,630 --> 00:18:51,000
 eat.

293
00:18:51,000 --> 00:18:55,000
 Dhamma practice is the opposite.

294
00:18:55,000 --> 00:19:02,850
 In the beginning it's not very sweet at all, but as you

295
00:19:02,850 --> 00:19:05,000
 continue to practice,

296
00:19:05,000 --> 00:19:13,000
 it gets sweeter into its sweetness is beyond compare.

297
00:19:13,000 --> 00:19:17,000
 That's the dhamma for tonight.

298
00:19:17,000 --> 00:19:21,000
 Thank you all for tuning in. Have a good night.

