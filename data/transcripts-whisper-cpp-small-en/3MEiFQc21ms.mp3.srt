1
00:00:00,000 --> 00:00:04,000
 How do Buddhists view military service?

2
00:00:04,000 --> 00:00:10,000
 This is interesting because it actually relates to the monk

3
00:00:10,000 --> 00:00:12,000
 question I think or the answer does.

4
00:00:12,000 --> 00:00:16,570
 Because the word military service has come to mean so many

5
00:00:16,570 --> 00:00:18,000
 different things, right?

6
00:00:18,000 --> 00:00:21,340
 You don't have to necessarily kill people to be in the

7
00:00:21,340 --> 00:00:22,000
 military.

8
00:00:22,000 --> 00:00:28,000
 Military can stop floods, for example.

9
00:00:28,000 --> 00:00:31,690
 If the military is put to use stopping floods, that's a

10
00:00:31,690 --> 00:00:33,000
 great thing.

11
00:00:33,000 --> 00:00:36,720
 You took away all the weapons from the military, took away

12
00:00:36,720 --> 00:00:48,000
 all of their weapons and gave them shovels and sandbags.

13
00:00:48,000 --> 00:00:51,000
 Then I think it would be a good thing.

14
00:00:51,000 --> 00:00:59,000
 You have a standing army that looks after people's welfare.

15
00:00:59,000 --> 00:01:07,900
 But no, obviously the military is for the purpose of doing

16
00:01:07,900 --> 00:01:12,000
 battle, causing harm,

17
00:01:12,000 --> 00:01:18,690
 hurting other beings, maybe in defense, but a person who

18
00:01:18,690 --> 00:01:25,000
 enters the military is doing it with the understanding

19
00:01:25,000 --> 00:01:32,720
 and the intention that they are going to bring violence to

20
00:01:32,720 --> 00:01:34,000
 other beings.

21
00:01:34,000 --> 00:01:41,980
 And as I said, they can think of it as in defense, but it's

22
00:01:41,980 --> 00:01:48,000
 a violent profession by its very nature.

23
00:01:48,000 --> 00:01:50,810
 I don't know. I guess my question would be, in what way

24
00:01:50,810 --> 00:01:58,280
 would you think that military service would be a positive

25
00:01:58,280 --> 00:01:59,000
 thing?

26
00:01:59,000 --> 00:02:09,080
 Or is there any way that you can think of it as not being a

27
00:02:09,080 --> 00:02:15,000
 negative thing, I suppose?

28
00:02:15,000 --> 00:02:19,050
 Okay. My job would be to help children of foreign countries

29
00:02:19,050 --> 00:02:20,000
 not hate Americans.

30
00:02:20,000 --> 00:02:23,000
 But the question is, if there were a war, would you be

31
00:02:23,000 --> 00:02:25,000
 asked to pick up a gun and fight?

32
00:02:25,000 --> 00:02:29,100
 I don't really know how to... That's what I said. The

33
00:02:29,100 --> 00:02:31,400
 military has become something quite a bit more than just

34
00:02:31,400 --> 00:02:32,000
 guys with guns.

35
00:02:32,000 --> 00:02:42,260
 But I still think you're given a duty to fight, are you not

36
00:02:42,260 --> 00:02:43,000
?

37
00:02:43,000 --> 00:02:47,880
 There are many issues here because I know that being in the

38
00:02:47,880 --> 00:02:54,110
 American military, for example, can be a great way,

39
00:02:54,110 --> 00:02:56,000
 theoretically, to help people.

40
00:02:56,000 --> 00:03:03,160
 You're given a salary, you're given a lot of equipment and

41
00:03:03,160 --> 00:03:05,000
 resources.

42
00:03:05,000 --> 00:03:08,590
 And yeah, joining other organizations might be a great

43
00:03:08,590 --> 00:03:12,320
 thing, but there's something to be said about the clout of

44
00:03:12,320 --> 00:03:15,000
 the American military in that sense.

45
00:03:15,000 --> 00:03:19,050
 The problem is that, I don't understand it fully, but as I

46
00:03:19,050 --> 00:03:24,000
 understand, when you join the military...

47
00:03:24,000 --> 00:03:28,070
 No, I guess that's not true. If you're in intelligence and

48
00:03:28,070 --> 00:03:32,000
 in the Air Force, you're not involved with killing.

49
00:03:32,000 --> 00:03:35,000
 More like a desk job, right?

50
00:03:35,000 --> 00:03:38,180
 Well, then I guess there's the moral question of... Is

51
00:03:38,180 --> 00:03:43,110
 there a moral question of joining an organization that, at

52
00:03:43,110 --> 00:03:50,000
 its very nature, is for the purpose of...

53
00:03:50,000 --> 00:03:55,360
 I don't want to say... The purpose is to engage in combat,

54
00:03:55,360 --> 00:04:01,000
 no? Or to...

55
00:04:01,000 --> 00:04:09,000
 Well, it's a military. Are there ethical problems there?

56
00:04:09,000 --> 00:04:13,000
 I guess it's... I don't have strong feelings about this.

57
00:04:13,000 --> 00:04:17,800
 You take your job, and if your job is for the purpose of

58
00:04:17,800 --> 00:04:22,840
 helping other people, and if through your job you can help

59
00:04:22,840 --> 00:04:26,000
 other people, then power to you.

60
00:04:26,000 --> 00:04:31,270
 Even if your job doesn't help other people, I'm not of the

61
00:04:31,270 --> 00:04:36,360
 bent that you have to be concerned with anything larger

62
00:04:36,360 --> 00:04:39,000
 than doing your own task.

63
00:04:39,000 --> 00:04:42,350
 And I know people are, and there's a lot of people who have

64
00:04:42,350 --> 00:04:46,170
 this social conscience, that they feel like they shouldn't

65
00:04:46,170 --> 00:04:48,000
 work for an organization.

66
00:04:48,000 --> 00:04:53,000
 This is the same debate about vegetarianism, right?

67
00:04:53,000 --> 00:04:56,460
 You can't eat meat because you're contributing to the

68
00:04:56,460 --> 00:04:58,000
 killing of animals.

69
00:04:58,000 --> 00:05:03,860
 You can't use Gillette razors because you're contributing

70
00:05:03,860 --> 00:05:07,000
 to the cruelty towards animals.

71
00:05:07,000 --> 00:05:11,160
 You can't buy certain stocks. You can't buy certain

72
00:05:11,160 --> 00:05:16,760
 products. You can't... Don't buy Nike. Don't buy this. Don

73
00:05:16,760 --> 00:05:18,000
't buy that.

74
00:05:18,000 --> 00:05:22,920
 These sort of things. And I don't buy it. It seems the

75
00:05:22,920 --> 00:05:25,000
 Buddha didn't buy it.

76
00:05:25,000 --> 00:05:28,850
 And there's even... We have this wonderful example that we

77
00:05:28,850 --> 00:05:32,000
 always tell this woman whose husband was a hunter.

78
00:05:32,000 --> 00:05:35,780
 When she was young, she went to practice meditation,

79
00:05:35,780 --> 00:05:40,000
 listened to the Buddha's teaching, and became a sotapan.

80
00:05:40,000 --> 00:05:45,000
 Which means she was, in a sense, enlightened.

81
00:05:45,000 --> 00:05:48,700
 And then she got married because in India their marriage

82
00:05:48,700 --> 00:05:52,820
 would have been decided for them, so she was married off to

83
00:05:52,820 --> 00:05:55,000
 this man who was a hunter.

84
00:05:55,000 --> 00:06:00,340
 And every day she would clean his traps, clean the blood

85
00:06:00,340 --> 00:06:04,430
 and the guts and whatever, the hair off of these traps, and

86
00:06:04,430 --> 00:06:07,000
 put them out on the table for him.

87
00:06:07,000 --> 00:06:10,000
 And every day he would go out and kill animals.

88
00:06:10,000 --> 00:06:14,070
 And so they asked, "How is this possible that she can do

89
00:06:14,070 --> 00:06:15,000
 this?"

90
00:06:15,000 --> 00:06:19,390
 And the Buddha said, "It's because..." He said, "It's like

91
00:06:19,390 --> 00:06:21,000
 if your hand has no cut.

92
00:06:21,000 --> 00:06:25,000
 If you have no wound on your hand, you can handle poison.

93
00:06:25,000 --> 00:06:30,580
 But if you have a wound on your hand, you can't handle

94
00:06:30,580 --> 00:06:32,000
 poison."

95
00:06:32,000 --> 00:06:35,000
 He says a little more poetically than that.

96
00:06:35,000 --> 00:06:41,340
 But the point being that when a person has no bad

97
00:06:41,340 --> 00:06:49,590
 intentions, the act of cleaning blood and gore off of these

98
00:06:49,590 --> 00:06:54,910
 killing machines, even to that extent, is not considered to

99
00:06:54,910 --> 00:06:56,000
 be immoral.

100
00:06:56,000 --> 00:06:58,820
 Because it's not. It's the same as eating meat. The act

101
00:06:58,820 --> 00:07:02,000
 itself is never immoral.

102
00:07:02,000 --> 00:07:05,250
 The Buddha actually taught against the idea of karma. He

103
00:07:05,250 --> 00:07:09,440
 said, "Karma is not the important thing. It's the intention

104
00:07:09,440 --> 00:07:10,000
."

105
00:07:10,000 --> 00:07:12,600
 So if the woman is intending, "Oh, here I'm helping my

106
00:07:12,600 --> 00:07:15,900
 husband, making him happy and doing my job. If I don't, he

107
00:07:15,900 --> 00:07:19,000
 might beat me," kind of thing.

108
00:07:19,000 --> 00:07:22,930
 That might have had a part in it, because there's some

109
00:07:22,930 --> 00:07:26,000
 horrible things that go on out there.

110
00:07:26,000 --> 00:07:32,000
 But even as a hunter, he's not the most cultured of beings.

111
00:07:32,000 --> 00:07:38,460
 Even without that, just the idea that she's doing her job

112
00:07:38,460 --> 00:07:41,000
 and doing what is her way.

113
00:07:41,000 --> 00:07:46,260
 For a person who eats meat, they're just eating the food in

114
00:07:46,260 --> 00:07:49,000
 order to gain the benefit.

115
00:07:49,000 --> 00:07:53,890
 This is in line, I think, with my understanding of the

116
00:07:53,890 --> 00:08:03,300
 Buddhist teaching. Because we're a deal in terms of

117
00:08:03,300 --> 00:08:07,000
 experience.

118
00:08:07,000 --> 00:08:10,510
 "Don't some monks practice some military aspects like Sha

119
00:08:10,510 --> 00:08:13,000
olin? I'll never live this one down."

120
00:08:13,000 --> 00:08:17,470
 No, Buddhist monks don't practice Shaolin. Shaolin monks

121
00:08:17,470 --> 00:08:20,870
 practice Shaolin. I'm sorry. They call themselves Buddhists

122
00:08:20,870 --> 00:08:21,000
.

123
00:08:21,000 --> 00:08:26,440
 But in what way does kung fu have to do with the Buddhist

124
00:08:26,440 --> 00:08:28,000
 teaching?

125
00:08:28,000 --> 00:08:33,420
 It's well to each their own. But no, Shaolin monks are a

126
00:08:33,420 --> 00:08:36,000
 very specific type of monk.

127
00:08:36,000 --> 00:08:40,500
 I don't know. I guess I just can't answer that, because it

128
00:08:40,500 --> 00:08:43,000
's so different from what I do.

129
00:08:43,000 --> 00:08:46,000
 I used to practice karate, but that was a long time ago.

130
00:08:46,000 --> 00:08:51,670
 Karate isn't kung fu, but it's not a part of the Buddhist

131
00:08:51,670 --> 00:08:54,000
 teaching for sure.

132
00:08:54,000 --> 00:08:56,810
 Those monks are doing it. I mean, there's a whole history

133
00:08:56,810 --> 00:08:58,000
 there of how it came about,

134
00:08:58,000 --> 00:09:00,510
 because there was war and the monks had to defend

135
00:09:00,510 --> 00:09:02,000
 themselves or whatever.

136
00:09:02,000 --> 00:09:06,450
 I'm not sure how it all came about. Anyway, question on

137
00:09:06,450 --> 00:09:07,000
 joining the military.

138
00:09:07,000 --> 00:09:11,370
 As I said, take it as your job, because the American

139
00:09:11,370 --> 00:09:13,000
 military is such a huge organization

140
00:09:13,000 --> 00:09:17,490
 and so many people aren't involved with the killing part of

141
00:09:17,490 --> 00:09:18,000
 it.

142
00:09:18,000 --> 00:09:24,500
 But you have to be fairly careful there as well, because if

143
00:09:24,500 --> 00:09:28,000
 you are put in a position

144
00:09:28,000 --> 00:09:36,570
 where you have to be involved with or encourage the use of

145
00:09:36,570 --> 00:09:42,000
 force or the distribution of weapons, for example,

146
00:09:42,000 --> 00:09:44,870
 it's certainly something that you have to take into

147
00:09:44,870 --> 00:09:46,000
 consideration.

148
00:09:46,000 --> 00:09:49,360
 Be careful that you don't somehow get conscripted in your

149
00:09:49,360 --> 00:09:54,000
 soldier, go and fight.

150
00:09:54,000 --> 00:09:58,330
 But, you know, we're living in hard times. If you have a

151
00:09:58,330 --> 00:10:00,000
 chance to help people, then power to you.

152
00:10:00,000 --> 00:10:03,230
 Go out there and help. Because actually in Canada, the

153
00:10:03,230 --> 00:10:06,000
 military is mostly involved in peacekeeping,

154
00:10:06,000 --> 00:10:09,370
 helping to bring peace. Or they were, I don't know what it

155
00:10:09,370 --> 00:10:12,000
's like now. Anyway, military.

