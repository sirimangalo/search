1
00:00:00,000 --> 00:00:04,000
 What do you think of modern education?

2
00:00:04,000 --> 00:00:08,000
 Not much.

3
00:00:08,000 --> 00:00:12,000
 I wanted to leave school when I was

4
00:00:12,000 --> 00:00:16,000
 in grade 11, when I was what,

5
00:00:16,000 --> 00:00:20,000
 16 years old.

6
00:00:20,000 --> 00:00:23,350
 My parents did something really neat and that took me out

7
00:00:23,350 --> 00:00:24,000
 of school.

8
00:00:24,000 --> 00:00:28,000
 When I was in kindergarten,

9
00:00:28,000 --> 00:00:32,000
 when I finished kindergarten, I was going to grade 1,

10
00:00:32,000 --> 00:00:36,000
 how old that is,

11
00:00:36,000 --> 00:00:40,000
 6, 7, 5, 6, 6.

12
00:00:40,000 --> 00:00:42,750
 They decided they were going to take me and my older

13
00:00:42,750 --> 00:00:44,000
 brother out of school.

14
00:00:44,000 --> 00:00:48,000
 Then I spent grades 1 to 8

15
00:00:48,000 --> 00:00:52,000
 at home.

16
00:00:52,000 --> 00:00:56,000
 It was pure torture. It was really good actually. We

17
00:00:56,000 --> 00:00:56,000
 learned a lot.

18
00:00:56,000 --> 00:01:00,000
 We were free. We didn't do anything. Every day we had to do

19
00:01:00,000 --> 00:01:00,000
 some math

20
00:01:00,000 --> 00:01:04,000
 and some reading.

21
00:01:04,000 --> 00:01:08,000
 Our parents would buy us all sorts of used books. They'd go

22
00:01:08,000 --> 00:01:08,000
 to the used bookstores and just

23
00:01:08,000 --> 00:01:12,000
 buy boxes of used books. So we got to read a lot.

24
00:01:12,000 --> 00:01:16,000
 As a result, we learned so much. Our minds became sharp

25
00:01:16,000 --> 00:01:20,000
 and all four of us have gone on to do good things.

26
00:01:20,000 --> 00:01:24,000
 I think that's an important answer to your question.

27
00:01:24,000 --> 00:01:28,000
 It shows part of the uselessness that I'm going to come to.

28
00:01:28,000 --> 00:01:32,000
 Because that's really my conclusion.

29
00:01:32,000 --> 00:01:36,000
 It goes back to your Thoreau quote.

30
00:01:36,000 --> 00:01:40,000
 Modern education is really not nearly as useful

31
00:01:40,000 --> 00:01:44,000
 as people think. The most useful thing that modern

32
00:01:44,000 --> 00:01:44,000
 education brings is socialization.

33
00:01:44,000 --> 00:01:48,000
 As a result, I'm a social reject.

34
00:01:48,000 --> 00:01:52,000
 I wear rags for crying out loud.

35
00:01:52,000 --> 00:01:56,000
 I live in the forest and can't get along in society.

36
00:01:56,000 --> 00:02:00,000
 This isn't really what happens because my brothers are fine

37
00:02:00,000 --> 00:02:04,000
 in society. There's a certain element

38
00:02:04,000 --> 00:02:08,000
 of socialization which you could call brainwashing. It's

39
00:02:08,000 --> 00:02:08,000
 kind of sad

40
00:02:08,000 --> 00:02:12,000
 how it affects certain people. It leads us

41
00:02:12,000 --> 00:02:16,000
 to buy into so much that we would otherwise be free from.

42
00:02:16,000 --> 00:02:20,000
 But it doesn't really teach you anything. I can

43
00:02:20,000 --> 00:02:24,000
 verify that because I spent the first eight

44
00:02:24,000 --> 00:02:28,000
 grades learning on my own basically. Our parents bought us

45
00:02:28,000 --> 00:02:32,000
 a computer. We had a computer when I was six.

46
00:02:32,000 --> 00:02:36,000
 They had all this neat software

47
00:02:36,000 --> 00:02:40,000
 and so we learned a lot of stuff from the computer.

48
00:02:40,000 --> 00:02:44,000
 Just by reading, we would read more than anyone.

49
00:02:44,000 --> 00:02:48,000
 Kids don't like to read.

50
00:02:48,000 --> 00:02:51,090
 There's no one likes to read because they weren't brought

51
00:02:51,090 --> 00:02:52,000
 up like that. We weren't brought up reading.

52
00:02:52,000 --> 00:02:56,000
 We'd read, read, read, read so much.

53
00:02:56,000 --> 00:03:00,000
 That's an incredible way of becoming educated.

54
00:03:00,000 --> 00:03:04,000
 It's just taking the example of great writers.

55
00:03:04,000 --> 00:03:08,000
 The great point is that you don't

56
00:03:08,000 --> 00:03:12,000
 get a lot from school. I certainly didn't.

57
00:03:12,000 --> 00:03:16,000
 It became more and more pronounced.

58
00:03:16,000 --> 00:03:20,000
 In grade 11, I just suddenly, without even

59
00:03:20,000 --> 00:03:24,000
 any kind of view, it just suddenly hit me that I don't want

60
00:03:24,000 --> 00:03:24,000
 to be in school.

61
00:03:24,000 --> 00:03:28,000
 It probably tied into the whole idea that

62
00:03:28,000 --> 00:03:32,000
 I had that many people have that

63
00:03:32,000 --> 00:03:36,000
 "Well, what is school for?"

64
00:03:36,000 --> 00:03:40,000
 Maybe I read it somewhere because it just seemed like it

65
00:03:40,000 --> 00:03:40,000
 was a trap.

66
00:03:40,000 --> 00:03:44,000
 You go to school to get educated

67
00:03:44,000 --> 00:03:48,000
 and the education is only for the purposes of getting a job

68
00:03:48,000 --> 00:03:48,000
.

69
00:03:48,000 --> 00:03:52,000
 What are you getting a job for? To make money.

70
00:03:52,000 --> 00:03:55,290
 You spend all this time making money and then you have to

71
00:03:55,290 --> 00:03:56,000
 get a house and a car

72
00:03:56,000 --> 00:04:00,000
 and whatever, a family. By the time you have money and you

73
00:04:00,000 --> 00:04:00,000
 have status

74
00:04:00,000 --> 00:04:04,000
 and so on, you're too old to enjoy it. It's time to get old

75
00:04:04,000 --> 00:04:04,000
.

76
00:04:04,000 --> 00:04:08,000
 The point being that happiness only is supposed to come

77
00:04:08,000 --> 00:04:08,000
 when you retire.

78
00:04:08,000 --> 00:04:12,000
 By the time you retire, you're too old to enjoy it anyway

79
00:04:12,000 --> 00:04:12,000
 and you're just

80
00:04:12,000 --> 00:04:16,000
 going to be about sickness, old age and death.

81
00:04:16,000 --> 00:04:20,000
 That was really what was going through my mind at the time.

82
00:04:20,000 --> 00:04:24,000
 That, and I think I read the Tao Te Ching around that time

83
00:04:24,000 --> 00:04:24,000
 and

84
00:04:24,000 --> 00:04:28,000
 became a Taoist. I wanted to go to meet the Dalai Lama and

85
00:04:28,000 --> 00:04:28,000
 become a monk.

86
00:04:28,000 --> 00:04:32,000
 That's sort of an answer

87
00:04:32,000 --> 00:04:36,000
 as to what I think of education. It's basically that

88
00:04:36,000 --> 00:04:40,000
 it's useless and it's just for the purposes of perpetuating

89
00:04:40,000 --> 00:04:44,000
 uselessness.

90
00:04:44,000 --> 00:04:48,000
 That's a hard thing for people to swallow because it's so

91
00:04:48,000 --> 00:04:48,000
 much

92
00:04:48,000 --> 00:04:52,000
 a part of, becomes so much a part of reality for us.

93
00:04:52,000 --> 00:04:56,000
 School. School is what kids do. Everyone goes to school.

94
00:04:56,000 --> 00:05:00,000
 You not go to school. School is useless.

95
00:05:00,000 --> 00:05:04,000
 Well, do monkeys go to school?

96
00:05:04,000 --> 00:05:08,000
 What other animal goes to school? What do we get out of

97
00:05:08,000 --> 00:05:08,000
 school?

98
00:05:08,000 --> 00:05:12,000
 We get society. We get

99
00:05:12,000 --> 00:05:16,000
 economics. We get iPads.

100
00:05:16,000 --> 00:05:20,000
 iPods. iPhones.

101
00:05:20,000 --> 00:05:24,000
 We get "I", no? A lot of "I".

102
00:05:24,000 --> 00:05:28,000
 Look at what our education has brought us.

103
00:05:28,000 --> 00:05:31,870
 The sky is polluted, the water is polluted, the trees are

104
00:05:31,870 --> 00:05:32,000
 gone,

105
00:05:32,000 --> 00:05:36,000
 the world's heating up.

106
00:05:36,000 --> 00:05:40,000
 It hasn't brought us a lot of good. I'm not saying things

107
00:05:40,000 --> 00:05:40,000
 like learning

108
00:05:40,000 --> 00:05:44,000
 and wisdom are not good, but they can be gained in such

109
00:05:44,000 --> 00:05:46,550
 better ways. I mean, being a monk, that's one of the

110
00:05:46,550 --> 00:05:48,000
 greatest things about being a monk.

111
00:05:48,000 --> 00:05:52,000
 See, what happened is I went to university thinking, well,

112
00:05:52,000 --> 00:05:52,000
 maybe

113
00:05:52,000 --> 00:05:54,940
 university will be different. Spent half a year and did a

114
00:05:54,940 --> 00:05:56,000
 lot of stupid things.

115
00:05:56,000 --> 00:06:00,000
 Got involved with, well, did some drugs and

116
00:06:00,000 --> 00:06:04,000
 like four different women I was interested in.

117
00:06:04,000 --> 00:06:08,000
 Got on all these committees and became involved. So many,

118
00:06:08,000 --> 00:06:08,000
 you just went everywhere.

119
00:06:08,000 --> 00:06:12,000
 And realized that it wasn't any different. There was, no

120
00:06:12,000 --> 00:06:12,000
 matter what

121
00:06:12,000 --> 00:06:16,000
 I did and which way I went, it was all meaningless.

122
00:06:16,000 --> 00:06:20,000
 So I vowed to myself that I'm going to learn, I'm going to

123
00:06:20,000 --> 00:06:20,000
 find a way to learn without

124
00:06:20,000 --> 00:06:24,000
 university. Because university wasn't

125
00:06:24,000 --> 00:06:27,370
 learning for me. I actually made a big fuss about it before

126
00:06:27,370 --> 00:06:28,000
 I left.

127
00:06:28,000 --> 00:06:32,000
 I got all our fellow students in our program together. It

128
00:06:32,000 --> 00:06:32,000
 was a special program for people who

129
00:06:32,000 --> 00:06:36,000
 want to learn. You needed like a 92 average to get into the

130
00:06:36,000 --> 00:06:36,000
 program.

131
00:06:36,000 --> 00:06:40,000
 And all these, you had to have all these extracurriculars

132
00:06:40,000 --> 00:06:40,000
 and so on.

133
00:06:40,000 --> 00:06:44,000
 So I thought, wow, this is, wow, to get into this program

134
00:06:44,000 --> 00:06:48,000
 is a good one. So I made a big fuss when I left.

135
00:06:48,000 --> 00:06:52,000
 I got everyone together and we started talking about what

136
00:06:52,000 --> 00:06:52,000
 was wrong

137
00:06:52,000 --> 00:06:56,000
 with the program and how there wasn't real learning. It was

138
00:06:56,000 --> 00:06:56,000
 just rehashing

139
00:06:56,000 --> 00:07:00,000
 old stuff. Whatever. And so, anyway,

140
00:07:00,000 --> 00:07:03,630
 I dropped out with the, the point is I dropped out with the

141
00:07:03,630 --> 00:07:04,000
 idea of doing some real

142
00:07:04,000 --> 00:07:08,000
 learning. And so the point I want to make is that

143
00:07:08,000 --> 00:07:12,000
 I found that now. Being a monk is the ideal

144
00:07:12,000 --> 00:07:16,000
 in every sense of the word, the ideal opportunity

145
00:07:16,000 --> 00:07:20,000
 to learn. Maniratana is going to university

146
00:07:20,000 --> 00:07:24,000
 and learning, is able to learn everything about

147
00:07:24,000 --> 00:07:28,000
 what is proper to learn. I mean,

148
00:07:28,000 --> 00:07:32,000
 the Buddha's teaching itself is a profound body,

149
00:07:32,000 --> 00:07:36,000
 a vast body of knowledge. So simply learning

150
00:07:36,000 --> 00:07:40,000
 the truth and learning good things and learning about what

151
00:07:40,000 --> 00:07:40,000
 is right

152
00:07:40,000 --> 00:07:44,000
 and what is wrong and learning profound philosophy.

153
00:07:44,000 --> 00:07:48,000
 I have the ideal opportunity to

154
00:07:48,000 --> 00:07:52,000
 do that here. But of course there's a deeper meaning to

155
00:07:52,000 --> 00:07:52,000
 that

156
00:07:52,000 --> 00:07:56,000
 is that now I have the opportunity to do the real learning.

157
00:07:56,000 --> 00:07:56,000
 I've learned what

158
00:07:56,000 --> 00:08:00,000
 it is that is necessary to learn and that of course is

159
00:08:00,000 --> 00:08:00,000
 learning

160
00:08:00,000 --> 00:08:04,000
 about oneself. You could extrapolate on that and say

161
00:08:04,000 --> 00:08:08,000
 there's so many incredible things that you can learn. I

162
00:08:08,000 --> 00:08:08,000
 mean,

163
00:08:08,000 --> 00:08:10,950
 what other school gives you the opportunity to learn how to

164
00:08:10,950 --> 00:08:12,000
 fly through the air?

165
00:08:12,000 --> 00:08:16,000
 Read people's minds. See heaven, see hell.

166
00:08:16,000 --> 00:08:20,000
 Remember your past lives. What

167
00:08:20,000 --> 00:08:24,000
 university has a curriculum?

168
00:08:24,000 --> 00:08:28,000
 Textbooks designed to teach people how to remember their

169
00:08:28,000 --> 00:08:28,000
 past

170
00:08:28,000 --> 00:08:32,000
 lives. We have that. If you want to

171
00:08:32,000 --> 00:08:36,000
 remember your past lives, we've got a course for you. I don

172
00:08:36,000 --> 00:08:36,000
't teach it,

173
00:08:36,000 --> 00:08:40,000
 but I teach a different subject.

174
00:08:40,000 --> 00:08:44,000
 Because finally, if you want to be free from greed, anger,

175
00:08:44,000 --> 00:08:44,000
 and delusion

176
00:08:44,000 --> 00:08:48,000
 and have none of these left to have no greed, no anger, and

177
00:08:48,000 --> 00:08:48,000
 delusion

178
00:08:48,000 --> 00:08:52,000
 in your mind ever and no possibility of it

179
00:08:52,000 --> 00:08:56,000
 ever to come back. We've got a course for that. We've got

180
00:08:56,000 --> 00:08:56,000
 curriculum and we've got textbooks.

181
00:08:56,000 --> 00:09:00,000
 And we've got teachers.

182
00:09:00,000 --> 00:09:04,000
 School of Xavier teaches that.

183
00:09:04,000 --> 00:09:08,000
 I don't know what that is.

184
00:09:08,000 --> 00:09:12,000
 Sorry?

185
00:09:12,000 --> 00:09:16,000
 No.

