1
00:00:00,000 --> 00:00:18,320
 Good evening everyone.

2
00:00:18,320 --> 00:00:31,000
 Live May 1st 2016.

3
00:00:31,000 --> 00:00:36,800
 Today's quote is from the Anguttranikaya again.

4
00:00:36,800 --> 00:00:40,880
 This time the book of fours.

5
00:00:40,880 --> 00:00:48,880
 Again the quote only singles out part of the sutta.

6
00:00:48,880 --> 00:00:51,400
 Again this isn't the words of the Buddha.

7
00:00:51,400 --> 00:01:01,120
 This is the words of Ananda.

8
00:01:01,120 --> 00:01:09,150
 Ananda was the attendant and cousin of the Buddha and he

9
00:01:09,150 --> 00:01:12,320
 spent much of the Buddha's

10
00:01:12,320 --> 00:01:17,270
 later life caring for him and following him around and

11
00:01:17,270 --> 00:01:20,160
 listening to him and gathering

12
00:01:20,160 --> 00:01:23,880
 and remembering the Buddha's teachings.

13
00:01:23,880 --> 00:01:32,160
 Had a very good memory it seems.

14
00:01:32,160 --> 00:01:36,960
 So unsurprising that people would come to him for the dham

15
00:01:36,960 --> 00:01:39,280
ma perhaps after the Buddha

16
00:01:39,280 --> 00:01:46,710
 passed away because Ananda is one of the great monks who

17
00:01:46,710 --> 00:01:50,040
 outlived the Buddha.

18
00:01:50,040 --> 00:01:53,460
 Sariputta and Mughalana, the Buddha's two chief disciples,

19
00:01:53,460 --> 00:01:55,200
 passed away before the Buddha

20
00:01:55,200 --> 00:01:56,200
 did.

21
00:01:56,200 --> 00:01:58,200
 But Ananda lived on.

22
00:01:58,200 --> 00:02:07,160
 Ananda lived to 120 so the legend goes.

23
00:02:07,160 --> 00:02:17,280
 Before he died he knew that both sides of his family were

24
00:02:17,280 --> 00:02:22,840
 going to fight over his relics

25
00:02:22,840 --> 00:02:26,590
 so if he died when he was sick and dying he went to stay

26
00:02:26,590 --> 00:02:29,040
 with his family but both families

27
00:02:29,040 --> 00:02:32,160
 wanted him to stay with them.

28
00:02:32,160 --> 00:02:38,280
 I'm maybe just making this up but the end result is he flew

29
00:02:38,280 --> 00:02:41,960
 up into the air and spontaneously

30
00:02:41,960 --> 00:02:49,050
 combusted and made a determination so that half of his

31
00:02:49,050 --> 00:02:52,880
 bones went on either side of the

32
00:02:52,880 --> 00:02:53,880
 fence.

33
00:02:53,880 --> 00:02:55,680
 Something like that.

34
00:02:55,680 --> 00:02:56,680
 The river maybe.

35
00:02:56,680 --> 00:02:59,680
 I think it was a river.

36
00:02:59,680 --> 00:03:04,680
 There's a lot of stories like this.

37
00:03:04,680 --> 00:03:08,960
 Ananda's a special monk.

38
00:03:08,960 --> 00:03:15,610
 The idea we get about him is he was very compassionate and

39
00:03:15,610 --> 00:03:20,000
 interested in helping people who maybe

40
00:03:20,000 --> 00:03:31,200
 had a hard time.

41
00:03:31,200 --> 00:03:37,140
 He was moved by compassion to speak up for the bhikkhunis

42
00:03:37,140 --> 00:03:40,680
 even after the Buddha cautioned

43
00:03:40,680 --> 00:03:42,760
 against ordaining women.

44
00:03:42,760 --> 00:03:46,160
 Ananda kind of pushed the Buddha to ordain women.

45
00:03:46,160 --> 00:03:53,350
 Of course that's the whole issue but there is an argument

46
00:03:53,350 --> 00:03:56,800
 to be had for why the Buddha

47
00:03:56,800 --> 00:04:02,240
 is hesitant whether you agree or not.

48
00:04:02,240 --> 00:04:06,680
 The point being that it makes it more difficult.

49
00:04:06,680 --> 00:04:11,180
 You can say well that's no excuse and it really isn't in

50
00:04:11,180 --> 00:04:13,600
 the end but certainly when you're

51
00:04:13,600 --> 00:04:20,000
 trying to be celibate and the majority of those trying to

52
00:04:20,000 --> 00:04:23,480
 be celibate are heterosexual

53
00:04:23,480 --> 00:04:27,800
 men bringing women into the mix makes it more difficult.

54
00:04:27,800 --> 00:04:29,760
 So Buddha was hesitant.

55
00:04:29,760 --> 00:04:34,220
 And also in India women were perhaps sheltered so there's

56
00:04:34,220 --> 00:04:35,400
 arguments.

57
00:04:35,400 --> 00:04:44,450
 But Ananda was not having any of it and really kind of

58
00:04:44,450 --> 00:04:50,040
 convinced the Buddha to ask.

59
00:04:50,040 --> 00:04:52,880
 And he taught lay men, lay women.

60
00:04:52,880 --> 00:04:54,760
 He was big on teaching people.

61
00:04:54,760 --> 00:04:57,080
 He was really helpful towards women especially.

62
00:04:57,080 --> 00:05:02,440
 It actually got him in some trouble sometimes because women

63
00:05:02,440 --> 00:05:04,880
 would take fancy to him.

64
00:05:04,880 --> 00:05:09,400
 But he was never moved by that.

65
00:05:09,400 --> 00:05:16,140
 He even claimed that he never had any sexual desire for a

66
00:05:16,140 --> 00:05:18,400
 woman as a monk.

67
00:05:18,400 --> 00:05:25,200
 Anyway, all this is beside the point.

68
00:05:25,200 --> 00:05:27,000
 There are these four Anandas.

69
00:05:27,000 --> 00:05:31,800
 So they came to Ananda and Ananda just taught them these

70
00:05:31,800 --> 00:05:34,640
 four what are called the Pari-Sundhi

71
00:05:34,640 --> 00:05:38,800
 Padani Angan.

72
00:05:38,800 --> 00:05:42,000
 Pari-Sundhi means purity.

73
00:05:42,000 --> 00:05:49,240
 Padania means something you should strive for.

74
00:05:49,240 --> 00:05:54,840
 Angan is just members or things.

75
00:05:54,840 --> 00:05:59,660
 So there are these four things regarding which you should

76
00:05:59,660 --> 00:06:02,080
 strive for the purity of, for the

77
00:06:02,080 --> 00:06:03,720
 purification of.

78
00:06:03,720 --> 00:06:08,720
 So four things you should try to purify.

79
00:06:08,720 --> 00:06:10,720
 Should strive to purify.

80
00:06:10,720 --> 00:06:16,560
 Katamani, Tatarri, what are these four?

81
00:06:16,560 --> 00:06:22,880
 Number one, Seelapari-Sundhi Padani Angan.

82
00:06:22,880 --> 00:06:31,080
 The striving for the purification of virtue.

83
00:06:31,080 --> 00:06:36,280
 And that's where the quote comes from.

84
00:06:36,280 --> 00:06:45,200
 Number two, Chittapari-Sundhi Padani Angan.

85
00:06:45,200 --> 00:06:53,240
 Striving for the purification of mind.

86
00:06:53,240 --> 00:07:01,040
 Number three, Dittipari-Sundhi Padani Angan.

87
00:07:01,040 --> 00:07:06,360
 Striving for the purification of view.

88
00:07:06,360 --> 00:07:11,760
 And number four, Wimuttipari-Sundhi Padani Angan.

89
00:07:11,760 --> 00:07:20,680
 Striving for the purification of release, freedom.

90
00:07:20,680 --> 00:07:27,400
 So these four we should strive to purify, strive to

91
00:07:27,400 --> 00:07:29,200
 complete.

92
00:07:29,200 --> 00:07:34,580
 So the first one that the quote talks about is purification

93
00:07:34,580 --> 00:07:35,840
 of virtue.

94
00:07:35,840 --> 00:07:41,480
 Seelapari-Sundhi Padani Angan.

95
00:07:41,480 --> 00:07:48,080
 And we talked a little bit about this last night.

96
00:07:48,080 --> 00:07:53,680
 The precepts and rules and keeping rules and in general

97
00:07:53,680 --> 00:07:56,960
 rules, if you have the right rules,

98
00:07:56,960 --> 00:08:01,160
 they tend to be a good guide for your behavior.

99
00:08:01,160 --> 00:08:02,600
 And so we purify them.

100
00:08:02,600 --> 00:08:06,450
 We look and see which rules were breaking, which rules we

101
00:08:06,450 --> 00:08:08,720
 tend to break, and then we

102
00:08:08,720 --> 00:08:13,680
 work to stop breaking those rules.

103
00:08:13,680 --> 00:08:20,350
 What is impure in regards to, or unfulfilled in regards to

104
00:08:20,350 --> 00:08:24,520
 our Paripurangwa, Paripurisam.

105
00:08:24,520 --> 00:08:31,160
 I will fulfill those virtues or those rules, specifically

106
00:08:31,160 --> 00:08:35,040
 dealing with rules, sikapada,

107
00:08:35,040 --> 00:08:38,560
 sikapada, that are not fulfilled.

108
00:08:38,560 --> 00:08:40,560
 I will strive to fulfill them.

109
00:08:40,560 --> 00:08:44,020
 So some people keep the five precepts and then they don't

110
00:08:44,020 --> 00:08:45,440
 keep the eight precepts and

111
00:08:45,440 --> 00:08:48,880
 then they strive to keep the eight precepts.

112
00:08:48,880 --> 00:08:52,440
 Some people keep the eight precepts and then they strive to

113
00:08:52,440 --> 00:08:54,360
 keep the ten precepts and then

114
00:08:54,360 --> 00:08:57,930
 people keep the ten precepts and then they strive to become

115
00:08:57,930 --> 00:08:59,480
 a monk and keep many more

116
00:08:59,480 --> 00:09:00,480
 precepts.

117
00:09:00,480 --> 00:09:03,650
 And then you have the precepts and then you break them and

118
00:09:03,650 --> 00:09:05,240
 then you strive not to break

119
00:09:05,240 --> 00:09:06,240
 them.

120
00:09:06,240 --> 00:09:10,400
 And then you work to purify them.

121
00:09:10,400 --> 00:09:15,930
 And then there's this curious statement, "Those that are

122
00:09:15,930 --> 00:09:24,160
 fulfilled, tatatatatpanyaya anugahisam."

123
00:09:24,160 --> 00:09:35,720
 I will support with wisdom.

124
00:09:35,720 --> 00:09:40,520
 Support those that are fulfilled with wisdom.

125
00:09:40,520 --> 00:09:48,240
 Tatatata, here or there or again and again or where

126
00:09:48,240 --> 00:09:50,920
 appropriate.

127
00:09:50,920 --> 00:09:55,940
 So the commentary doesn't give much explanation of what

128
00:09:55,940 --> 00:09:58,720
 this means but I guess it means because

129
00:09:58,720 --> 00:10:03,520
 it's easy to keep the letter of the rule, right?

130
00:10:03,520 --> 00:10:06,400
 And so you can say, "Oh, I'm keeping all these rules fine."

131
00:10:06,400 --> 00:10:10,050
 You can keep the five precepts and still be a bad person

132
00:10:10,050 --> 00:10:12,200
 keeping the five precepts.

133
00:10:12,200 --> 00:10:17,120
 So it's about filling in the gaps and it's about fulfilling

134
00:10:17,120 --> 00:10:19,600
 the purpose of the precepts

135
00:10:19,600 --> 00:10:21,680
 with wisdom.

136
00:10:21,680 --> 00:10:24,530
 So you might not kill but you can still be very angry and

137
00:10:24,530 --> 00:10:25,880
 you can still want to hurt

138
00:10:25,880 --> 00:10:29,040
 or even kill.

139
00:10:29,040 --> 00:10:33,610
 You might not steal but you still covet that of others and

140
00:10:33,610 --> 00:10:35,640
 are jealous and so on.

141
00:10:35,640 --> 00:10:40,760
 You don't cheat but you still feel desire and you don't

142
00:10:40,760 --> 00:10:43,600
 have the understanding of hurting

143
00:10:43,600 --> 00:10:47,260
 others, of what it means to hurt others and what is the

144
00:10:47,260 --> 00:10:49,320
 result of hurting others.

145
00:10:49,320 --> 00:10:52,000
 And you use wisdom to augment that.

146
00:10:52,000 --> 00:10:57,510
 Keeping the precepts, there's this idea that even if it's

147
00:10:57,510 --> 00:11:00,560
 painful, keeping the precepts

148
00:11:00,560 --> 00:11:03,000
 is a good thing.

149
00:11:03,000 --> 00:11:04,000
 People might question that.

150
00:11:04,000 --> 00:11:07,110
 They think, "Well, if whatever you're doing is causing you

151
00:11:07,110 --> 00:11:08,560
 suffering, why would you do

152
00:11:08,560 --> 00:11:09,560
 it?"

153
00:11:09,560 --> 00:11:14,830
 But then we can ask the question, "Suppose there's an

154
00:11:14,830 --> 00:11:17,920
 alcoholic or a drug addict and

155
00:11:17,920 --> 00:11:21,230
 if they stop taking drugs and they go through withdrawal,

156
00:11:21,230 --> 00:11:23,000
 would you say that's a reason

157
00:11:23,000 --> 00:11:24,360
 to go back to taking drugs?"

158
00:11:24,360 --> 00:11:25,360
 Of course not.

159
00:11:25,360 --> 00:11:27,160
 And that's really all this is.

160
00:11:27,160 --> 00:11:30,480
 It's a kind of a withdrawal when you keep precepts, when

161
00:11:30,480 --> 00:11:32,200
 you keep these rules that say

162
00:11:32,200 --> 00:11:37,930
 the eight precepts or so on, it's quite unpleasant for the

163
00:11:37,930 --> 00:11:39,440
 first time.

164
00:11:39,440 --> 00:11:41,600
 This is because you're going through withdrawal.

165
00:11:41,600 --> 00:11:44,880
 It doesn't mean that it's wrong.

166
00:11:44,880 --> 00:11:47,480
 In fact, it's a good sign, a sign that you're actually

167
00:11:47,480 --> 00:11:49,120
 dealing with the problem instead

168
00:11:49,120 --> 00:11:56,440
 of placating it with your addiction.

169
00:11:56,440 --> 00:12:02,880
 So that's the first one, "Sīla Parīsuddhi Madhān yanga."

170
00:12:02,880 --> 00:12:12,550
 Another one is the thing that ought to be purified, which

171
00:12:12,550 --> 00:12:17,280
 we call the mind, the jitta

172
00:12:17,280 --> 00:12:19,880
 parīsuddhi, the mind.

173
00:12:19,880 --> 00:12:20,880
 Or not the mind-mind.

174
00:12:20,880 --> 00:12:25,410
 It's a difficult one to translate because literally it does

175
00:12:25,410 --> 00:12:27,280
 mean the mind or mind.

176
00:12:27,280 --> 00:12:32,470
 It's not exactly that, it's the purification of mind-states

177
00:12:32,470 --> 00:12:34,600
 because it's not the end of

178
00:12:34,600 --> 00:12:35,600
 the line.

179
00:12:35,600 --> 00:12:42,320
 Jitta parīsuddhi is actually just a concentration.

180
00:12:42,320 --> 00:12:46,260
 So your mind is pure, that's great, but it's not your whole

181
00:12:46,260 --> 00:12:46,880
 mind.

182
00:12:46,880 --> 00:12:51,290
 It's not the mind in an abstract sense, it's just a mind in

183
00:12:51,290 --> 00:12:53,600
 an absolute sense, in the sense

184
00:12:53,600 --> 00:12:58,640
 of this mind being pure, this moment of mind.

185
00:12:58,640 --> 00:13:02,730
 So as you practice meditation, after some time, after you

186
00:13:02,730 --> 00:13:04,600
 become proficient, you'll

187
00:13:04,600 --> 00:13:08,170
 find these states arising where you're peaceful, where you

188
00:13:08,170 --> 00:13:10,280
're pure, where your mind just feels

189
00:13:10,280 --> 00:13:16,400
 completely free from attachment or craving or aversion.

190
00:13:16,400 --> 00:13:22,280
 Your mind will be in a clear state, in a pure state.

191
00:13:22,280 --> 00:13:25,840
 We talk about the four jhanas, and they're just

192
00:13:25,840 --> 00:13:28,120
 explanations about these sorts of states

193
00:13:28,120 --> 00:13:32,580
 of mind where you don't have any liking or disliking or d

194
00:13:32,580 --> 00:13:36,040
rowsiness or distraction or

195
00:13:36,040 --> 00:13:37,040
 doubt.

196
00:13:37,040 --> 00:13:46,440
 No judgment, no reaction, just purity of mind.

197
00:13:46,440 --> 00:13:49,210
 That's quite useful because it allows you to see clearly if

198
00:13:49,210 --> 00:13:50,960
 you then focus that on reality,

199
00:13:50,960 --> 00:13:54,360
 you'll be able to see things in ways that you weren't able

200
00:13:54,360 --> 00:13:55,840
 to see things when you were

201
00:13:55,840 --> 00:14:00,800
 judging them, when you were reacting to them.

202
00:14:00,800 --> 00:14:08,920
 So that's really what the meditation is all about.

203
00:14:08,920 --> 00:14:13,130
 Meditation is getting yourself in the frame of mind so that

204
00:14:13,130 --> 00:14:15,280
 you can accomplish the third

205
00:14:15,280 --> 00:14:19,950
 one which is purification of view, views and opinions,

206
00:14:19,950 --> 00:14:23,000
 clearing yourselves up, clearing

207
00:14:23,000 --> 00:14:31,290
 yourself up, your mind up in regards to your beliefs, your

208
00:14:31,290 --> 00:14:35,720
 opinions, the things you hold

209
00:14:35,720 --> 00:14:40,920
 to be true.

210
00:14:40,920 --> 00:14:44,080
 This is really the heart of the matter because it's not

211
00:14:44,080 --> 00:14:46,040
 about calming yourself down, it's

212
00:14:46,040 --> 00:14:52,390
 about rightening yourself, purifying the source because the

213
00:14:52,390 --> 00:14:55,160
 source of our mind state, the

214
00:14:55,160 --> 00:14:59,230
 source which determines whether we're going to have a

215
00:14:59,230 --> 00:15:01,760
 wholesome mind or an unwholesome

216
00:15:01,760 --> 00:15:08,360
 mind, a pure mind or an impure mind is our view, our

217
00:15:08,360 --> 00:15:10,040
 outlook.

218
00:15:10,040 --> 00:15:12,940
 So if you have the view that certain things are worth att

219
00:15:12,940 --> 00:15:14,840
aining, you'll strive after them.

220
00:15:14,840 --> 00:15:18,450
 If you have this view that certain things are worth

221
00:15:18,450 --> 00:15:20,960
 avoiding, you'll avoid them.

222
00:15:20,960 --> 00:15:25,640
 If you have the view that it's right to good to be angry

223
00:15:25,640 --> 00:15:28,440
 and greedy and deluded, then you'll

224
00:15:28,440 --> 00:15:32,130
 do things that give rise to those states and you'll give

225
00:15:32,130 --> 00:15:33,840
 rise to those things.

226
00:15:33,840 --> 00:15:41,400
 If you have views that greed, anger and delusion are wrong

227
00:15:41,400 --> 00:15:45,680
 or you see them as being a problem

228
00:15:45,680 --> 00:15:48,720
 then you're less likely to give rise to those emotions.

229
00:15:48,720 --> 00:15:52,680
 So it's about purifying your view and in fact it's not

230
00:15:52,680 --> 00:15:55,360
 about attaining any one view or any

231
00:15:55,360 --> 00:15:59,950
 outlook, it's not about acquiring an outlook, it's about

232
00:15:59,950 --> 00:16:02,760
 acquiring an understanding of things

233
00:16:02,760 --> 00:16:03,760
 as they are.

234
00:16:03,760 --> 00:16:08,010
 So it's not really a view in the end except in the literal

235
00:16:08,010 --> 00:16:10,160
 sense that you're able to view

236
00:16:10,160 --> 00:16:12,480
 things as they are.

237
00:16:12,480 --> 00:16:15,040
 Your view is in line with reality.

238
00:16:15,040 --> 00:16:16,280
 That's really all we're aiming for.

239
00:16:16,280 --> 00:16:19,640
 We're not aiming to claim anything.

240
00:16:19,640 --> 00:16:23,500
 The only claim we make is that if you look, you'll see

241
00:16:23,500 --> 00:16:25,660
 things as they are and once you

242
00:16:25,660 --> 00:16:29,480
 see things as they are, you will agree with us that they

243
00:16:29,480 --> 00:16:31,600
 are the sway, that we can all

244
00:16:31,600 --> 00:16:35,360
 come to an agreement on the way things are.

245
00:16:35,360 --> 00:16:39,940
 A big part of this, as I've mentioned before, is a paradigm

246
00:16:39,940 --> 00:16:42,080
 shift and it's what I tried

247
00:16:42,080 --> 00:16:45,680
 to explain in the second volume on how to meditate and I

248
00:16:45,680 --> 00:16:47,760
 think the first chapter, one

249
00:16:47,760 --> 00:16:53,330
 of the first chapters, is that we tend to look at reality

250
00:16:53,330 --> 00:16:56,160
 in terms of the world around

251
00:16:56,160 --> 00:16:57,160
 us.

252
00:16:57,160 --> 00:16:59,840
 Obviously, I mean who doesn't, right?

253
00:16:59,840 --> 00:17:03,420
 If you ask what is real, well this room, we're sitting in

254
00:17:03,420 --> 00:17:05,560
 this room, but in fact that's really

255
00:17:05,560 --> 00:17:08,480
 just a projection.

256
00:17:08,480 --> 00:17:13,080
 All of that is still dependent upon something more basic

257
00:17:13,080 --> 00:17:16,240
 and that's where we shift our focus.

258
00:17:16,240 --> 00:17:19,440
 So instead of looking at the world in terms of the room

259
00:17:19,440 --> 00:17:21,240
 around us, we look at it in terms

260
00:17:21,240 --> 00:17:26,820
 of our perception of what then becomes the room around us

261
00:17:26,820 --> 00:17:28,840
 and so experience.

262
00:17:28,840 --> 00:17:34,360
 We base reality on experience and that's the beginning of

263
00:17:34,360 --> 00:17:38,680
 right view because without that,

264
00:17:38,680 --> 00:17:43,470
 there's no way we can be sure that we're all going to come

265
00:17:43,470 --> 00:17:46,440
 to the same conclusion because

266
00:17:46,440 --> 00:17:48,020
 the room here is abstract.

267
00:17:48,020 --> 00:17:52,240
 Some people think it's nice and don't like it.

268
00:17:52,240 --> 00:17:54,520
 Our perception of the room can be quite different.

269
00:17:54,520 --> 00:17:57,330
 Some people looking at it from one direction, some people

270
00:17:57,330 --> 00:17:58,200
 from another.

271
00:17:58,200 --> 00:18:04,720
 Experience is what leads to, in the world it leads to so

272
00:18:04,720 --> 00:18:08,400
 much conflict and confusion and

273
00:18:08,400 --> 00:18:12,560
 misunderstanding and so on.

274
00:18:12,560 --> 00:18:15,320
 But you can't have that when you look at experience.

275
00:18:15,320 --> 00:18:18,800
 Experience is more basic and it doesn't change based on

276
00:18:18,800 --> 00:18:20,360
 your perception of it.

277
00:18:20,360 --> 00:18:21,920
 It is what it is.

278
00:18:21,920 --> 00:18:29,440
 You might want it to be otherwise but it is what is real.

279
00:18:29,440 --> 00:18:35,120
 So that's what comes and goes and changes.

280
00:18:35,120 --> 00:18:42,080
 That's dithi parisuddhi.

281
00:18:42,080 --> 00:18:45,640
 And number four, wimutti parisuddhi is purification of

282
00:18:45,640 --> 00:18:48,360
 freedom or purification through freedom

283
00:18:48,360 --> 00:18:49,360
 maybe.

284
00:18:49,360 --> 00:18:51,360
 It's the fourth.

285
00:18:51,360 --> 00:18:54,120
 Once you have right view, right view is for what?

286
00:18:54,120 --> 00:18:56,080
 Right view is for freedom.

287
00:18:56,080 --> 00:19:00,480
 We want to see things as they are so we can free ourselves

288
00:19:00,480 --> 00:19:02,680
 from the prison that we've

289
00:19:02,680 --> 00:19:07,580
 placed ourselves in, this prison of desire and aversion and

290
00:19:07,580 --> 00:19:08,680
 delusion.

291
00:19:08,680 --> 00:19:11,120
 We trap ourselves.

292
00:19:11,120 --> 00:19:16,030
 We bind ourselves to certain the way we think things should

293
00:19:16,030 --> 00:19:18,520
 be and the way we think things

294
00:19:18,520 --> 00:19:24,200
 shouldn't be by the things we think I am and the things I

295
00:19:24,200 --> 00:19:27,120
 am not, the things I want to

296
00:19:27,120 --> 00:19:38,000
 be, the things I don't want to be, that kind of thing.

297
00:19:38,000 --> 00:19:44,760
 And so this fourth one, Ananda says, with the other three,

298
00:19:44,760 --> 00:19:48,360
 with purification of seela,

299
00:19:48,360 --> 00:20:00,480
 jitta and dithi, one rajaniye su dhamme su jitangvi rajiti,

300
00:20:00,480 --> 00:20:02,520
 one becomes dispassionate

301
00:20:02,520 --> 00:20:08,030
 about dhammas, about things that one might be passionate

302
00:20:08,030 --> 00:20:09,000
 about.

303
00:20:09,000 --> 00:20:13,160
 Wimochaniye su dhamme su jitangvi moti iti, one frees

304
00:20:13,160 --> 00:20:15,920
 oneself from dhammas, from things

305
00:20:15,920 --> 00:20:19,600
 that one should free oneself from.

306
00:20:19,600 --> 00:20:24,920
 And having done so, samawimutti pussati, one touches or att

307
00:20:24,920 --> 00:20:28,120
ains right release, right freedom.

308
00:20:28,120 --> 00:20:35,880
 Ayam ucchati wimutti parisuddhi, this is called pur

309
00:20:35,880 --> 00:20:42,640
ification through release or unreleased.

310
00:20:42,640 --> 00:20:52,180
 So these are the four parisuddhi padanyan gani that were

311
00:20:52,180 --> 00:20:56,320
 taught by that blessed one who

312
00:20:56,320 --> 00:21:04,020
 is nose and seas and enlightened self, self enlightened

313
00:21:04,020 --> 00:21:05,520
 Buddha.

314
00:21:05,520 --> 00:21:09,230
 They were taught for the purification of beings, for the

315
00:21:09,230 --> 00:21:11,760
 overcoming of sorrow, lamentation

316
00:21:11,760 --> 00:21:26,000
 and despair, for the eradication, for the overcoming of the

317
00:21:26,000 --> 00:21:28,560
 destruction of physical

318
00:21:28,560 --> 00:21:34,250
 pain and mental pain, for giving up or freeing oneself, for

319
00:21:34,250 --> 00:21:37,040
 attaining the right path and

320
00:21:37,040 --> 00:21:43,640
 for seeing for oneself, nirvana.

321
00:21:43,640 --> 00:21:46,540
 That's a standard, that's actually what the Buddha taught,

322
00:21:46,540 --> 00:21:48,040
 it's a quote from the Buddha

323
00:21:48,040 --> 00:21:50,040
 from the Satipatthana sutta.

324
00:21:50,040 --> 00:21:55,600
 Satan nang wisuddhi aa sokaparideva nang samatikamaya.

325
00:21:55,600 --> 00:21:58,600
 Dukkha dhaumanasa nang atangamaya.

326
00:21:58,600 --> 00:22:01,600
 Nyaya sa adikamaya.

327
00:22:01,600 --> 00:22:13,200
 Nimvanasah satchikiraya.

328
00:22:13,200 --> 00:22:18,400
 So four good things, another way of looking at the path.

329
00:22:18,400 --> 00:22:21,110
 There's a lot of these where the Buddha would, Anand is

330
00:22:21,110 --> 00:22:23,000
 taking this from the Buddha's teaching,

331
00:22:23,000 --> 00:22:29,760
 this is four things that really condense the path.

332
00:22:29,760 --> 00:22:34,300
 Because when you're teaching, you have to remind people of

333
00:22:34,300 --> 00:22:36,760
 the road map, the direction,

334
00:22:36,760 --> 00:22:38,160
 where are we going?

335
00:22:38,160 --> 00:22:42,040
 And this is a complete road map.

336
00:22:42,040 --> 00:22:45,610
 You start with seela and then with morality you train

337
00:22:45,610 --> 00:22:48,040
 yourself in right behavior and right

338
00:22:48,040 --> 00:22:52,930
 speech to keep yourself ethical, moral, virtuous and then

339
00:22:52,930 --> 00:22:55,600
 you begin to focus the mind and so

340
00:22:55,600 --> 00:22:56,600
 you practice meditation.

341
00:22:56,600 --> 00:23:00,350
 And as you do that, you start to see things clearly, you

342
00:23:00,350 --> 00:23:02,520
 start to see the Four Noble Truths

343
00:23:02,520 --> 00:23:03,520
 in the end.

344
00:23:03,520 --> 00:23:09,850
 You see nama-rupa, you see three characteristics, you see

345
00:23:09,850 --> 00:23:12,560
 the Four Noble Truths.

346
00:23:12,560 --> 00:23:17,360
 And then finally, wimutti, you become free.

347
00:23:17,360 --> 00:23:18,360
 Quite simple.

348
00:23:18,360 --> 00:23:25,650
 It's good to have these guides because it keeps us, reminds

349
00:23:25,650 --> 00:23:28,440
 us of the very straight

350
00:23:28,440 --> 00:23:30,810
 and straightforward nature of the Buddha's teaching, not

351
00:23:30,810 --> 00:23:32,040
 getting caught up in details

352
00:23:32,040 --> 00:23:34,040
 or getting off track.

353
00:23:34,040 --> 00:23:39,840
 It's so easy for us to let our minds allow us to wander and

354
00:23:39,840 --> 00:23:42,760
 get lost on the wrong path,

355
00:23:42,760 --> 00:23:46,160
 the path that leads to stress and suffering.

356
00:23:46,160 --> 00:23:50,320
 It's good for us to remember these things.

357
00:23:50,320 --> 00:23:57,320
 Okay, so that's the dhamma for this evening.

358
00:23:57,320 --> 00:24:20,800
 If you have any questions.

359
00:24:20,800 --> 00:24:23,750
 A guy who couldn't quit talking even when another guy was

360
00:24:23,750 --> 00:24:26,920
 shooting dung into his room.

361
00:24:26,920 --> 00:24:29,920
 That's from the jatakas.

362
00:24:29,920 --> 00:24:35,640
 I love you.

363
00:24:35,640 --> 00:24:40,080
 Which jataka now?

364
00:24:40,080 --> 00:24:43,720
 Are fully enlightened beings amoral?

365
00:24:43,720 --> 00:24:45,720
 Good question.

366
00:24:45,720 --> 00:24:48,720
 No, I wouldn't.

367
00:24:48,720 --> 00:25:00,080
 I guess I wouldn't say that.

368
00:25:00,080 --> 00:25:01,880
 It really depends how you look at it.

369
00:25:01,880 --> 00:25:06,280
 And it's just words, right?

370
00:25:06,280 --> 00:25:17,360
 So some people might want to say that.

371
00:25:17,360 --> 00:25:23,100
 Some people might want to say that they're actually quite

372
00:25:23,100 --> 00:25:24,080
 moral.

373
00:25:24,080 --> 00:25:31,170
 Enlightened being keeps the precepts purely and will not

374
00:25:31,170 --> 00:25:33,640
 break from them.

375
00:25:33,640 --> 00:25:39,900
 But that's really because there's a sense that morality is

376
00:25:39,900 --> 00:25:42,680
 intrinsic to reality.

377
00:25:42,680 --> 00:25:48,600
 Reality is not immoral.

378
00:25:48,600 --> 00:25:53,040
 It's from a dhammapada.

379
00:25:53,040 --> 00:25:55,680
 I don't think the story itself.

380
00:25:55,680 --> 00:25:58,760
 Maybe it is.

381
00:25:58,760 --> 00:26:01,320
 Pretty sure it's from the jatakas though.

382
00:26:01,320 --> 00:26:03,720
 Let me see.

383
00:26:03,720 --> 00:26:08,080
 I'm pretty sure it's a jataka story.

384
00:26:08,080 --> 00:26:11,160
 It may be in the dhammapada as well.

385
00:26:11,160 --> 00:26:13,160
 I may be wrong.

386
00:26:13,160 --> 00:26:17,160
 But I know the story.

387
00:26:17,160 --> 00:26:27,400
 I can't think off the top of my head the exact jataka.

388
00:26:27,400 --> 00:26:31,840
 While the jatakas are on the internet, just read them.

389
00:26:31,840 --> 00:26:35,240
 Read them and tell us which one it is.

390
00:26:35,240 --> 00:26:39,240
 There's only 547 of them.

391
00:26:39,240 --> 00:26:40,240
 547.

392
00:26:40,240 --> 00:26:41,240
 I can probably find them here.

393
00:26:41,240 --> 00:26:42,240
 I have a...

394
00:26:42,240 --> 00:26:43,240
 I have a...

395
00:26:43,240 --> 00:26:44,240
 I have a...

396
00:26:44,240 --> 00:26:45,240
 I have a...

397
00:26:45,240 --> 00:26:46,240
 I have a...

398
00:26:46,240 --> 00:26:47,240
 I have a...

399
00:26:47,240 --> 00:26:48,240
 I have a...

400
00:26:48,240 --> 00:26:49,240
 I have a...

401
00:26:49,240 --> 00:26:50,240
 I have a...

402
00:26:50,240 --> 00:26:51,240
 I have a...

403
00:26:51,240 --> 00:26:52,240
 I have a...

404
00:26:52,240 --> 00:26:53,240
 I have a...

405
00:26:53,240 --> 00:26:54,240
 I have a...

406
00:26:54,240 --> 00:26:55,240
 I have a...

407
00:26:55,240 --> 00:26:56,240
 I have a...

408
00:26:56,240 --> 00:26:57,240
 I have a...

409
00:26:57,240 --> 00:26:58,240
 I have a...

410
00:26:58,240 --> 00:26:59,240
 I have a...

411
00:26:59,240 --> 00:27:00,240
 I have a...

412
00:27:00,240 --> 00:27:01,240
 I have a...

413
00:27:01,240 --> 00:27:02,240
 I have a...

414
00:27:02,240 --> 00:27:03,240
 I have a...

415
00:27:03,240 --> 00:27:04,240
 I have a...

416
00:27:04,240 --> 00:27:05,240
 I have a...

417
00:27:05,240 --> 00:27:06,240
 I have a...

418
00:27:06,240 --> 00:27:07,240
 I have a...

419
00:27:07,240 --> 00:27:08,240
 I have a...

420
00:27:08,240 --> 00:27:09,240
 I have a...

421
00:27:09,240 --> 00:27:10,240
 I have a...

422
00:27:10,240 --> 00:27:11,240
 I have a...

423
00:27:11,240 --> 00:27:12,240
 I have a...

424
00:27:12,240 --> 00:27:13,240
 I have a...

425
00:27:13,240 --> 00:27:14,240
 I have a...

426
00:27:14,240 --> 00:27:15,240
 I have a...

427
00:27:15,240 --> 00:27:16,240
 I have a...

428
00:27:16,240 --> 00:27:17,240
 I have a...

429
00:27:17,240 --> 00:27:18,240
 I have a...

430
00:27:18,240 --> 00:27:19,240
 I have a...

431
00:27:19,240 --> 00:27:20,240
 I have a...

432
00:27:20,240 --> 00:27:21,240
 I have a...

433
00:27:21,240 --> 00:27:22,240
 I have a...

434
00:27:22,240 --> 00:27:23,240
 I have a...

435
00:27:23,240 --> 00:27:24,240
 I have a...

436
00:27:24,240 --> 00:27:25,240
 I have a...

437
00:27:25,240 --> 00:27:26,240
 I have a...

438
00:27:26,240 --> 00:27:27,240
 I have a...

439
00:27:27,240 --> 00:27:28,240
 I have a...

440
00:27:28,240 --> 00:27:29,240
 I have a...

441
00:27:29,240 --> 00:27:30,240
 I have a...

442
00:27:30,240 --> 00:27:31,240
 I have a...

443
00:27:31,240 --> 00:27:32,240
 I have a...

444
00:27:32,240 --> 00:27:33,240
 I have a...

445
00:27:33,240 --> 00:27:34,240
 I have a...

446
00:27:34,240 --> 00:27:35,240
 I have a...

447
00:27:35,240 --> 00:27:36,240
 I have a...

448
00:27:36,240 --> 00:27:37,240
 I have a...

449
00:27:37,240 --> 00:27:38,240
 I have a...

450
00:27:38,240 --> 00:27:39,240
 I have a...

451
00:27:39,240 --> 00:27:40,240
 I have a...

452
00:27:40,240 --> 00:27:41,240
 I have a...

453
00:27:41,240 --> 00:27:42,240
 I have a...

454
00:27:42,240 --> 00:27:43,240
 I have a...

455
00:27:43,240 --> 00:27:44,240
 I have a...

456
00:27:44,240 --> 00:27:45,240
 I have a...

457
00:27:45,240 --> 00:27:46,240
 I have a...

458
00:27:46,240 --> 00:27:47,240
 I have a...

459
00:27:47,240 --> 00:27:48,240
 I have a...

460
00:27:48,240 --> 00:27:49,240
 I have a...

461
00:27:49,240 --> 00:27:50,240
 I have a...

462
00:27:50,240 --> 00:27:51,240
 I have a...

463
00:27:51,240 --> 00:27:52,240
 I have a...

464
00:27:52,240 --> 00:27:53,240
 I have a...

465
00:27:53,240 --> 00:27:54,240
 I have a...

466
00:27:54,240 --> 00:27:55,240
 I have a...

467
00:27:55,240 --> 00:27:56,240
 I have a...

468
00:27:56,240 --> 00:27:57,240
 I have a...

469
00:27:57,240 --> 00:27:58,240
 I have a...

470
00:27:58,240 --> 00:27:59,240
 I have a...

471
00:27:59,240 --> 00:28:00,240
 I have a...

472
00:28:00,240 --> 00:28:01,240
 I have a...

473
00:28:01,240 --> 00:28:02,240
 I have a...

474
00:28:02,240 --> 00:28:03,240
 I have a...

475
00:28:03,240 --> 00:28:04,240
 I have a...

476
00:28:04,240 --> 00:28:05,240
 I have a...

477
00:28:05,240 --> 00:28:06,240
 I have a...

478
00:28:06,240 --> 00:28:07,240
 I have a...

479
00:28:07,240 --> 00:28:08,240
 I have a...

480
00:28:08,240 --> 00:28:09,240
 I have a...

481
00:28:09,240 --> 00:28:10,240
 I have a...

482
00:28:10,240 --> 00:28:11,240
 I have a...

483
00:28:11,240 --> 00:28:12,240
 I have a...

484
00:28:12,240 --> 00:28:13,240
 I have a...

485
00:28:13,240 --> 00:28:14,240
 I have a...

486
00:28:14,240 --> 00:28:15,240
 I have a...

487
00:28:15,240 --> 00:28:16,240
 I have a...

488
00:28:16,240 --> 00:28:17,240
 I have a...

489
00:28:17,240 --> 00:28:18,240
 I have a...

490
00:28:18,240 --> 00:28:19,240
 I have a...

491
00:28:19,240 --> 00:28:20,240
 I have a...

492
00:28:20,240 --> 00:28:21,240
 I have a...

493
00:28:21,240 --> 00:28:22,240
 I have a...

494
00:28:22,240 --> 00:28:23,240
 I have a...

495
00:28:23,240 --> 00:28:24,240
 I have a...

496
00:28:24,240 --> 00:28:25,240
 I have a...

497
00:28:25,240 --> 00:28:26,240
 I have a...

498
00:28:26,240 --> 00:28:27,240
 I have a...

499
00:28:27,240 --> 00:28:28,240
 I have a...

500
00:28:28,240 --> 00:28:29,240
 I have a...

501
00:28:29,240 --> 00:28:30,240
 I have a...

502
00:28:30,240 --> 00:28:31,240
 I have a...

503
00:28:31,240 --> 00:28:32,240
 I have a...

504
00:28:32,240 --> 00:28:33,240
 I have a...

505
00:28:33,240 --> 00:28:34,240
 I have a...

506
00:28:34,240 --> 00:28:35,240
 I have a...

507
00:28:35,240 --> 00:28:36,240
 I have a...

508
00:28:36,240 --> 00:28:37,240
 I have a...

509
00:28:37,240 --> 00:28:38,240
 I have a...

510
00:28:38,240 --> 00:28:39,240
 I have a...

511
00:28:39,240 --> 00:28:40,240
 I have a...

512
00:28:40,240 --> 00:28:41,240
 I have a...

513
00:28:41,240 --> 00:28:42,240
 I have a...

514
00:28:42,240 --> 00:28:43,240
 I have a...

515
00:28:43,240 --> 00:28:44,240
 I have a...

516
00:28:44,240 --> 00:28:45,240
 I have a...

517
00:28:45,240 --> 00:28:46,240
 I have a...

518
00:28:46,240 --> 00:28:47,240
 I have a...

519
00:28:47,240 --> 00:28:48,240
 I have a...

520
00:28:48,240 --> 00:28:49,240
 I have a...

521
00:28:49,240 --> 00:28:50,240
 I have a...

522
00:28:50,240 --> 00:28:51,240
 I have a...

523
00:28:51,240 --> 00:28:52,240
 I have a...

524
00:28:52,240 --> 00:28:53,240
 I have a...

525
00:28:53,240 --> 00:28:54,240
 I have a...

526
00:28:54,240 --> 00:28:55,240
 I have a...

527
00:28:55,240 --> 00:28:56,240
 I have a...

528
00:28:56,240 --> 00:28:57,240
 I have a...

529
00:28:57,240 --> 00:28:58,240
 I have a...

530
00:28:58,240 --> 00:28:59,240
 I have a...

531
00:28:59,240 --> 00:29:00,240
 I have a...

532
00:29:00,240 --> 00:29:01,240
 I have a...

533
00:29:01,240 --> 00:29:02,240
 I have a...

534
00:29:02,240 --> 00:29:03,240
 I have a...

535
00:29:03,240 --> 00:29:04,240
 I have a...

536
00:29:04,240 --> 00:29:05,240
 I have a...

537
00:29:05,240 --> 00:29:06,240
 I have a...

538
00:29:06,240 --> 00:29:07,240
 I have a...

539
00:29:07,240 --> 00:29:08,240
 I have a...

540
00:29:08,240 --> 00:29:09,240
 I have a...

541
00:29:09,240 --> 00:29:10,240
 I have a...

542
00:29:10,240 --> 00:29:11,240
 I have a...

543
00:29:11,240 --> 00:29:12,240
 I have a...

544
00:29:12,240 --> 00:29:13,240
 I have a...

545
00:29:13,240 --> 00:29:14,240
 I have a...

546
00:29:14,240 --> 00:29:15,240
 I have a...

547
00:29:15,240 --> 00:29:16,240
 I have a...

548
00:29:16,240 --> 00:29:17,240
 I have a...

549
00:29:17,240 --> 00:29:18,240
 I have a...

550
00:29:18,240 --> 00:29:19,240
 I have a...

551
00:29:19,240 --> 00:29:20,240
 I have a...

552
00:29:20,240 --> 00:29:21,240
 I have a...

553
00:29:21,240 --> 00:29:22,240
 I have a...

554
00:29:22,240 --> 00:29:23,240
 I have a...

555
00:29:23,240 --> 00:29:24,240
 I have a...

556
00:29:24,240 --> 00:29:25,240
 I have a...

557
00:29:25,240 --> 00:29:26,240
 I have a...

558
00:29:26,240 --> 00:29:27,240
 I have a...

559
00:29:27,240 --> 00:29:28,240
 I have a...

560
00:29:28,240 --> 00:29:29,240
 I have a...

561
00:29:29,240 --> 00:29:30,240
 I have a...

562
00:29:30,240 --> 00:29:31,240
 I have a...

563
00:29:31,240 --> 00:29:32,240
 I have a...

564
00:29:32,240 --> 00:29:33,240
 I have a...

