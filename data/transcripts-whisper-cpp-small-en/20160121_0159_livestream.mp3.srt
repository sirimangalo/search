1
00:00:00,000 --> 00:00:04,200
 Good evening everyone.

2
00:00:04,200 --> 00:00:17,240
 I'm broadcasting live January 20th.

3
00:00:17,240 --> 00:00:21,800
 So yeah, we had a large crowd today for meditation at the

4
00:00:21,800 --> 00:00:23,800
 university.

5
00:00:23,800 --> 00:00:27,440
 13 people I think came.

6
00:00:27,440 --> 00:00:36,520
 Which I guess isn't a large crowd, but it's a sign.

7
00:00:36,520 --> 00:00:38,760
 Well they do say that early on in the semester a lot of

8
00:00:38,760 --> 00:00:42,480
 people show up and then it peters

9
00:00:42,480 --> 00:00:46,300
 out as the air goes on, but this is more than we had all

10
00:00:46,300 --> 00:00:49,360
 last semester, like in the beginning.

11
00:00:49,360 --> 00:00:52,880
 So it's a great thing to see.

12
00:00:52,880 --> 00:00:56,640
 Also interested in meditation tomorrow I'll be back at our

13
00:00:56,640 --> 00:00:59,320
 five minute meditation table.

14
00:00:59,320 --> 00:01:12,760
 So yeah, it's nice to be doing things very useful.

15
00:01:12,760 --> 00:01:29,960
 I was looking at the quote today, I can't find it.

16
00:01:29,960 --> 00:01:53,480
 I'll find the poly for it, but it's an interesting quote.

17
00:01:53,480 --> 00:02:10,830
 Like with the good, Kalyanamitata nourishes Sila, nourishes

18
00:02:10,830 --> 00:02:12,840
 virtue.

19
00:02:12,840 --> 00:02:16,240
 Restraint of the senses nourishes the holy life.

20
00:02:16,240 --> 00:02:22,520
 These are things to be remembered for sure.

21
00:02:22,520 --> 00:02:28,530
 Doing things at the proper time nourishes health, that's a

22
00:02:28,530 --> 00:02:29,920
 curious one.

23
00:02:29,920 --> 00:02:35,840
 Eating at the proper time.

24
00:02:35,840 --> 00:02:43,320
 Not staying up all night, carousing maybe.

25
00:02:43,320 --> 00:02:46,640
 Not quarreling nourishes friendship.

26
00:02:46,640 --> 00:02:53,330
 Yes, we have to spell it out because we're funny creatures

27
00:02:53,330 --> 00:02:56,120
 as humans.

28
00:02:56,120 --> 00:02:59,540
 We're blind to the things that we do, we hurt ourselves.

29
00:02:59,540 --> 00:03:04,660
 People stub their toe on something and then kick it, as

30
00:03:04,660 --> 00:03:07,480
 though that makes it better.

31
00:03:07,480 --> 00:03:11,800
 Get angry at the table, get angry at the chair.

32
00:03:11,800 --> 00:03:14,420
 Or stumbled around in a dark room and then gotten angry at

33
00:03:14,420 --> 00:03:15,720
 the furniture for being in

34
00:03:15,720 --> 00:03:16,720
 the wrong place.

35
00:03:16,720 --> 00:03:21,200
 We do funny things as human beings.

36
00:03:21,200 --> 00:03:23,720
 Sometimes you have to spell it out.

37
00:03:23,720 --> 00:03:41,550
 Do it if you fight with your friends, you're not going to

38
00:03:41,550 --> 00:03:46,920
 have friends.

39
00:03:46,920 --> 00:03:50,120
 Listening carefully and asking questions nourishes wisdom.

40
00:03:50,120 --> 00:03:53,360
 You'll hear that.

41
00:03:53,360 --> 00:03:54,360
 Ask questions.

42
00:03:54,360 --> 00:03:57,680
 If you ask questions, it nourishes wisdom.

43
00:03:57,680 --> 00:04:01,230
 But even said, if a person doesn't ask questions, doesn't

44
00:04:01,230 --> 00:04:03,680
 approach wise people and ask questions

45
00:04:03,680 --> 00:04:14,840
 of them, they're born stupid in their next life.

46
00:04:14,840 --> 00:04:21,560
 Right livelihood nourishes rebirth in heaven.

47
00:04:21,560 --> 00:04:23,960
 Interesting stuff.

48
00:04:23,960 --> 00:04:24,960
 Who's on the chat?

49
00:04:24,960 --> 00:04:26,960
 Does anybody here listening?

50
00:04:26,960 --> 00:04:30,960
 Say hello if you are.

51
00:04:30,960 --> 00:04:37,280
 What have you guys been talking about?

52
00:04:37,280 --> 00:04:39,000
 Here's a question.

53
00:04:39,000 --> 00:04:43,500
 Is it unskillful practice to utilize isochronic tones and b

54
00:04:43,500 --> 00:04:46,800
inaural sound beats during meditation?

55
00:04:46,800 --> 00:04:49,880
 Yes, yes it is.

56
00:04:49,880 --> 00:05:00,960
 Sorry, it is because it's conditioning.

57
00:05:00,960 --> 00:05:04,480
 You're conditioning the universe by doing that.

58
00:05:04,480 --> 00:05:08,960
 You're cheating in a sense.

59
00:05:08,960 --> 00:05:12,590
 You're not in tune with reality, meaning you're

60
00:05:12,590 --> 00:05:17,000
 conditioning reality to suit your purpose.

61
00:05:17,000 --> 00:05:25,320
 And by its very nature is a problem.

62
00:05:25,320 --> 00:05:28,070
 We want to do something that's so simple and non-cond

63
00:05:28,070 --> 00:05:29,400
itional, non-judgmental.

64
00:05:29,400 --> 00:05:33,950
 That's why we do something so banal is walking back and

65
00:05:33,950 --> 00:05:35,880
 forth and sitting.

66
00:05:35,880 --> 00:05:48,000
 But we don't want to condition it as much as possible.

67
00:05:48,000 --> 00:05:50,600
 Family divide due to extreme selfishness.

68
00:05:50,600 --> 00:05:58,680
 How does someone go about forgiving and forgetting even

69
00:05:58,680 --> 00:06:01,840
 after meditation?

70
00:06:01,840 --> 00:06:06,360
 So meditation will help you to forgive.

71
00:06:06,360 --> 00:06:07,760
 You should never really forget.

72
00:06:07,760 --> 00:06:12,400
 It's not the point, but you should forgive.

73
00:06:12,400 --> 00:06:19,100
 Meditation helps you realize that it's pointless to hold a

74
00:06:19,100 --> 00:06:20,400
 grudge.

75
00:06:20,400 --> 00:06:21,400
 We all die in the end.

76
00:06:21,400 --> 00:06:30,440
 There's really no benefit to holding on to anger and hatred

77
00:06:30,440 --> 00:06:32,720
 and malice.

78
00:06:32,720 --> 00:06:34,520
 Oh here's something.

79
00:06:34,520 --> 00:06:38,800
 So I got in touch with the people on Second Life.

80
00:06:38,800 --> 00:06:42,080
 And it sounds like Saturdays.

81
00:06:42,080 --> 00:06:46,320
 Sometime during the day on Saturdays I will be giving a

82
00:06:46,320 --> 00:06:48,400
 lecture on Second Life.

83
00:06:48,400 --> 00:06:50,400
 Who knows what I'll be talking about.

84
00:06:50,400 --> 00:06:53,680
 Why do I have to think of something to talk about?

85
00:06:53,680 --> 00:07:02,640
 What are we doing on Saturdays?

86
00:07:02,640 --> 00:07:05,640
 Question and answer.

87
00:07:05,640 --> 00:07:10,840
 Well maybe I'll just teach meditation.

88
00:07:10,840 --> 00:07:15,000
 We can do a group meditation.

89
00:07:15,000 --> 00:07:17,600
 We'll do a guided meditation or something.

90
00:07:17,600 --> 00:07:20,960
 There's the beginner thing.

91
00:07:20,960 --> 00:07:23,320
 But I'll have to start thinking of things to talk about in

92
00:07:23,320 --> 00:07:24,080
 Second Life.

93
00:07:24,080 --> 00:07:27,640
 And I'll probably have to find a way to record it.

94
00:07:27,640 --> 00:07:28,640
 Oh boy.

95
00:07:28,640 --> 00:07:31,640
 A lot of work.

96
00:07:31,640 --> 00:07:36,800
 Now we'll see.

97
00:07:36,800 --> 00:07:41,520
 Anyway, thanks.

98
00:07:41,520 --> 00:07:44,320
 Nice to see everyone meditating and joining.

99
00:07:44,320 --> 00:07:47,760
 He was making use of the website.

100
00:07:47,760 --> 00:07:52,800
 A long list of meditators today.

101
00:07:52,800 --> 00:07:55,840
 That's great.

102
00:07:55,840 --> 00:08:00,960
 Nobody has any pressing questions or concerns.

103
00:08:00,960 --> 00:08:05,760
 I'll just say goodnight.

104
00:08:05,760 --> 00:08:11,840
 So, goodnight.

105
00:08:11,840 --> 00:08:12,840
 Goodnight.

