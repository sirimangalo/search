1
00:00:00,000 --> 00:00:07,000
 Okay, good evening.

2
00:00:07,000 --> 00:00:13,000
 Good evening everybody.

3
00:00:13,000 --> 00:00:19,000
 Broadcasting live.

4
00:00:19,000 --> 00:00:25,000
 January 17th.

5
00:00:25,000 --> 00:00:41,000
 I was thinking about these online hangouts.

6
00:00:41,000 --> 00:00:51,000
 It would be nice to have an actual hangout

7
00:00:51,000 --> 00:00:55,000
 where people came on and talked.

8
00:00:55,000 --> 00:01:00,000
 If they had questions, they could come on.

9
00:01:00,000 --> 00:01:03,000
 There are some problems, you know.

10
00:01:03,000 --> 00:01:09,000
 People not everyone is awake at that time.

11
00:01:09,000 --> 00:01:16,000
 People in Europe.

12
00:01:16,000 --> 00:01:23,330
 For a lot of people to only have one opportunity to ask

13
00:01:23,330 --> 00:01:26,000
 questions.

14
00:01:26,000 --> 00:01:28,000
 Not the greatest time.

15
00:01:28,000 --> 00:01:31,000
 But the format as it is isn't the best either.

16
00:01:31,000 --> 00:01:38,850
 There's a lot of people who don't want to ask questions in

17
00:01:38,850 --> 00:01:42,000
 public.

18
00:01:42,000 --> 00:01:52,000
 We need if we could have a group.

19
00:01:52,000 --> 00:01:57,000
 Kind of like what we do on Mumble.

20
00:01:57,000 --> 00:02:07,000
 Maybe just talk about the dumber or something.

21
00:02:07,000 --> 00:02:13,000
 Something to think about.

22
00:02:13,000 --> 00:02:19,000
 How to best use the technology.

23
00:02:19,000 --> 00:02:22,000
 Anyway, I'll keep making YouTube videos.

24
00:02:22,000 --> 00:02:25,000
 There's always that.

25
00:02:25,000 --> 00:02:28,000
 So today's quote is about householders.

26
00:02:28,000 --> 00:02:32,630
 I think the most immediately interesting thing about this

27
00:02:32,630 --> 00:02:36,000
 quote for me is

28
00:02:36,000 --> 00:02:40,620
 how I sometimes wonder whether people get the wrong

29
00:02:40,620 --> 00:02:50,000
 impression about Buddhism from me.

30
00:02:50,000 --> 00:02:54,000
 People like me.

31
00:02:54,000 --> 00:02:56,000
 Being a Buddhist monk.

32
00:02:56,000 --> 00:03:08,340
 I know here on McMaster, a lot of people think I'm a

33
00:03:08,340 --> 00:03:10,000
 representative of Buddhists.

34
00:03:10,000 --> 00:03:14,000
 So that all Buddhists wear clothes like this.

35
00:03:14,000 --> 00:03:17,000
 Or that maybe I'm just a zealous sort of Buddhist.

36
00:03:17,000 --> 00:03:20,690
 A lot of people don't realize that I'm actually an ordained

37
00:03:20,690 --> 00:03:22,000
 Buddhist monk.

38
00:03:22,000 --> 00:03:27,000
 So when I talk to them about the sort of rules that I keep,

39
00:03:27,000 --> 00:03:32,000
 I found myself, to my surprise, having to tell them that

40
00:03:32,000 --> 00:03:37,000
 that's not what all Buddhists do.

41
00:03:37,000 --> 00:03:44,000
 Not all Buddhists only eat in the morning.

42
00:03:44,000 --> 00:03:57,000
 Not all Buddhists don't touch money, that kind of thing.

43
00:03:57,000 --> 00:04:01,360
 And I wonder whether sometimes meditators, though they don

44
00:04:01,360 --> 00:04:02,000
't fall,

45
00:04:02,000 --> 00:04:07,000
 people following me know that I'm a Buddhist monk,

46
00:04:07,000 --> 00:04:11,670
 whether there's still the idea that somehow they should try

47
00:04:11,670 --> 00:04:18,000
 to emulate the monastic life.

48
00:04:18,000 --> 00:04:25,270
 And as honorable as that is, I think it misses some of the

49
00:04:25,270 --> 00:04:29,000
 reality of life that even a Buddhist monk has to face.

50
00:04:29,000 --> 00:04:37,000
 I mean, even I have to find a way to get my requisites.

51
00:04:37,000 --> 00:04:45,000
 If that means finding a tree to find shade under,

52
00:04:45,000 --> 00:04:49,920
 if it means someone offering me a place to stay in a hut or

53
00:04:49,920 --> 00:04:51,000
 something,

54
00:04:51,000 --> 00:04:54,000
 or maybe in a cave,

55
00:04:54,000 --> 00:04:56,440
 or if it means monks getting together and building a

56
00:04:56,440 --> 00:04:57,000
 monastery,

57
00:04:57,000 --> 00:05:01,970
 there are still requirements in going for food every day

58
00:05:01,970 --> 00:05:06,000
 and going into the village.

59
00:05:06,000 --> 00:05:11,870
 It's a far cry from what lay people have to do, but the

60
00:05:11,870 --> 00:05:19,000
 reality is still there.

61
00:05:19,000 --> 00:05:29,120
 So I get the feeling that sometimes people, because

62
00:05:29,120 --> 00:05:30,000
 meditation effectively ends a lot of ambition

63
00:05:30,000 --> 00:05:35,000
 as far as being successful in a worldly sense,

64
00:05:35,000 --> 00:05:40,780
 and it's often down on the idea of worldly success, or it

65
00:05:40,780 --> 00:05:42,000
 seems to be,

66
00:05:42,000 --> 00:05:53,000
 that people often become inert,

67
00:05:53,000 --> 00:06:08,000
 they lose their ability really to function in society.

68
00:06:08,000 --> 00:06:15,000
 The desire to work is gone, so why go to work?

69
00:06:15,000 --> 00:06:19,000
 Well, for obvious reasons, you need to live.

70
00:06:19,000 --> 00:06:25,810
 You have a hard time maybe getting out and applying for

71
00:06:25,810 --> 00:06:29,000
 jobs and so on.

72
00:06:29,000 --> 00:06:34,000
 I think it's pretty silly.

73
00:06:34,000 --> 00:06:39,000
 Why silly? Because a meditator is so much more efficient

74
00:06:39,000 --> 00:06:40,000
 than a non-meditator,

75
00:06:40,000 --> 00:06:42,000
 all other things being considered.

76
00:06:42,000 --> 00:06:45,600
 I mean, given a person before they meditate and after they

77
00:06:45,600 --> 00:06:47,000
 start doing mindfulness meditation,

78
00:06:47,000 --> 00:06:52,000
 there's so much, it's a completely different night and day,

79
00:06:52,000 --> 00:06:59,000
 that they should have no trouble finding a job,

80
00:06:59,000 --> 00:07:04,640
 working at a job, keeping a job, because they're honest and

81
00:07:04,640 --> 00:07:06,000
 trustworthy and all these things.

82
00:07:06,000 --> 00:07:18,000
 They make the best employee.

83
00:07:18,000 --> 00:07:29,000
 I mean, when I came back from Asia,

84
00:07:29,000 --> 00:07:34,380
 at first I went to stay in the monastery, so I did the

85
00:07:34,380 --> 00:07:37,000
 whole not working thing,

86
00:07:37,000 --> 00:07:41,690
 which by all means, if you can find a way to live in the

87
00:07:41,690 --> 00:07:45,000
 monastery, that's awesome.

88
00:07:45,000 --> 00:07:57,000
 But we shouldn't disregard the necessity to work,

89
00:07:57,000 --> 00:08:03,050
 because I mean, I did this, I needed to survive and I

90
00:08:03,050 --> 00:08:06,000
 wanted to get a way back to Thailand.

91
00:08:06,000 --> 00:08:15,000
 I was sat down going back and eventually becoming a monk.

92
00:08:15,000 --> 00:08:23,190
 So I went to tree planting and spent 10 hours a day, and I

93
00:08:23,190 --> 00:08:24,000
 was not a physical person.

94
00:08:24,000 --> 00:08:27,000
 I've never been a terribly physical person.

95
00:08:27,000 --> 00:08:33,000
 I guess I was in good health, good shape physically.

96
00:08:33,000 --> 00:08:37,520
 But not at the top, but I was at the top when we did tree

97
00:08:37,520 --> 00:08:39,000
 planting, because I could just push myself.

98
00:08:39,000 --> 00:08:44,000
 I had none of the baggage that people were carrying around.

99
00:08:44,000 --> 00:08:48,000
 I was able to focus and be mindful.

100
00:08:48,000 --> 00:08:53,000
 Then we went blueberry picking.

101
00:08:53,000 --> 00:08:56,000
 I found a way to make money.

102
00:08:56,000 --> 00:09:02,160
 So this quote is about happiness that lay people find, and

103
00:09:02,160 --> 00:09:04,550
 I think it could be kind of strange to hear the Buddha

104
00:09:04,550 --> 00:09:05,000
 talking.

105
00:09:05,000 --> 00:09:09,450
 It's at least refreshing to hear him talking about these

106
00:09:09,450 --> 00:09:14,000
 things that are on probably many people's minds,

107
00:09:14,000 --> 00:09:18,000
 those people who are not monastic people.

108
00:09:18,000 --> 00:09:26,960
 You know how he talks about not being in debt and having

109
00:09:26,960 --> 00:09:35,000
 the ability to enjoy the things that you have.

110
00:09:35,000 --> 00:09:47,920
 To have things. Ownership is a sense of happiness, to not

111
00:09:47,920 --> 00:09:55,000
 be without, to be destitute.

112
00:09:55,000 --> 00:10:01,630
 The first two, the happiness of ownership and the happiness

113
00:10:01,630 --> 00:10:05,000
 of wealth, are very mundane.

114
00:10:05,000 --> 00:10:08,980
 These are not very Buddhist happinesses, and we all know

115
00:10:08,980 --> 00:10:12,000
 that in the end they're not satisfying.

116
00:10:12,000 --> 00:10:17,200
 But there's a recognition of them, and I think that's

117
00:10:17,200 --> 00:10:19,000
 important.

118
00:10:19,000 --> 00:10:30,480
 It's important that we don't, they're not evil in the sense

119
00:10:30,480 --> 00:10:35,000
 of blocking our practice.

120
00:10:35,000 --> 00:10:38,220
 They'll drag you down, and for sure the attachments are a

121
00:10:38,220 --> 00:10:39,000
 problem.

122
00:10:39,000 --> 00:10:45,000
 But much more important is learning than denying.

123
00:10:45,000 --> 00:10:50,620
 So if you have desires or attachments, it's important that

124
00:10:50,620 --> 00:10:53,000
 you learn about them.

125
00:10:53,000 --> 00:10:58,080
 Now by all means, if you can do that without these things,

126
00:10:58,080 --> 00:11:01,000
 I guess I'm not really promoting them.

127
00:11:01,000 --> 00:11:08,280
 What I'm trying to say is that there's a danger potentially

128
00:11:08,280 --> 00:11:11,000
 of not being able to function,

129
00:11:11,000 --> 00:11:17,320
 and not deciding to give up meditation maybe, because you

130
00:11:17,320 --> 00:11:20,000
 think it stops you from functioning in society,

131
00:11:20,000 --> 00:11:27,000
 because it makes you, you lose your ambition.

132
00:11:27,000 --> 00:11:32,600
 It makes you inert. But in fact the point is you don't need

133
00:11:32,600 --> 00:11:34,000
 ambition to work,

134
00:11:34,000 --> 00:11:44,100
 you just need wisdom, really. And simple wisdom, wisdom

135
00:11:44,100 --> 00:11:48,000
 that tells you if you don't work, you don't eat.

136
00:11:48,000 --> 00:11:50,000
 Meditation should help you with that.

137
00:11:50,000 --> 00:11:53,550
 I mean, I think there's a potential in the beginning for it

138
00:11:53,550 --> 00:11:54,000
 not to,

139
00:11:54,000 --> 00:11:59,410
 for you to not, you to get just caught up in the

140
00:11:59,410 --> 00:12:02,000
 ramifications of the meditation,

141
00:12:02,000 --> 00:12:07,000
 that everything is without meaning and purpose and so on,

142
00:12:07,000 --> 00:12:14,120
 and just on a very superficial level, except this idea of

143
00:12:14,120 --> 00:12:16,000
 meaningless.

144
00:12:16,000 --> 00:12:20,000
 A deep understanding of meaninglessness allows you to work,

145
00:12:20,000 --> 00:12:22,000
 allows you to do anything.

146
00:12:22,000 --> 00:12:27,000
 You can do it as just the right thing to do at the time.

147
00:12:27,000 --> 00:12:31,770
 It's not the right thing to do to sit around and do nothing

148
00:12:31,770 --> 00:12:32,000
.

149
00:12:32,000 --> 00:12:38,000
 It's not the right thing to do necessarily all the time.

150
00:12:38,000 --> 00:12:39,630
 So that's, I mean, that's not really what the Buddha is

151
00:12:39,630 --> 00:12:40,000
 getting at,

152
00:12:40,000 --> 00:12:44,470
 but I thought that was an interesting point for us to

153
00:12:44,470 --> 00:12:47,000
 accept, for people to accept being a lay person.

154
00:12:47,000 --> 00:12:55,800
 I mean, it goes without saying that, without some kind of

155
00:12:55,800 --> 00:12:58,000
 support group,

156
00:12:58,000 --> 00:13:06,300
 people who are able to bring resources to the Buddhist

157
00:13:06,300 --> 00:13:08,000
 community,

158
00:13:08,000 --> 00:13:12,000
 monastics, we couldn't all be living in the monastery,

159
00:13:12,000 --> 00:13:16,020
 because we don't grow food in the monastery and there's

160
00:13:16,020 --> 00:13:21,000
 many things that we can't get in the monastery.

161
00:13:21,000 --> 00:13:24,070
 So I mean, we're walking, it would be great if everyone

162
00:13:24,070 --> 00:13:25,000
 could just come.

163
00:13:25,000 --> 00:13:30,600
 Then we have the story of Daimya, he's a prince who went

164
00:13:30,600 --> 00:13:31,000
 forth,

165
00:13:31,000 --> 00:13:33,000
 a very awesome story.

166
00:13:33,000 --> 00:13:37,280
 For 16 years he pretended to be useless, so they were going

167
00:13:37,280 --> 00:13:38,000
 to kill him,

168
00:13:38,000 --> 00:13:42,240
 and I even brought him out to the forest and then he just

169
00:13:42,240 --> 00:13:44,000
 got up and walked away.

170
00:13:44,000 --> 00:13:46,370
 But then everybody followed him, the whole city, the king

171
00:13:46,370 --> 00:13:48,000
 and the queen and everybody in the whole city.

172
00:13:48,000 --> 00:13:50,000
 It's a wonderful story.

173
00:13:50,000 --> 00:13:53,000
 I haven't done it justice by that description at all,

174
00:13:53,000 --> 00:13:59,070
 but just the point that in the end they wind up building a

175
00:13:59,070 --> 00:14:00,000
 city in the forest,

176
00:14:00,000 --> 00:14:03,610
 a monastery and everybody, the whole city, the king, the

177
00:14:03,610 --> 00:14:05,000
 queen, the prince,

178
00:14:05,000 --> 00:14:08,860
 and all the court and all the citizens, they all follow D

179
00:14:08,860 --> 00:14:11,000
aimya into the forest.

180
00:14:11,000 --> 00:14:14,000
 They basically move the city and live in the forest.

181
00:14:14,000 --> 00:14:23,000
 So unless that happens, there's a...

182
00:14:23,000 --> 00:14:28,870
 Yeah, we need to... I mean, there's great support that can

183
00:14:28,870 --> 00:14:30,000
 be done.

184
00:14:30,000 --> 00:14:34,790
 It's just if the difference is sitting around doing nothing

185
00:14:34,790 --> 00:14:39,000
, feeling kind of depressed,

186
00:14:39,000 --> 00:14:44,000
 maybe causing friction with your family and so on,

187
00:14:44,000 --> 00:14:47,000
 and just going out and working, you know,

188
00:14:47,000 --> 00:14:51,820
 doing a job where you can be mindful and making some money

189
00:14:51,820 --> 00:14:55,000
 to support yourself.

190
00:14:55,000 --> 00:15:04,000
 There's certainly not against Buddhism.

191
00:15:04,000 --> 00:15:12,000
 Not in a larger sense, for sure.

192
00:15:12,000 --> 00:15:19,370
 Of course, a more obvious aspect of this quote is kind of

193
00:15:19,370 --> 00:15:25,000
 the hinting at an emphasis on the mind.

194
00:15:25,000 --> 00:15:27,980
 When he thinks of this, he feels happiness and satisfaction

195
00:15:27,980 --> 00:15:28,000
.

196
00:15:28,000 --> 00:15:31,000
 That's repeated with great freedom.

197
00:15:31,000 --> 00:15:34,740
 When you think of this thing, happiness is very much a

198
00:15:34,740 --> 00:15:36,000
 mental thing.

199
00:15:36,000 --> 00:15:51,130
 It isn't really based on physical accommodations or

200
00:15:51,130 --> 00:15:58,000
 possessions and so on.

201
00:15:58,000 --> 00:16:03,390
 Happiness is very much in the mind and so blamelessness is

202
00:16:03,390 --> 00:16:08,000
 really the most wonderful form of happiness.

203
00:16:08,000 --> 00:16:12,000
 And that's the one... it's like a build-up.

204
00:16:12,000 --> 00:16:16,000
 Ownership is good, but wealth is better.

205
00:16:16,000 --> 00:16:20,000
 Ownership of valuable things, maybe.

206
00:16:20,000 --> 00:16:26,000
 And that's good, but freedom from debt is even better.

207
00:16:26,000 --> 00:16:30,000
 And that kind of speaks of karma. This is the kind of debt,

208
00:16:30,000 --> 00:16:34,000
 karmic debt as well.

209
00:16:34,000 --> 00:16:39,950
 But better than freedom from debt is... I guess the fourth

210
00:16:39,950 --> 00:16:43,000
 one is more like karmic debt.

211
00:16:43,000 --> 00:16:48,000
 Freedom from blame, having done nothing wrong.

212
00:16:48,000 --> 00:16:51,000
 That's for sure the best of these four.

213
00:16:51,000 --> 00:16:57,620
 A person who is free from blame is enlightened in an

214
00:16:57,620 --> 00:16:59,000
 ultimate sense.

215
00:16:59,000 --> 00:17:04,520
 But in a conventional sense, it just means when a person

216
00:17:04,520 --> 00:17:08,000
 has no real crimes or faults,

217
00:17:08,000 --> 00:17:11,850
 don't kill, don't steal, don't cheat, don't lie, don't take

218
00:17:11,850 --> 00:17:13,000
 drugs or alcohol,

219
00:17:13,000 --> 00:17:17,000
 don't gamble, and there's very little you can...

220
00:17:17,000 --> 00:17:21,000
 And they feel happy about that. They feel confident.

221
00:17:21,000 --> 00:17:28,000
 There's a confidence that thieves and murderers don't have.

222
00:17:28,000 --> 00:17:34,000
 There's a happiness and a peace that comes from that.

223
00:17:34,000 --> 00:17:47,300
 It's an interesting quote, as all the Buddhist teachings

224
00:17:47,300 --> 00:17:49,000
 are.

225
00:17:49,000 --> 00:17:56,500
 Anyway, that's all for tonight. Thank you all for tuning in

226
00:17:56,500 --> 00:17:57,000
.

227
00:17:57,000 --> 00:17:59,000
 Have a good night.

228
00:18:00,000 --> 00:18:15,000
 [

