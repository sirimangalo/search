1
00:00:00,000 --> 00:00:24,240
 Okay, so we're live with audio, I think.

2
00:00:24,240 --> 00:00:36,070
 And I think we might switch to audio only for four days of

3
00:00:36,070 --> 00:00:38,680
 the week.

4
00:00:38,680 --> 00:00:41,520
 Look to see how this goes.

5
00:00:41,520 --> 00:00:47,520
 Heck maybe we'll switch to audio only for holidays.

6
00:00:47,520 --> 00:00:49,520
 And maybe we'll do a Saturday.

7
00:00:49,520 --> 00:00:51,520
 Wait, no, Saturdays.

8
00:00:51,520 --> 00:00:57,760
 So Saturdays Buddhism 101.

9
00:00:57,760 --> 00:01:02,160
 I was talking to Robin, or Robin sent me a message

10
00:01:02,160 --> 00:01:05,720
 suggesting to maybe do Q&A once a

11
00:01:05,720 --> 00:01:13,240
 week instead of seven times a week.

12
00:01:13,240 --> 00:01:16,990
 So the idea actually of doing daily broadcasts was never

13
00:01:16,990 --> 00:01:19,520
 really to answer questions every

14
00:01:19,520 --> 00:01:20,520
 day.

15
00:01:20,520 --> 00:01:25,690
 I mean, it was always open, but it was more just to check

16
00:01:25,690 --> 00:01:28,880
 in, say hello, maybe talk about

17
00:01:28,880 --> 00:01:32,520
 a quote.

18
00:01:32,520 --> 00:01:37,800
 So let's see, maybe Tuesday we'll do Dhammapada.

19
00:01:37,800 --> 00:01:39,800
 Thursday we'll do Q&A.

20
00:01:39,800 --> 00:01:42,320
 Or wait, yeah, maybe.

21
00:01:42,320 --> 00:01:45,760
 And then Saturday we'll do Buddhism 101.

22
00:01:45,760 --> 00:01:53,290
 Or maybe we can do Buddhism 101 on Thursday and then Q&A on

23
00:01:53,290 --> 00:01:54,960
 Saturday.

24
00:01:54,960 --> 00:01:59,200
 Because Saturday is when most people are on, I would think.

25
00:01:59,200 --> 00:02:03,240
 I don't really know.

26
00:02:03,240 --> 00:02:08,720
 But tonight I think we'll just do audio.

27
00:02:08,720 --> 00:02:15,440
 So today's quote is from the introduction to the Jataka.

28
00:02:15,440 --> 00:02:18,610
 And I believe it's when Sumedha has just been given the

29
00:02:18,610 --> 00:02:20,840
 proclamation, the prognostication

30
00:02:20,840 --> 00:02:28,960
 of future Buddhahood at the feet of the Buddha Deepankar.

31
00:02:28,960 --> 00:02:33,170
 And so he's reminiscing on the fact that he's destined to

32
00:02:33,170 --> 00:02:35,320
 become a Buddha because the words

33
00:02:35,320 --> 00:02:41,200
 of a Supreme Buddha are always certain and short.

34
00:02:41,200 --> 00:02:49,620
 This sort of thing that, it's hard to, there's no evidence

35
00:02:49,620 --> 00:02:52,360
 for this, right?

36
00:02:52,360 --> 00:02:54,280
 Why is it that what a Buddha says is sure?

37
00:02:54,280 --> 00:03:02,600
 How do we know that the Buddha knows everything, that they

38
00:03:02,600 --> 00:03:05,520
're always right?

39
00:03:05,520 --> 00:03:12,500
 I don't think this, I don't think for the most part that it

40
00:03:12,500 --> 00:03:15,440
's really an important thing

41
00:03:15,440 --> 00:03:17,000
 to concern ourselves with.

42
00:03:17,000 --> 00:03:20,560
 I mean a lot of things like whether angels exist and

43
00:03:20,560 --> 00:03:23,240
 whether heaven exists, they're kind

44
00:03:23,240 --> 00:03:25,920
 of on the back burner.

45
00:03:25,920 --> 00:03:36,360
 They're kind of lesser important questions.

46
00:03:36,360 --> 00:03:41,160
 But it's awesome to think that there could be such a person

47
00:03:41,160 --> 00:03:43,480
 who always tells the truth

48
00:03:43,480 --> 00:03:45,480
 and knows all truth.

49
00:03:45,480 --> 00:03:51,400
 Wouldn't that be great?

50
00:03:51,400 --> 00:03:57,090
 It's awesome to think about what someone who is just so

51
00:03:57,090 --> 00:03:59,560
 pure and so perfect.

52
00:03:59,560 --> 00:04:05,900
 So in a sense that's all we have to think about with the

53
00:04:05,900 --> 00:04:09,600
 Buddha because the power of,

54
00:04:09,600 --> 00:04:12,920
 the Buddha isn't in whether he existed or not.

55
00:04:12,920 --> 00:04:19,160
 The power is in the ideal, perfection.

56
00:04:19,160 --> 00:04:26,800
 The power for us is when we think of such a perfect being.

57
00:04:26,800 --> 00:04:31,330
 So the point I'm trying to make is we shouldn't get hung up

58
00:04:31,330 --> 00:04:33,880
 on real or not real, true or not

59
00:04:33,880 --> 00:04:34,880
 true.

60
00:04:34,880 --> 00:04:41,120
 It's just the idea of truth, purity, wisdom.

61
00:04:41,120 --> 00:04:46,830
 The same goes with like the idea of heaven and rebirth and

62
00:04:46,830 --> 00:04:47,760
 so on.

63
00:04:47,760 --> 00:04:49,760
 So maybe you don't believe it, you don't think there's any

64
00:04:49,760 --> 00:04:50,240
 reason to.

65
00:04:50,240 --> 00:04:57,560
 There's no proof or no convincing evidence of rebirth.

66
00:04:57,560 --> 00:05:03,510
 But still, you don't have to entertain such doubt because

67
00:05:03,510 --> 00:05:06,040
 it can eat away at you.

68
00:05:06,040 --> 00:05:09,390
 Because no matter what, the best way to live is as though

69
00:05:09,390 --> 00:05:11,280
 our actions had consequences

70
00:05:11,280 --> 00:05:12,280
 all around.

71
00:05:12,280 --> 00:05:16,140
 The best way to live, better for ourselves, better for the

72
00:05:16,140 --> 00:05:18,480
 world, better for other people,

73
00:05:18,480 --> 00:05:19,480
 just better.

74
00:05:19,480 --> 00:05:27,040
 Even in this life.

75
00:05:27,040 --> 00:05:30,080
 So there's many, many cases.

76
00:05:30,080 --> 00:05:33,380
 The general concept that I'm talking about here is the

77
00:05:33,380 --> 00:05:34,880
 difference between reality and

78
00:05:34,880 --> 00:05:39,180
 abstraction.

79
00:05:39,180 --> 00:05:44,020
 So this applies for all sorts of concepts and views and

80
00:05:44,020 --> 00:05:46,960
 opinions that we might have.

81
00:05:46,960 --> 00:05:56,670
 They're all limited use in a practical sense, a limited

82
00:05:56,670 --> 00:05:58,420
 value.

83
00:05:58,420 --> 00:06:01,880
 Like the idea of progress, progress on the meditation path.

84
00:06:01,880 --> 00:06:07,050
 We're always concerned about our progress, how far we've

85
00:06:07,050 --> 00:06:09,640
 gotten, or the goal, what am

86
00:06:09,640 --> 00:06:13,180
 I aiming for, what am I doing this for, why am I meditating

87
00:06:13,180 --> 00:06:13,480
.

88
00:06:13,480 --> 00:06:19,470
 You can get discouraging when you lose sight of that and

89
00:06:19,470 --> 00:06:23,200
 you really don't see the point.

90
00:06:23,200 --> 00:06:29,980
 At any rate, it's harder, it's a lot of work to do

91
00:06:29,980 --> 00:06:33,600
 something for a future gain.

92
00:06:33,600 --> 00:06:38,800
 When if you focus on reality and the benefit of being pure

93
00:06:38,800 --> 00:06:42,400
 in that moment, there's no doubt,

94
00:06:42,400 --> 00:06:44,240
 no concern, no confusion.

95
00:06:44,240 --> 00:06:55,880
 So when you think of the Buddha, don't worry about what he

96
00:06:55,880 --> 00:07:00,920
 was like or something, think

97
00:07:00,920 --> 00:07:02,880
 about the purity.

98
00:07:02,880 --> 00:07:09,160
 Think about this ideal of a teacher who did know the truth.

99
00:07:09,160 --> 00:07:13,000
 There's so much evidence to show that he did know the truth

100
00:07:13,000 --> 00:07:14,720
 that nobody else knew.

101
00:07:14,720 --> 00:07:19,530
 He did have a way of looking at things that was unique and

102
00:07:19,530 --> 00:07:21,200
 uniquely wise.

103
00:07:21,200 --> 00:07:23,680
 So question.

104
00:07:23,680 --> 00:07:26,880
 So think about that.

105
00:07:26,880 --> 00:07:30,510
 When you reflect on the Buddha, there's some great power in

106
00:07:30,510 --> 00:07:30,960
 it.

107
00:07:30,960 --> 00:07:33,160
 It's a great support for our practice.

108
00:07:33,160 --> 00:07:39,960
 Buddha Nusati is one of the four protective meditations,

109
00:07:39,960 --> 00:07:43,720
 thinking about the purest sort

110
00:07:43,720 --> 00:07:52,080
 of being that exists, the ultimate impurity and compassion

111
00:07:52,080 --> 00:07:53,560
 and wisdom.

112
00:07:53,560 --> 00:08:03,700
 So just some thoughts on that quote and in general on how

113
00:08:03,700 --> 00:08:09,080
 we should frame our thoughts

114
00:08:09,080 --> 00:08:16,940
 and our mental pursuits concerning ourselves so much with

115
00:08:16,940 --> 00:08:21,000
 abstractions like cosmology and

116
00:08:21,000 --> 00:08:30,030
 existence, but focus on the reality and on the practicality

117
00:08:30,030 --> 00:08:34,200
 of our aspirations, of our

118
00:08:34,200 --> 00:08:42,280
 intentions, of our motivations, of our direction in life.

119
00:08:42,280 --> 00:08:45,560
 There's some dhamma for today.

120
00:08:45,560 --> 00:08:46,920
 Thanks everyone for tuning in.

121
00:08:46,920 --> 00:08:49,940
 I'll try to come back every night with some audio, but

122
00:08:49,940 --> 00:08:51,960
 Tuesday we'll have another dhamma

123
00:08:51,960 --> 00:08:52,960
 phala video.

124
00:08:52,960 --> 00:08:53,960
 So have a good night.

125
00:08:53,960 --> 00:08:53,960
 Be well.

126
00:08:53,960 --> 00:08:57,120
 1

127
00:08:57,120 --> 00:08:59,120
 1

128
00:08:59,120 --> 00:09:01,120
 1

129
00:09:01,120 --> 00:09:03,120
 1

130
00:09:03,120 --> 00:09:05,120
 1

131
00:09:05,120 --> 00:09:07,120
 1

