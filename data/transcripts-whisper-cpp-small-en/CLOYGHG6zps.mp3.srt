1
00:00:00,000 --> 00:00:05,290
 Hi, welcome back to Ask A Minute. Today's question comes

2
00:00:05,290 --> 00:00:09,000
 from Stilson. "If a person

3
00:00:09,000 --> 00:00:13,830
 succumbs to a craving for a drug, e.g. cannabis, perhaps

4
00:00:13,830 --> 00:00:15,000
 because they are only

5
00:00:15,000 --> 00:00:17,590
 beginning to understand how and why to control all add

6
00:00:17,590 --> 00:00:18,480
ictions through

7
00:00:18,480 --> 00:00:23,040
 meditation, is it right to continue meditating in that

8
00:00:23,040 --> 00:00:25,080
 altered mind state?"

9
00:00:26,760 --> 00:00:34,760
 The difficult one. Certainly with all addictions, that's

10
00:00:34,760 --> 00:00:35,720
 really the only way

11
00:00:35,720 --> 00:00:42,950
 you can start. You can't be expected to, in many cases, you

12
00:00:42,950 --> 00:00:43,760
 can't be expected to

13
00:00:43,760 --> 00:00:48,400
 stop cold turkey and one of the first steps is to observe

14
00:00:48,400 --> 00:00:49,680
 the addiction

15
00:00:49,680 --> 00:00:55,920
 itself, observe the state of intoxication. Unfortunately,

16
00:00:55,920 --> 00:00:56,480
 it's an

17
00:00:56,480 --> 00:01:00,800
 intoxicated state and therefore it's antithetical to

18
00:01:00,800 --> 00:01:02,080
 mindfulness or to

19
00:01:02,080 --> 00:01:09,600
 clear awareness. So the ability to do so is limited. But it

20
00:01:09,600 --> 00:01:10,240
's something to keep

21
00:01:10,240 --> 00:01:15,210
 in mind with all addictions is our acceptance will help us

22
00:01:15,210 --> 00:01:16,520
 to overcome it.

23
00:01:16,520 --> 00:01:23,840
 Our acceptance of our addiction, our acknowledgement of it,

24
00:01:23,840 --> 00:01:24,800
 that yes, it does

25
00:01:24,800 --> 00:01:31,340
 exist, we have this addiction, yes, we do want this object,

26
00:01:31,340 --> 00:01:33,600
 we do like the

27
00:01:33,600 --> 00:01:38,560
 sensations and so on and that's why we're using this mantra

28
00:01:38,560 --> 00:01:39,680
, the clear

29
00:01:39,680 --> 00:01:42,760
 thought reminding ourselves of what it is. It's an

30
00:01:42,760 --> 00:01:45,320
 affirmation of the state, not

31
00:01:45,320 --> 00:01:49,260
 saying that this is good, but saying that this is how we

32
00:01:49,260 --> 00:01:50,720
 feel about it and

33
00:01:50,720 --> 00:01:54,630
 this is how we perceive it and this is the nature of it.

34
00:01:54,630 --> 00:01:58,000
 Once we do that, what

35
00:01:58,000 --> 00:02:07,040
 happens is we start to get bored of it. We start to see it

36
00:02:07,040 --> 00:02:07,600
 for what it is.

37
00:02:07,600 --> 00:02:14,800
 It loses its magic, it loses the allure, it loses this ill

38
00:02:14,800 --> 00:02:18,960
usory attractiveness.

39
00:02:18,960 --> 00:02:22,110
 The truth about it, we come to see that the truth about it

40
00:02:22,110 --> 00:02:23,400
 is that there's

41
00:02:23,400 --> 00:02:26,430
 nothing really attractive or desirable about the object at

42
00:02:26,430 --> 00:02:27,680
 all. We come to see

43
00:02:27,680 --> 00:02:31,210
 that it's something that's made up of component states that

44
00:02:31,210 --> 00:02:32,080
 arise and states

45
00:02:32,080 --> 00:02:37,920
 that come and go and that are in no way, shape or form

46
00:02:37,920 --> 00:02:40,640
 satisfying and so

47
00:02:40,640 --> 00:02:47,040
 we see that there's no reason to go after it. I would say

48
00:02:47,040 --> 00:02:48,920
 that I'm

49
00:02:48,920 --> 00:02:52,110
 worried that it's kind of a cop out here where people say,

50
00:02:52,110 --> 00:02:53,840
 "Well, you know, I may be

51
00:02:53,840 --> 00:02:57,840
 intoxicated but at least I'm meditating on it," and that's

52
00:02:57,840 --> 00:02:58,840
 sort of getting into

53
00:02:58,840 --> 00:03:02,130
 the idea that you can somehow meditate in an intoxicated

54
00:03:02,130 --> 00:03:03,280
 state and I want to make

55
00:03:03,280 --> 00:03:06,670
 it clear that the reason why we don't do drugs and take

56
00:03:06,670 --> 00:03:08,200
 alcohol is because the

57
00:03:08,200 --> 00:03:12,330
 state of mind that it creates is more or less the opposite

58
00:03:12,330 --> 00:03:13,640
 of the kind of states

59
00:03:13,640 --> 00:03:16,790
 of mind that we're trying to create which are clarity of

60
00:03:16,790 --> 00:03:17,840
 mind. I would say

61
00:03:17,840 --> 00:03:23,490
 that cannabis is not a very addictive drug and alcohol as

62
00:03:23,490 --> 00:03:24,640
 well, I think, is

63
00:03:24,640 --> 00:03:28,170
 probably not so addictive either physically, though some

64
00:03:28,170 --> 00:03:29,160
 people say it is

65
00:03:29,160 --> 00:03:32,820
 and there are certainly people who say it's genetic and so

66
00:03:32,820 --> 00:03:35,720
 on, but I would say,

67
00:03:35,720 --> 00:03:38,230
 you know, try to wake yourself up and say to yourself, "

68
00:03:38,230 --> 00:03:39,040
Look, this isn't

69
00:03:39,040 --> 00:03:43,200
 helping me. This isn't providing any benefit and it's

70
00:03:43,200 --> 00:03:44,120
 certainly not giving me

71
00:03:44,120 --> 00:03:48,460
 clarity of mind," and so giving it up and trying to find an

72
00:03:48,460 --> 00:03:50,360
 alternative that

73
00:03:50,360 --> 00:03:54,470
 brings clarity of mind, i.e. meditation. Okay, so I hope

74
00:03:54,470 --> 00:03:56,440
 that helps out. Thanks

