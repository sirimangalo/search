1
00:00:00,000 --> 00:00:02,000
 Noting technique Q&A.

2
00:00:02,000 --> 00:00:05,320
 Awareness or noting

3
00:00:05,320 --> 00:00:09,460
 question. Why is it not enough to just be aware of the

4
00:00:09,460 --> 00:00:12,720
 phenomena that arise without noting them?

5
00:00:12,720 --> 00:00:14,320
 Answer.

6
00:00:14,320 --> 00:00:17,870
 More important than what is enough is an answer to the

7
00:00:17,870 --> 00:00:19,840
 question what is better.

8
00:00:19,840 --> 00:00:24,560
 It may be enough to simply be aware of a phenomenon,

9
00:00:24,560 --> 00:00:28,810
 but it is better to apply mindfulness to it. In our

10
00:00:28,810 --> 00:00:29,640
 tradition,

11
00:00:30,240 --> 00:00:33,960
 mindfulness is defined as the grasping of the object as it

12
00:00:33,960 --> 00:00:34,440
 is.

13
00:00:34,440 --> 00:00:37,700
 A quality that may or may not be present in ordinary

14
00:00:37,700 --> 00:00:38,520
 awareness.

15
00:00:38,520 --> 00:00:42,480
 The real question is, without reminding oneself of the

16
00:00:42,480 --> 00:00:45,240
 objective nature of the phenomenon,

17
00:00:45,240 --> 00:00:47,800
 i.e. without noting,

18
00:00:47,800 --> 00:00:51,350
 how can one be sure one is observing the phenomenon

19
00:00:51,350 --> 00:00:52,600
 objectively?

20
00:00:52,600 --> 00:00:56,280
 Commonly, new meditators have a natural

21
00:00:56,840 --> 00:01:00,480
 explanation to note. It is more comfortable to allow one's

22
00:01:00,480 --> 00:01:02,360
 awareness to stand unimpeded,

23
00:01:02,360 --> 00:01:05,940
 rather than to train the mind in the precision of

24
00:01:05,940 --> 00:01:07,400
 identifying the object.

25
00:01:07,400 --> 00:01:10,800
 The question of which is better is open for debate,

26
00:01:10,800 --> 00:01:14,200
 but one should not let their own prejudice be the deciding

27
00:01:14,200 --> 00:01:15,560
 factor either way.

28
00:01:15,560 --> 00:01:19,840
 The essential quality of Vipassana meditation is sati.

29
00:01:19,840 --> 00:01:22,520
 Sati means to remember,

30
00:01:22,520 --> 00:01:25,520
 recollect, or remind oneself of something.

31
00:01:26,320 --> 00:01:30,460
 According to the Visuddhimagga, the proximate cause is T

32
00:01:30,460 --> 00:01:32,200
irthasana, which means

33
00:01:32,200 --> 00:01:34,760
 firm recognition.

34
00:01:34,760 --> 00:01:39,920
 Recognition, or sahana, is present in ordinary experience,

35
00:01:39,920 --> 00:01:39,920
 and

36
00:01:39,920 --> 00:01:43,620
 noting is understood to augment and stabilize this

37
00:01:43,620 --> 00:01:46,650
 recognition, preventing the mind from wavering in its

38
00:01:46,650 --> 00:01:48,160
 objective observation.

39
00:01:48,160 --> 00:01:51,410
 While reminding oneself of the nature of phenomena in this

40
00:01:51,410 --> 00:01:54,360
 way may not always be comfortable or feel natural,

41
00:01:55,000 --> 00:01:59,050
 it is understood to be more reliably objective. The mental

42
00:01:59,050 --> 00:02:02,400
 activity involved in producing a label for the object

43
00:02:02,400 --> 00:02:06,250
 prevents any alternative judgment or extrapolation of the

44
00:02:06,250 --> 00:02:07,000
 object.

45
00:02:07,000 --> 00:02:10,880
 Put another way, it is inevitable that the mind will give

46
00:02:10,880 --> 00:02:14,160
 rise to some sort of label for every experience anyway.

47
00:02:14,160 --> 00:02:18,470
 Noting is a means of ensuring that the label is objective.

48
00:02:18,470 --> 00:02:22,280
 If one experiences aversion or doubt about the technique,

49
00:02:22,600 --> 00:02:26,660
 it is enough to note these mind states. They are undeniably

50
00:02:26,660 --> 00:02:29,080
 hindrances to meditation practice,

51
00:02:29,080 --> 00:02:33,520
 whereas noting is not. If one is able to overcome one's

52
00:02:33,520 --> 00:02:35,480
 aversion and doubt by noting,

53
00:02:35,480 --> 00:02:38,440
 disliking, disliking, or

54
00:02:38,440 --> 00:02:40,800
 doubting, doubting,

55
00:02:40,800 --> 00:02:44,100
 it can be assured that one will become comfortable with it

56
00:02:44,100 --> 00:02:47,240
 and see the benefit in the noting technique for oneself.

57
00:02:47,360 --> 00:02:50,680
 If one is unable to do so, one is welcome to seek out other

58
00:02:50,680 --> 00:02:53,000
 techniques and other traditions.

59
00:02:53,000 --> 00:02:55,000
 [Silence]

