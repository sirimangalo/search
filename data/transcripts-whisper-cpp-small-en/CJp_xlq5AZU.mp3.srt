1
00:00:00,000 --> 00:00:06,680
 Good evening everyone.

2
00:00:06,680 --> 00:00:22,280
 We're broadcasting live May 18th 2016.

3
00:00:22,280 --> 00:00:31,680
 Tonight's quote is from the "Mrindapanha" and without

4
00:00:31,680 --> 00:00:37,400
 context it's simply a description

5
00:00:37,400 --> 00:00:55,960
 of a virtuous person.

6
00:00:55,960 --> 00:00:58,720
 Interesting to learn the context.

7
00:00:58,720 --> 00:01:03,180
 It's not necessary but it is interesting because the reason

8
00:01:03,180 --> 00:01:05,040
 why he...

9
00:01:05,040 --> 00:01:10,430
 The "Mrindapanha" is, if I haven't mentioned this before, a

10
00:01:10,430 --> 00:01:19,040
 very large text.

11
00:01:19,040 --> 00:01:27,300
 It's a sizable text with questions and answers between King

12
00:01:27,300 --> 00:01:32,040
 Melinda and Nagasena who is an

13
00:01:32,040 --> 00:01:33,040
 Arahant.

14
00:01:33,040 --> 00:01:40,820
 And it's really a great book for getting some insight into

15
00:01:40,820 --> 00:01:43,800
 the Buddha's stance on various

16
00:01:43,800 --> 00:01:52,730
 issues and on dilemmas which are presented by the text, by

17
00:01:52,730 --> 00:01:56,320
 the canonical text.

18
00:01:56,320 --> 00:02:01,050
 It's so valuable that the Burmese edition of the "Tipitika"

19
00:02:01,050 --> 00:02:03,320
 I understand includes the

20
00:02:03,320 --> 00:02:08,620
 "Mrindapanha" in the "Kudakanika" even though it's much

21
00:02:08,620 --> 00:02:09,520
 later.

22
00:02:09,520 --> 00:02:14,940
 But it's definitely worth studying, worth reading if you

23
00:02:14,940 --> 00:02:16,560
 have the time.

24
00:02:16,560 --> 00:02:30,160
 So this, the context here is discussing suicide.

25
00:02:30,160 --> 00:02:33,880
 One should not destroy oneself.

26
00:02:33,880 --> 00:02:45,910
 Whoever does so should be dealt with according to the Dham

27
00:02:45,910 --> 00:02:50,600
ma I think.

28
00:02:50,600 --> 00:02:55,000
 Which means if a monk kills himself there's a pen, I think

29
00:02:55,000 --> 00:02:56,560
 there's...

30
00:02:56,560 --> 00:02:59,480
 Actually I don't know.

31
00:02:59,480 --> 00:03:04,960
 It's interesting if he's dead then well, "yathada mukaretob

32
00:03:04,960 --> 00:03:05,600
u".

33
00:03:05,600 --> 00:03:12,380
 Think if someone tries to kill themselves then they're

34
00:03:12,380 --> 00:03:15,400
 guilty of an offense.

35
00:03:15,400 --> 00:03:19,090
 So as I understand, I'm reading the Pali and as usual my

36
00:03:19,090 --> 00:03:21,080
 Pali is not great but it looks

37
00:03:21,080 --> 00:03:25,800
 like Melinda if I remember correctly as well.

38
00:03:25,800 --> 00:03:32,820
 He says well the Buddha says this but he also says that Jad

39
00:03:32,820 --> 00:03:34,240
ya, Jaraya, Bhiyadino and so on.

40
00:03:34,240 --> 00:03:38,550
 He teaches the Dhamma for the Samu Chidaya, for the cutting

41
00:03:38,550 --> 00:03:40,880
 off of these things of birth,

42
00:03:40,880 --> 00:03:44,080
 old age, sickness and death.

43
00:03:44,080 --> 00:03:47,370
 So if he teaches the Dhamma for cutting off birth, old age,

44
00:03:47,370 --> 00:03:49,000
 sickness and death, what's

45
00:03:49,000 --> 00:03:50,540
 wrong with killing yourself?

46
00:03:50,540 --> 00:03:53,240
 Because if you kill yourself these things only last up

47
00:03:53,240 --> 00:03:55,120
 until death and if you kill yourself

48
00:03:55,120 --> 00:03:56,920
 then you don't have these things.

49
00:03:56,920 --> 00:04:01,630
 And I guess the idea is why wouldn't an Arahant, because an

50
00:04:01,630 --> 00:04:03,320
 ordinary person obviously it's

51
00:04:03,320 --> 00:04:06,860
 not an escape, if you kill yourself you're just going to be

52
00:04:06,860 --> 00:04:08,280
 born again, but why wouldn't

53
00:04:08,280 --> 00:04:12,400
 an Arahant do it?

54
00:04:12,400 --> 00:04:22,040
 I think that's what he's saying.

55
00:04:22,040 --> 00:04:25,360
 And then he says why does he teach both of these?

56
00:04:25,360 --> 00:04:26,760
 Why can these both be true?

57
00:04:26,760 --> 00:04:31,750
 If he teaches one why does he say it's wrong for an Arahant

58
00:04:31,750 --> 00:04:33,560
 to kill himself?

59
00:04:33,560 --> 00:04:40,130
 And then Nagasena says well this is because a Sīla Vā, a

60
00:04:40,130 --> 00:04:42,920
 virtuous person, Sīla sampan

61
00:04:42,920 --> 00:04:55,860
 no agadasammo satanam is like, and then he gives these ten

62
00:04:55,860 --> 00:04:57,480
 similes, one who is virtuous

63
00:04:57,480 --> 00:05:02,020
 is like an antidote for destroying the poisons of defile

64
00:05:02,020 --> 00:05:03,680
ments in beings.

65
00:05:03,680 --> 00:05:09,410
 So if you have this sickness of greed, anger and delusion,

66
00:05:09,410 --> 00:05:11,800
 what is the antidote?

67
00:05:11,800 --> 00:05:19,200
 Where do you find the cure to the defilements of the mind?

68
00:05:19,200 --> 00:05:22,670
 If you have an anger problem, if you have an addiction

69
00:05:22,670 --> 00:05:24,400
 problem, what is the cure?

70
00:05:24,400 --> 00:05:28,270
 The cure is a virtuous person and by this he means an Arah

71
00:05:28,270 --> 00:05:30,720
ant, someone who has understood

72
00:05:30,720 --> 00:05:35,900
 the truth and who has freed themselves from all greed,

73
00:05:35,900 --> 00:05:38,080
 anger and delusion.

74
00:05:38,080 --> 00:05:41,760
 You find such a person they are the antidote and if you

75
00:05:41,760 --> 00:05:44,280
 partake of them in terms of listening

76
00:05:44,280 --> 00:05:48,200
 to their teaching, associating with them, listening to

77
00:05:48,200 --> 00:05:50,440
 their teaching, remembering their

78
00:05:50,440 --> 00:05:53,880
 teaching, understanding their teaching and putting their

79
00:05:53,880 --> 00:05:55,480
 teaching into practice, you

80
00:05:55,480 --> 00:05:56,480
 can cure yourself.

81
00:05:56,480 --> 00:06:03,480
 They are the antidote.

82
00:06:03,480 --> 00:06:05,160
 That's the first one, there are ten similes.

83
00:06:05,160 --> 00:06:09,540
 The second is there a healing balm for laying the sickness

84
00:06:09,540 --> 00:06:11,840
 of defilements in beings.

85
00:06:11,840 --> 00:06:16,350
 So if you have the fever of defilements, defilements are

86
00:06:16,350 --> 00:06:19,320
 seen as a fever, greed is a fever, it

87
00:06:19,320 --> 00:06:25,250
 makes you hot and bothered, anger is a fever, it burns you

88
00:06:25,250 --> 00:06:28,320
 out, delusion is a fever, it

89
00:06:28,320 --> 00:06:38,520
 makes you drunk and uncontrollable.

90
00:06:38,520 --> 00:06:44,990
 But this is a healing balm that cools and soothes and heals

91
00:06:44,990 --> 00:06:47,200
 your sickness.

92
00:06:47,200 --> 00:06:52,690
 Number three is like a precious gem because they grant all

93
00:06:52,690 --> 00:06:55,060
 beings their wishes.

94
00:06:55,060 --> 00:07:01,180
 So there's this special gem in Indian mythology, a wish-ful

95
00:07:01,180 --> 00:07:02,760
filling gem.

96
00:07:02,760 --> 00:07:07,010
 So this comes up in Buddhism often as a simile talking

97
00:07:07,010 --> 00:07:10,040
 about the wish-fulfilling jewel.

98
00:07:10,040 --> 00:07:14,290
 This jewel if you have this, it's like the genie in his

99
00:07:14,290 --> 00:07:15,000
 lamp.

100
00:07:15,000 --> 00:07:18,020
 But in Indian mythology it's a gem and if you rub the gem

101
00:07:18,020 --> 00:07:19,880
 or if you possess the gem,

102
00:07:19,880 --> 00:07:24,120
 all your wishes come true.

103
00:07:24,120 --> 00:07:29,630
 And so an arahant, an enlightened being, a virtuous being

104
00:07:29,630 --> 00:07:32,340
 is like this gem because they

105
00:07:32,340 --> 00:07:37,080
 give people what they want, happiness.

106
00:07:37,080 --> 00:07:39,480
 Sometimes we don't even know what we want.

107
00:07:39,480 --> 00:07:42,640
 We think we want something and see the, an arahant

108
00:07:42,640 --> 00:07:45,000
 obviously can't give you riches and

109
00:07:45,000 --> 00:07:48,680
 so on and power, but they can give you what is, what you

110
00:07:48,680 --> 00:07:50,720
 need I guess you would say.

111
00:07:50,720 --> 00:07:56,360
 They can't give you what you want necessarily.

112
00:07:56,360 --> 00:07:59,040
 But what we want is always happiness.

113
00:07:59,040 --> 00:08:01,650
 We want certain things because we think they bring us

114
00:08:01,650 --> 00:08:02,400
 happiness.

115
00:08:02,400 --> 00:08:05,280
 We want satisfaction.

116
00:08:05,280 --> 00:08:08,080
 We're just deluded into thinking that things that are

117
00:08:08,080 --> 00:08:09,960
 unsatisfying are going to satisfy

118
00:08:09,960 --> 00:08:13,160
 us.

119
00:08:13,160 --> 00:08:15,570
 We're deluded into thinking that things that are imper

120
00:08:15,570 --> 00:08:17,480
manent, things that are uncontrollable

121
00:08:17,480 --> 00:08:24,480
 are stable and controllable.

122
00:08:24,480 --> 00:08:30,840
 The enlightened being when we follow their teachings, we

123
00:08:30,840 --> 00:08:32,640
 find what is stable, satisfying

124
00:08:32,640 --> 00:08:33,640
 and controllable.

125
00:08:33,640 --> 00:08:38,960
 Well, not controllable actually, put that one aside.

126
00:08:38,960 --> 00:08:43,560
 Stable and satisfying anyway.

127
00:08:43,560 --> 00:08:44,560
 That's number three.

128
00:08:44,560 --> 00:08:50,940
 No, number four is like a ship for beings to go beyond the

129
00:08:50,940 --> 00:08:52,800
 four floods.

130
00:08:52,800 --> 00:09:04,160
 So the floods are kamuga, bhavuga, rijoga.

131
00:09:04,160 --> 00:09:13,080
 I forget what are the four floods.

132
00:09:13,080 --> 00:09:17,890
 Becoming the defilements, overcoming the ajavah, ajvaja, aj

133
00:09:17,890 --> 00:09:18,840
vijoga.

134
00:09:18,840 --> 00:09:21,720
 I think that's it.

135
00:09:21,720 --> 00:09:27,010
 So kamma, sensuality, the flood of sensuality, bhavuga is

136
00:09:27,010 --> 00:09:32,360
 becoming the flood of becoming.

137
00:09:32,360 --> 00:09:42,440
 titoga, I think the third one is views, the flood of views.

138
00:09:42,440 --> 00:09:47,840
 And the vitoga is the flood of ignorance.

139
00:09:47,840 --> 00:09:52,970
 But in general, it's just another way of describing the def

140
00:09:52,970 --> 00:09:54,200
ilements.

141
00:09:54,200 --> 00:09:57,840
 But the idea of the imagery of the ocean is a common one.

142
00:09:57,840 --> 00:10:02,270
 Samsara is like an ocean that we all drown, we flounder

143
00:10:02,270 --> 00:10:06,200
 around in, never seeing the shore.

144
00:10:06,200 --> 00:10:11,030
 We're just lost in this vast ocean of nothingness and

145
00:10:11,030 --> 00:10:14,440
 meaninglessness until we find a ship that

146
00:10:14,440 --> 00:10:17,680
 can sail us.

147
00:10:17,680 --> 00:10:21,220
 As if you're just swimming around in the ocean, you just go

148
00:10:21,220 --> 00:10:24,360
 around in circles, you get nowhere.

149
00:10:24,360 --> 00:10:27,720
 But if you have a ship, you can cut through the ocean, you

150
00:10:27,720 --> 00:10:29,160
 can go in whatever direction

151
00:10:29,160 --> 00:10:32,240
 you like.

152
00:10:32,240 --> 00:10:36,920
 You can find the shore.

153
00:10:36,920 --> 00:10:40,650
 He or she is like a caravan leader for taking beings across

154
00:10:40,650 --> 00:10:42,800
 the desert of repeated births.

155
00:10:42,800 --> 00:10:46,520
 So rebirth is like a desert.

156
00:10:46,520 --> 00:10:50,020
 We're always thirsting, we're always hot with our defile

157
00:10:50,020 --> 00:10:51,800
ments and our addictions and our

158
00:10:51,800 --> 00:10:56,640
 immersions and the suffering that comes from old age

159
00:10:56,640 --> 00:10:58,760
 sickness and death.

160
00:10:58,760 --> 00:11:02,680
 We suffer like being in a desert.

161
00:11:02,680 --> 00:11:06,880
 But a caravan leader can lead us through these things.

162
00:11:06,880 --> 00:11:08,080
 He's like the wind here.

163
00:11:08,080 --> 00:11:11,890
 She's like the wind for extinguishing the three fierce

164
00:11:11,890 --> 00:11:14,360
 fires in beings as greed, anger,

165
00:11:14,360 --> 00:11:15,360
 and delusions.

166
00:11:15,360 --> 00:11:19,800
 The wind, they come in and they douse the fire.

167
00:11:19,800 --> 00:11:24,240
 They blow the fire out with the power of their teachings.

168
00:11:24,240 --> 00:11:29,060
 They're like a great rain cloud for filling beings with

169
00:11:29,060 --> 00:11:30,440
 good thoughts.

170
00:11:30,440 --> 00:11:36,750
 So like a great rain cloud fills up the lakes and rivers,

171
00:11:36,750 --> 00:11:39,840
 brings rain and waters the crops

172
00:11:39,840 --> 00:11:49,240
 and cools the land.

173
00:11:49,240 --> 00:11:53,310
 So too a great leader, a great teacher, a great being,

174
00:11:53,310 --> 00:11:56,120
 someone who has purified themselves.

175
00:11:56,120 --> 00:12:02,370
 This is someone who is enlightened and why they should stay

176
00:12:02,370 --> 00:12:06,960
 or stick around because they

177
00:12:06,960 --> 00:12:09,480
 have so much to offer.

178
00:12:09,480 --> 00:12:14,480
 The world is dry without enlightened beings.

179
00:12:14,480 --> 00:12:17,440
 Without them it's you can't grow anything.

180
00:12:17,440 --> 00:12:21,640
 People don't grow spiritually, not in any meaningful way.

181
00:12:21,640 --> 00:12:31,520
 Just go around in circles, dry up like crops without water.

182
00:12:31,520 --> 00:12:34,280
 Like a teacher for encouraging beings to train themselves

183
00:12:34,280 --> 00:12:35,360
 in what is skilled.

184
00:12:35,360 --> 00:12:39,670
 It's interesting that it's like a teacher, as opposed to

185
00:12:39,670 --> 00:12:41,520
 just being a teacher.

186
00:12:41,520 --> 00:12:47,020
 Acharya Samo, but it's an interesting because in many ways

187
00:12:47,020 --> 00:12:49,720
 the enlightened followers of

188
00:12:49,720 --> 00:12:52,200
 the Buddha aren't considered teachers.

189
00:12:52,200 --> 00:12:56,360
 We don't consider ourselves to be teachers per se.

190
00:12:56,360 --> 00:12:57,520
 We're friends.

191
00:12:57,520 --> 00:12:58,520
 We're good friends.

192
00:12:58,520 --> 00:12:59,520
 Why?

193
00:12:59,520 --> 00:13:02,000
 Because we give good things.

194
00:13:02,000 --> 00:13:10,480
 We are not we but we as in Buddhists who practice we.

195
00:13:10,480 --> 00:13:13,240
 We have good things to give.

196
00:13:13,240 --> 00:13:18,390
 And so I mean enlightened being an Arahant has the greatest

197
00:13:18,390 --> 00:13:19,440
 to give.

198
00:13:19,440 --> 00:13:23,380
 And yeah, so we don't consider, we should never even if one

199
00:13:23,380 --> 00:13:25,120
 becomes enlightened one

200
00:13:25,120 --> 00:13:28,720
 would not consider oneself a teacher but rather a friend,

201
00:13:28,720 --> 00:13:31,120
 someone who offers advice and passes

202
00:13:31,120 --> 00:13:33,520
 on the Buddhist teaching.

203
00:13:33,520 --> 00:13:37,280
 But they're like a teacher because they do the same.

204
00:13:37,280 --> 00:13:41,930
 They might as well be called a teacher because they

205
00:13:41,930 --> 00:13:45,560
 encourage beings to train themselves.

206
00:13:45,560 --> 00:13:49,850
 And finally like a good guide for pointing out to beings

207
00:13:49,850 --> 00:13:51,800
 the path to security.

208
00:13:51,800 --> 00:13:55,880
 So we're lost, we're so lost.

209
00:13:55,880 --> 00:13:57,480
 We're going in the wrong direction.

210
00:13:57,480 --> 00:14:04,560
 We're running around in circles.

211
00:14:04,560 --> 00:14:06,560
 Can't find the way out.

212
00:14:06,560 --> 00:14:09,710
 If you ever been in a large forest it's quite scary, easy

213
00:14:09,710 --> 00:14:11,560
 to get lost, easy to lose your

214
00:14:11,560 --> 00:14:17,480
 direction and you're constantly misjudging.

215
00:14:17,480 --> 00:14:19,680
 It's very easy to go around in circles because you think,

216
00:14:19,680 --> 00:14:20,800
 oh maybe I have to go a little

217
00:14:20,800 --> 00:14:23,560
 bit to the left and you constantly think that until you

218
00:14:23,560 --> 00:14:24,440
 wind up back.

219
00:14:24,440 --> 00:14:28,820
 I did this once, I wound up back where I started because I

220
00:14:28,820 --> 00:14:31,360
 actually walked in a circle.

221
00:14:31,360 --> 00:14:34,300
 It's very hard to get out of the forest if you don't know

222
00:14:34,300 --> 00:14:35,560
 where you're going.

223
00:14:35,560 --> 00:14:37,840
 If you've never been there before you'd run out of

224
00:14:37,840 --> 00:14:38,560
 direction.

225
00:14:38,560 --> 00:14:42,540
 If you don't have a guide but someone who has been through

226
00:14:42,540 --> 00:14:44,520
 the forest, who knows the

227
00:14:44,520 --> 00:14:53,850
 forest and who knows the way out of the forest can guide

228
00:14:53,850 --> 00:14:58,120
 you out of the forest.

229
00:14:58,120 --> 00:15:01,520
 So that's our dhamma for tonight.

230
00:15:01,520 --> 00:15:03,560
 Reasons why not to kill yourself.

231
00:15:03,560 --> 00:15:07,160
 It seems like killing yourself would be, you could more

232
00:15:07,160 --> 00:15:09,400
 easily explain it potentially by

233
00:15:09,400 --> 00:15:13,840
 just the idea that an enlightened being would have no

234
00:15:13,840 --> 00:15:16,320
 reason to kill themselves.

235
00:15:16,320 --> 00:15:21,800
 Killing yourself would require desire for it to all end.

236
00:15:21,800 --> 00:15:25,060
 So it seems like an arahant wouldn't have any desire to

237
00:15:25,060 --> 00:15:26,760
 stick around but they would

238
00:15:26,760 --> 00:15:34,880
 just stick around and wait, let things work themselves out.

239
00:15:34,880 --> 00:15:38,160
 But it seems like there's something a little more here,

240
00:15:38,160 --> 00:15:41,080
 like the idea that they do things.

241
00:15:41,080 --> 00:15:45,400
 They eat alms for example, why they don't just stop eating.

242
00:15:45,400 --> 00:15:51,660
 They take alms because of this, because of some sense of

243
00:15:51,660 --> 00:15:54,960
 duty or some rightness to passing

244
00:15:54,960 --> 00:15:59,400
 on what they have gained.

245
00:15:59,400 --> 00:16:04,030
 And it seems they do, arahants seem to teach and pass on

246
00:16:04,030 --> 00:16:06,680
 the teaching rather than just

247
00:16:06,680 --> 00:16:07,680
 kill themselves.

248
00:16:07,680 --> 00:16:12,590
 Hey, we got a whole bunch of people online, good to see,

249
00:16:12,590 --> 00:16:13,960
 good crowd.

250
00:16:13,960 --> 00:16:18,160
 Thank you all for showing up.

251
00:16:18,160 --> 00:16:22,160
 So it looks like the iOS app is still in the works.

252
00:16:22,160 --> 00:16:27,800
 I'm going to try to proactively revamp this website.

253
00:16:27,800 --> 00:16:31,520
 We've got a new version of it but I just haven't put any

254
00:16:31,520 --> 00:16:34,160
 effort into getting it online so I'm

255
00:16:34,160 --> 00:16:37,240
 going to work with the person.

256
00:16:37,240 --> 00:16:40,990
 But that also means probably that the iOS app has to be

257
00:16:40,990 --> 00:16:42,440
 fixed for the new server.

258
00:16:42,440 --> 00:16:43,440
 I'm not sure.

259
00:16:43,440 --> 00:16:48,150
 I'm going to have to work together to make this all and get

260
00:16:48,150 --> 00:16:49,960
 it secure as well.

261
00:16:49,960 --> 00:16:56,010
 Our whole server has to, apparently has to have some kind

262
00:16:56,010 --> 00:16:58,240
 of the HTTPS thing.

263
00:16:58,240 --> 00:17:03,290
 It's not, it's a bit beyond my paid grade but somebody

264
00:17:03,290 --> 00:17:04,600
 knows how to do it.

265
00:17:04,600 --> 00:17:08,680
 So we've got to all work together to make this happen.

266
00:17:08,680 --> 00:17:12,240
 If anybody wants to volunteer in our organization in any

267
00:17:12,240 --> 00:17:15,040
 way, we're always looking for volunteers,

268
00:17:15,040 --> 00:17:16,040
 I think.

269
00:17:16,040 --> 00:17:19,040
 I think we are.

270
00:17:19,040 --> 00:17:21,840
 Anyway, get in touch and if we need volunteers, we'll let

271
00:17:21,840 --> 00:17:22,560
 you know.

272
00:17:22,560 --> 00:17:28,010
 As usual, as per last night, we're still looking for a stew

273
00:17:28,010 --> 00:17:28,520
ard.

274
00:17:28,520 --> 00:17:35,940
 So if anybody wants to come and stay with us for a while,

275
00:17:35,940 --> 00:17:39,720
 starting in July, that'd be

276
00:17:39,720 --> 00:17:40,720
 great.

277
00:17:40,720 --> 00:17:43,720
 Let us know.

278
00:17:43,720 --> 00:17:48,880
 Right, so we've got a couple of questions.

279
00:17:48,880 --> 00:17:53,040
 Does anyone know what are the five skandhas?

280
00:17:53,040 --> 00:17:56,040
 We call them khandhas in Pali.

281
00:17:56,040 --> 00:18:02,160
 Khandhas are aggregates and these are rupa, about the form.

282
00:18:02,160 --> 00:18:05,200
 Vedana means feeling.

283
00:18:05,200 --> 00:18:09,760
 Sanya means recognition or so on.

284
00:18:09,760 --> 00:18:16,000
 Sankara is mental formations.

285
00:18:16,000 --> 00:18:19,800
 And vinyana is consciousness.

286
00:18:19,800 --> 00:18:23,320
 So when you see something, there is the physical aspect.

287
00:18:23,320 --> 00:18:27,400
 There's the vedana, the feeling about it, pleasant, painful

288
00:18:27,400 --> 00:18:28,280
, neutral.

289
00:18:28,280 --> 00:18:32,810
 There's a recognition, you recognize it as a cat or so on

290
00:18:32,810 --> 00:18:33,720
 that you see.

291
00:18:33,720 --> 00:18:36,940
 There's the mental formations, means you judge it or you

292
00:18:36,940 --> 00:18:38,760
 like it or dislike it or so on.

293
00:18:38,760 --> 00:18:42,480
 You examine it and you react to it.

294
00:18:42,480 --> 00:18:55,600
 And vinyana just means the consciousness, the fact that you

295
00:18:55,600 --> 00:18:58,480
're aware.

296
00:18:58,480 --> 00:19:04,080
 What does unsystematic attention mean?

297
00:19:04,080 --> 00:19:05,840
 Unsystematic is just a loose translation.

298
00:19:05,840 --> 00:19:09,170
 That's not really what the, I think that's re-referring to

299
00:19:09,170 --> 00:19:10,640
 ayoni somanasikara.

300
00:19:10,640 --> 00:19:13,640
 Ayoni somanasikara means wise, ayoni somanasan wise.

301
00:19:13,640 --> 00:19:19,690
 So when you observe something and you react to it, this is

302
00:19:19,690 --> 00:19:20,800
 when you like it or dislike

303
00:19:20,800 --> 00:19:25,720
 it, when you let yourself get caught up in reactions to it.

304
00:19:25,720 --> 00:19:29,440
 If you've yoni somanasikara, it means you see it wisely.

305
00:19:29,440 --> 00:19:31,740
 You see it as impermanent suffering and non-self.

306
00:19:31,740 --> 00:19:36,180
 You see it as it is permanent, unsatisfying, uncontrollable

307
00:19:36,180 --> 00:19:36,200
.

308
00:19:36,200 --> 00:19:37,200
 You don't get attached to it.

309
00:19:37,200 --> 00:19:40,670
 You see it objectively, just as something that arises and

310
00:19:40,670 --> 00:19:41,240
 sees.

311
00:19:41,240 --> 00:19:43,960
 That's wise attention.

312
00:19:43,960 --> 00:19:53,000
 But wise is probably better than systematic.

313
00:19:53,000 --> 00:19:56,480
 Are all fears instances of dosa?

314
00:19:56,480 --> 00:20:02,600
 Yeah, vinyana doesn't fear according to the Visuddhi manga.

315
00:20:02,600 --> 00:20:07,070
 Vinyana doesn't fear because fear is always associated with

316
00:20:07,070 --> 00:20:07,560
 patinga.

317
00:20:07,560 --> 00:20:08,560
 Actually it's not the Visuddhi manga.

318
00:20:08,560 --> 00:20:12,320
 I think it's the commentary or the sub-commentary to the

319
00:20:12,320 --> 00:20:14,920
 Visuddhi manga asks, "Does Baya Yana

320
00:20:14,920 --> 00:20:15,920
 fear?"

321
00:20:15,920 --> 00:20:19,440
 No, it doesn't.

322
00:20:19,440 --> 00:20:22,250
 I think maybe the Visuddhi manga is the one that says, "No,

323
00:20:22,250 --> 00:20:23,160
 it doesn't fear."

324
00:20:23,160 --> 00:20:26,950
 And then the commentary to the Visuddhi manga says, "Why

325
00:20:26,950 --> 00:20:28,360
 doesn't it fear?"

326
00:20:28,360 --> 00:20:32,370
 Because fear is always associated with patinga, with

327
00:20:32,370 --> 00:20:33,400
 aversion.

328
00:20:33,400 --> 00:20:34,800
 So it's not really fear.

329
00:20:34,800 --> 00:20:37,680
 Baya Yana isn't fear.

330
00:20:37,680 --> 00:20:40,400
 Baya Yana is seeing the danger.

331
00:20:40,400 --> 00:20:44,860
 When you see the danger in being reborn, you see the danger

332
00:20:44,860 --> 00:20:47,200
 in attachment, that kind of

333
00:20:47,200 --> 00:20:49,560
 thing.

334
00:20:49,560 --> 00:20:52,440
 It's like when you see fire off in the distance, this is

335
00:20:52,440 --> 00:20:54,360
 what the Visuddhi manga says, you

336
00:20:54,360 --> 00:20:57,020
 see fire off in the distance, you think, "Wow, if anyone

337
00:20:57,020 --> 00:20:58,680
 falls into that fire, they're going

338
00:20:58,680 --> 00:20:59,680
 to burn themselves."

339
00:20:59,680 --> 00:21:05,570
 But you yourself, you're not afraid because it's over there

340
00:21:05,570 --> 00:21:05,760
.

341
00:21:05,760 --> 00:21:07,640
 So the Yana itself isn't afraid.

342
00:21:07,640 --> 00:21:11,640
 But during meditation, of course, you can be afraid.

343
00:21:11,640 --> 00:21:19,680
 It's just that the fear isn't the Yana, the knowledge.

344
00:21:19,680 --> 00:21:23,050
 So for Canada, six months usually, usually you can come to

345
00:21:23,050 --> 00:21:24,760
 Canada for six months if you

346
00:21:24,760 --> 00:21:25,760
 want to extend it.

347
00:21:25,760 --> 00:21:31,150
 I think there are ways to extend it, but there's probably a

348
00:21:31,150 --> 00:21:33,920
 lot of paperwork involved.

349
00:21:33,920 --> 00:21:41,480
 I just got back from New York today.

350
00:21:41,480 --> 00:21:46,320
 We drove since morning.

351
00:21:46,320 --> 00:21:56,400
 So Saturday, Sunday, we've got something at Cambodian

352
00:21:56,400 --> 00:21:57,400
 monastery.

353
00:21:57,400 --> 00:22:01,640
 We might have another meditator tomorrow, we'll see.

354
00:22:01,640 --> 00:22:04,040
 But I'm also going to try to make it over to my father's

355
00:22:04,040 --> 00:22:04,520
 house.

356
00:22:04,520 --> 00:22:05,520
 We'll see how that works.

357
00:22:05,520 --> 00:22:08,120
 How am I going to work that?

358
00:22:08,120 --> 00:22:12,440
 I'll be around next weekend as ways up.

359
00:22:12,440 --> 00:22:16,960
 Don't forget, Mrs. Saga, if you're in the area, Saturday

360
00:22:16,960 --> 00:22:38,280
 the 28th, come on out.

361
00:22:38,280 --> 00:22:57,000
 Are there any other questions?

362
00:22:57,000 --> 00:22:59,970
 When a monk or an ant or so forth passes on, would they be

363
00:22:59,970 --> 00:23:02,680
 mindful like when falling asleep?

364
00:23:02,680 --> 00:23:07,980
 I.e. trying to be mindful of each moment of the moment of

365
00:23:07,980 --> 00:23:09,440
 transition.

366
00:23:09,440 --> 00:23:16,600
 On our hand, when they pass on, they aren't born again.

367
00:23:16,600 --> 00:23:20,240
 As for a monk, well, monks can be, of course, corrupt.

368
00:23:20,240 --> 00:23:28,720
 So doesn't say anything just because someone is a monk.

369
00:23:28,720 --> 00:23:31,410
 Let's ask if a meditator would be mindful, like when

370
00:23:31,410 --> 00:23:32,480
 falling asleep.

371
00:23:32,480 --> 00:23:35,290
 Not like when falling asleep, because death isn't like

372
00:23:35,290 --> 00:23:36,280
 falling asleep.

373
00:23:36,280 --> 00:23:39,640
 Death is like an out-of-body experience.

374
00:23:39,640 --> 00:23:46,180
 The body stops working, so the mind leaves the body, stops

375
00:23:46,180 --> 00:23:49,040
 working with the body.

376
00:23:49,040 --> 00:23:55,100
 Sometimes it takes a lot, but the mind eventually, over a

377
00:23:55,100 --> 00:23:58,800
 period of time, sees that the body

378
00:23:58,800 --> 00:24:02,770
 reacts to the fact that the body is no longer working

379
00:24:02,770 --> 00:24:05,920
 properly, and that's why it leaves.

380
00:24:05,920 --> 00:24:08,010
 So you have either a near-death experience if the body

381
00:24:08,010 --> 00:24:09,440
 starts working again, or you have

382
00:24:09,440 --> 00:24:12,130
 a death experience if the body doesn't ever stop, start

383
00:24:12,130 --> 00:24:13,080
 working again.

384
00:24:13,080 --> 00:24:18,100
 And then the mind goes on and does whatever, maybe back to

385
00:24:18,100 --> 00:24:19,640
 be born again.

386
00:24:19,640 --> 00:24:22,710
 But if you're mindful, it's just this process becomes a lot

387
00:24:22,710 --> 00:24:24,520
 smoother, and you're less likely

388
00:24:24,520 --> 00:24:30,400
 to make improper choices, as with everything in mind.

389
00:24:30,400 --> 00:24:34,840
 During the time of death, it's probably pretty quick.

390
00:24:34,840 --> 00:24:37,910
 It's a lot that goes on, and if you're not mindful, it's

391
00:24:37,910 --> 00:24:39,320
 easy to get lost and go the

392
00:24:39,320 --> 00:24:46,560
 wrong way.

393
00:24:46,560 --> 00:24:48,680
 The 20th here is actually wasted.

394
00:24:48,680 --> 00:24:53,080
 The 20th in Thailand, I think, is wasted, maybe the 24th

395
00:24:53,080 --> 00:24:54,600
 first some places.

396
00:24:54,600 --> 00:24:57,760
 The 28th is when we're having our celebration here.

397
00:24:57,760 --> 00:25:05,440
 It's just a big thing here in Canada, in Ontario.

398
00:25:05,440 --> 00:25:22,960
 Okay, well, I'm going to go.

399
00:25:22,960 --> 00:25:29,640
 It's been a bit of a long day, but I'm trying to...

400
00:25:29,640 --> 00:25:32,470
 Well, this next little while is going to be a little bit

401
00:25:32,470 --> 00:25:34,160
 erratic, but for the next week

402
00:25:34,160 --> 00:25:36,520
 or so, we should be fairly stable.

403
00:25:36,520 --> 00:25:41,040
 So, I'll be back again tomorrow, most likely.

404
00:25:41,040 --> 00:25:44,120
 Anyway, have a good night, everyone.

405
00:25:44,120 --> 00:25:45,240
 Be well.

406
00:25:45,240 --> 00:25:46,240
 Thank you.

407
00:25:46,240 --> 00:25:47,240
 Thank you.

408
00:25:47,240 --> 00:25:47,240
 Thank you.

409
00:25:47,240 --> 00:25:48,240
 Thank you.

410
00:25:48,240 --> 00:25:48,240
 Thank you.

411
00:25:48,240 --> 00:25:49,240
 Thank you.

412
00:25:49,240 --> 00:25:49,240
 Thank you.

413
00:25:49,240 --> 00:25:50,240
 Thank you.

