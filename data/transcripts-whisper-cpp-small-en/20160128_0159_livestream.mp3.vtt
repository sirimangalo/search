WEBVTT

00:00:00.000 --> 00:00:07.000
 Good evening everyone.

00:00:07.000 --> 00:00:19.000
 I'm broadcasting live January 27, 2016.

00:00:19.000 --> 00:00:35.000
 Tonight's quote talks about drops of water.

00:00:35.000 --> 00:00:39.000
 When I was young I always wondered how rivers worked.

00:00:39.000 --> 00:00:50.000
 I grew up in the forest. You go back and there'd be streams

00:00:50.000 --> 00:00:50.000
 in the forest.

00:00:50.000 --> 00:00:54.000
 You look at the streams or the rivers and flowers.

00:00:54.000 --> 00:01:07.000
 Where's all the water coming from?

00:01:07.000 --> 00:01:10.000
 This is the imagery the Buddha evokes.

00:01:10.000 --> 00:01:17.000
 In India they've got major rivers like the Ganga River,

00:01:17.000 --> 00:01:20.000
 Big River,

00:01:20.000 --> 00:01:24.000
 Long River.

00:01:24.000 --> 00:01:27.810
 And if you go up to the mountains you see you can find the

00:01:27.810 --> 00:01:33.000
 source of the river.

00:01:33.000 --> 00:01:42.000
 So the imagery here is of how rain falls on the mountain.

00:01:42.000 --> 00:01:45.000
 And it swells the rivers.

00:01:45.000 --> 00:01:50.000
 But eventually the rain from the top of the mountain

00:01:50.000 --> 00:02:04.350
 trickles down into the rivers and into the lakes and

00:02:04.350 --> 00:02:08.000
 eventually makes it back to the ocean.

00:02:08.000 --> 00:02:16.370
 And he uses this imagery to explain how the practice works

00:02:16.370 --> 00:02:21.000
 or encourage us in our practice.

00:02:21.000 --> 00:02:27.000
 Because practice often seems like drops of water.

00:02:27.000 --> 00:02:33.000
 When your stomach rises you say to yourself, "Rising."

00:02:33.000 --> 00:02:38.000
 If it falls you say, "Falling." It feels like drops in a

00:02:38.000 --> 00:02:38.000
 bucket.

00:02:38.000 --> 00:02:41.000
 What could that possibly do?

00:02:41.000 --> 00:02:44.200
 Maybe you have hope in the beginning but then as you

00:02:44.200 --> 00:02:47.000
 practice you see there's too much.

00:02:47.000 --> 00:02:52.000
 Too many bad habits and bad thoughts.

00:02:52.000 --> 00:02:55.000
 It's too difficult. There's too much.

00:02:55.000 --> 00:02:58.000
 There's no way. It's not enough.

00:02:58.000 --> 00:03:02.000
 These drops of moments of mindfulness.

00:03:02.000 --> 00:03:07.050
 I'll be mindful for moments but then for hours I'll be un

00:03:07.050 --> 00:03:09.000
mindful.

00:03:09.000 --> 00:03:16.350
 There's no way I can ever make it all the way to become

00:03:16.350 --> 00:03:17.000
 enlightened,

00:03:17.000 --> 00:03:20.000
 to free myself from these problems.

00:03:20.000 --> 00:03:25.000
 So far to go I'm never going to make it.

00:03:25.000 --> 00:03:31.590
 You know the old saying, "The journey of a thousand miles"

00:03:31.590 --> 00:03:34.000
 starts with one step.

00:03:34.000 --> 00:03:42.000
 And water, drops of water may seem minuscule on their own.

00:03:42.000 --> 00:03:57.650
 But when you gather them together they can flood an entire

00:03:57.650 --> 00:04:00.000
 city.

00:04:00.000 --> 00:04:05.000
 So it's these drops that actually are the path.

00:04:05.000 --> 00:04:11.000
 It's these drops that will actually get us to the goal.

00:04:11.000 --> 00:04:13.000
 Because everything's a habit.

00:04:13.000 --> 00:04:19.000
 Everything is stacked upon other things.

00:04:19.000 --> 00:04:24.000
 All of our behaviors, they don't come from nowhere.

00:04:24.000 --> 00:04:27.000
 They aren't intrinsic in us.

00:04:27.000 --> 00:04:31.000
 People say I'm an angry person or I'm a greedy person

00:04:31.000 --> 00:04:38.290
 or I have a chemical imbalance that makes me depressed or

00:04:38.290 --> 00:04:41.000
 bipolar or whatever.

00:04:41.000 --> 00:04:49.000
 None of these things are intrinsic to us.

00:04:49.000 --> 00:04:51.810
 Even our own brains, our own bodies, these are not

00:04:51.810 --> 00:04:53.000
 intrinsic to us.

00:04:53.000 --> 00:04:57.000
 We know that. We're going to lose them.

00:04:57.000 --> 00:05:00.000
 There was something artificial that we've cultivated.

00:05:00.000 --> 00:05:04.510
 It's part of this habit of cultivating, this habit of

00:05:04.510 --> 00:05:15.000
 collecting, this habit of building, creating.

00:05:15.000 --> 00:05:18.770
 And so how do we fix this? How do we solve this? How do we

00:05:18.770 --> 00:05:20.000
 change this?

00:05:20.000 --> 00:05:23.000
 Well we start building new habits.

00:05:23.000 --> 00:05:27.650
 We do the same thing when we do it in a different direction

00:05:27.650 --> 00:05:28.000
.

00:05:28.000 --> 00:05:33.740
 We build up habits of mindfulness, habits of focus, habits

00:05:33.740 --> 00:05:37.000
 of peace, habits of clarity,

00:05:37.000 --> 00:05:44.000
 habits of contentment, habits of wisdom.

00:05:44.000 --> 00:05:46.000
 And how do you build a habit?

00:05:46.000 --> 00:05:50.900
 You have to build it step by step, moment by moment, ret

00:05:50.900 --> 00:05:54.000
raining your mind.

00:05:54.000 --> 00:06:00.000
 At the same time all the old habits will come back.

00:06:00.000 --> 00:06:09.360
 But the point is to not ever let yourself forget that it's

00:06:09.360 --> 00:06:14.000
 the drops.

00:06:14.000 --> 00:06:18.000
 It's the moment they change you.

00:06:18.000 --> 00:06:25.000
 They don't disappear, they don't go away.

00:06:25.000 --> 00:06:29.000
 The thing about water is it goes in a cycle.

00:06:29.000 --> 00:06:32.210
 When you boil water, you say the water evaporated, but it

00:06:32.210 --> 00:06:33.000
 didn't disappear.

00:06:33.000 --> 00:06:38.000
 It goes into the atmosphere.

00:06:38.000 --> 00:06:42.000
 How good deeds do that as well.

00:06:42.000 --> 00:06:45.840
 They don't go anywhere. They don't disappear when you do a

00:06:45.840 --> 00:06:47.000
 good deed.

00:06:47.000 --> 00:06:51.050
 When you're mindful, when you meditate, every moment that

00:06:51.050 --> 00:06:52.000
 you're mindful

00:06:52.000 --> 00:06:55.900
 it feels like you put a drop in the bucket and it

00:06:55.900 --> 00:06:57.000
 disappears.

00:06:57.000 --> 00:07:01.930
 But eventually you look in the bucket and you're starting

00:07:01.930 --> 00:07:03.000
 to fill it up.

00:07:03.000 --> 00:07:08.000
 Eventually you fill it and it overflows.

00:07:08.000 --> 00:07:11.000
 This is what drops of water do.

00:07:11.000 --> 00:07:14.320
 When you're mindful it feels like you're just practiced for

00:07:14.320 --> 00:07:15.000
 an hour, now that's gone.

00:07:15.000 --> 00:07:19.000
 It feels like it's gone because you don't see it.

00:07:19.000 --> 00:07:22.000
 But you've changed the universe.

00:07:22.000 --> 00:07:26.860
 If you compare that hour you spent being mindful with an

00:07:26.860 --> 00:07:28.000
 hour spent not being mindful.

00:07:28.000 --> 00:07:31.040
 If you had not been mindful for that hour you would be in a

00:07:31.040 --> 00:07:33.000
 totally different state.

00:07:33.000 --> 00:07:35.000
 Your life would be on a different path.

00:07:35.000 --> 00:07:41.000
 You've changed your life path, you've changed the universe.

00:07:41.000 --> 00:07:45.160
 So if you practice day in and day out, moment after moment,

00:07:45.160 --> 00:07:52.000
 building up these drops of water.

00:07:52.000 --> 00:07:55.000
 The results don't disappear.

00:07:55.000 --> 00:07:59.990
 They just mix with all the other results, with all the

00:07:59.990 --> 00:08:01.000
 other effects

00:08:01.000 --> 00:08:06.000
 of all the other things that you've done and that you do.

00:08:06.000 --> 00:08:10.480
 So this is encouragement, the Buddha offers us

00:08:10.480 --> 00:08:12.000
 encouragement.

00:08:12.000 --> 00:08:16.000
 To not be discouraged by bad habits that you still have,

00:08:16.000 --> 00:08:21.000
 but be encouraged by the practices that you're cultivating

00:08:21.000 --> 00:08:24.000
 to build new habits, good habits.

00:08:24.000 --> 00:08:34.000
 Habits that lead to peace, habits that lead to happiness.

00:08:34.000 --> 00:08:38.000
 So there you have a quote.

00:08:38.000 --> 00:08:47.000
 You have a little bit of Dhamma for the evening.

00:08:47.000 --> 00:08:56.000
 As usual, you have a whole bunch of people meditating.

00:08:56.000 --> 00:08:59.000
 You've got a bunch of people listening.

00:08:59.000 --> 00:09:06.000
 Thank you all for tuning in.

00:09:06.000 --> 00:09:08.000
 See you all next time.

00:09:08.000 --> 00:09:09.000
 Good night.

