 For many years, I was a regular reader of the Reader's
 Digest magazine.
 I remember one cartoon in one of the issues of Reader's
 Digest magazine.
 It may be 30 years old.
 So that cartoon was of a picture of a religious minister
 about to give a religious talk to
 the congregation.
 And the picture shows him from the back.
 And under the podium, under the microphone stand, there was
 a bag of golf clubs.
 And the caption says, "Today I will be brief."
 So following that cartoon, I think I will be brief today.
 But I'm not going to play golf.
 I was searching for a subject to talk about tonight.
 And I did not know what to talk about.
 Unfortunately, I got some topic from the interviews with
 the yogis.
 So some yogis asked me some questions.
 And one yogi even asked me to give the answer to her
 question when I gave it to my talk.
 So today's talk will not be on one subject only, but some
 questions the yogis asked me
 to explain.
 The first thing I want to talk about is about equanimity.
 Now a yogi wants to know what equanimity is.
 Equanimity as used in Buddhist books is a neutrality,
 neutrality of mind.
 Neutrality means not falling to either side.
 So to stay in the middle and not to fall to either side.
 And that is called equanimity in the Buddhist teachings.
 And in the teachings of the Buddha, equanimity is a mental
 state.
 It is, many of you know, one of the 52 mental factors
 taught in Abhidhamma.
 And this is called specific neutrality.
 So specific neutrality is a name for equanimity consisting
 in the equal balance of the associated
 states.
 Now the mental states arise together.
 There are two mental states, consciousness and mental
 factors.
 So consciousness and mental factors always arise together.
 So when they arise together, they arise together and they
 do their respective functions.
 So for them to do their respective functions, probably
 there is one mental state called
 specific neutrality.
 So this keeps the associated states or cognizant mental
 states in equal balance.
 This specific neutrality takes many forms.
 Now when this specific neutrality arises in the mind of an
 Arahant, it is called equanimity
 with six factors.
 That means when an Arahant sees a visible object, he is
 neither glad nor sad about that object.
 So he is not moved by the quality of the object.
 So an Arahant can dwell in equanimity and be mindful and
 fully aware of the object, but
 he will not be clad or said when he sees the object.
 So the same with the other kinds of objects.
 There are six kinds of objects and that is why this equanim
ity is called equanimity with
 six factors.
 Actually equanimity regarding six kinds of objects.
 So whenever an Arahant sees or experiences any one of the
 six kinds of objects, he is
 able to keep his mind neutral, not liking, not disliking.
 So that kind of equanimity is called equanimity with six
 factors.
 When the equanimity or neutrality is practiced towards
 beings, then it is called a divine
 abiding of Brahma-Vihara.
 So you know there are four kinds of Brahma-Vihara's.
 Loving kindness, compassion, sympathetic joy and equanimity
.
 Equanimity is neutrality towards all beings.
 That means when a person practices equanimity as a divine
 abiding, even when he sees a being
 in suffering, he will not feel compassion.
 Even if he sees a person in prosperity, he will not feel
 happiness.
 So whether he sees a person in distress or in prosperity,
 his mind is always neutral.
 So that kind of neutrality is one form equanimity takes.
 And equanimity can take a form of factor of enlightenment.
 So equanimity is one of the seven factors of enlightenment.
 So there equanimity means again neutrality among the cogniz
ant or associated states.
 That means when the equanimity is developed to the level of
 enlightenment factor, again
 this mental state can keep other factors of enlightenment
 in proper balance and letting
 them do their own respective functions.
 And when this arises in a yogi, a yogi does not have to
 make effort, say to be mindful
 or to watch objects.
 So it is effortless awareness of the objects when this equ
animity is developed to the enlightenment
 factor.
 And when it arises with jhana, it is called equanimity of j
hana.
 Now you know there are different stages of jhana, the first
 jhana, second jhana and so
 on.
 In the fourth jhana, this factor is so developed that even
 in the bliss, even in the highest
 bliss of jhana, it is impartial.
 That means the equanimity is not partial to the bliss
 attained through jhana.
 The bliss attained through jhana is said to be very refined
 and sublime.
 But even in such bliss, a yogi is impartial. Even among
 these highest bliss, he is impartial.
 He will not like or dislike that kind of bliss or he will
 not be interested in that kind
 of bliss.
 So he tries to transcend bliss and enjoy what is called equ
animity.
 And it is said that this equanimity is on a higher level
 than what we call sukha or bliss.
 And when equanimity arises with the highest jhana, it is
 called equanimity regarding purifying.
 That means the highest jhana is the most purified of all j
hanas.
 And when a yogi has reached the highest, the most purified
 jhana, he has no interest in
 purifying it because it is already purified.
 So the equanimity arising together with that highest jhana
 is called equanimity about purifying.
 That means he is indifferent in purifying because it is
 already purified.
 So equanimity is neutrality, impartiality, and being unm
oved by the objects either desirable
 or undesirable.
 So just one equanimity is called six-factor equanimity, equ
animity as a divine abiding
 and so on, depending on where it arises.
 So this equanimity or neutrality yogis experience when
 their meditation is good, when they have
 a good concentration.
 Now some people want to know whether this is nibbana.
 This is not nibbana.
 Now in order to understand this, you need to understand
 what nibbana is.
 And it is very difficult to talk about nibbana because it
 cannot be described in the terms
 we are used to.
 Because when we explain nibbana, we use negative terms like
 cessation of dukkha, cessation
 of suffering, or extinction of greed, hatred, and delusion,
 and so on.
 Dukkha is, let us say, a state, a state that can be taken
 as object.
 But it is not a subject.
 That means it does not take any object.
 But it is the object which is taken by the other states,
 other mental states.
 And it is said that nibbana is not made, not created, and
 unborn, and not conditioned.
 That is why it is difficult to understand and difficult to
 describe.
 So what is not conditioned has no beginning.
 Only when something is made or something is conditioned, it
 has a beginning.
 But since nibbana is not made, not conditioned, it has no
 beginning.
 That is why it has no end.
 If it has beginning, it must have an end.
 Now, since it has no beginning, it has no end.
 And how can we perceive or think of something that has no
 beginning and no end, and still
 it is something that can be taken as object?
 So it is very difficult.
 In a Bhidama, nibbana is classified as an external object.
 So nibbana cannot be in the minds of beings.
 It is an external object that can be taken as object by
 some types of consciousness.
 And it is also said that nibbana is not a mental state.
 Nibbana is not made up.
 So no mental state, no matter, no other element, and so on.
 So it is a state of peacefulness or it is the extinction of
 mental defilement or extinction
 of five aggregates or extinction of suffering.
 As the extinction of suffering, it is a separate object
 that is taken as object by the path
 consciousness and fruition consciousness.
 When using English, we can say nibbana is not a mental
 state.
 But if we use Pali, it is different.
 So I don't want you to be confused, but I must say this.
 Now the word for mind in Pali is nama, and a-m-a.
 And that nama is defined as something that inclines towards
 the object.
 That is one definition.
 And the other definition is something that makes others to
 incline towards it.
 So the first definition is ordinary definition.
 And the second definition involves causative sense.
 So the first definition is something that inclines towards
 the object.
 That means that takes the object.
 And the second definition is something that makes others
 incline towards it.
 So there are two definitions of the word nama in Pali.
 Now there are consciousness, mental states, material,
 properties of matter and nibbana.
 Now consciousness can take objects.
 That means consciousness inclines towards the object.
 And so consciousness is a nama.
 And mental factors always arise with consciousness.
 And so along with consciousness, they take objects.
 So mental factors are also called nama.
 And consciousness, one consciousness can be the object of
 another consciousness.
 That means consciousness can make another consciousness
 incline towards it.
 So by both definitions, consciousness is a nama.
 By both definitions, the mental factors are nama.
 Now we come to matter.
 We can take matter as object.
 So that means our mind inclines towards the object.
 But matter cannot take an object itself.
 So matter is no nama.
 It does not take an object.
 But nibbana is called nama because it can make the
 consciousness incline towards it.
 That means it can be taken as object by consciousness and
 mental factors.
 So if we use English, we say nibbana is not a mental state.
 So it's plain.
 But if we use Pali, we say nibbana is also a nama.
 But it is not the same nama as the consciousness and mental
 factors.
 So consciousness and mental factors are called nama
 according to both definitions.
 But nibbana is called nama according to the second
 definition only.
 So nibbana is included in nama.
 But since nibbana is included in nama, we cannot say that
 nibbana is mental.
 So we should be careful about this.
 We should better not talk about nibbana as nama at all.
 Now there is a saying in one discourse that we can find all
 four noble truths in this
 fathom long body.
 That means we can find all four noble truths in this body.
 And nibbana is the third of the noble truth.
 But actually we are not to understand that nibbana is in us
.
 Nibbana is in our mind or in our body.
 But it is said that nibbana can be found in this body
 because nibbana can be taken as
 object by our mind when we get enlightenment.
 So since it can be taken as object by the mind which is
 internal to us, Buddha said
 that all these four noble truths can be found in this body.
 So pointing out to that discourse, we are not to say that
 nibbana can be in us.
 Nibbana is internal because abitama is very exact and
 strict and in abitama it is classified
 as an external object.
 Although nibbana is mostly defined in negative terms, it is
 not a negative state.
 It is a positive state.
 Because it is a positive state, it can be taken as object
 by path consciousness and
 fruition consciousness.
 Now what is health?
 No disease.
 So absence of disease is what we call health.
 But it is not just the absence of disease, it is something
 positive.
 So we cannot describe what health is, but we know and we
 experience health.
 So when we are healthy, we know that we have health and
 when we have diseases, we know
 that we have no health.
 So health is a positive state, but if we are asked to
 explain what health is, we may just
 say it is absence of diseases.
 So in the same way nibbana is a positive state and then we
 say nibbana is the extinction of
 suffering and so on.
 Now the other question is about chitana or volition.
 Now chitana or volition is also a mental state.
 It is a mental state that organizes the other mental concom
itants or that pushes or that
 urges the other mental concomitants to do their respective
 functions.
 And this chitana, although it is a mental state and it
 disappears immediately after it arises,
 it has the potential to give results in the future.
 So when it disappears, it leaves some kind of potential to
 give results in the continuity
 of a being.
 We do not know where this potential is stored, but when the
 conditions are met with, when
 the conditions are favorable, then the results are produced
.
 So that chitana that translated in English as volition is
 the one that produces results.
 And that chitana is what we call kama.
 So the word kama etymologically means a work, doing or a
 deed.
 But actually technically kama is this chitana.
 But whenever you do something, this chitana always arises.
 So this chitana is always connected with deeds or works.
 And so the deeds come to be called kama.
 So nowadays if you ask what is kama, you may say kama is a
 good or bad deed.
 But technically kama is not a good or bad deed, but the ch
itana or volition that arises
 in the mind when one does a good or bad deed.
 And that chitana has, as I said, the potential to give
 results in the future.
 It is taught in Abhidhamma that chitana accompanies every
 type of consciousness.
 But not chitana concomitant with every type of
 consciousness is called kama.
 Chitana that accompanies only the wholesome type of
 consciousness and unwholesome type
 of consciousness is called kama.
 Chitana can arise with the resultant consciousness.
 Chitana can arise with what are called functional
 consciousness.
 So when chitana arises with resultant consciousness, when
 it arises with functional consciousness,
 it is not called kama because it has no power to produce
 results.
 Only when it accompanies the wholesome consciousness or un
wholesome consciousness is it called
 kama.
 Only then can it produce results.
 And the results that arise are produced by chitana, the
 mental state, and not by the
 things, say, in any act of giving.
 And it is not the things that are offered that produce
 results.
 For example, you offer a set of rope to the sangha and
 according to the law of kama, you
 acquire kusala, wholesome kama when you offer a set of
 ropes to the sangha.
 And the chitana accompanying your consciousness is kama.
 And it is this chitana that produces results in the future
 and not the things you offer.
 A rope, a set of ropes cannot produce anything.
 It is the product.
 But what produces results is not the thing that you offer
 but the mental state that accompanies
 your consciousness when you make that offering.
 So chitana is described as the mental state that wills or
 that encourages other mental
 states to do their respective functions.
 Now people want to know if chitana encourages others who
 encourages chitana.
 Now chitana is compared to a chief pupil in the class.
 The chief pupil does his own work and at the same time he
 makes the other students work.
 So chitana encourages itself and it encourages other mental
 states.
 And another question is how consciousness arises in the
 next life.
 Now you all know that kama produces results.
 At the beginning of one life, especially of, let us say
 human beings, at the beginning
 of life as a human being, there is produced a type of
 consciousness or there arises a
 type of consciousness which is produced by the kama in the
 past.
 According to the teachings of Abhidhamma, consciousness
 needs a physical base to arise,
 especially for human beings and other beings like animals.
 So if there is no physical base, consciousness cannot arise
 in these beings.
 So at the moment of, at the first moment in the life of a
 human being, as I said, there
 is produced a type of consciousness.
 But along with that type of consciousness, some material
 properties are also produced.
 So the arising of some material properties and the arising
 of that particular consciousness
 is what we call rebirth.
 So rebirth consists of a type of consciousness and some
 material properties, both results
 of kama in the past.
 Since there are material properties in the first arising of
 matter in a new life, consciousness
 can arise.
 So the consciousness in the previous life ceases with death
.
 So no consciousness or no material properties move into the
 new life.
 The new life is a new life and the old life ends with death
.
 And then there is a new life arising of the consciousness
 and material properties as a
 result of kama that was presented in the past.
 So we are not to say how can consciousness arise, why after
 death the senses have decayed.
 So the consciousness that arises in the new life depends on
 a new material base in that
 new life.
 And it has nothing to do with the sense organs in the first
 life.
 Now a new life is a life that is newly produced.
 So let us call it a new life, second life and then the
 first life.
 So although the second life is a new life, it is not
 totally disconnected with the first
 life.
 It is the result of the kama in the past life.
 So as cause and effect they are related.
 They have some kind of relationship.
 But nothing from the old life or nothing from the first
 life moves into the second life.
 Although nothing goes from one life to another, there is a
 kind of relation or connection
 between the first life and the second life that is as cause
 and effect.
 So that is why the kama of this person gives results to
 this continuity only and not to
 another continuity.
 Otherwise there will be chaos in the law of kama.
 My kama giving results to you or your kama giving results
 to me.
 That cannot happen.
 And one thing important to note about death and rebirth is
 that rebirth is not the result
 of death.
 That is important.
 Many people wrote that death brings about rebirth or
 sometimes they may say that as
 a result of former life there is a new life.
 Although rebirth immediately follows death, rebirth is not
 the result of death.
 As you know rebirth is the result of kama.
 Kama in the past, not the result of death.
 But immediately after death there is rebirth.
 So we are never to say that rebirth is the result of death.
 But we can say that rebirth is conditioned by death in the
 sense that death disappears
 so that rebirth can arise.
 It is something like giving a place to another person.
 So I am sitting in this place and if I vacate this place
 then another person can sit in
 its place.
 So I am the condition for another person.
 So in that sense death is a condition for rebirth.
 But that does not mean that rebirth is caused by death.
 For rebirth is the product of death.
 So there are two different things.
 Death belongs to the old life and rebirth belongs to the
 new life.
 So we must never say that rebirth is the result of death.
 Death brings about rebirth.
 It just vacates its place so that rebirth can take place.
 Rebirth means rebirth consciousness and material properties
 along with it.
 If you want to know more about the process of death and
 rebirth you study abitama.
 So only in abitama the process of death and rebirth are
 explained in detail.
 And only when you have the knowledge of abitama do you
 understand correctly.
 Otherwise you may misunderstand or you may make mistakes.
 So if you want to know more about the process, please study
 abitama.
 Okay I thought I would be brief.
 That is why I told you about the cartoon.
 But now it is not brief.
 It is almost time.
 I mean regular time.
 Now one more not story but something about the elephants.
 In our countries they use elephants to haul logs right?
 So elephants are trained to push the logs and also they are
 trained to stop when they
 hear the siren, when they hear the bell.
 So it is said that elephants would stop as soon as they
 hear the bell.
 Even if there are about only six inches of the log to push
 they will not push it.
 They will stop dead at the sound of the siren or the bell.
 So today I will be like an elephant.
 Say sorry sorry sorry.
 Sorry sorry sorry.
 Sorry sorry sorry sorry sorry sorry sorry sorry sorry sorry
 sorry sorry sorry sorry
