1
00:00:00,000 --> 00:00:11,450
 Okay, so here we are broadcasting live and also studying in

2
00:00:11,450 --> 00:00:12,000
 Nirmapada.

3
00:00:12,000 --> 00:00:15,000
 I'm just going to record here for YouTube.

4
00:00:15,000 --> 00:00:33,000
 This will also be available in audio on our website.

5
00:00:33,000 --> 00:00:59,400
 Okay, hello and welcome back to our study of the Dhammapada

6
00:00:59,400 --> 00:01:01,000
.

7
00:01:01,000 --> 00:01:06,970
 Today we continue on with verse number 72, which reads as

8
00:01:06,970 --> 00:01:23,000
 follows.

9
00:01:23,000 --> 00:01:32,000
 Which means, "Ya Vadeva Anathaya"

10
00:01:32,000 --> 00:01:40,580
 "Ya Vadeva to the extent, to which extent, Anathaya, Nyat

11
00:01:40,580 --> 00:01:44,000
ang Balasat Jayati"

12
00:01:44,000 --> 00:01:51,660
 "Knowledge arises to the extent that knowledge arises in a

13
00:01:51,660 --> 00:01:53,000
 fool."

14
00:01:53,000 --> 00:02:01,930
 "Bala Anathaya to that extent it arises to that person's

15
00:02:01,930 --> 00:02:05,000
 disadvantage."

16
00:02:05,000 --> 00:02:15,000
 "So knowledge arises to the disadvantage of a fool."

17
00:02:15,000 --> 00:02:23,000
 "Hanti Bala Sukang Sangh, for it destroys, kills."

18
00:02:23,000 --> 00:02:27,000
 "He puts an end to their good luck, Sukang Sangh."

19
00:02:27,000 --> 00:02:35,000
 "Munda Masa Vibhatayang, it crushes his or her head."

20
00:02:35,000 --> 00:02:44,000
 "Crushes her head, his head, their head."

21
00:02:44,000 --> 00:02:49,000
 Very important verse for all of us to keep in mind.

22
00:02:49,000 --> 00:02:55,520
 This is the sort of verse that you can think of as

23
00:02:55,520 --> 00:03:05,000
 exemplary of the Buddha's teaching.

24
00:03:05,000 --> 00:03:11,000
 It is indicative of the nature of the Buddha's teaching.

25
00:03:11,000 --> 00:03:17,760
 It shows how the place he gave to knowledge is not being a

26
00:03:17,760 --> 00:03:23,000
 bad thing, but clearly not being enough.

27
00:03:23,000 --> 00:03:29,000
 Having its limitations and potential disadvantages.

28
00:03:29,000 --> 00:03:41,000
 What was this verse told in regards to? It seems that there

29
00:03:41,000 --> 00:03:41,000
 is a theme here.

30
00:03:41,000 --> 00:03:48,270
 As with the last verse, verse 71, this is told about Mogg

31
00:03:48,270 --> 00:03:52,000
allana and the things he saw on Vulture's Peak.

32
00:03:52,000 --> 00:03:56,110
 It seems that Moggallana was coming down from Vulture's

33
00:03:56,110 --> 00:03:57,000
 Peak again.

34
00:03:57,000 --> 00:04:06,540
 Again with the same monk, Lakhana, who was a good friend of

35
00:04:06,540 --> 00:04:08,000
 Moggallana's.

36
00:04:08,000 --> 00:04:10,000
 Again he smiled.

37
00:04:10,000 --> 00:04:13,000
 And again, Lakhana asked him, "What are you smiling about?"

38
00:04:13,000 --> 00:04:17,000
 And again, just as before, Moggallana refuses to answer and

39
00:04:17,000 --> 00:04:17,000
 says,

40
00:04:17,000 --> 00:04:23,000
 "Ask me again when we get in front of the Buddha."

41
00:04:23,000 --> 00:04:27,000
 And indeed, Lakhana does exactly that.

42
00:04:27,000 --> 00:04:33,250
 And again, the Buddha admits that he saw, and again Moggall

43
00:04:33,250 --> 00:04:35,000
ana tells him what he saw.

44
00:04:35,000 --> 00:04:47,310
 And in this case, he saw a ghost, a huge invisible ethereal

45
00:04:47,310 --> 00:04:49,000
 being.

46
00:04:49,000 --> 00:04:55,000
 And this time, having his head crushed by hammers.

47
00:04:55,000 --> 00:05:00,460
 So again and again hammers would crash upon his skull and

48
00:05:00,460 --> 00:05:02,000
 would burst.

49
00:05:02,000 --> 00:05:08,860
 And again and again his head would be rebuilt, and it would

50
00:05:08,860 --> 00:05:10,000
 rebuild itself.

51
00:05:10,000 --> 00:05:15,950
 And again, the hammers would crash down and repeat in this

52
00:05:15,950 --> 00:05:17,000
 thing.

53
00:05:17,000 --> 00:05:20,240
 And he said, "I smiled because I've never seen such a being

54
00:05:20,240 --> 00:05:21,000
 before."

55
00:05:21,000 --> 00:05:28,000
 This is what the text says, so it's an interesting thing.

56
00:05:28,000 --> 00:05:30,530
 And then the Buddha says that he saw such a being but didn

57
00:05:30,530 --> 00:05:32,000
't want to say anything

58
00:05:32,000 --> 00:05:36,530
 because people would, if he didn't have a witness, people

59
00:05:36,530 --> 00:05:38,000
 wouldn't believe him.

60
00:05:38,000 --> 00:05:40,550
 And I think such is the case nowadays when I tell these

61
00:05:40,550 --> 00:05:41,000
 stories,

62
00:05:41,000 --> 00:05:48,840
 I risk, we risk people disbelieving it, feeling that such a

63
00:05:48,840 --> 00:05:50,000
 being could not exist.

64
00:05:50,000 --> 00:05:53,000
 So we have a bit of a problem here.

65
00:05:53,000 --> 00:05:59,000
 I'd like people to focus not on details.

66
00:05:59,000 --> 00:06:07,000
 You shouldn't focus on facts, historical facts.

67
00:06:07,000 --> 00:06:09,510
 Did this happen? Did that not happen? It's not really

68
00:06:09,510 --> 00:06:10,000
 important.

69
00:06:10,000 --> 00:06:13,000
 Does such a being exist? Does such a being not exist?

70
00:06:13,000 --> 00:06:17,740
 It's more important to understand the theory and obviously

71
00:06:17,740 --> 00:06:19,000
 more important to understand

72
00:06:19,000 --> 00:06:25,000
 this verse which doesn't require the story to be true.

73
00:06:25,000 --> 00:06:28,340
 On the other hand, it would be of course much to our

74
00:06:28,340 --> 00:06:31,000
 advantage if we could understand

75
00:06:31,000 --> 00:06:34,000
 the mechanics behind such a being.

76
00:06:34,000 --> 00:06:39,210
 Such a being can arise and be clear in our minds that there

77
00:06:39,210 --> 00:06:40,000
's also the potential for us

78
00:06:40,000 --> 00:06:44,860
 to be born as a huge hulking being having our head crushed

79
00:06:44,860 --> 00:06:46,000
 by hammers.

80
00:06:46,000 --> 00:06:50,000
 Apparently there is such a potential.

81
00:06:50,000 --> 00:06:54,700
 Even if we can't do that, we can't bring our minds around

82
00:06:54,700 --> 00:06:56,000
 the fact that

83
00:06:56,000 --> 00:07:00,000
 beings of different nature and ourselves exist

84
00:07:00,000 --> 00:07:07,000
 and the potential for the mind to create existence that is

85
00:07:07,000 --> 00:07:09,000
 undetectable

86
00:07:09,000 --> 00:07:16,480
 or seemingly undetectable by physical means, ordinary

87
00:07:16,480 --> 00:07:21,000
 physical means.

88
00:07:21,000 --> 00:07:24,620
 If we can't, we shouldn't let it get in the way of our

89
00:07:24,620 --> 00:07:25,000
 practice

90
00:07:25,000 --> 00:07:30,000
 and our appreciation of the knowledge in this verse.

91
00:07:30,000 --> 00:07:37,000
 If you can, well, there's always a benefit there.

92
00:07:37,000 --> 00:07:44,230
 The benefit would be that you can then have faith and

93
00:07:44,230 --> 00:07:48,000
 confidence in your practice.

94
00:07:48,000 --> 00:07:52,280
 It gives you encouragement in your practice to avoid such

95
00:07:52,280 --> 00:07:53,000
 things.

96
00:07:53,000 --> 00:07:56,000
 Nobody wants to be born such a being.

97
00:07:56,000 --> 00:08:01,160
 Anyway, then the monks were all like asking the Buddha how

98
00:08:01,160 --> 00:08:02,000
 such a thing could occur,

99
00:08:02,000 --> 00:08:04,000
 how such a being could exist.

100
00:08:04,000 --> 00:08:09,000
 What did such a being do to obtain such an existence?

101
00:08:09,000 --> 00:08:13,000
 Then the Buddha told the story of the past.

102
00:08:13,000 --> 00:08:19,000
 So it seems there was once a cripple.

103
00:08:19,000 --> 00:08:25,490
 This cripple was very good in one thing and that was using

104
00:08:25,490 --> 00:08:27,000
 a sling.

105
00:08:27,000 --> 00:08:36,000
 So he had this sling and he would shoot stones at these big

106
00:08:36,000 --> 00:08:38,000
 leaves,

107
00:08:38,000 --> 00:08:40,000
 whatever big leaves there were.

108
00:08:40,000 --> 00:08:44,000
 He could cut the leaves, which is by slinging stones.

109
00:08:44,000 --> 00:08:47,110
 He was such a sharp shooter that he could cut the leaves in

110
00:08:47,110 --> 00:08:48,000
 a certain pattern.

111
00:08:48,000 --> 00:08:55,000
 So just for fun he would cut them into shapes.

112
00:08:55,000 --> 00:08:58,000
 He did this to amuse the children.

113
00:08:58,000 --> 00:09:00,670
 The children would follow him around and ask him to make an

114
00:09:00,670 --> 00:09:01,000
 elephant

115
00:09:01,000 --> 00:09:07,000
 or ask him to make a bird or so on and he would oblige.

116
00:09:07,000 --> 00:09:12,740
 One day they were in the king's garden and they were

117
00:09:12,740 --> 00:09:15,000
 telling him that he was making all these shapes

118
00:09:15,000 --> 00:09:19,000
 and cutting the leaves out in various patterns.

119
00:09:19,000 --> 00:09:24,280
 When all of a sudden the king came to the garden and the

120
00:09:24,280 --> 00:09:25,000
 children ran away

121
00:09:25,000 --> 00:09:28,000
 and left the cripple behind.

122
00:09:28,000 --> 00:09:35,000
 The king found out and the king went to rest in the shade

123
00:09:35,000 --> 00:09:38,000
 and found that the shade was patched.

124
00:09:38,000 --> 00:09:41,400
 That the shade wasn't complete and he looked up and he saw

125
00:09:41,400 --> 00:09:42,000
 these shapes

126
00:09:42,000 --> 00:09:46,000
 and he said, "What is this? Where is this happening?

127
00:09:46,000 --> 00:09:48,000
 How is this happening?"

128
00:09:48,000 --> 00:09:51,000
 He inquired and found out that there was this cripple.

129
00:09:51,000 --> 00:09:55,000
 He thought, "Wow, I could use such a person."

130
00:09:55,000 --> 00:10:00,000
 He asked him, "Would you be able to shoot these pebbles?"

131
00:10:00,000 --> 00:10:03,790
 Speaking of sharp shooting, do you think you'd be able to

132
00:10:03,790 --> 00:10:08,000
 shoot these pebbles into someone's mouth?

133
00:10:08,000 --> 00:10:12,300
 The cripple said, "Yeah, I could do that." He said, "I'd

134
00:10:12,300 --> 00:10:16,000
 like you to shoot a pint pot of horse dung, cow dung,

135
00:10:16,000 --> 00:10:20,000
 into someone's mouth for me."

136
00:10:20,000 --> 00:10:24,310
 It turns out the king had an advisor who couldn't keep his

137
00:10:24,310 --> 00:10:25,000
 mouth shut

138
00:10:25,000 --> 00:10:29,000
 and would just lather on about anything and everything.

139
00:10:29,000 --> 00:10:34,160
 The king was annoyed by this and thought they would teach

140
00:10:34,160 --> 00:10:37,000
 this guy a lesson.

141
00:10:37,000 --> 00:10:41,000
 The cripple said, "Yes, I could do that."

142
00:10:41,000 --> 00:10:46,000
 The king took him to the court and hid him behind a curtain

143
00:10:46,000 --> 00:10:47,000
.

144
00:10:47,000 --> 00:10:49,000
 The cripple poked a little hole in it.

145
00:10:49,000 --> 00:10:52,000
 Every time this advisor opened his mouth,

146
00:10:52,000 --> 00:10:56,470
 he would shoot a little pellet of dung into the guy's mouth

147
00:10:56,470 --> 00:11:00,000
 and shut him up.

148
00:11:00,000 --> 00:11:04,000
 Apparently, he got away with this until he completely

149
00:11:04,000 --> 00:11:12,000
 got a whole pint pot full of dung into the guy's mouth.

150
00:11:12,000 --> 00:11:17,000
 The king then turned to the advisor and said,

151
00:11:17,000 --> 00:11:22,000
 "You're such a blather mouth. I can't take it anymore."

152
00:11:22,000 --> 00:11:26,000
 He said, "All this time you've been sitting there,

153
00:11:26,000 --> 00:11:29,000
 and even when I couldn't even keep your mouth shut,

154
00:11:29,000 --> 00:11:34,500
 even when we were shooting pellets of dung into your mouth

155
00:11:34,500 --> 00:11:35,000
."

156
00:11:35,000 --> 00:11:39,000
 It's bizarre, really, I didn't read this story.

157
00:11:39,000 --> 00:11:42,000
 It's quite interesting.

158
00:11:42,000 --> 00:11:45,000
 This is the story that's been passed down.

159
00:11:45,000 --> 00:11:48,000
 It's a bizarre sort of story, I think.

160
00:11:48,000 --> 00:11:50,000
 It's an interesting way to...

161
00:11:50,000 --> 00:11:52,000
 But it's a harmless way, I suppose.

162
00:11:52,000 --> 00:11:54,000
 They're not hurting the guy.

163
00:11:54,000 --> 00:12:01,000
 So it is kind of an ingenious way to get your point across.

164
00:12:01,000 --> 00:12:08,390
 It is said that the advisor never was very careful to open

165
00:12:08,390 --> 00:12:14,000
 his mouth in the future.

166
00:12:14,000 --> 00:12:20,000
 But the point is, then this cripple became quite wealthy.

167
00:12:20,000 --> 00:12:26,000
 The king showered honor and gain upon him.

168
00:12:26,000 --> 00:12:32,000
 Our story comes in, our verse comes in when...

169
00:12:32,000 --> 00:12:35,000
 The story of our verse comes in when another man saw this

170
00:12:35,000 --> 00:12:38,000
 and was quite impressed and thought,

171
00:12:38,000 --> 00:12:42,210
 "Wow, if I could learn that skill, I could probably gain

172
00:12:42,210 --> 00:12:48,000
 favor and gain and fame as well."

173
00:12:48,000 --> 00:12:52,400
 So here's the problem, when someone does something for the

174
00:12:52,400 --> 00:12:57,000
 purpose of fame and gain.

175
00:12:57,000 --> 00:13:06,000
 So his desire was already unwholesome, his desire to learn.

176
00:13:06,000 --> 00:13:09,480
 So he approaches this cripple and the cripple doesn't want

177
00:13:09,480 --> 00:13:10,000
 to teach him.

178
00:13:10,000 --> 00:13:16,330
 He persuades him to teach him, finally he says, "Fine, I'll

179
00:13:16,330 --> 00:13:19,000
 teach you what I know."

180
00:13:19,000 --> 00:13:27,000
 And the cripple, he does this, he butters up the cripple

181
00:13:27,000 --> 00:13:29,000
 and takes care of him

182
00:13:29,000 --> 00:13:32,930
 and acts as his servant for a while until finally the cripp

183
00:13:32,930 --> 00:13:33,000
le says,

184
00:13:33,000 --> 00:13:34,000
 "Fine, I'll teach you what I know."

185
00:13:34,000 --> 00:13:37,700
 And so he teaches him how to use this link until the man

186
00:13:37,700 --> 00:13:39,000
 gets quite good at it.

187
00:13:39,000 --> 00:13:45,740
 And so the short of it is that we have this man who's

188
00:13:45,740 --> 00:13:48,000
 learned how to use a sling

189
00:13:48,000 --> 00:13:52,000
 and is keen to show off.

190
00:13:52,000 --> 00:13:56,000
 And so the cripple says, "So how are you going to test?

191
00:13:56,000 --> 00:13:58,000
 What are you going to do with this skill?"

192
00:13:58,000 --> 00:14:01,970
 And he says, "Well, I figure I'll find something to shoot

193
00:14:01,970 --> 00:14:02,000
 at

194
00:14:02,000 --> 00:14:05,000
 so that people can know what a sharpshooter I am.

195
00:14:05,000 --> 00:14:08,570
 I'll figure I'll shoot at a cow or maybe I'll shoot at a

196
00:14:08,570 --> 00:14:10,000
 man somewhere."

197
00:14:10,000 --> 00:14:12,700
 And the cripple says, "What do you talk, you know, you can

198
00:14:12,700 --> 00:14:14,000
't just go around

199
00:14:14,000 --> 00:14:16,000
 shooting things and living beings.

200
00:14:16,000 --> 00:14:19,000
 If you shoot a cow, you get fined 500 gold pieces.

201
00:14:19,000 --> 00:14:22,700
 If you shoot a human, you get fined a thousand gold pieces

202
00:14:22,700 --> 00:14:25,000
," or something like that.

203
00:14:25,000 --> 00:14:29,490
 He says, "What you have to do is you have to find something

204
00:14:29,490 --> 00:14:30,000
 to shoot at

205
00:14:30,000 --> 00:14:36,000
 that nobody's going to give you a miss that has no father,

206
00:14:36,000 --> 00:14:40,000
 no mother, nobody to complain about."

207
00:14:40,000 --> 00:14:45,000
 So maybe some of you can tell where this is going.

208
00:14:45,000 --> 00:14:49,000
 He says, "Good idea. I'll find something like that."

209
00:14:49,000 --> 00:14:52,950
 So he looks around and he sees a cow and he says, "No, that

210
00:14:52,950 --> 00:14:54,000
 has an owner."

211
00:14:54,000 --> 00:14:58,000
 And the owner is going to be concerned about it.

212
00:14:58,000 --> 00:15:01,000
 And he looks and he sees a man and he says, "Yeah, right.

213
00:15:01,000 --> 00:15:04,380
 That's true. This man has a mother and a father and a wife

214
00:15:04,380 --> 00:15:07,000
 and children and not a good idea."

215
00:15:07,000 --> 00:15:11,000
 And he looks around, he sees a child, he sees all these...

216
00:15:11,000 --> 00:15:17,000
 Until finally he sees a Pacheka Buddha.

217
00:15:17,000 --> 00:15:20,000
 And he says, "This man."

218
00:15:20,000 --> 00:15:22,770
 He sees him going on alms round and it's amazing how he

219
00:15:22,770 --> 00:15:24,000
 could just miss the fact that

220
00:15:24,000 --> 00:15:27,480
 this is an enlightened being, but that's the case with

221
00:15:27,480 --> 00:15:29,000
 people who are so blinded by

222
00:15:29,000 --> 00:15:35,000
 their own desires and attachments.

223
00:15:35,000 --> 00:15:42,950
 And he says, "This man. No father, no mother, no wife, no

224
00:15:42,950 --> 00:15:44,000
 children.

225
00:15:44,000 --> 00:15:53,000
 This man is the perfect target. No one will miss him."

226
00:15:53,000 --> 00:15:58,460
 And so he takes this thing and he shoots the Pacheka Buddha

227
00:15:58,460 --> 00:16:05,000
 right in the head and the Pacheka Buddha dies.

228
00:16:05,000 --> 00:16:08,000
 He says, "Oh, I have a good shot with that."

229
00:16:08,000 --> 00:16:15,990
 And the people of the village, of course, are very much

230
00:16:15,990 --> 00:16:18,000
 attached to the Pacheka Buddha

231
00:16:18,000 --> 00:16:20,440
 and they find out he hasn't come for alms and so they go

232
00:16:20,440 --> 00:16:21,000
 and look for him

233
00:16:21,000 --> 00:16:25,180
 and they find that he's passed away, he's been killed and

234
00:16:25,180 --> 00:16:26,000
 murdered.

235
00:16:26,000 --> 00:16:31,670
 And this silly person comes up and says, "Yeah, that was me

236
00:16:31,670 --> 00:16:32,000
. I was...

237
00:16:32,000 --> 00:16:40,000
 Look at what a sharpshooter I am killing this guy."

238
00:16:40,000 --> 00:16:44,680
 The story is that the text says that he was able to shoot a

239
00:16:44,680 --> 00:16:48,000
 stone in one ear and out the other ear.

240
00:16:48,000 --> 00:16:53,820
 So according to the text that I guess we can agree that's

241
00:16:53,820 --> 00:16:59,000
 quite a feat.

242
00:16:59,000 --> 00:17:05,000
 And yeah, his bragging has the... doesn't quite have the

243
00:17:05,000 --> 00:17:06,000
 expected result.

244
00:17:06,000 --> 00:17:10,270
 They lynch him and I think they beat him and kill him and

245
00:17:10,270 --> 00:17:13,000
 he ends up going to hell as a result.

246
00:17:13,000 --> 00:17:22,480
 And once he's finished his stint in hell, he is reborn as a

247
00:17:22,480 --> 00:17:25,000
 ghost on Vulture Speak

248
00:17:25,000 --> 00:17:31,000
 and has hammers crashing down upon his skull.

249
00:17:31,000 --> 00:17:40,000
 And the Buddha says, "So you see, yawadeva anataya."

250
00:17:40,000 --> 00:17:45,000
 Knowledge arises in a fool to his or her disadvantage.

251
00:17:45,000 --> 00:17:54,000
 It destroys their good luck. It crushes their head.

252
00:17:54,000 --> 00:17:57,150
 The text, the verse is obviously metaphorical and so you

253
00:17:57,150 --> 00:17:59,000
 don't need the story of a ghost having his head crushed,

254
00:17:59,000 --> 00:18:03,900
 but that's the reference there. It actually can physically

255
00:18:03,900 --> 00:18:07,000
 crush your head if you're not careful.

256
00:18:07,000 --> 00:18:11,000
 You'll be born with hammers crushing your head.

257
00:18:11,000 --> 00:18:14,070
 But this isn't the point of the verse. The point of the

258
00:18:14,070 --> 00:18:21,030
 verse is that knowledge in the wrong hands isn't useful at

259
00:18:21,030 --> 00:18:22,000
 all.

260
00:18:22,000 --> 00:18:25,530
 We use it to our detriment. It's important in a worldly

261
00:18:25,530 --> 00:18:26,000
 sense.

262
00:18:26,000 --> 00:18:36,330
 This is an important philosophy, an important ethical point

263
00:18:36,330 --> 00:18:39,000
 that knowledge is ethically variable.

264
00:18:39,000 --> 00:18:43,470
 It's not always a good thing to have knowledge. More

265
00:18:43,470 --> 00:18:47,000
 knowledge isn't always a good thing.

266
00:18:47,000 --> 00:18:51,000
 And the variable is whether someone is wise or not.

267
00:18:51,000 --> 00:18:54,340
 Wisdom and intelligence are obviously two very different

268
00:18:54,340 --> 00:18:55,000
 things.

269
00:18:55,000 --> 00:19:00,190
 You can have wise people with very little knowledge or

270
00:19:00,190 --> 00:19:02,000
 intelligence.

271
00:19:02,000 --> 00:19:06,410
 In fact, you could argue that to some extent they're in

272
00:19:06,410 --> 00:19:08,000
versely related.

273
00:19:08,000 --> 00:19:13,990
 The more intelligence a person has, the harder it is for

274
00:19:13,990 --> 00:19:16,000
 them to be wise.

275
00:19:16,000 --> 00:19:19,000
 It's easier to become conceited about your knowledge.

276
00:19:19,000 --> 00:19:22,800
 It's easy to become attached to the gain that comes from

277
00:19:22,800 --> 00:19:24,000
 your knowledge.

278
00:19:24,000 --> 00:19:30,810
 And so even you find that professors of religion and even

279
00:19:30,810 --> 00:19:33,000
 professors who study Buddhism

280
00:19:33,000 --> 00:19:36,110
 go all the way to get their PhD and even become teachers in

281
00:19:36,110 --> 00:19:37,000
 their own right

282
00:19:37,000 --> 00:19:43,920
 can be very silly at times and have strange and bizarre

283
00:19:43,920 --> 00:19:47,000
 views and beliefs.

284
00:19:47,000 --> 00:19:52,880
 And obviously in a worldly sense, it's even more clear how

285
00:19:52,880 --> 00:19:56,000
 knowledge in the wrong hands can lead to suffering,

286
00:19:56,000 --> 00:20:06,110
 the knowledge of nuclear, what do they call it, reactivity,

287
00:20:06,110 --> 00:20:10,000
 whatever it is that led them to create the atom bomb.

288
00:20:10,000 --> 00:20:15,970
 Nuclear power could be used for benefit, could be used for

289
00:20:15,970 --> 00:20:17,000
 detriment.

290
00:20:17,000 --> 00:20:21,920
 In fact, even nuclear power itself, as we can see, has the

291
00:20:21,920 --> 00:20:27,000
 potential to cause great suffering.

292
00:20:27,000 --> 00:20:32,000
 Knowledge is not always a good thing.

293
00:20:32,000 --> 00:20:36,230
 Much of our knowledge even today, as we can see now, is

294
00:20:36,230 --> 00:20:39,000
 because of our lack of wisdom.

295
00:20:39,000 --> 00:20:50,650
 We're using it to gain wealth and prosperity in the present

296
00:20:50,650 --> 00:20:53,000
 moment at the expense of our future,

297
00:20:53,000 --> 00:20:58,700
 as it seems with the change of the climate based on human

298
00:20:58,700 --> 00:21:01,000
 activity and so on.

299
00:21:01,000 --> 00:21:04,710
 Knowledge in fact seems to be one of those, one thing that

300
00:21:04,710 --> 00:21:08,000
's set to kill us before we had engrossed ourselves.

301
00:21:08,000 --> 00:21:15,680
 We were never in danger of destroying our environment and

302
00:21:15,680 --> 00:21:23,000
 the environment on which we depend.

303
00:21:23,000 --> 00:21:25,730
 It's an interesting example. The same goes with nuclear

304
00:21:25,730 --> 00:21:33,480
 power, we were never at risk of destroying our civilization

305
00:21:33,480 --> 00:21:34,000
.

306
00:21:34,000 --> 00:21:39,290
 So knowledge is power and power of course is ethically

307
00:21:39,290 --> 00:21:44,000
 variable, can be beneficial,

308
00:21:44,000 --> 00:21:49,190
 can be used to create great wholesomeness, can be used to

309
00:21:49,190 --> 00:21:54,000
 create great evil as well.

310
00:21:54,000 --> 00:21:57,180
 But on the meditation side of things, there's also a point

311
00:21:57,180 --> 00:22:00,000
 to be made here that knowledge of meditation

312
00:22:00,000 --> 00:22:05,000
 can sometimes be to our detriment. If you know too much, it

313
00:22:05,000 --> 00:22:07,000
 can actually get in the way of your practice.

314
00:22:07,000 --> 00:22:12,000
 Certainly it doesn't necessarily help you practice.

315
00:22:12,000 --> 00:22:16,210
 Knowledge accompanied by practice on the other hand, again

316
00:22:16,210 --> 00:22:18,000
 the power of knowledge can be a great thing

317
00:22:18,000 --> 00:22:23,360
 if you're actually practicing. To have knowledge of, of

318
00:22:23,360 --> 00:22:25,000
 course, the correct way to practice,

319
00:22:25,000 --> 00:22:31,250
 to have knowledge of background, Buddhist theory, the abhid

320
00:22:31,250 --> 00:22:34,000
hamma, the nature of the mind,

321
00:22:34,000 --> 00:22:38,000
 wholesomeness, even knowledge of these stories.

322
00:22:38,000 --> 00:22:44,210
 If a person grasps them in the wrong way and becomes

323
00:22:44,210 --> 00:22:47,400
 skeptical, thinking such things could never happen, could

324
00:22:47,400 --> 00:22:48,000
 never exist,

325
00:22:48,000 --> 00:22:52,260
 how is it possible for such a ghost being to be born? To

326
00:22:52,260 --> 00:22:57,000
 focus on that aspect of it and become full of doubt.

327
00:22:57,000 --> 00:23:02,470
 If they don't have the experience to see how such a thing

328
00:23:02,470 --> 00:23:08,000
 is possible, how the mind can create such an existence,

329
00:23:08,000 --> 00:23:11,180
 then it will actually create doubt in the mind and be a

330
00:23:11,180 --> 00:23:13,000
 detriment to the practice.

331
00:23:13,000 --> 00:23:16,690
 On the other hand, one grasps it correctly and sees the

332
00:23:16,690 --> 00:23:18,000
 message behind it,

333
00:23:18,000 --> 00:23:23,140
 and moreover is able, even better, is able to understand

334
00:23:23,140 --> 00:23:26,000
 how such a thing could come about,

335
00:23:26,000 --> 00:23:30,930
 be able to see how the mind works and experience how it's

336
00:23:30,930 --> 00:23:34,000
 able to create such an existence.

337
00:23:34,000 --> 00:23:38,860
 Then it can be to one's advantage, reminding one of the

338
00:23:38,860 --> 00:23:46,250
 potential for suffering for those who are bent upon the

339
00:23:46,250 --> 00:23:49,000
 wrong path and unenholsomeness.

340
00:23:49,000 --> 00:23:54,300
 So it's just, this is just one more lesson for us to keep

341
00:23:54,300 --> 00:23:55,000
 in mind,

342
00:23:55,000 --> 00:23:58,000
 that knowledge and wisdom are two very different things.

343
00:23:58,000 --> 00:24:02,310
 Simply studying the Buddha's teaching, even listening to

344
00:24:02,310 --> 00:24:06,560
 watching these videos and reading about the Buddha's

345
00:24:06,560 --> 00:24:07,000
 teaching,

346
00:24:07,000 --> 00:24:11,660
 this isn't enough, it isn't a substitute for actually

347
00:24:11,660 --> 00:24:15,000
 practicing the Buddha's teaching.

348
00:24:15,000 --> 00:24:21,250
 So, more food for thought from the Buddha, and it's taught

349
00:24:21,250 --> 00:24:23,000
 in the Dhammapada.

350
00:24:24,000 --> 00:24:28,350
 So that's all for today. Thank you all for tuning in and

351
00:24:28,350 --> 00:24:30,000
 keep practicing.

352
00:24:30,000 --> 00:24:45,000
 [ Sighs ]

