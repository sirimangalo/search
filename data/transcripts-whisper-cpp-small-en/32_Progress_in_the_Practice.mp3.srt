1
00:00:00,000 --> 00:00:05,120
 Meditation, progress, questions and answers.

2
00:00:05,120 --> 00:00:07,920
 Progress in the practice.

3
00:00:07,920 --> 00:00:13,440
 How does one know if one is progressing in the practice?

4
00:00:13,440 --> 00:00:15,520
 Progress is a tricky thing.

5
00:00:15,520 --> 00:00:19,920
 In a sense, the whole idea of progress is misleading.

6
00:00:19,920 --> 00:00:22,480
 Where are you trying to progress to?

7
00:00:22,480 --> 00:00:24,640
 What are you trying to achieve?

8
00:00:24,640 --> 00:00:29,120
 Progress in meditation is about giving up and letting go.

9
00:00:29,120 --> 00:00:32,240
 Not becoming or obtaining something new.

10
00:00:32,240 --> 00:00:36,000
 Any concern that you are not getting anything out of

11
00:00:36,000 --> 00:00:36,960
 meditation

12
00:00:36,960 --> 00:00:42,560
 should be noted objectively and discarded.

13
00:00:42,560 --> 00:00:47,520
 How should my meditation progress after the initial stages

14
00:00:47,520 --> 00:00:48,960
 of concentration

15
00:00:48,960 --> 00:00:52,080
 of the breath and being witness to a clear thought?

16
00:00:52,080 --> 00:00:56,000
 And when will I know I have arrived?

17
00:00:56,000 --> 00:00:59,360
 Meditation is a long and gradual path.

18
00:00:59,360 --> 00:01:03,200
 Gradually, we see that there is nothing we could hold on to

19
00:01:03,200 --> 00:01:03,200
.

20
00:01:03,200 --> 00:01:07,840
 Cling to or try to shape and mould into the perfect reality

21
00:01:07,840 --> 00:01:09,280
 that would make us happy.

22
00:01:09,280 --> 00:01:10,560
 And so we let go.

23
00:01:10,560 --> 00:01:14,430
 The clear thought of mindfulness practice helps you to let

24
00:01:14,430 --> 00:01:14,720
 go

25
00:01:14,720 --> 00:01:19,120
 and be here and now without wanting to be somewhere else

26
00:01:19,120 --> 00:01:21,440
 or something else than what you are.

27
00:01:21,440 --> 00:01:24,960
 When you create a clear thought, you will see clearly.

28
00:01:24,960 --> 00:01:28,950
 If you practice correctly, you will see and learn things

29
00:01:28,950 --> 00:01:30,000
 about yourself

30
00:01:30,000 --> 00:01:32,000
 that you did not know before.

31
00:01:32,000 --> 00:01:35,710
 You will come to see things about yourself that you could

32
00:01:35,710 --> 00:01:36,800
 not see before

33
00:01:36,800 --> 00:01:40,530
 which will help you to deal with all of the challenges in

34
00:01:40,530 --> 00:01:40,960
 life

35
00:01:40,960 --> 00:01:44,720
 in a more rational, wise, and productive way.

36
00:01:44,720 --> 00:01:49,520
 In meditation practice, you will encounter many hindrances.

37
00:01:49,520 --> 00:01:52,920
 Some hindrances will make you think that you have hit a

38
00:01:52,920 --> 00:01:53,760
 roadblock.

39
00:01:53,760 --> 00:01:59,390
 States of liking, disliking, boredom, worry, depression,

40
00:01:59,390 --> 00:01:59,920
 etc.

41
00:01:59,920 --> 00:02:03,520
 Your mind will change constantly as you practice

42
00:02:03,520 --> 00:02:06,160
 and not every change will be positive.

43
00:02:06,160 --> 00:02:08,480
 The mind is always changing.

44
00:02:08,480 --> 00:02:11,520
 You might think that when you practice, you are just going

45
00:02:11,520 --> 00:02:13,440
 to get happier and happier

46
00:02:13,440 --> 00:02:17,570
 but mindfulness forces you to face both the positive and

47
00:02:17,570 --> 00:02:20,240
 the negative inside of yourself.

48
00:02:20,240 --> 00:02:24,340
 All sorts of negative mind states can arise during

49
00:02:24,340 --> 00:02:26,080
 meditation practice

50
00:02:26,080 --> 00:02:30,080
 and you should work to catch them and to apply mindfulness

51
00:02:30,080 --> 00:02:32,560
 to each one as it arises.

52
00:02:32,560 --> 00:02:36,500
 Progress in the path is working through these negative

53
00:02:36,500 --> 00:02:36,960
 states

54
00:02:36,960 --> 00:02:40,910
 until they are no longer triggered in the ways they used to

55
00:02:40,910 --> 00:02:41,840
 be triggered.

56
00:02:41,840 --> 00:02:45,440
 The best answer for when you have arrived at the goal

57
00:02:45,440 --> 00:02:49,790
 is that you have no more potential for greed, anger, or

58
00:02:49,790 --> 00:02:51,280
 delusion to arise.

59
00:02:51,280 --> 00:02:54,480
 When you just see things as they are, when you are

60
00:02:54,480 --> 00:02:57,520
 conscious regardless of how things are

61
00:02:57,520 --> 00:03:01,470
 and when nothing has the potential to trigger stress or

62
00:03:01,470 --> 00:03:03,280
 suffering in your mind,

63
00:03:03,280 --> 00:03:06,160
 that is when you've reached the goal.

64
00:03:06,160 --> 00:03:10,070
 Every time you sit down, you should learn something new

65
00:03:10,070 --> 00:03:11,120
 about reality.

66
00:03:11,120 --> 00:03:14,830
 If you are practicing and never learning anything, no

67
00:03:14,830 --> 00:03:16,000
 matter how small,

68
00:03:16,000 --> 00:03:20,000
 then I would say that maybe you are doing something wrong.

69
00:03:20,000 --> 00:03:23,610
 There are four things that we intend to learn from the

70
00:03:23,610 --> 00:03:24,560
 practice.

71
00:03:24,560 --> 00:03:28,320
 The first thing that you should learn from the practice

72
00:03:28,320 --> 00:03:33,580
 is the nature of experiential reality, both inside and in

73
00:03:33,580 --> 00:03:35,360
 the world around you.

74
00:03:35,360 --> 00:03:40,000
 You come to learn that reality is made up of saying,

75
00:03:40,000 --> 00:03:41,040
 hearing,

76
00:03:41,040 --> 00:03:45,280
 smelling, tasting, feeling, and thinking.

77
00:03:45,280 --> 00:03:49,850
 And that these in turn are composed of physical and mental

78
00:03:49,850 --> 00:03:52,240
 moments of experience.

79
00:03:52,240 --> 00:03:56,390
 The second thing that you learn is impermanence, suffering

80
00:03:56,390 --> 00:03:57,360
 and non-so.

81
00:03:57,360 --> 00:04:01,390
 You will see that the things you cling to are not worth

82
00:04:01,390 --> 00:04:02,080
 clinging to.

83
00:04:02,080 --> 00:04:06,720
 They are impermanent, unsatisfying, and uncontrollable,

84
00:04:06,720 --> 00:04:10,440
 leading you to weaken your grasp on the things you

85
00:04:10,440 --> 00:04:11,680
 experience.

86
00:04:11,680 --> 00:04:16,190
 Finally, your learning will culminate in realization of the

87
00:04:16,190 --> 00:04:17,520
 four noble truths,

88
00:04:17,520 --> 00:04:20,800
 specifically the noble path and the noble fruit.

89
00:04:20,800 --> 00:04:25,120
 The path is the eradication of certain defilements on

90
00:04:25,120 --> 00:04:27,920
 reaching the path of Sotapana,

91
00:04:27,920 --> 00:04:31,520
 Sakadagami, Anagami, and Arahant.

92
00:04:31,520 --> 00:04:36,480
 The fruition is the experience of freedom from suffering

93
00:04:36,480 --> 00:04:38,880
 caused by those defilements.

94
00:04:38,880 --> 00:04:42,800
 Once you realize the third and fourth knowledges,

95
00:04:42,800 --> 00:04:47,280
 questions about progress do not have the same weight

96
00:04:47,280 --> 00:04:50,400
 because you already know what the goal is.

97
00:04:50,400 --> 00:04:54,760
 Your only work from then on is to attain these knowledges

98
00:04:54,760 --> 00:04:55,920
 again and again,

99
00:04:55,920 --> 00:04:58,880
 weakening your defilements more and more.

100
00:04:58,880 --> 00:05:02,640
 Until realizing these super mundane knowledges,

101
00:05:02,640 --> 00:05:05,840
 you should look to understand your experiences,

102
00:05:05,840 --> 00:05:09,740
 seeing them as body and mind that they are not worth

103
00:05:09,740 --> 00:05:10,560
 clinging to

104
00:05:10,560 --> 00:05:13,120
 and how clinging leads to suffering.

105
00:05:13,120 --> 00:05:17,760
 Seeing these things is a sign of progress in the practice.

