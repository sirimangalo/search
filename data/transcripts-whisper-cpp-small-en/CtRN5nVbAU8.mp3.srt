1
00:00:00,000 --> 00:00:11,400
 Good evening, everyone. Welcome to our live broadcast. It

2
00:00:11,400 --> 00:00:15,960
 seems to me that a discussion

3
00:00:15,960 --> 00:00:21,630
 about not broadcasting every night was understood to

4
00:00:21,630 --> 00:00:26,600
 actually be a decision. It wasn't a decision.

5
00:00:26,600 --> 00:00:32,990
 I'm still going to do daily broadcasts, at least for the

6
00:00:32,990 --> 00:00:35,840
 near future. If any change comes,

7
00:00:35,840 --> 00:00:44,000
 I'll let you all know. But I think nightly broadcasts is

8
00:00:44,000 --> 00:00:46,400
 fine. Just as long as you're

9
00:00:46,400 --> 00:00:50,730
 all fine with some of them being short. Sometimes I'll just

10
00:00:50,730 --> 00:00:53,920
 come on and say good evening and give

11
00:00:53,920 --> 00:00:57,070
 some brief teaching. Or some nights maybe just answer

12
00:00:57,070 --> 00:00:59,400
 questions. It's very easy for me to sit

13
00:00:59,400 --> 00:01:02,120
 here and answer questions to actually come up with

14
00:01:02,120 --> 00:01:04,480
 something to talk about. Well, some nights it

15
00:01:04,480 --> 00:01:13,400
 might be, might just have other things on my plate. Anyway,

16
00:01:13,400 --> 00:01:15,560
 tonight we are looking at

17
00:01:15,560 --> 00:01:25,160
 Anguttarnikaya, Book of Three is number 49 or 50. I don't

18
00:01:25,160 --> 00:01:28,680
 know. The English says 49,

19
00:01:28,680 --> 00:01:33,450
 the Pali says 50. So if you're following along with Bhikkh

20
00:01:33,450 --> 00:01:35,840
obodi, it's number 49 in the Book of Three.

21
00:01:35,840 --> 00:01:46,190
 Ardur, or as the Americans say, Ardur, without view. It

22
00:01:46,190 --> 00:01:49,280
 looks funny, it's because I'm not American.

23
00:01:49,280 --> 00:01:56,480
 Ardur, Ardur is an interesting word. It'd be nice to have a

24
00:01:56,480 --> 00:01:58,600
 different one because Ardur

25
00:01:58,600 --> 00:02:03,450
 has different connotations in English, but it's the best

26
00:02:03,450 --> 00:02:08,080
 because the word atapi or atapi or

27
00:02:08,080 --> 00:02:18,560
 atapa, right? Atapa. Atapa comes from temperature. That's

28
00:02:18,560 --> 00:02:21,440
 the same root as the English word

29
00:02:21,440 --> 00:02:33,160
 temperature. Atapa comes from atapa. Atapya, which becomes

30
00:02:33,160 --> 00:02:41,240
 atapa. So the idea is in India,

31
00:02:41,240 --> 00:02:47,410
 exertion was seen as, was described as being energetic. So

32
00:02:47,410 --> 00:02:50,040
 in English we have that with Ardur,

33
00:02:50,040 --> 00:02:53,380
 right? But we don't use it exactly in the same way. We don

34
00:02:53,380 --> 00:02:55,880
't think of work as, or effort as,

35
00:02:55,880 --> 00:02:59,670
 as getting hot about something. I think of getting hot

36
00:02:59,670 --> 00:03:03,640
 about something or heated up is either to do

37
00:03:03,640 --> 00:03:08,920
 with lust or anger, which is not exactly what, not at all

38
00:03:08,920 --> 00:03:11,800
 what the Buddha is referring to,

39
00:03:11,800 --> 00:03:18,280
 and not how it's used in Pali. But Ardur can also mean

40
00:03:18,280 --> 00:03:23,640
 energy or energetic effort. So the meaning

41
00:03:23,640 --> 00:03:30,380
 here is atapangarniyam is what the Buddha says. There are

42
00:03:30,380 --> 00:03:36,760
 three, three, three things or three

43
00:03:36,760 --> 00:03:44,030
 subjects, three topics on which one should cultivate atapa.

44
00:03:44,030 --> 00:03:46,920
 Atapangarniyam. Gharaniyam

45
00:03:46,920 --> 00:03:54,040
 means should be done. Atapa is Ardur or effort. And I think

46
00:03:54,040 --> 00:03:59,520
 this is a fairly interesting teaching.

47
00:03:59,520 --> 00:04:07,890
 I mean the Buddha taught different reactions to the various

48
00:04:07,890 --> 00:04:12,120
 problems that we have, or challenges

49
00:04:12,120 --> 00:04:16,000
 that we're facing, the various aspects of the cause of

50
00:04:16,000 --> 00:04:18,800
 suffering. So in certain instances,

51
00:04:18,800 --> 00:04:23,300
 one should exert oneself. In certain instances, one should

52
00:04:23,300 --> 00:04:27,200
 restrain oneself. In certain instances,

53
00:04:27,200 --> 00:04:38,020
 one should continue. One should not push or not, not

54
00:04:38,020 --> 00:04:43,120
 retreat. But one, in certain cases,

55
00:04:43,120 --> 00:04:47,580
 one should simply continue the way one is going. And in

56
00:04:47,580 --> 00:04:51,200
 certain cases, one should cultivate

57
00:04:51,200 --> 00:04:58,500
 dispassion. So sort of not give, take out the zeal for

58
00:04:58,500 --> 00:05:02,800
 something. So not exactly restraint,

59
00:05:02,800 --> 00:05:07,810
 but abandonment, I guess. Anyway, different ways of looking

60
00:05:07,810 --> 00:05:10,640
 at how you deal with defilements.

61
00:05:10,640 --> 00:05:14,450
 And I think that's a valid thing to say, because here, this

62
00:05:14,450 --> 00:05:16,800
 only tells part of the story. It tells

63
00:05:16,800 --> 00:05:21,790
 the part of the story in these three things where the

64
00:05:21,790 --> 00:05:25,360
 salient quality or the outstanding quality is

65
00:05:25,360 --> 00:05:30,100
 effort. And so whether that means anything or not, but it

66
00:05:30,100 --> 00:05:33,200
 appears that these three are especially

67
00:05:33,200 --> 00:05:44,340
 relating to exertion. Because actually there are four great

68
00:05:44,340 --> 00:05:45,760
 exertions.

69
00:05:46,720 --> 00:05:51,030
 But here only two are mentioned. So the Buddha says, in

70
00:05:51,030 --> 00:05:54,160
 regards to unerasing evil states,

71
00:05:54,160 --> 00:06:01,760
 one should cultivate effort for their non-arising. Simple

72
00:06:01,760 --> 00:06:07,520
 enough. If they haven't arisen, one should

73
00:06:07,520 --> 00:06:11,530
 work hard to make sure they don't arise. For unerasing

74
00:06:11,530 --> 00:06:14,640
 wholesome dhammas, one should work hard

75
00:06:14,640 --> 00:06:25,200
 for their arising. And one should work hard for cultivating

76
00:06:25,200 --> 00:06:34,160
 adiwasana, which means

77
00:06:35,760 --> 00:06:41,850
 bearing with. Wasana means to live or to dwell, and adi

78
00:06:41,850 --> 00:06:44,400
 means on top of or with, you know,

79
00:06:44,400 --> 00:06:53,440
 while they're existing. One should cultivate forbearance or

80
00:06:53,440 --> 00:06:55,840
 ability to bear with

81
00:06:58,560 --> 00:07:06,710
 arisen painful feelings in the body that are harsh and

82
00:07:06,710 --> 00:07:09,280
 unpleasant, racking, sharp,

83
00:07:09,280 --> 00:07:14,520
 piercing, harrowing, disagreeable, sapping one's vitality.

84
00:07:14,520 --> 00:07:17,520
 That's interesting.

85
00:07:18,640 --> 00:07:24,600
 sapping one's vitality should probably read, banaharaam,

86
00:07:24,600 --> 00:07:28,560
 should probably read life-threatening.

87
00:07:28,560 --> 00:07:33,260
 I think sapping one's vitality is, and so it's not how we,

88
00:07:33,260 --> 00:07:36,240
 I like to translate this, like this,

89
00:07:36,240 --> 00:07:40,710
 the point is even when the painful feelings could

90
00:07:40,710 --> 00:07:42,560
 potentially kill you,

91
00:07:45,520 --> 00:07:48,100
 even when they seem deadly. I don't know, that's how it's

92
00:07:48,100 --> 00:07:50,000
 normally translated. Bhikkhu Bodhi has

93
00:07:50,000 --> 00:07:58,420
 chosen banaharaam to mean sapping vitality, which is, I'm

94
00:07:58,420 --> 00:08:01,680
 not convinced. I think the Buddha's trying

95
00:08:01,680 --> 00:08:07,340
 to make a point that ones should cultivate the ultimate equ

96
00:08:07,340 --> 00:08:11,280
animity. I'm not quite sure why these

97
00:08:11,280 --> 00:08:15,270
 three, the thing about the Nghutranika is sometimes in the

98
00:08:15,270 --> 00:08:18,800
 book of twos, threes, even fours and fives,

99
00:08:18,800 --> 00:08:23,380
 you have lists that are incomplete. And I think generally

100
00:08:23,380 --> 00:08:25,680
 the reason why they're incomplete is

101
00:08:25,680 --> 00:08:30,930
 given as being that in later books, well, later books there

102
00:08:30,930 --> 00:08:34,160
 are more of them, but that in certain

103
00:08:34,160 --> 00:08:38,730
 instances only certain qualities were important for the

104
00:08:38,730 --> 00:08:43,520
 audience. The Buddha tailored his teaching

105
00:08:43,520 --> 00:08:47,090
 according to the audience, so sometimes he would highlight

106
00:08:47,090 --> 00:08:49,440
 certain teachings. Either that, or as

107
00:08:49,440 --> 00:08:52,760
 they said, it might be that these ones are specifically

108
00:08:52,760 --> 00:08:55,360
 relating to effort, because you've

109
00:08:55,360 --> 00:09:00,710
 got the unerision, unwholesome states. So when you have the

110
00:09:00,710 --> 00:09:03,680
 potential still to get angry about things,

111
00:09:03,680 --> 00:09:06,650
 when you have the potential still to get attached to things

112
00:09:06,650 --> 00:09:09,600
, you work hard to stay mindful so that

113
00:09:09,600 --> 00:09:14,190
 that doesn't happen. But then the same goes with original

114
00:09:14,190 --> 00:09:18,000
 unwholesome states. If evil has a reason

115
00:09:18,000 --> 00:09:20,510
 in your mind, if you are angry, if you are addicted to

116
00:09:20,510 --> 00:09:22,160
 something, when they do arise,

117
00:09:22,160 --> 00:09:25,920
 you strive to be mindful, to not let them continue, to cut

118
00:09:25,920 --> 00:09:30,960
 them off, to take away their nourishment.

119
00:09:32,720 --> 00:09:36,720
 That which is feeding them, which is the repeated

120
00:09:36,720 --> 00:09:44,000
 application of the mind to the same

121
00:09:44,000 --> 00:09:49,500
 as the same state of mind, by changing it to objectivity,

122
00:09:49,500 --> 00:09:52,400
 to seeing it for what it is and so on.

123
00:09:52,400 --> 00:09:58,160
 And so you have it both ways, where there's an unerision,

124
00:09:58,160 --> 00:09:59,600
 don't let them arise. A risen,

125
00:10:00,800 --> 00:10:04,200
 cut them off. And the same goes with wholesome. Here the

126
00:10:04,200 --> 00:10:06,320
 Buddha talks about giving rise to

127
00:10:06,320 --> 00:10:13,440
 wholesomeness. Work hard to make wholesome qualities arise.

128
00:10:13,440 --> 00:10:14,800
 So through mindfulness,

129
00:10:14,800 --> 00:10:17,440
 mindfulness is something we work hard to make arise, but

130
00:10:17,440 --> 00:10:19,920
 through mindfulness, through the

131
00:10:19,920 --> 00:10:22,820
 hard work we do of mindfulness, we work hard to cultivate

132
00:10:22,820 --> 00:10:25,040
 all sorts of wholesome states, wisdom,

133
00:10:28,480 --> 00:10:39,760
 concentration, effort, confidence. We work hard to

134
00:10:39,760 --> 00:10:42,880
 cultivate wholesome states, but

135
00:10:42,880 --> 00:10:45,480
 the same goes with wholesome states that have already

136
00:10:45,480 --> 00:10:47,760
 arisen. You have to work hard to keep them.

137
00:10:47,760 --> 00:10:51,520
 So there are four right efforts based on unerision or

138
00:10:51,520 --> 00:10:54,960
 arisen wholesome and unwholesome. Here the Buddha

139
00:10:56,000 --> 00:11:00,090
 only talks about two of them, which is curious, but it's

140
00:11:00,090 --> 00:11:03,360
 relating to things that have unerision.

141
00:11:03,360 --> 00:11:14,860
 And the most important point here is that what is nice

142
00:11:14,860 --> 00:11:17,120
 about this teaching is that you've got

143
00:11:17,120 --> 00:11:20,750
 effort in regards to wholesomeness, effort in regards to un

144
00:11:20,750 --> 00:11:23,440
wholesomeness. But what about in

145
00:11:23,440 --> 00:11:27,860
 regards to your circumstance? The third one, what this

146
00:11:27,860 --> 00:11:32,160
 teaching points out is that we're not exerting

147
00:11:32,160 --> 00:11:34,590
 effort to change our circumstance, right? Because what's

148
00:11:34,590 --> 00:11:36,160
 the third one? The third one is being

149
00:11:36,160 --> 00:11:39,720
 patient with everything else basically. What are we working

150
00:11:39,720 --> 00:11:41,680
 hard to change? People would say,

151
00:11:41,680 --> 00:11:44,710
 what do you work for in life? Well, you work to be happy to

152
00:11:44,710 --> 00:11:46,480
 get rid of your suffering, right?

153
00:11:47,360 --> 00:11:51,000
 Not really. In Buddhism, that's not actually the focus of

154
00:11:51,000 --> 00:11:54,080
 our practice. We talk about being free

155
00:11:54,080 --> 00:11:56,810
 from suffering and being happy, and that's kind of the goal

156
00:11:56,810 --> 00:11:59,200
 you could say, but I mean,

157
00:11:59,200 --> 00:12:02,890
 it definitely is the goal. But in a practical sense, it's

158
00:12:02,890 --> 00:12:06,720
 not the present goal. It's not what

159
00:12:06,720 --> 00:12:09,820
 we're trying to do. It's not how we're approaching

160
00:12:09,820 --> 00:12:13,520
 experience. When they're suffering, you're in no

161
00:12:13,520 --> 00:12:17,460
 way, shape, or form trying to work hard to get rid of it.

162
00:12:17,460 --> 00:12:19,440
 Absolutely not. In fact, it would be to your

163
00:12:19,440 --> 00:12:23,780
 detriment in two ways. It would be to your detriment

164
00:12:23,780 --> 00:12:25,600
 because of the quality of aversion

165
00:12:25,600 --> 00:12:28,300
 that it would cultivate, but it would also be to your

166
00:12:28,300 --> 00:12:30,480
 detriment because suffering is what's going

167
00:12:30,480 --> 00:12:33,630
 to teach you. If you get rid of suffering, or if you try to

168
00:12:33,630 --> 00:12:35,600
 get rid of suffering, not only are you

169
00:12:35,600 --> 00:12:40,180
 going to cultivate aversion, but you're also missing an

170
00:12:40,180 --> 00:12:42,000
 opportunity to see your aversion,

171
00:12:42,000 --> 00:12:45,710
 to see how you react to suffering, and to learn the

172
00:12:45,710 --> 00:12:47,600
 difference between suffering and aversion,

173
00:12:47,600 --> 00:12:50,650
 that suffering is just suffering, or experiences like pain

174
00:12:50,650 --> 00:12:52,320
 and so on, or just experiencing.

175
00:12:52,320 --> 00:12:58,970
 So the real point I think that we should highlight in this

176
00:12:58,970 --> 00:12:59,360
 teaching

177
00:12:59,360 --> 00:13:02,830
 is that all we have to focus on is wholesome and non-wholes

178
00:13:02,830 --> 00:13:04,880
ome. We're not concerned about

179
00:13:05,520 --> 00:13:09,170
 pain or happiness. Happiness and pain, happiness and

180
00:13:09,170 --> 00:13:12,640
 suffering, these are to be seen as experience.

181
00:13:12,640 --> 00:13:17,080
 They arise and they cease. They're not to be judged or

182
00:13:17,080 --> 00:13:20,320
 reacted to. And that's another thing

183
00:13:20,320 --> 00:13:23,640
 that's missing here from the third one. In the third

184
00:13:23,640 --> 00:13:26,480
 teaching, the Buddha talks simply about

185
00:13:26,480 --> 00:13:30,370
 being patient with unpleasant feelings. But what we don't

186
00:13:30,370 --> 00:13:33,520
 think of too often is the Buddha's

187
00:13:33,520 --> 00:13:36,680
 teaching on how we have to be patient with pleasant

188
00:13:36,680 --> 00:13:41,360
 feelings. And this is a very important

189
00:13:41,360 --> 00:13:44,570
 part of the Buddha's teaching. Not only do we have to be

190
00:13:44,570 --> 00:13:48,000
 patient with suffering, but we have to be

191
00:13:48,000 --> 00:13:53,310
 what one would call patient with happiness. And it's the

192
00:13:53,310 --> 00:13:57,600
 same state and the same quality of mind,

193
00:13:58,320 --> 00:14:04,340
 because it's a reaction either way. It's the changing of

194
00:14:04,340 --> 00:14:09,840
 the mind to give up our inclination

195
00:14:09,840 --> 00:14:14,480
 to react. When we experience unpleasant feelings, our

196
00:14:14,480 --> 00:14:16,000
 inclination is to immediately

197
00:14:16,000 --> 00:14:23,690
 retreat, recoil from them. When we experience pleasant

198
00:14:23,690 --> 00:14:27,920
 feelings, our immediate inclination is to

199
00:14:29,360 --> 00:14:33,330
 seek them out, to incline towards them, to gravitate

200
00:14:33,330 --> 00:14:37,120
 towards them. And true and patience,

201
00:14:37,120 --> 00:14:42,300
 true patience is being able to deal with both, being able

202
00:14:42,300 --> 00:14:45,680
 to bear with both. So it's like the

203
00:14:45,680 --> 00:14:49,890
 dog, you know, this trick where you put a bone, a milk bone

204
00:14:49,890 --> 00:14:52,560
 on the dog's nose. And if the dog is

205
00:14:52,560 --> 00:14:55,650
 really well trained, it won't snap it. It'll sit there and

206
00:14:55,650 --> 00:14:57,760
 wait until the owner says, okay, go for

207
00:14:57,760 --> 00:15:03,450
 it. And then it will eat it. Training the dog, training the

208
00:15:03,450 --> 00:15:07,440
 mind to be well trained,

209
00:15:07,440 --> 00:15:12,830
 just like you train the dog. No, training the mind to let

210
00:15:12,830 --> 00:15:17,600
 go, really. Patience is such an important

211
00:15:17,600 --> 00:15:21,620
 part of the practice. It's very much the feeling that a med

212
00:15:21,620 --> 00:15:23,600
itator should have throughout the

213
00:15:23,600 --> 00:15:27,170
 practice, that they're being patient. And it feels like

214
00:15:27,170 --> 00:15:29,760
 they're burning off defilements. It's another

215
00:15:29,760 --> 00:15:33,250
 good thing about ātāpī. ātāpī in traditional Buddhist

216
00:15:33,250 --> 00:15:35,440
 circles is described, why we use the word,

217
00:15:35,440 --> 00:15:38,810
 a word relating to temperature is because you're burning up

218
00:15:38,810 --> 00:15:39,840
 the defilements.

219
00:15:39,840 --> 00:15:44,500
 And patience is the great way. The Buddha said, "kanti par

220
00:15:44,500 --> 00:15:47,600
amang tāpu tiddhika." Patience is the

221
00:15:47,600 --> 00:15:52,190
 best form of tāpā, tāpā being another form of the word,

222
00:15:52,190 --> 00:15:54,960
 a tāpī. It's the same root, same basic form.

223
00:15:54,960 --> 00:16:01,300
 Tapas really means temperature or heat. But what you're

224
00:16:01,300 --> 00:16:03,680
 doing is you're burning up defilements,

225
00:16:03,680 --> 00:16:06,050
 and you burn them up through patience. When you want

226
00:16:06,050 --> 00:16:08,400
 something and instead you're patient about it,

227
00:16:08,400 --> 00:16:13,240
 you can feel that even the physical, the chemicals will

228
00:16:13,240 --> 00:16:15,280
 bubble and boil inside.

229
00:16:16,240 --> 00:16:24,870
 All of these chemicals waiting to engage and bring about

230
00:16:24,870 --> 00:16:25,920
 states of pleasure.

231
00:16:25,920 --> 00:16:30,230
 And when something unpleasant comes, there's the tension in

232
00:16:30,230 --> 00:16:33,280
 the body, and you can feel the tension

233
00:16:33,280 --> 00:16:38,930
 and the habitual reactivity waiting to strike, waiting to

234
00:16:38,930 --> 00:16:42,400
 get upset, waiting to get angry.

235
00:16:45,760 --> 00:16:49,450
 And instead you're patient with it. This is what you should

236
00:16:49,450 --> 00:16:51,360
 feel in the meditation. You should feel

237
00:16:51,360 --> 00:16:55,100
 patient. You can feel like you're burning up the defile

238
00:16:55,100 --> 00:16:57,200
ments just by sitting with them,

239
00:16:57,200 --> 00:16:59,520
 just by sitting with the things that normally make you

240
00:16:59,520 --> 00:17:02,320
 react.

241
00:17:02,320 --> 00:17:10,360
 So three types of effort. If you want to learn about effort

242
00:17:10,360 --> 00:17:12,160
, effort as it relates to wholesomeness,

243
00:17:12,880 --> 00:17:17,220
 effort as it relates to unwholesomeness, and effort as it

244
00:17:17,220 --> 00:17:18,480
 relates to being patient

245
00:17:18,480 --> 00:17:22,060
 with everything else. The only two things that are really

246
00:17:22,060 --> 00:17:23,680
 important are wholesomeness and

247
00:17:23,680 --> 00:17:28,280
 unwholesomeness. Cultivating wholesomeness, destroying unwh

248
00:17:28,280 --> 00:17:32,080
olesomeness, and being patient

249
00:17:32,080 --> 00:17:35,050
 with everything else, which is really the same thing. It's

250
00:17:35,050 --> 00:17:35,920
 a part of the same.

251
00:17:37,760 --> 00:17:42,990
 So that's our teaching for tonight. I don't know if Robin,

252
00:17:42,990 --> 00:17:45,440
 Robin didn't come tonight.

253
00:17:45,440 --> 00:17:47,910
 Maybe she took it seriously that we're not supposed to have

254
00:17:47,910 --> 00:17:49,520
, but I said we're not going to have them

255
00:17:49,520 --> 00:17:55,140
 every night. It doesn't matter. I think we don't have any

256
00:17:55,140 --> 00:17:58,000
 old questions. I did scroll through,

257
00:17:58,880 --> 00:18:06,450
 and I think our first one is this evening from Glennis.

258
00:18:06,450 --> 00:18:07,920
 When I start to think about the knowledge

259
00:18:07,920 --> 00:18:11,260
 in your videos and then note that I am thinking, the act of

260
00:18:11,260 --> 00:18:14,000
 noting stops the thinking. I can't

261
00:18:14,000 --> 00:18:16,940
 note and think at the same time. But don't I want to be

262
00:18:16,940 --> 00:18:18,960
 able to think about the knowledge?

263
00:18:18,960 --> 00:18:24,220
 No, not really. I would say the knowledge is meant to evoke

264
00:18:24,220 --> 00:18:25,760
 something in you,

265
00:18:26,640 --> 00:18:29,790
 and that to trigger something. And I would say that's

266
00:18:29,790 --> 00:18:33,280
 enough. The point is to encourage you to

267
00:18:33,280 --> 00:18:35,320
 say to yourself thinking, thinking that's really the point

268
00:18:35,320 --> 00:18:37,360
 of all the teaching. If you consider

269
00:18:37,360 --> 00:18:40,860
 what I was teaching just now, hopefully the understanding

270
00:18:40,860 --> 00:18:42,400
 you get from it is that, oh,

271
00:18:42,400 --> 00:18:45,960
 now I have to cultivate mindfulness. I have to work harder

272
00:18:45,960 --> 00:18:48,080
 at cultivating mindfulness.

273
00:18:48,080 --> 00:18:52,670
 I would say if you have to spend time mulling it over,

274
00:18:52,670 --> 00:18:54,320
 thinking about it.

275
00:18:55,440 --> 00:18:59,130
 In a worldly sense, that's fine. And it's important for

276
00:18:59,130 --> 00:19:00,720
 things like teaching or things

277
00:19:00,720 --> 00:19:04,840
 like explaining it to others, figuring out what is the best

278
00:19:04,840 --> 00:19:06,320
 way to explain it. I mean,

279
00:19:06,320 --> 00:19:08,300
 the Buddha, it's not that the Buddha didn't think or that A

280
00:19:08,300 --> 00:19:09,200
rahants don't think.

281
00:19:09,200 --> 00:19:16,000
 And I guess that's really the point is that you're not

282
00:19:16,000 --> 00:19:16,560
 always,

283
00:19:16,560 --> 00:19:21,040
 not necessarily always supposed to be meditating. I mean,

284
00:19:21,040 --> 00:19:22,320
 there are other times where you want to

285
00:19:22,320 --> 00:19:25,170
 do other things, so if you want to teach someone, instead

286
00:19:25,170 --> 00:19:27,120
 of meditating, you have to think.

287
00:19:27,120 --> 00:19:30,810
 No, you have to meditate as well, but there will be a

288
00:19:30,810 --> 00:19:32,400
 portion of time where you have to think,

289
00:19:32,400 --> 00:19:36,540
 what am I going to teach tonight? Of course, much more

290
00:19:36,540 --> 00:19:38,800
 important is that you're meditating.

291
00:19:38,800 --> 00:19:42,600
 There's no question there. But for things like teaching,

292
00:19:42,600 --> 00:19:44,080
 explaining to others,

293
00:19:44,080 --> 00:19:48,750
 and even sometimes for stepping back and evaluating your

294
00:19:48,750 --> 00:19:51,280
 practice, sometimes you have to think.

295
00:19:51,280 --> 00:19:55,960
 So at that time, you don't meditate, that's all. But it's

296
00:19:55,960 --> 00:19:58,400
 not really, none of that's really

297
00:19:58,400 --> 00:20:03,300
 necessary if you're just able to be mindful, you're able to

298
00:20:03,300 --> 00:20:04,720
 say to yourself, thinking,

299
00:20:04,720 --> 00:20:07,190
 thinking, it disappears. Well, that's great. You're

300
00:20:07,190 --> 00:20:09,120
 starting to see that everything arises and

301
00:20:09,120 --> 00:20:12,600
 ceases. You're starting to teach yourself that even our

302
00:20:12,600 --> 00:20:14,800
 minds are arising and ceasing all the time.

303
00:20:18,480 --> 00:20:22,270
 But no, there will just be times where you just think, so

304
00:20:22,270 --> 00:20:23,280
 don't worry about that.

305
00:20:23,280 --> 00:20:26,240
 At that time, you're not meditating.

306
00:20:26,240 --> 00:20:34,560
 Wow, one question tonight. I think I scared everyone away

307
00:20:34,560 --> 00:20:36,240
 by saying I wasn't going to do it every night.

308
00:20:36,240 --> 00:20:44,240
 You'll see. I'm going to wait a couple of minutes because

309
00:20:44,240 --> 00:20:45,600
 there's a delay, I think,

310
00:20:45,600 --> 00:20:50,010
 so I have to wait for, wait a minute at least. More

311
00:20:50,010 --> 00:20:51,600
 questions come, I'll answer them. Look,

312
00:20:51,600 --> 00:20:55,440
 we've got a big, long list of meditators tonight. It must

313
00:20:55,440 --> 00:20:59,760
 be like 20, more than 20.

314
00:20:59,760 --> 00:21:05,440
 We've got Canadians, we've got Americans, we've got India,

315
00:21:05,440 --> 00:21:11,520
 Mexico, Sri Lanka,

316
00:21:13,920 --> 00:21:25,840
 another Mexico, Romania, New Zealand, Norway,

317
00:21:25,840 --> 00:21:33,240
 Moritas. Is that for real? We got someone from, I don't

318
00:21:33,240 --> 00:21:35,600
 even know where that is, Moritas.

319
00:21:40,720 --> 00:21:44,800
 Wow, in Singapore, all over.

320
00:21:44,800 --> 00:21:56,210
 All right, well then we'll end it there. Thank you all for

321
00:21:56,210 --> 00:22:00,560
 tuning in. See you all tomorrow, maybe.

322
00:22:00,560 --> 00:22:30,560
 Bye.

