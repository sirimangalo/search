1
00:00:00,000 --> 00:00:25,200
 Hello, I'm broadcasting live February 7th, 2016.

2
00:00:25,200 --> 00:00:32,200
 Today's quote is about well spoken speech.

3
00:00:32,200 --> 00:00:44,430
 A word is well spoken if it is spoken at the right time,

4
00:00:44,430 --> 00:00:48,200
 spoken in truth, spoken gently,

5
00:00:48,200 --> 00:00:55,550
 spoken about the goal, or spoken about things that are

6
00:00:55,550 --> 00:01:10,200
 meaningful and spoken with love.

7
00:01:10,200 --> 00:01:19,200
 So, I mean, quite actually quite a simple teaching.

8
00:01:19,200 --> 00:01:21,200
 Useful.

9
00:01:21,200 --> 00:01:28,200
 A lot of the teachings like this are just useful reminders

10
00:01:28,200 --> 00:01:33,200
 of the importance of goodness.

11
00:01:33,200 --> 00:01:34,200
 They set the tone.

12
00:01:34,200 --> 00:01:38,950
 If you read through the Buddhist teaching, this is really

13
00:01:38,950 --> 00:01:40,200
 all you see.

14
00:01:40,200 --> 00:01:55,200
 I mean, there's no reading between the lines.

15
00:01:55,200 --> 00:02:01,200
 There's no hidden agenda.

16
00:02:01,200 --> 00:02:12,710
 So, some religious movements and practices have a reason

17
00:02:12,710 --> 00:02:15,200
 for why.

18
00:02:15,200 --> 00:02:19,340
 Why would we care about words that are ill spoken or well

19
00:02:19,340 --> 00:02:20,200
 spoken?

20
00:02:20,200 --> 00:02:27,200
 In Buddhism we care about them because of them being good.

21
00:02:27,200 --> 00:02:30,200
 Good for goodness sake, for example.

22
00:02:30,200 --> 00:02:32,750
 That's not even quite exact because it's not for goodness

23
00:02:32,750 --> 00:02:33,200
 sake.

24
00:02:33,200 --> 00:02:38,200
 It's good because it's good.

25
00:02:38,200 --> 00:02:44,200
 Goodness is for goodness.

26
00:02:44,200 --> 00:02:51,790
 Goodness really is the essential quality in Buddhist

27
00:02:51,790 --> 00:02:53,200
 practice.

28
00:02:53,200 --> 00:03:01,490
 So, when we talk about the goal or when we talk about the

29
00:03:01,490 --> 00:03:04,200
 theory behind Buddhist practice,

30
00:03:04,200 --> 00:03:14,550
 it really is just a natural outcome of cultivation of

31
00:03:14,550 --> 00:03:17,200
 goodness.

32
00:03:17,200 --> 00:03:23,200
 So, in meditation when we talk about the goal being nirvana

33
00:03:23,200 --> 00:03:27,200
 or we talk about cessation, this kind of thing,

34
00:03:27,200 --> 00:03:32,200
 it really is just the same thing that we're talking about

35
00:03:32,200 --> 00:03:38,200
 when we say practicing to give up our addictions,

36
00:03:38,200 --> 00:03:47,110
 our aversion, our ego, practicing to give up on wholesomen

37
00:03:47,110 --> 00:03:48,200
ess.

38
00:03:48,200 --> 00:03:53,200
 That's really it. That's all we're talking about.

39
00:03:53,200 --> 00:03:56,830
 So, whether it be an action with the body, whether it be

40
00:03:56,830 --> 00:04:02,200
 speech, or whether it be just thoughts in the mind,

41
00:04:02,200 --> 00:04:12,200
 it really is just a matter of cultivating goodness.

42
00:04:13,200 --> 00:04:38,200
 [Silence]

43
00:04:38,200 --> 00:04:42,200
 I have too much to say tonight.

44
00:04:42,200 --> 00:04:49,200
 We're holding a peace symposium at McMaster,

45
00:04:49,200 --> 00:04:55,200
 and so I just had a meeting this evening about that,

46
00:04:55,200 --> 00:05:04,200
 potentially starting a peace association at McMaster.

47
00:05:04,200 --> 00:05:07,170
 That's worth talking about because it's curious that

48
00:05:07,170 --> 00:05:10,190
 Buddhism is as much about peace work as it is about

49
00:05:10,190 --> 00:05:11,200
 religion.

50
00:05:11,200 --> 00:05:17,050
 So, in university I find myself involved as much in peace

51
00:05:17,050 --> 00:05:20,200
 studies as I am in religion.

52
00:05:20,200 --> 00:05:25,200
 It goes to the point of what I was just saying,

53
00:05:25,200 --> 00:05:34,200
 Buddhism is just all about goodness, it's all about peace.

54
00:05:34,200 --> 00:05:34,200
 It's not about Buddhism.

55
00:05:34,200 --> 00:05:40,150
 There's nothing really specifically Buddhist about the

56
00:05:40,150 --> 00:05:41,200
 practice.

57
00:05:41,200 --> 00:05:48,200
 [Silence]

58
00:05:48,200 --> 00:05:51,990
 As much as it's about goodness, as much as it's about peace

59
00:05:51,990 --> 00:05:54,200
, as much as it's about wisdom,

60
00:05:54,200 --> 00:06:01,200
 it's about universal qualities of the mind really.

61
00:06:01,200 --> 00:06:05,580
 So, specifically talking about peace, this quote about

62
00:06:05,580 --> 00:06:06,200
 peace,

63
00:06:06,200 --> 00:06:19,460
 speech, specifically talking about speech, you notice it's

64
00:06:19,460 --> 00:06:25,200
 not, it's more about the mental intention behind the speech

65
00:06:25,200 --> 00:06:26,200
.

66
00:06:26,200 --> 00:06:31,200
 I mean, this is always important.

67
00:06:31,200 --> 00:06:38,900
 It's our mind that informs our speech, it's our mind that

68
00:06:38,900 --> 00:06:42,200
 informs our actions.

69
00:06:42,200 --> 00:06:46,860
 And so a big part of our practice, really the main thrust

70
00:06:46,860 --> 00:06:53,200
 of our practice is being conscientious,

71
00:06:53,200 --> 00:07:00,930
 remembering ourselves, keeping track of our minds, track of

72
00:07:00,930 --> 00:07:06,200
 our volitions and our intentions,

73
00:07:06,200 --> 00:07:13,200
 why we do things, how we do things, the state of our minds

74
00:07:13,200 --> 00:07:22,200
 when we do things, when we say things.

75
00:07:22,200 --> 00:07:28,950
 And all this has to do with not just Buddhism, but it has

76
00:07:28,950 --> 00:07:34,200
 to do with peace, and it has to do with goodness.

77
00:07:34,200 --> 00:07:43,200
 [Silence]

78
00:07:43,200 --> 00:07:54,010
 Also, I just got word that our online project is doing well

79
00:07:54,010 --> 00:07:56,200
, which is awesome.

80
00:07:56,200 --> 00:08:01,200
 Looks like we're probably here to stay.

81
00:08:01,200 --> 00:08:06,200
 [Silence]

82
00:08:06,200 --> 00:08:08,200
 That's nice.

83
00:08:08,200 --> 00:08:11,910
 Anyway, not too much tonight though. Nobody's coming on the

84
00:08:11,910 --> 00:08:14,200
 Hangout, I guess, to ask questions.

85
00:08:14,200 --> 00:08:19,200
 Post a link in our meditation page.

86
00:08:19,200 --> 00:08:23,200
 So I guess we'll just call it a night.

87
00:08:23,200 --> 00:08:25,650
 Thank you all for coming out. Thanks for coming out to med

88
00:08:25,650 --> 00:08:26,200
itate.

89
00:08:26,200 --> 00:08:31,200
 I've got a good list tonight. Have a good night, everyone.

90
00:08:31,200 --> 00:08:40,200
 [Silence]

91
00:08:40,200 --> 00:08:42,200
 Thank you, Father. Thank you.

92
00:08:42,200 --> 00:08:59,200
 [Silence]

