1
00:00:00,000 --> 00:00:27,220
 [

2
00:00:27,220 --> 00:00:45,220
 Good evening everyone. Broadcasting Live. January 31st 2016

3
00:00:45,220 --> 00:00:45,220
.

4
00:00:53,220 --> 00:01:21,510
 Today we have quotes, four quotes about the teaching of the

5
00:01:21,510 --> 00:01:22,220
 Buddhas.

6
00:01:22,220 --> 00:01:31,940
 It's interesting, he's pulled together quotes from three

7
00:01:31,940 --> 00:01:34,220
 different places and he skipped one.

8
00:01:34,220 --> 00:01:38,220
 Could have added a fifth, I'm not sure why he didn't.

9
00:01:38,220 --> 00:02:03,910
 [Hindi paramanthapodhiti ka nipanam paramanthapodhiti budd

10
00:02:03,910 --> 00:02:05,220
ha.]

11
00:02:05,220 --> 00:02:15,220
 The ones we have are "cease to do evil".

12
00:02:15,220 --> 00:02:17,970
 It's not really a good translation. "Cease to do evil"

13
00:02:17,970 --> 00:02:22,220
 means "sabapabasa akaranam" not doing all evils.

14
00:02:22,220 --> 00:02:37,900
 "Kushara sutta sampada" becoming full of or accomplished in

15
00:02:37,900 --> 00:02:41,220
 regards to wholesomeness or goodness or skillfulness.

16
00:02:41,220 --> 00:02:47,670
 Skillful things, good things. "Sathita priyodapanam" and

17
00:02:47,670 --> 00:02:53,220
 the purification of one's own mind.

18
00:02:53,220 --> 00:02:58,800
 "Nethan buddha nasaasana" this is the teaching of all Budd

19
00:02:58,800 --> 00:02:59,220
has.

20
00:02:59,220 --> 00:03:12,920
 "Nupavadu nupaghatu" despising them. "Nupavadu" not desp

21
00:03:12,920 --> 00:03:18,220
ising others. "Nupaghatu" not harming others.

22
00:03:18,220 --> 00:03:31,220
 "Pati mokhe dasaamodha" being restrained by monastic rules.

23
00:03:31,220 --> 00:03:42,220
 "Mattanyukta japatasmi" moderation in regards to food.

24
00:03:42,220 --> 00:03:53,220
 "Pantan chasayana asana" having a dwelling that is secluded

25
00:03:53,220 --> 00:03:53,220
.

26
00:03:53,220 --> 00:04:08,200
 "Aditya jayajayogo" being devoted to higher mind. Certainly

27
00:04:08,200 --> 00:04:10,220
 not mediation.

28
00:04:10,220 --> 00:04:14,220
 Something's wrong with the quote there.

29
00:04:14,220 --> 00:04:29,220
 "Nethan buddha nasaasa"

30
00:04:29,220 --> 00:04:33,230
 You know it's funny to talk about what is the teaching of

31
00:04:33,230 --> 00:04:34,220
 the Buddhas.

32
00:04:34,220 --> 00:04:41,650
 It's just the teaching of the Buddha. I just did a response

33
00:04:41,650 --> 00:04:46,220
 paper today for reading the Lotus Sutra.

34
00:04:46,220 --> 00:04:52,740
 And the first draft said the Buddha says this, the Buddha

35
00:04:52,740 --> 00:04:53,220
 says that.

36
00:04:53,220 --> 00:04:58,220
 It was just basically quoting what the text had said about

37
00:04:58,220 --> 00:05:01,220
 why the Buddha teaches in parables.

38
00:05:01,220 --> 00:05:05,180
 Then I redid it because I went over it again and I had to

39
00:05:05,180 --> 00:05:08,580
 edit it to say, "Well the text says that the Buddha says

40
00:05:08,580 --> 00:05:09,220
 this."

41
00:05:09,220 --> 00:05:21,910
 The Buddha is quoted as saying or is claimed to have said

42
00:05:21,910 --> 00:05:27,220
 or something like that.

43
00:05:27,220 --> 00:05:30,680
 Because I don't believe that the Buddha said the stuff that

44
00:05:30,680 --> 00:05:32,220
's in the Lotus Sutra.

45
00:05:32,220 --> 00:05:37,250
 I don't think that many people do. Besides those who are

46
00:05:37,250 --> 00:05:40,220
 devout Mahayana Buddhists.

47
00:05:40,220 --> 00:05:45,130
 The question is what did the Buddha teach and what is the

48
00:05:45,130 --> 00:05:47,220
 Buddha's teaching?

49
00:05:47,220 --> 00:05:52,760
 I think most of us agree that the Four Noble Truths are the

50
00:05:52,760 --> 00:05:58,220
 Buddha's teaching. Really the teaching of all Buddhas.

51
00:05:58,220 --> 00:06:03,220
 So it's a good reason for us to start there.

52
00:06:03,220 --> 00:06:08,500
 Because I know that in Mahayana Buddhism they also talk

53
00:06:08,500 --> 00:06:11,220
 about the Four Noble Truths.

54
00:06:11,220 --> 00:06:14,610
 And why we know it's the teaching of the Buddha, we don't

55
00:06:14,610 --> 00:06:25,220
 find it anywhere else. Someone had to come up with this.

56
00:06:25,220 --> 00:06:43,690
 This is something unique to the Buddha's teaching. I have

57
00:06:43,690 --> 00:06:47,220
 the idea that suffering is caused by craving.

58
00:06:47,220 --> 00:06:55,220
 I suppose it's not even that unique if you think about it.

59
00:06:55,220 --> 00:06:55,220
 Maybe the Eight Full Noble Path.

60
00:06:55,220 --> 00:07:01,300
 In India at the time a lot of people thought that desire

61
00:07:01,300 --> 00:07:11,220
 was the cause of suffering.

62
00:07:11,220 --> 00:07:14,510
 People were clear about birth being suffering, old age

63
00:07:14,510 --> 00:07:18,360
 being suffering, death being suffering. A lot of ascetics

64
00:07:18,360 --> 00:07:20,220
 knew this sort of thing.

65
00:07:20,220 --> 00:07:25,220
 But what they didn't know is how to be free from desire.

66
00:07:25,220 --> 00:07:30,430
 They talked about desires and they would torture themselves

67
00:07:30,430 --> 00:07:33,220
 to try to free them from desire.

68
00:07:33,220 --> 00:07:37,890
 So one of the last things the Buddha said before he passed

69
00:07:37,890 --> 00:07:42,030
 away was that it's the Eight Full Noble Path that

70
00:07:42,030 --> 00:07:44,220
 determines the religion.

71
00:07:44,220 --> 00:07:52,070
 In whatever he said, in whatever religion you find the

72
00:07:52,070 --> 00:07:57,840
 Eight Full Noble Path, in that religion you're going to

73
00:07:57,840 --> 00:08:01,220
 find Sotapanan, Sakadakami, Anagami, Arhan.

74
00:08:01,220 --> 00:08:05,220
 You're going to find the mind beings.

75
00:08:05,220 --> 00:08:13,220
 In any religion it doesn't have the Eight Full Noble Path.

76
00:08:13,220 --> 00:08:17,220
 If you want to talk about what did the Buddha teach?

77
00:08:17,220 --> 00:08:21,540
 Maybe the three trainings are the Eight Full Noble Path,

78
00:08:21,540 --> 00:08:26,250
 morality, concentration and wisdom. This is probably the

79
00:08:26,250 --> 00:08:30,270
 best way to explain to someone in a nutshell what Buddhism

80
00:08:30,270 --> 00:08:31,220
 is.

81
00:08:31,220 --> 00:08:35,220
 It's the Eight Full Noble Path.

82
00:08:35,220 --> 00:08:42,220
 So starting with morality.

83
00:08:42,220 --> 00:08:50,220
 Cultivating right action, right speech, right livelihood.

84
00:08:50,220 --> 00:08:57,220
 Then right concentration which is

85
00:08:57,220 --> 00:09:05,610
 right effort, right mindfulness and right focus, right

86
00:09:05,610 --> 00:09:08,220
 concentration.

87
00:09:08,220 --> 00:09:14,590
 And then right wisdom, which is right thought and right

88
00:09:14,590 --> 00:09:24,220
 view, right view and right thought.

89
00:09:24,220 --> 00:09:29,560
 I had a meeting today of the Hamilton Interfaith Peace

90
00:09:29,560 --> 00:09:30,220
 Group.

91
00:09:30,220 --> 00:09:40,470
 It's always been since I've joined this group. I guess I

92
00:09:40,470 --> 00:09:48,220
 joined in October, November.

93
00:09:48,220 --> 00:09:56,220
 The idea of faith is very much a theistic concept.

94
00:09:56,220 --> 00:09:59,990
 Not to say that Buddhists don't have faith and we've talked

95
00:09:59,990 --> 00:10:02,220
 about this before but

96
00:10:02,220 --> 00:10:07,750
 it's not exactly and it's not a core concept in Buddhism,

97
00:10:07,750 --> 00:10:08,220
 right?

98
00:10:08,220 --> 00:10:12,470
 It's nowhere near a core concept. Is faith useful? Yeah,

99
00:10:12,470 --> 00:10:16,220
 faith is a wholesome quality.

100
00:10:16,220 --> 00:10:19,220
 Wholesome in the sense that it gives you strength.

101
00:10:19,220 --> 00:10:24,630
 That it's possible of course to have faith in something

102
00:10:24,630 --> 00:10:27,720
 that is unjustifying. It's possible for faith to lead you

103
00:10:27,720 --> 00:10:28,220
 to danger, to suffering.

104
00:10:28,220 --> 00:10:40,220
 Not the faith itself but the thing that you have faith in.

105
00:10:40,220 --> 00:10:49,220
 And the faith propels you towards danger. So it's not

106
00:10:49,220 --> 00:10:54,220
 considered to be essential.

107
00:10:54,220 --> 00:11:00,220
 It's not central.

108
00:11:00,220 --> 00:11:05,980
 I suppose you could say it's essential as one of the

109
00:11:05,980 --> 00:11:10,220
 faculties but not in the sense of...

110
00:11:10,220 --> 00:11:15,220
 I mean, faith is...

111
00:11:15,220 --> 00:11:18,070
 When people talk about faith, they're talking about

112
00:11:18,070 --> 00:11:20,220
 something that they don't understand.

113
00:11:20,220 --> 00:11:25,220
 Because you can't say that science is faith-based and yet

114
00:11:25,220 --> 00:11:29,220
 scientists are often very confident of their theories.

115
00:11:29,220 --> 00:11:34,220
 But it's because they have reason to be confident of them.

116
00:11:34,220 --> 00:11:35,220
 They have faith in their theories.

117
00:11:35,220 --> 00:11:37,480
 They have faith in their instruments but it's based on

118
00:11:37,480 --> 00:11:41,220
 experience. It's based on observations.

119
00:11:41,220 --> 00:11:46,130
 It's based on knowledge and so on. That's not what is meant

120
00:11:46,130 --> 00:11:47,220
 by a faith.

121
00:11:47,220 --> 00:11:53,220
 My faith, your faith, a religious faith.

122
00:11:53,220 --> 00:11:58,470
 Anyway, today we were talking about this event that we're

123
00:11:58,470 --> 00:12:02,220
 going to be holding an April storytelling event.

124
00:12:02,220 --> 00:12:05,510
 We were trying to find the name for it and someone

125
00:12:05,510 --> 00:12:09,220
 suggested we should put the word "faith" in the title.

126
00:12:09,220 --> 00:12:12,220
 And I said, "Well, you know..."

127
00:12:12,220 --> 00:12:17,220
 It wasn't really all that keen to bring it up but I said,

128
00:12:17,220 --> 00:12:22,720
 "If you're inviting a Buddhist..." Because I'll be telling

129
00:12:22,720 --> 00:12:24,220
 one of the stories. If there's a Buddhist in there that

130
00:12:24,220 --> 00:12:26,220
 doesn't really...

131
00:12:26,220 --> 00:12:30,300
 It doesn't really fit with Buddhism. We're not faith-based.

132
00:12:30,300 --> 00:12:34,220
 We don't talk about faith. It's not a word that we use.

133
00:12:34,220 --> 00:12:43,490
 And caused a little bit of contention there because of

134
00:12:43,490 --> 00:12:46,220
 course the group itself is all about faith.

135
00:12:46,220 --> 00:12:56,220
 It's called the Hamilton Interfaith Peace Group.

136
00:12:56,220 --> 00:13:01,500
 But the reason I think of that is because that's really not

137
00:13:01,500 --> 00:13:05,220
 what the Buddhist teaching is about.

138
00:13:05,220 --> 00:13:11,520
 No matter how useful you say faith is, a key aspect of the

139
00:13:11,520 --> 00:13:15,220
 Buddhist teaching is definitely to move beyond faith.

140
00:13:15,220 --> 00:13:20,220
 Beyond belief in things that you don't understand.

141
00:13:20,220 --> 00:13:26,220
 The idea is to come to understand.

142
00:13:26,220 --> 00:13:30,220
 Focuses on understanding things, not in believing things.

143
00:13:30,220 --> 00:13:34,170
 And interestingly that's something that distinguishes Ther

144
00:13:34,170 --> 00:13:38,210
avada Buddhism from what I'm reading about Mahayana Buddhism

145
00:13:38,210 --> 00:13:40,220
 now.

146
00:13:40,220 --> 00:13:44,470
 At least in the Lotus Sutra, there starts to become an

147
00:13:44,470 --> 00:13:46,220
 emphasis on faith.

148
00:13:46,220 --> 00:13:49,220
 The Buddha says, "Everyone is going to become a Buddha."

149
00:13:49,220 --> 00:13:51,220
 And he says, "You just have to believe me.

150
00:13:51,220 --> 00:13:54,410
 You're going to have to believe me in this one. Believe in

151
00:13:54,410 --> 00:13:55,220
 me."

152
00:13:55,220 --> 00:13:59,730
 And then by the time the Lotus Sutra came about, there was

153
00:13:59,730 --> 00:14:01,220
 a lot of funny stuff going on.

154
00:14:01,220 --> 00:14:04,830
 So they started believing that the Buddha... There were

155
00:14:04,830 --> 00:14:06,220
 heavens full of Buddhas,

156
00:14:06,220 --> 00:14:10,220
 called Buddha Lands or whatever.

157
00:14:10,220 --> 00:14:14,220
 There was the pure land that grew out of this.

158
00:14:14,220 --> 00:14:21,140
 But there was the idea that Buddhas were constantly working

159
00:14:21,140 --> 00:14:25,220
 to enlighten beings for eternity.

160
00:14:25,220 --> 00:14:28,640
 They never really leave Samsara. It's a much more

161
00:14:28,640 --> 00:14:32,220
 complicated system that they have.

162
00:14:32,220 --> 00:14:36,380
 And so no wonder that people don't really get a sense of

163
00:14:36,380 --> 00:14:39,220
 what the Buddha taught, because...

164
00:14:39,220 --> 00:14:43,220
 Well, according to who, right?

165
00:14:43,220 --> 00:14:48,610
 So I submit to you, well, that what the Buddha taught is

166
00:14:48,610 --> 00:14:49,220
 through things.

167
00:14:49,220 --> 00:14:55,220
 Morality, concentration, and wisdom.

168
00:14:55,220 --> 00:15:02,220
 And when you develop concentration, it leads to... When you

169
00:15:02,220 --> 00:15:02,220
 develop morality,

170
00:15:02,220 --> 00:15:07,840
 the concentration that comes from morality is a great fruit

171
00:15:07,840 --> 00:15:08,220
.

172
00:15:08,220 --> 00:15:11,220
 When you have strong concentration, it leads to wisdom.

173
00:15:11,220 --> 00:15:15,220
 When you have wisdom, it leads to...

174
00:15:15,220 --> 00:15:20,220
 "Ah, suve hi, chit tangui mujhti."

175
00:15:20,220 --> 00:15:31,220
 The mind becomes free from Aswap, from the foundness.

176
00:15:31,220 --> 00:15:45,220
 So...

177
00:15:45,220 --> 00:15:54,220
 See everyone's active on Second Life.

178
00:15:54,220 --> 00:15:58,530
 I haven't been making videos. I'll try to make some this

179
00:15:58,530 --> 00:15:59,220
 week, we'll see.

180
00:15:59,220 --> 00:16:03,220
 It's just been a little bit swamped.

181
00:16:03,220 --> 00:16:07,220
 Everything's...

182
00:16:07,220 --> 00:16:13,220
 The TED Talk, I was supposed to do a TED Talk, and...

183
00:16:13,220 --> 00:16:20,220
 As I said, I kind of bombed the trial run.

184
00:16:20,220 --> 00:16:25,220
 But they invited me to another interview.

185
00:16:25,220 --> 00:16:29,740
 So there were 16 semi-finalists, and then we had the semi-

186
00:16:29,740 --> 00:16:31,220
final finalists.

187
00:16:31,220 --> 00:16:35,630
 Eight of us. So they cut it down to eight, and I was still

188
00:16:35,630 --> 00:16:36,220
 in it.

189
00:16:36,220 --> 00:16:39,290
 So I wrote them back and said I really don't think I can

190
00:16:39,290 --> 00:16:40,220
 give them my all.

191
00:16:40,220 --> 00:16:46,220
 I realized in the interview that I just don't have the...

192
00:16:46,220 --> 00:16:50,220
 I don't have the heart to put my heart into it.

193
00:16:50,220 --> 00:16:55,220
 It's not really something that's neat to give a talk about.

194
00:16:55,220 --> 00:17:00,220
 Talk to so many people in such a prestigious organization

195
00:17:00,220 --> 00:17:06,220
 that they don't want people like me.

196
00:17:06,220 --> 00:17:09,220
 I don't think so anyway.

197
00:17:09,220 --> 00:17:12,050
 I mean, if they want me to just get up and teach meditation

198
00:17:12,050 --> 00:17:17,220
, that's something I'd be passionate about.

199
00:17:17,220 --> 00:17:29,220
 I'm not really passionate about what I'd written about.

200
00:17:29,220 --> 00:17:37,220
 But I do have to find ways to...

201
00:17:37,220 --> 00:17:49,220
 To find ways to teach, no? It's a little bit...

202
00:17:49,220 --> 00:17:54,220
 It's a little bit cumbersome using the internet to teach.

203
00:17:54,220 --> 00:18:02,220
 I mean, not just to teach, but to build a community.

204
00:18:02,220 --> 00:18:10,010
 I shouldn't... I shouldn't... minimalize the importance of

205
00:18:10,010 --> 00:18:11,220
 a local community.

206
00:18:11,220 --> 00:18:15,220
 Hopefully that'll build up as well.

207
00:18:15,220 --> 00:18:21,220
 I want people to keep this place going, to keep me alive,

208
00:18:21,220 --> 00:18:28,220
 to keep putting out the trash,

209
00:18:28,220 --> 00:18:36,220
 cleaning the bathrooms, making sure meditators have food.

210
00:18:36,220 --> 00:18:40,340
 But like on the internet, the problem is building a

211
00:18:40,340 --> 00:18:42,220
 community, right?

212
00:18:42,220 --> 00:18:45,220
 I think this site has done quite a bit.

213
00:18:45,220 --> 00:18:52,220
 We've got a whole list of people on this evening.

214
00:18:52,220 --> 00:18:59,220
 There's always more that can be done.

215
00:18:59,220 --> 00:19:09,220
 It's not a perfect community.

216
00:19:09,220 --> 00:19:14,230
 This is nice. I'm trying to just get a sense of the

217
00:19:14,230 --> 00:19:15,220
 community.

218
00:19:15,220 --> 00:19:20,950
 Because it's nice coming on at 9 and giving a little bit of

219
00:19:20,950 --> 00:19:22,220
 a...

220
00:19:22,220 --> 00:19:26,220
 Having a chance to give a little bit of dumb...

221
00:19:26,220 --> 00:19:31,220
 And we have a community this way.

222
00:19:31,220 --> 00:19:55,220
 And we can practice together.

223
00:19:55,220 --> 00:19:57,220
 Anyway, sorry, it's been a long day.

224
00:19:57,220 --> 00:20:01,010
 We had a meeting this afternoon, really interesting meeting

225
00:20:01,010 --> 00:20:11,220
, interesting group of people working on this event.

226
00:20:11,220 --> 00:20:18,220
 But...

227
00:20:18,220 --> 00:20:24,220
 Yeah. I guess that's all for this evening.

228
00:20:24,220 --> 00:20:27,220
 Thank you all for tuning in. Have a good night.

