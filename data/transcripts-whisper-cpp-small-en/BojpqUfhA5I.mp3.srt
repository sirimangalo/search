1
00:00:00,000 --> 00:00:05,000
 Okay, good evening everyone.

2
00:00:05,000 --> 00:00:10,000
 Welcome to evening Dhamma.

3
00:00:10,000 --> 00:00:24,000
 Tonight we are looking at purification of view.

4
00:00:24,000 --> 00:00:33,000
 This is the first of the purifications relating to wisdom.

5
00:00:33,000 --> 00:00:48,330
 The first two purifications correspond directly with

6
00:00:48,330 --> 00:00:53,000
 morality and concentration or sila in samadhi.

7
00:00:53,000 --> 00:00:57,720
 The third to the seventh, as I said, have to do with wisdom

8
00:00:57,720 --> 00:01:03,000
, so this is number three.

9
00:01:03,000 --> 00:01:09,800
 And in a way, this is where it becomes more about results

10
00:01:09,800 --> 00:01:12,000
 than about practice.

11
00:01:12,000 --> 00:01:16,360
 It's hard to really say that you're practicing wisdom, you

12
00:01:16,360 --> 00:01:19,000
're training in wisdom.

13
00:01:19,000 --> 00:01:25,000
 It is described that way.

14
00:01:25,000 --> 00:01:31,000
 And to some extent you wouldn't...

15
00:01:31,000 --> 00:01:34,000
 To some extent you don't even practice concentration,

16
00:01:34,000 --> 00:01:40,000
 concentration as a result.

17
00:01:40,000 --> 00:01:43,120
 But the point is really what you're practicing is

18
00:01:43,120 --> 00:01:48,000
 mindfulness throughout the past.

19
00:01:48,000 --> 00:01:53,000
 When you cultivate ethics, it's really about being mindful.

20
00:01:53,000 --> 00:02:00,000
 True ethical behavior, morality, proper behavior in speech

21
00:02:00,000 --> 00:02:03,000
 on the past comes from being mindful.

22
00:02:03,000 --> 00:02:07,910
 It doesn't come from saying, "I'm not going to do this, I'm

23
00:02:07,910 --> 00:02:10,000
 not going to do that."

24
00:02:10,000 --> 00:02:12,000
 That is, of course, temporary.

25
00:02:12,000 --> 00:02:17,000
 It is a form of morality, but it's only makeshift.

26
00:02:17,000 --> 00:02:21,420
 It doesn't actually set the mind in the right state to

27
00:02:21,420 --> 00:02:24,000
 cultivate concentration.

28
00:02:24,000 --> 00:02:30,000
 But when you're mindful, morality, your ethical behavior,

29
00:02:30,000 --> 00:02:36,000
 the behavior that results from mindfulness, needs to focus.

30
00:02:36,000 --> 00:02:39,000
 Go to samadhi.

31
00:02:39,000 --> 00:02:44,760
 And when you're focused in the right way, and you continue

32
00:02:44,760 --> 00:02:52,050
 to be mindful with a focused sort of mindfulness, then

33
00:02:52,050 --> 00:02:56,000
 wisdom arises.

34
00:02:56,000 --> 00:03:04,780
 And so when we talk about right view or purification of

35
00:03:04,780 --> 00:03:11,000
 view, we're not exactly talking about views.

36
00:03:11,000 --> 00:03:20,490
 We're certainly considering the danger of beliefs and views

37
00:03:20,490 --> 00:03:28,000
, especially the views of self and identity.

38
00:03:28,000 --> 00:03:33,400
 But the purification of view is much more about a pur

39
00:03:33,400 --> 00:03:38,000
ification of the way we look at the world.

40
00:03:38,000 --> 00:03:49,000
 So view in the sense of literally how we view reality.

41
00:03:49,000 --> 00:03:55,600
 Our ordinary way of viewing reality is based on concepts of

42
00:03:55,600 --> 00:03:59,000
 people, places, things.

43
00:03:59,000 --> 00:04:02,840
 And so if I tell you people and places and things don't

44
00:04:02,840 --> 00:04:05,000
 exist, it seems absurd.

45
00:04:05,000 --> 00:04:13,280
 And in fact, it is rather meaningless to say something like

46
00:04:13,280 --> 00:04:14,000
 that.

47
00:04:14,000 --> 00:04:18,640
 Because in order to say that something exists or doesn't

48
00:04:18,640 --> 00:04:22,000
 exist, you have to ask in what sense.

49
00:04:22,000 --> 00:04:28,780
 In what sense do I not exist, do you not exist? Obviously,

50
00:04:28,780 --> 00:04:36,000
 clearly in some sense, people, places and things do exist.

51
00:04:36,000 --> 00:04:40,000
 And so to some extent, it appears somewhat arbitrary.

52
00:04:40,000 --> 00:04:46,000
 Which way of looking at the world you choose?

53
00:04:46,000 --> 00:04:50,830
 Obviously, if you choose to look at the world from a

54
00:04:50,830 --> 00:04:56,000
 phenomenological point of view based on experiences,

55
00:04:56,000 --> 00:05:01,020
 well, you're never going to experience a person, a place or

56
00:05:01,020 --> 00:05:02,000
 a thing.

57
00:05:02,000 --> 00:05:06,680
 Someone asked me this morning, I think, when it was one of

58
00:05:06,680 --> 00:05:12,000
 you who asked me recently, does the brain exist?

59
00:05:12,000 --> 00:05:15,880
 I was one of my online meditators, did the mind and the

60
00:05:15,880 --> 00:05:19,000
 brain exist independent of each other?

61
00:05:19,000 --> 00:05:24,010
 And it's a good question because it illustrates this point

62
00:05:24,010 --> 00:05:25,000
 quite well.

63
00:05:25,000 --> 00:05:30,660
 Because from a point of view of concepts of entities, the

64
00:05:30,660 --> 00:05:38,000
 brain exists and the mind doesn't exist.

65
00:05:38,000 --> 00:05:41,390
 Because you don't, you look around and you don't find the

66
00:05:41,390 --> 00:05:42,000
 mind.

67
00:05:42,000 --> 00:05:47,110
 Mind isn't a physical thing that you can identify. There it

68
00:05:47,110 --> 00:05:48,000
 is.

69
00:05:48,000 --> 00:05:52,000
 The mind isn't anywhere, it doesn't take up space.

70
00:05:52,000 --> 00:05:58,150
 The mind is not like this table or this rug or a person,

71
00:05:58,150 --> 00:06:01,000
 which is why we get into this, based on this sort of view,

72
00:06:01,000 --> 00:06:08,000
 we get into the idea of a person being made up of the brain

73
00:06:08,000 --> 00:06:09,000
.

74
00:06:09,000 --> 00:06:11,000
 And so the mind is just the brain.

75
00:06:11,000 --> 00:06:15,140
 Because we don't have any way of thinking about the mind

76
00:06:15,140 --> 00:06:17,000
 apart from the brain.

77
00:06:17,000 --> 00:06:21,310
 It's completely based on the way we look at the world, our

78
00:06:21,310 --> 00:06:22,000
 view.

79
00:06:22,000 --> 00:06:27,290
 Contrary from the point of view of a phenomenological point

80
00:06:27,290 --> 00:06:29,000
 of view,

81
00:06:29,000 --> 00:06:34,710
 the mind exists clearly. The mind is how phenomena are

82
00:06:34,710 --> 00:06:36,000
 experienced.

83
00:06:36,000 --> 00:06:39,000
 And the brain doesn't exist.

84
00:06:39,000 --> 00:06:44,000
 Because suppose you see a brain, well, that's just seeing.

85
00:06:44,000 --> 00:06:48,000
 And based on the seeing, there is the conceiving brain.

86
00:06:48,000 --> 00:06:54,000
 It's just the conceiving.

87
00:06:54,000 --> 00:06:57,920
 I don't know if this seems somewhat theoretical, but it's

88
00:06:57,920 --> 00:06:59,000
 clearly not.

89
00:06:59,000 --> 00:07:02,000
 For anyone who's practiced meditation, this is the shift

90
00:07:02,000 --> 00:07:05,000
 that needs to take place from one to the other.

91
00:07:05,000 --> 00:07:13,490
 And the claim I think that we would try to make quite

92
00:07:13,490 --> 00:07:18,290
 clearly is that there are not equal ways of looking at the

93
00:07:18,290 --> 00:07:19,000
 world.

94
00:07:19,000 --> 00:07:23,160
 A conceptual way of looking at the world doesn't really

95
00:07:23,160 --> 00:07:28,000
 help you, doesn't really change the way you are.

96
00:07:28,000 --> 00:07:32,280
 When we talk about experience and wisdom in an ordinary

97
00:07:32,280 --> 00:07:36,000
 sense, not in a Buddhist or meditative sense,

98
00:07:36,000 --> 00:07:40,650
 it's a very hard thing for us to grasp because we're mostly

99
00:07:40,650 --> 00:07:43,000
 not cultivating wisdom.

100
00:07:43,000 --> 00:07:48,120
 The wisdom has nothing to do with concepts. It can't have

101
00:07:48,120 --> 00:07:51,000
 anything to do with concepts.

102
00:07:51,000 --> 00:07:54,030
 And you can look at, you can take a microscope and look at

103
00:07:54,030 --> 00:07:55,000
 how the brain works.

104
00:07:55,000 --> 00:07:58,880
 And I can tell you all about how the addiction cycle in the

105
00:07:58,880 --> 00:08:00,000
 brain works.

106
00:08:00,000 --> 00:08:03,400
 I don't know too much about it, but I have asked people

107
00:08:03,400 --> 00:08:04,000
 about it.

108
00:08:04,000 --> 00:08:06,100
 And their scientists can tell you so much about how

109
00:08:06,100 --> 00:08:07,000
 addiction works.

110
00:08:07,000 --> 00:08:11,500
 They can be very clear about it, how the addiction cycle

111
00:08:11,500 --> 00:08:13,000
 works in the brain.

112
00:08:13,000 --> 00:08:15,350
 But be completely powerless to free themselves from

113
00:08:15,350 --> 00:08:19,000
 addiction, let alone anyone else.

114
00:08:19,000 --> 00:08:26,000
 And so they offer all these medications, psychoactive drugs

115
00:08:26,000 --> 00:08:32,000
 that appear to change the makeup of the brain

116
00:08:32,000 --> 00:08:39,580
 and therefore somehow cure the problem. But of course they

117
00:08:39,580 --> 00:08:48,000
 don't come close to curing the problem.

118
00:08:48,000 --> 00:08:51,030
 But when we talk about wisdom, we're talking about

119
00:08:51,030 --> 00:08:52,000
 experience.

120
00:08:52,000 --> 00:08:55,980
 We don't perhaps realize it, but we talk about people who

121
00:08:55,980 --> 00:09:00,280
 have lived, older people, who have lived a long life,

122
00:09:00,280 --> 00:09:01,000
 having wisdom.

123
00:09:01,000 --> 00:09:04,450
 It has very much to do with the experiences, usually the

124
00:09:04,450 --> 00:09:09,000
 suffering that they've gone through, personally.

125
00:09:09,000 --> 00:09:14,900
 Their personal experiences of reality, how they've lived

126
00:09:14,900 --> 00:09:20,000
 through addiction, how they've lived through suffering,

127
00:09:20,000 --> 00:09:27,280
 how they lived through difficulty, their perspective on the

128
00:09:27,280 --> 00:09:28,000
 world.

129
00:09:28,000 --> 00:09:32,400
 Not in terms of people they've met or so on, things they've

130
00:09:32,400 --> 00:09:39,000
 seen, but completely in regards to their experiences.

131
00:09:39,000 --> 00:09:45,070
 And so it's absolutely essential for it to gain true wisdom

132
00:09:45,070 --> 00:09:49,330
, that we take up this, that we take up an intensive form of

133
00:09:49,330 --> 00:09:54,000
 this experience-based learning.

134
00:09:54,000 --> 00:09:56,980
 And by learning we mean learning wisdom, not learning

135
00:09:56,980 --> 00:09:58,000
 intelligence.

136
00:09:58,000 --> 00:10:02,010
 What are you learning? You're learning... The content of

137
00:10:02,010 --> 00:10:04,000
 your learning is wisdom.

138
00:10:04,000 --> 00:10:07,020
 The content is not intelligent. You're not trying to gain

139
00:10:07,020 --> 00:10:08,000
 intelligence.

140
00:10:08,000 --> 00:10:12,000
 And these are just words, but they're kind of important.

141
00:10:12,000 --> 00:10:14,850
 Because by intelligence we would of course mean information

142
00:10:14,850 --> 00:10:18,000
, like I'm giving you information right now.

143
00:10:18,000 --> 00:10:23,000
 I can't give you enlightenment. I can't give you wisdom.

144
00:10:23,000 --> 00:10:26,710
 So wisdom is a useful word to separate away, and this is

145
00:10:26,710 --> 00:10:29,000
 where wisdom really resides.

146
00:10:29,000 --> 00:10:34,000
 This is how we distinguish wisdom from intelligence.

147
00:10:34,000 --> 00:10:41,000
 Wisdom is what you gain through experience.

148
00:10:41,000 --> 00:10:45,870
 So this is what you begin to acquire through the meditation

149
00:10:45,870 --> 00:10:47,000
 practice.

150
00:10:47,000 --> 00:10:57,490
 And the one thing about wisdom, well, wisdom, that I think

151
00:10:57,490 --> 00:11:02,000
 confounds people is they think of it as something deep,

152
00:11:02,000 --> 00:11:08,000
 abstruse, far away, you know?

153
00:11:08,000 --> 00:11:18,000
 Something enigmatic, elusive.

154
00:11:18,000 --> 00:11:21,330
 But in fact, wisdom is when you know that the stomach is

155
00:11:21,330 --> 00:11:22,000
 rising.

156
00:11:22,000 --> 00:11:26,710
 My teacher used to say, knowing the stomach is rising, when

157
00:11:26,710 --> 00:11:34,000
 you know the stomach is falling, that's wisdom.

158
00:11:34,000 --> 00:11:39,310
 Wisdom is actually quite simple. Wisdom is to see things as

159
00:11:39,310 --> 00:11:40,000
 they are.

160
00:11:40,000 --> 00:11:44,060
 And the first step in wisdom is to be able to see the

161
00:11:44,060 --> 00:11:45,000
 rising.

162
00:11:45,000 --> 00:11:49,000
 To know rising is rising, falling is falling.

163
00:11:49,000 --> 00:11:52,000
 I mean, the Buddha said as much, "Let seeing be seeing."

164
00:11:52,000 --> 00:11:55,000
 That's what we train.

165
00:11:55,000 --> 00:12:04,660
 The first and most, sort of the most essential stage of

166
00:12:04,660 --> 00:12:11,340
 wisdom, in some sense, most essential, because it really

167
00:12:11,340 --> 00:12:14,000
 sets you on the path.

168
00:12:14,000 --> 00:12:17,000
 Without it, you're not even on the path.

169
00:12:17,000 --> 00:12:22,900
 The most essential is this acquiring of right view, looking

170
00:12:22,900 --> 00:12:26,000
 at the world in the right way.

171
00:12:26,000 --> 00:12:30,000
 So when you walk, it's not you walking.

172
00:12:30,000 --> 00:12:33,960
 An ordinary way of looking at walking is, this is me

173
00:12:33,960 --> 00:12:35,000
 walking.

174
00:12:35,000 --> 00:12:40,230
 So I ask you, right, stepping right, stepping left, are

175
00:12:40,230 --> 00:12:42,000
 they one thing or separate things in the clouds?

176
00:12:42,000 --> 00:12:46,000
 No, it's me walking. That's one thing.

177
00:12:46,000 --> 00:12:49,000
 Of course, the meditator thinks that's absurd, right?

178
00:12:49,000 --> 00:12:51,560
 I told you this is one thing and this is the same thing as

179
00:12:51,560 --> 00:12:52,000
 you.

180
00:12:52,000 --> 00:12:55,320
 At this point, I'm hoping that you're quite clear that they

181
00:12:55,320 --> 00:12:58,690
're two very separate things, because they're two

182
00:12:58,690 --> 00:13:00,000
 experiences.

183
00:13:00,000 --> 00:13:02,000
 Very clear.

184
00:13:02,000 --> 00:13:05,400
 There's something you should feel proud about, and don't

185
00:13:05,400 --> 00:13:08,000
 feel proud, but should give you confidence.

186
00:13:08,000 --> 00:13:14,820
 Oh yeah, I'm not without wisdom, because it seems quite

187
00:13:14,820 --> 00:13:22,340
 simple, but it confounds people who haven't practiced

188
00:13:22,340 --> 00:13:24,000
 meditation.

189
00:13:24,000 --> 00:13:30,520
 So the first thing is the ability to separate reality into

190
00:13:30,520 --> 00:13:34,000
 moments instead of things.

191
00:13:34,000 --> 00:13:41,280
 An important milestone, if you will, is when the meditator

192
00:13:41,280 --> 00:13:46,000
 makes the shift and realizes that entities don't exist.

193
00:13:46,000 --> 00:13:51,250
 From moment to moment, all that exists is experiences that

194
00:13:51,250 --> 00:13:53,000
 arise and cease.

195
00:13:53,000 --> 00:13:56,090
 When you're walking, what is real? This idea of me walking,

196
00:13:56,090 --> 00:13:57,000
 it's all up here.

197
00:13:57,000 --> 00:14:00,000
 It's something you think, "Yeah, I'm walking."

198
00:14:00,000 --> 00:14:03,000
 No, this is you thinking. This is a thought.

199
00:14:03,000 --> 00:14:07,000
 And that thought arises and ceases.

200
00:14:07,000 --> 00:14:12,000
 The right foot moving arises and ceases.

201
00:14:12,000 --> 00:14:14,210
 The left foot moving. I mean, this is intellectual. I can

202
00:14:14,210 --> 00:14:15,000
 tell you this.

203
00:14:15,000 --> 00:14:18,000
 It doesn't really help you.

204
00:14:18,000 --> 00:14:21,490
 There's two groups. Some people hear this and they think

205
00:14:21,490 --> 00:14:26,000
 they understand it conceptually, understand it rationally.

206
00:14:26,000 --> 00:14:27,990
 And the other people are saying, "Well, of course, yes.

207
00:14:27,990 --> 00:14:30,000
 This is how I see things."

208
00:14:30,000 --> 00:14:38,000
 And the other people have reached this purification of you.

209
00:14:38,000 --> 00:14:42,450
 It's not incredibly helpful that I'm describing it to

210
00:14:42,450 --> 00:14:45,000
 people who haven't meditated.

211
00:14:45,000 --> 00:14:49,000
 But I assume that everyone here is on the verge of this.

212
00:14:49,000 --> 00:14:52,850
 So I'd urge you at this point, and from this point in this

213
00:14:52,850 --> 00:14:54,000
 series of talks,

214
00:14:54,000 --> 00:14:56,360
 it's all going to be very much about your meditation

215
00:14:56,360 --> 00:14:57,000
 practice.

216
00:14:57,000 --> 00:15:00,600
 It's not meditating. This sort of talk is not going to be

217
00:15:00,600 --> 00:15:01,000
 very useful to you.

218
00:15:01,000 --> 00:15:05,000
 For some people it might even be to their detriment.

219
00:15:05,000 --> 00:15:07,680
 Because when they begin to meditate, they'll be looking and

220
00:15:07,680 --> 00:15:08,000
 trying to,

221
00:15:08,000 --> 00:15:14,000
 "Okay, he said I'm going to see this. Where is it?"

222
00:15:14,000 --> 00:15:18,000
 Mild detriment is a sense that you might obsess over it.

223
00:15:18,000 --> 00:15:26,040
 It's not prohibitive in the sense that it's going to

224
00:15:26,040 --> 00:15:29,000
 prevent you from realizing.

225
00:15:29,000 --> 00:15:35,000
 You just have to be mindful in your thinking, thinking.

226
00:15:35,000 --> 00:15:39,000
 But this comes through mindfulness.

227
00:15:39,000 --> 00:15:42,710
 The ability to understand death is probably what we call it

228
00:15:42,710 --> 00:15:43,000
.

229
00:15:43,000 --> 00:15:48,000
 Death of a person is just a concept. That's not what real

230
00:15:48,000 --> 00:15:48,000
 death is.

231
00:15:48,000 --> 00:15:50,000
 Real death is every moment.

232
00:15:50,000 --> 00:15:55,890
 Understand that everything that arises ceases after just a

233
00:15:55,890 --> 00:15:57,000
 moment.

234
00:15:57,000 --> 00:16:02,000
 What really exists is not people, places, and things.

235
00:16:02,000 --> 00:16:05,000
 It's experiences.

236
00:16:05,000 --> 00:16:10,330
 To understand that this world of concepts is quite

237
00:16:10,330 --> 00:16:11,000
 problematic

238
00:16:11,000 --> 00:16:16,000
 because it belies an underlying impermanence.

239
00:16:16,000 --> 00:16:20,000
 It masks an underlying impermanence.

240
00:16:20,000 --> 00:16:22,980
 If you say, "This person exists," this is where the shock

241
00:16:22,980 --> 00:16:24,000
 comes when they die.

242
00:16:24,000 --> 00:16:30,000
 You think, "Well, what happened to the person?"

243
00:16:30,000 --> 00:16:34,200
 When we think of things as being this, when they change, we

244
00:16:34,200 --> 00:16:35,000
 get upset.

245
00:16:35,000 --> 00:16:39,000
 When they break, when they disappear.

246
00:16:39,000 --> 00:16:43,000
 If we understand reality from a point of view of experience

247
00:16:43,000 --> 00:16:45,000
, it's quite useful.

248
00:16:45,000 --> 00:16:48,000
 There's much that comes from it, but basically what comes

249
00:16:48,000 --> 00:16:50,000
 is we don't have these expectations.

250
00:16:50,000 --> 00:16:54,230
 If you understand that stepping right, stepping left, and

251
00:16:54,230 --> 00:16:56,000
 the stomach rising and falling,

252
00:16:56,000 --> 00:17:01,370
 if you understand that these things arise and cease, there

253
00:17:01,370 --> 00:17:03,000
's nothing to hold on to.

254
00:17:03,000 --> 00:17:06,000
 When you look at a person, it's just an experience.

255
00:17:06,000 --> 00:17:10,000
 You think about your own person.

256
00:17:10,000 --> 00:17:12,000
 Who you are is just moments of experience.

257
00:17:12,000 --> 00:17:14,000
 What are you going to hold on to?

258
00:17:14,000 --> 00:17:18,000
 What will you cling to?

259
00:17:18,000 --> 00:17:20,500
 When you stop clinging, of course, this is the chain

260
00:17:20,500 --> 00:17:25,000
 reaction of stages of wisdom that come.

261
00:17:25,000 --> 00:17:31,000
 The second thing about purification of view is the ability

262
00:17:31,000 --> 00:17:35,000
 to distinguish body and mind.

263
00:17:35,000 --> 00:17:39,220
 The first part is a distinguishing body from body and mind

264
00:17:39,220 --> 00:17:40,000
 from mind,

265
00:17:40,000 --> 00:17:44,990
 meaning moments of physical experience from the next moment

266
00:17:44,990 --> 00:17:45,000
.

267
00:17:45,000 --> 00:17:48,000
 Born die, born die.

268
00:17:48,000 --> 00:17:50,000
 Mental, the same.

269
00:17:50,000 --> 00:17:54,040
 When you know that the right foot is moving, that knowledge

270
00:17:54,040 --> 00:17:56,000
 arises and ceases as well.

271
00:17:56,000 --> 00:17:58,460
 When you know the left foot is moving, that knowledge

272
00:17:58,460 --> 00:17:59,000
 arises.

273
00:17:59,000 --> 00:18:03,740
 This is a rising and ceasing moment, the discreteness of

274
00:18:03,740 --> 00:18:05,000
 phenomena.

275
00:18:05,000 --> 00:18:09,170
 The second one is the ability to distinguish physical and

276
00:18:09,170 --> 00:18:10,000
 mental.

277
00:18:10,000 --> 00:18:13,020
 It's a bit of a mind game to think, "Is the physical

278
00:18:13,020 --> 00:18:15,000
 different from the mental?"

279
00:18:15,000 --> 00:18:18,000
 That's not really so important.

280
00:18:18,000 --> 00:18:20,670
 Although for a meditator it should be conceptually quite

281
00:18:20,670 --> 00:18:22,000
 clear to say,

282
00:18:22,000 --> 00:18:25,460
 "Yes, the mind is obviously something quite different from

283
00:18:25,460 --> 00:18:26,000
 the body."

284
00:18:26,000 --> 00:18:30,320
 The physical aspect of experience is quite different from

285
00:18:30,320 --> 00:18:32,000
 the mental aspect.

286
00:18:32,000 --> 00:18:35,940
 But the real point is simply to be able to say, "That's all

287
00:18:35,940 --> 00:18:37,000
 there is."

288
00:18:37,000 --> 00:18:40,000
 What is reality made of? It really is these two things.

289
00:18:40,000 --> 00:18:42,000
 There's physical aspects clearly.

290
00:18:42,000 --> 00:18:46,480
 I mean, when I walk there's the feelings of pressure and

291
00:18:46,480 --> 00:18:50,000
 hot and cold and hard and soft.

292
00:18:50,000 --> 00:18:53,000
 But there's also clearly the awareness of it.

293
00:18:53,000 --> 00:18:56,280
 The foot can move in my mind somewhere else. I'm thinking

294
00:18:56,280 --> 00:18:57,000
 about something else entirely.

295
00:18:57,000 --> 00:19:03,000
 So clearly they're two distinct and individual entities.

296
00:19:03,000 --> 00:19:07,890
 If the mind doesn't go to the foot there's no experience of

297
00:19:07,890 --> 00:19:09,000
 walking.

298
00:19:09,000 --> 00:19:11,000
 Right?

299
00:19:11,000 --> 00:19:14,110
 By that you think, "Well, if my mind is somewhere else, am

300
00:19:14,110 --> 00:19:15,000
 I not walking?"

301
00:19:15,000 --> 00:19:18,720
 It's like if a tree falls in the forest and no one's there

302
00:19:18,720 --> 00:19:22,000
 to hear, does it really make a sound?

303
00:19:22,000 --> 00:19:27,000
 What that tells us is that, yes, the physical in some sense

304
00:19:27,000 --> 00:19:29,000
 exists independent of the mental

305
00:19:29,000 --> 00:19:33,560
 because clearly if my mind is distracted while my foot is

306
00:19:33,560 --> 00:19:35,000
 still moving.

307
00:19:35,000 --> 00:19:38,840
 It's not like reality goes on pause because the mind isn't

308
00:19:38,840 --> 00:19:40,000
 focused on it.

309
00:19:40,000 --> 00:19:43,070
 If you're driving down the street and you get distracted it

310
00:19:43,070 --> 00:19:44,000
 doesn't mean the car stops.

311
00:19:44,000 --> 00:19:47,000
 The physical reality continues.

312
00:19:47,000 --> 00:19:54,290
 But the mental reality is distinct from the physical

313
00:19:54,290 --> 00:19:56,000
 reality.

314
00:19:56,000 --> 00:20:01,000
 What's important about this is simply to destroy the notion

315
00:20:01,000 --> 00:20:08,000
 of a self or a soul

316
00:20:08,000 --> 00:20:13,000
 or a God or anything of that sort.

317
00:20:13,000 --> 00:20:17,460
 This is what I mean. This is where it really impacts our

318
00:20:17,460 --> 00:20:21,000
 views, changes our views.

319
00:20:21,000 --> 00:20:28,000
 People can't exist. Souls, selves.

320
00:20:28,000 --> 00:20:31,720
 To understand that all of these things exist really only on

321
00:20:31,720 --> 00:20:33,000
 a conceptual level.

322
00:20:33,000 --> 00:20:39,730
 They end up being groupings of experience that are

323
00:20:39,730 --> 00:20:41,000
 conveniently,

324
00:20:41,000 --> 00:20:43,000
 convenient and practical.

325
00:20:43,000 --> 00:20:45,530
 It's practical to say that this is a microphone and this is

326
00:20:45,530 --> 00:20:46,000
 a computer

327
00:20:46,000 --> 00:20:49,000
 and it's practical to know those things and not just say,

328
00:20:49,000 --> 00:20:52,000
 "Oh, here's an experience of seeing."

329
00:20:52,000 --> 00:20:55,000
 From a meditative point all you need is experience.

330
00:20:55,000 --> 00:20:59,410
 But from a practical worldly point of course you need to

331
00:20:59,410 --> 00:21:02,000
 live in the world of concept.

332
00:21:02,000 --> 00:21:05,000
 Again, these two different ways of looking at reality.

333
00:21:05,000 --> 00:21:10,370
 To cultivate wisdom you need this way of looking at the

334
00:21:10,370 --> 00:21:11,000
 world.

335
00:21:11,000 --> 00:21:17,000
 This is the first important step on the path to wisdom.

336
00:21:17,000 --> 00:21:20,000
 Ditiri-sundhi.

337
00:21:20,000 --> 00:21:25,000
 Cultivating right view or right outlook.

338
00:21:25,000 --> 00:21:32,000
 There you go. That's number three in the purifications.

339
00:21:32,000 --> 00:21:36,000
 That's our dhamma for tonight.

340
00:21:56,000 --> 00:22:03,000
 I'd rather have you look at some questions now.

341
00:22:03,000 --> 00:22:07,000
 Go for the website loans.

342
00:22:07,000 --> 00:22:33,000
 [birds chirping]

343
00:22:33,000 --> 00:22:36,950
 The website's not loading so that's an excuse not to answer

344
00:22:36,950 --> 00:22:38,000
 questions.

345
00:22:38,000 --> 00:22:41,000
 That's all for tonight. Thank you all for tuning in.

346
00:22:41,000 --> 00:22:52,370
 I got the audio feed working as well so hopefully there was

347
00:22:52,370 --> 00:22:53,000
 an audio stream

348
00:22:53,000 --> 00:22:57,180
 for anyone who might happen to be following that or

349
00:22:57,180 --> 00:23:00,000
 listening to the MP3s later.

350
00:23:00,000 --> 00:23:04,910
 Those MP3s should be if all is working as planned up on our

351
00:23:04,910 --> 00:23:06,000
 website.

352
00:23:06,000 --> 00:23:10,000
 Thank you all again for tuning in. Have a good night.

353
00:23:11,000 --> 00:23:28,000
 [birds chirping]

