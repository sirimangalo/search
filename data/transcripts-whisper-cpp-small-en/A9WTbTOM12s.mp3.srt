1
00:00:00,000 --> 00:00:04,330
 Hi, so today I'll be explaining a third technique in the

2
00:00:04,330 --> 00:00:07,440
 meditation practice and this is called mindful

3
00:00:07,440 --> 00:00:11,620
 prostration. Now prostration is something which is well

4
00:00:11,620 --> 00:00:14,520
 familiar to many Buddhists around the world

5
00:00:14,520 --> 00:00:17,840
 and many religious people around the world. Some people

6
00:00:17,840 --> 00:00:20,920
 will use it, for instance, in Thailand to

7
00:00:20,920 --> 00:00:24,160
 pay respect to one's parents or pay respect to one's

8
00:00:24,160 --> 00:00:27,000
 teachers. In some religions they will use

9
00:00:27,000 --> 00:00:30,950
 it as a means of paying respect or worshipping a God or an

10
00:00:30,950 --> 00:00:35,120
 angel or some higher divinity. In the

11
00:00:35,120 --> 00:00:38,820
 meditation practice it is a means of paying respect to the

12
00:00:38,820 --> 00:00:41,480
 meditation practice. So it's done before

13
00:00:41,480 --> 00:00:46,550
 the walking and before the sitting and it's done simply to

14
00:00:46,550 --> 00:00:50,760
 create a state of respect and humility

15
00:00:50,760 --> 00:00:55,240
 and gratitude towards the meditation practice as something

16
00:00:55,240 --> 00:00:57,960
 which is of value to us. It's also a

17
00:00:57,960 --> 00:01:02,150
 means of starting the mind on the right track or creating

18
00:01:02,150 --> 00:01:04,240
 mindfulness in the mind before we

19
00:01:04,240 --> 00:01:07,360
 actually go ahead to do the walking and then do the sitting

20
00:01:07,360 --> 00:01:09,440
. So I'll now demonstrate how to do

21
00:01:09,440 --> 00:01:14,030
 what is called a mindful prostration. First of all, I'll

22
00:01:14,030 --> 00:01:16,320
 show how to do the Thai method of

23
00:01:16,320 --> 00:01:19,410
 prostration which is used to pay respect to one's parents

24
00:01:19,410 --> 00:01:23,000
 and one's teachers. And I'll show this

25
00:01:23,000 --> 00:01:28,870
 because it will be a framework, a basis, a model for the

26
00:01:28,870 --> 00:01:32,520
 mindful prostration so you can see what

27
00:01:32,520 --> 00:01:39,020
 it should look like. So in Thailand, prostration is done

28
00:01:39,020 --> 00:01:42,640
 with one sitting on one's knees. The

29
00:01:42,640 --> 00:01:45,350
 traditional way is to sit up on one's toes but if this is

30
00:01:45,350 --> 00:01:47,920
 uncomfortable you can sit down on the

31
00:01:47,920 --> 00:01:54,770
 tops of your feet. And an ordinary prostration is performed

32
00:01:54,770 --> 00:01:58,400
 with the hands coming up to the chest,

33
00:01:58,400 --> 00:02:03,780
 then up to the forehead, between the eyebrows, and then the

34
00:02:03,780 --> 00:02:06,440
 hands go down and they will come

35
00:02:06,440 --> 00:02:10,460
 out in a W pattern with the thumbs touching and they will

36
00:02:10,460 --> 00:02:13,200
 touch the floor. And they have to be

37
00:02:13,200 --> 00:02:16,740
 far out enough so that the elbows can come to rest in front

38
00:02:16,740 --> 00:02:19,240
 of the knees, not on top of the knees or

39
00:02:19,240 --> 00:02:24,550
 to the side. When the elbows are touching in front of the

40
00:02:24,550 --> 00:02:27,360
 knees and the hands are on the floor,

41
00:02:27,360 --> 00:02:33,310
 the forehead goes down and touches the thumbs between the

42
00:02:33,310 --> 00:02:36,440
 eyebrows and back up again to the

43
00:02:36,440 --> 00:02:40,240
 chest. And this is done three times as follows.

44
00:02:40,240 --> 00:03:02,730
 Now the mindful prostration is similar and is based on the

45
00:03:02,730 --> 00:03:05,320
 Thai prostration but in this case

46
00:03:05,320 --> 00:03:09,090
 it's a meditation practice as well. So it's done slowly and

47
00:03:09,090 --> 00:03:11,520
 it's with the mind focused on this time

48
00:03:11,520 --> 00:03:15,910
 on the hand. So when the hand turns, one creates a clear

49
00:03:15,910 --> 00:03:19,240
 thought, turning. And one says it to oneself

50
00:03:19,240 --> 00:03:24,210
 three times, turning, turning, turning. When the hand

51
00:03:24,210 --> 00:03:26,960
 starts to raise, one says raising,

52
00:03:26,960 --> 00:03:31,300
 raising, raising, three times and it's in time with the

53
00:03:31,300 --> 00:03:35,680
 motion. So again, since we're practicing based

54
00:03:35,680 --> 00:03:37,990
 on what's really happening, we have to do it as the

55
00:03:37,990 --> 00:03:40,080
 movement occurs. We can't say to ourselves

56
00:03:40,080 --> 00:03:43,930
 first, "raising, raising, raising," and then raise the hand

57
00:03:43,930 --> 00:03:45,680
. Or raise first and then say,

58
00:03:45,680 --> 00:03:48,560
 "raising, raising, raising." It has to be done at the same

59
00:03:48,560 --> 00:03:52,040
 time as the movement. So starting again,

60
00:03:52,040 --> 00:03:57,490
 turning, turning, turning, raising, raising, raising,

61
00:03:57,490 --> 00:04:01,880
 touching, touching, touching, turning,

62
00:04:01,880 --> 00:04:07,290
 turning, turning, raising, raising, raising, touching,

63
00:04:07,290 --> 00:04:11,360
 touching, touching, raising, raising,

64
00:04:11,360 --> 00:04:17,910
 raising, touching, touching, lowering, lowering, lowering,

65
00:04:17,910 --> 00:04:19,840
 touching, touching,

66
00:04:19,840 --> 00:04:24,850
 touching, bending, bending, bending, lowering, lowering,

67
00:04:24,850 --> 00:04:28,240
 lowering, touching, touching,

68
00:04:28,240 --> 00:04:33,640
 touching, covering, covering, covering, lowering, lowering,

69
00:04:33,640 --> 00:04:37,440
 lowering, touching, touching, touching,

70
00:04:37,440 --> 00:04:43,350
 covering, covering, covering, bending, bending, bending,

71
00:04:43,350 --> 00:04:45,160
 touching, touching,

72
00:04:45,160 --> 00:04:51,630
 raising, raising, raising, turning, turning, turning, and

73
00:04:51,630 --> 00:04:53,440
 so on. In this point on, it repeats

74
00:04:53,440 --> 00:04:57,420
 itself. So turning, turning, turning, raising, raising,

75
00:04:57,420 --> 00:05:00,160
 raising, touching, touching, touching,

76
00:05:00,160 --> 00:05:05,440
 turning, turning, turning, raising, raising, raising,

77
00:05:05,440 --> 00:05:07,680
 touching, touching, and up again to

78
00:05:07,680 --> 00:05:13,430
 the forehead, down, and so on. It repeats this process

79
00:05:13,430 --> 00:05:16,800
 three times. Slowly, slowly each time,

80
00:05:16,800 --> 00:05:20,450
 taking the time to say turning, turning, turning, raising,

81
00:05:20,450 --> 00:05:29,240
 raising, raising, and so on. Okay? Once

82
00:05:29,240 --> 00:05:31,590
 one has done this three times and one finishes turning,

83
00:05:31,590 --> 00:05:34,560
 turning, turning, as usual, raising,

84
00:05:34,560 --> 00:05:38,040
 raising, raising, touching, touching, and so on. After the

85
00:05:38,040 --> 00:05:39,320
 third time when it's done

86
00:05:39,320 --> 00:05:42,850
 prostration, one comes back up to this point and finishes

87
00:05:42,850 --> 00:05:45,120
 off, raising, raising, raising,

88
00:05:45,120 --> 00:05:50,330
 touching, touching, touching, lowering, lowering, lowering,

89
00:05:50,330 --> 00:05:51,960
 touching, touching,

90
00:05:51,960 --> 00:05:55,080
 touching, and instead of bending, bending, bending again,

91
00:05:55,080 --> 00:05:58,080
 one brings one's hand back to one's thighs,

92
00:05:58,080 --> 00:06:03,240
 lowering, lowering, lowering, touching, touching, touching,

93
00:06:03,240 --> 00:06:06,680
 covering, covering, covering, lowering,

94
00:06:06,680 --> 00:06:10,940
 lowering, lowering, touching, touching, touching, covering,

95
00:06:10,940 --> 00:06:14,120
 covering, covering. And when one has

96
00:06:14,120 --> 00:06:16,910
 finished this, one then continues on with the walking

97
00:06:16,910 --> 00:06:18,780
 meditation. Walking right goes

98
00:06:18,780 --> 00:06:22,330
 thus, left goes thus, right goes thus to the end, and back

99
00:06:22,330 --> 00:06:23,960
 and forth. And one will generally do

100
00:06:23,960 --> 00:06:27,580
 walking for 10 minutes or 15 minutes depending on the one's

101
00:06:27,580 --> 00:06:30,560
 energy and one's willpower. After doing

102
00:06:30,560 --> 00:06:33,330
 the walking meditation, one will do the sitting meditation.

103
00:06:33,330 --> 00:06:35,080
 And it's important that these three

104
00:06:35,080 --> 00:06:38,290
 meditation techniques are done in sequence without any

105
00:06:38,290 --> 00:06:41,160
 break in between. I'll talk more about that in

106
00:06:41,160 --> 00:06:44,810
 my next video. But for now, this is a brief demonstration

107
00:06:44,810 --> 00:06:46,440
 on how to do what is called

108
00:06:46,440 --> 00:06:48,970
 mindful prostration or a way of paying respect to the

109
00:06:48,970 --> 00:06:51,320
 meditation practice and setting up one's

110
00:06:51,320 --> 00:06:54,260
 mindfulness for the walking and the sitting meditation

111
00:06:54,260 --> 00:06:56,320
 which follow. Thanks for tuning in.

112
00:06:56,320 --> 00:06:57,320
 That's all for today.

113
00:06:57,320 --> 00:06:58,320
 [

