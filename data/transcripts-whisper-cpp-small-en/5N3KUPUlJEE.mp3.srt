1
00:00:00,000 --> 00:00:03,970
 When wanting arises when I meditate, I start meditating on

2
00:00:03,970 --> 00:00:04,920
 the wanting by saying

3
00:00:04,920 --> 00:00:10,340
 wanting and wanting. The wanting becomes stronger. It seems

4
00:00:10,340 --> 00:00:10,920
 like I'm only

5
00:00:10,920 --> 00:00:13,080
 suppressing the wanting rather than getting rid of it

6
00:00:13,080 --> 00:00:14,160
 completely. How do I

7
00:00:14,160 --> 00:00:23,760
 overcome this? I'm not quite clear on the logic that you're

8
00:00:23,760 --> 00:00:24,240
 using there,

9
00:00:24,240 --> 00:00:28,240
 but when something becomes stronger that's not generally a

10
00:00:28,240 --> 00:00:28,840
 sign of

11
00:00:28,840 --> 00:00:32,200
 suppression. Suppression is when you make it weaker by

12
00:00:32,200 --> 00:00:34,560
 pushing it down. And I

13
00:00:34,560 --> 00:00:41,460
 would suggest that that's the root problem. Because, let's

14
00:00:41,460 --> 00:00:43,880
 put it simply, our

15
00:00:43,880 --> 00:00:48,750
 negative mind states are not simple. The negativity that we

16
00:00:48,750 --> 00:00:50,040
 have in our mind is

17
00:00:50,040 --> 00:00:54,280
 not simply a drawer full of greed, a drawer full of anger,

18
00:00:54,280 --> 00:00:55,040
 and a drawer full of

19
00:00:55,040 --> 00:01:01,320
 illusion. It's so insanely mixed up. And I mean think about

20
00:01:01,320 --> 00:01:02,440
 the constructs that

21
00:01:02,440 --> 00:01:06,080
 you've created in your life. It's not difficult to

22
00:01:06,080 --> 00:01:07,200
 understand how

23
00:01:07,200 --> 00:01:11,450
 complex these habits can become and these negative

24
00:01:11,450 --> 00:01:13,000
 tendencies to cling and

25
00:01:13,000 --> 00:01:20,320
 to hate and to dislike, how they can be to become, to be

26
00:01:20,320 --> 00:01:21,160
 conceited, to be

27
00:01:21,160 --> 00:01:30,720
 arrogant, all of the various different formations. Now as a

28
00:01:30,720 --> 00:01:31,440
 result you can

29
00:01:31,440 --> 00:01:35,280
 never deal with a simple, you can never deal with a single

30
00:01:35,280 --> 00:01:37,000
 emotion and say I'm

31
00:01:37,000 --> 00:01:39,290
 just going to focus on this one, poof, it's going to

32
00:01:39,290 --> 00:01:41,200
 disappear. You've got

33
00:01:41,200 --> 00:01:46,000
 more issues going on there. The pleasure that you're

34
00:01:46,000 --> 00:01:46,920
 experiencing, if

35
00:01:46,920 --> 00:01:49,730
 it's becoming stronger in meditation, then that's a clear

36
00:01:49,730 --> 00:01:50,600
 sign that in your

37
00:01:50,600 --> 00:01:53,640
 daily life you're suppressing it. And you're probably

38
00:01:53,640 --> 00:01:54,720
 suppressing it with all

39
00:01:54,720 --> 00:02:02,450
 sorts of things. Probably with guilt, probably with anger,

40
00:02:02,450 --> 00:02:02,880
 which

41
00:02:02,880 --> 00:02:07,310
 is a part and parcel of guilt, with delusion, the idea that

42
00:02:07,310 --> 00:02:07,960
 you are in

43
00:02:07,960 --> 00:02:11,710
 control of your desires. So when I want to turn it on, it's

44
00:02:11,710 --> 00:02:12,840
 on. When I want to turn

45
00:02:12,840 --> 00:02:16,480
 it off, it's off. I love this person, but I won't go near

46
00:02:16,480 --> 00:02:18,360
 that person. I love

47
00:02:18,360 --> 00:02:21,670
 my wife, but these angels in heaven, when I see these nymph

48
00:02:21,670 --> 00:02:22,680
s in heaven, I won't

49
00:02:22,680 --> 00:02:27,360
 become attracted to them. So there's delusion, there's many

50
00:02:27,360 --> 00:02:27,880
 things that

51
00:02:27,880 --> 00:02:34,240
 are suppressing the wanting. When you're practicing

52
00:02:34,240 --> 00:02:36,560
 meditation, what you're

53
00:02:36,560 --> 00:02:38,600
 experiencing is actually a good thing, because you've

54
00:02:38,600 --> 00:02:39,680
 stopped suppressing it.

55
00:02:39,680 --> 00:02:42,240
 That's why it's getting stronger. It's not because you're

56
00:02:42,240 --> 00:02:44,040
 meditating on it, not

57
00:02:44,040 --> 00:02:47,630
 directly. It's not like meditation is growing more desire,

58
00:02:47,630 --> 00:02:48,600
 but you've got a

59
00:02:48,600 --> 00:02:54,650
 huge lump of desire inside, like just massive desire. And

60
00:02:54,650 --> 00:02:55,520
 that means you have

61
00:02:55,520 --> 00:03:01,200
 massive... well, you have the tendency or the propensity to

62
00:03:01,200 --> 00:03:02,880
 give rise to massive

63
00:03:02,880 --> 00:03:09,720
 amounts of desire, you know, continuous succession of mind

64
00:03:09,720 --> 00:03:11,280
 states, of desire

65
00:03:11,280 --> 00:03:14,900
 based mind states. That's the habit that you've developed

66
00:03:14,900 --> 00:03:17,560
 over the years. Rather

67
00:03:17,560 --> 00:03:20,200
 than dealing with it, you've been suppressing it, and that

68
00:03:20,200 --> 00:03:20,960
's why you don't

69
00:03:20,960 --> 00:03:24,840
 experience it. It's like you get some garbage and you throw

70
00:03:24,840 --> 00:03:25,120
 it in your

71
00:03:25,120 --> 00:03:28,090
 closet, and you get some more garbage, and you just get

72
00:03:28,090 --> 00:03:29,400
 this habit of throwing

73
00:03:29,400 --> 00:03:33,740
 garbage into your closet. Well, guess what? You end up with

74
00:03:33,740 --> 00:03:34,560
 a closet full of

75
00:03:34,560 --> 00:03:37,630
 garbage, and then meditation is like you open the closet

76
00:03:37,630 --> 00:03:38,680
 and you get to have it

77
00:03:38,680 --> 00:03:41,200
 all fall down on you. And then you're like, "Oh, I'll close

78
00:03:41,200 --> 00:03:42,720
 that up again." And then

79
00:03:42,720 --> 00:03:45,450
 you think, "What's wrong with this meditation? It's

80
00:03:45,450 --> 00:03:46,960
 bringing out

81
00:03:46,960 --> 00:03:51,200
 all this bad stuff." So you have a choice. You can keep it

82
00:03:51,200 --> 00:03:51,360
 in the

83
00:03:51,360 --> 00:03:54,840
 closet and, in fact, keep throwing more in. Or you can

84
00:03:54,840 --> 00:03:56,000
 train yourself, first of

85
00:03:56,000 --> 00:04:00,420
 all, to stop doing that and start to empty out the closet,

86
00:04:00,420 --> 00:04:01,240
 deal with it when

87
00:04:01,240 --> 00:04:07,180
 it arises. In that sense, there's nothing really wrong with

88
00:04:07,180 --> 00:04:09,040
 the experiences that

89
00:04:09,040 --> 00:04:14,800
 you're having. And another thing I would note is that it's

90
00:04:14,800 --> 00:04:15,700
 arguable that the

91
00:04:15,700 --> 00:04:20,800
 desire is not that which is increasing. What is increasing

92
00:04:20,800 --> 00:04:21,340
 is the

93
00:04:21,340 --> 00:04:25,220
 physical manifestations of the desire in the brain, in the

94
00:04:25,220 --> 00:04:26,560
 body. So a good example

95
00:04:26,560 --> 00:04:32,880
 is with lust, with physical attachment to the body. 99% of

96
00:04:32,880 --> 00:04:33,080
 it is

97
00:04:33,080 --> 00:04:38,640
 not the desire, the chemical reactions, the bodily changes,

98
00:04:38,640 --> 00:04:39,840
 the hormones, and so

99
00:04:39,840 --> 00:04:43,530
 on. This is all totally physical. It's not the desire. And

100
00:04:43,530 --> 00:04:45,080
 this is why it's so

101
00:04:45,080 --> 00:04:52,720
 important in regards to attachment to sexuality and that

102
00:04:52,720 --> 00:04:54,500
 sort of thing, to be

103
00:04:54,500 --> 00:04:57,860
 able to break up the experience into its component parts.

104
00:04:57,860 --> 00:04:58,720
 When you meditate on the

105
00:04:58,720 --> 00:05:03,020
 desire, you're allowing your body to give rise to these

106
00:05:03,020 --> 00:05:03,440
 states that

107
00:05:03,440 --> 00:05:06,340
 normally you would suppress. You would create a great

108
00:05:06,340 --> 00:05:07,640
 tension in the body that

109
00:05:07,640 --> 00:05:11,720
 would suppress so many of the systems that are now given

110
00:05:11,720 --> 00:05:12,440
 the chance to

111
00:05:12,440 --> 00:05:16,430
 release. And so the chemicals will arise and you'll have

112
00:05:16,430 --> 00:05:16,960
 great pleasure.

113
00:05:16,960 --> 00:05:21,420
 And then there arises the desire. But it can be that when

114
00:05:21,420 --> 00:05:23,000
 you're meditating,

115
00:05:23,000 --> 00:05:26,600
 the delusion and the guilt and so on is preventing you from

116
00:05:26,600 --> 00:05:27,080
 seeing this.

117
00:05:27,080 --> 00:05:31,130
 But when you're meditating, the desire can actually

118
00:05:31,130 --> 00:05:32,640
 disappear and you can feel

119
00:05:32,640 --> 00:05:35,280
 such great pleasure that would normally have been

120
00:05:35,280 --> 00:05:37,080
 associated with desire. But

121
00:05:37,080 --> 00:05:40,320
 because of your saying wanting, wanting, or more important,

122
00:05:40,320 --> 00:05:42,040
 saying happy, happy, or

123
00:05:42,040 --> 00:05:46,860
 pleasure, pleasure, or even feeling, feeling, and so on, is

124
00:05:46,860 --> 00:05:48,720
 that what's

125
00:05:48,720 --> 00:05:51,550
 occurring is this release, this bodily release. And by

126
00:05:51,550 --> 00:05:52,780
 bodily, we also mean the

127
00:05:52,780 --> 00:05:56,010
 brain, so the chemical releases and so on, that are now

128
00:05:56,010 --> 00:05:57,280
 given the chance because

129
00:05:57,280 --> 00:06:00,760
 there's less tension in the body. That's one really good

130
00:06:00,760 --> 00:06:01,000
 way of

131
00:06:01,000 --> 00:06:05,840
 understanding this, is that you've been suppressing or

132
00:06:05,840 --> 00:06:10,120
 restricting the flow of

133
00:06:10,120 --> 00:06:15,320
 bodily systems. And that's what's coming out. Desire

134
00:06:15,320 --> 00:06:19,600
 arguably is only a

135
00:06:19,600 --> 00:06:22,770
 static mind state. Just say we have more desire, less

136
00:06:22,770 --> 00:06:25,040
 desire. I'm not sure that

137
00:06:25,040 --> 00:06:28,120
 such a thing exists in Buddhist thought. Desire is the

138
00:06:28,120 --> 00:06:28,840
 moment where you like

139
00:06:28,840 --> 00:06:33,570
 something. Now if you like something continuously, then you

140
00:06:33,570 --> 00:06:34,640
 could call it

141
00:06:34,640 --> 00:06:36,820
 strong desire, but it's still only moment to moment to

142
00:06:36,820 --> 00:06:43,200
 moment desire. And that's

143
00:06:43,200 --> 00:06:46,150
 only a very small part, that's a very mental part of it. It

144
00:06:46,150 --> 00:06:47,040
's where the mind

145
00:06:47,040 --> 00:06:50,990
 creates partiality and says that this is positive,

146
00:06:50,990 --> 00:06:52,680
 identifies something,

147
00:06:52,680 --> 00:06:56,330
 categorizes it as a positive experience. It sounds like at

148
00:06:56,330 --> 00:06:57,320
 that moment you're

149
00:06:57,320 --> 00:06:59,550
 probably having a lot of negative experiences because you

150
00:06:59,550 --> 00:06:59,920
 have the

151
00:06:59,920 --> 00:07:03,160
 suppression tendencies. You know, more greed and you think

152
00:07:03,160 --> 00:07:04,200
 that's a bad thing,

153
00:07:04,200 --> 00:07:06,920
 right? These feelings are coming out more and that's a bad

154
00:07:06,920 --> 00:07:08,800
 thing. So actually, you

155
00:07:08,800 --> 00:07:12,500
 don't like it. You're not giving rise to so much greed. You

156
00:07:12,500 --> 00:07:13,320
're giving rise to a

157
00:07:13,320 --> 00:07:15,670
 lot of anger, which is even worse because now you're mixing

158
00:07:15,670 --> 00:07:16,600
 them up and creating

159
00:07:16,600 --> 00:07:22,280
 more complications. So it's very important, and this is in

160
00:07:22,280 --> 00:07:22,600
 the video

161
00:07:22,600 --> 00:07:25,560
 that this very famous video, it's only famous because of

162
00:07:25,560 --> 00:07:26,240
 the title, on

163
00:07:26,240 --> 00:07:29,940
 pornography and masturbation. As I said, you know, it's

164
00:07:29,940 --> 00:07:31,280
 important to let the lust

165
00:07:31,280 --> 00:07:34,290
 come up and to focus on it. Mostly it has nothing to do

166
00:07:34,290 --> 00:07:37,040
 with the desire, it's just

167
00:07:37,040 --> 00:07:39,800
 chemical reactions in the body. And to be able to see that,

168
00:07:39,800 --> 00:07:41,280
 it's a profound, it can

169
00:07:41,280 --> 00:07:44,830
 actually lead to great physical and mental pleasure. It can

170
00:07:44,830 --> 00:07:45,480
 lead to great

171
00:07:45,480 --> 00:07:48,520
 happiness. It's a release because you don't have to feel

172
00:07:48,520 --> 00:07:49,720
 guilty anymore. You

173
00:07:49,720 --> 00:07:55,760
 know, it's just an experience. And it has nothing to do

174
00:07:55,760 --> 00:07:56,680
 directly with

175
00:07:56,680 --> 00:07:59,490
 desire. The desire is that little moment where you say, "

176
00:07:59,490 --> 00:08:01,480
This is good." If you're

177
00:08:01,480 --> 00:08:03,680
 not saying that, if you're just experiencing it, this

178
00:08:03,680 --> 00:08:04,440
 happiness, this

179
00:08:04,440 --> 00:08:10,840
 pleasure, this chemicals, whatever, then you become free.

180
00:08:10,840 --> 00:08:11,440
 And when it's

181
00:08:11,440 --> 00:08:19,720
 gone, it's gone. It doesn't trouble you. So you could read,

182
00:08:19,720 --> 00:08:19,840
 you could watch

183
00:08:19,840 --> 00:08:23,000
 that video on pornography and masturbation, I think,

184
00:08:23,000 --> 00:08:23,600
 because it's also

185
00:08:23,600 --> 00:08:26,360
 about addiction in general. It talks about some of these

186
00:08:26,360 --> 00:08:28,160
 things, and

187
00:08:28,160 --> 00:08:31,210
 hopefully what I've said now is basically along the same

188
00:08:31,210 --> 00:08:32,400
 lines and useful

189
00:08:32,400 --> 00:08:38,540
 as well. Go ahead. I think you already said it in another

190
00:08:38,540 --> 00:08:41,080
 way, but my

191
00:08:41,080 --> 00:08:45,990
 first thought on this was that when the wanting seems to

192
00:08:45,990 --> 00:08:48,280
 become stronger, it can

193
00:08:48,280 --> 00:08:56,100
 as well be that you just see it more clearly. That's

194
00:08:56,100 --> 00:08:57,240
 another thing, right?

195
00:08:57,240 --> 00:09:02,710
 So it's actually, it has been there all the time, but you

196
00:09:02,710 --> 00:09:04,880
 didn't see it. You

197
00:09:04,880 --> 00:09:09,890
 covered it up and you thought it is whatever, but now you

198
00:09:09,890 --> 00:09:10,640
 see it as

199
00:09:10,640 --> 00:09:16,500
 wanting, as what it really is. So that's important to know,

200
00:09:16,500 --> 00:09:17,600
 I think.

201
00:09:17,600 --> 00:09:25,640
 Yeah. Because normally we have this self-created amnesia,

202
00:09:25,640 --> 00:09:26,920
 right? We don't

203
00:09:26,920 --> 00:09:29,840
 want to think about it. So when anger comes up, no. I'm not

204
00:09:29,840 --> 00:09:30,680
 supposed to get angry.

205
00:09:30,680 --> 00:09:36,000
 When greed comes, no. So we don't keep that in mind. "Oh, I

206
00:09:36,000 --> 00:09:36,600
 was angry

207
00:09:36,600 --> 00:09:40,300
 there." But we can get angry a million times a day and not

208
00:09:40,300 --> 00:09:41,120
 really remember

209
00:09:41,120 --> 00:09:45,650
 it, right? Because we're constantly throwing it under the

210
00:09:45,650 --> 00:09:46,400
 cup. "No, I didn't

211
00:09:46,400 --> 00:09:48,720
 get angry. No, I'm not going to get angry." But we're

212
00:09:48,720 --> 00:09:50,880
 already angry. It's just our

213
00:09:50,880 --> 00:09:58,200
 reaction that is sort of switching the subject, trying to

214
00:09:58,200 --> 00:09:58,880
 avoid the

215
00:09:58,880 --> 00:10:01,690
 subject. So in meditation, it's not that we're getting

216
00:10:01,690 --> 00:10:03,600
 angry more, it's that

217
00:10:03,600 --> 00:10:05,850
 we're letting it come up. We're thinking, "Okay, let's see

218
00:10:05,850 --> 00:10:07,680
 what anger is like." And boom, it

219
00:10:07,680 --> 00:10:10,970
 comes up and boom, then there's the physical. And the same

220
00:10:10,970 --> 00:10:12,840
 with lust. "Okay, let

221
00:10:12,840 --> 00:10:17,390
 it come up and look at it." And this is very clear that

222
00:10:17,390 --> 00:10:19,160
 most or many people miss

223
00:10:19,160 --> 00:10:21,360
 about the Buddha's teaching. If you read the Satipatanasr

224
00:10:21,360 --> 00:10:22,560
uta, what did the Buddha

225
00:10:22,560 --> 00:10:25,360
 say about these things? When you have sensual desire, you

226
00:10:25,360 --> 00:10:26,680
 know to yourself,

227
00:10:26,680 --> 00:10:30,760
 "I have sensual desire." There is sensual desire in the

228
00:10:30,760 --> 00:10:31,560
 mind. When

229
00:10:31,560 --> 00:10:35,600
 it's not present, you know it's not present. This is very

230
00:10:35,600 --> 00:10:38,850
 important. This creates the equanimity that we're talking

231
00:10:38,850 --> 00:10:40,760
 about. It allows you

232
00:10:40,760 --> 00:10:45,160
 to see the progression of states. It allows you to see how

233
00:10:45,160 --> 00:10:49,880
 greed works,

234
00:10:49,880 --> 00:10:53,800
 and anger and so on, and how the mind works, to be

235
00:10:53,800 --> 00:10:57,000
 objective about it. So yeah,

236
00:10:57,000 --> 00:11:00,580
 very clear. It may not be that it's coming more, it's just

237
00:11:00,580 --> 00:11:01,400
 that you're

238
00:11:01,400 --> 00:11:05,440
 actually not only more aware of it, but I would say even

239
00:11:05,440 --> 00:11:07,280
 further, you're

240
00:11:07,280 --> 00:11:11,600
 admitting it. Because we pretend we weren't. We say, "I'm

241
00:11:11,600 --> 00:11:12,120
 not an angry

242
00:11:12,120 --> 00:11:15,570
 person," but it's because we suppress it all. And then

243
00:11:15,570 --> 00:11:16,960
 meditation makes us angry

244
00:11:16,960 --> 00:11:21,360
 people again, and we get fed up about the meditation.

245
00:11:22,720 --> 00:11:27,000
 There's...

