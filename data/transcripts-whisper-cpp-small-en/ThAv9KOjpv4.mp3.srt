1
00:00:00,000 --> 00:00:04,070
 Hello, welcome back to Ask a Monk. Here I have a couple

2
00:00:04,070 --> 00:00:07,000
 more days before I have to arrange

3
00:00:07,000 --> 00:00:11,760
 for my travel to America and I'm going to take some time

4
00:00:11,760 --> 00:00:14,240
 out to go off and practice

5
00:00:14,240 --> 00:00:22,880
 alone. But in the meantime, I've got some time to answer a

6
00:00:22,880 --> 00:00:25,340
 couple of questions. So I'm

7
00:00:25,340 --> 00:00:29,730
 going to go back. I'm not going to open Ask a Monk up for

8
00:00:29,730 --> 00:00:32,340
 new questions because it looks

9
00:00:32,340 --> 00:00:36,050
 like there's over 100. I can't tell exactly how many

10
00:00:36,050 --> 00:00:38,320
 questions haven't been answered,

11
00:00:38,320 --> 00:00:41,760
 but it looks like there's over 100 that I haven't answered.

12
00:00:41,760 --> 00:00:43,080
 So I think that's enough

13
00:00:43,080 --> 00:00:47,840
 for a while and hopefully I'll get through some of them in

14
00:00:47,840 --> 00:00:50,080
 the next little while. So

15
00:00:51,200 --> 00:00:55,660
 the next question I'm going to answer is a question. I'm

16
00:00:55,660 --> 00:00:57,760
 currently reading a book titled

17
00:00:57,760 --> 00:01:02,180
 If You Meet the Buddha on the Road, Kill Him. My question

18
00:01:02,180 --> 00:01:04,600
 is, do you agree or disagree that

19
00:01:04,600 --> 00:01:08,970
 enlightenment already exists in all of us without having it

20
00:01:08,970 --> 00:01:11,600
 taught by an outside influence?

21
00:01:16,440 --> 00:01:21,150
 As with many of the things that we talk about in Buddhism,

22
00:01:21,150 --> 00:01:23,440
 we have to overcome this idea

23
00:01:23,440 --> 00:01:28,560
 of reifying things or giving an identity to things or an

24
00:01:28,560 --> 00:01:31,480
 entity, turning things into an

25
00:01:31,480 --> 00:01:40,400
 entity, let's put it that way. So when you talk about

26
00:01:40,400 --> 00:01:42,360
 enlightenment existing inside of

27
00:01:42,360 --> 00:01:46,970
 us, it kind of sounds like, and I'm not trying to criticize

28
00:01:46,970 --> 00:01:49,040
 you, but this is what we often

29
00:01:49,040 --> 00:01:53,150
 do is we hear about these things so much that we start to

30
00:01:53,150 --> 00:01:56,000
 look at them as things, as entities

31
00:01:56,000 --> 00:01:58,860
 in and of themselves. Like you could cut yourself open and

32
00:01:58,860 --> 00:02:00,680
 you'd find enlightenment, which obviously

33
00:02:00,680 --> 00:02:04,740
 isn't what you meant, but that's kind of what the

34
00:02:04,740 --> 00:02:07,680
 implication is or what the, if you hold

35
00:02:09,680 --> 00:02:13,380
 that kind of a concept, that's really what you're getting

36
00:02:13,380 --> 00:02:15,080
 at is that the idea is that

37
00:02:15,080 --> 00:02:20,380
 there's something inside of us. And what that would be, I'm

38
00:02:20,380 --> 00:02:21,680
 not sure, it's like the idea

39
00:02:21,680 --> 00:02:25,210
 that people say of a soul and that there is a soul inside

40
00:02:25,210 --> 00:02:27,040
 of us and the soul that goes

41
00:02:27,040 --> 00:02:31,490
 from one body to the next, the self, the ideas of God and

42
00:02:31,490 --> 00:02:33,040
 so on. Because in Buddhism, this

43
00:02:33,040 --> 00:02:37,500
 is a core concept, the idea of deconstructing identities

44
00:02:37,500 --> 00:02:40,040
 and entities and understanding

45
00:02:40,040 --> 00:02:48,420
 things scientifically. So this is important in this

46
00:02:48,420 --> 00:02:50,760
 question particularly because what

47
00:02:50,760 --> 00:02:55,160
 do we mean by the word enlightenment? And there's the word

48
00:02:55,160 --> 00:02:57,760
 nirvana and nirvana is something

49
00:02:58,280 --> 00:03:01,800
 that we often look at as an entity as well. We think of it

50
00:03:01,800 --> 00:03:03,680
 as a state or a realm. There's

51
00:03:03,680 --> 00:03:07,990
 a question coming up about the realm of nirvana, which can

52
00:03:07,990 --> 00:03:10,680
 really be misleading because it's

53
00:03:10,680 --> 00:03:18,000
 really just a name for an experience or a change that

54
00:03:18,000 --> 00:03:19,600
 occurs. It's not in and of itself

55
00:03:23,120 --> 00:03:26,410
 anything. Enlightenment, the word enlightenment, even if

56
00:03:26,410 --> 00:03:28,520
 you just look it up in the dictionary,

57
00:03:28,520 --> 00:03:32,930
 which is really, there's nothing secret about what the word

58
00:03:32,930 --> 00:03:35,520
 enlightenment means. It simply

59
00:03:35,520 --> 00:03:42,840
 means wisdom or the awakening to some realization. So then

60
00:03:42,840 --> 00:03:42,840
 in answer to your question, does enlightenment

61
00:03:42,840 --> 00:03:49,420
 exist inside of us? Well, no it doesn't because realization

62
00:03:49,420 --> 00:03:52,480
 is something that has to come

63
00:03:52,480 --> 00:03:55,030
 to you. It's something that has to arise. It's not

64
00:03:55,030 --> 00:03:56,760
 something that is already inside

65
00:03:56,760 --> 00:03:59,640
 of you. Either you've realized it or you haven't. If you've

66
00:03:59,640 --> 00:04:00,960
 already realized it, then you're

67
00:04:00,960 --> 00:04:03,400
 enlightened. If you haven't realized it, then you're not

68
00:04:03,400 --> 00:04:05,600
 enlightened. So for all of those

69
00:04:05,600 --> 00:04:09,370
 people who identify themselves as being unenlightened and

70
00:04:09,370 --> 00:04:12,600
 who could be identified as being unenlightened,

71
00:04:12,600 --> 00:04:15,220
 it's because they haven't realized a certain thing. So that

72
00:04:15,220 --> 00:04:17,360
 realization doesn't exist inside

73
00:04:17,360 --> 00:04:21,450
 of them. Unless you think of it as some kind of entity,

74
00:04:21,450 --> 00:04:24,360
 realization is something that occurs

75
00:04:24,360 --> 00:04:27,910
 in the mind. It's something that comes to you. There's no

76
00:04:27,910 --> 00:04:29,480
 secret hidden spot inside

77
00:04:29,480 --> 00:04:33,480
 of us where it exists. Either you realize something or you

78
00:04:33,480 --> 00:04:36,120
 don't. So this is one part of the question.

79
00:04:36,120 --> 00:04:39,830
 The other part is whether you need someone else to enlight

80
00:04:39,830 --> 00:04:41,760
en you. And of course the answer

81
00:04:41,760 --> 00:04:46,300
 is no. You can realize the truth obviously by yourself.

82
00:04:46,300 --> 00:04:48,760
 There's no rules. There's no

83
00:04:48,760 --> 00:04:53,620
 rules to enlightenment. There's no rules to existence. The

84
00:04:53,620 --> 00:04:55,920
 universe has a certain nature.

85
00:04:55,920 --> 00:04:59,780
 When you come to understand this nature, when you come to

86
00:04:59,780 --> 00:05:02,560
 understand how the universe works,

87
00:05:02,560 --> 00:05:06,610
 that's enlightenment. And it can come to anyone. It's a

88
00:05:06,610 --> 00:05:09,560
 thousand or a million times easier

89
00:05:09,560 --> 00:05:12,420
 when you're talking to someone who's already become

90
00:05:12,420 --> 00:05:14,640
 enlightened or is on the path to becoming

91
00:05:14,640 --> 00:05:18,710
 enlightened. There are many stages whereby you are able to

92
00:05:18,710 --> 00:05:20,320
 let go, where you're able

93
00:05:20,320 --> 00:05:25,030
 to see the freedom from suffering and get a glimpse of it

94
00:05:25,030 --> 00:05:27,320
 and start to open yourself

95
00:05:27,320 --> 00:05:31,840
 up to this realization. To learn from such a person is

96
00:05:31,840 --> 00:05:34,640
 obviously far more beneficial,

97
00:05:35,000 --> 00:05:40,660
 far easier than trying to find it on your own. But there

98
00:05:40,660 --> 00:05:42,000
 are many different paths and many

99
00:05:42,000 --> 00:05:44,840
 different people and there's a lot of theories and

100
00:05:44,840 --> 00:05:47,240
 philosophies surrounding this. Some people

101
00:05:47,240 --> 00:05:50,280
 say that you should work until you're able to realize it

102
00:05:50,280 --> 00:05:52,080
 for yourself. They want to be

103
00:05:52,080 --> 00:05:57,490
 able to understand it for themselves without any help from

104
00:05:57,490 --> 00:05:59,080
 other beings. And then there's

105
00:05:59,080 --> 00:06:01,640
 the whole issue of what it is that you have to realize in

106
00:06:01,640 --> 00:06:03,280
 order to become enlightened.

107
00:06:03,280 --> 00:06:07,050
 So I'm not going to go into the details or the various

108
00:06:07,050 --> 00:06:09,280
 ideas people have about what is

109
00:06:09,280 --> 00:06:12,420
 enlightenment. Let's just leave it at that where

110
00:06:12,420 --> 00:06:15,480
 enlightenment is realizing and a realization

111
00:06:15,480 --> 00:06:20,840
 of the nature of the universe and the nature of reality.

112
00:06:20,840 --> 00:06:22,480
 And in Buddhism, or as I understand

113
00:06:22,480 --> 00:06:26,560
 it, this is directly related to an understanding of

114
00:06:26,560 --> 00:06:30,040
 happiness and suffering. What is it that

115
00:06:30,040 --> 00:06:35,120
 causes suffering? An understanding that leads us to become

116
00:06:35,120 --> 00:06:37,040
 free from suffering, leads us

117
00:06:37,040 --> 00:06:41,120
 to give up those things, those attitudes and behaviors that

118
00:06:41,120 --> 00:06:43,880
 are causing us suffering. So

119
00:06:43,880 --> 00:06:47,610
 it's not simply a realization about the nature of the

120
00:06:47,610 --> 00:06:50,360
 cosmos or galaxies and solar systems

121
00:06:50,360 --> 00:06:54,610
 of physics and the Big Bang and so on. It's specifically a

122
00:06:54,610 --> 00:06:56,800
 realization which frees you

123
00:06:56,800 --> 00:06:59,970
 from suffering because there are a lot of realizations and

124
00:06:59,970 --> 00:07:01,600
 understandings that may not

125
00:07:01,600 --> 00:07:08,260
 do that. So this is what I understand by enlightenment. So

126
00:07:08,260 --> 00:07:08,600
 basically the answer is first of all that

127
00:07:08,600 --> 00:07:14,750
 no it doesn't exist inside of all of us, but yes it can be

128
00:07:14,750 --> 00:07:18,040
 attained by oneself without

129
00:07:18,040 --> 00:07:23,860
 another person to guide one. But the point is that it's a

130
00:07:23,860 --> 00:07:25,040
 lot easier when you have people,

131
00:07:25,560 --> 00:07:28,940
 friends. It doesn't have to be just a teacher-student

132
00:07:28,940 --> 00:07:31,920
 relationship. Friends who are also striving

133
00:07:31,920 --> 00:07:36,500
 for enlightenment or friends who have gone farther along

134
00:07:36,500 --> 00:07:38,760
 the path than oneself can be

135
00:07:38,760 --> 00:07:43,130
 a great help. So that's a good question because I think it

136
00:07:43,130 --> 00:07:44,720
's important that we talk and understand

137
00:07:44,720 --> 00:07:48,400
 what we mean by enlightenment so that we're not looking

138
00:07:48,400 --> 00:07:50,540
 around for it or sitting around

139
00:07:50,540 --> 00:07:54,590
 waiting for it to suddenly appear or so on or to rise out

140
00:07:54,590 --> 00:07:56,720
 of its cave, cavity inside

141
00:07:56,720 --> 00:08:00,440
 of us or something to open up like as though we're a flower

142
00:08:00,440 --> 00:08:02,400
 or something. It's a realization

143
00:08:02,400 --> 00:08:06,500
 and it comes from study and practice and study meaning

144
00:08:06,500 --> 00:08:09,400
 study of oneself and study of reality.

145
00:08:09,400 --> 00:08:12,550
 As you look at reality and look at your emotions and your

146
00:08:12,550 --> 00:08:14,280
 addictions and so on and come to

147
00:08:14,280 --> 00:08:17,520
 understand them simply seeing them for what they are, then

148
00:08:17,520 --> 00:08:19,280
 it's a realization that comes.

149
00:08:19,280 --> 00:08:21,970
 This is enlightenment. It's a realization that there's

150
00:08:21,970 --> 00:08:23,480
 nothing worth clinging to, that

151
00:08:23,480 --> 00:08:27,850
 all of our attachments are not leading us anywhere closer

152
00:08:27,850 --> 00:08:30,080
 to happiness. They're only

153
00:08:30,080 --> 00:08:34,460
 leading us to misery, dissatisfaction and further and

154
00:08:34,460 --> 00:08:37,080
 further addiction, onward and

155
00:08:37,080 --> 00:08:40,960
 onward forever. So I hope that helps. Thanks for the

156
00:08:40,960 --> 00:08:41,360
 question.

