 Hi and welcome back to Ask a Monk. After a long break I'm
 back and I'll be trying to give regular teachings and
 regular updates to the Ask a Monk program.
 I've got lots of questions to answer now so hopefully I'll
 get around to some of them in the near future.
 I'm settling down here in Sri Lanka so you're going to see
 hopefully some of the Sri Lankan countryside in these next
 videos.
 So the next question comes from Foster's Blue who says, "I
 find my interest in reading and focus in Zen, also Tibetan
 Buddhism and Christianity.
 Interest in Islam and Sufi writings. Paths cause me to
 wonder if I will have to choose or may I learn from various
 paths?"
 That's a really good question. It's one that was asked
 quite often in the Buddha's time because like today in that
 time there were many different paths, many different
 teachers.
 And I think it's important to agree that, I think most of
 us can agree that it's unhealthy and unuseful to dog
matically cling to a specific path and say this path is
 right and all others are wrong.
 And this isn't what the Buddha did. The Buddha himself was
 a very open-minded person who said that some of the things
 that other teachers teach we agree with.
 Some of the things that other teachers teach we don't agree
 with.
 And so he was never going to dogmatically say only my
 teachings are right and every other teaching is wrong.
 The point is that you can find wisdom everywhere and as you
 say all of these paths have something interesting and
 something true in them.
 So on the one hand it's obviously wrong to dogmatically
 stick to a specific path.
 On the other hand what you often see is nowadays as we
 start to learn more about the various religious traditions
 and as they come into contact you'll find people trying to
 smooth things over and make things easy by saying that all
 traditions are the same or all traditions have some equal
 value.
 And this the Buddha didn't agree with and this was one of
 the things that the Buddha tried very clearly to rectify
 this belief that any teacher who said they were enlightened
 was enlightened or so on.
 Or all teachings should be given an equal footing.
 And this is what we've done today. We've established the
 various religious traditions up as the world's major
 religions and we don't want to make any distinction between
 them which of course is unhealthy and improper.
 We should see them for what they are and scientifically
 take them apart and look at them in all their pieces and
 come to see what are their strengths and their weaknesses
 and compare them.
 And this is really what I would recommend is that you
 compare objectively the various religious traditions.
 What the Buddha said was that if you do this that you'll
 come to see that his teaching is the most deep, profound
 and the one that leads to the ultimate goal.
 So this is a very bold statement and it's one that I would
 say most religious traditions make.
 So the Buddha was not unlike other teachers in saying that
 his was the best.
 But what he said is that rather than accepting his teaching
 dogmatically, this was an inappropriate thing to do.
 One should examine objectively all of the religious
 traditions and decide which one is best.
 The Buddha simply said that you'll come to see that the
 most comprehensive, objective, scientific and practical in
 terms of verifiable in terms of bringing results.
 So we should never simply try to pick and choose from
 various religious traditions.
 We should understand that it could very well be that some
 of the teachers or the leaders of these various religious
 traditions were not on the same level.
 That just because someone is the leader of a religion or
 the leader of some group or because it is well liked by the
 majority of the people in the world or followed by a large
 number of people.
 It doesn't mean that it is true. It's possible that people
 are deluding themselves.
 It's possible that the majority of people are following the
 wrong path.
 And objectively and scientifically we should never take
 something simply on faith or even on the acceptance by the
 majority.
 We should study and examine it and try to see where it
 leads and what it brings and practice it and see for
 ourselves what it leads to.
 The point being that if you pick and choose from various
 traditions, now supposing that certain teachings are not in
 line with the truths,
 supposing that there are certain teachings that are leading
 you in the wrong direction,
 then they're going to come in conflict with those teachings
 that are leading you to the truth, leading you to reality,
 leading you to understanding, to enlightenment.
 So rather than simply picking and choosing, one should
 eventually either make up one's mind that none of the
 teachings, no teacher,
 and none of the teachings that exist in the world lead one
 to the ultimate realization of the truth and have a full
 and complete path for me to follow.
 And therefore I should make do with what I have and find
 the true path myself.
 Or accept that one of the teachings, one of the paths, one
 of the teachers that has taught since the beginning of time
 did have the right answer
 and did teach the way out of suffering and the way to true
 realization of reality and understanding and enlightenment
 and so on.
 And follow that path once you have explored and understood
 and examined.
 Because if you find such a path, then there's no reason to
 take from other paths, except sort of ancillary, to sort of
 somehow add something to the main path.
 Rather than going about coming and going from one path to
 the other, you have the right path in front of you. Why
 would you pick and choose when, quite likely,
 these other paths that you've decided that are not leading
 to ultimate reality and ultimate understanding are just
 going to conflict with this one.
 Now, it might be that some people say that there are
 different paths to the same goal.
 And I'm willing to accept this, that there very well could
 be different paths to the same goal.
 But it's important not to get too caught up in this sort of
 idea of different paths to the same goal, which we hear a
 lot.
 Because, you know, it's not really, we're not talking about
 a mountain where you can approach it from all different
 types.
 Besides, we're talking about reality. And reality is one.
 There can only be one reality, either it's true or it's not
, either it's reality or it's not.
 There can be a million falsehoods, infinite number of false
 paths. And this is why we see such a diversity of paths.
 Because we're seeing a lot of paths that are teaching
 something based on speculation, that there is this God or
 this heaven or so on and so on.
 And as a result, they conflict because of the multiplicity
 of falsehood, of illusion, of mental creation.
 Things that have nothing to do with reality can be multiple
. But reality has to, by its very nature, by its very
 definition, be one, be that which is real.
 And the truth has to be one. So the realization of the
 truth and true peace and happiness has to come through
 understanding and through somehow harmonizing oneself with
 this reality.
 So that one acts based on understanding, based on those
 acts in speech and thought that will lead one to peace,
 happiness and freedom from suffering.
 So, whereas there may be many different religious
 traditions that are saying the same thing in different ways
, it's quite more likely that many of them, and as we see,
 many of them are teaching the falsehood, are teaching
 something based on faith or based on illusion.
 So they may have something that is beneficial, but the core
 doctrine or the core theory or dogma is based on a supp
osition or a belief and not based on ultimate reality.
 So I would say don't believe any one path as being the
 truth, just dogmatically. But don't pick and choose unless
 your plan is to eventually create or find one path for
 yourself that is going to lead you to peace, happiness and
 freedom from suffering.
 And the other thing is don't, I would not accept a post
modernist view of things where you say, what is good for me
 is good for me, or what I think is good is good.
 And just because we agree with something or something
 agrees with us, therefore it is right for us. This is the
 final warning that I would give that picking and choosing
 is nice when you're learning or when you're trying to
 figure out what is good for you.
 And trying to figure out what sort of a path you're looking
 for and what are the differences between the paths and
 trying to find a framework for the path.
 But if eventually all you're doing is simply augmenting or
 verifying your own beliefs, your own views, your own
 opinions, your own ego really, and picking up things simply
 because they agree with your way of life, agree with the
 things that you are attached to.
 You are attached to or you cling to or you believe in. Then
 there's no spiritual development at all.
 You're simply reifying your ego, your defiance, your
 attachments, your clinging state of mind and you're not
 learning to understand something, you're not broadening
 your horizons, you're not developing spiritually at all.
 So eventually you're going to have to examine your own
 beliefs and your own views and hold them up against the
 various religious traditions and come to see which one is
 objectively true, which one objectively does lead to peace,
 happiness, freedom from suffering, spiritual development,
 enlightenment and so on.
 So I hope that helped and just a caution not to pick and
 choose. The spiritual path is not a buffet, it's something
 that you have to dive into and follow to your utmost and
 really have to give up all the things that you believe in,
 all the things that you hold on to.
 Eventually you're going to have to let go of everything so
 that you can come to be free totally and completely.
 And so as a result you really do need to pick one specific
 path and follow it diligently, not going a short ways and
 then backing off and going up another path and then backing
 off.
 You're going to have to figure out which path is right for
 you, whether it's some path that you've worked out on your
 own or whether it's one of the paths that has been
 established that you realize is the truth and is based on
 reality and has a verifiable goal and conclusion and has a
 detailed teaching that leads you to that conclusion, which
 I would recommend is the Buddha's teaching, that's why I'm
 following it.
 But the other thing I would say is that you probably, I
 would boast I suppose, that you won't find such a claim in
 the other religions, whereas most paths will say something
 like this where ours is the best, you won't find this
 objective explanation where you say that you find the one
 that is most objective and most verifiable in the hearing.
 And that's ours. You'll find instead where people say
 believe this, this is what we believe, it's the truth and
 if you don't believe it, this and this is going to happen.
 And one that's based on inference or extrapolation where
 you have some special experience and you say that's this
 God or that's that God or that's heaven or this realization
 or that realization.
 The Buddha's teaching is purely scientific. It's something
 that allows you to realize the truth without extrapolating
 or jumping to any conclusion whatsoever.
 Conclusions that you'll draw will be verifiable and will be
 based on an exact one to one correlation with your
 experiences.
 When you experience something you'll have the conclusion
 that that is so, it is like that because you've experienced
 it as opposed to extrapolating upon it.
 So my post, I would say, of Buddhism is that it is thus and
 it is the teaching that is most complete and has the most
 detailed and accurate and scientific teaching that has ver
ifiable results and leads to a verifiable goal
 and works for those who put their effort into it.
 So I guess ultimately my answer is no, you don't have to
 pick and choose. In the end you're going to come to
 Buddhism if you've been objective and if you've been honest
 with yourself, that eventually you'll come to realize that
 the Buddha was an enlightened being
 and that anyone who is enlightened will teach these things
 because this is the truth.
 So really the word Buddhism in fact is simply a word that
 means the teaching of the enlightened one.
 So the meaning is that this teaching is the teaching of
 those people who have for themselves come to realize the
 truth.
 And it's not because of faith, it's not a God that came
 down and told them these things or it wasn't given from
 anywhere, it was something that was realized for themselves
.
 So this is therefore the point of saying that not all
 religious traditions are the same because if someone is not
 enlightened they might still teach and they might still
 gain acceptance from a large number of people.
 But that doesn't mean that they're enlightened, that doesn
't mean that their teaching is right and good. If they're
 not yet enlightened this is the criteria of being Buddhism
 or not Buddhism.
 Something is Buddhism if it's based on the teaching of
 someone who is totally perfectly enlightened, someone who
 has come to realize the truth, the whole truth for
 themselves and has come to be fully free from clinging,
 free from craving, free from suffering.
 So I hope that helps. I'm sure it's not exactly what you're
 looking for. I know we like to pick and choose and be free
 in our pursuits.
 This is why there's the rise of post-modernism. But there
 you have it. This is Ask a Monk. Thanks for tuning in and
 wish you all the best.
