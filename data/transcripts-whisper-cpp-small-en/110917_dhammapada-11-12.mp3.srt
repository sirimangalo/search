1
00:00:00,000 --> 00:00:03,040
 Hello and welcome back to our study of the Dhammapada.

2
00:00:03,040 --> 00:00:08,070
 Today we'll continue with verses number 11 and 12 which

3
00:00:08,070 --> 00:00:08,960
 read as follows.

4
00:00:08,960 --> 00:00:17,740
 Asari sahramatinau, sareja sahradasinau, te sarang nadikac

5
00:00:17,740 --> 00:00:21,360
anti, micha sankapagojara,

6
00:00:21,360 --> 00:00:29,830
 sarancha sarato nyattva, asarancha asarato, te sarang adik

7
00:00:29,830 --> 00:00:35,200
acanti, sammasankapagojara.

8
00:00:35,200 --> 00:00:37,920
 So it's a twin pair of verses.

9
00:00:37,920 --> 00:00:42,230
 The first one, asari sahramatinau, for a person who

10
00:00:42,230 --> 00:00:47,440
 understands what is essential to be essential,

11
00:00:47,440 --> 00:00:50,410
 or understands, sorry, what understands what is unessential

12
00:00:50,410 --> 00:00:51,920
 to be essential.

13
00:00:51,920 --> 00:00:57,330
 Saro jah, sareja sahradasinau, and understands what is

14
00:00:57,330 --> 00:00:59,280
 essential or sees what is essential

15
00:00:59,280 --> 00:01:01,520
 as being unessential.

16
00:01:01,520 --> 00:01:05,260
 Te sarang nadikacanti, such a person doesn't come to what

17
00:01:05,260 --> 00:01:06,440
 is essential.

18
00:01:06,440 --> 00:01:11,320
 Micha sankapagojara, because they dwell in wrong thought,

19
00:01:11,320 --> 00:01:13,640
 in a wrong understanding.

20
00:01:13,640 --> 00:01:19,660
 Sarancha sarato nyattva, a person who knows the essential

21
00:01:19,660 --> 00:01:24,920
 to be essential, asarancha asarato,

22
00:01:24,920 --> 00:01:27,720
 and what is unessential to be unessential.

23
00:01:27,720 --> 00:01:33,450
 Te sarang adikacanti, sammasankapagojara, such a person

24
00:01:33,450 --> 00:01:35,360
 does come to what is essential

25
00:01:35,360 --> 00:01:38,280
 because they dwell in right thought.

26
00:01:38,280 --> 00:01:43,110
 Now these two verses were told in relation to the Buddha's

27
00:01:43,110 --> 00:01:45,200
 two chief disciples.

28
00:01:45,200 --> 00:01:48,270
 And their story, in brief, is that they were living in Raj

29
00:01:48,270 --> 00:01:50,240
agaha, in the time when the Buddha

30
00:01:50,240 --> 00:01:52,720
 had just become enlightened.

31
00:01:52,720 --> 00:01:57,040
 And they had left the home life because they had seen that

32
00:01:57,040 --> 00:01:59,880
 there was no essence or nothing,

33
00:01:59,880 --> 00:02:03,160
 it wasn't essential, or there was no benefit to be found in

34
00:02:03,160 --> 00:02:05,160
 the life that they were living.

35
00:02:05,160 --> 00:02:08,020
 They would go to these shows and they would laugh and they

36
00:02:08,020 --> 00:02:09,520
 would cry and they found that

37
00:02:09,520 --> 00:02:15,040
 it brought them no real true peace and happiness because it

38
00:02:15,040 --> 00:02:18,200
 was bereft of any sort of essence

39
00:02:18,200 --> 00:02:20,800
 or any depth.

40
00:02:20,800 --> 00:02:23,690
 And so they left home and they went to try to find a

41
00:02:23,690 --> 00:02:24,440
 teacher.

42
00:02:24,440 --> 00:02:27,870
 And they found a teacher, the first teacher they found was

43
00:02:27,870 --> 00:02:29,000
 named Sanjaya.

44
00:02:29,000 --> 00:02:32,440
 Sanjaya was a teacher in the time of the Buddha and he

45
00:02:32,440 --> 00:02:34,840
 taught his students how to avoid,

46
00:02:34,840 --> 00:02:39,390
 how to avoid views, how to avoid any kind of teaching

47
00:02:39,390 --> 00:02:40,600
 actually.

48
00:02:40,600 --> 00:02:42,320
 So when people would ask them a question they would say, "

49
00:02:42,320 --> 00:02:43,960
Well, I don't believe that but

50
00:02:43,960 --> 00:02:45,760
 I don't believe otherwise."

51
00:02:45,760 --> 00:02:49,170
 And so they would find this way to avoid answering the

52
00:02:49,170 --> 00:02:52,000
 question and therefore avoid committing

53
00:02:52,000 --> 00:02:53,000
 to any stance.

54
00:02:53,000 --> 00:02:57,230
 And of course this has the benefit, at least superficially,

55
00:02:57,230 --> 00:02:59,800
 of keeping a person from developing

56
00:02:59,800 --> 00:03:03,560
 wrong views but it becomes a real conceit and attachment in

57
00:03:03,560 --> 00:03:04,360
 itself.

58
00:03:04,360 --> 00:03:10,300
 And of course the mind is always running around in circles

59
00:03:10,300 --> 00:03:13,840
 trying to avoid any kind of stance

60
00:03:13,840 --> 00:03:16,560
 from the theory and that you develop the theory.

61
00:03:16,560 --> 00:03:20,580
 As a result there's this theory that any stance is unt

62
00:03:20,580 --> 00:03:23,680
enable, which of course would make sense

63
00:03:23,680 --> 00:03:27,050
 when no one has an understanding of reality because all of

64
00:03:27,050 --> 00:03:30,440
 the people's views in the time

65
00:03:30,440 --> 00:03:36,210
 were based simply on their partial or incorrect

66
00:03:36,210 --> 00:03:38,240
 understanding of reality.

67
00:03:38,240 --> 00:03:41,960
 But once you find the truth then they would avoid even that

68
00:03:41,960 --> 00:03:44,120
, they would avoid committing

69
00:03:44,120 --> 00:03:46,620
 even to the truth.

70
00:03:46,620 --> 00:03:49,320
 And so at first they thought this was kind of a neat idea

71
00:03:49,320 --> 00:03:50,800
 and so they practiced it and

72
00:03:50,800 --> 00:03:53,310
 when they realized that it was really quite superficial and

73
00:03:53,310 --> 00:03:54,600
 shallow and simple actually

74
00:03:54,600 --> 00:03:57,650
 all you have to do is just avoid everything, avoid

75
00:03:57,650 --> 00:04:00,080
 answering these difficult questions

76
00:04:00,080 --> 00:04:02,240
 and there you have this teaching.

77
00:04:02,240 --> 00:04:05,450
 So they left it behind as well and they went to find

78
00:04:05,450 --> 00:04:06,200
 another teacher.

79
00:04:06,200 --> 00:04:08,920
 And then it so happened that one of them, Upatissa, their

80
00:04:08,920 --> 00:04:11,360
 names were Upatissa and Kollita.

81
00:04:11,360 --> 00:04:15,170
 Upatissa one day he saw one of the Buddha's disciples

82
00:04:15,170 --> 00:04:17,520
 walking for alms, it was one of

83
00:04:17,520 --> 00:04:21,610
 the first five disciples of the Buddha and it had such a

84
00:04:21,610 --> 00:04:23,800
 profound effect on him just

85
00:04:23,800 --> 00:04:27,840
 seeing this monk walking on alms because obviously this was

86
00:04:27,840 --> 00:04:29,640
 an enlightened being.

87
00:04:29,640 --> 00:04:32,840
 And so the way he walked, the way he held his robe, the way

88
00:04:32,840 --> 00:04:34,440
 he held his bull, the way

89
00:04:34,440 --> 00:04:37,810
 he looked, the way he would talk, the way he would keep

90
00:04:37,810 --> 00:04:39,760
 silent, everything about the

91
00:04:39,760 --> 00:04:42,470
 way he acted, his whole deportment said that there was

92
00:04:42,470 --> 00:04:44,000
 something special about him.

93
00:04:44,000 --> 00:04:48,200
 It was just so natural and so clear and so perfect.

94
00:04:48,200 --> 00:04:54,390
 It wasn't that he had some heavy kind of focused or forced

95
00:04:54,390 --> 00:04:57,320
 way of behaving, it was that it

96
00:04:57,320 --> 00:05:03,610
 was very natural and very fluid and it spoke volumes about

97
00:05:03,610 --> 00:05:06,320
 his inner character.

98
00:05:06,320 --> 00:05:10,950
 So he took this as a sign and decided that he would go up

99
00:05:10,950 --> 00:05:13,160
 and talk to this monk.

100
00:05:13,160 --> 00:05:15,380
 He waited for him to finish alms and finish eating and then

101
00:05:15,380 --> 00:05:16,480
 he asked him, he came up to

102
00:05:16,480 --> 00:05:19,090
 him and asked him and said, "Venerable Sir, please tell me

103
00:05:19,090 --> 00:05:20,320
 who is your teacher and what

104
00:05:20,320 --> 00:05:22,960
 is his teaching?"

105
00:05:22,960 --> 00:05:27,080
 And the monk, Asaji, was his name, said, "My teacher is the

106
00:05:27,080 --> 00:05:29,840
 great Buddha, the fully enlightened

107
00:05:29,840 --> 00:05:30,840
 Buddha."

108
00:05:30,840 --> 00:05:35,940
 And he said, "As for his teaching, I've only recently gone

109
00:05:35,940 --> 00:05:36,840
 forth.

110
00:05:36,840 --> 00:05:37,840
 I'm new to this religion."

111
00:05:37,840 --> 00:05:41,490
 Of course, the Buddha's teaching was new, so he was one of

112
00:05:41,490 --> 00:05:43,320
 the seniors actually, but

113
00:05:43,320 --> 00:05:46,040
 he was still, it was true, he was still relatively new.

114
00:05:46,040 --> 00:05:49,520
 So he said, "It would be very difficult for me to teach it

115
00:05:49,520 --> 00:05:50,400
 in depth."

116
00:05:50,400 --> 00:05:52,950
 Of course, this was a reasonable thing to say, but it also

117
00:05:52,950 --> 00:05:54,480
 had a profound effect on Upatissa

118
00:05:54,480 --> 00:05:59,400
 because it made him think, "Wow, if this is what a novice

119
00:05:59,400 --> 00:06:01,320
 acts like," then I wonder what

120
00:06:01,320 --> 00:06:04,540
 it would be like to be someone who fully understands the

121
00:06:04,540 --> 00:06:05,320
 teaching.

122
00:06:05,320 --> 00:06:07,820
 So he said, "Please give me whatever you know because

123
00:06:07,820 --> 00:06:09,480
 obviously you know something.

124
00:06:09,480 --> 00:06:12,200
 So please give me the essence, whether it be a law that

125
00:06:12,200 --> 00:06:13,920
 what you survive for great words

126
00:06:13,920 --> 00:06:19,240
 and long speeches, just give me the essence."

127
00:06:19,240 --> 00:06:23,980
 So Asaji gave him the essence and it's so core that it's

128
00:06:23,980 --> 00:06:26,520
 very difficult for us to understand

129
00:06:26,520 --> 00:06:29,490
 and even more difficult to understand how such a simple

130
00:06:29,490 --> 00:06:31,240
 teaching could have the effect

131
00:06:31,240 --> 00:06:33,760
 that it did have on Upatissa.

132
00:06:33,760 --> 00:06:38,600
 The verse that he gave that he said to have given to Upat

133
00:06:38,600 --> 00:06:42,480
issa was, "Yedama, he tu babawa,

134
00:06:42,480 --> 00:06:51,190
 ye te sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu

135
00:06:51,190 --> 00:06:52,840
 babawa, ye sanghe tu babawa,

136
00:06:52,840 --> 00:06:54,880
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

137
00:06:54,880 --> 00:06:56,000
awa, ye sanghe tu babawa,

138
00:06:56,000 --> 00:06:56,570
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

139
00:06:56,570 --> 00:06:57,000
awa, ye sanghe tu babawa,

140
00:06:57,000 --> 00:06:57,570
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

141
00:06:57,570 --> 00:06:58,000
awa, ye sanghe tu babawa,

142
00:06:58,000 --> 00:06:58,610
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

143
00:06:58,610 --> 00:06:59,000
awa, ye sanghe tu babawa,

144
00:06:59,000 --> 00:06:59,570
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

145
00:06:59,570 --> 00:07:00,000
awa, ye sanghe tu babawa,

146
00:07:00,000 --> 00:07:00,570
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

147
00:07:00,570 --> 00:07:01,000
awa, ye sanghe tu babawa,

148
00:07:01,000 --> 00:07:10,420
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

149
00:07:10,420 --> 00:07:14,400
awa, ye sanghe tu babawa,

150
00:07:14,400 --> 00:07:24,330
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

151
00:07:24,330 --> 00:07:28,400
awa, ye sanghe tu babawa,

152
00:07:28,400 --> 00:07:28,970
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

153
00:07:28,970 --> 00:07:29,400
awa, ye sanghe tu babawa,

154
00:07:29,400 --> 00:07:29,970
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

155
00:07:29,970 --> 00:07:30,400
awa, ye sanghe tu babawa,

156
00:25:52,400 --> 00:25:53,190
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

157
00:25:53,190 --> 00:25:53,400
awa, ye sanghe tu babawa,

158
00:25:53,400 --> 00:25:53,990
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

159
00:25:53,990 --> 00:25:54,400
awa, ye sanghe tu babawa,

160
00:25:54,400 --> 00:25:55,010
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

161
00:25:55,010 --> 00:25:55,400
awa, ye sanghe tu babawa,

162
00:25:55,400 --> 00:25:55,970
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

163
00:25:55,970 --> 00:25:56,400
awa, ye sanghe tu babawa,

164
00:25:56,400 --> 00:25:56,970
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

165
00:25:56,970 --> 00:25:57,400
awa, ye sanghe tu babawa,

166
00:25:57,400 --> 00:25:57,970
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

167
00:25:57,970 --> 00:25:58,400
awa, ye sanghe tu babawa,

168
00:25:58,400 --> 00:25:58,970
 ye sanghe tu babawa, ye sanghe tu babawa, ye sanghe tu bab

169
00:25:58,970 --> 00:25:59,400
awa, ye sanghe tu babawa,

