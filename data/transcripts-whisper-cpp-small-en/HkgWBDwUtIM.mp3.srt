1
00:00:00,000 --> 00:00:04,680
 Hi, so welcome back to Ask a Monk.

2
00:00:04,680 --> 00:00:07,600
 Today's question is from thesemi-g123.

3
00:00:07,600 --> 00:00:12,250
 "I find it difficult to clear my mind of thought as they

4
00:00:12,250 --> 00:00:14,200
 always pop into my head and I can't

5
00:00:14,200 --> 00:00:15,560
 control it.

6
00:00:15,560 --> 00:00:18,380
 When I try to be aware of my breathing but not control it,

7
00:00:18,380 --> 00:00:19,900
 this is also very hard because

8
00:00:19,900 --> 00:00:22,960
 I become conscious and I automatically do so.

9
00:00:22,960 --> 00:00:26,160
 Any tips?"

10
00:00:26,160 --> 00:00:31,490
 Actually this explanation is a clear indication that you're

11
00:00:31,490 --> 00:00:33,880
 practicing correctly.

12
00:00:33,880 --> 00:00:37,420
 What I mean by that is you're starting to see the way your

13
00:00:37,420 --> 00:00:38,480
 mind works.

14
00:00:38,480 --> 00:00:40,750
 You're starting to see that the nature of the mind is to

15
00:00:40,750 --> 00:00:42,360
 automatically control things.

16
00:00:42,360 --> 00:00:43,700
 It'll try to control everything.

17
00:00:43,700 --> 00:00:47,080
 We try to control every part of our experience to make sure

18
00:00:47,080 --> 00:00:48,780
 that it is the way that we want

19
00:00:48,780 --> 00:00:52,880
 it to be and not be the way that we don't want it to be.

20
00:00:52,880 --> 00:00:55,890
 The first part of your question or the first part of your

21
00:00:55,890 --> 00:00:57,880
 problem as far as thoughts popping

22
00:00:57,880 --> 00:01:00,720
 into your head and not being able to control it shows that

23
00:01:00,720 --> 00:01:02,080
 you still have the idea that

24
00:01:02,080 --> 00:01:03,400
 we're trying to control things.

25
00:01:03,400 --> 00:01:04,400
 This is natural.

26
00:01:04,400 --> 00:01:06,360
 This is the way that we look at things.

27
00:01:06,360 --> 00:01:08,660
 We think we have to control everything.

28
00:01:08,660 --> 00:01:11,880
 When thought enters the mind that's wrong, trying to stop

29
00:01:11,880 --> 00:01:13,360
 it is of course our natural

30
00:01:13,360 --> 00:01:14,880
 reflex.

31
00:01:14,880 --> 00:01:17,660
 That's the wrong way to approach things.

32
00:01:17,660 --> 00:01:18,940
 That's what we're starting to realize.

33
00:01:18,940 --> 00:01:20,880
 You're starting to realize that you can't control it.

34
00:01:20,880 --> 00:01:25,570
 There's no way that you could possibly stop your mind from

35
00:01:25,570 --> 00:01:28,160
 thinking except temporarily

36
00:01:28,160 --> 00:01:31,670
 if you practice tranquility meditation to suppress the

37
00:01:31,670 --> 00:01:33,680
 thoughts temporarily but that

38
00:01:33,680 --> 00:01:36,400
 is never a permanent solution.

39
00:01:36,400 --> 00:01:39,750
 In the end, this is why we teach people to let go and to

40
00:01:39,750 --> 00:01:41,520
 accept the experience.

41
00:01:41,520 --> 00:01:44,730
 Then when you try to go the other way, at the same time, on

42
00:01:44,730 --> 00:01:46,360
 the one side you're saying

43
00:01:46,360 --> 00:01:48,400
 you're trying to control your thoughts and can't do it.

44
00:01:48,400 --> 00:01:50,250
 On the other side you're saying you're trying not to

45
00:01:50,250 --> 00:01:51,600
 control your breath but you can't do

46
00:01:51,600 --> 00:01:53,720
 that either.

47
00:01:53,720 --> 00:01:56,620
 This is the fight between the two ways of looking at things

48
00:01:56,620 --> 00:01:58,280
, trying to control and trying

49
00:01:58,280 --> 00:01:59,280
 not to control.

50
00:01:59,280 --> 00:02:02,730
 The correct way is to not control but even trying to not

51
00:02:02,730 --> 00:02:04,480
 control is a form of trying

52
00:02:04,480 --> 00:02:09,560
 to control, forcing yourself to not force things.

53
00:02:09,560 --> 00:02:11,840
 There's no way out of that.

54
00:02:11,840 --> 00:02:14,690
 The only way to become free from this is to see clearly

55
00:02:14,690 --> 00:02:16,400
 that trying to control things

56
00:02:16,400 --> 00:02:17,760
 is a problem.

57
00:02:17,760 --> 00:02:20,010
 As you watch the breath rising, falling and you see

58
00:02:20,010 --> 00:02:21,600
 yourself controlling it again and

59
00:02:21,600 --> 00:02:24,140
 again and again and you're forced to start over again and

60
00:02:24,140 --> 00:02:25,400
 again and again and forced

61
00:02:25,400 --> 00:02:29,630
 to deal with the frustration, forced to deal with the impat

62
00:02:29,630 --> 00:02:31,960
ience, the suffering involved

63
00:02:31,960 --> 00:02:35,270
 with controlling, involved with forcing the breath, your

64
00:02:35,270 --> 00:02:36,880
 mind will slowly realize it's

65
00:02:36,880 --> 00:02:40,270
 like you're teaching yourself as you would teach a child by

66
00:02:40,270 --> 00:02:41,800
 direct experience, learning

67
00:02:41,800 --> 00:02:44,690
 the hard way that this is the wrong way to deal with things

68
00:02:44,690 --> 00:02:44,840
.

69
00:02:44,840 --> 00:02:48,080
 You'll slowly let go and you'll slowly, naturally the

70
00:02:48,080 --> 00:02:50,080
 breath will come by itself and there will

71
00:02:50,080 --> 00:02:51,080
 be no more controlling.

72
00:02:51,080 --> 00:02:53,360
 You can't stop yourself from controlling.

73
00:02:53,360 --> 00:02:58,490
 All you can do is see clearly which you get by this clear

74
00:02:58,490 --> 00:03:01,640
 thought that clinging to things

75
00:03:01,640 --> 00:03:04,860
 and forcing things is a cause for suffering.

76
00:03:04,860 --> 00:03:09,360
 It's something that is painful, is stressful and is

77
00:03:09,360 --> 00:03:10,680
 unhealthy.

78
00:03:10,680 --> 00:03:13,480
 Okay, so I hope that helps.

79
00:03:13,480 --> 00:03:14,480
 Keep up the good work.

80
00:03:14,480 --> 00:03:17,480
 This is the first step in meditation.

81
00:03:17,480 --> 00:03:20,360
 The first step in letting go is to see that you're clinging

82
00:03:20,360 --> 00:03:21,680
 and I would encourage you

83
00:03:21,680 --> 00:03:25,000
 to look carefully and see what your emotions are during the

84
00:03:25,000 --> 00:03:26,680
 time that you feel like your

85
00:03:26,680 --> 00:03:28,760
 breath is being forced.

86
00:03:28,760 --> 00:03:29,760
 Are you angry?

87
00:03:29,760 --> 00:03:30,760
 Are you frustrated?

88
00:03:30,760 --> 00:03:32,600
 Are you stressed?

89
00:03:32,600 --> 00:03:36,680
 What is your reaction to this when you're forcing things?

90
00:03:36,680 --> 00:03:40,090
 Try not to get frustrated and try not to think that somehow

91
00:03:40,090 --> 00:03:41,760
 your practice is wrong.

92
00:03:41,760 --> 00:03:46,430
 Try to be patient and use it as a reason to build up

93
00:03:46,430 --> 00:03:49,720
 patience in your practice and to

94
00:03:49,720 --> 00:03:52,560
 try to look at things and let things be the way they are.

95
00:03:52,560 --> 00:03:55,240
 When you're forcing things, let yourself force them.

96
00:03:55,240 --> 00:03:57,710
 See that you're forcing it, feel the pain and forcing it,

97
00:03:57,710 --> 00:03:58,860
 feel the suffering and just

98
00:03:58,860 --> 00:04:01,690
 come to learn about that experience so that in the future

99
00:04:01,690 --> 00:04:03,200
 you can be clear that when you

100
00:04:03,200 --> 00:04:05,560
 force things, this is what happens.

101
00:04:05,560 --> 00:04:06,560
 Okay?

102
00:04:06,560 --> 00:04:08,600
 Again, keep up the good work.

103
00:04:08,600 --> 00:04:12,440
 Try not to hear that someone's actually meditating.

