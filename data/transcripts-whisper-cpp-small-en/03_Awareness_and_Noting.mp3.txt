 Noting technique Q&A.
 Awareness or noting
 question. Why is it not enough to just be aware of the
 phenomena that arise without noting them?
 Answer.
 More important than what is enough is an answer to the
 question what is better.
 It may be enough to simply be aware of a phenomenon,
 but it is better to apply mindfulness to it. In our
 tradition,
 mindfulness is defined as the grasping of the object as it
 is.
 A quality that may or may not be present in ordinary
 awareness.
 The real question is, without reminding oneself of the
 objective nature of the phenomenon,
 i.e. without noting,
 how can one be sure one is observing the phenomenon
 objectively?
 Commonly, new meditators have a natural
 explanation to note. It is more comfortable to allow one's
 awareness to stand unimpeded,
 rather than to train the mind in the precision of
 identifying the object.
 The question of which is better is open for debate,
 but one should not let their own prejudice be the deciding
 factor either way.
 The essential quality of Vipassana meditation is sati.
 Sati means to remember,
 recollect, or remind oneself of something.
 According to the Visuddhimagga, the proximate cause is T
irthasana, which means
 firm recognition.
 Recognition, or sahana, is present in ordinary experience,
 and
 noting is understood to augment and stabilize this
 recognition, preventing the mind from wavering in its
 objective observation.
 While reminding oneself of the nature of phenomena in this
 way may not always be comfortable or feel natural,
 it is understood to be more reliably objective. The mental
 activity involved in producing a label for the object
 prevents any alternative judgment or extrapolation of the
 object.
 Put another way, it is inevitable that the mind will give
 rise to some sort of label for every experience anyway.
 Noting is a means of ensuring that the label is objective.
 If one experiences aversion or doubt about the technique,
 it is enough to note these mind states. They are undeniably
 hindrances to meditation practice,
 whereas noting is not. If one is able to overcome one's
 aversion and doubt by noting,
 disliking, disliking, or
 doubting, doubting,
 it can be assured that one will become comfortable with it
 and see the benefit in the noting technique for oneself.
 If one is unable to do so, one is welcome to seek out other
 techniques and other traditions.
 [Silence]
