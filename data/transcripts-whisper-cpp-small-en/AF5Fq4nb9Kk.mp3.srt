1
00:00:00,000 --> 00:00:04,440
 Okay, I'm thinking about teaching elementary students

2
00:00:04,440 --> 00:00:06,120
 meditation, especially those who

3
00:00:06,120 --> 00:00:10,390
 have emotional disturbances such as strong anger, defiance,

4
00:00:10,390 --> 00:00:11,720
 hyperactivity.

5
00:00:11,720 --> 00:00:12,720
 What is the best technique?

6
00:00:12,720 --> 00:00:15,440
 Are they too young to learn?

7
00:00:15,440 --> 00:00:19,900
 Okay, I probably should do a separate video just to say

8
00:00:19,900 --> 00:00:22,200
 this, but maybe I'll just say

9
00:00:22,200 --> 00:00:26,150
 it again and again in all of them because it's something

10
00:00:26,150 --> 00:00:28,560
 that I believe quite strongly,

11
00:00:28,560 --> 00:00:33,590
 and I've said it now tonight a few times, is that don't go

12
00:00:33,590 --> 00:00:35,880
 where the real problems

13
00:00:35,880 --> 00:00:38,480
 are.

14
00:00:38,480 --> 00:00:44,410
 I would recommend against trying to deal with people who

15
00:00:44,410 --> 00:00:47,080
 have serious problems.

16
00:00:47,080 --> 00:00:52,240
 It's very altruistic to do so, but it's missing one

17
00:00:52,240 --> 00:00:56,200
 important, very important point.

18
00:00:56,200 --> 00:01:08,360
 The amount of effort you put in to helping someone who is

19
00:01:08,360 --> 00:01:12,520
 in a very bad state is compared

20
00:01:12,520 --> 00:01:17,400
 with the benefit of helping them, the potential benefit.

21
00:01:17,400 --> 00:01:22,520
 The effort level is incredibly high.

22
00:01:22,520 --> 00:01:27,740
 The amount of benefit that you can give to them is very low

23
00:01:27,740 --> 00:01:27,960
.

24
00:01:27,960 --> 00:01:28,960
 There's no miracles.

25
00:01:28,960 --> 00:01:36,880
 A person who has a lot of mental trouble is not likely to,

26
00:01:36,880 --> 00:01:40,920
 in most cases, become a fully

27
00:01:40,920 --> 00:01:43,920
 enlightened being in this life.

28
00:01:43,920 --> 00:01:44,920
 It just doesn't happen.

29
00:01:44,920 --> 00:01:47,920
 This monk that I was talking about is an example of that.

30
00:01:47,920 --> 00:01:49,360
 He practiced meditation.

31
00:01:49,360 --> 00:01:50,360
 He's been practicing.

32
00:01:50,360 --> 00:01:53,120
 He's still alive.

33
00:01:53,120 --> 00:01:56,960
 He's still taking baby steps.

34
00:01:56,960 --> 00:01:58,480
 He's still blowing up.

35
00:01:58,480 --> 00:02:01,000
 He's still got the same problems.

36
00:02:01,000 --> 00:02:03,360
 It's a very, very slow path.

37
00:02:03,360 --> 00:02:07,520
 Everyone who practices meditation is going to go through

38
00:02:07,520 --> 00:02:09,760
 the same conditions again and

39
00:02:09,760 --> 00:02:16,050
 again and again until finally they're worn down, broken

40
00:02:16,050 --> 00:02:19,000
 apart, and discarded.

41
00:02:19,000 --> 00:02:28,520
 Until that point, they will come back again and again and

42
00:02:28,520 --> 00:02:32,200
 again until they become weaker

43
00:02:32,200 --> 00:02:39,880
 and weaker and weaker until they disappear.

44
00:02:39,880 --> 00:02:44,000
 You get a far less return for your effort.

45
00:02:44,000 --> 00:02:47,650
 If you take the same amount of effort and you apply it to

46
00:02:47,650 --> 00:02:49,800
 people who are exceptional,

47
00:02:49,800 --> 00:02:58,640
 gifted, brilliant, focused, people who have potential, and

48
00:02:58,640 --> 00:03:02,480
 you teach them meditation,

49
00:03:02,480 --> 00:03:12,130
 first of all, very little effort, second of all, incredible

50
00:03:12,130 --> 00:03:16,320
 results, that they will grasp

51
00:03:16,320 --> 00:03:22,440
 it much easier, pick it up, and in many cases it's like

52
00:03:22,440 --> 00:03:24,480
 planting a seed.

53
00:03:24,480 --> 00:03:27,840
 You don't have to make it grow.

54
00:03:27,840 --> 00:03:31,280
 It grows by itself.

55
00:03:31,280 --> 00:03:37,480
 It goes viral because they pick it up and they become you.

56
00:03:37,480 --> 00:03:43,800
 They become the person who's spreading the teaching.

57
00:03:43,800 --> 00:03:52,220
 You help such people who are teachers and those teachers go

58
00:03:52,220 --> 00:03:55,400
 and help, go and teach.

59
00:03:55,400 --> 00:04:02,750
 So at the very least what you'll get, theoretically, is a

60
00:04:02,750 --> 00:04:07,160
 bunch of people who are very gifted,

61
00:04:07,160 --> 00:04:10,300
 who will then go out and pass along what you've passed

62
00:04:10,300 --> 00:04:13,840
 along to people who are less gifted

63
00:04:13,840 --> 00:04:16,880
 and on and on down the line.

64
00:04:16,880 --> 00:04:21,520
 There will be people who are actually working with these

65
00:04:21,520 --> 00:04:24,180
 kind of people who have serious

66
00:04:24,180 --> 00:04:29,180
 problems and they will do this, they will have, based on

67
00:04:29,180 --> 00:04:31,640
 the teachings that you have

68
00:04:31,640 --> 00:04:35,310
 been giving on a higher level, they will do the grunt work

69
00:04:35,310 --> 00:04:41,360
 of dealing with these people.

70
00:04:41,360 --> 00:04:47,380
 It will create more benefit in the long run, even for those

71
00:04:47,380 --> 00:04:50,080
 people who are in a bad way

72
00:04:50,080 --> 00:04:56,870
 because you're helping people who are in a position to help

73
00:04:56,870 --> 00:04:59,120
 others will create a chain.

74
00:04:59,120 --> 00:05:02,440
 It's like a pyramid scheme.

75
00:05:02,440 --> 00:05:07,240
 It will multiply and everyone will benefit.

76
00:05:07,240 --> 00:05:09,430
 What I'm saying is that people who come to practice

77
00:05:09,430 --> 00:05:10,960
 meditation with me end up becoming

78
00:05:10,960 --> 00:05:15,320
 social workers or teachers.

79
00:05:15,320 --> 00:05:19,120
 So I don't have to go out and teach those people.

80
00:05:19,120 --> 00:05:23,100
 I teach the people who will go out and teach them, which is

81
00:05:23,100 --> 00:05:25,480
, to me, far better use of one's

82
00:05:25,480 --> 00:05:26,480
 time.

83
00:05:26,480 --> 00:05:30,100
 The basic point being to try to pick the gifted, pick the

84
00:05:30,100 --> 00:05:32,200
 ones who are you going to get the

85
00:05:32,200 --> 00:05:38,040
 best return for your effort because it's not being selfish

86
00:05:38,040 --> 00:05:41,440
 or it's not being discriminatory.

87
00:05:41,440 --> 00:05:45,060
 It's being realistic and understanding that we're really in

88
00:05:45,060 --> 00:05:45,920
 a war here.

89
00:05:45,920 --> 00:05:50,360
 We don't have the luxury to pick and choose.

90
00:05:50,360 --> 00:05:56,010
 We have to go for the most benefit because death waits for

91
00:05:56,010 --> 00:05:58,600
 no one and nothing waits.

92
00:05:58,600 --> 00:06:02,960
 The universe will continue rolling on in its way and if we

93
00:06:02,960 --> 00:06:05,280
 don't keep up, we're going to

94
00:06:05,280 --> 00:06:07,020
 get left behind.

95
00:06:07,020 --> 00:06:09,720
 This is why people get burnt out when they deal with such

96
00:06:09,720 --> 00:06:10,240
 people.

97
00:06:10,240 --> 00:06:13,480
 Now, this is only half of the answer.

98
00:06:13,480 --> 00:06:17,040
 My answer is don't teach children.

99
00:06:17,040 --> 00:06:20,770
 This is only in relation to the part of especially those

100
00:06:20,770 --> 00:06:23,240
 who have emotional disturbances.

101
00:06:23,240 --> 00:06:24,240
 You're welcome to do it.

102
00:06:24,240 --> 00:06:28,680
 I'm not going to try to tell you what to do.

103
00:06:28,680 --> 00:06:31,990
 Just give you some idea and maybe it will help to change

104
00:06:31,990 --> 00:06:32,800
 your focus.

105
00:06:32,800 --> 00:06:37,040
 I would think it would be much, from my point of view, much

106
00:06:37,040 --> 00:06:38,620
 more inspiring to teach the

107
00:06:38,620 --> 00:06:41,520
 gifted children.

108
00:06:41,520 --> 00:06:46,970
 The way that you teach children, the question as to whether

109
00:06:46,970 --> 00:06:49,880
 they're too young, well, the

110
00:06:49,880 --> 00:06:53,240
 tradition goes seven years.

111
00:06:53,240 --> 00:06:54,240
 Everything's seven.

112
00:06:54,240 --> 00:06:57,970
 At seven years, they're old enough to understand, which is

113
00:06:57,970 --> 00:07:00,120
 most school children already.

114
00:07:00,120 --> 00:07:04,080
 Anything below seven years is considered to be too young.

115
00:07:04,080 --> 00:07:08,880
 At seven years, this is the cutoff and then they can start

116
00:07:08,880 --> 00:07:10,060
 learning.

117
00:07:10,060 --> 00:07:13,780
 The way to teach children is to introduce to them the

118
00:07:13,780 --> 00:07:16,640
 concept of mindfulness, the concept

119
00:07:16,640 --> 00:07:29,310
 of recognition, teaching them to recognize and to clearly

120
00:07:29,310 --> 00:07:32,600
 perceive the experience in

121
00:07:32,600 --> 00:07:38,040
 front of them, or anything really.

122
00:07:38,040 --> 00:07:43,580
 I tried an experiment once with some kids in a Thai

123
00:07:43,580 --> 00:07:46,920
 monastery in Los Angeles.

124
00:07:46,920 --> 00:07:48,520
 I had them focus on a cat.

125
00:07:48,520 --> 00:07:52,140
 I had them think of a cat and say to themselves in their

126
00:07:52,140 --> 00:07:53,720
 mind, "Cat, cat."

127
00:07:53,720 --> 00:07:58,730
 This was a room of like a hundred kids and they were unruly

128
00:07:58,730 --> 00:07:59,120
.

129
00:07:59,120 --> 00:08:03,940
 These were kids who come to the temple on Saturdays and

130
00:08:03,940 --> 00:08:06,280
 Sundays and get whatever lesson

131
00:08:06,280 --> 00:08:08,560
 that is possible to give them.

132
00:08:08,560 --> 00:08:11,960
 It's mainly about whatever.

133
00:08:11,960 --> 00:08:17,840
 It is what it is.

134
00:08:17,840 --> 00:08:20,000
 They were like anything I would say.

135
00:08:20,000 --> 00:08:23,010
 They would repeat it and they would make fun of it and

136
00:08:23,010 --> 00:08:23,920
 laugh at it.

137
00:08:23,920 --> 00:08:31,720
 I wasn't upset by that, but I was prepared in advance

138
00:08:31,720 --> 00:08:36,000
 because I was aware of the situation.

139
00:08:36,000 --> 00:08:38,800
 You can't lead these people directly into meditation.

140
00:08:38,800 --> 00:08:41,200
 You can't say, "Start watching the stomach, say rising and

141
00:08:41,200 --> 00:08:41,820
 falling."

142
00:08:41,820 --> 00:08:43,440
 Give them something that's going to appeal to them.

143
00:08:43,440 --> 00:08:47,400
 I thought, "Well, let's do some simple kid stuff."

144
00:08:47,400 --> 00:09:00,720
 Think of a cat and say to yourself, "Cat, cat, cat."

145
00:09:00,720 --> 00:09:01,800
 And then dog, dog.

146
00:09:01,800 --> 00:09:04,800
 I had them do dog, dog.

147
00:09:04,800 --> 00:09:05,800
 It is kind of fun at first.

148
00:09:05,800 --> 00:09:09,200
 It seems like I'm just playing a game with them, but

149
00:09:09,200 --> 00:09:11,160
 actually they start to conceive

150
00:09:11,160 --> 00:09:15,650
 and they start to focus and their mind starts to quiet down

151
00:09:15,650 --> 00:09:16,000
.

152
00:09:16,000 --> 00:09:17,000
 It did work.

153
00:09:17,000 --> 00:09:18,920
 I think it worked quite well.

154
00:09:18,920 --> 00:09:23,240
 It's something that you'd have to try long-term, but the

155
00:09:23,240 --> 00:09:25,600
 point of it is to get them around

156
00:09:25,600 --> 00:09:28,690
 slowly to the point of being able to watch and observe

157
00:09:28,690 --> 00:09:30,280
 their own experience.

158
00:09:30,280 --> 00:09:33,990
 First a cat and then a dog, and then I had them focus on

159
00:09:33,990 --> 00:09:35,360
 their parents.

160
00:09:35,360 --> 00:09:36,960
 This is important.

161
00:09:36,960 --> 00:09:38,920
 This really got me brownie points with the parents.

162
00:09:38,920 --> 00:09:46,640
 I think the parents are all sitting listening as well.

163
00:09:46,640 --> 00:09:51,350
 And I said, "Think of your mother and say to yourself, '

164
00:09:51,350 --> 00:09:54,040
Mother, mother, mother.'

165
00:09:54,040 --> 00:09:59,380
 And then your father and say to yourself, 'Father, father

166
00:09:59,380 --> 00:09:59,480
.'"

167
00:09:59,480 --> 00:10:02,510
 And then I came to what is actually the beginnings of

168
00:10:02,510 --> 00:10:04,080
 Buddhist meditation.

169
00:10:04,080 --> 00:10:05,800
 It's the focusing on the Buddha, right?

170
00:10:05,800 --> 00:10:09,620
 You have this famous Buddhist meditation of saying to

171
00:10:09,620 --> 00:10:12,120
 yourself, "Buddha, Buddha."

172
00:10:12,120 --> 00:10:13,120
 So I had them do that.

173
00:10:13,120 --> 00:10:16,040
 I said, "Picture the Buddha now, because the Buddha is a

174
00:10:16,040 --> 00:10:17,600
 big golden Buddha image."

175
00:10:17,600 --> 00:10:23,570
 I said, "Think of the Buddha and say Buddha, Buddha, Buddha

176
00:10:23,570 --> 00:10:23,680
."

177
00:10:23,680 --> 00:10:26,520
 And so they did this.

178
00:10:26,520 --> 00:10:29,330
 And then eventually I got around, I think after the Buddha,

179
00:10:29,330 --> 00:10:30,680
 then it was right to focusing

180
00:10:30,680 --> 00:10:36,940
 on the body, and then it was watching the stomach, watching

181
00:10:36,940 --> 00:10:38,840
 your breath.

182
00:10:38,840 --> 00:10:41,800
 I brought it to them that way.

183
00:10:41,800 --> 00:10:45,190
 I think that it's just a simple point there, the idea of

184
00:10:45,190 --> 00:10:47,280
 starting with something cartoonish

185
00:10:47,280 --> 00:10:52,520
 that they can relate to.

186
00:10:52,520 --> 00:10:58,540
 And of course it depends on the age level, but the

187
00:10:58,540 --> 00:11:03,440
 important point is to bring the concept

188
00:11:03,440 --> 00:11:05,560
 to them.

189
00:11:05,560 --> 00:11:09,320
 Find a way to explain mindfulness to them.

190
00:11:09,320 --> 00:11:12,240
 And I think that's clearly explained by using the cat

191
00:11:12,240 --> 00:11:14,000
 example, the dog example.

192
00:11:14,000 --> 00:11:16,140
 Because you're just focusing on something and you're seeing

193
00:11:16,140 --> 00:11:18,000
 this is a cat, this is a

194
00:11:18,000 --> 00:11:19,000
 dog.

195
00:11:19,000 --> 00:11:25,330
 And then when you're doing this, they're building up this

196
00:11:25,330 --> 00:11:26,840
 ability.

197
00:11:26,840 --> 00:11:31,680
 And all you do is you bring that ability, that proficiency,

198
00:11:31,680 --> 00:11:33,880
 or however, back to focus

199
00:11:33,880 --> 00:11:35,160
 on reality.

200
00:11:35,160 --> 00:11:36,160
 So that's what I think.

201
00:11:36,160 --> 00:12:02,740
 [

