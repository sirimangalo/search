1
00:00:00,000 --> 00:00:12,000
 So tonight we come back again for our daily meditation.

2
00:00:12,000 --> 00:00:28,760
 We've been talking about the various things that we need to

3
00:00:28,760 --> 00:00:29,640
 keep in mind

4
00:00:29,640 --> 00:00:35,640
 and develop and cultivate and deal with in our meditation

5
00:00:35,640 --> 00:00:45,640
 in order to develop, in order to progress.

6
00:00:45,640 --> 00:00:51,650
 I think the next stage in our discussion is in regards to

7
00:00:51,650 --> 00:00:58,640
 the final realization of the truth.

8
00:00:58,640 --> 00:01:03,640
 When we're aiming for in the practice.

9
00:01:03,640 --> 00:01:08,500
 The point isn't to expect to reach the goal today or

10
00:01:08,500 --> 00:01:10,640
 tomorrow or whenever.

11
00:01:10,640 --> 00:01:15,250
 But if we know what to aim for, and if we know what

12
00:01:15,250 --> 00:01:23,640
 direction our practice has to take,

13
00:01:23,640 --> 00:01:32,870
 then we can adjust and we can augment our practice and our

14
00:01:32,870 --> 00:01:36,640
 confidence in our practice

15
00:01:36,640 --> 00:01:42,640
 and progress at a steady pace.

16
00:01:42,640 --> 00:01:46,640
 Otherwise if we don't quite know where we're headed,

17
00:01:46,640 --> 00:01:51,560
 we'll always have this question of, "Is this it? Is that it

18
00:01:51,560 --> 00:01:55,640
? Am I getting there? Am I getting closer?"

19
00:01:55,640 --> 00:01:58,940
 Then we'll be like a person walking lost in the woods. We

20
00:01:58,940 --> 00:02:01,640
'll be very hesitant,

21
00:02:01,640 --> 00:02:04,640
 not knowing which way to go at all times.

22
00:02:04,640 --> 00:02:06,870
 So it's important to understand what we mean by

23
00:02:06,870 --> 00:02:13,640
 enlightenment, by realization of the truth.

24
00:02:13,640 --> 00:02:23,640
 Realization of the truth is bhoja or bodhi.

25
00:02:23,640 --> 00:02:27,590
 Bodhi, like the bodhi tree, is the place where the Buddha

26
00:02:27,590 --> 00:02:28,640
 became a Buddha.

27
00:02:28,640 --> 00:02:31,640
 Buddha and bodhi, same root.

28
00:02:31,640 --> 00:02:51,640
 Bhuj, which means to become awake, to wake up.

29
00:02:51,640 --> 00:02:53,940
 So we can ask, "Did the Buddha have a teaching on what is

30
00:02:53,940 --> 00:02:54,640
 enlightenment

31
00:02:54,640 --> 00:02:58,640
 and what is the factors of enlightenment?"

32
00:02:58,640 --> 00:03:01,640
 And the answer is yes.

33
00:03:01,640 --> 00:03:05,640
 The Buddha gave a very powerful teaching on bhojanga,

34
00:03:05,640 --> 00:03:15,100
 the anga, the factors or constituents, the requisites for

35
00:03:15,100 --> 00:03:17,640
 enlightenment.

36
00:03:17,640 --> 00:03:22,230
 And it paints a very clear picture of the direction our

37
00:03:22,230 --> 00:03:25,640
 practice has to take,

38
00:03:25,640 --> 00:03:29,260
 all the way to the end. It's not just giving us a

39
00:03:29,260 --> 00:03:29,640
 preliminary practice,

40
00:03:29,640 --> 00:03:32,920
 like the satipatthana, we talk about the foundations of

41
00:03:32,920 --> 00:03:33,640
 mindfulness.

42
00:03:33,640 --> 00:03:37,040
 But when we leave it off there, we don't have a clear

43
00:03:37,040 --> 00:03:38,640
 picture of where we're going.

44
00:03:38,640 --> 00:03:43,240
 So at this point, now that we're good at practicing and

45
00:03:43,240 --> 00:03:45,640
 come a long way,

46
00:03:45,640 --> 00:03:52,270
 developing and honing your skill in the practice of satipat

47
00:03:52,270 --> 00:03:52,640
thana,

48
00:03:52,640 --> 00:03:59,010
 of mindfulness, we have to look ahead to where we're aiming

49
00:03:59,010 --> 00:03:59,640
 for.

50
00:03:59,640 --> 00:04:03,500
 The first bhojanga, the first factor of enlightenment, is

51
00:04:03,500 --> 00:04:07,640
 actually mindfulness.

52
00:04:07,640 --> 00:04:14,630
 And so it's not for any reason that we've been emphasizing

53
00:04:14,630 --> 00:04:17,640
 the foundations of mindfulness the whole way.

54
00:04:17,640 --> 00:04:22,460
 This is the foundation, this is the beginning point, this

55
00:04:22,460 --> 00:04:28,640
 is where we start in our practice.

56
00:04:28,640 --> 00:04:32,900
 So we can understand from this teaching that from the

57
00:04:32,900 --> 00:04:33,640
 Buddha's point of view,

58
00:04:33,640 --> 00:04:37,640
 enlightenment starts with mindfulness.

59
00:04:37,640 --> 00:04:42,890
 And the attainment of enlightenment, or awakening, starts

60
00:04:42,890 --> 00:04:46,640
 with mindfulness.

61
00:04:46,640 --> 00:04:49,640
 When we're mindful of the body, mindful of the feelings,

62
00:04:49,640 --> 00:04:50,640
 mindfulness of the mind,

63
00:04:50,640 --> 00:04:56,640
 mindfulness of our emotions and mind states and so on,

64
00:04:56,640 --> 00:05:01,640
 the senses and aggregates.

65
00:05:01,640 --> 00:05:03,550
 Just these simple mundane things, this is where

66
00:05:03,550 --> 00:05:04,640
 enlightenment starts.

67
00:05:04,640 --> 00:05:12,330
 It really sets the, in the field, the framework for our

68
00:05:12,330 --> 00:05:13,640
 practice.

69
00:05:13,640 --> 00:05:20,580
 Because if you ask an ordinary, untrained person where to

70
00:05:20,580 --> 00:05:23,640
 start looking for enlightenment,

71
00:05:23,640 --> 00:05:27,640
 they'll never think to look inside themselves.

72
00:05:27,640 --> 00:05:30,640
 Maybe look inside themselves, but they'll think it must be

73
00:05:30,640 --> 00:05:31,640
 something special.

74
00:05:31,640 --> 00:05:34,640
 Not seeing, hearing, smelling, tasting, feeling, thinking,

75
00:05:34,640 --> 00:05:36,640
 these aren't special.

76
00:05:36,640 --> 00:05:40,330
 They'll talk about the third eye or the chakras or the kund

77
00:05:40,330 --> 00:05:40,640
alini,

78
00:05:40,640 --> 00:05:43,650
 or they have all these mystical words in different

79
00:05:43,650 --> 00:05:44,640
 traditions.

80
00:05:44,640 --> 00:05:46,640
 God is one, no?

81
00:05:46,640 --> 00:05:50,640
 Enlightenment, awakening is coming to realize God,

82
00:05:50,640 --> 00:05:55,640
 the love of God or becoming one with God or so on.

83
00:05:55,640 --> 00:05:58,870
 If you look at the pattern, it's to find things outside of

84
00:05:58,870 --> 00:06:01,640
 ordinary mundane experience,

85
00:06:01,640 --> 00:06:04,640
 which is why no one ever becomes enlightened.

86
00:06:04,640 --> 00:06:07,020
 Because it's right there under their nose and they look

87
00:06:07,020 --> 00:06:07,640
 everywhere else.

88
00:06:07,640 --> 00:06:10,640
 It's hidden.

89
00:06:10,640 --> 00:06:14,640
 We think enlightenment must be super-mundane.

90
00:06:14,640 --> 00:06:21,100
 So we look and we see, no, that's mundane, that can't be it

91
00:06:21,100 --> 00:06:21,640
.

92
00:06:21,640 --> 00:06:24,900
 When in fact the super-mundane comes from letting go of the

93
00:06:24,900 --> 00:06:25,640
 mundane.

94
00:06:25,640 --> 00:06:28,640
 The only way to let go of the mundane is to understand that

95
00:06:28,640 --> 00:06:31,640
 it's not worth clinging to.

96
00:06:31,640 --> 00:06:34,640
 Not worth pushing away, not worth pulling.

97
00:06:34,640 --> 00:06:38,640
 And understanding that everything about us is mundane.

98
00:06:38,640 --> 00:06:42,310
 All we are is the seeing, hearing, smelling, tasting,

99
00:06:42,310 --> 00:06:43,640
 feeling, thinking,

100
00:06:43,640 --> 00:06:47,640
 the body, the feelings, the mind and the dhamma.

101
00:06:47,640 --> 00:06:49,640
 That's all we are.

102
00:06:49,640 --> 00:06:52,640
 That's all that our experience, all that our existence,

103
00:06:52,640 --> 00:06:52,640
 everything,

104
00:06:52,640 --> 00:06:55,640
 whether it be an experience of God, an experience of heaven

105
00:06:55,640 --> 00:06:58,640
, an experience of self,

106
00:06:58,640 --> 00:07:01,640
 an experience of the third eye or the chakras or so on,

107
00:07:01,640 --> 00:07:05,640
 it's all just seeing, hearing, smelling, tasting, feeling

108
00:07:05,640 --> 00:07:09,640
 and/or thinking.

109
00:07:09,640 --> 00:07:11,640
 No more, no less.

110
00:07:11,640 --> 00:07:13,640
 All of it.

111
00:07:13,640 --> 00:07:20,640
 Any special experience is only this.

112
00:07:20,640 --> 00:07:25,920
 And so to understand reality we have to understand these

113
00:07:25,920 --> 00:07:26,640
 things.

114
00:07:26,640 --> 00:07:29,640
 To awaken to reality.

115
00:07:29,640 --> 00:07:34,920
 We can't be looking for something special because in the

116
00:07:34,920 --> 00:07:37,640
 end it's not special at all.

117
00:07:37,640 --> 00:07:41,570
 We have to look at everything until we see that everything

118
00:07:41,570 --> 00:07:41,640
 is,

119
00:07:41,640 --> 00:07:44,640
 in a way everything is special.

120
00:07:44,640 --> 00:07:47,640
 Everything that arises is special because it's arisen.

121
00:07:47,640 --> 00:07:51,960
 Nothing else has arisen, only this has arisen, so this is

122
00:07:51,960 --> 00:07:53,640
 special.

123
00:07:53,640 --> 00:07:56,640
 And when we treat it as equal to everything else,

124
00:07:56,640 --> 00:07:59,640
 give it as much importance as everything else,

125
00:07:59,640 --> 00:08:06,640
 then we don't cling to it or anything else.

126
00:08:06,640 --> 00:08:08,640
 So mindfulness is the beginning.

127
00:08:08,640 --> 00:08:11,640
 Now what does mindfulness lead to?

128
00:08:11,640 --> 00:08:15,640
 Mindfulness leads to something called dhamma-vitaya,

129
00:08:15,640 --> 00:08:20,830
 which is translated as investigation of reality or

130
00:08:20,830 --> 00:08:22,640
 investigation of truth

131
00:08:22,640 --> 00:08:32,790
 or the knowledge or the studying, the attainment or

132
00:08:32,790 --> 00:08:35,640
 acquisition of knowledge

133
00:08:35,640 --> 00:08:40,640
 about reality.

134
00:08:40,640 --> 00:08:42,640
 This is what mindfulness leads to.

135
00:08:42,640 --> 00:08:44,640
 This is what you should get about the practice.

136
00:08:44,640 --> 00:08:47,640
 Put in plain English, learning about yourself,

137
00:08:47,640 --> 00:08:53,260
 learning more about yourself, your experience and the world

138
00:08:53,260 --> 00:08:54,640
 around you.

139
00:08:54,640 --> 00:08:57,640
 Learning how your mind works, learning how your body works,

140
00:08:57,640 --> 00:09:06,010
 learning how they work together, learning what leads to

141
00:09:06,010 --> 00:09:07,640
 what.

142
00:09:07,640 --> 00:09:10,660
 This is the second factor of enlightenment, is this

143
00:09:10,660 --> 00:09:14,640
 learning that we do.

144
00:09:14,640 --> 00:09:18,640
 You should be thinking every time I sit down,

145
00:09:18,640 --> 00:09:22,640
 "Am I learning something new or not?"

146
00:09:22,640 --> 00:09:24,640
 You shouldn't be thinking, "When I sit down,

147
00:09:24,640 --> 00:09:27,640
 is it more happy and more peaceful than last time?

148
00:09:27,640 --> 00:09:31,640
 Am I more blissful? Am I more calm?"

149
00:09:31,640 --> 00:09:33,640
 You shouldn't be thinking like that.

150
00:09:33,640 --> 00:09:36,640
 This isn't the right direction.

151
00:09:36,640 --> 00:09:39,500
 The right direction is, "Am I learning something every time

152
00:09:39,500 --> 00:09:40,640
 I sit down?"

153
00:09:40,640 --> 00:09:43,640
 Because every time you sit, you should learn something new

154
00:09:43,640 --> 00:09:43,640
 about yourself,

155
00:09:43,640 --> 00:09:46,640
 and it should challenge you.

156
00:09:46,640 --> 00:09:49,400
 Your old ideas about what is meditation should be

157
00:09:49,400 --> 00:09:50,640
 challenged.

158
00:09:50,640 --> 00:09:54,640
 Your old ways of meditating should be challenged.

159
00:09:54,640 --> 00:09:58,640
 It's very easy to get in a rut.

160
00:09:58,640 --> 00:10:03,640
 Anybody who attains some special meditation experience

161
00:10:03,640 --> 00:10:09,640
 and continuously attains that they get in a rut,

162
00:10:09,640 --> 00:10:12,640
 it means they're stuck at that level.

163
00:10:12,640 --> 00:10:15,640
 The only way forward for them is not to develop that more

164
00:10:15,640 --> 00:10:15,640
 and more,

165
00:10:15,640 --> 00:10:19,640
 but is to challenge themselves, to go deeper in it,

166
00:10:19,640 --> 00:10:24,790
 and look at their experience and understand their

167
00:10:24,790 --> 00:10:25,640
 experience.

168
00:10:25,640 --> 00:10:29,640
 When someone practices samatha meditation, for example,

169
00:10:29,640 --> 00:10:32,920
 the only way for them to move on is not to develop more and

170
00:10:32,920 --> 00:10:33,640
 more samatha,

171
00:10:33,640 --> 00:10:37,640
 but to eventually look at the samatha meditation.

172
00:10:37,640 --> 00:10:40,640
 When you have calm or when you have happiness,

173
00:10:40,640 --> 00:10:44,640
 focus on mind to look at that and learn and ask yourself,

174
00:10:44,640 --> 00:10:48,640
 "What is focus? What is calm?"

175
00:10:48,640 --> 00:10:52,870
 Then you come to see that it is impermanent, unsatisfying

176
00:10:52,870 --> 00:10:54,640
 and uncontrollable.

177
00:10:54,640 --> 00:11:00,640
 And then you move on.

178
00:11:00,640 --> 00:11:03,640
 This is number two. Number three.

179
00:11:03,640 --> 00:11:06,640
 What does it lead to when you learn stuff about yourself?

180
00:11:06,640 --> 00:11:08,980
 What good is that? How does that lead you closer to

181
00:11:08,980 --> 00:11:09,640
 awakening?

182
00:11:09,640 --> 00:11:15,640
 It leads to energy. It leads to effort.

183
00:11:15,640 --> 00:11:20,120
 What you should gain from the practice, from seeing the

184
00:11:20,120 --> 00:11:23,640
 truth, is more energy.

185
00:11:23,640 --> 00:11:27,640
 You're more alert. You're more focused.

186
00:11:27,640 --> 00:11:30,540
 And you're more energetic in your awareness and your

187
00:11:30,540 --> 00:11:31,640
 clarity of mind,

188
00:11:31,640 --> 00:11:35,640
 in your practice of mindfulness.

189
00:11:35,640 --> 00:11:37,880
 So in developing mindfulness in the beginning, it's

190
00:11:37,880 --> 00:11:38,640
 difficult.

191
00:11:38,640 --> 00:11:41,640
 But what you should see as you progress is that because of

192
00:11:41,640 --> 00:11:45,640
 the confidence that you gain,

193
00:11:45,640 --> 00:11:48,640
 and the focus that you gain.

194
00:11:48,640 --> 00:11:52,430
 So instead of wasting your energy on doubting or thinking

195
00:11:52,430 --> 00:11:53,640
 about this or that,

196
00:11:53,640 --> 00:11:57,640
 "Am I doing the right thing? Am I practicing correctly?"

197
00:11:57,640 --> 00:12:01,640
 Once you realize the truth and more and more understand,

198
00:12:01,640 --> 00:12:04,990
 "That's useless. This is useless. Stop that. That's not

199
00:12:04,990 --> 00:12:05,640
 benefiting me."

200
00:12:05,640 --> 00:12:10,640
 And you focus your energies and you say, "This is useful."

201
00:12:10,640 --> 00:12:14,450
 And you gain confidence and focus, and as a result your

202
00:12:14,450 --> 00:12:15,640
 mind becomes charged.

203
00:12:15,640 --> 00:12:20,250
 And you have this great energy and you're mindful

204
00:12:20,250 --> 00:12:21,640
 continuously.

205
00:12:21,640 --> 00:12:25,780
 You're able to be mindful in succession because of your

206
00:12:25,780 --> 00:12:26,640
 effort.

207
00:12:26,640 --> 00:12:27,400
 [B

208
00:12:27,400 --> 00:12:33,640
 S]

209
00:12:33,640 --> 00:12:37,640
 [S]

210
00:12:37,640 --> 00:12:40,640
 And because of the focus, your effort becomes focused.

211
00:12:40,640 --> 00:12:44,640
 Instead of having to push and push and push with no benefit

212
00:12:44,640 --> 00:12:45,640
,

213
00:12:45,640 --> 00:12:48,640
 you start to refine your effort as well.

214
00:12:48,640 --> 00:12:52,640
 So the vidya bhojanga, the enlightenment factor of effort,

215
00:12:52,640 --> 00:12:55,640
 isn't just pushing and pushing and pushing.

216
00:12:55,640 --> 00:12:59,640
 What it means is you are strong in mind.

217
00:12:59,640 --> 00:13:02,640
 You're able to fight the defilements.

218
00:13:02,640 --> 00:13:07,640
 When bad things come up, you're able to push them away.

219
00:13:07,640 --> 00:13:13,640
 You're able to step like dancing to avoid the defilements.

220
00:13:13,640 --> 00:13:17,300
 So when a defilement might arise, you're able to see it

221
00:13:17,300 --> 00:13:17,640
 clearly,

222
00:13:17,640 --> 00:13:24,640
 seeing or hearing, and the defilement doesn't even arise.

223
00:13:24,640 --> 00:13:28,640
 You know what is a benefit, and you know how to create

224
00:13:28,640 --> 00:13:29,640
 wholesome thoughts

225
00:13:29,640 --> 00:13:31,640
 and wholesome mind states.

226
00:13:31,640 --> 00:13:35,640
 And so you are quicker to create them.

227
00:13:35,640 --> 00:13:41,240
 They come with more power, and when they come you're able

228
00:13:41,240 --> 00:13:44,640
 to keep them.

229
00:13:44,640 --> 00:13:47,820
 The effort in meditation practice that we're looking for is

230
00:13:47,820 --> 00:13:49,640
 not pushing, pushing, pushing.

231
00:13:49,640 --> 00:13:54,640
 It's keeping away bad states of mind.

232
00:13:54,640 --> 00:13:57,640
 It's kind of like a kung fu you could think of it as.

233
00:13:57,640 --> 00:14:03,640
 Not just pushing, pushing, pushing, but being able to dance

234
00:14:03,640 --> 00:14:07,640
 so that the bad things disappear and don't come,

235
00:14:07,640 --> 00:14:12,640
 and the good things appear and stay.

236
00:14:12,640 --> 00:14:31,640
 It's a powerful ability, your technique,

237
00:14:31,640 --> 00:14:40,640
 your ability to arrange your mind in the right way.

238
00:14:40,640 --> 00:14:45,640
 This is the effort to fight against.

239
00:14:45,640 --> 00:14:48,640
 When bad things come up, you get skillful.

240
00:14:48,640 --> 00:14:51,640
 Another way of thinking of this is you have the skills and

241
00:14:51,640 --> 00:14:51,640
 the kung fu.

242
00:14:51,640 --> 00:14:54,020
 So when a bad thing comes up, you know not to get angry

243
00:14:54,020 --> 00:14:54,640
 about it.

244
00:14:54,640 --> 00:14:59,640
 You know how to deal with it. Okay, angry, angry.

245
00:14:59,640 --> 00:15:01,640
 And you're really good at this mindfulness.

246
00:15:01,640 --> 00:15:05,640
 This is the effort that comes.

247
00:15:05,640 --> 00:15:08,640
 The energy or the strength, you might say.

248
00:15:08,640 --> 00:15:11,640
 So in the beginning your mindfulness is quite weak.

249
00:15:11,640 --> 00:15:14,640
 You say, "Pain, pain," and it doesn't go away.

250
00:15:14,640 --> 00:15:18,200
 Eventually you get so good that you know exactly, you dance

251
00:15:18,200 --> 00:15:18,640
 with it,

252
00:15:18,640 --> 00:15:22,640
 and you surf on the pain. It's like surfing on the wave.

253
00:15:22,640 --> 00:15:26,180
 When you go as a kid to play in the ocean, the waves drag

254
00:15:26,180 --> 00:15:27,640
 you under.

255
00:15:27,640 --> 00:15:33,430
 But when you become a surfer, you know how to ride the wave

256
00:15:33,430 --> 00:15:33,640
.

257
00:15:33,640 --> 00:15:35,640
 You get like that.

258
00:15:35,640 --> 00:15:38,640
 This is what we mean by effort, not just pushing,

259
00:15:38,640 --> 00:15:41,640
 but you get good at being mindful.

260
00:15:41,640 --> 00:15:45,550
 Once you get good at being mindful and you have the

261
00:15:45,550 --> 00:15:47,640
 strength of mind,

262
00:15:47,640 --> 00:15:49,640
 it gets you in a rut as well.

263
00:15:49,640 --> 00:15:55,640
 But this is a good rut, it's called "piti," or "rapture."

264
00:15:55,640 --> 00:16:00,270
 You imagine getting so good and developing it so much that

265
00:16:00,270 --> 00:16:02,640
 it becomes a habit.

266
00:16:02,640 --> 00:16:05,640
 I think because we have all these bad habits.

267
00:16:05,640 --> 00:16:09,640
 Bad habits of getting angry, bad habits of being greedy,

268
00:16:09,640 --> 00:16:13,790
 bad habits of saying this or saying that or wanting this or

269
00:16:13,790 --> 00:16:14,640
 wanting that

270
00:16:14,640 --> 00:16:18,640
 or doing this or doing that.

271
00:16:18,640 --> 00:16:21,750
 Isn't it wonderful to think that you could have a habit of

272
00:16:21,750 --> 00:16:22,640
 being mindful,

273
00:16:22,640 --> 00:16:25,640
 a habit of seeing things clearly,

274
00:16:25,640 --> 00:16:28,640
 so that when you're faced with any problem,

275
00:16:28,640 --> 00:16:33,640
 you get into the habit of just being mindful of it.

276
00:16:33,640 --> 00:16:36,640
 This is what it means by rapture because your mind gets

277
00:16:36,640 --> 00:16:38,640
 this tendency,

278
00:16:38,640 --> 00:16:44,750
 starts to develop it until it becomes kind of like a rapt

279
00:16:44,750 --> 00:16:45,640
ure.

280
00:16:45,640 --> 00:16:50,640
 Rapture meaning that your mind is stuck and fixed in it.

281
00:16:50,640 --> 00:16:55,640
 And it starts to become second nature, this mindfulness.

282
00:16:55,640 --> 00:16:59,040
 So that you can do it all the time with it when you're

283
00:16:59,040 --> 00:16:59,640
 eating your mind,

284
00:16:59,640 --> 00:17:03,090
 when you're bathing your mind, when you're going to toilet

285
00:17:03,090 --> 00:17:03,640
 your mind,

286
00:17:03,640 --> 00:17:09,640
 when you're just walking around the monastery in your mind.

287
00:17:09,640 --> 00:17:12,540
 When you're waking up your mind, when you're falling asleep

288
00:17:12,540 --> 00:17:16,640
, your mind.

289
00:17:16,640 --> 00:17:24,640
 It just starts to become second nature. This is rapture.

290
00:17:24,640 --> 00:17:29,580
 You get stuck in a rut. And because you get stuck in this r

291
00:17:29,580 --> 00:17:31,640
ut of being mindful,

292
00:17:31,640 --> 00:17:36,600
 because your mind is more and more and more and more and

293
00:17:36,600 --> 00:17:38,640
 more clear,

294
00:17:38,640 --> 00:17:42,270
 your mind starts to calm down. And then there's this calm

295
00:17:42,270 --> 00:17:43,640
 that kind of lasts.

296
00:17:43,640 --> 00:17:46,900
 It's necessary that if you really do feel calm, it's

297
00:17:46,900 --> 00:17:48,640
 necessary to focus on the calm.

298
00:17:48,640 --> 00:17:53,640
 But it starts to become kind of natural, quiet.

299
00:17:53,640 --> 00:17:58,640
 This is called basati. Your mind becomes tranquil.

300
00:17:58,640 --> 00:18:01,640
 Whereas before you might have been thinking a lot.

301
00:18:01,640 --> 00:18:08,160
 Once you really get into the practice, there's not much to

302
00:18:08,160 --> 00:18:08,640
 think about.

303
00:18:08,640 --> 00:18:14,640
 There's only like a soldier, left, right, left, right.

304
00:18:14,640 --> 00:18:19,530
 Doing performing the drills, doing your dudhi, following

305
00:18:19,530 --> 00:18:20,640
 the schedule.

306
00:18:20,640 --> 00:18:25,510
 The mind becomes very well trained, this is the point, like

307
00:18:25,510 --> 00:18:26,640
 a soldier.

308
00:18:26,640 --> 00:18:29,640
 The mind becomes very well trained.

309
00:18:29,640 --> 00:18:34,410
 Whereas before it was very untrained in doing this and that

310
00:18:34,410 --> 00:18:36,640
, and running here and running there.

311
00:18:36,640 --> 00:18:42,640
 That becomes quite trained. So it gets quiet.

312
00:18:42,640 --> 00:18:46,640
 Also in the beginning it was fighting against it.

313
00:18:46,640 --> 00:18:51,680
 And as you practice again and the old habits are replaced

314
00:18:51,680 --> 00:18:53,640
 by the new habit of mindfulness,

315
00:18:53,640 --> 00:19:00,640
 it quiets down all of the rebellion, it quiets down.

316
00:19:00,640 --> 00:19:03,640
 The mind begins to order itself.

317
00:19:03,640 --> 00:19:06,750
 So you're saying, "No, this, then, this," and running

318
00:19:06,750 --> 00:19:07,640
 everywhere.

319
00:19:07,640 --> 00:19:15,640
 Now it's ordered and orderly. Your mind is on track.

320
00:19:15,640 --> 00:19:24,640
 Pasadhi then Samadhi.

321
00:19:24,640 --> 00:19:29,640
 Then the mind becomes balanced or focused.

322
00:19:29,640 --> 00:19:33,370
 Samadhi, we always translate as concentrated, but I think

323
00:19:33,370 --> 00:19:34,640
 that really misses the point.

324
00:19:34,640 --> 00:19:45,640
 Samadhi, Sama means same or level.

325
00:19:45,640 --> 00:19:50,640
 This is where your mind becomes very, very clear.

326
00:19:50,640 --> 00:19:55,470
 In tune you can think of like a musical instrument that's

327
00:19:55,470 --> 00:19:57,640
 now perfectly in tune.

328
00:19:57,640 --> 00:20:03,610
 You play the guitar, for example. You can tell when it's in

329
00:20:03,610 --> 00:20:05,640
 tune and out of tune.

330
00:20:05,640 --> 00:20:09,680
 And as you develop and develop and quiet, your mind starts

331
00:20:09,680 --> 00:20:10,640
 to become in tune.

332
00:20:10,640 --> 00:20:16,640
 So every time your mind points, like ringing a bell, ding,

333
00:20:16,640 --> 00:20:18,640
 catching it.

334
00:20:18,640 --> 00:20:24,820
 Perfectly catching the A in B with just enough effort, just

335
00:20:24,820 --> 00:20:27,640
 enough concentration.

336
00:20:27,640 --> 00:20:33,640
 This is a focused mind.

337
00:20:33,640 --> 00:20:36,640
 Your mind is able to catch when you say, "Seeing."

338
00:20:36,640 --> 00:20:40,360
 You really are aware of the seeing, hearing, you really are

339
00:20:40,360 --> 00:20:40,640
.

340
00:20:40,640 --> 00:20:43,640
 This takes time. Something you have to understand.

341
00:20:43,640 --> 00:20:45,640
 You don't come here mindful.

342
00:20:45,640 --> 00:20:47,910
 You don't give you the technique, you say, "I get it," and

343
00:20:47,910 --> 00:20:50,640
 you do it and you're enlightened.

344
00:20:50,640 --> 00:20:56,640
 You say, "I get it," and, "Oh no, but I can't do it."

345
00:20:56,640 --> 00:20:59,100
 Then you work and you work and you practice and you

346
00:20:59,100 --> 00:20:59,640
 practice.

347
00:20:59,640 --> 00:21:02,640
 Then you start to think, "What am I doing this for?"

348
00:21:02,640 --> 00:21:05,640
 You don't realize, like the movie The Karate Kid.

349
00:21:05,640 --> 00:21:09,640
 You never saw the original Karate Kid, this movie.

350
00:21:09,640 --> 00:21:12,640
 He has some paint defense.

351
00:21:12,640 --> 00:21:17,640
 He hasn't waxed the cars. He comes back and he gets angry

352
00:21:17,640 --> 00:21:18,640
 and he says,

353
00:21:18,640 --> 00:21:22,810
 "Look, I'm happy to help you out by painting your fence and

354
00:21:22,810 --> 00:21:23,640
 waxing your cars,

355
00:21:23,640 --> 00:21:27,640
 but when am I going to learn Karate?"

356
00:21:27,640 --> 00:21:31,640
 He says, "Show me how to paint the fence."

357
00:21:31,640 --> 00:21:35,380
 He goes up and down and then he punches him and he blocks

358
00:21:35,380 --> 00:21:40,640
 it with his fence painting motion.

359
00:21:40,640 --> 00:21:43,780
 "How do you wash wax cars?" and he shows him how to wax it

360
00:21:43,780 --> 00:21:46,640
 and he punches him and he blocks it

361
00:21:46,640 --> 00:21:50,640
 with his car waxing motion.

362
00:21:50,640 --> 00:21:54,640
 Very good analogy for what we're doing.

363
00:21:54,640 --> 00:21:59,490
 "What are we doing?" He's singing, singing, and paying him,

364
00:21:59,490 --> 00:22:01,640
 "What's it for?"

365
00:22:01,640 --> 00:22:04,640
 We're getting better, better at that.

366
00:22:04,640 --> 00:22:13,840
 "What's it for?" It's for blocking the defilement, for

367
00:22:13,840 --> 00:22:20,640
 stopping our projections, our judgments.

368
00:22:20,640 --> 00:22:23,980
 And only when you get really, really good at it do you see

369
00:22:23,980 --> 00:22:24,640
 the point, do you understand,

370
00:22:24,640 --> 00:22:29,640
 do you realize what you've been doing all this time?

371
00:22:29,640 --> 00:22:34,640
 You've been training yourself.

372
00:22:34,640 --> 00:22:38,630
 But you're training yourself to stop hating yourself, to

373
00:22:38,630 --> 00:22:44,640
 stop becoming addicted to things,

374
00:22:44,640 --> 00:22:48,640
 to stop your judgments, your partiality.

375
00:22:48,640 --> 00:22:52,640
 Training yourself to just see things as they are.

376
00:22:52,640 --> 00:22:58,390
 It sounds so simple. I have the easy job to teach you how

377
00:22:58,390 --> 00:22:59,640
 to do it.

378
00:22:59,640 --> 00:23:03,520
 It's very easy to teach, really. You just have to say the

379
00:23:03,520 --> 00:23:04,640
 right things.

380
00:23:04,640 --> 00:23:12,830
 In practice you have to actually do them. That's the hard

381
00:23:12,830 --> 00:23:15,640
 part.

382
00:23:15,640 --> 00:23:18,640
 And what finally does the focus lead to?

383
00:23:18,640 --> 00:23:22,320
 What good is it to be focused? What good is it to have this

384
00:23:22,320 --> 00:23:23,640
 clarity of mind?

385
00:23:23,640 --> 00:23:27,720
 The final bhojanga, the final factor of enlightenment is

386
00:23:27,720 --> 00:23:29,640
 equanimity.

387
00:23:29,640 --> 00:23:32,640
 There's a lot of talk about equanimity in Buddhism.

388
00:23:32,640 --> 00:23:35,640
 We have to understand equanimity.

389
00:23:35,640 --> 00:23:39,240
 It's not the dumb equanimity of like a cow or something, or

390
00:23:39,240 --> 00:23:41,640
 a goat.

391
00:23:41,640 --> 00:23:45,640
 Even though they can be very angry at times.

392
00:23:45,640 --> 00:23:48,970
 They look at animals and they seem very equanimous because

393
00:23:48,970 --> 00:23:51,140
 they have so much delusion that they can't really think

394
00:23:51,140 --> 00:23:51,640
 clearly.

395
00:23:51,640 --> 00:23:55,030
 They aren't able to stay angry or greedy for a long time

396
00:23:55,030 --> 00:23:57,640
 because they have too much delusion.

397
00:23:57,640 --> 00:23:59,640
 So they just feel dumb.

398
00:23:59,640 --> 00:24:02,910
 And we can feel dumb when we're using the computer, for

399
00:24:02,910 --> 00:24:03,640
 example.

400
00:24:03,640 --> 00:24:08,640
 You just feel dumb. Equanimous.

401
00:24:08,640 --> 00:24:14,140
 But this isn't what is meant by an equanimity. Equanimity

402
00:24:14,140 --> 00:24:18,640
 is like your intellectual perception of things.

403
00:24:18,640 --> 00:24:23,360
 Because we always have such a bad... we give such a bad rap

404
00:24:23,360 --> 00:24:25,640
 to intellectualizing.

405
00:24:25,640 --> 00:24:28,640
 Buddhism is saying, "No, don't intellectualize."

406
00:24:28,640 --> 00:24:31,640
 Why? Because that's where all of our judgments come from.

407
00:24:31,640 --> 00:24:34,640
 That's where all of our views come from.

408
00:24:34,640 --> 00:24:39,890
 When you practice on the lower level of experience, all of

409
00:24:39,890 --> 00:24:44,490
 that gets filtered out until your intellectual activity is

410
00:24:44,490 --> 00:24:46,640
 totally neutral.

411
00:24:46,640 --> 00:24:49,640
 You become kind of dumb in a sense.

412
00:24:49,640 --> 00:24:53,460
 All of that intellectual chatter, "I don't agree with this.

413
00:24:53,460 --> 00:24:54,640
 That's no good."

414
00:24:54,640 --> 00:24:59,640
 It all levels out.

415
00:24:59,640 --> 00:25:03,970
 You say, "This is good. That's good. This is bad. That's

416
00:25:03,970 --> 00:25:04,640
 bad."

417
00:25:04,640 --> 00:25:07,640
 You say, "Oh, this is seeing."

418
00:25:07,640 --> 00:25:09,640
 And intellectually you'll actually do that.

419
00:25:09,640 --> 00:25:13,410
 In the beginning you have to just shut up, shut up, shut up

420
00:25:13,410 --> 00:25:14,640
, put it aside.

421
00:25:14,640 --> 00:25:17,640
 Stop it.

422
00:25:17,640 --> 00:25:22,980
 When you get to ubeka, which is this ubeka bhojanga, you

423
00:25:22,980 --> 00:25:25,640
 don't have to do that anymore.

424
00:25:25,640 --> 00:25:28,640
 It's kind of amazing, actually.

425
00:25:28,640 --> 00:25:32,640
 The mind is suddenly just seeing.

426
00:25:32,640 --> 00:25:39,640
 But that's seeing. But this, oh, it's hearing.

427
00:25:39,640 --> 00:25:43,640
 There's nothing else in the mind.

428
00:25:43,640 --> 00:25:48,690
 When you get to this point, that your mind is just not even

429
00:25:48,690 --> 00:25:53,640
 training anymore. It's knowing.

430
00:25:53,640 --> 00:25:57,290
 That's the moment, that's the time when you're on the edge

431
00:25:57,290 --> 00:26:00,640
 of enlightenment, when you're ready to enter into nirvana,

432
00:26:00,640 --> 00:26:04,640
 when the mind will slide off.

433
00:26:04,640 --> 00:26:14,640
 It's kind of like, the ordinary mind is like Velcro.

434
00:26:14,640 --> 00:26:23,910
 Or, no, like if you have two substances with glue, or with

435
00:26:23,910 --> 00:26:28,640
 sticky, something sticky in between them.

436
00:26:28,640 --> 00:26:34,810
 Like if you put jam, your hands are covered in jam and it

437
00:26:34,810 --> 00:26:36,640
 gets sticky.

438
00:26:36,640 --> 00:26:39,780
 And it seems like that's the nature of them, to stick

439
00:26:39,780 --> 00:26:40,640
 together.

440
00:26:40,640 --> 00:26:43,530
 You think, uh oh, the hands are all sticky and they're

441
00:26:43,530 --> 00:26:47,480
 stuck together. But when you pour water over them, they

442
00:26:47,480 --> 00:26:50,640
 just slide off.

443
00:26:50,640 --> 00:26:54,640
 Try to think of some sticky things, to give a bad example.

444
00:26:54,640 --> 00:27:02,180
 But you have this glue, no? When you pour water under, the

445
00:27:02,180 --> 00:27:08,640
 glue just dissolves in the water.

446
00:27:08,640 --> 00:27:11,710
 This is kind of how enlightenment occurs, how the one goes

447
00:27:11,710 --> 00:27:15,660
 into nirvana. It's not like suddenly broken or something

448
00:27:15,660 --> 00:27:16,640
 like that.

449
00:27:16,640 --> 00:27:23,140
 Or you suddenly pull away from samsara, you slide off of it

450
00:27:23,140 --> 00:27:23,640
.

451
00:27:23,640 --> 00:27:32,740
 Slowly, slowly the water dissolves the glue and shoom, the

452
00:27:32,740 --> 00:27:35,640
 mind slides off.

453
00:27:35,640 --> 00:27:38,520
 Because that's what's happening, right? Your clinging is

454
00:27:38,520 --> 00:27:42,610
 becoming less and less, until you're just seeing things as

455
00:27:42,610 --> 00:27:43,640
 they are.

456
00:27:43,640 --> 00:27:47,640
 Just experiencing things as they are.

457
00:27:47,640 --> 00:27:55,640
 And less and less clinging, less and less clinging, until

458
00:27:55,640 --> 00:27:59,640
 the mind flies and enters into nirvana.

459
00:27:59,640 --> 00:28:01,640
 This is the experience.

460
00:28:01,640 --> 00:28:06,360
 I can't explain too much about nirvana only. One thing my

461
00:28:06,360 --> 00:28:10,240
 teacher always said, always keep in mind when you're

462
00:28:10,240 --> 00:28:15,640
 wondering about nirvana, this is not that.

463
00:28:15,640 --> 00:28:19,230
 You have a question about nirvana, is this nirvana or is

464
00:28:19,230 --> 00:28:20,640
 this what is this?

465
00:28:20,640 --> 00:28:22,970
 Because you're thinking, is this the special thing that I'm

466
00:28:22,970 --> 00:28:27,640
 looking for? Always make your answer be, this is not that.

467
00:28:27,640 --> 00:28:30,640
 Because it's not.

468
00:28:30,640 --> 00:28:34,560
 That's all I can say about nirvana. If you keep that in

469
00:28:34,560 --> 00:28:38,640
 mind, you'll be okay. This is not that.

470
00:28:38,640 --> 00:28:42,070
 Then you always remind yourself and you won't get, because

471
00:28:42,070 --> 00:28:45,640
 otherwise you'll get stuck on something that is not that.

472
00:28:45,640 --> 00:28:51,640
 When you realize nirvana, I guess you could say ineffable.

473
00:28:51,640 --> 00:28:54,960
 There's no seeing, no hearing, no smelling, no tasting, no

474
00:28:54,960 --> 00:28:58,640
 feeling and no thinking.

475
00:28:58,640 --> 00:29:08,970
 There's not even any mind, the mind has become extinguished

476
00:29:08,970 --> 00:29:14,020
. There is a mind, but it's just technically, really the

477
00:29:14,020 --> 00:29:17,640
 mind has become free.

478
00:29:17,640 --> 00:29:21,640
 Because if you think the mind always has to take an object,

479
00:29:21,640 --> 00:29:24,830
 if you think of a mind without an object, you don't really

480
00:29:24,830 --> 00:29:25,640
 have a mind.

481
00:29:25,640 --> 00:29:34,930
 But that's this. This is a mind which has no object in a

482
00:29:34,930 --> 00:29:36,640
 sense.

483
00:29:36,640 --> 00:29:40,880
 They say it has nirvana as an object, but what does that

484
00:29:40,880 --> 00:29:41,640
 mean?

485
00:29:41,640 --> 00:29:49,640
 What does it mean to say it has the unbinding as an object?

486
00:29:49,640 --> 00:29:53,290
 Whatever object your mind takes and you think, "Is this it

487
00:29:53,290 --> 00:29:59,640
?" Tell yourself, "What is this? This is not that."

488
00:29:59,640 --> 00:30:06,840
 But here we have a path. I think it's quite clear where it

489
00:30:06,840 --> 00:30:11,630
's leading and it's quite reasonable to assume that this

490
00:30:11,630 --> 00:30:15,640
 path is at least worth checking out.

491
00:30:15,640 --> 00:30:19,060
 And of course, when we develop mindfulness, we see it doesn

492
00:30:19,060 --> 00:30:23,920
't lead to dhammavicaya, doesn't lead to viriya, viriya, v

493
00:30:23,920 --> 00:30:28,640
iti, pasuti, samadhi, utteka, they lead in this order.

494
00:30:28,640 --> 00:30:34,620
 And if they do, when we see that they do, then no need to

495
00:30:34,620 --> 00:30:36,640
 doubt anymore.

496
00:30:36,640 --> 00:30:43,580
 So anyway, this is kind of another hint or another glimpse

497
00:30:43,580 --> 00:30:48,640
 or another guide on which to base our practice.

498
00:30:48,640 --> 00:30:56,640
 Now we get to do the real work, get on with our practice.

499
00:30:56,640 --> 00:31:02,640
 Now we can start mindful prostration, walking and sitting.

