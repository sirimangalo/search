1
00:00:00,000 --> 00:00:03,120
 Mindfulness and Noting

2
00:00:03,120 --> 00:00:05,120
 Question.

3
00:00:05,120 --> 00:00:07,920
 What does it mean to note something?

4
00:00:07,920 --> 00:00:10,800
 What is the thing that you are noting?

5
00:00:10,800 --> 00:00:14,240
 What is the difference between noting and being mindful if

6
00:00:14,240 --> 00:00:17,600
 there is any, or the relationship between them?

7
00:00:17,600 --> 00:00:19,200
 Answer.

8
00:00:19,200 --> 00:00:22,990
 The word mindfulness is an imperfect translation of the

9
00:00:22,990 --> 00:00:23,920
 word sati.

10
00:00:23,920 --> 00:00:27,680
 Sati is the state of mind that rightly grasps the object in

11
00:00:27,680 --> 00:00:30,400
 such a way that it can be seen clearly.

12
00:00:30,400 --> 00:00:33,900
 Sati can also be used in a conventional sense as the

13
00:00:33,900 --> 00:00:38,000
 ability to remember things that happened a long time ago.

14
00:00:38,000 --> 00:00:41,900
 In meditation, it means the ability to remember the present

15
00:00:41,900 --> 00:00:42,640
 moment.

16
00:00:42,640 --> 00:00:47,870
 It means not being distracted by or dwelling in abstract

17
00:00:47,870 --> 00:00:49,040
 thought.

18
00:00:49,040 --> 00:00:54,480
 Abstract thought is not evil or bad, but it is not mindful.

19
00:00:54,480 --> 00:00:58,200
 In our everyday activities, we have to rely on abstract

20
00:00:58,200 --> 00:00:59,120
 concepts.

21
00:00:59,120 --> 00:01:02,160
 Many of our ordinary activities will be difficult to

22
00:01:02,160 --> 00:01:05,600
 perform mindfully because being mindful means only being

23
00:01:05,600 --> 00:01:08,160
 aware of the moments of experience.

24
00:01:08,160 --> 00:01:12,000
 It is hard to maintain a stream of conceptual thought when

25
00:01:12,000 --> 00:01:13,360
 you are mindful.

26
00:01:13,360 --> 00:01:16,620
 Likewise, when we dwell in abstract thought, we are

27
00:01:16,620 --> 00:01:18,880
 generally unmindful of reality.

28
00:01:18,880 --> 00:01:22,580
 For example, you are probably sitting while reading this,

29
00:01:22,580 --> 00:01:26,410
 but you likely have no awareness of that fact while reading

30
00:01:26,410 --> 00:01:26,720
.

31
00:01:26,720 --> 00:01:29,540
 When you are caught up in abstract thought, you quickly

32
00:01:29,540 --> 00:01:31,120
 forget that you are sitting.

33
00:01:31,120 --> 00:01:34,000
 But when you read, did you know that you are sitting?

34
00:01:34,000 --> 00:01:37,440
 Suddenly, you are reminded that you are sitting.

35
00:01:37,440 --> 00:01:40,450
 I think this is why the Buddha chose the word sati to

36
00:01:40,450 --> 00:01:44,240
 describe meditation not just as a state, but as an activity

37
00:01:44,240 --> 00:01:45,520
, remembering.

38
00:01:45,520 --> 00:01:49,260
 Sati is what leads to seeing clearly, and the act of

39
00:01:49,260 --> 00:01:53,770
 cultivating sati is this activity of reminding yourself of

40
00:01:53,770 --> 00:01:56,560
 your experiential reality.

41
00:01:56,560 --> 00:02:00,880
 Abstract thought is useful for cultivating right view.

42
00:02:00,880 --> 00:02:04,560
 Abstraction is the basis of language, so for someone else

43
00:02:04,560 --> 00:02:07,940
 to remind us of something, they need to use words that

44
00:02:07,940 --> 00:02:10,080
 describe abstract concepts.

45
00:02:10,080 --> 00:02:12,640
 Words themselves are also concepts.

46
00:02:12,640 --> 00:02:16,800
 The word sati does not mean anything, it is just a sound.

47
00:02:16,800 --> 00:02:20,350
 Its meaning is only conceptual based on how our minds

48
00:02:20,350 --> 00:02:21,920
 interpret the sound.

49
00:02:21,920 --> 00:02:25,780
 We know what sati refers to, and based on our faculties of

50
00:02:25,780 --> 00:02:29,410
 memory and recognition, we associate that word with a

51
00:02:29,410 --> 00:02:30,800
 certain mind state.

52
00:02:30,800 --> 00:02:34,320
 The mind state described by sati is real.

53
00:02:34,320 --> 00:02:38,300
 Mindfulness is real, but the word mindfulness is just a

54
00:02:38,300 --> 00:02:39,200
 concept.

55
00:02:39,200 --> 00:02:43,630
 When I say mindfulness, it evokes a perception of certain

56
00:02:43,630 --> 00:02:47,680
 real mind states that are useful and not abstract.

57
00:02:47,680 --> 00:02:51,040
 You may find this teaching useful.

58
00:02:51,040 --> 00:02:54,150
 Teaching is a useful abstraction that can change the way

59
00:02:54,150 --> 00:02:56,890
 you look at things and allow you to see reality in a

60
00:02:56,890 --> 00:02:57,920
 different way.

61
00:02:57,920 --> 00:03:00,970
 But it will never be a replacement for the experience of

62
00:03:00,970 --> 00:03:02,400
 reality for yourself.

63
00:03:02,400 --> 00:03:04,400
 It will never be enough.

64
00:03:04,400 --> 00:03:07,370
 You could listen to all of my talks and it would never be

65
00:03:07,370 --> 00:03:10,490
 enough, unless you transform the teachings into actual

66
00:03:10,490 --> 00:03:13,440
 practice that allows you to cultivate the realities

67
00:03:13,440 --> 00:03:15,760
 described by the teachings.

68
00:03:15,760 --> 00:03:18,320
 This is where noting comes in.

69
00:03:18,320 --> 00:03:22,010
 Noting is a sort of gateway between the teaching and the

70
00:03:22,010 --> 00:03:23,120
 mind states.

71
00:03:23,120 --> 00:03:26,830
 Noting simply means saying a word to yourself that has a

72
00:03:26,830 --> 00:03:30,480
 connection to the reality you are experiencing at that

73
00:03:30,480 --> 00:03:31,280
 moment.

74
00:03:31,280 --> 00:03:34,400
 When you engage in this practice of saying something to

75
00:03:34,400 --> 00:03:37,600
 yourself, this is what we call mantra meditation.

76
00:03:37,600 --> 00:03:40,080
 Mantra meditation is very old.

77
00:03:40,080 --> 00:03:43,330
 It was around before the Buddha himself and is widespread

78
00:03:43,330 --> 00:03:48,190
 and broadly recognized today by many religious, spiritual,

79
00:03:48,190 --> 00:03:51,280
 and even secular practices as an effective tool because it

80
00:03:51,280 --> 00:03:52,960
 is the simplest means of focusing the mind state.

81
00:03:52,960 --> 00:04:01,600
 It is similar to how we use words to express emotion.

82
00:04:01,600 --> 00:04:06,070
 If I yell at someone and say "you hurt me" it focuses my

83
00:04:06,070 --> 00:04:09,610
 mind on the anger and self-righteous indignation I feel in

84
00:04:09,610 --> 00:04:13,360
 that moment, augmenting those mind states.

85
00:04:13,360 --> 00:04:15,280
 Words are quite powerful.

86
00:04:15,280 --> 00:04:19,510
 They have the effect of augmenting our mental states and

87
00:04:19,510 --> 00:04:22,480
 emotions, often to our detriment.

88
00:04:22,480 --> 00:04:26,270
 We speak words in response to many of our experiences in

89
00:04:26,270 --> 00:04:27,600
 ordinary life.

90
00:04:27,600 --> 00:04:31,080
 When you like something, you may say to yourself "this is

91
00:04:31,080 --> 00:04:34,720
 great" which reinforces your attachment to that thing.

92
00:04:34,720 --> 00:04:38,040
 Mantras make use of this process by which words evoke

93
00:04:38,040 --> 00:04:39,840
 specific states of mind.

94
00:04:39,840 --> 00:04:43,030
 Self-help gurus often suggest mantras that help their

95
00:04:43,030 --> 00:04:46,690
 followers create specific states of mind, stimulating the

96
00:04:46,690 --> 00:04:48,640
 mind in a particular way.

97
00:04:48,640 --> 00:04:52,210
 It is not common for someone to intentionally practice with

98
00:04:52,210 --> 00:04:55,790
 a mantra that stimulates unwholesome qualities like greed

99
00:04:55,790 --> 00:04:59,480
 or anger, but we often use words that evoke unwholesome

100
00:04:59,480 --> 00:05:01,520
 mind states unknowingly.

101
00:05:01,520 --> 00:05:04,560
 This is why meditation can be dangerous.

102
00:05:04,560 --> 00:05:08,160
 Quite conceivably, you could create a meditation practice

103
00:05:08,160 --> 00:05:12,090
 that would make you more angry, more greedy, or more del

104
00:05:12,090 --> 00:05:12,800
uded.

105
00:05:12,800 --> 00:05:15,830
 Mantras that stimulate delusion may be the most common, as

106
00:05:15,830 --> 00:05:18,940
 religious teachings that have wrong views often involve

107
00:05:18,940 --> 00:05:23,040
 teachings, sayings, mottos, and even mantras that support

108
00:05:23,040 --> 00:05:24,880
 delusion.

109
00:05:24,880 --> 00:05:28,800
 A good meditation may be used to evoke positive emotions.

110
00:05:28,800 --> 00:05:32,020
 One example that should come to mind for many people is M

111
00:05:32,020 --> 00:05:36,040
etta, or friendliness meditation, where we use the mantra "

112
00:05:36,040 --> 00:05:39,920
may I be happy, may all beings be happy."

113
00:05:39,920 --> 00:05:43,180
 The words themselves are not wholesome, but they have the

114
00:05:43,180 --> 00:05:46,350
 potential to evoke wholesome emotive states like friend

115
00:05:46,350 --> 00:05:48,720
liness, compassion, and kindness.

116
00:05:48,720 --> 00:05:51,740
 Other wholesome meditations are designed to cultivate not

117
00:05:51,740 --> 00:05:55,780
 emotions, but certain other qualities of mind like focus or

118
00:05:55,780 --> 00:05:56,880
 clarity.

119
00:05:56,880 --> 00:05:59,520
 Cassina meditation is a good example.

120
00:05:59,520 --> 00:06:01,600
 Cassina means totality.

121
00:06:01,600 --> 00:06:04,790
 A Cassina is an object that you use to create a sense of

122
00:06:04,790 --> 00:06:08,440
 totality, where your whole world is just one thing, like a

123
00:06:08,440 --> 00:06:10,080
 color, for example.

124
00:06:10,080 --> 00:06:12,920
 You may start by focusing on a circle of white, and you

125
00:06:12,920 --> 00:06:17,840
 would just repeat to yourself, "white, white, white."

126
00:06:17,840 --> 00:06:21,060
 The mantra helps evoke a single pointed attention on the

127
00:06:21,060 --> 00:06:21,920
 color.

128
00:06:21,920 --> 00:06:25,420
 White is not anything special, but, because it is a simple

129
00:06:25,420 --> 00:06:28,950
 concept and very singular, focusing on it allows for the

130
00:06:28,950 --> 00:06:31,520
 cultivation of focused attention.

131
00:06:31,520 --> 00:06:35,090
 Repeating "white" is not likely to make you angry, greedy,

132
00:06:35,090 --> 00:06:38,880
 or deluded. It is not going to evoke bad things.

133
00:06:38,880 --> 00:06:42,200
 Such practice has the potential to help you create very

134
00:06:42,200 --> 00:06:44,400
 powerful and pure states of mind.

135
00:06:44,400 --> 00:06:48,460
 Eventually, if you practice such meditation, all you will

136
00:06:48,460 --> 00:06:51,440
 see, even when you close your eyes, is white.

137
00:06:51,440 --> 00:06:56,400
 The benefit is this very strong concentrated awareness.

138
00:06:56,400 --> 00:07:00,130
 With mindfulness meditation, the purpose of the mantra is

139
00:07:00,130 --> 00:07:03,240
 to help our minds focus on reality rather than on a concept

140
00:07:03,240 --> 00:07:04,000
.

141
00:07:04,000 --> 00:07:08,350
 We use abstraction, noting, to bring the mind back to

142
00:07:08,350 --> 00:07:09,360
 reality.

143
00:07:09,360 --> 00:07:13,360
 It is a unique form of meditation. It may share technical

144
00:07:13,360 --> 00:07:16,000
 similarities to other types of meditation,

145
00:07:16,000 --> 00:07:18,800
 but it is unique in application because rather than

146
00:07:18,800 --> 00:07:21,520
 focusing your attention on another abstraction,

147
00:07:21,520 --> 00:07:25,200
 the object of the mantra is real or experiential.

148
00:07:25,200 --> 00:07:28,450
 The word "real" can mean different things, but experiential

149
00:07:28,450 --> 00:07:30,560
 means it is a part of experience,

150
00:07:30,560 --> 00:07:35,040
 which is the essence of ultimate reality in Buddhism.

151
00:07:35,040 --> 00:07:39,580
 When you say to yourself, "seeing, seeing," it helps focus

152
00:07:39,580 --> 00:07:42,560
 your mind on the experience so you remember simply that you

153
00:07:42,560 --> 00:07:44,480
 are seeing at that moment.

154
00:07:44,480 --> 00:07:47,700
 There is this awareness and presence that is objective,

155
00:07:47,700 --> 00:07:51,260
 unlike our ordinary experience of things, which is steeped

156
00:07:51,260 --> 00:07:53,520
 in judgment and subjectivity.

157
00:07:53,520 --> 00:07:56,690
 Ordinarily, we recognize that an object has brought us

158
00:07:56,690 --> 00:08:00,120
 pleasure in the past, so we like and conceive of it as

159
00:08:00,120 --> 00:08:00,880
 positive,

160
00:08:00,880 --> 00:08:04,950
 or we associate it with a past suffering and dislike it, or

161
00:08:04,950 --> 00:08:08,690
 we associate it with some view, conceit or arrogance, and

162
00:08:08,690 --> 00:08:10,800
 delusion arises.

163
00:08:10,800 --> 00:08:13,960
 We might look in the mirror and see ourselves and think, "I

164
00:08:13,960 --> 00:08:18,160
 am so handsome," or "I am so ugly,"

165
00:08:18,160 --> 00:08:24,030
 and conceit arises, or some view of self like, "This is me.

166
00:08:24,030 --> 00:08:26,880
 This is mine. This is what I am."

167
00:08:26,880 --> 00:08:29,750
 This practice of reminding yourself of the simple nature of

168
00:08:29,750 --> 00:08:32,560
 each experience has the potential to remove all of those

169
00:08:32,560 --> 00:08:36,880
 habits of greed, anger, and delusion.

170
00:08:36,880 --> 00:08:40,240
 It evokes objective states in our minds.

171
00:08:40,240 --> 00:08:43,950
 Without mindfulness, we respond to experiences with

172
00:08:43,950 --> 00:08:48,190
 thoughts of, "This is bad. This is good. This is me. This

173
00:08:48,190 --> 00:08:49,120
 is mine."

174
00:08:49,120 --> 00:08:52,950
 But with the mindfulness practice, you say, "This is this,"

175
00:08:52,950 --> 00:08:57,930
 i.e., "seeing is seeing, hearing is hearing, thinking is

176
00:08:57,930 --> 00:08:59,120
 thinking."

177
00:08:59,120 --> 00:09:02,320
 This is explicitly what the Buddha taught.

178
00:09:02,320 --> 00:09:06,160
 What you can read about him teaching time and again.

179
00:09:06,160 --> 00:09:09,190
 We have shells of the Buddha's teaching, but this very

180
00:09:09,190 --> 00:09:13,120
 basic teaching is so simple and so ordinary that it often

181
00:09:13,120 --> 00:09:14,640
 gets overlooked.

182
00:09:14,640 --> 00:09:19,350
 What is Buddhism? Buddhism is about seeing, hearing,

183
00:09:19,350 --> 00:09:23,520
 smelling, tasting, feeling, thinking.

184
00:09:23,520 --> 00:09:27,050
 We are made up of the five aggregates, and they arise at

185
00:09:27,050 --> 00:09:30,480
 the moment of experience of any of the six senses.

186
00:09:30,480 --> 00:09:33,120
 Mindfulness is simply the practice of purifying our

187
00:09:33,120 --> 00:09:36,430
 perception of these moments of experience, so our reactions

188
00:09:36,430 --> 00:09:40,950
 are in line with reality rather than partiality or delusion

189
00:09:40,950 --> 00:09:41,760
.

190
00:09:41,760 --> 00:09:45,330
 It is important not to confuse the act of noting with the

191
00:09:45,330 --> 00:09:47,360
 states of mind that it evokes.

192
00:09:47,360 --> 00:09:51,590
 Mindfulness is a tool. It is an artificial tool used to

193
00:09:51,590 --> 00:09:55,110
 bring the mind closer to reality and become more objective

194
00:09:55,110 --> 00:09:57,920
 about the reality or the experience.

195
00:09:57,920 --> 00:10:02,350
 Mindfulness, or noting, is not the objectivity or awareness

196
00:10:02,350 --> 00:10:02,800
.

197
00:10:02,800 --> 00:10:06,150
 Noting is the tool that you must use to evoke certain mind

198
00:10:06,150 --> 00:10:06,880
 states.

199
00:10:06,880 --> 00:10:10,270
 If you are anxious and you know that anxiety is composed of

200
00:10:10,270 --> 00:10:14,100
 different physical and mental states, you can still just

201
00:10:14,100 --> 00:10:17,200
 say, "Anxious, anxious."

202
00:10:17,200 --> 00:10:20,880
 The idea is to help you experience reality with a clear

203
00:10:20,880 --> 00:10:21,520
 mind.

204
00:10:21,520 --> 00:10:25,020
 The mantra is not some kind of pill that you swallow or a

205
00:10:25,020 --> 00:10:28,160
 magical wand that you wave to change reality.

206
00:10:28,160 --> 00:10:31,070
 You do not have to apply the technique to every little

207
00:10:31,070 --> 00:10:32,400
 thing you experience.

208
00:10:32,400 --> 00:10:36,710
 With anxiety, for example, the whole experience is anxiety,

209
00:10:36,710 --> 00:10:40,480
 but some aspects of it are physical and some are mental.

210
00:10:40,480 --> 00:10:43,380
 The butterflies that you feel in your stomach, the heart

211
00:10:43,380 --> 00:10:46,540
 beating quickly, and the tension in your shoulders can all

212
00:10:46,540 --> 00:10:48,000
 be noted individually,

213
00:10:48,000 --> 00:10:51,290
 which will help you experience them without reaction or

214
00:10:51,290 --> 00:10:54,580
 judgment by simply saying to yourself when you are tense in

215
00:10:54,580 --> 00:10:57,520
 the shoulders, "Tense, tense,"

216
00:10:57,520 --> 00:11:00,100
 or when you feel there are butterflies in your stomach

217
00:11:00,100 --> 00:11:04,560
 because you are anxious, saying, "Feeling, feeling."

218
00:11:04,560 --> 00:11:08,370
 The experience does not necessarily go away, but it also

219
00:11:08,370 --> 00:11:10,560
 does not lead to more anxiety.

220
00:11:10,560 --> 00:11:14,170
 You will see that the practice of noting evokes states of

221
00:11:14,170 --> 00:11:17,760
 neutrality and objectivity that are not reactive.

222
00:11:17,760 --> 00:11:21,820
 Some states, like anxiety, grow. With anxiety, you start

223
00:11:21,820 --> 00:11:23,040
 with a thought.

224
00:11:23,040 --> 00:11:26,880
 For example, I might think, "There are 50 people now

225
00:11:26,880 --> 00:11:28,880
 listening to me talk here,"

226
00:11:28,880 --> 00:11:32,160
 and that thought may evoke certain states of anxiety in the

227
00:11:32,160 --> 00:11:35,920
 mind, which in turn create feelings in the body.

228
00:11:35,920 --> 00:11:40,160
 Those feelings in the body may then make me more anxious.

229
00:11:40,160 --> 00:11:44,380
 Then I may think, "Oh, I am anxious," and the feelings and

230
00:11:44,380 --> 00:11:47,840
 mind states bounce back and forth like this.

231
00:11:47,840 --> 00:11:51,490
 The physical states lead to more anxiety. More anxiety

232
00:11:51,490 --> 00:11:54,000
 leads to stronger physical reactions,

233
00:11:54,000 --> 00:11:57,820
 creating a feedback loop wherein anxiety becomes stronger

234
00:11:57,820 --> 00:12:02,190
 and stronger until it can even lead to a full-blown panic

235
00:12:02,190 --> 00:12:03,440
 attack.

236
00:12:03,440 --> 00:12:07,600
 In each moment of anxiety, there is a process occurring.

237
00:12:07,600 --> 00:12:11,180
 First, there is an experience. Then there is conceiving

238
00:12:11,180 --> 00:12:12,640
 about that experience.

239
00:12:12,640 --> 00:12:15,960
 And finally, there is the reaction based on one's

240
00:12:15,960 --> 00:12:18,320
 conceptions of the experience.

241
00:12:18,320 --> 00:12:21,450
 This process can be broken by simply changing one's

242
00:12:21,450 --> 00:12:23,520
 conception of the experience.

243
00:12:23,520 --> 00:12:27,190
 Rather than conceiving of the experience as, "I am anxious,

244
00:12:27,190 --> 00:12:28,480
 this is a problem,"

245
00:12:28,480 --> 00:12:32,260
 you change the conception to just, "This is anxiety," or

246
00:12:32,260 --> 00:12:34,640
 the physical, "This is feeling,"

247
00:12:34,640 --> 00:12:38,460
 by using mantras like "anxious" or "feeling" to remind

248
00:12:38,460 --> 00:12:41,840
 yourself of the objective nature of the experience.

249
00:12:41,840 --> 00:12:45,280
 An act which evokes very different states of mind from

250
00:12:45,280 --> 00:12:46,960
 ordinary conception.

251
00:12:46,960 --> 00:12:50,450
 The mantra is not magic. It is not in and of itself

252
00:12:50,450 --> 00:12:51,520
 mindfulness.

253
00:12:51,520 --> 00:12:55,290
 But if used consistently and with proper attention, it can

254
00:12:55,290 --> 00:12:57,680
 evoke states of objectivity and mindfulness

255
00:12:57,680 --> 00:13:01,450
 that break away from the conceptual process that leads to

256
00:13:01,450 --> 00:13:04,000
 building up of things like anxiety.

257
00:13:04,000 --> 00:13:07,760
 Conceptualization can be useful, and the mantra is a useful

258
00:13:07,760 --> 00:13:08,720
 conceptualization

259
00:13:08,720 --> 00:13:11,930
 that helps us direct our attention to the object of our

260
00:13:11,930 --> 00:13:12,960
 experience.

261
00:13:12,960 --> 00:13:16,080
 In other types of meditation, a mantra is used to direct

262
00:13:16,080 --> 00:13:19,440
 one's attention to concepts like white.

263
00:13:19,440 --> 00:13:23,570
 In mindfulness, the object is not concepts, but experient

264
00:13:23,570 --> 00:13:24,720
ial reality.

265
00:13:24,720 --> 00:13:27,760
 This is an important difference because reality has certain

266
00:13:27,760 --> 00:13:30,480
 qualities that are not present in concepts.

267
00:13:30,480 --> 00:13:34,560
 Firstly, impermanence. Whereas we perceive conceptual

268
00:13:34,560 --> 00:13:37,360
 objects to be stable in our imagination,

269
00:13:37,360 --> 00:13:41,600
 experiences arise and cease from moment to moment.

270
00:13:41,600 --> 00:13:45,510
 Because concepts rely on experiences to arise, our lack of

271
00:13:45,510 --> 00:13:48,160
 familiarity with the impermanence of reality

272
00:13:48,160 --> 00:13:51,360
 leads to disappointment when the conceptual things we come

273
00:13:51,360 --> 00:13:55,120
 to expect cannot be produced on a whim.

274
00:13:55,120 --> 00:13:58,850
 Through observing experiences directly, we become familiar

275
00:13:58,850 --> 00:14:00,960
 with the chaotic nature of reality,

276
00:14:00,960 --> 00:14:04,720
 and the whole idea of expectation becomes absurd.

277
00:14:04,720 --> 00:14:07,560
 We cannot know what the future will bring, and forming

278
00:14:07,560 --> 00:14:10,720
 expectations in our minds becomes a habit

279
00:14:10,720 --> 00:14:14,240
 that can only lead to eventual disappointment.

280
00:14:14,240 --> 00:14:18,090
 There is no benefit to expecting. What does it mean to say,

281
00:14:18,090 --> 00:14:20,320
 "I want things to be this way"?

282
00:14:20,320 --> 00:14:24,200
 All it means is you are cultivating a habit of desire. It

283
00:14:24,200 --> 00:14:29,120
 does not have any bearing on how things are going to be.

284
00:14:29,120 --> 00:14:33,420
 Secondly, mindfulness of experiences provides understanding

285
00:14:33,420 --> 00:14:34,560
 of suffering.

286
00:14:34,560 --> 00:14:38,340
 When you try to fix or control your conceptual reality, you

287
00:14:38,340 --> 00:14:41,840
 become stressed and are susceptible to disappointment.

288
00:14:41,840 --> 00:14:45,680
 Even if you are able occasionally to get whatever you want,

289
00:14:45,680 --> 00:14:48,000
 you then just want it more.

290
00:14:48,000 --> 00:14:51,050
 By keeping the mind present and engaged with your actual

291
00:14:51,050 --> 00:14:54,570
 experiences, you see this stress and attachment for what it

292
00:14:54,570 --> 00:14:54,720
 is,

293
00:14:54,720 --> 00:14:58,670
 realizing that reality admits of no satisfaction for those

294
00:14:58,670 --> 00:15:01,670
 who cling to concepts or experiences as they are

295
00:15:01,670 --> 00:15:05,040
 unpredictable and uncontrollable.

296
00:15:05,040 --> 00:15:10,140
 Thirdly, mindfulness shows you non-self, that the

297
00:15:10,140 --> 00:15:12,800
 conceptual entities we evoke in our minds are merely

298
00:15:12,800 --> 00:15:17,200
 illusions formed of perception, recognition, and memory.

299
00:15:17,200 --> 00:15:20,570
 Through mindful observation of experiences, you see that

300
00:15:20,570 --> 00:15:23,360
 momentary experiences underlie all things.

301
00:15:23,360 --> 00:15:26,620
 They have no substance beyond our imagination and the

302
00:15:26,620 --> 00:15:28,640
 experiences that trigger it.

303
00:15:28,640 --> 00:15:32,060
 As a result of this clarity of vision, we no longer obsess

304
00:15:32,060 --> 00:15:36,190
 about controlling the world around us or cling to things as

305
00:15:36,190 --> 00:15:37,680
 "me" or "mine."

306
00:15:37,680 --> 00:15:41,100
 We see that this clinging to illusions leads only to

307
00:15:41,100 --> 00:15:42,080
 suffering.

308
00:15:42,080 --> 00:15:45,270
 Our lack of familiarity with these fundamental aspects of

309
00:15:45,270 --> 00:15:49,140
 nature leads us to conceive of experiences as entities and

310
00:15:49,140 --> 00:15:52,660
 those entities as bringing us pleasure or pain, which

311
00:15:52,660 --> 00:15:56,880
 causes us to react positively or negatively towards them.

312
00:15:56,880 --> 00:16:00,640
 Through mindfulness, we start to see experiences just as

313
00:16:00,640 --> 00:16:01,760
 experiences.

314
00:16:01,760 --> 00:16:04,980
 Noting is simply a tool to help evoke a state of

315
00:16:04,980 --> 00:16:06,240
 mindfulness.

316
00:16:06,240 --> 00:16:10,120
 When practicing noting meditation, it is important not to

317
00:16:10,120 --> 00:16:13,660
 worry too much about the details, like whether you are

318
00:16:13,660 --> 00:16:17,000
 noting everything or noting the correct thing or noting

319
00:16:17,000 --> 00:16:18,560
 something correctly.

320
00:16:18,560 --> 00:16:21,730
 You do not have to note everything. You do not have to

321
00:16:21,730 --> 00:16:24,240
 worry about being perfectly correct.

322
00:16:24,240 --> 00:16:28,530
 We note sitting. Sitting, for example, even though it is

323
00:16:28,530 --> 00:16:31,120
 not the concept of sitting we are focused on,

324
00:16:31,120 --> 00:16:33,820
 the noting of sitting focuses your attention on the

325
00:16:33,820 --> 00:16:37,100
 experience of sitting, the pressure on your bottom, the

326
00:16:37,100 --> 00:16:40,320
 tension in your back and in your legs, and so on, which is

327
00:16:40,320 --> 00:16:41,680
 absolutely real.

328
00:16:41,680 --> 00:16:45,430
 Saying to yourself, "Sitting, sitting," evokes this

329
00:16:45,430 --> 00:16:47,440
 objective awareness of the experience.

330
00:16:47,440 --> 00:16:52,210
 Saying, "Sitting," is not saying, "This is good," or "This

331
00:16:52,210 --> 00:16:53,120
 is bad."

332
00:16:53,120 --> 00:16:55,900
 It is an objective thing to say so, even though it is

333
00:16:55,900 --> 00:16:58,960
 conceptual, and the sitting itself is conceptual.

334
00:16:58,960 --> 00:17:02,390
 The mantra helps you focus on reality. Anyone who is

335
00:17:02,390 --> 00:17:05,840
 critical of this should read what the Buddha himself said.

336
00:17:05,840 --> 00:17:10,320
 When sitting, know clearly to yourself, "I am sitting."

337
00:17:10,320 --> 00:17:13,740
 Most important is that mantra meditation or noting

338
00:17:13,740 --> 00:17:18,140
 meditation, or whatever you call it, is a concrete, easily

339
00:17:18,140 --> 00:17:21,920
 accessible practice that almost anyone can undertake.

340
00:17:21,920 --> 00:17:25,500
 Right here and now, you can use it to become objectively

341
00:17:25,500 --> 00:17:28,800
 aware of your own experiences as they truly are.

342
00:17:28,800 --> 00:17:31,850
 Noting is a very useful tool, but do not let the tool

343
00:17:31,850 --> 00:17:35,810
 become the object. Do not focus on the noting, thinking, "

344
00:17:35,810 --> 00:17:40,390
Is it working? Why is it not working? Why do I say pain,

345
00:17:40,390 --> 00:17:44,830
 pain, and the pain does not go away? I expect results. Why

346
00:17:44,830 --> 00:17:46,720
 am I not getting them?"

347
00:17:46,720 --> 00:17:50,210
 You are not getting good results because of expectation.

348
00:17:50,210 --> 00:17:51,680
 The mantra is just a tool.

349
00:17:51,680 --> 00:17:54,980
 The consequent mindfulness is what does the work to free us

350
00:17:54,980 --> 00:17:56,160
 from suffering.

