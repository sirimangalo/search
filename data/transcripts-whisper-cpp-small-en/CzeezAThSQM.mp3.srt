1
00:00:00,000 --> 00:00:07,000
 Good evening, everyone.

2
00:00:07,000 --> 00:00:13,000
 I'm broadcasting live.

3
00:00:13,000 --> 00:00:19,000
 March 20th.

4
00:00:19,000 --> 00:00:31,000
 I'm going to cut my meditation short this evening.

5
00:00:31,000 --> 00:00:35,000
 It's been a long day, lots of different things.

6
00:00:35,000 --> 00:00:37,000
 I'm caught up in the...

7
00:00:37,000 --> 00:00:45,000
 There goes my legs.

8
00:00:45,000 --> 00:01:01,000
 So, yeah, I was doing homework.

9
00:01:01,000 --> 00:01:13,000
 Anyway, here on time.

10
00:01:13,000 --> 00:01:22,000
 So today's quote is quite an interesting one.

11
00:01:22,000 --> 00:01:45,000
 Let's see if we can get 61.

12
00:01:45,000 --> 00:02:11,000
 71. Oh, yeah, okay.

13
00:02:11,000 --> 00:02:18,150
 This switch is called "Gitta" sometimes, "Mano" sometimes,

14
00:02:18,150 --> 00:02:24,000
 "Suginana" sometimes.

15
00:02:24,000 --> 00:02:36,000
 There's many names for "mind".

16
00:02:36,000 --> 00:02:41,000
 "Gritiya chaddiwa sasa chaa".

17
00:02:41,000 --> 00:02:47,000
 That by day and by night.

18
00:02:47,000 --> 00:03:00,000
 "Anyadeva upachati, anyanyurjati".

19
00:03:00,000 --> 00:03:08,000
 "Anyadeva upachati" arises as one thing.

20
00:03:08,000 --> 00:03:15,000
 "Anyanyurjati" ceases as another.

21
00:03:15,000 --> 00:03:23,000
 One thing, one mind arises when another ceases.

22
00:03:23,000 --> 00:03:32,910
 "Seyata bhikkavai makkato aranyi pavanai charamanu sakangan

23
00:03:32,910 --> 00:03:34,000
ati".

24
00:03:34,000 --> 00:03:40,000
 When a monkey grabs a branch, let's go of another branch.

25
00:03:40,000 --> 00:03:45,000
 "Tangmuchitva anyanganati".

26
00:03:45,000 --> 00:03:47,000
 Then he goes, "That one thing grabs another one".

27
00:03:47,000 --> 00:03:52,000
 "Eyvamevako bhikkavai" in the same way monks.

28
00:03:52,000 --> 00:04:00,000
 "Yangidanguchititangitipi manoitipi vinyanangitipi".

29
00:04:00,000 --> 00:04:05,000
 That which is called "Gitta", that which is called "Manat",

30
00:04:05,000 --> 00:04:09,000
 that which is called "Vinyana".

31
00:04:09,000 --> 00:04:12,000
 "Tangratiya chaddiwa sasa chaa".

32
00:04:12,000 --> 00:04:16,000
 This by day or by night, and by night.

33
00:04:16,000 --> 00:04:25,000
 "Anyadi, Anyadi, Vaupatthi, Anyangyurjati" arises one,

34
00:04:25,000 --> 00:04:31,000
 ceases another.

35
00:04:31,000 --> 00:04:42,230
 It's a simple philosophy and claim, but it has deep meaning

36
00:04:42,230 --> 00:04:43,000
.

37
00:04:43,000 --> 00:04:55,030
 You see, so science and ordinary people have this idea of a

38
00:04:55,030 --> 00:04:56,000
 substratum,

39
00:04:56,000 --> 00:05:00,000
 most religions as well.

40
00:05:00,000 --> 00:05:06,400
 So science is quite convinced that there is a substratum of

41
00:05:06,400 --> 00:05:08,000
 reality.

42
00:05:08,000 --> 00:05:11,780
 Religion is usually quite convinced that there is a substr

43
00:05:11,780 --> 00:05:13,000
atum of mentality,

44
00:05:13,000 --> 00:05:23,000
 the soul, the self, God.

45
00:05:23,000 --> 00:05:32,000
 And so there is a lot of postulation of various substrata.

46
00:05:32,000 --> 00:05:40,150
 And so science cultivates substrata that tend to accord

47
00:05:40,150 --> 00:05:42,000
 with reality,

48
00:05:42,000 --> 00:05:51,000
 religion often not, but religion also does a better job

49
00:05:51,000 --> 00:06:02,000
 according with mental reality sometimes than science.

50
00:06:02,000 --> 00:06:08,000
 Nonetheless, the tendency is to postulate substrata.

51
00:06:08,000 --> 00:06:14,000
 Substrata means like a framework, like a mind, a body,

52
00:06:14,000 --> 00:06:22,170
 a three-dimensional, four-dimensional reality that we live

53
00:06:22,170 --> 00:06:23,000
 in.

54
00:06:23,000 --> 00:06:28,000
 Buddhism doesn't do that.

55
00:06:28,000 --> 00:06:32,720
 Buddhism has a very simple take on the universe that this

56
00:06:32,720 --> 00:06:35,000
 moment is real,

57
00:06:35,000 --> 00:06:40,940
 this moment is an experience, and then it ceases, and then

58
00:06:40,940 --> 00:06:43,000
 this moment is real.

59
00:06:43,000 --> 00:06:49,170
 And that moment, so when we talk about mind in Theravada

60
00:06:49,170 --> 00:06:50,000
 Buddhism,

61
00:06:50,000 --> 00:06:56,170
 we mean that moment of mind, one moment of mind, one moment

62
00:06:56,170 --> 00:06:58,000
 of experience,

63
00:06:58,000 --> 00:07:05,000
 one moment of awareness or contemplation of an object,

64
00:07:05,000 --> 00:07:15,000
 observation of an object, one moment of experience.

65
00:07:15,000 --> 00:07:18,000
 And so if you ask the question, who's right?

66
00:07:18,000 --> 00:07:22,000
 It's not really the appropriate question.

67
00:07:22,000 --> 00:07:26,700
 Scientists tend to scoff at Buddhism as being overly simple

68
00:07:26,700 --> 00:07:28,000
, simplistic,

69
00:07:28,000 --> 00:07:33,000
 and they'll say, okay, well, maybe you're right,

70
00:07:33,000 --> 00:07:37,000
 but you haven't said anything about reality, you know?

71
00:07:37,000 --> 00:07:45,490
 You haven't figured out subatomic particles or how to build

72
00:07:45,490 --> 00:07:48,000
 an atom bomb,

73
00:07:48,000 --> 00:07:54,000
 nor have you explored the vast reaches of the universe,

74
00:07:54,000 --> 00:08:00,000
 the solar system and the galaxy, the known universe.

75
00:08:00,000 --> 00:08:05,000
 You haven't really explored reality.

76
00:08:05,000 --> 00:08:14,000
 We can say, no, no, we haven't.

77
00:08:14,000 --> 00:08:17,000
 So the question is to what end?

78
00:08:17,000 --> 00:08:22,000
 The question of all of this is to what end?

79
00:08:22,000 --> 00:08:27,000
 Religion makes all sorts of claims about reality,

80
00:08:27,000 --> 00:08:32,000
 and we can ask what is the result.

81
00:08:32,000 --> 00:08:39,000
 I mean, if it were useful and positive to delude yourself

82
00:08:39,000 --> 00:08:46,880
 and make claims about reality that were not in accord with

83
00:08:46,880 --> 00:08:48,000
 reality,

84
00:08:48,000 --> 00:08:51,000
 that had no evidence, that were not based on evidence,

85
00:08:51,000 --> 00:08:59,330
 or somehow beneficial, then there wouldn't be much of a

86
00:08:59,330 --> 00:09:00,000
 problem.

87
00:09:00,000 --> 00:09:02,000
 Most religions would do fine,

88
00:09:02,000 --> 00:09:07,020
 but because they tend to not relate to actual experience

89
00:09:07,020 --> 00:09:09,000
 and reality,

90
00:09:09,000 --> 00:09:14,000
 they tend to rely more on belief.

91
00:09:14,000 --> 00:09:22,000
 They tend to be quite artificial and stilted or forced.

92
00:09:22,000 --> 00:09:26,550
 I mean, if you step back or say you step out of a

93
00:09:26,550 --> 00:09:28,000
 meditation course

94
00:09:28,000 --> 00:09:33,380
 where you've been studying your own mind and what's really

95
00:09:33,380 --> 00:09:35,000
 here and now,

96
00:09:35,000 --> 00:09:37,000
 and then you contemplate one of these religions,

97
00:09:37,000 --> 00:09:40,000
 pick any religion at random,

98
00:09:40,000 --> 00:09:46,930
 contemplate what they're actually saying, it's shocking how

99
00:09:46,930 --> 00:09:51,000
 absurd it all is.

100
00:09:51,000 --> 00:09:53,000
 What are you talking about?

101
00:09:53,000 --> 00:09:58,000
 What is this garbage?

102
00:09:58,000 --> 00:10:05,000
 Really, I mean, so much belief.

103
00:10:05,000 --> 00:10:09,000
 There was this guy, he said this.

104
00:10:09,000 --> 00:10:16,000
 He claimed this, he did this, and he will save us.

105
00:10:16,000 --> 00:10:20,000
 Or there's this God up there,

106
00:10:20,000 --> 00:10:23,630
 and he'll have to do his pray or repeat his name or chant

107
00:10:23,630 --> 00:10:26,000
 this or chant that,

108
00:10:26,000 --> 00:10:32,000
 and he will save you.

109
00:10:32,000 --> 00:10:35,000
 So they run into lots of problems,

110
00:10:35,000 --> 00:10:38,520
 and wise people tend to stay away from most of the

111
00:10:38,520 --> 00:10:40,000
 religions out there

112
00:10:40,000 --> 00:10:47,300
 because they tend to be not very much, not very closely

113
00:10:47,300 --> 00:10:49,000
 aligned to reality.

114
00:10:49,000 --> 00:10:54,000
 But science, science does a good job focusing on reality,

115
00:10:54,000 --> 00:10:56,000
 aligning itself with experience.

116
00:10:56,000 --> 00:11:01,000
 They are able to run experiments that actually accord with

117
00:11:01,000 --> 00:11:01,000
 the way things are,

118
00:11:01,000 --> 00:11:05,000
 but they've got a different problem.

119
00:11:05,000 --> 00:11:08,000
 And we can argue about what is more real,

120
00:11:08,000 --> 00:11:14,000
 the substratum of physical and even maybe mental reality,

121
00:11:14,000 --> 00:11:19,000
 let's say physical reality, or experience.

122
00:11:19,000 --> 00:11:21,000
 Which one is more real?

123
00:11:21,000 --> 00:11:25,000
 We can argue about that, but it's not an important argument

124
00:11:25,000 --> 00:11:25,000
 again.

125
00:11:25,000 --> 00:11:29,000
 To what end?

126
00:11:29,000 --> 00:11:31,000
 What has science given us?

127
00:11:31,000 --> 00:11:33,000
 What has it brought us?

128
00:11:33,000 --> 00:11:37,530
 It's brought us all sorts of magical, wonderful things like

129
00:11:37,530 --> 00:11:39,000
 the internet,

130
00:11:39,000 --> 00:11:43,250
 the fact that I'm able to talk to people all around the

131
00:11:43,250 --> 00:11:44,000
 world,

132
00:11:44,000 --> 00:11:46,000
 it's kind of magical.

133
00:11:46,000 --> 00:11:49,900
 It's brought us all this, very powerful, this kind of

134
00:11:49,900 --> 00:11:51,000
 knowledge.

135
00:11:51,000 --> 00:11:53,000
 But has it brought us happiness?

136
00:11:53,000 --> 00:11:54,000
 Has it brought us peace?

137
00:11:54,000 --> 00:12:00,000
 Has it brought us understanding?

138
00:12:00,000 --> 00:12:03,300
 And I mean a specific, I'm thinking of a specific type of

139
00:12:03,300 --> 00:12:06,000
 understanding, really.

140
00:12:06,000 --> 00:12:10,000
 Has it brought us to understanding of ourselves?

141
00:12:10,000 --> 00:12:12,000
 No.

142
00:12:12,000 --> 00:12:16,660
 No, people who study science get angry, become addicted to

143
00:12:16,660 --> 00:12:17,000
 things,

144
00:12:17,000 --> 00:12:21,000
 they have a hard time dealing with ordinary experiences,

145
00:12:21,000 --> 00:12:26,000
 and so they suffer.

146
00:12:26,000 --> 00:12:30,000
 They stress, they kill themselves,

147
00:12:30,000 --> 00:12:38,000
 they drown their, soak their brains in alcohol or drugs,

148
00:12:38,000 --> 00:12:42,000
 because they can't deal with ordinary reality, right?

149
00:12:42,000 --> 00:12:52,000
 They study reality so much, but can't deal with it.

150
00:12:52,000 --> 00:12:56,000
 And then you have this Buddhist, I'd say,

151
00:12:56,000 --> 00:13:01,270
 a meditator who focuses on their reality, who looks at

152
00:13:01,270 --> 00:13:05,000
 their experience.

153
00:13:05,000 --> 00:13:08,000
 When they have emotions, they see them as emotions,

154
00:13:08,000 --> 00:13:12,880
 when they have pain, they see it as pain, and experiences

155
00:13:12,880 --> 00:13:15,000
 of seeing or hearing,

156
00:13:15,000 --> 00:13:17,000
 they see them for what they are.

157
00:13:17,000 --> 00:13:23,200
 They put aside any theories or thoughts about a substratum,

158
00:13:23,200 --> 00:13:24,000
 about reality.

159
00:13:24,000 --> 00:13:26,000
 They go, "Here, I'm in this room."

160
00:13:26,000 --> 00:13:30,000
 They even lose track of being in a room.

161
00:13:30,000 --> 00:13:33,990
 They just see the mind arising and they see the mind arises

162
00:13:33,990 --> 00:13:34,000
,

163
00:13:34,000 --> 00:13:38,650
 is aware of something, and ceases, and immediately there's

164
00:13:38,650 --> 00:13:39,000
 another mind,

165
00:13:39,000 --> 00:13:43,000
 but it's totally different.

166
00:13:43,000 --> 00:13:48,610
 It's a whole new mind, a whole new experience, and they see

167
00:13:48,610 --> 00:13:51,000
 this, too.

168
00:13:51,000 --> 00:13:56,110
 They're able to see a reality or a way of looking at

169
00:13:56,110 --> 00:14:02,000
 reality for themselves,

170
00:14:02,000 --> 00:14:07,000
 and that leads to objectivity.

171
00:14:07,000 --> 00:14:11,380
 They're able to fully comprehend reality, I guess you could

172
00:14:11,380 --> 00:14:12,000
 say.

173
00:14:12,000 --> 00:14:14,970
 Because science, you can study it, but you can't fully

174
00:14:14,970 --> 00:14:16,000
 comprehend it.

175
00:14:16,000 --> 00:14:20,230
 You can't look at the wall and comprehend that that wall is

176
00:14:20,230 --> 00:14:23,000
 made up of mostly empty space.

177
00:14:23,000 --> 00:14:29,710
 To give an example, moreover, you're not able to comprehend

178
00:14:29,710 --> 00:14:32,000
 all those things.

179
00:14:32,000 --> 00:14:34,610
 I guess I would say it's not even worth it, it wouldn't

180
00:14:34,610 --> 00:14:36,000
 even be worth it if we could.

181
00:14:36,000 --> 00:14:41,000
 In fact, our comprehension of things is part of the problem

182
00:14:41,000 --> 00:14:41,000
.

183
00:14:41,000 --> 00:14:44,680
 Because when you think about concepts, when you think about

184
00:14:44,680 --> 00:14:45,000
 entities,

185
00:14:45,000 --> 00:14:47,000
 you have to abstract something.

186
00:14:47,000 --> 00:14:51,000
 You have this mouse.

187
00:14:51,000 --> 00:14:54,000
 What happens when I think of this mouse?

188
00:14:54,000 --> 00:14:58,000
 There's something going on in my mind.

189
00:14:58,000 --> 00:15:02,320
 There's an experience, and there's an experience which says

190
00:15:02,320 --> 00:15:04,000
 there is a mouse in your hand,

191
00:15:04,000 --> 00:15:08,570
 but it has nothing to do directly with the experience of

192
00:15:08,570 --> 00:15:10,000
 seeing the mouse.

193
00:15:10,000 --> 00:15:15,000
 I think the seeing and the feeling that created it.

194
00:15:15,000 --> 00:15:17,000
 It's all in the mind at that point.

195
00:15:17,000 --> 00:15:22,000
 There are minds arising and leading therefore, therefore,

196
00:15:22,000 --> 00:15:27,000
 therefore one to the other.

197
00:15:27,000 --> 00:15:29,000
 And I'm not aware of that.

198
00:15:29,000 --> 00:15:33,000
 So there's a reality that is being ignored.

199
00:15:33,000 --> 00:15:36,000
 Because instead, I'm focused on the idea of the mouse.

200
00:15:36,000 --> 00:15:41,000
 So there's something that I'm missing.

201
00:15:41,000 --> 00:15:44,270
 And as a result, when likes and dislikes come up, I miss

202
00:15:44,270 --> 00:15:45,000
 them as well.

203
00:15:45,000 --> 00:15:47,000
 So I like the mouse, or I dislike it.

204
00:15:47,000 --> 00:15:53,280
 And that's what leads to frustration and addiction and so

205
00:15:53,280 --> 00:15:54,000
 on.

206
00:15:54,000 --> 00:15:58,610
 So it leads us to attach to things and people and

207
00:15:58,610 --> 00:15:59,000
 experiences,

208
00:15:59,000 --> 00:16:05,900
 and leads us to get frustrated and upset and bored and

209
00:16:05,900 --> 00:16:06,000
 disappointed

210
00:16:06,000 --> 00:16:12,000
 when we don't get what we want.

211
00:16:12,000 --> 00:16:15,000
 So when we pay attention to that, as I'm picking up the

212
00:16:15,000 --> 00:16:15,000
 mouse,

213
00:16:15,000 --> 00:16:19,500
 I pay attention to the experience and then the thinking and

214
00:16:19,500 --> 00:16:21,000
 the judging.

215
00:16:21,000 --> 00:16:27,750
 And I am in a position to understand and to see the

216
00:16:27,750 --> 00:16:32,000
 experience clearly

217
00:16:32,000 --> 00:16:38,000
 and to not react or to understand my reactions

218
00:16:38,000 --> 00:16:44,250
 and to see how they're hurting me and to slowly give them

219
00:16:44,250 --> 00:16:45,000
 up.

220
00:16:45,000 --> 00:16:53,750
 This is an important quote to help us understand where our

221
00:16:53,750 --> 00:16:55,000
 focus should be.

222
00:16:55,000 --> 00:16:59,000
 Our focus should be on experiential reality.

223
00:16:59,000 --> 00:17:03,000
 This is what the Buddha talked about.

224
00:17:03,000 --> 00:17:04,000
 So here, what do we have?

225
00:17:04,000 --> 00:17:06,000
 This whole sutta is interesting.

226
00:17:06,000 --> 00:17:12,000
 The uninstructed world thing, the putrujana, right?

227
00:17:12,000 --> 00:17:23,000
 A sutva bhikkave putrujana, an uninstructed putrujana.

228
00:17:23,000 --> 00:17:26,000
 Might experience revulsion towards this body.

229
00:17:26,000 --> 00:17:31,000
 He might become dispassionate towards it and be liberated,

230
00:17:31,000 --> 00:17:33,000
 or he might even be liberated.

231
00:17:33,000 --> 00:17:36,000
 Because growth and decline is seen in this body.

232
00:17:36,000 --> 00:17:39,000
 It is seen and taken up.

233
00:17:39,000 --> 00:17:50,000
 Okay, he's saying might be liberated from the body.

234
00:17:50,000 --> 00:17:54,210
 The body is revolt, it's easier to see it's revolting as

235
00:17:54,210 --> 00:17:55,000
 you get old

236
00:17:55,000 --> 00:17:57,000
 and as you get sick and so on.

237
00:17:57,000 --> 00:18:01,000
 It's possible for ordinary people to let go of it.

238
00:18:01,000 --> 00:18:06,000
 But the mind, the mind we are unable to let go of.

239
00:18:06,000 --> 00:18:10,610
 He says the ordinary uninstructed world thing is unable to

240
00:18:10,610 --> 00:18:11,000
 experience

241
00:18:11,000 --> 00:18:14,000
 revulsion towards it, unable to become dispassionate

242
00:18:14,000 --> 00:18:14,000
 towards it

243
00:18:14,000 --> 00:18:17,000
 and be liberated from it.

244
00:18:17,000 --> 00:18:20,000
 Because for a long time this has been held by him

245
00:18:20,000 --> 00:18:21,000
 appropriate and grasped us.

246
00:18:21,000 --> 00:18:28,000
 This is me, mine, this I am, this is my self.

247
00:18:28,000 --> 00:18:31,000
 This is my mind, this I am.

248
00:18:31,000 --> 00:18:34,000
 This is what we think of the mind, right?

249
00:18:34,000 --> 00:18:38,000
 Who doesn't think the mind is me and mine?

250
00:18:38,000 --> 00:18:41,000
 The body we can let go of.

251
00:18:41,000 --> 00:18:44,000
 This is my mind, this is my mind.

252
00:18:44,000 --> 00:18:48,000
 This is my mind, this is my mind.

253
00:18:48,000 --> 00:18:51,000
 This is what we think of the mind, right?

254
00:18:51,000 --> 00:18:55,000
 Who doesn't think the mind is me and mine?

255
00:18:55,000 --> 00:18:59,000
 The body we can let go of.

256
00:18:59,000 --> 00:19:05,390
 When the mind is pleased or displeased, that's me, that's I

257
00:19:05,390 --> 00:19:06,000
.

258
00:19:06,000 --> 00:19:07,380
 [Singing in

259
00:19:07,380 --> 00:19:08,480
 in

260
00:19:08,480 --> 00:19:10,480
 in

261
00:19:10,480 --> 00:19:12,480
 in

262
00:19:12,480 --> 00:19:14,480
 in

263
00:19:14,480 --> 00:19:16,480
 in

264
00:19:16,480 --> 00:19:18,480
 in

265
00:19:18,480 --> 00:19:20,480
 in

266
00:19:20,480 --> 00:19:22,480
 in

267
00:19:22,480 --> 00:19:24,480
 in

268
00:19:24,480 --> 00:19:26,480
 in

269
00:19:26,480 --> 00:19:28,480
 in

270
00:19:28,480 --> 00:19:30,480
 in

271
00:19:30,480 --> 00:19:32,480
 in

272
00:19:32,480 --> 00:19:34,480
 in

273
00:19:34,480 --> 00:19:36,480
 in

274
00:19:36,480 --> 00:19:38,480
 in

275
00:19:38,480 --> 00:19:40,480
 in

276
00:19:40,480 --> 00:19:42,480
 in

277
00:19:42,480 --> 00:19:44,480
 in

278
00:19:44,480 --> 00:19:46,480
 in

279
00:19:46,480 --> 00:19:48,480
 in

280
00:19:48,480 --> 00:19:50,480
 in

281
00:19:50,480 --> 00:19:52,480
 in

282
00:19:52,480 --> 00:19:54,480
 in

283
00:19:54,480 --> 00:19:56,480
 in

284
00:19:56,480 --> 00:19:58,480
 in

285
00:19:58,480 --> 00:20:00,480
 in

286
00:20:00,480 --> 00:20:02,480
 in

287
00:20:02,480 --> 00:20:04,480
 in

288
00:20:04,480 --> 00:20:06,480
 in

289
00:20:06,480 --> 00:20:08,480
 in

290
00:20:08,480 --> 00:20:10,480
 in

291
00:20:10,480 --> 00:20:12,480
 in

292
00:20:12,480 --> 00:20:14,480
 in

293
00:20:14,480 --> 00:20:16,480
 in

294
00:20:16,480 --> 00:20:18,480
 in

295
00:20:18,480 --> 00:20:20,480
 in

296
00:20:20,480 --> 00:20:22,480
 in

297
00:20:22,480 --> 00:20:24,480
 in

298
00:20:24,480 --> 00:20:26,480
 in

299
00:20:26,480 --> 00:20:28,480
 in

300
00:20:28,480 --> 00:20:30,480
 in

301
00:20:30,480 --> 00:20:32,480
 in

302
00:20:32,480 --> 00:20:34,480
 in

303
00:20:34,480 --> 00:20:36,480
 in

304
00:20:36,480 --> 00:20:38,480
 in

305
00:20:38,480 --> 00:20:40,480
 in

306
00:20:40,480 --> 00:20:42,480
 in

307
00:20:42,480 --> 00:20:44,480
 in

308
00:20:44,480 --> 00:20:46,480
 in

309
00:20:46,480 --> 00:20:48,480
 in

310
00:20:48,480 --> 00:20:50,480
 in

311
00:20:50,480 --> 00:20:52,480
 in

312
00:20:52,480 --> 00:20:54,480
 in

313
00:20:54,480 --> 00:20:56,480
 in

314
00:20:56,480 --> 00:20:58,480
 in

315
00:20:58,480 --> 00:21:00,480
 in

316
00:21:00,480 --> 00:21:02,480
 in

317
00:21:02,480 --> 00:21:04,480
 in

318
00:21:04,480 --> 00:21:06,480
 in

319
00:21:06,480 --> 00:21:08,480
 in

320
00:21:08,480 --> 00:21:10,480
 in

321
00:21:10,480 --> 00:21:12,480
 in

322
00:21:12,480 --> 00:21:14,480
 in

323
00:21:14,480 --> 00:21:16,480
 in

324
00:21:16,480 --> 00:21:18,480
 in

325
00:21:18,480 --> 00:21:20,480
 in

326
00:21:20,480 --> 00:21:22,480
 in

327
00:21:22,480 --> 00:21:24,480
 in

328
00:21:24,480 --> 00:21:26,480
 in

329
00:21:26,480 --> 00:21:28,480
 in

330
00:21:28,480 --> 00:21:30,480
 in

331
00:21:30,480 --> 00:21:32,480
 in

332
00:21:32,480 --> 00:21:34,480
 in

333
00:21:34,480 --> 00:21:36,480
 in

334
00:21:36,480 --> 00:21:38,480
 in

335
00:21:38,480 --> 00:21:40,480
 in

336
00:21:40,480 --> 00:21:42,480
 in

337
00:21:42,480 --> 00:21:44,480
 in

338
00:21:44,480 --> 00:21:46,480
 in

339
00:21:46,480 --> 00:21:48,480
 in

340
00:21:48,480 --> 00:21:50,480
 in

341
00:21:50,480 --> 00:21:52,480
 in

342
00:21:52,480 --> 00:21:54,480
 in

343
00:21:54,480 --> 00:21:56,480
 in

344
00:21:56,480 --> 00:21:58,480
 in

345
00:21:58,480 --> 00:22:00,480
 in

346
00:22:00,480 --> 00:22:02,480
 in

347
00:22:02,480 --> 00:22:04,480
 in

348
00:22:04,480 --> 00:22:06,480
 in

349
00:22:06,480 --> 00:22:08,480
 in

350
00:22:08,480 --> 00:22:10,480
 in

351
00:22:10,480 --> 00:22:12,480
 in

352
00:22:12,480 --> 00:22:14,480
 in

353
00:22:14,480 --> 00:22:16,480
 in

354
00:22:16,480 --> 00:22:18,480
 in

355
00:22:18,480 --> 00:22:20,480
 in

356
00:22:20,480 --> 00:22:22,480
 in

357
00:22:24,480 --> 00:22:26,480
 in

358
00:22:26,480 --> 00:22:28,480
 in

359
00:22:28,480 --> 00:22:30,480
 in

360
00:22:30,480 --> 00:22:32,480
 in

361
00:22:32,480 --> 00:22:34,480
 in

362
00:22:34,480 --> 00:22:36,480
 in

363
00:22:36,480 --> 00:22:38,480
 in

364
00:22:38,480 --> 00:22:40,480
 in

365
00:22:40,480 --> 00:22:42,480
 in

366
00:22:42,480 --> 00:22:44,480
 in

367
00:22:44,480 --> 00:22:46,480
 in

368
00:22:46,480 --> 00:22:48,480
 in

369
00:22:48,480 --> 00:22:50,480
 in

370
00:22:50,480 --> 00:22:52,480
 in in

371
00:22:52,480 --> 00:22:54,480
 in

372
00:22:54,480 --> 00:22:56,480
 in

373
00:22:56,480 --> 00:22:58,480
 in

374
00:22:58,480 --> 00:23:00,480
 in

375
00:23:00,480 --> 00:23:02,480
 in

376
00:23:02,480 --> 00:23:04,480
 in

377
00:23:04,480 --> 00:23:06,480
 in

378
00:23:06,480 --> 00:23:08,480
 in

379
00:23:08,480 --> 00:23:10,480
 in

380
00:23:10,480 --> 00:23:12,480
 in

381
00:23:12,480 --> 00:23:14,480
 in

382
00:23:14,480 --> 00:23:16,480
 in

383
00:23:16,480 --> 00:23:18,480
 in

384
00:23:18,480 --> 00:23:20,480
 in power over you

385
00:23:20,480 --> 00:23:22,480
 whether it be something pleasant that you want to

386
00:23:22,480 --> 00:23:24,480
 there's no you, there's no

387
00:23:24,480 --> 00:23:26,480
 thing

388
00:23:26,480 --> 00:23:28,480
 you see it if you see it as an experience it

389
00:23:28,480 --> 00:23:30,480
 arises

390
00:23:30,480 --> 00:23:32,480
 there's something that upsets you or

391
00:23:32,480 --> 00:23:34,480
 or

392
00:23:34,480 --> 00:23:36,480
 frustrates you

393
00:23:36,480 --> 00:23:38,480
 that thing is just an experience you see it

394
00:23:38,480 --> 00:23:40,480
 arising, it's easy, you see the experience

395
00:23:40,480 --> 00:23:42,480
 what does it mean?

396
00:23:42,480 --> 00:23:44,480
 it's just an experience

397
00:23:44,480 --> 00:23:46,480
 you see the frustration

398
00:23:46,480 --> 00:23:48,480
 you see the upset

399
00:23:48,480 --> 00:23:50,480
 arises and ceases

400
00:23:50,480 --> 00:23:52,480
 you stop

401
00:23:52,480 --> 00:23:54,480
 hiding, you stop running

402
00:23:54,480 --> 00:23:56,480
 you stop hiding, you stop running

403
00:23:56,480 --> 00:23:58,480
 stop

404
00:23:58,480 --> 00:24:00,480
 living in conceptual reality

405
00:24:00,480 --> 00:24:02,480
 with things and people and places

406
00:24:02,480 --> 00:24:04,480
 start living in reality

407
00:24:04,480 --> 00:24:06,480
 the true reality which is

408
00:24:06,480 --> 00:24:08,480
 your experience

409
00:24:08,480 --> 00:24:10,480
 start seeing what's going on

410
00:24:10,480 --> 00:24:12,480
 underneath that

411
00:24:12,480 --> 00:24:14,480
 chrome plating

412
00:24:14,480 --> 00:24:16,480
 chrome plating

413
00:24:16,480 --> 00:24:18,480
 of beautiful things

414
00:24:18,480 --> 00:24:20,480
 and desirable things

415
00:24:20,480 --> 00:24:22,480
 and ugly things and

416
00:24:22,480 --> 00:24:24,480
 scary things

417
00:24:24,480 --> 00:24:26,480
 underneath it's all

418
00:24:26,480 --> 00:24:28,480
 just an experience

419
00:24:28,480 --> 00:24:32,480
 anyway

420
00:24:32,480 --> 00:24:34,480
 there's the quote

421
00:24:34,480 --> 00:24:36,480
 for this evening, that's the

422
00:24:36,480 --> 00:24:38,480
 dumb luck for this evening

423
00:24:38,480 --> 00:24:40,480
 now as

424
00:24:40,480 --> 00:24:42,480
 usual I'm going to put the quote

425
00:24:42,480 --> 00:24:44,480
 I'll put the link

426
00:24:44,480 --> 00:24:46,480
 to the hangout up if you would

427
00:24:46,480 --> 00:24:48,480
 like to join and

428
00:24:48,480 --> 00:24:50,480
 ask a question you're welcome to

429
00:24:50,480 --> 00:24:52,480
 click the

430
00:24:52,480 --> 00:24:54,480
 link only if you have a

431
00:24:54,480 --> 00:24:56,480
 question and only if you're in a

432
00:24:56,480 --> 00:24:58,480
 respectful like no coming on naked

433
00:24:58,480 --> 00:25:00,480
 or smoking or drinking

434
00:25:00,480 --> 00:25:02,480
 alcohol or

435
00:25:02,480 --> 00:25:04,480
 eating food

436
00:25:04,480 --> 00:25:06,480
 you should

437
00:25:06,480 --> 00:25:08,480
 consider this to be a

438
00:25:08,480 --> 00:25:10,480
 dhamma hangout

439
00:25:12,480 --> 00:25:14,480
 scare everybody away

440
00:25:14,480 --> 00:25:34,480
 you can go Vanessa

441
00:25:34,480 --> 00:25:36,480
 to it

442
00:25:38,480 --> 00:25:40,480
 you

443
00:25:42,480 --> 00:25:44,480
 you

444
00:25:46,480 --> 00:25:48,480
 you

445
00:25:50,480 --> 00:25:52,480
 you

446
00:25:54,480 --> 00:25:56,480
 you

447
00:25:58,480 --> 00:26:00,480
 you

448
00:26:02,480 --> 00:26:04,480
 you

449
00:26:28,480 --> 00:26:30,480
 all right nobody with burning questions

450
00:26:30,480 --> 00:26:32,480
 sleeping to be

451
00:26:32,480 --> 00:26:34,480
 to join the hangout

452
00:26:34,480 --> 00:26:36,480
 and I think I'll say goodnight

453
00:26:36,480 --> 00:26:38,480
 see you all tomorrow

454
00:26:40,480 --> 00:26:42,480
 you

455
00:26:44,480 --> 00:26:46,480
 you

