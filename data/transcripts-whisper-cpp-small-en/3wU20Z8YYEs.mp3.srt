1
00:00:00,000 --> 00:00:06,380
 "Is one able to ordain with a medical condition such as

2
00:00:06,380 --> 00:00:08,800
 epilepsy that they take medicine for?"

3
00:00:08,800 --> 00:00:20,650
 I'll start now. Yes, it's possible to meditate. The

4
00:00:20,650 --> 00:00:23,040
 question, of course, is what the medication

5
00:00:23,040 --> 00:00:27,920
 does to your mind. I would, if possible, much rather have,

6
00:00:27,920 --> 00:00:30,480
 especially with epilepsy, have a

7
00:00:30,480 --> 00:00:35,860
 person slowly begin to wean themselves off the medication

8
00:00:35,860 --> 00:00:39,680
 in favor of the meditation. And when

9
00:00:39,680 --> 00:00:42,940
 an epileptic fit occurs, because as I understand they're

10
00:00:42,940 --> 00:00:45,680
 mainly physical, I don't know how it

11
00:00:45,680 --> 00:00:50,380
 affects the mind and maybe you can enlighten us, but if you

12
00:00:50,380 --> 00:00:52,920
're having an epileptic fit and you're

13
00:00:52,920 --> 00:00:56,070
 still able to be mindful of it, then just be mindful of it

14
00:00:56,070 --> 00:00:57,760
 as best you can. If you want to

15
00:00:57,760 --> 00:01:02,070
 take some medication, then take some medication. "To ordain

16
00:01:02,070 --> 00:01:07,800
, I think that is not allowed."

17
00:01:07,800 --> 00:01:14,580
 Well, yeah, I think this is not possible when you have

18
00:01:14,580 --> 00:01:19,000
 epilepsy. This is one of the questions

19
00:01:19,000 --> 00:01:24,070
 of the sicknesses. Okay, so it's a simple answer. No, you

20
00:01:24,070 --> 00:01:28,240
 can't. It depends how extreme it is,

21
00:01:28,240 --> 00:01:33,140
 I suppose. If you can find, if you can, we should do it as

22
00:01:33,140 --> 00:01:35,680
 a test. When I said we'll practice

23
00:01:35,680 --> 00:01:40,410
 meditation, I'm sorry I misread the question, but do that

24
00:01:40,410 --> 00:01:43,240
 as a test. If through meditation

25
00:01:43,240 --> 00:01:49,470
 practice your epilepsy is even cured, then you could ordain

26
00:01:49,470 --> 00:01:52,240
 because the word is just a word.

27
00:01:52,240 --> 00:01:56,850
 If certain things occur in the body, they may change over

28
00:01:56,850 --> 00:01:59,280
 time. Of course, these sicknesses,

29
00:01:59,280 --> 00:02:01,900
 if a person becomes cured of these things, then they can

30
00:02:01,900 --> 00:02:03,800
 ordain. So you should test it out in the

31
00:02:03,800 --> 00:02:06,880
 meditation, see how your meditation practice affects it

32
00:02:06,880 --> 00:02:08,800
 because you should certainly have

33
00:02:08,800 --> 00:02:12,840
 these things dealt with before you ordain. Having any

34
00:02:12,840 --> 00:02:15,280
 condition for which you have to take

35
00:02:15,280 --> 00:02:19,710
 medication for, which you have to take, or in order to

36
00:02:19,710 --> 00:02:23,280
 function properly, have to take medication for

37
00:02:23,280 --> 00:02:28,620
 is a real sign that you may not be able to ordain because

38
00:02:28,620 --> 00:02:30,560
 you can't be sure that you're

39
00:02:30,560 --> 00:02:34,030
 going to get that medication even though you'll be allowed

40
00:02:34,030 --> 00:02:36,320
 to. There's allowances to ask for it,

41
00:02:36,320 --> 00:02:39,790
 and so on. It becomes a burden, and because of the

42
00:02:39,790 --> 00:02:42,920
 uncertainty of being a monk and the

43
00:02:42,920 --> 00:02:48,810
 necessity of being content with whatever you get, makes it

44
00:02:48,810 --> 00:02:51,440
 quite a burden to the Sangha and a

45
00:02:51,440 --> 00:02:55,150
 barrier to your own ability to carry out all of the duties

46
00:02:55,150 --> 00:02:57,320
 of a monk and keep the hundreds of rules

47
00:02:57,320 --> 00:03:08,520
 and the practices and the communal lifestyle. There are

48
00:03:08,520 --> 00:03:13,120
 certain sicknesses, I don't remember all,

49
00:03:13,120 --> 00:03:21,070
 leprosy is one, and ringworms, I think, is one. So there

50
00:03:21,070 --> 00:03:25,880
 are certain, and I think epilepsy is one

51
00:03:25,880 --> 00:03:31,770
 of them. There are certain sicknesses you cannot have, or

52
00:03:31,770 --> 00:03:36,360
 if you have them you cannot ordain. There

53
00:03:36,360 --> 00:03:40,460
 is a part of the ordination procedure where your questions,

54
00:03:40,460 --> 00:03:42,480
 do you have this, do you have that,

55
00:03:42,480 --> 00:03:50,580
 and if you say yes I do, then you'll probably be advised

56
00:03:50,580 --> 00:03:56,200
 not to ordain. Okay.

57
00:03:56,200 --> 00:03:57,200
 Thank you.

