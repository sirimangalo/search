1
00:00:00,000 --> 00:00:07,000
 Okay, good evening.

2
00:00:07,000 --> 00:00:27,000
 I'm broadcasting live from Stoney Creek, Ontario, Canada.

3
00:00:27,000 --> 00:00:32,000
 I'm Bailey Dama.

4
00:00:32,000 --> 00:00:41,750
 It's interesting to compare teaching meditation to

5
00:00:41,750 --> 00:00:48,080
 practicing meditation in the context of

6
00:00:48,080 --> 00:00:56,110
 insight meditation. It's clear that these are two different

7
00:00:56,110 --> 00:01:00,160
 skills in many ways. It's possible

8
00:01:00,160 --> 00:01:05,670
 to be a good meditator but lack certain skills that are

9
00:01:05,670 --> 00:01:09,880
 required to be a good teacher. And

10
00:01:09,880 --> 00:01:17,740
 it's possible to be a good teacher and a relatively poor

11
00:01:17,740 --> 00:01:23,040
 meditator. But on the other hand, the

12
00:01:23,040 --> 00:01:25,040
 activity being the same.

13
00:01:25,040 --> 00:01:35,630
 First of all, obviously someone who's never done meditation

14
00:01:35,630 --> 00:01:41,040
 would have a hard time teaching.

15
00:01:41,040 --> 00:01:47,480
 And someone who has practiced extensive meditation would

16
00:01:47,480 --> 00:01:51,040
 have a much easier time and a much clearer

17
00:01:51,040 --> 00:02:00,580
 understanding of meditation. Not to mention how useful the

18
00:02:00,580 --> 00:02:04,040
 benefits of meditation are

19
00:02:04,040 --> 00:02:19,620
 in teaching in general, not just meditation but in relating

20
00:02:19,620 --> 00:02:23,040
 to others.

21
00:02:23,040 --> 00:02:37,310
 And then there's the fact that the activity is the same.

22
00:02:37,310 --> 00:02:43,040
 Meditation is about practicing meditation

23
00:02:43,040 --> 00:02:47,350
 or teaching meditation is both about training the mind.

24
00:02:47,350 --> 00:02:50,040
 Training the mind. In teaching you're

25
00:02:50,040 --> 00:02:57,880
 just training someone else's mind or you're being the inst

26
00:02:57,880 --> 00:03:01,040
igation for the cultivation

27
00:03:01,040 --> 00:03:05,050
 of understanding. So when you sit down to practice

28
00:03:05,050 --> 00:03:09,040
 meditation, your first task is basically

29
00:03:09,040 --> 00:03:17,760
 teaching yourself how to direct the mind. As a teacher, the

30
00:03:17,760 --> 00:03:21,040
 view is the same, it's just

31
00:03:21,040 --> 00:03:39,050
 someone else's mind that you're directing. And this is, I

32
00:03:39,050 --> 00:03:41,040
 think this is useful to talk

33
00:03:41,040 --> 00:03:50,070
 about because it allows us to say that to some extent

34
00:03:50,070 --> 00:03:55,040
 anyway. You can teach yourself

35
00:03:55,040 --> 00:03:58,850
 how to meditate or you can be your own teacher in the

36
00:03:58,850 --> 00:04:02,040
 practice of meditation. If you understand

37
00:04:02,040 --> 00:04:15,800
 the method and the means of the method behind how to direct

38
00:04:15,800 --> 00:04:19,040
 your mind.

39
00:04:19,040 --> 00:04:28,130
 So last week in our study of the Visuddhi Maga we came

40
00:04:28,130 --> 00:04:33,040
 across just a brief passage, actually

41
00:04:33,040 --> 00:04:42,500
 a set of methodology, whatever, means by which one can

42
00:04:42,500 --> 00:04:49,040
 cultivate the mind, cultivate states

43
00:04:49,040 --> 00:04:58,080
 of mind, meditative states of mind. And each one was

44
00:04:58,080 --> 00:05:00,040
 explained. It was in the context of

45
00:05:00,040 --> 00:05:05,490
 the meditation, but it's always been a teaching that stuck

46
00:05:05,490 --> 00:05:07,040
 with me as something that I recognized

47
00:05:07,040 --> 00:05:12,600
 in teaching especially. It made me see that the parallel

48
00:05:12,600 --> 00:05:14,040
 between teaching someone else

49
00:05:14,040 --> 00:05:25,280
 and doing it yourself. It said that the key to being a

50
00:05:25,280 --> 00:05:31,040
 successful meditator is in knowing

51
00:05:31,040 --> 00:05:42,070
 when, knowing when, when for things, when to exert the mind

52
00:05:42,070 --> 00:05:49,040
, when to restrain the mind,

53
00:05:49,040 --> 00:05:55,880
 knowing when to encourage the mind and knowing when to look

54
00:05:55,880 --> 00:06:04,040
 upon the mind with equanimity.

55
00:06:04,040 --> 00:06:15,150
 These are the four means of directing the mind. Important

56
00:06:15,150 --> 00:06:16,040
 because it's clear that we

57
00:06:16,040 --> 00:06:23,480
 shouldn't always engage in each of these. It should know

58
00:06:23,480 --> 00:06:28,040
 when, so when to. It's this

59
00:06:28,040 --> 00:06:30,900
 skill that is necessary both for a teacher in meditation

60
00:06:30,900 --> 00:06:32,040
 and for a meditator. As a teacher

61
00:06:32,040 --> 00:06:49,260
 you have to be alert and in tune with the student's state

62
00:06:49,260 --> 00:06:53,040
 of mind. Are they in need of encouragement

63
00:06:53,040 --> 00:06:59,140
 or are they in need of advice on, should they be told to

64
00:06:59,140 --> 00:07:03,040
 push harder, should they be told

65
00:07:03,040 --> 00:07:08,380
 to relax, restrain the mind, should they be told, should

66
00:07:08,380 --> 00:07:11,040
 they be given encouragement

67
00:07:11,040 --> 00:07:15,960
 or should they just be left alone and looked upon with equ

68
00:07:15,960 --> 00:07:19,040
animity. Clearly all, not all

69
00:07:19,040 --> 00:07:24,990
 of these are useful in the same situation. Just remembering

70
00:07:24,990 --> 00:07:28,040
 these four is useful. Useful

71
00:07:28,040 --> 00:07:31,910
 for a teacher but also quite useful for a meditator. A med

72
00:07:31,910 --> 00:07:34,040
itator remembers these, they

73
00:07:34,040 --> 00:07:37,780
 can adjust their practice themselves. So this is on the

74
00:07:37,780 --> 00:07:40,040
 level of adjusting your practice.

75
00:07:40,040 --> 00:07:45,000
 It's not talking about the actual practice of course. When

76
00:07:45,000 --> 00:07:48,040
 we practice it's simply about

77
00:07:48,040 --> 00:07:55,250
 cultivating mindfulness. But in order to frame that

78
00:07:55,250 --> 00:08:00,040
 practice in the right, in the right light

79
00:08:00,040 --> 00:08:09,390
 or in the right mindset, you have to adjust and so that's

80
00:08:09,390 --> 00:08:14,040
 what this is. So exerting the

81
00:08:14,040 --> 00:08:25,870
 mind means cultivating effort, energy. Cultivating those

82
00:08:25,870 --> 00:08:31,040
 qualities of mind that bring about energy.

83
00:08:31,040 --> 00:08:38,870
 This is when the mind is overly sluggish or intently

84
00:08:38,870 --> 00:08:56,040
 focused and the mind is inflexible.

85
00:08:56,040 --> 00:09:14,110
 The ability to adapt, to keep up with the pace of

86
00:09:14,110 --> 00:09:21,040
 experience, cultivating effort. We have

87
00:09:21,040 --> 00:09:26,040
 to know when to exert the mind. We should exert the mind.

88
00:09:26,040 --> 00:09:29,040
 We should know when we're lacking

89
00:09:29,040 --> 00:09:34,390
 in energy. So we exert the mind when we are overly focused,

90
00:09:34,390 --> 00:09:37,040
 overly concentrated, when we're

91
00:09:37,040 --> 00:09:43,520
 stuck on a single object or when we're not being mindful,

92
00:09:43,520 --> 00:09:46,040
 when we're not really acknowledging

93
00:09:46,040 --> 00:09:50,810
 the object, we're really aware of the object, maybe walking

94
00:09:50,810 --> 00:09:54,040
 back and forth, not really aware.

95
00:09:54,040 --> 00:10:02,840
 Exert our minds in this way. Become aware. This is when we

96
00:10:02,840 --> 00:10:06,040
 cultivate effort, when we cultivate

97
00:10:06,040 --> 00:10:11,580
 investigation, sending the mind to the object. Do we really

98
00:10:11,580 --> 00:10:15,040
 know that we're walking? Do we

99
00:10:15,040 --> 00:10:17,100
 really know that we're lifting the foot and moving the foot

100
00:10:17,100 --> 00:10:18,040
? Do we really know that the

101
00:10:18,040 --> 00:10:22,570
 stomach is rising and falling? Do we have to exert our mind

102
00:10:22,570 --> 00:10:26,040
? Are we making the... we have

103
00:10:26,040 --> 00:10:33,050
 to remind ourselves, rising and falling? Are we really med

104
00:10:33,050 --> 00:10:37,040
itating? Are we just sitting?

105
00:10:37,040 --> 00:10:50,090
 Are we just walking? Conversely, we have to know when to

106
00:10:50,090 --> 00:10:52,040
 restrain the mind. This is when

107
00:10:52,040 --> 00:10:56,360
 the mind is too energetic, too excited, so distracted. We

108
00:10:56,360 --> 00:10:58,040
're thinking about too many

109
00:10:58,040 --> 00:11:02,080
 things and again we're not mindful but for another reason

110
00:11:02,080 --> 00:11:04,040
 we're not mindful because our

111
00:11:04,040 --> 00:11:11,030
 minds are overactive. We have to know when to restrain the

112
00:11:11,030 --> 00:11:14,040
 mind. Focus is a lot like

113
00:11:14,040 --> 00:11:16,170
 the focus of a camera where you have to... it's not about

114
00:11:16,170 --> 00:11:18,040
 going to one extreme or another,

115
00:11:18,040 --> 00:11:23,670
 it's about finding the perfect balance. That's what we do.

116
00:11:23,670 --> 00:11:26,040
 We try to balance the mind.

117
00:11:26,040 --> 00:11:30,960
 If we're too energetic, we have to calm down. So we develop

118
00:11:30,960 --> 00:11:34,040
 those qualities of mind that

119
00:11:34,040 --> 00:11:40,430
 are calming. We still practice mindfulness but we become

120
00:11:40,430 --> 00:11:43,040
 aware that our efforts are too

121
00:11:43,040 --> 00:11:46,690
 energetic and we start to focus on fewer objects or we

122
00:11:46,690 --> 00:11:49,040
 become aware of the overarching state

123
00:11:49,040 --> 00:11:53,230
 of mind to collect our thoughts and keep them from becoming

124
00:11:53,230 --> 00:11:55,040
 scattered. We just say to

125
00:11:55,040 --> 00:11:58,870
 ourselves, scattered, scattered or distracted, or

126
00:11:58,870 --> 00:12:04,040
 overwhelmed, overwhelmed. Our mind becomes

127
00:12:04,040 --> 00:12:09,130
 more focused so it's the same activity but it's a different

128
00:12:09,130 --> 00:12:12,040
 quality. The quality of energy

129
00:12:12,040 --> 00:12:22,150
 when we're too stuck, fixed in the mind. The quality of

130
00:12:22,150 --> 00:12:27,040
 calm and tranquility when our minds

131
00:12:27,040 --> 00:12:37,410
 are too distracted. We have to know when to encourage the

132
00:12:37,410 --> 00:12:40,040
 mind. This is when we're feeling

133
00:12:40,040 --> 00:12:45,980
 very interested, when we have doubts or when we're disincl

134
00:12:45,980 --> 00:12:49,040
ined to meditate, when we're

135
00:12:49,040 --> 00:12:54,170
 disinclined to practice. Putting our full heart into it, it

136
00:12:54,170 --> 00:12:56,040
's clear in the mind as to

137
00:12:56,040 --> 00:13:04,990
 the benefit. It's not clearly of interest to us to meditate

138
00:13:04,990 --> 00:13:06,040
. So here we need to encourage

139
00:13:06,040 --> 00:13:09,350
 ourselves. Sometimes this is not even through meditation,

140
00:13:09,350 --> 00:13:11,040
 this is only through listening

141
00:13:11,040 --> 00:13:14,890
 to the Dhamma or associating with good people. That's why

142
00:13:14,890 --> 00:13:17,040
 teachers are so useful because

143
00:13:17,040 --> 00:13:19,800
 they're good at encouragement, they're good at reminding

144
00:13:19,800 --> 00:13:21,040
 you why you're doing it, they're

145
00:13:21,040 --> 00:13:25,940
 good at being an example of why you should do it. I don't

146
00:13:25,940 --> 00:13:30,040
 know when we need encouragement.

147
00:13:30,040 --> 00:13:33,560
 Sometimes we don't need encouragement and as a teacher, be

148
00:13:33,560 --> 00:13:35,040
 careful not to encourage

149
00:13:35,040 --> 00:13:40,520
 meditators. Sometimes they're overly confident and you have

150
00:13:40,520 --> 00:13:43,040
 to make them question so they

151
00:13:43,040 --> 00:13:53,510
 can see that their confidence may not be well founded.

152
00:13:53,510 --> 00:13:57,040
 Sometimes it leads to wrong practice

153
00:13:57,040 --> 00:14:03,490
 and we think we're doing it right, we just go all out,

154
00:14:03,490 --> 00:14:07,040
 ignore any idea that it might be

155
00:14:07,040 --> 00:14:13,420
 wrong. But certainly one of the teacher's main objectives

156
00:14:13,420 --> 00:14:17,040
 is, for main purposes, is

157
00:14:17,040 --> 00:14:24,510
 to encourage the meditator. But finally, sometimes there

158
00:14:24,510 --> 00:14:27,040
 has to be a certain, it's interesting

159
00:14:27,040 --> 00:14:32,060
 that there's nothing about discouraging the meditator,

160
00:14:32,060 --> 00:14:35,040
 which is interesting but somehow

161
00:14:35,040 --> 00:14:42,670
 rings true, I'm not sure exactly what the intention is but

162
00:14:42,670 --> 00:14:46,040
 practically it really is

163
00:14:46,040 --> 00:14:51,930
 the opposite of encouraging because the teacher actually

164
00:14:51,930 --> 00:14:57,040
 seems unproductive from experience.

165
00:14:57,040 --> 00:15:01,240
 Discouragement seems quite unproductive as a teacher. It

166
00:15:01,240 --> 00:15:03,040
 actually doesn't seem useful

167
00:15:03,040 --> 00:15:06,680
 for a teacher to discourage the meditator, it doesn't seem

168
00:15:06,680 --> 00:15:09,040
 to work very well. The meditator

169
00:15:09,040 --> 00:15:15,020
 is doing wrong. What seems to work incredibly well is

170
00:15:15,020 --> 00:15:19,040
 exactly what is said here, looking

171
00:15:19,040 --> 00:15:25,040
 on with equanimity. Actually in the context of this passage

172
00:15:25,040 --> 00:15:28,040
, looking on with equanimity

173
00:15:28,040 --> 00:15:36,370
 is when things are going well. The meditator is doing well.

174
00:15:36,370 --> 00:15:39,040
 So when everything is going

175
00:15:39,040 --> 00:15:43,050
 fine, the teacher, in this case the meditator, should just

176
00:15:43,050 --> 00:15:45,040
 continue as they're going, should

177
00:15:45,040 --> 00:15:50,370
 not adjust their practice at all. But this works quite well

178
00:15:50,370 --> 00:15:53,040
 when the meditator needs

179
00:15:53,040 --> 00:16:01,880
 reining in, when you yourself need to assess your practice,

180
00:16:01,880 --> 00:16:07,040
 when you're not sure. Equanimity

181
00:16:07,040 --> 00:16:12,890
 is the best means of figuring out what's wrong if anything.

182
00:16:12,890 --> 00:16:15,040
 When you don't know, you don't

183
00:16:15,040 --> 00:16:19,200
 know it's anything wrong. Equanimity allows you to see what

184
00:16:19,200 --> 00:16:21,040
's wrong. As a teacher, the

185
00:16:21,040 --> 00:16:23,700
 interesting thing is when you're a teacher and you're an

186
00:16:23,700 --> 00:16:26,040
 equanimous, when you sit silently

187
00:16:26,040 --> 00:16:28,140
 and listen to a meditator and don't give them the

188
00:16:28,140 --> 00:16:30,040
 encouragement that they're expecting,

189
00:16:30,040 --> 00:16:33,350
 there's sort of the greatest discouragement for wrong

190
00:16:33,350 --> 00:16:36,040
 behavior that you can give. The

191
00:16:36,040 --> 00:16:38,950
 meditator is excited about something that they shouldn't be

192
00:16:38,950 --> 00:16:40,040
 excited about, where it's

193
00:16:40,040 --> 00:16:45,000
 like a dead end. When meditators start freaking out over

194
00:16:45,000 --> 00:16:48,040
 experiences they've had or emotions

195
00:16:48,040 --> 00:16:51,390
 that come up. When they see that the teacher is unfazed by

196
00:16:51,390 --> 00:16:53,040
 it, totally uninterested in

197
00:16:53,040 --> 00:16:57,740
 it, the equanimous towards it, then likewise they treat it

198
00:16:57,740 --> 00:17:00,040
 as something meaningless as

199
00:17:00,040 --> 00:17:04,090
 well and don't get caught up in it. They come and tell the

200
00:17:04,090 --> 00:17:06,040
 teacher what great things came

201
00:17:06,040 --> 00:17:09,130
 for practice and the teacher says, "Oh yeah, were you

202
00:17:09,130 --> 00:17:11,040
 mindful of it?" They say, "Okay,

203
00:17:11,040 --> 00:17:16,650
 well that obviously wasn't enough to surprise me, to excite

204
00:17:16,650 --> 00:17:20,040
 my teacher, so I'll give it

205
00:17:20,040 --> 00:17:29,490
 up." Knowing when to be equanimous, where it's equally

206
00:17:29,490 --> 00:17:31,040
 important, but a text describes

207
00:17:31,040 --> 00:17:34,680
 it as important when things are going well. You should

208
00:17:34,680 --> 00:17:37,040
 never become excited about things

209
00:17:37,040 --> 00:17:41,150
 going well. When everything's going well, that's the time

210
00:17:41,150 --> 00:17:43,040
 to coast, or not to coast,

211
00:17:43,040 --> 00:17:46,720
 to be on cruise control, to cruise. To just continue

212
00:17:46,720 --> 00:17:49,040
 exactly as you've been doing it,

213
00:17:49,040 --> 00:17:57,110
 to not change anything. It's very close to the actual

214
00:17:57,110 --> 00:18:02,040
 meditation practice of not rocking

215
00:18:02,040 --> 00:18:05,540
 the boat, because you want to see what's going on. You're

216
00:18:05,540 --> 00:18:07,040
 going to see when things go wrong.

217
00:18:07,040 --> 00:18:10,730
 As soon as things go wrong, you'll see it if you're impart

218
00:18:10,730 --> 00:18:12,040
ially watching, objectively

219
00:18:12,040 --> 00:18:18,410
 observing your experience. So just a little something to

220
00:18:18,410 --> 00:18:20,040
 think about. These four things

221
00:18:20,040 --> 00:18:24,390
 are a good guideline you want to teach yourself. I think it

222
00:18:24,390 --> 00:18:27,040
's essential that you remember these

223
00:18:27,040 --> 00:18:33,670
 ideas, the idea of when. You should not always be exerting

224
00:18:33,670 --> 00:18:37,040
 yourself, or pushing yourself.

225
00:18:37,040 --> 00:18:43,040
 The whole idea of reprimanding the students coast for

226
00:18:43,040 --> 00:18:48,040
 yourself shouldn't be hard on yourself.

227
00:18:48,040 --> 00:18:52,540
 Most of the time be equanimous. It's only when you see that

228
00:18:52,540 --> 00:18:55,040
 something's going wrong

229
00:18:55,040 --> 00:18:59,800
 that you react by encouraging, by exerting yourself further

230
00:18:59,800 --> 00:19:02,040
, or by restraining your mind,

231
00:19:02,040 --> 00:19:07,750
 or by providing the encouragement that's missing. You're

232
00:19:07,750 --> 00:19:10,040
 doing something wrong if the mind

233
00:19:10,040 --> 00:19:18,630
 is misbehaving. You should just look on with equanimity and

234
00:19:18,630 --> 00:19:21,040
 let go. Don't follow it, don't

235
00:19:21,040 --> 00:19:26,090
 cling to it. Don't become excited by it. Anyway, there are

236
00:19:26,090 --> 00:19:29,040
 some thoughts, some dhamma for tonight.

237
00:19:29,040 --> 00:19:32,040
 Thank you all for tuning in, and be well.

