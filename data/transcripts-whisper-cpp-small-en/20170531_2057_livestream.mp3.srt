1
00:00:00,000 --> 00:00:07,000
 Okay, good evening everyone.

2
00:00:07,000 --> 00:00:21,000
 Welcome to our daily Dhamma.

3
00:00:21,000 --> 00:00:30,000
 Today's topic is, what is it like to be enlightened?

4
00:00:30,000 --> 00:00:36,000
 It's a good question, isn't it?

5
00:00:36,000 --> 00:00:48,000
 It is what we're striving for, ultimately, right?

6
00:00:48,000 --> 00:00:56,000
 So why is it a good question? Why is it good to know?

7
00:00:56,000 --> 00:01:00,000
 I think obviously it's something we all are curious about

8
00:01:00,000 --> 00:01:05,000
 and want to know.

9
00:01:05,000 --> 00:01:09,210
 First of all, so that we know whether it's something that

10
00:01:09,210 --> 00:01:12,000
 might be worth attaining ourselves,

11
00:01:12,000 --> 00:01:18,000
 and also to know how far we are from it.

12
00:01:18,000 --> 00:01:23,000
 But another, I think, interesting point is,

13
00:01:23,000 --> 00:01:32,000
 it allows us to direct ourselves in the right direction.

14
00:01:32,000 --> 00:01:44,930
 So even when we're not enlightened ourselves, we can mimic

15
00:01:44,930 --> 00:01:49,000
 an enlightened being.

16
00:01:49,000 --> 00:01:52,000
 Mimic is not the right word. We can emulate.

17
00:01:52,000 --> 00:01:58,000
 That's the word.

18
00:01:58,000 --> 00:02:02,000
 Emulate is like when we keep the five precepts, when we don

19
00:02:02,000 --> 00:02:02,000
't kill.

20
00:02:02,000 --> 00:02:07,040
 Well, enlightened beings don't kill naturally for the unen

21
00:02:07,040 --> 00:02:08,000
lightened,

22
00:02:08,000 --> 00:02:14,050
 something you have to work at. So you emulate enlightened

23
00:02:14,050 --> 00:02:16,000
 behavior.

24
00:02:16,000 --> 00:02:19,000
 A cynic might say, "Well, you fake."

25
00:02:19,000 --> 00:02:22,000
 But you know the saying, "Fake it till you make it."

26
00:02:22,000 --> 00:02:26,000
 There's something interesting there because

27
00:02:26,000 --> 00:02:29,000
 the Tibetans believe that by pretending to be a Buddha,

28
00:02:29,000 --> 00:02:34,000
 or not pretending by really believing that you are a Buddha

29
00:02:34,000 --> 00:02:34,000
,

30
00:02:34,000 --> 00:02:41,000
 you actually become closer to being a Buddha.

31
00:02:41,000 --> 00:02:44,480
 And well, I don't think that that's, I don't agree that

32
00:02:44,480 --> 00:02:47,000
 that's adequate practice.

33
00:02:47,000 --> 00:02:49,000
 I don't know that the Tibetans really do either,

34
00:02:49,000 --> 00:02:52,000
 although I don't know too much about their tradition.

35
00:02:52,000 --> 00:02:57,000
 But there's something to it.

36
00:02:57,000 --> 00:03:03,000
 I mean, it gives you a road map.

37
00:03:03,000 --> 00:03:08,000
 It gives you some markers to live by,

38
00:03:08,000 --> 00:03:11,220
 to know when you're acting like an enlightened being and to

39
00:03:11,220 --> 00:03:12,000
 know when you're not,

40
00:03:12,000 --> 00:03:25,000
 so to know when you have to change.

41
00:03:25,000 --> 00:03:27,850
 So this is the question, "What is it like to be enlightened

42
00:03:27,850 --> 00:03:28,000
?"

