 Mindfulness and Noting
 Question.
 What does it mean to note something?
 What is the thing that you are noting?
 What is the difference between noting and being mindful if
 there is any, or the relationship between them?
 Answer.
 The word mindfulness is an imperfect translation of the
 word sati.
 Sati is the state of mind that rightly grasps the object in
 such a way that it can be seen clearly.
 Sati can also be used in a conventional sense as the
 ability to remember things that happened a long time ago.
 In meditation, it means the ability to remember the present
 moment.
 It means not being distracted by or dwelling in abstract
 thought.
 Abstract thought is not evil or bad, but it is not mindful.
 In our everyday activities, we have to rely on abstract
 concepts.
 Many of our ordinary activities will be difficult to
 perform mindfully because being mindful means only being
 aware of the moments of experience.
 It is hard to maintain a stream of conceptual thought when
 you are mindful.
 Likewise, when we dwell in abstract thought, we are
 generally unmindful of reality.
 For example, you are probably sitting while reading this,
 but you likely have no awareness of that fact while reading
.
 When you are caught up in abstract thought, you quickly
 forget that you are sitting.
 But when you read, did you know that you are sitting?
 Suddenly, you are reminded that you are sitting.
 I think this is why the Buddha chose the word sati to
 describe meditation not just as a state, but as an activity
, remembering.
 Sati is what leads to seeing clearly, and the act of
 cultivating sati is this activity of reminding yourself of
 your experiential reality.
 Abstract thought is useful for cultivating right view.
 Abstraction is the basis of language, so for someone else
 to remind us of something, they need to use words that
 describe abstract concepts.
 Words themselves are also concepts.
 The word sati does not mean anything, it is just a sound.
 Its meaning is only conceptual based on how our minds
 interpret the sound.
 We know what sati refers to, and based on our faculties of
 memory and recognition, we associate that word with a
 certain mind state.
 The mind state described by sati is real.
 Mindfulness is real, but the word mindfulness is just a
 concept.
 When I say mindfulness, it evokes a perception of certain
 real mind states that are useful and not abstract.
 You may find this teaching useful.
 Teaching is a useful abstraction that can change the way
 you look at things and allow you to see reality in a
 different way.
 But it will never be a replacement for the experience of
 reality for yourself.
 It will never be enough.
 You could listen to all of my talks and it would never be
 enough, unless you transform the teachings into actual
 practice that allows you to cultivate the realities
 described by the teachings.
 This is where noting comes in.
 Noting is a sort of gateway between the teaching and the
 mind states.
 Noting simply means saying a word to yourself that has a
 connection to the reality you are experiencing at that
 moment.
 When you engage in this practice of saying something to
 yourself, this is what we call mantra meditation.
 Mantra meditation is very old.
 It was around before the Buddha himself and is widespread
 and broadly recognized today by many religious, spiritual,
 and even secular practices as an effective tool because it
 is the simplest means of focusing the mind state.
 It is similar to how we use words to express emotion.
 If I yell at someone and say "you hurt me" it focuses my
 mind on the anger and self-righteous indignation I feel in
 that moment, augmenting those mind states.
 Words are quite powerful.
 They have the effect of augmenting our mental states and
 emotions, often to our detriment.
 We speak words in response to many of our experiences in
 ordinary life.
 When you like something, you may say to yourself "this is
 great" which reinforces your attachment to that thing.
 Mantras make use of this process by which words evoke
 specific states of mind.
 Self-help gurus often suggest mantras that help their
 followers create specific states of mind, stimulating the
 mind in a particular way.
 It is not common for someone to intentionally practice with
 a mantra that stimulates unwholesome qualities like greed
 or anger, but we often use words that evoke unwholesome
 mind states unknowingly.
 This is why meditation can be dangerous.
 Quite conceivably, you could create a meditation practice
 that would make you more angry, more greedy, or more del
uded.
 Mantras that stimulate delusion may be the most common, as
 religious teachings that have wrong views often involve
 teachings, sayings, mottos, and even mantras that support
 delusion.
 A good meditation may be used to evoke positive emotions.
 One example that should come to mind for many people is M
etta, or friendliness meditation, where we use the mantra "
may I be happy, may all beings be happy."
 The words themselves are not wholesome, but they have the
 potential to evoke wholesome emotive states like friend
liness, compassion, and kindness.
 Other wholesome meditations are designed to cultivate not
 emotions, but certain other qualities of mind like focus or
 clarity.
 Cassina meditation is a good example.
 Cassina means totality.
 A Cassina is an object that you use to create a sense of
 totality, where your whole world is just one thing, like a
 color, for example.
 You may start by focusing on a circle of white, and you
 would just repeat to yourself, "white, white, white."
 The mantra helps evoke a single pointed attention on the
 color.
 White is not anything special, but, because it is a simple
 concept and very singular, focusing on it allows for the
 cultivation of focused attention.
 Repeating "white" is not likely to make you angry, greedy,
 or deluded. It is not going to evoke bad things.
 Such practice has the potential to help you create very
 powerful and pure states of mind.
 Eventually, if you practice such meditation, all you will
 see, even when you close your eyes, is white.
 The benefit is this very strong concentrated awareness.
 With mindfulness meditation, the purpose of the mantra is
 to help our minds focus on reality rather than on a concept
.
 We use abstraction, noting, to bring the mind back to
 reality.
 It is a unique form of meditation. It may share technical
 similarities to other types of meditation,
 but it is unique in application because rather than
 focusing your attention on another abstraction,
 the object of the mantra is real or experiential.
 The word "real" can mean different things, but experiential
 means it is a part of experience,
 which is the essence of ultimate reality in Buddhism.
 When you say to yourself, "seeing, seeing," it helps focus
 your mind on the experience so you remember simply that you
 are seeing at that moment.
 There is this awareness and presence that is objective,
 unlike our ordinary experience of things, which is steeped
 in judgment and subjectivity.
 Ordinarily, we recognize that an object has brought us
 pleasure in the past, so we like and conceive of it as
 positive,
 or we associate it with a past suffering and dislike it, or
 we associate it with some view, conceit or arrogance, and
 delusion arises.
 We might look in the mirror and see ourselves and think, "I
 am so handsome," or "I am so ugly,"
 and conceit arises, or some view of self like, "This is me.
 This is mine. This is what I am."
 This practice of reminding yourself of the simple nature of
 each experience has the potential to remove all of those
 habits of greed, anger, and delusion.
 It evokes objective states in our minds.
 Without mindfulness, we respond to experiences with
 thoughts of, "This is bad. This is good. This is me. This
 is mine."
 But with the mindfulness practice, you say, "This is this,"
 i.e., "seeing is seeing, hearing is hearing, thinking is
 thinking."
 This is explicitly what the Buddha taught.
 What you can read about him teaching time and again.
 We have shells of the Buddha's teaching, but this very
 basic teaching is so simple and so ordinary that it often
 gets overlooked.
 What is Buddhism? Buddhism is about seeing, hearing,
 smelling, tasting, feeling, thinking.
 We are made up of the five aggregates, and they arise at
 the moment of experience of any of the six senses.
 Mindfulness is simply the practice of purifying our
 perception of these moments of experience, so our reactions
 are in line with reality rather than partiality or delusion
.
 It is important not to confuse the act of noting with the
 states of mind that it evokes.
 Mindfulness is a tool. It is an artificial tool used to
 bring the mind closer to reality and become more objective
 about the reality or the experience.
 Mindfulness, or noting, is not the objectivity or awareness
.
 Noting is the tool that you must use to evoke certain mind
 states.
 If you are anxious and you know that anxiety is composed of
 different physical and mental states, you can still just
 say, "Anxious, anxious."
 The idea is to help you experience reality with a clear
 mind.
 The mantra is not some kind of pill that you swallow or a
 magical wand that you wave to change reality.
 You do not have to apply the technique to every little
 thing you experience.
 With anxiety, for example, the whole experience is anxiety,
 but some aspects of it are physical and some are mental.
 The butterflies that you feel in your stomach, the heart
 beating quickly, and the tension in your shoulders can all
 be noted individually,
 which will help you experience them without reaction or
 judgment by simply saying to yourself when you are tense in
 the shoulders, "Tense, tense,"
 or when you feel there are butterflies in your stomach
 because you are anxious, saying, "Feeling, feeling."
 The experience does not necessarily go away, but it also
 does not lead to more anxiety.
 You will see that the practice of noting evokes states of
 neutrality and objectivity that are not reactive.
 Some states, like anxiety, grow. With anxiety, you start
 with a thought.
 For example, I might think, "There are 50 people now
 listening to me talk here,"
 and that thought may evoke certain states of anxiety in the
 mind, which in turn create feelings in the body.
 Those feelings in the body may then make me more anxious.
 Then I may think, "Oh, I am anxious," and the feelings and
 mind states bounce back and forth like this.
 The physical states lead to more anxiety. More anxiety
 leads to stronger physical reactions,
 creating a feedback loop wherein anxiety becomes stronger
 and stronger until it can even lead to a full-blown panic
 attack.
 In each moment of anxiety, there is a process occurring.
 First, there is an experience. Then there is conceiving
 about that experience.
 And finally, there is the reaction based on one's
 conceptions of the experience.
 This process can be broken by simply changing one's
 conception of the experience.
 Rather than conceiving of the experience as, "I am anxious,
 this is a problem,"
 you change the conception to just, "This is anxiety," or
 the physical, "This is feeling,"
 by using mantras like "anxious" or "feeling" to remind
 yourself of the objective nature of the experience.
 An act which evokes very different states of mind from
 ordinary conception.
 The mantra is not magic. It is not in and of itself
 mindfulness.
 But if used consistently and with proper attention, it can
 evoke states of objectivity and mindfulness
 that break away from the conceptual process that leads to
 building up of things like anxiety.
 Conceptualization can be useful, and the mantra is a useful
 conceptualization
 that helps us direct our attention to the object of our
 experience.
 In other types of meditation, a mantra is used to direct
 one's attention to concepts like white.
 In mindfulness, the object is not concepts, but experient
ial reality.
 This is an important difference because reality has certain
 qualities that are not present in concepts.
 Firstly, impermanence. Whereas we perceive conceptual
 objects to be stable in our imagination,
 experiences arise and cease from moment to moment.
 Because concepts rely on experiences to arise, our lack of
 familiarity with the impermanence of reality
 leads to disappointment when the conceptual things we come
 to expect cannot be produced on a whim.
 Through observing experiences directly, we become familiar
 with the chaotic nature of reality,
 and the whole idea of expectation becomes absurd.
 We cannot know what the future will bring, and forming
 expectations in our minds becomes a habit
 that can only lead to eventual disappointment.
 There is no benefit to expecting. What does it mean to say,
 "I want things to be this way"?
 All it means is you are cultivating a habit of desire. It
 does not have any bearing on how things are going to be.
 Secondly, mindfulness of experiences provides understanding
 of suffering.
 When you try to fix or control your conceptual reality, you
 become stressed and are susceptible to disappointment.
 Even if you are able occasionally to get whatever you want,
 you then just want it more.
 By keeping the mind present and engaged with your actual
 experiences, you see this stress and attachment for what it
 is,
 realizing that reality admits of no satisfaction for those
 who cling to concepts or experiences as they are
 unpredictable and uncontrollable.
 Thirdly, mindfulness shows you non-self, that the
 conceptual entities we evoke in our minds are merely
 illusions formed of perception, recognition, and memory.
 Through mindful observation of experiences, you see that
 momentary experiences underlie all things.
 They have no substance beyond our imagination and the
 experiences that trigger it.
 As a result of this clarity of vision, we no longer obsess
 about controlling the world around us or cling to things as
 "me" or "mine."
 We see that this clinging to illusions leads only to
 suffering.
 Our lack of familiarity with these fundamental aspects of
 nature leads us to conceive of experiences as entities and
 those entities as bringing us pleasure or pain, which
 causes us to react positively or negatively towards them.
 Through mindfulness, we start to see experiences just as
 experiences.
 Noting is simply a tool to help evoke a state of
 mindfulness.
 When practicing noting meditation, it is important not to
 worry too much about the details, like whether you are
 noting everything or noting the correct thing or noting
 something correctly.
 You do not have to note everything. You do not have to
 worry about being perfectly correct.
 We note sitting. Sitting, for example, even though it is
 not the concept of sitting we are focused on,
 the noting of sitting focuses your attention on the
 experience of sitting, the pressure on your bottom, the
 tension in your back and in your legs, and so on, which is
 absolutely real.
 Saying to yourself, "Sitting, sitting," evokes this
 objective awareness of the experience.
 Saying, "Sitting," is not saying, "This is good," or "This
 is bad."
 It is an objective thing to say so, even though it is
 conceptual, and the sitting itself is conceptual.
 The mantra helps you focus on reality. Anyone who is
 critical of this should read what the Buddha himself said.
 When sitting, know clearly to yourself, "I am sitting."
 Most important is that mantra meditation or noting
 meditation, or whatever you call it, is a concrete, easily
 accessible practice that almost anyone can undertake.
 Right here and now, you can use it to become objectively
 aware of your own experiences as they truly are.
 Noting is a very useful tool, but do not let the tool
 become the object. Do not focus on the noting, thinking, "
Is it working? Why is it not working? Why do I say pain,
 pain, and the pain does not go away? I expect results. Why
 am I not getting them?"
 You are not getting good results because of expectation.
 The mantra is just a tool.
 The consequent mindfulness is what does the work to free us
 from suffering.
