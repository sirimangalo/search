1
00:00:00,000 --> 00:00:19,320
 Good evening.

2
00:00:19,320 --> 00:00:35,040
 Broadcasting Live.

3
00:00:35,040 --> 00:00:50,240
 So today's quote starts with the word "Eta".

4
00:00:50,240 --> 00:00:55,440
 Eta is a very powerful word.

5
00:00:55,440 --> 00:01:00,040
 It's very iconically Buddhist.

6
00:01:00,040 --> 00:01:05,400
 It's a really good Dhamma-Bhada verse.

7
00:01:05,400 --> 00:01:08,840
 It starts with "Eta".

8
00:01:08,840 --> 00:01:15,000
 "Eta passatimanglokan".

9
00:01:15,000 --> 00:01:17,000
 Come look at this world.

10
00:01:17,000 --> 00:01:32,880
 See if I can find that verse.

11
00:01:32,880 --> 00:01:55,880
 "Eta passatimanglokan".

12
00:01:55,880 --> 00:02:06,880
 "Yatambhala vissi danti nati sango vijanatam".

13
00:02:06,880 --> 00:02:08,880
 "Come look" "come" "Eta".

14
00:02:08,880 --> 00:02:10,880
 "Eta" means "come".

15
00:02:10,880 --> 00:02:17,880
 It's the imperative form of the verb.

16
00:02:17,880 --> 00:02:21,880
 "E" the root is just the letter "I".

17
00:02:21,880 --> 00:02:25,880
 It becomes "Eti".

18
00:02:25,880 --> 00:02:29,880
 "Eti" is "he goes" or "he she comes".

19
00:02:29,880 --> 00:02:31,880
 "Goes" or "comes".

20
00:02:31,880 --> 00:02:35,880
 I think it can mean either.

21
00:02:35,880 --> 00:02:38,880
 "Eta" "come"

22
00:02:38,880 --> 00:02:43,880
 "Passata" "see" or "look".

23
00:02:43,880 --> 00:02:50,880
 "Eta passata" is a core part of Buddhism.

24
00:02:50,880 --> 00:02:52,880
 "Come and see".

25
00:02:52,880 --> 00:02:58,880
 The Dhamma of the Buddha is called "Ehi passiko".

26
00:02:58,880 --> 00:03:04,880
 "Ehi passa"

27
00:03:04,880 --> 00:03:07,880
 "Ehi passata passati"

28
00:03:07,880 --> 00:03:09,880
 and then "ika".

29
00:03:09,880 --> 00:03:12,880
 "Passika" is something that can be seen.

30
00:03:12,880 --> 00:03:16,880
 "Ehi passika" is something that can be seen.

31
00:03:16,880 --> 00:03:19,880
 Something that you can come and see.

32
00:03:19,880 --> 00:03:21,880
 It's an odd word.

33
00:03:21,880 --> 00:03:23,880
 "Ehi" is actually imperative.

34
00:03:23,880 --> 00:03:27,880
 "Ehi passika" is something that you can say about it.

35
00:03:27,880 --> 00:03:29,880
 "Come and see".

36
00:03:29,880 --> 00:03:35,880
 Something that invites you to come and see.

37
00:03:35,880 --> 00:03:37,880
 Something about what you can say.

38
00:03:37,880 --> 00:03:43,880
 This is "commenseable".

39
00:03:43,880 --> 00:03:47,880
 It's interesting that that becomes a word because

40
00:03:47,880 --> 00:03:49,880
 this is something that the Buddha says often.

41
00:03:49,880 --> 00:03:52,880
 "Eta passata"

42
00:03:52,880 --> 00:03:54,880
 And so the Dhamma of the Buddha verse is

43
00:03:54,880 --> 00:03:56,880
 "Eta passati mong lokang"

44
00:03:56,880 --> 00:03:58,880
 "Come look at this world"

45
00:03:58,880 --> 00:04:01,880
 "Jitangrajara tu pamang"

46
00:04:01,880 --> 00:04:11,880
 That is decked out in the mind like a king's chariot.

47
00:04:11,880 --> 00:04:13,880
 It's all...

48
00:04:13,880 --> 00:04:16,880
 It appears to the mind to be bejeweled.

49
00:04:16,880 --> 00:04:19,880
 To be golden. To be royal.

50
00:04:19,880 --> 00:04:24,880
 Like a royal chariot all decked out.

51
00:04:24,880 --> 00:04:28,880
 "Yatambhala viseedanti"

52
00:04:28,880 --> 00:04:34,880
 This world which the fool sinks into.

53
00:04:34,880 --> 00:04:36,880
 "Viseedati"

54
00:04:36,880 --> 00:04:40,880
 Becomes mired in.

55
00:04:40,880 --> 00:04:47,880
 "Nati sango vijanatam"

56
00:04:47,880 --> 00:04:50,880
 But in regards to which

57
00:04:50,880 --> 00:04:57,880
 there is no association by the wise.

58
00:04:57,880 --> 00:04:59,880
 "Vijanata"

59
00:04:59,880 --> 00:05:05,880
 "Vijanatam"

60
00:05:05,880 --> 00:05:08,880
 People who see clearly or understand who have

61
00:05:08,880 --> 00:05:14,880
 "Vidpassana" or "Vijanati"

62
00:05:14,880 --> 00:05:17,880
 "Vijanati" is an interesting word.

63
00:05:17,880 --> 00:05:19,880
 To know clearly.

64
00:05:19,880 --> 00:05:21,880
 "Vipassana" means to secretly.

65
00:05:21,880 --> 00:05:24,880
 "Vijanati" means to know clearly.

66
00:05:24,880 --> 00:05:26,880
 It's the same thing.

67
00:05:26,880 --> 00:05:29,880
 It means wisdom.

68
00:05:29,880 --> 00:05:34,880
 So fools, bala, become enmeshed and mired in this world

69
00:05:34,880 --> 00:05:38,880
 because it's shiny.

70
00:05:38,880 --> 00:05:44,880
 But there is no association with the world

71
00:05:44,880 --> 00:05:46,880
 for those who see clearly.

72
00:05:46,880 --> 00:05:48,880
 No clearly.

73
00:05:48,880 --> 00:05:50,880
 That's not our verse today.

74
00:05:50,880 --> 00:05:53,880
 That's not the passage that we're looking at today.

75
00:05:53,880 --> 00:05:55,880
 But it's interesting.

76
00:05:55,880 --> 00:05:59,880
 Sort of apropos.

77
00:05:59,880 --> 00:06:03,880
 So let's switch and look at our verse here.

78
00:06:03,880 --> 00:06:28,880
 Our verse today is from the "Anguttar Nikaya"

79
00:06:28,880 --> 00:06:38,880
 It's actually a set of verses.

80
00:06:38,880 --> 00:06:40,880
 Sorry, a set of passages.

81
00:06:40,880 --> 00:06:43,880
 Five passages.

82
00:06:43,880 --> 00:06:46,880
 So the quote that we have on our page

83
00:06:46,880 --> 00:06:52,880
 doesn't adequately represent what's actually going on.

84
00:06:52,880 --> 00:06:58,880
 This isn't actually the Buddha saying to the monks, "Come."

85
00:06:58,880 --> 00:07:00,880
 What he says is

86
00:07:00,880 --> 00:07:05,880
 "Yete ananda bhikkhu na wah ajirapabhajita

87
00:07:05,880 --> 00:07:10,880
 adhunagata imangtamavindayam"

88
00:07:10,880 --> 00:07:16,880
 Whatever monk is newly gone forth

89
00:07:16,880 --> 00:07:23,880
 "Adhunagata"

90
00:07:23,880 --> 00:07:26,880
 have recently come

91
00:07:26,880 --> 00:07:31,880
 imangtamavindayam to this dharmavindayam

92
00:07:31,880 --> 00:07:33,880
 "Te wo ananda"

93
00:07:33,880 --> 00:07:36,880
 "Te bayu ananda"

94
00:07:36,880 --> 00:07:38,880
 "Te wo ananda bhikkhu"

95
00:07:38,880 --> 00:07:42,880
 Those bhikkhus ananda by you

96
00:07:42,880 --> 00:07:48,880
 "Samadapetaba" should be enjoined,

97
00:07:48,880 --> 00:07:52,880
 should be incited, should be rallied,

98
00:07:52,880 --> 00:07:55,880
 should be exhorted

99
00:07:55,880 --> 00:08:00,880
 "Panchasu damis" in regards to five dhammas.

100
00:08:00,880 --> 00:08:02,880
 So the neat thing about this quote is there's actually five

101
00:08:02,880 --> 00:08:02,880
 things

102
00:08:02,880 --> 00:08:07,880
 that's part of a bigger set.

103
00:08:07,880 --> 00:08:10,880
 You just take that quote out of context

104
00:08:10,880 --> 00:08:13,880
 and actually it's a little bit, well it's a good one,

105
00:08:13,880 --> 00:08:17,880
 it's an important one, but there's five aspects to this.

106
00:08:17,880 --> 00:08:19,880
 It's why you can't really easily quote the Buddhas

107
00:08:19,880 --> 00:08:22,880
 because you have to understand the context

108
00:08:22,880 --> 00:08:27,510
 and understand that it's much deeper than just quoting

109
00:08:27,510 --> 00:08:29,880
 something.

110
00:08:29,880 --> 00:08:32,880
 In this case there's five parts.

111
00:08:32,880 --> 00:08:36,570
 It says five things new, you should tell people who come

112
00:08:36,570 --> 00:08:36,880
 new.

113
00:08:36,880 --> 00:08:39,880
 So as with much of the Buddha's teachings

114
00:08:39,880 --> 00:08:45,100
 it's talking about the monks, but it fits just as well with

115
00:08:45,100 --> 00:08:45,880
 lay people,

116
00:08:45,880 --> 00:08:53,880
 people who have come to meditate for the first time.

117
00:08:53,880 --> 00:09:00,880
 They should also be exhorted with these sorts of things.

118
00:09:00,880 --> 00:09:04,880
 And which five?

119
00:09:04,880 --> 00:09:09,880
 So Samadapeta bah, they should be enjoined

120
00:09:09,880 --> 00:09:13,880
 or they should be caused to take up.

121
00:09:13,880 --> 00:09:17,880
 Samadapeta bah, you should encourage them to take up.

122
00:09:17,880 --> 00:09:20,880
 Niwesetaba, you should establish them

123
00:09:20,880 --> 00:09:25,880
 and encourage them to become settled in these things.

124
00:09:25,880 --> 00:09:30,030
 But itapeta bah, you should cause them to be established in

125
00:09:30,030 --> 00:09:30,880
 these things,

126
00:09:30,880 --> 00:09:36,880
 well-distablished and to take up these things.

127
00:09:36,880 --> 00:09:38,880
 Which five?

128
00:09:38,880 --> 00:09:41,880
 And then he gives five quotes.

129
00:09:41,880 --> 00:09:48,880
 eta tum heya uso, come you friend.

130
00:09:48,880 --> 00:09:56,880
 Si lavaah uta, be moral, be ethical.

131
00:09:56,880 --> 00:10:03,430
 Patimoka samwarasamurta, we haraata, dwell, guarding the

132
00:10:03,430 --> 00:10:03,880
 rules,

133
00:10:03,880 --> 00:10:05,880
 guarding ethical precepts.

134
00:10:05,880 --> 00:10:08,880
 Amongst them we got lots of rules, the patimoka,

135
00:10:08,880 --> 00:10:11,880
 that's what it's referring to, but you can adapt it.

136
00:10:11,880 --> 00:10:14,880
 It's just be ethical.

137
00:10:14,880 --> 00:10:20,880
 ajara go, jara sampana, know your pasture,

138
00:10:20,880 --> 00:10:24,880
 know what is your pasture and what is not.

139
00:10:24,880 --> 00:10:33,880
 ajara go, jara, ajara is, be mindful of, ajara means

140
00:10:33,880 --> 00:10:34,880
 conduct.

141
00:10:34,880 --> 00:10:37,880
 Go, jara means pasture.

142
00:10:37,880 --> 00:10:41,880
 Go is a cow, so go, jara is a place that a cow goes,

143
00:10:41,880 --> 00:10:43,880
 it literally means pasture.

144
00:10:43,880 --> 00:10:50,880
 But it's adapted to mean your limits or your boundaries,

145
00:10:50,880 --> 00:10:54,490
 so know where it's proper to go, where it's not proper to

146
00:10:54,490 --> 00:10:54,880
 go,

147
00:10:54,880 --> 00:10:58,030
 what it's proper to do, the sorts of people it's proper to

148
00:10:58,030 --> 00:10:58,880
 hang out with,

149
00:10:58,880 --> 00:11:04,880
 proper to seek advice from, that kind of thing.

150
00:11:04,880 --> 00:11:11,880
 anumathesu, wadhesu bia dasa'vino, dwell as one who sees

151
00:11:11,880 --> 00:11:12,880
 the danger,

152
00:11:12,880 --> 00:11:17,880
 bia dasa'vino, dasa'vino, who sees the danger.

153
00:11:17,880 --> 00:11:23,270
 anumathesu, wadhesu, in the smallest of faults, the

154
00:11:23,270 --> 00:11:24,880
 smallest of faults,

155
00:11:24,880 --> 00:11:30,880
 see the danger in the smallest of wrong.

156
00:11:30,880 --> 00:11:41,380
 samadaya sikatha sikapadesu, train yourselves in the

157
00:11:41,380 --> 00:11:43,880
 precepts,

158
00:11:43,880 --> 00:11:46,880
 having accepted them.

159
00:11:46,880 --> 00:11:50,880
 So the first thing he says is be ethical,

160
00:11:50,880 --> 00:11:58,880
 be aware of the proper conduct, behavior, speech of a monk.

161
00:11:58,880 --> 00:12:00,880
 I mean, he's talking about monks.

162
00:12:00,880 --> 00:12:04,880
 What is proper behavior for a monk?

163
00:12:04,880 --> 00:12:06,880
 Understand that.

164
00:12:06,880 --> 00:12:08,880
 He goes for meditators as well.

165
00:12:08,880 --> 00:12:11,880
 When you come to do a meditation course,

166
00:12:11,880 --> 00:12:14,970
 you have to know what is proper etiquette in a monastery,

167
00:12:14,970 --> 00:12:15,880
 in a meditation center,

168
00:12:15,880 --> 00:12:19,150
 and just in general, because we're at a much higher

169
00:12:19,150 --> 00:12:20,880
 standard as Buddhists.

170
00:12:20,880 --> 00:12:28,880
 Buddhism isn't a practice for mediocrity.

171
00:12:28,880 --> 00:12:36,880
 Buddhists are and should be held to a higher standard,

172
00:12:36,880 --> 00:12:40,880
 just because of calling themselves Buddhists.

173
00:12:40,880 --> 00:12:44,610
 Once you call yourself Buddhist or you follow the Buddhist

174
00:12:44,610 --> 00:12:44,880
 way,

175
00:12:44,880 --> 00:12:48,430
 you have to be held to a higher standard because that's all

176
00:12:48,430 --> 00:12:48,880
 it is.

177
00:12:48,880 --> 00:12:50,880
 That's what being a Buddhist means.

178
00:12:50,880 --> 00:12:54,880
 It's not about faith or belief or names or anything.

179
00:12:54,880 --> 00:13:03,880
 You're a Buddhist if you cultivate higher things.

180
00:13:03,880 --> 00:13:04,880
 That's the first one.

181
00:13:04,880 --> 00:13:06,880
 The second one is the one we have in the quote.

182
00:13:06,880 --> 00:13:14,320
 "Eta tum heya uso kam o friend, indre so guta dvara vihar

183
00:13:14,320 --> 00:13:14,880
ata."

184
00:13:14,880 --> 00:13:22,880
 Dwell as one who has guarded the doors of the senses.

185
00:13:22,880 --> 00:13:30,880
 Guta dvara with doors guarded, indre so of the senses.

186
00:13:30,880 --> 00:13:33,880
 So the senses are doors.

187
00:13:33,880 --> 00:13:35,880
 Guard them.

188
00:13:35,880 --> 00:13:37,880
 This is how mindfulness works.

189
00:13:37,880 --> 00:13:39,880
 The Buddha talks often about this.

190
00:13:39,880 --> 00:13:41,880
 It works as a guard.

191
00:13:41,880 --> 00:13:43,880
 Like if you have a gate to a city,

192
00:13:43,880 --> 00:13:47,880
 you only let in people who are trustworthy.

193
00:13:47,880 --> 00:13:49,880
 In the same way, it's like a filter.

194
00:13:49,880 --> 00:13:52,520
 So in the same way, mindfulness stands at the door of the

195
00:13:52,520 --> 00:13:52,880
 senses

196
00:13:52,880 --> 00:13:56,880
 and only lets in the seeing, the hearing, the smelling.

197
00:13:56,880 --> 00:13:59,880
 It doesn't let in the defilements.

198
00:13:59,880 --> 00:14:03,880
 It doesn't let the defilements rise.

199
00:14:03,880 --> 00:14:07,880
 It doesn't let in any of the baggage.

200
00:14:07,880 --> 00:14:12,170
 All that it lets come to the consciousness is the

201
00:14:12,170 --> 00:14:12,880
 experience.

202
00:14:12,880 --> 00:14:17,880
 So it creates objectivity. That's the idea.

203
00:14:17,880 --> 00:14:23,880
 Arakasattino with the guard of mindfulness.

204
00:14:23,880 --> 00:14:25,880
 Nipaka satino.

205
00:14:25,880 --> 00:14:28,880
 Nipaka is an interesting word. I don't really know it.

206
00:14:28,880 --> 00:14:36,880
 Nipaka means infused, boiled.

207
00:14:36,880 --> 00:14:38,880
 I'm not quite sure about that.

208
00:14:38,880 --> 00:14:40,880
 Oh wait, there's a variant.

209
00:14:40,880 --> 00:14:43,880
 Nipaka.

210
00:14:43,880 --> 00:14:48,880
 Let's look at nipaka.

211
00:14:48,880 --> 00:14:52,880
 We all have a poly lesson tonight.

212
00:14:52,880 --> 00:14:53,880
 Prudence.

213
00:14:53,880 --> 00:14:55,880
 Careful mindfulness.

214
00:14:55,880 --> 00:14:59,880
 Nipaka is a strange one.

215
00:14:59,880 --> 00:15:05,880
 Nipaka, from nipaka means carefulness.

216
00:15:05,880 --> 00:15:07,880
 Careful mindfulness.

217
00:15:07,880 --> 00:15:16,880
 One of the mindfulness and caution.

218
00:15:16,880 --> 00:15:23,380
 So this idea that mindfulness works or functions through

219
00:15:23,380 --> 00:15:24,880
 caution.

220
00:15:24,880 --> 00:15:27,880
 Caution is mindfulness. Mindfulness is caution.

221
00:15:27,880 --> 00:15:33,880
 It means approaching things carefully, meticulously.

222
00:15:33,880 --> 00:15:47,880
 It's mindful.

223
00:15:47,880 --> 00:15:49,880
 And then he launches into this.

224
00:15:49,880 --> 00:15:57,880
 That's really part of the quote.

225
00:15:57,880 --> 00:15:59,880
 Sara-kitamanasa, satara-keina-jeta-sa, samana-hagata.

226
00:15:59,880 --> 00:16:12,360
 Be one who has, who is accomplished or has attained a mind

227
00:16:12,360 --> 00:16:16,880
 guarded by mindfulness.

228
00:16:16,880 --> 00:16:21,500
 With a guarded mind, become one who has a mind guarded with

229
00:16:21,500 --> 00:16:22,880
 mindfulness.

230
00:16:22,880 --> 00:16:25,290
 It's a bit awkward in English. It's just the way they speak

231
00:16:25,290 --> 00:16:25,880
 in Pali.

232
00:16:25,880 --> 00:16:27,880
 And it's the way the Buddha would speak.

233
00:16:27,880 --> 00:16:33,080
 It's a rhetorical tool of repeating yourself in different

234
00:16:33,080 --> 00:16:33,880
 ways.

235
00:16:33,880 --> 00:16:39,490
 So, sara-kitamanasa means be one, if I'm reading this

236
00:16:39,490 --> 00:16:39,880
 correctly,

237
00:16:39,880 --> 00:16:45,880
 be one who has a mind that is well guarded.

238
00:16:45,880 --> 00:16:48,880
 Satara-keina-jeta-sa, samana-hagata.

239
00:16:48,880 --> 00:16:58,520
 Be one who goes with or who has a mind guarded by

240
00:16:58,520 --> 00:17:00,880
 mindfulness.

241
00:17:00,880 --> 00:17:09,880
 I think. Satara-keina, satyara-keina.

242
00:17:09,880 --> 00:17:12,880
 So they should be established in guarding the senses.

243
00:17:12,880 --> 00:17:15,880
 The first one is established in guarding the patimoka.

244
00:17:15,880 --> 00:17:20,880
 The second one is guarding the faculties.

245
00:17:20,880 --> 00:17:26,880
 And the third one, etatum heavuso apabhasahota.

246
00:17:26,880 --> 00:17:30,880
 Be one who talks a little. Apabhasah.

247
00:17:30,880 --> 00:17:32,880
 One who talks only a little.

248
00:17:32,880 --> 00:17:42,880
 Bhase paryantakarino. One who makes an end to speech,

249
00:17:42,880 --> 00:17:46,880
 or knows the end to speech, maybe.

250
00:17:46,880 --> 00:17:51,880
 Paryantakarino, restricted, who has limited speech.

251
00:17:51,880 --> 00:17:53,880
 There you go. The Buddha actually does say,

252
00:17:53,880 --> 00:17:59,880
 Bhase paryantakarino, one who limits one's speech.

253
00:17:59,880 --> 00:18:05,880
 Puts a boundary on one's speech. That's what I mean.

254
00:18:05,880 --> 00:18:10,880
 So bhase paryantakarino, one who limits one's speech,

255
00:18:10,880 --> 00:18:15,880
 should be limited to the Dhamma, to good things.

256
00:18:15,880 --> 00:18:17,880
 Eta-tumeya, number four.

257
00:18:17,880 --> 00:18:22,410
 Eta-tumeya-vaso aranyika-huta. Be one who dwells in the

258
00:18:22,410 --> 00:18:22,880
 forest.

259
00:18:22,880 --> 00:18:30,880
 Aranyavanna-patani, pandani-sena-sanani, vatiseyvata.

260
00:18:30,880 --> 00:18:40,880
 Vatiseyvata means use or indulgence in, makes use of,

261
00:18:40,880 --> 00:18:47,880
 or makes use of dwellings that are secluded in the forest.

262
00:18:47,880 --> 00:18:53,880
 Aranyika, be one who dwells in the forest.

263
00:18:53,880 --> 00:19:08,880
 In general, kaya-upakase, suclusions, upakase.

264
00:19:08,880 --> 00:19:13,880
 Be one, so you should exhort them in bodily seclusion,

265
00:19:13,880 --> 00:19:15,880
 seclusion of the body.

266
00:19:15,880 --> 00:19:17,880
 Because there's two kinds of seclusion.

267
00:19:17,880 --> 00:19:20,430
 There's seclusion of the mind and there's seclusion of the

268
00:19:20,430 --> 00:19:20,880
 body.

269
00:19:20,880 --> 00:19:24,010
 Seclusion of the mind is most important. That's through

270
00:19:24,010 --> 00:19:25,880
 meditation practice.

271
00:19:25,880 --> 00:19:28,880
 But seclusion of the body is important for the practice.

272
00:19:28,880 --> 00:19:31,630
 That's why people come here and we just put them in their

273
00:19:31,630 --> 00:19:31,880
 room

274
00:19:31,880 --> 00:19:34,880
 and leave them for all day on their own.

275
00:19:34,880 --> 00:19:37,880
 Because the idea is to be secluded.

276
00:19:37,880 --> 00:19:41,420
 It's important from a physical perspective as well, to se

277
00:19:41,420 --> 00:19:42,880
clude yourself.

278
00:19:42,880 --> 00:19:47,380
 It's like in a lab, not so that you can run away from your

279
00:19:47,380 --> 00:19:47,880
 problems,

280
00:19:47,880 --> 00:19:52,000
 but so that you can get into lab mode and learn about

281
00:19:52,000 --> 00:19:56,880
 yourself, study yourself.

282
00:19:56,880 --> 00:19:58,880
 Number four, number five,

283
00:19:58,880 --> 00:20:02,880
 eita tumheauso samaditika,

284
00:20:02,880 --> 00:20:08,880
 hoda, be one who has right view, samadasanina,

285
00:20:08,880 --> 00:20:13,880
 be one who sees rightly samanagata,

286
00:20:13,880 --> 00:20:21,880
 one who is accomplished in right vision and right view.

287
00:20:21,880 --> 00:20:34,880
 In right view I've talked about it last Saturday.

288
00:20:34,880 --> 00:20:38,880
 Right view, there are five types of right view actually.

289
00:20:38,880 --> 00:20:41,550
 I didn't mention this last time. I talked about it a bit

290
00:20:41,550 --> 00:20:41,880
 differently.

291
00:20:41,880 --> 00:20:44,570
 There's the right view of karma, the responsibility of

292
00:20:44,570 --> 00:20:44,880
 karma.

293
00:20:44,880 --> 00:20:52,880
 There's right view of jhana, right view in regards to jhana

294
00:20:52,880 --> 00:20:52,880
,

295
00:20:52,880 --> 00:20:55,880
 in regards to tranquility meditation.

296
00:20:55,880 --> 00:21:00,120
 Vipassana samaditi, right view of insight, right view about

297
00:21:00,120 --> 00:21:00,880
 the path,

298
00:21:00,880 --> 00:21:02,880
 right view about the fruition.

299
00:21:02,880 --> 00:21:06,880
 I'm not so keen on that distinction of right view,

300
00:21:06,880 --> 00:21:09,880
 but right view means to see clearly.

301
00:21:09,880 --> 00:21:12,880
 It's really the core of Theravada Buddhism,

302
00:21:12,880 --> 00:21:19,790
 wisdom, to cultivate understanding, understand things as

303
00:21:19,790 --> 00:21:20,880
 they are.

304
00:21:20,880 --> 00:21:24,880
 Anyway, so that's the dhamma for tonight.

305
00:21:24,880 --> 00:21:28,880
 The quote was specifically about one of the five,

306
00:21:28,880 --> 00:21:34,150
 but all together it puts together sort of a nice guide for

307
00:21:34,150 --> 00:21:35,880
 training new monks,

308
00:21:35,880 --> 00:21:37,880
 new meditators.

309
00:21:37,880 --> 00:21:41,480
 They should be mindful, but they should also be ethical

310
00:21:41,480 --> 00:21:41,880
 first,

311
00:21:41,880 --> 00:21:44,880
 and then mindful.

312
00:21:44,880 --> 00:21:48,880
 They should...

313
00:21:48,880 --> 00:21:50,880
 What was number three?

314
00:21:50,880 --> 00:21:52,880
 They should speak only a little.

315
00:21:52,880 --> 00:21:56,880
 They should be secluded physically,

316
00:21:56,880 --> 00:21:59,880
 and they should be instructed in right view,

317
00:21:59,880 --> 00:22:05,880
 or they should be encouraged to cultivate right view.

318
00:22:05,880 --> 00:22:08,210
 So focused on that, really, that's the focus of our

319
00:22:08,210 --> 00:22:08,880
 meditation.

320
00:22:08,880 --> 00:22:12,160
 The meditation isn't just to calm down, it isn't just to

321
00:22:12,160 --> 00:22:12,880
 relax.

322
00:22:12,880 --> 00:22:14,880
 You're focused on trying to see clearly.

323
00:22:14,880 --> 00:22:21,550
 That's an important aim for us to focus on in our

324
00:22:21,550 --> 00:22:23,880
 meditation.

325
00:22:23,880 --> 00:22:30,880
 Okay, so that's the dhamma for tonight.

326
00:22:30,880 --> 00:22:32,880
 I'll post the hangout.

327
00:22:32,880 --> 00:22:36,880
 Come on if you want to ask a question.

328
00:22:36,880 --> 00:22:49,880
 If you just want to say hi, you can come on, I suppose.

329
00:22:49,880 --> 00:22:57,880
 I'm going to pile on, I don't mind.

330
00:22:57,880 --> 00:23:00,450
 But if you have a question, please come on, you're welcome

331
00:23:00,450 --> 00:23:00,880
 to join.

332
00:23:00,880 --> 00:23:07,880
 We get consistently 25-ish viewers on YouTube.

333
00:23:07,880 --> 00:23:10,880
 This is good.

334
00:23:10,880 --> 00:23:13,880
 It means we've got a small community here.

335
00:23:13,880 --> 00:23:15,880
 Hello, Bonsie.

336
00:23:15,880 --> 00:23:17,880
 Hi, Simon.

337
00:23:17,880 --> 00:23:21,880
 My sound is not good.

338
00:23:21,880 --> 00:23:38,880
 Let me just...

339
00:23:38,880 --> 00:23:42,880
 There are no questions.

340
00:23:42,880 --> 00:23:54,880
 I just wish you all a good night.

341
00:23:54,880 --> 00:23:56,880
 Hello, Bonsie.

342
00:23:56,880 --> 00:23:58,880
 Hi. Do you have a question?

343
00:23:58,880 --> 00:24:00,880
 No, I just want to say hello.

344
00:24:00,880 --> 00:24:03,880
 Okay.

345
00:24:03,880 --> 00:24:05,880
 Thank you for the dhamma talk today.

346
00:24:05,880 --> 00:24:06,880
 That was a wonderful quote.

347
00:24:06,880 --> 00:24:09,880
 Yeah.

348
00:24:09,880 --> 00:24:13,570
 Which one did you think was wonderful, the quote or the

349
00:24:13,570 --> 00:24:14,880
 whole passage?

350
00:24:14,880 --> 00:24:18,900
 I mean, the whole passage really, but the quote really

351
00:24:18,900 --> 00:24:21,880
 struck me as well, just the two lines there.

352
00:24:21,880 --> 00:24:24,930
 I wasn't all that struck by it. I found it kind of

353
00:24:24,930 --> 00:24:25,880
 confusing.

354
00:24:25,880 --> 00:24:27,880
 Oh, how was that?

355
00:24:27,880 --> 00:24:31,980
 Well, watchfully mindful, carefully mindful with the ways

356
00:24:31,980 --> 00:24:33,880
 of the mind well watched.

357
00:24:33,880 --> 00:24:37,110
 Maybe not confusing, but I just knew right away it wasn't

358
00:24:37,110 --> 00:24:38,880
 quite what was being said.

359
00:24:38,880 --> 00:24:42,880
 It's...

360
00:24:42,880 --> 00:24:48,000
 Because he starts off being watchfully mindful and then

361
00:24:48,000 --> 00:24:51,880
 watching the ways of the mind,

362
00:24:51,880 --> 00:24:55,580
 with the ways of the mind well watched, possessed of a mind

363
00:24:55,580 --> 00:24:55,880
.

364
00:24:55,880 --> 00:24:58,880
 So it's saying two different things.

365
00:24:58,880 --> 00:25:02,460
 First, you're watching the mind and then you've got the

366
00:25:02,460 --> 00:25:03,880
 mind that's watching,

367
00:25:03,880 --> 00:25:05,880
 which it is not really wrong.

368
00:25:05,880 --> 00:25:09,750
 That's why I went to the Pali because I wanted to see what

369
00:25:09,750 --> 00:25:11,880
's really being said here.

370
00:25:11,880 --> 00:25:15,640
 I mean, it's important to frame it in regards to the senses

371
00:25:15,640 --> 00:25:16,880
, which he does.

372
00:25:16,880 --> 00:25:20,880
 The quote does say guarding the senses.

373
00:25:20,880 --> 00:25:24,940
 But it's arakasatino, nipakasatino, which is just this

374
00:25:24,940 --> 00:25:28,250
 rhetorical tool of repeating the same thing in different

375
00:25:28,250 --> 00:25:28,880
 words

376
00:25:28,880 --> 00:25:36,880
 with mindfulness as a guard, with mindfulness in care,

377
00:25:36,880 --> 00:25:41,800
 or being careful, with careful mindfulness, with guarding

378
00:25:41,800 --> 00:25:43,880
 mindfulness.

379
00:25:43,880 --> 00:25:49,780
 With a mind well guarded and then with a mind that is

380
00:25:49,780 --> 00:25:51,880
 guarded by mindfulness.

381
00:25:51,880 --> 00:25:58,880
 I'm not really sure about that translation as usual.

382
00:25:58,880 --> 00:25:59,880
 That makes sense.

383
00:25:59,880 --> 00:26:03,200
 Yeah, so actually the thing that really struck me was like

384
00:26:03,200 --> 00:26:04,880
 just the first line there,

385
00:26:04,880 --> 00:26:08,880
 "Come, live with the nose of the senses guarded."

386
00:26:08,880 --> 00:26:11,880
 As soon as I read that, it was like, that's a nice quote.

387
00:26:11,880 --> 00:26:15,880
 But again, definitely understand what you meant there.

388
00:26:15,880 --> 00:26:18,880
 So, I mean, you see, watchfully mindful isn't correct.

389
00:26:18,880 --> 00:26:19,880
 I mean, it's nice.

390
00:26:19,880 --> 00:26:22,570
 That part I really like actually, but it's not watchfully

391
00:26:22,570 --> 00:26:22,880
 mindful.

392
00:26:22,880 --> 00:26:27,880
 Watchfully arakasatino, it's not watchfully, it's guard.

393
00:26:27,880 --> 00:26:33,880
 Arak means to guard or to maybe to care for, you could say,

394
00:26:33,880 --> 00:26:37,880
 but maybe he's doing okay.

395
00:26:37,880 --> 00:26:38,880
 I mean, it's all fine.

396
00:26:38,880 --> 00:26:40,880
 He's certainly got license.

397
00:26:40,880 --> 00:26:43,880
 There's nothing wrong with changing it around.

398
00:26:43,880 --> 00:26:46,880
 I like going to the Pali to find exactly what's being said.

399
00:26:46,880 --> 00:26:49,490
 That's why it's nice to learn the original because you get

400
00:26:49,490 --> 00:26:50,880
 a taste of what the Buddha

401
00:26:50,880 --> 00:26:53,880
 is actually saying.

402
00:26:53,880 --> 00:26:58,880
 Yeah, there's no replacement for the original.

403
00:26:58,880 --> 00:26:59,880
 That's true.

404
00:26:59,880 --> 00:27:06,880
 It's always so wonderful to get the Pali.

405
00:27:06,880 --> 00:27:09,880
 I just got thrown off by the second half of the translation

406
00:27:09,880 --> 00:27:15,880
 because it's a bit wonky.

407
00:27:15,880 --> 00:27:16,880
 I see.

408
00:27:16,880 --> 00:27:20,880
 Well, thanks for making that clear as well.

409
00:27:20,880 --> 00:27:26,880
 And five points of right view as well for adding them in.

410
00:27:26,880 --> 00:27:27,880
 Okay.

411
00:27:27,880 --> 00:27:28,880
 Anyway, good night, everyone.

412
00:27:28,880 --> 00:27:29,880
 Good night, Simon.

413
00:27:29,880 --> 00:27:30,880
 Good night.

414
00:27:30,880 --> 00:27:31,880
 Thank you.

415
00:27:31,880 --> 00:27:32,880
 Bye.

416
00:27:32,880 --> 00:27:32,880
 Bye.

417
00:27:32,880 --> 00:27:33,880
 Thank you.

418
00:27:33,880 --> 00:27:34,880
 Bye.

419
00:27:34,880 --> 00:27:34,880
 Bye.

420
00:27:34,880 --> 00:27:35,880
 Bye.

421
00:27:36,880 --> 00:27:36,880
 Bye.

422
00:27:36,880 --> 00:27:37,880
 Bye.

423
00:27:37,880 --> 00:27:38,880
 Bye.

424
00:27:38,880 --> 00:27:39,880
 Bye.

425
00:27:39,880 --> 00:27:40,880
 Bye.

426
00:27:40,880 --> 00:27:41,880
 Bye.

427
00:27:41,880 --> 00:27:42,880
 Bye.

428
00:27:42,880 --> 00:27:43,880
 Bye.

429
00:27:43,880 --> 00:27:44,880
 Bye.

430
00:27:44,880 --> 00:27:45,880
 Bye.

431
00:27:45,880 --> 00:27:46,880
 Bye.

432
00:27:46,880 --> 00:27:47,880
 Bye.

433
00:27:47,880 --> 00:27:48,880
 Bye.

434
00:27:48,880 --> 00:27:49,880
 Bye.

