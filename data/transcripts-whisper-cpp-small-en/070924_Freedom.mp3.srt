1
00:00:00,000 --> 00:00:12,930
 Today I'd like to talk a little bit more about the Buddhist

2
00:00:12,930 --> 00:00:15,000
 teaching on freedom.

3
00:00:15,000 --> 00:00:28,300
 In the meditation practice that we follow, our goal is to

4
00:00:28,300 --> 00:00:32,000
 attain freedom.

5
00:00:32,000 --> 00:00:36,420
 And for meditators who come for a short time, who have time

6
00:00:36,420 --> 00:00:39,000
 enough to finish the foundation course,

7
00:00:39,000 --> 00:00:45,550
 time even to finish the advanced course, we expect that

8
00:00:45,550 --> 00:00:51,430
 they should at the very least be able to get a taste of

9
00:00:51,430 --> 00:00:53,000
 freedom,

10
00:00:53,000 --> 00:01:02,140
 that they should be able to realize for themselves what it

11
00:01:02,140 --> 00:01:12,000
 means to be free, even on a small scale, even just a taste.

12
00:01:12,000 --> 00:01:17,010
 And so it's important to understand what we mean by the

13
00:01:17,010 --> 00:01:20,000
 word "freedom" in Buddhism.

14
00:01:20,000 --> 00:01:24,100
 Of course when we hear the word "freedom" in the West, or

15
00:01:24,100 --> 00:01:28,000
 in the modern world, throughout the world, really,

16
00:01:28,000 --> 00:01:34,000
 we generally think about freedom too.

17
00:01:34,000 --> 00:01:42,050
 Freedom to say, freedom to act, freedom to think, in

18
00:01:42,050 --> 00:01:48,130
 whatever way we like, in whatever way appeals to us, in

19
00:01:48,130 --> 00:01:52,000
 whatever way we think fit.

20
00:01:52,000 --> 00:01:56,020
 And so we think being truly free is being able to think

21
00:01:56,020 --> 00:02:00,070
 whatever it is you intend to think, holding whatever views

22
00:02:00,070 --> 00:02:05,250
 you think appropriate, thinking whatever thoughts you think

23
00:02:05,250 --> 00:02:07,000
 appropriate,

24
00:02:07,000 --> 00:02:11,140
 and not having anyone else tell you that you're wrong, or

25
00:02:11,140 --> 00:02:17,600
 not have anyone else try to stop you from thinking and

26
00:02:17,600 --> 00:02:21,000
 believing what you will.

27
00:02:21,000 --> 00:02:26,270
 And we think of freedom as saying whatever it is that you

28
00:02:26,270 --> 00:02:28,000
 like to say.

29
00:02:28,000 --> 00:02:34,740
 We feel free when we speak that we can say whatever we want

30
00:02:34,740 --> 00:02:40,820
 without any punishment, whatever we want to say, we can say

31
00:02:40,820 --> 00:02:45,000
, "And this is freedom."

32
00:02:45,000 --> 00:02:55,000
 Or we can do whatever we like to do.

33
00:02:55,000 --> 00:02:58,160
 And we think of this as a sort of a freedom, but if we look

34
00:02:58,160 --> 00:03:05,190
 at how it plays out, really, we, in the end, are none of us

35
00:03:05,190 --> 00:03:11,000
 free to do or say or think what we will.

36
00:03:11,000 --> 00:03:17,330
 That our thoughts and our speech and our actions are all

37
00:03:17,330 --> 00:03:23,970
 defined by an innate character, which is based on our likes

38
00:03:23,970 --> 00:03:29,000
 and our dislikes, our beliefs and our delusions and our

39
00:03:29,000 --> 00:03:36,320
 conditioning, social conditioning, parental conditioning,

40
00:03:36,320 --> 00:03:40,000
 religious conditioning.

41
00:03:40,000 --> 00:03:47,000
 And we find ourselves trapped in our freedom.

42
00:03:47,000 --> 00:03:50,890
 So we say, "I have the freedom to believe whatever I like

43
00:03:50,890 --> 00:03:55,700
 to believe," but actually our beliefs come from something

44
00:03:55,700 --> 00:03:59,000
 which we really have no control over.

45
00:03:59,000 --> 00:04:04,760
 Come from our parents or come from our upbringing, come

46
00:04:04,760 --> 00:04:10,000
 from our society, come from the media and so on.

47
00:04:10,000 --> 00:04:14,730
 They moreover come from our own desires and our own a

48
00:04:14,730 --> 00:04:16,000
versions.

49
00:04:16,000 --> 00:04:23,150
 When we want something, we create all sorts of beliefs as

50
00:04:23,150 --> 00:04:28,000
 to why we deserve to get what we want.

51
00:04:28,000 --> 00:04:33,000
 "I deserve to have this, I deserve to have that."

52
00:04:33,000 --> 00:04:38,460
 And when bad things come to us, we make all sorts of

53
00:04:38,460 --> 00:04:44,580
 beliefs and views as to why I don't deserve or my suffering

54
00:04:44,580 --> 00:04:47,000
 is not justified.

55
00:04:47,000 --> 00:04:53,920
 We have all sorts of thoughts and views which are based on

56
00:04:53,920 --> 00:05:02,220
 our conditioning and our speech is based on finding ways to

57
00:05:02,220 --> 00:05:06,000
 get the things which bring us pleasure,

58
00:05:06,000 --> 00:05:18,000
 for how to remove the things which bring us displeasure.

59
00:05:18,000 --> 00:05:18,500
 When we see other people experiencing happiness and we feel

60
00:05:18,500 --> 00:05:19,000
 jealous or upset because they got better than we did

61
00:05:19,000 --> 00:05:22,550
 or we want the things that they have, we try to find ways

62
00:05:22,550 --> 00:05:28,030
 to disparage them, say bad things about them and hurt them

63
00:05:28,030 --> 00:05:30,000
 because we want what they have.

64
00:05:30,000 --> 00:05:35,340
 We want them to not have their position or their wealth or

65
00:05:35,340 --> 00:05:37,000
 their status.

66
00:05:37,000 --> 00:05:42,140
 And so we say and even do things which are often purely

67
00:05:42,140 --> 00:05:46,000
 based on our desires and our aversions.

68
00:05:46,000 --> 00:05:52,240
 We're chasing after sensual desires, chasing after objects

69
00:05:52,240 --> 00:05:58,480
 of pleasure, running away, chasing away, fighting off all

70
00:05:58,480 --> 00:06:03,000
 the time, things which are displeasing.

71
00:06:03,000 --> 00:06:06,570
 So we find ourselves not really free no matter how much we

72
00:06:06,570 --> 00:06:11,260
 say we should be free, that no one should tell us how to

73
00:06:11,260 --> 00:06:14,000
 act, how to speak, how to think.

74
00:06:14,000 --> 00:06:16,630
 And so in fact we see this is exactly what the Lord Buddha

75
00:06:16,630 --> 00:06:21,830
 did, is taught us how we should act, how we should speak

76
00:06:21,830 --> 00:06:25,000
 and how we should even think.

77
00:06:25,000 --> 00:06:31,960
 You explained that there were modes of behavior, modes of

78
00:06:31,960 --> 00:06:39,750
 speech and modes of thought which were unhelpful, which

79
00:06:39,750 --> 00:06:47,000
 were wrong, which should be abandoned.

80
00:06:47,000 --> 00:06:54,080
 The Lord Buddha taught, of course, freedom from, which is

81
00:06:54,080 --> 00:07:03,660
 freedom from evil, freedom from suffering, freedom from def

82
00:07:03,660 --> 00:07:07,000
ilement.

83
00:07:07,000 --> 00:07:10,970
 And if we look at it we can see this is a much more worthy

84
00:07:10,970 --> 00:07:15,400
 sort of freedom, that really all that all of us want is to

85
00:07:15,400 --> 00:07:19,790
 be free from any kind of suffering, any kind of upset, any

86
00:07:19,790 --> 00:07:23,000
 kind of displeasure whatsoever.

87
00:07:23,000 --> 00:07:26,840
 When we talk about happiness, really what we mean is just

88
00:07:26,840 --> 00:07:28,000
 to be at peace.

89
00:07:28,000 --> 00:07:32,710
 We don't ever, most of us think that we have to experience

90
00:07:32,710 --> 00:07:36,000
 extreme states of pleasure all the time.

91
00:07:36,000 --> 00:07:45,160
 We simply wish that we should not have to face unpleasant

92
00:07:45,160 --> 00:07:55,000
ness, pain and aching and upset and frustration and so on.

93
00:07:55,000 --> 00:08:02,440
 That we should be able to find some sort of peace and sort

94
00:08:02,440 --> 00:08:09,000
 of a constant state of happiness in our lives.

95
00:08:09,000 --> 00:08:16,000
 So the Lord Buddha taught freedom from suffering.

96
00:08:16,000 --> 00:08:21,330
 And he explained the reason why people suffer is because,

97
00:08:21,330 --> 00:08:26,000
 in essence, they don't understand suffering.

98
00:08:26,000 --> 00:08:29,000
 They don't understand the cause of suffering.

99
00:08:29,000 --> 00:08:33,440
 Just like why people suffer from obesity or people suffer

100
00:08:33,440 --> 00:08:37,450
 from heart disease or people suffer from various illnesses,

101
00:08:37,450 --> 00:08:43,000
 ailments, which really are curable or even preventable.

102
00:08:43,000 --> 00:08:46,880
 But because people don't understand these diseases, how to

103
00:08:46,880 --> 00:08:51,070
 treat them, how to cure them, how to act when one is

104
00:08:51,070 --> 00:08:54,000
 afflicted by this illness,

105
00:08:54,000 --> 00:08:56,940
 and because they don't understand what caused the illness

106
00:08:56,940 --> 00:09:00,890
 in the first place, they aren't able to change their

107
00:09:00,890 --> 00:09:02,000
 lifestyles.

108
00:09:02,000 --> 00:09:06,660
 You find people suffering from so many various illnesses

109
00:09:06,660 --> 00:09:10,000
 unable to get themselves out of the cycle.

110
00:09:10,000 --> 00:09:16,520
 Often simply because they don't understand and they don't

111
00:09:16,520 --> 00:09:21,000
 see the danger in what they are doing.

112
00:09:21,000 --> 00:09:25,960
 As far as suffering goes, the Lord Buddha saw that mostly

113
00:09:25,960 --> 00:09:31,070
 the way we look at suffering is simply as a painful feeling

114
00:09:31,070 --> 00:09:36,000
 or something temporary which we can remove.

115
00:09:36,000 --> 00:09:39,530
 We can see only so far as those unpleasant things. We see

116
00:09:39,530 --> 00:09:45,000
 this is suffering when we receive something unpleasant.

117
00:09:45,000 --> 00:09:53,930
 And because we can see only so far that it is an occurrence

118
00:09:53,930 --> 00:09:57,000
 which is visible,

119
00:09:57,000 --> 00:10:03,090
 we look at suffering as something which arises and is treat

120
00:10:03,090 --> 00:10:06,000
able by various means.

121
00:10:06,000 --> 00:10:08,990
 Because we don't understand that suffering is something

122
00:10:08,990 --> 00:10:11,000
 which is an intrinsic part of life.

123
00:10:11,000 --> 00:10:20,000
 Lord Buddha said, "This is a reason why we suffer."

124
00:10:20,000 --> 00:10:26,450
 We chase after all sorts of pleasant, pleasurable

125
00:10:26,450 --> 00:10:28,000
 sensations.

126
00:10:28,000 --> 00:10:33,800
 And whenever pain or suffering comes up, we immediately try

127
00:10:33,800 --> 00:10:42,750
 to find a way to remove the source of the thing which we

128
00:10:42,750 --> 00:10:45,000
 dislike.

129
00:10:45,000 --> 00:10:49,580
 When we see something unpleasant, we run away from it. When

130
00:10:49,580 --> 00:10:56,000
 we hear something unpleasant, we close our ears.

131
00:10:56,000 --> 00:10:59,170
 We always find ourselves running away from the cause of

132
00:10:59,170 --> 00:11:00,000
 suffering.

133
00:11:00,000 --> 00:11:02,930
 When we have a backache, we get a massage. When we have a

134
00:11:02,930 --> 00:11:05,000
 headache, we take a pill.

135
00:11:05,000 --> 00:11:12,220
 When we have a heartache, we take drugs or alcohol and find

136
00:11:12,220 --> 00:11:17,000
 ourselves indulging in all sorts of unwholesome behaviors

137
00:11:17,000 --> 00:11:23,260
 simply because we don't see that suffering is an intrinsic

138
00:11:23,260 --> 00:11:25,000
 part of life.

139
00:11:25,000 --> 00:11:32,610
 We look at people who aren't able to understand things like

140
00:11:32,610 --> 00:11:37,000
 meditation or understand how the mind works,

141
00:11:37,000 --> 00:11:41,770
 aren't able to understand why someone would want to train

142
00:11:41,770 --> 00:11:43,000
 their mind.

143
00:11:43,000 --> 00:11:51,000
 We see them chasing after pleasure, after pleasure.

144
00:11:51,000 --> 00:11:54,570
 We can see them performing all sorts of unwholesome deeds

145
00:11:54,570 --> 00:12:02,000
 and we see their minds becoming weak, becoming unstable.

146
00:12:02,000 --> 00:12:07,130
 We see these same people who will tell you how to live for

147
00:12:07,130 --> 00:12:11,000
 the moment and chase after pleasure in the here and now.

148
00:12:11,000 --> 00:12:17,350
 We see these same people running out to get therapists and

149
00:12:17,350 --> 00:12:24,080
 find a masseuse or a chiropractor or a doctor or someone to

150
00:12:24,080 --> 00:12:29,000
 get rid of their constant

151
00:12:29,000 --> 00:12:33,810
 ever-present suffering, the onslaught of so many various

152
00:12:33,810 --> 00:12:37,000
 different kinds of suffering which arise.

153
00:12:37,000 --> 00:12:40,890
 Even simply sitting still, you see, the average person is

154
00:12:40,890 --> 00:12:44,700
 unable to sit still for any long period of time unless they

155
00:12:44,700 --> 00:12:46,000
 have a very comfortable,

156
00:12:46,000 --> 00:12:50,000
 very plush, expensive chair to sit in.

157
00:12:50,000 --> 00:12:53,020
 And even then you watch them and after some time they

158
00:12:53,020 --> 00:12:55,610
 become bored, they become restless and they have to change

159
00:12:55,610 --> 00:12:56,000
 position.

160
00:12:56,000 --> 00:13:00,160
 They have to get up and go and chase after more pleasure.

161
00:13:00,160 --> 00:13:03,000
 And they're really never at a state of peace and happiness.

162
00:13:03,000 --> 00:13:07,450
 They're unable to sit still, unable to rest and so their

163
00:13:07,450 --> 00:13:11,560
 mind keeps chasing and chasing and chasing and running and

164
00:13:11,560 --> 00:13:13,000
 running and running.

165
00:13:13,000 --> 00:13:16,720
 The Lord Buddha taught that in the very end there's no one

166
00:13:16,720 --> 00:13:19,000
 who can run away from suffering,

167
00:13:19,000 --> 00:13:23,280
 there's no person who can run to a place where there is

168
00:13:23,280 --> 00:13:25,000
 only happiness.

169
00:13:25,000 --> 00:13:28,530
 In the end we all have to suffer through old age, sex and

170
00:13:28,530 --> 00:13:31,660
 death, no matter how much happiness or pleasure we might

171
00:13:31,660 --> 00:13:32,000
 find here and now.

172
00:13:32,000 --> 00:13:37,000
 In the end we have to leave it all behind.

173
00:13:37,000 --> 00:13:42,040
 And then we see old people on their deathbed crying, unable

174
00:13:42,040 --> 00:13:45,000
 to deal with unpleasantness,

175
00:13:45,000 --> 00:13:50,590
 unable to deal with the fear and the pain which arises at

176
00:13:50,590 --> 00:13:53,000
 the moment of death.

177
00:13:53,000 --> 00:13:56,400
 And it's really shameful to see because throughout their

178
00:13:56,400 --> 00:14:00,000
 whole life they were confronted by these very same

179
00:14:00,000 --> 00:14:00,000
 sensations

180
00:14:00,000 --> 00:14:02,660
 and they kept running and running away from them, seeing

181
00:14:02,660 --> 00:14:06,000
 that all they had to do was find a doctor or so on,

182
00:14:06,000 --> 00:14:10,090
 someone who could cure their illness and then they would be

183
00:14:10,090 --> 00:14:11,000
 happy again.

184
00:14:11,000 --> 00:14:15,680
 Never thinking that in the end it would come to defeat them

185
00:14:15,680 --> 00:14:19,000
, it would become so strong that there would be no way out.

186
00:14:19,000 --> 00:14:27,000
 In the end they would have to face death.

187
00:14:27,000 --> 00:14:33,760
 And so it takes something very special, takes a very strong

188
00:14:33,760 --> 00:14:36,000
 sense of understanding,

189
00:14:36,000 --> 00:14:39,880
 something which has perhaps been developed in lifetime

190
00:14:39,880 --> 00:14:45,000
 after lifetime after lifetime for a person to really think,

191
00:14:45,000 --> 00:14:50,650
 think beyond the ephemeral, think beyond the states of

192
00:14:50,650 --> 00:14:56,860
 happiness which arise and cease incessantly and are, none

193
00:14:56,860 --> 00:15:00,000
 of them really satisfying.

194
00:15:00,000 --> 00:15:08,870
 Think through to the point where we no longer wish to run

195
00:15:08,870 --> 00:15:14,000
 after these states of pleasure.

196
00:15:14,000 --> 00:15:16,730
 And we can see that in the end we have to leave them all

197
00:15:16,730 --> 00:15:19,000
 behind and they're really worthless.

198
00:15:19,000 --> 00:15:22,620
 They don't leave us as better people, happier or more

199
00:15:22,620 --> 00:15:24,000
 peaceful people.

200
00:15:24,000 --> 00:15:29,000
 In fact they create disturbance and upset in our minds.

201
00:15:29,000 --> 00:15:33,000
 And so to practice meditation.

202
00:15:33,000 --> 00:15:39,580
 For someone who has come to this point it's very special

203
00:15:39,580 --> 00:15:41,000
 and very hard to find.

204
00:15:41,000 --> 00:15:43,400
 It's something which is very encouraging to see that there

205
00:15:43,400 --> 00:15:45,000
 are still so many people in this world

206
00:15:45,000 --> 00:15:48,970
 who are interested and keen on coming to practice

207
00:15:48,970 --> 00:15:50,000
 meditation.

208
00:15:50,000 --> 00:15:58,190
 Seeing that these sensual pleasures out there are really

209
00:15:58,190 --> 00:16:01,000
 not satisfying.

210
00:16:01,000 --> 00:16:04,000
 And when we come to practice so then we can really and

211
00:16:04,000 --> 00:16:07,600
 truly come to see the truth of suffering and the causes of

212
00:16:07,600 --> 00:16:09,000
 suffering.

213
00:16:09,000 --> 00:16:13,000
 It starts by seeing the characteristic of suffering.

214
00:16:13,000 --> 00:16:16,120
 Seeing that every single thing in this world has the

215
00:16:16,120 --> 00:16:18,000
 characteristic of suffering.

216
00:16:18,000 --> 00:16:21,000
 There's nothing inside of ourselves or in the world around

217
00:16:21,000 --> 00:16:24,000
 us which can truly satisfy us.

218
00:16:24,000 --> 00:16:27,090
 Characteristic of suffering means it is something which

219
00:16:27,090 --> 00:16:30,000
 cannot bring about satisfaction or contentment.

220
00:16:30,000 --> 00:16:33,000
 It's not something like we think that it is.

221
00:16:33,000 --> 00:16:36,040
 We think that everything which we gain is something which

222
00:16:36,040 --> 00:16:40,000
 we will be able to put up on a mantle or put on our table

223
00:16:40,000 --> 00:16:47,000
 or put in our room, put in our home, keep as our own.

224
00:16:47,000 --> 00:16:53,000
 And that will perpetually bring us peace and happiness.

225
00:16:53,000 --> 00:16:56,000
 In fact we find precisely the opposite.

226
00:16:56,000 --> 00:16:59,820
 That the more we get, the more we obtain, the more

227
00:16:59,820 --> 00:17:04,000
 distracted and distraught and upset our minds become.

228
00:17:04,000 --> 00:17:09,000
 Always constantly thinking about the next pleasure.

229
00:17:09,000 --> 00:17:12,880
 Constantly worried or afraid of any sort of suffering that

230
00:17:12,880 --> 00:17:14,000
 might come.

231
00:17:14,000 --> 00:17:16,000
 Our mind is flitting here and there.

232
00:17:16,000 --> 00:17:21,560
 If you watch you can see people who are engaged in consumer

233
00:17:21,560 --> 00:17:22,000
ism.

234
00:17:22,000 --> 00:17:25,340
 You can see how their minds work just by watching their

235
00:17:25,340 --> 00:17:26,000
 faces.

236
00:17:26,000 --> 00:17:32,790
 They're unable to sit still, unable to be at peace with

237
00:17:32,790 --> 00:17:35,000
 themselves.

238
00:17:35,000 --> 00:17:39,000
 And no matter how much happiness they might find,

239
00:17:39,000 --> 00:17:42,650
 you can see that in the end when suffering overwhelms them

240
00:17:42,650 --> 00:17:45,000
 they will be completely at a loss.

241
00:17:45,000 --> 00:17:50,000
 The same people end up crying and falling into despair.

242
00:17:50,000 --> 00:17:54,000
 When even the slightest unhappiness comes to them.

243
00:17:54,000 --> 00:17:57,280
 In the end when old age sickness and death come there's no

244
00:17:57,280 --> 00:18:00,000
 way that they can possibly hope to deal with it.

245
00:18:00,000 --> 00:18:03,640
 And when they die with all the defilements and all the

246
00:18:03,640 --> 00:18:05,000
 upset in their mind

247
00:18:05,000 --> 00:18:09,000
 there's no way that they can hope to be free from suffering

248
00:18:09,000 --> 00:18:13,000
 after they pass away.

249
00:18:13,000 --> 00:18:16,770
 When we see that this is the nature of all sankhara, all

250
00:18:16,770 --> 00:18:20,000
 things which exist in the world, all things which arise,

251
00:18:20,000 --> 00:18:24,340
 all things which are formed, which come into being based on

252
00:18:24,340 --> 00:18:25,000
 the laws of nature,

253
00:18:25,000 --> 00:18:33,000
 the laws of physics, the laws of karma, the law of the mind

254
00:18:33,000 --> 00:18:33,000
,

255
00:18:33,000 --> 00:18:36,000
 the meditator starts to let go.

256
00:18:36,000 --> 00:18:41,880
 The mind starts to become bored and uninterested, disench

257
00:18:41,880 --> 00:18:44,000
anted, they say.

258
00:18:44,000 --> 00:18:46,000
 The mind starts to turn away.

259
00:18:46,000 --> 00:18:49,840
 Whereas before the mind was something like sitting on the

260
00:18:49,840 --> 00:18:51,000
 edge of your seat.

261
00:18:51,000 --> 00:18:56,540
 Our minds, our hearts are sitting waiting, waiting for the

262
00:18:56,540 --> 00:19:02,000
 next pleasant sight to jump out, jump out at it.

263
00:19:02,000 --> 00:19:05,990
 When we see a beautiful person, someone who is the object

264
00:19:05,990 --> 00:19:10,000
 of our desire, right away the mind leaps out.

265
00:19:10,000 --> 00:19:15,860
 When we smell good food, right away our mind, our mouth

266
00:19:15,860 --> 00:19:18,000
 starts to water.

267
00:19:18,000 --> 00:19:23,630
 When we taste something, some good food, right away our

268
00:19:23,630 --> 00:19:25,000
 minds perk up.

269
00:19:25,000 --> 00:19:29,540
 Jumping out, savoring the taste, waiting for the next taste

270
00:19:29,540 --> 00:19:34,670
, eating more, eating quicker, get more taste, more of the

271
00:19:34,670 --> 00:19:37,000
 good taste.

272
00:19:37,000 --> 00:19:40,290
 When we think, when we start to think pleasant thoughts,

273
00:19:40,290 --> 00:19:42,000
 our mind jumps out at them.

274
00:19:42,000 --> 00:19:45,350
 When the meditator starts to practice and sees this jumping

275
00:19:45,350 --> 00:19:49,000
 of the mind, which is not at peace, which is not happy,

276
00:19:49,000 --> 00:19:54,570
 which is not in any way wholesome or beneficial, the medit

277
00:19:54,570 --> 00:19:57,000
ator starts to become bored,

278
00:19:57,000 --> 00:19:59,000
 starts to see as well that this is not under our control.

279
00:19:59,000 --> 00:20:02,980
 Once we establish this and develop it, it's not something

280
00:20:02,980 --> 00:20:06,000
 we can then control and turn off or on as we wish.

281
00:20:06,000 --> 00:20:12,140
 It's something which then envelops us, like a great fire

282
00:20:12,140 --> 00:20:16,000
 that comes to burn our whole being,

283
00:20:16,000 --> 00:20:22,000
 something we cannot, a fire we cannot easily put out.

284
00:20:22,000 --> 00:20:24,590
 And so, like a person whose house is on fire, we become dis

285
00:20:24,590 --> 00:20:27,000
enchanted and start to say,

286
00:20:27,000 --> 00:20:31,780
 "This house is really no longer worth affecting. The fire

287
00:20:31,780 --> 00:20:35,000
 has completely enveloped the house."

288
00:20:35,000 --> 00:20:39,070
 And the meditator starts to loosen their grip on the body

289
00:20:39,070 --> 00:20:43,000
 and the mind and even the whole world around.

290
00:20:43,000 --> 00:20:46,640
 The mind starts to let go. So, when we see good things or

291
00:20:46,640 --> 00:20:50,000
 see bad things, the mind is not leaping out anymore.

292
00:20:50,000 --> 00:20:52,880
 When we hear good things, hear bad things, we don't like

293
00:20:52,880 --> 00:20:56,000
 what someone says, we don't like what someone does.

294
00:20:56,000 --> 00:21:01,000
 We think this is a bad person, that is a bad person.

295
00:21:01,000 --> 00:21:06,000
 Mean people, unfair, so on.

296
00:21:06,000 --> 00:21:15,000
 When we practice, we start to lose these sort of feelings.

297
00:21:15,000 --> 00:21:19,510
 The mind starts to become at peace. We feel like this upset

298
00:21:19,510 --> 00:21:24,000
 or this anger or this fear or boredom or desire,

299
00:21:24,000 --> 00:21:29,930
 which we used to carry around like a flag. It starts to

300
00:21:29,930 --> 00:21:33,000
 become a burden.

301
00:21:33,000 --> 00:21:35,990
 Something which we no longer want, we no longer wish for,

302
00:21:35,990 --> 00:21:40,000
 when we start to let go and give up.

303
00:21:40,000 --> 00:21:43,440
 This is the benefit of the practice. This is how we attain

304
00:21:43,440 --> 00:21:44,000
 freedom.

305
00:21:44,000 --> 00:21:47,710
 Because freedom from suffering is not something that

306
00:21:47,710 --> 00:21:51,000
 another person can take from you or give to you.

307
00:21:51,000 --> 00:21:55,350
 Not like freedom to do this or do that. People can take

308
00:21:55,350 --> 00:21:57,000
 this from you.

309
00:21:57,000 --> 00:22:01,410
 People can even take your freedom to think if they are

310
00:22:01,410 --> 00:22:03,000
 clever enough.

311
00:22:03,000 --> 00:22:10,480
 But no one can take away or give you freedom from suffering

312
00:22:10,480 --> 00:22:11,000
.

313
00:22:11,000 --> 00:22:21,280
 It's something which comes to us at the time when we let go

314
00:22:21,280 --> 00:22:22,000
.

315
00:22:22,000 --> 00:22:25,530
 At the time when we see impermanence, at the time when we

316
00:22:25,530 --> 00:22:26,000
 see suffering,

317
00:22:26,000 --> 00:22:30,320
 at the time when we are walking and sitting and we are

318
00:22:30,320 --> 00:22:32,000
 saying to ourselves, "Rising and falling."

319
00:22:32,000 --> 00:22:35,880
 And the mind is seeing clearly the nature of the things

320
00:22:35,880 --> 00:22:38,000
 inside of us and around us.

321
00:22:38,000 --> 00:22:42,000
 This is how we attain what we call freedom from suffering.

322
00:22:42,000 --> 00:22:46,000
 It comes by seeing the truth of suffering.

323
00:22:46,000 --> 00:22:49,430
 That there is really no one holding on to us or holding us

324
00:22:49,430 --> 00:22:50,000
 down.

325
00:22:50,000 --> 00:22:54,000
 No one keeping us in suffering.

326
00:22:54,000 --> 00:22:58,280
 This sort of freedom is something which comes when we

327
00:22:58,280 --> 00:23:00,000
 ourselves let go.

328
00:23:00,000 --> 00:23:05,880
 When we ourselves cease to hold on, cease to attach, cease

329
00:23:05,880 --> 00:23:08,000
 to chase after.

330
00:23:08,000 --> 00:23:16,000
 We ourselves are binding ourselves, imprisoning ourselves.

331
00:23:16,000 --> 00:23:19,320
 When we see this, when we see that the cause of our

332
00:23:19,320 --> 00:23:21,000
 suffering is our own mind,

333
00:23:21,000 --> 00:23:24,000
 this is called the freedom from suffering.

334
00:23:24,000 --> 00:23:27,070
 This is the truth of suffering, seeing the truth of

335
00:23:27,070 --> 00:23:28,000
 suffering.

336
00:23:28,000 --> 00:23:32,020
 Seeing the cause of suffering and abandoning the cause,

337
00:23:32,020 --> 00:23:33,000
 means letting go.

338
00:23:33,000 --> 00:23:37,300
 At the moment when the mind turns away, turns away and

339
00:23:37,300 --> 00:23:42,000
 finally completely abandons attachment.

340
00:23:42,000 --> 00:23:46,000
 This is called the freedom from suffering.

341
00:23:46,000 --> 00:23:49,540
 And so, freedom in this sense is also not something which

342
00:23:49,540 --> 00:23:51,000
 requires that we go anywhere

343
00:23:51,000 --> 00:23:57,000
 or leave the world or become anything.

344
00:23:57,000 --> 00:24:01,760
 It's a freedom which we can obtain even when living in the

345
00:24:01,760 --> 00:24:02,000
 world

346
00:24:02,000 --> 00:24:06,710
 or we can carry with us even living in the world that no

347
00:24:06,710 --> 00:24:09,000
 one can take away from us.

348
00:24:09,000 --> 00:24:12,000
 Because we see the things we see, we hear, we smell, we

349
00:24:12,000 --> 00:24:12,000
 taste.

350
00:24:12,000 --> 00:24:16,000
 These things cannot really make us suffer.

351
00:24:16,000 --> 00:24:19,000
 It is only our own craving which can bring about suffering.

352
00:24:19,000 --> 00:24:22,000
 The cause of suffering is not unpleasant sights or sounds

353
00:24:22,000 --> 00:24:25,000
 or smells or tastes or feelings or thoughts.

354
00:24:25,000 --> 00:24:28,000
 The cause of suffering is attachment.

355
00:24:28,000 --> 00:24:32,000
 So we can live simply as we have been living,

356
00:24:32,000 --> 00:24:39,070
 only without any sort of craving or aversion to the objects

357
00:24:39,070 --> 00:24:41,000
 of the sense.

358
00:24:41,000 --> 00:24:44,000
 This is what the Lord Buddha called freedom.

359
00:24:44,000 --> 00:24:47,590
 When we practice meditation, we are looking for at least a

360
00:24:47,590 --> 00:24:49,000
 small taste of freedom.

361
00:24:49,000 --> 00:24:52,690
 And we hope that all of the meditators will come to

362
00:24:52,690 --> 00:24:54,000
 practice with us.

363
00:24:54,000 --> 00:24:58,000
 Should be able to obtain at least a small taste of freedom.

364
00:24:58,000 --> 00:25:03,000
 And if they continue to practice, we can expect that once

365
00:25:03,000 --> 00:25:05,000
 they are able to attain

366
00:25:05,000 --> 00:25:08,000
 what they know for themselves is the right path.

367
00:25:08,000 --> 00:25:12,000
 They are able to say for themselves without anyone else

368
00:25:12,000 --> 00:25:15,000
 convincing them or coercing them.

369
00:25:15,000 --> 00:25:19,000
 Once they can say that this or that is the right path,

370
00:25:19,000 --> 00:25:22,560
 we can expect that they continue on that path that they

371
00:25:22,560 --> 00:25:25,000
 should not only be able to taste freedom

372
00:25:25,000 --> 00:25:29,000
 but in the end be able to become completely free.

373
00:25:29,000 --> 00:25:33,830
 This is the goal which the Lord Buddha is meditators, his

374
00:25:33,830 --> 00:25:36,000
 followers.

375
00:25:36,000 --> 00:25:41,000
 This is the goal which we present to our meditators.

376
00:25:41,000 --> 00:25:46,130
 Saying only we expect that you should gain a taste of

377
00:25:46,130 --> 00:25:47,000
 freedom.

378
00:25:47,000 --> 00:25:52,000
 Once you have obtained a taste of freedom, it's up to you

379
00:25:52,000 --> 00:25:53,000
 to look inside yourself

380
00:25:53,000 --> 00:25:58,560
 and see whether you know for yourself that this path leads

381
00:25:58,560 --> 00:26:00,000
 to freedom.

382
00:26:00,000 --> 00:26:06,040
 Once you have tasted freedom, we expect and we can observe

383
00:26:06,040 --> 00:26:07,000
 that the meditators

384
00:26:07,000 --> 00:26:13,180
 who have genuinely tasted freedom, that they are then able

385
00:26:13,180 --> 00:26:14,000
 to say for themselves

386
00:26:14,000 --> 00:26:16,000
 that they have found the right path.

387
00:26:16,000 --> 00:26:20,000
 They have found the path which leads to complete freedom.

388
00:26:20,000 --> 00:26:22,310
 The path which will lead them in this very life without

389
00:26:22,310 --> 00:26:26,000
 leaving the world, without going anywhere, being anything,

390
00:26:26,000 --> 00:26:29,600
 will lead them to complete freedom from suffering based on

391
00:26:29,600 --> 00:26:31,000
 their own practice

392
00:26:31,000 --> 00:26:39,000
 and their own dedication to the practice.

393
00:26:39,000 --> 00:26:44,280
 So this is the Lord Buddha's teaching on what it means to

394
00:26:44,280 --> 00:26:45,000
 be free.

395
00:26:45,000 --> 00:26:48,000
 This is the Dhamma which I give today.

396
00:26:50,000 --> 00:26:52,000
 [ Sigh ]

