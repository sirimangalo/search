WEBVTT

00:00:00.000 --> 00:00:05.000
 Good evening everyone.

00:00:05.000 --> 00:00:12.000
 We're broadcasting live April 28th.

00:00:12.000 --> 00:00:27.120
 Tonight's talk is about reverence.

00:00:27.120 --> 00:00:41.120
 This monk comes and asks the Buddha.

00:00:41.120 --> 00:00:43.120
 I asked him two questions actually.

00:00:43.120 --> 00:00:46.120
 This quote only gives one.

00:00:46.120 --> 00:00:59.200
 He says, "Konukobante he tukopajayo yena tathagate parinibh

00:00:59.200 --> 00:01:00.120
ute sadhamo na kira titiko hoti."

00:01:00.120 --> 00:01:09.120
 What is the cause? What is the reason?

00:01:09.120 --> 00:01:16.360
 By which, when the tathagata has entered into parinibhana,

00:01:16.360 --> 00:01:21.120
 the good dhamma doesn't last long.

00:01:21.120 --> 00:01:25.120
 Doesn't last. So this quote only does last.

00:01:25.120 --> 00:01:29.350
 The next question is, "What is the cause? What is the

00:01:29.350 --> 00:01:33.120
 reason why it does last long?"

00:01:33.120 --> 00:01:40.120
 "Kira titiko hoti."

00:01:40.120 --> 00:01:48.120
 This is from the Anguttra Nikaya, book of sevens.

00:01:48.120 --> 00:01:52.120
 So it's a list of seven things.

00:01:52.120 --> 00:02:01.120
 Seven things that when you pay respect to them, reverence,

00:02:01.120 --> 00:02:07.120
 the word is "garawa," "sagarawa."

00:02:07.120 --> 00:02:12.120
 "Gara" means heavy, "garu."

00:02:12.120 --> 00:02:15.120
 "Garu" means heavy.

00:02:15.120 --> 00:02:20.120
 "Garu" actually is the word "guru" in Sanskrit.

00:02:20.120 --> 00:02:23.120
 "Guru" means teacher.

00:02:23.120 --> 00:02:34.120
 But I believe the origin is from the word "heavy."

00:02:34.120 --> 00:02:40.120
 And "garawa" means to take something seriously.

00:02:40.120 --> 00:02:54.230
 If we appreciate and think of as worth something, so we

00:02:54.230 --> 00:02:57.120
 worship in a sense, in a literal sense.

00:02:57.120 --> 00:03:08.120
 We place worth on these things.

00:03:08.120 --> 00:03:12.120
 Then the dhamma will last long after the Buddha disappears.

00:03:12.120 --> 00:03:19.030
 Because when the Buddha is around, he's a teacher of all

00:03:19.030 --> 00:03:21.120
 beings.

00:03:21.120 --> 00:03:29.120
 He's the unexcelled teacher, unexcelled trainer.

00:03:29.120 --> 00:03:32.120
 So when he's around, there's no question.

00:03:32.120 --> 00:03:34.120
 The dhamma will last.

00:03:34.120 --> 00:03:40.120
 When he's gone, however, what we've got left is the Arahant

00:03:40.120 --> 00:03:42.120
 disciples of the Buddha,

00:03:42.120 --> 00:03:48.360
 and then they pass away, and then hopefully more Arahants

00:03:48.360 --> 00:03:49.120
 come.

00:03:49.120 --> 00:03:52.120
 The question is, what keeps the lineage going?

00:03:52.120 --> 00:03:55.120
 What keeps the chain going?

00:03:55.120 --> 00:03:58.480
 And so it's suttas like this that are of great interest to

00:03:58.480 --> 00:04:02.120
 those of us in later generations,

00:04:02.120 --> 00:04:07.060
 because it gives us an idea of how we support not only our

00:04:07.060 --> 00:04:08.120
 own practice,

00:04:08.120 --> 00:04:16.300
 but how we carry on the legacy of the Buddha, pass on the

00:04:16.300 --> 00:04:20.120
 legacy to future generations as well.

00:04:20.120 --> 00:04:26.750
 How we take what was given to us and cherish it and nurture

00:04:26.750 --> 00:04:30.120
 it and keep it alive.

00:04:30.120 --> 00:04:34.120
 It's like we're given a seed to a great tree,

00:04:34.120 --> 00:04:39.470
 and it's up to us to plant it and to cultivate it for

00:04:39.470 --> 00:04:42.120
 future generations.

00:04:42.120 --> 00:04:46.120
 The dhamma is like that at some point, or it's like a fire.

00:04:46.120 --> 00:04:50.120
 In ancient times, in cave time, in ancient, ancient times,

00:04:50.120 --> 00:04:59.030
 they had to carry a coal with them from campfire to camp

00:04:59.030 --> 00:05:00.120
fire.

00:05:00.120 --> 00:05:04.120
 The easiest way to keep fire was to carry something,

00:05:04.120 --> 00:05:08.120
 and if the fire went out, then you're in trouble.

00:05:08.120 --> 00:05:10.120
 You had a fire carrier.

00:05:10.120 --> 00:05:14.120
 I read a story about it once anyway.

00:05:14.120 --> 00:05:21.120
 You have to care for the dhamma.

00:05:21.120 --> 00:05:24.120
 So how do we care for the dhamma?

00:05:24.120 --> 00:05:33.900
 What are the ways by which we ensure that this teaching

00:05:33.900 --> 00:05:36.120
 will continue?

00:05:36.120 --> 00:05:38.120
 So the Buddha gives seven things.

00:05:38.120 --> 00:05:43.120
 We take these things seriously and appreciate them.

00:05:43.120 --> 00:05:45.120
 And in a sense, worship them.

00:05:45.120 --> 00:05:50.430
 But worship, not in the way we use it, just assign it some

00:05:50.430 --> 00:05:52.120
 worth.

00:05:52.120 --> 00:05:54.510
 And of course, the first three are the Buddha, the dhamma,

00:05:54.510 --> 00:05:55.120
 and the sangha.

00:05:55.120 --> 00:05:57.120
 The first one is the sata.

00:05:57.120 --> 00:06:01.480
 Sata means teacher, but in Buddhism, it usually refers to

00:06:01.480 --> 00:06:02.120
 the Buddha.

00:06:02.120 --> 00:06:04.120
 And here that's what it refers to.

00:06:04.120 --> 00:06:10.730
 It's a special word that is generally only used for the

00:06:10.730 --> 00:06:12.120
 Buddha.

00:06:12.120 --> 00:06:23.120
 It literally means teacher, something like teacher.

00:06:23.120 --> 00:06:29.470
 And so if a monk or a bhikkhu or a bhikkhuni, a male or

00:06:29.470 --> 00:06:31.120
 female monk,

00:06:31.120 --> 00:06:39.120
 or a rupasaka or rupasika, a male or female lay disciple,

00:06:39.120 --> 00:06:42.120
 it's just anyone who's a man or a woman,

00:06:42.120 --> 00:06:46.120
 and anyone who's in between, anyone at all.

00:06:46.120 --> 00:06:55.120
 If they do not take the teacher seriously and assign worth

00:06:55.120 --> 00:07:02.750
 and appreciation to the teacher or the dhamma or the sangha

00:07:02.750 --> 00:07:03.120
,

00:07:03.120 --> 00:07:10.120
 they do not dwell agara, agarawa, vihranthi.

00:07:10.120 --> 00:07:18.120
 They dwell without reverence, without appreciation,

00:07:18.120 --> 00:07:23.120
 without taking seriously.

00:07:23.120 --> 00:07:32.120
 And then the fourth is sikha, sikha, sikhaaya.

00:07:32.120 --> 00:07:37.120
 This is the training, sikha.

00:07:37.120 --> 00:07:42.120
 Sikha means the training, so the training in,

00:07:42.120 --> 00:07:47.120
 in a general sense, the training in giving up craving,

00:07:47.120 --> 00:07:52.120
 training to give up,

00:07:52.120 --> 00:07:59.120
 and the training in morality,

00:07:59.120 --> 00:08:05.120
 the training in concentration and wisdom.

00:08:05.120 --> 00:08:10.120
 But here's specifically just the act of,

00:08:10.120 --> 00:08:13.120
 and the things that we do like practicing meditation

00:08:13.120 --> 00:08:15.120
 and keeping precepts.

00:08:15.120 --> 00:08:21.120
 So the things that we do and the things that we don't do,

00:08:21.120 --> 00:08:23.120
 we abstain from certain things

00:08:23.120 --> 00:08:29.120
 and we take on certain behaviors, certain practices.

00:08:29.120 --> 00:08:33.120
 This is the training and the learning,

00:08:33.120 --> 00:08:38.120
 studying, listening to the dhamma, remembering the dhamma,

00:08:38.120 --> 00:08:41.120
 reciting the dhamma, thinking about the dhamma,

00:08:41.120 --> 00:08:43.120
 all of these things.

00:08:43.120 --> 00:08:46.120
 This is all the training that we go through,

00:08:46.120 --> 00:08:47.120
 taking that seriously.

00:08:47.120 --> 00:08:51.120
 If you don't take that seriously,

00:08:51.120 --> 00:08:54.120
 and number five is samadhi.

00:08:54.120 --> 00:08:56.120
 We don't take concentration seriously.

00:08:56.120 --> 00:09:01.120
 We go to spells it out explicitly, concentration.

00:09:01.120 --> 00:09:05.120
 Samadhi is, samadhi is an interesting word.

00:09:05.120 --> 00:09:07.120
 It could just mean focus, you know.

00:09:07.120 --> 00:09:12.120
 Sammas is like same.

00:09:12.120 --> 00:09:15.120
 It means level or even.

00:09:15.120 --> 00:09:18.120
 So not too much, not too little.

00:09:18.120 --> 00:09:21.120
 When your mind gets perfectly focused,

00:09:21.120 --> 00:09:23.550
 that's why it's kind of like focused rather than

00:09:23.550 --> 00:09:24.120
 concentration

00:09:24.120 --> 00:09:27.120
 because it's like a camera lens.

00:09:27.120 --> 00:09:30.540
 If you focus too much, it gets blurry, too little also

00:09:30.540 --> 00:09:31.120
 blurry.

00:09:31.120 --> 00:09:36.120
 You need perfect focus and then you can see.

00:09:36.120 --> 00:09:40.410
 So your mind has to be balanced is really what samadhi is

00:09:40.410 --> 00:09:42.120
 all about.

00:09:42.120 --> 00:09:45.120
 People think of samadhi as being concentrated,

00:09:45.120 --> 00:09:52.120
 so no thoughts, focused only on one thing, no distractions.

00:09:52.120 --> 00:09:54.120
 But it's not necessarily that.

00:09:54.120 --> 00:09:59.120
 It just means the mind that is seeing things clearly,

00:09:59.120 --> 00:10:04.910
 the mind that is focused on something as it is in that

00:10:04.910 --> 00:10:06.120
 moment.

00:10:06.120 --> 00:10:09.120
 You don't have to block everything out

00:10:09.120 --> 00:10:11.120
 and just concentrate on a single thing.

00:10:11.120 --> 00:10:17.100
 You have to be focused on whatever arises clearly, seeing

00:10:17.100 --> 00:10:19.120
 it clearly.

00:10:19.120 --> 00:10:21.120
 That's why we use this word when we say to ourselves

00:10:21.120 --> 00:10:25.120
 seeing or hearing or rising, falling.

00:10:25.120 --> 00:10:27.120
 We're trying to focus.

00:10:27.120 --> 00:10:32.120
 Focus means on this, the core of it.

00:10:32.120 --> 00:10:36.120
 The core of this is rising or whatever rising means.

00:10:36.120 --> 00:10:38.120
 This is falling.

00:10:38.120 --> 00:10:41.120
 The core of seeing is seeing. The core of pain is pain.

00:10:41.120 --> 00:10:46.120
 There's no judgments or reactions or anything.

00:10:46.120 --> 00:10:56.120
 Get rid of all of those.

00:10:56.120 --> 00:11:07.120
 Number six is apamada, apamade, agarwa, wihranati, apatissa

00:11:07.120 --> 00:11:07.120
.

00:11:07.120 --> 00:11:16.120
 Apamada is the last words of the Buddha

00:11:16.120 --> 00:11:18.120
 is that we should cultivate apamada.

00:11:18.120 --> 00:11:21.120
 Pamada comes from the root mud.

00:11:21.120 --> 00:11:23.120
 Mud means to be drunk.

00:11:23.120 --> 00:11:27.120
 Pamada is like really drunk.

00:11:27.120 --> 00:11:32.120
 But it's just a prefix that modifies it, that augments it.

00:11:32.120 --> 00:11:47.670
 So, pamada is like negligent or intoxicated, mixed up in

00:11:47.670 --> 00:11:49.120
 the mind.

00:11:49.120 --> 00:11:56.000
 Apamada is to be clear-minded, to be un-intoxicated, to be

00:11:56.000 --> 00:11:57.120
 sober.

00:11:57.120 --> 00:12:02.130
 So not drunk on lust or drunk on anger, drunk on delusion

00:12:02.130 --> 00:12:07.120
 or arrogance or conceit.

00:12:07.120 --> 00:12:10.120
 To not be drunk on any of these emotions.

00:12:10.120 --> 00:12:12.120
 So we should take that seriously.

00:12:12.120 --> 00:12:14.120
 We should appreciate that.

00:12:14.120 --> 00:12:19.120
 If we don't appreciate that, that's what the Buddha says.

00:12:19.120 --> 00:12:27.120
 And the seventh one is quite curious.

00:12:27.120 --> 00:12:32.120
 But curious in a good way.

00:12:32.120 --> 00:12:35.120
 It's just surprising, I think.

00:12:35.120 --> 00:12:38.120
 It's not what you'd expect as the last one.

00:12:38.120 --> 00:12:41.120
 Pati santara.

00:12:41.120 --> 00:12:50.120
 Ajahn, my teacher talked about it. He mentions this.

00:12:50.120 --> 00:12:56.120
 Many times the Buddha talks about pati santara.

00:12:56.120 --> 00:13:00.120
 Pati santara agarwa.

00:13:00.120 --> 00:13:03.120
 Pati santara agarwa.

00:13:03.120 --> 00:13:07.120
 If we don't take seriously or appreciate pati santara.

00:13:07.120 --> 00:13:14.020
 Pati santara is, I don't quite know the etymology, but it

00:13:14.020 --> 00:13:20.120
 means hospitality.

00:13:20.120 --> 00:13:25.590
 Or it could mean friendliness, it could mean goodwill,

00:13:25.590 --> 00:13:27.120
 friendship.

00:13:27.120 --> 00:13:34.120
 But it's really just hospitality when you welcome people.

00:13:34.120 --> 00:13:36.120
 But in this context, it makes perfect sense, right?

00:13:36.120 --> 00:13:38.770
 If you have a meditation center where you don't welcome

00:13:38.770 --> 00:13:39.120
 people,

00:13:39.120 --> 00:13:45.120
 when someone walks in, they don't know.

00:13:45.120 --> 00:13:50.120
 Everyone looks at them kind of, "What are you doing here?"

00:13:50.120 --> 00:13:53.600
 Sometimes you go to a monastery and no one wants to talk to

00:13:53.600 --> 00:13:54.120
 you.

00:13:54.120 --> 00:13:58.120
 Go to a meditation center and maybe no one, everyone's,

00:13:58.120 --> 00:14:03.610
 if people are kind of putting on airs like they're real med

00:14:03.610 --> 00:14:05.120
itators or something.

00:14:05.120 --> 00:14:08.530
 Or if they just don't care and they're just concerned about

00:14:08.530 --> 00:14:10.120
 their own practice.

00:14:10.120 --> 00:14:17.120
 If such a place existed, then how would you ever share?

00:14:17.120 --> 00:14:22.120
 How would that ever lead to spreading the dhamma?

00:14:22.120 --> 00:14:27.120
 I've heard, there are places where you go into the office,

00:14:27.120 --> 00:14:34.120
 the meditation center office and they just yell at you.

00:14:34.120 --> 00:14:42.310
 They look down upon you and they don't know how to deal

00:14:42.310 --> 00:14:44.120
 with people.

00:14:44.120 --> 00:14:46.120
 And I've talked with other people.

00:14:46.120 --> 00:14:50.120
 In Chum Tong, the people in the office would rotate.

00:14:50.120 --> 00:14:53.120
 And so you had to catch the right person in the office

00:14:53.120 --> 00:14:56.120
 or they never know what was going to happen.

00:14:56.120 --> 00:14:58.950
 You want to book a room for someone and maybe they yell at

00:14:58.950 --> 00:14:59.120
 you,

00:14:59.120 --> 00:15:01.120
 maybe they're very nice.

00:15:01.120 --> 00:15:08.120
 You have to find the right person and talk to them.

00:15:08.120 --> 00:15:09.120
 It's very important.

00:15:09.120 --> 00:15:11.120
 So it's something for us to remember.

00:15:11.120 --> 00:15:13.120
 We have to welcome.

00:15:13.120 --> 00:15:15.120
 And in general, you could talk about this.

00:15:15.120 --> 00:15:21.120
 Something I was thinking about is it'd be neat someday

00:15:21.120 --> 00:15:26.120
 if our community gets, if our community grows,

00:15:26.120 --> 00:15:32.120
 if we could have groups of people, you know,

00:15:32.120 --> 00:15:38.170
 like maybe once a week we could have kind of a more formal

00:15:38.170 --> 00:15:40.120
 online session.

00:15:40.120 --> 00:15:48.120
 And if someone was organizing a group in their home,

00:15:48.120 --> 00:15:55.120
 then they could join the hangout with their group.

00:15:55.120 --> 00:15:56.120
 Wow, wouldn't that be neat?

00:15:56.120 --> 00:15:58.120
 That's what we should do.

00:15:58.120 --> 00:16:04.120
 Once a week, we could do once a month to start.

00:16:04.120 --> 00:16:08.120
 We have groups of people.

00:16:08.120 --> 00:16:13.120
 And someone puts together a group in their home

00:16:13.120 --> 00:16:18.120
 and people come to their home and they join the hangout.

00:16:18.120 --> 00:16:24.120
 And we could have up to 10 groups.

00:16:24.120 --> 00:16:26.120
 We have to talk about that.

00:16:26.120 --> 00:16:30.120
 So the idea is to welcome people even into your home, you

00:16:30.120 --> 00:16:30.120
 know.

00:16:30.120 --> 00:16:32.120
 You set up a Dhamma group.

00:16:32.120 --> 00:16:33.120
 People do this.

00:16:33.120 --> 00:16:35.120
 They have a Dhamma group in their home.

00:16:35.120 --> 00:16:40.120
 They've got space.

00:16:40.120 --> 00:16:45.120
 And then they set up.

00:16:45.120 --> 00:16:48.120
 They, you know, sometimes they just meditate together.

00:16:48.120 --> 00:16:50.120
 Sometimes they listen to a talk.

00:16:50.120 --> 00:16:55.120
 I know Dhamma groups that just put on some CD or something,

00:16:55.120 --> 00:16:57.120
 you know, somebody giving a talk.

00:16:57.120 --> 00:17:01.120
 And everybody listens and then they do meditation together.

00:17:01.120 --> 00:17:06.120
 So we could do something like that.

00:17:06.120 --> 00:17:09.480
 But it could be live, you know, live from all over the

00:17:09.480 --> 00:17:10.120
 world.

00:17:10.120 --> 00:17:12.120
 10 different groups.

00:17:12.120 --> 00:17:16.120
 That's the maximum you can have 10 people in the hangout.

00:17:16.120 --> 00:17:21.120
 So if anybody's interested in setting up a Dhamma group,

00:17:21.120 --> 00:17:23.120
 let me know.

00:17:23.120 --> 00:17:27.600
 And we'll try and arrange something where we meet together

00:17:27.600 --> 00:17:28.120
 like this,

00:17:28.120 --> 00:17:35.120
 but we'll have a special day where I'll give a real talk.

00:17:35.120 --> 00:17:38.120
 I'll give a longer talk.

00:17:38.120 --> 00:17:45.120
 And then we'll connect.

00:17:45.120 --> 00:17:46.120
 All right.

00:17:46.120 --> 00:17:48.120
 So those are the seven.

00:17:48.120 --> 00:17:53.770
 That's one list of seven things that lead to lead to the D

00:17:53.770 --> 00:17:55.120
hamma lasting.

00:17:55.120 --> 00:17:58.120
 So if we don't spread it, if we don't share it,

00:17:58.120 --> 00:18:01.120
 if we're not welcoming of people who want to learn.

00:18:01.120 --> 00:18:05.110
 And the Buddha wasn't big on on spreading the Dhamma in

00:18:05.110 --> 00:18:06.120
 terms of going around

00:18:06.120 --> 00:18:10.120
 teaching people, but much more about wealth,

00:18:10.120 --> 00:18:13.160
 as far as I can see much more about welcoming people who

00:18:13.160 --> 00:18:14.120
 wanted to learn.

00:18:14.120 --> 00:18:16.120
 And people wanted to learn.

00:18:16.120 --> 00:18:20.120
 It was all about finding ways to accommodate them.

00:18:20.120 --> 00:18:22.120
 If they do come, if they don't come,

00:18:22.120 --> 00:18:25.120
 don't have to go out of our way looking for people.

00:18:25.120 --> 00:18:33.120
 We're not trying to push it this on anybody.

00:18:33.120 --> 00:18:35.120
 That's kind of how beautiful it is.

00:18:35.120 --> 00:18:36.120
 We don't need students.

00:18:36.120 --> 00:18:38.120
 We're not looking for students.

00:18:38.120 --> 00:18:43.740
 And just people who are looking for it, we open the door

00:18:43.740 --> 00:18:44.120
 for them,

00:18:44.120 --> 00:18:46.120
 provide them the opportunity.

00:18:46.120 --> 00:18:49.120
 Tonight a woman came to visit.

00:18:49.120 --> 00:18:51.120
 She lived in Cambodia for nine years.

00:18:51.120 --> 00:18:55.120
 And she just came and she's seen some of my videos,

00:18:55.120 --> 00:19:01.120
 and I gave her the booklet.

00:19:01.120 --> 00:19:02.120
 And then she just did meditation.

00:19:02.120 --> 00:19:03.120
 I didn't meditate with her.

00:19:03.120 --> 00:19:06.120
 I came upstairs and did my -- because I do walking.

00:19:06.120 --> 00:19:09.120
 She didn't want to do walking.

00:19:09.120 --> 00:19:14.560
 So -- but it would be nice if we could have a group here as

00:19:14.560 --> 00:19:16.120
 well.

00:19:16.120 --> 00:19:18.590
 I think probably what we'd do is just have people come up

00:19:18.590 --> 00:19:19.120
 here at nine.

00:19:19.120 --> 00:19:22.120
 If someone wants to hear the Dhamma, they can come at nine,

00:19:22.120 --> 00:19:30.120
 come sit up with us here and -- for now, anyway.

00:19:30.120 --> 00:19:34.120
 We're looking to get a bigger place, so.

00:19:34.120 --> 00:19:37.120
 Anyway, so these seven, the Buddha, the Dhamma, the Sangha,

00:19:37.120 --> 00:19:38.120
 take them seriously.

00:19:38.120 --> 00:19:41.120
 That's another thing is often people don't take the Buddha

00:19:41.120 --> 00:19:42.120
 too seriously

00:19:42.120 --> 00:19:46.260
 or they -- you know, these talks about people who spit on

00:19:46.260 --> 00:19:47.120
 Buddha images

00:19:47.120 --> 00:19:52.120
 and burn them and just treat them like rubbish,

00:19:52.120 --> 00:19:56.120
 thinking that's not the real Buddha.

00:19:56.120 --> 00:19:58.120
 But there's something to it, you know.

00:19:58.120 --> 00:20:00.120
 If you don't take the Buddha images seriously,

00:20:00.120 --> 00:20:04.120
 what does that say about how you feel about the Buddha?

00:20:04.120 --> 00:20:07.120
 Yeah, people say it's just an image of the Buddhas and

00:20:07.120 --> 00:20:07.120
 whatever,

00:20:07.120 --> 00:20:11.120
 but there's something to images.

00:20:11.120 --> 00:20:13.120
 They represent something.

00:20:13.120 --> 00:20:16.120
 In ancient times, they wouldn't even make images

00:20:16.120 --> 00:20:18.120
 because they revered the Buddha, it seems,

00:20:18.120 --> 00:20:22.120
 because they revered the Buddha so much.

00:20:22.120 --> 00:20:25.490
 So when we have these images, we try to treat them quite

00:20:25.490 --> 00:20:26.120
 carefully

00:20:26.120 --> 00:20:29.120
 because we respect the Buddha so much.

00:20:29.120 --> 00:20:32.120
 If you don't, I mean, this is the thing,

00:20:32.120 --> 00:20:35.120
 this is the Dhamma won't last because there's no figure,

00:20:35.120 --> 00:20:39.120
 there's no -- there's none of this sort of religious

00:20:39.120 --> 00:20:40.120
 feeling

00:20:40.120 --> 00:20:43.120
 that keeps things together, the sense of urgency,

00:20:43.120 --> 00:20:48.500
 the sense of zeal and interest that's so powerful in any

00:20:48.500 --> 00:20:49.120
 religion.

00:20:49.120 --> 00:20:52.050
 It could be for the purposes of evil, it could be for the

00:20:52.050 --> 00:20:53.120
 purposes of good,

00:20:53.120 --> 00:20:56.120
 but it's a power.

00:20:56.120 --> 00:20:59.120
 If you don't take these things seriously,

00:20:59.120 --> 00:21:02.120
 I don't appreciate them, don't revere them.

00:21:02.120 --> 00:21:05.120
 Very hard to keep going.

00:21:05.120 --> 00:21:08.120
 You know, we talk about secular Buddhism,

00:21:08.120 --> 00:21:11.290
 yeah, it's fine, but it's hard to get that feeling and

00:21:11.290 --> 00:21:13.120
 appreciation, you know.

00:21:13.120 --> 00:21:17.120
 Part of religion is the feeling,

00:21:17.120 --> 00:21:23.120
 the sense of reverence, appreciation,

00:21:23.120 --> 00:21:26.120
 not just taking something clinically in terms of,

00:21:26.120 --> 00:21:28.120
 yes, this helps me relieve stress,

00:21:28.120 --> 00:21:31.500
 but yes, this is the teaching of the perfectly enlightened

00:21:31.500 --> 00:21:32.120
 Buddha,

00:21:32.120 --> 00:21:38.070
 you know, it carries a lot more weight, you know, in that

00:21:38.070 --> 00:21:39.120
 sense.

00:21:39.120 --> 00:21:44.120
 And then the training, concentration,

00:21:44.120 --> 00:21:55.120
 Apamada, which is vigilance or sobriety, and hospitality.

00:21:55.120 --> 00:21:58.150
 We have to take the training seriously, both study and

00:21:58.150 --> 00:21:59.120
 practice.

00:21:59.120 --> 00:22:01.120
 We have to take the concentration,

00:22:01.120 --> 00:22:06.120
 remember to try and be concentrated, remember and practice,

00:22:06.120 --> 00:22:09.120
 not just study.

00:22:09.120 --> 00:22:14.120
 And be mindful, Apamada actually being sober,

00:22:14.120 --> 00:22:17.120
 the Buddha said it's a synonym for being mindful,

00:22:17.120 --> 00:22:20.120
 so this really means using mindfulness,

00:22:20.120 --> 00:22:23.120
 seeing things clearly as they are,

00:22:23.120 --> 00:22:28.690
 grasping things as they are, remembering things as they are

00:22:28.690 --> 00:22:29.120
.

00:22:29.120 --> 00:22:31.120
 And finally, we have to be hospitable,

00:22:31.120 --> 00:22:34.120
 so we have to welcome people to join,

00:22:34.120 --> 00:22:36.120
 not just practicing for ourselves,

00:22:36.120 --> 00:22:40.510
 but providing the opportunity and being friendly and

00:22:40.510 --> 00:22:42.120
 welcoming.

00:22:42.120 --> 00:22:45.120
 Don't just shy away and say,

00:22:45.120 --> 00:22:47.120
 "Oh, I'm not a teacher, I can't teach you the Dhamma,

00:22:47.120 --> 00:22:49.120
 anyone can teach."

00:22:49.120 --> 00:22:52.120
 You teach how you were taught, pass it on.

00:22:52.120 --> 00:22:55.120
 It doesn't mean you have to answer all of their questions

00:22:55.120 --> 00:22:57.120
 and problems and give them a device,

00:22:57.120 --> 00:22:59.510
 it just means you have to explain to them how to do

00:22:59.510 --> 00:23:00.120
 meditation,

00:23:00.120 --> 00:23:03.120
 which is quite simple.

00:23:03.120 --> 00:23:06.120
 And just reassure them that it has benefits,

00:23:06.120 --> 00:23:09.060
 and if they try it, they will see the benefits for

00:23:09.060 --> 00:23:10.120
 themselves.

00:23:10.120 --> 00:23:14.120
 That's all you need to do.

00:23:14.120 --> 00:23:18.120
 So,

00:23:18.120 --> 00:23:22.120
 well, that's the Dhamma for tonight.

00:23:22.120 --> 00:23:32.120
 You guys can go.

00:23:32.120 --> 00:23:33.120
 [

00:23:33.120 --> 00:23:44.120
 silence ]

00:23:44.120 --> 00:23:49.120
 Okay, Larry has a question.

00:23:49.120 --> 00:23:53.120
 Well, noting posture, movements, and tensions,

00:23:53.120 --> 00:23:55.120
 I might lapse into contemplating death

00:23:55.120 --> 00:23:58.120
 or contemplating my good fortune,

00:23:58.120 --> 00:24:01.120
 and I realize I'm contemplating and not noting.

00:24:01.120 --> 00:24:04.120
 How should one's balance the process of noting

00:24:04.120 --> 00:24:08.120
 and the process of also contemplation?

00:24:08.120 --> 00:24:13.120
 Well, if that thought arises, that's fine,

00:24:13.120 --> 00:24:15.120
 you can do both, you just have the thought

00:24:15.120 --> 00:24:17.120
 and then you say thinking, thinking,

00:24:17.120 --> 00:24:22.120
 or if you feel happy, you can say happy, happy.

00:24:22.120 --> 00:24:28.120
 I mean, the thought arises on its own,

00:24:28.120 --> 00:24:31.120
 we're just trying to be mindful of it,

00:24:31.120 --> 00:24:38.120
 because even wholesomeness can be caught up in delusion,

00:24:38.120 --> 00:24:44.120
 not directly, but you can become unwholesome

00:24:44.120 --> 00:24:47.970
 about your wholesomeness if you start to get attached to it

00:24:47.970 --> 00:24:48.120
,

00:24:48.120 --> 00:24:52.120
 attached to the idea of it anyway.

00:24:52.120 --> 00:25:03.120
 [ silence ]

00:25:03.120 --> 00:25:07.120
 I mean, it won't lead to freedom,

00:25:07.120 --> 00:25:12.120
 so you can switch back and forth,

00:25:12.120 --> 00:25:14.120
 for the death one especially,

00:25:14.120 --> 00:25:19.120
 if you're mindful of death, that's a useful meditation,

00:25:19.120 --> 00:25:22.120
 it's good to give you the impetus to practice,

00:25:22.120 --> 00:25:25.120
 it gives you those religious feeling,

00:25:25.120 --> 00:25:29.120
 sanwega we call it,

00:25:29.120 --> 00:25:31.120
 so that's mindful, it's a different meditation,

00:25:31.120 --> 00:25:33.120
 it's useful to practice that

00:25:33.120 --> 00:25:35.120
 and then go back to practicing mindfulness.

00:25:35.120 --> 00:25:44.120
 As far as the contemplation of good fortune,

00:25:44.120 --> 00:25:50.120
 be careful of that, because it can slip into complacency.

00:25:50.120 --> 00:25:53.120
 The angels think like that, they think everything,

00:25:53.120 --> 00:26:00.120
 they think they've got some safety or so on,

00:26:00.120 --> 00:26:02.120
 they probably are not having that problem,

00:26:02.120 --> 00:26:04.120
 but you have to be careful about that,

00:26:04.120 --> 00:26:08.120
 I'm not convinced that it's wholesome.

00:26:08.120 --> 00:26:10.120
 We talk about contentment,

00:26:10.120 --> 00:26:12.120
 there's something in there that's probably

00:26:12.120 --> 00:26:15.120
 associated with contentment,

00:26:15.120 --> 00:26:20.120
 but appreciation is a lot like liking,

00:26:20.120 --> 00:26:26.120
 and clinging, because things can change at any time,

00:26:26.120 --> 00:26:30.120
 your safety is completely impermanent,

00:26:30.120 --> 00:26:38.120
 so it's an illusion, there's no safety in the samsara.

00:26:38.120 --> 00:26:42.120
 Everything can leave you in the moment,

00:26:42.120 --> 00:26:45.120
 so in what way is it safe?

00:26:45.120 --> 00:26:47.120
 In the end it's all just seeing, hearing,

00:26:47.120 --> 00:26:55.120
 smelling, tasting, feeling, thinking.

00:26:55.120 --> 00:26:58.120
 I'm continuing the series on the Dhammapada,

00:26:58.120 --> 00:27:01.120
 it's probably the only reason I continue them

00:27:01.120 --> 00:27:04.120
 is because people ask me the questions like this,

00:27:04.120 --> 00:27:05.120
 because I always think,

00:27:05.120 --> 00:27:08.120
 "Oh well, maybe nobody wants them anymore,

00:27:08.120 --> 00:27:11.120
 I haven't heard, nobody's asked about them in a while,

00:27:11.120 --> 00:27:19.120
 so probably people are sick of them."

00:27:19.120 --> 00:27:23.120
 I was busy with finals, but it's kind of just,

00:27:23.120 --> 00:27:25.120
 well now I'm not, so sure I can start up

00:27:25.120 --> 00:27:33.120
 the Dhammapada series again.

00:27:33.120 --> 00:27:37.120
 I mean it's not that I stopped, I can continue it,

00:27:37.120 --> 00:27:45.120
 do more Dhammapada if people want it.

00:27:45.120 --> 00:27:47.120
 How is it that we bow down to and revere

00:27:47.120 --> 00:27:51.120
 the meditation practice, I mean the practice itself?

00:27:51.120 --> 00:27:53.120
 Should we hold the triple gem or the practice

00:27:53.120 --> 00:27:56.120
 in mind for reverence?

00:27:56.120 --> 00:27:58.120
 Well I don't think you need to hold anything

00:27:58.120 --> 00:28:00.120
 in mind for reverence,

00:28:00.120 --> 00:28:03.120
 it's just taking it seriously.

00:28:03.120 --> 00:28:05.120
 I mean you can do meditations on the Buddha,

00:28:05.120 --> 00:28:08.120
 the Dhamma and the Sangha obviously,

00:28:08.120 --> 00:28:10.120
 but that's not what I don't think what the Buddha's saying,

00:28:10.120 --> 00:28:13.120
 he's just if they don't respect,

00:28:13.120 --> 00:28:15.120
 because there's a lot of disrespect,

00:28:15.120 --> 00:28:18.120
 disrespect for the Sangha for example,

00:28:18.120 --> 00:28:21.120
 monks get a lot of disrespect,

00:28:21.120 --> 00:28:25.120
 all of it, teachers get a lot of disrespect,

00:28:25.120 --> 00:28:28.120
 not a lot, not mostly, mostly respect,

00:28:28.120 --> 00:28:31.120
 but there's always people who have very little respect

00:28:31.120 --> 00:28:38.120
 and the point is well that is harmful to the,

00:28:38.120 --> 00:28:43.120
 you know, you could say respect has to be earned,

00:28:43.120 --> 00:28:48.120
 sure fine, but there's something about being respectful,

00:28:48.120 --> 00:28:52.120
 if you don't want to practice,

00:28:52.120 --> 00:28:53.120
 you don't have to come practice,

00:28:53.120 --> 00:28:58.120
 but if someone's teaching, you know,

00:28:58.120 --> 00:29:02.120
 there's a lot of, and then there's disrespect to the Buddha

00:29:02.120 --> 00:29:02.120
,

00:29:02.120 --> 00:29:09.120
 disrespect to the Dhamma,

00:29:09.120 --> 00:29:13.120
 you know, not taking it seriously,

00:29:13.120 --> 00:29:16.120
 disrespect, not necessarily disrespect, right,

00:29:16.120 --> 00:29:19.120
 but not taking it at practice seriously,

00:29:19.120 --> 00:29:22.810
 you know, people who do walking meditation, talking on the

00:29:22.810 --> 00:29:24.120
 phone,

00:29:24.120 --> 00:29:28.120
 and it's not even how disrespectful that is,

00:29:28.120 --> 00:29:31.120
 it's just, you know, if you don't take it seriously,

00:29:31.120 --> 00:29:33.120
 you're not going to get any results,

00:29:33.120 --> 00:29:35.120
 so the question if you do an hour of meditation,

00:29:35.120 --> 00:29:37.120
 how much are you really meditating,

00:29:37.120 --> 00:29:40.120
 are you really taking it seriously,

00:29:40.120 --> 00:29:42.120
 are you, do you respect,

00:29:42.120 --> 00:29:45.120
 and it's not respect in terms of disrespect,

00:29:45.120 --> 00:29:48.120
 it's like do you appreciate, that's the best,

00:29:48.120 --> 00:29:49.120
 do you appreciate,

00:29:49.120 --> 00:29:52.120
 and Garo means heavy, so it's like taking it seriously,

00:29:52.120 --> 00:29:54.120
 seeing it as a weighty thing,

00:29:54.120 --> 00:29:57.120
 or do you see it, do you take it lightly, right,

00:29:57.120 --> 00:30:00.120
 it's the opposite of taking something lightly,

00:30:00.120 --> 00:30:02.120
 if you take meditation lightly,

00:30:02.120 --> 00:30:05.120
 it's hard, you won't get the results,

00:30:05.120 --> 00:30:08.120
 if you don't get the results, Buddhism will pass away.

00:30:08.120 --> 00:30:20.120
 [Silence]

00:30:20.120 --> 00:30:22.840
 But yeah, if you want to do this, if you want to pay

00:30:22.840 --> 00:30:23.120
 respect,

00:30:23.120 --> 00:30:26.120
 like there's a quick chant that we do,

00:30:26.120 --> 00:30:28.120
 we do it in the opening ceremony,

00:30:28.120 --> 00:30:32.120
 so I often do it as a, kind of like a mantra really,

00:30:32.120 --> 00:30:34.120
 we pay respect to the five things,

00:30:34.120 --> 00:30:36.120
 the Buddha, the Dhamma, the Sangha,

00:30:36.120 --> 00:30:38.120
 the meditation practice,

00:30:38.120 --> 00:30:42.120
 and the teacher who offers the meditation practice.

00:30:42.120 --> 00:30:45.120
 Namami Buddha Ngunasa Garantang,

00:30:45.120 --> 00:30:48.120
 [Silence]

00:30:48.120 --> 00:30:52.120
 Namami Dhamma Muniraja Desitang,

00:30:52.120 --> 00:30:55.120
 Namami Sangham Muniraja Savakang,

00:30:55.120 --> 00:30:59.120
 Namami Kamathanak Nivana Dikamupayang,

00:30:59.120 --> 00:31:03.120
 Namami Kamathanak Jayakacharya Nivana Magudesa Kang,

00:31:03.120 --> 00:31:07.120
 Sabangdo Sang Kamantuno,

00:31:07.120 --> 00:31:11.120
 the last part, Sabangdo Sang for all faults,

00:31:11.120 --> 00:31:17.120
 for all faults, Kamantuno, may they forgive us.

00:31:17.120 --> 00:31:19.120
 May they forgive us all,

00:31:19.120 --> 00:31:30.120
 discretions, any wrongdoing.

00:31:30.120 --> 00:31:32.120
 It's in the opening ceremony,

00:31:32.120 --> 00:31:34.120
 I'm not sure where you get the opening ceremony.

00:31:34.120 --> 00:31:36.120
 I think it's in our,

00:31:36.120 --> 00:31:38.120
 where is the opening ceremony?

00:31:38.120 --> 00:31:42.120
 Maybe it's not even online.

00:31:42.120 --> 00:31:45.120
 But it might be taken from the Visuddhi manga,

00:31:45.120 --> 00:31:49.120
 a lot of that is taken from the Visuddhi manga.

00:31:49.120 --> 00:32:00.120
 Maybe not.

00:32:00.120 --> 00:32:05.120
 Hmm.

00:32:05.120 --> 00:32:09.120
 Yeah, respect for the five.

00:32:09.120 --> 00:32:11.120
 It's what we do before we start the minute.

00:32:11.120 --> 00:32:15.120
 It's the first thing we do in the opening ceremony,

00:32:15.120 --> 00:32:22.120
 almost the first thing, first big thing.

00:32:22.120 --> 00:32:24.120
 No, no, this is the first thing, isn't it?

00:32:24.120 --> 00:32:26.120
 Yeah, it's how we start the opening ceremony

00:32:26.120 --> 00:32:27.120
 and the closing ceremony.

00:32:27.120 --> 00:32:31.120
 So I don't do ceremonies here, not yet anyway.

00:32:31.120 --> 00:32:33.120
 But normally we'd have a ceremony

00:32:33.120 --> 00:32:36.120
 where we go through all of this in Pali.

00:32:36.120 --> 00:32:39.120
 It's quite nice.

00:32:39.120 --> 00:32:42.120
 The kind of, it's the thing,

00:32:42.120 --> 00:32:43.120
 you should take it seriously.

00:32:43.120 --> 00:32:44.120
 If we were to take it seriously,

00:32:44.120 --> 00:32:47.120
 we probably should do the ceremonies.

00:32:47.120 --> 00:32:50.120
 But you know, in the West, sometimes, again,

00:32:50.120 --> 00:32:52.120
 people don't take it seriously enough.

00:32:52.120 --> 00:32:57.120
 It's hard to push that on them.

00:32:57.120 --> 00:32:59.120
 It's not entirely necessary.

00:32:59.120 --> 00:33:00.120
 It doesn't mean you do a ceremony.

00:33:00.120 --> 00:33:03.120
 That's the thing, it's just a ceremony.

00:33:03.120 --> 00:33:07.120
 But something to consider once we get established here.

00:33:07.120 --> 00:33:08.120
 Maybe I can teach Michael,

00:33:08.120 --> 00:33:10.120
 because another thing is you need two people.

00:33:10.120 --> 00:33:15.120
 I do part, but I need a layperson to lead the meditators.

00:33:15.120 --> 00:33:17.120
 I need someone to lead the meditators.

00:33:17.120 --> 00:33:20.120
 I'll teach Michael how to do it someday.

00:33:20.120 --> 00:33:24.120
 If he sticks around.

00:33:24.120 --> 00:33:25.120
 Maybe he'll become a monk.

00:33:25.120 --> 00:33:30.120
 If he does, I get to give him the name Mogaraja again.

00:33:30.120 --> 00:33:33.120
 Because it's the closest thing to Michael, right?

00:33:33.120 --> 00:33:37.120
 So our last Michael got the name Mogaraja,

00:33:37.120 --> 00:33:41.120
 and boy did that cause a stir.

00:33:41.120 --> 00:33:43.800
 You know, Sri Lankan people said they didn't want to bow

00:33:43.800 --> 00:33:45.120
 down to it.

00:33:45.120 --> 00:33:51.120
 Because the word Moga means useless or bad or stupid.

00:33:51.120 --> 00:33:54.120
 But Mogaraja was one of the Buddha's chief disciples,

00:33:54.120 --> 00:33:57.120
 one of the 80 great disciples.

00:33:57.120 --> 00:34:06.120
 Shame, really.

00:34:06.120 --> 00:34:12.120
 Yeah, I have the five reverences somewhere.

00:34:12.120 --> 00:34:17.500
 Not sure where, but probably buried away somewhere in my

00:34:17.500 --> 00:34:19.120
 documents folder.

00:34:19.120 --> 00:34:26.120
 You copy them.

00:34:26.120 --> 00:34:29.120
 Anyway, that's all for tonight.

00:34:29.120 --> 00:34:33.120
 Thank you all for tuning in.

00:34:33.120 --> 00:34:36.120
 Have a good night.

00:34:37.120 --> 00:34:40.120
 Thank you.

