1
00:00:00,000 --> 00:00:06,260
 Okay, and we're live. So welcome back to our suta study.

2
00:00:06,260 --> 00:00:07,000
 Today we will be finishing up

3
00:00:07,000 --> 00:00:13,470
 hopefully the Juddha Hati Padaw, Juddha Hati Padaw Palisudd

4
00:00:13,470 --> 00:00:15,600
ha. I'm going to do Minikai

5
00:00:15,600 --> 00:00:21,720
 number 27, picking up where we left off on Thursday of last

6
00:00:21,720 --> 00:00:23,600
 week. So as usual, we will

7
00:00:24,720 --> 00:00:30,000
 be, once we get started, we'll be reading the, chanting the

8
00:00:30,000 --> 00:00:31,720
 Pali, and then reading and

9
00:00:31,720 --> 00:00:36,190
 discussing the English. There's not much left in this suta,

10
00:00:36,190 --> 00:00:38,160
 actually it might be a short

11
00:00:38,160 --> 00:00:42,260
 one. The discussion might be short because it's just going

12
00:00:42,260 --> 00:00:44,240
 to go over the four jhanas.

13
00:00:44,240 --> 00:00:48,620
 There's a couple of paragraphs that are interesting. So,

14
00:00:48,620 --> 00:00:51,240
 yeah, I guess we can just start.

15
00:00:51,240 --> 00:00:58,240
 Namo Tasa Bhagavatu Vahratu Samhasa Sambuddhasa Namo Tasa

16
00:00:58,240 --> 00:00:58,240
 Bhagavatu Vahratu Samhasa Sambuddhasa

17
00:01:15,320 --> 00:01:22,320
 Namo Tasa Bhagavatu Vahratu Sambuddhasa So, Imina Jhaariy

18
00:01:22,320 --> 00:01:22,320
ana, Silha Khande, Namahana

19
00:01:22,320 --> 00:01:34,760
 Ghatu Imina Jhaariyana, Sandhurtiya, Samhana Ghatu Imina J

20
00:01:34,760 --> 00:01:34,760
haariyana, Indriya Sambariyana,

21
00:01:40,920 --> 00:01:47,920
 Sanhara Khande, Sanhara Khande, Sanhara Khande, Sanhara K

22
00:01:47,920 --> 00:01:47,920
hande, Sanhara Khande, Sanhara

23
00:01:47,920 --> 00:01:58,260
 Khande, Sanhara Khande, Sanhara Khande, Sanhara Khande, San

24
00:01:58,260 --> 00:02:00,200
hara Khande, Sanhara Khande, Sanhara

25
00:02:06,200 --> 00:02:11,990
 Khande, Sanhara Khande, Sanhara Khande, Sanhara Khande, San

26
00:02:11,990 --> 00:02:13,200
hara Khande, Sanhara Khande, Sanhara

27
00:02:13,200 --> 00:02:17,500
 Khande, Sanhara Khande, Sanhara Khande, Sanhara Khande, San

28
00:02:17,500 --> 00:02:20,200
hara Khande, Sanhara Khande, Sanhara

29
00:02:20,200 --> 00:02:29,390
 Khande, Sanhara Khande, Sanhara Khande, Sanhara Khande, San

30
00:02:29,390 --> 00:02:30,200
hara Khande, Sanhara Khande, Sanhara

31
00:02:30,200 --> 00:02:35,320
 Khande, Sanhara Khande, Sanhara Khande, Sanhara Khande, San

32
00:02:35,320 --> 00:02:37,200
hara Khande, Sanhara Khande,

33
00:02:37,200 --> 00:02:44,790
 Sanhara Khande, Sanhara Khande, Sanhara Khande, Sanhara K

34
00:02:44,790 --> 00:02:49,200
hande, Sanhara Khande, Sanhara

35
00:02:55,200 --> 00:03:00,320
 Khande, Sanhara Khande, Sanhara Khande, Sanhara Khande, San

36
00:03:00,320 --> 00:03:02,200
hara Khande, Sanhara Khande, Sanhara

37
00:03:02,200 --> 00:03:06,790
 Khande, Sanhara Khande, Sanhara Khande, Sanhara Khande, San

38
00:03:06,790 --> 00:03:09,200
hara Khande, Sanhara Khande, Sanhara

39
00:03:47,200 --> 00:04:03,740
 Khande, Sanhara Khande, Sanhara Khande, Sanhara Khande, San

40
00:04:03,740 --> 00:04:05,200
hara Khande, Sanhara Khande, Sanhara

41
00:04:10,200 --> 00:04:23,200
 Khande, Sanhara Khande, Sanhara Khande, Sanhara Khande, San

42
00:04:23,200 --> 00:04:24,200
hara Khande, Sanhara Khande, Sanhara

43
00:05:23,200 --> 00:05:39,820
 Khande, Sanhara Khande, Sanhara Khande, Sanhara Khande, San

44
00:05:39,820 --> 00:05:41,200
hara Khande, Sanhara Khande, Sanhara

45
00:06:03,200 --> 00:06:13,520
 Khande, Sanhara Khande, Sanhara Khande, Sanhara Khande, San

46
00:06:13,520 --> 00:06:17,200
hara Khande, Sanhara Khande, Sanhara

47
00:07:32,200 --> 00:07:49,270
 Khande, Pariya Date, Anangane, Vigatupakilese, Mudubhute,

48
00:07:49,270 --> 00:07:53,450
 Kamaniye, Tirtayane, Japate, Bhuveniwasa, Nusa Tinyanaya, T

49
00:07:53,450 --> 00:07:54,200
ritama, Bhininan, Meeti

50
00:07:55,200 --> 00:08:05,440
 Sohane kawihita, Bhuveniwasa, Nusa Tinyanaya, Tirthanke,

51
00:08:05,440 --> 00:08:08,200
 Kampijatin, Vibhijati, Yopay

52
00:08:09,200 --> 00:08:19,350
 Sakaran saudhe, Sanghani kawihita, Bhuveniwasa, Nusa Tinyan

53
00:08:19,350 --> 00:08:24,200
aya, Tirthanke, Vibhijatin, Ramana, Tirthanke, Tirthanke, T

54
00:08:24,200 --> 00:08:24,200
irthipi

55
00:08:25,200 --> 00:08:40,800
 Tathagatani sevi, Tangeetipi, Tathagataranjeetangeetipi, N

56
00:08:40,800 --> 00:08:41,920
athwayvatava, Aryasa wakho, Nitham gat chati, Samma sambudho

57
00:08:41,920 --> 00:08:42,200
, Bhagavat

58
00:08:43,200 --> 00:08:56,740
 Dhammoh suvartipanno, Bhagavat dosa wakha sangho, Sohayav

59
00:08:56,740 --> 00:09:00,270
ang samahite, Chittay parisunde pariodate, Anangane, Vigatup

60
00:09:00,270 --> 00:09:05,200
akilese, Mudubhute, Kamaniye,

61
00:09:06,200 --> 00:09:16,770
 Tirthanke, Sanghani kawihita, Bhuveniwasa, Nusa Tinyanaya,

62
00:09:16,770 --> 00:09:18,710
 Tirthanke, Nusa Tinyanaya, Tirthanke, Vibhijatanjeetipi, N

63
00:09:18,710 --> 00:09:19,200
itham gat jnidhavat

64
00:09:20,200 --> 00:09:47,200
 Dhammoh suv

65
00:09:47,200 --> 00:10:04,510
 Dhammoh suvartipanno, Bhagavat dosa wakha sangho, Sohayav

66
00:10:04,510 --> 00:10:13,180
ang samahite, Chittay parisunde pariodate, Anangane, Vigatup

67
00:10:13,180 --> 00:10:15,200
akilese, Mudubhute, Kamaniye,

68
00:10:19,200 --> 00:10:28,170
 Anangana, Vigatupakilese, Nusa Tinyanaya, Tirthanke, Vibhij

69
00:10:28,170 --> 00:10:31,200
atam, Nusa Tinyanaya, Sohayavang samahite,

70
00:10:32,200 --> 00:10:46,820
 Anangana, Vigatupakilese, Nusa Tinyanaya, Tirthanke, Vibhij

71
00:10:46,820 --> 00:10:50,200
atam, Nusa Tinyanaya, Tirthanke,

72
00:10:51,200 --> 00:11:02,540
 Vigatupakilese, Nusa Tinyanaya, Tirthanke, Vigatupakilese,

73
00:11:02,540 --> 00:11:04,200
 Nusa Tinyanaya, Tirthanke,

74
00:11:05,200 --> 00:11:25,200
 Vigatupakilese, Nusa Tinyanaya, Tirthanke, Vigatupakilese,

75
00:11:25,200 --> 00:11:25,200
 Nusa Tinyanaya, Tirthanke,

76
00:11:26,200 --> 00:11:39,200
 Vigatupakilese, Nusa Tinyanaya, Tirthanke, Vigatupakilese,

77
00:11:39,200 --> 00:11:39,200
 Nusa Tinyanaya, Tirthanke,

78
00:11:40,200 --> 00:12:00,200
 Vigatupakilese, Nusa Tinyanaya, Tirthanke, Vigatupakilese,

79
00:12:00,200 --> 00:12:00,200
 Nusa Tinyanaya, Tirthanke,

80
00:12:01,200 --> 00:12:14,200
 Vigatupakilese, Nusa Tinyanaya, Tirthanke, Vigatupakilese,

81
00:12:14,200 --> 00:12:14,200
 Nusa Tinyanaya,

82
00:12:15,200 --> 00:12:27,730
 Vigatupakilese, Nusa Tinyanaya, Tirthanke, Vigatupakilese,

83
00:12:27,730 --> 00:12:32,200
 Nusa Tinyanaya, Tirthanke,

84
00:12:33,200 --> 00:12:46,200
 Vigatupakilese, Nusa Tinyanaya, Tirthanke, Vigatupakilese,

85
00:12:46,200 --> 00:12:46,200
 Nusa Tinyanaya,

86
00:12:47,200 --> 00:12:48,060
 Tirthanke, Tirthanke, Vigatupi, Eta Vatako Brahmanaliya Sav

87
00:12:48,060 --> 00:12:48,280
akk

88
00:12:48,280 --> 00:13:06,050
 Nirthangatohoti, Sammasambhudoh Bhagavat, Swakato Bhagavat

89
00:13:06,050 --> 00:13:06,280
adamosupartipanno,

90
00:13:06,280 --> 00:13:15,640
 Bhagavatoshavakasangoti, Eta Vatako Brahmanali padopamo viy

91
00:13:15,640 --> 00:13:17,280
arena paripurohot,

92
00:13:17,280 --> 00:13:24,280
 Vigatupakilese, Nusa Tinyanaya, Tirthanke, Vigatupakilese,

93
00:13:24,280 --> 00:13:44,560
 Vigatap

94
00:13:45,280 --> 00:13:52,280
 Nusa Tinyanaya, Tirthanke, Vigatupakasangoti,

95
00:13:52,280 --> 00:14:00,280
 Vigatupakasangoti, Sammasambhudoh Bhagavatamosupartipanno,

96
00:14:00,280 --> 00:14:08,280
 Vigatupakasangoti, Sammasambhudoh Bhagavatamosupartipanno,

97
00:14:09,280 --> 00:14:16,280
 Vigatupakasangoti, Sammasambhudoh Bhagavatamoh,

98
00:14:16,280 --> 00:14:24,280
 Vigatupakasangoti, Sammasambhudoh Bhagavatamosupartipanno,

99
00:14:24,280 --> 00:14:32,280
 Vigatupakasangoti, Sammasambhudoh Bhagavatamosupartipanno,

100
00:14:33,280 --> 00:14:40,090
 Hi, I'm okay, actually. So now we're in the... there's not

101
00:14:40,090 --> 00:14:40,280
 much here,

102
00:14:40,280 --> 00:14:43,280
 it should go by pretty quickly. The first bit is kind of

103
00:14:43,280 --> 00:14:45,280
 interesting, so go ahead.

104
00:14:45,280 --> 00:14:50,280
 So again, first of all, again, we're continuing on from

105
00:14:50,280 --> 00:14:51,280
 after morality,

106
00:14:51,280 --> 00:14:55,720
 the Buddha's starting to give some background on the

107
00:14:55,720 --> 00:14:58,280
 practice that leads one to be capable

108
00:14:59,280 --> 00:15:03,330
 and determining whether to know that this teaching is

109
00:15:03,330 --> 00:15:05,280
 really the true teaching.

110
00:15:05,280 --> 00:15:09,690
 So just like whether you can judge when and how you can

111
00:15:09,690 --> 00:15:12,280
 judge whether the elephant that you're tracking

112
00:15:12,280 --> 00:15:16,280
 is really a bull elephant, really the head of the herd.

113
00:15:16,280 --> 00:15:20,840
 Possessing this aggregate of noble virtue and this noble

114
00:15:20,840 --> 00:15:22,280
 restraint of the faculties

115
00:15:23,280 --> 00:15:26,880
 and possessing this noble mindfulness and full awareness,

116
00:15:26,880 --> 00:15:29,280
 he resorts to a scheduled resting place,

117
00:15:29,280 --> 00:15:35,120
 to a secluded resting place, the forest, the root of a tree

118
00:15:35,120 --> 00:15:37,280
, a mountain, a ravine,

119
00:15:37,280 --> 00:15:42,420
 a hillside cave, a charnel ground, a jungle thicket, an

120
00:15:42,420 --> 00:15:44,280
 open space, a heap of straw.

121
00:15:45,280 --> 00:15:53,730
 He doesn't often use this long form of... it must be

122
00:15:53,730 --> 00:15:57,280
 elsewhere, but it's not the standard phrase to use.

123
00:15:57,280 --> 00:16:02,280
 Giving all these options. Going to the forest, root of a

124
00:16:02,280 --> 00:16:04,280
 tree, a mountain, a ravine.

125
00:16:04,280 --> 00:16:11,730
 Doesn't even mention Akuti. So the idea is here that the

126
00:16:11,730 --> 00:16:14,280
 best thing would be to just go up and live in nature.

127
00:16:16,280 --> 00:16:19,280
 Or preferably where there's not too many mosquitoes.

128
00:16:19,280 --> 00:16:22,280
 There's people who can give you food.

129
00:16:22,280 --> 00:16:25,090
 There's people who can give you food, but that's not so

130
00:16:25,090 --> 00:16:26,280
 difficult to find.

131
00:16:26,280 --> 00:16:29,400
 You don't need that much food, especially when you're just

132
00:16:29,400 --> 00:16:30,280
 meditating.

133
00:16:30,280 --> 00:16:36,480
 Or the way of thinking of this is the time during the day

134
00:16:36,480 --> 00:16:40,280
 when the person is meditating.

135
00:16:41,280 --> 00:16:47,280
 So it's not necessarily referring to the dwelling place.

136
00:16:47,280 --> 00:16:52,280
 Once one has lived in the monastery and undertaken morality

137
00:16:52,280 --> 00:16:54,930
, then one should leave the monastery and go off into the

138
00:16:54,930 --> 00:16:55,280
 forest

139
00:16:55,280 --> 00:16:58,380
 or go off into a place suitable outside of the monastery

140
00:16:58,380 --> 00:16:59,280
 but nearby.

141
00:16:59,280 --> 00:17:03,280
 So one may actually be staying in Akuti somewhere.

142
00:17:03,280 --> 00:17:07,000
 But quite often they would just live in, maybe in robe

143
00:17:07,000 --> 00:17:08,280
 tents or something.

144
00:17:09,280 --> 00:17:11,800
 Turn their tents into robes. It was a common thing.

145
00:17:11,800 --> 00:17:12,280
 Apparently, Sariputta did that one.

146
00:17:12,280 --> 00:17:15,280
 Turn their robes into tents.

147
00:17:15,280 --> 00:17:18,280
 Can you put one of the robes up on a cord or something?

148
00:17:18,280 --> 00:17:20,280
 Turn their tents into robes.

149
00:17:20,280 --> 00:17:22,280
 Turn their robes into tents.

150
00:17:22,280 --> 00:17:26,470
 On returning from his alms round, after his meal, he sits

151
00:17:26,470 --> 00:17:30,420
 down, folding his legs crosswise, setting his body erect,

152
00:17:30,420 --> 00:17:33,280
 and establishing mindfulness before him.

153
00:17:34,280 --> 00:17:37,440
 Abandoning covetousness for the world, he abides with the

154
00:17:37,440 --> 00:17:40,640
 mind free from covetousness. He purifies his mind from cove

155
00:17:40,640 --> 00:17:41,280
tousness.

156
00:17:41,280 --> 00:17:45,570
 Abandoning ill will and hatred, he abides with the mind

157
00:17:45,570 --> 00:17:48,620
 free from ill will. Compassionate for the welfare of all

158
00:17:48,620 --> 00:17:51,580
 living beings, he purifies his mind from ill will and

159
00:17:51,580 --> 00:17:52,280
 hatred.

160
00:17:53,280 --> 00:17:57,140
 Abandoning sloth and torpor, he abides free from sloth and

161
00:17:57,140 --> 00:18:01,190
 torpor. Percipients of light, mindful and fully aware, he

162
00:18:01,190 --> 00:18:04,280
 purifies his mind from sloth and torpor.

163
00:18:04,280 --> 00:18:07,400
 Abandoning restlessness and remorse, he abides unagitated

164
00:18:07,400 --> 00:18:11,090
 with the mind inwardly peaceful. He purifies his mind from

165
00:18:11,090 --> 00:18:13,280
 restlessness and remorse.

166
00:18:13,280 --> 00:18:17,430
 Abandoning doubt, he abides having gone beyond doubt. Un

167
00:18:17,430 --> 00:18:20,700
 perplexed about wholesome states, he purifies his mind from

168
00:18:20,700 --> 00:18:21,280
 doubt.

169
00:18:22,280 --> 00:18:25,950
 Okay, so two parts. The first part is discussing how the

170
00:18:25,950 --> 00:18:30,280
 person should sit during meditation, which is common.

171
00:18:30,280 --> 00:18:34,280
 So first thing first is you have to eat first, right?

172
00:18:34,280 --> 00:18:38,820
 It's not true. So that doesn't mean you can't meditate

173
00:18:38,820 --> 00:18:40,870
 before you've had your one meal, because monks are eating

174
00:18:40,870 --> 00:18:41,280
 one meal a day.

175
00:18:41,280 --> 00:18:44,510
 Doesn't mean you can't meditate before that, but meditation

176
00:18:44,510 --> 00:18:47,570
 always goes better after the food, because it's the one

177
00:18:47,570 --> 00:18:49,280
 food that we'd have during the day.

178
00:18:50,280 --> 00:18:54,200
 And after that, then you have the energy to continue in the

179
00:18:54,200 --> 00:18:55,280
 meditation.

180
00:18:55,280 --> 00:18:58,600
 So folding your legs crosswise would be ideally sitting in

181
00:18:58,600 --> 00:19:01,550
 the full lotus position. That's what that's referring to

182
00:19:01,550 --> 00:19:03,280
 with your feet actually up on your hips.

183
00:19:03,280 --> 00:19:08,100
 And setting your body erect means actually getting the

184
00:19:08,100 --> 00:19:12,280
 spine aligned, one bone on top of the other.

185
00:19:16,280 --> 00:19:20,220
 Establishing mindfulness before him. Now this is not, we

186
00:19:20,220 --> 00:19:24,680
 often kind of trivialize this, because we're not expecting

187
00:19:24,680 --> 00:19:28,280
 the meditators to get powerful states of concentration.

188
00:19:28,280 --> 00:19:31,050
 So we'd suggest that you sit comfortably, if that means sl

189
00:19:31,050 --> 00:19:33,280
ouching a little bit. It's not really a problem.

190
00:19:34,280 --> 00:19:38,330
 Because obviously these are not in any way related to

191
00:19:38,330 --> 00:19:42,700
 understanding reality as it is, but they are related to the

192
00:19:42,700 --> 00:19:47,280
 acquisition of concentration of tranquility.

193
00:19:47,280 --> 00:19:51,210
 So if you're practicing to gain tranquility first, there's

194
00:19:51,210 --> 00:19:54,500
 heavy states of, these strong states of calm, which is

195
00:19:54,500 --> 00:19:58,580
 going to talk about here, then you really do need to take

196
00:19:58,580 --> 00:20:01,280
 care of such things as food and posture.

197
00:20:02,280 --> 00:20:05,650
 Because neither, even food isn't necessary for, or isn't

198
00:20:05,650 --> 00:20:09,670
 such a big deal for attainment of insight. But for tranqu

199
00:20:09,670 --> 00:20:11,280
ility it's important.

200
00:20:11,280 --> 00:20:13,630
 So then he talks about, what he's talking about here are

201
00:20:13,630 --> 00:20:16,520
 the five hindrances from here on in. You abandon the five

202
00:20:16,520 --> 00:20:20,870
 hindrances, abide free from the five hindrances and pur

203
00:20:20,870 --> 00:20:24,280
ifying your mind from the five hindrances.

204
00:20:25,280 --> 00:20:32,330
 They give some, each one has its own little unique phrase.

205
00:20:32,330 --> 00:20:37,770
 So abandoning co-dishness means desires for worldly things,

206
00:20:37,770 --> 00:20:43,280
 ill will, has to do with hatred, has to do with compassion,

207
00:20:43,280 --> 00:20:44,280
 so one becomes compassionate.

208
00:20:44,280 --> 00:20:48,280
 Sloth and torpor, one becomes a percipient of light.

209
00:20:49,280 --> 00:20:55,650
 Night is something that the Buddha talks about. It's a

210
00:20:55,650 --> 00:20:59,440
 curious statement, it has to do with having a bright mind,

211
00:20:59,440 --> 00:21:00,280
 you know.

212
00:21:03,280 --> 00:21:07,610
 There actually, there's a place where the Buddha talks

213
00:21:07,610 --> 00:21:12,600
 about when it's night, having the perception that it's like

214
00:21:12,600 --> 00:21:19,800
 daytime, somehow giving rise to energy, which is why often

215
00:21:19,800 --> 00:21:24,280
 we're encouraged to keep our lights on, turn your lights on

216
00:21:24,280 --> 00:21:25,280
 when you're doing meditation.

217
00:21:25,280 --> 00:21:29,280
 It stops you from falling asleep. It's a way of cheating.

218
00:21:30,280 --> 00:21:33,280
 I never did that.

219
00:21:33,280 --> 00:21:39,160
 It can't help at night. Some triggers, some darkness

220
00:21:39,160 --> 00:21:41,280
 triggers something in the mind.

221
00:21:41,280 --> 00:21:45,290
 Rest assists in remorse, one becomes unagitated, and

222
00:21:45,290 --> 00:21:47,280
 peaceful in working.

223
00:21:47,280 --> 00:21:51,320
 Abandoning doubt, one becomes un perplexed about wholesome

224
00:21:51,320 --> 00:21:55,240
 state, so one knows what's good and what's bad, and doesn't

225
00:21:55,240 --> 00:21:57,280
 have any doubt about them.

226
00:21:58,280 --> 00:22:00,990
 So this would have to do with repeated application of the

227
00:22:00,990 --> 00:22:04,720
 meditation practice, whether it's insight or tranquility,

228
00:22:04,720 --> 00:22:06,280
 it doesn't really matter.

229
00:22:06,280 --> 00:22:09,880
 The point is that you continuously apply the mind until the

230
00:22:09,880 --> 00:22:13,240
 mind straightens out and the mind begins to get into the

231
00:22:13,240 --> 00:22:16,280
 habit of having freedom from these five things.

232
00:22:16,280 --> 00:22:18,860
 If you're practicing insight, then it leads to insight, if

233
00:22:18,860 --> 00:22:21,390
 you're practicing tranquility, then it leads, as talked

234
00:22:21,390 --> 00:22:23,920
 about here, it leads first to all the states of tranquility

235
00:22:23,920 --> 00:22:24,280
.

236
00:22:25,280 --> 00:22:29,210
 Having thus abandoned these five hindrances, imperfections

237
00:22:29,210 --> 00:22:33,310
 of the mind that weaken wisdom, quite secluded from sensual

238
00:22:33,310 --> 00:22:37,380
 pleasures, secluded from unwholesome states, he enters upon

239
00:22:37,380 --> 00:22:40,640
 and abides in the first jhana, which is accompanied by

240
00:22:40,640 --> 00:22:43,890
 applied and sustained thoughts, with rapture and pleasure

241
00:22:43,890 --> 00:22:45,280
 born with seclusion.

242
00:22:46,280 --> 00:22:50,060
 This Brahman is called a footprint of the tathagata,

243
00:22:50,060 --> 00:22:53,320
 something scraped by the tathagata, something marked by the

244
00:22:53,320 --> 00:22:56,590
 tathagata, but a noble disciple does not yet come to the

245
00:22:56,590 --> 00:22:57,280
 conclusion.

246
00:22:57,280 --> 00:23:00,290
 The Blessed One is fully enlightened, the Dhamma is well

247
00:23:00,290 --> 00:23:03,510
 proclaimed by the Blessed One, the Sangha is practicing the

248
00:23:03,510 --> 00:23:04,280
 good way.

249
00:23:05,280 --> 00:23:09,110
 So if you remember what the original simile was that when

250
00:23:09,110 --> 00:23:12,430
 you see the big footprint, you should come to the

251
00:23:12,430 --> 00:23:15,280
 conclusion that that's a bull elephant.

252
00:23:15,280 --> 00:23:17,850
 And the Buddha said, "Well, actually, even with bull

253
00:23:17,850 --> 00:23:20,550
 elephants, that's not always the case, because it could

254
00:23:20,550 --> 00:23:23,480
 still be a very big she-elephant, and even when you see the

255
00:23:23,480 --> 00:23:26,440
 markings high up out of the tusks, you don't think that

256
00:23:26,440 --> 00:23:29,680
 only a bull elephant can do that, because young elephants

257
00:23:29,680 --> 00:23:32,780
 can also be very big and stand up on their hind legs and so

258
00:23:32,780 --> 00:23:33,280
 on."

259
00:23:34,280 --> 00:23:39,930
 So the same goes with the jhanas, one doesn't come to the

260
00:23:39,930 --> 00:23:46,280
 conclusion that the teacher of these things is enlightened,

261
00:23:46,280 --> 00:23:49,380
 or that this practice is the practice to enlighten them,

262
00:23:49,380 --> 00:23:51,950
 because people outside of the Buddhist teaching can also

263
00:23:51,950 --> 00:23:53,280
 cultivate these things.

264
00:23:53,280 --> 00:23:55,960
 It's not unique to Buddhism, by any means it's a

265
00:23:55,960 --> 00:23:59,100
 characteristic of tranquility meditation, which is

266
00:23:59,100 --> 00:24:02,180
 practiced in many different religious and spiritual

267
00:24:02,180 --> 00:24:03,280
 traditions.

268
00:24:03,280 --> 00:24:07,640
 We've talked about the jhanas, so then the second jhanas

269
00:24:07,640 --> 00:24:12,580
 are the same, the third jhanas are the same, the fourth jh

270
00:24:12,580 --> 00:24:16,740
ana is the same, we've talked about these before, or we've

271
00:24:16,740 --> 00:24:18,280
 read through these before anyway.

272
00:24:18,280 --> 00:24:21,530
 None of these are a reason to come to the conclusion that

273
00:24:21,530 --> 00:24:25,400
 the Buddha is enlightened, but the final one, now we have

274
00:24:25,400 --> 00:24:28,280
 the births, we've done these as well, right?

275
00:24:28,280 --> 00:24:30,220
 We've talked about reflecting men and bulls, even when you

276
00:24:30,220 --> 00:24:34,650
 remember past lives, we don't consider them, knowledge of

277
00:24:34,650 --> 00:24:38,570
 passing away and reappearing of beings, understand how

278
00:24:38,570 --> 00:24:41,970
 being passed away, we've read all this before in other sutt

279
00:24:41,970 --> 00:24:42,280
as.

280
00:24:43,280 --> 00:24:48,150
 So remembering past lives and seeing beings arise and pass

281
00:24:48,150 --> 00:24:52,420
 away, die from one birth and are born in another birth,

282
00:24:52,420 --> 00:24:55,520
 also not a sign of enlightenment, of a characteristic of

283
00:24:55,520 --> 00:24:59,610
 enlightenment, but the last one is, we have it, yeah, this

284
00:24:59,610 --> 00:25:00,280
 one here.

285
00:25:00,280 --> 00:25:01,090
 When his concentrated mind is thus purified, bright, unblem

286
00:25:01,090 --> 00:25:01,900
ished, rid of imperfection, malleable, willy, steady, and

287
00:25:01,900 --> 00:25:17,730
 attained to imperturbability, he directs it to knowledge of

288
00:25:17,730 --> 00:25:19,280
 the destruction of the taints.

289
00:25:19,280 --> 00:25:22,870
 He understands as it actually is, this is suffering, this

290
00:25:22,870 --> 00:25:26,010
 is the origin of suffering, this is the cessation of

291
00:25:26,010 --> 00:25:29,290
 suffering, this is the way leading to the cessation of

292
00:25:29,290 --> 00:25:33,680
 suffering, these are the taints, this is the origin of the

293
00:25:33,680 --> 00:25:37,680
 taints, this is the cessation of the taints, this is the

294
00:25:37,680 --> 00:25:39,280
 way leading to the cessation of the taints.

295
00:25:39,280 --> 00:25:42,460
 So you notice that he uses the same formula for suffering

296
00:25:42,460 --> 00:25:45,760
 as he does for the taints, and in the Samaditya suttas, Sar

297
00:25:45,760 --> 00:25:48,770
iputta goes through several of them, twelve I think

298
00:25:48,770 --> 00:25:50,280
 different things.

299
00:25:50,280 --> 00:25:54,280
 So you see that the formula is the important thing.

300
00:25:54,280 --> 00:25:57,040
 What is the cause, what is the problem, and what is the

301
00:25:57,040 --> 00:25:58,280
 cause of the problem?

302
00:25:58,280 --> 00:26:04,300
 What is the solution or the healthy state, the good state,

303
00:26:04,300 --> 00:26:09,280
 and what is the path that leads to the good state?

304
00:26:09,280 --> 00:26:12,620
 So there is the bad thing, cause of bad thing, good thing,

305
00:26:12,620 --> 00:26:14,280
 cause of good thing, same here.

306
00:26:14,280 --> 00:26:18,440
 Bad thing, the taints, cause of the bad thing, good thing,

307
00:26:18,440 --> 00:26:20,280
 cause of the good thing.

308
00:26:20,280 --> 00:26:28,430
 The good thing is also the absence of the bad thing. So you

309
00:26:28,430 --> 00:26:29,960
 could say bad thing, cause of bad thing, cessation of bad

310
00:26:29,960 --> 00:26:35,280
 thing, and paths leading to cessation of the bad thing.

311
00:26:35,280 --> 00:26:41,350
 So this one, now he's going to talk down here about it here

312
00:26:41,350 --> 00:26:46,280
, we didn't separate into two paragraphs.

313
00:26:46,280 --> 00:26:49,280
 Okay, here's the point, is that with the practice of Samad

314
00:26:49,280 --> 00:26:52,280
itya meditation, the mind becomes quite clear and bright.

315
00:26:52,280 --> 00:26:55,830
 So it is a good thing to practice first. You practice it

316
00:26:55,830 --> 00:26:58,380
 and your mind becomes imperturbable, then you can direct it

317
00:26:58,380 --> 00:26:59,280
 to the knowledge and destruction of the taints.

318
00:26:59,280 --> 00:27:02,800
 You don't need to exactly, as long as you get to a state of

319
00:27:02,800 --> 00:27:04,280
 imperturbability.

320
00:27:04,280 --> 00:27:07,460
 It doesn't have to be based on all this stuff above. You

321
00:27:07,460 --> 00:27:09,980
 don't have to remember your path, it helps to do all that

322
00:27:09,980 --> 00:27:13,850
 stuff, because your mind becomes so strong, that's very

323
00:27:13,850 --> 00:27:15,280
 quick to become enlightened.

324
00:27:15,280 --> 00:27:20,290
 If you go straight to the goal, you just get enough to see

325
00:27:20,290 --> 00:27:22,280
 things as they are.

326
00:27:22,280 --> 00:27:26,470
 You don't have to go through all the loops and whoops,

327
00:27:26,470 --> 00:27:30,820
 there are monks who said that they didn't have these things

328
00:27:30,820 --> 00:27:33,280
 and they still became arahats.

329
00:27:33,280 --> 00:27:36,960
 All you have to do is this one, understand suffering and

330
00:27:36,960 --> 00:27:40,530
 the cause of suffering and the cessation of suffering, the

331
00:27:40,530 --> 00:27:43,280
 way they do the cessation of suffering.

332
00:27:43,280 --> 00:27:48,340
 Same with the taints. The taints are all the defilements,

333
00:27:48,340 --> 00:27:53,170
 all the attachments that we have, all the things that

334
00:27:53,170 --> 00:27:57,020
 corrupt our mind, and they all come from, still from dhanha

335
00:27:57,020 --> 00:27:59,680
, still from some kind of craving for things to be this way

336
00:27:59,680 --> 00:28:01,280
 or that, or to not be this way or that.

337
00:28:01,280 --> 00:28:08,280
 Okay, and this one?

338
00:28:08,280 --> 00:28:12,280
 This too Brahman is called a footprint of the tathagata.

339
00:28:12,280 --> 00:28:15,620
 Oh wait, we're not there yet, so sorry he doesn't yet, wait

340
00:28:15,620 --> 00:28:19,280
, go ahead. This one's different, not different, go ahead.

341
00:28:19,280 --> 00:28:22,300
 Something marked by the tathagata, but our noble disciple

342
00:28:22,300 --> 00:28:25,120
 still has not yet come to the conclusion. The Blessed One

343
00:28:25,120 --> 00:28:28,250
 is fully enlightened, the dhamma is well proclaimed by the

344
00:28:28,250 --> 00:28:31,280
 Blessed One, the sangha is practicing the good way.

345
00:28:31,280 --> 00:28:35,280
 Rather he is in the process of coming to this conclusion.

346
00:28:35,280 --> 00:28:42,280
 Okay, so I'll let Bhikkhu Bodhi explain why this one's

347
00:28:42,280 --> 00:28:42,280
 different, it's special, but it's not the most special yet.

348
00:28:42,280 --> 00:28:48,180
 So this is the moment of the path coming to the conclusion,

349
00:28:48,180 --> 00:28:49,280
 nitha n

350
00:28:49,280 --> 00:28:55,100
 This is because there's one moment, the moment when one

351
00:28:55,100 --> 00:28:58,590
 experiences, realizes the Four Noble Truths, one hasn't

352
00:28:58,590 --> 00:28:59,280
 become free.

353
00:28:59,280 --> 00:29:06,010
 During that moment of realizing the Truth, there's no

354
00:29:06,010 --> 00:29:10,280
 knowledge of freedom, so it has to come to the next moment.

355
00:29:10,280 --> 00:29:13,570
 That's what the commentary says about that. There's other

356
00:29:13,570 --> 00:29:18,140
 ways I think to explain that, but that would be the

357
00:29:18,140 --> 00:29:21,280
 canonical, the orthodox interpretation.

358
00:29:21,280 --> 00:29:25,500
 Anyway, the point is, once he knows and sees thus, read the

359
00:29:25,500 --> 00:29:26,280
 next paragraph.

360
00:29:26,280 --> 00:29:29,710
 When he knows and sees thus, his mind is liberated from the

361
00:29:29,710 --> 00:29:33,540
 taint of central desire, from the taint of being, and from

362
00:29:33,540 --> 00:29:34,280
 the taint of ignorance.

363
00:29:34,280 --> 00:29:37,380
 When it is liberated, there comes to knowledge, it is

364
00:29:37,380 --> 00:29:40,840
 liberated. He understands, birth is destroyed, the holy

365
00:29:40,840 --> 00:29:44,280
 life has been lived, what had to be done has been done,

366
00:29:44,280 --> 00:29:46,280
 there is no more coming to any state of being.

367
00:29:46,280 --> 00:29:50,430
 Right, let me go read the next part, let me read it out

368
00:29:50,430 --> 00:29:51,280
 together.

369
00:29:51,280 --> 00:29:54,300
 This too Brahmin is called the footprint of the tathagata,

370
00:29:54,300 --> 00:29:57,490
 something scraped by the tathagata, something marked by the

371
00:29:57,490 --> 00:29:58,280
 tathagata.

372
00:29:58,280 --> 00:30:01,230
 It is at this point that a noble disciple has come to the

373
00:30:01,230 --> 00:30:04,690
 conclusion that Blessed One is fully enlightened, the Dham

374
00:30:04,690 --> 00:30:07,320
ma is well proclaimed by the Blessed One, the Sankha is

375
00:30:07,320 --> 00:30:09,280
 practicing the good way.

376
00:30:09,280 --> 00:30:12,220
 And it is at this point, Brahmin, that to similarly the

377
00:30:12,220 --> 00:30:15,280
 elephant's footprint has been completed in detail.

378
00:30:15,280 --> 00:30:18,650
 At that moment of realization, that is when one is coming

379
00:30:18,650 --> 00:30:22,280
 to the conclusion that this is the path to enlightenment.

380
00:30:22,280 --> 00:30:26,210
 The next moment, from that point on, one has already come

381
00:30:26,210 --> 00:30:30,290
 to the conclusion that this is the path to enlightenment,

382
00:30:30,290 --> 00:30:34,840
 this is the top, this is the bull elephant of religion, of

383
00:30:34,840 --> 00:30:36,280
 spiritual practice.

384
00:30:36,280 --> 00:30:41,840
 This one has realized for one's own. Not much to say there,

385
00:30:41,840 --> 00:30:45,280
 no? All of these are stock passages.

386
00:30:45,280 --> 00:30:47,820
 And we got something a little bit new here, I don't think

387
00:30:47,820 --> 00:30:49,280
 we've read this one before.

388
00:30:49,280 --> 00:31:02,630
 When this was said, the Raman Jahnu Soni said to Blessed

389
00:31:02,630 --> 00:31:02,810
 One, "Magnificent Master Gautama, magnificent Master Gaut

390
00:31:02,810 --> 00:31:02,930
ama, Master Gautama has made the Dhamma clear in many ways,

391
00:31:02,930 --> 00:31:04,280
 as though he were turning upright what had been overthrown,

392
00:31:04,280 --> 00:31:07,800
 revealing what was hidden, showing the way to one who was

393
00:31:07,800 --> 00:31:11,030
 lost, or holding up a lamp in the dark for those with eyes

394
00:31:11,030 --> 00:31:12,280
ight to see forms.

395
00:31:12,280 --> 00:31:16,230
 I go to Master Gautama for refuge and to the Dhamma and to

396
00:31:16,230 --> 00:31:20,310
 the Sankha of bhikkhus. From today, let Master Gautama

397
00:31:20,310 --> 00:31:23,080
 remember me as a lay follower who has gone to him for

398
00:31:23,080 --> 00:31:24,280
 refuge for life."

399
00:31:24,280 --> 00:31:29,940
 There's a stock passage and it'll change here. Everyone

400
00:31:29,940 --> 00:31:35,280
 seems to say the same thing exactly, but this changes.

401
00:31:35,280 --> 00:31:39,860
 Some of them ask to go forth, sometimes they will ask to go

402
00:31:39,860 --> 00:31:43,520
 forth as a monk, and in this case he just decided he just

403
00:31:43,520 --> 00:31:46,280
 asked to become a lay follower, which is also common.

404
00:31:46,280 --> 00:31:49,720
 It depends on who the person was. This is a stock passage,

405
00:31:49,720 --> 00:31:52,570
 it's quite poetic and it's quite apt really because you

406
00:31:52,570 --> 00:31:54,280
 feel that way when you practice.

407
00:31:54,280 --> 00:31:58,950
 It feels like someone had just turned upright would have

408
00:31:58,950 --> 00:32:03,280
 been flipped upside down, revealing what was hidden,

409
00:32:03,280 --> 00:32:06,500
 suddenly showing the lighting up a lamp in the dark for

410
00:32:06,500 --> 00:32:08,280
 those with eyesight to see.

411
00:32:08,280 --> 00:32:12,280
 Now you notice he still calls him Gautama.

412
00:32:12,280 --> 00:32:21,280
 That's his clan name.

413
00:32:21,280 --> 00:32:26,280
 Alright, so that's the end of the Chula Hati Padopama Sutta

414
00:32:26,280 --> 00:32:29,280
.

415
00:32:29,280 --> 00:32:38,980
 So, looking in there. Hey, we weren't singing, except we

416
00:32:38,980 --> 00:32:42,280
 were singing chanting.

417
00:32:42,280 --> 00:32:47,370
 Anyway, thank you all for tuning in and see you all next

418
00:32:47,370 --> 00:32:48,280
 time.

