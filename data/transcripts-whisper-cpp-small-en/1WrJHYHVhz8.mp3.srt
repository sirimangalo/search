1
00:00:00,000 --> 00:00:06,600
 I have recently experienced involuntary body vibrations

2
00:00:06,600 --> 00:00:09,000
 starting from my lower extremities

3
00:00:09,000 --> 00:00:11,760
 during my anaphonacity meditation.

4
00:00:11,760 --> 00:00:15,820
 I have to extend my feet from my meditative posture which I

5
00:00:15,820 --> 00:00:17,900
 felt disturbing my calmness

6
00:00:17,900 --> 00:00:18,980
 of my body.

7
00:00:18,980 --> 00:00:22,200
 Could you advise on this?

8
00:00:22,200 --> 00:00:33,040
 Sumedha, you want to start?

9
00:00:33,040 --> 00:00:36,800
 Could you go into just a little bit more detail with what

10
00:00:36,800 --> 00:00:39,560
 you mean by involuntary body vibrations?

11
00:00:39,560 --> 00:00:44,760
 Are you shaking, convulsing in the hips or legs?

12
00:00:44,760 --> 00:00:48,800
 Or is it kind of like a, what do you call it, sometimes the

13
00:00:48,800 --> 00:00:50,720
 legs sort of jolts a little

14
00:00:50,720 --> 00:00:51,720
 bit of a...

15
00:00:51,720 --> 00:01:01,280
 Vibrations, like you know sometimes when you hold it post

16
00:01:01,280 --> 00:01:05,720
urally you've got tension.

17
00:01:05,720 --> 00:01:07,720
 My feet were shaking and vibrating.

18
00:01:07,720 --> 00:01:15,520
 Well, I don't know what to say.

19
00:01:15,520 --> 00:01:20,090
 The first thing I'd say is not to move straight away but to

20
00:01:20,090 --> 00:01:22,920
 acknowledge the vibrations and

21
00:01:22,920 --> 00:01:25,910
 acknowledge whatever feeling arises, like if you're

22
00:01:25,910 --> 00:01:27,920
 frustrated or you're worried about

23
00:01:27,920 --> 00:01:33,890
 the vibrations and acknowledge the vibrations, not just to

24
00:01:33,890 --> 00:01:36,240
 move straight away.

25
00:01:36,240 --> 00:01:52,620
 My best girlfriend was sitting her first Vavasana course

26
00:01:52,620 --> 00:01:54,760
 and the teacher said, "Are you feeling

27
00:01:54,760 --> 00:01:56,520
 any kind of sensations?"

28
00:01:56,520 --> 00:01:57,520
 She said, "Yes."

29
00:01:57,520 --> 00:01:58,520
 "What kind?"

30
00:01:58,520 --> 00:02:00,520
 "Are you having this shaking?"

31
00:02:00,520 --> 00:02:04,480
 She was really shaking a lot.

32
00:02:04,480 --> 00:02:10,200
 My teacher said, "Well, stop."

33
00:02:10,200 --> 00:02:13,450
 I thought to myself, because I could hear the question

34
00:02:13,450 --> 00:02:14,920
 answers, "Oh gosh."

35
00:02:14,920 --> 00:02:16,800
 She didn't trust the teacher after that.

36
00:02:16,800 --> 00:02:19,180
 She didn't want to talk to the teacher after that because

37
00:02:19,180 --> 00:02:20,520
 she felt that if she couldn't

38
00:02:20,520 --> 00:02:24,000
 stop it, it was involuntary.

39
00:02:24,000 --> 00:02:32,330
 So I would say just to relax and maybe say shaking, shaking

40
00:02:32,330 --> 00:02:36,520
, shaking and just know that

41
00:02:36,520 --> 00:02:38,880
 you're not going to shake forever.

42
00:02:38,880 --> 00:02:43,250
 Maybe it could go on for months or years even, but lif

43
00:02:43,250 --> 00:02:46,800
etimes, who knows, but the shaking probably

44
00:02:46,800 --> 00:02:49,080
 will come and go.

45
00:02:49,080 --> 00:02:51,120
 So nothing to worry about.

46
00:02:51,120 --> 00:02:54,880
 It is what it is.

47
00:02:54,880 --> 00:02:58,470
 Sorry, actually I had a question about this because I

48
00:02:58,470 --> 00:03:00,680
 actually read, I had to have had

49
00:03:00,680 --> 00:03:04,200
 the same problem before with the vibrations from just

50
00:03:04,200 --> 00:03:06,400
 sitting in an inconvenient posture

51
00:03:06,400 --> 00:03:09,820
 and built up some tension and then after a while the body

52
00:03:09,820 --> 00:03:11,240
 starts to vibrate.

53
00:03:11,240 --> 00:03:15,520
 I was just acknowledging it and not moving.

54
00:03:15,520 --> 00:03:18,690
 Then I recently read in a book which said you should

55
00:03:18,690 --> 00:03:20,680
 acknowledge it for a while and

56
00:03:20,680 --> 00:03:24,660
 then you should actually adjust your posture because it's

57
00:03:24,660 --> 00:03:27,040
 just too distracting to meditation

58
00:03:27,040 --> 00:03:30,240
 to just keep shaking, shaking, shaking.

59
00:03:30,240 --> 00:03:33,920
 So I don't know what you think about that.

60
00:03:33,920 --> 00:03:38,070
 I think it's one of the examples of how you should never

61
00:03:38,070 --> 00:03:40,000
 have a hard and fast rule.

62
00:03:40,000 --> 00:03:41,640
 Remember what we're doing here.

63
00:03:41,640 --> 00:03:48,080
 It's not a military exercise.

64
00:03:48,080 --> 00:03:50,560
 So it's not that you have to do it this way or this way or

65
00:03:50,560 --> 00:03:51,240
 this way.

66
00:03:51,240 --> 00:03:52,720
 It's a learning experience.

67
00:03:52,720 --> 00:03:56,680
 So if you do it one way, you learn something in one way.

68
00:03:56,680 --> 00:04:01,020
 If you do it another way, you learn something in another

69
00:04:01,020 --> 00:04:01,560
 way.

70
00:04:01,560 --> 00:04:03,880
 Where you get wrong is where you say, "I will not move.

71
00:04:03,880 --> 00:04:04,920
 I should not move."

72
00:04:04,920 --> 00:04:06,520
 Or where you say, "I will move.

73
00:04:06,520 --> 00:04:09,600
 I have to move right away."

74
00:04:09,600 --> 00:04:14,090
 So either you gain an incredible amount of stress from

75
00:04:14,090 --> 00:04:16,200
 never moving or else you gain

76
00:04:16,200 --> 00:04:19,540
 a great amount of aversion and you cultivate a habit of

77
00:04:19,540 --> 00:04:21,320
 aversion by moving immediately

78
00:04:21,320 --> 00:04:27,120
 when the problem arises.

79
00:04:27,120 --> 00:04:30,720
 So I would say don't establish a rule like that at all.

80
00:04:30,720 --> 00:04:33,150
 As a meditation teacher, I wouldn't and I would say, "Well,

81
00:04:33,150 --> 00:04:33,640
 okay."

82
00:04:33,640 --> 00:04:36,640
 You can try different things with the meditators and say, "

83
00:04:36,640 --> 00:04:38,280
Try to acknowledge it first and

84
00:04:38,280 --> 00:04:42,690
 if it doesn't go away, then how we would explain it is if

85
00:04:42,690 --> 00:04:45,340
 you do move, then be mindful of it."

86
00:04:45,340 --> 00:04:47,610
 So we would never say, "And then move," or "And then don't

87
00:04:47,610 --> 00:04:48,080
 move."

88
00:04:48,080 --> 00:04:50,270
 We would say, "Be mindful of it as much as you can and then

89
00:04:50,270 --> 00:04:51,440
 if you have to move, just

90
00:04:51,440 --> 00:04:59,160
 say moving, moving when you move your legs."

91
00:04:59,160 --> 00:05:07,520
 And allow the meditator to experience it as they will.

92
00:05:07,520 --> 00:05:10,000
 The original question here is a little bit different.

93
00:05:10,000 --> 00:05:17,440
 First of all, because you're practicing anapanasati, not an

94
00:05:17,440 --> 00:05:19,200
apanasati.

95
00:05:19,200 --> 00:05:23,860
 Anapanasati means the in breath, panaminsi, out breath, or

96
00:05:23,860 --> 00:05:26,400
 vice versa, one or the other.

97
00:05:26,400 --> 00:05:29,480
 And this isn't exactly what we teach.

98
00:05:29,480 --> 00:05:32,790
 If you call it anapanasati, you're probably practicing some

99
00:05:32,790 --> 00:05:34,880
 form of tranquility meditation.

100
00:05:34,880 --> 00:05:40,310
 That's what it sounds like, because the question here that

101
00:05:40,310 --> 00:05:43,080
 you have is how to keep the calm

102
00:05:43,080 --> 00:05:49,360
 or how to cope with the loss of calm.

103
00:05:49,360 --> 00:05:54,300
 And from a vipassana or an insight point of view, the

104
00:05:54,300 --> 00:05:57,800
 problem isn't that moving disturbs

105
00:05:57,800 --> 00:05:58,800
 your calm.

106
00:05:58,800 --> 00:06:00,450
 The problem is that you don't want the calm to be disturbed

107
00:06:00,450 --> 00:06:00,600
.

108
00:06:00,600 --> 00:06:07,240
 The problem is that there's a clinging to the calm.

109
00:06:07,240 --> 00:06:12,670
 In the technical jargon, we call nikanti, which means this

110
00:06:12,670 --> 00:06:15,440
 subtle clinging to something

111
00:06:15,440 --> 00:06:16,920
 positive.

112
00:06:16,920 --> 00:06:19,360
 So when you experience happiness, you'll cling to the

113
00:06:19,360 --> 00:06:21,240
 happiness and you'll become content

114
00:06:21,240 --> 00:06:22,640
 with the happiness.

115
00:06:22,640 --> 00:06:24,520
 If there's calm, you'll become content with the calm.

116
00:06:24,520 --> 00:06:27,000
 If there's quiet, you'll become content with the quiet.

117
00:06:27,000 --> 00:06:30,720
 If you see lights and colors and pictures, you'll become

118
00:06:30,720 --> 00:06:32,240
 content with them.

119
00:06:32,240 --> 00:06:34,700
 Whatever good experiences arise in meditation, you'll

120
00:06:34,700 --> 00:06:35,840
 become attached to it.

121
00:06:35,840 --> 00:06:39,090
 So the point is not to feel this calm and the point is not

122
00:06:39,090 --> 00:06:40,640
 for that calm to stay.

123
00:06:40,640 --> 00:06:44,120
 The Buddha taught that the path to become free from

124
00:06:44,120 --> 00:06:46,680
 suffering is to see impermanence,

125
00:06:46,680 --> 00:06:49,120
 suffering and non-self.

126
00:06:49,120 --> 00:06:53,090
 So if I can ask you, when the calm disappears, does that

127
00:06:53,090 --> 00:06:56,000
 mean it's permanent or impermanent?

128
00:06:56,000 --> 00:06:57,920
 Is it satisfying or unsatisfying?

129
00:06:57,920 --> 00:07:03,120
 Can it really satisfy you or is it unable to satisfy you?

130
00:07:03,120 --> 00:07:05,140
 Maybe you still think that it's satisfying, you just have

131
00:07:05,140 --> 00:07:06,120
 to figure out the way to make

132
00:07:06,120 --> 00:07:08,240
 it satisfying, but actually it's not.

133
00:07:08,240 --> 00:07:11,500
 Actually what you're seeing is a part of the truth that it

134
00:07:11,500 --> 00:07:13,340
 is not satisfying and non-self

135
00:07:13,340 --> 00:07:15,000
 that you can't control it.

136
00:07:15,000 --> 00:07:17,880
 You can't keep the calm, you can't move and still have the

137
00:07:17,880 --> 00:07:18,320
 calm.

138
00:07:18,320 --> 00:07:21,320
 So you have to see that the calm is not permanent.

139
00:07:21,320 --> 00:07:25,720
 This is really the core of the answer to the question.

140
00:07:25,720 --> 00:07:30,670
 Now in regards to vibrating, dealing with shaking, dealing

141
00:07:30,670 --> 00:07:32,320
 with swaying and so on and

142
00:07:32,320 --> 00:07:37,630
 many experiences like it, the theory that we hold behind

143
00:07:37,630 --> 00:07:40,240
 this is that it is related to

144
00:07:40,240 --> 00:07:45,200
 rapture, or it comes under the heading of rapture.

145
00:07:45,200 --> 00:07:50,360
 So if you ever see Christians, these Christian evangelical,

146
00:07:50,360 --> 00:07:52,800
 even evangelists who go into

147
00:07:52,800 --> 00:07:58,710
 this rapture and start convulsing on the floor or so on,

148
00:07:58,710 --> 00:08:02,120
 that's the sort of phenomenon that

149
00:08:02,120 --> 00:08:03,120
 we're dealing with.

150
00:08:03,120 --> 00:08:05,900
 Some people will sit and shake back and forth, some people,

151
00:08:05,900 --> 00:08:07,320
 their whole body will vibrate

152
00:08:07,320 --> 00:08:08,320
 and so on.

153
00:08:08,320 --> 00:08:10,920
 There are many things and it can become quite strong.

154
00:08:10,920 --> 00:08:15,550
 So in fact one technique is to stop, is to tell yourself to

155
00:08:15,550 --> 00:08:16,300
 stop.

156
00:08:16,300 --> 00:08:19,540
 One teacher in Bangkok, he said the way you have to do it

157
00:08:19,540 --> 00:08:21,700
 is just say to yourself, "Stop!

158
00:08:21,700 --> 00:08:24,440
 Tell yourself to stop."

159
00:08:24,440 --> 00:08:26,900
 The point behind that is not that you're trying to control

160
00:08:26,900 --> 00:08:28,320
 it, but the point is that even

161
00:08:28,320 --> 00:08:33,900
 though you say that it's involuntary, the fact that you

162
00:08:33,900 --> 00:08:36,560
 feel calm or so on suggests

163
00:08:36,560 --> 00:08:42,260
 that there could be in some situation some kind of

164
00:08:42,260 --> 00:08:46,460
 propulsion or impulsion intention

165
00:08:46,460 --> 00:08:50,970
 in the mind that is very subliminal and so subliminal that

166
00:08:50,970 --> 00:08:53,040
 you are unable to detect that

167
00:08:53,040 --> 00:08:54,040
 it's even there.

168
00:08:54,040 --> 00:08:56,770
 So there's the contentment with the feelings and that

169
00:08:56,770 --> 00:08:58,840
 creates the shaking and the vibrating

170
00:08:58,840 --> 00:09:03,840
 and then you become even caught up in the vibrating.

171
00:09:03,840 --> 00:09:07,810
 So when you say to yourself, "Stop!" you're changing that

172
00:09:07,810 --> 00:09:08,400
 intention.

173
00:09:08,400 --> 00:09:15,920
 It's like you're adjusting your volition.

174
00:09:15,920 --> 00:09:17,360
 There's power in words.

175
00:09:17,360 --> 00:09:21,280
 So when you would say just say to yourself, "Stop!" instead

176
00:09:21,280 --> 00:09:22,960
 of saying feeling, feeling

177
00:09:22,960 --> 00:09:25,440
 or so on, just tell yourself to stop.

178
00:09:25,440 --> 00:09:28,660
 Just say to yourself, "Stop!" and your mind will change and

179
00:09:28,660 --> 00:09:30,000
 your mind will wake up and

180
00:09:30,000 --> 00:09:34,790
 because of the harshness of it, your mind will adjust and

181
00:09:34,790 --> 00:09:37,280
 you'll be able to be mindful.

182
00:09:37,280 --> 00:09:39,680
 Because if you just continue to acknowledge it, because of

183
00:09:39,680 --> 00:09:41,000
 the underlying attachment and

184
00:09:41,000 --> 00:09:44,660
 not acknowledging that attachment, it won't really be an

185
00:09:44,660 --> 00:09:46,040
 acknowledgement.

186
00:09:46,040 --> 00:09:49,440
 It will become a mantra and you will just get caught up

187
00:09:49,440 --> 00:09:50,640
 more shaking.

188
00:09:50,640 --> 00:09:55,550
 This is actually one particular special sort of experience

189
00:09:55,550 --> 00:09:57,720
 that has to be dealt with a

190
00:09:57,720 --> 00:10:02,820
 little bit special sometimes because of the repetitive and

191
00:10:02,820 --> 00:10:05,040
 the habitual nature of it.

192
00:10:05,040 --> 00:10:07,740
 The mind can become so caught up in it that it just gets

193
00:10:07,740 --> 00:10:08,760
 out of control.

194
00:10:08,760 --> 00:10:12,370
 As you see these Christians, they just start convulsing and

195
00:10:12,370 --> 00:10:14,360
 it's totally uncontrollable.

196
00:10:14,360 --> 00:10:15,720
 But the truth is that they're controlling it.

197
00:10:15,720 --> 00:10:16,720
 They're creating it.

198
00:10:16,720 --> 00:10:17,720
 They're cultivating it.

199
00:10:17,720 --> 00:10:20,880
 Eventually they become so good at it that it seems to come

200
00:10:20,880 --> 00:10:21,720
 by itself.

201
00:10:21,720 --> 00:10:25,480
 But through the whole process, there's a cultivation going

202
00:10:25,480 --> 00:10:25,800
 on.

203
00:10:25,800 --> 00:10:31,080
 When you make a determination to stop it, that can help.

204
00:10:31,080 --> 00:10:32,080
 So there are two keys.

205
00:10:32,080 --> 00:10:36,880
 First of all, you may very well be cultivating, the vibr

206
00:10:36,880 --> 00:10:39,000
ating and the shaking.

207
00:10:39,000 --> 00:10:41,480
 In that case, you have to try different things.

208
00:10:41,480 --> 00:10:46,360
 You can try moving is fine, but just be mindful moving,

209
00:10:46,360 --> 00:10:47,360
 moving.

210
00:10:47,360 --> 00:10:50,840
 If it's really not going away, you can tell it to stop.

211
00:10:50,840 --> 00:10:52,640
 Tell your mind to stop.

212
00:10:52,640 --> 00:10:55,970
 You don't expect it to work all the time, but it can help

213
00:10:55,970 --> 00:10:57,640
 to remove the clinging in

214
00:10:57,640 --> 00:11:01,240
 the mind.

215
00:11:01,240 --> 00:11:02,480
 Sometimes you can just acknowledge it.

216
00:11:02,480 --> 00:11:05,080
 Sometimes if you just acknowledge it into the point that

217
00:11:05,080 --> 00:11:06,680
 the mind becomes bored of it,

218
00:11:06,680 --> 00:11:09,440
 then the mind will let go of it and there will be a change

219
00:11:09,440 --> 00:11:10,840
 occurring in the mind.

220
00:11:10,840 --> 00:11:13,070
 And really in the end, that is the key, is to eventually

221
00:11:13,070 --> 00:11:14,240
 just become bored of it.

222
00:11:14,240 --> 00:11:18,200
 And when the mind is truly bored of it, it will disappear.

223
00:11:18,200 --> 00:11:20,550
 The mind will stop cultivating and be like, "Enough of that

224
00:11:20,550 --> 00:11:20,760
.

225
00:11:20,760 --> 00:11:22,200
 It's not really making me happy.

226
00:11:22,200 --> 00:11:26,120
 It's not really doing anything for me."

227
00:11:26,120 --> 00:11:27,120
 So that's the first part.

228
00:11:27,120 --> 00:11:30,510
 The other problem you have here is the clinging to the calm

229
00:11:30,510 --> 00:11:32,680
 and the wrong idea that calm somehow

230
00:11:32,680 --> 00:11:34,600
 has some intrinsic benefit.

231
00:11:34,600 --> 00:11:36,720
 The purpose of calm is to see things clearly.

232
00:11:36,720 --> 00:11:40,050
 Once you're calm, then you have to apply insight to the

233
00:11:40,050 --> 00:11:42,600
 experience, whether it be the calmness

234
00:11:42,600 --> 00:11:45,240
 or whether it be the object of meditation.

235
00:11:45,240 --> 00:11:48,080
 You have to look at that and be able to see impermanence.

236
00:11:48,080 --> 00:11:51,000
 So in fact, what you're seeing is a positive thing.

237
00:11:51,000 --> 00:11:52,930
 You're seeing impermanent suffering and non-self, but

238
00:11:52,930 --> 00:11:54,320
 because of the clinging and because of

239
00:11:54,320 --> 00:11:56,890
 the defilements in the mind, there's still the attachment

240
00:11:56,890 --> 00:11:58,440
 and the thought that something's

241
00:11:58,440 --> 00:11:59,440
 wrong.

242
00:11:59,440 --> 00:12:00,520
 It's changing.

243
00:12:00,520 --> 00:12:01,520
 Something wrong.

244
00:12:01,520 --> 00:12:02,520
 It's disappeared.

245
00:12:02,520 --> 00:12:03,880
 That's the truth of life.

246
00:12:03,880 --> 00:12:05,280
 Everything that arises disappears.

247
00:12:05,280 --> 00:12:07,800
 Okay?

248
00:12:07,800 --> 00:12:08,800
 Okay.

249
00:12:08,800 --> 00:12:09,800
 Okay.

