WEBVTT

00:00:00.000 --> 00:00:02.500
 Overcoming Doubt.

00:00:02.500 --> 00:00:05.020
 Why is it so important to overcome doubt

00:00:05.020 --> 00:00:07.620
 early in the practice?

00:00:07.620 --> 00:00:09.620
 Doubt is one of the five hindrances.

00:00:09.620 --> 00:00:11.520
 If your mind is consumed by doubt,

00:00:11.520 --> 00:00:14.340
 it will be impossible to commit yourself wholeheartedly

00:00:14.340 --> 00:00:15.600
 to the practice.

00:00:15.600 --> 00:00:18.340
 So overcoming doubt is very important.

00:00:18.340 --> 00:00:20.300
 Purification by overcoming doubt

00:00:20.300 --> 00:00:22.500
 is one of the seven purifications

00:00:22.500 --> 00:00:26.660
 in the path of purification, coming after purification

00:00:26.660 --> 00:00:28.420
 of view.

00:00:28.420 --> 00:00:30.860
 Purification of view means changing the way

00:00:30.860 --> 00:00:33.160
 you look at the world, moving from a conceptual view

00:00:33.160 --> 00:00:36.580
 of reality to one based on direct experience.

00:00:36.580 --> 00:00:39.460
 Purification by overcoming doubt relates to doubt

00:00:39.460 --> 00:00:41.140
 about how reality works.

00:00:41.140 --> 00:00:43.900
 So it is logical that it will arise only after you

00:00:43.900 --> 00:00:46.900
 have a proper perspective on reality.

00:00:46.900 --> 00:00:49.060
 Doubt is a quality of mind that most people

00:00:49.060 --> 00:00:50.980
 deal with on a regular basis.

00:00:50.980 --> 00:00:53.180
 Trying to decide what to do with their lives,

00:00:53.180 --> 00:00:56.140
 finding the right course of action in every situation.

00:00:56.140 --> 00:00:59.140
 We doubt about our lives, about what we should do,

00:00:59.140 --> 00:01:00.740
 and about the nature of the world.

00:01:00.740 --> 00:01:03.420
 Much of science was created to help answer questions

00:01:03.420 --> 00:01:05.700
 about the nature of our world and to help us understand

00:01:05.700 --> 00:01:08.820
 reality so that we might live better lives.

00:01:08.820 --> 00:01:11.420
 Religion was created for the same sort of purpose.

00:01:11.420 --> 00:01:13.580
 Both religion and science try to provide us

00:01:13.580 --> 00:01:15.580
 with answers to subdue our doubts,

00:01:15.580 --> 00:01:17.340
 though what we call religion has often

00:01:17.340 --> 00:01:20.340
 been more about belief without much basis in evidence,

00:01:20.340 --> 00:01:23.060
 while science has been much more about evidence-based

00:01:23.060 --> 00:01:24.780
 knowledge.

00:01:24.780 --> 00:01:27.060
 Psychologically, there is quite a bit of overlap

00:01:27.060 --> 00:01:29.540
 when it comes to believing something and knowing it.

00:01:29.540 --> 00:01:32.020
 What is important is that you are provided with answers

00:01:32.020 --> 00:01:33.260
 that quell your doubt.

00:01:33.260 --> 00:01:35.780
 Science provides answers based on evidence,

00:01:35.780 --> 00:01:38.100
 and so it is very good at overcoming doubt

00:01:38.100 --> 00:01:39.300
 conventionally.

00:01:39.300 --> 00:01:41.900
 If you rely on science, if you believe and put your faith

00:01:41.900 --> 00:01:44.300
 in science, then it is very good at overcoming

00:01:44.300 --> 00:01:45.900
 intellectual doubt.

00:01:45.900 --> 00:01:49.140
 Very good at helping one find ways to live one's life.

00:01:49.140 --> 00:01:52.220
 Science cannot, however, free one from doubt completely

00:01:52.220 --> 00:01:53.620
 for two reasons.

00:01:53.620 --> 00:01:56.220
 First, because science does not know everything,

00:01:56.220 --> 00:01:59.340
 and scientists have not found answers to all questions.

00:01:59.340 --> 00:02:01.940
 And secondly, because the answers science gives

00:02:01.940 --> 00:02:03.980
 are not one's own answers.

00:02:03.980 --> 00:02:06.860
 Science cannot help you know the truth of your reality.

00:02:06.860 --> 00:02:08.580
 If you look at depression, for example,

00:02:08.580 --> 00:02:11.060
 science can help you understand the technical aspects

00:02:11.060 --> 00:02:12.820
 of depression, and this understanding

00:02:12.820 --> 00:02:14.340
 may help you deal with depression

00:02:14.340 --> 00:02:16.860
 on a superficial level, but it cannot provide you

00:02:16.860 --> 00:02:19.980
 with the direct understanding required to free you from it.

00:02:19.980 --> 00:02:22.780
 Likewise, for anxiety, fear, and all other sorts

00:02:22.780 --> 00:02:24.900
 of mental suffering.

00:02:24.900 --> 00:02:27.340
 Science cannot tell you how to live your life

00:02:27.340 --> 00:02:29.100
 or how to react to situations.

00:02:29.100 --> 00:02:30.820
 It cannot bring true wisdom.

00:02:30.820 --> 00:02:32.540
 This is what we commonly understand

00:02:32.540 --> 00:02:35.660
 as the difference between wisdom and intelligence.

00:02:35.660 --> 00:02:38.060
 Religion, on the other hand, tries to provide wisdom,

00:02:38.060 --> 00:02:40.460
 making claims on topics like ethics, happiness,

00:02:40.460 --> 00:02:41.940
 and the nature of reality.

00:02:41.940 --> 00:02:44.060
 Most religion, however, is more about providing

00:02:44.060 --> 00:02:47.620
 psychological support than actual understanding.

00:02:47.620 --> 00:02:49.980
 Belief in God, for example, is best understood

00:02:49.980 --> 00:02:52.220
 as a coping mechanism of sorts.

00:02:52.220 --> 00:02:54.260
 People use God to reassure themselves

00:02:54.260 --> 00:02:56.970
 that there is meaning to life, no matter how hard things

00:02:56.970 --> 00:02:57.820
 get.

00:02:57.820 --> 00:02:59.500
 From a point of view of psychology,

00:02:59.500 --> 00:03:01.300
 God and most of the other concepts

00:03:01.300 --> 00:03:03.380
 we associate with religion are just tools

00:03:03.380 --> 00:03:05.780
 that help cope with the difficulties of life.

00:03:05.780 --> 00:03:07.580
 Most of religion is like this.

00:03:07.580 --> 00:03:10.260
 Buddhists around the world perform all sorts of rituals

00:03:10.260 --> 00:03:12.900
 like chanting, ringing bells, bowing, and so on.

00:03:12.900 --> 00:03:15.900
 Psychologically, these are useful practices

00:03:15.900 --> 00:03:18.260
 that help promote positive states of mind,

00:03:18.260 --> 00:03:19.880
 but none are really enough for us

00:03:19.880 --> 00:03:21.420
 to completely overcome doubt.

00:03:22.220 --> 00:03:24.740
 Overcoming doubt is the stage on the path

00:03:24.740 --> 00:03:27.260
 where the meditator begins to actually understand

00:03:27.260 --> 00:03:29.980
 not only the nature and building blocks of reality,

00:03:29.980 --> 00:03:32.980
 but how it works, specifically how the physical

00:03:32.980 --> 00:03:36.060
 and mental aspects of experience work and interact,

00:03:36.060 --> 00:03:37.220
 and even more specifically,

00:03:37.220 --> 00:03:40.020
 how our experiential problems come to be.

00:03:40.020 --> 00:03:42.340
 One of our biggest problems is that we do not even

00:03:42.340 --> 00:03:44.740
 understand our problems.

00:03:44.740 --> 00:03:46.640
 When we feel anxious or depressed,

00:03:46.640 --> 00:03:49.500
 we usually do not even know where it comes from.

00:03:49.500 --> 00:03:52.060
 When you go to the doctor for anxiety or depression,

00:03:52.060 --> 00:03:54.440
 they can explain the cause in terms of the physical state

00:03:54.440 --> 00:03:57.320
 of your brain, which may be conceptually correct,

00:03:57.320 --> 00:03:59.720
 but it is wrong in the sense that this sort of answer

00:03:59.720 --> 00:04:02.280
 cannot free you from depression.

00:04:02.280 --> 00:04:03.380
 It is the wrong answer

00:04:03.380 --> 00:04:05.480
 because it is the wrong type of answer.

00:04:05.480 --> 00:04:09.740
 The right type of answer is one that we see for ourselves,

00:04:09.740 --> 00:04:12.040
 one in which we comprehend our disorders

00:04:12.040 --> 00:04:13.580
 for what they really are.

00:04:13.580 --> 00:04:16.400
 After becoming familiar with the nature of experience,

00:04:16.400 --> 00:04:18.820
 the meditator begins to see how experiences work

00:04:18.820 --> 00:04:20.860
 in terms of cause and effect.

00:04:20.860 --> 00:04:25.340
 This is called the second of the 16 stages of knowledge.

00:04:25.340 --> 00:04:28.780
 This second stage, or the knowledge that grasps

00:04:28.780 --> 00:04:33.300
 conditionality is true understanding of the law of karma.

00:04:33.300 --> 00:04:35.860
 In Buddhism, karma means mental inclination,

00:04:35.860 --> 00:04:37.580
 intention, or volition.

00:04:37.580 --> 00:04:39.620
 At this stage, the meditator observes

00:04:39.620 --> 00:04:41.940
 through direct experience that inclination leads

00:04:41.940 --> 00:04:45.500
 to result in actions, which lead to result in the effects.

00:04:45.500 --> 00:04:46.900
 There is no need for belief,

00:04:46.900 --> 00:04:48.540
 as the meditator sees directly,

00:04:48.540 --> 00:04:50.860
 that certain mind states lead to certain results,

00:04:50.860 --> 00:04:53.540
 which is a crucial part of the mechanics of reality.

00:04:53.540 --> 00:04:57.780
 Knowledge that grasps conditionality

00:04:57.780 --> 00:05:00.900
 involves seeing how craving is a cause for suffering,

00:05:00.900 --> 00:05:03.460
 which often causes some conflict as one wrestles

00:05:03.460 --> 00:05:07.220
 with one's prior perceptions of the benefits of craving.

00:05:07.220 --> 00:05:09.640
 Some meditators first instinct is to think

00:05:09.640 --> 00:05:11.620
 that something is wrong with their meditation

00:05:11.620 --> 00:05:14.780
 because they cannot believe that craving leads to suffering

00:05:14.780 --> 00:05:14.780
.

00:05:14.780 --> 00:05:16.380
 When they want things, they get them.

00:05:16.380 --> 00:05:18.580
 So they see the problem as meditation,

00:05:18.580 --> 00:05:20.800
 preventing them from getting what they want.

00:05:20.800 --> 00:05:22.780
 They often see a lot of suffering and stress

00:05:22.780 --> 00:05:24.220
 in their minds at this stage

00:05:24.220 --> 00:05:26.180
 as a result of their attachments.

00:05:26.180 --> 00:05:28.740
 This stage of the meditation can be quite stressful

00:05:28.740 --> 00:05:30.740
 as one is forced to choose between clinging

00:05:30.740 --> 00:05:33.320
 to one's desires or letting them go.

00:05:33.320 --> 00:05:34.900
 Both seem to be potential ways

00:05:34.900 --> 00:05:36.660
 to free oneself from suffering.

00:05:36.660 --> 00:05:39.060
 Your two options are get what you want

00:05:39.060 --> 00:05:40.700
 or give up the wanting.

00:05:40.700 --> 00:05:42.580
 Either method theoretically frees you

00:05:42.580 --> 00:05:44.140
 from the wanting and suffering.

00:05:45.460 --> 00:05:48.620
 Purification by overcoming doubt leads to the knowledge

00:05:48.620 --> 00:05:51.140
 that the things that we cling to are not stable

00:05:51.140 --> 00:05:54.340
 or predictable, not satisfying and not controllable.

00:05:54.340 --> 00:05:56.240
 These are called the three characteristics

00:05:56.240 --> 00:05:58.120
 of all arisen phenomena.

00:05:58.120 --> 00:06:01.140
 Seeing how phenomena interact with each other

00:06:01.140 --> 00:06:03.780
 on a moment to moment basis leads one to realize

00:06:03.780 --> 00:06:05.420
 that the problem is not the failure

00:06:05.420 --> 00:06:07.580
 to acquire the objects of our desire,

00:06:07.580 --> 00:06:09.380
 but the desire itself.

00:06:09.380 --> 00:06:11.680
 One realizes that if one were content,

00:06:11.680 --> 00:06:13.220
 one would not suffer.

00:06:13.220 --> 00:06:16.100
 The things we cling to are impermanent, unsatisfying

00:06:16.100 --> 00:06:17.420
 and uncontrollable.

00:06:17.420 --> 00:06:20.340
 They cannot possibly provide real satisfaction.

00:06:20.340 --> 00:06:23.140
 They are impermanent, uncertain and unstable.

00:06:23.140 --> 00:06:26.580
 They are suffering in the sense that they are unsatisfying.

00:06:26.580 --> 00:06:27.820
 And if one clings to them,

00:06:27.820 --> 00:06:30.920
 one will suffer because they cannot last.

00:06:30.920 --> 00:06:32.380
 They are not oneself.

00:06:32.380 --> 00:06:33.940
 They do not belong to oneself

00:06:33.940 --> 00:06:36.740
 and they have no self of their own to cling to.

00:06:36.740 --> 00:06:38.540
 They are experiences that arise

00:06:38.540 --> 00:06:41.000
 and cease based on causes and conditions.

00:06:41.900 --> 00:06:44.100
 The realization of the three characteristics

00:06:44.100 --> 00:06:48.060
 can only come about after purification by overcoming doubt.

00:06:48.060 --> 00:06:50.700
 Since one must first observe the causal relationships

00:06:50.700 --> 00:06:53.540
 between phenomena before one can understand their value.

00:06:53.540 --> 00:06:56.660
 Freedom from doubt means certainty afforded

00:06:56.660 --> 00:06:58.900
 by direct knowledge, which provides the mind

00:06:58.900 --> 00:07:01.380
 the fortitude required to attain the higher stages

00:07:01.380 --> 00:07:04.020
 of knowledge and freedom from suffering.

00:07:04.020 --> 00:07:05.860
 Before attaining this freedom from doubt,

00:07:05.860 --> 00:07:07.340
 you might think that the meditation

00:07:07.340 --> 00:07:09.380
 is causing the impermanence, suffering

00:07:09.380 --> 00:07:11.620
 and non-self characteristics that you observe

00:07:11.620 --> 00:07:14.020
 in your meditative experiences.

00:07:14.020 --> 00:07:16.220
 You might be inclined to stop meditating

00:07:16.220 --> 00:07:18.480
 so as to return to ordinary reality

00:07:18.480 --> 00:07:21.260
 where things are pleasant, satisfying and controllable.

00:07:21.260 --> 00:07:24.020
 This is because meditation forces you

00:07:24.020 --> 00:07:25.820
 to confront the nature of reality,

00:07:25.820 --> 00:07:28.980
 whereas ordinarily we are constantly avoiding it.

00:07:28.980 --> 00:07:31.080
 Once you see clearly the causal relationship

00:07:31.080 --> 00:07:33.620
 between experiences, you have no need to avoid

00:07:33.620 --> 00:07:37.060
 or manipulate reality as you are able to skillfully respond

00:07:37.060 --> 00:07:40.400
 to any experience without reaction or partiality.

00:07:41.300 --> 00:07:43.700
 The state of mind before attaining freedom from doubt

00:07:43.700 --> 00:07:45.720
 is like beginning as a carpenter.

00:07:45.720 --> 00:07:47.080
 Before you can build a cabinet,

00:07:47.080 --> 00:07:48.800
 you have to learn about your tools.

00:07:48.800 --> 00:07:50.500
 In the early stages, you are still learning

00:07:50.500 --> 00:07:51.860
 how reality works.

00:07:51.860 --> 00:07:54.020
 Once you do that and the doubt disappears,

00:07:54.020 --> 00:07:55.820
 your mind becomes client and wieldy

00:07:55.820 --> 00:07:58.860
 like a tool in the hands of a skilled carpenter.

00:07:58.860 --> 00:08:00.740
 Such a mind is a tool you can apply

00:08:00.740 --> 00:08:02.620
 towards overcoming suffering.

00:08:02.620 --> 00:08:05.180
 That is why it is important to overcome doubt.

00:08:05.180 --> 00:08:07.940
 (static buzzing)

