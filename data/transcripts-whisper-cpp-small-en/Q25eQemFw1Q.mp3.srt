1
00:00:00,000 --> 00:00:05,000
 I'm beginning to see that finding true happiness in worldly

2
00:00:05,000 --> 00:00:07,000
 activities is not possible.

3
00:00:07,000 --> 00:00:10,000
 However, I need to have a working life.

4
00:00:10,000 --> 00:00:15,590
 I feel apathy or a lack of motivation for these regular

5
00:00:15,590 --> 00:00:17,000
 worldly duties.

6
00:00:17,000 --> 00:00:21,630
 Will my motivation for daily regular work return as I

7
00:00:21,630 --> 00:00:28,000
 continue to develop my practice?

8
00:00:28,000 --> 00:00:31,000
 Yes.

9
00:00:31,000 --> 00:00:35,000
 That's your answer?

10
00:00:35,000 --> 00:00:44,000
 Well, that's the main part of the answer, let's say so.

11
00:00:44,000 --> 00:00:49,520
 These things happen in the course of meditation or when you

12
00:00:49,520 --> 00:00:53,000
 practice throughout daily life,

13
00:00:53,000 --> 00:01:00,500
 but this state of mind is as impermanent as any other state

14
00:01:00,500 --> 00:01:02,000
 of mind.

15
00:01:02,000 --> 00:01:17,110
 When you live your life and are aware or are not aware of

16
00:01:17,110 --> 00:01:19,000
 what you are doing when you are mindful

17
00:01:19,000 --> 00:01:24,260
 or when you are not being mindful, everything changes,

18
00:01:24,260 --> 00:01:27,000
 especially the mind states.

19
00:01:27,000 --> 00:01:37,970
 So this apathy and lack of motivation is happening in your

20
00:01:37,970 --> 00:01:39,000
 mind.

21
00:01:39,000 --> 00:01:44,720
 It is something that when you say you feel apathy, it's

22
00:01:44,720 --> 00:01:48,000
 probably not a physical feeling,

23
00:01:48,000 --> 00:01:53,240
 it might become physical, but probably in the beginning it

24
00:01:53,240 --> 00:01:55,000
's really mental

25
00:01:55,000 --> 00:01:59,260
 and then takes over of your body and then you don't move

26
00:01:59,260 --> 00:02:00,000
 anymore.

27
00:02:00,000 --> 00:02:05,600
 But as you describe it, I would say it started as a mental

28
00:02:05,600 --> 00:02:13,000
 thing that the motivation just slid away.

29
00:02:13,000 --> 00:02:18,120
 This has to happen, this should happen and it's not the bad

30
00:02:18,120 --> 00:02:20,000
 sign that it happens

31
00:02:20,000 --> 00:02:24,160
 and you shouldn't worry too much about it that it has

32
00:02:24,160 --> 00:02:25,000
 happened.

33
00:02:25,000 --> 00:02:34,110
 When you continue and you understand and you let go of your

34
00:02:34,110 --> 00:02:37,000
 apathy and your motivation

35
00:02:37,000 --> 00:02:42,020
 or lack of motivation, when you just accept things as they

36
00:02:42,020 --> 00:02:43,000
 really are,

37
00:02:43,000 --> 00:02:47,000
 then the motivation will come back.

38
00:02:47,000 --> 00:02:52,010
 It will of course be different than before, it will not be

39
00:02:52,010 --> 00:02:54,000
 so enthusiastic anymore

40
00:02:54,000 --> 00:02:58,230
 like, "Oh, I'm going to work, I don't know if you ever had

41
00:02:58,230 --> 00:02:59,000
 that."

42
00:02:59,000 --> 00:03:09,000
 But it might become more seeing the need of doing things

43
00:03:09,000 --> 00:03:12,000
 that have to be done and doing them

44
00:03:12,000 --> 00:03:21,820
 in the order as they come and in the order that they have

45
00:03:21,820 --> 00:03:24,000
 to be done.

46
00:03:28,000 --> 00:03:33,500
 So you should really just continue to practice as you

47
00:03:33,500 --> 00:03:37,000
 practiced before and be mindful

48
00:03:37,000 --> 00:03:46,210
 and to see that you don't find happiness in worldly

49
00:03:46,210 --> 00:03:51,000
 activity is a great gift

50
00:03:51,000 --> 00:03:58,000
 that you have found or that you got from the practice.

51
00:03:58,000 --> 00:04:05,310
 Don't throw that away because you want to be ready for your

52
00:04:05,310 --> 00:04:06,000
 work.

53
00:04:06,000 --> 00:04:09,970
 This is what a therapist would do with you, they would tell

54
00:04:09,970 --> 00:04:10,000
 you,

55
00:04:10,000 --> 00:04:15,310
 "Stop practicing and make your mind available for the

56
00:04:15,310 --> 00:04:18,000
 worldly things again."

57
00:04:18,000 --> 00:04:22,000
 Don't throw away that gift that you got.

58
00:04:22,000 --> 00:04:28,000
 Try to go through it and acknowledge it as it is.

59
00:04:28,000 --> 00:04:39,000
 It is maybe hard now, but it will not be always like that.

60
00:04:39,000 --> 00:04:43,000
 There is, I want to mention, a danger of course.

61
00:04:43,000 --> 00:04:48,000
 When you give in too much into your lack of motivation

62
00:04:48,000 --> 00:04:55,000
 and when you indulge in apathy, when you say,

63
00:04:55,000 --> 00:05:00,000
 "Oh, I'm so lazy, I'm so demotivated,"

64
00:05:00,000 --> 00:05:09,000
 and you let your mind and your body get drowsy,

65
00:05:09,000 --> 00:05:15,230
 then there is a real danger that you will not find out of

66
00:05:15,230 --> 00:05:16,000
 it.

67
00:05:16,000 --> 00:05:20,270
 So the most important thing for you now is to keep

68
00:05:20,270 --> 00:05:24,000
 practicing, to keep being mindful.

69
00:05:32,000 --> 00:05:37,150
 The first thing I wanted to say was, "Of course, it's going

70
00:05:37,150 --> 00:05:39,000
 to demotivate you.

71
00:05:39,000 --> 00:05:42,000
 It's going to make you want to go and live in the forest

72
00:05:42,000 --> 00:05:44,000
 and leave your job behind."

73
00:05:44,000 --> 00:05:50,000
 But then I came to the same conclusion that, no, actually,

74
00:05:50,000 --> 00:05:55,000
 it doesn't mean that you have to be, that there will be

75
00:05:55,000 --> 00:05:58,000
 anything that gets in the way of your job.

76
00:05:58,000 --> 00:06:02,300
 I think maybe the mistake is thinking that you somehow have

77
00:06:02,300 --> 00:06:03,000
 to be excited

78
00:06:03,000 --> 00:06:06,000
 or motivated in order to do work.

79
00:06:06,000 --> 00:06:13,960
 Through the practice, you learn the basic principle of only

80
00:06:13,960 --> 00:06:16,000
 doing things that have purpose.

81
00:06:16,000 --> 00:06:19,980
 So you find yourself unable to do things that have no

82
00:06:19,980 --> 00:06:21,000
 purpose.

83
00:06:21,000 --> 00:06:27,430
 You'd be unable to work for the purpose of getting high

84
00:06:27,430 --> 00:06:29,000
 status in society

85
00:06:29,000 --> 00:06:33,330
 or for becoming rich or for building up an empire or a

86
00:06:33,330 --> 00:06:36,000
 career or becoming famous or any of these things.

87
00:06:36,000 --> 00:06:39,080
 You'll be unable to do that eventually in your practice

88
00:06:39,080 --> 00:06:41,000
 because you're unable to do useless things.

89
00:06:41,000 --> 00:06:45,000
 These are all things that are useless or even unbeneficial.

90
00:06:45,000 --> 00:06:48,000
 They create problems in your practice.

91
00:06:48,000 --> 00:06:59,330
 The problem is thinking that you need to truly appreciate

92
00:06:59,330 --> 00:07:01,000
 the work in order to do it.

93
00:07:01,000 --> 00:07:04,000
 The work itself has some purpose.

94
00:07:04,000 --> 00:07:12,000
 The purpose in most cases is to fulfill the needs,

95
00:07:12,000 --> 00:07:18,880
 your basic needs of food and shelter and clothing and so on

96
00:07:18,880 --> 00:07:19,000
.

97
00:07:19,000 --> 00:07:21,710
 In that regard, you will find that the meditation is

98
00:07:21,710 --> 00:07:24,000
 actually a great boon for your practice.

99
00:07:24,000 --> 00:07:27,280
 People who continue to work in the world after having

100
00:07:27,280 --> 00:07:30,000
 practiced find themselves,

101
00:07:30,000 --> 00:07:36,000
 in general, able to work quite efficiently.

102
00:07:36,000 --> 00:07:41,300
 Their employers appreciate them more and will often favor

103
00:07:41,300 --> 00:07:42,000
 them,

104
00:07:42,000 --> 00:07:49,300
 in fact, because of their lack of contrary mind states like

105
00:07:49,300 --> 00:07:52,000
 gossiping and laziness and so on.

106
00:07:52,000 --> 00:07:55,380
 A person who practices meditation, of course, has a great

107
00:07:55,380 --> 00:07:56,000
 amount of energy

108
00:07:56,000 --> 00:07:59,760
 and the ability to put out energy in repetitive monotonous

109
00:07:59,760 --> 00:08:02,000
 tasks through their practice.

110
00:08:02,000 --> 00:08:10,620
 I think it actually is a false demotivation that meditators

111
00:08:10,620 --> 00:08:11,000
 often get

112
00:08:11,000 --> 00:08:13,420
 because they read in the suttas about these monks living

113
00:08:13,420 --> 00:08:14,000
 off in caves

114
00:08:14,000 --> 00:08:17,000
 and so they think, "Oh, why can't I do that?"

115
00:08:17,000 --> 00:08:19,790
 and they feel depressed because they have to work an office

116
00:08:19,790 --> 00:08:20,000
 job

117
00:08:20,000 --> 00:08:22,560
 and thinking that they're living a lie or something like

118
00:08:22,560 --> 00:08:23,000
 that.

119
00:08:23,000 --> 00:08:30,370
 When you're clear about your path and about how to get to

120
00:08:30,370 --> 00:08:31,000
 the point

121
00:08:31,000 --> 00:08:34,000
 where you're able to live a meditator life,

122
00:08:34,000 --> 00:08:37,280
 "So slowly I'm going to work and I'm going to clear up my

123
00:08:37,280 --> 00:08:42,000
 debts and my burdens and so on,"

124
00:08:42,000 --> 00:08:45,340
 then you find that you're able to put all of your effort

125
00:08:45,340 --> 00:08:46,000
 into work.

126
00:08:46,000 --> 00:08:49,280
 For instance, when I knew that I needed money to save up to

127
00:08:49,280 --> 00:08:53,000
 go to Thailand to become a monk,

128
00:08:53,000 --> 00:08:57,920
 it was really easy for me to raise money, to work in any

129
00:08:57,920 --> 00:08:59,000
 job.

130
00:08:59,000 --> 00:09:01,620
 I did tree planting and it was really easy for me to put

131
00:09:01,620 --> 00:09:04,000
 out an intense amount of effort

132
00:09:04,000 --> 00:09:07,560
 and really actually push the limits even though not being a

133
00:09:07,560 --> 00:09:09,000
 physical person.

134
00:09:09,000 --> 00:09:12,270
 When I was working in a restaurant, I found I had great

135
00:09:12,270 --> 00:09:13,000
 patience

136
00:09:13,000 --> 00:09:16,630
 and they were really happy to have me because I wasn't

137
00:09:16,630 --> 00:09:17,000
 cheating them

138
00:09:17,000 --> 00:09:21,000
 and I wasn't trying to get out of work and so on.

139
00:09:21,000 --> 00:09:26,000
 I was willing to take the worst jobs with patience

140
00:09:26,000 --> 00:09:29,000
 because I was able to be mindful of it and so on.

141
00:09:29,000 --> 00:09:31,880
 I would think that it should help you in your work once you

142
00:09:31,880 --> 00:09:34,000
 can give up that idea

143
00:09:34,000 --> 00:09:37,600
 that somehow this is meaningless and so on and focus on

144
00:09:37,600 --> 00:09:39,000
 what is the meaning of it.

145
00:09:39,000 --> 00:09:41,000
 You're working to fulfill some purpose

146
00:09:41,000 --> 00:09:45,000
 and so it's actually irrational for you to spend your time

147
00:09:45,000 --> 00:09:47,000
 moping and apathetic during the work.

148
00:09:47,000 --> 00:09:51,110
 You have to see clearly what is the point of it and do it

149
00:09:51,110 --> 00:09:53,000
 just for that reason.

150
00:09:54,000 --> 00:09:57,000
 There's one other thing I want to mention.

151
00:09:57,000 --> 00:10:05,050
 In the Eightfold Noble Path is the right livelihood

152
00:10:05,050 --> 00:10:06,000
 mentioned

153
00:10:06,000 --> 00:10:11,730
 and I think when you have a job that is not going against

154
00:10:11,730 --> 00:10:13,000
 right livelihood,

155
00:10:13,000 --> 00:10:20,310
 then it is perfectly alright to live the working life and

156
00:10:20,310 --> 00:10:22,000
 to live the lay life

157
00:10:22,000 --> 00:10:29,000
 and to work and you should get over your lack of motivation

158
00:10:29,000 --> 00:10:34,000
 because it's not a bad job that you are doing.

159
00:10:34,000 --> 00:10:39,700
 When you are doing a job which is going against the right

160
00:10:39,700 --> 00:10:41,000
 livelihood,

161
00:10:41,000 --> 00:10:45,000
 then you should consider to do something else

162
00:10:45,000 --> 00:10:51,990
 and then your lack of motivation has possibly other reasons

163
00:10:51,990 --> 00:10:54,000
 than we mentioned now.

164
00:10:54,000 --> 00:10:59,000
 So that is one thing you should consider.

