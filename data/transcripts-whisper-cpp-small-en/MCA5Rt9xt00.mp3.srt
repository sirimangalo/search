1
00:00:00,000 --> 00:00:04,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,000 --> 00:00:13,000
 Today we continue with verse 216, which reads as follows.

3
00:00:15,000 --> 00:00:22,000
 Dhanhaya Jayati Soko Dhanhaya Jayati Bhaian

4
00:00:22,000 --> 00:00:30,000
 Dhanhaya Uipamutasa Nati Soko Gudubhayang

5
00:00:33,000 --> 00:00:48,130
 Which means, from thirst comes sorrow, from thirst comes

6
00:00:48,130 --> 00:00:50,000
 danger.

7
00:00:53,000 --> 00:01:02,860
 For one who is liberated from thirst, from thirsting, from

8
00:01:02,860 --> 00:01:08,000
 craving, there is no sorrow, whence fear, or whence danger.

9
00:01:13,000 --> 00:01:20,290
 This verse was supposed to have been taught in response to

10
00:01:20,290 --> 00:01:26,390
 a story about a Brahmin householder in the time of the

11
00:01:26,390 --> 00:01:27,000
 Buddha.

12
00:01:27,000 --> 00:01:32,710
 The story goes that in Sawati there was a Brahmin household

13
00:01:32,710 --> 00:01:33,000
er.

14
00:01:33,000 --> 00:01:38,980
 Brahmin were the upper class, or are the upper class in

15
00:01:38,980 --> 00:01:43,000
 India, one of the upper classes.

16
00:01:43,000 --> 00:01:52,000
 And he had some farmland and he spent his days farming.

17
00:01:53,000 --> 00:02:00,950
 And so the story tells us that he was a holder of false

18
00:02:00,950 --> 00:02:02,000
 views.

19
00:02:02,000 --> 00:02:05,500
 So as many people in India at the time of the Buddha, he

20
00:02:05,500 --> 00:02:13,000
 had views that were unspecified but contrary to reality.

21
00:02:13,000 --> 00:02:18,280
 So probably views about God and views about sacrifice and

22
00:02:18,280 --> 00:02:22,000
 ritual and all these sorts of things.

23
00:02:22,000 --> 00:02:26,600
 Views that there was no result of karma, views that God

24
00:02:26,600 --> 00:02:30,000
 would protect him, that sort of thing.

25
00:02:35,000 --> 00:02:41,450
 And he went out one day to clear his field. So I guess

26
00:02:41,450 --> 00:02:44,310
 probably he had some help, maybe, maybe not, maybe he was

27
00:02:44,310 --> 00:02:45,000
 not wealthy.

28
00:02:45,000 --> 00:02:48,870
 But he was spending his day clearing the field, maybe he

29
00:02:48,870 --> 00:02:52,000
 had a horse and an ox and a plough or something.

30
00:02:52,000 --> 00:02:58,000
 And the Buddha was walking for alms round.

31
00:02:59,000 --> 00:03:04,000
 And he saw the Brahmin and it's kind of an interesting

32
00:03:04,000 --> 00:03:10,000
 story. The Buddha engaged with people in quite unique ways.

33
00:03:10,000 --> 00:03:19,430
 Not ever the same. But one thing that he often did when he

34
00:03:19,430 --> 00:03:23,100
 saw potential in someone, which the story says that he did

35
00:03:23,100 --> 00:03:24,000
 in this Brahmin,

36
00:03:24,000 --> 00:03:27,140
 is he would ask them questions, simple questions, not even

37
00:03:27,140 --> 00:03:29,000
 questions really about the Dhamma,

38
00:03:29,000 --> 00:03:31,320
 but he would often ask people questions about what they

39
00:03:31,320 --> 00:03:32,000
 were doing.

40
00:03:32,000 --> 00:03:36,940
 And so he asked the Brahmin, "What are you doing Brahmin?"

41
00:03:36,940 --> 00:03:40,000
 And he said, "I'm clearing my field."

42
00:03:40,000 --> 00:03:44,930
 And the Buddha would ask these questions, obviously not

43
00:03:44,930 --> 00:03:49,000
 because he was ignorant of what they were doing,

44
00:03:49,000 --> 00:03:52,870
 but it's a means of not only breaking the ice and creating

45
00:03:52,870 --> 00:03:57,000
 a connection, which the Buddha seemed to want to do,

46
00:03:57,000 --> 00:04:01,240
 but it's also a means of having the Brahmin get a deeper

47
00:04:01,240 --> 00:04:05,000
 understanding of what he himself was doing.

48
00:04:05,000 --> 00:04:07,870
 It's a very good way to get people to become aware of what

49
00:04:07,870 --> 00:04:10,000
 they're doing, to be more mindful.

50
00:04:12,000 --> 00:04:16,380
 It's a great way to start a relationship in the Dhamma, to

51
00:04:16,380 --> 00:04:19,000
 ask people what they're doing,

52
00:04:19,000 --> 00:04:25,360
 and as a teacher like the Buddha to impart this sense of

53
00:04:25,360 --> 00:04:27,000
 presence.

54
00:04:27,000 --> 00:04:32,120
 He said, "I'm clearing my field." And the Buddha continued

55
00:04:32,120 --> 00:04:34,000
 on his way for alms.

56
00:04:34,000 --> 00:04:39,440
 And the next day he came back and the Brahmin was, maybe he

57
00:04:39,440 --> 00:04:41,000
 was still clearing his field,

58
00:04:41,000 --> 00:04:49,000
 but eventually he started to plant the rice.

59
00:04:49,000 --> 00:04:51,310
 And the Buddha asked him, "What are you doing?" He said, "I

60
00:04:51,310 --> 00:04:52,000
'm planting my rice."

61
00:04:52,000 --> 00:04:54,330
 And every day the Buddha would go by and ask him what he

62
00:04:54,330 --> 00:04:55,000
 was doing.

63
00:04:55,000 --> 00:05:00,440
 He said, "I'm planting the rice. Now I'm weeding the field

64
00:05:00,440 --> 00:05:01,000
."

65
00:05:01,000 --> 00:05:05,000
 Now I'm, maybe it wasn't rice. I don't think you weed rice,

66
00:05:05,000 --> 00:05:08,000
 but he was weeding the field, whatever it was.

67
00:05:09,000 --> 00:05:12,700
 Tending it, guarding it. Every day he told him what he was

68
00:05:12,700 --> 00:05:13,000
 doing.

69
00:05:13,000 --> 00:05:16,500
 Till eventually he got a sort of a friendship with the

70
00:05:16,500 --> 00:05:17,000
 Buddha.

71
00:05:17,000 --> 00:05:23,000
 And he said to the Buddha one day, he said, "Bo Gotama."

72
00:05:23,000 --> 00:05:25,360
 Bo is not very respectful, so he didn't really call him

73
00:05:25,360 --> 00:05:29,000
 venerable, but he said, "Mr. Gotama" kind of thing.

74
00:05:29,000 --> 00:05:32,000
 Gotama was the Buddha's name.

75
00:05:34,000 --> 00:05:38,650
 He said, "Every day you come by and you ask me about my

76
00:05:38,650 --> 00:05:40,000
 rice field.

77
00:05:40,000 --> 00:05:44,260
 I think I've decided that you and I shall be partners in

78
00:05:44,260 --> 00:05:45,000
 this.

79
00:05:45,000 --> 00:05:49,000
 You really seem to care about what I'm doing."

80
00:05:49,000 --> 00:05:52,000
 And he said, "You and I will be partners in this."

81
00:05:52,000 --> 00:05:58,240
 And he said, "When the rice is, when the crop is ripe, I

82
00:05:58,240 --> 00:06:00,000
 will share it with you.

83
00:06:00,000 --> 00:06:06,000
 I won't eat any of my crops without sharing with you."

84
00:06:06,000 --> 00:06:10,000
 He said, "You'll be my partner in this."

85
00:06:10,000 --> 00:06:14,000
 And the Buddha was silent and continued on in alms round.

86
00:06:14,000 --> 00:06:19,560
 Eventually the crops grew and it was the night before the

87
00:06:19,560 --> 00:06:21,000
 crops were due to be harvested.

88
00:06:21,000 --> 00:06:24,000
 And he said, "Tomorrow I'm going to harvest my crops."

89
00:06:25,000 --> 00:06:29,310
 And he was, I guess, quite excited about this and

90
00:06:29,310 --> 00:06:32,760
 anticipating and not only enjoying it himself, but sharing

91
00:06:32,760 --> 00:06:34,000
 it with his partner.

92
00:06:34,000 --> 00:06:41,630
 And that night there was a great rainstorm, a flood, and it

93
00:06:41,630 --> 00:06:46,000
 flooded away his entire crop, lost it all.

94
00:06:49,000 --> 00:06:55,570
 And in the morning he woke up and went to see his crop, how

95
00:06:55,570 --> 00:07:00,000
 it had fared, and found that it was gone.

96
00:07:00,000 --> 00:07:02,000
 And he was devastated.

97
00:07:02,000 --> 00:07:08,010
 He thought immediately, "Here I promise to share it with my

98
00:07:08,010 --> 00:07:11,000
 friend Gautama. I can't share any of it.

99
00:07:11,000 --> 00:07:16,440
 I can't, of course, enjoy it myself." And he was depressed

100
00:07:16,440 --> 00:07:20,000
 and wouldn't eat and just lay in his bed all day.

101
00:07:20,000 --> 00:07:28,230
 And the Buddha noticed that he was absent and went to his

102
00:07:28,230 --> 00:07:29,000
 door.

103
00:07:29,000 --> 00:07:32,420
 And the Brahmin's servants told him that the Buddha was

104
00:07:32,420 --> 00:07:33,000
 here.

105
00:07:33,000 --> 00:07:35,000
 He said, "Let him in and give him a seat."

106
00:07:35,000 --> 00:07:37,480
 And the Buddha came in and sat down and said, "Where's the

107
00:07:37,480 --> 00:07:38,000
 Brahmin?"

108
00:07:38,000 --> 00:07:41,000
 He said, "Oh, he's in bed."

109
00:07:41,000 --> 00:07:45,000
 And the Buddha said, "Tell him to come out."

110
00:07:45,000 --> 00:07:51,130
 And the Brahmin came out and sat down and conversed with

111
00:07:51,130 --> 00:07:55,760
 the Buddha, gave friendly greetings, and the Buddha asked

112
00:07:55,760 --> 00:07:57,000
 him, "What's wrong?"

113
00:07:57,000 --> 00:08:00,820
 And the Brahmin explained that the crop had disappeared,

114
00:08:00,820 --> 00:08:04,000
 and because of that he was devastated.

115
00:08:05,000 --> 00:08:09,000
 He was sad. He was depressed.

116
00:08:09,000 --> 00:08:16,000
 And the Buddha said, "Do you know really why you're sad?"

117
00:08:16,000 --> 00:08:22,070
 That's an interesting question because the common answer,

118
00:08:22,070 --> 00:08:25,000
 of course, is, "Well, I'm sad because I lost my crop.

119
00:08:25,000 --> 00:08:30,100
 I'm sad because what I had hoped to happen didn't happen. I

120
00:08:30,100 --> 00:08:34,000
 didn't get what I wanted," to put it simply.

121
00:08:35,000 --> 00:08:38,160
 And that's why I'm sad. So why would the Buddha ask such a

122
00:08:38,160 --> 00:08:39,000
 thing?

123
00:08:39,000 --> 00:08:44,390
 The Brahmin, when he heard this, he responded, he said, he

124
00:08:44,390 --> 00:08:49,000
 admitted that, "No, actually, I don't know."

125
00:08:49,000 --> 00:08:52,000
 He realized it's deeper than just that.

126
00:08:52,000 --> 00:08:54,000
 And he said, "But you know."

127
00:08:54,000 --> 00:09:02,050
 And the Buddha said, "I do. It's because of dhanha, craving

128
00:09:02,050 --> 00:09:04,000
, thirst."

129
00:09:04,000 --> 00:09:07,210
 The literal translation is thirst, but it just means

130
00:09:07,210 --> 00:09:08,000
 craving.

131
00:09:08,000 --> 00:09:16,080
 Because of craving comes fear. If fear arises, it arises

132
00:09:16,080 --> 00:09:17,000
 because of...

133
00:09:18,000 --> 00:09:24,000
 Sorry, if sorrow arises, it arises because of craving.

134
00:09:24,000 --> 00:09:27,000
 And then he taught this verse.

135
00:09:27,000 --> 00:09:37,000
 So the lesson here is about that distinction.

136
00:09:37,000 --> 00:09:40,810
 This is our final verse in this series of verses that are

137
00:09:40,810 --> 00:09:42,000
 all the same.

138
00:09:42,000 --> 00:09:45,430
 It's the same verse, just a different word and a different

139
00:09:45,430 --> 00:09:46,000
 story.

140
00:09:47,000 --> 00:09:49,360
 And you can see it's actually quite remarkable. If you just

141
00:09:49,360 --> 00:09:51,000
 read the verses, you don't realize it.

142
00:09:51,000 --> 00:09:54,140
 If you're reading through the verses, it seems like, "Oh

143
00:09:54,140 --> 00:09:56,000
 boy, it's the same verse again and again.

144
00:09:56,000 --> 00:09:59,000
 I get it the first time. Oh, it's just a different word."

145
00:09:59,000 --> 00:10:02,230
 But each story gives us a different facet to the dangers of

146
00:10:02,230 --> 00:10:03,000
 craving.

147
00:10:03,000 --> 00:10:06,580
 The dangers of what is ultimately the same emotional mind

148
00:10:06,580 --> 00:10:09,610
 state, but different circumstances, different qualities to

149
00:10:09,610 --> 00:10:10,000
 it.

150
00:10:11,000 --> 00:10:16,790
 So this one is in regards to ambition, really. Ambition

151
00:10:16,790 --> 00:10:21,000
 meaning in the expectation of worldly success.

152
00:10:21,000 --> 00:10:26,700
 Ambition in the sense of engaging in an activity to get a

153
00:10:26,700 --> 00:10:32,000
 result in the world, usually in economic activity.

154
00:10:32,000 --> 00:10:36,000
 So in this case, he engaged in...

155
00:10:37,000 --> 00:10:42,000
 What's the word? Farming. Agriculture, that's the word.

156
00:10:42,000 --> 00:10:46,000
 He engaged in agriculture in order to make a profit.

157
00:10:46,000 --> 00:10:49,880
 A profit for himself and a profit to be able to share with

158
00:10:49,880 --> 00:10:52,000
 his friend, his partner.

159
00:10:57,000 --> 00:11:03,770
 And it's a very common experience in life to have our

160
00:11:03,770 --> 00:11:09,940
 ambitions, our undertakings, not bear the fruit that we had

161
00:11:09,940 --> 00:11:11,000
 hoped them to.

162
00:11:12,000 --> 00:11:17,670
 This is something we see acutely in these days with people

163
00:11:17,670 --> 00:11:24,000
 who are suffering from losing their jobs, from lack of

164
00:11:24,000 --> 00:11:32,000
 employment, from having their savings depleted

165
00:11:32,000 --> 00:11:39,460
 in their savings, in stocks and retirement funds and so on,

166
00:11:39,460 --> 00:11:41,000
 all reduced.

167
00:11:41,000 --> 00:11:45,670
 I mean, that's actually the least of it, but the loss of

168
00:11:45,670 --> 00:11:50,610
 jobs, the loss of economic ability, capacity to feed

169
00:11:50,610 --> 00:11:57,570
 yourself even, to pay the bills is devastating in these

170
00:11:57,570 --> 00:11:58,000
 times.

171
00:11:59,000 --> 00:12:08,630
 People afraid, by danger has arisen, fear has arisen,

172
00:12:08,630 --> 00:12:17,230
 unable to make ends meet and unable to cope with their

173
00:12:17,230 --> 00:12:19,000
 circumstance.

174
00:12:21,000 --> 00:12:23,760
 This is reality. This is something you can't avoid. This is

175
00:12:23,760 --> 00:12:29,920
 something Buddhism doesn't really hope to avoid, although

176
00:12:29,920 --> 00:12:34,000
 there are some guidelines for why it happens.

177
00:12:34,000 --> 00:12:37,840
 It doesn't just happen randomly. And this is a wrong view

178
00:12:37,840 --> 00:12:42,470
 and it's a problematic view that we see in the world where

179
00:12:42,470 --> 00:12:44,000
 people believe that,

180
00:12:45,000 --> 00:12:50,070
 or not even believe perhaps, or just have a sense that what

181
00:12:50,070 --> 00:12:54,200
 happens, be it pleasant or unpleasant, is by chance. This

182
00:12:54,200 --> 00:12:55,000
 is a wrong view.

183
00:12:55,000 --> 00:12:59,660
 It's not random, but for all intents and purposes,

184
00:12:59,660 --> 00:13:04,180
 practically speaking, it is random in the sense that we don

185
00:13:04,180 --> 00:13:06,000
't know what's going to happen.

186
00:13:07,000 --> 00:13:10,620
 We can't control what's going to happen. It's not because I

187
00:13:10,620 --> 00:13:14,220
 want myself to only experience good things that good things

188
00:13:14,220 --> 00:13:15,000
 happen.

189
00:13:15,000 --> 00:13:19,820
 The sense that there is some non-random or non-random

190
00:13:19,820 --> 00:13:24,210
 quality to it is in the sense that it relates to our

191
00:13:24,210 --> 00:13:29,400
 relationship with the world, karma, our relationship with

192
00:13:29,400 --> 00:13:33,290
 others, with the world in general, our relationship with

193
00:13:33,290 --> 00:13:34,000
 reality.

194
00:13:35,000 --> 00:13:37,190
 Therefore, out of touch with reality, we're very much

195
00:13:37,190 --> 00:13:39,370
 liable to do things that are going to cause us problems in

196
00:13:39,370 --> 00:13:44,030
 the future, not just in this life, but throughout the

197
00:13:44,030 --> 00:13:48,000
 moment by moment existence in samsara.

198
00:13:58,000 --> 00:14:01,680
 But be that as it may, at this moment in time, we don't

199
00:14:01,680 --> 00:14:06,900
 know what's going to happen. And so for some people, it's

200
00:14:06,900 --> 00:14:11,000
 simply a matter of hedging your bets.

201
00:14:11,000 --> 00:14:15,260
 People have a general sense in their life that bad things

202
00:14:15,260 --> 00:14:19,000
 have happened to me, but I have pretty good luck.

203
00:14:20,000 --> 00:14:23,830
 And we get this sense. We may not express it verbally, voc

204
00:14:23,830 --> 00:14:26,720
ally, though some people do. I've been pretty lucky in my

205
00:14:26,720 --> 00:14:29,000
 life. You hear that? It's quite common.

206
00:14:29,000 --> 00:14:34,000
 But we have a sense that, "Okay, I'm doing okay."

207
00:14:34,000 --> 00:14:38,960
 And this is the exact... Many of the people who had this

208
00:14:38,960 --> 00:14:44,000
 had this exact same sense right up until devastation hit.

209
00:14:45,000 --> 00:14:49,000
 Brahman was thinking, "Yeah, everything was going fine."

210
00:14:49,000 --> 00:14:50,000
 And then he was utterly devastated.

211
00:14:50,000 --> 00:14:55,640
 We can't avoid. And thinking that we can avoid is negligent

212
00:14:55,640 --> 00:14:59,000
. It's negligent in the extreme.

213
00:14:59,000 --> 00:15:03,140
 Thinking that we're going to get lucky is what everyone who

214
00:15:03,140 --> 00:15:07,560
 ever experiences devastation thinks, because they weren't

215
00:15:07,560 --> 00:15:09,000
 prepared for it.

216
00:15:11,000 --> 00:15:13,290
 Not everyone. Some people are pessimistic by nature. They

217
00:15:13,290 --> 00:15:16,870
 think very bad things are going to happen. But for those

218
00:15:16,870 --> 00:15:20,000
 people, interestingly, I would say it's less devastating

219
00:15:20,000 --> 00:15:24,010
 because they weren't surprised by it. They weren't taken

220
00:15:24,010 --> 00:15:25,000
 off guard by it.

221
00:15:25,000 --> 00:15:29,720
 So there's an advantage to being pessimistic. Of course,

222
00:15:29,720 --> 00:15:33,060
 the disadvantage is quite glaring, is that you're all

223
00:15:33,060 --> 00:15:36,000
 constantly unhappy if you're pessimistic.

224
00:15:39,000 --> 00:15:44,000
 So clearly there is some better way to engage with reality.

225
00:15:44,000 --> 00:15:53,050
 The first point is that we have to accept disappointment as

226
00:15:53,050 --> 00:15:55,000
 a part of life.

227
00:15:55,000 --> 00:16:00,170
 That disappointment is a part of life. And this is an

228
00:16:00,170 --> 00:16:04,140
 important lesson because we often make like it's not a part

229
00:16:04,140 --> 00:16:05,000
 of life.

230
00:16:06,000 --> 00:16:08,770
 It may not be a part of your life. Acting like it's not

231
00:16:08,770 --> 00:16:11,680
 going to be a part of your life is negligent in the extreme

232
00:16:11,680 --> 00:16:12,000
.

233
00:16:12,000 --> 00:16:19,210
 It's what causes devastation. That is the root cause of

234
00:16:19,210 --> 00:16:25,110
 devastation. Or it's what's preventing us from preventing

235
00:16:25,110 --> 00:16:26,000
 devastation.

236
00:16:27,000 --> 00:16:33,150
 If you act as though it will not come to you, you'll never

237
00:16:33,150 --> 00:16:38,000
 work to ensure that it doesn't come to you.

238
00:16:38,000 --> 00:16:41,920
 If it's not going to come, why work to solve something that

239
00:16:41,920 --> 00:16:44,000
's not going to be a problem?

240
00:16:45,000 --> 00:16:51,320
 So understanding, it's a big part of Buddhism,

241
00:16:51,320 --> 00:16:56,000
 understanding that disappointment is a part of life.

242
00:16:56,000 --> 00:17:02,390
 Understanding that it is a potential in our lives. It's a

243
00:17:02,390 --> 00:17:04,950
 big part of answering the question because you need to ask

244
00:17:04,950 --> 00:17:06,000
 the question first.

245
00:17:06,000 --> 00:17:10,400
 How do I prepare for it? And if you don't think you have to

246
00:17:10,400 --> 00:17:12,000
, you'll never ask the question.

247
00:17:14,000 --> 00:17:17,850
 But the bigger lesson is the lesson that the Buddha gives

248
00:17:17,850 --> 00:17:23,210
 and that is that it is not that disappointment, the

249
00:17:23,210 --> 00:17:29,330
 physical reality of disappointment, that is the cause of

250
00:17:29,330 --> 00:17:31,000
 suffering.

251
00:17:31,000 --> 00:17:35,840
 You don't suffer because you didn't get what you want. You

252
00:17:35,840 --> 00:17:40,000
 suffer because you wanted it in the first place.

253
00:17:41,000 --> 00:17:44,710
 And so this Brahman was quite excited and you might even

254
00:17:44,710 --> 00:17:47,000
 say that the Buddha set him up.

255
00:17:47,000 --> 00:17:51,480
 It may very well be you could say that. The Buddha made him

256
00:17:51,480 --> 00:17:55,530
 more excited, right? Because the Buddha kept asking him and

257
00:17:55,530 --> 00:17:59,290
 kept making him think, "Oh yes, yes, I am going to enjoy

258
00:17:59,290 --> 00:18:00,000
 this."

259
00:18:01,000 --> 00:18:04,530
 I don't know if that's true. I don't want to pin that on

260
00:18:04,530 --> 00:18:07,760
 the Buddha, but it would be a useful lesson tool if you

261
00:18:07,760 --> 00:18:09,000
 think that way.

262
00:18:09,000 --> 00:18:14,990
 Take people's natural ignorance and obliviousness to

263
00:18:14,990 --> 00:18:19,450
 potential suffering when you know that something bad is

264
00:18:19,450 --> 00:18:25,020
 never going to happen and using it to teach them something.

265
00:18:25,020 --> 00:18:27,000
 It's a bit cruel, I think.

266
00:18:28,000 --> 00:18:34,700
 But this Brahman was not, he didn't suffer because he wasn

267
00:18:34,700 --> 00:18:43,100
't able to harvest his crop. He was suffering because of

268
00:18:43,100 --> 00:18:45,000
 this buildup.

269
00:18:46,000 --> 00:18:53,630
 He built up such excitement that has nothing to do with the

270
00:18:53,630 --> 00:19:00,300
 reality of cultivating a crop and then harvesting it, or

271
00:19:00,300 --> 00:19:04,000
 cultivating a crop and then not being able to harvest it.

272
00:19:05,000 --> 00:19:09,640
 And then, theoretically, one can do all the work, get no

273
00:19:09,640 --> 00:19:13,690
 result from it. Not because it was never going to bring

274
00:19:13,690 --> 00:19:19,740
 result, but because of some accident or some unfortunate

275
00:19:19,740 --> 00:19:21,000
 event.

276
00:19:24,000 --> 00:19:27,550
 You cannot suffer from it. It's possible to have all that

277
00:19:27,550 --> 00:19:31,220
 happen and be completely at peace, or at least indifferent,

278
00:19:31,220 --> 00:19:34,000
 right? Indifferent for many reasons.

279
00:19:34,000 --> 00:19:37,500
 For example, if it wasn't your crop, if it was someone else

280
00:19:37,500 --> 00:19:41,000
's and you were just doing the work and you got paid anyway,

281
00:19:41,000 --> 00:19:43,760
 you wouldn't care at all whether the crop could be

282
00:19:43,760 --> 00:19:45,000
 harvested or not.

283
00:19:46,000 --> 00:19:49,670
 But his identification, his investment in the results

284
00:19:49,670 --> 00:19:51,000
 devastated him.

285
00:19:51,000 --> 00:19:56,710
 It's not to say we can dismiss the great tragedy that this

286
00:19:56,710 --> 00:20:01,190
 Brahman suffered. I mean, it sounds like he still had

287
00:20:01,190 --> 00:20:04,720
 servants, so he probably wasn't destitute, but it was a

288
00:20:04,720 --> 00:20:07,000
 loss of livelihood on his part.

289
00:20:08,000 --> 00:20:12,730
 You know, it was expecting and it probably impacted his

290
00:20:12,730 --> 00:20:17,970
 lifestyle. For people in today's world and throughout

291
00:20:17,970 --> 00:20:21,710
 history, but what we see today in the circumstances of the

292
00:20:21,710 --> 00:20:26,170
 world is facing now. A lot of people are in fairly dire

293
00:20:26,170 --> 00:20:27,000
 straits.

294
00:20:30,000 --> 00:20:33,990
 And there's no dismissing that. It's true that its impact,

295
00:20:33,990 --> 00:20:39,420
 there's a profound impact. That is not the reason why

296
00:20:39,420 --> 00:20:46,950
 people are sad, depressed, fearful, worried about the

297
00:20:46,950 --> 00:20:54,030
 future, worried about the present, angry, frustrated, and

298
00:20:54,030 --> 00:20:55,000
 so on.

299
00:20:56,000 --> 00:21:01,190
 All of that is for a very different reason. It's because of

300
00:21:01,190 --> 00:21:06,000
 our attachment to results and our attachment to pleasure.

301
00:21:06,000 --> 00:21:11,400
 Because it's not just the anticipation of pleasure. It's

302
00:21:11,400 --> 00:21:14,000
 the pleasure in anticipating.

303
00:21:15,000 --> 00:21:19,390
 This Brahman, as with all the other stories, as with the

304
00:21:19,390 --> 00:21:23,690
 last story, the story of this man who made the beautiful

305
00:21:23,690 --> 00:21:27,490
 golden image of the woman, it wasn't just the anticipation,

306
00:21:27,490 --> 00:21:30,280
 though that was a big part of it. It was also the pleasure

307
00:21:30,280 --> 00:21:33,410
 every time he thought of being married to such a beautiful

308
00:21:33,410 --> 00:21:34,000
 woman.

309
00:21:35,000 --> 00:21:39,140
 Every time this Brahman talked with the Buddha and thought

310
00:21:39,140 --> 00:21:43,730
 about what he was going to experience, there was pleasure

311
00:21:43,730 --> 00:21:49,540
 associated with it. That's what causes greed to be such a

312
00:21:49,540 --> 00:21:53,500
 craving, to be such a pernicious mind state, such a

313
00:21:53,500 --> 00:21:56,000
 difficult mind state to overcome.

314
00:21:57,000 --> 00:22:01,270
 As it's pleasant, it's enjoyable. It's not just enjoyable

315
00:22:01,270 --> 00:22:05,000
 getting what you want. It's also enjoyable wanting it.

316
00:22:05,000 --> 00:22:08,760
 There's a pleasure involved. Not always. There can be

317
00:22:08,760 --> 00:22:12,000
 neutral, especially if you're quite mindful.

318
00:22:13,000 --> 00:22:19,640
 But there's often quite a pleasure that encourages the

319
00:22:19,640 --> 00:22:27,680
 desire. It's why it's very lightly blamed. We don't blame

320
00:22:27,680 --> 00:22:30,660
 people for wanting things. Not the way we blame people for

321
00:22:30,660 --> 00:22:34,000
 getting angry or being conceited and arrogant and so on.

322
00:22:35,000 --> 00:22:41,910
 In the world, it's more rare to scold or blame someone for

323
00:22:41,910 --> 00:22:46,410
 being greedy. We do. It happens if someone is very greedy

324
00:22:46,410 --> 00:22:50,480
 excessively, but just for wanting things? No, not the same

325
00:22:50,480 --> 00:22:54,000
 way as when people get angry or so on.

326
00:22:55,000 --> 00:22:58,310
 We don't blame it in ourselves either. When we get angry,

327
00:22:58,310 --> 00:23:01,540
 we feel we don't like that. But we don't say, "Oh, I don't

328
00:23:01,540 --> 00:23:04,280
 like being so... I don't like liking things." We like

329
00:23:04,280 --> 00:23:06,000
 liking things. We enjoy them.

330
00:23:06,000 --> 00:23:09,410
 Perhaps you've heard Buddhist teachings and you realize, "

331
00:23:09,410 --> 00:23:13,000
Oh, there's danger." And so you get upset with yourself.

332
00:23:13,000 --> 00:23:15,000
 That's not very wholesome either.

333
00:23:16,000 --> 00:23:21,760
 In general, we enjoy liking. And this is what the Brahman

334
00:23:21,760 --> 00:23:27,670
 was cultivating, this joy, this excitement that was going

335
00:23:27,670 --> 00:23:30,000
 to come to fruition.

336
00:23:31,000 --> 00:23:35,820
 Now, it's not to praise greed or craving because it's

337
00:23:35,820 --> 00:23:41,330
 pleasant. It actually makes it worse because the pleasant

338
00:23:41,330 --> 00:23:47,110
 feeling doesn't assuage or doesn't appease the craving. It

339
00:23:47,110 --> 00:23:49,000
 makes it worse, makes it stronger.

340
00:23:50,000 --> 00:23:56,820
 It's not the kind of pleasure that is content. The pleasure

341
00:23:56,820 --> 00:24:00,810
 involved with craving reinforces the craving. When you want

342
00:24:00,810 --> 00:24:04,360
 something and you're excited or you're happy about what you

343
00:24:04,360 --> 00:24:07,000
're going to get, you only want it more.

344
00:24:08,000 --> 00:24:15,340
 And so the devastation increases. This is the danger in

345
00:24:15,340 --> 00:24:22,000
 craving, the danger in the pleasure of craving.

346
00:24:23,000 --> 00:24:28,620
 And so this is the lesson, this sort of mid-level lesson of

347
00:24:28,620 --> 00:24:34,720
 how it is craving that leads to suffering. Of course, on a

348
00:24:34,720 --> 00:24:41,490
 deeper level and on a meditative level, this is sort of the

349
00:24:41,490 --> 00:24:44,000
 lesson that I've been repeating throughout these verses.

350
00:24:45,000 --> 00:24:49,450
 And even deeper than this teaching, which is an important

351
00:24:49,450 --> 00:24:53,960
 teaching on the disadvantages of craving, is the teaching

352
00:24:53,960 --> 00:24:58,430
 on how even pleasure, even getting what you want, and of

353
00:24:58,430 --> 00:25:02,600
 course the craving for getting what you want, all of it is

354
00:25:02,600 --> 00:25:04,000
 unsatisfying.

355
00:25:07,000 --> 00:25:12,200
 So the pleasure of wanting, the pleasure of getting what

356
00:25:12,200 --> 00:25:17,500
 you want, the experience of it, when you experience it as

357
00:25:17,500 --> 00:25:22,780
 it is, you see that it's not worth clinging to, that there

358
00:25:22,780 --> 00:25:29,000
's nothing special or concrete about it.

359
00:25:30,000 --> 00:25:34,480
 It's not a real thing. It's real in the sense of, it's not

360
00:25:34,480 --> 00:25:38,600
 real in the sense of lasting, in the sense of being

361
00:25:38,600 --> 00:25:42,920
 something you can hold on to. When craving arises, it

362
00:25:42,920 --> 00:25:46,000
 arises temporarily, momentarily.

363
00:25:46,000 --> 00:25:51,340
 When pleasure arises, it arises momentarily. It arises like

364
00:25:51,340 --> 00:25:56,000
 everything else. It ceases, everything that arises ceases.

365
00:25:57,000 --> 00:25:59,320
 And this isn't theory. This is what you see when you

366
00:25:59,320 --> 00:26:02,500
 practice. As you watch, if you like something and you say

367
00:26:02,500 --> 00:26:06,000
 liking, if you want something and you say wanting, wanting,

368
00:26:06,000 --> 00:26:10,940
 if you feel happy and you say happy, happy, you don't learn

369
00:26:10,940 --> 00:26:12,000
 anything profound about it.

370
00:26:12,000 --> 00:26:14,670
 You don't suddenly get this billboard that tells you, "That

371
00:26:14,670 --> 00:26:19,510
's not worth clinging to." Nothing happens. You see, there's

372
00:26:19,510 --> 00:26:20,000
 nothing to it.

373
00:26:21,000 --> 00:26:24,210
 And that, it takes time, but eventually you realize that's

374
00:26:24,210 --> 00:26:27,000
 what insight is. That's what vipassana means.

375
00:26:27,000 --> 00:26:29,930
 Vipassana is seeing that there's nothing to it. There's

376
00:26:29,930 --> 00:26:33,790
 nothing special. There is no billboard that will suddenly

377
00:26:33,790 --> 00:26:37,500
 flash and tell you what this pleasure is, what this craving

378
00:26:37,500 --> 00:26:38,000
 is.

379
00:26:38,000 --> 00:26:41,410
 You see, that craving is just craving. It's an experience

380
00:26:41,410 --> 00:26:45,740
 that arises and ceases. It's very simple. It's not much to

381
00:26:45,740 --> 00:26:49,060
 it. But that's how you'll see it. And when you start to see

382
00:26:49,060 --> 00:26:50,000
 it that way,

383
00:26:51,000 --> 00:26:53,830
 you'll see no reason to cling to anything. No one will tell

384
00:26:53,830 --> 00:26:57,270
 you nothing will tell you not to cling. It's not this

385
00:26:57,270 --> 00:27:01,000
 lesson of, "Oh yeah, that might lead to problems."

386
00:27:01,000 --> 00:27:04,000
 It's the lesson of, "This is a thing that arose and ceased

387
00:27:04,000 --> 00:27:06,940
." And so craving doesn't have any place. When you see

388
00:27:06,940 --> 00:27:10,060
 things just as they are, there's no room for clinging.

389
00:27:10,060 --> 00:27:12,000
 There's no room for craving.

390
00:27:16,000 --> 00:27:21,460
 So this is the last verse in this series of verses. We're

391
00:27:21,460 --> 00:27:25,320
 still in the piyawaga, I think. So there's a couple of more

392
00:27:25,320 --> 00:27:28,000
 about the word piyabat there on different topics.

393
00:27:28,000 --> 00:27:32,240
 And so this ends a series of talks that I think are, the

394
00:27:32,240 --> 00:27:37,090
 teachings are very important. They give us all the many

395
00:27:37,090 --> 00:27:38,000
 facets.

396
00:27:39,000 --> 00:27:41,500
 As I said, if you read the verses, you don't see this. But

397
00:27:41,500 --> 00:27:43,900
 by looking at all these stories, you see, "Oh yes, many

398
00:27:43,900 --> 00:27:46,930
 different ways that are very, very common." We see them

399
00:27:46,930 --> 00:27:48,000
 every day.

400
00:27:49,000 --> 00:27:56,890
 They're such a prevalent reality in our lives. So we have

401
00:27:56,890 --> 00:28:02,680
 losing a child, losing a loved one, losing the potential

402
00:28:02,680 --> 00:28:07,000
 for a loved one, fighting over a romantic interest.

403
00:28:08,000 --> 00:28:12,340
 The loss of livelihood or the loss of expected livelihood,

404
00:28:12,340 --> 00:28:16,610
 not getting what you want in the world, or not getting

405
00:28:16,610 --> 00:28:19,000
 perhaps what you need to survive.

406
00:28:20,000 --> 00:28:24,400
 Again, this isn't to belittle that. It's not to say that it

407
00:28:24,400 --> 00:28:29,500
's not devastating. It's not significant. It's just to say

408
00:28:29,500 --> 00:28:35,370
 that you don't have to be devastated by the very real

409
00:28:35,370 --> 00:28:40,660
 consequences or results of events that are outside of your

410
00:28:40,660 --> 00:28:41,000
 control.

411
00:28:42,000 --> 00:28:46,360
 You have two choices. You can experience it mindfully and

412
00:28:46,360 --> 00:28:51,140
 suffer physically, or you can experience it unmindfully and

413
00:28:51,140 --> 00:28:55,000
 suffer physically and mentally. That's the choice.

414
00:28:56,000 --> 00:29:02,110
 You can't choose not very readily how you're going to live

415
00:29:02,110 --> 00:29:07,230
 physically, but you can choose more readily. You can work

416
00:29:07,230 --> 00:29:12,460
 anyway to live in such a way that is free from mental

417
00:29:12,460 --> 00:29:14,000
 suffering.

418
00:29:15,000 --> 00:29:19,090
 It takes work. It's not to belittle it, but it's much more

419
00:29:19,090 --> 00:29:22,980
 stable and much more powerful than trying to control the

420
00:29:22,980 --> 00:29:27,230
 world around you so that you don't ever experience

421
00:29:27,230 --> 00:29:28,000
 disappointment.

422
00:29:28,000 --> 00:29:34,390
 So verse 216, that's the teaching for tonight. Thank you

423
00:29:34,390 --> 00:29:35,000
 all for listening.

424
00:29:36,000 --> 00:30:01,380
 [

