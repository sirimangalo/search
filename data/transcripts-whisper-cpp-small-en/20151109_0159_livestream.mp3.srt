1
00:00:00,000 --> 00:00:08,080
 Good evening everyone.

2
00:00:08,080 --> 00:00:15,120
 Welcome to our live broadcast November 8th, 2015.

3
00:00:15,120 --> 00:00:24,160
 Robin, now I'm echoing on your speakers.

4
00:00:24,160 --> 00:00:28,030
 You won't because it's the same way I didn't hear that you

5
00:00:28,030 --> 00:00:29,960
 were echoing through my speakers.

6
00:00:29,960 --> 00:00:30,960
 Not anymore.

7
00:00:30,960 --> 00:00:33,960
 Oh wait, go ahead and speak.

8
00:00:33,960 --> 00:00:37,320
 Oh wait, no it's me who speaks.

9
00:00:37,320 --> 00:00:38,320
 Now I don't hear it.

10
00:00:38,320 --> 00:00:51,480
 Now it's fine.

11
00:00:51,480 --> 00:00:55,960
 So yeah, I was invited to something else at 9pm.

12
00:00:55,960 --> 00:01:00,960
 But I rejected it.

13
00:01:00,960 --> 00:01:06,800
 It's never nice to refuse an invitation, right?

14
00:01:06,800 --> 00:01:09,400
 But it was too similar to what I'm doing here.

15
00:01:09,400 --> 00:01:15,480
 Yeah, I am echoing again.

16
00:01:15,480 --> 00:01:20,000
 On and off.

17
00:01:20,000 --> 00:01:26,800
 So they wanted me to teach meditation and answer questions.

18
00:01:26,800 --> 00:01:29,760
 But I don't know really what the group is.

19
00:01:29,760 --> 00:01:33,800
 I was on this group before once and I did this.

20
00:01:33,800 --> 00:01:39,410
 But to teach meditation again, it's, you know, I don't know

21
00:01:39,410 --> 00:01:42,480
 that it's really worthwhile.

22
00:01:42,480 --> 00:01:47,000
 If they want, they can come here and learn meditation.

23
00:01:47,000 --> 00:01:48,000
 They can read my booklet.

24
00:01:48,000 --> 00:01:52,440
 It teaches how to meditate.

25
00:01:52,440 --> 00:01:55,520
 We have 28 people signed up, I think, for the meetings.

26
00:01:55,520 --> 00:01:56,520
 So that's great.

27
00:01:56,520 --> 00:01:58,360
 A little bit of problem.

28
00:01:58,360 --> 00:02:01,920
 If you're one of those people and you're, we haven't met

29
00:02:01,920 --> 00:02:03,600
 yet, you have to really get

30
00:02:03,600 --> 00:02:06,440
 a sense of how it works.

31
00:02:06,440 --> 00:02:10,400
 And you really should visit the hangouts.google.com link.

32
00:02:10,400 --> 00:02:11,600
 Because that's your homepage.

33
00:02:11,600 --> 00:02:13,600
 That should be your hub.

34
00:02:13,600 --> 00:02:16,210
 You don't really need to use the meeting page after you've

35
00:02:16,210 --> 00:02:16,920
 signed up.

36
00:02:16,920 --> 00:02:22,190
 As long as you get the time right, you just call me on Hang

37
00:02:22,190 --> 00:02:23,040
outs.

38
00:02:23,040 --> 00:02:25,710
 Of course there's a link that comes in for the first

39
00:02:25,710 --> 00:02:26,480
 meeting.

40
00:02:26,480 --> 00:02:29,340
 There's a link, a button that comes up and you push that

41
00:02:29,340 --> 00:02:31,000
 button and it starts a Hangout

42
00:02:31,000 --> 00:02:33,440
 for you to get you into it.

43
00:02:33,440 --> 00:02:37,520
 But from there, I'm going to use the actual hangouts.google

44
00:02:37,520 --> 00:02:41,680
.com page to send you stuff.

45
00:02:41,680 --> 00:02:44,440
 So you need to have that open.

46
00:02:44,440 --> 00:02:46,260
 And you need to know that you're calling me, I'm not

47
00:02:46,260 --> 00:02:46,880
 calling you.

48
00:02:46,880 --> 00:02:51,880
 Because well, I don't have your contact information.

49
00:02:51,880 --> 00:02:54,510
 First of all, and second of all, I'm not going to go m

50
00:02:54,510 --> 00:02:55,440
ucking around.

51
00:02:55,440 --> 00:02:59,240
 It's up to you to, but the mountain doesn't go to Mohammed.

52
00:02:59,240 --> 00:03:02,480
 Is that how it goes?

53
00:03:02,480 --> 00:03:03,480
 Mohammed has to go to the mountain.

54
00:03:03,480 --> 00:03:06,360
 I don't know if that's proper, but I can't.

55
00:03:06,360 --> 00:03:12,400
 I don't know if he, I don't really have the...

56
00:03:12,400 --> 00:03:15,400
 That's right.

57
00:03:15,400 --> 00:03:18,400
 Maybe.

58
00:03:18,400 --> 00:03:32,480
 Yeah, because I have to do it 29 times, 28.

59
00:03:32,480 --> 00:03:38,220
 And so I've been thinking to maybe add another slot, but,

60
00:03:38,220 --> 00:03:41,520
 or two, or three, or four, but

61
00:03:41,520 --> 00:03:44,480
 it might require quitting university.

62
00:03:44,480 --> 00:03:45,600
 So we'll see how that works.

63
00:03:45,600 --> 00:03:49,620
 I might just decide that this university thing wasn't worth

64
00:03:49,620 --> 00:03:50,560
 anything after all.

65
00:03:50,560 --> 00:03:56,060
 I mean, I already know it's not worth that much, but I mean

66
00:03:56,060 --> 00:03:59,000
, I didn't expect this really

67
00:03:59,000 --> 00:04:02,880
 to be set up and doing so much teaching.

68
00:04:02,880 --> 00:04:07,360
 So I have to change based on the situation.

69
00:04:07,360 --> 00:04:08,960
 Is that useful?

70
00:04:08,960 --> 00:04:11,960
 That's nice.

71
00:04:11,960 --> 00:04:13,960
 Let's turn down the light over here.

72
00:04:13,960 --> 00:04:21,160
 I'll do that later.

73
00:04:21,160 --> 00:04:28,160
 So do we have any questions?

74
00:04:28,160 --> 00:04:31,160
 Yes.

75
00:04:31,160 --> 00:04:50,160
 Oh, but you know, it's not working.

76
00:04:50,160 --> 00:04:55,840
 It's not going to work.

77
00:04:55,840 --> 00:04:56,840
 That's what I need.

78
00:04:56,840 --> 00:04:57,840
 Go ahead and speak.

79
00:04:57,840 --> 00:04:58,840
 Test.

80
00:04:58,840 --> 00:04:59,840
 Test.

81
00:04:59,840 --> 00:05:01,840
 Test.

82
00:05:01,840 --> 00:05:03,840
 Test.

83
00:05:03,840 --> 00:05:05,840
 Test.

84
00:05:05,840 --> 00:05:07,840
 Hmm.

85
00:05:07,840 --> 00:05:16,840
 Look back to no output from.

86
00:05:16,840 --> 00:05:20,840
 Look back up monitor.

87
00:05:20,840 --> 00:05:22,840
 I forget.

88
00:05:22,840 --> 00:05:23,840
 I don't know.

89
00:05:23,840 --> 00:05:25,840
 Can you speak again?

90
00:05:25,840 --> 00:05:26,840
 Yes.

91
00:05:26,840 --> 00:05:28,840
 Can you hear me?

92
00:05:28,840 --> 00:05:35,840
 Yeah, but why isn't that?

93
00:05:35,840 --> 00:05:37,840
 Test.

94
00:05:37,840 --> 00:05:39,840
 Test.

95
00:05:39,840 --> 00:05:41,840
 Test.

96
00:05:41,840 --> 00:05:43,840
 No, I think it's working.

97
00:05:43,840 --> 00:05:45,840
 Maybe, maybe not.

98
00:05:45,840 --> 00:05:46,840
 Go ahead.

99
00:05:46,840 --> 00:05:47,840
 Can you say again?

100
00:05:47,840 --> 00:05:48,840
 Test.

101
00:05:48,840 --> 00:05:49,840
 No, I'm not.

102
00:05:49,840 --> 00:05:50,840
 Go for a question.

103
00:05:50,840 --> 00:05:51,840
 Oh, the question.

104
00:05:51,840 --> 00:05:53,840
 Okay.

105
00:05:53,840 --> 00:05:58,840
 Question is, Bonthe, we have a group of Tibetan monks

106
00:05:58,840 --> 00:06:00,560
 coming to our college this coming week, and their schedule

107
00:06:00,560 --> 00:06:01,840
 involves building a sand mandala.

108
00:06:01,840 --> 00:06:03,840
 Is that a sort of tradition for monks?

109
00:06:03,840 --> 00:06:05,840
 Does it have any spiritual value?

110
00:06:05,840 --> 00:06:07,840
 Thank you.

111
00:06:07,840 --> 00:06:08,840
 Yeah, they're Tibetan monks.

112
00:06:08,840 --> 00:06:12,360
 I'm not a Tibetan monk, so I'm not really well-equipped to

113
00:06:12,360 --> 00:06:13,840
 answer that question.

114
00:06:13,840 --> 00:06:18,840
 Sorry.

115
00:06:18,840 --> 00:06:21,840
 Bonthe, I have a question concerning right livelihood.

116
00:06:21,840 --> 00:06:24,840
 What are your thoughts about working in the games industry

117
00:06:24,840 --> 00:06:28,770
 and potentially making games or simulations that contain

118
00:06:28,770 --> 00:06:30,840
 violent aspects?

119
00:06:30,840 --> 00:06:32,840
 I'm not so concerned about the violent aspects.

120
00:06:32,840 --> 00:06:36,800
 I understand that concern, but it's not, I think, as

121
00:06:36,800 --> 00:06:39,840
 concerning as people make it out to be.

122
00:06:39,840 --> 00:06:44,800
 I think there is a concern there, sure, but the bigger

123
00:06:44,800 --> 00:06:47,840
 concern is the entertainment.

124
00:06:47,840 --> 00:06:48,840
 Maybe not the bigger concern.

125
00:06:48,840 --> 00:06:49,840
 I don't know.

126
00:06:49,840 --> 00:06:53,840
 But personally, to me, it's more about the entertainment,

127
00:06:53,840 --> 00:06:57,180
 doing something that's not of any value to people, not of

128
00:06:57,180 --> 00:06:57,840
 any use to them.

129
00:06:57,840 --> 00:07:04,760
 And in fact, it's helping people to waste time and to

130
00:07:04,760 --> 00:07:11,840
 become negligent.

131
00:07:11,840 --> 00:07:15,550
 So anything to do with entertainment, I think you can look

132
00:07:15,550 --> 00:07:17,840
 at the eight precepts.

133
00:07:17,840 --> 00:07:20,410
 So breaking the five precepts, if livelihood involves

134
00:07:20,410 --> 00:07:22,840
 breaking the five precepts, that's really bad.

135
00:07:22,840 --> 00:07:29,160
 But if it involves breaking the eight precepts, then it's

136
00:07:29,160 --> 00:07:40,120
 problematic because you're involved in activities that

137
00:07:40,120 --> 00:07:43,840
 encourage other people to,

138
00:07:43,840 --> 00:07:50,920
 to engage in activities that hinder them on the path,

139
00:07:50,920 --> 00:07:54,840
 obstruct them on the path.

140
00:07:54,840 --> 00:08:00,240
 So that isn't the case with livelihood like selling food or

141
00:08:00,240 --> 00:08:04,840
 necessities, things that people need,

142
00:08:04,840 --> 00:08:10,480
 so many things that you can offer or you can provide that

143
00:08:10,480 --> 00:08:16,390
 actually are either neutral or actually are conducive to

144
00:08:16,390 --> 00:08:18,840
 spiritual benefit.

145
00:08:18,840 --> 00:08:21,840
 So it's definitely a problem.

146
00:08:21,840 --> 00:08:24,840
 It's definitely not without its problems.

147
00:08:24,840 --> 00:08:36,840
 It's not wrong, wrong, but it's maybe a little wrong.

148
00:08:36,840 --> 00:08:40,960
 As to the violent aspect, I mean, you're not actually

149
00:08:40,960 --> 00:08:41,840
 killing anything.

150
00:08:41,840 --> 00:08:45,280
 And I wouldn't say you're necessarily, I mean, I used to

151
00:08:45,280 --> 00:08:48,840
 when I was young, we played Doom and Wolfenstein.

152
00:08:48,840 --> 00:08:55,160
 We played Wolfenstein before it was 3D and Warcraft,

153
00:08:55,160 --> 00:08:56,840
 Starcraft.

154
00:08:56,840 --> 00:09:01,860
 A lot of killing, a lot of games with killing. Duke Nukem,

155
00:09:01,860 --> 00:09:03,840
 we played.

156
00:09:03,840 --> 00:09:07,790
 The killing aspect is not really, you know, it's more like

157
00:09:07,790 --> 00:09:08,840
 tag, really.

158
00:09:08,840 --> 00:09:11,840
 Or yeah, it's more like tag than anything.

159
00:09:11,840 --> 00:09:14,840
 Or when I was younger, we played paintball.

160
00:09:14,840 --> 00:09:18,310
 We actually went out into the, we all had paint guns and we

161
00:09:18,310 --> 00:09:20,840
 went out into the forest and tried,

162
00:09:20,840 --> 00:09:23,010
 we spent more time trying to find each other than actually

163
00:09:23,010 --> 00:09:23,840
 shooting each other.

164
00:09:23,840 --> 00:09:27,440
 But shooting each other, no one was, there was no sense

165
00:09:27,440 --> 00:09:28,840
 that you were actually hurting each other.

166
00:09:28,840 --> 00:09:32,840
 It was a competition.

167
00:09:32,840 --> 00:09:37,510
 So really that aspect, I think, isn't nearly as problematic

168
00:09:37,510 --> 00:09:37,840
.

169
00:09:37,840 --> 00:09:42,950
 I mean, if someone is, the problem I suppose is if someone

170
00:09:42,950 --> 00:09:45,840
 is psychopathic or sociopathic,

171
00:09:45,840 --> 00:09:48,840
 then they'll take it in a whole other way.

172
00:09:48,840 --> 00:09:51,840
 And for them it's completely, it's not tag anymore.

173
00:09:51,840 --> 00:10:01,360
 It's actually a means of being cruel or enacting fantasies

174
00:10:01,360 --> 00:10:05,840
 of cruelty and evil.

175
00:10:05,840 --> 00:10:08,840
 So there is that, there is a problem there.

176
00:10:08,840 --> 00:10:14,930
 But for most people I don't think it's really all that it's

177
00:10:14,930 --> 00:10:16,840
 made out to be.

178
00:10:16,840 --> 00:10:21,260
 I spent a lot of time playing video games online and greed

179
00:10:21,260 --> 00:10:24,840
 and delusion are rampant.

180
00:10:24,840 --> 00:10:28,270
 Online I imagine it's even worse because you've got the

181
00:10:28,270 --> 00:10:29,840
 internet community.

182
00:10:29,840 --> 00:10:33,510
 I heard something about, there's a lot of awful, awful

183
00:10:33,510 --> 00:10:35,840
 stuff that goes on in the chats.

184
00:10:35,840 --> 00:10:38,280
 And they actually talk to each other, right? You've got

185
00:10:38,280 --> 00:10:38,840
 headsets.

186
00:10:38,840 --> 00:10:43,840
 Of course. We talk on mumble. We do reads on mumble.

187
00:10:43,840 --> 00:10:48,350
 Right. And so there's a lot of people calling each other

188
00:10:48,350 --> 00:10:50,840
 names and insulting each other's mothers apparently.

189
00:10:50,840 --> 00:10:53,840
 It's a big thing. Am I right?

190
00:10:53,840 --> 00:10:59,200
 Well, I mean there's a lot of trolling, sure. But there's

191
00:10:59,200 --> 00:10:59,840
 text chat as well.

192
00:10:59,840 --> 00:11:03,560
 So trade chat they call it. That's where there's a lot of

193
00:11:03,560 --> 00:11:06,840
 trolling, which is a chat for the whole realm.

194
00:11:06,840 --> 00:11:10,840
 But there's a lot of greed and a lot of delusion.

195
00:11:10,840 --> 00:11:15,640
 If you're trying to meditate and control that kind of thing

196
00:11:15,640 --> 00:11:19,840
, online games are probably the wrong direction.

197
00:11:19,840 --> 00:11:22,840
 At least that type.

198
00:11:24,840 --> 00:11:32,570
 How to accept people who treat you with no respect. I'm

199
00:11:32,570 --> 00:11:38,840
 finding it hard to let go of my feelings.

200
00:11:38,840 --> 00:11:45,840
 Yeah, well, a lot of meditation. Have you read my booklet?

201
00:11:45,840 --> 00:11:50,920
 I recommend reading my booklet, starting to practice. It

202
00:11:50,920 --> 00:11:51,840
 should help you let go.

203
00:11:51,840 --> 00:11:54,460
 I mean, letting go isn't something you can do. It's

204
00:11:54,460 --> 00:11:57,840
 something that comes naturally when you see clearly.

205
00:11:57,840 --> 00:12:08,090
 And then a follow up question on the Right Livelihood about

206
00:12:08,090 --> 00:12:11,840
 the game industry.

207
00:12:11,840 --> 00:12:16,030
 Would the response differ if the games were non-violent? In

208
00:12:16,030 --> 00:12:19,840
 other words, encouraging collaboration and cooperation?

209
00:12:19,840 --> 00:12:24,390
 Yeah, I mean, maybe a little bit. But again, it depends on

210
00:12:24,390 --> 00:12:31,040
 whether they're actually helping cultivate wholesome mind

211
00:12:31,040 --> 00:12:31,840
 states.

212
00:12:31,840 --> 00:12:35,940
 Again, it's fake, right? So you're not actually doing

213
00:12:35,940 --> 00:12:39,840
 anything for anybody. Not most of the time.

214
00:12:39,840 --> 00:12:44,180
 So what's the point of it is to have fun or to spend time

215
00:12:44,180 --> 00:12:45,840
 with each other?

216
00:12:45,840 --> 00:12:51,270
 Then it's probably not so problematic. It's still probably

217
00:12:51,270 --> 00:12:53,840
 a problem with the eight presets.

218
00:12:53,840 --> 00:12:56,500
 Isn't that what Second Life is like, though? I've never

219
00:12:56,500 --> 00:12:58,840
 actually signed into Second Life.

220
00:12:58,840 --> 00:13:02,850
 It's not a platform. It's not a game. There's no objective.

221
00:13:02,850 --> 00:13:04,840
 It's a world. It's virtual reality.

222
00:13:04,840 --> 00:13:08,980
 It's what we all expected to be already happening. We

223
00:13:08,980 --> 00:13:10,840
 expected everyone to be on Second Life.

224
00:13:10,840 --> 00:13:13,870
 But it's never really taken off the way it was expected to.

225
00:13:13,870 --> 00:13:14,840
 Not yet, anyway.

226
00:13:14,840 --> 00:13:17,740
 Now with maybe all this new virtual reality stuff, it'll

227
00:13:17,740 --> 00:13:18,840
 actually happen.

228
00:13:18,840 --> 00:13:24,280
 But Second Life is a world. You buy land, you pay real

229
00:13:24,280 --> 00:13:27,840
 money to buy land and set up a house.

230
00:13:27,840 --> 00:13:33,840
 There's a lot of dating and stuff. People walk around half

231
00:13:33,840 --> 00:13:34,840
 naked.

232
00:13:34,840 --> 00:13:37,840
 You pay real money or some kind of a game gold?

233
00:13:37,840 --> 00:13:40,840
 It's game money, but you buy it. You have to buy it.

234
00:13:40,840 --> 00:13:42,840
 With real money?

235
00:13:42,840 --> 00:13:46,090
 Yeah, there's no real way to make it. You can give each

236
00:13:46,090 --> 00:13:48,840
 other. People gave me like $4,000.

237
00:13:48,840 --> 00:13:54,620
 I have like $4,000 Linden Dollars, which I think is like $

238
00:13:54,620 --> 00:13:56,840
40 US or something.

239
00:13:56,840 --> 00:13:58,840
 That's awesome. Virtual, Donna.

240
00:13:58,840 --> 00:14:05,110
 Yeah, well, I said, I said, please stop because I can't use

241
00:14:05,110 --> 00:14:05,840
 it.

242
00:14:05,840 --> 00:14:07,840
 That's funny.

243
00:14:07,840 --> 00:14:16,840
 Just see the.

244
00:14:16,840 --> 00:14:19,420
 I am six foot seven. And when I cross, when I sit cross-

245
00:14:19,420 --> 00:14:24,000
legged, my foot ends up hurting as the position I'm in, my

246
00:14:24,000 --> 00:14:26,840
 right foot is under my left crossed leg.

247
00:14:26,840 --> 00:14:36,420
 What position should my left foot be in so I can stop this

248
00:14:36,420 --> 00:14:39,840
 from happening?

249
00:14:39,840 --> 00:14:43,750
 I'm going to just put one leg under one leg in front of the

250
00:14:43,750 --> 00:14:47,840
 other with the heel touching the shin of the back leg.

251
00:14:47,840 --> 00:14:49,330
 But it's still probably not going to be easy in the

252
00:14:49,330 --> 00:14:52,840
 beginning. It just takes time to get used to it.

253
00:14:52,840 --> 00:14:56,270
 Start. You can start with the cushions under your knees and

254
00:14:56,270 --> 00:14:56,840
 legs.

255
00:14:56,840 --> 00:15:00,840
 You can start sitting with your butt a little bit higher.

256
00:15:00,840 --> 00:15:01,840
 It's just a matter of time.

257
00:15:01,840 --> 00:15:04,840
 It's like anything. It takes training.

258
00:15:04,840 --> 00:15:08,670
 Is that what's called the Burmese position or is that

259
00:15:08,670 --> 00:15:09,840
 different?

260
00:15:09,840 --> 00:15:14,840
 Yeah, I think that's what it's called.

261
00:15:14,840 --> 00:15:18,840
 I'm not sure why.

262
00:15:18,840 --> 00:15:31,840
 Someone in Burma must have been the first.

263
00:15:31,840 --> 00:15:36,110
 OK, so apparently you can't hear me on the feed. But as

264
00:15:36,110 --> 00:15:37,840
 long as as long as they can hear you.

265
00:15:37,840 --> 00:15:40,700
 George said can't hear me on the feed. But as long as they

266
00:15:40,700 --> 00:15:43,460
 can hear the answer, I guess that's the way to hear you

267
00:15:43,460 --> 00:15:46,570
 know, that must have been a bit that's outdated because I'm

268
00:15:46,570 --> 00:15:47,840
 seeing it pick you up.

269
00:15:47,840 --> 00:15:51,840
 Oh, OK. That was seven minutes ago.

270
00:15:51,840 --> 00:15:55,790
 Bonthe, I tend to let my words slip and curse somewhat

271
00:15:55,790 --> 00:15:58,060
 often. And from what I understand, this is not in line with

272
00:15:58,060 --> 00:16:01,480
 right speech. Is it just a matter of a case of practice and

273
00:16:01,480 --> 00:16:04,920
 being more mindful of when I speak or is it not so much of

274
00:16:04,920 --> 00:16:05,840
 a problem?

275
00:16:05,840 --> 00:16:12,840
 I feel like it's not good to do.

276
00:16:12,840 --> 00:16:18,840
 Well, cursing isn't a big deal.

277
00:16:18,840 --> 00:16:23,580
 It's not really wrong speech and not exactly, but it's

278
00:16:23,580 --> 00:16:27,840
 usually associated with negative mind states.

279
00:16:27,840 --> 00:16:32,750
 No worse speeches if you call someone names harsh speeches

280
00:16:32,750 --> 00:16:37,050
 like calling someone a dog or a pig or a monkey or a goat

281
00:16:37,050 --> 00:16:41,950
 or calling people nasty names, hurting other people with

282
00:16:41,950 --> 00:16:42,840
 speech.

283
00:16:42,840 --> 00:16:49,370
 If you just say, damn it, or like this Israeli monk who

284
00:16:49,370 --> 00:16:52,840
 kept saying bullshit all the time.

285
00:16:52,840 --> 00:16:55,590
 I tried to explain to him, you know, that's probably not

286
00:16:55,590 --> 00:16:58,310
 the best thing to say around Western people about American

287
00:16:58,310 --> 00:17:00,840
 people because we don't know how it is in Israel.

288
00:17:00,840 --> 00:17:04,840
 But that word means that's a little bit course.

289
00:17:04,840 --> 00:17:08,150
 But it's not that's not considered harsh speech. Our

290
00:17:08,150 --> 00:17:11,300
 speeches. If someone says something, you say, oh, you're

291
00:17:11,300 --> 00:17:14,020
 speaking, you know, all you ever speak is bullshit or

292
00:17:14,020 --> 00:17:14,840
 something.

293
00:17:14,840 --> 00:17:21,840
 That would be harsh speech.

294
00:17:21,840 --> 00:17:31,840
 I'll cut up on questions.

295
00:17:31,840 --> 00:17:38,840
 Are there any announcements?

296
00:17:38,840 --> 00:17:42,840
 No, no, we have this. No, I've already mentioned that.

297
00:17:42,840 --> 00:17:47,840
 Not for me tomorrow. Another demo.

298
00:17:47,840 --> 00:18:16,840
 Tonight, that's all for now.

299
00:18:16,840 --> 00:18:23,890
 Well, hopefully that worked with the live with the audio. I

300
00:18:23,890 --> 00:18:24,840
 don't really know.

301
00:18:24,840 --> 00:18:28,590
 But I'm using the problem is that I'm using these which are

302
00:18:28,590 --> 00:18:30,840
 Bluetooth and it's all mixed up.

303
00:18:30,840 --> 00:18:34,370
 So hopefully that worked anyway. Thank you all for coming

304
00:18:34,370 --> 00:18:34,840
 out.

305
00:18:34,840 --> 00:18:43,840
 Thanks Robin for your help.

