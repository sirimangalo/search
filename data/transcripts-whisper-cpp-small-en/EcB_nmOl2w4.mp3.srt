1
00:00:00,000 --> 00:00:06,470
 Hello everyone. Just here today to give an update on our

2
00:00:06,470 --> 00:00:09,240
 urban Buddhist monastery and

3
00:00:09,240 --> 00:00:15,720
 meditation center project that we've begun here in Ontario,

4
00:00:15,720 --> 00:00:18,320
 Canada. The project started

5
00:00:18,320 --> 00:00:31,360
 as an attempt to gauge interest in a potential future

6
00:00:31,360 --> 00:00:33,640
 creation of a monastery and meditation

7
00:00:33,640 --> 00:00:39,060
 center here in Ontario, Canada. The date was set to January

8
00:00:39,060 --> 00:00:42,160
 of 2016 that if we had enough

9
00:00:42,160 --> 00:00:48,180
 support by that time, we would go ahead and begin to

10
00:00:48,180 --> 00:00:52,520
 acquire a property on a one-year lease

11
00:00:52,520 --> 00:00:59,030
 and begin to establish a presence here. The response was so

12
00:00:59,030 --> 00:01:01,240
 great, as you can see if you've

13
00:01:01,240 --> 00:01:11,340
 been to the project support page, that in about 10 days we

14
00:01:11,340 --> 00:01:15,280
 had received half of our

15
00:01:15,280 --> 00:01:20,160
 expected support or hoped for support. And so we asked the

16
00:01:20,160 --> 00:01:21,840
 question as to whether or

17
00:01:21,840 --> 00:01:25,080
 not, we discussed the question, whether or not we could

18
00:01:25,080 --> 00:01:26,880
 just go ahead and not wait till

19
00:01:26,880 --> 00:01:31,750
 January because in September we will be or I will be

20
00:01:31,750 --> 00:01:35,680
 involved with starting up the McMaster

21
00:01:35,680 --> 00:01:40,840
 Buddhism Association in Hamilton, Ontario at the university

22
00:01:40,840 --> 00:01:42,960
. And I will begin to get

23
00:01:42,960 --> 00:01:48,860
 involved with the university studying, teaching, Buddhism,

24
00:01:48,860 --> 00:01:51,520
 meditation and so on. So the idea

25
00:01:51,520 --> 00:01:56,860
 would be to, or the idea was that we would begin right away

26
00:01:56,860 --> 00:01:59,480
 and establish our presence

27
00:01:59,480 --> 00:02:03,720
 at the beginning of the semester school year, a September

28
00:02:03,720 --> 00:02:05,520
 school year. And we agreed that

29
00:02:05,520 --> 00:02:08,960
 this was doable and so we've gone ahead and done it. We now

30
00:02:08,960 --> 00:02:11,560
 have signed a lease and we'll

31
00:02:11,560 --> 00:02:18,820
 be moving into a property close to the McMaster University

32
00:02:18,820 --> 00:02:22,760
 in September. So the project has

33
00:02:22,760 --> 00:02:27,880
 changed, has accelerated and now we're in the phase where

34
00:02:27,880 --> 00:02:30,320
 we're looking for, looking

35
00:02:30,320 --> 00:02:32,570
 to answer the question as to whether or not this is

36
00:02:32,570 --> 00:02:34,120
 sustainable. We now have a one-year

37
00:02:34,120 --> 00:02:39,260
 lease so can we sustain it for a year and have an outlook

38
00:02:39,260 --> 00:02:41,880
 to be able to extend it into

39
00:02:41,880 --> 00:02:46,580
 the future. If at the end of the year we have the support,

40
00:02:46,580 --> 00:02:48,960
 we will continue it and this

41
00:02:48,960 --> 00:02:56,760
 will become a resource here in Ontario. So just a little

42
00:02:56,760 --> 00:02:58,240
 bit of background on the project

43
00:02:58,240 --> 00:03:01,460
 because if you're seeing this video it might be the only

44
00:03:01,460 --> 00:03:03,800
 thing that you see about the project.

45
00:03:03,800 --> 00:03:06,760
 It's on the one hand the idea is to be a resource for

46
00:03:06,760 --> 00:03:09,280
 people here in Ontario, especially at

47
00:03:09,280 --> 00:03:13,490
 the university, so for university students. But the idea

48
00:03:13,490 --> 00:03:15,840
 behind that is to create a community.

49
00:03:15,840 --> 00:03:18,750
 It doesn't matter where it is, this just happens to be the

50
00:03:18,750 --> 00:03:21,520
 place where I find myself. So the

51
00:03:21,520 --> 00:03:26,660
 idea was to start a community here to set up a network and

52
00:03:26,660 --> 00:03:29,040
 a support group that, or an

53
00:03:29,040 --> 00:03:35,210
 organization that could carry out, maintain, support the

54
00:03:35,210 --> 00:03:38,520
 activities that I've been doing

55
00:03:38,520 --> 00:03:42,330
 more or less on my own for a long time or with the help of

56
00:03:42,330 --> 00:03:44,320
 various people at various

57
00:03:44,320 --> 00:03:48,250
 times. So the idea is now to establish there's such a group

58
00:03:48,250 --> 00:03:50,680
 here as could help with things

59
00:03:50,680 --> 00:03:57,900
 like recording videos, maintenance of a website and who

60
00:03:57,900 --> 00:04:03,240
 could help organize residential courses.

61
00:04:03,240 --> 00:04:07,870
 And so part of it is going to be about local activities. So

62
00:04:07,870 --> 00:04:10,680
 we'll try to have daily meetings,

63
00:04:10,680 --> 00:04:14,710
 daily sessions here with an audience or at the new property

64
00:04:14,710 --> 00:04:16,600
 with an audience, coming

65
00:04:16,600 --> 00:04:18,860
 to listen to talks to learn how to meditate. We'll have an

66
00:04:18,860 --> 00:04:19,980
 hour a day where people can

67
00:04:19,980 --> 00:04:25,500
 come in and learn how to meditate for the first time. We'll

68
00:04:25,500 --> 00:04:27,360
 have daily talks and so

69
00:04:27,360 --> 00:04:32,360
 on. And I will be giving talks at McMaster. It'll allow me

70
00:04:32,360 --> 00:04:33,720
 to be closely involved with

71
00:04:33,720 --> 00:04:37,500
 the university and to also provide a resource for the

72
00:04:37,500 --> 00:04:40,840
 McMaster Buddhism Association, a location

73
00:04:40,840 --> 00:04:49,410
 where they can hold activities that are better suited for a

74
00:04:49,410 --> 00:04:54,560
 dedicated location. And on top

75
00:04:54,560 --> 00:04:57,760
 of that, there will be residential courses, which of course

76
00:04:57,760 --> 00:04:59,580
 will be probably most beneficial

77
00:04:59,580 --> 00:05:04,700
 to the local community. But on top of that, of course, the

78
00:05:04,700 --> 00:05:06,760
 other part of this is to be

79
00:05:06,760 --> 00:05:10,340
 a support for the international activities that I've been

80
00:05:10,340 --> 00:05:12,160
 doing for such a long time.

81
00:05:12,160 --> 00:05:15,100
 So first of all, the residential courses are international.

82
00:05:15,100 --> 00:05:16,960
 People do come from other countries.

83
00:05:16,960 --> 00:05:22,390
 So it is a resource for people who have come here where I

84
00:05:22,390 --> 00:05:25,880
 am now and not having moved yet.

85
00:05:25,880 --> 00:05:30,610
 People have come from Europe, come from America. People

86
00:05:30,610 --> 00:05:33,920
 have even, people visiting from Asia,

87
00:05:33,920 --> 00:05:40,330
 that kind of thing. All over the world, really. So there's

88
00:05:40,330 --> 00:05:43,040
 that. And there's also, of course,

89
00:05:43,040 --> 00:05:46,780
 the internet activities. Now, a lot of the things I do on

90
00:05:46,780 --> 00:05:48,600
 the internet, as you can see,

91
00:05:48,600 --> 00:05:53,300
 they've been sporadic and quickly changing. I tend to move

92
00:05:53,300 --> 00:05:55,520
 from one project to another

93
00:05:55,520 --> 00:05:59,880
 because my own situation changes. My ability and my

94
00:05:59,880 --> 00:06:02,800
 resources and the situation I find

95
00:06:02,800 --> 00:06:07,480
 myself in is fluid. Now, the idea is with a more solid

96
00:06:07,480 --> 00:06:10,200
 location that will be able to

97
00:06:10,200 --> 00:06:13,280
 set up a better schedule and with the network of people,

98
00:06:13,280 --> 00:06:15,440
 with people who can help, for example,

99
00:06:15,440 --> 00:06:21,540
 recording, encoding, uploading videos, be able to be more

100
00:06:21,540 --> 00:06:24,200
 efficient and as a result,

101
00:06:24,200 --> 00:06:26,640
 get more out there and maybe finish off some of the YouTube

102
00:06:26,640 --> 00:06:28,000
 projects that I haven't finished

103
00:06:28,000 --> 00:06:33,080
 yet. So that's basically what it's about. To make it happen

104
00:06:33,080 --> 00:06:34,960
, though, we need support.

105
00:06:34,960 --> 00:06:37,600
 And so this is what I said is we need people to take the

106
00:06:37,600 --> 00:06:39,560
 initiative. I said, "I'm not going

107
00:06:39,560 --> 00:06:41,840
 to, if we're going to do this project, it can't be under my

108
00:06:41,840 --> 00:06:43,160
 initiative. There are many

109
00:06:43,160 --> 00:06:47,420
 things as a monk that I can't be the one to initiate." And

110
00:06:47,420 --> 00:06:50,080
 so we have great volunteers

111
00:06:50,080 --> 00:06:53,330
 from the internet community who have stepped up to help

112
00:06:53,330 --> 00:06:55,080
 with that. But we also need support

113
00:06:55,080 --> 00:06:59,930
 from the general community. And so that they have, one of

114
00:06:59,930 --> 00:07:02,720
 the volunteers set up this online

115
00:07:02,720 --> 00:07:07,070
 support project, which this video is attached to. And if

116
00:07:07,070 --> 00:07:09,280
 not, then there's a link in the

117
00:07:09,280 --> 00:07:14,220
 description to the project. And please check it out. And if

118
00:07:14,220 --> 00:07:15,640
 you think it's a worthy cause,

119
00:07:15,640 --> 00:07:21,120
 then by all means, help us to make this a reality. So for

120
00:07:21,120 --> 00:07:24,480
 the benefit, if you found

121
00:07:24,480 --> 00:07:27,970
 that these teachings are beneficial, then for your own

122
00:07:27,970 --> 00:07:29,800
 benefit. If you think it's beneficial

123
00:07:29,800 --> 00:07:33,210
 to others and you want to do this as a service to the world

124
00:07:33,210 --> 00:07:35,280
, like the rest of us are doing,

125
00:07:35,280 --> 00:07:38,070
 then by all means, become a part of something great,

126
00:07:38,070 --> 00:07:40,240
 something beneficial. And we're all

127
00:07:40,240 --> 00:07:44,030
 doing this because of the importance of goodness and

128
00:07:44,030 --> 00:07:47,040
 because of a belief and an understanding

129
00:07:47,040 --> 00:07:49,650
 that goodness and happiness are closely related. If you

130
00:07:49,650 --> 00:07:52,200
 want to be happy, this requires the

131
00:07:52,200 --> 00:07:57,690
 undertaking of goodness. If you aren't cultivating goodness

132
00:07:57,690 --> 00:08:00,960
, don't expect happiness in your future.

133
00:08:00,960 --> 00:08:04,960
 So this is our practice of goodness with an intention to

134
00:08:04,960 --> 00:08:07,600
 bring happiness both to ourselves

135
00:08:07,600 --> 00:08:12,440
 and to others. So thank you for watching this. And thank

136
00:08:12,440 --> 00:08:16,000
 you to everyone, joy and appreciation

137
00:08:16,000 --> 00:08:19,650
 to everyone who has expressed their support and their

138
00:08:19,650 --> 00:08:22,320
 appreciation and most importantly,

139
00:08:22,320 --> 00:08:26,080
 who has put these teachings into practice to show us that

140
00:08:26,080 --> 00:08:27,960
 the effort is not in vain.

141
00:08:27,960 --> 00:08:32,400
 So that's all I have to say. Thank you again. Wishing you

142
00:08:32,400 --> 00:08:34,560
 all the best and peace and happiness

