1
00:00:00,000 --> 00:00:08,380
 So, we've been talking about meditation lately and how to

2
00:00:08,380 --> 00:00:16,000
 practice and why to practice.

3
00:00:16,000 --> 00:00:21,060
 The real reason for meditating is because you want to be

4
00:00:21,060 --> 00:00:22,000
 happy.

5
00:00:22,000 --> 00:00:25,120
 We all want to find happiness.

6
00:00:25,120 --> 00:00:29,000
 We all want to be free from suffering.

7
00:00:29,000 --> 00:00:31,800
 Nobody wants to suffer.

8
00:00:31,800 --> 00:00:34,720
 And yet we suffer.

9
00:00:34,720 --> 00:00:39,380
 So one of the promises that we have from the meditation

10
00:00:39,380 --> 00:00:43,700
 practice, from Buddhism, is that

11
00:00:43,700 --> 00:00:50,880
 it helps us become free from suffering.

12
00:00:50,880 --> 00:00:54,960
 This is something that we're not taught very often in life.

13
00:00:54,960 --> 00:00:56,480
 Something that we're taught to avoid.

14
00:00:56,480 --> 00:00:59,560
 That we're taught is wrong.

15
00:00:59,560 --> 00:01:04,160
 That we have taught ourselves is wrong.

16
00:01:04,160 --> 00:01:08,460
 When the baby first comes out of the womb, most often the

17
00:01:08,460 --> 00:01:10,640
 first thing they do is start

18
00:01:10,640 --> 00:01:13,280
 crying, complaining.

19
00:01:13,280 --> 00:01:24,440
 Of course they say it's instinct for the baby to cry.

20
00:01:24,440 --> 00:01:30,920
 Usually it comes with quite a bit of unhappiness.

21
00:01:30,920 --> 00:01:35,050
 We learn that the best way to get what we want is to start

22
00:01:35,050 --> 00:01:35,960
 crying.

23
00:01:35,960 --> 00:01:39,640
 Or we know that the best way we can get what we want.

24
00:01:39,640 --> 00:01:46,360
 When we want something, when we're suffering.

25
00:01:46,360 --> 00:01:49,980
 We're hungry or we're thirsty or we want this or we want

26
00:01:49,980 --> 00:01:50,640
 that.

27
00:01:50,640 --> 00:02:11,920
 We want attention, we want warmth or cool and so on.

28
00:02:11,920 --> 00:02:17,380
 But suffering is a very intrinsic part of our lives, no

29
00:02:17,380 --> 00:02:20,200
 matter how we try to run away

30
00:02:20,200 --> 00:02:22,200
 from it.

31
00:02:22,200 --> 00:02:26,610
 Even just sitting here right now, you will find that from

32
00:02:26,610 --> 00:02:28,720
 moment to moment there are

33
00:02:28,720 --> 00:02:33,880
 many things that arise that could very well bring us

34
00:02:33,880 --> 00:02:35,360
 suffering.

35
00:02:35,360 --> 00:02:41,420
 Maybe we find it too hot, maybe we find it too cold, maybe

36
00:02:41,420 --> 00:02:44,200
 we're bored because we don't

37
00:02:44,200 --> 00:02:50,570
 have the excitement or the pleasure that we're accustomed

38
00:02:50,570 --> 00:02:54,480
 to or that we're hoping for or

39
00:02:54,480 --> 00:03:01,960
 expecting when we leave here.

40
00:03:01,960 --> 00:03:05,630
 Maybe there's pain in our legs or pain in the back or pain

41
00:03:05,630 --> 00:03:06,640
 in the head.

42
00:03:06,640 --> 00:03:13,360
 Maybe we're tired and maybe we're hungry.

43
00:03:13,360 --> 00:03:17,650
 Right here and now we can see there's quite a bit of

44
00:03:17,650 --> 00:03:19,000
 suffering.

45
00:03:19,000 --> 00:03:30,490
 Our first instinct for overcoming suffering is to see it as

46
00:03:30,490 --> 00:03:40,880
 a feeling, that truth of suffering

47
00:03:40,880 --> 00:03:49,200
 is that very feeling that arises of suffering.

48
00:03:49,200 --> 00:03:51,560
 You see, that's the truth of suffering.

49
00:03:51,560 --> 00:03:55,480
 So the way to escape suffering is to run away from the

50
00:03:55,480 --> 00:03:58,720
 feelings, is to not have those feelings

51
00:03:58,720 --> 00:04:05,960
 arise.

52
00:04:05,960 --> 00:04:11,010
 It would be quite terrible if this were the case because we

53
00:04:11,010 --> 00:04:13,720
 can't be free from feelings,

54
00:04:13,720 --> 00:04:17,360
 we can't be free from painful feelings.

55
00:04:17,360 --> 00:04:23,580
 It's really the problem with our approach to the whole idea

56
00:04:23,580 --> 00:04:25,440
 of suffering.

57
00:04:25,440 --> 00:04:27,790
 So we think when we have a headache, we take some pill and

58
00:04:27,790 --> 00:04:29,120
 the headache goes away or when

59
00:04:29,120 --> 00:04:33,000
 we have a pain in the legs, we just adjust ourselves.

60
00:04:33,000 --> 00:04:41,880
 We adjust our position and then we're free from suffering.

61
00:04:41,880 --> 00:04:44,000
 But this isn't the truth of suffering.

62
00:04:44,000 --> 00:04:45,560
 This is called dukkavidhana.

63
00:04:45,560 --> 00:04:52,960
 It's just a suffering feeling.

64
00:04:52,960 --> 00:04:58,710
 So the approach to trying to escape these painful feelings

65
00:04:58,710 --> 00:04:59,960
 is wrong.

66
00:04:59,960 --> 00:05:04,780
 When people hear about suffering in a Buddhist context,

67
00:05:04,780 --> 00:05:07,360
 this is how they think of it.

68
00:05:07,360 --> 00:05:11,860
 So they think that somehow meditation is going to free them

69
00:05:11,860 --> 00:05:14,280
 from painful feelings or they

70
00:05:14,280 --> 00:05:16,730
 don't want to meditate at all because they find that

71
00:05:16,730 --> 00:05:18,760
 meditation brings painful feelings.

72
00:05:18,760 --> 00:05:23,560
 They don't want to sit still, they don't want to listen to

73
00:05:23,560 --> 00:05:25,760
 the Dhamma, they want to

74
00:05:25,760 --> 00:05:28,200
 find pleasure all the time.

75
00:05:28,200 --> 00:05:36,680
 When pain comes up, they don't know how to deal with it.

76
00:05:36,680 --> 00:05:38,720
 They think something's wrong with the pain.

77
00:05:38,720 --> 00:05:39,720
 Of course, this is natural.

78
00:05:39,720 --> 00:05:45,120
 This is what we brought up to think, to believe.

79
00:05:45,120 --> 00:05:49,820
 It's reinforced by our whole society, all of the things

80
00:05:49,820 --> 00:05:54,680
 that you can buy to escape suffering,

81
00:05:54,680 --> 00:06:00,960
 all the pleasures you can obtain to be free from suffering.

82
00:06:00,960 --> 00:06:06,580
 As humans, we have many diversions to keep us away from

83
00:06:06,580 --> 00:06:10,120
 painful feelings and yet we still

84
00:06:10,120 --> 00:06:12,960
 experience them and they still come back.

85
00:06:12,960 --> 00:06:19,680
 We can't ever escape them completely.

86
00:06:19,680 --> 00:06:28,880
 And this is what separates a person who is, you might say,

87
00:06:28,880 --> 00:06:34,280
 special, a person who is capable

88
00:06:34,280 --> 00:06:39,930
 of understanding the truth from a person who is not ready

89
00:06:39,930 --> 00:06:42,760
 to understand the truth.

90
00:06:42,760 --> 00:06:46,870
 This is really the defining factor, whether they realize it

91
00:06:46,870 --> 00:06:48,880
, whether they realize that

92
00:06:48,880 --> 00:06:53,050
 suffering is actually a part of life, whether they realize

93
00:06:53,050 --> 00:06:55,400
 that running away from suffering

94
00:06:55,400 --> 00:07:03,210
 only makes you more and more averse to it, that trying to

95
00:07:03,210 --> 00:07:07,320
 find pleasure constantly is

96
00:07:07,320 --> 00:07:11,600
 only a cause for more suffering, greater suffering.

97
00:07:11,600 --> 00:07:15,330
 As I was talking about these people who when they get sick

98
00:07:15,330 --> 00:07:17,280
 or when they're dying, you see

99
00:07:17,280 --> 00:07:19,800
 them with pleading looks in their eyes.

100
00:07:19,800 --> 00:07:25,770
 It's sad but you think, well, you did this to yourself,

101
00:07:25,770 --> 00:07:26,960
 really.

102
00:07:26,960 --> 00:07:29,650
 Did you not think that one day you were going to have to

103
00:07:29,650 --> 00:07:31,760
 face such suffering, always running

104
00:07:31,760 --> 00:07:35,720
 away from it?

105
00:07:35,720 --> 00:07:40,660
 This is one thing that we hope to avoid, this Buddhist,

106
00:07:40,660 --> 00:07:44,040
 this fear, this anxiety, this intense

107
00:07:44,040 --> 00:07:55,480
 suffering that comes and we can no longer avoid pain.

108
00:07:55,480 --> 00:08:00,090
 And this starts from realizing that this isn't the way out

109
00:08:00,090 --> 00:08:01,480
 of suffering.

110
00:08:01,480 --> 00:08:09,360
 This is not the solution.

111
00:08:09,360 --> 00:08:12,460
 And so this is why we begin to practice meditation because

112
00:08:12,460 --> 00:08:14,200
 we see we have to look closer.

113
00:08:14,200 --> 00:08:18,000
 You see, meditation is just study, studying the problem

114
00:08:18,000 --> 00:08:18,720
 closer.

115
00:08:18,720 --> 00:08:26,400
 Well, we have suffering, so let's meditate on it, let's

116
00:08:26,400 --> 00:08:28,120
 study it.

117
00:08:28,120 --> 00:08:32,880
 And so we understand that suffering is a part of life.

118
00:08:32,880 --> 00:08:38,350
 Now for some people this is enough and this makes them sort

119
00:08:38,350 --> 00:08:40,360
 of stoic about it.

120
00:08:40,360 --> 00:08:42,800
 They bear with it.

121
00:08:42,800 --> 00:08:47,600
 Some people are very good at bearing with suffering.

122
00:08:47,600 --> 00:08:50,850
 Rather than trying to seek out pleasure, they just grit and

123
00:08:50,850 --> 00:08:51,520
 bear it.

124
00:08:51,520 --> 00:08:54,010
 So their life is quite miserable but they're not taken

125
00:08:54,010 --> 00:08:55,080
 aback by suffering.

126
00:08:55,080 --> 00:09:00,860
 You might say that this is a bit better than the hedonistic

127
00:09:00,860 --> 00:09:03,640
 path because at least when

128
00:09:03,640 --> 00:09:08,360
 suffering comes they're not surprised by it.

129
00:09:08,360 --> 00:09:10,660
 But on the other hand, you might think it's more miserable

130
00:09:10,660 --> 00:09:11,960
 because they're not enjoying

131
00:09:11,960 --> 00:09:12,960
 it either way.

132
00:09:12,960 --> 00:09:16,820
 They're not free from suffering but they're also not

133
00:09:16,820 --> 00:09:19,400
 enjoying the pleasure of life.

134
00:09:19,400 --> 00:09:25,580
 You think, well, they're just living a life full of

135
00:09:25,580 --> 00:09:27,360
 suffering.

136
00:09:27,360 --> 00:09:30,200
 But still you might say that there's some sort of peace

137
00:09:30,200 --> 00:09:31,920
 that comes from understanding

138
00:09:31,920 --> 00:09:33,880
 that life has suffering.

139
00:09:33,880 --> 00:09:36,340
 Now this isn't what the Buddha had in mind either in

140
00:09:36,340 --> 00:09:38,080
 regards to the truth of suffering.

141
00:09:38,080 --> 00:09:41,370
 This is what people will often accuse Buddhists of,

142
00:09:41,370 --> 00:09:43,960
 avoiding pleasure and just living a life

143
00:09:43,960 --> 00:09:47,440
 of suffering.

144
00:09:47,440 --> 00:09:48,440
 Life is suffering, right?

145
00:09:48,440 --> 00:09:50,120
 This is what they say about Buddhists.

146
00:09:50,120 --> 00:09:51,720
 Buddhists believe that life is suffering.

147
00:09:51,720 --> 00:09:53,760
 It's not really true.

148
00:09:53,760 --> 00:10:00,480
 The Buddha never said that life is suffering.

149
00:10:00,480 --> 00:10:03,980
 Buddha was trying to find a response to the problem of the

150
00:10:03,980 --> 00:10:06,000
 existence of suffering, that

151
00:10:06,000 --> 00:10:10,720
 suffering exists.

152
00:10:10,720 --> 00:10:14,120
 And the answer isn't just to say it's part of life.

153
00:10:14,120 --> 00:10:17,560
 That's not the answer.

154
00:10:17,560 --> 00:10:19,880
 It gets you thinking and it gets you realizing that you

155
00:10:19,880 --> 00:10:20,880
 have to go further.

156
00:10:20,880 --> 00:10:24,040
 So this is an important step.

157
00:10:24,040 --> 00:10:26,440
 It's not enough but this is the step that starts you

158
00:10:26,440 --> 00:10:28,280
 looking closer and saying, "Well,

159
00:10:28,280 --> 00:10:31,680
 if it is a part of life, then how do I escape it?"

160
00:10:31,680 --> 00:10:34,560
 Not just it's a part of life so live with it.

161
00:10:34,560 --> 00:10:37,320
 But if I can't escape the pain, if I can't escape painful

162
00:10:37,320 --> 00:10:38,920
 feelings, how do I escape from

163
00:10:38,920 --> 00:10:39,920
 suffering?

164
00:10:39,920 --> 00:10:43,700
 And so we look deeper.

165
00:10:43,700 --> 00:10:51,030
 And as you look deeper, you start to realize that actually

166
00:10:51,030 --> 00:10:57,160
 there's no difference between

167
00:10:57,160 --> 00:11:01,200
 painful or pleasant experiences.

168
00:11:01,200 --> 00:11:04,920
 There's nothing good about pleasure.

169
00:11:04,920 --> 00:11:08,440
 There's nothing bad about pain.

170
00:11:08,440 --> 00:11:15,110
 You see that the truth about pleasure, the truth about pain

171
00:11:15,110 --> 00:11:17,960
 is they arise, they stay

172
00:11:17,960 --> 00:11:23,320
 for a short time and then they cease.

173
00:11:23,320 --> 00:11:29,520
 That's really all you can say about them.

174
00:11:29,520 --> 00:11:30,520
 All you should say about them.

175
00:11:30,520 --> 00:11:32,640
 Well, we can say a lot about them.

176
00:11:32,640 --> 00:11:35,960
 But the more you say it, the more you cling to or project

177
00:11:35,960 --> 00:11:37,880
 on these experiences like this

178
00:11:37,880 --> 00:11:42,130
 is good, this is bad, the more suffering you have, the more

179
00:11:42,130 --> 00:11:47,400
 upset there is in the mind.

180
00:11:47,400 --> 00:11:52,770
 And the mind is simply aware of the experience as it is,

181
00:11:52,770 --> 00:11:55,800
 this is this, that is that.

182
00:11:55,800 --> 00:11:57,600
 We realize that is truly all it is.

183
00:11:57,600 --> 00:11:59,400
 This is pleasure, this is pain.

184
00:11:59,400 --> 00:12:04,020
 There's not much more you can say about it without

185
00:12:04,020 --> 00:12:07,960
 projecting, without adding something

186
00:12:07,960 --> 00:12:11,080
 that isn't there.

187
00:12:11,080 --> 00:12:15,390
 So you say this pleasure is good, but you realize that it's

188
00:12:15,390 --> 00:12:17,120
 actually not good, there's

189
00:12:17,120 --> 00:12:20,240
 nothing good about it, it's just an experience.

190
00:12:20,240 --> 00:12:22,880
 It comes and it goes.

191
00:12:22,880 --> 00:12:27,360
 And you realize moreover that when you say it's good, when

192
00:12:27,360 --> 00:12:29,680
 you appreciate it, put some

193
00:12:29,680 --> 00:12:38,740
 value on it, you begin to create an addiction to it, an

194
00:12:38,740 --> 00:12:42,160
 attachment to it.

195
00:12:42,160 --> 00:12:45,230
 Your mind remembers that and says, "Oh, well that will make

196
00:12:45,230 --> 00:12:46,320
 me happy again."

197
00:12:46,320 --> 00:12:48,520
 And later on it comes back and says, "Hey, remember that?

198
00:12:48,520 --> 00:12:50,600
 That made me happy.

199
00:12:50,600 --> 00:12:51,600
 Let's go get that."

200
00:12:51,600 --> 00:12:55,570
 So this is why when we sit down to meditate, we are always

201
00:12:55,570 --> 00:12:58,160
 thinking about pleasant things.

202
00:12:58,160 --> 00:13:02,640
 What can I do doing this to you?

203
00:13:02,640 --> 00:13:05,500
 Pleasant sights, pleasant sounds, pleasant experiences that

204
00:13:05,500 --> 00:13:06,800
 we've had, because the mind

205
00:13:06,800 --> 00:13:08,880
 keeps them and reminds us of them.

206
00:13:08,880 --> 00:13:13,580
 When is such a helpful thing to remind us of all the things

207
00:13:13,580 --> 00:13:15,760
 that can make us happy?

208
00:13:15,760 --> 00:13:18,680
 And so we're constantly bombarded by these memories, these

209
00:13:18,680 --> 00:13:19,360
 reminders.

210
00:13:19,360 --> 00:13:21,360
 "Hey, remember that?

211
00:13:21,360 --> 00:13:23,800
 That will make you happy."

212
00:13:23,800 --> 00:13:24,800
 This is addiction.

213
00:13:24,800 --> 00:13:25,800
 This is the mind.

214
00:13:25,800 --> 00:13:28,560
 This is how the brain works.

215
00:13:28,560 --> 00:13:33,850
 The brain experiences pleasure and keeps a memory of what

216
00:13:33,850 --> 00:13:36,560
 it was that brought pleasure.

217
00:13:36,560 --> 00:13:39,240
 And then it just fires again and again randomly.

218
00:13:39,240 --> 00:13:43,110
 Even sitting here you can think of, you can realize that

219
00:13:43,110 --> 00:13:45,360
 your mind is creating these.

220
00:13:45,360 --> 00:13:49,920
 All of the pleasurable things in our lives, just by sitting

221
00:13:49,920 --> 00:13:51,880
 here we see that our mind

222
00:13:51,880 --> 00:13:54,160
 is reminding us of constantly.

223
00:13:54,160 --> 00:13:56,800
 "Hey, go do that.

224
00:13:56,800 --> 00:13:59,960
 Hey, what are you doing sitting here doing nothing?

225
00:13:59,960 --> 00:14:03,280
 Why don't you go?"

226
00:14:03,280 --> 00:14:05,600
 Or "When are we going to have a chance to do this?

227
00:14:05,600 --> 00:14:06,920
 When can we do that?

228
00:14:06,920 --> 00:14:12,700
 When can we find pleasure and enjoy the things that we

229
00:14:12,700 --> 00:14:13,960
 enjoy?"

230
00:14:13,960 --> 00:14:19,520
 And so we're never really at peace.

231
00:14:19,520 --> 00:14:21,560
 We're never really happy.

232
00:14:21,560 --> 00:14:27,680
 The mind is always wanting something that it doesn't have.

233
00:14:27,680 --> 00:14:29,670
 Even to the point that it gets in our dreams and it's

234
00:14:29,670 --> 00:14:30,840
 reminding us in our dreams.

235
00:14:30,840 --> 00:14:34,990
 And we have all sorts of crazy dreams about the things that

236
00:14:34,990 --> 00:14:35,840
 we like.

237
00:14:35,840 --> 00:14:37,880
 That's how the mind works.

238
00:14:37,880 --> 00:14:43,000
 It just keeps firing and reminding us.

239
00:14:43,000 --> 00:14:45,400
 It's addiction, how addiction works.

240
00:14:45,400 --> 00:14:47,280
 So when we don't get what we want, we're unhappy.

241
00:14:47,280 --> 00:14:49,720
 The mind is expecting.

242
00:14:49,720 --> 00:14:55,420
 There are these receptors in the brain that are waiting for

243
00:14:55,420 --> 00:14:57,000
 their drugs.

244
00:14:57,000 --> 00:14:59,360
 Because the brain is full of drugs.

245
00:14:59,360 --> 00:15:04,170
 It's got all sorts of chemicals that react with each other

246
00:15:04,170 --> 00:15:08,600
 and give us a nice feeling.

247
00:15:08,600 --> 00:15:09,600
 But we have to trigger it.

248
00:15:09,600 --> 00:15:15,040
 If we don't trigger it, then the brain is upset.

249
00:15:15,040 --> 00:15:17,200
 The brain is displeased.

250
00:15:17,200 --> 00:15:22,040
 The brain is stressed.

251
00:15:22,040 --> 00:15:28,900
 And so either we can keep chasing after pleasure or we can

252
00:15:28,900 --> 00:15:32,200
 learn to overcome this.

253
00:15:32,200 --> 00:15:41,640
 So this is what we start to realize.

254
00:15:41,640 --> 00:15:47,030
 Not only unhelpful or painful, but also meaningless and

255
00:15:47,030 --> 00:15:50,320
 totally unrelated to reality.

256
00:15:50,320 --> 00:15:53,070
 So we come to see not only that suffering is a part of life

257
00:15:53,070 --> 00:15:54,560
, but suffering is inherent

258
00:15:54,560 --> 00:15:59,000
 in every single thing.

259
00:15:59,000 --> 00:16:06,480
 Suffering is caused by every experience that we cling to.

260
00:16:06,480 --> 00:16:16,120
 Moreover we see that nothing is worth clinging to.

261
00:16:16,120 --> 00:16:20,840
 Suffering is a characteristic of all things.

262
00:16:20,840 --> 00:16:27,750
 Just like how a hot cold, pain is a characteristic of hot

263
00:16:27,750 --> 00:16:32,720
 colds, burning of fire, let's say.

264
00:16:32,720 --> 00:16:35,280
 Pain is a characteristic of fire.

265
00:16:35,280 --> 00:16:40,800
 Not directly, but as a human being it certainly is.

266
00:16:40,800 --> 00:16:47,360
 Anyone who touches fire with their hand will feel pain.

267
00:16:47,360 --> 00:16:49,850
 When the Buddha talked about suffering being inherent in

268
00:16:49,850 --> 00:16:51,320
 all things, he didn't mean that

269
00:16:51,320 --> 00:16:53,440
 their mere appearance was suffering.

270
00:16:53,440 --> 00:16:59,160
 He meant that when we cling to them, they are cause for

271
00:16:59,160 --> 00:17:00,800
 suffering.

272
00:17:00,800 --> 00:17:05,980
 Or he meant that they are useless, they are totally

273
00:17:05,980 --> 00:17:11,160
 meaningless, without any value, worthless.

274
00:17:11,160 --> 00:17:15,720
 You see, they are not worth clinging to.

275
00:17:15,720 --> 00:17:19,470
 They saw that clinging to anything is just giving rise to

276
00:17:19,470 --> 00:17:21,960
 suffering, giving rise to addiction

277
00:17:21,960 --> 00:17:27,080
 and so on.

278
00:17:27,080 --> 00:17:30,360
 And this is really the practice that we are looking for.

279
00:17:30,360 --> 00:17:34,910
 When we see clearly and when we realize this, that all of

280
00:17:34,910 --> 00:17:38,480
 our experiences are just an experience.

281
00:17:38,480 --> 00:17:45,280
 They are not good, they are not bad.

282
00:17:45,280 --> 00:17:50,520
 This is what we are aiming for in the practice.

283
00:17:50,520 --> 00:17:53,240
 But it isn't yet the truth of suffering.

284
00:17:53,240 --> 00:17:58,520
 Of course, it sounds kind of dismal, right?

285
00:17:58,520 --> 00:18:01,920
 What you get to see is that everything is useless, is

286
00:18:01,920 --> 00:18:04,160
 meaningless, is worthless.

287
00:18:04,160 --> 00:18:07,900
 But you see, it's this practice that leads you to the truth

288
00:18:07,900 --> 00:18:09,760
 of suffering, which is the

289
00:18:09,760 --> 00:18:11,720
 fourth category.

290
00:18:11,720 --> 00:18:20,440
 The true truth of suffering is the realization that this is

291
00:18:20,440 --> 00:18:23,440
 true of all things.

292
00:18:23,440 --> 00:18:26,390
 So the practice is to see clearer and clearer that this is

293
00:18:26,390 --> 00:18:28,340
 suffering, that clinging to this

294
00:18:28,340 --> 00:18:30,240
 causes suffering, and that causes suffering.

295
00:18:30,240 --> 00:18:34,900
 This is the practice that we, or the wisdom that we gain

296
00:18:34,900 --> 00:18:36,840
 from the practice.

297
00:18:36,840 --> 00:18:40,080
 But the truth of suffering is when the mind lets go.

298
00:18:40,080 --> 00:18:45,780
 When the mind changes its opinion about samsara, about

299
00:18:45,780 --> 00:18:50,520
 existence, and it says, "Wow, yes, all

300
00:18:50,520 --> 00:18:54,150
 of existence, experience is really pointless, is

301
00:18:54,150 --> 00:18:56,560
 meaningless, is worthless."

302
00:18:56,560 --> 00:19:01,560
 Of course, this is a very difficult realization to come to.

303
00:19:01,560 --> 00:19:06,770
 This is why nirvana, nirvana is such a difficult and

304
00:19:06,770 --> 00:19:09,720
 sometimes fearsome thing.

305
00:19:09,720 --> 00:19:12,880
 Because it's hard to find someone who wants to be free from

306
00:19:12,880 --> 00:19:14,300
 experience, right?

307
00:19:14,300 --> 00:19:17,080
 But you see, that's the whole problem.

308
00:19:17,080 --> 00:19:25,120
 Very difficult to understand, very difficult to accept, but

309
00:19:25,120 --> 00:19:28,760
 true, true nonetheless.

310
00:19:28,760 --> 00:19:32,130
 And eventually, a person who practices meditation realizes

311
00:19:32,130 --> 00:19:36,000
 this, comes to see that, "Yes, indeed,

312
00:19:36,000 --> 00:19:41,480
 or it rejects experience, it rejects the arising and ce

313
00:19:41,480 --> 00:19:44,720
asing of the five aggregates, it rejects

314
00:19:44,720 --> 00:19:48,680
 the arising and ceasing of the sixth sense of..."

315
00:19:48,680 --> 00:19:55,080
 And it lets go, and it enters into a state of cessation.

316
00:19:55,080 --> 00:19:59,460
 The mind, you might say, ceases, where there is total bliss

317
00:19:59,460 --> 00:20:02,000
 and peace and freedom from suffering,

318
00:20:02,000 --> 00:20:07,080
 which we call nirvana, nirvana, no arising.

319
00:20:07,080 --> 00:20:10,480
 It's the most peaceful, most cool.

320
00:20:10,480 --> 00:20:15,960
 The Buddha said, "Cool" is a good way to describe it.

321
00:20:15,960 --> 00:20:23,480
 It's the ultimate cool, the ultimate cool, you might say,

322
00:20:23,480 --> 00:20:27,480
 the coolest thing in existence.

323
00:20:27,480 --> 00:20:33,960
 Because it's absolutely cool.

324
00:20:33,960 --> 00:20:39,400
 There's no heat of defilement, no heat of suffering.

325
00:20:39,400 --> 00:20:43,160
 And so a meditator realizes this even just for a moment,

326
00:20:43,160 --> 00:20:46,160
 this experience, they have a

327
00:20:46,160 --> 00:20:50,120
 great sense of the existence of this, the existence of

328
00:20:50,120 --> 00:20:52,600
 peace, the possibility of being

329
00:20:52,600 --> 00:20:54,740
 free from suffering.

330
00:20:54,740 --> 00:21:00,840
 This is what makes them want to sodapan or a suvannas.

331
00:21:00,840 --> 00:21:07,520
 Once they have seen nirvana or realized nirvana, even just

332
00:21:07,520 --> 00:21:11,320
 for a few moments, they realize that

333
00:21:11,320 --> 00:21:15,310
 there is something outside of experience, something to

334
00:21:15,310 --> 00:21:16,640
 compare it with.

335
00:21:16,640 --> 00:21:21,650
 If you've never experienced true peace, then of course you

336
00:21:21,650 --> 00:21:24,520
 think of peace as being something

337
00:21:24,520 --> 00:21:26,480
 that you have experienced.

338
00:21:26,480 --> 00:21:31,120
 Peace is these peaceful feelings that arise.

339
00:21:31,120 --> 00:21:32,680
 That's peace.

340
00:21:32,680 --> 00:21:36,600
 Because you've never experienced true peace.

341
00:21:36,600 --> 00:21:40,500
 Once you experience it, you have this profound shift of

342
00:21:40,500 --> 00:21:43,040
 your understanding of suffering and

343
00:21:43,040 --> 00:21:46,860
 understanding of peace, you realize that true peace is

344
00:21:46,860 --> 00:21:47,800
 within us.

345
00:21:47,800 --> 00:21:50,800
 It has nothing to do with experience.

346
00:21:50,800 --> 00:21:54,680
 Right here and right now, nothing to do with any one

347
00:21:54,680 --> 00:21:58,240
 experience or any experience whatsoever.

348
00:21:58,240 --> 00:22:03,990
 It's freedom from all of this, all of these external

349
00:22:03,990 --> 00:22:08,000
 phenomena, bombarding us moment by

350
00:22:08,000 --> 00:22:10,920
 moment by moment.

351
00:22:10,920 --> 00:22:16,520
 So this is what we mean by the truth of suffering.

352
00:22:16,520 --> 00:22:19,410
 And this is what we're trying to gain in the practice, is

353
00:22:19,410 --> 00:22:20,920
 freedom from suffering.

354
00:22:20,920 --> 00:22:24,120
 This peace, this happiness, this is the true goal of the

355
00:22:24,120 --> 00:22:25,600
 meditation practice.

356
00:22:25,600 --> 00:22:32,070
 This is why we want to free ourselves from delusion, free

357
00:22:32,070 --> 00:22:35,960
 ourselves from our ignorance.

358
00:22:35,960 --> 00:22:39,360
 This is why we want to study ourselves.

359
00:22:39,360 --> 00:22:42,410
 Because when we study ourselves, we'll come to see this

360
00:22:42,410 --> 00:22:44,280
 truth, the truth of suffering.

361
00:22:44,280 --> 00:22:45,840
 It's really the only truth that you need.

362
00:22:45,840 --> 00:22:48,530
 Once you realize the truth of suffering, then you give up

363
00:22:48,530 --> 00:22:50,200
 your attachment to suffering.

364
00:22:50,200 --> 00:22:52,990
 You give up your attachment to suffering, that's the path

365
00:22:52,990 --> 00:22:54,360
 that leads to the cessation

366
00:22:54,360 --> 00:22:55,360
 of suffering.

367
00:22:55,360 --> 00:22:59,990
 Once you're on the path and you attain the cessation of

368
00:22:59,990 --> 00:23:01,280
 suffering.

369
00:23:01,280 --> 00:23:05,600
 This is what the Four Noble Truths arise all together.

370
00:23:05,600 --> 00:23:09,440
 Just like they say when you light a candle.

371
00:23:09,440 --> 00:23:16,230
 When you light a candle, the wick burns up, the wax melts,

372
00:23:16,230 --> 00:23:20,320
 the flame arises and the darkness,

373
00:23:20,320 --> 00:23:23,800
 or light arises and the darkness is dispelled.

374
00:23:23,800 --> 00:23:26,000
 All at the same time, four things happen.

375
00:23:26,000 --> 00:23:32,110
 In the same way, the Four Noble Truths arise all together

376
00:23:32,110 --> 00:23:36,280
 once you see the truth of suffering.

377
00:23:36,280 --> 00:23:38,600
 So this is what we're aiming for.

378
00:23:38,600 --> 00:23:41,420
 It's an important thing to understand, at least to the

379
00:23:41,420 --> 00:23:43,120
 extent of not running away from

380
00:23:43,120 --> 00:23:47,560
 suffering, studying it, finding a better way out.

381
00:23:47,560 --> 00:23:51,360
 Even when a person just says to themselves, "Pain, pain."

382
00:23:51,360 --> 00:23:54,990
 When a person is sick or dying or in great pain, if only we

383
00:23:54,990 --> 00:23:56,720
 could teach them all this,

384
00:23:56,720 --> 00:24:00,020
 how to remind themselves and look at it and come to see, "

385
00:24:00,020 --> 00:24:03,720
Right, the pain isn't the problem.

386
00:24:03,720 --> 00:24:06,320
 The problem is the way I'm looking at it.

387
00:24:06,320 --> 00:24:10,410
 The problem is my inability to accept the pain, my

388
00:24:10,410 --> 00:24:13,200
 inability to see the pain as just

389
00:24:13,200 --> 00:24:20,040
 pain, my projections, my reactions to the pain.

390
00:24:20,040 --> 00:24:21,040
 This is the real problem."

391
00:24:21,040 --> 00:24:27,460
 Come to see that there's no reason to concern yourself with

392
00:24:27,460 --> 00:24:29,520
 the pain at all.

393
00:24:29,520 --> 00:24:32,660
 And slowly, slowly, if you're really clear on it, then you

394
00:24:32,660 --> 00:24:34,080
 can even let go of the pain

395
00:24:34,080 --> 00:24:37,600
 and not even have to experience the pain.

396
00:24:37,600 --> 00:24:41,990
 So entering people when they're dying is quite common for

397
00:24:41,990 --> 00:24:44,680
 meditators, or relatively common

398
00:24:44,680 --> 00:24:48,920
 for meditators to become enlightened when they're passing

399
00:24:48,920 --> 00:24:49,560
 away.

400
00:24:49,560 --> 00:24:54,280
 Because of the intensity of the experience and the immedi

401
00:24:54,280 --> 00:24:56,640
acy of it, it requires them

402
00:24:56,640 --> 00:25:05,140
 to either cling or let go, forces them to see suffering

403
00:25:05,140 --> 00:25:09,800
 directly and encourages them

404
00:25:09,800 --> 00:25:12,960
 to let go because there's nothing to cling to.

405
00:25:12,960 --> 00:25:18,580
 So, but for all of us, this is an important teaching to

406
00:25:18,580 --> 00:25:21,920
 keep in mind when we do practice

407
00:25:21,920 --> 00:25:27,200
 meditation so that we're not partial to any one experience.

408
00:25:27,200 --> 00:25:32,480
 We're able to experience the whole range of phenomena that

409
00:25:32,480 --> 00:25:35,280
 arise without partiality.

410
00:25:35,280 --> 00:25:39,440
 We're able to see them as they are.

411
00:25:39,440 --> 00:25:41,440
 So that's the dhamma for tonight.

412
00:25:41,440 --> 00:25:45,070
 Now we can put it into practice by studying ourselves

413
00:25:45,070 --> 00:25:46,800
 through meditation.

414
00:25:46,800 --> 00:25:48,120
 We need to practice together.

