1
00:00:00,000 --> 00:00:07,000
 It's funny the broadcast just stopped.

2
00:00:07,000 --> 00:00:36,000
 Here I am going on and on.

3
00:00:36,000 --> 00:00:45,000
 I'm sorry about that. I'm back now.

4
00:00:45,000 --> 00:00:48,000
 What was the last thing anyone heard?

5
00:00:48,000 --> 00:00:56,000
 I was just talking about Second Life.

6
00:00:56,000 --> 00:01:02,000
 Ten minutes of it.

7
00:01:02,000 --> 00:01:08,910
 To sum up what I just said, and nobody heard but me and the

8
00:01:08,910 --> 00:01:15,000
 mice in my room,

9
00:01:15,000 --> 00:01:24,980
 Second Life is an interesting platform. It might be hard

10
00:01:24,980 --> 00:01:29,000
 for some people.

11
00:01:29,000 --> 00:01:34,260
 It might not work for some computers, but I'll try to

12
00:01:34,260 --> 00:01:37,000
 upload it to YouTube afterwards.

13
00:01:37,000 --> 00:01:41,050
 The TEDx talk, first of all I'm not even sure they're going

14
00:01:41,050 --> 00:01:42,000
 to choose me,

15
00:01:42,000 --> 00:01:46,880
 but it looks good as long as I don't really watch the

16
00:01:46,880 --> 00:01:48,000
 interviewers,

17
00:01:48,000 --> 00:01:51,310
 or it doesn't seem too boring to them what I'm talking

18
00:01:51,310 --> 00:01:52,000
 about.

19
00:01:52,000 --> 00:01:59,000
 I'll probably get in. We'll see. I don't, I don't.

20
00:01:59,000 --> 00:02:02,000
 I don't know that I'll be given a recording.

21
00:02:02,000 --> 00:02:05,970
 I'm assuming that they record them and put them up on their

22
00:02:05,970 --> 00:02:14,000
 TEDx channel.

23
00:02:14,000 --> 00:02:25,800
 Saturday, come on by. See if you can get Second Life

24
00:02:25,800 --> 00:02:27,000
 working.

25
00:02:27,000 --> 00:02:35,000
 That's all. I'm going to sign off now.

26
00:02:35,000 --> 00:02:40,000
 It's been a long day as I said.

27
00:02:40,000 --> 00:02:44,560
 I'll be back tomorrow and then Saturday. I'll do the Second

28
00:02:44,560 --> 00:02:45,000
 Life thing.

29
00:02:45,000 --> 00:02:47,000
 See how it works.

30
00:02:47,000 --> 00:02:50,940
 Have a good night everyone. Thanks for joining me with the

31
00:02:50,940 --> 00:02:52,000
 meditation.

32
00:02:52,000 --> 00:02:58,000
 Have a good rest of people, as usual. Good night.

33
00:02:59,000 --> 00:03:14,000
 [

