1
00:00:00,000 --> 00:00:04,000
 Hello and welcome back to our study of the Dhammatandah.

2
00:00:04,000 --> 00:00:05,060
 Today we continue on with

3
00:00:05,060 --> 00:00:09,300
 verse number 96 which reads as follows

4
00:00:09,300 --> 00:00:17,560
 Santam tasa manan hoti santa vajaj kamah ca samadanya wimut

5
00:00:17,560 --> 00:00:19,720
asa upasantasa

6
00:00:19,720 --> 00:00:31,880
 tathaino which means calm or peaceful is his or is that

7
00:00:31,880 --> 00:00:35,360
 person's mind peaceful

8
00:00:35,360 --> 00:00:45,330
 their speech and their acts who with right knowledge is

9
00:00:45,330 --> 00:00:47,760
 freed

10
00:00:49,520 --> 00:00:57,000
 such a one who is such a one for such a one no such a one

11
00:00:57,000 --> 00:00:59,680
 who is at peace

12
00:00:59,680 --> 00:01:07,030
 themselves so one who has right right who is delivered

13
00:01:07,030 --> 00:01:10,520
 through right knowledge

14
00:01:10,880 --> 00:01:23,490
 who is at peace themselves something like that so talking

15
00:01:23,490 --> 00:01:25,520
 about the three

16
00:01:25,520 --> 00:01:33,280
 doors the three doors of karma the mind or the word speech

17
00:01:33,280 --> 00:01:35,120
 door and the body

18
00:01:35,120 --> 00:01:42,290
 door so the three ways we can perform karma and such a such

19
00:01:42,290 --> 00:01:43,280
 a one who has

20
00:01:43,280 --> 00:01:49,420
 become free is peaceful in all of these this story is one

21
00:01:49,420 --> 00:01:51,380
 of the more heart

22
00:01:51,380 --> 00:02:02,640
 warming stories not a long story the story about a certain

23
00:02:02,640 --> 00:02:05,360
 monk who seems to

24
00:02:05,360 --> 00:02:12,140
 have practiced well learned all of the rules of the monk

25
00:02:12,140 --> 00:02:14,560
hood he was known as

26
00:02:14,560 --> 00:02:19,040
 kosambhi wasi tisa his name was Tisa Tisa was like a common

27
00:02:19,040 --> 00:02:19,720
 name like John

28
00:02:19,720 --> 00:02:24,990
 back then it's a very common name but kosambhi wasi means

29
00:02:24,990 --> 00:02:28,200
 he dwelled at kosambhi

30
00:02:28,200 --> 00:02:34,130
 and he stayed there for the three rains for the three

31
00:02:34,130 --> 00:02:36,400
 months of the rains

32
00:02:36,400 --> 00:02:41,870
 retreat and at the end the lay person who looked at who was

33
00:02:41,870 --> 00:02:42,920
 looking after him

34
00:02:42,920 --> 00:02:50,280
 came to him and and offered him a set of robes and some

35
00:02:50,280 --> 00:02:52,680
 medicines so the kind of

36
00:02:52,680 --> 00:03:00,230
 medicines like sugar or honey or ghee like butter you know

37
00:03:00,230 --> 00:03:01,160
 kind of medicines

38
00:03:01,160 --> 00:03:03,420
 that actually you can only keep for a certain amount of

39
00:03:03,420 --> 00:03:06,480
 time and so the monk

40
00:03:06,480 --> 00:03:12,430
 didn't immediately receive them because monks can only have

41
00:03:12,430 --> 00:03:15,840
 one set of robes and

42
00:03:15,840 --> 00:03:20,100
 they can only keep these medicines for a short time and can

43
00:03:20,100 --> 00:03:21,120
 only use them when

44
00:03:21,120 --> 00:03:24,240
 they're sick and that kind of thing so he says something

45
00:03:24,240 --> 00:03:25,720
 that's actually quite

46
00:03:25,720 --> 00:03:28,270
 interesting from a monastic point of view he says he asks

47
00:03:28,270 --> 00:03:29,240
 what are they in the

48
00:03:29,240 --> 00:03:31,370
 label says oh these are the offering that we give to the

49
00:03:31,370 --> 00:03:32,200
 monk whenever they

50
00:03:32,200 --> 00:03:37,830
 whoever stays with us please accept them and he says I'm

51
00:03:37,830 --> 00:03:39,120
 sorry I have no need of

52
00:03:39,120 --> 00:03:44,030
 need of these because he can't reasonably take them and use

53
00:03:44,030 --> 00:03:45,680
 them and

54
00:03:45,680 --> 00:03:49,240
 he says why can't you take them and he says what he says is

55
00:03:49,240 --> 00:03:49,840
 interesting he

56
00:03:49,840 --> 00:03:56,670
 says that he has no novice to to perform the usual duties

57
00:03:56,670 --> 00:03:58,600
 and I think the meaning

58
00:03:58,600 --> 00:04:02,600
 here is that the novice would take the robes and offer them

59
00:04:02,600 --> 00:04:03,640
 to the elder if

60
00:04:03,640 --> 00:04:06,470
 they're if the need should ever should arise so they were

61
00:04:06,470 --> 00:04:07,540
 using the novices

62
00:04:07,540 --> 00:04:10,550
 it's kind of odd to sort of get around the world rules by

63
00:04:10,550 --> 00:04:11,840
 having a novice carry

64
00:04:11,840 --> 00:04:14,490
 these things for you and I'm not convinced that it's the

65
00:04:14,490 --> 00:04:15,240
 best reading of

66
00:04:15,240 --> 00:04:20,810
 the rules but these stories tend to have those sorts of

67
00:04:20,810 --> 00:04:22,240
 practices in them that

68
00:04:22,240 --> 00:04:26,220
 don't seem exactly to be how it was laid out in them by the

69
00:04:26,220 --> 00:04:27,880
 Buddha but anyway who

70
00:04:27,880 --> 00:04:31,830
 am I to know but that's that's sort of how it went whether

71
00:04:31,830 --> 00:04:32,640
 right or wrong I

72
00:04:32,640 --> 00:04:39,430
 don't know and so the layman says oh well if that's true

73
00:04:39,430 --> 00:04:42,000
 take my son take my son

74
00:04:42,000 --> 00:04:44,870
 with you and have him ordain as a novice and he'll look

75
00:04:44,870 --> 00:04:45,800
 after you and I guess

76
00:04:45,800 --> 00:04:50,960
 carry your sugar and your butter around for you because no

77
00:04:50,960 --> 00:04:51,600
 misses don't have

78
00:04:51,600 --> 00:04:56,910
 those kind of rules that they can't keep this can't keep

79
00:04:56,910 --> 00:04:58,760
 that and so he accepted

80
00:04:58,760 --> 00:05:05,800
 and this his son who was seven years old he gives him over

81
00:05:05,800 --> 00:05:08,840
 to the elder and says

82
00:05:08,840 --> 00:05:12,410
 please accept him as a novice so the other take other takes

83
00:05:12,410 --> 00:05:14,120
 the boy wets his

84
00:05:14,120 --> 00:05:20,080
 hair and teaches him the dajjapancha kamatana the fivefold

85
00:05:20,080 --> 00:05:20,960
 meditation with

86
00:05:20,960 --> 00:05:26,100
 the skin as its fifth meaning Kesa hair on the head Loma

87
00:05:26,100 --> 00:05:27,920
 hair on the body naka

88
00:05:27,920 --> 00:05:36,920
 nails than ta teeth that Joe is the skin Kesa Loma naka

89
00:05:36,920 --> 00:05:38,400
 than that and Joe these

90
00:05:38,400 --> 00:05:41,420
 are what you teach them a new monk it's the tradition

91
00:05:41,420 --> 00:05:42,720
 because it's a basic

92
00:05:42,720 --> 00:05:45,990
 meditation focusing on the parts of the body to help see

93
00:05:45,990 --> 00:05:46,800
 them as they are and

94
00:05:46,800 --> 00:05:54,410
 not be partial towards them or against them useful for

95
00:05:54,410 --> 00:05:55,760
 monks if they find

96
00:05:55,760 --> 00:05:58,900
 themselves getting into thoughts of lust about the body

97
00:05:58,900 --> 00:06:03,920
 lust for the body and as

98
00:06:03,920 --> 00:06:07,140
 soon as the razor it says as soon as the razor touched his

99
00:06:07,140 --> 00:06:08,720
 head the novice became

100
00:06:08,720 --> 00:06:15,060
 an Arhan the boy became an Arhan he was ready he was he was

101
00:06:15,060 --> 00:06:15,920
 clearly someone who

102
00:06:15,920 --> 00:06:20,450
 had been cultivating perfections and been cultivating good

103
00:06:20,450 --> 00:06:21,640
 things for a long time

104
00:06:21,640 --> 00:06:26,200
 because that was all it took just this this click with

105
00:06:26,200 --> 00:06:27,600
 teaching of this

106
00:06:27,600 --> 00:06:33,110
 meditation and the changing the the recognition probably he

107
00:06:33,110 --> 00:06:33,960
 was a monk in his

108
00:06:33,960 --> 00:06:40,280
 past life and so he became an Arhan right away and didn't

109
00:06:40,280 --> 00:06:40,880
 tell the elder the

110
00:06:40,880 --> 00:06:43,620
 elder had no idea because the elder actually being a good

111
00:06:43,620 --> 00:06:44,600
 monk but he was

112
00:06:44,600 --> 00:06:51,140
 but he wasn't enlightened he was just an ordinary world

113
00:06:51,140 --> 00:06:54,200
 thing and so he stayed

114
00:06:54,200 --> 00:06:58,290
 there for another two weeks and then he decided to go back

115
00:06:58,290 --> 00:06:59,600
 to see the Buddha I

116
00:06:59,600 --> 00:07:02,060
 thought well I'll bring this novice to go and see the

117
00:07:02,060 --> 00:07:03,320
 Buddha that'll be a good

118
00:07:03,320 --> 00:07:07,420
 way to introduce him to the the Buddha's Sassan and maybe

119
00:07:07,420 --> 00:07:08,320
 the Buddha can teach

120
00:07:08,320 --> 00:07:10,760
 him something maybe it'll help him on his own breath

121
00:07:10,760 --> 00:07:12,200
 because who knows maybe

122
00:07:12,200 --> 00:07:15,400
 one day this novice will become a good meditator like me

123
00:07:15,400 --> 00:07:18,600
 something like that and

124
00:07:18,600 --> 00:07:24,890
 so on the way they stopped off at this lodging and at a

125
00:07:24,890 --> 00:07:27,080
 monastery but they only

126
00:07:27,080 --> 00:07:38,360
 had one Kutti and because the novice spent all his time

127
00:07:38,360 --> 00:07:39,040
 looking after the

128
00:07:39,040 --> 00:07:41,630
 elder he got a Kutti for the elder and he spent his time

129
00:07:41,630 --> 00:07:42,800
 doing all these duties

130
00:07:42,800 --> 00:07:48,490
 that the novice would have to do is sweeping it out and

131
00:07:48,490 --> 00:07:49,480
 opening windows

132
00:07:49,480 --> 00:07:56,790
 closing windows putting on putting the bed down and so on

133
00:07:56,790 --> 00:07:58,320
 and by the time he

134
00:07:58,320 --> 00:08:01,630
 had taken care of the elders needs it was too late for him

135
00:08:01,630 --> 00:08:02,560
 to get a Kutti for

136
00:08:02,560 --> 00:08:09,660
 himself a hut for himself and so the elder said you know

137
00:08:09,660 --> 00:08:10,160
 you don't have a

138
00:08:10,160 --> 00:08:13,440
 why they said oh don't you have a Kutti for yourself no I'm

139
00:08:13,440 --> 00:08:14,960
 sorry I don't know

140
00:08:14,960 --> 00:08:18,860
 opportunity so the elder said well then fine you just stay

141
00:08:18,860 --> 00:08:20,240
 with me you can just

142
00:08:20,240 --> 00:08:25,480
 stay with me and in the morning well we'll look after it

143
00:08:25,480 --> 00:08:26,840
 would be better

144
00:08:26,840 --> 00:08:30,920
 better to stay with me than elsewhere

145
00:08:31,800 --> 00:08:34,200
 and then the elder goes and lies down on the bed and

146
00:08:34,200 --> 00:08:36,000
 immediately falls asleep

147
00:08:36,000 --> 00:08:40,050
 because he's just an ordinary world thing but the novice

148
00:08:40,050 --> 00:08:41,600
 thinking the novice

149
00:08:41,600 --> 00:08:45,490
 realizes something and that is that this is for the past

150
00:08:45,490 --> 00:08:47,080
 two nights he has also

151
00:08:47,080 --> 00:08:50,910
 spent the night with novices have a rule he's not a full

152
00:08:50,910 --> 00:08:52,560
 monk a novice takes on

153
00:08:52,560 --> 00:08:55,680
 ten precepts so they're allowed to wear the robes but they

154
00:08:55,680 --> 00:08:56,400
 haven't yet become

155
00:08:56,400 --> 00:09:04,410
 monk so they haven't they are not considered to be in the

156
00:09:04,410 --> 00:09:05,560
 inner circle I

157
00:09:05,560 --> 00:09:08,330
 guess you could say but the point is they still don't fit

158
00:09:08,330 --> 00:09:09,240
 in with the monks

159
00:09:09,240 --> 00:09:17,970
 and so to be in too close association can lead to problems

160
00:09:17,970 --> 00:09:18,800
 and and

161
00:09:18,800 --> 00:09:24,420
 difficulties for the monks so they are only so only allowed

162
00:09:24,420 --> 00:09:25,500
 to stay with the

163
00:09:25,500 --> 00:09:29,220
 monks for a maximum of two nights after the third night it

164
00:09:29,220 --> 00:09:30,060
's not that they're not

165
00:09:30,060 --> 00:09:32,900
 allowed to it's that the monk commits an offense if he

166
00:09:32,900 --> 00:09:34,280
 stays with a non-ordained

167
00:09:34,280 --> 00:09:39,490
 person three nights in a row meaning so because the ideas

168
00:09:39,490 --> 00:09:40,760
 monks have to stick to

169
00:09:40,760 --> 00:09:43,370
 themselves have to not get too in too close association

170
00:09:43,370 --> 00:09:44,320
 with people who are

171
00:09:44,320 --> 00:09:47,860
 not keeping the rules because it can drag them down and so

172
00:09:47,860 --> 00:09:49,400
 on and it can

173
00:09:49,400 --> 00:09:54,630
 people who haven't yet been trained as monks can often

174
00:09:54,630 --> 00:09:56,400
 criticize unjustly you

175
00:09:56,400 --> 00:09:58,650
 know well the story went that the monks were snoring or

176
00:09:58,650 --> 00:09:59,680
 something in the monks

177
00:09:59,680 --> 00:10:03,700
 and the lay people started getting critical of them and

178
00:10:03,700 --> 00:10:04,880
 overly critical or

179
00:10:04,880 --> 00:10:08,900
 that kind of thing you know they lose any sense of respect

180
00:10:08,900 --> 00:10:10,600
 so anyway there's

181
00:10:10,600 --> 00:10:15,010
 some sense that they have to be a bit separate and then

182
00:10:15,010 --> 00:10:15,920
 obvious realize this

183
00:10:15,920 --> 00:10:19,400
 and you realize if I stay in here overnight as he told me

184
00:10:19,400 --> 00:10:20,440
 he'll be

185
00:10:20,440 --> 00:10:25,500
 breaking a rule can't have that so the novice sits up sits

186
00:10:25,500 --> 00:10:27,080
 cross-legged and

187
00:10:27,080 --> 00:10:30,320
 comfortably because he's in our hunt just does practice his

188
00:10:30,320 --> 00:10:30,560
 sitting

189
00:10:30,560 --> 00:10:34,640
 meditation all night because the loophole to the rule again

190
00:10:34,640 --> 00:10:34,920
 it's all

191
00:10:34,920 --> 00:10:39,200
 about loopholes it seems is that if he's sitting up it's

192
00:10:39,200 --> 00:10:40,160
 not considered to have

193
00:10:40,160 --> 00:10:45,460
 slept have stayed together overnight don't ask me but that

194
00:10:45,460 --> 00:10:46,880
's somehow helps

195
00:10:46,880 --> 00:10:52,960
 to fix his things and he stayed there all night and the

196
00:10:52,960 --> 00:10:54,160
 elder just snored away

197
00:10:54,160 --> 00:10:57,550
 totally oblivious but the elder did wake up in the morning

198
00:10:57,550 --> 00:10:58,600
 and was actually

199
00:10:58,600 --> 00:11:01,700
 conscientious conscientious enough to think about this and

200
00:11:01,700 --> 00:11:02,400
 he woke up and it

201
00:11:02,400 --> 00:11:05,950
 completely pitched dark still the night still still not a

202
00:11:05,950 --> 00:11:08,720
 full night and so he

203
00:11:08,720 --> 00:11:12,320
 he picks up his he's on the bed and he reaches over and he

204
00:11:12,320 --> 00:11:14,000
 picks up his fan so

205
00:11:14,000 --> 00:11:17,180
 in those times the monks would have a fan to blow driveway

206
00:11:17,180 --> 00:11:18,320
 mosquitoes and to

207
00:11:18,320 --> 00:11:20,620
 cool themselves off in the heat and that kind of thing

208
00:11:20,620 --> 00:11:22,400
 different uses for a fan

209
00:11:22,400 --> 00:11:27,280
 and he he took the fan and he pounded the mat where he knew

210
00:11:27,280 --> 00:11:28,800
 the the novice was

211
00:11:28,800 --> 00:11:34,210
 and he said hey wake up you have to go outside and then the

212
00:11:34,210 --> 00:11:35,320
 novice didn't say

213
00:11:35,320 --> 00:11:40,620
 anything so he threw the he threw the fan at the novice he

214
00:11:40,620 --> 00:11:41,800
 didn't see him he

215
00:11:41,800 --> 00:11:43,540
 didn't know where the novice was he thought the novice was

216
00:11:43,540 --> 00:11:44,160
 lying down so he

217
00:11:44,160 --> 00:11:46,920
 said okay oh you know maybe aim towards his feet or

218
00:11:46,920 --> 00:11:48,680
 something and the novice

219
00:11:48,680 --> 00:11:53,990
 because he was sitting up the fan hit him square in the eye

220
00:11:53,990 --> 00:11:55,880
 and poked his eye

221
00:11:55,880 --> 00:12:00,280
 out destroyed his eye

222
00:12:00,280 --> 00:12:13,970
 and the other had no idea the other had no clue the novice

223
00:12:13,970 --> 00:12:14,680
 knowing that his eye

224
00:12:14,680 --> 00:12:19,180
 was poked out what does he do he says to the elder what did

225
00:12:19,180 --> 00:12:20,760
 you say because I

226
00:12:20,760 --> 00:12:23,620
 guess he was a little distracted he says get out get out it

227
00:12:23,620 --> 00:12:26,120
's time to go out so

228
00:12:26,120 --> 00:12:30,280
 the novice what does he do covers his eye whichever eye it

229
00:12:30,280 --> 00:12:31,240
 was covers his eye

230
00:12:31,240 --> 00:12:37,040
 with one hand and gets up and walks out doesn't say

231
00:12:37,040 --> 00:12:39,360
 anything about it completely

232
00:12:39,360 --> 00:12:44,470
 quiet and on top of that not only does he is he totally

233
00:12:44,470 --> 00:12:46,080
 neutral totally composed

234
00:12:46,080 --> 00:12:53,020
 having just had his eye poked out he goes and does the

235
00:12:53,020 --> 00:12:54,800
 duties of a visiting

236
00:12:54,800 --> 00:12:59,490
 monk so he goes and sweeps the tray it sweeps the pads

237
00:12:59,490 --> 00:13:01,280
 sweeps out the the

238
00:13:01,280 --> 00:13:07,220
 outhouse sets out water for the elder washing he sweeps you

239
00:13:07,220 --> 00:13:07,720
 know he's got one

240
00:13:07,720 --> 00:13:10,740
 hand and so he's sweeping with one hand with the broom

241
00:13:10,740 --> 00:13:11,880
 sweeping out the

242
00:13:11,880 --> 00:13:15,670
 house and putting out water for washing and water for

243
00:13:15,670 --> 00:13:18,440
 drinking and then he even

244
00:13:18,440 --> 00:13:22,040
 goes and gets a tooth a tooth they had these sticks that

245
00:13:22,040 --> 00:13:22,800
 they would use to

246
00:13:22,800 --> 00:13:25,400
 brush their teeth you can still get them in India it's kind

247
00:13:25,400 --> 00:13:26,760
 of neat on the Buddhist

248
00:13:26,760 --> 00:13:30,780
 Circuit you can I got a whole bunch of them when I was

249
00:13:30,780 --> 00:13:32,480
 there last year and so

250
00:13:32,480 --> 00:13:36,600
 he brought he brings a toothpick a toothbrush to the elder

251
00:13:36,600 --> 00:13:38,240
 monk and it's

252
00:13:38,240 --> 00:13:40,690
 kind of dark so the elder doesn't really see but he sees

253
00:13:40,690 --> 00:13:41,840
 the novice come with one

254
00:13:41,840 --> 00:13:50,650
 hand and offer him a toothbrush and apparently another one

255
00:13:50,650 --> 00:13:52,880
 of the customs

256
00:13:52,880 --> 00:13:57,470
 when offering to an elder in order to cultivate respect is

257
00:13:57,470 --> 00:13:58,760
 to offer it with

258
00:13:58,760 --> 00:14:02,920
 two hands so this is protocol between monks and junior

259
00:14:02,920 --> 00:14:03,840
 monks senior monks

260
00:14:03,840 --> 00:14:07,500
 novices and senior monks for lay people they can do as they

261
00:14:07,500 --> 00:14:09,160
 like but there's a

262
00:14:09,160 --> 00:14:12,890
 kind of a protocol in order to keep humility and not get

263
00:14:12,890 --> 00:14:16,280
 arrogant and not so

264
00:14:16,280 --> 00:14:19,470
 we go with someone senior then you offer to them with two

265
00:14:19,470 --> 00:14:21,520
 hands and so the elder

266
00:14:21,520 --> 00:14:26,290
 got kind of miffed at this he said well what kind of a

267
00:14:26,290 --> 00:14:27,320
 novice are you not

268
00:14:27,320 --> 00:14:31,050
 properly trained offering toothbrush to your teacher with

269
00:14:31,050 --> 00:14:31,960
 one hand what kind of

270
00:14:31,960 --> 00:14:34,470
 novice does that he's kind of I did I thought I taught you

271
00:14:34,470 --> 00:14:37,240
 better than that

272
00:14:37,240 --> 00:14:44,550
 novices when rebels are I know full well how to the rules

273
00:14:44,550 --> 00:14:45,200
 that are to be

274
00:14:45,200 --> 00:14:52,310
 performed but my one hand isn't free my one hand isn't

275
00:14:52,310 --> 00:14:56,680
 disengaged what what's

276
00:14:56,680 --> 00:15:02,330
 wrong what's why why can't you use your other hand and then

277
00:15:02,330 --> 00:15:03,640
 the novice has to

278
00:15:03,640 --> 00:15:08,590
 tell them so the novice relates to him from start to finish

279
00:15:08,590 --> 00:15:09,360
 I was sitting up

280
00:15:09,360 --> 00:15:12,740
 because I was sitting up when you threw the fan at me he

281
00:15:12,740 --> 00:15:17,280
 poked my eye out and

282
00:15:17,280 --> 00:15:25,310
 the elder is understandably upset moved shaken by this and

283
00:15:25,310 --> 00:15:27,240
 he bows down he he

284
00:15:27,240 --> 00:15:31,160
 goes down on his knees and says the novice please forgive

285
00:15:31,160 --> 00:15:32,560
 me I'm so sorry

286
00:15:32,560 --> 00:15:38,190
 would please spare me anything be my refuge is what the Pal

287
00:15:38,190 --> 00:15:41,680
i says and he

288
00:15:41,680 --> 00:15:44,140
 crouches down on the ground in front of him and begs him

289
00:15:44,140 --> 00:15:45,920
 for forgiveness and the

290
00:15:45,920 --> 00:15:50,400
 novice what does he do just perfection after perfection

291
00:15:50,400 --> 00:15:52,920
 this is a wonderful way

292
00:15:52,920 --> 00:15:56,510
 of describing the Erland as remember the seven-year-old kid

293
00:15:56,510 --> 00:15:57,480
 isn't is a fully

294
00:15:57,480 --> 00:15:58,060
 enlightened being or an enlightened being not a Buddha but

295
00:15:58,060 --> 00:16:03,800
 an Arant he says

296
00:16:03,800 --> 00:16:07,400
 venerable sir it isn't your fault it was not it was not it

297
00:16:07,400 --> 00:16:08,360
 was not because it was

298
00:16:08,360 --> 00:16:11,900
 not in order to shame you that I spoke I just wanted to let

299
00:16:11,900 --> 00:16:12,840
 you know what

300
00:16:12,840 --> 00:16:17,300
 happened I knew I had to ease your mind but I didn't tell

301
00:16:17,300 --> 00:16:18,520
 you also to ease your

302
00:16:18,520 --> 00:16:21,500
 mind I didn't it's not that I wanted to keep you it from

303
00:16:21,500 --> 00:16:23,120
 you it's that I knew

304
00:16:23,120 --> 00:16:26,610
 you might get upset and so in order to spare you that I

305
00:16:26,610 --> 00:16:28,440
 didn't say anything it's

306
00:16:28,440 --> 00:16:32,680
 not your fault you're not to blame this is the fault of

307
00:16:32,680 --> 00:16:34,640
 some Sahara he says this

308
00:16:34,640 --> 00:16:37,840
 is the fault of the rounds of rebirth you know if you're

309
00:16:37,840 --> 00:16:39,160
 gonna be reborn these

310
00:16:39,160 --> 00:16:42,520
 things are going to happen and he leaves it at that this

311
00:16:42,520 --> 00:16:44,080
 seven-year-old kid can

312
00:16:44,080 --> 00:16:50,860
 you imagine and so the elder is even even further moved by

313
00:16:50,860 --> 00:16:52,880
 that and the

314
00:16:52,880 --> 00:16:55,490
 novice tries to comfort him but he won't be covered he won

315
00:16:55,490 --> 00:16:57,680
't be comforted so he

316
00:16:57,680 --> 00:17:00,880
 takes the novice to the Buddha he picks up the novice's

317
00:17:00,880 --> 00:17:02,720
 bowl and requisites and

318
00:17:02,720 --> 00:17:06,160
 carries them for the novice and brings him to the Buddha

319
00:17:06,160 --> 00:17:07,120
 shaking his head and

320
00:17:07,120 --> 00:17:11,320
 and weeping the whole way and when they get to the Buddha

321
00:17:11,320 --> 00:17:12,440
 that he goes straight

322
00:17:12,440 --> 00:17:16,620
 straight to the Buddha hands and bows down and prepares to

323
00:17:16,620 --> 00:17:17,600
 tell the Buddha and

324
00:17:17,600 --> 00:17:24,190
 the Buddha says oh Biku is everything okay with you what

325
00:17:24,190 --> 00:17:25,320
 kind of how do you

326
00:17:25,320 --> 00:17:29,910
 answer that says I trust you have not suffered any excess

327
00:17:29,910 --> 00:17:32,960
 discomfort and the

328
00:17:32,960 --> 00:17:38,130
 monk says bande I I indeed have lived it quite at quite an

329
00:17:38,130 --> 00:17:41,920
 ease myself but here I

330
00:17:41,920 --> 00:17:47,500
 have this novice with me take a look at this this novice is

331
00:17:47,500 --> 00:17:48,960
 he is beyond anything

332
00:17:48,960 --> 00:17:52,920
 that I've ever seen and the Buddha said what why what has

333
00:17:52,920 --> 00:17:55,000
 he done and the elder

334
00:17:55,000 --> 00:17:58,240
 told the whole story to the Buddha and he said I don't

335
00:17:58,240 --> 00:17:59,920
 understand when but one

336
00:17:59,920 --> 00:18:02,370
 day when when I asked him to pardon when I asked him to

337
00:18:02,370 --> 00:18:03,800
 forgive me he said it's

338
00:18:03,800 --> 00:18:06,850
 not your fault it's the fault of some sarah don't be

339
00:18:06,850 --> 00:18:08,360
 disturbed don't be upset

340
00:18:08,360 --> 00:18:13,770
 he tried to comfort me when here he is with his eye poked

341
00:18:13,770 --> 00:18:15,080
 out holding it with

342
00:18:15,080 --> 00:18:22,100
 one hand offering me a toothbrush and the Buddha says oh

343
00:18:22,100 --> 00:18:23,320
 those who have freed

344
00:18:23,320 --> 00:18:27,220
 themselves from defilements have neither anger nor hatred

345
00:18:27,220 --> 00:18:29,320
 towards anyone on the

346
00:18:29,320 --> 00:18:33,450
 contrary they're in a state of calm their senses their mind

347
00:18:33,450 --> 00:18:34,800
 their speech and

348
00:18:34,800 --> 00:18:42,950
 their body and then he taught this verse his thoughts are

349
00:18:42,950 --> 00:18:45,600
 calm his speech is calm

350
00:18:45,600 --> 00:18:53,510
 santang dasa mananghu the mind of such a person is calm

351
00:18:53,510 --> 00:18:55,680
 speech is calm his deeds

352
00:18:55,680 --> 00:19:00,640
 are calm who has who what sort of person is this one who

353
00:19:00,640 --> 00:19:01,600
 has freed themselves

354
00:19:01,600 --> 00:19:07,840
 with right knowledge the person does that you know someone

355
00:19:07,840 --> 00:19:08,800
 who is tranquil or

356
00:19:08,800 --> 00:19:18,960
 peaceful so what can we learn from this it you know it it

357
00:19:18,960 --> 00:19:22,600
 it sets the sets the

358
00:19:22,600 --> 00:19:29,340
 bar right it holds no doesn't hold any punches this is

359
00:19:29,340 --> 00:19:31,280
 telling us how far we

360
00:19:31,280 --> 00:19:39,480
 plan we intend to go how far we can go how how perfect we

361
00:19:39,480 --> 00:19:41,680
 expect to become

362
00:19:41,680 --> 00:19:45,730
 through through the practice how far we believe it possible

363
00:19:45,730 --> 00:19:46,720
 to go maybe not in

364
00:19:46,720 --> 00:19:52,390
 this life but eventually through the practice our

365
00:19:52,390 --> 00:19:55,320
 understanding is this isn't

366
00:19:55,320 --> 00:20:00,220
 some simple thing where oh yes you've you know you don't

367
00:20:00,220 --> 00:20:01,800
 complain about a

368
00:20:01,800 --> 00:20:06,110
 toothache anymore when you get it have a stomach ache you

369
00:20:06,110 --> 00:20:07,900
're okay or you're okay

370
00:20:07,900 --> 00:20:11,720
 skipping lunch now because you've let go

371
00:20:11,720 --> 00:20:19,080
 it's not like that

372
00:20:19,840 --> 00:20:24,630
 where we're going is to the to the end of suffering where

373
00:20:24,630 --> 00:20:25,840
 one really doesn't

374
00:20:25,840 --> 00:20:29,590
 react really doesn't get upset where one is really and

375
00:20:29,590 --> 00:20:31,120
 truly able to experience

376
00:20:31,120 --> 00:20:36,840
 things as they are so the mind and the speech and the body

377
00:20:36,840 --> 00:20:37,560
 don't become

378
00:20:37,560 --> 00:20:40,960
 tranquil because you don't encounter bad things it's not

379
00:20:40,960 --> 00:20:42,280
 about building up such

380
00:20:42,280 --> 00:20:46,290
 good karma that you never experience bad things it's not

381
00:20:46,290 --> 00:20:48,000
 that kind of invincible

382
00:20:48,000 --> 00:20:54,680
 it's the invincibility of the mind where you have seen

383
00:20:54,680 --> 00:20:57,040
 clearly and the wonderful

384
00:20:57,040 --> 00:20:59,620
 thing is that it just it comes simply from seeing clearly

385
00:20:59,620 --> 00:21:00,600
 which is that the

386
00:21:00,600 --> 00:21:06,280
 point of this verse that true true peace the kind that can

387
00:21:06,280 --> 00:21:08,240
 see you through losing

388
00:21:08,240 --> 00:21:14,400
 an eye or a limb or even your life that kind of peace and

389
00:21:14,400 --> 00:21:17,160
 tranquility is only to

390
00:21:17,160 --> 00:21:21,850
 be found through wisdom and is to be found simply through

391
00:21:21,850 --> 00:21:23,080
 wisdom with no

392
00:21:23,080 --> 00:21:30,200
 other requirement whatsoever because when you see things as

393
00:21:30,200 --> 00:21:31,600
 they are there's

394
00:21:31,600 --> 00:21:35,520
 no reason to get upset there's no benefit to getting upset

395
00:21:35,520 --> 00:21:36,640
 there's no

396
00:21:36,640 --> 00:21:40,300
 benefit to be had from this novice yelling and screaming

397
00:21:40,300 --> 00:21:41,320
 and wailing and

398
00:21:41,320 --> 00:21:44,310
 crying it's reasonable if it would be reasonable if he did

399
00:21:44,310 --> 00:21:45,120
 you think well he's

400
00:21:45,120 --> 00:21:49,460
 only human but somehow this is the idea is that enlightened

401
00:21:49,460 --> 00:21:51,120
 being becomes it

402
00:21:51,120 --> 00:21:56,120
 goes beyond the ordinary state of a human

403
00:21:56,120 --> 00:22:01,610
 able to attain a state of greatness stay in a state of

404
00:22:01,610 --> 00:22:04,000
 profound peace and

405
00:22:04,000 --> 00:22:07,120
 tranquility

406
00:22:07,120 --> 00:22:13,550
 so this is again something for us to emulate but I think

407
00:22:13,550 --> 00:22:14,160
 more here is

408
00:22:14,160 --> 00:22:18,790
 something for us to compare how would you do fair if you

409
00:22:18,790 --> 00:22:19,920
 lost an eye in that

410
00:22:19,920 --> 00:22:23,630
 way sometimes it's good to think about these things it's

411
00:22:23,630 --> 00:22:24,480
 good to imagine the

412
00:22:24,480 --> 00:22:28,950
 situation it gives you a chance to see your own defilements

413
00:22:28,950 --> 00:22:30,160
 to get a taste of

414
00:22:30,160 --> 00:22:33,790
 what you have left to accomplish what is left inside of you

415
00:22:33,790 --> 00:22:34,800
 that will cause you

416
00:22:34,800 --> 00:22:37,390
 suffering because losing an eye doesn't cause you suffering

417
00:22:37,390 --> 00:22:39,240
 it's only when you

418
00:22:39,240 --> 00:22:44,560
 react negatively to it oh that's not what I wanted when you

419
00:22:44,560 --> 00:22:45,480
 had expectations

420
00:22:45,480 --> 00:22:50,530
 for that I wanted to use it for things but this novice had

421
00:22:50,530 --> 00:22:51,280
 no use for the eye

422
00:22:51,280 --> 00:22:58,280
 losing it was like losing a piece of trash

423
00:22:58,280 --> 00:23:04,470
 meaningless of no consequence because his peace was his

424
00:23:04,470 --> 00:23:05,680
 happiness was

425
00:23:05,680 --> 00:23:10,150
 independent of the eye independent of the physical body

426
00:23:10,150 --> 00:23:10,800
 independent of

427
00:23:10,800 --> 00:23:15,630
 anything in the world that's where our practice leads us it

428
00:23:15,630 --> 00:23:16,560
's not like we want

429
00:23:16,560 --> 00:23:19,840
 to lose an eye or it's you know I mean how could you come

430
00:23:19,840 --> 00:23:20,160
 how could you

431
00:23:20,160 --> 00:23:24,410
 criticize this wouldn't it you know wouldn't it be great if

432
00:23:24,410 --> 00:23:25,560
 when the most

433
00:23:25,560 --> 00:23:29,280
 horror of the horrific thing happened to us we can be happy

434
00:23:29,280 --> 00:23:31,000
 as horrific things do

435
00:23:31,000 --> 00:23:35,450
 happen to people and they're horrified by them they're they

436
00:23:35,450 --> 00:23:36,920
 can go crazy because

437
00:23:36,920 --> 00:23:39,430
 of them wouldn't it be great if that weren't the case

438
00:23:39,430 --> 00:23:40,640
 wouldn't it be great if

439
00:23:40,640 --> 00:23:44,170
 we could deal with anything that we wouldn't have to live

440
00:23:44,170 --> 00:23:45,280
 our lives in fear

441
00:23:45,280 --> 00:23:49,740
 protecting our bodies constantly obsessing over them

442
00:23:49,740 --> 00:23:51,340
 worried about them

443
00:23:51,340 --> 00:23:56,510
 worried about wrinkles worried about fat worried about this

444
00:23:56,510 --> 00:23:58,600
 and that well here we

445
00:23:58,600 --> 00:24:04,400
 see how far we are and how far we have to go and we can see

446
00:24:04,400 --> 00:24:06,200
 the need for work

447
00:24:06,200 --> 00:24:10,130
 that we have work to do because we really have a long way

448
00:24:10,130 --> 00:24:11,720
 to go it's not

449
00:24:11,720 --> 00:24:15,320
 enough to say oh look at me I don't I had back pain and now

450
00:24:15,320 --> 00:24:15,960
 I can deal with

451
00:24:15,960 --> 00:24:21,040
 the back pain can you deal with losing an eye and you know

452
00:24:21,040 --> 00:24:21,920
 how far away you are

453
00:24:21,920 --> 00:24:27,150
 from the goal so anyway that's the Dhamma Pada for tonight

454
00:24:27,150 --> 00:24:28,080
 thank you all for

455
00:24:28,080 --> 00:24:34,120
 tuning in keep practicing and wishing

