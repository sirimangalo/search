1
00:00:00,000 --> 00:00:23,880
 Good evening everyone, welcome to evening dhamma.

2
00:00:23,880 --> 00:00:33,620
 Tonight we're looking at the sabassa vasutta, vajiminikaya

3
00:00:33,620 --> 00:00:36,880
 number two.

4
00:00:36,880 --> 00:00:45,760
 We're looking at vinodana bahatabha.

5
00:00:45,760 --> 00:00:52,820
 And we're dealing with many different aspects of erad

6
00:00:52,820 --> 00:00:55,680
icating defilements.

7
00:00:55,680 --> 00:01:00,180
 And again by defilements we mean it in a very technical

8
00:01:00,180 --> 00:01:02,440
 sense, a defilement that makes something

9
00:01:02,440 --> 00:01:06,660
 impure.

10
00:01:06,660 --> 00:01:12,510
 And in the case of the mind it's that which leads to stress

11
00:01:12,510 --> 00:01:14,440
 and suffering.

12
00:01:14,440 --> 00:01:21,810
 Those aspects of the mind that are of no use to us and are

13
00:01:21,810 --> 00:01:25,640
 in fact to our detriment.

14
00:01:25,640 --> 00:01:29,320
 They sully the mind, they taint the mind.

15
00:01:29,320 --> 00:01:37,160
 They're what cause us to get caught up in addiction and

16
00:01:37,160 --> 00:01:43,000
 diversion and stress and suffering.

17
00:01:43,000 --> 00:01:51,100
 And so if you recall the most important practice for doing

18
00:01:51,100 --> 00:01:53,080
 away with defilements of course

19
00:01:53,080 --> 00:01:54,080
 is seeing.

20
00:01:54,080 --> 00:01:59,880
 It's the first one of the seven.

21
00:01:59,880 --> 00:02:04,330
 And so we talk about how mindfulness is all about learning

22
00:02:04,330 --> 00:02:05,800
 to see clearly.

23
00:02:05,800 --> 00:02:09,160
 Why are we mindful?

24
00:02:09,160 --> 00:02:10,960
 Really to see clearly.

25
00:02:10,960 --> 00:02:11,960
 To see things as they are.

26
00:02:11,960 --> 00:02:15,120
 And this is what we call vipassana.

27
00:02:15,120 --> 00:02:17,940
 When you're mindful you start to learn and understand

28
00:02:17,940 --> 00:02:18,680
 yourself.

29
00:02:18,680 --> 00:02:23,330
 But there's actually two aspects to the practice that

30
00:02:23,330 --> 00:02:26,600
 really go hand in hand and are equally

31
00:02:26,600 --> 00:02:29,760
 important.

32
00:02:29,760 --> 00:02:33,060
 And it's one way of looking at what we call vinodana, the

33
00:02:33,060 --> 00:02:35,120
 section we're looking at today.

34
00:02:35,120 --> 00:02:40,380
 Vinodana means to dispel.

35
00:02:40,380 --> 00:02:45,520
 And so the question is why don't we see clearly anyway?

36
00:02:45,520 --> 00:02:48,080
 Why don't we see clearly normally?

37
00:02:48,080 --> 00:02:54,760
 Why do we need to do this special practice to see clearly?

38
00:02:54,760 --> 00:03:01,000
 It's because we have darkness, we have clouds, we have def

39
00:03:01,000 --> 00:03:04,480
ilements covering our minds.

40
00:03:04,480 --> 00:03:07,720
 So our final goal is to be free from defilements because

41
00:03:07,720 --> 00:03:09,960
 they're what cause us suffering.

42
00:03:09,960 --> 00:03:13,390
 But in order to be free from defilements, funny thing is we

43
00:03:13,390 --> 00:03:14,840
 actually have to be free

44
00:03:14,840 --> 00:03:17,440
 from defilements.

45
00:03:17,440 --> 00:03:24,520
 It's useful to understand this, what this means.

46
00:03:24,520 --> 00:03:28,230
 When we talk about being free from defilements in the sense

47
00:03:28,230 --> 00:03:30,360
 of our goal, we're not actually

48
00:03:30,360 --> 00:03:34,390
 talking about right here and now being free from defile

49
00:03:34,390 --> 00:03:35,200
ments.

50
00:03:35,200 --> 00:03:38,240
 I mean that comes anyway, right?

51
00:03:38,240 --> 00:03:41,120
 From time to time we're not angry, we're not greedy.

52
00:03:41,120 --> 00:03:44,640
 There are times where our minds are quite pure.

53
00:03:44,640 --> 00:03:51,060
 Now what we're talking about is in fact a uprooting of the

54
00:03:51,060 --> 00:03:54,720
 causes of defilement, mainly

55
00:03:54,720 --> 00:03:56,680
 ignorance.

56
00:03:56,680 --> 00:04:03,160
 Once you see clearly, then the defilements don't come back.

57
00:04:03,160 --> 00:04:07,540
 For example, if there's something that you dislike, well by

58
00:04:07,540 --> 00:04:08,800
 understanding that thing

59
00:04:08,800 --> 00:04:12,250
 that you dislike and understanding the disliking, you come

60
00:04:12,250 --> 00:04:14,480
 to see that the thing that you dislike

61
00:04:14,480 --> 00:04:19,660
 wasn't worth disliking and the dislike had no benefit or

62
00:04:19,660 --> 00:04:22,560
 purpose and was in fact harmful

63
00:04:22,560 --> 00:04:23,560
 to you.

64
00:04:23,560 --> 00:04:26,600
 When you see that, then you're disinclined with it.

65
00:04:26,600 --> 00:04:30,280
 But in order to see that, you have to be free, your mind

66
00:04:30,280 --> 00:04:32,640
 has to be free from defilement.

67
00:04:32,640 --> 00:04:35,320
 And so really that's a lot about what this sutta is about.

68
00:04:35,320 --> 00:04:42,860
 It's about the different ways in which we support our minds

69
00:04:42,860 --> 00:04:46,920
 to see clearly and we cultivate

70
00:04:46,920 --> 00:04:57,430
 and we protect our minds and encourage our minds to see

71
00:04:57,430 --> 00:05:00,760
 more clearly.

72
00:05:00,760 --> 00:05:04,450
 And so a big part of mindfulness is not just about seeing

73
00:05:04,450 --> 00:05:06,520
 clearly, but is in fact about

74
00:05:06,520 --> 00:05:08,560
 purifying the mind momentarily.

75
00:05:08,560 --> 00:05:12,200
 So we call it momentary concentration.

76
00:05:12,200 --> 00:05:15,220
 So there's a lot of talk in Buddhism about entering into

77
00:05:15,220 --> 00:05:16,080
 the jhanas.

78
00:05:16,080 --> 00:05:20,700
 I mean, the Buddha talked a lot about these and he

79
00:05:20,700 --> 00:05:23,840
 explained it as momentary or temporary

80
00:05:23,840 --> 00:05:25,400
 cessation of defilement.

81
00:05:25,400 --> 00:05:26,960
 It's a pure state.

82
00:05:26,960 --> 00:05:34,900
 A jhana is a state that is free from any kind of desire or

83
00:05:34,900 --> 00:05:36,960
 aversion.

84
00:05:36,960 --> 00:05:40,780
 And so that's what we're doing when we're being mindful, we

85
00:05:40,780 --> 00:05:43,120
're cultivating this momentarily.

86
00:05:43,120 --> 00:05:45,640
 And this is what allows us to see clearly.

87
00:05:45,640 --> 00:05:51,510
 Mindfulness is in fact a form of abandoning unwholesome

88
00:05:51,510 --> 00:05:53,200
 mind states.

89
00:05:53,200 --> 00:05:56,700
 So for example, when you see something and normally that

90
00:05:56,700 --> 00:05:58,600
 seeing would, you see a plate

91
00:05:58,600 --> 00:06:03,990
 of food and normally that would lead to a craving for the

92
00:06:03,990 --> 00:06:07,080
 food or a desire for the food.

93
00:06:07,080 --> 00:06:13,320
 When you say to yourself, seeing, seeing, you abandon that.

94
00:06:13,320 --> 00:06:15,520
 You dispel that.

95
00:06:15,520 --> 00:06:19,320
 When you want something and you say to yourself, wanting,

96
00:06:19,320 --> 00:06:21,760
 wanting, you dispel the wanting.

97
00:06:21,760 --> 00:06:24,320
 That's a form of cleansing the mind.

98
00:06:24,320 --> 00:06:28,310
 But I think what this section is talking about is something

99
00:06:28,310 --> 00:06:30,440
 a little different that is also

100
00:06:30,440 --> 00:06:31,440
 quite useful.

101
00:06:31,440 --> 00:06:34,880
 And remember, these later sections are, except for the last

102
00:06:34,880 --> 00:06:37,440
 one, the one we'll do next time,

103
00:06:37,440 --> 00:06:42,900
 are mainly about auxiliary practices, ancillary practices,

104
00:06:42,900 --> 00:06:45,920
 practices that are supportive.

105
00:06:45,920 --> 00:06:50,260
 And so with, we know, and I hear we're talking more about,

106
00:06:50,260 --> 00:06:52,880
 and the commentary reaffirms this,

107
00:06:52,880 --> 00:06:59,630
 we're talking about intellectually or reflect, reflexively

108
00:06:59,630 --> 00:07:03,160
 reflecting on your experiences

109
00:07:03,160 --> 00:07:10,200
 and making a determined effort to abandon.

110
00:07:10,200 --> 00:07:12,800
 When you're meditating, it's easy to say, be mindful, but

111
00:07:12,800 --> 00:07:14,080
 there's a lot of things that

112
00:07:14,080 --> 00:07:19,720
 are quite strong and just tear you away from the practice.

113
00:07:19,720 --> 00:07:22,440
 Maybe you get caught up in the past or the future.

114
00:07:22,440 --> 00:07:25,240
 It's very common, right?

115
00:07:25,240 --> 00:07:30,520
 You start obsessing over something you did or something

116
00:07:30,520 --> 00:07:33,880
 that happened to you in the past.

117
00:07:33,880 --> 00:07:37,110
 Grieving about the past or hating yourself, angry at

118
00:07:37,110 --> 00:07:39,360
 yourself because of the past, that

119
00:07:39,360 --> 00:07:42,760
 kind of thing, feeling guilty about the past.

120
00:07:42,760 --> 00:07:46,120
 Or worried about the future, worry, fear, ambition about

121
00:07:46,120 --> 00:07:47,000
 the future.

122
00:07:47,000 --> 00:07:51,720
 You can make plans, a 10 year plan while you're sitting in

123
00:07:51,720 --> 00:07:53,000
 meditation.

124
00:07:53,000 --> 00:07:57,960
 Or maybe even just daydreams, thinking up stories, telling

125
00:07:57,960 --> 00:08:00,520
 yourself stories or fantasies

126
00:08:00,520 --> 00:08:03,720
 in your mind.

127
00:08:03,720 --> 00:08:10,880
 And these things are pernicious or are stubborn.

128
00:08:10,880 --> 00:08:13,240
 And so we know Dana is this sort of thing.

129
00:08:13,240 --> 00:08:17,030
 It's of dealing with this and saying to yourself, "Look,

130
00:08:17,030 --> 00:08:18,400
 this is useless.

131
00:08:18,400 --> 00:08:19,800
 This is not to my benefit.

132
00:08:19,800 --> 00:08:23,470
 These things are a cause for stress and they're distracting

133
00:08:23,470 --> 00:08:25,000
 me from my practice."

134
00:08:25,000 --> 00:08:31,440
 There are three kinds of thoughts that we have to do this

135
00:08:31,440 --> 00:08:32,440
 with.

136
00:08:32,440 --> 00:08:37,390
 So the Pali says, "Ida bhikkhu pati sankha yoni," so

137
00:08:37,390 --> 00:08:41,080
 reflecting wisely, "upanankamavita

138
00:08:41,080 --> 00:08:49,760
 kang," and a risen thought relating to sensuality.

139
00:08:49,760 --> 00:08:56,260
 "Nadhi wa seti bhajahati we no deti, bhayanti karo ti anamb

140
00:08:56,260 --> 00:08:58,440
havang gameti."

141
00:08:58,440 --> 00:09:02,400
 It's nice to read all that.

142
00:09:02,400 --> 00:09:05,120
 It's actually, the commentary does a really good job

143
00:09:05,120 --> 00:09:07,320
 explaining all these synonyms really.

144
00:09:07,320 --> 00:09:10,640
 They're all saying the same thing over and over again.

145
00:09:10,640 --> 00:09:12,240
 "Nadhi wa seti."

146
00:09:12,240 --> 00:09:17,040
 "Adhi wa seti" is referring to an earlier section on being

147
00:09:17,040 --> 00:09:19,800
 patient with, bearing with.

148
00:09:19,800 --> 00:09:22,760
 So these things one doesn't bear with.

149
00:09:22,760 --> 00:09:27,920
 One doesn't endure these kind of thoughts.

150
00:09:27,920 --> 00:09:32,850
 If one is caught up in sensuality, thinking about sights

151
00:09:32,850 --> 00:09:35,560
 and sounds and smells and tastes

152
00:09:35,560 --> 00:09:40,490
 and feelings and thoughts, sensual experiences that are

153
00:09:40,490 --> 00:09:44,760
 pleasant, one gets caught up in those.

154
00:09:44,760 --> 00:09:45,760
 One banishes them.

155
00:09:45,760 --> 00:09:49,600
 One doesn't endure that thought and let it continue.

156
00:09:49,600 --> 00:09:53,160
 He doesn't get caught up in the thought.

157
00:09:53,160 --> 00:09:56,920
 "Bhajahati," one abandons it.

158
00:09:56,920 --> 00:09:57,920
 Right?

159
00:09:57,920 --> 00:09:58,920
 Yeah, abandons.

160
00:09:58,920 --> 00:10:02,920
 "We no deti," dispels it.

161
00:10:02,920 --> 00:10:06,920
 "Bhayanti karo ti," makes it go away.

162
00:10:06,920 --> 00:10:19,240
 "Anabhavang gameti," causes it to go to oblivion.

163
00:10:19,240 --> 00:10:21,040
 Basically saying the same thing.

164
00:10:21,040 --> 00:10:28,230
 There's different ways. It's basically talking about

165
00:10:28,230 --> 00:10:29,880
 reassuring yourself or giving yourself

166
00:10:29,880 --> 00:10:36,800
 a pep talk and say, "Look, these thoughts don't go there."

167
00:10:36,800 --> 00:10:38,400
 It's making a decision really.

168
00:10:38,400 --> 00:10:44,280
 "Hey, I've come here to meditate," or "What am I doing as a

169
00:10:44,280 --> 00:10:46,080
 human being?"

170
00:10:46,080 --> 00:10:48,280
 "I've been born as a human being.

171
00:10:48,280 --> 00:10:51,970
 I should take this opportunity that I've come in contact

172
00:10:51,970 --> 00:10:53,720
 with the Buddhist teaching

173
00:10:53,720 --> 00:11:05,720
 and I have my health to keep my mind focused on reality."

174
00:11:05,720 --> 00:11:11,130
 So with sensuality, and the second one is with ill will or

175
00:11:11,130 --> 00:11:14,040
 anger, any thoughts of anger

176
00:11:14,040 --> 00:11:23,030
 when we are upset at someone or thinking about enmity and

177
00:11:23,030 --> 00:11:28,520
 revenge and that kind of thing.

178
00:11:28,520 --> 00:11:33,280
 And the third one is wehings, wanting to harm others.

179
00:11:33,280 --> 00:11:36,360
 I don't really know.

180
00:11:36,360 --> 00:11:41,470
 I always get confused about the difference between those

181
00:11:41,470 --> 00:11:42,160
 two.

182
00:11:42,160 --> 00:11:46,480
 Basically anything to do with greed, anger and delusion.

183
00:11:46,480 --> 00:11:51,440
 Any kind of bad intention in the mind.

184
00:11:51,440 --> 00:11:53,120
 And that's what he says at the end.

185
00:11:53,120 --> 00:12:14,880
 "I have arisen evil, unwholesome dhammas."

186
00:12:14,880 --> 00:12:15,880
 Evil unwholesome dhammas.

187
00:12:15,880 --> 00:12:19,380
 I mean, they don't sound all that bad, the things I've been

188
00:12:19,380 --> 00:12:20,560
 talking about.

189
00:12:20,560 --> 00:12:24,010
 For most people, I often get comments, people say, "I don't

190
00:12:24,010 --> 00:12:25,160
 understand what's so wrong

191
00:12:25,160 --> 00:12:33,390
 with this entertainment and this thinking and remembering

192
00:12:33,390 --> 00:12:36,880
 and planning and so on.

193
00:12:36,880 --> 00:12:38,880
 What's so wrong with it?"

194
00:12:38,880 --> 00:12:45,040
 We have to remember what we're talking about here.

195
00:12:45,040 --> 00:12:48,640
 This practice may not appeal to everyone.

196
00:12:48,640 --> 00:12:54,240
 I mean, it certainly doesn't appeal to everyone.

197
00:12:54,240 --> 00:12:58,710
 It does make a claim to be the best course of action for

198
00:12:58,710 --> 00:12:59,920
 everyone.

199
00:12:59,920 --> 00:13:05,790
 But certainly many people will not see that and will not be

200
00:13:05,790 --> 00:13:09,120
 interested in it in that way.

201
00:13:09,120 --> 00:13:13,080
 But what we're talking about here is the purification of

202
00:13:13,080 --> 00:13:14,000
 the mind.

203
00:13:14,000 --> 00:13:19,850
 We are talking about uprooting in the sense of it never

204
00:13:19,850 --> 00:13:22,280
 coming back again.

205
00:13:22,280 --> 00:13:28,670
 Things like addiction, aversion, any kind of anger,

206
00:13:28,670 --> 00:13:30,640
 frustration.

207
00:13:30,640 --> 00:13:31,640
 They never come back.

208
00:13:31,640 --> 00:13:34,880
 That's what we're aiming for.

209
00:13:34,880 --> 00:13:43,480
 Any kind of lust, passion, ambition, suffering.

210
00:13:43,480 --> 00:13:44,480
 Any kind of suffering.

211
00:13:44,480 --> 00:13:47,680
 There will be no more suffering in the mind.

212
00:13:47,680 --> 00:13:54,280
 It's really something quite lofty, profound.

213
00:13:54,280 --> 00:13:55,880
 Profound lofty anyway.

214
00:13:55,880 --> 00:14:00,800
 It's a lofty goal and it might not be to everyone's taste.

215
00:14:00,800 --> 00:14:03,200
 I think it's important to align those two.

216
00:14:03,200 --> 00:14:10,720
 If you don't see the problem with fantasizing or getting

217
00:14:10,720 --> 00:14:14,800
 caught up in thoughts of anger

218
00:14:14,800 --> 00:14:21,580
 or revenge or so on, then maybe it's because you're not

219
00:14:21,580 --> 00:14:24,800
 really interested in the goal of

220
00:14:24,800 --> 00:14:29,720
 Buddhism.

221
00:14:29,720 --> 00:14:32,130
 What we're talking about is not becoming a normal human

222
00:14:32,130 --> 00:14:32,620
 being.

223
00:14:32,620 --> 00:14:35,940
 This isn't just something that's meant to take you out of

224
00:14:35,940 --> 00:14:37,760
 extreme states of suffering

225
00:14:37,760 --> 00:14:41,360
 and allow you to live your life normally.

226
00:14:41,360 --> 00:14:44,200
 This is about becoming a noble individual.

227
00:14:44,200 --> 00:14:49,080
 It's really taking you out of ordinary humanity.

228
00:14:49,080 --> 00:14:53,440
 The ups and the downs of life.

229
00:14:53,440 --> 00:15:00,800
 It's rising above the roller coasters of pleasure and pain.

230
00:15:00,800 --> 00:15:09,600
 Not pleasure and pain, but being pleased and displeased.

231
00:15:09,600 --> 00:15:10,920
 Being happy and unhappy.

232
00:15:10,920 --> 00:15:14,640
 There will be no more happy and unhappy.

233
00:15:14,640 --> 00:15:17,680
 Just be happy, really.

234
00:15:17,680 --> 00:15:21,610
 I mean, not in the sense that we know it, but there will be

235
00:15:21,610 --> 00:15:23,360
 always happiness in the

236
00:15:23,360 --> 00:15:31,290
 sense of freedom from suffering and the sense of peace in

237
00:15:31,290 --> 00:15:33,040
 the mind.

238
00:15:33,040 --> 00:15:42,040
 We're aiming for this peace and this most sublime state.

239
00:15:42,040 --> 00:15:45,510
 For this section, what we're dealing with here is this

240
00:15:45,510 --> 00:15:46,760
 making a decision.

241
00:15:46,760 --> 00:15:53,920
 What am I here for?

242
00:15:53,920 --> 00:15:57,880
 A decision that these states are a waste of time.

243
00:15:57,880 --> 00:16:01,210
 Pulling yourself back to the present moment and then of

244
00:16:01,210 --> 00:16:03,680
 course dealing with them mindfully.

245
00:16:03,680 --> 00:16:09,520
 A thought is just a thought.

246
00:16:09,520 --> 00:16:17,550
 Of course, in a deep sense, there's no need for the

247
00:16:17,550 --> 00:16:22,840
 intellectualization because you see

248
00:16:22,840 --> 00:16:25,750
 that a thought is just a thought, an experience is just an

249
00:16:25,750 --> 00:16:26,600
 experience.

250
00:16:26,600 --> 00:16:36,860
 You see that there is no inherent attractiveness or repuls

251
00:16:36,860 --> 00:16:42,200
iveness to any experience.

252
00:16:42,200 --> 00:16:44,880
 That every experience is neutral.

253
00:16:44,880 --> 00:16:47,640
 It is what it is.

254
00:16:47,640 --> 00:16:57,120
 We've simply developed excitement, different kinds of

255
00:16:57,120 --> 00:17:00,840
 excitement that causes us to hurt

256
00:17:00,840 --> 00:17:07,180
 ourselves and experiences don't actually cause us stress or

257
00:17:07,180 --> 00:17:08,720
 suffering.

258
00:17:08,720 --> 00:17:11,000
 It's our reactions to them.

259
00:17:11,000 --> 00:17:16,960
 It's our states of excitement and allowing ourselves to

260
00:17:16,960 --> 00:17:19,960
 make more out of things than

261
00:17:19,960 --> 00:17:22,960
 they actually are.

262
00:17:22,960 --> 00:17:28,040
 All of this kind of teaching and reminding yourself of

263
00:17:28,040 --> 00:17:31,320
 these sorts of concepts is really

264
00:17:31,320 --> 00:17:34,080
 what the Buddha is talking about, the sort of thing he's

265
00:17:34,080 --> 00:17:35,280
 talking about here.

266
00:17:35,280 --> 00:17:41,030
 He's talking about telling yourself and teaching yourself

267
00:17:41,030 --> 00:17:44,320
 and pointing out, "This is not useful."

268
00:17:44,320 --> 00:17:49,500
 Being able to decide this path that I'm going down in my

269
00:17:49,500 --> 00:17:52,360
 mind, this way that I'm focusing

270
00:17:52,360 --> 00:17:59,250
 my mind, this way of acting, way of behaving mentally is

271
00:17:59,250 --> 00:18:00,480
 wrong.

272
00:18:00,480 --> 00:18:01,480
 Wrong for me.

273
00:18:01,480 --> 00:18:03,080
 It doesn't bring me happiness.

274
00:18:03,080 --> 00:18:04,600
 It doesn't bring me peace.

275
00:18:04,600 --> 00:18:09,530
 It doesn't make me a better person for myself or others in

276
00:18:09,530 --> 00:18:10,560
 any way.

277
00:18:10,560 --> 00:18:16,720
 You abandon it and switch to being mindful instead.

278
00:18:16,720 --> 00:18:21,560
 Instead of focusing on the content of thought and getting

279
00:18:21,560 --> 00:18:24,000
 caught up in it in any way, a

280
00:18:24,000 --> 00:18:25,000
 thought is just a thought.

281
00:18:25,000 --> 00:18:30,250
 It's a very important Buddhist theory, Buddhist concept if

282
00:18:30,250 --> 00:18:33,040
 you want to understand what the

283
00:18:33,040 --> 00:18:37,390
 Buddha taught or how the Buddha taught or the Buddha's exh

284
00:18:37,390 --> 00:18:38,640
ortation was.

285
00:18:38,640 --> 00:18:42,690
 That seeing should just be seeing, hearing should just be

286
00:18:42,690 --> 00:18:43,600
 hearing.

287
00:18:43,600 --> 00:18:47,120
 Sensing should be sensing, thinking should be thinking.

288
00:18:47,120 --> 00:18:48,880
 No more, no less.

289
00:18:48,880 --> 00:18:54,650
 If we look at reality in this way, we'll gain some novel

290
00:18:54,650 --> 00:18:57,960
 realizations that allow us to free

291
00:18:57,960 --> 00:19:10,820
 our minds, really truly free our minds and feel the freedom

292
00:19:10,820 --> 00:19:13,720
 of purity.

293
00:19:13,720 --> 00:19:17,700
 So one more section and next time we'll have the last

294
00:19:17,700 --> 00:19:20,400
 section and maybe we'll go back to

295
00:19:20,400 --> 00:19:22,520
 the Dhammapada.

296
00:19:22,520 --> 00:19:26,800
 That's all for tonight.

297
00:19:26,800 --> 00:19:33,000
 We'll take some questions.

298
00:19:33,000 --> 00:19:35,610
 How can someone know if she is escaping her life or if she

299
00:19:35,610 --> 00:19:37,160
 is really following her inner

300
00:19:37,160 --> 00:19:41,400
 call to become a nun?

301
00:19:41,400 --> 00:19:51,110
 Okay, without reading the long paragraph that you wrote, it

302
00:19:51,110 --> 00:19:55,280
's interesting to read.

303
00:19:55,280 --> 00:20:02,440
 I think if you're in tension, I mean the best, there are

304
00:20:02,440 --> 00:20:06,280
 many reasons to ordain and people

305
00:20:06,280 --> 00:20:09,970
 ordain for all sorts of reasons and it makes for a very

306
00:20:09,970 --> 00:20:11,760
 colorful community.

307
00:20:11,760 --> 00:20:15,630
 But if you really want to ordain for the right reasons, it

308
00:20:15,630 --> 00:20:17,720
 should be because you want to

309
00:20:17,720 --> 00:20:22,120
 practice meditation, that and only that.

310
00:20:22,120 --> 00:20:25,830
 If you have some other romantic ideas about living the mon

311
00:20:25,830 --> 00:20:27,600
astic life or living in the

312
00:20:27,600 --> 00:20:32,530
 forest or that kind of thing, it's problematic and it will

313
00:20:32,530 --> 00:20:34,880
 be a problem for you and your

314
00:20:34,880 --> 00:20:41,270
 community when it leads to communities that are often off

315
00:20:41,270 --> 00:20:42,260
 base.

316
00:20:42,260 --> 00:20:46,310
 So you have to ask yourself why you're doing it and be

317
00:20:46,310 --> 00:20:50,440
 clear about why you're doing it.

318
00:20:50,440 --> 00:20:55,510
 That being said, there are people who ordain for mundane

319
00:20:55,510 --> 00:20:58,120
 reasons and end up going on to

320
00:20:58,120 --> 00:21:02,120
 becoming enlightened.

321
00:21:02,120 --> 00:21:04,440
 It's not something you should worry too much about.

322
00:21:04,440 --> 00:21:07,670
 If you'd like to ordain, well, just make sure you have good

323
00:21:07,670 --> 00:21:09,560
 intentions and you're not ordaining

324
00:21:09,560 --> 00:21:13,840
 for bad reasons.

325
00:21:13,840 --> 00:21:16,960
 Really it's a good thing to do if you can find a place.

326
00:21:16,960 --> 00:21:21,030
 The big problem is nowadays in modern times it's not easy

327
00:21:21,030 --> 00:21:23,840
 to find a great place to ordain.

328
00:21:23,840 --> 00:21:29,510
 Of course it's not easy to get to one of the places, good

329
00:21:29,510 --> 00:21:32,080
 places that do exist.

330
00:21:32,080 --> 00:21:35,220
 When noting anger, the thoughts and burning sensations get

331
00:21:35,220 --> 00:21:36,040
 more vivid.

332
00:21:36,040 --> 00:21:37,040
 This raises fear.

333
00:21:37,040 --> 00:21:39,600
 "Would Sati protect us in this situation?"

334
00:21:39,600 --> 00:21:44,240
 Yeah, I mean you'd note the fear.

335
00:21:44,240 --> 00:21:45,840
 Really that's what it's all about.

336
00:21:45,840 --> 00:21:50,280
 It's about vividly experiencing these things, the thoughts

337
00:21:50,280 --> 00:21:52,560
 and the burning sensations.

338
00:21:52,560 --> 00:21:53,840
 They're not the problem.

339
00:21:53,840 --> 00:21:57,920
 The problem is your reactions, your fear and so on.

340
00:21:57,920 --> 00:22:03,420
 And so do that, go through that to the point where you see

341
00:22:03,420 --> 00:22:06,240
 that and you start to become

342
00:22:06,240 --> 00:22:11,060
 resilient so that these thoughts and burning sensations don

343
00:22:11,060 --> 00:22:13,120
't have power over you.

344
00:22:13,120 --> 00:22:21,640
 I'm trying to sit in full lotus position.

345
00:22:21,640 --> 00:22:24,360
 Ask them because I learn more if my practice is challenging

346
00:22:24,360 --> 00:22:25,840
 but is irrelevant of this kind

347
00:22:25,840 --> 00:22:26,840
 of goal.

348
00:22:26,840 --> 00:22:33,680
 Okay, you learn more if your practice is challenging.

349
00:22:33,680 --> 00:22:36,590
 This isn't really I think a great attitude because it's

350
00:22:36,590 --> 00:22:38,480
 understandable but it's another

351
00:22:38,480 --> 00:22:43,700
 one of these, I would caution that this is probably another

352
00:22:43,700 --> 00:22:48,360
 one of these "Let's make

353
00:22:48,360 --> 00:22:50,640
 it quicker" kind of states.

354
00:22:50,640 --> 00:22:55,180
 Let's do something, find a trick to improve my practice.

355
00:22:55,180 --> 00:22:57,520
 And there are no tricks to improving your practice.

356
00:22:57,520 --> 00:23:01,200
 Trying to find a trick is in and of itself going to hinder

357
00:23:01,200 --> 00:23:02,440
 your practice.

358
00:23:02,440 --> 00:23:06,980
 You should practice some mundane old practice, sit with

359
00:23:06,980 --> 00:23:09,720
 your legs, one leg in front of the

360
00:23:09,720 --> 00:23:13,630
 other and just slog through it without trying to find any

361
00:23:13,630 --> 00:23:14,380
 trick.

362
00:23:14,380 --> 00:23:20,480
 The full lotus position is arguably better for samatha

363
00:23:20,480 --> 00:23:22,000
 practice.

364
00:23:22,000 --> 00:23:25,550
 I would argue that it's not better for vipassana practice

365
00:23:25,550 --> 00:23:27,280
 for a couple of reasons.

366
00:23:27,280 --> 00:23:31,330
 For this reason because it's just a trick, insight doesn't

367
00:23:31,330 --> 00:23:36,160
 have anything to do with that.

368
00:23:36,160 --> 00:23:41,830
 And because since you don't have these deep powerful states

369
00:23:41,830 --> 00:23:44,480
 of concentration, it's going

370
00:23:44,480 --> 00:23:49,790
 to aggravate, it potentially causes aggravation in your

371
00:23:49,790 --> 00:23:50,840
 joints.

372
00:23:50,840 --> 00:23:53,760
 If you're in the full lotus.

373
00:23:53,760 --> 00:23:55,560
 It's not to say you shouldn't.

374
00:23:55,560 --> 00:23:58,580
 If you want to sit in the full lotus, I'm not going to tell

375
00:23:58,580 --> 00:24:00,040
 you "No, that's bad."

376
00:24:00,040 --> 00:24:06,920
 But I would caution against it generally.

377
00:24:06,920 --> 00:24:09,970
 Especially if it's just because you want to challenge

378
00:24:09,970 --> 00:24:10,820
 yourself.

379
00:24:10,820 --> 00:24:19,400
 Challenge yourself by not challenging yourself.

380
00:24:19,400 --> 00:24:21,400
 Does this recent eclipse have any meaning?

381
00:24:21,400 --> 00:24:26,460
 I think it means that the moon came between the earth and

382
00:24:26,460 --> 00:24:27,480
 the sun.

383
00:24:27,480 --> 00:24:29,720
 I'm not sure about that.

384
00:24:29,720 --> 00:24:34,350
 It could also mean that Rahu has eaten the, eats the sun,

385
00:24:34,350 --> 00:24:36,760
 swallows the sun for a period

386
00:24:36,760 --> 00:24:37,760
 of time.

387
00:24:37,760 --> 00:24:42,400
 Rahu is a demon or a god.

388
00:24:42,400 --> 00:24:45,400
 You pick.

389
00:24:45,400 --> 00:24:50,040
 Dear Bhante, is dukkha and suffering the same?

390
00:24:50,040 --> 00:24:52,280
 Well, suffering is an English word.

391
00:24:52,280 --> 00:24:56,520
 And I'm not being facetious by saying that because dukkha

392
00:24:56,520 --> 00:24:58,720
 is translated in different

393
00:24:58,720 --> 00:25:02,440
 ways but generally the answer I would give is yes.

394
00:25:02,440 --> 00:25:04,960
 But it's hard to say.

395
00:25:04,960 --> 00:25:07,240
 It depends what you mean by suffering.

396
00:25:07,240 --> 00:25:10,720
 And maybe it depends what you mean by dukkha as well.

397
00:25:10,720 --> 00:25:14,880
 But generally that's dukkha is translated as suffering.

398
00:25:14,880 --> 00:25:18,800
 Does the term Buddha nature also play a role in terra vada?

399
00:25:18,800 --> 00:25:22,040
 Not really.

400
00:25:22,040 --> 00:25:27,240
 Not really.

401
00:25:27,240 --> 00:25:31,240
 In my opinion, I don't think it does.

402
00:25:31,240 --> 00:25:32,880
 So that's all the questions.

403
00:25:32,880 --> 00:25:34,640
 Thank you all for tuning in.

404
00:25:34,640 --> 00:25:35,280
 Have a good night.

405
00:25:35,280 --> 00:25:36,280
 Thank you.

406
00:25:36,280 --> 00:25:37,280
 Thank you.

407
00:25:37,280 --> 00:25:37,280
 Thank you.

408
00:25:37,280 --> 00:25:38,280
 Thank you.

409
00:25:38,280 --> 00:25:39,280
 Thank you.

410
00:25:39,280 --> 00:25:40,280
 Thank you.

411
00:25:40,280 --> 00:25:41,280
 Thank you.

412
00:25:41,280 --> 00:25:42,280
 Thank you.

413
00:25:42,280 --> 00:25:43,280
 Thank you.

414
00:25:43,280 --> 00:25:44,280
 Thank you.

415
00:25:44,280 --> 00:25:45,280
 Thank you.

416
00:25:45,280 --> 00:25:46,280
 Thank you.

417
00:25:46,280 --> 00:25:47,280
 Thank you.

418
00:25:47,280 --> 00:25:48,280
 Thank you.

419
00:25:48,280 --> 00:25:49,280
 Thank you.

420
00:25:49,280 --> 00:25:50,280
 Thank you.

421
00:25:50,280 --> 00:25:51,280
 Thank you.

422
00:25:51,280 --> 00:25:52,280
 Thank you.

423
00:25:52,280 --> 00:25:53,280
 Thank you.

424
00:25:53,280 --> 00:25:54,280
 Thank you.

425
00:25:54,280 --> 00:25:55,280
 Thank you.

426
00:25:55,280 --> 00:25:56,280
 Thank you.

427
00:25:56,280 --> 00:25:57,280
 Thank you.

428
00:25:57,280 --> 00:25:58,280
 Thank you.

429
00:25:58,280 --> 00:25:59,280
 Thank you.

430
00:25:59,280 --> 00:26:00,280
 Thank you.

431
00:26:00,280 --> 00:26:01,280
 Thank you.

432
00:26:01,280 --> 00:26:02,280
 Thank you.

433
00:26:02,280 --> 00:26:03,280
 Thank you.

434
00:26:03,280 --> 00:26:04,280
 Thank you.

435
00:26:04,280 --> 00:26:05,280
 Thank you.

436
00:26:05,280 --> 00:26:06,280
 Thank you.

437
00:26:06,280 --> 00:26:07,280
 Thank you.

438
00:26:07,280 --> 00:26:08,280
 Thank you.

439
00:26:08,280 --> 00:26:09,280
 Thank you.

440
00:26:09,280 --> 00:26:10,280
 Thank you.

441
00:26:10,280 --> 00:26:11,280
 Thank you.

