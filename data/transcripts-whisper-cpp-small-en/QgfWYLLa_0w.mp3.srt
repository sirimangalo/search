1
00:00:00,000 --> 00:00:07,000
 Okay, so again here tonight with us is Sumedha, the newest

2
00:00:07,000 --> 00:00:10,280
 member of our monastic community

3
00:00:10,280 --> 00:00:16,650
 and she has just finished the foundation and advanced

4
00:00:16,650 --> 00:00:18,720
 courses.

5
00:00:18,720 --> 00:00:20,040
 What does that mean?

6
00:00:20,040 --> 00:00:26,630
 The foundation course is a course for someone who is new to

7
00:00:26,630 --> 00:00:28,920
 this tradition.

8
00:00:28,920 --> 00:00:32,280
 In general you'd say someone who is new to the Mahasi Sayad

9
00:00:32,280 --> 00:00:33,720
aw tradition.

10
00:00:33,720 --> 00:00:36,590
 So if someone had practiced in the Mahasi Sayadaw tradition

11
00:00:36,590 --> 00:00:39,680
 already we'd kind of accelerate

12
00:00:39,680 --> 00:00:42,360
 the foundation course.

13
00:00:42,360 --> 00:00:49,060
 But if anyone hasn't practiced in the school that we follow

14
00:00:49,060 --> 00:00:52,120
 of Ajahn Tong then we'll put

15
00:00:52,120 --> 00:00:56,440
 them through some sort of foundation course but we might

16
00:00:56,440 --> 00:00:58,640
 accelerate it and call it an

17
00:00:58,640 --> 00:01:02,530
 intermediate course if they have practiced the same sort of

18
00:01:02,530 --> 00:01:04,200
 tradition and same sort of

19
00:01:04,200 --> 00:01:05,200
 technique.

20
00:01:05,200 --> 00:01:09,080
 Now in Sumedha's case she's practiced in other traditions

21
00:01:09,080 --> 00:01:10,680
 before but I think never

22
00:01:10,680 --> 00:01:14,120
 done a formal course in this tradition.

23
00:01:14,120 --> 00:01:17,570
 And so she got a full, almost full foundation course and in

24
00:01:17,570 --> 00:01:19,480
 fact it was quicker than normal

25
00:01:19,480 --> 00:01:27,320
 because she's practiced meditation for many years.

26
00:01:27,320 --> 00:01:30,780
 And the advanced course is basically a repeat of the

27
00:01:30,780 --> 00:01:33,440
 foundation course but it's only, the

28
00:01:33,440 --> 00:01:37,500
 foundation course generally takes 15 to 20 days to complete

29
00:01:37,500 --> 00:01:37,680
.

30
00:01:37,680 --> 00:01:42,610
 The advanced course only takes 10 days and you skip some of

31
00:01:42,610 --> 00:01:45,000
 the beginning stages because

32
00:01:45,000 --> 00:01:48,740
 the meditator is assumed to have already become

33
00:01:48,740 --> 00:01:52,320
 accomplished in at least the basic stages.

34
00:01:52,320 --> 00:01:54,370
 And so she's done both of those and that's really the

35
00:01:54,370 --> 00:01:55,640
 curriculum that we have and at

36
00:01:55,640 --> 00:01:59,040
 this point the meditator will continue on with advanced

37
00:01:59,040 --> 00:02:00,920
 course after advanced course

38
00:02:00,920 --> 00:02:03,560
 at their convenience.

39
00:02:03,560 --> 00:02:06,400
 So we do 10 day by 10 day courses.

40
00:02:06,400 --> 00:02:09,030
 And there are other things that can be done over the long

41
00:02:09,030 --> 00:02:10,520
 term but in general that's how

42
00:02:10,520 --> 00:02:13,000
 it goes.

43
00:02:13,000 --> 00:02:15,820
 So what I want to ask tonight, first maybe you can tell us

44
00:02:15,820 --> 00:02:17,600
 a little bit about your background

45
00:02:17,600 --> 00:02:22,330
 and what got you interested in meditation and Buddhism and

46
00:02:22,330 --> 00:02:24,400
 a little bit about, just

47
00:02:24,400 --> 00:02:31,280
 briefly about the courses that you've gone through.

48
00:02:31,280 --> 00:02:37,680
 I suppose I picked up a Buddhist book somehow along the

49
00:02:37,680 --> 00:02:42,880
 line around 2006 and sparked some

50
00:02:42,880 --> 00:02:47,380
 interest in meditation but then it wasn't until I was

51
00:02:47,380 --> 00:02:50,860
 living in Vancouver, British Columbia

52
00:02:50,860 --> 00:02:55,120
 and my housemates had all set goenka courses.

53
00:02:55,120 --> 00:02:58,400
 And they said, "Oh you should go, it's wonderful and you'll

54
00:02:58,400 --> 00:03:00,040
 learn how to deal with pain."

55
00:03:00,040 --> 00:03:11,080
 And then one of the housemates ended up going and saying it

56
00:03:11,080 --> 00:03:13,280
 was quite humbling and so I

57
00:03:13,280 --> 00:03:17,680
 signed myself up and I thought I was quite happy going into

58
00:03:17,680 --> 00:03:19,520
 the course and got there

59
00:03:19,520 --> 00:03:25,140
 and I was very humbled and found my levels of contentment

60
00:03:25,140 --> 00:03:27,640
 when I was stripped of all

61
00:03:27,640 --> 00:03:32,280
 of my artistic allowances.

62
00:03:32,280 --> 00:03:37,200
 I've been a painter so I wasn't allowed to paint, I wasn't

63
00:03:37,200 --> 00:03:39,720
 allowed to, you know, I guess

64
00:03:39,720 --> 00:03:45,030
 at that point I was drinking and smoking quite regularly so

65
00:03:45,030 --> 00:03:47,780
 that was taken out and just eating

66
00:03:47,780 --> 00:03:59,390
 after noon. What else? Sleeping when I want to, the pain of

67
00:03:59,390 --> 00:04:00,280
 sitting compounds everything,

68
00:04:00,280 --> 00:04:05,170
 I guess it's about 10 hours a day with the goenka tradition

69
00:04:05,170 --> 00:04:07,040
 and the technique like this

70
00:04:07,040 --> 00:04:11,920
 one just is an emotional exfoliator so on top of taking all

71
00:04:11,920 --> 00:04:14,200
 the comforts of daily life

72
00:04:14,200 --> 00:04:21,250
 out it brings all of the stuff that I had been distracting

73
00:04:21,250 --> 00:04:24,720
 myself with, distracting

74
00:04:24,720 --> 00:04:28,560
 myself from with the central pleasures.

75
00:04:28,560 --> 00:04:32,730
 After seeing that first course I felt like I should really

76
00:04:32,730 --> 00:04:34,880
 meditate and stay at centers

77
00:04:34,880 --> 00:04:42,720
 until I can really find a sense of contentment with being

78
00:04:42,720 --> 00:04:46,720
 stripped down of all those allowances

79
00:04:46,720 --> 00:04:56,770
 of art and music and socializing, I guess extroverting in

80
00:04:56,770 --> 00:05:03,040
 general, just finding contentment

81
00:05:03,040 --> 00:05:12,040
 from within and so I just kept sitting course after course,

82
00:05:12,040 --> 00:05:16,960
 I guess about maybe 8 or 10

83
00:05:16,960 --> 00:05:23,300
 goenka courses plus I just went out and goenka says you can

84
00:05:23,300 --> 00:05:26,240
, once you know the time table

85
00:05:26,240 --> 00:05:29,010
 it's pretty simple, you just go and you can sit self

86
00:05:29,010 --> 00:05:30,880
 courses so I went out and sat some

87
00:05:30,880 --> 00:05:39,770
 in the forest and one fellow meditator allowed me to sit on

88
00:05:39,770 --> 00:05:43,240
 her property in Hawaii and then

89
00:05:43,240 --> 00:05:46,300
 I tried to sit a bit longer which is not really okay in the

90
00:05:46,300 --> 00:05:48,320
 goenka tradition to sit over 10

91
00:05:48,320 --> 00:05:52,620
 days by yourself but I had my friend drop me off somewhere

92
00:05:52,620 --> 00:05:54,640
 where I couldn't really go

93
00:05:54,640 --> 00:05:57,780
 back on the plan and it was storm season so it was

94
00:05:57,780 --> 00:06:00,400
 hurricane first winds and I was stuck

95
00:06:00,400 --> 00:06:06,820
 out in an archipelago, Haida Gwaii, it's like 100

96
00:06:06,820 --> 00:06:10,840
 kilometers off the northern coast of British

97
00:06:10,840 --> 00:06:17,660
 Columbia and I tried to sit a 20 day course on my own and a

98
00:06:17,660 --> 00:06:20,920
 friend had given me a forest

99
00:06:20,920 --> 00:06:25,240
 tradition book which is very inspiring and had actually

100
00:06:25,240 --> 00:06:27,440
 inspired me to put myself in

101
00:06:27,440 --> 00:06:33,140
 such a situation and then I had sat 10 day courses

102
00:06:33,140 --> 00:06:36,480
 completely alone in the forest before

103
00:06:36,480 --> 00:06:43,110
 I had no problem but for some reason that going for 20 days

104
00:06:43,110 --> 00:06:46,240
 was really really difficult

105
00:06:46,240 --> 00:06:49,570
 I was having a hard time getting out of my sleeping bag in

106
00:06:49,570 --> 00:06:50,960
 the morning, I was having

107
00:06:50,960 --> 00:06:55,030
 a hard time getting out of my sleeping bag and I decided to

108
00:06:55,030 --> 00:06:56,840
 just collect seaweed off

109
00:06:56,840 --> 00:07:03,320
 the shore and dry it out and eat that and popcorn to clean

110
00:07:03,320 --> 00:07:06,960
 out the system and eventually

111
00:07:06,960 --> 00:07:09,430
 I started adding stuff but the problem was I fasted for a

112
00:07:09,430 --> 00:07:10,760
 couple days in the middle of

113
00:07:10,760 --> 00:07:14,790
 the course, I had been really taking very little food and

114
00:07:14,790 --> 00:07:16,480
 then I fasted and I think

115
00:07:16,480 --> 00:07:20,970
 I might have drank a bunch of salt water like a gallon or

116
00:07:20,970 --> 00:07:23,200
 something and then I ate some

117
00:07:23,200 --> 00:07:27,870
 like quinoa or lentils or something and I felt like super

118
00:07:27,870 --> 00:07:29,960
woman and I thought okay I'm

119
00:07:29,960 --> 00:07:34,590
 going to the cave now and so I set out with my backpack and

120
00:07:34,590 --> 00:07:36,640
 the sun had come out and it

121
00:07:36,640 --> 00:07:41,040
 had been so wet and no one lives on the west coast of Haida

122
00:07:41,040 --> 00:07:44,200
 Gwaii because it's so wet and

123
00:07:44,200 --> 00:07:49,620
 the elements, it's the longest stretch of untouched forest

124
00:07:49,620 --> 00:07:51,920
 so the Jurassic nature of

125
00:07:51,920 --> 00:07:57,900
 it is great for meditation I guess, powerful but it's

126
00:07:57,900 --> 00:08:03,240
 relentless with its weather, the

127
00:08:03,240 --> 00:08:08,280
 wind gets the rain vertical and upside down, it was pudd

128
00:08:08,280 --> 00:08:11,320
ling inside the door somehow, shaking

129
00:08:11,320 --> 00:08:15,490
 the cabin with the winds actually. And in the end did you

130
00:08:15,490 --> 00:08:17,160
 stay there 20 days? Well I

131
00:08:17,160 --> 00:08:22,270
 ended up getting stuck out there for 26 because of the

132
00:08:22,270 --> 00:08:25,400
 storms and I really felt like, I felt

133
00:08:25,400 --> 00:08:28,210
 like that was the longest time of my life, I've had near-

134
00:08:28,210 --> 00:08:29,920
death experiences but that was

135
00:08:29,920 --> 00:08:34,020
 the most drawn out feeling like I might die out here

136
00:08:34,020 --> 00:08:36,800
 because I ran out of firewood and

137
00:08:36,800 --> 00:08:41,010
 I ran out of gas in the chainsaw and then I was feeling

138
00:08:41,010 --> 00:08:43,440
 like I can't live in the forest

139
00:08:43,440 --> 00:08:49,540
 anymore because I couldn't, I was bucking wood and I couldn

140
00:08:49,540 --> 00:08:52,320
't handle killing the moss

141
00:08:52,320 --> 00:08:55,310
 and I had been farming and I thought of all the weeds and

142
00:08:55,310 --> 00:08:57,120
 all the weeds I had been killing

143
00:08:57,120 --> 00:09:03,200
 and I felt like I couldn't do it anymore. And then when I

144
00:09:03,200 --> 00:09:05,920
 set out for the cave I was,

145
00:09:05,920 --> 00:09:09,740
 I mean I had been warned that there was landslides along

146
00:09:09,740 --> 00:09:12,280
 the coast but I got myself into two

147
00:09:12,280 --> 00:09:18,730
 landslides and the fear, I mean it just was very humbling

148
00:09:18,730 --> 00:09:21,960
 of an ego, I guess my ego got

149
00:09:21,960 --> 00:09:25,300
 crushed with nature like the entire forest. I didn't

150
00:09:25,300 --> 00:09:27,400
 understand exactly what a landslide

151
00:09:27,400 --> 00:09:35,050
 was up close and personal and how to get through it but I

152
00:09:35,050 --> 00:09:40,400
 ended up somehow thankfully getting

153
00:09:40,400 --> 00:09:46,440
 back to the cabin alive and I was reading the Pikubodi's

154
00:09:46,440 --> 00:09:49,800
 discourses and had never actually

155
00:09:49,800 --> 00:09:54,200
 read like a translation of, just you know a straight sort

156
00:09:54,200 --> 00:09:56,720
 of translation of the discourses

157
00:09:56,720 --> 00:09:59,830
 I had only really heard it through Grenko which is great

158
00:09:59,830 --> 00:10:01,000
 but it's not the same and I

159
00:10:01,000 --> 00:10:09,290
 think it cut through a lot of layers of delusion and really

160
00:10:09,290 --> 00:10:11,360
 made me consider like what's the

161
00:10:11,360 --> 00:10:18,730
 best way I can spend my time and I had been planning to

162
00:10:18,730 --> 00:10:22,320
 start a meditation center up there

163
00:10:22,320 --> 00:10:28,490
 and bring teachers there and run it off the grid and yoga

164
00:10:28,490 --> 00:10:32,240
 center, meditation center.

165
00:10:32,240 --> 00:10:37,760
 And you mentioned that this was a sort of a turning point

166
00:10:37,760 --> 00:10:40,600
 where you realized that you

167
00:10:40,600 --> 00:10:44,550
 needed to do this kind of thing under a teacher in the

168
00:10:44,550 --> 00:10:45,920
 beginning.

169
00:10:45,920 --> 00:10:54,730
 That was my mind. I would read like the sutas or the Ach

170
00:10:54,730 --> 00:10:57,120
arya Man's biography and I'd feel

171
00:10:57,120 --> 00:11:01,370
 so inspired, incredibly inspired and then I would sit and

172
00:11:01,370 --> 00:11:03,360
 my whole body just hurt and

173
00:11:03,360 --> 00:11:07,240
 I never had like a recent course at that point it had been

174
00:11:07,240 --> 00:11:09,520
 about three years of sitting or

175
00:11:09,520 --> 00:11:14,140
 maybe two years of sitting and it had been a long time

176
00:11:14,140 --> 00:11:17,280
 since I had just the entire course

177
00:11:17,280 --> 00:11:20,870
 like scanning the body like just every part of it seemed to

178
00:11:20,870 --> 00:11:22,400
 hurt and Mehta was out of

179
00:11:22,400 --> 00:11:29,310
 the question. It was just I was so agitated and I was so my

180
00:11:29,310 --> 00:11:33,640
 mind was ruthless with trying

181
00:11:33,640 --> 00:11:39,180
 to get away with things just not you know daydreaming

182
00:11:39,180 --> 00:11:42,400
 pretty much and I thought oh if

183
00:11:42,400 --> 00:11:44,560
 I have a teacher that can read my mind I can't get away

184
00:11:44,560 --> 00:11:46,040
 with this and then I thought well

185
00:11:46,040 --> 00:11:49,260
 even if I don't have a teacher that can read my mind if I'm

186
00:11:49,260 --> 00:11:50,680
 in robes I don't think there's

187
00:11:50,680 --> 00:11:56,400
 any way I'm going to really let myself mess around because

188
00:11:56,400 --> 00:11:59,160
 that's I mean I have so much

189
00:11:59,160 --> 00:12:05,160
 respect for Buddha's work and like there wouldn't be you

190
00:12:05,160 --> 00:12:09,760
 know such requisites available I mean

191
00:12:09,760 --> 00:12:14,630
 maybe there would be in another way but. So that was the

192
00:12:14,630 --> 00:12:16,040
 time when you also thought that

193
00:12:16,040 --> 00:12:20,770
 you wanted to ordain where it hit you also reading Ajahn

194
00:12:20,770 --> 00:12:24,560
 Man's biography. Okay so what

195
00:12:24,560 --> 00:12:33,400
 led you to our center? Palanyani. You went to Aranyabodhi

196
00:12:33,400 --> 00:12:36,120
 and this is where you in your

197
00:12:36,120 --> 00:12:41,260
 efforts to find a place to ordain. Yeah I thought I go I

198
00:12:41,260 --> 00:12:43,840
 think I actually decided when

199
00:12:43,840 --> 00:12:47,310
 I was out there I should go to the monastery in California

200
00:12:47,310 --> 00:12:49,160
 and I had heard of the nunnery

201
00:12:49,160 --> 00:12:53,840
 I thought I'll get some direction for Thailand there. So

202
00:12:53,840 --> 00:12:56,520
 your intention was to go to Thailand?

203
00:12:56,520 --> 00:13:01,650
 Yeah I had I had no idea that it was at all even somewhat

204
00:13:01,650 --> 00:13:04,800
 difficult to ordain as a female

205
00:13:04,800 --> 00:13:11,850
 monk or even a nun I had no idea that there was any sort of

206
00:13:11,850 --> 00:13:15,280
 controversy at all I went

207
00:13:15,280 --> 00:13:20,630
 to Birkim monastery on the way down and the abbot. To see

208
00:13:20,630 --> 00:13:22,680
 if you could ordain there? No

209
00:13:22,680 --> 00:13:26,280
 just to ask for his advice and he said oh there's actually

210
00:13:26,280 --> 00:13:28,320
 a buffet of ordinations that

211
00:13:28,320 --> 00:13:36,830
 you can choose from. Oh really? He had had one lady who

212
00:13:36,830 --> 00:13:39,880
 went I guess 10 years ago to

213
00:13:39,880 --> 00:13:51,320
 be a Mechi, Mechi Quinn. Quinn I think and I don't know

214
00:13:51,320 --> 00:13:55,080
 where she's at but a fellow meditator

215
00:13:55,080 --> 00:13:57,720
 there said she actually met her and said she was having a

216
00:13:57,720 --> 00:13:59,480
 hard time and the general feeling

217
00:13:59,480 --> 00:14:04,050
 I got was that the Mechi life was not easy and it was not

218
00:14:04,050 --> 00:14:06,920
 people weren't exactly liking

219
00:14:06,920 --> 00:14:13,640
 it so much and so it really kind of and I wasn't really

220
00:14:13,640 --> 00:14:17,480
 feeling the Mechi too much but

221
00:14:17,480 --> 00:14:20,590
 you know I was I'd rather be Mechi than a lay person I

222
00:14:20,590 --> 00:14:22,560
 think and then one of the Ajans

223
00:14:22,560 --> 00:14:27,700
 at Wat Phan Anh Chhat when I told him that I was thinking

224
00:14:27,700 --> 00:14:32,040
 of ordaining at Wat Phap Phang

225
00:14:32,040 --> 00:14:36,560
 he said I don't think that's a good idea I think you'll get

226
00:14:36,560 --> 00:14:38,800
 disheartened and maybe that's

227
00:14:38,800 --> 00:14:44,430
 because they they do a lot of work there and I feel

228
00:14:44,430 --> 00:14:49,200
 extremely grateful that Palanyani invited

229
00:14:49,200 --> 00:14:51,610
 me here and gave me a little bit of warning because she's

230
00:14:51,610 --> 00:14:53,040
 actually been a Mechi herself

231
00:14:53,040 --> 00:14:56,380
 as well and she's been through the ropes. What do you think

232
00:14:56,380 --> 00:14:58,600
 of Sri Lanka to be the country

233
00:14:58,600 --> 00:15:06,730
 so far? I think it's I haven't seen much of it but being

234
00:15:06,730 --> 00:15:15,040
 here for some reason I feel at

235
00:15:15,040 --> 00:15:18,350
 home but I've been living on a lot of islands like this

236
00:15:18,350 --> 00:15:20,400
 island actually kind of reminds

237
00:15:20,400 --> 00:15:24,280
 me a bit of a Buddhist version of Puerto Rico I was living

238
00:15:24,280 --> 00:15:26,920
 on Puerto Rico. I know the feeling

239
00:15:26,920 --> 00:15:30,430
 of the island I was born on an island and and it's amazing

240
00:15:30,430 --> 00:15:32,400
 to have the same feelings come

241
00:15:32,400 --> 00:15:36,160
 back here hey you realize that there is something to be on

242
00:15:36,160 --> 00:15:40,160
 an island. Yeah it makes a bit of

243
00:15:40,160 --> 00:15:44,810
 solidarity or homogeneous. This is also true. Okay but let

244
00:15:44,810 --> 00:15:47,800
's skip ahead a little bit to

245
00:15:47,800 --> 00:15:53,270
 what I'm really interested in is something quite specific

246
00:15:53,270 --> 00:15:55,680
 and I want to pull it out of

247
00:15:55,680 --> 00:16:01,560
 context so the question is very simple question what do you

248
00:16:01,560 --> 00:16:04,760
 think or how do you feel you have

249
00:16:04,760 --> 00:16:07,920
 benefited from this course and what I'd like to why what I

250
00:16:07,920 --> 00:16:09,880
 mean by pulling it out of context

251
00:16:09,880 --> 00:16:12,580
 is I'd like you to just talk about this course and not

252
00:16:12,580 --> 00:16:14,440
 compare it to your other courses in

253
00:16:14,440 --> 00:16:17,800
 speech and not compared to your other courses at all if the

254
00:16:17,800 --> 00:16:19,560
 benefits you gain some of the

255
00:16:19,560 --> 00:16:22,570
 benefits you gain were the same as other courses just talk

256
00:16:22,570 --> 00:16:24,420
 about them as the benefits of this

257
00:16:24,420 --> 00:16:28,100
 course or you know if you think there are benefits which we

258
00:16:28,100 --> 00:16:29,560
 assume that you think there

259
00:16:29,560 --> 00:16:34,130
 are some benefits otherwise you wouldn't still be here so

260
00:16:34,130 --> 00:16:36,280
 how you're feeling before you you

261
00:16:36,280 --> 00:16:38,740
 went into this course how that is different from your

262
00:16:38,740 --> 00:16:40,320
 feeling afterwards so it should

263
00:16:40,320 --> 00:16:43,010
 be specifically about this course but it could be the same

264
00:16:43,010 --> 00:16:44,400
 sorts of things you've gained

265
00:16:44,400 --> 00:16:48,430
 in other courses which is fine but only only referring to

266
00:16:48,430 --> 00:16:50,680
 this course well I mean I would

267
00:16:50,680 --> 00:16:58,550
 say that this course did have its special kind of advantage

268
00:16:58,550 --> 00:17:01,840
 that I could it's very hard to

269
00:17:01,840 --> 00:17:09,600
 talk about consciousness so I guess going into the course I

270
00:17:09,600 --> 00:17:13,160
 was quite confident having

271
00:17:13,160 --> 00:17:20,950
 that other courses and that got a bit shattered just with

272
00:17:20,950 --> 00:17:26,360
 the unknowingness of what I'm getting

273
00:17:26,360 --> 00:17:35,770
 myself into which kind of started happening on day one it's

274
00:17:35,770 --> 00:17:39,280
 like I get I put in a room

275
00:17:39,280 --> 00:17:47,130
 with a television of my mind and it kind of comes back to

276
00:17:47,130 --> 00:17:51,360
 like contentment levels and

277
00:17:51,360 --> 00:18:00,120
 being told to sit there and just I guess flip that I mean I

278
00:18:00,120 --> 00:18:05,120
 guess let's see like the the

279
00:18:05,120 --> 00:18:12,450
 meditation technique here of saying thinking thinking

280
00:18:12,450 --> 00:18:17,040
 thinking when you're thinking was

281
00:18:17,040 --> 00:18:30,190
 a real knife for the ego's head almost it just really to

282
00:18:30,190 --> 00:18:32,520
 put it good way to put it

283
00:18:32,520 --> 00:18:45,480
 I'm inspired okay so I can sit there and I feel I guess

284
00:18:45,480 --> 00:18:50,680
 confident in the sense that like

285
00:18:50,680 --> 00:18:55,960
 I could easily sit with my mind and watch it I guess but

286
00:18:55,960 --> 00:18:58,480
 like a you know TV but it does

287
00:18:58,480 --> 00:19:02,440
 get annoying after a few days of you know not sitting on a

288
00:19:02,440 --> 00:19:04,240
 couch and watching it or

289
00:19:04,240 --> 00:19:09,580
 with all your food and drinks and every popcorn and some of

290
00:19:09,580 --> 00:19:12,440
 the channels can get quite violent

291
00:19:12,440 --> 00:19:18,300
 and are just agitating as the technique goes on and this

292
00:19:18,300 --> 00:19:22,160
 meditation technique was telling

293
00:19:22,160 --> 00:19:27,210
 me sitting across the room and saying now seeing seeing

294
00:19:27,210 --> 00:19:29,960
 seeing say to yourself seeing

295
00:19:29,960 --> 00:19:37,570
 seeing which my mind didn't want to do when it had and you

296
00:19:37,570 --> 00:19:41,280
 know this was a very I guess

297
00:19:41,280 --> 00:19:47,800
 a big the just having the kind of babysitter of not letting

298
00:19:47,800 --> 00:19:50,880
 myself get away with going

299
00:19:50,880 --> 00:19:56,860
 along with what was happening because I guess I got to a

300
00:19:56,860 --> 00:20:01,240
 point where I could be with sensations

301
00:20:01,240 --> 00:20:07,330
 or be with the breath of some in some way but still kind of

302
00:20:07,330 --> 00:20:10,240
 be enjoying watching the

303
00:20:10,240 --> 00:20:16,660
 workings of the mind so I guess my mind kind of threw a fit

304
00:20:16,660 --> 00:20:19,840
 for a while the babysitter

305
00:20:19,840 --> 00:20:23,640
 of the meditation techniques saying now just say thinking

306
00:20:23,640 --> 00:20:25,960
 thinking thinking just say seeing

307
00:20:25,960 --> 00:20:31,990
 seeing seeing almost kind of like teasing you like I have

308
00:20:31,990 --> 00:20:33,200
 the remote control and I can

309
00:20:33,200 --> 00:20:35,460
 turn off this TV if you want I can turn off this radio of

310
00:20:35,460 --> 00:20:36,840
 your mind but you're gonna have

311
00:20:36,840 --> 00:20:43,570
 to practice and so I I guess eventually I gave in and it's

312
00:20:43,570 --> 00:20:47,040
 like the the room disappears

313
00:20:47,040 --> 00:20:53,010
 and I'm you know in a great state of mind like like being

314
00:20:53,010 --> 00:20:55,760
 you know out on out on the

315
00:20:55,760 --> 00:20:59,360
 west coast in Haida Gwaii but that's only so great for a

316
00:20:59,360 --> 00:21:01,320
 little bit and then I noticed

317
00:21:01,320 --> 00:21:05,640
 the babysitter still there saying seeing seeing like when

318
00:21:05,640 --> 00:21:08,080
 you said to me you're feeling very

319
00:21:08,080 --> 00:21:14,430
 happy and then you hear the babysitter happy or calm was it

320
00:21:14,430 --> 00:21:17,320
 calm and you're like no no

321
00:21:17,320 --> 00:21:23,030
 it just shatters everything it's not it's not that fun

322
00:21:23,030 --> 00:21:25,880
 anymore to have that overlooking

323
00:21:25,880 --> 00:21:34,620
 impartiality sit with you and it's like why isn't it fun

324
00:21:34,620 --> 00:21:38,040
 why like why and I kind of I

325
00:21:38,040 --> 00:21:44,330
 sort of tried to examine myself a bit more and it's like

326
00:21:44,330 --> 00:21:46,960
 well what's like I guess and

327
00:21:46,960 --> 00:21:50,960
 with the teachers help like it's really just greed like

328
00:21:50,960 --> 00:21:54,320
 indulgence I'm not indulging in

329
00:21:54,320 --> 00:21:58,500
 you know food I'm not indulging in sleep I mean sleep

330
00:21:58,500 --> 00:22:01,440
 deprived of anything but I'm indulging

331
00:22:01,440 --> 00:22:06,300
 in my mental states and daydreaming I'm indulging in

332
00:22:06,300 --> 00:22:10,280
 letting myself think letting the ego take

333
00:22:10,280 --> 00:22:18,460
 hold take reins and and that broke down I guess a whole not

334
00:22:18,460 --> 00:22:22,200
her level to a state where

335
00:22:22,200 --> 00:22:27,230
 what would you call that cessation that I don't actually

336
00:22:27,230 --> 00:22:30,080
 remember which is interesting

337
00:22:30,080 --> 00:22:34,540
 but what has been important for me with meditation is not

338
00:22:34,540 --> 00:22:37,480
 so much the experience as much as like

339
00:22:37,480 --> 00:22:42,600
 the effects it has and the effect coming out of a state

340
00:22:42,600 --> 00:22:48,440
 that I don't remember because apparently

341
00:22:48,440 --> 00:22:54,140
 his memory is impermanent memory is impermanent and if they

342
00:22:54,140 --> 00:22:55,320
 were impermanent there would be

343
00:22:55,320 --> 00:23:01,790
 suffering no right and if they're suffering it's not

344
00:23:01,790 --> 00:23:06,080
 cessation so I mean I'm not trying

345
00:23:06,080 --> 00:23:08,790
 to put words into your mouth I'm just caught a running

346
00:23:08,790 --> 00:23:10,560
 commentary no go ahead the most of

347
00:23:10,560 --> 00:23:13,950
 it I'm totally with you the most important are the results

348
00:23:13,950 --> 00:23:15,400
 so if it the proof of the

349
00:23:15,400 --> 00:23:20,670
 pudding is under the crust what was the the benefit that

350
00:23:20,670 --> 00:23:23,760
 the change positive or negative

351
00:23:23,760 --> 00:23:28,200
 coming out of course very positive and just a lot of faith

352
00:23:28,200 --> 00:23:32,920
 in turning away from even like

353
00:23:32,920 --> 00:23:37,780
 experiences in the course I'd been I realized I'd been

354
00:23:37,780 --> 00:23:40,720
 clinging to oh wow this this is like

355
00:23:40,720 --> 00:23:46,440
 this is a lot of pain oh my gosh like whoa like what is

356
00:23:46,440 --> 00:23:50,000
 coming out or this like mental

357
00:23:50,000 --> 00:23:55,820
 states I know you know names for different types of mental

358
00:23:55,820 --> 00:23:58,560
 states or revisions coming

359
00:23:58,560 --> 00:24:06,090
 in and going and just making things special I think when D

360
00:24:06,090 --> 00:24:09,600
hamma talk you said this is

361
00:24:09,600 --> 00:24:14,680
 not that and when you said it didn't hit me like it did I

362
00:24:14,680 --> 00:24:17,440
 guess maybe a few days later

363
00:24:17,440 --> 00:24:21,200
 it's like this is not that because it's no longer you know

364
00:24:21,200 --> 00:24:22,880
 I'm no longer staying in the

365
00:24:22,880 --> 00:24:27,700
 present moment if I'm if I've had some sort of experience

366
00:24:27,700 --> 00:24:30,280
 and I'm still thinking about

367
00:24:30,280 --> 00:24:34,920
 it I'm still walking with it and I'm not saying thinking

368
00:24:34,920 --> 00:24:37,320
 thinking thinking and I'm creating

369
00:24:37,320 --> 00:24:44,930
 more I'm proliferating myself really and so I guess having

370
00:24:44,930 --> 00:24:48,640
 surrender to the technique and

371
00:24:48,640 --> 00:24:53,650
 I guess it's experiencing on some level states of cessation

372
00:24:53,650 --> 00:24:56,840
 and coming out experiencing the

373
00:24:56,840 --> 00:25:04,280
 extreme sense of calm and peace and it's a bit more clarity

374
00:25:04,280 --> 00:25:07,320
 I guess it gives a lot of

375
00:25:07,320 --> 00:25:12,370
 faith of the danger in proliferating my thoughts and myself

376
00:25:12,370 --> 00:25:15,160
 and not keeping them in check not

377
00:25:15,160 --> 00:25:23,420
 babysitting them and it gives yeah a lot of inspiration to

378
00:25:23,420 --> 00:25:29,160
 keep practicing I guess wonderful

379
00:25:29,160 --> 00:25:33,230
 this by the way is the definition the Buddha gave to the

380
00:25:33,230 --> 00:25:37,600
 word bhikkhu bhayangi kati bhikkhu

381
00:25:37,600 --> 00:25:42,560
 samsare bhayangi kati the one who sees the fierceness in

382
00:25:42,560 --> 00:25:45,360
 clinging in proliferating in

383
00:25:45,360 --> 00:25:51,010
 proliferating really this is what the word bhikkhu means so

384
00:25:51,010 --> 00:25:52,960
 very well said as though

385
00:25:52,960 --> 00:25:56,760
 we're coming from a book but thank you that was a great

386
00:25:56,760 --> 00:25:59,560
 thing and it'll be a great resource

387
00:25:59,560 --> 00:26:03,700
 for people who are in similar situations and are having

388
00:26:03,700 --> 00:26:06,720
 similar thoughts to give them encouragement

389
00:26:06,720 --> 00:26:11,280
 and help them to make the right choice for their future

