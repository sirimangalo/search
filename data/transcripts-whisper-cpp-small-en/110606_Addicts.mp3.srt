1
00:00:00,000 --> 00:00:26,440
 [

2
00:00:26,440 --> 00:00:31,440
 Meditation for Meditation]

3
00:00:31,440 --> 00:00:37,440
 Meditation for Meditation

4
00:00:37,440 --> 00:00:43,440
 Meditation for Meditation

5
00:00:43,440 --> 00:00:46,440
 Meditation isn't the answer or isn't the immediate answer.

6
00:00:46,440 --> 00:00:51,060
 That rather such people should undertake traditional

7
00:00:51,060 --> 00:00:55,440
 therapy or rehab or so on

8
00:00:55,440 --> 00:00:57,440
 before taking on the meditation.

9
00:00:57,440 --> 00:01:03,180
 Now, I think that it's important to understand that there

10
00:01:03,180 --> 00:01:06,440
 are answers besides the meditation practice.

11
00:01:06,440 --> 00:01:09,720
 The meditation practice isn't the only thing that's

12
00:01:09,720 --> 00:01:10,440
 necessary.

13
00:01:10,440 --> 00:01:14,590
 This is why the Buddha recommended morality as well because

14
00:01:14,590 --> 00:01:16,440
 ideally we do away with these states.

15
00:01:16,440 --> 00:01:18,820
 Ideally we give them up when we start to practice

16
00:01:18,820 --> 00:01:19,440
 meditation.

17
00:01:19,440 --> 00:01:25,610
 And he himself said, or made it quite clear in different

18
00:01:25,610 --> 00:01:26,440
 ways,

19
00:01:26,440 --> 00:01:31,440
 that the meditation relies on morality and without morality

20
00:01:31,440 --> 00:01:34,440
 it's not truly going to succeed.

21
00:01:34,440 --> 00:01:38,050
 Now the problem with saying that one should avoid

22
00:01:38,050 --> 00:01:41,440
 meditation or should not consider meditation

23
00:01:41,440 --> 00:01:46,610
 is that it ignores the fact that meditation should indeed

24
00:01:46,610 --> 00:01:47,440
 be a part

25
00:01:47,440 --> 00:01:50,440
 or is a part of learning to keep morality.

26
00:01:50,440 --> 00:01:54,700
 That even for people who can't keep basic morality who are

27
00:01:54,700 --> 00:01:55,440
 stuck in addiction

28
00:01:55,440 --> 00:02:03,960
 and have physical and mental addictions to substances or to

29
00:02:03,960 --> 00:02:06,440
 the hormones in the body

30
00:02:06,440 --> 00:02:10,690
 it was mentioned about masturbation in the person who asked

31
00:02:10,690 --> 00:02:15,440
 the question was mentioning that and so on.

32
00:02:15,440 --> 00:02:23,700
 There is that side but the meditation, the mental side is

33
00:02:23,700 --> 00:02:26,440
 very much a part of the problem

34
00:02:26,440 --> 00:02:30,440
 and is thus very important to address.

35
00:02:30,440 --> 00:02:37,520
 Now, when we practice meditation we have to keep in mind

36
00:02:37,520 --> 00:02:38,440
 the physical side

37
00:02:38,440 --> 00:02:42,360
 and we have to understand that our state of being is going

38
00:02:42,360 --> 00:02:44,440
 to affect our meditation

39
00:02:44,440 --> 00:02:46,910
 and it's going to really set the tone of our meditation

40
00:02:46,910 --> 00:02:47,440
 practice.

41
00:02:47,440 --> 00:02:51,760
 So we need to eat the right food, we need to look after our

42
00:02:51,760 --> 00:02:55,440
 bodies and our health

43
00:02:55,440 --> 00:02:59,430
 in terms of medicine and so on, we have to find a suitable

44
00:02:59,430 --> 00:03:01,440
 place to live and so on.

45
00:03:01,440 --> 00:03:05,150
 Along with the many other aspects of our practice that we

46
00:03:05,150 --> 00:03:06,440
 have to keep in mind.

47
00:03:06,440 --> 00:03:10,440
 So there's no reason not to practice meditation

48
00:03:10,440 --> 00:03:16,530
 but especially for people who are suffering from severe

49
00:03:16,530 --> 00:03:21,440
 forms of addiction, extreme forms of addiction.

50
00:03:21,440 --> 00:03:24,620
 There are going to be many other things that they'll have

51
00:03:24,620 --> 00:03:25,440
 to keep in mind.

52
00:03:25,440 --> 00:03:29,250
 The Buddha gave a talk on all of the many ways to do away

53
00:03:29,250 --> 00:03:31,440
 with the problems in the mind.

54
00:03:31,440 --> 00:03:34,870
 For instance, we have to guard our senses so we shouldn't

55
00:03:34,870 --> 00:03:37,440
 just look around, stare at things

56
00:03:37,440 --> 00:03:46,440
 and watch and engage in the pleasures of the sense.

57
00:03:46,440 --> 00:03:50,410
 We should restrain ourselves in terms of food, in terms of

58
00:03:50,410 --> 00:03:52,440
 entertainment and so on.

59
00:03:52,440 --> 00:03:58,440
 We should guard our faculties so that we are mindful

60
00:03:58,440 --> 00:04:04,910
 and we're able to keep track of our state of mind and of

61
00:04:04,910 --> 00:04:06,440
 the world around us

62
00:04:06,440 --> 00:04:10,440
 so to not get caught up in entertainment and pleasure.

63
00:04:10,440 --> 00:04:13,850
 I've given talks on this before, the various parts of the

64
00:04:13,850 --> 00:04:14,440
 practice.

65
00:04:14,440 --> 00:04:17,440
 So that certainly is true.

66
00:04:17,440 --> 00:04:23,230
 But the concern as to whether people who take up the

67
00:04:23,230 --> 00:04:24,440
 meditation practice

68
00:04:24,440 --> 00:04:28,700
 and aren't capable that they might therefore turn away from

69
00:04:28,700 --> 00:04:30,440
 the practice and give it up

70
00:04:30,440 --> 00:04:37,420
 seems a little bit specious that the truth of the matter is

71
00:04:37,420 --> 00:04:37,440
, and what we see,

72
00:04:37,440 --> 00:04:41,440
 there are reasons why they give up.

73
00:04:41,440 --> 00:04:46,440
 To an extent, we can mitigate these.

74
00:04:46,440 --> 00:04:51,440
 It's often because there's improper instruction.

75
00:04:51,440 --> 00:04:55,440
 It's often because there isn't the comprehensive practice.

76
00:04:55,440 --> 00:04:58,440
 It's often because of the surroundings.

77
00:04:58,440 --> 00:05:04,440
 So every person's situation is different.

78
00:05:04,440 --> 00:05:08,440
 What's important is how we address this issue of people

79
00:05:08,440 --> 00:05:11,440
 giving up the practice and leaving it behind.

80
00:05:11,440 --> 00:05:14,440
 The first thing we should say is that just because a person

81
00:05:14,440 --> 00:05:15,440
 gives up the practice

82
00:05:15,440 --> 00:05:17,440
 doesn't mean they haven't gained anything.

83
00:05:17,440 --> 00:05:20,440
 So a person might begin to practice meditation

84
00:05:20,440 --> 00:05:26,440
 and get to a certain level or get to a certain point

85
00:05:26,440 --> 00:05:29,440
 and then give it up or put it aside.

86
00:05:29,440 --> 00:05:34,450
 Now, we shouldn't therefore be discouraged or think that

87
00:05:34,450 --> 00:05:35,440
 this person is useless

88
00:05:35,440 --> 00:05:38,440
 or that they have no potential in the meditation practice.

89
00:05:38,440 --> 00:05:43,440
 It can be that after some time they'll come back to it.

90
00:05:43,440 --> 00:05:46,500
 And what we gain, the things that we do, especially things

91
00:05:46,500 --> 00:05:48,440
 that affect our state of mind,

92
00:05:48,440 --> 00:05:51,440
 have a profound effect on our psyche.

93
00:05:51,440 --> 00:05:54,440
 And they stay deeply ingrained in our memory.

94
00:05:54,440 --> 00:05:57,440
 Meditation is something that it's very difficult to forget

95
00:05:57,440 --> 00:05:59,440
 and people can always come back to it.

96
00:05:59,440 --> 00:06:03,440
 Just learning the basics, the technique of meditation,

97
00:06:03,440 --> 00:06:06,440
 without even practicing it, can be a great thing

98
00:06:06,440 --> 00:06:10,440
 because in times of need it often comes back

99
00:06:10,440 --> 00:06:14,440
 and people do take up the practice in earnest.

100
00:06:14,440 --> 00:06:19,440
 So I think that's the first point that I would make.

101
00:06:19,440 --> 00:06:26,100
 The second one is that environment plays a great role in

102
00:06:26,100 --> 00:06:27,440
 addiction recovery.

103
00:06:27,440 --> 00:06:31,440
 So the physical aspects of addiction are obvious

104
00:06:31,440 --> 00:06:35,440
 and environment isn't going to get rid of those.

105
00:06:35,440 --> 00:06:37,340
 No matter where you are, you still have the hormones coming

106
00:06:37,340 --> 00:06:37,440
 up,

107
00:06:37,440 --> 00:06:40,470
 you still have the chemical reactions, the chemical

108
00:06:40,470 --> 00:06:41,440
 interactions,

109
00:06:41,440 --> 00:06:44,440
 and so on, the physical craving.

110
00:06:44,440 --> 00:06:47,950
 But at least half of the problem, actually much more of the

111
00:06:47,950 --> 00:06:48,440
 problem,

112
00:06:48,440 --> 00:06:50,440
 is the mental side.

113
00:06:50,440 --> 00:06:52,440
 And that you can influence by your environment,

114
00:06:52,440 --> 00:06:54,440
 by the people you surround yourself with,

115
00:06:54,440 --> 00:06:56,440
 by the interactions you have,

116
00:06:56,440 --> 00:06:59,440
 by the situations that you get yourself into.

117
00:06:59,440 --> 00:07:03,070
 I mean obviously if all of your friends are addicted as

118
00:07:03,070 --> 00:07:03,440
 well,

119
00:07:03,440 --> 00:07:06,440
 if all of your friends go out to bars and drinking

120
00:07:06,440 --> 00:07:08,440
 or do drugs or so on,

121
00:07:08,440 --> 00:07:13,440
 or if there's this hyper sexuality in the world around you,

122
00:07:13,440 --> 00:07:17,440
 then watching television or going to the mall,

123
00:07:17,440 --> 00:07:19,440
 going to the beach and so on,

124
00:07:19,440 --> 00:07:21,440
 and seeing the objects of your desire,

125
00:07:21,440 --> 00:07:25,440
 then obviously it's going to be much more difficult for you

126
00:07:25,440 --> 00:07:27,440
 to overcome the states.

127
00:07:27,440 --> 00:07:30,440
 Now this is where meditation can excel

128
00:07:30,440 --> 00:07:33,420
 because a meditation center is pretty much the ideal

129
00:07:33,420 --> 00:07:34,440
 environment.

130
00:07:34,440 --> 00:07:38,390
 You're surrounded by people who are interested in

131
00:07:38,390 --> 00:07:39,440
 meditation,

132
00:07:39,440 --> 00:07:42,440
 who are trying to purify their own minds,

133
00:07:42,440 --> 00:07:45,440
 who are supportive, who are talking about the same things

134
00:07:45,440 --> 00:07:48,440
 and are encouraging each other in the same things.

135
00:07:48,440 --> 00:07:51,440
 You have people talking about the meditation practice

136
00:07:51,440 --> 00:07:53,440
 and teaching the meditation practice.

137
00:07:53,440 --> 00:07:55,440
 You have a really supportive environment

138
00:07:55,440 --> 00:07:58,440
 and that's really important, that makes a real difference.

139
00:07:58,440 --> 00:08:03,440
 I think the people who turn away most often

140
00:08:03,440 --> 00:08:05,440
 are those people who have never had that environment

141
00:08:05,440 --> 00:08:08,440
 or who have not had it on a long term basis.

142
00:08:08,440 --> 00:08:10,440
 So people will go to a retreat for 10 days

143
00:08:10,440 --> 00:08:13,440
 and all of the people come together for 10 days,

144
00:08:13,440 --> 00:08:15,440
 no one's living there, no one's staying there.

145
00:08:15,440 --> 00:08:17,440
 So there isn't the community feeling,

146
00:08:17,440 --> 00:08:22,620
 you don't feel like you're really living in this place, in

147
00:08:22,620 --> 00:08:23,440
 this environment.

148
00:08:23,440 --> 00:08:26,250
 It's quite different when you have a monastery that you go

149
00:08:26,250 --> 00:08:26,440
 to

150
00:08:26,440 --> 00:08:29,440
 and there are people staying there and living there

151
00:08:29,440 --> 00:08:31,440
 and you can live for a month or a year

152
00:08:31,440 --> 00:08:35,440
 and undertake the practice as a lifestyle.

153
00:08:35,440 --> 00:08:38,440
 That's a real great support.

154
00:08:38,440 --> 00:08:41,440
 And you'll see that in addiction therapy as well,

155
00:08:41,440 --> 00:08:44,440
 that it should be residential.

156
00:08:44,440 --> 00:08:47,440
 So the meditation in that sense

157
00:08:47,440 --> 00:08:51,440
 provides an excellent form of addiction therapy

158
00:08:51,440 --> 00:08:55,440
 just by the basic environment.

159
00:08:55,440 --> 00:09:01,100
 Another thing is in regards to the physical addiction

160
00:09:01,100 --> 00:09:01,440
 itself,

161
00:09:01,440 --> 00:09:03,440
 and I've talked about this before,

162
00:09:03,440 --> 00:09:05,440
 that physical addiction is one thing,

163
00:09:05,440 --> 00:09:10,340
 but part of our practice is to not be free from the

164
00:09:10,340 --> 00:09:12,440
 physical addiction,

165
00:09:12,440 --> 00:09:15,800
 but to rise above it, to see that it's only a physical

166
00:09:15,800 --> 00:09:16,440
 reaction.

167
00:09:16,440 --> 00:09:19,220
 The cravings that occur in the mind are actually not c

168
00:09:19,220 --> 00:09:19,440
ravings,

169
00:09:19,440 --> 00:09:22,440
 they're just physical processes.

170
00:09:22,440 --> 00:09:24,440
 So someone who's addicted to nicotine, for example,

171
00:09:24,440 --> 00:09:28,440
 or someone who's addicted to the sexual hormones,

172
00:09:28,440 --> 00:09:31,440
 this is only the physical side.

173
00:09:31,440 --> 00:09:34,440
 It's something that arises and ceases.

174
00:09:34,440 --> 00:09:36,440
 It's actually neutral.

175
00:09:36,440 --> 00:09:41,440
 It's only our deeply ingrained reactions to the physical

176
00:09:41,440 --> 00:09:43,440
 that causes the problem.

177
00:09:43,440 --> 00:09:49,680
 So if we can simply see the feelings, the sensations for

178
00:09:49,680 --> 00:09:50,440
 what they are,

179
00:09:50,440 --> 00:09:53,440
 the sensation of hormones arising,

180
00:09:53,440 --> 00:09:57,440
 whatever the chemical interactions of the hormones

181
00:09:57,440 --> 00:10:02,440
 or of nicotine and the cravings in the brain and so on,

182
00:10:02,440 --> 00:10:05,440
 then they'll cease to have any power over us.

183
00:10:05,440 --> 00:10:09,680
 This is why I said actually the physical is not the real

184
00:10:09,680 --> 00:10:10,440
 problem.

185
00:10:10,440 --> 00:10:15,540
 And so for many people it's actually the lack of

186
00:10:15,540 --> 00:10:17,440
 instruction.

187
00:10:17,440 --> 00:10:22,040
 I know there are often people who will go to a meditation

188
00:10:22,040 --> 00:10:22,440
 center

189
00:10:22,440 --> 00:10:25,440
 and will not get proper instruction for whatever reason.

190
00:10:25,440 --> 00:10:27,440
 Sometimes it's because they don't listen.

191
00:10:27,440 --> 00:10:30,440
 And this comes back to the idea that many people have

192
00:10:30,440 --> 00:10:31,440
 pointed out already

193
00:10:31,440 --> 00:10:33,440
 that you can't help everyone.

194
00:10:33,440 --> 00:10:36,440
 So in the end it is true that,

195
00:10:36,440 --> 00:10:38,440
 and it's very much worth bearing in mind,

196
00:10:38,440 --> 00:10:41,330
 that we should never be frustrated when the people around

197
00:10:41,330 --> 00:10:41,440
 us

198
00:10:41,440 --> 00:10:42,440
 don't want to meditate.

199
00:10:42,440 --> 00:10:45,440
 We should take it upon ourselves to meditate.

200
00:10:45,440 --> 00:10:48,910
 And that will have an effect on our friends and family and

201
00:10:48,910 --> 00:10:49,440
 so on.

202
00:10:49,440 --> 00:10:53,440
 But in the end it's up to the individual.

203
00:10:53,440 --> 00:10:56,180
 And there's so many people in the world who won't ever med

204
00:10:56,180 --> 00:10:56,440
itate,

205
00:10:56,440 --> 00:10:58,440
 not in this life.

206
00:10:58,440 --> 00:11:01,440
 Which is why I said, "Give people what you can,

207
00:11:01,440 --> 00:11:05,440
 and help people as you can, and don't expect too much."

208
00:11:05,440 --> 00:11:13,440
 But on the other hand, if we do give,

209
00:11:13,440 --> 00:11:18,440
 and if we are clear, and if we understand correctly

210
00:11:18,440 --> 00:11:21,440
 what it is that, what is the meditation,

211
00:11:21,440 --> 00:11:24,440
 and how should one practice meditation,

212
00:11:24,440 --> 00:11:26,440
 I've never really had a problem.

213
00:11:26,440 --> 00:11:28,930
 I've never found anyone who didn't gain benefit from the

214
00:11:28,930 --> 00:11:29,440
 practice.

215
00:11:29,440 --> 00:11:32,440
 You have to give up at a certain point and say,

216
00:11:32,440 --> 00:11:35,110
 "That's all the person could gain, and that's enough for

217
00:11:35,110 --> 00:11:35,440
 them."

218
00:11:35,440 --> 00:11:38,230
 And not expect them, or get frustrated when they don't get

219
00:11:38,230 --> 00:11:38,440
 more,

220
00:11:38,440 --> 00:11:40,440
 or when they don't take it more seriously.

221
00:11:40,440 --> 00:11:46,440
 But there has to be someone there to guide them,

222
00:11:46,440 --> 00:11:47,440
 and to instruct them.

223
00:11:47,440 --> 00:11:50,440
 So I think it's important that we study,

224
00:11:50,440 --> 00:11:55,440
 and that we get clear in our own minds about the practice.

225
00:11:55,440 --> 00:12:00,440
 And try our best to give people the information.

226
00:12:00,440 --> 00:12:03,440
 What they do with that information is up to them.

227
00:12:03,440 --> 00:12:06,440
 And I would submit that even just giving them the

228
00:12:06,440 --> 00:12:06,440
 information,

229
00:12:06,440 --> 00:12:08,440
 as I said, is a great thing.

230
00:12:08,440 --> 00:12:12,210
 And I would never say to someone that you shouldn't med

231
00:12:12,210 --> 00:12:12,440
itate,

232
00:12:12,440 --> 00:12:14,440
 you should do something else first.

233
00:12:14,440 --> 00:12:17,200
 I would say there are many other things that you could do

234
00:12:17,200 --> 00:12:17,440
 with the meditation,

235
00:12:17,440 --> 00:12:19,440
 complementing the meditation.

236
00:12:19,440 --> 00:12:21,440
 But meditation should be essential,

237
00:12:21,440 --> 00:12:24,440
 and eventually becomes really the only thing that you need

238
00:12:24,440 --> 00:12:26,440
 once you understand and get it,

239
00:12:26,440 --> 00:12:30,610
 and experience the benefits and the results of the

240
00:12:30,610 --> 00:12:31,440
 meditation.

241
00:12:31,440 --> 00:12:33,440
 Then your mind will incline towards it,

242
00:12:33,440 --> 00:12:36,440
 and you'll find that it more and more becomes your answer

243
00:12:36,440 --> 00:12:39,440
 to just about every problem that you have.

244
00:12:39,440 --> 00:12:42,440
 So, there's an answer to this question.

245
00:12:42,440 --> 00:12:45,440
 That's been another episode of Ask a Monk.

246
00:12:45,440 --> 00:12:49,660
 Thank you all for tuning in, and hope to see you on the

247
00:12:49,660 --> 00:12:50,440
 forum,

248
00:12:50,440 --> 00:12:53,440
 submitting your own questions and your own answers.

249
00:12:53,440 --> 00:12:55,440
 I'd like to thank everyone for submitting answers.

250
00:12:55,440 --> 00:12:59,440
 It certainly makes my job easier to have people who have,

251
00:12:59,440 --> 00:13:03,440
 you know, in this way have studied the meditation.

252
00:13:03,440 --> 00:13:06,440
 It's not my teaching, but studied the Buddha's teaching

253
00:13:06,440 --> 00:13:11,040
 and this tradition of, this interpretation of the Buddha's

254
00:13:11,040 --> 00:13:11,440
 teaching,

255
00:13:11,440 --> 00:13:14,440
 this tradition based on the Buddha's teaching.

256
00:13:14,440 --> 00:13:17,440
 And they're able to use that to help others.

257
00:13:17,440 --> 00:13:19,440
 It's great to see, and I'd encourage you to do that,

258
00:13:19,440 --> 00:13:23,440
 not only here, but also in your own family,

259
00:13:23,440 --> 00:13:27,440
 in your own town, in your own area.

260
00:13:27,440 --> 00:13:30,440
 So, again, thanks for tuning in. All the best.

