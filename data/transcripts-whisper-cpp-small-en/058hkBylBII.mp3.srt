1
00:00:00,000 --> 00:00:05,000
 Good evening, everyone.

2
00:00:05,000 --> 00:00:13,000
 Broadcasting Live, April 3rd, 2016.

3
00:00:13,000 --> 00:00:20,000
 Today's quote, again from the Anguttara Nikaya.

4
00:00:20,000 --> 00:00:27,950
 I think Andy's not doing a very good job of translating, in

5
00:00:27,950 --> 00:00:30,000
 my opinion.

6
00:00:30,000 --> 00:00:42,000
 I don't know where he would get attention from.

7
00:00:42,000 --> 00:00:45,220
 So we have to really ignore this quote, the English,

8
00:00:45,220 --> 00:00:47,000
 because he's done a poor job translating.

9
00:00:47,000 --> 00:00:51,000
 There's no way around it.

10
00:00:51,000 --> 00:01:00,370
 So the dhammas here are the four dhammas that lead to, and

11
00:01:00,370 --> 00:01:02,000
 not progress, they lead to benefit.

12
00:01:02,000 --> 00:01:09,360
 Hitta, the word is hitta, which just means welfare or

13
00:01:09,360 --> 00:01:11,000
 benefit.

14
00:01:11,000 --> 00:01:13,000
 And it's not even worldly progress.

15
00:01:13,000 --> 00:01:21,000
 So he's not even translating, he's very much paraphrasing.

16
00:01:21,000 --> 00:01:25,000
 Chatta rome bhayag apancha.

17
00:01:25,000 --> 00:01:38,000
 He's talking to someone named bhayag apancha.

18
00:01:38,000 --> 00:01:41,000
 I don't know this in the back story, anyway, not important.

19
00:01:41,000 --> 00:01:47,210
 Chatta rome dhamma, there are these four dhammas, kuluputas

20
00:01:47,210 --> 00:01:50,000
adhitta dhammahitaya sangatanti.

21
00:01:50,000 --> 00:01:54,180
 So ditta dhamma, again we have this phrase, ditta means

22
00:01:54,180 --> 00:01:57,000
 seen, dhamma is reality.

23
00:01:57,000 --> 00:02:01,000
 So in the reality that is seen.

24
00:02:01,000 --> 00:02:05,250
 In other words, right now, without delay, without, not in

25
00:02:05,250 --> 00:02:06,000
 the next life.

26
00:02:06,000 --> 00:02:10,000
 Ditta dhamma often means referring to this life.

27
00:02:10,000 --> 00:02:13,000
 So ditta dhammahitaya means benefit in this life.

28
00:02:13,000 --> 00:02:16,510
 So it's not exactly worldly, but the point is, these are

29
00:02:16,510 --> 00:02:22,000
 the things that lead to benefit in this life.

30
00:02:22,000 --> 00:02:29,140
 Because it doesn't take practice of the Buddha's teaching

31
00:02:29,140 --> 00:02:32,000
 to succeed in this life.

32
00:02:32,000 --> 00:02:36,630
 You can actually be a terrible, terrible person and still

33
00:02:36,630 --> 00:02:39,000
 succeed to some extent in this life.

34
00:02:39,000 --> 00:02:43,750
 Your life may be shorter than others, or it may eventually

35
00:02:43,750 --> 00:02:49,000
 turn around, but even evil people.

36
00:02:49,000 --> 00:02:54,990
 So for that reason, benefit in this life that only touches

37
00:02:54,990 --> 00:02:59,000
 this life, it's not so much related to the dhamma.

38
00:02:59,000 --> 00:03:03,000
 And hence the use of the word worldly.

39
00:03:03,000 --> 00:03:05,860
 But that's not exactly what it means. These are important,

40
00:03:05,860 --> 00:03:07,000
 but they're more important.

41
00:03:07,000 --> 00:03:10,000
 They're important in a broad sense.

42
00:03:10,000 --> 00:03:14,000
 The four actually can be applied, generally.

43
00:03:14,000 --> 00:03:17,440
 With some, I mean, they can be applied to a monk, they can

44
00:03:17,440 --> 00:03:19,000
 be applied to meditators.

45
00:03:19,000 --> 00:03:24,510
 It's mostly actually directed to lay people, but, and

46
00:03:24,510 --> 00:03:26,000
 people living in the world.

47
00:03:26,000 --> 00:03:29,000
 But nonetheless, they're good to keep in mind.

48
00:03:29,000 --> 00:03:31,700
 And these are actually one of the core teachings in

49
00:03:31,700 --> 00:03:33,000
 cultural Buddhism.

50
00:03:33,000 --> 00:03:36,000
 Like in Thailand, it's a big one.

51
00:03:36,000 --> 00:03:40,110
 These four are something that we used to learn in schools,

52
00:03:40,110 --> 00:03:42,000
 and the novices learned in schools.

53
00:03:42,000 --> 00:03:46,000
 One of the first things we learned.

54
00:03:46,000 --> 00:03:56,100
 Dhammi, prayot, nailokni, dhammas that have benefit in this

55
00:03:56,100 --> 00:03:57,000
 life.

56
00:03:57,000 --> 00:04:01,000
 Prayot, dohni, prayot, lokna.

57
00:04:01,000 --> 00:04:05,220
 So they pair with another set of dhammas, which are those

58
00:04:05,220 --> 00:04:12,000
 that have samparayaka, hita.

59
00:04:12,000 --> 00:04:17,000
 Samparayaka, hita, something like that.

60
00:04:17,000 --> 00:04:24,000
 Which is the dhammas that have benefit in the next life.

61
00:04:24,000 --> 00:04:27,370
 It's another set of four dhammas. So we'll talk about those

62
00:04:27,370 --> 00:04:28,000
 as well.

63
00:04:28,000 --> 00:04:30,000
 In fact, we even have a third set.

64
00:04:30,000 --> 00:04:33,770
 So we can round it off and talk about all the dhammas that

65
00:04:33,770 --> 00:04:35,000
 have benefit.

66
00:04:35,000 --> 00:04:42,000
 This monk in Bangkok, he used to talk about this a lot.

67
00:04:42,000 --> 00:04:44,000
 Prayot, lokni, prayot, lokna, prayot, sounsoun.

68
00:04:44,000 --> 00:04:48,880
 The third one is sounsoun, which means the very highest

69
00:04:48,880 --> 00:04:50,000
 benefit.

70
00:04:50,000 --> 00:04:59,000
 Prayot, kunipan, that which leads you to niban, nirvana.

71
00:04:59,000 --> 00:05:05,890
 But his first four, utthana sampada, means effort. You have

72
00:05:05,890 --> 00:05:09,000
 to be endowed with effort.

73
00:05:09,000 --> 00:05:14,000
 dita dhamma hita, dita dhamma sukhai. If you want benefit,

74
00:05:14,000 --> 00:05:17,870
 if you're looking for your welfare and happiness in the

75
00:05:17,870 --> 00:05:20,000
 here and now,

76
00:05:20,000 --> 00:05:23,000
 you need effort. You need to work.

77
00:05:23,000 --> 00:05:29,170
 So he explains what that means. You have to make your

78
00:05:29,170 --> 00:05:30,000
 living.

79
00:05:30,000 --> 00:05:33,990
 You have to be skillful and diligent, possessing judgment

80
00:05:33,990 --> 00:05:35,000
 about it

81
00:05:35,000 --> 00:05:38,000
 in order to carry it out and arrange it properly.

82
00:05:38,000 --> 00:05:41,000
 Initiative, bhikkhu bodhi translates as initiative.

83
00:05:41,000 --> 00:05:46,660
 I'm still not convinced, but he means the gumption, taking

84
00:05:46,660 --> 00:05:50,000
 up the task.

85
00:05:50,000 --> 00:05:56,000
 In all things, this is the case. The question is, are you

86
00:05:56,000 --> 00:06:00,000
 making effort in your work?

87
00:06:00,000 --> 00:06:02,530
 So this applies for meditators as well. If you want the

88
00:06:02,530 --> 00:06:07,000
 meditation to be successful,

89
00:06:07,000 --> 00:06:13,000
 you have to work at it. To some extent, we can apply this.

90
00:06:13,000 --> 00:06:27,720
 Right. And this is actually related to, as I said, it was

91
00:06:27,720 --> 00:06:29,000
 being talked to,

92
00:06:29,000 --> 00:06:44,000
 Dika Jahnu, who goes by the clan name Viagapadja. Anyway.

93
00:06:44,000 --> 00:06:48,000
 So he says, "We use sandalwood from Kasi. We wear garlands,

94
00:06:48,000 --> 00:06:49,000
 scents and ungivens.

95
00:06:49,000 --> 00:06:54,000
 We receive gold and silver." Meaning, we're not monks.

96
00:06:54,000 --> 00:06:58,600
 We're not even spiritually religious people. We live in the

97
00:06:58,600 --> 00:07:01,000
 world, we work,

98
00:07:01,000 --> 00:07:04,500
 and we play, and we laugh, and we sing, and we eat, and we

99
00:07:04,500 --> 00:07:07,000
 drink, and make merry.

100
00:07:07,000 --> 00:07:11,320
 But what can we do? He says, "Teach us the Dhamma in a way

101
00:07:11,320 --> 00:07:12,000
 that will lead to our

102
00:07:12,000 --> 00:07:15,370
 welfare and happiness in this present life and in future

103
00:07:15,370 --> 00:07:16,000
 lives."

104
00:07:16,000 --> 00:07:20,580
 "Teach the Dhamma Sukhaya, deach the Dhamma Hittaya." And

105
00:07:20,580 --> 00:07:23,000
 then the other one, so in future lives,

106
00:07:23,000 --> 00:07:29,080
 "Samparaya Hitta." Sorry. "Samparaya Hittaya, Samparaya Su

107
00:07:29,080 --> 00:07:32,000
khaya." It's the other one.

108
00:07:32,000 --> 00:07:35,990
 So for the next life. So the Buddha separates them because

109
00:07:35,990 --> 00:07:37,000
 they're two different things.

110
00:07:37,000 --> 00:07:40,370
 As I said, in this life, it's more related to sort of

111
00:07:40,370 --> 00:07:42,000
 functional things.

112
00:07:42,000 --> 00:07:46,070
 So the first one is you have to work. You have to have the

113
00:07:46,070 --> 00:07:47,000
 initiative.

114
00:07:47,000 --> 00:07:51,000
 You can't just sit around and wait for happiness and

115
00:07:51,000 --> 00:07:53,000
 welfare to come to you.

116
00:07:53,000 --> 00:07:58,000
 If you want to succeed in life, you have to work.

117
00:07:58,000 --> 00:08:03,150
 Number two, "Arakha Sampada." You have to guard or protect

118
00:08:03,150 --> 00:08:07,000
 or save up, right?

119
00:08:07,000 --> 00:08:13,000
 You have to guard your wealth, like which you have amassed.

120
00:08:13,000 --> 00:08:17,150
 You have to think to yourself, "How can I prevent kings and

121
00:08:17,150 --> 00:08:18,000
 thieves from taking it,

122
00:08:18,000 --> 00:08:22,530
 fire from burning it, floods from shaping it off, and

123
00:08:22,530 --> 00:08:25,000
 displeasing heirs from taking it?"

124
00:08:25,000 --> 00:08:31,670
 You know, heirs can be displeasing. Aries, as in people who

125
00:08:31,670 --> 00:08:34,000
 inherit your things,

126
00:08:34,000 --> 00:08:37,930
 maybe they come looking for their inheritance early or

127
00:08:37,930 --> 00:08:39,000
 something.

128
00:08:39,000 --> 00:08:47,800
 So what can I do to sustain my life? Again, a worldly thing

129
00:08:47,800 --> 00:08:51,000
, but it works for all of us.

130
00:08:51,000 --> 00:08:59,000
 I have to concern myself about my robes and my belongings.

131
00:08:59,000 --> 00:09:04,000
 But in regards to meditation, this is important as well.

132
00:09:04,000 --> 00:09:08,560
 Not only do we have to work hard in meditation, but we have

133
00:09:08,560 --> 00:09:11,000
 to be careful.

134
00:09:11,000 --> 00:09:14,750
 Ajahn Tong talks about this similarly, I think, from the

135
00:09:14,750 --> 00:09:16,000
 Visuddhi manga,

136
00:09:16,000 --> 00:09:20,000
 someone rocking a cradle.

137
00:09:20,000 --> 00:09:24,000
 In the olden days, they would have the baby in a hammock,

138
00:09:24,000 --> 00:09:25,000
 maybe, I don't know,

139
00:09:25,000 --> 00:09:29,160
 or in a cradle, I guess, and they have it on a string, so

140
00:09:29,160 --> 00:09:30,000
 they could sit far away

141
00:09:30,000 --> 00:09:35,000
 and they'd just pull on the string to keep the rocker going

142
00:09:35,000 --> 00:09:35,000
.

143
00:09:35,000 --> 00:09:38,120
 And that would keep the baby asleep. But you had to be

144
00:09:38,120 --> 00:09:39,000
 careful.

145
00:09:39,000 --> 00:09:45,000
 If you didn't keep your eye on it, the baby would wake up.

146
00:09:45,000 --> 00:09:49,180
 So the idea is that meditation is like something very

147
00:09:49,180 --> 00:09:50,000
 delicate

148
00:09:50,000 --> 00:09:56,300
 that you have to keep your mind on. If you're not paying

149
00:09:56,300 --> 00:10:00,000
 attention, the baby will wake up.

150
00:10:00,000 --> 00:10:03,810
 You'll lose your mindfulness. It's very easy to get off

151
00:10:03,810 --> 00:10:04,000
 track.

152
00:10:04,000 --> 00:10:07,710
 You see, in meditation again and again, you'll get off

153
00:10:07,710 --> 00:10:08,000
 track

154
00:10:08,000 --> 00:10:11,930
 and you have to bring yourself back again and again,

155
00:10:11,930 --> 00:10:13,000
 guarding.

156
00:10:13,000 --> 00:10:19,000
 So it's analogous to the... it works on that level as well.

157
00:10:19,000 --> 00:10:23,000
 But in terms of anything, you could put this on any level.

158
00:10:23,000 --> 00:10:30,000
 You need to... anything you do, you need to have effort

159
00:10:30,000 --> 00:10:35,720
 and you have to be careful, guard, guard what you have

160
00:10:35,720 --> 00:10:40,000
 gained through your effort.

161
00:10:40,000 --> 00:10:43,870
 So we don't squander. Sometimes people come to meditate

162
00:10:43,870 --> 00:10:44,000
 here

163
00:10:44,000 --> 00:10:47,190
 and then they go out in the world and think they're

164
00:10:47,190 --> 00:10:48,000
 invincible

165
00:10:48,000 --> 00:10:50,840
 because it's easier to be invincible here. There's not so

166
00:10:50,840 --> 00:10:52,000
 much bothering you.

167
00:10:52,000 --> 00:10:55,000
 When you go out there, you realize, "Oh, I didn't...

168
00:10:55,000 --> 00:10:58,000
 maybe I didn't quite get as much out of the meditation as I

169
00:10:58,000 --> 00:10:58,000
 thought.

170
00:10:58,000 --> 00:11:01,000
 Maybe I got this much and you thought you got this much."

171
00:11:01,000 --> 00:11:06,980
 And of course, your expectations are not met because you're

172
00:11:06,980 --> 00:11:09,000
 not mindful.

173
00:11:09,000 --> 00:11:12,000
 Sometimes we go out and it's a shock.

174
00:11:12,000 --> 00:11:16,680
 You wake up, call and you realize you have to guard

175
00:11:16,680 --> 00:11:18,000
 yourself.

176
00:11:18,000 --> 00:11:21,000
 Part of it is negligence.

177
00:11:21,000 --> 00:11:24,000
 But that's the thing, is you can't be negligent.

178
00:11:24,000 --> 00:11:27,210
 If you want to succeed in anything meditate, spiritual or

179
00:11:27,210 --> 00:11:28,000
 worldly,

180
00:11:28,000 --> 00:11:33,000
 you have to be careful.

181
00:11:33,000 --> 00:11:40,000
 Number three is meeting good friends, "kali anamitata."

182
00:11:40,000 --> 00:11:44,000
 This of course goes with everything.

183
00:11:44,000 --> 00:11:47,000
 In Thailand, in meditation, this was a problem.

184
00:11:47,000 --> 00:11:50,200
 Too many people in the monastery and it's easy to get

185
00:11:50,200 --> 00:11:53,000
 caught up with the wrong people.

186
00:11:53,000 --> 00:11:58,000
 Easy to sit down and chat and to get sidetracked.

187
00:11:58,000 --> 00:12:02,000
 And we sidetracked each other as well.

188
00:12:02,000 --> 00:12:05,000
 Luckily here in Hamilton, we have very small centers

189
00:12:05,000 --> 00:12:10,000
 and meditators come and they're not bothered by others.

190
00:12:10,000 --> 00:12:13,000
 But by good friendship, it means people who are practicing,

191
00:12:13,000 --> 00:12:16,000
 who appreciate practice, who teach practice,

192
00:12:16,000 --> 00:12:24,000
 who accommodate you in your practice.

193
00:12:24,000 --> 00:12:33,000
 People who care about your spiritual practice.

194
00:12:33,000 --> 00:12:36,660
 People who can offer you advice and who can give you the

195
00:12:36,660 --> 00:12:37,000
 space you need

196
00:12:37,000 --> 00:12:41,000
 and that kind of thing.

197
00:12:41,000 --> 00:12:45,000
 People who can guide you and be an example for you.

198
00:12:45,000 --> 00:12:48,000
 That's a good friend. The Buddha is our good friend really.

199
00:12:48,000 --> 00:12:51,000
 That's why we think about the Buddha a lot because

200
00:12:51,000 --> 00:12:54,720
 as someone who when you think about him, it gives you a

201
00:12:54,720 --> 00:12:56,000
 good example.

202
00:12:56,000 --> 00:13:00,000
 It gives you something to relate your practice to.

203
00:13:00,000 --> 00:13:05,000
 Someone who has gone the distance.

204
00:13:05,000 --> 00:13:10,000
 Number four is samajivita.

205
00:13:10,000 --> 00:13:15,000
 Samajivita.

206
00:13:15,000 --> 00:13:20,000
 Samaji-vita.

207
00:13:20,000 --> 00:13:27,000
 It should be samajivita.

208
00:13:27,000 --> 00:13:30,000
 Samajivita means living according to your means.

209
00:13:30,000 --> 00:13:33,000
 So this is a fairly worldly one.

210
00:13:33,000 --> 00:13:36,070
 So with friendship, of course, that's obviously important

211
00:13:36,070 --> 00:13:37,000
 in the world as well.

212
00:13:37,000 --> 00:13:42,000
 If you don't have good friends, you won't succeed.

213
00:13:42,000 --> 00:13:44,720
 Having to deal with the stress of enemies and having to

214
00:13:44,720 --> 00:13:46,000
 deal with their problems,

215
00:13:46,000 --> 00:13:47,570
 having to deal with people who lead you in the wrong

216
00:13:47,570 --> 00:13:49,000
 direction.

217
00:13:49,000 --> 00:13:51,000
 It applies in all aspects.

218
00:13:51,000 --> 00:13:57,000
 Samajivita is mostly related to the world. It's explicitly.

219
00:13:57,000 --> 00:13:59,000
 This means living within your means.

220
00:13:59,000 --> 00:14:02,410
 So if you want to succeed in the world, you have to not

221
00:14:02,410 --> 00:14:07,000
 only acquire and protect your wealth,

222
00:14:07,000 --> 00:14:11,000
 but you have to not squander it yourself.

223
00:14:11,000 --> 00:14:15,720
 You have to budget and you have to be content with what you

224
00:14:15,720 --> 00:14:17,000
 have and that kind of thing.

225
00:14:17,000 --> 00:14:19,350
 Because if you live outside your means, this is where

226
00:14:19,350 --> 00:14:20,000
 people go into debt.

227
00:14:20,000 --> 00:14:23,000
 It might seem like a trite sort of statement,

228
00:14:23,000 --> 00:14:27,000
 but it's amazing how many people live outside their means.

229
00:14:27,000 --> 00:14:32,000
 Aren't they able to stop themselves from borrowing money

230
00:14:32,000 --> 00:14:34,000
 and getting into serious debt?

231
00:14:34,000 --> 00:14:41,000
 And they can say, you have to be careful.

232
00:14:41,000 --> 00:14:49,000
 But the simple word we can apply to meditation as well.

233
00:14:49,000 --> 00:14:52,000
 Because I want to really apply this to meditation.

234
00:14:52,000 --> 00:14:55,000
 We want to be talking about what is of most benefit.

235
00:14:55,000 --> 00:14:58,070
 We don't have all that much time to focus too much on

236
00:14:58,070 --> 00:15:02,000
 worldly things, even though they're healthy.

237
00:15:02,000 --> 00:15:05,080
 And also I've got a meditator here listening, so I have to

238
00:15:05,080 --> 00:15:12,000
 help him, give him something to think about.

239
00:15:12,000 --> 00:15:20,630
 But samadhi vita means the way you're living to be balanced

240
00:15:20,630 --> 00:15:21,000
.

241
00:15:21,000 --> 00:15:24,000
 Not too much excess, but not too little.

242
00:15:24,000 --> 00:15:36,300
 So the excess and the lack, if not enough, too much, both

243
00:15:36,300 --> 00:15:38,000
 are problematic.

244
00:15:38,000 --> 00:15:41,000
 If you don't practice enough, if you practice too much,

245
00:15:41,000 --> 00:15:44,120
 you practice without taking a break, it can also be to your

246
00:15:44,120 --> 00:15:45,000
 detriment.

247
00:15:45,000 --> 00:15:51,520
 But mostly living throughout the day balanced with a

248
00:15:51,520 --> 00:15:53,000
 balanced mind,

249
00:15:53,000 --> 00:16:01,000
 balancing your faculties, confidence, effort, mindfulness,

250
00:16:01,000 --> 00:16:02,000
 concentration,

251
00:16:02,000 --> 00:16:08,000
 and wisdom balancing them, and not practicing too hard,

252
00:16:08,000 --> 00:16:11,000
 not practicing too little.

253
00:16:11,000 --> 00:16:18,000
 We know the work we have to do,

254
00:16:18,000 --> 00:16:22,000
 so we do it consistently and systematically.

255
00:16:22,000 --> 00:16:26,000
 That's what's important.

256
00:16:26,000 --> 00:16:29,080
 Anyway, those are the benefits in this world, mostly

257
00:16:29,080 --> 00:16:32,000
 worldly things.

258
00:16:32,000 --> 00:16:38,210
 The ones that lead to benefit in the next life, I think he

259
00:16:38,210 --> 00:16:40,000
 then goes on.

260
00:16:40,000 --> 00:16:45,260
 There are four other things that lead to happiness and

261
00:16:45,260 --> 00:16:47,000
 welfare in future lives.

262
00:16:47,000 --> 00:16:50,610
 So these are a little bit more spiritual, but this is more

263
00:16:50,610 --> 00:16:53,000
 leading to heaven.

264
00:16:53,000 --> 00:16:57,000
 Let's see if I can get the Pali.

265
00:16:57,000 --> 00:17:04,000
 Here we are.

266
00:17:04,000 --> 00:17:09,000
 So the four, these are the samparaya, samparaya hitta,

267
00:17:09,000 --> 00:17:14,000
 samparaya hitta, samparaya sukhaya.

268
00:17:14,000 --> 00:17:16,000
 You need to benefit in the next life.

269
00:17:16,000 --> 00:17:19,000
 So these are more spiritual, and these are, you could say,

270
00:17:19,000 --> 00:17:25,000
 sort of the positive, mundane results.

271
00:17:25,000 --> 00:17:27,740
 Heaven being still mundane, but these are the sort of

272
00:17:27,740 --> 00:17:29,000
 things that lead you to heaven.

273
00:17:29,000 --> 00:17:31,400
 So there's a positive, mundane results of spiritual

274
00:17:31,400 --> 00:17:35,000
 practice, like meditation.

275
00:17:35,000 --> 00:17:38,000
 But actually, these four are not all that mundane at all,

276
00:17:38,000 --> 00:17:41,000
 but they're sort of relating to going to heaven,

277
00:17:41,000 --> 00:17:44,000
 because again, he's telling this to a layman.

278
00:17:44,000 --> 00:17:50,000
 He's not probably meditating, but probably should.

279
00:17:50,000 --> 00:17:53,000
 But anyway, as Buddha gives him these four,

280
00:17:53,000 --> 00:17:57,000
 sanda means confidence, or you could say faith.

281
00:17:57,000 --> 00:18:00,900
 Sada, sampada means you have right faith, faith in the

282
00:18:00,900 --> 00:18:02,000
 right thing,

283
00:18:02,000 --> 00:18:06,380
 faith in the Buddha, the Dhammasanga, faith in spiritual

284
00:18:06,380 --> 00:18:07,000
 teaching,

285
00:18:07,000 --> 00:18:11,000
 faith in good people, faith in good things,

286
00:18:11,000 --> 00:18:15,900
 confidence in yourself, confidence in good things about

287
00:18:15,900 --> 00:18:17,000
 yourself,

288
00:18:17,000 --> 00:18:23,000
 confidence in your ability to cultivate goodness.

289
00:18:23,000 --> 00:18:30,000
 Number two, sila sampada, morality, endowed with morality.

290
00:18:30,000 --> 00:18:40,000
 There are the five precepts, guarding your senses,

291
00:18:40,000 --> 00:18:44,000
 and considering the use of your requisite so that the

292
00:18:44,000 --> 00:18:44,000
 things you use,

293
00:18:44,000 --> 00:18:50,000
 you don't use them for the wrong purposes,

294
00:18:50,000 --> 00:18:57,000
 and right livelihood.

295
00:18:57,000 --> 00:19:02,000
 And number three, jaga sampada, endowed with generosity.

296
00:19:02,000 --> 00:19:08,110
 Or jaga is maybe renunciation or abandoning, giving up, I

297
00:19:08,110 --> 00:19:09,000
 guess.

298
00:19:09,000 --> 00:19:11,000
 We usually translate it as generosity,

299
00:19:11,000 --> 00:19:14,000
 and in a mundane sense that's what it means.

300
00:19:14,000 --> 00:19:18,430
 It means giving gifts and supporting others and being

301
00:19:18,430 --> 00:19:20,000
 charitable.

302
00:19:20,000 --> 00:19:24,000
 But on a deeper level it means giving up.

303
00:19:24,000 --> 00:19:26,000
 And you give up your desire.

304
00:19:26,000 --> 00:19:28,000
 Giving up your desire is actually a great gift,

305
00:19:28,000 --> 00:19:30,000
 because the less desire you have,

306
00:19:30,000 --> 00:19:33,000
 the more you're able to give and give up and help,

307
00:19:33,000 --> 00:19:35,000
 support others and do for others.

308
00:19:35,000 --> 00:19:39,000
 But if you're always obsessed with your own benefit,

309
00:19:39,000 --> 00:19:45,000
 you have a very little time to give to others.

310
00:19:45,000 --> 00:19:49,000
 Number four, upanya sampada, wisdom.

311
00:19:49,000 --> 00:19:51,000
 One should be endowed with wisdom.

312
00:19:51,000 --> 00:19:53,000
 This is a great benefit.

313
00:19:53,000 --> 00:19:55,000
 It's also a benefit for this life,

314
00:19:55,000 --> 00:19:59,290
 but sometimes wise people still may not do that well in

315
00:19:59,290 --> 00:20:00,000
 this life.

316
00:20:00,000 --> 00:20:02,000
 Nonetheless, they do well in their minds.

317
00:20:02,000 --> 00:20:07,000
 Wisdom is the greatest, the most powerful weapon we have,

318
00:20:07,000 --> 00:20:12,000
 the tool we have for benefit, for welfare, for happiness.

319
00:20:12,000 --> 00:20:15,000
 Because with wisdom you know right from wrong.

320
00:20:15,000 --> 00:20:18,000
 Wisdom, you're able to rise above your problems

321
00:20:18,000 --> 00:20:19,000
 when things go wrong.

322
00:20:19,000 --> 00:20:21,000
 You're able to see them as they are,

323
00:20:21,000 --> 00:20:24,000
 and it's not really even about rising above them.

324
00:20:24,000 --> 00:20:27,000
 It's seeing through the cloud of ignorance

325
00:20:27,000 --> 00:20:31,000
 that leads us to see things as problems.

326
00:20:31,000 --> 00:20:33,000
 Because there's no such thing.

327
00:20:33,000 --> 00:20:37,000
 Problem is just a name that we give, a label that we give

328
00:20:37,000 --> 00:20:37,000
 to something.

329
00:20:37,000 --> 00:20:39,000
 The truth is there's experience.

330
00:20:39,000 --> 00:20:43,000
 Wisdom helps us to see that.

331
00:20:43,000 --> 00:20:46,000
 As a result, we don't get attached to things because we,

332
00:20:46,000 --> 00:20:49,000
 out of wisdom we see that that's a problem.

333
00:20:49,000 --> 00:20:51,000
 It doesn't lead to happiness.

334
00:20:51,000 --> 00:20:54,020
 We don't get angry about things because we see that that

335
00:20:54,020 --> 00:20:55,000
 leads to problems.

336
00:20:55,000 --> 00:20:58,280
 When we give up delusion, wisdom is the opposite of

337
00:20:58,280 --> 00:20:59,000
 delusion.

338
00:20:59,000 --> 00:21:01,000
 Wisdom is like the bright light.

339
00:21:01,000 --> 00:21:07,000
 When you shine the bright light, the darkness goes away.

340
00:21:07,000 --> 00:21:09,000
 So the Buddha said,

341
00:21:09,000 --> 00:21:13,000
 "Vinayyalokey abidja dolmanasam."

342
00:21:13,000 --> 00:21:19,520
 Giving up in this world, greed and anger, desire and

343
00:21:19,520 --> 00:21:20,000
 aversion.

344
00:21:20,000 --> 00:21:23,000
 And Ajahn Tong, he said, "Why doesn't he ask?"

345
00:21:23,000 --> 00:21:26,000
 I think the commenter actually says, "Why doesn't?"

346
00:21:26,000 --> 00:21:29,000
 But I remember a talk, Ajahn Tong said,

347
00:21:29,000 --> 00:21:34,000
 "Why doesn't the Buddha mention delusion?"

348
00:21:34,000 --> 00:21:37,000
 It's because mindfulness, this is from the satipatanas,

349
00:21:37,000 --> 00:21:39,000
 mindfulness is like a bright light.

350
00:21:39,000 --> 00:21:42,000
 When you shine it in, the darkness disappears.

351
00:21:42,000 --> 00:21:45,000
 So you don't even have to mention delusion.

352
00:21:45,000 --> 00:21:47,610
 When you're being mindful, you're working to give up greed

353
00:21:47,610 --> 00:21:48,000
 and anger.

354
00:21:48,000 --> 00:21:53,410
 Because you've removed the delusion during that time, there

355
00:21:53,410 --> 00:21:54,000
's wisdom.

356
00:21:54,000 --> 00:22:01,000
 Wisdom can arise about the greed and the anger.

357
00:22:01,000 --> 00:22:06,960
 And so those four are that which leads to benefit in future

358
00:22:06,960 --> 00:22:08,000
 lives.

359
00:22:08,000 --> 00:22:10,810
 And the third group that I said, the one which leads to nir

360
00:22:10,810 --> 00:22:15,000
vana is actually the four satipatanas, I think.

361
00:22:15,000 --> 00:22:19,000
 I'm pretty sure that's what Numbhochodong used to say.

362
00:22:19,000 --> 00:22:22,000
 What are the four that lead to nirvana?

363
00:22:22,000 --> 00:22:24,550
 Nirvana, well, it doesn't say in this suta, this only gives

364
00:22:24,550 --> 00:22:25,000
 the two sets.

365
00:22:25,000 --> 00:22:28,660
 But the third set you have to talk about, if you're looking

366
00:22:28,660 --> 00:22:30,000
 for that, which is the ultimate benefit,

367
00:22:30,000 --> 00:22:31,000
 it's the four satipatanas.

368
00:22:31,000 --> 00:22:34,000
 Because the Buddha said, "Eka yanoi ang bika vai mango."

369
00:22:34,000 --> 00:22:40,000
 This is the one way, the direct way that leads to nirvana.

370
00:22:40,000 --> 00:22:47,000
 And that's the four satipatanas.

371
00:22:47,000 --> 00:22:53,000
 Anyway, so another little bit of dhamma tonight.

372
00:22:53,000 --> 00:22:55,000
 I still have more work to do tonight.

373
00:22:55,000 --> 00:22:56,000
 We're at crunch time.

374
00:22:56,000 --> 00:23:00,000
 Tomorrow, I don't know if I'll be able to broadcast.

375
00:23:00,000 --> 00:23:03,000
 We have the symposium on Tuesday.

376
00:23:03,000 --> 00:23:06,000
 And people have been emailing us.

377
00:23:06,000 --> 00:23:08,000
 Students leave everything to the last moment.

378
00:23:08,000 --> 00:23:12,150
 I should have known that they're all emailing us all days,

379
00:23:12,150 --> 00:23:14,000
 apologizing for waiting for the last minute

380
00:23:14,000 --> 00:23:19,000
 and hoping they can join the symposium.

381
00:23:19,000 --> 00:23:24,000
 Anyway, so that's all for tonight.

382
00:23:24,000 --> 00:23:26,000
 Have a good night, everyone.

383
00:23:28,000 --> 00:23:29,000
 Thank you.

