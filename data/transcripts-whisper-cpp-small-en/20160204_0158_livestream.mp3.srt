1
00:00:00,000 --> 00:00:23,000
 [silence]

2
00:00:23,000 --> 00:00:35,000
 Well, good evening. Broadcasting Live, February 3rd, 2016.

3
00:00:35,000 --> 00:00:48,000
 [silence]

4
00:00:48,000 --> 00:01:01,000
 And today's quote talks about giving yet again.

5
00:01:01,000 --> 00:01:05,020
 Such as me are a lot of these quotes about the good things,

6
00:01:05,020 --> 00:01:14,000
 the good part, the good of giving.

7
00:01:14,000 --> 00:01:30,000
 Well, giving is good, no question.

8
00:01:30,000 --> 00:01:34,000
 It's interesting the gift of food, especially.

9
00:01:34,000 --> 00:01:39,490
 Again, it's rather a self-serving quote because food is

10
00:01:39,490 --> 00:01:45,000
 something that the monks need to get from,

11
00:01:45,000 --> 00:01:56,010
 probably from the audience of this quote, the people

12
00:01:56,010 --> 00:01:59,000
 hearing it.

13
00:01:59,000 --> 00:02:10,310
 But it is an interesting expression of the idea of how

14
00:02:10,310 --> 00:02:13,000
 karma works.

15
00:02:13,000 --> 00:02:18,840
 That some aspect of karma has to do with the good that it

16
00:02:18,840 --> 00:02:21,000
 does for others.

17
00:02:21,000 --> 00:02:32,000
 Not in the sense that the giving to others is efficacious,

18
00:02:32,000 --> 00:02:37,470
 or the benefit of others is efficacious, or is that which

19
00:02:37,470 --> 00:02:40,000
 brings results.

20
00:02:40,000 --> 00:02:48,160
 But there's a sense of deserving and feeling deserving of

21
00:02:48,160 --> 00:02:54,000
 the same sorts of things that you give.

22
00:02:54,000 --> 00:03:05,190
 Karma easily becomes sort of like a spiritual bank account,

23
00:03:05,190 --> 00:03:06,000
 right?

24
00:03:06,000 --> 00:03:12,950
 A spiritual exchange system where you give the things that

25
00:03:12,950 --> 00:03:14,000
 you want

26
00:03:14,000 --> 00:03:18,080
 and you think that somehow you're going to be given them

27
00:03:18,080 --> 00:03:19,000
 almost,

28
00:03:19,000 --> 00:03:26,000
 it easily becomes somehow deistic or even theistic,

29
00:03:26,000 --> 00:03:32,000
 where the universe or some cosmic force.

30
00:03:32,000 --> 00:03:38,790
 That's how theists and deists try to introduce God into

31
00:03:38,790 --> 00:03:40,000
 Buddhism.

32
00:03:40,000 --> 00:03:44,810
 They're saying there's some cosmic force that provides

33
00:03:44,810 --> 00:03:47,000
 retribution,

34
00:03:47,000 --> 00:03:51,000
 which isn't of course not the case.

35
00:03:51,000 --> 00:03:54,130
 There's an orderliness to things, but it has much more to

36
00:03:54,130 --> 00:03:56,000
 do with your mind.

37
00:03:56,000 --> 00:04:06,000
 It has much more to do with your mind.

38
00:04:06,000 --> 00:04:09,020
 I mean there's the cosmic orderliness of the body as well,

39
00:04:09,020 --> 00:04:10,000
 the physical realm as well,

40
00:04:10,000 --> 00:04:20,000
 but the efficacy of the mind is what is important.

41
00:04:20,000 --> 00:04:29,970
 And so a person who is generous to others will feel worthy

42
00:04:29,970 --> 00:04:33,000
 of having others be generous to them.

43
00:04:33,000 --> 00:04:37,100
 That's really how this quote should be understood, quotes

44
00:04:37,100 --> 00:04:38,000
 like it.

45
00:04:38,000 --> 00:04:52,000
 And so that has more broader reaching consequences

46
00:04:52,000 --> 00:04:59,000
 or ramifications that are broader meaning

47
00:04:59,000 --> 00:05:10,000
 in the sense that our actions change us.

48
00:05:10,000 --> 00:05:15,750
 So you can't say like I'm doing something, killing for a

49
00:05:15,750 --> 00:05:18,000
 good cause or so on.

50
00:05:18,000 --> 00:05:22,030
 You say well the good cause justifies that, just justifies

51
00:05:22,030 --> 00:05:23,000
 the evil deed.

52
00:05:23,000 --> 00:05:26,000
 That's a misunderstanding of how karma really works.

53
00:05:26,000 --> 00:05:31,730
 The act of killing, no matter what your good intentions may

54
00:05:31,730 --> 00:05:36,000
 be, it changes you.

55
00:05:36,000 --> 00:05:39,000
 So if someone is stingy, you can notice this.

56
00:05:39,000 --> 00:05:42,000
 It's not across the board of course, it's all psychological

57
00:05:42,000 --> 00:05:42,000
,

58
00:05:42,000 --> 00:05:47,980
 but there's a general relationship between people who are

59
00:05:47,980 --> 00:05:49,000
 stingy

60
00:05:49,000 --> 00:05:57,400
 and people's stinginess and their inability to enjoy their

61
00:05:57,400 --> 00:05:59,000
 possessions themselves.

62
00:05:59,000 --> 00:06:03,940
 The more stingy one becomes, the more unpleasant one's life

63
00:06:03,940 --> 00:06:05,000
 becomes.

64
00:06:05,000 --> 00:06:08,620
 So we have these stories of rich people who are terribly

65
00:06:08,620 --> 00:06:09,000
 stingy

66
00:06:09,000 --> 00:06:15,000
 but then they don't spend anything on themselves either.

67
00:06:15,000 --> 00:06:19,000
 People like this exist in the world.

68
00:06:19,000 --> 00:06:21,000
 They don't have an open heart.

69
00:06:21,000 --> 00:06:26,050
 They aren't generous and kind to others and so they don't

70
00:06:26,050 --> 00:06:29,000
 feel deserving themselves.

71
00:06:29,000 --> 00:06:36,310
 It's not even conscious really, it's almost subconscious

72
00:06:36,310 --> 00:06:39,000
 you could say.

73
00:06:39,000 --> 00:06:43,000
 This is how you get.

74
00:06:43,000 --> 00:06:49,000
 This is probably most, according to Buddhism, this is how,

75
00:06:49,000 --> 00:06:54,000
 if we extrapolate this, then this is how we would explain

76
00:06:54,000 --> 00:06:56,000
 mental illness.

77
00:06:56,000 --> 00:07:01,000
 People who have severe mental illness.

78
00:07:01,000 --> 00:07:07,660
 I mean, talking about karma, often it's kind of unpleasant

79
00:07:07,660 --> 00:07:09,000
 to hear.

80
00:07:09,000 --> 00:07:13,440
 People don't want, it makes people upset and feel a verse

81
00:07:13,440 --> 00:07:14,000
 towards Buddhism

82
00:07:14,000 --> 00:07:18,000
 and feel like it's cruel speech or so on.

83
00:07:18,000 --> 00:07:21,000
 The idea that you deserve it, right?

84
00:07:21,000 --> 00:07:24,000
 A crippled person deserves to be crippled or something like

85
00:07:24,000 --> 00:07:24,000
 that.

86
00:07:24,000 --> 00:07:27,040
 A person with a mental illness deserves to be born with

87
00:07:27,040 --> 00:07:30,000
 schizophrenia or Down syndrome

88
00:07:30,000 --> 00:07:37,500
 and not to just lump everything together but anything that

89
00:07:37,500 --> 00:07:40,000
's terribly unfair.

90
00:07:40,000 --> 00:07:43,320
 You've got to admire the elegance of it, how it makes them

91
00:07:43,320 --> 00:07:44,000
 all fair.

92
00:07:44,000 --> 00:07:50,000
 Internally the system makes everything fair.

93
00:07:50,000 --> 00:07:52,000
 And really that's it really.

94
00:07:52,000 --> 00:08:01,440
 It's not a matter of rejoicing in this fact, it's actually

95
00:08:01,440 --> 00:08:02,000
 quite unfortunate

96
00:08:02,000 --> 00:08:06,000
 that our actions have consequences.

97
00:08:06,000 --> 00:08:11,370
 It's elegant and it makes sense of something that is not

98
00:08:11,370 --> 00:08:14,000
 hard to make sense of.

99
00:08:14,000 --> 00:08:18,680
 Whereas we normally have to appeal to God and then have

100
00:08:18,680 --> 00:08:20,000
 some twisted logic

101
00:08:20,000 --> 00:08:24,570
 about how God is hard to understand and that's why people

102
00:08:24,570 --> 00:08:26,000
 are born crippled.

103
00:08:26,000 --> 00:08:31,000
 It's kind of absurd really.

104
00:08:31,000 --> 00:08:42,050
 My humans are born with cerebral palsy or why some people

105
00:08:42,050 --> 00:08:44,000
 get polio,

106
00:08:44,000 --> 00:08:48,000
 children get polio or these kind of things.

107
00:08:48,000 --> 00:08:53,000
 Why children of thalidomide were born without arms and legs

108
00:08:53,000 --> 00:08:57,000
 and that kind of thing.

109
00:08:57,000 --> 00:09:02,330
 So I think people get angry when they hear the idea that

110
00:09:02,330 --> 00:09:04,000
 you might deserve to be like that

111
00:09:04,000 --> 00:09:12,000
 but it would make a lot more sense if you did.

112
00:09:12,000 --> 00:09:19,000
 Rather than to just call it some random chant.

113
00:09:19,000 --> 00:09:29,000
 But looking specifically at mental illness,

114
00:09:29,000 --> 00:09:32,000
 looking specifically at mental illness, there's a sense

115
00:09:32,000 --> 00:09:39,000
 that it's all the effects of your

116
00:09:39,000 --> 00:09:49,000
 built up habits coming to get back to you in this life.

117
00:09:49,000 --> 00:09:58,000
 And so to detail, the workings of it is beyond me for sure.

118
00:09:58,000 --> 00:10:01,000
 But you can get a sense of it from this kind of a quote,

119
00:10:01,000 --> 00:10:05,510
 and this kind of an idea that a person feels like they

120
00:10:05,510 --> 00:10:08,000
 deserve certain things.

121
00:10:08,000 --> 00:10:11,670
 Not consciously, but they start to get feelings like, as I

122
00:10:11,670 --> 00:10:12,000
 said,

123
00:10:12,000 --> 00:10:18,410
 rich people who are stingy will have a hard time enjoying

124
00:10:18,410 --> 00:10:20,000
 their luxuries.

125
00:10:20,000 --> 00:10:25,000
 Whereas rich people who are kind and generous will not have

126
00:10:25,000 --> 00:10:26,000
 the same feelings of guilt

127
00:10:26,000 --> 00:10:32,150
 and so it will be easier for them to enjoy life and to be

128
00:10:32,150 --> 00:10:37,000
 happy with the riches that they have.

129
00:10:37,000 --> 00:10:44,000
 Be content.

130
00:10:44,000 --> 00:10:56,500
 And so the extrapolation is that a person who is, say,

131
00:10:56,500 --> 00:11:01,000
 obsessive will end up,

132
00:11:01,000 --> 00:11:03,560
 I don't know, just the idea of something that would lead

133
00:11:03,560 --> 00:11:11,000
 someone to be born with OCD or ADHD

134
00:11:11,000 --> 00:11:19,000
 or even worse like schizophrenia and bipolar.

135
00:11:19,000 --> 00:11:27,370
 To have hallucinations, it's interesting where these might

136
00:11:27,370 --> 00:11:29,000
 come from.

137
00:11:29,000 --> 00:11:33,250
 I would say a lot of them don't come directly from evil

138
00:11:33,250 --> 00:11:34,000
 deeds,

139
00:11:34,000 --> 00:11:38,000
 they come from bad habits that you've developed.

140
00:11:38,000 --> 00:11:42,000
 So if you develop a bad habit in your mind in one life,

141
00:11:42,000 --> 00:11:49,440
 a bad habit of obsession or of paranoia, say, or any number

142
00:11:49,440 --> 00:11:50,000
 really,

143
00:11:50,000 --> 00:11:54,000
 it becomes very strong and you die within.

144
00:11:54,000 --> 00:12:00,870
 Then that mind which is now lost and confused and unable to

145
00:12:00,870 --> 00:12:01,000
,

146
00:12:01,000 --> 00:12:09,000
 or is bereft of the constant reaffirmation of who they are

147
00:12:09,000 --> 00:12:10,000
 that comes from the brain

148
00:12:10,000 --> 00:12:13,310
 that's always reminding you through the senses and

149
00:12:13,310 --> 00:12:15,000
 providing some cohesive pattern.

150
00:12:15,000 --> 00:12:18,000
 So not having that.

151
00:12:18,000 --> 00:12:22,560
 Instead the emotions begin to take over instead of the

152
00:12:22,560 --> 00:12:28,000
 familiarity of being a human being, for example.

153
00:12:28,000 --> 00:12:33,860
 And so these emotions destabilize the idea of being human

154
00:12:33,860 --> 00:12:35,000
 or whatever.

155
00:12:35,000 --> 00:12:40,520
 And so you can be born as a human, you start to create the

156
00:12:40,520 --> 00:12:43,000
 new fetus in the womb,

157
00:12:43,000 --> 00:12:47,350
 but have it tainted by one's emotions and one's predile

158
00:12:47,350 --> 00:12:48,000
ctions.

159
00:12:48,000 --> 00:12:51,260
 If they're very strong, they can, I mean, this is

160
00:12:51,260 --> 00:12:54,000
 speculative about how karma probably works,

161
00:12:54,000 --> 00:12:58,000
 it makes sense.

162
00:12:58,000 --> 00:13:01,600
 But therefore you'd be born with a physical illness or even

163
00:13:01,600 --> 00:13:04,000
 a mental illness.

164
00:13:04,000 --> 00:13:06,900
 I mean, most of us are born with mental illnesses. We all

165
00:13:06,900 --> 00:13:08,000
 are.

166
00:13:08,000 --> 00:13:13,940
 I guess you could say it's just a matter of how extreme it

167
00:13:13,940 --> 00:13:14,000
 is

168
00:13:14,000 --> 00:13:23,000
 and whether you find ways to overcome it.

169
00:13:23,000 --> 00:13:27,000
 Interesting, last night we were talking about determinism

170
00:13:27,000 --> 00:13:30,110
 and there's this whole interesting idea of whether karma is

171
00:13:30,110 --> 00:13:33,000
 deterministic or so on.

172
00:13:33,000 --> 00:13:36,460
 It's beyond me about how to explain and how to figure it

173
00:13:36,460 --> 00:13:37,000
 all out,

174
00:13:37,000 --> 00:13:40,000
 but the best I can do is an understanding that determinism

175
00:13:40,000 --> 00:13:47,000
 requires abstraction that is not present in reality.

176
00:13:47,000 --> 00:13:51,560
 And anyone who becomes deterministic has set themselves,

177
00:13:51,560 --> 00:13:58,000
 unfortunately, in a deterministic view.

178
00:13:58,000 --> 00:14:03,000
 I mean, it goes beyond reality, goes beyond experience,

179
00:14:03,000 --> 00:14:09,170
 and therefore has unfortunate consequences. That's the best

180
00:14:09,170 --> 00:14:10,000
 I can do.

181
00:14:10,000 --> 00:14:14,000
 Anyway, tonight's quote, there you have it.

182
00:14:14,000 --> 00:14:19,120
 Today we had another good turnout at our weekly meditation

183
00:14:19,120 --> 00:14:21,000
 session at McMaster.

184
00:14:21,000 --> 00:14:27,250
 We also have three meditators here, Finland, America, two

185
00:14:27,250 --> 00:14:31,000
 Americans and the Finnish man.

186
00:14:31,000 --> 00:14:36,000
 And Aruna is still here. So we have a full house.

187
00:14:36,000 --> 00:14:41,210
 And it looks like we're going to have a full house until,

188
00:14:41,210 --> 00:14:46,000
 well, we're booked up through April, I think.

189
00:14:46,000 --> 00:14:49,310
 It's pretty awesome. And more people calling, people

190
00:14:49,310 --> 00:14:53,000
 wanting to come and I'm having to push them back.

191
00:14:53,000 --> 00:14:56,150
 So our calendar, we're only accepting two meditators at a

192
00:14:56,150 --> 00:14:57,000
 time right now,

193
00:14:57,000 --> 00:15:02,000
 and that's why it's full, because that's very few.

194
00:15:02,000 --> 00:15:05,220
 Now, the neat thing about that is over the course of the

195
00:15:05,220 --> 00:15:08,000
 year, we end up having lots of meditators.

196
00:15:08,000 --> 00:15:11,850
 We can still have lots of meditators, even with limited

197
00:15:11,850 --> 00:15:13,000
 facilities.

198
00:15:13,000 --> 00:15:16,350
 You know, we might want to talk about upping that. We could

199
00:15:16,350 --> 00:15:18,000
 theoretically hold three as we are now,

200
00:15:18,000 --> 00:15:21,680
 but we have to talk about that because I'm afraid that it's

201
00:15:21,680 --> 00:15:24,000
 going to stretch us in.

202
00:15:24,000 --> 00:15:28,250
 We don't have the greatest, the most, we have a great group

203
00:15:28,250 --> 00:15:32,000
, but we don't have the most organized or,

204
00:15:32,000 --> 00:15:36,070
 that's not organization, we don't have the most solid

205
00:15:36,070 --> 00:15:39,000
 organization in the sense of having enough people,

206
00:15:39,000 --> 00:15:44,720
 local and in the organizational committee, who have time

207
00:15:44,720 --> 00:15:47,000
 and energy and ability to,

208
00:15:47,000 --> 00:15:51,000
 and everyone's working and doing this for free and so,

209
00:15:51,000 --> 00:15:54,000
 until we get a stronger organization,

210
00:15:54,000 --> 00:15:59,000
 we just have to see how strong our organization is. Anyway,

211
00:15:59,000 --> 00:16:01,000
 thanks everyone for showing up.

212
00:16:01,000 --> 00:16:06,710
 Really good turnout tonight. Keep practicing. We'll show

213
00:16:06,710 --> 00:16:08,000
 you all the best.

214
00:16:09,000 --> 00:16:34,440
 [

