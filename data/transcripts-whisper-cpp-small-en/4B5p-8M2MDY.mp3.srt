1
00:00:00,000 --> 00:00:02,000
 Good evening.

2
00:00:02,000 --> 00:00:06,390
 So we're looking at the Dhammapada again tonight,

3
00:00:06,390 --> 00:00:12,000
 continuing with verse 193, which reads as follows.

4
00:00:12,000 --> 00:00:20,820
 "Dulabho purisa janyu nasu sambhata jayati yataso jayati de

5
00:00:20,820 --> 00:00:26,000
 ro dankulansukkame dati."

6
00:00:26,000 --> 00:00:36,640
 Which means a thoroughbred man is hard to find, hard to get

7
00:00:36,640 --> 00:00:40,000
, hard to find.

8
00:00:40,000 --> 00:00:46,000
 They are not born everywhere.

9
00:00:47,000 --> 00:00:55,380
 "Dulabho yataso jayati de ro, in a place where such a wise

10
00:00:55,380 --> 00:01:09,000
 person is born, that family finds great happiness."

11
00:01:09,000 --> 00:01:13,090
 So there's not much of a story. This is another one of An

12
00:01:13,090 --> 00:01:15,000
anda's questions.

13
00:01:15,000 --> 00:01:18,250
 Ananda didn't become enlightened until after the Buddha

14
00:01:18,250 --> 00:01:19,000
 passed away.

15
00:01:19,000 --> 00:01:22,280
 So you sometimes wonder whether these questions were a part

16
00:01:22,280 --> 00:01:23,000
 of that.

17
00:01:23,000 --> 00:01:26,840
 I think that's perhaps a little unfair, but he seems to be

18
00:01:26,840 --> 00:01:31,810
 very much interested in gaining all the theoretical

19
00:01:31,810 --> 00:01:35,000
 knowledge he can while the Buddha is still around.

20
00:01:35,000 --> 00:01:38,070
 And I don't think, you can necessarily chastise him for it,

21
00:01:38,070 --> 00:01:40,000
 I don't think the Buddha ever did.

22
00:01:40,000 --> 00:01:43,880
 The Buddha seemed to understand that Ananda wouldn't become

23
00:01:43,880 --> 00:01:50,000
 enlightened, but that his goal and his intent, his path,

24
00:01:50,000 --> 00:01:56,000
 because not all Buddhist practitioners have the same path,

25
00:01:56,000 --> 00:02:00,000
 his path would be one of theoretical study.

26
00:02:00,000 --> 00:02:03,170
 And so he had many questions just to make sure he

27
00:02:03,170 --> 00:02:06,000
 understood and that he knew, not just understood but knew,

28
00:02:06,000 --> 00:02:10,290
 and had in his mind all of these teachings, apparently had

29
00:02:10,290 --> 00:02:12,000
 a very good memory.

30
00:02:12,000 --> 00:02:15,030
 There are people in the world, you hear about them, they

31
00:02:15,030 --> 00:02:18,000
 have these photographic memories or something.

32
00:02:18,000 --> 00:02:22,040
 Apparently Ananda had a very good memory, so he could

33
00:02:22,040 --> 00:02:24,000
 remember anything, but he had to hear it.

34
00:02:24,000 --> 00:02:27,590
 So he asked the Buddha many questions, and his question

35
00:02:27,590 --> 00:02:32,000
 here was, "Where do you find a thoroughbred human?"

36
00:02:32,000 --> 00:02:38,000
 He said, "We know about thoroughbred horses and elephants,

37
00:02:38,000 --> 00:02:41,000
 and they're actually pretty easy to find.

38
00:02:41,000 --> 00:02:45,080
 You can find them anywhere, but where can you find a

39
00:02:45,080 --> 00:02:47,000
 thoroughbred human?"

40
00:02:47,000 --> 00:02:52,270
 So the story says the Buddha took it to mean that Ananda

41
00:02:52,270 --> 00:02:55,000
 was talking about a Buddha.

42
00:02:55,000 --> 00:03:01,310
 So there's two ways we can look at this verse. From the

43
00:03:01,310 --> 00:03:03,000
 story perspective, we're talking about the Buddha.

44
00:03:03,000 --> 00:03:06,430
 Where do you find a Buddha? And it's not easy to find a

45
00:03:06,430 --> 00:03:09,000
 Buddha. It takes a long, long time.

46
00:03:09,000 --> 00:03:13,000
 It's not just two or three days and then you get a Buddha.

47
00:03:13,000 --> 00:03:18,330
 A Buddha is something that requires uncountable lengths of

48
00:03:18,330 --> 00:03:19,000
 time,

49
00:03:19,000 --> 00:03:24,170
 many multiple uncountable lengths of time for a person to

50
00:03:24,170 --> 00:03:26,000
 become a Buddha.

51
00:03:26,000 --> 00:03:31,000
 They're very rare. They don't just arise up anywhere.

52
00:03:31,000 --> 00:03:34,340
 So he talked a little bit about the conditions that are

53
00:03:34,340 --> 00:03:37,000
 required for the arising of a Buddha,

54
00:03:37,000 --> 00:03:41,220
 what sort of family they'll be born into and that sort of

55
00:03:41,220 --> 00:03:42,000
 thing.

56
00:03:42,000 --> 00:03:47,130
 But from the perspective of the verse, there's another

57
00:03:47,130 --> 00:03:50,990
 aspect. Yes, the verse says that it's hard to find a Buddha

58
00:03:50,990 --> 00:03:51,000
,

59
00:03:51,000 --> 00:03:53,720
 but it also says how great or how hard it is to find a

60
00:03:53,720 --> 00:03:55,000
 thoroughbred person.

61
00:03:55,000 --> 00:03:58,140
 It also talks about how great it is for their family and

62
00:03:58,140 --> 00:04:01,710
 just how great it is and how much happiness comes from a

63
00:04:01,710 --> 00:04:05,000
 person who is thoroughbred.

64
00:04:05,000 --> 00:04:09,080
 And it makes us think of the Buddha's teaching on thorough

65
00:04:09,080 --> 00:04:10,000
bred humans.

66
00:04:10,000 --> 00:04:12,840
 And it seems pretty clear from that teaching that the

67
00:04:12,840 --> 00:04:16,440
 Buddha didn't reserve this teaching only for Buddhas, this

68
00:04:16,440 --> 00:04:18,000
 designation.

69
00:04:18,000 --> 00:04:20,670
 So just because someone isn't a Buddha doesn't mean they

70
00:04:20,670 --> 00:04:22,000
 can't be thoroughbred.

71
00:04:22,000 --> 00:04:24,000
 So there's two things I want to talk about here.

72
00:04:24,000 --> 00:04:29,400
 The first one, coming off the story perspective, is a

73
00:04:29,400 --> 00:04:32,000
 little bit of humility

74
00:04:32,000 --> 00:04:36,000
 because it's easy to think that you're special.

75
00:04:36,000 --> 00:04:41,050
 And in Buddhism, this manifests itself as people thinking

76
00:04:41,050 --> 00:04:45,000
 that they're qualified to become a Buddha.

77
00:04:45,000 --> 00:04:48,730
 And I think, I'm certainly not a person to put someone in

78
00:04:48,730 --> 00:04:52,000
 their place and say, "You can't become a Buddha."

79
00:04:52,000 --> 00:04:56,080
 But I think the general consensus in our tradition is that

80
00:04:56,080 --> 00:05:00,250
 a lot more people want to become Buddhas than will ever

81
00:05:00,250 --> 00:05:03,000
 actually attain Buddhahood.

82
00:05:03,000 --> 00:05:10,000
 So Kondanya was, no, Kondanya, what's his name?

83
00:05:10,000 --> 00:05:16,410
 Kachayana, I think. I think Kachayana, one of the Buddha's

84
00:05:16,410 --> 00:05:21,000
 chief disciples, had made a vow to become a Buddha.

85
00:05:21,000 --> 00:05:23,000
 It wasn't him, it was someone.

86
00:05:23,000 --> 00:05:27,090
 And when he came to this life and saw the Buddha, he

87
00:05:27,090 --> 00:05:30,120
 thought to himself, "Oh boy, that's just too pure and too

88
00:05:30,120 --> 00:05:31,000
 perfect.

89
00:05:31,000 --> 00:05:34,210
 I'm not capable of that." And he gave it up and became a

90
00:05:34,210 --> 00:05:35,000
 monk.

91
00:05:35,000 --> 00:05:38,000
 But a lot of people just have the idea to become a Buddha.

92
00:05:38,000 --> 00:05:44,530
 And I think it can be based on this sort of conceit of

93
00:05:44,530 --> 00:05:49,000
 thinking that you're special.

94
00:05:49,000 --> 00:05:53,580
 Thinking that you have some special quality that makes you

95
00:05:53,580 --> 00:05:56,000
 better than everyone else.

96
00:05:56,000 --> 00:05:59,960
 And so when I read this first, one of the things I think of

97
00:05:59,960 --> 00:06:03,000
 is it's a reminder that I'm not a Buddha.

98
00:06:03,000 --> 00:06:06,970
 I'm not this kind of a thoroughbred. I'm not anything

99
00:06:06,970 --> 00:06:08,000
 special.

100
00:06:08,000 --> 00:06:11,000
 I think that's potentially useful.

101
00:06:11,000 --> 00:06:13,480
 Of course you have to be careful not to be too hard on

102
00:06:13,480 --> 00:06:18,220
 yourself or think of yourself as bad or evil or corrupt or

103
00:06:18,220 --> 00:06:21,000
 just useless or pointless.

104
00:06:21,000 --> 00:06:27,000
 But on the other hand, we have to find freedom.

105
00:06:27,000 --> 00:06:31,270
 We have to, to some extent, see how worthless and useless

106
00:06:31,270 --> 00:06:36,000
 we are, our bodies and our minds.

107
00:06:36,000 --> 00:06:40,000
 Because part of letting go is seeing how useless it all is.

108
00:06:40,000 --> 00:06:44,310
 Part of becoming a Buddha even is to realize how useless

109
00:06:44,310 --> 00:06:48,000
 the person who is the Buddha actually is.

110
00:06:48,000 --> 00:06:52,810
 Meaning in an ultimate sense, the person, the being, the

111
00:06:52,810 --> 00:06:55,000
 identity,

112
00:06:55,000 --> 00:06:58,740
 and even the physical and mental manifestations are in and

113
00:06:58,740 --> 00:07:01,000
 of themselves of no value.

114
00:07:01,000 --> 00:07:07,000
 They have nothing that you should cherish or hold on to.

115
00:07:07,000 --> 00:07:12,840
 So reminding ourselves that we are just ordinary and that

116
00:07:12,840 --> 00:07:18,000
 our minds are chaotic and unwieldy,

117
00:07:18,000 --> 00:07:22,480
 reminding ourselves that we are not something special can

118
00:07:22,480 --> 00:07:24,000
 be quite useful.

119
00:07:24,000 --> 00:07:27,130
 It's useful to come down to earth and to not hold yourself

120
00:07:27,130 --> 00:07:33,630
 up over others or to think of yourself as something special

121
00:07:33,630 --> 00:07:34,000
.

122
00:07:34,000 --> 00:07:39,000
 If you want to become a Buddha, you have to let go of that.

123
00:07:39,000 --> 00:07:43,160
 If you want to become enlightened, you also have to let go

124
00:07:43,160 --> 00:07:44,000
 of that.

125
00:07:44,000 --> 00:07:53,280
 But on the other side, it's important to be encouraged and

126
00:07:53,280 --> 00:07:58,500
 it's important to remember the greatness of the Buddhist

127
00:07:58,500 --> 00:07:59,000
 teaching,

128
00:07:59,000 --> 00:08:02,210
 of the practice, the greatness of mindfulness, the

129
00:08:02,210 --> 00:08:04,000
 greatness of purity of mind,

130
00:08:04,000 --> 00:08:09,000
 the greatness of greatness, the greatness of goodness.

131
00:08:09,000 --> 00:08:12,630
 And so we talk about what is the difference between a

132
00:08:12,630 --> 00:08:17,000
 thoroughbred and just an ordinary human being,

133
00:08:17,000 --> 00:08:22,000
 an untrainable sort of, if you relate it to animals.

134
00:08:22,000 --> 00:08:26,040
 You have these animals that are thoroughbred, like a horse

135
00:08:26,040 --> 00:08:27,000
 or a dog.

136
00:08:27,000 --> 00:08:31,150
 I don't have much familiarity with elephants, thoroughbred

137
00:08:31,150 --> 00:08:34,000
 elephants, but purebred elephants.

138
00:08:34,000 --> 00:08:38,700
 Purebred dogs are somewhat exceptional. They have an

139
00:08:38,700 --> 00:08:41,750
 intelligence that's, I guess they say, bring bread into

140
00:08:41,750 --> 00:08:42,000
 them,

141
00:08:42,000 --> 00:08:45,690
 but you could also think of it as something to do with the

142
00:08:45,690 --> 00:08:49,220
 human connection and how humans are born as animals and

143
00:08:49,220 --> 00:08:51,000
 animals are born as humans.

144
00:08:51,000 --> 00:08:55,820
 They have a strong connection to being human or to just

145
00:08:55,820 --> 00:08:59,000
 intelligence or high-mindedness.

146
00:08:59,000 --> 00:09:04,000
 And so they're very easy to train.

147
00:09:04,000 --> 00:09:08,520
 And so we make the simile, the analogy with human beings,

148
00:09:08,520 --> 00:09:11,000
 comparison to human beings.

149
00:09:11,000 --> 00:09:14,450
 What sort of human being is easy to train? What sort of a

150
00:09:14,450 --> 00:09:18,410
 human being, when they put their mind to it, can achieve

151
00:09:18,410 --> 00:09:21,000
 greatness?

152
00:09:21,000 --> 00:09:26,040
 And the Buddha talked about, in particular, in relation to

153
00:09:26,040 --> 00:09:29,000
 the path, he talked about a horse,

154
00:09:29,000 --> 00:09:36,720
 a horse that knows the way to go, a horse that is

155
00:09:36,720 --> 00:09:39,000
 responsive.

156
00:09:39,000 --> 00:09:43,240
 So there are different kinds of horse and different levels

157
00:09:43,240 --> 00:09:45,000
 of capacity of horses.

158
00:09:45,000 --> 00:09:51,670
 The rider pulls up the goat, the stick, and they just have

159
00:09:51,670 --> 00:09:57,200
 to show it in the right direction, and the horse knows the

160
00:09:57,200 --> 00:10:01,000
 horse knows to go.

161
00:10:01,000 --> 00:10:04,000
 The second type of horse, you actually have to touch them.

162
00:10:04,000 --> 00:10:05,570
 That would be a very special horse. We didn't even have to

163
00:10:05,570 --> 00:10:06,000
 touch them.

164
00:10:06,000 --> 00:10:10,140
 They knew right away, go left, go right, go faster, go

165
00:10:10,140 --> 00:10:11,000
 slower.

166
00:10:11,000 --> 00:10:15,380
 But the second one, you have to touch them. And when they

167
00:10:15,380 --> 00:10:17,000
 feel the touch, not a painful touch, but just a touch,

168
00:10:17,000 --> 00:10:22,000
 and they know which way to go, to speed up, to slow down.

169
00:10:22,000 --> 00:10:25,250
 Third type of horse, you actually have to make it hurt. It

170
00:10:25,250 --> 00:10:28,000
 doesn't hurt, the horse won't go.

171
00:10:28,000 --> 00:10:29,920
 I don't know, it sounds kind of cruel. I'm not giving

172
00:10:29,920 --> 00:10:32,000
 directions on how to deal with horses.

173
00:10:32,000 --> 00:10:35,330
 I think probably horse riding has some bad karma associated

174
00:10:35,330 --> 00:10:36,000
 with it.

175
00:10:36,000 --> 00:10:39,380
 With similes you can talk about these sorts of things, like

176
00:10:39,380 --> 00:10:42,000
 hunters, and so on, that sort of thing.

177
00:10:42,000 --> 00:10:49,050
 But so, you hit it until it hurts. When it hurts, then the

178
00:10:49,050 --> 00:10:50,000
 horse knows to go.

179
00:10:50,000 --> 00:10:55,150
 The fourth type of horse, you really have to get to the

180
00:10:55,150 --> 00:10:56,000
 bone.

181
00:10:56,000 --> 00:11:01,220
 You really have to like, really make it injured, or bruised

182
00:11:01,220 --> 00:11:02,000
 even.

183
00:11:02,000 --> 00:11:07,010
 You bruise it, if you make it really hurt, then the horse

184
00:11:07,010 --> 00:11:08,000
 will go.

185
00:11:08,000 --> 00:11:11,000
 Go this way, go that way, and so on.

186
00:11:11,000 --> 00:11:14,000
 I hope I'm not getting these wrong, it's been a long time,

187
00:11:14,000 --> 00:11:16,190
 but I think that's the fourth one, you really have to hit

188
00:11:16,190 --> 00:11:18,000
 to the bone, I think.

189
00:11:18,000 --> 00:11:22,000
 Yeah, that's what it is.

190
00:11:22,000 --> 00:11:24,560
 Of course, the fifth type of horse, you can hit them until

191
00:11:24,560 --> 00:11:29,380
 they die, and you hit them until your goat breaks, or the

192
00:11:29,380 --> 00:11:30,000
 stick breaks,

193
00:11:30,000 --> 00:11:35,580
 or the horse falls down, and it will never go the right way

194
00:11:35,580 --> 00:11:36,000
.

195
00:11:36,000 --> 00:11:43,240
 So that rather violent list of horses, how does that relate

196
00:11:43,240 --> 00:11:47,000
 to humans, how does that relate to us, to meditators?

197
00:11:47,000 --> 00:11:50,920
 It's nothing like the, I'm not going to hit anyone with a

198
00:11:50,920 --> 00:11:54,000
 stick, this isn't that sort of teaching.

199
00:11:54,000 --> 00:12:00,000
 No, the stick here is death and suffering.

200
00:12:00,000 --> 00:12:03,640
 Some people, just when they hear about it, if you remember

201
00:12:03,640 --> 00:12:08,000
 the Buddha, the bodhisattva, when he heard about,

202
00:12:08,000 --> 00:12:12,000
 well actually that's not true, but they had never heard of,

203
00:12:12,000 --> 00:12:16,200
 the legend goes that he had never heard of old age sickness

204
00:12:16,200 --> 00:12:17,000
 or death.

205
00:12:17,000 --> 00:12:20,290
 But the first type of person anyway, just when they hear

206
00:12:20,290 --> 00:12:23,000
 about it, they actually tried to not let him know about it,

207
00:12:23,000 --> 00:12:25,930
 that's how the legend goes, I don't know if it's actually

208
00:12:25,930 --> 00:12:28,000
 true, but that's what they say.

209
00:12:28,000 --> 00:12:32,420
 But when you hear about it, hear about people dying, like

210
00:12:32,420 --> 00:12:35,000
 in a tsunami or an earthquake,

211
00:12:35,000 --> 00:12:38,200
 or you hear about murders, these mass murders now that we

212
00:12:38,200 --> 00:12:42,000
 hear about people just going in and shooting people,

213
00:12:42,000 --> 00:12:45,640
 because they're different colored skin, or because they're

214
00:12:45,640 --> 00:12:51,000
 gay, or because they're different religion, or whatever.

215
00:12:51,000 --> 00:12:56,360
 And you hear about that, you hear about, not about the evil

216
00:12:56,360 --> 00:12:59,000
 of the killing, but about the death and the suffering,

217
00:12:59,000 --> 00:13:04,500
 and you think, wow, that really is the nature of life, that

218
00:13:04,500 --> 00:13:08,000
's in the realm of possibility.

219
00:13:08,000 --> 00:13:13,270
 I am not at all prepared to deal with such reality, which

220
00:13:13,270 --> 00:13:17,000
 means I'm out of touch with reality, I am not safe,

221
00:13:17,000 --> 00:13:22,970
 I am not where I should be, I have not attained what needs

222
00:13:22,970 --> 00:13:25,000
 to be attained.

223
00:13:25,000 --> 00:13:28,430
 Some people think like this, and they really get a sense of

224
00:13:28,430 --> 00:13:32,000
 urgency and work and become enlightened

225
00:13:32,000 --> 00:13:40,470
 and learn the way to free themselves from the danger of old

226
00:13:40,470 --> 00:13:44,000
 age sickness and death.

227
00:13:44,000 --> 00:13:47,750
 I'm suffering just from hearing about it. That's like the

228
00:13:47,750 --> 00:13:50,000
 horse that you don't even have to hit them with it.

229
00:13:50,000 --> 00:13:54,000
 It hasn't hit them at all.

230
00:13:54,000 --> 00:13:58,480
 The second type of person they have to see someone, or they

231
00:13:58,480 --> 00:14:01,000
 have to actually come in contact with someone

232
00:14:01,000 --> 00:14:07,470
 who is suffering and sick and old or dying or dead, they

233
00:14:07,470 --> 00:14:11,290
 have to actually see it, or come in contact with it in some

234
00:14:11,290 --> 00:14:12,000
 way.

235
00:14:12,000 --> 00:14:16,750
 And when they do, then they realize this could happen to me

236
00:14:16,750 --> 00:14:20,000
, and it's just this is the nature of reality.

237
00:14:20,000 --> 00:14:24,850
 Suffering is a part of life. Here I have been negligent,

238
00:14:24,850 --> 00:14:31,000
 engaging in mindless, meaningless pleasures and so on.

239
00:14:31,000 --> 00:14:34,650
 The third type of person, it has to be someone close to

240
00:14:34,650 --> 00:14:39,790
 them. When their parents die, or relatives or friends or

241
00:14:39,790 --> 00:14:42,000
 loved ones,

242
00:14:42,000 --> 00:14:45,510
 when they get old, sick and die, then they see, like I said

243
00:14:45,510 --> 00:14:49,650
 before, it's like the armies of Mara have attacked your

244
00:14:49,650 --> 00:14:50,000
 fortress

245
00:14:50,000 --> 00:14:53,660
 and the walls are crumbling, it's like you see the people

246
00:14:53,660 --> 00:14:55,000
 dying around you.

247
00:14:55,000 --> 00:14:58,830
 It's like an army has Mahasthaya Nida Matruna, who have

248
00:14:58,830 --> 00:15:05,050
 been attacked by the great armies of Mara, the great armies

249
00:15:05,050 --> 00:15:07,000
 of death.

250
00:15:07,000 --> 00:15:12,500
 And the fourth type of person, well, they don't get it even

251
00:15:12,500 --> 00:15:16,000
 when people around them are dying.

252
00:15:16,000 --> 00:15:21,390
 They have to themselves be old, sick or dying, have to be

253
00:15:21,390 --> 00:15:26,280
 in great pain or suffering, and then they realize it, then

254
00:15:26,280 --> 00:15:28,000
 they realize something needs to be done.

255
00:15:28,000 --> 00:15:32,570
 So it's quite desperate at that point, but they are still

256
00:15:32,570 --> 00:15:35,000
 considered to be trainable, right?

257
00:15:35,000 --> 00:15:38,000
 Because they realize that something needs to be done, then

258
00:15:38,000 --> 00:15:39,000
 they go and do it.

259
00:15:39,000 --> 00:15:44,750
 They take up the practice of training their mind, purifying

260
00:15:44,750 --> 00:15:48,520
 their mind, cleansing their mind of all the clinging and

261
00:15:48,520 --> 00:15:51,000
 attachment that might cause them suffering.

262
00:15:51,000 --> 00:15:54,000
 And they're able to free themselves.

263
00:15:54,000 --> 00:15:56,700
 The fifth type of human being, what's that one? Well, they

264
00:15:56,700 --> 00:16:00,400
 get old, sick and die, and even on their deathbed, they're

265
00:16:00,400 --> 00:16:05,040
 still drinking and smoking or cursing people or clinging to

266
00:16:05,040 --> 00:16:06,000
 things,

267
00:16:06,000 --> 00:16:10,000
 still up into their last moment, their last breath.

268
00:16:10,000 --> 00:16:15,220
 They die in a bad way. Even when they're dying, they don't

269
00:16:15,220 --> 00:16:16,000
 realize.

270
00:16:16,000 --> 00:16:20,180
 And part of it is just our culture, you know, we're taught

271
00:16:20,180 --> 00:16:21,000
 ways to be.

272
00:16:21,000 --> 00:16:23,450
 If you had only one day to live, what would you do? Go on a

273
00:16:23,450 --> 00:16:25,000
 party, you know?

274
00:16:25,000 --> 00:16:30,000
 Do all these skydive, you know? Go traveling, see things,

275
00:16:30,000 --> 00:16:32,000
 see all the things outside of yourself,

276
00:16:32,000 --> 00:16:38,950
 none of which will prepare you and make you better equipped

277
00:16:38,950 --> 00:16:44,000
 to deal with this part of life that is death.

278
00:16:44,000 --> 00:16:50,000
 None of it will lead to greater things.

279
00:16:50,000 --> 00:16:54,130
 So someone who does, someone who takes up the practice, for

280
00:16:54,130 --> 00:16:58,130
 one of these reasons, is considered to be a thoroughbred,

281
00:16:58,130 --> 00:16:59,000
 purebred.

282
00:16:59,000 --> 00:17:04,000
 It's a sign of greatness. So it's something to be proud of

283
00:17:04,000 --> 00:17:07,000
 and encouraged by.

284
00:17:07,000 --> 00:17:10,000
 Just not to let it go to your head, I think.

285
00:17:10,000 --> 00:17:14,000
 The other reminder that we're not Buddha, that we're just,

286
00:17:14,000 --> 00:17:18,000
 we're actually rejects at this point, is quite humbling.

287
00:17:18,000 --> 00:17:21,620
 If we had been great people, we would have probably become

288
00:17:21,620 --> 00:17:25,230
 enlightened when the Buddha was here, but we were around

289
00:17:25,230 --> 00:17:28,000
 back then, probably humans,

290
00:17:28,000 --> 00:17:32,200
 maybe animals, who knows, we could have been anywhere, but

291
00:17:32,200 --> 00:17:36,000
 we missed it, we missed our opportunity and here we are.

292
00:17:36,000 --> 00:17:41,920
 We're like the beggars outside of the palace walls taking

293
00:17:41,920 --> 00:17:49,000
 scraps, 2500 years later.

294
00:17:49,000 --> 00:17:53,000
 And the other part of that is how great it is, right?

295
00:17:53,000 --> 00:17:58,900
 So the greatness and the benefit and the happiness, the

296
00:17:58,900 --> 00:18:01,470
 Buddha, sukham, sukhamedati, leads to great happiness when

297
00:18:01,470 --> 00:18:03,000
 there is such a person.

298
00:18:03,000 --> 00:18:07,010
 It's happiness for their family, happiness for their

299
00:18:07,010 --> 00:18:08,000
 society.

300
00:18:08,000 --> 00:18:13,760
 I think this is something that is a good reminder and would

301
00:18:13,760 --> 00:18:18,290
 be a great thing for people to realize, for families to

302
00:18:18,290 --> 00:18:19,000
 realize,

303
00:18:19,000 --> 00:18:22,850
 when they are hard on their children or pushing their

304
00:18:22,850 --> 00:18:27,390
 children into making money or finding worldly success or

305
00:18:27,390 --> 00:18:29,000
 stability,

306
00:18:29,000 --> 00:18:32,090
 getting good grades and how they are disappointed in their

307
00:18:32,090 --> 00:18:35,950
 kids if they don't do well in school or if they change

308
00:18:35,950 --> 00:18:42,000
 their religion or if they happen to be homosexual or if

309
00:18:42,000 --> 00:18:42,000
 they,

310
00:18:42,000 --> 00:18:46,340
 any number of things, that transgender is a big thing now,

311
00:18:46,340 --> 00:18:48,000
 many much hate there.

312
00:18:48,000 --> 00:18:52,000
 How disappointed people are because of things like that.

313
00:18:52,000 --> 00:18:57,470
 Anyway, I'm not getting into all the many types of

314
00:18:57,470 --> 00:19:01,800
 disappointment that parents have and all of the

315
00:19:01,800 --> 00:19:07,490
 expectations for worldly success and stability and rect

316
00:19:07,490 --> 00:19:08,000
itude,

317
00:19:08,000 --> 00:19:16,740
 moral rectitude in various ways, religion and sexuality and

318
00:19:16,740 --> 00:19:18,000
 so on.

319
00:19:18,000 --> 00:19:21,880
 All of these things that lead parents to denounce their

320
00:19:21,880 --> 00:19:24,000
 children and throw them out.

321
00:19:24,000 --> 00:19:30,350
 And all of the things that parents look for in their

322
00:19:30,350 --> 00:19:37,060
 children and push their children into, getting a good job

323
00:19:37,060 --> 00:19:41,000
 and making money and getting married.

324
00:19:41,000 --> 00:19:44,260
 For many cultures, marriage is a very important thing. You

325
00:19:44,260 --> 00:19:48,000
 must get married, that's a measure of success.

326
00:19:48,000 --> 00:19:51,170
 And if only people realized what was a true greatness and

327
00:19:51,170 --> 00:19:54,760
 what would really be a truly great thing for your children

328
00:19:54,760 --> 00:19:58,560
 to do would be for them to practice and free themselves

329
00:19:58,560 --> 00:19:59,000
 from suffering,

330
00:19:59,000 --> 00:20:03,950
 to purify their minds and to train themselves, to realize

331
00:20:03,950 --> 00:20:09,830
 that all of that useless, meaningless stuff is useless and

332
00:20:09,830 --> 00:20:12,000
 meaningless.

333
00:20:12,000 --> 00:20:19,280
 That is of no consequence. And all the energy people put

334
00:20:19,280 --> 00:20:26,590
 into pushing their children and forcing their children to

335
00:20:26,590 --> 00:20:31,000
 find success is all wasted and misguided.

336
00:20:31,000 --> 00:20:38,080
 And there's so much greatness that comes not from money or

337
00:20:38,080 --> 00:20:44,490
 worldly success but from a child or a person who follows

338
00:20:44,490 --> 00:20:49,570
 the path and finds their own way to free themselves from

339
00:20:49,570 --> 00:20:55,300
 all clinging and craving and anger and conceit and ego and

340
00:20:55,300 --> 00:20:56,000
 so on.

341
00:20:56,000 --> 00:20:59,720
 How much greatness comes not just to them but to their

342
00:20:59,720 --> 00:21:02,000
 family and to their society.

343
00:21:02,000 --> 00:21:05,610
 They become a morally upstanding citizen, they become

344
00:21:05,610 --> 00:21:10,160
 someone who is able to give advice, someone who guides

345
00:21:10,160 --> 00:21:15,000
 others and supports others in their own spiritual practice,

346
00:21:15,000 --> 00:21:17,000
 someone who is harmless,

347
00:21:17,000 --> 00:21:24,730
 who is not cruel or unkind, someone who is generous and

348
00:21:24,730 --> 00:21:31,570
 wise. It's such a great thing to have in society that this

349
00:21:31,570 --> 00:21:37,000
 is just a reminder of what are the important things in life

350
00:21:37,000 --> 00:21:40,000
 and what are the important qualities that we should aspire

351
00:21:40,000 --> 00:21:46,000
 for in ourselves and aspire for in the people around us.

352
00:21:46,000 --> 00:21:51,650
 Hard to find, but wherever you do find such a person, such

353
00:21:51,650 --> 00:21:57,000
 a wise person, great happiness comes to everyone.

354
00:21:57,000 --> 00:22:02,000
 So that's the verse. That's the Dhammapada for tonight.

355
00:22:02,000 --> 00:22:04,000
 Thank you all for listening.

