1
00:00:00,000 --> 00:00:19,000
 So last night we talked about the benefits or the goals of

2
00:00:19,000 --> 00:00:23,800
 the practice of meditation.

3
00:00:23,800 --> 00:00:36,660
 There are some pretty lofty aims though. Some pretty hefty

4
00:00:36,660 --> 00:00:41,800
 goals to aspire towards.

5
00:00:41,800 --> 00:00:52,130
 The purification of the mind, overcoming all mental defile

6
00:00:52,130 --> 00:00:53,800
ments.

7
00:00:53,800 --> 00:01:00,450
 So the next question is, what can we do or how do we

8
00:01:00,450 --> 00:01:05,800
 progress to attain these goals?

9
00:01:05,800 --> 00:01:11,380
 What do we mean by progress? How do we measure our progress

10
00:01:11,380 --> 00:01:11,800
?

11
00:01:11,800 --> 00:01:20,210
 This is a question that meditators always want an answer to

12
00:01:20,210 --> 00:01:20,800
.

13
00:01:20,800 --> 00:01:39,890
 How to tell how far we progress? Am I on the right track?

14
00:01:39,890 --> 00:01:45,800
 Am I doing well?

15
00:01:45,800 --> 00:01:51,950
 If we want to know how successful we are, we can look at

16
00:01:51,950 --> 00:01:56,800
 the goals and see how close we are to the goals.

17
00:01:56,800 --> 00:02:00,800
 But sometimes it's not clear. Sometimes we don't know.

18
00:02:00,800 --> 00:02:08,120
 We can't see how much purity and how much defilement we

19
00:02:08,120 --> 00:02:10,800
 have in the mind.

20
00:02:10,800 --> 00:02:14,890
 And even if we can't see, we still have to know how to go

21
00:02:14,890 --> 00:02:18,800
 forward, how to move closer to the goal.

22
00:02:18,800 --> 00:02:32,060
 What do we do that... what is it that we do that brings us

23
00:02:32,060 --> 00:02:36,800
 closer to the goal?

24
00:02:36,800 --> 00:02:44,500
 So we have to understand the path. We have to understand

25
00:02:44,500 --> 00:02:47,800
 the path of progress.

26
00:02:47,800 --> 00:02:51,010
 One way of explaining and understanding the Buddha's

27
00:02:51,010 --> 00:02:52,800
 teaching, the path of the Buddha,

28
00:02:52,800 --> 00:03:02,220
 coming closer to the core of existence or the essential

29
00:03:02,220 --> 00:03:05,800
 quality of existence,

30
00:03:05,800 --> 00:03:15,800
 moving away from that which is unessential,

31
00:03:15,800 --> 00:03:26,800
 and trying to attain the essence.

32
00:03:26,800 --> 00:03:32,920
 The meaning is that there's a great amount in this world

33
00:03:32,920 --> 00:03:34,800
 that is unessential.

34
00:03:34,800 --> 00:03:44,180
 Or another way of putting it is leading nowhere, leads to

35
00:03:44,180 --> 00:03:49,800
 no end, leads to no benefit.

36
00:03:49,800 --> 00:03:54,990
 And since the path of addiction, craving more and more and

37
00:03:54,990 --> 00:03:59,800
 chasing after sensual desires, sensual pleasures,

38
00:03:59,800 --> 00:04:04,800
 it leads us on and on and on and builds up.

39
00:04:04,800 --> 00:04:08,800
 It could build up forever for as long as we can build it up

40
00:04:08,800 --> 00:04:08,800
.

41
00:04:08,800 --> 00:04:18,170
 Until finally we at some point can't attain what we strive

42
00:04:18,170 --> 00:04:18,800
 for.

43
00:04:18,800 --> 00:04:22,800
 And then it all comes crashing down.

44
00:04:22,800 --> 00:04:30,800
 It can build and build and build and then it's finished.

45
00:04:30,800 --> 00:04:32,930
 All of the development work that we do in the world to

46
00:04:32,930 --> 00:04:43,800
 build the world, to build society, all the same.

47
00:04:43,800 --> 00:04:45,800
 So what is it that's essential?

48
00:04:45,800 --> 00:04:52,620
 We've been living a life of a monk, as living a life of

49
00:04:52,620 --> 00:05:00,800
 poverty and chastity and renunciation, of simplicity.

50
00:05:00,800 --> 00:05:05,060
 Even still we have to wake up in the morning, we have to go

51
00:05:05,060 --> 00:05:14,800
 into the village for food, every day, every day.

52
00:05:14,800 --> 00:05:19,800
 To no end, this is not the essential.

53
00:05:19,800 --> 00:05:23,310
 Some people think becoming a monk, then you've reached the

54
00:05:23,310 --> 00:05:26,780
 goal, you've reached the pinnacle of existence, but you

55
00:05:26,780 --> 00:05:28,800
 really haven't.

56
00:05:28,800 --> 00:05:33,800
 This isn't the essence.

57
00:05:33,800 --> 00:05:35,800
 A monk's life is just a life.

58
00:05:35,800 --> 00:05:39,280
 You still have to wear clothes, you still have to go for

59
00:05:39,280 --> 00:05:42,800
 food, you still have to eat and use the washroom.

60
00:05:42,800 --> 00:05:47,800
 You still get a whole second back.

61
00:05:47,800 --> 00:05:51,260
 So out of all of existence, all of life, what is the

62
00:05:51,260 --> 00:05:54,800
 essence? What is it that is essential?

63
00:05:54,800 --> 00:06:01,800
 The Buddha gave us very clear teaching on what is essential

64
00:06:01,800 --> 00:06:01,800
.

65
00:06:01,800 --> 00:06:06,830
 It's very much a core of the Buddha's teaching that we hear

66
00:06:06,830 --> 00:06:08,800
 about quite often.

67
00:06:08,800 --> 00:06:12,800
 There are five things.

68
00:06:12,800 --> 00:06:16,800
 Morality, this is essential.

69
00:06:16,800 --> 00:06:21,800
 Concentration or focus, this is essential.

70
00:06:21,800 --> 00:06:26,800
 Wisdom is essential.

71
00:06:26,800 --> 00:06:33,290
 And liberation, and the knowledge of liberation, these five

72
00:06:33,290 --> 00:06:36,800
 things are essential.

73
00:06:36,800 --> 00:06:40,800
 What we understand to be the essence of existence.

74
00:06:40,800 --> 00:06:46,010
 And it goes back to what we were talking about, what I

75
00:06:46,010 --> 00:06:50,510
 talked about earlier about how when your mind is pure, when

76
00:06:50,510 --> 00:06:52,800
 you develop in this way,

77
00:06:52,800 --> 00:06:55,800
 because these are things that anyone can have.

78
00:06:55,800 --> 00:07:05,800
 It's not specifically Buddhist or monastic.

79
00:07:05,800 --> 00:07:11,620
 It's not specific to any one group of people or any type of

80
00:07:11,620 --> 00:07:12,800
 person.

81
00:07:12,800 --> 00:07:17,800
 It's the essence, and everything else is an essential.

82
00:07:17,800 --> 00:07:23,790
 You can be a monk, you can be a lay person, but to find the

83
00:07:23,790 --> 00:07:25,800
 essence of existence,

84
00:07:25,800 --> 00:07:29,800
 and to put it another way, to find success in the practice,

85
00:07:29,800 --> 00:07:35,060
 and to reach the goals that we talked about yesterday, to

86
00:07:35,060 --> 00:07:39,800
 find true peace and happiness and freedom from suffering.

87
00:07:39,800 --> 00:07:43,800
 We need these five things.

88
00:07:43,800 --> 00:07:47,800
 So first we need morality. What does morality mean?

89
00:07:47,800 --> 00:07:51,800
 Well, on the one hand it means we have to keep precepts.

90
00:07:51,800 --> 00:07:54,800
 We have to at least keep the five precepts.

91
00:07:54,800 --> 00:07:59,030
 No matter if we can keep the eight precepts, or even the

92
00:07:59,030 --> 00:08:01,800
 ten, or the monastic precepts.

93
00:08:01,800 --> 00:08:06,060
 This is a way of ordering our minds, ordering our lives, to

94
00:08:06,060 --> 00:08:10,460
 keep ourselves out of trouble so we don't say or do bad

95
00:08:10,460 --> 00:08:11,800
 things.

96
00:08:11,800 --> 00:08:14,780
 But they're really just fence posts. The rules are really

97
00:08:14,780 --> 00:08:15,800
 just fence posts.

98
00:08:15,800 --> 00:08:18,800
 It's up to us to fill in the gaps.

99
00:08:18,800 --> 00:08:24,500
 If you just put fence posts down, you can't keep animals in

100
00:08:24,500 --> 00:08:29,800
, you can't keep anything corralled up.

101
00:08:29,800 --> 00:08:35,800
 You need a fence. The rules are not a fence.

102
00:08:35,800 --> 00:08:40,520
 No matter how many rules you take, or practices, or even

103
00:08:40,520 --> 00:08:44,800
 taking the aesthetic practices, these are just fence posts.

104
00:08:44,800 --> 00:08:51,020
 They don't really stop you from doing and saying bad things

105
00:08:51,020 --> 00:08:54,800
 or things that you regret later.

106
00:08:54,800 --> 00:08:57,800
 The fence is the fence of mindfulness.

107
00:08:57,800 --> 00:09:00,580
 So everything starts with mindfulness. When you walk and

108
00:09:00,580 --> 00:09:04,190
 know that you're walking, when you talk and know that you

109
00:09:04,190 --> 00:09:04,800
're talking,

110
00:09:04,800 --> 00:09:09,320
 when you're aware of what's going on in your body and with

111
00:09:09,320 --> 00:09:11,800
 your speech and in your mind,

112
00:09:11,800 --> 00:09:15,800
 this is where true morality starts.

113
00:09:15,800 --> 00:09:18,430
 When you catch yourself wanting to say this, wanting to say

114
00:09:18,430 --> 00:09:20,800
 that, wanting to do this, wanting to do that,

115
00:09:20,800 --> 00:09:25,610
 then you stop yourself and eventually clarify your mind

116
00:09:25,610 --> 00:09:29,800
 through mindfulness to the point where you don't want to do

117
00:09:29,800 --> 00:09:38,800
 or say things that are a source of regret.

118
00:09:38,800 --> 00:09:42,040
 So even just walking in meditation, we're developing

119
00:09:42,040 --> 00:09:48,800
 morality, we're changing our behavior, our character,

120
00:09:48,800 --> 00:09:57,800
 doing away with all sorts of bad character traits.

121
00:09:57,800 --> 00:10:01,790
 All of the things that we know in our lives that we regret

122
00:10:01,790 --> 00:10:03,800
 or that we feel we don't like about ourselves,

123
00:10:03,800 --> 00:10:07,190
 we do away with them in the meditation practice. We make

124
00:10:07,190 --> 00:10:11,800
 our minds clearer and calmer and do away piece by piece

125
00:10:11,800 --> 00:10:17,240
 with anger and greed and delusion. So we don't do things

126
00:10:17,240 --> 00:10:22,800
 that are regrettable or say things that we regret later.

127
00:10:22,800 --> 00:10:29,960
 This is morality. Focus. Focus means you can practice

128
00:10:29,960 --> 00:10:32,800
 training your mind to focus on a single object

129
00:10:32,800 --> 00:10:39,010
 to fix your mind on something. But this is a kind of

130
00:10:39,010 --> 00:10:43,260
 artificial focus, artificial concentration because

131
00:10:43,260 --> 00:10:45,800
 everything else is excluded.

132
00:10:45,800 --> 00:10:48,300
 It can be useful in the beginning but eventually we want to

133
00:10:48,300 --> 00:10:49,800
 be able to be in focus all the time.

134
00:10:49,800 --> 00:10:55,110
 So our mind is in clear focus no matter what we do, no

135
00:10:55,110 --> 00:10:57,800
 matter what we encounter.

136
00:10:57,800 --> 00:11:00,080
 And we gain this through meditation as well. When we're

137
00:11:00,080 --> 00:11:03,170
 walking and know that the foot is moving, we're clearly

138
00:11:03,170 --> 00:11:04,800
 focused on the foot.

139
00:11:04,800 --> 00:11:08,960
 And we teach ourselves to be clearly focused so there's no

140
00:11:08,960 --> 00:11:14,800
 arising of liking, disliking, drowsiness, distraction.

141
00:11:14,800 --> 00:11:19,420
 There are no bad thoughts. You don't have to talk anymore

142
00:11:19,420 --> 00:11:23,800
 about doing or saying bad things with a focused mind when

143
00:11:23,800 --> 00:11:26,800
 your mind sees clearly everything.

144
00:11:26,800 --> 00:11:32,520
 Every experience, there doesn't even arise greed or anger

145
00:11:32,520 --> 00:11:34,800
 or delusion in the mind.

146
00:11:34,800 --> 00:11:50,910
 So it's not possible for there to arise bad speech or bad

147
00:11:50,910 --> 00:11:53,800
 deeds.

148
00:11:53,800 --> 00:11:59,010
 When the mind is clear, we get this simply from walking and

149
00:11:59,010 --> 00:12:02,800
 from sitting when you watch the stomach rising and falling.

150
00:12:02,800 --> 00:12:05,600
 You know the rising, you know the falling. When you teach

151
00:12:05,600 --> 00:12:09,300
 yourself this way of interacting with reality, with

152
00:12:09,300 --> 00:12:09,800
 everything,

153
00:12:09,800 --> 00:12:14,800
 with your thoughts, with your emotions, with your feelings,

154
00:12:14,800 --> 00:12:19,680
 with things you hear and see and taste and smell and feel

155
00:12:19,680 --> 00:12:22,800
 and think.

156
00:12:22,800 --> 00:12:30,430
 When you purify your mind, when your mind becomes clear and

157
00:12:30,430 --> 00:12:32,800
 set on clarity.

158
00:12:32,800 --> 00:12:36,940
 When this happens, it leads to the third one, which is

159
00:12:36,940 --> 00:12:37,800
 wisdom.

160
00:12:37,800 --> 00:12:42,550
 In the development of wisdom, you can read books or you can

161
00:12:42,550 --> 00:12:45,820
 listen to talks and you can gain some sort of intellectual

162
00:12:45,820 --> 00:12:47,800
 appreciation of the truth.

163
00:12:47,800 --> 00:12:53,650
 But it's not the same. You can see it's not the same as

164
00:12:53,650 --> 00:12:55,800
 experiencing it for yourself.

165
00:12:55,800 --> 00:12:57,910
 When you experience it for yourself, you realize that all

166
00:12:57,910 --> 00:13:01,800
 of the book learning, all of the study that you've done was

167
00:13:01,800 --> 00:13:05,800
 something like the peel of the apple.

168
00:13:05,800 --> 00:13:13,350
 Such a very small and meaningless or insignificant part of

169
00:13:13,350 --> 00:13:21,800
 the apple, of the truth, part of wisdom.

170
00:13:21,800 --> 00:13:28,420
 When you realize the true wisdom that comes from meditation

171
00:13:28,420 --> 00:13:34,800
 practice, then you understand the difference.

172
00:13:34,800 --> 00:13:38,430
 So how does this come in meditation? When you see things

173
00:13:38,430 --> 00:13:45,080
 clearly, you change the way you look at all objects of

174
00:13:45,080 --> 00:13:46,800
 experience.

175
00:13:46,800 --> 00:13:49,900
 The things that you used to like and were attached to, you

176
00:13:49,900 --> 00:13:51,800
 see how meaningless there was.

177
00:13:51,800 --> 00:13:56,850
 How there was nothing there of any value, of any

178
00:13:56,850 --> 00:13:58,800
 significance.

179
00:13:58,800 --> 00:14:04,400
 The things that you were partial towards, there's nothing

180
00:14:04,400 --> 00:14:11,730
 intrinsically better about them, intrinsically attractive

181
00:14:11,730 --> 00:14:13,800
 about them.

182
00:14:13,800 --> 00:14:17,400
 You realize how much suffering comes from clinging to

183
00:14:17,400 --> 00:14:19,800
 things that are insubstantial.

184
00:14:19,800 --> 00:14:24,140
 The things that you were averse towards or afraid of, you

185
00:14:24,140 --> 00:14:27,990
 come to see that there's nothing to be afraid of. There's

186
00:14:27,990 --> 00:14:28,800
 nothing there to be afraid of.

187
00:14:28,800 --> 00:14:33,110
 And there's nothing here to be afraid. There's no "I" that

188
00:14:33,110 --> 00:14:35,800
 we have to protect.

189
00:14:35,800 --> 00:14:39,830
 When we have pain and we don't like the pain, we come to

190
00:14:39,830 --> 00:14:43,800
 see that there's no "I" who is receiving the pain.

191
00:14:43,800 --> 00:14:50,060
 There's no being who is feeling the pain. There's nothing

192
00:14:50,060 --> 00:14:56,800
 to avoid. There's nothing being afflicted by the pain.

193
00:14:56,800 --> 00:14:59,800
 It's just arising and ceasing. It's an experience.

194
00:14:59,800 --> 00:15:06,550
 So given that there's nothing afflicting, you naturally

195
00:15:06,550 --> 00:15:12,800
 lose any idea of being angry at it or upset about it.

196
00:15:12,800 --> 00:15:16,300
 In brief you come to see impermanence, that everything

197
00:15:16,300 --> 00:15:19,800
 inside of you and in the world around you is changing.

198
00:15:19,800 --> 00:15:27,370
 It's not substantial. It has no essence. You see that

199
00:15:27,370 --> 00:15:29,960
 everything inside of you and in the world around you is

200
00:15:29,960 --> 00:15:30,800
 suffering.

201
00:15:30,800 --> 00:15:38,100
 It's not painful in and of itself, but when you cling to it

202
00:15:38,100 --> 00:15:40,800
, it's like fire.

203
00:15:40,800 --> 00:15:45,600
 When you try to adjust it or fix it or make it or change it

204
00:15:45,600 --> 00:15:52,800
 in any way, you only meet with stress and aggravation.

205
00:15:52,800 --> 00:15:55,560
 And then everything inside of us and in the world around us

206
00:15:55,560 --> 00:15:58,800
 is uncontrollable. It doesn't belong to us.

207
00:15:58,800 --> 00:16:02,490
 It isn't an entity or a self. It doesn't belong to any

208
00:16:02,490 --> 00:16:03,800
 entity or self.

209
00:16:03,800 --> 00:16:10,740
 It's just experience that arises and ceases and comes and

210
00:16:10,740 --> 00:16:11,800
 goes.

211
00:16:11,800 --> 00:16:14,810
 And so you stop trying to cling, you stop trying to change,

212
00:16:14,810 --> 00:16:20,800
 you stop trying to hold and fix.

213
00:16:20,800 --> 00:16:25,440
 And you really do just let things go as they're going to go

214
00:16:25,440 --> 00:16:30,800
. You interact rather than react.

215
00:16:30,800 --> 00:16:40,130
 When this comes, you're free. This is the fourth essential

216
00:16:40,130 --> 00:16:42,800
 reality.

217
00:16:42,800 --> 00:16:47,810
 The Buddha said, "Bhanyaya pari sudjati" -- that's through

218
00:16:47,810 --> 00:16:51,800
 wisdom that we become pure.

219
00:16:51,800 --> 00:16:56,800
 The freedom that we're looking for.

220
00:16:56,800 --> 00:17:02,410
 When you have the purity of mind, you become free from all

221
00:17:02,410 --> 00:17:04,800
 defilements of mind.

222
00:17:04,800 --> 00:17:08,400
 This is true freedom. When we talk about freedom, we always

223
00:17:08,400 --> 00:17:13,800
 think, "I can do anything, then I'm free."

224
00:17:13,800 --> 00:17:17,610
 But you can already do anything. You can develop in any way

225
00:17:17,610 --> 00:17:18,800
, but this is unessential.

226
00:17:18,800 --> 00:17:23,780
 It doesn't lead anywhere. Whether you do something or don't

227
00:17:23,780 --> 00:17:27,800
 do something is really of no consequence.

228
00:17:27,800 --> 00:17:31,860
 When you do something, it's still existence. You haven't

229
00:17:31,860 --> 00:17:33,800
 changed that fact.

230
00:17:33,800 --> 00:17:39,180
 When you understand existence, then you're free. Then

231
00:17:39,180 --> 00:17:41,800
 nothing in existence can harm you.

232
00:17:41,800 --> 00:17:45,820
 If someone tries to hurt you or something comes that causes

233
00:17:45,820 --> 00:17:48,800
 harm to you, you know that there's nothing being harmed.

234
00:17:48,800 --> 00:17:52,800
 There's only the experience, and it arises in safety.

235
00:17:52,800 --> 00:17:56,780
 So you're not afraid. You see that there's nothing to be

236
00:17:56,780 --> 00:17:57,800
 afraid of.

237
00:17:57,800 --> 00:18:12,800
 [crickets chirping]

238
00:18:12,800 --> 00:18:18,510
 The final essence or essential reality that we strive for

239
00:18:18,510 --> 00:18:22,800
 is the knowledge, this reassurance that we have.

240
00:18:22,800 --> 00:18:27,800
 Buddha said this is the fifth one, which is the final stage

241
00:18:27,800 --> 00:18:30,800
, where after becoming free you know that you're free.

242
00:18:30,800 --> 00:18:35,370
 This is the stage of enlightenment, when one is aware and

243
00:18:35,370 --> 00:18:38,660
 knows for oneself that they have become free from all

244
00:18:38,660 --> 00:18:40,800
 suffering and stress.

245
00:18:40,800 --> 00:18:50,360
 Whether they're able to live in the world or encounter any

246
00:18:50,360 --> 00:18:57,800
 kind of existence or experience without suffering or stress

247
00:18:57,800 --> 00:19:05,700
, without attachment or clinging, without aversion or desire

248
00:19:05,700 --> 00:19:08,800
 or delusion.

249
00:19:08,800 --> 00:19:12,890
 So how these come about, once wisdom arises, then naturally

250
00:19:12,890 --> 00:19:14,800
 the mind begins to let go.

251
00:19:14,800 --> 00:19:21,970
 You can see this already. When you begin to practice, you

252
00:19:21,970 --> 00:19:23,800
 see that there's a clinging in the mind,

253
00:19:23,800 --> 00:19:28,390
 and when you focus on it you change the way the mind's

254
00:19:28,390 --> 00:19:33,040
 understanding of the experience, or you bring about

255
00:19:33,040 --> 00:19:34,800
 understanding

256
00:19:34,800 --> 00:19:39,800
 until the mind is clear about the experience.

257
00:19:39,800 --> 00:19:45,880
 But as you continue to do this continuously, develop this

258
00:19:45,880 --> 00:19:49,800
 quality, eventually the mind completely lets go of reality.

259
00:19:49,800 --> 00:19:56,800
 There's this final release where the mind lets go.

260
00:19:56,800 --> 00:20:01,790
 It enters into a state that is free from the arising of

261
00:20:01,790 --> 00:20:07,920
 mundane experience, or the arising of seeing or hearing or

262
00:20:07,920 --> 00:20:09,800
 smelling or tasting or feeling.

263
00:20:09,800 --> 00:20:18,800
 It's free from all of that. It's like released.

264
00:20:18,800 --> 00:20:22,830
 And after that experience there arises this knowledge that

265
00:20:22,830 --> 00:20:26,850
 one has entered into this experience, that one has had this

266
00:20:26,850 --> 00:20:27,800
 experience.

267
00:20:27,800 --> 00:20:34,000
 And the mind immediately falls into this comparison, the

268
00:20:34,000 --> 00:20:38,670
 knowledge of the difference between that state and the

269
00:20:38,670 --> 00:20:41,800
 ordinary state of experience.

270
00:20:41,800 --> 00:20:49,170
 And this is what leads to enlightenment. There's clarity,

271
00:20:49,170 --> 00:20:51,800
 there's clear knowledge in the mind,

272
00:20:51,800 --> 00:20:56,800
 that what was experienced is true happiness and true peace,

273
00:20:56,800 --> 00:21:01,510
 and that any happiness or peace that people claim to exist

274
00:21:01,510 --> 00:21:05,750
 in the world could never compare to that peace and that

275
00:21:05,750 --> 00:21:06,800
 happiness.

276
00:21:06,800 --> 00:21:13,800
 There's unshakable knowledge and vision in this fact.

277
00:21:13,800 --> 00:21:17,660
 This is what leads to enlightenment, the knowledge of

278
00:21:17,660 --> 00:21:18,800
 enlightenment.

279
00:21:18,800 --> 00:21:22,970
 Because one has experience for oneself the true peace and

280
00:21:22,970 --> 00:21:26,860
 happiness, which is the freedom from the arising of

281
00:21:26,860 --> 00:21:27,800
 experience.

282
00:21:27,800 --> 00:21:36,460
 When one has that peace and that happiness, one never

283
00:21:36,460 --> 00:21:41,460
 strives or can never believe that there would be another

284
00:21:41,460 --> 00:21:44,800
 kind of peace or happiness greater than it.

285
00:21:44,800 --> 00:21:50,380
 Because any other peace or happiness is always dependent on

286
00:21:50,380 --> 00:21:55,700
 a risen phenomenon coming and going, and can never truly

287
00:21:55,700 --> 00:21:56,800
 satisfy.

288
00:21:56,800 --> 00:22:04,800
 So this is the path and the progress.

289
00:22:04,800 --> 00:22:09,030
 In the beginning of our practice we develop morality, so we

290
00:22:09,030 --> 00:22:11,370
 try to catch ourselves when we're going to say or do bad

291
00:22:11,370 --> 00:22:14,800
 things, things that we'll regret later.

292
00:22:14,800 --> 00:22:17,200
 When we've done things that we regret then we say, "Okay,

293
00:22:17,200 --> 00:22:20,040
 so next time I'm going to be careful and watch myself and

294
00:22:20,040 --> 00:22:24,700
 try to be mindful of that, be mindful before I fall into

295
00:22:24,700 --> 00:22:25,800
 that."

296
00:22:25,800 --> 00:22:29,810
 Once we continue that, and again and again bringing our

297
00:22:29,810 --> 00:22:34,340
 mind back to the foot, back to the hand, keeping our mind

298
00:22:34,340 --> 00:22:37,800
 with the object, we develop focus and concentration.

299
00:22:37,800 --> 00:22:40,770
 Once we develop focus and concentration we begin to see

300
00:22:40,770 --> 00:22:44,680
 things clearly, the defilements and the hindrances in the

301
00:22:44,680 --> 00:22:47,780
 mind disappear, wanting this, wanting that, disliking this,

302
00:22:47,780 --> 00:22:51,800
 disliking that, drowsiness, distraction, doubt.

303
00:22:51,800 --> 00:22:54,800
 They all leave the mind.

304
00:22:54,800 --> 00:22:58,910
 Once they leave the mind, the mind is able to see the

305
00:22:58,910 --> 00:23:01,800
 objects of experience clearly.

306
00:23:01,800 --> 00:23:05,160
 Once one sees clearer and clearer and develops wisdom, the

307
00:23:05,160 --> 00:23:06,800
 mind eventually lets go.

308
00:23:06,800 --> 00:23:10,060
 Once the mind lets go there's cessation and then the

309
00:23:10,060 --> 00:23:12,800
 knowledge that cessation has occurred.

310
00:23:12,800 --> 00:23:16,800
 That knowledge is the knowledge of enlightenment.

311
00:23:16,800 --> 00:23:21,210
 This is the progress of the path, quite in brief, but it's

312
00:23:21,210 --> 00:23:25,530
 the teaching of the Buddha on what is the essential aspect

313
00:23:25,530 --> 00:23:28,800
 of existence, those things that are essential.

314
00:23:28,800 --> 00:23:32,430
 This is important to keep in mind when we think about what

315
00:23:32,430 --> 00:23:37,320
 we're going to do today, tomorrow, for our lives, and what

316
00:23:37,320 --> 00:23:42,800
 we want to do as monastics or as lay people.

317
00:23:42,800 --> 00:23:47,060
 We see what is most important, most essential, and try to

318
00:23:47,060 --> 00:23:50,800
 find a way to devote ourselves to these things.

319
00:23:50,800 --> 00:23:58,200
 So it's worth appreciating that everyone, all of us, that

320
00:23:58,200 --> 00:24:03,890
 we can be confident in ourselves, confident of ourselves,

321
00:24:03,890 --> 00:24:06,800
 and proud of ourselves in a sense,

322
00:24:06,800 --> 00:24:10,720
 that we have taken up the practice of meditation, and here

323
00:24:10,720 --> 00:24:14,800
 we are coming together to meditate every day, every day.

324
00:24:14,800 --> 00:24:18,390
 We can feel good about the fact that at least to some

325
00:24:18,390 --> 00:24:22,980
 extent we're developing these essential qualities of mind,

326
00:24:22,980 --> 00:24:25,800
 morality, concentration, wisdom,

327
00:24:25,800 --> 00:24:30,550
 and leading ourselves closer and closer to freedom from

328
00:24:30,550 --> 00:24:31,800
 suffering.

329
00:24:31,800 --> 00:24:36,660
 We can be reassured that what we're doing in our life is

330
00:24:36,660 --> 00:24:37,800
 not a waste.

331
00:24:37,800 --> 00:24:42,800
 We're doing something that is of true value and benefit.

332
00:24:42,800 --> 00:24:47,420
 So we're reminding ourselves and feeling confident about

333
00:24:47,420 --> 00:24:51,030
 what we're doing and happy about this, and not looking for

334
00:24:51,030 --> 00:24:56,800
 other things or trying to find another way of ourselves.

335
00:24:56,800 --> 00:25:02,750
 The practice of the Buddhist teaching is to become content

336
00:25:02,750 --> 00:25:07,800
 with simplicity, content with a life of mental development.

337
00:25:07,800 --> 00:25:12,230
 Always thinking most important is to make yourself a better

338
00:25:12,230 --> 00:25:15,380
 person or to purify your mind further and further, develop

339
00:25:15,380 --> 00:25:17,800
 your mind further and further.

340
00:25:17,800 --> 00:25:21,800
 So this is what we're undertaking here.

341
00:25:21,800 --> 00:25:26,800
 That is the brief pep talk for tonight.

342
00:25:26,800 --> 00:25:30,980
 So thank you for listening, and now we continue with the p

343
00:25:30,980 --> 00:25:31,800
ep talk.

