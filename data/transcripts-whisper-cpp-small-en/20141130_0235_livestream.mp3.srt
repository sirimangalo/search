1
00:00:00,000 --> 00:00:14,570
 Okay, so here we are with our new live stream audio

2
00:00:14,570 --> 00:00:19,680
 broadcast.

3
00:00:19,680 --> 00:00:37,600
 We are sitting out by a smaller pond within what Jumtong.

4
00:00:37,600 --> 00:00:52,630
 The sun is shining, the birds are singing, and it's the

5
00:00:52,630 --> 00:00:55,000
 kind of setting that makes you

6
00:00:55,000 --> 00:01:04,720
 think that all is right in the world.

7
00:01:04,720 --> 00:01:21,360
 This is a big question that comes up in life.

8
00:01:21,360 --> 00:01:25,680
 You look at the world around you, sometimes it seems like

9
00:01:25,680 --> 00:01:27,400
 everything is just okay and

10
00:01:27,400 --> 00:01:30,560
 everything is all right.

11
00:01:30,560 --> 00:01:41,470
 You wonder why is it that with all the good that there is,

12
00:01:41,470 --> 00:01:45,080
 all the seeming perfection

13
00:01:45,080 --> 00:01:52,840
 there is in the world, why is it that we are still beset by

14
00:01:52,840 --> 00:01:54,880
 suffering?

15
00:01:54,880 --> 00:02:01,880
 When you look around we've really got everything we need.

16
00:02:01,880 --> 00:02:16,000
 We have food and shelter and life shouldn't be good.

17
00:02:16,000 --> 00:02:23,840
 But even when you've got all these things, why is it that

18
00:02:23,840 --> 00:02:27,440
 even then we still seem to

19
00:02:27,440 --> 00:02:41,080
 be beset by suffering?

20
00:02:41,080 --> 00:02:45,040
 One way of answering this question, why do we suffer when

21
00:02:45,040 --> 00:02:46,880
 this is the comeback that makes

22
00:02:46,880 --> 00:02:48,360
 us suffer in the world?

23
00:02:48,360 --> 00:03:12,960
 The Buddha's teaching on Mara.

24
00:03:12,960 --> 00:03:28,760
 It's similar to the concept of Satan in Christianity.

25
00:03:28,760 --> 00:03:35,440
 Mara literally, a fairly literal translation would be evil.

26
00:03:35,440 --> 00:03:45,360
 In etymologically it probably comes from the root mar which

27
00:03:45,360 --> 00:03:47,600
 means to die.

28
00:03:47,600 --> 00:03:58,690
 So the commentaries define it as that which, mariti, that's

29
00:03:58,690 --> 00:04:02,720
 what causes goodness to die,

30
00:04:02,720 --> 00:04:15,370
 all good things to die, that which kills good things, that

31
00:04:15,370 --> 00:04:21,240
 which ruins the good life, which

32
00:04:21,240 --> 00:04:32,400
 prevents us from being happy.

33
00:04:32,400 --> 00:04:40,030
 That's the thing really, you can't say that life is good,

34
00:04:40,030 --> 00:04:43,080
 there's all is well.

35
00:04:43,080 --> 00:04:47,410
 As we know that it's not really true, there's evil in the

36
00:04:47,410 --> 00:04:48,280
 world.

37
00:04:48,280 --> 00:04:54,280
 There are five kinds of evil in the world.

38
00:04:54,280 --> 00:05:14,940
 This is what ruins the, ruins our happiness, ruins our

39
00:05:14,940 --> 00:05:18,520
 peace.

40
00:05:18,520 --> 00:05:29,560
 So there, according to the Buddha there are five kinds of

41
00:05:29,560 --> 00:05:31,240
 Mara.

42
00:05:31,240 --> 00:05:44,140
 If we understand these five then we're well equipped to

43
00:05:44,140 --> 00:05:50,680
 meet the problem head on.

44
00:05:50,680 --> 00:05:58,590
 Five kinds of evil that exist in the world are, first of

45
00:05:58,590 --> 00:06:03,000
 all what we call khanda-ma.

46
00:06:03,000 --> 00:06:06,000
 Khanda means the aggregates.

47
00:06:06,000 --> 00:06:19,650
 So the aggregates are a type of form of evil, they spoil

48
00:06:19,650 --> 00:06:22,400
 things.

49
00:06:22,400 --> 00:06:28,260
 Like you might be, I might be sitting here enjoying this

50
00:06:28,260 --> 00:06:31,840
 lovely morning but then suddenly

51
00:06:31,840 --> 00:06:37,530
 the chair I'm sitting in starts to chafe against my rear

52
00:06:37,530 --> 00:06:40,800
 end sitting in this hard plastic chair

53
00:06:40,800 --> 00:06:44,920
 and so pain arises.

54
00:06:44,920 --> 00:06:52,740
 And it's like just had to come and spoil the, spoil the

55
00:06:52,740 --> 00:06:54,040
 mood.

56
00:06:54,040 --> 00:07:06,400
 All is well until that happened.

57
00:07:06,400 --> 00:07:14,280
 All is well until that happened.

58
00:07:14,280 --> 00:07:19,690
 Mainly the body, but also the other kind of the other

59
00:07:19,690 --> 00:07:21,400
 aggregates.

60
00:07:21,400 --> 00:07:25,210
 Finding out that experience has an element of suffering in

61
00:07:25,210 --> 00:07:25,680
 it.

62
00:07:25,680 --> 00:07:33,870
 But I said nati khanda-ma dukha, there's no suffering like

63
00:07:33,870 --> 00:07:36,600
 the aggregates.

64
00:07:36,600 --> 00:07:48,270
 The body, feelings, recognitions, thoughts, consciousness,

65
00:07:48,270 --> 00:07:53,120
 these are actually dukha.

66
00:07:53,120 --> 00:07:59,060
 They can't be a source of refuge or peace for us.

67
00:07:59,060 --> 00:08:02,450
 Because they're impermanent suffering in non-self you can't

68
00:08:02,450 --> 00:08:05,600
 rely upon them, even the good ones,

69
00:08:05,600 --> 00:08:07,600
 the good aspects.

70
00:08:07,600 --> 00:08:34,760
 They only come in turns with the bad aspects.

71
00:08:34,760 --> 00:08:38,280
 So this is the first evil that we have to deal with.

72
00:08:38,280 --> 00:08:43,140
 This is really where our practice focuses on overcoming

73
00:08:43,140 --> 00:08:43,920
 this.

74
00:08:43,920 --> 00:08:49,330
 What do you do if you have pain from sitting too long or a

75
00:08:49,330 --> 00:08:52,360
 headache or a stomach ache or

76
00:08:52,360 --> 00:08:53,360
 back ache?

77
00:08:53,360 --> 00:08:57,360
 What do you do when you're sick?

78
00:08:57,360 --> 00:09:04,800
 What do you do when you meet with pains and aches?

79
00:09:04,800 --> 00:09:09,230
 What do you do when you meet with bad thoughts and have bad

80
00:09:09,230 --> 00:09:10,960
 memories, anguishing over the

81
00:09:10,960 --> 00:09:14,120
 past or the future?

82
00:09:14,120 --> 00:09:27,680
 This is what the meditation aims to address.

83
00:09:27,680 --> 00:09:35,160
 Because when you're mindful you can't get rid of the evil

84
00:09:35,160 --> 00:09:38,360
 of the body unless you're

85
00:09:38,360 --> 00:09:42,260
 the evil of the aggregates, unless you just don't be born

86
00:09:42,260 --> 00:09:43,000
 again.

87
00:09:43,000 --> 00:09:50,440
 But until then the path out of suffering is to free

88
00:09:50,440 --> 00:09:56,040
 yourself from the control, free yourself

89
00:09:56,040 --> 00:10:01,320
 from the control, being controlled by these things, free

90
00:10:01,320 --> 00:10:04,040
 yourself from their power.

91
00:10:04,040 --> 00:10:12,850
 When pain no longer upsets you, that's freedom from the

92
00:10:12,850 --> 00:10:15,040
 evil of it.

93
00:10:15,040 --> 00:10:16,040
 This is the first evil.

94
00:10:16,040 --> 00:10:20,880
 The second evil is called kilesa mara.

95
00:10:20,880 --> 00:10:27,700
 Now this one we aim to be truly free from as soon and as

96
00:10:27,700 --> 00:10:31,160
 completely as possible.

97
00:10:31,160 --> 00:10:36,800
 Kilesa means defilement.

98
00:10:36,800 --> 00:10:43,560
 So something that makes the mind unclean, impure.

99
00:10:43,560 --> 00:10:48,420
 It's just a figurative, figure of speech refers to those

100
00:10:48,420 --> 00:10:51,840
 states of mind that lead to suffering.

101
00:10:51,840 --> 00:10:54,480
 They're evil.

102
00:10:54,480 --> 00:10:58,040
 They're evil because they lead us to suffer.

103
00:10:58,040 --> 00:11:01,480
 Hunger leads us to suffer.

104
00:11:01,480 --> 00:11:03,360
 Greed leads us to suffer.

105
00:11:03,360 --> 00:11:04,360
 Delusion leads us to suffer.

106
00:11:04,360 --> 00:11:11,750
 And all of the many types of these defilements in all their

107
00:11:11,750 --> 00:11:14,000
 many flavors.

108
00:11:14,000 --> 00:11:18,380
 So these are just reactions to experience and they become

109
00:11:18,380 --> 00:11:20,560
 habitual to the point where

110
00:11:20,560 --> 00:11:22,680
 we react without thinking.

111
00:11:22,680 --> 00:11:27,530
 We like certain things and want certain things because we

112
00:11:27,530 --> 00:11:30,360
've cultivated a recognition based

113
00:11:30,360 --> 00:11:38,350
 on our recognition that this is like that and based on our

114
00:11:38,350 --> 00:11:42,320
 judgments, our thoughts.

115
00:11:42,320 --> 00:11:44,960
 I like this or this is good.

116
00:11:44,960 --> 00:11:49,330
 We cultivate the habit of liking certain things and disl

117
00:11:49,330 --> 00:11:52,240
iking other things based on our aversion

118
00:11:52,240 --> 00:11:57,140
 based on the pain that they caused and our conceiving of

119
00:11:57,140 --> 00:11:58,800
 the pain as bad.

120
00:11:58,800 --> 00:12:10,260
 Our delusion, our misunderstanding of reality is becoming

121
00:12:10,260 --> 00:12:16,400
 partial to certain experiences.

122
00:12:16,400 --> 00:12:24,080
 And they cultivate the habit of delusion, conceit and

123
00:12:24,080 --> 00:12:28,800
 arrogance and just all around ignorance

124
00:12:28,800 --> 00:12:40,960
 that leads us to misunderstand, misrepresent.

125
00:12:40,960 --> 00:12:50,080
 And respond or react inappropriately.

126
00:12:50,080 --> 00:12:59,120
 React in a way that leads us to suffer.

127
00:12:59,120 --> 00:13:02,860
 So the meditation does away with this one completely

128
00:13:02,860 --> 00:13:05,080
 because it changes the way you

129
00:13:05,080 --> 00:13:06,080
 react to things.

130
00:13:06,080 --> 00:13:11,320
 Instead of reacting, you simply interact.

131
00:13:11,320 --> 00:13:15,920
 So seeing is just seeing and you react appropriately.

132
00:13:15,920 --> 00:13:18,750
 Hearing is just hearing, smelling, tasting, feeling,

133
00:13:18,750 --> 00:13:19,440
 thinking.

134
00:13:19,440 --> 00:13:23,370
 No matter what you experience, it's all just one of these

135
00:13:23,370 --> 00:13:24,000
 six.

136
00:13:24,000 --> 00:13:28,680
 And when you see that, you react appropriately.

137
00:13:28,680 --> 00:13:32,270
 Really all of our suffering and all of our defilements of

138
00:13:32,270 --> 00:13:34,240
 mind are just because we don't

139
00:13:34,240 --> 00:13:36,160
 see things as they are.

140
00:13:36,160 --> 00:13:39,660
 If you really saw things as they were, you wouldn't react

141
00:13:39,660 --> 00:13:41,520
 inappropriately because we

142
00:13:41,520 --> 00:13:46,680
 want to be happy ostensibly.

143
00:13:46,680 --> 00:13:51,220
 It goes to stands to reason that if we really were clear of

144
00:13:51,220 --> 00:13:53,960
 mind, we would never do anything

145
00:13:53,960 --> 00:13:57,680
 that caused us unhappiness.

146
00:13:57,680 --> 00:14:08,240
 It's irrational.

147
00:14:08,240 --> 00:14:15,150
 The third Mara is Abhisankara Mara, which refers to our

148
00:14:15,150 --> 00:14:17,800
 actions, karma.

149
00:14:17,800 --> 00:14:21,120
 And karma can be a nasty evil.

150
00:14:21,120 --> 00:14:23,880
 It traps us.

151
00:14:23,880 --> 00:14:26,120
 Good karma isn't so bad.

152
00:14:26,120 --> 00:14:31,240
 Even good deeds isn't so bad.

153
00:14:31,240 --> 00:14:37,040
 There's nothing practically speaking wrong with it.

154
00:14:37,040 --> 00:14:42,410
 Though in the end, an enlightened being gives up even good

155
00:14:42,410 --> 00:14:43,320
 karma.

156
00:14:43,320 --> 00:14:51,590
 Meaning, when they do good deeds, they have no intention or

157
00:14:51,590 --> 00:14:54,800
 no wish for a result.

158
00:14:54,800 --> 00:14:56,860
 That's really the thing is that even good karma is

159
00:14:56,860 --> 00:14:58,280
 generally done with a wish for a

160
00:14:58,280 --> 00:15:01,520
 certain result.

161
00:15:01,520 --> 00:15:05,560
 That's fine if your wish is to be a pure of mind and free

162
00:15:05,560 --> 00:15:07,120
 from suffering.

163
00:15:07,120 --> 00:15:09,270
 The reason an enlightened being has done away with this is

164
00:15:09,270 --> 00:15:10,560
 because they don't need it.

165
00:15:10,560 --> 00:15:15,440
 They already are it.

166
00:15:15,440 --> 00:15:19,380
 But often we do good deeds wishing to become rich or

167
00:15:19,380 --> 00:15:22,080
 wishing to go to heaven or so on.

168
00:15:22,080 --> 00:15:26,090
 Wishing to have good friends as you do unto others as you

169
00:15:26,090 --> 00:15:28,200
 would have them do unto you.

170
00:15:28,200 --> 00:15:30,420
 So we're wishing for people to do good things to us and

171
00:15:30,420 --> 00:15:31,720
 when they do bad things we still

172
00:15:31,720 --> 00:15:37,400
 get angry.

173
00:15:37,400 --> 00:15:42,440
 The biggest evil here is in regards to bad deeds.

174
00:15:42,440 --> 00:15:51,780
 Words that hurt others because they come back and bite you

175
00:15:51,780 --> 00:15:54,160
 in the butt.

176
00:15:54,160 --> 00:15:57,280
 They come back and get you in the end.

177
00:15:57,280 --> 00:16:03,200
 What goes around comes around.

178
00:16:03,200 --> 00:16:07,760
 That which we perform it becomes habitual.

179
00:16:07,760 --> 00:16:12,110
 It changes us to the story of this man who was very, very

180
00:16:12,110 --> 00:16:14,480
 mean to an enlightened being

181
00:16:14,480 --> 00:16:20,840
 and how that stuck with him and eventually he was born.

182
00:16:20,840 --> 00:16:29,560
 All he would eat was his own excrement.

183
00:16:29,560 --> 00:16:31,830
 Everyone thought that was such a weird story but then I

184
00:16:31,830 --> 00:16:33,360
 looked it up on Wikipedia and it's

185
00:16:33,360 --> 00:16:40,840
 actually a certain people actually do suffer from this

186
00:16:40,840 --> 00:16:45,800
 delusion and they have only a desire

187
00:16:45,800 --> 00:16:49,240
 to eat their own excrement.

188
00:16:49,240 --> 00:16:56,660
 It's kind of extreme but the kind of thing that just the

189
00:16:56,660 --> 00:17:00,720
 claim that this sort of thing

190
00:17:00,720 --> 00:17:04,370
 is caused by the perversion of mind that comes from evil

191
00:17:04,370 --> 00:17:05,280
 thoughts.

192
00:17:05,280 --> 00:17:09,640
 I mean no matter what you believe it's clear to see that

193
00:17:09,640 --> 00:17:12,440
 people who do nasty things become

194
00:17:12,440 --> 00:17:16,000
 perverse in mind.

195
00:17:16,000 --> 00:17:22,490
 There's that famous book, Crime and Punishment, which

196
00:17:22,490 --> 00:17:25,520
 illustrates this well.

197
00:17:25,520 --> 00:17:31,750
 You don't realize it maybe if you're around people who, you

198
00:17:31,750 --> 00:17:34,720
 know what they say, people

199
00:17:34,720 --> 00:17:38,400
 who are mean to their pets tend to be abusive to their

200
00:17:38,400 --> 00:17:41,080
 family members as well and this kind

201
00:17:41,080 --> 00:17:42,080
 of thing.

202
00:17:42,080 --> 00:17:43,080
 But it changes you.

203
00:17:43,080 --> 00:17:44,440
 It makes you less happy.

204
00:17:44,440 --> 00:17:45,440
 It makes you more scared.

205
00:17:45,440 --> 00:17:52,840
 When you're mean to others you become suspicious yourself.

206
00:17:52,840 --> 00:17:57,880
 When you cheat others you don't trust others.

207
00:17:57,880 --> 00:17:59,930
 When you're a mean person you tend to surround yourself

208
00:17:59,930 --> 00:18:00,760
 with mean people.

209
00:18:00,760 --> 00:18:03,890
 You don't like to be in the company of good people because

210
00:18:03,890 --> 00:18:05,920
 they make you feel uncomfortable,

211
00:18:05,920 --> 00:18:06,920
 this kind of thing.

212
00:18:06,920 --> 00:18:12,290
 So it definitely changes your life when you start to do

213
00:18:12,290 --> 00:18:13,160
 evil.

214
00:18:13,160 --> 00:18:15,260
 There's this show that my family was watching called

215
00:18:15,260 --> 00:18:16,040
 Breaking Bad.

216
00:18:16,040 --> 00:18:20,910
 I don't know whether this show, I never of course watched

217
00:18:20,910 --> 00:18:23,640
 it, but I wonder if you watched

218
00:18:23,640 --> 00:18:26,830
 that show you'd be able to see whether it was actually

219
00:18:26,830 --> 00:18:28,440
 according to reality.

220
00:18:28,440 --> 00:18:31,660
 The other problem is that we watch a lot of television and

221
00:18:31,660 --> 00:18:32,560
 television rarely.

222
00:18:32,560 --> 00:18:35,910
 I mean of course it's all in the minds of the people who

223
00:18:35,910 --> 00:18:37,840
 produce it so unless they're

224
00:18:37,840 --> 00:18:42,120
 enlightened which is highly unlikely, they're probably

225
00:18:42,120 --> 00:18:44,800
 going to get reality wrong, right?

226
00:18:44,800 --> 00:18:50,400
 It'll make you think that it's not the way it is.

227
00:18:50,400 --> 00:18:54,070
 So it's difficult for us to actually see the truth of karma

228
00:18:54,070 --> 00:18:56,120
 because we have all these wrong

229
00:18:56,120 --> 00:19:02,000
 ideas from media and from culture and in our even religion.

230
00:19:02,000 --> 00:19:06,080
 But if we take a look at reality we can see how it's

231
00:19:06,080 --> 00:19:09,800
 changing us when we do good and bad

232
00:19:09,800 --> 00:19:11,000
 deeds.

233
00:19:11,000 --> 00:19:12,000
 And so these are dangerous.

234
00:19:12,000 --> 00:19:20,400
 It would be a cause for great suffering.

235
00:19:20,400 --> 00:19:22,360
 Again meditation clarifies this.

236
00:19:22,360 --> 00:19:24,640
 You don't have to get rid of good deeds.

237
00:19:24,640 --> 00:19:25,920
 In fact you shouldn't.

238
00:19:25,920 --> 00:19:28,730
 Good deeds are a means of keeping you from performing bad

239
00:19:28,730 --> 00:19:29,240
 deeds.

240
00:19:29,240 --> 00:19:37,410
 You keep your mind in a place of wishing goodness for

241
00:19:37,410 --> 00:19:39,160
 others.

242
00:19:39,160 --> 00:19:41,360
 And then you'll just be doing good deeds as a matter of

243
00:19:41,360 --> 00:19:42,960
 course because when someone wants

244
00:19:42,960 --> 00:19:49,070
 your help you have none of this selfishness that leads us

245
00:19:49,070 --> 00:19:51,960
 to reject other people.

246
00:19:51,960 --> 00:19:59,690
 If someone really is in need of help and you know that you

247
00:19:59,690 --> 00:20:04,360
 can help them, you just do it.

248
00:20:04,360 --> 00:20:12,600
 It's number three.

249
00:20:12,600 --> 00:20:16,680
 The fourth evil in the world is called Machumara.

250
00:20:16,680 --> 00:20:19,960
 This is the evil of death.

251
00:20:19,960 --> 00:20:23,400
 Yeah, death is evil.

252
00:20:23,400 --> 00:20:28,570
 It's pretty easy to understand conventionally how this is

253
00:20:28,570 --> 00:20:34,320
 true because at least conventionally

254
00:20:34,320 --> 00:20:42,080
 speaking it's the real showstopper in life.

255
00:20:42,080 --> 00:20:46,390
 No matter what your goals are, no matter what your plans

256
00:20:46,390 --> 00:20:49,440
 are, they're all going to be circumscribed

257
00:20:49,440 --> 00:20:51,440
 by the fact that you have to die.

258
00:20:51,440 --> 00:20:58,160
 They're all in the context of your imminent death.

259
00:20:58,160 --> 00:21:05,710
 So if someone says I'm going to build a civilization and

260
00:21:05,710 --> 00:21:08,240
 watch it flourish

261
00:21:08,240 --> 00:21:13,150
 for the next one thousand years or the next one hundred

262
00:21:13,150 --> 00:21:15,840
 years, then they're probably not

263
00:21:15,840 --> 00:21:18,400
 being realistic.

264
00:21:18,400 --> 00:21:21,740
 So the goodness that we can do, the great things that we

265
00:21:21,740 --> 00:21:23,520
 can do are circumscribed by

266
00:21:23,520 --> 00:21:24,520
 death.

267
00:21:24,520 --> 00:21:33,620
 Limited by our mortality because when you die you don't

268
00:21:33,620 --> 00:21:38,280
 know where you're going.

269
00:21:38,280 --> 00:21:42,120
 In most cases you won't remember anything.

270
00:21:42,120 --> 00:21:49,490
 You lose everything from the confusion and the limitations

271
00:21:49,490 --> 00:21:52,640
 of your new existence.

272
00:21:52,640 --> 00:21:55,960
 Seems like the brain limits our understanding.

273
00:21:55,960 --> 00:22:01,920
 So if you're born with a brain you're going to be stuck.

274
00:22:01,920 --> 00:22:05,920
 It's like being stuck in a prison.

275
00:22:05,920 --> 00:22:12,840
 Death is a real problem and so we consider it evil.

276
00:22:12,840 --> 00:22:23,480
 It's evil to have everything cut short.

277
00:22:23,480 --> 00:22:25,240
 You don't control that.

278
00:22:25,240 --> 00:22:29,940
 It's not like we say well I've had enough of this life, it

279
00:22:29,940 --> 00:22:31,440
's time to die.

280
00:22:31,440 --> 00:22:37,420
 Most of us are wishing we could stay on longer, have more

281
00:22:37,420 --> 00:22:38,440
 time.

282
00:22:38,440 --> 00:22:45,080
 More time as human beings.

283
00:22:45,080 --> 00:22:51,390
 Of course there are some people, then there are those

284
00:22:51,390 --> 00:22:55,160
 people who wish to die, people who

285
00:22:55,160 --> 00:22:59,400
 are suicidal.

286
00:22:59,400 --> 00:23:03,100
 Which on the surface seems actually kind of like you've

287
00:23:03,100 --> 00:23:04,400
 beaten evil, right?

288
00:23:04,400 --> 00:23:06,040
 It's no longer evil to you.

289
00:23:06,040 --> 00:23:12,280
 I have no problem with death, I'm ready to die, that kind

290
00:23:12,280 --> 00:23:13,640
 of thing.

291
00:23:13,640 --> 00:23:17,440
 But it's not really that people who kill themselves aren't

292
00:23:17,440 --> 00:23:19,560
 usually, it's not really that they

293
00:23:19,560 --> 00:23:29,600
 want to die, it's that they can't stand living.

294
00:23:29,600 --> 00:23:35,520
 And so it's ostensibly a good idea to just end it, right?

295
00:23:35,520 --> 00:23:38,400
 Unfortunately it doesn't really end anything.

296
00:23:38,400 --> 00:23:40,240
 That's the big surprise.

297
00:23:40,240 --> 00:23:46,110
 In fact it makes things worse because your aversion to life

298
00:23:46,110 --> 00:23:48,720
 and your inability to cope

299
00:23:48,720 --> 00:23:52,380
 is going to have serious consequences when you're

300
00:23:52,380 --> 00:23:55,600
 confronted with your deathbed visions.

301
00:23:55,600 --> 00:23:59,910
 You're going to react strongly to whatever you experience

302
00:23:59,910 --> 00:24:02,400
 because you haven't cultivated

303
00:24:02,400 --> 00:24:06,500
 the ability to deal with pleasant and unpleasant

304
00:24:06,500 --> 00:24:07,920
 experiences.

305
00:24:07,920 --> 00:24:10,560
 So you'll cling to anything pleasant that comes, or

306
00:24:10,560 --> 00:24:12,360
 anything unpleasant that comes,

307
00:24:12,360 --> 00:24:16,000
 even the most coarse base visions.

308
00:24:16,000 --> 00:24:18,600
 And so it means you're indiscriminate.

309
00:24:18,600 --> 00:24:22,060
 This is the problem with dying and being born again, most

310
00:24:22,060 --> 00:24:23,720
 people are indiscriminate and

311
00:24:23,720 --> 00:24:26,240
 they'll grab at anything.

312
00:24:26,240 --> 00:24:28,240
 That's quite dangerous because you don't know what that is.

313
00:24:28,240 --> 00:24:31,360
 It's usually, as you can see from meditation, it usually

314
00:24:31,360 --> 00:24:33,520
 starts off with the most coarse.

315
00:24:33,520 --> 00:24:36,890
 It's not like you're dying, you'll be immediately presented

316
00:24:36,890 --> 00:24:38,760
 by someone who says, "Hey, want

317
00:24:38,760 --> 00:24:41,120
 to go to heaven?"

318
00:24:41,120 --> 00:24:46,240
 It's a much more refined, quiet mind.

319
00:24:46,240 --> 00:24:48,700
 The much louder minds are more likely to take your

320
00:24:48,700 --> 00:24:50,920
 attention, like the ones that say, "I'm

321
00:24:50,920 --> 00:24:56,800
 hungry," or "I'm thirsty," or "Oh, this looks shiny and

322
00:24:56,800 --> 00:24:57,840
 nice."

323
00:24:57,840 --> 00:25:04,160
 The ones that are carnal and coarse, gross.

324
00:25:04,160 --> 00:25:10,240
 The fear base, the anger base, the greed base, these ones.

325
00:25:10,240 --> 00:25:11,520
 You can see this from meditation.

326
00:25:11,520 --> 00:25:14,360
 This isn't theoretical.

327
00:25:14,360 --> 00:25:17,800
 Meditation gives you an insight into how the mind works.

328
00:25:17,800 --> 00:25:22,730
 It's going to be the same when you pass away because when

329
00:25:22,730 --> 00:25:25,800
 you die, similar to meditation

330
00:25:25,800 --> 00:25:29,000
 in the sense that your mind is quiet.

331
00:25:29,000 --> 00:25:33,880
 You're not getting the stimulus from the outside world.

332
00:25:33,880 --> 00:25:39,680
 When you meditate, you see the coarse things coming up.

333
00:25:39,680 --> 00:25:41,440
 Working through them is the first order of business.

334
00:25:41,440 --> 00:25:46,000
 When you die, you won't have to deal with them anymore.

335
00:25:46,000 --> 00:25:50,340
 At the very least, when you die, you should have a very

336
00:25:50,340 --> 00:25:53,680
 refined mind and be able to discriminate

337
00:25:53,680 --> 00:25:57,960
 between the various things that present themselves.

338
00:25:57,960 --> 00:26:04,880
 So suicide is not such a bright move.

339
00:26:04,880 --> 00:26:09,730
 It's like you're in class and you're learning all the

340
00:26:09,730 --> 00:26:12,360
 things you have to learn.

341
00:26:12,360 --> 00:26:14,640
 Some ways in, you say, "This class is terrible.

342
00:26:14,640 --> 00:26:15,640
 This sucks.

343
00:26:15,640 --> 00:26:18,280
 Just give me the exam now and let's get it over with."

344
00:26:18,280 --> 00:26:24,660
 It's not a very bright move, academically speaking, because

345
00:26:24,660 --> 00:26:27,040
 when you take the exam and

346
00:26:27,040 --> 00:26:33,890
 you're not ready for it, your chances of passing are

347
00:26:33,890 --> 00:26:37,560
 reduced to say the least.

348
00:26:37,560 --> 00:26:43,180
 So that's the fourth one, the fifth evil that we have to

349
00:26:43,180 --> 00:26:47,160
 cope with is actually called Devaputamara,

350
00:26:47,160 --> 00:26:54,700
 which refers to the iconic satanic figure, the fallen angel

351
00:26:54,700 --> 00:26:55,240
.

352
00:26:55,240 --> 00:27:01,740
 Interesting in Buddhist cosmology, Mara, Mara as an angel,

353
00:27:01,740 --> 00:27:05,080
 as a divine being, is actually

354
00:27:05,080 --> 00:27:10,680
 in the highest of the sensual realm heavens.

355
00:27:10,680 --> 00:27:14,910
 The idea being that there is some sort of structure to the

356
00:27:14,910 --> 00:27:16,920
 universe beyond the human

357
00:27:16,920 --> 00:27:18,520
 realm.

358
00:27:18,520 --> 00:27:23,300
 So there are the four great kings that look after the

359
00:27:23,300 --> 00:27:26,560
 directions and they watch over the

360
00:27:26,560 --> 00:27:30,800
 earth maybe.

361
00:27:30,800 --> 00:27:33,250
 All of the earth angels report to them and there's some

362
00:27:33,250 --> 00:27:34,760
 kind of social structure going

363
00:27:34,760 --> 00:27:38,950
 on and they report to the next level of angels and so on,

364
00:27:38,950 --> 00:27:41,240
 all the way up to this highest

365
00:27:41,240 --> 00:27:44,800
 level of the bureaucracy.

366
00:27:44,800 --> 00:27:47,200
 At the highest level, there's the Mara's.

367
00:27:47,200 --> 00:27:50,600
 I think it's the highest level.

368
00:27:50,600 --> 00:27:52,600
 Yeah, Paranimita Vasavati.

369
00:27:52,600 --> 00:27:58,680
 Paranimita Vasavati means the delight in the creations of

370
00:27:58,680 --> 00:27:59,880
 others.

371
00:27:59,880 --> 00:28:02,740
 Delight in the creations of others means they're happy that

372
00:28:02,740 --> 00:28:04,280
 we're all messed up down here and

373
00:28:04,280 --> 00:28:07,240
 that we're in chaos.

374
00:28:07,240 --> 00:28:08,560
 They enjoy watching this.

375
00:28:08,560 --> 00:28:11,620
 If you've ever, you know, in Hare Krishna, they talk about

376
00:28:11,620 --> 00:28:12,800
 the Krishna Leela.

377
00:28:12,800 --> 00:28:15,000
 This is all just Krishna's games.

378
00:28:15,000 --> 00:28:17,280
 It's kind of the same thing.

379
00:28:17,280 --> 00:28:22,340
 Krishna's maybe a Mara according to Buddhism, evil, because

380
00:28:22,340 --> 00:28:24,560
 they enjoy the fact that we're

381
00:28:24,560 --> 00:28:31,120
 stuck in samsara and they delight in watching us go to war

382
00:28:31,120 --> 00:28:34,240
 and all sorts of things.

383
00:28:34,240 --> 00:28:37,210
 Not just the bad stuff, but they just want to keep us in s

384
00:28:37,210 --> 00:28:37,880
amsara.

385
00:28:37,880 --> 00:28:41,290
 This is why according to the tradition, they don't like

386
00:28:41,290 --> 00:28:43,280
 Buddhists very much and they weren't

387
00:28:43,280 --> 00:28:45,160
 very happy about the Buddha.

388
00:28:45,160 --> 00:28:51,240
 They like playing games with us and so on.

389
00:28:51,240 --> 00:28:54,880
 Buddhist meditators who are trying very hard to leave sams

390
00:28:54,880 --> 00:28:57,040
ara will sometimes be confronted

391
00:28:57,040 --> 00:29:01,180
 by images and strange things and we often attribute that to

392
00:29:01,180 --> 00:29:03,200
 these angels who are trying

393
00:29:03,200 --> 00:29:04,760
 to get in our way.

394
00:29:04,760 --> 00:29:08,780
 There's actually stories in the suttas, so if you believe

395
00:29:08,780 --> 00:29:10,960
 it or not, it seems like there

396
00:29:10,960 --> 00:29:15,360
 was the idea that this was going on like silly things.

397
00:29:15,360 --> 00:29:19,930
 There was once this, the monks were meditating and this cow

398
00:29:19,930 --> 00:29:22,240
 came and threatened to knock

399
00:29:22,240 --> 00:29:25,480
 over their alms bowls that were all stacked up.

400
00:29:25,480 --> 00:29:29,850
 When the Buddha was there and the Buddha said, "That's Mara

401
00:29:29,850 --> 00:29:30,240
.

402
00:29:30,240 --> 00:29:32,800
 Don't let it get to you."

403
00:29:32,800 --> 00:29:37,070
 It was just a Mara giving them this vision and disturbing

404
00:29:37,070 --> 00:29:39,080
 their concentration.

405
00:29:39,080 --> 00:29:42,540
 You see this with monks sometimes thinking visions of

406
00:29:42,540 --> 00:29:44,600
 things and you often just think

407
00:29:44,600 --> 00:29:48,280
 it's their own minds creating it, but who knows?

408
00:29:48,280 --> 00:29:58,120
 Sometimes they talk to, they have voices in their head.

409
00:29:58,120 --> 00:30:03,560
 Just this idea of the fact that angels shouldn't be trusted

410
00:30:03,560 --> 00:30:03,960
.

411
00:30:03,960 --> 00:30:07,960
 For religious people, it does have a certain significance

412
00:30:07,960 --> 00:30:10,840
 because people in other religions,

413
00:30:10,840 --> 00:30:15,650
 of course in the time of the Buddha, there were plenty of

414
00:30:15,650 --> 00:30:18,400
 these, would talk about being

415
00:30:18,400 --> 00:30:22,120
 able to communicate with angels or God.

416
00:30:22,120 --> 00:30:26,460
 Nowadays people say they're talking to God and so on,

417
00:30:26,460 --> 00:30:28,040
 talking to angels.

418
00:30:28,040 --> 00:30:31,760
 In Thai Buddhism, many people talk about being the avatar,

419
00:30:31,760 --> 00:30:33,720
 so they hear from these angels

420
00:30:33,720 --> 00:30:40,170
 and they have teachers who tell them this and that, tell

421
00:30:40,170 --> 00:30:44,360
 them to do this and do that.

422
00:30:44,360 --> 00:30:46,940
 The idea is usually they think that the only criticism they

423
00:30:46,940 --> 00:30:48,240
 have, the only thing that they

424
00:30:48,240 --> 00:30:53,640
 have to face is the people's disbelief in the fact that

425
00:30:53,640 --> 00:30:57,040
 they're actually communicating

426
00:30:57,040 --> 00:30:58,040
 with these beings.

427
00:30:58,040 --> 00:31:02,640
 So as long as they can prove that they're actually

428
00:31:02,640 --> 00:31:06,360
 communicating with these beings,

429
00:31:06,360 --> 00:31:10,840
 then everything's all right.

430
00:31:10,840 --> 00:31:13,160
 So they ask themselves, "Am I really hearing this?"

431
00:31:13,160 --> 00:31:16,630
 And it's like, "Yes, I'm quite sure that this is a real

432
00:31:16,630 --> 00:31:18,560
 being, it's not just my mind coming

433
00:31:18,560 --> 00:31:19,560
 up with this."

434
00:31:19,560 --> 00:31:21,750
 So they think, "Okay, well that's enough for me to listen

435
00:31:21,750 --> 00:31:22,160
 to it."

436
00:31:22,160 --> 00:31:26,240
 In fact, it's a ridiculous conclusion.

437
00:31:26,240 --> 00:31:30,110
 It's the conclusion that these people seem to come to is, "

438
00:31:30,110 --> 00:31:32,160
Oh, this is real, so I better

439
00:31:32,160 --> 00:31:35,240
 follow what they say."

440
00:31:35,240 --> 00:31:39,650
 So this teaching that Mara is the highest angel is really a

441
00:31:39,650 --> 00:31:40,920
 stab at that.

442
00:31:40,920 --> 00:31:43,680
 Because how do you know that these angels have your best

443
00:31:43,680 --> 00:31:44,880
 interests at heart?

444
00:31:44,880 --> 00:31:47,520
 How do you know they're on your side?

445
00:31:47,520 --> 00:31:51,600
 Even if we accept the fact that you are actually talking to

446
00:31:51,600 --> 00:31:53,840
 angels or even God, how do you

447
00:31:53,840 --> 00:31:56,120
 know they're doing what's in your best interest?

448
00:31:56,120 --> 00:32:01,090
 Why do Christians believe that God has their best interests

449
00:32:01,090 --> 00:32:02,120
 at heart?

450
00:32:02,120 --> 00:32:11,350
 It's just to say that we're not slaves or play toys, an

451
00:32:11,350 --> 00:32:14,560
 experiment, depending on what

452
00:32:14,560 --> 00:32:21,360
 you believe.

453
00:32:21,360 --> 00:32:25,040
 Just because if you hear someone come in, if a human comes

454
00:32:25,040 --> 00:32:26,880
 in and tells you something,

455
00:32:26,880 --> 00:32:28,990
 do you immediately believe it because it's a human, because

456
00:32:28,990 --> 00:32:30,040
 they're real and you're not

457
00:32:30,040 --> 00:32:31,760
 imagining them?

458
00:32:31,760 --> 00:32:35,960
 Why would you do the same?

459
00:32:35,960 --> 00:32:40,320
 Why would you think any differently for angels?

460
00:32:40,320 --> 00:32:47,180
 Obviously, there's more power up in heaven, but they say

461
00:32:47,180 --> 00:32:49,040
 about power.

462
00:32:49,040 --> 00:32:52,960
 So that's the fifth Mara that we have to contend with.

463
00:32:52,960 --> 00:32:57,460
 If you hear anything or see anything or think you're in

464
00:32:57,460 --> 00:33:00,400
 communication with higher beings,

465
00:33:00,400 --> 00:33:07,390
 just understand that the world is full of evil, all realms,

466
00:33:07,390 --> 00:33:09,720
 all dimensions.

467
00:33:09,720 --> 00:33:11,800
 Everywhere there's good and evil.

468
00:33:11,800 --> 00:33:14,890
 We talk about things like evil and suffering because that's

469
00:33:14,890 --> 00:33:15,760
 the problem.

470
00:33:15,760 --> 00:33:18,090
 There's a lot of good in the world as well, even the human

471
00:33:18,090 --> 00:33:18,520
 world.

472
00:33:18,520 --> 00:33:21,130
 We shouldn't get down on ourselves or down on the world and

473
00:33:21,130 --> 00:33:26,320
 think it's full of evil either.

474
00:33:26,320 --> 00:33:29,750
 So once we've understood what is the true evil, then we can

475
00:33:29,750 --> 00:33:30,680
 deal with it.

476
00:33:30,680 --> 00:33:37,720
 Once that's gone, then no problem.

477
00:33:37,720 --> 00:33:41,680
 So that's the live Dhamma for today.

478
00:33:41,680 --> 00:33:45,120
 Thank you all for tuning in and have a great meditation.

479
00:33:46,720 --> 00:33:47,720
 You're welcome.

