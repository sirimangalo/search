1
00:00:00,000 --> 00:00:13,730
 [Q] I read your reply to my question, "whether past life

2
00:00:13,730 --> 00:00:16,000
 recollections are reliable?"

3
00:00:16,000 --> 00:00:19,620
 I'm confused that you said that in ultimate reality all

4
00:00:19,620 --> 00:00:22,680
 that exists is mind and body.

5
00:00:22,680 --> 00:00:24,680
 What do you mean?

6
00:00:24,680 --> 00:00:45,860
 [Mooji] I hesitate because it's a difficult question to

7
00:00:45,860 --> 00:00:46,680
 answer.

8
00:00:46,680 --> 00:00:51,790
 It's not a question that should be answered from an

9
00:00:51,790 --> 00:00:54,920
 intellectual point of view, because

10
00:00:54,920 --> 00:00:58,880
 I can give you the answer and I will.

11
00:00:58,880 --> 00:01:01,660
 I want to preface it by saying that the only thing that's

12
00:01:01,660 --> 00:01:03,220
 going to help is if you actually

13
00:01:03,220 --> 00:01:05,080
 start looking at ultimate reality.

14
00:01:05,080 --> 00:01:09,360
 This is the first thing you have to learn in meditation, is

15
00:01:09,360 --> 00:01:11,800
 what are the building blocks

16
00:01:11,800 --> 00:01:16,360
 of reality?

17
00:01:16,360 --> 00:01:17,840
 And now you know my answer.

18
00:01:17,840 --> 00:01:19,960
 You should see what your answer is.

19
00:01:19,960 --> 00:01:23,880
 When you look at ultimate reality, what do you see?

20
00:01:23,880 --> 00:01:36,640
 But the way you can understand my answer is in terms of how

21
00:01:36,640 --> 00:01:39,060
 I define ultimate reality.

22
00:01:39,060 --> 00:01:47,960
 What do I mean by ultimate reality?

23
00:01:47,960 --> 00:02:02,200
 I define reality based on the first ... the starting point

24
00:02:02,200 --> 00:02:04,280
 of everything.

25
00:02:04,280 --> 00:02:11,930
 Even if you do experiments on third-person, on impersonal,

26
00:02:11,930 --> 00:02:15,480
 "objective reality", physical

27
00:02:15,480 --> 00:02:19,000
 reality, the physical world out there, you're still

28
00:02:19,000 --> 00:02:21,560
 required to approach it from the point

29
00:02:21,560 --> 00:02:24,720
 of view of your own experiences.

30
00:02:24,720 --> 00:02:30,200
 There's this neat thing about quantum physics that requires

31
00:02:30,200 --> 00:02:32,200
 the experimenter.

32
00:02:32,200 --> 00:02:37,450
 You have to know what the observation is before you can

33
00:02:37,450 --> 00:02:40,920
 talk about what the results will be

34
00:02:40,920 --> 00:02:47,080
 without knowing ... without something being experienced.

35
00:02:47,080 --> 00:02:54,240
 There's no way to talk about the outcome.

36
00:02:54,240 --> 00:03:01,870
 This is the quandary that came up out of quantum physics,

37
00:03:01,870 --> 00:03:04,800
 as I understand it.

38
00:03:04,800 --> 00:03:07,840
 This is great evidence for the fact that reality has to

39
00:03:07,840 --> 00:03:10,220
 start with experience, which from Buddha's

40
00:03:10,220 --> 00:03:14,000
 point of view, it's really a silly ... from a person who's

41
00:03:14,000 --> 00:03:15,840
 seen ultimate reality, it's

42
00:03:15,840 --> 00:03:21,440
 really a silly thing to even talk about.

43
00:03:21,440 --> 00:03:26,820
 Because obviously, what exists, right here, right now, is

44
00:03:26,820 --> 00:03:28,200
 what exists.

45
00:03:28,200 --> 00:03:29,200
 Experience is what exists.

46
00:03:29,200 --> 00:03:33,600
 There's nothing else that exists.

47
00:03:33,600 --> 00:03:34,600
 The sun doesn't exist.

48
00:03:34,600 --> 00:03:35,600
 Pluto doesn't exist.

49
00:03:35,600 --> 00:03:37,840
 Mars doesn't exist.

50
00:03:37,840 --> 00:03:39,520
 Solar systems don't exist.

51
00:03:39,520 --> 00:03:42,640
 All of these things are ... they only exist up here.

52
00:03:42,640 --> 00:03:43,640
 They exist in our mind.

53
00:03:43,640 --> 00:03:45,360
 We think about them.

54
00:03:45,360 --> 00:03:48,920
 We say, "Yes, that's the sun.

55
00:03:48,920 --> 00:03:50,760
 That's the moon.

56
00:03:50,760 --> 00:03:51,760
 This is grass.

57
00:03:51,760 --> 00:03:53,800
 This is a tree," and so on.

58
00:03:53,800 --> 00:03:55,200
 But actually, what is it?

59
00:03:55,200 --> 00:03:58,370
 It's seeing, hearing, smelling, tasting, feeling, and

60
00:03:58,370 --> 00:03:59,060
 thinking.

61
00:03:59,060 --> 00:04:02,800
 That's all that's real.

62
00:04:02,800 --> 00:04:08,080
 These are the only aspects of ultimate reality, meaning

63
00:04:08,080 --> 00:04:11,320
 they're the only things that can be

64
00:04:11,320 --> 00:04:12,920
 experienced.

65
00:04:12,920 --> 00:04:15,140
 All that can ever possibly be experienced is seeing,

66
00:04:15,140 --> 00:04:18,320
 hearing, smelling, tasting, feeling,

67
00:04:18,320 --> 00:04:20,600
 and thinking.

68
00:04:20,600 --> 00:04:23,280
 What are the constituents of these six things?

69
00:04:23,280 --> 00:04:27,900
 What are the constituents of these experiences, of

70
00:04:27,900 --> 00:04:29,200
 experience in general?

71
00:04:29,200 --> 00:04:31,950
 If you observe it, you will see that there are two

72
00:04:31,950 --> 00:04:33,060
 constituents.

73
00:04:33,060 --> 00:04:37,720
 One is the physical, one is the mental.

74
00:04:37,720 --> 00:04:40,110
 For example, when the stomach rises ... Well, the stomach

75
00:04:40,110 --> 00:04:42,280
 is ... Theoretically, it's rising

76
00:04:42,280 --> 00:04:45,560
 and falling all the time.

77
00:04:45,560 --> 00:04:48,240
 But if the mind is not there, there's no experience of it.

78
00:04:48,240 --> 00:04:51,440
 Without the mental, there's no experience of rising.

79
00:04:51,440 --> 00:04:55,220
 But if the stomach doesn't rise, then even if the mind is

80
00:04:55,220 --> 00:04:57,280
 there, there will still not

81
00:04:57,280 --> 00:04:59,160
 be the experience of rising.

82
00:04:59,160 --> 00:05:03,640
 Without the body and the mind, the feeling of rising can't

83
00:05:03,640 --> 00:05:04,460
 arise.

84
00:05:04,460 --> 00:05:08,520
 If your eye is good and there's light touching the eye, but

85
00:05:08,520 --> 00:05:10,680
 your mind is somewhere else,

86
00:05:10,680 --> 00:05:12,220
 there's no seeing.

87
00:05:12,220 --> 00:05:14,520
 If your eyes are not good and the mind is there, there

88
00:05:14,520 --> 00:05:15,540
 still is no seeing.

89
00:05:15,540 --> 00:05:18,420
 If your eyes are closed, even if you put your eyes and your

90
00:05:18,420 --> 00:05:19,920
 mind there, you still don't

91
00:05:19,920 --> 00:05:21,580
 see anything.

92
00:05:21,580 --> 00:05:24,140
 If the lights are out, if you turn out the lights, you

93
00:05:24,140 --> 00:05:25,680
 still don't see anything.

94
00:05:25,680 --> 00:05:28,450
 The physical and the mental are the two constituents of

95
00:05:28,450 --> 00:05:29,280
 experience.

96
00:05:29,280 --> 00:05:33,770
 That's what is meant by body and mind as all that exists,

97
00:05:33,770 --> 00:05:36,400
 because we define existence as

98
00:05:36,400 --> 00:05:39,150
 experience and we're only ... Whether you want to define

99
00:05:39,150 --> 00:05:40,640
 existence as something else,

100
00:05:40,640 --> 00:05:42,480
 we're not concerned with that.

101
00:05:42,480 --> 00:05:46,200
 All that we're concerned with as Buddhists is the reality

102
00:05:46,200 --> 00:05:47,480
 of experience.

103
00:05:47,480 --> 00:05:51,880
 That to us is their equivalent reality and experience.

