1
00:00:00,000 --> 00:00:04,270
 In the teachings of the Buddha, you know, the virtue of the

2
00:00:04,270 --> 00:00:08,000
 seed, we call it "seela", the keeping of precepts.

3
00:00:08,000 --> 00:00:11,250
 The virtue of, I mean, the purity of virtue is very

4
00:00:11,250 --> 00:00:15,020
 important because it is here said, the firm ground of

5
00:00:15,020 --> 00:00:16,000
 virtue, right?

6
00:00:16,000 --> 00:00:20,000
 Right, on the feet of energy on the ground of virtue.

7
00:00:20,000 --> 00:00:26,300
 Yes, so, seela is compared to ground and energy is compared

8
00:00:26,300 --> 00:00:32,000
 to the feet and faith or confidence is compared to hand.

9
00:00:32,000 --> 00:00:35,200
 Because if you have a hand, you can pick up things you want

10
00:00:35,200 --> 00:00:38,000
 to pick up. If you do not have hands, you cannot.

11
00:00:38,000 --> 00:00:41,870
 In the same way, if you do not have any confidence, then

12
00:00:41,870 --> 00:00:45,000
 you cannot pick up crucial or good things.

13
00:00:45,000 --> 00:00:52,210
 So, the hand is compared to, I mean, confidence of faith is

14
00:00:52,210 --> 00:00:54,000
 compared to a hand.

15
00:00:54,000 --> 00:00:59,430
 And the food, maga, is compared to an "X", which cuts all

16
00:00:59,430 --> 00:01:03,000
 together the mental defilements.

17
00:01:03,000 --> 00:01:06,740
 So, if your conduct isn't purely virtuous, then there's no

18
00:01:06,740 --> 00:01:09,440
 way that you're not going to be involved in that creative

19
00:01:09,440 --> 00:01:10,000
 karma.

20
00:01:10,000 --> 00:01:13,600
 Because if your value is not virtuous, then whatever you

21
00:01:13,600 --> 00:01:17,000
 call yourself, you're still going to be created.

22
00:01:17,000 --> 00:01:18,000
 That's right, yes.

23
00:01:18,000 --> 00:01:22,000
 What is an example of functional?

24
00:01:22,000 --> 00:01:28,000
 Functional is something like, I gave this example, but T

25
00:01:28,000 --> 00:01:28,000
singhua didn't like that.

26
00:01:28,000 --> 00:01:35,000
 He said it was too, maybe, I don't know, it's like a fruit,

27
00:01:35,000 --> 00:01:39,000
 which does not bear fruit.

28
00:01:39,000 --> 00:01:44,230
 It's still growing, living, it still has branches and

29
00:01:44,230 --> 00:01:48,000
 leaves and so on, but it does not bear fruit.

30
00:01:48,000 --> 00:01:49,000
 Something like that.

31
00:01:49,000 --> 00:01:55,000
 An Araham, after becoming an Araham, still do good things.

32
00:01:55,000 --> 00:01:59,690
 They still teach people, still practice meditation and

33
00:01:59,690 --> 00:02:02,000
 still help other people.

34
00:02:02,000 --> 00:02:10,020
 But all their actions do not constitute karma because they

35
00:02:10,020 --> 00:02:17,810
 have eradicated the root of existence, that is, the craving

36
00:02:17,810 --> 00:02:20,000
 and ignorance.

37
00:02:20,000 --> 00:02:26,550
 So, their actions are just actions and their actions do not

38
00:02:26,550 --> 00:02:31,000
 bring reactions, I mean, results.

39
00:02:33,000 --> 00:02:43,850
 We, in Burma, at the New Year ceremony, the cannons were

40
00:02:43,850 --> 00:02:46,000
 fired.

41
00:02:46,000 --> 00:02:50,900
 And so, when something is just noise and has no substance,

42
00:02:50,900 --> 00:02:56,000
 we say, this is the New Year cannon or something like that.

43
00:02:56,000 --> 00:02:58,000
 What do you call this?

44
00:02:58,000 --> 00:03:02,000
 Volley of something?

45
00:03:02,000 --> 00:03:06,000
 So, it is something like that.

46
00:03:06,000 --> 00:03:11,000
 Fruit, I mean, trees bear no fruit.

47
00:03:11,000 --> 00:03:13,000
 They are still fruit.

48
00:03:13,000 --> 00:03:17,000
 As a lid of yes.

49
00:03:17,000 --> 00:03:25,000
 Second one, Samba Sambuddha.

50
00:03:25,000 --> 00:03:33,950
 Now, Buddha, we use the word Buddha to refer to him, but

51
00:03:33,950 --> 00:03:41,000
 his full epithet is Samma Sambuddha.

52
00:03:41,000 --> 00:03:45,000
 Now, Samma is one prefix, Sam is one prefix.

53
00:03:45,000 --> 00:03:48,000
 There are two prefixes here, and Buddha.

54
00:03:48,000 --> 00:03:54,000
 Samma means rightly, and Sam means by himself.

55
00:03:54,000 --> 00:03:58,000
 So, he discovers the Four Noble Truths.

56
00:03:58,000 --> 00:04:01,000
 Buddha means to know.

57
00:04:01,000 --> 00:04:04,530
 So, he knows the Four Noble Truths, or he discovers the

58
00:04:04,530 --> 00:04:09,450
 Four Noble Truths, rightly and without assistance from any

59
00:04:09,450 --> 00:04:11,000
 person, by himself.

60
00:04:11,000 --> 00:04:15,000
 So, we emphasize this when we talk about Buddha.

61
00:04:15,000 --> 00:04:20,000
 Buddha are those persons who do not need any teachers.

62
00:04:20,000 --> 00:04:25,930
 So, Bodhisattva went to two teachers before he became the

63
00:04:25,930 --> 00:04:31,660
 Buddha, but he did not follow their advice up to the

64
00:04:31,660 --> 00:04:34,000
 attainment of Bodho.

65
00:04:34,000 --> 00:04:40,160
 He discovered their practice and then went to a place and

66
00:04:40,160 --> 00:04:43,000
 practiced by himself.

67
00:04:43,000 --> 00:04:47,000
 So, we say Bodha had no teachers.

68
00:04:47,000 --> 00:04:49,000
 And this is emphasized by Sambuddha.

69
00:04:49,000 --> 00:04:53,810
 So, Sambuddha means self-enlightened, and Samma Bodha means

70
00:04:53,810 --> 00:04:56,000
 rightly enlightened.

71
00:04:56,000 --> 00:05:00,860
 So, Samma Sambuddha means rightly self-enlightened,

72
00:05:00,860 --> 00:05:03,000
 something like that.

73
00:05:03,000 --> 00:05:10,000
 And here, to know means to know the Four Noble Truths.

74
00:05:10,000 --> 00:05:18,240
 And then, on page 211, paragraph 28, the other things which

75
00:05:18,240 --> 00:05:21,000
 Bodha knew are given.

76
00:05:21,000 --> 00:05:24,720
 There are six bases, six groups of consciousness, and so on

77
00:05:24,720 --> 00:05:25,000
.

78
00:05:25,000 --> 00:05:30,000
 So, these are the topics taught in Abhidhamma.

79
00:05:30,000 --> 00:05:34,010
 Actually, they are found in Sotabhita Ghatu, but their

80
00:05:34,010 --> 00:05:38,000
 treatment in food can be found in Abhidhamma.

81
00:05:38,000 --> 00:05:44,910
 Sometimes, people take Abhidhamma to be a separate thing,

82
00:05:44,910 --> 00:05:50,000
 not connected with a sutta or something like that.

83
00:05:50,000 --> 00:05:55,610
 But actually, those we found in Abhidhamma, most of them

84
00:05:55,610 --> 00:05:59,000
 are also taught in Sotabhita Ghatu.

85
00:05:59,000 --> 00:06:02,280
 That is why we need the knowledge of Abhidhamma to

86
00:06:02,280 --> 00:06:04,000
 understand the sutas.

87
00:06:04,000 --> 00:06:12,000
 We should not treat them as separate.

88
00:06:12,000 --> 00:06:18,000
 We cannot study sutas without a knowledge of Abhidhamma.

89
00:06:18,000 --> 00:06:24,000
 That means, if we want to understand fully and correctly.

90
00:06:24,000 --> 00:06:29,110
 So, these are topics taught in Abhidhamma and Sotabhita

91
00:06:29,110 --> 00:06:30,000
 also.

92
00:06:30,000 --> 00:06:47,250
 So, here, Bodha's penetration into the four noble truths,

93
00:06:47,250 --> 00:06:50,000
 or discovery of four noble truths,

94
00:06:50,000 --> 00:07:01,000
 is meant by this second attribute, Sambuddha.

95
00:07:01,000 --> 00:07:10,950
 Now, Mahayana, it's usually thought that an arhat is one

96
00:07:10,950 --> 00:07:16,000
 who is enlightened by himself.

97
00:07:16,000 --> 00:07:20,000
 Well, actually, it's a Pajigabuddha Buddha.

98
00:07:20,000 --> 00:07:25,000
 Pajigabuddha is one who is enlightened by himself.

99
00:07:25,000 --> 00:07:29,330
 That's usually distinguished from a Buddha who isn't

100
00:07:29,330 --> 00:07:34,880
 totally enlightened by himself, or in the vacuum with other

101
00:07:34,880 --> 00:07:36,000
 people.

102
00:07:36,000 --> 00:07:44,550
 The Pajigabuddhas are not called Samma Sambuddha. They are

103
00:07:44,550 --> 00:07:48,290
 called just Sambuddha, or Pajigabuddhas, but not Samma Samb

104
00:07:48,290 --> 00:07:49,000
uddha.

105
00:07:49,000 --> 00:07:51,000
 So, they're not rightly?

106
00:07:51,000 --> 00:07:58,710
 Rightly means in all aspects. They do not understand in all

107
00:07:58,710 --> 00:08:02,000
 aspects of the Dhamma.

108
00:08:02,000 --> 00:08:08,330
 They are self-enlightened persons, but their knowledge may

109
00:08:08,330 --> 00:08:17,000
 be not as wide, as comprehensive as that of the Buddhas.

110
00:08:17,000 --> 00:08:22,000
 And also, sometimes they are called silent Buddhas.

111
00:08:22,000 --> 00:08:27,000
 That comes from the fact that they do not teach much.

112
00:08:27,000 --> 00:08:32,170
 Although sometimes they teach, but they seldom teach. They

113
00:08:32,170 --> 00:08:34,000
 want to be away from people and live in the forest.

114
00:08:34,000 --> 00:08:37,000
 And so, they are called silent Buddhas.

115
00:08:37,000 --> 00:08:40,310
 Well, sometimes it's been said because they weren't taught,

116
00:08:40,310 --> 00:08:42,000
 it's hard for them to teach.

117
00:08:42,000 --> 00:08:46,500
 And that's usually kind of the conscious distinction of the

118
00:08:46,500 --> 00:08:49,000
 Buddha, but only the...

119
00:08:49,000 --> 00:08:55,000
 And also, they appear only when there are no Buddhas.

120
00:08:55,000 --> 00:09:05,000
 So, they appear between interrains of Buddhas.

121
00:09:05,000 --> 00:09:11,710
 And then, I hope you read the footnote. The footnotes are

122
00:09:11,710 --> 00:09:13,000
 very good.

123
00:09:13,000 --> 00:09:16,000
 So, we'll go next to the third one.

124
00:09:16,000 --> 00:09:21,000
 He is endowed with clear vision and virtuous conduct.

125
00:09:21,000 --> 00:09:26,000
 There are two words here, "vijja" and "charanā".

126
00:09:26,000 --> 00:09:31,540
 "Vijja" means understanding or vision, and "charanā" means

127
00:09:31,540 --> 00:09:33,000
 conduct.

128
00:09:33,000 --> 00:09:39,000
 So, Buddha is endowed with both clear vision and conduct.

129
00:09:39,000 --> 00:09:42,000
 There are three kinds of clear vision.

130
00:09:42,000 --> 00:09:48,850
 That means remembering his past lives and then divine eye

131
00:09:48,850 --> 00:09:53,000
 and destruction of defilements.

132
00:09:53,000 --> 00:09:57,000
 They are called three clear visions.

133
00:09:57,000 --> 00:10:02,000
 So, whenever three is mentioned, these are meant.

134
00:10:02,000 --> 00:10:09,880
 Remembering past lives, divine vision and destruction of

135
00:10:09,880 --> 00:10:16,000
 defilements, these are called three clear visions.

136
00:10:16,000 --> 00:10:19,000
 Sometimes, eight kinds of clear visions are mentioned.

137
00:10:19,000 --> 00:10:26,000
 So, in that case, there are some more.

138
00:10:26,000 --> 00:10:32,000
 So, the eight kinds are stated in the "ambhata" sutta.

139
00:10:32,000 --> 00:10:35,000
 For there, eight kinds of clear vision are stated.

140
00:10:35,000 --> 00:10:39,490
 Eight are made up of six kinds of direct knowledge,

141
00:10:39,490 --> 00:10:43,090
 together with insight and the supernormal power of the mind

142
00:10:43,090 --> 00:10:44,000
-made body.

143
00:10:44,000 --> 00:10:47,110
 So, there are eight kinds of clear visions mentioned in

144
00:10:47,110 --> 00:10:48,000
 that sutta.

145
00:10:48,000 --> 00:10:52,370
 But if you go to that sutta, you will not find them clearly

146
00:10:52,370 --> 00:10:53,000
 stated.

147
00:10:53,000 --> 00:11:00,140
 Because in Pali, when they don't want to repeat, they just

148
00:11:00,140 --> 00:11:02,000
 put a sign there.

149
00:11:02,000 --> 00:11:08,000
 Like you use dots, right? Three or four dots.

150
00:11:08,000 --> 00:11:16,040
 So, in Pali also, they use the letter "p-a" or sometimes "l

151
00:11:16,040 --> 00:11:17,000
-a".

152
00:11:17,000 --> 00:11:21,270
 So, when you see this, you understand that there are some

153
00:11:21,270 --> 00:11:24,000
 things missing here, or they don't want to repeat.

154
00:11:24,000 --> 00:11:27,000
 So, you have to go back to where they first occurred.

155
00:11:27,000 --> 00:11:32,160
 And if you don't know where to find them, then you will be

156
00:11:32,160 --> 00:11:33,000
 lost.

157
00:11:33,000 --> 00:11:43,680
 So, today I looked them up in page 100, and they were not

158
00:11:43,680 --> 00:11:47,000
 printed in the book.

159
00:11:47,000 --> 00:11:51,440
 Because they were mentioned in the second sutta, the "ambh

160
00:11:51,440 --> 00:11:53,000
ata" sutta, the third sutta.

161
00:11:53,000 --> 00:11:59,000
 So, this is one problem with us modern people.

162
00:11:59,000 --> 00:12:03,380
 These books are meant to be read from the beginning to the

163
00:12:03,380 --> 00:12:04,000
 end.

164
00:12:04,000 --> 00:12:08,330
 Not just pick up a sutta in the middle and you read it and

165
00:12:08,330 --> 00:12:10,000
 you understand.

166
00:12:10,000 --> 00:12:16,000
 So, they are meant to be studied from the beginning.

167
00:12:16,000 --> 00:12:23,000
 I can give you the page numbers of the English translation.

168
00:12:23,000 --> 00:12:27,000
 So, the eight are the six kinds of direct knowledge.

169
00:12:27,000 --> 00:12:35,000
 That means the performing miracles, that is one.

170
00:12:35,000 --> 00:12:42,230
 And then divine ear, and then reading other people's minds,

171
00:12:42,230 --> 00:12:49,000
 and remembering past lives, and then divine eye.

172
00:12:49,000 --> 00:12:51,000
 You get five, right?

173
00:12:51,000 --> 00:12:56,000
 And then destruction of mental development, six.

174
00:12:56,000 --> 00:12:58,000
 And then two are mentioned here.

175
00:12:58,000 --> 00:13:03,270
 Vipassana, insight, and supernormal power of the mind-made

176
00:13:03,270 --> 00:13:04,000
 body.

177
00:13:04,000 --> 00:13:08,720
 That means you practice jhana and then you are able to

178
00:13:08,720 --> 00:13:11,000
 multiply yourself.

179
00:13:11,000 --> 00:13:15,700
 So, these are the eight kinds of clear vision stated in

180
00:13:15,700 --> 00:13:17,000
 that sutta.

181
00:13:17,000 --> 00:13:21,000
 And by contact is meant 15 things here.

182
00:13:21,000 --> 00:13:24,000
 Restrained by virtue, guarding the doors of the sense

183
00:13:24,000 --> 00:13:27,000
 faculties, knowledge of the right amount in eating,

184
00:13:27,000 --> 00:13:30,400
 devotion to wakefulness, the seven good states there on the

185
00:13:30,400 --> 00:13:31,000
 footnote,

186
00:13:31,000 --> 00:13:35,000
 and the four jhanas of the fine material sphere.

187
00:13:35,000 --> 00:13:38,450
 For it is precisely by means of these 15 things that a

188
00:13:38,450 --> 00:13:41,000
 noble disciple contacts himself,

189
00:13:41,000 --> 00:13:43,000
 that he goes towards the deadness.

190
00:13:43,000 --> 00:13:46,160
 That is why it is called virtue, conduct, according to what

191
00:13:46,160 --> 00:13:47,000
 it is said.

192
00:13:47,000 --> 00:13:51,390
 Now, you have to understand the Pali word charana, to

193
00:13:51,390 --> 00:13:54,000
 understand this explanation.

194
00:13:54,000 --> 00:13:58,000
 The word charana comes from the root chara, c-a-r-a.

195
00:13:58,000 --> 00:14:02,000
 And chara means to walk, to go.

196
00:14:02,000 --> 00:14:07,000
 So charana means some means of going.

197
00:14:07,000 --> 00:14:14,140
 So these 15 are the means of going to the direction of nip

198
00:14:14,140 --> 00:14:15,000
ana.

199
00:14:15,000 --> 00:14:19,000
 And this is why it is said here,

200
00:14:19,000 --> 00:14:23,600
 it is precisely by means of these 15 things that a noble

201
00:14:23,600 --> 00:14:25,000
 disciple contacts himself,

202
00:14:25,000 --> 00:14:27,000
 that he goes towards the deadness.

203
00:14:27,000 --> 00:14:31,000
 That is why it is called Pali charana.

204
00:14:31,000 --> 00:14:42,000
 And then we will go to the next one.

205
00:14:42,000 --> 00:14:46,000
 Sublime, paragraph 33.

206
00:14:46,000 --> 00:14:53,000
 Now please remember the Pali word here, sugata,

207
00:14:53,000 --> 00:14:59,390
 because the English translation sublime does not make much

208
00:14:59,390 --> 00:15:01,000
 sense here.

209
00:15:01,000 --> 00:15:08,000
 So sugata, su means here,

210
00:15:08,000 --> 00:15:12,000
 the first meaning is good.

211
00:15:12,000 --> 00:15:16,000
 So su means good, and kata means going.

212
00:15:16,000 --> 00:15:19,000
 So sugata means good going.

213
00:15:19,000 --> 00:15:22,000
 That means Buddha has a good going.

214
00:15:22,000 --> 00:15:28,000
 That is why Buddha is called sugata,

215
00:15:28,000 --> 00:15:33,000
 because his going is purified and blameless.

216
00:15:33,000 --> 00:15:37,000
 And what is that? That is a noble path.

217
00:15:37,000 --> 00:15:44,000
 So noble efulpa is called, he has a good going.

218
00:15:44,000 --> 00:15:48,000
 So that is the first meaning, sugata.

219
00:15:48,000 --> 00:15:55,000
 Now the second meaning is also,

220
00:15:55,000 --> 00:15:58,000
 gone to an excellent place.

221
00:15:58,000 --> 00:16:04,000
 Now bia gata means gone, not like in the first meaning.

222
00:16:04,000 --> 00:16:06,000
 In the first meaning gata means going.

223
00:16:06,000 --> 00:16:10,000
 But in the second meaning gata means gone to.

224
00:16:10,000 --> 00:16:13,000
 Su means an excellent place.

225
00:16:13,000 --> 00:16:17,000
 So one who has gone to an excellent place is called sugata.

226
00:16:17,000 --> 00:16:21,000
 Here an excellent place is nibana.

227
00:16:21,000 --> 00:16:26,000
 Now the third meaning, of having gone rightly,

228
00:16:26,000 --> 00:16:30,000
 now su can mean also rightly.

229
00:16:30,000 --> 00:16:36,000
 So who has gone rightly is called sugata.

230
00:16:36,000 --> 00:16:43,000
 And rightly here means not approaching to the extremes,

231
00:16:43,000 --> 00:16:47,000
 but following the middle path.

232
00:16:47,000 --> 00:16:52,100
 And the last one, the fourth meaning, because of en

233
00:16:52,100 --> 00:16:54,000
unciation rightly.

234
00:16:54,000 --> 00:16:59,500
 Now here the original word is sugata, with a d, d as in

235
00:16:59,500 --> 00:17:01,000
 David.

236
00:17:01,000 --> 00:17:05,000
 And that d is changed to t.

237
00:17:05,000 --> 00:17:09,000
 It is a polygrammatical explanation.

238
00:17:09,000 --> 00:17:13,000
 So here it means sugata.

239
00:17:13,000 --> 00:17:16,000
 Gata means speaking or saying.

240
00:17:16,000 --> 00:17:18,000
 And su means rightly.

241
00:17:18,000 --> 00:17:22,000
 So one who speaks rightly is called sugata.

242
00:17:22,000 --> 00:17:30,000
 So there are four meanings to this word.

243
00:17:30,000 --> 00:17:36,000
 So with regard to the fourth meaning,

244
00:17:36,000 --> 00:17:41,000
 the sudah is coded here, paragraph 35.

245
00:17:41,000 --> 00:17:44,580
 Such speech as the perfect one knows to be untrue and

246
00:17:44,580 --> 00:17:45,000
 incorrect,

247
00:17:45,000 --> 00:17:49,000
 conducive to harm and displeasing and unwelcome to others,

248
00:17:49,000 --> 00:17:51,000
 that he does not speak and so on.

249
00:17:51,000 --> 00:17:56,740
 Now according to this sudah, there are six kinds of

250
00:17:56,740 --> 00:17:58,000
 speeches.

251
00:17:58,000 --> 00:18:04,000
 And only two of them, the Buddha's use.

252
00:18:04,000 --> 00:18:09,130
 So those six are mentioned here in the translation of the

253
00:18:09,130 --> 00:18:10,000
 sudahs.

254
00:18:10,000 --> 00:18:16,940
 So in brief they mean speech which is untrue, harmful and

255
00:18:16,940 --> 00:18:19,000
 displeasing.

256
00:18:19,000 --> 00:18:24,000
 Bodhas do not use such speech.

257
00:18:24,000 --> 00:18:29,070
 And then speech which is true but harmful and displeasing

258
00:18:29,070 --> 00:18:30,000
 to the heiress.

259
00:18:30,000 --> 00:18:33,000
 This also Bodhas do not use.

260
00:18:33,000 --> 00:18:38,000
 And the third one is true, beneficial.

261
00:18:38,000 --> 00:18:40,000
 That means not harming.

262
00:18:40,000 --> 00:18:46,150
 True, beneficial but displeasing to the listeners that Bod

263
00:18:46,150 --> 00:18:48,000
has use.

264
00:18:48,000 --> 00:18:59,000
 But here in the translation it is said,

265
00:18:59,000 --> 00:19:03,000
 perfect one knows the time to expound.

266
00:19:03,000 --> 00:19:06,370
 That means if it is time to say such a speech then Bodhas

267
00:19:06,370 --> 00:19:07,000
 use this speech.

268
00:19:07,000 --> 00:19:11,360
 So it may not be pleasing to the listeners but if it is

269
00:19:11,360 --> 00:19:14,000
 beneficial and it is true then Bodha will say.

270
00:19:14,000 --> 00:19:22,530
 Why does the meaning of harmful, which harmful and

271
00:19:22,530 --> 00:19:27,000
 embarrassing?

272
00:19:27,000 --> 00:19:33,000
 If you follow his words then you will get benefits.

273
00:19:33,000 --> 00:19:38,000
 Or if you follow his words you will come to the God.

274
00:19:38,000 --> 00:19:41,000
 Opposite of that.

275
00:19:41,000 --> 00:19:46,000
 You will come to failure or something.

276
00:19:46,000 --> 00:19:51,000
 Sometimes people talk to other people.

277
00:19:51,000 --> 00:19:54,000
 People give advice to other people.

278
00:19:54,000 --> 00:20:02,000
 And those advice may not be conducive to success.

279
00:20:02,000 --> 00:20:08,650
 So which is true, beneficial and displeasing Bodhas use

280
00:20:08,650 --> 00:20:11,000
 such speeches.

281
00:20:11,000 --> 00:20:17,440
 Then the fourth one, untrue, harmful but pleasing to other

282
00:20:17,440 --> 00:20:19,000
 people.

283
00:20:19,000 --> 00:20:22,000
 Bodhas never use this.

284
00:20:22,000 --> 00:20:26,000
 And then true, harmful, pleasing.

285
00:20:26,000 --> 00:20:30,000
 No, Bodha never use.

286
00:20:30,000 --> 00:20:33,880
 Then the last one, true, beneficial and pleasing to the

287
00:20:33,880 --> 00:20:35,000
 listeners.

288
00:20:35,000 --> 00:20:39,000
 So Bodhas use only two kinds of speeches.

289
00:20:39,000 --> 00:20:44,990
 True, beneficial, displeasing or true, beneficial, pleasing

290
00:20:44,990 --> 00:20:45,000
.

291
00:20:45,000 --> 00:20:47,000
 The others Bodha do not use.

292
00:20:47,000 --> 00:20:54,240
 That is why Bodha is described as sukata, one who speaks

293
00:20:54,240 --> 00:20:57,000
 rightly.

294
00:20:57,000 --> 00:21:02,000
 Then no one of the world, loka-widu.

295
00:21:02,000 --> 00:21:06,000
 You may have read this.

296
00:21:06,000 --> 00:21:12,250
 It is something like, in any comment today, something like

297
00:21:12,250 --> 00:21:14,000
 a Buddhist cosmology.

298
00:21:14,000 --> 00:21:21,000
 And not all of them can actually be found in the Sudha.

299
00:21:21,000 --> 00:21:23,000
 Some of them can be found in the Sudha.

300
00:21:23,000 --> 00:21:28,950
 And others are developed maybe later after the death of the

301
00:21:28,950 --> 00:21:30,000
 Bodha.

302
00:21:30,000 --> 00:21:37,000
 So you may take them as just what you call that.

303
00:21:37,000 --> 00:21:42,000
 The great result.

304
00:21:42,000 --> 00:21:46,000
 You may take them as true or not.

305
00:21:46,000 --> 00:21:53,000
 Because if you look at the world right now,

306
00:21:53,000 --> 00:21:57,470
 according to the knowledge of modern science about the

307
00:21:57,470 --> 00:21:58,000
 world,

308
00:21:58,000 --> 00:22:05,000
 then they are very different from.

309
00:22:05,000 --> 00:22:13,000
 Indians are, maybe the western people say Indians are,

310
00:22:13,000 --> 00:22:18,470
 are fond of using numbers, say 84,000 or something like

311
00:22:18,470 --> 00:22:19,000
 that.

312
00:22:19,000 --> 00:22:24,000
 So here also, lower of the world.

313
00:22:24,000 --> 00:22:27,000
 Now, there are three kinds of worlds mentioned here.

314
00:22:27,000 --> 00:22:31,360
 The world of formations, the world of beings and the world

315
00:22:31,360 --> 00:22:33,000
 of location.

316
00:22:33,000 --> 00:22:38,000
 You will find that in paragraph 37.

317
00:22:38,000 --> 00:22:41,910
 The world of formations, the world of beings and the world

318
00:22:41,910 --> 00:22:43,000
 of location.

319
00:22:43,000 --> 00:22:46,980
 The world of formations really means the world of both

320
00:22:46,980 --> 00:22:50,000
 animate and inanimate things.

321
00:22:50,000 --> 00:22:53,000
 Both beings and inanimate things.

322
00:22:53,000 --> 00:22:55,600
 The world of beings means just beings and the world of

323
00:22:55,600 --> 00:22:59,000
 location means outside world.

324
00:22:59,000 --> 00:23:07,100
 And so Buddha, I mean inanimate world, like the Earth and

325
00:23:07,100 --> 00:23:09,000
 the mountain rivers and so on.

326
00:23:09,000 --> 00:23:18,000
 And if you can draw a diagram of what is mentioned here,

327
00:23:18,000 --> 00:23:23,400
 you will get a rough picture of the Buddhist conception of

328
00:23:23,400 --> 00:23:28,000
 the world and the other things.

329
00:23:28,000 --> 00:23:36,000
 The suns and moons and the universe.

330
00:23:36,000 --> 00:23:40,840
 Then in paragraph 38, the first word likewise I think

331
00:23:40,840 --> 00:23:42,000
 should go.

332
00:23:42,000 --> 00:23:51,000
 Not likewise, but it should be for or maybe in detail.

333
00:23:51,000 --> 00:23:57,580
 Because paragraph 38 and those following are the detailed

334
00:23:57,580 --> 00:24:03,000
 description of how Buddha knows the world.

335
00:24:03,000 --> 00:24:11,970
 So one world being all beings subsist by a new human and so

336
00:24:11,970 --> 00:24:13,000
 on.

337
00:24:13,000 --> 00:24:17,270
 Then paragraph 39, he knows all beings' habits. That is

338
00:24:17,270 --> 00:24:18,000
 very important.

339
00:24:18,000 --> 00:24:22,140
 He knows all beings' habits. Knows their inherent

340
00:24:22,140 --> 00:24:25,000
 tendencies. Knows their temperaments. Knows their bench.

341
00:24:25,000 --> 00:24:28,480
 Knows them with little dust in their eyes and with much

342
00:24:28,480 --> 00:24:30,000
 dust on their eyes.

343
00:24:30,000 --> 00:24:33,000
 With king faculties and with dalfa faculties.

344
00:24:33,000 --> 00:24:35,000
 With good behavior and with bad behavior.

345
00:24:35,000 --> 00:24:38,000
 Each to teach and easy to teach and hard to teach.

346
00:24:38,000 --> 00:24:41,000
 Capable and incapable of achievement.

347
00:24:41,000 --> 00:24:44,710
 Therefore this world of beings was known to him in all ways

348
00:24:44,710 --> 00:24:45,000
.

349
00:24:45,000 --> 00:24:52,330
 So only Bodhas have this ability of knowing exactly the

350
00:24:52,330 --> 00:24:56,000
 beings' habits, their inherent tendencies and so on.

351
00:24:56,000 --> 00:25:01,000
 Even the Arahants do not possess this ability.

352
00:25:01,000 --> 00:25:09,040
 And then Bodhas' understanding of the physical world may be

353
00:25:09,040 --> 00:25:11,000
 developed.

354
00:25:11,000 --> 00:25:19,000
 There are some statements with regard to the physical world

355
00:25:19,000 --> 00:25:23,000
 and then they are developed later on.

356
00:25:23,000 --> 00:25:28,250
 So this is the description of the world according to say

357
00:25:28,250 --> 00:25:30,000
 the Buddhists.

358
00:25:30,000 --> 00:25:36,090
 And also the footnote is very helpful and informative with

359
00:25:36,090 --> 00:25:38,000
 regard to this.

360
00:25:38,000 --> 00:25:46,000
 You will get a view of the Buddhist universe.

361
00:25:46,000 --> 00:25:51,540
 The Buddhist universe is a wrong thing. Not like a globe

362
00:25:51,540 --> 00:25:53,000
 but it is wrong.

363
00:25:53,000 --> 00:25:58,460
 And it is surrounded by what we call the wall cycle

364
00:25:58,460 --> 00:26:00,000
 mountains.

365
00:26:00,000 --> 00:26:05,050
 So there are mountains around and then in this circle there

366
00:26:05,050 --> 00:26:06,000
 is water.

367
00:26:06,000 --> 00:26:11,050
 And also the four great islands, we call them the four

368
00:26:11,050 --> 00:26:12,000
 great islands.

369
00:26:12,000 --> 00:26:14,000
 Maybe the continents.

370
00:26:14,000 --> 00:26:26,000
 And each island has 500 small islands.

371
00:26:26,000 --> 00:26:32,590
 And in the middle of this universe there is the Mount Sinir

372
00:26:32,590 --> 00:26:33,000
u or Meru.

373
00:26:33,000 --> 00:26:37,000
 It is not visible to human beings.

374
00:26:37,000 --> 00:26:42,170
 And surrounding the Meru, this mountain, great mountain,

375
00:26:42,170 --> 00:26:45,000
 there are seven concentric mountains.

376
00:26:45,000 --> 00:26:48,000
 Lower mountain, seven.

377
00:26:48,000 --> 00:26:52,000
 And in between these seven there is water.

378
00:26:52,000 --> 00:26:57,000
 So they are called inner oceans or something.

379
00:26:57,000 --> 00:27:01,950
 There is the outer ocean and then there are seven inner

380
00:27:01,950 --> 00:27:03,000
 oceans.

381
00:27:03,000 --> 00:27:06,000
 And they are the height.

382
00:27:06,000 --> 00:27:13,000
 The Siniru is 84,000 leagues and then the first tallest,

383
00:27:13,000 --> 00:27:15,000
 the highest mountain,

384
00:27:15,000 --> 00:27:18,000
 the surrounding mountain is half that height.

385
00:27:18,000 --> 00:27:21,000
 And then the next one is half that height and so on.

386
00:27:21,000 --> 00:27:24,040
 But if you want to draw according to scale it is very

387
00:27:24,040 --> 00:27:25,000
 difficult.

388
00:27:25,000 --> 00:27:29,000
 I have tried it but I cannot.

389
00:27:29,000 --> 00:27:35,430
 You have to draw a big drawing, not on small paper because

390
00:27:35,430 --> 00:27:36,000
 half height, half height,

391
00:27:36,000 --> 00:27:40,000
 half height, half height and so on.

392
00:27:40,000 --> 00:27:50,000
 And that is called one wall cycle, let's say one universe.

393
00:27:50,000 --> 00:27:56,950
 And sun and moon are said to go around this, around the

394
00:27:56,950 --> 00:28:02,000
 middle mountain at its half height.

395
00:28:02,000 --> 00:28:19,360
 And so the sun and moon are only 24,000 leagues from the

396
00:28:19,360 --> 00:28:21,000
 earth.

397
00:28:21,000 --> 00:28:25,230
 And the size of the sun is 50 leagues and the size of the

398
00:28:25,230 --> 00:28:27,000
 moon is 49 leagues.

399
00:28:27,000 --> 00:28:31,770
 There is only one league difference between the size of the

400
00:28:31,770 --> 00:28:34,000
 moon and the size of the sun.

401
00:28:34,000 --> 00:28:38,000
 And then the stars and planets go around them.

402
00:28:38,000 --> 00:28:47,420
 And it is said that when the sun goes around the Meru, it

403
00:28:47,420 --> 00:28:50,000
 throws a shadow, right?

404
00:28:50,000 --> 00:28:55,390
 So when three continents are light, one continent is dark

405
00:28:55,390 --> 00:29:02,000
 and that is night and the other is a day.

406
00:29:02,000 --> 00:29:12,310
 So only one quarter of the universe is dark and the other

407
00:29:12,310 --> 00:29:18,000
 three parts are light at one time.

408
00:29:18,000 --> 00:29:24,690
 There are four great islands and the South Island is called

409
00:29:24,690 --> 00:29:26,000
 Jambutipa.

410
00:29:26,000 --> 00:29:38,330
 And it is a place where Buddhas and all wise men and pure

411
00:29:38,330 --> 00:29:42,000
 people are born.

412
00:29:42,000 --> 00:29:50,770
 And that land is described as something like wide, wide in

413
00:29:50,770 --> 00:29:55,000
 one side and then what about that?

414
00:29:55,000 --> 00:29:59,000
 Sloping or something?

415
00:29:59,000 --> 00:30:04,000
 Becoming small in the other side. So that fits with India.

416
00:30:04,000 --> 00:30:12,570
 So India is Jambutipa, the southern island. So if India is

417
00:30:12,570 --> 00:30:16,000
 southern island, then Europe should be western island

418
00:30:16,000 --> 00:30:22,080
 and China or Japan should be eastern island. And I think

419
00:30:22,080 --> 00:30:25,000
 America should be the northern island.

420
00:30:25,000 --> 00:30:34,000
 Because when it is day in Asia, then it is night here.

421
00:30:34,000 --> 00:30:42,630
 So America could be the north great island which is called

422
00:30:42,630 --> 00:30:45,000
 Uttarakuru.

423
00:30:45,000 --> 00:30:54,000
 So this is the cosmology of Buddhism.

424
00:30:54,000 --> 00:31:01,000
 Now, there is one thing.

425
00:31:01,000 --> 00:31:10,000
 On page 220, you see the paragraph number 43.

426
00:31:10,000 --> 00:31:16,000
 Those lines, 1, 2, 3, 4, 5, they are misplaced.

427
00:31:16,000 --> 00:31:20,640
 The wall sphere mountain's line of summit plunges down into

428
00:31:20,640 --> 00:31:24,480
 the sea just two and eighty thousand lakes and towers up in

429
00:31:24,480 --> 00:31:25,000
 light gateways.

430
00:31:25,000 --> 00:31:31,000
 And ringing one walled element all round in its entirety.

431
00:31:31,000 --> 00:31:38,340
 So these lines should be on page 221, just above paragraph

432
00:31:38,340 --> 00:31:41,000
 44 and not here.

433
00:31:41,000 --> 00:31:46,000
 I don't know why he put these here, maybe by mistake.

434
00:31:46,000 --> 00:32:00,000
 So these lines should be on the other page, page 221.

435
00:32:00,000 --> 00:32:05,680
 Now here you find the moon's disk is 49 lakes across and

436
00:32:05,680 --> 00:32:09,000
 the sun's disk is 50 lakes.

437
00:32:09,000 --> 00:32:11,450
 Therefore this wall of location was known to him in all

438
00:32:11,450 --> 00:32:15,600
 ways too, so he is noir of walls because he has seen the

439
00:32:15,600 --> 00:32:17,000
 wall in all ways.

440
00:32:17,000 --> 00:32:23,070
 The Buddha is called a loka-widu, noir of the walls, noir

441
00:32:23,070 --> 00:32:27,460
 of the wall of beings, or physical walls, and wall of

442
00:32:27,460 --> 00:32:31,000
 formations, or wall of conditioned things.

443
00:32:31,000 --> 00:32:39,000
 That's why Buddha is called noir of the walls.

444
00:32:39,000 --> 00:32:46,610
 And when we read these sub-commentaries we find more of

445
00:32:46,610 --> 00:32:50,000
 these descriptions.

446
00:32:50,000 --> 00:32:54,000
 I don't know where they come from.

447
00:32:54,000 --> 00:32:59,980
 Actually not from the sodas, but I don't know where they

448
00:32:59,980 --> 00:33:05,000
 may have got them from some writings of Hindus,

449
00:33:05,000 --> 00:33:09,660
 because there are such books in Hinduism and so they may

450
00:33:09,660 --> 00:33:12,000
 have got them from them.

451
00:33:12,000 --> 00:33:17,000
 But they are not important actually.

452
00:33:17,000 --> 00:33:31,000
 And one thing is, one thing is Himalayas is 500 lakes high.

453
00:33:31,000 --> 00:33:36,000
 500 lakes means one lake is about 8 miles.

454
00:33:36,000 --> 00:33:41,000
 So how many miles?

455
00:33:41,000 --> 00:33:49,000
 4,000 miles high.

456
00:33:49,000 --> 00:33:58,000
 Sometimes they measure not by height, maybe by path.

457
00:33:58,000 --> 00:34:08,000
 So to reach the summit you have to climb a number of miles.

458
00:34:08,000 --> 00:34:12,000
 Mount Tamilpai is how many miles high?

459
00:34:12,000 --> 00:34:16,000
 Maybe one mile or two miles high?

460
00:34:16,000 --> 00:34:23,470
 Not from the sea level, I mean measuring with the pole, not

461
00:34:23,470 --> 00:34:25,000
 measuring by path.

462
00:34:25,000 --> 00:34:29,000
 We say as the crow flies, direct.

463
00:34:29,000 --> 00:34:31,000
 This is not as the crow flies.

464
00:34:31,000 --> 00:34:33,000
 No.

465
00:34:33,000 --> 00:34:38,000
 That's why we are so many miles.

466
00:34:38,000 --> 00:34:41,000
 Himalayas is 500 miles high and so on.

467
00:34:41,000 --> 00:34:51,000
 It may not be how many miles high?

468
00:34:51,000 --> 00:35:04,000
 Mount Everest is 5 miles high, right?

469
00:35:04,000 --> 00:35:29,000
 So it may be some kind of a symbolic type of thing.

470
00:35:29,000 --> 00:35:39,000
 One statement, the conference takes another.

471
00:35:39,000 --> 00:35:44,040
 Because it is said that when three islands are like one

472
00:35:44,040 --> 00:35:46,000
 island, it's dark.

473
00:35:46,000 --> 00:35:52,740
 So following this, this world is what is meant by the Lokad

474
00:35:52,740 --> 00:35:54,000
 in our books.

475
00:35:54,000 --> 00:35:59,860
 Because one part of the world is dark when the others are

476
00:35:59,860 --> 00:36:01,000
 light.

477
00:36:01,000 --> 00:36:08,000
 Not exactly one quarter, but something like that.

478
00:36:08,000 --> 00:36:14,430
 Maybe their knowledge of geography is very limited in those

479
00:36:14,430 --> 00:36:15,000
 days.

480
00:36:15,000 --> 00:36:21,250
 And then there may be sages who just contemplate and then

481
00:36:21,250 --> 00:36:24,000
 talk about these things.

482
00:36:24,000 --> 00:36:27,000
 And they came to be accepted.

483
00:36:27,000 --> 00:36:32,810
 Nobody seemed to bother about measuring the height of Himal

484
00:36:32,810 --> 00:36:36,000
ayas or Mount Everest.

485
00:36:36,000 --> 00:36:41,570
 And they had no way, no means of measuring the height of

486
00:36:41,570 --> 00:36:44,000
 mountains in those days.

487
00:36:44,000 --> 00:36:53,000
 So they may be just measuring by path.

488
00:36:53,000 --> 00:37:05,000
 But they are not really important things.

489
00:37:05,000 --> 00:37:34,000
 So next time, maybe about another 20 pages.

490
00:37:35,000 --> 00:37:50,660
 [

