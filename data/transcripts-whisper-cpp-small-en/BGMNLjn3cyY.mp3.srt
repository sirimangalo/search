1
00:00:00,000 --> 00:00:06,660
 Hello everyone, this is me, I'm here in Thailand, just

2
00:00:06,660 --> 00:00:14,000
 staying at this kutti in a little of

3
00:00:14,000 --> 00:00:20,630
 a orchard, which is right behind a shopping mall in the

4
00:00:20,630 --> 00:00:23,400
 middle of Bangkok, on the outskirts

5
00:00:23,400 --> 00:00:28,550
 of Bangkok. So I just wanted to first of all make a video

6
00:00:28,550 --> 00:00:30,680
 to let you know that I'm still

7
00:00:30,680 --> 00:00:37,570
 alive and still planning to from time to time make videos.

8
00:00:37,570 --> 00:00:39,840
 Hopefully this little device

9
00:00:39,840 --> 00:00:47,210
 here will let me make videos on the go and the quality will

10
00:00:47,210 --> 00:00:50,480
 be not as good as the regular

11
00:00:50,480 --> 00:00:55,080
 videos. It will allow for more flexibility because I still

12
00:00:55,080 --> 00:00:59,120
 might be traveling for a little

13
00:00:59,120 --> 00:01:06,060
 while. And also to try to revive this ask a monk thing so

14
00:01:06,060 --> 00:01:08,360
 that when people have questions

15
00:01:08,360 --> 00:01:14,120
 or when I receive questions that I think might be suitable

16
00:01:14,120 --> 00:01:17,160
 to make a video about or be well

17
00:01:17,160 --> 00:01:23,610
 responded to in this format, I can make a video. So I

18
00:01:23,610 --> 00:01:27,040
 thought I'd start it off by asking

19
00:01:27,040 --> 00:01:31,770
 a question myself and giving a hopefully fairly brief

20
00:01:31,770 --> 00:01:35,080
 answer. It's a fairly complex issue.

21
00:01:35,080 --> 00:01:39,150
 So the question is, how do we solve all of the world's

22
00:01:39,150 --> 00:01:42,320
 problems? That's the question.

23
00:01:42,320 --> 00:01:46,350
 Because it can get quite depressing to think about all of

24
00:01:46,350 --> 00:01:48,600
 the world's problems, to think

25
00:01:48,600 --> 00:01:54,300
 about global warming, for example. This orchard over here

26
00:01:54,300 --> 00:01:57,040
 used to have many, many trees in

27
00:01:57,040 --> 00:02:01,680
 it. And over here in front of me, pointed out, can see

28
00:02:01,680 --> 00:02:03,800
 there were there are quite a

29
00:02:03,800 --> 00:02:10,340
 few trees and for Bangkok, it's quite well green. But they

30
00:02:10,340 --> 00:02:12,160
 say half the trees were

31
00:02:12,160 --> 00:02:16,930
 destroyed in last year's flooding. What some people say is

32
00:02:16,930 --> 00:02:18,920
 because of global warming or

33
00:02:18,920 --> 00:02:23,140
 is going to get even worse because of the degradation of

34
00:02:23,140 --> 00:02:27,880
 the environment. As an example,

35
00:02:27,880 --> 00:02:33,200
 and then the poverty and the inequality in the world and

36
00:02:33,200 --> 00:02:36,080
 all of the many problems that

37
00:02:36,080 --> 00:02:41,120
 exist in the world, all of the many challenges that we face

38
00:02:41,120 --> 00:02:43,080
 as a human race. So I'm going

39
00:02:43,080 --> 00:02:48,740
 to try to tackle this in a very short YouTube video. So the

40
00:02:48,740 --> 00:02:51,200
 main premise of the answer that

41
00:02:51,200 --> 00:02:57,720
 we have to understand first, which I'm going to base my

42
00:02:57,720 --> 00:03:01,840
 answer on, is the importance in

43
00:03:01,840 --> 00:03:06,920
 distinguishing between the world and reality or

44
00:03:06,920 --> 00:03:10,560
 distinguishing between the issues that

45
00:03:10,560 --> 00:03:16,490
 we face or the conventional playing field or the

46
00:03:16,490 --> 00:03:19,880
 conventional framework in which we

47
00:03:19,880 --> 00:03:26,220
 look at the issues that we're faced with and ultimate

48
00:03:26,220 --> 00:03:29,520
 reality. Because ultimate reality

49
00:03:29,520 --> 00:03:35,880
 isn't a three dimensional space. It isn't a world. It doesn

50
00:03:35,880 --> 00:03:38,400
't admit of people. It doesn't

51
00:03:38,400 --> 00:03:44,620
 admit of society. It doesn't even the earth doesn't admit

52
00:03:44,620 --> 00:03:47,560
 of any of the problems or so

53
00:03:47,560 --> 00:03:52,870
 called problems that we talk about when we try to tackle

54
00:03:52,870 --> 00:03:55,040
 this human beings. Ultimate

55
00:03:55,040 --> 00:04:00,380
 reality doesn't have any problems. This is a very important

56
00:04:00,380 --> 00:04:03,160
 part of the answer. Ultimate

57
00:04:03,160 --> 00:04:06,390
 reality is individual experience. So my experience is

58
00:04:06,390 --> 00:04:09,320
 ultimate reality. Your experience is ultimate

59
00:04:09,320 --> 00:04:13,340
 reality. This is ultimate reality. And altogether, all of

60
00:04:13,340 --> 00:04:16,000
 these experiences make up the world.

61
00:04:16,000 --> 00:04:22,440
 And all of our experiences from one moment to the next make

62
00:04:22,440 --> 00:04:25,360
 up our reality and give us

63
00:04:25,360 --> 00:04:28,680
 this idea or create in our minds these concepts of there

64
00:04:28,680 --> 00:04:31,080
 being problems, of there being issues

65
00:04:31,080 --> 00:04:36,930
 that have to be tackled. And as a result, we wind up

66
00:04:36,930 --> 00:04:39,980
 dwelling in conceptual reality.

67
00:04:39,980 --> 00:04:47,020
 Our solutions are all conceptual. They're about restoring

68
00:04:47,020 --> 00:04:50,240
 the environment or creating

69
00:04:50,240 --> 00:04:55,170
 programs for recycling and so on. Finding political

70
00:04:55,170 --> 00:04:58,040
 solutions for war and political

71
00:04:58,040 --> 00:05:02,660
 solutions for inequality and famine and disease and so on.

72
00:05:02,660 --> 00:05:05,080
 And finding cures for sicknesses

73
00:05:05,080 --> 00:05:12,420
 and so on. All of this is not the Buddhist answer to the

74
00:05:12,420 --> 00:05:15,600
 world's problems. Because all

75
00:05:15,600 --> 00:05:19,630
 of the world's problems in ultimate reality are just

76
00:05:19,630 --> 00:05:21,920
 experience. The only thing that exists

77
00:05:21,920 --> 00:05:29,400
 is the experience of the phenomena that arise and the

78
00:05:29,400 --> 00:05:34,200
 reactions to them and the actions

79
00:05:34,200 --> 00:05:39,560
 that we base on, that are based on these reactions and

80
00:05:39,560 --> 00:05:46,200
 based on these experiences. So, you know,

81
00:05:46,200 --> 00:05:50,910
 well it may seem quite noble and honorable to try to build

82
00:05:50,910 --> 00:05:53,200
 hospitals and schools and

83
00:05:53,200 --> 00:05:57,440
 developing countries and to plant trees and so on. All of

84
00:05:57,440 --> 00:05:59,640
 these things, and actually they

85
00:05:59,640 --> 00:06:04,190
 are wholesome and they help people and they make our minds

86
00:06:04,190 --> 00:06:06,320
 in some sense more elevated.

87
00:06:06,320 --> 00:06:10,300
 They aren't really solving the problem and they have no

88
00:06:10,300 --> 00:06:12,320
 hope of ever solving the world's

89
00:06:12,320 --> 00:06:17,640
 problems. Because they are dealing with the effect or they

90
00:06:17,640 --> 00:06:20,840
're dealing with the extrapolation

91
00:06:20,840 --> 00:06:27,020
 on the problem. It's like chasing a shadow. We are trying

92
00:06:27,020 --> 00:06:29,360
 to fix the shadows. We're trying

93
00:06:29,360 --> 00:06:32,760
 to get rid of a shadow, for example. And we know that a

94
00:06:32,760 --> 00:06:34,920
 shadow is dependent on the object

95
00:06:34,920 --> 00:06:39,920
 that casts the shadow. So all of these issues in the world

96
00:06:39,920 --> 00:06:42,560
 are merely based on our individual

97
00:06:42,560 --> 00:06:47,440
 experiences. Now, this can be verified from a conventional

98
00:06:47,440 --> 00:06:49,680
 point of view. For example,

99
00:06:49,680 --> 00:06:52,250
 we talk about greed being a problem and greed being a

100
00:06:52,250 --> 00:06:54,400
 source of many of the world's problems.

101
00:06:54,400 --> 00:06:58,020
 Now from a conventional point of view, this is easy to see.

102
00:06:58,020 --> 00:06:59,520
 You can even understand how

103
00:06:59,520 --> 00:07:02,810
 people stopped consuming so much and stopped needing so

104
00:07:02,810 --> 00:07:04,960
 much and stopped with so much excess

105
00:07:04,960 --> 00:07:10,320
 of greed and desire. The resources on Earth would really be

106
00:07:10,320 --> 00:07:12,840
 enough to feed everyone and

107
00:07:12,840 --> 00:07:16,650
 many, many more people than we already have on Earth if we

108
00:07:16,650 --> 00:07:18,200
 were to be content. But of

109
00:07:18,200 --> 00:07:21,280
 course we're not content. We aren't able to live with what

110
00:07:21,280 --> 00:07:22,480
 we have and we always want

111
00:07:22,480 --> 00:07:26,260
 more and more and more. And because of this, there's the

112
00:07:26,260 --> 00:07:28,280
 degradation of the environment.

113
00:07:28,280 --> 00:07:32,290
 There's war and there's conflict and there's economic

114
00:07:32,290 --> 00:07:34,280
 disparity and so on. There's even,

115
00:07:34,280 --> 00:07:38,730
 you could say, there's disease that comes from pollution,

116
00:07:38,730 --> 00:07:40,640
 that comes from even just

117
00:07:40,640 --> 00:07:43,770
 eating meat or livestock production and all of these

118
00:07:43,770 --> 00:07:46,120
 diseases that come from our inability

119
00:07:46,120 --> 00:07:55,760
 to be content with simple lives and with simple experiences

120
00:07:55,760 --> 00:07:57,480
. But from an ultimate point of

121
00:07:57,480 --> 00:08:01,560
 view, it actually goes deeper than that. That when you give

122
00:08:01,560 --> 00:08:04,520
 up the causes and conditions

123
00:08:04,520 --> 00:08:08,900
 that make up the framework of our reality that are causing

124
00:08:08,900 --> 00:08:11,360
 these problems, the problems

125
00:08:11,360 --> 00:08:15,840
 disappear and dissolve for you. Because your reality is

126
00:08:15,840 --> 00:08:18,640
 that experience. Your reality is

127
00:08:18,640 --> 00:08:25,040
 your experience of the six senses and the world around you.

128
00:08:25,040 --> 00:08:27,360
 When you change your reaction,

129
00:08:27,360 --> 00:08:30,890
 when you change your intentions, then your experience

130
00:08:30,890 --> 00:08:34,840
 changes. This is why things like

131
00:08:34,840 --> 00:08:38,640
 the concept of heaven or an angel, the concept of hell,

132
00:08:38,640 --> 00:08:40,920
 these ideas of being born in such

133
00:08:40,920 --> 00:08:43,850
 places are not really far fetched from a Buddhist point of

134
00:08:43,850 --> 00:08:47,000
 view. This is an effect, a result

135
00:08:47,000 --> 00:08:50,360
 of having a pure mind. And so for a person with a pure mind

136
00:08:50,360 --> 00:08:51,640
, they have no need to worry

137
00:08:51,640 --> 00:08:55,880
 about such things as global warming or famine or so on. And

138
00:08:55,880 --> 00:08:57,480
 if we look a little bit more

139
00:08:57,480 --> 00:09:03,870
 broadly than we mostly do, we'll come to see that actually

140
00:09:03,870 --> 00:09:06,880
 this is necessary. It's necessary

141
00:09:06,880 --> 00:09:10,880
 to take this kind of an outlook. Or it's logical to take

142
00:09:10,880 --> 00:09:12,960
 such an outlook if we're going to

143
00:09:12,960 --> 00:09:16,320
 be honest with ourselves, because anyone who dedicates

144
00:09:16,320 --> 00:09:18,280
 their whole existence to fixing

145
00:09:18,280 --> 00:09:24,360
 the world's problems is really trying to fix something that

146
00:09:24,360 --> 00:09:28,520
, well, it's impossible to fix.

147
00:09:28,520 --> 00:09:32,820
 They're fighting a lost cause. Why? Because we know that

148
00:09:32,820 --> 00:09:35,040
 the earth is unsavable. This earth

149
00:09:35,040 --> 00:09:38,730
 is going to eventually go kaput. It's going to burn up to a

150
00:09:38,730 --> 00:09:41,040
 crisp and it's going to eventually

151
00:09:41,040 --> 00:09:44,760
 or the sun is going to explode or whatever. And eventually,

152
00:09:44,760 --> 00:09:46,640
 at some point in the future,

153
00:09:46,640 --> 00:09:49,870
 there will be no human race. There will be no humans in

154
00:09:49,870 --> 00:09:52,640
 this solar system. Unless somehow

155
00:09:52,640 --> 00:09:57,740
 we find a way to go to another planet. But the point being

156
00:09:57,740 --> 00:10:00,360
 that the universe is far more

157
00:10:00,360 --> 00:10:06,240
 than the issues that we chase around and we try to solve.

158
00:10:06,240 --> 00:10:09,280
 The true issues are our universal

159
00:10:09,280 --> 00:10:15,240
 and unlimited eternal experience, this unending experience

160
00:10:15,240 --> 00:10:18,360
 and how to interact with that and

161
00:10:18,360 --> 00:10:29,070
 how to become free from the problems that are inherent in

162
00:10:29,070 --> 00:10:31,880
 this, chasing after things

163
00:10:31,880 --> 00:10:39,720
 and continuing on. Okay, so that is how I want to frame

164
00:10:39,720 --> 00:10:42,800
 this. Now, that's basically the answer

165
00:10:42,800 --> 00:10:45,800
 to this, is coming back to the present moment and obviously

166
00:10:45,800 --> 00:10:47,360
 undertaking the practice. Obviously

167
00:10:47,360 --> 00:10:49,990
 what I'm trying to say, undertake the practice of

168
00:10:49,990 --> 00:10:52,520
 meditation. Because once we come to look

169
00:10:52,520 --> 00:10:56,570
 at reality here and now and to see things as they are and

170
00:10:56,570 --> 00:10:58,480
 to just get real, to stop

171
00:10:58,480 --> 00:11:03,660
 having all these ideas and all these grand delusions of

172
00:11:03,660 --> 00:11:06,880
 grandeur and the idea that, oh,

173
00:11:06,880 --> 00:11:10,170
 we're going to save the world and oh, becoming so

174
00:11:10,170 --> 00:11:13,600
 passionate about causes like economic equality

175
00:11:13,600 --> 00:11:18,200
 and so on and so on. Rather than actually doing something

176
00:11:18,200 --> 00:11:20,520
 to purify our minds, we wind

177
00:11:20,520 --> 00:11:24,360
 up getting more and more caught up in that. So the answer

178
00:11:24,360 --> 00:11:26,040
 is for us to pull out of it

179
00:11:26,040 --> 00:11:29,230
 and to become free. And this has an effect for both

180
00:11:29,230 --> 00:11:31,440
 ourselves and for the people around

181
00:11:31,440 --> 00:11:36,360
 us and so on. It's really the only way out because it's the

182
00:11:36,360 --> 00:11:38,480
 only solution that is based

183
00:11:38,480 --> 00:11:43,590
 on true reality and what is really going on. So that's the

184
00:11:43,590 --> 00:11:46,400
 basic framework. Now, based

185
00:11:46,400 --> 00:11:50,870
 on this and sort of as an outline of what I mean by the

186
00:11:50,870 --> 00:11:53,680
 practice of meditation, I'd

187
00:11:53,680 --> 00:12:00,600
 like to outline four points to keep in mind or four aspects

188
00:12:00,600 --> 00:12:03,160
 of the solution. The first

189
00:12:03,160 --> 00:12:09,190
 one is our solution to the world's problems have to be here

190
00:12:09,190 --> 00:12:11,880
. We can't solve other people's

191
00:12:11,880 --> 00:12:16,270
 problems. You can give people this sort of advice, advice

192
00:12:16,270 --> 00:12:18,160
 that I'm giving or you can

193
00:12:18,160 --> 00:12:21,310
 give people other advice. And if you think that that's the

194
00:12:21,310 --> 00:12:22,800
 right way, you explain to

195
00:12:22,800 --> 00:12:25,800
 them this is how you solve things and so on. But it's up to

196
00:12:25,800 --> 00:12:26,960
 each and every one of us to

197
00:12:26,960 --> 00:12:30,070
 solve our problems. You can't go and solve the problems in

198
00:12:30,070 --> 00:12:31,720
 another country or even solve

199
00:12:31,720 --> 00:12:34,880
 the problems in the person next to you. You can only solve

200
00:12:34,880 --> 00:12:36,560
 the problems that are in your

201
00:12:36,560 --> 00:12:40,720
 own mind. This is what we mean by meditation. It doesn't

202
00:12:40,720 --> 00:12:44,200
 mean to go around and preach to

203
00:12:44,200 --> 00:12:48,020
 people. Meditation is not this video. It's not spreading

204
00:12:48,020 --> 00:12:49,960
 these teachings or even watching

205
00:12:49,960 --> 00:12:53,890
 these. Meditation is when you yourself begin to look at

206
00:12:53,890 --> 00:12:56,360
 your reality, begin to take apart

207
00:12:56,360 --> 00:13:00,230
 this experience and see it for what it is piece by piece by

208
00:13:00,230 --> 00:13:01,680
 piece. And until finally

209
00:13:01,680 --> 00:13:04,410
 your mind becomes straight about it. You no longer have

210
00:13:04,410 --> 00:13:06,120
 greed, you no longer have anger,

211
00:13:06,120 --> 00:13:09,830
 and you no longer have the delusion that causes you to do

212
00:13:09,830 --> 00:13:12,120
 and say and think that thing. And

213
00:13:12,120 --> 00:13:15,860
 so first thing is it has to be here and here means you have

214
00:13:15,860 --> 00:13:17,760
 to start with yourself. Number

215
00:13:17,760 --> 00:13:20,650
 two, it has to be now. We can't be worrying about problems

216
00:13:20,650 --> 00:13:22,080
 in the future. We can't be

217
00:13:22,080 --> 00:13:27,170
 thinking, "Oh, maybe 10 years, 20 years, 100 years down the

218
00:13:27,170 --> 00:13:28,560
 road or sometime in the future

219
00:13:28,560 --> 00:13:30,920
 there's going to be these problems and we have to worry

220
00:13:30,920 --> 00:13:32,440
 about them and we have to concern

221
00:13:32,440 --> 00:13:36,150
 about the future." When you do that you're no longer in

222
00:13:36,150 --> 00:13:37,560
 touch with reality. The Buddha

223
00:13:37,560 --> 00:13:42,050
 said this is like grass that when you cut it off it's no

224
00:13:42,050 --> 00:13:44,400
 longer able to receive the

225
00:13:44,400 --> 00:13:49,170
 water from its roots and therefore it dries up. Your mind

226
00:13:49,170 --> 00:13:51,320
 dries up and you wind up in

227
00:13:51,320 --> 00:13:55,550
 great suffering and this is what leads to of course worry

228
00:13:55,550 --> 00:13:57,720
 and fear and as a result anger

229
00:13:57,720 --> 00:14:02,770
 and greed and so on and all of these things. Just like

230
00:14:02,770 --> 00:14:05,160
 grass that is cut off at the root.

231
00:14:05,160 --> 00:14:08,870
 So the person who is uprooted from the present moment is

232
00:14:08,870 --> 00:14:11,160
 unable to find the answers to their

233
00:14:11,160 --> 00:14:14,990
 problems. The past as well we can't go back to. We can't be

234
00:14:14,990 --> 00:14:16,920
 angry about the past or trying

235
00:14:16,920 --> 00:14:19,930
 to make up for the past or we've heard other people who

236
00:14:19,930 --> 00:14:21,880
 have done bad things. We have to

237
00:14:21,880 --> 00:14:24,580
 be here in the present doing the best we can. We have to

238
00:14:24,580 --> 00:14:26,320
 give up the past. If you've done

239
00:14:26,320 --> 00:14:30,110
 bad things, if you've had many problems in the past you

240
00:14:30,110 --> 00:14:32,000
 have to stop seeing your problems

241
00:14:32,000 --> 00:14:35,340
 as something that has been going on for years and years

242
00:14:35,340 --> 00:14:37,440
 like I have an addiction or I have

243
00:14:37,440 --> 00:14:40,080
 this grudge and this hatred and I have this problem with

244
00:14:40,080 --> 00:14:41,680
 this person. You have to take

245
00:14:41,680 --> 00:14:46,950
 it here and now. We have to be able to give up the past to

246
00:14:46,950 --> 00:14:48,880
 make a break from the past

247
00:14:48,880 --> 00:14:53,800
 and say that's who I was that's completely gone and will

248
00:14:53,800 --> 00:14:56,600
 never arise again and it doesn't

249
00:14:56,600 --> 00:15:00,680
 have to affect the way I act right now. I can act in any

250
00:15:00,680 --> 00:15:02,800
 way I want in this moment and

251
00:15:02,800 --> 00:15:07,210
 so we have to take that and change our life for once and

252
00:15:07,210 --> 00:15:09,640
 for all we have to make a break

253
00:15:09,640 --> 00:15:16,760
 from the past and the same with the future. Number three,

254
00:15:16,760 --> 00:15:22,360
 our solutions have to be based

255
00:15:22,360 --> 00:15:27,570
 on reality. I've already discussed this. Our solutions are

256
00:15:27,570 --> 00:15:29,760
 here and now but they can't

257
00:15:29,760 --> 00:15:35,980
 be based on our living conditions or they can't be based on

258
00:15:35,980 --> 00:15:38,920
 our physical health or they can't

259
00:15:38,920 --> 00:15:42,680
 be based on our relationships with other people. They have

260
00:15:42,680 --> 00:15:44,680
 to be based on ultimate reality

261
00:15:44,680 --> 00:15:48,050
 and what we mean by ultimate reality is experience. So when

262
00:15:48,050 --> 00:15:50,200
 you see something that is your reality

263
00:15:50,200 --> 00:15:53,080
 and there's a problem there because when you see things you

264
00:15:53,080 --> 00:15:54,520
 become attached to them. When

265
00:15:54,520 --> 00:15:56,880
 you hear things that's where the problem arises because

266
00:15:56,880 --> 00:15:58,360
 when you hear you become attached

267
00:15:58,360 --> 00:16:01,300
 to it and so on. When you feel pain in the body you become

268
00:16:01,300 --> 00:16:02,880
 attached to it, you get angry

269
00:16:02,880 --> 00:16:05,650
 and upset about it. When you feel pleasure in the body you

270
00:16:05,650 --> 00:16:06,800
 become attached to it, you

271
00:16:06,800 --> 00:16:10,450
 like it, you want more of it. These are the problems. This

272
00:16:10,450 --> 00:16:12,640
 is the ultimate reality. This

273
00:16:12,640 --> 00:16:18,090
 is where the problem exists and this is where we have to

274
00:16:18,090 --> 00:16:22,000
 tackle it. The fourth aspect of

275
00:16:22,000 --> 00:16:27,670
 the solution is that our focus, once we focus on reality it

276
00:16:27,670 --> 00:16:30,760
 has to be a focus on the goodness

277
00:16:30,760 --> 00:16:34,480
 of reality. So the fourth aspect is goodness. That we can't

278
00:16:34,480 --> 00:16:36,600
 simply be doing this for intellectual

279
00:16:36,600 --> 00:16:39,460
 purposes and we have to be clear that our intention is to

280
00:16:39,460 --> 00:16:41,000
 become a better person, is

281
00:16:41,000 --> 00:16:44,100
 to purify our minds. The Buddhist teaching is for the

282
00:16:44,100 --> 00:16:46,120
 purpose of purifying the mind.

283
00:16:46,120 --> 00:16:50,660
 This is what makes it special that it actually has a way of

284
00:16:50,660 --> 00:16:53,360
 bringing people to freedom from

285
00:16:53,360 --> 00:16:58,370
 greed, freedom from anger, freedom from delusion. To have

286
00:16:58,370 --> 00:17:01,680
 no unwholesome, unskillful, useless

287
00:17:01,680 --> 00:17:06,750
 mind state. To have a mind that is pure, a mind that acts

288
00:17:06,750 --> 00:17:09,200
 and speaks and thinks from

289
00:17:09,200 --> 00:17:13,650
 a pure and a wise mind in an appropriate way at all times.

290
00:17:13,650 --> 00:17:15,920
 So as we practice meditation,

291
00:17:15,920 --> 00:17:20,830
 as we learn more about ourselves, our focus should be on

292
00:17:20,830 --> 00:17:24,040
 for the purpose of making ourselves

293
00:17:24,040 --> 00:17:28,190
 a better person. This is the ultimate task because once we

294
00:17:28,190 --> 00:17:30,160
're a better person it's like

295
00:17:30,160 --> 00:17:34,120
 the source of a river. If the source of the river is impure

296
00:17:34,120 --> 00:17:35,840
 then all the way down the

297
00:17:35,840 --> 00:17:40,010
 river the whole river will be tainted and is useless, undr

298
00:17:40,010 --> 00:17:41,440
inkable. But if the source

299
00:17:41,440 --> 00:17:44,720
 of the water is pure then all the way down the river,

300
00:17:44,720 --> 00:17:46,680
 meaning whatever we do when our

301
00:17:46,680 --> 00:17:51,000
 mind is pure, will be of benefit to other beings, any other

302
00:17:51,000 --> 00:17:53,000
 being who comes in contact

303
00:17:53,000 --> 00:17:55,960
 with it based on that pure mind, all of the world will

304
00:17:55,960 --> 00:17:57,900
 begin to change, the whole world

305
00:17:57,900 --> 00:18:04,330
 around us. Our goodness, our purity will free us from

306
00:18:04,330 --> 00:18:08,240
 suffering and will make us a truly

307
00:18:08,240 --> 00:18:13,130
 useful, truly skillful, truly beneficial part of this world

308
00:18:13,130 --> 00:18:14,520
. And a good example is the Buddha

309
00:18:14,520 --> 00:18:19,290
 himself. One person was able to change so much, was able to

310
00:18:19,290 --> 00:18:21,080
 do so much for the world

311
00:18:21,080 --> 00:18:24,810
 that even now people are practicing his teachings and are

312
00:18:24,810 --> 00:18:27,620
 freeing themselves from so many unpleasant

313
00:18:27,620 --> 00:18:31,760
 and wholesome states of mind and states of being, people

314
00:18:31,760 --> 00:18:34,080
 who have addictions and people

315
00:18:34,080 --> 00:18:36,570
 who have aversions, people who have worries and phobias and

316
00:18:36,570 --> 00:18:37,680
 so on, are able to become

317
00:18:37,680 --> 00:18:41,140
 free from this because of the Buddhist issue. One person

318
00:18:41,140 --> 00:18:43,280
 was able to influence so many other

319
00:18:43,280 --> 00:18:45,980
 people who were then able to influence so many other people

320
00:18:45,980 --> 00:18:47,100
 and even today are able

321
00:18:47,100 --> 00:18:50,090
 to influence people and change things about the world. So

322
00:18:50,090 --> 00:18:53,800
 this I think is at least one

323
00:18:53,800 --> 00:18:58,010
 answer on what we can do to solve all of the problems in

324
00:18:58,010 --> 00:19:00,760
 the world because I truly believe

325
00:19:00,760 --> 00:19:05,390
 and not just believe out of faith but based on what I've

326
00:19:05,390 --> 00:19:07,600
 come to see in the practice,

327
00:19:07,600 --> 00:19:11,040
 that the answer to the problems lie within ourselves, the

328
00:19:11,040 --> 00:19:12,200
 answer to all the problems

329
00:19:12,200 --> 00:19:14,790
 in the world and that the problems that you see in the

330
00:19:14,790 --> 00:19:16,400
 world around you that make you

331
00:19:16,400 --> 00:19:20,560
 depressed, that make you feel hopeless and so on, are

332
00:19:20,560 --> 00:19:23,920
 completely solvable, completely fixable

333
00:19:23,920 --> 00:19:28,790
 here and now, in the present moment here, now if you base

334
00:19:28,790 --> 00:19:31,000
 yourself on reality and develop

335
00:19:31,000 --> 00:19:40,090
 goodness. So these four aspects, please make up one

336
00:19:40,090 --> 00:19:43,840
 solution that I would offer here to

337
00:19:43,840 --> 00:19:47,030
 all of the problems in the world. So here's another Ask A

338
00:19:47,030 --> 00:19:48,560
 Monk. I've asked myself and

339
00:19:48,560 --> 00:19:52,200
 I've answered myself and hopefully this has been of use for

340
00:19:52,200 --> 00:19:53,720
 people and that you'll be

341
00:19:53,720 --> 00:19:57,420
 able to put these concepts into practice or use this as an

342
00:19:57,420 --> 00:19:59,640
 encouragement for your practice,

343
00:19:59,640 --> 00:20:02,790
 not just encouragement to watch more of my videos or watch

344
00:20:02,790 --> 00:20:04,240
 more videos or learn more

345
00:20:04,240 --> 00:20:07,110
 and so on but to actually say, "Yeah, I should take the

346
00:20:07,110 --> 00:20:08,880
 time to do some meditation." And

347
00:20:08,880 --> 00:20:11,800
 I hope that you all do and that your meditation is fruitful

348
00:20:11,800 --> 00:20:13,760
 and that you are able to free yourself

349
00:20:13,760 --> 00:20:18,400
 from the problems that exist in your mind. And just as for

350
00:20:18,400 --> 00:20:21,280
 myself, this is my task to

351
00:20:21,280 --> 00:20:24,650
 free myself from the problems that exist in my mind. So

352
00:20:24,650 --> 00:20:26,680
 wishing you all the best and see

353
00:20:26,680 --> 00:20:27,080
 you next time.

354
00:20:27,080 --> 00:20:57,080
 [

