 [ Silence ]
 >> Okay. Good evening, everyone.
 [ Silence ]
 I'm broadcasting live February 13th.
 [ Silence ]
 Oops.
 [ Silence ]
 Sorry.
 [ Silence ]
 Okay. So I'm going to post this Hangout.
 Actually, that's not.
 We'll post it later.
 But joining the Hangout, the idea was for people who,
 if you have questions.
 If you have a question, you can join the Hangout and ask it
.
 I got someone sent me a note saying, complaining about,
 somehow being dismissive of Larry, I think, last night.
 So I apologize if that was the case, but probably was,
 in some way, the case because I was kind of hoping to
 encourage,
 not like a panel like we had set up in the past,
 but just people asking questions.
 So if you don't have a question, the idea is come on the
 Hangout
 for a question.
 If you don't have a question, just listen at home.
 It doesn't have to be authoritarian or anything,
 but I think it's better to just keep that for questions.
 And I apologize if it seems somewhat cold or harsh or
 whatever.
 What's the thing when you teach, when you put yourself out
 there,
 you open yourself up for criticism.
 It's a reason why a lot of people, I think, are afraid of
 teaching,
 are afraid of putting themselves out there in a thick skin.
 You also get good feedback, positive feedback, but you get
 negative feedback,
 and you'll make mistakes, and people will call you out on
 them.
 If you hide away in your room, you don't have to face the
 criticism.
 So tonight we have a quote about gold.
 Gold is of two kinds, unrefined and refined.
 So we come into the meditation unrefined, like unrefined
 gold,
 with all sorts of defilements.
 It has iron, copper, tin, lead, and silver.
 These are five other types of metal that are mixed in with
 gold
 and keep it from being pure, pliant, and keep it from being
 workable.
 You can't do much with ordinary gold, or you need to purify
 it.
 In the same way, the mind is defiled by five impurities,
 and these are the five hindrances.
 When I first started meditating, there was this British ex-
meditator,
 who some years before that made a cartoon of the five hindr
ances.
 When I first got there, they were handing out these pages
 with the cartoons of the five hindrances and cartoons of
 the five faculties,
 being confidence, effort, mindfulness, concentration, and
 wisdom.
 They're quite useful cartoons, quite useful to have those
 that have a visual reminder of them.
 I don't know if they still give them out, or if they still
 even have them.
 When I was working there, we used to photocopied them.
 The five hindrances are the first set of dhammas
 that we have to look at in the meditation practice.
 This is under that ketchal category of dhammas,
 which never had a successful explanation of how to
 translate dhamma in that context.
 It seems like it means more than one thing,
 something that you couldn't really translate,
 you just have to understand it as a category of things.
 But the first category is the hindrances,
 which makes it seem like a real meditation teaching.
 The dhammas in the Satipatthana Sutta
 refer to those things that you have to understand or you
 have to know about,
 or that you encounter on your progress, on your path.
 So as you're mindful of reality, the objective reality,
 this is the body and the mind, ordinary reality that's not
 caught up in goodness or evil,
 not ethically charged.
 You're going to react.
 When you experience through the senses, you're going to
 react,
 see things, hear things.
 When you think things, you're going to react to your
 thoughts.
 Physical sensations of pleasure and pain, you're going to
 react.
 And this is the five hindrances.
 So as you're practicing, the five hindrances will come up,
 mainly as reaction, in reaction to experiences.
 They're hindrances because they get in the way of your
 progress.
 You'll like certain things, dislike certain things.
 I'm used to using simplified versions of these.
 The five hindrances, the technical names of them are
 kamachanda, meaning sense desire.
 So he's got them here.
 Sense, desire, ill will.
 And you have sloth and torpor.
 Sloth and torpor are two words that we just don't use
 anymore,
 so it's a shame that we're still using sloth and torpor.
 It's because there's two words.
 Basically laziness, inertia, muddled, unwieldiness of the
 mind.
 And then restlessness and worry, that's a good definition.
 Restlessness and worry are put together, but they're
 actually two different things.
 Restlessness is when the mind is not focused,
 when you're thinking lots of different things.
 Worry is a little bit different.
 Worry is a sort of a fear or anxiety.
 Concern about, I mean the technical descriptions are
 concerned about things that you've done in the past,
 bad deeds that you've done, worried about consequences,
 that kind of thing.
 And tina, no, vichiki-cha which means doubt.
 The five hindrances are one of the first things that we
 teach meditators.
 They're always going to be important.
 They're what you focus on, or they're a big part of your
 focus,
 and they always have to be in your mind from beginning to
 end of the course.
 If you don't understand the five hindrances, if you don't
 get them,
 they'll destroy you in the practice.
 As it gets harder, as it gets more intensive in a
 meditation course,
 not practicing in daily life, of course they come into play
 there,
 but during a meditation course they really make or break
 your practice.
 As soon as you let them get to you and you forget to be
 mindful of them,
 or if you don't work hard to learn how to be mindful of
 them and how to overcome them,
 if you don't become comfortable dealing with them,
 then they'll start to weasel their way into your mind,
 and they'll start eating away at your confidence,
 eating away at your resolve, eating away at your effort,
 and they'll drag you down and they'll make you...
 they'll make you discouraged, and they can cause you to
 fail.
 Which should be really encouraging, because that's all that
 is going to make you fail.
 We say that it gets tough, or it is difficult to meditate,
 or there are times where you feel like you can't meditate,
 or you're just overwhelmed, and you don't know what to do,
 but it's not the situation that makes it impossible.
 There's no situation in the course that makes it impossible
 to meditate.
 It's only your own mind, your own emotions, your own
 reactions to things.
 And so it's actually quite easy to figure out the solution
 to the problems that come up,
 not only actually in meditation, but in our lives.
 It's quite encouraging for figuring out a succeeded life.
 Not easy, but at least you have a clear picture of what you
 need to do.
 And let's just deal with the five hundredths.
 They're really the only things standing between us and
 success in anything,
 because they destroy the minds... they destroy the
 faculties of the mind.
 So they destroy your confidence, they destroy your effort,
 they destroy your mindfulness, your concentration and your
 wisdom.
 They distract you from these things, they keep you from
 developing them.
 They keep them from... these faculties from growing strong,
 from working together to free the mind.
 So for that reason, these are very important dumbness.
 Meditation, in our tradition we simplify them,
 liking, disliking, drowsiness, distraction, doubt.
 I'm the one who came up with those words, I don't know that
 anyone else is using those.
 Why I use them is because it's liking and then four D's,
 liking, disliking, drowsiness, distraction, doubt.
 It's easy to say, it's easy to remember.
 But it's very similar to how a Jandong, my teacher, teaches
 them in Thai.
 가나, 마차, 나, 보, 보, 소살, 차, 마차, 보, �
�, 소살.
 As you see, it gives them very simple words.
 Even if you don't know what those mean, you can hear that
 they're very easy to say.
 가, 마차, 마 means no, and 아 means like.
 아, 마차, 보 is just tired or drowsy.
 보 means 보살, 보살 is distracted or flustered.
 And 소살 is doubt.
 We need these simple words because we're on the front lines
.
 We need to be able to use these as weapons.
 We need to have a weapon to combat these.
 So it's like knowing your enemy.
 We have to know a way of getting to them.
 If you have these words like "central desire, ill will" and
 so on, "sloth and lacing",
 it doesn't really work, practically speaking.
 It's important that we memorize these.
 When people come to practice in 전형, 아장형 will
 remind them of these.
 When it starts with the four foundations of mindfulness,
 you have to memorize the four 새힕형, 가에에, 나지�
��, 나지형.
 And then under 나지형 you have to memorize the five hind
rances.
 전형, 마장형, and it will have you repeat after him.
 전형, 마장형, 마장형, 보아, 보아, 보살, 보�
�, 보살.
 And then you have to say it yourself.
 They're that important.
 As you can see, the Buddha says,
 "When you give up these five debasements,
 these five defilements,
 one can then direct the mind to the realization by psychic
 knowledge
 of whatever can be realized by psychic knowledge.
 You can see it directly, whatever its range might be."
 So when you're talking about psychic powers,
 magical powers, it's possible to gain these
 when your mind is free from hindrance,
 but it's also possible.
 The sixth of the abhinya of the psychic powers
 is the destruction of defilement.
 So the highest psychic power is the power of wisdom
 of overcoming the defilement.
 That's the dhamma for tonight.
 Now I'm going to post the link to the hangout,
 but it's only really if you want to come on and ask
 questions.
 So if you have a question, come on the hangout.
 Don't be shy.
 Come and ask.
 This is our new way of doing things.
 You've got to actually be brave enough to come on and ask
 your question.
 You also need a mic in that case, which I guess is a bit of
 a thing.
 Some people might not have a mic.
 [silence]
 Huh.
 This person has posted this several times.
 I guess I never actually answered it.
 Someone's asking about whether they should
 learn more from normal how-to videos.
 If you're 17 years old, don't worry about the children's
 videos.
 I mean, adults have liked them. Didn't I answer this
 already?
 Adults have found those videos useful as well,
 and they are kind of useful because they simplify
 everything.
 But I recommend my booklet.
 If you haven't read the booklet on how to meditate,
 that's where you should start.
 Meditate's videos are just about non-insight things.
 So, no questions?
 No questions that I'm going to hand out.
 [silence]
 Alright. Have a good night, everyone.
 See you all tomorrow.
 [silence]
 [silence]
