1
00:00:00,000 --> 00:00:05,360
 Tell us a bit about how he got there, what took him to

2
00:00:05,360 --> 00:00:10,000
 Buddhism, and finally to be brave enough to want to ordain.

3
00:00:10,000 --> 00:00:15,230
 And Ian, related to it, asks Nagasena, did you ever have

4
00:00:15,230 --> 00:00:20,190
 doubt about going forth, maybe thinking you wouldn't last

5
00:00:20,190 --> 00:00:21,000
 long?

6
00:00:21,000 --> 00:00:31,140
 Hmm, well, how I got here is easy enough. Kind of. Took the

7
00:00:31,140 --> 00:00:34,000
 plane, tax a year.

8
00:00:34,000 --> 00:00:40,420
 But I think, I don't know, I mean, it was a long time

9
00:00:40,420 --> 00:00:47,720
 coming, pretty much, I think. I've always had a bit of an

10
00:00:47,720 --> 00:00:52,000
 inclination towards it, I guess.

11
00:00:52,000 --> 00:00:58,820
 Since I was young, I got into spiritual things and

12
00:00:58,820 --> 00:01:06,970
 religions and looking at different things, mainly Hinduism

13
00:01:06,970 --> 00:01:09,000
 and Sufism,

14
00:01:09,000 --> 00:01:17,310
 both of which have ascetics, and I was always like, "Oh,

15
00:01:17,310 --> 00:01:22,000
 that would be the best thing to do."

16
00:01:22,000 --> 00:01:27,780
 Just because, to me, I always saw the, "Well, when you stay

17
00:01:27,780 --> 00:01:31,070
 in the world, it's just, you just do the same thing that

18
00:01:31,070 --> 00:01:32,000
 everyone does.

19
00:01:32,000 --> 00:01:35,060
 You go to school, so you get a job, you get the job, so

20
00:01:35,060 --> 00:01:38,300
 that way you can live, so you get married, so you get this,

21
00:01:38,300 --> 00:01:42,000
 get that, and then you just get old, and then you die."

22
00:01:42,000 --> 00:01:46,420
 None of it really matters, you can't do anything with it if

23
00:01:46,420 --> 00:01:51,070
 you're gonna die. You just hold in, have fun with it for

24
00:01:51,070 --> 00:01:54,140
 that little bit of time, so then it just becomes

25
00:01:54,140 --> 00:01:55,000
 meaningless.

26
00:01:55,000 --> 00:02:01,180
 There's nothing really to it, so there's always something

27
00:02:01,180 --> 00:02:05,000
 that I was wanting to do, I guess.

28
00:02:05,000 --> 00:02:11,760
 Then I got here, did the meditation courses, and yeah,

29
00:02:11,760 --> 00:02:14,000
 there's doubt a lot of times.

30
00:02:14,000 --> 00:02:20,830
 During the meditation courses, there's everything, it's

31
00:02:20,830 --> 00:02:24,000
 like life in a shot glass.

32
00:02:24,000 --> 00:02:29,430
 You get big ups and big downs and everywhere in between,

33
00:02:29,430 --> 00:02:31,000
 and all this.

34
00:02:31,000 --> 00:02:38,440
 So, yeah, there's moments of incredible inspiration, where

35
00:02:38,440 --> 00:02:43,830
 I was just, "This is the only thing," and then two hours

36
00:02:43,830 --> 00:02:48,000
 later I'd just be thinking, "How can I get out of here?"

37
00:02:48,000 --> 00:02:56,010
 "What can I do to leave all this?" So, yeah, but in the end

38
00:02:56,010 --> 00:03:02,840
 it was just a matter of really having to look at it in the

39
00:03:02,840 --> 00:03:08,770
 face, I guess, and just be like, "Well, there's all these

40
00:03:08,770 --> 00:03:12,470
 various things happening in my mind, I wanna do this, I

41
00:03:12,470 --> 00:03:14,000
 wanna do that."

42
00:03:14,000 --> 00:03:18,170
 But you know that I came all the way out here to Sri Lanka

43
00:03:18,170 --> 00:03:22,150
 for this purpose because of the way things were in me

44
00:03:22,150 --> 00:03:27,010
 following these wants and all this, and they make a good

45
00:03:27,010 --> 00:03:31,000
 argument when you're in a tough situation.

46
00:03:31,000 --> 00:03:36,060
 But when you just look at it, it's like the thinking

47
00:03:36,060 --> 00:03:40,630
 question again. It's just, when you look at it as just

48
00:03:40,630 --> 00:03:45,000
 thoughts, it's just wanting, it kind of loses its punch.

49
00:03:45,000 --> 00:03:50,530
 It's just, you give it its importance. If you find this or

50
00:03:50,530 --> 00:03:56,000
 that important, it's because you're making it important.

51
00:03:56,000 --> 00:03:59,440
 There's nothing important about wanting this or wanting

52
00:03:59,440 --> 00:04:03,310
 that. All your wants and cravings, there's nothing of value

53
00:04:03,310 --> 00:04:06,740
 in them, it's just that you convince yourself that there is

54
00:04:06,740 --> 00:04:10,000
, because that's the way the mind works.

55
00:04:10,000 --> 00:04:17,110
 So you just, it's really a matter of letting go, I think.

56
00:04:17,110 --> 00:04:23,370
 You just kind of have to do what you know is right, and

57
00:04:23,370 --> 00:04:26,000
 then it works out.

58
00:04:26,000 --> 00:04:31,340
 You didn't have other Buddhist teachers before you took the

59
00:04:31,340 --> 00:04:33,000
 courses there?

60
00:04:33,000 --> 00:04:48,100
 No. But, yeah, I don't know. You just have to do what you

61
00:04:48,100 --> 00:04:52,700
 know is right without getting caught up in all the stories

62
00:04:52,700 --> 00:04:55,000
 that you'll come up with.

63
00:04:55,000 --> 00:04:58,210
 So that's all that it is, is stories. Is that you just

64
00:04:58,210 --> 00:05:01,450
 create everything. This means this, and it has this

65
00:05:01,450 --> 00:05:05,230
 importance, and it's related to that, and this way, and it

66
00:05:05,230 --> 00:05:07,510
's all just stories that you make up, and it really doesn't

67
00:05:07,510 --> 00:05:08,000
 matter.

68
00:05:08,000 --> 00:05:13,900
 You just have to see that. And then it becomes easy, then

69
00:05:13,900 --> 00:05:18,000
 it's not so much a big thing anymore.

70
00:05:18,000 --> 00:05:29,000
 Then you just have to memorize, memorize the procedure.

71
00:05:29,000 --> 00:05:35,320
 Sorry to take the mic away from you, but just to continue

72
00:05:35,320 --> 00:05:39,310
 on with that, he's not telling you the actual story of how

73
00:05:39,310 --> 00:05:47,000
 it happened when he finished the advanced course, no?

74
00:05:47,000 --> 00:05:51,830
 And we were all sitting around down here trying to decide

75
00:05:51,830 --> 00:05:58,570
 what to do. Should we ordain him tomorrow, or should we

76
00:05:58,570 --> 00:06:00,000
 wait a week?

77
00:06:00,000 --> 00:06:05,320
 And it was debatable, because if we set it for tomorrow, or

78
00:06:05,320 --> 00:06:09,660
 whenever it was, then what happens if he decides he's not

79
00:06:09,660 --> 00:06:11,000
 going to ordain?

80
00:06:11,000 --> 00:06:13,560
 Then the people who are expecting to come, they won't have

81
00:06:13,560 --> 00:06:16,100
 time to change, or so and so. And we'd already told them

82
00:06:16,100 --> 00:06:18,450
 that it was going to be a week later, because we wanted to

83
00:06:18,450 --> 00:06:20,000
 make sure that he wants to ordain.

84
00:06:20,000 --> 00:06:23,110
 And then we realized, wait a minute, if we give him a week,

85
00:06:23,110 --> 00:06:26,210
 he might really change his mind, and he'll just keep doub

86
00:06:26,210 --> 00:06:28,000
ting it more and more and more.

87
00:06:28,000 --> 00:06:35,580
 And then he just won't ordain in the end. Yeah, better. And

88
00:06:35,580 --> 00:06:38,450
 Palanya Nghi gave a good example, is that, in her case,

89
00:06:38,450 --> 00:06:39,190
 when she was interested in it, and didn't ordain, and then

90
00:06:39,190 --> 00:06:40,000
 years later she came back to it.

91
00:06:40,000 --> 00:06:44,210
 And it could be years before he actually has this chance

92
00:06:44,210 --> 00:06:48,210
 again. He may never have this chance again. If we give him

93
00:06:48,210 --> 00:06:51,000
 that week, too dangerous.

94
00:06:51,000 --> 00:06:55,380
 And then Bante Anoma was saying, come on, the extra week

95
00:06:55,380 --> 00:06:59,570
 will be good for him to get used to it, or be sure about it

96
00:06:59,570 --> 00:07:00,000
.

97
00:07:00,000 --> 00:07:05,160
 And I said, maybe for Sri Lankan people, but for Westerners

98
00:07:05,160 --> 00:07:09,100
, you never know. So we decided that this is what we're

99
00:07:09,100 --> 00:07:12,000
 going to do. If he ordains, he's going to ordain.

100
00:07:12,000 --> 00:07:16,680
 What did you ordain Monday? He was a Monday, no? He's going

101
00:07:16,680 --> 00:07:19,940
 to ordain Monday. If he doesn't ordain Monday, he's not

102
00:07:19,940 --> 00:07:21,000
 going to ordain.

103
00:07:21,000 --> 00:07:25,000
 We'll do it like that. I think that's what we decided on.

104
00:07:25,000 --> 00:07:30,000
 Yeah, and that was not the next day, but the day after.

105
00:07:30,000 --> 00:07:33,750
 So then the next morning I went to see him. We had this

106
00:07:33,750 --> 00:07:36,000
 beautiful sunny morning chat.

107
00:07:36,000 --> 00:07:42,010
 Sunday, Sunday, Sunday morning chat. And I think, I don't

108
00:07:42,010 --> 00:07:46,380
 know, still. And so we were back and forth, and I was

109
00:07:46,380 --> 00:07:47,000
 saying,

110
00:07:47,000 --> 00:07:51,580
 "Well, that's not a very good way to start. That doesn't

111
00:07:51,580 --> 00:07:55,680
 sound like the start of a spiritual life. You have to be

112
00:07:55,680 --> 00:07:57,000
 more sure than that."

113
00:07:57,000 --> 00:08:01,000
 And then we talked about it some more and more. And I said,

114
00:08:01,000 --> 00:08:03,990
 and it was interesting how it came out, because it made me

115
00:08:03,990 --> 00:08:05,000
 see something.

116
00:08:05,000 --> 00:08:09,550
 And it was what I said to him in the end, is that sometimes

117
00:08:09,550 --> 00:08:12,000
 you just have to take the leap.

118
00:08:12,000 --> 00:08:15,420
 Because he wasn't sure if he would take the leap. And I

119
00:08:15,420 --> 00:08:18,220
 think that's good advice to anyone who's thinking about

120
00:08:18,220 --> 00:08:19,000
 this course.

121
00:08:19,000 --> 00:08:23,820
 Don't sit there and waffle about it. Just, if you're going

122
00:08:23,820 --> 00:08:25,000
 to do it, do it.

123
00:08:25,000 --> 00:08:30,650
 And when the time comes, take the leap. Do it. And you'll

124
00:08:30,650 --> 00:08:34,000
 get that over with.

125
00:08:34,000 --> 00:08:37,930
 Too often, things that are worth doing don't get done

126
00:08:37,930 --> 00:08:40,000
 because of hesitation.

127
00:08:40,000 --> 00:08:43,310
 And this is that famous saying, "He who hesitates is lost

128
00:08:43,310 --> 00:08:44,000
 forever."

129
00:08:44,000 --> 00:08:47,690
 And you lose the opportunity. You only get an opportunity

130
00:08:47,690 --> 00:08:48,000
 once.

131
00:08:48,000 --> 00:08:50,760
 If an opportunity comes later, it's a different opportunity

132
00:08:50,760 --> 00:08:51,000
.

133
00:08:51,000 --> 00:08:54,000
 That opportunity is always a one-time thing.

134
00:08:54,000 --> 00:08:57,450
 When you have the chance to do something that you know is

135
00:08:57,450 --> 00:09:00,000
 right, don't hesitate with it.

136
00:09:00,000 --> 00:09:02,390
 Take the leap. And then the other thing that we were

137
00:09:02,390 --> 00:09:04,000
 talking about is that you won't regret it.

138
00:09:04,000 --> 00:09:08,080
 It's not like, in this case, you're doing something that

139
00:09:08,080 --> 00:09:10,000
 you could possibly later on say,

140
00:09:10,000 --> 00:09:14,660
 "Oh, I wasted my life sitting there in the forest when I

141
00:09:14,660 --> 00:09:20,000
 could have been doing something much more."

142
00:09:20,000 --> 00:09:23,000
 No, because there's nothing else. And this is clear.

143
00:09:23,000 --> 00:09:29,320
 So, overcoming and suppressing those doubts and just going

144
00:09:29,320 --> 00:09:31,000
 for it.

145
00:09:31,000 --> 00:09:37,000
 And I guess the corollary question that we have to ask is,

146
00:09:37,000 --> 00:09:38,000
 how do you feel now?

147
00:09:38,000 --> 00:09:42,000
 Was it a mistake?

148
00:09:42,000 --> 00:09:47,000
 Completely. I'm happy with it.

149
00:09:47,000 --> 00:09:53,570
 I mean, yeah, the talk was definitely helpful because then

150
00:09:53,570 --> 00:09:54,000
 it's like,

151
00:09:54,000 --> 00:09:57,000
 you're seeing these problems that you're having.

152
00:09:57,000 --> 00:10:01,860
 And then it's just the other person being like, "Huh, yeah,

153
00:10:01,860 --> 00:10:03,000
 well, this or that."

154
00:10:03,000 --> 00:10:10,000
 And kind of making you just look at it better.

155
00:10:10,000 --> 00:10:18,000
 And, yeah, I'm definitely happy with the decision.

156
00:10:18,000 --> 00:10:26,460
 It's just the hesitation and such. Because then you

157
00:10:26,460 --> 00:10:28,000
 hesitate about it, "Oh, I don't know."

158
00:10:28,000 --> 00:10:31,300
 And then more doubt comes. Then you start weaving more and

159
00:10:31,300 --> 00:10:32,000
 more things.

160
00:10:32,000 --> 00:10:35,000
 You just have to do it and then you're happy with it.

161
00:10:35,000 --> 00:10:37,940
 As soon as you made the decision, it was like, "Okay, one

162
00:10:37,940 --> 00:10:39,000
 more day."

163
00:10:39,000 --> 00:10:41,000
 And then it was just a matter of purpose.

164
00:10:41,000 --> 00:10:44,000
 Yeah, then it's just, then it becomes easy.

165
00:10:44,000 --> 00:10:52,000
 So, yeah, I mean, it's good. It's better than anything else

166
00:10:52,000 --> 00:10:56,000
 I could see myself doing.

167
00:10:56,000 --> 00:11:04,000
 It reminds me of when you had the question on education.

168
00:11:04,000 --> 00:11:08,730
 You said, "This is the best school." That's what it makes

169
00:11:08,730 --> 00:11:10,000
 me think of.

170
00:11:10,000 --> 00:11:14,910
 Because, I mean, it really is the best thing that you can

171
00:11:14,910 --> 00:11:16,000
 be doing.

172
00:11:16,000 --> 00:11:20,000
 You just have time to meditate, to study.

173
00:11:20,000 --> 00:11:27,280
 You just wear some robes, go walk around the village to get

174
00:11:27,280 --> 00:11:28,000
 some food.

175
00:11:28,000 --> 00:11:31,850
 And then you just have more time to do something that's

176
00:11:31,850 --> 00:11:34,000
 actually useful for yourself

177
00:11:34,000 --> 00:11:41,850
 instead of useful for continuing on all the various doings

178
00:11:41,850 --> 00:11:44,000
 of the world.

179
00:11:44,000 --> 00:11:45,000
 [

