1
00:00:00,000 --> 00:00:04,090
 Hello and welcome back to Ask a Monk. Today I'll be

2
00:00:04,090 --> 00:00:05,360
 answering the question as

3
00:00:05,360 --> 00:00:11,360
 to what a Buddhist does when someone they know dies, which

4
00:00:11,360 --> 00:00:12,640
 is not

5
00:00:12,640 --> 00:00:16,720
 directly related to the meditation practice, but as Buddh

6
00:00:16,720 --> 00:00:17,280
ists and

7
00:00:17,280 --> 00:00:20,600
 meditators in the Buddhist tradition, this is something

8
00:00:20,600 --> 00:00:22,640
 that we, an experience

9
00:00:22,640 --> 00:00:28,790
 that we have to deal with, that for most of us will be on

10
00:00:28,790 --> 00:00:30,640
 the extreme end,

11
00:00:30,640 --> 00:00:34,150
 something that is quite difficult to deal with and to come

12
00:00:34,150 --> 00:00:35,680
 to terms with.

13
00:00:35,680 --> 00:00:40,220
 So simply meditating it away is not really sufficient. So

14
00:00:40,220 --> 00:00:41,120
 there are things that we

15
00:00:41,120 --> 00:00:46,000
 should talk about in regards to this. From a practical

16
00:00:46,000 --> 00:00:47,040
 point of view, a Buddhist

17
00:00:47,040 --> 00:00:53,760
 point of view, I would say there are three things that we

18
00:00:53,760 --> 00:00:53,840
 should do, that we

19
00:00:53,840 --> 00:00:58,480
 should keep in mind, three duties as Buddhists, or three

20
00:00:58,480 --> 00:00:59,920
 points to keep in

21
00:00:59,920 --> 00:01:03,670
 mind when a person we know dies. The first one is that we

22
00:01:03,670 --> 00:01:04,720
 should never ever

23
00:01:04,720 --> 00:01:10,590
 grieve or think that grieving is somehow appropriate,

24
00:01:10,590 --> 00:01:12,560
 somehow useful, somehow good.

25
00:01:12,560 --> 00:01:17,080
 The reason why we grieve is because it's great suffering

26
00:01:17,080 --> 00:01:18,560
 for us. You can think of

27
00:01:18,560 --> 00:01:22,930
 a small child who has to stay with a babysitter the first

28
00:01:22,930 --> 00:01:23,360
 time when their

29
00:01:23,360 --> 00:01:27,790
 parents go away, will often cry all night thinking that

30
00:01:27,790 --> 00:01:30,000
 they're not even

31
00:01:30,000 --> 00:01:34,840
 thinking that their parents have left, but simply suffering

32
00:01:34,840 --> 00:01:35,800
 terribly because of

33
00:01:35,800 --> 00:01:38,760
 the clinging that they have, that they've developed as a

34
00:01:38,760 --> 00:01:39,840
 small child for their

35
00:01:39,840 --> 00:01:45,750
 parents. So this terrible suffering that goes on at loss,

36
00:01:45,750 --> 00:01:47,520
 which really

37
00:01:47,520 --> 00:01:50,280
 carries over into everything in our lives. People cry over

38
00:01:50,280 --> 00:01:51,040
 many different

39
00:01:51,040 --> 00:01:54,550
 things. Now the point being that we tend to cling to people

40
00:01:54,550 --> 00:01:55,640
 a lot more than we

41
00:01:55,640 --> 00:02:01,840
 cling to any other object really. Other people are things

42
00:02:01,840 --> 00:02:02,120
 that

43
00:02:02,120 --> 00:02:07,390
 we can interact with, that have great deep meaning for us

44
00:02:07,390 --> 00:02:08,560
 in our lives and so

45
00:02:08,560 --> 00:02:12,640
 we cling to them greatly. This is why we feel suffering

46
00:02:12,640 --> 00:02:14,600
 when a person passes

47
00:02:14,600 --> 00:02:21,730
 away. It's not based on any logic or reason that a person

48
00:02:21,730 --> 00:02:22,920
 should grieve and

49
00:02:22,920 --> 00:02:29,320
 we should really get this point across that there is no

50
00:02:29,320 --> 00:02:30,640
 reason to

51
00:02:30,640 --> 00:02:35,250
 mourn. It is something that is based on our addiction. The

52
00:02:35,250 --> 00:02:38,280
 reason why we cry is

53
00:02:38,280 --> 00:02:43,060
 because of the chemicals involved with crying. Crying is a

54
00:02:43,060 --> 00:02:46,160
 reaction that we have

55
00:02:46,160 --> 00:02:51,150
 developed as a species to deal with difficult situations.

56
00:02:51,150 --> 00:02:52,080
 It's something that

57
00:02:52,080 --> 00:02:56,560
 releases pleasure chemicals and endorphins. I think it

58
00:02:56,560 --> 00:02:57,360
 makes you feel

59
00:02:57,360 --> 00:03:01,630
 calm and happy and peaceful so it becomes an addiction

60
00:03:01,630 --> 00:03:04,400
 actually and this is

61
00:03:04,400 --> 00:03:08,560
 how grieving works. It can be crying, some people take to

62
00:03:08,560 --> 00:03:10,040
 alcohol or

63
00:03:10,040 --> 00:03:14,260
 any sort of addiction but crying is again another addiction

64
00:03:14,260 --> 00:03:14,720
. So we should

65
00:03:14,720 --> 00:03:17,780
 never think that this is somehow important or a part of the

66
00:03:17,780 --> 00:03:18,320
 process, a

67
00:03:18,320 --> 00:03:21,410
 part of the grieving process. Many people will go on for

68
00:03:21,410 --> 00:03:22,880
 years suffering

69
00:03:22,880 --> 00:03:29,000
 because a person has passed away. The point is that there's

70
00:03:29,000 --> 00:03:29,920
 no benefit to it.

71
00:03:29,920 --> 00:03:32,950
 You don't help the person who passed away, you don't help

72
00:03:32,950 --> 00:03:34,160
 yourself, you don't do

73
00:03:34,160 --> 00:03:36,990
 something that they would want for you and there's no one

74
00:03:36,990 --> 00:03:38,320
 who wants their loved

75
00:03:38,320 --> 00:03:47,000
 ones to suffer, to mourn, to have to be without. In the end

76
00:03:47,000 --> 00:03:47,480
 it's really

77
00:03:47,480 --> 00:03:50,760
 just useless and in fact worse than useless it drags you

78
00:03:50,760 --> 00:03:51,640
 down in many ways.

79
00:03:51,640 --> 00:04:00,000
 It drags you down into a cycle of suffering and depression

80
00:04:00,000 --> 00:04:01,360
 and

81
00:04:01,360 --> 00:04:04,590
 also addiction. It makes you become, because you start to

82
00:04:04,590 --> 00:04:05,720
 find a way out,

83
00:04:05,720 --> 00:04:09,980
 you try to find a way to block it out and not have to think

84
00:04:09,980 --> 00:04:10,640
 about it which

85
00:04:10,640 --> 00:04:15,920
 gives rise to many different types of addiction. Now this

86
00:04:15,920 --> 00:04:17,200
 brings up a good

87
00:04:17,200 --> 00:04:22,250
 point that often people are unable to follow this advice.

88
00:04:22,250 --> 00:04:23,840
 This first point,

89
00:04:23,840 --> 00:04:29,200
 important as it is, it's best phrased as don't see it as a

90
00:04:29,200 --> 00:04:30,120
 good thing because

91
00:04:30,120 --> 00:04:34,390
 if you say to people don't mourn, well unless they had no

92
00:04:34,390 --> 00:04:35,480
 attachment to the

93
00:04:35,480 --> 00:04:38,970
 person they're going to feel sad. I've given this talk

94
00:04:38,970 --> 00:04:40,160
 before at funerals

95
00:04:40,160 --> 00:04:43,040
 and then after you give a talk the people come up and cry.

96
00:04:43,040 --> 00:04:43,920
 So you think

97
00:04:43,920 --> 00:04:47,520
 well why did I explain to them about not to cry and how it

98
00:04:47,520 --> 00:04:48,600
's not really useful

99
00:04:48,600 --> 00:04:53,720
 or any benefit? But they didn't intend to. You can't blame

100
00:04:53,720 --> 00:04:54,760
 people for crying when

101
00:04:54,760 --> 00:05:02,380
 a loved one dies. So it's, but some people go to the next

102
00:05:02,380 --> 00:05:03,160
 level and think yes

103
00:05:03,160 --> 00:05:05,170
 yes this is good and this is important and so they

104
00:05:05,170 --> 00:05:06,040
 encourage it in themselves

105
00:05:06,040 --> 00:05:11,960
 and they encourage the phenomenon and so they end up crying

106
00:05:11,960 --> 00:05:12,760
 more and

107
00:05:12,760 --> 00:05:15,260
 they think of it as somehow a good thing and so they can

108
00:05:15,260 --> 00:05:16,160
 get caught up and

109
00:05:16,160 --> 00:05:19,730
 actually waste a lot of time and energy doing so. But this

110
00:05:19,730 --> 00:05:21,320
 also raises another

111
00:05:21,320 --> 00:05:29,470
 point that we most often get ourselves into this position.

112
00:05:29,470 --> 00:05:30,400
 Why when a person

113
00:05:30,400 --> 00:05:33,680
 dies we can go on for days and weeks and months and even

114
00:05:33,680 --> 00:05:34,760
 years mourning is

115
00:05:34,760 --> 00:05:41,080
 because we've been negligent, we've been heedless in

116
00:05:41,080 --> 00:05:42,280
 regards to our mind. We

117
00:05:42,280 --> 00:05:45,110
 haven't been paying attention to our attachments and you

118
00:05:45,110 --> 00:05:45,880
 know we live our

119
00:05:45,880 --> 00:05:48,930
 lives thinking that attaching to other people is good. It's

120
00:05:48,930 --> 00:05:49,880
 intimacy in

121
00:05:49,880 --> 00:05:54,680
 relationships and love and so on. But this really has

122
00:05:54,680 --> 00:05:55,240
 nothing to do

123
00:05:55,240 --> 00:05:58,080
 with love. It has to do with our own clinging. When you

124
00:05:58,080 --> 00:05:59,280
 love someone it's not

125
00:05:59,280 --> 00:06:03,440
 what they can do for you and what they bring to you and the

126
00:06:03,440 --> 00:06:03,840
 great things

127
00:06:03,840 --> 00:06:07,130
 that they give to you. That's a kind of, we use the word

128
00:06:07,130 --> 00:06:09,200
 love, but it really is an

129
00:06:09,200 --> 00:06:12,760
 attachment. Love is when you wish for good things for other

130
00:06:12,760 --> 00:06:14,480
 people and there's

131
00:06:14,480 --> 00:06:18,010
 nothing good that comes from crying. You don't somehow

132
00:06:18,010 --> 00:06:19,080
 respect the person's

133
00:06:19,080 --> 00:06:22,990
 memory by crying, by bringing yourself suffering. You don't

134
00:06:22,990 --> 00:06:23,600
 do something that

135
00:06:23,600 --> 00:06:26,670
 they would want you to do or that they would want for you.

136
00:06:26,670 --> 00:06:27,240
 So this is the

137
00:06:27,240 --> 00:06:31,800
 first point. But no, what I was going to say is that for

138
00:06:31,800 --> 00:06:32,960
 this reason, because we

139
00:06:32,960 --> 00:06:37,330
 get ourselves into this mess, part of this is even before a

140
00:06:37,330 --> 00:06:38,840
 person dies we

141
00:06:38,840 --> 00:06:42,160
 should be quite careful with our relationships. Our

142
00:06:42,160 --> 00:06:43,400
 relationships should

143
00:06:43,400 --> 00:06:46,440
 be meaningful but the meaning should come from our love for

144
00:06:46,440 --> 00:06:47,160
 each other, our

145
00:06:47,160 --> 00:06:52,730
 wish for each other to be happy and in fact our wish for

146
00:06:52,730 --> 00:06:53,840
 all beings to be

147
00:06:53,840 --> 00:06:59,000
 happy. The only way we can really have pure love for other

148
00:06:59,000 --> 00:06:59,720
 beings is

149
00:06:59,720 --> 00:07:03,670
 to have it be impartial. It's not that we don't love anyone

150
00:07:03,670 --> 00:07:04,280
, we end up

151
00:07:04,280 --> 00:07:07,210
 loving everyone. This is the Buddha said that we should

152
00:07:07,210 --> 00:07:08,800
 strive to love all

153
00:07:08,800 --> 00:07:14,560
 beings just as a mother might love her only child. If we

154
00:07:14,560 --> 00:07:15,160
 can get to this

155
00:07:15,160 --> 00:07:17,850
 point, if we really have love for people it won't matter

156
00:07:17,850 --> 00:07:19,000
 who comes and who goes.

157
00:07:19,000 --> 00:07:26,030
 Once it's true love then the next person you see is a loved

158
00:07:26,030 --> 00:07:26,520
 one for you,

159
00:07:26,520 --> 00:07:32,780
 is the one you love and it continues on when they go. There

160
00:07:32,780 --> 00:07:33,040
's

161
00:07:33,040 --> 00:07:36,330
 nothing in love that says they need to do anything for you

162
00:07:36,330 --> 00:07:36,880
 or be anything

163
00:07:36,880 --> 00:07:41,110
 for you. So when they're gone there's no suffering, there's

164
00:07:41,110 --> 00:07:41,520
 no stress,

165
00:07:41,520 --> 00:07:46,430
 there's no sadness. So this is an important reason why we

166
00:07:46,430 --> 00:07:46,880
 should be

167
00:07:46,880 --> 00:07:50,620
 practicing meditation because when we don't practice

168
00:07:50,620 --> 00:07:52,360
 meditation we cling and

169
00:07:52,360 --> 00:07:56,800
 cling and cling and we set ourselves up for so much stress

170
00:07:56,800 --> 00:07:57,840
 and suffering that we

171
00:07:57,840 --> 00:08:00,920
 see all around us. Many people are living their lives in

172
00:08:00,920 --> 00:08:01,560
 great peace and

173
00:08:01,560 --> 00:08:05,430
 happiness simply because they haven't met with the

174
00:08:05,430 --> 00:08:07,080
 inevitable crash, they have

175
00:08:07,080 --> 00:08:11,960
 an inevitable loss, loss of youth when they get old, so

176
00:08:11,960 --> 00:08:12,720
 many things that they

177
00:08:12,720 --> 00:08:17,640
 can't enjoy and suddenly they're confronted with old age or

178
00:08:17,640 --> 00:08:21,460
 sickness when people get cancer and eventually death. The

179
00:08:21,460 --> 00:08:22,400
 death of loved

180
00:08:22,400 --> 00:08:25,240
 ones or the death of or one's own death which they're

181
00:08:25,240 --> 00:08:26,960
 totally unprepared for.

182
00:08:26,960 --> 00:08:31,440
 Meditation practice is to allow us to have meaningful but

183
00:08:31,440 --> 00:08:34,800
 non-attached

184
00:08:34,800 --> 00:08:40,920
 relationships with other people where we give and we spread

185
00:08:40,920 --> 00:08:41,160
 the

186
00:08:41,160 --> 00:08:47,000
 goodness all around us and try our best to make other

187
00:08:47,000 --> 00:08:48,960
 people happy and to

188
00:08:48,960 --> 00:08:53,560
 bring peace to them instead of wishing for them to bring

189
00:08:53,560 --> 00:08:55,040
 good things to us

190
00:08:55,040 --> 00:08:58,770
 which is of course a totally uncertain something we can't

191
00:08:58,770 --> 00:09:00,040
 depend on. So this is

192
00:09:00,040 --> 00:09:05,080
 the first point is that it's really useless to grieve and

193
00:09:05,080 --> 00:09:05,200
 it's

194
00:09:05,200 --> 00:09:08,040
 something that as Buddhists we should on the other hand

195
00:09:08,040 --> 00:09:09,040
 spend our

196
00:09:09,040 --> 00:09:13,920
 lives thinking about the inevitability of death and this

197
00:09:13,920 --> 00:09:14,600
 brings me to the

198
00:09:14,600 --> 00:09:18,840
 second point that when a person dies it should be a

199
00:09:18,840 --> 00:09:20,480
 reminder to us and the Buddha

200
00:09:20,480 --> 00:09:25,120
 was very clear about this for monks when a person dies and

201
00:09:25,120 --> 00:09:26,040
 even for

202
00:09:26,040 --> 00:09:31,120
 laypeople when a person dies that we should consider this

203
00:09:31,120 --> 00:09:31,320
 to be a

204
00:09:31,320 --> 00:09:35,380
 sign, a sign that we too will have to go. We should be

205
00:09:35,380 --> 00:09:36,600
 clear that this is

206
00:09:36,600 --> 00:09:39,750
 something that we can't avoid, it's a fact of life. So the

207
00:09:39,750 --> 00:09:41,240
 contemplation on the

208
00:09:41,240 --> 00:09:47,400
 reality of death is a very important part of our acceptance

209
00:09:47,400 --> 00:09:48,480
 when a person

210
00:09:48,480 --> 00:09:56,680
 dies. So first of all the realization that this was a part

211
00:09:56,680 --> 00:09:56,960
 of that

212
00:09:56,960 --> 00:10:00,140
 person's life and that it's a part of nature and not

213
00:10:00,140 --> 00:10:01,640
 getting caught up in this

214
00:10:01,640 --> 00:10:06,880
 idea of a person or this one life or this being existed and

215
00:10:06,880 --> 00:10:07,200
 now they're

216
00:10:07,200 --> 00:10:10,500
 gone or so on but thinking in terms of reality in terms of

217
00:10:10,500 --> 00:10:12,120
 our experience and

218
00:10:12,120 --> 00:10:16,780
 that this is the nature of reality that all things that

219
00:10:16,780 --> 00:10:17,600
 arise in

220
00:10:17,600 --> 00:10:20,330
 our mind the idea of a person will eventually cease

221
00:10:20,330 --> 00:10:21,760
 everything that we cling

222
00:10:21,760 --> 00:10:29,920
 to a person, a place, a thing, even an ideology. Eventually

223
00:10:29,920 --> 00:10:31,200
 we will have

224
00:10:31,200 --> 00:10:36,040
 to give it up, nothing will last forever and so we use

225
00:10:36,040 --> 00:10:40,960
 death as a really good

226
00:10:40,960 --> 00:10:45,430
 good wake-up call for us to realize the truth of reality

227
00:10:45,430 --> 00:10:47,280
 and also to reflect on

228
00:10:47,280 --> 00:10:51,850
 our own mortality. When a person dies we should take that

229
00:10:51,850 --> 00:10:52,480
 as a warning

230
00:10:52,480 --> 00:10:56,370
 to ourselves. We should watch and see and look and think of

231
00:10:56,370 --> 00:10:57,680
 the person who is dead

232
00:10:57,680 --> 00:11:01,840
 and think this will also come to me one day will I be ready

233
00:11:01,840 --> 00:11:03,080
 for it. As would it's

234
00:11:03,080 --> 00:11:07,620
 we this is an important not only is it a good meditation

235
00:11:07,620 --> 00:11:09,280
 and useful for ourselves

236
00:11:09,280 --> 00:11:11,470
 but it's a really good way to come to terms with the person

237
00:11:11,470 --> 00:11:12,560
's death because

238
00:11:12,560 --> 00:11:17,600
 instead of grieving or thinking oh now that person's gone

239
00:11:17,600 --> 00:11:19,080
 you realize that you

240
00:11:19,080 --> 00:11:23,400
 remind yourself that this is reality and the real problem

241
00:11:23,400 --> 00:11:23,840
 is our

242
00:11:23,840 --> 00:11:27,430
 attachments to a specific state of being of a person, a

243
00:11:27,430 --> 00:11:29,040
 place, a thing being like

244
00:11:29,040 --> 00:11:32,480
 this not being like that or being with us and not being

245
00:11:32,480 --> 00:11:33,960
 away from us. Once we

246
00:11:33,960 --> 00:11:39,560
 start to look at reality and look at the experience of

247
00:11:39,560 --> 00:11:40,520
 reality from

248
00:11:40,520 --> 00:11:44,820
 moment to moment now what's happening and so on then the

249
00:11:44,820 --> 00:11:47,040
 idea of how things

250
00:11:47,040 --> 00:11:51,020
 should be or how things are could be or how things used to

251
00:11:51,020 --> 00:11:51,840
 be or how things

252
00:11:51,840 --> 00:11:55,320
 will be or so on is really irrelevant and it doesn't have

253
00:11:55,320 --> 00:11:56,600
 the same hold on us.

254
00:11:56,600 --> 00:12:00,270
 We're able to live here and now in peace and happiness and

255
00:12:00,270 --> 00:12:01,000
 that here and now

256
00:12:01,000 --> 00:12:04,560
 extends forever of course when you're able to be happy here

257
00:12:04,560 --> 00:12:05,440
 and now then

258
00:12:05,440 --> 00:12:10,500
 you're able to be happy for eternity and that's the reality

259
00:12:10,500 --> 00:12:12,480
 of life. So that's

260
00:12:12,480 --> 00:12:15,350
 number two is that we should use it as an opportunity to

261
00:12:15,350 --> 00:12:16,280
 reflect on our own

262
00:12:16,280 --> 00:12:20,290
 mortality and reflect on the inevitability of death that it

263
00:12:20,290 --> 00:12:20,840
 comes to

264
00:12:20,840 --> 00:12:23,540
 all beings and we should remind ourselves that this is

265
00:12:23,540 --> 00:12:24,120
 really the

266
00:12:24,120 --> 00:12:30,330
 reality of that person and it shouldn't shouldn't actually

267
00:12:30,330 --> 00:12:32,400
 be any be of any

268
00:12:32,400 --> 00:12:36,170
 positive or negative significance it should be seen as the

269
00:12:36,170 --> 00:12:37,600
 way things are. We

270
00:12:37,600 --> 00:12:41,270
 should not be happy or unhappy about it, should not be

271
00:12:41,270 --> 00:12:43,360
 regretful or mourning or

272
00:12:43,360 --> 00:12:47,110
 so on. This is what we should do is we should take it as an

273
00:12:47,110 --> 00:12:48,360
 opportunity to

274
00:12:48,360 --> 00:12:53,360
 reflect on the way things go in life. Now this is more for

275
00:12:53,360 --> 00:12:54,000
 ourselves for

276
00:12:54,000 --> 00:12:57,840
 the person who passed away most people want to know what we

277
00:12:57,840 --> 00:12:58,200
 should do

278
00:12:58,200 --> 00:13:01,920
 for the person. This is the third point that as Buddhists

279
00:13:01,920 --> 00:13:03,400
 we should do things on

280
00:13:03,400 --> 00:13:06,360
 behalf of the person who has passed away. We should

281
00:13:06,360 --> 00:13:07,960
 whatever good things that

282
00:13:07,960 --> 00:13:12,510
 person undertook or even if they didn't do any good things

283
00:13:12,510 --> 00:13:13,960
 we should carry on

284
00:13:13,960 --> 00:13:24,600
 their name by or celebrate their life by giving by building

285
00:13:24,600 --> 00:13:25,400
 things by

286
00:13:25,400 --> 00:13:29,950
 building things that will be of use to people, dedicating

287
00:13:29,950 --> 00:13:31,760
 our works to them and

288
00:13:31,760 --> 00:13:35,670
 so on, dedicating books or whatever we do dedicating our

289
00:13:35,670 --> 00:13:37,680
 work our good deeds to

290
00:13:37,680 --> 00:13:40,840
 that person be it things that they did in their life and we

291
00:13:40,840 --> 00:13:42,000
 can continue on or

292
00:13:42,000 --> 00:13:47,060
 just things done in their name with some mention of them

293
00:13:47,060 --> 00:13:50,200
 because this this is

294
00:13:50,200 --> 00:13:52,850
 really a way of celebrating the person's life and it's a

295
00:13:52,850 --> 00:13:54,120
 way of bringing meaning

296
00:13:54,120 --> 00:13:56,590
 to that person's life. It's a way of saying that this

297
00:13:56,590 --> 00:13:57,720
 person's life had real

298
00:13:57,720 --> 00:14:02,500
 meaning. If this person hadn't lived then these good deeds

299
00:14:02,500 --> 00:14:03,040
 wouldn't come.

300
00:14:03,040 --> 00:14:06,140
 We do these deeds especially for that person and therefore

301
00:14:06,140 --> 00:14:07,160
 they are those

302
00:14:07,160 --> 00:14:11,180
 person's good deeds that have been done. It's a way of

303
00:14:11,180 --> 00:14:13,960
 respecting and celebrating

304
00:14:13,960 --> 00:14:17,840
 and honoring the person who passed away. It's also a way of

305
00:14:17,840 --> 00:14:18,920
 coming to terms with

306
00:14:18,920 --> 00:14:22,430
 the death in the best way possible celebrating the person's

307
00:14:22,430 --> 00:14:23,080
 life saying

308
00:14:23,080 --> 00:14:27,560
 this person passed away. Most people the best they can do

309
00:14:27,560 --> 00:14:27,880
 is to

310
00:14:27,880 --> 00:14:30,210
 remember the good things the person did and say oh yeah I

311
00:14:30,210 --> 00:14:31,240
 was such a good person

312
00:14:31,240 --> 00:14:33,590
 so they celebrate and they have a party and they get drunk

313
00:14:33,590 --> 00:14:34,560
 and so on and that's

314
00:14:34,560 --> 00:14:38,050
 it but that's not really celebrating a person's life. When

315
00:14:38,050 --> 00:14:38,720
 you celebrate a

316
00:14:38,720 --> 00:14:40,910
 person's life it should be all the good things that they

317
00:14:40,910 --> 00:14:42,080
 did should be continued

318
00:14:42,080 --> 00:14:45,490
 on or goodness should be done in their name so it's a

319
00:14:45,490 --> 00:14:46,600
 person who means

320
00:14:46,600 --> 00:14:50,020
 something to you and you think their life had meaning then

321
00:14:50,020 --> 00:14:50,920
 you should do

322
00:14:50,920 --> 00:14:54,610
 something it may not be a lot but you should make effort to

323
00:14:54,610 --> 00:14:55,440
 do something in

324
00:14:55,440 --> 00:15:01,680
 their name and that will be your way of finding closure and

325
00:15:01,680 --> 00:15:02,600
 of ending that

326
00:15:02,600 --> 00:15:06,650
 person's life on a meaningful note saying this person died

327
00:15:06,650 --> 00:15:07,600
 and the result

328
00:15:07,600 --> 00:15:12,470
 was this we have done this on behalf of that person.

329
00:15:12,470 --> 00:15:14,080
 Sometimes what that means is

330
00:15:14,080 --> 00:15:17,380
 an inheritance that we get we we use part of the

331
00:15:17,380 --> 00:15:19,120
 inheritance to honor the

332
00:15:19,120 --> 00:15:23,560
 person to do something not not because out of duty or

333
00:15:23,560 --> 00:15:25,400
 something but as as a good

334
00:15:25,400 --> 00:15:28,380
 deed for us and a good deed for them and something bringing

335
00:15:28,380 --> 00:15:29,400
 goodness to the world

336
00:15:29,400 --> 00:15:33,620
 on behalf of that person. What you find more often which is

337
00:15:33,620 --> 00:15:34,160
 really the worst

338
00:15:34,160 --> 00:15:37,020
 thing a person could possibly do when another person dies

339
00:15:37,020 --> 00:15:38,720
 is people fight over

340
00:15:38,720 --> 00:15:43,350
 the inheritance of their parents or or people who passed

341
00:15:43,350 --> 00:15:44,200
 away which is really a

342
00:15:44,200 --> 00:15:48,230
 horrible thing but apparently goes on quite often and this

343
00:15:48,230 --> 00:15:49,440
 is something as

344
00:15:49,440 --> 00:15:52,920
 Buddhist we should guard against totally we should never

345
00:15:52,920 --> 00:15:54,040
 think of a person's

346
00:15:54,040 --> 00:15:59,180
 another person's possessions that somehow belonging to us

347
00:15:59,180 --> 00:15:59,880
 or or that

348
00:15:59,880 --> 00:16:03,140
 somehow we deserve them never we should never look at a

349
00:16:03,140 --> 00:16:04,080
 person and think this

350
00:16:04,080 --> 00:16:06,800
 person owes me something as Buddhist we should never think

351
00:16:06,800 --> 00:16:08,920
 like that so the idea

352
00:16:08,920 --> 00:16:12,410
 even if your parents you you help them or whatever you

353
00:16:12,410 --> 00:16:13,300
 should never think that

354
00:16:13,300 --> 00:16:15,590
 somehow that's going to give you some inheritance and you

355
00:16:15,590 --> 00:16:16,280
're going to get

356
00:16:16,280 --> 00:16:19,130
 something we should be very careful to guard against this

357
00:16:19,130 --> 00:16:19,840
 because it will crop

358
00:16:19,840 --> 00:16:24,000
 up and when they die you'll find yourself fighting and

359
00:16:24,000 --> 00:16:26,040
 breaking apart your family

360
00:16:26,040 --> 00:16:28,520
 really could you imagine what your parents would what the

361
00:16:28,520 --> 00:16:29,200
 parents would

362
00:16:29,200 --> 00:16:32,830
 think when they see their children you know at each other's

363
00:16:32,830 --> 00:16:33,560
 throats over things

364
00:16:33,560 --> 00:16:36,520
 that they actually left behind that their parents had to

365
00:16:36,520 --> 00:16:37,360
 throw away that's

366
00:16:37,360 --> 00:16:40,320
 really garbage well it could be money it could be home

367
00:16:40,320 --> 00:16:42,040
 possession it's all

368
00:16:42,040 --> 00:16:44,590
 garbage because in the end they had to leave it behind and

369
00:16:44,590 --> 00:16:45,600
 you have to leave it

370
00:16:45,600 --> 00:16:50,390
 behind much better is whatever good things that they have

371
00:16:50,390 --> 00:16:51,320
 you can take some

372
00:16:51,320 --> 00:16:54,310
 of it if it's useful to support you but making sure that

373
00:16:54,310 --> 00:16:55,640
 everyone else is taken

374
00:16:55,640 --> 00:16:59,500
 care of and giving things in their name you know if the

375
00:16:59,500 --> 00:17:01,920
 least you do is giving

376
00:17:01,920 --> 00:17:07,380
 up some some hold that you have on on a what you know this

377
00:17:07,380 --> 00:17:08,740
 this fights that

378
00:17:08,740 --> 00:17:15,340
 people have over inheritance in in order that harmony might

379
00:17:15,340 --> 00:17:16,600
 be might be bestowed

380
00:17:16,600 --> 00:17:19,900
 upon your family in order for the sake of harmony and out

381
00:17:19,900 --> 00:17:21,100
 of respect for the

382
00:17:21,100 --> 00:17:23,770
 person who passed away I'm not going to fight over this if

383
00:17:23,770 --> 00:17:24,520
 the least if that's

384
00:17:24,520 --> 00:17:26,840
 the least you then you've done a great thing in their

385
00:17:26,840 --> 00:17:28,180
 memory and you've honored

386
00:17:28,180 --> 00:17:32,910
 their memory so you know there are many ways that you can

387
00:17:32,910 --> 00:17:33,840
 do this but this is

388
00:17:33,840 --> 00:17:38,490
 it could be the least is to not for for goodness sake don't

389
00:17:38,490 --> 00:17:39,920
 fight over people

390
00:17:39,920 --> 00:17:45,280
 who passed away fight over their belongings it's a terrible

391
00:17:45,280 --> 00:17:45,480
 terrible

392
00:17:45,480 --> 00:17:48,070
 thing and be very careful not to let that happen to you

393
00:17:48,070 --> 00:17:49,240
 because it might come

394
00:17:49,240 --> 00:17:52,270
 into the mind we can never be sure we might find ourselves

395
00:17:52,270 --> 00:17:53,640
 doing that so these

396
00:17:53,640 --> 00:17:55,990
 are three points that I think you should keep in mind when

397
00:17:55,990 --> 00:17:56,760
 a person you know

398
00:17:56,760 --> 00:18:01,320
 passes away as a Buddhist or as a person who follows these

399
00:18:01,320 --> 00:18:02,760
 sorts of teachings and

400
00:18:02,760 --> 00:18:05,520
 really any good teachings it's nothing specifically to do

401
00:18:05,520 --> 00:18:06,640
 with with Buddhism

402
00:18:06,640 --> 00:18:12,040
 so hope this helps thanks for tuning in and all the best

403
00:18:12,040 --> 00:18:14,080
 you

