1
00:00:00,000 --> 00:00:16,040
 Today I will finish up the set of talks on the asava, the

2
00:00:16,040 --> 00:00:17,160
 developments which make the

3
00:00:17,160 --> 00:00:19,160
 mind go bad.

4
00:00:19,160 --> 00:00:29,110
 It's important to understand not trying to make things seem

5
00:00:29,110 --> 00:00:34,200
 very negative or very unpleasant.

6
00:00:34,200 --> 00:00:37,490
 We're only trying to do away with that part of our

7
00:00:37,490 --> 00:00:41,080
 existence which is undesirable, that

8
00:00:41,080 --> 00:00:45,450
 part of our existence which is uncomfortable, which is

9
00:00:45,450 --> 00:00:46,720
 unpleasant.

10
00:00:46,720 --> 00:00:48,790
 All of the good things, all of the wonderful things are the

11
00:00:48,790 --> 00:00:51,160
 things which we should keep.

12
00:00:51,160 --> 00:00:55,770
 Things like love, things like mindfulness and wisdom,

13
00:00:55,770 --> 00:00:59,000
 consideration, gratitude, charity

14
00:00:59,000 --> 00:01:05,120
 and so on, compassion, peace.

15
00:01:05,120 --> 00:01:07,350
 All of these good things are the things which we are

16
00:01:07,350 --> 00:01:08,080
 striving for.

17
00:01:08,080 --> 00:01:11,190
 But the reason why we don't achieve them all of the time is

18
00:01:11,190 --> 00:01:12,720
 because of certain things

19
00:01:12,720 --> 00:01:16,710
 which exist in our own hearts and in the hearts of the

20
00:01:16,710 --> 00:01:18,400
 people around us.

21
00:01:18,400 --> 00:01:24,490
 And so when our unwholesome tendencies collide with other

22
00:01:24,490 --> 00:01:28,200
 people's unwholesome mind states,

23
00:01:28,200 --> 00:01:33,310
 this is where the cause of suffering, this is what leads to

24
00:01:33,310 --> 00:01:36,080
 struggle, what leads to conflict,

25
00:01:36,080 --> 00:01:39,920
 which leads to suffering in the end.

26
00:01:39,920 --> 00:01:41,600
 So we're trying to do away with these things.

27
00:01:41,600 --> 00:01:43,910
 It's important to understand that we want to be happy and

28
00:01:43,910 --> 00:01:45,360
 we're practicing for the purpose

29
00:01:45,360 --> 00:01:49,560
 of being at peace and being happy.

30
00:01:49,560 --> 00:01:52,240
 What we're looking at like the doctor, we'll look at the

31
00:01:52,240 --> 00:01:53,500
 sickness in the body.

32
00:01:53,500 --> 00:01:56,800
 We're looking at the sicknesses in the mind.

33
00:01:56,800 --> 00:02:00,760
 We're strong enough, we are competent people.

34
00:02:00,760 --> 00:02:06,070
 We are people who have strong minds, people who have strong

35
00:02:06,070 --> 00:02:07,920
 goodness inside.

36
00:02:07,920 --> 00:02:11,100
 And not people who are set on doing evil deeds or set on

37
00:02:11,100 --> 00:02:12,320
 doing bad deeds.

38
00:02:12,320 --> 00:02:15,530
 So this is why we can talk about and we can teach and we

39
00:02:15,530 --> 00:02:16,840
 can practice together to try

40
00:02:16,840 --> 00:02:20,660
 and do away with these things because they are a relatively

41
00:02:20,660 --> 00:02:22,480
 small part of our minds.

42
00:02:22,480 --> 00:02:24,600
 All of the good things are very strong in our minds.

43
00:02:24,600 --> 00:02:29,400
 This is why we're able to deal with these unwholesome

44
00:02:29,400 --> 00:02:30,440
 things.

45
00:02:30,440 --> 00:02:33,320
 And the reason why we come to do this is because of the

46
00:02:33,320 --> 00:02:35,120
 wholesomeness in our mind.

47
00:02:35,120 --> 00:02:37,630
 If we didn't have goodness in our minds, we wouldn't have

48
00:02:37,630 --> 00:02:38,920
 any interest in doing away

49
00:02:38,920 --> 00:02:41,750
 with the bad things, the things which are unpleasant, which

50
00:02:41,750 --> 00:02:43,480
 are unwholesome, which are

51
00:02:43,480 --> 00:02:49,240
 a cause for suffering for ourselves and others.

52
00:02:49,240 --> 00:02:53,770
 But since we are, and this is not a boasting or bragging,

53
00:02:53,770 --> 00:02:56,400
 it's an offering encouragement

54
00:02:56,400 --> 00:02:59,910
 to everyone who has come here, an appreciation to everyone

55
00:02:59,910 --> 00:03:02,160
 who comes to practice meditation.

56
00:03:02,160 --> 00:03:05,840
 It's a very obvious, stating an obvious fact, that a person

57
00:03:05,840 --> 00:03:07,960
 who comes to practice to develop

58
00:03:07,960 --> 00:03:11,420
 themselves, to cultivate good deeds and good states of mind

59
00:03:11,420 --> 00:03:13,440
, wholesome states of mind which

60
00:03:13,440 --> 00:03:16,440
 lead to happiness and lead to peace.

61
00:03:16,440 --> 00:03:19,790
 It takes quite a special person, a person who is really

62
00:03:19,790 --> 00:03:21,800
 keen on bringing happiness and

63
00:03:21,800 --> 00:03:24,400
 peace to this world.

64
00:03:24,400 --> 00:03:27,590
 So it's worth appreciating and worth understanding in this

65
00:03:27,590 --> 00:03:28,040
 way.

66
00:03:28,040 --> 00:03:32,120
 And since the people who come here have these good states

67
00:03:32,120 --> 00:03:34,560
 of mind, this is why they're able

68
00:03:34,560 --> 00:03:39,290
 to address the unwholesome side because of the goodness

69
00:03:39,290 --> 00:03:41,600
 which they have inside.

70
00:03:41,600 --> 00:03:49,630
 And so today's talk is about the bhagwana bhagatapar, the

71
00:03:49,630 --> 00:03:53,960
 unwholesome states inside which can be

72
00:03:53,960 --> 00:03:59,430
 removed through developing or through cultivating, through

73
00:03:59,430 --> 00:04:02,880
 bringing about a higher state of being,

74
00:04:02,880 --> 00:04:09,520
 through raising up one's mind, bhagwana.

75
00:04:09,520 --> 00:04:15,560
 And this is the peak of the talk.

76
00:04:15,560 --> 00:04:19,620
 This is the culmination of all of the other ways and

77
00:04:19,620 --> 00:04:22,880
 methods for overcoming wholesomeness

78
00:04:22,880 --> 00:04:24,480
 and doing away with wholesomeness.

79
00:04:24,480 --> 00:04:29,740
 In the end they all meet or they all come together and are

80
00:04:29,740 --> 00:04:32,440
 for the purpose of this last

81
00:04:32,440 --> 00:04:38,570
 one which is mental cultivation or mental development, bhag

82
00:04:38,570 --> 00:04:39,480
wana.

83
00:04:39,480 --> 00:04:43,240
 Now bhagwana in classic Buddhist thought is of two kinds.

84
00:04:43,240 --> 00:04:47,080
 So there's samatha bhagwana and vipassana bhagwana.

85
00:04:47,080 --> 00:04:50,850
 So when it comes to ordinary practice for the purpose of

86
00:04:50,850 --> 00:04:52,880
 doing away with defilements,

87
00:04:52,880 --> 00:04:56,400
 it's really not necessary to divide meditation practice or

88
00:04:56,400 --> 00:04:58,720
 mental development into two types.

89
00:04:58,720 --> 00:05:03,870
 It's only a technical classification and it's very useful

90
00:05:03,870 --> 00:05:06,560
 for pointing out those types of

91
00:05:06,560 --> 00:05:10,870
 meditation which will indeed not lead to become freedom

92
00:05:10,870 --> 00:05:12,480
 from defilement.

93
00:05:12,480 --> 00:05:16,740
 Those states of meditation which are for the purpose of

94
00:05:16,740 --> 00:05:19,680
 simply dwelling in bliss or dwelling

95
00:05:19,680 --> 00:05:24,510
 in peace, in a state of calm and equanimity which has

96
00:05:24,510 --> 00:05:27,560
 nothing to do with an understanding

97
00:05:27,560 --> 00:05:32,800
 about ourselves and our minds.

98
00:05:32,800 --> 00:05:37,280
 For instance, practicing meditation based on a concept.

99
00:05:37,280 --> 00:05:42,400
 When we meditate based on a word, a mantra, a mantra in

100
00:05:42,400 --> 00:05:45,280
 this case which has nothing to

101
00:05:45,280 --> 00:05:47,360
 do with the present moment.

102
00:05:47,360 --> 00:05:50,230
 Maybe on the past or the future or maybe some conceptual

103
00:05:50,230 --> 00:05:51,880
 reality which we create in our

104
00:05:51,880 --> 00:05:55,570
 minds or focusing on the light which doesn't exist but

105
00:05:55,570 --> 00:05:57,800
 which we create in our minds.

106
00:05:57,800 --> 00:05:59,840
 So an imaginary concept.

107
00:05:59,840 --> 00:06:03,410
 These kinds of meditations will not lead ultimately to

108
00:06:03,410 --> 00:06:06,880
 freedom from defilement, freedom from suffering.

109
00:06:06,880 --> 00:06:10,520
 And because they will not lead to wisdom and understanding

110
00:06:10,520 --> 00:06:12,680
 and they're not considered mental

111
00:06:12,680 --> 00:06:13,680
 development.

112
00:06:13,680 --> 00:06:16,440
 They don't lead us to understand the reason why we get

113
00:06:16,440 --> 00:06:18,640
 angry, the reason why we get greedy.

114
00:06:18,640 --> 00:06:21,970
 They only have the ability to cover up and replace greed

115
00:06:21,970 --> 00:06:22,840
 and anger.

116
00:06:22,840 --> 00:06:27,350
 They don't have the ability to replace delusion with wisdom

117
00:06:27,350 --> 00:06:27,620
.

118
00:06:27,620 --> 00:06:31,860
 And so they can't be considered mental development or bhagw

119
00:06:31,860 --> 00:06:33,920
ana in the highest sense.

120
00:06:33,920 --> 00:06:37,220
 They could be considered samatha bhagwana in the sense that

121
00:06:37,220 --> 00:06:38,960
 they only lead to states of

122
00:06:38,960 --> 00:06:39,960
 calm.

123
00:06:39,960 --> 00:06:44,080
 Samatha meaning calm or tranquility.

124
00:06:44,080 --> 00:06:46,840
 But in the practice of the Lord Buddha it is composed of

125
00:06:46,840 --> 00:06:48,560
 both samatha and vipassana.

126
00:06:48,560 --> 00:06:53,150
 It means the mind becomes peaceful and calm and also sees

127
00:06:53,150 --> 00:06:55,480
 clearly at the same time.

128
00:06:55,480 --> 00:06:59,820
 So when we practice rising, falling or walking, stepping

129
00:06:59,820 --> 00:07:02,520
 right, stepping left, we can feel

130
00:07:02,520 --> 00:07:06,830
 our minds calming down, becoming more relaxed and more at

131
00:07:06,830 --> 00:07:08,640
 peace as we become better and

132
00:07:08,640 --> 00:07:13,280
 more proficient and if we're really sincere about our

133
00:07:13,280 --> 00:07:15,200
 acknowledgement.

134
00:07:15,200 --> 00:07:17,780
 And at the same time we come to see the truth about the

135
00:07:17,780 --> 00:07:19,560
 things which we hold on to, which

136
00:07:19,560 --> 00:07:25,720
 we grasp after, which we are addicted to and desire for.

137
00:07:25,720 --> 00:07:28,330
 We can see that they are impermanent, we can see that they

138
00:07:28,330 --> 00:07:29,680
 are unsatisfying and we can

139
00:07:29,680 --> 00:07:33,520
 see that they are not under our control.

140
00:07:33,520 --> 00:07:36,460
 When we see in this way we come to let go and we come to

141
00:07:36,460 --> 00:07:38,080
 truly be free from the asa

142
00:07:38,080 --> 00:07:39,080
 vah.

143
00:07:39,080 --> 00:07:43,570
 And so, and this is called insight through seeing clearly,

144
00:07:43,570 --> 00:07:44,740
 vipassana.

145
00:07:44,740 --> 00:07:47,910
 So the practice which we follow is both samatha and vipass

146
00:07:47,910 --> 00:07:49,740
ana you can say because it makes

147
00:07:49,740 --> 00:07:54,580
 the mind at peace and at rest but ultimately through

148
00:07:54,580 --> 00:07:58,240
 insight, not through repressing the

149
00:07:58,240 --> 00:08:01,740
 defilements or avoiding bad mind states but rather

150
00:08:01,740 --> 00:08:04,000
 examining them and coming to see the

151
00:08:04,000 --> 00:08:08,000
 cause of suffering and those states which lead to suffering

152
00:08:08,000 --> 00:08:09,800
, those states which lead

153
00:08:09,800 --> 00:08:13,940
 to peace and happiness and coming to see the difference and

154
00:08:13,940 --> 00:08:16,160
 see the truth about the things

155
00:08:16,160 --> 00:08:21,040
 which we grasp after.

156
00:08:21,040 --> 00:08:24,620
 And according to the Lord Buddha, this type of bhagwa now

157
00:08:24,620 --> 00:08:26,860
 or mental development is composed

158
00:08:26,860 --> 00:08:32,190
 of seven factors and these seven factors are the bhagwa na

159
00:08:32,190 --> 00:08:35,040
 bahata bah, the things which

160
00:08:35,040 --> 00:08:41,210
 lead one to destroy or do away with or clean up, clean out

161
00:08:41,210 --> 00:08:44,080
 the asa vah and these do it

162
00:08:44,080 --> 00:08:47,040
 in the highest sense.

163
00:08:47,040 --> 00:08:50,080
 And these all together, they start with mindfulness.

164
00:08:50,080 --> 00:08:54,280
 The first part of mental development is mindfulness of

165
00:08:54,280 --> 00:08:55,160
 course.

166
00:08:55,160 --> 00:09:00,480
 It's called sati sambo chong, sati sambo chanka.

167
00:09:00,480 --> 00:09:04,160
 Bo cha means wisdom or enlightenment.

168
00:09:04,160 --> 00:09:08,720
 Ang ka means a member or a factor.

169
00:09:08,720 --> 00:09:12,860
 So this is the factor of enlightenment or the factor of

170
00:09:12,860 --> 00:09:15,320
 seeing clearly or awakening,

171
00:09:15,320 --> 00:09:19,480
 waking up to the truth which is called mindfulness.

172
00:09:19,480 --> 00:09:22,220
 And this is very clear I think to meditators even when they

173
00:09:22,220 --> 00:09:24,160
 first come to practice, mindfulness

174
00:09:24,160 --> 00:09:28,410
 is something which really does wake you up to the state of

175
00:09:28,410 --> 00:09:30,600
 mind which you live with.

176
00:09:30,600 --> 00:09:34,730
 Many times we don't see how distracted, how corrupted our

177
00:09:34,730 --> 00:09:36,160
 mind has become.

178
00:09:36,160 --> 00:09:41,030
 Our mind has become like a wild animal through lack of

179
00:09:41,030 --> 00:09:44,560
 training and it's going to take quite

180
00:09:44,560 --> 00:09:45,640
 a bit of work to change.

181
00:09:45,640 --> 00:09:47,520
 This is waking up.

182
00:09:47,520 --> 00:09:50,040
 This is the first part of mental development.

183
00:09:50,040 --> 00:09:52,940
 So it's of course based on the four foundations of

184
00:09:52,940 --> 00:09:54,000
 mindfulness.

185
00:09:54,000 --> 00:09:57,670
 When we're mindful of the body, stepping right, stepping

186
00:09:57,670 --> 00:09:59,840
 left or sitting rising, falling,

187
00:09:59,840 --> 00:10:02,080
 this is mindfulness of the body.

188
00:10:02,080 --> 00:10:07,720
 This is the first part of sati, mindfulness.

189
00:10:07,720 --> 00:10:11,010
 Creating the clear thought based on the body as the Lord

190
00:10:11,010 --> 00:10:13,240
 Buddha said, "Gachantu wa gachamiti

191
00:10:13,240 --> 00:10:14,640
 bachanati."

192
00:10:14,640 --> 00:10:18,680
 When walking they know I'm walking or walking.

193
00:10:18,680 --> 00:10:21,840
 In English it's harder to say I am walking, it's longer

194
00:10:21,840 --> 00:10:23,000
 than in the body.

195
00:10:23,000 --> 00:10:26,480
 So we simply say walking, walking, walking or we break it

196
00:10:26,480 --> 00:10:27,640
 up into pieces.

197
00:10:27,640 --> 00:10:31,560
 Stepping right, stepping left or lifting, placing, lifting,

198
00:10:31,560 --> 00:10:33,480
 placing or lifting, moving,

199
00:10:33,480 --> 00:10:39,760
 placing, lifting here, lifting, moving, placing and so on.

200
00:10:39,760 --> 00:10:41,940
 But the most important thing is creating the clear thought

201
00:10:41,940 --> 00:10:42,960
 in the present moment.

202
00:10:42,960 --> 00:10:43,960
 This is mindfulness.

203
00:10:43,960 --> 00:10:47,770
 The clear thought which replaces the distracted or the del

204
00:10:47,770 --> 00:10:50,120
uded or the judgmental thought.

205
00:10:50,120 --> 00:10:56,680
 It's instead a clear thought based on the present reality.

206
00:10:56,680 --> 00:10:57,840
 Body and then we have feelings.

207
00:10:57,840 --> 00:11:00,400
 It's the same when we have painful feelings or calm

208
00:11:00,400 --> 00:11:02,040
 feelings or happy feelings.

209
00:11:02,040 --> 00:11:05,520
 We simply say pain, pain, creating a clear thought,

210
00:11:05,520 --> 00:11:07,880
 reminding ourselves that it's only

211
00:11:07,880 --> 00:11:08,880
 pain.

212
00:11:08,880 --> 00:11:12,960
 We feel happy reminding ourselves that it's only happiness.

213
00:11:12,960 --> 00:11:15,460
 We feel happy reminding ourselves that it's only calm and

214
00:11:15,460 --> 00:11:16,600
 not holding on to any of these

215
00:11:16,600 --> 00:11:18,400
 states of mind.

216
00:11:18,400 --> 00:11:21,930
 When we have thoughts, this is mindfulness of the mind, we

217
00:11:21,930 --> 00:11:23,680
're thinking about the past

218
00:11:23,680 --> 00:11:26,850
 or future, thinking good thoughts, thinking bad thoughts,

219
00:11:26,850 --> 00:11:28,840
 we simply say to ourselves,

220
00:11:28,840 --> 00:11:33,420
 "Thinking, thinking, thinking," not judging, replacing the

221
00:11:33,420 --> 00:11:35,000
 thinking with a new kind of

222
00:11:35,000 --> 00:11:36,880
 thinking which is clearly aware.

223
00:11:36,880 --> 00:11:40,400
 Like the mind, it's called the mind knows the mind.

224
00:11:40,400 --> 00:11:47,790
 "Chitta-ejita-anukasi," the mind in the mind, seeing the

225
00:11:47,790 --> 00:11:48,120
 mind in the mind, the seeing a

226
00:11:48,120 --> 00:11:52,380
 part of the mind which is present, in this case the

227
00:11:52,380 --> 00:11:53,560
 thoughts.

228
00:11:53,560 --> 00:11:57,840
 And then mindfulness of the mind objects or the dhamma, the

229
00:11:57,840 --> 00:11:59,200
 states of mind.

230
00:11:59,200 --> 00:12:02,360
 So in this case the five hindrances, liking, disliking, d

231
00:12:02,360 --> 00:12:05,360
rowsiness, distraction, doubt,

232
00:12:05,360 --> 00:12:08,480
 these five things are very important for meditators.

233
00:12:08,480 --> 00:12:13,960
 They are a cause for the meditation to go bad, to go wrong.

234
00:12:13,960 --> 00:12:17,940
 If these five things exist in the mind and are not removed,

235
00:12:17,940 --> 00:12:19,760
 are not rooted out, they

236
00:12:19,760 --> 00:12:23,250
 will ever and again cause stress and suffering and a

237
00:12:23,250 --> 00:12:25,360
 general inability to continue in the

238
00:12:25,360 --> 00:12:26,360
 practice.

239
00:12:26,360 --> 00:12:29,990
 So it's important that we are very vigilant in doing away

240
00:12:29,990 --> 00:12:32,040
 with these when they arise.

241
00:12:32,040 --> 00:12:34,970
 Liking, when we like something we say to ourselves, "Liking

242
00:12:34,970 --> 00:12:37,360
, liking," when we don't like something,

243
00:12:37,360 --> 00:12:42,360
 "Liking, disliking," we feel drowsy, we say drowsy, drowsy.

244
00:12:42,360 --> 00:12:46,000
 Distracted, we say distracted, distracted.

245
00:12:46,000 --> 00:12:48,800
 And we doubt, we say doubting, doubting, doubting.

246
00:12:48,800 --> 00:12:53,070
 And this is the five hindrances, this is under the heading

247
00:12:53,070 --> 00:12:55,200
 of dhamma or mind states.

248
00:12:55,200 --> 00:12:58,210
 Now two more things that we have to keep in mind in terms

249
00:12:58,210 --> 00:13:00,040
 of mindfulness, these are the

250
00:13:00,040 --> 00:13:03,010
 things which I've explained during the time when we're not

251
00:13:03,010 --> 00:13:04,480
 practicing, when we're not

252
00:13:04,480 --> 00:13:07,360
 doing walking or when we're not doing sitting.

253
00:13:07,360 --> 00:13:09,540
 When we're not doing walking or sitting we have to be aware

254
00:13:09,540 --> 00:13:10,960
 of the four pictures, standing,

255
00:13:10,960 --> 00:13:15,040
 walking, sitting, and lying down, standing, standing.

256
00:13:15,040 --> 00:13:18,890
 When we walk, walking, walking, walking, we sit, sitting,

257
00:13:18,890 --> 00:13:19,600
 sitting.

258
00:13:19,600 --> 00:13:23,480
 When we lie down, lying, lying, lying, and so on.

259
00:13:23,480 --> 00:13:26,560
 And then the six senses, seeing, hearing, smelling, tasting

260
00:13:26,560 --> 00:13:28,040
, feeling, thinking, just

261
00:13:28,040 --> 00:13:31,180
 saying to ourselves, seeing, seeing, hearing, hearing, and

262
00:13:31,180 --> 00:13:31,640
 so on.

263
00:13:31,640 --> 00:13:37,240
 And we see here smell, taste, feel, or think anything.

264
00:13:37,240 --> 00:13:42,130
 Acknowledging in this way is the, it creates a state of

265
00:13:42,130 --> 00:13:43,920
 developed mind.

266
00:13:43,920 --> 00:13:46,340
 It's this way of cultivating the other factors of

267
00:13:46,340 --> 00:13:47,240
 enlightenment.

268
00:13:47,240 --> 00:13:49,670
 When we start off with mindfulness, all of the other

269
00:13:49,670 --> 00:13:51,360
 factors of enlightenment arise in

270
00:13:51,360 --> 00:13:52,360
 order.

271
00:13:52,360 --> 00:13:55,120
 So this is the first one.

272
00:13:55,120 --> 00:13:58,520
 And it is also one which we have to keep coming back to.

273
00:13:58,520 --> 00:14:00,880
 We can't say, "Once we have mindfulness then we can go on

274
00:14:00,880 --> 00:14:02,240
 to the next one and forget about

275
00:14:02,240 --> 00:14:04,000
 mindfulness."

276
00:14:04,000 --> 00:14:07,140
 Until we have, for as long as we have mindfulness, the

277
00:14:07,140 --> 00:14:09,240
 other factors will continue to arise.

278
00:14:09,240 --> 00:14:13,750
 But when we give up mindfulness, the other factors will not

279
00:14:13,750 --> 00:14:15,440
 be able to send us on to

280
00:14:15,440 --> 00:14:17,720
 become awake, to wake up.

281
00:14:17,720 --> 00:14:21,140
 It has to come from mindfulness to create the other six

282
00:14:21,140 --> 00:14:21,960
 factors.

283
00:14:21,960 --> 00:14:26,140
 At all times mindfulness is, Lord Buddha said, "Satiñca k

284
00:14:26,140 --> 00:14:28,480
oa han kuwe sabhatikang matamih."

285
00:14:28,480 --> 00:14:35,860
 Mindfulness, O monks, is always useful, is ever of use, is

286
00:14:35,860 --> 00:14:38,680
 always beneficial.

287
00:14:38,680 --> 00:14:43,640
 Something which we have to always continue developing until

288
00:14:43,640 --> 00:14:45,760
 the moment when we become

289
00:14:45,760 --> 00:14:49,560
 free from suffering, until the moment when we wake up and

290
00:14:49,560 --> 00:14:51,440
 realize the truth and let go

291
00:14:51,440 --> 00:14:52,680
 and become free.

292
00:14:52,680 --> 00:14:54,360
 This is the first one.

293
00:14:54,360 --> 00:14:58,690
 The second one, once we have mindfulness, then there will

294
00:14:58,690 --> 00:15:01,160
 arise a certain understanding.

295
00:15:01,160 --> 00:15:03,160
 It's called "dhammavicaya."

296
00:15:03,160 --> 00:15:08,760
 And this is, it's called considering or discriminating

297
00:15:08,760 --> 00:15:11,160
 between mind states.

298
00:15:11,160 --> 00:15:15,950
 There will arise a discrimination which is non-judgmental,

299
00:15:15,950 --> 00:15:18,220
 which simply sees things for

300
00:15:18,220 --> 00:15:19,920
 what they are.

301
00:15:19,920 --> 00:15:22,800
 It sees those things which are impermanent as impermanent.

302
00:15:22,800 --> 00:15:25,700
 It sees those things which are unsatisfying as unsatisfying

303
00:15:25,700 --> 00:15:25,920
.

304
00:15:25,920 --> 00:15:28,680
 It sees those things which are not under our control as not

305
00:15:28,680 --> 00:15:29,800
 under our control.

306
00:15:29,800 --> 00:15:33,100
 And it will start to see more and more clearly that

307
00:15:33,100 --> 00:15:35,240
 actually there's no difference and any

308
00:15:35,240 --> 00:15:38,960
 kind of judgment is simply a delusion in the mind.

309
00:15:38,960 --> 00:15:41,210
 That everything inside of us and in the world around us has

310
00:15:41,210 --> 00:15:42,480
 the same characteristics.

311
00:15:42,480 --> 00:15:47,620
 And this is impermanence, suffering, impermanence, unsatisf

312
00:15:47,620 --> 00:15:51,240
ying and not under our control, impermanent

313
00:15:51,240 --> 00:15:53,960
 suffering and non-self.

314
00:15:53,960 --> 00:15:56,520
 These three characteristics are called the "samanyalakana."

315
00:15:56,520 --> 00:15:58,400
 They're ubiquitous.

316
00:15:58,400 --> 00:16:02,870
 They exist in everything around us and inside of us and in

317
00:16:02,870 --> 00:16:04,760
 the world around us.

318
00:16:04,760 --> 00:16:07,000
 When we see in this way, this is "dhammavicaya."

319
00:16:07,000 --> 00:16:10,400
 This is the process of letting go.

320
00:16:10,400 --> 00:16:16,500
 Once we have "dhammavicaya," we will start to develop this

321
00:16:16,500 --> 00:16:19,240
 very set state of effort or

322
00:16:19,240 --> 00:16:20,240
 exertion.

323
00:16:20,240 --> 00:16:24,250
 "Virya sampu-chanka," the factor of enlightenment which is

324
00:16:24,250 --> 00:16:25,080
 effort.

325
00:16:25,080 --> 00:16:27,080
 In the beginning it's very difficult to put out effort.

326
00:16:27,080 --> 00:16:29,120
 We put out effort often in the wrong way.

327
00:16:29,120 --> 00:16:32,450
 We find ourselves walking and sitting but not really being

328
00:16:32,450 --> 00:16:33,240
 mindful.

329
00:16:33,240 --> 00:16:36,100
 But once we start to see impermanent suffering and non-self

330
00:16:36,100 --> 00:16:37,800
, we don't hold on to anything.

331
00:16:37,800 --> 00:16:39,080
 When it comes up, we let it go.

332
00:16:39,080 --> 00:16:40,200
 We acknowledge, let it go.

333
00:16:40,200 --> 00:16:43,270
 We say, "We use mindfulness and continue to let it go, let

334
00:16:43,270 --> 00:16:44,800
 it go, let it go," not holding

335
00:16:44,800 --> 00:16:46,640
 on to anything.

336
00:16:46,640 --> 00:16:49,460
 Then we'll start to develop like a train when it first

337
00:16:49,460 --> 00:16:51,440
 starts up, it's very slow and slowly

338
00:16:51,440 --> 00:16:53,760
 gains momentum.

339
00:16:53,760 --> 00:16:56,240
 As we start to practice, we'll find ourselves gaining

340
00:16:56,240 --> 00:16:57,000
 momentum.

341
00:16:57,000 --> 00:16:59,120
 This is "Virya."

342
00:16:59,120 --> 00:17:01,120
 And then "piti," it will arise.

343
00:17:01,120 --> 00:17:02,120
 "Piti sampu-chanka."

344
00:17:02,120 --> 00:17:04,920
 "Piti" means rapture.

345
00:17:04,920 --> 00:17:07,660
 It means our minds will, like the locomotive, once it's

346
00:17:07,660 --> 00:17:09,440
 going, it starts going and going

347
00:17:09,440 --> 00:17:12,680
 and it starts to go of its own power.

348
00:17:12,680 --> 00:17:16,620
 "Piti" means the mind starts to work on its own, starts to

349
00:17:16,620 --> 00:17:18,200
 fall into the track, fall

350
00:17:18,200 --> 00:17:22,130
 into the groove, finding mindfulness a very natural and a

351
00:17:22,130 --> 00:17:24,600
 very peaceful and a very harmonious

352
00:17:24,600 --> 00:17:26,720
 way to live.

353
00:17:26,720 --> 00:17:30,760
 Simply replacing, creating a clear thought instead of all

354
00:17:30,760 --> 00:17:33,080
 of the unclear, deluded, fuzzy

355
00:17:33,080 --> 00:17:35,560
 thoughts which continue on ever.

356
00:17:35,560 --> 00:17:38,380
 And again, we replace this with clear thought.

357
00:17:38,380 --> 00:17:40,780
 Simply knowing, simply being aware of everything we do

358
00:17:40,780 --> 00:17:42,520
 according to the Satipatthana sutta.

359
00:17:42,520 --> 00:17:47,320
 And the discourse on mindfulness.

360
00:17:47,320 --> 00:17:50,150
 This is "piti," our mind will become, at this time many med

361
00:17:50,150 --> 00:17:52,280
itators will have such a charged

362
00:17:52,280 --> 00:17:56,160
 state that they might find themselves getting goosebumps,

363
00:17:56,160 --> 00:17:58,160
 or they might find themselves

364
00:17:58,160 --> 00:18:00,480
 crying for no apparent reason.

365
00:18:00,480 --> 00:18:04,360
 They might find themselves smiling, they might find

366
00:18:04,360 --> 00:18:06,880
 themselves feeling very light, find themselves

367
00:18:06,880 --> 00:18:09,520
 rocking back and forth, and so on.

368
00:18:09,520 --> 00:18:11,940
 All of these things we have to be careful that we don't get

369
00:18:11,940 --> 00:18:12,720
 stuck on as well.

370
00:18:12,720 --> 00:18:16,730
 They are very good things, they are a sign of good progress

371
00:18:16,730 --> 00:18:17,040
.

372
00:18:17,040 --> 00:18:19,030
 It's a factor of enlightenment, but we have to be careful

373
00:18:19,030 --> 00:18:20,400
 that we don't get stuck on them.

374
00:18:20,400 --> 00:18:23,760
 When we cry, we have to say crying, crying.

375
00:18:23,760 --> 00:18:26,040
 When we feel happy, we have to say happy, happy.

376
00:18:26,040 --> 00:18:29,040
 If we feel light, we have to say light, light.

377
00:18:29,040 --> 00:18:35,480
 We see anything, we have to say seeing, seeing, so on.

378
00:18:35,480 --> 00:18:39,600
 Once we have rapture, then there will arise basati.

379
00:18:39,600 --> 00:18:41,600
 Basati is tranquility.

380
00:18:41,600 --> 00:18:45,440
 Then the mind will really start to calm down.

381
00:18:45,440 --> 00:18:48,180
 All this time the mind is still very active, and the medit

382
00:18:48,180 --> 00:18:49,760
ators sometimes feel like they

383
00:18:49,760 --> 00:18:54,750
 are hopeless, like they are not able to carry out the job

384
00:18:54,750 --> 00:18:57,520
 which they have undertaken.

385
00:18:57,520 --> 00:19:02,360
 They feel like they are not able to get the results which

386
00:19:02,360 --> 00:19:04,520
 they had hoped for.

387
00:19:04,520 --> 00:19:23,230
 This is because in our daily lives we had no meditation

388
00:19:23,230 --> 00:19:23,960
 practice.

389
00:19:23,960 --> 00:19:27,400
 We had not developed our minds in any way.

390
00:19:27,400 --> 00:19:30,930
 Only when we do come to practice then we begin to train our

391
00:19:30,930 --> 00:19:31,600
 minds.

392
00:19:31,600 --> 00:19:35,340
 We have to deal with so many years of a mind which is unt

393
00:19:35,340 --> 00:19:37,080
rained, which we just let go in

394
00:19:37,080 --> 00:19:39,400
 any way it wanted.

395
00:19:39,400 --> 00:19:43,550
 We see now the disadvantages or the fault in such a way of

396
00:19:43,550 --> 00:19:45,840
 existence, just letting our

397
00:19:45,840 --> 00:19:51,690
 minds wander and develop bad habits and encouraging bad

398
00:19:51,690 --> 00:19:52,880
 habits.

399
00:19:52,880 --> 00:19:55,200
 Now when we come to train ourselves, yes, in the beginning

400
00:19:55,200 --> 00:19:56,320
 it is very difficult, but

401
00:19:56,320 --> 00:19:59,680
 this is not a reason for meditators to become discouraged.

402
00:19:59,680 --> 00:20:04,570
 It is only a reason to understand that there is indeed a

403
00:20:04,570 --> 00:20:07,480
 correct and an incorrect way to

404
00:20:07,480 --> 00:20:13,760
 keep the mind, to use the mind.

405
00:20:13,760 --> 00:20:15,960
 And when we come to practice we can see what is the correct

406
00:20:15,960 --> 00:20:17,040
 way and the correct way is

407
00:20:17,040 --> 00:20:19,480
 to have clear thought at all times.

408
00:20:19,480 --> 00:20:22,110
 Simply be aware of whatever comes up, whether it is a good

409
00:20:22,110 --> 00:20:23,220
 thing or a bad thing.

410
00:20:23,220 --> 00:20:25,400
 Not be judging, not be judgmental about it.

411
00:20:25,400 --> 00:20:28,880
 Even our judgments, not to be judgmental about them.

412
00:20:28,880 --> 00:20:32,070
 To see that these are only an old way of thinking, a way

413
00:20:32,070 --> 00:20:34,480
 which is not conducive to happiness

414
00:20:34,480 --> 00:20:39,400
 but can only lead to suffering if we let it, and learning

415
00:20:39,400 --> 00:20:40,960
 to let go of it.

416
00:20:40,960 --> 00:20:43,120
 Then the mind will calm down.

417
00:20:43,120 --> 00:20:46,300
 Once we practice on and practice on, eventually we won't

418
00:20:46,300 --> 00:20:48,200
 have to put out any effort, our mind

419
00:20:48,200 --> 00:20:56,680
 will calm down of its own volition.

420
00:20:56,680 --> 00:20:59,750
 Once the mind calms down, the passati arises, then there

421
00:20:59,750 --> 00:21:02,200
 arises samati, which is concentration.

422
00:21:02,200 --> 00:21:06,680
 The mind will be fixed very strongly, like playing a piano.

423
00:21:06,680 --> 00:21:10,480
 The hands will be very sure.

424
00:21:10,480 --> 00:21:13,920
 Once we do away with all of our old habits using our hands

425
00:21:13,920 --> 00:21:15,560
 and we can learn to play the

426
00:21:15,560 --> 00:21:19,620
 piano, our fingers will be very sure on the keys and we

427
00:21:19,620 --> 00:21:21,760
 will find we are able to play

428
00:21:21,760 --> 00:21:23,800
 the notes very accurately.

429
00:21:23,800 --> 00:21:27,720
 In meditation, it's the same for when we practice.

430
00:21:27,720 --> 00:21:30,380
 When we say rising, our mind will be very clearly aware of

431
00:21:30,380 --> 00:21:31,120
 the rising.

432
00:21:31,120 --> 00:21:35,310
 We say falling, the mind will be very clearly aware of the

433
00:21:35,310 --> 00:21:35,960
 falling.

434
00:21:35,960 --> 00:21:39,090
 Rising, falling, rising, falling, and as the same with

435
00:21:39,090 --> 00:21:40,040
 everything.

436
00:21:40,040 --> 00:21:42,080
 This is what we have to develop.

437
00:21:42,080 --> 00:21:44,910
 All of these things we have to develop them, these are

438
00:21:44,910 --> 00:21:46,280
 mental development.

439
00:21:46,280 --> 00:21:49,220
 When we develop them, starting with mindfulness up to, all

440
00:21:49,220 --> 00:21:50,800
 the way up to concentration, there

441
00:21:50,800 --> 00:21:54,240
 will arise the seventh one, which is equanimity.

442
00:21:54,240 --> 00:21:58,120
 This is an equanimity which is at complete peace with the

443
00:21:58,120 --> 00:21:59,440
 world around it.

444
00:21:59,440 --> 00:22:03,120
 It sees everything as simply for what it is.

445
00:22:03,120 --> 00:22:07,100
 Just like a tree lives in nature without judging, without

446
00:22:07,100 --> 00:22:08,960
 getting upset, or just as a rock or

447
00:22:08,960 --> 00:22:14,320
 a mountain, just as a very true part of nature.

448
00:22:14,320 --> 00:22:18,050
 It will become a very strange sort of being, a being which

449
00:22:18,050 --> 00:22:20,040
 is very difficult to find.

450
00:22:20,040 --> 00:22:24,670
 One that doesn't fight, that doesn't get in conflict, that

451
00:22:24,670 --> 00:22:27,040
 doesn't cry, that doesn't get

452
00:22:27,040 --> 00:22:31,240
 sad, doesn't grieve, doesn't get upset.

453
00:22:31,240 --> 00:22:37,380
 It doesn't feel any sense of sorrow or loss at anything

454
00:22:37,380 --> 00:22:39,240
 whatsoever.

455
00:22:39,240 --> 00:22:43,890
 A person who is completely at peace with the world around

456
00:22:43,890 --> 00:22:44,640
 them.

457
00:22:44,640 --> 00:22:47,290
 Because they have found the truth, they have seen the truth

458
00:22:47,290 --> 00:22:48,600
, and their mind has become

459
00:22:48,600 --> 00:22:52,600
 set in the truth through the other six factors.

460
00:22:52,600 --> 00:22:54,920
 So altogether these are the seven factors of enlightenment.

461
00:22:54,920 --> 00:22:58,210
 And when they come together in this way, the equanimity

462
00:22:58,210 --> 00:23:00,400
 which arises will send one into

463
00:23:00,400 --> 00:23:01,800
 freedom.

464
00:23:01,800 --> 00:23:04,930
 Just like an airplane, when the airplane goes and goes,

465
00:23:04,930 --> 00:23:06,720
 just like it goes through seven

466
00:23:06,720 --> 00:23:07,720
 stages.

467
00:23:07,720 --> 00:23:11,970
 And at the seventh stage, the airplane is fast enough, it

468
00:23:11,970 --> 00:23:12,840
 can fly.

469
00:23:12,840 --> 00:23:15,970
 The moment when we have equanimity, the equanimity will be

470
00:23:15,970 --> 00:23:17,760
 the last stage to allow us to fly

471
00:23:17,760 --> 00:23:23,750
 and be free from any sort of attachment or any sort of bond

472
00:23:23,750 --> 00:23:24,240
.

473
00:23:24,240 --> 00:23:28,380
 Just like a bird or an airplane flying in the sky without

474
00:23:28,380 --> 00:23:31,400
 any friction or any bond whatsoever.

475
00:23:31,400 --> 00:23:33,550
 These seven altogether are the things which we have to

476
00:23:33,550 --> 00:23:34,120
 develop.

477
00:23:34,120 --> 00:23:35,360
 Most importantly, mindfulness.

478
00:23:35,360 --> 00:23:37,560
 It's not important to remember everything.

479
00:23:37,560 --> 00:23:40,110
 If you get the sort of gist of it, you can see where

480
00:23:40,110 --> 00:23:42,280
 mindfulness is leading and eventually

481
00:23:42,280 --> 00:23:45,080
 will become a complete peace with everything.

482
00:23:45,080 --> 00:23:48,070
 And we can see this if we're careful and if we're honest

483
00:23:48,070 --> 00:23:49,160
 with ourselves.

484
00:23:49,160 --> 00:23:53,250
 We can see that the correct practice does lead indeed to

485
00:23:53,250 --> 00:23:55,160
 true peace and harmony.

486
00:23:55,160 --> 00:23:59,390
 It's only up to us to continue practicing in the correct

487
00:23:59,390 --> 00:24:00,320
 manner.

488
00:24:00,320 --> 00:24:03,360
 And that's all now for the talk on the Sabha Sava.

489
00:24:03,360 --> 00:24:07,220
 If you're interested in learning more about it, it's in the

490
00:24:07,220 --> 00:24:09,160
 second discourse of the Mijim

491
00:24:09,160 --> 00:24:15,680
 and the Kaya in the middle-length discourses. Otherwise we

492
00:24:15,680 --> 00:24:16,160
 just continue practicing.

493
00:24:16,160 --> 00:24:20,970
 I sincerely wish for everyone listening now that they are

494
00:24:20,970 --> 00:24:23,760
 able to find freedom from the

495
00:24:23,760 --> 00:24:26,500
 things which make the mind go bad, which cause the mind to

496
00:24:26,500 --> 00:24:27,080
 go sour.

497
00:24:27,080 --> 00:24:31,160
 They are able to do away with mental defilements and come

498
00:24:31,160 --> 00:24:33,480
 to true peace and true freedom on

499
00:24:33,480 --> 00:24:34,480
 their own time.

500
00:24:34,480 --> 00:24:35,480
 And that's all for today.

501
00:24:35,480 --> 00:24:39,050
 Now we'll continue with meditation practice at your own

502
00:24:39,050 --> 00:24:39,640
 pace.

503
00:24:39,640 --> 00:24:46,640
 [

