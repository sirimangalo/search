1
00:00:00,000 --> 00:00:14,720
 Okay, good evening everyone.

2
00:00:14,720 --> 00:00:17,400
 Welcome to our evening broadcast.

3
00:00:17,400 --> 00:00:25,400
 Evening Dhamma talk.

4
00:00:25,400 --> 00:00:28,160
 Tonight's topic is a challenging one.

5
00:00:28,160 --> 00:00:29,160
 It's not going to be long.

6
00:00:29,160 --> 00:00:35,400
 There's not too much to say but...

7
00:00:35,400 --> 00:00:37,800
 Sometimes we sugarcoat the truth.

8
00:00:37,800 --> 00:00:40,760
 I think I'm guilty of that.

9
00:00:40,760 --> 00:00:45,960
 Making Buddhism sound more pleasant or more accommodating

10
00:00:45,960 --> 00:00:49,720
 than it actually is.

11
00:00:49,720 --> 00:00:57,850
 And the truth is, the Buddha really saw our situation as

12
00:00:57,850 --> 00:00:58,600
 human beings.

13
00:00:58,600 --> 00:01:03,440
 It was quite wretched.

14
00:01:03,440 --> 00:01:05,360
 There's no escaping that.

15
00:01:05,360 --> 00:01:09,210
 So when we talk about Buddhism as being pessimistic, we can

16
00:01:09,210 --> 00:01:10,480
 deny it all we want.

17
00:01:10,480 --> 00:01:14,280
 But to some extent, it's not pessimistic.

18
00:01:14,280 --> 00:01:15,840
 I've talked about that.

19
00:01:15,840 --> 00:01:18,120
 That word has too much associated with it.

20
00:01:18,120 --> 00:01:21,120
 But it definitely doesn't look favorable upon it.

21
00:01:21,120 --> 00:01:25,300
 It makes a claim that you can argue you could say it's

22
00:01:25,300 --> 00:01:27,520
 pessimistic because you might disagree

23
00:01:27,520 --> 00:01:31,160
 with it.

24
00:01:31,160 --> 00:01:40,050
 But the argument is that our existence as human beings is

25
00:01:40,050 --> 00:01:43,120
 quite wretched.

26
00:01:43,120 --> 00:01:49,300
 And it has very much to do with the fact that we're

27
00:01:49,300 --> 00:01:51,200
 dependent.

28
00:01:51,200 --> 00:01:57,470
 We are as slaves, as sick people, in ways that we don't

29
00:01:57,470 --> 00:01:58,920
 realize.

30
00:01:58,920 --> 00:02:04,720
 We think that we're happy sometimes.

31
00:02:04,720 --> 00:02:06,720
 We don't really understand what happiness is.

32
00:02:06,720 --> 00:02:09,880
 We think we do.

33
00:02:09,880 --> 00:02:16,770
 But when we think of as happiness, it turns out to be a

34
00:02:16,770 --> 00:02:20,800
 cause for great suffering.

35
00:02:20,800 --> 00:02:30,300
 It turns out to be an inherent problem with our way of

36
00:02:30,300 --> 00:02:34,480
 looking at the world, based on

37
00:02:34,480 --> 00:02:40,220
 the fact that we are very much dependent, and therefore at

38
00:02:40,220 --> 00:02:43,280
 the mercy of the vicissitudes

39
00:02:43,280 --> 00:02:47,040
 of life.

40
00:02:47,040 --> 00:02:51,520
 So this particular suta, it's in the sanyuta nikaya, nidana

41
00:02:51,520 --> 00:02:54,000
 sanyuta, which is a wonderful

42
00:02:54,000 --> 00:02:58,200
 sanyuta.

43
00:02:58,200 --> 00:03:00,560
 Sanyuta just means collection.

44
00:03:00,560 --> 00:03:04,170
 So it's this collection of discourses based around paticca

45
00:03:04,170 --> 00:03:05,120
 samuppada.

46
00:03:05,120 --> 00:03:08,320
 So if you want to learn about dependent origination, this

47
00:03:08,320 --> 00:03:10,560
 is the chapter to read, the collection

48
00:03:10,560 --> 00:03:12,120
 of suttas to read.

49
00:03:12,120 --> 00:03:15,680
 Sanyuta nikaya is really wonderful, it's just very, very

50
00:03:15,680 --> 00:03:18,040
 big, so it's not an easy book to

51
00:03:18,040 --> 00:03:19,040
 really go through.

52
00:03:19,040 --> 00:03:23,960
 That doesn't mean you shouldn't.

53
00:03:23,960 --> 00:03:28,200
 But this is number 63, what's it called?

54
00:03:28,200 --> 00:03:30,200
 Puta mang supama suta.

55
00:03:30,200 --> 00:03:34,160
 Upama is a simile.

56
00:03:34,160 --> 00:03:39,880
 Mangsa means flesh, and puta means son or child.

57
00:03:39,880 --> 00:03:45,040
 So the simile of the flesh of the child.

58
00:03:45,040 --> 00:03:47,200
 Actually the suta is actually about food.

59
00:03:47,200 --> 00:03:52,040
 I remember a few days ago I think, or some days ago, I

60
00:03:52,040 --> 00:03:54,840
 mentioned this as the first of

61
00:03:54,840 --> 00:04:04,960
 the ten dharakapana, kumarapana, the ten questions of the

62
00:04:04,960 --> 00:04:07,520
 young child.

63
00:04:07,520 --> 00:04:13,270
 The first one is sabhayasata aharititika, all beings subs

64
00:04:13,270 --> 00:04:14,760
ist on food.

65
00:04:14,760 --> 00:04:16,320
 And I said there are four types of food.

66
00:04:16,320 --> 00:04:19,200
 Well this suta lays them out.

67
00:04:19,200 --> 00:04:26,490
 The four types of food are kabolinkahara, it's material

68
00:04:26,490 --> 00:04:27,680
 food.

69
00:04:27,680 --> 00:04:37,040
 Pasa hahara, or nutriment, is a better word, the nutriment

70
00:04:37,040 --> 00:04:39,280
 of contact.

71
00:04:39,280 --> 00:04:48,560
 The third is manosanjaitana, the nutriment of mental

72
00:04:48,560 --> 00:04:53,200
 intention or volition.

73
00:04:53,200 --> 00:04:57,150
 And the fourth is vinyanahara, which is the food of

74
00:04:57,150 --> 00:04:59,680
 consciousness, the nourishment of

75
00:04:59,680 --> 00:05:02,440
 consciousness, and nutriment.

76
00:05:02,440 --> 00:05:07,120
 And so what it means by nutriment is again this dependency.

77
00:05:07,120 --> 00:05:08,120
 It's the cause.

78
00:05:08,120 --> 00:05:12,580
 I mean the whole of this collection and really all of the

79
00:05:12,580 --> 00:05:15,480
 Buddhist teaching is about seeing

80
00:05:15,480 --> 00:05:20,620
 cause and effect, about learning how all of who we are and

81
00:05:20,620 --> 00:05:23,680
 what we experience is dependent

82
00:05:23,680 --> 00:05:26,820
 on causality.

83
00:05:26,820 --> 00:05:31,140
 We got the way we are for a reason and we learn about those

84
00:05:31,140 --> 00:05:34,160
 reasons and we realize how

85
00:05:34,160 --> 00:05:38,300
 the situation of dealing with these types of food, they don

86
00:05:38,300 --> 00:05:40,400
't sound bad at all I suppose

87
00:05:40,400 --> 00:05:44,430
 when you think about, when we hear about them because we're

88
00:05:44,430 --> 00:05:46,480
 quite enamored with them.

