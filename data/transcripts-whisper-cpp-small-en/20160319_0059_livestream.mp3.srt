1
00:00:00,000 --> 00:00:07,000
 And we're live.

2
00:00:07,000 --> 00:00:14,000
 Good evening everyone.

3
00:00:14,000 --> 00:00:30,000
 March 18th, 9pm as usual.

4
00:00:30,000 --> 00:00:37,000
 Start by looking at the quote.

5
00:00:37,000 --> 00:00:45,000
 This quote is half of a sutta, it's the second half.

6
00:00:45,000 --> 00:00:55,000
 So it starts actually with five asamaya,

7
00:00:55,000 --> 00:00:59,000
 asamaya padana,

8
00:00:59,000 --> 00:01:13,000
 five non-times for striving.

9
00:01:13,000 --> 00:01:18,000
 So you've got the asamaya and you've got the asamaya.

10
00:01:18,000 --> 00:01:22,000
 There are times for striving and there are times

11
00:01:22,000 --> 00:01:30,000
 where you should not strive.

12
00:01:30,000 --> 00:01:32,440
 And I'm looking at the commentaries, trying to get some

13
00:01:32,440 --> 00:01:33,000
 clarification here

14
00:01:33,000 --> 00:01:43,000
 because I think you have to qualify this to some extent.

15
00:01:43,000 --> 00:01:51,000
 But there's a valuable lesson to be learned here.

16
00:01:51,000 --> 00:01:56,000
 I suppose the first lesson to consider is

17
00:01:56,000 --> 00:02:01,000
 other ways of deciding what's the right time to meditate,

18
00:02:01,000 --> 00:02:09,000
 to strive, are often misguided.

19
00:02:09,000 --> 00:02:13,000
 So we have to think,

20
00:02:13,000 --> 00:02:17,000
 "Well, I'd like to meditate but I just can't find the time.

21
00:02:17,000 --> 00:02:23,000
 I just can't find the right time to do it.

22
00:02:23,000 --> 00:02:31,000
 And I don't have enough time or it's just not the time."

23
00:02:31,000 --> 00:02:34,000
 In Buddhist countries they often say things like,

24
00:02:34,000 --> 00:02:38,000
 "Well, when I get old I'll meditate."

25
00:02:38,000 --> 00:02:42,000
 "When I'm young I've got work to do,

26
00:02:42,000 --> 00:02:51,000
 but when I get old I'll take the time to meditate."

27
00:02:51,000 --> 00:02:55,000
 So we put it aside.

28
00:02:55,000 --> 00:03:00,000
 I think to some extent this sutta, as I said,

29
00:03:00,000 --> 00:03:04,380
 has to be qualified because it would also say that

30
00:03:04,380 --> 00:03:08,000
 mindfulness is always useful.

31
00:03:08,000 --> 00:03:14,360
 That's, I think, important that striving is not the only

32
00:03:14,360 --> 00:03:15,000
 way of meditating,

33
00:03:15,000 --> 00:03:21,000
 not the only way of progressing, of practicing.

34
00:03:21,000 --> 00:03:24,000
 Anyway, let's go through these.

35
00:03:24,000 --> 00:03:28,200
 It's important because there is a certain amount of

36
00:03:28,200 --> 00:03:29,000
 striving

37
00:03:29,000 --> 00:03:31,000
 that you shouldn't do at a certain time.

38
00:03:31,000 --> 00:03:34,000
 So let's start with the asamaya.

39
00:03:34,000 --> 00:03:37,000
 "Katame pancha" - what five?

40
00:03:37,000 --> 00:03:45,200
 "Idda bhikkave bhikkurjinno hoti" - here monks, a monk, a

41
00:03:45,200 --> 00:03:50,000
 meditator, is old.

42
00:03:50,000 --> 00:03:57,000
 "Jara ya bhibhuto" - has become very elderly.

43
00:03:57,000 --> 00:04:01,000
 "Ayam bhikkave patamo asamayo padana ya"

44
00:04:01,000 --> 00:04:03,000
 So how do you interpret this?

45
00:04:03,000 --> 00:04:05,980
 Is he saying that old people shouldn't meditate, shouldn't

46
00:04:05,980 --> 00:04:09,000
 practice, shouldn't strive spiritually?

47
00:04:09,000 --> 00:04:13,860
 No, in fact, I think what is being said here is this is a

48
00:04:13,860 --> 00:04:15,000
 bad time.

49
00:04:15,000 --> 00:04:18,000
 It's harder. It's a harder time.

50
00:04:18,000 --> 00:04:21,710
 And the important lesson there is don't wait till you're

51
00:04:21,710 --> 00:04:22,000
 old

52
00:04:22,000 --> 00:04:28,000
 because old people will tell you there's difficulties.

53
00:04:28,000 --> 00:04:33,000
 Interestingly enough, as I said, it's when people mostly

54
00:04:33,000 --> 00:04:33,000
 there's an excuse,

55
00:04:33,000 --> 00:04:36,000
 "I'm too young, I'll do it when I'm older."

56
00:04:36,000 --> 00:04:39,440
 So interestingly enough, a lot of old people do take up

57
00:04:39,440 --> 00:04:41,000
 meditation and get good results.

58
00:04:41,000 --> 00:04:46,800
 But you accumulate so much baggage that there's a lot more

59
00:04:46,800 --> 00:04:48,000
 work to be done.

60
00:04:48,000 --> 00:04:51,000
 And you have the Buddhist words here and in other places

61
00:04:51,000 --> 00:04:55,000
 that this is not the right way to look at things.

62
00:04:55,000 --> 00:05:00,430
 Because if we skip down to the first one on the positive

63
00:05:00,430 --> 00:05:01,000
 side,

64
00:05:01,000 --> 00:05:05,000
 "Iy dabikvibhikudahrohoti"

65
00:05:05,000 --> 00:05:13,000
 One is "Iy uvah yang susu"

66
00:05:13,000 --> 00:05:15,000
 I don't know what "susu" means.

67
00:05:15,000 --> 00:05:16,000
 "Yang"

68
00:05:16,000 --> 00:05:21,610
 And we're saying "yang kalakeh so" with black hair because

69
00:05:21,610 --> 00:05:24,000
 everyone in India had black hair.

70
00:05:24,000 --> 00:05:27,000
 Until it turned gray.

71
00:05:27,000 --> 00:05:32,000
 "Badrena"

72
00:05:32,000 --> 00:05:37,000
 "Badrena yobanena"

73
00:05:37,000 --> 00:05:38,000
 "Yobanena"

74
00:05:38,000 --> 00:05:40,000
 "youth"

75
00:05:40,000 --> 00:05:44,000
 In the height of their youth or something like that.

76
00:05:44,000 --> 00:05:46,000
 "Samanagato"

77
00:05:46,000 --> 00:05:54,990
 is possessed of the height of youth, the best part of youth

78
00:05:54,990 --> 00:06:02,000
, the strength of youth, the greatness of youth.

79
00:06:02,000 --> 00:06:05,000
 "Badrena yobanena"

80
00:06:05,000 --> 00:06:11,090
 In the first "waya", the first part of life, the first age

81
00:06:11,090 --> 00:06:12,000
 of life.

82
00:06:12,000 --> 00:06:16,000
 "Ayang bhikvibhatam o samaeyo badana"

83
00:06:16,000 --> 00:06:23,000
 This is the first occasion, right time for striving.

84
00:06:23,000 --> 00:06:28,000
 Do it while you're young.

85
00:06:28,000 --> 00:06:33,330
 Because every moment that we're unmindful is accumulating

86
00:06:33,330 --> 00:06:36,000
 karma, is accumulating habits.

87
00:06:36,000 --> 00:06:40,070
 And so you can say that old people benefit from meditation

88
00:06:40,070 --> 00:06:41,000
 absolutely.

89
00:06:41,000 --> 00:06:47,000
 But they also have a lot of habits built up.

90
00:06:47,000 --> 00:06:52,100
 So as far as how far they can get, you know, it depends on

91
00:06:52,100 --> 00:06:56,490
 the person of course, but young people have greater

92
00:06:56,490 --> 00:06:59,000
 potential, they've got greater strength.

93
00:06:59,000 --> 00:07:04,390
 If you've seen some of these young monks meditating day and

94
00:07:04,390 --> 00:07:10,000
 night, able to walk very well, able to sit very still,

95
00:07:10,000 --> 00:07:15,000
 your minds are sharp.

96
00:07:15,000 --> 00:07:19,000
 I mean it goes both ways, because old people have a lot of

97
00:07:19,000 --> 00:07:21,590
 the wisdom that's required to understand why we're med

98
00:07:21,590 --> 00:07:22,000
itating.

99
00:07:22,000 --> 00:07:29,720
 Young people meditate for a little while and then get bored

100
00:07:29,720 --> 00:07:32,000
 and give it up.

101
00:07:32,000 --> 00:07:35,300
 Nonetheless, something that you should start young, get a

102
00:07:35,300 --> 00:07:36,000
 head start.

103
00:07:36,000 --> 00:07:40,420
 Any old meditator will tell you, "Don't wait until you're

104
00:07:40,420 --> 00:07:43,000
 old, it's that much harder."

105
00:07:43,000 --> 00:07:48,300
 Number two, when you're sick. When you're sick, don't put

106
00:07:48,300 --> 00:07:49,000
 out effort.

107
00:07:49,000 --> 00:07:52,530
 This is interesting because you should meditate, you should

108
00:07:52,530 --> 00:07:54,000
 be very mindful as I said.

109
00:07:54,000 --> 00:07:59,510
 But in other places I said that sickness is a great

110
00:07:59,510 --> 00:08:02,000
 catalyst for insight.

111
00:08:02,000 --> 00:08:06,200
 But you shouldn't work hard because you can aggravate the

112
00:08:06,200 --> 00:08:09,000
 sickness and prolong the sickness.

113
00:08:09,000 --> 00:08:12,230
 You should take care of your body, you know, rest a lot,

114
00:08:12,230 --> 00:08:13,000
 lie down.

115
00:08:13,000 --> 00:08:21,260
 Don't do lots of walking and don't push yourself, don't

116
00:08:21,260 --> 00:08:25,000
 push your body anyway.

117
00:08:25,000 --> 00:08:28,360
 On the opposite, when you're healthy, that's when you

118
00:08:28,360 --> 00:08:32,000
 should meditate because when you're sick it gets harder.

119
00:08:32,000 --> 00:08:35,190
 It may be a great time to be mindful but it's much harder

120
00:08:35,190 --> 00:08:36,000
 to be mindful.

121
00:08:36,000 --> 00:08:39,000
 And of course you can't do all the exercises.

122
00:08:39,000 --> 00:08:42,360
 So the point is, when you're healthy, take that time to med

123
00:08:42,360 --> 00:08:46,000
itate. Don't be negligent.

124
00:08:46,000 --> 00:08:49,000
 There's a lot of talk about the negligence of health.

125
00:08:49,000 --> 00:08:52,280
 "Oh, I'm healthy now. I don't need to worry about suffering

126
00:08:52,280 --> 00:08:55,000
. I don't need to worry about my defilements.

127
00:08:55,000 --> 00:08:58,000
 I'm healthy, I can get what I want, I can do what I want.

128
00:08:58,000 --> 00:09:02,340
 If I have problems, I just chase them away or I run away

129
00:09:02,340 --> 00:09:04,000
 from them."

130
00:09:04,000 --> 00:09:08,650
 And then when we're sick, we don't know what to do. We're

131
00:09:08,650 --> 00:09:13,000
 unprepared.

132
00:09:13,000 --> 00:09:16,430
 So it's a good lesson, rather than thinking, "Oh, I'm happy

133
00:09:16,430 --> 00:09:19,790
, so why should I worry? Why should I work? Why should I

134
00:09:19,790 --> 00:09:21,000
 train myself?"

135
00:09:21,000 --> 00:09:25,000
 We should think that when you're happy, when you're well,

136
00:09:25,000 --> 00:09:28,000
 you should prepare yourself for being unwell.

137
00:09:28,000 --> 00:09:32,410
 You should prepare yourself. It's a good time to train

138
00:09:32,410 --> 00:09:35,000
 yourself and to make yourself stronger.

139
00:09:35,000 --> 00:09:37,760
 Because making yourself stronger once you're already unwell

140
00:09:37,760 --> 00:09:39,000
 is much more difficult.

141
00:09:39,000 --> 00:09:42,990
 A person's never practiced meditation, they never practiced

142
00:09:42,990 --> 00:09:45,000
 mindfulness, and then they get sick.

143
00:09:45,000 --> 00:09:48,120
 It's very hard to teach them meditation at that point

144
00:09:48,120 --> 00:09:50,000
 because they won't hear it.

145
00:09:50,000 --> 00:09:55,210
 Their mind is too reactionary. Better to start when it's

146
00:09:55,210 --> 00:09:56,000
 easy.

147
00:09:56,000 --> 00:10:04,000
 If you start when it's hard, it's much harder to start.

148
00:10:04,000 --> 00:10:07,430
 It doesn't mean you shouldn't be mindful and you shouldn't

149
00:10:07,430 --> 00:10:09,000
 try your best to be mindful when you're sick.

150
00:10:09,000 --> 00:10:15,570
 It's a great time to learn and to learn to let go because

151
00:10:15,570 --> 00:10:19,000
 you have suffering.

152
00:10:19,000 --> 00:10:24,000
 Suffering is something that we react to very strongly.

153
00:10:24,000 --> 00:10:33,770
 Number three, "dubikang hoati." One at a time when there is

154
00:10:33,770 --> 00:10:35,000
 not much food.

155
00:10:35,000 --> 00:10:41,000
 "Dusasang." "Dusasang" is "seesasa."

156
00:10:41,000 --> 00:10:45,000
 Crops, right? When there are poor crops.

157
00:10:45,000 --> 00:10:54,000
 "Dulabapindang." When it's hard to get alms.

158
00:10:54,000 --> 00:11:05,900
 "Nasukarang uncheena." It's not easy to gather to something

159
00:11:05,900 --> 00:11:10,000
, something ask.

160
00:11:10,000 --> 00:11:25,000
 "Yapiti." Why does it say that? "Yapiti."

161
00:11:25,000 --> 00:11:30,460
 It's not easy to keep one's life going on sustenance or

162
00:11:30,460 --> 00:11:33,000
 something like that.

163
00:11:33,000 --> 00:11:40,170
 When it's hard to get food, then food is scarce. This is an

164
00:11:40,170 --> 00:11:41,000
 issue for monks, more than lay people, I suppose.

165
00:11:41,000 --> 00:11:46,050
 But it can happen when you're poor, that food is scarce.

166
00:11:46,050 --> 00:11:50,000
 Well, don't push your body at that point.

167
00:11:50,000 --> 00:11:53,330
 Remember, we had one person who came to meditate and wanted

168
00:11:53,330 --> 00:11:57,300
 to do a fast, so they were just going to have fruit for

169
00:11:57,300 --> 00:11:59,000
 like three weeks.

170
00:11:59,000 --> 00:12:05,060
 And so after two weeks, he came to my hut and said he had

171
00:12:05,060 --> 00:12:08,000
 pulled a ligament in his leg.

172
00:12:08,000 --> 00:12:12,270
 Because it's hard, this isn't like some yoga retreat where

173
00:12:12,270 --> 00:12:16,040
 you just lie on your mat and stretch all, do simple

174
00:12:16,040 --> 00:12:17,000
 stretches.

175
00:12:17,000 --> 00:12:21,860
 You're actually doing hours and hours of walking, and it's

176
00:12:21,860 --> 00:12:27,000
 insight, so you're dealing with ordinary states.

177
00:12:27,000 --> 00:12:34,000
 You're not entering into very high and special states.

178
00:12:34,000 --> 00:12:36,610
 You're dealing with a lot of difficulties because you're

179
00:12:36,610 --> 00:12:39,000
 trying to learn and understand how your mind works.

180
00:12:39,000 --> 00:12:42,590
 You're digging right in there with the problems. It's very

181
00:12:42,590 --> 00:12:56,000
 strenuous. No, it's very taxing on your system.

182
00:12:56,000 --> 00:13:01,700
 So when you, you have to nourish your body. When you're not

183
00:13:01,700 --> 00:13:07,190
 able to find nourishment, that's the time you have to be

184
00:13:07,190 --> 00:13:09,000
 careful.

185
00:13:09,000 --> 00:13:22,000
 And then, "Bhayang hoti atta visan kopu."

186
00:13:22,000 --> 00:13:35,050
 When there is danger, "Chakka samam channapada pariyayanti

187
00:13:35,050 --> 00:13:36,000
."

188
00:13:36,000 --> 00:13:40,380
 I think I have to look up a translation. I'm not familiar

189
00:13:40,380 --> 00:13:42,000
 with any of this.

190
00:13:42,000 --> 00:13:45,440
 There is peril, turbulence in the wilderness, and the

191
00:13:45,440 --> 00:13:49,100
 people of the countryside mounted on their vehicles flee on

192
00:13:49,100 --> 00:13:50,000
 all sides.

193
00:13:50,000 --> 00:13:54,000
 Thank you, Bhikkhu Bodhi.

194
00:13:54,000 --> 00:13:57,000
 Ah, there's danger.

195
00:13:57,000 --> 00:14:00,110
 When there's danger, you should be careful. You shouldn't

196
00:14:00,110 --> 00:14:02,000
 go out and do walking.

197
00:14:02,000 --> 00:14:07,510
 If there's a war in your area, you have to take care of

198
00:14:07,510 --> 00:14:09,000
 your body.

199
00:14:09,000 --> 00:14:14,340
 It's not a good time for striving because you'll be worried

200
00:14:14,340 --> 00:14:20,000
 about dangers, and you might even be attacked and harmed.

201
00:14:20,000 --> 00:14:23,530
 And so, I mean, part of the lesson here is that when you

202
00:14:23,530 --> 00:14:27,000
 don't have danger, when life is good,

203
00:14:27,000 --> 00:14:30,890
 this is when take advantage of the moment, take advantage

204
00:14:30,890 --> 00:14:32,000
 of the opportunity.

205
00:14:32,000 --> 00:14:48,000
 Ganau mau pachaga. Don't let the moment pass you by.

206
00:14:48,000 --> 00:14:54,520
 So when there's harmony in society, manusa samanga samodham

207
00:14:54,520 --> 00:14:57,000
ana, avivandhamana,

208
00:14:57,000 --> 00:15:03,520
 when people are dwelling in harmony, when they're not

209
00:15:03,520 --> 00:15:07,000
 fighting, kiro dakki bodha,

210
00:15:07,000 --> 00:15:11,490
 having become like milk and water, mixing like milk and

211
00:15:11,490 --> 00:15:14,000
 water without any turbulence.

212
00:15:14,000 --> 00:15:20,080
 When people can mix, when people look at each other fondly,

213
00:15:20,080 --> 00:15:24,000
 piya chakkuhi sampasanta aviharanthi,

214
00:15:24,000 --> 00:15:35,630
 when they dwell in harmony, eyeing each other favorably,

215
00:15:35,630 --> 00:15:40,000
 kindly, dearly.

216
00:15:40,000 --> 00:15:45,870
 Number five is when your community, when the sangha is

217
00:15:45,870 --> 00:15:49,000
 dwelling, when the sangha is split up,

218
00:15:49,000 --> 00:15:53,140
 when your meditation community, when your community, when

219
00:15:53,140 --> 00:15:54,000
 your family,

220
00:15:54,000 --> 00:15:58,000
 that's a time where it's difficult to strive.

221
00:15:58,000 --> 00:16:06,160
 So when your family is in harmony, you should take that

222
00:16:06,160 --> 00:16:09,000
 opportunity to strive.

223
00:16:09,000 --> 00:16:12,610
 And so I don't think this really means that you shouldn't

224
00:16:12,610 --> 00:16:14,000
 try to be mindful.

225
00:16:14,000 --> 00:16:17,760
 You should just give up and throw in the towel when things

226
00:16:17,760 --> 00:16:19,000
 are difficult.

227
00:16:19,000 --> 00:16:23,370
 It's more of a lesson of how difficult it is with those

228
00:16:23,370 --> 00:16:26,000
 things, and so to take advantage of the time.

229
00:16:26,000 --> 00:16:29,000
 But it's also a warning that you shouldn't push too hard

230
00:16:29,000 --> 00:16:35,000
 when there are things that will be affected by striving.

231
00:16:35,000 --> 00:16:39,100
 So when your body might be injured because you're striving,

232
00:16:39,100 --> 00:16:41,000
 because you're not getting enough nourishment,

233
00:16:41,000 --> 00:16:46,220
 when you might be killed because there's war going on and

234
00:16:46,220 --> 00:16:48,000
 that kind of thing,

235
00:16:48,000 --> 00:16:53,580
 or you might be subject to all sorts of problems because

236
00:16:53,580 --> 00:16:55,000
 people are fighting,

237
00:16:55,000 --> 00:16:59,410
 and your family or your community is fighting, you should

238
00:16:59,410 --> 00:17:04,000
 resolve some of these problems.

239
00:17:04,000 --> 00:17:11,140
 Anyway, interesting sutta, giving the five good times to

240
00:17:11,140 --> 00:17:12,000
 strive

241
00:17:12,000 --> 00:17:20,000
 and the five times when striving becomes difficult.

242
00:17:20,000 --> 00:17:29,240
 So next up, I'm going to post the Hangout link on

243
00:17:29,240 --> 00:17:33,000
 meditation.siri-mongolo.org.

244
00:17:33,000 --> 00:17:35,990
 If you want to join the Hangout, you're welcome to come and

245
00:17:35,990 --> 00:17:37,000
 ask questions.

246
00:17:37,000 --> 00:17:41,000
 This is where I take live questions.

247
00:17:41,000 --> 00:17:48,000
 Is anyone brave enough to join me with a video webcam and a

248
00:17:48,000 --> 00:17:50,000
 microphone?

249
00:17:50,000 --> 00:17:53,410
 Give it a couple of minutes. If nobody shows up, then we

250
00:17:53,410 --> 00:17:55,000
 say good night.

251
00:17:55,000 --> 00:18:02,990
 Tomorrow I'm giving a talk in Second Life as far as I know.

252
00:18:02,990 --> 00:18:06,080
 It's at 12 pm, 12 noon, Second Lifetime in the Buddha

253
00:18:06,080 --> 00:18:07,000
 Center.

254
00:18:07,000 --> 00:18:12,230
 So if you know what that means, come on out. If you don't

255
00:18:12,230 --> 00:18:21,000
 know what that means, don't worry about it.

256
00:18:21,000 --> 00:18:26,120
 Oh, and it looks like I'm going to New York City. If you're

257
00:18:26,120 --> 00:18:29,000
 in the New York City area,

258
00:18:29,000 --> 00:18:33,800
 it looks like I might be there from the 16th of April to

259
00:18:33,800 --> 00:18:36,000
 the 23rd of April.

260
00:18:36,000 --> 00:18:42,000
 It looks sometime around that time. It's not confirmed yet,

261
00:18:42,000 --> 00:18:48,000
 but it's decided upon, agreed upon.

262
00:18:48,000 --> 00:18:53,350
 It looks like I'll be doing some meditation teachings in

263
00:18:53,350 --> 00:18:55,000
 New York City.

264
00:18:55,000 --> 00:19:01,980
 Some special memories of New York City. I was flying to

265
00:19:01,980 --> 00:19:07,000
 Thailand as a monk,

266
00:19:07,000 --> 00:19:12,000
 and I brought along a novice, a Cambodian novice.

267
00:19:12,000 --> 00:19:16,030
 We agreed. This was of course back when I was with monks

268
00:19:16,030 --> 00:19:18,000
 who were all using money,

269
00:19:18,000 --> 00:19:21,960
 but I decided, this was the first time I decided that I was

270
00:19:21,960 --> 00:19:24,000
 going to stop using money.

271
00:19:24,000 --> 00:19:28,000
 So I said, "We're not bringing any money on this trip."

272
00:19:28,000 --> 00:19:32,490
 We got to New York City, and I had left my passport on the

273
00:19:32,490 --> 00:19:34,000
 plane in the seat in front of me,

274
00:19:34,000 --> 00:19:43,870
 and it went back to Toronto. We missed our plane from New

275
00:19:43,870 --> 00:19:46,000
 York City to Korea,

276
00:19:46,000 --> 00:19:55,000
 I can't remember, Korea or Taiwan, probably Taiwan.

277
00:19:55,000 --> 00:20:09,000
 We spent almost 24 hours in New York City,

278
00:20:09,000 --> 00:20:12,740
 and we wouldn't have survived if the novice hadn't secretly

279
00:20:12,740 --> 00:20:13,000
.

280
00:20:13,000 --> 00:20:15,630
 I said, "I don't know what we're going to do." So he pulls

281
00:20:15,630 --> 00:20:21,000
 out like $100, and he had to suck some money.

282
00:20:21,000 --> 00:20:26,150
 So he had money to get a taxi, and then to get a train back

283
00:20:26,150 --> 00:20:27,000
 to the airport.

284
00:20:27,000 --> 00:20:33,300
 So he got a taxi to the Cambodian monastery, and then we

285
00:20:33,300 --> 00:20:38,000
 slept some time in the JFK airport.

286
00:20:38,000 --> 00:20:43,000
 But I was there a couple of years ago as well, just passed

287
00:20:43,000 --> 00:20:44,000
 through.

288
00:20:44,000 --> 00:20:51,270
 I ended up passing a couple of nights in Long Island. Maybe

289
00:20:51,270 --> 00:20:53,000
 I'll go to Long Island again this time,

290
00:20:53,000 --> 00:20:57,000
 through the Sri Lankan Meditation Center there.

291
00:20:57,000 --> 00:21:03,000
 That's a really nice, good friend of mine, Nanda.

292
00:21:03,000 --> 00:21:18,560
 Hello, Stephen. Or, Stefan. Hello? Your mic is live, I can

293
00:21:18,560 --> 00:21:21,000
 hear you.

294
00:21:21,000 --> 00:21:27,000
 I can hear you, but I don't know what you're saying.

295
00:21:27,000 --> 00:21:40,000
 Do you have a question? Hello?

296
00:21:40,000 --> 00:21:48,000
 I don't know, man.

297
00:21:48,000 --> 00:21:51,000
 Interesting.

298
00:21:51,000 --> 00:22:03,000
 All right, I'm assuming no questions.

299
00:22:03,000 --> 00:22:06,600
 Where will I be teaching in New York City? I don't know yet

300
00:22:06,600 --> 00:22:07,000
.

301
00:22:07,000 --> 00:22:13,330
 Nothing's confirmed. It's all very prelim. I'll let you

302
00:22:13,330 --> 00:22:15,000
 know.

303
00:22:15,000 --> 00:22:23,340
 Have a good night, everyone. See some of you in second life

304
00:22:23,340 --> 00:22:40,000
 tomorrow. Good night.

305
00:22:41,000 --> 00:22:42,000
 Thank you.

306
00:22:43,000 --> 00:22:44,000
 Thank you.

