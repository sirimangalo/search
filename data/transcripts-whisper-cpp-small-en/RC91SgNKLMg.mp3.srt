1
00:00:00,000 --> 00:00:04,560
 I'm currently going through some emotional hard time.

2
00:00:04,560 --> 00:00:07,240
 Sometimes I feel hopelessness or being upset for my

3
00:00:07,240 --> 00:00:10,060
 situation throughout the day.

4
00:00:10,060 --> 00:00:13,540
 So should I note it and put my whole thought into that

5
00:00:13,540 --> 00:00:15,700
 experience until it goes away or

6
00:00:15,700 --> 00:00:19,330
 should I just shift my mind onto the rising and falling of

7
00:00:19,330 --> 00:00:20,380
 my stomach?

8
00:00:20,380 --> 00:00:23,420
 I feel that I still haven't let go of my loss even though I

9
00:00:23,420 --> 00:00:25,100
'm using the meditation as a

10
00:00:25,100 --> 00:00:29,980
 replacement.

11
00:00:29,980 --> 00:00:34,560
 Where did I hear this recently?

12
00:00:34,560 --> 00:00:38,200
 Someone said a very similar thing on the chat here.

13
00:00:38,200 --> 00:00:45,270
 They asked me a question and they said that meditation was

14
00:00:45,270 --> 00:00:48,440
 kind of a means for them to

15
00:00:48,440 --> 00:00:50,760
 escape the problem.

16
00:00:50,760 --> 00:00:56,990
 So I was trying to explain, they're practicing and they

17
00:00:56,990 --> 00:01:01,000
 keep thinking about these thoughts

18
00:01:01,000 --> 00:01:03,000
 keep coming back, right?

19
00:01:03,000 --> 00:01:11,040
 The meditation is bringing them up or so on.

20
00:01:11,040 --> 00:01:13,870
 They were saying, "Isn't it great that the meditation

21
00:01:13,870 --> 00:01:15,760
 allows you to see these things?"

22
00:01:15,760 --> 00:01:20,070
 They said, "Well, no, meditation, I guess it gives me some

23
00:01:20,070 --> 00:01:22,560
 kind of relief sometimes."

24
00:01:22,560 --> 00:01:26,140
 As you say, it's like a replacement or a means of shifting

25
00:01:26,140 --> 00:01:28,520
 the attention away from the experience

26
00:01:28,520 --> 00:01:34,760
 or a means of finding peace in hard times.

27
00:01:34,760 --> 00:01:38,390
 But I said right away and directly in response that that's

28
00:01:38,390 --> 00:01:40,360
 not what meditation is for.

29
00:01:40,360 --> 00:01:44,440
 It's for you to learn about your problems and to learn

30
00:01:44,440 --> 00:01:46,640
 about what it is that you're

31
00:01:46,640 --> 00:01:47,640
 experiencing.

32
00:01:47,640 --> 00:01:51,430
 So it's not quite an answer to your question but something

33
00:01:51,430 --> 00:01:53,400
 that I would question in your

34
00:01:53,400 --> 00:01:56,550
 mind and be sure of yourself that you're not doing this,

35
00:01:56,550 --> 00:01:58,600
 that you're not expecting something

36
00:01:58,600 --> 00:02:01,440
 of meditation.

37
00:02:01,440 --> 00:02:09,760
 In general, that's really one of the big key points that we

38
00:02:09,760 --> 00:02:13,160
 have to understand in terms

39
00:02:13,160 --> 00:02:16,950
 of meditation is that we really shouldn't expect to get

40
00:02:16,950 --> 00:02:19,080
 anything out of meditation,

41
00:02:19,080 --> 00:02:22,470
 that we shouldn't expect to create anything out of

42
00:02:22,470 --> 00:02:23,560
 meditation.

43
00:02:23,560 --> 00:02:28,840
 We're trying to take what is already there and understand

44
00:02:28,840 --> 00:02:31,080
 it and straighten it.

45
00:02:31,080 --> 00:02:33,200
 The mind, the Buddha said straighten the mind.

46
00:02:33,200 --> 00:02:37,090
 Well, that means straighten out our habits and our

47
00:02:37,090 --> 00:02:40,000
 reactions and our ways of responding

48
00:02:40,000 --> 00:02:45,070
 and reacting to things as opposed to interacting, as

49
00:02:45,070 --> 00:02:47,520
 opposed to being with.

50
00:02:47,520 --> 00:02:52,970
 That isn't an answer to your question but it's something

51
00:02:52,970 --> 00:02:55,500
 that I would try to stress

52
00:02:55,500 --> 00:02:58,370
 based on the fact that you're saying you're using

53
00:02:58,370 --> 00:03:00,360
 meditation as a replacement.

54
00:03:00,360 --> 00:03:03,760
 Meditation is for the purpose of coming to terms with your

55
00:03:03,760 --> 00:03:05,920
 emotional hard time, the emotional

56
00:03:05,920 --> 00:03:11,400
 stress and difficulty.

57
00:03:11,400 --> 00:03:16,110
 Maybe I'll address the question and then turn it over to

58
00:03:16,110 --> 00:03:19,280
 the panel, the other panelists.

59
00:03:19,280 --> 00:03:23,020
 Let me just briefly, I'll give some pointers here because

60
00:03:23,020 --> 00:03:25,240
 the question is quite direct,

61
00:03:25,240 --> 00:03:28,330
 should you note it all the time or should you put it aside

62
00:03:28,330 --> 00:03:30,080
 and go back to the stomach.

63
00:03:30,080 --> 00:03:32,520
 You can really do either.

64
00:03:32,520 --> 00:03:36,150
 There are no hard and fast rules but you have to assess

65
00:03:36,150 --> 00:03:38,320
 what is the result of doing one

66
00:03:38,320 --> 00:03:41,320
 or the other.

67
00:03:41,320 --> 00:03:44,860
 If you're able to be mindful of it and actually if you're

68
00:03:44,860 --> 00:03:46,880
 able to be mindful of it, it should

69
00:03:46,880 --> 00:03:49,300
 dissolve quite quickly.

70
00:03:49,300 --> 00:03:51,600
 It may be that you're not able to be mindful of it and that

71
00:03:51,600 --> 00:03:52,720
 after a while it makes you

72
00:03:52,720 --> 00:03:59,060
 so stressed out that it actually brings on more stress in

73
00:03:59,060 --> 00:04:01,960
 your mind and gets you to a

74
00:04:01,960 --> 00:04:05,640
 point where you feel like you're going crazy.

75
00:04:05,640 --> 00:04:08,280
 If at that point you decide you've been acknowledging it

76
00:04:08,280 --> 00:04:10,520
 for some time, it's not going away, it's

77
00:04:10,520 --> 00:04:13,490
 overwhelming and so on, you can come back to the stomach,

78
00:04:13,490 --> 00:04:15,080
 start with the stomach again

79
00:04:15,080 --> 00:04:18,080
 and something like starting over.

80
00:04:18,080 --> 00:04:19,440
 What you don't want to do is avoid it.

81
00:04:19,440 --> 00:04:22,510
 You don't want to, "I'm not going to touch that, I'm not

82
00:04:22,510 --> 00:04:24,000
 going to be mindful of it, it's

83
00:04:24,000 --> 00:04:26,800
 just too difficult."

84
00:04:26,800 --> 00:04:28,840
 It's like getting a running start.

85
00:04:28,840 --> 00:04:33,160
 Start way back up and start here, rising, falling.

86
00:04:33,160 --> 00:04:37,080
 When it comes up again, slowly, slowly catch it piece by

87
00:04:37,080 --> 00:04:38,720
 piece until you're able to face

88
00:04:38,720 --> 00:04:43,000
 it head on.

89
00:04:43,000 --> 00:04:44,280
 There are other techniques as well.

90
00:04:44,280 --> 00:04:46,280
 It doesn't mean you have to come back to the stomach.

91
00:04:46,280 --> 00:04:48,650
 One of the greatest techniques when a person is overwhelmed

92
00:04:48,650 --> 00:04:49,800
 by something is to do lying

93
00:04:49,800 --> 00:04:53,950
 meditation because as I said, that's where you're the most

94
00:04:53,950 --> 00:04:54,880
 relaxed.

95
00:04:54,880 --> 00:04:58,300
 When you're stressed, when you're overstressed, you can try

96
00:04:58,300 --> 00:04:59,920
 doing lying meditation.

97
00:04:59,920 --> 00:05:03,340
 It's a way of retreating from the stress, finding something

98
00:05:03,340 --> 00:05:04,720
 that's easier for you to

99
00:05:04,720 --> 00:05:05,720
 deal with.

100
00:05:05,720 --> 00:05:06,720
 Okay, this is easy.

101
00:05:06,720 --> 00:05:09,340
 Still maybe have some of the emotions but you'll find that

102
00:05:09,340 --> 00:05:10,880
 they're easier to deal with.

103
00:05:10,880 --> 00:05:19,040
 They're not as intense because you feel more peaceful.

104
00:05:19,040 --> 00:05:22,320
 You feel better lying down, more relaxed lying down.

105
00:05:22,320 --> 00:05:27,500
 As an example, you can stop meditating, take a break, come

106
00:05:27,500 --> 00:05:29,640
 back and try again and so on.

107
00:05:29,640 --> 00:05:32,040
 But there are no hard and fast rules in that sense.

108
00:05:32,040 --> 00:05:35,050
 But I would like to have answers from other people, so

109
00:05:35,050 --> 00:05:36,880
 maybe you have some thoughts on

110
00:05:36,880 --> 00:05:37,880
 this.

111
00:05:37,880 --> 00:05:39,880
 Yes, I do.

112
00:05:39,880 --> 00:05:50,520
 I do.

113
00:05:50,520 --> 00:06:00,190
 I think you should be very open with the moment to see what

114
00:06:00,190 --> 00:06:03,160
 is necessary.

115
00:06:03,160 --> 00:06:08,190
 Sometimes when you note upset and hopelessness for a long

116
00:06:08,190 --> 00:06:10,960
 time and it doesn't go away, it

117
00:06:10,960 --> 00:06:16,850
 is not good to stay there because you eventually start to

118
00:06:16,850 --> 00:06:20,480
 indulge in it without knowing it.

119
00:06:20,480 --> 00:06:25,190
 And you just, you think you're just noting it but you're

120
00:06:25,190 --> 00:06:27,480
 kind of feeding it with it and

121
00:06:27,480 --> 00:06:32,060
 you feel good in it and you haven't, you have not noticed

122
00:06:32,060 --> 00:06:33,120
 that yet.

123
00:06:33,120 --> 00:06:38,140
 So in such a case when it does not go away, it might be

124
00:06:38,140 --> 00:06:41,360
 good to focus on something else

125
00:06:41,360 --> 00:06:46,650
 because there is always something that can be prominent as

126
00:06:46,650 --> 00:06:47,480
 well.

127
00:06:47,480 --> 00:06:53,910
 And when you go back to the rising and the falling, for

128
00:06:53,910 --> 00:06:57,600
 example, in my experience it

129
00:06:57,600 --> 00:07:02,320
 brings you a good stability.

130
00:07:02,320 --> 00:07:09,020
 When I'm, for example, lost in emotions, what of course

131
00:07:09,020 --> 00:07:12,440
 happens and it's kind of whirling

132
00:07:12,440 --> 00:07:17,830
 around and I can't really catch it or it's too much, then

133
00:07:17,830 --> 00:07:20,600
 going back to the rising and

134
00:07:20,600 --> 00:07:28,200
 the falling of the stomach brings me, I called it the other

135
00:07:28,200 --> 00:07:32,400
 day, kind of in a position of

136
00:07:32,400 --> 00:07:36,160
 in the eye of the hurricane.

137
00:07:36,160 --> 00:07:40,310
 There it's peaceful and you are alert and you can from that

138
00:07:40,310 --> 00:07:42,140
 position observe what is

139
00:07:42,140 --> 00:07:50,160
 going on around without getting hurt by it or without

140
00:07:50,160 --> 00:07:54,720
 getting carried away by it.

141
00:07:54,720 --> 00:07:59,160
 So yeah, I think it's good from time to time to go back to

142
00:07:59,160 --> 00:08:01,840
 the rising and falling then.

143
00:08:01,840 --> 00:08:02,840
 Sure.

144
00:08:02,840 --> 00:08:07,000
 The, I think there's really no hard and fast rule and you

145
00:08:07,000 --> 00:08:08,880
 rule with the punches.

146
00:08:08,880 --> 00:08:11,000
 You consider you're in a boxing match.

147
00:08:11,000 --> 00:08:14,640
 There's no answer whether should I punch or should I dodge.

148
00:08:14,640 --> 00:08:17,060
 Sometimes you have to dodge, sometimes you have to punch,

149
00:08:17,060 --> 00:08:17,760
 sometimes you have to even

150
00:08:17,760 --> 00:08:22,100
 take a punch and sometimes you get knocked down and you get

151
00:08:22,100 --> 00:08:23,240
 back up again.

152
00:08:23,240 --> 00:08:29,280
 The story, the song goes.

153
00:08:29,280 --> 00:08:32,240
 But you shouldn't, I think you shouldn't have a hard and

154
00:08:32,240 --> 00:08:33,880
 fast rule and that's what I would

155
00:08:33,880 --> 00:08:38,100
 caution against because suppose someone takes the hard and

156
00:08:38,100 --> 00:08:40,440
 fast rule that when these things

157
00:08:40,440 --> 00:08:42,880
 come up, I'm going to go back to the stomach.

158
00:08:42,880 --> 00:08:48,940
 If you take that as a rule, then it's easy to see how it

159
00:08:48,940 --> 00:08:52,400
 might lead to developing aversion

160
00:08:52,400 --> 00:08:56,200
 towards it or a habit of avoiding the experience.

161
00:08:56,200 --> 00:08:59,590
 If you force yourself, you take it as a rule that you have

162
00:08:59,590 --> 00:09:01,360
 to stay with it, then it can

163
00:09:01,360 --> 00:09:05,340
 drive you crazy and it can lead you to not want to meditate

164
00:09:05,340 --> 00:09:05,680
.

165
00:09:05,680 --> 00:09:14,660
 It can lead you to feel incapable of finding any benefit in

166
00:09:14,660 --> 00:09:17,240
 the meditation.

167
00:09:17,240 --> 00:09:19,760
 Even entering into the jhanas in that sense is good.

168
00:09:19,760 --> 00:09:22,200
 Mahasya Sayantas said if you've practiced jhana before,

169
00:09:22,200 --> 00:09:23,400
 then it's great because you

170
00:09:23,400 --> 00:09:28,780
 can go back and feel peaceful again and get your confidence

171
00:09:28,780 --> 00:09:31,240
 back up and go out and fight.

172
00:09:31,240 --> 00:09:34,610
 But the other thing I would say is that actually staying

173
00:09:34,610 --> 00:09:37,040
 with it and suppose it makes you really

174
00:09:37,040 --> 00:09:40,860
 angry and upset because this is often what happens.

175
00:09:40,860 --> 00:09:43,310
 We give the meditator an entire day to themselves and we're

176
00:09:43,310 --> 00:09:44,780
 not there sitting with them telling

177
00:09:44,780 --> 00:09:45,840
 them what to do.

178
00:09:45,840 --> 00:09:47,280
 So they do everything wrong.

179
00:09:47,280 --> 00:09:51,800
 They'll go away to this extreme, way to that extreme and it

180
00:09:51,800 --> 00:09:53,600
's actually not that big of

181
00:09:53,600 --> 00:09:56,250
 a deal as long as you have a teacher to pull you back on

182
00:09:56,250 --> 00:09:58,560
 track because it teaches you something.

183
00:09:58,560 --> 00:10:01,320
 So the meditator said, "Oh, I was sitting with it and I was

184
00:10:01,320 --> 00:10:02,160
 so..."

185
00:10:02,160 --> 00:10:04,360
 It just made me more and more angry and then you can say, "

186
00:10:04,360 --> 00:10:07,280
Well, what's it like to be angry?"

187
00:10:07,280 --> 00:10:12,920
 Now you know what it's like to be angry for a whole day.

188
00:10:12,920 --> 00:10:15,850
 What we're trying to do is learn and the best thing you can

189
00:10:15,850 --> 00:10:17,600
 learn from is your mistakes.

190
00:10:17,600 --> 00:10:22,500
 It takes a long time but you learn from your defilements

191
00:10:22,500 --> 00:10:23,520
 really.

192
00:10:23,520 --> 00:10:31,000
 Sometimes, you have to go through these things to really

193
00:10:31,000 --> 00:10:35,440
 get over them, to be so fed up with

194
00:10:35,440 --> 00:10:40,060
 them, to know them again and again and then you thought, "

195
00:10:40,060 --> 00:10:41,840
Oh, I got over it."

196
00:10:41,840 --> 00:10:45,640
 And then it comes up again and you note it again.

197
00:10:45,640 --> 00:10:51,000
 Then at one time, at one point, there comes the moment when

198
00:10:51,000 --> 00:10:53,360
 you really get fed up with

199
00:10:53,360 --> 00:10:59,440
 it, when you let go of it because you just saw it enough.

200
00:10:59,440 --> 00:11:03,050
 But I think the key to all of these answers is that you

201
00:11:03,050 --> 00:11:04,660
 have to be dynamic.

202
00:11:04,660 --> 00:11:09,680
 You should never have a hard and fast rule.

203
00:11:09,680 --> 00:11:12,960
 The path is how long is the path and you've got to know

204
00:11:12,960 --> 00:11:14,400
 every trick in the book to get

205
00:11:14,400 --> 00:11:16,240
 through it.

206
00:11:16,240 --> 00:11:22,440
 Can we say that Samma Vayama right effort is a good

207
00:11:22,440 --> 00:11:25,600
 measurement for that?

208
00:11:25,600 --> 00:11:27,200
 Samma Vayama?

209
00:11:27,200 --> 00:11:28,200
 Yeah.

210
00:11:28,200 --> 00:11:30,200
 I believe the word they use is kuṣṣṣṣuṣpaya.

211
00:11:30,200 --> 00:11:32,200
 Do you know this one?

212
00:11:32,200 --> 00:11:33,200
 No question.

213
00:11:33,200 --> 00:11:36,470
 Kuṣṣṣṣuṣpaya is knowing the right thing to do at

214
00:11:36,470 --> 00:11:38,720
 the right time, is having all these

215
00:11:38,720 --> 00:11:40,400
 tricks, knowing the right trick to you.

216
00:11:40,400 --> 00:11:41,400
 Yeah, that's good as well.

217
00:11:41,400 --> 00:11:46,730
 Kuṣṣṣṣuṣpaya, it's the word that they use in the

218
00:11:46,730 --> 00:11:50,120
 Mahayana to explain or in some Buddhists

219
00:11:50,120 --> 00:11:56,200
 will use to explain doing of evil deeds for a good purpose.

220
00:11:56,200 --> 00:11:59,580
 I've heard this argument that this is that argument about

221
00:11:59,580 --> 00:12:01,400
 lying to save someone's life,

222
00:12:01,400 --> 00:12:02,400
 for example.

223
00:12:02,400 --> 00:12:05,190
 This is considered skillful means, doing an evil deed for

224
00:12:05,190 --> 00:12:06,440
 the purpose of goodness.

225
00:12:06,440 --> 00:12:09,880
 But that's not how it's understood in the early text.

226
00:12:09,880 --> 00:12:11,880
 Kuṣṣṣluṣṣa lupaya.

227
00:12:11,880 --> 00:12:16,080
 Kuṣṣṣa lupaya means wholesome or skillful.

228
00:12:16,080 --> 00:12:28,280
 Upaya means a device or a means, I guess, a stratagem, you

229
00:12:28,280 --> 00:12:31,080
 could say.

230
00:12:31,080 --> 00:12:33,430
 It's this kind of thing where, what do you do when you're

231
00:12:33,430 --> 00:12:34,520
 really stressed out?

232
00:12:34,520 --> 00:12:37,520
 If you know the kuṣṣṣluṣa ya, it's to lie down.

233
00:12:37,520 --> 00:12:40,070
 Because lying down is actually something we tell meditators

234
00:12:40,070 --> 00:12:40,600
 not to do.

235
00:12:40,600 --> 00:12:42,080
 It's not a good thing.

236
00:12:42,080 --> 00:12:45,120
 But in certain instances, it's the right thing to do.

237
00:12:45,120 --> 00:12:47,600
 When you're really stressed out, do lying meditation.

238
00:12:47,600 --> 00:12:50,830
 There was one monk, this crazy monk that I always refer

239
00:12:50,830 --> 00:12:52,960
 back to who slit his wrists,

240
00:12:52,960 --> 00:12:55,960
 lit himself on fire.

241
00:12:55,960 --> 00:13:01,120
 At one point he came to our teacher and he got a hand to Aj

242
00:13:01,120 --> 00:13:02,360
ahn Tong.

243
00:13:02,360 --> 00:13:03,360
 Nothing can face him.

244
00:13:03,360 --> 00:13:06,890
 This monk came up to him and Ajahn Tong says, "How's

245
00:13:06,890 --> 00:13:07,800
 walking?

246
00:13:07,800 --> 00:13:08,800
 How's sitting?"

247
00:13:08,800 --> 00:13:10,640
 He says, "Ajahn, I can't walk.

248
00:13:10,640 --> 00:13:11,640
 I can't do walking."

249
00:13:11,640 --> 00:13:12,880
 He says, "Well, fine, then just do sitting."

250
00:13:12,880 --> 00:13:13,880
 He says, "I can't sit.

251
00:13:13,880 --> 00:13:16,240
 Fine, then just do lying meditation.

252
00:13:16,240 --> 00:13:17,560
 I can't lie down."

253
00:13:17,560 --> 00:13:20,570
 He's explaining why he can't walk, why he can't sit, why he

254
00:13:20,570 --> 00:13:21,560
 can't lie down.

255
00:13:21,560 --> 00:13:23,560
 The rest of us are like, "What's he going to do then?"

256
00:13:23,560 --> 00:13:26,320
 He says, "Then do standing meditation."

257
00:13:26,320 --> 00:13:31,240
 He goes, "Yeah, I can do standing."

258
00:13:31,240 --> 00:13:33,760
 He ended up doing standing meditation.

259
00:13:33,760 --> 00:13:36,280
 He couldn't lie down because he was going crazy really.

260
00:13:36,280 --> 00:13:42,000
 He was contorting himself into all these shapes.

261
00:13:42,000 --> 00:13:48,400
 The things I had to deal with this guy was an interesting

262
00:13:48,400 --> 00:13:50,760
 experience in psychology.

263
00:13:50,760 --> 00:14:01,560
 May I come back to the samavayama?

264
00:14:01,560 --> 00:14:12,170
 When I remember correctly, it is to raise the wholesome

265
00:14:12,170 --> 00:14:17,800
 states that are there.

266
00:14:17,800 --> 00:14:21,690
 The wholesome states that haven't arisen, the bhavana

267
00:14:21,690 --> 00:14:23,440
 develop them, the ones that are

268
00:14:23,440 --> 00:14:30,080
 already there, anurakana protect them.

269
00:14:30,080 --> 00:14:32,960
 The evil states that are already there, bhahana abandon

270
00:14:32,960 --> 00:14:33,760
 them.

271
00:14:33,760 --> 00:14:38,690
 The evil states that are not yet there, maybe that's anurak

272
00:14:38,690 --> 00:14:41,160
ana, I can't remember, a guard

273
00:14:41,160 --> 00:14:43,880
 against them.

274
00:14:43,880 --> 00:14:48,510
 So, clear what you're saying, from time to time you do one

275
00:14:48,510 --> 00:14:50,680
 or the other is the idea?

276
00:14:50,680 --> 00:14:56,790
 Yeah, I thought having that in mind as a measurement for

277
00:14:56,790 --> 00:14:59,880
 the meditation in general.

278
00:14:59,880 --> 00:15:04,830
 But there is a better teaching that is more from time to

279
00:15:04,830 --> 00:15:08,040
 time-ish because the samavayama,

280
00:15:08,040 --> 00:15:11,170
 it's difficult because it actually is supposed to all come

281
00:15:11,170 --> 00:15:13,240
 together at the same time, right?

282
00:15:13,240 --> 00:15:18,880
 It's all, being mindful really does all of that at once.

283
00:15:18,880 --> 00:15:21,330
 It can be seen in that way but there's one really good

284
00:15:21,330 --> 00:15:22,920
 teaching that I think is given

285
00:15:22,920 --> 00:15:26,270
 in several places, probably it's in the Anguttara Nikaya, I

286
00:15:26,270 --> 00:15:27,800
'd like to find it again.

287
00:15:27,800 --> 00:15:32,400
 He said, the Buddha says, "A skillful meditator or a skill

288
00:15:32,400 --> 00:15:35,120
ful monk knows when it's appropriate

289
00:15:35,120 --> 00:15:39,340
 to encourage the mind, when it's appropriate to discourage

290
00:15:39,340 --> 00:15:41,720
 the mind, when it's appropriate

291
00:15:41,720 --> 00:15:49,710
 to keep the mind, stay the mind, stop it, and when it's

292
00:15:49,710 --> 00:15:52,360
 appropriate."

293
00:15:52,360 --> 00:15:58,600
 See there are these four different times.

294
00:15:58,600 --> 00:16:01,640
 You really feel this as a teacher.

295
00:16:01,640 --> 00:16:04,380
 When you're teaching people meditation you have to be

296
00:16:04,380 --> 00:16:05,320
 exactly this.

297
00:16:05,320 --> 00:16:07,630
 Sometimes a meditator will come to you and you have to say,

298
00:16:07,630 --> 00:16:09,000
 "Yes, good, keep going."

299
00:16:09,000 --> 00:16:10,000
 Encourage them.

300
00:16:10,000 --> 00:16:13,910
 Or more like, "No, no, no, you're doing fine, that's really

301
00:16:13,910 --> 00:16:15,160
 not a problem."

302
00:16:15,160 --> 00:16:16,800
 You're learning more, see?

303
00:16:16,800 --> 00:16:19,440
 You've learned more about your side, encouraging them.

304
00:16:19,440 --> 00:16:20,440
 Sometimes you have to discourage them.

305
00:16:20,440 --> 00:16:22,990
 They'll come to you and say, "Wow, I had a great meditation

306
00:16:22,990 --> 00:16:23,280
."

307
00:16:23,280 --> 00:16:24,440
 They'll say, "Oh yeah?

308
00:16:24,440 --> 00:16:25,440
 You like it?

309
00:16:25,440 --> 00:16:26,440
 Oh, I like it.

310
00:16:26,440 --> 00:16:27,440
 I really like, I loved it."

311
00:16:27,440 --> 00:16:30,840
 Do you know what liking is?

312
00:16:30,840 --> 00:16:34,200
 Liking, well, that's greed.

313
00:16:34,200 --> 00:16:35,320
 Is greed a good thing?

314
00:16:35,320 --> 00:16:36,320
 What does greed lead to?

315
00:16:36,320 --> 00:16:37,320
 It leads to addiction.

316
00:16:37,320 --> 00:16:41,600
 You don't like, no need to like things and so on.

317
00:16:41,600 --> 00:16:43,100
 Pull them back.

318
00:16:43,100 --> 00:16:48,080
 Sometimes you have to keep them on course.

319
00:16:48,080 --> 00:16:50,220
 It's playing this game, but this is how the Buddha taught

320
00:16:50,220 --> 00:16:51,480
 us to teach ourselves, to train

321
00:16:51,480 --> 00:16:52,680
 ourselves.

322
00:16:52,680 --> 00:16:55,570
 When the mind wants to leap out to something you have to be

323
00:16:55,570 --> 00:16:56,820
 able to pull it back.

324
00:16:56,820 --> 00:16:59,960
 When the mind is lazy or so on you have to have ways of

325
00:16:59,960 --> 00:17:02,080
 encouraging it, for example,

326
00:17:02,080 --> 00:17:05,530
 contemplation of death, reminding yourself we've only got a

327
00:17:05,530 --> 00:17:06,920
 short time to live and so

328
00:17:06,920 --> 00:17:08,280
 on.

329
00:17:08,280 --> 00:17:11,040
 That's a really good teaching.

330
00:17:11,040 --> 00:17:12,480
 I'm not sure about Sammawayama.

331
00:17:12,480 --> 00:17:16,760
 I mean, obviously it can be interpreted in that way as from

332
00:17:16,760 --> 00:17:18,320
 time to time doing one or

333
00:17:18,320 --> 00:17:21,440
 the other.

334
00:17:21,440 --> 00:17:25,660
 But most important about Sammawayama is to see our effort

335
00:17:25,660 --> 00:17:27,880
 as comprehensive and also to

336
00:17:27,880 --> 00:17:32,280
 see effort as not being just push, push, push, right?

337
00:17:32,280 --> 00:17:37,000
 It's about balancing and it's really all about developing

338
00:17:37,000 --> 00:17:41,440
 mindfulness to overcome the defilement,

339
00:17:41,440 --> 00:17:42,880
 to have a wholesome state of mind.

340
00:17:42,880 --> 00:17:43,880
 1

341
00:17:43,880 --> 00:17:44,880
 1

342
00:17:44,880 --> 00:17:45,880
 1

