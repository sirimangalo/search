1
00:00:00,000 --> 00:00:16,580
 Today I'll continue on with the great blessings of the Lord

2
00:00:16,580 --> 00:00:19,280
 Buddha.

3
00:00:19,280 --> 00:00:28,170
 Sometimes it might seem a little bit odd to be talking

4
00:00:28,170 --> 00:00:31,520
 about such things.

5
00:00:31,520 --> 00:00:41,070
 Sometimes they're not directly related to the meditation

6
00:00:41,070 --> 00:00:42,680
 practice.

7
00:00:42,680 --> 00:00:54,720
 But we can understand these things as virtues and qualities

8
00:00:54,720 --> 00:00:57,720
 of mind which come from mental

9
00:00:57,720 --> 00:01:03,040
 development and can only come from mental development,

10
00:01:03,040 --> 00:01:05,760
 which we cannot simply fake or

11
00:01:05,760 --> 00:01:13,360
 pretend to possess.

12
00:01:13,360 --> 00:01:19,270
 But until we practice to develop ourselves on the path of

13
00:01:19,270 --> 00:01:22,400
 meditation and mindfulness,

14
00:01:22,400 --> 00:01:26,240
 it's not very likely that these things will truly exist in

15
00:01:26,240 --> 00:01:27,200
 our heart.

16
00:01:27,200 --> 00:01:34,580
 So we can take it that these are the goals of successful

17
00:01:34,580 --> 00:01:38,760
 insight meditation and we can

18
00:01:38,760 --> 00:01:44,360
 practice strenuously to achieve them.

19
00:01:44,360 --> 00:01:49,460
 So the next set of auspiciousness or blessings, good signs

20
00:01:49,460 --> 00:01:52,640
 which are a sign that we are practicing

21
00:01:52,640 --> 00:01:57,560
 correctly and have come to some sort of fruit in the

22
00:01:57,560 --> 00:02:02,800
 practice as follows in Pali.

23
00:02:02,800 --> 00:02:08,730
 "Kara wo jah niwato jah santuti jakatanyutta kalena tama sa

24
00:02:08,730 --> 00:02:26,880
vana nithama kalana tama"

25
00:02:26,880 --> 00:02:37,920
 "Kata nyutta gratitude kalena tama savana nithama"

26
00:02:37,920 --> 00:02:41,390
 Being able to hear the dhamma or hearing the dhamma,

27
00:02:41,390 --> 00:02:47,200
 hearing the truth at the right time.

28
00:02:47,200 --> 00:02:49,600
 These are great "etamangalamutamang".

29
00:02:49,600 --> 00:02:57,650
 These are great auspiciousness or great blessings and the

30
00:02:57,650 --> 00:02:59,360
 true sun.

31
00:02:59,360 --> 00:03:01,360
 Now in order.

32
00:03:01,360 --> 00:03:05,200
 "Kara wo karawa" means respect.

33
00:03:05,200 --> 00:03:12,470
 The Lord Buddha taught that all people would do well to pay

34
00:03:12,470 --> 00:03:15,560
 respect to six things.

35
00:03:15,560 --> 00:03:19,580
 So respect for these six things would bring great benefit

36
00:03:19,580 --> 00:03:21,640
 to their lives, would bring

37
00:03:21,640 --> 00:03:23,640
 great happiness to their minds.

38
00:03:23,640 --> 00:03:28,300
 This is respect for the Buddha, respect for the dhamma,

39
00:03:28,300 --> 00:03:31,080
 respect for the sangha, respect

40
00:03:31,080 --> 00:03:37,350
 for the training, respect for the "apamata" which we talked

41
00:03:37,350 --> 00:03:41,000
 about yesterday, being heatful

42
00:03:41,000 --> 00:03:52,360
 or mindful, and respect for hospitality.

43
00:03:52,360 --> 00:03:58,470
 Respect for the Buddha means respect for the being or any

44
00:03:58,470 --> 00:04:02,440
 being who has become enlightened

45
00:04:02,440 --> 00:04:04,600
 for themselves.

46
00:04:04,600 --> 00:04:11,400
 Being enlightened for oneself is not an easy thing to do.

47
00:04:11,400 --> 00:04:19,800
 It takes at least four asankaya, four uncountable aeons.

48
00:04:19,800 --> 00:04:25,720
 Sometimes eight, sometimes twenty, sometimes many.

49
00:04:25,720 --> 00:04:31,110
 The Lord Buddha Gotama, the Buddha we know took him four as

50
00:04:31,110 --> 00:04:34,120
ankaya and one hundred thousand

51
00:04:34,120 --> 00:04:39,000
 great aeons.

52
00:04:39,000 --> 00:04:40,280
 Great aeons.

53
00:04:40,280 --> 00:04:45,560
 And asankaya, four asankaya may not seem like much.

54
00:04:45,560 --> 00:04:49,990
 But when asankaya they say, asankaya means you cannot count

55
00:04:49,990 --> 00:04:51,160
 how long.

56
00:04:51,160 --> 00:04:54,320
 It's uncountable.

57
00:04:54,320 --> 00:05:00,480
 You can't count so many years, so many centuries, so many a

58
00:05:00,480 --> 00:05:01,440
eons.

59
00:05:01,440 --> 00:05:10,970
 But they have a simile that says, a hundred years of human

60
00:05:10,970 --> 00:05:13,760
 life, one day in the angel

61
00:05:13,760 --> 00:05:16,400
 world, in the heavens.

62
00:05:16,400 --> 00:05:22,410
 In the heavens, one day in heaven is like one hundred years

63
00:05:22,410 --> 00:05:24,720
 in the human realm.

64
00:05:24,720 --> 00:05:29,640
 Now suppose there were a large hole, someone dug a very

65
00:05:29,640 --> 00:05:32,720
 large hole or a canyon, you could

66
00:05:32,720 --> 00:05:40,680
 say a canyon that was seven miles long and seven miles wide

67
00:05:40,680 --> 00:05:44,080
 and seven miles deep.

68
00:05:44,080 --> 00:05:48,740
 And every one hundred angel years, where one day in the

69
00:05:48,740 --> 00:05:51,400
 angel world is a hundred human

70
00:05:51,400 --> 00:05:56,060
 years, every one hundred years in the angel world, an angel

71
00:05:56,060 --> 00:05:58,320
 were to come and drop a sesame

72
00:05:58,320 --> 00:06:06,040
 seed in this very large hole.

73
00:06:06,040 --> 00:06:10,990
 And every one hundred angel years another sesame seed,

74
00:06:10,990 --> 00:06:13,160
 another sesame seed.

75
00:06:13,160 --> 00:06:20,520
 Fill the hole, the entire hole was full of sesame seeds.

76
00:06:20,520 --> 00:06:27,150
 And then one by one they were to take the sesame seeds out

77
00:06:27,150 --> 00:06:28,360
 again.

78
00:06:28,360 --> 00:06:34,080
 This is the time it would take to fill the hole up and then

79
00:06:34,080 --> 00:06:36,680
 empty the hole again.

80
00:06:36,680 --> 00:06:41,020
 Every one hundred angel years, one sesame seed.

81
00:06:41,020 --> 00:06:47,790
 This is the amount of time, this task would be accomplished

82
00:06:47,790 --> 00:06:51,400
 before an asankaya was finished.

83
00:06:51,400 --> 00:06:57,900
 And the Lord Buddha had to take four periods, four uncount

84
00:06:57,900 --> 00:07:01,280
able periods of time to become

85
00:07:01,280 --> 00:07:02,880
 Buddha.

86
00:07:02,880 --> 00:07:08,640
 This is something worthy of great respect, great reference.

87
00:07:08,640 --> 00:07:13,750
 For all of us, maybe we, probably we don't have any such

88
00:07:13,750 --> 00:07:15,240
 aspiration.

89
00:07:15,240 --> 00:07:18,440
 To become a Buddha you have to know everything.

90
00:07:18,440 --> 00:07:22,140
 For all of us we are content with simply giving up

91
00:07:22,140 --> 00:07:23,440
 everything.

92
00:07:23,440 --> 00:07:26,120
 Lord Buddha also gave up everything but he also knew

93
00:07:26,120 --> 00:07:27,040
 everything.

94
00:07:27,040 --> 00:07:31,520
 If you want to know everything it takes four asankaya, it

95
00:07:31,520 --> 00:07:33,200
 takes a long time.

96
00:07:33,200 --> 00:07:39,360
 To simply give up everything doesn't take so long.

97
00:07:39,360 --> 00:07:42,640
 We started it here, we can come for 21 days and this is a

98
00:07:42,640 --> 00:07:43,280
 start.

99
00:07:43,280 --> 00:07:45,640
 A start in this life.

100
00:07:45,640 --> 00:07:47,640
 To all of ourselves.

101
00:07:47,640 --> 00:07:53,040
 This is respect for the Buddha, respect for his purity.

102
00:07:53,040 --> 00:07:57,400
 Lord Buddha was completely pure.

103
00:07:57,400 --> 00:08:03,190
 No greed, no anger, no delusion, not even a drop of

104
00:08:03,190 --> 00:08:05,280
 imperfection.

105
00:08:05,280 --> 00:08:07,280
 He had great wisdom.

106
00:08:07,280 --> 00:08:12,720
 They say you can't find an end to the Buddha's wisdom.

107
00:08:12,720 --> 00:08:15,480
 And he had great compassion.

108
00:08:15,480 --> 00:08:20,380
 But not only did he realize the Dhamma for himself but he

109
00:08:20,380 --> 00:08:23,080
 also taught other people.

110
00:08:23,080 --> 00:08:27,800
 When he invited to teach he didn't refuse to teach.

111
00:08:27,800 --> 00:08:33,390
 He never refused to try to give the right teaching not for

112
00:08:33,390 --> 00:08:36,280
 his own gain or because it

113
00:08:36,280 --> 00:08:41,360
 was a duty or so on.

114
00:08:41,360 --> 00:08:45,010
 Because he was invited to teach and because his heart was

115
00:08:45,010 --> 00:08:46,600
 full of compassion.

116
00:08:46,600 --> 00:08:53,560
 And someone asked him to teach he would never refuse.

117
00:08:53,560 --> 00:08:57,380
 Respect for the Dhamma, his respect for the teaching of the

118
00:08:57,380 --> 00:08:58,440
 Lord Buddha.

119
00:08:58,440 --> 00:09:01,240
 Even the Lord Buddha himself played respect to the Dhamma.

120
00:09:01,240 --> 00:09:08,490
 The Dhamma is those things which the Lord Buddha realized

121
00:09:08,490 --> 00:09:10,520
 for himself.

122
00:09:10,520 --> 00:09:13,290
 And the one thing the Lord Buddha paid respect to, he said,

123
00:09:13,290 --> 00:09:14,920
 "I couldn't pay respect to anyone

124
00:09:14,920 --> 00:09:16,600
 else in the world.

125
00:09:16,600 --> 00:09:18,880
 There's no one else as a Buddha.

126
00:09:18,880 --> 00:09:20,880
 None of these other people are Buddha.

127
00:09:20,880 --> 00:09:23,000
 So how could I respect to them?"

128
00:09:23,000 --> 00:09:28,500
 He said, "I will respect the Dhamma, the truth which I have

129
00:09:28,500 --> 00:09:29,880
 realized."

130
00:09:29,880 --> 00:09:35,520
 And so he also paid respect to the Dhamma.

131
00:09:35,520 --> 00:09:37,280
 The Dhamma is of two kinds.

132
00:09:37,280 --> 00:09:40,920
 We have Lokya Dhamma and Lokutkaradhamma.

133
00:09:40,920 --> 00:09:44,920
 Lokya Dhamma is things that we develop in the world.

134
00:09:44,920 --> 00:09:47,800
 The truth of the world.

135
00:09:47,800 --> 00:09:51,390
 Or those good things, those good qualities which we have to

136
00:09:51,390 --> 00:09:52,920
 develop in the world.

137
00:09:52,920 --> 00:09:57,400
 There are the four foundations of mindfulness.

138
00:09:57,400 --> 00:10:01,120
 Mindfulness of the postures of the body and so on.

139
00:10:01,120 --> 00:10:04,200
 Mindfulness of the feelings.

140
00:10:04,200 --> 00:10:10,880
 Pain and ache and so on.

141
00:10:10,880 --> 00:10:15,460
 Mindfulness of the mind, thinking about the past and future

142
00:10:15,460 --> 00:10:18,320
, good thinking and bad thinking.

143
00:10:18,320 --> 00:10:23,400
 And mindfulness of the Dhamma, the five hindrances, viking,

144
00:10:23,400 --> 00:10:25,560
 disliking, drowsing.

145
00:10:25,560 --> 00:10:26,560
 These are called Lokya Dhamma.

146
00:10:26,560 --> 00:10:31,480
 These are things which we develop in the world.

147
00:10:31,480 --> 00:10:36,630
 We come to see the truth of these things, the benefit of

148
00:10:36,630 --> 00:10:38,280
 these things.

149
00:10:38,280 --> 00:10:41,280
 We use these things to see the truth.

150
00:10:41,280 --> 00:10:44,080
 So they're called Dhamma.

151
00:10:44,080 --> 00:10:48,820
 Then we come to see impermanence.

152
00:10:48,820 --> 00:10:52,280
 Impermanence is a very important thing for us to see.

153
00:10:52,280 --> 00:10:56,120
 And we won't hold on to bad things or good things.

154
00:10:56,120 --> 00:10:59,120
 Sometimes something comes up and it's very bad and we want

155
00:10:59,120 --> 00:11:00,400
 to run away or we want to

156
00:11:00,400 --> 00:11:04,960
 change or we want to fix things.

157
00:11:04,960 --> 00:11:09,130
 We can't stand the way it is thinking if we don't do

158
00:11:09,130 --> 00:11:12,320
 something it might never change.

159
00:11:12,320 --> 00:11:17,710
 Impermanence means we can see that it's changing all the

160
00:11:17,710 --> 00:11:18,600
 time.

161
00:11:18,600 --> 00:11:21,260
 We come to let go of those things that we hold on to

162
00:11:21,260 --> 00:11:23,640
 because we see they're not permanent.

163
00:11:23,640 --> 00:11:26,640
 Even happy or good things.

164
00:11:26,640 --> 00:11:30,520
 We thought it was good so we try to get it again and again.

165
00:11:30,520 --> 00:11:37,040
 We don't see that it's always impermanent no matter what we

166
00:11:37,040 --> 00:11:37,800
 do.

167
00:11:37,800 --> 00:11:41,080
 We can never make it permanent.

168
00:11:41,080 --> 00:11:43,950
 Whatever good or happy things we have, we have to let go of

169
00:11:43,950 --> 00:11:44,800
 them as well.

170
00:11:44,800 --> 00:11:48,770
 We'll go back to the past or look to the future for good

171
00:11:48,770 --> 00:11:52,520
 things and let go of everything.

172
00:11:52,520 --> 00:11:58,000
 Then we'll be happy and then we'll be free.

173
00:11:58,000 --> 00:12:02,080
 And then we can see suffering.

174
00:12:02,080 --> 00:12:06,440
 Suffering is another very important thing.

175
00:12:06,440 --> 00:12:08,680
 Important to see where is suffering.

176
00:12:08,680 --> 00:12:13,040
 When we practice we will see suffering in places we didn't

177
00:12:13,040 --> 00:12:14,960
 think was suffering.

178
00:12:14,960 --> 00:12:18,360
 The ordinary state of a human being is they know they are

179
00:12:18,360 --> 00:12:19,400
 suffering.

180
00:12:19,400 --> 00:12:23,690
 They know there is pain and there is upset in their lives,

181
00:12:23,690 --> 00:12:25,600
 upset in their minds.

182
00:12:25,600 --> 00:12:32,120
 When they don't admit it to other people they still have

183
00:12:32,120 --> 00:12:33,040
 inside.

184
00:12:33,040 --> 00:12:38,840
 But they don't know what is the cause of suffering.

185
00:12:38,840 --> 00:12:43,200
 They don't know what really is, what really are those

186
00:12:43,200 --> 00:12:45,840
 things which are stressful.

187
00:12:45,840 --> 00:12:51,490
 They don't know the truth about everything that they hold

188
00:12:51,490 --> 00:12:53,840
 on to and attach to and want

189
00:12:53,840 --> 00:13:02,210
 and need and get upset about and worry about and fret about

190
00:13:02,210 --> 00:13:04,040
 and so on.

191
00:13:04,040 --> 00:13:07,200
 They don't see that these things are actually suffering.

192
00:13:07,200 --> 00:13:11,960
 When we attach, when we hold on, when we cling to anything

193
00:13:11,960 --> 00:13:14,440
 in the world, good or bad.

194
00:13:14,440 --> 00:13:16,080
 We can't see that this is suffering.

195
00:13:16,080 --> 00:13:18,440
 This is why we suffer.

196
00:13:18,440 --> 00:13:25,940
 We can't see these things are like a hot coal or a hot ball

197
00:13:25,940 --> 00:13:29,760
 of iron and it's something which

198
00:13:29,760 --> 00:13:31,760
 will cause suffering.

199
00:13:31,760 --> 00:13:33,760
 When we hold on to it it will cause us suffering.

200
00:13:33,760 --> 00:13:36,540
 If we don't hold on to anything there is no suffering in

201
00:13:36,540 --> 00:13:37,320
 the world.

202
00:13:37,320 --> 00:13:44,070
 If we don't hold on to anything we are happy among so many

203
00:13:44,070 --> 00:13:46,800
 miserable people.

204
00:13:46,800 --> 00:13:57,480
 We are at peace amongst so many unpeaceful people.

205
00:13:57,480 --> 00:14:04,120
 This is suffering and then non-self.

206
00:14:04,120 --> 00:14:11,740
 Non-self is when learning to see that in truth of reality

207
00:14:11,740 --> 00:14:15,880
 there is only body and mind.

208
00:14:15,880 --> 00:14:20,330
 Instead of ourselves or in other people as well or in the

209
00:14:20,330 --> 00:14:22,560
 whole world around us it's

210
00:14:22,560 --> 00:14:26,680
 only body and mind.

211
00:14:26,680 --> 00:14:32,100
 There's no him or her or they or them or us or we or good

212
00:14:32,100 --> 00:14:34,760
 or bad or self or soul or so

213
00:14:34,760 --> 00:14:35,760
 on.

214
00:14:35,760 --> 00:14:42,080
 All of these things are only concepts.

215
00:14:42,080 --> 00:14:45,630
 The purpose of seeing non-self is so that we don't hold on

216
00:14:45,630 --> 00:14:47,360
 to something as an entity,

217
00:14:47,360 --> 00:14:50,360
 as an object.

218
00:14:50,360 --> 00:14:54,280
 All the things that we are angry or upset about.

219
00:14:54,280 --> 00:14:58,280
 This person, that person, there's no person.

220
00:14:58,280 --> 00:15:03,650
 When we come to see that this is only seeing, hearing,

221
00:15:03,650 --> 00:15:07,480
 smelling, tasting, feeling, only

222
00:15:07,480 --> 00:15:14,150
 body and feelings and perception, memory, thoughts,

223
00:15:14,150 --> 00:15:17,520
 consciousness, all many pieces that

224
00:15:17,520 --> 00:15:21,240
 make up a whole.

225
00:15:21,240 --> 00:15:26,550
 None of these pieces are actually self or soul or me or

226
00:15:26,550 --> 00:15:27,480
 them.

227
00:15:27,480 --> 00:15:33,320
 We won't get angry at other people and see that the other

228
00:15:33,320 --> 00:15:36,240
 person is same as me, only

229
00:15:36,240 --> 00:15:40,940
 full of many things arising and ceasing or good things,

230
00:15:40,940 --> 00:15:45,840
 sometimes bad things sometimes.

231
00:15:45,840 --> 00:15:52,440
 We won't hold on to ourselves or other people.

232
00:15:52,440 --> 00:15:58,240
 Good things come, we also won't think of it as an entity.

233
00:15:58,240 --> 00:16:04,930
 We'll see it as things with many states arising and ceasing

234
00:16:04,930 --> 00:16:07,640
, coming and going.

235
00:16:07,640 --> 00:16:09,800
 Come to see that it's actually dissatisfying.

236
00:16:09,800 --> 00:16:10,800
 It's non-self.

237
00:16:10,800 --> 00:16:14,640
 It's not a real entity.

238
00:16:14,640 --> 00:16:18,280
 It's only made up of body and mind.

239
00:16:18,280 --> 00:16:24,570
 When we acknowledge seeing, hearing, smelling, tasting,

240
00:16:24,570 --> 00:16:27,720
 feeling and so on, when we acknowledge

241
00:16:27,720 --> 00:16:34,880
 every moment, every moment, we'll come to see non-self.

242
00:16:34,880 --> 00:16:38,920
 We'll come to see non-self in everything.

243
00:16:38,920 --> 00:16:44,740
 Nothing in the world is mine or me or belongs to me or is

244
00:16:44,740 --> 00:16:48,080
 self or belongs to self and so

245
00:16:48,080 --> 00:16:50,040
 on.

246
00:16:50,040 --> 00:16:51,200
 This is all lokiyatama.

247
00:16:51,200 --> 00:16:54,000
 This is the truth which we come to see in the world.

248
00:16:54,000 --> 00:16:59,130
 Lokutradhama is above the world, outside of the world,

249
00:16:59,130 --> 00:17:02,600
 outside of the universe, something

250
00:17:02,600 --> 00:17:08,640
 which exists outside of mundane existence.

251
00:17:08,640 --> 00:17:16,400
 This is the reality which they call nirvana or nirvana.

252
00:17:16,400 --> 00:17:22,560
 It's the attainment of cessation, of suffering.

253
00:17:22,560 --> 00:17:28,580
 When we give up attachment to the world and the mind flies

254
00:17:28,580 --> 00:17:31,880
 away, the mind is released,

255
00:17:31,880 --> 00:17:34,680
 even just for a moment.

256
00:17:34,680 --> 00:17:36,640
 We see nirvana even just for a moment.

257
00:17:36,640 --> 00:17:40,000
 This is great happiness, true happiness.

258
00:17:40,000 --> 00:17:47,920
 This is the dhamma, something which is worthy of respect.

259
00:17:47,920 --> 00:17:50,400
 Anyone who has seen nirvana, they continue to live their

260
00:17:50,400 --> 00:17:51,640
 life as a normal person.

