1
00:00:00,000 --> 00:00:27,220
 [

2
00:00:27,220 --> 00:00:30,220
 The Brahma-Viharas]

3
00:00:30,220 --> 00:00:40,220
 Tonight's quote is about the Brahma-Viharas.

4
00:00:40,220 --> 00:00:46,900
 The Brahma-Viharas, of course, are one of the more well-

5
00:00:46,900 --> 00:00:51,970
known Buddhist meditation practices besides inside

6
00:00:51,970 --> 00:00:53,220
 meditation.

7
00:00:53,220 --> 00:00:58,220
 Sometimes even more so than inside meditation.

8
00:00:58,220 --> 00:01:06,220
 People sometimes equate or associate Buddhism more with

9
00:01:06,220 --> 00:01:15,220
 compassion, love than they do with insight.

10
00:01:15,220 --> 00:01:19,750
 And for good reason there are forms of Buddhism that focus

11
00:01:19,750 --> 00:01:24,220
 more on the Brahma-Viharas, especially compassion.

12
00:01:24,220 --> 00:01:29,450
 Compassion is considered to be key to maya and the Buddhism

13
00:01:29,450 --> 00:01:33,220
 because it's key to becoming a Buddha.

14
00:01:33,220 --> 00:01:37,220
 Buddha doesn't just wish for their own enlightenment.

15
00:01:37,220 --> 00:01:46,260
 Because of their strong compassion they wish for all beings

16
00:01:46,260 --> 00:01:49,220
 to become alive.

17
00:01:49,220 --> 00:01:55,680
 But in Theravada Buddhism the four Brahma-Viharas are what

18
00:01:55,680 --> 00:01:59,220
 one might call a supportive meditation,

19
00:01:59,220 --> 00:02:03,220
 or auxiliary if you want to use a technical term.

20
00:02:03,220 --> 00:02:09,710
 Meaning they're not the main, but they support one's main

21
00:02:09,710 --> 00:02:11,220
 practice.

22
00:02:11,220 --> 00:02:14,220
 They support the practice of inside meditation.

23
00:02:14,220 --> 00:02:20,220
 They can also be used as a base to gain great concentration

24
00:02:20,220 --> 00:02:20,220
.

25
00:02:20,220 --> 00:02:26,430
 So you could practice metta on its own for some time to

26
00:02:26,430 --> 00:02:28,220
 focus the mind.

27
00:02:28,220 --> 00:02:32,750
 Karuna, mudita, veda, you could focus practice this for on

28
00:02:32,750 --> 00:02:34,220
 their own.

29
00:02:34,220 --> 00:02:39,020
 But the best use they have and the most common use is as a

30
00:02:39,020 --> 00:02:41,220
 supportive meditation.

31
00:02:41,220 --> 00:02:46,220
 Meaning they keep your mind from getting off track.

32
00:02:46,220 --> 00:02:57,120
 So if you're angry, you use metta helps you avoid being

33
00:02:57,120 --> 00:02:59,220
 angry at someone.

34
00:02:59,220 --> 00:03:05,210
 If you're cruel or irritable then compassion helps you

35
00:03:05,210 --> 00:03:07,220
 overcome that.

36
00:03:07,220 --> 00:03:18,220
 If you're jealous, stingy, then joy, sympathetic joy,

37
00:03:18,220 --> 00:03:27,300
 appreciation, what they call mudita, helps you overcome

38
00:03:27,300 --> 00:03:28,220
 that.

39
00:03:28,220 --> 00:03:33,800
 And if you're partial or attached, if you care too much

40
00:03:33,800 --> 00:03:38,500
 about others such that it makes you worried or anxious or

41
00:03:38,500 --> 00:03:39,220
 upset

42
00:03:39,220 --> 00:03:45,820
 when they are not as you expect them to be, then upekka

43
00:03:45,820 --> 00:03:50,220
 helps you with that equanimity.

44
00:03:50,220 --> 00:03:55,290
 But these are part of a larger set of four meditations that

45
00:03:55,290 --> 00:04:01,220
 are called the arakaka-madhana, the protective meditations.

46
00:04:01,220 --> 00:04:05,220
 They protect your practice.

47
00:04:05,220 --> 00:04:12,070
 And the four are buddha-nusati, asubha-nusati, metta-nusati

48
00:04:12,070 --> 00:04:14,220
 and marana-nusati.

49
00:04:14,220 --> 00:04:17,220
 Maybe not in that order.

50
00:04:17,220 --> 00:04:22,220
 Buddha-nusati, mindfulness of the buddha.

51
00:04:22,220 --> 00:04:25,220
 Thinking about the buddha or reflecting on the buddha.

52
00:04:25,220 --> 00:04:28,220
 This is a good supportive meditation.

53
00:04:28,220 --> 00:04:36,690
 These four are like when you're growing a small tree, you

54
00:04:36,690 --> 00:04:39,220
 use wooden supports to keep it up.

55
00:04:39,220 --> 00:04:42,220
 Otherwise it gets blown over in the wind.

56
00:04:42,220 --> 00:04:50,150
 So this support, it's not essential, but it's not the tree

57
00:04:50,150 --> 00:04:51,220
 itself.

58
00:04:51,220 --> 00:04:55,220
 But it keeps the tree from dying, from being destroyed,

59
00:04:55,220 --> 00:04:58,220
 from falling.

60
00:04:58,220 --> 00:05:02,220
 Same way these meditations help our main meditation.

61
00:05:02,220 --> 00:05:06,190
 So buddha-nusati, thinking about the buddha helps us

62
00:05:06,190 --> 00:05:08,220
 cultivate confidence,

63
00:05:08,220 --> 00:05:14,220
 reassurance, when we feel some kind of

64
00:05:14,220 --> 00:05:18,220
 unsure about ourselves or ability, gives us confidence.

65
00:05:18,220 --> 00:05:21,840
 We think about the buddha, what he had to do, what he went

66
00:05:21,840 --> 00:05:23,220
 through as an example.

67
00:05:23,220 --> 00:05:26,220
 Having role models, having an example.

68
00:05:26,220 --> 00:05:28,550
 This is why it's great to be in the meditation center, you

69
00:05:28,550 --> 00:05:30,220
 see other people meditating.

70
00:05:30,220 --> 00:05:34,220
 It makes you want to meditate.

71
00:05:34,220 --> 00:05:38,220
 Today we have two new meditators coming a couple of days

72
00:05:38,220 --> 00:05:39,220
 after I arrived.

73
00:05:39,220 --> 00:05:42,220
 We have Kori and Michelle.

74
00:05:42,220 --> 00:05:45,220
 Michelle is an old meditator.

75
00:05:45,220 --> 00:05:50,220
 She's been meditating with me since she was five.

76
00:05:50,220 --> 00:05:54,220
 Many, many years ago her mother brought her and her sister

77
00:05:54,220 --> 00:05:58,220
 to meditate here in Canada.

78
00:05:58,220 --> 00:06:02,330
 Not with me, with another teacher, but we meditated

79
00:06:02,330 --> 00:06:03,220
 together.

80
00:06:03,220 --> 00:06:10,190
 Kori has been following me on the internet, but we've just

81
00:06:10,190 --> 00:06:13,220
 met for the first time.

82
00:06:13,220 --> 00:06:17,220
 So yes, thinking about the buddha is always one way of

83
00:06:17,220 --> 00:06:18,220
 accomplishing this.

84
00:06:18,220 --> 00:06:23,670
 When you have a buddha image and you reflect on his great

85
00:06:23,670 --> 00:06:27,220
 qualities that you're aspiring to, to some extent.

86
00:06:27,220 --> 00:06:31,630
 Buddha and the siddhi. So you sit there and say, "Buddho,

87
00:06:31,630 --> 00:06:33,220
 buddho," thinking about the buddha.

88
00:06:33,220 --> 00:06:38,220
 "Arahang, arahang," all the qualities of the buddha.

89
00:06:38,220 --> 00:06:42,220
 The second one is asubha and the siddhi.

90
00:06:42,220 --> 00:06:45,220
 Asubha means not beautiful.

91
00:06:45,220 --> 00:06:51,290
 So asubha is reflecting on the not beautiful aspects of the

92
00:06:51,290 --> 00:06:52,220
 body.

93
00:06:52,220 --> 00:06:57,220
 And this one helps us in times when we have great lust.

94
00:06:57,220 --> 00:07:01,060
 We have great lust for the body, for sexual desire, that

95
00:07:01,060 --> 00:07:02,220
 kind of thing.

96
00:07:02,220 --> 00:07:04,220
 Asubha helps you overcome that.

97
00:07:04,220 --> 00:07:07,920
 As you look at the body, not saying, "This is awful, this

98
00:07:07,920 --> 00:07:09,220
 is disgusting."

99
00:07:09,220 --> 00:07:13,830
 But you start to see how disgusting it is, how unpleasant

100
00:07:13,830 --> 00:07:15,220
 it actually is,

101
00:07:15,220 --> 00:07:20,220
 how there's nothing in here that is actually proper.

102
00:07:20,220 --> 00:07:24,540
 The desire for the human body is an illusion. It's based on

103
00:07:24,540 --> 00:07:27,220
 the illusion of beauty.

104
00:07:27,220 --> 00:07:29,220
 There's nothing beautiful.

105
00:07:29,220 --> 00:07:31,600
 So you take the body apart into pieces, as well as the hair

106
00:07:31,600 --> 00:07:32,220
 beautiful.

107
00:07:32,220 --> 00:07:34,980
 Well, of course, hair is beautiful. So then you meditate on

108
00:07:34,980 --> 00:07:35,220
 it.

109
00:07:35,220 --> 00:07:38,220
 You say, "Hair, hair, hair."

110
00:07:38,220 --> 00:07:41,890
 And you study about it and you think about it and you start

111
00:07:41,890 --> 00:07:43,220
 to see that hair is actually quite disgusting.

112
00:07:43,220 --> 00:07:48,560
 It's greasy and smelly and it's feeding off of the blood

113
00:07:48,560 --> 00:07:51,220
 and the oil of the scalp.

114
00:07:51,220 --> 00:07:54,220
 It's stuck in the scalp like rice.

115
00:07:54,220 --> 00:07:57,810
 Those of us studying the Visuddhi Maga, those of you

116
00:07:57,810 --> 00:07:59,220
 studying it with me,

117
00:07:59,220 --> 00:08:03,220
 remember the description of the 32 parts of the body.

118
00:08:03,220 --> 00:08:07,220
 It's quite vivid.

119
00:08:07,220 --> 00:08:10,840
 When you go through the parts of the body, the hair, the

120
00:08:10,840 --> 00:08:12,220
 skin, the nails, the teeth,

121
00:08:12,220 --> 00:08:19,220
 the hair, the body hair, the nails, the teeth, the skin,

122
00:08:19,220 --> 00:08:25,220
 and all the inner parts as well, the flesh and the blood.

123
00:08:25,220 --> 00:08:36,220
 So you just repeat to yourself, "Hair, hair, skin, skin."

124
00:08:36,220 --> 00:08:43,220
 The third meditation is Mita, of these four Bhagma Mihars.

125
00:08:43,220 --> 00:08:48,220
 Mita is singled out as useful.

126
00:08:48,220 --> 00:08:51,220
 Mita helps us overcome anger.

127
00:08:51,220 --> 00:08:56,200
 As I said, when we're upset at someone, when we have some

128
00:08:56,200 --> 00:08:58,220
 kind of conflict with another person.

129
00:08:58,220 --> 00:09:04,390
 It's a great source of stress and hindrance in our practice

130
00:09:04,390 --> 00:09:06,220
, obviously.

131
00:09:06,220 --> 00:09:08,860
 So of course being mindful is the best way to cure all of

132
00:09:08,860 --> 00:09:09,220
 these,

133
00:09:09,220 --> 00:09:12,500
 but when they're extreme and when we're just not able to

134
00:09:12,500 --> 00:09:13,220
 shake them,

135
00:09:13,220 --> 00:09:16,220
 it gives us a little boost.

136
00:09:16,220 --> 00:09:20,110
 Practicing, in this case, love gives us a boost to help us

137
00:09:20,110 --> 00:09:21,220
 deal with it.

138
00:09:21,220 --> 00:09:23,220
 So you say, "May that person be happy."

139
00:09:23,220 --> 00:09:27,220
 Just as I want to be happy, may they be happy as well.

140
00:09:27,220 --> 00:09:33,220
 May they be free from suffering, etc.

141
00:09:33,220 --> 00:09:37,220
 And the fourth one is Marana Nisati.

142
00:09:37,220 --> 00:09:41,920
 So Marana is for when you're lazy, which is why the Buddha

143
00:09:41,920 --> 00:09:44,220
 said it's good to meditate on death daily.

144
00:09:44,220 --> 00:09:46,220
 Every day think about death.

145
00:09:46,220 --> 00:09:50,220
 In fact, you should all the time be thinking about death.

146
00:09:50,220 --> 00:09:53,220
 And in fact, we passana is a way of thinking about death.

147
00:09:53,220 --> 00:09:56,220
 So in an ultimate sense, we're born and die every moment.

148
00:09:56,220 --> 00:10:00,800
 So we passana is seeing us die, seeing them death of the

149
00:10:00,800 --> 00:10:01,220
 being,

150
00:10:01,220 --> 00:10:09,630
 and realizing that there is no real stability or there's no

151
00:10:09,630 --> 00:10:14,220
 constant entity.

152
00:10:14,220 --> 00:10:18,220
 But so in general, we think about our death.

153
00:10:18,220 --> 00:10:20,220
 We think about the physical death.

154
00:10:20,220 --> 00:10:25,080
 "This life is uncertain. I'm going to die one day. That is

155
00:10:25,080 --> 00:10:26,220
 certain."

156
00:10:26,220 --> 00:10:30,220
 "Aduangmi ji-vitang duangmarana."

157
00:10:30,220 --> 00:10:34,220
 "Ji-vitanganiya-tangmi maranangniya-tang."

158
00:10:34,220 --> 00:10:38,220
 "Life is uncertain. Death is certain."

159
00:10:38,220 --> 00:10:45,230
 Death is the end of life for every being that comes to us

160
00:10:45,230 --> 00:10:46,220
 all.

161
00:10:46,220 --> 00:10:50,220
 Thinking about that helps give you a sense of urgency,

162
00:10:50,220 --> 00:10:57,220
 because when you realize that if you die unprepared,

163
00:10:57,220 --> 00:11:01,220
 it has a great impact on what comes next.

164
00:11:01,220 --> 00:11:06,220
 Your clarity of mind, your purity of mind.

165
00:11:06,220 --> 00:11:09,900
 Death is the end of everything, all these things that I

166
00:11:09,900 --> 00:11:13,220
 depend upon, that I care for.

167
00:11:13,220 --> 00:11:18,220
 I can't take them with me when I die.

168
00:11:18,220 --> 00:11:20,530
 So I mean that probably only works for people who believe

169
00:11:20,530 --> 00:11:22,220
 in an afterlife.

170
00:11:22,220 --> 00:11:25,220
 But even still, if you think this is it,

171
00:11:25,220 --> 00:11:30,220
 then you might as well make the best of it, the most of it.

172
00:11:30,220 --> 00:11:33,220
 You think that when we die it's death,

173
00:11:33,220 --> 00:11:37,220
 and still, best to become the best person you can.

174
00:11:37,220 --> 00:11:41,220
 Do the best you can if this is the only chance you've got.

175
00:11:41,220 --> 00:11:47,220
 It just seems sensible.

176
00:11:47,220 --> 00:11:49,220
 Anyway, not too much to talk about there.

177
00:11:49,220 --> 00:11:53,220
 You can read through the vibhanga in this case.

178
00:11:53,220 --> 00:11:56,220
 The vibhanga is just the part of the abhidhamma,

179
00:11:56,220 --> 00:12:00,220
 talks about the four brahma-liyas.

180
00:12:00,220 --> 00:12:01,930
 It's also in the vishuddhi manga, there's a really good

181
00:12:01,930 --> 00:12:04,220
 section on the four brahma-liyas,

182
00:12:04,220 --> 00:12:10,220
 based on the vibhanga, based on the abhidhamma.

183
00:12:10,220 --> 00:12:12,220
 So questions.

184
00:12:12,220 --> 00:12:14,220
 Fernando asks, "How was the meeting with my teacher?"

185
00:12:14,220 --> 00:12:18,220
 It was brief. He's very busy.

186
00:12:18,220 --> 00:12:21,220
 But we caught him, as we always do.

187
00:12:21,220 --> 00:12:25,350
 And took some pictures, though I haven't gotten any of the

188
00:12:25,350 --> 00:12:26,220
 pictures yet.

189
00:12:26,220 --> 00:12:29,220
 And gave him a whole bunch of robes again,

190
00:12:29,220 --> 00:12:35,220
 30-some robe sets for him to give away to others.

191
00:12:35,220 --> 00:12:41,270
 And he expressed his appreciation more for the fact that I

192
00:12:41,270 --> 00:12:43,220
 was going to Sri Lanka than anything, curiously.

193
00:12:43,220 --> 00:12:49,220
 He likes the idea of helping out Sri Lankan Buddhism.

194
00:12:49,220 --> 00:12:52,580
 It was kind of just something that piqued his interest and

195
00:12:52,580 --> 00:12:53,220
 has before,

196
00:12:53,220 --> 00:12:58,010
 because he's been to Sri Lanka, and because of the, I guess

197
00:12:58,010 --> 00:13:00,220
, a sense of gratitude is maybe the right thing to say,

198
00:13:00,220 --> 00:13:06,940
 or just a sense of appreciation for Sri Lanka, and sort of

199
00:13:06,940 --> 00:13:14,220
 a responsibility to care for Buddhism there as well.

200
00:13:14,220 --> 00:13:20,220
 So he was happy to hear that I was going to teach there.

201
00:13:20,220 --> 00:13:24,010
 Didn't say too much. He was expressed appreciation for the

202
00:13:24,010 --> 00:13:27,220
 new meditation center, but he already knew about it.

203
00:13:27,220 --> 00:13:36,220
 So it wasn't all that much talk.

204
00:13:36,220 --> 00:13:41,850
 Honestly, going to Asia was not probably all it was meant

205
00:13:41,850 --> 00:13:44,220
 to be or cut out to be.

206
00:13:44,220 --> 00:13:48,740
 I mean, as I said before, I wasn't totally sold on the idea

207
00:13:48,740 --> 00:13:51,220
 of going in the first place.

208
00:13:51,220 --> 00:13:58,500
 So maybe I downplayed my expectations, but it was just

209
00:13:58,500 --> 00:14:01,220
 another trip.

210
00:14:01,220 --> 00:14:04,920
 Teaching in Sri Lanka was nice. We had one really good

211
00:14:04,920 --> 00:14:08,220
 session at a meditation center there.

212
00:14:08,220 --> 00:14:11,370
 A whole bunch of people came out, and they were surprised

213
00:14:11,370 --> 00:14:12,220
 because I'm not there,

214
00:14:12,220 --> 00:14:15,830
 and because they don't know who I am really, they're

215
00:14:15,830 --> 00:14:18,220
 surprised at the things I say.

216
00:14:18,220 --> 00:14:24,220
 Sort of shocked at the defense of this meditation technique

217
00:14:24,220 --> 00:14:27,220
, because a lot of them have turned against it.

218
00:14:27,220 --> 00:14:34,220
 It's become sort of a rebel meditation, I guess.

219
00:14:34,220 --> 00:14:38,020
 People believing that Anapanas, that the mindfulness of the

220
00:14:38,020 --> 00:14:41,550
 breath of the nose, is the most orthodox meditation

221
00:14:41,550 --> 00:14:42,220
 practice.

222
00:14:42,220 --> 00:14:48,130
 And so they concentrate on that. So having to describe this

223
00:14:48,130 --> 00:14:50,220
 meditation, wherever I went, I said,

224
00:14:50,220 --> 00:14:53,550
 "Do you know who I am? Are you sure you want me to come and

225
00:14:53,550 --> 00:14:54,220
 talk?"

226
00:14:54,220 --> 00:15:00,220
 Because of course, last time I went, I had these problems.

227
00:15:00,220 --> 00:15:04,290
 I would go places, and then they would be surprised at what

228
00:15:04,290 --> 00:15:07,220
 I was saying and not in a happy way.

229
00:15:07,220 --> 00:15:11,640
 There was one place the translator actually refused to

230
00:15:11,640 --> 00:15:15,220
 translate because he didn't agree with what I said.

231
00:15:15,220 --> 00:15:21,220
 That was pretty awful.

232
00:15:21,220 --> 00:15:25,220
 Thank you all for meditating. It's good to see green here.

233
00:15:25,220 --> 00:15:28,220
 Green means that you guys have actually been meditating.

234
00:15:28,220 --> 00:15:32,420
 I mean, yes, logging your meditation is not necessary. It's

235
00:15:32,420 --> 00:15:34,220
 not like it actually means anything.

236
00:15:34,220 --> 00:15:37,220
 No, you don't have to do it because it makes me feel good.

237
00:15:37,220 --> 00:15:44,670
 But if you don't, then we start to get, I mean, as I said

238
00:15:44,670 --> 00:15:50,220
 before, it gives me an excuse to weed out sort of

239
00:15:50,220 --> 00:15:53,220
 speculative questions.

240
00:15:53,220 --> 00:15:55,220
 Because I can say, "Ah, you're an orange."

241
00:15:55,220 --> 00:15:57,860
 So newcomers show up, they don't meditate, and they start

242
00:15:57,860 --> 00:15:59,220
 asking crazy questions.

243
00:15:59,220 --> 00:16:04,220
 You can sort of dismiss them.

244
00:16:04,220 --> 00:16:09,100
 And it would focus our questions because we've been med

245
00:16:09,100 --> 00:16:10,220
itating.

246
00:16:10,220 --> 00:16:13,580
 I don't mind if you were meditating earlier in the day and

247
00:16:13,580 --> 00:16:21,220
 you just come and click on it later, like click on it now.

248
00:16:21,220 --> 00:16:24,520
 I suppose it's not ideal, but ideal is we do some

249
00:16:24,520 --> 00:16:26,220
 meditation before.

250
00:16:26,220 --> 00:16:29,120
 I mean, I guess the biggest thing is here we're coming

251
00:16:29,120 --> 00:16:31,220
 together to talk about the Dhamma.

252
00:16:31,220 --> 00:16:33,220
 So before we do that, we should meditate.

253
00:16:33,220 --> 00:16:36,370
 So if you're saying, "Well, I'm not meditating right before

254
00:16:36,370 --> 00:16:38,220
, but I'm meditating earlier."

255
00:16:38,220 --> 00:16:43,310
 Part of the point is to meditate just before we talk so

256
00:16:43,310 --> 00:16:47,220
 that our minds are in the right place.

257
00:16:47,220 --> 00:16:49,220
 I think that's reasonable.

258
00:16:49,220 --> 00:16:53,070
 But as far as logging it, I mean, it allows me to make sure

259
00:16:53,070 --> 00:16:54,220
 that...

260
00:16:54,220 --> 00:16:59,230
 I mean, the big point of this site is to provide

261
00:16:59,230 --> 00:17:01,220
 encouragement.

262
00:17:01,220 --> 00:17:04,270
 If you don't find that this is encouraging you to meditate,

263
00:17:04,270 --> 00:17:06,890
 the fact that you've got to actually log it and commit to

264
00:17:06,890 --> 00:17:09,220
 it and don't use it by all means.

265
00:17:09,220 --> 00:17:12,560
 But the whole point here was to encourage people to med

266
00:17:12,560 --> 00:17:13,220
itate.

267
00:17:13,220 --> 00:17:17,350
 I think some people do find that the site helps them, I

268
00:17:17,350 --> 00:17:18,220
 assume.

269
00:17:18,220 --> 00:17:29,220
 If not, well, you can just do away with it.

270
00:17:29,220 --> 00:17:31,220
 So no other questions?

271
00:17:31,220 --> 00:17:34,840
 For those of you joining us on YouTube and last night as

272
00:17:34,840 --> 00:17:37,750
 well, the questions and stuff is going on at our own

273
00:17:37,750 --> 00:17:42,220
 website at meditation.siri-mangamo.org.

274
00:17:42,220 --> 00:17:46,630
 Usually someone's kind enough to put that in the comments

275
00:17:46,630 --> 00:17:49,220
 section of the YouTube video.

276
00:17:49,220 --> 00:17:57,220
 We broadcast hopefully every night, usually every night.

277
00:17:57,220 --> 00:18:00,220
 But I guess there's no questions tonight.

278
00:18:00,220 --> 00:18:08,220
 I know I'm just recently back.

279
00:18:08,220 --> 00:18:14,220
 Alright then, have a good night everyone.

280
00:18:14,220 --> 00:18:18,220
 See you all tomorrow.

