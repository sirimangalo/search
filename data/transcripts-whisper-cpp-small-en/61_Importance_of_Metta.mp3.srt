1
00:00:00,000 --> 00:00:02,000
 Importance of Metta

2
00:00:02,000 --> 00:00:05,500
 How important is Metta meditation?

3
00:00:05,500 --> 00:00:08,000
 It is not something I have done often,

4
00:00:08,000 --> 00:00:12,000
 and I am not even certain I know how to do it correctly.

5
00:00:12,000 --> 00:00:17,210
 If you want a really good discussion or explanation of all

6
00:00:17,210 --> 00:00:19,000
 four of the Brahma Viharas,

7
00:00:19,000 --> 00:00:23,000
 I would recommend a book authored by Mahasi Sayadaw called

8
00:00:23,000 --> 00:00:24,500
 Brahma Viharadhamma.

9
00:00:24,500 --> 00:00:28,340
 It is quite extensive and gives detailed instructions on

10
00:00:28,340 --> 00:00:30,500
 how to develop loving-kindness.

11
00:00:30,500 --> 00:00:33,500
 There are two ways of understanding Metta.

12
00:00:33,500 --> 00:00:36,500
 You can use it as a support for your meditation,

13
00:00:36,500 --> 00:00:40,020
 or you can use it to develop Samatha, Tranquility

14
00:00:40,020 --> 00:00:40,500
 Meditation,

15
00:00:40,500 --> 00:00:43,500
 which can lead to the first three Gnanas.

16
00:00:43,500 --> 00:00:46,000
 If you want to use Metta to develop Samatha,

17
00:00:46,000 --> 00:00:49,300
 this should be done with a qualified teacher who can help

18
00:00:49,300 --> 00:00:51,000
 lead you to the Gnanas.

19
00:00:51,000 --> 00:00:55,000
 We often practice Metta after Vipassana meditation

20
00:00:55,000 --> 00:00:58,740
 as a means of extending the wholesome states of mind

21
00:00:58,740 --> 00:01:01,000
 cultivated during Vipassana practice.

22
00:01:01,000 --> 00:01:05,150
 You can also practice Metta before Vipassana practice to

23
00:01:05,150 --> 00:01:07,000
 clear up anger or aversion.

24
00:01:07,000 --> 00:01:10,500
 You can also practice it during Vipassana practice.

25
00:01:10,500 --> 00:01:13,620
 When you are overwhelmed with anger, you can use Metta to

26
00:01:13,620 --> 00:01:15,500
 help set your mind straight.

27
00:01:15,500 --> 00:01:18,900
 Metta helps with straightening out your mind because anger

28
00:01:18,900 --> 00:01:21,500
 and hate are based on misunderstanding.

29
00:01:21,500 --> 00:01:24,330
 Metta helps remind you that relating to people with

30
00:01:24,330 --> 00:01:25,500
 aversion is wrong.

31
00:01:25,500 --> 00:01:29,500
 Metta practice leads you to not just to give up the anger,

32
00:01:29,500 --> 00:01:33,060
 but to give up the delusion that says, "This person is bad

33
00:01:33,060 --> 00:01:33,500
."

34
00:01:33,500 --> 00:01:37,330
 When you send Metta, you develop the right view that it is

35
00:01:37,330 --> 00:01:39,500
 proper for all beings to be happy.

36
00:01:39,500 --> 00:01:42,410
 So you are not just developing love, but you are also

37
00:01:42,410 --> 00:01:44,000
 developing right view.

38
00:01:44,000 --> 00:01:47,180
 It is similar to how focusing on the various parts of the

39
00:01:47,180 --> 00:01:50,500
 body does not just lead to giving up bodily lust,

40
00:01:50,500 --> 00:01:54,040
 but it also leads to giving up the wrong view that this

41
00:01:54,040 --> 00:01:55,500
 body is beautiful.

