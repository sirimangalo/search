1
00:00:00,000 --> 00:00:05,000
 Hello, welcome back to Ask a Monk. Today some quick

2
00:00:05,000 --> 00:00:07,000
 questions, relatively easy to answer.

3
00:00:07,000 --> 00:00:13,180
 The first one is in regards to monks names because it's

4
00:00:13,180 --> 00:00:15,600
 clearly observable that Buddhist

5
00:00:15,600 --> 00:00:21,630
 monks have different names, names that were obviously taken

6
00:00:21,630 --> 00:00:23,920
 when they ordained. The origin

7
00:00:25,680 --> 00:00:32,680
 of this is not really clear to me. I think what I would

8
00:00:32,680 --> 00:00:32,680
 guess is that it's sort of progressed

9
00:00:32,680 --> 00:00:40,860
 as time went on. What we can clearly see is in the Buddha's

10
00:00:40,860 --> 00:00:44,240
 time, most people are dating

11
00:00:44,240 --> 00:00:48,770
 as monks, kept their original name, and there were

12
00:00:48,770 --> 00:00:51,520
 exceptions to this, but it seems those

13
00:00:53,280 --> 00:00:57,980
 exceptions were exceptional, that for the most part people

14
00:00:57,980 --> 00:01:00,280
 kept their original names.

15
00:01:00,280 --> 00:01:06,100
 And that was no problem because the names were already in

16
00:01:06,100 --> 00:01:08,760
 Pali's, and this was the language

17
00:01:08,760 --> 00:01:14,520
 that they were speaking. So when they did the ordination

18
00:01:14,520 --> 00:01:16,540
 ceremony, there was never any

19
00:01:16,540 --> 00:01:20,120
 thought that the name would have to fit the language of the

20
00:01:20,120 --> 00:01:22,000
, not the ceremony, but the

21
00:01:22,000 --> 00:01:27,050
 act, the formal act of ordination. The problem was that

22
00:01:27,050 --> 00:01:29,000
 this act became formalized, or it

23
00:01:29,000 --> 00:01:34,160
 was set down as a formal act that had to use a specific

24
00:01:34,160 --> 00:01:36,000
 language. And so when the Buddha's

25
00:01:36,000 --> 00:01:41,240
 teaching traveled to other places, they still used this old

26
00:01:41,240 --> 00:01:44,440
 language. And so I would assume

27
00:01:46,120 --> 00:01:50,770
 that that is where the need to change people's names to Pal

28
00:01:50,770 --> 00:01:53,120
i, or to give them Pali names arose.

29
00:01:53,120 --> 00:02:00,230
 And so how that worked exactly, I can't guess, and I'm sure

30
00:02:00,230 --> 00:02:03,440
 there are people who are more

31
00:02:03,440 --> 00:02:07,960
 knowledgeable in this than I am, but how it works today

32
00:02:07,960 --> 00:02:10,400
 depends very much on where you

33
00:02:10,400 --> 00:02:14,940
 are. Probably the most reasonable method of choosing a

34
00:02:14,940 --> 00:02:17,400
 Buddhist monk's name is based on

35
00:02:17,400 --> 00:02:23,180
 the person's name and whatever language that they are,

36
00:02:23,180 --> 00:02:26,640
 where you simply change it to fit

37
00:02:26,640 --> 00:02:32,420
 the Pali language. And you do see that to some extent, at

38
00:02:32,420 --> 00:02:33,640
 least insofar as they try

39
00:02:34,120 --> 00:02:38,470
 to find Pali words that fit with the person's name. So I

40
00:02:38,470 --> 00:02:41,120
 can't remember some examples, but

41
00:02:41,120 --> 00:02:46,380
 I remember trying to figure out what was a good Pali name

42
00:02:46,380 --> 00:02:49,680
 that sounded like, or Pali words,

43
00:02:49,680 --> 00:02:56,560
 a term that sounded like a person's name. So, but how it

44
00:02:56,560 --> 00:02:56,680
 works a lot of the time is that

45
00:03:00,760 --> 00:03:04,990
 they will pick a name that has some meaning, and it could

46
00:03:04,990 --> 00:03:07,760
 be a fairly pretentious or elevated

47
00:03:07,760 --> 00:03:17,950
 sort of name, and that's what you find in Thailand for the

48
00:03:17,950 --> 00:03:20,840
 most part. Or for the greater

49
00:03:20,840 --> 00:03:25,340
 part you'll find them picking names that sound good, or

50
00:03:25,340 --> 00:03:27,840
 that have some sort of meaning.

51
00:03:27,840 --> 00:03:32,530
 So my name, Yutadhamu, or however you want to say it, means

52
00:03:32,530 --> 00:03:34,840
 according to the Thai books,

53
00:03:34,840 --> 00:03:46,310
 one who has the Dhamma composed. And it's a difficult one

54
00:03:46,310 --> 00:03:52,800
 to translate, the translation

55
00:03:53,400 --> 00:03:57,660
 loses something. But the meaning is one who has mastered

56
00:03:57,660 --> 00:04:00,080
 the teaching, or so on, or has,

57
00:04:00,080 --> 00:04:03,190
 if not mastered, but has a good grasp of the Buddha's

58
00:04:03,190 --> 00:04:05,240
 teaching, or of the Dhamma, or of

59
00:04:05,240 --> 00:04:08,970
 truth, or of reality. Now when I asked my teacher about the

60
00:04:08,970 --> 00:04:11,440
 name, he said, he changed

61
00:04:11,440 --> 00:04:15,930
 it a little, he said, it means someone who is composed of D

62
00:04:15,930 --> 00:04:18,440
hamma, who is made up of Dhamma,

63
00:04:18,520 --> 00:04:23,250
 which I like a lot, first of all, because it's less ostent

64
00:04:23,250 --> 00:04:25,520
atious, but also because it's

65
00:04:25,520 --> 00:04:30,820
 true. We're all made up of realities, and it's a reminder

66
00:04:30,820 --> 00:04:33,080
 to look inside to find the

67
00:04:33,080 --> 00:04:37,950
 truth, that simply coming to understand ourselves is the

68
00:04:37,950 --> 00:04:40,480
 understanding of reality.

69
00:04:41,640 --> 00:04:45,830
 Another way that people choose the names is based on the

70
00:04:45,830 --> 00:04:48,640
 teacher. So I know when my teacher

71
00:04:48,640 --> 00:04:54,140
 would pick names for nuns, for example, who came and asked

72
00:04:54,140 --> 00:04:56,480
 him for a name, because for

73
00:04:56,480 --> 00:04:59,410
 monks it was often taken in a fairly formal way, and they

74
00:04:59,410 --> 00:05:00,960
 picked it out of a book based

75
00:05:00,960 --> 00:05:05,220
 on, oh that's right, my name was based on this formal

76
00:05:05,220 --> 00:05:07,960
 system of the day you were born.

77
00:05:08,480 --> 00:05:10,880
 That day of the week you were born on, they have a book,

78
00:05:10,880 --> 00:05:12,400
 and they know which letters are

79
00:05:12,400 --> 00:05:15,830
 going to go well with that day, and it's based on astrology

80
00:05:15,830 --> 00:05:17,880
 and numerology or whatever, I

81
00:05:17,880 --> 00:05:22,670
 don't know, superstition really. So my name is a name for a

82
00:05:22,670 --> 00:05:24,880
 person who was born on a Wednesday

83
00:05:24,880 --> 00:05:28,890
 night, which I wasn't actually, they got it wrong, I was

84
00:05:28,890 --> 00:05:31,080
 born on a Wednesday morning,

85
00:05:31,080 --> 00:05:36,910
 which would have given me another name, but that's another

86
00:05:36,910 --> 00:05:37,920
 story.

87
00:05:37,920 --> 00:05:40,260
 The other way that they do it, which is also quite

88
00:05:40,260 --> 00:05:42,280
 reasonable I think, is to give it based

89
00:05:42,280 --> 00:05:46,200
 on one's teacher, and you'll find that here in Sri Lanka as

90
00:05:46,200 --> 00:05:48,960
 well I think. So if my name

91
00:05:48,960 --> 00:05:52,530
 is Yuta Dhamma, I might give all my students names that

92
00:05:52,530 --> 00:05:54,680
 have the word Yuta in them or the

93
00:05:54,680 --> 00:05:58,720
 word Dhamma in them to sort of put my mark or my brand on

94
00:05:58,720 --> 00:06:01,000
 my students, and I've seen

95
00:06:01,000 --> 00:06:05,740
 this done, I don't do it, or I haven't done it with people

96
00:06:05,740 --> 00:06:08,000
 whose names I've chosen, but

97
00:06:08,000 --> 00:06:14,230
 I don't think I would do such a thing. But my teacher does

98
00:06:14,230 --> 00:06:16,320
 that, he gives them, my teacher's

99
00:06:16,320 --> 00:06:20,210
 name is Siri Mangalow, and he'll give people the words

100
00:06:20,210 --> 00:06:22,720
 starting with Siri, so Siri this,

101
00:06:22,720 --> 00:06:27,260
 Siri that, for the most part, I've seen him give a lot of

102
00:06:27,260 --> 00:06:29,560
 those names. So this is one

103
00:06:29,560 --> 00:06:32,630
 of those questions that doesn't really, you know, there's

104
00:06:32,630 --> 00:06:34,320
 nothing deep or spiritual about

105
00:06:34,320 --> 00:06:37,580
 the question, but it's one that crops up and a lot of

106
00:06:37,580 --> 00:06:39,960
 people ask and at least wonder, so

107
00:06:39,960 --> 00:06:42,000
 good to answer. Thanks for the question.

108
00:06:42,000 --> 00:06:49,000
 [Silence]

