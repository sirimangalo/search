1
00:00:00,000 --> 00:00:04,000
 and sympathetic joy or gladness, mudita.

2
00:00:04,000 --> 00:00:13,000
 Its near enemy is joy based on the home life.

3
00:00:13,000 --> 00:00:21,000
 That means becoming merry, joyful, with attachment.

4
00:00:21,000 --> 00:00:25,000
 Such joy has been described.

5
00:00:26,000 --> 00:00:31,140
 And a version of Bodham or non-the light which is diss

6
00:00:31,140 --> 00:00:33,000
imilar to the similar joy is its far enemy.

7
00:00:33,000 --> 00:00:37,000
 So something like envy is its far enemy.

8
00:00:37,000 --> 00:00:41,560
 So gladness will be practiced free from fear or danger of

9
00:00:41,560 --> 00:00:42,000
 that.

10
00:00:42,000 --> 00:00:47,310
 And then equanimity has the equanimity of unknowing based

11
00:00:47,310 --> 00:00:50,000
 on the home life as its near enemy.

12
00:00:50,000 --> 00:00:53,000
 That is not knowing, ignorance.

13
00:00:53,000 --> 00:01:02,140
 Ignorance which is not accompanied by pleasurable feeling

14
00:01:02,140 --> 00:01:05,000
 or displeasure-able feeling.

15
00:01:05,000 --> 00:01:10,000
 So accompanied by indifferent feeling and it is ignorance.

16
00:01:10,000 --> 00:01:14,000
 Since both share in ignoring faults and virtues.

17
00:01:15,000 --> 00:01:22,240
 And then its and greed and resentment which are dissimilar

18
00:01:22,240 --> 00:01:26,000
 to similar unknowing are its far enemies.

19
00:01:26,000 --> 00:01:30,000
 So its far enemies are greed and resentment.

20
00:01:30,000 --> 00:01:36,000
 You can either fall into greed or into resentment.

21
00:01:36,000 --> 00:01:39,000
 And they are far enemies.

22
00:01:39,000 --> 00:01:42,940
 Therefore equanimity must be practiced free from fear or

23
00:01:42,940 --> 00:01:44,000
 danger of that.

24
00:01:46,000 --> 00:01:51,000
 So each of these has two kinds of enemies.

25
00:01:51,000 --> 00:01:54,000
 Near enemies and far enemies.

26
00:01:54,000 --> 00:01:58,500
 So we should be careful both about near enemies and far

27
00:01:58,500 --> 00:02:01,000
 enemies when we practice.

28
00:02:01,000 --> 00:02:04,000
 And the beginning, middle and end.

29
00:02:04,000 --> 00:02:08,000
 Now zeal, zeal is the Pali Chanda.

30
00:02:08,000 --> 00:02:11,360
 Consisting and desire to act is the beginning of all these

31
00:02:11,360 --> 00:02:12,000
 things.

32
00:02:12,000 --> 00:02:15,000
 Suppression of the hindrances etc. is the middle.

33
00:02:15,000 --> 00:02:16,000
 Absorption is the end.

34
00:02:16,000 --> 00:02:18,000
 So jhana is the end.

35
00:02:18,000 --> 00:02:23,000
 The object is a single living being or many living beings.

36
00:02:23,000 --> 00:02:26,000
 Has a mental object consisting in concept.

37
00:02:26,000 --> 00:02:29,000
 Because beings are concept it is called concept.

38
00:02:29,000 --> 00:02:32,000
 Then the order of extension.

39
00:02:32,000 --> 00:02:37,360
 Now here in the order of the extension of the object takes

40
00:02:37,360 --> 00:02:40,000
 place either in excess or in absorption.

41
00:02:41,000 --> 00:02:43,000
 Here is the order of it.

42
00:02:43,000 --> 00:02:45,760
 Just as its skills plough and first delimits an area and

43
00:02:45,760 --> 00:02:47,000
 then does its ploughing.

44
00:02:47,000 --> 00:02:50,520
 So first a single dwelling should be delimited and loving

45
00:02:50,520 --> 00:02:52,000
 kindness developed towards all beings.

46
00:02:52,000 --> 00:02:54,000
 They are in the way beginning.

47
00:02:54,000 --> 00:02:57,000
 In this dwelling may all beings be free from enmity.

48
00:02:57,000 --> 00:03:03,840
 And then when you have gained experience with that then you

49
00:03:03,840 --> 00:03:08,000
 can extend your range of loving kindness

50
00:03:08,000 --> 00:03:13,830
 by taking in two dwellings, then three dwellings, four

51
00:03:13,830 --> 00:03:15,000
 dwellings and so on.

52
00:03:15,000 --> 00:03:18,780
 Until the whole village, the district, the kingdom, one

53
00:03:18,780 --> 00:03:22,000
 direction and so on up to one wall sphere.

54
00:03:22,000 --> 00:03:25,750
 Or even beyond that and develop loving kindness towards the

55
00:03:25,750 --> 00:03:27,000
 beings in such areas.

56
00:03:27,000 --> 00:03:33,350
 That is why when I teach loving kindness I teach in two

57
00:03:33,350 --> 00:03:34,000
 ways.

58
00:03:34,000 --> 00:03:38,160
 The first is by way of persons may I be well happy and

59
00:03:38,160 --> 00:03:42,000
 peaceful, may my teachers be well happy and peaceful,

60
00:03:42,000 --> 00:03:45,000
 may my parents be well happy and peaceful and so on.

61
00:03:45,000 --> 00:03:48,390
 And the other way is may all beings in this building be

62
00:03:48,390 --> 00:03:50,000
 well happy and peaceful

63
00:03:50,000 --> 00:03:54,140
 and may all beings in this area, in this city, in this

64
00:03:54,140 --> 00:03:58,090
 county, in this state, in this country, in this world be

65
00:03:58,090 --> 00:04:00,000
 well happy and peaceful in this universe.

66
00:04:00,000 --> 00:04:02,300
 And then may all beings in general be well happy and

67
00:04:02,300 --> 00:04:03,000
 peaceful.

68
00:04:04,000 --> 00:04:08,000
 So this is extending little by little.

69
00:04:08,000 --> 00:04:11,000
 Now the outcome.

70
00:04:11,000 --> 00:04:17,500
 What results can you get from the practice of these four

71
00:04:17,500 --> 00:04:21,000
 kinds of divine abidings?

72
00:04:21,000 --> 00:04:25,270
 Just as the immaterial states are the outcome of the

73
00:04:25,270 --> 00:04:26,000
 casinos

74
00:04:27,000 --> 00:04:30,480
 and the base consisting of neither perception nor non-per

75
00:04:30,480 --> 00:04:33,000
ception is the outcome of concentration.

76
00:04:33,000 --> 00:04:36,000
 And there should be a corner there.

77
00:04:36,000 --> 00:04:42,000
 And fruition attainment is the outcome of insight

78
00:04:42,000 --> 00:04:45,640
 and the attainment of cessation is the outcome of serenity

79
00:04:45,640 --> 00:04:47,000
 coupled with insight.

80
00:04:47,000 --> 00:04:50,900
 So the divine abiding of equanimity is the outcome of the

81
00:04:50,900 --> 00:04:53,000
 first three divine abidings.

82
00:04:53,000 --> 00:04:57,330
 Because in order to be successful in the fourth one you

83
00:04:57,330 --> 00:05:03,130
 have to be successful first in the three preceding divine

84
00:05:03,130 --> 00:05:04,000
 abidings.

85
00:05:04,000 --> 00:05:08,090
 For just as the gable rafters cannot be placed in the air

86
00:05:08,090 --> 00:05:11,000
 without having first set up this scaffolding

87
00:05:11,000 --> 00:05:15,470
 and built the framework of being so it is not possible to

88
00:05:15,470 --> 00:05:17,000
 develop the fourth jhana

89
00:05:17,000 --> 00:05:20,320
 in these without having already developed the third jhana

90
00:05:20,320 --> 00:05:21,000
 in them.

91
00:05:21,000 --> 00:05:26,510
 Now in this paragraph it is said that immaterial states are

92
00:05:26,510 --> 00:05:28,000
 the outcome of the kasinas.

93
00:05:28,000 --> 00:05:33,000
 Kasina really means jhanas developed on kasinas.

94
00:05:33,000 --> 00:05:38,710
 You take kasinas as the object of meditation and then you

95
00:05:38,710 --> 00:05:41,000
 practice meditation and you get jhana.

96
00:05:44,000 --> 00:05:49,940
 If you do not get rupa with jhana then you do not get arupa

97
00:05:49,940 --> 00:05:51,000
 with jhana.

98
00:05:51,000 --> 00:05:57,720
 That is why it is said that the kasina jhanas are and the

99
00:05:57,720 --> 00:06:01,000
 immaterial states are the outcome of kasina jhanas.

100
00:06:01,000 --> 00:06:06,210
 And the base consisting of neither perception nor non-per

101
00:06:06,210 --> 00:06:09,000
ception is the outcome of concentration.

102
00:06:12,000 --> 00:06:17,330
 The fourth aruba with jhana is the outcome of the third ar

103
00:06:17,330 --> 00:06:20,000
uba with jhana concentration.

104
00:06:20,000 --> 00:06:24,000
 And fruition attainment is the outcome of insight.

105
00:06:24,000 --> 00:06:27,070
 Now when you practice vibhasana meditation and you become

106
00:06:27,070 --> 00:06:28,000
 enlightened.

107
00:06:28,000 --> 00:06:34,990
 So when you gain enlightenment you gain fruition attainment

108
00:06:34,990 --> 00:06:35,000
.

109
00:06:35,000 --> 00:06:39,000
 So fruition attainment is the outcome of insight meditation

110
00:06:39,000 --> 00:06:39,000
.

111
00:06:39,000 --> 00:06:41,760
 And the attainment of cessation is the outcome of serenity

112
00:06:41,760 --> 00:06:43,000
 coupled with insight.

113
00:06:43,000 --> 00:06:47,000
 Now there is an attainment called cessation attainment.

114
00:06:47,000 --> 00:06:52,000
 That means cessation of mental activities.

115
00:06:52,000 --> 00:07:00,000
 It is a very highly developed type of jhana.

116
00:07:02,000 --> 00:07:08,910
 And in order to get into that jhana you must first have all

117
00:07:08,910 --> 00:07:12,000
 the eight or nine jhanas.

118
00:07:12,000 --> 00:07:17,230
 So you enter into first jhana and then emerge from it and

119
00:07:17,230 --> 00:07:21,000
 then enter into second jhana, third jhana and so on.

120
00:07:21,000 --> 00:07:25,000
 Until you reach the fourth aruba with jhana.

121
00:07:27,000 --> 00:07:35,000
 And from first jhana after emerging from the first jhana

122
00:07:35,000 --> 00:07:38,000
 you practice vibhasana there.

123
00:07:38,000 --> 00:07:40,930
 And then you enter into second jhana, that is samatha, se

124
00:07:40,930 --> 00:07:42,000
renity meditation.

125
00:07:42,000 --> 00:07:45,170
 And then you practice vibhasana on that jhana or on the

126
00:07:45,170 --> 00:07:47,000
 factors of jhana and so on.

127
00:07:47,000 --> 00:07:52,000
 So vibhasana and samatha go in a coupled way.

128
00:07:52,000 --> 00:07:58,040
 So it is stated here that outcome of serenity coupled with

129
00:07:58,040 --> 00:07:59,000
 insight.

130
00:07:59,000 --> 00:08:09,040
 The explanation of this attainment will be given at the end

131
00:08:09,040 --> 00:08:11,000
 of the book.

132
00:08:11,000 --> 00:08:14,460
 So the diviner biting of equanimity is the outcome of the

133
00:08:14,460 --> 00:08:17,000
 first three diviner biting and so on.

134
00:08:17,000 --> 00:08:20,900
 Now questions, why are the loving kindness, compassion,

135
00:08:20,900 --> 00:08:26,000
 gladness and equanimity called divine abiding and so on?

136
00:08:26,000 --> 00:08:33,200
 Now the word, the Pali word for divine abiding is brahma v

137
00:08:33,200 --> 00:08:34,000
ihara.

138
00:08:34,000 --> 00:08:40,000
 Vihara means living. So it is here translated as abiding.

139
00:08:40,000 --> 00:08:46,400
 And brahma, the word brahma has two meanings here. One is

140
00:08:46,400 --> 00:08:52,000
 the best and the other is faultless, so immaculate.

141
00:08:52,000 --> 00:08:56,950
 And these abidings are called the best because they are the

142
00:08:56,950 --> 00:09:00,000
 right attitude towards beings.

143
00:09:00,000 --> 00:09:04,160
 So these are the right attitudes we should have towards

144
00:09:04,160 --> 00:09:07,000
 beings and so they are called the best.

145
00:09:07,000 --> 00:09:11,350
 And just as brahma gods abide with immaculate minds, so the

146
00:09:11,350 --> 00:09:14,600
 meditators who associate themselves with these abidings

147
00:09:14,600 --> 00:09:17,000
 abide on an equal footing with brahma gods.

148
00:09:17,000 --> 00:09:25,630
 So the brahmas have immaculate minds so their minds are not

149
00:09:25,630 --> 00:09:32,000
 contaminated by some mental defilements.

150
00:09:32,000 --> 00:09:39,240
 And a person who practices any one of these meditations is

151
00:09:39,240 --> 00:09:41,000
 like a brahma.

152
00:09:41,000 --> 00:09:49,460
 Because the brahma gods in the world of the brahmas always

153
00:09:49,460 --> 00:09:55,890
 practice these four divine abidings because they live with

154
00:09:55,890 --> 00:09:57,000
 these four divine abidings.

155
00:09:57,000 --> 00:10:03,390
 They have no other thing to do. So these are called brahma

156
00:10:03,390 --> 00:10:04,000
 vihara.

157
00:10:04,000 --> 00:10:11,000
 The abiding like that of the brahmas. We can call that two.

158
00:10:11,000 --> 00:10:12,000
 We can see that two.

159
00:10:12,000 --> 00:10:15,870
 So they are called divine abidings in the sense of best and

160
00:10:15,870 --> 00:10:18,000
 in the sense of immaculate.

161
00:10:18,000 --> 00:10:27,000
 So best way of abiding or a false way of abiding.

162
00:10:27,000 --> 00:10:33,660
 Why are there only four? And then the answer given is

163
00:10:33,660 --> 00:10:38,000
 because the way to purity is fulfilled.

164
00:10:38,000 --> 00:10:42,630
 Loving kindness is the way to purity for one who has much

165
00:10:42,630 --> 00:10:48,000
 ill will. Compassion is that for one who has much cruelty.

166
00:10:48,000 --> 00:10:53,000
 Clerkness is that for one who has much non-delight.

167
00:10:53,000 --> 00:10:59,000
 And equanimity is that for one who has much creed.

168
00:10:59,000 --> 00:11:02,000
 How about grumpy?

169
00:11:02,000 --> 00:11:04,000
 Grumpy?

170
00:11:04,000 --> 00:11:09,000
 I don't know.

171
00:11:09,000 --> 00:11:13,430
 And attention given to beings is only fourfold. That is to

172
00:11:13,430 --> 00:11:17,550
 say as bringing welfare, as removing suffering, as being

173
00:11:17,550 --> 00:11:20,000
 glad at their success and as unconcerned.

174
00:11:20,000 --> 00:11:24,220
 That is to say impartial neutrality. That is the second

175
00:11:24,220 --> 00:11:25,000
 reason.

176
00:11:25,000 --> 00:11:27,700
 The third reason is one abiding and measureless state

177
00:11:27,700 --> 00:11:31,000
 should practice loving kindness and the rest like a mother

178
00:11:31,000 --> 00:11:32,000
 with four sons.

179
00:11:32,000 --> 00:11:35,290
 Namely a child and invalid, one in the flesh of youth and

180
00:11:35,290 --> 00:11:38,000
 one busy with his own affairs.

181
00:11:38,000 --> 00:11:41,270
 For she wants the child to grow up, wants the invalid to

182
00:11:41,270 --> 00:11:43,920
 get well, wants the one in the flesh of youth to enjoy for

183
00:11:43,920 --> 00:11:46,000
 long the benefits of youth.

184
00:11:46,000 --> 00:11:49,680
 And it's not at all boudard about the one who is busy with

185
00:11:49,680 --> 00:11:51,000
 his own affairs.

186
00:11:51,000 --> 00:11:55,100
 So these four brahma viyasas. That is why the measureless

187
00:11:55,100 --> 00:11:57,000
 states are only four.

188
00:11:57,000 --> 00:12:02,000
 The first two purity and the other sides are four.

189
00:12:02,000 --> 00:12:08,020
 The order is just loving kindness, compression, gladness

190
00:12:08,020 --> 00:12:10,000
 and equanimity.

191
00:12:10,000 --> 00:12:13,000
 Now why are they called measureless?

192
00:12:13,000 --> 00:12:16,000
 Paragraph 110.

193
00:12:16,000 --> 00:12:19,000
 For their scoop is measureless beings.

194
00:12:19,000 --> 00:12:24,610
 That means their object is beings without measure, without

195
00:12:24,610 --> 00:12:26,000
 exception.

196
00:12:26,000 --> 00:12:31,490
 So all of them occur with a measureless scope. For their

197
00:12:31,490 --> 00:12:33,000
 scope is measureless beings.

198
00:12:33,000 --> 00:12:36,500
 And instead of assuming a measure such as loving kindness

199
00:12:36,500 --> 00:12:39,500
 etc. should be developed only to what a single being or in

200
00:12:39,500 --> 00:12:43,950
 an area of such an extent, the occur with universal

201
00:12:43,950 --> 00:12:45,000
 provision.

202
00:12:45,000 --> 00:12:47,000
 What does that mean?

203
00:12:47,000 --> 00:12:47,320
 [

204
00:12:47,320 --> 00:12:49,320
 [

205
00:12:49,320 --> 00:12:51,320
 [

206
00:12:51,320 --> 00:12:53,320
 [

207
00:12:53,320 --> 00:12:55,320
 [

208
00:12:55,320 --> 00:12:57,320
 [

209
00:12:57,320 --> 00:12:59,320
 ]]

210
00:12:59,320 --> 00:13:02,630
 Then when you practice loving kindness to a person, it

211
00:13:02,630 --> 00:13:07,000
 cannot be called measureless.

212
00:13:07,000 --> 00:13:09,000
 [

213
00:13:09,000 --> 00:13:11,000
 ]

214
00:13:11,000 --> 00:13:19,000
 Now the translation is a little off the mark.

215
00:13:19,000 --> 00:13:24,100
 What is meant here or what is written by the origin of

216
00:13:24,100 --> 00:13:28,070
 order is even towards one person, even when you are

217
00:13:28,070 --> 00:13:31,960
 practicing towards one person, it can be called measureless

218
00:13:31,960 --> 00:13:32,000
.

219
00:13:32,000 --> 00:13:39,120
 Because you do not limit your loving kindness to one part

220
00:13:39,120 --> 00:13:41,000
 of his body.

221
00:13:41,000 --> 00:13:43,000
 That is what is meant here.

222
00:13:43,000 --> 00:13:48,960
 So even in regard to a single being, they occur with

223
00:13:48,960 --> 00:13:52,860
 universal provision without assuming a measure as loving

224
00:13:52,860 --> 00:13:57,000
 kindness should be developed in this much part of him.

225
00:13:57,000 --> 00:14:03,080
 Say something like, may his head be well heavy and peaceful

226
00:14:03,080 --> 00:14:04,000
 or so.

227
00:14:04,000 --> 00:14:06,530
 May the upper part of his body be well heavy and peaceful

228
00:14:06,530 --> 00:14:07,000
 or so.

229
00:14:07,000 --> 00:14:12,000
 So you take the whole being.

230
00:14:12,000 --> 00:14:16,550
 So even with regard to a single person, it can be called

231
00:14:16,550 --> 00:14:18,000
 measureless.

232
00:14:18,000 --> 00:14:22,000
 That is what is meant here.

233
00:14:22,000 --> 00:14:27,000
 And then producing three jhanas and four jhanas.

234
00:14:27,000 --> 00:14:32,000
 So the first three can produce how many jhanas?

235
00:14:32,000 --> 00:14:36,000
 Three or four.

236
00:14:36,000 --> 00:14:41,000
 The last one can produce the fourth or the fifth.

237
00:14:41,000 --> 00:14:49,000
 Only one jhana.

238
00:14:49,000 --> 00:14:55,000
 Now there is a discussion here with an opponent.

239
00:14:55,000 --> 00:15:01,430
 Now from paragraph 112, the argument and counter-argument,

240
00:15:01,430 --> 00:15:03,000
 something like that.

241
00:15:03,000 --> 00:15:10,000
 So the opponent pointed to a sudha.

242
00:15:10,000 --> 00:15:16,670
 And he took that sudha to mean that all four or five jhanas

243
00:15:16,670 --> 00:15:23,800
 can be obtained by the practice of metta, karuna and mudita

244
00:15:23,800 --> 00:15:25,000
 also.

245
00:15:30,000 --> 00:15:40,760
 And the form we see in that sudha may point to that if you

246
00:15:40,760 --> 00:15:45,000
 are not really familiar with abhidhamma.

247
00:15:45,000 --> 00:15:48,930
 Buddha said, after you practice loving kindness, then you

248
00:15:48,930 --> 00:15:54,210
 should go on making this loving kindness with vitaka and

249
00:15:54,210 --> 00:15:59,000
 with jhara, without vitaka, but with vitara and so on.

250
00:15:59,000 --> 00:16:06,000
 So when you read it superficially, you may also think that

251
00:16:06,000 --> 00:16:09,290
 you can get all four or five jhanas through metta

252
00:16:09,290 --> 00:16:11,000
 meditation and so on.

253
00:16:11,000 --> 00:16:16,000
 And that is not so. So the author said, he should be told,

254
00:16:16,000 --> 00:16:18,000
 do not put it like that.

255
00:16:18,000 --> 00:16:22,020
 For if that was so, then contemplation of the body etc.

256
00:16:22,020 --> 00:16:26,000
 would also have quadruple and quintuple jhana.

257
00:16:26,000 --> 00:16:30,700
 Because Buddha taught about contemplation of body also in

258
00:16:30,700 --> 00:16:32,000
 the same way.

259
00:16:32,000 --> 00:16:35,960
 But contemplation of the body can produce only the first jh

260
00:16:35,960 --> 00:16:38,000
ana, not the other jhana.

261
00:16:38,000 --> 00:16:41,000
 It is mentioned back.

262
00:16:41,000 --> 00:16:45,630
 And there is not even the first jhana in the contemplation

263
00:16:45,630 --> 00:16:47,000
 of feeling.

264
00:16:47,000 --> 00:16:56,000
 Or in the other two, nah, or in the other two is wrong.

265
00:16:56,000 --> 00:17:02,820
 So in each place we should have not even the first jhana in

266
00:17:02,820 --> 00:17:08,230
 the contemplation of feeling, let alone the second and so

267
00:17:08,230 --> 00:17:09,000
 on.

268
00:17:09,000 --> 00:17:13,000
 So please correct that. Let alone the second and so on.

269
00:17:13,000 --> 00:17:18,120
 So when you practice feeling contemplation, you cannot get

270
00:17:18,120 --> 00:17:24,760
 even the first jhana, let alone second, third, fourth and

271
00:17:24,760 --> 00:17:26,000
 fifth.

272
00:17:26,000 --> 00:17:31,820
 So do not misrepresent the blessed one by adherence to the

273
00:17:31,820 --> 00:17:33,000
 letter.

274
00:17:33,000 --> 00:17:36,000
 And then he explained this.

275
00:17:36,000 --> 00:17:45,000
 Now, about five lines down, paragraph 114.

276
00:17:45,000 --> 00:17:50,000
 But the blessed one had no confidence yet in that vehicle.

277
00:17:50,000 --> 00:17:54,000
 That was the word I was talking to you about.

278
00:17:54,000 --> 00:18:01,070
 So actually the Buddha scolded that monk by saying, these

279
00:18:01,070 --> 00:18:02,000
 people,

280
00:18:02,000 --> 00:18:06,000
 I think that is some misguided man merely questioned me,

281
00:18:06,000 --> 00:18:13,000
 and when the Dhamma is expounded to them, they still fancy

282
00:18:13,000 --> 00:18:17,000
 that they need to follow me, something like that.

283
00:18:17,000 --> 00:18:19,440
 So that monk asked the Buddha to teach him, and Buddha

284
00:18:19,440 --> 00:18:21,000
 taught him, and he didn't practice.

285
00:18:21,000 --> 00:18:25,000
 And then he again asked Buddha to teach him.

286
00:18:25,000 --> 00:18:28,570
 But Buddha saw that he had the capability to become an arah

287
00:18:28,570 --> 00:18:31,000
ant, so this time the Buddha taught him.

288
00:18:31,000 --> 00:18:35,000
 And this.

289
00:18:35,000 --> 00:18:44,000
 So, school, can you give me a more appropriate word?

290
00:18:44,000 --> 00:18:48,000
 Dismissed with something.

291
00:18:48,000 --> 00:18:50,000
 Scolded.

292
00:18:50,000 --> 00:18:52,000
 Yeah, scolded.

293
00:18:52,000 --> 00:18:55,000
 What's the word reject?

294
00:18:55,000 --> 00:19:00,000
 Chest ice.

295
00:19:00,000 --> 00:19:04,000
 Very good, yes.

296
00:19:04,000 --> 00:19:10,020
 So Buddha chastised him, and not had no confidence yet in

297
00:19:10,020 --> 00:19:11,000
 him.

298
00:19:11,000 --> 00:19:17,850
 He misread the word, the right word, but with a long

299
00:19:17,850 --> 00:19:20,000
 division.

300
00:19:20,000 --> 00:19:27,000
 So Buddha chastised the monk.

301
00:19:27,000 --> 00:19:33,000
 And the meaning here is, although when you read the soda,

302
00:19:33,000 --> 00:19:38,570
 it seems to mean that you can practice loving kindness

303
00:19:38,570 --> 00:19:39,000
 meditation,

304
00:19:39,000 --> 00:19:43,000
 and then you can get first again that fourth fifth jhana

305
00:19:43,000 --> 00:19:48,000
 from the practice of loving kindness meditation.

306
00:19:48,000 --> 00:19:52,100
 But what Buddha meant there was that you practice

307
00:19:52,100 --> 00:19:54,000
 meditation, mitha meditation,

308
00:19:54,000 --> 00:19:58,000
 and you may get first jhana or second jhana or third jhana.

309
00:19:58,000 --> 00:20:01,000
 But do not be content with that.

310
00:20:01,000 --> 00:20:05,600
 Try to get other higher jhana by taking other subjects of

311
00:20:05,600 --> 00:20:08,000
 meditation, not mitha meditation.

312
00:20:08,000 --> 00:20:13,280
 If you want to get fourth or fifth jhana, then you have to

313
00:20:13,280 --> 00:20:17,000
 practice other kind of meditation, not mitha meditation.

314
00:20:17,000 --> 00:20:21,490
 Because by the practice of mitha meditation, you can get

315
00:20:21,490 --> 00:20:25,000
 only to the third or the fourth jhana.

316
00:20:25,000 --> 00:20:35,000
 That is what is meant.

317
00:20:35,000 --> 00:20:44,340
 And in the footnote on page 349, "Mere unification of the

318
00:20:44,340 --> 00:20:49,000
 mind, the kind of concentration that is undeveloped,"

319
00:20:49,000 --> 00:20:54,480
 I think that is not too developed, not undeveloped, because

320
00:20:54,480 --> 00:20:59,000
 it is a little developed, but not developed enough.

321
00:20:59,000 --> 00:21:09,160
 So that is not too developed and just obtained by one in

322
00:21:09,160 --> 00:21:16,000
 pursuit of development and so on.

323
00:21:16,000 --> 00:21:28,650
 Now, next we will go to paragraph 119, "The highest limit

324
00:21:28,650 --> 00:21:31,000
 of each."

325
00:21:31,000 --> 00:21:34,570
 And while they are twofold by way of the triple, quadruple

326
00:21:34,570 --> 00:21:37,000
 jhana and the single, remaining jhana,

327
00:21:37,000 --> 00:21:40,270
 still they should be understood to be distinguishable in

328
00:21:40,270 --> 00:21:42,000
 each case by different efficacy,

329
00:21:42,000 --> 00:21:46,380
 consisting of having beauty as the highest, etc., for the

330
00:21:46,380 --> 00:21:49,000
 asotas, kakya, the haliti, vasanasoda, and so on.

331
00:21:49,000 --> 00:21:54,680
 Now, beauty as the highest is not a good translation for

332
00:21:54,680 --> 00:21:56,000
 the word.

333
00:21:56,000 --> 00:22:14,000
 That is liberation with the beautiful as object.

334
00:22:14,000 --> 00:22:22,420
 And here liberation means jhanas, jhanas which take

335
00:22:22,420 --> 00:22:31,000
 pleasant things as objects, something like that.

336
00:22:31,000 --> 00:22:39,890
 So loving khanas is the basic sub-book for the liberation

337
00:22:39,890 --> 00:22:45,000
 with the beautiful as object.

338
00:22:45,000 --> 00:22:47,690
 And one who abides in compassion has come to know

339
00:22:47,690 --> 00:22:50,000
 thoroughly the danger in materiality

340
00:22:50,000 --> 00:22:52,800
 since compassion is aroused in him when he sees the

341
00:22:52,800 --> 00:22:59,000
 suffering of beings that has its material-centric core.

342
00:22:59,000 --> 00:23:07,120
 That is, has its cause formed, beaten with sticks, and so

343
00:23:07,120 --> 00:23:08,000
 on.

344
00:23:08,000 --> 00:23:11,000
 You get the sentence there?

345
00:23:11,000 --> 00:23:13,000
 121.

346
00:23:13,000 --> 00:23:25,000
 Has its cause formed, beaten with sticks, forming the body.

347
00:23:25,000 --> 00:23:28,250
 So it should be one who abides in compassion has come to

348
00:23:28,250 --> 00:23:31,000
 know thoroughly the danger in materiality

349
00:23:31,000 --> 00:23:33,000
 as love is not so good?

350
00:23:33,000 --> 00:23:36,390
 In materiality, since compassion is aroused in him when he

351
00:23:36,390 --> 00:23:38,000
 sees the suffering of beings

352
00:23:38,000 --> 00:23:45,000
 that has its cause formed, beaten with sticks, and so on.

353
00:23:45,000 --> 00:23:48,000
 So it has as its cause formed?

354
00:23:48,000 --> 00:23:54,000
 Yes, beaten with sticks, and so on.

355
00:23:54,000 --> 00:24:00,000
 And then...

356
00:24:00,000 --> 00:24:10,000
 123.

357
00:24:10,000 --> 00:24:14,060
 There is a lot of existent and non-existent things in that

358
00:24:14,060 --> 00:24:15,000
 passage.

359
00:24:15,000 --> 00:24:26,350
 So what you should keep in mind is happiness and pain, suk

360
00:24:26,350 --> 00:24:30,000
ha and doka, are said to be existent

361
00:24:30,000 --> 00:24:35,870
 because they are... they belong to paramata, they belong to

362
00:24:35,870 --> 00:24:37,000
 ultimate reality.

363
00:24:37,000 --> 00:24:45,580
 Because sukha, as well as doka, are among the mental

364
00:24:45,580 --> 00:24:48,000
 factors.

365
00:24:48,000 --> 00:24:54,080
 Sukha is a pleasurable feeling, and doka is a displeasure

366
00:24:54,080 --> 00:24:55,000
 feeling.

367
00:24:55,000 --> 00:25:00,200
 And so they are among the 52 mental states, mental factors,

368
00:25:00,200 --> 00:25:04,000
 and so they are said to be existent things.

369
00:25:04,000 --> 00:25:08,000
 But a being is non-existent according to ultimate reality,

370
00:25:08,000 --> 00:25:11,000
 or in the ultimate sense.

371
00:25:11,000 --> 00:25:15,300
 A being is just a combination of mind and matter, or a

372
00:25:15,300 --> 00:25:18,000
 combination of five aggregates.

373
00:25:18,000 --> 00:25:22,000
 And so when we say a being, it is non-existent.

374
00:25:22,000 --> 00:25:27,540
 And when we say sukha and doka, then sukha and doka are

375
00:25:27,540 --> 00:25:29,000
 existent.

376
00:25:29,000 --> 00:25:32,750
 If you keep that in mind, then I think you can understand

377
00:25:32,750 --> 00:25:34,000
 this paragraph.

378
00:25:34,000 --> 00:25:36,000
 123.

379
00:25:36,000 --> 00:25:39,000
 Now, upika.

380
00:25:39,000 --> 00:25:43,650
 Upika is not interested in the happiness of beings or in

381
00:25:43,650 --> 00:25:47,000
 suffering of beings.

382
00:25:47,000 --> 00:25:52,440
 It takes just the beings as an object, not the heaviness or

383
00:25:52,440 --> 00:25:54,000
 suffering of beings,

384
00:25:54,000 --> 00:25:57,000
 not the sukha or doka of beings.

385
00:25:57,000 --> 00:26:05,020
 So a person who practices upika meditation becomes skilled

386
00:26:05,020 --> 00:26:12,000
 in taking what is non-existent, that is, beings, as object.

387
00:26:12,000 --> 00:26:19,690
 So it is easier for him, for such a person, to take the non

388
00:26:19,690 --> 00:26:25,000
-existent, the other kind of non-existent as object.

389
00:26:25,000 --> 00:26:30,640
 And that is the ways consisting of which is non-existent to

390
00:26:30,640 --> 00:26:37,480
 the individual essence of consciousness, which is a reality

391
00:26:37,480 --> 00:26:38,000
.

392
00:26:38,000 --> 00:26:46,690
 Now, the third aruba vajra, the third immaterial jhana is

393
00:26:46,690 --> 00:26:48,000
 what?

394
00:26:48,000 --> 00:26:55,310
 Non-existence of the first aruba vajra jhana. So non-

395
00:26:55,310 --> 00:27:00,000
existence is itself non-existent.

396
00:27:00,000 --> 00:27:05,000
 Non-existence is a concept. It is not reality.

397
00:27:05,000 --> 00:27:13,300
 So a person who is skilled in taking concept as object when

398
00:27:13,300 --> 00:27:16,000
 he practices upika meditation,

399
00:27:16,000 --> 00:27:23,000
 will find it very easy to take non-existence or nothingness

400
00:27:23,000 --> 00:27:29,260
 of the first consciousness as object when he tries to get

401
00:27:29,260 --> 00:27:32,000
 the immaterial jhana.

402
00:27:32,000 --> 00:27:40,530
 Now, in order to get the third aruba vajra jhana, you have

403
00:27:40,530 --> 00:27:49,000
 to take the absence of first aruba vajra jhana as object.

404
00:27:49,000 --> 00:27:54,000
 You know, aruba vajra jhana, the immaterial jhana.

405
00:27:54,000 --> 00:28:00,770
 Most of the jhanas we are talking about are the material j

406
00:28:00,770 --> 00:28:02,000
hanas.

407
00:28:02,000 --> 00:28:07,220
 So to get to the immaterial jhanas, we will describe in the

408
00:28:07,220 --> 00:28:09,000
 coming chapter.

409
00:28:09,000 --> 00:28:11,000
 Is it the same as formless?

410
00:28:11,000 --> 00:28:15,000
 Oh yes, formless, right, formless jhana.

411
00:28:15,000 --> 00:28:22,330
 So in order to get the third formless jhana, you have to

412
00:28:22,330 --> 00:28:29,000
 meditate on the absence of the first formless jhana.

413
00:28:29,000 --> 00:28:35,000
 So that absence is nothing. So that is just a concept.

414
00:28:35,000 --> 00:28:39,840
 So if you are skilled in taking concept as object when you

415
00:28:39,840 --> 00:28:44,490
 practice upika meditation, then you will find it easier

416
00:28:44,490 --> 00:28:48,000
 here to take nothingness as object.

417
00:28:48,000 --> 00:28:55,000
 So that is what is explained in paragraph 123.

418
00:28:55,000 --> 00:29:01,240
 Descendants are long and involved, so it's not so easy to

419
00:29:01,240 --> 00:29:03,000
 understand.

420
00:29:03,000 --> 00:29:09,950
 So please keep in mind that when you say existent, it means

421
00:29:09,950 --> 00:29:12,000
 suka and doka here.

422
00:29:12,000 --> 00:29:16,400
 And when it says non-existent, that means the beings,

423
00:29:16,400 --> 00:29:26,000
 because the beings are not existent in the ultimate sense.

424
00:29:26,000 --> 00:29:34,000
 Okay, the others are not difficult to understand.

425
00:29:34,000 --> 00:29:41,460
 The footnote number 20 is very good, because even in the

426
00:29:41,460 --> 00:29:46,000
 Burmese edition, there is a mistake.

427
00:29:46,000 --> 00:29:50,400
 And in the Sinhalese edition, the reading, I think the

428
00:29:50,400 --> 00:29:54,000
 reading is right, and he took the Sinhalese edition here.

429
00:29:54,000 --> 00:29:58,980
 You may not understand this, but here he said reading in

430
00:29:58,980 --> 00:30:06,270
 both cases, "Havijya manag hainat dakkam", and not "dukkam

431
00:30:06,270 --> 00:30:07,000
".

432
00:30:07,000 --> 00:30:11,720
 "Dakkam" and "dukkam", they have two very different

433
00:30:11,720 --> 00:30:13,000
 meanings.

434
00:30:13,000 --> 00:30:18,000
 "Dakkam" means skilled, and "dukkam" means suffering.

435
00:30:18,000 --> 00:30:24,490
 So here, the correct reading should be "Dakkam" and not "du

436
00:30:24,490 --> 00:30:29,000
kkam", just the difference of A and U.

437
00:30:29,000 --> 00:30:36,000
 So he detected this, and very good.

438
00:30:36,000 --> 00:30:43,270
 And then on page 353, it is interesting here, because by

439
00:30:43,270 --> 00:30:50,000
 the practice of these, you also fulfill the ten paramitas.

440
00:30:50,000 --> 00:30:53,920
 In Theravada Buddhism, there are ten paramitas to be

441
00:30:53,920 --> 00:30:59,720
 fulfilled, especially by bodhisattvas and also by any

442
00:30:59,720 --> 00:31:01,000
 person.

443
00:31:01,000 --> 00:31:06,200
 So these ten paramitas are mentioned, and to all beings,

444
00:31:06,200 --> 00:31:08,000
 they give gifts.

445
00:31:08,000 --> 00:31:16,000
 That means giving, that is the first paramita.

446
00:31:16,000 --> 00:31:19,340
 And in order to avoid doing harm to beings, they undertake

447
00:31:19,340 --> 00:31:23,000
 the precepts of virtue, that is second paramita.

448
00:31:23,000 --> 00:31:27,990
 The first is seelah, and the second, I mean, the first is

449
00:31:27,990 --> 00:31:31,000
 dhana, and the second seelah.

450
00:31:31,000 --> 00:31:34,390
 They practice renunciation, that is the third paramita, for

451
00:31:34,390 --> 00:31:37,000
 the purpose of perfecting their virtue.

452
00:31:37,000 --> 00:31:40,570
 They cleanse their understanding, that is the fourth, pañ

453
00:31:40,570 --> 00:31:44,060
ña, for the purpose of non-confusion about bodhisattvas and

454
00:31:44,060 --> 00:31:45,000
 bad-for beings.

455
00:31:45,000 --> 00:31:48,960
 They constantly arouse energy, that is the fifth, virya,

456
00:31:48,960 --> 00:31:52,000
 having beings' welfare and happiness attack.

457
00:31:52,000 --> 00:31:56,270
 When they have acquired heroic fortitude through supreme

458
00:31:56,270 --> 00:32:06,000
 energy, they become patient, kanti, seventh, sixth.

459
00:32:06,000 --> 00:32:10,960
 They do not deceive, that means truthfulness, that is

460
00:32:10,960 --> 00:32:14,000
 seventh, they do not deceive when promising,

461
00:32:14,000 --> 00:32:17,000
 "We shall give you this, we shall do this for you."

462
00:32:17,000 --> 00:32:22,950
 They are unshakably resolute upon beings, so resoluteness,

463
00:32:22,950 --> 00:32:25,000
 the eighth paramita.

464
00:32:25,000 --> 00:32:30,000
 In Bali it is called a diktaana, firmness of resolution.

465
00:32:30,000 --> 00:32:35,210
 Through unshakable loving-kindness, this is the ninth, they

466
00:32:35,210 --> 00:32:37,000
 place them first before themselves.

467
00:32:37,000 --> 00:32:42,000
 Actually, here what it means is, they do favour first.

468
00:32:42,000 --> 00:32:47,100
 Now Bali word, pubba kari, pubba means first and kari means

469
00:32:47,100 --> 00:32:48,000
 doing.

470
00:32:48,000 --> 00:32:52,630
 So they do first means, they don't wait for you to do

471
00:32:52,630 --> 00:32:56,000
 favour to them, so they do first.

472
00:32:56,000 --> 00:33:01,700
 So through unshakable loving-kindness, they do favour first

473
00:33:01,700 --> 00:33:02,000
.

474
00:33:02,000 --> 00:33:07,010
 And then through equanimity, the tenth, they expect no

475
00:33:07,010 --> 00:33:08,000
 reward.

476
00:33:08,000 --> 00:33:12,420
 Having thus fulfilled the ten perfections, these divine ab

477
00:33:12,420 --> 00:33:13,000
idings,

478
00:33:13,000 --> 00:33:18,200
 then perfect all the good states classed as the ten powers,

479
00:33:18,200 --> 00:33:20,000
 the four kinds of fearlessness,

480
00:33:20,000 --> 00:33:24,330
 and the six kinds of knowledge not shared by disciples, and

481
00:33:24,330 --> 00:33:27,000
 the eighteen states of the enlightened one.

482
00:33:27,000 --> 00:33:29,790
 This is how they bring to perfection all the good states,

483
00:33:29,790 --> 00:33:31,000
 beginning with giving.

484
00:33:31,000 --> 00:33:36,260
 So these ten paramitras can be fulfilled by the practice of

485
00:33:36,260 --> 00:33:39,000
 the four divine abidings.

486
00:33:39,000 --> 00:33:47,000
 There is so much to talk about.

487
00:33:47,000 --> 00:33:54,000
 I am telling you the good, the next, but I cannot.

488
00:33:54,000 --> 00:33:58,000
 Okay, next week.

489
00:33:58,000 --> 00:34:07,000
 So we should read through, let me see, how many pages?

490
00:34:07,000 --> 00:34:10,000
 Maybe more than five pages.

491
00:34:10,000 --> 00:34:12,000
 Four chapter?

492
00:34:12,000 --> 00:34:16,000
 Three hundred, four chapter, let me see.

493
00:34:16,000 --> 00:34:20,000
 Yes.

494
00:34:20,000 --> 00:34:25,000
 Three hundred seventy.

495
00:34:25,000 --> 00:34:28,000
 Let me see.

496
00:34:28,000 --> 00:34:30,000
 Two seventy-two.

497
00:34:30,000 --> 00:34:34,000
 That's eighteen pages.

498
00:34:34,000 --> 00:34:36,000
 Eighteen pages?

499
00:34:36,000 --> 00:34:39,000
 Yes, two seventy-two.

500
00:34:52,000 --> 00:34:55,000
 Three seventy-two.

501
00:34:55,000 --> 00:34:59,000
 The next chapter, the whole chapter.

502
00:35:01,000 --> 00:35:04,000
 The next chapter, the whole chapter.

503
00:35:05,000 --> 00:35:08,000
 The next chapter, the whole chapter.

504
00:35:10,000 --> 00:35:13,000
 The next chapter, the whole chapter.

505
00:35:14,000 --> 00:35:17,000
 The next chapter, the whole chapter.

506
00:35:18,000 --> 00:35:21,000
 The next chapter, the whole chapter.

507
00:35:22,000 --> 00:35:25,000
 The next chapter, the whole chapter.

508
00:35:26,000 --> 00:35:29,000
 The next chapter, the whole chapter.

509
00:35:30,000 --> 00:35:33,000
 The next chapter, the whole chapter.

510
00:35:34,000 --> 00:35:37,000
 The next chapter, the whole chapter.

511
00:35:38,000 --> 00:35:41,000
 The next chapter, the whole chapter.

512
00:35:41,000 --> 00:35:44,000
 The next chapter, the whole chapter.

513
00:35:44,000 --> 00:35:47,000
 The next chapter, the whole chapter.

514
00:35:47,000 --> 00:35:50,000
 The next chapter, the whole chapter.

515
00:35:50,000 --> 00:35:53,000
 The next chapter, the whole chapter.

516
00:35:53,000 --> 00:35:56,000
 The next chapter, the whole chapter.

517
00:35:57,000 --> 00:35:59,000
 The next chapter, the whole chapter.

518
00:35:59,000 --> 00:36:02,000
 The next chapter, the whole chapter.

