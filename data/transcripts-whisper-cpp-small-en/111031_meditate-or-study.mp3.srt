1
00:00:00,000 --> 00:00:04,320
 Okay, this is a question in response to a comment made

2
00:00:04,320 --> 00:00:06,560
 during one of the meditation sessions.

3
00:00:06,560 --> 00:00:10,830
 Someone mentioned that he/she should meditate more than

4
00:00:10,830 --> 00:00:12,980
 reading or listening to Buddhist

5
00:00:12,980 --> 00:00:16,080
 talks because it is also a pleasure.

6
00:00:16,080 --> 00:00:19,320
 Do you suggest us to stop reading or watching Buddhist

7
00:00:19,320 --> 00:00:23,240
 YouTube talks and just meditating

8
00:00:23,240 --> 00:00:24,240
 for now?

9
00:00:24,240 --> 00:00:27,880
 Just meditate for now.

10
00:00:27,880 --> 00:00:31,240
 Well, not all pleasure is wrong.

11
00:00:31,240 --> 00:00:32,880
 In fact, no pleasure is wrong.

12
00:00:32,880 --> 00:00:35,740
 Pleasure isn't the problem.

13
00:00:35,740 --> 00:00:40,200
 Not all pleasure is associated with negative mind states.

14
00:00:40,200 --> 00:00:42,990
 There's a difference between pleasure and attachment or

15
00:00:42,990 --> 00:00:44,320
 pleasure and liking, if you

16
00:00:44,320 --> 00:00:48,480
 want to say.

17
00:00:48,480 --> 00:00:51,480
 There's also a kind of, you could say there's a kind of

18
00:00:51,480 --> 00:00:53,480
 liking that is in a roundabout way

19
00:00:53,480 --> 00:00:54,480
 beneficial.

20
00:00:54,480 --> 00:00:58,790
 So, people who like to practice meditation, people who like

21
00:00:58,790 --> 00:01:00,640
 to listen to the Buddhist

22
00:01:00,640 --> 00:01:05,050
 teaching, who are waiting for these sessions because it

23
00:01:05,050 --> 00:01:07,680
 stimulates them and so on, it's

24
00:01:07,680 --> 00:01:10,630
 kind of a negative thing because what they're really

25
00:01:10,630 --> 00:01:12,800
 attached to is the feelings and maybe

26
00:01:12,800 --> 00:01:16,280
 when I joke or when they hear stories.

27
00:01:16,280 --> 00:01:19,960
 There are these Buddhist teachers who give talks and

28
00:01:19,960 --> 00:01:22,520
 everyone likes to hear their stories.

29
00:01:22,520 --> 00:01:24,870
 And if you read some of the old Buddhist texts, there's a

30
00:01:24,870 --> 00:01:26,240
 lot of nice stories in there and

31
00:01:26,240 --> 00:01:27,240
 it's stimulating.

32
00:01:27,240 --> 00:01:30,310
 In the mind, it makes you feel happy, it makes you feel sad

33
00:01:30,310 --> 00:01:30,560
.

34
00:01:30,560 --> 00:01:33,560
 It's like when we watch a movie or when we read a novel or

35
00:01:33,560 --> 00:01:35,600
 something, it stimulates you.

36
00:01:35,600 --> 00:01:37,400
 So that's kind of negative, that aspect.

37
00:01:37,400 --> 00:01:44,190
 But in a roundabout way, it can be helpful because of the

38
00:01:44,190 --> 00:01:45,720
 content.

39
00:01:45,720 --> 00:01:54,210
 Because actually when you get here, the focus changes or

40
00:01:54,210 --> 00:01:58,080
 the focus is different.

41
00:01:58,080 --> 00:02:00,940
 So the point really is in the content.

42
00:02:00,940 --> 00:02:03,920
 If the content of the teachings is truly the Buddhist

43
00:02:03,920 --> 00:02:05,960
 teaching and you truly are paying

44
00:02:05,960 --> 00:02:12,970
 attention and making effort to understand and are con

45
00:02:12,970 --> 00:02:18,240
forming your mind to the teachings,

46
00:02:18,240 --> 00:02:21,760
 trying to get your head around it, then for sure it's a

47
00:02:21,760 --> 00:02:23,840
 good thing to do all of that.

48
00:02:23,840 --> 00:02:26,240
 You can become enlightened listening to the dhamma.

49
00:02:26,240 --> 00:02:29,240
 I hope that was kind of clear when we were talking about

50
00:02:29,240 --> 00:02:30,520
 the last question.

51
00:02:30,520 --> 00:02:33,130
 As you're listening, just listening to me talk can be a

52
00:02:33,130 --> 00:02:34,720
 meditation practice in itself

53
00:02:34,720 --> 00:02:41,190
 because you're going back again to your moment to moment

54
00:02:41,190 --> 00:02:45,400
 experience and just becoming aware

55
00:02:45,400 --> 00:02:56,960
 of the practical aspects of the teaching.

56
00:02:56,960 --> 00:03:01,230
 But as to the amount, you really need to meditate more than

57
00:03:01,230 --> 00:03:03,600
 you go on YouTube and listen to

58
00:03:03,600 --> 00:03:05,000
 my talks.

59
00:03:05,000 --> 00:03:10,530
 I would say if you've watched all my talks, that's probably

60
00:03:10,530 --> 00:03:12,040
 too much.

61
00:03:12,040 --> 00:03:15,260
 The best thing would be if you could watch a selection of

62
00:03:15,260 --> 00:03:17,000
 them and then go meditate.

63
00:03:17,000 --> 00:03:19,240
 I don't know, it depends.

64
00:03:19,240 --> 00:03:26,200
 But not just talking about my talks.

65
00:03:26,200 --> 00:03:27,360
 You use it in moderation.

66
00:03:27,360 --> 00:03:33,480
 I guess I've only got 300 and some videos if it was just

67
00:03:33,480 --> 00:03:34,400
 mine.

68
00:03:34,400 --> 00:03:37,660
 One a day you've got a whole year's worth or two a day or

69
00:03:37,660 --> 00:03:38,600
 whatever.

70
00:03:38,600 --> 00:03:42,590
 There's teachings from all sorts of different traditions,

71
00:03:42,590 --> 00:03:44,480
 all different teachers.

72
00:03:44,480 --> 00:03:48,200
 It has to be in moderation and I think there are people out

73
00:03:48,200 --> 00:03:48,840
 there.

74
00:03:48,840 --> 00:03:56,070
 This is an endemic, this is a problem in Buddhism that

75
00:03:56,070 --> 00:04:01,160
 people become intellectual Buddhists and

76
00:04:01,160 --> 00:04:07,010
 do a lot of studying and sometimes a lot of talking,

77
00:04:07,010 --> 00:04:12,840
 discussing, arguing, debating, thinking.

78
00:04:12,840 --> 00:04:14,930
 If you watched my talk that the Sri Lankan people really

79
00:04:14,930 --> 00:04:16,200
 like this talk, the Sri Lankan

80
00:04:16,200 --> 00:04:19,200
 meditators who I've talked to really like this one about, I

81
00:04:19,200 --> 00:04:20,360
 really like it too.

82
00:04:20,360 --> 00:04:21,560
 I give this talk often.

83
00:04:21,560 --> 00:04:23,440
 It's not my talk actually.

84
00:04:23,440 --> 00:04:25,990
 First of all, it's based on the sutta and second of all, it

85
00:04:25,990 --> 00:04:27,320
's a talk my teacher always

86
00:04:27,320 --> 00:04:28,600
 gives.

87
00:04:28,600 --> 00:04:31,840
 That's why I picked it up.

88
00:04:31,840 --> 00:04:35,200
 On the five types of people in the world, only one of which

89
00:04:35,200 --> 00:04:36,760
 is said to be someone, or

90
00:04:36,760 --> 00:04:40,760
 five types of Buddhists, only one of which is said to be

91
00:04:40,760 --> 00:04:43,200
 someone who lives by the Dhamma.

92
00:04:43,200 --> 00:04:46,500
 So I think the title is called "One Who Lives by the Dhamma

93
00:04:46,500 --> 00:04:48,200
" or something like that.

94
00:04:48,200 --> 00:04:52,800
 The Pali is Dhamma-rihari.

95
00:04:52,800 --> 00:04:55,170
 And so a person who just studies the Buddhist teaching,

96
00:04:55,170 --> 00:04:57,280
 this isn't considered to be a Dhamma-rihari.

97
00:04:57,280 --> 00:05:03,590
 A person who studies and then goes and teaches others, this

98
00:05:03,590 --> 00:05:06,440
 isn't a Dhamma-rihari.

99
00:05:06,440 --> 00:05:10,080
 A person who thinks about the teachings, goes and considers

100
00:05:10,080 --> 00:05:11,640
 and reflects and so on, this

101
00:05:11,640 --> 00:05:13,520
 isn't a Dhamma-rihari.

102
00:05:13,520 --> 00:05:17,930
 A person who chants and recites and memorizes the teachings

103
00:05:17,930 --> 00:05:20,600
, this also isn't a Dhamma-rihari.

104
00:05:20,600 --> 00:05:22,980
 But a person who does two things is considered to be

105
00:05:22,980 --> 00:05:24,720
 someone who lives by the Dhamma.

106
00:05:24,720 --> 00:05:28,110
 A person who takes the teachings, and the Buddha uses the

107
00:05:28,110 --> 00:05:29,320
 word "studies."

108
00:05:29,320 --> 00:05:32,890
 A person after they study, after they become full of

109
00:05:32,890 --> 00:05:35,120
 knowledge of the Buddha's teaching.

110
00:05:35,120 --> 00:05:40,400
 So he says, he's clear that the knowledge is a good thing.

111
00:05:40,400 --> 00:05:44,570
 Then they go off and practice tranquility meditation, calm

112
00:05:44,570 --> 00:05:46,520
 their mind down and develop

113
00:05:46,520 --> 00:05:47,520
 insight.

114
00:05:47,520 --> 00:05:53,140
 "Uttarintyasa pañjaya ataṁ bhajanāti" They come to see

115
00:05:53,140 --> 00:05:56,000
 clearly the meaning of that

116
00:05:56,000 --> 00:06:05,490
 knowledge, the meaning or the essence, the realization of

117
00:06:05,490 --> 00:06:10,280
 what is meant by the teachings.

118
00:06:10,280 --> 00:06:12,600
 So we can memorize the teachings and we can logically

119
00:06:12,600 --> 00:06:14,280
 accept them and we hear an "itjang

120
00:06:14,280 --> 00:06:18,240
 dukanganata" and permanent suffering, non-stop.

121
00:06:18,240 --> 00:06:22,280
 But the actual meaning we get from the practice.

122
00:06:22,280 --> 00:06:30,000
 "Uttarintyasa pañjaya, utarintyasa pañjaya, utarintyasa

123
00:06:30,000 --> 00:06:34,360
 pañjaya, utarintyasa pañjaya,

124
00:06:34,360 --> 00:06:40,410
 utarintyasa pañjaya, utarintyasa pañjaya, utarintyasa pa

125
00:06:40,410 --> 00:06:41,360
ñjaya, utarintyasa pañjaya,

126
00:06:41,360 --> 00:06:44,740
 utarintyasa pañjaya, utarintyasa pañjaya, utarintyasa pa

127
00:06:44,740 --> 00:06:49,360
ñjaya, utarintyasa pañjaya,

128
00:06:49,360 --> 00:06:51,250
 utarintyasa pañjaya, utarintyasa pañjaya, utarintyasa pa

129
00:06:51,250 --> 00:06:52,360
ñjaya, utarintyasa pañjaya,

130
00:06:52,360 --> 00:06:52,910
 utarintyasa pañjaya, utarintyasa pañjaya, utarintyasa pa

131
00:06:52,910 --> 00:06:53,360
ñjaya, utarintyasa pañjaya,

132
00:06:53,360 --> 00:06:54,150
 utarintyasa pañjaya, utarintyasa pañjaya, utarintyasa pa

133
00:06:54,150 --> 00:06:54,360
ñjaya, utarintyasa pañjaya,

134
00:06:54,360 --> 00:07:11,700
 utarintyasa pañjaya, utarintyasa pañjaya, utarintyasa pa

135
00:07:11,700 --> 00:07:14,360
ñjaya, utarintyasa pañjaya,

136
00:07:14,360 --> 00:07:18,600
 utarintyasa pañjaya, utarintyasa pañjaya, utarintyasa pa

137
00:07:18,600 --> 00:07:21,360
ñjaya, utarintyasa pañjaya,

138
00:07:21,360 --> 00:07:24,740
 utarintyasa pañjaya, utarintyasa pañjaya, utarintyasa pa

139
00:07:24,740 --> 00:07:28,360
ñjaya, utarintyasa pañjaya,

140
00:07:28,360 --> 00:07:31,030
 utarintyasa pañjaya, utarintyasa pañjaya, utarintyasa pa

141
00:07:31,030 --> 00:07:32,360
ñjaya, utarintyasa pañjaya,

