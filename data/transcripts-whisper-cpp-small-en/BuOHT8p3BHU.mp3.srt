1
00:00:00,000 --> 00:00:05,420
 Okay, good evening. So we're no longer doing live

2
00:00:05,420 --> 00:00:07,000
 broadcasts, I think, not in the immediate

3
00:00:07,000 --> 00:00:13,790
 future. The live is, well, doing it this way gives me the

4
00:00:13,790 --> 00:00:15,680
 option of having better quality

5
00:00:15,680 --> 00:00:22,530
 broadcasts, recordings. Anyway, most importantly it's for

6
00:00:22,530 --> 00:00:25,040
 you guys who are here doing the course.

7
00:00:26,520 --> 00:00:33,520
 It's the core of what I feel is my work as, well, in

8
00:00:33,520 --> 00:00:33,520
 sharing the Dhamma. Of course, the

9
00:00:33,520 --> 00:00:40,870
 core of my work is my own, but in terms of sharing the Dham

10
00:00:40,870 --> 00:00:44,400
ma with others, the core of

11
00:00:44,400 --> 00:00:48,360
 it from my perspective is you coming here to do the course,

12
00:00:48,360 --> 00:00:49,920
 so I put myself at your

13
00:00:49,920 --> 00:00:54,700
 disposal and if there's anything you need, let me know.

14
00:00:54,700 --> 00:00:56,920
 Part of it is giving regular

15
00:00:56,920 --> 00:01:01,450
 talks, so I'm happy to do that for you. Just to give

16
00:01:01,450 --> 00:01:04,040
 encouragement and background and direction,

17
00:01:04,040 --> 00:01:09,960
 trying to always keep it in line with the Buddha's teaching

18
00:01:09,960 --> 00:01:12,920
, not give you my perspective

19
00:01:12,920 --> 00:01:17,390
 on things, but give you something that I think is greater

20
00:01:17,390 --> 00:01:19,920
 than that and more valuable. It's

21
00:01:19,920 --> 00:01:25,350
 the Buddha's perspective. So tonight I thought I would talk

22
00:01:25,350 --> 00:01:28,080
 about something that seems to

23
00:01:28,080 --> 00:01:33,120
 have been very important to the Buddha and it seems fairly

24
00:01:33,120 --> 00:01:35,400
 easy to see why that is. And

25
00:01:35,400 --> 00:01:39,760
 not just important to the Buddha, but also important to his

26
00:01:39,760 --> 00:01:41,840
 followers, it seems. We know

27
00:01:41,840 --> 00:01:47,330
 this because we have it in several places, well, we have it

28
00:01:47,330 --> 00:01:48,840
 repeated. So something you

29
00:01:48,840 --> 00:01:51,460
 might know about the Buddha's teaching as it's been

30
00:01:51,460 --> 00:01:56,080
 recorded is there's a lot of repetition.

31
00:01:56,080 --> 00:01:59,560
 But in this case it's quite curious because we have a set

32
00:01:59,560 --> 00:02:02,400
 of discourses called the Majjhmanikaya,

33
00:02:02,400 --> 00:02:05,930
 the middle length discourses. It's one of the more

34
00:02:05,930 --> 00:02:09,400
 prominent sets because there's 152

35
00:02:09,400 --> 00:02:12,280
 different talks that the Buddha gave and they're medium

36
00:02:12,280 --> 00:02:14,440
 sized. They're not really the long

37
00:02:14,440 --> 00:02:18,400
 ones, but they're not also very short ones that are also

38
00:02:18,400 --> 00:02:20,720
 collected. They're sort of in

39
00:02:20,720 --> 00:02:23,560
 the middle and so they're a really good sort of entrance to

40
00:02:23,560 --> 00:02:24,960
 the Buddha's teaching. There's

41
00:02:24,960 --> 00:02:30,540
 just so much, such a diversity of topics. But one section

42
00:02:30,540 --> 00:02:31,960
 near the end has, I haven't

43
00:02:33,520 --> 00:02:37,800
 counted them, but there's several repetitions of this

44
00:02:37,800 --> 00:02:40,520
 teaching and they're not very long.

45
00:02:40,520 --> 00:02:45,820
 But it's the same teaching again and again and it's the

46
00:02:45,820 --> 00:02:48,880
 monks repeating it or the Buddha

47
00:02:48,880 --> 00:02:55,070
 teaching it to them in various situations. So it seems to

48
00:02:55,070 --> 00:02:55,880
 have been a teaching of the

49
00:02:56,520 --> 00:03:01,570
 Buddha that was kept very closely in mind by his followers.

50
00:03:01,570 --> 00:03:03,520
 Once you hear it you'll

51
00:03:03,520 --> 00:03:08,100
 I think understand why. It's called the Baddekarata, which

52
00:03:08,100 --> 00:03:10,600
 means a single excellent night. There's

53
00:03:10,600 --> 00:03:18,740
 a bit of controversy about that translation. Some people

54
00:03:18,740 --> 00:03:23,120
 translated us to something else

55
00:03:23,120 --> 00:03:30,120
 because of how there's an ambiguity in the word. But it

56
00:03:30,120 --> 00:03:30,120
 fairly clearly, from my perspective,

57
00:03:30,120 --> 00:03:40,130
 refers to having one good night. In Pali, something you

58
00:03:40,130 --> 00:03:41,280
 have to understand is the word

59
00:03:41,280 --> 00:03:49,160
 night is used in the same sense that we use the word day in

60
00:03:49,160 --> 00:03:51,840
 English. So when we talk about

61
00:03:51,880 --> 00:03:54,580
 having a good day, they would talk about having a good

62
00:03:54,580 --> 00:03:56,600
 night. Why we know that is because that's

63
00:03:56,600 --> 00:04:00,440
 how they talked about the passing of time. If you talk

64
00:04:00,440 --> 00:04:02,880
 about how long something has gone

65
00:04:02,880 --> 00:04:05,730
 on, you would say how many nights has it gone on. So

66
00:04:05,730 --> 00:04:07,640
 instead of saying how many days you've

67
00:04:07,640 --> 00:04:11,350
 been here, you say how many nights have you been here. When

68
00:04:11,350 --> 00:04:13,160
 you talk about seniority, they

69
00:04:13,160 --> 00:04:16,380
 would ask, they would talk about it in terms of having more

70
00:04:16,380 --> 00:04:18,240
 nights. There's a term called

71
00:04:18,240 --> 00:04:23,490
 Ratanyu, which means one who knows many nights. Ratanyu is

72
00:04:23,490 --> 00:04:25,240
 the most senior. So we have a monk.

73
00:04:25,240 --> 00:04:29,540
 In the Buddhist teaching, the most senior monk is called An

74
00:04:29,540 --> 00:04:31,840
yokondanya. He was the first

75
00:04:31,840 --> 00:04:36,660
 ordained piku. So he's called Ratanyu, one who knows, one

76
00:04:36,660 --> 00:04:38,840
 who has lived through many

77
00:04:38,840 --> 00:04:46,560
 nights. So when we hear Baddekarata, eka means one. Baddha

78
00:04:46,560 --> 00:04:47,880
 is like bandhana. It's

79
00:04:47,880 --> 00:04:52,190
 like bandhana. Baddha means exalted. It translates as

80
00:04:52,190 --> 00:04:53,880
 excellent. But it really means something

81
00:04:53,880 --> 00:04:58,530
 like exalted. When you put someone up high, you call them

82
00:04:58,530 --> 00:05:00,880
 badanta. Badanta is a word you

83
00:05:00,880 --> 00:05:07,220
 refer to someone who is high, who you put up, like reverend

84
00:05:07,220 --> 00:05:09,520
 or venerable. So having

85
00:05:09,520 --> 00:05:16,380
 one venerable night, no, probably one exalted night. But in

86
00:05:16,380 --> 00:05:16,560
 colloquial terms, it just means

87
00:05:17,760 --> 00:05:22,260
 having a good day. When you have a really good day, one

88
00:05:22,260 --> 00:05:24,760
 good day. Because it's a way

89
00:05:24,760 --> 00:05:31,760
 of describing what you need, what is required, what is the

90
00:05:31,760 --> 00:05:32,800
 goal. The goal is this one moment

91
00:05:32,800 --> 00:05:39,320
 where you see everything clearly. Getting to the point

92
00:05:39,320 --> 00:05:43,040
 where you have a good day because

93
00:05:43,040 --> 00:05:46,750
 it refers, or it hints back to the Buddha's good day and

94
00:05:46,750 --> 00:05:48,760
 the Buddha's good night. When

95
00:05:48,760 --> 00:05:51,690
 the Buddha sat under the Bodhi tree and stayed up all night

96
00:05:51,690 --> 00:05:53,280
 and throughout the night and

97
00:05:53,280 --> 00:05:56,940
 three watches of the night he saw three different things

98
00:05:56,940 --> 00:05:59,480
 and the third watch was where he freed

99
00:05:59,480 --> 00:06:05,540
 him. He became free from suffering. So it's in that vein

100
00:06:05,540 --> 00:06:06,480
 that we think of that as a way

101
00:06:06,480 --> 00:06:13,480
 of describing the good night. So the teaching starts with

102
00:06:13,480 --> 00:06:13,480
 something that should be very

103
00:06:13,480 --> 00:06:22,110
 familiar to you. I repeat it and you have surely heard it

104
00:06:22,110 --> 00:06:28,640
 from other places in Buddhism.

105
00:06:35,640 --> 00:06:40,230
 Don't go back to the past or bring up the past and don't

106
00:06:40,230 --> 00:06:42,640
 worry about the future. Which

107
00:06:42,640 --> 00:06:51,560
 in the past has gone already, which in the future has not

108
00:06:51,560 --> 00:06:55,200
 come, not yet come. It's a

109
00:06:55,200 --> 00:07:01,610
 fairly simple teaching and it is, I think, a good doorway,

110
00:07:01,610 --> 00:07:04,980
 a good entrance to the practice

111
00:07:04,980 --> 00:07:10,420
 of mindfulness to learn about being in the present moment.

112
00:07:10,420 --> 00:07:11,400
 Being in the past and in the

113
00:07:11,400 --> 00:07:15,810
 future, they're not the only problems, of course, but they

114
00:07:15,810 --> 00:07:17,800
're the first problem that

115
00:07:17,800 --> 00:07:21,980
 we come to in meditation. The first challenge that we work

116
00:07:21,980 --> 00:07:24,800
 on is in terms of trying to keep

117
00:07:24,800 --> 00:07:29,040
 our mind from planning, from living in the future and to

118
00:07:29,040 --> 00:07:31,200
 keep our mind from dwelling

119
00:07:31,200 --> 00:07:38,200
 in the past. It's a common problem that can become quite

120
00:07:38,200 --> 00:07:38,200
 intense for people when they

121
00:07:38,200 --> 00:07:43,590
 are mourning loss or when they've had things happen to them

122
00:07:43,590 --> 00:07:45,960
 in the past that were just

123
00:07:45,960 --> 00:07:50,600
 traumatic and meditators who were traumatized so that they

124
00:07:50,600 --> 00:07:53,040
 couldn't let go of the past.

125
00:07:53,040 --> 00:07:55,600
 People who are worried about the future, anxious about the

126
00:07:55,600 --> 00:07:57,720
 future, can become quite an obsession.

127
00:07:57,720 --> 00:08:00,010
 Of course, for all of us, it's a problem, right? When you

128
00:08:00,010 --> 00:08:01,120
 finish the course, you know

129
00:08:01,120 --> 00:08:05,890
 you start thinking about the future. It's like drilling

130
00:08:05,890 --> 00:08:08,120
 into your head, you can't get

131
00:08:08,120 --> 00:08:15,760
 it out. Very strong, you'll notice it can be very strong.

132
00:08:15,760 --> 00:08:19,870
 So a very important teaching that's not mentioned here, but

133
00:08:19,870 --> 00:08:21,800
 what this relates to is the Buddha's

134
00:08:21,800 --> 00:08:26,530
 teaching on how it, what is the reality of living in the

135
00:08:26,530 --> 00:08:28,800
 past and in the future. He talks

136
00:08:28,800 --> 00:08:34,340
 about, I think in the Jataka, he talks about it like when

137
00:08:34,340 --> 00:08:37,200
 you cut grass, when you live

138
00:08:37,200 --> 00:08:40,130
 in the past or when you live in the future, you're cut off

139
00:08:40,130 --> 00:08:42,120
 from reality. So your mind

140
00:08:42,120 --> 00:08:47,490
 dries up like when you cut grass. There's a verse that goes

141
00:08:47,490 --> 00:08:49,120
 something like, "For the

142
00:08:49,760 --> 00:08:53,870
 past I do not mourn nor for the future weep. I take the

143
00:08:53,870 --> 00:08:56,200
 present as it comes and thus my

144
00:08:56,200 --> 00:08:59,120
 color keep." That's a translation of the Jataka that they

145
00:08:59,120 --> 00:09:03,200
 turned into poetry.

146
00:09:03,200 --> 00:09:07,550
 But the problem is when you live in the past or live in the

147
00:09:07,550 --> 00:09:09,800
 future, it's abstract, it's

148
00:09:09,800 --> 00:09:13,670
 conceptual. It's a very different state of mind than being

149
00:09:13,670 --> 00:09:15,320
 mindful. If you sit here and

150
00:09:15,320 --> 00:09:18,160
 you're aware of the breath, you're aware of yourself

151
00:09:18,160 --> 00:09:20,320
 sitting, you're aware of the sound

152
00:09:20,320 --> 00:09:23,430
 you hear, you're aware of the sights and so on, you have a

153
00:09:23,430 --> 00:09:25,000
 very simple connection with

154
00:09:25,000 --> 00:09:30,930
 reality, a very simple experience of things. So there's

155
00:09:30,930 --> 00:09:32,000
 very little stress and it's not

156
00:09:32,000 --> 00:09:39,370
 very taxing on the mind. When you start getting into the

157
00:09:39,370 --> 00:09:40,560
 past and the future, by its very

158
00:09:40,560 --> 00:09:44,470
 nature it requires more mind work and there's of course

159
00:09:44,470 --> 00:09:46,760
 much more that can come up. If you

160
00:09:46,760 --> 00:09:50,110
 see something, it's only very simple defilements that can

161
00:09:50,110 --> 00:09:52,080
 come up. Maybe you like it or you

162
00:09:52,080 --> 00:09:56,820
 dislike it, but if you get into abstract thinking, so many

163
00:09:56,820 --> 00:09:59,080
 of your phobias or neuroses can come

164
00:09:59,080 --> 00:10:08,880
 up. So many potential problems, addictions and so on. So

165
00:10:08,880 --> 00:10:08,880
 staying out of the past, you

166
00:10:10,880 --> 00:10:11,820
 know, so staying out of the past and the future becomes a

167
00:10:11,820 --> 00:10:14,040
 very important teaching. But there's

168
00:10:14,040 --> 00:10:17,980
 another part to this and it's sort of another step. There's

169
00:10:17,980 --> 00:10:20,000
 two parts, two main parts to

170
00:10:20,000 --> 00:10:24,020
 this teaching. So the next part goes, "Bhacupananchayodham

171
00:10:24,020 --> 00:10:27,000
am datta tatta vipassati." And it's easy to

172
00:10:27,000 --> 00:10:31,990
 sort of skip this one over because it says, "Whatever dham

173
00:10:31,990 --> 00:10:34,720
mas arise in the present moment

174
00:10:36,680 --> 00:10:42,680
 in front of you, see them clearly." And so you might just

175
00:10:42,680 --> 00:10:43,680
 take this as an extension of

176
00:10:43,680 --> 00:10:47,460
 the first part whereby we, okay, stay in the present, but

177
00:10:47,460 --> 00:10:49,200
 it's not enough to be in the

178
00:10:49,200 --> 00:10:53,630
 present. It's almost enough or in a way it's enough. It's

179
00:10:53,630 --> 00:10:56,040
 not simply to be in the present,

180
00:10:56,040 --> 00:10:58,990
 but it's to see the present clearly. And the commentary

181
00:10:58,990 --> 00:11:00,760
 makes this clear that, oh no, the

182
00:11:00,760 --> 00:11:04,390
 Buddha actually makes this clear when he talks about it. He

183
00:11:04,390 --> 00:11:06,260
 just, he explains this after

184
00:11:06,260 --> 00:11:10,900
 he gives this couple of verses. He explains it and he says

185
00:11:10,900 --> 00:11:13,260
 it's possible to be defeated

186
00:11:13,260 --> 00:11:17,230
 by the present moment, even when you're out of the past and

187
00:11:17,230 --> 00:11:18,800
 out of the future. Even the

188
00:11:18,800 --> 00:11:21,950
 present moment, of course, can be a cause for the arising

189
00:11:21,950 --> 00:11:24,160
 of conceptual thought, specifically

190
00:11:24,160 --> 00:11:28,050
 of the arising of ego, the idea that this is me, this is

191
00:11:28,050 --> 00:11:31,160
 mine. If you see something or

192
00:11:31,160 --> 00:11:34,420
 anything, if you hear something, someone says to you, "You

193
00:11:34,420 --> 00:11:36,200
're good, you're bad." If someone

194
00:11:36,200 --> 00:11:39,060
 says good things about you, you're puffed up. If someone

195
00:11:39,060 --> 00:11:40,480
 says bad things about you,

196
00:11:40,480 --> 00:11:45,790
 you're angry or upset or depressed or lowered into your

197
00:11:45,790 --> 00:11:47,480
 self-esteem. So it's not simply

198
00:11:47,480 --> 00:11:50,460
 to be in the present moment, it's to see clearly in the

199
00:11:50,460 --> 00:11:52,300
 present moment. The point being you

200
00:11:52,300 --> 00:11:55,150
 can only see the present, it's only the present moment that

201
00:11:55,150 --> 00:11:57,480
 you can see clearly. It's not

202
00:11:57,480 --> 00:12:00,180
 possible to see clearly about the past or the future, not

203
00:12:00,180 --> 00:12:01,440
 in the way that the Buddha

204
00:12:01,440 --> 00:12:05,140
 describes. A vipassati, if you've caught that, that's just

205
00:12:05,140 --> 00:12:06,480
 another form of the word

206
00:12:06,480 --> 00:12:11,660
 vipassana. Vipassana is the noun, vipassati is the verb.

207
00:12:11,660 --> 00:12:13,480
 One sees clearly. So seeing the

208
00:12:13,480 --> 00:12:16,920
 present clearly is a description of what you're doing when

209
00:12:16,920 --> 00:12:18,920
 you practice mindfulness and it's

210
00:12:18,920 --> 00:12:23,100
 the practice of mindfulness. It is the heart of this

211
00:12:23,100 --> 00:12:25,920
 teaching. It allows you to break through

212
00:12:25,920 --> 00:12:31,880
 conception, concepts and abstract thinking and see clearly

213
00:12:31,880 --> 00:12:34,240
 reality in a very simple sense

214
00:12:34,240 --> 00:12:39,520
 of what's really there, not what you think is there, not

215
00:12:39,520 --> 00:12:43,760
 what someone tells you is there,

216
00:12:43,760 --> 00:12:49,210
 not what you believe is there. Of course there's so much

217
00:12:49,210 --> 00:12:50,760
 that comes from that. You'll start

218
00:12:50,760 --> 00:12:55,700
 to see suffering and why you suffer. You'll start to see

219
00:12:55,700 --> 00:12:59,160
 your emotions, positive and negative

220
00:12:59,160 --> 00:13:04,220
 qualities of mind that are beneficial to yourself and

221
00:13:04,220 --> 00:13:07,520
 others and harmful to yourselves and others.

222
00:13:08,680 --> 00:13:12,980
 And you'll work through that. It's a process of change,

223
00:13:12,980 --> 00:13:15,680
 mindfulness, insight, clarity of

224
00:13:15,680 --> 00:13:21,050
 mind. It changes so much. But hopefully you can start to

225
00:13:21,050 --> 00:13:23,760
 see that it cleans so much. That's

226
00:13:23,760 --> 00:13:27,210
 the essence of the Badekarata teaching. The rest is more

227
00:13:27,210 --> 00:13:29,000
 like a pep talk. It's a very

228
00:13:29,000 --> 00:13:33,820
 beautiful teaching. It's very short and concise. That's the

229
00:13:33,820 --> 00:13:36,000
 first half. Andita ananwa kameya.

230
00:13:37,280 --> 00:13:40,490
 My teacher is one of the things he said most. Anytime

231
00:13:40,490 --> 00:13:42,960
 anyone was in the past or in the future,

232
00:13:42,960 --> 00:13:45,790
 which of course often happens, he would repeat this verse

233
00:13:45,790 --> 00:13:47,360
 in Pali and then just the first

234
00:13:47,360 --> 00:13:53,560
 part and then translate it into Thai. It's a very powerful

235
00:13:53,560 --> 00:13:54,360
 one. And it's expressed in

236
00:13:54,360 --> 00:13:58,790
 many different ways in the Buddhist teaching. But here this

237
00:13:58,790 --> 00:14:00,920
 verse is literally repeated

238
00:14:00,920 --> 00:14:07,320
 several times in a row and several suttas. So the next part

239
00:14:07,320 --> 00:14:07,920
 relates more to why we should

240
00:14:07,920 --> 00:14:14,750
 do this. It's sort of a classic carrot and stick, talking

241
00:14:14,750 --> 00:14:17,960
 about the dangers if you don't

242
00:14:17,960 --> 00:14:22,880
 or the bad stuff, reasons why you should because there's

243
00:14:22,880 --> 00:14:25,520
 bad things and the good things that

244
00:14:25,520 --> 00:14:32,160
 come of it. So the first part is asangi rangasankupang.

245
00:14:32,160 --> 00:14:32,520
 This is unshakable. This is invincible.

246
00:14:32,520 --> 00:14:39,320
 Dangvidvama nubruhaye. One should be sure of this. Knowing

247
00:14:39,320 --> 00:14:41,800
 this one should be sure of

248
00:14:41,800 --> 00:14:48,400
 it. Something like that. I want to pause here because this

249
00:14:48,400 --> 00:14:49,720
 is I think a very important statement

250
00:14:52,440 --> 00:14:56,790
 and I mention this often to meditators and in my talks

251
00:14:56,790 --> 00:14:59,440
 about how perfect and potent,

252
00:14:59,440 --> 00:15:08,350
 how efficacious is a big word, means how this practice is

253
00:15:08,350 --> 00:15:13,240
 able to bring about results. So

254
00:15:17,440 --> 00:15:21,700
 when we think of all of the methods and schemes and plans

255
00:15:21,700 --> 00:15:24,440
 we have for finding happiness and

256
00:15:24,440 --> 00:15:29,050
 peace and stability and so on, all of our plans and

257
00:15:29,050 --> 00:15:32,600
 ambitions, they fall short. They

258
00:15:32,600 --> 00:15:39,240
 can't help but fail because they're not permanent. They don

259
00:15:39,240 --> 00:15:42,280
't lead to something that's permanent.

260
00:15:42,280 --> 00:15:47,390
 The idea of a thing that is permanent is a red herring in

261
00:15:47,390 --> 00:15:49,280
 the first place. If you try

262
00:15:49,280 --> 00:15:53,790
 to create stability in your life, you know that that's only

263
00:15:53,790 --> 00:15:55,840
 temporary. If you try to

264
00:15:55,840 --> 00:15:59,270
 create pleasure, you should know that that's only temporary

265
00:15:59,270 --> 00:16:01,120
 as well and problematic because

266
00:16:01,120 --> 00:16:07,820
 these attempts that we make get us into dependency. And

267
00:16:07,820 --> 00:16:08,120
 that's the key is because seeing clearly

268
00:16:10,160 --> 00:16:13,710
 and being mindful and being in the present moment is

269
00:16:13,710 --> 00:16:16,480
 independent. You can never be taken

270
00:16:16,480 --> 00:16:21,020
 away from the present moment. No one can say to you, "We're

271
00:16:21,020 --> 00:16:22,840
 going to put you over here

272
00:16:22,840 --> 00:16:26,990
 and remove you from the present moment." Experience is the

273
00:16:26,990 --> 00:16:29,160
 one thing that no one can take away

274
00:16:29,160 --> 00:16:33,860
 from you. So if experience becomes your refuge rather than

275
00:16:33,860 --> 00:16:36,160
 a curse or something that you're

276
00:16:36,560 --> 00:16:39,960
 constantly running away from, which is a very important

277
00:16:39,960 --> 00:16:41,920
 point, that's what we spend a lot

278
00:16:41,920 --> 00:16:45,940
 of our time doing. We're so afraid or incapable of being in

279
00:16:45,940 --> 00:16:48,060
 the present moment that we are

280
00:16:48,060 --> 00:16:51,580
 always running into the past or the future, chasing after

281
00:16:51,580 --> 00:16:53,760
 pleasures, chasing after things

282
00:16:53,760 --> 00:16:57,910
 that can't possibly satisfy us. That the present is

283
00:16:57,910 --> 00:17:00,760
 something so unfamiliar, scary, uninteresting,

284
00:17:05,520 --> 00:17:12,170
 impulsive almost. If we can change that by coming to be

285
00:17:12,170 --> 00:17:12,520
 present and be alert and have

286
00:17:12,520 --> 00:17:23,020
 a clarity of mind about it, we become invincible. That's an

287
00:17:23,020 --> 00:17:26,360
 important word. The only way you

288
00:17:26,360 --> 00:17:29,510
 can become invincible is if you're able to accept

289
00:17:29,510 --> 00:17:32,360
 everything, if you're not accept but

290
00:17:32,360 --> 00:17:38,530
 able to be unfazed, unmoved, unshaken, a sangiranga sangup

291
00:17:38,530 --> 00:17:39,360
ang. This is an important thing for

292
00:17:39,360 --> 00:17:45,550
 you to understand and to get a sense of, regardless of what

293
00:17:45,550 --> 00:17:48,800
 results you've gained from the practice

294
00:17:48,800 --> 00:17:53,430
 otherwise like peace or happiness or all these good things

295
00:17:53,430 --> 00:17:56,040
 we talk about. Knowing that is

296
00:17:56,040 --> 00:17:59,990
 far more powerful because if you do the right thing, this

297
00:17:59,990 --> 00:18:02,240
 is an important concept as well

298
00:18:02,240 --> 00:18:05,950
 if you do the right thing, whatever that means, once you

299
00:18:05,950 --> 00:18:08,160
 get a sense that this is the right

300
00:18:08,160 --> 00:18:10,720
 thing, if you're clear on that, you don't have to ever

301
00:18:10,720 --> 00:18:12,240
 worry about what's going to be

302
00:18:12,240 --> 00:18:14,800
 the result. You don't have to say, "Okay, I'm doing the

303
00:18:14,800 --> 00:18:16,120
 right thing but is it going to

304
00:18:16,120 --> 00:18:20,760
 lead to good things?" It's the wrong question. It's an

305
00:18:20,760 --> 00:18:23,120
 absurd question because the only way

306
00:18:23,120 --> 00:18:27,650
 it could be right is if it were leading to good things. If

307
00:18:27,650 --> 00:18:28,720
 you do that, it helps very

308
00:18:28,720 --> 00:18:32,280
 much to overcome doubt. You don't have to worry about

309
00:18:32,280 --> 00:18:34,880
 results ever. It's a very important

310
00:18:34,880 --> 00:18:38,880
 quality of mindfulness that you never worry about results.

311
00:18:38,880 --> 00:18:40,600
 You're always focused on, "Is

312
00:18:40,600 --> 00:18:44,480
 it right? Is this truly unshakable? Is this truly

313
00:18:44,480 --> 00:18:47,600
 invincible? Am I truly doing something?"

314
00:18:47,600 --> 00:18:55,330
 And doing something that will make me safe. The reason why

315
00:18:55,330 --> 00:18:56,080
 we can say that this is the

316
00:18:57,160 --> 00:19:00,710
 case with such assurance is because of how simple it is,

317
00:19:00,710 --> 00:19:02,560
 because of how pure it is. I'm

318
00:19:02,560 --> 00:19:07,070
 not telling you to believe in God or believe in the Buddha

319
00:19:07,070 --> 00:19:09,360
 or pray or do rituals or take

320
00:19:09,360 --> 00:19:13,840
 on faith some doctrine or so on. I'm saying, "See clearly

321
00:19:13,840 --> 00:19:15,640
 what's here and now." So when

322
00:19:15,640 --> 00:19:19,640
 this becomes the teaching, the core teaching with nothing

323
00:19:19,640 --> 00:19:21,960
 else, unadulterated, then you

324
00:19:21,960 --> 00:19:26,490
 can have perfect confidence. If you're straight in your own

325
00:19:26,490 --> 00:19:28,960
 mind, you can have perfect confidence

326
00:19:28,960 --> 00:19:35,960
 that it's the right thing. It is what it says to be.

327
00:19:35,960 --> 00:19:42,560
 And then the next part, "Ajai wakichamatapang kocanya maran

328
00:19:42,560 --> 00:19:44,960
ang suey." Another important

329
00:19:46,000 --> 00:19:51,000
 part of it that's often repeated, "Ajai wakichamatapang."

330
00:19:51,000 --> 00:19:53,000
 Today the task should be done with effort.

331
00:19:53,000 --> 00:20:02,580
 Today the work should be done with effort. "Kocanya maran

332
00:20:02,580 --> 00:20:04,280
ang suey." Who knows whether

333
00:20:04,280 --> 00:20:10,180
 death might come even tomorrow. This is one of the reasons

334
00:20:10,180 --> 00:20:11,280
 anyway. It's an important reminder

335
00:20:11,280 --> 00:20:15,370
 we don't know what's coming in the future. We don't know

336
00:20:15,370 --> 00:20:18,280
 death, probably not, because

337
00:20:18,280 --> 00:20:22,240
 it's not the most common thing for someone to die though.

338
00:20:22,240 --> 00:20:24,280
 Absolutely there's a chance

339
00:20:24,280 --> 00:20:28,780
 that any one of us in this room or listening here could die

340
00:20:28,780 --> 00:20:31,280
 tomorrow. Nobody knows. But

341
00:20:31,280 --> 00:20:35,490
 anything could happen. Someone near you could die. Someone

342
00:20:35,490 --> 00:20:38,280
's, you could, you could die.

343
00:20:39,280 --> 00:20:46,280
 Someone's, you could lose something. You could get sick.

344
00:20:46,280 --> 00:20:46,280
 Anything could happen. There could

345
00:20:46,280 --> 00:20:50,530
 be a tsunami, you know, when we were in Thailand and the

346
00:20:50,530 --> 00:20:53,520
 tsunami came. How many people their

347
00:20:53,520 --> 00:20:58,080
 whole life was destroyed, even if they did survive the

348
00:20:58,080 --> 00:21:00,520
 horror of the aftermath of that

349
00:21:00,520 --> 00:21:07,370
 big tsunami. Anything can happen. So we do the work today,

350
00:21:07,370 --> 00:21:08,320
 get ready, because

351
00:21:09,280 --> 00:21:13,150
 all of that relates to change and it's change that creates

352
00:21:13,150 --> 00:21:16,280
 this instability and suffering.

353
00:21:16,280 --> 00:21:21,960
 And so the part of being invincible is the being flexible,

354
00:21:21,960 --> 00:21:24,920
 being able to adapt. How can

355
00:21:24,920 --> 00:21:29,520
 you adapt? By making your base and your refuge experience.

356
00:21:29,520 --> 00:21:31,920
 Because once you understand and

357
00:21:31,920 --> 00:21:35,210
 are familiar with experience and are unfazed and unshaken

358
00:21:35,210 --> 00:21:37,520
 by it, then you've covered everything

359
00:21:37,520 --> 00:21:44,520
 because there's nothing but experience. There's no

360
00:21:44,520 --> 00:21:44,520
 bargaining with it. Death. There's

361
00:21:44,520 --> 00:22:04,760
 no bargaining with death. Death with its great army. Maha

362
00:22:04,760 --> 00:22:05,400
 say Nina.

363
00:22:06,400 --> 00:22:10,870
 Yeah. Death, death is, I mean it is special. You could talk

364
00:22:10,870 --> 00:22:13,400
 about, talk like this about

365
00:22:13,400 --> 00:22:18,930
 anything but death is a very good example and probably the

366
00:22:18,930 --> 00:22:22,000
 prime one. Because, well,

367
00:22:22,000 --> 00:22:26,220
 we don't know, not only do we not know when it's going to

368
00:22:26,220 --> 00:22:28,640
 come, we know that it is going

369
00:22:28,640 --> 00:26:36,230
 to come. And it's very much, it's quite a vivid imagery,

370
00:26:36,230 --> 00:22:34,760
 this idea of death with its

371
00:22:34,760 --> 00:22:39,580
 armies. I see that if you, well you guys, some of you are

372
00:22:39,580 --> 00:22:41,760
 fairly young, but as you get

373
00:22:41,760 --> 00:22:45,350
 older and people start dying around you, you start to get a

374
00:22:45,350 --> 00:22:47,200
 sense of death being like an

375
00:22:47,200 --> 00:22:52,000
 army. I mean I'm still young, I'm 40. But, you know, as

376
00:22:52,000 --> 00:22:54,200
 three of my grandparents were

377
00:22:54,200 --> 00:22:59,060
 gone, I had a cousin fall off a roof a few years ago, he's

378
00:22:59,060 --> 00:23:01,560
 dead. My stepfather just found

379
00:23:01,760 --> 00:23:06,990
 out he has cancer. Death is coming and we can feel the

380
00:23:06,990 --> 00:23:08,760
 fortress weakening around us.

381
00:23:08,760 --> 00:23:13,570
 You look and you see, you're getting crow's nest, gray hair

382
00:23:13,570 --> 00:23:16,120
, arthritis, you're getting

383
00:23:16,120 --> 00:23:20,970
 fat and old and your mind is growing weak and so on. You

384
00:23:20,970 --> 00:23:23,600
 can feel death pounding down

385
00:23:23,600 --> 00:23:31,440
 the doors. Maha say Nina, great army, it's not just you. It

386
00:23:31,440 --> 00:23:31,440
's not just you. It's not

387
00:23:31,640 --> 00:23:35,400
 just one threat. It's like one of those horror movies and

388
00:23:35,400 --> 00:23:37,520
 you're in the house and people

389
00:23:37,520 --> 00:23:42,180
 start dying around you and you just know you're next. May I

390
00:23:42,180 --> 00:23:44,520
 say it with a smile because we

391
00:23:44,520 --> 00:23:49,200
 kind of have something powerful against it. And that's this

392
00:23:49,200 --> 00:23:51,520
 teaching. I mean really the

393
00:23:51,520 --> 00:23:56,820
 essence of the Baddhicarata Sutta. So then the Buddha says,

394
00:23:56,820 --> 00:23:58,520
 "Ey wang whi haring

395
00:24:00,680 --> 00:24:05,780
 aata bing, ahoratam a tanditam." One working thus day and

396
00:24:05,780 --> 00:24:07,680
 night, ahoratam. A tanditam not

397
00:24:07,680 --> 00:24:19,650
 flagging, not relaxing, not slagging. What's the word? Sl

398
00:24:19,650 --> 00:24:20,560
agging off.

399
00:24:27,440 --> 00:24:32,070
 Tang whi baddhicaratoti. Such a person is called a baddhic

400
00:24:32,070 --> 00:24:34,440
arata. One who has a single

401
00:24:34,440 --> 00:24:44,230
 excellent night, has a good night, a good day. Santo ajikat

402
00:24:44,230 --> 00:24:44,920
emuni. Santo ajikatemuni. I'm

403
00:24:52,320 --> 00:24:54,840
 not sure if that's calling, I think that might be calling

404
00:24:54,840 --> 00:24:56,600
 the Buddha, talking about the Buddha

405
00:24:56,600 --> 00:24:59,820
 as being peaceful. Actually I'm not, yeah I think it's the

406
00:24:59,820 --> 00:25:01,400
 peaceful sage. Thus says

407
00:25:01,400 --> 00:25:04,680
 the Buddha who is the peaceful sage. But I wanted to

408
00:25:04,680 --> 00:25:06,360
 highlight the word santa, santo

409
00:25:06,360 --> 00:25:10,450
 ajikatemuni. So the Buddha was a muni, he was wise, but he

410
00:25:10,450 --> 00:25:13,160
 was also peaceful. And I

411
00:25:13,160 --> 00:25:16,360
 wanted to highlight that word because well again that's

412
00:25:16,360 --> 00:25:18,120
 what you should gain from the

413
00:25:18,120 --> 00:25:22,080
 practice. Ultimately it is about finding peace. You stay in

414
00:25:22,080 --> 00:25:24,080
 the present moment, you learn

415
00:25:24,080 --> 00:25:27,260
 about reality, you become more comfortable and more

416
00:25:27,260 --> 00:25:29,400
 familiar with the way your own mind

417
00:25:29,400 --> 00:25:34,160
 works. So much changes and what changes is the stress and

418
00:25:34,160 --> 00:25:36,400
 the chaos in the mind, the

419
00:25:36,400 --> 00:25:41,870
 suffering in the mind is reduced and there's a great peace

420
00:25:41,870 --> 00:25:44,240
 that comes from that. So I've

421
00:25:47,600 --> 00:25:50,980
 gotten into the habit of wishing people to have a good day

422
00:25:50,980 --> 00:25:52,600
 and now you know it wasn't

423
00:25:52,600 --> 00:25:55,140
 just because of being polite, I think it's a good polit

424
00:25:55,140 --> 00:25:56,720
eness. Wishing people to have

425
00:25:56,720 --> 00:26:00,920
 a good day is kind of a secret way of wishing for them to

426
00:26:00,920 --> 00:26:03,720
 have a real good day. An exalted

427
00:26:03,720 --> 00:26:08,500
 day is an excellent day, an excellent night where they are

428
00:26:08,500 --> 00:26:10,800
 able to find true peace and

429
00:26:11,920 --> 00:26:16,550
 free themselves from stress and suffering. So there you go,

430
00:26:16,550 --> 00:26:18,920
 that's the demo for tonight.

431
00:26:18,920 --> 00:26:23,920
 Thank you all for listening. Have a good night.

432
00:26:23,920 --> 00:26:24,920
 [

433
00:26:24,920 --> 00:26:35,920
 1

