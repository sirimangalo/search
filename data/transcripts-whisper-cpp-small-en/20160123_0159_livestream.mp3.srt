1
00:00:00,000 --> 00:00:05,000
 Good evening everyone.

2
00:00:05,000 --> 00:00:13,000
 Broadcasting Live, January 22nd, 2016.

3
00:00:13,000 --> 00:00:22,000
 How's everyone tonight?

4
00:00:22,000 --> 00:00:43,000
 Good.

5
00:00:43,000 --> 00:00:46,000
 What's your question?

6
00:00:46,000 --> 00:00:51,000
 Bante, I am a beginner. Is this thought correct?

7
00:00:51,000 --> 00:01:09,000
 Is that the beginning or the next question?

8
00:01:09,000 --> 00:01:15,190
 Daily activities, as a lay person, need concentration on

9
00:01:15,190 --> 00:01:20,000
 the activities and concentrating on mind.

10
00:01:20,000 --> 00:01:23,000
 Don't understand.

11
00:01:23,000 --> 00:01:28,380
 Maybe the grammar is just not right, but you have to focus

12
00:01:28,380 --> 00:01:31,000
 on your daily activities.

13
00:01:31,000 --> 00:01:36,000
 And so you can't meditate, it's not the idea.

14
00:01:36,000 --> 00:01:39,180
 If you are able to be mindful throughout your daily

15
00:01:39,180 --> 00:01:44,000
 activities, then you'd be an arahant pretty much.

16
00:01:44,000 --> 00:01:48,250
 So don't worry about that. Just be a little bit mindful if

17
00:01:48,250 --> 00:01:50,000
 you can, you know?

18
00:01:50,000 --> 00:01:54,000
 It will help you with your daily activities.

19
00:01:54,000 --> 00:02:18,000
 [silence]

20
00:02:18,000 --> 00:02:24,330
 Okay, here's a big question. 17 years old. Should I learn

21
00:02:24,330 --> 00:02:31,000
 more from the normal videos, or is this before somewhere?

22
00:02:31,000 --> 00:02:37,730
 Yeah, I mean the kids' videos, a lot of adults have talked

23
00:02:37,730 --> 00:02:43,000
 about how they're useful for adults as well.

24
00:02:43,000 --> 00:02:48,620
 I mean the point of the children's videos was to give an

25
00:02:48,620 --> 00:02:52,000
 understanding of why we're doing it, you know?

26
00:02:52,000 --> 00:02:59,280
 These kids have a hard time understanding why we would want

27
00:02:59,280 --> 00:03:03,000
 to focus on ourselves, so it's a build-up to it.

28
00:03:03,000 --> 00:03:06,720
 So I mean that's useful for adults as well, to really get a

29
00:03:06,720 --> 00:03:08,000
 sense that this kind of meditation,

30
00:03:08,000 --> 00:03:14,020
 where you actually repeat a word, is probably the most

31
00:03:14,020 --> 00:03:20,000
 ancient and common sort of meditation technique.

32
00:03:20,000 --> 00:03:24,230
 And you can see why. I mean if you use those kids' videos

33
00:03:24,230 --> 00:03:26,000
 for kids, a three-year-old,

34
00:03:26,000 --> 00:03:29,670
 I've gotten feedback that a three-year-old is able to

35
00:03:29,670 --> 00:03:31,000
 understand it.

36
00:03:31,000 --> 00:03:36,370
 Apparently they can't become enlightened. You have to be

37
00:03:36,370 --> 00:03:41,000
 seven years old or older to become enlightened.

38
00:03:41,000 --> 00:03:47,490
 But I don't know, that's what the commentaries say, I think

39
00:03:47,490 --> 00:03:48,000
.

40
00:03:48,000 --> 00:03:53,000
 But regardless, there's benefit.

41
00:03:53,000 --> 00:03:59,160
 So I mean those videos do work for kids, but you're welcome

42
00:03:59,160 --> 00:04:01,000
 to watch them.

43
00:04:01,000 --> 00:04:05,710
 I wouldn't put too much focus, obviously, on any of the

44
00:04:05,710 --> 00:04:09,000
 videos for kids except the last one.

45
00:04:09,000 --> 00:04:21,350
 But the last one is sort of a hesitantly optimistic, I'm

46
00:04:21,350 --> 00:04:22,000
 trying to say.

47
00:04:22,000 --> 00:04:26,000
 It's what I'd like to present to people.

48
00:04:26,000 --> 00:04:32,090
 And I'm hesitant about it because it turns the four satipat

49
00:04:32,090 --> 00:04:34,000
thani into five,

50
00:04:34,000 --> 00:04:38,710
 which, you know, obviously who am I to start having new

51
00:04:38,710 --> 00:04:40,000
 enumerations,

52
00:04:40,000 --> 00:04:45,080
 but practically speaking having five groups, the body, the

53
00:04:45,080 --> 00:04:47,000
 feelings, the thoughts,

54
00:04:47,000 --> 00:04:52,740
 the emotions and the senses give you five groups, five ways

55
00:04:52,740 --> 00:04:55,000
 of looking at experience.

56
00:04:55,000 --> 00:04:58,000
 That's quite useful, I think.

57
00:04:58,000 --> 00:05:01,000
 So the layout in that video is probably useful.

58
00:05:01,000 --> 00:05:04,240
 But you can tell, like my tone is for kids, it's not for a

59
00:05:04,240 --> 00:05:07,000
 17-year-old for sure.

60
00:05:07,000 --> 00:05:15,000
 So I'd focus more on the booklet and videos.

61
00:05:15,000 --> 00:05:19,000
 And if you have time, do a meditation course.

62
00:05:19,000 --> 00:05:22,490
 You can do one of our online meditation courses if you're

63
00:05:22,490 --> 00:05:23,000
 not.

64
00:05:23,000 --> 00:05:26,430
 And we can meet every week, talk about your meditation if

65
00:05:26,430 --> 00:05:28,000
 you've got questions.

66
00:05:28,000 --> 00:05:30,360
 It's pretty simple, it's not like we talk for a long time

67
00:05:30,360 --> 00:05:31,000
 necessarily,

68
00:05:31,000 --> 00:05:37,000
 but we can lead you through the steps.

69
00:05:37,000 --> 00:05:49,000
 Our quote today is about alcohol

70
00:05:49,000 --> 00:05:57,000
 and the folly that comes from it.

71
00:05:57,000 --> 00:06:01,420
 Whoever follows the Dhamma should not drink or encourage

72
00:06:01,420 --> 00:06:03,000
 others to drink.

73
00:06:03,000 --> 00:06:07,000
 Knowing in that intoxication is the result.

74
00:06:07,000 --> 00:06:09,310
 Because of intoxication, the fool commits evil deeds and

75
00:06:09,310 --> 00:06:12,000
 makes others negligent too.

76
00:06:12,000 --> 00:06:16,680
 So avoid this root of wrong, this folly liked only by fools

77
00:06:16,680 --> 00:06:17,000
.

78
00:06:17,000 --> 00:06:46,000
 [silence]

79
00:06:46,000 --> 00:07:08,000
 The fool commits evil deeds.

80
00:07:08,000 --> 00:07:16,950
 That's the thing really is alcohol isn't inherently a

81
00:07:16,950 --> 00:07:19,000
 problem.

82
00:07:19,000 --> 00:07:24,000
 Being intoxicated isn't inherently a problem.

83
00:07:24,000 --> 00:07:28,000
 But it's an interesting substance.

84
00:07:28,000 --> 00:07:32,000
 Substances are interesting.

85
00:07:32,000 --> 00:07:38,000
 I was talking today about psychedelics.

86
00:07:38,000 --> 00:07:41,780
 I mean just taking a substance in and of itself, the act of

87
00:07:41,780 --> 00:07:49,000
 taking a substance is problematic.

88
00:07:49,000 --> 00:07:53,670
 But when you take a substance for the purpose of, not all

89
00:07:53,670 --> 00:07:56,000
 substances are the same.

90
00:07:56,000 --> 00:07:59,930
 When you take a substance for the purpose of actually

91
00:07:59,930 --> 00:08:02,000
 removing mindfulness,

92
00:08:02,000 --> 00:08:07,000
 removing your ability to be concerned, to be alert,

93
00:08:07,000 --> 00:08:11,000
 what I say is it destroys your inhibitions.

94
00:08:11,000 --> 00:08:18,000
 Well those inhibitions are there for a reason.

95
00:08:18,000 --> 00:08:20,000
 They don't go away.

96
00:08:20,000 --> 00:08:24,000
 They don't become less inhibited because you drink alcohol.

97
00:08:24,000 --> 00:08:29,000
 Except during the time when you drink alcohol.

98
00:08:29,000 --> 00:08:32,000
 Or maybe you do become less inhibited.

99
00:08:32,000 --> 00:08:36,290
 The point being that the reason we have inhibitions is

100
00:08:36,290 --> 00:08:39,000
 because we can discern

101
00:08:39,000 --> 00:08:41,000
 that what we're doing is incorrect.

102
00:08:41,000 --> 00:08:45,000
 Now whether our inhibitions are proper or improper,

103
00:08:45,000 --> 00:08:49,910
 the very ability to have inhibitions is to know right from

104
00:08:49,910 --> 00:08:50,000
 wrong,

105
00:08:50,000 --> 00:08:54,000
 maybe discerning about what you're doing.

106
00:08:54,000 --> 00:08:58,000
 That faculty is gone.

107
00:08:58,000 --> 00:09:00,720
 Along with many faculties that are gone, the ability to

108
00:09:00,720 --> 00:09:02,000
 drive is gone.

109
00:09:02,000 --> 00:09:04,000
 That kind of thing.

110
00:09:04,000 --> 00:09:07,000
 Depth perception, I don't know, all sorts of things.

111
00:09:07,000 --> 00:09:11,000
 It's poison, alcohol is poison.

112
00:09:11,000 --> 00:09:16,000
 We were talking about psychedelics today.

113
00:09:16,000 --> 00:09:20,850
 I admitted that psychedelics have the potential to open

114
00:09:20,850 --> 00:09:25,000
 your mind, broaden your horizons.

115
00:09:25,000 --> 00:09:30,000
 Psychedelics are interesting in that psilocybin anyway,

116
00:09:30,000 --> 00:09:33,000
 and apparently this guy was arguing that LSD as well.

117
00:09:33,000 --> 00:09:38,000
 But psilocybin anyway doesn't do what we think it does.

118
00:09:38,000 --> 00:09:41,000
 I mean there's a study anyway, or there was a study.

119
00:09:41,000 --> 00:09:43,000
 Who knows whether it's actually right.

120
00:09:43,000 --> 00:09:48,000
 Apparently psilocybin doesn't increase brain activity.

121
00:09:48,000 --> 00:09:54,000
 What we think of these things is they must induce some sort

122
00:09:54,000 --> 00:09:56,000
 of hyperactivity in the brain, right?

123
00:09:56,000 --> 00:09:57,000
 That's why we hallucinate.

124
00:09:57,000 --> 00:09:59,000
 Apparently that's not what happens.

125
00:09:59,000 --> 00:10:03,000
 They actually inhibit brain activity.

126
00:10:03,000 --> 00:10:13,300
 There is a reduction of brain activity under the influence

127
00:10:13,300 --> 00:10:17,000
 of hallucinogens.

128
00:10:17,000 --> 00:10:27,000
 And so I was saying, as interesting as that might be,

129
00:10:27,000 --> 00:10:30,990
 it gives the idea that it might come from the fact that the

130
00:10:30,990 --> 00:10:32,000
 mind is freer,

131
00:10:32,000 --> 00:10:35,000
 it's not restrained by the senses.

132
00:10:55,000 --> 00:10:58,000
 So there's hallucinations.

133
00:10:58,000 --> 00:11:10,000
 The important point is,

134
00:11:10,000 --> 00:11:17,530
 there's a potential to open up your mind, to give you a new

135
00:11:17,530 --> 00:11:19,000
 perspective.

136
00:11:19,000 --> 00:11:22,260
 But I said, you know, a car accident will give you a new

137
00:11:22,260 --> 00:11:23,000
 perspective.

138
00:11:23,000 --> 00:11:25,000
 It does the same sort of thing.

139
00:11:25,000 --> 00:11:29,070
 It says nothing about the actual act of doing the drugs or

140
00:11:29,070 --> 00:11:30,000
 getting in a car accident.

141
00:11:30,000 --> 00:11:32,000
 It's not to say that the car accident is good,

142
00:11:32,000 --> 00:11:36,000
 but anything that takes you out of your comfort zone,

143
00:11:36,000 --> 00:11:39,970
 being taken out of your comfort zone, is a good wake-up

144
00:11:39,970 --> 00:11:41,000
 call.

145
00:11:41,000 --> 00:11:48,000
 It's not to say that that which takes you out of...

146
00:11:48,000 --> 00:11:51,910
 So that what takes you out of your comfort zone is in any

147
00:11:51,910 --> 00:11:54,000
 way good and beneficial.

148
00:11:54,000 --> 00:12:06,020
 And even getting alcohol poisoning would probably sober you

149
00:12:06,020 --> 00:12:08,000
 up, right?

150
00:12:08,000 --> 00:12:12,000
 Getting put in the hospital for alcohol poisoning.

151
00:12:14,000 --> 00:12:18,000
 But what was more interesting to me is,

152
00:12:18,000 --> 00:12:24,380
 this idea of just the fact of taking substances is

153
00:12:24,380 --> 00:12:26,000
 problematic.

154
00:12:26,000 --> 00:12:29,010
 I mean, you can see it with alcohol, your intention is to

155
00:12:29,010 --> 00:12:30,000
 do something silly,

156
00:12:30,000 --> 00:12:36,000
 to lose whatever mindfulness you might naturally have.

157
00:12:36,000 --> 00:12:40,220
 But just the intention, like in the case of psilocybin or

158
00:12:40,220 --> 00:12:43,000
 in the case of hallucinogens,

159
00:12:45,000 --> 00:12:47,050
 your whole reason for taking... Why are you taking this

160
00:12:47,050 --> 00:12:48,000
 chemical?

161
00:12:48,000 --> 00:12:52,640
 I mean, if you're accidentally ingested, then there's

162
00:12:52,640 --> 00:12:54,000
 nothing to it.

163
00:12:54,000 --> 00:12:57,000
 But when you intentionally take it,

164
00:12:57,000 --> 00:13:01,410
 your understanding that leads you to do it is sort of a

165
00:13:01,410 --> 00:13:03,000
 karma for sure.

166
00:13:03,000 --> 00:13:04,000
 I mean, it's a habit.

167
00:13:04,000 --> 00:13:08,000
 What that means, it's going to influence your outlook.

168
00:13:08,000 --> 00:13:13,910
 It's a part of your outlook on life, is taking drugs,

169
00:13:13,910 --> 00:13:15,000
 taking substances.

170
00:13:15,000 --> 00:13:20,600
 And so if you repeatedly take hallucinogens or if you

171
00:13:20,600 --> 00:13:22,000
 repeatedly do alcohol,

172
00:13:22,000 --> 00:13:25,000
 it's going to affect who you are.

173
00:13:25,000 --> 00:13:29,000
 I mean, putting aside the effects of the actual drugs,

174
00:13:29,000 --> 00:13:32,540
 which is actually another interesting point because you can

175
00:13:32,540 --> 00:13:34,000
 argue that hallucinogens

176
00:13:34,000 --> 00:13:38,000
 or even alcohol are good for you.

177
00:13:38,000 --> 00:13:45,000
 But you know, that's anecdotal evidence.

178
00:13:45,000 --> 00:13:47,810
 This is a clear case of where anecdotal evidence falls very

179
00:13:47,810 --> 00:13:49,000
, very short

180
00:13:49,000 --> 00:13:51,000
 because you'd have to look scientifically.

181
00:13:51,000 --> 00:13:53,600
 I mean, in the case of meditation, we all say meditation's

182
00:13:53,600 --> 00:13:54,000
 helpful for you,

183
00:13:54,000 --> 00:13:56,000
 but they say, "Well, that's anecdotal."

184
00:13:56,000 --> 00:13:59,000
 Except it turns out to be true that when you do study it,

185
00:13:59,000 --> 00:14:03,000
 all indicators show that meditation is beneficial.

186
00:14:03,000 --> 00:14:05,000
 I mean, but that's fairly obvious.

187
00:14:05,000 --> 00:14:07,890
 The person takes alcohol and thinks, "Well, it's good for

188
00:14:07,890 --> 00:14:08,000
 me,

189
00:14:08,000 --> 00:14:11,000
 and it helps me loosen up."

190
00:14:11,000 --> 00:14:17,180
 I think a smart intellectual people would not take that

191
00:14:17,180 --> 00:14:19,000
 very seriously

192
00:14:19,000 --> 00:14:26,990
 because if you think of when people are accustomed to

193
00:14:26,990 --> 00:14:31,000
 drinking alcohol,

194
00:14:31,000 --> 00:14:34,110
 let's not say alcoholics, but even just accustomed to

195
00:14:34,110 --> 00:14:35,000
 drinking alcohol,

196
00:14:35,000 --> 00:14:38,000
 it certainly doesn't make them better people.

197
00:14:38,000 --> 00:14:41,000
 It doesn't make them happier people.

198
00:14:41,000 --> 00:14:44,000
 I suppose many people do believe that.

199
00:14:44,000 --> 00:14:47,000
 If you don't take alcohol, you're just going to...

200
00:14:47,000 --> 00:14:49,390
 Well, the thing is, you're going to have to deal with your

201
00:14:49,390 --> 00:14:50,000
 problems.

202
00:14:50,000 --> 00:14:53,000
 That's the whole thing.

203
00:14:53,000 --> 00:14:56,000
 But I mean, even hallucinogens.

204
00:14:56,000 --> 00:15:01,000
 To say that if you were to continue to do hallucinogens,

205
00:15:01,000 --> 00:15:07,200
 that would somehow be a beneficial regimen of spiritual

206
00:15:07,200 --> 00:15:09,000
 activity.

207
00:15:09,000 --> 00:15:13,000
 I think I'm 90% sure that's...

208
00:15:13,000 --> 00:15:18,000
 I'm 90% sure anyway that there are side effects to it,

209
00:15:18,000 --> 00:15:20,000
 apart from just the intention to take it,

210
00:15:20,000 --> 00:15:24,000
 which, as I say, is I think the key to it.

211
00:15:24,000 --> 00:15:29,000
 If your spirituality involves taking chemicals,

212
00:15:29,000 --> 00:15:32,000
 it's a misunderstanding of how the mind works

213
00:15:32,000 --> 00:15:36,000
 because it's like these ideas of enlightening other beings,

214
00:15:36,000 --> 00:15:40,250
 like transmitting the Dhamma so that other people become

215
00:15:40,250 --> 00:15:41,000
 enlightened.

216
00:15:41,000 --> 00:15:45,500
 Mahayana Buddhism has this, the idea that you can enlighten

217
00:15:45,500 --> 00:15:46,000
 others,

218
00:15:46,000 --> 00:15:53,000
 a Buddha can enlighten you, just by force kind of.

219
00:15:53,000 --> 00:15:57,000
 Well, maybe they don't, but I think some schools do.

220
00:15:57,000 --> 00:16:00,000
 At any rate, suppose there is this idea,

221
00:16:00,000 --> 00:16:02,000
 and I think a lot of people do have this idea,

222
00:16:02,000 --> 00:16:06,000
 that there are ways by which you can become enlightened.

223
00:16:06,000 --> 00:16:09,000
 Take a shortcut.

224
00:16:09,000 --> 00:16:12,000
 And so this person I was talking with, he said,

225
00:16:12,000 --> 00:16:14,000
 "I'm all about shortcuts."

226
00:16:14,000 --> 00:16:18,000
 And so what I was trying to argue was...

227
00:16:18,000 --> 00:16:22,000
 The whole act of taking shortcuts is a problem.

228
00:16:22,000 --> 00:16:24,000
 It's not dealing with your issues.

229
00:16:24,000 --> 00:16:31,000
 It's not natural. It's not real.

230
00:16:31,000 --> 00:16:33,000
 And I think that's an interesting key point,

231
00:16:33,000 --> 00:16:35,000
 that it can never be possible.

232
00:16:35,000 --> 00:16:39,000
 By very nature, taking a shortcut can't be possible.

233
00:16:39,000 --> 00:16:42,000
 The mind isn't the body.

234
00:16:42,000 --> 00:16:47,140
 So the idea that somehow affecting the body can fix the

235
00:16:47,140 --> 00:16:48,000
 mind

236
00:16:48,000 --> 00:16:51,000
 is, I think, intrinsically...

237
00:16:51,000 --> 00:16:54,000
 I don't mean to say that it's just hard or we can't do it.

238
00:16:54,000 --> 00:16:57,000
 It means it's intrinsically impossible.

239
00:16:57,000 --> 00:17:05,000
 By its very nature, trying to change the brain fails.

240
00:17:05,000 --> 00:17:09,000
 And I guess what I would say more is,

241
00:17:09,000 --> 00:17:13,000
 the individual's act of trying to change their own brain,

242
00:17:13,000 --> 00:17:16,000
 to fix the problem by changing their own brain,

243
00:17:16,000 --> 00:17:17,000
 is going to affect them.

244
00:17:17,000 --> 00:17:20,000
 It's going to be a karmically problematic idea.

245
00:17:20,000 --> 00:17:22,000
 That's the idea.

246
00:17:22,000 --> 00:17:24,000
 Anyway, it's interesting ideas around alcohol.

247
00:17:24,000 --> 00:17:27,000
 Alcohol is pretty obvious to a meditator.

248
00:17:27,000 --> 00:17:28,000
 You can't be mindful.

249
00:17:28,000 --> 00:17:30,000
 You can't progress in meditation

250
00:17:30,000 --> 00:17:33,000
 if you're engaging in drinking alcohol.

251
00:17:33,000 --> 00:17:35,000
 It's interesting teaching at the university,

252
00:17:35,000 --> 00:17:37,000
 because I'm assuming many of these people drink alcohol.

253
00:17:37,000 --> 00:17:40,000
 I don't know what percent, but...

254
00:17:40,000 --> 00:17:43,000
 I've got a lot of karma to pay back

255
00:17:43,000 --> 00:17:48,000
 when I was in residence in my first year of university.

256
00:17:48,000 --> 00:17:51,000
 They were handing out these pamphlets that said,

257
00:17:51,000 --> 00:17:54,000
 "If your friends can't have fun without drinking,

258
00:17:54,000 --> 00:17:56,000
 then maybe you need new friends."

259
00:17:56,000 --> 00:17:58,000
 And so I cut out the word "without"

260
00:17:58,000 --> 00:18:02,000
 and put it up on our door.

261
00:18:02,000 --> 00:18:05,000
 So it said, "If your friends can't have fun drinking,

262
00:18:05,000 --> 00:18:09,000
 maybe you need new friends."

263
00:18:09,000 --> 00:18:12,000
 And I got my roommate.

264
00:18:12,000 --> 00:18:14,000
 He was a nice guy from...

265
00:18:14,000 --> 00:18:16,000
 He was a nice guy. I haven't seen him in a long time.

266
00:18:16,000 --> 00:18:19,000
 He's from Sault Ste. Marie, Northern Ontario,

267
00:18:19,000 --> 00:18:25,000
 where I'm the same latitude as me.

268
00:18:25,000 --> 00:18:29,000
 North Northern Ontario.

269
00:18:29,000 --> 00:18:33,000
 And I got him to start drinking alcohol.

270
00:18:33,000 --> 00:18:36,000
 Yeah.

271
00:18:36,000 --> 00:18:39,000
 It wasn't a great time.

272
00:18:39,000 --> 00:18:52,000
 [Silence]

273
00:18:52,000 --> 00:18:55,000
 Anyway, so tomorrow 3 p.m. Eastern Time,

274
00:18:55,000 --> 00:18:58,000
 12 p.m. Second Lifetime.

275
00:18:58,000 --> 00:19:03,000
 I'll be on the Buddha Centre.

276
00:19:03,000 --> 00:19:05,000
 I guess we'll meet in the main hall.

277
00:19:05,000 --> 00:19:08,000
 That's the easiest place to find me.

278
00:19:08,000 --> 00:19:13,000
 There's a nice place in the forest that they have.

279
00:19:13,000 --> 00:19:18,000
 It's probably a little harder for people to find.

280
00:19:18,000 --> 00:19:21,000
 Maybe once we get settled, we'll move to the deer park

281
00:19:21,000 --> 00:19:23,000
 that they have.

282
00:19:23,000 --> 00:19:25,000
 I haven't seen what the deer park's like right now,

283
00:19:25,000 --> 00:19:30,000
 but I'm assuming there's still a place there.

284
00:19:30,000 --> 00:19:32,000
 I was flying around Second Life,

285
00:19:32,000 --> 00:19:36,000
 well, around the Buddha Centre this afternoon.

286
00:19:36,000 --> 00:19:41,000
 And Second Life, your avatars can actually fly.

287
00:19:41,000 --> 00:19:45,000
 Just trying to remember how the whole system works.

288
00:19:45,000 --> 00:19:51,000
 How to move my character, how to move the camera.

289
00:19:51,000 --> 00:19:57,000
 I spent about 5, 10 minutes wandering around.

290
00:19:57,000 --> 00:19:59,000
 So yeah, I'll see you guys tomorrow.

291
00:19:59,000 --> 00:20:00,000
 Audio is a problem.

292
00:20:00,000 --> 00:20:03,000
 Second Life isn't flawless, or it wasn't flawless before,

293
00:20:03,000 --> 00:20:05,000
 but it's often because of people's hardware.

294
00:20:05,000 --> 00:20:08,000
 It's just difficult to get everything working.

295
00:20:08,000 --> 00:20:10,000
 So some people may not hear,

296
00:20:10,000 --> 00:20:12,000
 but you're not expected to use audio.

297
00:20:12,000 --> 00:20:13,000
 You just listen.

298
00:20:13,000 --> 00:20:15,000
 You just need good speakers or a headphone,

299
00:20:15,000 --> 00:20:18,000
 and you have to turn your mic off, actually,

300
00:20:18,000 --> 00:20:20,000
 because that's a big problem.

301
00:20:20,000 --> 00:20:21,000
 It's often people's feedback,

302
00:20:21,000 --> 00:20:24,000
 and then we have to mute them or ban them.

303
00:20:24,000 --> 00:20:31,000
 I don't know what they do.

304
00:20:31,000 --> 00:20:33,000
 Yeah, SLT doesn't mean Sri Lankan time.

305
00:20:33,000 --> 00:20:36,000
 SLT means Second Life Time.

306
00:20:36,000 --> 00:20:38,000
 Actually, it does mean Sri Lankan time as well,

307
00:20:38,000 --> 00:20:42,000
 but not for us.

308
00:20:42,000 --> 00:20:47,000
 So anyway, I guess that's all for tonight.

309
00:20:47,000 --> 00:20:50,000
 See you all tomorrow afternoon,

310
00:20:50,000 --> 00:20:54,000
 or any of you who are able to get on Second Life.

311
00:20:54,000 --> 00:20:57,000
 Have a good night, everybody.

