1
00:00:00,000 --> 00:00:02,560
 I was thinking about alcohol.

2
00:00:02,560 --> 00:00:06,600
 Let's say I got an advice for people they don't drink.

3
00:00:06,600 --> 00:00:09,920
 They don't want to look too religious to others.

4
00:00:09,920 --> 00:00:13,700
 Let's say, because I certainly don't like somebody like--

5
00:00:13,700 --> 00:00:16,640
 anyway, somebody wants to go out or somewhere,

6
00:00:16,640 --> 00:00:19,440
 just go drink two up to three beers.

7
00:00:19,440 --> 00:00:21,560
 You're not going to get drunk.

8
00:00:21,560 --> 00:00:23,680
 And it's a good solution.

9
00:00:23,680 --> 00:00:29,120
 It's not bad if you just get two beers just

10
00:00:29,120 --> 00:00:33,360
 to show that you can drink, but not to just say

11
00:00:33,360 --> 00:00:34,600
 that you get drunk.

12
00:00:34,600 --> 00:00:36,560
 Just to show you.

13
00:00:36,560 --> 00:00:39,280
 But even if it were that simple, I would say, no.

14
00:00:39,280 --> 00:00:48,760
 If I go around telling people to-- well, simply put,

15
00:00:48,760 --> 00:00:51,000
 you're setting a bad example for people.

16
00:00:51,000 --> 00:00:56,400
 You are reinforcing the belief that drinking is OK.

17
00:00:56,400 --> 00:00:59,250
 So even if you weren't getting drunk as a result of

18
00:00:59,250 --> 00:00:59,560
 drinking

19
00:00:59,560 --> 00:01:02,240
 two beers, which actually you are,

20
00:01:02,240 --> 00:01:03,720
 even the smallest bit of alcohol,

21
00:01:03,720 --> 00:01:05,280
 it makes you the smallest bit drunk.

22
00:01:05,280 --> 00:01:09,680
 It's not a-- there's not a line or something.

23
00:01:09,680 --> 00:01:12,200
 Whoops, now I'm drunk.

24
00:01:12,200 --> 00:01:15,160
 Comes gradually to some voice in the air.

25
00:01:15,160 --> 00:01:17,520
 You're not going to get drunk if you have a--

26
00:01:17,520 --> 00:01:19,040
 But do you understand what I'm saying?

27
00:01:19,040 --> 00:01:21,280
 There is no such thing as drunk.

28
00:01:21,280 --> 00:01:23,760
 Drunk is just a line in the sand.

29
00:01:23,760 --> 00:01:25,360
 The more you drink, the more drunk you are.

30
00:01:25,360 --> 00:01:28,760
 It's not a line where suddenly, whoops, I'm drunk.

31
00:01:28,760 --> 00:01:30,200
 Up until this point, I was fine.

32
00:01:30,200 --> 00:01:31,640
 But suddenly, boom, I'm drunk.

33
00:01:31,640 --> 00:01:33,920
 No, it's not like-- it's poison.

34
00:01:33,920 --> 00:01:34,720
 So it poisons.

35
00:01:34,720 --> 00:01:35,960
 If you have a little bit of poison,

36
00:01:35,960 --> 00:01:37,640
 you're a little bit poisoned.

37
00:01:37,640 --> 00:01:39,400
 I mean, I'm talking from experience.

38
00:01:39,400 --> 00:01:40,000
 I've been there.

39
00:01:40,000 --> 00:01:42,240
 I'm a bit of a lush myself.

40
00:01:42,240 --> 00:01:48,760
 But I've been-- I can hear where that person's coming from,

41
00:01:48,760 --> 00:01:49,760
 saying, so hang over.

42
00:01:49,760 --> 00:01:55,320
 They don't have much to add to the discussion.

43
00:01:55,320 --> 00:01:58,080
 Pickled.

44
00:01:58,080 --> 00:02:00,360
 It's always unpleasant.

45
00:02:00,360 --> 00:02:02,880
 Always unpleasant when somebody asks me to drink something.

46
00:02:02,880 --> 00:02:04,920
 But there's so much more than that as well.

47
00:02:04,920 --> 00:02:06,720
 There's the crowd that you're hanging out with.

48
00:02:06,720 --> 00:02:09,240
 You're hanging out with people who are drinking as well.

49
00:02:09,240 --> 00:02:11,640
 And you're encouraging it.

50
00:02:11,640 --> 00:02:13,640
 You're engaged in this situation.

51
00:02:13,640 --> 00:02:14,200
 But--

52
00:02:14,200 --> 00:02:15,760
 I mean, it's very hard.

53
00:02:15,760 --> 00:02:22,560
 I mean, because in real life, you meet with your friends.

54
00:02:22,560 --> 00:02:28,240
 And they really got nothing else to do and get drunk.

55
00:02:28,240 --> 00:02:31,760
 And it's just kind of pathetic when you see that you can't

56
00:02:31,760 --> 00:02:36,920
 deal with your social life in the Buddhist way, really.

57
00:02:36,920 --> 00:02:39,360
 Because people are just-- you know what?

58
00:02:39,360 --> 00:02:45,440
 I mean, let's go and get drunk or let's smoke weed.

59
00:02:45,440 --> 00:02:47,320
 So weed is all right.

60
00:02:47,320 --> 00:02:48,680
 Because it's not messy.

61
00:02:48,680 --> 00:02:53,520
 I mean, it doesn't give you-- it might show you some

62
00:02:53,520 --> 00:02:54,240
 reality

63
00:02:54,240 --> 00:02:55,040
 at least.

64
00:02:55,040 --> 00:02:57,880
 Might show you that your mind is--

65
00:02:57,880 --> 00:02:59,960
 But you're talking about marijuana?

66
00:02:59,960 --> 00:03:01,360
 Yes, yes, yes, yes.

67
00:03:01,360 --> 00:03:04,480
 Showing you a reality.

68
00:03:04,480 --> 00:03:07,800
 I mean, it might show you.

69
00:03:07,800 --> 00:03:10,840
 It might show you some sort of--

70
00:03:10,840 --> 00:03:15,280
 You know, I mean, some people say, hey.

71
00:03:15,280 --> 00:03:18,200
 No, some people say, oh, I don't smoke really

72
00:03:18,200 --> 00:03:19,680
 because I don't feel good.

73
00:03:19,680 --> 00:03:20,760
 I say, you know what?

74
00:03:20,760 --> 00:03:23,960
 Because every day, that's how you feel every day.

75
00:03:23,960 --> 00:03:26,280
 That's what I think.

76
00:03:26,280 --> 00:03:29,960
 So I think that marijuana is not that bad in social life.

77
00:03:29,960 --> 00:03:40,480
 Yeah, I mean, if social life is the goal,

78
00:03:40,480 --> 00:03:41,480
 it's not the goal.

79
00:03:41,480 --> 00:03:42,480
 Well, it's not a goal.

80
00:03:42,480 --> 00:03:49,440
 I mean, I don't have a need for social life.

81
00:03:49,440 --> 00:03:54,760
 But sometimes you have in front of your situation

82
00:03:54,760 --> 00:04:00,320
 that you basically become a loner.

83
00:04:00,320 --> 00:04:01,120
 You know what I mean?

84
00:04:01,120 --> 00:04:02,880
 Sounds wonderful.

85
00:04:02,880 --> 00:04:07,680
 Yeah, well, it's like, what are people going to think about

86
00:04:07,680 --> 00:04:08,040
 me?

87
00:04:08,040 --> 00:04:09,040
 You know what I mean?

88
00:04:10,800 --> 00:04:13,960
 Well, that doesn't sound very skillful in thought.

89
00:04:13,960 --> 00:04:16,000
 Yeah, it doesn't sound very good.

90
00:04:16,000 --> 00:04:20,400
 But I want them to think that I'm normal.

91
00:04:20,400 --> 00:04:23,720
 Is that a skillful thought or an unskillful thought?

92
00:04:23,720 --> 00:04:26,960
 Is that a skillful desire or an unskillful desire?

93
00:04:26,960 --> 00:04:30,040
 You see, I stopped drinking a long time ago.

94
00:04:30,040 --> 00:04:32,400
 But somebody asked me, I came to a conclusion like,

95
00:04:32,400 --> 00:04:37,680
 I'd rather have two beers and have it be off my back.

96
00:04:37,680 --> 00:04:39,680
 Why I don't drink like this?

97
00:04:39,680 --> 00:04:45,120
 No, you could open up a whole new line of conversation

98
00:04:45,120 --> 00:04:49,640
 if you could talk to people about-- a great reason.

99
00:04:49,640 --> 00:04:51,880
 If people want to avoid that kind of thing,

100
00:04:51,880 --> 00:04:54,720
 you could say it's because I want

101
00:04:54,720 --> 00:04:57,280
 to take care of the other people who are drinking.

102
00:04:57,280 --> 00:05:00,880
 Be the designated driver, for example.

103
00:05:00,880 --> 00:05:07,280
 Because I'm much happier when I can take care of people,

104
00:05:07,280 --> 00:05:10,880
 when I'm in full control of my senses.

105
00:05:10,880 --> 00:05:12,840
 What an opportunity you're missing

106
00:05:12,840 --> 00:05:19,360
 by becoming drunk to-- not to laugh at drunk people,

107
00:05:19,360 --> 00:05:24,160
 I suppose, but to help or to be the designated driver,

108
00:05:24,160 --> 00:05:26,000
 for example.

109
00:05:26,000 --> 00:05:30,720
 But you're in the wrong crowd.

110
00:05:30,720 --> 00:05:33,360
 If your friends can't-- what was this thing?

111
00:05:33,360 --> 00:05:37,160
 If your friends can't have fun without drinking,

112
00:05:37,160 --> 00:05:39,080
 you need new friends.

113
00:05:39,080 --> 00:05:42,200
 I mean, I'm talking about population here.

114
00:05:42,200 --> 00:05:44,040
 I'm not talking about friends.

115
00:05:44,040 --> 00:05:45,640
 I'm talking about a few people.

116
00:05:45,640 --> 00:05:49,680
 I'm talking about the whole society.

117
00:05:49,680 --> 00:05:50,680
 Lots of good people.

118
00:05:50,680 --> 00:05:51,680
 Look at the people here.

119
00:05:51,680 --> 00:05:53,480
 We're not drinking.

120
00:05:53,480 --> 00:05:55,560
 No, I don't drink.

121
00:05:55,560 --> 00:05:58,680
 That's why I'm here.

122
00:05:58,680 --> 00:05:59,600
 Oh, there you go.

123
00:05:59,600 --> 00:06:00,680
 Find a better society.

124
00:06:00,680 --> 00:06:05,560
 There's more to it than that.

125
00:06:05,560 --> 00:06:07,480
 Really, the key is when you drink two beers,

126
00:06:07,480 --> 00:06:09,520
 you become two beers drunk.

127
00:06:09,520 --> 00:06:11,320
 It's a bad idea.

128
00:06:11,320 --> 00:06:11,840
 And you--

129
00:06:11,840 --> 00:06:12,760
 Sorry about it.

130
00:06:12,760 --> 00:06:14,600
 If you have really liked beers--

131
00:06:14,600 --> 00:06:15,320
 Sorry about it.

132
00:06:15,320 --> 00:06:15,800
 I'm not--

133
00:06:15,800 --> 00:06:18,760
 You have one.

134
00:06:18,760 --> 00:06:24,760
 Just stating that it's a shame in many ways.

135
00:06:24,760 --> 00:06:32,520
 Because what it does to your mind and your idea of what's

136
00:06:32,520 --> 00:06:34,400
 right and so on.

137
00:06:34,400 --> 00:06:41,800
 And the dull state that it brings to drink.

138
00:06:41,800 --> 00:06:44,800
 And the opportunities that are missed to change your life,

139
00:06:44,800 --> 00:06:47,200
 to change your society.

140
00:06:47,200 --> 00:06:50,600
 That would be forced by having to confront this.

141
00:06:50,600 --> 00:06:53,280
 By having to draw the line in the sand and say,

142
00:06:53,280 --> 00:06:55,320
 no, I won't cross this.

143
00:06:55,320 --> 00:06:57,120
 Yeah, but I've got no problem.

144
00:06:57,120 --> 00:07:00,120
 I'm so confident about it.

145
00:07:00,120 --> 00:07:01,760
 Well, then start those conversations.

146
00:07:01,760 --> 00:07:03,760
 I don't want to--

147
00:07:03,760 --> 00:07:06,000
 No, no, no, no.

148
00:07:06,000 --> 00:07:06,840
 I don't drink.

149
00:07:06,840 --> 00:07:10,440
 And here's the reason why.

150
00:07:10,440 --> 00:07:12,120
 Yeah, but it sounds like--

151
00:07:12,120 --> 00:07:17,400
 it makes me feel like I'm old.

152
00:07:17,400 --> 00:07:19,360
 You know what I mean?

153
00:07:19,360 --> 00:07:21,520
 Choose the path.

154
00:07:21,520 --> 00:07:23,400
 Choose the path that you want to follow.

155
00:07:23,400 --> 00:07:25,640
 If you want to follow the social life, the friends,

156
00:07:25,640 --> 00:07:28,840
 and everything along those lines, and follow the path.

157
00:07:28,840 --> 00:07:32,040
 If you want to follow the Buddha's teachings,

158
00:07:32,040 --> 00:07:33,740
 and follow that path, you have to

159
00:07:33,740 --> 00:07:34,360
 make a choice.

160
00:07:34,360 --> 00:07:36,760
 You can't have one or the other.

161
00:07:36,760 --> 00:07:45,240
 You see, I wasn't drinking for two years.

162
00:07:45,240 --> 00:07:47,120
 I stopped drinking two years ago.

163
00:07:47,120 --> 00:07:49,880
 And I committed myself to this very because I used to drink

164
00:07:49,880 --> 00:07:51,080
 a lot from Poland.

165
00:07:51,080 --> 00:07:54,080
 I started drinking vodka when I was 11.

166
00:07:54,080 --> 00:07:58,720
 So I know quite a lot about drinking, not to mention my

167
00:07:58,720 --> 00:08:03,560
 family, and many other families.

168
00:08:03,560 --> 00:08:11,320
 But I'm confident at that point that I can dream and by no

169
00:08:11,320 --> 00:08:13,000
 me enjoy it.

170
00:08:13,000 --> 00:08:15,160
 And I was like--

171
00:08:15,160 --> 00:08:18,560
 I was so after two years not drinking, my mate asked me,

172
00:08:18,560 --> 00:08:20,240
 like, hey, let's go have--

173
00:08:20,240 --> 00:08:22,680
 because he had some problem with something.

174
00:08:22,680 --> 00:08:24,880
 And let's have some--

175
00:08:24,880 --> 00:08:29,080
 I hope you let's go and have a good time.

176
00:08:29,080 --> 00:08:30,680
 I have some beers.

177
00:08:30,680 --> 00:08:35,200
 And I drank these two beers, three beers.

178
00:08:35,200 --> 00:08:38,600
 And I was like, I didn't enjoy it.

179
00:08:38,600 --> 00:08:40,640
 I didn't enjoy it, you know?

180
00:08:40,640 --> 00:08:42,920
 I mean, I didn't like it.

181
00:08:42,920 --> 00:08:45,600
 You're only talking from within your own vibe.

182
00:08:45,600 --> 00:08:47,280
 Sorry, I don't mean to sound too hard.

183
00:08:47,280 --> 00:08:49,080
 But I just like to argue.

184
00:08:49,080 --> 00:08:50,360
 I don't mean to come down on you.

185
00:08:50,360 --> 00:08:58,600
 But just the point that I would observe is that we can

186
00:08:58,600 --> 00:09:01,400
 only look at our own-- we can only look from our own

187
00:09:01,400 --> 00:09:02,160
 position.

188
00:09:02,160 --> 00:09:04,280
 We can't look from outside and see what

189
00:09:04,280 --> 00:09:05,920
 actually is going on.

190
00:09:05,920 --> 00:09:07,520
 And I would be willing to bet--

191
00:09:07,520 --> 00:09:10,280
 and I'm not trying to be hard or like some religious person

192
00:09:10,280 --> 00:09:12,000
 saying no, bad, you.

193
00:09:12,000 --> 00:09:17,520
 But I'd be willing to bet, just as a kind of a friendly

194
00:09:17,520 --> 00:09:24,120
 wager, that if you did stop-- if you did go to the extent

195
00:09:24,120 --> 00:09:28,240
 of not drinking those two beers.

196
00:09:28,240 --> 00:09:30,480
 Or let's talk about someone who does that on a weekly

197
00:09:30,480 --> 00:09:33,000
 basis, say, because for you it might just be whenever the

198
00:09:33,000 --> 00:09:34,360
 occasion occurs.

199
00:09:34,360 --> 00:09:36,520
 But suppose someone said, well, every weekend I have a

200
00:09:36,520 --> 00:09:38,120
 couple of beers with my friends.

201
00:09:38,120 --> 00:09:40,880
 That if they stop doing that, it would change their life in

202
00:09:40,880 --> 00:09:42,720
 some meaningful way.

203
00:09:42,720 --> 00:09:44,960
 Maybe not a huge, profound way.

204
00:09:44,960 --> 00:09:48,480
 But in a meaningful way, by stopping that, their life would

205
00:09:48,480 --> 00:09:49,440
 change.

206
00:09:49,440 --> 00:09:51,920
 Their mind would be affected.

207
00:09:51,920 --> 00:09:54,600
 And moreover, their surroundings would be affected.

208
00:09:54,600 --> 00:09:56,480
 The other people would be affected.

209
00:09:56,480 --> 00:09:57,680
 Their whole life would change.

210
00:09:57,680 --> 00:10:00,440
 So the rules are sikapada.

211
00:10:00,440 --> 00:10:05,440
 They're rules of training, or they're paths of training.

212
00:10:05,440 --> 00:10:08,680
 They're for the purpose of leading you onward.

213
00:10:08,680 --> 00:10:12,240
 They're not to just maintain the status quo and not go to

214
00:10:12,240 --> 00:10:13,920
 hell.

215
00:10:13,920 --> 00:10:18,320
 They're a means of enlightening you.

216
00:10:18,320 --> 00:10:19,560
 [SPEAKING ARABIC]

217
00:10:19,560 --> 00:10:27,800
 [SPEAKING ARABIC]

218
00:10:27,800 --> 00:10:32,160
 The concentration that comes from morality--

219
00:10:32,160 --> 00:10:34,440
 morality is what leads to concentration.

220
00:10:34,440 --> 00:10:37,640
 And the concentration which comes from morality that is

221
00:10:37,640 --> 00:10:42,240
 properly developed is of great benefit.

222
00:10:42,240 --> 00:10:43,240
 [END PLAYBACK]

