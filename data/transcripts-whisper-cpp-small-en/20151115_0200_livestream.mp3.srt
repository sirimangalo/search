1
00:00:00,000 --> 00:00:15,000
 [Silence]

2
00:00:15,000 --> 00:00:18,000
 Okay, good evening everyone.

3
00:00:18,000 --> 00:00:24,000
 I'm broadcasting live November 14th, 2015.

4
00:00:24,000 --> 00:00:31,140
 Today I was in Toronto for the inauguration or the

5
00:00:31,140 --> 00:00:35,000
 promotion ceremony at a Chinese monastery.

6
00:00:35,000 --> 00:00:38,000
 A Chinese monastery.

7
00:00:38,000 --> 00:00:41,000
 They have a new abbot.

8
00:00:41,000 --> 00:00:44,000
 And the abbot is a very good, well, a friend of mine,

9
00:00:44,000 --> 00:00:47,000
 someone who's becoming a very good friend.

10
00:00:47,000 --> 00:00:52,000
 We're good friends at this point.

11
00:00:52,000 --> 00:00:57,000
 So he invited a bunch of monks and I was one of them.

12
00:00:57,000 --> 00:01:00,000
 And it was quite a ceremony.

13
00:01:00,000 --> 00:01:09,000
 They have quite intricate ceremonies in that tradition.

14
00:01:09,000 --> 00:01:12,000
 But it was quite well done.

15
00:01:12,000 --> 00:01:16,750
 And then they gave us lunch, which wasn't nearly as well

16
00:01:16,750 --> 00:01:19,000
 organized as the actual ceremony.

17
00:01:19,000 --> 00:01:22,150
 But I think they were so busy trying to make perfect the

18
00:01:22,150 --> 00:01:27,000
 ceremony that...

19
00:01:27,000 --> 00:01:30,000
 It was all good.

20
00:01:30,000 --> 00:01:38,330
 And then rushed back just in time to teach, to lead this,

21
00:01:38,330 --> 00:01:43,000
 or to help with this presentation

22
00:01:43,000 --> 00:01:50,000
 to the Residence Life Council, which is all these people

23
00:01:50,000 --> 00:01:55,000
 who look after the first years in residence.

24
00:01:55,000 --> 00:02:01,170
 And people came from all over Canada, actually, from

25
00:02:01,170 --> 00:02:02,000
 various universities.

26
00:02:02,000 --> 00:02:05,640
 So these are undergrad university students who work for

27
00:02:05,640 --> 00:02:12,000
 their universities to support new students.

28
00:02:12,000 --> 00:02:20,000
 And so we had a 50-minute presentation.

29
00:02:20,000 --> 00:02:27,000
 I talked for about 25 minutes.

30
00:02:27,000 --> 00:02:30,000
 And it was good. It went really well.

31
00:02:30,000 --> 00:02:33,000
 They were quite appreciative.

32
00:02:33,000 --> 00:02:36,530
 A few people actually came up to me afterwards and in the

33
00:02:36,530 --> 00:02:37,000
 hallway

34
00:02:37,000 --> 00:02:43,700
 and took my booklet and took my card and were interested in

35
00:02:43,700 --> 00:02:45,000
 learning more.

36
00:02:45,000 --> 00:02:56,000
 One man from Dubai, actually, is an immigrant, that means.

37
00:02:56,000 --> 00:03:03,190
 He was saying in Dubai it's a very, very consumeristic

38
00:03:03,190 --> 00:03:05,000
 materialist.

39
00:03:05,000 --> 00:03:08,000
 So he just wants to get away from that lifestyle.

40
00:03:08,000 --> 00:03:10,570
 He wants to simplify his life, give up his possessions, and

41
00:03:10,570 --> 00:03:11,000
 so on.

42
00:03:11,000 --> 00:03:19,000
 So we had a good talk.

43
00:03:19,000 --> 00:03:21,000
 That's about it.

44
00:03:21,000 --> 00:03:29,000
 That's what happened in the Dhamma today.

45
00:03:29,000 --> 00:03:33,000
 Anything new and exciting with you, Robin?

46
00:03:33,000 --> 00:03:37,000
 No, I watched a lot of the news out of Paris today.

47
00:03:37,000 --> 00:03:39,000
 Oh, Paris, right.

48
00:03:39,000 --> 00:03:43,000
 I heard last night we were having a bonfire.

49
00:03:43,000 --> 00:03:45,000
 Oh, that's another thing, last night we had a bonfire.

50
00:03:45,000 --> 00:03:47,000
 That's why I wasn't here.

51
00:03:47,000 --> 00:03:53,000
 And right, there was something in Paris. What happened?

52
00:03:53,000 --> 00:03:57,840
 There were, I think, seven or eight coordinated ISIS

53
00:03:57,840 --> 00:04:03,670
 terrorists who three blew themselves up and others shot

54
00:04:03,670 --> 00:04:04,000
 people.

55
00:04:04,000 --> 00:04:07,000
 They blew themselves up and shot people.

56
00:04:07,000 --> 00:04:09,000
 Yeah, 129 people did.

57
00:04:09,000 --> 00:04:11,000
 129.

58
00:04:11,000 --> 00:04:16,660
 So like over 500 people involved injured or killed, one way

59
00:04:16,660 --> 00:04:18,000
 or another.

60
00:04:18,000 --> 00:04:24,000
 It's very sad.

61
00:04:24,000 --> 00:04:42,000
 It is very sad.

62
00:04:42,000 --> 00:04:50,000
 There's a line in the song by this Canadian band that goes,

63
00:04:50,000 --> 00:04:54,860
 "What makes a person so poisonous righteous that they'll

64
00:04:54,860 --> 00:05:01,000
 send hell to anyone who just disagrees?"

65
00:05:01,000 --> 00:05:04,000
 That's a good line.

66
00:05:04,000 --> 00:05:08,000
 It's an interesting song. It's "Acapela."

67
00:05:08,000 --> 00:05:12,000
 They did a lot of acapella songs.

68
00:05:12,000 --> 00:05:16,000
 It's about the war in the Gulf, actually.

69
00:05:16,000 --> 00:05:19,000
 It's not taking sides, I don't think.

70
00:05:19,000 --> 00:05:22,420
 It says, "We got a call to write a song about the war in

71
00:05:22,420 --> 00:05:26,000
 the Gulf, but we couldn't hurt anyone's feelings.

72
00:05:26,000 --> 00:05:30,000
 So we tried, then gave up, because there was no such song.

73
00:05:30,000 --> 00:05:33,000
 But the trying was very revealing.

74
00:05:33,000 --> 00:05:38,630
 What makes a person so poisonous righteous that they'll

75
00:05:38,630 --> 00:05:43,000
 send hell to anyone who just disagrees?"

76
00:05:43,000 --> 00:05:45,000
 I don't know about the war in the Gulf.

77
00:05:45,000 --> 00:05:48,350
 I'm not quite sure what they're referring to, but I get the

78
00:05:48,350 --> 00:05:49,000
 sentiment.

79
00:05:49,000 --> 00:05:51,000
 It's a really good question.

80
00:05:51,000 --> 00:05:57,000
 I mean, the answer is not like it's hard to understand.

81
00:05:57,000 --> 00:06:02,000
 It's just amazing how sad.

82
00:06:02,000 --> 00:06:06,740
 That's the saddest part, that people will be so poisonous

83
00:06:06,740 --> 00:06:08,000
 righteous.

84
00:06:08,000 --> 00:06:11,330
 Of course. And then, of course, the response is, you know,

85
00:06:11,330 --> 00:06:12,000
 so many people are saying,

86
00:06:12,000 --> 00:06:16,000
 "Well, we have to go and kill these people."

87
00:06:16,000 --> 00:06:25,240
 But of course, that's only going to just perpetuate this

88
00:06:25,240 --> 00:06:28,000
 over and over.

89
00:06:28,000 --> 00:06:31,000
 Yeah.

90
00:06:31,000 --> 00:06:42,260
 But I think the point is you have to turn to the people who

91
00:06:42,260 --> 00:06:44,000
 are rational.

92
00:06:44,000 --> 00:06:49,180
 And I don't think that's the people who are going to

93
00:06:49,180 --> 00:06:52,000
 respond with violence.

94
00:06:52,000 --> 00:06:57,130
 I mean, to some extent, you have to understand that the

95
00:06:57,130 --> 00:07:00,000
 people on the other side are complicit and responsible,

96
00:07:00,000 --> 00:07:04,390
 and there's a lot of, I think, interest in war and fear and

97
00:07:04,390 --> 00:07:05,000
 that kind of thing.

98
00:07:05,000 --> 00:07:09,000
 It sells people, the arms industry.

99
00:07:09,000 --> 00:07:21,280
 Politicians tend to like it because it makes people vote

100
00:07:21,280 --> 00:07:24,000
 for them.

101
00:07:24,000 --> 00:07:31,480
 It's a good way of controlling people, and control them

102
00:07:31,480 --> 00:07:33,000
 through fear.

103
00:07:33,000 --> 00:07:38,000
 So you have to be clear where we stand, I think.

104
00:07:38,000 --> 00:07:43,000
 We can't take sides, ever.

105
00:07:43,000 --> 00:07:47,000
 We take sides ideologically.

106
00:07:47,000 --> 00:07:50,910
 We should take an ideological side in the sense that

107
00:07:50,910 --> 00:07:53,000
 violence is wrong,

108
00:07:53,000 --> 00:07:58,000
 and instilling fear in people is wrong, bigotry is wrong,

109
00:07:58,000 --> 00:08:02,000
 arrogance, righteousness, all of these things are wrong.

110
00:08:02,000 --> 00:08:07,000
 Well, not righteousness, but this arrogance.

111
00:08:07,000 --> 00:08:10,720
 Righteousness actually is good, but it has to be truly

112
00:08:10,720 --> 00:08:12,000
 righteous.

113
00:08:12,000 --> 00:08:16,340
 There's the idea of being self-righteous, which is the

114
00:08:16,340 --> 00:08:18,000
 sense of,

115
00:08:18,000 --> 00:08:21,000
 "You're right because you think it."

116
00:08:21,000 --> 00:08:23,000
 That's the only reason, right?

117
00:08:23,000 --> 00:08:27,000
 It's right because I think it. I'm right.

118
00:08:27,000 --> 00:08:29,000
 You can't be right, right?

119
00:08:29,000 --> 00:08:32,000
 It's the ideas you have are either right or wrong.

120
00:08:32,000 --> 00:08:36,000
 The rightness exists in the ideas intrinsically.

121
00:08:36,000 --> 00:08:41,000
 It's nothing to do with you being right.

122
00:08:41,000 --> 00:08:45,410
 Well, I mean, there is that, but focusing on that is the

123
00:08:45,410 --> 00:08:46,000
 problem

124
00:08:46,000 --> 00:08:55,000
 that you'll concede it because you're right.

125
00:08:55,000 --> 00:08:58,000
 I guess we also should talk about not being afraid of death

126
00:08:58,000 --> 00:09:07,000
 because terrorism relies on people being afraid.

127
00:09:07,000 --> 00:09:10,750
 I mean, killing a hundred and some people is not going to

128
00:09:10,750 --> 00:09:12,000
 change the world, right?

129
00:09:12,000 --> 00:09:17,000
 But it will if everyone gets very afraid of it.

130
00:09:17,000 --> 00:09:19,000
 I don't know whether that sounds kind of cruel,

131
00:09:19,000 --> 00:09:26,000
 but my point being, this doesn't war make.

132
00:09:26,000 --> 00:09:29,000
 It's a horrible thing that a hundred and some people died.

133
00:09:29,000 --> 00:09:34,000
 I mean, that's just really horrific.

134
00:09:34,000 --> 00:09:39,000
 I mean, tsunami was far more horrific than tsunami in that

135
00:09:39,000 --> 00:09:40,000
 devastated Asia, right?

136
00:09:40,000 --> 00:09:45,000
 Far worse.

137
00:09:45,000 --> 00:09:48,000
 The president of France called it an act of war

138
00:09:48,000 --> 00:09:55,000
 and the Pope said it was the start of World War III.

139
00:09:55,000 --> 00:09:56,000
 It may very well be.

140
00:09:56,000 --> 00:10:00,000
 I mean, that could be what's happening because I mean,

141
00:10:00,000 --> 00:10:03,700
 the point is we don't have bad guys on one side and good

142
00:10:03,700 --> 00:10:05,000
 guys on another.

143
00:10:05,000 --> 00:10:11,260
 The bad guys on the other, on our side, are going to push

144
00:10:11,260 --> 00:10:14,000
 for war, I suppose.

145
00:10:14,000 --> 00:10:18,490
 I suppose there's probably a general sense of not wanting

146
00:10:18,490 --> 00:10:20,000
 to go to war.

147
00:10:20,000 --> 00:10:22,000
 I don't know how it is now as of yesterday.

148
00:10:22,000 --> 00:10:25,000
 That's a big deal, right?

149
00:10:25,000 --> 00:10:30,000
 Yeah.

150
00:10:30,000 --> 00:10:33,000
 And not so long ago, I mean, in the United States,

151
00:10:33,000 --> 00:10:35,000
 people were burnt out of war,

152
00:10:35,000 --> 00:10:41,400
 but probably within the last week ISIS had bombed a Russian

153
00:10:41,400 --> 00:10:42,000
 plane,

154
00:10:42,000 --> 00:10:48,000
 and now this, within one week's time, it frightens people.

155
00:10:48,000 --> 00:10:51,000
 It changes things.

156
00:10:51,000 --> 00:11:07,000
 It's not good for Bernie Sanders.

157
00:11:07,000 --> 00:11:10,000
 No, it's not good at all for Bernie Sanders.

158
00:11:10,000 --> 00:11:11,000
 Poor Bernie.

159
00:11:11,000 --> 00:11:13,000
 You're having a democratic debate tonight,

160
00:11:13,000 --> 00:11:16,000
 and it was supposed to be an economic debate,

161
00:11:16,000 --> 00:11:19,580
 and in that Bernie would have really shown that's really

162
00:11:19,580 --> 00:11:20,000
 his thing,

163
00:11:20,000 --> 00:11:24,990
 and they switched gears, and they're making the focus

164
00:11:24,990 --> 00:11:26,000
 foreign relations

165
00:11:26,000 --> 00:11:28,000
 due to this incident.

166
00:11:28,000 --> 00:11:32,160
 And they sat on the news, one of his aides flipped out or

167
00:11:32,160 --> 00:11:33,000
 something,

168
00:11:33,000 --> 00:11:35,000
 because he didn't want that.

169
00:11:35,000 --> 00:11:38,190
 I'm not sure if it was exactly what it was, but they

170
00:11:38,190 --> 00:11:39,000
 portrayed him very poorly.

171
00:11:39,000 --> 00:11:41,140
 They said his aide flipped out about the change in

172
00:11:41,140 --> 00:11:43,000
 questioning or something.

173
00:11:43,000 --> 00:11:47,000
 So, yeah.

174
00:11:47,000 --> 00:11:51,000
 Probably won't be his best night.

175
00:11:51,000 --> 00:11:55,600
 Well, I guess, I mean, the point is that he's very anti-war

176
00:11:55,600 --> 00:11:56,000
.

177
00:11:56,000 --> 00:11:58,000
 He's a good guy.

178
00:11:58,000 --> 00:12:00,000
 He seems to be one of us.

179
00:12:00,000 --> 00:12:02,000
 Oh, yes, definitely.

180
00:12:02,000 --> 00:12:05,000
 So, now that people are starting to want to go to war,

181
00:12:05,000 --> 00:12:09,000
 that's not good for the peace movement.

182
00:12:09,000 --> 00:12:12,000
 Yes, and people wanting to talk about, you know,

183
00:12:12,000 --> 00:12:16,000
 this as the focus of tonight's debate that was laid.

184
00:12:16,000 --> 00:12:22,000
 He almost put one to say it's a conspiracy against him.

185
00:12:22,000 --> 00:12:31,000
 Oh, well.

186
00:12:31,000 --> 00:12:34,000
 We're all going to die anyway.

187
00:12:34,000 --> 00:12:38,000
 The Earth is slowly but surely moving closer to the sun,

188
00:12:38,000 --> 00:12:40,000
 or is the sun going to explode?

189
00:12:40,000 --> 00:12:41,000
 I can't remember what it is.

190
00:12:41,000 --> 00:12:47,950
 The sun is going to explode first and burn us all to a

191
00:12:47,950 --> 00:12:49,000
 crisp.

192
00:12:49,000 --> 00:12:51,000
 Obviously, we won't be here.

193
00:12:51,000 --> 00:12:56,000
 But if you are, you'll be burned to a crisp.

194
00:12:56,000 --> 00:12:59,020
 Actually, I think what will happen is all the water will

195
00:12:59,020 --> 00:13:02,000
 evaporate first, right?

196
00:13:02,000 --> 00:13:05,000
 So, you'll die of thirst.

197
00:13:05,000 --> 00:13:07,000
 I don't know what you'll die of.

198
00:13:07,000 --> 00:13:09,000
 You'll die of them.

199
00:13:09,000 --> 00:13:12,000
 There's a good Jataka quote.

200
00:13:12,000 --> 00:13:32,000
 Something, something.

201
00:13:32,000 --> 00:13:38,000
 The outcast and the well-born, the fool and ikta wives.

202
00:13:38,000 --> 00:13:43,450
 The rich are poor, one end is sure, each man among them

203
00:13:43,450 --> 00:13:45,000
 dies.

204
00:13:45,000 --> 00:13:47,000
 Very true.

205
00:13:47,000 --> 00:14:02,000
 Do we have any questions tonight?

206
00:14:02,000 --> 00:14:04,000
 I think we do.

207
00:14:07,000 --> 00:14:10,000
 Venerable Bante, in Satipatanasuta,

208
00:14:10,000 --> 00:14:14,120
 what is the meaning of observing the body internally,

209
00:14:14,120 --> 00:14:15,000
 externally,

210
00:14:15,000 --> 00:14:18,000
 and both internally and externally?

211
00:14:18,000 --> 00:14:21,000
 I understand observing body internally,

212
00:14:21,000 --> 00:14:25,220
 but not very clear on what is observing body externally

213
00:14:25,220 --> 00:14:26,000
 means.

214
00:14:26,000 --> 00:14:30,000
 Well, there's two senses in which it could be understood.

215
00:14:30,000 --> 00:14:37,380
 I think the commentary favors the interpretation of the

216
00:14:37,380 --> 00:14:40,000
 internal and external sense-bases.

217
00:14:40,000 --> 00:14:46,190
 So there are, the 'I' is internal and the 'sights' are

218
00:14:46,190 --> 00:14:47,000
 external.

219
00:14:47,000 --> 00:14:55,000
 So if you say, if you say light, light,

220
00:14:55,000 --> 00:14:57,000
 then you're focusing on the external aspect.

221
00:14:57,000 --> 00:15:01,630
 If you say 'I', 'I', I mean, you wouldn't really say these

222
00:15:01,630 --> 00:15:02,000
 things,

223
00:15:02,000 --> 00:15:05,660
 but if you focus, when you say seeing, if you're aware of

224
00:15:05,660 --> 00:15:07,000
 the 'I' seeing,

225
00:15:07,000 --> 00:15:09,000
 then that's internal.

226
00:15:09,000 --> 00:15:12,630
 If you're aware of what you see, the light, that's external

227
00:15:12,630 --> 00:15:13,000
.

228
00:15:13,000 --> 00:15:15,000
 It just means there's two aspects.

229
00:15:15,000 --> 00:15:20,460
 Or when you hear, there is the, if you're aware of the ear

230
00:15:20,460 --> 00:15:21,000
 hearing,

231
00:15:21,000 --> 00:15:23,000
 that's the internal.

232
00:15:23,000 --> 00:15:26,000
 If you're aware of the sound, it's just your focus.

233
00:15:26,000 --> 00:15:28,000
 It can be both.

234
00:15:28,000 --> 00:15:30,000
 Give me one or the other, it can be both.

235
00:15:30,000 --> 00:15:32,000
 That's one way of interpreting.

236
00:15:32,000 --> 00:15:35,000
 But the other one is, if you look at the satipatthana sutta

237
00:15:35,000 --> 00:15:35,000
,

238
00:15:35,000 --> 00:15:39,000
 there are certain sections that are external.

239
00:15:39,000 --> 00:15:44,640
 Not many of them, but there's the section on the cemetery

240
00:15:44,640 --> 00:15:46,000
 contemplations.

241
00:15:46,000 --> 00:15:49,000
 So that's mindfulness of someone else's body.

242
00:15:49,000 --> 00:15:52,000
 It's mindfulness of an external body.

243
00:15:52,000 --> 00:15:56,270
 And so that word 'internally' and 'externally' are both

244
00:15:56,270 --> 00:15:57,000
 internally and externally.

245
00:15:57,000 --> 00:16:01,000
 That phrase occurs at the end of each section.

246
00:16:01,000 --> 00:16:04,840
 But this kind of happens in Pali, where they just repeat it

247
00:16:04,840 --> 00:16:05,000
.

248
00:16:05,000 --> 00:16:08,000
 It's just repeated after the end of each section,

249
00:16:08,000 --> 00:16:12,000
 even though it only applies to certain sections.

250
00:16:12,000 --> 00:16:15,650
 So it may very well be that all that means is, in this sut

251
00:16:15,650 --> 00:16:16,000
ta,

252
00:16:16,000 --> 00:16:20,000
 there are certain sections that are external

253
00:16:20,000 --> 00:16:22,000
 and certain that are internal.

254
00:16:22,000 --> 00:16:26,000
 Of course, most of them are internal.

255
00:16:26,000 --> 00:16:32,990
 But you could think of ways in which you could be aware of

256
00:16:32,990 --> 00:16:35,000
 external feelings

257
00:16:35,000 --> 00:16:37,000
 and thoughts and emotions.

258
00:16:37,000 --> 00:16:40,000
 If you're aware of someone else, you watch them get angry,

259
00:16:40,000 --> 00:16:44,000
 that's kind of a mindfulness, or it could be.

260
00:16:44,000 --> 00:16:46,000
 But it's conceptual. That's not vipassana.

261
00:16:46,000 --> 00:16:48,000
 That won't work for vipassana, but it would work

262
00:16:48,000 --> 00:16:51,990
 in certain instances for samatha, or it works for general

263
00:16:51,990 --> 00:16:53,000
 mental development

264
00:16:53,000 --> 00:16:56,000
 in terms of seeing how anger affects other people, for

265
00:16:56,000 --> 00:16:56,000
 example,

266
00:16:56,000 --> 00:16:59,000
 seeing how greed affects other people.

267
00:16:59,000 --> 00:17:03,000
 It's something you can be mindful of, theoretically,

268
00:17:03,000 --> 00:17:07,000
 or conceptually mindful of, and can help you in your

269
00:17:07,000 --> 00:17:08,000
 spiritual development.

270
00:17:08,000 --> 00:17:12,280
 It's not vipassana. But the sati batana suta isn't just vip

271
00:17:12,280 --> 00:17:14,000
assana.

272
00:17:14,000 --> 00:17:19,180
 What about movement, walking, sitting? Is that internal or

273
00:17:19,180 --> 00:17:20,000
 external?

274
00:17:20,000 --> 00:17:26,000
 That's internal. The body is internal, but external is the

275
00:17:26,000 --> 00:17:26,000
...

276
00:17:26,000 --> 00:17:30,640
 So when you touch the floor, the floor aspect, the hardness

277
00:17:30,640 --> 00:17:31,000
 aspect

278
00:17:31,000 --> 00:17:34,400
 comes from the floor. The softness from the foot is

279
00:17:34,400 --> 00:17:36,000
 internal,

280
00:17:36,000 --> 00:17:39,000
 but the hardness from the floor is external.

281
00:17:39,000 --> 00:17:41,470
 When your teeth go together, they're both hard, that's both

282
00:17:41,470 --> 00:17:42,000
 internal.

283
00:17:42,000 --> 00:17:45,000
 But if you put a spoon in your mouth and you bite on that,

284
00:17:45,000 --> 00:17:48,000
 or food, if you put food in your mouth and you bite on that

285
00:17:48,000 --> 00:17:48,000
,

286
00:17:48,000 --> 00:17:52,000
 the food is external. I mean, it gets blurred there

287
00:17:52,000 --> 00:17:56,100
 because the food becomes internal, but it might be

288
00:17:56,100 --> 00:17:59,000
 considered internal at that point.

289
00:17:59,000 --> 00:18:11,000
 Thank you, Bhante.

290
00:18:11,000 --> 00:18:15,620
 A comment that we might cross, "Andromita and the Milky Way

291
00:18:15,620 --> 00:18:16,000
 will collapse

292
00:18:16,000 --> 00:18:19,880
 with it before the sun turns nova in a matter of billions

293
00:18:19,880 --> 00:18:21,000
 of years."

294
00:18:21,000 --> 00:18:28,000
 Perhaps. Okay. I'll put that on my calendar.

295
00:18:28,000 --> 00:18:35,090
 One of those perpetual calendars for billions of years from

296
00:18:35,090 --> 00:18:36,000
 now.

297
00:18:36,000 --> 00:18:39,000
 I'm not sure if this is a question for you, Bhante,

298
00:18:39,000 --> 00:18:42,000
 or if it's a general discussion about the sun and all,

299
00:18:42,000 --> 00:18:45,000
 but when that happens, I wonder if people will be reborn

300
00:18:45,000 --> 00:18:50,000
 in the nearest M-class planet, wherever that is.

301
00:18:50,000 --> 00:18:53,000
 Wondering, wondering.

302
00:18:53,000 --> 00:18:57,000
 Yes, wondering what an M-class planet is.

303
00:18:57,000 --> 00:19:03,000
 Wondering, wondering, wondering.

304
00:19:03,000 --> 00:19:05,270
 And someone asking, "Will you be answering questions? I'm

305
00:19:05,270 --> 00:19:06,000
 here."

306
00:19:06,000 --> 00:19:09,000
 Yes, submit your question.

307
00:19:09,000 --> 00:19:11,000
 I wonder if they're not getting the audio or video.

308
00:19:11,000 --> 00:19:14,000
 Maybe you can post a link to the YouTube.

309
00:19:14,000 --> 00:19:15,000
 Sure.

310
00:19:15,000 --> 00:19:19,000
 Do you have the link, or only I do, right?

311
00:19:19,000 --> 00:19:22,000
 I can get it through YouTube.

312
00:19:22,000 --> 00:19:27,000
 Well, I got it here.

313
00:19:27,000 --> 00:19:33,000
 Copy.

314
00:19:33,000 --> 00:19:35,350
 So the YouTube is like, what, 30 seconds behind or

315
00:19:35,350 --> 00:19:36,000
 something?

316
00:19:36,000 --> 00:20:00,000
 Yes.

317
00:20:00,000 --> 00:20:03,000
 So how was the bonfire?

318
00:20:03,000 --> 00:20:06,000
 It was good. No one came, but it was good.

319
00:20:06,000 --> 00:20:10,000
 We had two new people come, and they left early.

320
00:20:10,000 --> 00:20:14,000
 We started talking about some fairly theoretical stuff,

321
00:20:14,000 --> 00:20:18,000
 and then they just got up and left and said goodbye.

322
00:20:18,000 --> 00:20:22,000
 We wonder, "Oh, did we scare them away? I don't know."

323
00:20:22,000 --> 00:20:30,000
 We stayed for two hours.

324
00:20:30,000 --> 00:20:33,000
 It's an interesting group.

325
00:20:33,000 --> 00:20:36,000
 They're not all meditators.

326
00:20:36,000 --> 00:20:41,000
 Most of them are interested in meditation.

327
00:20:41,000 --> 00:20:46,000
 But we had about six or eight people.

328
00:20:46,000 --> 00:20:51,000
 Oh, that's good. It sounds nice.

329
00:20:51,000 --> 00:20:56,190
 We did talk about the idea of doing public meditations in

330
00:20:56,190 --> 00:20:58,000
 the student center.

331
00:20:58,000 --> 00:21:02,720
 There's a big open area where there's huge crowds of people

332
00:21:02,720 --> 00:21:04,000
 walking through,

333
00:21:04,000 --> 00:21:08,660
 and we could just sit down and do meditation in the public

334
00:21:08,660 --> 00:21:10,000
 space there.

335
00:21:10,000 --> 00:21:12,000
 We could even just use it to teach meditation.

336
00:21:12,000 --> 00:21:19,000
 I could teach people as they came in.

337
00:21:19,000 --> 00:21:22,000
 And then if we got a group going, we could start doing

338
00:21:22,000 --> 00:21:25,000
 flash moms, meditation flash moms.

339
00:21:25,000 --> 00:21:29,000
 Did you ever see that video we did in Winnipeg?

340
00:21:29,000 --> 00:21:31,000
 We did.

341
00:21:31,000 --> 00:21:34,390
 I walked in, sat down, then 30 seconds later a second

342
00:21:34,390 --> 00:21:36,000
 person came and sat down,

343
00:21:36,000 --> 00:21:39,000
 and 30 seconds we added a person every 30 seconds.

344
00:21:39,000 --> 00:21:44,000
 It was only about 20 of us, but it was fun.

345
00:21:44,000 --> 00:21:47,920
 Yeah, it definitely sounds a little more comfortable in the

346
00:21:47,920 --> 00:21:54,000
 student center than outdoors this time of year.

347
00:21:54,000 --> 00:22:02,310
 Yeah, so maybe we'll try that. It's just a matter of

348
00:22:02,310 --> 00:22:07,000
 scheduling.

349
00:22:07,000 --> 00:22:10,000
 Darwin explained M class planet is life-supporting planet.

350
00:22:10,000 --> 00:22:13,000
 Thank you, Darwin.

351
00:22:13,000 --> 00:22:20,000
 I hadn't heard that term before.

352
00:22:20,000 --> 00:22:24,160
 What is a good strategy when trying to get people to med

353
00:22:24,160 --> 00:22:25,000
itate?

354
00:22:25,000 --> 00:22:28,000
 Friends and family.

355
00:22:28,000 --> 00:22:31,030
 I acknowledge your desire for other people to meditate.

356
00:22:31,030 --> 00:22:35,000
 That's important.

357
00:22:35,000 --> 00:22:39,000
 Because I can't think of a good strategy.

358
00:22:39,000 --> 00:22:43,520
 It's not about getting people to meditate. Meditation's

359
00:22:43,520 --> 00:22:45,000
 hard. If you don't really want to do it,

360
00:22:45,000 --> 00:22:49,000
 it's not really likely to succeed.

361
00:22:49,000 --> 00:22:54,980
 Maybe that's pessimistic, but it can't help but be a bit

362
00:22:54,980 --> 00:22:57,000
 pessimistic or skeptical.

363
00:22:57,000 --> 00:22:59,990
 Because you can get people to do a little bit of meditation

364
00:22:59,990 --> 00:23:00,000
,

365
00:23:00,000 --> 00:23:02,430
 but you find you're pushing them and it's effort, and then

366
00:23:02,430 --> 00:23:04,000
 they just give it up anyway.

367
00:23:04,000 --> 00:23:06,870
 If a person wants to do it, it's something that you won't

368
00:23:06,870 --> 00:23:08,000
 need to push them.

369
00:23:08,000 --> 00:23:14,280
 I think a good strategy is to talk about it, but not have

370
00:23:14,280 --> 00:23:17,000
 any emotional vestedness.

371
00:23:17,000 --> 00:23:20,620
 Don't let it get to you emotionally. I want them to med

372
00:23:20,620 --> 00:23:23,000
itate. Just do it as a matter of course.

373
00:23:23,000 --> 00:23:26,030
 Of course you're going to tell people to meditate. Of

374
00:23:26,030 --> 00:23:27,000
 course you're going to talk about it.

375
00:23:27,000 --> 00:23:36,000
 Don't be afraid to talk about it.

376
00:23:36,000 --> 00:23:41,340
 It's so hard when you see someone who is struggling and you

377
00:23:41,340 --> 00:23:46,000
 know how much it would help them.

378
00:23:46,000 --> 00:23:49,000
 That's the thing, for many people it wouldn't help them.

379
00:23:49,000 --> 00:23:54,150
 They'd try to practice and they'd fail. Then they'd get

380
00:23:54,150 --> 00:23:57,000
 discouraged.

381
00:23:57,000 --> 00:23:59,640
 It would help people if they suddenly had no more defile

382
00:23:59,640 --> 00:24:00,000
ments,

383
00:24:00,000 --> 00:24:05,000
 but it's getting from here to there.

384
00:24:05,000 --> 00:24:08,000
 For most people it's just too far.

385
00:24:08,000 --> 00:24:11,000
 So you have to be ready for it in other words?

386
00:24:11,000 --> 00:24:15,000
 Yeah, you have to have some potentiality.

387
00:24:15,000 --> 00:24:24,360
 We don't have time or resources to bring people from zero

388
00:24:24,360 --> 00:24:27,000
 to a hundred.

389
00:24:27,000 --> 00:24:29,000
 It's not really in best interest to do so.

390
00:24:29,000 --> 00:24:32,000
 You're better off taking in people who are really ready

391
00:24:32,000 --> 00:24:35,400
 because they'll turn around and help the people who are

392
00:24:35,400 --> 00:24:38,000
 less ready and less ready and so on.

393
00:24:38,000 --> 00:24:41,000
 It'll work like that.

394
00:24:41,000 --> 00:24:47,000
 That makes sense.

395
00:24:47,000 --> 00:24:50,190
 How can one truly become enlightened if no matter how much

396
00:24:50,190 --> 00:24:54,000
 he meditates, he will still require food?

397
00:24:54,000 --> 00:24:57,000
 Is this a person who has done this meditation with us?

398
00:24:57,000 --> 00:25:01,000
 I think this is a new person.

399
00:25:01,000 --> 00:25:09,000
 I'm going to skip this question and say,

400
00:25:09,000 --> 00:25:11,510
 "You have to start meditating with us if you want me to

401
00:25:11,510 --> 00:25:13,000
 answer your questions."

402
00:25:13,000 --> 00:25:17,440
 It's not a hard and fast rule, but I want you to be med

403
00:25:17,440 --> 00:25:24,000
itating before you ask this question.

404
00:25:24,000 --> 00:25:27,000
 Maybe you have started meditating, but you should read my

405
00:25:27,000 --> 00:25:28,000
 booklet if you haven't.

406
00:25:28,000 --> 00:25:32,460
 If you have, or once you have, then start meditating and

407
00:25:32,460 --> 00:25:36,000
 come back when you're green

408
00:25:36,000 --> 00:25:41,800
 because your question thing is yellow or orange, which

409
00:25:41,800 --> 00:25:45,000
 means you haven't meditated.

410
00:25:45,000 --> 00:25:50,140
 Why? Because the question is a little bit... it has to have

411
00:25:50,140 --> 00:25:51,000
 a nuanced answer

412
00:25:51,000 --> 00:25:56,530
 and it's a question that may be not coming from lack of

413
00:25:56,530 --> 00:26:01,000
 information or lack of understanding about meditation.

414
00:26:01,000 --> 00:26:04,140
 So I want to be sure that you understand meditation before

415
00:26:04,140 --> 00:26:06,000
 you ask that, if you want to ask that.

416
00:26:06,000 --> 00:26:11,850
 Maybe you'll meditate and you'll say, "Oh, I don't need to

417
00:26:11,850 --> 00:26:16,000
 ask that anymore."

418
00:26:16,000 --> 00:26:19,520
 I think that's why people who have been meditating for a

419
00:26:19,520 --> 00:26:22,000
 while, they ask fewer questions.

420
00:26:22,000 --> 00:26:28,000
 Yeah. In the meditation course, that happens.

421
00:26:28,000 --> 00:26:31,450
 The beginner meditators, you have a lot to tell them to

422
00:26:31,450 --> 00:26:33,000
 explain and you have to guide them.

423
00:26:33,000 --> 00:26:36,020
 But as they get on in the course, there's really less and

424
00:26:36,020 --> 00:26:37,000
 less for you to do

425
00:26:37,000 --> 00:26:53,920
 except just confirm that they're doing okay and give them

426
00:26:53,920 --> 00:27:00,000
 the new exercise.

427
00:27:00,000 --> 00:27:05,840
 Someone asked me, and this man from Dubai asked me, he

428
00:27:05,840 --> 00:27:09,000
 asked a question in the group

429
00:27:09,000 --> 00:27:14,000
 whether it's okay to listen to music when you meditate.

430
00:27:14,000 --> 00:27:16,180
 And so I explained, he said, "Well, it depends what you

431
00:27:16,180 --> 00:27:17,000
 mean by meditation,

432
00:27:17,000 --> 00:27:20,620
 but in our meditation we're trying to become objective, so

433
00:27:20,620 --> 00:27:24,000
 everything is music."

434
00:27:24,000 --> 00:27:29,120
 The point is that if you are partial towards the music and

435
00:27:29,120 --> 00:27:31,000
 the music makes it easier for you to focus.

436
00:27:31,000 --> 00:27:33,500
 Actually, he asked this in the beginning, I just said it

437
00:27:33,500 --> 00:27:35,000
 like that, but then he came up later

438
00:27:35,000 --> 00:27:37,920
 and he said, "Well, the music is like nature music with

439
00:27:37,920 --> 00:27:40,000
 waterfalls and that kind of thing."

440
00:27:40,000 --> 00:27:43,430
 And I said, "Well, if it makes it easier for you to focus,

441
00:27:43,430 --> 00:27:45,000
 then it's a crutch for you."

442
00:27:45,000 --> 00:27:49,480
 And we're not interested in that. We're interested in how

443
00:27:49,480 --> 00:27:53,000
 you react when it's difficult.

444
00:27:53,000 --> 00:28:01,000
 So we could make it very, very easy to meditate.

445
00:28:01,000 --> 00:28:04,760
 One meditator here recently, he was, I mean, always medit

446
00:28:04,760 --> 00:28:06,000
ators will try to find a trick

447
00:28:06,000 --> 00:28:09,000
 or a way to make the meditation easier.

448
00:28:09,000 --> 00:28:11,000
 And I said, "Dumal, I can make it really easy for you."

449
00:28:11,000 --> 00:28:13,750
 And he said, "Yeah, you can." I said, "Yeah, you do 10

450
00:28:13,750 --> 00:28:16,000
 minutes of lying meditation a day,

451
00:28:16,000 --> 00:28:19,590
 and that's your meditation. Very easy. But what do you get

452
00:28:19,590 --> 00:28:20,000
 from it?"

453
00:28:20,000 --> 00:28:24,000
 We get nothing.

454
00:28:24,000 --> 00:28:26,740
 We're trying to make it more difficult. We're trying to

455
00:28:26,740 --> 00:28:28,000
 challenge ourselves.

456
00:28:28,000 --> 00:28:30,690
 We're trying to see what happens, and we're trying to learn

457
00:28:30,690 --> 00:28:32,000
 how we react when the going gets tough

458
00:28:32,000 --> 00:28:35,000
 so that we can change the way we react.

459
00:28:35,000 --> 00:28:39,070
 If you don't have things that trigger you, then you'll

460
00:28:39,070 --> 00:28:43,000
 never learn how to overcome the trigger.

461
00:28:43,000 --> 00:28:54,000
 So making it easy to meditate is not useful.

462
00:28:54,000 --> 00:28:56,410
 It's great that someone from Dubai is interested in

463
00:28:56,410 --> 00:28:57,000
 meditation.

464
00:28:57,000 --> 00:29:02,750
 I mean, everything is so flashy and so many spectacular

465
00:29:02,750 --> 00:29:04,000
 things there.

466
00:29:04,000 --> 00:29:17,620
 So it's great that somebody is interested in something that

467
00:29:17,620 --> 00:29:24,000
 isn't impressed by that.

468
00:29:24,000 --> 00:29:29,030
 I find it easier to note experiences during walking

469
00:29:29,030 --> 00:29:32,000
 meditation quietly out loud.

470
00:29:32,000 --> 00:29:35,000
 "Is this okay? Thanks."

471
00:29:35,000 --> 00:29:38,110
 Yeah, again, you find it easier, and that's something that

472
00:29:38,110 --> 00:29:39,000
 you shouldn't do.

473
00:29:39,000 --> 00:29:47,000
 We're not trying to make it easy. We're trying to be...

474
00:29:47,000 --> 00:29:52,340
 trying to get stronger. You'll never get stronger if you

475
00:29:52,340 --> 00:29:56,000
 just make it easier on yourself.

476
00:29:56,000 --> 00:30:07,000
 So it's okay, but it's less useful.

477
00:30:07,000 --> 00:30:10,510
 It's also distracting because then your mind isn't with the

478
00:30:10,510 --> 00:30:11,000
 object.

479
00:30:11,000 --> 00:30:14,000
 It's with your mouth. It's at your mouth.

480
00:30:14,000 --> 00:30:17,000
 It's mixed up trying to walk and talk at the same time.

481
00:30:17,000 --> 00:30:20,000
 Your mind is doing two things.

482
00:30:20,000 --> 00:30:24,000
 An easy way to get more distracted.

483
00:30:24,000 --> 00:30:27,000
 You want to make it easy, stop doing walking meditation.

484
00:30:27,000 --> 00:30:28,000
 Just start dancing.

485
00:30:28,000 --> 00:30:33,000
 Turn on some music and start dancing. That'll be easy.

486
00:30:33,000 --> 00:30:38,000
 And shout out the lyrics to the song.

487
00:30:38,000 --> 00:30:41,000
 It's not about making it easy.

488
00:30:41,000 --> 00:30:45,000
 It's actually more about making it fairly difficult.

489
00:30:45,000 --> 00:30:48,250
 Not to the extent that you should punch yourself to

490
00:30:48,250 --> 00:30:50,000
 understand pain or something,

491
00:30:50,000 --> 00:30:56,530
 but difficult in the sense of patience, requiring patience

492
00:30:56,530 --> 00:30:58,000
 and focus.

493
00:30:58,000 --> 00:31:03,660
 But if something is so distracting you that it won't leave

494
00:31:03,660 --> 00:31:04,000
 you,

495
00:31:04,000 --> 00:31:09,260
 then don't recommend to stand and note that for a little

496
00:31:09,260 --> 00:31:10,000
 bit.

497
00:31:10,000 --> 00:31:15,000
 Yeah, I mean that's only because again walking and noting

498
00:31:15,000 --> 00:31:16,000
 something else

499
00:31:16,000 --> 00:31:21,800
 is too diverse. It's better to be focused on one thing, so

500
00:31:21,800 --> 00:31:23,000
 stopping makes sense.

501
00:31:42,000 --> 00:31:47,000
 We've caught up. I think we're all caught up, Aunty.

502
00:31:47,000 --> 00:31:55,000
 Okay. Well then, good night everyone.

503
00:31:55,000 --> 00:31:57,000
 Thank you, Bhagavai.

504
00:31:57,000 --> 00:32:00,000
 You're tuning in. Thanks Robin for your help.

505
00:32:00,000 --> 00:32:03,000
 Thank you. Good night.

506
00:32:05,000 --> 00:32:07,000
 Thank you.

