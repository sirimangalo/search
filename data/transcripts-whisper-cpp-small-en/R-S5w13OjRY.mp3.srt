1
00:00:00,000 --> 00:00:16,270
 Hello everyone. Welcome back to the Q&A series. Today's

2
00:00:16,270 --> 00:00:19,960
 question is about dealing with intense

3
00:00:19,960 --> 00:00:29,390
 emotions. So the question was specifically regarding

4
00:00:29,390 --> 00:00:36,200
 longing. Someone whose fiancée who

5
00:00:36,200 --> 00:00:40,960
 they loved very much was away for an extended period of

6
00:00:40,960 --> 00:00:43,640
 time and they found it interrupted

7
00:00:43,640 --> 00:00:48,890
 their meditation practice. Intense states of longing that

8
00:00:48,890 --> 00:00:51,280
 prevented them from having

9
00:00:51,280 --> 00:00:56,930
 a peaceful meditation session. So the question is what to

10
00:00:56,930 --> 00:01:06,680
 do. What to do about this. So I

11
00:01:06,680 --> 00:01:19,650
 think first and foremost we have to be clear about how we

12
00:01:19,650 --> 00:01:24,600
 look at peace from a meditative

13
00:01:24,600 --> 00:01:31,220
 perspective. How does peace relate to mindfulness practice?

14
00:01:31,220 --> 00:01:34,500
 Peace is the goal of mindfulness

15
00:01:34,500 --> 00:01:39,630
 insight meditation practice. It's the goal of Buddhism. But

16
00:01:39,630 --> 00:01:47,280
 how peaceful our meditation

17
00:01:47,280 --> 00:01:55,880
 practice is going to be is going to depend very much upon

18
00:01:55,880 --> 00:01:58,880
 our habits and the qualities

19
00:01:58,880 --> 00:02:07,330
 of the states of mind that make up our habit. Mindfulness

20
00:02:07,330 --> 00:02:10,920
 is about focusing on our objective

21
00:02:10,920 --> 00:02:16,410
 reality. It's about confronting that reality. We saya bhim

22
00:02:16,410 --> 00:02:19,320
ukha. That's the Pali of facing,

23
00:02:19,320 --> 00:02:27,120
 confronting. Confronting reality basically. So if your

24
00:02:27,120 --> 00:02:30,920
 reality is one of longing then

25
00:02:30,920 --> 00:02:36,080
 that's the reality that you're going to face. Expecting

26
00:02:36,080 --> 00:02:41,080
 your meditation to be peaceful is

27
00:02:41,080 --> 00:02:48,240
 incorrect, is improper, unreasonable. Mindfulness

28
00:02:48,240 --> 00:02:53,160
 meditation is about creating specific pleasant

29
00:02:53,160 --> 00:02:56,910
 states. It's about dealing with the states that exist in

30
00:02:56,910 --> 00:02:58,840
 your mind and learning about

31
00:02:58,840 --> 00:03:04,560
 them and understanding them. So that's first is not to

32
00:03:04,560 --> 00:03:07,120
 consider that your practice is bad

33
00:03:07,120 --> 00:03:10,560
 because in fact the description that you're giving of your

34
00:03:10,560 --> 00:03:12,200
 practice is something that's

35
00:03:12,200 --> 00:03:18,290
 praiseworthy. From the sounds of it you're practicing quite

36
00:03:18,290 --> 00:03:21,040
 well. What you're seeing

37
00:03:21,040 --> 00:03:28,330
 is that the result of longing is stressful. And what you're

38
00:03:28,330 --> 00:03:31,440
 also seeing or are able to

39
00:03:31,440 --> 00:03:37,500
 see from this over time especially is that the attachment

40
00:03:37,500 --> 00:03:40,360
 you have for this person is

41
00:03:40,360 --> 00:03:45,640
 leading to the longing which is leading to stress and

42
00:03:45,640 --> 00:03:49,160
 unpleasantness. It's a fact. It's

43
00:03:49,160 --> 00:03:57,500
 an aspect of clinging. The Buddha didn't just make up all

44
00:03:57,500 --> 00:04:01,760
 up that stuff about craving being

45
00:04:01,760 --> 00:04:13,340
 the cause of suffering. The pleasant sensations that we

46
00:04:13,340 --> 00:04:16,760
 strive for, that we crave for lead

47
00:04:16,760 --> 00:04:22,260
 to addiction and dissatisfaction when we don't get what we

48
00:04:22,260 --> 00:04:25,720
 want. When we are a piye hi sampa

49
00:04:25,720 --> 00:04:31,730
 yo go being associated with what is not pleasant, piye hi

50
00:04:31,730 --> 00:04:35,320
 we pi, we pa yo go being separated

51
00:04:35,320 --> 00:04:43,610
 from what is dear to us. This is suffering. It's distress.

52
00:04:43,610 --> 00:04:47,400
 But important to note as well

53
00:04:47,400 --> 00:04:54,390
 is that mindfulness is not about denying or rejecting the

54
00:04:54,390 --> 00:04:57,800
 desire, the attachment, what

55
00:04:57,800 --> 00:05:02,560
 you would say the love for this person. It's for facing it

56
00:05:02,560 --> 00:05:05,160
 and observing it which is what

57
00:05:05,160 --> 00:05:09,720
 you're doing. And the practice that you're doing from the

58
00:05:09,720 --> 00:05:12,080
 sounds of it, if continued,

59
00:05:12,080 --> 00:05:18,300
 if you're patient, and if you can face those emotions is

60
00:05:18,300 --> 00:05:22,120
 going to lead to more contentment,

61
00:05:22,120 --> 00:05:28,160
 more peace, less attachment, less stress and less suffering

62
00:05:28,160 --> 00:05:31,200
. And this can be generalized

63
00:05:31,200 --> 00:05:35,750
 to any sort of extreme state like this. You don't have to

64
00:05:35,750 --> 00:05:37,120
 reject. First of all, don't

65
00:05:37,120 --> 00:05:41,970
 reject the stress and the suffering that comes from these

66
00:05:41,970 --> 00:05:45,280
 states that appears in meditation.

67
00:05:45,280 --> 00:05:50,240
 "Oh, my meditation is bad because I'm stressed and disquiet

68
00:05:50,240 --> 00:05:52,760
ed during the practice. I'm no

69
00:05:52,760 --> 00:05:57,790
 good at this meditation. I'm doing something wrong." There

70
00:05:57,790 --> 00:05:58,960
's one of my videos I think

71
00:05:58,960 --> 00:06:06,120
 was quite complete in describing, I think it's called, "The

72
00:06:06,120 --> 00:06:09,400
 Experience of Reality."

73
00:06:09,400 --> 00:06:11,740
 And I definitely recommend that video because it summed up

74
00:06:11,740 --> 00:06:13,120
 pretty much everything I wanted

75
00:06:13,120 --> 00:06:18,850
 to say about this sort of idea that we have bad practice

76
00:06:18,850 --> 00:06:22,440
 because it's unpleasant, it's

77
00:06:22,440 --> 00:06:26,010
 unpredictable because it's uncontrollable. Why that's

78
00:06:26,010 --> 00:06:28,600
 actually a good sign. It's a sign

79
00:06:28,600 --> 00:06:35,740
 of learning something about cause and effect, the nature of

80
00:06:35,740 --> 00:06:38,480
 reality and so on. But also

81
00:06:38,480 --> 00:06:45,490
 don't reject or fight against the negative, against the

82
00:06:45,490 --> 00:06:50,040
 causes of the negative experiences.

83
00:06:50,040 --> 00:06:57,790
 So desire aversion, trying to reject our attachment and

84
00:06:57,790 --> 00:07:02,920
 reject our love for other people and so

85
00:07:02,920 --> 00:07:08,680
 on. It's not necessary, it's not really all that helpful.

86
00:07:08,680 --> 00:07:09,880
 We're best served by studying

87
00:07:09,880 --> 00:07:14,070
 and observing. Then you don't have to believe me that I say

88
00:07:14,070 --> 00:07:16,160
, "Oh, well, this has to do with

89
00:07:16,160 --> 00:07:19,600
 your intense attachment to this person and you'd be better

90
00:07:19,600 --> 00:07:21,840
 off getting rid of it." Naturally,

91
00:07:21,840 --> 00:07:25,160
 as you observe, you become less attached to people. You

92
00:07:25,160 --> 00:07:27,000
 become less dependent on them

93
00:07:27,000 --> 00:07:30,650
 and thus less susceptible to the suffering that comes from

94
00:07:30,650 --> 00:07:32,560
 change. The suffering that

95
00:07:32,560 --> 00:07:37,730
 comes not from change but from our inability to bear with

96
00:07:37,730 --> 00:07:40,200
 and to deal with and to live

97
00:07:40,200 --> 00:07:48,350
 with change in permanence. That's about the core of it in

98
00:07:48,350 --> 00:07:51,880
 regards to intense states. Sometimes

99
00:07:51,880 --> 00:07:54,970
 they can get so intense that it's very difficult to

100
00:07:54,970 --> 00:07:57,800
 practice. It's easy to say, "Well, just

101
00:07:57,800 --> 00:08:02,420
 try and bear with them." That's the theory. There are many

102
00:08:02,420 --> 00:08:05,680
 ways that you can reduce extreme

103
00:08:05,680 --> 00:08:09,280
 states but a lot of them have to do with not having a fian

104
00:08:09,280 --> 00:08:11,640
cé, for example, living in the

105
00:08:11,640 --> 00:08:18,300
 forest, being celibate, eating one meal a day. One great

106
00:08:18,300 --> 00:08:20,160
 thing that works even for people

107
00:08:20,160 --> 00:08:26,360
 who have partners and children and material lives, worldly

108
00:08:26,360 --> 00:08:29,360
 lives, is to do a meditation

109
00:08:29,360 --> 00:08:36,380
 course. Find some time to either do a course intensively or

110
00:08:36,380 --> 00:08:39,120
 do some sort of "we do" courses

111
00:08:39,120 --> 00:08:42,260
 where people meet once a week. It's far less effective and

112
00:08:42,260 --> 00:08:43,840
 less profound is the change

113
00:08:43,840 --> 00:08:48,560
 if you do a one hour a day meditation course. But I'd say

114
00:08:48,560 --> 00:08:51,480
 it's still helpful. Undergo some

115
00:08:51,480 --> 00:08:54,790
 training and the guidance from the teacher, the

116
00:08:54,790 --> 00:08:57,520
 psychological support that comes from

117
00:08:57,520 --> 00:09:00,630
 knowing that you've got someone in your corner who's

118
00:09:00,630 --> 00:09:02,920
 guiding you. And in fact the guidance

119
00:09:02,920 --> 00:09:06,530
 that they give, if they're qualified to give it, can be

120
00:09:06,530 --> 00:09:08,840
 quite helpful in terms of helping

121
00:09:08,840 --> 00:09:11,750
 you to deal with the more extreme states especially. So

122
00:09:11,750 --> 00:09:15,880
 there you go. That's an answer to that

123
00:09:15,880 --> 00:09:19,360
 question. Thank you all for tuning in. Have a good day.

124
00:09:19,360 --> 00:09:26,360
 [

