1
00:00:00,000 --> 00:00:07,000
 [birds chirping]

2
00:00:07,000 --> 00:00:27,000
 [birds chirping]

3
00:00:27,000 --> 00:00:33,000
 [music fades]

4
00:00:33,000 --> 00:00:37,000
 Good evening everyone.

5
00:00:37,000 --> 00:00:44,000
 Welcome to evening Dhamma.

6
00:00:44,000 --> 00:00:49,000
 Welcome back.

7
00:00:49,000 --> 00:00:57,000
 Today we're continuing on studying the Satipatthana Sutta.

8
00:00:57,000 --> 00:01:05,100
 We're in what is called Sampajanya, Sampajanya Baba, or

9
00:01:05,100 --> 00:01:07,000
 Sampajana Baba.

10
00:01:07,000 --> 00:01:11,000
 Baba means sections somehow.

11
00:01:11,000 --> 00:01:16,000
 What we're looking at is Sampajanya,

12
00:01:16,000 --> 00:01:20,000
 this concept of Sampajanya,

13
00:01:20,000 --> 00:01:28,260
 which a lot is made of this word, but in many ways it's

14
00:01:28,260 --> 00:01:31,000
 just another way of describing mindfulness

15
00:01:31,000 --> 00:01:42,000
 or describing the effects of mindfulness.

16
00:01:42,000 --> 00:01:46,650
 When we talk about Sati, Sati is the act of grasping the

17
00:01:46,650 --> 00:01:48,000
 object.

18
00:01:48,000 --> 00:01:52,000
 Often they're paired together, Sati and Sampajanya,

19
00:01:52,000 --> 00:01:58,000
 or sometimes it's just Sati and Panya, Satipanya.

20
00:01:58,000 --> 00:02:04,100
 Sampajanya really is just a fancy way of talking about

21
00:02:04,100 --> 00:02:05,000
 wisdom,

22
00:02:05,000 --> 00:02:11,000
 I mean, not in a good way, because, in a good way,

23
00:02:11,000 --> 00:02:18,810
 sometimes if you just say wisdom, wisdom, wisdom, it loses

24
00:02:18,810 --> 00:02:20,000
 the meaning.

25
00:02:20,000 --> 00:02:24,000
 It just becomes a catch word, a buzzword.

26
00:02:24,000 --> 00:02:29,000
 So when you say Panya, Panya just means wisdom,

27
00:02:29,000 --> 00:02:37,000
 but when you say full and complete understanding,

28
00:02:37,000 --> 00:02:45,000
 it has a little bit more of a meaning to it.

29
00:02:45,000 --> 00:02:48,000
 But it really means, it's what we mean when we say wisdom,

30
00:02:48,000 --> 00:02:54,660
 but what do we mean by wisdom? It means full and complete

31
00:02:54,660 --> 00:02:57,000
 knowledge,

32
00:02:57,000 --> 00:03:04,080
 or intensive, the commentary talks about Sampajanya as

33
00:03:04,080 --> 00:03:08,000
 being continuous knowledge,

34
00:03:08,000 --> 00:03:16,380
 always knowing. So it can be, I mean, we interpret it in

35
00:03:16,380 --> 00:03:20,000
 different ways.

36
00:03:20,000 --> 00:03:25,000
 But it means knowledge, it means this special knowing,

37
00:03:25,000 --> 00:03:28,550
 what it means is non-intellectual knowing, it means really

38
00:03:28,550 --> 00:03:31,000
 being aware of something.

39
00:03:31,000 --> 00:03:37,320
 Again, it's this, well, the commentary describes four ways

40
00:03:37,320 --> 00:03:38,000
 of understanding,

41
00:03:38,000 --> 00:03:42,350
 four types of Sampajanya, only one of which we're actually

42
00:03:42,350 --> 00:03:43,000
 interested in,

43
00:03:43,000 --> 00:03:46,930
 but in order to give an idea of what the word means, or how

44
00:03:46,930 --> 00:03:49,000
 it's used.

45
00:03:49,000 --> 00:03:55,750
 So the first is Ata Sampajanya, knowledge, or full and

46
00:03:55,750 --> 00:04:01,000
 complete knowledge of purpose.

47
00:04:01,000 --> 00:04:07,300
 So it kind of means having, well, it means having wisdom

48
00:04:07,300 --> 00:04:09,000
 about purpose,

49
00:04:09,000 --> 00:04:14,000
 or wisdom because of knowing the purpose.

50
00:04:14,000 --> 00:04:19,000
 So you have this clear state of mind because you're sure

51
00:04:19,000 --> 00:04:22,000
 why you're doing something.

52
00:04:22,000 --> 00:04:26,000
 So it means knowing the difference between purpose and

53
00:04:26,000 --> 00:04:26,000
 purpose,

54
00:04:26,000 --> 00:04:36,490
 useful and not useful, purpose and purposeless, purposeful

55
00:04:36,490 --> 00:04:40,000
 and purposeless.

56
00:04:40,000 --> 00:04:45,000
 Knowing that doing something has benefit, has value,

57
00:04:45,000 --> 00:04:50,000
 and knowing that doing something is futile has no value.

58
00:04:50,000 --> 00:04:53,190
 That's one of the big things, I mean, this, in regards to

59
00:04:53,190 --> 00:04:54,000
 the Dhamma,

60
00:04:54,000 --> 00:04:58,000
 it's one of the big things that we learn in Buddhism.

61
00:04:58,000 --> 00:05:03,570
 Learning that killing is not useful, stealing is not useful

62
00:05:03,570 --> 00:05:04,000
.

63
00:05:04,000 --> 00:05:10,000
 Learning that ambition is not useful.

64
00:05:10,000 --> 00:05:15,030
 Learning that addiction is not useful, attachment is not

65
00:05:15,030 --> 00:05:16,000
 useful.

66
00:05:16,000 --> 00:05:20,380
 And people who practice meditation intensively, their lives

67
00:05:20,380 --> 00:05:21,000
 begin to change.

68
00:05:21,000 --> 00:05:27,000
 They stop getting caught up in so much complex activity

69
00:05:27,000 --> 00:05:31,000
 because they start to see it's not useful.

70
00:05:31,000 --> 00:05:34,000
 That's not the sampajanya we're talking about in the sutta,

71
00:05:34,000 --> 00:05:40,000
 but that's a way that it's used. It's a type of wisdom.

72
00:05:40,000 --> 00:05:47,390
 The second type is sappaya sampajanya, wisdom about suit

73
00:05:47,390 --> 00:05:49,000
ability.

74
00:05:49,000 --> 00:05:51,000
 So suitability is a bit different than purpose.

75
00:05:51,000 --> 00:05:56,530
 It means something might be useful, but is it useful for me

76
00:05:56,530 --> 00:05:57,000
?

77
00:05:57,000 --> 00:06:01,310
 This is important. There are some things that are just

78
00:06:01,310 --> 00:06:02,000
 useless.

79
00:06:02,000 --> 00:06:06,000
 It means no one should engage in them, greed, anger,

80
00:06:06,000 --> 00:06:07,000
 delusion,

81
00:06:07,000 --> 00:06:10,000
 to name the basic ones.

82
00:06:10,000 --> 00:06:15,430
 But there are some things that are just unsuitable for an

83
00:06:15,430 --> 00:06:17,000
 individual.

84
00:06:17,000 --> 00:06:21,530
 Some things are suitable for all individuals. Sattī is one

85
00:06:21,530 --> 00:06:22,000
.

86
00:06:22,000 --> 00:06:26,700
 Practice of mindfulness. You can never say, "Well, I don't

87
00:06:26,700 --> 00:06:28,000
 think this is suitable."

88
00:06:28,000 --> 00:06:31,990
 But for some people, different environments are more

89
00:06:31,990 --> 00:06:33,000
 suitable.

90
00:06:33,000 --> 00:06:36,150
 The vīsuddhi-māga goes into some fairly practical

91
00:06:36,150 --> 00:06:37,000
 examples.

92
00:06:37,000 --> 00:06:40,000
 For someone who is of greedy temperament,

93
00:06:40,000 --> 00:06:43,640
 you should put them in a dirty room or a plain and simple

94
00:06:43,640 --> 00:06:44,000
 room

95
00:06:44,000 --> 00:06:50,220
 with a broken bed and give them a broken bowl and give them

96
00:06:50,220 --> 00:06:52,000
 crappy food.

97
00:06:52,000 --> 00:06:56,000
 Someone who is greedy should have all sorts of bad stuff.

98
00:06:56,000 --> 00:06:59,000
 But if someone is of angry temperament,

99
00:06:59,000 --> 00:07:02,000
 you should give them the nicest room and good food

100
00:07:02,000 --> 00:07:09,000
 and a pleasant view maybe.

101
00:07:09,000 --> 00:07:14,480
 Give them all sorts of nice stuff to counter their inherent

102
00:07:14,480 --> 00:07:16,000
 negativity.

103
00:07:16,000 --> 00:07:19,980
 For someone who is deluded, I think you shouldn't give them

104
00:07:19,980 --> 00:07:21,000
 a big room

105
00:07:21,000 --> 00:07:24,000
 because their mind will be able to expand.

106
00:07:24,000 --> 00:07:26,000
 You shouldn't give them a view.

107
00:07:26,000 --> 00:07:30,000
 You should give them a small room or something like that.

108
00:07:30,000 --> 00:07:33,000
 There's a whole detail. It's quite interesting.

109
00:07:33,000 --> 00:07:39,000
 There are ways if you really want to get into it.

110
00:07:39,000 --> 00:07:51,000
 The commentary here gives an example of a person who is...

111
00:07:51,000 --> 00:08:02,000
 a person who is contemplating a dead corpse.

112
00:08:02,000 --> 00:08:06,000
 If you're a male, you shouldn't contemplate a female corpse

113
00:08:06,000 --> 00:08:08,000
 because it's not the point.

114
00:08:08,000 --> 00:08:12,330
 It goes against the purpose of seeing the loathsome-ness of

115
00:08:12,330 --> 00:08:13,000
 the body.

116
00:08:13,000 --> 00:08:18,020
 Arguably. But arguably it might help if you see a woman

117
00:08:18,020 --> 00:08:20,000
 become bloated and disgusting.

118
00:08:20,000 --> 00:08:23,000
 The Buddha used this.

119
00:08:23,000 --> 00:08:26,000
 No, he didn't. He used it for a woman.

120
00:08:26,000 --> 00:08:31,460
 The commentary does say a man should not reflect upon a

121
00:08:31,460 --> 00:08:33,000
 female corpse

122
00:08:33,000 --> 00:08:37,000
 and a woman shouldn't contemplate on a male corpse.

123
00:08:37,000 --> 00:08:39,000
 That would be unsuitable.

124
00:08:39,000 --> 00:08:42,000
 For loving kindness, it certainly works that way.

125
00:08:42,000 --> 00:08:45,000
 Men should do much better.

126
00:08:45,000 --> 00:08:50,000
 If they're cultivating the jhanas, they should focus on men

127
00:08:50,000 --> 00:08:52,000
, male individuals.

128
00:08:52,000 --> 00:08:54,000
 Focus their kindness on them.

129
00:08:54,000 --> 00:08:57,000
 Otherwise, if they're heterosexual, obviously,

130
00:08:57,000 --> 00:09:00,000
 the point is don't focus on something that's going to be

131
00:09:00,000 --> 00:09:03,000
 the object of your attachment,

132
00:09:03,000 --> 00:09:08,000
 depending on what is your proclivity.

133
00:09:08,000 --> 00:09:13,000
 The third type of sampajanya is gochara sampajanya.

134
00:09:13,000 --> 00:09:16,000
 Gochara means pasture.

135
00:09:16,000 --> 00:09:18,000
 Go means a cow.

136
00:09:18,000 --> 00:09:26,000
 And jhara means walking or place of pasture, really.

137
00:09:26,000 --> 00:09:29,000
 So it's cow pasture, gochara.

138
00:09:29,000 --> 00:09:32,420
 Cows having a great significance in India, the word took on

139
00:09:32,420 --> 00:09:34,000
 greater meaning,

140
00:09:34,000 --> 00:09:37,000
 and it means resort.

141
00:09:37,000 --> 00:09:41,770
 So knowledge of the sort of people you should hang out with

142
00:09:41,770 --> 00:09:42,000
,

143
00:09:42,000 --> 00:09:47,940
 the sort of places you should go, the sort of wisdom about

144
00:09:47,940 --> 00:09:49,000
 this.

145
00:09:49,000 --> 00:09:51,550
 The fourth type of sampajanya is the one we're looking at

146
00:09:51,550 --> 00:09:52,000
 here.

147
00:09:52,000 --> 00:09:55,360
 The commentary acknowledges that none of these are the type

148
00:09:55,360 --> 00:09:56,000
 of sampajanya

149
00:09:56,000 --> 00:09:59,000
 that we're talking about here.

150
00:09:59,000 --> 00:10:01,000
 In this section, we're going to talk about sampajanya,

151
00:10:01,000 --> 00:10:06,000
 which is called asamuha sampajanya.

152
00:10:06,000 --> 00:10:11,000
 Samuha means that which partakes in wisdom.

153
00:10:11,000 --> 00:10:15,000
 Asamuha means that which does not partake in...

154
00:10:15,000 --> 00:10:19,000
 Sorry, asamuha is that which partakes in delusion.

155
00:10:19,000 --> 00:10:23,000
 Asamuha is that which does not partake of delusion or in

156
00:10:23,000 --> 00:10:25,000
 delusion,

157
00:10:25,000 --> 00:10:28,000
 which means wisdom.

158
00:10:28,000 --> 00:10:34,000
 The full and complete knowledge that has to do with wisdom

159
00:10:34,000 --> 00:10:43,000
 or has to do with non-ignorance, non-delusion, non-conf

160
00:10:43,000 --> 00:10:44,000
usion.

161
00:10:44,000 --> 00:10:47,000
 And this particular...

162
00:10:47,000 --> 00:10:51,770
 The usage here means specifically this isn't a conventional

163
00:10:51,770 --> 00:10:53,000
 sort of wisdom.

164
00:10:53,000 --> 00:10:55,000
 The other three are conventional.

165
00:10:55,000 --> 00:10:59,000
 I mean, they come generally from meditation practice,

166
00:10:59,000 --> 00:11:04,570
 but they can also come from theory, from rationalization or

167
00:11:04,570 --> 00:11:06,000
 logic,

168
00:11:06,000 --> 00:11:09,000
 rational thinking or logic.

169
00:11:09,000 --> 00:11:10,000
 But this one is asamuha.

170
00:11:10,000 --> 00:11:15,000
 This means in a true, full, complete knowledge.

171
00:11:15,000 --> 00:11:21,000
 This is where one comes to know unshakably, directly,

172
00:11:21,000 --> 00:11:31,000
 without any intermediary from one's meditation practice.

173
00:11:31,000 --> 00:11:34,350
 So what we're talking about here is this experience of the

174
00:11:34,350 --> 00:11:35,000
 meditator

175
00:11:35,000 --> 00:11:37,000
 who knows what's happening,

176
00:11:37,000 --> 00:11:41,170
 where suddenly they're transported from the mental

177
00:11:41,170 --> 00:11:43,000
 intellectual activity

178
00:11:43,000 --> 00:11:46,440
 to the awareness, "Now I'm sitting, now I'm walking, now I

179
00:11:46,440 --> 00:11:48,000
'm standing."

180
00:11:48,000 --> 00:11:54,000
 Not intellectual thinking, but it's the best approximation

181
00:11:54,000 --> 00:11:56,000
 that we can put into words.

182
00:11:56,000 --> 00:12:01,000
 It's this knowing and full and complete knowledge,

183
00:12:01,000 --> 00:12:03,000
 not just some wavering knowledge that everyone has,

184
00:12:03,000 --> 00:12:06,000
 "Oh yes, I'm going to walk now."

185
00:12:06,000 --> 00:12:11,000
 No walking, being aware of the movement.

186
00:12:11,000 --> 00:12:12,000
 And so it's quite simple.

187
00:12:12,000 --> 00:12:14,000
 It goes through just about everything,

188
00:12:14,000 --> 00:12:18,000
 but it's interesting to remind ourselves that this isn't

189
00:12:18,000 --> 00:12:18,000
 just a practice

190
00:12:18,000 --> 00:12:21,000
 for the sitting man.

191
00:12:21,000 --> 00:12:25,640
 Sati is something that, for the meditation to progress and

192
00:12:25,640 --> 00:12:27,000
 succeed,

193
00:12:27,000 --> 00:12:32,000
 has to be cultivated everywhere.

194
00:12:32,000 --> 00:12:36,000
 So he says, "Bhikkhu, abhikkhu,"

195
00:12:36,000 --> 00:12:44,440
 it's our bhikkhu, "abhikkante patikante sampajanakari hoti

196
00:12:44,440 --> 00:12:45,000
."

197
00:12:45,000 --> 00:12:50,530
 And going forward and coming back and walking back and

198
00:12:50,530 --> 00:12:52,000
 forth,

199
00:12:52,000 --> 00:12:54,000
 sampajanakari hoti.

200
00:12:54,000 --> 00:13:01,000
 One is a person who does or cultivates or makes,

201
00:13:01,000 --> 00:13:04,000
 makes themselves fully and completely aware.

202
00:13:04,000 --> 00:13:06,760
 My commentary describes this, I think, as going on alms

203
00:13:06,760 --> 00:13:07,000
round,

204
00:13:07,000 --> 00:13:11,230
 and it goes through a long process of talking about going

205
00:13:11,230 --> 00:13:12,000
 and coming back.

206
00:13:12,000 --> 00:13:17,000
 It can also just be walking on a walking path

207
00:13:17,000 --> 00:13:23,000
 when you're walking forward and back.

208
00:13:23,000 --> 00:13:26,000
 But the idea here is outside of the meditation practice.

209
00:13:26,000 --> 00:13:30,160
 So when you leave your room to come up here to listen to

210
00:13:30,160 --> 00:13:31,000
 the talk,

211
00:13:31,000 --> 00:13:34,000
 when you go back to your room, that kind of thing,

212
00:13:34,000 --> 00:13:38,000
 when you go out for food, when you go back for food,

213
00:13:38,000 --> 00:13:42,000
 walking this way and that way is maybe what it means.

214
00:13:42,000 --> 00:13:47,180
 Alokite vilokite, when looking forward, when turning to

215
00:13:47,180 --> 00:13:48,000
 look around,

216
00:13:48,000 --> 00:13:50,000
 one is mindful.

217
00:13:50,000 --> 00:13:54,000
 Saminjite passarite, when one extends one's arm,

218
00:13:54,000 --> 00:13:58,000
 when one flexes one's arm, for example,

219
00:13:58,000 --> 00:14:07,000
 reaching, pulling.

220
00:14:07,000 --> 00:14:14,000
 Sanghati, pata, jiwara, dharani, sampajanakari hoti.

221
00:14:14,000 --> 00:14:17,000
 When one is carrying one's coat,

222
00:14:17,000 --> 00:14:20,000
 Sanghati is one, the monk's coat,

223
00:14:20,000 --> 00:14:26,000
 pata, bowl, jiwara, other robes,

224
00:14:26,000 --> 00:14:29,000
 carrying, when one is carrying something,

225
00:14:29,000 --> 00:14:33,000
 carrying one's bowl and robes, really.

226
00:14:33,000 --> 00:14:35,000
 For monks, those are the belongings.

227
00:14:35,000 --> 00:14:37,670
 So when you go to pick up your robes, picking it up mind

228
00:14:37,670 --> 00:14:38,000
fully,

229
00:14:38,000 --> 00:14:41,000
 these things that we have to do on a daily basis,

230
00:14:41,000 --> 00:14:45,000
 doing it with mindfulness, with clear awareness.

231
00:14:45,000 --> 00:14:49,000
 So your mind isn't somewhere else, your mind is there too.

232
00:14:49,000 --> 00:14:53,530
 When you're carrying something, carrying your bowl on alms

233
00:14:53,530 --> 00:14:54,000
 round,

234
00:14:54,000 --> 00:14:58,000
 you are there with it. The mind is there too.

235
00:14:58,000 --> 00:15:02,320
 It's not the body carrying and the mind is doing something

236
00:15:02,320 --> 00:15:03,000
 else.

237
00:15:03,000 --> 00:15:07,000
 Asi te pite kai te sai te,

238
00:15:07,000 --> 00:15:10,000
 when one,

239
00:15:10,000 --> 00:15:17,000
 in regards to what is eaten and drunk,

240
00:15:17,000 --> 00:15:26,000
 swallowed, tasted, sampajanakari hoti,

241
00:15:26,000 --> 00:15:30,000
 when eating and drinking.

242
00:15:30,000 --> 00:15:33,000
 Eating meditation is a wonderful thing.

243
00:15:33,000 --> 00:15:37,000
 Encourage it for all meditators and people interested in

244
00:15:37,000 --> 00:15:39,000
 mindfulness meditation.

245
00:15:39,000 --> 00:15:43,520
 Chewing, chewing, swallowing, oh you've got nothing better

246
00:15:43,520 --> 00:15:44,000
 to do.

247
00:15:44,000 --> 00:15:47,000
 Why don't we actually be there when we eat?

248
00:15:47,000 --> 00:15:49,000
 Instead of eating and letting our minds wander

249
00:15:49,000 --> 00:15:53,000
 and get preoccupied just with the pleasure of eating.

250
00:15:53,000 --> 00:15:58,000
 Why don't we actually be there, chewing, chewing?

251
00:15:58,000 --> 00:16:04,000
 It's quite an enlightening experience.

252
00:16:04,000 --> 00:16:07,000
 And here's my favorite one I think is,

253
00:16:07,000 --> 00:16:12,000
 uchara pasava kame sampajanakari hoti.

254
00:16:12,000 --> 00:16:16,000
 Favorite, I mean, it's the most interesting one.

255
00:16:16,000 --> 00:16:23,000
 Uchara is defecating, pasava is urinating.

256
00:16:23,000 --> 00:16:28,000
 In the act of urinating and defecating, sampajanakari hoti.

257
00:16:28,000 --> 00:16:30,000
 I mean it shouldn't be any surprise,

258
00:16:30,000 --> 00:16:37,000
 but that it's in the text is quite revealing.

259
00:16:37,000 --> 00:16:44,000
 The urinating meditation, defecating meditation.

260
00:16:44,000 --> 00:16:46,000
 We actually go there.

261
00:16:46,000 --> 00:16:50,000
 When we say everywhere, we mean everywhere.

262
00:16:50,000 --> 00:16:53,000
 Being mindful of the feelings of urinating and defecating,

263
00:16:53,000 --> 00:16:58,000
 mindful of the sounds and the smells.

264
00:16:58,000 --> 00:17:07,720
 bhāsītītī nisine sūtī jāgarītī bhāsītī duni bh

265
00:17:07,720 --> 00:17:08,000
āve

266
00:17:08,000 --> 00:17:15,710
 When walking, when standing, when sitting, when lying, when

267
00:17:15,710 --> 00:17:18,000
 awake,

268
00:17:18,000 --> 00:17:23,000
 or when waking up,

269
00:17:23,000 --> 00:17:27,000
 when speaking, when staying silent.

270
00:17:27,000 --> 00:17:29,000
 When speaking, bhāsītī.

271
00:17:29,000 --> 00:17:32,000
 Are you aware of your lips moving?

272
00:17:32,000 --> 00:17:35,460
 Are you mindful of the thoughts and emotions going through

273
00:17:35,460 --> 00:17:38,000
 your mind when you talk?

274
00:17:38,000 --> 00:17:39,000
 It's a difficult one.

275
00:17:39,000 --> 00:17:44,000
 But are you here? Are you present?

276
00:17:44,000 --> 00:17:48,000
 If not, when you're not, you're not meditating.

277
00:17:48,000 --> 00:17:53,390
 It's quite revealing to think this isn't a one hour a day

278
00:17:53,390 --> 00:17:54,000
 thing.

279
00:17:54,000 --> 00:18:01,000
 It's a person who is dedicated to the practice.

280
00:18:01,000 --> 00:18:09,000
 It's a 18 hour thing at least, if not more.

281
00:18:09,000 --> 00:18:11,950
 I remember I tell this story often of subbing in for

282
00:18:11,950 --> 00:18:13,000
 another teacher.

283
00:18:13,000 --> 00:18:16,000
 They had the meditators do 8 hours a day, 10 hours a day,

284
00:18:16,000 --> 00:18:19,450
 and the meditators would come back so stressed, some of

285
00:18:19,450 --> 00:18:20,000
 them.

286
00:18:20,000 --> 00:18:24,410
 "Oh, I was able to do, today I did 10 hours a day. It was

287
00:18:24,410 --> 00:18:26,000
 hard, but I did it."

288
00:18:26,000 --> 00:18:28,000
 And this meditator said this to me and I said,

289
00:18:28,000 --> 00:18:33,450
 "Okay, today I want you to do 18 hours a day of meditation

290
00:18:33,450 --> 00:18:34,000
."

291
00:18:34,000 --> 00:18:38,800
 And his eyes almost, I remember his eyes just, he was unf

292
00:18:38,800 --> 00:18:40,000
athomable.

293
00:18:40,000 --> 00:18:42,920
 He was dreading what I was going to tell him and said, "18

294
00:18:42,920 --> 00:18:44,000
 hours.

295
00:18:44,000 --> 00:18:49,000
 Who is this new guy and what's he going to do?"

296
00:18:49,000 --> 00:18:51,000
 And explained to him, you know,

297
00:18:51,000 --> 00:18:54,530
 "I don't care how much formal meditation you do, but try to

298
00:18:54,530 --> 00:18:57,000
 be mindful when you wake up.

299
00:18:57,000 --> 00:19:01,000
 This isn't an hours thing. This is an always thing.

300
00:19:01,000 --> 00:19:06,000
 Be mindful, jagarite when waking up, lying,

301
00:19:06,000 --> 00:19:09,000
 when you want to sit up wanting to sit, sitting, sitting."

302
00:19:09,000 --> 00:19:13,000
 And I described to him how to be mindful standing, walking,

303
00:19:13,000 --> 00:19:14,000
 sitting,

304
00:19:14,000 --> 00:19:19,000
 standing, standing, stretching, reaching, pulling, brushing

305
00:19:19,000 --> 00:19:19,000
 your teeth,

306
00:19:19,000 --> 00:19:26,000
 eating your food, showering, urinating, defecating, being

307
00:19:26,000 --> 00:19:32,000
 mindful.

308
00:19:32,000 --> 00:19:36,000
 It's an all the time thing.

309
00:19:36,000 --> 00:19:41,200
 Which is really a, I mean this is a good example of why the

310
00:19:41,200 --> 00:19:43,000
 monastic life is so important.

311
00:19:43,000 --> 00:19:50,330
 Not for everyone, but important to have as a means of ded

312
00:19:50,330 --> 00:19:53,000
icating yourself in this way.

313
00:19:53,000 --> 00:19:58,000
 When you really want to learn how to live, right?

314
00:19:58,000 --> 00:20:01,440
 You don't want to go out and live, you want to learn how to

315
00:20:01,440 --> 00:20:02,000
 live.

316
00:20:02,000 --> 00:20:03,000
 Mostly we do it the other way.

317
00:20:03,000 --> 00:20:05,420
 We go out and live and we think we'll learn how to do it

318
00:20:05,420 --> 00:20:06,000
 along the way,

319
00:20:06,000 --> 00:20:10,010
 which, well, doesn't work out usually the way we planned

320
00:20:10,010 --> 00:20:12,000
 because you don't know how.

321
00:20:12,000 --> 00:20:17,000
 You're doing it all wrong and you're building bad habits.

322
00:20:17,000 --> 00:20:21,990
 So going off into the forest or an empty place, a monastery

323
00:20:21,990 --> 00:20:24,000
, for lack of a better word,

324
00:20:24,000 --> 00:20:27,680
 a place where there are a bunch more, a bunch of other begg

325
00:20:27,680 --> 00:20:29,000
ars who don't beg,

326
00:20:29,000 --> 00:20:34,230
 but see the danger of getting ambitious and caught up in s

327
00:20:34,230 --> 00:20:35,000
amsara

328
00:20:35,000 --> 00:20:40,000
 and dwelling with them and learning how to live.

329
00:20:40,000 --> 00:20:45,000
 This is what the monastic life is for.

330
00:20:45,000 --> 00:20:50,000
 And that's the section on sampanjanya.

331
00:20:50,000 --> 00:20:58,000
 So getting right along, that's the Dhamma for tonight.

332
00:20:58,000 --> 00:21:01,000
 Let's look at questions.

333
00:21:01,000 --> 00:21:06,000
 Six questions today.

334
00:21:06,000 --> 00:21:09,000
 How do you be more mindful during day-to-day activities?

335
00:21:09,000 --> 00:21:13,020
 I seem to often forget my reasons for things fail to be

336
00:21:13,020 --> 00:21:14,000
 present.

337
00:21:14,000 --> 00:21:17,990
 Well, the best way is to do an intensive meditation course

338
00:21:17,990 --> 00:21:18,000
 in all ways

339
00:21:18,000 --> 00:21:21,620
 because it, again, teaches you how to live rather than

340
00:21:21,620 --> 00:21:23,000
 having to go out and live

341
00:21:23,000 --> 00:21:25,000
 and try to work it out.

342
00:21:25,000 --> 00:21:28,000
 When you're here, you don't have anything better to do.

343
00:21:28,000 --> 00:21:36,000
 So being mindful is an easy thing to cultivate.

344
00:21:36,000 --> 00:21:40,000
 But failing that, failing at doing an intensive course,

345
00:21:40,000 --> 00:21:43,000
 well, again, very much as I just said,

346
00:21:43,000 --> 00:21:48,000
 you learn how to be mindful of just about everything you do

347
00:21:48,000 --> 00:21:48,000
.

348
00:21:48,000 --> 00:21:55,160
 Saminjite, passarite, stretching, flexing, everything you

349
00:21:55,160 --> 00:21:56,000
 do.

350
00:21:56,000 --> 00:21:58,000
 Does insight lead to tranquil states?

351
00:21:58,000 --> 00:22:05,000
 Yes, insight leads to the ultimate tranquility of nirvana.

352
00:22:05,000 --> 00:22:11,000
 Which, of course, leads to mundane tranquil states as well.

353
00:22:11,000 --> 00:22:13,000
 What is the meaning of knowing mind?

354
00:22:13,000 --> 00:22:15,320
 Do we have to mentally recite rising and falling, or is it

355
00:22:15,320 --> 00:22:16,000
 just to be aware?

356
00:22:16,000 --> 00:22:19,410
 Well, the problem is if you're not reciting rising and

357
00:22:19,410 --> 00:22:20,000
 falling,

358
00:22:20,000 --> 00:22:24,410
 it's very hard to just be aware, right, without that

359
00:22:24,410 --> 00:22:29,000
 practice of grasping,

360
00:22:29,000 --> 00:22:37,000
 concretely augmenting the mind.

361
00:22:37,000 --> 00:22:39,000
 It's very hard to just be aware.

362
00:22:39,000 --> 00:22:41,000
 That's the whole point.

363
00:22:41,000 --> 00:22:43,000
 People go the other way and say,

364
00:22:43,000 --> 00:22:46,300
 "We don't have to use these words. We're just going to be

365
00:22:46,300 --> 00:22:47,000
 aware."

366
00:22:47,000 --> 00:22:55,190
 Well, if the mantra isn't present, that's what the mantra

367
00:22:55,190 --> 00:22:57,000
 is for.

368
00:22:57,000 --> 00:22:59,000
 What is a Buddhist point of view on love?

369
00:22:59,000 --> 00:23:02,000
 There are many kinds of love.

370
00:23:02,000 --> 00:23:05,110
 It's really just a word, and it means different things to

371
00:23:05,110 --> 00:23:07,000
 different people in different contexts.

372
00:23:07,000 --> 00:23:09,120
 What does Buddhism say about love of another in a

373
00:23:09,120 --> 00:23:10,000
 relationship?

374
00:23:10,000 --> 00:23:12,000
 Is it wrong because of clinging?

375
00:23:12,000 --> 00:23:15,510
 Well, the love isn't wrong, but the clinging is technically

376
00:23:15,510 --> 00:23:16,000
 wrong

377
00:23:16,000 --> 00:23:18,000
 because the clinging is what's going to lead to suffering.

378
00:23:18,000 --> 00:23:21,000
 The love won't.

379
00:23:21,000 --> 00:23:23,000
 Don't they love everyone equally?

380
00:23:23,000 --> 00:23:27,060
 Loving everyone equally means not preferring one person

381
00:23:27,060 --> 00:23:28,000
 over another

382
00:23:28,000 --> 00:23:33,080
 and thus not being upset when you can't be with the one you

383
00:23:33,080 --> 00:23:35,000
 are attached to.

384
00:23:35,000 --> 00:23:38,000
 Again, love and attachment, from a Buddhist point of view,

385
00:23:38,000 --> 00:23:42,000
 are two very different things.

386
00:23:42,000 --> 00:23:45,350
 As a byproduct, does vipassana make you a more creative

387
00:23:45,350 --> 00:23:48,000
 person, not a smart person?

388
00:23:48,000 --> 00:23:53,170
 In the short term, maybe if creativity is a part of your

389
00:23:53,170 --> 00:23:54,000
 ambition,

390
00:23:54,000 --> 00:23:57,000
 it will definitely allow you to focus your creativity.

391
00:23:57,000 --> 00:24:00,960
 Eventually, I would say it makes you very uncreative

392
00:24:00,960 --> 00:24:03,000
 because you have no desire to create anymore.

393
00:24:03,000 --> 00:24:05,000
 But that's more long-term.

394
00:24:05,000 --> 00:24:12,490
 Eventually, you have no desire for anything. You're just

395
00:24:12,490 --> 00:24:17,000
 happy and at peace.

396
00:24:17,000 --> 00:24:20,420
 But if you've been creative by being able to see the world

397
00:24:20,420 --> 00:24:22,000
 completely differently,

398
00:24:22,000 --> 00:24:27,000
 yeah, absolutely. They're very open-minded to some extent.

399
00:24:27,000 --> 00:24:35,000
 To some extent, they're not interested in entertaining,

400
00:24:35,000 --> 00:24:40,000
 specious ideas or imaginary situations.

401
00:24:40,000 --> 00:24:43,000
 But to the extent that they're willing to, they're

402
00:24:43,000 --> 00:24:44,000
 perfectly open-minded,

403
00:24:44,000 --> 00:24:49,000
 able to imagine things.

404
00:24:49,000 --> 00:24:52,240
 Shopanhauer says, "Besides becoming a monk, you can

405
00:24:52,240 --> 00:24:55,000
 experience bouts of escape from the will to life

406
00:24:55,000 --> 00:24:58,660
 through immersing yourself in art and philosophy. What do

407
00:24:58,660 --> 00:25:00,000
 you think?"

408
00:25:00,000 --> 00:25:04,000
 Again, I don't think much about Shopanhauer.

409
00:25:04,000 --> 00:25:08,150
 I'm going to take the easy way out and just say, "Yeah,

410
00:25:08,150 --> 00:25:13,000
 that's not my sort of question."

411
00:25:13,000 --> 00:25:16,530
 Okay, that's all the questions. Thank you all for coming

412
00:25:16,530 --> 00:25:19,000
 out. Have a good night.

413
00:25:19,000 --> 00:25:20,000
 [

