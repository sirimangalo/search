1
00:00:00,000 --> 00:00:07,000
 Okay, good evening everyone.

2
00:00:07,000 --> 00:00:26,000
 Welcome to our evening Dhamma.

3
00:00:26,000 --> 00:00:29,360
 Thank you all for coming out. It's always great to see such

4
00:00:29,360 --> 00:00:30,000
 a turnout.

5
00:00:30,000 --> 00:00:35,710
 Locally, everyone has come out of their rooms after a hard

6
00:00:35,710 --> 00:00:37,000
 day's work,

7
00:00:37,000 --> 00:00:41,000
 struggling with their own minds.

8
00:00:41,000 --> 00:00:47,990
 All of you in Second Life and on YouTube who come out to

9
00:00:47,990 --> 00:00:50,000
 listen.

10
00:00:50,000 --> 00:00:56,000
 I think a full house here in Second Life on YouTube.

11
00:00:56,000 --> 00:01:07,000
 My channel has surpassed 50,000 subscribers apparently.

12
00:01:07,000 --> 00:01:13,270
 I appreciate the effort people are taking to practice the

13
00:01:13,270 --> 00:01:15,000
 Buddha's teaching.

14
00:01:15,000 --> 00:01:22,070
 It's not easy. It's not easy. Sometimes it seems like

15
00:01:22,070 --> 00:01:26,000
 Buddhism is all about suffering.

16
00:01:26,000 --> 00:01:30,020
 Hey wait, didn't the Buddha talk about suffering? Wasn't

17
00:01:30,020 --> 00:01:36,000
 that something he'd like to talk about?

18
00:01:36,000 --> 00:01:46,410
 That's not very encouraging. I think a lot of people of

19
00:01:46,410 --> 00:01:49,000
 course look at Buddhism and are not satisfied with it.

20
00:01:49,000 --> 00:01:55,000
 On the surface, talking a lot about suffering.

21
00:01:55,000 --> 00:02:05,350
 And then when they begin to practice meditation, a lot of

22
00:02:05,350 --> 00:02:08,000
 suffering. What the heck, right?

23
00:02:08,000 --> 00:02:12,340
 This isn't what we want. What do we want? What does

24
00:02:12,340 --> 00:02:15,000
 everyone want?

25
00:02:15,000 --> 00:02:20,780
 We want this happiness. Everyone wants happiness. No one

26
00:02:20,780 --> 00:02:23,000
 wants suffering.

27
00:02:23,000 --> 00:02:26,280
 So to spend all our time talking about, thinking about

28
00:02:26,280 --> 00:02:33,000
 suffering, it sounds rather unpleasant.

29
00:02:33,000 --> 00:02:38,000
 It's a recipe for unhappiness.

30
00:02:38,000 --> 00:02:40,620
 A question might come up. Why didn't the Buddha talk more

31
00:02:40,620 --> 00:02:41,000
 about happiness?

32
00:02:41,000 --> 00:02:46,690
 If he was so enlightened and so happy, was the Buddha happy

33
00:02:46,690 --> 00:02:47,000
?

34
00:02:47,000 --> 00:02:52,000
 If so, why didn't he talk about it more?

35
00:02:52,000 --> 00:02:54,300
 So the suspicion that perhaps the Buddha wasn't happy and

36
00:02:54,300 --> 00:03:05,340
 perhaps he was just denying the existence or the potential

37
00:03:05,340 --> 00:03:09,000
 for happiness.

38
00:03:09,000 --> 00:03:12,000
 Why did he phrase it in a negative?

39
00:03:12,000 --> 00:03:16,830
 It's a question and it's led Buddhist teachers to try and

40
00:03:16,830 --> 00:03:21,000
 reframe the Four Noble Truths as happiness.

41
00:03:21,000 --> 00:03:25,800
 Happiness, the cause of happiness, the cessation of

42
00:03:25,800 --> 00:03:28,380
 happiness and the path which leads to the cessation of

43
00:03:28,380 --> 00:03:29,000
 happiness.

44
00:03:29,000 --> 00:03:36,000
 And so to explain it in exactly the opposite way.

45
00:03:36,000 --> 00:03:40,530
 Which is fine, but the question remains, why didn't the

46
00:03:40,530 --> 00:03:46,000
 Buddha do that? Why didn't the Buddha talk like that?

47
00:03:46,000 --> 00:03:52,370
 It comes down, I think, a big problem is the question of

48
00:03:52,370 --> 00:03:56,000
 course, what is happiness?

49
00:03:56,000 --> 00:04:05,000
 What do we mean when we say happiness?

50
00:04:05,000 --> 00:04:10,310
 I think we certainly don't mean physical pleasure, no? Is

51
00:04:10,310 --> 00:04:14,000
 that what we mean by happiness?

52
00:04:14,000 --> 00:04:20,000
 I think most of us have gotten beyond that.

53
00:04:20,000 --> 00:04:25,080
 As children we seek out the pleasure of ice cream, the

54
00:04:25,080 --> 00:04:31,540
 pleasure of bright colors, shiny objects, toys, games, tick

55
00:04:31,540 --> 00:04:37,000
ling, snuggling, hugging, kissing.

56
00:04:37,000 --> 00:04:44,390
 Adults actually fall into that as well, but sensual

57
00:04:44,390 --> 00:04:46,000
 happiness.

58
00:04:46,000 --> 00:04:51,420
 And I think for the most part adults come to see beyond

59
00:04:51,420 --> 00:04:52,000
 that.

60
00:04:52,000 --> 00:04:55,530
 Because as you grow up you realize it's actually really

61
00:04:55,530 --> 00:04:58,000
 just a recipe for disappointment.

62
00:04:58,000 --> 00:05:01,000
 Through addiction, right?

63
00:05:01,000 --> 00:05:07,440
 When children become addicted to pleasure, they're

64
00:05:07,440 --> 00:05:11,000
 ultimately disappointed.

65
00:05:11,000 --> 00:05:15,820
 When they can't get what they want, they throw a tantrum

66
00:05:15,820 --> 00:05:18,000
 and they suffer a lot.

67
00:05:18,000 --> 00:05:23,560
 And they start to see the, maybe not consciously, but

68
00:05:23,560 --> 00:05:29,000
 certainly through practice and through experience.

69
00:05:29,000 --> 00:05:34,070
 They see the nature of sensuality as being a mix, an

70
00:05:34,070 --> 00:05:38,000
 inevitable mix of pleasure and pain.

71
00:05:38,000 --> 00:05:42,000
 You can't really have one without the other.

72
00:05:42,000 --> 00:05:45,290
 Because being partial to something is having expectations

73
00:05:45,290 --> 00:05:46,000
 about it.

74
00:05:46,000 --> 00:05:48,000
 May it stay.

75
00:05:48,000 --> 00:05:52,500
 Of course nothing lasts forever and when things eventually

76
00:05:52,500 --> 00:05:57,280
 change there's disappointment, there's pain, there's stress

77
00:05:57,280 --> 00:06:01,000
, there's suffering.

78
00:06:01,000 --> 00:06:09,440
 So beyond that I think generally people will look at more

79
00:06:09,440 --> 00:06:12,000
 refined forms of happiness.

80
00:06:12,000 --> 00:06:15,760
 And even though it's very hard to give up the addiction to

81
00:06:15,760 --> 00:06:21,110
 sensual pleasure, we tend to see that it might be fun but

82
00:06:21,110 --> 00:06:22,000
 it can't be considered true happiness.

83
00:06:22,000 --> 00:06:30,300
 And so we look beyond that to things like Bhawasuka or we

84
00:06:30,300 --> 00:06:32,000
 Bhawasuka.

85
00:06:32,000 --> 00:06:36,100
 Kaamasuka, it's not the way but what about Bhawasuka and we

86
00:06:36,100 --> 00:06:40,000
 Bhawasuka, meaning what about states of being?

87
00:06:40,000 --> 00:06:43,000
 Look at us sitting in this room.

88
00:06:43,000 --> 00:06:46,000
 How happy we are. How wonderful is this?

89
00:06:46,000 --> 00:06:52,000
 We have a roof over our heads, good company, good food.

90
00:06:52,000 --> 00:06:58,000
 I don't know how good the food is but we have frozen food.

91
00:06:58,000 --> 00:07:03,000
 Warmth.

92
00:07:03,000 --> 00:07:08,880
 Our ability to sit here together in peace, not have to

93
00:07:08,880 --> 00:07:10,000
 fight.

94
00:07:10,000 --> 00:07:14,520
 And Bhawasuka, we Bhawasuka, we have states of being when

95
00:07:14,520 --> 00:07:17,000
 you get a new job for example.

96
00:07:17,000 --> 00:07:19,310
 It's funny because why would you celebrate that? It means

97
00:07:19,310 --> 00:07:21,000
 you have to go work.

98
00:07:21,000 --> 00:07:24,790
 But the idea of having a new job, the concept of it is a

99
00:07:24,790 --> 00:07:26,000
 great thing.

100
00:07:26,000 --> 00:07:29,570
 Everyone's congratulatory which has nothing to do with sens

101
00:07:29,570 --> 00:07:33,000
uality because sensually speaking it's a lot.

102
00:07:33,000 --> 00:07:37,000
 You're just suffering because you have to go to work.

103
00:07:37,000 --> 00:07:41,180
 But conceptually it's a great thing because it ultimately

104
00:07:41,180 --> 00:07:44,610
 allows you more sensual pleasure potentially or it allows

105
00:07:44,610 --> 00:07:46,000
 you freedom from pain.

106
00:07:46,000 --> 00:07:54,000
 So the concept of getting a job is a great happiness.

107
00:07:54,000 --> 00:07:59,000
 Having a family. Someone sent me a...

108
00:07:59,000 --> 00:08:05,210
 I don't really want to tease this person too much but they

109
00:08:05,210 --> 00:08:10,300
 sent me an email because they wanted to let me know of the

110
00:08:10,300 --> 00:08:14,000
 wonderful occasion of having a child.

111
00:08:14,000 --> 00:08:21,140
 And the way they expressed it was they wanted to share the

112
00:08:21,140 --> 00:08:23,000
 great moment with me.

113
00:08:23,000 --> 00:08:27,150
 And I thought to myself, well from a Buddhist perspective

114
00:08:27,150 --> 00:08:31,000
 it's not really something we congratulate people on.

115
00:08:31,000 --> 00:08:36,450
 When the Buddha left home he found out he was a father and

116
00:08:36,450 --> 00:08:43,000
 he said, "Rahu Lang Jatang, a fetter has been born."

117
00:08:43,000 --> 00:08:45,170
 And the king heard this and the king said, "Well then let

118
00:08:45,170 --> 00:08:46,000
 that be his name."

119
00:08:46,000 --> 00:08:48,990
 Kind of out of spite it seems but that's what he called it.

120
00:08:48,990 --> 00:08:55,250
 This son ended up being called "Rahu" which means a bond or

121
00:08:55,250 --> 00:08:56,000
 a bind.

122
00:08:56,000 --> 00:09:04,710
 Because before he left the Bodhisatta said, "Rahu Lang Jat

123
00:09:04,710 --> 00:09:06,000
ang."

124
00:09:06,000 --> 00:09:13,000
 But you know this is it. When things happen they please us.

125
00:09:13,000 --> 00:09:17,290
 This is what life is all about. The "Jua" the "Vivre" for

126
00:09:17,290 --> 00:09:18,000
 example.

127
00:09:18,000 --> 00:09:22,490
 Taking a walk in the forest. It's not really the sensuality

128
00:09:22,490 --> 00:09:25,000
 per se. It's the peace.

129
00:09:25,000 --> 00:09:27,560
 You might even say the "Wibhaba Tanna" and the "Wibhaba Su

130
00:09:27,560 --> 00:09:28,000
ka."

131
00:09:28,000 --> 00:09:31,490
 Not having to deal with all the stress of life. You go to

132
00:09:31,490 --> 00:09:35,490
 work, you go to school, you deal with your family and it's

133
00:09:35,490 --> 00:09:37,000
 all stress and suffering.

134
00:09:37,000 --> 00:09:45,330
 So you leave just the freedom from all of that stress. That

135
00:09:45,330 --> 00:09:50,000
's happiness.

136
00:09:50,000 --> 00:09:52,940
 There are other ways of looking at happiness. I know some

137
00:09:52,940 --> 00:09:59,410
 people are optimistic and they think optimism, positivity

138
00:09:59,410 --> 00:10:01,000
 is happiness.

139
00:10:01,000 --> 00:10:08,270
 So as long as you see the good in everything. It's an

140
00:10:08,270 --> 00:10:09,000
 example I think.

141
00:10:09,000 --> 00:10:13,000
 Pollyanna is a bit of an example of that.

142
00:10:13,000 --> 00:10:19,070
 But when I was, there was this movie "Life is Wonderful" I

143
00:10:19,070 --> 00:10:22,000
 think when I was in university a long time ago.

144
00:10:22,000 --> 00:10:27,000
 About the Holocaust and this Jewish man and his son.

145
00:10:27,000 --> 00:10:29,780
 He looked at everything positively and as a result good

146
00:10:29,780 --> 00:10:32,790
 things happened to him but eventually he was killed in a

147
00:10:32,790 --> 00:10:34,000
 concentration camp.

148
00:10:34,000 --> 00:10:42,000
 But up to the end he had a positive attitude.

149
00:10:42,000 --> 00:10:45,600
 So we look at this kind of thing and we say "Well that's it

150
00:10:45,600 --> 00:10:46,000
."

151
00:10:46,000 --> 00:10:50,120
 This is I think how neuroscientists tend to talk about

152
00:10:50,120 --> 00:10:51,000
 happiness.

153
00:10:51,000 --> 00:10:55,900
 That some people are more capable of being happy because of

154
00:10:55,900 --> 00:10:58,000
 their positive outlook.

155
00:10:58,000 --> 00:11:03,890
 So there was this one researcher who was talking about how

156
00:11:03,890 --> 00:11:09,000
 through meditation you can become more positive.

157
00:11:09,000 --> 00:11:16,000
 You can retrain your brain and your mind to be more happy.

158
00:11:16,000 --> 00:11:19,830
 As a result of being more positive and rebounding from

159
00:11:19,830 --> 00:11:23,000
 negative experiences quicker and so on.

160
00:11:23,000 --> 00:11:28,880
 I think that gets closer to Buddhist concept but of course

161
00:11:28,880 --> 00:11:33,000
 positivity has its problems as well.

162
00:11:33,000 --> 00:11:38,520
 It's less likely to lead to disappointment but it's still

163
00:11:38,520 --> 00:11:43,500
 likely to lead one to complacency in the sense that one

164
00:11:43,500 --> 00:11:44,000
 suffers.

165
00:11:44,000 --> 00:11:52,000
 But one doesn't take it seriously and one refuses to.

166
00:11:52,000 --> 00:11:55,000
 One doesn't engage in dwelling on it.

167
00:11:55,000 --> 00:11:58,000
 At the same time doesn't engage in fixing it.

168
00:11:58,000 --> 00:12:00,000
 Doesn't free oneself from it.

169
00:12:00,000 --> 00:12:02,280
 So it keeps coming to suffering. It does come back. It's

170
00:12:02,280 --> 00:12:04,000
 just it doesn't last long.

171
00:12:04,000 --> 00:12:13,000
 So it's arguably superior to the other sorts of happiness.

172
00:12:13,000 --> 00:12:18,620
 Moving on in Buddhism, the other type of happiness that's

173
00:12:18,620 --> 00:12:23,060
 sort of in a category of its own is the happiness of

174
00:12:23,060 --> 00:12:25,000
 meditation.

175
00:12:25,000 --> 00:12:28,860
 It could be together with this idea of positivity but it's

176
00:12:28,860 --> 00:12:30,000
 not exactly.

177
00:12:30,000 --> 00:12:34,000
 It's a change in mind state to be sure.

178
00:12:34,000 --> 00:12:40,200
 It's a change in mind state to the extent that one

179
00:12:40,200 --> 00:12:50,360
 experience constant and prolonged states that are pleas

180
00:12:50,360 --> 00:12:55,000
urable, are happy.

181
00:12:55,000 --> 00:12:59,340
 So like the first jhana, the first trance of tranquility

182
00:12:59,340 --> 00:13:03,000
 meditation is accompanied with happiness.

183
00:13:03,000 --> 00:13:08,000
 It's a kind of happiness that's removed from addiction.

184
00:13:08,000 --> 00:13:11,600
 You can't actually be addicted to the happiness because the

185
00:13:11,600 --> 00:13:14,000
 mind isn't cultivating addiction at that time.

186
00:13:14,000 --> 00:13:20,060
 There's no it's so focused or there's no room for clinging

187
00:13:20,060 --> 00:13:21,000
 to it.

188
00:13:21,000 --> 00:13:25,620
 It's so powerful that the mind doesn't develop the

189
00:13:25,620 --> 00:13:27,000
 addiction.

190
00:13:27,000 --> 00:13:32,000
 It's freeing. It's liberating in that sense.

191
00:13:32,000 --> 00:13:37,040
 And so often people who practice this will describe it as

192
00:13:37,040 --> 00:13:41,000
 being a liberation or being liberating.

193
00:13:41,000 --> 00:13:44,000
 It is possible to become attached to the idea of the jhanas

194
00:13:44,000 --> 00:13:49,000
 and when you leave them behind want them again.

195
00:13:49,000 --> 00:13:53,100
 And so ultimately the Buddha noted about this type of

196
00:13:53,100 --> 00:13:54,000
 happiness.

197
00:13:54,000 --> 00:13:57,960
 He said well that's the pinnacle really of any kind of

198
00:13:57,960 --> 00:14:01,000
 feeling but it's still impermanent.

199
00:14:01,000 --> 00:14:06,000
 When you leave it you can still be left wanting more.

200
00:14:06,000 --> 00:14:09,000
 You think about that and you compare it to what you have

201
00:14:09,000 --> 00:14:12,000
 when you're out at the jhana and you want the jhana back

202
00:14:12,000 --> 00:14:15,000
 but they're not permanent.

203
00:14:15,000 --> 00:14:18,620
 And ultimately this is the real problem with any of the

204
00:14:18,620 --> 00:14:23,000
 types of happiness, anything that we could call happiness

205
00:14:23,000 --> 00:14:34,000
 beyond the ultimate happiness which we're going to get to.

206
00:14:34,000 --> 00:14:39,110
 It doesn't last. You can say I've got the best life. This

207
00:14:39,110 --> 00:14:43,000
 is working for me.

208
00:14:43,000 --> 00:14:46,360
 People who have good jobs, good cards, even start at the

209
00:14:46,360 --> 00:14:47,000
 bottom.

210
00:14:47,000 --> 00:14:51,830
 People who have sensual height, the pinnacle of sensual

211
00:14:51,830 --> 00:14:53,000
 pleasure.

212
00:14:53,000 --> 00:15:00,000
 Angels for example, human beings who are rich, powerful.

213
00:15:00,000 --> 00:15:05,060
 The drug addicts even will think for a time that they are

214
00:15:05,060 --> 00:15:06,000
 in heaven.

215
00:15:06,000 --> 00:15:09,760
 But it doesn't last. When you have a good job, a good

216
00:15:09,760 --> 00:15:12,000
 status, security, you feel secure.

217
00:15:12,000 --> 00:15:16,420
 It doesn't last. People who have positive, even if you have

218
00:15:16,420 --> 00:15:19,990
 positive thoughts, if your mind, if your brain is wired

219
00:15:19,990 --> 00:15:25,360
 such that you are able to be positive about everything, to

220
00:15:25,360 --> 00:15:28,000
 be happy even in the face of great suffering.

221
00:15:28,000 --> 00:15:33,210
 Even that doesn't last. There's no reason to think that

222
00:15:33,210 --> 00:15:36,000
 that state is going to be permanent.

223
00:15:36,000 --> 00:15:49,000
 That positivity leads. That it's stable.

224
00:15:49,000 --> 00:15:56,710
 In fact, what we find through wisdom, meaning as we truly

225
00:15:56,710 --> 00:16:04,500
 understand the nature of all of this reality, happiness and

226
00:16:04,500 --> 00:16:06,000
 suffering,

227
00:16:06,000 --> 00:16:11,620
 is that freedom from suffering is far preferable to any of

228
00:16:11,620 --> 00:16:13,000
 these states.

229
00:16:13,000 --> 00:16:19,000
 So we have to talk about, technically, happiness as being a

230
00:16:19,000 --> 00:16:21,000
 negative thing.

231
00:16:21,000 --> 00:16:24,610
 Negative in the sense of being free from something as

232
00:16:24,610 --> 00:16:27,000
 opposed to gaining something.

233
00:16:27,000 --> 00:16:31,320
 But it's not being free from any one experience. It's not

234
00:16:31,320 --> 00:16:33,570
 like we Baba Soka where when something goes away you feel

235
00:16:33,570 --> 00:16:34,000
 happy.

236
00:16:34,000 --> 00:16:37,000
 "Oh good, it's gone. What a relief."

237
00:16:37,000 --> 00:16:40,370
 And that's a key point because people misunderstand that

238
00:16:40,370 --> 00:16:43,560
 freedom from suffering, the happiness is because, "Oh good,

239
00:16:43,560 --> 00:16:45,000
 I'm not suffering anymore."

240
00:16:45,000 --> 00:16:52,920
 But it's not. It's not a relief. It's not a feeling of

241
00:16:52,920 --> 00:16:54,000
 relief.

242
00:16:54,000 --> 00:16:58,890
 Because the things that caused you happiness and caused you

243
00:16:58,890 --> 00:17:02,000
 suffering before are still there.

244
00:17:02,000 --> 00:17:08,230
 The change is that you're no longer seeking them. You're no

245
00:17:08,230 --> 00:17:12,000
 longer disturbed by them.

246
00:17:12,000 --> 00:17:16,160
 You're no longer subject to their control. So there's the

247
00:17:16,160 --> 00:17:20,000
 happiness of freedom.

248
00:17:20,000 --> 00:17:25,100
 The happiness of being in a state or of gaining the

249
00:17:25,100 --> 00:17:31,670
 knowledge, the understanding that nothing is worth clinging

250
00:17:31,670 --> 00:17:32,000
 to.

251
00:17:32,000 --> 00:17:37,000
 Basically that there is nothing that can make you happy.

252
00:17:37,000 --> 00:17:40,080
 It's a kind of happiness that goes beyond any type of

253
00:17:40,080 --> 00:17:41,000
 happiness.

254
00:17:41,000 --> 00:17:49,440
 It is not dependent on anything. It's not subject to change

255
00:17:49,440 --> 00:17:50,000
.

256
00:17:50,000 --> 00:17:55,000
 It's not vulnerable. It's invincible.

257
00:17:55,000 --> 00:17:59,830
 And beyond that it is experienced as far preferable, far

258
00:17:59,830 --> 00:18:01,000
 superior.

259
00:18:01,000 --> 00:18:06,020
 Anyone who has attained a state of freedom no longer holds

260
00:18:06,020 --> 00:18:11,130
 any delusion about sensuality or becoming or even non-bec

261
00:18:11,130 --> 00:18:14,000
oming, even the jhanas.

262
00:18:14,000 --> 00:18:20,980
 The idea that they might be somehow ultimate or true

263
00:18:20,980 --> 00:18:23,000
 happiness.

264
00:18:23,000 --> 00:18:26,330
 And so this is the Buddha actually said quite clearly, is

265
00:18:26,330 --> 00:18:30,000
 that "Nati Santi Parang Sukham."

266
00:18:30,000 --> 00:18:33,870
 Which isn't a quote that we often bring up because it

267
00:18:33,870 --> 00:18:38,640
 sounds kind of contentious because it's hard to understand

268
00:18:38,640 --> 00:18:41,000
 this with all of our ideas of what is happiness.

269
00:18:41,000 --> 00:18:47,630
 Nati Santi Parang Sukham means there's no happiness outside

270
00:18:47,630 --> 00:18:49,000
 of peace.

271
00:18:49,000 --> 00:18:56,000
 Peace is the only sort of happiness.

272
00:18:56,000 --> 00:19:02,000
 Which is, it's sensible, it's reasonable.

273
00:19:02,000 --> 00:19:06,000
 But it's hard to understand because it's a negative.

274
00:19:06,000 --> 00:19:16,000
 Happiness, peace is a sublime state.

275
00:19:16,000 --> 00:19:21,000
 It's not really familiar to most people.

276
00:19:21,000 --> 00:19:26,000
 It's not really even all that desirable for a lot of people

277
00:19:26,000 --> 00:19:26,000
.

278
00:19:26,000 --> 00:19:29,540
 Except it made me an abstract sense, "Oh, I wish I had some

279
00:19:29,540 --> 00:19:32,000
 peace of mind," for example.

280
00:19:32,000 --> 00:19:45,000
 But peace is something quite sublime.

281
00:19:45,000 --> 00:19:50,600
 There was once asked of Sariputta, one of the monks, Yamaka

282
00:19:50,600 --> 00:19:53,000
, I think was his name.

283
00:19:53,000 --> 00:19:58,550
 He asked Sariputta, "If there's no Vedana, if there's no

284
00:19:58,550 --> 00:20:04,000
 Vedana in Nibbana, how can you say it's happiness?"

285
00:20:04,000 --> 00:20:06,760
 How can you say Nibbana is happiness when there's no Vedana

286
00:20:06,760 --> 00:20:07,000
?

287
00:20:07,000 --> 00:20:15,000
 It means feeling, so happiness is a type of feeling, right?

288
00:20:15,000 --> 00:20:15,000
 That's the idea of happy feelings.

289
00:20:15,000 --> 00:20:19,000
 Pleasant feelings, unpleasant feelings, neutral feelings.

290
00:20:19,000 --> 00:20:23,000
 If there's no feelings in Nibbana, if freedom has no

291
00:20:23,000 --> 00:20:27,000
 feelings, how can you say it's happiness?

292
00:20:27,000 --> 00:20:32,020
 I think this is a sticking point for a lot of people. There

293
00:20:32,020 --> 00:20:36,510
's a fear of the idea of Nibbana. I have to give up the

294
00:20:36,510 --> 00:20:37,000
 world.

295
00:20:37,000 --> 00:20:44,000
 But there's so much in the world that I want.

296
00:20:44,000 --> 00:20:46,320
 Actually, Nibbana isn't about giving up the world. It's

297
00:20:46,320 --> 00:20:49,000
 about leaving it behind.

298
00:20:49,000 --> 00:20:54,110
 Something that can be experienced for a short time. It's

299
00:20:54,110 --> 00:20:57,000
 not an all-or-nothing thing.

300
00:20:57,000 --> 00:21:00,990
 It's not like, "Oops, wait, let me back. I want to go back.

301
00:21:00,990 --> 00:21:05,000
 Let me off, let me off."

302
00:21:05,000 --> 00:21:09,280
 Something you experience and you're able to see. Is it

303
00:21:09,280 --> 00:21:12,000
 worth it? Is it not worth it?

304
00:21:12,000 --> 00:21:17,430
 Sariputta's reply was, "It's precisely because there is no

305
00:21:17,430 --> 00:21:21,000
 Vedana that Nibbana is happiness."

306
00:21:21,000 --> 00:21:26,020
 Sublime. Very difficult to understand unless you practice

307
00:21:26,020 --> 00:21:27,000
 meditation.

308
00:21:27,000 --> 00:21:30,530
 Of course, as you practice, you start to shed more and more

309
00:21:30,530 --> 00:21:34,000
 of your attachments, more and more of your desires.

310
00:21:34,000 --> 00:21:37,580
 As you see, the things that you thought were happiness are

311
00:21:37,580 --> 00:21:39,000
 not really happiness.

312
00:21:39,000 --> 00:21:42,690
 Until finally you truly let go. There's an epiphany and the

313
00:21:42,690 --> 00:21:45,000
 mind says, "Oh, yes, nothing's worth clinging to."

314
00:21:45,000 --> 00:21:49,000
 And then poof. A cessation.

315
00:21:49,000 --> 00:21:53,520
 Then you can answer this question whether Nibbana is

316
00:21:53,520 --> 00:21:55,000
 happiness or not.

317
00:21:55,000 --> 00:22:01,000
 When you experience it for yourself.

318
00:22:01,000 --> 00:22:05,070
 So, there you go. There's the Dhamma for this evening. I

319
00:22:05,070 --> 00:22:07,000
 hope the audio didn't cut out.

320
00:22:07,000 --> 00:22:14,040
 Thank you all here locally for sitting patiently, coming

321
00:22:14,040 --> 00:22:17,000
 out to listen.

322
00:22:17,000 --> 00:22:20,240
 And to everyone at home, wish you all good practice. And

323
00:22:20,240 --> 00:22:22,000
 thank you for tuning in.

