1
00:00:00,000 --> 00:00:03,520
 mind returning to nose or upper lip.

2
00:00:03,520 --> 00:00:07,600
 What do you do if the mind continuously returns to a more

3
00:00:07,600 --> 00:00:10,160
 familiar object of meditation,

4
00:00:10,160 --> 00:00:13,360
 for example the tip of the nose or upper lip?

5
00:00:13,360 --> 00:00:17,440
 Noting sensations caused by the breath at any parts of the

6
00:00:17,440 --> 00:00:19,120
 body is not wrong.

7
00:00:19,120 --> 00:00:24,160
 Wisdom can be cultivated based on any real experience.

8
00:00:24,160 --> 00:00:27,760
 The abdomen is favoured due to its gross nature

9
00:00:27,760 --> 00:00:31,090
 as opposed to the sensations that will care around the

10
00:00:31,090 --> 00:00:31,440
 mouth

11
00:00:31,440 --> 00:00:35,670
 which are more subtle and therefore favour tranquility

12
00:00:35,670 --> 00:00:36,880
 meditation.

13
00:00:36,880 --> 00:00:41,680
 As our tradition has developed specific techniques based on

14
00:00:41,680 --> 00:00:44,480
 starting with the abdomen as a base,

15
00:00:44,480 --> 00:00:48,910
 we ask meditators coming to our centre to use it as their

16
00:00:48,910 --> 00:00:50,560
 primary object.

17
00:00:50,560 --> 00:00:54,850
 If meditators are accustomed to using another object of

18
00:00:54,850 --> 00:00:55,840
 meditation,

19
00:00:55,840 --> 00:01:00,480
 they should note the experiences that arise based on past

20
00:01:00,480 --> 00:01:01,040
 habits

21
00:01:01,040 --> 00:01:04,400
 when the mind returns to the other object,

22
00:01:04,400 --> 00:01:09,870
 for example, hot, cold feeling of simply knowing that the

23
00:01:09,870 --> 00:01:12,720
 mind has returned to the nose or mouth.

24
00:01:12,720 --> 00:01:17,360
 Note the experience until the mind becomes disinterested

25
00:01:17,360 --> 00:01:22,690
 as per a point one should return to the abdomen and

26
00:01:22,690 --> 00:01:25,840
 continue noting as usual.

