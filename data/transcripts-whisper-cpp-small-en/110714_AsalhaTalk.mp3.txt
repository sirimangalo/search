 Now I'll give a short talk explaining what today means and
 a little bit about the purpose
 of this sort of ceremony. So our understanding is that 2600
 years ago, according to Sri Lankan
 Reckoning on this very day, 2600 years ago exactly, the
 Buddha first gave his first teaching.
 And for those of you who are not familiar with the story of
 the Buddha, I'll just give
 a little bit of background here. So the man who was to be
 the Buddha, we understand, many
 people know this much about the story, he was a prince. And
 at the age of 29 he got
 fed up with the meaninglessness of luxury and the life as a
 prince in a royal household.
 And so he decided to leave home in order to seek out some
 answers to these questions.
 Why we have to be born, why are we born, why do we get old,
 why do we get sick, why do
 we die, what's the point of it all, and how to overcome
 this, what to do, what is it that
 we have to do, what is the way to find freedom from
 suffering. And so he spent six years
 trying in vain to find an answer, a way out of suffering,
 to find a real answer to the
 problems in life. For his whole prince life it was the
 understanding that the way out
 of suffering is to find more and more pleasure to engage in
 pleasant experiences and find
 ways to avoid unpleasant experiences. And after 29 years of
 living this way, he realized
 that this wasn't sufficient. That as I've talked about
 before, you can only escape unpleasant
 situations for so long. Once you realize that there's no
 way to escape old age, sickness
 and death, then the answer that is seeking out sensual
 pleasure is not sufficient. It's
 like medicine that isn't strong enough for the sickness.
 There's no amount of pleasure
 which can take away the reality of old age, sickness and
 death. And in fact, in some ways
 it makes these things worse because it creates partiality
 and addiction and aversion. And
 so he realized that there was no amount of money or luxury,
 no amount of power or influence
 that could overcome these things, that could stop these
 things. You can't buy your way
 out of death and you can't survive. None of the luxury or
 power or influence that you
 have in this life survives with you after your death. You
 have to give it all up, 100%.
 So he spent six years trying to find a better answer. And
 it was fruitless because he didn't
 have a clear idea of where to look. The story goes, or the
 commentary to the story is that
 he had in a past life, because it took many, many lifetimes
 for him to develop his mind
 to the state where he was able to even come to these real
izations and leave home without
 a teacher, without anyone's guidance, to do it on his own,
 especially from such a wonderful,
 pleasurable lifestyle. It was something very difficult. But
 in one of his past lifetimes,
 some long, long time ago, he saw some, or he saw an
 enlightened being, someone who had
 done the same thing and come to realize the truth and come
 to find a way out of suffering.
 And he was an ignorant person at the time. And so he said
 about that person, he said,
 "Oh, they were torturing themselves." This person was tort
uring himself. So he said something
 nasty to this enlightened being. No useless, no good, good
 for nothing, doing something
 that's torturing yourself. And they say that's the reason,
 because he had set himself on this
 idea that a person who leaves home is torturing himself.
 When it finally came time for him
 to leave home, he had this wrong idea that torturing
 himself was the right way out of
 suffering. And in fact, this was a common belief at the
 time of the Buddha. This was
 the belief of people in India at the time that the way out
 of suffering was to burn up your
 attachments. So if you're attached to things that are
 pleasant, then you should teach yourself
 to be content with unpleasantness by creating unpleasant
 situations, making it difficult
 for yourself, creating suffering. Because this would offset
 the pleasure that you have, the
 attachment that you have to pleasure. And this is actually
 quite reasonable, and it's
 actually something that we do often. Even if we haven't
 left the home life, we feel
 guilty about our pleasures. So we torture ourselves. People
 who work very hard will often have
 this idea that they want this and want that, and they feel
 guilty about it. And so they
 think the right thing to do is to work very, very hard and
 make trouble for yourself, really.
 But they'll say how people who just indulge in pleasure are
 lazy and so on, and they'll
 develop this idea that if you want to be a good person, you
 have to work. You need a
 good work ethic, which is really pretty silly. It's
 something that is driving our world.
 There are two driving forces in the world. One is our
 incessant quest for pleasure, and
 the other is this idea that somehow we have to work,
 somehow we have to develop and advance.
 So they really go hand in hand. The idea that the best
 thing we can do is indulge, and the
 idea that the best thing we can do is develop or create.
 The best thing we can do is work.
 So we engage in all sorts of silly works to create things,
 and thinking that somehow this
 is a morally right thing. There was a famous philosopher in
 America, Henry David Thoreau,
 who was very critical of this idea, these people who work.
 He told a story of something
 about this man who had an ox pull a stone across the field
 and then later on pull the
 same stone back to the other side of the field. This is
 basically what we do, is we engage
 in useless works, and thinking that somehow it builds
 character to torture yourself. The
 Buddha did this intensively. This was the idea at the time
 that somehow it had spiritual
 benefit. It actually may have some limited spiritual
 benefit. The Buddha said it was
 useless, or not connected with what is right. But it
 actually may have the preliminary benefit
 of opening your mind up to more experiences. So it could
 very well bring patience and forbearance
 and the ability to tolerance. So it could be useful for
 meditation in a preliminary
 way. In this sense, we all have to go through this phase,
 because when we first come to
 meditate it can be torture. Having to sit still, having to
 sit cross-legged, having
 to eat only in the morning and so on can be quite difficult
, because our tolerance is
 quite low in the beginning. Our state of being is quite out
 of balance. Just to create balance,
 we often have to go through some suffering. Just the
 tension in the body, for example.
 Many people come to meditate, and just sitting still
 creates tension in the shoulders, tension
 in the back, because our minds are tense, our minds are in
 a state of stress. So in
 some sense we do have to go through a little bit of torture
 in the beginning, but we should
 never have the idea that somehow this is the way out of
 suffering. When some people come
 to meditate and they think the idea is to experience
 suffering, and they will scorn
 those people who meditate and find peace and happiness and
 bliss. On the other hand, some
 people come to meditate and think that it has to be a state
 of bliss and peace and happiness.
 And if you're not feeling calm and peaceful, then something
 is wrong with your meditation.
 Both of these are wrong understandings, and it leads back
 to the two extremes. So this
 is the context that we come into for the Buddha's first
 teaching. Two months prior, he had become
 enlightened, and he spent two months walking. That's not
 true. He spent, wait a second,
 let's get this right, May, June, July. So two months, and
 he spent 49 days reflecting
 on his enlightenment and living just in the bliss and peace
 and happiness of enlightenment.
 And then the last days, what is that? That's actually not
 many days. We have 49 days, which
 is 30, 19, so only 11 days or something. I hope I've got
 this right. Anyway, that he
 spent walking to find these five recklessness, because what
 happened was he had been torturing
 himself for six years, and then he realized it was useless.
 He realized that if he tortured
 himself any further, he would die. He couldn't go any
 further. He'd done everything. He'd
 stopped eating food. He'd even stopped breathing. He'd
 tortured himself in every way possible,
 and he said, "I can't go any farther with this, and yet
 still I have no wisdom and understanding."
 This is the key, because many people will practice
 meditation and gain this or gain
 that and never gain any wisdom or understanding about
 themselves or about reality. This is
 a wake-up call. It should be a wake-up call for all of us,
 for those of us who sit in
 meditation and find great peace or happiness, and also for
 those of us who sit in meditation
 and torture ourselves, that if we're not really gaining
 wisdom and understanding, then there's
 something wrong. The Buddha said, "Maybe there's another
 way." He tried to allow these peaceful
 and happy feelings to arise. He went and ate food and
 brought his body back to a state
 of normality, where it wasn't always groaning and
 complaining to him. Then he started practicing
 and allowed peaceful and happy feelings to come up and
 allowed the natural state of the
 mind to arise. It was quite peaceful and happy. He watched
 this, and he watched everything
 that came up, and he came to understand how attachment
 works. This is what I've been saying
 many times, is that if you want to understand attachment,
 you have to let the pleasure come
 up. You have to let the desire come up. If you just repress
 them all the time, then you're
 not going to be able to see the truth. What we want to
 understand is our addictions and
 how to overcome them. If we don't let them arise, then we
'll never be able to understand
 and overcome them. We'll just always be repressing them.
 Once we understand the nature of the
 object of our addiction and the nature of the addiction
 itself, then we'll be able to
 overcome it. We'll be able to give it up. This is the path
 to enlightenment. The last thing
 the Buddha realized was that everything that arises arises
 based on the cause. He saw the
 cause of suffering, and he saw the way out of suffering. He
 saw that suffering is only
 caused by craving and clinging. He saw how it works, that
 there was no good reason to
 cling to anything, that it was the clinging itself that was
 causing the suffering. When
 ordinarily we think that there are certain things that when
 we cling to them, they're
 going to bring us happiness. We think the way out of
 suffering is to just arrange things
 so that we only get those things that are worth clinging to
. The Buddha realized that
 it's the clinging itself that leads to suffering, nothing
 to do with the object. The point is
 that everyone else still thought that the way out of
 suffering was to torture yourself.
 These five followers of the Bodhisattva, the Buddha-to-be,
 became quite disenchanted with
 him and lost all their faith in him when he started taking
 food again and stopped torturing
 himself. They thought he had given up the path, given up
 the practice, and so they left. Now
 the Buddha went back to find them, and this is where he
 gave the first teaching. The first
 teaching started on this day. This is what this day
 represents today. It starts with
 these two extremes. Some people who read this don't quite
 understand with the context of
 this teaching, but this is the real context, the point that
 the Buddha had come to, the
 realization that these two extremes were useless. The fact
 that these five recklessness still
 thought that it was in terms of these two extremes, that
 one extreme was the right way,
 one extreme was the wrong way. The Buddha said both
 extremes are the wrong way. They
 thought he had gone back to the other extreme of indulgence
, and he said no, actually both
 of these. It's not that I think that pleasure is the right
 extreme and pain is the wrong
 extreme. It's that both extremes are useless. Engaging in
 sensual pleasure leads only to
 addiction and attachment and partiality, but engaging in
 torturing yourself is also just
 repressing and avoiding the nature of clinging. We don't
 have a problem with unpleasant things.
 We don't want them. We're not going to cling to unpleasant
 things. We have to look at the
 pleasant things that we cling to, because once we don't
 cling to anything, then we'll
 have no partiality, and the unpleasant things will no
 longer be unpleasant. We make no comparison.
 This is what we are undertaking here. We are taking the
 first step, and we take this
 step again and again when we take the precepts. It's a
 reaffirmation that we have started
 on the path and that we too want to come to see the truth
 of reality and want to overcome
 our attachments. The first step is to focus our energies
 and to focus our activities in
 terms of those activities which will bring clarity of mind
 and wisdom and understanding
 while avoiding those activities that are going to cloud the
 mind. This is where we start
 with these five or eight precepts, with the teaching of
 morality. The three refuges is
 simply an asservation, a determination that we are going to
 practice. This is like saying,
 I am committing myself to this path. The Buddha is my
 teacher, the Dhamma is the teaching I'm
 following, and the Sangha are my support group, the other
 Buddhist meditators and practitioners.
 I'm going to take these three things. There's a note here
 that we really should take this
 seriously, that we should think of the Buddha often. The
 Buddha recommended to think of
 him. We have Buddha images, and we do prostrations in front
 of the Buddha image. It's not because
 of worship or so on, it's just a relationship we have with
 the Buddha, with someone who
 reminds us of good things. It's not God, it's not someone
 who's going to answer our prayers,
 but it really does have a strong effect on your mind. This
 is why theistic religions
 really have a powerful effect on people's minds because it
 creates faith and confidence.
 If we channel those good qualities of mind in a way that is
 actually meaningful, then
 this is the practice to gain wisdom and understanding. Then
 this faith will have a profound effect
 on our practice, this confidence that we have, thinking,
 yes, we have this great teacher
 who taught these wonderful things, and he was right. We're
 feeling good and confident
 about that. When we have doubts, we just think of how hard
 the Buddha had to work, and we
 think of the sacrifice he made, and we think of his example
, that it is possible and his
 reassurance that someone can practice in this way. We
 should think about the Buddha. The
 Dhamma is something that we should think about. We should
 study the Dhamma. The Dhamma is
 the teaching, so we should take this seriously, and we
 should actually spend time reading
 the Buddha's teaching, listening to talks, and so on. You
 can pick up books, and you
 can look on the internet. There are textual teachings, and
 so on.
 And the Sangha, we really should take this seriously, and
 here's a chance for me to plug
 our new website, which is my.suryamungalu.org, which is a
 chance for everyone to get involved
 in our group. If you go and log on, I guess everyone here
 has already logged on, but please
 do make the most of it. If there are problems with it, let
 me know. If you have suggestions,
 let me know. And please do use this website at the very
 least as a means of communicating
 with each other. It doesn't mean that we have to come on
 and post this and post that and
 be really active. This means come on every so often. Let us
 know what you're doing and
 meditating, and if you have any questions, you can post
 them on the ask.suryamungalu.org.
 But organizing things. Someone started a Dhamma group in
 England. Owen started a Dhamma group
 in England based on the Mahasi Sayadaw tradition. And so
 hopefully that group will come together
 and can use the my.suryamungalu.org. Maybe that will help.
 Different ways that we can use this at the very least to
 see that there are other people
 in our group. And we have somehow an international group,
 those people who have come here to
 practice. Using the Sangha as a support group, because it's
 great encouragement and you really
 do need this kind of encouragement, especially when you're
 living on your own, surrounded
 often by people who aren't meditating. So here is an
 opportunity for us to come together
 and just post something every so often and read things. You
 can read, at the very least,
 you can read what I'm doing, if that's interesting. What we
're doing here and the projects that
 we have and the work that's going on. So this is the three
 refuges. Then the precepts are
 the beginning of the practice. The three refuges are just a
 determination of the practice that
 we're going to take. The precepts are beginning of the
 practice. So we have this determination,
 okay, my first step is I'm going to focus my activities.
 And this is going to create
 concentration because I'm not going to have this guilt all
 the time. I'm not going to
 have clouding in my mind. I'm not going to have anger and
 greed. I'm going to cut these
 things off. I'm going to abstain from them. When I want
 something, I'm going to force
 myself to look at the wanting. When I don't like something,
 I'm going to force myself
 to look at the disliking. When I have views and opinions, I
'm going to force myself to
 challenge them and to give up views and opinions in favor
 of realizations and understanding.
 So this morality is really for the purpose of the
 development of concentration or focus
 and wisdom. Again, I don't like using the word
 concentration because I think it's misleading.
 It's probably not even appropriate. The word is samadhi.
 Samadhi means composure really.
 Sama means same. It doesn't mean same, but it comes from
 the same root as the word same
 in English. Sama means level. So when your mind is balanced
, having a balanced mind,
 it doesn't mean you concentrate your mind to the point
 where you don't experience anything
 else or so on. It's where you see something perfectly
 clearly. It's like focusing a camera,
 as I've said before. You have to focus the camera exactly
 correctly. If you focus it
 too much, this would be like concentrating, then it goes
 out of focus again. If you don't
 focus enough, then it doesn't work. You have to find
 exactly the right focus. This is simply
 seeing things as they are. So as we use the technique of
 being mindful, reminding ourselves,
 this is seeing, this is hearing, this is smelling, this is
 tasting. As the Buddha said, "When
 you see something, let it only be seeing. When you hear
 something, let it only be hearing."
 So remind yourself, this is seeing, this is hearing. When
 it's pain, remind yourself,
 this is pain. When you feel happy, remind yourself, this is
 happiness. It's not good,
 it's not bad, it's not me, it's not mine. By doing this,
 your mind becomes focused. It
 comes into focus and you start to see things clearly. Your
 mind might not be concentrated
 in the sense it might still jump here and there, but when
 it jumps, you clearly see
 it jumps. You see the next object in line as it is, not as
 you'd like it to be or with
 any projections, but simply and clearly as the reality for
 what it is. It's actually
 quite simple. So concentration or focus is therefore for
 the purpose of gaining wisdom,
 because once you're in focus, you will see things as they
 are. Once you see things as
 they are, you'll become free from suffering. Your mind will
 let go. This is really the
 key that the way to become free from suffering is not by
 any force of mind. It's simply by
 understanding things as they are. The only reason one would
 ever create suffering for
 oneself is when one doesn't realize that one is creating
 suffering for oneself. Think about
 it. No one would ever do something if they knew that that
 thing was going to cause them
 harm. The only reason we do things is because we think it
 has some benefit, that there's
 some purpose to it, that this is a good thing to do. Once
 you see clearly that something
 is not a good thing to do, that there's no benefit that
 comes from it, then you give
 it up. If you examine it and you see that there is benefit
 and it does create goodness
 and peace and happiness, then you develop it. So the only
 thing we need to do in order
 to become free from suffering is quite simple, is just to
 see things as they are, because
 you'll never do something that is useless when you know
 that it's useless. The problem
 is we don't know that many of the things that we do and say
 and think are useless and detrimental
 to ourselves and to other beings. So this is a brief
 exposition of what this ceremony
 means and what this day means or should mean to all of us.
 I'd like to thank everyone for
 joining. Maybe before we quit, for those of you who still
 have time and it's not too late
 or too early in the morning, we can do a short meditation,
 group meditation. Then I'll take
 questions if there are still people left and we can go from
 there. So I'm going to start
 with the meditation. You're welcome to leave whenever you
 want. I'm not going to keep the
 audio on now. I'm going to turn the audio off so you can
 just set your own timer or just
 peek every so often and see when the text chat is. I think
 if you turn the sound on in the
 text chat, there's a little speaker in the bottom right
 corner. If you click that, then
 every time someone says something, it gives you a beep. So
 after 15 minutes I'm going
 to say something and you'll hear that beep. Otherwise, this
 has been another episode of
 Monk Radio. Thanks for tuning in.
 [
