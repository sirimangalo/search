1
00:00:00,000 --> 00:00:26,440
 [

2
00:00:26,440 --> 00:00:31,440
 Meditation for Meditation]

3
00:00:31,440 --> 00:00:35,440
 Meditation for Meditation

4
00:00:35,440 --> 00:00:40,440
 Meditation for Meditation

5
00:00:40,440 --> 00:00:45,440
 Meditation for Meditation

6
00:00:45,440 --> 00:00:50,440
 Meditation for Meditation

7
00:00:50,440 --> 00:00:55,440
 Meditation for Meditation

8
00:00:55,440 --> 00:01:00,440
 Meditation for Meditation

9
00:01:00,440 --> 00:01:05,440
 It's important to understand that there are answers besides

10
00:01:05,440 --> 00:01:06,440
 the meditation practice.

11
00:01:06,440 --> 00:01:09,720
 The meditation practice isn't the only thing that's

12
00:01:09,720 --> 00:01:10,440
 necessary.

13
00:01:10,440 --> 00:01:14,650
 This is why the Buddha recommended morality as well,

14
00:01:14,650 --> 00:01:16,440
 because ideally we do away with these states.

15
00:01:16,440 --> 00:01:19,620
 Ideally we give them up when we start to practice

16
00:01:19,620 --> 00:01:20,440
 meditation.

17
00:01:20,440 --> 00:01:25,670
 And he himself said, or made it quite clear in different

18
00:01:25,670 --> 00:01:26,440
 ways,

19
00:01:26,440 --> 00:01:30,440
 that the meditation relies on morality,

20
00:01:30,440 --> 00:01:34,440
 and without morality it's not truly going to succeed.

21
00:01:34,440 --> 00:01:37,620
 Now the problem with saying that one should avoid

22
00:01:37,620 --> 00:01:41,440
 meditation or should not consider meditation

23
00:01:41,440 --> 00:01:46,710
 is that it ignores the fact that meditation should indeed

24
00:01:46,710 --> 00:01:47,440
 be a part,

25
00:01:47,440 --> 00:01:50,440
 or is a part of learning to keep morality.

26
00:01:50,440 --> 00:01:54,760
 That even for people who can't keep basic morality, who are

27
00:01:54,760 --> 00:01:55,440
 stuck in addiction

28
00:01:55,440 --> 00:02:03,440
 and have physical and mental addictions to substances

29
00:02:03,440 --> 00:02:08,100
 or to the hormones in the body, as it was mentioned about

30
00:02:08,100 --> 00:02:09,440
 masturbation,

31
00:02:09,440 --> 00:02:14,320
 the person who asked the question was mentioning that, and

32
00:02:14,320 --> 00:02:15,440
 so on.

33
00:02:15,440 --> 00:02:23,440
 There is that side, but the meditation, the mental side,

34
00:02:23,440 --> 00:02:27,960
 is very much a part of the problem and is thus very

35
00:02:27,960 --> 00:02:30,440
 important to address.

36
00:02:30,440 --> 00:02:37,440
 Now, when we practice meditation we have to keep in mind

37
00:02:37,440 --> 00:02:38,440
 the physical side

38
00:02:38,440 --> 00:02:42,280
 and we have to understand that our state of being is going

39
00:02:42,280 --> 00:02:44,440
 to affect our meditation,

40
00:02:44,440 --> 00:02:47,440
 is going to really set the tone of our meditation practice.

41
00:02:47,440 --> 00:02:51,600
 So, you know, we need to eat the right food, we need to

42
00:02:51,600 --> 00:02:52,440
 look after our bodies

43
00:02:52,440 --> 00:02:57,440
 and our health in terms of medicine and so on.

44
00:02:57,440 --> 00:03:01,440
 We have to find a suitable place to live and so on,

45
00:03:01,440 --> 00:03:04,910
 along with the many other aspects of our practice that we

46
00:03:04,910 --> 00:03:06,440
 have to keep in mind.

47
00:03:06,440 --> 00:03:10,440
 So, there's no reason not to practice meditation,

48
00:03:10,440 --> 00:03:18,050
 but especially for people who are suffering from severe

49
00:03:18,050 --> 00:03:19,440
 forms of addiction,

50
00:03:19,440 --> 00:03:23,070
 extreme forms of addiction, there are going to be many

51
00:03:23,070 --> 00:03:23,440
 other things

52
00:03:23,440 --> 00:03:25,440
 that they'll have to keep in mind.

53
00:03:25,440 --> 00:03:29,440
 The Buddha gave a talk on all of the many ways to do away

54
00:03:29,440 --> 00:03:31,440
 with the problems in the mind.

55
00:03:31,440 --> 00:03:34,200
 For instance, we have to guard our senses, so we shouldn't

56
00:03:34,200 --> 00:03:35,440
 just look around,

57
00:03:35,440 --> 00:03:43,460
 stare at things and watch and engage in the pleasures of

58
00:03:43,460 --> 00:03:45,440
 the sense.

59
00:03:45,440 --> 00:03:50,250
 We should restrain ourselves in terms of food, in terms of

60
00:03:50,250 --> 00:03:52,440
 entertainment and so on.

61
00:03:52,440 --> 00:03:57,440
 We should guard our faculties so that we are mindful

62
00:03:57,440 --> 00:04:04,140
 and we're able to keep track of our state of mind and of

63
00:04:04,140 --> 00:04:06,440
 the world around us,

64
00:04:06,440 --> 00:04:09,440
 so to not get caught up in entertainment and pleasure.

65
00:04:09,440 --> 00:04:13,430
 I've given talks on this before, the various parts of the

66
00:04:13,430 --> 00:04:14,440
 practice.

67
00:04:14,440 --> 00:04:22,110
 So, that certainly is true, but the concern as to whether

68
00:04:22,110 --> 00:04:22,440
 people

69
00:04:22,440 --> 00:04:25,440
 who take up the meditation practice and aren't capable,

70
00:04:25,440 --> 00:04:29,700
 that they might therefore turn away from the practice and

71
00:04:29,700 --> 00:04:30,440
 give it up,

72
00:04:30,440 --> 00:04:36,190
 it seems a little bit specious that the truth of the matter

73
00:04:36,190 --> 00:04:36,440
 is,

74
00:04:36,440 --> 00:04:41,440
 and what we see, is that there are reasons why they give up

75
00:04:41,440 --> 00:04:41,440
.

76
00:04:41,440 --> 00:04:46,440
 And to an extent, we can mitigate these.

77
00:04:46,440 --> 00:04:51,440
 It's often because there's improper instruction,

78
00:04:51,440 --> 00:04:55,440
 it's often because there isn't the comprehensive practice,

79
00:04:55,440 --> 00:04:58,440
 it's often because of the surroundings.

80
00:04:58,440 --> 00:05:03,440
 So, every person's situation is different.

81
00:05:03,440 --> 00:05:08,440
 What's important is how we address this issue of people

82
00:05:08,440 --> 00:05:11,440
 giving up the practice and leaving it behind.

83
00:05:11,440 --> 00:05:14,440
 The first thing we should say is that just because a person

84
00:05:14,440 --> 00:05:15,440
 gives up the practice

85
00:05:15,440 --> 00:05:17,440
 doesn't mean they haven't gained anything.

86
00:05:17,440 --> 00:05:20,440
 So, a person might begin to practice meditation

87
00:05:20,440 --> 00:05:26,440
 and get to a certain level or get to a certain point

88
00:05:26,440 --> 00:05:29,440
 and then give it up or put it aside.

89
00:05:29,440 --> 00:05:34,620
 Now, we shouldn't therefore be discouraged or think that

90
00:05:34,620 --> 00:05:35,440
 this person is useless

91
00:05:35,440 --> 00:05:38,440
 or that they have no potential in the meditation practice.

92
00:05:38,440 --> 00:05:43,440
 It can be that after some time they'll come back to it.

93
00:05:43,440 --> 00:05:46,440
 And what we gain, the things that we do,

94
00:05:46,440 --> 00:05:48,440
 especially things that affect our state of mind,

95
00:05:48,440 --> 00:05:51,440
 have a profound effect on our psyche

96
00:05:51,440 --> 00:05:54,440
 and they stay deeply ingrained in our memory.

97
00:05:54,440 --> 00:05:57,440
 Meditation is something that it's very difficult to forget

98
00:05:57,440 --> 00:05:59,440
 and people can always come back to it.

99
00:05:59,440 --> 00:06:03,440
 Just learning the basics, the technique of meditation,

100
00:06:03,440 --> 00:06:06,440
 without even practicing it, can be a great thing

101
00:06:06,440 --> 00:06:09,440
 because in times of need it often comes back

102
00:06:09,440 --> 00:06:14,440
 and people do take up the practice in earnest.

103
00:06:14,440 --> 00:06:19,440
 So, I think that's the first point that I would make.

104
00:06:19,440 --> 00:06:26,160
 The second one is that environment plays a great role in

105
00:06:26,160 --> 00:06:27,440
 addiction recovery.

106
00:06:27,440 --> 00:06:31,440
 So, the physical aspects of addiction are obvious

107
00:06:31,440 --> 00:06:35,440
 and environment isn't going to get rid of those.

108
00:06:35,440 --> 00:06:37,340
 No matter where you are, you still have the hormones coming

109
00:06:37,340 --> 00:06:37,440
 up,

110
00:06:37,440 --> 00:06:40,470
 you still have the chemical reactions, the chemical

111
00:06:40,470 --> 00:06:41,440
 interactions

112
00:06:41,440 --> 00:06:44,440
 and so on, the physical craving.

113
00:06:44,440 --> 00:06:46,440
 But at least half of the problem,

114
00:06:46,440 --> 00:06:50,440
 actually much more of the problem, is the mental side.

115
00:06:50,440 --> 00:06:52,440
 And that you can influence by your environment,

116
00:06:52,440 --> 00:06:54,440
 by the people you surround yourself with,

117
00:06:54,440 --> 00:06:56,440
 by the interactions you have,

118
00:06:56,440 --> 00:06:59,440
 by the situations that you get yourself into.

119
00:06:59,440 --> 00:07:03,440
 Obviously, if all of your friends are addicted as well,

120
00:07:03,440 --> 00:07:06,440
 if your friends go out to bars and drinking

121
00:07:06,440 --> 00:07:08,440
 or do drugs or so on,

122
00:07:08,440 --> 00:07:14,440
 or if there's this hyper sexuality in the world around you,

123
00:07:14,440 --> 00:07:17,440
 then watching television or going to the mall,

124
00:07:17,440 --> 00:07:19,440
 going to the beach and so on,

125
00:07:19,440 --> 00:07:21,440
 and seeing the objects of your desire,

126
00:07:21,440 --> 00:07:24,440
 then obviously it's going to be much more difficult

127
00:07:24,440 --> 00:07:27,440
 for you to overcome this state.

128
00:07:27,440 --> 00:07:30,440
 Now, this is where meditation can excel

129
00:07:30,440 --> 00:07:33,840
 because a meditation center is pretty much the ideal

130
00:07:33,840 --> 00:07:34,440
 environment.

131
00:07:34,440 --> 00:07:38,600
 You're surrounded by people who are interested in

132
00:07:38,600 --> 00:07:39,440
 meditation,

133
00:07:39,440 --> 00:07:42,440
 who are trying to purify their own minds,

134
00:07:42,440 --> 00:07:45,440
 who are supportive, who are talking about the same things

135
00:07:45,440 --> 00:07:48,440
 and are encouraging each other in the same things.

136
00:07:48,440 --> 00:07:51,440
 You have people talking about the meditation practice

137
00:07:51,440 --> 00:07:53,440
 and teaching the meditation practice.

138
00:07:53,440 --> 00:07:55,440
 You have a really supportive environment

139
00:07:55,440 --> 00:07:58,440
 and that's really important. That makes a real difference.

140
00:07:58,440 --> 00:08:02,440
 I think the people who turn away most often

141
00:08:02,440 --> 00:08:05,440
 are those people who have never had that environment

142
00:08:05,440 --> 00:08:08,440
 or who have not had it on a long-term basis.

143
00:08:08,440 --> 00:08:10,440
 So, people will go to a retreat for 10 days

144
00:08:10,440 --> 00:08:13,440
 and all of the people come together for 10 days,

145
00:08:13,440 --> 00:08:15,440
 but no one's living there, no one's staying there.

146
00:08:15,440 --> 00:08:17,440
 So, there isn't the community feeling.

147
00:08:17,440 --> 00:08:22,640
 You don't feel like you're really living in this place, in

148
00:08:22,640 --> 00:08:23,440
 this environment.

149
00:08:23,440 --> 00:08:26,250
 It's quite different when you have a monastery that you go

150
00:08:26,250 --> 00:08:26,440
 to

151
00:08:26,440 --> 00:08:28,440
 and there are people staying there and living there

152
00:08:28,440 --> 00:08:31,440
 and you can live for a month or a year

153
00:08:31,440 --> 00:08:35,440
 and undertake the practice as a lifestyle.

154
00:08:35,440 --> 00:08:38,440
 That's a real great support.

155
00:08:38,440 --> 00:08:40,440
 And you'll see that in addiction therapy as well.

156
00:08:40,440 --> 00:08:43,440
 They advise the same thing, that it should be residential.

157
00:08:43,440 --> 00:08:49,530
 So, the meditation in that sense provides an excellent form

158
00:08:49,530 --> 00:08:50,440
 of addiction therapy

159
00:08:50,440 --> 00:08:54,440
 just by the basic environment.

160
00:08:54,440 --> 00:09:00,000
 Another thing is in regards to the physical addiction

161
00:09:00,000 --> 00:09:00,440
 itself

162
00:09:00,440 --> 00:09:04,440
 and I've talked about this before, that physical addiction

163
00:09:04,440 --> 00:09:05,440
 is one thing

164
00:09:05,440 --> 00:09:10,720
 but part of our practice is to not be free from the

165
00:09:10,720 --> 00:09:11,440
 physical addiction

166
00:09:11,440 --> 00:09:15,920
 but to rise above it, to see that it's only a physical

167
00:09:15,920 --> 00:09:16,440
 reaction.

168
00:09:16,440 --> 00:09:19,100
 The cravings that occur in the mind are actually not c

169
00:09:19,100 --> 00:09:19,440
ravings,

170
00:09:19,440 --> 00:09:22,440
 they're just physical processes.

171
00:09:22,440 --> 00:09:24,440
 So, someone who's addicted to nicotine, for example,

172
00:09:24,440 --> 00:09:28,440
 or someone who's addicted to the sexual hormones,

173
00:09:28,440 --> 00:09:31,440
 this is only the physical side.

174
00:09:31,440 --> 00:09:34,440
 It's something that arises and ceases.

175
00:09:34,440 --> 00:09:36,440
 It's actually neutral.

176
00:09:36,440 --> 00:09:41,440
 It's only our deeply ingrained reactions to the physical

177
00:09:41,440 --> 00:09:43,440
 that causes the problem.

178
00:09:43,440 --> 00:09:49,630
 So, if we can simply see the feelings, the sensations for

179
00:09:49,630 --> 00:09:50,440
 what they are,

180
00:09:50,440 --> 00:09:53,440
 the sensation of hormones arising,

181
00:09:53,440 --> 00:09:57,440
 whatever the chemical interactions of the hormones

182
00:09:57,440 --> 00:10:02,440
 or of nicotine and the cravings in the brain and so on,

183
00:10:02,440 --> 00:10:05,440
 then they'll cease to have any power over us.

184
00:10:05,440 --> 00:10:09,690
 This is why I said actually the physical is not the real

185
00:10:09,690 --> 00:10:10,440
 problem.

186
00:10:10,440 --> 00:10:16,610
 And so, for many people, it's actually the lack of

187
00:10:16,610 --> 00:10:17,440
 instruction.

188
00:10:17,440 --> 00:10:21,810
 I know there are often people who will go to a meditation

189
00:10:21,810 --> 00:10:22,440
 center

190
00:10:22,440 --> 00:10:25,440
 and will not get proper instruction for whatever reason.

191
00:10:25,440 --> 00:10:27,440
 Sometimes it's because they don't listen.

192
00:10:27,440 --> 00:10:30,630
 And this comes back to the idea that many people have

193
00:10:30,630 --> 00:10:31,440
 pointed out already

194
00:10:31,440 --> 00:10:33,440
 that you can't help everyone.

195
00:10:33,440 --> 00:10:38,440
 So, in the end, it is true that, and it's very much worth

196
00:10:38,440 --> 00:10:39,440
 bearing in mind,

197
00:10:39,440 --> 00:10:41,590
 we should never be frustrated when the people around us don

198
00:10:41,590 --> 00:10:42,440
't want to meditate.

199
00:10:42,440 --> 00:10:45,440
 We should take it upon ourselves to meditate.

200
00:10:45,440 --> 00:10:49,070
 And that will have an effect on our friends and family and

201
00:10:49,070 --> 00:10:49,440
 so on.

202
00:10:49,440 --> 00:10:53,440
 But in the end, it's up to the individual.

203
00:10:53,440 --> 00:10:56,100
 And there's so many people in the world who won't ever med

204
00:10:56,100 --> 00:10:58,440
itate, not in this life.

205
00:10:58,440 --> 00:11:01,970
 Which is why I said, "Give people what you can and help

206
00:11:01,970 --> 00:11:03,440
 people as you can

207
00:11:03,440 --> 00:11:06,440
 and don't expect too much."

208
00:11:06,440 --> 00:11:15,440
 On the other hand, if we do give, and if we are clear,

209
00:11:15,440 --> 00:11:20,480
 and if we understand correctly what it is that, what is the

210
00:11:20,480 --> 00:11:21,440
 meditation,

211
00:11:21,440 --> 00:11:24,440
 and how should one practice meditation?

212
00:11:24,440 --> 00:11:26,440
 I've never really had a problem.

213
00:11:26,440 --> 00:11:28,810
 I've never found anyone who didn't gain benefit from the

214
00:11:28,810 --> 00:11:29,440
 practice.

215
00:11:29,440 --> 00:11:32,440
 You have to give up at a certain point and say,

216
00:11:32,440 --> 00:11:35,140
 "That's all the person could gain and that's enough for

217
00:11:35,140 --> 00:11:35,440
 them

218
00:11:35,440 --> 00:11:38,230
 and not expect them or get frustrated when they don't get

219
00:11:38,230 --> 00:11:38,440
 more

220
00:11:38,440 --> 00:11:41,440
 or when they don't take it more seriously."

221
00:11:41,440 --> 00:11:46,730
 But there has to be someone there to guide them and to

222
00:11:46,730 --> 00:11:47,440
 instruct them.

223
00:11:47,440 --> 00:11:52,760
 So I think it's important that we study and that we get

224
00:11:52,760 --> 00:11:54,440
 clear in our own minds

225
00:11:54,440 --> 00:12:00,120
 about the practice and try our best to give people the

226
00:12:00,120 --> 00:12:01,440
 information.

227
00:12:01,440 --> 00:12:03,440
 What they do with that information is up to them.

228
00:12:03,440 --> 00:12:06,650
 And I would submit that even just giving them the

229
00:12:06,650 --> 00:12:07,440
 information, as I said,

230
00:12:07,440 --> 00:12:09,440
 is a great thing.

231
00:12:09,440 --> 00:12:12,210
 And I would never say to someone that you shouldn't med

232
00:12:12,210 --> 00:12:12,440
itate,

233
00:12:12,440 --> 00:12:14,440
 you should do something else first.

234
00:12:14,440 --> 00:12:16,870
 I would say there are many other things that you could do

235
00:12:16,870 --> 00:12:17,440
 with the meditation,

236
00:12:17,440 --> 00:12:20,830
 complementing the meditation, but meditation should be

237
00:12:20,830 --> 00:12:21,440
 essential

238
00:12:21,440 --> 00:12:24,440
 and eventually becomes really the only thing that you need.

239
00:12:24,440 --> 00:12:29,440
 Once you understand and get it and experience the benefits

240
00:12:29,440 --> 00:12:31,440
 and the results of the meditation,

241
00:12:31,440 --> 00:12:35,230
 then your mind will incline towards it and you'll find that

242
00:12:35,230 --> 00:12:35,440
 it more and more

243
00:12:35,440 --> 00:12:38,870
 becomes your answer to just about every problem that you

244
00:12:38,870 --> 00:12:39,440
 have.

245
00:12:39,440 --> 00:12:42,440
 So there's an answer to this question.

246
00:12:42,440 --> 00:12:45,440
 That's been another episode of Ask a Monk.

247
00:12:45,440 --> 00:12:49,830
 Thank you all for tuning in and hope to see you on the

248
00:12:49,830 --> 00:12:50,440
 forum

249
00:12:50,440 --> 00:12:53,440
 submitting your own questions and your own answers.

250
00:12:53,440 --> 00:12:55,440
 I'd like to thank everyone for submitting answers.

251
00:12:55,440 --> 00:13:00,030
 It certainly makes my job easier to have people who have,

252
00:13:00,030 --> 00:13:00,440
 you know,

253
00:13:00,440 --> 00:13:04,440
 in this way of studying the meditation.

254
00:13:04,440 --> 00:13:07,440
 It's not my teaching, but studying the Buddha's teaching

255
00:13:07,440 --> 00:13:12,440
 and this interpretation of the Buddha's teaching,

256
00:13:12,440 --> 00:13:15,440
 this tradition based on the Buddha's teaching,

257
00:13:15,440 --> 00:13:18,440
 and are able to use that to help others.

258
00:13:18,440 --> 00:13:20,440
 It's great to see and I'd encourage you to do that,

259
00:13:20,440 --> 00:13:24,440
 not only here, but also in your own family,

260
00:13:24,440 --> 00:13:27,440
 in your own town, in your own area.

261
00:13:27,440 --> 00:13:30,440
 So again, thanks for tuning in. All the best.

