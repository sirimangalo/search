1
00:00:00,000 --> 00:00:12,560
 Good evening everyone.

2
00:00:12,560 --> 00:00:26,760
 Broadcasting Live, January 16, 2016.

3
00:00:26,760 --> 00:00:32,160
 So yeah, I said I was probably going to do Q&A tonight.

4
00:00:32,160 --> 00:00:41,200
 I don't see many Qs, so...

5
00:00:41,200 --> 00:00:47,880
 There's something peaceful about audio broadcasts.

6
00:00:47,880 --> 00:00:49,840
 I'm fond of these.

7
00:00:49,840 --> 00:01:03,600
 More fun than the hangouts.

8
00:01:03,600 --> 00:01:04,600
 Simpler.

9
00:01:04,600 --> 00:01:11,480
 Just push a button and then broadcasting.

10
00:01:11,480 --> 00:01:13,560
 So I'm looking at the chat.

11
00:01:13,560 --> 00:01:21,440
 If anybody has any questions, you're welcome to post them.

12
00:01:21,440 --> 00:01:24,240
 We have a quote that we can look at.

13
00:01:24,240 --> 00:01:26,640
 A very short quote.

14
00:01:26,640 --> 00:01:31,760
 It's a quote about the difference between the defilements.

15
00:01:31,760 --> 00:01:34,760
 There's not much to say about it.

16
00:01:34,760 --> 00:01:37,640
 It's a very interesting quote.

17
00:01:37,640 --> 00:01:41,520
 It speaks for itself.

18
00:01:41,520 --> 00:01:52,770
 What you can point out is that based on this quote,

19
00:01:52,770 --> 00:01:56,120
 delusion is the worst.

20
00:01:56,120 --> 00:02:02,360
 Greed is slow to change, but not much blamed.

21
00:02:02,360 --> 00:02:09,680
 People don't blame you if you're greedy.

22
00:02:09,680 --> 00:02:14,840
 Here on the other hand, quick to change, but much blamed.

23
00:02:14,840 --> 00:02:18,760
 A little bit of anger.

24
00:02:18,760 --> 00:02:36,360
 Momentary outburst of anger can destroy relationships.

25
00:02:36,360 --> 00:02:43,660
 Which is funny, you know, because it doesn't really

26
00:02:43,660 --> 00:02:49,560
 adequately reflect a person's character.

27
00:02:49,560 --> 00:02:55,720
 One mistake is all it takes.

28
00:02:55,720 --> 00:02:58,920
 Suppose you could say the same with greed.

29
00:02:58,920 --> 00:03:03,880
 Suppose a husband or a wife cheats on their spouse.

30
00:03:03,880 --> 00:03:13,080
 You could argue that says something about their character.

31
00:03:13,080 --> 00:03:19,360
 Just trying to think like a moment of weakness, right?

32
00:03:19,360 --> 00:03:23,000
 Anger is more blamed.

33
00:03:23,000 --> 00:03:26,760
 It's clear.

34
00:03:26,760 --> 00:03:32,080
 But the worst is delusion.

35
00:03:32,080 --> 00:03:36,400
 It's slow to change and much blamed.

36
00:03:36,400 --> 00:03:54,200
 So this is arrogance and conceit, wrong view, ignorance.

37
00:03:54,200 --> 00:03:57,160
 And it's slow to change.

38
00:03:57,160 --> 00:04:01,890
 You can sense this when you meditate, the greed and

39
00:04:01,890 --> 00:04:04,840
 delusion are slow to change.

40
00:04:04,840 --> 00:04:22,120
 More sticky and heavier, anger is generally speaking

41
00:04:22,120 --> 00:04:30,440
 lighter and more quick.

42
00:04:30,440 --> 00:04:44,280
 I don't have anything else to talk about.

43
00:04:44,280 --> 00:04:49,000
 It's been a fairly uneventful day here.

44
00:04:49,000 --> 00:04:51,960
 I've been working on my TED Talk.

45
00:04:51,960 --> 00:04:54,960
 TEDx Talk.

46
00:04:54,960 --> 00:05:01,440
 And Latin.

47
00:05:01,440 --> 00:05:15,440
 Otherwise just another day at the monastery.

48
00:05:15,440 --> 00:05:20,440
 Someone brought food today.

49
00:05:20,440 --> 00:05:30,640
 It's a local resource.

50
00:05:30,640 --> 00:05:33,360
 And we're not even sure if we're going to survive out the

51
00:05:33,360 --> 00:05:34,240
 year, right?

52
00:05:34,240 --> 00:05:37,240
 The end of this year, August.

53
00:05:37,240 --> 00:05:47,800
 We'll see whether we can even continue on.

54
00:05:47,800 --> 00:05:55,520
 It's nice that people can come in and see if it continues.

55
00:05:55,520 --> 00:05:59,280
 Today we had at least one person come in and meditate.

56
00:05:59,280 --> 00:06:06,080
 A McMaster student came for us Saturday.

57
00:06:06,080 --> 00:06:21,960
 Just came in and started meditating.

58
00:06:21,960 --> 00:06:46,840
 We're going to go to the next one.

59
00:06:46,840 --> 00:07:13,900
 I guess the one thing I can say about the quote is, for

60
00:07:13,900 --> 00:07:15,840
 some people it might not have been

61
00:07:15,840 --> 00:07:22,630
 clear that these three things are the essence of all that's

62
00:07:22,630 --> 00:07:24,480
 blameworthy.

63
00:07:24,480 --> 00:07:26,480
 There's nothing else.

64
00:07:26,480 --> 00:07:32,000
 So the Akuṣṭha Mūra, the roots of unwholesome.

65
00:07:32,000 --> 00:07:36,000
 So all unwholesome minds are just rooted in these three

66
00:07:36,000 --> 00:07:36,840
 things.

67
00:07:36,840 --> 00:07:38,600
 It's not complicated.

68
00:07:38,600 --> 00:07:46,920
 The problem is not a complicated thing.

69
00:07:46,920 --> 00:07:52,360
 The causes of our problems are not complicated.

70
00:07:52,360 --> 00:07:58,120
 Quite simple.

71
00:07:58,120 --> 00:08:13,320
 Greed is the desire for something, for a sensual experience

72
00:08:13,320 --> 00:08:19,840
 or for something to come or for

73
00:08:19,840 --> 00:08:22,840
 something to go.

74
00:08:22,840 --> 00:08:29,840
 Anger is aversion.

75
00:08:29,840 --> 00:08:41,280
 And delusion is the cloudiness of the mind.

76
00:08:41,280 --> 00:08:55,030
 It includes things like arrogance and conceit and wrong

77
00:08:55,030 --> 00:09:00,960
 views and ignorance.

78
00:09:00,960 --> 00:09:01,960
 We don't have that much.

79
00:09:01,960 --> 00:09:06,920
 It's not that difficult of a job, right?

80
00:09:06,920 --> 00:09:13,640
 But the Buddha said these are the binds of defilements are

81
00:09:13,640 --> 00:09:15,920
 stronger than steel.

82
00:09:15,920 --> 00:09:27,880
 He said the bond of craving is stronger than any other bond

83
00:09:27,880 --> 00:09:33,200
 of leather or steel.

84
00:09:33,200 --> 00:09:37,640
 I guess he wouldn't have said steel, but iron.

85
00:09:37,640 --> 00:09:43,640
 I don't think they had steel back then.

86
00:09:43,640 --> 00:09:45,640
 I'm not sure.

87
00:09:45,640 --> 00:09:47,640
 Stronger than any physical bind.

88
00:09:47,640 --> 00:09:50,640
 These are not strong binds of leather and metal.

89
00:09:50,640 --> 00:10:03,130
 In fact, the bonds are so strong and the bond of delusion

90
00:10:03,130 --> 00:10:09,840
 and ignorance is so strong.

91
00:10:09,840 --> 00:10:29,250
 It's amazing that the Buddha even was able to figure this

92
00:10:29,250 --> 00:10:33,040
 all out.

93
00:10:33,040 --> 00:10:34,720
 There's not all that much to say about it.

94
00:10:34,720 --> 00:10:39,700
 I think I was thinking, is it really necessary to keep

95
00:10:39,700 --> 00:10:42,320
 making YouTube videos, saying the

96
00:10:42,320 --> 00:10:47,030
 same sorts of things or trying to come up with new things

97
00:10:47,030 --> 00:10:47,920
 to say?

98
00:10:47,920 --> 00:10:57,440
 It's important to consider what is the most important.

99
00:10:57,440 --> 00:11:00,720
 Setting up these online courses was a neat thing.

100
00:11:00,720 --> 00:11:04,200
 I think that's important.

101
00:11:04,200 --> 00:11:09,880
 Encouraging people in meditation to actually do the work,

102
00:11:09,880 --> 00:11:11,200
 progress.

103
00:11:11,200 --> 00:11:23,290
 It's great to hear people talk about how this, how the

104
00:11:23,290 --> 00:11:31,200
 videos and the teachings changed their

105
00:11:31,200 --> 00:11:32,200
 lives.

106
00:11:32,200 --> 00:11:34,200
 It's really neat.

107
00:11:34,200 --> 00:11:39,200
 That's really what we have to focus on.

108
00:11:39,200 --> 00:11:53,290
 How do we encourage people and bring people, more people to

109
00:11:53,290 --> 00:11:56,480
 practice?

110
00:11:56,480 --> 00:12:01,280
 The goal is personal.

111
00:12:01,280 --> 00:12:05,440
 The goal is not to help others.

112
00:12:05,440 --> 00:12:11,200
 The goal is to help ourselves.

113
00:12:11,200 --> 00:12:15,410
 But as part of helping ourselves, as part of our life,

114
00:12:15,410 --> 00:12:17,720
 helping others as part of our

115
00:12:17,720 --> 00:12:19,720
 practice, part of our life.

116
00:12:19,720 --> 00:12:35,040
 Anyway, I've got a lot to say tonight.

117
00:12:35,040 --> 00:12:38,920
 I think I'll say goodnight.

118
00:12:38,920 --> 00:12:42,480
 Let's look first and see who's meditating.

119
00:12:42,480 --> 00:12:48,000
 We have a bunch of people today.

120
00:12:48,000 --> 00:12:49,000
 Thanks everyone.

121
00:12:49,000 --> 00:12:50,000
 Thanks for being a part of the community.

122
00:12:50,000 --> 00:12:59,160
 It's great to see people coming and meditating.

123
00:12:59,160 --> 00:13:02,160
 This is where real goodness happens.

124
00:13:02,160 --> 00:13:05,160
 It's good to see.

125
00:13:05,160 --> 00:13:09,200
 Anyway, have a good night.

126
00:13:09,200 --> 00:13:12,600
 See you all tomorrow.

127
00:13:12,600 --> 00:13:28,840
 [

