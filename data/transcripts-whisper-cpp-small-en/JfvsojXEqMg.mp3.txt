 Hi everyone, I'm Adder, a volunteer for Siri Mangello
 International, the organization that
 supports Yutadamo Biku and his teachings.
 I'd like to take a few minutes to tell you all about some
 of the things our organization
 does.
 If you're watching this video, you're probably aware of the
 plethora of videos that Bontay
 Yutadamo posts on YouTube to share his teachings.
 You might not know about some of the other things we do.
 Feel free to visit siri-mangello.org for more information.
 One thing that we offer is a live meditation center located
 in Ontario, Canada.
 If you'd like to schedule an intensive retreat under the
 guidance of Bontay Yutadamo, please
 visit our website to do so.
 Furthermore, Bontay offers online instruction in our
 Meditation Plus community.
 If you visit meditation.siri-mangello.org, you can schedule
 a time to meet weekly with
 Bontay Yutadamo and do an online meditation course.
 Additionally, the community hosts a chat room, our Ask
 Questions board, where you can ask
 the questions that Yutadamo responds to online.
 And there's also a meditation timer and a way to track your
 meditation progress.
 We also host a weekly Dhamma study group.
 And this happens in the online voice chat platform called
 Discord.
 And I have a link below for you to go there.
 Additionally, our Discord hosts a general room for
 discussion and is a great place for
 our volunteers to connect.
 If you're interested in volunteering for the organization,
 feel free to get on Discord and
 get in touch with us.
 Finally, I want you all to know that our organization
 depends on the generous donations from our
 supporters.
 If you're interested in supporting Siri Mangello, please
 visit siri-mangello.org/support.
 Thank you all.
 Have a nice day.
