1
00:00:00,000 --> 00:00:07,000
 Okay, good evening everyone.

2
00:00:07,000 --> 00:00:14,000
 Welcome to our evening dhamma.

3
00:00:14,000 --> 00:00:27,580
 Today, if I got my calendar correct, today is the full moon

4
00:00:27,580 --> 00:00:29,000
 of the month of Maga.

5
00:00:29,000 --> 00:00:37,000
 Which makes this Maga Puja.

6
00:00:37,000 --> 00:00:42,000
 Maga Puja.

7
00:00:42,000 --> 00:00:46,000
 Or Maga Punami.

8
00:00:46,000 --> 00:00:54,000
 Punami means the day of the full moon. Maga.

9
00:00:54,000 --> 00:01:02,000
 But Maga Puja means it's a time of commemoration.

10
00:01:02,000 --> 00:01:08,000
 So normally on this day there is an excuse.

11
00:01:08,000 --> 00:01:14,000
 And there are big celebrations or special talks and special

12
00:01:14,000 --> 00:01:23,000
 good doings and monasteries.

13
00:01:23,000 --> 00:01:34,000
 What we commemorate is what is called the Ovada Patimoka.

14
00:01:34,000 --> 00:01:42,510
 Now in the beginning of the dispensation of the Buddha when

15
00:01:42,510 --> 00:01:45,000
 he began to teach,

16
00:01:45,000 --> 00:01:51,000
 there was no established set of rules.

17
00:01:51,000 --> 00:02:01,310
 And so the tradition goes that he would preside over the

18
00:02:01,310 --> 00:02:04,000
 meeting of the monks every two weeks

19
00:02:04,000 --> 00:02:09,000
 on the full moon, on the empty moon, on the new moon.

20
00:02:09,000 --> 00:02:18,000
 And would recite the Ovada Patimoka.

21
00:02:18,000 --> 00:02:23,000
 How it started is on this day, apparently.

22
00:02:23,000 --> 00:02:24,000
 I mean this is the tradition.

23
00:02:24,000 --> 00:02:28,470
 So we have this story commemorating this event when it's

24
00:02:28,470 --> 00:02:31,000
 the Chaturanga Sanipata.

25
00:02:31,000 --> 00:02:41,020
 The four fold meeting where a group of 1,250 arahants

26
00:02:41,020 --> 00:02:43,000
 gathered together

27
00:02:43,000 --> 00:02:46,000
 to listen to the Buddha's teaching.

28
00:02:46,000 --> 00:02:51,470
 So the conjecture is that this is actually the early on in

29
00:02:51,470 --> 00:02:56,000
 the Buddha's ministry,

30
00:02:56,000 --> 00:02:58,000
 for lack of a better word.

31
00:02:58,000 --> 00:03:03,610
 When he went to Rajagaha and he stopped off on his way

32
00:03:03,610 --> 00:03:04,000
 there,

33
00:03:04,000 --> 00:03:12,850
 he stopped off in Gaya Sisa and he met with these three

34
00:03:12,850 --> 00:03:14,000
 fire assentings.

35
00:03:14,000 --> 00:03:18,500
 So he spent some time with his first fire ascetic and these

36
00:03:18,500 --> 00:03:21,000
 ascetics who worshipped fire.

37
00:03:21,000 --> 00:03:29,300
 And through various means and miracles, he converted these

38
00:03:29,300 --> 00:03:40,000
 three brothers along with their 1,250 followers.

39
00:03:40,000 --> 00:03:44,000
 Is that right?

40
00:03:44,000 --> 00:03:50,000
 I can't remember exactly. Something like that.

41
00:03:50,000 --> 00:04:01,000
 Anyway, so all of these monks were off meditating.

42
00:04:01,000 --> 00:04:05,500
 And suddenly one of them, through his practice, achieved

43
00:04:05,500 --> 00:04:09,000
 liberation, became an arahant.

44
00:04:09,000 --> 00:04:14,000
 And the story goes that he got up and decided he would go

45
00:04:14,000 --> 00:04:16,000
 and pay respect to the Buddha

46
00:04:16,000 --> 00:04:22,000
 and let the Buddha know of his attainment.

47
00:04:22,000 --> 00:04:27,000
 So he walked to where the Buddha was seated up on the hill

48
00:04:27,000 --> 00:04:29,000
 in the opening and in the clearing.

49
00:04:29,000 --> 00:04:31,600
 And when he got there, he sat down, paid respect to the

50
00:04:31,600 --> 00:04:32,000
 Buddha,

51
00:04:32,000 --> 00:04:39,180
 and then he turned just to make sure there was no...he wasn

52
00:04:39,180 --> 00:04:40,000
't going to interrupt anything.

53
00:04:40,000 --> 00:04:48,880
 And he saw another monk walking towards them, ostensibly to

54
00:04:48,880 --> 00:04:50,000
 do the same thing.

55
00:04:50,000 --> 00:04:55,140
 And so sure enough, a second monk had become an arahant and

56
00:04:55,140 --> 00:04:56,000
 did the same thing.

57
00:04:56,000 --> 00:05:02,000
 So the first monk said, "I'll wait for him to come."

58
00:05:02,000 --> 00:05:04,570
 And the second monk got there. He did the same thing,

59
00:05:04,570 --> 00:05:08,000
 turned around, and the third monk was walking.

60
00:05:08,000 --> 00:05:12,000
 The idea is they all came together.

61
00:05:12,000 --> 00:05:16,850
 One of the marvelous things about the meeting is that it

62
00:05:16,850 --> 00:05:20,000
 was completely uncoordinated.

63
00:05:20,000 --> 00:05:26,000
 There was no prior engagement.

64
00:05:26,000 --> 00:05:33,000
 And yet all 1,250 of them came together.

65
00:05:33,000 --> 00:05:38,000
 And they were all arahants and so on.

66
00:05:38,000 --> 00:05:42,870
 And so the Buddha, on that day, is said to have given the u

67
00:05:42,870 --> 00:05:44,000
vanda patimoka.

68
00:05:44,000 --> 00:05:46,480
 And so it's just a means of really commemorating this

69
00:05:46,480 --> 00:05:47,000
 teaching,

70
00:05:47,000 --> 00:05:51,000
 which is really important for a good summary.

71
00:05:51,000 --> 00:05:59,000
 It's quite well known in the Buddhist world.

72
00:05:59,000 --> 00:06:06,050
 It's one of those verses that is important to Buddhists,

73
00:06:06,050 --> 00:06:09,000
 Theravada Buddhists.

74
00:06:09,000 --> 00:06:10,000
 So how does it go?

75
00:06:10,000 --> 00:06:18,870
 The Buddha taught the uvanda patimoka, kanti paramang dapot

76
00:06:18,870 --> 00:06:20,000
itika.

77
00:06:20,000 --> 00:06:31,000
 Kanti, patience, is the highest form of asceticism.

78
00:06:31,000 --> 00:06:42,070
 Nibaanang paramang vadanti buddha. Nibaana is the greatest

79
00:06:42,070 --> 00:06:44,000
 say, the Buddhas.

80
00:06:44,000 --> 00:07:02,930
 Nahepa panchito parupagati. No, one is not a recluse, a ren

81
00:07:02,930 --> 00:07:04,000
unciant who injures others.

82
00:07:04,000 --> 00:07:13,640
 Samano hoti parang vihay tayanto. One is not a recluse who

83
00:07:13,640 --> 00:07:18,000
 abuses others.

84
00:07:18,000 --> 00:07:24,960
 Sabapapasa akaranang, the not doing of any evil, kusulasa

85
00:07:24,960 --> 00:07:26,000
 upasampada,

86
00:07:26,000 --> 00:07:29,000
 being full of good, full of wholesomeness.

87
00:07:29,000 --> 00:07:34,560
 Satyita pariyodapanang, together with the purifying of one

88
00:07:34,560 --> 00:07:36,000
's own mind.

89
00:07:36,000 --> 00:07:43,260
 Itang buddha nasaasanam. This is the teaching of all the

90
00:07:43,260 --> 00:07:45,000
 Buddhas.

91
00:07:45,000 --> 00:07:50,420
 Anupavado anupaghato, not speaking badly of anyone or

92
00:07:50,420 --> 00:07:53,000
 harming anyone.

93
00:07:53,000 --> 00:07:59,170
 Patimokhe jasangwaro, being restrained or guarded in

94
00:07:59,170 --> 00:08:01,000
 regards to the patimoka,

95
00:08:01,000 --> 00:08:06,000
 which is the ways of the community.

96
00:08:06,000 --> 00:08:14,000
 Matanyutta japatasmin, knowing moderation in eating.

97
00:08:14,000 --> 00:08:24,040
 Pantancha saiyana asanam, dwelling in the secluded lodging,

98
00:08:24,040 --> 00:08:27,000
 having a secluded lodging.

99
00:08:27,000 --> 00:08:36,000
 Adityitte jayogoh etang buddha nasaasanam, and the striving

100
00:08:36,000 --> 00:08:43,000
 or exerting oneself

101
00:08:43,000 --> 00:08:49,000
 in regards to higher minds, higher mind states.

102
00:08:49,000 --> 00:08:56,000
 This is the teaching of all the Buddhas.

103
00:08:56,000 --> 00:08:59,410
 And so this was considered, or is considered to be, the

104
00:08:59,410 --> 00:09:01,000
 essence of the Buddha's exhortation,

105
00:09:01,000 --> 00:09:09,000
 it's sort of a pithy exhortation, something that we recite,

106
00:09:09,000 --> 00:09:11,000
 something that we remember.

107
00:09:11,000 --> 00:09:17,000
 It's apparently something that all Buddhas teach as their

108
00:09:17,000 --> 00:09:24,000
 admonishment or exhortation on the holiday.

109
00:09:24,000 --> 00:09:31,470
 So kanti paramangtapodidhika, patience. This is an

110
00:09:31,470 --> 00:09:33,000
 important, this sentence I often remind my meditators of.

111
00:09:33,000 --> 00:09:36,020
 Meditation can be such torture, but there's nothing wrong

112
00:09:36,020 --> 00:09:37,000
 with the meditation.

113
00:09:37,000 --> 00:09:43,160
 It's not that meditation is painful or stressful, not at

114
00:09:43,160 --> 00:09:44,000
 all.

115
00:09:44,000 --> 00:09:52,000
 It's just that we lack patience.

116
00:09:52,000 --> 00:09:59,610
 We possess those qualities of mind that require us to be

117
00:09:59,610 --> 00:10:02,000
 patient with.

118
00:10:02,000 --> 00:10:06,410
 Meaning we have greed, we have anger, and to counter them

119
00:10:06,410 --> 00:10:08,000
 we need patience.

120
00:10:08,000 --> 00:10:11,960
 And patience is the greatest form of asceticism, austerity,

121
00:10:11,960 --> 00:10:13,000
 torture really,

122
00:10:13,000 --> 00:10:17,830
 as in the time of the Buddha tapas, tapa, wouldn't mean

123
00:10:17,830 --> 00:10:20,000
 torturing yourself.

124
00:10:20,000 --> 00:10:24,360
 Meditation, patience can be quite a torture, having to sit

125
00:10:24,360 --> 00:10:27,000
 through and not react.

126
00:10:27,000 --> 00:10:30,250
 It's actually a lot easier, and this is why this is the

127
00:10:30,250 --> 00:10:32,790
 highest, because it's much easier when you feel pleasure to

128
00:10:32,790 --> 00:10:35,000
 just beat yourself and get rid of it, right?

129
00:10:35,000 --> 00:10:39,010
 Self-flagellation, which is apparently a religious thing

130
00:10:39,010 --> 00:10:44,090
 that Christians used to do it, and Indian ascetics used to

131
00:10:44,090 --> 00:10:45,000
 do it.

132
00:10:45,000 --> 00:10:50,610
 Buddhists don't do that. We torture ourselves in other ways

133
00:10:50,610 --> 00:10:51,000
.

134
00:10:51,000 --> 00:11:00,740
 We torture ourselves with nothing by refusing to engage in

135
00:11:00,740 --> 00:11:05,000
 aversion, addiction,

136
00:11:05,000 --> 00:11:09,660
 by standing firm in the present moment in reality, being

137
00:11:09,660 --> 00:11:11,000
 patient.

138
00:11:11,000 --> 00:11:17,000
 Patient with good things, patient with bad things.

139
00:11:17,000 --> 00:11:24,290
 Nibānāṅ paramangvadantibuddha. So nibāṇa is the

140
00:11:24,290 --> 00:11:25,000
 highest, again an important point, and it's fairly simple,

141
00:11:25,000 --> 00:11:28,950
 and there's not a lot you can say about nibāṇa, it's

142
00:11:28,950 --> 00:11:30,000
 freedom.

143
00:11:30,000 --> 00:11:34,590
 But putting it at the top is important. That's what sort of

144
00:11:34,590 --> 00:11:39,000
 distinguishes Buddhism from any other religion.

145
00:11:39,000 --> 00:11:42,020
 Other religions have their own versions of similar things,

146
00:11:42,020 --> 00:11:45,000
 but let's be clear in Buddhism, what is the goal?

147
00:11:45,000 --> 00:11:48,280
 It's not heaven, it's not becoming one with God or one with

148
00:11:48,280 --> 00:11:51,000
 everything or anything. It's freedom.

149
00:11:51,000 --> 00:11:58,090
 It's the unbinding, or it's release, the cessation of

150
00:11:58,090 --> 00:12:00,000
 suffering.

151
00:12:00,000 --> 00:12:03,490
 Nāhipa-pajito-parupa-gādhi, and here's actually, well

152
00:12:03,490 --> 00:12:06,000
 this is another section on,

153
00:12:06,000 --> 00:12:11,000
 this is really the bhātimoka part where you shouldn't do.

154
00:12:11,000 --> 00:12:15,340
 It's just a simple admonishment that whatever we do, if it

155
00:12:15,340 --> 00:12:21,000
 falls under, harming others, speaking ill of them,

156
00:12:21,000 --> 00:12:24,470
 acting or speaking in such a way as to cause harm and

157
00:12:24,470 --> 00:12:30,000
 suffering to them, this isn't the way of a reckless.

158
00:12:30,000 --> 00:12:38,710
 I remember once there was this monk who was, what was it,

159
00:12:38,710 --> 00:12:43,000
 he was saying something.

160
00:12:43,000 --> 00:12:45,950
 He was calling me all sorts of awful names. He had mental

161
00:12:45,950 --> 00:12:48,000
 problems, British men.

162
00:12:48,000 --> 00:12:51,660
 He'd been beaten as a child, abused as a child, done a lot

163
00:12:51,660 --> 00:12:54,000
 of drugs through his life and so on,

164
00:12:54,000 --> 00:13:01,120
 and then he became a monk and he was a real terror. So he

165
00:13:01,120 --> 00:13:02,000
 called me all sorts of names, and something came up about

166
00:13:02,000 --> 00:13:02,000
 how

167
00:13:02,000 --> 00:13:07,420
 I didn't treat him like a monk and so on. And I said, "You

168
00:13:07,420 --> 00:13:09,000
're not a monk."

169
00:13:09,000 --> 00:13:14,100
 And he stopped and he walked away. And then he came back

170
00:13:14,100 --> 00:13:16,000
 about an hour later with the rule book

171
00:13:16,000 --> 00:13:22,630
 and he accused me of an offense because I had claimed that

172
00:13:22,630 --> 00:13:26,000
 he had committed an offense.

173
00:13:26,000 --> 00:13:29,630
 He somehow saw that I had accused him of being no longer a

174
00:13:29,630 --> 00:13:32,000
 monk, so he had committed a serious offense.

175
00:13:32,000 --> 00:13:36,000
 Anyway, I said to him, "No, no, I didn't mean it that way."

176
00:13:36,000 --> 00:13:39,110
 I said, "What is it?" And I recited this verse. I said, "

177
00:13:39,110 --> 00:13:44,000
You're not a monk, if you abuse others."

178
00:13:44,000 --> 00:13:47,180
 He calmed down actually and we were able to talk again. He

179
00:13:47,180 --> 00:13:49,000
 was kind of like that.

180
00:13:49,000 --> 00:13:52,580
 He started out a role with it. He would be threatening to

181
00:13:52,580 --> 00:13:54,000
 beat you the one minute and then

182
00:13:54,000 --> 00:13:58,080
 he'd be sitting joking and then at the other the next

183
00:13:58,080 --> 00:13:59,000
 minute.

184
00:13:59,000 --> 00:14:09,000
 Yeah, life is a monk in Thailand. Never a dull moment.

185
00:14:09,000 --> 00:14:13,230
 This is an important exhortation for us. Be clear in your

186
00:14:13,230 --> 00:14:16,000
 minds. This isn't the way of the Buddha.

187
00:14:16,000 --> 00:14:21,260
 Whether you're a monk or not a monk, you wish to be a true

188
00:14:21,260 --> 00:14:24,000
 recluse, a true renunciant.

189
00:14:24,000 --> 00:14:27,530
 You don't have to put on robes to do that. Be a follower of

190
00:14:27,530 --> 00:14:33,000
 the Buddha, follow his way, follow his guidance.

191
00:14:33,000 --> 00:14:40,390
 Don't abuse others. Don't harm others. Don't speak ill of

192
00:14:40,390 --> 00:14:42,000
 them.

193
00:14:42,000 --> 00:14:45,360
 Sabapapa sah karanang, don't do any evil. This is the

194
00:14:45,360 --> 00:14:47,000
 important part. This part is what everyone repeats.

195
00:14:47,000 --> 00:14:49,000
 This is the essence of it.

196
00:14:49,000 --> 00:14:53,230
 Kussadaso upa sampada, be full of good, be full of wholes

197
00:14:53,230 --> 00:14:54,000
omeness.

198
00:14:54,000 --> 00:14:57,970
 Sacheta pariudapanang, and purify your mind. So these three

199
00:14:57,970 --> 00:15:01,000
 things really make the essence of Buddhism.

200
00:15:01,000 --> 00:15:04,400
 Do not do any evil, to be full of good and to purify your

201
00:15:04,400 --> 00:15:05,000
 mind.

202
00:15:05,000 --> 00:15:08,060
 Now the first two alone aren't enough, and this is

203
00:15:08,060 --> 00:15:09,000
 important.

204
00:15:09,000 --> 00:15:14,620
 It distinguishes Buddhism from any other types of religious

205
00:15:14,620 --> 00:15:18,000
, spiritual or even good practices.

206
00:15:18,000 --> 00:15:21,900
 Because anyone who devotes their time to doing good and

207
00:15:21,900 --> 00:15:26,100
 avoiding evil is ignoring the crucial aspect of that which

208
00:15:26,100 --> 00:15:27,000
 causes us to do evil,

209
00:15:27,000 --> 00:15:31,000
 and which prevents us from doing good. And that's our mind.

210
00:15:31,000 --> 00:15:37,570
 If your mind is not pure, if your mind is not well, very

211
00:15:37,570 --> 00:15:45,000
 hard to avoid evil, very hard to be full of goodness.

212
00:15:45,000 --> 00:15:49,000
 And so our main activity really is purification of the mind

213
00:15:49,000 --> 00:15:54,520
, because once your mind is pure, you can't possibly do evil

214
00:15:54,520 --> 00:15:55,000
.

215
00:15:55,000 --> 00:16:03,000
 Again, possibly you are by nature constantly doing good.

216
00:16:03,000 --> 00:16:08,000
 And it goes the other way. It's not to say that you should

217
00:16:08,000 --> 00:16:10,000
 just ignore evil and good.

218
00:16:10,000 --> 00:16:14,580
 Evil will take away from your practice, prevent you from

219
00:16:14,580 --> 00:16:16,000
 progressing.

220
00:16:16,000 --> 00:16:19,770
 And doing good will support your practice, support the

221
00:16:19,770 --> 00:16:23,400
 strength of mind needed to become enlightened, to purify

222
00:16:23,400 --> 00:16:24,000
 your mind.

223
00:16:24,000 --> 00:16:28,450
 So these three together, "Etangbuddha and Asasanang", this

224
00:16:28,450 --> 00:16:32,000
 is the teaching of all the Buddhas.

225
00:16:32,000 --> 00:16:34,950
 But then he repeats, he gives another list of things that

226
00:16:34,950 --> 00:16:39,480
 are also the teaching of the Buddhas, so we think about

227
00:16:39,480 --> 00:16:42,000
 these as well.

228
00:16:42,000 --> 00:16:47,180
 "Anupavado", "Anupagado", again, not speaking ill of others

229
00:16:47,180 --> 00:16:49,000
, not harming others.

230
00:16:49,000 --> 00:16:57,000
 "Pati Mokhe Jisangwaro", being restrained by discipline.

231
00:16:57,000 --> 00:17:00,790
 So this is for monks, all the rules of the monks, for lay

232
00:17:00,790 --> 00:17:04,590
 people, it's the five or the eight precepts, right

233
00:17:04,590 --> 00:17:07,000
 livelihood and so on.

234
00:17:07,000 --> 00:17:09,000
 All of this is the teaching of the Buddhas.

235
00:17:09,000 --> 00:17:13,000
 "Matañuta ajambatasming", knowing moderation and eating.

236
00:17:13,000 --> 00:17:17,070
 Now the Buddha, you see, this is something that we often

237
00:17:17,070 --> 00:17:18,000
 neglect. Be careful.

238
00:17:18,000 --> 00:17:21,760
 Your consumption, and we can apply this not just to food,

239
00:17:21,760 --> 00:17:24,000
 but to everything that we consume,

240
00:17:24,000 --> 00:17:27,570
 whether it be television or whether it be internet or

241
00:17:27,570 --> 00:17:30,000
 whether it be music or whatever.

242
00:17:30,000 --> 00:17:33,180
 Be careful about your consumption, because what you take in

243
00:17:33,180 --> 00:17:36,000
 affects you, it changes who you are,

244
00:17:36,000 --> 00:17:43,670
 and if you're not mindful, it can lead to addiction, stress

245
00:17:43,670 --> 00:17:46,000
 and suffering.

246
00:17:46,000 --> 00:17:50,920
 But certainly food is an important thing. It's the one

247
00:17:50,920 --> 00:17:53,000
 thing that we need every day.

248
00:17:53,000 --> 00:17:58,000
 And so at the least it acts as a good base reference.

249
00:17:58,000 --> 00:18:00,530
 If you're addicted to food, it's a sign you have an

250
00:18:00,530 --> 00:18:02,000
 addictive personality.

251
00:18:02,000 --> 00:18:04,970
 If you're addicted to good food, for example, or picky

252
00:18:04,970 --> 00:18:08,000
 about your food or that kind of thing.

253
00:18:08,000 --> 00:18:13,200
 If you obsess over food, eating too much or maybe eating

254
00:18:13,200 --> 00:18:14,000
 not enough,

255
00:18:14,000 --> 00:18:19,780
 mostly eating too much or eating unhealthy food because it

256
00:18:19,780 --> 00:18:22,000
 tastes better or so on,

257
00:18:22,000 --> 00:18:24,000
 or moderation and eating.

258
00:18:24,000 --> 00:18:28,630
 "Bhandan ca sayanasanam", another thing is that these show

259
00:18:28,630 --> 00:18:31,000
 what a simple life it is.

260
00:18:31,000 --> 00:18:35,000
 There's not complex rules or rituals in Buddhism.

261
00:18:35,000 --> 00:18:39,050
 Another way of looking at this is live this way and you've

262
00:18:39,050 --> 00:18:42,000
 got Buddhism in a nutshell.

263
00:18:42,000 --> 00:18:45,240
 "Bhandan ca sayanasanam" to live in a secluded area, to

264
00:18:45,240 --> 00:18:48,000
 have a secluded lodging.

265
00:18:48,000 --> 00:18:51,440
 Of course the ideal is to be in a forest or in a cave on a

266
00:18:51,440 --> 00:18:53,000
 mountain or something,

267
00:18:53,000 --> 00:18:57,000
 but even here in the city we've found a way.

268
00:18:57,000 --> 00:19:02,000
 It's quite remarkable, exceptional and much appreciated by

269
00:19:02,000 --> 00:19:07,000
 all of us.

270
00:19:07,000 --> 00:19:10,140
 We much appreciate all the support that has made this

271
00:19:10,140 --> 00:19:11,000
 possible.

272
00:19:11,000 --> 00:19:16,280
 Here we have rooms that are quiet and we have a secluded

273
00:19:16,280 --> 00:19:17,000
 dwelling here

274
00:19:17,000 --> 00:19:21,450
 where we don't sit around chatting all day and spend our

275
00:19:21,450 --> 00:19:24,000
 time cultivating goodness,

276
00:19:24,000 --> 00:19:31,000
 and good things.

277
00:19:31,000 --> 00:19:38,000
 "Adidjit deja aayogo" and the devotion to higher mindedness

278
00:19:38,000 --> 00:19:39,000
 or higher minds.

279
00:19:39,000 --> 00:19:43,000
 This means going beyond the mundane states of consciousness

280
00:19:43,000 --> 00:19:48,000
 or the ordinary states of greed, anger and delusion,

281
00:19:48,000 --> 00:19:55,000
 to have clarity of mind and quietude of mind,

282
00:19:55,000 --> 00:19:58,000
 and to cultivate wisdom in the super mundane state.

283
00:19:58,000 --> 00:20:03,000
 The jhanas as well and then the super mundane jhanas.

284
00:20:03,000 --> 00:20:10,000
 I'm engaged in the realization of nirvana.

285
00:20:10,000 --> 00:20:12,000
 It's quite simple.

286
00:20:12,000 --> 00:20:16,000
 This last one is another really good way of describing it.

287
00:20:16,000 --> 00:20:20,000
 Don't harm others, or this is the morality part,

288
00:20:20,000 --> 00:20:26,000
 don't harm others, don't speak ill of others.

289
00:20:26,000 --> 00:20:31,030
 Follow the rules of the community and the discipline of a

290
00:20:31,030 --> 00:20:34,000
 Buddhist.

291
00:20:34,000 --> 00:20:37,990
 No moderation in eating, so besides eating all you're doing

292
00:20:37,990 --> 00:20:41,000
 is cultivating goodness.

293
00:20:41,000 --> 00:20:48,420
 Just be careful of the necessities of life and stay in se

294
00:20:48,420 --> 00:20:50,000
clusion.

295
00:20:50,000 --> 00:20:54,000
 When you're in seclusion, cultivate higher mind states.

296
00:20:54,000 --> 00:20:59,290
 Don't just sit around and think or let yourself get

297
00:20:59,290 --> 00:21:01,000
 distracted.

298
00:21:01,000 --> 00:21:05,900
 This is the teaching of all the Buddha's eight dhanas and d

299
00:21:05,900 --> 00:21:08,000
hanasasana.

300
00:21:08,000 --> 00:21:11,000
 We have here in the Dighanikaya it says,

301
00:21:11,000 --> 00:21:15,490
 "Vipasi Bhagavara han sammasa buddho bhikkhusankhe iyawang

302
00:21:15,490 --> 00:21:18,000
 patimokangundi sate."

303
00:21:18,000 --> 00:21:21,000
 Vipasi is a Buddha in way in the past.

304
00:21:21,000 --> 00:21:28,000
 Who knows, maybe in some other epoch or eon or something.

305
00:21:28,000 --> 00:21:31,000
 A long time ago there was another Buddha.

306
00:21:31,000 --> 00:21:35,000
 So the idea is this is apparently what all Buddhas teach.

307
00:21:35,000 --> 00:21:38,190
 No matter what else they may teach or not teach, they

308
00:21:38,190 --> 00:21:39,000
 always teach this.

309
00:21:39,000 --> 00:21:44,000
 And this is what they teach to their assembly.

310
00:21:44,000 --> 00:21:47,000
 Either way, it's what we teach to our assembly.

311
00:21:47,000 --> 00:21:51,550
 And I think it's sort of an important milestone for us to

312
00:21:51,550 --> 00:21:52,000
 mark.

313
00:21:52,000 --> 00:21:58,100
 Well, we commemorate on this day the Awadapati moka on the

314
00:21:58,100 --> 00:22:00,000
 full moon of manga.

315
00:22:00,000 --> 00:22:02,000
 So there you go.

316
00:22:02,000 --> 00:22:06,000
 That's the dhamma for tonight. Thank you all for tuning in.

317
00:22:06,000 --> 00:22:08,000
 Wish you all good practice.

