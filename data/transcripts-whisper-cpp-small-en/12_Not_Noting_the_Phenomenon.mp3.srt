1
00:00:00,000 --> 00:00:05,270
 Not noting the phenomenon. Question. Is cultivating a habit

2
00:00:05,270 --> 00:00:07,840
 of noting that does not correspond to a

3
00:00:07,840 --> 00:00:11,100
 special mindfulness of the phenomenon a step in the wrong

4
00:00:11,100 --> 00:00:15,920
 direction? Answer. Yes, it would lead you in

5
00:00:15,920 --> 00:00:19,700
 the wrong direction. The practice is to note the phenomenon

6
00:00:19,700 --> 00:00:22,000
 or the experience that is the most

7
00:00:22,000 --> 00:00:25,880
 prominent in the present moment. Note it for as long as it

8
00:00:25,880 --> 00:00:28,640
 lasts, like hearing, hearing. And when

9
00:00:28,640 --> 00:00:32,320
 that experience seizes, go back to noting the rising and

10
00:00:32,320 --> 00:00:35,520
 falling of the abdomen. If you hear a

11
00:00:35,520 --> 00:00:39,710
 dog back without seeing it, do you note dog, dog or hearing

12
00:00:39,710 --> 00:00:42,800
, hearing? You cannot experience dog,

13
00:00:42,800 --> 00:00:46,330
 but you can experience hearing and that is a very important

14
00:00:46,330 --> 00:00:47,360
 distinction.

15
00:00:47,360 --> 00:00:52,350
 Hearing is an experience, but dog is a concept. Concepts

16
00:00:52,350 --> 00:00:55,360
 can be valid objects of meditation.

17
00:00:55,360 --> 00:00:59,070
 You could actually focus on the sound of a dog barking and

18
00:00:59,070 --> 00:01:02,320
 say dog, dog visualizing a dog in

19
00:01:02,320 --> 00:01:06,450
 your mind. That is a valid form of meditation, but it do

20
00:01:06,450 --> 00:01:09,840
 not lead you to an understanding of reality

21
00:01:09,840 --> 00:01:14,380
 because it is not real. It is a concept that arises in your

22
00:01:14,380 --> 00:01:18,000
 mind, a concept of a dog. The reality of it

23
00:01:18,000 --> 00:01:21,500
 is a vision in the mind, a sound at the ear and the

24
00:01:21,500 --> 00:01:24,960
 experience of hearing which really does occur.

25
00:01:24,960 --> 00:01:28,970
 The importance of using the words like hearing, hearing,

26
00:01:28,970 --> 00:01:31,360
 hearing is that it throws away the

27
00:01:31,360 --> 00:01:35,390
 concept. In the beginning it may not, as the mind may still

28
00:01:35,390 --> 00:01:38,160
 be caught up in concepts, but eventually

29
00:01:38,160 --> 00:01:42,670
 the mind lets go of the concept because again and again you

30
00:01:42,670 --> 00:01:44,880
 are saying no, it is not a dog,

31
00:01:44,880 --> 00:01:49,380
 it is just hearing. No, it is not a dog, it is just hearing

32
00:01:49,380 --> 00:01:51,440
. The mind would try to respond,

33
00:01:51,440 --> 00:01:56,050
 but it is a dog. No, it is just hearing. But it is a dog.

34
00:01:56,050 --> 00:02:00,000
 No, it is just hearing. But I like the sound.

35
00:02:00,000 --> 00:02:04,140
 No, it is just sound. Eventually the mind will become

36
00:02:04,140 --> 00:02:07,520
 accustomed to being the actual experience

37
00:02:07,520 --> 00:02:11,630
 and it will concede to that, oh yes, it is just sound for

38
00:02:11,630 --> 00:02:14,240
 just hearing. This is the importance

39
00:02:14,240 --> 00:02:18,520
 of the correct practice. If you say dog, dog, dog, you will

40
00:02:18,520 --> 00:02:20,480
 never get to that point where you

41
00:02:20,480 --> 00:02:24,920
 experience reality for what it is, it is very important to

42
00:02:24,920 --> 00:02:27,600
 know the difference between concepts

43
00:02:27,600 --> 00:02:31,760
 and reality. People who have never practiced intensive

44
00:02:31,760 --> 00:02:34,480
 meditation have a hard time understanding

45
00:02:34,480 --> 00:02:38,530
 this difference. When thought about experiential reality

46
00:02:38,530 --> 00:02:40,960
 they become confused saying, what do you

47
00:02:40,960 --> 00:02:44,250
 mean this is just a concept? What do you mean this is not

48
00:02:44,250 --> 00:02:47,200
 reality? If you have never really spent time

49
00:02:47,200 --> 00:02:51,170
 experiencing reality objectively without conceiving

50
00:02:51,170 --> 00:02:54,400
 anything of it, it is hard to tell the difference

51
00:02:54,400 --> 00:02:58,920
 between reality and conception. For a person who has spent

52
00:02:58,920 --> 00:03:01,280
 time meditating mindfully on the other

53
00:03:01,280 --> 00:03:04,400
 hand, it is hard to understand how you would not tell the

54
00:03:04,400 --> 00:03:07,840
 difference between the two. This question

55
00:03:07,840 --> 00:03:12,170
 also relates to the broad subject of why we have a wedge at

56
00:03:12,170 --> 00:03:15,120
 all and why we use the noting technique.

57
00:03:15,120 --> 00:03:19,470
 We use the noting technique because of our understanding of

58
00:03:19,470 --> 00:03:21,600
 the word sati which is usually

59
00:03:21,600 --> 00:03:25,810
 translated as mindfulness. The word sati literally means

60
00:03:25,810 --> 00:03:28,880
 remembering or recalling. Sati means

61
00:03:28,880 --> 00:03:33,110
 remembering one's present experience for what it is without

62
00:03:33,110 --> 00:03:36,080
 reaction or interpretation of any sort

63
00:03:36,080 --> 00:03:40,000
 or mental conceiving whatsoever. The Buddha himself

64
00:03:40,000 --> 00:03:43,120
 explained sati as when walking one knows I am

65
00:03:43,120 --> 00:03:47,200
 walking, when standing one knows I am standing, when there

66
00:03:47,200 --> 00:03:49,520
 is a painful feeling one knows there

67
00:03:49,520 --> 00:03:53,610
 is a painful feeling. We use this sort of reminder to keep

68
00:03:53,610 --> 00:03:56,560
 from forgetting about our experiences

69
00:03:56,560 --> 00:04:00,570
 and getting lost in the conceptual world of our minds. Rem

70
00:04:00,570 --> 00:04:03,760
inding ourselves of reality in this way

71
00:04:03,760 --> 00:04:07,320
 allows us to dwell as the Buddha said not clinging to

72
00:04:07,320 --> 00:04:10,720
 anything, not being dependent on anything,

73
00:04:10,720 --> 00:04:14,370
 leaving behind our mental constructs remembering

74
00:04:14,370 --> 00:04:18,880
 experiences just as they are. What word one uses

75
00:04:18,880 --> 00:04:22,920
 to remember one's experiences as they are isn't really all

76
00:04:22,920 --> 00:04:25,600
 that important as long as it has these

77
00:04:25,600 --> 00:04:29,870
 three effects. One, the effect of bringing the mind closer

78
00:04:29,870 --> 00:04:33,280
 to the object as it is. Two, keeping it on

79
00:04:33,280 --> 00:04:37,820
 the object and three, preventing proliferation based on the

80
00:04:37,820 --> 00:04:40,880
 object. If you do not use a word to remind

81
00:04:40,880 --> 00:04:44,500
 yourself in this way, your mind will be hard pressed to

82
00:04:44,500 --> 00:04:47,360
 fully fix on the object. This is why people

83
00:04:47,360 --> 00:04:50,540
 practice mantra meditation in general, even tranquility

84
00:04:50,540 --> 00:04:54,880
 based mantras like buddho or om because

85
00:04:54,880 --> 00:04:58,840
 the word focuses the mind on the object whatever the object

86
00:04:58,840 --> 00:05:01,520
 might be, spiritual or mundane. When

87
00:05:01,520 --> 00:05:05,400
 you say to yourself pain, pain, the mind is drawn to the

88
00:05:05,400 --> 00:05:08,400
 pain. It gravitates towards the pain

89
00:05:08,400 --> 00:05:12,430
 naturally. For some people if they feel pain they will say

90
00:05:12,430 --> 00:05:15,520
 happy, happy, happy because they want to

91
00:05:15,520 --> 00:05:20,090
 feel happy or they will say no pain, no pain, no pain. As

92
00:05:20,090 --> 00:05:23,040
 they want to have no pain ask yourself

93
00:05:23,040 --> 00:05:26,330
 what would that do? Would that bring the mind to see the

94
00:05:26,330 --> 00:05:29,280
 pain clearly for what it is? No. What it

95
00:05:29,280 --> 00:05:33,370
 does is create aversion in the mind and a tendency to

96
00:05:33,370 --> 00:05:37,040
 recoil from pain. It is this sort of aversive

97
00:05:37,040 --> 00:05:41,440
 attitude that leads some people to take medication or drugs

98
00:05:41,440 --> 00:05:44,480
 to escape the experiences. Some people

99
00:05:44,480 --> 00:05:48,210
 have the idea that meditation is going to be another drug

100
00:05:48,210 --> 00:05:50,640
 that allows one to get away from pain.

101
00:05:50,640 --> 00:05:54,420
 If you use a word that is not related at all it is just

102
00:05:54,420 --> 00:05:58,080
 going to create confusion. I doubt the

103
00:05:58,080 --> 00:06:02,180
 questioner is seriously considering that as a meditation

104
00:06:02,180 --> 00:06:04,480
 practice like you feel pain and say

105
00:06:04,480 --> 00:06:08,130
 apple, apple, apple or something. You might use the

106
00:06:08,130 --> 00:06:11,360
 functional words or phrase like no pain to get

107
00:06:11,360 --> 00:06:15,470
 your mind onto something else. A common secular meditation

108
00:06:15,470 --> 00:06:17,840
 technique if you are angry is to say

109
00:06:17,840 --> 00:06:25,900
 to yourself 100, 99, 98, 97, 96, 95 counting down from 100.

110
00:06:25,900 --> 00:06:29,040
 Eventually you forget about the anger

111
00:06:29,040 --> 00:06:32,470
 so it is a valid means of getting rid of the anger but it

112
00:06:32,470 --> 00:06:34,960
 does not teach you about the anger.

113
00:06:34,960 --> 00:06:38,250
 This is the crucial difference between mindfulness

114
00:06:38,250 --> 00:06:41,520
 meditation and many other mantra meditation

115
00:06:41,520 --> 00:06:45,540
 practices. In mindfulness meditation we are using the

116
00:06:45,540 --> 00:06:48,720
 mantra to bring us closer to the object

117
00:06:48,720 --> 00:06:59,200
 as opposed to avoiding the mundane reality.

