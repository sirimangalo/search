1
00:00:00,000 --> 00:00:23,880
 Okay, good evening everyone.

2
00:00:23,880 --> 00:00:40,880
 I'm going to be doing live January 18th, 2016.

3
00:00:40,880 --> 00:00:52,850
 I'd like to have a set of verses, I think, from the Singaro

4
00:00:52,850 --> 00:00:55,880
 Vada Sapte.

5
00:00:55,880 --> 00:01:00,850
 These are the dhammas that are lynch-pil, dhammas that hold

6
00:01:00,850 --> 00:01:02,880
 the world together,

7
00:01:02,880 --> 00:01:12,880
 that allow the world to turn smoothly, which just means

8
00:01:12,880 --> 00:01:16,880
 keep society running smoothly,

9
00:01:16,880 --> 00:01:21,880
 keep people at peace with each other.

10
00:01:21,880 --> 00:01:30,080
 Man, if we could keep these dhammas as a society, right, as

11
00:01:30,080 --> 00:01:35,880
 any society, that would be something.

12
00:01:35,880 --> 00:01:38,880
 Wise and disciplined.

13
00:01:38,880 --> 00:01:47,280
 Wisdom and discipline are two sides of the Buddhist coin, I

14
00:01:47,280 --> 00:01:53,880
 guess, two sides of the Buddhist practice.

15
00:01:53,880 --> 00:02:03,760
 The dhamma and the vinaya, the dhamma is about wisdom and

16
00:02:03,760 --> 00:02:09,880
 the vinaya is about discipline.

17
00:02:09,880 --> 00:02:15,360
 I just came out of a Buddhist studies class, and I was

18
00:02:15,360 --> 00:02:21,880
 studying the Mahayana, getting in a little bit of trouble,

19
00:02:21,880 --> 00:02:22,880
 not exactly trouble,

20
00:02:22,880 --> 00:02:29,880
 but a little bit contentious because they're saying things

21
00:02:29,880 --> 00:02:33,880
 that I don't really agree with.

22
00:02:33,880 --> 00:02:39,760
 It's not even a matter of disagreeing with the doctor, I'm

23
00:02:39,760 --> 00:02:44,880
 concerned that it's probably going to be, I don't know,

24
00:02:44,880 --> 00:02:58,420
 the same theories that just don't seem to add up, like

25
00:02:58,420 --> 00:03:08,880
 saying that the vinaya itself appears to have been set up

26
00:03:08,880 --> 00:03:17,210
 for a rather cushy way of life, not rather, but a fairly

27
00:03:17,210 --> 00:03:20,880
 comfortable way of life,

28
00:03:20,880 --> 00:03:26,030
 and that it was only with the Mahayana, at the time of the

29
00:03:26,030 --> 00:03:29,880
 Mahayana, that they started reforming.

30
00:03:29,880 --> 00:03:35,850
 I think it sounded like the reason for doing this sort of

31
00:03:35,850 --> 00:03:41,870
 theory is in order to find a parallel with the Christian

32
00:03:41,870 --> 00:03:43,880
 religion,

33
00:03:43,880 --> 00:03:51,230
 but it's a real stretch. I guess the theory is that, well,

34
00:03:51,230 --> 00:03:52,880
 if it happened in the Christian religion,

35
00:03:52,880 --> 00:03:54,470
 it must have happened in the same way in the Buddhist

36
00:03:54,470 --> 00:03:57,880
 religion, but it really wasn't.

37
00:03:57,880 --> 00:04:03,650
 It's hard to imagine that in the early times, or that the

38
00:04:03,650 --> 00:04:08,880
 earliest vinaya was somehow kind of, well, institutional,

39
00:04:08,880 --> 00:04:13,100
 and I mean, some of the rules do seem a little bit

40
00:04:13,100 --> 00:04:14,880
 institutional.

41
00:04:14,880 --> 00:04:19,380
 He was mentioning that when a monk dies, there are specific

42
00:04:19,380 --> 00:04:22,880
 rules for where his belongings are supposed to go,

43
00:04:22,880 --> 00:04:29,890
 but I think the problem is that there are other vinayas,

44
00:04:29,890 --> 00:04:31,880
 like other Buddhist groups,

45
00:04:31,880 --> 00:04:39,210
 whose vinayas survived, and they have a lot of stuff about

46
00:04:39,210 --> 00:04:43,880
 money and what to do with it,

47
00:04:43,880 --> 00:04:47,910
 and when the king gives you a lau into that kind of thing,

48
00:04:47,910 --> 00:04:48,880
 and it was confusing to me,

49
00:04:48,880 --> 00:04:50,890
 because I'm trying to figure out which vinaya this was that

50
00:04:50,890 --> 00:04:56,880
 we were talking about. It didn't sound much like ours.

51
00:04:56,880 --> 00:05:03,910
 But the vinaya is very much about discipline and the vinaya

52
00:05:03,910 --> 00:05:05,880
 as we have it.

53
00:05:05,880 --> 00:05:11,200
 The problem is it's not very well followed at all, but the

54
00:05:11,200 --> 00:05:17,880
 vinaya itself is quite austere, I think.

55
00:05:17,880 --> 00:05:22,570
 I mean, when I try to keep the rules, people say, "Oh, he's

56
00:05:22,570 --> 00:05:23,880
 an austere monk.

57
00:05:23,880 --> 00:05:27,230
 There's this monk who's trying to pass me off as a dutanga

58
00:05:27,230 --> 00:05:27,880
 monk."

59
00:05:27,880 --> 00:05:31,650
 He says, "Oh, this is a dutanga monk." And I said, "I don't

60
00:05:31,650 --> 00:05:32,880
 keep any other dutangas."

61
00:05:32,880 --> 00:05:38,140
 Dutangas being these actually fairly, well, reasonably

62
00:05:38,140 --> 00:05:42,860
 extreme practices that are allowed and praised, but are not

63
00:05:42,860 --> 00:05:43,880
 required.

64
00:05:43,880 --> 00:05:47,980
 I mean, just by keeping the rules, people start to call you

65
00:05:47,980 --> 00:05:48,880
 austere.

66
00:05:48,880 --> 00:05:53,880
 In Thai they say, "tukh, kring, tukh, king."

67
00:05:53,880 --> 00:05:56,880
 "Tukh" means to carry or to hold.

68
00:05:56,880 --> 00:06:06,430
 "King" means, "kring" means very severe, very harsh, very

69
00:06:06,430 --> 00:06:10,880
 rigidly, strictly.

70
00:06:10,880 --> 00:06:15,950
 "Kring" means strictly. I'm thinking, "You know, really, it

71
00:06:15,950 --> 00:06:16,880
's just about keeping rules.

72
00:06:16,880 --> 00:06:24,970
 It's not strict or it's not enough. "King" means kind of

73
00:06:24,970 --> 00:06:28,430
 severe, but it's funny because that's the flavor of the

74
00:06:28,430 --> 00:06:33,880
 Buddhist rules for monks.

75
00:06:33,880 --> 00:06:39,650
 Anyway, as far as this verse goes, for laypeople it's quite

76
00:06:39,650 --> 00:06:40,880
 similar.

77
00:06:40,880 --> 00:06:44,900
 I mean, you heard in your general term, a lot of Buddhists

78
00:06:44,900 --> 00:06:47,880
 live a fairly simple life, I would say.

79
00:06:47,880 --> 00:06:50,720
 They live as a layperson. They might be married enough kids

80
00:06:50,720 --> 00:07:00,370
, but there's a sense of contentment and lack of greed and

81
00:07:00,370 --> 00:07:02,880
 ambition and that kind of thing.

82
00:07:02,880 --> 00:07:08,200
 Being kind and intelligent. I don't know what the words

83
00:07:08,200 --> 00:07:10,880
 there are. I don't have the Pali with me.

84
00:07:10,880 --> 00:07:16,780
 "Humble and free from pride." One like this will earn or

85
00:07:16,780 --> 00:07:17,880
 win respect.

86
00:07:17,880 --> 00:07:22,170
 So that's interesting. Intelligence is important, quite

87
00:07:22,170 --> 00:07:25,880
 shortly, if the Pali is a proper translation.

88
00:07:25,880 --> 00:07:31,480
 Intelligence is important, especially for laypeople, proper

89
00:07:31,480 --> 00:07:33,880
 monks as well, to teach.

90
00:07:33,880 --> 00:07:38,970
 It's important not to shun these things. These are skills

91
00:07:38,970 --> 00:07:40,880
 and skills are important.

92
00:07:40,880 --> 00:07:47,450
 It's important that we be careful not to get lazy. He talks

93
00:07:47,450 --> 00:07:48,880
 about lazy below.

94
00:07:48,880 --> 00:07:55,410
 Humility and freedom from pride are, of course, essential

95
00:07:55,410 --> 00:07:56,880
 to the past.

96
00:07:56,880 --> 00:08:00,040
 It's not fake humility. You have to be humble enough to

97
00:08:00,040 --> 00:08:01,880
 know that you're arrogant.

98
00:08:01,880 --> 00:08:14,520
 Fake humility is often bad. A lot of fake humility, where

99
00:08:14,520 --> 00:08:21,880
 people either speak poorly of themselves or...

100
00:08:21,880 --> 00:08:30,880
 It's not to fool others, but in a sense fooling yourself.

101
00:08:30,880 --> 00:08:32,990
 It's important to realize if you're arrogant, to realize

102
00:08:32,990 --> 00:08:34,880
 that you're arrogant.

103
00:08:34,880 --> 00:08:37,770
 True humility is about accepting, not accepting, but

104
00:08:37,770 --> 00:08:43,880
 recognizing that, objecting and freeing yourself from it.

105
00:08:43,880 --> 00:08:48,870
 That's an important distinction because the Buddha appears

106
00:08:48,870 --> 00:08:51,880
 to be somewhat arrogant at times.

107
00:08:51,880 --> 00:08:56,880
 Monks as well in the canon and Buddhist teachers.

108
00:08:56,880 --> 00:09:01,270
 I mean, my teacher often appears... It's not arrogant, and

109
00:09:01,270 --> 00:09:08,880
 if you're at all in tune with wisdom and understanding,

110
00:09:08,880 --> 00:09:15,170
 it doesn't really seem arrogant, but it's bold and telling

111
00:09:15,170 --> 00:09:16,880
 it like it is.

112
00:09:16,880 --> 00:09:23,880
 Not afraid of hurting people's feelings necessarily.

113
00:09:23,880 --> 00:09:29,300
 Not afraid to tell people how good you are at times when it

114
00:09:29,300 --> 00:09:32,880
's necessary, like when the Buddha would say,

115
00:09:32,880 --> 00:09:37,020
 "He's enlightened, that there's no one like him in the

116
00:09:37,020 --> 00:09:43,880
 world." The truth of it is...

117
00:09:43,880 --> 00:09:50,110
 It makes it a powerful statement, not a statement of

118
00:09:50,110 --> 00:09:51,880
 arrogance.

119
00:09:51,880 --> 00:09:55,950
 So being humble and free from pride doesn't necessarily

120
00:09:55,950 --> 00:09:56,880
 appear to be...

121
00:09:56,880 --> 00:10:05,380
 Not at all times, anyway, appear to be certainly not self-

122
00:10:05,380 --> 00:10:10,880
effacing or negating or something.

123
00:10:10,880 --> 00:10:15,880
 Just when people... If someone asks you to do something,

124
00:10:15,880 --> 00:10:20,880
 you don't say, "I'm above that," or, "How dare they?"

125
00:10:20,880 --> 00:10:24,770
 If someone insults you or attacks you or someone criticizes

126
00:10:24,770 --> 00:10:28,880
 you, you say, "Thank you," that kind of thing.

127
00:10:28,880 --> 00:10:31,880
 The ability to accept it and internalize it.

128
00:10:31,880 --> 00:10:37,270
 I remember once an example that really humbled me. I mean,

129
00:10:37,270 --> 00:10:42,880
 not to say, but it was really just cringe-worthy, really.

130
00:10:42,880 --> 00:10:48,350
 Once there was this big ceremony, like really, at my

131
00:10:48,350 --> 00:10:51,880
 monastery. However, we go up to the main temple,

132
00:10:51,880 --> 00:10:58,310
 and where there's one of the fairly well-respected relics

133
00:10:58,310 --> 00:10:59,880
 of the Buddha.

134
00:10:59,880 --> 00:11:03,190
 There's a lot of people say in Thailand, "Everyone says

135
00:11:03,190 --> 00:11:06,330
 they have relics of the Buddha, but this is one that has a

136
00:11:06,330 --> 00:11:06,880
 history,

137
00:11:06,880 --> 00:11:11,870
 like 700-year history, that they've sort of kept track of

138
00:11:11,870 --> 00:11:13,880
 it, so it's an old one.

139
00:11:13,880 --> 00:11:16,980
 So it may actually be a piece that they've been disposed of

140
00:11:16,980 --> 00:11:17,880
, who knows?

141
00:11:17,880 --> 00:11:24,070
 And so they take it out every so often, and they bathe it.

142
00:11:24,070 --> 00:11:25,880
 Everybody gets to pour water over it.

143
00:11:25,880 --> 00:11:28,880
 It's a very old ritual.

144
00:11:28,880 --> 00:11:34,050
 And so they took it out, and right in front of the big, in

145
00:11:34,050 --> 00:11:38,880
 the main Buddha image, where they keep the relic,

146
00:11:38,880 --> 00:11:44,300
 Ajahn, my teacher, there's a seat prepared for him, right

147
00:11:44,300 --> 00:11:45,880
 in the middle.

148
00:11:45,880 --> 00:11:52,930
 And I was like with him for some reason, probably helping

149
00:11:52,930 --> 00:11:53,880
 him.

150
00:11:53,880 --> 00:12:00,020
 And I was trying to get out of his way, and I walked in

151
00:12:00,020 --> 00:12:03,510
 front of him just as he moved to go and sit down in his

152
00:12:03,510 --> 00:12:03,880
 seat.

153
00:12:03,880 --> 00:12:07,310
 And so he stopped, and I stopped, and he looked at me, and

154
00:12:07,310 --> 00:12:09,880
 he offered me the seat, basically.

155
00:12:09,880 --> 00:12:12,630
 I can't remember if he offered me the seat, or he said, "Go

156
00:12:12,630 --> 00:12:13,880
 ahead, basically."

157
00:12:13,880 --> 00:12:19,350
 It was like I made the worst possible move in front of this

158
00:12:19,350 --> 00:12:23,880
 whole group of people, just got in his way.

159
00:12:23,880 --> 00:12:30,370
 And it was so like to just say, "Go ahead if you want to go

160
00:12:30,370 --> 00:12:30,880
."

161
00:12:30,880 --> 00:12:33,880
 I just arrived at the Kita Hoda.

162
00:12:33,880 --> 00:12:39,880
 Here I am, messing up this ceremony.

163
00:12:39,880 --> 00:12:43,300
 Maybe it doesn't sound that impressive, but it was at the

164
00:12:43,300 --> 00:12:43,880
 time.

165
00:12:43,880 --> 00:12:50,050
 I don't remember exactly how it happened, but it was really

166
00:12:50,050 --> 00:12:50,880
 bad.

167
00:12:50,880 --> 00:12:54,670
 But he never, when people praise him, it's as though he

168
00:12:54,670 --> 00:12:55,880
 doesn't hear.

169
00:12:55,880 --> 00:13:02,920
 There's something about that in the text, "Equal in praise

170
00:13:02,920 --> 00:13:03,880
 and blame."

171
00:13:03,880 --> 00:13:05,880
 It's as though they don't even hear it.

172
00:13:05,880 --> 00:13:12,120
 When someone blames them, it's also as though they don't

173
00:13:12,120 --> 00:13:13,880
 even hear it.

174
00:13:13,880 --> 00:13:17,880
 Yeah.

175
00:13:17,880 --> 00:13:23,880
 Lots of stories of ASEAN-THONG.

176
00:13:23,880 --> 00:13:34,880
 Rising early and shunningly easiness, whether you go.

177
00:13:34,880 --> 00:13:44,880
 Sleep less, eat less, talk less, practice more.

178
00:13:44,880 --> 00:13:51,880
 So that's what ASEAN-THONG is.

179
00:13:51,880 --> 00:13:55,690
 "The remaining calm in times of strife, faultless in

180
00:13:55,690 --> 00:14:00,020
 conduct and clever elections, one like this will be in

181
00:14:00,020 --> 00:14:01,880
 respect."

182
00:14:01,880 --> 00:14:05,700
 Being able to make friends and keep them, welcoming others

183
00:14:05,700 --> 00:14:06,880
 and sharing with them.

184
00:14:06,880 --> 00:14:11,880
 These are just great general advice people.

185
00:14:11,880 --> 00:14:15,070
 There's so much like this in the Tipitika that we often don

186
00:14:15,070 --> 00:14:19,880
't hear about because we focus very much on core elements,

187
00:14:19,880 --> 00:14:21,880
 like meditation, "I want to meditate."

188
00:14:21,880 --> 00:14:30,490
 We miss sometimes these gems that are like ornaments of

189
00:14:30,490 --> 00:14:31,880
 dumb.

190
00:14:31,880 --> 00:14:36,730
 So being able to make friends and keep them, it is a skill,

191
00:14:36,730 --> 00:14:37,880
 you know?

192
00:14:37,880 --> 00:14:44,880
 If a person is able to do the best they can with everyone

193
00:14:44,880 --> 00:14:45,880
 and to make lots of friends.

194
00:14:45,880 --> 00:14:50,050
 Not having any friends is not a problem, but it is a sign,

195
00:14:50,050 --> 00:14:50,880
 right?

196
00:14:50,880 --> 00:14:57,400
 It's often a sign of some problem if you don't have friends

197
00:14:57,400 --> 00:14:57,880
.

198
00:14:57,880 --> 00:15:02,280
 Welcoming others and sharing with them, sharing with others

199
00:15:02,280 --> 00:15:03,880
 is very important.

200
00:15:03,880 --> 00:15:07,880
 Welcoming as well.

201
00:15:07,880 --> 00:15:12,880
 You know, in some societies when a traveler comes,

202
00:15:12,880 --> 00:15:17,820
 the first thing they do is get them something to eat or

203
00:15:17,820 --> 00:15:21,880
 drink, bring them something as a gift.

204
00:15:21,880 --> 00:15:23,880
 That's just a custom.

205
00:15:23,880 --> 00:15:28,880
 I think in Judaism there's something about that.

206
00:15:28,880 --> 00:15:31,880
 And I think Christianity picks it up as well.

207
00:15:31,880 --> 00:15:36,880
 I know many cultures who have come to their village, their

208
00:15:36,880 --> 00:15:36,880
 town,

209
00:15:36,880 --> 00:15:41,870
 their village more likely, the town probably not so much,

210
00:15:41,870 --> 00:15:50,880
 but villages they will immediately offer you hospitality.

211
00:15:50,880 --> 00:15:54,570
 Being generous and kindly in speech, doing favors for

212
00:15:54,570 --> 00:15:57,880
 others and treating all alike.

213
00:15:57,880 --> 00:16:01,880
 Now, not so easy, huh?

214
00:16:01,880 --> 00:16:05,100
 But doing favors for others is something we should be

215
00:16:05,100 --> 00:16:06,880
 happily delighted to do.

216
00:16:06,880 --> 00:16:08,880
 We should delight in goodness.

217
00:16:08,880 --> 00:16:12,880
 We should do our best to delight in these things.

218
00:16:12,880 --> 00:16:17,550
 This is where we should find happiness in doing good things

219
00:16:17,550 --> 00:16:17,880
.

220
00:16:17,880 --> 00:16:19,880
 Feeling all alike is very difficult.

221
00:16:19,880 --> 00:16:23,880
 It's easy to become partial to certain people.

222
00:16:23,880 --> 00:16:26,880
 Partial to people because they're family,

223
00:16:26,880 --> 00:16:30,880
 or partial to people because they're charming or beautiful,

224
00:16:30,880 --> 00:16:34,200
 because they're the same skin color or the same culture or

225
00:16:34,200 --> 00:16:37,880
 the same religion as you.

226
00:16:37,880 --> 00:16:40,880
 Very difficult to treat all people alike.

227
00:16:40,880 --> 00:16:43,880
 Buddha said he loved Devadatta as much as he loved Rahu.

228
00:16:43,880 --> 00:16:45,880
 Rahu loves his son.

229
00:16:45,880 --> 00:16:50,060
 Devadatta was his cousin who tried to kill him several

230
00:16:50,060 --> 00:16:50,880
 times.

231
00:16:50,880 --> 00:16:53,880
 So the story goes.

232
00:16:53,880 --> 00:16:58,880
 These are things that make the world turn smoothly.

233
00:16:58,880 --> 00:17:02,880
 Force remembering, worth keeping in mind.

234
00:17:02,880 --> 00:17:07,880
 Sad.

235
00:17:07,880 --> 00:17:11,880
 Is anybody out there?

236
00:17:11,880 --> 00:17:16,880
 Uh-oh, is nobody's audio working?

237
00:17:16,880 --> 00:17:19,880
 Maybe it's not even recording.

238
00:17:19,880 --> 00:17:21,880
 Test.

239
00:17:21,880 --> 00:17:24,880
 I think it's recording.

240
00:17:24,880 --> 00:17:28,880
 Maybe it's very quiet.

241
00:17:28,880 --> 00:17:32,880
 Test.

242
00:17:32,880 --> 00:17:41,880
 Thanks Anna.

243
00:17:41,880 --> 00:17:45,880
 Anna says audio is working fine.

244
00:17:45,880 --> 00:17:47,880
 Who else is out there?

245
00:17:47,880 --> 00:17:49,880
 Murat Khan,

246
00:17:49,880 --> 00:17:50,880
 Wayne,

247
00:17:50,880 --> 00:17:54,880
 and Mike Gohmball and Christopher Stiller.

248
00:17:54,880 --> 00:17:56,880
 Wayne.

249
00:17:56,880 --> 00:18:05,880
 Hi everybody.

250
00:18:05,880 --> 00:18:07,880
 Well I'm a bit quiet.

251
00:18:07,880 --> 00:18:09,880
 That's just how I am.

252
00:18:09,880 --> 00:18:11,880
 But I do have the volume,

253
00:18:11,880 --> 00:18:14,420
 this neat app that actually lets me set the recording

254
00:18:14,420 --> 00:18:14,880
 volume so

255
00:18:14,880 --> 00:18:16,880
 I can go a little bit louder.

256
00:18:16,880 --> 00:18:21,880
 See but then when I actually talk up it gets very loud.

257
00:18:21,880 --> 00:18:23,880
 And it can actually clip,

258
00:18:23,880 --> 00:18:30,180
 which means be too loud for the mic and it becomes

259
00:18:30,180 --> 00:18:32,880
 distorted.

260
00:18:32,880 --> 00:18:34,880
 Hi Aurora,

261
00:18:34,880 --> 00:18:37,880
 Michael,

262
00:18:37,880 --> 00:18:39,880
 Sabai-di.

263
00:18:39,880 --> 00:18:45,880
 Can't remember who that is.

264
00:18:45,880 --> 00:18:47,880
 Austin.

265
00:18:47,880 --> 00:18:59,880
 I was thinking tonight, it's probably crazy but...

266
00:18:59,880 --> 00:19:02,880
 There's this,

267
00:19:02,880 --> 00:19:05,880
 I don't know if any of you are familiar with Second Life.

268
00:19:05,880 --> 00:19:10,880
 I used to teach on Second Life.

269
00:19:10,880 --> 00:19:16,880
 And, oh, Sachika.

270
00:19:16,880 --> 00:19:20,880
 Why does Sachika have a Thai name?

271
00:19:20,880 --> 00:19:29,880
 What if we were to meet in Second Life?

272
00:19:29,880 --> 00:19:32,880
 What if we had a...

273
00:19:32,880 --> 00:19:36,880
 What if like every Saturday I did a thing on Second Life?

274
00:19:36,880 --> 00:19:40,880
 It's just a really neat platform for meeting.

275
00:19:40,880 --> 00:19:43,880
 Because it gives you a sense of meeting.

276
00:19:43,880 --> 00:19:46,880
 It gives you a sense of how many people are there.

277
00:19:46,880 --> 00:19:49,880
 It gives you a sense of the sort of people that are there.

278
00:19:49,880 --> 00:19:52,880
 It's got group audio, which is really awesome.

279
00:19:52,880 --> 00:19:54,880
 And it's directional audio.

280
00:19:54,880 --> 00:19:57,880
 So you really hear people where they are,

281
00:19:57,880 --> 00:20:00,880
 in the direction that they are.

282
00:20:00,880 --> 00:20:03,880
 Now there's a Buddha center,

283
00:20:03,880 --> 00:20:05,880
 a Buddha center there.

284
00:20:05,880 --> 00:20:07,880
 Maybe that's what we should do.

285
00:20:07,880 --> 00:20:09,880
 I'll just tell people to go there on Saturdays

286
00:20:09,880 --> 00:20:13,880
 and I'll get it set up on my computer.

287
00:20:13,880 --> 00:20:21,880
 We were just talking about ways to build community

288
00:20:21,880 --> 00:20:25,880
 and to have a weekly meeting.

289
00:20:25,880 --> 00:20:29,880
 I'm not convinced that the hangout is the best way.

290
00:20:29,880 --> 00:20:38,880
 But the Buddha center is kind of interesting.

291
00:20:38,880 --> 00:20:41,880
 And there are people still on it.

292
00:20:41,880 --> 00:20:43,880
 I think, actually I don't know.

293
00:20:43,880 --> 00:20:45,880
 I'm on their board. If you go to the Buddha center,

294
00:20:45,880 --> 00:20:47,880
 I'm sure it was one of their teachers.

295
00:20:47,880 --> 00:20:49,880
 But I haven't taught in there for years.

296
00:20:49,880 --> 00:20:53,880
 I can just get in touch with them.

297
00:20:53,880 --> 00:20:55,880
 Maybe I'll do that for Saturday.

298
00:20:55,880 --> 00:20:58,880
 See how it goes.

299
00:20:58,880 --> 00:21:13,880
 [silence]

300
00:21:13,880 --> 00:21:15,880
 Of course, that's the thing.

301
00:21:15,880 --> 00:21:17,880
 For some people it's an ogle

302
00:21:17,880 --> 00:21:20,880
 because Second Life requires,

303
00:21:20,880 --> 00:21:23,880
 I think it requires, it used to require special graphics,

304
00:21:23,880 --> 00:21:25,880
 but probably nowadays it doesn't.

305
00:21:25,880 --> 00:21:30,880
 It requires a little bit of internet know-how

306
00:21:30,880 --> 00:21:32,880
 or computer know-how.

307
00:21:32,880 --> 00:21:34,880
 If you don't know much about computers,

308
00:21:34,880 --> 00:21:36,880
 it's not easy to get on.

309
00:21:36,880 --> 00:21:39,880
 It's not that hard.

310
00:21:39,880 --> 00:21:44,880
 It's not that hard, I think.

311
00:21:44,880 --> 00:21:51,880
 A lot of Buddhists on Second Life,

312
00:21:51,880 --> 00:21:55,880
 or the Buddha Center, it used to be anyway.

313
00:21:55,880 --> 00:21:58,880
 Not a lot, but a good number.

314
00:21:58,880 --> 00:22:03,880
 Of course, we often, at times,

315
00:22:03,880 --> 00:22:06,880
 got strange people coming in.

316
00:22:06,880 --> 00:22:09,880
 People who are half naked.

317
00:22:09,880 --> 00:22:10,880
 We had a bear once.

318
00:22:10,880 --> 00:22:14,880
 I was getting a top and a bear walked up his side.

319
00:22:14,880 --> 00:22:19,880
 But really neat platform because of the audio.

320
00:22:19,880 --> 00:22:21,880
 It's two-way audio.

321
00:22:21,880 --> 00:22:24,880
 So you can give a talk and everyone hears you.

322
00:22:24,880 --> 00:22:29,880
 And then they can talk back to you and everyone hears them.

323
00:22:29,880 --> 00:22:37,880
 Anyway, for tonight, this is it.

324
00:22:37,880 --> 00:22:39,880
 Thank you all for tuning in.

325
00:22:39,880 --> 00:22:41,880
 Have a good night.

326
00:22:41,880 --> 00:23:00,880
 [

