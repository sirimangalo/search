1
00:00:00,000 --> 00:00:04,500
 And then with regard to books, means responsibility for the

2
00:00:04,500 --> 00:00:05,360
 scriptures.

3
00:00:05,360 --> 00:00:15,360
 At the end of the page, the elder asked him,

4
00:00:15,360 --> 00:00:18,000
 "How are you in the scriptures, friend?

5
00:00:18,000 --> 00:00:22,120
 I am studying the Majjhima Venerable Sah."

6
00:00:22,120 --> 00:00:23,520
 Now,

7
00:00:24,880 --> 00:00:29,550
 the Pali word here means, "I am familiar with Majjhima Nik

8
00:00:29,550 --> 00:00:30,320
aya."

9
00:00:30,320 --> 00:00:32,160
 Not just studying.

10
00:00:32,160 --> 00:00:34,160
 I am familiar with that.

11
00:00:34,160 --> 00:00:37,600
 I am well acquainted with Majjhima Nikaya.

12
00:00:37,600 --> 00:00:48,640
 And then,

13
00:00:48,640 --> 00:00:50,880
 on the next page,

14
00:00:51,200 --> 00:00:56,080
 "When a man is still learning the first 50 by heart,"

15
00:00:56,080 --> 00:01:02,240
 I mean, the Pali word is reciting, reciting the first 50.

16
00:01:02,240 --> 00:01:07,820
 He is faced with the middle 50, and when he is still rec

17
00:01:07,820 --> 00:01:09,520
iting that by heart,

18
00:01:09,520 --> 00:01:11,200
 he is faced with the last 50.

19
00:01:11,200 --> 00:01:17,040
 There are altogether 150, actually, I did about three more,

20
00:01:17,320 --> 00:01:23,640
 150 sodas in that collection of middle length sayings.

21
00:01:23,640 --> 00:01:28,290
 And they are divided into three, the first, the second, and

22
00:01:28,290 --> 00:01:29,080
 the third group.

23
00:01:29,080 --> 00:01:31,480
 So when you are reciting the first group,

24
00:01:31,480 --> 00:01:34,280
 the second group may come to you and you may mix up.

25
00:01:34,280 --> 00:01:36,360
 And when you are reciting the second group,

26
00:01:36,360 --> 00:01:39,480
 the third may come to your mind and you will mix them up.

27
00:01:39,480 --> 00:01:41,000
 So it is not an easy task.

28
00:01:41,000 --> 00:01:46,540
 Being familiar with the Majjhima Nikaya, that is what is

29
00:01:46,540 --> 00:01:47,080
 meant here.

30
00:01:48,520 --> 00:01:51,880
 So it is not learning, but reciting.

31
00:01:51,880 --> 00:01:58,440
 And in paragraph 52,

32
00:01:58,440 --> 00:02:04,920
 then he recited the Dhadukkata to the Bhikus.

33
00:02:04,920 --> 00:02:09,420
 Recited here means taught them, not just, not just recite

34
00:02:09,420 --> 00:02:10,280
 to them.

35
00:02:10,280 --> 00:02:12,120
 He taught them Dhadukkata.

36
00:02:12,120 --> 00:02:15,160
 Dhadukkata is the third book of Abhidhamma.

37
00:02:15,160 --> 00:02:26,760
 [ Pause ]

38
00:02:26,760 --> 00:02:28,840
 And then in the next paragraph,

39
00:02:28,840 --> 00:02:37,640
 about 10 lines from the bottom.

40
00:02:37,640 --> 00:02:44,440
 Go and learn it from our own teachers.

41
00:02:45,560 --> 00:02:46,120
 You find that?

42
00:02:46,120 --> 00:02:48,520
 Our should be sure.

43
00:02:48,520 --> 00:02:53,800
 Because this monk

44
00:02:53,800 --> 00:03:01,480
 proclaimed that he would expound the three pitegas

45
00:03:01,480 --> 00:03:06,840
 without studying the commentaries.

46
00:03:06,840 --> 00:03:12,120
 So the other monk wanted to make him realize that

47
00:03:13,000 --> 00:03:15,160
 he was not qualified to do that.

48
00:03:15,160 --> 00:03:19,320
 So he asked a question and the other monk gave an answer.

49
00:03:19,320 --> 00:03:21,560
 And then the monk said, "Hmm."

50
00:03:21,560 --> 00:03:26,280
 Something like, "It is not correct or something."

51
00:03:26,280 --> 00:03:30,920
 Then again, three times he asked this question and three

52
00:03:30,920 --> 00:03:31,880
 times he said, "Hmm."

53
00:03:31,880 --> 00:03:38,600
 So he gave the different answers for different times.

54
00:03:38,600 --> 00:03:43,720
 So the elder later on said that the first answer you give

55
00:03:43,720 --> 00:03:45,160
 was a correct answer.

56
00:03:45,160 --> 00:03:47,480
 But since you have not learned from a teacher,

57
00:03:47,480 --> 00:03:51,320
 you are not firm on your answers.

58
00:03:51,320 --> 00:03:55,400
 And so when I say whom, you give another different answer

59
00:03:55,400 --> 00:03:57,080
 and then another.

60
00:03:57,080 --> 00:04:02,060
 So go and learn from your own teachers, not just reading

61
00:04:02,060 --> 00:04:03,640
 books or something like that.

62
00:04:03,640 --> 00:04:10,520
 (Laughs)

63
00:04:10,520 --> 00:04:13,480
 Even in these modern times,

64
00:04:13,480 --> 00:04:21,080
 just learning from books is not quite enough.

65
00:04:21,080 --> 00:04:29,590
 Now, for example, Abidama, you need a teacher or a friend

66
00:04:29,590 --> 00:04:30,280
 to help you.

67
00:04:31,080 --> 00:04:34,120
 Otherwise, you will not understand properly.

68
00:04:34,120 --> 00:04:40,600
 So go and learn it from your own teachers.

69
00:04:40,600 --> 00:04:43,320
 And then where shall I go?

70
00:04:43,320 --> 00:04:44,200
 And so he went there.

71
00:04:44,200 --> 00:04:47,800
 And then on the next page,

72
00:04:47,800 --> 00:04:53,240
 close to 55,

73
00:04:53,240 --> 00:04:57,960
 "What are you saying, Venerable Sir?

74
00:04:57,960 --> 00:05:00,360
 Have I not heard it all from you?"

75
00:05:00,360 --> 00:05:02,760
 Now, after learning from that monk,

76
00:05:02,760 --> 00:05:06,200
 the monk asked him to give...

77
00:05:06,200 --> 00:05:10,680
 I mean, after teaching that monk,

78
00:05:10,680 --> 00:05:16,140
 the teacher monk asked the pupil monk to give him a subject

79
00:05:16,140 --> 00:05:17,080
 of meditation.

80
00:05:17,080 --> 00:05:19,320
 So the monk said, "What are you saying?

81
00:05:19,320 --> 00:05:19,960
 When was I?

82
00:05:19,960 --> 00:05:22,440
 Have I not heard it all from you?

83
00:05:22,440 --> 00:05:25,560
 You have been teaching me these things and now you are

84
00:05:25,560 --> 00:05:29,880
 asking me to give you a subject of meditation.

85
00:05:29,880 --> 00:05:32,920
 What can I explain to you that you do not already know?"

86
00:05:32,920 --> 00:05:34,120
 Then the senior elder said,

87
00:05:34,120 --> 00:05:37,620
 "This path is different for one who has actually traveled

88
00:05:37,620 --> 00:05:38,120
 by it."

89
00:05:38,120 --> 00:05:41,880
 The Bali sentence really means,

90
00:05:41,880 --> 00:05:45,960
 "This is the path of one who has actually traveled by it."

91
00:05:45,960 --> 00:05:50,600
 That means, "I know only from books,

92
00:05:50,600 --> 00:05:56,100
 but you have practiced meditation and you have gained some

93
00:05:56,100 --> 00:05:57,640
 enlightenment."

94
00:05:58,360 --> 00:06:01,000
 So this path is different for me.

95
00:06:01,000 --> 00:06:03,720
 So please teach me meditation,

96
00:06:03,720 --> 00:06:06,520
 although I have taught you the books.

97
00:06:06,520 --> 00:06:09,080
 So that is what is meant here.

98
00:06:09,080 --> 00:06:13,360
 So this path is different for one who has actually traveled

99
00:06:13,360 --> 00:06:13,640
 by it.

100
00:06:13,640 --> 00:06:15,000
 Or it means,

101
00:06:15,000 --> 00:06:18,840
 "This is the path of one who has actually traveled by it."

102
00:06:18,840 --> 00:06:24,280
 So those who have not traveled by it do not really know

103
00:06:24,280 --> 00:06:25,320
 this path.

104
00:06:25,320 --> 00:06:27,880
 And I have not traveled this path.

105
00:06:28,280 --> 00:06:31,160
 So please teach me meditation, something like that.

106
00:06:31,160 --> 00:06:35,800
 Then in the next paragraph,

107
00:06:35,800 --> 00:06:39,080
 "The Arahant path befits our teacher."

108
00:06:39,080 --> 00:06:41,400
 That means the attainment of Arahantship.

109
00:06:41,400 --> 00:06:47,320
 So the teacher practiced meditation and became an Arahant.

110
00:06:47,320 --> 00:06:48,600
 So the people said,

111
00:06:48,600 --> 00:06:50,600
 "The Arahantship befits our teacher."

112
00:06:55,240 --> 00:06:58,490
 And then in the next paragraph, "Supernormal powers

113
00:06:58,490 --> 00:06:59,800
 regarding supernormal powers."

114
00:06:59,800 --> 00:07:08,430
 "They are hard to maintain like a prone infant or like a

115
00:07:08,430 --> 00:07:08,600
 what?"

116
00:07:08,600 --> 00:07:14,040
 "Young corn in my book like a baby hair."

117
00:07:14,040 --> 00:07:18,600
 First, "prone infant." What is prone?

118
00:07:18,600 --> 00:07:22,200
 Face down or face up?

119
00:07:24,200 --> 00:07:26,200
 Both?

120
00:07:26,200 --> 00:07:28,200
 I thought it was lying.

121
00:07:28,200 --> 00:07:31,800
 Prown is lying down.

122
00:07:31,800 --> 00:07:33,880
 Yeah, face down?

123
00:07:33,880 --> 00:07:36,200
 I don't know, just...

124
00:07:36,200 --> 00:07:37,480
 It's not that flat.

125
00:07:37,480 --> 00:07:40,840
 I don't know.

126
00:07:40,840 --> 00:07:46,440
 In Pali it means lying down, face up.

127
00:07:46,440 --> 00:07:46,920
 Face up.

128
00:07:52,360 --> 00:07:56,680
 And like a... what do you have there?

129
00:07:56,680 --> 00:07:58,680
 Like a young corn.

130
00:07:58,680 --> 00:08:01,400
 That means you had a 10-hour crop.

131
00:08:01,400 --> 00:08:06,840
 That's fragile.

132
00:08:06,840 --> 00:08:13,480
 In the first edition, he had baby hair

133
00:08:13,480 --> 00:08:17,240
 because he misread the Pali word.

134
00:08:17,240 --> 00:08:19,000
 The Pali word is

135
00:08:20,280 --> 00:08:23,240
 S-A-S-A.

136
00:08:23,240 --> 00:08:26,840
 But he read it as S-A-S-S-A.

137
00:08:26,840 --> 00:08:28,440
 I know.

138
00:08:28,440 --> 00:08:30,840
 The Pali word is S-A-S-S-A.

139
00:08:30,840 --> 00:08:34,840
 And he read it as S-A-S-A.

140
00:08:34,840 --> 00:08:38,440
 S-A-S-A means a hair or a rabbit.

141
00:08:38,440 --> 00:08:45,960
 S-A-S-S-A means a crop or corn or something that grows.

142
00:08:48,520 --> 00:08:51,880
 So I think the editors, or he himself,

143
00:08:51,880 --> 00:08:54,840
 corrected it in the second edition.

144
00:08:54,840 --> 00:09:02,600
 So supernormal powers and impediment

145
00:09:02,600 --> 00:09:04,840
 only for Vibhasana meditation

146
00:09:04,840 --> 00:09:10,520
 because they are gained through the practice of Samadhi.

147
00:09:10,520 --> 00:09:15,430
 So they are not the impediment for Samadhi or Samatha

148
00:09:15,430 --> 00:09:16,760
 meditation.

149
00:09:16,760 --> 00:09:19,160
 But for Vibhasana meditation, they are impediments.

150
00:09:19,160 --> 00:09:24,670
 Now approaching a good friend, the giver of a meditation

151
00:09:24,670 --> 00:09:25,160
 subject.

152
00:09:25,160 --> 00:09:30,470
 So you must find a teacher who can give you a meditation

153
00:09:30,470 --> 00:09:31,320
 subject.

154
00:09:31,320 --> 00:09:35,480
 And approaching a teacher is described in detail.

155
00:09:35,480 --> 00:09:41,480
 And it is very different from the practice,

156
00:09:41,480 --> 00:09:44,680
 especially in this country and the West.

157
00:09:45,400 --> 00:09:48,520
 Here teachers want to attract pupils

158
00:09:48,520 --> 00:09:59,430
 and so they are very willing and very eager to teach and

159
00:09:59,430 --> 00:10:00,200
 something like that.

160
00:10:00,200 --> 00:10:03,640
 But here the one who wants to practice meditation

161
00:10:03,640 --> 00:10:09,320
 has to approach a teacher in a very careful way

162
00:10:09,320 --> 00:10:12,840
 and then not to offend him and so on.

163
00:10:14,120 --> 00:10:15,400
 So it is very different.

164
00:10:15,400 --> 00:10:20,600
 Here teachers want to please those who come to them.

165
00:10:20,600 --> 00:10:32,290
 Now the practice of loving kindness meditation is mentioned

166
00:10:32,290 --> 00:10:33,160
 here, right?

167
00:10:33,160 --> 00:10:37,080
 "May they be happy and free from affliction."

168
00:10:37,080 --> 00:10:40,090
 Then he should develop it towards all deities within the

169
00:10:40,090 --> 00:10:40,440
 boundary.

170
00:10:40,440 --> 00:10:44,680
 Or now the boundary on page 98.

171
00:10:44,680 --> 00:10:47,080
 Finally Bhikkhu takes up a meditation subject.

172
00:10:47,080 --> 00:10:49,970
 He should first develop loving kindness towards a community

173
00:10:49,970 --> 00:10:51,880
 of Bhikkhus within the boundary.

174
00:10:51,880 --> 00:10:54,680
 Means within the boundary of the monastery.

175
00:10:54,680 --> 00:11:08,840
 The Pali word here used is Sima,

176
00:11:09,720 --> 00:11:14,080
 which also means a consecrated place where formal acts of

177
00:11:14,080 --> 00:11:16,440
 Sangha are performed.

178
00:11:16,440 --> 00:11:20,360
 But here Sima simply means a boundary.

179
00:11:20,360 --> 00:11:26,200
 So that's the boundary of a monastery.

180
00:11:26,200 --> 00:11:31,860
 So loving kindness towards the monks living in the

181
00:11:31,860 --> 00:11:32,520
 monastery,

182
00:11:32,520 --> 00:11:33,000
 just that.

183
00:11:33,000 --> 00:11:36,280
 "May they be happy and free from affliction."

184
00:11:36,280 --> 00:11:38,920
 Then he should develop it towards the all deities within

185
00:11:38,920 --> 00:11:39,480
 the boundary.

186
00:11:39,640 --> 00:11:41,480
 That means within the monastery.

187
00:11:41,480 --> 00:11:44,600
 Then towards the all principal people in the village.

188
00:11:44,600 --> 00:11:48,680
 That is his alms resort, where he goes for alms.

189
00:11:48,680 --> 00:11:53,530
 Then to all human beings there and to all living beings

190
00:11:53,530 --> 00:11:56,680
 dependent on the human beings.

191
00:11:56,680 --> 00:11:59,480
 This is also misunderstanding of the word.

192
00:11:59,480 --> 00:12:03,400
 One word in Pali.

193
00:12:05,720 --> 00:12:17,400
 What really is meant here is that then to all beings

194
00:12:17,400 --> 00:12:20,440
 beginning with human beings.

195
00:12:20,440 --> 00:12:26,550
 So after sending loving kindness thoughts to principal

196
00:12:26,550 --> 00:12:28,040
 people in the village.

197
00:12:28,040 --> 00:12:31,520
 That means a village head man or an official in the village

198
00:12:31,520 --> 00:12:31,800
.

199
00:12:32,600 --> 00:12:37,210
 He should send thoughts to all beings beginning with to all

200
00:12:37,210 --> 00:12:38,200
 human beings.

201
00:12:38,200 --> 00:12:41,560
 So may all human beings be well happy and peaceful.

202
00:12:41,560 --> 00:12:46,280
 And then may all may all beings be well happy and peaceful.

203
00:12:46,280 --> 00:12:51,520
 So not all human beings there and to all living beings

204
00:12:51,520 --> 00:12:53,560
 dependent on the human beings.

205
00:12:53,560 --> 00:12:54,600
 No, that is not so.

206
00:12:55,480 --> 00:13:01,380
 So the translation should be then to all beings beginning

207
00:13:01,380 --> 00:13:03,320
 with human beings.

208
00:13:03,320 --> 00:13:09,160
 What does it mean towards all deities?

209
00:13:09,160 --> 00:13:16,380
 These spirits, tree spirits and guardian spirits living in

210
00:13:16,380 --> 00:13:19,000
 the precinct of the monastery.

211
00:13:22,440 --> 00:13:27,080
 We Buddhist believe that there are spirits all around and

212
00:13:27,080 --> 00:13:28,440
 tree spirits,

213
00:13:28,440 --> 00:13:32,700
 guardian spirits, guardian of the monastery, guardian of

214
00:13:32,700 --> 00:13:33,960
 the person and so on.

215
00:13:33,960 --> 00:13:36,120
 So we send thoughts to them.

216
00:13:36,120 --> 00:13:46,340
 And then with next paragraph with mindfulness of death

217
00:13:46,340 --> 00:13:47,560
 thinking I have got to die.

218
00:13:47,560 --> 00:13:52,030
 He gives up improper search and with the growing sense of

219
00:13:52,030 --> 00:13:52,760
 urgency,

220
00:13:52,760 --> 00:13:54,680
 he comes to live without attachment.

221
00:13:54,680 --> 00:13:57,640
 Actually he comes to live without sluggishness, not

222
00:13:57,640 --> 00:13:58,200
 attachment.

223
00:13:58,200 --> 00:14:04,280
 When there is an urgent sense of urgency, when there is the

224
00:14:04,280 --> 00:14:06,440
 thinking that I've got to die,

225
00:14:06,440 --> 00:14:08,920
 then I can't afford to be lazy.

226
00:14:08,920 --> 00:14:11,560
 I can't afford to be sluggish.

227
00:14:11,560 --> 00:14:13,000
 I must practice meditation.

228
00:14:13,000 --> 00:14:13,960
 I must make effort.

229
00:14:14,920 --> 00:14:19,480
 So the word here should be not attachment but sluggishness.

230
00:14:19,480 --> 00:14:21,960
 Is there another word for that?

231
00:14:21,960 --> 00:14:24,120
 Laziness, something like that.

232
00:14:24,120 --> 00:14:26,360
 Indolent, right?

233
00:14:26,360 --> 00:14:30,920
 To live without indolence or sluggishness.

234
00:14:30,920 --> 00:14:36,920
 That means to be energetic in the practice of meditation.

235
00:14:44,440 --> 00:14:48,650
 And then paragraph 61, the good friend, he is revered and

236
00:14:48,650 --> 00:14:49,560
 dearly loved

237
00:14:49,560 --> 00:14:51,640
 and the one who speaks and suffers speech.

238
00:14:51,640 --> 00:14:56,240
 The speech he addresses perform, he does not urge without a

239
00:14:56,240 --> 00:14:56,840
 reason.

240
00:14:56,840 --> 00:14:59,960
 That means urge to do improper things.

241
00:14:59,960 --> 00:15:05,720
 He is wholly solicitous of welfare and partial to progress.

242
00:15:05,720 --> 00:15:07,080
 What is partial to progress?

243
00:15:07,080 --> 00:15:10,520
 Does it make sense?

244
00:15:13,800 --> 00:15:16,680
 A partial often means favors.

245
00:15:16,680 --> 00:15:23,400
 Here the meaning is he is on the side of progress.

246
00:15:23,400 --> 00:15:26,200
 That means he is making progress.

247
00:15:26,200 --> 00:15:33,670
 So I look at this in the dictionary, it says to be fond of

248
00:15:33,670 --> 00:15:34,360
 something.

249
00:15:34,360 --> 00:15:37,560
 But it does not mean to be fond of progress.

250
00:15:37,560 --> 00:15:39,320
 He is on the side of progress.

251
00:15:39,320 --> 00:15:42,200
 So he's progressing?

252
00:15:42,200 --> 00:15:43,080
 Progressing, yes.

253
00:15:43,080 --> 00:16:01,880
 And on next page, about six, seven lines from the top,

254
00:16:01,880 --> 00:16:08,390
 the end of the paragraph, has reached the destruction of

255
00:16:08,390 --> 00:16:10,920
 cankers by augmenting insight.

256
00:16:10,920 --> 00:16:12,840
 That means by practicing insight.

257
00:16:13,480 --> 00:16:21,160
 That is just jhana as a proximate cause.

258
00:16:21,160 --> 00:16:26,010
 And then next paragraph, I am one whose cankers are

259
00:16:26,010 --> 00:16:28,360
 destroyed, why not?

260
00:16:28,360 --> 00:16:32,000
 He declares himself and he knows that his instructions will

261
00:16:32,000 --> 00:16:33,160
 be carried out.

262
00:16:33,160 --> 00:16:38,600
 Actually, he knows that the other one is a practitioner.

263
00:16:38,600 --> 00:16:42,040
 Not that his instructions will be carried out.

264
00:16:42,040 --> 00:16:45,400
 If you know that the other one is a meditator,

265
00:16:45,400 --> 00:16:49,480
 then you may tell him what you have attained.

266
00:16:49,480 --> 00:16:54,680
 But usually, normally monks do not tell the attainment to

267
00:16:54,680 --> 00:16:56,200
 other people.

268
00:16:56,200 --> 00:17:00,280
 So they will be practiced instead of carried out?

269
00:17:00,280 --> 00:17:04,200
 His instruction, not even his instructions.

270
00:17:04,200 --> 00:17:08,040
 But it could be said his instructions, though.

271
00:17:08,040 --> 00:17:11,320
 But yeah, practiced.

272
00:17:13,080 --> 00:17:19,320
 Or he knows that the other one is a practitioner.

273
00:17:19,320 --> 00:17:23,000
 The other one is the one who practices meditation.

274
00:17:23,000 --> 00:17:34,680
 And then paragraph 70 on page 101, about four lines down,

275
00:17:36,440 --> 00:17:41,560
 if he does not allow it when asked, then they can be done

276
00:17:41,560 --> 00:17:42,920
 when the opportunity offers.

277
00:17:42,920 --> 00:17:47,960
 When he does them, three two sticks should be brought.

278
00:17:47,960 --> 00:17:50,440
 That means should be given to him, should be presented.

279
00:17:50,440 --> 00:17:53,560
 He is more a medium and a big one.

280
00:17:53,560 --> 00:17:56,350
 And two kinds of mouth washing water and bathing water that

281
00:17:56,350 --> 00:17:57,880
 is hot and cold.

282
00:18:02,840 --> 00:18:09,170
 And then on the next page, paragraph 72, if he does not ask

283
00:18:09,170 --> 00:18:11,240
 but agrees to the duties being done,

284
00:18:11,240 --> 00:18:17,240
 then after 10 days or a fortnight have gone by,

285
00:18:17,240 --> 00:18:20,540
 he should make an opportunity by staying back one day at

286
00:18:20,540 --> 00:18:23,160
 the time of his dismissal.

287
00:18:23,160 --> 00:18:29,880
 That means even though he is dismissed,

288
00:18:29,880 --> 00:18:39,720
 he should ask permission and announce his purpose in coming

289
00:18:39,720 --> 00:18:42,280
 to that month, to that teacher.

290
00:18:42,280 --> 00:18:45,800
 So make an opportunity.

291
00:18:45,800 --> 00:18:46,920
 I don't know what that means.

292
00:18:46,920 --> 00:18:52,920
 The Pali word here is to ask permission.

293
00:18:58,200 --> 00:19:01,800
 So when he is dismissed, he must not go away.

294
00:19:01,800 --> 00:19:11,240
 So then he must not go away but ask him permission.

295
00:19:11,240 --> 00:19:13,800
 I want to say something.

296
00:19:13,800 --> 00:19:16,750
 And then when you ask and tell him that he comes for the

297
00:19:16,750 --> 00:19:18,360
 practice of meditation.

298
00:19:18,360 --> 00:19:26,040
 So it is how to approach a teacher.

299
00:19:26,040 --> 00:19:34,120
 Okay, there.

300
00:19:34,120 --> 00:19:40,600
 We will talk about temperament next week.

301
00:19:40,600 --> 00:19:47,720
 The explanation about temperament is very interesting.

302
00:19:47,720 --> 00:19:53,800
 Okay.

303
00:20:15,480 --> 00:20:19,460
 I am already thinking about next time, about when we come

304
00:20:19,460 --> 00:20:20,600
 next and how...

305
00:20:20,600 --> 00:20:27,380
 So I looked at the book and I looked at the next chunk that

306
00:20:27,380 --> 00:20:29,560
 we could do is quite long.

307
00:20:30,280 --> 00:20:30,680
 Okay.

308
00:20:30,680 --> 00:20:36,760
 So

309
00:20:36,760 --> 00:20:42,840
 so

310
00:20:42,840 --> 00:20:48,920
 so

311
00:20:48,920 --> 00:20:55,000
 so

312
00:20:55,000 --> 00:21:01,080
 so

313
00:21:01,080 --> 00:21:05,160
 so

