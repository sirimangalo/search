1
00:00:00,000 --> 00:00:08,000
 Okay, good evening everyone.

2
00:00:08,000 --> 00:00:13,000
 Broadcasting live, December 30th.

3
00:00:13,000 --> 00:00:20,300
 Audio first, because tonight we will be studying the Dham

4
00:00:20,300 --> 00:00:22,000
mapada again.

5
00:00:22,000 --> 00:00:33,120
 So we'll do a video recording and then we'll switch to live

6
00:00:33,120 --> 00:00:35,000
 video broadcast.

7
00:00:35,000 --> 00:00:38,000
 Test.

8
00:00:38,000 --> 00:00:43,000
 Okay.

9
00:00:43,000 --> 00:00:47,000
 Hello and welcome back to our study of the Dhammapada.

10
00:00:47,000 --> 00:00:53,320
 Tonight we continue on with verse 122, which reads as

11
00:00:53,320 --> 00:00:55,000
 follows.

12
00:00:55,000 --> 00:01:02,000
 "Mavamanyeta punyasa namandang agamisote,

13
00:01:02,000 --> 00:01:12,000
 Udubindunipatena, Udakumbopipurate, Diropurate punyasa,

14
00:01:12,000 --> 00:01:19,000
 Dokang Dokangpi ajinam."

15
00:01:19,000 --> 00:01:25,000
 Which is almost exactly the same as the last verse,

16
00:01:25,000 --> 00:01:30,000
 which if you'll remember was in regards to not thinking

17
00:01:30,000 --> 00:01:35,000
 little of evo-deeds,

18
00:01:35,000 --> 00:01:37,000
 thinking it won't come to me.

19
00:01:37,000 --> 00:01:39,000
 So here is the opposite.

20
00:01:39,000 --> 00:01:48,210
 "Mavamanyeta punyasa, don't look down upon or don't

21
00:01:48,210 --> 00:01:51,000
 underestimate the punya,"

22
00:01:51,000 --> 00:01:58,460
 which is goodness, thinking "namandang agamisote, it won't

23
00:01:58,460 --> 00:02:01,000
 come to me."

24
00:02:01,000 --> 00:02:11,000
 "For Udubindunipatena, Udakumbopipurate, a water pot

25
00:02:11,000 --> 00:02:14,000
 becomes full drop by drop

26
00:02:14,000 --> 00:02:20,000
 with drops of water, with drops of water falling.

27
00:02:20,000 --> 00:02:29,000
 Diropurate punyasa, a wise person, becomes full of goodness

28
00:02:29,000 --> 00:02:29,000
.

29
00:02:29,000 --> 00:02:46,000
 Dokang Dokangpi ajinam."

30
00:02:46,000 --> 00:02:52,110
 Even though it might be drop by drop, one becomes full with

31
00:02:52,110 --> 00:02:53,000
 goodness,

32
00:02:53,000 --> 00:03:03,000
 full of goodness.

33
00:03:03,000 --> 00:03:08,000
 Here we have a story.

34
00:03:08,000 --> 00:03:13,220
 The story goes that the Buddha was talking about different

35
00:03:13,220 --> 00:03:16,000
 types of giving.

36
00:03:16,000 --> 00:03:23,160
 It seems there are many stories of giving being a

37
00:03:23,160 --> 00:03:27,000
 fundamental religious practice

38
00:03:27,000 --> 00:03:34,340
 that you might say gets a little too much coverage in

39
00:03:34,340 --> 00:03:40,000
 Buddhist circles.

40
00:03:40,000 --> 00:03:45,980
 I think it's fair to say that often it's because there's a

41
00:03:45,980 --> 00:03:47,000
 concern for getting.

42
00:03:47,000 --> 00:03:49,570
 People talk about giving because they're concerned with

43
00:03:49,570 --> 00:03:50,000
 getting.

44
00:03:50,000 --> 00:03:56,000
 Often monasteries or meditation centers or monks would like

45
00:03:56,000 --> 00:03:57,000
 to get things

46
00:03:57,000 --> 00:04:03,610
 or are in need of things, realistically are in need of

47
00:04:03,610 --> 00:04:05,000
 things.

48
00:04:05,000 --> 00:04:08,590
 So there's kind of a tension there because you have to

49
00:04:08,590 --> 00:04:09,000
 accept that

50
00:04:09,000 --> 00:04:12,000
 while there is a need.

51
00:04:12,000 --> 00:04:15,000
 It seems a little bit suspicious and I've often commented

52
00:04:15,000 --> 00:04:16,000
 on this.

53
00:04:16,000 --> 00:04:31,000
 A little bit, not suspicious, a little bit uncomfortable

54
00:04:31,000 --> 00:04:36,420
 to talk about how good it is to give when you're actually

55
00:04:36,420 --> 00:04:37,000
 mean.

56
00:04:37,000 --> 00:04:42,000
 It would be good if we could receive, for example.

57
00:04:42,000 --> 00:04:47,000
 Anyway, giving is a spiritual practice.

58
00:04:47,000 --> 00:04:49,690
 It's a good spiritual practice. There's nothing wrong with

59
00:04:49,690 --> 00:04:50,000
 giving.

60
00:04:50,000 --> 00:04:56,000
 We recently gave a large donation, our group, many of you

61
00:04:56,000 --> 00:04:59,000
 who are watching this video perhaps,

62
00:04:59,000 --> 00:05:07,000
 to a children's home in Florida.

63
00:05:07,000 --> 00:05:10,840
 It was so good even my stepfather got involved, my mother

64
00:05:10,840 --> 00:05:12,000
 got involved,

65
00:05:12,000 --> 00:05:16,000
 and everyone was so happy.

66
00:05:16,000 --> 00:05:20,000
 My Sri Lankan friends in Florida got involved.

67
00:05:20,000 --> 00:05:22,480
 It was a way to really bring people together and has the

68
00:05:22,480 --> 00:05:26,000
 potential to bring people together again.

69
00:05:26,000 --> 00:05:29,190
 So we do this again next time, we'll even bring more people

70
00:05:29,190 --> 00:05:30,000
 together.

71
00:05:30,000 --> 00:05:32,000
 And that's what this story in the Dhammapada is about.

72
00:05:32,000 --> 00:05:37,280
 The Buddha talks about how some people give themselves, are

73
00:05:37,280 --> 00:05:39,000
 charitable, are kind,

74
00:05:39,000 --> 00:05:42,000
 but don't encourage other people to do good deeds.

75
00:05:42,000 --> 00:05:45,000
 This doesn't just have to do with giving any good deed.

76
00:05:45,000 --> 00:05:49,000
 You could say the same about meditation or morality.

77
00:05:49,000 --> 00:05:52,000
 If you're a moral yourself but you don't encourage people

78
00:05:52,000 --> 00:05:55,000
 or let other people know about

79
00:05:55,000 --> 00:06:03,000
 your views on ethical acts or ethical matters issues.

80
00:06:03,000 --> 00:06:07,870
 And another person might encourage others but not do good

81
00:06:07,870 --> 00:06:10,000
 deeds themselves.

82
00:06:10,000 --> 00:06:13,430
 And then one person does neither and another person does

83
00:06:13,430 --> 00:06:14,000
 both.

84
00:06:14,000 --> 00:06:18,000
 Both does good deeds and encourages others.

85
00:06:18,000 --> 00:06:24,200
 And so his teaching, it's something that we've talked about

86
00:06:24,200 --> 00:06:27,000
 before, is that if you do good deeds

87
00:06:27,000 --> 00:06:32,100
 but don't encourage others to do good deeds, then you'll

88
00:06:32,100 --> 00:06:36,000
 get great reward yourself.

89
00:06:36,000 --> 00:06:38,000
 Your life will get better.

90
00:06:38,000 --> 00:06:40,000
 But you won't be alone.

91
00:06:40,000 --> 00:06:42,000
 You won't be surrounded by other people.

92
00:06:42,000 --> 00:06:45,980
 If you become a good person yourself but you haven't made

93
00:06:45,980 --> 00:06:48,000
 friends with or worked together

94
00:06:48,000 --> 00:06:53,890
 with other people to do good deeds, then you'll be lacking

95
00:06:53,890 --> 00:06:56,000
 in friendship and companionship,

96
00:06:56,000 --> 00:07:02,450
 which is very important both in the world and in spiritual

97
00:07:02,450 --> 00:07:04,000
 practice.

98
00:07:04,000 --> 00:07:09,470
 And so as the Buddha was teaching, it turns out there was a

99
00:07:09,470 --> 00:07:13,000
 certain Pandita Puri-sa, a wise man,

100
00:07:13,000 --> 00:07:17,000
 who having heard this Dhamma-de-sana, thought to himself,

101
00:07:17,000 --> 00:07:20,000
 "Well then, that's what I should do.

102
00:07:20,000 --> 00:07:24,460
 Rather than just be generous and kind and do good deeds on

103
00:07:24,460 --> 00:07:25,000
 my own,

104
00:07:25,000 --> 00:07:28,000
 I should bring people together to do good deeds.

105
00:07:28,000 --> 00:07:31,000
 I'm familiar. This is what we did for Florida.

106
00:07:31,000 --> 00:07:34,000
 Why did we do these things? Bring people together.

107
00:07:34,000 --> 00:07:37,950
 Don't just do good deeds yourself. Do them together as a

108
00:07:37,950 --> 00:07:39,000
 group."

109
00:07:39,000 --> 00:07:52,000
 So he went to the Buddha and he asked, or maybe he went to

110
00:07:52,000 --> 00:07:53,000
 the Buddha,

111
00:07:53,000 --> 00:07:58,790
 and said to him, "Bhante, I would like to invite monks for

112
00:07:58,790 --> 00:08:00,000
 lunch."

113
00:08:00,000 --> 00:08:08,000
 And the Buddha said, "How many monks?"

114
00:08:08,000 --> 00:08:13,480
 And the man said, "All the monks. I want to invite them all

115
00:08:13,480 --> 00:08:15,000
 for lunch."

116
00:08:15,000 --> 00:08:22,420
 Now, how many monks were there? There were many, many monks

117
00:08:22,420 --> 00:08:23,000
.

118
00:08:23,000 --> 00:08:25,620
 I don't know if it says here how many monks, but it was a

119
00:08:25,620 --> 00:08:26,000
 lot.

120
00:08:26,000 --> 00:08:31,360
 So it was the number of monks. This is in Jaita, so there

121
00:08:31,360 --> 00:08:34,000
 were probably hundreds, if not more.

122
00:08:34,000 --> 00:08:37,000
 There were thousands even, maybe. I don't know.

123
00:08:37,000 --> 00:08:42,000
 Of course, they always over-exaggerate, but lots of monks.

124
00:08:42,000 --> 00:08:47,470
 More than any one poor family or even ordinary family would

125
00:08:47,470 --> 00:08:50,000
 be able to care for them.

126
00:08:50,000 --> 00:08:56,000
 So he did this completely on faith that he could get people

127
00:08:56,000 --> 00:09:00,000
 to join him in this great deed.

128
00:09:00,000 --> 00:09:06,430
 And it was really a powerful determination on his part to

129
00:09:06,430 --> 00:09:08,000
 be so bold.

130
00:09:08,000 --> 00:09:19,970
 And so he went into the village, into the city actually of

131
00:09:19,970 --> 00:09:26,000
 Sabati, and he went maybe banging a drum,

132
00:09:26,000 --> 00:09:29,660
 or he went to his own village, maybe not the city, but he

133
00:09:29,660 --> 00:09:34,000
 went around saying,

134
00:09:34,000 --> 00:09:38,130
 "Everyone, maybe banging a drum or something. I have

135
00:09:38,130 --> 00:09:40,590
 invited the congregation of monks presided over by the

136
00:09:40,590 --> 00:09:43,000
 Buddha for the meal tomorrow.

137
00:09:43,000 --> 00:09:47,140
 Come, give what you can, provide as much as your means

138
00:09:47,140 --> 00:09:48,000
 permit.

139
00:09:48,000 --> 00:09:52,770
 Let us do all the cooking in one place and give alms in

140
00:09:52,770 --> 00:09:54,000
 common."

141
00:09:54,000 --> 00:09:57,720
 So rather than the ordinary way, which would be, "Oh, well,

142
00:09:57,720 --> 00:10:00,000
 I'll invite a couple of monks to my house,

143
00:10:00,000 --> 00:10:04,000
 and I'll do this good deed or that good deed."

144
00:10:04,000 --> 00:10:08,000
 The idea was, "Let's all do one good deed together."

145
00:10:08,000 --> 00:10:11,190
 So that we share in the deed, and that our karma is

146
00:10:11,190 --> 00:10:15,700
 intertwined in the sense that we have similar futures

147
00:10:15,700 --> 00:10:17,000
 together.

148
00:10:17,000 --> 00:10:24,000
 We move forward and upward together.

149
00:10:24,000 --> 00:10:28,560
 And so the people were for the most part overjoyed, but

150
00:10:28,560 --> 00:10:32,000
 there was one rich guy who was not pleased.

151
00:10:32,000 --> 00:10:36,470
 And hearing what this guy was saying, preaching, and

152
00:10:36,470 --> 00:10:42,170
 talking about his intentions became quite angry with the

153
00:10:42,170 --> 00:10:45,000
 thought thus,

154
00:10:45,000 --> 00:10:51,420
 "Look at this guy. Rather than invite people, invite monks

155
00:10:51,420 --> 00:10:54,000
 based on his own means,

156
00:10:54,000 --> 00:11:01,740
 how dare he assume that other people should help him

157
00:11:01,740 --> 00:11:07,000
 fulfill his wishes, fulfill his intentions?

158
00:11:07,000 --> 00:11:13,140
 I mean, how dare he just assume or push us all or try to

159
00:11:13,140 --> 00:11:18,000
 persuade us all to do something that really is on him?"

160
00:11:18,000 --> 00:11:23,220
 This is his burden. He's the idiot who bit off more than he

161
00:11:23,220 --> 00:11:27,000
 can chew, and now he's coming and begging and pleading us.

162
00:11:27,000 --> 00:11:32,200
 He was very upset about this. A rich man. This guy was rich

163
00:11:32,200 --> 00:11:33,000
 as well.

164
00:11:33,000 --> 00:11:39,330
 When the guy came to his door, he took, it's funny, he took

165
00:11:39,330 --> 00:11:44,830
 two fingers in his thumb, I guess, or maybe three fingers

166
00:11:44,830 --> 00:11:46,000
 in his thumb.

167
00:11:46,000 --> 00:11:50,950
 Maybe it was like this. And grabbed just as much rice would

168
00:11:50,950 --> 00:11:54,000
 fit there, and dropped it in this guy's bowl,

169
00:11:54,000 --> 00:12:01,690
 and did the same with beans and jaggery or sugar and salt

170
00:12:01,690 --> 00:12:06,000
 or whatever. Whatever he gave, he gave just a little bit

171
00:12:06,000 --> 00:12:08,000
 when he was giving ingredients.

172
00:12:08,000 --> 00:12:15,140
 And so that's significant because he came to be known as

173
00:12:15,140 --> 00:12:23,000
 the cat foot rich man or the cat foot guy. Because I guess,

174
00:12:23,000 --> 00:12:24,000
 I think, because this looks like a cat's foot.

175
00:12:24,000 --> 00:12:30,010
 Cats have three paws, maybe, right? The three paws and the

176
00:12:30,010 --> 00:12:34,000
 thumb. Somehow that had a meaning at the time.

177
00:12:34,000 --> 00:12:45,000
 So his name was, he became known as cat footer, Bilalapada.

178
00:12:45,000 --> 00:12:51,730
 So the wise man, the guy who was organizing this, took the

179
00:12:51,730 --> 00:12:56,000
 offerings and he placed them apart.

180
00:12:56,000 --> 00:12:59,460
 So he took the other, everyone else's offerings and just

181
00:12:59,460 --> 00:13:04,220
 put them all together, but the rich man's offerings, he

182
00:13:04,220 --> 00:13:10,000
 kept them separate and

183
00:13:10,000 --> 00:13:14,430
 he guarded them individually. And now the rich man was sort

184
00:13:14,430 --> 00:13:16,000
 of watching this guy.

185
00:13:16,000 --> 00:13:19,390
 And as he saw this behavior, that he hadn't mixed them in

186
00:13:19,390 --> 00:13:23,000
 with everything else, that he had kept them separate.

187
00:13:23,000 --> 00:13:25,960
 He asked one of his servants, he told one of his servants

188
00:13:25,960 --> 00:13:30,730
 to go and spy on him and see what he was doing. What was up

189
00:13:30,730 --> 00:13:35,000
 with all this?

190
00:13:35,000 --> 00:13:40,460
 And the man followed after this guy, the organizer, and saw

191
00:13:40,460 --> 00:13:45,090
 that what he was doing is he was taking like one grain or a

192
00:13:45,090 --> 00:13:50,000
 few grains of the rich guy's rice

193
00:13:50,000 --> 00:13:58,110
 and putting a little bit in each and every thing that they

194
00:13:58,110 --> 00:14:07,000
 were cooking. So distributing it evenly among the product.

195
00:14:07,000 --> 00:14:09,580
 And so this guy goes back and tells the rich man who's

196
00:14:09,580 --> 00:14:12,000
 quite puzzled and doesn't know what's going on.

197
00:14:12,000 --> 00:14:18,550
 He can't see what angle this guy's playing, but he starts

198
00:14:18,550 --> 00:14:28,000
 to get suspicious because this guy has deliberately paid

199
00:14:28,000 --> 00:14:30,000
 special attention to the rich man's offering,

200
00:14:30,000 --> 00:14:36,490
 which was ridiculously small. And so he starts to get this

201
00:14:36,490 --> 00:14:42,150
 thought in his mind. He knows, that guy knows that my

202
00:14:42,150 --> 00:14:48,000
 offering was miniscule, was meaningless, was an insult.

203
00:14:48,000 --> 00:14:51,780
 And he's going to tell the Buddha, or he's going to

204
00:14:51,780 --> 00:14:56,190
 announce everyone, and he's going to announce that I gave

205
00:14:56,190 --> 00:15:00,000
 such and such, that I gave very, very little.

206
00:15:00,000 --> 00:15:04,460
 So what he does, he takes a knife, because it gets like a

207
00:15:04,460 --> 00:15:09,000
 sword or a knife or something, and he sticks it in his robe

208
00:15:09,000 --> 00:15:13,000
, and he goes to the monastery the next morning,

209
00:15:13,000 --> 00:15:15,880
 or no, he goes to the place where they're going to feed the

210
00:15:15,880 --> 00:15:20,080
 monks the next morning with the thought, if he does, as

211
00:15:20,080 --> 00:15:24,000
 soon as he opens his mouth, if he starts to talk about me,

212
00:15:24,000 --> 00:15:26,000
 I'm going to kill him.

213
00:15:26,000 --> 00:15:38,000
 That's how awful this guy really was.

214
00:15:38,000 --> 00:15:42,200
 It's quite odd that he, because later on he actually comes

215
00:15:42,200 --> 00:15:48,000
 to realize the Dhamma, but this is the story.

216
00:15:48,000 --> 00:15:50,930
 It's hard to know what's going on underneath, but he gets

217
00:15:50,930 --> 00:15:54,830
 very, very angry, and he must really think that this guy is

218
00:15:54,830 --> 00:15:56,000
 out to get him,

219
00:15:56,000 --> 00:16:06,830
 that this guy has set him up, and is kind of disappointed

220
00:16:06,830 --> 00:16:13,220
 or upset, or feels self-righteous about the fact that the

221
00:16:13,220 --> 00:16:17,000
 rich man didn't give what he could give.

222
00:16:17,000 --> 00:16:23,040
 Anyway, so he's there with his knife, sort of standing off

223
00:16:23,040 --> 00:16:28,000
 to the side, and what does this organizer do?

224
00:16:28,000 --> 00:16:32,740
 He proclaims to the Buddha that all the people gathered

225
00:16:32,740 --> 00:16:37,440
 there had given something for each, for every bit of the

226
00:16:37,440 --> 00:16:45,000
 food, and he said, "Please, Venerable Sir,

227
00:16:45,000 --> 00:16:51,700
 may everyone here receive a rich reward, because they all

228
00:16:51,700 --> 00:16:56,000
 gave according to their ability."

229
00:16:56,000 --> 00:17:01,970
 And the rich man heard this, and he was quite moved by the

230
00:17:01,970 --> 00:17:07,830
 generous, and he realized that he had this whole thing

231
00:17:07,830 --> 00:17:10,000
 misunderstood,

232
00:17:10,000 --> 00:17:13,990
 and he had misunderstood this man's intentions, and started

233
00:17:13,990 --> 00:17:18,000
 to realize that actually this guy was a really good person.

234
00:17:18,000 --> 00:17:21,710
 And it's funny, the translation says, "If I don't ask him

235
00:17:21,710 --> 00:17:25,260
 to pardon me, punishment from the king will fall upon my

236
00:17:25,260 --> 00:17:26,000
 head."

237
00:17:26,000 --> 00:17:29,000
 But I'm pretty sure that's not what it says.

238
00:17:29,000 --> 00:17:35,170
 You see, there's something called "Dewa Danda," which "Dewa

239
00:17:35,170 --> 00:17:39,000
" means "angel," but it can also mean "king."

240
00:17:39,000 --> 00:17:43,690
 The king is considered to be a god among men, or a deity

241
00:17:43,690 --> 00:17:48,070
 among men, so it could mean "king," but it really doesn't,

242
00:17:48,070 --> 00:17:49,000
 because it's talking about,

243
00:17:49,000 --> 00:17:53,180
 actually talking about his head, which, well, there's no

244
00:17:53,180 --> 00:17:57,000
 reason that the king would punish him, but it's the angels

245
00:17:57,000 --> 00:17:58,000
 that will

246
00:17:58,000 --> 00:18:02,640
 "meet out punishment by splitting his head into pieces,"

247
00:18:02,640 --> 00:18:08,050
 which is a common phrase. There was this belief that if you

248
00:18:08,050 --> 00:18:10,000
 did a terribly heinous crime,

249
00:18:10,000 --> 00:18:16,310
 the angels would "meet out punishment on your head by

250
00:18:16,310 --> 00:18:20,000
 splitting it into pieces."

251
00:18:20,000 --> 00:18:23,790
 And so he, actually this rich man who was very arrogant and

252
00:18:23,790 --> 00:18:28,270
 conceited and was very much on the wrong path, mended his

253
00:18:28,270 --> 00:18:29,000
 ways just standing there,

254
00:18:29,000 --> 00:18:32,720
 listening and watching and looking and seeing what a

255
00:18:32,720 --> 00:18:35,810
 wonderful thing he had missed out on because of his selfish

256
00:18:35,810 --> 00:18:41,000
ness and his greed and his stinginess.

257
00:18:41,000 --> 00:18:47,900
 And so he bowed down at the feet of this organizing fellow

258
00:18:47,900 --> 00:18:53,000
 and said, "Please forgive me, sir."

259
00:18:53,000 --> 00:18:59,390
 And the man looked down at him and said, "Why are you

260
00:18:59,390 --> 00:19:06,990
 asking me forgiveness? Totally oblivious and having never

261
00:19:06,990 --> 00:19:12,000
 thought anything bad about the man."

262
00:19:12,000 --> 00:19:16,130
 So he was totally surprised as to what was wrong. And the

263
00:19:16,130 --> 00:19:20,640
 treasurer explained to him, sorry, the rich man, explained

264
00:19:20,640 --> 00:19:24,000
 to him that really he was angry.

265
00:19:24,000 --> 00:19:28,660
 He gave that gift out of anger and he purposely gave very,

266
00:19:28,660 --> 00:19:33,000
 very little. And actually at this point felt kind of sad

267
00:19:33,000 --> 00:19:36,000
 that he hadn't given something significant.

268
00:19:36,000 --> 00:19:39,750
 And the Buddha saw this standing right there, sitting there

269
00:19:39,750 --> 00:19:44,820
 probably eating, and asked what was happening, what was

270
00:19:44,820 --> 00:19:46,000
 going on.

271
00:19:46,000 --> 00:19:52,070
 And so they told him. And the rich man said, "Well, I was

272
00:19:52,070 --> 00:19:56,280
 awful really and I feel like I missed a really good

273
00:19:56,280 --> 00:20:01,000
 opportunity to take part in something wholesome."

274
00:20:01,000 --> 00:20:05,190
 And the Buddha said, "Oh no, you didn't actually." Well,

275
00:20:05,190 --> 00:20:07,990
 the Buddha basically said, "Don't ever think of a good deed

276
00:20:07,990 --> 00:20:13,110
 as a trifle. In the end you gave. In the end you were, you

277
00:20:13,110 --> 00:20:15,000
 were relented.

278
00:20:15,000 --> 00:20:18,370
 You could have said no, you could have given nothing. But

279
00:20:18,370 --> 00:20:21,530
 in the end you gave something. You were kind, you were

280
00:20:21,530 --> 00:20:26,000
 generous. You gave something that belonged to you. You had

281
00:20:26,000 --> 00:20:31,000
 no duty or no obligation to give."

282
00:20:31,000 --> 00:20:34,940
 So even that is a good deed, the Buddha said. And he used

283
00:20:34,940 --> 00:20:40,470
 it as an opportunity to tell this verse. And it's kind of,

284
00:20:40,470 --> 00:20:48,180
 I would bet that there's the fact that this was a rich and

285
00:20:48,180 --> 00:20:53,150
 sort of well-to-do affluent individual played a part in

286
00:20:53,150 --> 00:20:56,000
 being fairly positive about this.

287
00:20:56,000 --> 00:20:59,320
 Because you could have warned the guy, you know, "Don't do

288
00:20:59,320 --> 00:21:02,750
 evil. Be careful." And yes, you've done a bad deed, but

289
00:21:02,750 --> 00:21:05,990
 rich people don't tend, rich people who have wealth, who

290
00:21:05,990 --> 00:21:07,000
 have power.

291
00:21:07,000 --> 00:21:13,240
 You attract more flies with honey is the same. Such people

292
00:21:13,240 --> 00:21:17,000
 you often have to be careful with.

293
00:21:17,000 --> 00:21:21,370
 It's funny, the people who are most virtuous and

294
00:21:21,370 --> 00:21:26,700
 spiritually uplifted, you tend to be able to treat them the

295
00:21:26,700 --> 00:21:30,000
 harshest. You tend to be able to be the hardest on them.

296
00:21:30,000 --> 00:21:34,260
 They can take it and it's useful for them and helpful for

297
00:21:34,260 --> 00:21:38,000
 them. But for people who are rich, you're better off to go.

298
00:21:38,000 --> 00:21:43,680
 For people who are spoiled perhaps, you're better off to go

299
00:21:43,680 --> 00:21:50,030
 positive. Nonetheless, there is a point here that any

300
00:21:50,030 --> 00:21:59,920
 amount of goodness should not be disregarded, should not be

301
00:21:59,920 --> 00:22:06,000
 underestimated.

302
00:22:06,000 --> 00:22:10,400
 And so then he taught this verse. And this is like the last

303
00:22:10,400 --> 00:22:13,770
 one, well even more so than the last one. I think this is

304
00:22:13,770 --> 00:22:17,000
 really an awesome verse for us to remember as meditators,

305
00:22:17,000 --> 00:22:22,000
 as Buddhists, as spiritual practitioners.

306
00:22:22,000 --> 00:22:26,640
 Because it's easy to get discouraged when you think of your

307
00:22:26,640 --> 00:22:31,270
 problems, when you think of your goals, when you think of

308
00:22:31,270 --> 00:22:33,000
 spiritual goals.

309
00:22:33,000 --> 00:22:36,690
 It's easy to think, "I'll never be like that. I'll never

310
00:22:36,690 --> 00:22:40,000
 get there. I'll never free myself from this."

311
00:22:40,000 --> 00:22:44,750
 And we become discouraged before we've been tried, because

312
00:22:44,750 --> 00:22:47,000
 it seems like a mountain.

313
00:22:47,000 --> 00:22:50,400
 But even a mountain, when you get up close, it's made of

314
00:22:50,400 --> 00:22:58,000
 rock. It's something that you can take apart.

315
00:22:58,000 --> 00:23:01,560
 All good things come step by step, the journey of a

316
00:23:01,560 --> 00:23:04,640
 thousand miles. It starts with the first step. It's a very

317
00:23:04,640 --> 00:23:08,000
 important concept, because this is really how it works.

318
00:23:08,000 --> 00:23:12,750
 Goodness doesn't disappear. Our good deeds don't just

319
00:23:12,750 --> 00:23:14,000
 disappear.

320
00:23:14,000 --> 00:23:24,440
 Every success has to come from individual good deeds. And

321
00:23:24,440 --> 00:23:30,200
 so whether it be generosity, and therefore the result being

322
00:23:30,200 --> 00:23:36,000
 affluence or high status,

323
00:23:36,000 --> 00:23:40,500
 all the good mundane things that come from being generous,

324
00:23:40,500 --> 00:23:45,000
 whether it be having good friends, all these good results,

325
00:23:45,000 --> 00:23:54,370
 they all come from small deeds, from small acts of kindness

326
00:23:54,370 --> 00:23:55,000
.

327
00:23:55,000 --> 00:24:00,830
 Because our lives are this way. Our lives work moment by

328
00:24:00,830 --> 00:24:05,000
 moment, and every moment we can do wholesomeness,

329
00:24:05,000 --> 00:24:09,000
 we can do a good deed, we can do a bad deed.

330
00:24:09,000 --> 00:24:12,570
 Every moment there's an opportunity, whether it be through

331
00:24:12,570 --> 00:24:18,000
 generosity or morality, or simply through meditation.

332
00:24:18,000 --> 00:24:22,410
 So our ethics, we don't have to have lofty ethics. Ethics

333
00:24:22,410 --> 00:24:25,000
 aren't about the principles themselves.

334
00:24:25,000 --> 00:24:29,600
 It's about the acts. It's about our state of mind and our

335
00:24:29,600 --> 00:24:34,000
 momentary interaction with the world around us.

336
00:24:34,000 --> 00:24:39,960
 And meditation likewise, enlightenment isn't something that

337
00:24:39,960 --> 00:24:45,000
 you jump to, or you fall into, or you break open.

338
00:24:45,000 --> 00:24:51,140
 It's not an instantaneous thing. It comes step by step and

339
00:24:51,140 --> 00:24:53,000
 moment by moment.

340
00:24:53,000 --> 00:24:56,000
 Every moment we have the opportunity to do good deeds.

341
00:24:56,000 --> 00:24:59,570
 Every moment that we're mindful, when you're mindful of the

342
00:24:59,570 --> 00:25:03,000
 foot lifting, when you're mindful of the foot moving,

343
00:25:03,000 --> 00:25:04,000
 mindful of the foot placing,

344
00:25:04,000 --> 00:25:08,300
 when you're mindful of the stomach rising, that one moment

345
00:25:08,300 --> 00:25:10,000
 is a wholesome mind.

346
00:25:10,000 --> 00:25:13,060
 When you see that you're angry and you remind yourself, "

347
00:25:13,060 --> 00:25:17,000
This is anger. It's not me. It's not mine. It's just anger."

348
00:25:17,000 --> 00:25:21,000
 When you see something and you remind yourself, "It's you."

349
00:25:21,000 --> 00:25:24,970
 Every time you do a good deed like this, when you're kind

350
00:25:24,970 --> 00:25:27,900
 to someone, when you're generous, when you're compassionate

351
00:25:27,900 --> 00:25:29,000
, when you're helpful,

352
00:25:29,000 --> 00:25:33,960
 when you're patient, when you're frustrated and impatient

353
00:25:33,960 --> 00:25:38,890
 with people, and then you remind yourself impatient or

354
00:25:38,890 --> 00:25:40,000
 frustrated,

355
00:25:40,000 --> 00:25:45,710
 and as a result become more patient, the power of this is

356
00:25:45,710 --> 00:25:48,000
 not to be underestimated.

357
00:25:48,000 --> 00:25:52,280
 Because this is how true power comes about, not in a single

358
00:25:52,280 --> 00:25:55,000
 bound, not by one great act.

359
00:25:55,000 --> 00:25:59,000
 You don't break through to become a good person.

360
00:25:59,000 --> 00:26:02,890
 Goodness comes moment by moment. It's something anyone can

361
00:26:02,890 --> 00:26:07,000
 do because it's here and now and it's moment by moment.

362
00:26:07,000 --> 00:26:10,350
 This is what's so awesome about this path, this practice,

363
00:26:10,350 --> 00:26:12,000
 Buddhism, meditation.

364
00:26:12,000 --> 00:26:17,200
 It's not something lofty or difficult. As difficult as it

365
00:26:17,200 --> 00:26:19,000
 is, it's just moments.

366
00:26:19,000 --> 00:26:24,370
 It's something you can do any moment, every moment that you

367
00:26:24,370 --> 00:26:26,000
 do a good deed.

368
00:26:26,000 --> 00:26:32,000
 It's one step closer to the goal. It's one step higher,

369
00:26:32,000 --> 00:26:35,490
 step up on the ladder, bringing you higher, making you

370
00:26:35,490 --> 00:26:36,000
 happier,

371
00:26:36,000 --> 00:26:43,180
 bringing peace and clarity and freedom to you and to those

372
00:26:43,180 --> 00:26:45,000
 around you.

373
00:26:45,000 --> 00:26:47,710
 The main thing about this story that doesn't really come

374
00:26:47,710 --> 00:26:50,000
 out in the verse is about encouraging others.

375
00:26:50,000 --> 00:26:54,410
 So not only should we encourage others to be generous, but

376
00:26:54,410 --> 00:26:56,000
 even more importantly,

377
00:26:56,000 --> 00:27:00,450
 we should encourage others to be ethical and spiritual,

378
00:27:00,450 --> 00:27:02,000
 contemplative.

379
00:27:02,000 --> 00:27:05,070
 We should encourage others to meditate, to better

380
00:27:05,070 --> 00:27:09,000
 themselves, to not just be content to be an ordinary person

381
00:27:09,000 --> 00:27:09,000
,

382
00:27:09,000 --> 00:27:12,600
 to get old, sick and die and let the world forget about

383
00:27:12,600 --> 00:27:16,000
 them, but to move forward and upward

384
00:27:16,000 --> 00:27:20,990
 and to make use of the time that they have and the energy

385
00:27:20,990 --> 00:27:25,000
 that they have and the life that they have,

386
00:27:25,000 --> 00:27:36,720
 to put it to some use, to bring it to have value, true

387
00:27:36,720 --> 00:27:39,000
 value.

388
00:27:39,000 --> 00:27:47,200
 So in encouraging others, we gain this extra support of

389
00:27:47,200 --> 00:27:50,000
 having good people around us.

390
00:27:50,000 --> 00:27:53,610
 If you don't encourage others in goodness, well, you can be

391
00:27:53,610 --> 00:27:55,000
 as good as you want.

392
00:27:55,000 --> 00:27:57,550
 It's not sure who you'll be surrounded by. You'll often be

393
00:27:57,550 --> 00:27:59,000
 surrounded by people who complain about it

394
00:27:59,000 --> 00:28:02,350
 because they don't have the sense of the importance of

395
00:28:02,350 --> 00:28:07,590
 goodness and they haven't had the taste of the fruit of

396
00:28:07,590 --> 00:28:10,000
 goodness.

397
00:28:10,000 --> 00:28:12,810
 So you end up often being surrounded by people who don't

398
00:28:12,810 --> 00:28:16,000
 understand goodness, who don't appreciate goodness,

399
00:28:16,000 --> 00:28:19,110
 who don't have the clarity and the happiness that comes

400
00:28:19,110 --> 00:28:20,000
 from goodness,

401
00:28:20,000 --> 00:28:24,550
 and therefore surrounded often by miserable people. We don

402
00:28:24,550 --> 00:28:26,000
't want that.

403
00:28:26,000 --> 00:28:29,780
 So it's very important to when we do good deeds, to

404
00:28:29,780 --> 00:28:32,000
 encourage other people in it

405
00:28:32,000 --> 00:28:38,000
 and to work together to perform good deeds.

406
00:28:38,000 --> 00:28:42,550
 So anyway, that's the Dhammapada verse tonight. We want

407
00:28:42,550 --> 00:28:49,000
 more teaching on good things, bad things, spiritual things.

408
00:28:49,000 --> 00:28:53,990
 Thank you all for tuning in. Wishing you all good practice

409
00:28:53,990 --> 00:28:58,000
 and success in practicing together in a good way.

410
00:28:58,000 --> 00:29:01,000
 Thank you. Have a good night.

411
00:29:02,000 --> 00:29:12,000
 Okay.

412
00:29:12,000 --> 00:29:31,000
 So now we can move to a live broadcast.

413
00:29:31,000 --> 00:29:43,000
 Okay, we're going to go live video.

414
00:29:43,000 --> 00:29:50,000
 Good evening, everyone.

415
00:29:50,000 --> 00:29:56,640
 For those of you who are watching this or joining us live

416
00:29:56,640 --> 00:29:58,000
 on YouTube,

417
00:29:58,000 --> 00:30:02,660
 we've just recorded a Dhammapada video and now we could

418
00:30:02,660 --> 00:30:08,000
 answer, I could answer some questions if anybody has them.

419
00:30:08,000 --> 00:30:18,000
 Or we could just say good night.

420
00:30:18,000 --> 00:30:21,320
 I didn't mention that I don't think, did I mention about

421
00:30:21,320 --> 00:30:24,000
 the digital Pali reader? I don't think I did.

422
00:30:24,000 --> 00:30:30,960
 No. The digital Pali reader doesn't really work anymore. It

423
00:30:30,960 --> 00:30:32,000
 does, but it may not soon.

424
00:30:32,000 --> 00:30:34,000
 What changed, Fante?

425
00:30:34,000 --> 00:30:45,340
 Firefox Mozilla started being somewhat fascist, dictatorial

426
00:30:45,340 --> 00:30:50,000
 about their add-on policy.

427
00:30:50,000 --> 00:30:53,720
 So now we have to actually submit our add-ons for their

428
00:30:53,720 --> 00:30:58,710
 review or Firefox won't even install them. It's kind of

429
00:30:58,710 --> 00:31:01,000
 ridiculous.

430
00:31:01,000 --> 00:31:06,000
 I had to upload all my add-ons to their site.

431
00:31:06,000 --> 00:31:13,000
 Many people have contacted me.

432
00:31:13,000 --> 00:31:14,000
 Don't blame me.

433
00:31:14,000 --> 00:31:17,060
 But there's a way to get around it. For those of you who

434
00:31:17,060 --> 00:31:21,170
 are wondering, I just had to, just tonight I had to go, I

435
00:31:21,170 --> 00:31:23,000
 had to get around it again, I think.

436
00:31:23,000 --> 00:31:27,020
 You have to go into your configuration and switch something

437
00:31:27,020 --> 00:31:30,840
 to false, something about signatures. You can read about it

438
00:31:30,840 --> 00:31:40,000
. There's sites that tell you how to do it.

439
00:31:40,000 --> 00:31:44,000
 Would it be any easier to use Google instead of Firefox?

440
00:31:44,000 --> 00:31:51,280
 No, it's integrated with Firefox. I made a decision because

441
00:31:51,280 --> 00:31:54,000
 of how open Mozilla was.

442
00:31:54,000 --> 00:31:59,920
 That Firefox was easier and more powerful. But now it seems

443
00:31:59,920 --> 00:32:02,000
 like that was a bit of a mistake.

444
00:32:02,000 --> 00:32:12,110
 I don't see any questions, just a comment that someone's

445
00:32:12,110 --> 00:32:23,000
 having trouble with your meditation timer, not working.

446
00:32:23,000 --> 00:32:29,240
 Well, if anyone wants to troubleshoot the website or rep

447
00:32:29,240 --> 00:32:35,720
rogram it, feel free. I think it's at this point as is until

448
00:32:35,720 --> 00:32:39,000
 we get some real developers.

449
00:32:39,000 --> 00:32:43,850
 I wonder, there was someone who was working on a meditation

450
00:32:43,850 --> 00:32:49,000
 app for iPhone for you. Did that ever get finished?

451
00:32:49,000 --> 00:32:55,210
 No, I didn't hear back at all about that. It was kind of

452
00:32:55,210 --> 00:32:58,000
 slick what they were doing.

453
00:32:58,000 --> 00:33:08,000
 Yeah, I remember seeing the little preview.

454
00:33:08,000 --> 00:33:16,180
 Okay, well, maybe we'll call it a night. Thank you all for

455
00:33:16,180 --> 00:33:19,000
 tuning in. Sorry, there's not much to see here.

456
00:33:19,000 --> 00:33:23,990
 We'll have a Dama Padra video up soon. So see you all

457
00:33:23,990 --> 00:33:28,230
 tomorrow. Tomorrow's the oh, tomorrow here is something we

458
00:33:28,230 --> 00:33:32,000
 could have a meditate into the new year. How about that?

459
00:33:32,000 --> 00:33:36,600
 That would be nice. Tomorrow we'll have broadcast at nine

460
00:33:36,600 --> 00:33:41,990
 and then after the broadcast, we'll just meditate until

461
00:33:41,990 --> 00:33:46,000
 midnight together.

462
00:33:46,000 --> 00:33:50,400
 That sounds good. Thank you, Bonta. Okay, so everyone come

463
00:33:50,400 --> 00:33:52,000
 on tomorrow.

464
00:33:52,000 --> 00:33:57,400
 930. 9. We'll have the broadcast and we'll go all the way

465
00:33:57,400 --> 00:34:01,880
 to midnight. We won't broadcast till midnight, but we'll do

466
00:34:01,880 --> 00:34:05,000
 it on our meditation site.

467
00:34:05,000 --> 00:34:08,610
 You don't have to meditate for all the hours, but you can

468
00:34:08,610 --> 00:34:10,000
 meditate on and off.

469
00:34:10,000 --> 00:34:14,860
 And at midnight, we'll all come back on the meditation site

470
00:34:14,860 --> 00:34:19,430
 and just say, Happy New Year, wish each other Happy New

471
00:34:19,430 --> 00:34:20,000
 Year.

472
00:34:20,000 --> 00:34:25,920
 Just an excuse to get together, get together and do good

473
00:34:25,920 --> 00:34:27,000
 deeds.

474
00:34:27,000 --> 00:34:29,000
 Okay, good night.

475
00:34:29,000 --> 00:34:31,000
 Good night, Bonta. Thank you.

476
00:34:31,000 --> 00:34:32,000
 Thank you.

477
00:34:32,000 --> 00:34:36,000
 Thank you.

