1
00:00:00,000 --> 00:00:06,990
 Hi, so this is a question on whether awful states of mind

2
00:00:06,990 --> 00:00:09,160
 or negative states of mind

3
00:00:09,160 --> 00:00:14,020
 or addictions and so on are considered wrong concentration

4
00:00:14,020 --> 00:00:15,760
 or micha samadhi.

5
00:00:15,760 --> 00:00:23,110
 Now I'm not really sure about the technical details here,

6
00:00:23,110 --> 00:00:26,800
 but just off the top of my head,

7
00:00:26,800 --> 00:00:30,570
 micha samadhi is wrong concentration is when you focus your

8
00:00:30,570 --> 00:00:32,760
 mind on something that is associated

9
00:00:32,760 --> 00:00:33,760
 with negative states.

10
00:00:33,760 --> 00:00:39,050
 So this is at least bordering on wrong concentration, but I

11
00:00:39,050 --> 00:00:42,360
 think the point here is that these things

12
00:00:42,360 --> 00:00:46,890
 are probably arising on their own, so there's probably no

13
00:00:46,890 --> 00:00:48,480
 conscious focus on it.

14
00:00:48,480 --> 00:00:51,720
 Wrong concentration is when you deliberately focus on

15
00:00:51,720 --> 00:00:53,800
 something negative and encourage

16
00:00:53,800 --> 00:00:57,450
 it or incorporate it into your meditation like greed or

17
00:00:57,450 --> 00:00:58,120
 anger.

18
00:00:58,120 --> 00:01:00,620
 But when these states come up, then we're not talking about

19
00:01:00,620 --> 00:01:01,680
 wrong concentration, we're

20
00:01:01,680 --> 00:01:05,030
 talking about meditation practice where you're considering

21
00:01:05,030 --> 00:01:06,840
 and contemplating the negative

22
00:01:06,840 --> 00:01:11,790
 and positive states which according to the tradition, it's

23
00:01:11,790 --> 00:01:13,560
 perfectly proper and correct

24
00:01:13,560 --> 00:01:15,180
 to contemplate.

25
00:01:15,180 --> 00:01:18,600
 So I would encourage the contemplation of these things to

26
00:01:18,600 --> 00:01:20,240
 see that they are negative

27
00:01:20,240 --> 00:01:23,120
 and to see that they are unhelpful, to see for yourself,

28
00:01:23,120 --> 00:01:25,120
 not just pushing them away and

29
00:01:25,120 --> 00:01:27,000
 trying to forget about them.

30
00:01:27,000 --> 00:01:30,760
 They shouldn't be repressed, they shouldn't be forgotten

31
00:01:30,760 --> 00:01:32,280
 about or ignored, they should

32
00:01:32,280 --> 00:01:34,880
 be analyzed and understood.

33
00:01:34,880 --> 00:01:38,490
 And once you understand them, you'll be able to let go of

34
00:01:38,490 --> 00:01:40,880
 them simply by saying to yourself,

35
00:01:40,880 --> 00:01:44,650
 liking, liking, wanting, wanting, or focusing on the

36
00:01:44,650 --> 00:01:47,440
 feelings associated with it as in happy,

37
00:01:47,440 --> 00:01:52,820
 happy or if it's a negative one, angry, angry or pain, pain

38
00:01:52,820 --> 00:01:53,640
 or so on.

39
00:01:53,640 --> 00:01:57,640
 So I wouldn't worry about these things, but obviously

40
00:01:57,640 --> 00:02:00,280
 paying very close attention to them,

41
00:02:00,280 --> 00:02:04,970
 not encouraging them, not tolerating them in the sense of

42
00:02:04,970 --> 00:02:07,300
 thinking it's okay to follow

43
00:02:07,300 --> 00:02:08,720
 your addictions and so on.

44
00:02:08,720 --> 00:02:13,700
 They should be very carefully acknowledged, contemplated

45
00:02:13,700 --> 00:02:16,880
 and discarded for being negative.

46
00:02:16,880 --> 00:02:19,720
 I hope that clears that up.

47
00:02:19,720 --> 00:02:22,270
 It's not wrong that these things are coming up, but they

48
00:02:22,270 --> 00:02:23,720
 shouldn't be encouraged and they

49
00:02:23,720 --> 00:02:25,760
 certainly shouldn't be focused on.

50
00:02:25,760 --> 00:02:27,320
 Keep the questions coming.

