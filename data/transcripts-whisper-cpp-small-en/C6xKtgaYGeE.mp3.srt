1
00:00:00,000 --> 00:00:02,500
 Is thinking a good thing or a bad thing?

2
00:00:02,500 --> 00:00:05,000
 If we don't think, how will we live?

3
00:00:05,000 --> 00:00:09,500
 Well, is living a good thing or a bad thing?

4
00:00:09,500 --> 00:00:14,500
 If you need to think to live, then...

5
00:00:14,500 --> 00:00:17,000
 then...

6
00:00:17,000 --> 00:00:20,500
 you have to ask yourself, "Do I need to live?"

7
00:00:20,500 --> 00:00:26,190
 If you need to live, then, well, I guess that means you

8
00:00:26,190 --> 00:00:27,000
 need to think.

9
00:00:28,500 --> 00:00:31,940
 Of course, some people can live without thinking, I suppose

10
00:00:31,940 --> 00:00:32,000
,

11
00:00:32,000 --> 00:00:35,000
 or without thinking a lot, so...

12
00:00:35,000 --> 00:00:38,450
 it's not true that you can't... you stop living if you stop

13
00:00:38,450 --> 00:00:39,000
 thinking.

14
00:00:39,000 --> 00:00:46,500
 But whether thinking is a good thing or a bad thing really

15
00:00:46,500 --> 00:00:49,000
 depends on your perspective and on your belief,

16
00:00:49,000 --> 00:00:52,110
 because it's actually not a good thing or a bad thing

17
00:00:52,110 --> 00:00:53,000
 intrinsically.

18
00:00:53,000 --> 00:00:55,000
 It's just a thing.

19
00:00:56,000 --> 00:00:59,000
 And the Buddha's teaching is to realize that,

20
00:00:59,000 --> 00:01:04,000
 to let go of your attachments to thoughts as good or bad,

21
00:01:04,000 --> 00:01:07,000
 and desire for new thoughts,

22
00:01:07,000 --> 00:01:11,000
 or even your desire for not having thoughts.

23
00:01:11,000 --> 00:01:18,000
 To give up entirely thinking, and to let go.

