1
00:00:00,000 --> 00:00:04,000
 Hello, welcome back to Ask a Monk.

2
00:00:04,000 --> 00:00:14,100
 Today's question is on whether I think that there is more

3
00:00:14,100 --> 00:00:17,000
 suffering in America,

4
00:00:17,000 --> 00:00:22,140
 this is the question, where it is almost impossible not to

5
00:00:22,140 --> 00:00:23,000
 find food,

6
00:00:23,000 --> 00:00:29,270
 and I guess because people in America have developed their

7
00:00:29,270 --> 00:00:31,000
 society,

8
00:00:31,000 --> 00:00:35,840
 or more suffering in a tribal society. This is the question

9
00:00:35,840 --> 00:00:36,000
.

10
00:00:36,000 --> 00:00:45,000
 So, first of all, I'd like to sort of expand the question,

11
00:00:45,000 --> 00:00:50,250
 because I think the important point is whether, not whether

12
00:00:50,250 --> 00:00:52,000
 America now,

13
00:00:52,000 --> 00:00:57,200
 or America when it was owned by the Native American or

14
00:00:57,200 --> 00:01:00,000
 First Nations people, is better.

15
00:01:00,000 --> 00:01:06,000
 The question is whether there's more suffering to be found

16
00:01:06,000 --> 00:01:08,000
 in a society

17
00:01:08,000 --> 00:01:17,000
 with technological advancements and politics and government

18
00:01:17,000 --> 00:01:18,000
 and roads

19
00:01:18,000 --> 00:01:23,000
 and cars and computers and iPods and so on,

20
00:01:23,000 --> 00:01:32,170
 or whether there is more suffering in a lifestyle that is

21
00:01:32,170 --> 00:01:36,000
 simple, unrefined,

22
00:01:36,000 --> 00:01:43,930
 unadvanced and unorganized, organized on a level similar to

23
00:01:43,930 --> 00:01:45,000
 a tribe.

24
00:01:45,000 --> 00:01:50,100
 It's a difficult question, but why it's difficult is

25
00:01:50,100 --> 00:01:52,000
 because I think there's merits to both sides.

26
00:01:52,000 --> 00:01:58,060
 I want to be able to say that material advancement in some

27
00:01:58,060 --> 00:02:03,000
 way heightens people's ability

28
00:02:03,000 --> 00:02:11,830
 or interest in things like science, things like objective

29
00:02:11,830 --> 00:02:15,000
 investigation and analysis and so on.

30
00:02:15,000 --> 00:02:18,820
 I mean, the education side of things, I want to be able to

31
00:02:18,820 --> 00:02:22,000
 say that that's a good thing to some degree.

32
00:02:22,000 --> 00:02:27,230
 But I think given the choice, I would have to say that

33
00:02:27,230 --> 00:02:29,000
 there's probably less suffering

34
00:02:29,000 --> 00:02:35,000
 to be found in a less advanced society.

35
00:02:35,000 --> 00:02:39,290
 And I think we have to advance this beyond the tribal

36
00:02:39,290 --> 00:02:45,000
 societies of, say, North America in olden times.

37
00:02:45,000 --> 00:02:50,000
 Now there are still some tribal societies, but not really

38
00:02:50,000 --> 00:02:53,000
 to the extent that you would have found some time ago.

39
00:02:53,000 --> 00:02:56,770
 Because I think this is a rather limited sort of way of

40
00:02:56,770 --> 00:02:58,000
 looking at things.

41
00:02:58,000 --> 00:03:01,760
 There are many societies or cultures or groups of

42
00:03:01,760 --> 00:03:05,000
 individuals, groups of people living in the world

43
00:03:05,000 --> 00:03:10,390
 who are living in limited technological advancement and so

44
00:03:10,390 --> 00:03:11,000
 on.

45
00:03:11,000 --> 00:03:18,100
 I think living in Thailand and living in Sri Lanka, I've

46
00:03:18,100 --> 00:03:19,000
 been able to see some of these groups.

47
00:03:19,000 --> 00:03:24,480
 I mean, Thailand, for example, is quite advanced in many

48
00:03:24,480 --> 00:03:28,000
 ways, but in the villages, in the countryside,

49
00:03:28,000 --> 00:03:31,540
 you'll still find people who are very technologically

50
00:03:31,540 --> 00:03:36,000
 behind and living in fairly unorganized manners

51
00:03:36,000 --> 00:03:41,000
 in terms of political structure and so on.

52
00:03:41,000 --> 00:03:47,750
 And here in Sri Lanka, even more so, you don't see the

53
00:03:47,750 --> 00:03:51,000
 impact of modern society

54
00:03:51,000 --> 00:03:55,000
 to such a great extent as you would in the West.

55
00:03:55,000 --> 00:04:03,510
 And I think, beneficially, at least to some extent, that it

56
00:04:03,510 --> 00:04:07,000
 has allowed people to be free from some of the suffering.

57
00:04:07,000 --> 00:04:10,260
 So stacking up the sufferings, I think the obvious

58
00:04:10,260 --> 00:04:13,000
 suffering that was pointed out in the question

59
00:04:13,000 --> 00:04:20,000
 is living in a tribal society or an "uncivilized" society.

60
00:04:20,000 --> 00:04:24,720
 It's going to be more difficult to find food, and I don't

61
00:04:24,720 --> 00:04:27,000
 really think this is that big of a deal.

62
00:04:27,000 --> 00:04:31,330
 In fact, I probably would care to wager that in North

63
00:04:31,330 --> 00:04:34,000
 America, it was a lot easier to find food

64
00:04:34,000 --> 00:04:39,350
 before technology came into effect, and now the food

65
00:04:39,350 --> 00:04:41,000
 quality has gone down,

66
00:04:41,000 --> 00:04:44,000
 and people have to work harder for it, and so on.

67
00:04:44,000 --> 00:04:49,000
 I would say, but anyway, that's maybe a bit speculative.

68
00:04:49,000 --> 00:04:53,900
 But the real suffering, I think, that comes, what's being

69
00:04:53,900 --> 00:05:00,000
 pointed to here is the lack of amenities,

70
00:05:00,000 --> 00:05:06,000
 living in rough conditions, having to put up with insects,

71
00:05:06,000 --> 00:05:08,000
 having to put up with leeches,

72
00:05:08,000 --> 00:05:11,870
 having to put up with snakes and scorpions, having to put

73
00:05:11,870 --> 00:05:13,000
 up with mosquitoes,

74
00:05:13,000 --> 00:05:16,660
 because you don't have screens on your windows, or you don

75
00:05:16,660 --> 00:05:20,000
't have the technology to prevent these things,

76
00:05:20,000 --> 00:05:24,610
 having to put up with limited medical facilities, and so on

77
00:05:24,610 --> 00:05:25,000
.

78
00:05:25,000 --> 00:05:28,890
 There are a lot of sufferings that come with living in a

79
00:05:28,890 --> 00:05:30,000
 simple manner,

80
00:05:30,000 --> 00:05:35,450
 and I think it's fair to point out that the monastic life

81
00:05:35,450 --> 00:05:38,000
 is, by its nature,

82
00:05:38,000 --> 00:05:44,300
 the Buddhist monastic life is of a sort of simple life, and

83
00:05:44,300 --> 00:05:47,000
 as a result, there is a certain amount of suffering.

84
00:05:47,000 --> 00:05:49,250
 This is why I think this is an interesting question, and

85
00:05:49,250 --> 00:05:51,000
 why I'd like to take some time to answer it,

86
00:05:51,000 --> 00:05:54,630
 because I have to personally go through some of the suffer

87
00:05:54,630 --> 00:05:59,000
ings that the villagers here even have to go through,

88
00:05:59,000 --> 00:06:02,000
 perhaps even more so than they have to go through.

89
00:06:02,000 --> 00:06:06,240
 I might be going through some of the sufferings that you

90
00:06:06,240 --> 00:06:10,000
 might expect from a tribal society,

91
00:06:10,000 --> 00:06:14,780
 because, for me, food is limited. I only eat one meal a day

92
00:06:14,780 --> 00:06:15,000
,

93
00:06:15,000 --> 00:06:18,570
 so you could say that's somehow similar to the suffering

94
00:06:18,570 --> 00:06:22,000
 that is brought about by having to hunt for your food,

95
00:06:22,000 --> 00:06:24,320
 not knowing where you're going to get your next meal, not

96
00:06:24,320 --> 00:06:28,000
 having a farmland or a grocery store or money

97
00:06:28,000 --> 00:06:33,780
 that you can easily access these things with, and having to

98
00:06:33,780 --> 00:06:36,000
 put up with limited medical facilities.

99
00:06:36,000 --> 00:06:43,000
 But I think the real key here is that, or a real good way

100
00:06:43,000 --> 00:06:48,000
 to answer this question is to point out the fact that

101
00:06:48,000 --> 00:06:52,810
 so many people undertake this lifestyle on purpose and

102
00:06:52,810 --> 00:06:54,000
 voluntarily,

103
00:06:54,000 --> 00:06:56,820
 and why would they do it if it was just going to lead to

104
00:06:56,820 --> 00:06:58,000
 their suffering?

105
00:06:58,000 --> 00:07:00,000
 And I think that's a question that a lot of people ask.

106
00:07:00,000 --> 00:07:06,100
 Why do people decide to live in caves and wear robes, a

107
00:07:06,100 --> 00:07:10,000
 single set of robes, etc., etc.,

108
00:07:10,000 --> 00:07:13,700
 or eating only one meal a day, putting up with all of these

109
00:07:13,700 --> 00:07:15,000
 difficulties?

110
00:07:15,000 --> 00:07:18,000
 And I think it's because it's a trade-off.

111
00:07:18,000 --> 00:07:26,410
 If you give up, or if you want to be free from all of the

112
00:07:26,410 --> 00:07:31,700
 stresses and all of the sufferings that are on the other

113
00:07:31,700 --> 00:07:35,000
 side of the fence,

114
00:07:35,000 --> 00:07:38,080
 then you have to, you can't expect to have all the

115
00:07:38,080 --> 00:07:39,000
 amenities.

116
00:07:39,000 --> 00:07:40,000
 I mean, they go hand in hand.

117
00:07:40,000 --> 00:07:43,940
 The sufferings on the other side of a technologically

118
00:07:43,940 --> 00:07:48,400
 advanced society are the stresses of having to work a 9-5

119
00:07:48,400 --> 00:07:49,000
 job,

120
00:07:49,000 --> 00:07:52,520
 the stresses of having to think a lot, of having to deal

121
00:07:52,520 --> 00:07:55,000
 with people, crowds of people,

122
00:07:55,000 --> 00:07:57,530
 having to deal with traffic, having to deal with the

123
00:07:57,530 --> 00:08:00,000
 complications that come with technology,

124
00:08:00,000 --> 00:08:07,860
 like fixing your car and your house and all of the laws,

125
00:08:07,860 --> 00:08:11,540
 and having to deal with laws and lawyers and people suing

126
00:08:11,540 --> 00:08:13,000
 you and so on.

127
00:08:13,000 --> 00:08:16,390
 There's a huge burden of stress, having to be worried about

128
00:08:16,390 --> 00:08:21,000
 thieves, having to be worried about crooks,

129
00:08:21,000 --> 00:08:25,450
 not crook, but people cheating you, having to be worried

130
00:08:25,450 --> 00:08:30,000
 about your boss, your co-workers, your clients and so on,

131
00:08:30,000 --> 00:08:32,000
 and a million other things.

132
00:08:32,000 --> 00:08:36,180
 I mean, no matter what work you apply yourself to, in an

133
00:08:36,180 --> 00:08:38,000
 advanced society, there are more complications,

134
00:08:38,000 --> 00:08:41,820
 and as a result, more stress, and as a result, more

135
00:08:41,820 --> 00:08:43,000
 suffering.

136
00:08:43,000 --> 00:08:50,820
 So I think the trade-off there is quite worth it in my mind

137
00:08:50,820 --> 00:08:54,680
, because I'd like to, you know, the answer for me is quite

138
00:08:54,680 --> 00:08:55,000
 clear,

139
00:08:55,000 --> 00:08:57,690
 because having put up with all these sufferings of having

140
00:08:57,690 --> 00:09:02,000
 to have mosquitoes bite me and who knows what bite me,

141
00:09:02,000 --> 00:09:07,180
 and having to put up with leeches and, you know, the

142
00:09:07,180 --> 00:09:09,000
 worries that I have to put up with,

143
00:09:09,000 --> 00:09:12,840
 worrying about stepping on a snake, worrying about that

144
00:09:12,840 --> 00:09:16,610
 four-foot lizard down below me and the monkeys crapping on

145
00:09:16,610 --> 00:09:18,000
 my head and so on.

146
00:09:18,000 --> 00:09:23,340
 I really don't feel upset or affected by these things. They

147
00:09:23,340 --> 00:09:26,400
're a physical reality, and they're much more physical than

148
00:09:26,400 --> 00:09:28,000
 they are mental.

149
00:09:28,000 --> 00:09:33,290
 I feel so much more at peace here, living in the jungle,

150
00:09:33,290 --> 00:09:36,350
 which, you know, might look peaceful, but is actually quite

151
00:09:36,350 --> 00:09:37,000
 dangerous,

152
00:09:37,000 --> 00:09:40,000
 and keeps you on your toes in many ways.

153
00:09:40,000 --> 00:09:42,680
 Even during meditation, you have to, you know, there's

154
00:09:42,680 --> 00:09:45,000
 these little bugs that you can't even see them.

155
00:09:45,000 --> 00:09:49,000
 They're worse than mosquitoes, and they bite you.

156
00:09:49,000 --> 00:09:53,980
 But it's not a stress for me, not in the same way as living

157
00:09:53,980 --> 00:09:59,000
 in, say, the big cities of Colombo, Bangkok, Los Angeles.

158
00:09:59,000 --> 00:10:03,140
 The stresses that's involved there, you know, is an order

159
00:10:03,140 --> 00:10:05,000
 of magnitude greater.

160
00:10:05,000 --> 00:10:08,590
 So I'm not sure this exactly answers the question, but this

161
00:10:08,590 --> 00:10:11,000
 is how I would prefer to approach it.

162
00:10:11,000 --> 00:10:16,750
 I think there's far less stress to be had from a simple

163
00:10:16,750 --> 00:10:24,230
 lifestyle, because the stresses are much more physical than

164
00:10:24,230 --> 00:10:25,000
 they are mental,

165
00:10:25,000 --> 00:10:30,000
 and the mental stress is far less.

166
00:10:30,000 --> 00:10:34,050
 Another thing about living in a simple society, in a simple

167
00:10:34,050 --> 00:10:39,440
 lifestyle, is that it's much more in tune with our

168
00:10:39,440 --> 00:10:41,000
 programming physically.

169
00:10:41,000 --> 00:10:44,000
 I mean, we're still very much related to the monkeys.

170
00:10:44,000 --> 00:10:49,550
 And so this kind of atmosphere jives with us, seeing the

171
00:10:49,550 --> 00:10:52,240
 trees, why people go to nature and they feel peaceful all

172
00:10:52,240 --> 00:10:53,000
 of a sudden,

173
00:10:53,000 --> 00:10:55,260
 is because of how, it's not because there's anything

174
00:10:55,260 --> 00:10:58,000
 special there, it's because of what's not there.

175
00:10:58,000 --> 00:11:01,810
 There's nothing jarring with your experience, no horns

176
00:11:01,810 --> 00:11:06,340
 blasting, no bright lights, no beautiful pictures catching

177
00:11:06,340 --> 00:11:07,000
 your attention,

178
00:11:07,000 --> 00:11:09,000
 no ugly sights and so on.

179
00:11:09,000 --> 00:11:14,000
 It's very peaceful, very calm, very natural, right?

180
00:11:14,000 --> 00:11:16,000
 I mean, that's why we're here.

181
00:11:16,000 --> 00:11:21,000
 And I think you'll find that in a traditional society.

182
00:11:21,000 --> 00:11:25,860
 So I think, and I think this is borne out by the Buddhist

183
00:11:25,860 --> 00:11:30,150
 teaching, it's borne out by the examples, obviously, of

184
00:11:30,150 --> 00:11:32,000
 those people who have ordained as monks.

185
00:11:32,000 --> 00:11:34,640
 There was a story in the tepidaka, in the Buddhist teaching

186
00:11:34,640 --> 00:11:38,000
, that we always go back to, I think, in the commentaries,

187
00:11:38,000 --> 00:11:38,000
 actually.

188
00:11:38,000 --> 00:11:40,000
 I'm not sure.

189
00:11:40,000 --> 00:11:45,110
 Well, at least it's in the commentaries about this king, a

190
00:11:45,110 --> 00:11:51,000
 relative of the, a king who became a monk, Mahakapina.

191
00:11:51,000 --> 00:11:55,000
 And after he became a monk, he would sit under this tree.

192
00:11:55,000 --> 00:11:57,000
 He used to be a king, right?

193
00:11:57,000 --> 00:11:59,820
 So after he became a monk, he would sit under a tree and go

194
00:11:59,820 --> 00:12:04,000
, "Ahosukang, ahosukang, ahosukang,"

195
00:12:04,000 --> 00:12:08,820
 which means, "Oh, what happiness. Oh, what bliss. Oh, what

196
00:12:08,820 --> 00:12:10,000
 happiness."

197
00:12:10,000 --> 00:12:12,360
 So you might say he was actually contemplating the

198
00:12:12,360 --> 00:12:16,240
 happiness, just looking at it and examining it in the same

199
00:12:16,240 --> 00:12:20,000
 way that we say to ourselves, "Happy, happy."

200
00:12:20,000 --> 00:12:23,430
 But the monks heard him saying this and remarking this, and

201
00:12:23,430 --> 00:12:26,000
 they took it the wrong way.

202
00:12:26,000 --> 00:12:29,020
 They took it to mean he was, for some reason they took it

203
00:12:29,020 --> 00:12:32,000
 that he must be thinking about his kingly happiness.

204
00:12:32,000 --> 00:12:35,450
 He was remembering, "Oh, what happiness it was to be a king

205
00:12:35,450 --> 00:12:36,000
."

206
00:12:36,000 --> 00:12:38,470
 And so they took him to the Buddha, and the Buddha asked

207
00:12:38,470 --> 00:12:40,000
 him, you know, "What was it?"

208
00:12:40,000 --> 00:12:44,310
 And he said, "No. When I was a king, I had to worry about

209
00:12:44,310 --> 00:12:47,000
 all of these stresses and concerns.

210
00:12:47,000 --> 00:12:50,000
 My life was always in danger of assassination.

211
00:12:50,000 --> 00:12:52,790
 I had all this treasure that I had to guard, all these

212
00:12:52,790 --> 00:12:56,000
 ministers that I had to manage and, you know,

213
00:12:56,000 --> 00:12:58,820
 to stop from cheating me and all the people who had come to

214
00:12:58,820 --> 00:13:02,270
 me with their problems and all the laws I had to enforce

215
00:13:02,270 --> 00:13:04,000
 and so on and so on.

216
00:13:04,000 --> 00:13:07,430
 And I have none of that now. Now I have this tree, three

217
00:13:07,430 --> 00:13:09,000
 robes, my bowl, my tree.

218
00:13:09,000 --> 00:13:13,390
 And this is the greatest happiness I've ever known in my

219
00:13:13,390 --> 00:13:15,000
 life," he said.

220
00:13:15,000 --> 00:13:20,000
 So there's a story to go with this question.

221
00:13:20,000 --> 00:13:24,710
 I think one final note is that regardless of the answer to

222
00:13:24,710 --> 00:13:28,810
 this question, whether there's more suffering in either one

223
00:13:28,810 --> 00:13:29,000
,

224
00:13:29,000 --> 00:13:32,930
 I think the point is that whatever lifestyle you can

225
00:13:32,930 --> 00:13:37,000
 undertake, not that has the least amount of suffering,

226
00:13:37,000 --> 00:13:40,520
 but which has the least amount of unwholesomeness involved

227
00:13:40,520 --> 00:13:43,000
 in it, that is the best lifestyle.

228
00:13:43,000 --> 00:13:46,110
 So for those people who might be discouraged then by the

229
00:13:46,110 --> 00:13:49,530
 fact that they have to live in a technologically advanced

230
00:13:49,530 --> 00:13:51,000
 society as a Buddhist

231
00:13:51,000 --> 00:13:54,070
 and they feel like they should be living in the forest, I

232
00:13:54,070 --> 00:13:56,320
 don't mean to say that there's anything wrong with living

233
00:13:56,320 --> 00:13:57,000
 in the city.

234
00:13:57,000 --> 00:14:00,440
 I've lived in Los Angeles. I've gone on alms round in North

235
00:14:00,440 --> 00:14:06,000
 Hollywood and in places in Bangkok, in Colombo.

236
00:14:06,000 --> 00:14:09,920
 I've lived in these places in fairly chaotic and stressful

237
00:14:09,920 --> 00:14:11,000
 situations.

238
00:14:11,000 --> 00:14:14,900
 But the point is to live your life in such a way that it's

239
00:14:14,900 --> 00:14:17,000
 free from unwholesomeness.

240
00:14:17,000 --> 00:14:20,520
 And this is where the question gets interesting because

241
00:14:20,520 --> 00:14:24,830
 many tribal societies are involved in what Buddhists would

242
00:14:24,830 --> 00:14:26,000
 call great unwholesomeness.

243
00:14:26,000 --> 00:14:30,000
 Their way of lifestyle is hunting as an example.

244
00:14:30,000 --> 00:14:35,770
 And though they do it to survive, it's by nature caught up

245
00:14:35,770 --> 00:14:39,000
 with these states of cruelty,

246
00:14:39,000 --> 00:14:45,000
 states of anger and delusion.

247
00:14:45,000 --> 00:14:50,000
 And as a result, it keeps them bound to the wheel of life

248
00:14:50,000 --> 00:14:53,000
 that they're going to have to take turns with the deer.

249
00:14:53,000 --> 00:14:57,000
 This is what the First Nations people believe.

250
00:14:57,000 --> 00:15:01,000
 They still believe that you take turns.

251
00:15:01,000 --> 00:15:03,670
 I hunt the deer this life and in the next life I'm the deer

252
00:15:03,670 --> 00:15:05,000
 and they hunt me and so on.

253
00:15:05,000 --> 00:15:08,260
 There are these kinds of beliefs and this is really in line

254
00:15:08,260 --> 00:15:10,000
 with the Buddhist teachings.

255
00:15:10,000 --> 00:15:14,590
 So if you're fine with that, having to be hunted every

256
00:15:14,590 --> 00:15:19,000
 other lifetime or every few lifetimes, then so be it.

257
00:15:19,000 --> 00:15:22,770
 But from a Buddhist point of view, this isn't the way out

258
00:15:22,770 --> 00:15:24,000
 of suffering.

259
00:15:24,000 --> 00:15:26,000
 It's because there's a lot of suffering.

260
00:15:26,000 --> 00:15:29,140
 I mean tribes, of course, then there's tribal warfare and

261
00:15:29,140 --> 00:15:31,000
 they're not free from all of this.

262
00:15:31,000 --> 00:15:34,340
 And we're not free from it on the other side, us living in

263
00:15:34,340 --> 00:15:37,000
 technologically advanced societies.

264
00:15:37,000 --> 00:15:41,000
 Living in Canada, I wasn't free from these things.

265
00:15:41,000 --> 00:15:42,000
 I was hunting as well.

266
00:15:42,000 --> 00:15:44,000
 I didn't need to, but I went hunting with my father.

267
00:15:44,000 --> 00:15:49,580
 We engage in a lot of unwholesomeness in advanced societies

268
00:15:49,580 --> 00:15:50,000
.

269
00:15:50,000 --> 00:15:53,000
 So that's the most important.

270
00:15:53,000 --> 00:15:55,000
 You can live in a city, you can live in nature.

271
00:15:55,000 --> 00:15:58,820
 The reason why we choose to live in nature is I think

272
00:15:58,820 --> 00:16:03,000
 overall the amount of peace that you have is the one side.

273
00:16:03,000 --> 00:16:05,990
 But there's also a lot less unwholesomeness, that you're

274
00:16:05,990 --> 00:16:10,850
 free from having to deal with other people's unwholesomen

275
00:16:10,850 --> 00:16:11,000
ess

276
00:16:11,000 --> 00:16:15,050
 and having to see all these beautiful attractive sights,

277
00:16:15,050 --> 00:16:18,000
 wanting to get this, wanting to get that,

278
00:16:18,000 --> 00:16:21,000
 being annoyed by people, being upset by people.

279
00:16:21,000 --> 00:16:26,390
 So you're able to focus much clearer on the very core

280
00:16:26,390 --> 00:16:30,000
 aspects of what really is reality

281
00:16:30,000 --> 00:16:34,000
 without having to get caught up in unwholesome mind states

282
00:16:34,000 --> 00:16:37,000
 of stress and addiction and so on.

283
00:16:37,000 --> 00:16:39,000
 And I think that's clear.

284
00:16:39,000 --> 00:16:41,770
 I think being here in the forest is much better for my

285
00:16:41,770 --> 00:16:45,000
 meditation practice than being in the city.

286
00:16:45,000 --> 00:16:49,160
 But in the end, it's not the biggest concern and I'm not

287
00:16:49,160 --> 00:16:52,000
 afraid to live in the city if I have to,

288
00:16:52,000 --> 00:16:55,540
 because for me the most important is to continue the

289
00:16:55,540 --> 00:16:59,000
 meditation practice and to learn more about my reactions

290
00:16:59,000 --> 00:17:02,000
 and my interactions with the world around me.

291
00:17:02,000 --> 00:17:05,000
 And sometimes that can be tested by suffering.

292
00:17:05,000 --> 00:17:08,150
 Again, the Buddhist teaching is not to run away from

293
00:17:08,150 --> 00:17:10,630
 suffering, it's to learn and to come to understand

294
00:17:10,630 --> 00:17:11,000
 suffering.

295
00:17:11,000 --> 00:17:13,730
 When you understand suffering, you'll understand what is

296
00:17:13,730 --> 00:17:17,000
 really causing suffering, that it's not the objects,

297
00:17:17,000 --> 00:17:20,280
 it's your relationship and your reactions to them, your

298
00:17:20,280 --> 00:17:22,000
 attachment to things being in a certain way

299
00:17:22,000 --> 00:17:25,080
 and not being in another way, your attachment to the

300
00:17:25,080 --> 00:17:27,000
 objects of the sense and so on.

301
00:17:27,000 --> 00:17:29,570
 When you can be free from that, it doesn't matter where you

302
00:17:29,570 --> 00:17:33,000
 live, whether it be in a cave or in an apartment complex.

303
00:17:33,000 --> 00:17:36,000
 So there's the answer to your question.

304
00:17:36,000 --> 00:17:38,000
 Thanks and have a good day.

