1
00:00:00,000 --> 00:00:05,850
 your study. Peter asked what is the formal way to greet a

2
00:00:05,850 --> 00:00:07,600
 monk in Sri Lanka

3
00:00:07,600 --> 00:00:10,940
 and is it different from other countries? Is high bhante

4
00:00:10,940 --> 00:00:11,920
 acceptable?

5
00:00:11,920 --> 00:00:24,070
 I think high bhante you can say when you're familiar. I

6
00:00:24,070 --> 00:00:28,080
 would possibly call

7
00:00:28,960 --> 00:00:34,980
 say high bhante if I meet bhante you tadamo somewhere here

8
00:00:34,980 --> 00:00:35,920
 in the monastery.

9
00:00:35,920 --> 00:00:44,920
 If it's an official occasion or a monk you don't know don't

10
00:00:44,920 --> 00:00:47,680
 say high bhante I would say rather

11
00:00:55,040 --> 00:01:04,540
 say namasa khan bhante or... That's Thai you know. No, isn

12
00:01:04,540 --> 00:01:05,760
't it? Is it?

13
00:01:05,760 --> 00:01:11,320
 Okay then don't say that. High corruption of the word nam

14
00:01:11,320 --> 00:01:14,560
askara. It would be namaste no?

15
00:01:14,560 --> 00:01:17,570
 Namaste bhante. I would say that's that would be but nam

16
00:01:17,570 --> 00:01:19,600
aste is also no good because that's

17
00:01:19,600 --> 00:01:26,480
 Sanskrit. Well I mean sorry to jump in but yeah that's fine

18
00:01:26,480 --> 00:01:28,400
. Would you say high father

19
00:01:28,400 --> 00:01:33,780
 to a priest or high brother to a Christian monk? I don't

20
00:01:33,780 --> 00:01:35,920
 think people normally would if they

21
00:01:35,920 --> 00:01:38,950
 have respect for the person they would I would think at

22
00:01:38,950 --> 00:01:40,880
 least say hello father or something like

23
00:01:40,880 --> 00:01:44,770
 that because I mean bhante is not a nickname it's a title

24
00:01:44,770 --> 00:01:47,680
 of respect it's like high venerable I

25
00:01:47,680 --> 00:01:52,730
 guess you could say it but it's you know I mean it gets

26
00:01:52,730 --> 00:01:55,280
 ridiculous if you like say

27
00:01:55,280 --> 00:01:58,580
 hi your majesty or something like that you don't really for

28
00:01:58,580 --> 00:01:59,920
 a king or something but

29
00:01:59,920 --> 00:02:04,710
 it's not on that level but because of the the fact that bh

30
00:02:04,710 --> 00:02:07,360
ante is a title of respect I think

31
00:02:07,360 --> 00:02:13,520
 high bhante is a little bit too informal. It differs from

32
00:02:13,520 --> 00:02:15,760
 country to country as we

33
00:02:17,360 --> 00:02:26,960
 experienced just now in Thailand you would say namaskara or

34
00:02:26,960 --> 00:02:28,320
 namaskara and

35
00:02:28,320 --> 00:02:36,120
 and you would in most countries I think put your hands

36
00:02:36,120 --> 00:02:38,960
 together and

37
00:02:38,960 --> 00:02:45,410
 don't look straight into the face in the beginning later in

38
00:02:45,410 --> 00:02:47,040
 the conversation yes but

39
00:02:47,520 --> 00:02:51,560
 in the beginning have the head a little bit lowered or the

40
00:02:51,560 --> 00:02:53,760
 even a little bit bent or so

41
00:02:53,760 --> 00:02:59,710
 if it's an important monk the the more important the monk

42
00:02:59,710 --> 00:03:01,680
 the deeper the bow

43
00:03:01,680 --> 00:03:07,450
 and things like that. Yeah I mean here in Sri Lanka they

44
00:03:07,450 --> 00:03:11,120
 they touch their hands to your feet

45
00:03:11,120 --> 00:03:14,430
 if people do that when they respect you and we do that when

46
00:03:14,430 --> 00:03:16,080
 we respect the senior monk

47
00:03:16,080 --> 00:03:19,240
 we touch we touch our head actually to the feet of the

48
00:03:19,240 --> 00:03:21,760
 elder and people do that for their parents

49
00:03:21,760 --> 00:03:27,360
 or I learned here in Sri Lanka from the bhikkhunis who have

50
00:03:27,360 --> 00:03:30,560
 stayed in Sri Lanka before me that we do

51
00:03:30,560 --> 00:03:38,490
 like people do to the queen of England I don't know how is

52
00:03:38,490 --> 00:03:41,280
 that how that is called but

53
00:03:42,720 --> 00:03:49,040
 yeah probably like and with the palms together so

54
00:03:49,040 --> 00:03:56,170
 that would the the appropriate greeting for a bhikkhuni to

55
00:03:56,170 --> 00:04:00,560
 greet a higher monk. We were talking

56
00:04:00,560 --> 00:04:03,910
 about the word bhante because I'm still pretty adamant that

57
00:04:03,910 --> 00:04:06,160
 it's masculine but that's really

58
00:04:06,160 --> 00:04:11,670
 a grammatical question they'll often say ay-ay-ay or you

59
00:04:11,670 --> 00:04:14,080
 could say bad day I think but no one

60
00:04:14,080 --> 00:04:17,860
 would have no one uses it so but I think you could looking

61
00:04:17,860 --> 00:04:20,480
 at because it is used there's two two

62
00:04:20,480 --> 00:04:24,410
 angels talking together and one says calls the other bhante

63
00:04:24,410 --> 00:04:26,720
 the female calls the male bhante

64
00:04:26,720 --> 00:04:29,690
 and the male turns around and calls the female bad day

65
00:04:29,690 --> 00:04:32,400
 which they're actually pretty much the

66
00:04:32,400 --> 00:04:37,430
 same word just one's masculine one's feminine it's not not

67
00:04:37,430 --> 00:04:40,320
 so clear they are not asking for

68
00:04:40,320 --> 00:04:50,640
 okay

