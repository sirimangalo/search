1
00:00:00,000 --> 00:00:10,000
 The first definition is what is meant here, but there are

2
00:00:10,000 --> 00:00:11,120
 other kinds of death.

3
00:00:11,120 --> 00:00:15,440
 So death is termination, cutting off. In other words, the

4
00:00:15,440 --> 00:00:15,900
 Alehan's

5
00:00:15,900 --> 00:00:21,780
 termination of suffering of the wrong is not indented here,

6
00:00:21,780 --> 00:00:23,700
 because when an

7
00:00:23,700 --> 00:00:27,160
 Alehan dies, he dies and then there is no more rebirth for

8
00:00:27,160 --> 00:00:28,480
 him. So that kind of

9
00:00:28,480 --> 00:00:34,520
 death is not meant here, nor is momentary death. That means

10
00:00:34,520 --> 00:00:35,760
 at every moment there

11
00:00:35,760 --> 00:00:39,500
 is death, because there is one moment of thought and then

12
00:00:39,500 --> 00:00:40,600
 it dies and then another

13
00:00:40,600 --> 00:00:44,480
 moment of thought and so on. So we are dying and being

14
00:00:44,480 --> 00:00:46,960
 reborn every moment. That

15
00:00:46,960 --> 00:00:50,070
 kind of death is called momentary death, the momentary

16
00:00:50,070 --> 00:00:51,600
 dissolution of formations,

17
00:00:51,600 --> 00:00:55,450
 not the death of conventional metaphorical usage in such

18
00:00:55,450 --> 00:00:56,560
 expression as

19
00:00:56,560 --> 00:01:06,710
 the tree is dead, the metal is dead and so on. In the Ris

20
00:01:06,710 --> 00:01:08,360
odi Mother, it's said

21
00:01:08,360 --> 00:01:18,490
 the iron is dead actually, not metal, iron is dead. Now

22
00:01:18,490 --> 00:01:21,560
 there is some kind of

23
00:01:21,560 --> 00:01:30,560
 alchemy in the West as well as in the East. These people,

24
00:01:30,560 --> 00:01:31,520
 what do you call them,

25
00:01:31,520 --> 00:01:35,680
 are alchemists. These people try to do something to the

26
00:01:35,680 --> 00:01:37,000
 metal, to make

27
00:01:37,000 --> 00:01:43,550
 them beneficial to people. And so there is a saying in Bham

28
00:01:43,550 --> 00:01:50,080
mis, "If you can kill, if

29
00:01:50,080 --> 00:01:55,280
 you can kill the iron, you can feed the whole country." I

30
00:01:55,280 --> 00:01:56,640
 don't know what

31
00:01:56,640 --> 00:02:03,120
 that means. That means if you can make iron produce

32
00:02:03,120 --> 00:02:06,040
 something maybe, or there is a

33
00:02:06,040 --> 00:02:11,280
 saying that if you can kill the iron, then you can turn

34
00:02:11,280 --> 00:02:12,640
 lead into gold

35
00:02:12,640 --> 00:02:23,720
 and you can cure diseases. They try to burn these metals

36
00:02:23,720 --> 00:02:24,760
 and make them into ashes

37
00:02:24,760 --> 00:02:30,440
 and then they mix them with honey and so on and treat

38
00:02:30,440 --> 00:02:33,480
 people. And many

39
00:02:33,480 --> 00:02:41,210
 people believe that such ashes can treat many kinds of

40
00:02:41,210 --> 00:02:45,120
 diseases. So when we say

41
00:02:45,120 --> 00:02:51,040
 the iron is dead, the copper is dead and we are using

42
00:02:51,040 --> 00:02:54,000
 conventional usage. So that

43
00:02:54,000 --> 00:02:58,370
 kind of dead is not meant here. So what is meant here as

44
00:02:58,370 --> 00:02:59,440
 dead is just

45
00:02:59,440 --> 00:03:04,520
 the end of one life. As in that end here, it is of two

46
00:03:04,520 --> 00:03:05,920
 kinds and that is to say

47
00:03:05,920 --> 00:03:09,930
 timely death and untimely death. Now timely death comes

48
00:03:09,930 --> 00:03:11,280
 about with the exhaustion

49
00:03:11,280 --> 00:03:15,150
 of merit or with the exhaustion of a lifespan of with both.

50
00:03:15,150 --> 00:03:16,240
 Untimely death

51
00:03:16,240 --> 00:03:20,760
 comes about through karma that interrupts or that cut off

52
00:03:20,760 --> 00:03:24,040
 the other life-producing

53
00:03:24,040 --> 00:03:31,320
 karma. Now, death through exhaustion of merit, that means

54
00:03:31,320 --> 00:03:32,600
 that through the

55
00:03:32,600 --> 00:03:38,770
 exhaustion of the force of karma. That means suppose the

56
00:03:38,770 --> 00:03:40,560
 lifespan is now 100

57
00:03:40,560 --> 00:03:49,680
 years, but a person whose karma cannot give him 100 years

58
00:03:49,680 --> 00:03:52,040
 may die before he

59
00:03:52,040 --> 00:03:55,720
 reaches 100 years. So he may die at the age of 50, 40 or

60
00:03:55,720 --> 00:03:58,320
 even younger. So when he dies in that

61
00:03:58,320 --> 00:04:05,880
 way, he is said to die through exhaustion of merit or

62
00:04:05,880 --> 00:04:11,780
 exhaustion of force of karma. That is why now people, many

63
00:04:11,780 --> 00:04:12,920
 people die before they

64
00:04:12,920 --> 00:04:19,640
 reach to the end of the lifespan. Sometimes people have

65
00:04:19,640 --> 00:04:22,200
 very strong karma and

66
00:04:22,200 --> 00:04:28,180
 they, that the karma could make them live say a thousand

67
00:04:28,180 --> 00:04:29,920
 years, many hundreds of

68
00:04:29,920 --> 00:04:38,000
 years. But if people, if they are born at a time when human

69
00:04:38,000 --> 00:04:38,600
 beings live

70
00:04:38,600 --> 00:04:43,080
 only 100 years, then they will die at the end of that span.

71
00:04:43,080 --> 00:04:44,840
 So such death is called

72
00:04:44,840 --> 00:04:51,070
 death through exhaustion of a lifespan. They may have karma

73
00:04:51,070 --> 00:04:53,000
 which can make them

74
00:04:53,000 --> 00:04:56,880
 live more than 100 years, but since they are born in the

75
00:04:56,880 --> 00:04:58,720
 time when people live

76
00:04:58,720 --> 00:05:02,160
 only 100 years, they have to die at the age of 100 years.

77
00:05:02,160 --> 00:05:03,360
 So that kind of death is

78
00:05:03,360 --> 00:05:12,790
 called exhaustion of a lifespan. The death through

79
00:05:12,790 --> 00:05:16,320
 exhaustion of both is, say, a

80
00:05:16,320 --> 00:05:23,390
 person has a karma which can cause him to live 100 years

81
00:05:23,390 --> 00:05:27,840
 and he is

82
00:05:27,840 --> 00:05:32,960
 reborn at a time when people live 100 years and he died, he

83
00:05:32,960 --> 00:05:34,240
 dies at the age of

84
00:05:34,240 --> 00:05:39,630
 100 years. So that is death through both. And the last one

85
00:05:39,630 --> 00:05:42,160
 is untimely death. That

86
00:05:42,160 --> 00:05:47,040
 is mostly tragic death. A time for the death of those whose

87
00:05:47,040 --> 00:05:47,920
 continuity is

88
00:05:47,920 --> 00:05:51,520
 interrupted by karma capable of causing them to fall from

89
00:05:51,520 --> 00:05:52,640
 their place at that

90
00:05:52,640 --> 00:05:57,120
 very moment as in the case of Dusi Mara, Kalaburaja, etc.

91
00:05:57,120 --> 00:05:58,200
 All for the death of

92
00:05:58,200 --> 00:06:01,740
 those whose life continuity is interrupted by assault with

93
00:06:01,740 --> 00:06:03,720
 weapons, etc.

94
00:06:03,720 --> 00:06:08,290
 due to previous karma. Now sometimes people do a very

95
00:06:08,290 --> 00:06:10,320
 heinous offense or

96
00:06:10,320 --> 00:06:13,920
 crime, I mean killing an arahant or something like that,

97
00:06:13,920 --> 00:06:16,960
 and such people

98
00:06:16,960 --> 00:06:26,400
 sometimes die immediately. Here is given Dusi Mara. So he

99
00:06:26,400 --> 00:06:30,160
 tried to kill Saripoda.

100
00:06:30,160 --> 00:06:35,220
 You know, one of the Saripoda shaped his head and so his

101
00:06:35,220 --> 00:06:37,120
 head was very

102
00:06:37,120 --> 00:06:43,160
 smooth and shiny. So Dusi Mara wanted to hit him and so he

103
00:06:43,160 --> 00:06:44,840
 hit him on the head.

104
00:06:44,840 --> 00:06:55,040
 And at that time Saripoda was in Jhana, in Samhapadi. So

105
00:06:55,040 --> 00:06:56,560
 nothing happened to

106
00:06:56,560 --> 00:07:01,120
 Saripoda. But because of that bad karma, he died

107
00:07:01,120 --> 00:07:03,680
 immediately at Dusi Mara,

108
00:07:03,680 --> 00:07:10,400
 something like that. And Kalaburaja means a king who killed

109
00:07:10,400 --> 00:07:12,040
 a sage,

110
00:07:12,040 --> 00:07:17,460
 who practiced patience. So there's a sage called Khandiwada

111
00:07:17,460 --> 00:07:18,920
. He was very

112
00:07:18,920 --> 00:07:23,160
 famous and once he came to the city and sat in the garden

113
00:07:23,160 --> 00:07:24,080
 and then the

114
00:07:24,080 --> 00:07:28,620
 flesh and growth of the king. And so the king was asleep

115
00:07:28,620 --> 00:07:31,240
 and so his queens and

116
00:07:31,240 --> 00:07:37,720
 concubines went to the hermit to listen to the dharma. Now

117
00:07:37,720 --> 00:07:40,280
 when the

118
00:07:40,280 --> 00:07:45,840
 king woke up, he didn't see his wife and so he looked

119
00:07:45,840 --> 00:07:47,160
 around and then when he

120
00:07:47,160 --> 00:07:52,360
 reached the hermit he asked, "What are you?" "No, I'm a her

121
00:07:52,360 --> 00:07:53,920
mit." "What do you practice?"

122
00:07:53,920 --> 00:07:57,240
 "I practice patience." "What is patience?" "Patience means

123
00:07:57,240 --> 00:07:59,560
 forbearing whatever

124
00:07:59,560 --> 00:08:03,400
 people do to you." Then he said, "Let us see your patience

125
00:08:03,400 --> 00:08:05,400
." So he called his, what

126
00:08:05,400 --> 00:08:09,670
 to call, executioners and let them first cut off the hands

127
00:08:09,670 --> 00:08:11,040
 of the sage and ask

128
00:08:11,040 --> 00:08:17,440
 him, "Are you still patient?" He said, "Patience does not

129
00:08:17,440 --> 00:08:18,760
 lie in the hands."

130
00:08:18,760 --> 00:08:24,600
 Then he had the feet cut off and he asked him again and he

131
00:08:24,600 --> 00:08:26,840
 said, "I don't, I'm not angry

132
00:08:26,840 --> 00:08:32,170
 with you. I'm not upset. Patience is here." So he kicked

133
00:08:32,170 --> 00:08:34,760
 him and he went away.

134
00:08:34,760 --> 00:08:39,720
 So when the king went away, the general heard about that

135
00:08:39,720 --> 00:08:42,320
 and so he rushed to the

136
00:08:42,320 --> 00:08:49,240
 to the sage, to the hermit and asked him to be angry with

137
00:08:49,240 --> 00:08:51,640
 the king. So he said,

138
00:08:51,640 --> 00:08:54,980
 "Please be angry with the king because if you get angry

139
00:08:54,980 --> 00:08:56,680
 with the king, then he will

140
00:08:56,680 --> 00:09:01,840
 suffer less." But the sage said, "People like me do not get

141
00:09:01,840 --> 00:09:03,680
 angry. Let him live

142
00:09:03,680 --> 00:09:11,200
 long." Something like that. But because of that offense, he

143
00:09:11,200 --> 00:09:12,440
 was, what do you call it, he was

144
00:09:12,440 --> 00:09:17,840
 swallowed by the earth. He was consumed by the earth. So

145
00:09:17,840 --> 00:09:19,160
 such death is called

146
00:09:19,160 --> 00:09:25,240
 untimely death. They have past trauma and they have life

147
00:09:25,240 --> 00:09:27,680
 span but their crime is so

148
00:09:27,680 --> 00:09:35,040
 so bad that they had to die and sometimes to die in an

149
00:09:35,040 --> 00:09:38,680
 accident, these are called untimely death.

150
00:09:38,680 --> 00:09:42,950
 About his, about if he got angry, his karma would be less.

151
00:09:42,950 --> 00:09:46,680
 How his action was the same action.

152
00:09:46,680 --> 00:09:53,800
 Yeah. Now how would his karma be less because somebody got

153
00:09:53,800 --> 00:09:56,480
 angry? Just

154
00:09:56,480 --> 00:10:07,480
 to say that. Yes, but it is like with regard to wholesome

155
00:10:07,480 --> 00:10:09,680
 acts, it is like

156
00:10:09,680 --> 00:10:16,680
 showing seed on a field which is further and which is not

157
00:10:16,680 --> 00:10:19,160
 further. So when the

158
00:10:19,160 --> 00:10:28,850
 person and also the killing, the effect or result of

159
00:10:28,850 --> 00:10:32,240
 killing differs with the

160
00:10:32,240 --> 00:10:39,620
 person with the being who is killed. So if the person

161
00:10:39,620 --> 00:10:41,760
 killed is virtuous, then

162
00:10:41,760 --> 00:10:46,870
 there is more Aku Sala. And if the person being killed is

163
00:10:46,870 --> 00:10:48,280
 larger than there is more

164
00:10:48,280 --> 00:10:53,000
 Aku Sala. So there are variations in degree of offense with

165
00:10:53,000 --> 00:10:55,600
 regard to the virtue of

166
00:10:55,600 --> 00:11:01,420
 the person being killed and also whether there is more

167
00:11:01,420 --> 00:11:03,120
 effort needed to kill.

168
00:11:03,120 --> 00:11:09,080
 To kill an end than to kill an elephant. Not the same. I

169
00:11:09,080 --> 00:11:12,160
 mean, they do not have the same

170
00:11:12,160 --> 00:11:18,000
 degree of Aku Sala and wholesomeness because to kill an

171
00:11:18,000 --> 00:11:19,040
 elephant you have to make

172
00:11:19,040 --> 00:11:24,950
 much effort. So the more effort there is, the more Aku Sala

173
00:11:24,950 --> 00:11:29,070
. So what you are saying is that the general is

174
00:11:29,070 --> 00:11:31,760
 misunderstood of this thing?

175
00:11:31,760 --> 00:11:37,240
 No, the general did not misunderstand him. But the general

176
00:11:37,240 --> 00:11:39,240
 wanted to maybe

177
00:11:39,240 --> 00:11:46,240
 offense less damaging to the king. Because if the monk, I

178
00:11:46,240 --> 00:11:53,340
 mean if the monk gets angry with him, he does less virtuous

179
00:11:53,340 --> 00:11:53,760
.

180
00:11:53,760 --> 00:11:57,160
 It is a funny example because it is a little bit different

181
00:11:57,160 --> 00:12:02,400
 in Mahayana. But a Mahayana approach might be to be

182
00:12:02,400 --> 00:12:05,080
 convinced to help the king.

183
00:12:05,080 --> 00:12:11,410
 He might be angry at him but that in the sense is a play on

184
00:12:11,410 --> 00:12:18,960
 virtue. I mean because he wants the betterment of the king,

185
00:12:18,960 --> 00:12:21,000
 the king's punishment not to be so bad.

186
00:12:21,000 --> 00:12:24,480
 And that is the argument that is being given to him. It is

187
00:12:24,480 --> 00:12:29,480
 kind of a funny virtuous argument.

188
00:12:29,480 --> 00:12:33,330
 Because he would only be convinced by virtue that to help

189
00:12:33,330 --> 00:12:38,960
 the king that he would be angry. It would be out of anger,

190
00:12:38,960 --> 00:12:40,840
 out of actual anger.

191
00:12:40,840 --> 00:12:45,400
 It would be, I don't know, I don't want to get angry.

192
00:12:45,400 --> 00:12:53,350
 But when he gets angry, then he is, it will amount to

193
00:12:53,350 --> 00:12:58,640
 breaking some vows or something.

194
00:12:58,640 --> 00:13:02,650
 Say, "I was not angry whatever people do to me." So he

195
00:13:02,650 --> 00:13:09,750
 might have made that vow. And so he kept it until his death

196
00:13:09,750 --> 00:13:10,280
.

197
00:13:10,280 --> 00:13:14,950
 Another theoretical question. You said something about some

198
00:13:14,950 --> 00:13:19,610
 people who have the karma to live 1000 years and they have

199
00:13:19,610 --> 00:13:21,480
 been living in 100 years.

200
00:13:21,480 --> 00:13:29,030
 What happened to the man who lived here? Do you have some

201
00:13:29,030 --> 00:13:31,640
 kind of a credit?

202
00:13:31,640 --> 00:13:41,880
 I wish he could.

203
00:13:41,880 --> 00:13:46,130
 In order for karma to ripen, to give results, we need

204
00:13:46,130 --> 00:13:48,280
 different conditions.

205
00:13:48,280 --> 00:13:52,200
 Sometimes the time of the ripening of the karma, the place

206
00:13:52,200 --> 00:13:55,890
 of the ripening of the karma, the condition of the ripening

207
00:13:55,890 --> 00:13:57,240
 of the karma and so on.

208
00:13:57,240 --> 00:14:03,610
 So when karma gives results, it depends on these conditions

209
00:14:03,610 --> 00:14:04,040
.

210
00:14:04,040 --> 00:14:16,320
 So when, you know, Mahathisti ever in one of his talks said

211
00:14:16,320 --> 00:14:21,200
, "I am afraid that many Burmese people will be reborn in

212
00:14:21,200 --> 00:14:23,240
 the United States."

213
00:14:23,240 --> 00:14:28,330
 Why? Because they do a lot of meritorious deeds. And so

214
00:14:28,330 --> 00:14:33,740
 these, especially Tana, these meritorious deeds will give

215
00:14:33,740 --> 00:14:35,720
 them great results.

216
00:14:35,720 --> 00:14:45,190
 Then Bhama is a poor country and so when the karma is able

217
00:14:45,190 --> 00:14:50,620
 to make him a billionaire, then Bhama cannot produce

218
00:14:50,620 --> 00:14:51,560
 billionaires.

219
00:14:51,560 --> 00:14:56,700
 Something like that. So in order for the karma to give full

220
00:14:56,700 --> 00:15:01,560
 results, then it needs favorable conditions.

221
00:15:01,560 --> 00:15:07,210
 Favorable effort made by the person and then time and many,

222
00:15:07,210 --> 00:15:09,480
 many, many things.

223
00:15:09,480 --> 00:15:14,340
 So isn't it possible that someone who dies young, can you

224
00:15:14,340 --> 00:15:16,840
 take, can you ever take that as a sign of good karma?

225
00:15:16,840 --> 00:15:21,580
 I mean, if they have a more favorable rebirth, having died

226
00:15:21,580 --> 00:15:22,440
 young.

227
00:15:22,440 --> 00:15:29,610
 But to die young is the result of not so good karma. I mean

228
00:15:29,610 --> 00:15:31,240
, not to live long.

229
00:15:31,240 --> 00:15:34,990
 Let's say, okay, let's say if they kept on, you know, their

230
00:15:34,990 --> 00:15:38,970
 life was so miserable, someone who's born in, you know, in

231
00:15:38,970 --> 00:15:40,280
 the ghetto or something,

232
00:15:40,280 --> 00:15:42,680
 there's no chance they're ever going to come in contact

233
00:15:42,680 --> 00:15:44,200
 with the Dharma or something.

234
00:15:44,200 --> 00:15:48,770
 And they die young, so they would have a better chance in

235
00:15:48,770 --> 00:15:50,680
 another environment.

236
00:15:50,680 --> 00:15:54,200
 Or is it always a sign of bad karma?

237
00:15:54,200 --> 00:16:00,120
 Not bad karma, but people are born as human beings as a

238
00:16:00,120 --> 00:16:04,840
 result of wholesome karma, let us call it, wholesome karma.

239
00:16:04,840 --> 00:16:08,340
 But this wholesome karma has different, what do you call it

240
00:16:08,340 --> 00:16:13,560
, capabilities, different abilities, different power.

241
00:16:13,560 --> 00:16:18,680
 So some karma can give them only, say, 10 years.

242
00:16:18,680 --> 00:16:22,600
 Wholesome karma can give only 10 years.

243
00:16:22,600 --> 00:16:27,000
 Then the other karma may be able to give them 20 years, 30

244
00:16:27,000 --> 00:16:28,280
 years and so on.

245
00:16:28,280 --> 00:16:35,830
 So whenever a person is reborn as a human being, he is said

246
00:16:35,830 --> 00:16:39,880
 to be reborn as a result of good karma.

247
00:16:39,880 --> 00:16:46,400
 But that good karma may be able to give him only 10 years,

248
00:16:46,400 --> 00:16:49,160
 20 years and so on.

249
00:16:49,160 --> 00:16:57,720
 So that person dies not as a result of bad karma, but as a

250
00:16:57,720 --> 00:17:04,190
 result of good karma which is not good enough to make him

251
00:17:04,190 --> 00:17:07,160
 live longer.

252
00:17:07,160 --> 00:17:13,600
 So in other words, children that die young then don't have

253
00:17:13,600 --> 00:17:18,040
 as much good karma as someone that's lived to 85 or 90.

254
00:17:18,040 --> 00:17:20,040
 That's right.

255
00:17:27,080 --> 00:17:33,110
 Then the collection on death, I think they're not so

256
00:17:33,110 --> 00:17:48,840
 difficult. How to reflect on death in different ways?

257
00:17:48,840 --> 00:17:54,910
 Paragraph 7. Some exercise it merely in this way, that

258
00:17:54,910 --> 00:17:59,930
 means death will take place, death will come, say, "I will

259
00:17:59,930 --> 00:18:02,760
 die one day" or something like that.

260
00:18:02,760 --> 00:18:05,670
 Their hindrances get suppressed, their mindfulness becomes

261
00:18:05,670 --> 00:18:08,790
 established with death as its object and the meditation

262
00:18:08,790 --> 00:18:11,080
 subject reaches excess.

263
00:18:11,080 --> 00:18:14,280
 Those are gifted people.

264
00:18:14,280 --> 00:18:17,960
 But if one finds that it does not get so far, he should do

265
00:18:17,960 --> 00:18:20,680
 his recollection of death in eight ways.

266
00:18:20,680 --> 00:18:25,050
 And that is, as having the appearance of a murderer as the

267
00:18:25,050 --> 00:18:26,600
 ruin of success.

268
00:18:26,600 --> 00:18:33,880
 Here, not the ruin of success, but success and failure.

269
00:18:33,880 --> 00:18:37,880
 And 3. By comparison, 4. As to sharing the body with many.

270
00:18:37,880 --> 00:18:42,630
 That means we have to share our body with other worms or

271
00:18:42,630 --> 00:18:46,360
 insects or germs and all these things.

272
00:18:46,360 --> 00:18:47,800
 Something like that.

273
00:18:47,800 --> 00:18:52,760
 And then, as to the frailty of life, as signless, as to the

274
00:18:52,760 --> 00:18:56,450
 limitedness of the extent and as to the shortness of the

275
00:18:56,450 --> 00:18:57,240
 moment.

276
00:18:57,240 --> 00:19:07,080
 So we can reflect on death in different ways.

277
00:19:07,080 --> 00:19:17,350
 And at the end, towards the end of the recollection, there

278
00:19:17,350 --> 00:19:29,000
 is a long footnote discussing panetti.

279
00:19:29,000 --> 00:19:35,400
 I think we'll discuss it next week.

280
00:19:35,400 --> 00:19:42,040
 But here today, let me see.

281
00:19:42,040 --> 00:19:48,540
 There are many examples given in this and we cannot go to

282
00:19:48,540 --> 00:19:50,440
 every reference.

283
00:19:50,440 --> 00:19:56,920
 Because on page 250, there are many references given.

284
00:19:56,920 --> 00:19:58,690
 Footnote 6, Mahasamaja, Mandatu, Mahasudasana, Dhanandimi,

285
00:19:58,690 --> 00:20:08,040
 Nimi, Jodhita, Jatila, Oga and so on.

286
00:20:08,040 --> 00:20:16,100
 And then, oh yes, some of these persons mentioned in these

287
00:20:16,100 --> 00:20:24,630
 pages are found mostly in Jataka tales and also in other

288
00:20:24,630 --> 00:20:26,040
 sodas.

289
00:20:26,040 --> 00:20:31,960
 And then paragraph 19, Vasudeva, Baladu and so on.

290
00:20:31,960 --> 00:20:37,480
 Mostly they are taken from Hindu books actually.

291
00:20:37,480 --> 00:20:43,000
 In the Mahabharata, these names appear.

292
00:20:43,000 --> 00:20:50,200
 And so there is some kind of, what to call,

293
00:20:50,200 --> 00:20:56,850
 some kind of relationship between Hindu stories and

294
00:20:56,850 --> 00:20:59,640
 Buddhist stories.

295
00:20:59,640 --> 00:21:07,680
 Some Hindu stories are told as Buddhist stories in the J

296
00:21:07,680 --> 00:21:09,160
ataka.

297
00:21:09,160 --> 00:21:13,580
 So these persons, Vasudeva, Baladu, Bhimasena, Yudhish, J

298
00:21:13,580 --> 00:21:15,480
ila and Jhanur and so on.

299
00:21:15,480 --> 00:21:19,700
 They are mentioned in one of the Jharigas also, but I think

300
00:21:19,700 --> 00:21:24,200
 they originally came from the Hindu sources.

301
00:21:24,200 --> 00:21:29,720
 In the Mahabharata, there are five brothers and two leaders

302
00:21:29,720 --> 00:21:33,080
 who are among themselves and so on.

303
00:21:33,080 --> 00:21:40,510
 But the main point here is such persons who are of great,

304
00:21:40,510 --> 00:21:43,160
 great marriage, who are of great strength,

305
00:21:43,160 --> 00:21:46,680
 who possess supernormal powers.

306
00:21:46,680 --> 00:21:53,050
 Even the Buddha had to die and so there is no one to say we

307
00:21:53,050 --> 00:21:54,600
 will not die.

308
00:21:54,600 --> 00:22:00,050
 So comparing ourselves with these persons, we can reflect

309
00:22:00,050 --> 00:22:00,760
 on death.

310
00:22:00,760 --> 00:22:09,000
 So that was come to us one day.

311
00:22:09,000 --> 00:22:14,130
 And this recollection also has to be done with wisdom, done

312
00:22:14,130 --> 00:22:15,400
 with understanding.

313
00:22:15,400 --> 00:22:27,880
 Otherwise, let me see, if he exercises his paragraph five,

314
00:22:27,880 --> 00:22:32,030
 if he exercises his attention unwisely in recollecting the

315
00:22:32,030 --> 00:22:34,040
 possible death of an agreeable person,

316
00:22:34,040 --> 00:22:38,200
 sorrow arises as any mother on recollecting the death of a

317
00:22:38,200 --> 00:22:39,960
 beloved child she bore.

318
00:22:39,960 --> 00:22:44,570
 And blackness arises in recollecting the death of a

319
00:22:44,570 --> 00:22:46,840
 disabled person as an enemy

320
00:22:46,840 --> 00:22:49,000
 than recollecting the death of their enemies.

321
00:22:49,000 --> 00:22:53,280
 And no sense of urgency arises on recollecting the death of

322
00:22:53,280 --> 00:22:54,600
 a neutral people

323
00:22:54,600 --> 00:22:59,240
 as happens in a corpse burner unseen in dead bodies.

324
00:22:59,240 --> 00:23:02,620
 And anxiety arises only collecting one's own death as

325
00:23:02,620 --> 00:23:04,280
 happens in a timid person

326
00:23:04,280 --> 00:23:08,200
 unseen in mother with a twice-deader.

327
00:23:08,200 --> 00:23:12,320
 In all that there is neither mindfulness or sense of

328
00:23:12,320 --> 00:23:14,120
 urgency nor knowledge.

329
00:23:14,120 --> 00:23:17,250
 So he should look here and there at beings that have been

330
00:23:17,250 --> 00:23:18,120
 killed or have died

331
00:23:18,120 --> 00:23:22,700
 and afford to be death of beings already dead but formerly

332
00:23:22,700 --> 00:23:24,600
 seen enjoying good things,

333
00:23:24,600 --> 00:23:27,920
 doing so with mindfulness, with a sense of urgency and with

334
00:23:27,920 --> 00:23:28,600
 knowledge.

335
00:23:28,600 --> 00:23:32,280
 After which he can exercise his attention and the way

336
00:23:32,280 --> 00:23:34,680
 beginning death will take place.

337
00:23:34,680 --> 00:23:40,610
 So when you practice this kind of meditation you have to be

338
00:23:40,610 --> 00:23:42,040
 very careful.

339
00:23:42,040 --> 00:23:51,820
 And it is amazing that thinking of death makes you less

340
00:23:51,820 --> 00:23:55,960
 afraid of death.

341
00:23:55,960 --> 00:24:07,660
 And also the recollection on death can reduce to a very

342
00:24:07,660 --> 00:24:14,120
 great degree your pride or attachment.

343
00:24:14,120 --> 00:24:18,160
 You may have experienced something like say you are very

344
00:24:18,160 --> 00:24:19,320
 sick or very ill

345
00:24:19,320 --> 00:24:24,200
 and you thought you you you are going to die.

346
00:24:24,200 --> 00:24:27,740
 In that case you don't have any attachment or you don't

347
00:24:27,740 --> 00:24:30,360
 have any say anger or whatever.

348
00:24:30,360 --> 00:24:35,240
 You're gonna know you have any pride in yourself.

349
00:24:35,240 --> 00:24:40,280
 You are very very what about it?

350
00:24:40,280 --> 00:24:44,680
 Not proud? Very humble right?

351
00:24:44,680 --> 00:24:48,500
 And you you're like an other hand at that time because you

352
00:24:48,500 --> 00:24:49,320
 don't want anything.

353
00:24:49,320 --> 00:24:54,200
 You are not attached to anything at that time.

354
00:24:54,200 --> 00:25:01,030
 So recollection on death is a very good weapon in fighting

355
00:25:01,030 --> 00:25:03,800
 against attachment,

356
00:25:03,800 --> 00:25:09,640
 hatred, pride and others.

357
00:25:09,640 --> 00:25:17,690
 And when death really comes say you'll be able to to face

358
00:25:17,690 --> 00:25:18,360
 it

359
00:25:18,360 --> 00:25:22,640
 with more calmness than those who do not practice this kind

360
00:25:22,640 --> 00:25:23,960
 of meditation.

361
00:25:23,960 --> 00:25:30,520
 That is why Buddha said monks must practice every day.

362
00:25:30,520 --> 00:25:36,120
 The recollection of the Buddha,

363
00:25:36,120 --> 00:25:40,940
 loving kindness meditation and foulness of the body

364
00:25:40,940 --> 00:25:46,120
 meditation and his meditation.

365
00:25:46,120 --> 00:25:49,000
 What does it mean?

366
00:25:49,000 --> 00:25:55,660
 Recollection of the Buddha, loving kindness, foulness of

367
00:25:55,660 --> 00:25:57,320
 the body meditation

368
00:25:57,320 --> 00:26:00,840
 and recollection of death.

369
00:26:00,840 --> 00:26:08,680
 One thing I want to say is paragraph 35

370
00:26:08,680 --> 00:26:12,670
 has to be limitedness of the extent. The extent of human

371
00:26:12,670 --> 00:26:14,280
 life is short now.

372
00:26:14,280 --> 00:26:20,360
 One who lives long lives a hundred years, more or less.

373
00:26:20,360 --> 00:26:24,440
 But what Buddha meant here is one who lives long lives

374
00:26:24,440 --> 00:26:29,080
 lives a hundred years or a little more.

375
00:26:29,080 --> 00:26:34,520
 Not more or less, or a little more. That means he may live

376
00:26:34,520 --> 00:26:36,120
 to be 120 years,

377
00:26:36,120 --> 00:26:41,480
 150 years, 160 years, so on. Not more or less.

378
00:26:41,480 --> 00:26:45,640
 So one who lives long lives a hundred years

379
00:26:45,640 --> 00:26:48,680
 or a little more.

380
00:26:48,680 --> 00:26:54,280
 There are two senators like that.

381
00:26:54,280 --> 00:26:58,520
 Okay I think that's all.

382
00:26:58,520 --> 00:27:07,400
 So we will discuss panyati next week and then go to

383
00:27:07,400 --> 00:27:10,600
 mindfulness of the book, go into the chapter,

384
00:27:10,600 --> 00:27:15,000
 I mean the section on mindfulness of the body.

385
00:27:15,000 --> 00:27:27,000
 Up to three, now 270, not many pages.

386
00:27:27,000 --> 00:27:34,680
 [

387
00:27:34,680 --> 00:27:44,680
 Pause ]

388
00:27:44,680 --> 00:27:58,360
 [ Pause ]

389
00:27:58,360 --> 00:28:06,360
 [ Pause ]

