1
00:00:00,000 --> 00:00:05,520
 Miriam asks, she has three younger brothers who really like

2
00:00:05,520 --> 00:00:07,000
 to go out and party.

3
00:00:07,000 --> 00:00:10,510
 Since I don't go out because I don't see the point, they

4
00:00:10,510 --> 00:00:12,000
 tell me I have no life.

5
00:00:12,000 --> 00:00:14,000
 Even my parents tell me the same.

6
00:00:14,000 --> 00:00:17,330
 And I tell them I see no point in doing what they do,

7
00:00:17,330 --> 00:00:19,000
 because there's no gain from that,

8
00:00:19,000 --> 00:00:21,000
 and that I'm happy being like this.

9
00:00:21,000 --> 00:00:25,000
 I found this is distracting me in meditation, and I too

10
00:00:25,000 --> 00:00:27,000
 attach to my family.

11
00:00:27,000 --> 00:00:41,000
 I think going through the Mangalam Sutta,

12
00:00:41,000 --> 00:00:58,000
 cherishing family, supporting his mother and father, and

13
00:00:58,000 --> 00:00:58,000
 taking care for the welfare of your family

14
00:00:58,000 --> 00:01:02,000
 is one of the highest blessings the Buddha says.

15
00:01:02,000 --> 00:01:11,460
 So that you care, that maybe they're doing something that's

16
00:01:11,460 --> 00:01:14,000
 unhealthy for their body,

17
00:01:14,000 --> 00:01:23,000
 and perhaps you're seeing how distracting it is.

18
00:01:23,000 --> 00:01:29,740
 But what they're actually engaging in is a huge distraction

19
00:01:29,740 --> 00:01:32,000
 in and of itself.

20
00:01:32,000 --> 00:01:38,450
 Alcohol distracts the mind, it creates lots of carelessness

21
00:01:38,450 --> 00:01:40,000
 and addiction.

22
00:01:40,000 --> 00:01:49,570
 But I've had similar experiences, and I would say that what

23
00:01:49,570 --> 00:01:53,000
's been really helpful,

24
00:01:53,000 --> 00:01:57,620
 rather than kind of getting into too much talk about it at

25
00:01:57,620 --> 00:01:58,000
 all,

26
00:01:58,000 --> 00:02:04,000
 would be to simply offer alternatives.

27
00:02:04,000 --> 00:02:09,600
 People go out and party and watch TV and do mindless,

28
00:02:09,600 --> 00:02:13,000
 careless, harmful things,

29
00:02:13,000 --> 00:02:18,220
 often because their culture sort of offers that and doesn't

30
00:02:18,220 --> 00:02:21,000
 give too many alternatives.

31
00:02:21,000 --> 00:02:25,000
 And it seems like you have a meditation practice going,

32
00:02:25,000 --> 00:02:35,280
 and you could invite them to participate, whether it's

33
00:02:35,280 --> 00:02:37,000
 going to some sort of Dhamma talk in the area

34
00:02:37,000 --> 00:02:40,000
 that you think might be of interest to them,

35
00:02:40,000 --> 00:02:44,850
 or seeing if they'll actually sit down with you and close

36
00:02:44,850 --> 00:02:47,000
 their eyes and practice,

37
00:02:47,000 --> 00:02:57,830
 just offering time together with them that could make their

38
00:02:57,830 --> 00:03:00,000
 mind a bit more calm and peaceful

39
00:03:00,000 --> 00:03:04,000
 so they don't feel like they need to go out so much.

40
00:03:04,000 --> 00:03:06,000
 Hold a meditation party.

41
00:03:06,000 --> 00:03:10,120
 You don't have anything wrong with partying, it's just we

42
00:03:10,120 --> 00:03:12,000
 want a specific type of party.

43
00:03:12,000 --> 00:03:14,000
 Meditation party.

44
00:03:14,000 --> 00:03:17,590
 Yeah, actually we do, well in Thailand more so, they do all

45
00:03:17,590 --> 00:03:18,000
 night.

46
00:03:18,000 --> 00:03:21,210
 Actually, like two nights ago, I don't know who stayed up

47
00:03:21,210 --> 00:03:22,000
 all night.

48
00:03:22,000 --> 00:03:26,000
 The bugs kept me up most of the night, I'd say.

49
00:03:26,000 --> 00:03:31,000
 But we were at Anuradhapura, is that what you say?

50
00:03:31,000 --> 00:03:37,020
 And so sleeping outside, sleeping, I don't know how much

51
00:03:37,020 --> 00:03:38,000
 sleeping was going on,

52
00:03:38,000 --> 00:03:40,000
 but meditating outside.

53
00:03:40,000 --> 00:03:49,140
 And it's very Buddhist to do all night meditation every

54
00:03:49,140 --> 00:03:51,000
 half moon,

55
00:03:51,000 --> 00:04:00,290
 and it's quite fun, in a sense, even though it can be very

56
00:04:00,290 --> 00:04:01,000
 challenging

57
00:04:01,000 --> 00:04:05,000
 to actually stay up with your eyes closed.

58
00:04:05,000 --> 00:04:11,470
 Or you can change to do walking meditation, but there's so

59
00:04:11,470 --> 00:04:17,000
 much sort of peaceful energy

60
00:04:17,000 --> 00:04:20,430
 that can arise, very calm and peaceful energy that can

61
00:04:20,430 --> 00:04:24,000
 arise from feeling lethargy in the body

62
00:04:24,000 --> 00:04:30,040
 and not just caving in or opening the fridge and just

63
00:04:30,040 --> 00:04:34,000
 feeling it arise and pass away.

64
00:04:34,000 --> 00:04:43,200
 It can, yeah, it's a whole other type of partying of the

65
00:04:43,200 --> 00:04:44,000
 mind.

66
00:04:44,000 --> 00:04:47,000
 Anyone else?

67
00:04:47,000 --> 00:04:54,930
 I just wanted to say one simple thing, is that they tell

68
00:04:54,930 --> 00:04:56,000
 you you don't have a life,

69
00:04:56,000 --> 00:05:00,800
 you have no life, tell them you have peace, tell them you

70
00:05:00,800 --> 00:05:02,000
 have happiness.

71
00:05:02,000 --> 00:05:05,000
 You have to find something good for yourself.

72
00:05:05,000 --> 00:05:09,000
 When I was at home, my parents would yell at me.

73
00:05:09,000 --> 00:05:13,000
 My stepmother would yell at me saying I was brainwashed,

74
00:05:13,000 --> 00:05:18,650
 how I was a zombie and how I was lost and how I was no fun

75
00:05:18,650 --> 00:05:19,000
 anymore,

76
00:05:19,000 --> 00:05:24,230
 and not fun to be around because I used to play guitar and

77
00:05:24,230 --> 00:05:25,000
 I would joke

78
00:05:25,000 --> 00:05:27,000
 and I would play games and so on.

79
00:05:27,000 --> 00:05:29,000
 I didn't want to do any of that anymore.

80
00:05:29,000 --> 00:05:33,000
 You're like a zombie.

81
00:05:33,000 --> 00:05:35,400
 But I knew inside that what I was doing was correct and it

82
00:05:35,400 --> 00:05:38,000
 was actually quite torture for me,

83
00:05:38,000 --> 00:05:40,680
 torture to have to listen to this and feel kind of the

84
00:05:40,680 --> 00:05:42,000
 anguish and the conflict and so on

85
00:05:42,000 --> 00:05:44,000
 because I was a new meditator.

86
00:05:44,000 --> 00:05:48,000
 But it's not, that's a fact of life.

87
00:05:48,000 --> 00:05:51,170
 You're surrounded by people who don't want to meditate and

88
00:05:51,170 --> 00:05:52,000
 that's what I was.

89
00:05:52,000 --> 00:05:54,000
 So it was a lot of suffering.

90
00:05:54,000 --> 00:05:55,000
 It didn't change anything.

91
00:05:55,000 --> 00:05:58,600
 It didn't make me say, "Oh, maybe I should go party with

92
00:05:58,600 --> 00:05:59,000
 them."

93
00:05:59,000 --> 00:06:02,000
 There really is no answer.

94
00:06:02,000 --> 00:06:03,000
 It just is what it is.

95
00:06:03,000 --> 00:06:05,000
 That's the way it is.

96
00:06:05,000 --> 00:06:07,630
 Once you develop in the meditation, then you have no

97
00:06:07,630 --> 00:06:08,000
 problem.

98
00:06:08,000 --> 00:06:12,000
 You just smile at them and say, "If I'm a zombie, then I'm

99
00:06:12,000 --> 00:06:12,000
 a zombie,

100
00:06:12,000 --> 00:06:15,000
 but I have peace, happiness and freedom from suffering."

101
00:06:15,000 --> 00:06:16,000
 What do you have?

102
00:06:16,000 --> 00:06:18,000
 What do you get from going out?

103
00:06:18,000 --> 00:06:20,500
 Right now in your mind, you have a whole bunch of anger and

104
00:06:20,500 --> 00:06:24,000
 dissatisfaction with me and so on.

105
00:06:24,000 --> 00:06:27,460
 And worry about me and so on and all of these things are

106
00:06:27,460 --> 00:06:28,000
 causing suffering for you.

107
00:06:28,000 --> 00:06:30,000
 I don't have that.

108
00:06:30,000 --> 00:06:35,000
 So I consider this state to be much more peaceful and happy

109
00:06:35,000 --> 00:06:35,000
.

110
00:06:35,000 --> 00:06:37,000
 But of course you need to attain that to yourself.

111
00:06:37,000 --> 00:06:40,000
 In the beginning, there is no answer.

112
00:06:40,000 --> 00:06:47,000
 You're just going through this transition period

113
00:06:47,000 --> 00:06:50,370
 where your parents haven't come to realize the truth of

114
00:06:50,370 --> 00:06:51,000
 what you're saying

115
00:06:51,000 --> 00:06:56,000
 and you yourself haven't yet experienced the fruits

116
00:06:56,000 --> 00:07:01,000
 or are still only cultivating the fruits.

117
00:07:01,000 --> 00:07:07,000
 It's like you have a field of grain or something

118
00:07:07,000 --> 00:07:09,000
 and it hasn't bore fruit yet and people are saying,

119
00:07:09,000 --> 00:07:12,000
 "Oh, what are you wasting all your time?"

120
00:07:12,000 --> 00:07:13,900
 Because they can't see the fruits and you don't have

121
00:07:13,900 --> 00:07:15,000
 anything to show them

122
00:07:15,000 --> 00:07:17,000
 because you don't have the fruits yet.

123
00:07:17,000 --> 00:07:19,390
 But you know we've planted correctly and you've done

124
00:07:19,390 --> 00:07:20,000
 everything properly

125
00:07:20,000 --> 00:07:22,000
 and it's only a matter of time.

126
00:07:22,000 --> 00:07:24,000
 And then when the fruit comes, then you can show them and

127
00:07:24,000 --> 00:07:24,000
 say,

128
00:07:24,000 --> 00:07:27,000
 "See, see, that was very much worth it."

129
00:07:27,000 --> 00:07:30,620
 And you people who are like the story of the ants in the

130
00:07:30,620 --> 00:07:32,000
 Grasshopper, right?

131
00:07:32,000 --> 00:07:37,560
 The Grasshopper just lazes around all summer and plays his

132
00:07:37,560 --> 00:07:40,000
 fiddle and has fun.

133
00:07:40,000 --> 00:07:42,000
 And the ants are working and working and working and he

134
00:07:42,000 --> 00:07:42,000
 says,

135
00:07:42,000 --> 00:07:44,050
 "Look at these stupid ants. They're wasting their time

136
00:07:44,050 --> 00:07:46,000
 working, working, collecting food.

137
00:07:46,000 --> 00:07:49,000
 What are they doing? They should enjoy life."

138
00:07:49,000 --> 00:07:52,730
 And then the winter comes and the Grasshopper can't find

139
00:07:52,730 --> 00:07:53,000
 food

140
00:07:53,000 --> 00:07:55,670
 and he's freezing and cold and so he goes and knocks on the

141
00:07:55,670 --> 00:07:56,000
 ants' door

142
00:07:56,000 --> 00:07:59,740
 and they say, "Oh yes, we've got lots of food. We're doing

143
00:07:59,740 --> 00:08:00,000
 fine

144
00:08:00,000 --> 00:08:02,000
 because we prepared for the winter."

145
00:08:02,000 --> 00:08:04,000
 So that kind of thing.

146
00:08:04,000 --> 00:08:08,000
 I mean it's a matter of where your priorities are.

147
00:08:08,000 --> 00:08:13,370
 Do you want to just waste your potential here and now in

148
00:08:13,370 --> 00:08:17,000
 enjoying these base pleasures

149
00:08:17,000 --> 00:08:19,150
 or do you want to work and cultivate higher pleasure,

150
00:08:19,150 --> 00:08:21,000
 higher happiness and higher peace?

151
00:08:21,000 --> 00:08:24,000
 That's more lasting and more sustainable.

152
00:08:24,000 --> 00:08:27,230
 But you have to put it with the ridicule of people like the

153
00:08:27,230 --> 00:08:29,000
 Grasshopper.

154
00:08:29,000 --> 00:08:33,000
 [Birds chirping]

