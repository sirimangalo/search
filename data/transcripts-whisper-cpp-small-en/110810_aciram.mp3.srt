1
00:00:00,000 --> 00:00:08,800
 anicca vata sankhara upadvayadham mino upadjitvani rujanti

2
00:00:08,800 --> 00:00:12,000
 desangupasumosukho

3
00:00:12,000 --> 00:00:17,720
 impermanent indeed are all formations of a nature to arise

4
00:00:17,720 --> 00:00:19,600
 and pass away.

5
00:00:19,600 --> 00:00:22,520
 having arisen they all cease.

6
00:00:22,520 --> 00:00:25,200
 their stilling is happiness.

7
00:00:25,200 --> 00:00:31,660
 achirangvata yanka yo padthavingadhi sezati chudho apetav

8
00:00:31,660 --> 00:00:36,200
inyano niratangvakalangarang

9
00:00:36,200 --> 00:00:40,360
 before long indeed this body will lie on the earth,

10
00:00:40,360 --> 00:00:44,600
 discarded, left by the mind, as useless

11
00:00:44,600 --> 00:00:45,680
 as a wooden log.

