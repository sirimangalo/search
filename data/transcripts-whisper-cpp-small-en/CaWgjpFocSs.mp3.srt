1
00:00:00,000 --> 00:00:05,000
 Yesterday my dad had to go to the hospital for emergency.

2
00:00:05,000 --> 00:00:07,610
 When I was driving I was praying that nothing was going to

3
00:00:07,610 --> 00:00:08,000
 happen to him.

4
00:00:08,000 --> 00:00:10,000
 It turns out that there was nothing serious.

5
00:00:10,000 --> 00:00:15,000
 It got me thinking, what can we do to help our parents

6
00:00:15,000 --> 00:00:17,000
 prepare for death

7
00:00:17,000 --> 00:00:20,000
 and have a better rebirth or state?

8
00:00:20,000 --> 00:00:27,060
 Can we do anything to help them even though they are not

9
00:00:27,060 --> 00:00:29,000
 religious?

10
00:00:29,000 --> 00:00:39,000
 It comes back to, can you help anyone at all really?

11
00:00:39,000 --> 00:00:42,000
 The answer is only indirectly.

12
00:00:42,000 --> 00:00:46,320
 You can provide people with information that they are

13
00:00:46,320 --> 00:00:52,000
 already seeking.

14
00:00:52,000 --> 00:01:00,000
 You can prompt them when they already have good intentions.

15
00:01:00,000 --> 00:01:02,430
 It's like the old adage, "You can lead a horse to water,

16
00:01:02,430 --> 00:01:08,000
 but you can't make a drink."

17
00:01:08,000 --> 00:01:15,820
 There's no need for your father to be religious, not in the

18
00:01:15,820 --> 00:01:19,000
 sense of believing in anything.

19
00:01:19,000 --> 00:01:24,000
 The word "religion" doesn't just mean to believe in things.

20
00:01:24,000 --> 00:01:28,000
 The word "religious" means to take something seriously.

21
00:01:28,000 --> 00:01:31,000
 We use it in secular life as well.

22
00:01:31,000 --> 00:01:37,620
 He was dieting religiously or he kept to his diet

23
00:01:37,620 --> 00:01:40,000
 religiously.

24
00:01:40,000 --> 00:01:43,080
 People who do things religiously means they take it

25
00:01:43,080 --> 00:01:44,000
 seriously.

26
00:01:44,000 --> 00:01:50,130
 The point being that they need to have some realization or

27
00:01:50,130 --> 00:01:53,600
 some knowledge of the severity of the situation that they

28
00:01:53,600 --> 00:01:54,000
're in,

29
00:01:54,000 --> 00:02:01,000
 which is being unprepared for death.

30
00:02:01,000 --> 00:02:09,680
 If people don't realize the importance for preparing for

31
00:02:09,680 --> 00:02:10,000
 death,

32
00:02:10,000 --> 00:02:14,000
 it's quite a dangerous situation.

33
00:02:14,000 --> 00:02:21,000
 The point is to first understand the importance of it

34
00:02:21,000 --> 00:02:24,570
 and then of course take the steps necessary to prepare for

35
00:02:24,570 --> 00:02:25,000
 it.

36
00:02:25,000 --> 00:02:28,000
 If the question was, "How do I prepare for death?"

37
00:02:28,000 --> 00:02:30,000
 then there's a lot of tips that I can give.

38
00:02:30,000 --> 00:02:33,000
 Of course, the best one is to practice lots of meditation.

39
00:02:33,000 --> 00:02:40,000
 You can think about death and spend some time considering

40
00:02:40,000 --> 00:02:42,650
 the fact that you're going to have to die and preparing

41
00:02:42,650 --> 00:02:44,000
 yourself for it mentally.

42
00:02:44,000 --> 00:02:50,000
 There's a story of the bodhisattva in the time well before

43
00:02:50,000 --> 00:02:53,000
 the Buddha in one of the jataka tales.

44
00:02:53,000 --> 00:02:57,000
 It's the uraga jataka, I think.

45
00:02:57,000 --> 00:03:05,000
 The uraga jataka is a story of the…

46
00:03:05,000 --> 00:03:09,000
 I think it's the uraga jataka, I may be wrong.

47
00:03:09,000 --> 00:03:14,210
 Anyway, it's about the snake bites his son and the son dies

48
00:03:14,210 --> 00:03:16,000
 immediately.

49
00:03:16,000 --> 00:03:21,500
 But the bodhisattva has been teaching his family about

50
00:03:21,500 --> 00:03:24,000
 death and reminding them.

51
00:03:24,000 --> 00:03:30,000
 He always made sure that every day they were aware of it

52
00:03:30,000 --> 00:03:32,000
 and they understood that they weren't going to live.

53
00:03:32,000 --> 00:03:34,000
 He was practicing mindfulness of death.

54
00:03:34,000 --> 00:03:36,000
 It seems kind of morbid, I suppose.

55
00:03:36,000 --> 00:03:39,330
 It shouldn't be understood to have encompassed his life so

56
00:03:39,330 --> 00:03:43,000
 that they were always standing around in fear of death.

57
00:03:43,000 --> 00:03:45,000
 The point was to not be afraid of it.

58
00:03:45,000 --> 00:03:48,990
 To understand or to bring ourselves out of this

59
00:03:48,990 --> 00:03:52,000
 conventional reality of being a human being

60
00:03:52,000 --> 00:03:55,150
 and realizing this state of being a human being is not

61
00:03:55,150 --> 00:03:56,000
 going to last.

62
00:03:56,000 --> 00:03:59,540
 Whether there's something after or before it or not, it's

63
00:03:59,540 --> 00:04:02,000
 only a very small part of reality,

64
00:04:02,000 --> 00:04:05,930
 a small part of the universe, a small part of the infinity

65
00:04:05,930 --> 00:04:07,000
 of time.

66
00:04:07,000 --> 00:04:11,750
 So thinking about death, it's a part of bringing yourself

67
00:04:11,750 --> 00:04:13,000
 out of this.

68
00:04:13,000 --> 00:04:17,530
 So he did this and when his son died, he looked over and

69
00:04:17,530 --> 00:04:19,000
 saw his son was dead.

70
00:04:19,000 --> 00:04:22,660
 And he called some. Some guy was walking past the road and

71
00:04:22,660 --> 00:04:23,000
 so he called him and said,

72
00:04:23,000 --> 00:04:28,000
 "Can you go tell my wife to just bring one lunch today?"

73
00:04:28,000 --> 00:04:35,000
 And he said, "Tell her to bring our daughter in law as well

74
00:04:35,000 --> 00:04:35,000
."

75
00:04:35,000 --> 00:04:41,260
 And the servant, the maid or whatever, "Come dressed in

76
00:04:41,260 --> 00:04:43,000
 your best clothes."

77
00:04:43,000 --> 00:04:46,000
 This is what he said, "Please tell my wife this."

78
00:04:46,000 --> 00:04:50,000
 So the guy goes to their house and tells his wife.

79
00:04:50,000 --> 00:04:52,000
 And she knows, "Oh, my son is dead."

80
00:04:52,000 --> 00:04:54,000
 She knows right away what the deal is.

81
00:04:54,000 --> 00:04:56,000
 "My son must have died."

82
00:04:56,000 --> 00:04:58,520
 She puts on her best clothes, gets the whole family

83
00:04:58,520 --> 00:05:01,000
 together and takes them out with one lunch.

84
00:05:01,000 --> 00:05:03,480
 He sits down, eats his lunch and then he says, "Look, our

85
00:05:03,480 --> 00:05:06,000
 son died. We've got to have a funeral."

86
00:05:06,000 --> 00:05:09,000
 And so they prepare the funeral for him.

87
00:05:09,000 --> 00:05:14,240
 And suddenly up in heaven, Sakka, the king of the angels,

88
00:05:14,240 --> 00:05:16,000
 his throne gets hot.

89
00:05:16,000 --> 00:05:18,580
 And you'll hear this a lot in the Jatakas and the Dhammap

90
00:05:18,580 --> 00:05:19,000
ada.

91
00:05:19,000 --> 00:05:22,000
 No, I think most of the Jatakas.

92
00:05:22,000 --> 00:05:26,190
 When Sakka's throne gets hot, it means someone's

93
00:05:26,190 --> 00:05:29,000
 threatening to dethrone him.

94
00:05:29,000 --> 00:05:35,490
 It means someone's got some great power of mind that might

95
00:05:35,490 --> 00:05:36,000
 threaten him.

96
00:05:36,000 --> 00:05:40,000
 And it means that he might be dethroned.

97
00:05:40,000 --> 00:05:42,000
 So he says, "Who is it that's..."

98
00:05:42,000 --> 00:05:44,350
 He always looks down and says, "Who is it that's going to

99
00:05:44,350 --> 00:05:45,000
 dethrone me?

100
00:05:45,000 --> 00:05:47,000
 Who is it that's threatening me with this?"

101
00:05:47,000 --> 00:05:50,000
 But he's a good guy and that's how he got to become Sakka.

102
00:05:50,000 --> 00:05:54,000
 But he always wants to test these people.

103
00:05:54,000 --> 00:05:59,950
 And so he goes down and he puts himself in the form of a

104
00:05:59,950 --> 00:06:01,000
 Brahmin.

105
00:06:01,000 --> 00:06:06,000
 He's just a guy, really, a high class fellow.

106
00:06:06,000 --> 00:06:11,000
 And he walks up to them and he sees them burning this.

107
00:06:11,000 --> 00:06:14,000
 He looks at them burning their sun.

108
00:06:14,000 --> 00:06:16,000
 And he says, "Oh, look at you burning.

109
00:06:16,000 --> 00:06:19,000
 You must be burning a beast or an enemy of some sort."

110
00:06:19,000 --> 00:06:22,140
 And the bodhisattva looks at him and says, "No, we're

111
00:06:22,140 --> 00:06:24,000
 burning my sun."

112
00:06:24,000 --> 00:06:26,000
 And he looks at him and he says, "What do you mean?

113
00:06:26,000 --> 00:06:28,730
 How could this be your sun? You're not crying. You're not

114
00:06:28,730 --> 00:06:29,000
 upset.

115
00:06:29,000 --> 00:06:34,000
 You're not grieving at all."

116
00:06:34,000 --> 00:06:36,000
 He said, "Must have been a pretty poor sun."

117
00:06:36,000 --> 00:06:38,000
 And the bodhisattva says, "Well..."

118
00:06:38,000 --> 00:06:45,000
 And he gives this very famous verse in Buddhist circles.

119
00:06:45,000 --> 00:06:48,000
 Maybe actually not so famous. A very profound verse anyway.

120
00:06:48,000 --> 00:06:51,000
 Whether it's famous or not.

121
00:06:51,000 --> 00:06:55,000
 That goes something like...

122
00:06:55,000 --> 00:06:59,000
 Just as you can't...

123
00:06:59,000 --> 00:07:02,000
 Basically, don't cry over spilled milk.

124
00:07:02,000 --> 00:07:06,590
 He says, "Our tears won't bring back something that is

125
00:07:06,590 --> 00:07:07,000
 broken."

126
00:07:07,000 --> 00:07:14,350
 And something like, "Our cries won't bring the moon down

127
00:07:14,350 --> 00:07:16,000
 from up above."

128
00:07:16,000 --> 00:07:20,310
 And the verse after verse, he asks the man's wife, he asks

129
00:07:20,310 --> 00:07:21,000
 the daughter, he says,

130
00:07:21,000 --> 00:07:25,170
 "This was your husband. He must have beaten you if you're

131
00:07:25,170 --> 00:07:26,000
 not crying or you're not upset."

132
00:07:26,000 --> 00:07:29,000
 And he said, "Women are tenders."

133
00:07:29,000 --> 00:07:31,000
 He says something like that.

134
00:07:31,000 --> 00:07:33,000
 And women cry, you know.

135
00:07:33,000 --> 00:07:40,000
 And she says, "Not me. I loved him and he was a great man

136
00:07:40,000 --> 00:07:43,000
 and I was faithful and he was faithful to me."

137
00:07:43,000 --> 00:07:45,000
 But he's gone now.

138
00:07:45,000 --> 00:07:48,610
 And the verses go something like, "He fares the way he has

139
00:07:48,610 --> 00:07:50,000
 to tread."

140
00:07:50,000 --> 00:07:52,640
 And so they didn't grieve at all. It's a fairly famous

141
00:07:52,640 --> 00:07:53,000
 story.

142
00:07:53,000 --> 00:07:55,650
 Not exactly what you're asking, but it does give you some

143
00:07:55,650 --> 00:07:59,000
 idea as to how people prepare themselves.

144
00:07:59,000 --> 00:08:03,160
 What I always say, the whole idea of helping other people,

145
00:08:03,160 --> 00:08:09,000
 of course, is just to point out how troublesome it is.

146
00:08:09,000 --> 00:08:13,520
 But the best way to help people is to help yourself, to

147
00:08:13,520 --> 00:08:16,000
 make yourself an example.

148
00:08:16,000 --> 00:08:20,030
 And not only to make yourself an example, but to create the

149
00:08:20,030 --> 00:08:25,000
 power of mind necessary to feed them good vibes.

150
00:08:25,000 --> 00:08:29,270
 You'll find even just by sitting in meditation or after you

151
00:08:29,270 --> 00:08:31,000
 do insight meditation,

152
00:08:31,000 --> 00:08:34,590
 spend some time sending love to them, sending your good

153
00:08:34,590 --> 00:08:37,350
 thoughts, wishing them to be happy, wishing them to find

154
00:08:37,350 --> 00:08:38,000
 peace.

155
00:08:38,000 --> 00:08:41,000
 And you'll find it's incredible how they respond to that,

156
00:08:41,000 --> 00:08:45,000
 just that power of mind, how it changes them.

157
00:08:45,000 --> 00:08:48,000
 And it changes their attitude towards you.

158
00:08:48,000 --> 00:08:51,220
 It's interesting to find that the next time you talk to

159
00:08:51,220 --> 00:08:55,000
 that person, they're just ecstatic to hear from you,

160
00:08:55,000 --> 00:09:00,950
 and so happy to hear from you, and so kind, and just well-

161
00:09:00,950 --> 00:09:04,000
intentioned towards you.

162
00:09:04,000 --> 00:09:10,270
 So the power of our minds and the power of our intentions,

163
00:09:10,270 --> 00:09:13,000
 it's kind of like how theists will use prayer.

164
00:09:13,000 --> 00:09:16,790
 They set their mind on something, and sometimes the power

165
00:09:16,790 --> 00:09:18,000
 can carry through.

166
00:09:18,000 --> 00:09:22,130
 Well, the point is it gives them the... sets their mind in

167
00:09:22,130 --> 00:09:24,000
 the right way.

168
00:09:24,000 --> 00:09:27,020
 Then when they approach other people or so on, they will be

169
00:09:27,020 --> 00:09:28,000
 kind and generous,

170
00:09:28,000 --> 00:09:31,000
 and so good things will happen to them in return.

171
00:09:31,000 --> 00:09:35,000
 Beyond that, I mean, you can see what you can do.

172
00:09:35,000 --> 00:09:40,240
 As I said, if he's not religious, and if that means that he

173
00:09:40,240 --> 00:09:44,000
's not concerned about death,

174
00:09:44,000 --> 00:09:48,470
 or he's not taking the time to prepare himself, then it

175
00:09:48,470 --> 00:09:50,000
 might be quite difficult.

176
00:09:50,000 --> 00:09:56,000
 But you can give the prompt.

177
00:09:56,000 --> 00:10:00,000
 You can prompt people if they have the goodness in them,

178
00:10:00,000 --> 00:10:04,300
 or if they have what it takes in them to realize the

179
00:10:04,300 --> 00:10:07,000
 importance of such a thing.

180
00:10:07,000 --> 00:10:10,000
 I'm obviously avoiding saying, "Teach him meditation,"

181
00:10:10,000 --> 00:10:14,000
 because it sounds like probably he's still far from that,

182
00:10:14,000 --> 00:10:19,000
 but you take maybe steps, and you're not living his life,

183
00:10:19,000 --> 00:10:25,000
 so obviously you can't enlighten him. No one can.

184
00:10:25,000 --> 00:10:29,000
 But you can give him the prompt, and you can be an example,

185
00:10:29,000 --> 00:10:33,000
 and you can be a support and a friend.

186
00:10:33,000 --> 00:10:35,000
 That's really an important thing when people are dying,

187
00:10:35,000 --> 00:10:38,000
 is to be a good support, to be someone who's stable.

188
00:10:38,000 --> 00:10:42,000
 It calms people down to be with them and to not be crying

189
00:10:42,000 --> 00:10:43,000
 and moaning.

190
00:10:43,000 --> 00:10:46,650
 When my grandmother was dying, what they did with her is

191
00:10:46,650 --> 00:10:48,000
 they had her sing these songs.

192
00:10:48,000 --> 00:10:51,020
 She would have pain, horrible pain, and they had her sing

193
00:10:51,020 --> 00:10:52,000
 songs and so on.

194
00:10:52,000 --> 00:10:55,460
 It seemed kind of pathetic, really, because it wasn't

195
00:10:55,460 --> 00:10:56,000
 helping the pain,

196
00:10:56,000 --> 00:11:01,040
 it was just kind of helping her pretend that it wasn't

197
00:11:01,040 --> 00:11:02,000
 there.

198
00:11:02,000 --> 00:11:06,000
 But I did some chanting for her, some let her listen,

199
00:11:06,000 --> 00:11:09,000
 and she quieted down during the time that I was chanting.

200
00:11:09,000 --> 00:11:11,000
 I didn't know at that time, it was a long time ago,

201
00:11:11,000 --> 00:11:14,000
 I didn't know how to teach meditation.

202
00:11:14,000 --> 00:11:19,000
 I also would have had a lot more to say.

203
00:11:19,000 --> 00:11:22,000
 But being that stability for people,

204
00:11:22,000 --> 00:11:27,000
 and being a reminder for them,

205
00:11:27,000 --> 00:11:32,000
 because when your father is on death's door,

206
00:11:32,000 --> 00:11:35,000
 or whatever, at his last moment,

207
00:11:35,000 --> 00:11:36,790
 he's going to think about everything that happened in his

208
00:11:36,790 --> 00:11:37,000
 life,

209
00:11:37,000 --> 00:11:41,000
 and you will go through his mind as well, for sure.

210
00:11:41,000 --> 00:11:45,000
 So your job now is to make an impression on him.

211
00:11:45,000 --> 00:11:48,000
 I mean, these are the least that you can do.

212
00:11:48,000 --> 00:11:50,380
 If you can get him to meditate, then by all means, that's

213
00:11:50,380 --> 00:11:51,000
 wonderful.

214
00:11:51,000 --> 00:11:54,000
 And this can often happen for people who have had strokes,

215
00:11:54,000 --> 00:11:57,000
 or who have had seizures, or so on.

216
00:11:57,000 --> 00:12:02,000
 They can become religious, they can see the importance,

217
00:12:02,000 --> 00:12:04,000
 because when you're in that position,

218
00:12:04,000 --> 00:12:06,000
 your mind becomes focused and concentrated,

219
00:12:06,000 --> 00:12:09,000
 and you do start to remember things you've done,

220
00:12:09,000 --> 00:12:13,400
 remember, reflect on your life, and you feel bad about the

221
00:12:13,400 --> 00:12:14,000
 things.

222
00:12:14,000 --> 00:12:16,740
 People who have near-death experiences often change their

223
00:12:16,740 --> 00:12:19,000
 lives as a result.

224
00:12:19,000 --> 00:12:22,000
 They will realize that they only have so long,

225
00:12:22,000 --> 00:12:24,000
 and it can come to them at any time.

226
00:12:24,000 --> 00:12:30,000
 The invincible ideas leaves them.

227
00:12:30,000 --> 00:12:32,000
 So I hope that helps.

