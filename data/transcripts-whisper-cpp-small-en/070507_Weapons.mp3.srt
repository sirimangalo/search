1
00:00:00,000 --> 00:00:11,000
 [ Satsang with Mooji]

2
00:00:11,000 --> 00:00:21,000
 When we're going to overcome the dangers of samsara,

3
00:00:21,000 --> 00:00:31,000
 there's another teaching which is the wall or the fortress.

4
00:00:31,000 --> 00:00:37,940
 We have seven dhammas or seven teachings which become a

5
00:00:37,940 --> 00:00:39,000
 fortress for us,

6
00:00:39,000 --> 00:00:43,000
 a guard for us during the time that we practice,

7
00:00:43,000 --> 00:00:49,000
 a way for us to overcome or to avoid the dangers,

8
00:00:49,000 --> 00:00:56,400
 the twelve kinds of danger which we have to face in the

9
00:00:56,400 --> 00:00:58,000
 future.

10
00:00:58,000 --> 00:01:02,000
 And these seven things, when we practice them,

11
00:01:02,000 --> 00:01:06,000
 they will be both a guard against the dangers

12
00:01:06,000 --> 00:01:17,000
 and will be a means for us to escape or to go beyond.

13
00:01:17,000 --> 00:01:23,810
 They will go beyond ever having to meet with such dangers

14
00:01:23,810 --> 00:01:25,000
 again.

15
00:01:25,000 --> 00:01:33,000
 The seven things are, "hata sanyato", number one,

16
00:01:33,000 --> 00:01:40,000
 guarding our hands or watching our hands or keeping watch

17
00:01:40,000 --> 00:01:43,000
 on the movements of our hands,

18
00:01:43,000 --> 00:01:46,000
 the things which our hands do.

19
00:01:46,000 --> 00:01:52,000
 Number two, "patasanyato", guarding our feet.

20
00:01:52,000 --> 00:02:04,000
 Number three, "sanyatatamo", guarding ourselves.

21
00:02:04,000 --> 00:02:13,000
 Number four, "ajchattarato", guarding our minds,

22
00:02:13,000 --> 00:02:27,000
 guarding what is within, guarding our internal self.

23
00:02:27,000 --> 00:02:34,390
 Number, we have, "hassurewa jasanyato", guarding our speech

24
00:02:34,390 --> 00:02:35,000
.

25
00:02:35,000 --> 00:02:42,400
 And number six, "susamahito", being internally composed,

26
00:02:42,400 --> 00:02:45,000
 well composed inside.

27
00:02:45,000 --> 00:02:53,880
 And number seven, "eko santusito", being content with

28
00:02:53,880 --> 00:02:58,000
 solitude, with being alone.

29
00:02:58,000 --> 00:03:02,730
 And these seven things, the Lord Buddha said, are seven

30
00:03:02,730 --> 00:03:07,000
 protections, a guard or a fortress or a wall,

31
00:03:07,000 --> 00:03:13,000
 which keeps danger out, keeps suffering away,

32
00:03:13,000 --> 00:03:18,010
 and which leads us to become free from all suffering in the

33
00:03:18,010 --> 00:03:19,000
 future.

34
00:03:19,000 --> 00:03:23,000
 "Hatta sanyato" means guarding what we do with our hands,

35
00:03:23,000 --> 00:03:25,960
 because most of the good and evil which we do in the

36
00:03:25,960 --> 00:03:28,000
 physical realm is with our hands.

37
00:03:28,000 --> 00:03:31,100
 When we kill it's normally with our hands, when we steal it

38
00:03:31,100 --> 00:03:34,000
's normally with our hands, and so on.

39
00:03:34,000 --> 00:03:38,000
 Many bad things come from our hands.

40
00:03:38,000 --> 00:03:41,220
 But also in terms of meditation, simply watching what our

41
00:03:41,220 --> 00:03:42,000
 hands are doing,

42
00:03:42,000 --> 00:03:46,610
 because we use them more often than any part of our

43
00:03:46,610 --> 00:03:48,000
 physical body.

44
00:03:48,000 --> 00:03:58,000
 Or we use them more than most parts of our physical body.

45
00:03:58,000 --> 00:04:02,510
 And so we watch what are we doing with our hands when we're

46
00:04:02,510 --> 00:04:17,000
 eating, when we're acting, whatever we do.

47
00:04:17,000 --> 00:04:23,000
 Being aware of what our hands are, what our hands are.

48
00:04:23,000 --> 00:04:29,490
 Most importantly, "gart" means guarding evil and wholesome

49
00:04:29,490 --> 00:04:35,000
 acts of body that are done with the hand.

50
00:04:35,000 --> 00:04:37,940
 This is something which of course protects us from any of

51
00:04:37,940 --> 00:04:46,000
 the dangers of guilt or of punishment for wrongdoing.

52
00:04:46,000 --> 00:04:49,820
 But it's also important for meditators that we are aware of

53
00:04:49,820 --> 00:04:53,000
 the movements of our hands when we're eating,

54
00:04:53,000 --> 00:04:58,000
 or when we're washing our clothes or brushing our teeth,

55
00:04:58,000 --> 00:04:59,000
 and so on.

56
00:04:59,000 --> 00:05:01,000
 Being aware of what our hands are doing.

57
00:05:01,000 --> 00:05:08,000
 And we can make acknowledgement as we move.

58
00:05:08,000 --> 00:05:12,000
 And then "pada sanyato" means watching our feet.

59
00:05:12,000 --> 00:05:17,330
 So as we act mostly with our hands, we move around mostly

60
00:05:17,330 --> 00:05:19,000
 with our feet.

61
00:05:19,000 --> 00:05:22,400
 So we have to be careful about where we go and what we're

62
00:05:22,400 --> 00:05:28,000
 doing, why are we doing the things we're doing.

63
00:05:28,000 --> 00:05:33,650
 Be careful not to let our feet take us into a place which

64
00:05:33,650 --> 00:05:36,000
 is inappropriate.

65
00:05:36,000 --> 00:05:44,330
 Let our feet do the walking. They might take us to, make it

66
00:05:44,330 --> 00:05:48,000
 easily take us to a place which is inappropriate.

67
00:05:48,000 --> 00:05:54,820
 And most often for meditators when we walk, it's easy for

68
00:05:54,820 --> 00:05:58,000
 our minds to wander into what they see

69
00:05:58,000 --> 00:06:01,000
 and into the new experiences as we walk around.

70
00:06:01,000 --> 00:06:03,990
 So it's important that when we're walking as meditators

71
00:06:03,990 --> 00:06:06,000
 that we're aware that we're walking.

72
00:06:06,000 --> 00:06:09,960
 Wherever we walk, when we walk around the center, or in

73
00:06:09,960 --> 00:06:15,000
 ordinary life when we walk around to our various

74
00:06:15,000 --> 00:06:15,000
 destinations

75
00:06:15,000 --> 00:06:19,000
 that we're aware of our feet. Walking, walking, walking.

76
00:06:19,000 --> 00:06:23,220
 We should keep our minds in with the present moment so that

77
00:06:23,220 --> 00:06:26,000
 they don't get caught up by the new experiences,

78
00:06:26,000 --> 00:06:31,330
 the phenomena which arise, which come in as we walk around,

79
00:06:31,330 --> 00:06:35,000
 as we go to a new place.

80
00:06:35,000 --> 00:06:39,470
 If we guard our feet in this way, we can overcome any

81
00:06:39,470 --> 00:06:41,000
 dangers as well.

82
00:06:41,000 --> 00:06:45,540
 We don't let ourselves be taken away into places which are

83
00:06:45,540 --> 00:06:47,000
 inappropriate.

84
00:06:47,000 --> 00:06:49,600
 Guarding our feet doesn't just mean the feet, it means

85
00:06:49,600 --> 00:06:51,000
 guarding our minds

86
00:06:51,000 --> 00:06:55,980
 and guarding the place where we go, not letting ourselves

87
00:06:55,980 --> 00:06:59,000
 go into the wrong place.

88
00:06:59,000 --> 00:07:03,230
 But it also means guarding against all evil deeds which can

89
00:07:03,230 --> 00:07:05,000
 be done with the feet.

90
00:07:05,000 --> 00:07:11,410
 And then number three, "vaya jasanyatva watching our speech

91
00:07:11,410 --> 00:07:12,000
."

92
00:07:12,000 --> 00:07:15,630
 So in ordinary life this means guarding against telling

93
00:07:15,630 --> 00:07:20,000
 lies or harsh speech, divisive speech or useless speech.

94
00:07:20,000 --> 00:07:24,490
 Guarding against all evil deeds which are done when we say

95
00:07:24,490 --> 00:07:30,000
 bad things to people or about people behind their backs.

96
00:07:30,000 --> 00:07:34,970
 When we try to deceive people or hurt other people with

97
00:07:34,970 --> 00:07:36,000
 speech.

98
00:07:36,000 --> 00:07:40,220
 But in meditation practice it goes even further and of

99
00:07:40,220 --> 00:07:45,250
 course meditators are expected to talk as little as

100
00:07:45,250 --> 00:07:46,000
 possible.

101
00:07:46,000 --> 00:07:50,500
 To talk only as necessary when it's important to speak than

102
00:07:50,500 --> 00:07:53,000
 speak when it's not necessary to speak.

103
00:07:53,000 --> 00:07:57,010
 There's no reason except in the pleasure which comes from

104
00:07:57,010 --> 00:07:58,000
 speaking.

105
00:07:58,000 --> 00:08:08,000
 We avoid or we refrain from such speech.

106
00:08:08,000 --> 00:08:12,990
 The reason being was as we talk and talk and if we tend to

107
00:08:12,990 --> 00:08:14,000
 speak too much

108
00:08:14,000 --> 00:08:17,520
 we find ourselves becoming distracted and find more and

109
00:08:17,520 --> 00:08:19,000
 more thoughts arising

110
00:08:19,000 --> 00:08:22,470
 and our minds are not able to calm down and this can be a

111
00:08:22,470 --> 00:08:23,000
 danger.

112
00:08:23,000 --> 00:08:26,000
 This is a way to fall into some of the dangers.

113
00:08:26,000 --> 00:08:29,590
 It's easy for us to get lost back in the world, to get

114
00:08:29,590 --> 00:08:34,000
 caught up in the world again based on our speech,

115
00:08:34,000 --> 00:08:38,190
 based on simply talking too much, talking more than is

116
00:08:38,190 --> 00:08:39,000
 required.

117
00:08:39,000 --> 00:08:42,210
 We find ourselves talking about things which are unrelated

118
00:08:42,210 --> 00:08:45,000
 to meditation and suddenly we get caught up in a whirlpool

119
00:08:45,000 --> 00:08:52,000
 or caught up by a shark or a crocodile or something.

120
00:08:52,000 --> 00:08:57,450
 As I talked about in the dangers for someone who is on a

121
00:08:57,450 --> 00:08:58,000
 boat,

122
00:08:58,000 --> 00:09:05,540
 same of the dangers for one is swimming across the ocean,

123
00:09:05,540 --> 00:09:08,000
 swimming across the ocean of samsara.

124
00:09:08,000 --> 00:09:15,000
 We have to watch our hands, our feet and our mouth.

125
00:09:15,000 --> 00:09:18,000
 And number four, we have to watch ourselves.

126
00:09:18,000 --> 00:09:20,970
 This means apart from these three things we have to watch

127
00:09:20,970 --> 00:09:22,000
 our whole self.

128
00:09:22,000 --> 00:09:28,000
 We have to watch our eyes, guard our eyes.

129
00:09:28,000 --> 00:09:30,730
 For meditators the best thing and for monks of course as

130
00:09:30,730 --> 00:09:37,000
 well is that we watch the ground where we're walking.

131
00:09:37,000 --> 00:09:42,890
 Mahasi Sayad does send you, even though you have perfectly

132
00:09:42,890 --> 00:09:47,000
 good vision, you should act as if you're blind.

133
00:09:47,000 --> 00:09:52,120
 Even if we can hear perfectly we should act as if we're

134
00:09:52,120 --> 00:09:53,000
 deaf.

135
00:09:53,000 --> 00:09:57,200
 Even though we have such strength of body, this is guarding

136
00:09:57,200 --> 00:10:02,000
 ourselves, we give up this strength of body.

137
00:10:02,000 --> 00:10:08,090
 We move slowly, we act as if we're sick, as if we're an old

138
00:10:08,090 --> 00:10:14,000
 person with very little strength.

139
00:10:14,000 --> 00:10:18,790
 We act with very little strength, thinking that if we were

140
00:10:18,790 --> 00:10:21,000
 to get caught up in our strength

141
00:10:21,000 --> 00:10:25,360
 and get caught up in rapid movements, we would lose our

142
00:10:25,360 --> 00:10:28,000
 concentration and our mindfulness.

143
00:10:28,000 --> 00:10:32,000
 It could be a danger for us in our practice.

144
00:10:32,000 --> 00:10:35,440
 Of course there are many evils which come from not guarding

145
00:10:35,440 --> 00:10:36,000
 oneself.

146
00:10:36,000 --> 00:10:40,620
 If we don't guard where we look, if we don't guard what we

147
00:10:40,620 --> 00:10:45,000
 do with our body, we can end up in great danger.

148
00:10:45,000 --> 00:10:49,580
 Both in terms of suffering and in terms of attraction, we

149
00:10:49,580 --> 00:10:55,000
 can fall into desire for things that we see,

150
00:10:55,000 --> 00:10:58,000
 for intoxication with our own strength.

151
00:10:58,000 --> 00:11:02,340
 We have to guard ourselves and be aware of our body as it's

152
00:11:02,340 --> 00:11:03,000
 moving.

153
00:11:03,000 --> 00:11:06,270
 When we bend, we acknowledge bending, when we stretch,

154
00:11:06,270 --> 00:11:10,000
 stretching, when we turn around, turning,

155
00:11:10,000 --> 00:11:14,000
 when we reach for something, reaching, and so on.

156
00:11:14,000 --> 00:11:17,300
 And the sixth sense is seeing, hearing, smelling, tasting,

157
00:11:17,300 --> 00:11:21,000
 feeling. Think this is guarding ourselves.

158
00:11:21,000 --> 00:11:25,560
 Number five is being internally, guarding ourselves

159
00:11:25,560 --> 00:11:29,000
 internally means guarding the heart.

160
00:11:29,000 --> 00:11:36,400
 So once we have a watch, we have a good hold on all of the

161
00:11:36,400 --> 00:11:40,000
 external parts of what makes us who we are,

162
00:11:40,000 --> 00:11:44,000
 then we guard also internally, we guard our mind.

163
00:11:44,000 --> 00:11:47,310
 Because even if we're not doing anything bad with our hands

164
00:11:47,310 --> 00:11:49,000
 or our feet or any other part of the body,

165
00:11:49,000 --> 00:11:55,840
 or our speech, if our mind is distracted or is angry or is

166
00:11:55,840 --> 00:12:00,000
 lustful, greedy, or deluded,

167
00:12:00,000 --> 00:12:04,470
 we have wrong view or conceit, or we're holding on to wrong

168
00:12:04,470 --> 00:12:05,000
 ideas,

169
00:12:05,000 --> 00:12:10,840
 or holding on to ourselves, judging ourselves or other

170
00:12:10,840 --> 00:12:12,000
 people.

171
00:12:12,000 --> 00:12:15,000
 These can be, of course, a great danger for us.

172
00:12:15,000 --> 00:12:17,590
 They lead us into great suffering and will, of course,

173
00:12:17,590 --> 00:12:22,570
 later lead us to do bad things in the future by speech or

174
00:12:22,570 --> 00:12:25,000
 action.

175
00:12:25,000 --> 00:12:27,180
 So we have to guard internally and when our mind goes off

176
00:12:27,180 --> 00:12:29,000
 into bad things, we have to catch it.

177
00:12:29,000 --> 00:12:32,000
 When we like something, we say liking, liking.

178
00:12:32,000 --> 00:12:36,000
 If we don't like something, we say disliking, disliking.

179
00:12:36,000 --> 00:12:39,560
 If we feel drowsy or lazy, we say lazy, lazy. If we feel

180
00:12:39,560 --> 00:12:41,000
 acted, distracted.

181
00:12:41,000 --> 00:12:44,070
 If we have doubts, doubt about this or that ourselves or

182
00:12:44,070 --> 00:12:47,000
 the practice, doubting, doubting.

183
00:12:47,000 --> 00:12:52,790
 Guarding ourselves internally from falling into evil and

184
00:12:52,790 --> 00:12:55,000
 wholesome states.

185
00:12:55,000 --> 00:13:00,690
 And number six, "su-su-ma-hi-to" means developing not only

186
00:13:00,690 --> 00:13:05,000
 should we guard our minds against unwholesome states,

187
00:13:05,000 --> 00:13:08,460
 we should develop in our minds all sorts of wholesome

188
00:13:08,460 --> 00:13:09,000
 states.

189
00:13:09,000 --> 00:13:13,250
 Meaning we should practice tranquility meditation and we

190
00:13:13,250 --> 00:13:15,000
 should practice insight meditation.

191
00:13:15,000 --> 00:13:18,630
 Or if we have only short time, then we can practice simply

192
00:13:18,630 --> 00:13:20,000
 insight meditation.

193
00:13:20,000 --> 00:13:25,310
 But at any rate, we have to practice to calm our minds and

194
00:13:25,310 --> 00:13:28,600
 to develop insight and characteristics which are imper

195
00:13:28,600 --> 00:13:32,000
manent, suffering and non-self.

196
00:13:32,000 --> 00:13:36,340
 This is "su-su-su-ma-hi-to". We develop a sense of

197
00:13:36,340 --> 00:13:46,000
 awareness of calm and peaceful and equanimous awareness.

198
00:13:46,000 --> 00:13:49,290
 Where we don't look at things as good or bad, we don't have

199
00:13:49,290 --> 00:13:50,000
 any more judgment for things.

200
00:13:50,000 --> 00:13:54,050
 Our mind is balanced and is well composed and is very

201
00:13:54,050 --> 00:13:56,000
 focused internally.

202
00:13:56,000 --> 00:13:59,810
 Seeing everything is simply phenomena which arise and sees

203
00:13:59,810 --> 00:14:02,000
 arise and sees it every moment.

204
00:14:02,000 --> 00:14:06,060
 Not holding on to anything as permanent, as satisfying as

205
00:14:06,060 --> 00:14:07,000
 our self.

206
00:14:07,000 --> 00:14:10,230
 This is the development of the practice of the four

207
00:14:10,230 --> 00:14:14,000
 foundations of mindfulness which we are undertaking.

208
00:14:14,000 --> 00:14:19,010
 And the last weapon or the last guard which we have is

209
00:14:19,010 --> 00:14:22,000
 being content with seclusion.

210
00:14:22,000 --> 00:14:26,000
 Being happy, being alone.

211
00:14:26,000 --> 00:14:32,870
 When we are content with staying to ourselves without

212
00:14:32,870 --> 00:14:40,000
 getting involved or actively involved with other people.

213
00:14:40,000 --> 00:14:43,600
 This is the time when we can really come to see the truth

214
00:14:43,600 --> 00:14:45,000
 about ourselves.

215
00:14:45,000 --> 00:14:48,890
 But if we are working about or talking to or being around

216
00:14:48,890 --> 00:14:52,550
 other people, it's very difficult for us to see our own

217
00:14:52,550 --> 00:14:55,000
 emotions or our own problems.

218
00:14:55,000 --> 00:14:58,120
 Because we are always looking at the faults and the

219
00:14:58,120 --> 00:15:00,000
 problems of other people.

220
00:15:00,000 --> 00:15:03,210
 And as long as we look at the faults and the problems of

221
00:15:03,210 --> 00:15:06,800
 other people, it's very difficult for us to come to terms

222
00:15:06,800 --> 00:15:11,000
 with our own faults and our own problems.

223
00:15:11,000 --> 00:15:19,680
 For this reason, the Lord Buddha was always very appreci

224
00:15:19,680 --> 00:15:23,000
ative of solitude.

225
00:15:23,000 --> 00:15:28,800
 Finding a place and finding time to look inside, to see

226
00:15:28,800 --> 00:15:30,000
 ourselves.

227
00:15:30,000 --> 00:15:38,530
 And Lord Buddha was always praising solitude whenever we

228
00:15:38,530 --> 00:15:45,060
 could find time or find a place to be alone and not have to

229
00:15:45,060 --> 00:15:50,000
 fall in with, get involved with people through love or lust

230
00:15:50,000 --> 00:15:50,000
,

231
00:15:50,000 --> 00:15:54,210
 through desire or attraction, or out of anger or

232
00:15:54,210 --> 00:15:58,810
 frustration at other people trying to change them or

233
00:15:58,810 --> 00:16:05,000
 improve them or fix their problems and their faults.

234
00:16:05,000 --> 00:16:08,390
 When we stay to ourselves then we only have our own

235
00:16:08,390 --> 00:16:10,000
 problems to deal with.

236
00:16:10,000 --> 00:16:14,710
 And this makes it very easy for us, or much easier for us

237
00:16:14,710 --> 00:16:17,000
 to improve ourselves.

238
00:16:17,000 --> 00:16:19,750
 This is a weapon. If we get involved with other people it

239
00:16:19,750 --> 00:16:21,000
 can be a great danger.

240
00:16:21,000 --> 00:16:25,000
 It can be a cause for great danger to arise.

241
00:16:25,000 --> 00:16:27,760
 So these seven things, they go along with the teaching on

242
00:16:27,760 --> 00:16:29,000
 the 12 dangers.

243
00:16:29,000 --> 00:16:32,000
 These are our weapons.

244
00:16:32,000 --> 00:16:35,230
 Guarding our hands, really, truly watching what our hands

245
00:16:35,230 --> 00:16:36,000
 are doing.

246
00:16:36,000 --> 00:16:39,000
 Guarding our feet, watching where we're going.

247
00:16:39,000 --> 00:16:43,740
 Guarding our speech, trying not to say a lot or say things

248
00:16:43,740 --> 00:16:46,000
 which are unimportant.

249
00:16:46,000 --> 00:16:49,000
 Guarding ourselves, the rest of our body.

250
00:16:49,000 --> 00:16:51,000
 Guarding our mind internally.

251
00:16:51,000 --> 00:16:56,690
 And developing our mind in terms of tranquility and wisdom

252
00:16:56,690 --> 00:16:58,000
 and insight.

253
00:16:58,000 --> 00:17:02,000
 And keeping to ourselves, staying in seclusion.

254
00:17:02,000 --> 00:17:07,950
 One of our greatest weapons is a viveka vuta, the weapon of

255
00:17:07,950 --> 00:17:10,000
 solitude.

256
00:17:10,000 --> 00:17:15,000
 So this is the teaching I'd like to give for today.

257
00:17:15,000 --> 00:17:19,080
 Now we'll continue with mindful prostration walking and

258
00:17:19,080 --> 00:17:20,000
 sitting.

