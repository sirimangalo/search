1
00:00:00,000 --> 00:00:08,230
 Oh good evening. Tonight I thought I would talk about

2
00:00:08,230 --> 00:00:11,600
 social distancing.

3
00:00:11,600 --> 00:00:21,880
 It's a buzzword these days as people are concerned with the

4
00:00:21,880 --> 00:00:27,120
 spread of sickness.

5
00:00:27,120 --> 00:00:32,710
 So there's a call for people to engage in social distancing

6
00:00:32,710 --> 00:00:34,320
.

7
00:00:34,320 --> 00:00:41,390
 I think it's a unique opportunity because we have so many

8
00:00:41,390 --> 00:00:43,760
 people in the world

9
00:00:43,760 --> 00:00:50,040
 engaging in a similar activity that may be quite different

10
00:00:50,040 --> 00:00:52,960
 from how they normally act,

11
00:00:52,960 --> 00:01:00,080
 how they normally behave. And moreover an activity that is

12
00:01:00,080 --> 00:01:04,080
 very close to the sorts of things

13
00:01:04,080 --> 00:01:07,280
 recommended in buddhism.

14
00:01:07,280 --> 00:01:17,680
 Because social distancing is a way of looking inside. It's

15
00:01:17,680 --> 00:01:20,800
 a way of focusing your attention on

16
00:01:22,080 --> 00:01:29,380
 your own being. And so it goes quite well with mental

17
00:01:29,380 --> 00:01:31,920
 development and introspection and

18
00:01:31,920 --> 00:01:38,720
 developing understanding of the nature of one's own mind.

19
00:01:38,720 --> 00:01:49,520
 If done well. And so of course it's not to say that just by

20
00:01:49,520 --> 00:01:51,200
 practicing social distancing you

21
00:01:51,200 --> 00:01:55,660
 somehow become wiser and understand yourself more. And so I

22
00:01:55,660 --> 00:01:59,120
 thought I'd talk a little bit about

23
00:01:59,120 --> 00:02:05,930
 how buddhists engage in social distancing or how one might

24
00:02:05,930 --> 00:02:08,480
 engage properly in

25
00:02:08,480 --> 00:02:15,710
 social distancing for beneficial results. For it to be a

26
00:02:15,710 --> 00:02:18,160
 beneficial activity.

27
00:02:18,160 --> 00:02:25,440
 So it's not to say that one should also always be socially

28
00:02:25,440 --> 00:02:25,920
 distant.

29
00:02:25,920 --> 00:02:31,380
 But it has to be acknowledged and buddhism does try to help

30
00:02:31,380 --> 00:02:33,680
 people to acknowledge that

31
00:02:33,680 --> 00:02:38,800
 if we're constantly caught up in society

32
00:02:38,800 --> 00:02:44,240
 we'll have very little opportunity to understand ourselves.

33
00:02:44,240 --> 00:02:45,520
 Very little progress

34
00:02:47,200 --> 00:02:52,240
 in understanding the nature of our minds in terms of what

35
00:02:52,240 --> 00:02:54,400
 is causing us stress and suffering and

36
00:02:54,400 --> 00:02:56,800
 how we might change that.

37
00:02:56,800 --> 00:03:03,260
 So as with all things social distancing is in the physical

38
00:03:03,260 --> 00:03:03,920
 realm.

39
00:03:03,920 --> 00:03:09,590
 We're not talking about becoming mentally distant. There's

40
00:03:09,590 --> 00:03:11,920
 no call in the world right now for people

41
00:03:11,920 --> 00:03:16,120
 to be emotionally or mentally distant from each other. No

42
00:03:16,120 --> 00:03:18,560
 we're talking about something physical

43
00:03:18,560 --> 00:03:22,240
 and so on the face it actually doesn't have anything to do

44
00:03:22,240 --> 00:03:27,600
 with buddhism. But as with all things

45
00:03:27,600 --> 00:03:35,280
 it all things physical the quality of it depends very much

46
00:03:35,280 --> 00:03:37,520
 on the state of the mind

47
00:03:37,520 --> 00:03:41,370
 as one performs the act. And moreover the performing of

48
00:03:41,370 --> 00:03:44,480
 certain physical acts can facilitate

49
00:03:44,480 --> 00:03:47,460
 the evoking of certain mind states. So when we do walking

50
00:03:47,460 --> 00:03:49,280
 meditation and sitting meditation you

51
00:03:49,280 --> 00:03:53,500
 might say well it's just physical and what use is that? But

52
00:03:53,500 --> 00:03:56,080
 by engaging in such a simple activity

53
00:03:56,080 --> 00:04:01,670
 we give the mind an opportunity to flower, to blossom, to

54
00:04:01,670 --> 00:04:05,760
 do its work because it's not

55
00:04:05,760 --> 00:04:07,920
 preoccupied with complicated movements.

56
00:04:07,920 --> 00:04:22,880
 And so one good way to look at any physical activity is in

57
00:04:22,880 --> 00:04:24,000
 terms of it being

58
00:04:24,000 --> 00:04:27,050
 beneficial or harmful and the different ways in which you

59
00:04:27,050 --> 00:04:28,640
 could undertake it that might be

60
00:04:28,640 --> 00:04:34,610
 harmful is what we call the four agati. Agati is loosely

61
00:04:34,610 --> 00:04:39,600
 translated as partialities or biases.

62
00:04:39,600 --> 00:04:45,230
 Biases better. But literally it means what you're led by I

63
00:04:45,230 --> 00:04:47,440
 think, what leads you.

64
00:04:47,440 --> 00:04:53,000
 And so we do certain things we're led by this or we're led

65
00:04:53,000 --> 00:04:53,840
 by that.

66
00:04:53,840 --> 00:05:07,920
 And there are four reasons why one might do something. What

67
00:05:07,920 --> 00:05:09,680
 leads one or what one has

68
00:05:09,680 --> 00:05:20,280
 gone over to as a reason for doing something physical. The

69
00:05:20,280 --> 00:05:22,640
 same goes of course with speech.

70
00:05:22,640 --> 00:05:28,400
 Anything in the in those physical realm. And these four are

71
00:05:28,400 --> 00:05:33,280
 chanda. We do something based on

72
00:05:33,280 --> 00:05:43,120
 desire or liking it or or liking in general. Dosa we do

73
00:05:43,120 --> 00:05:46,400
 something based on anger.

74
00:05:48,720 --> 00:05:53,780
 Bhaya we do something based on fear. And moha we do

75
00:05:53,780 --> 00:05:57,120
 something based on delusion.

76
00:05:57,120 --> 00:06:03,300
 These are the four agati. They're all they're all bad

77
00:06:03,300 --> 00:06:05,680
 reasons to do something.

78
00:06:05,680 --> 00:06:09,350
 So we'll talk about them and then we'll talk about some of

79
00:06:09,350 --> 00:06:10,800
 the right reasons to do things

80
00:06:10,800 --> 00:06:14,480
 or how to make things especially things like social dist

81
00:06:14,480 --> 00:06:16,240
ancing really beneficial.

82
00:06:18,000 --> 00:06:21,760
 And to avoid making them problematic because something so

83
00:06:21,760 --> 00:06:23,280
 extreme for many people

84
00:06:23,280 --> 00:06:27,590
 as social distancing can have debilitating effects can have

85
00:06:27,590 --> 00:06:28,560
 negative effects.

86
00:06:28,560 --> 00:06:41,440
 So chanda ghati is the bias of partiality or of liking.

87
00:06:44,000 --> 00:06:47,600
 And this encompasses all kinds of desire all kinds of greed

88
00:06:47,600 --> 00:06:47,680
.

89
00:06:47,680 --> 00:06:56,040
 So people might social distance because of stinginess miser

90
00:06:56,040 --> 00:06:58,560
liness. Miser's become very distant

91
00:06:58,560 --> 00:07:03,790
 because of people asking them for for donations and gifts

92
00:07:03,790 --> 00:07:05,600
 and so on loans often.

93
00:07:07,680 --> 00:07:12,580
 I met a woman in Thailand when I was living there. I met

94
00:07:12,580 --> 00:07:14,400
 her a few times once we went to

95
00:07:14,400 --> 00:07:18,620
 I was with Adjan Tong quite often my teacher and she would

96
00:07:18,620 --> 00:07:20,880
 every time she came she was not

97
00:07:20,880 --> 00:07:25,610
 she was very rich and she was quite exceptional. I've never

98
00:07:25,610 --> 00:07:26,960
 met anyone like her

99
00:07:26,960 --> 00:07:30,530
 everywhere she went when she met a monk especially because

100
00:07:30,530 --> 00:07:33,760
 she had great confidence and great

101
00:07:35,040 --> 00:07:39,480
 respect for the monastic life. She would always give

102
00:07:39,480 --> 00:07:41,840
 something. I remember going up

103
00:07:41,840 --> 00:07:48,490
 to to sit in on the reporting of the meditators the

104
00:07:48,490 --> 00:07:51,120
 meetings between the teacher and the meditator

105
00:07:51,120 --> 00:07:54,370
 and she was there and she saw me come up and she had just

106
00:07:54,370 --> 00:07:58,080
 been giving out gifts to the monks and

107
00:07:58,080 --> 00:08:01,840
 she saw me there and immediately she thought and she looked

108
00:08:01,840 --> 00:08:04,160
 in and she had a pen in her purse she

109
00:08:04,160 --> 00:08:07,030
 looked in her purse and she said here I would like to give

110
00:08:07,030 --> 00:08:08,000
 you a gift of a pen.

111
00:08:08,000 --> 00:08:13,950
 But for most people it's not like that. We went to her

112
00:08:13,950 --> 00:08:16,800
 house once and she was talking about

113
00:08:16,800 --> 00:08:21,540
 all the many requests for donations that she would get. She

114
00:08:21,540 --> 00:08:23,760
 would get letters and letters every day

115
00:08:23,760 --> 00:08:27,940
 from teachers and firefighters and police and so on.

116
00:08:27,940 --> 00:08:30,480
 Everybody asking for a donation.

117
00:08:31,280 --> 00:08:36,650
 And so for most people this becomes too much rich people.

118
00:08:36,650 --> 00:08:39,120
 It becomes too much for

119
00:08:39,120 --> 00:08:43,240
 ordinary people as well. We become quite distant if we

120
00:08:43,240 --> 00:08:45,200
 think people are going to

121
00:08:45,200 --> 00:08:49,190
 or if people are constantly asking for loans or gifts or so

122
00:08:49,190 --> 00:08:49,600
 on.

123
00:08:49,600 --> 00:08:54,080
 And this is problematic. You

124
00:08:56,960 --> 00:09:00,260
 to some extent you have to protect yourself if people are

125
00:09:00,260 --> 00:09:02,720
 greedy and and they're clingy or so on.

126
00:09:02,720 --> 00:09:07,070
 But it can't be because of your own greed. It has to be of

127
00:09:07,070 --> 00:09:09,120
 course because of something

128
00:09:09,120 --> 00:09:14,160
 more beneficial. For some people they get very greedy and

129
00:09:14,160 --> 00:09:16,640
 therefore keep to themselves because

130
00:09:16,640 --> 00:09:20,550
 they don't want to share. For things like social distancing

131
00:09:20,550 --> 00:09:22,560
 for most people it's not going to be

132
00:09:22,560 --> 00:09:28,240
 like that but the results of having to seclude yourself for

133
00:09:28,240 --> 00:09:31,120
 many people are quite devastating.

134
00:09:31,120 --> 00:09:38,090
 Loss of livelihood, for students loss of education. For

135
00:09:38,090 --> 00:09:41,520
 some people it can lead to great stress

136
00:09:41,520 --> 00:09:45,410
 having to be in close quarters with others. So you call it

137
00:09:45,410 --> 00:09:47,520
 social distancing but in fact it puts

138
00:09:47,520 --> 00:09:50,440
 you close to people who you might have a hard time being

139
00:09:50,440 --> 00:09:53,040
 close to. Your family members sometimes you

140
00:09:53,040 --> 00:09:56,820
 might not get along. Siblings even children with their

141
00:09:56,820 --> 00:09:58,000
 parents and so on.

142
00:09:58,000 --> 00:10:07,770
 But in some cases it can I think perhaps be an excuse for

143
00:10:07,770 --> 00:10:10,080
 increased

144
00:10:12,960 --> 00:10:17,860
 engagement in sensuality. So for many people students a

145
00:10:17,860 --> 00:10:19,840
 chance to stay at home and not have

146
00:10:19,840 --> 00:10:23,470
 to go to school might seem like a chance to just play games

147
00:10:23,470 --> 00:10:25,600
 all the time or watch television all

148
00:10:25,600 --> 00:10:29,590
 the time. For some people it might just be a means of

149
00:10:29,590 --> 00:10:33,920
 relaxing of being lazy. For some people

150
00:10:33,920 --> 00:10:38,060
 driven by stress the stress of all the other reasons all

151
00:10:38,060 --> 00:10:40,880
 the other problems associated with

152
00:10:42,640 --> 00:10:49,680
 the pandemic and sickness and so on. It can drive them to

153
00:10:49,680 --> 00:10:53,120
 increased entertainment and things like

154
00:10:53,120 --> 00:10:57,330
 binge eating and so on. And as a result there's a lot of

155
00:10:57,330 --> 00:11:00,160
 problems that come from this. As a result

156
00:11:00,160 --> 00:11:06,740
 there will be increased irritability for someone who is

157
00:11:06,740 --> 00:11:10,000
 constantly engaged in entertainment and

158
00:11:11,120 --> 00:11:15,470
 enjoyment of sensuality. They have the increased capacity

159
00:11:15,470 --> 00:11:18,960
 of becoming angry, irritated, annoyed.

160
00:11:18,960 --> 00:11:24,080
 Leads to fighting among siblings and families and loved

161
00:11:24,080 --> 00:11:27,200
 ones. So a big reason perhaps why

162
00:11:27,200 --> 00:11:34,800
 people engaging in this sort of social distancing or having

163
00:11:34,800 --> 00:11:37,280
 stress having to stay at home as a

164
00:11:37,280 --> 00:11:42,460
 family is because of their incapacity to be content, to be

165
00:11:42,460 --> 00:11:45,520
 at peace and so constantly fighting over

166
00:11:45,520 --> 00:11:49,580
 what television program we're going to watch or fighting

167
00:11:49,580 --> 00:11:52,320
 over belongings and money, fighting over

168
00:11:52,320 --> 00:11:57,260
 all sorts of things that should bring are supposed to bring

169
00:11:57,260 --> 00:12:01,920
 pleasure and instead bring great suffering.

170
00:12:05,360 --> 00:12:09,270
 So when engaging in this sort of activity you have to be

171
00:12:09,270 --> 00:12:10,160
 mindful of

172
00:12:10,160 --> 00:12:18,020
 sorts of things like sensuality and craving and so on.

173
00:12:18,020 --> 00:12:21,680
 Because it might be seen as a license or

174
00:12:21,680 --> 00:12:26,560
 an opportunity to increase in our activity, our engagement

175
00:12:26,560 --> 00:12:29,360
 in entertainment and so on.

176
00:12:29,360 --> 00:12:31,680
 Which is problematic.

177
00:12:34,880 --> 00:12:38,400
 The second reason based on anger.

178
00:12:38,400 --> 00:12:44,400
 So social distancing based on anger might easily be carried

179
00:12:44,400 --> 00:12:46,400
 out by someone who is racist.

180
00:12:46,400 --> 00:12:51,880
 You'll see people who don't want to associate it with

181
00:12:51,880 --> 00:12:55,680
 people of other races or skin colors or even

182
00:12:55,680 --> 00:12:59,560
 nationalities and languages and cultures, religions. Relig

183
00:12:59,560 --> 00:13:03,440
ions are a very common one based on anger

184
00:13:03,440 --> 00:13:07,760
 or hatred, ethnicity, hatred of other ethnicities.

185
00:13:07,760 --> 00:13:13,730
 And you see this even with the current pandemic you hear

186
00:13:13,730 --> 00:13:14,400
 about

187
00:13:14,400 --> 00:13:22,640
 people avoiding others and becoming very angry at people

188
00:13:22,640 --> 00:13:25,440
 based on their looks, their ethnicity,

189
00:13:25,440 --> 00:13:29,260
 Chinese people because there's an understanding that the

190
00:13:29,260 --> 00:13:31,120
 disease originated in China.

191
00:13:33,040 --> 00:13:36,910
 And so there's an increase in racism as I understand

192
00:13:36,910 --> 00:13:39,040
 relating to Chinese people.

193
00:13:39,040 --> 00:13:47,920
 But there are many opportunities for the arising of anger.

194
00:13:47,920 --> 00:13:49,440
 We get angry at people who

195
00:13:49,440 --> 00:13:54,070
 perhaps seem to have done something negligent. You get

196
00:13:54,070 --> 00:13:56,160
 angry at people who don't practice social

197
00:13:56,160 --> 00:14:04,070
 distancing. Or you just social distance yourself from

198
00:14:04,070 --> 00:14:09,440
 people with a cruel angry attitude.

199
00:14:09,440 --> 00:14:12,720
 And our attitude when we distance, our attitude when we do

200
00:14:12,720 --> 00:14:14,160
 anything is so important.

201
00:14:14,160 --> 00:14:17,990
 You might feel self-righteous when you do something

202
00:14:17,990 --> 00:14:19,280
 thinking it's the right thing to do.

203
00:14:19,280 --> 00:14:23,940
 But your way of doing it very well might be, but it becomes

204
00:14:23,940 --> 00:14:25,600
 quite unwholesome if it's done with

205
00:14:25,600 --> 00:14:30,060
 the wrong attitude. And so someone tries to shake our hands

206
00:14:30,060 --> 00:14:32,480
 and we shy away from them angrily and

207
00:14:32,480 --> 00:14:36,580
 say, what are you doing? We turn a cold shoulder, we act

208
00:14:36,580 --> 00:14:38,240
 cold towards others.

209
00:14:38,240 --> 00:14:44,960
 All of it boils down to a lack of mindfulness. If a person

210
00:14:44,960 --> 00:14:47,120
 has mindfulness, they're able to

211
00:14:47,120 --> 00:14:52,260
 interact with others in a way that is harmonious, in a way

212
00:14:52,260 --> 00:14:55,040
 that is free from any kind of cruelty,

213
00:14:55,040 --> 00:15:01,970
 any kind of harshness. And so it's a very good example of

214
00:15:01,970 --> 00:15:05,120
 how we really have a responsibility

215
00:15:05,120 --> 00:15:09,940
 to ourselves and to others for the purpose of bringing and

216
00:15:09,940 --> 00:15:12,480
 maintaining peace and happiness,

217
00:15:12,480 --> 00:15:17,360
 to act in a way that is free from qualities like anger.

218
00:15:20,720 --> 00:15:26,560
 And absolutely, of course, we can't be mean to...

219
00:15:26,560 --> 00:15:31,360
 Refugees is apparently a thing. We have to be conscientious

220
00:15:31,360 --> 00:15:31,360
.

221
00:15:31,360 --> 00:15:39,880
 There are many ways we can make it quite easy by closing

222
00:15:39,880 --> 00:15:42,080
 our borders to refugees and so on.

223
00:15:47,120 --> 00:15:51,280
 That end up being more harmful or...

224
00:15:51,280 --> 00:15:56,210
 Well, whether you can balance what is more harmful, what is

225
00:15:56,210 --> 00:15:58,080
 less harmful, but end up being quite cruel

226
00:15:58,080 --> 00:16:02,800
 and needlessly cruel. Because of course, the alternative is

227
00:16:02,800 --> 00:16:06,000
 more difficult.

228
00:16:06,000 --> 00:16:11,260
 If you let refugees in during this time, you have to... It

229
00:16:11,260 --> 00:16:14,640
 can be challenging to deal with them.

230
00:16:15,520 --> 00:16:18,090
 And rather than deal with the challenge, it's an example of

231
00:16:18,090 --> 00:16:19,440
 how we just act cruelly.

232
00:16:19,440 --> 00:16:25,390
 So with something like social distancing, there may be

233
00:16:25,390 --> 00:16:28,480
 times where compassion overrides, perhaps

234
00:16:28,480 --> 00:16:31,980
 someone has hurt themselves and you don't know who this

235
00:16:31,980 --> 00:16:33,520
 person is, you've never met them,

236
00:16:33,520 --> 00:16:35,930
 you see them on the street and they've hurt fallen down or

237
00:16:35,930 --> 00:16:36,480
 something.

238
00:16:36,480 --> 00:16:40,850
 And you go and... I'm not sure if you're even allowed to

239
00:16:40,850 --> 00:16:42,480
 pick people up, you don't know.

240
00:16:42,480 --> 00:16:46,070
 But anyway, cases where you might have to break your

241
00:16:46,070 --> 00:16:49,040
 quarantine, for example,

242
00:16:49,040 --> 00:16:53,280
 and then just wash your hands, instead of being, "Oh, I can

243
00:16:53,280 --> 00:16:55,120
't help this person because that would

244
00:16:55,120 --> 00:17:01,380
 endanger me with the sickness." In fact, in many cases, you

245
00:17:01,380 --> 00:17:02,560
 might think,

246
00:17:02,560 --> 00:17:07,090
 in a life or death situation, you might of course throw out

247
00:17:07,090 --> 00:17:10,240
 any kind of caution, any kind of sense

248
00:17:10,240 --> 00:17:15,070
 of something like social distancing and you have to be

249
00:17:15,070 --> 00:17:18,160
 thoughtful of what is a better thing to do.

250
00:17:18,160 --> 00:17:22,190
 And you see this, I think, with people, countries closing

251
00:17:22,190 --> 00:17:23,520
 their borders to refugees.

252
00:17:23,520 --> 00:17:28,640
 Of course, I'm not a politician, I don't know the details,

253
00:17:28,640 --> 00:17:28,880
 but

254
00:17:28,880 --> 00:17:31,890
 it's something that has to be done quite thoughtfully and

255
00:17:31,890 --> 00:17:32,960
 conscientiously

256
00:17:32,960 --> 00:17:38,880
 in terms of the negative impact, potential negative impact

257
00:17:38,880 --> 00:17:45,760
 and cruelty involved. The third reason why we do things is

258
00:17:45,760 --> 00:17:47,600
 through fear.

259
00:17:47,600 --> 00:17:52,800
 And I think this is the probably the most common emotion

260
00:17:52,800 --> 00:17:55,760
 among the negative reasons or

261
00:17:55,760 --> 00:18:03,120
 qualities of mind that we engage in when we distance

262
00:18:03,120 --> 00:18:04,640
 ourselves in these times.

263
00:18:05,760 --> 00:18:12,000
 A lot of people afraid doing it out of fear. And fear is a

264
00:18:12,000 --> 00:18:14,960
 great motivator.

265
00:18:14,960 --> 00:18:22,600
 We are quick to use fear as a means of motivating ourselves

266
00:18:22,600 --> 00:18:25,680
. We allow it many times and we allow it to

267
00:18:25,680 --> 00:18:32,800
 persist because it appears to motivate us and it does. I

268
00:18:32,800 --> 00:18:34,160
 mean, fear is stressful,

269
00:18:34,160 --> 00:18:39,740
 fear is an exciter and as a result it gets the mind working

270
00:18:39,740 --> 00:18:40,160
.

271
00:18:40,160 --> 00:18:45,440
 But of course, that's just a sign that before the fear our

272
00:18:45,440 --> 00:18:47,760
 mind wasn't working properly,

273
00:18:47,760 --> 00:18:51,920
 we weren't thoughtful. We become thoughtful about something

274
00:18:51,920 --> 00:18:52,880
 when it scares us.

275
00:18:52,880 --> 00:18:56,400
 How can we fix it? How can we deal with it?

276
00:19:00,080 --> 00:19:04,490
 But as you know with fear, it doesn't encourage us to think

277
00:19:04,490 --> 00:19:05,440
 rationally.

278
00:19:05,440 --> 00:19:10,840
 It doesn't always necessarily or really have the quality of

279
00:19:10,840 --> 00:19:12,880
 encouraging or promoting or

280
00:19:12,880 --> 00:19:19,350
 augmenting our wisdom, our mindfulness, our sense of

281
00:19:19,350 --> 00:19:21,440
 presence when we deal with the problem

282
00:19:21,440 --> 00:19:28,080
 and it becomes quite reactionary. And so we have to

283
00:19:28,080 --> 00:19:34,880
 sometimes question our level of

284
00:19:34,880 --> 00:19:39,590
 reactivity, our level of action in relation to things like

285
00:19:39,590 --> 00:19:43,120
 social distancing. A good example,

286
00:19:43,120 --> 00:19:50,550
 it works for all of these. For many people, I think social

287
00:19:50,550 --> 00:19:57,920
 distancing is not that troublesome

288
00:19:57,920 --> 00:20:03,370
 or burdensome. For many people it is, but for in some ways

289
00:20:03,370 --> 00:20:07,520
 it can seem quite pleasant. So people

290
00:20:07,520 --> 00:20:11,210
 engaging in it or agreeing to engage in it, you see the

291
00:20:11,210 --> 00:20:13,840
 world, it's quite quick to engage in

292
00:20:13,840 --> 00:20:17,680
 this social distancing because in some ways it subconscious

293
00:20:17,680 --> 00:20:19,040
ly is quite pleasant.

294
00:20:19,040 --> 00:20:26,780
 Or out of anger, we do things rashly. We engage in social

295
00:20:26,780 --> 00:20:29,520
 distancing, knee jerk reaction,

296
00:20:30,480 --> 00:20:35,920
 out of anger, the aversion towards sickness. I don't want

297
00:20:35,920 --> 00:20:41,120
 to get sick. So it can be irrational.

298
00:20:41,120 --> 00:20:43,620
 We haven't thought about it. We haven't thought about what

299
00:20:43,620 --> 00:20:45,360
 is the real benefit. And so our actions

300
00:20:45,360 --> 00:20:49,910
 are often irrational and fear is the same. We do things, a

301
00:20:49,910 --> 00:20:52,320
 lot of the fear going on has caused

302
00:20:52,320 --> 00:20:57,000
 people to do things that are in many ways unhelpful. And

303
00:20:57,000 --> 00:20:59,200
 sometimes it's unhelpful without realizing you

304
00:20:59,200 --> 00:21:01,550
 do things thinking they're going to be helpful like wear

305
00:21:01,550 --> 00:21:05,040
 masks or so on. And apparently, well,

306
00:21:05,040 --> 00:21:08,680
 I don't know. It's an example. There was some talk about

307
00:21:08,680 --> 00:21:10,800
 how the masks aren't helping, but

308
00:21:10,800 --> 00:21:14,830
 I don't know if they are. But cases like this, people

309
00:21:14,830 --> 00:21:17,600
 buying great amounts of toilet paper,

310
00:21:17,600 --> 00:21:22,950
 for example, was a big thing. You know, just mass hysteria

311
00:21:22,950 --> 00:21:25,840
 or mass panic. And sometimes panic

312
00:21:25,840 --> 00:21:29,680
 can help you. You know, you did something and it physically

313
00:21:29,680 --> 00:21:31,920
, maybe you did get all the toilet

314
00:21:31,920 --> 00:21:34,410
 paper in the store, but it can be quite unwholesome and

315
00:21:34,410 --> 00:21:35,760
 unpleasant because of course,

316
00:21:35,760 --> 00:21:39,490
 you other people then who need an ordinary amount of toilet

317
00:21:39,490 --> 00:21:40,640
 paper can get any.

318
00:21:40,640 --> 00:21:50,380
 And it causes us to act irrationally and without it causes

319
00:21:50,380 --> 00:21:52,800
 us to lose our regard for

320
00:21:54,560 --> 00:22:01,720
 propriety, rightness. Fear is a stimulator. And so it can

321
00:22:01,720 --> 00:22:04,560
 often appear to be helpful.

322
00:22:04,560 --> 00:22:13,760
 But we can see from the results of hysteria of fear that

323
00:22:16,000 --> 00:22:21,680
 that it in fact takes us away from a sense of calm, clear,

324
00:22:21,680 --> 00:22:26,080
 measured understanding of the situation.

325
00:22:26,080 --> 00:22:33,720
 The fourth reason why we do things, why we might do

326
00:22:33,720 --> 00:22:36,480
 something is out of delusion.

327
00:22:36,480 --> 00:22:42,290
 And so something like social distancing, as with anything

328
00:22:42,290 --> 00:22:43,520
 we have to consider,

329
00:22:43,520 --> 00:22:48,310
 we have to ask ourselves, is it proper to be doing and so

330
00:22:48,310 --> 00:22:48,880
 on.

331
00:22:48,880 --> 00:22:54,560
 People who engage out of delusion might be just be doing it

332
00:22:54,560 --> 00:22:56,000
 because other people have

333
00:22:56,000 --> 00:22:59,890
 told them to do it. You know, people engage in seclusion

334
00:22:59,890 --> 00:23:01,840
 out of arrogance and conceit,

335
00:23:01,840 --> 00:23:04,860
 thinking you're better than someone causes you to distance

336
00:23:04,860 --> 00:23:05,760
 yourself. But

337
00:23:05,760 --> 00:23:09,850
 with something like social distancing, in this case, the

338
00:23:09,850 --> 00:23:10,560
 pandemic,

339
00:23:10,560 --> 00:23:20,440
 a really good example is of how we engage in social dist

340
00:23:20,440 --> 00:23:22,960
ancing, because everyone else is doing

341
00:23:22,960 --> 00:23:26,640
 it because we have an idea that it's going to keep us safe

342
00:23:26,640 --> 00:23:30,400
 from sickness. But we do it haphazardly.

343
00:23:30,400 --> 00:23:35,260
 And we do it with a disconnect from reality. You know, we

344
00:23:35,260 --> 00:23:37,520
 often do this, the herd mentality,

345
00:23:37,520 --> 00:23:40,240
 everybody's doing it, okay, this will keep me safe. The

346
00:23:40,240 --> 00:23:41,920
 thing about wearing the masks and so on.

347
00:23:41,920 --> 00:23:45,280
 And then we engage in other activities, which of course are

348
00:23:45,280 --> 00:23:47,600
 which happen to be,

349
00:23:47,600 --> 00:23:52,310
 you know, nullifying any benefit that we might get from

350
00:23:52,310 --> 00:23:55,360
 social distancing. Like we still,

351
00:23:55,360 --> 00:23:58,860
 we don't shake people's hands, but we touch doorknobs in

352
00:23:58,860 --> 00:24:00,240
 public places.

353
00:24:00,240 --> 00:24:03,280
 And then we pick our nose or we scratch our face or so on.

354
00:24:06,800 --> 00:24:12,230
 And so it's an example, or it's an example that shows the

355
00:24:12,230 --> 00:24:14,640
 difference between being mindful

356
00:24:14,640 --> 00:24:20,610
 and being unmindful. The unmindful state is one that causes

357
00:24:20,610 --> 00:24:23,280
 us to engage in activities that

358
00:24:23,280 --> 00:24:28,080
 are potentially harmful to ourselves, simply because of the

359
00:24:28,080 --> 00:24:28,880
 darkness,

360
00:24:28,880 --> 00:24:31,520
 because of the lack of clarity, the lack of awareness.

361
00:24:33,920 --> 00:24:37,230
 With the mindful of delusion, we do many things that are

362
00:24:37,230 --> 00:24:39,600
 contrary to our intention.

363
00:24:39,600 --> 00:24:42,560
 So engage in social distancing, but

364
00:24:42,560 --> 00:24:52,300
 again, we take produce from the grocery store and without

365
00:24:52,300 --> 00:24:53,680
 washing it, we

366
00:24:53,680 --> 00:24:57,040
 eat it to put it in our mouths and so on.

367
00:24:59,840 --> 00:25:02,980
 And just doing things unmindfully, like picking your nose

368
00:25:02,980 --> 00:25:06,000
 after you've, whatever,

369
00:25:06,000 --> 00:25:15,880
 sucking on your thumb or something that causes us to negate

370
00:25:15,880 --> 00:25:18,640
 any benefit in the worldly sense

371
00:25:18,640 --> 00:25:21,520
 of something like social distancing.

372
00:25:23,600 --> 00:25:31,440
 So ultimately the right way or the right way to engage in

373
00:25:31,440 --> 00:25:33,440
 something like this, social distancing,

374
00:25:33,440 --> 00:25:37,920
 is of course with mindfulness, with wisdom.

375
00:25:37,920 --> 00:25:44,800
 And so it's important that we learn about why we're doing

376
00:25:44,800 --> 00:25:46,160
 everything that we do. When we do

377
00:25:46,160 --> 00:25:48,980
 something like social distancing, we learn about it, we

378
00:25:48,980 --> 00:25:52,080
 study it. It's a kind of wisdom that it

379
00:25:52,080 --> 00:25:59,920
 shows us a sort of mindfulness, our deliberate engagement

380
00:25:59,920 --> 00:26:03,120
 in understanding what it is that we're

381
00:26:03,120 --> 00:26:06,060
 doing so that we do it properly. The internet is a

382
00:26:06,060 --> 00:26:08,240
 wonderful thing now for information,

383
00:26:08,240 --> 00:26:12,560
 our capacity to learn about the proper ways to do things,

384
00:26:12,560 --> 00:26:14,560
 the benefits of certain things,

385
00:26:14,560 --> 00:26:17,120
 and how to make things beneficial in a worldly sense.

386
00:26:19,680 --> 00:26:25,020
 We have to think about how we're going to engage in this

387
00:26:25,020 --> 00:26:27,280
 sort of social distancing,

388
00:26:27,280 --> 00:26:31,040
 what we have to plan out, how we engage in the world and so

389
00:26:31,040 --> 00:26:33,760
 on. But most importantly,

390
00:26:33,760 --> 00:26:38,170
 we have to be mindful. Anything that could be quite radical

391
00:26:38,170 --> 00:26:40,560
 like this, where we have to remove

392
00:26:40,560 --> 00:26:44,070
 ourselves from our ordinary activity, where we have to

393
00:26:44,070 --> 00:26:47,360
 leave our social circle and we have to

394
00:26:48,560 --> 00:26:52,580
 remember not to shake people's hands, not to kiss people's

395
00:26:52,580 --> 00:26:53,360
 cheeks, not to

396
00:26:53,360 --> 00:26:57,940
 touch a door handle, for example, or if we've touched a

397
00:26:57,940 --> 00:27:00,320
 door handle to wash our hands before

398
00:27:00,320 --> 00:27:05,410
 we touch our face and so on. We have to remember all of

399
00:27:05,410 --> 00:27:08,560
 that, which means we have to be conscious,

400
00:27:08,560 --> 00:27:12,900
 we have to be present. It's very easy to have as a theory,

401
00:27:12,900 --> 00:27:14,400
 "I'm going to do this, I'm going to do

402
00:27:14,400 --> 00:27:16,530
 that, I'm not going to do this, I'm not going to do that

403
00:27:16,530 --> 00:27:18,400
 when the time comes." It all gets thrown

404
00:27:18,400 --> 00:27:22,660
 out of the window because you're just not mindful. And so

405
00:27:22,660 --> 00:27:32,240
 above most things, most especially,

406
00:27:32,240 --> 00:27:35,810
 when you engage in something like social distancing, and

407
00:27:35,810 --> 00:27:37,520
 Buddhists, we have a great

408
00:27:39,680 --> 00:27:43,300
 wealth of experience relating to things like social dist

409
00:27:43,300 --> 00:27:46,800
ancing. You absolutely need a proper

410
00:27:46,800 --> 00:27:50,830
 state of mind. You need to be present. It's an example of

411
00:27:50,830 --> 00:27:53,600
 an activity where mindfulness is crucial

412
00:27:53,600 --> 00:27:58,180
 because of course, when you're alone, you'll drive yourself

413
00:27:58,180 --> 00:28:00,160
 crazy if you're not mindful,

414
00:28:00,160 --> 00:28:03,170
 if you're not able to be present, or you'll be driven to

415
00:28:03,170 --> 00:28:05,840
 distraction. You'll just engage in

416
00:28:05,840 --> 00:28:11,500
 sensual indulgence and cultivate addiction and so on. Or

417
00:28:11,500 --> 00:28:17,520
 you'll fight with your family, you'll engage

418
00:28:17,520 --> 00:28:23,850
 in anger, you'll build up fear and hysteria and panic. You

419
00:28:23,850 --> 00:28:26,160
'll develop,

420
00:28:29,120 --> 00:28:39,840
 you'll cultivate delusion, your ego and conceit and so on,

421
00:28:39,840 --> 00:28:45,600
 based on your seclusion. Because

422
00:28:45,600 --> 00:28:50,380
 there's an inward focus, of course, when you distance

423
00:28:50,380 --> 00:28:54,000
 yourself from others. And so if your

424
00:28:54,000 --> 00:28:59,920
 mind is bent in the wrong way, the inward focus is going to

425
00:28:59,920 --> 00:29:02,640
 magnify that. And the effects are going

426
00:29:02,640 --> 00:29:06,550
 to be magnified. Because you're focused inward, any

427
00:29:06,550 --> 00:29:10,320
 negative, any of these four, the greed, the

428
00:29:10,320 --> 00:29:14,120
 anger, the fear, the delusion, any of these, when focused

429
00:29:14,120 --> 00:29:16,320
 inward, are going to make you sick.

430
00:29:18,160 --> 00:29:23,980
 And so if our true goal in this social distancing is to be

431
00:29:23,980 --> 00:29:26,080
 free from sickness,

432
00:29:26,080 --> 00:29:31,980
 well, absolutely all that always mental illness is going to

433
00:29:31,980 --> 00:29:35,360
 trump any kind of physical illness.

434
00:29:35,360 --> 00:29:42,200
 And so our concern over physical illness is important. Yes,

435
00:29:42,200 --> 00:29:43,280
 social distancing is

436
00:29:43,920 --> 00:29:50,600
 useful simply for the fact that it prevents and reduces the

437
00:29:50,600 --> 00:29:53,120
 likelihood of us getting sick or

438
00:29:53,120 --> 00:29:58,760
 the speed at which the pandemic spreads and so on. But if

439
00:29:58,760 --> 00:30:03,680
 it's engaged in such a way

440
00:30:03,680 --> 00:30:08,100
 that it increases our mental illness, then we haven't

441
00:30:08,100 --> 00:30:10,880
 accomplished anything at all.

442
00:30:13,600 --> 00:30:19,840
 So food for thought and a good reason for us to engage in

443
00:30:19,840 --> 00:30:22,400
 meditation at this time, absolutely

444
00:30:22,400 --> 00:30:27,840
 now that in this time of social distancing,

445
00:30:27,840 --> 00:30:35,200
 we have a unique opportunity for

446
00:30:40,240 --> 00:30:45,230
 for so much meditation practice that we should take the

447
00:30:45,230 --> 00:30:46,560
 opportunity.

448
00:30:46,560 --> 00:30:52,320
 We should take it as a real special opportunity for us to

449
00:30:52,320 --> 00:31:00,320
 try this practice of developing and working on our minds.

450
00:31:00,320 --> 00:31:06,080
 Of course, our center is shut down. So we aren't accepting

451
00:31:06,080 --> 00:31:09,120
 new meditators. And I assume most

452
00:31:09,120 --> 00:31:12,690
 meditation centers are as well. But there are many ways to

453
00:31:12,690 --> 00:31:15,760
 practice meditation, many resources for

454
00:31:15,760 --> 00:31:19,660
 you to practice meditation on your own, you can read books

455
00:31:19,660 --> 00:31:21,760
 on how to meditate, we have a booklet

456
00:31:21,760 --> 00:31:26,640
 on how to meditate. And we also have a stay at home course

457
00:31:26,640 --> 00:31:29,600
 that we all have had for a long time,

458
00:31:29,600 --> 00:31:33,560
 where people who practice at home, they do at least an hour

459
00:31:33,560 --> 00:31:36,720
 of meditation a day. And we meet,

460
00:31:37,920 --> 00:31:42,450
 we talk on on a voice call once a week, and give a new

461
00:31:42,450 --> 00:31:45,520
 exercise because of course, the booklet that

462
00:31:45,520 --> 00:31:48,900
 we have only gives the basic exercise. So every week we

463
00:31:48,900 --> 00:31:51,040
 give you a new exercise adding to the

464
00:31:51,040 --> 00:31:56,840
 challenge and pushing you further to train your mind to see

465
00:31:56,840 --> 00:32:00,160
 clearly to be present and to understand

466
00:32:00,160 --> 00:32:05,200
 yourself. So I encourage absolutely everyone during this

467
00:32:05,200 --> 00:32:08,400
 time, this is a prime opportunity to

468
00:32:08,400 --> 00:32:11,860
 practice meditation if you haven't, or to increase your

469
00:32:11,860 --> 00:32:14,240
 practice if you're already practicing.

470
00:32:14,240 --> 00:32:19,340
 And absolutely, if you're interested in taking a meditation

471
00:32:19,340 --> 00:32:22,560
 course at home, you can look on our

472
00:32:22,560 --> 00:32:28,040
 website siri-mongolo.org under the courses and you can read

473
00:32:28,040 --> 00:32:30,720
 about it. So those are my thoughts

474
00:32:30,720 --> 00:32:34,800
 on social distancing. Just thought I would do a video on it

475
00:32:34,800 --> 00:32:37,280
. I wish you all the best in this,

476
00:32:37,280 --> 00:32:44,010
 in these trying times. And I hope everyone is able to stay

477
00:32:44,010 --> 00:32:47,040
 calm and peaceful inside.

478
00:32:48,880 --> 00:32:52,170
 And that internally and externally we all find peace,

479
00:32:52,170 --> 00:32:54,880
 happiness, and freedom from suffering.

480
00:32:54,880 --> 00:33:00,640
 Thank you. I wish you all the best.

