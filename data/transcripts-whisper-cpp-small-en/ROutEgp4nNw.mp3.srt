1
00:00:00,000 --> 00:00:28,080
 Good morning.

2
00:00:28,080 --> 00:00:36,040
 The core of the Buddha's teaching is the Four Noble Truths.

3
00:00:36,040 --> 00:00:45,760
 Four truths about reality.

4
00:00:45,760 --> 00:00:46,760
 They're noble.

5
00:00:46,760 --> 00:00:50,640
 They are the truths of the noble ones.

6
00:00:50,640 --> 00:00:58,640
 They're the truths that make those people who understand

7
00:00:58,640 --> 00:01:01,120
 them noble because of their

8
00:01:01,120 --> 00:01:09,840
 understanding.

9
00:01:09,840 --> 00:01:19,250
 All four of the truths relate to the concept of dukkha or

10
00:01:19,250 --> 00:01:21,800
 suffering.

11
00:01:21,800 --> 00:01:24,040
 That's why they're noble.

12
00:01:24,040 --> 00:01:32,640
 Because they accomplish a noble goal.

13
00:01:32,640 --> 00:01:38,040
 The goal of freeing one from suffering.

14
00:01:38,040 --> 00:01:45,200
 They relate to what is truly important, valuable, precious.

15
00:01:45,200 --> 00:01:58,880
 And that is peace and happiness and freedom from suffering.

16
00:01:58,880 --> 00:02:04,390
 Suffering is a word that you can use to describe all the

17
00:02:04,390 --> 00:02:08,000
 problems that might ever arise.

18
00:02:08,000 --> 00:02:10,200
 Something's a problem because it relates to suffering.

19
00:02:10,200 --> 00:02:17,080
 If it didn't relate to suffering, it wouldn't really be a

20
00:02:17,080 --> 00:02:18,600
 problem.

21
00:02:18,600 --> 00:02:24,840
 So it's an important word.

22
00:02:24,840 --> 00:02:33,210
 It's the word that we need to pay attention to, keep our

23
00:02:33,210 --> 00:02:35,520
 attention on.

24
00:02:35,520 --> 00:02:49,400
 Buddha said, suffering is to be fully understood.

25
00:02:49,400 --> 00:02:58,400
 Parinyeya.

26
00:02:58,400 --> 00:03:06,440
 And that's just it, that suffering isn't fully understood.

27
00:03:06,440 --> 00:03:10,170
 It's not that we can't escape suffering, it's that we can't

28
00:03:10,170 --> 00:03:11,360
 understand it.

29
00:03:11,360 --> 00:03:16,000
 We don't understand it.

30
00:03:16,000 --> 00:03:24,040
 In fact, our understanding is often very much focused on

31
00:03:24,040 --> 00:03:29,680
 escape, avoidance, elimination.

32
00:03:29,680 --> 00:03:38,360
 How can we get rid of suffering?

33
00:03:38,360 --> 00:03:43,820
 Because our understanding of suffering is on a very

34
00:03:43,820 --> 00:03:47,440
 rudimentary level generally.

35
00:03:47,440 --> 00:03:53,080
 There are four ways of understanding suffering.

36
00:03:53,080 --> 00:03:59,440
 Mostly we tend to understand it in the first way.

37
00:03:59,440 --> 00:04:05,680
 First way of understanding is understanding suffering is as

38
00:04:05,680 --> 00:04:12,280
 what we call dukkha vedana.

39
00:04:12,280 --> 00:04:16,320
 Dukkha, painful or suffering.

40
00:04:16,320 --> 00:04:22,240
 Vedana means feeling.

41
00:04:22,240 --> 00:04:32,400
 It's a painful feeling, that suffering.

42
00:04:32,400 --> 00:04:35,160
 How do we get rid of it?

43
00:04:35,160 --> 00:04:38,340
 It sounds right, it sounds like how we should understand

44
00:04:38,340 --> 00:04:39,200
 suffering.

45
00:04:39,200 --> 00:04:43,810
 If we didn't have any painful mental or physical feelings,

46
00:04:43,810 --> 00:04:45,680
 that would be great.

47
00:04:45,680 --> 00:04:48,400
 Problem isn't that it's wrong.

48
00:04:48,400 --> 00:04:50,960
 Problem is that it's simplistic.

49
00:04:50,960 --> 00:04:59,650
 You belies a lack of understanding, a positive

50
00:04:59,650 --> 00:05:07,560
 understanding, a limited understanding.

51
00:05:07,560 --> 00:05:13,440
 Because if all you see is dukkha vedana, then all you'll do

52
00:05:13,440 --> 00:05:16,280
 is try to run away and escape

53
00:05:16,280 --> 00:05:19,280
 suffering, find ways to avoid it.

54
00:05:19,280 --> 00:05:30,560
 Of course the answer is simple, fix it.

55
00:05:30,560 --> 00:05:38,560
 Remove it, avoid it, escape it.

56
00:05:38,560 --> 00:05:41,880
 And so we spend all our time trying to avoid suffering.

57
00:05:41,880 --> 00:05:44,440
 We build up habits of avoidance.

58
00:05:44,440 --> 00:05:49,980
 We build up defense mechanisms and structures that keep us

59
00:05:49,980 --> 00:05:52,840
 protected from suffering.

60
00:05:52,840 --> 00:06:01,410
 Spend a lot of our lives, a lot of our time afraid of

61
00:06:01,410 --> 00:06:03,960
 suffering.

62
00:06:03,960 --> 00:06:12,530
 And if we're lucky or good or have good karma, we can for

63
00:06:12,530 --> 00:06:17,400
 some time avoid suffering.

64
00:06:17,400 --> 00:06:21,040
 It appears that that's a solution.

65
00:06:21,040 --> 00:06:27,040
 Find a way to live that allows you to avoid the problem.

66
00:06:27,040 --> 00:06:32,740
 But as Buddhist meditators we know this isn't sustainable.

67
00:06:32,740 --> 00:06:37,550
 The second way to understand suffering is called dukkha sab

68
00:06:37,550 --> 00:06:38,280
hawa.

69
00:06:38,280 --> 00:06:47,310
 Sabhawa means something that has existence, existential

70
00:06:47,310 --> 00:06:50,480
 phenomenon, a thing that really

71
00:06:50,480 --> 00:06:51,480
 exists.

72
00:06:51,480 --> 00:06:59,390
 But here the meaning is that suffering is in an inescapable

73
00:06:59,390 --> 00:07:00,800
 reality.

74
00:07:00,800 --> 00:07:02,480
 It's there, it exists.

75
00:07:02,480 --> 00:07:07,700
 It is no matter how you try to run from it, no matter how

76
00:07:07,700 --> 00:07:10,240
 much you try to make it so it

77
00:07:10,240 --> 00:07:12,480
 isn't.

78
00:07:12,480 --> 00:07:20,320
 It is.

79
00:07:20,320 --> 00:07:27,280
 Try to avoid pain and suffering.

80
00:07:27,280 --> 00:07:30,720
 We still get sick.

81
00:07:30,720 --> 00:07:33,560
 We still get old.

82
00:07:33,560 --> 00:07:36,000
 We still die.

83
00:07:36,000 --> 00:07:39,840
 We still get hungry and thirsty.

84
00:07:39,840 --> 00:07:50,640
 We still get afraid and disappointed.

85
00:07:50,640 --> 00:07:59,790
 Avoiding suffering is not only unsustainable, unpredictable

86
00:07:59,790 --> 00:07:59,880
.

87
00:07:59,880 --> 00:08:09,080
 It's also incomplete and unsuccessful.

88
00:08:09,080 --> 00:08:17,430
 We don't really realize how much we suffer, how much we

89
00:08:17,430 --> 00:08:23,640
 limit our state of peace and happiness,

90
00:08:23,640 --> 00:08:30,440
 how limited is our peace of mind.

91
00:08:30,440 --> 00:08:37,510
 We don't realize until we take up the practice of observing

92
00:08:37,510 --> 00:08:42,720
, developing our minds, observing

93
00:08:42,720 --> 00:08:50,720
 reality and developing wisdom.

94
00:08:50,720 --> 00:09:01,290
 When we do that, then we become to see suffering more

95
00:09:01,290 --> 00:09:02,880
 clearly.

96
00:09:02,880 --> 00:09:08,080
 We come to see that this is a part of life.

97
00:09:08,080 --> 00:09:11,280
 It's unavoidable.

98
00:09:11,280 --> 00:09:16,260
 This is actually the reason, tukka sabhava is the main

99
00:09:16,260 --> 00:09:22,000
 reason why people come to practice

100
00:09:22,000 --> 00:09:28,840
 meditation, religion, spirituality.

101
00:09:28,840 --> 00:09:32,090
 They can't figure out a way to free themselves from

102
00:09:32,090 --> 00:09:33,120
 suffering.

103
00:09:33,120 --> 00:09:36,840
 So they turn to more spiritual practices.

104
00:09:36,840 --> 00:09:42,560
 Even spiritual practices designed to facilitate avoidance.

105
00:09:42,560 --> 00:09:45,530
 Many meditation practices are just to get away from the

106
00:09:45,530 --> 00:09:46,360
 suffering.

107
00:09:46,360 --> 00:09:51,510
 So still in the frame of mind that it's just dukkavedana,

108
00:09:51,510 --> 00:09:54,280
 something you can avoid if you

109
00:09:54,280 --> 00:10:00,920
 figure out how.

110
00:10:00,920 --> 00:10:05,240
 But when people become desperate, suffering is so great

111
00:10:05,240 --> 00:10:07,520
 they realize they can't avoid

112
00:10:07,520 --> 00:10:14,040
 it or else they have a keen insight and understanding.

113
00:10:14,040 --> 00:10:23,760
 If someone has some kind of high-mindedness to understand,

114
00:10:23,760 --> 00:10:26,440
 this isn't the right way.

115
00:10:26,440 --> 00:10:28,960
 Avoiding isn't the right way.

116
00:10:28,960 --> 00:10:30,760
 Running away from suffering isn't the right way.

117
00:10:30,760 --> 00:10:31,760
 It's not sustainable.

118
00:10:31,760 --> 00:10:36,490
 They re-if they get this sense, either from experience or

119
00:10:36,490 --> 00:10:38,440
 from introspection.

120
00:10:38,440 --> 00:10:42,880
 It's a major reason why someone might look for a better way

121
00:10:42,880 --> 00:10:45,480
 rather than avoid suffering.

122
00:10:45,480 --> 00:10:47,280
 How can I understand it?

123
00:10:47,280 --> 00:10:51,360
 How can I free myself from its power?

124
00:10:51,360 --> 00:10:57,780
 A big part of the Buddhist path, the path of purification,

125
00:10:57,780 --> 00:11:00,600
 is this shift from trying

126
00:11:00,600 --> 00:11:08,030
 to run away from suffering, to letting go of our concern

127
00:11:08,030 --> 00:11:10,840
 about suffering.

128
00:11:10,840 --> 00:11:14,030
 Letting go of our concern about whether we feel pain or don

129
00:11:14,030 --> 00:11:14,960
't feel pain.

130
00:11:14,960 --> 00:11:19,440
 Whether things are the way we want or not the way we want.

131
00:11:19,440 --> 00:11:25,360
 Rather than trying to always get what we want, the shift to

132
00:11:25,360 --> 00:11:28,000
 letting go of wanting.

133
00:11:28,000 --> 00:11:34,280
 Placing the blame back on our desire.

134
00:11:34,280 --> 00:11:38,600
 And a big part of the fruit of the practice is the

135
00:11:38,600 --> 00:11:42,720
 realization of true happiness not through

136
00:11:42,720 --> 00:11:48,660
 escaping suffering, but by changing the way we look at it

137
00:11:48,660 --> 00:11:52,080
 and realizing true peace beyond

138
00:11:52,080 --> 00:11:58,720
 what we ever thought possible.

139
00:11:58,720 --> 00:12:05,330
 Irrespective of our experiences, regardless of whether we

140
00:12:05,330 --> 00:12:08,480
 feel pain or have unpleasant

141
00:12:08,480 --> 00:12:16,160
 thoughts or memories.

142
00:12:16,160 --> 00:12:21,320
 This is the third type of understanding.

143
00:12:21,320 --> 00:12:24,320
 Dukkalakana.

144
00:12:24,320 --> 00:12:30,580
 Once we begin to practice spirituality, we've come to see

145
00:12:30,580 --> 00:12:33,880
 that there must be a better way.

146
00:12:33,880 --> 00:12:38,480
 And we come to see that it has nothing to do with what we

147
00:12:38,480 --> 00:12:39,880
 experience.

148
00:12:39,880 --> 00:12:43,480
 Having realized that suffering is an unavoidable part of

149
00:12:43,480 --> 00:12:45,880
 experience, we come to realize that

150
00:12:45,880 --> 00:12:54,560
 it really isn't about whether I experience this or that at

151
00:12:54,560 --> 00:12:55,720
 all.

152
00:12:55,720 --> 00:13:00,600
 Suffering is simply a part of our tendency to cling to

153
00:13:00,600 --> 00:13:01,680
 things.

154
00:13:01,680 --> 00:13:08,050
 Our tendency to misunderstand where and how happiness is

155
00:13:08,050 --> 00:13:09,160
 found.

156
00:13:09,160 --> 00:13:14,600
 Peace is found.

157
00:13:14,600 --> 00:13:18,690
 So we realize we're just biased towards this or that

158
00:13:18,690 --> 00:13:23,400
 experience when in fact that bias

159
00:13:23,400 --> 00:13:25,960
 is the problem.

160
00:13:25,960 --> 00:13:30,260
 Not that we don't get what we want, but that we want and

161
00:13:30,260 --> 00:13:32,680
 like and dislike in the first

162
00:13:32,680 --> 00:13:34,680
 place.

163
00:13:34,680 --> 00:13:40,300
 Dukkalakana is the understanding that comes only through

164
00:13:40,300 --> 00:13:43,480
 spirituality that suffering is

165
00:13:43,480 --> 00:13:50,960
 a characteristic of all things.

166
00:13:50,960 --> 00:13:55,020
 In the sense that it doesn't matter what you're referring

167
00:13:55,020 --> 00:13:57,240
 to, all phenomenon can cause us

168
00:13:57,240 --> 00:14:01,740
 pain and suffering.

169
00:14:01,740 --> 00:14:05,160
 Not because of their nature exactly, but because of our

170
00:14:05,160 --> 00:14:07,480
 misunderstanding of their nature, that

171
00:14:07,480 --> 00:14:11,230
 it's worth clinging to, that some benefit comes from

172
00:14:11,230 --> 00:14:16,480
 clinging, from craving, from bias.

173
00:14:16,480 --> 00:14:20,720
 Something wrong really with reality.

174
00:14:20,720 --> 00:14:25,640
 What's wrong is how we understand it, misunderstand it.

175
00:14:25,640 --> 00:14:32,680
 The idea that we can gain something, the idea that somehow

176
00:14:32,680 --> 00:14:37,920
 it's going to satisfy us.

177
00:14:37,920 --> 00:14:45,030
 Dukkalakana, when the meditator realizes that it's a

178
00:14:45,030 --> 00:14:49,360
 characteristic of all things, their

179
00:14:49,360 --> 00:14:53,680
 perception of things shifts and they begin to let go.

180
00:14:53,680 --> 00:15:00,570
 They begin to fly, to free themselves from the clinging to

181
00:15:00,570 --> 00:15:04,000
 this or that experience.

182
00:15:04,000 --> 00:15:14,520
 And bouncing around, pushing, pulling.

183
00:15:14,520 --> 00:15:23,940
 It's the third type of understanding that we get through

184
00:15:23,940 --> 00:15:28,600
 meditation that causes us to

185
00:15:28,600 --> 00:15:29,600
 let go.

186
00:15:29,600 --> 00:15:34,560
 The fourth is when the mind lets go.

187
00:15:34,560 --> 00:15:40,020
 The fourth understanding is called dukkasetja, the truth of

188
00:15:40,020 --> 00:15:41,360
 suffering.

189
00:15:41,360 --> 00:15:46,790
 The truth of suffering refers to the realization that

190
00:15:46,790 --> 00:15:49,800
 nothing is worth clinging to.

191
00:15:49,800 --> 00:16:00,320
 Dukkalakana is all of experience.

192
00:16:00,320 --> 00:16:03,260
 It's not one experience or another, it's our relationship

193
00:16:03,260 --> 00:16:07,080
 to experience that causes suffering.

194
00:16:07,080 --> 00:16:10,560
 And when you see that clearly enough, there is one moment,

195
00:16:10,560 --> 00:16:12,760
 dukkasetja, the truth of suffering

196
00:16:12,760 --> 00:16:15,400
 when it hits you.

197
00:16:15,400 --> 00:16:20,340
 It's the culmination of this practice of creating

198
00:16:20,340 --> 00:16:24,200
 understanding and clarity of mind.

199
00:16:24,200 --> 00:16:27,170
 When one realizes just in a moment that nothing's worth

200
00:16:27,170 --> 00:16:29,480
 clinging to, it's the natural evolution

201
00:16:29,480 --> 00:16:36,970
 of the natural consequence of observing how clinging causes

202
00:16:36,970 --> 00:16:38,800
 suffering.

203
00:16:38,800 --> 00:16:42,520
 That there comes a moment where the mind is free of any

204
00:16:42,520 --> 00:16:44,480
 kind of craving or clinging to

205
00:16:44,480 --> 00:16:52,760
 anything, realizing that craving is the cause of suffering,

206
00:16:52,760 --> 00:16:55,760
 nothing is worth clinging to.

207
00:16:55,760 --> 00:16:58,400
 Clinging is the problem.

208
00:16:58,400 --> 00:16:59,400
 And the mind lets go.

209
00:16:59,400 --> 00:17:06,750
 All it takes is that one moment and that, that is the truth

210
00:17:06,750 --> 00:17:09,000
 of suffering.

211
00:17:09,000 --> 00:17:20,600
 It's the path that leads to freedom from suffering.

212
00:17:20,600 --> 00:17:30,920
 And it's the moment when one becomes an enlightened being,

213
00:17:30,920 --> 00:17:33,880
 a noble being.

214
00:17:33,880 --> 00:17:42,040
 That is the essence of the Buddhist teaching, the basis of

215
00:17:42,040 --> 00:17:45,840
 the Four Noble Truths.

216
00:17:45,840 --> 00:17:51,040
 So four types of suffering, something for us to, any medit

217
00:17:51,040 --> 00:17:53,560
ator should be aware of this

218
00:17:53,560 --> 00:17:59,700
 distinction, getting a sense of what we mean when we talk

219
00:17:59,700 --> 00:18:03,040
 about suffering and where we're

220
00:18:03,040 --> 00:18:12,280
 headed, what is the goal, what is the path.

221
00:18:12,280 --> 00:18:16,920
 Also a useful teaching for non-meditators to understand

222
00:18:16,920 --> 00:18:19,640
 that the way we look at suffering

223
00:18:19,640 --> 00:18:26,160
 is often simplistic, ineffective, and a big part of the

224
00:18:26,160 --> 00:18:27,560
 problem.

225
00:18:27,560 --> 00:18:32,420
 Not that we can't free ourselves from suffering, but that

226
00:18:32,420 --> 00:18:35,160
 in trying to free ourselves from

227
00:18:35,160 --> 00:18:38,720
 suffering we only cause more problems for ourselves.

228
00:18:38,720 --> 00:18:42,000
 So that's the demo for this morning.

229
00:18:42,000 --> 00:18:42,680
 Thank you.

230
00:18:42,680 --> 00:18:43,000
 [

231
00:18:43,000 --> 00:18:49,120
 End of Audio ]

232
00:18:49,120 --> 00:18:51,120
 [ Silence ]

