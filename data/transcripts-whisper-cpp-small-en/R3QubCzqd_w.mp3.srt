1
00:00:00,000 --> 00:00:03,290
 Is there any way for us to better understand how good acts

2
00:00:03,290 --> 00:00:04,000
 bring good results?

3
00:00:04,000 --> 00:00:06,720
 It seems sometimes that people who do good don't reap a

4
00:00:06,720 --> 00:00:08,000
 noticeable benefit.

5
00:00:08,000 --> 00:00:11,000
 Is it subtle, immaterial, etc.?

6
00:00:11,000 --> 00:00:14,270
 The only way to understand karma is through the practice of

7
00:00:14,270 --> 00:00:15,000
 meditation.

8
00:00:15,000 --> 00:00:17,540
 The only way to appreciate it properly is through the

9
00:00:17,540 --> 00:00:19,000
 practice of meditation.

10
00:00:19,000 --> 00:00:23,200
 It's not some magical thing whereby you kill someone and

11
00:00:23,200 --> 00:00:24,000
 you get killed,

12
00:00:24,000 --> 00:00:28,000
 or you give someone food and you get food, or so on.

13
00:00:28,000 --> 00:00:31,000
 I've talked about this before.

14
00:00:31,000 --> 00:00:34,000
 The Buddha rejected the concept of karma.

15
00:00:34,000 --> 00:00:37,000
 We have this idea that the Buddha taught the law of karma.

16
00:00:37,000 --> 00:00:40,000
 The Buddha himself even said he taught the law of karma.

17
00:00:40,000 --> 00:00:43,600
 But if you look at what really went on, what the Buddha

18
00:00:43,600 --> 00:00:46,000
 really did is he rejected karma.

19
00:00:46,000 --> 00:00:51,000
 Totally, completely rejected karma.

20
00:00:51,000 --> 00:00:55,000
 It's not fair to say that because he redefined it.

21
00:00:55,000 --> 00:00:59,440
 But what I mean by that is Hinduism had this concept that

22
00:00:59,440 --> 00:01:06,000
 an act was in and of itself good or bad.

23
00:01:06,000 --> 00:01:11,000
 It still does.

24
00:01:11,000 --> 00:01:18,000
 I believe Hinduism, Brahmanism, the Vedas have this idea

25
00:01:18,000 --> 00:01:24,000
 that ritual activity brings benefit.

26
00:01:24,000 --> 00:01:28,560
 There were even the beliefs that ritual activity was to be

27
00:01:28,560 --> 00:01:31,000
 done without the idea that it brings benefit.

28
00:01:31,000 --> 00:01:35,000
 But not doing it would be unthought of.

29
00:01:35,000 --> 00:01:37,820
 There was no idea that there's a benefit that comes from it

30
00:01:37,820 --> 00:01:38,000
.

31
00:01:38,000 --> 00:01:40,000
 It's just it is done.

32
00:01:40,000 --> 00:01:42,000
 You do the rituals.

33
00:01:42,000 --> 00:01:46,000
 The question of why you do them, there's no why.

34
00:01:46,000 --> 00:01:48,000
 It's just done.

35
00:01:48,000 --> 00:01:49,000
 This was karma.

36
00:01:49,000 --> 00:01:52,000
 Karma was things to be done.

37
00:01:52,000 --> 00:01:54,000
 Things that were bad.

38
00:01:54,000 --> 00:02:00,000
 Killing someone was bad because of the physical act.

39
00:02:00,000 --> 00:02:03,000
 Eating certain foods is bad karma.

40
00:02:03,000 --> 00:02:07,040
 Eating meat, for example, this idea that eating meat is bad

41
00:02:07,040 --> 00:02:08,000
 karma.

42
00:02:08,000 --> 00:02:10,000
 The Buddha rejected all this.

43
00:02:10,000 --> 00:02:16,000
 He said, "Jaitana hang bhikkave kamang vadami."

44
00:02:16,000 --> 00:02:19,000
 Something like that.

45
00:02:19,000 --> 00:02:26,610
 He said, "What really goes on, what is really done, is not

46
00:02:26,610 --> 00:02:28,000
 a bodily action.

47
00:02:28,000 --> 00:02:31,640
 What is really done, the act that is done, is the intention

48
00:02:31,640 --> 00:02:32,000
."

49
00:02:32,000 --> 00:02:34,000
 He didn't reject it, as I said.

50
00:02:34,000 --> 00:02:37,000
 He rejected what we would normally think of as karma.

51
00:02:37,000 --> 00:02:40,920
 He said, "What is really done, the act that is really done,

52
00:02:40,920 --> 00:02:42,000
 is a mental one.

53
00:02:42,000 --> 00:02:47,000
 That's where karma exists."

54
00:02:47,000 --> 00:02:53,990
 When we talk about the law of karma in Buddhism, we're not

55
00:02:53,990 --> 00:02:57,000
 talking about acts of body.

56
00:02:57,000 --> 00:02:59,390
 We're not talking about people doing good things and

57
00:02:59,390 --> 00:03:02,000
 getting a good thing come to them.

58
00:03:02,000 --> 00:03:07,500
 We're talking about the changes that occur in one's mind

59
00:03:07,500 --> 00:03:11,000
 based on one's intentions.

60
00:03:11,000 --> 00:03:16,710
 When one gives rise to an unwholesome intention, when a

61
00:03:16,710 --> 00:03:20,200
 mind state that is associated with greed or anger or

62
00:03:20,200 --> 00:03:21,000
 delusion,

63
00:03:21,000 --> 00:03:25,000
 then it will give rise to suffering.

64
00:03:25,000 --> 00:03:28,940
 If one gives rise to a state of mind that is based on non-g

65
00:03:28,940 --> 00:03:31,000
reed, non-anger, non-delusion,

66
00:03:31,000 --> 00:03:37,220
 wisdom, renunciation, kindness, or love, then it brings

67
00:03:37,220 --> 00:03:40,000
 happiness, it brings peace.

68
00:03:40,000 --> 00:03:43,080
 The only way you can verify this is through looking at

69
00:03:43,080 --> 00:03:45,000
 these things, through watching them.

70
00:03:45,000 --> 00:03:47,000
 It's very quick, actually.

71
00:03:47,000 --> 00:03:51,770
 If you take a meditation course for three days or five days

72
00:03:51,770 --> 00:03:54,000
, I would say even three days,

73
00:03:54,000 --> 00:03:56,690
 you do three days of intensive meditation, you'll begin to

74
00:03:56,690 --> 00:03:58,000
 see the workings of karma.

75
00:03:58,000 --> 00:04:04,210
 You can enter into, if you're with proper guidance, in

76
00:04:04,210 --> 00:04:06,000
 about three days of insight meditation,

77
00:04:06,000 --> 00:04:12,000
 you can understand what we call bhajiya bharigahanyana,

78
00:04:12,000 --> 00:04:19,210
 the ability to see the causes and effects, knowledge of

79
00:04:19,210 --> 00:04:22,000
 cause and effect, which is karma,

80
00:04:22,000 --> 00:04:25,400
 the ability to see that when the mind gives rise to this

81
00:04:25,400 --> 00:04:29,000
 state, this state has to follow.

82
00:04:29,000 --> 00:04:31,570
 Once you start to look at greed, look at anger, look at

83
00:04:31,570 --> 00:04:32,000
 delusion,

84
00:04:32,000 --> 00:04:36,000
 you can't look at delusion, but looking at greed and anger,

85
00:04:36,000 --> 00:04:42,000
 you will come to see the suffering associated with them,

86
00:04:42,000 --> 00:04:43,770
 that as soon as the greed arises, there's tension in the

87
00:04:43,770 --> 00:04:47,000
 mind, anger arises, there's suffering in the mind.

88
00:04:47,000 --> 00:04:50,000
 These are associated states.

89
00:04:50,000 --> 00:04:54,380
 You see how it changes the mind, how it dulls the mind, how

90
00:04:54,380 --> 00:04:58,000
 it reduces the clarity of the mind.

91
00:04:58,000 --> 00:05:09,140
 As a result, not only the mind, but it disrupts reality, so

92
00:05:09,140 --> 00:05:11,000
 the whole world around you is affected.

93
00:05:11,000 --> 00:05:14,000
 As you get more angry, you tend to act, and for example,

94
00:05:14,000 --> 00:05:18,000
 people who are angry tend to be less careful.

95
00:05:18,000 --> 00:05:24,220
 So people who strip and fall or hit their heads or cut

96
00:05:24,220 --> 00:05:28,390
 themselves, it can often be because of the karma of

97
00:05:28,390 --> 00:05:30,000
 cultivating anger in the past.

98
00:05:30,000 --> 00:05:33,990
 So we say hurting yourself is caused by karma, but on a

99
00:05:33,990 --> 00:05:39,000
 basic level, it's just because you're less careful.

100
00:05:39,000 --> 00:05:47,000
 A person who is greedy will be less indulgent.

101
00:05:47,000 --> 00:05:53,080
 So a person who is stingy, for example, people who can't

102
00:05:53,080 --> 00:05:57,550
 give up and can't let go, who try to steal things from

103
00:05:57,550 --> 00:06:02,000
 others or take things from others,

104
00:06:02,000 --> 00:06:09,040
 will as a result be constantly unhappy, constantly

105
00:06:09,040 --> 00:06:11,000
 dissatisfied.

106
00:06:11,000 --> 00:06:18,180
 And as a result, you will see that we'll eventually become

107
00:06:18,180 --> 00:06:23,000
 less accepting of peace and happiness,

108
00:06:23,000 --> 00:06:31,380
 and we'll engage in stressful activities. People who are

109
00:06:31,380 --> 00:06:40,000
 stingy will be less likely to find happiness themselves.

110
00:06:40,000 --> 00:06:42,960
 In generalization, there are many different types of people

111
00:06:42,960 --> 00:06:48,000
, but they will be, as a result of their state of mind,

112
00:06:48,000 --> 00:06:51,860
 people have less peace, obviously, because of their, in a

113
00:06:51,860 --> 00:06:55,710
 general sense, the desire in the mind will make them less

114
00:06:55,710 --> 00:06:56,000
 peaceful,

115
00:06:56,000 --> 00:06:58,000
 because they'll always be thinking about this.

116
00:06:58,000 --> 00:07:01,300
 As soon as they sit down, their mind will jump away to the

117
00:07:01,300 --> 00:07:03,000
 object of their desire.

118
00:07:03,000 --> 00:07:08,250
 If it's another human being that you're in love with, you

119
00:07:08,250 --> 00:07:11,000
 say, you obviously can't meditate.

120
00:07:11,000 --> 00:07:13,000
 You're always thinking about this person.

121
00:07:13,000 --> 00:07:17,050
 For example, if it's food, then you're always thinking

122
00:07:17,050 --> 00:07:18,000
 about the food.

123
00:07:18,000 --> 00:07:21,000
 If it's computer games, then you're always thinking about

124
00:07:21,000 --> 00:07:22,000
 computer games.

125
00:07:22,000 --> 00:07:25,090
 I remember I used to play computer games, and I tried med

126
00:07:25,090 --> 00:07:27,000
itating when I was younger,

127
00:07:27,000 --> 00:07:30,330
 but I realized I would have gotten to such a point where I

128
00:07:30,330 --> 00:07:32,000
 couldn't even sit alone in the room,

129
00:07:32,000 --> 00:07:35,020
 because I would be constantly thinking about the computer,

130
00:07:35,020 --> 00:07:39,000
 going to the computer, playing computer games.

131
00:07:39,000 --> 00:07:44,270
 Anyway, so meditate. Best way to understand karma, how good

132
00:07:44,270 --> 00:07:47,000
 acts bring results, is to practice meditation.

