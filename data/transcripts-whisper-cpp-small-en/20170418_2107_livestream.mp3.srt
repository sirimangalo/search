1
00:00:00,000 --> 00:00:04,000
 livestream isn't working, audio isn't working

2
00:00:04,000 --> 00:00:10,000
 oh now it's working, no that's up as well

3
00:00:10,000 --> 00:00:17,000
 I don't know if I'm gonna be, what day is it today?

4
00:00:17,000 --> 00:00:20,000
 it's 18th

5
00:00:20,000 --> 00:00:23,000
 I'm gonna be away for the first half of May

6
00:00:23,000 --> 00:00:26,000
 so

7
00:00:27,000 --> 00:00:30,000
 thinking maybe I'll just take a break for a while

8
00:00:30,000 --> 00:00:33,000
 there's no more meditators here, everyone's gone home

9
00:00:33,000 --> 00:00:36,000
 so

10
00:00:36,000 --> 00:00:42,000
 I don't know, does anyone have any questions?

11
00:00:56,000 --> 00:01:00,550
 yeah, I think I'll take a little while off, school's

12
00:01:00,550 --> 00:01:01,000
 finished

13
00:01:01,000 --> 00:01:04,000
 I think I've had enough of school for a while

14
00:01:04,000 --> 00:01:08,000
 and

15
00:01:08,000 --> 00:01:11,000
 gonna go visit my parents for a couple weeks, my father

16
00:01:11,000 --> 00:01:16,310
 I'm gonna go visit Manatulan Island, where I was born,

17
00:01:16,310 --> 00:01:17,000
 hopefully

18
00:01:17,000 --> 00:01:23,000
 and then mid-May we got meditators coming again

19
00:01:23,000 --> 00:01:26,000
 and we'll get back into broadcasting as well

20
00:01:26,000 --> 00:01:35,000
 I'm just studying a few classes, courses at McMaster

21
00:01:35,000 --> 00:01:38,000
 I thought that going to the university would help me

22
00:01:38,000 --> 00:01:42,980
 find a community and find people interested in meditation

23
00:01:42,980 --> 00:01:43,000
 but

24
00:01:43,000 --> 00:01:47,000
 it wasn't as successful as I'd hoped

25
00:01:47,000 --> 00:01:52,000
 so, and the idea of studying some things, I mean

26
00:01:52,000 --> 00:01:54,600
 the only really useful thing that came out of it that I can

27
00:01:54,600 --> 00:01:56,000
 think of is French and

28
00:01:56,000 --> 00:01:59,000
 Latin, I learned a couple languages

29
00:01:59,000 --> 00:02:01,610
 French especially, I suppose, I don't know what Latin's

30
00:02:01,610 --> 00:02:03,000
 gonna do for me but

31
00:02:03,000 --> 00:02:05,000
 languages are always useful

32
00:02:05,000 --> 00:02:20,000
 I think let's leave the questions on the site until

33
00:02:20,000 --> 00:02:23,000
 I can do a YouTube video about them

34
00:02:23,000 --> 00:02:38,410
 just tell it to stop, when you're swaying back and forth

35
00:02:38,410 --> 00:02:39,000
 just say stop

36
00:02:39,000 --> 00:02:43,000
 make it stop, there's what's called nikanti

37
00:02:43,000 --> 00:02:46,000
 this subtle enjoyment of it

38
00:02:46,000 --> 00:02:48,800
 the reinforcement of it, it's more comfortable to be sway

39
00:02:48,800 --> 00:02:49,000
ing

40
00:02:49,000 --> 00:02:53,000
 to have to sit through the pain and the mental stress

41
00:02:53,000 --> 00:02:56,470
 it's quite comfortable to sway back and forth so you have

42
00:02:56,470 --> 00:02:57,000
 to tell it to stop and

43
00:02:57,000 --> 00:03:00,000
 grow up

44
00:03:00,000 --> 00:03:05,000
 and face the ordinary experiences

45
00:03:15,000 --> 00:03:19,000
 meaning you have to make a determination to stop it

46
00:03:19,000 --> 00:03:25,000
 so that you don't subconsciously encourage it

47
00:03:26,000 --> 00:03:28,000
 encourage it

48
00:03:28,000 --> 00:03:34,000
 there

