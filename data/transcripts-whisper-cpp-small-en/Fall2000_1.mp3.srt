1
00:00:00,000 --> 00:00:14,690
 Today I will explain to you the four foundations of

2
00:00:14,690 --> 00:00:19,400
 mindfulness in brief.

3
00:00:19,400 --> 00:00:25,750
 You have been practicing foundations of mindfulness for a

4
00:00:25,750 --> 00:00:28,640
 long time and so you need to know the

5
00:00:28,640 --> 00:00:32,830
 four foundations. In order for you to remember the four

6
00:00:32,830 --> 00:00:38,000
 foundations, I have you recite this

7
00:00:38,000 --> 00:00:44,900
 passage every morning after taking that precept. I have

8
00:00:44,900 --> 00:00:50,200
 explained this passage here many times and

9
00:00:50,200 --> 00:00:59,700
 so for many people it may be what they already know. But

10
00:00:59,700 --> 00:01:05,320
 there are new people also and so it

11
00:01:05,320 --> 00:01:13,140
 will be good to give the explanation of this first passage

12
00:01:13,140 --> 00:01:18,040
 from the discourse called the

13
00:01:18,040 --> 00:01:22,990
 great discourse on the foundations of mindfulness. This

14
00:01:22,990 --> 00:01:34,400
 passage in the discourse explains the four

15
00:01:34,400 --> 00:01:42,220
 foundations of mindfulness in brief. The word translated as

16
00:01:42,220 --> 00:01:46,000
 foundation of mindfulness is

17
00:01:46,000 --> 00:01:54,760
 satipatana and satipatana is defined and explained in the

18
00:01:54,760 --> 00:02:00,760
 commentary as the mindfulness which is

19
00:02:00,760 --> 00:02:09,090
 firmly established. Mindfulness which goes to the object

20
00:02:09,090 --> 00:02:14,480
 with force, so which rushes to the object

21
00:02:14,480 --> 00:02:22,750
 and then covering the object and being well established. So

22
00:02:22,750 --> 00:02:27,840
 that is what is called satipatana

23
00:02:27,840 --> 00:02:33,070
 or the foundations of mindfulness. So actually foundation

24
00:02:33,070 --> 00:02:36,440
 of mindfulness means mindfulness

25
00:02:36,440 --> 00:02:43,610
 which is founded which is firmly established. The

26
00:02:43,610 --> 00:02:49,880
 characteristic of mindfulness is described

27
00:02:49,880 --> 00:02:58,470
 as not wobbling that means not floating on the surface. So

28
00:02:58,470 --> 00:03:03,560
 the mindfulness must go deep into the

29
00:03:03,560 --> 00:03:12,720
 object not just float on the object. The commentary is

30
00:03:12,720 --> 00:03:20,080
 explained that mindfulness must be not like a

31
00:03:20,080 --> 00:03:26,610
 ball or something that floats on the object but it must go

32
00:03:26,610 --> 00:03:30,560
 deep into the object it must sink into

33
00:03:30,560 --> 00:03:36,720
 the object as a rock sinks into the water. So when we

34
00:03:36,720 --> 00:03:41,600
 practice mindfulness we try to establish a

35
00:03:41,600 --> 00:03:47,900
 family on the object of our choice and also on the objects

36
00:03:47,900 --> 00:03:51,840
 that become prominent at the present

37
00:03:51,840 --> 00:04:05,210
 moment. Boda began with the words this is the only way bh

38
00:04:05,210 --> 00:04:11,480
ikkhus and so on. So in these words

39
00:04:11,480 --> 00:04:20,330
 there is a word bhikkhu. Bhikkhu is translated as monk but

40
00:04:20,330 --> 00:04:27,760
 bhikkhu has more meaning than a monk.

41
00:04:27,760 --> 00:04:37,220
 Now the word bhikkhu normally means one who begs. So those

42
00:04:37,220 --> 00:04:44,240
 who begs are called bhikkhus but the

43
00:04:44,240 --> 00:04:51,150
 monks do not beg like the beggars do. So when the monks beg

44
00:04:51,150 --> 00:04:56,040
 they just stand at the door saying

45
00:04:56,040 --> 00:05:00,870
 nothing and then lay people will come out of the houses and

46
00:05:00,870 --> 00:05:04,120
 offer something to them or sometimes

47
00:05:04,120 --> 00:05:09,940
 they may be asked to pass on. So they did not say anything

48
00:05:09,940 --> 00:05:13,400
 but they accept what is given to them

49
00:05:13,400 --> 00:05:22,220
 and that is called the aryan begging or noble begging.

50
00:05:22,220 --> 00:05:27,200
 There is another meaning of the word

51
00:05:27,200 --> 00:05:33,610
 bhikkhu and that is one who sees danger in the round of

52
00:05:33,610 --> 00:05:38,560
 rebirths. So according to that definition

53
00:05:38,560 --> 00:05:44,740
 not only monks but also lay people who see danger in the

54
00:05:44,740 --> 00:05:49,680
 round of rebirths can be called bhikkhus.

55
00:05:49,680 --> 00:06:00,260
 That is why I use the word bhikkhus rather than monks here

56
00:06:00,260 --> 00:06:04,440
 because if we use the word monk for

57
00:06:04,440 --> 00:06:10,600
 the Pali word bhikkhu some people may think that this

58
00:06:10,600 --> 00:06:15,760
 discourse is for monks only but when we have

59
00:06:15,760 --> 00:06:21,790
 the word bhikkhu without translation then we can say that

60
00:06:21,790 --> 00:06:25,920
 this sutta or this discourse is not for

61
00:06:25,920 --> 00:06:35,040
 monks only but for all those who see danger in the round of

62
00:06:35,040 --> 00:06:42,760
 rebirths. The reason why bhikkhus or

63
00:06:42,760 --> 00:06:48,810
 monks are always addressed by the Buddha was that Buddha

64
00:06:48,810 --> 00:06:52,800
 lived in a monastery and he lived with

65
00:06:52,800 --> 00:06:58,290
 monks so whenever he wanted to give a talk he addressed the

66
00:06:58,290 --> 00:07:01,640
 monks naturally. There may be some

67
00:07:01,640 --> 00:07:09,190
 lay people but the monks are the principal receivers of his

68
00:07:09,190 --> 00:07:12,640
 teachings and so Buddha always

69
00:07:12,640 --> 00:07:20,980
 used the word bhikkhu when he wanted to call them bhikkhu.

70
00:07:20,980 --> 00:07:24,560
 And according to the commentary

71
00:07:24,560 --> 00:07:32,390
 many lay people in that area where this discourse was given

72
00:07:32,390 --> 00:07:36,400
 and that is called the kingdom of Kuru.

73
00:07:36,400 --> 00:07:43,000
 It is somewhere near modern Delhi so the commentary

74
00:07:43,000 --> 00:07:48,200
 explains that many lay people also practice the

75
00:07:48,200 --> 00:07:53,150
 four foundations of mindfulness. If they meet together and

76
00:07:53,150 --> 00:07:55,760
 ask what foundation of mindfulness

77
00:07:55,760 --> 00:08:01,910
 are you practicing and if a person answers no I practice

78
00:08:01,910 --> 00:08:06,240
 nothing then he is scolded. So when

79
00:08:06,240 --> 00:08:10,950
 this somebody says that I practice the contemplation on the

80
00:08:10,950 --> 00:08:13,920
 body or on the feelings then they would

81
00:08:13,920 --> 00:08:21,180
 praise him. So according to that explanation many lay

82
00:08:21,180 --> 00:08:25,920
 people also practiced the foundations of

83
00:08:25,920 --> 00:08:26,640
 mindfulness.

84
00:08:32,640 --> 00:08:43,760
 In this sentence Buddha said this is the only way. Now the

85
00:08:43,760 --> 00:08:50,640
 Pali word for this is ikayano. This word ikayano

86
00:08:50,640 --> 00:08:58,100
 has meanings other than the only way. But I like this

87
00:08:58,100 --> 00:09:05,120
 explanation and so I retain the word the only

88
00:09:05,120 --> 00:09:11,510
 way. Now there are many people who do not want Buddha to

89
00:09:11,510 --> 00:09:15,520
 say this is the only way. So according

90
00:09:15,520 --> 00:09:21,200
 to them there must be other ways also to reach nibbana. But

91
00:09:21,200 --> 00:09:29,280
 through practice we can we can

92
00:09:29,280 --> 00:09:35,180
 prove that the translation the only way is a correct

93
00:09:35,180 --> 00:09:40,400
 translation of the Pali word ikayano

94
00:09:40,400 --> 00:09:50,050
 and it is preferable to other translations because so long

95
00:09:50,050 --> 00:09:55,760
 as you have mindfulness so long as

96
00:09:55,760 --> 00:10:02,740
 mindfulness is standing as a guard at the doors of the eyes

97
00:10:02,740 --> 00:10:07,360
 ears and so on no undesirable

98
00:10:08,080 --> 00:10:13,640
 unwholesome mental states can enter your mind. So long as

99
00:10:13,640 --> 00:10:16,720
 there is mindfulness they cannot enter

100
00:10:16,720 --> 00:10:21,340
 your mind and so mindfulness is the only way to prevent

101
00:10:21,340 --> 00:10:24,240
 them from entering your mind and making

102
00:10:24,240 --> 00:10:30,350
 your mind contaminated or in other words to purify your

103
00:10:30,350 --> 00:10:35,440
 mind. So the translation the only way is

104
00:10:36,080 --> 00:10:45,530
 I think preferable here. In the Dhamma Baddha Buddha said

105
00:10:45,530 --> 00:10:49,120
 more explicitly so there he said

106
00:10:49,120 --> 00:10:57,160
 this alone is the way and there is no other. So following

107
00:10:57,160 --> 00:11:00,960
 that statement in the Dhamma Baddha

108
00:11:02,080 --> 00:11:08,940
 the translation of the word ikayana as the only way is

109
00:11:08,940 --> 00:11:12,880
 according to the wishes of the Buddha.

110
00:11:12,880 --> 00:11:22,050
 If it is to purify the mind it must be mindfulness but

111
00:11:22,050 --> 00:11:25,440
 mindfulness can be practiced in many ways.

112
00:11:27,520 --> 00:11:32,510
 Even in the discourse on the foundations of mindfulness

113
00:11:32,510 --> 00:11:36,720
 itself Buddha taught 21 different

114
00:11:36,720 --> 00:11:42,350
 ways of practicing mindfulness. So mindfulness can take

115
00:11:42,350 --> 00:11:45,440
 many forms but whatever form it takes

116
00:11:45,440 --> 00:11:50,140
 it must be mindfulness and it must not be otherwise. That

117
00:11:50,140 --> 00:11:53,680
 is why mindfulness is said to be the only way.

118
00:11:53,680 --> 00:12:09,950
 At the outset at the beginning of this discourse Buddha

119
00:12:09,950 --> 00:12:14,320
 gave us five benefits

120
00:12:15,120 --> 00:12:24,770
 that we can get from the practice of mindfulness. The first

121
00:12:24,770 --> 00:12:26,880
 is the purification of beings.

122
00:12:26,880 --> 00:12:32,280
 Now this is the only way for the purification of beings

123
00:12:32,280 --> 00:12:37,840
 that means when mindfulness is practiced

124
00:12:38,560 --> 00:12:43,280
 it will lead to the purification of beings and purification

125
00:12:43,280 --> 00:12:46,560
 of beings means purification of the

126
00:12:46,560 --> 00:12:51,660
 mind of beings not the physical body. So for the pur

127
00:12:51,660 --> 00:12:54,960
ification of physical body you don't need to

128
00:12:54,960 --> 00:13:00,170
 practice mindfulness meditation. So here purification means

129
00:13:00,170 --> 00:13:01,760
 purification of mind.

130
00:13:02,320 --> 00:13:09,430
 So in order to purify the mind of unwholesome mental states

131
00:13:09,430 --> 00:13:13,920
 we practice mindfulness. So

132
00:13:13,920 --> 00:13:19,960
 this is one benefit we can get from the practice of

133
00:13:19,960 --> 00:13:21,680
 mindfulness.

134
00:13:24,400 --> 00:13:30,010
 Mind is said to be purified when there are no unwholesome

135
00:13:30,010 --> 00:13:33,360
 mental states in the mind. Now

136
00:13:33,360 --> 00:13:43,600
 attachment, greed, hate, anger, delusion, pride, jealousy

137
00:13:43,600 --> 00:13:47,680
 and so on they are all called unwholesome

138
00:13:47,680 --> 00:13:53,050
 mental states. So they contaminate the mind. So long as

139
00:13:53,050 --> 00:13:56,080
 they are in the mind, the mind is said to

140
00:13:56,080 --> 00:14:03,080
 be impure. But by the practice of mindfulness when one is

141
00:14:03,080 --> 00:14:07,440
 able to prevent them from arising in the

142
00:14:07,440 --> 00:14:11,590
 mind or entering the mind then the mind is said to be pure.

143
00:14:11,590 --> 00:14:14,320
 So for the purification of the mind

144
00:14:15,120 --> 00:14:17,440
 we practice mindfulness.

145
00:14:17,440 --> 00:14:30,810
 Now the second benefit Buddha stated was overcoming of

146
00:14:30,810 --> 00:14:32,480
 sorrow and lamentation.

147
00:14:32,480 --> 00:14:37,180
 So if we want to overcome sorrow and lamentation we must

148
00:14:37,180 --> 00:14:39,200
 practice mindfulness.

149
00:14:39,200 --> 00:14:51,760
 Here sorrow means just sorrow and lamentation means crying

150
00:14:51,760 --> 00:14:56,480
 aloud. So when people are sorry

151
00:14:56,480 --> 00:15:01,520
 they may just be sorry or they may cry and they may say

152
00:15:01,520 --> 00:15:04,800
 some things when they are crying and so

153
00:15:04,800 --> 00:15:10,100
 that is called lamentation. Both sorrow and lamentation can

154
00:15:10,100 --> 00:15:12,480
 be overcome by the practice of

155
00:15:12,480 --> 00:15:19,370
 mindfulness. The mindfulness practice is to treat with

156
00:15:19,370 --> 00:15:24,800
 mindfulness every object that arises in the

157
00:15:24,800 --> 00:15:32,790
 mind. Now when there is sorrow we can practice mindfulness

158
00:15:32,790 --> 00:15:36,400
 of that sorrow or we can take that

159
00:15:36,400 --> 00:15:43,220
 sorrow as the object of our attention and we try to to to

160
00:15:43,220 --> 00:15:48,000
 pay attention to it or to make mental

161
00:15:48,000 --> 00:15:58,110
 notes of it and by that way sorrow will disappear. It is

162
00:15:58,110 --> 00:16:02,720
 important that when you have sorrow and you

163
00:16:02,720 --> 00:16:08,350
 want to overcome it then you take sorrow itself as the

164
00:16:08,350 --> 00:16:13,360
 object of your attention and not the cause

165
00:16:13,360 --> 00:16:19,630
 of sorrow. Now it is important that we understand the cause

166
00:16:19,630 --> 00:16:24,160
 of sorrow and sorrow itself. So long as

167
00:16:24,160 --> 00:16:28,970
 we take the cause of sorrow as object sorrow will increase

168
00:16:28,970 --> 00:16:31,920
 but once you turn your mind to sorrow

169
00:16:31,920 --> 00:16:38,410
 itself at that moment the cause of sorrow disappears from

170
00:16:38,410 --> 00:16:42,400
 your mind and so when the cause disappears

171
00:16:44,160 --> 00:16:48,560
 the result which is sorrow must also disappear. So it this

172
00:16:48,560 --> 00:16:51,200
 is the mindfulness method

173
00:16:51,200 --> 00:16:59,750
 to deal with not only sorrow but other undesirable mental

174
00:16:59,750 --> 00:17:01,440
 states.

175
00:17:01,440 --> 00:17:18,390
 The third benefit is the disappearance of pain and grief.

176
00:17:18,390 --> 00:17:21,440
 Now pain here means physical pain

177
00:17:22,160 --> 00:17:28,740
 and grief means mental pain. Now here by the practice of

178
00:17:28,740 --> 00:17:34,720
 mindfulness physical pain may not disappear

179
00:17:34,720 --> 00:17:45,310
 but by the practice of mindfulness you will understand pain

180
00:17:45,310 --> 00:17:46,560
 more clearly

181
00:17:47,440 --> 00:17:53,970
 and you will see that pain also is impermanent it comes and

182
00:17:53,970 --> 00:17:57,600
 goes and so when you realize the

183
00:17:57,600 --> 00:18:03,210
 impermanence of pain you are able to to live with it you

184
00:18:03,210 --> 00:18:06,800
 are able to accept it so it will no longer

185
00:18:08,720 --> 00:18:14,110
 give you much trouble. So although physical pain may not

186
00:18:14,110 --> 00:18:18,560
 disappear if you practice mindfulness

187
00:18:18,560 --> 00:18:26,190
 you will be able to live with pain and grief is mental pain

188
00:18:26,190 --> 00:18:31,520
. Mental pain means grief,

189
00:18:33,120 --> 00:18:38,800
 disappointment, discouragement and fear and many other

190
00:18:38,800 --> 00:18:44,240
 mental states. So

191
00:18:44,240 --> 00:18:51,560
 grief can be overcome or grief can disappear by the

192
00:18:51,560 --> 00:18:57,280
 practice of mindfulness. It is more or less

193
00:18:57,280 --> 00:19:01,240
 the same as dealing with sorrow. So when there is grief,

194
00:19:01,240 --> 00:19:04,640
 when there is mental pain you try to be

195
00:19:04,640 --> 00:19:10,050
 mindful of that pain it's mental pain itself and when your

196
00:19:10,050 --> 00:19:13,840
 concentration becomes strong then you

197
00:19:13,840 --> 00:19:18,350
 you will be able to overcome it and it will disappear. So

198
00:19:18,350 --> 00:19:23,040
 by the practice of mindfulness

199
00:19:23,680 --> 00:19:30,270
 physical pain may or may not disappear but grief or mental

200
00:19:30,270 --> 00:19:33,520
 pain will surely disappear.

201
00:19:44,880 --> 00:19:50,300
 And the next benefit is reaching the noble path. Now the

202
00:19:50,300 --> 00:19:54,240
 word path should be understood correctly.

203
00:19:54,240 --> 00:20:02,080
 Here it is written with a capital P.

204
00:20:02,080 --> 00:20:08,160
 In Pali it is called Maga.

205
00:20:12,960 --> 00:20:19,930
 Maga arises at the moment of enlightenment. Suppose a

206
00:20:19,930 --> 00:20:24,400
 person is practicing Vipassana meditation

207
00:20:24,400 --> 00:20:28,920
 and he will go through different stages of Vipassana

208
00:20:28,920 --> 00:20:32,320
 meditation one by one and when

209
00:20:33,520 --> 00:20:41,310
 his Vipassana has become mature then a time will come when

210
00:20:41,310 --> 00:20:46,000
 a type of consciousness arises in his

211
00:20:46,000 --> 00:20:50,800
 mind and that type of consciousness he has never

212
00:20:50,800 --> 00:20:56,080
 experienced before and that type of consciousness

213
00:20:57,200 --> 00:21:02,560
 can eradicate the mental defilements once and for all. So

214
00:21:02,560 --> 00:21:05,600
 that consciousness is called

215
00:21:05,600 --> 00:21:12,160
 path consciousness and to reach that consciousness or to

216
00:21:12,160 --> 00:21:16,960
 have that consciousness arise in one's mind

217
00:21:16,960 --> 00:21:21,190
 then one needs to practice mindfulness. So the practice of

218
00:21:21,190 --> 00:21:24,240
 mindfulness will ultimately

219
00:21:25,120 --> 00:21:31,040
 lead the practitioner to reaching the noble path.

220
00:21:31,040 --> 00:21:44,270
 The last benefit is the realization of Nibbana. So when

221
00:21:44,270 --> 00:21:48,320
 path or Maga consciousness arises

222
00:21:49,200 --> 00:21:55,920
 it takes Nibbana as object. So Nibbana is the extinction of

223
00:21:55,920 --> 00:21:59,440
 mental defilements and the

224
00:21:59,440 --> 00:22:06,050
 extinction of suffering. So path consciousness arises

225
00:22:06,050 --> 00:22:08,960
 taking Nibbana as object.

226
00:22:08,960 --> 00:22:16,270
 So to take Nibbana as object is called realization of Nibb

227
00:22:16,270 --> 00:22:16,800
ana.

228
00:22:17,520 --> 00:22:26,760
 So Buddha pointed out the benefits that we will get if we

229
00:22:26,760 --> 00:22:33,680
 practice mindfulness meditation.

230
00:22:33,680 --> 00:22:37,840
 Now these five benefits are actually just one.

231
00:22:40,480 --> 00:22:46,010
 Purification of mind. So when mind is totally pure there

232
00:22:46,010 --> 00:22:48,400
 can be no sorrow or lamentation

233
00:22:48,400 --> 00:22:53,440
 and there can be no grief and when mind is totally pure

234
00:22:53,440 --> 00:23:01,640
 it reaches a stage of path consciousness and when path

235
00:23:01,640 --> 00:23:05,280
 consciousness arises it takes Nibbana

236
00:23:05,280 --> 00:23:09,200
 as object and at that moment the mind is totally pure again

237
00:23:09,200 --> 00:23:13,600
. So the benefit we will get from the

238
00:23:13,600 --> 00:23:20,330
 practice of foundations of mindfulness is just total pur

239
00:23:20,330 --> 00:23:22,320
ification of mind.

240
00:23:33,120 --> 00:23:38,040
 And what is this only way? Buddha said this is the four

241
00:23:38,040 --> 00:23:41,120
 foundations of mindfulness. So the four

242
00:23:41,120 --> 00:23:44,880
 foundations of mindfulness or the practice of mindfulness

243
00:23:44,880 --> 00:23:47,200
 is the only way to achieve purification

244
00:23:47,200 --> 00:23:54,060
 of mind and so on. And what are the four? Herein in this

245
00:23:54,060 --> 00:23:58,000
 teaching bhikkhus, in this teaching means

246
00:23:58,000 --> 00:24:02,150
 in the dispensation of the Buddha. A bhikkhu dwells

247
00:24:02,150 --> 00:24:04,720
 contemplating the body and the body

248
00:24:04,720 --> 00:24:10,620
 ardent clearly comprehending and mindful removing covetous

249
00:24:10,620 --> 00:24:13,520
ness and grief in the world.

250
00:24:13,520 --> 00:24:18,240
 And this is the first foundation of mindfulness and the

251
00:24:18,240 --> 00:24:22,800
 others are second third and fourth.

252
00:24:22,800 --> 00:24:37,600
 So the four foundations of mindfulness are described here

253
00:24:37,600 --> 00:24:42,720
 by way of person or by way of a

254
00:24:42,720 --> 00:24:49,350
 monk practicing it. So the first foundation of mindfulness

255
00:24:49,350 --> 00:24:52,240
 is contemplating the body

256
00:24:53,120 --> 00:24:58,730
 in the body. Now the word contemplating is the translation

257
00:24:58,730 --> 00:25:02,000
 of the Bali word anupasana.

258
00:25:02,000 --> 00:25:12,920
 Anupasana means repeatedly seeing, repeatedly observing. So

259
00:25:12,920 --> 00:25:17,680
 the English word contemplating

260
00:25:17,680 --> 00:25:22,480
 may mean some other thing but here contemplating means

261
00:25:22,480 --> 00:25:26,080
 watching it repeatedly, seeing it repeatedly

262
00:25:26,080 --> 00:25:30,140
 and contemplating the body in the body means contemplating

263
00:25:30,140 --> 00:25:31,840
 the body in the body and not

264
00:25:31,840 --> 00:25:35,920
 contemplating the feeling in the body and so on. So the

265
00:25:35,920 --> 00:25:37,920
 word body is repeated here

266
00:25:39,200 --> 00:25:48,400
 to show that the contemplation must be precise. The body in

267
00:25:48,400 --> 00:25:49,280
 the body and not

268
00:25:49,280 --> 00:25:53,440
 feeling in the body, consciousness in the body and so on.

269
00:25:53,440 --> 00:26:02,060
 The word body here does not necessarily mean the physical

270
00:26:02,060 --> 00:26:06,320
 body only or the whole physical body only.

271
00:26:07,120 --> 00:26:15,590
 The body here means a group, a mess of different things.

272
00:26:15,590 --> 00:26:19,680
 Now if you go to the details in the

273
00:26:19,680 --> 00:26:24,450
 discourse you will find that the breathing in and breathing

274
00:26:24,450 --> 00:26:28,400
 out are called body and postures of the

275
00:26:28,400 --> 00:26:34,480
 body are called body and so on. So by the word body here we

276
00:26:34,480 --> 00:26:41,120
 must understand the group of or the

277
00:26:41,120 --> 00:26:47,980
 combination of different parts not necessarily the whole

278
00:26:47,980 --> 00:26:49,680
 physical body.

279
00:26:57,440 --> 00:27:01,600
 So when you are mindful of the breath you are practicing

280
00:27:01,600 --> 00:27:04,160
 the contemplation of the body

281
00:27:04,160 --> 00:27:13,170
 in the body and when we practice Buddha taught that we must

282
00:27:13,170 --> 00:27:17,200
 be ardent, clearly comprehending

283
00:27:17,920 --> 00:27:24,970
 and mindful. So by these words Buddha showed us the

284
00:27:24,970 --> 00:27:29,040
 components of the practice. So when we practice

285
00:27:29,040 --> 00:27:35,010
 mindfulness we must have these components arise in our

286
00:27:35,010 --> 00:27:35,680
 minds.

287
00:27:35,680 --> 00:27:43,360
 So the first component is

288
00:27:47,120 --> 00:27:56,170
 described by the word ardent. That means the bhikkhu or the

289
00:27:56,170 --> 00:27:59,920
 yogi makes effort. So by

290
00:27:59,920 --> 00:28:05,280
 ardent Buddha meant that we must make effort to be mindful.

291
00:28:05,280 --> 00:28:09,760
 Without effort we cannot be mindful.

292
00:28:14,720 --> 00:28:21,290
 And in the commentaries it is explained that the effort has

293
00:28:21,290 --> 00:28:27,680
 the ability to dry up or to heat

294
00:28:27,680 --> 00:28:34,130
 up the mental defilements. So that means when there is

295
00:28:34,130 --> 00:28:40,000
 effort, when we make effort we can drive away

296
00:28:41,120 --> 00:28:47,020
 or we can burn the mental defilements. Now the Pali word

297
00:28:47,020 --> 00:28:49,920
 for this English word is

298
00:28:49,920 --> 00:29:02,430
 and comes from the word which means heat. So a person who

299
00:29:02,430 --> 00:29:07,680
 has heat is called and heat here means

300
00:29:08,640 --> 00:29:14,210
 effort. So the effort has the ability to to heat or to burn

301
00:29:14,210 --> 00:29:16,560
 the mental defilements.

302
00:29:16,560 --> 00:29:25,840
 When you read the passage in Pali you can feel the meaning

303
00:29:25,840 --> 00:29:30,720
 of the word ata bhi as heating up

304
00:29:31,280 --> 00:29:37,490
 the mental defilements. But in the English translation you

305
00:29:37,490 --> 00:29:40,720
 cannot get that kind of meaning.

306
00:29:40,720 --> 00:29:47,520
 The first component Buddha mentioned here is

307
00:29:50,560 --> 00:29:55,970
 effort or making effort. And the second is clearly

308
00:29:55,970 --> 00:30:02,160
 comprehending. So a yogi when he practices

309
00:30:02,160 --> 00:30:08,080
 mindfulness, here mindfulness of the body, he must clearly

310
00:30:08,080 --> 00:30:10,800
 comprehend the body. He must

311
00:30:10,800 --> 00:30:17,690
 clearly see the body. That means he must see correctly. And

312
00:30:17,690 --> 00:30:20,640
 clear comprehension here

313
00:30:20,640 --> 00:30:28,600
 means not just seeing or understanding but understanding

314
00:30:28,600 --> 00:30:33,520
 that there are just the parts

315
00:30:33,520 --> 00:30:42,450
 of the body. There are just the mind that is aware of the

316
00:30:42,450 --> 00:30:46,080
 body and there is the body. And

317
00:30:46,080 --> 00:30:50,970
 there are only these two things going on at every moment.

318
00:30:50,970 --> 00:30:52,720
 And there is nothing

319
00:30:54,320 --> 00:31:04,930
 which we can call a soul or a self or ata in this body. So

320
00:31:04,930 --> 00:31:07,600
 clearly comprehending means seeing

321
00:31:07,600 --> 00:31:14,650
 seeing the things correctly, seeing that they are just the

322
00:31:14,650 --> 00:31:20,080
 the manifestation of the different

323
00:31:22,640 --> 00:31:30,550
 aspects of the mind and body. And there is nothing apart

324
00:31:30,550 --> 00:31:34,720
 from mind and body which we can call an

325
00:31:34,720 --> 00:31:40,540
 agent or a person or a soul or a self. So understanding

326
00:31:40,540 --> 00:31:44,160
 that way is called clear comprehension.

327
00:31:44,800 --> 00:31:53,280
 So that will come only after after some time of meditation.

328
00:31:53,280 --> 00:31:56,160
 It will not come at the very beginning

329
00:31:56,160 --> 00:32:02,690
 of the practice but with practice a yogi will be able to

330
00:32:02,690 --> 00:32:07,760
 see in this way that there is nothing we

331
00:32:07,760 --> 00:32:15,420
 can call a person or a soul or self apart from the mind and

332
00:32:15,420 --> 00:32:19,440
 matter that is that are functioning

333
00:32:19,440 --> 00:32:26,830
 at the moment. When you try to be mindful of the breath and

334
00:32:26,830 --> 00:32:31,040
 when you see that there are the breath

335
00:32:31,920 --> 00:32:36,370
 and the mind that is mindful of the breath and nothing more

336
00:32:36,370 --> 00:32:40,880
 than you are said to to have clear

337
00:32:40,880 --> 00:32:47,930
 comprehension. And the third component Buddha mentioned is

338
00:32:47,930 --> 00:32:49,440
 mindfulness.

339
00:32:52,400 --> 00:32:59,800
 So a yogi practices mindfulness and mindfulness means full

340
00:32:59,800 --> 00:33:04,560
 awareness of the object. Not not a

341
00:33:04,560 --> 00:33:11,900
 simple awareness, not a superficial awareness, but a deep

342
00:33:11,900 --> 00:33:17,040
 going awareness. And that full awareness

343
00:33:17,040 --> 00:33:24,550
 of the object is what we call mindfulness. Now if you look

344
00:33:24,550 --> 00:33:28,080
 at this sentence you will find

345
00:33:28,080 --> 00:33:33,140
 that the first is effort and the second is understanding

346
00:33:33,140 --> 00:33:35,840
 and the third is mindfulness.

347
00:33:36,960 --> 00:33:44,510
 Sometimes we cannot follow the order of the words in the

348
00:33:44,510 --> 00:33:50,400
 discourse because from the practical point

349
00:33:50,400 --> 00:33:59,740
 of view mindfulness must come after ardent. You make effort

350
00:33:59,740 --> 00:34:04,720
 and you are mindful and your mindfulness

351
00:34:04,720 --> 00:34:09,900
 will will improve with practice and only after some

352
00:34:09,900 --> 00:34:14,240
 practice only after your mindfulness has become

353
00:34:14,240 --> 00:34:20,990
 firm and mature that you will see things clearly or that

354
00:34:20,990 --> 00:34:24,240
 clear comprehension can come.

355
00:34:24,240 --> 00:34:31,730
 So although the order of the words here is the effort first

356
00:34:31,730 --> 00:34:33,840
, understanding second,

357
00:34:33,840 --> 00:34:38,640
 and mindfulness third, in practice we must understand that

358
00:34:38,640 --> 00:34:45,930
 effort first and then there is mindfulness and third clear

359
00:34:45,930 --> 00:34:47,920
 comprehension.

360
00:34:47,920 --> 00:34:56,230
 There is a statement in the commentaries which says the

361
00:34:56,230 --> 00:35:00,560
 order of the words is one thing and the

362
00:35:00,560 --> 00:35:05,410
 order of the meaning is another. So that means sometimes we

363
00:35:05,410 --> 00:35:08,000
 do not follow the order of words

364
00:35:08,000 --> 00:35:11,210
 but we follow the order of the meaning or order of what

365
00:35:11,210 --> 00:35:12,400
 actually happens.

366
00:35:12,400 --> 00:35:21,160
 So here we must understand that first there is effort and

367
00:35:21,160 --> 00:35:25,920
 then mindfulness and then clear comprehension.

368
00:35:26,960 --> 00:35:30,960
 And here

369
00:35:30,960 --> 00:35:41,520
 as this sentence stands there is one component missing.

370
00:35:41,520 --> 00:35:49,900
 When you practice mindfulness your mind will become

371
00:35:49,900 --> 00:35:52,240
 concentrated

372
00:35:53,920 --> 00:35:59,150
 only when your mind is concentrated can you see clearly can

373
00:35:59,150 --> 00:36:02,000
 you have clear comprehension.

374
00:36:02,000 --> 00:36:10,830
 So concentration is also an important component in the

375
00:36:10,830 --> 00:36:14,560
 practice but Buddha did not mention

376
00:36:15,520 --> 00:36:21,770
 concentration here so the sub-commentary explained that by

377
00:36:21,770 --> 00:36:26,880
 mindfulness we must also take concentration.

378
00:36:26,880 --> 00:36:35,040
 So following the sub-commentary we get four components here

379
00:36:35,040 --> 00:36:43,200
 effort, mindfulness, concentration, and understanding.

380
00:36:44,080 --> 00:36:49,760
 Now there is one more component

381
00:36:49,760 --> 00:36:59,440
 not mentioned here and that is faith or confidence.

382
00:36:59,440 --> 00:37:04,260
 Now we need to have confidence in the teachings of the

383
00:37:04,260 --> 00:37:07,360
 Buddha we need to have confidence

384
00:37:09,040 --> 00:37:13,580
 in the practice and we need to have confidence in ourselves

385
00:37:13,580 --> 00:37:17,200
. If we do not have confidence if we do not

386
00:37:17,200 --> 00:37:23,390
 have devotion towards the Buddha we will not practice at

387
00:37:23,390 --> 00:37:26,960
 all. So although

388
00:37:28,560 --> 00:37:35,340
 confidence is not functioning actively when we practice

389
00:37:35,340 --> 00:37:39,680
 mindfulness it is still in the background.

390
00:37:39,680 --> 00:37:49,980
 So with confidence we get five components so confidence,

391
00:37:49,980 --> 00:37:53,360
 effort, mindfulness, concentration,

392
00:37:54,000 --> 00:38:00,100
 and understanding and these are the five that are called

393
00:38:00,100 --> 00:38:01,760
 controlling faculties.

394
00:38:01,760 --> 00:38:10,580
 By these words ardent clearly comprehending and mindful

395
00:38:10,580 --> 00:38:14,720
 Buddha showed us the components

396
00:38:14,720 --> 00:38:21,030
 of the practice and by the next expression removing covet

397
00:38:21,030 --> 00:38:24,400
ousness and grief in the world

398
00:38:24,400 --> 00:38:31,440
 Buddha showed us the mental state to be abandoned.

399
00:38:41,600 --> 00:38:47,290
 Or by the words removing covetousness and grief in the

400
00:38:47,290 --> 00:38:49,440
 world Buddha showed us

401
00:38:49,440 --> 00:38:58,230
 what is removed, what are removed, or what are abandoned by

402
00:38:58,230 --> 00:39:01,920
 the practice of mindfulness. Or

403
00:39:02,880 --> 00:39:10,110
 when we practice mindfulness we remove we abandon or we

404
00:39:10,110 --> 00:39:13,360
 prevent covetousness

405
00:39:13,360 --> 00:39:16,480
 and grief from arising in our minds.

406
00:39:16,480 --> 00:39:23,840
 You don't have to make special effort to remove covetous

407
00:39:23,840 --> 00:39:27,680
ness and grief. When you are

408
00:39:29,680 --> 00:39:34,380
 mindful, when your mind is concentrated, and when you see

409
00:39:34,380 --> 00:39:38,320
 things clearly then you are at the same

410
00:39:38,320 --> 00:39:44,560
 time removing that means preventing covetousness and grief

411
00:39:44,560 --> 00:39:46,800
 to arise in your mind.

412
00:39:52,160 --> 00:39:56,980
 So at every moment of your practice, at every moment of

413
00:39:56,980 --> 00:40:00,000
 mindfulness, at every moment of clear

414
00:40:00,000 --> 00:40:05,750
 comprehension, you are removing the covetousness and grief.

415
00:40:05,750 --> 00:40:12,320
 Now the original words for covetousness

416
00:40:12,320 --> 00:40:19,780
 and grief are abhijya and dharmanasa. Now the word abhijya

417
00:40:19,780 --> 00:40:23,200
 translated here as covetousness

418
00:40:23,200 --> 00:40:27,520
 primarily means the

419
00:40:27,520 --> 00:40:34,640
 the desire

420
00:40:37,920 --> 00:40:44,240
 to possess something that other people possess. Suppose a

421
00:40:44,240 --> 00:40:45,120
 person has

422
00:40:45,120 --> 00:40:54,530
 a new car then if another person wants to get that new car

423
00:40:54,530 --> 00:40:59,200
 for his own then he is said to have

424
00:40:59,760 --> 00:41:06,420
 abhijya or covetousness. So abhijya in this sense is not

425
00:41:06,420 --> 00:41:10,640
 just attachment or desire or craving

426
00:41:10,640 --> 00:41:17,620
 but it is the desire to possess the possessions of other

427
00:41:17,620 --> 00:41:22,160
 people. Say you have a good car and I want

428
00:41:22,160 --> 00:41:27,300
 to get that car for myself and that is covetousness but

429
00:41:27,300 --> 00:41:31,920
 here in this sentence covetousness or abhijya

430
00:41:31,920 --> 00:41:38,600
 is used not in this narrow sense it means just attachment

431
00:41:38,600 --> 00:41:41,120
 or craving or desire.

432
00:41:43,840 --> 00:41:49,440
 I the word grief also you must understand not just grief

433
00:41:49,440 --> 00:41:55,120
 but ill will, hate, anger, disappointment,

434
00:41:55,120 --> 00:42:01,520
 depression, discouragement and so on.

435
00:42:06,400 --> 00:42:12,970
 Now there are five mental hindrances and among these five

436
00:42:12,970 --> 00:42:14,800
 these two are included

437
00:42:14,800 --> 00:42:22,660
 covetousness and grief or attachment and ill will and these

438
00:42:22,660 --> 00:42:26,720
 two are the more powerful hindrances

439
00:42:26,720 --> 00:42:31,190
 than the other three and so when these two are mentioned we

440
00:42:31,190 --> 00:42:34,080
 must understand that the others are

441
00:42:34,640 --> 00:42:40,380
 also mentioned. So by the words removing covetousness and

442
00:42:40,380 --> 00:42:42,080
 grief in the world

443
00:42:42,080 --> 00:42:47,120
 really means removing all the five mental hindrances.

444
00:42:47,120 --> 00:42:56,870
 So as you practice mindfulness you remove these mental hind

445
00:42:56,870 --> 00:42:59,600
rances. So when the mental hindrances

446
00:42:59,600 --> 00:43:03,460
 are removed you get concentration. So when you get

447
00:43:03,460 --> 00:43:06,400
 concentration you see things clearly.

448
00:43:06,400 --> 00:43:15,840
 Now in this sentence the translation removing is important

449
00:43:15,840 --> 00:43:19,680
 because in many other translations

450
00:43:21,200 --> 00:43:30,710
 they said having removed or having abandoned or having

451
00:43:30,710 --> 00:43:37,040
 overcome but the translating the

452
00:43:37,040 --> 00:43:45,710
 Pali word with having is not correct here because if we say

453
00:43:45,710 --> 00:43:50,960
 having removed covetousness and grief

454
00:43:50,960 --> 00:43:56,000
 it will it may mean or it will mean that we must first

455
00:43:56,000 --> 00:43:59,360
 remove covetousness and grief

456
00:43:59,360 --> 00:44:03,860
 and then practice mindfulness but actually we practice

457
00:44:03,860 --> 00:44:06,960
 mindfulness to get rid of or to remove

458
00:44:06,960 --> 00:44:12,420
 covetousness and grief. If we have already removed covetous

459
00:44:12,420 --> 00:44:14,960
ness and grief we don't need to practice

460
00:44:14,960 --> 00:44:21,940
 mindfulness we already have achieved our goal. So the Pali

461
00:44:21,940 --> 00:44:28,160
 word here used here although it can mean

462
00:44:28,880 --> 00:44:34,160
 having the removed or whatever here it does not have that

463
00:44:34,160 --> 00:44:38,080
 sense. So here it means that the

464
00:44:38,080 --> 00:44:46,770
 removing and being mindful occur at the same time. So while

465
00:44:46,770 --> 00:44:52,240
 he is mindful he is removing covetousness

466
00:44:52,240 --> 00:44:56,820
 and grief. So he doesn't have to make a special effort to

467
00:44:56,820 --> 00:44:59,440
 remove covetousness and grief. As he

468
00:44:59,440 --> 00:45:04,920
 goes along he is removing covetousness and grief and not

469
00:45:04,920 --> 00:45:08,720
 after removing covetousness and grief

470
00:45:08,720 --> 00:45:12,940
 but he removes the covetousness and grief as he goes along

471
00:45:12,940 --> 00:45:15,520
 with his practice of mindfulness.

472
00:45:19,920 --> 00:45:28,850
 So by this short statement Buddha taught us what to do when

473
00:45:28,850 --> 00:45:31,600
 we practice mindfulness meditation

474
00:45:31,600 --> 00:45:43,200
 and what mental states we can remove or we can prevent as

475
00:45:43,200 --> 00:45:46,960
 we go along the practice of mindfulness.

476
00:45:46,960 --> 00:45:52,560
 Okay I thought I could finish explaining the four

477
00:45:52,560 --> 00:45:56,320
 foundations of mindfulness today

478
00:45:56,320 --> 00:46:03,360
 but as it is we will have to wait until tomorrow for the

479
00:46:03,360 --> 00:46:08,320
 explanation of the foundations of

480
00:46:08,320 --> 00:46:17,680
 mindfulness.

