1
00:00:00,000 --> 00:00:21,400
 Okay, good evening everyone.

2
00:00:21,400 --> 00:00:25,360
 Nice to see you all here again.

3
00:00:25,360 --> 00:00:39,040
 Our meditators here locally.

4
00:00:39,040 --> 00:00:43,800
 It's Friday night.

5
00:00:43,800 --> 00:00:46,550
 Friday night I think is still the time when people go out

6
00:00:46,550 --> 00:00:50,080
 and

7
00:00:50,080 --> 00:01:06,080
 seek happiness in their various ways.

8
00:01:06,080 --> 00:01:11,400
 Sometimes meditators get a little bit discouraged thinking

9
00:01:11,400 --> 00:01:14,960
 about all the fun they could be having

10
00:01:14,960 --> 00:01:19,240
 out in the world.

11
00:01:19,240 --> 00:01:23,560
 All the happiness that they're missing out on.

12
00:01:23,560 --> 00:01:26,040
 So much happiness out there, no?

13
00:01:26,040 --> 00:01:27,040
 Easy.

14
00:01:27,040 --> 00:01:40,200
 It's like low hanging fruit dangling just waiting to be pl

15
00:01:40,200 --> 00:01:42,440
ucked.

16
00:01:42,440 --> 00:01:47,450
 If you think about it, if you're discerning and perceptive,

17
00:01:47,450 --> 00:01:49,640
 there really isn't a lot of

18
00:01:49,640 --> 00:01:57,440
 happiness out there to be had.

19
00:01:57,440 --> 00:02:04,710
 All the happiness we might find is bound up by craving and

20
00:02:04,710 --> 00:02:08,360
 clinging and addiction.

21
00:02:08,360 --> 00:02:12,640
 We seek and we seek and we yearn for happiness.

22
00:02:12,640 --> 00:02:19,640
 We get little bits of it.

23
00:02:19,640 --> 00:02:22,160
 And then it's gone and we're left with clinging and craving

24
00:02:22,160 --> 00:02:33,840
 and yearning.

25
00:02:33,840 --> 00:02:36,680
 So in Buddhism we are much concerned with happiness as well

26
00:02:36,680 --> 00:02:36,960
.

27
00:02:36,960 --> 00:02:40,240
 It's not that happiness isn't important.

28
00:02:40,240 --> 00:02:44,240
 It's that we're not interested in low hanging fruit.

29
00:02:44,240 --> 00:02:49,040
 We're not interested in the easy form of happiness because

30
00:02:49,040 --> 00:02:51,800
 unfortunately the easy forms of happiness

31
00:02:51,800 --> 00:02:57,720
 are not truly satisfying.

32
00:02:57,720 --> 00:03:03,060
 These are the ones that are all caught up with clinging and

33
00:03:03,060 --> 00:03:06,440
 craving like bait on a hook.

34
00:03:06,440 --> 00:03:13,560
 They get you more caught up and more entangled, more

35
00:03:13,560 --> 00:03:19,000
 stressed and they lead to more suffering.

36
00:03:19,000 --> 00:03:21,250
 But there are other kinds of happiness and this is what we

37
00:03:21,250 --> 00:03:22,640
 feel great about on this Friday

38
00:03:22,640 --> 00:03:29,830
 night when we come together to meditate, to consider the D

39
00:03:29,830 --> 00:03:34,200
hamma, the Buddhist teaching.

40
00:03:34,200 --> 00:03:43,810
 What kinds of happiness do we have in the Buddhist teaching

41
00:03:43,810 --> 00:03:44,400
?

42
00:03:44,400 --> 00:03:50,890
 Well the first happiness we have is this gathering, our

43
00:03:50,890 --> 00:03:53,480
 ability to be here.

44
00:03:53,480 --> 00:03:59,490
 We have the Buddha center, we have here a meditation center

45
00:03:59,490 --> 00:03:59,920
.

46
00:03:59,920 --> 00:04:06,660
 We have the happiness of our community, of our place, of

47
00:04:06,660 --> 00:04:10,280
 our physical experience that

48
00:04:10,280 --> 00:04:15,700
 allows us to get away from the stresses and the meaning

49
00:04:15,700 --> 00:04:19,040
lessness of banal existence and

50
00:04:19,040 --> 00:04:26,380
 come to a spiritual place, come to a gathering of spiritual

51
00:04:26,380 --> 00:04:29,840
 intent where we all have the

52
00:04:29,840 --> 00:04:34,740
 same goal and the same direction in our minds, where there

53
00:04:34,740 --> 00:04:37,760
's wholesomeness, where there's

54
00:04:37,760 --> 00:04:51,040
 goodness, where there's reassurance on our path.

55
00:04:51,040 --> 00:04:53,290
 So even before we start to practice, even before we do

56
00:04:53,290 --> 00:04:54,680
 anything wholesome, we've got

57
00:04:54,680 --> 00:05:01,540
 this greatness of community, the greatness of our ability

58
00:05:01,540 --> 00:05:04,640
 to gather together in this

59
00:05:04,640 --> 00:05:11,620
 way, just to be in this place, not physically the space but

60
00:05:11,620 --> 00:05:14,880
 to be in an environment that

61
00:05:14,880 --> 00:05:24,800
 is encouraging of a spiritual practice.

62
00:05:24,800 --> 00:05:27,980
 But there's more with all of the practice that we do.

63
00:05:27,980 --> 00:05:30,820
 The second great happiness that we have is from our

64
00:05:30,820 --> 00:05:32,720
 morality, from our ethics.

65
00:05:32,720 --> 00:05:35,920
 We have this reassurance in our minds that we are good

66
00:05:35,920 --> 00:05:36,680
 people.

67
00:05:36,680 --> 00:05:42,960
 It's a question, am I a good person?

68
00:05:42,960 --> 00:05:46,260
 Is there anything good about me, anything great, anything

69
00:05:46,260 --> 00:05:47,960
 that wives people will esteem

70
00:05:47,960 --> 00:05:51,600
 and appreciate and praise?

71
00:05:51,600 --> 00:05:54,640
 Well now there is.

72
00:05:54,640 --> 00:06:02,270
 Here and now we're in a situation, we're in a position, we

73
00:06:02,270 --> 00:06:04,640
're on a path that leads towards

74
00:06:04,640 --> 00:06:11,230
 greatness, that is made up of greatness, great things all

75
00:06:11,230 --> 00:06:15,000
 along the way, starting with our

76
00:06:15,000 --> 00:06:19,520
 ethics.

77
00:06:19,520 --> 00:06:25,660
 We should be very happy and we are in these situations

78
00:06:25,660 --> 00:06:29,280
 quite content in our spiritual

79
00:06:29,280 --> 00:06:34,400
 state of wholesomeness of mind, that we're not inclined

80
00:06:34,400 --> 00:06:39,120
 towards greed or anger or delusion,

81
00:06:39,120 --> 00:06:50,720
 that we're actually struggling to keep our minds free from

82
00:06:50,720 --> 00:06:53,840
 defilement.

83
00:06:53,840 --> 00:06:57,700
 We can be happy that at this moment we are good people,

84
00:06:57,700 --> 00:06:59,920
 because goodness of course is

85
00:06:59,920 --> 00:07:04,970
 momentary, people as well are changing from moment to

86
00:07:04,970 --> 00:07:06,000
 moment.

87
00:07:06,000 --> 00:07:15,870
 Whoever was negligent in the past but later on becomes

88
00:07:15,870 --> 00:07:21,840
 vigilant, becomes mindful.

89
00:07:21,840 --> 00:07:27,130
 So among lokang pabhasi di abhomudovatandimah, such a

90
00:07:27,130 --> 00:07:28,960
 person lights up the world like the

91
00:07:28,960 --> 00:07:33,440
 moon coming out from behind the cloud.

92
00:07:33,440 --> 00:07:38,830
 I don't have to worry about what we were in the past, we've

93
00:07:38,830 --> 00:07:41,320
 come here with all sorts of

94
00:07:41,320 --> 00:07:45,160
 baggage while I leave the baggage at the door.

95
00:07:45,160 --> 00:07:47,960
 Now we are on the right path, for this time we are

96
00:07:47,960 --> 00:07:50,240
 cultivating wholesomeness and we can

97
00:07:50,240 --> 00:07:57,360
 feel great about that, we can feel good about ourselves.

98
00:07:57,360 --> 00:08:01,630
 The third happiness we have is when we start to become

99
00:08:01,630 --> 00:08:04,440
 mindful, focused in the present

100
00:08:04,440 --> 00:08:09,830
 moment, we tap into reality again, we sink our roots into

101
00:08:09,830 --> 00:08:12,400
 the earth, into the soil of

102
00:08:12,400 --> 00:08:19,380
 understanding, into the soil of mindfulness and we are nour

103
00:08:19,380 --> 00:08:22,720
ished by the reality of it.

104
00:08:22,720 --> 00:08:29,200
 We leave behind our concepts and our beliefs and views and

105
00:08:29,200 --> 00:08:33,280
 opinions, personalities even,

106
00:08:33,280 --> 00:08:39,380
 we leave it all behind for here and now, being aware of the

107
00:08:39,380 --> 00:08:43,120
 body, feelings, the mind, the

108
00:08:43,120 --> 00:08:51,000
 emotions and the senses and so on.

109
00:08:51,000 --> 00:08:55,770
 And this is happiness for us, this is great satisfaction,

110
00:08:55,770 --> 00:08:58,080
 there's great satisfaction in

111
00:08:58,080 --> 00:09:02,810
 being present, no matter how much pain or stress there

112
00:09:02,810 --> 00:09:05,800
 might be in the present moment,

113
00:09:05,800 --> 00:09:17,920
 once you're truly present it all loses its edge, it all

114
00:09:17,920 --> 00:09:22,800
 fades into just being experienced

115
00:09:22,800 --> 00:09:30,940
 without any of the positive or negative qualities, it just

116
00:09:30,940 --> 00:09:33,760
 is its experience and we become quite

117
00:09:33,760 --> 00:09:36,600
 peaceful.

118
00:09:36,600 --> 00:09:46,800
 This is the third happiness that we get from staying in on

119
00:09:46,800 --> 00:09:50,160
 a Friday night.

120
00:09:50,160 --> 00:09:55,180
 The fourth we get is insight, wisdom, this is the fourth

121
00:09:55,180 --> 00:09:58,080
 happiness that comes from being

122
00:09:58,080 --> 00:10:00,440
 here today.

123
00:10:00,440 --> 00:10:03,620
 So we start to think more about good teachings and

124
00:10:03,620 --> 00:10:06,120
 listening to the teacher, we get some

125
00:10:06,120 --> 00:10:09,760
 good information and tips and direction, we're able to

126
00:10:09,760 --> 00:10:12,120
 direct our minds in the right direction

127
00:10:12,120 --> 00:10:15,850
 and start to understand ourselves, we start to see the mind

128
00:10:15,850 --> 00:10:18,480
 as impermanent, unsatisfying,

129
00:10:18,480 --> 00:10:23,060
 uncontrollable, we see the body as well as unstable,

130
00:10:23,060 --> 00:10:25,720
 unpredictable and we start to see

131
00:10:25,720 --> 00:10:32,480
 that everything that we cling to is not worth clinging to

132
00:10:32,480 --> 00:10:36,000
 and we start to let go and gain

133
00:10:36,000 --> 00:10:52,200
 true wisdom and insight into reality.

134
00:10:52,200 --> 00:11:01,060
 And finally the last thing we get is freedom, the greatest

135
00:11:01,060 --> 00:11:05,880
 happiness of all is freedom.

136
00:11:05,880 --> 00:11:09,790
 We come here to practice and as we gain insight, even just

137
00:11:09,790 --> 00:11:12,640
 sitting here listening to the dhamma,

138
00:11:12,640 --> 00:11:21,530
 it's quite reasonable to expect freedom, that we start to

139
00:11:21,530 --> 00:11:25,720
 free ourselves from first from

140
00:11:25,720 --> 00:11:29,020
 our wrong views and then from our wrong thoughts and then

141
00:11:29,020 --> 00:11:31,120
 from our wrong emotions, all the

142
00:11:31,120 --> 00:11:34,630
 wrong, wrong, wrong, the things that cause us suffering and

143
00:11:34,630 --> 00:11:36,120
 stress or that cause us to

144
00:11:36,120 --> 00:11:42,000
 hurt others, all that is wrong about us fades away and what

145
00:11:42,000 --> 00:11:44,840
 is right comes up, we enter

146
00:11:44,840 --> 00:11:50,080
 on the right path, we enter into right thought, we

147
00:11:50,080 --> 00:11:54,240
 cultivate right view and we become free,

148
00:11:54,240 --> 00:12:05,250
 from all the entanglement, all the bonds of suffering that

149
00:12:05,250 --> 00:12:09,320
 come from defilement.

150
00:12:09,320 --> 00:12:13,630
 And so there's much happiness to be had, the greatest

151
00:12:13,630 --> 00:12:16,080
 happiness, two very different kinds

152
00:12:16,080 --> 00:12:24,440
 of happiness, one is very easy, low hanging fruit, easy for

153
00:12:24,440 --> 00:12:28,100
 humans anyway, easy for I

154
00:12:28,100 --> 00:12:32,790
 guess most beings to just put a like in this to a leper, a

155
00:12:32,790 --> 00:12:35,360
 leper whose fingers or limbs

156
00:12:35,360 --> 00:12:39,040
 are falling off or they're getting scabs and they take

157
00:12:39,040 --> 00:12:41,920
 these, the ends of their limbs and

158
00:12:41,920 --> 00:12:49,480
 they cauterize them with flame, apparently that brings some

159
00:12:49,480 --> 00:12:50,880
 relief.

160
00:12:50,880 --> 00:12:55,640
 The Buddha said that's the sort of happiness, the sort of

161
00:12:55,640 --> 00:12:58,680
 sensual happiness, trying to find

162
00:12:58,680 --> 00:13:07,930
 relief like the leper, it's the desperate, it's the

163
00:13:07,930 --> 00:13:11,000
 desperate sort of happiness, craving

164
00:13:11,000 --> 00:13:12,000
 and clinging.

165
00:13:12,000 --> 00:13:15,650
 One of my meditators pointed out something quite funny once

166
00:13:15,650 --> 00:13:17,200
 and it summed it up quite

167
00:13:17,200 --> 00:13:22,400
 nicely, he said he was, he was, he was sweeping in his kuti

168
00:13:22,400 --> 00:13:25,800
 one day and there were two cockroaches

169
00:13:25,800 --> 00:13:29,890
 and so not wanting to hurt them, he started sweeping them

170
00:13:29,890 --> 00:13:31,760
 out and they started running

171
00:13:31,760 --> 00:13:35,410
 around as cockroaches do and he got them sort of together

172
00:13:35,410 --> 00:13:37,320
 and near the door and as soon

173
00:13:37,320 --> 00:13:40,370
 as they got really, really close together, they jumped, one

174
00:13:40,370 --> 00:13:41,760
 of them jumped on the other

175
00:13:41,760 --> 00:13:51,510
 and they started mating and he said that, that about sums

176
00:13:51,510 --> 00:13:54,720
 up the nature of craving,

177
00:13:54,720 --> 00:13:58,760
 life is short.

178
00:13:58,760 --> 00:14:01,230
 Let's see gratification while we can, I mean just the

179
00:14:01,230 --> 00:14:04,480
 animal instinct but it's in us all,

180
00:14:04,480 --> 00:14:08,440
 we're suffering so much that we grasp at anything, we're

181
00:14:08,440 --> 00:14:13,400
 grasping at straws or grasping at nothing,

182
00:14:13,400 --> 00:14:15,520
 grasping at anything.

183
00:14:15,520 --> 00:14:20,330
 But the other happiness is more complicated, more, not

184
00:14:20,330 --> 00:14:23,200
 complicated but more subtle, it

185
00:14:23,200 --> 00:14:28,750
 takes, it's more delicate, takes a more delicate approach,

186
00:14:28,750 --> 00:14:31,600
 more refined approach, it can't

187
00:14:31,600 --> 00:14:36,250
 just be grasped at, it has to be cultivated carefully, it

188
00:14:36,250 --> 00:14:40,320
 has everything to do with care,

189
00:14:40,320 --> 00:14:48,660
 patience, sophistication I suppose, a delicacy, refinement

190
00:14:48,660 --> 00:14:52,720
 and so it's subtle and profound,

191
00:14:52,720 --> 00:14:55,800
 it's lasting and it's stable and it's leading only to

192
00:14:55,800 --> 00:14:57,920
 greater and greater happiness.

193
00:14:57,920 --> 00:15:01,800
 This is the other thing is the easy happiness leads to less

194
00:15:01,800 --> 00:15:03,840
 and less happiness, that's how

195
00:15:03,840 --> 00:15:06,710
 the brain works, it's all caught up in the brain and

196
00:15:06,710 --> 00:15:08,680
 chemicals, it's how some sorrow

197
00:15:08,680 --> 00:15:12,020
 works you might say, the more you get what you want, the

198
00:15:12,020 --> 00:15:13,760
 more you want it and the less

199
00:15:13,760 --> 00:15:16,160
 satisfied you are with what you get.

200
00:15:16,160 --> 00:15:26,160
 It's the law of diminishing returns of karma or of karma,

201
00:15:26,160 --> 00:15:29,360
 of sensuality.

202
00:15:29,360 --> 00:15:31,660
 Whereas the happiness of the Dhamma, it may be very little

203
00:15:31,660 --> 00:15:32,900
 happiness at first because

204
00:15:32,900 --> 00:15:36,700
 there's so much confusion in the mind, so much defilement

205
00:15:36,700 --> 00:15:38,320
 that sitting still is not

206
00:15:38,320 --> 00:15:42,970
 very pleasant but the happiness increases over time, the

207
00:15:42,970 --> 00:15:45,340
 more you practice, the more

208
00:15:45,340 --> 00:15:50,550
 happiness, the more peace, the more freedom that comes to

209
00:15:50,550 --> 00:15:55,680
 very different types of happiness.

210
00:15:55,680 --> 00:16:00,040
 So congratulations and appreciation to everyone for

211
00:16:00,040 --> 00:16:02,880
 choosing this way which we consider to

212
00:16:02,880 --> 00:16:09,440
 be far far superior.

213
00:16:09,440 --> 00:16:12,820
 And thank you for coming out tonight, there's the Dhamma,

214
00:16:12,820 --> 00:16:14,720
 that's the Dhamma for this evening,

215
00:16:14,720 --> 00:16:20,480
 wishing you all good practice.

216
00:16:20,480 --> 00:16:44,120
 Okay, we got some questions here.

217
00:16:44,120 --> 00:16:47,100
 If there are no thoughts between breaths and sitting

218
00:16:47,100 --> 00:16:49,420
 meditation, I have started noting the

219
00:16:49,420 --> 00:16:53,780
 calm and empty states with just noting calm, calm and empty

220
00:16:53,780 --> 00:16:54,720
, empty.

221
00:16:54,720 --> 00:16:57,050
 After some time in meditation doing this, the thoughts seem

222
00:16:57,050 --> 00:16:58,080
 to come only as shapes and

223
00:16:58,080 --> 00:17:01,040
 colors later, it seems that the thoughts disappear

224
00:17:01,040 --> 00:17:02,000
 completely.

225
00:17:02,000 --> 00:17:06,310
 Should I especially notice these states or just going with

226
00:17:06,310 --> 00:17:10,880
 calm, calm and empty, empty?

227
00:17:10,880 --> 00:17:14,950
 Well between sitting, I mean you might do rising, falling

228
00:17:14,950 --> 00:17:17,100
 and then note sitting, we start

229
00:17:17,100 --> 00:17:21,040
 to make it more complex as you go through the course we'll

230
00:17:21,040 --> 00:17:23,480
 add sitting, so rising, falling,

231
00:17:23,480 --> 00:17:24,480
 sitting.

232
00:17:24,480 --> 00:17:27,510
 But it sounds like what you're doing is fine except when

233
00:17:27,510 --> 00:17:29,160
 you start to see things you should

234
00:17:29,160 --> 00:17:31,400
 say seeing, seeing.

235
00:17:31,400 --> 00:17:34,550
 When you feel completely calm or quiet you should note

236
00:17:34,550 --> 00:17:35,600
 quiet, quiet.

237
00:17:35,600 --> 00:17:38,180
 When the thoughts disappear you can actually just say

238
00:17:38,180 --> 00:17:40,160
 disappearing or knowing, knowing that

239
00:17:40,160 --> 00:17:41,560
 they're disappearing.

240
00:17:41,560 --> 00:17:45,120
 If you like it you would say liking, liking, but it sounds

241
00:17:45,120 --> 00:17:46,840
 like you're doing fine.

242
00:17:46,840 --> 00:17:54,720
 I'm worried too much about what you're doing.

243
00:17:54,720 --> 00:17:59,990
 If you came and did a meditation course it might sort of

244
00:17:59,990 --> 00:18:02,200
 make things clear.

245
00:18:02,200 --> 00:18:06,210
 Someone's re-asking a question, I must have been mean and

246
00:18:06,210 --> 00:18:08,600
 not answered it the first time.

247
00:18:08,600 --> 00:18:12,480
 I did the basic meditation course and now there are two

248
00:18:12,480 --> 00:18:14,760
 courses, I wonder with who?

249
00:18:14,760 --> 00:18:16,760
 With me?

250
00:18:16,760 --> 00:18:18,760
 Someone using a pseudonym.

251
00:18:18,760 --> 00:18:24,800
 I have a teacher I do reports to, okay so it's not me.

252
00:18:24,800 --> 00:18:28,200
 I would say 90% of the days in the year I don't meditate.

253
00:18:28,200 --> 00:18:32,920
 90% of the days in the year that's not very good.

254
00:18:32,920 --> 00:18:38,270
 90% of the years, 90% of the days in the year I don't med

255
00:18:38,270 --> 00:18:39,200
itate.

256
00:18:39,200 --> 00:18:41,080
 That's a lot.

257
00:18:41,080 --> 00:18:43,040
 I tried long all day meditations.

258
00:18:43,040 --> 00:18:45,460
 I tried doing very short meditations every five minutes and

259
00:18:45,460 --> 00:18:46,640
 I never persevere more than

260
00:18:46,640 --> 00:18:47,640
 a few days.

261
00:18:47,640 --> 00:18:53,120
 I'm really frustrated with this, can't find a solution.

262
00:18:53,120 --> 00:18:59,040
 Yeah that's interesting.

263
00:18:59,040 --> 00:19:01,120
 Maybe you should start meditating on your daily life, the

264
00:19:01,120 --> 00:19:02,480
 sorts of things that you're doing

265
00:19:02,480 --> 00:19:08,320
 that may be not conducive for meditation practice.

266
00:19:08,320 --> 00:19:11,040
 My answers are not going to give you the solution.

267
00:19:11,040 --> 00:19:13,710
 There's nothing I can say that's going to make it right for

268
00:19:13,710 --> 00:19:14,120
 you.

269
00:19:14,120 --> 00:19:17,250
 You just need someone to give you a kick in the pants or to

270
00:19:17,250 --> 00:19:18,840
 do it yourself and actually

271
00:19:18,840 --> 00:19:19,840
 begin to meditate.

272
00:19:19,840 --> 00:19:22,990
 Figure out what it is that you're doing that keeps you from

273
00:19:22,990 --> 00:19:23,920
 meditating.

274
00:19:23,920 --> 00:19:27,780
 Maybe there's a version to meditation.

275
00:19:27,780 --> 00:19:29,880
 Maybe you need to come and do a course with me, maybe I'll

276
00:19:29,880 --> 00:19:30,840
 be a better teacher.

277
00:19:30,840 --> 00:19:32,880
 Maybe your teachers are just really lousy.

278
00:19:32,880 --> 00:19:35,920
 I don't know.

279
00:19:35,920 --> 00:19:38,720
 I'm just teasing.

280
00:19:38,720 --> 00:19:43,560
 But there's nothing to do but just do it.

281
00:19:43,560 --> 00:19:46,280
 Nike, isn't that a Nike saying?

282
00:19:46,280 --> 00:19:50,960
 Nike get it right?

283
00:19:50,960 --> 00:19:53,920
 Can sensory deprivation tanks be used for meditation?

284
00:19:53,920 --> 00:19:57,860
 I've always found this sort of fascinating, that idea

285
00:19:57,860 --> 00:20:00,280
 because for us it's all about the

286
00:20:00,280 --> 00:20:01,280
 senses.

287
00:20:01,280 --> 00:20:05,200
 It's not really about shutting off the senses.

288
00:20:05,200 --> 00:20:07,600
 That's where all the problems come from.

289
00:20:07,600 --> 00:20:10,790
 Why would you want to deprive the senses when that's where

290
00:20:10,790 --> 00:20:13,280
 all your studying has to be done?

291
00:20:13,280 --> 00:20:17,160
 I don't know.

292
00:20:17,160 --> 00:20:21,640
 That should be filed under the speculative questions.

293
00:20:21,640 --> 00:20:23,800
 Nothing to do with our technique.

294
00:20:23,800 --> 00:20:31,800
 I certainly never tell people to do.

295
00:20:31,800 --> 00:20:39,790
 Certainly never tell people to enter into sensory

296
00:20:39,790 --> 00:20:42,520
 deprivation.

297
00:20:42,520 --> 00:20:44,640
 Since Buddhists do not believe in determinism, is it

298
00:20:44,640 --> 00:20:46,200
 correct to say that the future cannot

299
00:20:46,200 --> 00:20:50,170
 be predicted with one-in-a-person accuracy except for

300
00:20:50,170 --> 00:20:51,600
 certain events?

301
00:20:51,600 --> 00:20:56,450
 You're asking still on the level of determinism or free

302
00:20:56,450 --> 00:20:57,240
 will.

303
00:20:57,240 --> 00:20:59,360
 We don't work on that level.

304
00:20:59,360 --> 00:21:02,750
 That's not how Buddhism thinks for lack of a better word,

305
00:21:02,750 --> 00:21:04,120
 how Buddhism acts.

306
00:21:04,120 --> 00:21:07,880
 Buddhism is very much about here and now.

307
00:21:07,880 --> 00:21:10,700
 It's about learning the true nature of reality.

308
00:21:10,700 --> 00:21:14,410
 It's not about philosophizing about whether the future can

309
00:21:14,410 --> 00:21:16,040
 or can't be predicted.

310
00:21:16,040 --> 00:21:17,200
 That's not what Buddhism is about.

311
00:21:17,200 --> 00:21:22,510
 I'm going to cop out and say, "That's not what we deal with

312
00:21:22,510 --> 00:21:22,520
."

313
00:21:22,520 --> 00:21:26,000
 How do we sign up to take a course with you?

314
00:21:26,000 --> 00:21:30,720
 You can go to our website at syrimongolo.org.

315
00:21:30,720 --> 00:21:36,120
 Looks like someone's Robin has posted a link.

316
00:21:36,120 --> 00:21:38,520
 How long did it take you to learn and be fluent in Thai?

317
00:21:38,520 --> 00:21:42,200
 Now that's not a question for me, but whatever.

318
00:21:42,200 --> 00:21:44,400
 I'll indulge it because you're here in person.

319
00:21:44,400 --> 00:21:49,500
 I wouldn't indulge these sorts of questions because there's

320
00:21:49,500 --> 00:21:51,520
 just too many of them.

321
00:21:51,520 --> 00:21:53,480
 Questions that are not related to meditation.

322
00:21:53,480 --> 00:21:57,710
 It took me about a year to get fairly fluent in Thai, but I

323
00:21:57,710 --> 00:21:59,920
 was learning for six or seven

324
00:21:59,920 --> 00:22:05,080
 years and it really got improved in stages.

325
00:22:05,080 --> 00:22:09,480
 It got really good when I first learned to read.

326
00:22:09,480 --> 00:22:12,500
 Then I started taking the Thai Dhamma classes and I had to

327
00:22:12,500 --> 00:22:14,000
 take the exams in Thai, so I

328
00:22:14,000 --> 00:22:17,770
 had to write an essay, a two-page essay in Thai, which is a

329
00:22:17,770 --> 00:22:19,640
 lot more difficult than it

330
00:22:19,640 --> 00:22:20,640
 might sound.

331
00:22:20,640 --> 00:22:23,880
 Spelling is very difficult.

332
00:22:23,880 --> 00:22:25,440
 Then I started teaching people.

333
00:22:25,440 --> 00:22:30,000
 I was actually asked to teach meditation to Thai people.

334
00:22:30,000 --> 00:22:34,520
 Then I had to really learn how to get my concepts across.

335
00:22:34,520 --> 00:22:36,480
 Then I actually started giving talks in Thai and that

336
00:22:36,480 --> 00:22:38,920
 really got ... So yeah, it went in

337
00:22:38,920 --> 00:22:39,920
 stages.

338
00:22:39,920 --> 00:22:41,680
 It became quite fluent.

339
00:22:41,680 --> 00:22:46,640
 I guess I'm still fairly fluent.

340
00:22:46,640 --> 00:22:49,100
 Seeing some lay people use the word "sanga" isn't the word

341
00:22:49,100 --> 00:22:50,560
 reserved for monks, nuns, and

342
00:22:50,560 --> 00:22:51,560
 novices?

343
00:22:51,560 --> 00:22:55,200
 Well, there's two kinds of "sanga".

344
00:22:55,200 --> 00:22:59,140
 The "sanga" that you're thinking of is the "wineya" "sanga

345
00:22:59,140 --> 00:23:00,960
", which requires actually

346
00:23:00,960 --> 00:23:03,420
 four monks and up.

347
00:23:03,420 --> 00:23:04,680
 One monk is not a "sanga".

348
00:23:04,680 --> 00:23:07,450
 I once gave a talk and I was talking about the difference

349
00:23:07,450 --> 00:23:09,400
 and I said, "Any monk is "sanga",

350
00:23:09,400 --> 00:23:12,000
 that's only a "samuti" "sanga".

351
00:23:12,000 --> 00:23:14,840
 This layman here really ... What a jerk.

352
00:23:14,840 --> 00:23:17,910
 He came up to me later and he said, "I just wanted to talk

353
00:23:17,910 --> 00:23:18,600
 to you.

354
00:23:18,600 --> 00:23:19,680
 You were wrong."

355
00:23:19,680 --> 00:23:23,470
 This guy, Thai guy, he was a real ... He was very much full

356
00:23:23,470 --> 00:23:24,400
 of himself.

357
00:23:24,400 --> 00:23:26,760
 He came up to me and he had to correct me because I was

358
00:23:26,760 --> 00:23:27,280
 wrong.

359
00:23:27,280 --> 00:23:28,280
 He was right.

360
00:23:28,280 --> 00:23:29,680
 I was wrong.

361
00:23:29,680 --> 00:23:30,680
 One monk is not a "sanga".

362
00:23:30,680 --> 00:23:34,280
 It takes at least four monks to be a "sanga".

363
00:23:34,280 --> 00:23:38,420
 That is wrong in the sense that the "sanga" is the entire

364
00:23:38,420 --> 00:23:40,640
 monastic community and we all

365
00:23:40,640 --> 00:23:42,380
 belong to the "sanga".

366
00:23:42,380 --> 00:23:46,250
 One monk is a part of the "sanga", is what I was trying to

367
00:23:46,250 --> 00:23:46,880
 say.

368
00:23:46,880 --> 00:23:52,720
 "Sanga" in Thai they would even say, "A monk is "sanga".

369
00:23:52,720 --> 00:23:55,960
 It's like a soldier is military.

370
00:23:55,960 --> 00:23:58,200
 It doesn't mean that soldier is the military.

371
00:23:58,200 --> 00:24:00,400
 It means they are military.

372
00:24:00,400 --> 00:24:02,640
 But that's one kind.

373
00:24:02,640 --> 00:24:08,190
 The other kind, the "suta-sanga" is all those people who

374
00:24:08,190 --> 00:24:12,200
 are on the path to enlightenment.

375
00:24:12,200 --> 00:24:15,830
 Anyone who is already practicing the Buddha's teaching

376
00:24:15,830 --> 00:24:18,640
 could be considered "sanga" because

377
00:24:18,640 --> 00:24:21,160
 they are on the path to "sotapana".

378
00:24:21,160 --> 00:24:26,000
 They are doing the "pubanga-manga".

379
00:24:26,000 --> 00:24:31,480
 It's just a matter of how ... What level you mean the word

380
00:24:31,480 --> 00:24:32,360
 "sanga" because then there's

381
00:24:32,360 --> 00:24:33,360
 also "aryasanga".

382
00:24:33,360 --> 00:24:38,440
 "Aryasanga" is of course enlightened beings.

383
00:24:38,440 --> 00:24:45,990
 So "aryasanga" you have to actually have reached "manga-n

384
00:24:45,990 --> 00:24:47,160
ana".

385
00:24:47,160 --> 00:24:52,050
 That's the "savaka-sanga" that's talked about in the "sanga

386
00:24:52,050 --> 00:24:53,240
-nusati".

387
00:24:53,240 --> 00:24:54,600
 But a "sanga" is just a community.

388
00:24:54,600 --> 00:24:55,960
 The word just means community.

389
00:24:55,960 --> 00:25:00,370
 So it could also be used in a non-Buddhist sense, I suppose

390
00:25:00,370 --> 00:25:00,680
.

391
00:25:00,680 --> 00:25:04,000
 I don't know that it ever is used like that in the Pali.

392
00:25:04,000 --> 00:25:08,680
 I can't remember really.

393
00:25:08,680 --> 00:25:13,440
 Okay, well thank you everyone.

394
00:25:13,440 --> 00:25:14,040
 Have a good night.

395
00:25:14,040 --> 00:25:15,040
 God bless you.

396
00:25:15,040 --> 00:25:16,040
 God bless you.

397
00:25:16,040 --> 00:25:16,040
 God bless you.

398
00:25:16,040 --> 00:25:17,040
 God bless you.

399
00:25:17,040 --> 00:25:17,040
 God bless you.

400
00:25:17,040 --> 00:25:18,040
 God bless you.

