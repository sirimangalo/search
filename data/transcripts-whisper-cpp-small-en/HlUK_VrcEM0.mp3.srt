1
00:00:00,000 --> 00:00:05,160
 Hello everyone, just an update here to let you know that if

2
00:00:05,160 --> 00:00:06,480
 you haven't seen already,

3
00:00:06,480 --> 00:00:14,000
 I've shut down the Ask a Monk series for the winter,

4
00:00:14,000 --> 00:00:16,680
 meaning that you can still look at

5
00:00:16,680 --> 00:00:20,720
 the questions and answers, but you can't ask any more

6
00:00:20,720 --> 00:00:23,560
 questions because there's still quite

7
00:00:23,560 --> 00:00:28,150
 a few that I haven't answered and I'm hoping to get them

8
00:00:28,150 --> 00:00:31,560
 all finished, most of them finished

9
00:00:31,560 --> 00:00:38,160
 by the time I go to Thailand, with the exception of course

10
00:00:38,160 --> 00:00:41,800
 of the question about my story of

11
00:00:41,800 --> 00:00:47,680
 my monk, which honestly I don't think is all that exciting

12
00:00:47,680 --> 00:00:49,360
 anyway. It's going to be a little

13
00:00:49,360 --> 00:00:54,690
 bit difficult to tell because of course it involves a lot

14
00:00:54,690 --> 00:00:57,480
 of different people who still

15
00:00:57,480 --> 00:01:02,430
 might have some interest in what I do and so it's a balance

16
00:01:02,430 --> 00:01:04,640
 between getting the story

17
00:01:04,640 --> 00:01:11,390
 told and saying too much. You know, you have to involve

18
00:01:11,390 --> 00:01:12,920
 other people in the story so I

19
00:01:12,920 --> 00:01:19,040
 have to figure out how to tell it more in a dharmic sort of

20
00:01:19,040 --> 00:01:21,960
 way that teaches lessons

21
00:01:21,960 --> 00:01:27,820
 rather than just tells stories. So, okay, and the rest of

22
00:01:27,820 --> 00:01:30,440
 the questions I'll be answering,

23
00:01:30,440 --> 00:01:36,800
 I would hope, and then I'll be gone. I'm off to Thailand on

24
00:01:36,800 --> 00:01:39,680
 the 25th of October and hopefully

25
00:01:39,680 --> 00:01:47,700
 I'll be back around the 21st of December, heading off to

26
00:01:47,700 --> 00:01:51,120
 Sri Lanka for nine days or

27
00:01:51,120 --> 00:01:56,620
 ten days, I can't remember exactly nine days I think, to

28
00:01:56,620 --> 00:02:00,160
 see the country to attend a Buddhist

29
00:02:00,160 --> 00:02:03,950
 conference. They're having this yearly conference of some

30
00:02:03,950 --> 00:02:06,560
 world Buddhist fellowship or fellowship

31
00:02:06,560 --> 00:02:10,810
 of world Buddhists or something, but it's a chance also to

32
00:02:10,810 --> 00:02:12,880
 see the country and to, hey,

33
00:02:12,880 --> 00:02:18,070
 who knows, maybe one day I'll wind up living there. But I'm

34
00:02:18,070 --> 00:02:20,200
 still hoping that after that

35
00:02:20,200 --> 00:02:24,060
 I'll be back here in America to try to make it work here.

36
00:02:24,060 --> 00:02:26,520
 Hopefully we'll be moving closer

37
00:02:26,520 --> 00:02:29,960
 to the goal of opening a forest monastery. I know a little

38
00:02:29,960 --> 00:02:31,800
 bit of a disappointment there

39
00:02:31,800 --> 00:02:36,610
 when it turns out that there's not many people among my

40
00:02:36,610 --> 00:02:39,800
 supporters interested in a remote

41
00:02:39,800 --> 00:02:45,020
 forest monastery. So hopefully that does happen, otherwise

42
00:02:45,020 --> 00:02:47,720
 I'll be living here in the city,

43
00:02:47,720 --> 00:02:52,000
 which is fine too, as long as I'm in a place that I can go

44
00:02:52,000 --> 00:02:57,800
 for almsround. So what other

45
00:02:57,800 --> 00:03:05,670
 updates I was going to... Yeah, the other one is I've still

46
00:03:05,670 --> 00:03:09,760
 only got seven replies to my

47
00:03:09,760 --> 00:03:13,190
 request for videos, which is a little bit surprising. I

48
00:03:13,190 --> 00:03:16,160
 thought there'd be more of you out there.

49
00:03:16,160 --> 00:03:19,150
 That sounds like there's a few people who still intend to

50
00:03:19,150 --> 00:03:20,760
 make videos, but to the rest

51
00:03:20,760 --> 00:03:23,870
 of you, I mean, come on, there's hundreds of people who

52
00:03:23,870 --> 00:03:25,960
 watch these videos every day.

53
00:03:25,960 --> 00:03:29,460
 A thousand, over a thousand people watch my videos every

54
00:03:29,460 --> 00:03:31,640
 day, which is... I don't expect

55
00:03:31,640 --> 00:03:34,050
 most of them to be interested, but most of you have been

56
00:03:34,050 --> 00:03:35,680
 leaving comments about how great

57
00:03:35,680 --> 00:03:39,820
 this is. Come on, show me how great it is. Show us all. I

58
00:03:39,820 --> 00:03:41,320
 mean, the thing is not just

59
00:03:41,320 --> 00:03:44,180
 to show me, although that's appreciated, it's nice to see

60
00:03:44,180 --> 00:03:46,920
 that people are actually practicing.

61
00:03:46,920 --> 00:03:51,340
 The point is to show the world, not to show yourself, but

62
00:03:51,340 --> 00:03:53,640
 to show meditation, to show

63
00:03:53,640 --> 00:03:58,240
 that people are actually practicing, to show people that

64
00:03:58,240 --> 00:04:01,520
 this technique, this tool is something

65
00:04:01,520 --> 00:04:06,350
 that's of use and people are putting it to use. It's proof.

66
00:04:06,350 --> 00:04:09,560
 It's a proof of concept.

67
00:04:09,560 --> 00:04:13,320
 So yeah, and the video is still on my channel. It's going

68
00:04:13,320 --> 00:04:19,480
 to be my channel video. Or is it?

69
00:04:19,480 --> 00:04:22,490
 No, maybe I'll make this one my channel video, in which

70
00:04:22,490 --> 00:04:25,840
 case I'll put a link right here to

71
00:04:25,840 --> 00:04:29,280
 the other video so you can go and leave your video

72
00:04:29,280 --> 00:04:31,560
 responses to that video. If you leave

73
00:04:31,560 --> 00:04:34,330
 your video responses to this one, fine, but I'd rather you

74
00:04:34,330 --> 00:04:35,720
 leave them to the other one.

75
00:04:35,720 --> 00:04:40,370
 It explains what I'm looking for. And also that way they're

76
00:04:40,370 --> 00:04:42,680
 all in one place. So okay,

77
00:04:42,680 --> 00:04:46,380
 thanks everyone for your support, your comments, feedback,

78
00:04:46,380 --> 00:04:48,400
 and so on. Oh, the other thing is

79
00:04:48,400 --> 00:04:52,200
 I'm not obviously going to be checking my email and

80
00:04:52,200 --> 00:04:54,840
 comments while I'm away, but I'd

81
00:04:54,840 --> 00:05:00,740
 also ask that regardless. I don't leave questions in the

82
00:05:00,740 --> 00:05:03,240
 comments section. I think I'm not going

83
00:05:03,240 --> 00:05:06,320
 to be able to answer them anymore. Sometimes I do go in

84
00:05:06,320 --> 00:05:07,920
 there and try to answer. Sometimes

85
00:05:07,920 --> 00:05:10,600
 it's just random, which ones I happen to answer, but it's

86
00:05:10,600 --> 00:05:12,120
 really too much at this point. If

87
00:05:12,120 --> 00:05:15,530
 you really have a question, ask it on Ask a Monk when I get

88
00:05:15,530 --> 00:05:17,080
 back or send me a private

89
00:05:17,080 --> 00:05:20,290
 message when I get back. If you send it in the meantime, I

90
00:05:20,290 --> 00:05:21,920
 may just skim through it when

91
00:05:21,920 --> 00:05:25,450
 I get back because I'm sure I'll have lots and lots of

92
00:05:25,450 --> 00:05:27,680
 emails and stuff to answer when

93
00:05:27,680 --> 00:05:32,520
 I get back. But when I return, send me a private message.

94
00:05:32,520 --> 00:05:35,880
 Send me an email. While I'm away,

95
00:05:35,880 --> 00:05:40,250
 you can go crazy, watch all the old videos. There's a whole

96
00:05:40,250 --> 00:05:42,120
 bunch of Ask a Monk videos

97
00:05:42,120 --> 00:05:45,600
 up. There's videos on meditation. There's my second life

98
00:05:45,600 --> 00:05:47,440
 talks. There's a whole bunch

99
00:05:47,440 --> 00:05:51,760
 of old talks that I gave a while back, which could be

100
00:05:51,760 --> 00:05:54,800
 outdated. A lot of them weren't during

101
00:05:54,800 --> 00:05:58,300
 the time I was still relearning English because I'm coming

102
00:05:58,300 --> 00:06:00,360
 back from Asia where we don't speak

103
00:06:00,360 --> 00:06:05,740
 much English. In the English we do speak is quite informal

104
00:06:05,740 --> 00:06:08,200
 because we not only have to

105
00:06:08,200 --> 00:06:11,730
 deal with Thai people, but people from all over the world

106
00:06:11,730 --> 00:06:13,360
 who speak poor to little to

107
00:06:13,360 --> 00:06:17,920
 no English. But there are old talks and they're on my web

108
00:06:17,920 --> 00:06:19,360
 blog. If you go to my web blog,

109
00:06:19,360 --> 00:06:23,020
 you'll see an audio list. You can listen to those.

110
00:06:23,020 --> 00:06:24,320
 Otherwise, there's lots of stuff on

111
00:06:24,320 --> 00:06:29,720
 the web that has nothing to do with me. I encourage you to

112
00:06:29,720 --> 00:06:32,520
 check out and read and listen

113
00:06:32,520 --> 00:06:36,550
 to and watch whatever is up there. The other thing is if

114
00:06:36,550 --> 00:06:38,440
 you do watch my videos and if

115
00:06:38,440 --> 00:06:41,470
 you do like them, hit the like button because what I

116
00:06:41,470 --> 00:06:44,120
 figured out is that affects the popularity

117
00:06:44,120 --> 00:06:47,090
 and then YouTube puts them at the top of the list and more

118
00:06:47,090 --> 00:06:49,000
 people can watch them. So good

119
00:06:49,000 --> 00:06:51,410
 way if you want to support this work, make sure to hit the

120
00:06:51,410 --> 00:06:52,640
 like button, make sure to

121
00:06:52,640 --> 00:06:59,730
 subscribe. These kind of things increase the exposure

122
00:06:59,730 --> 00:07:03,440
 exponentially as I'm told.

123
00:07:03,440 --> 00:07:05,950
 So thanks a lot and keep practicing. I'm going to be away

124
00:07:05,950 --> 00:07:07,320
 in the forest for two months. I

125
00:07:07,320 --> 00:07:10,350
 expect some serious practice from everyone who's been

126
00:07:10,350 --> 00:07:12,880
 watching these videos. I'm using

127
00:07:12,880 --> 00:07:16,800
 a new mic here. I hope the sound is okay. This is the mic I

128
00:07:16,800 --> 00:07:19,000
'm going to be taking to Thailand

129
00:07:19,000 --> 00:07:23,080
 because it's on top of the camera and it means I can take

130
00:07:23,080 --> 00:07:25,800
 videos of my teacher and so on.

131
00:07:25,800 --> 00:07:29,530
 This is exciting. If you have, when I come back, you'll be

132
00:07:29,530 --> 00:07:31,360
 able to see videos of Thailand

133
00:07:31,360 --> 00:07:35,570
 and Sri Lanka, my teacher in the forest and so on. During

134
00:07:35,570 --> 00:07:37,120
 the time I'm not meditating,

135
00:07:37,120 --> 00:07:40,200
 you can be sure I'll be taking some videos. Okay, thanks.

136
00:07:40,200 --> 00:07:42,000
 That's all. Have a good night.

