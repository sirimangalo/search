1
00:00:00,000 --> 00:00:04,710
 Is Anatta something we can cultivate directly or does it

2
00:00:04,710 --> 00:00:05,760
 come about indirectly?

3
00:00:05,760 --> 00:00:13,280
 If directly, how does the self or ego not get involved?

4
00:00:13,280 --> 00:00:21,400
 Well, we don't use words like directly or indirectly, I

5
00:00:21,400 --> 00:00:22,560
 suppose.

6
00:00:22,560 --> 00:00:34,400
 And I think what you mean by that, do we actively work to

7
00:00:34,400 --> 00:00:36,120
 work on an intellectual level, I would

8
00:00:36,120 --> 00:00:39,360
 say, to bring that about?

9
00:00:39,360 --> 00:00:45,720
 Or do we do something else and the knowledge comes about?

10
00:00:45,720 --> 00:00:49,930
 I think it is indirectly, so I'm going to be able to avoid

11
00:00:49,930 --> 00:00:52,400
 your whole if directly clause.

12
00:00:52,400 --> 00:01:00,240
 Because I think it is indirectly.

13
00:01:00,240 --> 00:01:02,440
 You don't think about non-self.

14
00:01:02,440 --> 00:01:04,890
 The practice that we do, let's give a whole overview of the

15
00:01:04,890 --> 00:01:06,120
 practice that we do and then

16
00:01:06,120 --> 00:01:08,480
 you'll understand, I think.

17
00:01:08,480 --> 00:01:10,960
 Hopefully it will help anyway.

18
00:01:10,960 --> 00:01:14,370
 The practice that we follow is called Insight Meditation

19
00:01:14,370 --> 00:01:16,360
 based on the Four Foundations of

20
00:01:16,360 --> 00:01:17,960
 Mindfulness.

21
00:01:17,960 --> 00:01:20,560
 So there are two parts to the practice.

22
00:01:20,560 --> 00:01:24,120
 You're not practicing vipassana.

23
00:01:24,120 --> 00:01:30,260
 You can't practice vipassana in an absolute sense or in a

24
00:01:30,260 --> 00:01:32,320
 literal sense.

25
00:01:32,320 --> 00:01:34,160
 You practice mindfulness.

26
00:01:34,160 --> 00:01:37,980
 You practice looking at things.

27
00:01:37,980 --> 00:01:40,640
 You practice looking at things as they are.

28
00:01:40,640 --> 00:01:42,520
 And you practice seeing things as they are.

29
00:01:42,520 --> 00:01:46,160
 You practice what is called Pati Sati Mata.

30
00:01:46,160 --> 00:01:51,600
 This bear mindfulness of things specifically as they are.

31
00:01:51,600 --> 00:01:53,000
 Pati means specifically.

32
00:01:53,000 --> 00:01:55,600
 Mata means bear or just.

33
00:01:55,600 --> 00:01:57,440
 Sati means mindful.

34
00:01:57,440 --> 00:02:01,220
 So this one word that the Buddha uses to explain the

35
00:02:01,220 --> 00:02:04,200
 meaning of the word Sati in this context

36
00:02:04,200 --> 00:02:10,610
 means to see things just as they are and fully have that be

37
00:02:10,610 --> 00:02:12,960
 your full attention.

38
00:02:12,960 --> 00:02:18,550
 So this is what we try to cultivate, this state of full

39
00:02:18,550 --> 00:02:21,600
 awareness or this state of exact

40
00:02:21,600 --> 00:02:27,400
 awareness or exact remembrance of things as they are.

41
00:02:27,400 --> 00:02:30,480
 As a result of doing that based on the body, the feelings,

42
00:02:30,480 --> 00:02:32,160
 the mind and the dhammas, the

43
00:02:32,160 --> 00:02:34,770
 Four Foundations of Mindfulness, there arises knowledge of

44
00:02:34,770 --> 00:02:36,560
 impermanence, knowledge of suffering

45
00:02:36,560 --> 00:02:38,040
 and knowledge of non-self.

46
00:02:38,040 --> 00:02:39,040
 Why?

47
00:02:39,040 --> 00:02:41,320
 Because that's the nature of these things.

48
00:02:41,320 --> 00:02:42,880
 That's how these things are.

49
00:02:42,880 --> 00:02:46,930
 When we look at things and see things as they are, we can't

50
00:02:46,930 --> 00:02:49,960
 help but see their characteristics.

51
00:02:49,960 --> 00:02:53,460
 And their characteristics happen to be, you could say it's

52
00:02:53,460 --> 00:02:55,040
 just by chance or it's just

53
00:02:55,040 --> 00:02:58,270
 how the world's formed or whatever, happen to be they're

54
00:02:58,270 --> 00:03:00,560
 impermanent, they're unsatisfying

55
00:03:00,560 --> 00:03:03,560
 and they're uncontrollable or they're impermanent,

56
00:03:03,560 --> 00:03:05,160
 suffering and non-self.

57
00:03:05,160 --> 00:03:08,200
 And so that's where the idea, that's where non-self comes

58
00:03:08,200 --> 00:03:08,600
 in.

59
00:03:08,600 --> 00:03:13,310
 It comes about as a realization of seeing things as they

60
00:03:13,310 --> 00:03:14,000
 are.

61
00:03:14,000 --> 00:03:18,170
 Like our teacher said, when you see a tiger, you see it's

62
00:03:18,170 --> 00:03:19,100
 stripes.

63
00:03:19,100 --> 00:03:23,040
 You don't have to look at the tiger and see where it's

64
00:03:23,040 --> 00:03:25,160
 stripes or how do I find it's stripes

65
00:03:25,160 --> 00:03:27,920
 or does this tiger have stripes?

66
00:03:27,920 --> 00:03:32,160
 As soon as you see the tiger, you can't help but in seeing

67
00:03:32,160 --> 00:03:34,720
 the tiger to see it's stripes.

68
00:03:34,720 --> 00:03:42,360
 So the same goes by seeing the characteristics of reality.

69
00:03:42,360 --> 00:03:47,990
 You could still argue that any practice of meditation is e

70
00:03:47,990 --> 00:03:51,260
gotistical, is the ego getting

71
00:03:51,260 --> 00:03:55,520
 in the way because there is the intention to do it.

72
00:03:55,520 --> 00:03:58,460
 And so this goes back to the desire, it goes back to

73
00:03:58,460 --> 00:04:00,560
 forcing and there was even one monk

74
00:04:00,560 --> 00:04:03,550
 who went so far and he's quite a famous monk in Thailand

75
00:04:03,550 --> 00:04:06,640
 who said, "Mindfulness is non-self."

76
00:04:06,640 --> 00:04:15,880
 So the idea of developing it is therefore fallacious.

77
00:04:15,880 --> 00:04:18,990
 It's not proper for a person to try to develop mindfulness

78
00:04:18,990 --> 00:04:20,520
 because it's non-self.

79
00:04:20,520 --> 00:04:23,470
 This is clear from the Abhidhamma or from the Buddha's

80
00:04:23,470 --> 00:04:24,280
 teaching.

81
00:04:24,280 --> 00:04:25,320
 So you have a big problem there.

82
00:04:25,320 --> 00:04:30,150
 So his theory was just to have people sit near him and they

83
00:04:30,150 --> 00:04:33,160
'd become mindful by association

84
00:04:33,160 --> 00:04:36,480
 with the wise.

85
00:04:36,480 --> 00:04:41,560
 And there's kind of something to that in terms of things

86
00:04:41,560 --> 00:04:44,320
 arising by themselves, but it's

87
00:04:44,320 --> 00:04:47,200
 such a dangerous thing and I can't help but think that it's

88
00:04:47,200 --> 00:04:48,560
 going to lead his students

89
00:04:48,560 --> 00:04:52,950
 astray and cause them to be lazy and to waste a lot of time

90
00:04:52,950 --> 00:04:55,760
 to get very little results because

91
00:04:55,760 --> 00:04:58,980
 while they're sitting there next to him waiting for the

92
00:04:58,980 --> 00:05:01,200
 mindfulness to arise and waiting for

93
00:05:01,200 --> 00:05:06,910
 wisdom to arise, they could be actively developing it and

94
00:05:06,910 --> 00:05:10,680
 as a result getting very good results.

95
00:05:10,680 --> 00:05:13,720
 During the practice, you don't want to be pushing it.

96
00:05:13,720 --> 00:05:17,980
 You don't want to be actively forcing the mindfulness to

97
00:05:17,980 --> 00:05:21,040
 arise or so on, but there is

98
00:05:21,040 --> 00:05:26,880
 an inclination of mind that has to come about, an

99
00:05:26,880 --> 00:05:29,160
 investigation.

100
00:05:29,160 --> 00:05:35,090
 The mind has to make the choice that instead of following

101
00:05:35,090 --> 00:05:37,920
 concepts and ideas and judgments

102
00:05:37,920 --> 00:05:43,670
 and so on, instead it's going to follow the reality and

103
00:05:43,670 --> 00:05:46,240
 focus on the reality.

104
00:05:46,240 --> 00:05:49,530
 To say that there is no self and there is no soul, as I

105
00:05:49,530 --> 00:05:51,280
 said, is just to denounce those

106
00:05:51,280 --> 00:05:53,840
 things that have no bearing on reality.

107
00:05:53,840 --> 00:05:56,640
 Experience is not automatic.

108
00:05:56,640 --> 00:05:59,680
 It's not to say that there is not choice made.

109
00:05:59,680 --> 00:06:01,720
 There's in fact choice made at every moment.

110
00:06:01,720 --> 00:06:04,080
 We make a choice between many different things at every

111
00:06:04,080 --> 00:06:04,600
 moment.

112
00:06:04,600 --> 00:06:09,120
 The we is what the I is, is irrelevant.

113
00:06:09,120 --> 00:06:12,630
 The idea of whether it be a soul or a self or this or that,

114
00:06:12,630 --> 00:06:14,400
 these all have no basis.

115
00:06:14,400 --> 00:06:15,440
 It is what it is.

116
00:06:15,440 --> 00:06:18,430
 There is the experience and the experience is made up of

117
00:06:18,430 --> 00:06:19,160
 choices.

118
00:06:19,160 --> 00:06:22,270
 If we make a choice to judge something, then the choice of

119
00:06:22,270 --> 00:06:23,440
 judgment arises.

120
00:06:23,440 --> 00:06:26,070
 If there is the choice of seeing things as they are, then

121
00:06:26,070 --> 00:06:27,240
 there is that choice.

122
00:06:27,240 --> 00:06:31,540
 If you don't take that choice, then the habits in the mind

123
00:06:31,540 --> 00:06:33,880
 are going to lead you or you're

124
00:06:33,880 --> 00:06:35,480
 making a choice anyway.

125
00:06:35,480 --> 00:06:41,000
 You make a choice when you choose to do nothing.

126
00:06:41,000 --> 00:06:44,460
 If you were to make the argument that the ego gets in the

127
00:06:44,460 --> 00:06:46,600
 way every time you meditate,

128
00:06:46,600 --> 00:06:50,970
 then it's a misunderstanding of what we mean by non-

129
00:06:50,970 --> 00:06:52,680
existent of ego.

130
00:06:52,680 --> 00:06:57,180
 We don't mean that there is no choice and no effort to be

131
00:06:57,180 --> 00:06:59,440
 made in the present moment

132
00:06:59,440 --> 00:07:00,900
 of any kind.

133
00:07:00,900 --> 00:07:06,120
 What we mean is that things like ego and self and me and

134
00:07:06,120 --> 00:07:09,080
 mind don't have any place in reality

135
00:07:09,080 --> 00:07:11,000
 because there are no entities.

136
00:07:11,000 --> 00:07:12,700
 There is only experience.

137
00:07:12,700 --> 00:07:14,320
 That experience is very personal.

138
00:07:14,320 --> 00:07:18,560
 It is very individual.

139
00:07:18,560 --> 00:07:20,800
 There are moment to moment choices.

140
00:07:20,800 --> 00:07:23,410
 At any moment you can verify this by saying, "Now I'm going

141
00:07:23,410 --> 00:07:24,680
 to make a choice," and choosing

142
00:07:24,680 --> 00:07:25,680
 something.

143
00:07:25,680 --> 00:07:28,920
 At any moment, especially when you practice meditation, you

144
00:07:28,920 --> 00:07:30,300
 can see that in fact we are

145
00:07:30,300 --> 00:07:31,300
 quite empowered.

146
00:07:31,300 --> 00:07:37,610
 That at every moment we can make a choice to be mindful or

147
00:07:37,610 --> 00:07:39,080
 to cling.

148
00:07:39,080 --> 00:07:47,420
 I would like to point out something about how your question

149
00:07:47,420 --> 00:07:48,500
 is put.

150
00:07:48,500 --> 00:07:54,320
 Is Anatta something we can cultivate?

151
00:07:54,320 --> 00:08:04,230
 Anatta in itself cannot be said directly or indirectly

152
00:08:04,230 --> 00:08:09,280
 because it's neither nor.

153
00:08:09,280 --> 00:08:13,800
 Anatta is not something that we can cultivate.

154
00:08:13,800 --> 00:08:19,430
 Anatta is a state of mind or is an insight that arises in

155
00:08:19,430 --> 00:08:23,000
 your mind that you can understand,

156
00:08:23,000 --> 00:08:30,980
 you can see through, you can understand that something is

157
00:08:30,980 --> 00:08:34,960
 out of control without being

158
00:08:34,960 --> 00:08:38,400
 possible to be controlled.

159
00:08:38,400 --> 00:08:43,780
 That there is no such thing within yourself or outside

160
00:08:43,780 --> 00:08:47,600
 yourself that could be controlling

161
00:08:47,600 --> 00:08:51,080
 anything.

162
00:08:51,080 --> 00:08:59,540
 The way you write your question is already, I don't want to

163
00:08:59,540 --> 00:09:03,800
 say wrong, but kind of wrong

164
00:09:03,800 --> 00:09:07,880
 because it's misleading.

165
00:09:07,880 --> 00:09:13,560
 It's coming from the point that Anatta is something, the

166
00:09:13,560 --> 00:09:15,880
 opposite of Ata, and it is

167
00:09:15,880 --> 00:09:18,280
 not that.

168
00:09:18,280 --> 00:09:24,480
 It's giving up the idea of Ata.

169
00:09:24,480 --> 00:09:28,030
 Just like impermanence is not a thing that exists, it's

170
00:09:28,030 --> 00:09:29,960
 giving up the idea that there

171
00:09:29,960 --> 00:09:32,480
 is permanent things that exist.

172
00:09:32,480 --> 00:09:36,080
 You see that there is no permanent things that exist.

173
00:09:36,080 --> 00:09:40,390
 And suffering is giving up the idea of finding satisfaction

174
00:09:40,390 --> 00:09:40,680
.

175
00:09:40,680 --> 00:09:43,960
 So you can't really cultivate it.

176
00:09:43,960 --> 00:09:47,480
 What you can do is you can cultivate your mind, you can

177
00:09:47,480 --> 00:09:49,520
 practice mindfulness and you

178
00:09:49,520 --> 00:09:55,880
 can do vipassana meditation and understand Anatta.

179
00:09:55,880 --> 00:09:57,800
 Let go of Ata.

180
00:09:57,800 --> 00:10:01,080
 In the end, if you want to understand Buddhism simply, as

181
00:10:01,080 --> 00:10:03,360
 the Buddha said, it's quite simple,

182
00:10:03,360 --> 00:10:05,240
 giving up clinging.

183
00:10:05,240 --> 00:10:07,230
 What are these things people always wonder, what are these

184
00:10:07,230 --> 00:10:08,360
 things, impermanent suffering

185
00:10:08,360 --> 00:10:11,480
 and non-self, when am I going to see them?

186
00:10:11,480 --> 00:10:13,120
 They're letting go.

187
00:10:13,120 --> 00:10:15,650
 Where when you see this stuff is garbage, it's useless, it

188
00:10:15,650 --> 00:10:16,780
's meaningless, it has no

189
00:10:16,780 --> 00:10:17,780
 benefit to me.

190
00:10:17,780 --> 00:10:22,050
 There's nothing in the world that when I cling to it is

191
00:10:22,050 --> 00:10:24,720
 going to bring me happiness.

192
00:10:24,720 --> 00:10:30,080
 It's so simple that we miss it and we want to read more.

193
00:10:30,080 --> 00:10:31,080
 Where's the real stuff?

194
00:10:31,080 --> 00:10:33,350
 And start reading the Buddha's books and these huge books

195
00:10:33,350 --> 00:10:34,720
 and all of the Buddha's teaching

196
00:10:34,720 --> 00:10:38,440
 that is gathered together, trying to find the essence.

197
00:10:38,440 --> 00:10:39,440
 And you miss it.

198
00:10:39,440 --> 00:10:42,240
 The Buddha said it on the first page, Four Noble Truths.

199
00:10:42,240 --> 00:10:45,570
 This suffering is to be understood and the cause of

200
00:10:45,570 --> 00:10:47,760
 suffering is to be abandoned.

201
00:10:47,760 --> 00:10:49,570
 So all you have to do is look at things and see that they

202
00:10:49,570 --> 00:10:50,280
're suffering.

203
00:10:50,280 --> 00:10:54,080
 Once you see that they're suffering, you let go of them.

204
00:10:54,080 --> 00:10:58,430
 That letting go is the realization of impermanent suffering

205
00:10:58,430 --> 00:10:59,680
 and non-self.

206
00:10:59,680 --> 00:11:02,250
 It's a realization of a lot of things and the Buddha just

207
00:11:02,250 --> 00:11:03,560
 gave the salient points, these

208
00:11:03,560 --> 00:11:05,160
 impermanent suffering and non-self.

209
00:11:05,160 --> 00:11:07,760
 It's a reason realizing that things are not beautiful.

210
00:11:07,760 --> 00:11:11,640
 It's a realizing that things are not stable, that things

211
00:11:11,640 --> 00:11:13,960
 are not controllable, that they're

212
00:11:13,960 --> 00:11:19,960
 not maintainable.

213
00:11:19,960 --> 00:11:27,600
 All of the, they're not reliable or they're not meaningful.

214
00:11:27,600 --> 00:11:28,600
 So many different things.

215
00:11:28,600 --> 00:11:32,080
 It's a realization that they're useless basically.

216
00:11:32,080 --> 00:11:36,720
 The Buddha says, "Sabh, sabh, damma, na, lang, abhiney, v

217
00:11:36,720 --> 00:11:37,600
aisaya."

218
00:11:37,600 --> 00:11:39,000
 This is the first thing.

219
00:11:39,000 --> 00:11:40,870
 All you need to know to start practicing the Buddha's

220
00:11:40,870 --> 00:11:42,680
 teaching, to start practicing mindfulness

221
00:11:42,680 --> 00:11:46,600
 is that no dhammas are worth clinging to.

222
00:11:46,600 --> 00:11:50,840
 That's enough, that's enough theory to get you started on

223
00:11:50,840 --> 00:11:52,160
 the practice.

224
00:11:52,160 --> 00:11:53,160
 Thank you.

225
00:11:53,160 --> 00:12:20,080
 [

