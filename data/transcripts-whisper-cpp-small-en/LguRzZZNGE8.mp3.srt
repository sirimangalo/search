1
00:00:00,000 --> 00:00:06,320
 Okay, whenever I attempt to meditate I get distracted very

2
00:00:06,320 --> 00:00:08,200
 easily about five to

3
00:00:08,200 --> 00:00:11,770
 ten minutes in and I feel that I've gained nothing. How do

4
00:00:11,770 --> 00:00:12,780
 I train myself to

5
00:00:12,780 --> 00:00:23,080
 focus? Don't train yourself to focus. Don't train yourself

6
00:00:23,080 --> 00:00:24,560
 to focus.

7
00:00:24,560 --> 00:00:34,440
 Train yourself to focus on first the disappointment, that

8
00:00:34,440 --> 00:00:34,920
 having gained

9
00:00:34,920 --> 00:00:40,450
 nothing because that's clinging, wanting something. You

10
00:00:40,450 --> 00:00:41,520
 want to gain something.

11
00:00:41,520 --> 00:00:47,960
 It's just consumerism and capitalism. You put your capital

12
00:00:47,960 --> 00:00:48,240
 in, you

13
00:00:48,240 --> 00:00:51,450
 should get something out of it. It's kind of frustrating

14
00:00:51,450 --> 00:00:52,960
 now and you realize that

15
00:00:52,960 --> 00:00:57,390
 meditation doesn't give you anything. It gives you nothing.

16
00:00:57,390 --> 00:00:59,480
 All this work you put

17
00:00:59,480 --> 00:01:03,180
 in you get nothing out of it. So focus on that. Focus on

18
00:01:03,180 --> 00:01:05,520
 this goal-based mind,

19
00:01:05,520 --> 00:01:14,180
 this results-based mind, the deadlines and quotas that we

20
00:01:14,180 --> 00:01:16,320
're taught to. The

21
00:01:16,320 --> 00:01:22,320
 bottom line. Focus on that. This greed, this frustration

22
00:01:22,320 --> 00:01:23,920
 and so on. Then once

23
00:01:23,920 --> 00:01:26,170
 you've overcome that then you can start to focus on the

24
00:01:26,170 --> 00:01:27,200
 distraction and learn

25
00:01:27,200 --> 00:01:29,350
 about the distraction. By the time you do that then your

26
00:01:29,350 --> 00:01:30,280
 meditation should get

27
00:01:30,280 --> 00:01:38,680
 better because you're actually learning a lot. During those

28
00:01:38,680 --> 00:01:39,800
 five and ten minutes

29
00:01:39,800 --> 00:01:42,060
 of distraction you're learning a lot. You're learning how

30
00:01:42,060 --> 00:01:42,760
 your mind works.

31
00:01:42,760 --> 00:01:45,960
 You're learning that it's not into your control. You're

32
00:01:45,960 --> 00:01:46,720
 learning three things,

33
00:01:46,720 --> 00:01:50,260
 that it's impermanent, it's suffering and it's non-self. It

34
00:01:50,260 --> 00:01:51,760
 changes its subject,

35
00:01:51,760 --> 00:01:55,680
 it's unstable, it's unsatisfying and it's uncontrollable.

36
00:01:55,680 --> 00:01:57,400
 That's the nature of your

37
00:01:57,400 --> 00:01:59,420
 mind and the more you practice the more you'll see that.

38
00:01:59,420 --> 00:02:00,560
 After you practice for

39
00:02:00,560 --> 00:02:03,330
 five to ten minutes you'll actually be able to let go of

40
00:02:03,330 --> 00:02:04,760
 things a lot better or

41
00:02:04,760 --> 00:02:08,360
 a little bit better, five to ten minutes better, five to

42
00:02:08,360 --> 00:02:11,520
 ten minutes worth. Your

43
00:02:11,520 --> 00:02:14,810
 mind will be less inclined to try to force things because

44
00:02:14,810 --> 00:02:15,520
 you will see

45
00:02:15,520 --> 00:02:20,000
 that your mind is not subject to being forced. You'll see

46
00:02:20,000 --> 00:02:21,360
 that you can't control

47
00:02:21,360 --> 00:02:24,220
 your mind. You'll be more inclined to think, you'll be more

48
00:02:24,220 --> 00:02:25,440
 inclined to let

49
00:02:25,440 --> 00:02:28,640
 your lesson inclined to make your mind wander, allow your

50
00:02:28,640 --> 00:02:30,360
 mind to wander, less

51
00:02:30,360 --> 00:02:34,700
 inclined to daydream and so on because now you see it just

52
00:02:34,700 --> 00:02:36,400
 winds you up and

53
00:02:36,400 --> 00:02:42,480
 gets you upset and leads to suffering.

