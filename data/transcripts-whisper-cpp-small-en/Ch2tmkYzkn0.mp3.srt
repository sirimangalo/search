1
00:00:00,000 --> 00:00:06,160
 Okay, good evening everyone.

2
00:00:06,160 --> 00:00:12,560
 We're broadcasting live October 20th, 2015.

3
00:00:12,560 --> 00:00:13,960
 Tonight we're not going to do a Dhammapada.

4
00:00:13,960 --> 00:00:18,310
 I'm going to ask to do a short session tonight because I

5
00:00:18,310 --> 00:00:22,600
 have an exam tomorrow and various

6
00:00:22,600 --> 00:00:24,080
 things going on this week.

7
00:00:24,080 --> 00:00:27,600
 So just wasn't able to get around to it.

8
00:00:27,600 --> 00:00:31,210
 Today was a big day as well and wasn't able to get around

9
00:00:31,210 --> 00:00:34,360
 to study for the Dhammapada.

10
00:00:34,360 --> 00:00:38,400
 Takes at least a little bit of preparation.

11
00:00:38,400 --> 00:00:41,920
 So hopefully we'll do that tomorrow instead.

12
00:00:41,920 --> 00:00:45,520
 Tonight if there are any questions, I'll answer questions.

13
00:00:45,520 --> 00:00:48,280
 Otherwise, well, let's start with that.

14
00:00:48,280 --> 00:00:49,280
 Are there any questions?

15
00:00:49,280 --> 00:00:52,280
 Yes, I'm sure there are.

16
00:00:52,280 --> 00:00:59,040
 I just have to see where they start.

17
00:00:59,040 --> 00:01:04,800
 Hello, Bonté.

18
00:01:04,800 --> 00:01:07,270
 Sometimes in meditation, I don't know if I'm forcing myself

19
00:01:07,270 --> 00:01:08,360
 or if I'm just trying to be

20
00:01:08,360 --> 00:01:10,240
 sincere in the practice.

21
00:01:10,240 --> 00:01:13,040
 And when I let go of many thoughts, when I let go, many

22
00:01:13,040 --> 00:01:15,040
 thoughts come to my mind, although

23
00:01:15,040 --> 00:01:17,200
 I try to be mindful about them.

24
00:01:17,200 --> 00:01:20,350
 But I feel a bit lazy and feeling that I am not sincere

25
00:01:20,350 --> 00:01:22,000
 enough in my practice.

26
00:01:22,000 --> 00:01:24,000
 Can you help me with this complex?

27
00:01:24,000 --> 00:01:25,000
 Thanks.

28
00:01:25,000 --> 00:01:40,380
 Well, the best entry point there is feeling lazy or judging

29
00:01:40,380 --> 00:01:41,800
 yourself as lazy.

30
00:01:41,800 --> 00:01:44,040
 However, it actually appears to you.

31
00:01:44,040 --> 00:01:47,120
 If you really do have a feeling of lethargy, then you

32
00:01:47,120 --> 00:01:48,860
 should acknowledge that.

33
00:01:48,860 --> 00:01:52,530
 If you have just a guilty feeling like, oh, I'm being lazy,

34
00:01:52,530 --> 00:01:54,120
 then that's a judgment.

35
00:01:54,120 --> 00:01:58,020
 And you should acknowledge that, like judging or thinking

36
00:01:58,020 --> 00:02:00,400
 or disliking or however it appears

37
00:02:00,400 --> 00:02:03,680
 to you.

38
00:02:03,680 --> 00:02:10,200
 But trying to be sincere is usually problematic.

39
00:02:10,200 --> 00:02:12,960
 I mean, you either are or you aren't.

40
00:02:12,960 --> 00:02:15,120
 It's not about trying.

41
00:02:15,120 --> 00:02:17,260
 Yoda said, "Do or do not.

42
00:02:17,260 --> 00:02:19,480
 There is no try."

43
00:02:19,480 --> 00:02:22,790
 I mean, on some level you are trying, and trying is

44
00:02:22,790 --> 00:02:23,800
 important.

45
00:02:23,800 --> 00:02:27,240
 It's important to keep trying and never give up.

46
00:02:27,240 --> 00:02:30,500
 But as you practice, you'll start to realize that it's not

47
00:02:30,500 --> 00:02:31,680
 exactly a trying.

48
00:02:31,680 --> 00:02:33,680
 It's just that, okay, now I'm mindful.

49
00:02:33,680 --> 00:02:34,680
 I'm mindful.

50
00:02:34,680 --> 00:02:35,680
 Again, I'm mindful.

51
00:02:35,680 --> 00:02:36,680
 Again, I'm mindful.

52
00:02:36,680 --> 00:02:42,030
 It's not a sense of being sincere or pushing, in the sense

53
00:02:42,030 --> 00:02:45,300
 of cultivating something, because

54
00:02:45,300 --> 00:02:48,960
 that becomes a sankara, in a sense.

55
00:02:48,960 --> 00:02:50,600
 It's a formation.

56
00:02:50,600 --> 00:02:54,510
 You start to have an artificial formation arise, which we

57
00:02:54,510 --> 00:02:55,480
 don't want.

58
00:02:55,480 --> 00:02:59,400
 We want to be natural.

59
00:02:59,400 --> 00:03:02,060
 The effort that you put out is just that one moment, to be

60
00:03:02,060 --> 00:03:03,480
 mindful in one moment and then

61
00:03:03,480 --> 00:03:09,800
 again in another moment.

62
00:03:09,800 --> 00:03:12,910
 Focus more on your judgment of thinking you're lazy,

63
00:03:12,910 --> 00:03:15,200
 because it sounds like that's okay if

64
00:03:15,200 --> 00:03:18,800
 you let the thoughts come to the mind.

65
00:03:18,800 --> 00:03:19,800
 That's fine.

66
00:03:19,800 --> 00:03:22,960
 They're coming not because you want them to.

67
00:03:22,960 --> 00:03:26,880
 You just have to catch them when they do come.

68
00:03:26,880 --> 00:03:29,740
 Once you realize that you're thinking, they're thinking,

69
00:03:29,740 --> 00:03:31,520
 eventually you'll catch it quicker

70
00:03:31,520 --> 00:03:36,800
 and quicker.

71
00:03:36,800 --> 00:03:38,720
 Question for anyone who can answer it.

72
00:03:38,720 --> 00:03:41,720
 It seems as though the number of minutes in the meditation

73
00:03:41,720 --> 00:03:43,300
 log decreases gradually.

74
00:03:43,300 --> 00:03:46,080
 Is there a time limit on how long your minutes of

75
00:03:46,080 --> 00:03:47,920
 meditation stay in the log?

76
00:03:47,920 --> 00:03:53,120
 I think a week.

77
00:03:53,120 --> 00:03:56,680
 I think your personal meditation log is for the past week.

78
00:03:56,680 --> 00:03:59,000
 Don't quote me on that, but there is a time limit.

79
00:03:59,000 --> 00:04:00,000
 Yes.

80
00:04:00,000 --> 00:04:03,440
 It's not giving you your lifetime meditation.

81
00:04:03,440 --> 00:04:07,480
 It's just giving you the past week, I think.

82
00:04:07,480 --> 00:04:11,920
 Yeah, that's whatever.

83
00:04:11,920 --> 00:04:15,800
 It's just a simple log.

84
00:04:15,800 --> 00:04:22,140
 We need a professional to make this site look all beautiful

85
00:04:22,140 --> 00:04:23,680
 and jQuery it up a bit.

86
00:04:23,680 --> 00:04:26,520
 Or he jacks it up.

87
00:04:26,520 --> 00:04:29,960
 I don't know what you'd use.

88
00:04:29,960 --> 00:04:32,000
 Stylish it up.

89
00:04:32,000 --> 00:04:34,960
 We're supposed to be boring.

90
00:04:34,960 --> 00:04:35,960
 We're meditators.

91
00:04:35,960 --> 00:04:36,960
 That's true.

92
00:04:36,960 --> 00:04:39,960
 Good that it's kind of boring and uninteresting.

93
00:04:39,960 --> 00:04:43,640
 Yeah, so we don't want to be distracted from meditation.

94
00:04:43,640 --> 00:04:46,080
 It's a bit of a mess, really.

95
00:04:46,080 --> 00:04:50,160
 It looks like a patchwork robe.

96
00:04:50,160 --> 00:04:53,790
 Looks like a monk picked up pieces off the floor and sewed

97
00:04:53,790 --> 00:04:55,080
 them together.

98
00:04:55,080 --> 00:04:57,880
 As I recall, you put this together pretty quickly.

99
00:04:57,880 --> 00:04:58,880
 Considering.

100
00:04:58,880 --> 00:04:59,880
 Considering.

101
00:04:59,880 --> 00:05:02,880
 It's very functional.

102
00:05:02,880 --> 00:05:06,430
 Bonté, I was wondering if I have started a habit in

103
00:05:06,430 --> 00:05:08,880
 meditation that is incorrect.

104
00:05:08,880 --> 00:05:11,800
 When I turn my awareness specifically to thoughts, they

105
00:05:11,800 --> 00:05:13,640
 disappear as soon as I note them.

106
00:05:13,640 --> 00:05:17,070
 I feel like this is sort of like cultivating aversion, but

107
00:05:17,070 --> 00:05:18,040
 I'm not sure.

108
00:05:18,040 --> 00:05:22,040
 I've always known to simply observe them, not get them to

109
00:05:22,040 --> 00:05:22,880
 go away.

110
00:05:22,880 --> 00:05:28,680
 No, it's not getting them to go away, but that's clear.

111
00:05:28,680 --> 00:05:31,480
 They do go away.

112
00:05:31,480 --> 00:05:33,360
 Your intention is not to make them go away.

113
00:05:33,360 --> 00:05:36,360
 Your intention is to remind yourself that's a thought.

114
00:05:36,360 --> 00:05:39,520
 Unfortunately, no, of course, when you remind yourself that

115
00:05:39,520 --> 00:05:41,120
's a thought, you're thinking

116
00:05:41,120 --> 00:05:43,320
 something new, and so you've broken the chain.

117
00:05:43,320 --> 00:05:44,320
 That's normal.

118
00:05:44,320 --> 00:05:52,820
 Aversion is something entirely different.

119
00:05:52,820 --> 00:05:54,920
 Can you explain today's quote?

120
00:05:54,920 --> 00:05:59,800
 The quote is, "Of the tree in whose shade one sits or lies,

121
00:05:59,800 --> 00:06:00,600
 not a branch of it should

122
00:06:00,600 --> 00:06:01,600
 he break.

123
00:06:01,600 --> 00:06:05,400
 For if he did, he would be a betrayer of a friend, an ev

124
00:06:05,400 --> 00:06:06,320
ildoer."

125
00:06:06,320 --> 00:06:07,320
 I recognize this one.

126
00:06:07,320 --> 00:06:10,080
 It is from the Jataka.

127
00:06:10,080 --> 00:06:12,200
 Let's find the good.

128
00:06:12,200 --> 00:06:14,850
 There's a really good English translation, if I remember

129
00:06:14,850 --> 00:06:15,520
 correctly.

130
00:06:15,520 --> 00:06:28,360
 Except it's got the wrong numbers.

131
00:06:28,360 --> 00:06:32,240
 493.

132
00:06:32,240 --> 00:06:51,160
 Which one is it in the name?

133
00:06:51,160 --> 00:06:56,160
 It's called the Maha Uanija.

134
00:06:56,160 --> 00:07:21,200
 There we are.

135
00:07:21,200 --> 00:07:25,160
 The tree that gives you pleasant shade to sit or lie at

136
00:07:25,160 --> 00:07:25,840
 need.

137
00:07:25,840 --> 00:07:27,720
 You should not tear its branches down.

138
00:07:27,720 --> 00:07:33,560
 A cruel want, indeed.

139
00:07:33,560 --> 00:07:40,120
 It's funny, really.

140
00:07:40,120 --> 00:07:55,320
 But it's a weird verse, really.

141
00:07:55,320 --> 00:08:01,120
 I assumed it was a metaphoric, right?

142
00:08:01,120 --> 00:08:02,680
 It evades me.

143
00:08:02,680 --> 00:08:03,680
 It eludes me.

144
00:08:03,680 --> 00:08:12,880
 There's something about how this relates to a good friend.

145
00:08:12,880 --> 00:08:16,740
 There's some story of this man, someone who betrays a

146
00:08:16,740 --> 00:08:20,560
 friend, and then it's compared to

147
00:08:20,560 --> 00:08:36,360
 cutting the branch off a tree that shades you.

148
00:08:36,360 --> 00:08:37,360
 I think it's a metaphor.

149
00:08:37,360 --> 00:08:40,760
 The Vajataka doesn't look like it's actually metaphorical.

150
00:08:40,760 --> 00:08:50,510
 But the idea is you don't betray a friend or someone who

151
00:08:50,510 --> 00:08:55,400
 has been helpful to you.

152
00:08:55,400 --> 00:08:58,480
 Are the three marks conceptual?

153
00:08:58,480 --> 00:09:00,440
 No.

154
00:09:00,440 --> 00:09:14,480
 They are characteristics of reality.

155
00:09:14,480 --> 00:09:16,760
 We're caught up on questions for the moment.

156
00:09:16,760 --> 00:09:20,680
 Could I talk about offering you a robe?

157
00:09:20,680 --> 00:09:22,200
 Sure.

158
00:09:22,200 --> 00:09:24,180
 Okay.

159
00:09:24,180 --> 00:09:28,850
 This is the time of year when the rain's retreat ends and

160
00:09:28,850 --> 00:09:31,960
 typically people will offer a robe

161
00:09:31,960 --> 00:09:36,460
 to a monk at their local monastery.

162
00:09:36,460 --> 00:09:39,530
 Not long ago, not long enough ago, just about a week ago,

163
00:09:39,530 --> 00:09:41,280
 it occurred to me that maybe our

164
00:09:41,280 --> 00:09:44,860
 internet community could find a way to offer Venerable Yut

165
00:09:44,860 --> 00:09:45,900
odama a robe.

166
00:09:45,900 --> 00:09:50,490
 It's a little complicated since we're kind of spread out

167
00:09:50,490 --> 00:09:52,280
 around the world.

168
00:09:52,280 --> 00:09:55,800
 Little complicated, but we got a way to figure it out.

169
00:09:55,800 --> 00:10:02,600
 I've been in touch with a person in Thailand who is placing

170
00:10:02,600 --> 00:10:05,560
 an order for a robe, and I

171
00:10:05,560 --> 00:10:08,590
 have an opportunity for people to share in the merit of

172
00:10:08,590 --> 00:10:09,120
 this.

173
00:10:09,120 --> 00:10:11,840
 I just posted it into the link there.

174
00:10:11,840 --> 00:10:15,220
 If anyone would like to share in the merit of this, please

175
00:10:15,220 --> 00:10:15,600
 do.

176
00:10:15,600 --> 00:10:19,120
 What actually happened was a couple of things.

177
00:10:19,120 --> 00:10:23,260
 I wasn't actually sure of the cost of the robe because it

178
00:10:23,260 --> 00:10:25,800
 is in Bhat, and I'm not very

179
00:10:25,800 --> 00:10:29,440
 knowledgeable about that, but I have a rough estimate.

180
00:10:29,440 --> 00:10:33,590
 But I have a plan for everything that is donated over and

181
00:10:33,590 --> 00:10:36,600
 above the actual cost of the robe.

182
00:10:36,600 --> 00:10:42,150
 That is before Venerable Yutodama began his own monastery,

183
00:10:42,150 --> 00:10:45,120
 he stayed at Stony Creek, a

184
00:10:45,120 --> 00:10:47,800
 monastery called Wat Khmer Kram.

185
00:10:47,800 --> 00:10:49,690
 That one point said he'd really like to do something nice

186
00:10:49,690 --> 00:10:50,160
 for them.

187
00:10:50,160 --> 00:10:54,600
 They had supported him for a long time, and he'd like to do

188
00:10:54,600 --> 00:10:56,800
 something nice for them.

189
00:10:56,800 --> 00:11:00,690
 This is kind of our opportunity to share in that merit as

190
00:11:00,690 --> 00:11:01,320
 well.

191
00:11:01,320 --> 00:11:04,120
 Anyone who would like to share in the merit of the robe,

192
00:11:04,120 --> 00:11:04,980
 please do so.

193
00:11:04,980 --> 00:11:08,620
 Everything that we receive over and above the cost of the

194
00:11:08,620 --> 00:11:10,160
 robe will be re-donated to

195
00:11:10,160 --> 00:11:14,200
 Wat Khmer Kram because they also have an online campaign.

196
00:11:14,200 --> 00:11:17,040
 They're doing a building project of some sort.

197
00:11:17,040 --> 00:11:21,330
 If you look at the campaign that I created in the text of

198
00:11:21,330 --> 00:11:24,000
 it, it shows the second campaign

199
00:11:24,000 --> 00:11:27,240
 where any additional funds will be re-gifted.

200
00:11:27,240 --> 00:11:31,770
 There's a lot of good merit potential if anybody's

201
00:11:31,770 --> 00:11:32,320
 interested.

202
00:11:32,320 --> 00:11:35,160
 What's going to happen with the robe is it will be mailed

203
00:11:35,160 --> 00:11:36,880
 from Thailand to the monastery

204
00:11:36,880 --> 00:11:40,460
 and to people who are local, Aruna and Stefano, who's going

205
00:11:40,460 --> 00:11:42,320
 to be going to the monastery to

206
00:11:42,320 --> 00:11:44,320
 meditate for a couple months.

207
00:11:44,320 --> 00:11:49,160
 They'll offer it to Venerable Yitadhamo or possibly even

208
00:11:49,160 --> 00:11:50,560
 put together a little ceremony

209
00:11:50,560 --> 00:11:54,360
 and possibly invite others if others are available.

210
00:11:54,360 --> 00:11:57,000
 Certainly anybody who's in the area, let us know.

211
00:11:57,000 --> 00:12:00,440
 We can figure out something and make it nice.

212
00:12:00,440 --> 00:12:04,990
 It will probably be a little bit after the normal deadline,

213
00:12:04,990 --> 00:12:07,080
 but hopefully not too long

214
00:12:07,080 --> 00:12:09,200
 after the normal deadline.

215
00:12:09,200 --> 00:12:12,240
 That's about it.

216
00:12:12,240 --> 00:12:12,760
 Thank you, Bhanthi.

217
00:12:12,760 --> 00:12:20,720
 And still caught up on questions.

218
00:12:20,720 --> 00:12:22,520
 All right.

219
00:12:22,520 --> 00:12:24,240
 Let's go.

220
00:12:24,240 --> 00:12:27,040
 Say goodnight.

221
00:12:27,040 --> 00:12:34,050
 I can't, in regards to money, I can't talk and I can't even

222
00:12:34,050 --> 00:12:37,360
 say, I can't even express

223
00:12:37,360 --> 00:12:46,010
 thanks in regards to money, but I appreciate the thought of

224
00:12:46,010 --> 00:12:48,680
 trying to provide me with robes

225
00:12:48,680 --> 00:12:50,680
 and that's kind of awesome.

226
00:12:50,680 --> 00:12:53,200
 You can have a new color, I think.

227
00:12:53,200 --> 00:12:59,570
 So it's much appreciated everyone's continuing interest and

228
00:12:59,570 --> 00:13:01,960
 support to keep me alive and

229
00:13:01,960 --> 00:13:05,520
 closed and housed and that kind of thing.

230
00:13:05,520 --> 00:13:07,160
 It's much appreciated.

231
00:13:07,160 --> 00:13:10,720
 It's quite a nice opportunity on our end too because for a

232
00:13:10,720 --> 00:13:12,360
 lot of us, we didn't grow up

233
00:13:12,360 --> 00:13:18,000
 in Buddhist culture and this is new and it's really cool.

234
00:13:18,000 --> 00:13:19,000
 So thank you.

235
00:13:19,000 --> 00:13:21,320
 It's awesome that we're able to do this.

236
00:13:21,320 --> 00:13:25,850
 They said it couldn't be done, but we're starting a center

237
00:13:25,850 --> 00:13:28,840
 without charging money to anyone.

238
00:13:28,840 --> 00:13:29,840
 Yes.

239
00:13:29,840 --> 00:13:31,560
 Which is pretty cool.

240
00:13:31,560 --> 00:13:33,160
 Yes, it is.

241
00:13:33,160 --> 00:13:34,160
 Goodnight.

242
00:13:34,160 --> 00:13:35,160
 Goodnight, Bhanthi.

243
00:13:35,160 --> 00:13:36,160
 Thank you.

244
00:13:36,160 --> 00:13:37,160
 Thank you for your help.

245
00:13:37,160 --> 00:13:37,160
 Thank you.

246
00:13:37,160 --> 00:13:38,160
 Thank you.

247
00:13:38,160 --> 00:13:38,160
 Thank you.

248
00:13:38,160 --> 00:13:39,160
 Thank you.

249
00:13:39,160 --> 00:13:40,160
 Thank you.

