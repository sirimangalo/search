1
00:00:00,000 --> 00:00:07,600
 So when he sees rise and fall, so with the paragraph 100

2
00:00:07,600 --> 00:00:11,000
 and so on, he explains that

3
00:00:11,000 --> 00:00:19,000
 the truth depends on origination and others.

4
00:00:19,000 --> 00:00:22,290
 So when he sees rise and fall in the two ways according to

5
00:00:22,290 --> 00:00:24,000
 condition and according to instant,

6
00:00:24,000 --> 00:00:28,000
 thus the truth of origination becomes evident to him.

7
00:00:28,000 --> 00:00:33,260
 The truth of origination means the second noble truth, the

8
00:00:33,260 --> 00:00:35,000
 origin of suffering.

9
00:00:35,000 --> 00:00:38,410
 So the truth of origination becomes evident to him through

10
00:00:38,410 --> 00:00:40,000
 seeing rise according to condition

11
00:00:40,000 --> 00:00:45,000
 going to his discovery of the progenitor.

12
00:00:45,000 --> 00:00:51,540
 So when he sees the rise and fall in two ways according to

13
00:00:51,540 --> 00:00:54,000
 condition and according to instant,

14
00:00:54,000 --> 00:01:01,000
 then the second truth becomes evident to him.

15
00:01:01,000 --> 00:01:03,580
 The truth of suffering becomes evident to him through

16
00:01:03,580 --> 00:01:06,000
 seeing rise and fall according to instant.

17
00:01:06,000 --> 00:01:14,050
 Now here, according to instant, going to his discovery of

18
00:01:14,050 --> 00:01:17,000
 the suffering due to birth.

19
00:01:17,000 --> 00:01:22,000
 Now, we should say suffering which is birth.

20
00:01:22,000 --> 00:01:30,000
 Suffering and birth are same here, not due to birth.

21
00:01:30,000 --> 00:01:32,530
 The truth of suggestion becomes evident to him through

22
00:01:32,530 --> 00:01:35,000
 seeing fall according to condition

23
00:01:35,000 --> 00:01:39,100
 going to his discovery of the non-arising of things

24
00:01:39,100 --> 00:01:42,000
 produced by conditions when their conditions do not arise.

25
00:01:42,000 --> 00:01:45,650
 The truth of suffering becomes evident to him through

26
00:01:45,650 --> 00:01:48,000
 seeing fall according to instant

27
00:01:48,000 --> 00:01:57,000
 going to his discovery of the suffering which is death.

28
00:01:57,000 --> 00:02:00,560
 And his seeing of rise and fall becomes evident to him as

29
00:02:00,560 --> 00:02:03,000
 the truth of the path that

30
00:02:03,000 --> 00:02:06,290
 this is the mundane path going to evolution of confusion

31
00:02:06,290 --> 00:02:07,000
 about it.

32
00:02:07,000 --> 00:02:15,000
 So all the four noble truths become evident to him through

33
00:02:15,000 --> 00:02:17,000
 seeing rise and fall

34
00:02:17,000 --> 00:02:23,470
 of the aggregates in two ways according to condition and

35
00:02:23,470 --> 00:02:27,000
 according to instant.

36
00:02:27,000 --> 00:02:30,990
 And then the dependent origination and forward order

37
00:02:30,990 --> 00:02:33,000
 becomes evident to him through seeing rise

38
00:02:33,000 --> 00:02:37,450
 according to condition going to his discovery that when

39
00:02:37,450 --> 00:02:40,000
 this exists, that comes to be.

40
00:02:40,000 --> 00:02:44,860
 The dependent origination in reverse order becomes evident

41
00:02:44,860 --> 00:02:47,000
 to him through seeing fall according to condition

42
00:02:47,000 --> 00:02:50,580
 going to his discovery that when this does not exist, that

43
00:02:50,580 --> 00:02:52,000
 does not come to be.

44
00:02:52,000 --> 00:02:55,670
 Dependently, recent states become evident to him through

45
00:02:55,670 --> 00:02:59,000
 seeing rise and fall according to instant

46
00:02:59,000 --> 00:03:03,000
 going to his discovery of the characteristic of the forms

47
00:03:03,000 --> 00:03:05,000
 for the things possessed of rise and fall are formed

48
00:03:05,000 --> 00:03:14,590
 and conditionally arisen. So seeing five aggregates, seeing

49
00:03:14,590 --> 00:03:18,000
 the rise and fall of five aggregates in these ways

50
00:03:18,000 --> 00:03:23,990
 make him understand more clearly the dependent origination

51
00:03:23,990 --> 00:03:26,000
 in forward order, in reverse order

52
00:03:26,000 --> 00:03:31,400
 and also the states mentioned in the dependent origination

53
00:03:31,400 --> 00:03:35,000
 which are called dependent recent states.

54
00:03:35,000 --> 00:03:41,490
 That means actually all that are mentioned in the dependent

55
00:03:41,490 --> 00:03:43,000
 origination.

56
00:03:43,000 --> 00:03:46,360
 Everything mentioned in the dependent origination arise

57
00:03:46,360 --> 00:03:48,000
 from some other conditions.

58
00:03:48,000 --> 00:03:52,000
 So they are all dependent recent states.

59
00:03:52,000 --> 00:03:57,560
 Now the method of identity becomes evident to him through

60
00:03:57,560 --> 00:04:02,000
 seeing rise according to condition and so on.

61
00:04:02,000 --> 00:04:07,150
 Do you remember the four methods with which to understand

62
00:04:07,150 --> 00:04:10,000
 the dependent origination?

63
00:04:10,000 --> 00:04:21,000
 We go back to chapter 17.

64
00:04:21,000 --> 00:04:28,000
 Chapter 17, paragraph 309.

65
00:04:28,000 --> 00:04:32,000
 Then there are four methods of treating the meaning here.

66
00:04:32,000 --> 00:04:36,100
 They are method of identity, the method of diversity, the

67
00:04:36,100 --> 00:04:40,700
 method of uninterest and the method of ineluctable regular

68
00:04:40,700 --> 00:04:41,000
ity.

69
00:04:41,000 --> 00:04:46,020
 So these are the four methods by which we should understand

70
00:04:46,020 --> 00:04:50,000
 dependent origination or understand things.

71
00:04:50,000 --> 00:04:58,340
 These methods become evidence to him who sees rise and fall

72
00:04:58,340 --> 00:05:02,000
 of aggregates in two ways.

73
00:05:02,000 --> 00:05:07,370
 The method of identity becomes evident to him through

74
00:05:07,370 --> 00:05:11,000
 seeing rise according to condition

75
00:05:11,000 --> 00:05:14,220
 going to his discovery of unbroken continuity in the

76
00:05:14,220 --> 00:05:16,000
 connection of cosmic truth.

77
00:05:16,000 --> 00:05:24,890
 So there is a connection of cause and fruit or cause and

78
00:05:24,890 --> 00:05:27,000
 effect.

79
00:05:27,000 --> 00:05:33,540
 So when one sees the rising according to condition, that

80
00:05:33,540 --> 00:05:37,430
 means because there is this condition and there is the

81
00:05:37,430 --> 00:05:38,000
 fruit.

82
00:05:38,000 --> 00:05:43,350
 Because this is a condition, there is a fruit of it and so

83
00:05:43,350 --> 00:05:44,000
 on.

84
00:05:44,000 --> 00:05:48,240
 So when he sees this, he understands it by way of the

85
00:05:48,240 --> 00:05:50,000
 method of identity.

86
00:05:50,000 --> 00:05:58,000
 That means there is a continuation of cause and effect.

87
00:05:58,000 --> 00:06:02,000
 And then he more thoroughly abandons the annihilation view.

88
00:06:02,000 --> 00:06:06,000
 When he sees this, he abandons the annihilation view.

89
00:06:06,000 --> 00:06:12,070
 That means he abandons the view that it being is annihil

90
00:06:12,070 --> 00:06:14,000
ated as death.

91
00:06:14,000 --> 00:06:17,000
 Nothing arises after death.

92
00:06:17,000 --> 00:06:24,520
 So he abandons this view when he sees arising according to

93
00:06:24,520 --> 00:06:26,000
 condition.

94
00:06:26,000 --> 00:06:30,290
 Because there is the condition and that which arises

95
00:06:30,290 --> 00:06:33,000
 depending on that condition.

96
00:06:33,000 --> 00:06:37,470
 So this condition and fruit connection goes on and on and

97
00:06:37,470 --> 00:06:38,000
 on.

98
00:06:38,000 --> 00:06:44,000
 So there is some kind of identity in this continuity.

99
00:06:44,000 --> 00:06:48,310
 The method of diversity becomes evident to him through

100
00:06:48,310 --> 00:06:51,000
 seeing rise according to instant.

101
00:06:51,000 --> 00:06:54,000
 At every instant there is arising.

102
00:06:54,000 --> 00:06:58,920
 So going to his discovery that each state is new as it

103
00:06:58,920 --> 00:07:00,000
 arises.

104
00:07:00,000 --> 00:07:08,740
 Although there is some kind of continuity, everyone is a

105
00:07:08,740 --> 00:07:11,000
 new one.

106
00:07:11,000 --> 00:07:23,250
 So since everything is new, new at every moment, there is

107
00:07:23,250 --> 00:07:29,000
 no permanency in the states.

108
00:07:29,000 --> 00:07:41,200
 So when he sees arising according to instant, then he can

109
00:07:41,200 --> 00:07:46,000
 abandon the eternity view that things are permanent.

110
00:07:46,000 --> 00:07:50,500
 Because at every moment there is a new thing arising and

111
00:07:50,500 --> 00:07:54,000
 all things disappearing.

112
00:07:54,000 --> 00:07:58,000
 So there can be no permanency in these states.

113
00:07:58,000 --> 00:08:00,000
 So he sees that.

114
00:08:00,000 --> 00:08:03,880
 The method of uninterestedness becomes evident to him

115
00:08:03,880 --> 00:08:08,000
 through seeing rise and fall according to condition.

116
00:08:08,000 --> 00:08:11,720
 According to his discovery of the inability of states to

117
00:08:11,720 --> 00:08:14,000
 have mastery exercised over them.

118
00:08:14,000 --> 00:08:18,000
 Then he more thoroughly abandons the self view.

119
00:08:18,000 --> 00:08:23,950
 Self means if it is to be called self, then it must be able

120
00:08:23,950 --> 00:08:28,000
 to exercise its authority over it.

121
00:08:28,000 --> 00:08:31,000
 But now they just rise and fall.

122
00:08:31,000 --> 00:08:38,000
 The states just rise and fall.

123
00:08:38,000 --> 00:08:46,690
 And nobody can exercise authority over them to be permanent

124
00:08:46,690 --> 00:08:49,000
 or whatever.

125
00:08:49,000 --> 00:08:54,000
 So when he sees rising and falling according to condition,

126
00:08:54,000 --> 00:08:59,000
 then he is able to abandon the self view.

127
00:08:59,000 --> 00:09:02,000
 The view that there is a permanent self.

128
00:09:02,000 --> 00:09:06,390
 The method of intellectable regularity becomes evident to

129
00:09:06,390 --> 00:09:10,000
 him through seeing rise according to condition.

130
00:09:10,000 --> 00:09:12,970
 According to his discovery of the arising of the fruit from

131
00:09:12,970 --> 00:09:15,000
 the suitable conditions are there.

132
00:09:15,000 --> 00:09:19,660
 Then he more thoroughly abandons the moral inefficacy of

133
00:09:19,660 --> 00:09:21,000
 action view.

134
00:09:21,000 --> 00:09:27,150
 That means whatever you do does not amount to karma or

135
00:09:27,150 --> 00:09:30,000
 something like that.

136
00:09:30,000 --> 00:09:37,410
 So he is able to abandon that view because he sees the

137
00:09:37,410 --> 00:09:43,510
 arising of aggregate or arising of states according to

138
00:09:43,510 --> 00:09:44,000
 conditions.

139
00:09:44,000 --> 00:09:48,680
 Because of this condition there is this fruit and this

140
00:09:48,680 --> 00:09:52,900
 fruit is from this condition only and not from any other

141
00:09:52,900 --> 00:09:54,000
 condition.

142
00:09:54,000 --> 00:10:02,660
 And so he is able to abandon the wrong view that even

143
00:10:02,660 --> 00:10:07,380
 though you do something, you do not amount to doing

144
00:10:07,380 --> 00:10:11,000
 anything, any karma or whatever.

145
00:10:11,000 --> 00:10:13,790
 Now the characteristic of not-self becomes evident to him

146
00:10:13,790 --> 00:10:16,000
 through seeing rise according to condition.

147
00:10:16,000 --> 00:10:21,000
 According to his discovery that states have no curiosity.

148
00:10:21,000 --> 00:10:26,160
 We have finished the four methods with regard to the

149
00:10:26,160 --> 00:10:29,000
 dependent origination.

150
00:10:29,000 --> 00:10:31,000
 Now characteristics.

151
00:10:31,000 --> 00:10:37,070
 So the states have no curiosity really means no effort of

152
00:10:37,070 --> 00:10:44,000
 their own and that their existence depends upon conditions.

153
00:10:44,000 --> 00:10:49,490
 Now in this paragraph there are two things which we should

154
00:10:49,490 --> 00:10:54,000
 note and that is characteristic of individual essence.

155
00:10:54,000 --> 00:10:57,000
 That is down the paragraph.

156
00:10:57,000 --> 00:11:01,000
 And the characteristic of what is formed.

157
00:11:01,000 --> 00:11:03,000
 I want you to understand these two.

158
00:11:03,000 --> 00:11:07,600
 Characteristic of individual essence and characteristic of

159
00:11:07,600 --> 00:11:09,000
 what is formed.

160
00:11:09,000 --> 00:11:13,780
 Characteristic of individual essence means characteristic

161
00:11:13,780 --> 00:11:22,860
 of their characteristics which they do not share with any

162
00:11:22,860 --> 00:11:24,000
 other state.

163
00:11:24,000 --> 00:11:28,000
 Like say contact.

164
00:11:28,000 --> 00:11:34,000
 Contact has the characteristic of impinging the object.

165
00:11:34,000 --> 00:11:37,860
 And that characteristic is of contact only and not of

166
00:11:37,860 --> 00:11:41,000
 feeling, not of perception and so on.

167
00:11:41,000 --> 00:11:45,130
 So they are called individual characteristics or individual

168
00:11:45,130 --> 00:11:46,000
 essence.

169
00:11:46,000 --> 00:11:52,270
 And the characteristic of what is formed means the imperman

170
00:11:52,270 --> 00:11:56,000
ence, suffering and no-self nature.

171
00:11:56,000 --> 00:12:00,000
 They are called characteristics of what is formed.

172
00:12:00,000 --> 00:12:03,980
 They are actually common characteristics of conditioned

173
00:12:03,980 --> 00:12:05,000
 phenomena.

174
00:12:05,000 --> 00:12:08,450
 So everything that is conditioned has these three

175
00:12:08,450 --> 00:12:10,000
 characteristics.

176
00:12:10,000 --> 00:12:15,000
 The arising, presence and dissolution.

177
00:12:15,000 --> 00:12:18,460
 And Ayogi comes to see both characteristics of individual

178
00:12:18,460 --> 00:12:22,040
 essence and the characteristics of what is formed when he

179
00:12:22,040 --> 00:12:27,000
 practices Vipassana meditation.

180
00:12:27,000 --> 00:12:35,000
 So these two become evident to him.

181
00:12:35,000 --> 00:12:38,000
 Going to his discovery of the non-existence of fall at the

182
00:12:38,000 --> 00:12:41,000
 instance of rise and the non-existence of rise at the

183
00:12:41,000 --> 00:12:42,000
 instance of fall.

184
00:12:42,000 --> 00:12:44,530
 When there is rising there is no falling, when there is no

185
00:12:44,530 --> 00:12:46,000
 falling there is no rising.

186
00:12:46,000 --> 00:12:50,210
 So when he discovers this then the characteristic of what

187
00:12:50,210 --> 00:12:53,980
 is formed and characteristic of individual essence become

188
00:12:53,980 --> 00:12:55,000
 clear to him.

189
00:12:55,000 --> 00:13:03,100
 Now, here only rise and fall are mentioned and not the

190
00:13:03,100 --> 00:13:07,000
 intermediate stage.

191
00:13:07,000 --> 00:13:11,350
 So the footnote says that the inclusion of only rise and

192
00:13:11,350 --> 00:13:15,210
 fall here is because this kind of knowledge occurs as

193
00:13:15,210 --> 00:13:17,000
 seeing only rise and fall.

194
00:13:17,000 --> 00:13:23,000
 Not because of non-existence of the instance of presence.

195
00:13:23,000 --> 00:13:30,000
 Because in Vipassana the yogis see only rising and falling.

196
00:13:30,000 --> 00:13:33,000
 That is why rising and falling are mentioned here.

197
00:13:33,000 --> 00:13:37,000
 Not because the intermediate stage is non-existent.

198
00:13:37,000 --> 00:13:42,000
 There is an intermediate stage which is called presence.

199
00:13:42,000 --> 00:13:49,560
 But in Vipassana meditation only rising and disappearing

200
00:13:49,560 --> 00:13:54,530
 are seen by yogis. That is why these two only are mentioned

201
00:13:54,530 --> 00:13:55,000
.

202
00:13:55,000 --> 00:13:59,190
 When the different truth aspects of the independent orig

203
00:13:59,190 --> 00:14:03,600
ination methods and characteristics have become evident to

204
00:14:03,600 --> 00:14:04,000
 him thus,

205
00:14:04,000 --> 00:14:09,310
 then formations appear to him as perpetually renewed. I

206
00:14:09,310 --> 00:14:14,130
 want to say always new instead of saying perpetually

207
00:14:14,130 --> 00:14:15,000
 renewed.

208
00:14:15,000 --> 00:14:19,700
 Because they are not renewed actually. At every moment

209
00:14:19,700 --> 00:14:24,000
 there is a new phenomenon existing.

210
00:14:24,000 --> 00:14:29,000
 Not that something is renewed.

211
00:14:29,000 --> 00:14:35,650
 Then how do things stay the same? First of all, do you mean

212
00:14:35,650 --> 00:14:39,000
 that both mind and matter all states are always new?

213
00:14:39,000 --> 00:14:41,000
 Then how do things stay the same?

214
00:14:41,000 --> 00:14:44,490
 And how do they know, how does the glass know that it keeps

215
00:14:44,490 --> 00:14:48,000
 staying glass if it is continually stopping and starting

216
00:14:48,000 --> 00:14:49,000
 over again?

217
00:14:49,000 --> 00:14:56,010
 In the place of the old one which has disappeared, a new

218
00:14:56,010 --> 00:15:01,000
 one takes its place. Off of the same nature.

219
00:15:01,000 --> 00:15:05,000
 Yeah, because we talked about that. They have a nature.

220
00:15:05,000 --> 00:15:11,940
 Off the same nature, of the same kind. But let us say

221
00:15:11,940 --> 00:15:19,000
 something like, not one glass, say many glasses.

222
00:15:19,000 --> 00:15:22,350
 So when one glass disappears, then you put another glass

223
00:15:22,350 --> 00:15:23,000
 there.

224
00:15:23,000 --> 00:15:28,440
 So it is, we can say it is the same in the sense that the

225
00:15:28,440 --> 00:15:34,260
 one disappears is a glass and the one which is now there is

226
00:15:34,260 --> 00:15:36,000
 also a glass.

227
00:15:36,000 --> 00:15:40,520
 But they are different. The one is removed and the other is

228
00:15:40,520 --> 00:15:42,000
 put in its place.

229
00:15:42,000 --> 00:15:46,920
 In the same way, one material property disappears and

230
00:15:46,920 --> 00:15:52,750
 another takes its place. Another of the same kind. Of

231
00:15:52,750 --> 00:15:58,000
 similar kind.

232
00:15:58,000 --> 00:16:04,000
 So these states it seems being previously unreasoned arise.

233
00:16:04,000 --> 00:16:06,000
 And being a reason they cease.

234
00:16:06,000 --> 00:16:09,400
 And they are not only perpetually renewed or they are not

235
00:16:09,400 --> 00:16:12,000
 only always new, but they are also short-lived.

236
00:16:12,000 --> 00:16:15,560
 Like dew drops at sunrise, like a bubble on water, like a

237
00:16:15,560 --> 00:16:20,370
 line drawn on water, like a mustard seed on an oil's point,

238
00:16:20,370 --> 00:16:22,000
 like a lightning flesh.

239
00:16:22,000 --> 00:16:26,440
 And they appear without coal, like a conjuring trick, like

240
00:16:26,440 --> 00:16:30,410
 a mirage, like a dream, like a circle of a foiling fire

241
00:16:30,410 --> 00:16:31,000
brand.

242
00:16:31,000 --> 00:16:36,900
 And this is not mentioned in the discourses. So we cannot

243
00:16:36,900 --> 00:16:41,630
 trace that. Like a goblin city, like a frog, like a

244
00:16:41,630 --> 00:16:44,000
 planting trunk and so on.

245
00:16:44,000 --> 00:16:53,360
 No, goblin city. You know goblin city? It is like a city

246
00:16:53,360 --> 00:17:01,000
 created by goblins or ghosts or some spirits.

247
00:17:01,000 --> 00:17:06,320
 So sometimes you find yourself in a house, in a big

248
00:17:06,320 --> 00:17:12,760
 building, and then next morning you find yourself lying on

249
00:17:12,760 --> 00:17:17,490
 the ground and nothing of the house you experience during

250
00:17:17,490 --> 00:17:19,000
 the night could be seen.

251
00:17:19,000 --> 00:17:25,520
 So that kind of experience happened to many people. I don't

252
00:17:25,520 --> 00:17:30,470
 know if they have been here too. So they are called goblin

253
00:17:30,470 --> 00:17:31,000
 city.

254
00:17:31,000 --> 00:17:40,630
 That means just the cities or whatever make a beer as real

255
00:17:40,630 --> 00:17:44,000
 to human beings.

256
00:17:44,000 --> 00:17:50,690
 Maybe in the movies you find something like this. It doesn

257
00:17:50,690 --> 00:17:55,440
't enter the house and he enjoys the food or whatever in the

258
00:17:55,440 --> 00:17:56,000
 house.

259
00:17:56,000 --> 00:18:00,840
 And in the morning he finds himself lying on the ground,

260
00:18:00,840 --> 00:18:08,000
 something like that. That is called goblin city.

261
00:18:08,000 --> 00:18:13,610
 Then at this point he has attained tender inside knowledge.

262
00:18:13,610 --> 00:18:16,000
 Now it is still tender.

263
00:18:16,000 --> 00:18:18,950
 Called contemplation of rise and fall, which has become

264
00:18:18,950 --> 00:18:22,320
 established by penetrating the 50 characteristics in this

265
00:18:22,320 --> 00:18:23,000
 manner.

266
00:18:23,000 --> 00:18:27,780
 Only what is subject to fall arises and do be a reason

267
00:18:27,780 --> 00:18:34,000
 necessitates fall. I don't think that is acceptable.

268
00:18:34,000 --> 00:18:41,200
 Only what is subject to fall arises and what has arisen

269
00:18:41,200 --> 00:18:44,000
 naturally falls.

270
00:18:44,000 --> 00:18:47,220
 With the attainment of this he is known as a beginner of

271
00:18:47,220 --> 00:18:51,440
 insight. Only when you are changed to this stage are you

272
00:18:51,440 --> 00:18:54,000
 called a beginner of insight.

273
00:18:54,000 --> 00:19:04,000
 Before that you are not an official beginner of insight.

274
00:19:04,000 --> 00:19:10,000
 Now imperfections of insight, impediment of insight.

275
00:19:10,000 --> 00:19:17,000
 When a yogi reaches this stage, then the ten imperfections

276
00:19:17,000 --> 00:19:20,000
 or impurities of insight arise in him.

277
00:19:20,000 --> 00:19:25,930
 These are the obstacles to progress, obstacles to

278
00:19:25,930 --> 00:19:31,000
 attainment, obstacles to enlightenment.

279
00:19:31,000 --> 00:19:35,810
 Because if a yogi takes them to be enlightenment, then he

280
00:19:35,810 --> 00:19:40,340
 would not practice any father and so he would not get to

281
00:19:40,340 --> 00:19:42,000
 enlightenment.

282
00:19:42,000 --> 00:19:51,000
 These are called imperfections, impurities of insight.

283
00:19:51,000 --> 00:19:57,630
 There are ten of them. They are given as one illumination,

284
00:19:57,630 --> 00:20:02,000
 two knowledge, three rupturous happiness, that means P.E.D.

285
00:20:02,000 --> 00:20:07,610
 Four tranquility, five bliss pleasure sukha, six resolution

286
00:20:07,610 --> 00:20:11,000
, seven exertion, eight assurance.

287
00:20:11,000 --> 00:20:14,800
 I would say establishment and nine equanimity and ten

288
00:20:14,800 --> 00:20:16,000
 attachment.

289
00:20:16,000 --> 00:20:25,430
 So these ten things happen or arise to a person who has

290
00:20:25,430 --> 00:20:35,000
 reached this stage of contemplating of rise and fall.

291
00:20:35,000 --> 00:20:39,920
 And these do not arise either in a noble disciple who has

292
00:20:39,920 --> 00:20:45,800
 reached penetration of the truth, that means who has gained

293
00:20:45,800 --> 00:20:47,000
 enlightenment,

294
00:20:47,000 --> 00:20:51,070
 or in persons erring by virtue neglectful of their

295
00:20:51,070 --> 00:20:54,000
 meditation subject and idolas.

296
00:20:54,000 --> 00:21:05,000
 So they arise only to those who are practicing meditation,

297
00:21:05,000 --> 00:21:09,000
 who gives to the right course, devotes himself continuously

298
00:21:09,000 --> 00:21:15,000
 to his meditation subject and is a beginner of insight.

299
00:21:15,000 --> 00:21:20,620
 So these ten will not arise in noble ones and also in those

300
00:21:20,620 --> 00:21:24,000
 who do not practice meditation.

301
00:21:24,000 --> 00:21:32,000
 And there are ten.

302
00:21:32,000 --> 00:21:37,000
 And then these ten are explained one by one, right?

303
00:21:37,000 --> 00:21:41,000
 So let's go to illumination.

304
00:21:41,000 --> 00:21:46,000
 Now illumination is illumination due to insight.

305
00:21:46,000 --> 00:21:49,640
 When it arises the meditator thinks such illumination never

306
00:21:49,640 --> 00:21:51,000
 arose in me before.

307
00:21:51,000 --> 00:21:53,000
 I have surely reached the path, reached fruition.

308
00:21:53,000 --> 00:21:56,150
 Thus he takes what is not a path to be the path and what is

309
00:21:56,150 --> 00:21:58,000
 not fruition to be fruition.

310
00:21:58,000 --> 00:22:00,700
 When he takes what is not a path to be path and what is not

311
00:22:00,700 --> 00:22:03,360
 fruition to be fruition, the course of this insight is

312
00:22:03,360 --> 00:22:04,000
 interrupted.

313
00:22:04,000 --> 00:22:07,070
 Because he will not practice anymore. He drops his own

314
00:22:07,070 --> 00:22:10,700
 basic meditation subject and sits just enjoying the

315
00:22:10,700 --> 00:22:12,000
 illumination.

316
00:22:12,000 --> 00:22:15,450
 Now this illumination arises in one bhikkhu illuminating

317
00:22:15,450 --> 00:22:18,000
 only as much as the seat he is sitting on.

318
00:22:18,000 --> 00:22:20,710
 In another the interior of his room, in another the

319
00:22:20,710 --> 00:22:24,360
 exterior of his room, in another the whole monastery, a

320
00:22:24,360 --> 00:22:29,000
 quarter leap, a half leap, a week, two weeks, three weeks.

321
00:22:29,000 --> 00:22:31,910
 In another bhikkhu it arises making a single light from

322
00:22:31,910 --> 00:22:34,000
 Earth's surface up to the Brahma wall.

323
00:22:34,000 --> 00:22:37,470
 But in the blessed one it arose eliminating the ten

324
00:22:37,470 --> 00:22:40,000
 thousand fours walled elements.

325
00:22:40,000 --> 00:22:45,000
 And there is a story. Now, please read the footnote.

326
00:22:45,000 --> 00:22:51,190
 Illumination due to insight is a luminous materiality

327
00:22:51,190 --> 00:22:54,000
 originated by inside consciousness.

328
00:22:54,000 --> 00:22:58,230
 And that originated by temperature belonging to his own

329
00:22:58,230 --> 00:23:01,000
 continuity that meet in his body.

330
00:23:01,000 --> 00:23:06,760
 Of these that originated by inside consciousness is bright

331
00:23:06,760 --> 00:23:11,000
 and is found only in the meditator's body.

332
00:23:11,000 --> 00:23:16,530
 The other kind is independent of his body. I don't want to

333
00:23:16,530 --> 00:23:17,000
 say that.

334
00:23:17,000 --> 00:23:23,480
 The other kind leaves his body and spreads all around

335
00:23:23,480 --> 00:23:28,000
 according to the power of his knowledge.

336
00:23:28,000 --> 00:23:32,000
 Here the translation is not so good.

337
00:23:32,000 --> 00:23:38,030
 So the other kind leaves his body and spreads all around

338
00:23:38,030 --> 00:23:42,000
 according to the power of his knowledge.

339
00:23:42,000 --> 00:23:47,890
 And it is manifest to him only, not true. It becomes

340
00:23:47,890 --> 00:23:49,000
 manifest.

341
00:23:49,000 --> 00:23:53,900
 I would say it is manifest to him only. And he sees

342
00:23:53,900 --> 00:23:58,000
 anything material in the place touched by it.

343
00:23:58,000 --> 00:24:04,000
 So the illumination is manifest only to him.

344
00:24:04,000 --> 00:24:08,000
 Others won't see the illumination.

345
00:24:08,000 --> 00:24:12,000
 And he sees anything material in the place touched by it.

346
00:24:12,000 --> 00:24:19,000
 But he would see things illuminated by that bright light.

347
00:24:19,000 --> 00:24:26,000
 And then the sub-commentary raises a question.

348
00:24:26,000 --> 00:24:31,570
 Does he see with the eye consciousness or mind

349
00:24:31,570 --> 00:24:34,000
 consciousness?

350
00:24:34,000 --> 00:24:39,100
 And then he said it should be said to be seeing with mind

351
00:24:39,100 --> 00:24:41,000
 consciousness.

352
00:24:41,000 --> 00:24:45,510
 That means he does not see with his eyes, with his physical

353
00:24:45,510 --> 00:24:50,360
 eyes, but with his mind he sees things touched by or

354
00:24:50,360 --> 00:24:56,000
 illuminated by that bright light.

355
00:24:56,000 --> 00:25:03,000
 And then the story of two elders.

356
00:25:03,000 --> 00:25:06,000
 And next is knowledge.

357
00:25:06,000 --> 00:25:08,000
 Knowledge is knowledge due to insight.

358
00:25:08,000 --> 00:25:10,510
 As he is estimating and judging material and immaterial

359
00:25:10,510 --> 00:25:14,000
 states, perhaps knowledge that is unerring.

360
00:25:14,000 --> 00:25:19,680
 Unerring means undeterred, clean, incisive and very sharp

361
00:25:19,680 --> 00:25:23,000
 arises in him like a lightning flash.

362
00:25:23,000 --> 00:25:26,260
 And then BEETI, raptureless happiness, is happiness due to

363
00:25:26,260 --> 00:25:27,000
 insight.

364
00:25:27,000 --> 00:25:30,590
 Perhaps at that time the five kinds of happiness, namely

365
00:25:30,590 --> 00:25:34,010
 minor happiness, momentary happiness, showering happiness,

366
00:25:34,010 --> 00:25:37,170
 uplifting happiness and pervading raptureless happiness,

367
00:25:37,170 --> 00:25:40,000
 arise in him feeling his whole body.

368
00:25:40,000 --> 00:25:47,000
 You remember the five mentioned earlier in the chapters?

369
00:25:47,000 --> 00:25:53,000
 Chapter 4, paragraph 94 and so on.

370
00:25:53,000 --> 00:26:03,760
 Then tranquility, then bliss, resolution, exertion, and

371
00:26:03,760 --> 00:26:05,000
 then assurance.

372
00:26:05,000 --> 00:26:08,000
 Assurance is literally establishment.

373
00:26:08,000 --> 00:26:10,000
 So assurance really means mindfulness.

374
00:26:10,000 --> 00:26:18,000
 His mindfulness is very strong at this point.

375
00:26:18,000 --> 00:26:22,320
 And then equanimity is about insight and equanimity in ad

376
00:26:22,320 --> 00:26:23,000
verting.

377
00:26:23,000 --> 00:26:26,250
 For equanimity about insight, which is neutrality about

378
00:26:26,250 --> 00:26:29,000
 formation, see strongly in him at that time.

379
00:26:29,000 --> 00:26:33,310
 So does equanimity in adverting in the mind-door, not it is

380
00:26:33,310 --> 00:26:34,000
 also.

381
00:26:34,000 --> 00:26:39,000
 So does equanimity in adverting in the mind-door.

382
00:26:39,000 --> 00:26:43,740
 That means equanimity in adverting in the mind-door also

383
00:26:43,740 --> 00:26:46,000
 arises strongly in him.

384
00:26:46,000 --> 00:26:56,000
 And then attachment, that is real attachment.

385
00:26:56,000 --> 00:27:02,000
 Now, paragraph 124.

386
00:27:02,000 --> 00:27:05,110
 Here illumination, etc., are called imperfections because

387
00:27:05,110 --> 00:27:08,220
 they are the basis for imperfection, not because they are

388
00:27:08,220 --> 00:27:12,000
 chemically unprofitable, not because they are acousal.

389
00:27:12,000 --> 00:27:14,980
 But attachment is both an imperfection and the basis for

390
00:27:14,980 --> 00:27:16,000
 imperfection.

391
00:27:16,000 --> 00:27:19,780
 So the last one is real acousala because it is lower

392
00:27:19,780 --> 00:27:21,000
 attachment.

393
00:27:21,000 --> 00:27:26,380
 The other nine are not acousala, but they are ground for ac

394
00:27:26,380 --> 00:27:27,000
ousala.

395
00:27:27,000 --> 00:27:31,370
 They are conditioned for acousala because when the yogi has

396
00:27:31,370 --> 00:27:35,710
 an illumination, then he will be attached to his

397
00:27:35,710 --> 00:27:38,000
 illumination and so on.

398
00:27:38,000 --> 00:27:41,640
 And as basis, the amount is 10, but with the different ways

399
00:27:41,640 --> 00:27:44,000
 of taking them, they come to 30.

400
00:27:44,000 --> 00:28:00,110
 So by false view, by conceit, and then by attachment, by

401
00:28:00,110 --> 00:28:03,000
 real attachment, they become 30.

402
00:28:03,000 --> 00:28:09,370
 So each one can be taken by real false view, by real conce

403
00:28:09,370 --> 00:28:13,000
it, and by real attachment.

404
00:28:13,000 --> 00:28:21,000
 And then his skillful meditator is not deceived by these.

405
00:28:21,000 --> 00:28:29,320
 So he sees that they are impermanent and so on, and they

406
00:28:29,320 --> 00:28:36,000
 are not the right path to enlightenment.

407
00:28:36,000 --> 00:28:39,790
 So he decides here that the illumination and so on are not

408
00:28:39,790 --> 00:28:45,430
 the right path, but the practice of we persona only is the

409
00:28:45,430 --> 00:28:47,000
 right path.

410
00:28:50,000 --> 00:28:53,580
 When he decides this and this knowledge is established in

411
00:28:53,580 --> 00:28:57,270
 him, then he is said to have reached the knowledge and

412
00:28:57,270 --> 00:29:01,000
 vision of what is the path and what is not the path.

413
00:29:01,000 --> 00:29:06,680
 So he, a person, a yogi, after reaching the contemplation

414
00:29:06,680 --> 00:29:12,000
 of the right and forth, will encounter these impediments.

415
00:29:12,000 --> 00:29:16,990
 And when he encounters these impediments, he must be able

416
00:29:16,990 --> 00:29:21,430
 to see them as impediments and not as the signs of

417
00:29:21,430 --> 00:29:24,000
 enlightenment and so on.

418
00:29:24,000 --> 00:29:27,510
 If he takes them to be signs of enlightenment, then he will

419
00:29:27,510 --> 00:29:32,440
 stop there and he will not go any further, and so he will

420
00:29:32,440 --> 00:29:41,000
 be deprived of the progress inside enlightenment.

421
00:29:41,000 --> 00:29:47,240
 And then at this point, the three truths are defined and so

422
00:29:47,240 --> 00:29:48,000
 on.

423
00:29:48,000 --> 00:29:59,490
 So this is the end of the chapter 20 and the we persona

424
00:29:59,490 --> 00:30:07,000
 knowledge called contemplation of...

425
00:30:07,000 --> 00:30:11,100
 It is not the end of contemplation of right and forth. We

426
00:30:11,100 --> 00:30:13,560
 will have to go further with the contemplation of right and

427
00:30:13,560 --> 00:30:14,000
 forth.

428
00:30:14,000 --> 00:30:18,540
 But one purity ends here, that is the purification by

429
00:30:18,540 --> 00:30:23,000
 knowledge and vision of what is path and what is not path.

430
00:30:23,000 --> 00:30:27,760
 Because the yogi is practicing contemplation of right and

431
00:30:27,760 --> 00:30:31,920
 forth, and when he reaches to a certain level of the

432
00:30:31,920 --> 00:30:37,000
 contemplation of right and forth, these impediments came.

433
00:30:37,000 --> 00:30:43,570
 When he is disturbed by these impediments, he could not see

434
00:30:43,570 --> 00:30:48,000
 right and forth really very clearly.

435
00:30:48,000 --> 00:30:54,000
 So he has to practice again to see right and forth clearly.

436
00:30:54,000 --> 00:31:00,070
 So the contemplation of right and forth does not end here.

437
00:31:00,070 --> 00:31:03,000
 It will go over to the next chapter.

438
00:31:03,000 --> 00:31:07,470
 But the purification of the knowledge and vision of what is

439
00:31:07,470 --> 00:31:11,000
 the path and what is not the path ends here.

440
00:31:11,000 --> 00:31:18,000
 So there is overlapping of purity and we persona knowledge.

441
00:31:18,000 --> 00:31:26,000
 So if you look at this handout, you will see it clearly.

442
00:31:26,000 --> 00:31:29,000
 I have one more question.

443
00:31:29,000 --> 00:31:32,000
 Oh yes, please look at that.

444
00:31:32,000 --> 00:31:34,000
 Do you have another one?

445
00:31:34,000 --> 00:31:36,000
 No.

446
00:31:36,000 --> 00:31:44,110
 Could you explain some way that we could work with seeing,

447
00:31:44,110 --> 00:31:48,640
 rising, falling, is it like reaching, reaching, and then

448
00:31:48,640 --> 00:31:54,000
 falling, and then picking up, rising, and seeing?

449
00:31:54,000 --> 00:31:58,000
 Or try to be looking at the consciousness?

450
00:31:58,000 --> 00:32:04,370
 At first, we did not try to see the rising and falling. We

451
00:32:04,370 --> 00:32:10,000
 tried to get good concentration.

452
00:32:10,000 --> 00:32:15,250
 We tried to be mindful of what is happening at the present

453
00:32:15,250 --> 00:32:16,000
 moment.

454
00:32:16,000 --> 00:32:23,530
 And then later on, the arising and disappearing of things

455
00:32:23,530 --> 00:32:29,000
 you observe will become evident to you.

456
00:32:29,000 --> 00:32:34,340
 So what is important is that you get to get a necessary

457
00:32:34,340 --> 00:32:37,000
 amount of concentration.

458
00:32:37,000 --> 00:32:42,670
 And as your concentration gets better, then you will come

459
00:32:42,670 --> 00:32:46,000
 to see the arising and falling.

460
00:32:46,000 --> 00:32:49,000
 First, we have to see the thing clearly, right?

461
00:32:49,000 --> 00:32:52,340
 And then after that, we say, "This is arising. This is

462
00:32:52,340 --> 00:32:53,000
 falling."

463
00:32:53,000 --> 00:32:55,000
 Or, "It is not arising. It is not falling."

464
00:32:55,000 --> 00:32:59,840
 But before we see the real arising and falling, we have to

465
00:32:59,840 --> 00:33:02,000
 see the thing itself.

466
00:33:03,000 --> 00:33:03,000
 [

467
00:33:03,000 --> 00:33:20,000
 "It is not arising." ]

468
00:33:20,000 --> 00:33:24,120
 Is it possible to get the tapes from this class, copies of

469
00:33:24,120 --> 00:33:28,000
 the tapes from this class, to study as we go along?

470
00:33:28,000 --> 00:33:29,000
 Yeah.

471
00:33:29,000 --> 00:33:33,000
 How can we make copies?

472
00:33:33,000 --> 00:33:36,080
 Could you make copies of the ones that we made for just

473
00:33:36,080 --> 00:33:37,000
 this class?

474
00:33:37,000 --> 00:33:38,000
 Today?

475
00:33:38,000 --> 00:33:41,500
 No, the last three classes. We just started three times in

476
00:33:41,500 --> 00:33:42,000
 class.

477
00:33:42,000 --> 00:33:43,000
 Yes.

478
00:33:43,000 --> 00:33:44,000
 Thank you.

479
00:33:44,000 --> 00:33:46,000
 Four times.

480
00:33:54,000 --> 00:34:00,000
 Just for the center. This is going to take this.

481
00:34:00,000 --> 00:34:04,250
 The other lady who is absent today, she always takes the

482
00:34:04,250 --> 00:34:05,000
 tapes.

483
00:34:05,000 --> 00:34:07,000
 Oh, you want me to make one?

484
00:34:07,000 --> 00:34:09,000
 This is for the center.

485
00:34:09,000 --> 00:34:11,000
 Oh, I see.

486
00:34:11,000 --> 00:34:12,000
 [

487
00:34:14,000 --> 00:34:15,000
 "It is not arising." ]

488
00:34:15,000 --> 00:34:16,000
 [

489
00:34:16,000 --> 00:34:17,000
 "It is not arising." ]

490
00:34:18,000 --> 00:34:18,000
 [

491
00:34:18,000 --> 00:34:19,000
 "It is not arising." ]

492
00:34:21,000 --> 00:34:22,000
 [

493
00:34:22,000 --> 00:34:23,000
 "It is not arising." ]

