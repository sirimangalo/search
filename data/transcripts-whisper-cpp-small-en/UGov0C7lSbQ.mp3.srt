1
00:00:00,000 --> 00:00:05,760
 Hello and welcome back to our study of the Dhammapada.

2
00:00:05,760 --> 00:00:12,020
 Today we continue on with verses 133 and 134 which read as

3
00:00:12,020 --> 00:00:13,200
 follows.

4
00:00:13,200 --> 00:00:23,960
 Mahoja parusankanti utta pati vadayutta dukahi sarambha k

5
00:00:23,960 --> 00:00:30,840
atha katha patidanda puseyutta

6
00:00:30,840 --> 00:00:39,170
 sache naree naree siatana kangso upahadohyata ei sapat do

7
00:00:39,170 --> 00:00:47,480
 sinimbaanang sarambho te nuijati

8
00:00:47,480 --> 00:00:58,600
 which translate roughly as do not speak harshly to anyone

9
00:00:58,600 --> 00:01:08,410
 having been spoken to they may speak back to you they may

10
00:01:08,410 --> 00:01:10,520
 reply in kind

11
00:01:10,520 --> 00:01:17,720
 dukahi sarambha katha angry words are painful

12
00:01:17,720 --> 00:01:24,600
 patidanda puseyutta and you may be

13
00:01:25,160 --> 00:01:31,420
 struck may come to blows and may be struck as a result or

14
00:01:31,420 --> 00:01:34,600
 may be hit in return

15
00:01:34,600 --> 00:01:38,360
 may lead to violence perhaps

16
00:01:38,360 --> 00:01:47,480
 sache naree siatana if you can keep yourself still

17
00:01:47,480 --> 00:01:57,400
 kangso upahadohyata like a broken bell or a broken gong

18
00:01:57,400 --> 00:02:05,970
 ei sapat do sinimbaanang this is the way you will attain n

19
00:02:05,970 --> 00:02:08,040
ibana

20
00:02:08,040 --> 00:02:15,400
 sarambho te nuijati and there will be no anger found in you

21
00:02:15,720 --> 00:02:21,180
 so this has to do with harsh speech and the origin story is

22
00:02:21,180 --> 00:02:22,360
 relatively long

23
00:02:22,360 --> 00:02:26,040
 compared to the some of the stories recently have been

24
00:02:26,040 --> 00:02:31,320
 fairly short um don't have to go into all the detail but

25
00:02:31,320 --> 00:02:38,680
 briefly to sum up there was a monk called konda dhaana

26
00:02:38,680 --> 00:02:48,680
 and everywhere he went he was followed by a vision

27
00:02:48,680 --> 00:02:54,680
 of a woman so quite curious whenever whenever he would go

28
00:02:54,680 --> 00:02:55,640
 for alms

29
00:02:55,640 --> 00:02:59,160
 people would see this woman following behind him

30
00:02:59,160 --> 00:03:01,420
 and they would say here's a portion for you and here's a

31
00:03:01,420 --> 00:03:02,360
 portion for your lady

32
00:03:02,360 --> 00:03:06,920
 friend and he never saw this woman he would

33
00:03:06,920 --> 00:03:10,920
 turn around and see no woman but everybody else saw this

34
00:03:10,920 --> 00:03:16,280
 this woman and so word kind of got around

35
00:03:16,280 --> 00:03:22,350
 and the monks got upset but it tells the story of how this

36
00:03:22,350 --> 00:03:23,960
 happened so in

37
00:03:23,960 --> 00:03:27,060
 in ancient and the story is actually somewhat interesting i

38
00:03:27,060 --> 00:03:27,800
 mean it's all

39
00:03:27,800 --> 00:03:32,500
 quite fanciful i mean for for a modern skeptical audience i

40
00:03:32,500 --> 00:03:33,240
 think

41
00:03:33,240 --> 00:03:36,760
 there's a lot of distrust of these sort of

42
00:03:36,760 --> 00:03:39,960
 magical type tales how could there be this

43
00:03:39,960 --> 00:03:47,630
 woman well the story goes that in the time of buddha digai

44
00:03:47,630 --> 00:03:49,720
 digai

45
00:03:49,720 --> 00:03:55,020
 one of the past buddhas so in in in in another age in

46
00:03:55,020 --> 00:03:58,280
 another universe perhaps

47
00:04:00,280 --> 00:04:08,920
 why does it say diga or kasapa i don't know kasapa or diga

48
00:04:08,920 --> 00:04:12,920
 anyway there were these two monks who were so close

49
00:04:12,920 --> 00:04:15,010
 it was as though they were brothers they would do

50
00:04:15,010 --> 00:04:16,040
 everything together

51
00:04:16,040 --> 00:04:20,580
 they were the best of friends and the closest of companions

52
00:04:20,580 --> 00:04:21,480
 and so they

53
00:04:21,480 --> 00:04:00,440
 would always come come out and and come out together go for

54
00:04:00,440 --> 00:04:25,960
 their meals

55
00:04:25,960 --> 00:04:30,430
 together and go to meetings together meditate together that

56
00:04:30,430 --> 00:04:32,280
 kind of thing

57
00:04:32,280 --> 00:04:35,080
 and it happens that there was an angel a goddess

58
00:04:35,080 --> 00:04:42,840
 a female angel somehow who saw them and thought to herself

59
00:04:42,840 --> 00:04:48,120
 i wonder if there's a way that i can split these two up

60
00:04:48,120 --> 00:04:51,960
 listen carefully this is gives some insight into the true

61
00:04:51,960 --> 00:04:53,240
 nature of

62
00:04:53,240 --> 00:04:57,800
 of the divine you know we always many other religions

63
00:04:57,800 --> 00:05:00,440
 worship or pray to or

64
00:05:00,440 --> 00:05:06,680
 expect protection from angelic beings well it's not always

65
00:05:06,680 --> 00:05:07,640
 their

66
00:05:07,640 --> 00:05:12,920
 inclination so this this just on a whim this angel thought

67
00:05:12,920 --> 00:05:13,720
 i wonder if i can

68
00:05:13,720 --> 00:05:18,920
 split these two up and so one day when

69
00:05:21,240 --> 00:05:25,240
 when when they were walking in along the highway or the

70
00:05:25,240 --> 00:05:26,120
 forest

71
00:05:26,120 --> 00:05:31,880
 one of the monks said hold on i've got to go use

72
00:05:31,880 --> 00:05:35,540
 go what do you say attend the needs of nature i think it

73
00:05:35,540 --> 00:05:36,680
 says in

74
00:05:36,680 --> 00:05:40,840
 in the english he had to use he had to pass water

75
00:05:40,840 --> 00:05:46,840
 he had to urinate and so he went off into the bushes

76
00:05:46,840 --> 00:05:51,880
 and as he was coming out this angel appeared behind him

77
00:05:51,880 --> 00:05:55,880
 as a beautiful woman half naked and adjusting her robe and

78
00:05:55,880 --> 00:06:01,500
 and her and and fixing her hair as if coming out of the

79
00:06:01,500 --> 00:06:01,880
 bush

80
00:06:01,880 --> 00:06:07,860
 that the elder had just been been in and the other monk saw

81
00:06:07,860 --> 00:06:08,440
 this

82
00:06:08,440 --> 00:06:13,160
 and immediately suspected the worst in the first monk

83
00:06:13,160 --> 00:06:17,800
 accused him of

84
00:06:17,800 --> 00:06:24,680
 of of breaking the discipline accused him of

85
00:06:24,680 --> 00:06:27,770
 having intimate relations with the woman and the mother

86
00:06:27,770 --> 00:06:29,000
 monk denied it of course

87
00:06:29,000 --> 00:06:33,560
 and they ended up completely splitting and this monk went

88
00:06:33,560 --> 00:06:39,160
 before the other monks and accused him and all the monks

89
00:06:39,160 --> 00:06:42,680
 you know he interrogated this other monk who denied it all

90
00:06:42,680 --> 00:06:45,800
 so they couldn't come to a decision over it but

91
00:06:45,800 --> 00:06:51,960
 completely destroyed their friendship now this angel

92
00:06:51,960 --> 00:06:53,880
 realized and and began to

93
00:06:53,880 --> 00:06:57,880
 feel the effects of how evil it was the thing that she had

94
00:06:57,880 --> 00:07:03,080
 done she wasn't malicious she was just somewhat

95
00:07:03,080 --> 00:07:09,960
 deluded and and perhaps impetuous or careless

96
00:07:09,960 --> 00:07:13,320
 intoxicated perhaps in her powers and her

97
00:07:13,320 --> 00:07:18,710
 her great her own greatness but she felt terribly guilty

98
00:07:18,710 --> 00:07:20,040
 and so

99
00:07:20,040 --> 00:07:23,260
 she went down and told the monk the the monk who had

100
00:07:23,260 --> 00:07:24,440
 accused the second monk

101
00:07:24,440 --> 00:07:27,920
 who had accused the first monk told him what she had done

102
00:07:27,920 --> 00:07:28,200
 and this

103
00:07:28,200 --> 00:07:34,760
 monk believed her but they say it didn't ever fix their

104
00:07:34,760 --> 00:07:39,000
 their friendship they ended up never becoming friends

105
00:07:39,000 --> 00:07:42,680
 fast friends again because they didn't they couldn't

106
00:07:42,680 --> 00:07:46,760
 it was like the first one felt like he had

107
00:07:46,760 --> 00:07:52,840
 the other one had betrayed him by believing such a thing

108
00:07:52,840 --> 00:07:53,160
 could be

109
00:07:53,160 --> 00:07:55,640
 possible

110
00:07:55,640 --> 00:07:59,880
 so those two monks were reborn according to their karma the

111
00:07:59,880 --> 00:08:00,840
 goddess was born in

112
00:08:00,840 --> 00:08:06,360
 hell and suffered therefore a period of an

113
00:08:06,360 --> 00:08:10,920
 interval between two buddhas was born as a man and

114
00:08:10,920 --> 00:08:12,520
 eventually became

115
00:08:12,520 --> 00:08:18,360
 our protagonist or antagonist our the name of this

116
00:08:18,360 --> 00:08:24,760
 story namesake of this story kuta kundalhana

117
00:08:24,760 --> 00:08:31,190
 uh became a man it was born a boy and grew up and became a

118
00:08:31,190 --> 00:08:31,960
 monk

119
00:08:31,960 --> 00:08:36,360
 but everywhere he went this woman followed him and

120
00:08:36,360 --> 00:08:42,360
 so the monks were really upset about this and thought we

121
00:08:42,360 --> 00:08:43,240
 gotta do something

122
00:08:43,240 --> 00:08:45,850
 because everyone's starting to talk and they're going to

123
00:08:45,850 --> 00:08:47,880
 think that

124
00:08:47,880 --> 00:08:51,990
 we let women women follow us around this monk is going with

125
00:08:51,990 --> 00:08:52,600
 a woman

126
00:08:52,600 --> 00:08:55,600
 so they went to anatta pindika who's who's the owner of the

127
00:08:55,600 --> 00:08:56,600
 monastery or the

128
00:08:56,600 --> 00:09:00,120
 donor of the monastery and they said look you have to kick

129
00:09:00,120 --> 00:09:00,360
 this

130
00:09:00,360 --> 00:09:03,400
 monk out really i'm not sure why they would do this

131
00:09:03,400 --> 00:09:07,560
 because you know the buddha was there but

132
00:09:07,560 --> 00:09:10,470
 this this i've seen this happen before where monks go to

133
00:09:10,470 --> 00:09:11,480
 lay people trying to

134
00:09:11,480 --> 00:09:15,560
 solve the monastic problems but anatta pindika was having

135
00:09:15,560 --> 00:09:15,960
 none of

136
00:09:15,960 --> 00:09:19,560
 it he said well isn't the buddha you know isn't the buddha

137
00:09:19,560 --> 00:09:20,280
 in savati and

138
00:09:20,280 --> 00:09:22,600
 he said well the buddha will know what to do

139
00:09:22,600 --> 00:09:25,860
 they did nothing so they went to visaka who was the other

140
00:09:25,860 --> 00:09:27,640
 chief lay disciple

141
00:09:27,640 --> 00:09:31,780
 she also did nothing so finally they went to the king for

142
00:09:31,780 --> 00:09:32,760
 some reason

143
00:09:32,760 --> 00:09:36,120
 and they told the king about this that there was this

144
00:09:36,120 --> 00:09:38,760
 monk who

145
00:09:38,760 --> 00:09:41,930
 who had a woman following him around everywhere he went and

146
00:09:41,930 --> 00:09:42,680
 the king should

147
00:09:42,680 --> 00:09:46,040
 do something about it and the king of course being just an

148
00:09:46,040 --> 00:09:52,120
 ordinary worldling was taken by their words and went to the

149
00:09:52,120 --> 00:09:54,920
 monastery and brought some men and surrounded this

150
00:09:54,920 --> 00:09:59,320
 monk's hut and ordered him to come out and the monk

151
00:09:59,320 --> 00:10:02,120
 looked out and saw the king and so he went

152
00:10:02,120 --> 00:10:08,040
 and uh he came out no so the elder came out

153
00:10:08,040 --> 00:10:11,520
 and the king saw the immediately saw that this there was a

154
00:10:11,520 --> 00:10:12,280
 woman behind this

155
00:10:12,280 --> 00:10:14,520
 monk

156
00:10:14,520 --> 00:10:18,520
 and so the elders when when when he saw the king

157
00:10:18,520 --> 00:10:21,960
 he went back in and sat on a seat and when the king went in

158
00:10:21,960 --> 00:10:26,120
 didn't see the woman and so he looked everywhere he looked

159
00:10:26,120 --> 00:10:26,120
 in

160
00:10:26,120 --> 00:10:29,270
 under everything in all corners of the room up in the ra

161
00:10:29,270 --> 00:10:30,280
fters everywhere couldn't

162
00:10:30,280 --> 00:10:35,640
 find this woman he said where is she where's who the woman

163
00:10:35,640 --> 00:10:39,520
 and this goes on and and eventually he figures out that it

164
00:10:39,520 --> 00:10:39,720
's

165
00:10:39,720 --> 00:10:44,200
 just a some kind of strange phantom phenomenon

166
00:10:44,200 --> 00:10:48,570
 and so he uh he says you know look i because he takes the

167
00:10:48,570 --> 00:10:49,240
 monk out sees the

168
00:10:49,240 --> 00:10:52,320
 woman brings the monk back in doesn't see the woman it's

169
00:10:52,320 --> 00:10:52,920
 not real

170
00:10:52,920 --> 00:10:55,150
 and so he tells the monk he says look you're always going

171
00:10:55,150 --> 00:10:55,720
 to have this

172
00:10:55,720 --> 00:10:59,470
 trouble if this keeps happening so you come to my home for

173
00:10:59,470 --> 00:11:00,760
 alms you come to

174
00:11:00,760 --> 00:11:03,570
 the royal palace for alms and the other monks found out

175
00:11:03,570 --> 00:11:04,120
 about this

176
00:11:04,120 --> 00:11:08,280
 and they were incensed they said you know not only is this

177
00:11:08,280 --> 00:11:11,800
 monk corrupt but now the king is corrupt as well and so

178
00:11:11,800 --> 00:11:12,840
 they were bad-nosing the

179
00:11:12,840 --> 00:11:16,360
 king and bad-nosing this monk

180
00:11:18,440 --> 00:11:21,500
 and they told this they came to this monk they said well so

181
00:11:21,500 --> 00:11:21,880
 now you're the

182
00:11:21,880 --> 00:11:26,680
 king's bastard they said and the monk

183
00:11:26,680 --> 00:11:31,080
 um fairly reasonably although although

184
00:11:31,080 --> 00:11:37,960
 wrongly became incensed at these monks and and

185
00:11:37,960 --> 00:11:40,920
 shot back at them you're the ones who are corrupt you're

186
00:11:40,920 --> 00:11:43,000
 the bastards

187
00:11:43,000 --> 00:11:45,480
 you consort with women you're the ones who have

188
00:11:45,480 --> 00:11:48,550
 they said all these things that weren't true and these

189
00:11:48,550 --> 00:11:49,400
 monks were shocked and

190
00:11:49,400 --> 00:11:53,800
 they went to see the buddha and they told the buddha this

191
00:11:53,800 --> 00:11:54,360
 and the

192
00:11:54,360 --> 00:11:59,000
 buddha called this monk to him and asked him if it was true

193
00:11:59,000 --> 00:12:02,000
 yes it's strange why did you that well they said these

194
00:12:02,000 --> 00:12:03,560
 things to me well but

195
00:12:03,560 --> 00:12:05,790
 buddha said but they were saying these things because of

196
00:12:05,790 --> 00:12:06,760
 what they'd seen now

197
00:12:06,760 --> 00:12:09,720
 did you see them consorting with women did you

198
00:12:09,720 --> 00:12:12,920
 do you do you see them

199
00:12:13,800 --> 00:12:16,560
 do you have any reason to say these things he says no

200
00:12:16,560 --> 00:12:17,640
 actually

201
00:12:17,640 --> 00:12:20,360
 actually i'm

202
00:12:20,360 --> 00:12:26,840
 i just made that all up

203
00:12:26,840 --> 00:12:31,240
 and the buddha said when they you know for you

204
00:12:31,240 --> 00:12:34,280
 this is so this bad thing has happened to you because of

205
00:12:34,280 --> 00:12:37,530
 things you've done in your past life there's something that

206
00:12:37,530 --> 00:12:38,440
 you have to bear

207
00:12:38,440 --> 00:12:42,840
 with and so then he told this verse

208
00:12:42,840 --> 00:12:46,280
 so don't speak harshly so

209
00:12:46,280 --> 00:12:51,000
 the story is interesting and it provides a little bit of

210
00:12:51,000 --> 00:12:58,200
 interesting context and the general sort of philosophical

211
00:12:58,200 --> 00:13:02,840
 topic here is about misconception and what sort of

212
00:13:02,840 --> 00:13:04,760
 misconception is

213
00:13:04,760 --> 00:13:11,720
 is important and what sort is not if you see

214
00:13:11,720 --> 00:13:15,130
 these monks who saw this woman behind the monk and they had

215
00:13:15,130 --> 00:13:16,600
 no reason

216
00:13:16,600 --> 00:13:21,320
 to doubt it you know their their claim that this man was

217
00:13:21,320 --> 00:13:21,560
 being

218
00:13:21,560 --> 00:13:28,200
 followed by a monk was wrong but reasonable was

219
00:13:28,200 --> 00:13:32,520
 on an ultimate sense on an ultimate level it was

220
00:13:32,520 --> 00:13:36,230
 it was justified but this is the the monk's

221
00:13:36,230 --> 00:13:38,120
 misunderstanding

222
00:13:38,120 --> 00:13:41,400
 the monk's wrong

223
00:13:41,400 --> 00:13:44,590
 it doesn't even seem like a misunderstanding it seems like

224
00:13:44,590 --> 00:13:44,680
 he

225
00:13:44,680 --> 00:13:48,840
 premeditated his misunderstanding wasn't in terms of

226
00:13:48,840 --> 00:13:52,840
 these other monks being bastards or or being

227
00:13:52,840 --> 00:13:56,120
 followed around by women or associating with women

228
00:13:56,120 --> 00:13:58,910
 he knew that was not true but it's still it's a

229
00:13:58,910 --> 00:14:00,200
 misunderstanding you see

230
00:14:00,200 --> 00:14:02,840
 but it's a deeper misunderstanding it's a misunderstanding

231
00:14:02,840 --> 00:14:03,960
 that somehow

232
00:14:03,960 --> 00:14:08,270
 saying these words getting angry even is somehow the right

233
00:14:08,270 --> 00:14:09,560
 answer that there's

234
00:14:09,560 --> 00:14:13,950
 some benefit to these things this is fairly important

235
00:14:13,950 --> 00:14:15,240
 because buddhism

236
00:14:15,240 --> 00:14:20,030
 still considers all evil to come from misunderstanding it's

237
00:14:20,030 --> 00:14:20,040
 a

238
00:14:20,040 --> 00:14:23,580
 you don't just get angry you get angry because you think

239
00:14:23,580 --> 00:14:24,280
 somehow that it's

240
00:14:24,280 --> 00:14:27,400
 good to get angry you don't understand anger

241
00:14:27,400 --> 00:14:33,080
 it's crucial because our ordinary way of dealing with anger

242
00:14:33,080 --> 00:14:36,360
 is when we when we feel that it's wrong is to suppress it

243
00:14:36,360 --> 00:14:37,160
 the same with greed

244
00:14:37,160 --> 00:14:39,960
 when you want something the only way to deal with that is

245
00:14:39,960 --> 00:14:43,160
 to suppress your urge

246
00:14:43,160 --> 00:14:46,930
 so we believe the anger is is is wrong and that the root is

247
00:14:46,930 --> 00:14:49,080
 the anger it's not

248
00:14:49,080 --> 00:14:51,740
 the anger isn't the root problem the greed isn't the root

249
00:14:51,740 --> 00:14:52,200
 problem

250
00:14:52,200 --> 00:14:54,430
 the root problem is you don't understand greed you don't

251
00:14:54,430 --> 00:14:55,400
 understand anger you

252
00:14:55,400 --> 00:14:58,280
 don't understand the things that you're getting greedy

253
00:14:58,280 --> 00:15:01,800
 and angry about you know when you want something it's

254
00:15:01,800 --> 00:15:05,800
 because you think it is stable satisfying controllable

255
00:15:05,800 --> 00:15:09,240
 when you dislike something it's because you feel like you

256
00:15:09,240 --> 00:15:13,720
 you can make it stable you can change it to be

257
00:15:13,720 --> 00:15:20,600
 stable satisfying and controllable and only when you see

258
00:15:20,600 --> 00:15:26,290
 when you see clearly when you come to see the truth about

259
00:15:26,290 --> 00:15:26,840
 this

260
00:15:27,720 --> 00:15:31,320
 because we don't really know about the the other set of

261
00:15:31,320 --> 00:15:33,640
 monks whether they had

262
00:15:33,640 --> 00:15:36,510
 a reaction to these things so they had this experience

263
00:15:36,510 --> 00:15:37,240
 where

264
00:15:37,240 --> 00:15:40,520
 they saw this woman or they even heard about this woman

265
00:15:40,520 --> 00:15:41,320
 following the monk

266
00:15:41,320 --> 00:15:46,040
 around it says they actually saw it but we

267
00:15:46,040 --> 00:15:49,160
 don't know what their reaction was if their reaction was

268
00:15:49,160 --> 00:15:50,200
 was was neutral and

269
00:15:50,200 --> 00:15:55,160
 and objective or whether their reaction was angry

270
00:15:55,160 --> 00:15:58,520
 but it's reasonable and and their misunderstanding was only

271
00:15:58,520 --> 00:15:59,400
 in an abstract

272
00:15:59,400 --> 00:16:04,840
 sense in terms of concepts so the concepts

273
00:16:04,840 --> 00:16:08,660
 they got the concepts wrong they they thought this was a

274
00:16:08,660 --> 00:16:09,480
 real woman

275
00:16:09,480 --> 00:16:13,150
 when in fact it was just a phantom woman but in an ultimate

276
00:16:13,150 --> 00:16:14,120
 sense

277
00:16:14,120 --> 00:16:16,700
 they were they were saying it was factual this monk appears

278
00:16:16,700 --> 00:16:17,000
 to be

279
00:16:17,000 --> 00:16:20,200
 followed around by woman

280
00:16:21,720 --> 00:16:25,250
 and so how this relates in general to all of us because i

281
00:16:25,250 --> 00:16:26,200
 don't think

282
00:16:26,200 --> 00:16:30,440
 these circumstances are all that common

283
00:16:30,440 --> 00:16:33,470
 but obviously the general circumstances of finding yourself

284
00:16:33,470 --> 00:16:34,520
 being accused

285
00:16:34,520 --> 00:16:37,720
 wrongfully of finders of finding yourself being

286
00:16:37,720 --> 00:16:43,100
 abused finding yourself being manipulated or taken

287
00:16:43,100 --> 00:16:44,520
 advantage of

288
00:16:44,520 --> 00:16:51,000
 finding yourself being unfairly treated now sometimes it's

289
00:16:51,000 --> 00:16:51,320
 with

290
00:16:51,320 --> 00:16:56,040
 reasons sometimes justified sometimes you really are being

291
00:16:56,040 --> 00:16:59,400
 unfairly treated sometimes there's malicious intent

292
00:16:59,400 --> 00:17:02,600
 but sometimes there's not

293
00:17:02,600 --> 00:17:07,000
 and well i mean and and whether there is or there isn't

294
00:17:07,000 --> 00:17:11,180
 actually is not the the important and the most crucial

295
00:17:11,180 --> 00:17:12,200
 aspect i mean if you

296
00:17:12,200 --> 00:17:16,590
 know you're being wrongfully treated this monk should have

297
00:17:16,590 --> 00:17:17,320
 you know he was

298
00:17:17,320 --> 00:17:20,600
 fully in his right to say i'm not being followed by woman

299
00:17:20,600 --> 00:17:23,400
 that's what he did in the beginning yes that's not true

300
00:17:23,400 --> 00:17:24,440
 there is no woman

301
00:17:24,440 --> 00:17:29,660
 i've never consorted with a woman it's just not true and

302
00:17:29,660 --> 00:17:31,400
 and but i mean

303
00:17:31,400 --> 00:17:35,640
 the important point is you can't control

304
00:17:35,640 --> 00:17:44,200
 your the results of your situation you can't always solve

305
00:17:44,200 --> 00:17:49,160
 everything and everyone has experience as a

306
00:17:49,160 --> 00:17:53,320
 misunderstanding sometimes you're reconcilable

307
00:17:53,320 --> 00:17:57,050
 sometimes to the point that people actually think you're a

308
00:17:57,050 --> 00:17:58,520
 bad person

309
00:17:58,520 --> 00:18:02,320
 you know as in this case when in fact you've done nothing

310
00:18:02,320 --> 00:18:02,840
 wrong

311
00:18:02,840 --> 00:18:06,040
 just because you didn't have a chance to clear your name it

312
00:18:06,040 --> 00:18:06,040
's

313
00:18:06,040 --> 00:18:10,920
 it's it does speak to the importance of

314
00:18:10,920 --> 00:18:15,080
 you know trying to trying to explain yourself

315
00:18:16,200 --> 00:18:19,720
 but in the end you're never going to fix everything

316
00:18:19,720 --> 00:18:23,880
 you can't fix life and so you're always going to be

317
00:18:23,880 --> 00:18:26,950
 it's just another situation where there's going to be the

318
00:18:26,950 --> 00:18:27,720
 potential for

319
00:18:27,720 --> 00:18:32,040
 suffering where there is going to be an unpleasant

320
00:18:32,040 --> 00:18:35,780
 experience apply an experience that has the potential for

321
00:18:35,780 --> 00:18:37,320
 the arising of

322
00:18:37,320 --> 00:18:43,320
 of anger of dislike of aversion and so this monk of course

323
00:18:43,320 --> 00:18:44,200
 became terribly

324
00:18:44,200 --> 00:18:48,520
 adverse simply based on his not on his

325
00:18:48,520 --> 00:18:50,750
 misunderstanding of the situation but on his

326
00:18:50,750 --> 00:18:54,760
 misunderstanding of the right

327
00:18:54,760 --> 00:18:57,470
 of the reality of the situation therefore the right

328
00:18:57,470 --> 00:18:59,640
 response

329
00:18:59,640 --> 00:19:03,710
 or the nature of reality he didn't understand that getting

330
00:19:03,710 --> 00:19:04,760
 anger was a bad

331
00:19:04,760 --> 00:19:08,760
 thing and understand what that others might

332
00:19:08,760 --> 00:19:12,440
 well there will be results there will be consequences

333
00:19:12,440 --> 00:19:17,800
 dukkha he does it say dukkha he

334
00:19:17,800 --> 00:19:26,760
 sarambha kata there indeed angry words are unpleasant

335
00:19:26,760 --> 00:19:31,240
 are stressful are suffering dukkha

336
00:19:31,240 --> 00:19:35,000
 it might lead to blows even

337
00:19:35,000 --> 00:19:38,520
 the next verse is actually a sort of a generally good

338
00:19:38,520 --> 00:19:43,240
 um sort of buddhist i don't know what the word is saying or

339
00:19:43,240 --> 00:19:44,360
 aphorism is that the

340
00:19:44,360 --> 00:19:47,800
 word if you keep yourself silent as a broken

341
00:19:47,800 --> 00:19:54,200
 gong you have already reached already reached

342
00:19:54,200 --> 00:19:59,960
 you reach i don't think it's already oh patossi has passed

343
00:20:01,000 --> 00:20:10,090
 asi pato asi maybe the arahant keeps himself silent

344
00:20:10,090 --> 00:20:10,200
 remember the

345
00:20:10,200 --> 00:20:14,220
 story of this arahant who was accused of stealing a ruby i

346
00:20:14,220 --> 00:20:14,920
 think we had this

347
00:20:14,920 --> 00:20:19,000
 story already right and it turned out this bird had eaten

348
00:20:19,000 --> 00:20:22,120
 it but he wouldn't turn in the bird because he

349
00:20:22,120 --> 00:20:25,480
 knew if he did the bird would be killed and he didn't want

350
00:20:25,480 --> 00:20:26,280
 the to be responsible

351
00:20:26,280 --> 00:20:29,080
 for that bad karma so he kept completely silent even as he

352
00:20:29,080 --> 00:20:30,040
 was being

353
00:20:30,040 --> 00:20:36,040
 tortured as the thief

354
00:20:36,040 --> 00:20:39,720
 as the suspect for the thief

355
00:20:39,720 --> 00:20:44,500
 be silent as a broken gong i mean this is it speaks to

356
00:20:44,500 --> 00:20:45,400
 patience

357
00:20:45,400 --> 00:20:48,660
 you'd be patient with samsara this there's another story of

358
00:20:48,660 --> 00:20:49,560
 this novice who

359
00:20:49,560 --> 00:20:54,040
 had his eye poked out and so all he did is cover his eye

360
00:20:54,040 --> 00:20:54,120
 and

361
00:20:54,120 --> 00:20:56,580
 then when he went to go offer water to his teacher who had

362
00:20:56,580 --> 00:20:58,280
 poked his eye out

363
00:20:58,280 --> 00:21:01,640
 he offered with one hand and the teacher asked why are you

364
00:21:01,640 --> 00:21:03,160
 offering with one hand

365
00:21:03,160 --> 00:21:08,520
 well my other hand's occupied but he was an arahant as well

366
00:21:08,520 --> 00:21:13,240
 and so he also wasn't upset and the teacher was

367
00:21:13,240 --> 00:21:19,240
 was mortified and he begged forgiveness and the novice said

368
00:21:19,240 --> 00:21:24,200
 it's not your fault venerables or it's the fault of samsara

369
00:21:24,200 --> 00:21:28,200
 and again this is the case with this story our situations

370
00:21:28,200 --> 00:21:29,400
 are

371
00:21:29,400 --> 00:21:32,720
 the best best answer is they're the fault of samsara it's

372
00:21:32,720 --> 00:21:33,400
 the way of

373
00:21:33,400 --> 00:21:36,760
 the way things go it's maybe not the way things had to go

374
00:21:36,760 --> 00:21:37,400
 they could have gone

375
00:21:37,400 --> 00:21:41,400
 differently but the point is that it's part you

376
00:21:41,400 --> 00:21:45,640
 know it's how samsara works sometimes misunderstandings

377
00:21:45,640 --> 00:21:46,920
 arise it's not

378
00:21:46,920 --> 00:21:51,720
 always clear it's not always pleasant

379
00:21:51,720 --> 00:21:54,470
 unpleasantness can arise from many different things from

380
00:21:54,470 --> 00:21:55,240
 natural

381
00:21:55,240 --> 00:22:00,040
 disasters from human violence and it can arise from simple

382
00:22:00,040 --> 00:22:02,120
 misunderstandings

383
00:22:02,120 --> 00:22:05,160
 you might call it a sort of structural violence where the

384
00:22:05,160 --> 00:22:06,200
 structure rather than

385
00:22:06,200 --> 00:22:11,160
 the people is a cause for suffering

386
00:22:11,160 --> 00:22:14,520
 there's no one to blame for it not directly

387
00:22:14,520 --> 00:22:21,640
 but suffering is is potentially inbuilt into the structure

388
00:22:23,320 --> 00:22:25,620
 so in terms of our meditation we try to just be mindful

389
00:22:25,620 --> 00:22:27,320
 rather than trying to

390
00:22:27,320 --> 00:22:31,160
 fix things because fixes are not consistent

391
00:22:31,160 --> 00:22:34,760
 they're not consistently attainable you can't

392
00:22:34,760 --> 00:22:40,800
 you can't always fix your problems and so trying to make

393
00:22:40,800 --> 00:22:42,600
 that a solution

394
00:22:42,600 --> 00:22:48,840
 is the wrong path the wrong the wrong way to be

395
00:22:48,840 --> 00:22:52,190
 the wrong way to be instead of trying to find solutions to

396
00:22:52,190 --> 00:22:53,800
 our problems

397
00:22:53,800 --> 00:22:56,760
 we learn to unravel them and to just be with them

398
00:22:56,760 --> 00:23:01,500
 so just to stop seeing them as problems to see through the

399
00:23:01,500 --> 00:23:02,280
 problems to the

400
00:23:02,280 --> 00:23:08,680
 ultimate reality and to understand the experiences and the

401
00:23:08,680 --> 00:23:14,040
 objects of experience objectively without judgment without

402
00:23:14,040 --> 00:23:15,800
 partiality

403
00:23:15,800 --> 00:23:19,640
 without expectation this is what we do throughout the

404
00:23:19,640 --> 00:23:24,040
 meditation we become like a broken gong that just

405
00:23:24,040 --> 00:23:27,320
 sits there it doesn't ever ring doesn't ever

406
00:23:27,320 --> 00:23:31,880
 react you know if you're wrung a broken gong it doesn't

407
00:23:31,880 --> 00:23:33,000
 reverberate

408
00:23:33,000 --> 00:23:36,680
 just like that there's no anger there's no reaction

409
00:23:36,680 --> 00:23:40,840
 when people when people are nice to you you don't become

410
00:23:40,840 --> 00:23:46,440
 elated when they speak well of you when they praise you

411
00:23:46,440 --> 00:23:52,360
 and when they accuse you when they vilify you

412
00:23:52,360 --> 00:23:55,080
 this is like a gong that doesn't no matter how you hit it

413
00:23:55,080 --> 00:23:55,720
 it doesn't

414
00:23:55,720 --> 00:23:59,320
 reverberate it's broken

415
00:23:59,320 --> 00:24:04,480
 so that is our dhamma for tonight thank you all for tuning

416
00:24:04,480 --> 00:24:04,840
 in we should know

417
00:24:04,840 --> 00:24:09,160
 the best

