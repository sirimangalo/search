1
00:00:00,000 --> 00:00:04,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,000 --> 00:00:09,980
 Today we continue on with verse number 121, which reads as

3
00:00:09,980 --> 00:00:11,000
 follows.

4
00:00:11,000 --> 00:00:22,920
 "Mavamanyeta pappasya namandang agamisati udabindu nipatena

5
00:00:22,920 --> 00:00:33,550
 udakambopi purati bhalo purati pappasya thokang thokampi aj

6
00:00:33,550 --> 00:00:35,000
ilam."

7
00:00:35,000 --> 00:00:47,780
 Which means one should not look down, one should not think

8
00:00:47,780 --> 00:00:50,000
 little of evil.

9
00:00:50,000 --> 00:01:05,000
 Thinking "namandang agamisati" "this will not come to me."

10
00:01:05,000 --> 00:01:15,340
 Someone should not look down upon, not think little of evil

11
00:01:15,340 --> 00:01:16,000
.

12
00:01:16,000 --> 00:01:21,000
 Thinking evil results won't come of them.

13
00:01:21,000 --> 00:01:27,000
 So in the case of just a small evil, they disappoint.

14
00:01:27,000 --> 00:01:36,000
 Because "udabindu nipatena udakumbopi purati"

15
00:01:36,000 --> 00:01:44,000
 Raindrops or water falling one drop at a time.

16
00:01:44,000 --> 00:01:55,000
 Even drops of water are able to fill a water pot.

17
00:01:55,000 --> 00:02:04,000
 Are able to make a flood of water.

18
00:02:04,000 --> 00:02:09,390
 "Udakumba" flood of water or a water jug depending on who

19
00:02:09,390 --> 00:02:10,000
 you ask.

20
00:02:10,000 --> 00:02:17,000
 "Purati" fills the water jug probably.

21
00:02:17,000 --> 00:02:25,550
 In the same way, "balo purati pappasya" the full becomes

22
00:02:25,550 --> 00:02:27,000
 full of evil.

23
00:02:27,000 --> 00:02:30,000
 "Tokang thokampi ajilam"

24
00:02:30,000 --> 00:02:38,000
 Even though "b" it is gathered, "tokang thokang"

25
00:02:38,000 --> 00:02:40,000
 I always remember this "tokang thokang"

26
00:02:40,000 --> 00:02:45,000
 "Tokang" means a bit or a small amount.

27
00:02:45,000 --> 00:02:54,000
 "Tokang thokang" means little by little.

28
00:02:54,000 --> 00:02:59,000
 Little by little one becomes full of evil.

29
00:02:59,000 --> 00:03:07,840
 So this was taught in regards to a certain bhikkhu, a monk

30
00:03:07,840 --> 00:03:10,000
 whose name isn't given.

31
00:03:10,000 --> 00:03:14,400
 But it was a monk who failed to keep, who did something

32
00:03:14,400 --> 00:03:17,000
 actually quite minor.

33
00:03:17,000 --> 00:03:23,280
 And the act itself wasn't necessarily, well it wasn't

34
00:03:23,280 --> 00:03:25,000
 terribly evil.

35
00:03:25,000 --> 00:03:32,000
 You might even say that the act itself was negligible.

36
00:03:32,000 --> 00:03:34,970
 Not something certainly to make a story about, but it's an

37
00:03:34,970 --> 00:03:38,000
 interesting story, it's very short.

38
00:03:38,000 --> 00:03:43,290
 So what happened was he was not, he didn't take care of his

39
00:03:43,290 --> 00:03:44,000
 requisites.

40
00:03:44,000 --> 00:03:47,360
 He didn't take care of the furniture and the bedding and

41
00:03:47,360 --> 00:03:50,000
 his belongings in general.

42
00:03:50,000 --> 00:03:53,840
 Sometimes he'd have to bring a chair outside and then he'd

43
00:03:53,840 --> 00:03:55,000
 leave it outside.

44
00:03:55,000 --> 00:04:00,790
 Or he'd take his bedding outside to air it out, his sheets

45
00:04:00,790 --> 00:04:02,000
 or mattress or something.

46
00:04:02,000 --> 00:04:05,000
 And then he'd leave them outside and then get rained on.

47
00:04:05,000 --> 00:04:10,260
 Or he would leave things unattended or uncared for and the

48
00:04:10,260 --> 00:04:13,000
 mice would eat them or the ants would eat them or so on.

49
00:04:13,000 --> 00:04:16,100
 And so he went through a lot of stuff and a lot of the

50
00:04:16,100 --> 00:04:20,540
 belongings, not just of his but of the sangha, of the mon

51
00:04:20,540 --> 00:04:23,000
asteries were ruined as a result.

52
00:04:23,000 --> 00:04:26,110
 So again it's just one of those things that shouldn't be

53
00:04:26,110 --> 00:04:29,000
 done but it's not like he's going to hell for it.

54
00:04:29,000 --> 00:04:31,890
 So the monks admonished him, they said, "You know look, you

55
00:04:31,890 --> 00:04:33,000
 shouldn't do this.

56
00:04:33,000 --> 00:04:36,530
 This is where they ask him, don't you think you should put

57
00:04:36,530 --> 00:04:39,000
 your stuff away and take care of it?"

58
00:04:39,000 --> 00:04:42,570
 And he would say to them, "You know it's just a trifling,

59
00:04:42,570 --> 00:04:46,550
 it's not a big deal. Really it's not even really worth

60
00:04:46,550 --> 00:04:48,000
 worrying about.

61
00:04:48,000 --> 00:04:52,860
 We've got more important things like meditation and study

62
00:04:52,860 --> 00:04:54,000
 and so on."

63
00:04:54,000 --> 00:04:57,770
 And so he would do the same thing again and again, never

64
00:04:57,770 --> 00:05:00,000
 learning, never changing.

65
00:05:00,000 --> 00:05:03,490
 And so the monks kind of got fed up with this and they went

66
00:05:03,490 --> 00:05:06,000
 to the Buddha and they said, "Look what's going on."

67
00:05:06,000 --> 00:05:10,000
 And so the teacher sent, the Buddha sent for him and said,

68
00:05:10,000 --> 00:05:11,000
 "Is it true that you're acting in this way?

69
00:05:11,000 --> 00:05:14,000
 Is it true that you don't put your belongings away?"

70
00:05:14,000 --> 00:05:19,000
 And when admonished that you don't stop, you keep doing it.

71
00:05:19,000 --> 00:05:23,000
 And so even to the Buddha he said the same thing. He said,

72
00:05:23,000 --> 00:05:25,000
 "It's such a small thing.

73
00:05:25,000 --> 00:05:29,520
 I've only done, you know, it's wrong but it's such a small

74
00:05:29,520 --> 00:05:32,000
 wrong, why worry about it?

75
00:05:32,000 --> 00:05:35,000
 It's not worth worrying about."

76
00:05:35,000 --> 00:05:39,000
 And the Buddha took this as an opportunity to teach.

77
00:05:39,000 --> 00:05:43,210
 So he was concerned, not exactly with the act, although he

78
00:05:43,210 --> 00:05:48,000
 did end up making a rule against leaving stuff out.

79
00:05:48,000 --> 00:05:52,460
 But he was very, he seems concerned with the attitude, this

80
00:05:52,460 --> 00:05:58,560
 attitude of making little of your faults or dismissing

81
00:05:58,560 --> 00:06:00,000
 small faults.

82
00:06:00,000 --> 00:06:03,970
 Which is interesting because there's this idea of being

83
00:06:03,970 --> 00:06:07,000
 easy going and don't sweat the small stuff.

84
00:06:07,000 --> 00:06:11,040
 And there's something to that, certainly. You shouldn't

85
00:06:11,040 --> 00:06:14,000
 make things bigger than they actually are.

86
00:06:14,000 --> 00:06:17,850
 But you can't leave, it's like you can't leave an infection

87
00:06:17,850 --> 00:06:19,000
 unattended.

88
00:06:19,000 --> 00:06:24,000
 As far as evil goes, there's no such thing as little.

89
00:06:24,000 --> 00:06:27,380
 And there's another quote in here that I think we've

90
00:06:27,380 --> 00:06:30,540
 skipped over, comes from another one, and I was trying to

91
00:06:30,540 --> 00:06:31,000
 remember it.

92
00:06:31,000 --> 00:06:36,000
 It's, "In regards to evil, apakanti nabhaman nitabang."

93
00:06:36,000 --> 00:06:40,000
 It is not proper to say apakang, it's just a little.

94
00:06:40,000 --> 00:06:44,000
 You should never look upon an evil deed as it's too little.

95
00:06:44,000 --> 00:06:48,000
 So the deed itself I don't think is terribly an evil thing.

96
00:06:48,000 --> 00:06:52,000
 It's negligent to not look after your belongings.

97
00:06:52,000 --> 00:06:54,670
 But the attitude is much more important, and the import of

98
00:06:54,670 --> 00:06:59,150
 this verse is, of course, far beyond leaving stuff out and

99
00:06:59,150 --> 00:07:00,000
 so on.

100
00:07:00,000 --> 00:07:06,010
 It's about the attitude of making light of something that

101
00:07:06,010 --> 00:07:12,000
 is negligent, making light of anything that is evil.

102
00:07:12,000 --> 00:07:15,160
 And this goes back to what we were talking about in the

103
00:07:15,160 --> 00:07:18,000
 past couple of verses and even earlier verses.

104
00:07:18,000 --> 00:07:25,410
 Like this monk who did whatever he wanted, aided, and was

105
00:07:25,410 --> 00:07:31,000
 sexually active and so on, and it seemed pleasant.

106
00:07:31,000 --> 00:07:36,460
 I mean, when he wasn't doing what he wanted, he was

107
00:07:36,460 --> 00:07:41,000
 miserable, and he was suffering physically.

108
00:07:41,000 --> 00:07:45,100
 And so then when he started doing whatever he wanted, he

109
00:07:45,100 --> 00:07:48,000
 actually flourished and prospered. He was healthier and

110
00:07:48,000 --> 00:07:48,000
 happier and so on.

111
00:07:48,000 --> 00:07:53,000
 So it looked good. It was like, "Well, that's great."

112
00:07:53,000 --> 00:07:55,700
 I think meditators, when they come here, they often fall

113
00:07:55,700 --> 00:07:57,000
 into this kind of doubt.

114
00:07:57,000 --> 00:07:59,960
 They think, "What am I doing here again? If I want to stop

115
00:07:59,960 --> 00:08:02,260
 the suffering, we're talking about ending suffering, why

116
00:08:02,260 --> 00:08:04,000
 don't I just go home?"

117
00:08:04,000 --> 00:08:07,000
 Then there would be no suffering.

118
00:08:07,000 --> 00:08:09,960
 It's actually quite remarkable that we come to meditate at

119
00:08:09,960 --> 00:08:13,230
 all, especially with all the wonderful things out there,

120
00:08:13,230 --> 00:08:18,950
 wonderful ways of finding happiness, finding pleasure

121
00:08:18,950 --> 00:08:20,000
 anyway.

122
00:08:20,000 --> 00:08:24,950
 But for some of us, for those of us who are perhaps more

123
00:08:24,950 --> 00:08:29,000
 introspective and maybe more observant,

124
00:08:29,000 --> 00:08:33,070
 we come to see that it's not actually satisfaction, it's

125
00:08:33,070 --> 00:08:37,100
 not actually happiness, and this is concerning as you watch

126
00:08:37,100 --> 00:08:40,000
 yourself filling yourself up, not with happiness,

127
00:08:40,000 --> 00:08:44,210
 but with desire, with greed, with attachment, and

128
00:08:44,210 --> 00:08:46,000
 ultimately with evil.

129
00:08:46,000 --> 00:08:49,400
 Evil meaning those things that cause you suffering. So you

130
00:08:49,400 --> 00:08:53,000
 become more addicted, and that addiction is pretty evil.

131
00:08:53,000 --> 00:08:57,000
 I mean, in a sense, it brings suffering to you.

132
00:08:57,000 --> 00:09:03,000
 And also anger and frustration and boredom and conflict.

133
00:09:03,000 --> 00:09:05,890
 Just conflict, whenever you can't get what you want or

134
00:09:05,890 --> 00:09:09,000
 someone stands in the way of you getting what you want.

135
00:09:09,000 --> 00:09:17,370
 If you have enough desire, you will create great conflict

136
00:09:17,370 --> 00:09:20,000
 and adversity.

137
00:09:20,000 --> 00:09:26,360
 You can even go to war over greed in this world. Many wars

138
00:09:26,360 --> 00:09:35,000
 started just over simple greed.

139
00:09:35,000 --> 00:09:39,460
 And so it takes real introspection to get there, but that

140
00:09:39,460 --> 00:09:44,000
 is the underlying truth, is that the evil doesn't go away.

141
00:09:44,000 --> 00:09:47,120
 If you cultivate addiction, you become more addicted. If

142
00:09:47,120 --> 00:09:50,900
 you cultivate anger and aversion, you become more set in

143
00:09:50,900 --> 00:09:52,000
 those ways.

144
00:09:52,000 --> 00:09:56,000
 It's how habits are formed. It's how we become who we are.

145
00:09:56,000 --> 00:10:02,060
 We come to meditate and we think that we're surprised at

146
00:10:02,060 --> 00:10:06,430
 how much we think and how much anger and aversion and how

147
00:10:06,430 --> 00:10:10,650
 much greed and attachment and all these things that are

148
00:10:10,650 --> 00:10:11,000
 inside.

149
00:10:11,000 --> 00:10:14,650
 Where did they all come from, we think? This is like you

150
00:10:14,650 --> 00:10:17,250
 have water dripping into the pot, and then you look down

151
00:10:17,250 --> 00:10:19,000
 and you say, "Oh, the pot's quite full."

152
00:10:19,000 --> 00:10:22,990
 And you don't know how did it get full, but actually the

153
00:10:22,990 --> 00:10:27,000
 water was dripping down. The mind is the same.

154
00:10:27,000 --> 00:10:30,060
 When you open it up and look, when you actually take a look

155
00:10:30,060 --> 00:10:33,000
, that's when you see what you're doing to yourself.

156
00:10:33,000 --> 00:10:39,020
 For the most part, human beings are protected from the

157
00:10:39,020 --> 00:10:42,000
 results on a large scale.

158
00:10:42,000 --> 00:10:46,380
 For the most part, we can avoid the suffering that comes

159
00:10:46,380 --> 00:10:51,000
 from not getting what we want or getting what we don't want

160
00:10:51,000 --> 00:10:51,000
.

161
00:10:51,000 --> 00:10:55,410
 We're able to, because we live in kind of a paradise, many

162
00:10:55,410 --> 00:11:00,000
 of us, not all of us, of course, relative to other animals,

163
00:11:00,000 --> 00:11:03,610
 for sure we live in a, many of us live in quite a paradise,

164
00:11:03,610 --> 00:11:06,000
 always able to get food every day.

165
00:11:06,000 --> 00:11:11,000
 I mean, just that is quite remarkable.

166
00:11:11,000 --> 00:11:13,820
 There's no reason why we should be able to get enough food

167
00:11:13,820 --> 00:11:16,000
 to eat, but we do get more than enough food.

168
00:11:16,000 --> 00:11:20,760
 And further than that, we get pleasure and entertainment

169
00:11:20,760 --> 00:11:23,000
 and all of these things.

170
00:11:23,000 --> 00:11:26,380
 And so we aren't able to see what the result is of

171
00:11:26,380 --> 00:11:30,000
 addiction, what the result is of aversion.

172
00:11:30,000 --> 00:11:33,000
 When we don't like something, we have ways of removing it.

173
00:11:33,000 --> 00:11:38,000
 You have pests in your home, you use pesticides.

174
00:11:38,000 --> 00:11:42,000
 We have many, many ways to get rid of the problems.

175
00:11:42,000 --> 00:11:46,010
 You have a headache, take a pill, you have a backache, get

176
00:11:46,010 --> 00:11:47,000
 a massage.

177
00:11:47,000 --> 00:11:51,000
 You feel bored, there's lots of things to entertain you.

178
00:11:51,000 --> 00:11:54,940
 And so little by little we fill up our pot, not with good,

179
00:11:54,940 --> 00:11:56,000
 but with evil.

180
00:11:56,000 --> 00:11:59,040
 And the next verse, if you could guess, the next verse is

181
00:11:59,040 --> 00:12:01,000
 going to be about doing good.

182
00:12:01,000 --> 00:12:05,010
 You shouldn't, shouldn't, well, we'll talk about that one

183
00:12:05,010 --> 00:12:06,000
 next time.

184
00:12:06,000 --> 00:12:10,070
 But what I wanted to say especially is meditation changes

185
00:12:10,070 --> 00:12:11,000
 all that.

186
00:12:11,000 --> 00:12:15,000
 Meditation allows you to see.

187
00:12:15,000 --> 00:12:19,000
 And this is really how we understand karma in Buddhism.

188
00:12:19,000 --> 00:12:22,000
 People talk about karma as being a kind of a belief and it

189
00:12:22,000 --> 00:12:23,000
's really not.

190
00:12:23,000 --> 00:12:27,030
 Most people can't, have to believe it because they aren't

191
00:12:27,030 --> 00:12:28,000
 able to see it.

192
00:12:28,000 --> 00:12:30,360
 But karma is something that you have to see on the moment

193
00:12:30,360 --> 00:12:31,000
ary level.

194
00:12:31,000 --> 00:12:33,840
 A person who practices insight meditation is able to

195
00:12:33,840 --> 00:12:37,000
 understand karma, is able to see it.

196
00:12:37,000 --> 00:12:39,000
 Because you are able to see moment to moment.

197
00:12:39,000 --> 00:12:41,350
 When I cling to things, it just leads to stress, it leads

198
00:12:41,350 --> 00:12:44,000
 to suffering, it leads to busyness.

199
00:12:44,000 --> 00:12:47,810
 It doesn't make me more content, more satisfied, more happy

200
00:12:47,810 --> 00:12:48,000
.

201
00:12:48,000 --> 00:12:51,150
 When I get angry, the same thing, anger is just painful and

202
00:12:51,150 --> 00:12:53,000
 unpleasant, it causes suffering.

203
00:12:53,000 --> 00:12:56,230
 On the other hand, when I cultivate mindfulness, when I

204
00:12:56,230 --> 00:12:59,150
 cultivate clarity, in that moment, it's sending out

205
00:12:59,150 --> 00:13:00,000
 goodness.

206
00:13:00,000 --> 00:13:04,000
 It's rippling out the effects of it.

207
00:13:04,000 --> 00:13:07,000
 You can see. You don't know where it's going.

208
00:13:07,000 --> 00:13:10,460
 Just like evil, when you become attached, it goes into the

209
00:13:10,460 --> 00:13:13,000
 mix in your brain and it affects your brain and your body.

210
00:13:13,000 --> 00:13:15,490
 You don't really see where it's going until later when it

211
00:13:15,490 --> 00:13:17,000
 gets full and then it overflows.

212
00:13:17,000 --> 00:13:19,000
 And then you see the result.

213
00:13:19,000 --> 00:13:21,000
 But when you meditate, you can see the source.

214
00:13:21,000 --> 00:13:23,000
 You can see what you're sending out.

215
00:13:23,000 --> 00:13:27,000
 You get to see how your reactions are affecting your brain,

216
00:13:27,000 --> 00:13:30,000
 are affecting your body, are affecting the world around you

217
00:13:30,000 --> 00:13:33,000
, the people around you, and everything you say and you do.

218
00:13:33,000 --> 00:13:35,000
 When you're mindful, you can see all this.

219
00:13:35,000 --> 00:13:37,000
 It's a great thing about meditation.

220
00:13:37,000 --> 00:13:40,220
 It's one of the first things you learn in the meditation

221
00:13:40,220 --> 00:13:41,000
 practice.

222
00:13:41,000 --> 00:13:44,000
 Not intellectually, but you see it.

223
00:13:44,000 --> 00:13:46,000
 You see how the mind works.

224
00:13:46,000 --> 00:13:49,000
 You see how karma works.

225
00:13:49,000 --> 00:13:56,930
 You lose this doubt about good and evil and right and wrong

226
00:13:56,930 --> 00:13:57,000
.

227
00:13:57,000 --> 00:13:59,000
 You come to understand that there is a result.

228
00:13:59,000 --> 00:14:05,000
 There are consequences to our action.

229
00:14:05,000 --> 00:14:08,000
 This is important for our meditation.

230
00:14:08,000 --> 00:14:12,210
 It's an important encouragement, I think, especially for

231
00:14:12,210 --> 00:14:15,000
 those of us who are meditating to verify.

232
00:14:15,000 --> 00:14:18,660
 And to say to ourselves, "This is a great thing I'm doing

233
00:14:18,660 --> 00:14:20,000
 and I'm able to see this.

234
00:14:20,000 --> 00:14:27,070
 That I can adjust and that I can change my habits so that I

235
00:14:27,070 --> 00:14:31,120
 fill my pot up, not with addiction and aversion, but with

236
00:14:31,120 --> 00:14:35,000
 mindfulness and wisdom."

237
00:14:35,000 --> 00:14:39,000
 So that's the Dhammapada short story.

238
00:14:39,000 --> 00:14:43,000
 Wonderful verse.

239
00:14:43,000 --> 00:14:45,000
 Thank you all for tuning in.

240
00:14:45,000 --> 00:14:46,000
 I'm wishing you all the best.

