1
00:00:00,000 --> 00:00:06,350
 Hi, so today I'm going to talk a little bit about the word

2
00:00:06,350 --> 00:00:07,000
 "religion".

3
00:00:07,000 --> 00:00:12,480
 Religion is a very interesting topic for me as a Buddhist

4
00:00:12,480 --> 00:00:13,000
 monk.

5
00:00:13,000 --> 00:00:20,000
 And I think it's one that's very misunderstood in the world

6
00:00:20,000 --> 00:00:20,000
,

7
00:00:20,000 --> 00:00:24,000
 both by people who claim to be religious and by those who

8
00:00:24,000 --> 00:00:26,000
 say they're not religious.

9
00:00:26,000 --> 00:00:34,000
 And we see that in general the word "religion" comes to be

10
00:00:34,000 --> 00:00:36,000
 a dichotomy

11
00:00:36,000 --> 00:00:41,000
 between whether you believe in God or do not believe in God

12
00:00:41,000 --> 00:00:41,000
.

13
00:00:41,000 --> 00:00:44,600
 So in terms of religion we have the theists and we have the

14
00:00:44,600 --> 00:00:45,000
 atheists.

15
00:00:45,000 --> 00:00:47,450
 And then we have this sort of gray area in the middle where

16
00:00:47,450 --> 00:00:49,000
 people are agnostic.

17
00:00:49,000 --> 00:00:52,900
 And we can see it all in some way or other has to do with

18
00:00:52,900 --> 00:00:54,000
 belief in God.

19
00:00:54,000 --> 00:00:57,290
 And I think people would be shocked, many people would be

20
00:00:57,290 --> 00:00:58,000
 surprised at least,

21
00:00:58,000 --> 00:01:02,090
 to find that there's an entirely different way of looking

22
00:01:02,090 --> 00:01:03,000
 at religion

23
00:01:03,000 --> 00:01:07,000
 than having to do with whether you believe in God or not.

24
00:01:07,000 --> 00:01:11,080
 In fact it's clear that the word "religion" may have very

25
00:01:11,080 --> 00:01:14,000
 little to do with a belief in God.

26
00:01:14,000 --> 00:01:17,570
 The word "religion" could simply mean being bound, being

27
00:01:17,570 --> 00:01:19,000
 bound to a lifestyle,

28
00:01:19,000 --> 00:01:22,870
 being bound to a way of living, it could have to do with

29
00:01:22,870 --> 00:01:24,000
 monastic life,

30
00:01:24,000 --> 00:01:29,160
 it could even simply come from the word "religion" as

31
00:01:29,160 --> 00:01:31,000
 opposed to "negligence",

32
00:01:31,000 --> 00:01:35,360
 which means caution. The opposite of negligence, which is

33
00:01:35,360 --> 00:01:39,000
 to be cautious and to be careful.

34
00:01:39,000 --> 00:01:42,310
 And this is coming from a Buddhist perspective where we

35
00:01:42,310 --> 00:01:44,000
 believe very much in being careful

36
00:01:44,000 --> 00:01:48,000
 and in being bound to certain ethical principles.

37
00:01:48,000 --> 00:01:52,030
 So we see that when the dichotomy is between being theistic

38
00:01:52,030 --> 00:01:55,000
 or atheistic,

39
00:01:55,000 --> 00:01:59,000
 the whole idea of morals is somehow problematic.

40
00:01:59,000 --> 00:02:03,650
 It's problematic for the atheists in finding a reason not

41
00:02:03,650 --> 00:02:05,000
 to do bad things.

42
00:02:05,000 --> 00:02:10,000
 And as a result their morality often becomes relative.

43
00:02:10,000 --> 00:02:13,490
 So in certain instances one might not kill, in other

44
00:02:13,490 --> 00:02:15,000
 instances one might kill.

45
00:02:15,000 --> 00:02:20,000
 Because there's nothing intrinsically wrong with killing.

46
00:02:20,000 --> 00:02:24,450
 Why? Because there's no belief in God who is the ultimate

47
00:02:24,450 --> 00:02:26,000
 source of morality,

48
00:02:26,000 --> 00:02:30,000
 according to both the theists and the atheists.

49
00:02:30,000 --> 00:02:33,970
 For the theists it presents a problem because you've got

50
00:02:33,970 --> 00:02:35,000
 this dilemma.

51
00:02:35,000 --> 00:02:38,470
 Is something wrong because God says it's wrong or does God

52
00:02:38,470 --> 00:02:40,000
 say it's wrong because it's wrong?

53
00:02:40,000 --> 00:02:42,000
 And either way you have a problem.

54
00:02:42,000 --> 00:02:45,000
 If you say it's wrong just because God says it's wrong,

55
00:02:45,000 --> 00:02:48,120
 well then the question is, what if God says that it's okay

56
00:02:48,120 --> 00:02:51,000
 to do this, to do that, to do the other thing?

57
00:02:51,000 --> 00:02:54,320
 Is it just because God says it's right or God says it's

58
00:02:54,320 --> 00:02:55,000
 wrong?

59
00:02:55,000 --> 00:03:00,840
 Even if it's clearly a wrong thing to do, does that make it

60
00:03:00,840 --> 00:03:03,000
 right just because God says it's right?

61
00:03:03,000 --> 00:03:06,000
 And if not, then what's the need for God?

62
00:03:06,000 --> 00:03:08,140
 What is it that makes something wrong and what makes

63
00:03:08,140 --> 00:03:09,000
 something right?

64
00:03:09,000 --> 00:03:12,540
 And in the tradition that I follow there's a whole other

65
00:03:12,540 --> 00:03:14,000
 way of looking at this.

66
00:03:14,000 --> 00:03:18,000
 And it has to do with, of course, the meditation practice

67
00:03:18,000 --> 00:03:23,000
 and the effect of things on a person's mind.

68
00:03:23,000 --> 00:03:27,000
 The effect that killing and stealing and lying and cheating

69
00:03:27,000 --> 00:03:30,000
 and taking drugs, taking alcohol,

70
00:03:30,000 --> 00:03:32,840
 the effect these things have on who you are and the effect

71
00:03:32,840 --> 00:03:35,000
 they have on the world around you.

72
00:03:35,000 --> 00:03:37,600
 The effect they have on your relationships with other

73
00:03:37,600 --> 00:03:39,000
 people, with other beings,

74
00:03:39,000 --> 00:03:45,000
 and on your journey, your spiritual progress.

75
00:03:45,000 --> 00:03:49,000
 So we look at religion as a very important thing.

76
00:03:49,000 --> 00:03:53,000
 It's something that we bind ourselves to, we feel bound to.

77
00:03:53,000 --> 00:03:55,100
 It's a way we live our lives and we live our lives in a

78
00:03:55,100 --> 00:04:01,000
 very cautious way, trying to do everything based on wisdom,

79
00:04:01,000 --> 00:04:04,220
 based on mindfulness, based on clear awareness of what we

80
00:04:04,220 --> 00:04:05,000
're doing.

81
00:04:05,000 --> 00:04:08,320
 Is this a good thing to do? Am I doing this because I'm

82
00:04:08,320 --> 00:04:11,000
 simply angry or because I'm addicted

83
00:04:11,000 --> 00:04:17,610
 or because I'm looking down or I'm looking at other people

84
00:04:17,610 --> 00:04:21,000
 condescending or jealous or stingy

85
00:04:21,000 --> 00:04:24,000
 or have all these different unpleasant states of mind?

86
00:04:24,000 --> 00:04:27,010
 Or am I doing this to help myself, to help other people, to

87
00:04:27,010 --> 00:04:29,000
 create peace, to create harmony?

88
00:04:29,000 --> 00:04:33,390
 And so this is how we judge and this is how we live our

89
00:04:33,390 --> 00:04:34,000
 lives.

90
00:04:34,000 --> 00:04:36,210
 We live our lives in what we consider to be a very

91
00:04:36,210 --> 00:04:40,000
 religious way, which has nothing to do with the question,

92
00:04:40,000 --> 00:04:43,000
 this seemingly meaningless question.

93
00:04:43,000 --> 00:04:45,300
 To ask the idea of whether I believe in God or not, don't

94
00:04:45,300 --> 00:04:47,000
 believe in God is really ridiculous.

95
00:04:47,000 --> 00:04:51,000
 It has nothing to do with what makes me a religious person.

96
00:04:51,000 --> 00:04:54,770
 My religion is very much how I live my life, who I am, what

97
00:04:54,770 --> 00:04:58,000
 I do, and my own spiritual progress,

98
00:04:58,000 --> 00:05:01,240
 as well as the benefit that my life has on other people for

99
00:05:01,240 --> 00:05:04,000
 their spiritual progress and other beings.

100
00:05:04,000 --> 00:05:08,610
 And so the belief in God has really nothing to do with

101
00:05:08,610 --> 00:05:12,000
 religion and my understanding of it.

102
00:05:12,000 --> 00:05:15,800
 It's only one way of looking at religion and it's very

103
00:05:15,800 --> 00:05:19,000
 irrelevant to the way I look at religion.

104
00:05:19,000 --> 00:05:22,120
 So that's just some thoughts for tonight and I thought I'd

105
00:05:22,120 --> 00:05:24,000
 share those with everybody.

106
00:05:24,000 --> 00:05:25,000
 So thanks for tuning in.

107
00:05:25,000 --> 00:05:26,000
 All the best.

