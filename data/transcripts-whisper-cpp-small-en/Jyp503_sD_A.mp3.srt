1
00:00:00,000 --> 00:00:07,120
 Since it's a complicated world, to what extent should monks

2
00:00:07,120 --> 00:00:09,340
 and nuns, for example in their

3
00:00:09,340 --> 00:00:16,960
 years of formative training, be aware of or learned in

4
00:00:16,960 --> 00:00:19,360
 secular laws?

5
00:00:19,360 --> 00:00:27,090
 So that they don't break them, I suppose, it wouldn't do to

6
00:00:27,090 --> 00:00:30,880
 have monks breaking laws.

7
00:00:30,880 --> 00:00:38,120
 Otherwise trying to think, what exactly does that mean?

8
00:00:38,120 --> 00:00:39,560
 Yeah that's really it, no?

9
00:00:39,560 --> 00:00:45,380
 If what you mean is, should they know that what they're

10
00:00:45,380 --> 00:00:48,400
 doing is against the law in this

11
00:00:48,400 --> 00:00:51,850
 way or that way, then yeah I think that's quite useful

12
00:00:51,850 --> 00:00:53,520
 depending on the law.

13
00:00:53,520 --> 00:00:59,450
 I mean the whole copyright law is maybe an exception in my

14
00:00:59,450 --> 00:01:00,280
 mind.

15
00:01:00,280 --> 00:01:02,480
 Don't quote me on that.

16
00:01:02,480 --> 00:01:05,570
 Don't make a video and put it up on the internet with me

17
00:01:05,570 --> 00:01:06,600
 saying that.

18
00:01:06,600 --> 00:01:15,370
 But other than that I can't think of a reason why they

19
00:01:15,370 --> 00:01:19,560
 would need to know the laws.

20
00:01:19,560 --> 00:01:24,410
 To my understanding, we should not learn, we should

21
00:01:24,410 --> 00:01:27,200
 probably be aware of the laws of

22
00:01:27,200 --> 00:01:33,930
 a country, but we should not study it because it is worldly

23
00:01:33,930 --> 00:01:37,200
 studies and we shouldn't do

24
00:01:37,200 --> 00:01:38,200
 that.

25
00:01:38,200 --> 00:01:45,640
 I think that is so.

26
00:01:45,640 --> 00:01:52,340
 I think as a monk or nun we have the rules, we have the pat

27
00:01:52,340 --> 00:01:55,800
imoka and when we keep it and

28
00:01:55,800 --> 00:02:01,780
 when we take it serious we hardly ever can break seriously

29
00:02:01,780 --> 00:02:04,200
 any law of a country.

30
00:02:04,200 --> 00:02:15,200
 I mean there are some laws like the former mentioned.

31
00:02:15,200 --> 00:02:21,600
 In general if you keep your precepts as a monk, the patim

32
00:02:21,600 --> 00:02:24,880
oka, you can't break really

33
00:02:24,880 --> 00:02:26,860
 the laws of a country.

34
00:02:26,860 --> 00:02:41,280
 So there is not really the need to learn secular laws.

35
00:02:41,280 --> 00:02:46,980
 Yeah I mean the whole premise that somehow the complexity

36
00:02:46,980 --> 00:02:49,520
 of the world requires it is

37
00:02:49,520 --> 00:02:57,880
 not really a sufficient reason to learn about that

38
00:02:57,880 --> 00:03:00,520
 complexity.

39
00:03:00,520 --> 00:03:03,170
 There's nothing wrong with learning complex subjects if

40
00:03:03,170 --> 00:03:04,480
 they're inherently useful like

41
00:03:04,480 --> 00:03:08,360
 how to sew a robe for example.

42
00:03:08,360 --> 00:03:11,270
 It's kind of complicated but there's nothing wrong with

43
00:03:11,270 --> 00:03:12,960
 learning it because it's a good

44
00:03:12,960 --> 00:03:20,320
 thing to learn because it supports your monastic life.

45
00:03:20,320 --> 00:03:23,200
 Learning how to use a computer for example can be useful to

46
00:03:23,200 --> 00:03:24,720
 spread the teaching to people

47
00:03:24,720 --> 00:03:28,200
 who use computers.

48
00:03:28,200 --> 00:03:35,840
 So the complexity is not sufficient but there's nothing

49
00:03:35,840 --> 00:03:40,240
 wrong with learning complex subjects

50
00:03:40,240 --> 00:03:42,360
 to an extent.

51
00:03:42,360 --> 00:03:45,060
 Maybe you could go even further and say that because of the

52
00:03:45,060 --> 00:03:46,440
 complexity we want to try to

53
00:03:46,440 --> 00:03:50,520
 avoid that sort of thing as much as possible.

54
00:03:50,520 --> 00:03:53,550
 I mean hopefully you're not suggesting that monks and nuns

55
00:03:53,550 --> 00:03:55,120
 should try to make their lives

56
00:03:55,120 --> 00:03:59,120
 more complex as the world becomes more complex.

57
00:03:59,120 --> 00:04:01,590
 We're trying to give up the world and we're trying to give

58
00:04:01,590 --> 00:04:03,040
 up the complexity so our lives

59
00:04:03,040 --> 00:04:06,100
 should be as simple as possible especially when the world

60
00:04:06,100 --> 00:04:10,840
 becomes more and more complicated.

61
00:04:10,840 --> 00:04:13,720
 I would think that's reasonable to say.

62
00:04:13,720 --> 00:04:13,740
 [

