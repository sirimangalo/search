1
00:00:00,000 --> 00:00:04,400
 It's about what to do after becoming enlightened.

2
00:00:04,400 --> 00:00:11,720
 It says, "More and more people are waking up spiritually.

3
00:00:11,720 --> 00:00:15,040
 And for most of them, the question becomes, now what?

4
00:00:15,040 --> 00:00:17,520
 Information about life after awakening

5
00:00:17,520 --> 00:00:21,240
 is usually not made public, as we are chanting.

6
00:00:21,240 --> 00:00:23,880
 It's most often shared only between teachers

7
00:00:23,880 --> 00:00:25,680
 and the students.

8
00:00:25,680 --> 00:00:28,520
 The end of your world-- that's the title of the book--

9
00:00:28,520 --> 00:00:31,080
 is his response to a growing need for direction

10
00:00:31,080 --> 00:00:33,200
 in this critical path."

11
00:00:33,200 --> 00:00:37,320
 It's interesting to see the question of whether you

12
00:00:37,320 --> 00:00:39,760
 need guidance after enlightenment.

13
00:00:39,760 --> 00:00:44,360
 Well, that's obviously the difference, I think.

14
00:00:44,360 --> 00:00:46,360
 It's incredibly odd from a Buddhist point of view

15
00:00:46,360 --> 00:00:49,640
 that an enlightened being should need any guidance.

16
00:00:49,640 --> 00:00:51,320
 Yeah, but even in Buddhism, there's

17
00:00:51,320 --> 00:00:56,960
 a difference between an Arahant and a Buddha, right?

18
00:00:56,960 --> 00:01:01,120
 But an Arahant doesn't work to become a Buddha.

19
00:01:01,120 --> 00:01:03,480
 An Arahant doesn't work to become a Buddha.

20
00:01:03,480 --> 00:01:06,700
 They don't have any desire left, so they don't have any

21
00:01:06,700 --> 00:01:07,040
 intention

22
00:01:07,040 --> 00:01:08,560
 to do anything.

23
00:01:08,560 --> 00:01:12,160
 They don't need guidance because they're not going anywhere

24
00:01:12,160 --> 00:01:12,160
.

25
00:01:12,160 --> 00:01:13,160
 They're just unhappy.

26
00:01:13,160 --> 00:01:20,600
 But there's a difference between an Arahant and a Buddha.

27
00:01:20,600 --> 00:01:21,100
 Yes.

28
00:01:24,440 --> 00:01:27,000
 But not in the sense of needing guidance.

29
00:01:27,000 --> 00:01:31,640
 Although, an Arahant would take guidance from a Buddha,

30
00:01:31,640 --> 00:01:35,080
 but it would only be functional.

31
00:01:35,080 --> 00:01:38,220
 So if the Buddha said, no, don't say that, or do say this,

32
00:01:38,220 --> 00:01:40,960
 or go do this, or go do that, they would do it.

33
00:01:40,960 --> 00:01:42,920
 But not in order to become more enlightened,

34
00:01:42,920 --> 00:01:47,640
 or not because they were at a loss for what to do.

35
00:01:53,440 --> 00:01:57,360
 Maybe an Arahant may not necessarily

36
00:01:57,360 --> 00:02:00,800
 know what to do in each situation,

37
00:02:00,800 --> 00:02:04,160
 and be in a worldly realm.

38
00:02:04,160 --> 00:02:06,160
 That wouldn't cause them any suffering.

39
00:02:06,160 --> 00:02:08,800
 It wouldn't be a problem for them.

40
00:02:08,800 --> 00:02:09,400
 No, it isn't.

41
00:02:09,400 --> 00:02:10,960
 OK, so you have a point.

42
00:02:10,960 --> 00:02:12,400
 There is maybe room for it.

43
00:02:12,400 --> 00:02:19,600
 For rules that would benefit others, not the enlightened.

44
00:02:19,600 --> 00:02:24,240
 The enlightened being himself or herself

45
00:02:24,240 --> 00:02:31,040
 would be like a guidance to benefit others

46
00:02:31,040 --> 00:02:34,840
 once you've reached the enlightened.

47
00:02:34,840 --> 00:02:35,840
 Well, that could be.

48
00:02:35,840 --> 00:02:40,080
 But not all Arahants are inclined to benefit others.

49
00:02:40,080 --> 00:02:42,280
 Some Arahants just go off and live in the forest.

50
00:02:42,280 --> 00:02:47,520
 So you could incline them to do that,

51
00:02:47,520 --> 00:02:51,920
 or invite them to do that, or ask them to do that.

52
00:02:51,920 --> 00:02:53,800
 And so there is something on there.

53
00:02:53,800 --> 00:02:58,280
 But it sounds much like the idea of needing guidance and so

54
00:02:58,280 --> 00:02:58,680
 on.

55
00:02:58,680 --> 00:03:02,400
 An Arahant doesn't need guidance.

56
00:03:02,400 --> 00:03:06,480
 Anyway, it just seems like an odd--

57
00:03:06,480 --> 00:03:08,540
 the suspicion, of course, is that they're

58
00:03:08,540 --> 00:03:10,040
 dealing with an enlightenment that we

59
00:03:10,040 --> 00:03:12,160
 wouldn't consider to be enlightened.

60
00:03:12,160 --> 00:03:16,160
 Frank, it has a chapter.

61
00:03:16,160 --> 00:03:17,760
 The title is interesting.

62
00:03:17,760 --> 00:03:20,480
 It's for the memories.

63
00:03:20,480 --> 00:03:21,200
 I got it.

64
00:03:21,200 --> 00:03:22,640
 I lost it.

65
00:03:22,640 --> 00:03:26,480
 It's like the experience that many people

66
00:03:26,480 --> 00:03:30,440
 seem to have, that they experience

67
00:03:30,440 --> 00:03:33,120
 a kind of enlightenment, or something of experience.

68
00:03:33,120 --> 00:03:35,840
 And then it fades away with time.

69
00:03:35,840 --> 00:03:40,440
 Yeah, that's common.

70
00:03:40,440 --> 00:03:43,080
 That's a sign that it's not enlightenment.

71
00:03:43,080 --> 00:03:54,120
 But it's also possible that you see or experience nirvana,

72
00:03:54,120 --> 00:03:57,520
 and then you go back to normal life.

73
00:03:57,520 --> 00:03:58,520
 Yeah, for sure.

74
00:03:58,520 --> 00:04:00,520
 And you still have that.

75
00:04:00,520 --> 00:04:03,000
 Yeah, so you may not have-- you may not

76
00:04:03,000 --> 00:04:09,320
 go back to nirvana for a long time, maybe.

77
00:04:09,320 --> 00:04:10,840
 Certainly possible.

78
00:04:10,840 --> 00:04:15,470
 But what doesn't leave you is the state of being a sotapana

79
00:04:15,470 --> 00:04:15,480
.

80
00:04:15,480 --> 00:04:18,840
 And there's some quality of the mind involved in that

81
00:04:18,840 --> 00:04:19,840
 that doesn't disappear.

82
00:04:19,840 --> 00:04:27,280
 That's totally unalterable.

83
00:04:27,280 --> 00:04:30,840
 What's that quality?

84
00:04:30,840 --> 00:04:36,120
 The state of being a sotapana, sotapati pala,

85
00:04:36,120 --> 00:04:37,600
 the fruit of a sotapana.

86
00:04:37,600 --> 00:04:41,320
 I mean, the details of it, the sotapana

87
00:04:41,320 --> 00:04:46,960
 has no envy or jealousy.

88
00:04:46,960 --> 00:04:47,960
 What's the other one?

89
00:04:47,960 --> 00:04:49,640
 Stinginess or envy.

90
00:04:49,640 --> 00:04:55,080
 They have no wrong view about the existence of a self.

91
00:04:55,080 --> 00:04:57,080
 They have no attachment to rites and rituals.

92
00:04:57,080 --> 00:04:59,900
 And they have no doubt about the Buddha, the Dhamma, and

93
00:04:59,900 --> 00:05:01,560
 the Sangha.

94
00:05:01,560 --> 00:05:08,360
 They are also said to keep [NON-ENGLISH SPEECH]

95
00:05:08,360 --> 00:05:13,520
 pure sila, pure five precepts, the morality that

96
00:05:13,520 --> 00:05:16,840
 is dear to the noble ones, which is said to be the five

97
00:05:16,840 --> 00:05:17,440
 precepts.

98
00:05:17,440 --> 00:05:19,760
 So the idea is that a sotapana would not

99
00:05:19,760 --> 00:05:21,200
 break any of the five precepts.

100
00:05:21,200 --> 00:05:26,760
 I have a question.

101
00:05:26,760 --> 00:05:30,440
 Is the concept of a sotapana present in Yandri tradition

102
00:05:30,440 --> 00:05:32,760
 the Buddhist move?

103
00:05:32,760 --> 00:05:33,760
 I can't really say.

104
00:05:33,760 --> 00:05:38,880
 I have real difficulty with other tradition questions

105
00:05:38,880 --> 00:05:42,480
 because I don't study them.

106
00:05:42,480 --> 00:05:43,640
 Suppose maybe I should.

107
00:05:43,640 --> 00:05:47,200
 But I think the reason for not is you just

108
00:05:47,200 --> 00:05:49,280
 get so disgusted by how-- not disgusted,

109
00:05:49,280 --> 00:05:52,200
 but kind of turned off by how many traditions there are

110
00:05:52,200 --> 00:05:55,840
 and how they just seem to just pop up every week.

111
00:05:55,840 --> 00:06:00,360
 So I'm much more comfortable saying I do what I do.

112
00:06:00,360 --> 00:06:02,600
 And I have no problem you doing what you do.

113
00:06:02,600 --> 00:06:03,960
 But it's not what I do.

114
00:06:03,960 --> 00:06:06,520
 So don't try to convert me.

115
00:06:06,520 --> 00:06:08,280
 And we can be friends.

116
00:06:08,280 --> 00:06:17,400
 So is it-- I think it is.

117
00:06:17,400 --> 00:06:22,240
 I think from what I understand, most Buddhist schools,

118
00:06:22,240 --> 00:06:25,880
 the ones that you hear about, are very much in line

119
00:06:25,880 --> 00:06:28,000
 and have the same understanding.

120
00:06:28,000 --> 00:06:31,600
 They just tend to focus on a different aspect.

121
00:06:31,600 --> 00:06:36,680
 So in general, the Mahayana just focuses on becoming a

122
00:06:36,680 --> 00:06:37,160
 Buddha.

123
00:06:37,160 --> 00:06:40,360
 That's really the main difference,

124
00:06:40,360 --> 00:06:44,280
 to go into details and talk about no, but no, but you

125
00:06:44,280 --> 00:06:44,480
 really

126
00:06:44,480 --> 00:06:48,400
 get into different monasteries more than different schools.

127
00:06:48,400 --> 00:06:51,920
 Because in general, Mahayana, they

128
00:06:51,920 --> 00:06:56,200
 don't deny the teachings of the Theravada.

129
00:06:56,200 --> 00:06:59,840
 They just don't focus on it.

130
00:06:59,840 --> 00:07:02,160
 There's very little disagreement.

131
00:07:02,160 --> 00:07:03,560
 If we just talk to each other-- this

132
00:07:03,560 --> 00:07:05,840
 was the thing we got doing this Buddhist news together,

133
00:07:05,840 --> 00:07:08,440
 me and Lama Kunga.

134
00:07:08,440 --> 00:07:10,680
 We really agreed on mostly everything.

135
00:07:10,680 --> 00:07:13,920
 We argued a little bit, but people argued.

136
00:07:13,920 --> 00:07:16,280
 It was constructive dialogue.

137
00:07:16,280 --> 00:07:21,880
 But we're not-- it kind of surprises you.

138
00:07:21,880 --> 00:07:24,240
 I suppose it will be.

139
00:07:24,240 --> 00:07:29,880
 You think that you probably will be incompatible.

140
00:07:29,880 --> 00:07:34,560
 And then you find out that actually you're really saying

141
00:07:34,560 --> 00:07:36,320
 things very much in line with each other.

142
00:07:36,320 --> 00:07:39,680
 [AUDIO OUT]

