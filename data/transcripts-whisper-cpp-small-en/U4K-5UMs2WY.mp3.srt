1
00:00:00,000 --> 00:00:07,000
 Okay, we're live. So today we go on to

2
00:00:07,000 --> 00:00:13,000
 Nijimu Nikaya number 27,

3
00:00:13,000 --> 00:00:17,570
 the Jura Hati Padau Palasuta. I've actually given talks on

4
00:00:17,570 --> 00:00:18,000
 before and studied before.

5
00:00:18,000 --> 00:00:23,000
 So it's going to be familiar to some people.

6
00:00:23,000 --> 00:00:30,000
 It's familiar to me anyway.

7
00:00:30,000 --> 00:00:35,000
 So, again the format is first we will be chanting part of

8
00:00:35,000 --> 00:00:35,000
 the Pali

9
00:00:35,000 --> 00:00:42,030
 and then reading the English and discussing it paragraph by

10
00:00:42,030 --> 00:00:43,000
 paragraph.

11
00:00:43,000 --> 00:00:49,000
 If you're here live you can ask questions via YouTube.

12
00:00:49,000 --> 00:00:54,000
 If you're watching this after the fact as well, you get to

13
00:00:54,000 --> 00:01:03,000
 leave comments.

14
00:01:03,000 --> 00:01:21,000
 I'll just get started here in a minute.

15
00:01:21,000 --> 00:01:35,000
 [pause]

16
00:01:35,000 --> 00:01:49,000
 Get into the text here.

17
00:01:49,000 --> 00:01:56,000
 Okay, we're ready to start.

18
00:01:56,000 --> 00:02:14,000
 [music]

19
00:02:14,000 --> 00:02:43,000
 [singing]

20
00:02:43,000 --> 00:03:11,000
 [singing]

21
00:03:11,000 --> 00:03:21,000
 [singing]

22
00:03:21,000 --> 00:03:31,000
 [singing]

23
00:03:31,000 --> 00:03:41,000
 [singing]

24
00:03:41,000 --> 00:03:51,000
 [singing]

25
00:03:51,000 --> 00:04:01,000
 [singing]

26
00:04:01,000 --> 00:04:11,000
 [singing]

27
00:04:11,000 --> 00:04:21,000
 [singing]

28
00:04:21,000 --> 00:04:31,000
 [singing]

29
00:04:31,000 --> 00:04:41,000
 [singing]

30
00:04:41,000 --> 00:04:51,000
 [singing]

31
00:04:51,000 --> 00:05:01,000
 [singing]

32
00:05:01,000 --> 00:05:11,000
 [singing]

33
00:05:11,000 --> 00:05:21,000
 [singing]

34
00:05:21,000 --> 00:05:31,000
 [singing]

35
00:05:31,000 --> 00:05:41,000
 [singing]

36
00:05:41,000 --> 00:05:51,000
 [singing]

37
00:05:51,000 --> 00:06:01,000
 [singing]

38
00:06:01,000 --> 00:06:11,000
 [singing]

39
00:06:11,000 --> 00:06:21,000
 [singing]

40
00:06:21,000 --> 00:06:31,000
 [singing]

41
00:06:31,000 --> 00:06:41,000
 [singing]

42
00:06:41,000 --> 00:06:51,000
 [singing]

43
00:06:51,000 --> 00:07:01,000
 [singing]

44
00:07:01,000 --> 00:07:11,000
 [singing]

45
00:07:11,000 --> 00:07:21,000
 [singing]

46
00:07:21,000 --> 00:07:31,000
 [singing]

47
00:07:31,000 --> 00:07:41,000
 [singing]

48
00:07:41,000 --> 00:07:51,000
 [singing]

49
00:07:51,000 --> 00:08:01,000
 [singing]

50
00:08:01,000 --> 00:08:11,000
 [singing]

51
00:08:11,000 --> 00:08:21,000
 [singing]

52
00:08:21,000 --> 00:08:31,000
 [singing]

53
00:08:31,000 --> 00:08:41,000
 [singing]

54
00:08:41,000 --> 00:08:51,000
 [singing]

55
00:08:51,000 --> 00:09:01,000
 [singing]

56
00:09:01,000 --> 00:09:11,000
 [singing]

57
00:09:11,000 --> 00:09:21,000
 [singing]

58
00:09:21,000 --> 00:09:31,000
 [singing]

59
00:09:31,000 --> 00:09:41,000
 [singing]

60
00:09:41,000 --> 00:09:51,000
 [singing]

61
00:09:51,000 --> 00:10:01,000
 [singing]

62
00:10:01,000 --> 00:10:11,000
 [singing]

63
00:10:11,000 --> 00:10:21,000
 [singing]

64
00:10:21,000 --> 00:10:31,000
 [singing]

65
00:10:31,000 --> 00:10:41,000
 [singing]

66
00:10:41,000 --> 00:10:51,000
 [singing]

67
00:10:51,000 --> 00:11:01,000
 [singing]

68
00:11:01,000 --> 00:11:11,000
 [singing]

69
00:11:11,000 --> 00:11:21,000
 [singing]

70
00:11:21,000 --> 00:11:31,000
 [singing]

71
00:11:31,000 --> 00:11:41,000
 [singing]

72
00:11:41,000 --> 00:11:51,000
 [singing]

73
00:11:51,000 --> 00:12:01,000
 [singing]

74
00:12:01,000 --> 00:12:11,000
 [singing]

75
00:12:11,000 --> 00:12:21,000
 [singing]

76
00:12:21,000 --> 00:12:31,000
 [singing]

77
00:12:31,000 --> 00:12:41,000
 [singing]

78
00:12:41,000 --> 00:12:51,000
 [singing]

79
00:12:51,000 --> 00:13:01,000
 [singing]

80
00:13:01,000 --> 00:13:11,000
 [singing]

81
00:13:11,000 --> 00:13:21,000
 [singing]

82
00:13:21,000 --> 00:13:31,000
 [singing]

83
00:13:31,000 --> 00:13:41,000
 [singing]

84
00:13:41,000 --> 00:13:51,000
 [singing]

85
00:13:51,000 --> 00:14:01,000
 [singing]

86
00:14:01,000 --> 00:14:11,000
 [singing]

87
00:14:11,000 --> 00:14:21,000
 [singing]

88
00:14:21,000 --> 00:14:31,000
 [singing]

89
00:14:31,000 --> 00:14:41,000
 [singing]

90
00:14:41,000 --> 00:14:51,000
 [singing]

91
00:14:51,000 --> 00:14:59,000
 [singing]

92
00:14:59,000 --> 00:15:07,000
 [singing]

93
00:15:09,000 --> 00:15:17,000
 [singing]

94
00:15:17,000 --> 00:15:27,000
 [singing]

95
00:15:29,000 --> 00:15:37,000
 [singing]

96
00:15:37,000 --> 00:15:47,000
 [singing]

97
00:15:47,000 --> 00:15:55,000
 [singing]

98
00:15:55,000 --> 00:16:05,000
 [singing]

99
00:16:05,000 --> 00:16:15,000
 [singing]

100
00:16:17,000 --> 00:16:25,000
 [singing]

101
00:16:25,000 --> 00:16:25,000
 [singing]

102
00:16:25,000 --> 00:16:25,000
 [singing]

103
00:16:25,000 --> 00:16:25,000
 [singing]

104
00:16:25,000 --> 00:16:25,000
 [singing]

105
00:16:25,000 --> 00:16:25,000
 [singing]

106
00:16:25,000 --> 00:16:25,000
 [singing]

107
00:16:25,000 --> 00:16:25,000
 [singing]

108
00:16:25,000 --> 00:16:25,000
 [singing]

109
00:16:25,000 --> 00:16:25,000
 [singing]

110
00:16:25,000 --> 00:16:25,000
 [singing]

111
00:16:25,000 --> 00:16:25,000
 [singing]

112
00:16:25,000 --> 00:16:25,000
 [singing]

113
00:16:25,000 --> 00:16:25,000
 [singing]

114
00:16:25,000 --> 00:16:25,000
 [singing]

115
00:16:25,000 --> 00:16:25,000
 [singing]

116
00:16:25,000 --> 00:16:25,500
 [singing]

117
00:16:25,500 --> 00:16:26,000
 [singing]

118
00:16:26,000 --> 00:16:26,000
 [singing]

119
00:16:26,000 --> 00:16:26,500
 [singing]

120
00:16:26,500 --> 00:16:27,000
 [singing]

121
00:16:27,000 --> 00:16:27,000
 [singing]

122
00:16:27,000 --> 00:16:27,500
 [singing]

123
00:16:27,500 --> 00:16:27,500
 [singing]

124
00:16:27,500 --> 00:16:28,000
 [singing]

125
00:16:28,000 --> 00:16:28,000
 [singing]

126
00:16:28,000 --> 00:16:28,500
 [singing]

127
00:16:28,500 --> 00:16:29,000
 [singing]

128
00:16:29,000 --> 00:16:29,000
 [singing]

129
00:16:29,000 --> 00:16:29,500
 [singing]

130
00:16:29,500 --> 00:16:30,000
 [singing]

131
00:16:30,000 --> 00:16:30,000
 [singing]

132
00:16:30,000 --> 00:16:30,000
 [singing]

133
00:16:30,000 --> 00:16:30,500
 [singing]

134
00:16:30,500 --> 00:16:31,000
 [singing]

135
00:16:31,000 --> 00:16:31,000
 [singing]

136
00:16:31,000 --> 00:16:41,000
 [singing]

137
00:16:41,000 --> 00:16:41,500
 [singing]

138
00:16:41,500 --> 00:16:42,000
 [singing]

139
00:16:42,000 --> 00:16:42,500
 [singing]

140
00:16:42,500 --> 00:16:43,000
 [singing]

141
00:16:43,000 --> 00:16:43,500
 [singing]

142
00:16:43,500 --> 00:16:44,000
 [singing]

143
00:16:44,000 --> 00:16:44,500
 [singing]

144
00:16:44,500 --> 00:16:45,000
 [singing]

145
00:16:45,000 --> 00:16:45,500
 [singing]

146
00:16:45,500 --> 00:16:46,000
 [singing]

147
00:16:46,000 --> 00:16:46,500
 [singing]

148
00:16:46,500 --> 00:16:47,000
 [singing]

149
00:16:47,000 --> 00:16:47,500
 [singing]

150
00:16:47,500 --> 00:16:48,000
 [singing]

151
00:16:48,000 --> 00:16:48,500
 [singing]

152
00:16:48,500 --> 00:16:49,000
 [singing]

153
00:16:49,000 --> 00:16:49,500
 [singing]

154
00:16:49,500 --> 00:16:50,000
 [singing]

155
00:16:50,000 --> 00:16:50,500
 [singing]

156
00:16:50,500 --> 00:16:51,000
 [singing]

157
00:16:51,000 --> 00:16:51,500
 [singing]

158
00:16:51,500 --> 00:16:52,000
 [singing]

159
00:16:52,000 --> 00:16:52,500
 [singing]

160
00:16:52,500 --> 00:16:53,000
 [singing]

161
00:16:53,000 --> 00:16:53,500
 [singing]

162
00:16:53,500 --> 00:16:54,000
 [singing]

163
00:16:54,000 --> 00:16:54,500
 [singing]

164
00:16:54,500 --> 00:16:55,000
 [singing]

165
00:16:55,000 --> 00:16:55,500
 [singing]

166
00:16:55,500 --> 00:16:56,000
 [singing]

167
00:16:56,000 --> 00:16:56,500
 [singing]

168
00:16:56,500 --> 00:16:57,000
 [singing]

169
00:16:57,000 --> 00:16:57,500
 [singing]

170
00:16:57,500 --> 00:16:58,000
 [singing]

171
00:16:58,000 --> 00:16:58,500
 [singing]

172
00:16:58,500 --> 00:17:03,000
 [singing]

173
00:17:03,000 --> 00:17:05,500
 Hey, we're still broadcasting.

174
00:17:05,500 --> 00:17:08,500
 I wonder what that's like.

175
00:17:08,500 --> 00:17:12,500
 Right, so some technical difficulty.

176
00:17:12,500 --> 00:17:16,900
 Our computer just worked on us, and everything's up where

177
00:17:16,900 --> 00:17:17,500
 it should be.

178
00:17:17,500 --> 00:17:19,850
 So I think we're going to stop the poly there, so we can

179
00:17:19,850 --> 00:17:20,500
 put enough time.

180
00:17:20,500 --> 00:17:22,500
 We'll only put a third down the sutta.

181
00:17:22,500 --> 00:17:25,500
 But I think near the end it gets repetitive.

182
00:17:25,500 --> 00:17:27,000
 It repeats from other sutta.

183
00:17:27,000 --> 00:17:30,500
 So hopefully we've actually done about a half of that.

184
00:17:30,500 --> 00:17:33,500
 But with the poly, we haven't done a half.

185
00:17:33,500 --> 00:17:36,500
 Let's see.

186
00:17:36,500 --> 00:17:46,500
 Just want to...

187
00:17:46,500 --> 00:17:59,000
 [silence]

188
00:17:59,000 --> 00:18:02,000
 Let's see if we can find a better place to start.

189
00:18:02,000 --> 00:18:08,000
 No, I think we'll do it in three parts then.

190
00:18:08,000 --> 00:18:10,000
 Okay, so today we'll do the first part.

191
00:18:10,000 --> 00:18:12,000
 We've gotten through the...

192
00:18:12,000 --> 00:18:16,000
 Sorry, I'll just pull up the text here again.

193
00:18:16,000 --> 00:18:19,000
 [silence]

194
00:18:19,000 --> 00:18:25,000
 Okay, so...

195
00:18:25,000 --> 00:18:28,000
 We got through the...

196
00:18:28,000 --> 00:18:30,630
 Preface of the text, which is actually a whole section in

197
00:18:30,630 --> 00:18:31,000
 itself.

198
00:18:31,000 --> 00:18:41,000
 So we'll stop there, and we'll...

199
00:18:41,000 --> 00:18:46,000
 Right. We'll start with the English.

200
00:18:46,000 --> 00:18:49,990
 Thus have I heard, on one occasion the Blessed One was

201
00:18:49,990 --> 00:18:51,000
 living at Sawati in Jaitas Grove,

202
00:18:51,000 --> 00:18:54,000
 Anantapindika's park.

203
00:18:54,000 --> 00:18:56,000
 Now...

204
00:18:56,000 --> 00:18:58,000
 A little tidbit of information.

205
00:18:58,000 --> 00:18:59,990
 This was the first sutta preached by Mahinda Thira

206
00:18:59,990 --> 00:19:03,000
 following his arrival in Sri Lanka.

207
00:19:03,000 --> 00:19:05,000
 That's interesting. I wonder if that's why...

208
00:19:05,000 --> 00:19:09,000
 It's one of the only sutta's that I taught in Sri Lanka.

209
00:19:09,000 --> 00:19:13,770
 There's a video on knowing the Buddha, I think, at Polanar

210
00:19:13,770 --> 00:19:14,000
ua.

211
00:19:14,000 --> 00:19:17,000
 Or Buddha on YouTube, I was just looking at it.

212
00:19:17,000 --> 00:19:19,000
 Trying to think where I'd remember giving a talk,

213
00:19:19,000 --> 00:19:23,750
 and it was actually while we were visiting Polanarua in Sri

214
00:19:23,750 --> 00:19:25,000
 Lanka.

215
00:19:25,000 --> 00:19:29,000
 Maybe that's why I was giving it.

216
00:19:29,000 --> 00:19:33,000
 Now, on that occasion the Brahmin, John Usoini,

217
00:19:33,000 --> 00:19:36,000
 was living out at Sawati in the middle of the day,

218
00:19:36,000 --> 00:19:40,000
 in a white chariot, drawn by white mares.

219
00:19:40,000 --> 00:19:43,310
 He saw the wanderer Pilotika coming in the distance and

220
00:19:43,310 --> 00:19:44,000
 asked him,

221
00:19:44,000 --> 00:19:48,400
 "Now, where is Master Vachayana coming from in the middle

222
00:19:48,400 --> 00:19:49,000
 of the day?"

223
00:19:49,000 --> 00:19:53,990
 His name was Pilotika, but they called him by his clan name

224
00:19:53,990 --> 00:19:55,000
, Vachayana,

225
00:19:55,000 --> 00:19:59,000
 which is his family name.

226
00:19:59,000 --> 00:20:02,000
 You can just read through this.

227
00:20:02,000 --> 00:20:06,000
 This is a prologue to the actual sutta.

228
00:20:06,000 --> 00:20:09,000
 What would have caused it to come about?

229
00:20:09,000 --> 00:20:12,000
 Why did the Buddha teach?

230
00:20:12,000 --> 00:20:19,000
 Sir, I am coming from the presence of the recluse Gotama.

231
00:20:19,000 --> 00:20:23,040
 What does Master Vachayana think of the recluse Gotama's

232
00:20:23,040 --> 00:20:25,000
 lucidity of wisdom?

233
00:20:25,000 --> 00:20:27,000
 He is wise, is he not?

234
00:20:27,000 --> 00:20:30,250
 Sir, who am I to know the recluse Gotama's lucidity of

235
00:20:30,250 --> 00:20:31,000
 wisdom?

236
00:20:31,000 --> 00:20:34,540
 One would surely have to be as equal to know the recluse

237
00:20:34,540 --> 00:20:36,000
 Gotama's lucidity of wisdom.

238
00:20:36,000 --> 00:20:39,700
 Master Vachayana praises the recluse Gotama with high

239
00:20:39,700 --> 00:20:41,000
 praise indeed.

240
00:20:41,000 --> 00:20:45,000
 Sir, who am I to praise the recluse Gotama?

241
00:20:45,000 --> 00:20:48,390
 The recluse Gotama is praised by the praised as best among

242
00:20:48,390 --> 00:20:49,000
 gods and humans.

243
00:20:49,000 --> 00:20:56,000
 That's some high praise, no? He's really setting him up.

244
00:20:56,000 --> 00:21:00,170
 What reasons is Master Vachayana see that he has such firm

245
00:21:00,170 --> 00:21:03,000
 confidence in the recluse Gotama?

246
00:21:03,000 --> 00:21:10,000
 Here we get the simile of the elephant foot print.

247
00:21:10,000 --> 00:21:18,000
 The elephant foot print.

248
00:21:18,000 --> 00:21:21,000
 But this is the original part.

249
00:21:21,000 --> 00:21:27,000
 So the Buddha is going to give his own interpretation.

250
00:21:27,000 --> 00:21:32,000
 This guy talks about four things.

251
00:21:32,000 --> 00:21:35,100
 Sir, suppose a wise elephant woodsman were to enter an

252
00:21:35,100 --> 00:21:36,000
 elephant wood

253
00:21:36,000 --> 00:21:39,550
 and were to see in the elephant wood a big elephant's foot

254
00:21:39,550 --> 00:21:40,000
 print,

255
00:21:40,000 --> 00:21:43,000
 long in extent and brought across.

256
00:21:43,000 --> 00:21:46,680
 He would come to the conclusion, "Indeed, this is a big

257
00:21:46,680 --> 00:21:48,000
 bull elephant."

258
00:21:48,000 --> 00:21:51,000
 So too, when I saw four footprints of the reckless Gotama,

259
00:21:51,000 --> 00:21:54,210
 I came to the conclusion, "The Blessed One is fully

260
00:21:54,210 --> 00:21:55,000
 enlightened.

261
00:21:55,000 --> 00:21:58,000
 The Dhamma is well proclaimed in the Blessed One.

262
00:21:58,000 --> 00:22:00,000
 Sanka is practicing the good way."

263
00:22:00,000 --> 00:22:02,000
 What are the four?

264
00:22:02,000 --> 00:22:10,690
 Okay, so his reckoning is that when a wise elephant woods

265
00:22:10,690 --> 00:22:13,000
man sees a big elephant's foot print,

266
00:22:13,000 --> 00:22:16,000
 they'll know right away that this is a big bull elephant.

267
00:22:16,000 --> 00:22:19,000
 You can tell an elephant by its foot print.

268
00:22:19,000 --> 00:22:22,590
 Now, spoiler, the Buddha is going to say that you can't

269
00:22:22,590 --> 00:22:23,000
 actually.

270
00:22:23,000 --> 00:22:25,000
 He's going to actually extend this, which is interesting.

271
00:22:25,000 --> 00:22:29,000
 But it's still an interesting simile, the point that

272
00:22:29,000 --> 00:22:31,000
 although it is limited,

273
00:22:31,000 --> 00:22:35,320
 you see, the problem with this is, of course, that people

274
00:22:35,320 --> 00:22:37,000
 can be...

275
00:22:37,000 --> 00:22:41,000
 He's going to talk about praise, but you can be praised

276
00:22:41,000 --> 00:22:44,000
 without warrant, right?

277
00:22:44,000 --> 00:22:47,690
 It's possible that people who don't warrant or don't

278
00:22:47,690 --> 00:22:51,000
 deserve praise can be praised.

279
00:22:51,000 --> 00:22:54,560
 So he's got this idea that if you hear about how great

280
00:22:54,560 --> 00:22:56,000
 people are from others,

281
00:22:56,000 --> 00:22:58,000
 it's a sign that surely they are great.

282
00:22:58,000 --> 00:23:01,000
 Now, in this case, it's true, but there are, of course,

283
00:23:01,000 --> 00:23:02,000
 cases where that's not true,

284
00:23:02,000 --> 00:23:06,470
 where people are highly praised, but underneath, they're

285
00:23:06,470 --> 00:23:12,000
 actually rogues and devils.

286
00:23:12,000 --> 00:23:16,080
 But anyway, he's got four ways of... four elephant

287
00:23:16,080 --> 00:23:18,000
 footprints that he says show him

288
00:23:18,000 --> 00:23:20,000
 that the Buddha is a true bull elephant.

289
00:23:20,000 --> 00:23:22,000
 So, the first one.

290
00:23:22,000 --> 00:23:25,430
 Sir, I have seen here certain learned nobles who were

291
00:23:25,430 --> 00:23:27,000
 clever, knowledgeable

292
00:23:27,000 --> 00:23:30,440
 about the doctrines of others, as sharp as hair-splitting

293
00:23:30,440 --> 00:23:31,000
 marksmen,

294
00:23:31,000 --> 00:23:33,760
 they wander about, as it were, demolishing the views of

295
00:23:33,760 --> 00:23:36,000
 others with their sharp wits.

296
00:23:36,000 --> 00:23:39,160
 When they hear the recluse go to mount will visit such and

297
00:23:39,160 --> 00:23:40,000
 such a village or town,

298
00:23:40,000 --> 00:23:43,330
 they formulate a question thus, we will go to the recluse

299
00:23:43,330 --> 00:23:45,000
 go to mount and ask him a question.

300
00:23:45,000 --> 00:23:48,350
 If he is asked like this, he will answer like this, and so

301
00:23:48,350 --> 00:23:51,000
 we will refute his doctrine in this way,

302
00:23:51,000 --> 00:23:54,200
 and if he is asked like that, he will answer like that, and

303
00:23:54,200 --> 00:23:57,000
 so we will refute his doctrine in that way.

304
00:23:57,000 --> 00:24:01,670
 So, to people who... anyone who... the most clever of the

305
00:24:01,670 --> 00:24:04,000
 clever, whether they be...

306
00:24:04,000 --> 00:24:09,910
 and just to skip ahead, the four are actually going to be

307
00:24:09,910 --> 00:24:12,000
 nobles, brahmins,

308
00:24:12,000 --> 00:24:16,000
 what do we have, nobles, brahmins,

309
00:24:16,000 --> 00:24:19,990
 householders, which are like merchants, and then reck

310
00:24:19,990 --> 00:24:21,000
lessness.

311
00:24:21,000 --> 00:24:26,000
 So, the wisest of the wise out of all these four groups,

312
00:24:26,000 --> 00:24:30,620
 sharp as hair-splitting marksmen, the best of the best,

313
00:24:30,620 --> 00:24:32,000
 they come to go to the Buddha,

314
00:24:32,000 --> 00:24:35,380
 thinking that they are going to outsmart him, and that

315
00:24:35,380 --> 00:24:40,000
 probably he will be shamed into becoming their reckless.

316
00:24:40,000 --> 00:24:45,460
 He will be awed by their wit and their wisdom, and will

317
00:24:45,460 --> 00:24:47,000
 become their disciples.

318
00:24:47,000 --> 00:24:49,000
 And then...

319
00:24:49,000 --> 00:24:52,270
 They hear the recluse go to mount has come to visit such

320
00:24:52,270 --> 00:24:54,000
 and such a village or town,

321
00:24:54,000 --> 00:24:57,760
 they go to the recluse go to mount, and the recluse go to

322
00:24:57,760 --> 00:24:59,000
 mount instructs, urges,

323
00:24:59,000 --> 00:25:02,000
 rouses, and gladdens them with a talk in the Dhamma.

324
00:25:02,000 --> 00:25:05,650
 After they have been instructed, urged, roused, and gladd

325
00:25:05,650 --> 00:25:07,000
ened by the recluse go to mount

326
00:25:07,000 --> 00:25:10,470
 with a talk in the Dhamma, they do not so much ask him the

327
00:25:10,470 --> 00:25:11,000
 question,

328
00:25:11,000 --> 00:25:14,000
 so how should they refute his doctrine?

329
00:25:14,000 --> 00:25:20,000
 In actual fact, they become his subjects.

330
00:25:20,000 --> 00:25:22,000
 Oops.

331
00:25:22,000 --> 00:25:24,000
 Sorry about that.

332
00:25:24,000 --> 00:25:26,000
 My fault.

333
00:25:28,000 --> 00:25:33,000
 In actual fact, they become his disciples.

334
00:25:33,000 --> 00:25:36,000
 When I saw this first footprint of the reckless go to mount

335
00:25:36,000 --> 00:25:36,000
,

336
00:25:36,000 --> 00:25:39,230
 I came to the conclusion, the blessed one is fully

337
00:25:39,230 --> 00:25:40,000
 enlightened,

338
00:25:40,000 --> 00:25:43,000
 the Dhamma is well proclaimed by the blessed one,

339
00:25:43,000 --> 00:25:45,000
 the Sanka is practicing the good way.

340
00:25:45,000 --> 00:25:48,000
 So he kind of jumps to conclusions here.

341
00:25:48,000 --> 00:25:51,440
 I mean, it's a good sign, but the Buddha is going to say

342
00:25:51,440 --> 00:25:53,000
 that it's not the only sign.

343
00:25:53,000 --> 00:25:57,620
 Anyway, it's impressive that this occurs, that no matter

344
00:25:57,620 --> 00:25:58,000
 who comes to see the Buddha,

345
00:25:58,000 --> 00:26:03,250
 it's sure to impress just about anyone the fact that no

346
00:26:03,250 --> 00:26:06,000
 matter who goes to try to debate

347
00:26:06,000 --> 00:26:09,000
 with the Buddha, they always end up becoming his disciples.

348
00:26:09,000 --> 00:26:13,000
 So all of the nobles, this would be the kings and royalty,

349
00:26:13,000 --> 00:26:17,220
 all of the high class aristocracy, they all end up becoming

350
00:26:17,220 --> 00:26:18,000
 his disciples,

351
00:26:18,000 --> 00:26:21,390
 even though they think that he is going to be shamed by

352
00:26:21,390 --> 00:26:22,000
 them.

353
00:26:22,000 --> 00:26:26,000
 So this is the first.

354
00:26:26,000 --> 00:26:29,000
 The second is the Brahmins do the same.

355
00:26:29,000 --> 00:26:34,000
 The third is that householders do the same.

356
00:26:34,000 --> 00:26:40,880
 And the fourth is that the fourth is a little bit different

357
00:26:40,880 --> 00:26:41,000
.

358
00:26:41,000 --> 00:26:44,000
 The fourth is that preclusors do the same,

359
00:26:44,000 --> 00:26:49,000
 but not only do they become his students,

360
00:26:49,000 --> 00:26:59,130
 but in actual fact, they ask the reckless go to God to

361
00:26:59,130 --> 00:27:00,000
 allow them to go forth

362
00:27:00,000 --> 00:27:03,270
 from the home life into homelessness, and he gives them the

363
00:27:03,270 --> 00:27:04,000
 going forth.

364
00:27:04,000 --> 00:27:07,840
 Not long after they have gone forth, dwelling alone,

365
00:27:07,840 --> 00:27:10,000
 withdrawn, diligent, ardent, and resolute,

366
00:27:10,000 --> 00:27:13,370
 by realizing for themselves with direct knowledge, they

367
00:27:13,370 --> 00:27:15,000
 hear and now enter upon

368
00:27:15,000 --> 00:27:19,250
 and abide in that supreme goal of the holy life by the sake

369
00:27:19,250 --> 00:27:20,000
 of which clansmen rightly go forth

370
00:27:20,000 --> 00:27:23,000
 from the home life into homelessness.

371
00:27:23,000 --> 00:27:27,030
 They say thus, "We were very nearly lost, we very nearly

372
00:27:27,030 --> 00:27:28,000
 perished.

373
00:27:28,000 --> 00:27:31,600
 For formerly we claimed that we were recluses, though we

374
00:27:31,600 --> 00:27:33,000
 were not really recluses.

375
00:27:33,000 --> 00:27:36,230
 We claimed that we were Brahmins, though we were not really

376
00:27:36,230 --> 00:27:37,000
 Brahmins.

377
00:27:37,000 --> 00:27:40,220
 We claimed that we were Arahants, though we were not really

378
00:27:40,220 --> 00:27:41,000
 Arahants.

379
00:27:41,000 --> 00:27:44,170
 But now we are recluses, now we are Brahmins, now we are A

380
00:27:44,170 --> 00:27:45,000
rahants.

381
00:27:45,000 --> 00:27:49,350
 When I saw this fourth footprint of the reckless go to Ma,

382
00:27:49,350 --> 00:27:50,000
 I came to the conclusion,

383
00:27:50,000 --> 00:27:53,000
 the blessed one is fully enlightened."

384
00:27:53,000 --> 00:28:07,000
 Okay, so he's actually seen...

385
00:28:07,000 --> 00:28:10,280
 So what he's seen of lay people is that they all become

386
00:28:10,280 --> 00:28:14,000
 converted and established great faith in the Buddha.

387
00:28:14,000 --> 00:28:18,000
 But what he's also seen is even somewhat more impressive,

388
00:28:18,000 --> 00:28:21,870
 that those people who actually are looking for spiritual

389
00:28:21,870 --> 00:28:23,000
 enlightenment,

390
00:28:23,000 --> 00:28:27,190
 who are practicing the spiritual path, when they come to

391
00:28:27,190 --> 00:28:28,000
 the Buddha,

392
00:28:28,000 --> 00:28:31,250
 not only do they become converted, but they actually attain

393
00:28:31,250 --> 00:28:32,000
 their goal.

394
00:28:32,000 --> 00:28:35,510
 They actually do become enlightened by practicing the

395
00:28:35,510 --> 00:28:36,000
 Buddha's teaching,

396
00:28:36,000 --> 00:28:44,560
 and they claim for themselves to be true Arahants, true re

397
00:28:44,560 --> 00:28:49,000
cluses, true Brahmins, true Arahants.

398
00:28:49,000 --> 00:28:57,150
 So the most impressive footprint of all he points out to

399
00:28:57,150 --> 00:28:59,000
 Januson.

400
00:28:59,000 --> 00:29:02,020
 "When I saw these four footprints of the reckless go to Ma,

401
00:29:02,020 --> 00:29:04,000
 I came to the conclusion,

402
00:29:04,000 --> 00:29:07,060
 the blessed one is fully enlightened, that Dhamma is well

403
00:29:07,060 --> 00:29:09,000
 proclaimed by the blessed one.

404
00:29:09,000 --> 00:29:11,000
 Sankar is practicing the good way."

405
00:29:11,000 --> 00:29:13,000
 Which is reasonable actually.

406
00:29:13,000 --> 00:29:17,670
 Certainly reasonable to assume that this is not just any

407
00:29:17,670 --> 00:29:20,000
 old run of the mill teaching,

408
00:29:20,000 --> 00:29:23,000
 or run of the mill teacher.

409
00:29:23,000 --> 00:29:27,560
 And so Brahman Janusoni does what any of us would think to

410
00:29:27,560 --> 00:29:28,000
 do,

411
00:29:28,000 --> 00:29:31,000
 maybe not exactly, but he gets quite excited.

412
00:29:31,000 --> 00:29:35,310
 "When this was said, the Brahman Janusoni got down from his

413
00:29:35,310 --> 00:29:36,000
 all white chariot,

414
00:29:36,000 --> 00:29:39,370
 drawn by white mares, and arranging his upper robe on one

415
00:29:39,370 --> 00:29:40,000
 shoulder,

416
00:29:40,000 --> 00:29:44,110
 he extended his hands in reverential salutation toward the

417
00:29:44,110 --> 00:29:45,000
 blessed one,

418
00:29:45,000 --> 00:29:49,000
 and uttered this exclamation three times.

419
00:29:49,000 --> 00:29:57,700
 Honor to the blessed one, accomplished and fully

420
00:29:57,700 --> 00:30:00,000
 enlightened.

421
00:30:00,000 --> 00:30:04,700
 Perhaps sometime or other, we might meet Master Gautama and

422
00:30:04,700 --> 00:30:12,000
 have some conversation with him."

423
00:30:12,000 --> 00:30:16,000
 This is actually, if you heard us in the chanting, this is

424
00:30:16,000 --> 00:30:18,000
 Namautasa Bhagavatam Vadamatam Vadamatasa.

425
00:30:18,000 --> 00:30:23,620
 So you can see that this Namautasa actually comes from the

426
00:30:23,620 --> 00:30:25,000
 Tipitaka.

427
00:30:25,000 --> 00:30:30,000
 We have it here and we have it elsewhere as well.

428
00:30:30,000 --> 00:30:33,000
 "Common means we respect, arranging a robe on one shoulder,

429
00:30:33,000 --> 00:30:37,810
 and holding your hands up to your chest in reverential sal

430
00:30:37,810 --> 00:30:39,000
utation."

431
00:30:39,000 --> 00:30:42,150
 Doesn't mean holding like giving a military salute or

432
00:30:42,150 --> 00:30:43,000
 something.

433
00:30:43,000 --> 00:30:48,560
 It's pulling your hands up to your chest and anjali, like a

434
00:30:48,560 --> 00:30:51,000
 prayer kind of thing.

435
00:30:51,000 --> 00:30:54,480
 And then he says, "Well, maybe I can meet the Master Gaut

436
00:30:54,480 --> 00:30:55,000
ama," but then he does go,

437
00:30:55,000 --> 00:30:58,150
 whether right away or maybe later, maybe another day or

438
00:30:58,150 --> 00:30:59,000
 something.

439
00:30:59,000 --> 00:31:03,190
 "Then the Brahmin, John Nusoni, went to the blessed one and

440
00:31:03,190 --> 00:31:05,000
 exchanged greetings with him.

441
00:31:05,000 --> 00:31:08,340
 When this courteous and amiable talk was finished, he sat

442
00:31:08,340 --> 00:31:09,000
 down at one side

443
00:31:09,000 --> 00:31:12,400
 and related to the blessed one his entire conversation with

444
00:31:12,400 --> 00:31:14,000
 the wanderer Pidotika.

445
00:31:14,000 --> 00:31:17,770
 Thereupon the blessed one told him, 'At this point, Brahmin

446
00:31:17,770 --> 00:31:18,000
,

447
00:31:18,000 --> 00:31:21,360
 the simile of the elephant's footprint has not yet been

448
00:31:21,360 --> 00:31:23,000
 completed in detail.'

449
00:31:23,000 --> 00:31:27,250
 As to how it is completed in detail, listen and attend

450
00:31:27,250 --> 00:31:29,000
 carefully to what I shall say.

451
00:31:29,000 --> 00:31:32,000
 'Yes, sir,' the Brahmin, John Nusoni replied.

452
00:31:32,000 --> 00:31:34,000
 The blessed one said this."

453
00:31:34,000 --> 00:31:37,000
 That's as far as you've gotten, actually.

454
00:31:37,000 --> 00:31:40,000
 That is about a third and we would have gone further, I

455
00:31:40,000 --> 00:31:40,000
 think,

456
00:31:40,000 --> 00:31:43,820
 but then we got cut out so we stopped there with the Palais

457
00:31:43,820 --> 00:31:44,000
.

458
00:31:44,000 --> 00:31:46,000
 Let's stop there with the English.

459
00:31:46,000 --> 00:31:48,000
 Here we have the preface to the sutta.

460
00:31:48,000 --> 00:31:51,000
 Tomorrow we'll actually get into what the Buddha says.

461
00:31:51,000 --> 00:31:55,330
 Of course, you can read ahead if you want if you have the s

462
00:31:55,330 --> 00:31:57,000
utta in front of you.

463
00:31:57,000 --> 00:32:00,000
 It looks like we'll have three sessions.

464
00:32:00,000 --> 00:32:07,990
 Tomorrow we'll discuss the Buddha's idea of how you tell

465
00:32:07,990 --> 00:32:13,000
 whether someone is a bull elephant.

466
00:32:13,000 --> 00:32:16,000
 Let's cut back to here.

467
00:32:16,000 --> 00:32:18,000
 Let me see ourselves again.

468
00:32:18,000 --> 00:32:21,000
 That's all for tonight. Thank you all for stopping in.

469
00:32:21,000 --> 00:32:24,000
 We don't have quite the question thing up this time.

470
00:32:24,000 --> 00:32:26,430
 I don't know whether that actually worked, but hopefully

471
00:32:26,430 --> 00:32:27,000
 this video has gone up

472
00:32:27,000 --> 00:32:30,000
 and we've done the first third of the Julia Hartt

473
00:32:30,000 --> 00:32:32,000
 deep-butt opening this time.

474
00:32:32,000 --> 00:32:34,000
 Thank you all for tuning in. Have a good night.

