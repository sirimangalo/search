1
00:00:00,000 --> 00:00:04,390
 Hello everyone and welcome back to our study of the Dhammap

2
00:00:04,390 --> 00:00:05,120
ada.

3
00:00:05,120 --> 00:00:09,060
 Today we continue on with verse number 108 which reads as

4
00:00:09,060 --> 00:00:10,640
 follows.

5
00:00:10,640 --> 00:00:11,960
 Yankinciitang vahutang valoke sangvacharan yajita punyapeko

6
00:00:11,960 --> 00:00:30,080
 sabampitang nachatubhagameti abhiwadana ujukate suseyo

7
00:00:30,080 --> 00:00:34,010
 Which means it's actually a bit difficult because the

8
00:00:34,010 --> 00:00:37,280
 grammar doesn't really work in English.

9
00:00:37,280 --> 00:00:48,930
 So whatever sacrifice or worship there is in the world, if

10
00:00:48,930 --> 00:00:53,640
 for 100 years sangvacharan yajita punyapeko one desiring

11
00:00:53,640 --> 00:00:55,680
 punya, desiring merit,

12
00:00:55,680 --> 00:01:01,040
 should perform such a sacrifice or such a worship.

13
00:01:01,040 --> 00:01:06,660
 And yes, it's actually a very very similar verse and story

14
00:01:06,660 --> 00:01:08,480
 to the last two.

15
00:01:08,480 --> 00:01:14,160
 Sabampitang tatubhagameti, it doesn't come to a quarter of

16
00:01:14,160 --> 00:01:15,520
 the value.

17
00:01:15,520 --> 00:01:19,040
 All of that doesn't come to a quarter of the value.

18
00:01:19,040 --> 00:01:24,480
 The hundred years of sacrifice or worship that one pays.

19
00:01:24,480 --> 00:01:33,730
 Abhiwadana ujukate suseyo For the greater, in comparison to

20
00:01:33,730 --> 00:01:34,560
 the greater,

21
00:01:34,560 --> 00:01:40,480
 abhiwadana or respect or reverence that one pays to.

22
00:01:40,480 --> 00:01:45,690
 And today our word is ujugata in regards to those who are u

23
00:01:45,690 --> 00:01:49,840
jugata, gone to or become straight.

24
00:01:49,840 --> 00:01:57,170
 Those who have gained rectitude, moral or ethical or mental

25
00:01:57,170 --> 00:02:00,000
 straightness, uprightness.

26
00:02:00,000 --> 00:02:04,480
 Those who are upright.

27
00:02:04,480 --> 00:02:11,040
 So same story, we have Saripurta and it's becoming apparent

28
00:02:11,040 --> 00:02:14,640
 that he was really instrumental

29
00:02:14,640 --> 00:02:19,690
 in helping a lot of his relatives and now his friends

30
00:02:19,690 --> 00:02:22,080
 become enlightened.

31
00:02:22,080 --> 00:02:28,400
 So now we have Saripurta's friend, Sahayaka Brahmana.

32
00:02:28,400 --> 00:02:32,080
 Brahman who was a friend of Saripurta.

33
00:02:32,080 --> 00:02:34,400
 Friend of Saripurta's from before.

34
00:02:34,400 --> 00:02:38,350
 And so Saripurta went to him and asked him, "Do you do any

35
00:02:38,350 --> 00:02:39,360
 good deeds?"

36
00:02:39,360 --> 00:02:42,320
 And he says, "Oh yes, what do you do?"

37
00:02:42,320 --> 00:02:46,160
 "Oh, I offer sacrificial slaughter."

38
00:02:46,160 --> 00:02:49,910
 And of course Saripurta doesn't see that as being a wholes

39
00:02:49,910 --> 00:02:50,800
ome deed.

40
00:02:50,800 --> 00:02:54,320
 This is about really the crux of these three stories.

41
00:02:54,320 --> 00:03:03,510
 The idea that good deeds, our concept of good deeds is

42
00:03:03,510 --> 00:03:07,440
 often quite misled.

43
00:03:07,440 --> 00:03:10,000
 This even occurs in Buddhism.

44
00:03:10,000 --> 00:03:13,310
 You'll see Buddhists performing deeds that they think are

45
00:03:13,310 --> 00:03:14,640
 good deeds when in fact they

46
00:03:14,640 --> 00:03:21,240
 are perhaps useless.

47
00:03:21,240 --> 00:03:28,120
 So the case of, I've told this story before, but once we

48
00:03:28,120 --> 00:03:31,720
 had a katina ceremony where I

49
00:03:31,720 --> 00:03:36,890
 was staying and all the villagers came together to do the k

50
00:03:36,890 --> 00:03:38,720
atina ceremony.

51
00:03:38,720 --> 00:03:42,800
 So what they did is they had this very important, I was in

52
00:03:42,800 --> 00:03:45,160
 a rural area, so it was an area where

53
00:03:45,160 --> 00:03:48,120
 people didn't have a lot of money.

54
00:03:48,120 --> 00:03:51,590
 And there was one man who had built a house in the area

55
00:03:51,590 --> 00:03:53,640
 from Bangkok and he was quite

56
00:03:53,640 --> 00:03:58,680
 rich and so he got his rich friends from Bangkok to come up

57
00:03:58,680 --> 00:03:59,080
.

58
00:03:59,080 --> 00:04:00,980
 And they put together a bunch of money and they put the

59
00:04:00,980 --> 00:04:02,280
 money up on a tree and they brought

60
00:04:02,280 --> 00:04:05,830
 this money tree to the monastery and they wanted to do a k

61
00:04:05,830 --> 00:04:07,200
atina ceremony.

62
00:04:07,200 --> 00:04:10,120
 Well, it turns out half of them were drunk.

63
00:04:10,120 --> 00:04:16,620
 They had been drinking and the funniest part was, and what

64
00:04:16,620 --> 00:04:20,360
 sort of made it a bit ridiculous,

65
00:04:20,360 --> 00:04:24,280
 was that they didn't have a single robe with which to do

66
00:04:24,280 --> 00:04:25,400
 the katina.

67
00:04:25,400 --> 00:04:27,250
 So they brought this money tree thinking, "Well, that's kat

68
00:04:27,250 --> 00:04:27,560
ina.

69
00:04:27,560 --> 00:04:29,840
 Katina is a money tree because that's really what it's

70
00:04:29,840 --> 00:04:30,280
 become.

71
00:04:30,280 --> 00:04:35,000
 It's all about money trees, trees that are literally made

72
00:04:35,000 --> 00:04:36,040
 of money.

73
00:04:36,040 --> 00:04:40,010
 The leaves are folded up bits of money or just money

74
00:04:40,010 --> 00:04:42,160
 sticking up on sticks."

75
00:04:42,160 --> 00:04:48,480
 And so I said, "Okay, we're ready to do katina.

76
00:04:48,480 --> 00:04:49,480
 What do we do?"

77
00:04:49,480 --> 00:04:53,040
 And I said, "Well, where's the robe?"

78
00:04:53,040 --> 00:04:54,040
 And they didn't have a robe.

79
00:04:54,040 --> 00:04:57,280
 So I said, "Oh, well, why can we borrow one of yours?"

80
00:04:57,280 --> 00:04:58,280
 And I said, "Okay."

81
00:04:58,280 --> 00:04:59,280
 I took off.

82
00:04:59,280 --> 00:05:00,280
 I actually took off.

83
00:05:00,280 --> 00:05:02,480
 Anyway, I don't want to talk about it.

84
00:05:02,480 --> 00:05:04,480
 I'm sure it wasn't.

85
00:05:04,480 --> 00:05:09,940
 Point being, not really what I would call a wholesome

86
00:05:09,940 --> 00:05:14,080
 engagement, especially considering

87
00:05:14,080 --> 00:05:15,920
 the alcohol involved.

88
00:05:15,920 --> 00:05:19,310
 And the fact that after it was all over, they took the

89
00:05:19,310 --> 00:05:21,520
 money back and on some of the half

90
00:05:21,520 --> 00:05:23,630
 of the gifts that were supposed to go to the monastery,

91
00:05:23,630 --> 00:05:25,480
 they took back for the village.

92
00:05:25,480 --> 00:05:32,710
 It was really, it was a bit of a system that they had set

93
00:05:32,710 --> 00:05:33,880
 up.

94
00:05:33,880 --> 00:05:37,280
 This happens in Buddhist circles sometimes where we lose

95
00:05:37,280 --> 00:05:39,160
 sight of why are we doing this.

96
00:05:39,160 --> 00:05:41,900
 I think while we're doing it, often they're doing it just

97
00:05:41,900 --> 00:05:43,680
 because they're excited to have

98
00:05:43,680 --> 00:05:51,180
 their village and their community grow.

99
00:05:51,180 --> 00:05:55,990
 So the money goes to the community, I think, or the stuff

100
00:05:55,990 --> 00:05:58,320
 goes for the community.

101
00:05:58,320 --> 00:06:04,500
 And they don't realize the great benefit to actually giving

102
00:06:04,500 --> 00:06:05,560
 a gift.

103
00:06:05,560 --> 00:06:08,680
 And in fact, they aren't really interested in giving gifts.

104
00:06:08,680 --> 00:06:14,240
 On the other hand, there was that very same village ended

105
00:06:14,240 --> 00:06:16,960
 up being quite awesome about

106
00:06:16,960 --> 00:06:17,960
 giving alms.

107
00:06:17,960 --> 00:06:21,600
 There's another funny aspect of being there.

108
00:06:21,600 --> 00:06:25,040
 I was staying with an old monk, two old monks.

109
00:06:25,040 --> 00:06:27,230
 So this was later when I was staying with a second old monk

110
00:06:27,230 --> 00:06:27,440
.

111
00:06:27,440 --> 00:06:31,690
 But I was staying with a first old monk deeper in the

112
00:06:31,690 --> 00:06:32,040
 forest.

113
00:06:32,040 --> 00:06:38,830
 And he told me when he go on, he said, "Well, you can go on

114
00:06:38,830 --> 00:06:40,360
 alms round to the village, but

115
00:06:40,360 --> 00:06:41,360
 you won't get anything.

116
00:06:41,360 --> 00:06:46,510
 You might get some dried noodles or canned fish, but that's

117
00:06:46,510 --> 00:06:47,640
 about it.

118
00:06:47,640 --> 00:06:49,960
 It won't be enough to eat, but you can go."

119
00:06:49,960 --> 00:06:53,110
 And it was three and a half kilometers walk along a dirt

120
00:06:53,110 --> 00:06:54,880
 gravel road that was actually

121
00:06:54,880 --> 00:06:58,120
 somewhat painful to walk.

122
00:06:58,120 --> 00:07:00,980
 But I did walk three and a half kilometers and often had to

123
00:07:00,980 --> 00:07:02,720
 walk three and a half kilometers

124
00:07:02,720 --> 00:07:04,440
 back, but not always.

125
00:07:04,440 --> 00:07:07,640
 Sometimes someone going to the park, there was a national

126
00:07:07,640 --> 00:07:09,360
 park near where the monastery

127
00:07:09,360 --> 00:07:17,040
 was, would give us a ride back or give me or eventually us

128
00:07:17,040 --> 00:07:20,400
 when I had followers.

129
00:07:20,400 --> 00:07:24,270
 But the great thing, once I went and they could see that I

130
00:07:24,270 --> 00:07:26,400
 was interested in meditation

131
00:07:26,400 --> 00:07:37,040
 and wasn't some crabby old monk, they were really keen on.

132
00:07:37,040 --> 00:07:39,440
 And eventually the whole village began to give them.

133
00:07:39,440 --> 00:07:41,920
 And they really got this sense of the greatness.

134
00:07:41,920 --> 00:07:44,400
 People who I think had never really gotten into it.

135
00:07:44,400 --> 00:07:46,590
 And there was one man who was a really devout Buddhist and

136
00:07:46,590 --> 00:07:47,840
 very interested in the Dhamma

137
00:07:47,840 --> 00:07:50,980
 and very knowledgeable about the Dhamma and we used to talk

138
00:07:50,980 --> 00:07:52,120
 about the Dhamma.

139
00:07:52,120 --> 00:07:55,260
 And he was really impressed and he said, "This is a great

140
00:07:55,260 --> 00:07:55,880
 thing.

141
00:07:55,880 --> 00:07:59,870
 People who I've never seen these people give before are now

142
00:07:59,870 --> 00:08:01,080
 into giving."

143
00:08:01,080 --> 00:08:02,080
 So there was that.

144
00:08:02,080 --> 00:08:04,640
 But that's really what it's about.

145
00:08:04,640 --> 00:08:07,590
 It's the difference, this verses are about the difference

146
00:08:07,590 --> 00:08:09,240
 between spiritual practices.

147
00:08:09,240 --> 00:08:12,410
 We have this idea of the spiritual practice in many

148
00:08:12,410 --> 00:08:14,840
 Buddhist cultures of paying respect

149
00:08:14,840 --> 00:08:19,120
 to gods or angels and they've started making up angels.

150
00:08:19,120 --> 00:08:22,400
 They've taken them from Hindu myths like Ganesha.

151
00:08:22,400 --> 00:08:28,240
 People worship this monkey, no sorry, the elephant, god.

152
00:08:28,240 --> 00:08:34,160
 And they have all these myths about Ganesha and Hanuman.

153
00:08:34,160 --> 00:08:38,250
 In this same village, there was a woman who claimed to be

154
00:08:38,250 --> 00:08:39,280
 an avatar.

155
00:08:39,280 --> 00:08:45,680
 An avatar is a big thing in Thailand.

156
00:08:45,680 --> 00:08:48,200
 An avatar for Hanuman.

157
00:08:48,200 --> 00:08:50,360
 And of course Hanuman is this legend.

158
00:08:50,360 --> 00:08:51,360
 It's a story really.

159
00:08:51,360 --> 00:08:55,970
 I mean there's not even any ancient religious texts I think

160
00:08:55,970 --> 00:08:57,920
 that have Hanuman in them.

161
00:08:57,920 --> 00:09:02,260
 It's this tale of the Ramayana which is a fairly modern

162
00:09:02,260 --> 00:09:02,600
 tale.

163
00:09:02,600 --> 00:09:05,820
 And yet in India as well, they worship Hanuman or they seem

164
00:09:05,820 --> 00:09:06,240
 to.

165
00:09:06,240 --> 00:09:09,970
 They have monkeys, monkey statues in front of their fields

166
00:09:09,970 --> 00:09:11,760
 to ward away evil demons and

167
00:09:11,760 --> 00:09:15,800
 stuff.

168
00:09:15,800 --> 00:09:20,700
 But yeah, these sort of things not worth a quarter of the,

169
00:09:20,700 --> 00:09:24,480
 she says, not a fourth part.

170
00:09:24,480 --> 00:09:26,480
 Not Chaturbhaagameti, Chaturbhaagameti.

171
00:09:26,480 --> 00:09:30,080
 They don't come to a quarter, they don't even come to a

172
00:09:30,080 --> 00:09:32,200
 thousandth part as we were talking

173
00:09:32,200 --> 00:09:34,200
 about in the earlier ones.

174
00:09:34,200 --> 00:09:36,760
 Anyway, so he tells him this.

175
00:09:36,760 --> 00:09:39,080
 It's a little bit different.

176
00:09:39,080 --> 00:09:44,140
 He takes him, he says, "Look, you have to come to see the

177
00:09:44,140 --> 00:09:45,400
 teacher."

178
00:09:45,400 --> 00:09:47,860
 And he takes him to see the teacher and this time he

179
00:09:47,860 --> 00:09:49,800
 actually says, "Bhante, please tell

180
00:09:49,800 --> 00:09:53,160
 this man the way to the Brahman world."

181
00:09:53,160 --> 00:09:55,040
 And the Buddha, but the Buddha says the same thing.

182
00:09:55,040 --> 00:09:58,890
 Buddha asks him what he does and he says, "You could do

183
00:09:58,890 --> 00:10:00,920
 that for a year and yet it would

184
00:10:00,920 --> 00:10:06,440
 not be worth the smallest piece of offering."

185
00:10:06,440 --> 00:10:10,960
 And here he says something also a bit different.

186
00:10:10,960 --> 00:10:18,120
 Lokya Mahajanasa Dinnadana.

187
00:10:18,120 --> 00:10:28,010
 So if you were to give, if you were instead to give to the

188
00:10:28,010 --> 00:10:29,480
 populace, if you were instead

189
00:10:29,480 --> 00:10:33,410
 to give to poor people, for example, that would be, that

190
00:10:33,410 --> 00:10:35,680
 was one thing would be of greater

191
00:10:35,680 --> 00:10:36,680
 value.

192
00:10:36,680 --> 00:10:39,430
 So he distinguishes there and it's an interesting point

193
00:10:39,430 --> 00:10:41,120
 because here the Buddha is sort of seems

194
00:10:41,120 --> 00:10:46,200
 to be advocating giving of alms.

195
00:10:46,200 --> 00:10:49,050
 That's what the English translation says and that looks

196
00:10:49,050 --> 00:10:51,040
 like what the Pali says, but it's

197
00:10:51,040 --> 00:10:56,480
 something to do with giving gifts to worldly people.

198
00:10:56,480 --> 00:11:00,210
 It's much better because here he's slaughtering animals,

199
00:11:00,210 --> 00:11:02,640
 which of course is actually unwholesome.

200
00:11:02,640 --> 00:11:05,280
 But then he goes on to say, "Pasana jitdena mamasavakanam,

201
00:11:05,280 --> 00:11:14,960
 wandantana to pay respect to

202
00:11:14,960 --> 00:11:17,440
 my disciples."

203
00:11:17,440 --> 00:11:26,430
 Yeah, the kusala jit and the wholesome mind that comes is

204
00:11:26,430 --> 00:11:29,960
 far more powerful.

205
00:11:29,960 --> 00:11:33,450
 I think he's quite understating the truth here because

206
00:11:33,450 --> 00:11:35,520
 honestly sacrificing animals

207
00:11:35,520 --> 00:11:40,430
 is not only a portion, it's not only a fraction as good, it

208
00:11:40,430 --> 00:11:42,520
's actually harmful.

209
00:11:42,520 --> 00:11:46,970
 There's nothing good about offering slaughtering animals as

210
00:11:46,970 --> 00:11:48,160
 an offering.

211
00:11:48,160 --> 00:11:51,860
 So the Buddha is, I think, going a little bit easy on him

212
00:11:51,860 --> 00:11:53,720
 because probably if he were

213
00:11:53,720 --> 00:11:58,740
 to say that's actually an unwholesome thing, then it would

214
00:11:58,740 --> 00:12:01,040
 perhaps set the guy off and

215
00:12:01,040 --> 00:12:03,240
 make him upset at the Buddha.

216
00:12:03,240 --> 00:12:05,200
 So he couches it a bit nicer.

217
00:12:05,200 --> 00:12:11,060
 He doesn't say how much less good it is than a quarter, but

218
00:12:11,060 --> 00:12:13,560
 it's at least less than a quarter

219
00:12:13,560 --> 00:12:15,600
 is good.

220
00:12:15,600 --> 00:12:16,600
 It's actually harmful.

221
00:12:16,600 --> 00:12:19,440
 And that's true of many spiritual practices.

222
00:12:19,440 --> 00:12:24,050
 So again, as I've talked about, we have to be aware of the

223
00:12:24,050 --> 00:12:26,520
 true benefit of our spiritual

224
00:12:26,520 --> 00:12:27,520
 practices.

225
00:12:27,520 --> 00:12:31,140
 We can't just think, "I'm doing this and this is what

226
00:12:31,140 --> 00:12:32,840
 people tell me to do.

227
00:12:32,840 --> 00:12:35,960
 This is spiritual and therefore it's good."

228
00:12:35,960 --> 00:12:39,120
 Spiritual practice can actually be harmful.

229
00:12:39,120 --> 00:12:42,550
 This was a spiritual practice that obviously was, offering

230
00:12:42,550 --> 00:12:44,160
 killing animals in the name

231
00:12:44,160 --> 00:12:48,000
 of God, that kind of thing.

232
00:12:48,000 --> 00:12:51,630
 Sometimes we take people who have the idea that taking

233
00:12:51,630 --> 00:12:53,760
 drugs is spiritual and I'm not

234
00:12:53,760 --> 00:12:56,700
 going to go out and say that that's harmful, but my

235
00:12:56,700 --> 00:12:59,240
 suspicion is that it has a great potential

236
00:12:59,240 --> 00:13:01,840
 for harm if not being outright harmful.

237
00:13:01,840 --> 00:13:04,520
 I mean, I think a lot of people would say that taking even

238
00:13:04,520 --> 00:13:06,120
 psychedelic drugs and hoping

239
00:13:06,120 --> 00:13:09,620
 that it's somehow as beneficial as a spiritual practice, I

240
00:13:09,620 --> 00:13:11,560
 think some people would say that

241
00:13:11,560 --> 00:13:12,840
 that is outright harmful.

242
00:13:12,840 --> 00:13:15,650
 I'm not sure that I'd go quite so far because there is an

243
00:13:15,650 --> 00:13:17,520
 argument to be made that it opens

244
00:13:17,520 --> 00:13:20,280
 up your mind to alternate states of reality.

245
00:13:20,280 --> 00:13:24,140
 But I think there's a lot of delusion involved, like the

246
00:13:24,140 --> 00:13:26,480
 idea that those states have some

247
00:13:26,480 --> 00:13:30,890
 meaning or purpose or value when in fact they're all messed

248
00:13:30,890 --> 00:13:32,440
 up with delusion.

249
00:13:32,440 --> 00:13:37,510
 I've been there, done that, and it's just all a mess of our

250
00:13:37,510 --> 00:13:39,600
 mind, what our mind can

251
00:13:39,600 --> 00:13:48,080
 come up with when it's doped up and it's high.

252
00:13:48,080 --> 00:13:50,480
 So these kind of things.

253
00:13:50,480 --> 00:13:53,820
 And you compare that, for example, taking drugs as a

254
00:13:53,820 --> 00:13:55,800
 spiritual practice to a Buddhist

255
00:13:55,800 --> 00:13:58,680
 spiritual practice, like giving charity, or even just

256
00:13:58,680 --> 00:14:00,360
 holding your hands up and paying

257
00:14:00,360 --> 00:14:04,560
 respect to someone who's worthy of respect.

258
00:14:04,560 --> 00:14:05,560
 Who would think of that?

259
00:14:05,560 --> 00:14:09,990
 It shows how far we, many people are, from true spiritual

260
00:14:09,990 --> 00:14:11,000
 practice.

261
00:14:11,000 --> 00:14:16,210
 Many people say, "Oh, taking drugs, that's a spiritual

262
00:14:16,210 --> 00:14:19,200
 practice," or doing expansive

263
00:14:19,200 --> 00:14:23,450
 rituals or offering copious sacrifices or this kind of

264
00:14:23,450 --> 00:14:25,920
 thing, doing very complex or

265
00:14:25,920 --> 00:14:28,600
 very mystical things, maybe dancing.

266
00:14:28,600 --> 00:14:31,820
 People say, "In the West we become very hedonistic with our

267
00:14:31,820 --> 00:14:32,960
 spirituality."

268
00:14:32,960 --> 00:14:35,760
 So maybe people say, "Group orgies and karmic sex," and

269
00:14:35,760 --> 00:14:36,920
 that kind of thing.

270
00:14:36,920 --> 00:14:38,920
 These are spiritual.

271
00:14:38,920 --> 00:14:41,130
 Contrast that to the Buddha's idea of what true

272
00:14:41,130 --> 00:14:43,080
 spirituality is, holding your hands up

273
00:14:43,080 --> 00:14:44,440
 and paying respect.

274
00:14:44,440 --> 00:14:47,560
 That is far more spiritual.

275
00:14:47,560 --> 00:14:51,080
 So it's such a mundane-seeming thing, but far more

276
00:14:51,080 --> 00:14:53,800
 spiritual and far more beneficial,

277
00:14:53,800 --> 00:14:57,680
 even just for a moment than a hundred years of dancing,

278
00:14:57,680 --> 00:15:00,040
 spiritual dancing or spiritual

279
00:15:00,040 --> 00:15:11,480
 music or spiritual sex, drugs, all these things.

280
00:15:11,480 --> 00:15:15,250
 So for someone looking for punya, which we all are, punya

281
00:15:15,250 --> 00:15:16,420
 is goodness.

282
00:15:16,420 --> 00:15:21,650
 Even if we're just set on meditation, it's important not to

283
00:15:21,650 --> 00:15:23,760
 become self-centered in the

284
00:15:23,760 --> 00:15:28,430
 sense of, "Me, me, me, I want to become spiritually

285
00:15:28,430 --> 00:15:29,600
 enlightened."

286
00:15:29,600 --> 00:15:33,350
 Enlightenment is about letting go, so it's a lot of self-

287
00:15:33,350 --> 00:15:34,360
sacrifice.

288
00:15:34,360 --> 00:15:36,560
 An enlightened being would be able to give up anything.

289
00:15:36,560 --> 00:15:40,440
 If someone asks them for something, they would have no

290
00:15:40,440 --> 00:15:43,240
 sense of self, of helping themselves.

291
00:15:43,240 --> 00:15:45,920
 They have no qualms there.

292
00:15:45,920 --> 00:15:49,370
 They consider what is proper to do and they do it without

293
00:15:49,370 --> 00:15:51,320
 any attachment, without any

294
00:15:51,320 --> 00:15:52,320
 greed.

295
00:15:52,320 --> 00:15:53,720
 And they're very respectful.

296
00:15:53,720 --> 00:15:55,200
 They honor those who have helped them.

297
00:15:55,200 --> 00:15:57,990
 They're grateful to those who have helped them, that kind

298
00:15:57,990 --> 00:15:58,600
 of thing.

299
00:15:58,600 --> 00:16:00,320
 And they're very down to earth.

300
00:16:00,320 --> 00:16:04,360
 So noble spirituality is like this.

301
00:16:04,360 --> 00:16:05,360
 What's spiritual?

302
00:16:05,360 --> 00:16:08,650
 Well, paying respect to those who are worthy of respect,

303
00:16:08,650 --> 00:16:09,880
 giving gifts to those who are

304
00:16:09,880 --> 00:16:11,600
 worthy of gifts.

305
00:16:11,600 --> 00:16:15,760
 And mostly just being aware, being here, being present,

306
00:16:15,760 --> 00:16:18,680
 being with mundane reality, understanding

307
00:16:18,680 --> 00:16:23,690
 truth, true reality, and not being concerned with mysticism

308
00:16:23,690 --> 00:16:26,240
 or altered states of existence

309
00:16:26,240 --> 00:16:27,880
 or that kind of thing.

310
00:16:27,880 --> 00:16:30,820
 Astral travel, anyone who's obsessed with astral travel, I

311
00:16:30,820 --> 00:16:32,280
 mean, it's not to say enlightened

312
00:16:32,280 --> 00:16:36,080
 people can't practice it, but people who are striving for

313
00:16:36,080 --> 00:16:40,400
 that are really missing the point.

314
00:16:40,400 --> 00:16:44,910
 Mahasya Sayadaw tells the story of a woman who he knew who

315
00:16:44,910 --> 00:16:47,240
 was working very hard to try

316
00:16:47,240 --> 00:16:49,800
 and see out of her ears.

317
00:16:49,800 --> 00:16:54,760
 This was when she was trying to gain the spiritual ability

318
00:16:54,760 --> 00:16:56,920
 to see out of her ears.

319
00:16:56,920 --> 00:17:00,220
 And he said, "And this is when her eyes worked perfectly

320
00:17:00,220 --> 00:17:00,840
 fine."

321
00:17:00,840 --> 00:17:03,440
 So we get off done on the wrong path.

322
00:17:03,440 --> 00:17:10,650
 Your spirituality is often easy to mistake for ordinary

323
00:17:10,650 --> 00:17:12,240
 reality.

324
00:17:12,240 --> 00:17:15,070
 I always say that enlightened people are what we think we

325
00:17:15,070 --> 00:17:15,480
 are.

326
00:17:15,480 --> 00:17:17,400
 We all think of ourselves as living normally.

327
00:17:17,400 --> 00:17:21,840
 We think of ourselves as normal, but we're not.

328
00:17:21,840 --> 00:17:25,770
 Our defilements take us away from what we think we are,

329
00:17:25,770 --> 00:17:28,520
 take us away from mundane reality.

330
00:17:28,520 --> 00:17:31,910
 So I've made much of this very simple verse, which is very

331
00:17:31,910 --> 00:17:33,960
 much like the other two verses.

332
00:17:33,960 --> 00:17:37,240
 And I promise the next one is different.

333
00:17:37,240 --> 00:17:40,860
 Still along the same vein, but it's a totally different

334
00:17:40,860 --> 00:17:43,280
 context and story, everything.

335
00:17:43,280 --> 00:17:49,430
 And it's actually one of the, probably, if not the most

336
00:17:49,430 --> 00:17:53,080
 famous verses in the Dhammapada

337
00:17:53,080 --> 00:17:57,360
 coming up next time.

338
00:17:57,360 --> 00:18:01,890
 And then after that we have the story of Sankicca, which is

339
00:18:01,890 --> 00:18:04,560
 a very nice story, and so on and so

340
00:18:04,560 --> 00:18:05,560
 on.

341
00:18:05,560 --> 00:18:06,560
 Not all the stories are long.

342
00:18:06,560 --> 00:18:08,520
 Some of them are very short, as you can see.

343
00:18:08,520 --> 00:18:10,120
 Some of them are very long.

344
00:18:10,120 --> 00:18:15,060
 I think the longest ones have already gone by, so we're not

345
00:18:15,060 --> 00:18:18,200
 going to have great storytelling.

346
00:18:18,200 --> 00:18:21,520
 Not all of it is going to be great stories.

347
00:18:21,520 --> 00:18:25,010
 But always something new and some new message to be found

348
00:18:25,010 --> 00:18:26,400
 in the Dhammapada.

349
00:18:26,400 --> 00:18:30,000
 And of course the verses themselves are things you can

350
00:18:30,000 --> 00:18:31,160
 reflect upon.

351
00:18:31,160 --> 00:18:34,180
 What is important, this is the Sahaso-laga, which is

352
00:18:34,180 --> 00:18:34,960
 thousands.

353
00:18:34,960 --> 00:18:38,480
 So it's contrasting single things to thousands of things,

354
00:18:38,480 --> 00:18:39,760
 for the most part.

355
00:18:39,760 --> 00:18:43,860
 Thousand of something useless, that is one thing that is

356
00:18:43,860 --> 00:18:44,680
 useful.

357
00:18:44,680 --> 00:18:48,480
 So that's the Dhammapada for tonight.

358
00:18:48,480 --> 00:18:52,120
 Thank you all for tuning in.

359
00:18:52,120 --> 00:18:53,120
 Keep practicing.

360
00:18:53,120 --> 00:18:53,520
 Be well.

