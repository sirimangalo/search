1
00:00:00,000 --> 00:00:13,480
 So today I'll talk about blessings number 11 to 14.

2
00:00:13,480 --> 00:00:40,480
 To

3
00:00:40,480 --> 00:00:44,760
 care for one's father and one's mother.

4
00:00:44,760 --> 00:00:54,230
 Put the darasasankahoa to take care of one's family, one's

5
00:00:54,230 --> 00:01:04,680
 spouse and one's children.

6
00:01:04,680 --> 00:01:20,640
 Whatever actions are unconfused or unentangled, which are

7
00:01:20,640 --> 00:01:26,400
 clear and free from any actions

8
00:01:26,400 --> 00:01:28,400
 which are free and clear.

9
00:01:28,400 --> 00:01:42,880
 These are blessings in the highest sense.

10
00:01:42,880 --> 00:01:50,370
 To take care of one's father and mother is something we can

11
00:01:50,370 --> 00:01:53,880
 see plays a very important

12
00:01:53,880 --> 00:01:59,040
 role in the Lord Buddha's teaching.

13
00:01:59,040 --> 00:02:07,040
 Even as a bodhisattva, before the Lord Buddha had become fl

14
00:02:07,040 --> 00:02:08,400
inged.

15
00:02:08,400 --> 00:02:14,080
 In birth after birth he would look after his parents.

16
00:02:14,080 --> 00:02:24,680
 Something he had realized early on was most important.

17
00:02:24,680 --> 00:02:30,550
 And even once he became an enlightened Buddha, he continued

18
00:02:30,550 --> 00:02:33,400
 to look after his parents and

19
00:02:33,400 --> 00:02:44,850
 think of his parents and his disciples carried on the

20
00:02:44,850 --> 00:02:48,040
 tradition.

21
00:02:48,040 --> 00:02:51,950
 Before Suryaputta was going to pass away, he knew that he

22
00:02:51,950 --> 00:02:53,960
 was old and was ready to pass

23
00:02:53,960 --> 00:02:57,720
 away and he got very sick.

24
00:02:57,720 --> 00:03:02,220
 And he knew that his time had come, but he knew that he

25
00:03:02,220 --> 00:03:04,800
 shouldn't pass away before he

26
00:03:04,800 --> 00:03:11,940
 had gone back to teach his mother the right path and the

27
00:03:11,940 --> 00:03:14,400
 right way to go.

28
00:03:14,400 --> 00:03:25,490
 So even though he was sick, he went back to visit his

29
00:03:25,490 --> 00:03:30,240
 mother in Nalanda.

30
00:03:30,240 --> 00:03:37,720
 And even his mother was a Brahmin, he lived in ritual

31
00:03:37,720 --> 00:03:39,800
 sacrifice.

32
00:03:39,800 --> 00:03:47,460
 And thought that the Lord Buddha's teaching was just a

33
00:03:47,460 --> 00:03:50,120
 bunch of heresy.

34
00:03:50,120 --> 00:03:54,210
 Suryaputta, out of respect for his mother, in order to be a

35
00:03:54,210 --> 00:03:56,640
 support for his mother, thought

36
00:03:56,640 --> 00:04:02,020
 his mother, what was the truth of the Lord Buddha's

37
00:04:02,020 --> 00:04:03,320
 teaching.

38
00:04:03,320 --> 00:04:09,170
 You can see Tharavan's mother and father is not just

39
00:04:09,170 --> 00:04:13,480
 something that is useful in a worldly

40
00:04:13,480 --> 00:04:17,250
 sense, it's most important in terms of the practice of the

41
00:04:17,250 --> 00:04:18,040
 Dhamma.

42
00:04:18,040 --> 00:04:20,610
 Whatever problems we have with our mother and our father

43
00:04:20,610 --> 00:04:22,040
 will always come back to haunt

44
00:04:22,040 --> 00:04:32,720
 us during the practice.

45
00:04:32,720 --> 00:04:37,740
 And we practice and the most important is to send all of

46
00:04:37,740 --> 00:04:40,440
 the goodness that we have in

47
00:04:40,440 --> 00:04:45,060
 our hearts and all of the good deeds that we do in the

48
00:04:45,060 --> 00:04:48,160
 practice of meditation, all the

49
00:04:48,160 --> 00:04:50,960
 goodness which comes from the practice of meditation.

50
00:04:50,960 --> 00:04:57,600
 It's most important that we dedicate the goodness to our

51
00:04:57,600 --> 00:05:01,480
 mother and father, something which

52
00:05:01,480 --> 00:05:08,380
 makes our hearts calm and peaceful and allows us to

53
00:05:08,380 --> 00:05:11,880
 practice meditation.

54
00:05:11,880 --> 00:05:18,830
 And when we have a chance we should try to find a way to go

55
00:05:18,830 --> 00:05:22,440
 back and repair our parents

56
00:05:22,440 --> 00:05:28,160
 for all the good things that they've done to us.

57
00:05:28,160 --> 00:05:31,210
 Lord Buddha said it's very difficult to repay one's parents

58
00:05:31,210 --> 00:05:32,800
 because what they've done for

59
00:05:32,800 --> 00:05:41,050
 us is something that we cannot do for them, not in this

60
00:05:41,050 --> 00:05:42,400
 life.

61
00:05:42,400 --> 00:05:48,800
 They were our first teachers, they were our protectors,

62
00:05:48,800 --> 00:05:52,440
 supporters without ever asking

63
00:05:52,440 --> 00:05:59,920
 for repayment.

64
00:05:59,920 --> 00:06:03,640
 So the Lord Buddha taught that the only way we could really

65
00:06:03,640 --> 00:06:05,680
 do something that would equal

66
00:06:05,680 --> 00:06:11,200
 or would be able to rival the goodness that they had done

67
00:06:11,200 --> 00:06:14,280
 for us is if we bring the Dhamma

68
00:06:14,280 --> 00:06:16,360
 to them.

69
00:06:16,360 --> 00:06:20,470
 There's no amount of support we can give to them that could

70
00:06:20,470 --> 00:06:22,280
 equal the support that they

71
00:06:22,280 --> 00:06:27,560
 have given to us except in the Dhamma, except in the truth.

72
00:06:27,560 --> 00:06:33,320
 If we bring to them the reality of them to come to see what

73
00:06:33,320 --> 00:06:36,000
 are good deeds, what are

74
00:06:36,000 --> 00:06:41,900
 bad deeds, what is the fruit of bad deeds, what is the

75
00:06:41,900 --> 00:06:44,520
 fruit of good deeds.

76
00:06:44,520 --> 00:06:49,090
 Teach them to have faith in the truth, teach them to come

77
00:06:49,090 --> 00:06:51,560
 to see the truth, teach them

78
00:06:51,560 --> 00:06:56,320
 to have morality, to avoid evil deeds.

79
00:06:56,320 --> 00:07:02,120
 Teach them how to practice Vipassana meditation.

80
00:07:02,120 --> 00:07:14,920
 This is a way Lord Buddha said to Rupay once more.

81
00:07:14,920 --> 00:07:27,000
 The highest blessing because of the support it gives to the

82
00:07:27,000 --> 00:07:32,080
 mind.

83
00:07:32,080 --> 00:07:37,210
 As I said, the person who has never supported their parents

84
00:07:37,210 --> 00:07:40,000
 or has done bad things to one's

85
00:07:40,000 --> 00:07:48,800
 parents, you find it very difficult to practice meditation.

86
00:07:48,800 --> 00:07:54,270
 The blessing which comes from having supported one's

87
00:07:54,270 --> 00:07:58,480
 parents is the great peace and happiness

88
00:07:58,480 --> 00:08:06,220
 from knowing that one has paid back to people who are very

89
00:08:06,220 --> 00:08:09,760
 difficult to pay back.

90
00:08:09,760 --> 00:08:21,000
 When one brings them goodness and happiness in their hearts

91
00:08:21,000 --> 00:08:22,560
, it's a blessing in the greatest

92
00:08:22,560 --> 00:08:32,160
 and the highest sense.

93
00:08:32,160 --> 00:08:37,120
 Next is two blessings mixed into one.

94
00:08:37,120 --> 00:08:39,320
 We count them as two.

95
00:08:39,320 --> 00:08:46,830
 One is looking after one's children and one is looking

96
00:08:46,830 --> 00:08:49,600
 after her house.

97
00:08:49,600 --> 00:08:55,460
 These are two blessings which are blessings in a worldly

98
00:08:55,460 --> 00:08:56,360
 sense.

99
00:08:56,360 --> 00:09:04,680
 In Vai Nudadhamma, monks and nuns don't have spouses.

100
00:09:04,680 --> 00:09:11,650
 But as far as children, they can very easily have children

101
00:09:11,650 --> 00:09:15,520
 because in Buddhism, the word

102
00:09:15,520 --> 00:09:20,240
 child is used in many different ways.

103
00:09:20,240 --> 00:09:36,000
 There's the child that comes through the body.

104
00:09:36,000 --> 00:09:38,080
 This is the biological child.

105
00:09:38,080 --> 00:09:42,680
 One is the biological parent of the child.

106
00:09:42,680 --> 00:09:49,670
 In this case, it's not likely or not frequent where a monk

107
00:09:49,670 --> 00:09:53,120
 or a nun has such a child that

108
00:09:53,120 --> 00:10:10,800
 they could have from their past life as a lay person.

109
00:10:10,800 --> 00:10:17,360
 And then there's the child who is a child of the village.

110
00:10:17,360 --> 00:10:21,200
 It's living in the same village.

111
00:10:21,200 --> 00:10:25,470
 So when they say that there's an ancient proverb which says

112
00:10:25,470 --> 00:10:27,520
 that it takes a whole village to

113
00:10:27,520 --> 00:10:34,000
 raise a child, which is what the proverb means.

114
00:10:34,000 --> 00:10:40,720
 What this kind of child refers to.

115
00:10:40,720 --> 00:10:43,720
 Where the whole village looks after all the children.

116
00:10:43,720 --> 00:10:47,050
 Where we look after children as our own because they live

117
00:10:47,050 --> 00:10:49,000
 in the same village or they live

118
00:10:49,000 --> 00:10:56,000
 in the same area.

119
00:10:56,000 --> 00:11:02,570
 And then the third kind is one who comes to us as a student

120
00:11:02,570 --> 00:11:02,720
.

121
00:11:02,720 --> 00:11:05,620
 Whether someone else's parents bring them to us to look

122
00:11:05,620 --> 00:11:07,320
 after because we're a teacher

123
00:11:07,320 --> 00:11:14,080
 or else they come on their own accord.

124
00:11:14,080 --> 00:11:17,970
 This becomes our child in the sense of our student, someone

125
00:11:17,970 --> 00:11:19,920
 who we teach how to practice

126
00:11:19,920 --> 00:11:20,920
 meditation.

127
00:11:20,920 --> 00:11:30,720
 We teach how to practice the Lord Buddha's teaching.

128
00:11:30,720 --> 00:11:39,710
 And looking after children is the most important thing as

129
00:11:39,710 --> 00:11:41,080
 well.

130
00:11:41,080 --> 00:11:45,230
 Someone else has given to us all the goodness we have

131
00:11:45,230 --> 00:11:46,160
 inside.

132
00:11:46,160 --> 00:11:56,160
 Someone else has helped us to get whatever benefit we can

133
00:11:56,160 --> 00:12:01,160
 from the spiritual life.

134
00:12:01,160 --> 00:12:05,850
 And if we keep to ourselves and don't pass it on, it will

135
00:12:05,850 --> 00:12:07,600
 end right there.

136
00:12:07,600 --> 00:12:17,520
 We find the world will not be able to sustain the goodness.

137
00:12:17,520 --> 00:12:23,960
 There's no one to continue the goodness.

138
00:12:23,960 --> 00:12:32,000
 Even here in Thailand, maybe 50 years ago there was not so

139
00:12:32,000 --> 00:12:35,680
 much repassant of practice

140
00:12:35,680 --> 00:12:47,380
 in Thailand until Som did Putachan, the abbot of Wat Mahath

141
00:12:47,380 --> 00:12:48,080
ar.

142
00:12:48,080 --> 00:12:53,880
 He had a wish because he had studied so much of the Buddha

143
00:12:53,880 --> 00:12:56,840
's teaching and he had established

144
00:12:56,840 --> 00:13:04,890
 so much study of the Buddha's teaching in monastery and all

145
00:13:04,890 --> 00:13:08,400
 around Thailand even.

146
00:13:08,400 --> 00:13:14,320
 But he said what Thailand was missing was meditation

147
00:13:14,320 --> 00:13:15,840
 practice.

148
00:13:15,840 --> 00:13:20,200
 If they didn't have meditation practice there would be no

149
00:13:20,200 --> 00:13:22,840
 enlightened beings in the country.

150
00:13:22,840 --> 00:13:29,230
 There would be no one who could really become free from

151
00:13:29,230 --> 00:13:31,000
 suffering.

152
00:13:31,000 --> 00:13:34,560
 There was no strong concentrated practice of repassant of

153
00:13:34,560 --> 00:13:35,560
 meditation.

154
00:13:35,560 --> 00:13:41,950
 There was no one teaching how to practice repassant of

155
00:13:41,950 --> 00:13:43,880
 meditation.

156
00:13:43,880 --> 00:13:51,190
 So he helped to find the teacher and to establish the

157
00:13:51,190 --> 00:13:53,320
 teaching here in Thailand.

158
00:13:53,320 --> 00:13:57,140
 And now we can see Thailand is becoming famous for repass

159
00:13:57,140 --> 00:13:59,080
ant of meditation.

160
00:13:59,080 --> 00:14:08,600
 Wat Chom Thong is quite well known.

161
00:14:08,600 --> 00:14:18,720
 Wat Lam Boon, Wat Mahathat, Wat Am Bhawan, Wat Riwi Ka Som,

162
00:14:18,720 --> 00:14:20,040
 there's many places around

163
00:14:20,040 --> 00:14:24,000
 the country where one can practice repassant of meditation.

164
00:14:24,000 --> 00:14:31,580
 Millions of people practicing, maybe not millions,

165
00:14:31,580 --> 00:14:38,560
 thousands and thousands of people practicing.

166
00:14:38,560 --> 00:14:42,790
 And this is a kind of way of looking after one's children,

167
00:14:42,790 --> 00:14:43,840
 looking after the generations

168
00:14:43,840 --> 00:14:46,840
 to come.

169
00:14:46,840 --> 00:14:59,490
 The people who will come after we are gone to pass on

170
00:14:59,490 --> 00:15:02,680
 teaching, to keep the world in

171
00:15:02,680 --> 00:15:06,640
 peace and harmony.

172
00:15:06,640 --> 00:15:18,760
 This is one way of explaining puta sangha.

173
00:15:18,760 --> 00:15:21,440
 But in the world also we can, it's most important that we

174
00:15:21,440 --> 00:15:23,520
 look after our children, our biological

175
00:15:23,520 --> 00:15:26,680
 and the children around us.

176
00:15:26,680 --> 00:15:30,700
 But here when we talk about this in the setting of repass

177
00:15:30,700 --> 00:15:33,280
ant of meditation, we talk about

178
00:15:33,280 --> 00:15:37,580
 passing on and spreading the teaching, looking after other

179
00:15:37,580 --> 00:15:38,400
 people.

180
00:15:38,400 --> 00:15:41,480
 Right now we're coming to look after ourselves first.

181
00:15:41,480 --> 00:15:46,700
 But when we have a chance then we can go and look after

182
00:15:46,700 --> 00:15:50,320
 other people and help other people

183
00:15:50,320 --> 00:15:56,200
 to become free from suffering.

184
00:15:56,200 --> 00:16:01,420
 And the third blessing is looking after one's spouse,

185
00:16:01,420 --> 00:16:04,920
 specifically in a worldly sense.

186
00:16:04,920 --> 00:16:13,130
 You're hard to find an equivalent for the reckless and for

187
00:16:13,130 --> 00:16:17,000
 the monk and for the nun.

188
00:16:17,000 --> 00:16:21,450
 Looking after one's spouse is another important

189
00:16:21,450 --> 00:16:24,360
 establishment in Buddhism.

190
00:16:24,360 --> 00:16:29,510
 Of course it's just a concept, there's no ultimate reality

191
00:16:29,510 --> 00:16:31,800
 and a need for a spouse.

192
00:16:31,800 --> 00:16:36,510
 But in the Buddha's time and as well in the present time I

193
00:16:36,510 --> 00:16:38,960
 think it is a very important

194
00:16:38,960 --> 00:16:47,680
 establishment for people who are living in the world.

195
00:16:47,680 --> 00:16:52,060
 Even those followers of the Lord Buddha who became anakami,

196
00:16:52,060 --> 00:16:54,320
 they could still be married.

197
00:16:54,320 --> 00:17:02,360
 They still have a wife, still have a husband.

198
00:17:02,360 --> 00:17:06,360
 But they would become disinterested in all sexual activity

199
00:17:06,360 --> 00:17:08,240
 and all romantic activity.

200
00:17:08,240 --> 00:17:15,400
 But they could still live in something like brother and

201
00:17:15,400 --> 00:17:19,200
 sister because it is the most

202
00:17:19,200 --> 00:17:27,460
 helpful and supportive environment to live as a husband and

203
00:17:27,460 --> 00:17:28,680
 wife.

204
00:17:28,680 --> 00:17:30,680
 One is to live in the world.

205
00:17:30,680 --> 00:17:34,400
 It's very difficult to live alone.

206
00:17:34,400 --> 00:17:39,340
 You have to take care of the house which you live in and

207
00:17:39,340 --> 00:17:41,760
 you have to go out and try to

208
00:17:41,760 --> 00:17:47,600
 find money, try to find support and resources.

209
00:17:47,600 --> 00:17:50,880
 To do both of these things at once is quite difficult.

210
00:17:50,880 --> 00:17:59,640
 We have the establishment of the husband and wife.

211
00:17:59,640 --> 00:18:05,280
 One whichever one to stay home and whichever one to go out

212
00:18:05,280 --> 00:18:08,080
 and find the support to keep

213
00:18:08,080 --> 00:18:15,480
 one's, to keep both people's life going.

214
00:18:15,480 --> 00:18:21,300
 So we support each other by being faithful to one another,

215
00:18:21,300 --> 00:18:24,240
 by respecting one another,

216
00:18:24,240 --> 00:18:31,000
 by taking care of our respective duties.

217
00:18:31,000 --> 00:18:36,800
 This is a great blessing in the world.

218
00:18:36,800 --> 00:18:39,100
 So we ask how do monks and nuns get along if they don't

219
00:18:39,100 --> 00:18:40,480
 have this kind of support.

220
00:18:40,480 --> 00:18:47,040
 Monks and nuns have to rely only on themselves but they

221
00:18:47,040 --> 00:18:50,320
 also rely on each other.

222
00:18:50,320 --> 00:18:54,310
 Monks rely on the other monks and the nuns rely on the

223
00:18:54,310 --> 00:18:55,520
 other nuns.

224
00:18:55,520 --> 00:19:01,450
 They have some kind of very large family as a result of

225
00:19:01,450 --> 00:19:05,160
 their ordination that wherever

226
00:19:05,160 --> 00:19:08,400
 the monks go they can find support from other monks.

227
00:19:08,400 --> 00:19:11,090
 Where the nuns go they can find support from other nuns and

228
00:19:11,090 --> 00:19:12,320
 even the other way the monks

229
00:19:12,320 --> 00:19:16,120
 find support from nuns the nuns find support from monks.

230
00:19:16,120 --> 00:19:24,280
 They can support each other in so far as is appropriate.

231
00:19:24,280 --> 00:19:33,400
 This is the third, the third blessing.

232
00:19:33,400 --> 00:19:37,820
 The fourth one is whatever those deeds which are free and

233
00:19:37,820 --> 00:19:38,760
 clear.

234
00:19:38,760 --> 00:19:42,680
 An aquila means fused or entangled.

235
00:19:42,680 --> 00:19:49,000
 It could be entangled with deceit or treachery or evil.

236
00:19:49,000 --> 00:19:54,680
 It could be confused with delusion or laziness.

237
00:19:54,680 --> 00:20:03,040
 It's whatever work we have to do.

238
00:20:03,040 --> 00:20:11,080
 We do it with a clear mind.

239
00:20:11,080 --> 00:20:14,110
 In the time of the Lord Buddha there was a teaching called

240
00:20:14,110 --> 00:20:14,680
 karma.

241
00:20:14,680 --> 00:20:17,720
 Karma meant action.

242
00:20:17,720 --> 00:20:23,780
 Karma means that every action we do has karma potential,

243
00:20:23,780 --> 00:20:27,240
 has potential to bring fruits,

244
00:20:27,240 --> 00:20:30,520
 bring either suffering or happiness.

245
00:20:30,520 --> 00:20:35,470
 So nowadays people say the Lord Buddha taught the law of

246
00:20:35,470 --> 00:20:36,440
 karma.

247
00:20:36,440 --> 00:20:42,640
 It is true that he taught a law of karma.

248
00:20:42,640 --> 00:20:45,560
 But if you really going to call it the law of karma it's

249
00:20:45,560 --> 00:20:47,400
 actually the Lord Buddha didn't

250
00:20:47,400 --> 00:20:48,800
 teach the law of karma.

251
00:20:48,800 --> 00:20:58,180
 He taught that the law of karma was a false law.

252
00:20:58,180 --> 00:21:01,070
 Because according to the Lord Buddha karma is not the most

253
00:21:01,070 --> 00:21:02,140
 important thing.

254
00:21:02,140 --> 00:21:08,320
 Action is not the most important thing.

255
00:21:08,320 --> 00:21:11,730
 But rather than say it like that to all the people who

256
00:21:11,730 --> 00:21:14,040
 believed in karma as a very important

257
00:21:14,040 --> 00:21:18,960
 thing, the Lord Buddha was very clever.

258
00:21:18,960 --> 00:21:22,120
 He tried to explain what is real karma.

259
00:21:22,120 --> 00:21:25,770
 What constitutes a real action which will really bring

260
00:21:25,770 --> 00:21:26,600
 results.

261
00:21:26,600 --> 00:21:29,320
 Lord Buddha said it is the intention.

262
00:21:29,320 --> 00:21:33,080
 It is intention which is the deed.

263
00:21:33,080 --> 00:21:41,360
 The intention itself is the deed.

264
00:21:41,360 --> 00:21:44,450
 And so when we act with an impure heart with a heart of

265
00:21:44,450 --> 00:21:46,080
 greed or anger or delusion, this

266
00:21:46,080 --> 00:21:58,280
 is a deed which is called Akula.

267
00:21:58,280 --> 00:22:12,650
 If we just act without any special intention, it is not

268
00:22:12,650 --> 00:22:14,520
 true that every action will bring

269
00:22:14,520 --> 00:22:16,200
 some kind of karmic result.

270
00:22:16,200 --> 00:22:19,410
 It is only when we have greed or when we have anger or when

271
00:22:19,410 --> 00:22:20,640
 we have delusion.

272
00:22:20,640 --> 00:22:24,010
 So when we walk and we step on an ant without seeing the

273
00:22:24,010 --> 00:22:26,000
 ant, we can't say that that is

274
00:22:26,000 --> 00:22:29,760
 going to bring evil results to us.

275
00:22:29,760 --> 00:22:36,100
 But when we have anger in our mind, we kill or we steal or

276
00:22:36,100 --> 00:22:39,400
 we do evil things, for sure

277
00:22:39,400 --> 00:22:45,640
 this will bring evil results.

278
00:22:45,640 --> 00:22:48,040
 First it will bring evil results to our mind.

279
00:22:48,040 --> 00:23:00,620
 Then later on it will bring revenge and more suffering to

280
00:23:00,620 --> 00:23:02,040
 us.

281
00:23:02,040 --> 00:23:07,030
 And in the opposite sense, whatever deeds we do are

282
00:23:07,030 --> 00:23:10,840
 accompanied with mindfulness, with

283
00:23:10,840 --> 00:23:16,530
 intention to help, intention to bring freedom from

284
00:23:16,530 --> 00:23:20,640
 suffering, happiness and peace.

285
00:23:20,640 --> 00:23:25,320
 These deeds will surely bring good results in this life and

286
00:23:25,320 --> 00:23:26,920
 in the next life.

287
00:23:26,920 --> 00:23:31,420
 This is why it is most important that we develop our minds,

288
00:23:31,420 --> 00:23:34,520
 establish our minds in goodness.

289
00:23:34,520 --> 00:23:37,080
 See in our minds there are two sides.

290
00:23:37,080 --> 00:23:41,460
 There are good things in our minds and there are bad things

291
00:23:41,460 --> 00:23:42,760
 in our minds.

292
00:23:42,760 --> 00:23:45,160
 We always have the opportunity to choose.

293
00:23:45,160 --> 00:23:48,310
 Do we want to follow after bad things or do we want to

294
00:23:48,310 --> 00:23:50,720
 develop ourselves in good things?

295
00:23:50,720 --> 00:23:55,230
 When we come to practice vipassana, this we made the choice

296
00:23:55,230 --> 00:23:57,480
 to develop ourselves in good

297
00:23:57,480 --> 00:24:04,880
 things and to get rid of all the bad things in our minds.

298
00:24:04,880 --> 00:24:09,910
 So when we come to practice vipassana meditations, it's the

299
00:24:09,910 --> 00:24:11,880
 greatest blessing.

300
00:24:11,880 --> 00:24:18,040
 It brings all the blessings to us.

301
00:24:18,040 --> 00:24:22,560
 Our minds become free from confusion or entanglement.

302
00:24:22,560 --> 00:24:28,700
 Everything we do, everything we do becomes either a good

303
00:24:28,700 --> 00:24:30,960
 karma or no karma.

304
00:24:30,960 --> 00:24:37,520
 It's never any more bad karma.

305
00:24:37,520 --> 00:24:41,040
 Our minds are free from greed, free from anger, free from

306
00:24:41,040 --> 00:24:41,880
 delusion.

307
00:24:41,880 --> 00:24:48,260
 So when we act, we will act in a desire to help other

308
00:24:48,260 --> 00:24:52,240
 people, to support other people,

309
00:24:52,240 --> 00:24:57,150
 to help ourselves, to support ourselves, to bring only

310
00:24:57,150 --> 00:25:00,360
 peace and happiness to the world.

311
00:25:00,360 --> 00:25:03,000
 This is the 14th, blessing number 14.

312
00:25:03,000 --> 00:25:07,840
 God bless.

