1
00:00:00,000 --> 00:00:05,030
 Hello, welcome back to Ask a Monk. Now I have two questions

2
00:00:05,030 --> 00:00:07,000
 that were asked together.

3
00:00:07,000 --> 00:00:16,330
 The first question is whether after realizing nirvana, nir

4
00:00:16,330 --> 00:00:20,130
vana, which means freedom or release or emancipation or

5
00:00:20,130 --> 00:00:22,000
 however you want to translate it,

6
00:00:22,000 --> 00:00:28,240
 is there anything left to do or is nirvana it? I mean, is

7
00:00:28,240 --> 00:00:32,000
 that all you have to do? That's the first question.

8
00:00:32,000 --> 00:00:40,370
 So I'll answer this question first. The answer is easy. Nir

9
00:00:40,370 --> 00:00:45,000
vana is the last thing that we ever have to do.

10
00:00:45,000 --> 00:00:49,270
 Once you realize nirvana, there's nothing left that from a

11
00:00:49,270 --> 00:00:52,000
 Buddhist point of view you need to do.

12
00:00:52,000 --> 00:00:57,460
 The point is at that point you're free from suffering. So

13
00:00:57,460 --> 00:01:00,000
 what does this mean?

14
00:01:00,000 --> 00:01:05,070
 Nirvana is the cessation of suffering. This is what the

15
00:01:05,070 --> 00:01:10,130
 definition of it is. It's possible for a person to, not

16
00:01:10,130 --> 00:01:15,010
 easy, but it's possible for a human being to realize

17
00:01:15,010 --> 00:01:16,000
 freedom from suffering,

18
00:01:16,000 --> 00:01:22,740
 to come to be, even temporarily, to be free from the

19
00:01:22,740 --> 00:01:29,600
 arising of stress, the arising of any impermanent

20
00:01:29,600 --> 00:01:34,000
 phenomenon, that at that point the mind is perfectly free.

21
00:01:34,000 --> 00:01:39,220
 It's possible to enter or to touch that state, to

22
00:01:39,220 --> 00:01:45,150
 experience or to see or to realize that state without being

23
00:01:45,150 --> 00:01:52,000
 free from suffering, without being that way forever.

24
00:01:52,000 --> 00:01:54,900
 This is the first thing to point out. And this person we

25
00:01:54,900 --> 00:01:57,510
 have names for them, the sotapan, someone who has entered

26
00:01:57,510 --> 00:01:58,000
 the stream.

27
00:01:58,000 --> 00:02:02,610
 Because though they still have to suffer and they still

28
00:02:02,610 --> 00:02:10,360
 have more to do, they've already cracked the shell, so to

29
00:02:10,360 --> 00:02:13,300
 speak, and it's only a matter of time before they become

30
00:02:13,300 --> 00:02:15,000
 totally free from suffering.

31
00:02:15,000 --> 00:02:19,480
 So this is one understanding of nirvana. It's an experience

32
00:02:19,480 --> 00:02:21,000
 that someone has.

33
00:02:21,000 --> 00:02:24,660
 And at that point, one's life starts to go in the direction

34
00:02:24,660 --> 00:02:27,000
 towards freedom from suffering.

35
00:02:27,000 --> 00:02:31,670
 But what we mean by nirvana or parinirvana or so on is the

36
00:02:31,670 --> 00:02:35,000
 complete freedom from suffering.

37
00:02:35,000 --> 00:02:39,340
 So though a person might still live, they have no more

38
00:02:39,340 --> 00:02:43,650
 greed, no more anger, no more delusion. These things can't

39
00:02:43,650 --> 00:02:44,000
 arise.

40
00:02:44,000 --> 00:02:48,260
 There's perfect understanding of reality, of the

41
00:02:48,260 --> 00:02:51,000
 experiences around oneself.

42
00:02:51,000 --> 00:02:54,210
 When you see, hear, smell, taste, feel, think, there's only

43
00:02:54,210 --> 00:02:58,000
 the awareness of the object. There's no attachment to it.

44
00:02:58,000 --> 00:03:01,500
 So we say this is nirvana. This comes from realizing this

45
00:03:01,500 --> 00:03:03,000
 state of ultimate freedom.

46
00:03:03,000 --> 00:03:06,230
 When you realize it again and again, you start to see that

47
00:03:06,230 --> 00:03:09,990
 there's nothing that lasts, that all of the experiences

48
00:03:09,990 --> 00:03:13,000
 that we have simply arise and cease.

49
00:03:13,000 --> 00:03:15,690
 And there's nothing lasting, there's nothing permanent,

50
00:03:15,690 --> 00:03:18,230
 there's nothing stable. And so one loses all of one's

51
00:03:18,230 --> 00:03:19,000
 attachment.

52
00:03:19,000 --> 00:03:25,060
 At that point, one isn't subject to future rebirth. There's

53
00:03:25,060 --> 00:03:30,770
 this physical reality that we've somehow built up that has

54
00:03:30,770 --> 00:03:34,000
 to last out its lifetime.

55
00:03:34,000 --> 00:03:39,530
 And at the end of that cycle, that revolution, at death

56
00:03:39,530 --> 00:03:44,000
 there's nothing. So at death there's no more arising.

57
00:03:44,000 --> 00:03:49,830
 And at that point there's freedom from suffering, freedom

58
00:03:49,830 --> 00:03:55,940
 from impermanence, from birth, from having to come back

59
00:03:55,940 --> 00:03:57,000
 again.

60
00:03:57,000 --> 00:04:00,060
 So I think this is clear. The answer from a Buddhist point

61
00:04:00,060 --> 00:04:03,000
 of view, doctrinal point of view, is quite clear.

62
00:04:03,000 --> 00:04:06,090
 After that there's nothing left to do. The Buddha said, "K

63
00:04:06,090 --> 00:04:09,000
attan karuniyam." "Done is what needs to be done."

64
00:04:09,000 --> 00:04:13,000
 There's nothing left to do. There's nothing more here.

65
00:04:13,000 --> 00:04:18,330
 But I think an important point to make that is often missed

66
00:04:18,330 --> 00:04:23,000
 is that you don't somehow fall into this state.

67
00:04:23,000 --> 00:04:31,600
 This is the ultimate, the samam bonam, the final

68
00:04:31,600 --> 00:04:32,970
 emancipation. It comes to someone who has had enough, who

69
00:04:32,970 --> 00:04:34,000
 doesn't see any benefit in coming back.

70
00:04:34,000 --> 00:04:37,950
 It doesn't see any benefit in recreating this human state

71
00:04:37,950 --> 00:04:41,000
 or any state, again and again and again.

72
00:04:41,000 --> 00:04:43,530
 Most of us are not there. Most of us think that there's

73
00:04:43,530 --> 00:04:47,000
 still something to be done. We want this, we want that.

74
00:04:47,000 --> 00:04:50,760
 We've been taught that it's good to have a partner, to have

75
00:04:50,760 --> 00:04:54,000
 a family, to have a job, to help people and so on.

76
00:04:54,000 --> 00:04:57,000
 And we think that there's somehow some benefit in this.

77
00:04:57,000 --> 00:05:00,400
 On the other hand, we see that in ourselves there are

78
00:05:00,400 --> 00:05:03,910
 certain attachments that we have that are causing our

79
00:05:03,910 --> 00:05:05,000
 suffering.

80
00:05:05,000 --> 00:05:08,560
 So this is where we're at. We're at the point where we want

81
00:05:08,560 --> 00:05:10,920
 to hold on to certain things, but we want to let go of

82
00:05:10,920 --> 00:05:12,000
 other certain things.

83
00:05:12,000 --> 00:05:16,280
 So this just shows that we're somewhere along the path, we

84
00:05:16,280 --> 00:05:22,190
're somewhere between a useless person and an enlightened

85
00:05:22,190 --> 00:05:23,000
 person.

86
00:05:23,000 --> 00:05:26,000
 And it's up to us which way we're going to go.

87
00:05:26,000 --> 00:05:29,140
 If we take the side of letting go, we don't have to let go

88
00:05:29,140 --> 00:05:32,000
 of those things we don't want to let go of.

89
00:05:32,000 --> 00:05:34,220
 We let go of those things that we want to let go of. And

90
00:05:34,220 --> 00:05:36,000
 this is how the path works.

91
00:05:36,000 --> 00:05:40,100
 People who are afraid of things like nirvana, afraid of

92
00:05:40,100 --> 00:05:43,490
 letting go, and afraid of practicing meditation, because

93
00:05:43,490 --> 00:05:45,000
 they think if I practice meditation,

94
00:05:45,000 --> 00:05:47,760
 I'm going to let go of all these things I love, and I love

95
00:05:47,760 --> 00:05:50,520
 them, and I want those things, I don't want to let go of

96
00:05:50,520 --> 00:05:51,000
 them.

97
00:05:51,000 --> 00:05:53,680
 And the truth is, if you want it, if it's something you're

98
00:05:53,680 --> 00:05:56,370
 holding on to, you're never going to let it go, that's the

99
00:05:56,370 --> 00:05:57,000
 point.

100
00:05:57,000 --> 00:06:01,120
 But what you have to realize is that the more you

101
00:06:01,120 --> 00:06:05,660
 understand, the more mature you become, the more you

102
00:06:05,660 --> 00:06:09,200
 realize that you're not benefiting anyone or anything by

103
00:06:09,200 --> 00:06:11,000
 yourself or others,

104
00:06:11,000 --> 00:06:15,080
 by holding on to anything, by clinging to others, by

105
00:06:15,080 --> 00:06:18,000
 wanting things to be a certain way.

106
00:06:18,000 --> 00:06:21,020
 It's not of a benefit to you, and it's not of a benefit to

107
00:06:21,020 --> 00:06:24,390
 other people. That true happiness doesn't come from these

108
00:06:24,390 --> 00:06:25,000
 things.

109
00:06:25,000 --> 00:06:28,060
 People, when they hear about not coming back, they think, "

110
00:06:28,060 --> 00:06:30,580
Well, the great thing about Buddhism is that there is coming

111
00:06:30,580 --> 00:06:31,000
 back."

112
00:06:31,000 --> 00:06:36,840
 I remember talking with a Catholic woman, and she said, "Oh

113
00:06:36,840 --> 00:06:40,140
, it's so great to hear about how in Buddhism you've got a

114
00:06:40,140 --> 00:06:41,000
 chance to come back."

115
00:06:41,000 --> 00:06:43,610
 Because, of course, in the religion she had been brought up

116
00:06:43,610 --> 00:06:46,520
 with, that's it. When you die, it's either heaven or hell

117
00:06:46,520 --> 00:06:47,000
 forever.

118
00:06:47,000 --> 00:06:49,450
 But she was thinking, "Wow, you know, to be able to come

119
00:06:49,450 --> 00:06:51,000
 back and have another chance."

120
00:06:51,000 --> 00:06:54,340
 So for Westerners, I think this is common. We think, "Oh,

121
00:06:54,340 --> 00:06:57,360
 it's great. I'll be able to come back and try again like

122
00:06:57,360 --> 00:07:01,000
 that movie Groundhog Day, until you get it right."

123
00:07:01,000 --> 00:07:03,510
 And that's really the point, is until you get it right, you

124
00:07:03,510 --> 00:07:05,000
're going to keep coming back.

125
00:07:05,000 --> 00:07:09,810
 But whether this should be looked at, seen as a good thing

126
00:07:09,810 --> 00:07:12,000
 or not, is debatable.

127
00:07:12,000 --> 00:07:15,010
 I mean, ask yourself, don't ask yourself, "How are you

128
00:07:15,010 --> 00:07:16,000
 feeling now?"

129
00:07:16,000 --> 00:07:18,770
 Because most people think, "Well, I'm okay, and I've got

130
00:07:18,770 --> 00:07:21,980
 plans. If I can just this and this and this, there's a

131
00:07:21,980 --> 00:07:26,540
 chance that I'll be stable and happy and living a good life

132
00:07:26,540 --> 00:07:27,000
."

133
00:07:27,000 --> 00:07:31,640
 But then, don't look at that. Ask yourself what it's taken

134
00:07:31,640 --> 00:07:34,000
 to get even to the point that you're at now.

135
00:07:34,000 --> 00:07:37,000
 And think of all the lessons you've had to learn.

136
00:07:37,000 --> 00:07:39,510
 And if it's true that you come back again and again and

137
00:07:39,510 --> 00:07:43,000
 again, then you're going to have to learn all of those.

138
00:07:43,000 --> 00:07:45,400
 If you don't learn anything now and change anything about

139
00:07:45,400 --> 00:07:49,380
 this state, if you don't somehow increase your level of

140
00:07:49,380 --> 00:07:53,360
 maturity in a way that you've never done before in all your

141
00:07:53,360 --> 00:07:54,000
 lives,

142
00:07:54,000 --> 00:07:57,990
 then you're just going to have to learn these lessons again

143
00:07:57,990 --> 00:07:59,640
 and go through all of the suffering that we had to go

144
00:07:59,640 --> 00:08:04,160
 through as children, as teens, as young adults, as adults,

145
00:08:04,160 --> 00:08:05,000
 and so on.

146
00:08:05,000 --> 00:08:08,810
 And wait until you get old, sick, and die. I mean, see how

147
00:08:08,810 --> 00:08:10,000
 that's going to be.

148
00:08:10,000 --> 00:08:13,500
 And then, don't stop there. Look at the people around you

149
00:08:13,500 --> 00:08:15,000
 and in the world around us.

150
00:08:15,000 --> 00:08:18,580
 Who's to say, "In next life, you won't be one of the people

151
00:08:18,580 --> 00:08:21,000
 who suffer terribly in this life?"

152
00:08:21,000 --> 00:08:27,000
 So, it's not as simple a thing as some people might think.

153
00:08:27,000 --> 00:08:28,660
 They think, "Oh, great. I'll come back. I'll get to do this

154
00:08:28,660 --> 00:08:31,000
 again, and I'll do this differently, and I'll learn more."

155
00:08:31,000 --> 00:08:35,380
 So, no. It's not like that. If you're not careful, some lif

156
00:08:35,380 --> 00:08:39,350
etimes down the road, it's very possible that you'll end up

157
00:08:39,350 --> 00:08:41,000
 in a terrible situation,

158
00:08:41,000 --> 00:08:44,430
 a situation of intense suffering. Why do people suffer in

159
00:08:44,430 --> 00:08:48,600
 such horrible ways that they do? And who's to say, "You won

160
00:08:48,600 --> 00:08:50,000
't end up like them?"

161
00:08:50,000 --> 00:08:55,890
 So, there is some, we can see some benefit in at least

162
00:08:55,890 --> 00:09:01,000
 bringing ourselves somewhat out of this state,

163
00:09:01,000 --> 00:09:04,610
 to a state where we don't lose all of our memories every 50

164
00:09:04,610 --> 00:09:09,020
 to 100 years, where we're able to keep them and to continue

165
00:09:09,020 --> 00:09:10,000
 developing.

166
00:09:10,000 --> 00:09:16,770
 That's, at the very least, a good thing to do. And so, the

167
00:09:16,770 --> 00:09:20,000
 development of the mind shouldn't scare people.

168
00:09:20,000 --> 00:09:22,670
 The practice of Buddhism shouldn't be a scary thing that

169
00:09:22,670 --> 00:09:25,000
 you're going to let go of things you hold on to.

170
00:09:25,000 --> 00:09:29,160
 Buddhism is about learning. It's about coming to grow, is

171
00:09:29,160 --> 00:09:32,580
 growing up and coming to learn things and understand more

172
00:09:32,580 --> 00:09:34,000
 about reality.

173
00:09:34,000 --> 00:09:38,230
 It's not to brainwash us or to force some kind of detached

174
00:09:38,230 --> 00:09:42,990
 state. It's to help us to learn what's causing us suffering

175
00:09:42,990 --> 00:09:43,000
.

176
00:09:43,000 --> 00:09:48,130
 Because all of us can tell if we're not terribly blind, we

177
00:09:48,130 --> 00:09:52,000
 can verify that there is a lot of suffering in this world.

178
00:09:52,000 --> 00:09:55,670
 And the truth of the Buddha's teaching is that this

179
00:09:55,670 --> 00:09:59,680
 suffering is unnecessary. We don't have to suffer in this

180
00:09:59,680 --> 00:10:00,000
 way.

181
00:10:00,000 --> 00:10:02,470
 The reason we do is because we don't understand, and

182
00:10:02,470 --> 00:10:05,210
 because we don't understand, we therefore do things that

183
00:10:05,210 --> 00:10:09,000
 cause us suffering and that cause other people suffering.

184
00:10:09,000 --> 00:10:12,010
 So, slowly we learn how to overcome those things, how to be

185
00:10:12,010 --> 00:10:13,000
 free from them.

186
00:10:13,000 --> 00:10:17,000
 And eventually we learn how to be free from all suffering,

187
00:10:17,000 --> 00:10:21,000
 how to never cause suffering for others or for ourselves,

188
00:10:21,000 --> 00:10:25,170
 how to act in such a way that is not contradictory to our

189
00:10:25,170 --> 00:10:28,960
 wishes, where we wish to be happy, we wish to live in peace

190
00:10:28,960 --> 00:10:29,000
,

191
00:10:29,000 --> 00:10:32,880
 and yet we find ourselves acting and speaking and thinking

192
00:10:32,880 --> 00:10:37,320
 in ways that cause us suffering and cause disharmony in the

193
00:10:37,320 --> 00:10:39,000
 world around us.

194
00:10:39,000 --> 00:10:43,440
 So this is the, I think, sort of a detailed answer of why

195
00:10:43,440 --> 00:10:46,000
 freedom would be the last thing.

196
00:10:46,000 --> 00:10:50,380
 Because once you have no clinging, once you have no

197
00:10:50,380 --> 00:10:54,380
 attachment and no delusion, you will not cause any

198
00:10:54,380 --> 00:10:56,000
 suffering for yourself.

199
00:10:56,000 --> 00:11:04,170
 And as a result, you will not have to come back and there's

200
00:11:04,170 --> 00:11:13,000
 no more creating, no more forming, no more building.

201
00:11:13,000 --> 00:11:16,990
 You realize that there's nothing worth clinging to, that

202
00:11:16,990 --> 00:11:21,000
 true happiness doesn't come from the objects of the sense

203
00:11:21,000 --> 00:11:25,030
 or any of the objects that are inside of us or in the world

204
00:11:25,030 --> 00:11:26,000
 around us.

205
00:11:26,000 --> 00:11:28,840
 True happiness comes from freedom and you come to see that

206
00:11:28,840 --> 00:11:31,000
 this freedom is the ultimate happiness.

207
00:11:31,000 --> 00:11:34,000
 So, that's the answer to that question.

208
00:11:34,000 --> 00:11:37,200
 The second question, I'm going to have to deal with it here

209
00:11:37,200 --> 00:11:40,000
 so this video is going to be a little bit longer.

210
00:11:40,000 --> 00:11:48,130
 The second question is whether karma has physical results

211
00:11:48,130 --> 00:11:53,000
 only or affects the mind as well.

212
00:11:53,000 --> 00:11:56,730
 And, you know, these questions are actually fairly easy

213
00:11:56,730 --> 00:12:00,000
 because they're just simple doctrinal questions.

214
00:12:00,000 --> 00:12:02,000
 But they're interesting as well.

215
00:12:02,000 --> 00:12:05,350
 Karma, first we have to understand that karma is not a

216
00:12:05,350 --> 00:12:06,000
 thing.

217
00:12:06,000 --> 00:12:10,000
 It doesn't exist in the world. You can't show me karma.

218
00:12:10,000 --> 00:12:12,000
 Karma means action.

219
00:12:12,000 --> 00:12:14,490
 So, technically speaking, you can show me an action when,

220
00:12:14,490 --> 00:12:17,000
 you know, show me the karma of killing.

221
00:12:17,000 --> 00:12:20,000
 So, you can kill something. There's karma for you.

222
00:12:20,000 --> 00:12:23,370
 But the problem is that we come to take karma as this

223
00:12:23,370 --> 00:12:26,780
 substance that exists, like something you're carrying

224
00:12:26,780 --> 00:12:29,820
 around like a weight over your shoulder or that's somehow

225
00:12:29,820 --> 00:12:32,000
 tattooed on your skin or something like that.

226
00:12:32,000 --> 00:12:35,630
 That you carry around and you're just waiting for it to

227
00:12:35,630 --> 00:12:38,000
 drop on your head or something.

228
00:12:38,000 --> 00:12:42,000
 And that's not the case. Karma is a law.

229
00:12:42,000 --> 00:12:51,330
 It's a law that says that there is an effect to our actions

230
00:12:51,330 --> 00:12:52,000
.

231
00:12:52,000 --> 00:12:56,440
 And not just our actions, actually to our intentions, to

232
00:12:56,440 --> 00:12:58,000
 our state of mind.

233
00:12:58,000 --> 00:13:04,000
 That the mind is able to affect the world around us.

234
00:13:04,000 --> 00:13:06,000
 It's not a doctrine of determinism.

235
00:13:06,000 --> 00:13:09,000
 You know, if it were this substance or this thing that you

236
00:13:09,000 --> 00:13:12,000
 could measure, then there might be some determinism.

237
00:13:12,000 --> 00:13:14,940
 But it doesn't say that. Karma just says that it doesn't

238
00:13:14,940 --> 00:13:20,000
 say either way that there is a free will or determinism.

239
00:13:20,000 --> 00:13:23,370
 That's not the point of karma. Karma says that when there

240
00:13:23,370 --> 00:13:27,000
 is action, when you do kill or steal or lie.

241
00:13:27,000 --> 00:13:33,050
 When in your mind you develop these unwholesome tendencies

242
00:13:33,050 --> 00:13:37,990
 to these ideas for the creation of disharmony and suffering

243
00:13:37,990 --> 00:13:41,000
 and stress.

244
00:13:41,000 --> 00:13:46,280
 Then there isn't the effect. The effect is that stress is

245
00:13:46,280 --> 00:13:47,000
 created.

246
00:13:47,000 --> 00:13:50,570
 When you have the intention to help people as well. There's

247
00:13:50,570 --> 00:13:53,000
 another sort of stress created.

248
00:13:53,000 --> 00:13:57,060
 It's not an unpleasant stress, but it's a stress of sorts

249
00:13:57,060 --> 00:13:59,000
 that creates pleasure.

250
00:13:59,000 --> 00:14:03,920
 It's a vibration. It's like a vibration when you want to

251
00:14:03,920 --> 00:14:05,000
 help people.

252
00:14:05,000 --> 00:14:09,100
 When you have the intention to do good things. To give, to

253
00:14:09,100 --> 00:14:16,000
 help, to support, to teach, to even to just be kind.

254
00:14:16,000 --> 00:14:19,450
 When you study, when you learn, when you practice

255
00:14:19,450 --> 00:14:23,000
 meditation, all of these things have an effect.

256
00:14:23,000 --> 00:14:31,770
 They change who we are. It's important to understand that

257
00:14:31,770 --> 00:14:35,000
 aspect of karma.

258
00:14:35,000 --> 00:14:40,400
 The short answer to your question then is that karma, our

259
00:14:40,400 --> 00:14:45,000
 actions will affect both the body and the mind.

260
00:14:45,000 --> 00:14:52,000
 Technically speaking, karma gives rise to experiences.

261
00:14:52,000 --> 00:14:54,780
 It gives rise to seeing, hearing, smelling, tasting,

262
00:14:54,780 --> 00:14:56,000
 feeling, thinking.

263
00:14:56,000 --> 00:15:00,670
 This is the technical explanation that it's going to give

264
00:15:00,670 --> 00:15:04,000
 rise to sensual experience in the future.

265
00:15:04,000 --> 00:15:07,370
 If you do something and you intend to do something, that's

266
00:15:07,370 --> 00:15:10,000
 going to change the experiences that you have.

267
00:15:10,000 --> 00:15:12,540
 You're not going to see the same things as if you hadn't

268
00:15:12,540 --> 00:15:13,000
 done that.

269
00:15:13,000 --> 00:15:16,020
 If you kill someone, they're going to see the inside of a

270
00:15:16,020 --> 00:15:17,000
 jail and so on.

271
00:15:17,000 --> 00:15:20,000
 Hearing, smelling, tasting, feeling, thinking.

272
00:15:20,000 --> 00:15:23,000
 From a technical point of view, that's what happens.

273
00:15:23,000 --> 00:15:25,000
 There are different experiences that you have.

274
00:15:25,000 --> 00:15:28,000
 Those experiences are both physical and mental.

275
00:15:28,000 --> 00:15:36,000
 All it means is that we are affecting our destiny.

276
00:15:36,000 --> 00:15:39,440
 Whether this is based on free will or determinism isn't

277
00:15:39,440 --> 00:15:41,000
 really the question.

278
00:15:41,000 --> 00:15:45,310
 The point is that when we do do something, there is a

279
00:15:45,310 --> 00:15:46,000
 result.

280
00:15:46,000 --> 00:15:53,850
 So rather than worrying about such questions as whether it

281
00:15:53,850 --> 00:15:56,000
's free will or determinism

282
00:15:56,000 --> 00:15:59,540
 or what does karma affect, is it a physical or a mental

283
00:15:59,540 --> 00:16:03,000
 thing, we should train ourselves

284
00:16:03,000 --> 00:16:08,410
 to give up the unwholesome tendencies that cause us

285
00:16:08,410 --> 00:16:10,000
 suffering.

286
00:16:10,000 --> 00:16:12,370
 In the end, the interesting thing is that you give up both

287
00:16:12,370 --> 00:16:13,000
 kinds of karma

288
00:16:13,000 --> 00:16:16,000
 because you have no intention to create happiness either,

289
00:16:16,000 --> 00:16:20,000
 to create happiness in the sense of happy experiences.

290
00:16:20,000 --> 00:16:23,340
 You come to see that even pleasurable experiences are

291
00:16:23,340 --> 00:16:24,000
 temporary.

292
00:16:24,000 --> 00:16:26,800
 Even if you create harmony in the world, even if I were

293
00:16:26,800 --> 00:16:29,000
 able to, with my great karma,

294
00:16:29,000 --> 00:16:33,340
 create peace and harmony in the world, with through some

295
00:16:33,340 --> 00:16:37,000
 great act, a minor series of acts, it's temporary.

296
00:16:37,000 --> 00:16:40,110
 And unless there's wisdom and understanding that allows

297
00:16:40,110 --> 00:16:41,000
 people to let go,

298
00:16:41,000 --> 00:16:44,000
 they're just going to ruin it when I'm gone.

299
00:16:44,000 --> 00:16:48,440
 For instance, the Buddha's teaching, when the Buddha was

300
00:16:48,440 --> 00:16:52,000
 around, there was a lot of good in India.

301
00:16:52,000 --> 00:16:56,670
 It lasted for some time and then India went back and now it

302
00:16:56,670 --> 00:16:59,000
's an ordinary country again.

303
00:16:59,000 --> 00:17:02,000
 But for some time in India, it was a special place.

304
00:17:02,000 --> 00:17:06,550
 It was very Buddhist and there was very little killing and

305
00:17:06,550 --> 00:17:10,000
 so on, from what we understand.

306
00:17:10,000 --> 00:17:13,000
 Just an example, but the point is that it's impermanent.

307
00:17:13,000 --> 00:17:15,420
 And no matter what good things you do, even the Buddha's

308
00:17:15,420 --> 00:17:16,000
 teaching is impermanent.

309
00:17:16,000 --> 00:17:18,000
 It's not going to last forever.

310
00:17:18,000 --> 00:17:20,930
 And this is the teaching of a perfectly enlightened Buddha,

311
00:17:20,930 --> 00:17:22,000
 as we understand.

312
00:17:22,000 --> 00:17:28,480
 So, most important is to become free and to become free

313
00:17:28,480 --> 00:17:30,000
 from karma as well,

314
00:17:30,000 --> 00:17:35,260
 and to not have any attachments that things should turn out

315
00:17:35,260 --> 00:17:36,000
 in a certain way.

316
00:17:36,000 --> 00:17:40,000
 Not to be expecting or have expectations about the future,

317
00:17:40,000 --> 00:17:47,430
 to simply be content and in tune with reality, to see

318
00:17:47,430 --> 00:17:49,000
 reality for what it is,

319
00:17:49,000 --> 00:17:53,510
 and to be at peace with that, and to not cling and to not

320
00:17:53,510 --> 00:17:58,000
 want and to not hope and care and worry and so on.

321
00:17:58,000 --> 00:18:03,570
 But to live one's life in a dynamic way where you can

322
00:18:03,570 --> 00:18:08,870
 accept and react and respond appropriately to every

323
00:18:08,870 --> 00:18:10,000
 situation.

324
00:18:10,000 --> 00:18:14,000
 It doesn't mean that you live in one place and do nothing,

325
00:18:14,000 --> 00:18:15,000
 sit around and do nothing.

326
00:18:15,000 --> 00:18:18,000
 It means that you are able to live dynamically.

327
00:18:18,000 --> 00:18:20,000
 You're not a stick in the mud.

328
00:18:20,000 --> 00:18:23,000
 And when it's time to move, can't move.

329
00:18:23,000 --> 00:18:28,480
 Your mind is able to respond appropriately to all

330
00:18:28,480 --> 00:18:35,000
 experiences and react without attachment.

331
00:18:35,000 --> 00:18:40,000
 So, fairly detailed answers to your questions.

332
00:18:40,000 --> 00:18:44,000
 I hope they did hit the mark, at least to some extent.

333
00:18:44,000 --> 00:18:47,000
 So, thanks for the questions. All the best.

