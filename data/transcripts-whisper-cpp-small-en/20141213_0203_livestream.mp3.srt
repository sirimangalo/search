1
00:00:00,000 --> 00:00:12,050
 Okay, good morning. Broadcasting from Bangkok. Actually, I

2
00:00:12,050 --> 00:00:14,960
 crossed the river from Bangkok

3
00:00:14,960 --> 00:00:21,660
 in one of those delightful, one of these delightful hidden

4
00:00:21,660 --> 00:00:25,440
 sanctuaries that really Bangkok is

5
00:00:25,440 --> 00:00:31,310
 full of, isn't famous for but should be. Bangkok's a really

6
00:00:31,310 --> 00:00:37,680
 interesting city describing where I am.

7
00:00:37,680 --> 00:00:49,040
 I'm in an area called Bangke, which is highly urbanized.

8
00:00:50,800 --> 00:00:57,240
 And so the area I'm in is close to the mall it's called, or

9
00:00:57,240 --> 00:01:00,000
 Thai people say the mall,

10
00:01:00,000 --> 00:01:07,840
 which is a huge, well, a fairly big shopping complex.

11
00:01:07,840 --> 00:01:13,450
 It's, I think, one of the, it's the well-known sort of

12
00:01:13,450 --> 00:01:15,920
 landmark just down the road from where I am.

13
00:01:16,880 --> 00:01:21,720
 But as you come closer this way, you get to, there's the

14
00:01:21,720 --> 00:01:24,320
 market where I go in alms round and then

15
00:01:24,320 --> 00:01:31,080
 coming even closer is this huge, they call Tesco Lotus. It

16
00:01:31,080 --> 00:01:33,040
's a, Tesco is this

17
00:01:33,840 --> 00:01:42,220
 UK brand and they have a huge supermarket, which I guess is

18
00:01:42,220 --> 00:01:47,600
, I'm not sure exactly. I guess it's got

19
00:01:47,600 --> 00:01:52,660
 groceries, no food, but also got everything you could

20
00:01:52,660 --> 00:01:55,840
 possibly need in it. So basically,

21
00:01:56,640 --> 00:02:01,590
 everything for daily living and then some is in this one

22
00:02:01,590 --> 00:02:05,840
 store. So it's big and it's all over Thailand.

23
00:02:05,840 --> 00:02:15,580
 But I'm about 50 meters from it. So you'd think it would be

24
00:02:15,580 --> 00:02:18,320
 the, I don't know, 50 meters, I can't

25
00:02:18,320 --> 00:02:24,600
 judge, but no more than 100 meters. So you'd think that

26
00:02:24,600 --> 00:02:27,680
 being the case, this would be a pretty awful

27
00:02:27,680 --> 00:02:34,650
 place to live and reside. But I'm looking out my window at

28
00:02:34,650 --> 00:02:40,320
 a Sala tree about two feet thick

29
00:02:41,360 --> 00:02:47,430
 with big Sala fruit and watching the guy pick Sala flowers

30
00:02:47,430 --> 00:02:49,200
 off the tree.

31
00:02:49,200 --> 00:02:53,990
 Sala, the Sala tree is the tree that the Buddha passed away

32
00:02:53,990 --> 00:02:56,240
 under and was actually born under as

33
00:02:56,240 --> 00:03:01,780
 well. So they say, then if I turn and look, I have, there's

34
00:03:01,780 --> 00:03:04,960
 a little bird on the lawn by the pond.

35
00:03:05,840 --> 00:03:12,830
 My kuti is actually on top of this goldfish pond. They've

36
00:03:12,830 --> 00:03:16,400
 built it actually, well, half over top of

37
00:03:16,400 --> 00:03:20,940
 it. And you can hear the birds singing and I don't see,

38
00:03:20,940 --> 00:03:24,480
 barely see the building off in the distance

39
00:03:24,480 --> 00:03:30,450
 among the trees. When there was a flooding a few years back

40
00:03:30,450 --> 00:03:31,440
, it was actually

41
00:03:32,400 --> 00:03:35,330
 much before the flooding, it was actually much more lush.

42
00:03:35,330 --> 00:03:36,960
 This is quite lush as it is, but

43
00:03:36,960 --> 00:03:40,960
 many of the trees died from the extended flooding. They

44
00:03:40,960 --> 00:03:43,040
 just couldn't handle the trees that were here,

45
00:03:43,040 --> 00:03:50,050
 couldn't handle the water. And so what I mean to say is

46
00:03:50,050 --> 00:03:55,120
 this is somehow right behind the Lotus

47
00:03:55,120 --> 00:04:00,670
 Shopping Mall. There's this wonderful little refuge that

48
00:04:00,670 --> 00:04:03,120
 you can hear the people off in the

49
00:04:03,120 --> 00:04:07,190
 background, but that's just the people who live here and

50
00:04:07,190 --> 00:04:08,560
 run this place.

51
00:04:08,560 --> 00:04:14,030
 It's, they're great people. Tomorrow we'll be having the

52
00:04:14,030 --> 00:04:14,880
 monthly

53
00:04:14,880 --> 00:04:21,170
 almsgiving. I just happened to make it for that. So there

54
00:04:21,170 --> 00:04:23,360
 last night the owner of the place, a

55
00:04:24,320 --> 00:04:30,640
 judge came and invited me to give the talk for tomorrow's

56
00:04:30,640 --> 00:04:34,160
 almsgiving. I'm not sure if that's going

57
00:04:34,160 --> 00:04:36,820
 to go through because there was already a monk who was

58
00:04:36,820 --> 00:04:39,920
 scheduled to give the talk. But she said,

59
00:04:39,920 --> 00:04:43,710
 because I'm here she wants me to give it. No, just because

60
00:04:43,710 --> 00:04:46,800
 it's a special case. So we'll see.

61
00:04:48,960 --> 00:04:53,460
 It's the thing about being a monastic teacher. You have to

62
00:04:53,460 --> 00:04:56,080
 be ready to give a talk on a moment's

63
00:04:56,080 --> 00:05:00,420
 notice. People spring these things on you. When I was at J

64
00:05:00,420 --> 00:05:02,640
om Tong last year, my teacher just

65
00:05:02,640 --> 00:05:08,130
 a few hours before they were to give this weekly talk to

66
00:05:08,130 --> 00:05:11,360
 like a hundred meditators at the center.

67
00:05:11,360 --> 00:05:15,680
 He just says, "Okay, I want you to give the talk tonight."

68
00:05:18,400 --> 00:05:21,520
 But it's like that. Anyway, so this is where I am.

69
00:05:21,520 --> 00:05:30,350
 Trying to give Dhamma every day. I feel like I shouldn't

70
00:05:30,350 --> 00:05:32,880
 give lots and lots of Dhamma.

71
00:05:32,880 --> 00:05:36,650
 Otherwise I'll run those things to say or it just gets too

72
00:05:36,650 --> 00:05:40,080
 much. You know, people will wind up

73
00:05:40,080 --> 00:05:43,860
 having to process lots of information. So I was trying to

74
00:05:43,860 --> 00:05:46,080
 give like five minutes a day, but

75
00:05:48,080 --> 00:05:51,090
 it's hard to know what's enough and what's too much. What I

76
00:05:51,090 --> 00:05:54,880
 was thinking about talking about today was

77
00:05:54,880 --> 00:05:59,930
 another aspect of the Four Foundations of Mindfulness. Why

78
00:05:59,930 --> 00:06:02,480
 there are four. So the other

79
00:06:02,480 --> 00:06:05,090
 thing that the commentary says about why there are four

80
00:06:05,090 --> 00:06:07,040
 foundations of mindfulness is because

81
00:06:09,040 --> 00:06:19,600
 they correspond nicely with the four perversions. That's

82
00:06:19,600 --> 00:06:21,200
 probably not the best word for

83
00:06:21,200 --> 00:06:28,960
 the concept. "Vipalasa." "Vip" means in this case

84
00:06:32,240 --> 00:06:40,240
 "outside" or "transformed" in the sense of perverted. But

85
00:06:40,240 --> 00:06:42,720
 perversion in English has a

86
00:06:42,720 --> 00:06:46,960
 bad connotation. We're not talking about child molesters or

87
00:06:46,960 --> 00:06:48,960
 or voyeurs or this kind of thing.

88
00:06:48,960 --> 00:06:55,360
 Sexual deviant, sexual deviant, sexual people who have

89
00:06:57,680 --> 00:07:00,910
 convoluted sexual preferences. This is how we look at the

90
00:07:00,910 --> 00:07:02,960
 word perversion. But we mean it in a rather

91
00:07:02,960 --> 00:07:07,770
 scientific sense. Perverted in the sense of twisted. You

92
00:07:07,770 --> 00:07:14,880
 know, "out of line with reality"

93
00:07:14,880 --> 00:07:18,640
 is actually what we mean. So misconception might be a

94
00:07:18,640 --> 00:07:22,880
 better word. Perversion just has too many

95
00:07:22,880 --> 00:07:28,420
 bad connotations. So the four misconceptions or misconceiv

96
00:07:28,420 --> 00:07:30,400
ings. Misunderstandings.

97
00:07:30,400 --> 00:07:36,430
 And these four are the misconceptions in regards to things

98
00:07:36,430 --> 00:07:39,600
 that are not beautiful, that they are

99
00:07:39,600 --> 00:07:45,180
 beautiful. The misconception in regards to things that are

100
00:07:45,180 --> 00:07:48,720
 not pleasant, that they are pleasant.

101
00:07:50,480 --> 00:07:55,250
 And the misconception that those things that are not

102
00:07:55,250 --> 00:07:56,800
 permanent are permanent.

103
00:07:56,800 --> 00:08:02,840
 And finally the misconception that those things that are

104
00:08:02,840 --> 00:08:05,680
 not self are self.

105
00:08:05,680 --> 00:08:11,930
 We have these four misconceptions or perversions of

106
00:08:11,930 --> 00:08:13,360
 perception or

107
00:08:15,680 --> 00:08:20,330
 mistaken perceptions. And they're not just perceptions.

108
00:08:20,330 --> 00:08:22,640
 There are actually three levels of

109
00:08:22,640 --> 00:08:27,810
 misconception. I think perversion works better because it's

110
00:08:27,810 --> 00:08:29,280
 more general. Because we're not

111
00:08:29,280 --> 00:08:34,340
 talking about misconception or perception. We're instead

112
00:08:34,340 --> 00:08:38,640
 talking about three levels of perverted

113
00:08:41,760 --> 00:08:45,400
 perversion. I wish there was a better word because it's...

114
00:08:45,400 --> 00:08:48,320
 But if you understand this not to be like a

115
00:08:48,320 --> 00:08:54,290
 really... not to be based in the way we understand the word

116
00:08:54,290 --> 00:09:00,160
 pervert or perversion, it's twisted.

117
00:09:00,160 --> 00:09:09,270
 That's the problem with Buddhism. In many ways it's very

118
00:09:09,270 --> 00:09:11,360
 much outside of our daily discourse.

119
00:09:12,800 --> 00:09:17,200
 The whole path and many of the concepts are very much

120
00:09:17,200 --> 00:09:22,160
 contrary to the way we live our lives. And

121
00:09:22,160 --> 00:09:25,590
 as I said, the way we look at the world is actually very,

122
00:09:25,590 --> 00:09:28,000
 very wrong according to Buddhism.

123
00:09:28,000 --> 00:09:33,670
 Our whole paradigm is wrong. So many of the words have to

124
00:09:33,670 --> 00:09:35,920
 be made fresh. And

125
00:09:38,320 --> 00:09:40,770
 as a result, you wind up... English is good for that

126
00:09:40,770 --> 00:09:42,160
 because you can play with words.

127
00:09:42,160 --> 00:09:48,740
 Like you can turn something that's normally a noun into a

128
00:09:48,740 --> 00:09:50,720
 verb or a verb into a noun and that

129
00:09:50,720 --> 00:09:53,660
 kind of thing. And this is what the Buddha did with Pali as

130
00:09:53,660 --> 00:09:55,280
 well. This is what was done with

131
00:09:55,280 --> 00:10:00,490
 Pali, is the ability to put words together and make up

132
00:10:00,490 --> 00:10:03,680
 words like pervertedness or something

133
00:10:03,680 --> 00:10:08,910
 like that. Finding new words and using old words in new

134
00:10:08,910 --> 00:10:11,280
 ways. They have to be used in new ways and

135
00:10:11,280 --> 00:10:15,930
 they have to be defined in new ways. They have to be

136
00:10:15,930 --> 00:10:19,840
 brought back to their roots many times.

137
00:10:19,840 --> 00:10:23,770
 Communication is important. This is why the Buddha called

138
00:10:23,770 --> 00:10:27,280
 it actually language. Language is

139
00:10:28,800 --> 00:10:32,120
 a mastery. The ability to master languages is an important

140
00:10:32,120 --> 00:10:32,720
 skill.

141
00:10:32,720 --> 00:10:40,600
 So let's stick with perversion for now, understanding that

142
00:10:40,600 --> 00:10:41,120
 it's not

143
00:10:41,120 --> 00:10:46,670
 this kinky. It's not meant in a kinky sense. Although even

144
00:10:46,670 --> 00:10:48,640
 the word kink is a good word

145
00:10:48,640 --> 00:10:53,280
 because it suggests something not quite straight. Crooked.

146
00:10:57,920 --> 00:11:01,920
 But so the point being there are three levels of perversion

147
00:11:01,920 --> 00:11:04,640
. There's the perversion of

148
00:11:04,640 --> 00:11:12,800
 perception. This refers just to the level of experience.

149
00:11:12,800 --> 00:11:18,990
 So in regards to beauty, you perceive that something ugly

150
00:11:18,990 --> 00:11:19,760
 is beautiful.

151
00:11:21,200 --> 00:11:26,390
 Or just the feeling comes in the mind. A sense of something

152
00:11:26,390 --> 00:11:28,880
 being beautiful. The second level

153
00:11:28,880 --> 00:11:33,770
 is the perversion of thought. That in regards to something

154
00:11:33,770 --> 00:11:35,680
 ugly, once the perception arises,

155
00:11:35,680 --> 00:11:41,800
 one thinks, "Boy, that's beautiful. That thing is quite

156
00:11:41,800 --> 00:11:44,080
 desirable."

157
00:11:47,120 --> 00:11:52,320
 And the third one, third level is the level of view.

158
00:11:52,320 --> 00:11:57,130
 So once one thinks that something's beautiful, then one

159
00:11:57,130 --> 00:12:00,240
 acquires the view that this is beautiful. Yes,

160
00:12:00,240 --> 00:12:08,560
 indeed my thought is correct. That is beautiful. That thing

161
00:12:08,560 --> 00:12:10,640
 is indeed beautiful.

162
00:12:11,280 --> 00:12:16,330
 It's the affirmation of the thought. And it's a distinct

163
00:12:16,330 --> 00:12:17,200
 level. These three levels,

164
00:12:17,200 --> 00:12:20,280
 an understanding of the distinction is quite useful.

165
00:12:20,280 --> 00:12:24,000
 Because a sottapanen, take for example

166
00:12:24,000 --> 00:12:27,570
 a sottapanen, doesn't have the view that something is, for

167
00:12:27,570 --> 00:12:29,120
 example, beautiful.

168
00:12:29,120 --> 00:12:35,170
 It doesn't take things to be beautiful or doesn't believe

169
00:12:35,170 --> 00:12:36,720
 things to be beautiful. But

170
00:12:38,400 --> 00:12:42,450
 a sottapanen still has the potential to think that things

171
00:12:42,450 --> 00:12:44,240
 are beautiful. In other words,

172
00:12:44,240 --> 00:12:47,440
 the thought might arise. Not think, but not think in the

173
00:12:47,440 --> 00:12:49,920
 sense of believe, but think in the sense of

174
00:12:49,920 --> 00:12:53,470
 the thought can still arise for a sottapanen. That's

175
00:12:53,470 --> 00:12:57,600
 beautiful. Once that thought arises,

176
00:12:57,600 --> 00:13:03,460
 the sottapanen will immediately, or not necessarily

177
00:13:03,460 --> 00:13:06,800
 immediately, but will not ever

178
00:13:06,800 --> 00:13:09,820
 give rise to the belief that that thought is right. And

179
00:13:09,820 --> 00:13:11,120
 generally they will immediately

180
00:13:11,120 --> 00:13:16,270
 respond to that thought saying, boy, that's a silly thought

181
00:13:16,270 --> 00:13:17,600
, or with mindfulness,

182
00:13:17,600 --> 00:13:23,690
 recognize that that thought is coming from their old habits

183
00:13:23,690 --> 00:13:25,760
 of attachment and addiction and so on.

184
00:13:25,760 --> 00:13:28,800
 And of course they'll still have the perception.

185
00:13:31,040 --> 00:13:34,570
 The perceptions will still arise at the most base level

186
00:13:34,570 --> 00:13:36,800
 that something is beautiful. The

187
00:13:36,800 --> 00:13:41,200
 attraction of things will still exist, can still arise in

188
00:13:41,200 --> 00:13:43,040
 the sottapanen, for example.

189
00:13:43,040 --> 00:13:48,790
 So these levels are levels of perversion, really. The views

190
00:13:48,790 --> 00:13:50,640
 are the worst, the most perverse.

191
00:13:50,640 --> 00:13:56,150
 And perverse in the sense that they go against reality. And

192
00:13:56,150 --> 00:13:59,920
 the view is the worst because it's

193
00:13:59,920 --> 00:14:03,690
 the strongest. It's the one that's going to inform one's

194
00:14:03,690 --> 00:14:06,720
 actions the most. One has the view that

195
00:14:06,720 --> 00:14:09,620
 certain things are beautiful, certain things are pleasant,

196
00:14:09,620 --> 00:14:13,280
 certain things are permanent or stable,

197
00:14:13,280 --> 00:14:20,110
 and certain things are controllable or self. The view is

198
00:14:20,110 --> 00:14:23,280
 going to inform one's actions much more

199
00:14:23,280 --> 00:14:28,130
 than thoughts or perceptions, although those as well will

200
00:14:28,130 --> 00:14:31,760
 influence not to the same degree as one's

201
00:14:31,760 --> 00:14:36,100
 views. So these three levels, it's also useful to help us

202
00:14:36,100 --> 00:14:39,120
 discriminate between what we can,

203
00:14:39,120 --> 00:14:43,010
 what we especially as beginner meditators can influence. We

204
00:14:43,010 --> 00:14:46,240
 may not be able to influence our

205
00:14:46,240 --> 00:14:51,320
 perceptions as easily, but we can quite easily influence

206
00:14:51,320 --> 00:14:54,080
 our views. And this can change quite

207
00:14:54,080 --> 00:14:58,350
 easily, relatively speaking, compared to one's perceptions.

208
00:14:58,350 --> 00:15:00,080
 A meditator might find that for

209
00:15:00,080 --> 00:15:04,810
 years or even for lifetimes they're still plagued by these

210
00:15:04,810 --> 00:15:08,320
 perceptions and even thoughts that go

211
00:15:08,320 --> 00:15:11,920
 against reality. But quite quickly in the practice they

212
00:15:11,920 --> 00:15:14,320
 should be able to give up their views,

213
00:15:15,280 --> 00:15:18,160
 the views that these thoughts are right, and just seeing

214
00:15:18,160 --> 00:15:21,840
 these thoughts as habitual, as remnants of

215
00:15:21,840 --> 00:15:27,410
 old habits of one's old view. So those are the three levels

216
00:15:27,410 --> 00:15:30,560
. This is quite a, one of, I think,

217
00:15:30,560 --> 00:15:35,180
 one of the great important teachings that we have of the

218
00:15:35,180 --> 00:15:39,280
 Buddha, these four perversions and the three

219
00:15:40,240 --> 00:15:42,720
 levels of perversion, something that all Buddhist medit

220
00:15:42,720 --> 00:15:46,800
ators should know. So just briefly,

221
00:15:46,800 --> 00:15:49,450
 what do we mean by them and why do they correspond with the

222
00:15:49,450 --> 00:15:51,360
 four foundations? It's because beauty

223
00:15:51,360 --> 00:15:57,050
 and it's very much tied in with the body. So when you're

224
00:15:57,050 --> 00:15:58,720
 mindful of the body, it helps you get rid of

225
00:15:58,720 --> 00:16:02,310
 the perversion of beauty, seeing that the body is not

226
00:16:02,310 --> 00:16:05,760
 actually this beautiful thing. And objectively,

227
00:16:05,760 --> 00:16:09,340
 the body is quite disgusting, really. It's amazing that

228
00:16:09,340 --> 00:16:11,200
 human beings are able to find the

229
00:16:11,200 --> 00:16:18,510
 body beautiful. There are certain things that are in a

230
00:16:18,510 --> 00:16:21,600
 gross sense,

231
00:16:21,600 --> 00:16:31,440
 geometrically sound, I suppose, or simple or

232
00:16:35,520 --> 00:16:43,530
 so in the sense, the face can often be quite complementary.

233
00:16:43,530 --> 00:16:46,000
 The aspects of the face or

234
00:16:46,000 --> 00:16:54,730
 there's not much else. I mean, the rest is the shape of the

235
00:16:54,730 --> 00:16:57,120
 body is really just arbitrary.

236
00:16:57,120 --> 00:17:00,340
 And our attraction to it is completely arbitrary. It's

237
00:17:00,340 --> 00:17:03,200
 based on lifetime after lifetime of built up

238
00:17:03,200 --> 00:17:06,620
 habitual recognition as being something that brings

239
00:17:06,620 --> 00:17:12,080
 pleasure. It's habitual belief that this

240
00:17:12,080 --> 00:17:15,850
 is going to make me happy, the attraction to this, the

241
00:17:15,850 --> 00:17:19,120
 association with it, the contact with it.

242
00:17:19,120 --> 00:17:25,360
 But much of the body is actually disgusting, like the smell

243
00:17:25,360 --> 00:17:26,640
 of the body,

244
00:17:28,480 --> 00:17:34,320
 the feeling of the body, the texture. There's nothing here,

245
00:17:34,320 --> 00:17:35,760
 if it was in a different form,

246
00:17:35,760 --> 00:17:40,770
 there's nothing here that we would find at all desirable if

247
00:17:40,770 --> 00:17:42,880
 it wasn't something we recognized

248
00:17:42,880 --> 00:17:50,900
 as me, as mine, or as a source of potential sexual pleasure

249
00:17:50,900 --> 00:17:54,800
 usually, or comfort, emotional comfort in

250
00:17:54,800 --> 00:18:00,330
 terms of the embracing and connecting and kissing and

251
00:18:00,330 --> 00:18:04,960
 hugging and all this, or even the love for

252
00:18:04,960 --> 00:18:07,970
 family members, that kind of thing. But all this

253
00:18:07,970 --> 00:18:11,040
 recognition is really all that keeps us attached

254
00:18:11,040 --> 00:18:15,180
 to it. There's no sugar and spice and all things nice

255
00:18:15,180 --> 00:18:18,800
 inside even the female body, unfortunately.

256
00:18:20,000 --> 00:18:27,060
 But you know what I mean? Sorry to say. There certainly are

257
00:18:27,060 --> 00:18:28,480
 no diamonds or...

258
00:18:28,480 --> 00:18:31,190
 I think there are many things in the world that are

259
00:18:31,190 --> 00:18:33,520
 objectively more symmetrical and

260
00:18:33,520 --> 00:18:36,950
 easier from an objective point of view to understand the

261
00:18:36,950 --> 00:18:39,120
 attraction to diamonds, gold,

262
00:18:39,120 --> 00:18:46,310
 things that are like snowflakes maybe. But the human body

263
00:18:46,310 --> 00:18:49,360
 is a bit like a cesspool really,

264
00:18:49,360 --> 00:18:54,410
 it's full of urine and feces and sweat and grease and blood

265
00:18:54,410 --> 00:18:56,640
 and pus, all things that none of us

266
00:18:56,640 --> 00:19:01,190
 really want to see, or most of us are repulsed by. Let the

267
00:19:01,190 --> 00:19:04,320
 body sit around for a while without

268
00:19:04,320 --> 00:19:10,210
 washing it, without doing all the things that we do to

269
00:19:10,210 --> 00:19:14,640
 cover up its ugliness, and it becomes quite

270
00:19:14,640 --> 00:19:18,370
 repulsive. This is from a conventional point of view. Of

271
00:19:18,370 --> 00:19:20,720
 course, objectively speaking, there's

272
00:19:20,720 --> 00:19:24,100
 nothing repulsive even about a cesspool. A cesspool just is

273
00:19:24,100 --> 00:19:27,040
 what it is. The smell of a cesspool only

274
00:19:27,040 --> 00:19:30,300
 repulses us because of habit and the way the body is made

275
00:19:30,300 --> 00:19:32,800
 up. It's designed to be repulsed by a

276
00:19:32,800 --> 00:19:35,960
 cesspool, that's all. But there's nothing different between

277
00:19:35,960 --> 00:19:38,720
 a diamond and a cesspool, objectively

278
00:19:38,720 --> 00:19:43,640
 speaking. But certainly it's easy in the conventional point

279
00:19:43,640 --> 00:19:46,160
 of view to point out how the body is and

280
00:19:46,160 --> 00:19:49,390
 certainly not beautiful. So meditation helps us to see that

281
00:19:49,390 --> 00:19:51,200
. Once we're more objective, we're

282
00:19:51,200 --> 00:19:54,160
 able to let go of this somewhat ridiculous attachment to

283
00:19:54,160 --> 00:19:58,640
 the body. Mindfulness to the

284
00:19:58,640 --> 00:20:02,380
 feelings is best for helping us overcome the perversion of

285
00:20:02,380 --> 00:20:05,200
 pleasure, in perverted sense,

286
00:20:05,200 --> 00:20:08,650
 that those things that aren't pleasurable are pleasurable.

287
00:20:08,650 --> 00:20:10,240
 Once you watch the feelings,

288
00:20:10,240 --> 00:20:14,000
 you come to see that they're objectively just phenomena

289
00:20:14,000 --> 00:20:16,560
 that arise and cease. In fact,

290
00:20:16,560 --> 00:20:27,510
 they're incessant and they're invasive. You can't stop the

291
00:20:27,510 --> 00:20:29,200
 feelings from coming,

292
00:20:29,200 --> 00:20:31,860
 a happy, pleasant feeling, sometimes unpleasant feeling

293
00:20:31,860 --> 00:20:33,520
 sometimes, but even the pleasant ones,

294
00:20:33,520 --> 00:20:37,320
 because they arise and cease, arise and cease, eventually

295
00:20:37,320 --> 00:20:38,960
 one gets sick of them and realizes

296
00:20:38,960 --> 00:20:42,410
 that these feelings are not really of any benefit. They're

297
00:20:42,410 --> 00:20:46,320
 unpredictable and they're useless and

298
00:20:46,320 --> 00:20:50,560
 they serve no benefit. Mindfulness to the feelings is

299
00:20:50,560 --> 00:20:55,120
 useful for that. Mindfulness to the mind helps

300
00:20:55,120 --> 00:21:00,300
 us to see impermanence. We take the mind to be stable and

301
00:21:00,300 --> 00:21:02,400
 we try to make the mind stable. We

302
00:21:02,400 --> 00:21:05,410
 try to make reality stable. In fact, everything that we try

303
00:21:05,410 --> 00:21:07,280
 to make stable is circumscribed by

304
00:21:07,280 --> 00:21:14,130
 the fact that reality is based on the mind. When you talk

305
00:21:14,130 --> 00:21:17,600
 about the table, the bed, all of the

306
00:21:17,600 --> 00:21:23,170
 things around you, you're actually describing experiences

307
00:21:23,170 --> 00:21:27,200
 that are circumscribed by the mind,

308
00:21:27,200 --> 00:21:31,200
 that only last as long as that one mind state that observes

309
00:21:31,200 --> 00:21:33,920
 them. When you talk about anything in the

310
00:21:33,920 --> 00:21:37,190
 world, you're actually talking about your experiences. When

311
00:21:37,190 --> 00:21:38,640
 you watch the mind, you come to

312
00:21:38,640 --> 00:21:43,790
 see that they're all limited by the nature of the mind to

313
00:21:43,790 --> 00:21:45,600
 arise and cease.

314
00:21:45,600 --> 00:21:49,700
 So mindfulness of the mind helps overcome this idea of

315
00:21:49,700 --> 00:21:53,280
 permanence. Mindfulness of the dhammas

316
00:21:53,280 --> 00:21:57,880
 helps overcome the perversion of thinking things that are

317
00:21:57,880 --> 00:22:01,120
 not self or self. When you're mindful of

318
00:22:01,120 --> 00:22:05,450
 the various dhammas, various realities or the nature of

319
00:22:05,450 --> 00:22:08,720
 reality, the dhammas in many ways describe

320
00:22:08,720 --> 00:22:15,010
 how reality can be broken down into experience. So it

321
00:22:15,010 --> 00:22:18,560
 includes the hindrances, which are basically

322
00:22:18,560 --> 00:22:22,960
 the emotions. It includes the six senses, the five aggreg

323
00:22:22,960 --> 00:22:28,160
ates, all the many things that

324
00:22:28,160 --> 00:22:32,560
 make up reality. The mindfulness of these things helps

325
00:22:32,560 --> 00:22:36,320
 overcome the idea that we're in control,

326
00:22:36,320 --> 00:22:40,550
 especially of our emotions and our senses, and also that

327
00:22:40,550 --> 00:22:42,960
 there's any core. It's what helps us

328
00:22:43,760 --> 00:22:48,290
 realize that reality is just a bunch of experiences, that

329
00:22:48,290 --> 00:22:51,120
 the five aggregates don't describe

330
00:22:51,120 --> 00:22:54,340
 a being. You can't look inside the body and find the five

331
00:22:54,340 --> 00:22:56,320
 aggregates, but the five aggregates

332
00:22:56,320 --> 00:23:00,380
 describe a paradigm for reality that is based on experience

333
00:23:00,380 --> 00:23:03,360
. Each experience is made up of five

334
00:23:03,360 --> 00:23:07,560
 things called the five aggregates that arise at the six

335
00:23:07,560 --> 00:23:11,840
 senses. There are six types, so there's

336
00:23:11,840 --> 00:23:15,650
 seeing experience, there's hearing experience, smelling,

337
00:23:15,650 --> 00:23:18,000
 tasting, feeling, thinking experience,

338
00:23:18,000 --> 00:23:24,270
 and that's it. There's no self and no soul. So this is

339
00:23:24,270 --> 00:23:30,080
 another reason for having four foundations

340
00:23:30,080 --> 00:23:34,270
 of mindfulness, quite a good one. And each of these four

341
00:23:34,270 --> 00:23:38,960
 things, again, having three levels of

342
00:23:40,000 --> 00:23:44,680
 intensity, quite useful to think about in our meditation,

343
00:23:44,680 --> 00:23:47,680
 again, helping us to break down

344
00:23:47,680 --> 00:23:54,030
 the, break down the objects of experience, break down our

345
00:23:54,030 --> 00:23:58,560
 defilements, break down our problems,

346
00:23:58,560 --> 00:24:00,810
 help us to see what is the problem, help us to overcome the

347
00:24:00,810 --> 00:24:01,920
 problem. So that way,

348
00:24:01,920 --> 00:24:06,380
 it'll start to look at reality as it truly is. And it's a

349
00:24:06,380 --> 00:24:10,480
 means of helping overcome our misconceptions

350
00:24:10,480 --> 00:24:13,840
 and misunderstandings, the things that actually cause us

351
00:24:13,840 --> 00:24:15,840
 suffering when we're trying to find

352
00:24:15,840 --> 00:24:21,980
 happiness. So there's the demo for today. Thank you all for

353
00:24:21,980 --> 00:24:24,400
 tuning in and be well.

