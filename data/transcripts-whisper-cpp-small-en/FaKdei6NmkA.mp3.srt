1
00:00:00,000 --> 00:00:06,750
 Hi and welcome back to Buddhism 101. Today we will be

2
00:00:06,750 --> 00:00:11,160
 talking about the next

3
00:00:11,160 --> 00:00:16,220
 aspect of the Buddhist teaching and that is the

4
00:00:16,220 --> 00:00:19,800
 consequences of our actions. So

5
00:00:19,800 --> 00:00:25,880
 we've talked about charity, we've talked about morality.

6
00:00:25,880 --> 00:00:28,680
 The next step is to

7
00:00:28,680 --> 00:00:35,340
 look at how, sort of the why we should cultivate these good

8
00:00:35,340 --> 00:00:37,120
 deeds, why we should

9
00:00:37,120 --> 00:00:44,920
 abstain from unwholesome deeds. This is because our actions

10
00:00:44,920 --> 00:00:46,920
 have consequences

11
00:00:46,920 --> 00:00:53,670
 not only in the immediate sense but over the long term as

12
00:00:53,670 --> 00:00:56,120
 well and Buddhist theory

13
00:00:56,120 --> 00:01:12,250
 goes that our our journey in this universe continues.

14
00:01:12,250 --> 00:01:14,480
 Meaning we have

15
00:01:14,480 --> 00:01:21,690
 these experiences in this life and no one disputes that but

16
00:01:21,690 --> 00:01:22,400
 from a Buddhist

17
00:01:22,400 --> 00:01:28,240
 point of view the experiences are the foundation of reality

18
00:01:28,240 --> 00:01:31,400
. So the reality

19
00:01:31,400 --> 00:01:37,050
 around us, the time and space and so on, it's actually all

20
00:01:37,050 --> 00:01:39,680
 conceptual. If you

21
00:01:39,680 --> 00:01:44,480
 think of time, time is something that you think of. You

22
00:01:44,480 --> 00:01:45,840
 think of the past, you

23
00:01:45,840 --> 00:01:51,520
 think of the future. When you think of space, you look at

24
00:01:51,520 --> 00:01:53,360
 the room, this

25
00:01:53,360 --> 00:01:58,180
 comes up in the mind. The mind is aware of this space but

26
00:01:58,180 --> 00:02:00,000
 the basis of all of

27
00:02:00,000 --> 00:02:04,320
 that is experience. You're seeing, hearing, smelling,

28
00:02:04,320 --> 00:02:09,240
 tasting, feeling, thinking and

29
00:02:09,240 --> 00:02:20,520
 so those experiences that we have continue moment after

30
00:02:20,520 --> 00:02:20,920
 moment after

31
00:02:20,920 --> 00:02:27,890
 moment and continue even after the breakup of the physical

32
00:02:27,890 --> 00:02:30,920
 body. So if if we

33
00:02:30,920 --> 00:02:37,200
 look at another person and we see that their body has

34
00:02:37,200 --> 00:02:40,160
 ceased to live, it's very

35
00:02:40,160 --> 00:02:44,050
 hard to believe that there's anything going on there then

36
00:02:44,050 --> 00:02:45,200
 that that mind is

37
00:02:45,200 --> 00:02:50,020
 continuing because you can see all activity has ceased. Now

38
00:02:50,020 --> 00:02:51,360
 this is if you

39
00:02:51,360 --> 00:02:54,390
 look at things from an external point of view but if you

40
00:02:54,390 --> 00:02:55,640
 look at them from the

41
00:02:55,640 --> 00:02:58,730
 point of view of your own experience you can see your

42
00:02:58,730 --> 00:03:00,440
 experience is creating new

43
00:03:00,440 --> 00:03:07,410
 results. So if you're charitable and kind, this creates

44
00:03:07,410 --> 00:03:09,720
 positive results. If you're

45
00:03:09,720 --> 00:03:13,760
 moral and ethical, this helps you avoid negative results.

46
00:03:13,760 --> 00:03:16,600
 If you're on the

47
00:03:16,600 --> 00:03:26,440
 other hand greedy, stingy, cruel and immoral, then likewise

48
00:03:26,440 --> 00:03:27,640
 this will bring

49
00:03:27,640 --> 00:03:32,540
 other results. It changes and it creates a new aspect of

50
00:03:32,540 --> 00:03:33,920
 your existence. Every

51
00:03:33,920 --> 00:03:39,080
 moment we're creating and we're cultivating habits that not

52
00:03:39,080 --> 00:03:40,480
 only change

53
00:03:40,480 --> 00:03:44,970
 the world around us and change our environment but also

54
00:03:44,970 --> 00:03:46,160
 turn us and and and

55
00:03:46,160 --> 00:03:54,480
 put us on a new pathway. So because of the habits they

56
00:03:54,480 --> 00:03:55,560
 change our whole

57
00:03:55,560 --> 00:04:00,640
 perception and our whole state of being. So this is the

58
00:04:00,640 --> 00:04:02,680
 reason, this is the

59
00:04:02,680 --> 00:04:08,720
 significance of our emphasis on wholesome deeds and the

60
00:04:08,720 --> 00:04:10,160
 avoidance of unwholesome deeds.

61
00:04:10,160 --> 00:04:16,090
 So practicing charity and and cultivating morality. So how

62
00:04:16,090 --> 00:04:17,360
 it works is

63
00:04:17,360 --> 00:04:22,140
 when you die, your whole life they say flashes before your

64
00:04:22,140 --> 00:04:22,920
 eyes. Right? Well that

65
00:04:22,920 --> 00:04:28,040
 is the things in your mind that are most clear to you. The

66
00:04:28,040 --> 00:04:28,400
 things in

67
00:04:28,400 --> 00:04:32,030
 your mind that you hold on most strongly because the rest

68
00:04:32,030 --> 00:04:32,560
 of the things

69
00:04:32,560 --> 00:04:35,850
 becoming insignificant. The rest of your experiences, your

70
00:04:35,850 --> 00:04:37,740
 day-to-day life goes

71
00:04:37,740 --> 00:04:40,800
 out the window. You're not thinking about your shopping,

72
00:04:40,800 --> 00:04:41,600
 you're not thinking about

73
00:04:41,600 --> 00:04:45,520
 your laundry. When you're dying all of these things become

74
00:04:45,520 --> 00:04:47,160
 insignificant and so

75
00:04:47,160 --> 00:04:50,480
 they fade away. What's left is all the really important

76
00:04:50,480 --> 00:04:51,360
 stuff and that's why it

77
00:04:51,360 --> 00:04:54,770
 flashes up. You start to think of all the things you've

78
00:04:54,770 --> 00:04:56,080
 done to hurt others or the

79
00:04:56,080 --> 00:04:59,080
 good things you've done to help others. All the bad things

80
00:04:59,080 --> 00:05:00,120
 people have done to

81
00:05:00,120 --> 00:05:03,880
 you or the good things people have done for you. All of the

82
00:05:03,880 --> 00:05:05,400
 important and

83
00:05:05,400 --> 00:05:12,210
 emotional, emotionally charged aspects of your life come up

84
00:05:12,210 --> 00:05:15,200
. And we at that moment

85
00:05:15,200 --> 00:05:21,960
 the inclination is to cling to something.

86
00:05:21,960 --> 00:05:25,390
 Absolutely these are the things that we cling to. So

87
00:05:25,390 --> 00:05:27,160
 something is going to grab

88
00:05:27,160 --> 00:05:31,770
 your attention and you start to react to it. And we do this

89
00:05:31,770 --> 00:05:32,680
 in our life anyway.

90
00:05:32,680 --> 00:05:34,880
 When we have a thought we'll react to it and we'll say, "Oh

91
00:05:34,880 --> 00:05:35,720
 yeah I should do that."

92
00:05:35,720 --> 00:05:40,240
 And right away we start up a new ambition, a new train and

93
00:05:40,240 --> 00:05:41,040
 start a new

94
00:05:41,040 --> 00:05:44,540
 journey. The moment of death you see all of the physical

95
00:05:44,540 --> 00:05:46,040
 aspects of our being are

96
00:05:46,040 --> 00:05:55,080
 shutting down. So they also move out of the picture.

97
00:05:55,080 --> 00:06:02,000
 So memories of this life and the brain stops working, all

98
00:06:02,000 --> 00:06:02,240
 of the

99
00:06:02,240 --> 00:06:06,910
 thoughts that would come from the brain. And all that's

100
00:06:06,910 --> 00:06:08,440
 left is this. So in

101
00:06:08,440 --> 00:06:11,770
 our life, well we create new ambitions based on our desires

102
00:06:11,770 --> 00:06:12,400
 and so on. That's

103
00:06:12,400 --> 00:06:17,280
 part of it. But much of it is just our brain regurgitating

104
00:06:17,280 --> 00:06:19,280
 memories and sensory

105
00:06:19,280 --> 00:06:23,920
 perceptions. All of that is gone. All that we're left with

106
00:06:23,920 --> 00:06:25,680
 is the mind. So the

107
00:06:25,680 --> 00:06:31,870
 deeds that we have done become very powerful and the

108
00:06:31,870 --> 00:06:33,720
 clinging that goes on

109
00:06:33,720 --> 00:06:38,950
 becomes the catalyst. It's the only thing left. And so this

110
00:06:38,950 --> 00:06:39,920
 is why when

111
00:06:39,920 --> 00:06:44,560
 you start out, I mean if you believe or if you follow

112
00:06:44,560 --> 00:06:47,080
 Buddhist logic, reason and

113
00:06:47,080 --> 00:06:51,280
 thought, you start off with something very simple. As a

114
00:06:51,280 --> 00:06:52,280
 human being you'll

115
00:06:52,280 --> 00:06:57,160
 start off with a simple egg and a sperm, very small,

116
00:06:57,160 --> 00:06:58,600
 because that's all that

117
00:06:58,600 --> 00:07:01,900
 you've got. It's just the seed that you want to be a human

118
00:07:01,900 --> 00:07:02,960
 again or the idea of

119
00:07:02,960 --> 00:07:08,070
 being a human and the desire for human life. If you don't

120
00:07:08,070 --> 00:07:09,480
 have that, well there's

121
00:07:09,480 --> 00:07:13,080
 different things that happen. Example, if you're full of

122
00:07:13,080 --> 00:07:16,480
 anger, then when you die

123
00:07:16,480 --> 00:07:21,050
 and you grasp onto an angry, you know, unpleasant, disple

124
00:07:21,050 --> 00:07:24,480
ased thought, then you

125
00:07:24,480 --> 00:07:28,530
 don't, you're not born as a human. You're born in a state

126
00:07:28,530 --> 00:07:29,880
 of anger, a state of

127
00:07:29,880 --> 00:07:33,840
 pain, a state of suffering. What we would call the

128
00:07:33,840 --> 00:07:37,600
 equivalent of the Judeo-Christian

129
00:07:37,600 --> 00:07:41,920
 hell. Now it's not permanent. I mean these are states that

130
00:07:41,920 --> 00:07:43,720
 come about

131
00:07:43,720 --> 00:07:48,760
 based on your bent, you know, your intentions, your mind

132
00:07:48,760 --> 00:07:49,640
 state when you die.

133
00:07:49,640 --> 00:07:54,120
 So they are not, they last based on that, the power of that

134
00:07:54,120 --> 00:07:56,760
 desire, just as any

135
00:07:56,760 --> 00:08:02,300
 mind state, any state that comes from the mind. It has a

136
00:08:02,300 --> 00:08:04,640
 power and it arises and it

137
00:08:04,640 --> 00:08:08,730
 prevails and eventually it ceases. Now it can last a long,

138
00:08:08,730 --> 00:08:10,560
 long time depending on

139
00:08:10,560 --> 00:08:14,000
 how powerful is the evil intention, the angry, unwholesome,

140
00:08:14,000 --> 00:08:15,960
 unpleasant,

141
00:08:15,960 --> 00:08:22,040
 undispleased intention, you know. But then it's over. But

142
00:08:22,040 --> 00:08:23,160
 this is what happens,

143
00:08:23,160 --> 00:08:31,230
 anger, anger, the result of anger is to go to one of the

144
00:08:31,230 --> 00:08:33,840
 many hells. So any, you

145
00:08:33,840 --> 00:08:38,840
 create a state, you enter into a state of pain and

146
00:08:38,840 --> 00:08:40,080
 suffering. So a person who's

147
00:08:40,080 --> 00:08:44,980
 killed a lot of other beings or is cruel and unpleasant,

148
00:08:44,980 --> 00:08:46,200
 harsh speech, that kind

149
00:08:46,200 --> 00:08:53,810
 of thing. If your mind is full of greed on the other hand,

150
00:08:53,810 --> 00:08:56,480
 greed is what we

151
00:08:56,480 --> 00:08:59,650
 normally associate with ghosts. That's in Buddhism, that's

152
00:08:59,650 --> 00:09:01,280
 where it leads you. If

153
00:09:01,280 --> 00:09:05,230
 you have a mind full of greed, desire, attachment, clinging

154
00:09:05,230 --> 00:09:06,480
 to something. So if

155
00:09:06,480 --> 00:09:10,310
 you hear these ghost stories about ghosts that are sent to

156
00:09:10,310 --> 00:09:12,160
 haunt places that

157
00:09:12,160 --> 00:09:18,420
 have meaning for them, maybe they appear in some clothes

158
00:09:18,420 --> 00:09:19,760
 that are associated

159
00:09:19,760 --> 00:09:22,980
 with something they cling to and that kind of thing. This

160
00:09:22,980 --> 00:09:25,800
 is the general nature

161
00:09:25,800 --> 00:09:30,320
 of a ghost, is that they are hungry, they want something,

162
00:09:30,320 --> 00:09:31,400
 they are

163
00:09:31,400 --> 00:09:36,270
 attached to something, they can't let go. So if you live

164
00:09:36,270 --> 00:09:37,080
 your life full of

165
00:09:37,080 --> 00:09:40,750
 greed and when you die that's the overwhelming emotion and

166
00:09:40,750 --> 00:09:41,600
 that's what you

167
00:09:41,600 --> 00:09:46,760
 cling to is some greedy, lustful, desirous thought. Being

168
00:09:46,760 --> 00:09:47,840
 born as a ghost is

169
00:09:47,840 --> 00:09:54,680
 the likely destination. If you have delusion in your mind,

170
00:09:54,680 --> 00:10:01,510
 delusion being kind of confusion and arrogance may be conce

171
00:10:01,510 --> 00:10:04,720
it, wrong view or

172
00:10:04,720 --> 00:10:08,060
 if you're a person who takes a lot of mind-numbing drugs so

173
00:10:08,060 --> 00:10:09,080
 that you just have

174
00:10:09,080 --> 00:10:13,460
 a confused state of mind or if you are keen on stupidity

175
00:10:13,460 --> 00:10:15,120
 and ignorance and that

176
00:10:15,120 --> 00:10:17,880
 kind of thing, that interests you and that's your bend.

177
00:10:17,880 --> 00:10:19,440
 Then when you die, if

178
00:10:19,440 --> 00:10:23,320
 you die in a confused mind or if you die in a deluded mind,

179
00:10:23,320 --> 00:10:25,240
 born as an animal.

180
00:10:25,240 --> 00:10:29,070
 This is the state of animals, they're always sort of cloudy

181
00:10:29,070 --> 00:10:29,840
 in the mind,

182
00:10:29,840 --> 00:10:33,200
 confused and they can be very arrogant and conceitant but

183
00:10:33,200 --> 00:10:34,320
 they don't have much

184
00:10:34,320 --> 00:10:40,340
 wisdom or understanding, they're not able to comprehend

185
00:10:40,340 --> 00:10:43,640
 deep concepts like

186
00:10:43,640 --> 00:10:49,450
 meditation or spirituality, that kind of thing. Not really

187
00:10:49,450 --> 00:10:50,200
 easy for them because

188
00:10:50,200 --> 00:10:53,840
 their minds are clouded and that's what happens, that's

189
00:10:53,840 --> 00:10:54,400
 where you can go in that

190
00:10:54,400 --> 00:10:57,990
 case. Now an ordinary person who has ordinary thoughts will

191
00:10:57,990 --> 00:10:58,680
 be born as a

192
00:10:58,680 --> 00:11:01,140
 human being, that's sort of in the middle of somewhere, you

193
00:11:01,140 --> 00:11:02,080
 know, if you're not too

194
00:11:02,080 --> 00:11:05,350
 evil of person but not too good of a person, you'll

195
00:11:05,350 --> 00:11:06,600
 probably be born as a human

196
00:11:06,600 --> 00:11:09,440
 again and have to go through all the same sorts of things,

197
00:11:09,440 --> 00:11:10,440
 give or take

198
00:11:10,440 --> 00:11:17,040
 depending on your karma, there's very many variables. So if

199
00:11:17,040 --> 00:11:17,600
 you, you might be a

200
00:11:17,600 --> 00:11:21,060
 sick person, you might be a healthy person, you might be a

201
00:11:21,060 --> 00:11:22,160
 strong person, you

202
00:11:22,160 --> 00:11:25,120
 might be an intelligent person, all these very different

203
00:11:25,120 --> 00:11:27,200
 variables. You may be rich,

204
00:11:27,200 --> 00:11:31,400
 you may be poor, this kind of thing, but you'll be born a

205
00:11:31,400 --> 00:11:33,760
 human. Now to be born in

206
00:11:33,760 --> 00:11:38,260
 heaven, Buddhism has this concept of heaven but it's just

207
00:11:38,260 --> 00:11:39,600
 very much like being

208
00:11:39,600 --> 00:11:43,630
 a human except nicer, more comfortable. So there are states

209
00:11:43,630 --> 00:11:45,800
, there are many many

210
00:11:45,800 --> 00:11:53,670
 different states and it's all a great, so it's degrees of

211
00:11:53,670 --> 00:11:55,240
 happiness and suffering.

212
00:11:55,240 --> 00:11:59,800
 If you're a very good person, a person who is by nature

213
00:11:59,800 --> 00:12:01,240
 kind and caring and

214
00:12:01,240 --> 00:12:07,600
 helpful and engages in wholesomeness and and refrains from

215
00:12:07,600 --> 00:12:08,320
 unwholesomeness,

216
00:12:08,320 --> 00:12:12,240
 abstains from unwholesomeness, then you can be pretty sure,

217
00:12:12,240 --> 00:12:13,480
 you can be proud of

218
00:12:13,480 --> 00:12:17,430
 yourself, happy about, you can be confident in yourself,

219
00:12:17,430 --> 00:12:19,060
 reassured that

220
00:12:19,060 --> 00:12:23,490
 you're going to a good place. People who are by nature good

221
00:12:23,490 --> 00:12:24,760
, who has their

222
00:12:24,760 --> 00:12:29,110
 default, constantly thinking of ways to do good deeds and

223
00:12:29,110 --> 00:12:30,320
 careful to avoid

224
00:12:30,320 --> 00:12:32,640
 unwholesomeness like killing and stealing and all the

225
00:12:32,640 --> 00:12:33,560
 things that I talked

226
00:12:33,560 --> 00:12:40,160
 about with in regards to morality, then they go quite

227
00:12:40,160 --> 00:12:43,640
 easily go to heaven. So

228
00:12:43,640 --> 00:12:51,820
 these are some of the results of mundane good deeds. Now

229
00:12:51,820 --> 00:12:52,520
 important out of

230
00:12:52,520 --> 00:12:56,880
 all of this and what we'll talk about next time is not so

231
00:12:56,880 --> 00:12:58,160
 much the idea of

232
00:12:58,160 --> 00:13:00,470
 going to heaven and avoiding going to hell but really an

233
00:13:00,470 --> 00:13:01,460
 understanding of how

234
00:13:01,460 --> 00:13:05,210
 the system works because it's an important framework on

235
00:13:05,210 --> 00:13:06,280
 which to base our

236
00:13:06,280 --> 00:13:09,990
 meditation practice. If we base our meditation practice on

237
00:13:09,990 --> 00:13:12,080
 external, you know,

238
00:13:12,080 --> 00:13:16,310
 reality, then we start to think of our mental illnesses,

239
00:13:16,310 --> 00:13:17,920
 our mental problems as

240
00:13:17,920 --> 00:13:25,480
 being a part of nature or a part of biology and so not

241
00:13:25,480 --> 00:13:26,680
 subject to our

242
00:13:26,680 --> 00:13:33,890
 ability to change. So we lose this sense of self-respons

243
00:13:33,890 --> 00:13:36,040
ibility and that's

244
00:13:36,040 --> 00:13:38,430
 important. It's important to understand that that's not

245
00:13:38,430 --> 00:13:40,240
 really how it works, that

246
00:13:40,240 --> 00:13:45,800
 death isn't the end, that we really have a reason to

247
00:13:45,800 --> 00:13:47,560
 cultivate good and evil beyond

248
00:13:47,560 --> 00:13:52,940
 just the simple well. It's what good people do or it's to

249
00:13:52,940 --> 00:13:53,840
 live a good life in

250
00:13:53,840 --> 00:13:58,070
 this life because in the end if death was it then it would

251
00:13:58,070 --> 00:13:58,520
 all be pointless.

252
00:13:58,520 --> 00:14:00,910
 There would be no need to strive or worry or concern

253
00:14:00,910 --> 00:14:02,680
 yourself about goodness

254
00:14:02,680 --> 00:14:06,200
 or evil at all and so that's how many of us live our lives

255
00:14:06,200 --> 00:14:07,720
 and as a result we

256
00:14:07,720 --> 00:14:10,800
 have a lot of problems in this world because people are all

257
00:14:10,800 --> 00:14:11,720
 live for the

258
00:14:11,720 --> 00:14:15,510
 moment, you only live once, these kind of things and as a

259
00:14:15,510 --> 00:14:17,200
 result regardless of

260
00:14:17,200 --> 00:14:20,350
 what happens to them we can see the degradation that it

261
00:14:20,350 --> 00:14:21,160
 causes in the

262
00:14:21,160 --> 00:14:25,480
 world that we live in and that should be a sign for us that

263
00:14:25,480 --> 00:14:26,920
 we have, there's

264
00:14:26,920 --> 00:14:29,680
 perhaps more to the world than we think, more to the

265
00:14:29,680 --> 00:14:31,440
 universe than we think and

266
00:14:31,440 --> 00:14:35,420
 so this is important for you to examine and you don't have

267
00:14:35,420 --> 00:14:36,560
 to believe it right

268
00:14:36,560 --> 00:14:39,640
 off but I would encourage you to consider that this might

269
00:14:39,640 --> 00:14:40,200
 be the case

270
00:14:40,200 --> 00:14:44,760
 because it's an important part of why we want to,

271
00:14:44,760 --> 00:14:47,720
 cultivating the desire to

272
00:14:47,720 --> 00:14:52,520
 develop meditation practice which we'll be talking about in

273
00:14:52,520 --> 00:14:54,680
 upcoming segments of

274
00:14:54,680 --> 00:15:00,920
 Buddhism 101 but for now this is just an outline of what

275
00:15:00,920 --> 00:15:02,360
 happens to us in the

276
00:15:02,360 --> 00:15:05,320
 future where we go as a result of our deeds, what are the

277
00:15:05,320 --> 00:15:06,960
 consequences of good

278
00:15:06,960 --> 00:15:10,550
 and evil. So thank you for tuning in, wishing you all the

279
00:15:10,550 --> 00:15:11,960
 best.

