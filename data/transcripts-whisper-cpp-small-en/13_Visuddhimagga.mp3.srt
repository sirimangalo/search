1
00:00:00,000 --> 00:00:03,570
 in the teachings of the Buddha, you know, the virtue of

2
00:00:03,570 --> 00:00:06,240
 seed, we call it Zila, the

3
00:00:06,240 --> 00:00:10,260
 keeping of precepts. The virtue, I mean the purely of

4
00:00:10,260 --> 00:00:13,440
 virtue is very important because it is here said

5
00:00:13,440 --> 00:00:17,850
 the firm ground of virtue, right? Right, on the feet of

6
00:00:17,850 --> 00:00:22,480
 energy on the ground. Yes, so Zila is

7
00:00:22,480 --> 00:00:28,610
 compared to ground and energy is compared to the feet and

8
00:00:28,610 --> 00:00:31,040
 faith or confidence is compared to

9
00:00:31,040 --> 00:00:34,480
 hand because if you have a hand you can pick up things you

10
00:00:34,480 --> 00:00:36,320
 want to pick up. If you do not have

11
00:00:36,320 --> 00:00:39,590
 hands you cannot. In the same way if you do not have any

12
00:00:39,590 --> 00:00:42,320
 confidence then you cannot pick up

13
00:00:42,320 --> 00:00:47,600
 kusala or good things. So the hand is compared to, I mean,

14
00:00:47,600 --> 00:00:51,040
 confidence of faith is compared to

15
00:00:52,400 --> 00:00:57,660
 to a hand and the food, maga, is compared to an axe which

16
00:00:57,660 --> 00:01:02,640
 cuts all together the mental defiance.

17
00:01:02,640 --> 00:01:06,040
 So if your conduct isn't purely virtuous then there's no

18
00:01:06,040 --> 00:01:08,560
 way that you're not going to be involved

19
00:01:08,560 --> 00:01:11,650
 in not creating karmic because if your conduct is not

20
00:01:11,650 --> 00:01:14,160
 virtuous then you'll, whatever you call

21
00:01:14,160 --> 00:01:18,090
 yourself is to be created. That's right, yes. What is an

22
00:01:18,090 --> 00:01:23,040
 example of functional? Functional is something like

23
00:01:23,040 --> 00:01:28,620
 I gave this example but but Timur didn't like that. He said

24
00:01:28,620 --> 00:01:30,640
 it was too maybe,

25
00:01:30,640 --> 00:01:36,030
 I don't know, it's like a fruit which does not bear, I mean

26
00:01:36,030 --> 00:01:38,240
 a tree which does not bear fruit.

27
00:01:39,040 --> 00:01:43,080
 It's still, it's still is growing, living, it still has

28
00:01:43,080 --> 00:01:46,000
 branches and leaves and so on but it does not

29
00:01:46,000 --> 00:01:51,510
 bear fruit, something like that. An arahant, after becoming

30
00:01:51,510 --> 00:01:55,440
 an arahant, still do good things. They

31
00:01:55,440 --> 00:02:00,430
 still teach people, still practice meditation and still

32
00:02:00,430 --> 00:02:04,000
 help other people but all their actions

33
00:02:05,040 --> 00:02:10,170
 do not constitute karma because they have eradicated the

34
00:02:10,170 --> 00:02:13,200
 root of karma, I mean the root of

35
00:02:13,200 --> 00:02:19,860
 existence, that is the craving and ignorance. So their

36
00:02:19,860 --> 00:02:24,080
 actions are just actions and their

37
00:02:24,080 --> 00:02:31,940
 actions do not bring reactions, I mean results. We in Burma

38
00:02:31,940 --> 00:02:35,360
 at the New Year's ceremony,

39
00:02:35,360 --> 00:02:45,300
 the cannons were fired and so when something is just, just,

40
00:02:45,300 --> 00:02:48,080
 just noise and has no substance,

41
00:02:48,080 --> 00:02:51,440
 we say this is the New Year's cannon or something like that

42
00:02:51,440 --> 00:02:55,840
. What do you call that? Volley of

43
00:02:55,840 --> 00:03:00,720
 something? Yeah, so it is something like that.

44
00:03:03,360 --> 00:03:08,490
 Fruit, I mean a tree is bearing no fruit. They are still

45
00:03:08,490 --> 00:03:11,760
 fruit. That's a little of yes.

46
00:03:11,760 --> 00:03:25,040
 Okay, the second one, sammasan Buddha. Now Buddha, we use

47
00:03:25,040 --> 00:03:30,240
 the word Buddha to refer to him but his full

48
00:03:31,920 --> 00:03:40,630
 epithet is sammasan Buddha. Now samma is one prefix, sam is

49
00:03:40,630 --> 00:03:43,200
 one prefix. There are two prefixes here

50
00:03:43,200 --> 00:03:49,820
 and bhudam. Samma means rightly and sam means by himself.

51
00:03:49,820 --> 00:03:54,480
 So he discovers the four noble truths,

52
00:03:55,360 --> 00:03:59,480
 bhudam means to know. So he knows the four noble truths or

53
00:03:59,480 --> 00:04:02,000
 he discovers the four noble truths

54
00:04:02,000 --> 00:04:07,400
 rightly and without assistance from any person by himself.

55
00:04:07,400 --> 00:04:10,800
 So we emphasize this when we talk about

56
00:04:10,800 --> 00:04:14,410
 Buddha. Buddha are those persons who do not need any

57
00:04:14,410 --> 00:04:20,400
 teachers. So Bholi Sadhguru went to two teachers

58
00:04:20,400 --> 00:04:26,000
 before he became the Buddha but he did not follow their

59
00:04:26,000 --> 00:04:30,640
 advice up to the attainment of Bholi. He

60
00:04:30,640 --> 00:04:36,240
 discovered their practice and then went to a place and

61
00:04:36,240 --> 00:04:41,120
 practiced by himself. So we say Bholi had no

62
00:04:41,120 --> 00:04:45,640
 teachers and this is emphasized by this sambhoda. So samb

63
00:04:45,640 --> 00:04:49,520
hoda means self-enlightenment and samma

64
00:04:49,520 --> 00:04:53,970
 bhoda means rightly enlightened. So sammasan bhoda means

65
00:04:53,970 --> 00:04:57,040
 rightly self-enlightened, something like that.

66
00:04:57,040 --> 00:05:03,920
 And here to know means to know the four noble truths.

67
00:05:03,920 --> 00:05:16,000
 And then on page 211, paragraph 28, the other things which

68
00:05:16,000 --> 00:05:17,920
 Buddha knew are given.

69
00:05:17,920 --> 00:05:21,440
 There are six bases, six groups of consciousness and so on.

70
00:05:21,440 --> 00:05:24,080
 So these are the topics taught in

71
00:05:24,080 --> 00:05:29,690
 abhidhamma. Actually they are found in sotabhita too but

72
00:05:29,690 --> 00:05:32,800
 their treatment in food can be found in

73
00:05:32,800 --> 00:05:38,960
 abhidhamma. Sometimes people take abhidhamma to be a

74
00:05:38,960 --> 00:05:44,880
 separate thing, not connected with sudha or

75
00:05:44,880 --> 00:05:49,960
 something like that. But actually those we found in the ab

76
00:05:49,960 --> 00:05:53,760
hidhamma, most of them are also taught

77
00:05:53,760 --> 00:05:57,430
 in sotabhita. That is why we need a knowledge of abhidhamma

78
00:05:57,430 --> 00:06:01,600
 to understand the sotas. We should not

79
00:06:01,600 --> 00:06:09,790
 separate them, treat them as separate. We cannot study sot

80
00:06:09,790 --> 00:06:14,080
as without a knowledge of abhidhamma.

81
00:06:14,080 --> 00:06:18,230
 That means that is if we want to understand fully and

82
00:06:18,230 --> 00:06:23,200
 correctly. So these are topics taught in

83
00:06:23,200 --> 00:06:34,560
 abhidhamma and sotabhita also. So here Buddha's penetration

84
00:06:34,560 --> 00:06:37,040
 into the four noble truths or discovery

85
00:06:37,040 --> 00:06:44,510
 of four noble truths is meant by this second attribute sam

86
00:06:44,510 --> 00:06:46,400
ma, sambuddha.

87
00:06:46,400 --> 00:06:55,120
 Now, ahayana is usually thought that an arahat is one who

88
00:06:55,120 --> 00:06:56,240
 doesn't have it,

89
00:06:56,240 --> 00:07:01,510
 who is enlightened by himself. Well actually it's Pradhyek

90
00:07:01,510 --> 00:07:02,480
ha Buddha. Yes,

91
00:07:02,480 --> 00:07:07,380
 Pradhyekha Buddha is one who is enlightened by himself.

92
00:07:07,380 --> 00:07:11,600
 Yeah. And that's usually distinguished

93
00:07:11,600 --> 00:07:15,790
 from a Buddha who isn't at least totally enlightened by

94
00:07:15,790 --> 00:07:19,520
 himself or in the vacuum with other people.

95
00:07:19,520 --> 00:07:27,380
 The Pradhyekha Buddhas are not called samma-sambuddha. They

96
00:07:27,380 --> 00:07:30,960
 are called just sambuddha or

97
00:07:30,960 --> 00:07:36,200
 Pradhyekha Buddhas but not samma-sambuddha. So they're not

98
00:07:36,200 --> 00:07:38,880
 rightly. Rightly means in all aspects,

99
00:07:38,880 --> 00:07:43,760
 they do not understand in all aspects of the dharma.

100
00:07:43,760 --> 00:07:50,080
 They are self-enlightened persons but their knowledge may

101
00:07:50,080 --> 00:07:50,400
 be

102
00:07:52,400 --> 00:07:57,630
 not as wide, as comprehensive as that of the Buddhas. And

103
00:07:57,630 --> 00:08:02,560
 also sometimes they are called silent

104
00:08:02,560 --> 00:08:08,380
 Buddhas. That comes from the fact that they do not teach

105
00:08:08,380 --> 00:08:12,240
 much. Although sometimes they teach but

106
00:08:12,240 --> 00:08:15,230
 they seldom teach. They want to be away from people and

107
00:08:15,230 --> 00:08:18,080
 live in the forest and so they are

108
00:08:18,080 --> 00:08:21,480
 called silent Buddhas. Well sometimes it's because they

109
00:08:21,480 --> 00:08:24,000
 weren't taught, it's hard for them to teach.

110
00:08:24,000 --> 00:08:28,860
 And that's usually kind of the conscious distinction of the

111
00:08:28,860 --> 00:08:30,240
 Buddha but maybe the

112
00:08:30,240 --> 00:08:37,430
 and also they appear only when there are no Buddhas. So

113
00:08:37,430 --> 00:08:41,520
 they appear between interrains of Buddhas.

114
00:08:46,480 --> 00:08:51,680
 And then I hope you read the footnotes. The footnotes are

115
00:08:51,680 --> 00:08:52,000
 very good.

116
00:08:52,000 --> 00:08:59,540
 So we'll go next to the third one. He is endowed with clear

117
00:08:59,540 --> 00:09:02,000
 vision and virtuous conduct.

118
00:09:02,000 --> 00:09:07,030
 There are two words here, vijja and charana. Vijja means

119
00:09:07,030 --> 00:09:11,520
 understanding of vision and charana

120
00:09:11,520 --> 00:09:17,960
 means conduct. So Buddha is endowed with both clear vision

121
00:09:17,960 --> 00:09:19,520
 and conduct.

122
00:09:19,520 --> 00:09:23,280
 There are three kinds of clear vision. That means

123
00:09:23,280 --> 00:09:28,080
 remembering his past lives and then

124
00:09:28,080 --> 00:09:34,720
 divine eye and destruction of the family. They are called

125
00:09:34,720 --> 00:09:38,240
 three clear visions. So whenever

126
00:09:39,120 --> 00:09:44,310
 three is mentioned, these are meant. Remembering past lives

127
00:09:44,310 --> 00:09:50,320
, divine vision, and destruction of

128
00:09:50,320 --> 00:09:55,810
 defilement. These are called three clear visions. Sometimes

129
00:09:55,810 --> 00:09:57,280
 eight kinds of clear visions are

130
00:09:57,280 --> 00:10:05,700
 mentioned. So in that case there are some more. So the

131
00:10:05,700 --> 00:10:11,120
 eight kinds are stated in the unburger sutra.

132
00:10:11,120 --> 00:10:14,840
 For there are eight kinds of clear visions stated and eight

133
00:10:14,840 --> 00:10:17,040
 are made up of six kinds of direct

134
00:10:17,040 --> 00:10:21,000
 knowledge together with insight and the supernormal power

135
00:10:21,000 --> 00:10:24,080
 of the mind-made body. So there are eight

136
00:10:24,080 --> 00:10:26,960
 kinds of clear visions mentioned in that sutra. But if you

137
00:10:26,960 --> 00:10:29,600
 go to that sutra you will not find

138
00:10:29,600 --> 00:10:35,970
 them clearly stated because in Pali, when they don't want

139
00:10:35,970 --> 00:10:39,360
 to repeat, they just put a sign there

140
00:10:39,360 --> 00:10:48,330
 like you use dots, right? Three or four dots. So in Pali

141
00:10:48,330 --> 00:10:52,560
 also they use the letter P-A

142
00:10:54,000 --> 00:10:57,620
 or sometimes L-A. So when you see this, you understand that

143
00:10:57,620 --> 00:10:59,520
 there are some things

144
00:10:59,520 --> 00:11:03,970
 missing here or they don't want to repeat. So you have to

145
00:11:03,970 --> 00:11:06,000
 go back to where the first

146
00:11:06,000 --> 00:11:08,850
 occurred and if you don't know where to find them then you

147
00:11:08,850 --> 00:11:09,440
'll be lost.

148
00:11:11,840 --> 00:11:19,910
 So today I looked them up in in D1 100, page 100 and they

149
00:11:19,910 --> 00:11:23,600
 were not not printed in the book

150
00:11:23,600 --> 00:11:29,350
 because they were mentioned in the second sutra, the unbur

151
00:11:29,350 --> 00:11:31,680
ger sutra, third sutra. So

152
00:11:31,680 --> 00:11:38,680
 this is one problem with us modern people. These books are

153
00:11:38,680 --> 00:11:41,520
 meant to be read from the beginning to

154
00:11:41,520 --> 00:11:44,540
 the end, not just pick up a soda in the middle and you read

155
00:11:44,540 --> 00:11:46,000
 it and you understand.

156
00:11:46,000 --> 00:11:52,640
 So they are meant to be studied from the beginning.

157
00:11:52,640 --> 00:11:58,480
 I can give you the page numbers of the English translation.

158
00:11:58,480 --> 00:12:05,520
 So the eight are the six kinds of direct knowledge. That

159
00:12:05,520 --> 00:12:05,840
 means

160
00:12:09,440 --> 00:12:15,360
 the performing miracles, that is one. And then divine ear

161
00:12:15,360 --> 00:12:21,830
 and then breathing other people's mind and remembering past

162
00:12:21,830 --> 00:12:25,920
 lives and then divine eye.

163
00:12:25,920 --> 00:12:32,120
 You get five, right? And then destruction of mental defer

164
00:12:32,120 --> 00:12:35,520
ments, six. And then two are mentioned

165
00:12:35,520 --> 00:12:40,950
 here. We pass an insight and supernormal power of the mind-

166
00:12:40,950 --> 00:12:44,400
made body. That means you practice jhana

167
00:12:44,400 --> 00:12:48,960
 and then you are able to multiply yourself. So these are

168
00:12:48,960 --> 00:12:53,520
 the eight kinds of clear vision stated

169
00:12:53,520 --> 00:12:59,170
 in that sutra. And by contact is meant 15 things here. Rest

170
00:12:59,170 --> 00:13:01,440
raint by virtue, guarding the doors of

171
00:13:01,440 --> 00:13:04,440
 the sense faculties, knowledge of the right amount in

172
00:13:04,440 --> 00:13:07,440
 eating, devotion to wakefulness, the seven good

173
00:13:07,440 --> 00:13:11,670
 states there on the footnote, and the four jhanas of the

174
00:13:11,670 --> 00:13:14,880
 fine material sphere. For it is precisely

175
00:13:14,880 --> 00:13:18,760
 by means of these 15 things that a noble disciple contacts

176
00:13:18,760 --> 00:13:21,280
 himself, that he goes towards the dead

177
00:13:21,280 --> 00:13:25,080
 that that is why it is called virtue conduct according as

178
00:13:25,080 --> 00:13:27,440
 it is said. Now you have to understand

179
00:13:27,440 --> 00:13:32,540
 the Paliwar charana to understand this this explanation.

180
00:13:32,540 --> 00:13:35,040
 The war charana comes from the

181
00:13:35,040 --> 00:13:40,850
 root chara, c-a-r-a. And chara means to walk, to go. So

182
00:13:40,850 --> 00:13:43,760
 charana means some means of going.

183
00:13:43,760 --> 00:13:51,180
 So these 15 are the means of going to to the direction of n

184
00:13:51,180 --> 00:13:52,000
ipana.

185
00:13:54,080 --> 00:14:00,180
 And this is why it is said here it is precisely by means of

186
00:14:00,180 --> 00:14:01,920
 these 15 things that a noble

187
00:14:01,920 --> 00:14:04,850
 disciple contacts himself that he goes towards the dead

188
00:14:04,850 --> 00:14:07,840
 that is why it is called Pali charana.

189
00:14:07,840 --> 00:14:20,040
 And then we'll go to the next one. Subline paragraph 33.

190
00:14:20,040 --> 00:14:22,560
 Now please remember the Pali

191
00:14:22,560 --> 00:14:30,400
 war char. Su gata because the the English translation sub

192
00:14:30,400 --> 00:14:34,320
line will not make much sense here.

193
00:14:34,320 --> 00:14:44,560
 So su gata. Su means here in the first the first meaning is

194
00:14:44,560 --> 00:14:48,400
 good. So su means good and gata means

195
00:14:48,400 --> 00:14:54,350
 going. So su gata means good going. That means Buddha has a

196
00:14:54,350 --> 00:14:56,560
 good going that is called Buddha is

197
00:14:56,560 --> 00:15:01,820
 that is why Buddha is called su gata because his going is

198
00:15:01,820 --> 00:15:04,960
 purified and blameless. And

199
00:15:04,960 --> 00:15:09,430
 what is that? That is a noble path. So noble eightfold path

200
00:15:09,430 --> 00:15:14,400
 is called he has a good good going.

201
00:15:16,560 --> 00:15:21,900
 So that is the first meaning. Su gata. Now the second

202
00:15:21,900 --> 00:15:23,280
 meaning is also

203
00:15:23,280 --> 00:15:32,030
 gone to an excellent place. Now bia gata means gone. Not

204
00:15:32,030 --> 00:15:34,320
 like in the first meaning. In the first

205
00:15:34,320 --> 00:15:39,270
 meaning gata means going. But in the second meaning gata

206
00:15:39,270 --> 00:15:43,120
 means gone to. Su means an excellent place.

207
00:15:43,120 --> 00:15:46,520
 So one who has gone to an excellent place is called su gata

208
00:15:46,520 --> 00:15:49,520
. Here excellent place is nibana.

209
00:15:49,520 --> 00:15:57,550
 Now the third meaning of having gone rightly. Now su can

210
00:15:57,550 --> 00:16:03,120
 mean also rightly. So who has gone rightly

211
00:16:03,840 --> 00:16:10,340
 is called su gata. And rightly here means not approaching

212
00:16:10,340 --> 00:16:15,040
 to to the extremes but following

213
00:16:15,040 --> 00:16:20,050
 the middle path. And the last one the fourth meaning

214
00:16:20,050 --> 00:16:24,720
 because of enunciating rightly. Now here

215
00:16:25,440 --> 00:16:31,350
 the original word is su gata with a d d as in David. And

216
00:16:31,350 --> 00:16:35,600
 that d is changed to t. It's a

217
00:16:35,600 --> 00:16:42,320
 polygrammatical explanation. So here it means su gata. Gata

218
00:16:42,320 --> 00:16:46,800
 means speaking or saying. And su means

219
00:16:46,800 --> 00:16:51,010
 rightly. So one who speaks rightly is called su gata. So

220
00:16:51,010 --> 00:16:53,520
 there are four meanings to this word.

221
00:16:54,240 --> 00:17:01,100
 So with regard to the fourth meaning, the su gata is quoted

222
00:17:01,100 --> 00:17:07,440
 here. Paragraph 35. Such speech as the

223
00:17:07,440 --> 00:17:10,570
 perfect one knows to be untrue and incorrect, conducive to

224
00:17:10,570 --> 00:17:12,640
 harm, and displeasing and unwelcome

225
00:17:12,640 --> 00:17:17,640
 to others, that he does not speak and so on. Now according

226
00:17:17,640 --> 00:17:20,640
 to the su gata there are six kinds of

227
00:17:20,640 --> 00:17:29,190
 speeches. And only two of them, buddhas, buddhas use. So

228
00:17:29,190 --> 00:17:31,920
 those six are mentioned here in

229
00:17:31,920 --> 00:17:39,130
 translation of the sodas. So in brief they mean speech

230
00:17:39,130 --> 00:17:42,800
 which is untrue, harmful, and displeasing.

231
00:17:43,760 --> 00:17:50,640
 Bodas do not use such speech. And then speech which is true

232
00:17:50,640 --> 00:17:53,520
 but harmful and displeasing to

233
00:17:53,520 --> 00:17:59,060
 the hearing. This also buddhas do not use. And the third

234
00:17:59,060 --> 00:18:03,440
 one is true beneficial. That means not

235
00:18:03,440 --> 00:18:10,770
 harming. True beneficial but displeasing to the listeners.

236
00:18:10,770 --> 00:18:16,080
 That buddhas use. But here in the

237
00:18:16,080 --> 00:18:20,810
 translation it is said, perfect one knows the time to

238
00:18:20,810 --> 00:18:24,800
 expand. That means if it is time to say such a

239
00:18:24,800 --> 00:18:27,960
 speech then bodas use this speech. So it may not be

240
00:18:27,960 --> 00:18:30,960
 pleasing to the listeners but if it is beneficial

241
00:18:30,960 --> 00:18:36,270
 and it is true then buddha will say. Why does the meaning

242
00:18:36,270 --> 00:18:38,560
 of harmful? What is harmful in this?

243
00:18:38,560 --> 00:18:48,400
 That means if you follow his words then you will get

244
00:18:48,400 --> 00:18:53,600
 benefits. Or if you follow his words you will

245
00:18:53,600 --> 00:18:59,360
 come to the goal. Opposite of that. You will come to

246
00:18:59,360 --> 00:19:08,640
 failure or something. Sometimes people talk to

247
00:19:08,640 --> 00:19:13,230
 other people. People give advice to other people. And those

248
00:19:13,230 --> 00:19:15,440
 advice may not be conducive to

249
00:19:17,280 --> 00:19:27,830
 success. So which is true beneficial and displeasing. Bodas

250
00:19:27,830 --> 00:19:31,680
 use such speeches. Then the fourth one

251
00:19:31,680 --> 00:19:38,510
 untrue, harmful but pleasing to other people. Bodas never

252
00:19:38,510 --> 00:19:43,760
 use this. And then true, harmful, pleasing.

253
00:19:46,000 --> 00:19:51,840
 No, bodas never use. And the last one true, beneficial and

254
00:19:51,840 --> 00:19:54,160
 pleasing to the listeners.

255
00:19:54,160 --> 00:19:59,190
 So bodas use only two kinds of speeches. True, beneficial,

256
00:19:59,190 --> 00:20:02,960
 displeasing or true, beneficial,

257
00:20:02,960 --> 00:20:08,050
 pleasing. The other bodas do not use. That is why bodas are

258
00:20:08,050 --> 00:20:12,000
 described as sugata. One who speaks rightly.

259
00:20:12,800 --> 00:20:20,660
 Then Noah of the world, local we go. You may have read this

260
00:20:20,660 --> 00:20:26,400
. It is something like

261
00:20:26,400 --> 00:20:33,370
 in the commentary. Something like a Buddhist cosmology. And

262
00:20:33,370 --> 00:20:34,960
 not all of them

263
00:20:37,760 --> 00:20:41,060
 can actually be found in the sutras. Some of them can be

264
00:20:41,060 --> 00:20:43,120
 found in the sutras. And others are

265
00:20:43,120 --> 00:20:49,720
 developed maybe later after the death of the Buddha. So you

266
00:20:49,720 --> 00:20:53,280
 may take them as just what people did.

267
00:20:53,280 --> 00:20:57,040
 The great result.

268
00:20:57,040 --> 00:21:02,720
 You may take them as true or not.

269
00:21:04,880 --> 00:21:13,120
 Because if we look at the world right now, according to the

270
00:21:13,120 --> 00:21:15,760
 knowledge of modern science

271
00:21:15,760 --> 00:21:24,420
 about the world, then they are very different from. And a

272
00:21:24,420 --> 00:21:30,320
 lot Indians are maybe, the western

273
00:21:30,320 --> 00:21:36,450
 people say Indians are a form of using numbers. Say 84,000

274
00:21:36,450 --> 00:21:39,840
 something like that. So here also

275
00:21:39,840 --> 00:21:44,200
 Noah of the world. Now there are three kinds of gods

276
00:21:44,200 --> 00:21:48,480
 mentioned here. The world of formations,

277
00:21:48,480 --> 00:21:52,870
 the world of beings and the world of location. You'll find

278
00:21:52,870 --> 00:21:54,960
 that in paragraph 37.

279
00:21:57,440 --> 00:22:00,860
 The world of formations, the world of beings and the world

280
00:22:00,860 --> 00:22:02,960
 of location. The world of formations

281
00:22:02,960 --> 00:22:09,530
 really means the world of both animate and inanimate things

282
00:22:09,530 --> 00:22:11,760
. Both beings and inanimate things.

283
00:22:11,760 --> 00:22:14,670
 The world of beings means just beings and the world of

284
00:22:14,670 --> 00:22:17,200
 location means outside world.

285
00:22:17,200 --> 00:22:24,790
 And so Buddha, I mean inanimate world. Like the earth and

286
00:22:24,790 --> 00:22:26,560
 mountain rivers and so on.

287
00:22:28,560 --> 00:22:34,640
 And if you can draw a diagram of what is mentioned here,

288
00:22:34,640 --> 00:22:39,200
 you will get a rough picture of

289
00:22:39,200 --> 00:22:45,860
 the Buddhist conception of the world and the other things.

290
00:22:45,860 --> 00:22:48,960
 I mean the suns and moons and

291
00:22:49,840 --> 00:22:55,560
 the universe. Then in paragraph 38, the first words

292
00:22:55,560 --> 00:22:58,160
 likewise I think should go.

293
00:22:58,160 --> 00:23:06,960
 Not likewise, but it should be for or maybe in detail

294
00:23:06,960 --> 00:23:11,200
 because paragraph 38 and those following

295
00:23:11,200 --> 00:23:17,760
 are the detailed description of how Buddha knows the world.

296
00:23:17,760 --> 00:23:24,480
 So one world being all beings subsused by nutriment and so

297
00:23:24,480 --> 00:23:25,120
 on.

298
00:23:25,120 --> 00:23:33,930
 Then paragraph 39, he knows all beings' habits. That is

299
00:23:33,930 --> 00:23:36,960
 very important. He knows all beings' habits.

300
00:23:36,960 --> 00:23:41,280
 Knows their inherent tendencies, knows their temperaments,

301
00:23:41,280 --> 00:23:43,200
 knows their bends, knows them

302
00:23:43,200 --> 00:23:46,830
 as with little dust in their eyes and with much dust on

303
00:23:46,830 --> 00:23:49,520
 their eyes, with keen faculties and with

304
00:23:49,520 --> 00:23:53,890
 dull faculties, with good behavior and with bad behavior,

305
00:23:53,890 --> 00:23:55,760
 easy to teach and hard to teach,

306
00:23:55,760 --> 00:23:59,790
 capable and incapable of achievement. Therefore, this world

307
00:23:59,790 --> 00:24:02,240
 of beings was known to him in all ways.

308
00:24:02,240 --> 00:24:10,070
 Only Bodas have this ability of knowing exactly the beings'

309
00:24:10,070 --> 00:24:12,640
 habits, their inherent tendencies,

310
00:24:12,640 --> 00:24:18,240
 and so on. Even the Arahants do not possess this ability.

311
00:24:18,240 --> 00:24:24,950
 And then Bodas' understanding of the physical world may be

312
00:24:24,950 --> 00:24:27,440
 developed.

313
00:24:27,440 --> 00:24:36,870
 There are some statements with regard to the physical world

314
00:24:36,870 --> 00:24:38,160
 and then maybe they are developed

315
00:24:38,800 --> 00:24:44,070
 later on. So this is the description of the world according

316
00:24:44,070 --> 00:24:45,920
 to say the Buddhists.

317
00:24:45,920 --> 00:24:52,690
 And also the footnote is very helpful and informative with

318
00:24:52,690 --> 00:24:54,320
 regard to this.

319
00:24:54,320 --> 00:25:02,690
 You will get a view of the Buddhist universe. You know, the

320
00:25:02,690 --> 00:25:06,720
 Buddhist universe is a wrong,

321
00:25:06,720 --> 00:25:10,230
 wrong thing, not like a cloak, but it is wrong. And it is

322
00:25:10,230 --> 00:25:13,280
 surrounded by what we call

323
00:25:13,280 --> 00:25:18,300
 the wall cycle mountains. So there are mountains around.

324
00:25:18,300 --> 00:25:22,080
 And then in this circle, there is water.

325
00:25:22,080 --> 00:25:27,130
 And also the four great islands, we call them the four

326
00:25:27,130 --> 00:25:30,400
 great islands, maybe the continents.

327
00:25:31,200 --> 00:25:35,260
 And each island has, this is in the books, believe it or

328
00:25:35,260 --> 00:25:40,480
 not, each island has 500 small islands.

329
00:25:40,480 --> 00:25:46,530
 And in the middle of this universe, there is the Mount Sih-

330
00:25:46,530 --> 00:25:47,520
Niru or Meru.

331
00:25:47,520 --> 00:25:55,480
 And it is not visible to human beings. And surrounding the

332
00:25:55,480 --> 00:25:57,760
 Meru, this mountain,

333
00:25:57,760 --> 00:26:03,190
 great mountain, there are seven concentric mountains, lower

334
00:26:03,190 --> 00:26:06,400
 mountain, seven. And in between

335
00:26:06,400 --> 00:26:12,200
 these seven, there is water. So they are called inner

336
00:26:12,200 --> 00:26:15,680
 oceans or something. There is the outer ocean,

337
00:26:15,680 --> 00:26:20,090
 and then there are inner ocean, seven inner oceans. And

338
00:26:20,090 --> 00:26:21,600
 they are the height,

339
00:26:24,080 --> 00:26:29,690
 the Sih-Niru is 84,000 leagues. And then the first 12

340
00:26:29,690 --> 00:26:33,200
 highest mountain, the surrounding mountain is

341
00:26:33,200 --> 00:26:37,480
 half that height. And then next one is half that height and

342
00:26:37,480 --> 00:26:39,840
 so on. But if you want to draw according

343
00:26:39,840 --> 00:26:45,250
 to scale, it is very difficult. I have tried it, but I

344
00:26:45,250 --> 00:26:49,920
 cannot. You have to draw a big drawing,

345
00:26:49,920 --> 00:26:52,980
 not on a small table because half height, half height, half

346
00:26:52,980 --> 00:26:54,720
 height, half height and so on.

347
00:26:54,720 --> 00:27:06,300
 And that is called one wall cycle, let's say one universe.

348
00:27:06,300 --> 00:27:09,760
 And suns and moon are said to

349
00:27:09,760 --> 00:27:17,630
 go round this, round the middle mountain at its half height

350
00:27:17,630 --> 00:27:21,520
. And so the sun and moon are only

351
00:27:21,520 --> 00:27:35,170
 44,000 leagues from the earth. And the size of the sun is

352
00:27:35,170 --> 00:27:38,720
 50 leagues and the size of the moon is 49

353
00:27:38,720 --> 00:27:42,160
 leagues. There is only one league difference between the

354
00:27:42,160 --> 00:27:44,160
 size of the moon and size of the sun.

355
00:27:44,160 --> 00:27:48,990
 And then there will be these stars and planets go round

356
00:27:48,990 --> 00:27:53,280
 them. And it is said that

357
00:27:53,280 --> 00:28:00,120
 when the sun goes round the middle, it throws a shadow,

358
00:28:00,120 --> 00:28:02,080
 right? And so

359
00:28:02,960 --> 00:28:07,460
 when three continents are light, one continent is dark and

360
00:28:07,460 --> 00:28:10,000
 that is night and the other is a day.

361
00:28:10,000 --> 00:28:22,320
 So only one quarter of the universe is dark and the three,

362
00:28:23,120 --> 00:28:32,230
 three, the other three parts are light at one time. And

363
00:28:32,230 --> 00:28:35,120
 there are four Greek islands and the

364
00:28:35,120 --> 00:28:41,340
 salt island is called Jambutiipa. And it is a place where

365
00:28:41,340 --> 00:28:48,160
 Buddhas and all white men and

366
00:28:48,160 --> 00:28:54,760
 fewer people are born. And that land is described as

367
00:28:54,760 --> 00:29:00,800
 something like white, white, white in one

368
00:29:00,800 --> 00:29:08,060
 side and then what about that? Sloping or something? Bec

369
00:29:08,060 --> 00:29:10,800
oming small in the other side.

370
00:29:11,520 --> 00:29:17,040
 So that fits with India. So India is Jambutiipa, the

371
00:29:17,040 --> 00:29:21,360
 southern island. So if India is southern

372
00:29:21,360 --> 00:29:26,910
 island, then Europe should be western island and China or

373
00:29:26,910 --> 00:29:30,000
 Japan should be eastern island.

374
00:29:30,000 --> 00:29:35,680
 And I think America should be the northern island. Because

375
00:29:35,680 --> 00:29:40,560
 when it is day in Asia, then it is night

376
00:29:40,560 --> 00:29:49,100
 here. So America could be the north great island which is

377
00:29:49,100 --> 00:29:55,200
 called Uttarakuru. So this is

378
00:29:55,200 --> 00:30:06,240
 the cosmology of Buddhism. Now there is one thing

379
00:30:07,200 --> 00:30:17,510
 on page 220. You see the paragraph number 43. Those lines,

380
00:30:17,510 --> 00:30:22,800
 one, two, three, four, five,

381
00:30:22,800 --> 00:30:27,500
 they are misplaced. The Wall Sphere Mountains line of

382
00:30:27,500 --> 00:30:30,880
 summit splinters down into the sea just

383
00:30:30,880 --> 00:30:34,850
 two and eighty thousand lakes and towers up in light beat

384
00:30:34,850 --> 00:30:38,240
 with and ringing one wall element

385
00:30:38,240 --> 00:30:43,740
 all around in its entirety. So these lines should be on

386
00:30:43,740 --> 00:30:47,840
 page 221 just above paragraph 44

387
00:30:47,840 --> 00:30:53,690
 and not here. I don't know why he put these here. Maybe by

388
00:30:53,690 --> 00:30:54,640
 mistake.

389
00:30:56,160 --> 00:31:00,880
 So these lines should be on the other page, page 221.

390
00:31:00,880 --> 00:31:13,610
 Now here you find the moon's disk is 49 links across and

391
00:31:13,610 --> 00:31:15,120
 the sun's disk is 50 links.

392
00:31:15,120 --> 00:31:21,430
 Therefore this wall of location was known to him in all

393
00:31:21,430 --> 00:31:24,240
 ways too. So he is no wall of walls because

394
00:31:24,240 --> 00:31:24,920
 he has seen the wall in all ways. Buddha is called a loka v

395
00:31:24,920 --> 00:31:30,320
idu, Noah of the world. Noah of the

396
00:31:30,320 --> 00:31:37,670
 world of beings or physical world and world of formations

397
00:31:37,670 --> 00:31:40,000
 or world of conditioned things.

398
00:31:40,000 --> 00:31:43,520
 That's why Buddha is called Noah of the Worlds.

399
00:31:48,640 --> 00:31:53,840
 And when we read the sub-commentaries we find more of these

400
00:31:53,840 --> 00:31:54,480
 descriptions.

401
00:31:54,480 --> 00:32:02,670
 I don't know where they come from. Actually not from the

402
00:32:02,670 --> 00:32:05,120
 sodas but I don't know what they

403
00:32:05,120 --> 00:32:11,140
 made. They may have got them from some writings of Hindus

404
00:32:11,140 --> 00:32:12,800
 because there are

405
00:32:12,800 --> 00:32:16,810
 such books in Hinduism and so they may have got them from

406
00:32:16,810 --> 00:32:17,280
 them.

407
00:32:17,920 --> 00:32:21,360
 But they are not important actually.

408
00:32:21,360 --> 00:32:29,810
 And one thing is, so one thing is in this book, Himalayas

409
00:32:29,810 --> 00:32:37,360
 is 500 leaks high. 500 leaks means

410
00:32:38,000 --> 00:32:46,170
 one leak is about eight miles. So how many miles? 4,000

411
00:32:46,170 --> 00:32:47,520
 miles high.

412
00:32:47,520 --> 00:33:01,680
 Sometimes they measure not by height, maybe by path. So to

413
00:33:01,680 --> 00:33:03,120
 read the sub-ment you have to

414
00:33:05,120 --> 00:33:11,930
 climb say a number of miles. Say more than five is how many

415
00:33:11,930 --> 00:33:17,120
 miles high? Maybe one mile or two miles

416
00:33:17,120 --> 00:33:23,760
 high. Not from the sea level, I mean measuring with the

417
00:33:23,760 --> 00:33:27,600
 poles, not measuring by path.

418
00:33:27,600 --> 00:33:34,360
 We say as the crow flies direct. This is not as the crow

419
00:33:34,360 --> 00:33:38,320
 flies. No, maybe that's why we're so many miles.

420
00:33:38,320 --> 00:33:44,740
 Say Himalayas is 500 miles high and so on. Yeah you know it

421
00:33:44,740 --> 00:33:49,600
 may not be how many miles high?

422
00:33:54,240 --> 00:33:58,240
 Amongst every this five miles high.

423
00:33:58,240 --> 00:34:06,970
 But do you think I may refer to this the world as we know

424
00:34:06,970 --> 00:34:09,600
 it or the universe as we know it or

425
00:34:09,600 --> 00:34:16,300
 some kind of a mythical type of a thing because as you

426
00:34:16,300 --> 00:34:20,720
 indicated, the mirror cannot be seen

427
00:34:22,320 --> 00:34:28,150
 as human. So it may be you know some kind of a symbolic

428
00:34:28,150 --> 00:34:30,000
 type of thing.

429
00:34:30,000 --> 00:34:32,240
 Yeah but you know

430
00:34:32,240 --> 00:34:44,640
 one statement contradicts another because it is said that

431
00:34:44,640 --> 00:34:48,080
 when three islands are like one island

432
00:34:48,080 --> 00:34:53,340
 is dark. So following this, this this world is what what

433
00:34:53,340 --> 00:34:56,960
 what is meant by the local in our books

434
00:34:56,960 --> 00:35:02,660
 because you know one part of the world is dark when the

435
00:35:02,660 --> 00:35:03,680
 others are light.

436
00:35:03,680 --> 00:35:07,920
 Not not exactly one quarter but something like that.

437
00:35:11,440 --> 00:35:16,740
 Maybe their their knowledge of geography is very limited in

438
00:35:16,740 --> 00:35:20,960
 those days and then there may be sieges

439
00:35:20,960 --> 00:35:26,060
 who just contemplate and then talked about these things and

440
00:35:26,060 --> 00:35:28,640
 they came to be accepted.

441
00:35:28,640 --> 00:35:36,400
 Nobody seemed to bother about measuring the the height of

442
00:35:36,400 --> 00:35:37,920
 Himalayas of Mount Everest

443
00:35:38,800 --> 00:35:44,830
 and they they had no way no means of measuring the height

444
00:35:44,830 --> 00:35:47,040
 of mountains in those days.

445
00:35:47,040 --> 00:35:51,200
 So they may be just measuring by that path.

446
00:35:51,200 --> 00:35:56,480
 But they are they are not not really important things.

447
00:35:56,480 --> 00:36:02,880
 Okay so next time

448
00:36:06,160 --> 00:36:10,240
 maybe another 20 pages.

449
00:36:10,240 --> 00:36:14,320
 Okay.

