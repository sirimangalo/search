1
00:00:00,000 --> 00:00:05,580
 Hi and welcome back to Ask a Monk. After a long break I'm

2
00:00:05,580 --> 00:00:07,520
 back and I'll be trying to give

3
00:00:07,520 --> 00:00:14,480
 regular teachings and regular updates to the Ask a Monk

4
00:00:14,480 --> 00:00:16,960
 program. I've got lots of questions to answer now

5
00:00:16,960 --> 00:00:20,260
 so hopefully I'll get around to some of them in the near

6
00:00:20,260 --> 00:00:23,760
 future. I'm settling down here in Sri

7
00:00:23,760 --> 00:00:28,370
 Lanka so you're going to see hopefully some of the Sri Lank

8
00:00:28,370 --> 00:00:31,360
an countryside in these next videos.

9
00:00:31,360 --> 00:00:38,730
 So the next question comes from Foster's Blue who says, "I

10
00:00:38,730 --> 00:00:42,080
 find my interest in reading and focus in

11
00:00:42,080 --> 00:00:47,170
 Zen, also Tibetan Buddhism and Christianity. Interest in

12
00:00:47,170 --> 00:00:50,080
 Islam and Sufi writings has caused

13
00:00:50,080 --> 00:00:53,460
 me to wonder if I will have to choose or may I learn from

14
00:00:53,460 --> 00:00:54,800
 various paths."

15
00:00:54,800 --> 00:01:01,440
 That's a really good question. It's one that was asked

16
00:01:01,440 --> 00:01:04,240
 quite often in the Buddhist time

17
00:01:04,240 --> 00:01:07,240
 because like today in that time there were many different

18
00:01:07,240 --> 00:01:09,040
 paths, many different teachers.

19
00:01:11,600 --> 00:01:23,630
 And I think it's important to agree that, I think most of

20
00:01:23,630 --> 00:01:26,560
 us can agree that it's unhealthy and

21
00:01:26,560 --> 00:01:30,730
 unuseful to dogmatically cling to a specific path and say

22
00:01:30,730 --> 00:01:34,400
 this path is right and all others are wrong.

23
00:01:35,280 --> 00:01:40,830
 And this isn't what the Buddha did. The Buddha himself was

24
00:01:40,830 --> 00:01:43,520
 a very open-minded person who said

25
00:01:43,520 --> 00:01:48,160
 some of the things that other teachers teach we agree with,

26
00:01:48,160 --> 00:01:49,680
 some of the things that other teachers

27
00:01:49,680 --> 00:01:53,900
 teach we don't agree with. And so he was never going to dog

28
00:01:53,900 --> 00:01:56,400
matically say only my teachings are

29
00:01:56,400 --> 00:02:00,170
 right and every other teaching is wrong. The point is that

30
00:02:00,170 --> 00:02:02,480
 you can find wisdom everywhere and as you

31
00:02:02,480 --> 00:02:06,130
 say all of these paths have something interesting and

32
00:02:06,130 --> 00:02:09,840
 something true in them. So on the one hand

33
00:02:09,840 --> 00:02:15,190
 it's obviously wrong to dogmatically stick to a specific

34
00:02:15,190 --> 00:02:19,280
 path. On the other hand what you often

35
00:02:19,280 --> 00:02:23,430
 see is nowadays as we start to learn more about the various

36
00:02:23,430 --> 00:02:26,000
 religious traditions and as they come

37
00:02:26,000 --> 00:02:30,990
 into contact you'll find people trying to smooth things

38
00:02:30,990 --> 00:02:34,000
 over and make things easy by saying that

39
00:02:34,000 --> 00:02:38,140
 all traditions are the same or all traditions have some

40
00:02:38,140 --> 00:02:41,040
 equal value. And this the Buddha didn't

41
00:02:41,040 --> 00:02:43,680
 agree with and this was one of the things that the Buddha

42
00:02:43,680 --> 00:02:50,560
 tried very clearly to rectify this

43
00:02:50,560 --> 00:02:54,140
 belief that all any teacher who said they were enlightened

44
00:02:54,140 --> 00:02:57,040
 was enlightened and so on or all

45
00:02:57,040 --> 00:03:02,680
 teachings should be given an equal footing. And this is

46
00:03:02,680 --> 00:03:06,160
 what we've done today. We've established

47
00:03:06,160 --> 00:03:10,260
 the various religious traditions up as the world's major

48
00:03:10,260 --> 00:03:12,320
 religions and we don't want to

49
00:03:12,320 --> 00:03:15,850
 make any distinction between them which of course is

50
00:03:15,850 --> 00:03:17,760
 unhealthy and improper.

51
00:03:18,640 --> 00:03:22,240
 We should see them for what they are and scientifically

52
00:03:22,240 --> 00:03:26,560
 take them apart and look at

53
00:03:26,560 --> 00:03:30,050
 them in all their pieces and come to see what are their

54
00:03:30,050 --> 00:03:32,720
 strengths and their weaknesses and compare

55
00:03:32,720 --> 00:03:36,580
 them. And this is really what I would recommend is that you

56
00:03:36,580 --> 00:03:38,560
 compare objectively the various

57
00:03:38,560 --> 00:03:43,010
 religious traditions. What the Buddha said was that if you

58
00:03:43,010 --> 00:03:45,680
 do this that you'll come to see that

59
00:03:45,680 --> 00:03:49,560
 his teaching is the most deep, profound and the one that

60
00:03:49,560 --> 00:03:52,160
 leads to the ultimate goal. So this is

61
00:03:52,160 --> 00:03:56,330
 a very bold statement and it's one that I would say most

62
00:03:56,330 --> 00:03:59,680
 religious traditions make. So the Buddha

63
00:03:59,680 --> 00:04:02,980
 was not unlike other teachers and saying that his was the

64
00:04:02,980 --> 00:04:05,040
 best. But what he said is that you

65
00:04:05,040 --> 00:04:08,920
 rather than accepting his teaching dogmatically this was an

66
00:04:08,920 --> 00:04:12,080
 inappropriate thing to do. One should

67
00:04:12,880 --> 00:04:16,010
 examine objectively all of the religious traditions and

68
00:04:16,010 --> 00:04:18,320
 decide which one is best. The Buddha simply

69
00:04:18,320 --> 00:04:22,630
 said that you'll come to see that the most comprehensive

70
00:04:22,630 --> 00:04:26,080
 objective scientific and practical

71
00:04:26,080 --> 00:04:31,880
 in terms of verifiable in terms of bringing results was the

72
00:04:31,880 --> 00:04:34,000
 Buddha's teaching.

73
00:04:34,000 --> 00:04:39,190
 So we should never simply try to pick and choose from

74
00:04:39,190 --> 00:04:42,400
 various religious traditions.

75
00:04:42,400 --> 00:04:45,230
 We should understand that it could very well be that some

76
00:04:45,230 --> 00:04:47,200
 of the teachers or the leaders of these

77
00:04:47,200 --> 00:04:50,710
 various religious traditions were not on the same level.

78
00:04:50,710 --> 00:04:53,040
 That just because someone is the leader of

79
00:04:53,040 --> 00:04:58,760
 a religion or the leader of some group or because it is

80
00:04:58,760 --> 00:05:02,000
 well liked by the majority of the people in

81
00:05:02,000 --> 00:05:05,690
 the world or followed by a large number of people. It doesn

82
00:05:05,690 --> 00:05:10,320
't mean that it is true. It's possible that

83
00:05:10,320 --> 00:05:13,280
 people are diluting themselves. It's possible that the

84
00:05:13,280 --> 00:05:15,760
 majority of people are following the wrong path.

85
00:05:15,760 --> 00:05:19,190
 And objectively and scientifically we should never take

86
00:05:19,190 --> 00:05:22,640
 something simply on faith or even on

87
00:05:22,640 --> 00:05:28,280
 the acceptance by the majority. We should study it, examine

88
00:05:28,280 --> 00:05:32,080
 it and try to see where it leads

89
00:05:32,800 --> 00:05:38,290
 and what it brings and practice it and see for ourselves

90
00:05:38,290 --> 00:05:40,240
 what it leads to.

91
00:05:40,240 --> 00:05:46,220
 The point being that if you pick and choose from various

92
00:05:46,220 --> 00:05:48,640
 traditions, now supposing that

93
00:05:48,640 --> 00:05:52,640
 certain teachings are not in line with the truths, supp

94
00:05:52,640 --> 00:05:54,320
osing that there are certain teachings that

95
00:05:54,320 --> 00:05:56,930
 are leading you in the wrong direction, then they're going

96
00:05:56,930 --> 00:05:58,240
 to come in conflict with those

97
00:05:58,240 --> 00:06:00,990
 teachings that are leading you to the truth, leading you to

98
00:06:00,990 --> 00:06:02,960
 reality, leading you to understanding,

99
00:06:02,960 --> 00:06:09,520
 to enlightenment. So rather than simply picking and

100
00:06:09,520 --> 00:06:11,840
 choosing, one should eventually

101
00:06:11,840 --> 00:06:16,630
 either make up one's mind that none of the teachings, no

102
00:06:16,630 --> 00:06:18,960
 teacher and none of the teachings

103
00:06:18,960 --> 00:06:21,680
 that exist in the world, lead one to the ultimate

104
00:06:21,680 --> 00:06:24,880
 realization of the truth and have a full and

105
00:06:24,880 --> 00:06:28,660
 complete path for me to follow and therefore I should make

106
00:06:28,660 --> 00:06:31,040
 do with what I have and find the true

107
00:06:31,040 --> 00:06:35,130
 path myself. Or accept that one of the teachings, one of

108
00:06:35,130 --> 00:06:37,920
 the paths that one of the teachers that

109
00:06:37,920 --> 00:06:42,500
 has taught since the beginning of time did have the right

110
00:06:42,500 --> 00:06:44,960
 answer and did teach the

111
00:06:44,960 --> 00:06:49,740
 way out of suffering and the way to true realization of

112
00:06:49,740 --> 00:06:51,200
 reality and understanding

113
00:06:51,200 --> 00:06:55,810
 enlightenment and so on and follow that path once you have

114
00:06:55,810 --> 00:06:58,560
 explored and understood and examined.

115
00:06:58,560 --> 00:07:02,340
 Because if you find such a path then there's no reason to

116
00:07:02,340 --> 00:07:03,920
 take from other paths

117
00:07:03,920 --> 00:07:17,500
 except sort of ancillary to sort of somehow add something

118
00:07:17,500 --> 00:07:18,880
 to the main path

119
00:07:20,480 --> 00:07:23,420
 rather than going about coming and going from one path to

120
00:07:23,420 --> 00:07:25,120
 the other. You have the right path

121
00:07:25,120 --> 00:07:31,090
 in front of you, why would you pick and choose when quite

122
00:07:31,090 --> 00:07:33,120
 likely these other paths that you've

123
00:07:33,120 --> 00:07:37,120
 decided that are not leading to ultimate reality and

124
00:07:37,120 --> 00:07:42,720
 ultimate understanding are just going to

125
00:07:42,720 --> 00:07:47,260
 conflict with this one. Now it might be that some people

126
00:07:47,260 --> 00:07:50,240
 say that there are different paths to the

127
00:07:50,240 --> 00:07:54,510
 same goal and I'm willing to accept this that there very

128
00:07:54,510 --> 00:07:57,200
 well could be different paths to the

129
00:07:57,200 --> 00:08:01,810
 same goal but it's important not to get too caught up in

130
00:08:01,810 --> 00:08:05,200
 this sort of idea of different paths in the

131
00:08:05,200 --> 00:08:08,740
 same goal which we hear a lot because you know it's not

132
00:08:08,740 --> 00:08:11,360
 really we're not talking about a mountain

133
00:08:11,360 --> 00:08:13,710
 where you can approach it from all different types besides

134
00:08:13,710 --> 00:08:14,960
 we're talking about reality

135
00:08:15,680 --> 00:08:19,560
 and reality is one there can only be one reality either it

136
00:08:19,560 --> 00:08:21,920
's true or it's not either it's reality

137
00:08:21,920 --> 00:08:25,830
 or it's not there can be a million falsehoods infinite

138
00:08:25,830 --> 00:08:28,880
 number of false paths and this is why

139
00:08:28,880 --> 00:08:31,790
 we see such a diversity of paths because we're seeing a lot

140
00:08:31,790 --> 00:08:33,360
 of paths that are teaching something

141
00:08:33,360 --> 00:08:37,280
 based on speculation that there is this this god or this

142
00:08:37,280 --> 00:08:39,840
 heaven or so on and so on and as a result

143
00:08:39,840 --> 00:08:45,800
 they they they conflict because because of the multiplicity

144
00:08:45,800 --> 00:08:49,120
 of of falsehood of illusion of mental

145
00:08:49,120 --> 00:08:52,290
 creation things that have nothing to do with reality can be

146
00:08:52,290 --> 00:08:54,880
 multiple but reality has to by

147
00:08:54,880 --> 00:09:00,140
 its very nature by its very definition be one be that which

148
00:09:00,140 --> 00:09:03,280
 is real and the truth has to be one so

149
00:09:03,280 --> 00:09:07,320
 the realization of the truth and true peace and happiness

150
00:09:07,320 --> 00:09:10,480
 has to come through understanding and

151
00:09:10,480 --> 00:09:17,080
 through somehow harmonizing oneself with this reality so

152
00:09:17,080 --> 00:09:19,760
 that one acts based on understanding

153
00:09:19,760 --> 00:09:23,220
 based on those acts in speech and thought that will lead

154
00:09:23,220 --> 00:09:25,600
 one to peace happiness and freedom from

155
00:09:25,600 --> 00:09:29,910
 suffering so whereas there may be many different religious

156
00:09:29,910 --> 00:09:32,560
 traditions that are saying the same

157
00:09:32,560 --> 00:09:36,330
 thing in different in different ways it's quite more likely

158
00:09:36,330 --> 00:09:38,480
 that many of them are and and as we

159
00:09:38,480 --> 00:09:41,870
 see many of them are teaching um the falsehood or teaching

160
00:09:41,870 --> 00:09:44,080
 something based on faith or based on

161
00:09:44,080 --> 00:09:50,360
 illusion so they may have something that that is beneficial

162
00:09:50,360 --> 00:09:54,080
 um but they the the core doctrine or

163
00:09:54,080 --> 00:10:00,450
 the core theory or dogma um is is based on a supposition or

164
00:10:00,450 --> 00:10:03,680
 a belief and not based on ultimate

165
00:10:03,680 --> 00:10:10,180
 reality so i would say don't believe any any one path as

166
00:10:10,180 --> 00:10:14,640
 being the truth just dogmatically but

167
00:10:15,360 --> 00:10:21,550
 don't pick and choose unless your plan is to eventually

168
00:10:21,550 --> 00:10:24,880
 create or find one path for yourself

169
00:10:24,880 --> 00:10:27,560
 that that is going to lead you to peace happiness and

170
00:10:27,560 --> 00:10:30,240
 freedom from suffering and the other thing is

171
00:10:30,240 --> 00:10:35,760
 don't um i would not accept a post-modernist view of things

172
00:10:35,760 --> 00:10:38,800
 where you say what is good for me is

173
00:10:38,800 --> 00:10:42,800
 good for me or what i think is good is good and just

174
00:10:42,800 --> 00:10:46,000
 because we agree with something or something

175
00:10:46,000 --> 00:10:49,220
 agrees with us therefore it is right for us this is the

176
00:10:49,220 --> 00:10:51,440
 final warning that i would give that

177
00:10:51,440 --> 00:10:55,570
 picking and choosing is nice when when you're learning or

178
00:10:55,570 --> 00:10:57,200
 when you're trying to

179
00:10:57,200 --> 00:11:01,920
 figure out what sort of a path you're looking for and what

180
00:11:01,920 --> 00:11:02,880
 what is the

181
00:11:02,880 --> 00:11:06,020
 what are the differences between the paths and trying to

182
00:11:06,020 --> 00:11:07,680
 find a framework for the path

183
00:11:08,560 --> 00:11:13,000
 but if eventually all you're doing is simply uh augmenting

184
00:11:13,000 --> 00:11:15,840
 or verifying your own beliefs your

185
00:11:15,840 --> 00:11:19,560
 own views your own opinions your own ego really um and

186
00:11:19,560 --> 00:11:22,480
 picking up things simply because they

187
00:11:22,480 --> 00:11:26,160
 agree with your way of life agree with the things that you

188
00:11:26,160 --> 00:11:29,200
 you are attached to or you cling to or

189
00:11:29,200 --> 00:11:34,910
 you believe in then then there's no spiritual development

190
00:11:34,910 --> 00:11:39,120
 at all you're simply um reifying your

191
00:11:39,120 --> 00:11:46,680
 your ego your your defiance your um attachments your

192
00:11:46,680 --> 00:11:49,200
 clinging state of mind and you're not

193
00:11:49,200 --> 00:11:52,210
 learning to understand something you're not broadening your

194
00:11:52,210 --> 00:11:53,840
 horizons you're not developing

195
00:11:53,840 --> 00:12:00,750
 spiritually so eventually you're going to have to examine

196
00:12:00,750 --> 00:12:04,640
 your own beliefs and and your own views

197
00:12:04,640 --> 00:12:08,150
 and and hold them up against the various religious

198
00:12:08,150 --> 00:12:11,040
 traditions and come to see

199
00:12:11,040 --> 00:12:18,220
 which one is objectively true which one objectively lead

200
00:12:18,220 --> 00:12:21,840
 does lead to peace happiness

201
00:12:21,840 --> 00:12:24,440
 freedom from suffering spiritual development enlightening

202
00:12:24,440 --> 00:12:27,520
 themselves so i hope that helped

203
00:12:27,520 --> 00:12:33,420
 and just a caution not to not to pick and choose it's uh

204
00:12:33,420 --> 00:12:36,880
 the spiritual path is not a buffet

205
00:12:36,880 --> 00:12:41,590
 something that you have to dive into and follow to your

206
00:12:41,590 --> 00:12:45,600
 utmost and and really have to give up

207
00:12:45,600 --> 00:12:48,300
 all of all the things that you believe in all the things

208
00:12:48,300 --> 00:12:50,560
 that you hold on to eventually you're

209
00:12:50,560 --> 00:12:53,990
 going to have to let go of everything so that you can come

210
00:12:53,990 --> 00:12:56,160
 to be free totally and completely

211
00:12:56,160 --> 00:13:02,720
 and so as a result you really do need to pick one specific

212
00:13:02,720 --> 00:13:05,840
 path and follow it diligently

213
00:13:05,840 --> 00:13:08,870
 not going a short ways and then backing off and going up

214
00:13:08,870 --> 00:13:11,360
 another path and then backing off you're

215
00:13:11,360 --> 00:13:14,970
 going to have to figure out which one which path is right

216
00:13:14,970 --> 00:13:17,280
 for you whether it's some path that you've

217
00:13:17,280 --> 00:13:20,130
 worked out on your own or whether it's one of the paths

218
00:13:20,130 --> 00:13:23,120
 that has been established that you realize

219
00:13:23,120 --> 00:13:29,470
 is the truth and is based on reality and has a verifiable

220
00:13:29,470 --> 00:13:34,000
 goal and conclusion and has a

221
00:13:34,000 --> 00:13:37,400
 detailed teaching that leads you to that conclusion which i

222
00:13:37,400 --> 00:13:39,360
 would recommend is the buddhist teaching

223
00:13:39,360 --> 00:13:42,950
 that's why i'm following but the other thing i would say is

224
00:13:42,950 --> 00:13:45,680
 that you probably i would boast i

225
00:13:45,680 --> 00:13:50,230
 suppose that you you won't find such a a claim in the other

226
00:13:50,230 --> 00:13:52,960
 religions where whereas most paths will

227
00:13:52,960 --> 00:13:57,990
 say something like this or ours is the best you won't find

228
00:13:57,990 --> 00:14:02,720
 this objective explanation where you

229
00:14:02,720 --> 00:14:08,450
 say find the one that is most objective and most verifiable

230
00:14:08,450 --> 00:14:11,760
 in the year and now and most scientific

231
00:14:11,760 --> 00:14:14,700
 and that's ours you you'll you'll find instead where people

232
00:14:14,700 --> 00:14:16,240
 say believe this this is what we

233
00:14:16,240 --> 00:14:18,980
 believe it's the truth and and if you don't believe it this

234
00:14:18,980 --> 00:14:21,760
 and this and this is going to happen and

235
00:14:21,760 --> 00:14:28,130
 and one that's based on inference or extrapolation where

236
00:14:28,130 --> 00:14:30,240
 you have some special experience and you

237
00:14:30,240 --> 00:14:33,670
 say that's this god or that's that god or that's you know

238
00:14:33,670 --> 00:14:36,080
 heaven or or or this realization or that

239
00:14:36,080 --> 00:14:40,920
 realization um the buddhist teaching is purely scientific

240
00:14:40,920 --> 00:14:44,560
 it's something that allows you to

241
00:14:44,560 --> 00:14:48,710
 realize the truth without extrapolating or or jumping to

242
00:14:48,710 --> 00:14:50,800
 any conclusion whatsoever

243
00:14:50,800 --> 00:14:55,570
 conclusions that you'll draw will be verifiable and will be

244
00:14:55,570 --> 00:14:59,520
 based on an exact one-to-one correlation

245
00:14:59,520 --> 00:15:02,360
 with your experiences when you experience something you'll

246
00:15:02,360 --> 00:15:04,160
 have the conclusion that that

247
00:15:04,160 --> 00:15:07,880
 that is so it is like that because you've experienced it as

248
00:15:07,880 --> 00:15:10,640
 opposed to extrapolating upon it so my

249
00:15:10,640 --> 00:15:15,960
 post i would say of buddhism is that it it is thus and it

250
00:15:15,960 --> 00:15:19,520
 is the teaching that is most complete and

251
00:15:19,520 --> 00:15:27,680
 has the has the most detailed and accurate and scientific

252
00:15:27,680 --> 00:15:31,440
 teaching that that it has verifiable

253
00:15:31,440 --> 00:15:36,360
 results and leads to a verifiable goal and and works for

254
00:15:36,360 --> 00:15:39,440
 those who put their effort into it

255
00:15:39,440 --> 00:15:44,880
 so um i guess ultimately my answer is no you don't have to

256
00:15:44,880 --> 00:15:47,120
 pick you don't have to pick and choose in

257
00:15:47,120 --> 00:15:49,530
 the end you're going to come to buddhism if you've if you

258
00:15:49,530 --> 00:15:51,440
've been objective and if you've come to

259
00:15:51,440 --> 00:15:54,630
 and come if you've been honest with yourself that

260
00:15:54,630 --> 00:15:57,280
 eventually you'll come to realize that

261
00:15:57,280 --> 00:16:00,080
 what the buddha was an enlightened being and that's anyone

262
00:16:00,080 --> 00:16:02,400
 who is enlightened will teach these things

263
00:16:02,400 --> 00:16:06,950
 because this is the truth so really the the word buddhism

264
00:16:06,950 --> 00:16:11,040
 in fact is is simply a word that means

265
00:16:11,040 --> 00:16:14,200
 the teaching of the enlightened one so it's the the meaning

266
00:16:14,200 --> 00:16:17,360
 is that this teaching is the teaching

267
00:16:17,360 --> 00:16:21,780
 of those people who have for themselves come to realize the

268
00:16:21,780 --> 00:16:24,400
 truth and it's not because of faith

269
00:16:24,400 --> 00:16:28,330
 it's not it's not a god that came down and told them these

270
00:16:28,330 --> 00:16:30,560
 things or or it wasn't given from

271
00:16:30,560 --> 00:16:34,400
 anywhere it was something that was realized for themselves

272
00:16:34,400 --> 00:16:36,880
 so this is therefore the point of saying

273
00:16:36,880 --> 00:16:39,780
 that not all religious traditions are the same because if

274
00:16:39,780 --> 00:16:41,680
 someone is not enlightened they might

275
00:16:41,680 --> 00:16:45,100
 still teach and they might still gain acceptance from a

276
00:16:45,100 --> 00:16:47,680
 large number of people but that doesn't

277
00:16:47,680 --> 00:16:49,950
 mean that they're enlightened that doesn't mean that their

278
00:16:49,950 --> 00:16:51,360
 teaching is right and good if they're

279
00:16:51,360 --> 00:16:54,600
 not not yet enlightened this is the criteria of being buddh

280
00:16:54,600 --> 00:16:57,440
ism or not buddhism something is buddhism

281
00:16:57,440 --> 00:17:00,700
 if it's based on the teaching of someone who is totally

282
00:17:00,700 --> 00:17:02,560
 perfectly enlightened someone who has

283
00:17:02,560 --> 00:17:06,320
 come to realize the truth the whole truth for themselves

284
00:17:06,320 --> 00:17:08,640
 and has come to be fully free from

285
00:17:08,640 --> 00:17:11,870
 clinging free from craving free from suffering so i hope

286
00:17:11,870 --> 00:17:15,840
 that helps and i'm sure it's not exactly

287
00:17:15,840 --> 00:17:19,450
 what you're looking for i know we like to pick and choose

288
00:17:19,450 --> 00:17:24,720
 and and be free in our pursuits this is why

289
00:17:24,720 --> 00:17:29,010
 there's the rise of post-modernism but there you have it

290
00:17:29,010 --> 00:17:31,840
 this is ask a monk thanks for tuning in and

291
00:17:31,840 --> 00:17:41,840
 wish you all the best

