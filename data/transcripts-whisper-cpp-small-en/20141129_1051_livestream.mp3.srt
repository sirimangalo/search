1
00:00:00,000 --> 00:00:13,690
 Alright, so this is the first test of our fully functional

2
00:00:13,690 --> 00:00:18,720
 online live Dhamma, which

3
00:00:18,720 --> 00:00:23,040
 is a companion for our meditation group.

4
00:00:23,040 --> 00:00:29,940
 The idea being that maybe before your meditation session or

5
00:00:29,940 --> 00:00:34,280
 maybe after your meditation session,

6
00:00:34,280 --> 00:00:41,800
 it's nice to listen to Dhamma, listen to something

7
00:00:41,800 --> 00:00:47,440
 encouraging and insightful, hopefully useful

8
00:00:47,440 --> 00:01:02,040
 in your meditation practice.

9
00:01:02,040 --> 00:01:07,580
 So probably you're listening to this, having already been

10
00:01:07,580 --> 00:01:11,160
 recorded and placed on our website,

11
00:01:11,160 --> 00:01:18,270
 but from time to time I'm going to hopefully be giving

12
00:01:18,270 --> 00:01:22,720
 these live feeds as a supplement

13
00:01:22,720 --> 00:01:25,600
 to our meditation group.

14
00:01:25,600 --> 00:01:29,910
 And you can either listen to them on the website,

15
00:01:29,910 --> 00:01:34,160
 meditation.siri-mongolo.org, or using the

16
00:01:34,160 --> 00:01:37,400
 Android app.

17
00:01:37,400 --> 00:01:43,690
 So here I am today, sitting, watching the sun go down at

18
00:01:43,690 --> 00:01:47,000
 the edge of this fairly large

19
00:01:47,000 --> 00:01:58,060
 artificial reservoir, water reservoir behind what Patatzi

20
00:01:58,060 --> 00:02:03,240
 jumped home in Chiang Mai.

21
00:02:03,240 --> 00:02:15,400
 We talk about being invincible.

22
00:02:15,400 --> 00:02:20,360
 This is sort of a key to the Buddha's teaching.

23
00:02:20,360 --> 00:02:23,360
 Unshakeable.

24
00:02:23,360 --> 00:02:41,080
 "Akupa mai wimutti" - "My deliverance is unshakeable."

25
00:02:41,080 --> 00:02:50,250
 This is more familiar to Buddhists, where the Buddha said,

26
00:02:50,250 --> 00:02:54,560
 "Putasaloka dhammihi jitangyasana

27
00:02:54,560 --> 00:02:58,360
 kampati."

28
00:02:58,360 --> 00:03:05,290
 When touched by the worldly dhammas, worldly realities, or

29
00:03:05,290 --> 00:03:09,320
 the realities of life, jitangyasana

30
00:03:09,320 --> 00:03:12,160
 kampati, the mind of which person?

31
00:03:12,160 --> 00:03:20,660
 Mind of whatever person is not kampati, not wavers, doesn't

32
00:03:20,660 --> 00:03:23,880
 waver, nakampati.

33
00:03:23,880 --> 00:03:37,860
 This is the greatest blessing.

34
00:03:37,860 --> 00:03:48,550
 Because it's one of the defining criteria we have when we

35
00:03:48,550 --> 00:03:53,440
 are given a proposed path

36
00:03:53,440 --> 00:04:03,600
 of practice, or view of the nature of life, or view of the

37
00:04:03,600 --> 00:04:07,280
 right way to live.

38
00:04:07,280 --> 00:04:19,120
 When we discard those paths that rely upon specific

39
00:04:19,120 --> 00:04:24,080
 requirements for happiness, or for

40
00:04:24,080 --> 00:04:30,930
 peace, or for their rightness, or something to be right, it

41
00:04:30,930 --> 00:04:33,840
's not enough to say that it

42
00:04:33,840 --> 00:04:45,590
 leads you to a specific state, or it allows you to attain a

43
00:04:45,590 --> 00:04:48,920
 specific goal for as long

44
00:04:48,920 --> 00:04:57,530
 as the potential exists for one to be shaken, for one to

45
00:04:57,530 --> 00:05:02,600
 fall from that attainment, or lose

46
00:05:02,600 --> 00:05:17,880
 that state, or that goal.

47
00:05:17,880 --> 00:05:24,460
 When someone comes to you and explains to you a goal, you

48
00:05:24,460 --> 00:05:27,860
 have to be clear as to whether

49
00:05:27,860 --> 00:05:38,190
 that goal is unshakable, or whether it's dependent upon

50
00:05:38,190 --> 00:05:42,120
 certain criteria.

51
00:05:42,120 --> 00:06:01,800
 When you lose the things that support your attainment, or

52
00:06:01,800 --> 00:06:08,850
 support your peace, your happiness, then you fall back into

53
00:06:08,850 --> 00:06:10,560
 suffering.

54
00:06:10,560 --> 00:06:15,780
 So the defining feature of the Buddha's teaching is that it

55
00:06:15,780 --> 00:06:18,360
 renders you invincible.

56
00:06:18,360 --> 00:06:23,460
 And it's a defining factor of the meditation practice as

57
00:06:23,460 --> 00:06:26,040
 well, especially the meditation

58
00:06:26,040 --> 00:06:28,560
 practice really.

59
00:06:28,560 --> 00:06:32,560
 Because many meditation practices will allow you to attain

60
00:06:32,560 --> 00:06:34,640
 a specific state of peace, of

61
00:06:34,640 --> 00:06:40,240
 happiness, of greatness, allow you to see things far away,

62
00:06:40,240 --> 00:06:43,200
 or read people's minds, enter

63
00:06:43,200 --> 00:06:48,640
 into very deep and tranquil states.

64
00:06:48,640 --> 00:06:56,670
 And none of these things can be truly considered a goal, or

65
00:06:56,670 --> 00:07:01,840
 an end in and of themselves, because

66
00:07:01,840 --> 00:07:03,040
 they're shakable.

67
00:07:03,040 --> 00:07:05,680
 They can be shaken from them.

68
00:07:05,680 --> 00:07:13,480
 They only last as long as the cause exists.

69
00:07:13,480 --> 00:07:17,720
 When the thing that's causing it to, when the power behind

70
00:07:17,720 --> 00:07:19,720
 it is exhausted, then the

71
00:07:19,720 --> 00:07:28,640
 experience ceases.

72
00:07:28,640 --> 00:07:36,040
 So, we try to find a meditation practice that addresses

73
00:07:36,040 --> 00:07:41,120
 this, and also addresses the ordinary

74
00:07:41,120 --> 00:07:42,120
 state.

75
00:07:42,120 --> 00:07:50,970
 The problem with not meditating is that you're constantly

76
00:07:50,970 --> 00:07:55,040
 in danger of suffering.

77
00:07:55,040 --> 00:07:59,270
 You're constantly in danger of being perturbed, being

78
00:07:59,270 --> 00:08:02,480
 shaken, being upset by the vicissitudes

79
00:08:02,480 --> 00:08:03,480
 of life.

80
00:08:03,480 --> 00:08:05,480
 Lokadhammi, the Lokadham.

81
00:08:05,480 --> 00:08:11,950
 Lokadhamma are the eight vicissitudes of life, for good and

82
00:08:11,950 --> 00:08:15,120
 for bad, that you can never say

83
00:08:15,120 --> 00:08:16,720
 you're just going to get the good one.

84
00:08:16,720 --> 00:08:30,770
 So gain and loss, praise and blame, happiness and suffering

85
00:08:30,770 --> 00:08:31,640
.

86
00:08:31,640 --> 00:08:52,160
 Gain and loss, praise and blame.

87
00:08:52,160 --> 00:08:56,970
 And praise and blame, and status, social status, gain of

88
00:08:56,970 --> 00:09:01,240
 social status and loss of social status.

89
00:09:01,240 --> 00:09:05,030
 These things that in the world are considered to be good

90
00:09:05,030 --> 00:09:09,240
 and considered to be desirable,

91
00:09:09,240 --> 00:09:13,520
 also subject to their opposites.

92
00:09:13,520 --> 00:09:18,860
 Anyone who desires or clings to any of the good things in

93
00:09:18,860 --> 00:09:21,880
 life, opens themselves up,

94
00:09:21,880 --> 00:09:28,000
 makes themselves vulnerable to loss, to the suffering that

95
00:09:28,000 --> 00:09:30,200
 comes from losing.

96
00:09:30,200 --> 00:09:36,510
 So meditation practice tries to address this issue of

97
00:09:36,510 --> 00:09:41,360
 vulnerability and render you invincible.

98
00:09:41,360 --> 00:09:53,650
 And this invincibility comes from clarifying in one's mind

99
00:09:53,650 --> 00:09:57,360
 and establishing in one's,

100
00:09:57,360 --> 00:10:07,050
 establishing oneself in the realm of the very building

101
00:10:07,050 --> 00:10:10,600
 blocks of reality.

102
00:10:10,600 --> 00:10:16,340
 Breaking reality up and down into its constituent parts and

103
00:10:16,340 --> 00:10:19,640
 finding the common base elements

104
00:10:19,640 --> 00:10:22,520
 for everything.

105
00:10:22,520 --> 00:10:27,080
 The theory being once you understand the things that make

106
00:10:27,080 --> 00:10:30,760
 up everything, and once you master,

107
00:10:30,760 --> 00:10:35,260
 you attain mastery over these things, then it stands to

108
00:10:35,260 --> 00:10:36,920
 reason that you've solved the

109
00:10:36,920 --> 00:10:41,680
 problem, you've solved the entire problem.

110
00:10:41,680 --> 00:10:49,350
 You can't possibly hope to learn the specifics of every

111
00:10:49,350 --> 00:10:54,280
 situation, but the key is that you

112
00:10:54,280 --> 00:10:58,200
 don't need to, or the idea here is to find a way not to

113
00:10:58,200 --> 00:10:59,640
 have to do that.

114
00:10:59,640 --> 00:11:02,280
 So that you don't have to gain the experience of how to

115
00:11:02,280 --> 00:11:03,680
 deal with this person, this type

116
00:11:03,680 --> 00:11:07,960
 of person, that type of person, this type of situation.

117
00:11:07,960 --> 00:11:12,040
 So when you go to Canada, you have to be, you have to act

118
00:11:12,040 --> 00:11:14,080
 in a certain way, and when

119
00:11:14,080 --> 00:11:19,600
 you go to Thailand, you have to act in another way.

120
00:11:19,600 --> 00:11:24,410
 And so if your solution is that, then your solution is

121
00:11:24,410 --> 00:11:25,360
 impossible.

122
00:11:25,360 --> 00:11:28,430
 And most people look at it this way, the idea of being

123
00:11:28,430 --> 00:11:30,720
 invincible is impossible, because

124
00:11:30,720 --> 00:11:39,160
 you can never learn enough about the specifics of life.

125
00:11:39,160 --> 00:11:42,850
 But science doesn't work that way, and so scientists, when

126
00:11:42,850 --> 00:11:44,440
 they want to understand the

127
00:11:44,440 --> 00:11:47,180
 nature of matter, they don't go around looking at all the

128
00:11:47,180 --> 00:11:48,920
 different types of matter, they

129
00:11:48,920 --> 00:11:56,080
 try to find patterns and underlying similarities.

130
00:11:56,080 --> 00:11:59,770
 So in the end, building blocks, because when you understand

131
00:11:59,770 --> 00:12:05,600
 the building blocks of matter,

132
00:12:05,600 --> 00:12:09,330
 you can then understand any kind of matter that you come

133
00:12:09,330 --> 00:12:10,120
 across.

134
00:12:10,120 --> 00:12:15,960
 Even any elements, so they're still finding new elements,

135
00:12:15,960 --> 00:12:18,700
 but that doesn't require a new

136
00:12:18,700 --> 00:12:24,180
 type of understanding, because the basic subatomic

137
00:12:24,180 --> 00:12:27,560
 particles are still the same.

138
00:12:27,560 --> 00:12:33,190
 So from a Buddhist point of view, from an experiential

139
00:12:33,190 --> 00:12:35,640
 point of view, it's the same.

140
00:12:35,640 --> 00:12:38,780
 When we take reality as being experience-based, when we're

141
00:12:38,780 --> 00:12:40,600
 concerned about our experiences

142
00:12:40,600 --> 00:12:45,050
 and our suffering and our happiness, we can still find the

143
00:12:45,050 --> 00:12:47,720
 building blocks of experience.

144
00:12:47,720 --> 00:12:50,600
 And once we have the building blocks of experience, then no

145
00:12:50,600 --> 00:12:53,160
 matter what experience we have, it

146
00:12:53,160 --> 00:12:57,470
 doesn't matter what's out there, we still approach it as an

147
00:12:57,470 --> 00:12:58,680
 experience.

148
00:12:58,680 --> 00:13:02,360
 We still relate to it as an experience.

149
00:13:02,360 --> 00:13:06,490
 There's no reality out there that we can relate to that isn

150
00:13:06,490 --> 00:13:08,040
't an experience.

151
00:13:08,040 --> 00:13:10,950
 Even if it's something you've never seen or heard or

152
00:13:10,950 --> 00:13:13,000
 smelled or taste or felt, it's still

153
00:13:13,000 --> 00:13:17,060
 a thought, and thought is an experience.

154
00:13:17,060 --> 00:13:26,490
 So these are the building blocks that we settle our

155
00:13:26,490 --> 00:13:31,680
 meditation practice on, the building blocks

156
00:13:31,680 --> 00:13:34,440
 of experience.

157
00:13:34,440 --> 00:13:37,390
 So briefly, what are the building blocks of experience that

158
00:13:37,390 --> 00:13:38,160
 we focus on?

159
00:13:38,160 --> 00:13:41,200
 These are what we call the five aggregates.

160
00:13:41,200 --> 00:13:45,160
 Five aggregates are a constituent experience.

161
00:13:45,160 --> 00:13:50,240
 There's the physical aspect, which we call rupa.

162
00:13:50,240 --> 00:13:55,420
 There's the sensation, which we call vedana, which means

163
00:13:55,420 --> 00:13:58,160
 the pleasure, the pain, or the

164
00:13:58,160 --> 00:13:59,160
 calm feeling.

165
00:13:59,160 --> 00:14:04,360
 It's associated with the experience.

166
00:14:04,360 --> 00:14:07,990
 Mostly a calm, unless it's physical, it's mostly physical

167
00:14:07,990 --> 00:14:10,040
 or mental, like physical at

168
00:14:10,040 --> 00:14:13,080
 the body, feeling or mental.

169
00:14:13,080 --> 00:14:15,800
 It's mostly a neutral feeling.

170
00:14:15,800 --> 00:14:21,660
 Then there's the recognition of the aspect of the

171
00:14:21,660 --> 00:14:23,600
 experience.

172
00:14:23,600 --> 00:14:26,530
 When you see something, you're recognition of it, when you

173
00:14:26,530 --> 00:14:28,160
 hear something, recognition

174
00:14:28,160 --> 00:14:33,020
 of it, or finding it similar to something else, there's

175
00:14:33,020 --> 00:14:35,640
 that aspect of experience.

176
00:14:35,640 --> 00:14:40,810
 The thoughts that arise based on the experience, this is

177
00:14:40,810 --> 00:14:44,080
 our judgments and our reactions to

178
00:14:44,080 --> 00:14:47,840
 the experience.

179
00:14:47,840 --> 00:14:53,060
 And finally, the mind, which experiences the consciousness,

180
00:14:53,060 --> 00:14:55,600
 which experiences the object,

181
00:14:55,600 --> 00:14:59,320
 whatever it is, seeing, hearing, smelling, just living.

182
00:14:59,320 --> 00:15:05,450
 So these five aggregates, the constituents of experience,

183
00:15:05,450 --> 00:15:08,160
 arise at the six senses.

184
00:15:08,160 --> 00:15:12,250
 If you want to remember anything about Buddhism, it's the

185
00:15:12,250 --> 00:15:15,040
 five aggregates and the six senses.

186
00:15:15,040 --> 00:15:17,870
 That's the basis of reality, because at each of the six

187
00:15:17,870 --> 00:15:20,440
 senses, these five aggregates arise.

188
00:15:20,440 --> 00:15:23,890
 When you see something, there's the physical, there's the

189
00:15:23,890 --> 00:15:25,880
 feeling, there's the recognition,

190
00:15:25,880 --> 00:15:31,280
 there's the judgments and reactions, and there is the

191
00:15:31,280 --> 00:15:33,400
 awareness of seeing.

192
00:15:33,400 --> 00:15:35,400
 Same with hearing, smelling, tasting, feeling.

193
00:15:35,400 --> 00:15:38,960
 With thinking, it's a little bit different.

194
00:15:38,960 --> 00:15:41,840
 Similar but same constituents, but it works a little bit

195
00:15:41,840 --> 00:15:43,480
 differently because it doesn't

196
00:15:43,480 --> 00:15:50,720
 involve anything physical, except for the physical base of

197
00:15:50,720 --> 00:15:52,200
 the mind.

198
00:15:52,200 --> 00:15:54,610
 That same idea, still an experience, still involves at

199
00:15:54,610 --> 00:15:56,040
 least the other four aggregates

200
00:15:56,040 --> 00:16:02,800
 directly.

201
00:16:02,800 --> 00:16:05,280
 That's the building blocks of reality.

202
00:16:05,280 --> 00:16:07,820
 And so the Buddha devised what we call the four foundations

203
00:16:07,820 --> 00:16:09,160
 of mindfulness, or the four

204
00:16:09,160 --> 00:16:14,250
 satipatthana, the four establishments, or establishing,

205
00:16:14,250 --> 00:16:16,680
 four means of establishing what

206
00:16:16,680 --> 00:16:21,280
 we call mindfulness, but really should be understood as

207
00:16:21,280 --> 00:16:23,520
 recognition or remembrance.

208
00:16:23,520 --> 00:16:29,910
 And so our meditation is a means of becoming invincible in

209
00:16:29,910 --> 00:16:33,320
 regards to these five aspects

210
00:16:33,320 --> 00:16:35,630
 of experience at the six senses, these six types of

211
00:16:35,630 --> 00:16:37,720
 experience, seeing experience, hearing

212
00:16:37,720 --> 00:16:42,360
 experience, smelling experience, tasting experience,

213
00:16:42,360 --> 00:16:46,040
 feeling experience, and thinking experience.

214
00:16:46,040 --> 00:16:51,760
 Being invincible, so not being subject to upset, not being

215
00:16:51,760 --> 00:16:54,440
 upset by bad experience.

216
00:16:54,440 --> 00:16:56,640
 How do we achieve that?

217
00:16:56,640 --> 00:16:58,800
 We achieve that based on the four satipatthana.

218
00:16:58,800 --> 00:17:01,100
 So if we add this in there, the things that you have to

219
00:17:01,100 --> 00:17:02,960
 know about Buddhism or about Buddhist

220
00:17:02,960 --> 00:17:06,840
 meditation are just six, five, and four.

221
00:17:06,840 --> 00:17:09,590
 The six senses, the five aggregates, and the four satipatth

222
00:17:09,590 --> 00:17:11,000
ana, the four foundations of

223
00:17:11,000 --> 00:17:14,880
 mind.

224
00:17:14,880 --> 00:17:17,760
 So again, sati doesn't really mean mindfulness.

225
00:17:17,760 --> 00:17:22,560
 It means remembrance or reminding yourself.

226
00:17:22,560 --> 00:17:29,600
 So the practice is an active work or active undertaking of

227
00:17:29,600 --> 00:17:33,820
 reminding yourself, reminding

228
00:17:33,820 --> 00:17:37,970
 yourself what's really going on, returning your mind to

229
00:17:37,970 --> 00:17:39,960
 these building blocks.

230
00:17:39,960 --> 00:17:43,110
 So when you see something, right away you know that you're

231
00:17:43,110 --> 00:17:44,760
 seeing, but the next moment

232
00:17:44,760 --> 00:17:51,020
 your mind is generally off judging it, liking, disliking,

233
00:17:51,020 --> 00:17:54,520
 identifying with it, or so on.

234
00:17:54,520 --> 00:18:00,460
 Finding a way to maintain or prolong or obtain, or finding

235
00:18:00,460 --> 00:18:03,600
 a way to chase off or destroy,

236
00:18:03,600 --> 00:18:06,600
 or so on.

237
00:18:06,600 --> 00:18:10,280
 Cultivating all sorts of views and opinions and so on.

238
00:18:10,280 --> 00:18:13,400
 Arrogance and conceit and what have you.

239
00:18:13,400 --> 00:18:19,720
 So to avoid that, we want to remind ourselves that keep our

240
00:18:19,720 --> 00:18:23,280
 mind with the building block.

241
00:18:23,280 --> 00:18:26,280
 Keep our minds with that ultimate reality.

242
00:18:26,280 --> 00:18:29,900
 Keep our minds from judging.

243
00:18:29,900 --> 00:18:35,520
 Remind ourselves it's just seeing, it's just hearing.

244
00:18:35,520 --> 00:18:38,650
 If you do this, you start to understand why this is

245
00:18:38,650 --> 00:18:41,160
 important because you start to realize

246
00:18:41,160 --> 00:18:44,600
 that our judgments are really arbitrary.

247
00:18:44,600 --> 00:18:47,960
 They're habitual, they're arbitrary.

248
00:18:47,960 --> 00:18:53,700
 We've cultivated them just kind of randomly or based on our

249
00:18:53,700 --> 00:18:56,620
 own experiences rather than

250
00:18:56,620 --> 00:19:01,320
 any kind of rhyme or reason, any kind of logic.

251
00:19:01,320 --> 00:19:05,000
 Our likes and dislikes really are of no use to us.

252
00:19:05,000 --> 00:19:08,920
 They really have no intrinsic value, likes and dislike.

253
00:19:08,920 --> 00:19:10,400
 They don't make you happier.

254
00:19:10,400 --> 00:19:13,160
 They just make you vulnerable.

255
00:19:13,160 --> 00:19:15,360
 When you like something, then you're vulnerable to loss.

256
00:19:15,360 --> 00:19:20,560
 When you dislike something, then you're vulnerable to gain.

257
00:19:20,560 --> 00:19:22,400
 When you gain something, you don't like.

258
00:19:22,400 --> 00:19:26,160
 It doesn't make you happier.

259
00:19:26,160 --> 00:19:30,570
 So the thing, if your happiness is dependent on a specific

260
00:19:30,570 --> 00:19:32,760
 state or a specific object,

261
00:19:32,760 --> 00:19:33,760
 then you'll never be invincible.

262
00:19:33,760 --> 00:19:38,480
 You'll always be vulnerable to loss, to change.

263
00:19:38,480 --> 00:19:40,960
 This is the key of the Buddha's teaching.

264
00:19:40,960 --> 00:19:43,160
 It doesn't work.

265
00:19:43,160 --> 00:19:44,160
 It's inferior.

266
00:19:44,160 --> 00:19:49,030
 It's inferior to a happiness that is unshakable, a

267
00:19:49,030 --> 00:19:52,440
 happiness that can't be upset, that isn't

268
00:19:52,440 --> 00:19:56,000
 vulnerable.

269
00:19:56,000 --> 00:20:01,020
 So we practice the force of tipatthana to keep us grounded,

270
00:20:01,020 --> 00:20:03,520
 to keep us in reality, to

271
00:20:03,520 --> 00:20:06,880
 keep us clear in our mind.

272
00:20:06,880 --> 00:20:10,870
 And because it's based on the building blocks of reality,

273
00:20:10,870 --> 00:20:13,000
 the force of tipatthana cover

274
00:20:13,000 --> 00:20:16,260
 really all aspects of experience, the body, mindfulness of

275
00:20:16,260 --> 00:20:17,920
 the body, mindfulness of the

276
00:20:17,920 --> 00:20:21,280
 feeling, mindfulness of the mind, and mindfulness of the

277
00:20:21,280 --> 00:20:23,200
 dumbness, which is the emotions and

278
00:20:23,200 --> 00:20:27,850
 senses, even just taking the six senses as objects, seeing,

279
00:20:27,850 --> 00:20:30,320
 seeing, or hearing, or reminding

280
00:20:30,320 --> 00:20:33,320
 yourself.

281
00:20:33,320 --> 00:20:43,850
 It's, therefore, enough to protect you from any suffering

282
00:20:43,850 --> 00:20:48,880
 because it can be cultivated

283
00:20:48,880 --> 00:20:52,280
 in any situation.

284
00:20:52,280 --> 00:20:56,140
 And once cultivated, it leads to awareness of reality in

285
00:20:56,140 --> 00:20:57,560
 every situation.

286
00:20:57,560 --> 00:20:59,840
 Because no matter what you experience, it's always just

287
00:20:59,840 --> 00:21:01,800
 going to be seeing, hearing, smelling,

288
00:21:01,800 --> 00:21:03,360
 tasting, feeling, thinking.

289
00:21:03,360 --> 00:21:07,930
 All of your reactions, good or bad, are purely subjective

290
00:21:07,930 --> 00:21:09,080
 in your mind.

291
00:21:09,080 --> 00:21:13,560
 The only objective aspect is the experience.

292
00:21:13,560 --> 00:21:18,240
 When you can get that down, then you no longer react.

293
00:21:18,240 --> 00:21:19,240
 You just interact.

294
00:21:19,240 --> 00:21:21,920
 You just exist.

295
00:21:21,920 --> 00:21:24,040
 You become what most of us think we are.

296
00:21:24,040 --> 00:21:27,330
 Most of us think we are just seeing, hearing, smelling,

297
00:21:27,330 --> 00:21:29,400
 tasting, feeling, thinking.

298
00:21:29,400 --> 00:21:31,930
 We think that we're experiencing things, but we don't

299
00:21:31,930 --> 00:21:34,360
 realize that we're reacting to everything,

300
00:21:34,360 --> 00:21:41,040
 every moment, reacting and judging, clinging and philosoph

301
00:21:41,040 --> 00:21:44,120
izing and rationalizing.

302
00:21:44,120 --> 00:21:49,760
 We're really realizing that we're not just being, we're not

303
00:21:49,760 --> 00:21:51,400
 just hearing.

304
00:21:51,400 --> 00:21:56,370
 So there is a little bit of dhamma, something to start us

305
00:21:56,370 --> 00:21:57,080
 off.

306
00:21:57,080 --> 00:22:01,960
 We'll see, hopefully this is something that will continue.

307
00:22:01,960 --> 00:22:03,960
 The sun has almost gone down.

308
00:22:03,960 --> 00:22:07,720
 The mosquitoes will be coming out soon, so I think I'll

309
00:22:07,720 --> 00:22:08,680
 stop there.

310
00:22:08,680 --> 00:22:14,130
 Thank you all for taking part in our experiment, which has

311
00:22:14,130 --> 00:22:17,840
 been wildly successful in conducting

312
00:22:17,840 --> 00:22:20,760
 an online meditation group.

313
00:22:20,760 --> 00:22:26,670
 We're now having online meditation talks based on our

314
00:22:26,670 --> 00:22:29,120
 meditation practice.

315
00:22:29,120 --> 00:22:32,600
 So hope to see you all soon.

316
00:22:32,600 --> 00:22:33,640
 Be well.

