1
00:00:00,000 --> 00:00:07,440
 Hello, welcome back to Ask a Monk. Next question comes from

2
00:00:07,440 --> 00:00:09,560
 BD 951, who asks, "Can you speak

3
00:00:09,560 --> 00:00:13,710
 on the tepidaka? I heard the oral tradition wasn't written

4
00:00:13,710 --> 00:00:15,680
 down for many years, even then

5
00:00:15,680 --> 00:00:20,230
 not in language of the Buddha. If this is the case, why

6
00:00:20,230 --> 00:00:22,680
 argue over such things as did

7
00:00:22,680 --> 00:00:29,060
 the Buddha say rites or rituals or prayers and practices?"

8
00:00:29,060 --> 00:00:30,880
 Thanks. So talking about the

9
00:00:30,880 --> 00:00:36,580
 tepidaka and specifically talking about the tepidaka in

10
00:00:36,580 --> 00:00:39,080
 terms of its exactness. First

11
00:00:39,080 --> 00:00:42,240
 of all, the tepidaka is supposed to have been written down

12
00:00:42,240 --> 00:00:43,960
 in something very close to what

13
00:00:43,960 --> 00:00:50,160
 the Buddha actually spoke. This is the language that it was

14
00:00:50,160 --> 00:00:53,440
 memorized in. And it may have

15
00:00:53,440 --> 00:01:00,100
 been standardized a little bit, even in those early times

16
00:01:00,100 --> 00:01:03,720
 where there may be spellings or

17
00:01:03,720 --> 00:01:09,740
 forms that were standardized in terms of the spelling and

18
00:01:09,740 --> 00:01:13,160
 the harmonization of the actual

19
00:01:13,160 --> 00:01:17,960
 words. But the language itself is very similar to Indian

20
00:01:17,960 --> 00:01:20,520
 languages that you find today to

21
00:01:20,520 --> 00:01:24,560
 a certain extent and quite similar to Sanskrit actually,

22
00:01:24,560 --> 00:01:27,240
 but much more of a spoken language.

23
00:01:27,240 --> 00:01:31,250
 The language of Prakrit that they call Pali, or we call Pal

24
00:01:31,250 --> 00:01:34,520
i. It's also called Magadha.

25
00:01:34,520 --> 00:01:36,660
 Among Buddhists, we'll call it Magadha, the Magadha

26
00:01:36,660 --> 00:01:38,240
 language because that was the area

27
00:01:38,240 --> 00:01:41,180
 where the Buddha lived and we believe it to be the language

28
00:01:41,180 --> 00:01:42,640
 that he spoke. It may not

29
00:01:42,640 --> 00:01:48,170
 have been exactly as I said. The second thing is about an

30
00:01:48,170 --> 00:01:49,840
 oral tradition. And there's something

31
00:01:49,840 --> 00:01:54,220
 great about an oral tradition that is really misunderstood.

32
00:01:54,220 --> 00:01:56,440
 We think that an oral tradition

33
00:01:56,440 --> 00:01:59,170
 is somehow inferior to the written word and that's not

34
00:01:59,170 --> 00:02:00,880
 really the case. The mind is an

35
00:02:00,880 --> 00:02:09,240
 incredible computer and if used properly is much more

36
00:02:09,240 --> 00:02:16,040
 reliable than a book. And this is

37
00:02:16,040 --> 00:02:21,270
 far true. This is even more true when you're talking about

38
00:02:21,270 --> 00:02:23,880
 groups of people memorizing

39
00:02:23,880 --> 00:02:29,310
 texts. And now I've done some memorizing of texts and it's

40
00:02:29,310 --> 00:02:31,680
 true that sometimes I get it

41
00:02:31,680 --> 00:02:37,460
 wrong but I misremember something. But here we're talking

42
00:02:37,460 --> 00:02:39,800
 about a language that was native

43
00:02:39,800 --> 00:02:42,920
 to these people. This was the language that they spoke. So

44
00:02:42,920 --> 00:02:44,400
 what they were memorizing,

45
00:02:44,400 --> 00:02:47,610
 maybe not exactly as I said, but what they were memorizing

46
00:02:47,610 --> 00:02:49,200
 was something that was very

47
00:02:49,200 --> 00:02:54,790
 easily memorable to them. So whereas I might skip a word or

48
00:02:54,790 --> 00:02:57,680
 get a verb form wrong, I might

49
00:02:57,680 --> 00:03:01,690
 conjugate a verb wrong in the wrong form, they would never

50
00:03:01,690 --> 00:03:03,880
 do that. And skipping a word

51
00:03:03,880 --> 00:03:08,300
 would not be as likely either because it wouldn't make

52
00:03:08,300 --> 00:03:11,520
 sense and it wouldn't fit with the standard

53
00:03:11,520 --> 00:03:17,030
 mode of expression. And when you're talking about groups of

54
00:03:17,030 --> 00:03:19,480
 people, which we are, the

55
00:03:19,480 --> 00:03:23,870
 number of Buddhist monks when the Buddha passed away was, I

56
00:03:23,870 --> 00:03:26,560
 don't know, there were thousands

57
00:03:26,560 --> 00:03:31,020
 upon thousands, I don't know, thousands of them anyway. And

58
00:03:31,020 --> 00:03:32,920
 they would memorize these

59
00:03:32,920 --> 00:03:39,150
 texts in large groups of people. And it's incredible to me

60
00:03:39,150 --> 00:03:41,160
 even how just as one person

61
00:03:41,160 --> 00:03:45,100
 how much I can remember. I can memorize the sutta. I can

62
00:03:45,100 --> 00:03:47,160
 memorize pieces of this text

63
00:03:47,160 --> 00:03:50,760
 piece by piece by piece until I eventually get the sutta.

64
00:03:50,760 --> 00:03:51,760
 When I go back, I've missed

65
00:03:51,760 --> 00:03:55,050
 some things and I go back over it and over it. And

66
00:03:55,050 --> 00:03:57,920
 eventually it sticks in memory. There

67
00:03:57,920 --> 00:04:01,200
 are sutta's that I haven't memorized for a long time. There

68
00:04:01,200 --> 00:04:03,280
 was a story of a monk who

69
00:04:03,280 --> 00:04:08,510
 for 20 years he didn't look at the Majima Nikaya, which is

70
00:04:08,510 --> 00:04:11,720
 152 talks the Buddha gave. And 20

71
00:04:11,720 --> 00:04:15,410
 years later he could still recite it after not looking at

72
00:04:15,410 --> 00:04:17,800
 it for 20 years, not thinking

73
00:04:17,800 --> 00:04:21,670
 about it. He went off to practice meditation. And as a

74
00:04:21,670 --> 00:04:24,280
 result of the meditation practice,

75
00:04:24,280 --> 00:04:31,830
 they say he was able to memorize it without any trouble. So

76
00:04:31,830 --> 00:04:32,800
 there's something there. I

77
00:04:32,800 --> 00:04:37,450
 mean, obviously it's possible and seems quite likely that

78
00:04:37,450 --> 00:04:40,040
 there were errors that crept into

79
00:04:40,040 --> 00:04:45,560
 the Buddha's teaching. But I don't think the oral tradition

80
00:04:45,560 --> 00:04:47,580
 was really to blame. There's

81
00:04:47,580 --> 00:04:51,580
 an example in the Christian tradition. If you, there's a

82
00:04:51,580 --> 00:04:53,480
 man, a scholar, one of the

83
00:04:53,480 --> 00:05:00,000
 greatest Bible scholars, a great Bible scholar, Bart Ehrman

84
00:05:00,000 --> 00:05:03,280
, who explains that actually we

85
00:05:03,280 --> 00:05:06,500
 don't know what Jesus actually said because we don't have

86
00:05:06,500 --> 00:05:08,480
 the original texts. We don't

87
00:05:08,480 --> 00:05:11,380
 know if those texts are what Jesus actually said or not,

88
00:05:11,380 --> 00:05:13,160
 but we don't have those texts.

89
00:05:13,160 --> 00:05:18,240
 He said, what we have are mistakes upon mistakes upon

90
00:05:18,240 --> 00:05:21,800
 mistakes or errors or changes upon changes

91
00:05:21,800 --> 00:05:24,980
 upon changes. There's more changes in the, there's more

92
00:05:24,980 --> 00:05:27,080
 discrepancies between the different

93
00:05:27,080 --> 00:05:31,950
 texts than there are words in the Bible. And he attributes

94
00:05:31,950 --> 00:05:33,720
 this mostly, well, partially

95
00:05:33,720 --> 00:05:38,620
 to scribal errors. You know, I mean, scribes are generally

96
00:05:38,620 --> 00:05:41,360
 not meditating. They're not

97
00:05:41,360 --> 00:05:45,790
 engaged in the practice of the Buddhist or of the teaching.

98
00:05:45,790 --> 00:05:47,800
 Scribes are generally paid

99
00:05:47,800 --> 00:05:51,800
 or in the Buddhist tradition, maybe not paid, but they

100
00:05:51,800 --> 00:05:54,360
 wouldn't likely be the meditation

101
00:05:54,360 --> 00:05:58,800
 monks. And so as a result, there were many errors that

102
00:05:58,800 --> 00:06:01,420
 crept into the Christian texts.

103
00:06:01,420 --> 00:06:07,540
 But another reason was the doctrinal differences. So there

104
00:06:07,540 --> 00:06:11,060
 were many schisms and they changed

105
00:06:11,060 --> 00:06:13,590
 according to their schisms. That is maybe the case in

106
00:06:13,590 --> 00:06:15,240
 Buddhism as well, because there

107
00:06:15,240 --> 00:06:17,470
 were many different schools and there have been many

108
00:06:17,470 --> 00:06:20,160
 different schools. And as a result,

109
00:06:20,160 --> 00:06:27,320
 we do find the same discourse has different and often

110
00:06:27,320 --> 00:06:31,480
 pointedly different information

111
00:06:31,480 --> 00:06:34,450
 where this school, it follows after their idea and that

112
00:06:34,450 --> 00:06:36,240
 school, it follows after their

113
00:06:36,240 --> 00:06:40,640
 idea and so on. So that is the case. I think the point

114
00:06:40,640 --> 00:06:43,760
 though, whether there may be errors

115
00:06:43,760 --> 00:06:48,690
 and changes and so on in the tepidica or not, and whether

116
00:06:48,690 --> 00:06:51,600
 it actually is what the Buddha

117
00:06:51,600 --> 00:06:59,470
 said or not, is that we have a Buddhist corpus. We have

118
00:06:59,470 --> 00:07:02,560
 this tepidica, which is a very internally

119
00:07:02,560 --> 00:07:07,980
 consistent group of texts. And should we take it literally

120
00:07:07,980 --> 00:07:10,760
 as every piece of it is the Buddha's

121
00:07:10,760 --> 00:07:16,250
 teaching? No, I think to an extent we shouldn't. To an

122
00:07:16,250 --> 00:07:19,760
 extent we should be able to apply the

123
00:07:19,760 --> 00:07:25,530
 basic concepts that are found therein. Because what we have

124
00:07:25,530 --> 00:07:28,400
 is based on, obviously based

125
00:07:28,400 --> 00:07:31,400
 on the teachings of the Buddha, whether or not, it would be

126
00:07:31,400 --> 00:07:32,680
 nice if it was all exactly

127
00:07:32,680 --> 00:07:37,100
 what the Buddha said. And I tend to think, for myself, I

128
00:07:37,100 --> 00:07:39,360
 say, unless you can prove to

129
00:07:39,360 --> 00:07:42,860
 me or find some conclusive evidence that it's not, then I'd

130
00:07:42,860 --> 00:07:44,720
 rather think that it is with

131
00:07:44,720 --> 00:07:49,360
 the Buddha taught. And there are many people out there who

132
00:07:49,360 --> 00:07:52,280
 try to put the texts into times

133
00:07:52,280 --> 00:07:56,120
 and say, this group of texts was later because it's in a

134
00:07:56,120 --> 00:07:58,680
 different, meter is different, the

135
00:07:58,680 --> 00:08:03,120
 poetry is of a different type and so on. And they have all

136
00:08:03,120 --> 00:08:05,320
 these theories that seem to

137
00:08:05,320 --> 00:08:11,030
 me, I don't know, whatever. They have their theories, I

138
00:08:11,030 --> 00:08:14,600
 have my theories, or lack thereof.

139
00:08:14,600 --> 00:08:18,880
 But the point is that clearly if you read through the text,

140
00:08:18,880 --> 00:08:20,720
 clearly there is wisdom

141
00:08:20,720 --> 00:08:24,820
 here. Clearly there is something that has something to do

142
00:08:24,820 --> 00:08:26,880
 with Buddhism, which means

143
00:08:26,880 --> 00:08:30,180
 with enlightenment. Buddha means one who knows with

144
00:08:30,180 --> 00:08:33,080
 knowledge, with wisdom. There is Buddhism

145
00:08:33,080 --> 00:08:36,910
 in these texts. There is wisdom and enlightenment in these

146
00:08:36,910 --> 00:08:39,280
 texts. And if you can get that from

147
00:08:39,280 --> 00:08:42,560
 the text, I think that is really the most important thing.

148
00:08:42,560 --> 00:08:44,080
 There are passages in there

149
00:08:44,080 --> 00:08:48,580
 that are so profound and there will be a point where the

150
00:08:48,580 --> 00:08:51,920
 Buddha said, all you need to know,

151
00:08:51,920 --> 00:08:54,770
 before you start to practice meditation, all you need to

152
00:08:54,770 --> 00:08:56,160
 know is that nothing is worth

153
00:08:56,160 --> 00:09:00,280
 clinging to. Basically, that's something that you find in

154
00:09:00,280 --> 00:09:02,820
 the text. If a person learns that

155
00:09:02,820 --> 00:09:06,980
 nothing, no dhamma, no reality, no thing is worth clinging

156
00:09:06,980 --> 00:09:08,840
 to, then this is enough for

157
00:09:08,840 --> 00:09:11,480
 them to get started. This is enough as far as book learning

158
00:09:11,480 --> 00:09:12,960
 goes. This is enough as far

159
00:09:12,960 --> 00:09:19,910
 as a basic or a precursor to the practice of the Buddha's

160
00:09:19,910 --> 00:09:22,200
 teachings. So this is how we

161
00:09:22,200 --> 00:09:27,050
 should look at the tipitika. You know, for people to go

162
00:09:27,050 --> 00:09:28,040
 about quoting suttas and saying

163
00:09:28,040 --> 00:09:32,570
 this proves that, that proves that. And often saying, this

164
00:09:32,570 --> 00:09:35,040
 implies this, this implies that.

165
00:09:35,040 --> 00:09:40,240
 And therefore, dogmatically sticking to a set practice, I

166
00:09:40,240 --> 00:09:42,240
 think is really sad. There

167
00:09:42,240 --> 00:09:45,010
 are many different types of practice out there based on the

168
00:09:45,010 --> 00:09:46,680
 Buddha's teaching. And you'll

169
00:09:46,680 --> 00:09:49,940
 see that if you read the tipitika, if you read the suttas,

170
00:09:49,940 --> 00:09:51,840
 that there wasn't one practice

171
00:09:51,840 --> 00:09:56,640
 that the Buddha recommended for all people. So even if you

172
00:09:56,640 --> 00:09:58,600
 could take it literally, it

173
00:09:58,600 --> 00:10:00,620
 wouldn't do much good to say, you know, look at this sutt

174
00:10:00,620 --> 00:10:01,840
ata, the Buddha said, this is

175
00:10:01,840 --> 00:10:04,370
 the way to enlightenment. Then someone comes up, well, look

176
00:10:04,370 --> 00:10:05,520
 at this suttas, this is the

177
00:10:05,520 --> 00:10:11,260
 way to enlightenment. So it wouldn't even do good if it was

178
00:10:11,260 --> 00:10:12,960
. The important thing is

179
00:10:12,960 --> 00:10:18,370
 the basic understanding and the premise that we can then

180
00:10:18,370 --> 00:10:20,840
 use for our practice. There are

181
00:10:20,840 --> 00:10:23,340
 many things in the tipitika that will guide you away from

182
00:10:23,340 --> 00:10:24,880
 wrong practice. And you should

183
00:10:24,880 --> 00:10:28,160
 get those, you should understand those, but they're very

184
00:10:28,160 --> 00:10:30,220
 general principles. The principles

185
00:10:30,220 --> 00:10:35,620
 of impermanence, of impermanence, suffering and non-self in

186
00:10:35,620 --> 00:10:38,240
 the sense that there's nothing

187
00:10:38,240 --> 00:10:41,730
 in the world, there's nothing in samsara that is permanent,

188
00:10:41,730 --> 00:10:43,360
 that is lasting, that you can

189
00:10:43,360 --> 00:10:46,280
 cling to. There's nothing in samsara that is going to make

190
00:10:46,280 --> 00:10:47,560
 you happy if you do cling

191
00:10:47,560 --> 00:10:52,760
 to it because it's impermanent. And there's nothing in sams

192
00:10:52,760 --> 00:10:55,200
ara in the whole of existence

193
00:10:55,200 --> 00:10:58,660
 that you can say is me, is mine, that it is under my

194
00:10:58,660 --> 00:11:01,400
 control, that you can control, that

195
00:11:01,400 --> 00:11:04,750
 you can change to say, okay, it's impermanent, I'm going to

196
00:11:04,750 --> 00:11:06,480
 make it permanent, there's no

197
00:11:06,480 --> 00:11:10,150
 stability to be found. These three general principles, well

198
00:11:10,150 --> 00:11:11,160
, you'll find them throughout

199
00:11:11,160 --> 00:11:15,470
 the tipitika, but I think clearly those are core ideas in

200
00:11:15,470 --> 00:11:17,760
 the Buddha's teaching and that

201
00:11:17,760 --> 00:11:21,950
 has to do with the Four Noble Truths, which are basically

202
00:11:21,950 --> 00:11:24,560
 the core of the Buddha's teaching.

203
00:11:24,560 --> 00:11:26,930
 But you shouldn't take this as some kind of doctrine where,

204
00:11:26,930 --> 00:11:28,280
 yes, what did the Buddha teach

205
00:11:28,280 --> 00:11:30,930
 you, the Four Noble Truths, and then you think about them

206
00:11:30,930 --> 00:11:32,920
 and you understand them intellectually

207
00:11:32,920 --> 00:11:36,470
 and you know how they fit into all this text and this text

208
00:11:36,470 --> 00:11:38,480
 and how this group of teaching

209
00:11:38,480 --> 00:11:42,760
 fits in and so on. It's really useless. This is, you know,

210
00:11:42,760 --> 00:11:44,320
 the Buddha said, like people

211
00:11:44,320 --> 00:11:50,440
 who look after the cows of others. It's not so important to

212
00:11:50,440 --> 00:11:52,360
 know everything. It's important

213
00:11:52,360 --> 00:11:55,980
 to know what's important. It's important to know those

214
00:11:55,980 --> 00:11:57,920
 things and to know them well, those

215
00:11:57,920 --> 00:12:01,630
 things that lead you to freedom from suffering. And

216
00:12:01,630 --> 00:12:07,520
 intellectually, simply to understand the

217
00:12:07,520 --> 00:12:10,490
 types of things that are going to help and improve your

218
00:12:10,490 --> 00:12:12,280
 practice. What are the wrong

219
00:12:12,280 --> 00:12:15,850
 ways of practice? The wrong courses of action? What are the

220
00:12:15,850 --> 00:12:17,720
 right courses of action? What

221
00:12:17,720 --> 00:12:22,310
 are wholesome things? What are unwholesome things? And this

222
00:12:22,310 --> 00:12:23,800
 is really important, getting

223
00:12:23,800 --> 00:12:27,910
 the general principles. So reading the tipitika is great.

224
00:12:27,910 --> 00:12:30,120
 Read through it. But don't look

225
00:12:30,120 --> 00:12:32,470
 through it, you know, trying to pick out why did the Buddha

226
00:12:32,470 --> 00:12:33,760
 say this, why did the Buddha

227
00:12:33,760 --> 00:12:37,230
 say that? Get general principles and then go to practice

228
00:12:37,230 --> 00:12:38,520
 meditation. Because what the

229
00:12:38,520 --> 00:12:42,900
 Buddha was teaching was about reality, was about this, you

230
00:12:42,900 --> 00:12:44,880
 know, our existence, what's

231
00:12:44,880 --> 00:12:48,750
 happening right here and now, this listening to the sound

232
00:12:48,750 --> 00:12:50,960
 of your speakers and the feeling

233
00:12:50,960 --> 00:12:54,290
 in your chair and the hot and the heat and the cold, the

234
00:12:54,290 --> 00:12:56,160
 emotions going through your

235
00:12:56,160 --> 00:12:59,410
 mind, the intellectual process of liking what I say, disl

236
00:12:59,410 --> 00:13:01,200
iking what I say, judging what

237
00:13:01,200 --> 00:13:07,620
 I say, the ego, the, you know, the delusion and so on. All

238
00:13:07,620 --> 00:13:09,200
 of this is what the Buddha

239
00:13:09,200 --> 00:13:13,020
 taught. It's clearly there in the tipitika. And if

240
00:13:13,020 --> 00:13:15,520
 something in the tipitika goes against

241
00:13:15,520 --> 00:13:20,500
 reality, that's when you should discard it. Not when it

242
00:13:20,500 --> 00:13:23,200
 goes against what you believe.

243
00:13:23,200 --> 00:13:26,150
 And I think this is what leads a lot of people to criticize

244
00:13:26,150 --> 00:13:28,040
 and to find, you know, patterns

245
00:13:28,040 --> 00:13:30,920
 of what is really the Buddha's teaching and what is not.

246
00:13:30,920 --> 00:13:32,720
 Because it's based on their views

247
00:13:32,720 --> 00:13:37,080
 and their opinions. They don't like this teaching, so they

248
00:13:37,080 --> 00:13:39,580
 throw it out. They don't believe this

249
00:13:39,580 --> 00:13:45,480
 teaching is in line with their idea of what is right and so

250
00:13:45,480 --> 00:13:47,240
 on. But that's, you know,

251
00:13:47,240 --> 00:13:49,240
 there's nothing you can do about that because there are as

252
00:13:49,240 --> 00:13:51,760
 many views as there are people.

253
00:13:51,760 --> 00:13:57,490
 You can, you know, you can't possibly make everyone agree

254
00:13:57,490 --> 00:14:00,360
 with any teaching, even the

255
00:14:00,360 --> 00:14:05,260
 truth. There are more people probably who will refuse to

256
00:14:05,260 --> 00:14:07,920
 believe the truth than people who

257
00:14:07,920 --> 00:14:11,510
 will believe the truth. You know, maybe, maybe not. There

258
00:14:11,510 --> 00:14:14,440
 are definitely people who don't,

259
00:14:14,440 --> 00:14:18,780
 will refuse to believe the truth when given to them.

260
00:14:18,780 --> 00:14:21,440
 Probably I'd say more, more who refuse

261
00:14:21,440 --> 00:14:27,700
 to believe the truth. So, you know, don't, don't go looking

262
00:14:27,700 --> 00:14:31,360
 for a doctrine, per se, in

263
00:14:31,360 --> 00:14:36,830
 the tathitaka in terms of the Buddha taught this and this

264
00:14:36,830 --> 00:14:37,000
 is, this is the truth in the

265
00:14:37,000 --> 00:14:40,070
 text. So Buddha was against that. He said, you know, when

266
00:14:40,070 --> 00:14:41,560
 you know for yourself that

267
00:14:41,560 --> 00:14:46,270
 these things are to your benefit, then develop them. When

268
00:14:46,270 --> 00:14:49,040
 you know for yourself that certain

269
00:14:49,040 --> 00:14:52,360
 things are for your detriment, then abandon them. And that

270
00:14:52,360 --> 00:14:54,120
's what happens. Once you know

271
00:14:54,120 --> 00:14:57,410
 that something is for your detriment, you will not develop

272
00:14:57,410 --> 00:14:59,000
 it. It will, it will only

273
00:14:59,000 --> 00:15:02,460
 decrease and decline. And once you see clearly that it's

274
00:15:02,460 --> 00:15:04,560
 not your benefit, you will never

275
00:15:04,560 --> 00:15:10,560
 give rise to it again. It will disappear. So simple. You

276
00:15:10,560 --> 00:15:12,520
 gave one specific example here

277
00:15:12,520 --> 00:15:16,740
 which I'm not really, it doesn't really mean too much to me

278
00:15:16,740 --> 00:15:19,080
. You know, the example of whether

279
00:15:19,080 --> 00:15:22,550
 did the Buddha say rites and rituals or prayers and

280
00:15:22,550 --> 00:15:24,800
 practices. Well, that's really a different

281
00:15:24,800 --> 00:15:29,500
 question though. That's the question of translation because

282
00:15:29,500 --> 00:15:32,040
 those two terms come from the same

283
00:15:32,040 --> 00:15:36,860
 Pali passage, the same passage, the same word as Sila Bata

284
00:15:36,860 --> 00:15:39,360
 Paramasa. And so it's not whether

285
00:15:39,360 --> 00:15:44,070
 the Buddha said A or B, it's whether one or the other is a

286
00:15:44,070 --> 00:15:46,560
 more proper translation, both

287
00:15:46,560 --> 00:15:50,120
 etymologically and in terms of the, what the Buddha had,

288
00:15:50,120 --> 00:15:52,800
 what the Buddha meant by it. Sila

289
00:15:52,800 --> 00:15:58,950
 Bata Paramasa specifically, and this is, you know, it's

290
00:15:58,950 --> 00:16:02,160
 only an example, but Sila Bata,

291
00:16:02,160 --> 00:16:07,180
 that comes from what the Para Amasa Sila means refraining

292
00:16:07,180 --> 00:16:11,440
 from certain things. What that means

293
00:16:11,440 --> 00:16:15,860
 undertaking certain things or certain practices. So

294
00:16:15,860 --> 00:16:20,080
 whatever you translate them as, it means one

295
00:16:20,080 --> 00:16:27,080
 is negative and one is positive, refraining and undertaking

296
00:16:27,080 --> 00:16:29,840
 in the sense of here, yeah,

297
00:16:29,840 --> 00:16:35,910
 refraining and undertaking. Para Amasa, Para means others

298
00:16:35,910 --> 00:16:39,040
 or external and Amasa means clinging or

299
00:16:39,040 --> 00:16:46,130
 holding on to grasping. So it's holding on to these two

300
00:16:46,130 --> 00:16:50,320
 things, you know, refraining from certain

301
00:16:50,320 --> 00:16:53,440
 things and undertaking certain things and holding that to

302
00:16:53,440 --> 00:16:56,520
 be important. Like saying, to be a good

303
00:16:56,520 --> 00:16:59,640
 Buddhist, you have to do this, you have to light incense,

304
00:16:59,640 --> 00:17:02,160
 you have to bow in this way, and this way

305
00:17:02,160 --> 00:17:05,420
 and this way, you have to chant in this way and this way

306
00:17:05,420 --> 00:17:08,480
 and this way. You know, it's important

307
00:17:08,480 --> 00:17:15,270
 to practice, to do certain things and to refrain from

308
00:17:15,270 --> 00:17:20,400
 certain things that are outside of the path,

309
00:17:20,400 --> 00:17:23,450
 so that don't have anything to do with the path. If someone

310
00:17:23,450 --> 00:17:25,560
 says, you know, you have to light incense,

311
00:17:25,560 --> 00:17:29,810
 or you have to bow and this is the way to enlighten, or

312
00:17:29,810 --> 00:17:33,240
 this is important in the practice of

313
00:17:33,240 --> 00:17:37,120
 enlightenment, or if someone says you have to refrain from

314
00:17:37,120 --> 00:17:41,200
 eating garlic, or you have to refrain

315
00:17:41,200 --> 00:17:46,750
 from this or refrain from that, to practice the Buddhist

316
00:17:46,750 --> 00:17:50,200
 teaching and that has nothing to do with

317
00:17:50,200 --> 00:17:53,930
 the Buddhist teaching, or there's nothing to do with

318
00:17:53,930 --> 00:17:58,000
 reality, with understanding and realizing the

319
00:17:58,000 --> 00:18:01,710
 truth. That's what is meant by Siddhavata Paramasa. So the

320
00:18:01,710 --> 00:18:04,000
 problem you have here is translation,

321
00:18:04,000 --> 00:18:07,360
 which translation there is going to give people a better

322
00:18:07,360 --> 00:18:09,680
 understanding of the actual Pali word,

323
00:18:09,680 --> 00:18:12,980
 and as you can see, it's a complicated word. The best way

324
00:18:12,980 --> 00:18:15,120
 to solve that dilemma is to learn Pali,

325
00:18:15,120 --> 00:18:20,010
 and that's a great thing to do as a Buddhist. It's a life

326
00:18:20,010 --> 00:18:23,720
 goal. If you're a person who is dedicated

327
00:18:23,720 --> 00:18:26,940
 to this path, it's worth it to learn Pali, because any

328
00:18:26,940 --> 00:18:30,120
 translation is only makeshift. You see, there

329
00:18:30,120 --> 00:18:34,320
 are two examples which one is right. Well, neither one

330
00:18:34,320 --> 00:18:37,480
 exactly captures those four words that make

331
00:18:37,480 --> 00:18:43,340
 up the compound. So anyway, just food for thought and a

332
00:18:43,340 --> 00:18:46,880
 little bit of insight for today. Thanks for

333
00:18:46,880 --> 00:18:47,880
 the question. All the best.

