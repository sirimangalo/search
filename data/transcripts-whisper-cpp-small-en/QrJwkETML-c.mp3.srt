1
00:00:00,000 --> 00:00:06,580
 Hi and welcome back to Ask a Monk. Next question comes from

2
00:00:06,580 --> 00:00:08,000
 Tommy Turntables1.

3
00:00:08,000 --> 00:00:12,120
 I have a question about monks and health care. If a monk is

4
00:00:12,120 --> 00:00:14,000
 to live without employment or money,

5
00:00:14,000 --> 00:00:17,110
 what do they do if they find themselves needing medical

6
00:00:17,110 --> 00:00:18,000
 attention?

7
00:00:18,000 --> 00:00:22,890
 Well, first of all, monks don't live without employment

8
00:00:22,890 --> 00:00:24,000
 generally.

9
00:00:24,000 --> 00:00:28,190
 They do live without money, but their employment is to

10
00:00:28,190 --> 00:00:33,000
 teach the teachings of the Buddha, to teach meditation.

11
00:00:33,000 --> 00:00:40,500
 And so generally they do have supporters that will give

12
00:00:40,500 --> 00:00:44,000
 them or help them out in whatever they need.

13
00:00:44,000 --> 00:00:47,110
 If they need medicines, the supporters will give them

14
00:00:47,110 --> 00:00:48,000
 medicines.

15
00:00:48,000 --> 00:00:52,560
 If they need medical attention, the supporters will provide

16
00:00:52,560 --> 00:00:57,000
 them with the means to go to see a doctor or so on.

17
00:00:57,000 --> 00:01:01,740
 Just as the leader of any religion, the religious people in

18
00:01:01,740 --> 00:01:06,000
 other religions receive support generally monetarily.

19
00:01:06,000 --> 00:01:09,790
 But monks are the stipulations that they're only allowed to

20
00:01:09,790 --> 00:01:13,000
 receive those things that they need directly.

21
00:01:13,000 --> 00:01:16,860
 The point being that the receiving of money allows for an

22
00:01:16,860 --> 00:01:20,590
 incredible amount of freedom in what you then do with the

23
00:01:20,590 --> 00:01:21,000
 money.

24
00:01:21,000 --> 00:01:24,640
 It allows or leads generally to things which are not

25
00:01:24,640 --> 00:01:27,000
 conducive to the monk's life.

26
00:01:27,000 --> 00:01:30,740
 It's not as though monks have to do without the necessities

27
00:01:30,740 --> 00:01:31,000
.

28
00:01:31,000 --> 00:01:35,000
 The point is that medicine is a necessity.

29
00:01:35,000 --> 00:01:38,020
 There are four necessities for Buddhist monks and

30
00:01:38,020 --> 00:01:41,000
 considered to be necessities for all people.

31
00:01:41,000 --> 00:01:47,730
 The first one is robes. The second one is food. The third

32
00:01:47,730 --> 00:01:52,000
 one is shelter. And the fourth one is medicines.

33
00:01:52,000 --> 00:01:57,780
 Now, all four of these can be done without or can be

34
00:01:57,780 --> 00:02:01,000
 reduced to a great extent.

35
00:02:01,000 --> 00:02:06,110
 Robes, you can collect rags, food, you can just go on alms

36
00:02:06,110 --> 00:02:10,780
 and receive scraps from people's houses, whatever leftover

37
00:02:10,780 --> 00:02:13,000
 food they have, whatever they're willing to give out.

38
00:02:13,000 --> 00:02:17,410
 Shelter, you can live at the root of a tree, you can live

39
00:02:17,410 --> 00:02:21,000
 in the forest or live in a simple dwelling.

40
00:02:21,000 --> 00:02:24,440
 Generally living wherever people have a place for you to

41
00:02:24,440 --> 00:02:25,000
 live.

42
00:02:25,000 --> 00:02:29,880
 And medicines, you can use natural medicines. There are

43
00:02:29,880 --> 00:02:35,160
 certain medicines that are open to monks which are easy to

44
00:02:35,160 --> 00:02:36,000
 find.

45
00:02:36,000 --> 00:02:40,750
 And so on. So there's two ways to sort of answer this

46
00:02:40,750 --> 00:02:42,000
 question.

47
00:02:42,000 --> 00:02:44,850
 And one is that, yes, we have support. The other one is we

48
00:02:44,850 --> 00:02:47,000
 don't require nearly so much attention.

49
00:02:47,000 --> 00:02:52,130
 A lot of medical conditions can be dealt with without

50
00:02:52,130 --> 00:02:57,740
 medicine simply by bearing with the condition, bearing with

51
00:02:57,740 --> 00:02:59,000
 the difficulty.

52
00:02:59,000 --> 00:03:03,940
 For those that can't, there are doctors and people who are

53
00:03:03,940 --> 00:03:08,000
 willing to help to support you with the fees.

54
00:03:08,000 --> 00:03:13,750
 For myself, recently I stepped on a rusty nail and there

55
00:03:13,750 --> 00:03:19,290
 was a big concern that I might get tetanus because I hadn't

56
00:03:19,290 --> 00:03:23,000
 had a tetanus shot in 15 years or something.

57
00:03:23,000 --> 00:03:26,540
 And so I didn't mention it to anyone at first. Mainly I was

58
00:03:26,540 --> 00:03:28,000
 busy and I didn't think of it.

59
00:03:28,000 --> 00:03:31,060
 But then that evening I said, "Hey, you know, probably I

60
00:03:31,060 --> 00:03:33,000
 should go to a doctor on Monday."

61
00:03:33,000 --> 00:03:35,850
 It was the weekend. I thought, "You probably should go to

62
00:03:35,850 --> 00:03:38,660
 the doctor on Monday and get a tetanus shot. You have to do

63
00:03:38,660 --> 00:03:40,000
 it every 10 years."

64
00:03:40,000 --> 00:03:43,170
 And suddenly everyone was all freaking out and thought, "I

65
00:03:43,170 --> 00:03:46,000
 have to go right now, right now to get this shot."

66
00:03:46,000 --> 00:03:49,780
 And so they rushed me to the emergency room and made sure I

67
00:03:49,780 --> 00:03:51,000
 got this shot.

68
00:03:51,000 --> 00:03:54,110
 And at first I wasn't going to do it because I thought, "Oh

69
00:03:54,110 --> 00:03:56,000
, the emergency room, that's got to be really expensive."

70
00:03:56,000 --> 00:03:58,400
 And they said, "Oh, no, no, maybe $100 or something and don

71
00:03:58,400 --> 00:04:00,000
't worry. They would take care of it."

72
00:04:00,000 --> 00:04:03,000
 And it turned out that it was going to be over $500.

73
00:04:03,000 --> 00:04:07,550
 So at the hospital I just said, I expressed my surprise

74
00:04:07,550 --> 00:04:09,000
 that it was so expensive.

75
00:04:09,000 --> 00:04:12,000
 And it turns out emergency rooms are expensive.

76
00:04:12,000 --> 00:04:17,540
 But right away they gave me this application. It was a

77
00:04:17,540 --> 00:04:19,000
 Catholic hospital.

78
00:04:19,000 --> 00:04:21,200
 And they gave me this application, "If you're hard up,

79
00:04:21,200 --> 00:04:23,000
 there are people who will support you."

80
00:04:23,000 --> 00:04:26,230
 And it turns out the administration there has a program for

81
00:04:26,230 --> 00:04:29,000
 people who don't have the ability to pay.

82
00:04:29,000 --> 00:04:32,000
 And so I filled out this form explaining that I was a monk

83
00:04:32,000 --> 00:04:34,000
 and that I don't touch money

84
00:04:34,000 --> 00:04:37,000
 and I rely on the support of other people and so on.

85
00:04:37,000 --> 00:04:44,000
 And then it just vanished. I didn't hear anything about it.

86
00:04:44,000 --> 00:04:45,770
 And I was worried that the bill might get lost, so I called

87
00:04:45,770 --> 00:04:46,000
 them.

88
00:04:46,000 --> 00:04:50,000
 And they said it had been written off as charity.

89
00:04:50,000 --> 00:04:54,000
 So I think that's one specific example.

90
00:04:54,000 --> 00:04:58,140
 But I think on a more broad scale there are ways and there

91
00:04:58,140 --> 00:04:59,000
 are places.

92
00:04:59,000 --> 00:05:02,510
 I've heard of clinics among the Asian people that will take

93
00:05:02,510 --> 00:05:04,000
 monks for free and so on and so on.

94
00:05:04,000 --> 00:05:07,000
 So there are ways to get around it.

95
00:05:07,000 --> 00:05:10,230
 I don't have health insurance, though my students have

96
00:05:10,230 --> 00:05:12,000
 talked about getting it for me.

97
00:05:12,000 --> 00:05:18,440
 But right now, since I'm enrolled in this program with the

98
00:05:18,440 --> 00:05:23,000
 Catholic hospital system,

99
00:05:23,000 --> 00:05:26,000
 I don't really worry about it.

100
00:05:26,000 --> 00:05:31,000
 I think it's something that you could get health insurance

101
00:05:31,000 --> 00:05:33,000
 if you wanted as a monk,

102
00:05:33,000 --> 00:05:36,700
 if you have students who are willing to support you in that

103
00:05:36,700 --> 00:05:37,000
.

104
00:05:37,000 --> 00:05:38,000
 I've talked to them about it.

105
00:05:38,000 --> 00:05:41,910
 But right now in the situation I'm in, I'm not really

106
00:05:41,910 --> 00:05:43,000
 worried about it

107
00:05:43,000 --> 00:05:46,000
 because I've seen that there are ways to get around it.

108
00:05:46,000 --> 00:05:49,000
 So there's one specific question.

109
00:05:49,000 --> 00:05:52,740
 Interesting to talk about the requisites and how you get

110
00:05:52,740 --> 00:05:54,000
 around that with money.

111
00:05:54,000 --> 00:05:57,250
 I really think often people have this misconception that

112
00:05:57,250 --> 00:06:00,000
 without money you wouldn't be able to live,

113
00:06:00,000 --> 00:06:03,000
 you wouldn't be able to acquire the necessities.

114
00:06:03,000 --> 00:06:07,000
 And I suppose for many people that might be true.

115
00:06:07,000 --> 00:06:11,800
 For homeless people, for people who are out of a job who

116
00:06:11,800 --> 00:06:15,000
 are homeless, not by choice.

117
00:06:15,000 --> 00:06:18,000
 But as a religious person, you have a lot of support.

118
00:06:18,000 --> 00:06:23,560
 And the existence of all that support itself is one of the

119
00:06:23,560 --> 00:06:26,000
 reasons why monks are not allowed to handle money.

120
00:06:26,000 --> 00:06:33,330
 Because you can very easily abuse that support if you touch

121
00:06:33,330 --> 00:06:34,000
 money.

122
00:06:34,000 --> 00:06:38,090
 Then all the support, the faith that people have in you

123
00:06:38,090 --> 00:06:41,230
 means that they're just willing to pile money onto you all

124
00:06:41,230 --> 00:06:42,000
 the time.

125
00:06:42,000 --> 00:06:43,000
 And so this happens.

126
00:06:43,000 --> 00:06:44,000
 There's a lot of corruption.

127
00:06:44,000 --> 00:06:47,070
 I mean you see that in religions where religious people do

128
00:06:47,070 --> 00:06:48,000
 accept money.

129
00:06:48,000 --> 00:06:50,000
 There's a lot of corruption.

130
00:06:50,000 --> 00:06:53,710
 You see it in Buddhism where monks, if monks accept money

131
00:06:53,710 --> 00:06:56,000
 it becomes quite corrupt.

132
00:06:56,000 --> 00:06:59,720
 So it's not as though not touching money means we don't

133
00:06:59,720 --> 00:07:01,000
 have support.

134
00:07:01,000 --> 00:07:05,740
 It's a rule that forbids us from taking advantage of people

135
00:07:05,740 --> 00:07:07,000
's support.

136
00:07:07,000 --> 00:07:09,000
 Okay, so hope that helps to clear things up.

137
00:07:09,000 --> 00:07:11,000
 And thanks for the question.

138
00:07:11,000 --> 00:07:12,000
 Keep them coming.

