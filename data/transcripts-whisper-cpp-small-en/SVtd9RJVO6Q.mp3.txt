 Hi, welcome back to Ask a Monk. Today I thought I would
 answer three questions in a row because
 they seem fairly simple to answer and it wouldn't... I don
't think it would be worth making a video
 about each one. The first one is from Shazruzal. "Dear Yut
adamo, could you explain the importance
 of the Bodhi tree?" The Bodhi tree, what is meant by the
 Bodhi tree, is a fig, a sort
 of a fig tree that is found in India and it happens to be
 the tree that the historical
 Buddha sat under to become enlightened. Now according to
 the legends or what we have of
 the Buddha's teaching, in other world periods and ancient
 times, other Buddhas arose and
 they all had their own type of tree. Basically what it
 means is that the followers, the people
 came after, would always venerate that sort of tree because
 it was in the memory of the
 Buddha. So whenever we see this tree, we'll generally try
 to protect it and maybe put
 a terrace around it. It came to signify the Buddha himself.
 So instead of making a statue
 of the Buddha, they would plant a Bodhi tree or make a
 carving of a Bodhi leaf or you'll
 see people with Bodhi leaves or leaves from this fig tree
 or mala's made of the seeds
 or so on. So basically it just represents the Buddha. It's
 where the Buddha became enlightened.
 When the Buddha sat down to meditate for the final time
 before he was to become enlightened
 to realize the truth that he then taught to people for the
 rest of his life, it was under
 a Bodhi tree, this type of tree. And so it's called a Bodhi
 tree. The word Bodhi means
 enlightenment or wisdom. So it's the tree of enlightenment.
 And the original one is in
 India. It's in Buddha Gaya which is south of a city called
 Gaya in the province of Bihar.
 And actually, as I'm sure people can tell you, I'm not so
 clear on it, but I think this
 is the fourth generation of in a sequence of trees. It was
 killed, cut down and replanted
 from Sri Lanka. The Sri Lankans took a cut from the
 original Bodhi tree, brought it to
 Sri Lanka and when the Bodhi tree was killed in Bodhagaya
 in India, they brought it back
 from Sri Lanka. So you'll find the Maha Bodhi tree there
 which is probably the most, it's
 sort of like the Buddhist Mecca, I guess you could say, the
 most important Buddhist spot
 in the world. It's where Buddhists all year round will go
 to pay respect to the Buddha
 and to his enlightenment. Next question is from Itong Tian.
 Do you make your own food?
 No, I don't make my own food. There are some videos about
 the alms round that I've done
 so you can take a look at those. As a monk, we aren't
 allowed to store food, cook food,
 or store food, cook food. That's about it. We're not
 allowed to store food overnight.
 We're not allowed to cook food. We're not allowed to take
 food for ourselves. So, we're
 not allowed to store food for ourselves. So, we couldn't go
 into a store and buy food
 or even go into someone's cupboard and if they've given us
 permission to go in to take
 food. We're only allowed to eat food that has been given
 freely out of faith or as alms.
 So in India, people would give food as a tradition, they
 would give it to people of all religious
 beliefs, they would believe that they're doing something
 good, something, some sort of spiritually
 beneficial practice. It's a moral, it's a charitable
 practice, it's something that people
 do because it makes them happy and it makes them feel like
 good people. It makes them
 feel like they're doing something of worth. So, they would
 give that. Even nowadays, Buddhists
 would do the same thing. They maybe aren't able to become
 monks themselves or aren't
 able to even go to practice meditation, but they're able to
 practice at home and to do
 good deeds to support the monks and to support people who
 are practicing meditation. So,
 I go to the restaurants, the local Asian restaurants and
 they put food in my bowl every day and
 I've been doing that for the past nine years in Thailand
 and now in America. So, no, we're
 not allowed to cook food. The third question is, "I realize
 that I'm easily angered and
 I've had a hard time to quit smoking because of it. What
 kind of meditation should I do
 to help with this? What kind of tai chi could I also do?"
 Your answer would mean a lot, thanks.
 This is from 915 Rehir. And the reason I'm not going to go
 into detail about this is because
 really I've given a specific set of meditation and I think
 it really does help with anger.
 If you're to put into practice the teachings that I've
 posted, the teachings on how to
 meditate, which I assume you haven't looked at since you're
 asking this question, you'll
 see that there's both the way of meditating and the reasons
 why we meditate. One of the
 reasons is to overcome things like anger, to give up add
ictions like smoking. If you
 are interested specifically in smoking, I think it's an
 interesting adaptation of the meditation
 practice. I see that's not exactly what you're asking, but
 giving up smoking doesn't have
 just to do with anger. It has to do with feelings of guilt,
 which you have to do away with first
 once you've given up the feelings of guilt, then go on to
 the craving once you're able
 to accept the craving and to just see it for what it is,
 then go even deeper on to the
 feeling, the happy feeling that comes with the craving, the
 pleasure that comes with
 wanting. When you can just experience the pleasure and say
 to yourself, "Happy, happy,
 or pleasure, pleasure, feeling, feeling," then you can do
 away with the craving and
 do away with the needing and do away with the getting and
 do away with the smoking when
 you're able to bring it right back to both the thoughts
 about smoking and the feelings
 about smoking. The pleasant feeling of excitement that you
 get when you think of smoking. As
 far as Tai Chi, I don't teach Tai Chi, so you're on your
 own there. You have to find
 someone who teaches that. There are three questions that
 deserve answers, but I think
 the answers are pretty simple. There you have it. Thanks
 for the questions and keep practicing.
