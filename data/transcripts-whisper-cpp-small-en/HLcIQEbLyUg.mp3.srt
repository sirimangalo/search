1
00:00:00,000 --> 00:00:06,130
 Hello, so today I thought I would start my series on how to

2
00:00:06,130 --> 00:00:09,360
 meditate.

3
00:00:09,360 --> 00:00:15,720
 The first thing to explain in the series how to meditate is

4
00:00:15,720 --> 00:00:18,180
 what is meditation?

5
00:00:18,180 --> 00:00:21,810
 So in this video I'll take a look at an answer to this

6
00:00:21,810 --> 00:00:24,260
 question, what is meditation, and

7
00:00:24,260 --> 00:00:29,390
 try to answer the question how meditation accomplishes its

8
00:00:29,390 --> 00:00:30,560
 function.

9
00:00:30,560 --> 00:00:34,670
 And then I'll give a short demonstration of how to practice

10
00:00:34,670 --> 00:00:36,480
 sitting meditation.

11
00:00:36,480 --> 00:00:39,600
 So the first question, what is meditation?

12
00:00:39,600 --> 00:00:44,140
 This is a question which might give different answers, but

13
00:00:44,140 --> 00:00:46,200
 in this case I would take it

14
00:00:46,200 --> 00:00:50,160
 if we look at the actual word meditation and we take it

15
00:00:50,160 --> 00:00:52,360
 back to the origin of the word

16
00:00:52,360 --> 00:00:56,610
 meditation and we can see that it comes from a root very

17
00:00:56,610 --> 00:00:59,160
 similar to the word medication.

18
00:00:59,160 --> 00:01:03,080
 And this is useful because these two words then become

19
00:01:03,080 --> 00:01:04,120
 parallel.

20
00:01:04,120 --> 00:01:08,260
 The word medication is, as we all know it, it means

21
00:01:08,260 --> 00:01:11,080
 something which is used to remedy

22
00:01:11,080 --> 00:01:14,400
 the sicknesses which exist in the body.

23
00:01:14,400 --> 00:01:19,000
 So meditation then is the parallel.

24
00:01:19,000 --> 00:01:23,060
 Meditation is something which cures the sicknesses which

25
00:01:23,060 --> 00:01:24,560
 exist in the mind.

26
00:01:24,560 --> 00:01:27,390
 And if we give this definition of meditation then we can

27
00:01:27,390 --> 00:01:29,000
 understand clearly what it is

28
00:01:29,000 --> 00:01:32,300
 that we're trying to do in the practice of meditation.

29
00:01:32,300 --> 00:01:35,730
 So whereas some people might understand that meditation is

30
00:01:35,730 --> 00:01:37,400
 simply to feel peaceful and

31
00:01:37,400 --> 00:01:42,470
 calm or to get away from one's problems, to find a way to

32
00:01:42,470 --> 00:01:45,720
 escape, to find a quiet place

33
00:01:45,720 --> 00:01:50,050
 and avoid all of the difficulties and all of the problems

34
00:01:50,050 --> 00:01:51,880
 which exist inside.

35
00:01:51,880 --> 00:01:55,860
 Meditation in the understanding and the definition that I

36
00:01:55,860 --> 00:01:58,140
 give to it here means that we come

37
00:01:58,140 --> 00:02:01,820
 to look at and come to understand and come to do away with

38
00:02:01,820 --> 00:02:03,840
 the sicknesses, the things

39
00:02:03,840 --> 00:02:06,860
 which bring suffering to ourselves and the things which

40
00:02:06,860 --> 00:02:08,880
 bring suffering to other people.

41
00:02:08,880 --> 00:02:12,240
 Because these things of course exist inside of ourselves.

42
00:02:12,240 --> 00:02:17,590
 Things like anger or greed or jealousy or stress and worry,

43
00:02:17,590 --> 00:02:19,920
 fear, doubt and so on.

44
00:02:19,920 --> 00:02:23,320
 All the many things which we can label as mental sickness.

45
00:02:23,320 --> 00:02:26,160
 And so meditation the purpose is to go away with these

46
00:02:26,160 --> 00:02:26,800
 things.

47
00:02:26,800 --> 00:02:28,880
 This is an understanding of what is meditation.

48
00:02:28,880 --> 00:02:32,960
 Now the question of how meditation accomplishes its

49
00:02:32,960 --> 00:02:36,160
 function in the sense of being something

50
00:02:36,160 --> 00:02:41,200
 which does away with these causes of suffering.

51
00:02:41,200 --> 00:02:44,840
 Meditation creates a clear understanding of these things.

52
00:02:44,840 --> 00:02:48,130
 So we have sicknesses inside, we actually look at these

53
00:02:48,130 --> 00:02:50,080
 things and we come to understand

54
00:02:50,080 --> 00:02:52,280
 what is the cause of these things.

55
00:02:52,280 --> 00:02:56,390
 When we have suffering in our minds, sadness or depression

56
00:02:56,390 --> 00:02:58,440
 or despair or fear or worry,

57
00:02:58,440 --> 00:03:00,280
 where do these things come from?

58
00:03:00,280 --> 00:03:03,080
 Why is it that these things exist in our mind?

59
00:03:03,080 --> 00:03:05,900
 And when we can see the cause, just like in medicine when

60
00:03:05,900 --> 00:03:07,640
 we can see the cause of a sickness,

61
00:03:07,640 --> 00:03:08,880
 we can do away with the cause.

62
00:03:08,880 --> 00:03:13,870
 When we see what is causing our discomfort in the body, we

63
00:03:13,870 --> 00:03:16,520
 can do away with the discomfort

64
00:03:16,520 --> 00:03:18,160
 by doing away with the cause.

65
00:03:18,160 --> 00:03:19,920
 And the same in meditation.

66
00:03:19,920 --> 00:03:23,480
 So for instance we come to look at the things which exist

67
00:03:23,480 --> 00:03:25,680
 inside, for instance anger.

68
00:03:25,680 --> 00:03:29,230
 Suppose we feel angry inside, we come to look at our own

69
00:03:29,230 --> 00:03:29,960
 anger.

70
00:03:29,960 --> 00:03:33,260
 Instead of going off, when we get angry going off and

71
00:03:33,260 --> 00:03:35,680
 yelling or saying bad things or doing

72
00:03:35,680 --> 00:03:39,040
 bad things, instead we come back inside and look at the

73
00:03:39,040 --> 00:03:40,720
 cause of our suffering.

74
00:03:40,720 --> 00:03:44,370
 But actually the cause is not some other person, the cause

75
00:03:44,370 --> 00:03:47,040
 is not something external to ourselves,

76
00:03:47,040 --> 00:03:51,440
 the cause is really this following after this anger.

77
00:03:51,440 --> 00:03:54,530
 And so instead of following after it, we come to create

78
00:03:54,530 --> 00:03:56,320
 what we call a clear thought.

79
00:03:56,320 --> 00:03:59,660
 The practice of meditation is about creating a clear

80
00:03:59,660 --> 00:04:02,020
 understanding or a clear awareness

81
00:04:02,020 --> 00:04:05,460
 of the things inside of ourselves and by extension of the

82
00:04:05,460 --> 00:04:06,800
 things around us.

83
00:04:06,800 --> 00:04:10,990
 So when we feel anger, instead of following after it or rep

84
00:04:10,990 --> 00:04:13,040
ressing it, we come to stay

85
00:04:13,040 --> 00:04:17,060
 with it, we watch it, we let it arise, let it come and let

86
00:04:17,060 --> 00:04:17,720
 it go.

87
00:04:17,720 --> 00:04:19,240
 How do we do this?

88
00:04:19,240 --> 00:04:22,600
 When we feel anger, instead of saying this is good, this is

89
00:04:22,600 --> 00:04:24,160
 bad, this is me, this is

90
00:04:24,160 --> 00:04:27,420
 mine, this is because of that person, this is because of

91
00:04:27,420 --> 00:04:29,260
 this or that, we focus simply

92
00:04:29,260 --> 00:04:32,610
 on the anger and we use something like a mantra, except

93
00:04:32,610 --> 00:04:34,840
 this time the mantra in this case is

94
00:04:34,840 --> 00:04:39,680
 focused on the ultimate reality, the reality of the anger.

95
00:04:39,680 --> 00:04:44,640
 And we say to ourselves, "Angry, angry, angry, not good or

96
00:04:44,640 --> 00:04:47,600
 bad, not judging it at all, simply

97
00:04:47,600 --> 00:04:51,400
 being with it, angry, angry, angry."

98
00:04:51,400 --> 00:04:55,000
 And this is an example of how we create a clear thought.

99
00:04:55,000 --> 00:04:57,400
 Once we create this, we find our minds are not going after

100
00:04:57,400 --> 00:04:58,560
 it or not, we don't have to

101
00:04:58,560 --> 00:05:01,470
 go in yellow, we don't have to hurt other people and we don

102
00:05:01,470 --> 00:05:02,800
't have to repress it and

103
00:05:02,800 --> 00:05:07,160
 hurt ourselves, we simply are with it.

104
00:05:07,160 --> 00:05:11,740
 This is a, and by extension we can use this with everything

105
00:05:11,740 --> 00:05:14,180
, this is a brief explanation

106
00:05:14,180 --> 00:05:17,560
 of what it means to practice meditation, to cure the things

107
00:05:17,560 --> 00:05:19,240
, the problems which exist

108
00:05:19,240 --> 00:05:20,240
 inside.

109
00:05:20,240 --> 00:05:24,540
 If we feel sad, so we can say to ourselves, "Sad, sad, sad

110
00:05:24,540 --> 00:05:25,840
," and so on.

111
00:05:25,840 --> 00:05:33,270
 And next, how we do this, we start, the way to begin is to

112
00:05:33,270 --> 00:05:36,480
 develop an exercise.

113
00:05:36,480 --> 00:05:38,980
 So just like when children, they're learning how to walk,

114
00:05:38,980 --> 00:05:40,160
 or they can't just get up and

115
00:05:40,160 --> 00:05:44,350
 walk and run and play, they have to slowly, slowly start

116
00:05:44,350 --> 00:05:46,560
 from holding onto a chair or a

117
00:05:46,560 --> 00:05:48,840
 table and then slowly get up and walk.

118
00:05:48,840 --> 00:05:52,520
 When we practice meditation, next, what I'd like to explain

119
00:05:52,520 --> 00:05:54,040
 is we have an exercise to

120
00:05:54,040 --> 00:05:58,360
 give to the person who wishes to learn meditation.

121
00:05:58,360 --> 00:06:01,610
 So instead of sending you off, creating clear understanding

122
00:06:01,610 --> 00:06:03,320
, we start with something very

123
00:06:03,320 --> 00:06:06,560
 simple and we start with a sitting meditation.

124
00:06:06,560 --> 00:06:10,180
 And we're going to use this to help ourselves learn how to

125
00:06:10,180 --> 00:06:12,640
 create clear awareness, not following

126
00:06:12,640 --> 00:06:14,940
 after or repressing.

127
00:06:14,940 --> 00:06:19,260
 The way to do it, in the traditional way, is to sit cross-

128
00:06:19,260 --> 00:06:21,360
legged with your right foot

129
00:06:21,360 --> 00:06:24,250
 in front, or one foot in front of the other leg, this with

130
00:06:24,250 --> 00:06:25,760
 the heel touching the front

131
00:06:25,760 --> 00:06:30,110
 of the foot, not underneath, just in front, and the right

132
00:06:30,110 --> 00:06:32,120
 hand on top of the left hand

133
00:06:32,120 --> 00:06:33,360
 with the thumbs touching.

134
00:06:33,360 --> 00:06:35,560
 Now this is the traditional posture.

135
00:06:35,560 --> 00:06:38,910
 This can also be done on a chair, it can also be done lying

136
00:06:38,910 --> 00:06:41,000
 down, sitting against the wall,

137
00:06:41,000 --> 00:06:44,190
 in any position which is comfortable, which creates a good

138
00:06:44,190 --> 00:06:46,160
 environment for concentration.

139
00:06:46,160 --> 00:06:49,360
 Not something which is so comfortable, of course, that you

140
00:06:49,360 --> 00:06:50,280
 fall asleep.

141
00:06:50,280 --> 00:06:54,400
 It can be done in any posture which the physical body

142
00:06:54,400 --> 00:06:55,320
 allows.

143
00:06:55,320 --> 00:06:58,640
 And then to practice we're going to close our eyes and we

144
00:06:58,640 --> 00:07:00,760
're going to watch the stomach.

145
00:07:00,760 --> 00:07:03,280
 When the breath goes into the body, the stomach will rise.

146
00:07:03,280 --> 00:07:06,800
 It will do so naturally, you don't have to force it or try

147
00:07:06,800 --> 00:07:08,400
 to make it deeper or make

148
00:07:08,400 --> 00:07:12,160
 it shallow or faster or slower.

149
00:07:12,160 --> 00:07:15,860
 Simply watching as it rises, we create a clear thought

150
00:07:15,860 --> 00:07:18,280
 about the rising, not letting our

151
00:07:18,280 --> 00:07:23,640
 minds wander or getting obsessed with the phenomena, just

152
00:07:23,640 --> 00:07:26,600
 saying to ourselves, "Rising."

153
00:07:26,600 --> 00:07:32,980
 And when the breath goes out, the stomach falls, we say, "F

154
00:07:32,980 --> 00:07:36,240
alling, rising, falling,

155
00:07:36,240 --> 00:07:38,920
 rising, falling."

156
00:07:38,920 --> 00:07:41,960
 And we do this for say 10 minutes.

157
00:07:41,960 --> 00:07:46,200
 So we're sitting here, we will sit still for 10 minutes.

158
00:07:46,200 --> 00:07:50,310
 And this is a basic practice to give a foundation, sort of

159
00:07:50,310 --> 00:07:53,000
 a preliminary understanding of how

160
00:07:53,000 --> 00:07:55,760
 meditation works.

161
00:07:55,760 --> 00:07:58,530
 Notice as you practice in this way that there's great

162
00:07:58,530 --> 00:08:00,520
 difficulty for the mind to stay with

163
00:08:00,520 --> 00:08:03,400
 the rising and falling because there will be many other

164
00:08:03,400 --> 00:08:05,120
 objects which arise at the time

165
00:08:05,120 --> 00:08:06,640
 when we're trying to focus.

166
00:08:06,640 --> 00:08:09,450
 So instead of ignoring or trying to block these things out,

167
00:08:09,450 --> 00:08:11,240
 we can use them as our meditation.

168
00:08:11,240 --> 00:08:15,320
 Again, we're trying to focus on reality and we're trying to

169
00:08:15,320 --> 00:08:16,960
 focus on our own body and

170
00:08:16,960 --> 00:08:19,600
 our own mind, our own real nature.

171
00:08:19,600 --> 00:08:22,710
 So when we have, suppose we have pain, instead of blocking

172
00:08:22,710 --> 00:08:24,360
 it out, we simply switch.

173
00:08:24,360 --> 00:08:27,740
 Instead of saying, "Rising, falling," we watch the pain,

174
00:08:27,740 --> 00:08:29,400
 not getting upset or getting

175
00:08:29,400 --> 00:08:32,910
 stressed out or angry about the pain, simply saying to

176
00:08:32,910 --> 00:08:35,360
 ourselves, "Pain, pain," replacing

177
00:08:35,360 --> 00:08:39,930
 the attachment, the thought which gets upset with the

178
00:08:39,930 --> 00:08:42,960
 thought which is purely and clearly

179
00:08:42,960 --> 00:08:43,960
 aware.

180
00:08:43,960 --> 00:08:47,740
 Pain, pain, and we simply say, "Pain, pain," until the pain

181
00:08:47,740 --> 00:08:48,560
 goes away.

182
00:08:48,560 --> 00:08:51,790
 If we are thinking, if the mind starts to think, thinking

183
00:08:51,790 --> 00:08:53,320
 about the past or future,

184
00:08:53,320 --> 00:08:56,240
 whatever kind of thought, we simply say to ourselves, "Th

185
00:08:56,240 --> 00:08:57,920
inking, thinking," thinking

186
00:08:57,920 --> 00:09:01,440
 that goes the rising and falling.

187
00:09:01,440 --> 00:09:03,840
 After we say, "Thinking," then come back.

188
00:09:03,840 --> 00:09:08,340
 We think again, then go out again, thinking, thinking,

189
00:09:08,340 --> 00:09:09,360
 thinking.

190
00:09:09,360 --> 00:09:12,580
 If we have any sort of emotions coming up, if we have

191
00:09:12,580 --> 00:09:15,120
 liking or wanting, we say, "Liking,

192
00:09:15,120 --> 00:09:16,520
 liking or wanting, wanting."

193
00:09:16,520 --> 00:09:20,700
 If we feel angry or frustrated or upset or sad or depressed

194
00:09:20,700 --> 00:09:23,600
, we say, "Angry, angry, frustrated,

195
00:09:23,600 --> 00:09:27,420
 sad," we simply say to ourselves, "Give a name, create a

196
00:09:27,420 --> 00:09:28,640
 clear thought.

197
00:09:28,640 --> 00:09:30,600
 This is only what it is.

198
00:09:30,600 --> 00:09:33,120
 Not good, not bad, not me, not mine."

199
00:09:33,120 --> 00:09:36,640
 And we don't therefore follow after the emotion.

200
00:09:36,640 --> 00:09:39,040
 We feel drowsy, we say drowsy, drowsy.

201
00:09:39,040 --> 00:09:42,490
 If we feel distracted, not focused, we say distracted,

202
00:09:42,490 --> 00:09:43,400
 distracted.

203
00:09:43,400 --> 00:09:46,340
 If we have doubt, doubt about this or that ourselves, the

204
00:09:46,340 --> 00:09:48,520
 practice, say, "Doubting,

205
00:09:48,520 --> 00:09:52,760
 doubting, doubting," and then we come back always to the

206
00:09:52,760 --> 00:09:54,480
 rising and falling.

207
00:09:54,480 --> 00:09:57,970
 This is a short demonstration and a brief explanation of

208
00:09:57,970 --> 00:09:59,960
 how to practice meditation.

