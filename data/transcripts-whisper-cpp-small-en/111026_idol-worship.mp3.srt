1
00:00:00,000 --> 00:00:04,720
 Why do Muslims seem to dislike us so much?

2
00:00:04,720 --> 00:00:07,920
 I've tried to explain to a Muslim friend that we do not

3
00:00:07,920 --> 00:00:09,080
 worship idols

4
00:00:09,080 --> 00:00:14,000
 but it seems to fall on deaf ears.

5
00:00:14,000 --> 00:00:19,430
 It's actually the two issues here. I guess the implication

6
00:00:19,430 --> 00:00:20,800
 that you're making is that

7
00:00:20,800 --> 00:00:25,160
 Muslims dislike us because we worship idols

8
00:00:25,160 --> 00:00:30,800
 and Muslim people are very much against worshiping idols.

9
00:00:30,800 --> 00:00:35,040
 Well you know what? The problem is we do worship idols.

10
00:00:35,040 --> 00:00:37,800
 Sad to say.

11
00:00:37,800 --> 00:00:41,480
 But a lot of Buddhists do worship idols.

12
00:00:41,480 --> 00:00:45,080
 They think of the Buddha as a god

13
00:00:45,080 --> 00:00:49,760
 and more especially when they don't think of the Buddha as

14
00:00:49,760 --> 00:00:50,240
 a god.

15
00:00:50,240 --> 00:00:56,180
 But there are people who wear Buddha images around their

16
00:00:56,180 --> 00:00:56,840
 necks

17
00:00:56,840 --> 00:00:59,040
 thinking that it's going to protect them.

18
00:00:59,040 --> 00:01:01,880
 And that's an understatement. Scott you know this I think.

19
00:01:01,880 --> 00:01:05,840
 I think this is all familiar to you.

20
00:01:05,840 --> 00:01:09,840
 That there are people out there who

21
00:01:09,840 --> 00:01:13,920
 pay millions

22
00:01:13,920 --> 00:01:17,720
 of dollars for a single

23
00:01:17,720 --> 00:01:23,080
 small Buddha image.

24
00:01:23,080 --> 00:01:25,900
 Yeah I'm going to get to that. Actually it is quite a

25
00:01:25,900 --> 00:01:27,200
 generalization.

26
00:01:27,200 --> 00:01:32,120
 But don't worry I'm not going to let it. We will get there.

27
00:01:32,120 --> 00:01:35,720
 So

28
00:01:35,720 --> 00:01:39,000
 I mean there is a valid criticism there.

29
00:01:39,000 --> 00:01:44,000
 That this Relyan people

30
00:01:44,000 --> 00:01:50,800
 they look down upon this as a good thing.

31
00:01:50,800 --> 00:01:53,700
 They say you don't wear a Buddha image around your neck.

32
00:01:53,700 --> 00:01:55,040
 That's an image of the Buddha.

33
00:01:55,040 --> 00:01:58,440
 And the Thai people are all like yeah buying and selling

34
00:01:58,440 --> 00:02:00,720
 these Buddha images.

35
00:02:00,720 --> 00:02:03,350
 And they have this way of feeling them. You touch the

36
00:02:03,350 --> 00:02:04,880
 Buddha image with your thumb.

37
00:02:04,880 --> 00:02:07,890
 There was this German man who claimed to be able to do it.

38
00:02:07,890 --> 00:02:09,440
 So he was showing me.

39
00:02:09,440 --> 00:02:16,070
 Touching the Buddha image. And he would say this one is

40
00:02:16,070 --> 00:02:16,720
 powerful.

41
00:02:16,720 --> 00:02:19,070
 Or this one is not powerful. Or this one has this kind of

42
00:02:19,070 --> 00:02:21,120
 power. This one is that kind of power.

43
00:02:21,120 --> 00:02:25,120
 Thai people have this word kang. I think it is.

44
00:02:25,120 --> 00:02:29,840
 Kang wuch means powerful.

45
00:02:29,840 --> 00:02:33,260
 And so they know which one is powerful and which one is not

46
00:02:33,260 --> 00:02:33,280
.

47
00:02:33,280 --> 00:02:35,900
 Nowadays I don't even know if it's still a fad. But when I

48
00:02:35,900 --> 00:02:37,600
 was in Thailand there was this

49
00:02:37,600 --> 00:02:43,160
 disgusting fad of... It wasn't even Buddha images anymore.

50
00:02:43,160 --> 00:02:47,440
 It was this angel called Chhatukham.

51
00:02:47,440 --> 00:02:52,610
 Chhatukham which is a fictitious being that apparently that

52
00:02:52,610 --> 00:02:53,680
 is supposed to...

53
00:02:53,680 --> 00:02:55,960
 It was just made up. There's even a story about where it

54
00:02:55,960 --> 00:02:57,280
 was made up. It was made up

55
00:02:57,280 --> 00:03:05,440
 based on a Hindu. Based on not even exactly but based on a

56
00:03:05,440 --> 00:03:07,120
 Hindu deity.

57
00:03:08,800 --> 00:03:12,780
 That's supposed to guard this this Jaitya, this Pagoda in

58
00:03:12,780 --> 00:03:14,160
 Thailand.

59
00:03:14,160 --> 00:03:19,270
 And it became this huge fad. And people got rich off of

60
00:03:19,270 --> 00:03:23,760
 these things. Because people are...

61
00:03:23,760 --> 00:03:29,150
 Because other people can be so dumb and would buy them. So

62
00:03:29,150 --> 00:03:32,800
 even monasteries became quite wealthy

63
00:03:32,800 --> 00:03:36,720
 by having special edition. It was like those pong. You

64
00:03:36,720 --> 00:03:37,760
 remember pong?

65
00:03:37,760 --> 00:03:45,910
 Those little milk bottle caps collecting pong? I think it

66
00:03:45,910 --> 00:03:48,000
 was called pong.

67
00:03:48,000 --> 00:03:52,550
 You know it became the vegan collector's items. And they

68
00:03:52,550 --> 00:03:54,400
 were these round discs. They look like pong

69
00:03:54,400 --> 00:03:57,480
 actually. And you would collect them. And so they had

70
00:03:57,480 --> 00:03:59,920
 special edition and all of this garbage.

71
00:04:02,320 --> 00:04:05,460
 People say the same about buddha images. They have this

72
00:04:05,460 --> 00:04:11,680
 huge ceremony to bless and to christen

73
00:04:11,680 --> 00:04:14,400
 a buddha image and make it powerful. And they put all sorts

74
00:04:14,400 --> 00:04:18,720
 of spells over it. And all sorts of

75
00:04:18,720 --> 00:04:22,220
 stuff. Why I'm even talking about this is because there's

76
00:04:22,220 --> 00:04:24,160
 really a point that has to be made.

77
00:04:24,160 --> 00:04:28,720
 Yeah I'm really kind of beating around the bush. I'm sorry.

78
00:04:28,720 --> 00:04:30,720
 But there's a point that has to be made

79
00:04:30,720 --> 00:04:38,850
 here. Is that the... Well there's really a point there.

80
00:04:38,850 --> 00:04:42,400
 Because the buddha never taught us to make

81
00:04:42,400 --> 00:04:45,300
 images of anything. You know he wasn't critical. It was

82
00:04:45,300 --> 00:04:47,440
 like yeah big deal. Make an image. Don't

83
00:04:47,440 --> 00:04:50,480
 make an image. There was... The question never came up.

84
00:04:50,480 --> 00:04:55,280
 This is a famous story and one that we always

85
00:04:55,280 --> 00:05:03,200
 tell is that the first buddha images were greek. The

86
00:05:03,200 --> 00:05:07,920
 records that we have of buddhist india

87
00:05:07,920 --> 00:05:12,110
 are that they never made buddha images. They would never

88
00:05:12,110 --> 00:05:14,640
 put the buddha. And in fact they avoided it.

89
00:05:14,640 --> 00:05:18,880
 So when they would want it to tell a story and they would

90
00:05:18,880 --> 00:05:20,880
 inscribe it on rocks. The kings would do

91
00:05:20,880 --> 00:05:23,620
 this. Instead of putting the buddha they would put a wheel

92
00:05:23,620 --> 00:05:25,280
 of the dhamma or they would put a

93
00:05:25,280 --> 00:05:29,710
 bodhi leaf. Or they would put just an empty. You know when

94
00:05:29,710 --> 00:05:32,000
 they want to show the buddha on the

95
00:05:32,000 --> 00:05:36,190
 bodhisattva riding this horse. They would put just the

96
00:05:36,190 --> 00:05:39,440
 empty horse. Because they would never...

97
00:05:39,440 --> 00:05:43,670
 They would never... It was never done. And so the point the

98
00:05:43,670 --> 00:05:46,560
 point I'm trying to make is that

99
00:05:48,480 --> 00:05:53,710
 we really have a problem there. In that we really are

100
00:05:53,710 --> 00:06:00,320
 worshiping idols. And that should really stop.

101
00:06:00,320 --> 00:06:05,500
 And of course the defense that's always used that I haven't

102
00:06:05,500 --> 00:06:06,640
 used is that

103
00:06:06,640 --> 00:05:46,790
 we use the buddha image in order to recollect the buddha.

104
00:05:46,790 --> 00:06:15,840
 Which I think is can be a valid

105
00:06:16,800 --> 00:06:21,930
 excuse. But it sets you up for a lot of danger. Especially

106
00:06:21,930 --> 00:06:25,680
 when dealing with people who believe

107
00:06:25,680 --> 00:06:31,680
 graven images are sin and punishable by death or so on.

108
00:06:31,680 --> 00:06:37,400
 So okay so let's get back. Try to get back. Relate this

109
00:06:37,400 --> 00:06:39,840
 back to the question.

110
00:06:41,920 --> 00:06:52,700
 Because not all Muslims dislike us. But it is true I think

111
00:06:52,700 --> 00:06:54,080
 that there is a

112
00:06:54,080 --> 00:07:00,240
 there's definitely a a bad feeling between

113
00:07:00,240 --> 00:07:05,200
 what is essentially an atheistic religion

114
00:07:08,880 --> 00:07:11,600
 and the theistic religions.

115
00:07:11,600 --> 00:07:18,950
 I mean Hinduism is I guess an exception. Because in Hindu

116
00:07:18,950 --> 00:07:21,600
ism they don't exactly worship the gods.

117
00:07:21,600 --> 00:07:26,230
 They interact with them and there's general belief that one

118
00:07:26,230 --> 00:07:27,680
 can become god.

119
00:07:27,680 --> 00:07:31,800
 Judaism also doesn't have so much trouble. Because in

120
00:07:31,800 --> 00:07:35,120
 Judaism they can argue with god and

121
00:07:36,640 --> 00:07:42,520
 god isn't god is a very much a part of the universe. But he

122
00:07:42,520 --> 00:07:45,680
 doesn't play the central role in

123
00:07:45,680 --> 00:07:48,800
 what it means to be Jewish. So Jewish people can often come

124
00:07:48,800 --> 00:07:50,400
 and practice meditation.

125
00:07:50,400 --> 00:07:55,360
 But I think there's no beating around the bush that Muslims

126
00:07:55,360 --> 00:07:56,800
 people who are practicing Muslims

127
00:07:56,800 --> 00:08:02,070
 do have a problem with just as Christians do with people

128
00:08:02,070 --> 00:08:04,400
 who don't believe in god.

129
00:08:06,400 --> 00:08:13,960
 And that's aggravated when we take some graven image or

130
00:08:13,960 --> 00:08:18,240
 idol as you say an idol as

131
00:08:18,240 --> 00:08:25,370
 a substitute. Because to them that's the ultimate blasphemy

132
00:08:25,370 --> 00:08:29,440
. It's like putting a piece of rock

133
00:08:29,440 --> 00:14:32,430
 on the same level as the most important thing in the

134
00:14:32,430 --> 00:08:33,280
 universe which is their god.

135
00:08:35,040 --> 00:08:38,190
 So regardless of I mean obviously it's a silly thing to do

136
00:08:38,190 --> 00:08:39,200
 for us to be

137
00:08:39,200 --> 00:08:43,600
 paying so much attention to these images. But there's a

138
00:08:43,600 --> 00:08:46,400
 very valid point there that it's going

139
00:08:46,400 --> 00:08:49,740
 to get us into a lot of heat with those people who are feel

140
00:08:49,740 --> 00:08:52,400
 very strongly with god unnecessarily.

141
00:08:52,400 --> 00:08:55,880
 I think and why this is actually an important point is

142
00:08:55,880 --> 00:08:58,240
 because this is part of what helped

143
00:08:58,240 --> 00:09:01,310
 wipe out Buddhism in India. When the Muslims came to India

144
00:09:01,310 --> 00:09:03,040
 and they weren't it wasn't just

145
00:09:03,040 --> 00:09:06,290
 because they were Muslims but because of their militaristic

146
00:09:06,290 --> 00:09:10,480
 sort of people. But Muslim Islam

147
00:09:10,480 --> 00:09:13,230
 told them that this was wrong and so it was one of the

148
00:09:13,230 --> 00:09:14,800
 first things to go with these idol

149
00:09:14,800 --> 00:09:17,190
 worshipers which would were the Buddhists who had their

150
00:09:17,190 --> 00:09:17,840
 Buddha images.

151
00:09:22,480 --> 00:09:26,570
 Hinduism was much better, much more like a chameleon, much

152
00:09:26,570 --> 00:09:28,480
 better to fit easier to fit in.

153
00:09:28,480 --> 00:09:31,980
 And much harder to wipe out. But Buddhism is pretty easy to

154
00:09:31,980 --> 00:09:34,720
 wipe out. Especially when you have

155
00:09:34,720 --> 00:09:40,080
 have these Buddha images and temples and so on. If monks

156
00:09:40,080 --> 00:09:42,320
 were just people wearing rags living in

157
00:09:42,320 --> 00:09:46,710
 the forest I think it'd be a lot harder to wipe them out.

158
00:09:46,710 --> 00:09:50,080
 But when they're sitting ducks you know

159
00:09:50,080 --> 00:09:56,280
 getting fat off of donations living in in opulent golden

160
00:09:56,280 --> 00:10:02,240
 palaces. Not a hard target to hit.

161
00:10:02,240 --> 00:10:10,850
 And I think that can be extrapolated to a more modern

162
00:10:10,850 --> 00:10:13,760
 example. Buddhism is not

163
00:10:14,720 --> 00:10:20,380
 making much headway on a monastic level into the rest of

164
00:10:20,380 --> 00:10:22,560
 the world. Because

165
00:10:22,560 --> 00:10:27,180
 Buddhists when they build a Buddhist monastery first of all

166
00:10:27,180 --> 00:10:29,040
 they call it a temple and they have their

167
00:10:29,040 --> 00:10:34,550
 priests. And second of all they the way they go about it

168
00:10:34,550 --> 00:10:38,080
 for instance Thai monasteries they build

169
00:10:38,080 --> 00:10:43,380
 it out of gold as much gold as possible and rich beautiful

170
00:10:43,380 --> 00:10:46,960
 carpets and huge Buddha images and lots

171
00:10:46,960 --> 00:10:56,520
 of ceremonies and so on. It's an affront really to people's

172
00:10:56,520 --> 00:11:01,120
 sensibilities. People go there and

173
00:11:01,120 --> 00:11:05,950
 they're certainly not looking for that. It's making it very

174
00:11:05,950 --> 00:11:08,800
 difficult for us to bring Buddhism to

175
00:11:08,800 --> 00:11:14,930
 people by it's not just overkill it's total misrepresent

176
00:11:14,930 --> 00:11:17,040
ation of the Buddhist teaching

177
00:11:17,040 --> 00:11:20,300
 because gold has no place in the monastery. A monk can't

178
00:11:20,300 --> 00:11:24,240
 even touch gold. Touching gold

179
00:11:24,800 --> 00:11:28,910
 is an offense. Touching jewels touching anything any

180
00:11:28,910 --> 00:11:32,080
 precious material like that is an offense.

181
00:11:32,080 --> 00:11:45,440
 So it's a real danger and I think the idols the the Buddha

182
00:11:45,440 --> 00:11:46,960
 images are a part of that.

183
00:11:46,960 --> 00:11:50,680
 We don't need Buddha images to spread Buddhism. It's the D

184
00:11:50,680 --> 00:11:53,600
hamma which will spread Buddhism.

185
00:11:55,440 --> 00:11:58,520
 And yes you have the excuse that it helps us to remember

186
00:11:58,520 --> 00:12:01,600
 the Buddha and think about him.

187
00:12:01,600 --> 00:12:06,920
 So I'm not saying that a Buddha image is totally negative

188
00:12:06,920 --> 00:12:09,600
 but our obsession with them and

189
00:12:09,600 --> 00:12:14,420
 our placing them because you know if in that case well why

190
00:12:14,420 --> 00:12:16,480
 not have a Buddha image

191
00:12:18,080 --> 00:12:21,980
 placed somewhere in its own place instead of putting it up

192
00:12:21,980 --> 00:12:23,680
 in front of an altar for you

193
00:12:23,680 --> 00:12:29,640
 to worship and give donations to and so on. You have it up

194
00:12:29,640 --> 00:12:35,360
 as a reminder or something that you

195
00:12:35,360 --> 00:12:41,310
 can go and look at. Whatever. It's certainly not the most

196
00:12:41,310 --> 00:12:45,200
 important thing. The Buddha's Rupa-Caya

197
00:12:45,200 --> 00:12:48,480
 was not his most important Gaya, his most important body.

198
00:12:48,480 --> 00:12:55,250
 I think that I had something else to say but I think that's

199
00:12:55,250 --> 00:12:56,720
 that's saying quite a bit already

200
00:12:56,720 --> 00:13:05,760
 is that the Buddha the there's the danger the danger in the

201
00:13:05,760 --> 00:13:07,040
 Buddha image. I mean it's

202
00:13:07,040 --> 00:13:10,320
 really an affront to people who believe other things and

203
00:13:10,320 --> 00:13:12,560
 that's really the point is we don't

204
00:13:12,560 --> 00:13:16,760
 have to be so obvious about who we are. As I said a monk

205
00:13:16,760 --> 00:13:20,240
 living in the forest really is not a

206
00:13:20,240 --> 00:13:24,810
 threat to anyone. They still might go out and kill them

207
00:13:24,810 --> 00:13:27,120
 because oh these people don't believe in God.

208
00:13:27,120 --> 00:13:39,120
 There's not so much of a problem there. The other part of

209
00:13:39,120 --> 00:13:41,520
 the answer I guess that I should say is

210
00:13:41,520 --> 00:13:44,140
 it's because these people have wrong views and when people

211
00:13:44,140 --> 00:13:46,560
 have wrong views they tend to be

212
00:13:46,560 --> 00:13:51,600
 what to say judgmental but here I am judging them.

213
00:13:51,600 --> 00:13:55,040
 But you know as people who hold on to views they believe

214
00:13:55,040 --> 00:13:56,960
 this is right and nothing else is right

215
00:13:56,960 --> 00:14:01,280
 and so they they're apt to criticize anyone who just

216
00:14:01,280 --> 00:14:04,480
 disagrees. They're not just criticized. They

217
00:14:04,480 --> 00:14:08,530
 might even persecute and it's not just the Muslims. The

218
00:14:08,530 --> 00:14:11,120
 Christians were horrible about it. The Spanish

219
00:14:11,120 --> 00:14:15,800
 Inquisition, the Crusades. It has to do with belief. The

220
00:14:15,800 --> 00:14:18,800
 stronger your belief the less tolerant you are

221
00:14:18,800 --> 00:14:27,020
 of people who disagree. So the word atheist is just a shock

222
00:14:27,020 --> 00:14:32,240
 to people. It's a four-letter word.

