 Well, good evening, everyone.
 I'm not sure if that's any better.
 I'm broadcasting live May 7th.
 Today's quote is about water.
 From the Melinda Panha, which is probably, I would say,
 an underappreciated source of insight.
 It's much later, many years after the Buddha passed away.
 I think at least a few hundred, maybe more.
 But it's very well written, very well organized,
 and it's got much that is insightful about the Buddha's
 teaching.
 It's a lot of questions.
 It's the questions of King Melinda, or Meneander, I think.
 It's a Greek king, identified with a Greek king,
 who conquered India or something.
 He came in contact with these Buddhist monks
 and asked them all sorts of questions.
 Most of them couldn't answer the question.
 They directed him to Nagasena,
 a very well-learned monk who was skilled in debate
 and skilled in oration,
 and skilled in answering questions.
 It's many, many questions. It's quite a large volume.
 It's actually available on the Internet, an old translation
,
 but pretty good.
 You can find it at sacred-texts.org.
 It's worth reading.
 But this is about water.
 It's a comparison, and he makes a lot of comparisons in the
 book.
 He talks about nirvana, using a lot of different
 comparisons, similes.
 But here he's talking about a meditator.
 Let's see if we can find the word for student of meditation
.
 So the question is,
 "Aapasa pancha unga ningahitabhanti?"
 "Katama nidani pancha unga ningahitabhanti?"
 Oh, yeah.
 I think it's part of a larger text.
 He's asking, "What are the five ways, five things, factors
 of water
 that one should grasp, should take up?"
 So how should one be like water?
 I just want to find the word.
 Yoga vatjara.
 Someone who is practicing yoga, basically.
 So this is how the word yoga was used.
 Yoga was used to mean exertion.
 But yoga vatjara is a word that came to mean.
 It was probably adopted from Hinduism, but it was used in
 Buddhism
 to mean someone who is on the spiritual path,
 someone who is exerting themselves.
 We also call them yogi.
 In Thai they use this word a lot.
 They call meditators yogis.
 Yogi, they change it with the word.
 Someone who is practicing yoga is a yogi.
 But it doesn't mean yoga in the modern sense.
 Yoga just means one.
 Because it comes from the word yungu, which means to be
 connected.
 It's actually where I think the word yuta comes from, from
 my name.
 Yuta meaning a yoke, so the ox would have a yoke
 and would be tied to the work.
 So we're comparing water to such a person.
 How should such a person, someone who is undertaking the
 training
 of the Buddha, morality, concentration, wisdom,
 how should they be like water?
 Water is a good object for comparison.
 Why? Because water is naturally calm.
 Water is something that is still, smooth,
 has a reflection in the sense of being perfectly smooth and
 unshaken.
 In the same way a meditator should be like a pool of water,
 dispelling trickery, cajolary, insinuation and dissembling.
 It should be well poised, unshaken, untroubled and quite
 pure by nature.
 Water to us humans, water is like the purest substance.
 It's what we survive on. We need water to survive.
 We're just, funny, we're just talking about water today.
 I was at McMaster University at the Peace Studies booth.
 We were talking to prospective students about the Peace
 Studies program.
 And we were talking about water because the Peace Studies
 department
 has gotten interested in the water situation.
 It's the social justice aspect of Peace Studies.
 I'm not really involved in it.
 So they're trying to address the issue like in Flint,
 Michigan,
 the issue with the First Nations people here,
 not having clean water to drink and to use,
 having polluted water, having their water contaminated with
 chemicals.
 Toxins.
 And we're talking about Taoism.
 One of the people working at the booth had quite a
 conversation with her
 about many things, but one of them was water, how water is
 basically this.
 So I think she'd be interested in this quote.
 She'd probably send it to her.
 Water is pure, water is unshaken, water is calm.
 Water is nurturing, I think she would say,
 because she's into this idea of nurturing.
 She sees it as sort of a feminine quality.
 So we're talking about feminism and feminine qualities and
 masculine qualities.
 Compassion being feminine.
 I mean, traditionally being understood,
 but we're trying to avoid the words feminine and masculine,
 but talking about the different energies in the world.
 We live in a very "masculine world,"
 and so there's a certain type of energy that one might call
 masculine, hard.
 In the end we use the words hard and soft.
 Aggressive, maybe.
 Water is not these things.
 So fire and water, you might say.
 But here it's water is purity, water is clarity, water is
 calm.
 So the first one is this unshaken.
 As a meditator we should be unshaken by the vicissitudes of
 life,
 by the experiences in our meditation, because they'll try
 to shake you.
 And then they'll shake you again, and they'll shake you
 in new and different ways all the time.
 Things will change, and then that way that they change will
 change.
 You have to be adaptive and flexible.
 You have to be clever, and you have to be present.
 Because as soon as you become static, you get carried away
 by the vicissitudes of life.
 The second one is water is poised and naturally cool.
 And so yoga vatsaras should be compassionate,
 should be possessed of patience, should be cool in this way
.
 Patience, love and mercy.
 And they should not be hot-headed, they should not be quick
 to anger,
 they should not be irritable, they should not be impatient.
 Patience is very important.
 Patience with yourself, patience with other people,
 patience.
 And patience with things you want as well,
 just immediately giving up to being controlled by your
 desires.
 As water makes the impure pure, even so, one should be pure
,
 one should have no faults, one should not have any reason
 to be reprimanded by the wise,
 wise people should not be able to sense your one,
 no matter where they are, in private or in public.
 They should be pure in their deeds, in their speech.
 As water is wanted by everyone, even so a meditator should
 be the kind of person who is appreciated,
 you should work to make yourself not a burden, not a source
 of irritation for others,
 because this is problematic both for others and for
 yourself, it creates suffering.
 Obviously, you should not be such a person.
 You should think of water as being,
 water as being valuable, as being pure.
 Whenever someone, someone sees pure water,
 there is no, something negative about water,
 and it's very, very much desired and appreciated.
 We should be like water in that way.
 And finally, water troubles no one, it's kind of the same.
 The first one is, people think positively,
 and the next one is people should not think negatively.
 Don't think negatively of water, they think positively of
 it.
 So a yoga vatyar I should not do anything wrong that makes
 others unhappy.
 Water doesn't hurt people.
 Well, still water doesn't.
 Water actually can be quite troublesome in certain
 instances.
 You can drown with water.
 Water can scald you if it's too hot.
 But here this, the connection, the comparison is with a
 still forest pool,
 calm water, pure water, it's valuable and appreciated.
 I mean, it's a good imagery, it's the kind of thing that we
 can think of
 when you want to think of what kind of a person you'd like
 to be.
 As a meditator, what should I be?
 And water is something.
 It comes easily to mind, of course, because it's so much a
 part of our life.
 And when you think about the purity of water and the still
ness of a pool of water,
 the smoothness of the surface,
 you think about the cleansing nature of water,
 you think about how precious it is, how perfect it is.
 Something to aspire for, aspire towards.
 So, a little bit of Dhamma.
 Today, as I said, I was at McMaster, and then we went to
 see a new house.
 We're thinking about moving to a new house just to get more
 rooms
 because we have many people wanting to come and meditate
 here.
 So we went to look at a house we're talking about.
 We're going to try to look at several different houses
 and see which one is most suitable.
 One today was good.
 Actually, maybe a little smaller than this house, but more
 rooms.
 So, I have to think about whether it might feel a little
 claustrophobic because it's small.
 People have to stay here for a long time, maybe they need
 more space.
 You pack a lot of people into one small house,
 you have to think about that.
 Anyway, any questions?
 [Silence]
 What are your thoughts on the new Kadampa Buddhism?
 I don't have any thoughts on the new Kadampa Buddhism.
 I kind of like to shy away, and I'm happy that I don't know
 things about many things
 because otherwise, as happened with the Lotus Sutra, I tend
 to be somewhat opinionated.
 So, I kind of try to avoid questions like, "What do you
 think of X?"
 unless X is something within our tradition.
 Pretty sure it's Tibetan.
 But, yeah, if a Google search returns many results claiming
 that something is a cult,
 it's a good reason to be wary.
 You should start your own center, start your own group.
 Have people invite people over to your house, start a meet
up.
 Go to meetup.com and start a meditation group.
 Then you can come on our hangout, you can broadcast, and I
 can lead you.
 I can lead you in meditation or something.
 I can give a talk to your group.
 Everyone should start groups in their own area.
 We can have this big network.
 We all meditate together.
 We call it the New Siri Mungalow Buddhism.
 The Siri Mungalow tradition.
 When we're doing walking meditation with our kids, is it as
 effective
 if they like to walk at a much faster pace?
 Probably not.
 I would say walking meditation for young kids is difficult,
 because they tend to be impatient.
 If you want to teach kids how to meditate, it depends on
 their age, I suppose.
 But absolutely, they shouldn't walk fast.
 It's not about walking, it's about being aware of the
 movements of the feet.
 It's not an easy thing.
 Kids tend to be very excited and full of energy, they have
 a hard time being patient.
 So if you can teach them patience, that would be awesome.
 But walking meditation requires patience.
 So that's really the question.
 Can your kids develop patience?
 Because you have to walk slowly.
 You need to develop patience.
 They're walking fast because they're impatient.
 I would think.
 When I meditate, I feel claustrophobic.
 Well, have you read my booklet on how to meditate?
 If you haven't, that's where I'd recommend you start.
 It might help with your claustrophobia.
 Because if you feel that, it might be a fear, right?
 Phobia.
 So then you would say afraid, afraid, or worried, worried,
 or anxious, anxious.
 But if you haven't read my booklet, I recommend that.
 Why avoid these kinds?
 Because I haven't really done so much to answer your
 question,
 I haven't talked about details, because it's actually quite
 simple to fix
 if you're practicing the meditation as I teach, as we teach
.
 So there's not like any special tips for things like claust
rophobia.
 I mean, the thing is, these conditions don't exist.
 You feel fear, and that's the key, is that there's a
 feeling of fear that arises.
 And so the practice that we follow would be to say to
 yourself,
 afraid, afraid.
 But to read my booklet, I hope, I think it outlines that in
 fairly good detail.
 [silence]
 Any other questions?
 I'm really serious about this starting Dhamma groups in
 your area.
 I think it'd be great, and we can do it like once a week.
 We could all meet online, and I could give a talk.
 Or you could just do it every day of the week and just come
 on at 9 p.m. with your group
 and join our hangout.
 Of course, I suppose some people don't want their
 meditation broadcast on the internet.
 I suppose there's that.
 Well, one day a week we could do a private hangout
 that wouldn't have to be broadcast on the internet,
 although it's nice to have the internet community involved.
 Something to think about.
 I have done that though.
 Private Skype calls to groups.
 There was a group in Texas who wanted me to lead them in
 meditation once a week
 and have done online stuff like that.
 It's worth thinking about.
 Okay.
 And I'll say good night.
 Tomorrow, Mother's Day, I'm going to Mississauga for the
 Sri Lankan...
 Wissaka Celebration.
 Wissaka Poonja Celebration.
 Wissaka Poonami, the full moon of the month of Wissaka.
 Anyway, have a good night.
