1
00:00:00,000 --> 00:00:23,880
 In the practice of meditation, as with everything else we

2
00:00:23,880 --> 00:00:29,000
 do, we have to not only practice,

3
00:00:29,000 --> 00:00:38,730
 not only dedicate our time through the practice, but we

4
00:00:38,730 --> 00:00:41,000
 also have to practice in such a way

5
00:00:41,000 --> 00:00:47,310
 as to be sure that we're going to get results from the

6
00:00:47,310 --> 00:00:49,000
 practice.

7
00:00:49,000 --> 00:00:56,540
 It's not true that simply walking and sitting for long

8
00:00:56,540 --> 00:01:00,000
 periods at a time or long,

9
00:01:00,000 --> 00:01:06,700
 extended period of days and weeks and months and years is

10
00:01:06,700 --> 00:01:14,000
 sure to bring results.

11
00:01:14,000 --> 00:01:18,450
 It should go without saying that our practice needs to have

12
00:01:18,450 --> 00:01:24,000
 certain qualities about it

13
00:01:24,000 --> 00:01:31,120
 in order for us to guarantee some kind of success, some

14
00:01:31,120 --> 00:01:37,000
 sort of results from our practice.

15
00:01:37,000 --> 00:01:49,440
 Why this is, is because in order to succeed in the practice

16
00:01:49,440 --> 00:01:52,000
, in order to gain results,

17
00:01:52,000 --> 00:01:59,570
 we have to first gain this strength and a certain fortitude

18
00:01:59,570 --> 00:02:01,000
 of mind.

19
00:02:01,000 --> 00:02:04,650
 Our confidence has to be very strong. Our effort has to be

20
00:02:04,650 --> 00:02:06,000
 very strong.

21
00:02:06,000 --> 00:02:09,450
 Our mindfulness has to be very strong. Our concentration

22
00:02:09,450 --> 00:02:11,000
 has to be very strong.

23
00:02:11,000 --> 00:02:15,000
 And our wisdom has to be very strong.

24
00:02:15,000 --> 00:02:24,000
 Without these five strengths, our mind is too weak.

25
00:02:24,000 --> 00:02:29,880
 Our mind will be unable to overcome the evil, the unwholes

26
00:02:29,880 --> 00:02:31,000
omeness, the darkness,

27
00:02:31,000 --> 00:02:36,540
 and the cloudedness that's inside of us. We won't be able

28
00:02:36,540 --> 00:02:38,000
 to see clearly

29
00:02:38,000 --> 00:02:44,000
 as a result of the defilements which we carry around.

30
00:02:44,000 --> 00:02:47,000
 And it goes without saying that our ordinary state of mind

31
00:02:47,000 --> 00:02:49,000
 is not strong enough,

32
00:02:49,000 --> 00:02:58,340
 is weak in many ways, is lacking in many of these qualities

33
00:02:58,340 --> 00:02:59,000
.

34
00:02:59,000 --> 00:03:03,140
 And so unless we actually change our minds or build up this

35
00:03:03,140 --> 00:03:06,000
 strength and this fortitude of mind,

36
00:03:06,000 --> 00:03:10,440
 we can practice for years and years and years and never

37
00:03:10,440 --> 00:03:15,000
 truly become enlightened.

38
00:03:15,000 --> 00:03:26,280
 We have to work to gain these qualities of mind. We have to

39
00:03:26,280 --> 00:03:31,000
 work again and again.

40
00:03:31,000 --> 00:03:34,960
 And so sometimes we might ask, "Well, what are the

41
00:03:34,960 --> 00:03:44,000
 qualities that we need to...

42
00:03:44,000 --> 00:03:52,310
 have, we need to possess in order to be sure that our

43
00:03:52,310 --> 00:03:54,000
 practice is going to be a success,

44
00:03:54,000 --> 00:03:58,460
 is going to bring results?" And so we have this very famous

45
00:03:58,460 --> 00:04:00,000
 group of qualities

46
00:04:00,000 --> 00:04:09,120
 that the Lord Buddha said are a guarantee of success, given

47
00:04:09,120 --> 00:04:13,000
 all other things being equal.

48
00:04:13,000 --> 00:04:17,650
 If there is the ability to succeed in something, if it's

49
00:04:17,650 --> 00:04:21,000
 not blocked by fate or circumstance,

50
00:04:21,000 --> 00:04:26,180
 or as we say in Buddhism, as we would explain in Buddhism,

51
00:04:26,180 --> 00:04:27,000
 karma,

52
00:04:27,000 --> 00:04:30,460
 if there is still the chance to succeed in something, as

53
00:04:30,460 --> 00:04:37,000
 there surely is with all of us in regards to meditation,

54
00:04:37,000 --> 00:04:44,310
 then it is these four dhammas, this set of qualities of

55
00:04:44,310 --> 00:04:48,000
 mind that will make or break our practice,

56
00:04:48,000 --> 00:04:52,000
 that will allow us to succeed or cause us to fail.

57
00:04:52,000 --> 00:04:55,120
 Their presence will allow us to succeed. Their absence will

58
00:04:55,120 --> 00:04:57,000
 cause us to fail again and again,

59
00:04:57,000 --> 00:05:03,120
 until finally we can incorporate these four qualities of

60
00:05:03,120 --> 00:05:06,000
 mind into our practice.

61
00:05:06,000 --> 00:05:11,000
 So these four are called the "ithi-bhaada".

62
00:05:11,000 --> 00:05:15,540
 "Ithi" means power or strength or success or fortitude of

63
00:05:15,540 --> 00:05:17,000
 mind, as I said,

64
00:05:17,000 --> 00:05:21,820
 the power that we're looking for, to have strong confidence

65
00:05:21,820 --> 00:05:23,000
, strong efforts,

66
00:05:23,000 --> 00:05:27,000
 strong mindfulness, strong concentration and strong wisdom,

67
00:05:27,000 --> 00:05:28,000
 the strength.

68
00:05:28,000 --> 00:05:34,070
 "Bhaada" means the path or the practice or the items or

69
00:05:34,070 --> 00:05:38,000
 those things that lead to.

70
00:05:38,000 --> 00:05:53,000
 So these four dhammas are very important.

71
00:05:53,000 --> 00:06:03,940
 The first is called "chanda", which means zeal or content

72
00:06:03,940 --> 00:06:07,000
ment or interest,

73
00:06:07,000 --> 00:06:11,990
 you could say, or interest in practicing, desire to

74
00:06:11,990 --> 00:06:14,000
 practice,

75
00:06:14,000 --> 00:06:24,650
 or contentment with practice, intention to practice, whole

76
00:06:24,650 --> 00:06:26,000
heartedness,

77
00:06:26,000 --> 00:06:29,450
 this is the word "chanda". I guess simply it just means

78
00:06:29,450 --> 00:06:34,000
 desire, our desire to practice.

79
00:06:34,000 --> 00:06:44,490
 Number two, "viriya", which means effort, the effort to

80
00:06:44,490 --> 00:06:45,000
 practice.

81
00:06:45,000 --> 00:06:53,620
 Number three is "chitta", which means the mind or keeping

82
00:06:53,620 --> 00:06:55,000
 something in mind,

83
00:06:55,000 --> 00:07:00,000
 thinking about something, you could say it means thought,

84
00:07:00,000 --> 00:07:02,000
 thinking about the practice,

85
00:07:02,000 --> 00:07:06,000
 keeping the practice in mind.

86
00:07:06,000 --> 00:07:12,000
 And number four, "vimamsa", which means considering,

87
00:07:12,000 --> 00:07:16,000
 consideration or contemplation,

88
00:07:16,000 --> 00:07:32,000
 analysis of the practice, analyzing our practice.

89
00:07:32,000 --> 00:07:35,880
 And in fact, these four dhammas don't particularly relate

90
00:07:35,880 --> 00:07:38,000
 to the practice directly at all,

91
00:07:38,000 --> 00:07:41,000
 they can be used for anything in the world.

92
00:07:41,000 --> 00:07:47,540
 Whatever we apply these four dhammas to, we can be sure to

93
00:07:47,540 --> 00:07:49,000
 succeed.

94
00:07:49,000 --> 00:07:53,230
 For instance, if you wish to do evil deeds, you need these

95
00:07:53,230 --> 00:07:55,000
 four dhammas as well,

96
00:07:55,000 --> 00:07:58,310
 you need to have the interest or the desire to do evil

97
00:07:58,310 --> 00:07:59,000
 deeds.

98
00:07:59,000 --> 00:08:03,850
 You have to put out effort to do evil deeds, you have to

99
00:08:03,850 --> 00:08:09,000
 think about it and keep it in mind, focus on it.

100
00:08:09,000 --> 00:08:14,670
 And you have to analyze the act to figure out how to do it

101
00:08:14,670 --> 00:08:22,000
 properly, how to make sure it succeeds.

102
00:08:22,000 --> 00:08:24,480
 Whatever we want to do in the world, we want to be

103
00:08:24,480 --> 00:08:26,000
 successful in business,

104
00:08:26,000 --> 00:08:30,000
 we need these four, we need to have the desire to be busy,

105
00:08:30,000 --> 00:08:33,000
 to conduct business,

106
00:08:33,000 --> 00:08:35,930
 we need to have the effort, we need to have the

107
00:08:35,930 --> 00:08:39,000
 concentration, the focus, keeping it in mind,

108
00:08:39,000 --> 00:08:44,910
 and we need to consider the causes and effects and make a

109
00:08:44,910 --> 00:08:49,000
 plan of how we're going to run our business,

110
00:08:49,000 --> 00:08:58,000
 analyzing the turns of event as things as they come.

111
00:08:58,000 --> 00:09:00,880
 So these four dhammas are very useful things to remember

112
00:09:00,880 --> 00:09:03,000
 and to think about, no matter what we do.

113
00:09:03,000 --> 00:09:07,130
 But most especially in the meditation practice, they have a

114
00:09:07,130 --> 00:09:10,000
 great meaning and a great importance.

115
00:09:10,000 --> 00:09:14,000
 Chanda means that we actually want to practice.

116
00:09:14,000 --> 00:09:18,250
 And this is often a criticism of Buddhist meditation by

117
00:09:18,250 --> 00:09:20,000
 people who have studied the books

118
00:09:20,000 --> 00:09:24,000
 and are perhaps looking for something to criticize.

119
00:09:24,000 --> 00:09:28,330
 And they say that you're looking for the end of desire and

120
00:09:28,330 --> 00:09:33,650
 yet you admit that you yourself have the desire to practice

121
00:09:33,650 --> 00:09:34,000
.

122
00:09:34,000 --> 00:09:37,610
 And this is really a silly argument, the one that baffles

123
00:09:37,610 --> 00:09:41,410
 many people and makes them think that it's really true that

124
00:09:41,410 --> 00:09:44,000
 this is an impossible task.

125
00:09:44,000 --> 00:09:48,070
 But really the word desire in this case is not really

126
00:09:48,070 --> 00:09:49,000
 desire.

127
00:09:49,000 --> 00:09:53,600
 We're talking about two uses of the word desire. When we

128
00:09:53,600 --> 00:09:58,000
 talk about giving up desire, we mean desire for something,

129
00:09:58,000 --> 00:09:59,000
 for some experience,

130
00:09:59,000 --> 00:10:04,000
 for some state, for some phenomenon to arise.

131
00:10:04,000 --> 00:10:08,080
 When we talk about the desire to practice, we mean simply

132
00:10:08,080 --> 00:10:11,810
 an intention to practice based on the knowledge that it's a

133
00:10:11,810 --> 00:10:13,000
 good thing to do,

134
00:10:13,000 --> 00:10:16,890
 based on the knowledge that desire is a bad thing and that

135
00:10:16,890 --> 00:10:20,330
 only through contemplation are we going to be free from

136
00:10:20,330 --> 00:10:22,000
 that desire.

137
00:10:22,000 --> 00:10:26,200
 It's the difference between an attachment to something and

138
00:10:26,200 --> 00:10:33,000
 an intention to do something, to carry out some act.

139
00:10:33,000 --> 00:10:38,030
 When we desire for an object, then we carry out all sorts

140
00:10:38,030 --> 00:10:41,000
 of bad deeds in order to get it.

141
00:10:41,000 --> 00:10:44,840
 But when we desire to do something, it doesn't have to be a

142
00:10:44,840 --> 00:10:46,000
 desire at all.

143
00:10:46,000 --> 00:10:48,850
 In fact, it's very hard to think of anyone actually des

144
00:10:48,850 --> 00:10:53,000
iring to practice, we pass on the meditation.

145
00:10:53,000 --> 00:10:56,300
 In fact, if they did, you could say it would be a wrong, it

146
00:10:56,300 --> 00:11:02,000
 would be an unwholesome sort of mind state.

147
00:11:02,000 --> 00:11:04,860
 But what we mean by the fact when we say we desire or we

148
00:11:04,860 --> 00:11:07,570
 want to meditate, it's not that we have this desire or this

149
00:11:07,570 --> 00:11:10,000
 attachment to meditating.

150
00:11:10,000 --> 00:11:13,460
 It's the fact that we realize that if we don't meditate and

151
00:11:13,460 --> 00:11:15,000
 we don't do it in earnest,

152
00:11:15,000 --> 00:11:17,560
 we're going to slip back into all sorts of unwholesome

153
00:11:17,560 --> 00:11:21,000
 states and conduct ourselves wrongly

154
00:11:21,000 --> 00:11:26,360
 and give rise to all sorts of suffering and guilt and unh

155
00:11:26,360 --> 00:11:29,000
appiness in the future.

156
00:11:29,000 --> 00:11:32,020
 So it's because of this that people want to practice

157
00:11:32,020 --> 00:11:35,000
 meditation or intend to practice meditation.

158
00:11:35,000 --> 00:11:39,000
 In fact, this doesn't come easily in regards to meditation.

159
00:11:39,000 --> 00:11:41,000
 Most of us have a hard time meditating.

160
00:11:41,000 --> 00:11:47,840
 When we come to meditation in the beginning, it's a very

161
00:11:47,840 --> 00:11:51,000
 unpleasant practice.

162
00:11:51,000 --> 00:11:57,430
 It's something that we look towards with aversion simply

163
00:11:57,430 --> 00:12:01,000
 because we have to give up, we have to let go.

164
00:12:01,000 --> 00:12:05,280
 We cannot hold on, we cannot chase after, we can no longer

165
00:12:05,280 --> 00:12:09,480
 have any of the wonderful things that we believe are making

166
00:12:09,480 --> 00:12:11,000
 us happy.

167
00:12:11,000 --> 00:12:15,270
 We can no longer chase after them. We have to put up with

168
00:12:15,270 --> 00:12:19,000
 many unpleasant situations, unpleasant circumstances,

169
00:12:19,000 --> 00:12:29,930
 pain and achings, soreness, bad thoughts, boredom, rest

170
00:12:29,930 --> 00:12:35,000
lessness, frustration.

171
00:12:35,000 --> 00:12:39,200
 But as we start to either through the practice or by living

172
00:12:39,200 --> 00:12:44,000
 our lives and meeting with such suffering,

173
00:12:44,000 --> 00:12:49,000
 meeting with this realization that there is such suffering,

174
00:12:49,000 --> 00:12:54,650
 our current state of mind is only capable of a great amount

175
00:12:54,650 --> 00:12:56,000
 of sorrow.

176
00:12:56,000 --> 00:12:58,200
 It's not that we see the suffering outside of ourselves, we

177
00:12:58,200 --> 00:13:02,000
 see that our minds are not as strong as they should be

178
00:13:02,000 --> 00:13:05,000
 and so they suffer when things don't go our way.

179
00:13:05,000 --> 00:13:08,310
 For most people it requires them to see a very unpleasant

180
00:13:08,310 --> 00:13:09,000
 situation

181
00:13:09,000 --> 00:13:14,170
 before they realize how unequipped their mind is to deal

182
00:13:14,170 --> 00:13:16,000
 with the situation.

183
00:13:16,000 --> 00:13:18,980
 And then they come to practice meditation. But sometimes

184
00:13:18,980 --> 00:13:22,590
 people come to meditate innocently without thinking that it

185
00:13:22,590 --> 00:13:24,000
's such a big deal.

186
00:13:24,000 --> 00:13:29,020
 And only when they meditate do they realize how astray they

187
00:13:29,020 --> 00:13:32,000
've gone, how weak their minds are,

188
00:13:32,000 --> 00:13:37,000
 how unequipped their minds are for any difficulty, any

189
00:13:37,000 --> 00:13:38,000
 suffering.

190
00:13:38,000 --> 00:13:42,000
 And this is where chanda arises.

191
00:13:42,000 --> 00:13:46,160
 The ways we can give rise to chanda are in this way,

192
00:13:46,160 --> 00:13:49,000
 through seeing the suffering in the world around us,

193
00:13:49,000 --> 00:13:52,630
 or through seeing the suffering that comes from our mind

194
00:13:52,630 --> 00:13:54,000
 through meditation.

195
00:13:54,000 --> 00:13:57,720
 And it's something we should reflect upon often, that

196
00:13:57,720 --> 00:14:01,000
 really indeed we are weak in mind.

197
00:14:01,000 --> 00:14:05,210
 And if we allow ourselves to continue like this, without

198
00:14:05,210 --> 00:14:10,000
 training ourselves, without training our minds,

199
00:14:10,000 --> 00:14:13,000
 then we'll only meet with suffering in the future.

200
00:14:13,000 --> 00:14:16,980
 Chanda is a very important thing. Whatever you have the

201
00:14:16,980 --> 00:14:21,440
 intention or the desire to do, that is where we'll define

202
00:14:21,440 --> 00:14:23,000
 who you are.

203
00:14:23,000 --> 00:14:32,120
 The Buddha said chanda is the mula, the basis of all states

204
00:14:32,120 --> 00:14:35,000
, all dhammas.

205
00:14:35,000 --> 00:14:39,000
 Number two is vidya, which means effort.

206
00:14:39,000 --> 00:14:47,000
 Effort sort of comes in line with desire.

207
00:14:47,000 --> 00:14:49,490
 But on the other hand it's not enough just to want to do

208
00:14:49,490 --> 00:14:52,000
 something or to know that meditation is enough.

209
00:14:52,000 --> 00:14:55,450
 The Buddha said few are the people in the world who know

210
00:14:55,450 --> 00:15:01,360
 what needs to be done, who know that something needs to be

211
00:15:01,360 --> 00:15:02,000
 done,

212
00:15:02,000 --> 00:15:07,000
 more than just living our lives in a plain old ordinary way

213
00:15:07,000 --> 00:15:10,000
 of getting a job and making money and settling down

214
00:15:10,000 --> 00:15:16,120
 and trying to find stability for a short time and then that

215
00:15:16,120 --> 00:15:18,000
 passing away.

216
00:15:18,000 --> 00:15:21,540
 People who realize that there's more to life than that,

217
00:15:21,540 --> 00:15:23,000
 these are hard to find.

218
00:15:23,000 --> 00:15:25,320
 And he said what is even harder to find is people who

219
00:15:25,320 --> 00:15:27,000
 actually then do something about it.

220
00:15:27,000 --> 00:15:30,000
 And this is where effort comes into play.

221
00:15:30,000 --> 00:15:32,340
 If you don't have the effort to actually get up and

222
00:15:32,340 --> 00:15:35,610
 practice, to get up and do the walking, to get up and do

223
00:15:35,610 --> 00:15:38,000
 the sitting,

224
00:15:38,000 --> 00:15:46,000
 then your desire is impotent. It's an impotent desire.

225
00:15:46,000 --> 00:15:49,000
 And we require both of these that we have to put out effort

226
00:15:49,000 --> 00:15:51,000
 and put out effort at every moment.

227
00:15:51,000 --> 00:15:54,400
 So we have the desire to practice but we also have this

228
00:15:54,400 --> 00:15:56,000
 intention to practice.

229
00:15:56,000 --> 00:15:58,700
 Or we also have this effort in our practice, putting our

230
00:15:58,700 --> 00:16:00,000
 mind out at the object,

231
00:16:00,000 --> 00:16:03,080
 not just sitting for long hours at a time but also knowing

232
00:16:03,080 --> 00:16:09,000
 what's going on and being mindful of it.

233
00:16:09,000 --> 00:16:13,040
 The effort to get rid of unwholesome states, the effort to

234
00:16:13,040 --> 00:16:15,000
 build up wholesome states,

235
00:16:15,000 --> 00:16:19,000
 the effort to guard against unarisen unwholesome states,

236
00:16:19,000 --> 00:16:23,360
 and the effort to give rise to wholesome states and to

237
00:16:23,360 --> 00:16:29,000
 augment wholesome states that have already arisen.

238
00:16:29,000 --> 00:16:32,720
 This is another important. The Buddha said that it is

239
00:16:32,720 --> 00:16:36,000
 through effort that we overcome suffering.

240
00:16:36,000 --> 00:16:42,250
 When suffering comes to us in whatever many forms that it

241
00:16:42,250 --> 00:16:43,000
 comes,

242
00:16:43,000 --> 00:16:46,620
 it is not simple, there is no simple way out that we're

243
00:16:46,620 --> 00:16:49,310
 going to just take a pill and suddenly there's no more

244
00:16:49,310 --> 00:16:50,000
 suffering.

245
00:16:50,000 --> 00:16:52,560
 That's something that we have to work at. We have to

246
00:16:52,560 --> 00:16:54,000
 realize this as well.

247
00:16:54,000 --> 00:16:56,320
 If we have this effort then we can truly become free from

248
00:16:56,320 --> 00:16:57,000
 suffering.

249
00:16:57,000 --> 00:16:59,790
 If we don't have the effort it will be very difficult for

250
00:16:59,790 --> 00:17:04,000
 us and be even more painful for us to practice

251
00:17:04,000 --> 00:17:07,170
 because we don't have the effort to really and truly shar

252
00:17:07,170 --> 00:17:11,000
pen our minds and it will be very slow going.

253
00:17:11,000 --> 00:17:15,000
 So to meet with success we have to have this effort.

254
00:17:15,000 --> 00:17:18,790
 Number three, we need chitta, which means we need to think

255
00:17:18,790 --> 00:17:20,000
 about practice.

256
00:17:20,000 --> 00:17:24,000
 We need to keep it in our minds at all times.

257
00:17:24,000 --> 00:17:27,720
 Even when we're not practicing, when we're not doing the

258
00:17:27,720 --> 00:17:30,000
 walking or we're not doing the sitting,

259
00:17:30,000 --> 00:17:32,950
 we should always try to keep it in mind, always try to

260
00:17:32,950 --> 00:17:40,000
 think about it, keep it at the top of our thoughts,

261
00:17:40,000 --> 00:17:42,640
 at the top of our list of things to think about, things to

262
00:17:42,640 --> 00:17:43,000
 do.

263
00:17:43,000 --> 00:17:46,000
 Everything else should be subservient to the meditation.

264
00:17:46,000 --> 00:17:49,440
 We should always be thinking about coming back and being

265
00:17:49,440 --> 00:17:50,000
 mindful.

266
00:17:50,000 --> 00:17:53,070
 Even when we're doing other things during the day we should

267
00:17:53,070 --> 00:17:55,000
 try to be mindful, doing them.

268
00:17:55,000 --> 00:17:57,380
 Standing when we're standing, know that we're standing when

269
00:17:57,380 --> 00:17:59,000
 we're walking, know that we're walking.

270
00:17:59,000 --> 00:18:02,920
 Becoming aware of our feelings and our emotions, becoming

271
00:18:02,920 --> 00:18:05,000
 aware of our thoughts,

272
00:18:05,000 --> 00:18:10,000
 and keeping our mindfulness throughout the day.

273
00:18:10,000 --> 00:18:13,140
 Especially in meditation practice it's something that

274
00:18:13,140 --> 00:18:15,000
 requires us to keep it in mind.

275
00:18:15,000 --> 00:18:18,960
 Other works that we do we might be able to put aside for

276
00:18:18,960 --> 00:18:20,000
 some time.

277
00:18:20,000 --> 00:18:22,290
 We set things in motion and then leave them and wait to see

278
00:18:22,290 --> 00:18:23,000
 what they bring.

279
00:18:23,000 --> 00:18:27,540
 But with meditation it's not the case. Meditation is like

280
00:18:27,540 --> 00:18:29,000
 boiling water.

281
00:18:29,000 --> 00:18:32,000
 If you turn on the hot water and then later you turn it off

282
00:18:32,000 --> 00:18:34,000
 and then turn it on and then off,

283
00:18:34,000 --> 00:18:37,810
 the water will never boil, no matter how many times you

284
00:18:37,810 --> 00:18:40,000
 come back to turn the water on.

285
00:18:40,000 --> 00:18:45,000
 As soon as you turn it off it loses its momentum.

286
00:18:45,000 --> 00:18:47,560
 It's something that we have to work on until we truly do

287
00:18:47,560 --> 00:18:49,000
 become free from suffering.

288
00:18:49,000 --> 00:18:53,490
 Because our state of mind changes. When we're not mindful

289
00:18:53,490 --> 00:18:55,000
 then we lose all of our strength of mind,

290
00:18:55,000 --> 00:18:59,000
 of confidence, effort, concentration, wisdom.

291
00:18:59,000 --> 00:19:04,790
 We have to use the mindfulness at all times to gain these

292
00:19:04,790 --> 00:19:07,000
 other faculties.

293
00:19:07,000 --> 00:19:10,290
 And so it's of great importance that we keep the meditation

294
00:19:10,290 --> 00:19:11,000
 in mind.

295
00:19:11,000 --> 00:19:14,490
 In fact the Buddha said that only when we sleep then we

296
00:19:14,490 --> 00:19:17,000
 should be allowed to take a break.

297
00:19:17,000 --> 00:19:21,450
 But the way we do this when we lie down to sleep is even

298
00:19:21,450 --> 00:19:23,000
 when we lie down to sleep

299
00:19:23,000 --> 00:19:26,000
 we think about the moment when we're going to wake up.

300
00:19:26,000 --> 00:19:28,470
 We make a determination in our mind as to when we're going

301
00:19:28,470 --> 00:19:29,000
 to wake up

302
00:19:29,000 --> 00:19:33,000
 and then we're mindful until the moment when we fall asleep

303
00:19:33,000 --> 00:19:33,000
.

304
00:19:33,000 --> 00:19:37,190
 When we wake up we immediately get up and begin to meditate

305
00:19:37,190 --> 00:19:38,000
 again.

306
00:19:38,000 --> 00:19:44,000
 We should try to live in this way and try to be meditating

307
00:19:44,000 --> 00:19:49,000
 for all of our waking hours as best we can.

308
00:19:49,000 --> 00:19:52,000
 When we work in the world, when we have other things to do,

309
00:19:52,000 --> 00:19:56,000
 obviously this will be not the case.

310
00:19:56,000 --> 00:20:00,000
 We'll have to interrupt our practice many times.

311
00:20:00,000 --> 00:20:02,400
 But at any rate we can always come back and remind

312
00:20:02,400 --> 00:20:03,000
 ourselves.

313
00:20:03,000 --> 00:20:05,300
 And the more we're able to keep it in mind the better it

314
00:20:05,300 --> 00:20:06,000
 will be for us

315
00:20:06,000 --> 00:20:12,890
 and the quicker we'll be able to gain real benefits from

316
00:20:12,890 --> 00:20:15,000
 the practice.

317
00:20:15,000 --> 00:20:18,000
 The fourth quality, "vimangsa"

318
00:20:18,000 --> 00:20:21,120
 In terms of meditation this means figuring out the ways in

319
00:20:21,120 --> 00:20:23,000
 which we're meditating incorrectly

320
00:20:23,000 --> 00:20:27,000
 and being able to assess our own meditation practice.

321
00:20:27,000 --> 00:20:29,490
 Being able to catch ourselves when we fall into wrong

322
00:20:29,490 --> 00:20:32,000
 practice or bad habits.

323
00:20:32,000 --> 00:20:39,300
 And being able to play with this imperfect mind that we

324
00:20:39,300 --> 00:20:40,000
 have.

325
00:20:40,000 --> 00:20:44,000
 And being able to work with this imperfect state of affairs

326
00:20:44,000 --> 00:20:44,000
.

327
00:20:44,000 --> 00:20:47,160
 And learning how to roll with the punches and how to get

328
00:20:47,160 --> 00:20:49,000
 back up on our feet.

329
00:20:49,000 --> 00:20:53,180
 And how to work from one moment to one moment, from one day

330
00:20:53,180 --> 00:20:54,000
 to one day.

331
00:20:54,000 --> 00:20:56,000
 And training ourselves at every moment.

332
00:20:56,000 --> 00:20:59,000
 Trying to figure out what it is that we have to do next.

333
00:20:59,000 --> 00:21:03,000
 To augment our practice, to support our practice.

334
00:21:03,000 --> 00:21:06,000
 To look and see. Is our confidence waning?

335
00:21:06,000 --> 00:21:09,750
 Then we have doubts. We have to look and see and say to

336
00:21:09,750 --> 00:21:13,000
 ourselves, "Doubting, doubting."

337
00:21:13,000 --> 00:21:16,400
 Sometimes we have too much confidence and so we become

338
00:21:16,400 --> 00:21:21,000
 greedy or we become lazy.

339
00:21:21,000 --> 00:21:24,170
 Sometimes we have too much effort and so we become

340
00:21:24,170 --> 00:21:25,000
 distracted.

341
00:21:25,000 --> 00:21:29,530
 We have too much concentration and so we become, they add d

342
00:21:29,530 --> 00:21:31,000
rowsy or tired.

343
00:21:31,000 --> 00:21:33,000
 It means they're not balanced.

344
00:21:33,000 --> 00:21:35,440
 When we have lots of effort that's good but we have to

345
00:21:35,440 --> 00:21:37,000
 balance it with concentration.

346
00:21:37,000 --> 00:21:40,000
 So we work on our faculties in this way.

347
00:21:40,000 --> 00:21:45,560
 When we have lots of confidence that's good but without

348
00:21:45,560 --> 00:21:47,000
 wisdom it's blind.

349
00:21:47,000 --> 00:21:52,000
 And so we need them both. We need to balance our faculties.

350
00:21:52,000 --> 00:21:56,000
 And we need to catch ourselves in our practice.

351
00:21:56,000 --> 00:21:59,590
 We need to see why we're failing, why we're slacking, why

352
00:21:59,590 --> 00:22:01,000
 we're falling behind.

353
00:22:01,000 --> 00:22:07,000
 And we have to pick ourselves up and redouble our efforts.

354
00:22:07,000 --> 00:22:10,000
 Bring ourselves back in line, back on track.

355
00:22:10,000 --> 00:22:14,000
 And we have to work on this at all times.

356
00:22:14,000 --> 00:22:16,000
 We mungsah because we can't force our practice.

357
00:22:16,000 --> 00:22:19,000
 We can't force the meditation to be perfect.

358
00:22:19,000 --> 00:22:22,000
 We can't force ourselves to be good at meditating.

359
00:22:22,000 --> 00:22:26,000
 So we have to be clever in terms of how to train ourselves

360
00:22:26,000 --> 00:22:28,000
 and how to work with what we have.

361
00:22:28,000 --> 00:22:32,160
 And slowly but surely lead ourselves, lead our minds to a

362
00:22:32,160 --> 00:22:38,000
 state of clarity and a state of understanding.

363
00:22:38,000 --> 00:22:42,000
 So these four are very important for meditators.

364
00:22:42,000 --> 00:22:44,900
 They're things that we should constantly be thinking about

365
00:22:44,900 --> 00:22:46,000
 and considering.

366
00:22:46,000 --> 00:22:51,000
 We should at least use them as a guide for our practice.

367
00:22:51,000 --> 00:22:55,220
 In order to at least tell ourselves, to make it clear to

368
00:22:55,220 --> 00:22:57,550
 ourselves that if we don't have these we're not going to

369
00:22:57,550 --> 00:22:58,000
 get anywhere.

370
00:22:58,000 --> 00:23:01,940
 And looking to see do we really have this contentment or

371
00:23:01,940 --> 00:23:04,000
 this desire to practice.

372
00:23:04,000 --> 00:23:07,000
 And if we don't then we have to work on that.

373
00:23:07,000 --> 00:23:11,140
 And we have to see where our laziness lies or this evil in

374
00:23:11,140 --> 00:23:13,000
 our hearts that's polluting our minds

375
00:23:13,000 --> 00:23:17,260
 and making us think maybe that meditation is a bad thing or

376
00:23:17,260 --> 00:23:20,000
 a useless thing or so on.

377
00:23:20,000 --> 00:23:24,000
 That's giving us these bad vibes in regards to meditation.

378
00:23:24,000 --> 00:23:27,510
 And we have to work on this. Look directly at them and see

379
00:23:27,510 --> 00:23:28,000
 them.

380
00:23:28,000 --> 00:23:30,820
 If it's true that meditation is a bad thing then it should

381
00:23:30,820 --> 00:23:32,000
 stand up to the test.

382
00:23:32,000 --> 00:23:34,560
 We should be able to look at these thoughts and come to

383
00:23:34,560 --> 00:23:37,000
 some rational conclusion that

384
00:23:37,000 --> 00:23:40,000
 indeed meditation is a useless thing or a bad thing.

385
00:23:40,000 --> 00:23:43,000
 But what happens when you do is you'll see that there are

386
00:23:43,000 --> 00:23:50,000
 negative emotions of anger, of laziness, of cowardliness

387
00:23:50,000 --> 00:23:51,000
 even

388
00:23:51,000 --> 00:24:01,000
 that cause us to be afraid to be lazy as well.

389
00:24:01,000 --> 00:24:05,460
 So we need this kind of contentment and we need to put out

390
00:24:05,460 --> 00:24:08,000
 effort to see what we want to practice

391
00:24:08,000 --> 00:24:11,100
 but are we putting out the effort to actually practice or

392
00:24:11,100 --> 00:24:14,000
 are we making excuses and doing other things?

393
00:24:14,000 --> 00:24:19,480
 Is our effort being channeled into other pastimes or other

394
00:24:19,480 --> 00:24:21,000
 activities?

395
00:24:21,000 --> 00:24:26,000
 We need to see are we keeping it in mind? Are we really...

396
00:24:26,000 --> 00:24:30,000
 It's meditation in our minds at all times.

397
00:24:30,000 --> 00:24:33,440
 And we have to look and use... This is using vimamsa, this

398
00:24:33,440 --> 00:24:37,000
 is the fourth one, to consider in this way.

399
00:24:37,000 --> 00:24:40,350
 In and of itself as we vimamsa. So we have to remind

400
00:24:40,350 --> 00:24:43,270
 ourselves to constantly go over this and to look at our

401
00:24:43,270 --> 00:24:45,000
 practice.

402
00:24:45,000 --> 00:24:48,590
 Not overly analyzing our practice. In the end we do just

403
00:24:48,590 --> 00:24:50,000
 have to meditate.

404
00:24:50,000 --> 00:24:53,020
 But as we meditate on we have to keep ourselves in check

405
00:24:53,020 --> 00:24:54,000
 and see and look.

406
00:24:54,000 --> 00:24:57,160
 Are we really practicing? Sometimes we have to ask our

407
00:24:57,160 --> 00:25:00,390
 teacher questions when we can't figure things out for

408
00:25:00,390 --> 00:25:01,000
 ourselves.

409
00:25:01,000 --> 00:25:05,250
 It won't do to just walk and sit, walk and sit for years

410
00:25:05,250 --> 00:25:09,000
 without really having some quality to our practice.

411
00:25:09,000 --> 00:25:11,550
 And so these factors really help to add that quality to

412
00:25:11,550 --> 00:25:12,000
 them.

413
00:25:12,000 --> 00:25:15,170
 These are called the four intipata. And they're important

414
00:25:15,170 --> 00:25:22,000
 for meditators as they are for success in any undertaking.

415
00:25:22,000 --> 00:25:25,440
 So this is the Dhamma for today. Now we'll continue on with

416
00:25:25,440 --> 00:25:26,000
 practice.

417
00:25:26,000 --> 00:25:30,100
 First doing mindful frustration, then walking and then

418
00:25:30,100 --> 00:25:31,000
 sitting.

