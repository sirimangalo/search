1
00:00:00,000 --> 00:00:13,000
 Tonight we will begin to include in our meditation sessions

2
00:00:13,000 --> 00:00:27,600
 a theoretical guidance, some kind

3
00:00:27,600 --> 00:00:30,950
 of practical theory that we can apply to our meditation

4
00:00:30,950 --> 00:00:31,600
 practice.

5
00:00:31,600 --> 00:00:36,750
 And I'll try to find something every evening from the

6
00:00:36,750 --> 00:00:38,160
 Buddha's teaching that's applicable

7
00:00:38,160 --> 00:00:48,240
 to our practice, something that we can use.

8
00:00:48,240 --> 00:00:50,530
 So as this is the first talk, I thought it would be

9
00:00:50,530 --> 00:00:52,200
 important to start at the beginning

10
00:00:52,200 --> 00:01:01,080
 with the Satipatthana.

11
00:01:01,080 --> 00:01:05,260
 Of course we're all familiar with what are the four Satipat

12
00:01:05,260 --> 00:01:07,400
thana and we have our understanding

13
00:01:07,400 --> 00:01:16,200
 of how we put into practice the four Satipatthana.

14
00:01:16,200 --> 00:01:19,650
 And actually I think you all have heard something about how

15
00:01:19,650 --> 00:01:22,360
, what are the benefits of Satipatthana.

16
00:01:22,360 --> 00:01:26,960
 But I think it's important for us to go over this again.

17
00:01:26,960 --> 00:01:30,320
 It helps us to understand why we're practicing.

18
00:01:30,320 --> 00:01:33,640
 And it helps us to focus our practice in the right

19
00:01:33,640 --> 00:01:34,720
 direction.

20
00:01:34,720 --> 00:01:41,600
 So I'll try to go through these in a practical way.

21
00:01:41,600 --> 00:01:45,290
 The first reason why we practice with us is to purify our

22
00:01:45,290 --> 00:01:46,000
 minds.

23
00:01:46,000 --> 00:01:57,080
 So this should be our main focus in our practice.

24
00:01:57,080 --> 00:02:03,860
 It's not to see special things or to experience special

25
00:02:03,860 --> 00:02:05,260
 states.

26
00:02:05,260 --> 00:02:10,480
 The practice is for cleaning our minds, straightening our

27
00:02:10,480 --> 00:02:14,360
 minds, straightening out what's crooked

28
00:02:14,360 --> 00:02:17,890
 about our minds, cleaning what's dirty about our minds,

29
00:02:17,890 --> 00:02:19,760
 fixing what's broken about our

30
00:02:19,760 --> 00:02:28,880
 minds.

31
00:02:28,880 --> 00:02:33,970
 And not even trying to teach ourselves this is wrong or

32
00:02:33,970 --> 00:02:36,800
 that is wrong or find things that

33
00:02:36,800 --> 00:02:38,800
 are wrong about our minds.

34
00:02:38,800 --> 00:02:41,720
 But just fix that which we know is broken.

35
00:02:41,720 --> 00:02:43,080
 Clean that which we know is dirty.

36
00:02:43,080 --> 00:02:44,880
 But we know for ourselves.

37
00:02:44,880 --> 00:02:52,620
 We don't have to read books to find out what is bad and

38
00:02:52,620 --> 00:02:56,240
 what is unwholesome.

39
00:02:56,240 --> 00:03:00,180
 But when we look inside we'll see very clearly certain

40
00:03:00,180 --> 00:03:02,200
 things that are broken.

41
00:03:02,200 --> 00:03:04,800
 In the beginning we'll have all sorts of ideas about what's

42
00:03:04,800 --> 00:03:05,640
 wrong with us.

43
00:03:05,640 --> 00:03:13,560
 Maybe we're not smart enough or maybe we're not good enough

44
00:03:13,560 --> 00:03:15,120
 or so on.

45
00:03:15,120 --> 00:03:16,120
 Not focused enough.

46
00:03:16,120 --> 00:03:19,050
 Many meditators come and get angry at themselves because

47
00:03:19,050 --> 00:03:20,360
 they're not focused.

48
00:03:20,360 --> 00:03:26,950
 And so we begin to feel guilty and we think that something

49
00:03:26,950 --> 00:03:29,280
's wrong with us.

50
00:03:29,280 --> 00:03:33,550
 And what we begin to see when we apply the practice and

51
00:03:33,550 --> 00:03:36,000
 when we apply it day after day

52
00:03:36,000 --> 00:03:40,020
 after day is that really what's the real problem and the

53
00:03:40,020 --> 00:03:42,640
 most pressing problem is this guilt

54
00:03:42,640 --> 00:03:46,400
 and this self-hatred.

55
00:03:46,400 --> 00:03:51,240
 This feeling that something's wrong with us.

56
00:03:51,240 --> 00:03:55,230
 So the meaning is that we come to see for ourselves what's

57
00:03:55,230 --> 00:03:56,000
 wrong.

58
00:03:56,000 --> 00:03:58,500
 We don't need anyone to tell us and we don't need to have

59
00:03:58,500 --> 00:03:59,920
 any belief and we have to give

60
00:03:59,920 --> 00:04:02,160
 up all our belief.

61
00:04:02,160 --> 00:04:04,420
 Any belief that we have that this is wrong and that is

62
00:04:04,420 --> 00:04:05,800
 wrong and this is bad and that

63
00:04:05,800 --> 00:04:06,800
 is bad.

64
00:04:06,800 --> 00:04:12,690
 We have to give it up because all of that is a cause for us

65
00:04:12,690 --> 00:04:15,520
 to really hurt ourselves.

66
00:04:15,520 --> 00:04:23,680
 It's the reason why we suffer so horribly at times.

67
00:04:23,680 --> 00:04:26,560
 Something angry is bad but when you get angry that you're

68
00:04:26,560 --> 00:04:28,120
 angry it's far worse and that

69
00:04:28,120 --> 00:04:35,640
 leads to true and extreme suffering.

70
00:04:35,640 --> 00:04:38,390
 When you want things this is bad but when you hate yourself

71
00:04:38,390 --> 00:04:39,800
 because of your addictions

72
00:04:39,800 --> 00:04:44,160
 and because of your desires and you feel guilty about them

73
00:04:44,160 --> 00:04:45,520
 this is worse.

74
00:04:45,520 --> 00:04:52,760
 This is real and extreme suffering.

75
00:04:52,760 --> 00:04:56,130
 So when we talk about cleaning the mind, purifying the mind

76
00:04:56,130 --> 00:05:00,840
, we're talking about something empirical,

77
00:05:00,840 --> 00:05:03,960
 something you can see for yourself.

78
00:05:03,960 --> 00:05:07,840
 It's not based on commandments or requirements.

79
00:05:07,840 --> 00:05:13,140
 It's based on your experience of what hurts you and what

80
00:05:13,140 --> 00:05:14,400
 helps you, what makes you find,

81
00:05:14,400 --> 00:05:18,280
 what brings you peace and what makes you suffer.

82
00:05:18,280 --> 00:05:26,640
 This is the first reason why we practice.

83
00:05:26,640 --> 00:05:34,550
 The way this comes about in our practice is through the

84
00:05:34,550 --> 00:05:40,160
 direct experience and reaffirmation

85
00:05:40,160 --> 00:05:48,120
 that we use that basically means it is what it is.

86
00:05:48,120 --> 00:05:52,480
 Seeing is only seeing, hearing is only hearing.

87
00:05:52,480 --> 00:05:55,950
 Smelling is only smelling, tasting is only tasting, feeling

88
00:05:55,950 --> 00:05:57,080
 is only feeling.

89
00:05:57,080 --> 00:05:59,680
 Thinking is only feeling.

90
00:05:59,680 --> 00:06:04,040
 It is that and it's only that.

91
00:06:04,040 --> 00:06:09,110
 When we remind ourselves of this again and again, we

92
00:06:09,110 --> 00:06:12,280
 straighten out all that is wrong

93
00:06:12,280 --> 00:06:14,920
 with the mind.

94
00:06:14,920 --> 00:06:17,840
 Even when you're angry knowing that this is anger.

95
00:06:17,840 --> 00:06:24,800
 It is and it's only that.

96
00:06:24,800 --> 00:06:27,880
 And so there's no room for feeling bad about it or guilty

97
00:06:27,880 --> 00:06:29,800
 about it or getting frustrated

98
00:06:29,800 --> 00:06:36,440
 about your anger.

99
00:06:36,440 --> 00:06:39,830
 And then when you have pain before you even get angry about

100
00:06:39,830 --> 00:06:41,560
 it, you know that it's pain.

101
00:06:41,560 --> 00:06:54,720
 And so there's no room for you to get angry about it.

102
00:06:54,720 --> 00:07:04,840
 And so you naturally purify your mind.

103
00:07:04,840 --> 00:07:10,230
 It's kind of amazing actually how like some liquid deter

104
00:07:10,230 --> 00:07:13,280
gent like bleach for example,

105
00:07:13,280 --> 00:07:17,990
 bleach has this magical property that kills all bacteria

106
00:07:17,990 --> 00:07:20,440
 and germs and cleans away all

107
00:07:20,440 --> 00:07:22,720
 stains and so on.

108
00:07:22,720 --> 00:07:25,880
 Mindfulness is the same, it has this magical property that

109
00:07:25,880 --> 00:07:27,680
 everything, you don't even have

110
00:07:27,680 --> 00:07:31,880
 to go around looking for a cure for this problem, a cure

111
00:07:31,880 --> 00:07:33,520
 for that problem.

112
00:07:33,520 --> 00:07:37,750
 It's amazing how all of our problems can be solved by this

113
00:07:37,750 --> 00:07:39,080
 mindfulness.

114
00:07:39,080 --> 00:07:42,960
 And this is because of what the Buddha said that the true

115
00:07:42,960 --> 00:07:45,440
 cause of suffering and all suffering

116
00:07:45,440 --> 00:07:50,000
 is ignorance.

117
00:07:50,000 --> 00:07:53,210
 When we look at deeply into the Buddha's teaching we see

118
00:07:53,210 --> 00:07:55,240
 that the real problem isn't craving

119
00:07:55,240 --> 00:07:58,820
 actually even though the Buddha said craving is the cause

120
00:07:58,820 --> 00:07:59,920
 of suffering.

121
00:07:59,920 --> 00:08:05,470
 He explained it by saying that the cure for suffering is

122
00:08:05,470 --> 00:08:06,640
 wisdom.

123
00:08:06,640 --> 00:08:08,800
 So the real problem is the ignorant.

124
00:08:08,800 --> 00:08:12,020
 We're ignorant as to these things that we crave, these

125
00:08:12,020 --> 00:08:13,600
 things that we cling to.

126
00:08:13,600 --> 00:08:17,080
 We think somehow they're going to bring us happiness.

127
00:08:17,080 --> 00:08:22,480
 Once we see clearly that they don't, everything clears up.

128
00:08:22,480 --> 00:08:27,520
 It's like having a big knot.

129
00:08:27,520 --> 00:08:29,900
 And you look at the knot and you think, "Oh, that's a real

130
00:08:29,900 --> 00:08:30,520
 problem."

131
00:08:30,520 --> 00:08:33,880
 But once you solve the knot it's just a string with no knot

132
00:08:33,880 --> 00:08:36,040
, like the knot was never there.

133
00:08:36,040 --> 00:08:38,880
 You think, "How do I solve this?"

134
00:08:38,880 --> 00:08:42,520
 But all you do is untie it and there is no problem anymore.

135
00:08:42,520 --> 00:08:46,150
 It's like there was never a problem, it was just the string

136
00:08:46,150 --> 00:08:46,160
.

137
00:08:46,160 --> 00:08:48,740
 In the beginning it was the string, in the end it was the

138
00:08:48,740 --> 00:08:49,280
 string.

139
00:08:49,280 --> 00:08:52,240
 The only difference is the formation.

140
00:08:52,240 --> 00:08:55,240
 It was in the wrong, it wasn't straight.

141
00:08:55,240 --> 00:08:58,720
 And so we do this with the mind.

142
00:08:58,720 --> 00:09:04,880
 This is really the focus of our practice.

143
00:09:04,880 --> 00:09:07,660
 The second reason why we practice and really getting into

144
00:09:07,660 --> 00:09:09,080
 the benefits of straightening

145
00:09:09,080 --> 00:09:16,740
 your mind and purifying your mind is that once your mind is

146
00:09:16,740 --> 00:09:19,200
 pure you do away with all

147
00:09:19,200 --> 00:09:25,400
 kinds of mental sickness, mental illness, mental dis-ease,

148
00:09:25,400 --> 00:09:27,240
 all kinds of sorrows and

149
00:09:27,240 --> 00:09:32,540
 despairs and stresses and worries and obsessions, add

150
00:09:32,540 --> 00:09:34,520
ictions and so on.

151
00:09:34,520 --> 00:09:37,220
 All of these things that they talk about in the world as

152
00:09:37,220 --> 00:09:38,760
 being problems that you need

153
00:09:38,760 --> 00:09:42,880
 to solve with drugs or so on.

154
00:09:42,880 --> 00:09:48,400
 But it's not true that all of them can be cured for people

155
00:09:48,400 --> 00:09:51,520
 who have chemical imbalances

156
00:09:51,520 --> 00:09:52,520
 in the brain.

157
00:09:52,520 --> 00:09:58,170
 It's possible that meditation will never in this life solve

158
00:09:58,170 --> 00:09:59,960
 their problem.

159
00:09:59,960 --> 00:10:04,310
 The actual practice of the meditation for people who are

160
00:10:04,310 --> 00:10:06,680
 able to understand it, every

161
00:10:06,680 --> 00:10:10,040
 moment of practicing changes the mind and as a result

162
00:10:10,040 --> 00:10:11,480
 changes the brain.

163
00:10:11,480 --> 00:10:14,440
 There's this phenomenon they're learning about now called

164
00:10:14,440 --> 00:10:16,080
 neuroplasticity that the brain

165
00:10:16,080 --> 00:10:19,720
 can actually change the way it works.

166
00:10:19,720 --> 00:10:27,150
 An example is people who go blind find that, they find in

167
00:10:27,150 --> 00:10:30,880
 the brains of people who were

168
00:10:30,880 --> 00:10:35,030
 born blind or have gone blind that the part of the brain

169
00:10:35,030 --> 00:10:37,160
 that used to see is now used

170
00:10:37,160 --> 00:10:38,800
 for hearing, for example.

171
00:10:38,800 --> 00:10:44,560
 The brain starts to use this unused hard drive space or CPU

172
00:10:44,560 --> 00:10:47,680
 space for something new, for

173
00:10:47,680 --> 00:10:51,280
 another task.

174
00:10:51,280 --> 00:10:54,680
 But the point is that our brains, even our brains can

175
00:10:54,680 --> 00:10:55,480
 change.

176
00:10:55,480 --> 00:11:00,440
 It's unlikely that people with serious mental problems will

177
00:11:00,440 --> 00:11:03,600
 get far in the meditation, but

178
00:11:03,600 --> 00:11:07,590
 even getting a short way in the meditation can set them on

179
00:11:07,590 --> 00:11:09,720
 the right path and can change

180
00:11:09,720 --> 00:11:12,440
 their life.

181
00:11:12,440 --> 00:11:16,650
 So all kinds of mental sicknesses, mental illnesses come by

182
00:11:16,650 --> 00:11:18,400
 untying the knots in our

183
00:11:18,400 --> 00:11:21,000
 behavior, the knots in our habit.

184
00:11:21,000 --> 00:11:26,620
 We see that actually all of these problems we thought that

185
00:11:26,620 --> 00:11:29,760
 we had brain, we had a chemical

186
00:11:29,760 --> 00:11:30,760
 imbalance or so on.

187
00:11:30,760 --> 00:11:32,920
 We come to see that actually it was just knots.

188
00:11:32,920 --> 00:11:38,400
 It was just strings, strings, kind of a string theory.

189
00:11:38,400 --> 00:11:42,230
 All of this made of strings tied in knots and all we have

190
00:11:42,230 --> 00:11:44,160
 to do is untie the knots.

191
00:11:44,160 --> 00:11:48,520
 When you untie the knots it's like it was never there.

192
00:11:48,520 --> 00:11:49,520
 It's gone.

193
00:11:49,520 --> 00:11:53,960
 It's just an illusion.

194
00:11:53,960 --> 00:11:58,010
 The third reason why we practice is to overcome suffering,

195
00:11:58,010 --> 00:12:00,120
 pain in the body and pain in the

196
00:12:00,120 --> 00:12:07,040
 mind, to be free from it really.

197
00:12:07,040 --> 00:12:09,960
 Now we start with pain in the mind.

198
00:12:09,960 --> 00:12:13,190
 When you're sitting in meditation there's not much you can

199
00:12:13,190 --> 00:12:14,720
 do about physical pain.

200
00:12:14,720 --> 00:12:17,680
 Sometimes you have no choice but to sit through it.

201
00:12:17,680 --> 00:12:21,600
 You adjust, you stretch, you move and it doesn't go away.

202
00:12:21,600 --> 00:12:25,820
 It can get so bad until finally you realize that there's no

203
00:12:25,820 --> 00:12:26,720
 way out.

204
00:12:26,720 --> 00:12:30,560
 You realize that you have no way out.

205
00:12:30,560 --> 00:12:38,080
 You make this profound change by just saying, "Let it be."

206
00:12:38,080 --> 00:12:42,120
 When the pain comes up you stick with it.

207
00:12:42,120 --> 00:12:43,840
 In our daily life we're not able to do this.

208
00:12:43,840 --> 00:12:45,520
 It's something we don't ever know this.

209
00:12:45,520 --> 00:12:48,410
 We think, "Why do I have all this pain when I'm sitting

210
00:12:48,410 --> 00:12:49,720
 here in meditation?

211
00:12:49,720 --> 00:12:52,000
 When I'm at home I don't have this pain."

212
00:12:52,000 --> 00:12:56,360
 When you're at home you're shifting every few seconds.

213
00:12:56,360 --> 00:12:58,800
 Every minute you change position many times.

214
00:12:58,800 --> 00:13:04,720
 When you're in meditation you shift position much less.

215
00:13:04,720 --> 00:13:08,520
 It's not something caused by meditation.

216
00:13:08,520 --> 00:13:13,530
 It's actually now you're finally seeing the truth of the

217
00:13:13,530 --> 00:13:16,640
 body, the truth of suffering.

218
00:13:16,640 --> 00:13:25,080
 So finally after trying again and again to relieve the pain

219
00:13:25,080 --> 00:13:26,280
 you give in and you let the

220
00:13:26,280 --> 00:13:34,680
 pain be and you watch it and you say, "Pain, pain, pain."

221
00:13:34,680 --> 00:13:37,880
 You realize that it's not really pain at all.

222
00:13:37,880 --> 00:13:42,140
 Little pain isn't really painful until the mind says it's

223
00:13:42,140 --> 00:13:44,720
 no good, until the mind dislikes

224
00:13:44,720 --> 00:13:45,720
 it.

225
00:13:45,720 --> 00:13:48,460
 The Buddha said this is like you have one thorn in your

226
00:13:48,460 --> 00:13:50,440
 body and you take another thorn

227
00:13:50,440 --> 00:13:52,440
 to try to get it out.

228
00:13:52,440 --> 00:13:56,220
 You take a thorn off the tree and you break it and try to

229
00:13:56,220 --> 00:13:58,200
 get the first thorn out.

230
00:13:58,200 --> 00:14:00,620
 With that kind of method are you going to get less pain or

231
00:14:00,620 --> 00:14:01,320
 more pain?

232
00:14:01,320 --> 00:14:04,560
 It's most likely you're going to get twice as much pain.

233
00:14:04,560 --> 00:14:07,640
 When I had glass in my foot and I was trying to get it out

234
00:14:07,640 --> 00:14:09,280
 with a needle and it turned

235
00:14:09,280 --> 00:14:11,440
 out there was no glass in my foot after all.

236
00:14:11,440 --> 00:14:15,720
 But I got a lot of pain for it.

237
00:14:15,720 --> 00:14:18,960
 This is what we do with the mind pain.

238
00:14:18,960 --> 00:14:21,130
 This is a good example actually because in my mind I

239
00:14:21,130 --> 00:14:22,600
 thought there was glass there so

240
00:14:22,600 --> 00:14:25,200
 I was always trying to fix the problem.

241
00:14:25,200 --> 00:14:28,690
 It must have been something karmic because I tried many

242
00:14:28,690 --> 00:14:30,720
 times and then the doctor tried

243
00:14:30,720 --> 00:14:36,160
 and found so much pain.

244
00:14:36,160 --> 00:14:37,160
 This is what we do.

245
00:14:37,160 --> 00:14:39,880
 We think we're going to fix it and we try to fix it and end

246
00:14:39,880 --> 00:14:41,320
 up just causing more pain

247
00:14:41,320 --> 00:14:42,320
 for ourselves.

248
00:14:42,320 --> 00:14:44,930
 When we leave it alone, of course if there is glass in your

249
00:14:44,930 --> 00:14:46,280
 foot you should get it out.

250
00:14:46,280 --> 00:14:53,520
 It's not something simple but it's just an analogy.

251
00:14:53,520 --> 00:14:58,610
 The truth is for pain when we're sitting in meditation

252
00:14:58,610 --> 00:15:01,440
 nothing is going to happen to our

253
00:15:01,440 --> 00:15:09,760
 legs if we sit still, if we let it be, except that our legs

254
00:15:09,760 --> 00:15:12,280
 might go numb.

255
00:15:12,280 --> 00:15:14,240
 You can see that actually you get rid of physical pain as

256
00:15:14,240 --> 00:15:14,600
 well.

257
00:15:14,600 --> 00:15:19,910
 You say there's still physical pain but you don't feel it

258
00:15:19,910 --> 00:15:21,000
 as pain.

259
00:15:21,000 --> 00:15:26,610
 The fourth reason why we practice is to set ourselves on

260
00:15:26,610 --> 00:15:28,520
 the right path.

261
00:15:28,520 --> 00:15:32,020
 This is something that people are always asking what is the

262
00:15:32,020 --> 00:15:33,720
 right thing to do, what is the

263
00:15:33,720 --> 00:15:37,640
 right way to live my life, how can I live my life.

264
00:15:37,640 --> 00:15:40,110
 The truth is it's not important what you do or how you live

265
00:15:40,110 --> 00:15:40,800
 your life.

266
00:15:40,800 --> 00:15:43,240
 Everyone lives their life anyway.

267
00:15:43,240 --> 00:15:47,990
 Your life is going to go up and down and all around and

268
00:15:47,990 --> 00:15:50,200
 change in many ways.

269
00:15:50,200 --> 00:15:54,700
 The Buddha said it's like when you take a cloth, if you

270
00:15:54,700 --> 00:15:57,000
 have a white cloth and it's

271
00:15:57,000 --> 00:16:02,250
 covered in dirt and grime and oil and stains, then it doesn

272
00:16:02,250 --> 00:16:04,840
't matter what color you dip

273
00:16:04,840 --> 00:16:05,840
 it in.

274
00:16:05,840 --> 00:16:08,710
 You put it in red color or blue color or green color, it's

275
00:16:08,710 --> 00:16:10,360
 not going to take the color.

276
00:16:10,360 --> 00:16:13,330
 It's not going to come out and be a beautiful green or

277
00:16:13,330 --> 00:16:15,400
 beautiful red or beautiful yellow

278
00:16:15,400 --> 00:16:17,240
 color.

279
00:16:17,240 --> 00:16:20,890
 But if you take a clean cloth and you dip it in whatever

280
00:16:20,890 --> 00:16:22,840
 color you dip it in, it will

281
00:16:22,840 --> 00:16:25,400
 become clean.

282
00:16:25,400 --> 00:16:28,370
 Whatever you do in your life, the right path is not what

283
00:16:28,370 --> 00:16:29,400
 you're doing.

284
00:16:29,400 --> 00:16:34,910
 The right path is how you're doing it and your approach to

285
00:16:34,910 --> 00:16:35,840
 life.

286
00:16:35,840 --> 00:16:36,840
 Because life is life.

287
00:16:36,840 --> 00:16:42,400
 I'm not a monk, you're not a carpenter, an engineer, this

288
00:16:42,400 --> 00:16:43,340
 or that.

289
00:16:43,340 --> 00:16:44,340
 We are experienced.

290
00:16:44,340 --> 00:16:46,760
 We're moment to moment experienced.

291
00:16:46,760 --> 00:16:49,200
 That's who we are.

292
00:16:49,200 --> 00:16:52,200
 And every experience affects the next experience.

293
00:16:52,200 --> 00:16:57,450
 So if we act and speak and if we think and if we have a

294
00:16:57,450 --> 00:17:00,800
 pure mind, the next experience

295
00:17:00,800 --> 00:17:05,400
 will be based on that purity and happiness will follow us.

296
00:17:05,400 --> 00:17:09,640
 If we develop impure minds and unwholesome mind state, unwh

297
00:17:09,640 --> 00:17:12,160
olesome actions and speech,

298
00:17:12,160 --> 00:17:19,080
 then we'll receive the detriment of that and as a result

299
00:17:19,080 --> 00:17:22,520
 suffering will follow.

300
00:17:22,520 --> 00:17:24,880
 This is how we understand people wonder, "How can

301
00:17:24,880 --> 00:17:26,480
 meditation help me in my life?

302
00:17:26,480 --> 00:17:27,480
 What can it do for me?"

303
00:17:27,480 --> 00:17:29,840
 It does the most important thing for you.

304
00:17:29,840 --> 00:17:33,960
 It won't tell you what career you should take.

305
00:17:33,960 --> 00:17:37,400
 It will help you understand that which is much far more

306
00:17:37,400 --> 00:17:39,440
 important than your career.

307
00:17:39,440 --> 00:17:46,450
 And that is how to live, how to interact with reality

308
00:17:46,450 --> 00:17:50,000
 rather than react to it.

309
00:17:50,000 --> 00:17:53,540
 The final reason for practicing meditation is that it leads

310
00:17:53,540 --> 00:17:55,040
 us to nirvana or freedom

311
00:17:55,040 --> 00:17:58,640
 from suffering.

312
00:17:58,640 --> 00:17:59,640
 And this goes in stages.

313
00:17:59,640 --> 00:18:02,520
 So when we first start to practice or when we practice just

314
00:18:02,520 --> 00:18:03,800
 a little bit, all it does

315
00:18:03,800 --> 00:18:07,900
 is make us happier and more peaceful and more clear about

316
00:18:07,900 --> 00:18:10,160
 the world with more wisdom.

317
00:18:10,160 --> 00:18:13,110
 So we still have to maybe come back and be reborn and we

318
00:18:13,110 --> 00:18:14,760
 can even change and become a

319
00:18:14,760 --> 00:18:16,760
 bad person later on.

320
00:18:16,760 --> 00:18:19,520
 But it's helped us to some level.

321
00:18:19,520 --> 00:18:21,720
 If we practice more, then it will purify us to such an

322
00:18:21,720 --> 00:18:23,160
 extent that we don't have to come

323
00:18:23,160 --> 00:18:24,160
 back as a human.

324
00:18:24,160 --> 00:18:29,050
 We can be born in a pure state where we are able to be free

325
00:18:29,050 --> 00:18:31,240
 from suffering for a long

326
00:18:31,240 --> 00:18:33,480
 time and maybe still fall back.

327
00:18:33,480 --> 00:18:37,890
 If we practice on and on until we realize nirvana, then we

328
00:18:37,890 --> 00:18:39,960
'll be set in the practice.

329
00:18:39,960 --> 00:18:42,830
 And it will only be a matter of time and eventually we'll

330
00:18:42,830 --> 00:18:44,480
 become free and not have to come back

331
00:18:44,480 --> 00:18:45,480
 at all.

332
00:18:45,480 --> 00:18:50,440
 We'll go to pure and pure and pure existences until finally

333
00:18:50,440 --> 00:18:53,200
 we're free because the mind

334
00:18:53,200 --> 00:18:57,280
 has come to incline towards nirvana.

335
00:18:57,280 --> 00:18:59,590
 Just like a tree that is inclining in one direction is

336
00:18:59,590 --> 00:19:01,400
 going to fall in that direction.

337
00:19:01,400 --> 00:19:04,780
 But if you change the direction that it's going to fall,

338
00:19:04,780 --> 00:19:06,320
 then you can look and say,

339
00:19:06,320 --> 00:19:09,000
 "Now it's going to fall in this direction."

340
00:19:09,000 --> 00:19:13,480
 So whereas our mind is inclined towards samsara, we change

341
00:19:13,480 --> 00:19:15,880
 it to incline towards nirvana.

342
00:19:15,880 --> 00:19:19,780
 And all you have to do is incline towards nirvana and it

343
00:19:19,780 --> 00:19:22,040
 will fall eventually in that

344
00:19:22,040 --> 00:19:23,040
 direction.

345
00:19:23,040 --> 00:19:27,710
 So this is a teaching on the five benefits or five reasons

346
00:19:27,710 --> 00:19:29,880
 why we practice vipassana

347
00:19:29,880 --> 00:19:31,880
 in the day.

348
00:19:31,880 --> 00:19:33,960
 Practice the four foundations of mindfulness.

349
00:19:33,960 --> 00:19:36,880
 Just a short pep talk before we do our actual practice.

350
00:19:36,880 --> 00:19:40,240
 So now we'll continue on with walking and sitting.

