1
00:00:00,000 --> 00:00:11,040
 Okay, good evening everyone.

2
00:00:11,040 --> 00:00:15,000
 Welcome to our Wednesday Dhamma.

3
00:00:15,000 --> 00:00:29,720
 Tonight we're here to study and learn about the Buddha's

4
00:00:29,720 --> 00:00:34,080
 teaching on mindfulness meditation

5
00:00:34,080 --> 00:00:39,110
 for the purpose of seeing clearly, which is in turn for the

6
00:00:39,110 --> 00:00:40,640
 purpose of freeing ourselves

7
00:00:40,640 --> 00:00:46,200
 from suffering.

8
00:00:46,200 --> 00:00:58,650
 It's easy to underestimate the power of that statement to

9
00:00:58,650 --> 00:01:02,760
 become free from suffering.

10
00:01:02,760 --> 00:01:03,880
 Think about that.

11
00:01:03,880 --> 00:01:04,880
 Think about that statement.

12
00:01:04,880 --> 00:01:07,680
 It's a bold claim.

13
00:01:07,680 --> 00:01:10,240
 It's an easy one to be skeptical of.

14
00:01:10,240 --> 00:01:12,440
 There's always that.

15
00:01:12,440 --> 00:01:19,520
 But putting aside skepticism, it's a unique phrasing.

16
00:01:19,520 --> 00:01:26,090
 I can't think of any other religious or spiritual path that

17
00:01:26,090 --> 00:01:29,240
 phrases things that way.

18
00:01:29,240 --> 00:01:35,050
 There's usually something else, some other baggage, some

19
00:01:35,050 --> 00:01:36,480
 other idea.

20
00:01:36,480 --> 00:01:39,640
 Usually it's phrased in terms of happiness.

21
00:01:39,640 --> 00:01:48,800
 There's a lot of appealing to our desire for happiness.

22
00:01:48,800 --> 00:01:57,100
 It's quite appealing to hear someone talk about this

23
00:01:57,100 --> 00:01:59,040
 happiness.

24
00:01:59,040 --> 00:02:03,840
 The Buddha talked about that as well.

25
00:02:03,840 --> 00:02:09,480
 But he made a claim that seeing clearly frees you from

26
00:02:09,480 --> 00:02:10,840
 suffering.

27
00:02:10,840 --> 00:02:22,040
 Panyāyapasati, when one sees with wisdom.

28
00:02:22,040 --> 00:02:31,360
 It's easy to underestimate it.

29
00:02:31,360 --> 00:02:34,720
 And what I mean when I'm thinking of there specifically is

30
00:02:34,720 --> 00:02:37,640
 how that really covers everything.

31
00:02:37,640 --> 00:02:45,840
 It covers all of life's problems.

32
00:02:45,840 --> 00:02:50,690
 And it changes perspective and understanding about a lot of

33
00:02:50,690 --> 00:02:52,800
 the issues that we face in

34
00:02:52,800 --> 00:02:53,800
 the world.

35
00:02:53,800 --> 00:03:10,340
 I get asked questions about politics, economics, science,

36
00:03:10,340 --> 00:03:13,440
 religion.

37
00:03:13,440 --> 00:03:14,840
 What's important?

38
00:03:14,840 --> 00:03:16,240
 What is important?

39
00:03:16,240 --> 00:03:18,320
 What's better?

40
00:03:18,320 --> 00:03:21,640
 Someone asked me recently is, what do I think of democracy?

41
00:03:21,640 --> 00:03:24,480
 I don't get asked that very often.

42
00:03:24,480 --> 00:03:30,040
 I mean, I've thought about it before, but I don't get asked

43
00:03:30,040 --> 00:03:31,160
 it much.

44
00:03:31,160 --> 00:03:34,080
 I've been asked what I think of socialism, capitalism.

45
00:03:34,080 --> 00:03:40,750
 I've asked myself that question, what the Buddha thought of

46
00:03:40,750 --> 00:03:42,640
 those things.

47
00:03:42,640 --> 00:03:46,040
 I've argued with communists and socialists.

48
00:03:46,040 --> 00:03:50,780
 Not because I think socialism or communism is bad

49
00:03:50,780 --> 00:03:54,440
 necessarily, but because there's a

50
00:03:54,440 --> 00:03:59,560
 deeper issue, the environment is another good one, right?

51
00:03:59,560 --> 00:04:11,000
 The environment is responding to our lack of care for it.

52
00:04:11,000 --> 00:04:17,440
 And we're likely to be in some great danger in the coming

53
00:04:17,440 --> 00:04:21,760
 years, in our lifetimes, because

54
00:04:21,760 --> 00:04:23,960
 of our negligence.

55
00:04:23,960 --> 00:04:28,400
 And so should a Buddhist be concerned about climate change?

56
00:04:28,400 --> 00:04:32,080
 Should a Buddhist be concerned about the environment?

57
00:04:32,080 --> 00:04:38,260
 And it's easy to conflate Buddhism with something else,

58
00:04:38,260 --> 00:04:39,000
 right?

59
00:04:39,000 --> 00:04:45,370
 Perhaps you got into Buddhism because you were a liberal or

60
00:04:45,370 --> 00:04:48,240
 a libertarian, maybe.

61
00:04:48,240 --> 00:04:50,160
 I don't know about that.

62
00:04:50,160 --> 00:04:58,330
 Because you were interested in anarchy or some kind of

63
00:04:58,330 --> 00:05:01,480
 counterculture.

64
00:05:01,480 --> 00:05:08,400
 Hippies got into Buddhism, and so they made mixed.

65
00:05:08,400 --> 00:05:12,490
 And it's easy to think that the Buddha must have agreed

66
00:05:12,490 --> 00:05:14,360
 with your worldview.

67
00:05:14,360 --> 00:05:18,280
 You interpret Buddhism based on your worldview, and you

68
00:05:18,280 --> 00:05:21,840
 think, well, Buddhism is this or that.

69
00:05:21,840 --> 00:05:32,080
 So you have to be careful there.

70
00:05:32,080 --> 00:05:37,580
 What's more important than trying to figure out, and this

71
00:05:37,580 --> 00:05:39,840
 is really important, what's

72
00:05:39,840 --> 00:05:46,980
 more important than trying to figure out which system or

73
00:05:46,980 --> 00:05:51,400
 pursuit the Buddha might have agreed

74
00:05:51,400 --> 00:06:04,530
 with, is to understand the cause of the problems that those

75
00:06:04,530 --> 00:06:12,800
 systems and activities, undertakings,

76
00:06:12,800 --> 00:06:20,520
 philosophies and so on, seek to remedy.

77
00:06:20,520 --> 00:06:23,020
 Whereas if they're not talking about alleviating suffering,

78
00:06:23,020 --> 00:06:24,760
 if they're not involved with alleviating

79
00:06:24,760 --> 00:06:27,880
 suffering, then they're not really addressing any problem.

80
00:06:27,880 --> 00:06:29,910
 And if they are involved with alleviating suffering, well,

81
00:06:29,910 --> 00:06:32,040
 we've already found the answer.

82
00:06:32,040 --> 00:06:35,400
 So we can pack it up and go home as they say.

83
00:06:35,400 --> 00:06:40,170
 I don't need to engage in those things theoretically, right

84
00:06:40,170 --> 00:06:40,440
?

85
00:06:40,440 --> 00:06:47,320
 Buddhism does that for us.

86
00:06:47,320 --> 00:06:50,560
 It's obviously not a fair thing to say.

87
00:06:50,560 --> 00:06:52,760
 It's an easy conclusion to come to.

88
00:06:52,760 --> 00:06:56,240
 But the problem with that statement, of course, is that

89
00:06:56,240 --> 00:06:58,780
 Buddhism can't, the Buddhist teaching

90
00:06:58,780 --> 00:07:04,120
 can't be practiced unless certain conditions are met.

91
00:07:04,120 --> 00:07:10,450
 So if you're in a society that is unstable, how could you

92
00:07:10,450 --> 00:07:14,200
 possibly practice Buddhism?

93
00:07:14,200 --> 00:07:20,490
 Nonetheless, nonetheless, we don't have to talk about

94
00:07:20,490 --> 00:07:23,520
 practicing meditation or Buddhism,

95
00:07:23,520 --> 00:07:29,860
 but we have to understand what are the root causes of

96
00:07:29,860 --> 00:07:35,480
 suffering, the root causes of problems.

97
00:07:35,480 --> 00:07:40,970
 So if we're going to ask which system is best, we have to

98
00:07:40,970 --> 00:07:44,040
 ask which ones contribute to our

99
00:07:44,040 --> 00:07:50,090
 problems, which ones get in the way or serve to take us

100
00:07:50,090 --> 00:07:53,960
 away from the path to freedom from

101
00:07:53,960 --> 00:08:01,120
 suffering.

102
00:08:01,120 --> 00:08:03,440
 And I'm not trying to say that I have any answers there.

103
00:08:03,440 --> 00:08:08,180
 And I think the point is not to determine, oh, yes, some

104
00:08:08,180 --> 00:08:10,940
 communism, that's much better

105
00:08:10,940 --> 00:08:15,150
 for people, much better for the practice of Buddhism

106
00:08:15,150 --> 00:08:17,000
 somehow, right?

107
00:08:17,000 --> 00:08:21,070
 Or vice versa, maybe capitalism is better because, well, it

108
00:08:21,070 --> 00:08:23,240
's maybe a little bit simpler.

109
00:08:23,240 --> 00:08:36,280
 I don't know.

110
00:08:36,280 --> 00:08:42,650
 The point is that it requires the proper perspective, that

111
00:08:42,650 --> 00:08:46,560
 you can't argue for any sort of system

112
00:08:46,560 --> 00:08:54,910
 or philosophy or undertaking scientific pursuit, religion,

113
00:08:54,910 --> 00:08:57,360
 philosophy, etc.

114
00:08:57,360 --> 00:09:01,370
 You can't argue for it unless you're arguing in terms of

115
00:09:01,370 --> 00:09:03,960
 the right conditions, unless you're

116
00:09:03,960 --> 00:09:11,200
 understanding what it's for.

117
00:09:11,200 --> 00:09:16,630
 And without addressing the root causes of suffering, you

118
00:09:16,630 --> 00:09:19,040
 can't ask that are good to

119
00:09:19,040 --> 00:09:20,640
 undertake.

120
00:09:20,640 --> 00:09:27,120
 Like suppose there were some political structure that was

121
00:09:27,120 --> 00:09:31,320
 just perfect, led to a utopia, just

122
00:09:31,320 --> 00:09:33,440
 perfectly designed.

123
00:09:33,440 --> 00:09:37,560
 There's no question that it would be useless.

124
00:09:37,560 --> 00:09:40,360
 It would be utterly useless.

125
00:09:40,360 --> 00:09:43,560
 The perfect system would be utterly useless.

126
00:09:43,560 --> 00:09:52,120
 That's not fair, but utterly ineffectual in its goals.

127
00:09:52,120 --> 00:09:59,080
 If it was implemented by corrupt individuals, right?

128
00:09:59,080 --> 00:10:01,840
 You can say capitalism is a horrible system.

129
00:10:01,840 --> 00:10:03,480
 Maybe it is.

130
00:10:03,480 --> 00:10:05,520
 You can say communism is a horrible system.

131
00:10:05,520 --> 00:10:10,320
 I know a lot of people do.

132
00:10:10,320 --> 00:10:15,790
 But ultimately, if either of those systems, first of all,

133
00:10:15,790 --> 00:10:18,720
 were implemented by people who

134
00:10:18,720 --> 00:10:24,810
 were of a pure mind and a pure intention, first of all, the

135
00:10:24,810 --> 00:10:27,760
 system would not be, would

136
00:10:27,760 --> 00:10:35,000
 lose any capacity to harm those involved.

137
00:10:35,000 --> 00:10:39,680
 I mean, as a basic principle, it doesn't hold for

138
00:10:39,680 --> 00:10:43,640
 everything because there's also systemic

139
00:10:43,640 --> 00:10:51,870
 violence, which is just because of the nature of the system

140
00:10:51,870 --> 00:10:52,400
.

141
00:10:52,400 --> 00:10:57,880
 And of course, there's always institutionalization, red

142
00:10:57,880 --> 00:11:02,120
 tape, et cetera, can cause harm to people.

143
00:11:02,120 --> 00:11:06,520
 And this is the second thing.

144
00:11:06,520 --> 00:11:10,960
 Any system implemented by people with a pure heart would

145
00:11:10,960 --> 00:11:13,760
 quickly and constantly be refined

146
00:11:13,760 --> 00:11:20,190
 and adjusted and changed by those people with good

147
00:11:20,190 --> 00:11:22,240
 intentions.

148
00:11:22,240 --> 00:11:23,920
 And this goes for really anything.

149
00:11:23,920 --> 00:11:30,060
 You can't have positive change without positivity, without

150
00:11:30,060 --> 00:11:32,160
 good intentions.

151
00:11:32,160 --> 00:11:38,080
 And so you see some political leaders that you can identify

152
00:11:38,080 --> 00:11:40,920
 who really care, who really

153
00:11:40,920 --> 00:11:49,430
 are sensitive and thoughtful, conscientious, and have a

154
00:11:49,430 --> 00:11:53,920
 sense of the communal nature of

155
00:11:53,920 --> 00:11:59,040
 life where we all are in this together.

156
00:11:59,040 --> 00:12:01,620
 That harming someone else is really just as bad as harming

157
00:12:01,620 --> 00:12:02,280
 yourself.

158
00:12:02,280 --> 00:12:06,780
 Taking advantage of someone else is no better than them

159
00:12:06,780 --> 00:12:09,120
 taking advantage of you.

160
00:12:09,120 --> 00:12:15,570
 Having these things, there are people, and it's those

161
00:12:15,570 --> 00:12:20,360
 people who work to change systems

162
00:12:20,360 --> 00:12:26,520
 and to undertake activities that are positive.

163
00:12:26,520 --> 00:12:31,360
 And so if we put our emphasis on worldly things, politics,

164
00:12:31,360 --> 00:12:33,880
 of course, comes to mind with a

165
00:12:33,880 --> 00:12:37,870
 large American audience, if we put our emphasis on these

166
00:12:37,870 --> 00:12:40,480
 things, and I'm not saying that you

167
00:12:40,480 --> 00:12:44,800
 shouldn't engage in your civic duty, I think we should.

168
00:12:44,800 --> 00:12:47,950
 I think you should, I don't, but those of you who are a

169
00:12:47,950 --> 00:12:49,600
 part of society should.

170
00:12:49,600 --> 00:12:53,880
 But I don't think that's where your heart should be.

171
00:12:53,880 --> 00:12:57,510
 That's where your interests should lie, your interests

172
00:12:57,510 --> 00:12:59,640
 should lie in what truly frees us

173
00:12:59,640 --> 00:13:00,640
 from suffering.

174
00:13:00,640 --> 00:13:01,640
 It's quite simple.

175
00:13:01,640 --> 00:13:05,780
 It's not so many things, and it boils down to greed, anger,

176
00:13:05,780 --> 00:13:07,680
 and delusion, just three

177
00:13:07,680 --> 00:13:08,680
 things.

178
00:13:08,680 --> 00:13:09,680
 That's all.

179
00:13:09,680 --> 00:13:12,950
 If you want to break it down, boil it down even further, it

180
00:13:12,950 --> 00:13:14,600
's just delusion, because

181
00:13:14,600 --> 00:13:18,600
 without delusion you couldn't have the greed or anger.

182
00:13:18,600 --> 00:13:21,410
 But nonetheless, it's the greed and the anger that do most

183
00:13:21,410 --> 00:13:22,880
 of the heavy, or a lot of the

184
00:13:22,880 --> 00:13:25,880
 heavy lifting.

185
00:13:25,880 --> 00:13:38,960
 And so ultimately, ultimately, meditation is essential.

186
00:13:38,960 --> 00:13:43,180
 Meditation being the cultivation of clarity of mind that

187
00:13:43,180 --> 00:13:45,560
 frees us from delusion, and in

188
00:13:45,560 --> 00:13:51,490
 turn frees us from greed and anger, and in turn frees us

189
00:13:51,490 --> 00:13:55,400
 from causing suffering to ourselves

190
00:13:55,400 --> 00:13:57,280
 and others.

191
00:13:57,280 --> 00:13:58,280
 All good things.

192
00:13:58,280 --> 00:14:01,450
 If you want to talk about something good, if you have a

193
00:14:01,450 --> 00:14:02,960
 question about what you should

194
00:14:02,960 --> 00:14:08,880
 do with your life, maybe I should become a politician, or

195
00:14:08,880 --> 00:14:10,960
 maybe I should work to cure

196
00:14:10,960 --> 00:14:14,280
 cancer or something like that.

197
00:14:14,280 --> 00:14:21,710
 None of those things is ever going to be effective for as

198
00:14:21,710 --> 00:14:25,360
 long as the world contains the world.

199
00:14:25,360 --> 00:14:27,760
 It's corruption.

200
00:14:27,760 --> 00:14:32,770
 And so it appears to some extent that we're becoming more

201
00:14:32,770 --> 00:14:33,800
 corrupt.

202
00:14:33,800 --> 00:14:37,380
 And this is a common thing to hear with religions, that

203
00:14:37,380 --> 00:14:39,280
 things are getting worse, doomsday is

204
00:14:39,280 --> 00:14:40,280
 coming.

205
00:14:40,280 --> 00:14:42,840
 It doesn't mean it's not true though.

206
00:14:42,840 --> 00:14:44,560
 It doesn't mean it is true.

207
00:14:44,560 --> 00:14:46,760
 A lot of people say things are getting better.

208
00:14:46,760 --> 00:14:52,630
 Stephen Pinker, a very famous scientist of some sort, he's

209
00:14:52,630 --> 00:14:55,480
 put forward some conclusive

210
00:14:55,480 --> 00:15:01,360
 evidence that says that things are good, because we can be

211
00:15:01,360 --> 00:15:04,440
 inclined to doom and gloom.

212
00:15:04,440 --> 00:15:05,840
 But there are other indicators.

213
00:15:05,840 --> 00:15:13,040
 The environment's not in very good shape, clearly.

214
00:15:13,040 --> 00:15:19,480
 And sickness, we're now in an age that may be, may not be

215
00:15:19,480 --> 00:15:22,840
 reversible, where sickness

216
00:15:22,840 --> 00:15:25,280
 is becoming a greater thing.

217
00:15:25,280 --> 00:15:29,680
 The Buddha said that our lifespan is going to decrease.

218
00:15:29,680 --> 00:15:32,090
 I think, as I said before, something that might be

219
00:15:32,090 --> 00:15:33,880
 countering it, and it kind of seems

220
00:15:33,880 --> 00:15:38,600
 an arrogant or proud thing to say, but by the theory you've

221
00:15:38,600 --> 00:15:40,920
 got to accept the idea that

222
00:15:40,920 --> 00:15:47,170
 Buddhism has probably had, not that we can see it or point

223
00:15:47,170 --> 00:15:50,240
 to it anywhere, but has had

224
00:15:50,240 --> 00:15:53,340
 a positive impact on the world to make things get better in

225
00:15:53,340 --> 00:15:54,120
 some ways.

226
00:15:54,120 --> 00:15:56,690
 Of course, things like ethics and so on, I think a lot of

227
00:15:56,690 --> 00:15:58,080
 it wouldn't have been there

228
00:15:58,080 --> 00:16:01,680
 without the influence of Buddhism.

229
00:16:01,680 --> 00:16:05,280
 Not all of it, but a fair amount of it, because it's such

230
00:16:05,280 --> 00:16:06,440
 an emphasis.

231
00:16:06,440 --> 00:16:11,140
 And there are so many people who have practiced Buddhism

232
00:16:11,140 --> 00:16:15,240
 throughout the world, throughout

233
00:16:15,240 --> 00:16:17,240
 history.

234
00:16:17,240 --> 00:16:20,100
 But when that's gone, and as Buddhism starts to decay, and

235
00:16:20,100 --> 00:16:21,640
 even at the same time, there

236
00:16:21,640 --> 00:16:27,420
 are, of course, the corrupting influences, it's those, it's

237
00:16:27,420 --> 00:16:29,840
 there where the battle lies,

238
00:16:29,840 --> 00:16:35,200
 it's there where the work must be done.

239
00:16:35,200 --> 00:16:37,410
 So if you want your world to be a good place, if you really

240
00:16:37,410 --> 00:16:38,880
 want to do something worthwhile,

241
00:16:38,880 --> 00:16:43,600
 I mean, there really is no avoiding the very basis of the

242
00:16:43,600 --> 00:16:46,120
 practice of mindfulness.

243
00:16:46,120 --> 00:16:48,600
 And then, of course, if you want to become a mindful

244
00:16:48,600 --> 00:16:51,160
 politician or a mindful philosopher,

245
00:16:51,160 --> 00:16:57,920
 a mindful scientist, then by all means, you'll do very well

246
00:16:57,920 --> 00:16:58,400
.

247
00:16:58,400 --> 00:17:02,980
 But without that, and so by extension, without people who

248
00:17:02,980 --> 00:17:05,920
 are spreading that, who are helping

249
00:17:05,920 --> 00:17:10,700
 others to come to the practice of clarity, practice of

250
00:17:10,700 --> 00:17:13,920
 mindfulness and the clarity that

251
00:17:13,920 --> 00:17:22,290
 comes from it, there'll be no, no, no reason, no benefit,

252
00:17:22,290 --> 00:17:27,760
 no gain from focusing more theoretical

253
00:17:27,760 --> 00:17:31,640
 or conceptual goods in the world.

254
00:17:31,640 --> 00:17:36,600
 So anyway, that's my thought for tonight.

255
00:17:36,600 --> 00:17:39,600
 We're going to mini Dhamma talk.

256
00:17:39,600 --> 00:17:42,680
 I guess we have some questions.

257
00:17:42,680 --> 00:17:47,250
 So I'm going to say, at this point, chat will only be,

258
00:17:47,250 --> 00:17:50,000
 should only be used for questions

259
00:17:50,000 --> 00:17:51,000
 from here on.

260
00:17:51,000 --> 00:17:54,630
 If there's anything that's not a question, I'm going to ask

261
00:17:54,630 --> 00:17:56,040
 Chris to delete it.

262
00:17:56,040 --> 00:18:00,130
 Chris is again here kindly to help answer, ask the

263
00:18:00,130 --> 00:18:03,040
 questions and also to moderate the

264
00:18:03,040 --> 00:18:04,040
 chat.

265
00:18:04,040 --> 00:18:10,410
 We may have other moderators, I think we were trying to get

266
00:18:10,410 --> 00:18:11,360
 some.

267
00:18:11,360 --> 00:18:19,800
 Okay, let's begin.

268
00:18:19,800 --> 00:18:23,600
 Far into meditation, but seeing a lot and suffering from

269
00:18:23,600 --> 00:18:25,480
 that seeing, having a hard

270
00:18:25,480 --> 00:18:30,050
 time during this unfolding, how do I motivate myself to

271
00:18:30,050 --> 00:18:31,800
 keep meditating?

272
00:18:31,800 --> 00:18:34,920
 You don't suffer from the seeing.

273
00:18:34,920 --> 00:18:39,550
 You do suffer from the desire not to see, meaning we suffer

274
00:18:39,550 --> 00:18:41,720
 from our aversion towards

275
00:18:41,720 --> 00:18:46,760
 things and because meditation is about facing things.

276
00:18:46,760 --> 00:18:47,760
 Absolutely.

277
00:18:47,760 --> 00:18:53,200
 It's, it's not, it shouldn't be a cause, a big mystery as

278
00:18:53,200 --> 00:18:55,760
 to why meditation seems to

279
00:18:55,760 --> 00:18:59,630
 cause suffering, but it's not the meditation that's doing

280
00:18:59,630 --> 00:19:00,280
 that.

281
00:19:00,280 --> 00:19:04,360
 It's the aversion towards facing things.

282
00:19:04,360 --> 00:19:07,600
 We're triggered by things.

283
00:19:07,600 --> 00:19:10,970
 Meditation is about showing yourself that until you see

284
00:19:10,970 --> 00:19:12,920
 that, oh yes, this thing that

285
00:19:12,920 --> 00:19:15,120
 I'm triggered by is actually not a problem.

286
00:19:15,120 --> 00:19:18,160
 It's the being triggered, that's the problem.

287
00:19:18,160 --> 00:19:21,460
 Once you do that, you're no longer triggered because you're

288
00:19:21,460 --> 00:19:22,840
 no longer triggered.

289
00:19:22,840 --> 00:19:23,920
 You no longer suffer.

290
00:19:23,920 --> 00:19:25,280
 There is no other way.

291
00:19:25,280 --> 00:19:29,370
 It is impossible to avoid things as a means of freeing

292
00:19:29,370 --> 00:19:31,720
 yourself from suffering.

293
00:19:31,720 --> 00:19:33,040
 So that should motivate you.

294
00:19:33,040 --> 00:19:38,350
 There's absolutely nothing wrong with that experience

295
00:19:38,350 --> 00:19:40,920
 except for the fact that you're

296
00:19:40,920 --> 00:19:44,330
 reacting to, to the experience and probably reacting to the

297
00:19:44,330 --> 00:19:45,880
 reaction because it makes

298
00:19:45,880 --> 00:19:50,720
 you depressed and discouraged and it shouldn't.

299
00:19:50,720 --> 00:19:54,240
 But when you're discouraged, try and note that.

300
00:19:54,240 --> 00:20:01,240
 More importantly, just try and note the disliking, don't

301
00:20:01,240 --> 00:20:02,240
 like.

302
00:20:02,240 --> 00:20:03,240
 And be patient.

303
00:20:03,240 --> 00:20:04,720
 You're not going to get rid of the disliking.

304
00:20:04,720 --> 00:20:06,820
 You're not going to get rid of any of your habits overnight

305
00:20:06,820 --> 00:20:07,000
.

306
00:20:07,000 --> 00:20:08,000
 They're habits.

307
00:20:08,000 --> 00:20:09,000
 They're habitual.

308
00:20:09,000 --> 00:20:17,800
 They take work and effort and time to overcome.

309
00:20:17,800 --> 00:20:20,160
 Yuck.

310
00:20:20,160 --> 00:20:31,080
 Well, I think it's being interrupted.

311
00:20:31,080 --> 00:20:34,800
 How is the stream, everyone?

312
00:20:34,800 --> 00:20:45,160
 Not receiving enough video.

313
00:20:45,160 --> 00:21:14,520
 I don't really know what's going on.

