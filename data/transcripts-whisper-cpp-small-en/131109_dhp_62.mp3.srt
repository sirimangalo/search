1
00:00:00,000 --> 00:00:05,360
 Hi and welcome back to our study of the Dhammapada.

2
00:00:05,360 --> 00:00:09,910
 Today we continue on with verse number 62, which reads as

3
00:00:09,910 --> 00:00:13,000
 follows.

4
00:00:13,000 --> 00:00:23,830
 Puta mati, danam mati, iti balo vihanyati, atahi atano nati

5
00:00:23,830 --> 00:00:28,440
, kutoputta kutodanang.

6
00:00:28,440 --> 00:00:39,990
 Which means, puta mati, I have sons, danam mati, I have

7
00:00:39,990 --> 00:00:44,160
 wealth, iti balo vihanyati,

8
00:00:44,160 --> 00:00:53,550
 thus the fool worries himself or is backstored, is caught

9
00:00:53,550 --> 00:00:58,360
 up, is concerned, is distraught

10
00:00:58,360 --> 00:01:00,400
 by these thoughts.

11
00:01:00,400 --> 00:01:07,600
 Atahi atano nati, oneself indeed, is not oneself.

12
00:01:07,600 --> 00:01:18,980
 Kutoputta kutoputto danang, how therefore could either sons

13
00:01:18,980 --> 00:01:24,400
 or wealth belong to oneself.

14
00:01:24,400 --> 00:01:29,610
 The story behind the verse, the story goes that there was

15
00:01:29,610 --> 00:01:32,360
 this rich man called Ananda,

16
00:01:32,360 --> 00:01:41,600
 Ananda, say "T", the rich man named Ananda.

17
00:01:41,600 --> 00:01:46,590
 And he taught his children, he lived his life and taught

18
00:01:46,590 --> 00:01:49,720
 his children to not spend anything,

19
00:01:49,720 --> 00:01:56,520
 to never give, he said, "Don't ever give away anything

20
00:01:56,520 --> 00:01:58,800
 because wealth is difficult to gain,

21
00:01:58,800 --> 00:02:02,550
 it's hard to hold onto and it can vanish at any time, so

22
00:02:02,550 --> 00:02:04,720
 you have to work very, very hard

23
00:02:04,720 --> 00:02:13,480
 to keep it and keep every coin accounted for."

24
00:02:13,480 --> 00:02:19,810
 So over his years of cultivating wealth and obsessing over

25
00:02:19,810 --> 00:02:22,760
 wealth, he came to view it

26
00:02:22,760 --> 00:02:29,080
 as something that needed protecting.

27
00:02:29,080 --> 00:02:32,280
 He got so wound up that he thought every coin should be

28
00:02:32,280 --> 00:02:34,520
 accounted for and should never give

29
00:02:34,520 --> 00:02:40,570
 anything to anyone, should never give away or foolishly

30
00:02:40,570 --> 00:02:42,520
 spend your wealth.

31
00:02:42,520 --> 00:02:46,910
 It reminds us of the rich man with the pancakes, but this

32
00:02:46,910 --> 00:02:49,560
 is a different man, it seems to be

33
00:02:49,560 --> 00:02:59,000
 that wealth, there's a common theme in the Bali canon of

34
00:02:59,000 --> 00:03:02,360
 richness often leading to miserliness,

35
00:03:02,360 --> 00:03:11,200
 which of course you see in modern day as well.

36
00:03:11,200 --> 00:03:14,740
 There are some examples of rich people who are very

37
00:03:14,740 --> 00:03:17,240
 generous and kind, there are many

38
00:03:17,240 --> 00:03:19,790
 rich people who can only think about how they can amass

39
00:03:19,790 --> 00:03:21,520
 more wealth, they can get more and

40
00:03:21,520 --> 00:03:22,520
 more.

41
00:03:22,520 --> 00:03:29,350
 So the Buddha said for this reason, even if it were to rain

42
00:03:29,350 --> 00:03:32,600
 gold, you would never get

43
00:03:32,600 --> 00:03:36,840
 enough for even one person.

44
00:03:36,840 --> 00:03:40,110
 So this is a story of Ananda and not much of a story

45
00:03:40,110 --> 00:03:42,520
 because he passed away and left his

46
00:03:42,520 --> 00:03:45,040
 fortune to his son.

47
00:03:45,040 --> 00:03:48,270
 And the story is actually about his reincarnation when he

48
00:03:48,270 --> 00:03:50,600
 was reborn, he was reborn as an outcast

49
00:03:50,600 --> 00:04:01,290
 in a village of it says around a thousand outcast beggars

50
00:04:01,290 --> 00:04:07,320
 or poor unemployed outcasts

51
00:04:07,320 --> 00:04:10,110
 who were unable of course to get any but the worst of jobs

52
00:04:10,110 --> 00:04:12,080
 and had to live either off begging

53
00:04:12,080 --> 00:04:17,400
 or off slavery and slave labor, et cetera.

54
00:04:17,400 --> 00:04:20,510
 Now from the moment that he was conceived in his mother's

55
00:04:20,510 --> 00:04:22,160
 womb, the entire village was

56
00:04:22,160 --> 00:04:30,700
 unable to get anything to make even the slightest bit of

57
00:04:30,700 --> 00:04:34,280
 money or food, they weren't able to

58
00:04:34,280 --> 00:04:36,680
 beg, they weren't able to work.

59
00:04:36,680 --> 00:04:39,920
 From the moment that he was conceived, he cursed the whole

60
00:04:39,920 --> 00:04:40,680
 village.

61
00:04:40,680 --> 00:04:49,630
 So the story goes, some kind of group karma at work I guess

62
00:04:49,630 --> 00:04:49,800
.

63
00:04:49,800 --> 00:04:54,960
 And so they split the village, they got together and they

64
00:04:54,960 --> 00:04:58,440
 said something must be wrong here,

65
00:04:58,440 --> 00:04:59,440
 someone must be causing this.

66
00:04:59,440 --> 00:05:04,720
 And so they split the village in half and had them go in

67
00:05:04,720 --> 00:05:07,560
 separate locations and go begging

68
00:05:07,560 --> 00:05:13,780
 or go working or whatever and they found that one half was

69
00:05:13,780 --> 00:05:17,320
 able to get by fine as per normal.

70
00:05:17,320 --> 00:05:20,640
 The other group still was cursed.

71
00:05:20,640 --> 00:05:24,430
 And so they cut that group in half and then cut the other

72
00:05:24,430 --> 00:05:26,400
 group in half and then half

73
00:05:26,400 --> 00:05:29,470
 again until they got down to two families and they split

74
00:05:29,470 --> 00:05:30,960
 them up and they found that

75
00:05:30,960 --> 00:05:35,060
 this woman, this pregnant woman was the cause of all the

76
00:05:35,060 --> 00:05:37,520
 problems so they kicked them out

77
00:05:37,520 --> 00:05:42,360
 and sent them on their way.

78
00:05:42,360 --> 00:05:47,230
 And then when he was born, his parents found the same thing

79
00:05:47,230 --> 00:05:49,460
, that if they went on alms

80
00:05:49,460 --> 00:05:52,560
 alone or if they went out working or whatever alone, then

81
00:05:52,560 --> 00:05:54,240
 they could get money just fine

82
00:05:54,240 --> 00:05:59,560
 and they could get by as well as could be expected.

83
00:05:59,560 --> 00:06:02,320
 But if he was with them, they would all get nothing.

84
00:06:02,320 --> 00:06:07,120
 So they kicked him out as well and gave him a little piece

85
00:06:07,120 --> 00:06:09,280
 of maybe a piece of pot or

86
00:06:09,280 --> 00:06:17,160
 something and said, "Go, you must prepare for yourself."

87
00:06:17,160 --> 00:06:19,440
 They kept him around until he was like seven years old and

88
00:06:19,440 --> 00:06:20,760
 then they sent him off and said,

89
00:06:20,760 --> 00:06:21,760
 "Go by yourself."

90
00:06:21,760 --> 00:06:24,840
 The other thing is he was very, very ugly so he kind of

91
00:06:24,840 --> 00:06:26,280
 looked like an ogre.

92
00:06:26,280 --> 00:06:37,040
 This was the horrible repercussions of being such a miser.

93
00:06:37,040 --> 00:06:39,860
 One day he was wandering around trying to get whatever alms

94
00:06:39,860 --> 00:06:41,240
 he could, which of course

95
00:06:41,240 --> 00:06:42,240
 wasn't much.

96
00:06:42,240 --> 00:06:46,030
 He wandered back to his old home and he saw this house and

97
00:06:46,030 --> 00:06:47,840
 he recognized it and so he

98
00:06:47,840 --> 00:06:50,990
 just walked right in and he started looking around the

99
00:06:50,990 --> 00:06:51,600
 house.

100
00:06:51,600 --> 00:06:53,780
 Not quite sure why he was there but seeing that it looked

101
00:06:53,780 --> 00:06:55,040
 very familiar and kind of like

102
00:06:55,040 --> 00:06:56,040
 a home.

103
00:06:56,040 --> 00:06:58,190
 So he was walking around and he went into one room and

104
00:06:58,190 --> 00:06:59,800
 there were his grandchildren,

105
00:06:59,800 --> 00:07:04,150
 his sons' sons and they freaked out and they called out and

106
00:07:04,150 --> 00:07:06,120
 they said, "Monster, there's

107
00:07:06,120 --> 00:07:08,000
 a monster in here."

108
00:07:08,000 --> 00:07:12,290
 The servants came over and beat him and threw him out just

109
00:07:12,290 --> 00:07:15,200
 as the Buddha was walking by.

110
00:07:15,200 --> 00:07:19,390
 The Buddha's walking by going on alms around and he sees

111
00:07:19,390 --> 00:07:22,520
 this young beggar beaten to a pulp

112
00:07:22,520 --> 00:07:26,680
 or beaten quite severely lying on the side of the road.

113
00:07:26,680 --> 00:07:29,800
 Not to a pulp, he's still alive.

114
00:07:29,800 --> 00:07:32,690
 He's beaten up and the Buddha looks at him and then he

115
00:07:32,690 --> 00:07:34,520
 looks at Ananda who is our Ananda,

116
00:07:34,520 --> 00:07:37,960
 not the rich Ananda, walking beside him.

117
00:07:37,960 --> 00:07:47,220
 He turns and looks at Ananda and Ananda knows to take this

118
00:07:47,220 --> 00:07:52,000
 as an instigation to ask the

119
00:07:52,000 --> 00:07:57,400
 question so he asks the Buddha, "What's the story of this

120
00:07:57,400 --> 00:07:59,760
 guy, Reverend Sir?"

121
00:07:59,760 --> 00:08:03,970
 The Buddha said he used to be the great rich man, Ananda,

122
00:08:03,970 --> 00:08:05,960
 and this was his house.

123
00:08:05,960 --> 00:08:12,080
 Ananda called the rich man's son and the Buddha explained

124
00:08:12,080 --> 00:08:15,080
 to him, "This was your father."

125
00:08:15,080 --> 00:08:18,930
 He said, "I don't believe it looks at him and he's this

126
00:08:18,930 --> 00:08:20,160
 ugly outcast."

127
00:08:20,160 --> 00:08:24,080
 He said, "How can he go from a rich man to a beggar?"

128
00:08:24,080 --> 00:08:27,320
 The Buddha had him go in the house and find all his

129
00:08:27,320 --> 00:08:29,520
 treasures and the kid was able to

130
00:08:29,520 --> 00:08:33,640
 actually remember where everything was and so he proved it.

131
00:08:33,640 --> 00:08:36,400
 Then the Buddha taught this verse.

132
00:08:36,400 --> 00:08:38,600
 Simple story.

133
00:08:38,600 --> 00:08:42,960
 The point here is to remind us not to be negligent.

134
00:08:42,960 --> 00:08:45,880
 It's a simple lesson.

135
00:08:45,880 --> 00:08:50,190
 We think of material possessions, all of those things, not

136
00:08:50,190 --> 00:08:52,960
 just puta and dana, our sons and

137
00:08:52,960 --> 00:08:55,440
 our wealth, but everything.

138
00:08:55,440 --> 00:09:01,770
 Look at all of our belongings, all of our enjoyments in the

139
00:09:01,770 --> 00:09:04,800
 material realm, thinking

140
00:09:04,800 --> 00:09:09,480
 of them as me, as mine, as somehow controllable.

141
00:09:09,480 --> 00:09:14,270
 We either hold on to them as ours to enjoy or we hold on to

142
00:09:14,270 --> 00:09:16,640
 ours to control or ours to

143
00:09:16,640 --> 00:09:21,360
 own or ours to dwell in.

144
00:09:21,360 --> 00:09:23,480
 Our body, we think of it as ourselves.

145
00:09:23,480 --> 00:09:25,160
 Our house, we think of it as ourselves.

146
00:09:25,160 --> 00:09:27,760
 Our car, our bedroom, everything.

147
00:09:27,760 --> 00:09:30,400
 All of our family, we think of them as our family, all of

148
00:09:30,400 --> 00:09:31,960
 our friends, we think of them

149
00:09:31,960 --> 00:09:33,880
 as our friends.

150
00:09:33,880 --> 00:09:40,570
 So we get caught up in this expectation of being able to

151
00:09:40,570 --> 00:09:44,520
 control, of being able to rely

152
00:09:44,520 --> 00:09:49,760
 upon, of being able to enjoy all of these things.

153
00:09:49,760 --> 00:09:52,120
 And the Buddha said, "Even yourself is not yourself."

154
00:09:52,120 --> 00:09:54,920
 Atahiyatano nati.

155
00:09:54,920 --> 00:09:57,600
 It's funny, the more common one that people know is atahiy

156
00:09:57,600 --> 00:09:59,240
atano nato, which is, you wonder

157
00:09:59,240 --> 00:10:02,830
 whether one of them is actually a ... The other one is

158
00:10:02,830 --> 00:10:05,160
 actually a ... Anyway, the Buddha taught

159
00:10:05,160 --> 00:10:06,160
 both ways.

160
00:10:06,160 --> 00:10:11,350
 Atahiyatano nato self is a refuge of self, which means one

161
00:10:11,350 --> 00:10:13,480
 is one's own refuge.

162
00:10:13,480 --> 00:10:15,240
 But that's referring to the four satipatanas.

163
00:10:15,240 --> 00:10:20,940
 One makes a refuge by practicing on one's own, not relying

164
00:10:20,940 --> 00:10:22,920
 on anyone else.

165
00:10:22,920 --> 00:10:28,090
 But even that one can't control, one can't rely upon one's

166
00:10:28,090 --> 00:10:30,200
 expectations, one can't be

167
00:10:30,200 --> 00:10:40,920
 fulfilled in one's desires and in one's demands.

168
00:10:40,920 --> 00:10:47,920
 So as a result, iti balo yanyati, a fool, is vexed by these

169
00:10:47,920 --> 00:10:49,520
 thoughts.

170
00:10:49,520 --> 00:10:52,970
 They think of their children and their loved ones and all

171
00:10:52,970 --> 00:10:54,800
 of the people in their life as

172
00:10:54,800 --> 00:10:58,100
 being controllable in them and they're not able to control

173
00:10:58,100 --> 00:10:59,960
 these things, control these

174
00:10:59,960 --> 00:11:00,960
 people.

175
00:11:00,960 --> 00:11:03,750
 And then they suffer, they're vexed, they're worried all

176
00:11:03,750 --> 00:11:05,400
 the time, worried about how they

177
00:11:05,400 --> 00:11:08,800
 might be and trying to figure out ways to control people,

178
00:11:08,800 --> 00:11:10,720
 ways to control their family,

179
00:11:10,720 --> 00:11:13,780
 ways to control their friends, ways to control their

180
00:11:13,780 --> 00:11:16,760
 employers, their employees, their co-workers,

181
00:11:16,760 --> 00:11:18,840
 and so on and so on.

182
00:11:18,840 --> 00:11:22,720
 How can I control people to be the way I want them to be?

183
00:11:22,720 --> 00:11:25,450
 And the same goes with our dhanas, our possessions, our

184
00:11:25,450 --> 00:11:27,680
 wealth, all of our belongings, guarding

185
00:11:27,680 --> 00:11:32,270
 our houses, guarding our valuables, guarding our

186
00:11:32,270 --> 00:11:36,320
 possessions, guarding our enjoyments,

187
00:11:36,320 --> 00:11:41,610
 guarding our own bodies, being careful to make sure that we

188
00:11:41,610 --> 00:11:44,280
 can always enjoy the pleasure

189
00:11:44,280 --> 00:11:47,080
 that we enjoy now.

190
00:11:47,080 --> 00:11:51,180
 None of this can be controlled and when it's out of our

191
00:11:51,180 --> 00:11:53,800
 control, we're vexed by it, especially

192
00:11:53,800 --> 00:11:56,620
 foolish people who really believe that these things are

193
00:11:56,620 --> 00:11:58,400
 going to make them happy, people

194
00:11:58,400 --> 00:12:02,520
 who believe in the world bringing happiness end up being

195
00:12:02,520 --> 00:12:04,200
 vexed quite often.

196
00:12:04,200 --> 00:12:07,810
 And because of their lack of wisdom, they're not able to

197
00:12:07,810 --> 00:12:10,000
 see this and so as a result, they

198
00:12:10,000 --> 00:12:11,600
 constantly vexed.

199
00:12:11,600 --> 00:12:14,730
 They do it again and again and again, doing the same thing

200
00:12:14,730 --> 00:12:16,560
 over and over again, expecting

201
00:12:16,560 --> 00:12:22,300
 different results, forgetting that they are constantly

202
00:12:22,300 --> 00:12:23,280
 upset.

203
00:12:23,280 --> 00:12:26,160
 The Buddha reminds us, even ourselves is not, even ourself

204
00:12:26,160 --> 00:12:27,080
 is not ourself.

205
00:12:27,080 --> 00:12:30,480
 What this means is we can't even control ourselves.

206
00:12:30,480 --> 00:12:33,610
 We can make choices, we can have intentions, but we can't

207
00:12:33,610 --> 00:12:34,840
 really control it.

208
00:12:34,840 --> 00:12:39,280
 We can't say, "I'm going to be happy all the time," or "I'm

209
00:12:39,280 --> 00:12:41,600
 going to be calm all the time."

210
00:12:41,600 --> 00:12:44,620
 We can't say that we're never going to feel pain or we're

211
00:12:44,620 --> 00:12:46,120
 not going to get angry or we're

212
00:12:46,120 --> 00:12:48,120
 not going to get upset.

213
00:12:48,120 --> 00:12:54,990
 We can't say this, even about our own minds, even about our

214
00:12:54,990 --> 00:12:56,360
 own being.

215
00:12:56,360 --> 00:12:58,760
 So how then could we possibly do this?

216
00:12:58,760 --> 00:13:01,840
 How then could we possibly hold on to these things?

217
00:13:01,840 --> 00:13:11,030
 How then could we expect that we're going to enjoy the

218
00:13:11,030 --> 00:13:13,400
 possession of these things forever?

219
00:13:13,400 --> 00:13:16,780
 How could we possibly forget that we're going to lose them

220
00:13:16,780 --> 00:13:17,920
 all when we die?

221
00:13:17,920 --> 00:13:23,520
 This is what Ananda Sethi, this guy, didn't realize.

222
00:13:23,520 --> 00:13:27,950
 Really the most impressive part of this story is how

223
00:13:27,950 --> 00:13:29,920
 quickly he was gone.

224
00:13:29,920 --> 00:13:31,520
 This is what shocked his son.

225
00:13:31,520 --> 00:13:33,440
 It's unable to believe that.

226
00:13:33,440 --> 00:13:34,440
 Just like that.

227
00:13:34,440 --> 00:13:40,810
 From one moment, the moment of death turned him from a rich

228
00:13:40,810 --> 00:13:44,600
, powerful, influential person

229
00:13:44,600 --> 00:13:50,780
 to a nobody, to the lowest of the low, an outcast of the

230
00:13:50,780 --> 00:13:52,240
 outcast.

231
00:13:52,240 --> 00:13:53,240
 That's the truth.

232
00:13:53,240 --> 00:13:55,040
 That's what the Buddha means by it's not self.

233
00:13:55,040 --> 00:13:58,280
 You can't even hold on to your own being, your own status.

234
00:13:58,280 --> 00:14:00,160
 "I think I'm a monk.

235
00:14:00,160 --> 00:14:02,920
 The moment I die, that's gone."

236
00:14:02,920 --> 00:14:05,520
 You think you're a human being, the moment you die, that's

237
00:14:05,520 --> 00:14:05,960
 gone.

238
00:14:05,960 --> 00:14:11,160
 The next moment you can be an earthworm or a dung beetle.

239
00:14:11,160 --> 00:14:15,120
 Everything that we collect, everything that we hold on to,

240
00:14:15,120 --> 00:14:16,640
 no security for us.

241
00:14:16,640 --> 00:14:19,130
 All these people in some cultures, they will bury their

242
00:14:19,130 --> 00:14:20,840
 belongings with them, thinking

243
00:14:20,840 --> 00:14:25,300
 that they can bring it into their next life or they'll burn

244
00:14:25,300 --> 00:14:27,000
 certain objects.

245
00:14:27,000 --> 00:14:32,270
 In Thailand and China, they burn these really crappy houses

246
00:14:32,270 --> 00:14:35,160
 and fake stuff and everything.

247
00:14:35,160 --> 00:14:36,870
 You think, "Wow, they're going to get a lot of fake stuff

248
00:14:36,870 --> 00:14:37,760
 when they go to heaven."

249
00:14:37,760 --> 00:14:39,650
 The idea is that they can take that stuff with them if they

250
00:14:39,650 --> 00:14:40,120
 burn it.

251
00:14:40,120 --> 00:14:43,090
 Since it's all garbage, you think, "Wow, it's not really

252
00:14:43,090 --> 00:14:44,920
 ... Even if it did work."

253
00:14:44,920 --> 00:14:48,400
 Of course, it doesn't work.

254
00:14:48,400 --> 00:14:49,400
 We don't possess anything.

255
00:14:49,400 --> 00:14:51,920
 We just happen to be going in the same direction as all

256
00:14:51,920 --> 00:14:52,720
 this stuff.

257
00:14:52,720 --> 00:14:57,640
 We happen to be in the same physical locale.

258
00:14:57,640 --> 00:15:01,310
 We manage to make choices that bring all this stuff near us

259
00:15:01,310 --> 00:15:02,280
 and that's it.

260
00:15:02,280 --> 00:15:03,640
 We can't do any better than that.

261
00:15:03,640 --> 00:15:09,820
 We can't control ... Even our enjoyment is fleeting and ep

262
00:15:09,820 --> 00:15:12,280
hemeral, something that cannot

263
00:15:12,280 --> 00:15:13,280
 be relied upon.

264
00:15:13,280 --> 00:15:17,430
 It's very much out of our control based on causes and

265
00:15:17,430 --> 00:15:20,600
 conditions that are far more powerful

266
00:15:20,600 --> 00:15:22,600
 than any one of us.

267
00:15:22,600 --> 00:15:25,600
 It's a reminder for us.

268
00:15:25,600 --> 00:15:29,090
 In regards to meditation, a reminder for us to keep our

269
00:15:29,090 --> 00:15:31,000
 minds clear and to be clear about

270
00:15:31,000 --> 00:15:32,000
 all the things that we use.

271
00:15:32,000 --> 00:15:35,080
 It doesn't mean that we shouldn't be using our possessions.

272
00:15:35,080 --> 00:15:41,320
 It doesn't mean we shouldn't live.

273
00:15:41,320 --> 00:15:46,620
 It means that when we do experience even pleasure or when

274
00:15:46,620 --> 00:15:50,680
 you're eating food, the pleasure of

275
00:15:50,680 --> 00:15:55,570
 food, when you're seeing beautiful things, the pleasure

276
00:15:55,570 --> 00:15:57,640
 that comes from that.

277
00:15:57,640 --> 00:16:00,360
 But we should see it simply as a feeling.

278
00:16:00,360 --> 00:16:02,920
 We should see the experience simply as an experience.

279
00:16:02,920 --> 00:16:07,490
 Experience is seeing, hearing, smelling, tasting, feeling,

280
00:16:07,490 --> 00:16:08,440
 thinking.

281
00:16:08,440 --> 00:16:13,160
 See liking as liking, see disliking as disliking.

282
00:16:13,160 --> 00:16:15,160
 Come to see that these things are not ourself, are not

283
00:16:15,160 --> 00:16:16,080
 under our control.

284
00:16:16,080 --> 00:16:20,260
 Try to keep our minds clear because all of this will catch

285
00:16:20,260 --> 00:16:21,760
 you when you die.

286
00:16:21,760 --> 00:16:25,690
 If you have great wanting, someone who is very, very rich

287
00:16:25,690 --> 00:16:27,880
 because they're clinging so

288
00:16:27,880 --> 00:16:30,840
 much, what goes with them is the clinging.

289
00:16:30,840 --> 00:16:32,400
 That's why you're born very, very poor.

290
00:16:32,400 --> 00:16:35,480
 That's why rich people are very quick to be born very poor

291
00:16:35,480 --> 00:16:36,960
 because they have so much

292
00:16:36,960 --> 00:16:38,880
 greed and attachment.

293
00:16:38,880 --> 00:16:41,720
 It's that want that goes with you.

294
00:16:41,720 --> 00:16:43,040
 You're born wanting.

295
00:16:43,040 --> 00:16:45,960
 You're born in a constant state of want.

296
00:16:45,960 --> 00:16:48,000
 Amassing wealth doesn't make you wealthy.

297
00:16:48,000 --> 00:16:53,880
 Being generous and kind and having a mind of surplus.

298
00:16:53,880 --> 00:16:57,120
 When you think you have surplus, when you're content with

299
00:16:57,120 --> 00:16:59,000
 what you have, then you'll always

300
00:16:59,000 --> 00:17:00,000
 be content.

301
00:17:00,000 --> 00:17:03,120
 You'll never know the words nutty.

302
00:17:03,120 --> 00:17:05,000
 When you want something, you'll never be discontent.

303
00:17:05,000 --> 00:17:07,720
 You'll never know discontent because your mind is content.

304
00:17:07,720 --> 00:17:13,760
 It's funny how it works that way.

305
00:17:13,760 --> 00:17:17,520
 The mind that is clinging is the mind that is corrupt.

306
00:17:17,520 --> 00:17:20,490
 Clinging to self is the worst type of corruption because it

307
00:17:20,490 --> 00:17:22,760
 leads to all other likes and dislikes.

308
00:17:22,760 --> 00:17:25,400
 We have to keep our minds clear.

309
00:17:25,400 --> 00:17:26,680
 This is why meditation is important.

310
00:17:26,680 --> 00:17:29,780
 When we live our lives, however you live your lives, keep

311
00:17:29,780 --> 00:17:31,600
 your minds clear and do not cling

312
00:17:31,600 --> 00:17:36,400
 to self either in regards to your own self or in regards to

313
00:17:36,400 --> 00:17:39,000
 the things that you enjoy.

314
00:17:39,000 --> 00:17:43,950
 Peace lesson from the Dhammapada and from our friend, the

315
00:17:43,950 --> 00:17:47,400
 former Ananda Sethi, who managed

316
00:17:47,400 --> 00:17:50,240
 to find his way out of that mess.

317
00:17:50,240 --> 00:17:53,500
 So thank you all for tuning in and I wish you all peace,

318
00:17:53,500 --> 00:17:55,240
 happiness and freedom from

319
00:17:55,240 --> 00:17:56,680
 suffering and see you next time.

