1
00:00:00,000 --> 00:00:08,560
 Hi, in this video I'm going to teach you how to practice

2
00:00:08,560 --> 00:00:10,560
 meditation.

3
00:00:10,560 --> 00:00:13,180
 What is meditation?

4
00:00:13,180 --> 00:00:16,400
 Meditation means to focus your mind on something.

5
00:00:16,400 --> 00:00:21,050
 It means to clear your mind, to calm it down, and to clean

6
00:00:21,050 --> 00:00:23,180
 out all bad thoughts.

7
00:00:23,180 --> 00:00:26,780
 When you practice meditation, you try to focus on something

8
00:00:26,780 --> 00:00:28,400
 with all your attention, not

9
00:00:28,400 --> 00:00:33,080
 letting your mind wander or get mixed up in bad thoughts or

10
00:00:33,080 --> 00:00:34,280
 feelings.

11
00:00:34,280 --> 00:00:36,840
 Why practice meditation?

12
00:00:36,840 --> 00:00:40,960
 First, meditation makes you happy.

13
00:00:40,960 --> 00:00:45,030
 When your mind is focused and clear, you will forget about

14
00:00:45,030 --> 00:00:47,120
 all your problems and just be

15
00:00:47,120 --> 00:00:48,120
 yourself.

16
00:00:48,120 --> 00:00:52,440
 You'll stop thinking about the past, stop thinking about

17
00:00:52,440 --> 00:00:54,720
 the future, and be happy with

18
00:00:54,720 --> 00:00:59,040
 what you have here and now.

19
00:00:59,040 --> 00:01:05,080
 Second, meditation can teach you many things.

20
00:01:05,080 --> 00:01:08,130
 Some people who practice meditation learn how to leave

21
00:01:08,130 --> 00:01:09,800
 their bodies and fly through

22
00:01:09,800 --> 00:01:11,840
 the air.

23
00:01:11,840 --> 00:01:16,280
 Some are able to read other people's minds, and some can

24
00:01:16,280 --> 00:01:18,720
 see things far, far away that

25
00:01:18,720 --> 00:01:21,760
 no one else can see.

26
00:01:21,760 --> 00:01:26,550
 But most important of all, meditation teaches you about

27
00:01:26,550 --> 00:01:28,940
 yourself and the people around you.

28
00:01:28,940 --> 00:01:32,740
 It teaches you how your mind works so you don't get

29
00:01:32,740 --> 00:01:35,620
 confused or mixed up about anything

30
00:01:35,620 --> 00:01:37,920
 in your life.

31
00:01:37,920 --> 00:01:43,320
 Third, meditation makes your mind sharp and clear so you

32
00:01:43,320 --> 00:01:46,080
 can live your life better.

33
00:01:46,080 --> 00:01:49,680
 When your mind is clear, you're ready for anything.

34
00:01:49,680 --> 00:01:53,180
 You'll know how to talk to your friends, family, and even

35
00:01:53,180 --> 00:01:55,320
 people you meet for the first time

36
00:01:55,320 --> 00:01:58,920
 without feeling shy or embarrassed.

37
00:01:58,920 --> 00:02:02,390
 You'll be able to study and remember things much better and

38
00:02:02,390 --> 00:02:04,400
 work hard helping your family,

39
00:02:04,400 --> 00:02:09,520
 friends, and yourself in whatever work you have to do.

40
00:02:09,520 --> 00:02:14,320
 Finally, meditation makes you a better person.

41
00:02:14,320 --> 00:02:16,900
 When your mind is clear, you won't get angry about

42
00:02:16,900 --> 00:02:18,880
 something you don't like because you'll

43
00:02:18,880 --> 00:02:22,920
 understand that's just the way it is.

44
00:02:22,920 --> 00:02:27,330
 You won't get greedy about things you like because you'll

45
00:02:27,330 --> 00:02:29,680
 be happy with what you have.

46
00:02:29,680 --> 00:02:33,090
 And you won't be confused or attached to anything because

47
00:02:33,090 --> 00:02:34,960
 you'll understand everything about

48
00:02:34,960 --> 00:02:38,560
 yourself and the whole world around you.

49
00:02:38,560 --> 00:02:39,960
 Sound good?

50
00:02:39,960 --> 00:02:42,280
 Okay, let's start.

51
00:02:42,280 --> 00:02:45,860
 First, we need something to focus on.

52
00:02:45,860 --> 00:02:49,530
 It should be something simple, something easy for you to

53
00:02:49,530 --> 00:02:50,360
 think of.

54
00:02:50,360 --> 00:02:54,280
 For now, we'll start with a cat.

55
00:02:54,280 --> 00:02:59,180
 All you have to do is look at the cat and say to yourself,

56
00:02:59,180 --> 00:03:00,160
 "Cat."

57
00:03:00,160 --> 00:03:01,780
 That's it.

58
00:03:01,780 --> 00:03:09,480
 Just say to yourself again and again, "Cat, cat, cat."

59
00:03:09,480 --> 00:03:11,800
 You don't even have to say it out loud.

60
00:03:11,800 --> 00:03:15,120
 Just say it in your mind while you look at the cat.

61
00:03:15,120 --> 00:03:27,960
 Try it now.

62
00:03:27,960 --> 00:03:28,960
 Got it?

63
00:03:28,960 --> 00:03:33,620
 Okay, now close your eyes and think about a cat and say to

64
00:03:33,620 --> 00:03:36,040
 yourself, "Cat," just like

65
00:03:36,040 --> 00:03:37,600
 before.

66
00:03:37,600 --> 00:03:40,520
 Only this time, see the cat in your mind.

67
00:03:40,520 --> 00:03:44,920
 Cat, cat, cat.

68
00:03:44,920 --> 00:03:55,960
 Go ahead, close your eyes and try it now.

69
00:03:55,960 --> 00:03:58,920
 Done?

70
00:03:58,920 --> 00:04:07,520
 Okay, let's try a dog next.

71
00:04:07,520 --> 00:04:14,400
 Look at the dog and say to yourself, "Dog, dog, dog."

72
00:04:14,400 --> 00:04:29,040
 Say it slowly, don't rush.

73
00:04:29,040 --> 00:04:33,360
 Now close your eyes and try to see the dog in your mind.

74
00:04:33,360 --> 00:04:40,960
 Say dog, dog, dog and think of a dog.

75
00:04:40,960 --> 00:04:44,640
 If you can't see it in your mind, just open your eyes and

76
00:04:44,640 --> 00:04:45,640
 look again.

77
00:04:45,640 --> 00:04:52,120
 Peeking is allowed.

78
00:04:52,120 --> 00:05:03,560
 Okay, now let's try our family members.

79
00:05:03,560 --> 00:05:07,450
 Think of your mother or someone you love very much like

80
00:05:07,450 --> 00:05:09,000
 your mother.

81
00:05:09,000 --> 00:05:12,750
 If they are there with you, you can look at them and say to

82
00:05:12,750 --> 00:05:14,880
 yourself, "Mother," or whatever

83
00:05:14,880 --> 00:05:16,880
 name you call them.

84
00:05:16,880 --> 00:05:36,520
 Mother, mother, mother, again and again.

85
00:05:36,520 --> 00:05:41,610
 Once you can do it without looking, close your eyes, think

86
00:05:41,610 --> 00:05:44,040
 of them and say, "Mother,

87
00:05:44,040 --> 00:05:49,190
 mother, mother," or whatever name you call them, just

88
00:05:49,190 --> 00:05:52,240
 repeat that, thinking of them at

89
00:05:52,240 --> 00:06:00,600
 the same time.

90
00:06:00,600 --> 00:06:09,700
 Next, think of your father or someone you love like a

91
00:06:09,700 --> 00:06:11,640
 father.

92
00:06:11,640 --> 00:06:15,640
 Same thing, if they are there or if you have a picture of

93
00:06:15,640 --> 00:06:18,560
 them, just look and say, "Father,"

94
00:06:18,560 --> 00:06:22,980
 or "Dad," or whatever name you have for them until you can

95
00:06:22,980 --> 00:06:25,000
 do it without looking.

96
00:06:25,000 --> 00:06:43,440
 Father, father, father.

97
00:06:43,440 --> 00:06:56,960
 Then close your eyes and do the same thing.

98
00:06:56,960 --> 00:07:09,040
 Okay, now let's try something a bit different.

99
00:07:09,040 --> 00:07:12,600
 Now let's focus on ourselves.

100
00:07:12,600 --> 00:07:15,140
 To start, you can look at yourself.

101
00:07:15,140 --> 00:07:25,960
 Look at your legs, your arms, your body and say to yourself

102
00:07:25,960 --> 00:07:30,200
, "Me, me, me."

103
00:07:30,200 --> 00:07:45,600
 Just repeat it to yourself while looking at your body.

104
00:07:45,600 --> 00:07:50,520
 Then close your eyes and do the same thing.

105
00:07:50,520 --> 00:08:08,170
 Think of how you are sitting and say to yourself, "Me, me,

106
00:08:08,170 --> 00:08:10,360
 me."

107
00:08:10,360 --> 00:08:16,950
 Once you get the hang of it, you can just think to yourself

108
00:08:16,950 --> 00:08:20,880
 sitting, sitting, sitting, sitting,

109
00:08:20,880 --> 00:08:29,640
 thinking about yourself sitting still.

110
00:08:29,640 --> 00:08:36,190
 If you want, you can try it lying down and say to yourself,

111
00:08:36,190 --> 00:08:39,560
 "Lying, lying, lying," or

112
00:08:39,560 --> 00:08:54,570
 if you want, you can walk back and forth saying, "Walking,

113
00:08:54,570 --> 00:09:00,560
 walking, walking."

114
00:09:00,560 --> 00:09:07,080
 All of these are ways to practice meditation.

115
00:09:07,080 --> 00:09:10,760
 Another good object of meditation is the breath.

116
00:09:10,760 --> 00:09:12,160
 Try this.

117
00:09:12,160 --> 00:09:19,110
 Sit comfortably or even lie down and put your hands on your

118
00:09:19,110 --> 00:09:20,640
 stomach.

119
00:09:20,640 --> 00:09:27,880
 You should feel your stomach rising and falling when your

120
00:09:27,880 --> 00:09:31,880
 breath comes in and goes out.

121
00:09:31,880 --> 00:09:38,120
 When your stomach rises, just say to yourself, "Rising,"

122
00:09:38,120 --> 00:09:42,040
 and when it falls, say, "Falling,

123
00:09:42,040 --> 00:09:54,640
 rising, falling, rising, falling."

124
00:09:54,640 --> 00:09:59,960
 Again, you don't have to say it out loud.

125
00:09:59,960 --> 00:10:07,190
 Just put your mind on your stomach and think to yourself, "

126
00:10:07,190 --> 00:10:22,600
Rising, falling," as it happens.

127
00:10:22,600 --> 00:10:39,400
 The truth is you can meditate on just about anything.

128
00:10:39,400 --> 00:10:44,320
 You won't get upset or stressed out about bad feelings.

129
00:10:44,320 --> 00:10:48,820
 If you're thinking about something, you can just say, "Th

130
00:10:48,820 --> 00:10:51,040
inking, thinking," and you won't

131
00:10:51,040 --> 00:10:55,000
 be afraid of bad thoughts anymore.

132
00:10:55,000 --> 00:10:59,780
 If you want something, you can say, "Wanting, wanting," and

133
00:10:59,780 --> 00:11:01,760
 you won't be sad if you don't

134
00:11:01,760 --> 00:11:03,480
 get it.

135
00:11:03,480 --> 00:11:08,110
 If you feel angry, you can just say, "Angry, angry," and

136
00:11:08,110 --> 00:11:11,440
 you won't need to yell or scream

137
00:11:11,440 --> 00:11:16,200
 or hurt yourself or other people.

138
00:11:16,200 --> 00:11:19,630
 If you can't find anything to meditate on, just go back and

139
00:11:19,630 --> 00:11:21,200
 look at the stomach rising

140
00:11:21,200 --> 00:11:29,360
 and falling and say to yourself, "Rising, falling," like

141
00:11:29,360 --> 00:11:30,840
 before.

142
00:11:30,840 --> 00:11:35,640
 Or pick something like a cat or a dog or someone you love

143
00:11:35,640 --> 00:11:38,320
 and focus on them instead.

144
00:11:38,320 --> 00:11:54,200
 Go ahead, pick one object, and let's practice together.

145
00:11:54,200 --> 00:12:16,330
 By now, you should feel like your mind is more focused than

146
00:12:16,330 --> 00:12:20,400
 before.

147
00:12:20,400 --> 00:12:23,280
 In the beginning, it might be difficult because you're

148
00:12:23,280 --> 00:12:24,760
 learning something new.

149
00:12:24,760 --> 00:12:27,920
 But if you practice it again and again, you will find your

150
00:12:27,920 --> 00:12:29,720
 mind calming down and clearing

151
00:12:29,720 --> 00:12:35,390
 up, and you will begin to feel peaceful, happy, and free

152
00:12:35,390 --> 00:12:39,400
 from suffering more than ever before.

153
00:12:39,400 --> 00:12:42,500
 This is how to practice meditation.

154
00:12:42,500 --> 00:12:45,210
 If you want to try it by yourself, check out the next video

155
00:12:45,210 --> 00:12:46,560
 where you can practice all

156
00:12:46,560 --> 00:12:49,920
 of these techniques all by yourself.

157
00:12:49,920 --> 00:12:50,420
 Good luck!

