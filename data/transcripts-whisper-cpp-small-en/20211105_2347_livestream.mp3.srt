1
00:00:00,000 --> 00:00:12,820
 The two things that we need before we begin to practice Sat

2
00:00:12,820 --> 00:00:22,320
ipatthana are see the ethics

3
00:00:22,320 --> 00:00:35,000
 or morality and samadhi right to you.

4
00:00:35,000 --> 00:00:36,000
 That's it.

5
00:00:36,000 --> 00:00:40,520
 That's how you prepare yourself to practice Satipatthana.

6
00:00:40,520 --> 00:00:50,120
 How you practice cultivating a mind that is set on the

7
00:00:50,120 --> 00:00:55,360
 remembrance of things as they are

8
00:00:55,360 --> 00:01:07,360
 Satipatthana.

9
00:01:07,360 --> 00:01:11,800
 So it may seem quite simple, basic.

10
00:01:11,800 --> 00:01:20,120
 See that seems quite simple, samadhi tea.

11
00:01:20,120 --> 00:01:26,170
 But there's more to these things than simple word

12
00:01:26,170 --> 00:01:28,280
 definitions.

13
00:01:28,280 --> 00:01:33,060
 For example, see that is often associated with precepts.

14
00:01:33,060 --> 00:01:39,080
 And yes, precepts are a part of what is needed.

15
00:01:39,080 --> 00:01:45,360
 But in total, there are four parts to the practice of Sila.

16
00:01:45,360 --> 00:01:53,440
 And all four of them are important.

17
00:01:53,440 --> 00:01:58,700
 Sila is the base, the foundation of one's spiritual

18
00:01:58,700 --> 00:02:00,440
 development.

19
00:02:00,440 --> 00:02:08,600
 It creates stability, focus, strength.

20
00:02:08,600 --> 00:02:17,160
 Without it, you could say we're on shaky ground, even with

21
00:02:17,160 --> 00:02:19,320
 right view.

22
00:02:19,320 --> 00:02:28,040
 First of all, with regards to precepts, things, actions

23
00:02:28,040 --> 00:02:33,880
 that we perform, actions, speech.

24
00:02:33,880 --> 00:02:37,980
 This is an extreme form of disruption of practice because

25
00:02:37,980 --> 00:02:40,520
 it involves the world around us.

26
00:02:40,520 --> 00:02:48,030
 That creates instability in our lives, not to mention the

27
00:02:48,030 --> 00:02:50,640
 trauma that it brings and the

28
00:02:50,640 --> 00:02:53,880
 memories of the bad things that we've done.

29
00:02:53,880 --> 00:02:59,250
 That certainly disturbs our practice, prevents us from

30
00:02:59,250 --> 00:03:01,680
 progressing in the practice.

31
00:03:01,680 --> 00:03:07,550
 So things like killing and stealing, lying and cheating,

32
00:03:07,550 --> 00:03:10,520
 taking drugs and alcohol.

33
00:03:10,520 --> 00:03:17,290
 It's very hard to, it's extremely hard, maybe even

34
00:03:17,290 --> 00:03:22,160
 impossible to progress in seeing clearly

35
00:03:22,160 --> 00:03:28,050
 when your mind is so corrupted by the states of mind

36
00:03:28,050 --> 00:03:31,960
 involved with these actions.

37
00:03:31,960 --> 00:03:39,450
 There's such a corruption associated with the external

38
00:03:39,450 --> 00:03:45,920
 manifestations of corruption,

39
00:03:45,920 --> 00:03:52,600
 being angry, greedy, deluded, there's a disruption of your

40
00:03:52,600 --> 00:03:54,080
 practice.

41
00:03:54,080 --> 00:03:58,810
 But when it takes on an external manifestation, it is

42
00:03:58,810 --> 00:03:59,920
 extreme.

43
00:03:59,920 --> 00:04:11,560
 There is a categorical difference, a categorical shift.

44
00:04:11,560 --> 00:04:20,000
 It's no longer a state of mind, it's a reality, a physical

45
00:04:20,000 --> 00:04:27,000
 reality that has, of course, worldly

46
00:04:27,000 --> 00:04:31,540
 repercussions, but also leaves a more profound mark on the

47
00:04:31,540 --> 00:04:32,240
 mind.

48
00:04:32,240 --> 00:04:35,710
 So keeping precepts is important, and if you can, of course

49
00:04:35,710 --> 00:04:37,480
, before doing a meditation

50
00:04:37,480 --> 00:04:42,440
 course it's important that you take on the eight precepts.

51
00:04:42,440 --> 00:04:46,850
 But at the very least we should know as meditators about

52
00:04:46,850 --> 00:04:49,600
 the other three and try our best to

53
00:04:49,600 --> 00:04:54,380
 put them into practice whenever we can, cutting down on

54
00:04:54,380 --> 00:04:57,440
 food, trying to eat just what we need

55
00:04:57,440 --> 00:05:02,040
 to survive.

56
00:05:02,040 --> 00:05:08,120
 That's a day is enough.

57
00:05:08,120 --> 00:05:22,720
 Try to give up entertainment, beautification, and try to

58
00:05:22,720 --> 00:05:25,320
 reduce our sleep and our need for

59
00:05:25,320 --> 00:05:35,420
 the comfort of sleep, so sleeping on a simple bed or

60
00:05:35,420 --> 00:05:38,080
 mattress.

61
00:05:38,080 --> 00:05:44,140
 So this is useful, this is Sila, but there's more to beyond

62
00:05:44,140 --> 00:05:45,080
 that.

63
00:05:45,080 --> 00:05:55,370
 Sila isn't just about specific acts or breaches of conduct

64
00:05:55,370 --> 00:05:57,720
 or speech.

65
00:05:57,720 --> 00:06:02,320
 Sila involves our behavior.

66
00:06:02,320 --> 00:06:13,480
 It's much more complex than simply keeping some rules, some

67
00:06:13,480 --> 00:06:15,440
 important rules.

68
00:06:15,440 --> 00:06:22,440
 For example, the second aspect of Sila is reflecting on the

69
00:06:22,440 --> 00:06:25,480
 use of requisites, the things

70
00:06:25,480 --> 00:06:32,080
 that we use.

71
00:06:32,080 --> 00:06:35,910
 Most people, I think, have things that they use well beyond

72
00:06:35,910 --> 00:06:40,600
 what could be considered requisites,

73
00:06:40,600 --> 00:06:51,200
 but even those things fall under this category.

74
00:06:51,200 --> 00:06:56,600
 Absolutely, if we can find a way to reduce our need for

75
00:06:56,600 --> 00:07:00,040
 possessions to simply that which

76
00:07:00,040 --> 00:07:05,880
 is required to survive without letting them become a

77
00:07:05,880 --> 00:07:10,880
 distraction, a diversion, an obsession,

78
00:07:10,880 --> 00:07:14,960
 an addiction, and we would absolutely benefit from that and

79
00:07:14,960 --> 00:07:17,040
 our meditation would benefit

80
00:07:17,040 --> 00:07:21,800
 greatly from that.

81
00:07:21,800 --> 00:07:27,470
 But first and foremost, whatever it is that we do use,

82
00:07:27,470 --> 00:07:31,040
 regardless of cutting back or not

83
00:07:31,040 --> 00:07:36,450
 cutting back, we change the way we perceive, we begin to

84
00:07:36,450 --> 00:07:38,960
 look at the things that we use

85
00:07:38,960 --> 00:07:42,480
 in a new way.

86
00:07:42,480 --> 00:07:50,880
 So taking the very basic requisites as examples, cloth to

87
00:07:50,880 --> 00:07:55,600
 wear, we take on as a practice to

88
00:07:55,600 --> 00:08:02,050
 wear it only for the purpose it was meant, cover our nudity

89
00:08:02,050 --> 00:08:04,600
 and protect us from cold

90
00:08:04,600 --> 00:08:09,760
 and heat.

91
00:08:09,760 --> 00:08:13,760
 Being naked is not preferable, not in most places.

92
00:08:13,760 --> 00:08:17,480
 Meaning, in most places it's highly uncomfortable to go

93
00:08:17,480 --> 00:08:20,400
 naked, of course, so there's no question

94
00:08:20,400 --> 00:08:23,120
 that we need to wear clothing.

95
00:08:23,120 --> 00:08:30,130
 But our clothing becomes a crutch, an addiction, an

96
00:08:30,130 --> 00:08:35,520
 attachment, worrying about how we look,

97
00:08:35,520 --> 00:08:51,400
 enjoying the specialness of our clothes, brand names,

98
00:08:51,400 --> 00:08:57,320
 beautiful colors and so on.

99
00:08:57,320 --> 00:09:02,480
 It's an example of something that can become a source of

100
00:09:02,480 --> 00:09:05,000
 distraction and diversion and

101
00:09:05,000 --> 00:09:10,080
 attachment that takes us away from the present moment.

102
00:09:10,080 --> 00:09:14,120
 It keeps us from being at peace here and now because we're

103
00:09:14,120 --> 00:09:15,760
 thinking about it.

104
00:09:15,760 --> 00:09:21,440
 Food, food is probably the biggest out of all of them.

105
00:09:21,440 --> 00:09:26,320
 Food we use for entertainment, for diversion.

106
00:09:26,320 --> 00:09:28,840
 We eat when we're bored.

107
00:09:28,840 --> 00:09:31,040
 We eat when we're depressed.

108
00:09:31,040 --> 00:09:37,200
 We eat when we're anxious, afraid.

109
00:09:37,200 --> 00:09:40,480
 Food becomes a real crutch.

110
00:09:40,480 --> 00:09:45,360
 We eat just out of addiction.

111
00:09:45,360 --> 00:09:46,360
 Food is medicine.

112
00:09:46,360 --> 00:09:50,650
 Food is something that keeps us alive, that keeps us

113
00:09:50,650 --> 00:09:51,720
 healthy.

114
00:09:51,720 --> 00:09:55,760
 It's a very good thing, an important thing for the body.

115
00:09:55,760 --> 00:09:57,840
 We don't use it like that.

116
00:09:57,840 --> 00:10:07,880
 We don't normally eat simply because of a need to survive.

117
00:10:07,880 --> 00:10:10,120
 Even regarding our health we become obsessed with it,

118
00:10:10,120 --> 00:10:11,480
 worrying about what we're eating

119
00:10:11,480 --> 00:10:18,520
 and obsessing over the ingredients and so on.

120
00:10:18,520 --> 00:10:22,900
 Not that it's not good to be healthy, but it's not healthy

121
00:10:22,900 --> 00:10:25,360
 to be obsessed with anything.

122
00:10:25,360 --> 00:10:29,770
 Much more common of course is the eating of unhealthy foods

123
00:10:29,770 --> 00:10:30,080
.

124
00:10:30,080 --> 00:10:34,710
 We can see how horrible addiction can be and how unhealthy

125
00:10:34,710 --> 00:10:37,160
 it makes us, how anyone could

126
00:10:37,160 --> 00:10:43,560
 get diseases like diabetes or obesity and so on.

127
00:10:43,560 --> 00:10:51,800
 It really is kind of incredible that these diseases, these

128
00:10:51,800 --> 00:10:55,320
 sicknesses can arise.

129
00:10:55,320 --> 00:11:01,170
 There's no accidental, it's not common for it to be an

130
00:11:01,170 --> 00:11:03,880
 accidental getting of obesity

131
00:11:03,880 --> 00:11:09,800
 or diabetes or heart disease or liver disease or so on.

132
00:11:09,800 --> 00:11:14,510
 There are many kinds of cancer that can come from eating

133
00:11:14,510 --> 00:11:16,240
 the wrong foods.

134
00:11:16,240 --> 00:11:19,660
 Because if we ate simple foods just for what was healthy,

135
00:11:19,660 --> 00:11:24,320
 if society was based around that,

136
00:11:24,320 --> 00:11:28,240
 society isn't based around that.

137
00:11:28,240 --> 00:11:30,920
 Our food makes us sick.

138
00:11:30,920 --> 00:11:38,720
 This thing that was supposed to be a medicine makes us sick

139
00:11:38,720 --> 00:11:38,960
.

140
00:11:38,960 --> 00:11:43,110
 That's only remarkable from a Buddhist perspective again

141
00:11:43,110 --> 00:11:45,640
 because of how this is a hindrance to

142
00:11:45,640 --> 00:11:46,640
 our meditation.

143
00:11:46,640 --> 00:11:49,440
 This is important not because of how it makes you

144
00:11:49,440 --> 00:11:52,040
 physically sick, but because of the mental

145
00:11:52,040 --> 00:11:54,160
 sickness it involves.

146
00:11:54,160 --> 00:11:58,960
 The addiction, the distraction, the avoidance.

147
00:11:58,960 --> 00:12:07,710
 All of the mental conditions I mentioned that we use food

148
00:12:07,710 --> 00:12:13,200
 to avoid are the important reasons

149
00:12:13,200 --> 00:12:16,840
 why we practice mindfulness in the first place.

150
00:12:16,840 --> 00:12:21,040
 To understand and to work through these states.

151
00:12:21,040 --> 00:12:24,520
 You'll never get that if you're always avoiding them.

152
00:12:24,520 --> 00:12:32,440
 Food can be a real crutch in that sense.

153
00:12:32,440 --> 00:12:44,060
 It's a hindrance in that sense, keeping us from facing our

154
00:12:44,060 --> 00:12:46,280
 issues.

155
00:12:46,280 --> 00:12:48,720
 We really don't need a lot of food to survive.

156
00:12:48,720 --> 00:12:53,420
 If we learn to eat just food, just to survive, we'd be so

157
00:12:53,420 --> 00:12:54,840
 much happier.

158
00:12:54,840 --> 00:12:57,200
 People think it's hard to eat only one meal a day.

159
00:12:57,200 --> 00:12:58,880
 It's not really that hard.

160
00:12:58,880 --> 00:13:02,790
 It could be hard of course for those many people who have

161
00:13:02,790 --> 00:13:04,560
 to do physical labor.

162
00:13:04,560 --> 00:13:09,080
 But it's not exactly about the amount.

163
00:13:09,080 --> 00:13:12,390
 So if you have to eat more times a day, it's about eating

164
00:13:12,390 --> 00:13:14,320
 for the purpose for which food

165
00:13:14,320 --> 00:13:24,720
 is made, exists in the first place.

166
00:13:24,720 --> 00:13:31,340
 To keep us alive, to keep us continuing and give us fuel to

167
00:13:31,340 --> 00:13:35,120
 perform the actions, the speech,

168
00:13:35,120 --> 00:13:37,680
 the deeds that we need to do.

169
00:13:37,680 --> 00:13:54,440
 People need to keep us practicing meditation.

170
00:13:54,440 --> 00:14:04,440
 The third thing is, the third requisite is shelter or

171
00:14:04,440 --> 00:14:06,960
 dwelling.

172
00:14:06,960 --> 00:14:09,800
 There's really two parts.

173
00:14:09,800 --> 00:14:22,480
 There's the environment in which we live, the dwelling in

174
00:14:22,480 --> 00:14:25,720
 which we reside, and the bedding

175
00:14:25,720 --> 00:14:34,680
 on which we sit and lie, the furniture.

176
00:14:34,680 --> 00:14:37,560
 Both are an important part of the requisite.

177
00:14:37,560 --> 00:14:39,440
 You need a place to live.

178
00:14:39,440 --> 00:14:43,760
 You need a place to stay.

179
00:14:43,760 --> 00:14:49,080
 And you need a place to sit and to lie down.

180
00:14:49,080 --> 00:14:51,560
 But these become quite extravagant.

181
00:14:51,560 --> 00:14:55,870
 Our dwellings become a place for us to perform those deeds

182
00:14:55,870 --> 00:14:58,320
 that we wouldn't dare perform in

183
00:14:58,320 --> 00:15:00,920
 public.

184
00:15:00,920 --> 00:15:05,330
 Social activity is done in private, mostly entertainment,

185
00:15:05,330 --> 00:15:07,840
 the consumption of entertainment

186
00:15:07,840 --> 00:15:13,760
 of all sorts.

187
00:15:13,760 --> 00:15:20,520
 We take the privacy as a means of performing unwholesome

188
00:15:20,520 --> 00:15:24,200
 deeds instead of what it could

189
00:15:24,200 --> 00:15:28,440
 actually be used for to perform great wholesome deeds.

190
00:15:28,440 --> 00:15:32,320
 Solitude works both ways.

191
00:15:32,320 --> 00:15:35,910
 When you're alone, you feel confident in doing unwholesome

192
00:15:35,910 --> 00:15:38,000
 deeds, but when you're alone,

193
00:15:38,000 --> 00:15:42,120
 you have such great power to do wholesome deeds as well.

194
00:15:42,120 --> 00:15:46,420
 We think goodness is something you perform in society, do

195
00:15:46,420 --> 00:15:48,480
 good things in society.

196
00:15:48,480 --> 00:15:54,560
 We don't realize the greatest good for the world comes from

197
00:15:54,560 --> 00:15:57,240
 the cultivation of purity

198
00:15:57,240 --> 00:16:05,240
 within, which is best performed alone in solitude.

199
00:16:05,240 --> 00:16:08,030
 So using our dwelling for the right purpose and having a

200
00:16:08,030 --> 00:16:10,400
 proper dwelling, that's not extravagant.

201
00:16:10,400 --> 00:16:18,800
 It's not a distraction, a diversion from the present moment

202
00:16:18,800 --> 00:16:18,880
.

203
00:16:18,880 --> 00:16:19,880
 And the same with bedding.

204
00:16:19,880 --> 00:16:22,040
 Do not have extravagant bedding.

205
00:16:22,040 --> 00:16:25,840
 Again, that's going to distract you from the practice.

206
00:16:25,840 --> 00:16:31,960
 The fourth requisite is medicine.

207
00:16:31,960 --> 00:16:35,200
 Medicine is something that we do abuse.

208
00:16:35,200 --> 00:16:45,700
 In modern times, I think an unfortunate abuse is the rising

209
00:16:45,700 --> 00:16:48,840
 need for psychoactive drugs.

210
00:16:48,840 --> 00:16:52,780
 So medicine not just for the body, but not just for the

211
00:16:52,780 --> 00:16:55,040
 other organs or limbs or parts

212
00:16:55,040 --> 00:16:59,850
 of the body, but for the brain, not for the mind, but for

213
00:16:59,850 --> 00:17:01,080
 the brain.

214
00:17:01,080 --> 00:17:07,540
 In an attempt at avoiding the issues in the mind, rep

215
00:17:07,540 --> 00:17:12,360
ressing issues in the mind, you could

216
00:17:12,360 --> 00:17:15,240
 say, because it can't fix them.

217
00:17:15,240 --> 00:17:17,780
 There is nothing that can fix the problems in our mind

218
00:17:17,780 --> 00:17:19,120
 except for mindfulness.

219
00:17:19,120 --> 00:17:23,700
 It really isn't because mindfulness involves learning and

220
00:17:23,700 --> 00:17:25,240
 understanding.

221
00:17:25,240 --> 00:17:29,240
 It involves the ability to separate our thoughts from our

222
00:17:29,240 --> 00:17:32,120
 emotions, our physical experience,

223
00:17:32,120 --> 00:17:36,600
 from our mental reactions to them.

224
00:17:36,600 --> 00:17:38,400
 Medicine can't give us that.

225
00:17:38,400 --> 00:17:43,120
 We abuse many kinds of medicine.

226
00:17:43,120 --> 00:17:49,720
 Pain medication can be a real detriment to our practice.

227
00:17:49,720 --> 00:17:52,220
 It's not to say that you shouldn't ever take any of these

228
00:17:52,220 --> 00:17:52,760
 things.

229
00:17:52,760 --> 00:17:56,210
 It's to be clear what they're for and to use them for the

230
00:17:56,210 --> 00:17:57,480
 right purpose.

231
00:17:57,480 --> 00:18:00,060
 If you're in great pain, taking a little bit of pain

232
00:18:00,060 --> 00:18:01,880
 medication can be very helpful, but

233
00:18:01,880 --> 00:18:07,390
 trying to be free from pain all the time is a cause for

234
00:18:07,390 --> 00:18:10,200
 incredible aversion to pain, which

235
00:18:10,200 --> 00:18:19,200
 is a big problem.

236
00:18:19,200 --> 00:18:25,200
 These are an example of a part of ethics that's required or

237
00:18:25,200 --> 00:18:28,040
 important to consider.

238
00:18:28,040 --> 00:18:31,310
 Not just these four requisites, but again, anything we do

239
00:18:31,310 --> 00:18:31,720
 use.

240
00:18:31,720 --> 00:18:35,650
 Vehicles, computers, telephones, many of the modern things

241
00:18:35,650 --> 00:18:37,280
 that we have and use and see

242
00:18:37,280 --> 00:18:40,360
 as necessary.

243
00:18:40,360 --> 00:18:43,140
 It can be used for great good, but they can of course

244
00:18:43,140 --> 00:18:45,200
 become great distractions, great

245
00:18:45,200 --> 00:18:49,400
 problems for us in our practice.

246
00:18:49,400 --> 00:18:53,920
 This consideration beforehand, not exactly talking about

247
00:18:53,920 --> 00:18:57,040
 being mindful of them, but considering

248
00:18:57,040 --> 00:18:59,440
 carefully, why do we use these?

249
00:18:59,440 --> 00:19:00,440
 Why do I use this?

250
00:19:00,440 --> 00:19:02,560
 Is it for a good purpose?

251
00:19:02,560 --> 00:19:03,560
 How am I using it?

252
00:19:03,560 --> 00:19:05,560
 Am I using it in the right way?

253
00:19:05,560 --> 00:19:10,870
 Or is it becoming a distraction, a diversion, a hindrance

254
00:19:10,870 --> 00:19:12,560
 to my practice?

255
00:19:12,560 --> 00:19:16,720
 The third type of seal I regards livelihood.

256
00:19:16,720 --> 00:19:19,670
 Livelihood is maybe a bit of a surprise, but of course,

257
00:19:19,670 --> 00:19:21,440
 livelihood is a huge part of our

258
00:19:21,440 --> 00:19:22,480
 lives.

259
00:19:22,480 --> 00:19:27,730
 Even as a monk, even a monk, we have a sense of livelihood,

260
00:19:27,730 --> 00:19:30,560
 even at least insofar as having

261
00:19:30,560 --> 00:19:34,880
 to find a way to survive.

262
00:19:34,880 --> 00:19:39,580
 The Buddha gave us the allowance of going on alms round,

263
00:19:39,580 --> 00:19:42,120
 seeing that there were people

264
00:19:42,120 --> 00:19:46,690
 in the time of the Buddha and really in all times, people

265
00:19:46,690 --> 00:19:51,920
 who appreciate religious folk.

266
00:19:51,920 --> 00:19:56,320
 You see it in mega churches and religions with large follow

267
00:19:56,320 --> 00:19:58,400
ings, giving large sums of

268
00:19:58,400 --> 00:20:01,640
 money to their religious folk.

269
00:20:01,640 --> 00:20:06,640
 In Buddhism, the restriction is simply food, or the other

270
00:20:06,640 --> 00:20:09,280
 requisites, robes and so on,

271
00:20:09,280 --> 00:20:13,120
 but mainly food.

272
00:20:13,120 --> 00:20:17,040
 A monk is allowed to go on alms to receive what?

273
00:20:17,040 --> 00:20:18,960
 A meal.

274
00:20:18,960 --> 00:20:22,830
 Food from people who have food and who desire to give food

275
00:20:22,830 --> 00:20:25,120
 to religious people, desire to

276
00:20:25,120 --> 00:20:27,080
 give something.

277
00:20:27,080 --> 00:20:34,410
 They take only what is the smallest thing, and that's a bit

278
00:20:34,410 --> 00:20:37,320
 of rice and beans or food

279
00:20:37,320 --> 00:20:38,320
 of some sort.

280
00:20:38,320 --> 00:20:44,550
 There's a means of staying alive, and it's such a joyous

281
00:20:44,550 --> 00:20:48,480
 thing because it's not incredibly

282
00:20:48,480 --> 00:20:58,520
 burdensome upon the people giving.

283
00:20:58,520 --> 00:21:27,520
 For

284
00:21:27,520 --> 00:21:48,520
 a

285
00:21:48,520 --> 00:22:16,520
 living

286
00:22:16,520 --> 00:22:44,520
 is

287
00:22:44,520 --> 00:23:12,520
 a

288
00:23:12,520 --> 00:23:33,520
 wholesome

289
00:23:33,520 --> 00:23:51,520
 is

290
00:23:51,520 --> 00:24:19,520
 a

291
00:24:19,520 --> 00:24:47,520
 is

292
00:24:47,520 --> 00:25:15,520
 a

293
00:25:15,520 --> 00:25:43,520
 is

294
00:25:43,520 --> 00:26:11,520
 a

295
00:26:11,520 --> 00:26:39,520
 is

296
00:26:39,520 --> 00:27:07,520
 is

297
00:27:07,520 --> 00:27:35,520
 is

