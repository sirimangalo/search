 Okay, good evening everyone. Welcome to our daily Dhamma.
 Today's topic is, what is it like to be enlightened?
 It's a good question, isn't it? It is what we're striving
 for, ultimately, right?
 So why is it good? Why is it a good question? Why is it
 good to know?
 I think obviously it's something we all are curious about
 and want to know.
 First of all, so that we know whether it's something that
 might be worth attaining ourselves.
 Also to know how far we are from it.
 But another, I think, interesting point is
 it allows us to direct ourselves in the right direction.
 Even though we're not enlightened ourselves,
 we can
 mimic an enlightened being.
 Mimic is not the right word. We can emulate.
 It's the word.
 Emulate is like when we keep the five precepts. When we don
't kill, well, enlightened beings don't kill naturally.
 For the unenlightened, it's something you have to work at.
 So you emulate enlightened behavior.
 Some cynic might say, "Well, you fake." But you know the
 saying, "Fake it till you make it." There's something
 interesting there because
 the Tibetans believe that by pretending to be a Buddha or
 not pretending by
 really believing that you are a Buddha, you actually become
 closer to being a Buddha.
 And well, I don't think that that's, I don't agree that
 that's adequate practice.
 I don't know that the Tibetans really do either, though I
 don't know too much about their tradition.
 But
 there's something to it. I mean, it gives you a roadmap.
 It gives you some
 markers to live by.
 To know when you're acting like an enlightened being and to
 know when you're not, so to know what you have to change.
 So this is the question, "What is it like to be enlightened
?"
 There's a story that I think is,
 I think is interesting. I've got two lists that I can think
 of where the Buddha gave a list of
 characteristics of what it might be like to be enlightened.
 The first one is I think my favorite.
 Sariputta, the Buddha asks Sariputta. Sariputta says
 something
 about the Buddha and the Buddha says, "Oh, so you have
 faith in me that I'm enlightened?"
 And Sariputta said, "No, I don't have faith in you that you
're enlightened."
 And the Buddha went away.
 Sariputta was the Buddha's chief disciple, right? I mean
 chief disciple saying, "Hey, I don't have faith in you."
 And so the monks were all
 disturbed by this, the unenlightened monks were disturbed
 by it because they misunderstood what he meant. Of course,
 he didn't mean
 that Sariputta didn't mean he didn't think the Buddha was
 enlightened, but he knew that the Buddha was enlightened.
 It wasn't out of faith, right?
 So they started talking about this, "Oh, Sariputta has no
 faith in the Buddha."
 And the Buddha hears them talking and he says, "What are
 you guys talking about?"
 They say,
 they tell him and the Buddha said, "Yes, that's right. He's
 faithless."
 And he said, and he gave this verse, he said,
 "Asadho akattanyu jasandhityedho jayonarou."
 And these words you really have to know Pali to really get
 the joke.
 I mean, it's probably the closest, one of the closest
 examples the Buddha comes to humor.
 Asadho means faithless.
 Akattanyu means, it's a word that means ungrateful.
 A means not.
 And then kattaa means what is done and anyu means one who
 knows.
 So it means one who doesn't know, means in the sense of
 doesn't keep in mind the things that were done for them.
 Someone does something for you, you just
 forget about it, not interested.
 Not grateful is what it means. Akattanyu.
 Sanditjedo means one who breaks chains, breaks locks.
 A lockpick.
 So when you have, you know, they would lock up, lock their
 gates in India.
 I had to keep the robbers out and so a sanditjedo was a
 robber.
 Someone who picks locks, a housebreaker kind of thing.
 Sandi means a chain and
 sanditjedo means one who breaks and who cuts with whatever
 they had to cut the chain.
 Hattawakaso, someone who has destroyed all opportunity.
 Someone who has destroyed all opportunity. It's for someone
 who is
 without any, you know, without any future. Like this is
 what you'd say about a bum, someone who had dropped out of
 school and
 had no interest and maybe just did drugs all day or
 something, drank alcohol all day.
 So someone with no opportunity.
 Hattawakaso.
 Hantaso means someone who is hopeless.
 Means someone who you have no hope in. You look at them and
 you say
 and you cannot hope good for that person. There is no hope
 for them. All hope is lost.
 Then the Buddha says,
 Hattawakaso, one to someone then he says,
 Sathway, utama puriso.
 Utama puriso.
 This is the height of humanity, he says.
 He's talking about sareeputa.
 Yeah.
 And you have to know the Pali to know how this all makes
 sense. The first one's easy to understand that's from the
 story.
 Asadho means
 someone who doesn't have to believe anyone, not in regards
 to what's important.
 They have no faith in the Buddha because they know
 it's knowledge, it's no longer belief.
 Asadho.
 All these, I got an argument with a Buddhist, when Sri Lank
an Buddhist went, he said, oh, faith is so important. I said
, oh, yeah.
 Listen to the Buddha, he said, faithless is the best.
 This kind of joking, faith is important, faith is useful.
 And as a quality of mind, it's the same thing.
 In fact, the faith is much stronger in the mind when you
 actually know it. So it still is faith, but
 in a conventional sense, we wouldn't call it faith because
 you actually know,
 if you know something to be true, you don't have to believe
 it.
 But technically, you still do believe it.
 Akatanyu is an interesting compound because it actually can
 mean two different things.
 So, Akatanyu, katanyu means one who knows what is done, Ak
atanyu, one who doesn't know.
 But you can also split it, Akatat and Anyu.
 One who knows what is Akata.
 And Akata is that which is not made, that which is not
 produced.
 And there are only two things that are not, there's only
 one thing, I guess, that's not made,
 not produced, and that is Nibbana.
 Nibbana is called the Akatadhamma. It's not made, it's not
 produced, it's not caused.
 So Sariputta is someone who knows Nibbana is what it means.
 Sandhicheda is fairly easy because Sandhi is the chain,
 and it's the chain of samsara, the chain of causation, you
 know,
 because of ignorance there is karma, because of karma there
 is vinyana, birth.
 If you cut that chain, if you cut out the ignorance,
 then there is no karma, because there is no karma, there is
 no rebirth.
 Because there's no rebirth, then there is no suffering and
 so on.
 There's no craving, because there's no craving, there's no
 clinging,
 no clinging means no becoming and so on. You cut this chain
.
 [Hantavakaso]
 Hantavakaso means someone who has no opportunity.
 Opportunity here means opportunity for more arising,
 especially of defilements.
 In this person there's no opportunity for more karma, for
 more becoming.
 Everything they do is just functional, it's just, it's what
 we think we are.
 I mean it sounds like a zombie, but it's not, it's what we
 think we are.
 We think when I eat, I'm just eating, right?
 And then when you come to meditate, you realize it's not
 actually the case.
 When we eat, we're lost, we're often not even anywhere near
 the food,
 our minds are off doing something else, or if they're near
 the food, they're obsessing over it.
 It's good, it's bad, it's too sweet, too salty, too plain,
 too hot, too cold,
 or it's just right, it's perfect, it's delicious.
 An enlightened being is, there's no opportunity for any of
 that.
 There's no opportunity for defilements to get in, no
 opportunity for Mara, for evil.
 You can't hurt such a person, you can't trigger them, you
 can't instigate them.
 One hantavaka-so.
 One taso, someone who is hopeless.
 I mean this is a great word.
 Someone who's hopeless means someone who doesn't hope.
 Someone who hopes means they still want things.
 Meditators hope that tomorrow will be, the pain will go
 away tomorrow.
 I had a bad day today, I hope that tomorrow's a better day.
 Or I had a really good day, I hope tomorrow's just like
 today or even better.
 So here's a reminder for you, abandon all hope.
 Abandon all hope you enter here.
 Without hope, it's like without wanting.
 If you have no hope, then you're already perfect.
 It's really that simple.
 If you want to be happy, the only way to be happy is to
 stop wanting,
 and so therefore to stop hoping.
 Hope will always be a vulnerability, never be an asset.
 It's not an asset to become enlightened.
 You can't hope you're going to become enlightened.
 That doesn't work that way.
 Hope is a detriment because it means you're discontent.
 It means you're not fully present here, objective with
 reality.
 You're biased, partial,
 and therefore disappointed much of the time.
 [
 So that's familiar to me.
 You can only find it in the Paddisambhida manga, which is a
 sort of a lesser or...
 I mean, it's one of the more technical part, books of the
 Tipitaka,
 probably not actually the words of the Buddha.
 It's supposed to be the words of Sariputta, but maybe.
 So anyway, it says,
 [
 This appears to have come from an earlier text, but I don't
 know where that is.
 So it gives a list of qualities of an aria.
 I mean, this is the list of what it means to be an
 enlightened being.
 So it's another really good list to think of.
 So it's simpler than the other one.
 These two words often go together.
 They're on that translation of...
 They're not a translation. They relate to the verse that we
 have hanging on the wall.
 Akodno means one who doesn't get angry.
 Sariputta was famous for this.
 There was a monk who accused him of all sorts of things.
 There was another monk.
 There's a good story of a monk who heard that Sariputta
 didn't get angry.
 Everyone's praising Sariputta, and so he thought,
 "I'm going to test this."
 So he took a stick,
 and when Sariputta was walking, he actually came up behind
 him
 and just whacked him across the back with a stick.
 Yeah, it's never happened to me,
 but I've certainly been tested by my share of people.
 I don't have to compare myself to Sariputta, but as a monk,
 you know,
 it sounds crazy to think, but yeah, there are people who
 just...
 The first order of business for them is to test the people
 who they're going to look up to.
 So he slapped him across the back, and Sariputta just turns
 around
 and looks at him and then keeps walking.
 Akodno, he didn't get angry.
 Anupanahi is what relates to that verse on the wall.
 Anupanahi means to get angry back,
 or it means to hold on to the anger.
 Holding on to the anger is the worst evil, right?
 I mean, getting angry is not something we all work with,
 work at,
 work to overcome as meditators.
 In big meditation centers, meditators will often get angry,
 and people who work in the meditation center certainly will
 get angry at each other.
 And that's something that, to some extent, we have to allow
.
 We have to make allowance for each other.
 Yes, this person is angry at me.
 I mean, I think often it's dangerous we fall into this word
 where we don't allow it.
 Lack of English words.
 We aren't able to accept someone else's angry, intolerance,
 that sort.
 We're intolerant of other people's anger, and we think,
 "Hey, these are meditators. What are they doing getting
 angry?"
 I had one monk.
 He was threatening to smash me against a wall, and a really
 big British guy once.
 Another monk chased me through the forest once.
 I had a monk take a broomstick to my head once.
 He didn't actually hit me, but he would have.
 He really would have.
 He was ready to.
 I was testing him because, long story.
 But you have to allow for, to some extent.
 I mean, I think the broomstick was a bit much.
 The problem is when we hold on to it,
 you know, if you understand these things, you understand
 that these conditions come up,
 and you deal with them.
 I remember angry people coming to my teacher and leaving
 without the anger
 because he didn't partake in it, you know?
 I'd sit and watch, and people come up with all sorts,
 and it's like a sponge just taking it in, cleaning it out,
 in with a bad, out with a good.
 You know, that's kind of how we should be, is like filters.
 People bring us all their garbage.
 We take it in.
 And really, it means we don't take it in.
 But it's kind of like a filter.
 You act like a filter because normally not taking in means
 to reject it.
 You know, you're angry at me.
 I reject that.
 No, you're not allowed to be angry at me.
 Go away.
 Get out of my face.
 I won't.
 I can't handle this.
 Go in your room.
 Lock the door.
 That's how we normally reject people's anger.
 But that's this.
 That's holding on to it.
 That's reacting to it.
 It's really, the Buddha said it's the worst evil.
 The sevate na papi o yokudang patikuchiti.
 One who is angry back at someone who gets angry.
 It's the worst evil.
 Because that's what creates the conflict.
 To be a real filter and to have true purity.
 You take it in.
 You accept it.
 Someone's angry at you.
 I mean, the anger doesn't come to you.
 We think of it like the anger is some kind of vibe that
 makes us angry.
 But it's not.
 All that comes to us is seeing, hearing, smelling, tasting,
 feeling, thinking.
 Our anger is totally unrelated to their anger.
 That's what we don't realize.
 Your anger is not because they are angry.
 It's because you get tricked into reacting the way they
 react.
 Where we mimic someone's angry.
 We get angry.
 Someone's stressed.
 We get stressed.
 And we think, oh, these, they rubbed off on me.
 It didn't rub off.
 You're, you're mimicking them.
 You're, you're letting it trigger you.
 So,
 (speaking in foreign language)
 Not to get angry, not to hold on to anger.
 (speaking in foreign language)
 Means to not be crooked.
 What is (speaking in foreign language)
 I know (speaking in foreign language)
 (speaking in foreign language)
 Is to not look down on people.
 To not be condescending or arrogant, conceited.
 The, this one monk accused Sariputta of, of, of hitting him
.
 And what had happened is Sariputta had walked by and
 brushed his robe.
 Right?
 There was part of his robe saying Sariputta brushed him
 like that.
 And so he goes around saying that he really had a bone to
 pick.
 It was something about something else.
 He felt like Sariputta was partial against him.
 And so he, he wanted to create trouble.
 And so he went around saying that Sariputta had hit him.
 And Sariputta came before the Buddha and the Buddha said,
 "Hey, so they're saying that you hit this monk.
 Is it true?"
 Or he didn't even ask whether it's true because he knows it
's not true.
 But he said, "This is what they're saying."
 And Sariputta said, "You know, for someone who valued this
 body,
 someone who valued the physical body, then I might,
 such a person might hit another person."
 He said, "But this body is, is like a corpse to me that I
 have to carry around."
 "I have no attachment to the physical realm whatsoever."
 "I would have no reason to hit anyone."
 This, the teaching is that an enlightened being
 doesn't have any, you know, attachment to themselves.
 No reason to harm others because
 they don't cling to themselves.
 So there's no holding yourself as hired as someone else.
 The idea that you might hit someone else is just ridiculous
.
 And then we have "Wisudho, Sudatangatoh."
 "Wisudho" means someone who is pure.
 So I mean, this is really the key.
 And this is something that we're all, I think, fairly well
 aware of.
 An enlightened being doesn't have anger, greed, delusion.
 Their minds are pure.
 Sudatangatoh means going to that which is pure, which is
 nibbana again.
 They enter into states of purity where they become free
 from suffering.
 "Sampanaditi made dhavi." They have right view and their
 wives.
 "Dangjanya aryo." Such a person you know as an Arya.
 So "Sampanaditi" means right view.
 And there's much, there's many aspects to this.
 I mean, right view to some extent is just knowing that what
 the Buddha taught was right.
 But most importantly, it relates to the Four Noble Truths.
 To know that nothing is worth clinging to, basically.
 What you're learning now may not realize it, but if you
 think about it, and if you reflect upon what
 you are learning, not even maybe what you expected to learn
, but what you're learning is that nothing
 is worth clinging to. You're looking at, you're realizing
 that you're clinging to a lot of stuff
 that really isn't worth clinging to. Our suffering comes
 from clinging to things.
 That's the Four Noble Truths. When you really realize that,
 that's called right view, noble right view.
 That's what leads to nibbana. That's what leads to freedom.
 "May thou be as wisdom, or one who is wise." And so that
 relates a lot to right view.
 But there's more, I think. What right view does for you
 is allows you wisdom of so many, you know, of many
 different kinds.
 You find that amazingly you're able to solve all your
 worldly problems much better.
 And still challenges, but the challenges are reduced by an
 order of magnitude, at least.
 They're not difficult in the same way because you realize
 the true problem is never with your
 experiences or your situation, it's with your reactions.
 You're able to look at a situation
 objectively because you're not hating it or loving it. You
're observing it. And so therefore,
 you can see what most other people overlook. And they tie
 themselves in knots to get what they
 want and to get away from what they don't want. So that
 sort of wisdom, it's the wisdom of an
 enlightened being to be able to solve and to fix and to
 organize and to build great things.
 And that's the Dhamma for tonight. A little bit of insight
 into what it's like to be enlightened.
 I mean, just some idea of some of the qualities of mind
 that an enlightened being possesses.
 Useful for us as this is what we strive for and remind us
 of some of these things and hopefully
 give us something to aim for, that's something that we'd
 like to be. Hopefully some of this seems
 desirable, like something that you would like to strive
 towards. Because just feeling that way,
 just aligning yourself with these qualities, just that, I
 mean, is a great step towards becoming
 that. Because of your intention, your intention and your
 inclination will drive you. It's what
 will lead you. Having that framework puts you in the right
 direction. It focuses your practice.
 So there you go. That's the Dhamma for tonight. Thank you
 all for tuning in.
 You all can go practice. I'm going to answer a few
 questions.
 Let's see how many questions. Okay, we've got four
 questions. Since killing is against the precepts,
 is spraying a house against bugs cockroaches killing
 considered killing? I mean, unless it's
 repellent, those things usually kill the insect. So yes,
 that would be killing. As far as bacteria,
 I don't know. I don't really have an answer for that. I'm
 not sure whether bacteria has a mind or not.
 Someone was saying no, a monk, I asked a monk this once and
 he said no, because
 you don't even, you don't even know that the bacteria are
 there.
 That's kind of a weird answer. I'm not so convinced, but it
 was more complicated than that. But
 it's not something I would worry about personally.
 I mean, I don't sympathize, I don't empathize so much with
 bacteria. I empathize with insects.
 So therefore I feel it important not to kill insects. Don't
 empathize with bacteria.
 Okay, I'm feel less propelled to engage in social
 interactions.
 Okay, I'm also doing Samatha and Ripasa. Well, I mean, this
 is a long question, but
 my only answer to you is do one or the other. I mean, I don
't teach Samatha, I think you know.
 So if you want my advice, you really have to, your best bet
 is to just practice as I teach. Now,
 if you're practicing something else, really, you should
 find a teacher who will help you with that.
 What are Nagas in Buddhism?
 I don't offer anything to the Nagas. Nagas are dragons
 really, or it's a serpent race that
 apparently lives under the earth, under the water, I don't
 know, in their own realm, actually. It's
 like a another dimension or something. How important is
 kindness and or giving to the
 practice? How should one interact with people not directly
 involved with one's life?
 I think it's important. I mean, I think it's a quality of
 an enlightened being. So doing it is
 a way of emulating an enlightened being. Someone who,
 because an enlightened being has no attachment
 to things. So if someone asks you for something, it doesn't
 mean you should always give,
 but it is an interesting practice. It's a useful practice
 because it teaches you to let go.
 When you don't give, when someone asks you, when you don't
 help, when someone needs help,
 you're stingy, you're greedy, you're holding on.
 Maybe you're even arrogant.
 Okay, and that's the questions. So thank you all for coming
 out. Have a good night.
