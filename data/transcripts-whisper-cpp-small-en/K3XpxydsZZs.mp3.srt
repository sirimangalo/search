1
00:00:00,000 --> 00:00:05,240
 So we have a question from Emma about how your family is

2
00:00:05,240 --> 00:00:10,000
 reacting to your ordination for Sumaydha, I guess.

3
00:00:10,000 --> 00:00:16,000
 I guess one of the first things that arises in my mind is

4
00:00:16,000 --> 00:00:21,000
 why someone would ask this question,

5
00:00:21,000 --> 00:00:27,680
 this ordination perhaps something you're thinking of, and

6
00:00:27,680 --> 00:00:30,700
 maybe your family, maybe there's fear that your family

7
00:00:30,700 --> 00:00:31,000
 would not approve,

8
00:00:31,000 --> 00:00:37,050
 I think is big for a lot of people. My family, my parents

9
00:00:37,050 --> 00:00:42,000
 are Catholic and my dad actually said,

10
00:00:42,000 --> 00:00:44,520
 "Oh, it's good you're going the Buddhist route for the nun

11
00:00:44,520 --> 00:00:45,000
 thing."

12
00:00:45,000 --> 00:00:51,310
 When I was 16 I decided I wanted to be a Catholic nun. And

13
00:00:51,310 --> 00:00:54,680
 to be a Catholic nun you have to be a novice for I think

14
00:00:54,680 --> 00:00:55,000
 five or seven years

15
00:00:55,000 --> 00:00:59,230
 and it's a lifetime commitment and to break it is a big

16
00:00:59,230 --> 00:01:02,000
 deal. But with a Buddhist nun my dad thinks,

17
00:01:02,000 --> 00:01:05,400
 "Oh, she can easily disrobe and it's no big deal and this

18
00:01:05,400 --> 00:01:07,000
 is a temporary thing."

19
00:01:07,000 --> 00:01:12,120
 My mom thinks this is a temporary thing, I'm in a phase.

20
00:01:12,120 --> 00:01:16,000
 And so they're trying to be supportive.

21
00:01:16,000 --> 00:01:20,360
 My dad's actually coming out for the ordination, which is a

22
00:01:20,360 --> 00:01:26,000
 big deal. But I think he wants to travel as well.

23
00:01:26,000 --> 00:01:30,940
 Yeah, they're trying to be supportive. They don't really

24
00:01:30,940 --> 00:01:34,000
 understand Buddhism on an experiential level

25
00:01:34,000 --> 00:01:36,980
 because at least in this lifetime they haven't given

26
00:01:36,980 --> 00:01:38,000
 meditation a try.

27
00:01:38,000 --> 00:01:42,110
 But they're interested in learning about it and ask me

28
00:01:42,110 --> 00:01:43,000
 questions.

29
00:01:43,000 --> 00:01:47,400
 And my brothers and sisters, I think for them it's more

30
00:01:47,400 --> 00:01:50,000
 like being so far away from home.

31
00:01:50,000 --> 00:01:56,010
 It's not so much what I'm doing. My sister just gets really

32
00:01:56,010 --> 00:01:59,000
 worried when she doesn't hear from me and my brother's fine

33
00:01:59,000 --> 00:01:59,000
.

34
00:01:59,000 --> 00:02:03,680
 But yeah, my other sister, it's just more like being away

35
00:02:03,680 --> 00:02:05,000
 from the family.

36
00:02:05,000 --> 00:02:09,830
 And I guess when I actually ordain I'll be even a bit more

37
00:02:09,830 --> 00:02:14,000
 off the wire for at least some time probably.

38
00:02:14,000 --> 00:02:18,650
 So it's more of like missing the missing factor. But they

39
00:02:18,650 --> 00:02:23,000
're very supportive and which is a great, great blessing.

40
00:02:23,000 --> 00:02:27,000
 Were you going to say something, Bante?

41
00:02:27,000 --> 00:02:31,000
 How's his family acting?

42
00:02:31,000 --> 00:02:35,870
 Well, I'm not in my twenties anymore, so I've been talking

43
00:02:35,870 --> 00:02:38,000
 about it for a long time.

44
00:02:38,000 --> 00:02:43,050
 And the last time I saw them, just a couple of weeks ago,

45
00:02:43,050 --> 00:02:47,000
 they accepted it and I was surprised myself.

46
00:02:47,000 --> 00:02:52,000
 And no discussion. It was fine.

47
00:02:52,000 --> 00:02:55,000
 I was very happy about that.

48
00:02:55,000 --> 00:03:00,000
 It's funny how when the time comes for...

49
00:03:00,000 --> 00:03:05,780
 But after that I lived in a Zen center in a monastery for a

50
00:03:05,780 --> 00:03:07,000
 whole year.

51
00:03:07,000 --> 00:03:11,750
 So they were quite prepared for this. It wasn't some

52
00:03:11,750 --> 00:03:14,000
 shocking news.

53
00:03:14,000 --> 00:03:18,420
 Yeah, I think it was the same with me and it's maybe

54
00:03:18,420 --> 00:03:22,130
 something that's not really...something that we don't

55
00:03:22,130 --> 00:03:25,000
 realize until it happens is that in the end,

56
00:03:25,000 --> 00:03:29,230
 what's far more important for our parents is the

57
00:03:29,230 --> 00:03:32,000
 communication and the fact that we're going to...

58
00:03:32,000 --> 00:03:34,000
 They're going to lose us.

59
00:03:34,000 --> 00:03:38,840
 But for my parents it was much more losing me than about

60
00:03:38,840 --> 00:03:43,000
 having me change my religion or something.

61
00:03:43,000 --> 00:03:47,580
 It seems to me that there is something deep down that your

62
00:03:47,580 --> 00:03:52,570
 parents will gravitate more towards that and become

63
00:03:52,570 --> 00:03:54,000
 Buddhist if you want.

64
00:03:54,000 --> 00:03:57,000
 But don't let me never see you again.

65
00:03:57,000 --> 00:04:00,000
 That kind of thing.

66
00:04:00,000 --> 00:04:03,530
 So probably you could leverage that and say, "Look, I'm

67
00:04:03,530 --> 00:04:05,000
 going to run away from..."

68
00:04:05,000 --> 00:04:11,000
 There goes the internet.

69
00:04:11,000 --> 00:04:13,000
 Okay, stop.

