1
00:00:00,000 --> 00:00:06,670
 Okay, so this is our first monk radio for the Wasa period,

2
00:00:06,670 --> 00:00:07,600
 the rains period.

3
00:00:07,600 --> 00:00:14,800
 We entered into the rains on the second. There was some

4
00:00:14,800 --> 00:00:17,520
 confusion because in Thailand it's on the

5
00:00:17,520 --> 00:00:20,970
 third and we had all been under the, or we had the two of

6
00:00:20,970 --> 00:00:23,440
 us here had been under the impression

7
00:00:24,400 --> 00:00:31,190
 that it was starting on the third. Even, yes, and so I

8
00:00:31,190 --> 00:00:35,440
 assumed that the full moon, the holiday would

9
00:00:35,440 --> 00:00:41,350
 be on the second and on the 31st I asked them, I said, "So

10
00:00:41,350 --> 00:00:44,880
 when, how many tomorrow, day after

11
00:00:44,880 --> 00:00:48,300
 two days is the play or something? The holiday?" And they

12
00:00:48,300 --> 00:00:51,920
 said, "No, tomorrow the first." And so

13
00:00:51,920 --> 00:00:54,230
 it turned out and then we were all confused because the

14
00:00:54,230 --> 00:00:56,000
 holiday was on the first, but he said, "No,

15
00:00:56,000 --> 00:00:59,020
 no, then on the second is the real full moon and then the

16
00:00:59,020 --> 00:01:00,880
 third is when we enter the rains."

17
00:01:00,880 --> 00:01:04,280
 Anyway, so it was all confused and on the second he

18
00:01:04,280 --> 00:01:08,400
 realized, Bhandyanoma realized that this was,

19
00:01:08,400 --> 00:01:11,900
 that we'd missed the full moon day and the second was the

20
00:01:11,900 --> 00:01:14,320
 day to go into the rain. So he quickly

21
00:01:14,320 --> 00:01:21,270
 left and he's gone now back to Ratnapura and we have, I'm

22
00:01:21,270 --> 00:01:24,640
 staying here now with just the old monk

23
00:01:24,640 --> 00:01:30,810
 who's sick. Actually now he's gone to Kandy, Nuwara, the

24
00:01:30,810 --> 00:01:34,560
 city of Kandy, along with the four Thai yogis

25
00:01:34,560 --> 00:01:41,830
 and the Burmese nun and several of the villagers, everyone

26
00:01:41,830 --> 00:01:43,600
 piled into a van and they wanted me to

27
00:01:43,600 --> 00:01:46,810
 go as well. But you see they are not back yet. They said

28
00:01:46,810 --> 00:01:48,880
 they would be back in time for this

29
00:01:48,880 --> 00:01:51,900
 broadcast and they're not going to be back until nine, I

30
00:01:51,900 --> 00:01:56,160
 don't think. So I would have missed this

31
00:01:56,160 --> 00:02:02,660
 broadcast as well as other, other duties that I had to

32
00:02:02,660 --> 00:02:09,520
 attend to today. So I'm here alone right now,

33
00:02:09,520 --> 00:02:15,530
 waiting for them to come back and this is the rains except

34
00:02:15,530 --> 00:02:17,200
 it's raining very, very little

35
00:02:17,200 --> 00:02:19,600
 and that's a concern because our well is running dry.

36
00:02:19,600 --> 00:02:23,760
 Tomorrow Peter is coming, Peter Wolf,

37
00:02:23,760 --> 00:02:28,890
 who is one of our online community members, which is really

38
00:02:28,890 --> 00:02:31,040
, you know, really exciting to see

39
00:02:32,480 --> 00:02:39,080
 someone you already know through something very odd as our

40
00:02:39,080 --> 00:02:39,920
 community is,

41
00:02:39,920 --> 00:02:50,220
 come and actually make the, or lead to the coming to med

42
00:02:50,220 --> 00:02:52,480
itate in a more traditional sense.

43
00:02:52,480 --> 00:02:57,620
 So it's reaffirming for the benefit towards the benefit of

44
00:02:57,620 --> 00:03:00,720
 the online community and

45
00:03:02,640 --> 00:03:04,900
 shouldn't I think, or really makes, makes it seem

46
00:03:04,900 --> 00:03:08,640
 worthwhile to have the online community.

47
00:03:08,640 --> 00:03:13,920
 So he will be here tomorrow morning and then I just got an

48
00:03:13,920 --> 00:03:15,600
 email that on the

49
00:03:15,600 --> 00:03:21,710
 seventh or ninth, I think Harish is coming and he's a Sri

50
00:03:21,710 --> 00:03:24,400
 Lankan from Colombo.

51
00:03:25,840 --> 00:03:30,160
 So the Thai people are leaving today. Not sure about the

52
00:03:30,160 --> 00:03:32,560
 novice because she hasn't disrobed yet.

53
00:03:32,560 --> 00:03:35,910
 Her plan was to disrobe because she still has to clear

54
00:03:35,910 --> 00:03:37,920
 things with her mother. But I said,

55
00:03:37,920 --> 00:03:41,790
 if you're not back by 7 30, I'm not available to, to help

56
00:03:41,790 --> 00:03:44,240
 you to disrobe. So you'll just have to

57
00:03:44,240 --> 00:03:46,570
 stay on us and as a novice. And she was like, yeah, yeah,

58
00:03:46,570 --> 00:03:48,800
 don't worry. We'll be back. And if,

59
00:03:48,800 --> 00:03:51,860
 and we were joking, if she's not back, then she's, she's

60
00:03:51,860 --> 00:03:54,320
 just people. The other people are going to

61
00:03:54,320 --> 00:03:57,920
 have to explain to her boss why she's not going back to

62
00:03:57,920 --> 00:04:01,200
 work tomorrow and her motherhood would

63
00:04:01,200 --> 00:04:07,030
 be very concerned. So they're leaving and we still have

64
00:04:07,030 --> 00:04:10,640
 Polly from Finland. He's just finishing his

65
00:04:10,640 --> 00:04:15,820
 first course. He's here till October. So yeah, things are

66
00:04:15,820 --> 00:04:20,080
 going well. It's lots of good things

67
00:04:20,080 --> 00:04:23,300
 happening. I started a translation project. If you're

68
00:04:23,300 --> 00:04:25,200
 following my web blog or on the online

69
00:04:25,200 --> 00:04:29,230
 community, you saw that translations are interesting. I, I

70
00:04:29,230 --> 00:04:31,040
 remind, I was reminded after

71
00:04:31,040 --> 00:04:34,660
 doing it that you can't just literally translate the Polly

72
00:04:34,660 --> 00:04:38,080
 into English. It turns out a little bit

73
00:04:38,080 --> 00:04:41,530
 awkward. So I'm remind after getting some comments on it, I

74
00:04:41,530 --> 00:04:43,680
 was reminded of that, the fact that you

75
00:04:44,400 --> 00:04:48,720
 do actually need to take some, some literary license, I

76
00:04:48,720 --> 00:04:51,440
 think, which is a difficult thing to do

77
00:04:51,440 --> 00:04:55,130
 because you don't want to change the meaning, but probably

78
00:04:55,130 --> 00:04:57,280
 have to learn how to finesse it.

79
00:04:57,280 --> 00:05:00,700
 But so the idea is to do some translations. The reason for

80
00:05:00,700 --> 00:05:02,560
 starting to do translations is,

81
00:05:02,560 --> 00:05:06,390
 it's not that I'm looking for more work, but there's been

82
00:05:06,390 --> 00:05:09,360
 some, no, for a long time there's been this

83
00:05:10,000 --> 00:05:12,000
 um

84
00:05:12,000 --> 00:05:17,990
 and I guess a difference of opinion. So there's, there's

85
00:05:17,990 --> 00:05:20,800
 Buddhists, there's many,

86
00:05:20,800 --> 00:05:24,350
 there's, there's Buddhists out there who have translated

87
00:05:24,350 --> 00:05:26,880
 the majority of the Buddhist teaching,

88
00:05:26,880 --> 00:07:46,160
 but are not making those teachings available for the public

89
00:07:46,160 --> 00:05:38,080
 on the internet and are threatening

90
00:05:39,280 --> 00:05:44,160
 or in some cases threatening, in some cases just asking

91
00:05:44,160 --> 00:05:49,520
 anyone who does share the, their works on

92
00:05:49,520 --> 00:05:52,990
 the internet to take them down. Um, so in some cases

93
00:05:52,990 --> 00:05:56,560
 actually threatening them with legal action.

94
00:05:56,560 --> 00:06:00,340
 So and, and the response, you know, when, when, when we

95
00:06:00,340 --> 00:06:02,160
 disagree, when we express disagreement

96
00:06:02,160 --> 00:06:04,550
 with this, the response is, well, why don't you translate

97
00:06:04,550 --> 00:06:07,120
 them yourselves? So, you know, or, or

98
00:06:07,120 --> 00:06:11,340
 often, or in some cases it's even, um, you know, I don't

99
00:06:11,340 --> 00:06:14,240
 see you translating them and so on.

100
00:06:14,240 --> 00:06:18,500
 So it's kind of like, see, see, uh, no, it's more like if

101
00:06:18,500 --> 00:06:21,760
 that's the, the way it's going to be

102
00:06:21,760 --> 00:06:24,780
 and we're not going to be able, because I've got many of

103
00:06:24,780 --> 00:06:29,440
 the, these for, for profit texts, uh,

104
00:06:29,440 --> 00:06:32,560
 in PDF or doc format in, in some sort of format that could

105
00:06:32,560 --> 00:06:34,240
 easily be put on the internet,

106
00:06:34,240 --> 00:06:38,840
 but I'm not allowed to do it. Uh, so rather than create

107
00:06:38,840 --> 00:06:42,560
 conflict, more conflict than, than necessary,

108
00:06:42,560 --> 00:06:48,410
 I thought I'd do some translating myself and I'm not sure

109
00:06:48,410 --> 00:06:50,640
 how far it'll get. I've done one

110
00:06:50,640 --> 00:06:55,540
 suta from the Majima Nikaya so far and, um, it wasn't, it's

111
00:06:55,540 --> 00:06:58,160
 not difficult. It doesn't take that

112
00:06:58,160 --> 00:07:01,860
 much time and it's a good, the reason why I actually

113
00:07:01,860 --> 00:07:04,240
 decided in the end to do it is because

114
00:07:04,240 --> 00:07:06,780
 it's not difficult and it's something that is very

115
00:07:06,780 --> 00:07:09,120
 important for monks to engage in the study

116
00:07:09,120 --> 00:07:16,370
 of both the scriptures and, and the Pali language. Um, I'm

117
00:07:16,370 --> 00:07:17,840
 certainly not an expert,

118
00:07:17,840 --> 00:07:20,530
 a master in the Pali, of the Pali language and it's, it's

119
00:07:20,530 --> 00:07:22,800
 really a help for me to, to

120
00:07:24,000 --> 00:07:28,020
 continue my studies in that way. So just another

121
00:07:28,020 --> 00:07:30,160
 announcement that's on the go now.

122
00:07:30,160 --> 00:07:36,960
 Not much else. I think that's enough talk. And now if there

123
00:07:36,960 --> 00:07:39,680
 are any questions, we can, uh,

124
00:07:39,680 --> 00:07:41,360
 get on with the show.

125
00:07:41,360 --> 00:07:48,720
 Hmm.

