1
00:00:00,000 --> 00:00:07,000
 Okay, good evening everyone.

2
00:00:07,000 --> 00:00:12,000
 Welcome to our evening broadcast.

3
00:00:12,000 --> 00:00:20,000
 Evening Dhamma talk.

4
00:00:20,000 --> 00:00:23,000
 Tonight's topic is a challenging one.

5
00:00:23,000 --> 00:00:30,000
 It's not going to be long, there's not too much to say.

6
00:00:30,000 --> 00:00:33,000
 I mean sometimes we sugarcoat the truth.

7
00:00:33,000 --> 00:00:35,000
 I think I'm guilty of that.

8
00:00:35,000 --> 00:00:40,000
 Making Buddhism sound more pleasant or more accommodating

9
00:00:40,000 --> 00:00:44,000
 than it actually is.

10
00:00:44,000 --> 00:00:52,000
 And the truth is, the Buddha really saw

11
00:00:52,000 --> 00:00:58,000
 our situation as human beings is quite wretched.

12
00:00:58,000 --> 00:01:00,000
 There's no escaping that.

13
00:01:00,000 --> 00:01:03,000
 So when we talk about Buddhism as being pessimistic,

14
00:01:03,000 --> 00:01:08,000
 we can deny it all we want, but to some extent,

15
00:01:08,000 --> 00:01:10,000
 it's not pessimistic, I've talked about that.

16
00:01:10,000 --> 00:01:13,000
 That word has too much associated with it,

17
00:01:13,000 --> 00:01:16,000
 but it definitely doesn't look favorable upon it.

18
00:01:16,000 --> 00:01:20,000
 It makes a claim that you can argue,

19
00:01:20,000 --> 00:01:25,020
 you could say it's pessimistic because you might disagree

20
00:01:25,020 --> 00:01:26,000
 with it.

21
00:01:26,000 --> 00:01:30,050
 But the argument is that our existence as human beings is

22
00:01:30,050 --> 00:01:37,000
 quite wretched.

23
00:01:37,000 --> 00:01:44,280
 And it has very much to do with the fact that we're

24
00:01:44,280 --> 00:01:46,000
 dependent.

25
00:01:46,000 --> 00:01:53,220
 We are as slaves, as sick people, in ways that we don't

26
00:01:53,220 --> 00:01:54,000
 realize.

27
00:01:54,000 --> 00:01:59,000
 We think that we're happy sometimes.

28
00:01:59,000 --> 00:02:02,990
 When we don't really understand what happiness is, we think

29
00:02:02,990 --> 00:02:04,000
 we do.

30
00:02:04,000 --> 00:02:09,000
 But when we think of as happiness,

31
00:02:09,000 --> 00:02:16,000
 it turns out to be a cause for great suffering.

32
00:02:16,000 --> 00:02:24,280
 It turns out to be an inherent problem with our way of

33
00:02:24,280 --> 00:02:29,000
 looking at the world,

34
00:02:29,000 --> 00:02:33,000
 based on the fact that we are very much dependent,

35
00:02:33,000 --> 00:02:42,000
 and therefore at the mercy of the vicissitudes of life.

36
00:02:42,000 --> 00:02:47,000
 So this particular suta, it's in the Sanyuta Nikaya,

37
00:02:47,000 --> 00:02:53,000
 Nidhana Sanyuta, which is a wonderful Sanyuta.

38
00:02:53,000 --> 00:02:56,000
 Sanyuta just means collection.

39
00:02:56,000 --> 00:02:59,190
 So it's this collection of discourses based around paticca

40
00:02:59,190 --> 00:03:00,000
 samuppada.

41
00:03:00,000 --> 00:03:03,000
 So if you want to learn about dependent origination,

42
00:03:03,000 --> 00:03:06,460
 this is the chapter to read, the collection of sutas to

43
00:03:06,460 --> 00:03:07,000
 read.

44
00:03:07,000 --> 00:03:09,000
 Sanyuta Nikaya is really wonderful.

45
00:03:09,000 --> 00:03:13,620
 It's just very, very big, so it's not an easy book to

46
00:03:13,620 --> 00:03:16,000
 really go through.

47
00:03:16,000 --> 00:03:19,000
 That doesn't mean you shouldn't.

48
00:03:19,000 --> 00:03:22,000
 But this is number 63.

49
00:03:22,000 --> 00:03:26,000
 What's it called? Puta mang supama suta.

50
00:03:26,000 --> 00:03:33,100
 Supama is a simile. Mangsa means flesh, and puta means son

51
00:03:33,100 --> 00:03:35,000
 or child.

52
00:03:35,000 --> 00:03:40,000
 So the simile of the flesh of the child.

53
00:03:40,000 --> 00:03:43,000
 The suta is actually about food.

54
00:03:43,000 --> 00:03:48,000
 Remember a few days ago, I think, or some days ago,

55
00:03:48,000 --> 00:03:55,290
 I mentioned this as the first of the ten dharakapana, kum

56
00:03:55,290 --> 00:03:56,000
ara pana,

57
00:03:56,000 --> 00:04:02,000
 the ten questions of the young child.

58
00:04:02,000 --> 00:04:07,000
 The first one is sabhaya sata aharititika,

59
00:04:07,000 --> 00:04:10,000
 all beings subsist on food.

60
00:04:10,000 --> 00:04:12,000
 And I said there are four types of food.

61
00:04:12,000 --> 00:04:14,000
 Well, this suta lays them out.

62
00:04:14,000 --> 00:04:19,520
 The four types of food are kabalinka, kabalinka hahara, it

63
00:04:19,520 --> 00:04:24,000
's material food.

64
00:04:24,000 --> 00:04:30,240
 Pasahara, which is, or nutriment, is a better word, the nut

65
00:04:30,240 --> 00:04:34,000
riment of contact.

66
00:04:34,000 --> 00:04:40,000
 The third is manosanjaitana, manosanjaitana hahara,

67
00:04:40,000 --> 00:04:48,000
 the nutriment of mental intention or volition.

68
00:04:48,000 --> 00:04:52,810
 And the fourth is vinyanahara, which is the food of

69
00:04:52,810 --> 00:04:54,000
 consciousness,

70
00:04:54,000 --> 00:04:57,000
 the nourishment of consciousness, and nutriment.

71
00:04:57,000 --> 00:05:00,450
 And so what it means by nutriment is, again, this

72
00:05:00,450 --> 00:05:02,000
 dependency.

73
00:05:02,000 --> 00:05:04,000
 It's the cause.

74
00:05:04,000 --> 00:05:07,320
 I mean, the whole of this collection, and really all of the

75
00:05:07,320 --> 00:05:08,000
 Buddha's teaching,

76
00:05:08,000 --> 00:05:14,000
 about seeing cause and effect, about learning how all of

77
00:05:14,000 --> 00:05:14,000
 who we are

78
00:05:14,000 --> 00:05:22,000
 and what we experience is dependent on causality.

79
00:05:22,000 --> 00:05:26,100
 We got the way we are for a reason, and we learn about

80
00:05:26,100 --> 00:05:27,000
 those reasons.

81
00:05:27,000 --> 00:05:31,340
 And we realize how we're still becoming more and more based

82
00:05:31,340 --> 00:05:32,000
 on the food

83
00:05:32,000 --> 00:05:37,000
 that we take in, the nourishment, the nutriment.

84
00:05:37,000 --> 00:05:40,310
 If we take in bad, and if we take on bad, and if we

85
00:05:40,310 --> 00:05:41,000
 cultivate the bad,

86
00:05:41,000 --> 00:05:45,000
 then bad is the result.

87
00:05:45,000 --> 00:05:49,480
 If we cultivate the good, good is the result. It's quite

88
00:05:49,480 --> 00:05:52,000
 simple.

89
00:05:52,000 --> 00:05:54,520
 And so these four, the Buddha laid out as sort of an

90
00:05:54,520 --> 00:05:55,000
 outline

91
00:05:55,000 --> 00:06:02,000
 of the kinds of things that have an effect on us,

92
00:06:02,000 --> 00:06:05,000
 the kind of things, the different kind of things that

93
00:06:05,000 --> 00:06:05,000
 change us,

94
00:06:05,000 --> 00:06:12,000
 that feed who we are.

95
00:06:12,000 --> 00:06:15,400
 And I said in the beginning, he sees that it has quite

96
00:06:15,400 --> 00:06:16,000
 wretched,

97
00:06:16,000 --> 00:06:20,000
 this situation of dealing with these types of food.

98
00:06:20,000 --> 00:06:23,690
 They don't sound bad at all, I suppose, when you think

99
00:06:23,690 --> 00:06:24,000
 about,

100
00:06:24,000 --> 00:06:27,370
 when we hear about them, because we're quite enamored with

101
00:06:27,370 --> 00:06:28,000
 them.

102
00:06:28,000 --> 00:06:32,140
 But the Buddha, in this sutta, he lays out a description of

103
00:06:32,140 --> 00:06:33,000
 these four.

104
00:06:33,000 --> 00:06:38,000
 So he said, "How should you think of physical nutriment?

105
00:06:38,000 --> 00:06:41,610
 When we think of the food we eat, how should we think of it

106
00:06:41,610 --> 00:06:42,000
?"

107
00:06:42,000 --> 00:06:46,000
 And he gives the simile of the flesh of the child.

108
00:06:46,000 --> 00:06:50,140
 The simile is this, or the example is this, the analogy, I

109
00:06:50,140 --> 00:06:52,000
 guess, is this.

110
00:06:52,000 --> 00:06:55,000
 It's this allegory, I don't know, this story.

111
00:06:55,000 --> 00:07:00,000
 He says, "Imagine there was this couple, a husband and wife

112
00:07:00,000 --> 00:07:00,000
,

113
00:07:00,000 --> 00:07:04,000
 a wife and husband, a woman and a man with their son,

114
00:07:04,000 --> 00:07:07,000
 traveling through the desert.

115
00:07:07,000 --> 00:07:10,000
 They had to escape a plague or something,

116
00:07:10,000 --> 00:07:12,000
 and they hadn't brought much food with them,

117
00:07:12,000 --> 00:07:14,000
 and they didn't realize how long it was,

118
00:07:14,000 --> 00:07:17,840
 and they get halfway through the desert, and they run out

119
00:07:17,840 --> 00:07:19,000
 of food.

120
00:07:19,000 --> 00:07:21,000
 And so they're dying.

121
00:07:21,000 --> 00:07:24,000
 They're both dying, and their child is dying,

122
00:07:24,000 --> 00:07:27,670
 and they can no longer nurse him because their mother's

123
00:07:27,670 --> 00:07:29,000
 breast milk dries up,

124
00:07:29,000 --> 00:07:34,000
 or whatever."

125
00:07:34,000 --> 00:07:37,380
 And he's certainly not advocating this, but what the

126
00:07:37,380 --> 00:07:40,000
 husband and wife think to themselves

127
00:07:40,000 --> 00:07:43,000
 is that they're all going to die.

128
00:07:43,000 --> 00:07:46,000
 And he's not advocating this, this is not the teaching,

129
00:07:46,000 --> 00:07:49,000
 but he says, "Imagine that these two people,

130
00:07:49,000 --> 00:07:53,000
 who are ordinary people who have wrong ideas about things,

131
00:07:53,000 --> 00:07:58,180
 decide that they're going to kill their child and eat his

132
00:07:58,180 --> 00:08:02,000
 flesh, his or her flesh."

133
00:08:02,000 --> 00:08:04,000
 Which is a horrific image, really.

134
00:08:04,000 --> 00:08:07,000
 But he purposefully makes it as horrific as possible,

135
00:08:07,000 --> 00:08:11,000
 and he's not saying this is not horrific.

136
00:08:11,000 --> 00:08:13,000
 But what he says is, he says,

137
00:08:13,000 --> 00:08:18,080
 "Do you think that this man and this woman who loved their

138
00:08:18,080 --> 00:08:19,000
 child dearly

139
00:08:19,000 --> 00:08:24,000
 would eat that food for entertainment?

140
00:08:24,000 --> 00:08:28,370
 Do you think they'd sit around remarking on how delicious

141
00:08:28,370 --> 00:08:29,000
 it is?"

142
00:08:29,000 --> 00:08:33,000
 He asks the monks, "Would they eat it for enjoyment,

143
00:08:33,000 --> 00:08:38,000
 or I mean it's just the most awful example?"

144
00:08:38,000 --> 00:08:42,060
 But he says, "Would they only eat it for crossing this

145
00:08:42,060 --> 00:08:43,000
 desert?"

146
00:08:43,000 --> 00:08:47,180
 And they say, "Yes, that, in this wretched state, that's

147
00:08:47,180 --> 00:08:48,000
 what they would eat it for."

148
00:08:48,000 --> 00:08:53,000
 And he says, "This is how you should see food.

149
00:08:53,000 --> 00:08:55,000
 This is how you should see food."

150
00:08:55,000 --> 00:09:01,000
 And what it does is it points out how wretched we are,

151
00:09:01,000 --> 00:09:08,000
 that we don't see this, our dependency on food.

152
00:09:08,000 --> 00:09:12,000
 We think it's great that we can eat,

153
00:09:12,000 --> 00:09:21,000
 not seeing how ensnared we are, how caught up we are.

154
00:09:21,000 --> 00:09:25,000
 So it's a wake-up call for us to see,

155
00:09:25,000 --> 00:09:32,130
 using this food for pleasure as like chickens clocking

156
00:09:32,130 --> 00:09:34,000
 about in the pen,

157
00:09:34,000 --> 00:09:43,000
 having no clue that they're going to be slaughtered.

158
00:09:43,000 --> 00:09:47,000
 And it's the same because we go through our lives

159
00:09:47,000 --> 00:09:53,000
 enjoying things like food or all the physical pleasures,

160
00:09:53,000 --> 00:10:02,000
 sights, sounds, smells, not just tastes, feelings.

161
00:10:02,000 --> 00:10:06,000
 And we suffer terribly for this because we become ensnared.

162
00:10:06,000 --> 00:10:09,000
 We become, well, at the very least,

163
00:10:09,000 --> 00:10:17,000
 we're hopelessly ignorant of the reality of our situation.

164
00:10:17,000 --> 00:10:20,000
 And so unable to deal with stress and suffering,

165
00:10:20,000 --> 00:10:28,000
 unable to deal with old age, sickness, death, loss, trauma.

166
00:10:28,000 --> 00:10:30,000
 Instead we live our lives praying and hoping

167
00:10:30,000 --> 00:10:35,000
 that these sorts of things don't happen to us.

168
00:10:35,000 --> 00:10:36,000
 We're going to live forever, right?

169
00:10:36,000 --> 00:10:44,160
 Or at the very least, I won't meet with loss or trauma or

170
00:10:44,160 --> 00:10:54,000
 trouble, difficulty.

171
00:10:54,000 --> 00:10:57,000
 Well, we are like this woman and man crossing the desert.

172
00:10:57,000 --> 00:11:00,000
 We're in a wretched situation where we need these things,

173
00:11:00,000 --> 00:11:02,000
 where we are not independent of them.

174
00:11:02,000 --> 00:11:05,000
 And moreover, we are intoxicated by them.

175
00:11:05,000 --> 00:11:08,000
 We're caught up in food, how wonderful it is,

176
00:11:08,000 --> 00:11:12,000
 how much happiness it brings us.

177
00:11:12,000 --> 00:11:14,000
 When in fact we can see that it's just bringing,

178
00:11:14,000 --> 00:11:18,000
 we can see as we meditate that it's just bringing us stress

179
00:11:18,000 --> 00:11:23,000
 and like so many other physical things,

180
00:11:25,000 --> 00:11:29,000
 if we don't, it's something that we just need.

181
00:11:29,000 --> 00:11:37,000
 We can't be without.

182
00:11:37,000 --> 00:11:40,000
 And he says that, I mean, it's an optimistic teaching,

183
00:11:40,000 --> 00:11:42,000
 though many people might not agree.

184
00:11:42,000 --> 00:11:46,000
 The optimism is when you realize this, you free yourself.

185
00:11:46,000 --> 00:11:49,000
 If you really see food in this way,

186
00:11:49,000 --> 00:11:52,960
 not something to enjoy, something that's fraught with

187
00:11:52,960 --> 00:11:53,000
 problems

188
00:11:53,000 --> 00:11:59,000
 and caught up in the suffering of samsara,

189
00:11:59,000 --> 00:12:03,000
 and you have no attachment to it.

190
00:12:03,000 --> 00:12:06,000
 I would have said if you understand this about food

191
00:12:06,000 --> 00:12:10,000
 and you understand all of the senses,

192
00:12:10,000 --> 00:12:13,000
 and he says then there is no fetter.

193
00:12:13,000 --> 00:12:16,600
 And this is the key is that happiness is freedom from the

194
00:12:16,600 --> 00:12:17,000
 fetters,

195
00:12:17,000 --> 00:12:24,000
 freedom from any fetter, freedom from any bondage.

196
00:12:24,000 --> 00:12:33,100
 So that whether you get food or don't get food, you're at

197
00:12:33,100 --> 00:12:38,000
 peace.

198
00:12:38,000 --> 00:12:40,000
 There was someone was asking me,

199
00:12:40,000 --> 00:12:42,000
 why wouldn't an enlightened being just starve themselves?

200
00:12:42,000 --> 00:12:46,000
 And in fact, it apparently is something that's in the text.

201
00:12:46,000 --> 00:12:50,000
 There are enlightened beings who just wouldn't eat,

202
00:12:50,000 --> 00:12:53,000
 not purposefully perhaps,

203
00:12:53,000 --> 00:13:01,000
 but would just not be concerned about going to get food.

204
00:13:01,000 --> 00:13:04,680
 And it's not like, certainly not the way of all enlightened

205
00:13:04,680 --> 00:13:05,000
 beings,

206
00:13:05,000 --> 00:13:07,000
 but there's nothing wrong with this.

207
00:13:07,000 --> 00:13:10,000
 I mean, the point is when you're enlightened, you're free.

208
00:13:10,000 --> 00:13:14,280
 You don't require food to be happy, to satiate you, to

209
00:13:14,280 --> 00:13:16,000
 satisfy you,

210
00:13:16,000 --> 00:13:28,000
 to console you.

211
00:13:28,000 --> 00:13:30,750
 I'm not sure how this teaching tonight is going to be taken

212
00:13:30,750 --> 00:13:31,000
.

213
00:13:31,000 --> 00:13:34,000
 It doesn't get any better from here.

214
00:13:34,000 --> 00:13:36,000
 I don't care.

215
00:13:36,000 --> 00:13:40,000
 Number two, this is the real stuff.

216
00:13:40,000 --> 00:13:43,000
 No, this is the Buddha.

217
00:13:43,000 --> 00:13:46,000
 Number two is contact.

218
00:13:46,000 --> 00:13:49,000
 So what is the food of contact?

219
00:13:49,000 --> 00:13:53,000
 Buddha says, suppose there was a flayed cow.

220
00:13:53,000 --> 00:13:55,000
 This is a word that might not be familiar to everyone.

221
00:13:55,000 --> 00:13:57,000
 F-L-A-Y-E-D.

222
00:13:57,000 --> 00:14:02,000
 Flayed means whipped, right?

223
00:14:02,000 --> 00:14:05,000
 But flayed actually means the skin is broken.

224
00:14:05,000 --> 00:14:09,000
 So it's a cow that someone whipped, or maybe flayed,

225
00:14:09,000 --> 00:14:11,000
 as he used a different instrument, I'm not quite sure,

226
00:14:11,000 --> 00:14:18,000
 but to the point where it starts to bleed.

227
00:14:18,000 --> 00:14:25,000
 And he says, if this cow were to stand exposed to a wall,

228
00:14:25,000 --> 00:14:26,730
 well, the creatures dwelling on the wall would nibble at

229
00:14:26,730 --> 00:14:27,000
 her.

230
00:14:27,000 --> 00:14:30,000
 If it stood exposed to a tree,

231
00:14:30,000 --> 00:14:34,000
 then the creatures in the tree would nibble at her.

232
00:14:34,000 --> 00:14:36,000
 If she stands exposed to water,

233
00:14:36,000 --> 00:14:38,000
 the creatures dwelling in the water would nibble at her.

234
00:14:38,000 --> 00:14:41,000
 If she stands exposed to open air,

235
00:14:41,000 --> 00:14:45,000
 the flies in the air would nibble at her.

236
00:14:45,000 --> 00:14:49,000
 Whatever that flayed cloud stands exposed to,

237
00:14:49,000 --> 00:14:52,000
 the creatures dwelling there would nibble at her.

238
00:14:52,000 --> 00:14:57,000
 So he's setting up the most wretched example of contact,

239
00:14:57,000 --> 00:15:03,000
 this wretchedness of being susceptible to contact.

240
00:15:03,000 --> 00:15:05,000
 He doesn't see contact.

241
00:15:05,000 --> 00:15:09,000
 He didn't see contact as positive in any way.

242
00:15:09,000 --> 00:15:12,000
 We normally think of contact as a great thing.

243
00:15:12,000 --> 00:15:17,000
 When you come in contact with pleasant sounds and sights,

244
00:15:17,000 --> 00:15:24,000
 tastes, smells, feelings, particularly feelings,

245
00:15:24,000 --> 00:15:27,000
 contact is all about feelings.

246
00:15:27,000 --> 00:15:29,810
 When you come in contact with something, it makes you happy

247
00:15:29,810 --> 00:15:30,000
.

248
00:15:30,000 --> 00:15:34,000
 We think of this as pleasure and that as pleasure,

249
00:15:34,000 --> 00:15:40,000
 or this pleasure, that pleasure as happiness.

250
00:15:40,000 --> 00:15:45,000
 So the Buddha really did see all of this as tortuous,

251
00:15:45,000 --> 00:15:50,000
 as a cause for all of our suffering.

252
00:15:50,000 --> 00:15:53,000
 And the real deep reason,

253
00:15:53,000 --> 00:15:56,000
 you can try to explain it philosophically like,

254
00:15:56,000 --> 00:16:00,000
 "Oh, because it changes," and so on.

255
00:16:00,000 --> 00:16:03,000
 But the deep reason is simply because it arises and ceases.

256
00:16:03,000 --> 00:16:06,000
 It's incessant.

257
00:16:06,000 --> 00:16:08,000
 This is what you'll see in your practice.

258
00:16:08,000 --> 00:16:11,000
 As you get to these later stages,

259
00:16:11,000 --> 00:16:13,000
 you'll see things arising and ceasing

260
00:16:13,000 --> 00:16:18,000
 and become equanimous towards them.

261
00:16:18,000 --> 00:16:20,000
 See that that's all it is.

262
00:16:20,000 --> 00:16:22,000
 It's a bunch of arising and ceasing.

263
00:16:22,000 --> 00:16:26,000
 It never stops.

264
00:16:26,000 --> 00:16:32,970
 You're incessantly hounded by experience and thereby

265
00:16:32,970 --> 00:16:36,000
 contact.

266
00:16:36,000 --> 00:16:39,000
 This is what causes one to let go.

267
00:16:39,000 --> 00:16:42,000
 And the point being that it only hurts us

268
00:16:42,000 --> 00:16:46,000
 because we have expectations and attachment to it

269
00:16:46,000 --> 00:16:53,000
 once we see it as this, as worthless,

270
00:16:53,000 --> 00:16:57,000
 then we can experience it just fine.

271
00:16:57,000 --> 00:17:01,000
 The incessant nature of it,

272
00:17:01,000 --> 00:17:06,000
 the hounding of experience is no longer hounding us

273
00:17:06,000 --> 00:17:12,000
 because we no longer are susceptible to it.

274
00:17:12,000 --> 00:17:18,000
 We're no longer enamored by it or caught up by it.

275
00:17:18,000 --> 00:17:21,000
 All contact should be seen clearly.

276
00:17:21,000 --> 00:17:31,000
 This is not A or the way to find happiness.

277
00:17:31,000 --> 00:17:33,000
 It's number two.

278
00:17:33,000 --> 00:17:39,000
 Number three is mental volition, mental volition.

279
00:17:39,000 --> 00:17:43,000
 What causes karma? Our intentions.

280
00:17:43,000 --> 00:17:47,000
 When we want to kill someone or want to hurt someone

281
00:17:47,000 --> 00:17:51,000
 or when we want to be happy, when we want food,

282
00:17:51,000 --> 00:18:00,000
 when we want sex, when we want romance, music, anything.

283
00:18:00,000 --> 00:18:04,000
 When we want to be free from suffering,

284
00:18:04,000 --> 00:18:08,000
 mental volition, when we incline towards something.

285
00:18:08,000 --> 00:18:12,000
 And the Buddha again saw this as wretched.

286
00:18:12,000 --> 00:18:16,420
 Even our volitions, our desires, our ambitions, wretched,

287
00:18:16,420 --> 00:18:19,000
 he saw them.

288
00:18:19,000 --> 00:18:24,000
 He said, imagine, here's another allegory, I think,

289
00:18:24,000 --> 00:18:27,000
 is imagine there was a charcoal pit,

290
00:18:27,000 --> 00:18:34,000
 a big pit with blazing hot embers, glowing coals.

291
00:18:34,000 --> 00:18:37,000
 Have you ever seen one of these pits where they put coals

292
00:18:37,000 --> 00:18:41,000
 in a pit of very hot?

293
00:18:41,000 --> 00:18:48,000
 And then a man came along and then someone who didn't want

294
00:18:48,000 --> 00:18:48,000
 to die

295
00:18:48,000 --> 00:18:54,000
 desiring happiness and averse to suffering.

296
00:18:54,000 --> 00:18:59,000
 Then two strong men would grab him by one arm

297
00:18:59,000 --> 00:19:04,000
 and drag him towards the charcoal pit.

298
00:19:04,000 --> 00:19:08,000
 What do you think that man's volition would be?

299
00:19:08,000 --> 00:19:12,000
 That man's volition would be to get far away,

300
00:19:12,000 --> 00:19:14,000
 his longing would be to get far away.

301
00:19:14,000 --> 00:19:19,540
 He would wish, his wish would be to get far away from the

302
00:19:19,540 --> 00:19:22,000
 charcoal pit.

303
00:19:22,000 --> 00:19:25,000
 And he said, this is how you should, for what reason?

304
00:19:25,000 --> 00:19:28,000
 He says, because if I fall into this charcoal pit, anyway,

305
00:19:28,000 --> 00:19:34,000
 he says, this is how you should see mental volition.

306
00:19:34,000 --> 00:19:37,000
 It's the most wretched, you think of this as wretched,

307
00:19:37,000 --> 00:19:42,000
 this man wants something so bad, right?

308
00:19:42,000 --> 00:19:46,000
 And in fact, the funny thing is it's hard to believe

309
00:19:46,000 --> 00:19:52,000
 if you've never really studied or of course practiced

310
00:19:52,000 --> 00:19:55,780
 in Buddha's teaching, but it's not actually the charcoal

311
00:19:55,780 --> 00:19:56,000
 pit

312
00:19:56,000 --> 00:19:58,000
 that's going to make him suffer.

313
00:19:58,000 --> 00:20:01,000
 If he was okay about being dragged somehow,

314
00:20:01,000 --> 00:20:06,000
 amazingly okay with being dragged towards the charcoal pit

315
00:20:06,000 --> 00:20:12,320
 and okay with having his skin burnt up and okay with the

316
00:20:12,320 --> 00:20:13,000
 pain

317
00:20:13,000 --> 00:20:17,000
 and okay with the seizures and whatever else would happen,

318
00:20:17,000 --> 00:20:23,000
 okay with dying, then he wouldn't suffer.

319
00:20:23,000 --> 00:20:27,000
 But it's a great example of how, it's an extreme example

320
00:20:27,000 --> 00:20:30,000
 of how volition causes such great suffering.

321
00:20:30,000 --> 00:20:35,000
 I mean, if you wake up, if they were to knock him out

322
00:20:35,000 --> 00:20:38,000
 and throw him in the charcoal pit and then he wakes up in

323
00:20:38,000 --> 00:20:38,000
 it,

324
00:20:38,000 --> 00:20:41,000
 he would suffer less, right?

325
00:20:41,000 --> 00:20:44,000
 Could you imagine being dragged towards the charcoal pit?

326
00:20:44,000 --> 00:20:47,000
 How much suffering even before he gets there?

327
00:20:47,000 --> 00:20:52,000
 The anguish, the horror, right?

328
00:20:52,000 --> 00:20:54,000
 And he's not even there yet.

329
00:20:54,000 --> 00:20:56,000
 It's like, why are you suffering so much?

330
00:20:56,000 --> 00:21:01,000
 You're not even feeling it yet because of volition.

331
00:21:01,000 --> 00:21:03,000
 And he said, this is how we should see volition,

332
00:21:03,000 --> 00:21:07,000
 because it's all just trying to escape suffering

333
00:21:07,000 --> 00:21:10,000
 and find happiness where we run around like chickens

334
00:21:10,000 --> 00:21:17,000
 with our heads cut off looking for satisfaction.

335
00:21:17,000 --> 00:21:20,000
 And we heard ourselves and we heard other people

336
00:21:20,000 --> 00:21:23,000
 and we cause all, I mean look at all the problems

337
00:21:23,000 --> 00:21:25,000
 in society and the world.

338
00:21:25,000 --> 00:21:29,000
 How many of them are caused by this greed and ambition

339
00:21:29,000 --> 00:21:35,000
 and our conflicting desires?

340
00:21:35,000 --> 00:21:38,000
 So much stress and suffering.

341
00:21:38,000 --> 00:21:42,000
 Think of all the stress and suffering in this world.

342
00:21:42,000 --> 00:21:44,000
 Now all of you come here and you tell me,

343
00:21:44,000 --> 00:21:50,000
 we talk about the stress and suffering that you have.

344
00:21:50,000 --> 00:21:53,140
 It gives you perspective to think that the world is full of

345
00:21:53,140 --> 00:21:54,000
 people like us

346
00:21:54,000 --> 00:22:04,000
 suffering, tortured, wretched.

347
00:22:04,000 --> 00:22:06,000
 Yeah, well, and if you don't like this, you're welcome.

348
00:22:06,000 --> 00:22:09,000
 There's lots of other YouTube stuff out there.

349
00:22:09,000 --> 00:22:14,000
 And for those of you staying with me, well,

350
00:22:14,000 --> 00:22:17,000
 I think you kind of get what I'm saying.

351
00:22:17,000 --> 00:22:19,000
 This is deep stuff.

352
00:22:19,000 --> 00:22:23,000
 It's hard to swallow, I think.

353
00:22:23,000 --> 00:22:24,910
 If you've never really practiced, it just sounds

354
00:22:24,910 --> 00:22:26,000
 pessimistic.

355
00:22:26,000 --> 00:22:28,000
 It's really not.

356
00:22:28,000 --> 00:22:32,000
 It's realistic and it's optimistic

357
00:22:32,000 --> 00:22:37,000
 because it talks about how to find real happiness.

358
00:22:37,000 --> 00:22:42,000
 But I wasn't afraid of calling it like it is.

359
00:22:42,000 --> 00:22:47,000
 How this would be received by the world,

360
00:22:47,000 --> 00:22:52,000
 how it will be received tonight by YouTube, I don't know.

361
00:22:52,000 --> 00:22:54,000
 It doesn't really matter.

362
00:22:54,000 --> 00:22:58,000
 Truth is the truth.

363
00:22:58,000 --> 00:23:01,000
 The fourth one is consciousness.

364
00:23:01,000 --> 00:23:03,000
 And he says, how should you understand consciousness?

365
00:23:03,000 --> 00:23:06,000
 Yeah, it's not any better.

366
00:23:06,000 --> 00:23:08,000
 So we have consciousness.

367
00:23:08,000 --> 00:23:10,000
 We're always conscious, right?

368
00:23:10,000 --> 00:23:13,000
 Maybe except when we're sleeping

369
00:23:13,000 --> 00:23:18,000
 or in between experiences maybe.

370
00:23:18,000 --> 00:23:22,000
 But consciousness, how should we see consciousness?

371
00:23:22,000 --> 00:23:28,000
 He says, well, suppose there were a bandit,

372
00:23:28,000 --> 00:23:32,000
 a criminal, and they brought him before the king

373
00:23:32,000 --> 00:23:35,000
 and said, this man here is a bandit, a criminal.

374
00:23:35,000 --> 00:23:39,010
 He says, they say, "King, tell us what his punishment

375
00:23:39,010 --> 00:23:41,000
 should be."

376
00:23:41,000 --> 00:23:45,000
 And the king says, "Well, go and go,

377
00:23:45,000 --> 00:23:53,000
 and in the morning strike him with a hundred spears."

378
00:23:53,000 --> 00:23:56,000
 All morning, a hundred spears.

379
00:23:56,000 --> 00:23:57,000
 I don't know what it means.

380
00:23:57,000 --> 00:24:00,680
 Maybe poke him with the tip of it, bleed a little bit, a

381
00:24:00,680 --> 00:24:04,000
 hundred times.

382
00:24:04,000 --> 00:24:06,600
 So in the morning they go and they strike him with a

383
00:24:06,600 --> 00:24:07,000
 hundred,

384
00:24:07,000 --> 00:24:10,000
 they poke him with a hundred times.

385
00:24:10,000 --> 00:24:15,000
 And the king asks at noon, "Men, how's that man?"

386
00:24:15,000 --> 00:24:20,000
 And they say, "Still alive, sire."

387
00:24:20,000 --> 00:24:22,000
 Remarkably.

388
00:24:22,000 --> 00:24:25,000
 And then he says, "Then go at noon now

389
00:24:25,000 --> 00:24:28,000
 and strike him with a hundred spears."

390
00:24:28,000 --> 00:24:31,000
 And so they strike him with a hundred spears at noon,

391
00:24:31,000 --> 00:24:34,000
 and they pierce him with a hundred spears.

392
00:24:34,000 --> 00:24:39,190
 In the evening he asks again, "How is that man still alive,

393
00:24:39,190 --> 00:24:40,000
 sire?"

394
00:24:40,000 --> 00:24:42,690
 And he says, "Then go in the evening and strike him with

395
00:24:42,690 --> 00:24:45,000
 another hundred spears."

396
00:24:45,000 --> 00:24:46,720
 And so in the evening they strike him with another hundred

397
00:24:46,720 --> 00:24:47,000
 spears.

398
00:24:47,000 --> 00:24:49,000
 And the Buddha says, "What do you think?

399
00:24:49,000 --> 00:24:51,000
 That man being struck with three hundred spears,

400
00:24:51,000 --> 00:24:56,000
 would he experience pain and displeasure on that account?"

401
00:24:56,000 --> 00:24:59,810
 And they're like, "Venerable sir, even if he were struck

402
00:24:59,810 --> 00:25:00,000
 with one spear,

403
00:25:00,000 --> 00:25:03,000
 he would experience pain and displeasure on that account,

404
00:25:03,000 --> 00:25:06,000
 not to speak of three hundred spears."

405
00:25:06,000 --> 00:25:08,500
 The Buddha said, "In this way the nutriment of

406
00:25:08,500 --> 00:25:10,000
 consciousness should be understood,

407
00:25:10,000 --> 00:25:13,000
 should be seen."

408
00:25:13,000 --> 00:25:15,000
 Wretched.

409
00:25:15,000 --> 00:25:20,000
 Again, all consciousness.

410
00:25:20,000 --> 00:25:22,000
 All consciousness.

411
00:25:22,000 --> 00:25:26,000
 You see the incessant arising and ceasing of it.

412
00:25:26,000 --> 00:25:29,980
 You see that consciousness is just like a dart, it's an

413
00:25:29,980 --> 00:25:31,000
 invasion.

414
00:25:31,000 --> 00:25:33,000
 How hard is that to see?

415
00:25:33,000 --> 00:25:38,000
 Consciousness is an invasion.

416
00:25:38,000 --> 00:25:39,000
 It's very hard to see.

417
00:25:39,000 --> 00:25:41,770
 The only way you could really appreciate that is if you'd

418
00:25:41,770 --> 00:25:43,000
 seen nirvana.

419
00:25:43,000 --> 00:25:45,000
 You'd seen something better.

420
00:25:45,000 --> 00:25:50,000
 Experienced something better.

421
00:25:50,000 --> 00:25:55,000
 Something better than consciousness.

422
00:25:55,000 --> 00:25:57,000
 I mean, you say unconsciousness.

423
00:25:57,000 --> 00:26:02,000
 Well, suppose, I mean, it's one way of describing it.

424
00:26:02,000 --> 00:26:04,000
 But it's not like sleep.

425
00:26:04,000 --> 00:26:08,000
 It's like freedom.

426
00:26:08,000 --> 00:26:09,000
 I don't know.

427
00:26:09,000 --> 00:26:10,000
 It's a very deep teaching.

428
00:26:10,000 --> 00:26:15,250
 I'm afraid it will be misunderstood or unappreciated by the

429
00:26:15,250 --> 00:26:17,000
 general audience.

430
00:26:17,000 --> 00:26:21,540
 But I think it's also important that we do at least come to

431
00:26:21,540 --> 00:26:22,000
 terms

432
00:26:22,000 --> 00:26:24,000
 and we don't lie to ourselves about what we're dealing with

433
00:26:24,000 --> 00:26:24,000
.

434
00:26:24,000 --> 00:26:27,000
 If you don't like it, then you have to...

435
00:26:27,000 --> 00:26:29,800
 I mean, the best thing about all this is if you don't like

436
00:26:29,800 --> 00:26:31,000
 it, you can disprove it.

437
00:26:31,000 --> 00:26:33,000
 Problem is, it's true.

438
00:26:33,000 --> 00:26:37,240
 So, it would be nice if samsara was a wonderful place and

439
00:26:37,240 --> 00:26:39,000
 if we could work this out

440
00:26:39,000 --> 00:26:41,960
 so we all live together in harmony for the rest of eternity

441
00:26:41,960 --> 00:26:42,000
.

442
00:26:42,000 --> 00:26:45,810
 But, you know, those religions that teach people about this

443
00:26:45,810 --> 00:26:47,000
 eternal heaven

444
00:26:47,000 --> 00:26:51,000
 where you sing hallelujah forever, it's a nice thought.

445
00:26:51,000 --> 00:26:57,000
 It's a wonderful dream, but it's not reality.

446
00:26:57,000 --> 00:27:01,000
 But the great thing is, we can be free.

447
00:27:01,000 --> 00:27:04,000
 We can find peace.

448
00:27:04,000 --> 00:27:07,170
 I mean, whether you like this or not, ask yourself if you

449
00:27:07,170 --> 00:27:08,000
're happy.

450
00:27:08,000 --> 00:27:11,300
 I mean, the reason people come to me is because they're

451
00:27:11,300 --> 00:27:13,000
 generally not happy.

452
00:27:13,000 --> 00:27:15,000
 Some are fairly happy.

453
00:27:15,000 --> 00:27:19,000
 It's not all the case, but people come because they see.

454
00:27:19,000 --> 00:27:20,000
 They want to improve themselves.

455
00:27:20,000 --> 00:27:22,000
 I mean, the best people.

456
00:27:22,000 --> 00:27:24,000
 Some people come thinking they're just going to have fun

457
00:27:24,000 --> 00:27:25,000
 and that's problem

458
00:27:25,000 --> 00:27:29,400
 because they don't see the challenges and the problems and

459
00:27:29,400 --> 00:27:33,000
 the danger of clinging.

460
00:27:33,000 --> 00:27:36,000
 The best people, the ones who really get from this,

461
00:27:36,000 --> 00:27:42,000
 are the ones who need help and see that they need help.

462
00:27:42,000 --> 00:27:44,000
 Because they're able to find help.

463
00:27:44,000 --> 00:27:48,000
 They're able to help themselves.

464
00:27:48,000 --> 00:27:54,000
 So, there you go, a little food for thought.

465
00:27:54,000 --> 00:27:57,740
 There's the dhamma for tonight. Thank you all for tuning in

466
00:27:57,740 --> 00:27:58,000
.

467
00:27:58,000 --> 00:28:15,000
 Thank you all for coming.

468
00:28:15,000 --> 00:28:35,000
 So, do we have questions?

469
00:28:35,000 --> 00:28:45,000
 I can't even log in.

470
00:28:45,000 --> 00:28:48,000
 Okay, I'm going to save the questions for another day.

471
00:28:48,000 --> 00:28:51,000
 Thank you all for coming out. Have a good night.

