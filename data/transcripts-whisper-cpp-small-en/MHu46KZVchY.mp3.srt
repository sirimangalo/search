1
00:00:00,000 --> 00:00:07,000
 Yeah, sorry, I got disconnected there.

2
00:00:07,000 --> 00:00:18,410
 So there's, sorry, the, the, what I've said before is that

3
00:00:18,410 --> 00:00:19,600
 two halves don't always make

4
00:00:19,600 --> 00:00:22,380
 a whole.

5
00:00:22,380 --> 00:00:30,700
 You can't take two half persons and make them into a whole

6
00:00:30,700 --> 00:00:31,500
 person.

7
00:00:31,500 --> 00:00:35,100
 And this, there was a story, there was a book about this,

8
00:00:35,100 --> 00:00:36,880
 one of those New Age books that

9
00:00:36,880 --> 00:00:42,900
 talked about this, the Celestine Prophecy, I think, brought

10
00:00:42,900 --> 00:00:43,600
 this up.

11
00:00:43,600 --> 00:00:46,880
 And it talks about people who grasp for other people as

12
00:00:46,880 --> 00:00:49,160
 being half persons, not being whole

13
00:00:49,160 --> 00:00:51,790
 in and of themselves, which is really an interesting

14
00:00:51,790 --> 00:00:52,600
 philosophy.

15
00:00:52,600 --> 00:00:57,170
 And it mirrors, I think, Buddhism quite well, that you can

16
00:00:57,170 --> 00:01:00,120
't expect to find happiness outside

17
00:01:00,120 --> 00:01:01,120
 of yourself.

18
00:01:01,120 --> 00:01:03,800
 This is something that we're being, we're being lied to.

19
00:01:03,800 --> 00:01:07,440
 We're being taught things that are false.

20
00:01:07,440 --> 00:01:12,200
 The idea that happiness is going to come from, from other

21
00:01:12,200 --> 00:01:15,040
 people, that, that true happiness

22
00:01:15,040 --> 00:01:20,920
 is because the idea of a person is something, is something

23
00:01:20,920 --> 00:01:23,400
 totally conceptual.

24
00:01:23,400 --> 00:01:26,040
 All we have really is our experiences of seeing, hearing,

25
00:01:26,040 --> 00:01:28,300
 smelling, tasting, feeling and thinking.

26
00:01:28,300 --> 00:01:32,020
 We come up with the idea of the other person in our own

27
00:01:32,020 --> 00:01:32,800
 minds.

28
00:01:32,800 --> 00:01:40,280
 It's a, it's an illusion that we, we give rise to.

29
00:01:40,280 --> 00:01:54,160
 And based on that, based on this fact, it, it, you know, it

30
00:01:54,160 --> 00:01:54,960
 leads to all sorts of clinging

31
00:01:54,960 --> 00:02:04,320
 and it can only in the end lead to disappointment and, and

32
00:02:04,320 --> 00:02:07,760
 dissatisfaction.

33
00:02:07,760 --> 00:02:11,330
 Because when, when the truth changes, you know, when that

34
00:02:11,330 --> 00:02:12,960
 person dies or gets old or,

35
00:02:12,960 --> 00:02:17,960
 or when reality becomes different from your illusion, from

36
00:02:17,960 --> 00:02:20,600
 your, your concept, which it

37
00:02:20,600 --> 00:02:24,160
 has to do, then you will suffer.

38
00:02:24,160 --> 00:02:27,460
 You will be upset and disappointed.

39
00:02:27,460 --> 00:02:29,920
 It's bound up with attachment, really.

40
00:02:29,920 --> 00:02:32,800
 That's, that's, that's really the point.

41
00:02:32,800 --> 00:02:38,680
 You attach to something and you say, this is preferable to

42
00:02:38,680 --> 00:02:40,660
 something else.

43
00:02:40,660 --> 00:02:43,580
 You can philosophize about it all you want and say that,

44
00:02:43,580 --> 00:02:45,100
 you know, we need to find our,

45
00:02:45,100 --> 00:02:49,630
 you can use words like life partners or, you know, all the

46
00:02:49,630 --> 00:02:53,760
 sorts of words that you're using.

47
00:02:53,760 --> 00:02:57,840
 And, and it, it sort of makes it seem like there's some

48
00:02:57,840 --> 00:02:59,680
 meaning to it, right?

49
00:02:59,680 --> 00:03:02,300
 Or there's some importance to it.

50
00:03:02,300 --> 00:03:04,240
 But actually all that it is, is a craving.

51
00:03:04,240 --> 00:03:05,240
 It's an attachment.

52
00:03:05,240 --> 00:03:07,480
 It's an attachment to pleasure.

53
00:03:07,480 --> 00:03:12,080
 And there's many different, different causes of that

54
00:03:12,080 --> 00:03:13,200
 pleasure.

55
00:03:13,200 --> 00:03:17,190
 You know, the pleasure of just having someone to support

56
00:03:17,190 --> 00:03:19,220
 you because you feel weak, because

57
00:03:19,220 --> 00:03:21,520
 you're not strong in and of yourself.

58
00:03:21,520 --> 00:03:27,750
 The pleasure of physical contact, the pleasure of, of

59
00:03:27,750 --> 00:03:30,120
 safety, the pleasure that comes from

60
00:03:30,120 --> 00:03:39,240
 from conversation, from interacting with other people.

61
00:03:39,240 --> 00:03:42,720
 It's something that, that we enjoy.

62
00:03:42,720 --> 00:03:48,080
 And because we enjoy it, we cling to it, we want it more

63
00:03:48,080 --> 00:03:51,120
 and we wind up expecting it.

64
00:03:51,120 --> 00:03:56,770
 And the workings of the brain are a really good indicator

65
00:03:56,770 --> 00:03:59,080
 of the truth of this.

66
00:03:59,080 --> 00:04:02,240
 Because if you look at addiction research, as I've talked

67
00:04:02,240 --> 00:04:03,800
 about before, you can see how

68
00:04:03,800 --> 00:04:07,200
 this process is working in the brain.

69
00:04:07,200 --> 00:04:12,470
 The chemicals in the brain are released and they attach to

70
00:04:12,470 --> 00:04:15,360
 these receptors and therefore

71
00:04:15,360 --> 00:04:18,520
 it gives pleasure.

72
00:04:18,520 --> 00:04:21,700
 And that's great, except the next time the receptors have

73
00:04:21,700 --> 00:04:23,240
 been weakened or have been

74
00:04:23,240 --> 00:04:26,440
 broken or it's like it's been overstretched.

75
00:04:26,440 --> 00:04:28,720
 The system has been overtaxed.

76
00:04:28,720 --> 00:04:33,400
 And as a result, it requires more chemicals than last time.

77
00:04:33,400 --> 00:04:39,920
 So more stimulus than last time.

78
00:04:39,920 --> 00:04:46,490
 It's really, from a Buddhist point of view, it's because of

79
00:04:46,490 --> 00:04:49,320
 the accumulated force.

80
00:04:49,320 --> 00:04:53,280
 You know, the, the addiction stacks on top of each other

81
00:04:53,280 --> 00:04:55,600
 and it becomes worse and worse.

82
00:04:55,600 --> 00:04:59,110
 So when you have this much addiction and you go for it and

83
00:04:59,110 --> 00:05:01,320
 you, you chase after the things

84
00:05:01,320 --> 00:05:04,520
 that you're attached to, you're stacking on more addiction.

85
00:05:04,520 --> 00:05:08,010
 And because you have now this much addiction, you need more

86
00:05:08,010 --> 00:05:09,920
, it needs more food, right?

87
00:05:09,920 --> 00:05:12,000
 It's like this monster is getting bigger.

88
00:05:12,000 --> 00:05:16,880
 And when the monster gets bigger, it needs more food.

89
00:05:16,880 --> 00:05:20,200
 It's the power of the, the addiction and the habit.

90
00:05:20,200 --> 00:05:25,800
 You're creating a stronger habit, a stronger addiction.

91
00:05:25,800 --> 00:05:28,950
 And you can see how that works in the brain, but it's

92
00:05:28,950 --> 00:05:31,200
 exactly how it works in the mind.

93
00:05:31,200 --> 00:05:36,780
 So when your happiness is based on these sorts of things

94
00:05:36,780 --> 00:05:40,340
 and really anything external to

95
00:05:40,340 --> 00:05:47,430
 yourself or anything external, any object is, is only going

96
00:05:47,430 --> 00:05:51,040
 to, the only happiness it

97
00:05:51,040 --> 00:05:53,560
 can bring you is one that is rooted in attachment, rooted

98
00:05:53,560 --> 00:05:54,360
 in addiction.

99
00:05:54,360 --> 00:05:59,440
 And as a result, it can never satisfy you.

100
00:05:59,440 --> 00:06:01,580
 So that, that's part of what you're, hopefully addresses

101
00:06:01,580 --> 00:06:02,800
 part of what you're asking.

102
00:06:02,800 --> 00:06:06,250
 But, but I think the other important thing to mention here

103
00:06:06,250 --> 00:06:08,320
 is in regards to your example,

104
00:06:08,320 --> 00:06:10,930
 that there are a lot of people out there who want to have a

105
00:06:10,930 --> 00:06:12,440
 partner, but are suffering

106
00:06:12,440 --> 00:06:15,440
 because they cannot find one.

107
00:06:15,440 --> 00:06:20,500
 And this is a mistake that we often make the, the, the, the

108
00:06:20,500 --> 00:06:23,060
 making the wrong connection

109
00:06:23,060 --> 00:06:27,960
 that we are suffering because.

110
00:06:27,960 --> 00:06:32,120
 Thank you.

111
00:06:32,120 --> 00:06:38,760
 Give to the woman.

112
00:06:38,760 --> 00:06:42,280
 No, she needs.

113
00:06:42,280 --> 00:06:43,280
 Flashlights came.

114
00:06:43,280 --> 00:06:49,500
 Sorry, I'll get to your question in a second just to finish

115
00:06:49,500 --> 00:06:51,080
 up this one.

116
00:06:51,080 --> 00:06:56,210
 So we say we're suffering because we don't get what we want

117
00:06:56,210 --> 00:06:56,640
.

118
00:06:56,640 --> 00:07:01,750
 And the Buddha said himself, yam pi chang nala bhati tampi

119
00:07:01,750 --> 00:07:04,640
 du, yam pi chang nala bhati tampi

120
00:07:04,640 --> 00:07:06,680
 du kang.

121
00:07:06,680 --> 00:07:11,980
 When you, whatever you want, not getting that, that is

122
00:07:11,980 --> 00:07:13,400
 suffering.

123
00:07:13,400 --> 00:07:16,520
 So we think, well, yeah, then how do you get what you want?

124
00:07:16,520 --> 00:07:17,520
 Right?

125
00:07:17,520 --> 00:07:19,360
 What can we do to get what we want?

126
00:07:19,360 --> 00:07:23,980
 Because not getting it is managed or is suffering.

127
00:07:23,980 --> 00:07:27,320
 But the point here is not that you're not getting what you

128
00:07:27,320 --> 00:07:27,920
 want.

129
00:07:27,920 --> 00:07:34,630
 The point here is that you want what the Buddha was saying

130
00:07:34,630 --> 00:07:38,120
 actually is, is, is that wanting

131
00:07:38,120 --> 00:07:42,560
 things sets you up for suffering, suffering because when,

132
00:07:42,560 --> 00:07:44,560
 when you don't get what you

133
00:07:44,560 --> 00:07:46,840
 want, you suffer.

134
00:07:46,840 --> 00:07:51,190
 So he was kind of, you know, I think it's quite clear that

135
00:07:51,190 --> 00:07:53,640
 he was taking it for granted.

136
00:07:53,640 --> 00:07:58,280
 The understanding that you can't always get what you want.

137
00:07:58,280 --> 00:08:01,350
 Now it was kind of like, well, hopefully you don't believe

138
00:08:01,350 --> 00:08:03,080
 that you can get what you always

139
00:08:03,080 --> 00:08:04,080
 get what you want.

140
00:08:04,080 --> 00:08:07,450
 And given that you don't believe that you can always get

141
00:08:07,450 --> 00:08:09,320
 what you want, then wanting

142
00:08:09,320 --> 00:08:11,280
 is going to set you up for suffering.

143
00:08:11,280 --> 00:08:16,450
 And that's why the second noble truth is, is the cause of

144
00:08:16,450 --> 00:08:19,200
 suffering is, is craving or

145
00:08:19,200 --> 00:08:23,240
 desire.

146
00:08:23,240 --> 00:08:25,930
 Because and this is really the key of the Buddha's teaching

147
00:08:25,930 --> 00:08:26,180
.

148
00:08:26,180 --> 00:08:28,480
 If you know, if you learn about Buddhism and what the

149
00:08:28,480 --> 00:08:30,080
 Buddha taught, you'll learn that

150
00:08:30,080 --> 00:08:34,460
 the second noble truth is the cause of suffering is, is

151
00:08:34,460 --> 00:08:35,480
 craving.

152
00:08:35,480 --> 00:08:40,570
 So it's not that people are not suffering because they can

153
00:08:40,570 --> 00:08:42,440
't find a partner.

154
00:08:42,440 --> 00:08:45,000
 They're suffering because they want to find a partner in

155
00:08:45,000 --> 00:08:45,920
 the first place.

156
00:08:45,920 --> 00:08:49,420
 And what it translates, what it means, the essence of it is

157
00:08:49,420 --> 00:08:50,960
 they're not content with

158
00:08:50,960 --> 00:08:51,960
 their lives.

159
00:08:51,960 --> 00:08:53,520
 They're not content with reality.

160
00:08:53,520 --> 00:08:54,760
 They're not balanced.

161
00:08:54,760 --> 00:08:57,000
 They're not here and now.

162
00:08:57,000 --> 00:09:00,820
 They're not, they're not in tune with things as they are

163
00:09:00,820 --> 00:09:01,560
 there.

164
00:09:01,560 --> 00:09:04,380
 They don't have the understanding yet of the reality around

165
00:09:04,380 --> 00:09:05,960
 them, which, you know, I mean,

166
00:09:05,960 --> 00:09:08,160
 it's not to say they're bad people.

167
00:09:08,160 --> 00:09:10,400
 This is the state that we're all in.

168
00:09:10,400 --> 00:09:13,150
 So I'm not trying to say that, though, these people are

169
00:09:13,150 --> 00:09:14,840
 inferior in some way, that's, this

170
00:09:14,840 --> 00:09:16,580
 is how we are.

171
00:09:16,580 --> 00:09:20,190
 What I'm trying to do is sort of flush out what is the path

172
00:09:20,190 --> 00:09:21,780
 that we should take.

173
00:09:21,780 --> 00:09:26,110
 We can either try to find a way so that we always get what

174
00:09:26,110 --> 00:09:28,080
 we want, or we can try to

175
00:09:28,080 --> 00:09:31,600
 find a way so that we have no more wants, no more wanting.

176
00:09:31,600 --> 00:09:34,160
 I mean, they're both, it's both the same goal.

177
00:09:34,160 --> 00:09:37,580
 The goal in mind is to not have any more wanting either.

178
00:09:37,580 --> 00:09:41,220
 You get everything that you want and are totally satisfied,

179
00:09:41,220 --> 00:09:43,440
 or you give up wanting and are

180
00:09:43,440 --> 00:09:46,260
 in the same way, totally satisfied.

181
00:09:46,260 --> 00:09:50,850
 But as I've said, the problem is that it may sound logical

182
00:09:50,850 --> 00:09:53,280
 that way, but it's reality is

183
00:09:53,280 --> 00:09:54,640
 something totally different.

184
00:09:54,640 --> 00:09:59,060
 You can't become satisfied by always getting what you want.

185
00:09:59,060 --> 00:10:00,160
 It's impossible.

186
00:10:00,160 --> 00:10:03,310
 You're developing the habit of craving, the habit of

187
00:10:03,310 --> 00:10:04,160
 addiction.

188
00:10:04,160 --> 00:10:09,350
 If you go the other way and develop the habit, the way of

189
00:10:09,350 --> 00:10:13,400
 contentment, then you'll be developing

190
00:10:13,400 --> 00:10:16,800
 more and more contentment until eventually you're content

191
00:10:16,800 --> 00:10:18,040
 with everything.

192
00:10:18,040 --> 00:10:20,360
 As a result, you have no more wanting.

193
00:10:20,360 --> 00:10:21,800
 That way actually works.

194
00:10:21,800 --> 00:10:23,640
 They're not parallel and they don't both work.

195
00:10:23,640 --> 00:10:33,640
 [BLANK_AUDIO]

