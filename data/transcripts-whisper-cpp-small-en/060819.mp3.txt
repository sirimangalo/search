 Today we continue talking about the 38 Blessings of the
 Lord Buddha.
 We'll finish talking about 6 and then we'll go on to 7 to
 10.
 The next stanza goes, "Bahu satchan chasipan chawinayo chas
usikit o, tupasita chayawacha nyutama kalamata."
 "Bahu satcha" who have great knowledge, who have studied a
 lot.
 Sipan chasipan chawinayo chasusikit o, tupasita chayawacha
 nyutama kalamata.
 To be proficient in some sort of handicraft or in skill.
 To be proficient in the practice or in the performance of
 what one has learned.
 "Bhuinayo chasusikit o," to have a discipline which is well
 trained, to be well trained
 in discipline.
 "Tupasita chayawacha" Whatever speech is well spoken.
 "Ithamaan kalamuthamaan," these are blessings in the
 highest sense.
 "Bahu satcha" to have great knowledge and learning.
 Knowledge and learning, on a basic level it means having
 studied, having either listened
 to many talks on the Dhamma or had many teachers teaching
 about the Dhamma, teaching about
 the truth.
 Of course it could mean even in a worldly sense, it could
 mean nothing to do with the teaching
 of the Lord Buddha.
 Here we understand it to mean the teaching of the Lord
 Buddha but actually it could even
 be considered a blessing just to have learned a lot in a
 worldly sense.
 Knowledge is power.
 Whatever realm we work in, we work in business or we work
 in education, we work in any field
 in the world, we find that the more we have studied what we
 are doing, the greater will
 be our power and our ability to carry out our work.
 In the Dhamma it's the same.
 Of course even much more of a blessing because the Dhamma
 is that which keeps us from falling
 into evil, falling into suffering.
 When we have great knowledge or learning about the Dhamma,
 for sure we can much better put
 it into practice.
 If we learn all 38 of the blessings of the Lord Buddha, we
'll be much better off to put
 them all into practice.
 Rather than we know one or two, we only know the very
 basics of the Lord Buddha's teaching.
 We learn a lot about the Buddha's teaching.
 It would be much easier for us to put it all into practice
 because we understand it on
 a more broad level.
 It's a great blessing to have studied.
 With study you can also apply it according to the teachers,
 according to the teachings
 on great knowledge, great learning.
 It can also apply to practice and it can also apply to the
 results one gets from practice.
 Here we can find it only through book learning or through
 listening to what other people
 have to say.
 But really it can also mean because the knowledge which we
 gain from books and from hearing
 is really only very superficial knowledge.
 It doesn't constitute real understanding or real learning
 of a subject.
 So we put ourselves to the task.
 It really can't be expected to bring us any great fruit.
 So we'll combine it to this because the next one, sipang
 means handicraft or the ability
 to do the work.
 This applies more to the realm of practice.
 We need to have the ability to put into practice whatever
 we've learned.
 In the time of the Lord Buddha there were many stories of
 people who had studied but
 had never put into practice what they had learned.
 One story is the story of empty book.
 The Lord Buddha called him empty book.
 His name was Poteela which meant the form of a book in
 those times or a palm leaf, a
 manuscript, something used to write on.
 He had studied the whole of the Buddha's teaching, memor
ized the entire teaching of the Lord
 Buddha.
 When he went to see the Lord Buddha he thought that of
 course the Lord Buddha would praise
 him for his attainment.
 He had 500 students or 1000 students and he thought that
 the Lord Buddha would for sure
 have something good to say about him.
 So he went to see the Lord Buddha, pay respect to the Lord
 Buddha and the Lord Buddha saw
 him coming and said, "Oh here comes a good old empty book.
 Oh there goes empty book.
 Oh empty book sitting down.
 Empty book is getting up."
 They called him by the name empty book and Poteela was very
 ashamed to hear this from
 the Lord Buddha.
 He realized that it was because he had studied but never
 practiced.
 That the study which he had gained was really nothing.
 It was not something that really meant anything.
 It was still an empty book.
 So he went off and tried to find a teacher to teach him but
 no one would teach him until
 finally he found a novice, a seven year old novice who
 taught him meditation.
 Because no one else would dare to teach him.
 It would be quite difficult to teach because he knew
 everything already.
 People who know everything are very difficult to teach.
 The novice had a way of breaking his attachment to his
 learning and in the end he did attain
 to become an arahant through the practice of meditation.
 The Sipancha means the ability to practice, the skill in
 practice.
 In the world it means being skilled in a trade, skilled in
 a handicraft, skilled in whatever
 field one applies oneself to.
 This is a great blessing in the world.
 Something which is highly prized, someone who is skilled,
 someone who has a skill in
 what they do.
 It's a blessing, it's something you can take anywhere with
 us.
 If you have a business, you can't take the business with
 you but if you have a skill
 you can take it anywhere.
 Whatever skills that we have we can take them anywhere with
 us.
 But this is a great blessing, something we should all
 strive for.
 If we live in the world we should strive to have some skill
 which is well trained.
 It can be a blessing.
 In the meditation practice, in the Buddhist teaching of
 course, it means the skill in
 meditation practice.
 We should have skill in attaining to the states of calm
 which are conducive for insight meditation
 insight realization.
 If we really want to be skillful we should be skillful at
 attaining the magical powers,
 reading people's minds, remembering past lives, lying
 through the air, shape changing and
 so on.
 The divine eye, the divine ear, all these see things far
 away, hear things far away
 and so on.
 But whether we attain to those magical powers, magical
 states or not, most important is we
 become skillful in seeing the three characteristics.
 Become skillful in watching the rising and falling,
 watching the feelings, watching the
 mind, watching the mirize and seeing that all the things
 around us which used to make
 us so upset, they're really only body and mind, they're
 impermanent, they're unsatisfying
 and they're not under our control and to let them go.
 Gain skill in attaining to the complete freedom from
 suffering.
 Skill in attaining what we call peaceful cessation, where
 the mind lets go, where the mind is
 free, where the mind doesn't fall into suffering.
 So skill in, talking about skill in attaining, skill in
 practice, being able to put into
 practice what we've learned.
 Number nine is a well-trained discipline.
 Discipline is, as I always say, the most important
 foundation of the practice.
 It's a great blessing to be skilled, to be well-trained in
 discipline.
 As you even see it in the world among people who haven't
 practiced Buddhism, they have
 a rudimentary sort of discipline.
 You see it among soldiers.
 I even saw it once among an ordinary person in the world.
 There was a woman who was from Cambodia and she was
 Buddhist, so you can describe it somewhat
 to her Buddhist background, but I'd never seen anyone like
 her.
 She had a teacher and she brought her teacher who was a
 Westerner to come to see us in the
 monastery.
 And her teacher was not very polite.
 Her teacher was not mean, but she was somewhat demanding.
 Her discipline was not very strong, but the student who
 brought her teacher with her to
 see us in the monastery was probably the most well-discipl
ined person I'd ever met, maybe
 ever have met still.
 She had some sort of training in manners and she was, "Yes,
 Miss, no, Miss, can I help
 you, Miss?"
 Someone who was very well-trained, very disciplined and
 would not show any sort of anger or upset.
 She'd sit the way she sat, the way she walked.
 There was someone you could see who had received great
 training and discipline.
 Discipline means to have body and to have speech which is
 well refined, which is well-trained.
 Some people on the other hand are always moving, always f
idgeting about, always saying useless
 things, always talking about, and the way they talk is not
 trained or not polite or disciplined.
 In the practice of Buddhism, of course, it takes on a more
 specific meaning, a meaning
 of refraining from evil deeds with the body and with the
 speech.
 Because a person can be disciplined and still be able to do
 evil deeds.
 For instance, soldiers or police officers or even thieves
 and villains are able to have
 certain type of discipline.
 When it comes to refraining from evil deeds, they might not
 hesitate.
 So when we mean discipline in the Buddhist teaching, we're
 talking about refraining from
 deeds which are inappropriate.
 When we're lay people, we have to keep five precepts or
 eight precepts.
 These are rules which we have to train ourselves in for the
 purpose of the attainment of concentration.
 Morality is always, discipline is always for the purpose of
 attaining to concentration.
 For monks, we have so many rules.
 But the truth is when we keep all of these rules, we can
 realize that our concentration
 really improves.
 When this year I stopped to touch money, as a monk we're
 not supposed to be touching money,
 we're supposed to be buying things or involved with
 touching money.
 So I started to train myself in this and since I started
 training you can see clearly that
 one certainly does lead to concentration.
 It leads your mind to be fixed and composed.
 You don't have to be worrying or thinking about this or
 about that.
 All the things that surround you in the world, your mind
 becomes fixed and focused in one
 place, not restless or disturbed as before.
 But even in regards to all of the precepts of five, eight,
 or 227 rules that the monks
 have to keep, the most important, the more important
 discipline is the discipline which
 comes from the mind which is well trained.
 When the mind is not flitting here or flitting there or
 falling into bad or unwholesome states.
 The disciplined mind, when we talk about disciplining the
 body and the speech, well, people can
 force themselves to do that and still be evil people inside
.
 But the discipline of the mind is just the much more
 important thing.
 To have a mind which is well disciplined.
 We don't let our mind get angry or we don't let our mind
 become greedy or we don't let
 our mind become deluded.
 We come to see that these things are the cause of suffering
 or they're suffering themselves
 and they let go of them.
 This comes of course from the practice of meditation which
 we're all undertaking.
 This is the most important thing for us is always to push
 ourselves, to pull ourselves,
 to bring ourselves up to the level where we don't get angry
 even when people do bad things
 to us.
 We want to be better than them or we don't get angry at
 them.
 We send love to them, may they be happy, forgive them, may
 they be free from suffering.
 So on where our minds don't become upset or even attached
 to good things in the world.
 This is called Vinayo Chasusikit.
 I have a well trained discipline, a disciplined mind.
 And number 10, Supa Sita Yawata.
 Whatever words, whatever speech is well spoken.
 This is the greatest, this is a blessing in the greatest
 sense, the highest sense.
 Number 10 is whatever speech is well spoken.
 This is a blessing in the world is kind of funny because
 sometimes you meet people whose
 speech is so sweet.
 They're called the flatterer, someone who says always
 saying good.
 You hear them saying good things.
 And sometimes it happens that you meet someone who to your
 face they say so many nice good
 things.
 But if you're perceptive you know that right behind your
 back for sure they're not saying
 good things about you.
 You know why they're saying it?
 They're not saying it because they mean it or because of
 loving kindness which exists
 in their heart.
 They're saying it because they know when they say it.
 It's a way of ingratiating themselves with you.
 Sometimes you see people smiling but you know behind their
 smile they're really looking
 at you as though you're some kind of beast or so on.
 These kind of people what I mean is that sometimes people
 have sweet words and you think oh that's
 well said.
 But really the intentions behind their speech are not
 wholesome, not pure.
 For speech to really be truly well spoken has to come from
 a pure heart.
 For instance in the Buddha's time there was one monk who
 lived in the forest.
 And all his teaching was one story.
 He had heard many things but all he remembered he kept in
 mind only one story of the Buddha
 but he was so good at telling it that every time when he
 told the story when he came to
 the verse at the end and finished up the story that all of
 the angels and all of the beings
 living in the forest they would all say "sadhu, sadhu" and
 this great noise would ring through
 the forest.
 And this one day a monk came to visit him and he saw this
 and he said "oh, these people
 don't know good teaching when they hear it.
 I'll go up and give a talk."
 And he talked and talked and talked and talked and gave a
 long talk about many different
 things.
 There was a monk who had great learning, vast learning.
 And at the end he finished the talk and he sat and waited
 and there was no noise, he
 didn't hear anything.
 And the other monk said "yes, yes" and the other monk got
 up on the chair and just said
 his one verse, the end verse of the story and immediately
 the whole forest shook with
 sadhu.
 The monk who, because it was given from his heart.
 When we say things we have to be sure that our intentions
 are pure.
 We can't just say things to flatter other people.
 And we can't just be like a parrot repeating the words of
 other people.
 When we wish people good goodness for themselves we have to
 see do we really wish them goodness.
 When we teach the teaching of the Lord Buddha do we really
 mean it?
 Do we really understand the teaching?
 Otherwise we are going to just be like an empty book, like
 a parrot.
 Like the Lord Buddha said, someone who feeds cows, someone
 who looks at what do they call
 a shepherd or a cowherd, someone who looks after other
 people's cows.
 When you look after other people's cows you never get to
 possess the fruits of looking
 after the cow, the fruits of having the milk or having
 cheese or butter, the products which
 come from the cow.
 If the cows are not your cows, they are someone else's cows
.
 You will never get to taste the fruits that come from the
 cow.
 Buddha said in the same way even though someone knows a lot
, a lot of the teaching is able
 to teach it.
 If they haven't come to practice or understand the
 teachings they are just like someone who
 looks after other people's cows, they won't get the true
 flavor of the tambourine.
 For something to be well spoken it needs to be at the right
 time.
 Sometimes you say something good but it's at the wrong time
.
 It has to be spoken and it has to be the truth.
 Sometimes we speak nice things but it's not the truth.
 It has to be an appropriate thing to say.
 It can't be something useless or something which is unhelp
ful.
 It has to be said with love, with loving kindness in one's
 heart and so on.
 There are many factors which lead for something to be well
 spoken.
 This is considered a great blessing.
 It's a blessing for the person who speaks.
 It's a blessing for the person who hears it.
 When we hear the teachings of the Lord Buddha, it's one of
 the greatest blessings.
 When we talk about the teachings of the Lord Buddha, when
 we talk about meditation practice,
 it's one of the greatest blessings that leads to peace and
 to happiness in the world.
 Even animals, when they hear the teaching of the Lord
 Buddha, because it's so pure and
 there's nothing mean or evil being said, their minds can
 become pure.
 In the time of the Lord Buddha, there's a story of a frog
 that was reborn in heaven simply
 from listening to the Lord Buddha's teaching.
 He didn't understand a word but was sitting absorbing the
 sound vibrations and as a result
 of the wholesome mind which arose as a result, at the
 moment when the frog died, it was reborn
 in heaven.
 So it's considered a blessing.
 Whatever words are well spoken, it's considered a blessing
 as well.
 This is the blessings numbers 7 to 10.
 Thank you.
 Thank you.
 Thank you.
