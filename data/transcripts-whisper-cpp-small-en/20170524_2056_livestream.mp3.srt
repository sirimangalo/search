1
00:00:00,000 --> 00:00:13,920
 Okay, good evening everyone.

2
00:00:13,920 --> 00:00:28,880
 Welcome to our evening broadcast.

3
00:00:28,880 --> 00:00:40,880
 I'm going to talk about the Dharma.

4
00:00:40,880 --> 00:00:43,880
 Tonight I thought I'd talk about karma.

5
00:00:43,880 --> 00:00:48,880
 He used karma as a sort of a jumping off point.

6
00:00:48,880 --> 00:00:55,330
 Karma is I think one of the more interesting aspects of the

7
00:00:55,330 --> 00:00:57,880
 Dharma, the Buddhist Dharma.

8
00:00:57,880 --> 00:01:01,960
 I've talked about this, of course I've given many talks

9
00:01:01,960 --> 00:01:03,880
 about karma.

10
00:01:03,880 --> 00:01:07,630
 I'm going to go over some of the ideas, where it comes from

11
00:01:07,630 --> 00:01:09,880
, of course.

12
00:01:09,880 --> 00:01:12,880
 Karma is not a Buddhist word.

13
00:01:12,880 --> 00:01:14,880
 It's a Sanskrit word.

14
00:01:14,880 --> 00:01:17,880
 It's a simple word having to do with action.

15
00:01:17,880 --> 00:01:20,880
 Karma means action.

16
00:01:20,880 --> 00:01:24,630
 And so when someone had work to do and they said, "Oh, I've

17
00:01:24,630 --> 00:01:27,880
 got to go now because I have karmangkaroati."

18
00:01:27,880 --> 00:01:35,670
 It's probably different than karmangkaroati, I have work to

19
00:01:35,670 --> 00:01:37,880
 do.

20
00:01:37,880 --> 00:01:43,860
 So that's all I meant. I have some things to do, actions

21
00:01:43,860 --> 00:01:49,880
 that I have to perform.

22
00:01:49,880 --> 00:01:58,170
 And so in a spiritual sense it came to mean ritual action,

23
00:01:58,170 --> 00:01:59,880
 performing sacrifices.

24
00:01:59,880 --> 00:02:08,300
 They had very elaborate, complicated, esoteric rituals that

25
00:02:08,300 --> 00:02:10,880
 they had to perform.

26
00:02:10,880 --> 00:02:25,950
 And those rituals were considered to be spiritual karma or

27
00:02:25,950 --> 00:02:31,880
 a high sort of karma.

28
00:02:31,880 --> 00:02:40,880
 They were actions that had some magical results. You

29
00:02:40,880 --> 00:02:40,880
 perform them and there's a sense that you go to heaven and

30
00:02:40,880 --> 00:02:40,880
 that kind of thing.

31
00:02:40,880 --> 00:02:44,880
 Maybe you just get cows or sons or wealth.

32
00:02:44,880 --> 00:02:49,450
 Those are the big things, maybe not in that order, but

33
00:02:49,450 --> 00:02:53,880
 victory in battle, etc.

34
00:02:53,880 --> 00:02:59,620
 So when the Buddha came along, the idea of karma was well

35
00:02:59,620 --> 00:03:03,710
 established and he actually, you know, really if you look

36
00:03:03,710 --> 00:03:06,880
 at it, he taught against the concept of karma.

37
00:03:06,880 --> 00:03:09,880
 He taught against it in a couple of ways.

38
00:03:09,880 --> 00:03:15,160
 First of all, he said that ritual action is useless,

39
00:03:15,160 --> 00:03:16,880
 meaningless.

40
00:03:16,880 --> 00:03:20,040
 You don't really have seven years of bad luck if you break

41
00:03:20,040 --> 00:03:22,880
 a mirror, bad luck if you walk under a ladder.

42
00:03:22,880 --> 00:03:30,650
 You don't get good luck from rubbing a horse or a rabbit's

43
00:03:30,650 --> 00:03:31,880
 foot.

44
00:03:31,880 --> 00:03:36,880
 You don't go to heaven because you pour butter on the fire.

45
00:03:36,880 --> 00:03:39,880
 Sorry, it's a little harder than that.

46
00:03:39,880 --> 00:03:46,380
 And so he pointed out what he had seen and that is that

47
00:03:46,380 --> 00:03:52,880
 true karma is in our intentions or our state of mind.

48
00:03:52,880 --> 00:04:01,490
 Manopu, bangamadam, manasah jai padutena, basativa, karut

49
00:04:01,490 --> 00:04:01,880
iva.

50
00:04:01,880 --> 00:04:06,520
 If you actors speak with a defiled mind, then suffering

51
00:04:06,520 --> 00:04:07,880
 follows you.

52
00:04:07,880 --> 00:04:14,040
 If you actors speak with a pure mind, then happiness

53
00:04:14,040 --> 00:04:15,880
 follows you.

54
00:04:15,880 --> 00:04:20,270
 I thought that karma had everything to do with your state

55
00:04:20,270 --> 00:04:22,880
 of mind when you do something.

56
00:04:22,880 --> 00:04:26,990
 So if you step on an ant and you didn't know it was there,

57
00:04:26,990 --> 00:04:28,880
 your state of mind is not culpable.

58
00:04:28,880 --> 00:04:35,880
 It's not, you don't have the mens rea, the guilty mind.

59
00:04:35,880 --> 00:04:39,090
 Of course if you go and if you see ants on the ground and

60
00:04:39,090 --> 00:04:41,880
 you go out of your way to stomp them all to death,

61
00:04:41,880 --> 00:04:45,880
 then that certainly is a guilty mind.

62
00:04:45,880 --> 00:04:47,880
 So quite simple really.

63
00:04:47,880 --> 00:04:51,520
 I mean it was really just denying this special sense of

64
00:04:51,520 --> 00:04:55,110
 karma and reminding people that what's really important is

65
00:04:55,110 --> 00:04:56,880
 your awareness,

66
00:04:56,880 --> 00:05:06,880
 your intention, your inclination.

67
00:05:06,880 --> 00:05:11,880
 And so there is a lot of teaching about karma and Buddhism

68
00:05:11,880 --> 00:05:13,880
 in the sense of intention,

69
00:05:13,880 --> 00:05:17,880
 talking about cause and effect and so we hear about how

70
00:05:17,880 --> 00:05:20,470
 hurting someone in this life makes you sick in the next

71
00:05:20,470 --> 00:05:24,840
 life, killing someone in this life makes you short lived in

72
00:05:24,840 --> 00:05:29,880
 the next life.

73
00:05:29,880 --> 00:05:32,730
 Being generous in this life makes you rich in the next life

74
00:05:32,730 --> 00:05:37,410
, being humble in this life makes you famous in the next

75
00:05:37,410 --> 00:05:37,880
 life.

76
00:05:37,880 --> 00:05:39,880
 But it taught these sorts of things.

77
00:05:39,880 --> 00:05:45,880
 I mean it's a very coarse and general sense.

78
00:05:45,880 --> 00:05:48,860
 It's not one to one, it's not absolute, there's so many

79
00:05:48,860 --> 00:05:50,880
 more factors, right?

80
00:05:50,880 --> 00:05:55,190
 There's dita dama vedaniya kamma, kamma that ripens in this

81
00:05:55,190 --> 00:05:57,880
 life, there's kamma that ripens in the next life,

82
00:05:57,880 --> 00:06:02,020
 there's kamma that ripens in some other life and there's k

83
00:06:02,020 --> 00:06:05,880
amma that has already been extinguished.

84
00:06:05,880 --> 00:06:10,880
 That won't give a result.

85
00:06:10,880 --> 00:06:15,270
 Then there's all the many different kammas that have to

86
00:06:15,270 --> 00:06:15,880
 compete.

87
00:06:15,880 --> 00:06:20,920
 Some kamma creates results, some kamma supports existing or

88
00:06:20,920 --> 00:06:22,880
 potential results.

89
00:06:22,880 --> 00:06:27,550
 Some kamma weakens, the power of other kamma, some kamma

90
00:06:27,550 --> 00:06:34,880
 destroys the power of other kamma, enullifies it.

91
00:06:34,880 --> 00:06:37,880
 Anyways, we're looking at kamma.

92
00:06:37,880 --> 00:06:42,690
 What made me really think of this is I saw just earlier in

93
00:06:42,690 --> 00:06:49,880
 my room today a cat in the backyard and caught a squirrel.

94
00:06:49,880 --> 00:06:52,880
 I killed the squirrel dead.

95
00:06:52,880 --> 00:06:58,160
 So besides thinking about how cruel and evil cats are, it

96
00:06:58,160 --> 00:07:03,280
 made me wonder about the idea actually of survival of the

97
00:07:03,280 --> 00:07:03,880
 fittest.

98
00:07:03,880 --> 00:07:09,360
 The idea that this squirrel was unfit and therefore it felt

99
00:07:09,360 --> 00:07:12,880
 prey and that over time only the...

100
00:07:12,880 --> 00:07:17,780
 I mean it makes simplistic logical sense that this idea of

101
00:07:17,780 --> 00:07:19,880
 survival of the fittest,

102
00:07:19,880 --> 00:07:23,310
 and it looks very much like what happens in life, but it's

103
00:07:23,310 --> 00:07:27,110
 kind of silly because what if that squirrel was at the top

104
00:07:27,110 --> 00:07:28,880
 of its game,

105
00:07:28,880 --> 00:07:36,880
 but circumstances conspired, you know?

106
00:07:36,880 --> 00:07:40,880
 Sometimes the lazy cat, lazy squirrel gets lucky.

107
00:07:40,880 --> 00:07:44,630
 So it's not... so what they would say is that well over

108
00:07:44,630 --> 00:07:47,880
 time it's going to eventually be survival of the fittest.

109
00:07:47,880 --> 00:07:50,770
 But if you look at it closer, from a Buddhist point of view

110
00:07:50,770 --> 00:07:54,160
, I mean it's kind of interesting to think of which species

111
00:07:54,160 --> 00:07:54,880
 flourish.

112
00:07:54,880 --> 00:07:59,880
 What members of a species flourish?

113
00:07:59,880 --> 00:08:02,850
 From a Buddhist point of view, first of all it's those who

114
00:08:02,850 --> 00:08:03,880
 have good karma, right?

115
00:08:03,880 --> 00:08:08,290
 And if someone has good karma, they're much likely to be

116
00:08:08,290 --> 00:08:11,880
 healthier and stronger and faster than the rest, right?

117
00:08:11,880 --> 00:08:15,140
 Because they have a mind that is... I mean it's really they

118
00:08:15,140 --> 00:08:19,880
 have a mind that is so powerful and so focused,

119
00:08:19,880 --> 00:08:24,880
 so confident, so energized by their goodness.

120
00:08:24,880 --> 00:08:26,900
 And so survival of the fittest in that sense from a

121
00:08:26,900 --> 00:08:30,910
 Buddhist point of view turns out to be very much associated

122
00:08:30,910 --> 00:08:31,880
 with karma.

123
00:08:31,880 --> 00:08:35,650
 Or some other aspects like those that are... I mean what

124
00:08:35,650 --> 00:08:38,880
 then is interesting about those who have bad karma,

125
00:08:38,880 --> 00:08:43,250
 let's say like the squirrels that keep getting eaten, they

126
00:08:43,250 --> 00:08:45,980
 would be much more likely to be disenchanted with being a

127
00:08:45,980 --> 00:08:46,880
 squirrel,

128
00:08:46,880 --> 00:08:49,880
 and so much less likely to then be born as a squirrel,

129
00:08:49,880 --> 00:08:51,510
 whereas those who really liked being a squirrel and were

130
00:08:51,510 --> 00:08:53,880
 really good at being a squirrel

131
00:08:53,880 --> 00:08:58,880
 would probably be born again and again as a squirrel.

132
00:08:58,880 --> 00:09:03,900
 So you'd think that you'd get over time, and what you see,

133
00:09:03,900 --> 00:09:06,880
 it also fits the Buddhist... these Buddhist ideas of karma

134
00:09:06,880 --> 00:09:10,880
 that you see a streamlining and a refining,

135
00:09:10,880 --> 00:09:15,040
 and so you get this efficient group of people, right? The

136
00:09:15,040 --> 00:09:17,760
 squirrels who weren't very good squirrels wouldn't stay as

137
00:09:17,760 --> 00:09:18,880
 squirrels very long.

138
00:09:18,880 --> 00:09:21,570
 They wouldn't want to be born as squirrels again. But those

139
00:09:21,570 --> 00:09:25,660
 who do it really well and get work into teams, you ever

140
00:09:25,660 --> 00:09:29,870
 seen teams of squirrels working together to scare off cats

141
00:09:29,870 --> 00:09:33,880
 and birds and so on.

142
00:09:33,880 --> 00:09:38,330
 And of course humans, I mean much more important is talking

143
00:09:38,330 --> 00:09:40,880
 about humans, humans as well.

144
00:09:40,880 --> 00:09:45,880
 We're much more homogenous than you would think, right?

145
00:09:45,880 --> 00:09:49,880
 Our habits are very similar and we have a harmony.

146
00:09:49,880 --> 00:09:54,270
 It's like a dance that we're dancing with each other and

147
00:09:54,270 --> 00:09:59,630
 with the societies and countries and nations that we live

148
00:09:59,630 --> 00:09:59,880
 in,

149
00:09:59,880 --> 00:10:02,970
 because we've been practicing lifetime after lifetime. We

150
00:10:02,970 --> 00:10:03,880
're really good at it.

151
00:10:03,880 --> 00:10:08,230
 Those who aren't very good at it, well, maybe they have bad

152
00:10:08,230 --> 00:10:10,880
 karma so they're born as an animal,

153
00:10:10,880 --> 00:10:13,960
 or maybe they're not good at being a human in the sense

154
00:10:13,960 --> 00:10:15,880
 that they're too good for it.

155
00:10:15,880 --> 00:10:22,370
 They're just, you know, they're overqualified. And so when

156
00:10:22,370 --> 00:10:23,880
 they die, they get promoted.

157
00:10:23,880 --> 00:10:27,880
 They go to heaven or to the Brahma realms.

158
00:10:27,880 --> 00:10:31,140
 Very interesting to think about. What I want to point out

159
00:10:31,140 --> 00:10:33,880
 out of this is that there's so much to think about.

160
00:10:33,880 --> 00:10:37,880
 Karma is a very interesting subject.

161
00:10:37,880 --> 00:10:42,140
 And it's a warning because this isn't really Buddhism. It's

162
00:10:42,140 --> 00:10:43,880
 things that the Buddha taught.

163
00:10:43,880 --> 00:10:51,300
 But he had a completely different reason underneath for

164
00:10:51,300 --> 00:10:53,880
 teaching karma.

165
00:10:53,880 --> 00:10:55,750
 He didn't teach karma saying, "Yeah, so this is my teaching

166
00:10:55,750 --> 00:10:58,880
. Go out and do lots of good deeds and don't do bad deeds.

167
00:10:58,880 --> 00:11:03,720
 And learn how to be very efficient at, you know, say being

168
00:11:03,720 --> 00:11:09,880
 a human and, you know, be a very good person."

169
00:11:09,880 --> 00:11:14,220
 In fact, it's not what he taught. It's not the point. The

170
00:11:14,220 --> 00:11:20,560
 point was to show that there is a science behind this, that

171
00:11:20,560 --> 00:11:22,880
 reality is based on cause and effect.

