1
00:00:00,000 --> 00:00:05,120
 why everyone should meditate. Five reasons. The practice of

2
00:00:05,120 --> 00:00:07,600
 meditation allows us to live our lives

3
00:00:07,600 --> 00:00:12,190
 free from suffering. It allows us to give up grasping and

4
00:00:12,190 --> 00:00:14,160
 clinging so that everywhere and

5
00:00:14,160 --> 00:00:17,710
 anywhere we can be truly happy, at peace with ourselves and

6
00:00:17,710 --> 00:00:19,200
 the world around us.

7
00:00:19,200 --> 00:00:23,560
 This is because meditation benefits us in five ways, as

8
00:00:23,560 --> 00:00:24,080
 follows.

9
00:00:25,680 --> 00:00:31,170
 One. Meditation purifies the mind. What does it mean to

10
00:00:31,170 --> 00:00:34,400
 have a pure mind? A pure mind involves

11
00:00:34,400 --> 00:00:38,270
 mental clarity. Our minds give rise to many different

12
00:00:38,270 --> 00:00:41,680
 conflicting mind states. Some mind

13
00:00:41,680 --> 00:00:45,640
 states bring happiness and some bring suffering. There are

14
00:00:45,640 --> 00:00:47,760
 three general characteristics of mind

15
00:00:47,760 --> 00:00:52,000
 states that bring suffering. Positively inclined states,

16
00:00:52,000 --> 00:00:53,840
 greed, wanting, etc.

17
00:00:54,800 --> 00:00:59,920
 Negatively inclined states, anger, disliking, hatred, etc.

18
00:00:59,920 --> 00:01:03,040
 and neutral states, association

19
00:01:03,040 --> 00:01:07,470
 without judgment. These states are considered to be mental

20
00:01:07,470 --> 00:01:10,240
 impurities because they cause suffering.

21
00:01:10,240 --> 00:01:13,500
 They cause us to perform harmful deeds towards others and

22
00:01:13,500 --> 00:01:17,040
 ourselves. Positively inclined states

23
00:01:17,040 --> 00:01:20,560
 lead to addiction. Negatively inclined states lead to

24
00:01:20,560 --> 00:01:23,920
 immediate mental suffering. Neutral states

25
00:01:23,920 --> 00:01:28,350
 involve ideas of possession, me and mine, concepts, views,

26
00:01:28,350 --> 00:01:29,520
 and conceit.

27
00:01:29,520 --> 00:01:33,180
 Ideas which lead us to hold ourselves higher than others or

28
00:01:33,180 --> 00:01:34,640
 put ourselves down.

29
00:01:34,640 --> 00:01:40,020
 Meditation helps to overcome unwholesome mind states that

30
00:01:40,020 --> 00:01:43,040
 arise. When we see something,

31
00:01:43,040 --> 00:01:48,580
 we may like it positive or dislike it negative or create

32
00:01:48,580 --> 00:01:51,280
 some kind of idea of self-righteousness

33
00:01:51,280 --> 00:01:56,450
 or conceit related to it neutral. Each moment we practice

34
00:01:56,450 --> 00:01:59,840
 meditation, we create a clear state of

35
00:01:59,840 --> 00:02:03,550
 awareness instead of reacting to the experience. When we

36
00:02:03,550 --> 00:02:06,160
 see something, we simply remind ourselves

37
00:02:06,160 --> 00:02:10,740
 that it is seeing, that this is an experience of seeing.

38
00:02:10,740 --> 00:02:14,240
 When we feel pain, we remind ourselves

39
00:02:14,240 --> 00:02:19,270
 that it is pain and simply create the clear awareness of it

40
00:02:19,270 --> 00:02:21,760
 as pain. When we do this,

41
00:02:21,760 --> 00:02:25,480
 our minds are diverted from the habitual next step of

42
00:02:25,480 --> 00:02:28,880
 reacting to the experience as they are fixed

43
00:02:28,880 --> 00:02:33,670
 on the ultimate reality of the experience as it is. Neither

44
00:02:33,670 --> 00:02:36,880
 good nor bad, not me or mine, etc.

45
00:02:37,520 --> 00:02:42,050
 It simply is. This is what it means to purify the mind by

46
00:02:42,050 --> 00:02:46,400
 creating clear awareness. The purity that

47
00:02:46,400 --> 00:02:49,650
 comes from clear awareness is the first benefit of

48
00:02:49,650 --> 00:02:52,720
 meditation. It helps us to get rid of anger,

49
00:02:52,720 --> 00:02:56,570
 addiction, and all sorts of conceit and allows us to move

50
00:02:56,570 --> 00:02:58,800
 away from the unwholesome mind states

51
00:02:58,800 --> 00:03:04,380
 that cause suffering. Number two, meditation allows us to

52
00:03:04,380 --> 00:03:05,760
 overcome mental illness.

53
00:03:06,640 --> 00:03:10,630
 Mental illness refers not only to states that cause one to

54
00:03:10,630 --> 00:03:12,560
 enter a psychiatric ward.

55
00:03:12,560 --> 00:03:16,780
 Mental illness refers to any sort of mental distress or

56
00:03:16,780 --> 00:03:19,520
 disease. In this case, we understand

57
00:03:19,520 --> 00:03:25,760
 disease is as "disease", not being at ease. States like

58
00:03:25,760 --> 00:03:28,960
 depression, stress, anxiety,

59
00:03:30,160 --> 00:03:34,410
 all these are mental illness that can cause such great

60
00:03:34,410 --> 00:03:37,280
 suffering as to lead those who suffer from

61
00:03:37,280 --> 00:03:41,420
 them to seek solutions like pills, therapy, and other

62
00:03:41,420 --> 00:03:44,640
 external means of temporarily removing

63
00:03:44,640 --> 00:03:49,190
 them from our minds. Unfortunately, these solutions

64
00:03:49,190 --> 00:03:52,240
 generally fall short of a full cure

65
00:03:52,240 --> 00:03:55,450
 as they do not engage directly with the root cause of

66
00:03:55,450 --> 00:03:57,120
 mental illness, the mind.

67
00:03:58,240 --> 00:04:01,400
 Meditation practice is the most successful means of

68
00:04:01,400 --> 00:04:04,720
 overcoming mental illness because it does engage

69
00:04:04,720 --> 00:04:07,600
 directly with the mind. We do not ever have to take

70
00:04:07,600 --> 00:04:10,880
 medication for something which is purely mental.

71
00:04:10,880 --> 00:04:15,060
 We can eradicate on our own states of mind like depression,

72
00:04:15,060 --> 00:04:19,200
 stress, anxiety, insomnia, worry,

73
00:04:19,200 --> 00:04:23,500
 fear without any need to see a clinical therapist or doctor

74
00:04:23,500 --> 00:04:26,400
. Without training in meditation,

75
00:04:26,400 --> 00:04:30,260
 one may never realize that we have such an incredible tool

76
00:04:30,260 --> 00:04:32,320
 that allows us to break ourselves

77
00:04:32,320 --> 00:04:36,220
 of harmful mind states on a real and profound level. When

78
00:04:36,220 --> 00:04:39,440
 we practice meditation creating clear

79
00:04:39,440 --> 00:04:43,730
 remembrance of the present reality at every moment, we

80
00:04:43,730 --> 00:04:46,400
 evoke mind states that foster a new

81
00:04:46,400 --> 00:04:49,880
 way of looking at the world. Instead of perceiving our

82
00:04:49,880 --> 00:04:52,960
 experiences as problems, stressful,

83
00:04:52,960 --> 00:04:57,760
 depressing, fearful, etc. We simply see them as they are.

84
00:04:57,760 --> 00:05:00,240
 Before our mind is able to judge

85
00:05:00,240 --> 00:05:08,560
 the experience, we say to ourselves "seeing" "hearing" "

86
00:05:08,560 --> 00:05:11,920
thinking" etc. And there is no

87
00:05:11,920 --> 00:05:15,600
 opportunity for reactionary states like stress, depression,

88
00:05:15,600 --> 00:05:19,280
 or fear to arise. We can even note

89
00:05:19,280 --> 00:05:28,140
 "stressed" "depressed" or "afraid" and the chain reaction

90
00:05:28,140 --> 00:05:31,040
 that causes those states to overwhelm

91
00:05:31,040 --> 00:05:36,360
 us is cut off, causing them to fall away naturally by

92
00:05:36,360 --> 00:05:39,520
 reminding ourselves in this way that experiences

93
00:05:39,520 --> 00:05:42,870
 are just what they are. Unhealthy mind states have no

94
00:05:42,870 --> 00:05:45,760
 opportunity to become a habitual response

95
00:05:45,760 --> 00:05:49,700
 to a particular experience. Instead, we develop a new habit

96
00:05:49,700 --> 00:05:51,760
 of seeing things simply as they are.

97
00:05:51,760 --> 00:05:56,170
 Because of the change in perspective that comes from such

98
00:05:56,170 --> 00:05:57,920
 practice over the long term,

99
00:05:57,920 --> 00:06:01,340
 bad habits are abandoned and mental illness is overcome

100
00:06:01,340 --> 00:06:04,720
 directly. In this way, meditators

101
00:06:04,720 --> 00:06:09,140
 successfully overcome issues like insomnia, depression,

102
00:06:09,140 --> 00:06:12,400
 societal ideation, alcoholism,

103
00:06:12,400 --> 00:06:16,530
 and all sorts of addiction and harmful mind states. Often

104
00:06:16,530 --> 00:06:19,040
 though, simple reduction and encouragement

105
00:06:19,040 --> 00:06:22,970
 for a qualified meditation teacher, everyone experiences

106
00:06:22,970 --> 00:06:25,360
 mental dis-ease of various forms

107
00:06:25,360 --> 00:06:28,180
 to some degree. For this reason, everyone should practice

108
00:06:28,180 --> 00:06:28,880
 meditation.

109
00:06:28,880 --> 00:06:33,120
 3. Meditation does away with suffering. There are two kinds

110
00:06:33,120 --> 00:06:34,160
 of suffering that exist.

111
00:06:34,160 --> 00:06:38,050
 The first kind of suffering is bodily suffering. Suffering

112
00:06:38,050 --> 00:06:41,040
 caused by pain in our back, our legs,

113
00:06:41,040 --> 00:06:45,110
 our head, our stomach, and various other parts of the body.

114
00:06:45,110 --> 00:06:47,520
 The other kind of suffering is mental

115
00:06:47,520 --> 00:06:51,020
 suffering. This kind of suffering is the distress that

116
00:06:51,020 --> 00:06:53,920
 comes from mental illness as discussed above.

117
00:06:53,920 --> 00:06:57,760
 When we feel anguish in the mind, it can be much worse than

118
00:06:57,760 --> 00:07:01,120
 physical pain. Meditation is able to

119
00:07:01,120 --> 00:07:04,820
 do away with both of these kinds of suffering. It allows us

120
00:07:04,820 --> 00:07:07,040
 to understand that physical pain

121
00:07:07,040 --> 00:07:10,790
 is just a part of our existence. That physical discomfort

122
00:07:10,790 --> 00:07:12,720
 is actually not an uncomfortable

123
00:07:12,720 --> 00:07:15,900
 thing and that mental discomfort is caused by our

124
00:07:15,900 --> 00:07:18,720
 unnecessary judgment of experience like

125
00:07:18,720 --> 00:07:22,970
 pain and discomfort. Physical pain is simply a phenomenon

126
00:07:22,970 --> 00:07:25,200
 which arises and ceases. There is

127
00:07:25,200 --> 00:07:28,800
 nothing intrinsic to pain that tells us that it is bad. It

128
00:07:28,800 --> 00:07:30,880
 is simply the brain telling us that

129
00:07:30,880 --> 00:07:34,560
 there is pain. The mind itself makes a judgment of our

130
00:07:34,560 --> 00:07:37,920
 experience as good or bad. The mind can

131
00:07:37,920 --> 00:07:41,820
 be trained to interpret phenomena in any number of ways.

132
00:07:41,820 --> 00:07:44,240
 For example, it is possible to perceive

133
00:07:44,240 --> 00:07:48,330
 pain as a good thing, a pleasurable experience even. In

134
00:07:48,330 --> 00:07:51,360
 meditation, we learn to see pain as simply

135
00:07:51,360 --> 00:07:56,750
 pain. When we experience pain, we say to ourselves "pain,

136
00:07:56,750 --> 00:07:59,360
 pain," reminding ourselves that it is just

137
00:07:59,360 --> 00:08:02,760
 pain rather than allowing it to produce stress and

138
00:08:02,760 --> 00:08:06,240
 suffering. We do not have to experience pain as

139
00:08:06,240 --> 00:08:10,040
 negative, something to get rid of or do away with. The

140
00:08:10,040 --> 00:08:13,120
 reason we are unable to experience pain as it

141
00:08:13,120 --> 00:08:16,840
 is without reacting to it is because we normally lack the

142
00:08:16,840 --> 00:08:19,680
 right tool. Meditation is that tool.

143
00:08:19,680 --> 00:08:23,040
 It allows us to come to the realization of seeing things as

144
00:08:23,040 --> 00:08:26,160
 they actually are. When we see the

145
00:08:26,160 --> 00:08:29,410
 objects of experience clearly as they are, we are able to

146
00:08:29,410 --> 00:08:31,440
 fear ourselves from suffering.

147
00:08:31,440 --> 00:08:36,420
 Less obvious than suffering from pain, but perhaps more

148
00:08:36,420 --> 00:08:39,040
 pervasive, is suffering as a result

149
00:08:39,040 --> 00:08:43,460
 of thinking. When we think about something, recall memories

150
00:08:43,460 --> 00:08:45,280
 or worry about the future,

151
00:08:45,280 --> 00:08:49,720
 we may feel sadness, anger, excitement, anticipation, etc.

152
00:08:49,720 --> 00:08:52,160
 Our judgment complicate

153
00:08:52,160 --> 00:08:55,550
 all of our many experiences that are actually completely

154
00:08:55,550 --> 00:08:58,960
 neutral. When we say to ourselves

155
00:08:58,960 --> 00:09:04,680
 in our mind "thinking," we remind ourselves that thought is

156
00:09:04,680 --> 00:09:07,600
 just thought. We see thinking simply as

157
00:09:07,600 --> 00:09:13,220
 it is. When we say to ourselves "worrying," we see that

158
00:09:13,220 --> 00:09:15,120
 there is nothing actually worth

159
00:09:15,120 --> 00:09:19,280
 worrying over. We see that the experience is not under our

160
00:09:19,280 --> 00:09:22,400
 control. We cannot change it or make it

161
00:09:22,400 --> 00:09:27,010
 accord with our wishes. If our mind is clear, it simply

162
00:09:27,010 --> 00:09:29,520
 arises and falls away without leading

163
00:09:29,520 --> 00:09:33,540
 to stress or suffering. Many physical ailments arise from

164
00:09:33,540 --> 00:09:35,840
 stress and the buildup of tension

165
00:09:35,840 --> 00:09:39,440
 within the body. Meditation can permanently relieve this

166
00:09:39,440 --> 00:09:41,520
 tension. Though it may take time

167
00:09:41,520 --> 00:09:45,050
 for the mind to let go of the stress, in the end, pain

168
00:09:45,050 --> 00:09:48,000
 caused by the stress can be cured completely.

169
00:09:48,000 --> 00:09:52,140
 Changing our perspective to see the pain as just pain is

170
00:09:52,140 --> 00:09:54,320
 part of the letting go that allows for

171
00:09:54,320 --> 00:09:58,220
 healing. It is common for those who have suffered stress-

172
00:09:58,220 --> 00:10:00,560
based illnesses, with which doctors could

173
00:10:00,560 --> 00:10:03,840
 not help them come to realize that illness is based on

174
00:10:03,840 --> 00:10:06,160
 stress. And when they do away with the

175
00:10:06,160 --> 00:10:10,810
 stress, they do away with the illness. Meditation allows us

176
00:10:10,810 --> 00:10:12,880
 to free ourselves from

177
00:10:12,880 --> 00:10:16,230
 physical and mental. Some may say that suffering is

178
00:10:16,230 --> 00:10:19,280
 actually an important part of life and that

179
00:10:19,280 --> 00:10:22,620
 without suffering, we would not learn anything or that life

180
00:10:22,620 --> 00:10:25,280
 would be less interesting. It is

181
00:10:25,280 --> 00:10:28,920
 absolutely true that suffering does offer us opportunities

182
00:10:28,920 --> 00:10:30,720
 to learn. But we really take the

183
00:10:30,720 --> 00:10:34,960
 time to truly understand anything from it. Meditation is

184
00:10:34,960 --> 00:10:37,280
 the tool that allows us to truly

185
00:10:37,280 --> 00:10:40,880
 learn everything. There is to know about suffering. Once we

186
00:10:40,880 --> 00:10:43,440
 have learned all there is to know about

187
00:10:43,440 --> 00:10:48,590
 suffering, there will be no reason for us to suffer anymore

188
00:10:48,590 --> 00:10:51,200
. 4. Meditation helps you follow

189
00:10:51,200 --> 00:10:55,670
 the right path. There are many different paths in life. You

190
00:10:55,670 --> 00:10:58,640
 can practice a path which leads to

191
00:10:58,640 --> 00:11:02,620
 suffering. You can practice one that is charitable and

192
00:11:02,620 --> 00:11:06,000
 moral. There are many meditative lifestyles.

193
00:11:06,000 --> 00:11:09,760
 Yoga, tai chi, various traditions in Hinduism or even in

194
00:11:09,760 --> 00:11:12,880
 the western religions that teach you to

195
00:11:12,880 --> 00:11:15,900
 fix and focus your mind on a single object, leading to a

196
00:11:15,900 --> 00:11:18,240
 great state of peace and tranquility.

197
00:11:18,240 --> 00:11:21,960
 These meditative or religious practices are not what we

198
00:11:21,960 --> 00:11:24,160
 consider to be the right path.

199
00:11:24,720 --> 00:11:28,080
 It is not that any of these meditative paths are wrong. It

200
00:11:28,080 --> 00:11:30,400
 is just that these paths are not what

201
00:11:30,400 --> 00:11:34,620
 is meant by the right path. Just as meaning of right path

202
00:11:34,620 --> 00:11:36,880
 is not the path of becoming a monk,

203
00:11:36,880 --> 00:11:41,170
 a Buddhist, a lawyer, a doctor, etc. All of these paths

204
00:11:41,170 --> 00:11:43,440
 exist and may be good or bad.

205
00:11:43,440 --> 00:11:46,400
 They simply are not the right path according to the Buddha.

206
00:11:46,400 --> 00:11:50,880
 The right path of Buddha relates to the quality of one's

207
00:11:50,880 --> 00:11:53,840
 mind. It involves perfect clarity and

208
00:11:53,840 --> 00:11:58,410
 understanding of reality as it really is. It is called the

209
00:11:58,410 --> 00:12:00,160
 path because it leads to the

210
00:12:00,160 --> 00:12:03,630
 goal of freedom from suffering. Mindfulness might teach and

211
00:12:03,630 --> 00:12:05,760
 leads to freedom from suffering because

212
00:12:05,760 --> 00:12:09,380
 it puts us on the right path by helping us to see clearly,

213
00:12:09,380 --> 00:12:11,280
 purifying our minds from the

214
00:12:11,280 --> 00:12:14,950
 defilements that cloud and distort our perception, whether

215
00:12:14,950 --> 00:12:17,440
 we choose the path of a lawyer or doctor,

216
00:12:18,080 --> 00:12:21,700
 Christian or a Buddhist, whatever path we choose. Once we

217
00:12:21,700 --> 00:12:24,160
 create this clarity of mind and understand

218
00:12:24,160 --> 00:12:27,500
 things as they are, we do not give rise to greed and

219
00:12:27,500 --> 00:12:30,720
 attachment which would color our perception.

220
00:12:30,720 --> 00:12:34,160
 We do not give rise to anger which would inflame our mind.

221
00:12:34,160 --> 00:12:36,560
 We do not give rise to delusion and

222
00:12:36,560 --> 00:12:40,550
 conceit and views which would model our mind and confuse us

223
00:12:40,550 --> 00:12:43,200
. We simply understand things as they

224
00:12:43,200 --> 00:12:46,840
 are as they arise. Slowly but surely through mindfulness

225
00:12:46,840 --> 00:12:49,040
 meditation practice you will find

226
00:12:49,040 --> 00:12:53,160
 yourself better able to help yourself and others. You will

227
00:12:53,160 --> 00:12:55,760
 be able to work, study and live more

228
00:12:55,760 --> 00:12:59,210
 efficiently than ever before, simply by understanding

229
00:12:59,210 --> 00:13:01,360
 reality more clearly than before.

230
00:13:01,360 --> 00:13:06,700
 That is what wisdom means to understand things as they are,

231
00:13:06,700 --> 00:13:09,040
 not as we want them to be or want them

232
00:13:09,040 --> 00:13:12,290
 not to be, not understanding things in terms of our mental

233
00:13:12,290 --> 00:13:15,760
 defilements or impurities. Once we come to

234
00:13:15,760 --> 00:13:19,090
 clearly see things as they are, we have found the right

235
00:13:19,090 --> 00:13:21,840
 path. The right path involves knowing the

236
00:13:21,840 --> 00:13:24,990
 difference between the path which leads to our development

237
00:13:24,990 --> 00:13:26,640
 and the path which leads to our

238
00:13:26,640 --> 00:13:29,980
 destruction. Being on the right path does not mean that we

239
00:13:29,980 --> 00:13:32,320
 should live our lives in a certain way

240
00:13:32,320 --> 00:13:36,400
 but that when we do live our lives we see a way to make the

241
00:13:36,400 --> 00:13:39,520
 right decisions. Five meditation makes

242
00:13:39,520 --> 00:13:43,840
 you free. We practice meditation to gain freedom. We call

243
00:13:43,840 --> 00:13:46,480
 this nirvana which is the highest sort of

244
00:13:46,480 --> 00:13:49,880
 freedom. Freedom not to suffer, freedom from development

245
00:13:49,880 --> 00:13:52,400
 and mental impurity, freedom from

246
00:13:52,400 --> 00:13:57,860
 addiction, hatred bigotry, freedom from wrong views, conce

247
00:13:57,860 --> 00:14:00,160
it and all sorts of unpleasant,

248
00:14:00,160 --> 00:14:05,000
 unwholesome, unskillful and useless states of mind, total

249
00:14:05,000 --> 00:14:08,480
 freedom. It is not freedom to do anything.

250
00:14:08,480 --> 00:14:12,690
 Of course you are always free to do what you want but true

251
00:14:12,690 --> 00:14:15,280
 freedom can severely limit the things that

252
00:14:15,280 --> 00:14:18,570
 you decide to do because a lot of things we decide to do

253
00:14:18,570 --> 00:14:22,000
 are actually based on delusion or ignorance

254
00:14:22,000 --> 00:14:25,850
 and lead to suffering. Freedom according to Buddhism is not

255
00:14:25,850 --> 00:14:27,680
 freedom to, it is freedom from.

256
00:14:28,640 --> 00:14:32,340
 Real and true freedom is like a bird in flight not clinging

257
00:14:32,340 --> 00:14:35,360
 to anything, taking its two wings

258
00:14:35,360 --> 00:14:39,780
 as its only possessions. Wherever we go and whatever

259
00:14:39,780 --> 00:14:42,480
 situation that we are in, mindfulness

260
00:14:42,480 --> 00:14:47,910
 allows us not to attach to anything, not people, places or

261
00:14:47,910 --> 00:14:50,960
 things. Meditation helps us live our

262
00:14:50,960 --> 00:14:55,200
 lives. Whatever our situation and not be caught up with

263
00:14:55,200 --> 00:14:58,480
 quote-unquote should be quote-unquote should

264
00:14:58,480 --> 00:15:02,830
 not be quote-unquote wish it were quote-unquote wish it

265
00:15:02,830 --> 00:15:05,920
 weren't or any kind of judgment whatsoever.

266
00:15:05,920 --> 00:15:10,450
 A mindful person lives like a bird. Wherever they go they

267
00:15:10,450 --> 00:15:13,040
 eat the fruit from the tree upon which

268
00:15:13,040 --> 00:15:16,790
 they learn. The practice of meditation is something that

269
00:15:16,790 --> 00:15:19,680
 you have to practice by yourself. It is not

270
00:15:19,680 --> 00:15:23,460
 something that you can just take anyone's word for. With a

271
00:15:23,460 --> 00:15:25,280
 little bit of time and a little bit

272
00:15:25,280 --> 00:15:28,240
 of patience you yourself will come to understand the

273
00:15:28,240 --> 00:15:30,000
 benefits of meditation.

