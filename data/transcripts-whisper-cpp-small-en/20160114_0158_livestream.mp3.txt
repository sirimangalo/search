 Hey, good evening everyone.
 Broadcasting live January 13th, 2016.
 Today's quote is about charity.
 It's a very strong, powerful statement about the power of
 charity.
 If we knew the benefit of giving gifts, we wouldn't eat
 without giving gifts.
 And this is because of the power of the mind, the power of
 karma.
 So, giving a gift is much more than just the benefit that
 accrues to the person.
 It's even much more than the higher opinion that they'll
 then have for you.
 Giving gifts changes, you know.
 It's choosing a fork in the road, right?
 Coming to a fork in the road and choosing a path.
 This is a description of karma.
 So the real issue here is the issue of karma.
 It's not just this.
 This is specifically about charity, which makes it a
 profound statement in its own right.
 It reminds us of the importance of being charitable.
 And not just the importance, but the greatness.
 It's not like it should be an obligation.
 It's like if you only knew what good it did, it's something
 that really, really does good
 things for us.
 But it gives so much.
 It gives more than, first of all, it does give the goodness
 to another person, which
 is a great thing in itself, right?
 You help someone else, you've done a great thing.
 You've created happiness for that person.
 And then the second thing it does is it inclines them
 positively towards you.
 So that's another great thing.
 But even greater than all that is the effect that it has on
 your mind, from making you
 kind and charitable and more importantly, able to let go.
 It makes you less clingy.
 It reduces your attachments.
 It frees you from stinginess and miserliness and desire and
 addiction.
 So charity is good.
 But there's a deeper message here.
 There's two deeper, there's levels of deeper messages.
 So a deeper message is the message of karma, that our
 actions have consequences.
 If they have consequences no matter what, like if you don't
 look both ways before crossing
 the street, you get hit by a car or actions in and of
 themselves have consequences even
 before the mind is factored in.
 There are some people who actually live by this.
 They try and guess what's going to happen if they do this
 or if they do that.
 Some people become superstitious about it.
 If I walk under this ladder, they blow the consequences out
 of proportion.
 Simple acts like walking where a black cat has walked or
 smashing a mirror, etc.
 But actions do have consequences.
 If you break something that belongs to someone else, then
 you have to replace it for them
 even if it wasn't intentional, it becomes your
 responsibility.
 But that's not of course what we usually mean by karma
 because that's fairly superficial.
 The results sure they happen.
 But the results for that in that sense are quite limited.
 We're more interested in the results of mental action,
 mental volition behind our actions.
 Are we doing it out of greed?
 Are we doing it out of anger?
 Because these have real meaning.
 This is where we really get in trouble.
 Or if we're doing something out of mindfulness or wisdom or
 compassion or love, then it's
 really powerful.
 Obviously, if it's clear that you did something wrong as a
 mistake without realizing it, people
 are quick to forgive.
 But if you really meant to hurt someone, you're much less
 likely to be granted forgiveness.
 Likewise, if you give someone something, something, suppose
 you're going to throw something out
 and instead of throwing it out, you say, "Hey, do you want
 this?"
 Not much of a gift, right?
 But if there's something that you really enjoy, like you
 see that there's the last cookie
 in the jar and you're just looking at it and you're
 thinking, "I want that cookie."
 But then someone comes along and says, "Oh wow, there's one
 cookie left.
 Can I have it?"
 What do you do, right?
 When you say to yourself, when you say to the person, "Yes,
 you can have it."
 That's quite powerful.
 We have parents do this for their children, sacrificing so
 much.
 Some parents, of course, not all.
 Be able to sacrifice like that.
 That's just more powerful, is the point.
 So a gift, not all gifts are the same, right?
 Not all charity is the same.
 But the really interesting meaning or interesting aspect of
 this sentence is actually not about
 karma or giving at all.
 It's about knowledge.
 What the Buddha is saying is, "You guys don't know the
 truth."
 He's looking at us and he's saying, "You guys are so blind.
 You people on this earth, it's actually a condemnation, a
 criticism of us, that we're
 blind."
 This is one way of looking at it, of course.
 I'm not claiming to know the Buddha's intentions for
 teaching and being able to read his intentions,
 but it can very easily be understood as a challenge to us.
 Wake up call.
 This is just an example of how ignorant you people are.
 When the opportunity to give arises, you don't give.
 That shows how ignorant you are because if you knew the
 benefits of giving, you would
 just give naturally.
 I suppose you could argue that, "Well, what's the purpose
 of giving in the end?"
 It doesn't really lead you to enlightenment, does it?
 It's not really a practice because it doesn't lead to
 wisdom, to give, and so on.
 People argue this way, "Why did the Buddha talk about
 giving?"
 The meaning here is ... I mean, that really actually proves
 the point that this is just
 an example because you can say, "Well, why talk about
 giving when giving's not really
 essential?"
 The point is, here you have people who want to be happy,
 who want pleasure, who want to
 go to heaven, or who even if they don't believe in heaven
 or don't think about heaven, they
 want heaven.
 They want what heaven represents.
 They want states like heavenly states, pleasurable states
 in general.
 And yet they don't do anything to get them, is what he's
 saying, because pleasure only
 comes from goodness, from mundane, wholesome state, wholes
ome activity.
 Here these people want to be happy, and yet they do all
 sorts of things to make themselves
 unhappy.
 People don't know what leads to happiness.
 They don't do the things that lead to happiness.
 That's what he's saying.
 So it's really deeper and quite interesting for meditators
 and non-meditators alike.
 For meditators, it reminds us of what's most important,
 knowledge, wisdom, understanding.
 That's what we're aiming for.
 That's what we're working for.
 Meditation isn't magic.
 It's not something that's going to transform you or
 something.
 You're not just going to break away from samsara.
 You're going to learn about samsara.
 You're going to learn about reality and the unbecke
 experience.
 You're going to see through the cloud of ignorance and
 delusion and realize these things like,
 wow, giving is such a powerful thing.
 Morality and ethical ethics is such a powerful thing.
 Meditation is such a powerful thing.
 We don't see the way the Buddha sees.
 That's the key.
 Once we see the way the Buddha sees, Buddha saw, once we
 learn and we understand reality,
 and we free ourselves from suffering, we find true
 happiness.
 So just a little bit of dhamma for tonight.
 That's our quote.
 Maybe tomorrow we'll do question and answer.
 Maybe dhamma-pada.
 No, we're doing one dhamma-pada a week, one question and
 answer, and one Buddhism 101.
 I think that's the new regimen.
 I'm giving more five-minute meditation lessons.
 I'm going to try to get someone to take pictures so I can
 share with everybody.
 You get to see what it's like.
 It's really pretty awesome.
 I must have taught 50 people.
 I think I taught over 50 people how to meditate on
 yesterday.
 It's pretty good.
 I gave out 50 booklets on how to meditate.
 It worked just about as well as I could have hoped.
 Anyway, so that's all for tonight.
 Thank you all for tuning in.
