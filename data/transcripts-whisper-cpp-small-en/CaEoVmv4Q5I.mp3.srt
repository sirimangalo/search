1
00:00:00,000 --> 00:00:01,000
 Go ahead.

2
00:00:01,000 --> 00:00:04,890
 It seems in all other human endeavors such as science,

3
00:00:04,890 --> 00:00:06,800
 music, and art, there have been

4
00:00:06,800 --> 00:00:09,600
 technical evolution over time.

5
00:00:09,600 --> 00:00:13,680
 Has the same thing occurred in meditation techniques?

6
00:00:13,680 --> 00:00:21,320
 Yes, certainly.

7
00:00:21,320 --> 00:00:30,200
 Meditation techniques have been modified all the time.

8
00:00:30,200 --> 00:00:37,580
 The Buddha gives meditation instructions in the suttas and

9
00:00:37,580 --> 00:00:40,720
 they are quite clear, but the

10
00:00:40,720 --> 00:00:48,320
 understanding of them varies widely.

11
00:00:48,320 --> 00:00:55,840
 So many people are very sure about what the Buddha taught

12
00:00:55,840 --> 00:00:59,480
 and that they are those who

13
00:00:59,480 --> 00:01:04,690
 follow that teaching and another group claims just the same

14
00:01:04,690 --> 00:01:07,760
 and practice very differently.

15
00:01:07,760 --> 00:01:19,760
 So you can certainly assume that meditation techniques have

16
00:01:19,760 --> 00:01:25,240
 been modified very much.

17
00:01:25,240 --> 00:01:38,890
 From an orthodox point of view, the accepted explanation of

18
00:01:38,890 --> 00:01:39,360
 this is that there are different

19
00:01:39,360 --> 00:01:41,280
 types of people.

20
00:01:41,280 --> 00:01:44,200
 Some people you don't even have to give them a teaching.

21
00:01:44,200 --> 00:01:46,880
 You just tell them what sort of teaching you teach.

22
00:01:46,880 --> 00:01:49,400
 So instead of the satipatthana sutta, instead of even

23
00:01:49,400 --> 00:01:51,160
 teaching the satipatthani sutta, you'd

24
00:01:51,160 --> 00:01:55,830
 say there are these four satipatthanas, these four

25
00:01:55,830 --> 00:01:58,800
 foundations of mindfulness.

26
00:01:58,800 --> 00:02:00,940
 The amazing thing is you might not even have to explain

27
00:02:00,940 --> 00:02:02,320
 what are the four foundations of

28
00:02:02,320 --> 00:02:14,680
 mindfulness and some people just get it just like that.

29
00:02:14,680 --> 00:02:18,810
 Those dhammas that arise based on a cause, the Buddha has

30
00:02:18,810 --> 00:02:21,400
 taught the cause of those dhammas.

31
00:02:21,400 --> 00:02:24,440
 And Sariputta realized nirvana as a result of that.

32
00:02:24,440 --> 00:02:29,720
 He became a suttapana, upatissa, and moggallana as well.

33
00:02:29,720 --> 00:02:34,420
 These types of people are very rare and the theory is that

34
00:02:34,420 --> 00:02:36,640
 they were much more common

35
00:02:36,640 --> 00:02:41,010
 in the Buddha's time, but still very rare in the Buddha's

36
00:02:41,010 --> 00:02:41,320
 time.

37
00:02:41,320 --> 00:02:43,480
 Another type of person you have to explain in detail.

38
00:02:43,480 --> 00:02:47,920
 You have to say, "Well, what do you mean by that?"

39
00:02:47,920 --> 00:02:51,360
 Whatever dhammas arise based on the cause, the Buddha has

40
00:02:51,360 --> 00:02:53,640
 taught the cause of those dhammas.

41
00:02:53,640 --> 00:02:56,640
 Well, it means suffering.

42
00:02:56,640 --> 00:02:59,960
 All suffering has a cause.

43
00:02:59,960 --> 00:03:04,240
 And the Buddha has taught the cause of suffering, which is

44
00:03:04,240 --> 00:03:05,200
 craving.

45
00:03:05,200 --> 00:03:06,920
 When there is craving, then there will be...

46
00:03:06,920 --> 00:03:10,200
 So you explain the Four Noble Truths.

47
00:03:10,200 --> 00:03:11,620
 And maybe you have to explain in some detail, like maybe

48
00:03:11,620 --> 00:03:14,640
 you have to teach the Dhamma, Cakapawata

49
00:03:14,640 --> 00:03:18,430
 and the suttas, like how Gondanya became a suttapana, was

50
00:03:18,430 --> 00:03:19,600
 by listening to the Dhamma,

51
00:03:19,600 --> 00:03:22,280
 Cakapawata and the suttas.

52
00:03:22,280 --> 00:03:25,230
 So this type of person, just when they're listening to the

53
00:03:25,230 --> 00:03:27,520
 Dhamma, they can become enlightened

54
00:03:27,520 --> 00:03:28,840
 as a result of it.

55
00:03:28,840 --> 00:03:31,200
 Or Yasa is another example, just listening to the Buddha's

56
00:03:31,200 --> 00:03:32,400
 teaching once, he became a

57
00:03:32,400 --> 00:03:33,720
 suttapana.

58
00:03:33,720 --> 00:03:38,000
 Listening to the same teaching again, he became an arahant.

59
00:03:38,000 --> 00:03:42,450
 A third type of person has to be trained, not only given

60
00:03:42,450 --> 00:03:44,960
 the detailed discourse, but

61
00:03:44,960 --> 00:03:48,680
 has to be trained in it and has to be guided through it.

62
00:03:48,680 --> 00:03:51,200
 This type of person is not enough to teach them even the

63
00:03:51,200 --> 00:03:52,320
 satipatana suttas.

64
00:03:52,320 --> 00:03:55,090
 But then you have to say, "Okay, come and do a course in

65
00:03:55,090 --> 00:03:56,680
 satipatana and then guide them

66
00:03:56,680 --> 00:03:57,680
 through it.

67
00:03:57,680 --> 00:03:59,000
 Okay, then remember to be mindful.

68
00:03:59,000 --> 00:04:00,000
 Remember to be mindful.

69
00:04:00,000 --> 00:04:01,240
 Okay, now be mindful of this.

70
00:04:01,240 --> 00:04:03,360
 Are you mindful of this?"

71
00:04:03,360 --> 00:04:05,850
 And keeping them on track and guiding them through it to be

72
00:04:05,850 --> 00:04:07,120
 able to understand, "Oh,

73
00:04:07,120 --> 00:04:11,240
 this is the satipatana and this is the truth and this is

74
00:04:11,240 --> 00:04:14,240
 what happens and the realizations."

75
00:04:14,240 --> 00:04:17,830
 And a fourth type of person can't be trained and no matter

76
00:04:17,830 --> 00:04:19,880
 how much you help them and give

77
00:04:19,880 --> 00:04:22,880
 to them, they don't gain anything out of it or they can

78
00:04:22,880 --> 00:04:24,560
 only just get the meaning of

79
00:04:24,560 --> 00:04:28,200
 it, the meaning of the words.

80
00:04:28,200 --> 00:04:31,840
 So because of that, the tepidaka is not enough.

81
00:04:31,840 --> 00:04:36,560
 The tepidaka is enough, this is the Theravada opinion.

82
00:04:36,560 --> 00:04:39,080
 The tepidaka is not enough for everyone.

83
00:04:39,080 --> 00:04:41,120
 You need some explanation of the tepidaka.

84
00:04:41,120 --> 00:04:44,240
 So it's not wrong that people are explaining it.

85
00:04:44,240 --> 00:04:46,920
 The problem, of course, as Bhanjani said, is that one,

86
00:04:46,920 --> 00:04:48,720
 people explain it differently.

87
00:04:48,720 --> 00:04:52,600
 Two, they cling to their explanation as being correct.

88
00:04:52,600 --> 00:04:55,880
 And there is a standard explanation in the Theravada

89
00:04:55,880 --> 00:04:56,840
 tradition.

90
00:04:56,840 --> 00:04:57,840
 It's called the commentaries.

91
00:04:57,840 --> 00:05:01,460
 No, there's a huge commentary literature that has a very

92
00:05:01,460 --> 00:05:03,360
 specific interpretation of

93
00:05:03,360 --> 00:05:04,360
 the Buddhist teaching.

94
00:05:04,360 --> 00:05:07,180
 So it doesn't say, "This is the only interpretation," and

95
00:05:07,180 --> 00:05:10,440
 any other interpretation is heretic and

96
00:05:10,440 --> 00:05:11,440
 is wrong.

97
00:05:11,440 --> 00:05:14,590
 In fact, it even talks about different interpretations and

98
00:05:14,590 --> 00:05:16,920
 it gives the opinion, "This interpretation

99
00:05:16,920 --> 00:05:19,610
 is not correct because it's illogical or because it goes

100
00:05:19,610 --> 00:05:21,280
 against this sutta," but it's still

101
00:05:21,280 --> 00:05:23,640
 just an interpretation.

102
00:05:23,640 --> 00:05:27,810
 The problem is when people say, "The commentaries are just

103
00:05:27,810 --> 00:05:29,320
 interpretation.

104
00:05:29,320 --> 00:05:30,320
 Throw them out.

105
00:05:30,320 --> 00:05:32,740
 Practice the Satipatthana sutta in this way, this way, this

106
00:05:32,740 --> 00:05:33,800
 way, and this way."

107
00:05:33,800 --> 00:05:37,370
 It's very clear how the Buddha said this, which means this,

108
00:05:37,370 --> 00:05:38,600
 which means this.

109
00:05:38,600 --> 00:05:41,170
 And he, proactively, what they do is create another

110
00:05:41,170 --> 00:05:43,080
 commentary, while at the same time

111
00:05:43,080 --> 00:05:46,810
 saying that commentary is just an interpretation and

112
00:05:46,810 --> 00:05:49,360
 therefore shouldn't be followed.

113
00:05:49,360 --> 00:05:51,860
 Unless you can become enlightened through listening and

114
00:05:51,860 --> 00:05:53,200
 through understanding clearly

115
00:05:53,200 --> 00:05:56,720
 the suttas, you're going to have to interpret them and you

116
00:05:56,720 --> 00:05:58,320
're going to have to find a way

117
00:05:58,320 --> 00:05:59,520
 to apply them.

118
00:05:59,520 --> 00:06:03,830
 The Buddha said, "Gacantava gacamiti bhajananti," "When

119
00:06:03,830 --> 00:06:06,360
 going, a monk knows I am going."

120
00:06:06,360 --> 00:06:09,020
 If you can understand that and become enlightened just

121
00:06:09,020 --> 00:06:11,200
 listening to it, because you understand

122
00:06:11,200 --> 00:06:14,440
 then that this hearing, this sound is also just sound and

123
00:06:14,440 --> 00:06:16,160
 you can apply the meditation

124
00:06:16,160 --> 00:06:18,320
 while you're listening to it and become enlightened right

125
00:06:18,320 --> 00:06:20,080
 there, then there's no need to practice

126
00:06:20,080 --> 00:06:21,080
 it.

127
00:06:21,080 --> 00:06:23,260
 But if you can't, then you have to figure out, "Okay, I

128
00:06:23,260 --> 00:06:24,540
 guess that means I should do

129
00:06:24,540 --> 00:06:27,600
 some going and know that I'm going."

130
00:06:27,600 --> 00:06:31,840
 And then you have to practice that, "Okay, going, going, or

131
00:06:31,840 --> 00:06:33,520
 walking, walking."

132
00:06:33,520 --> 00:06:35,620
 And maybe you even have to go to the extent that it's not

133
00:06:35,620 --> 00:06:36,840
 just walking anymore, but I

134
00:06:36,840 --> 00:06:38,900
 have to do it one foot at a time because otherwise I'm not

135
00:06:38,900 --> 00:06:40,360
 really going to see things as they

136
00:06:40,360 --> 00:06:41,360
 are in the Buddha.

137
00:06:41,360 --> 00:06:42,910
 You know, you have all this Buddhist teaching and you

138
00:06:42,910 --> 00:06:44,200
 realize the Buddha was actually talking

139
00:06:44,200 --> 00:06:46,880
 about seeing the dhatus, the elements.

140
00:06:46,880 --> 00:06:49,970
 So if I'm going to see the elements, I have to move one

141
00:06:49,970 --> 00:06:51,080
 foot at a time.

142
00:06:51,080 --> 00:06:53,540
 And wait a minute, even in one step there's different

143
00:06:53,540 --> 00:06:54,600
 elements arising.

144
00:06:54,600 --> 00:06:56,520
 Lifting the foot doesn't feel the same as putting the foot

145
00:06:56,520 --> 00:06:56,820
 down.

146
00:06:56,820 --> 00:06:58,600
 So there's different experiences arising.

147
00:06:58,600 --> 00:07:02,120
 So let's lift the foot first and then put it down.

148
00:07:02,120 --> 00:07:04,200
 And then people come along and say, "What are you doing?"

149
00:07:04,200 --> 00:07:07,180
 The Buddha didn't teach six steps walking and you're doing

150
00:07:07,180 --> 00:07:08,460
 breaking the step up into

151
00:07:08,460 --> 00:07:09,560
 six parts.

152
00:07:09,560 --> 00:07:11,520
 That's not the Buddha's teaching.

153
00:07:11,520 --> 00:07:14,040
 But you see, this was necessary.

154
00:07:14,040 --> 00:07:16,040
 This sort of development was necessary.

155
00:07:16,040 --> 00:07:18,820
 So apart from just, as Palanjani said, the differences of

156
00:07:18,820 --> 00:07:20,200
 opinion, there is also the

157
00:07:20,200 --> 00:07:24,370
 cultivation that allows people who are in this third

158
00:07:24,370 --> 00:07:27,200
 category that are unable to become

159
00:07:27,200 --> 00:07:29,930
 enlightened just by hearing the Buddha's teaching and need

160
00:07:29,930 --> 00:07:31,880
 the interpret, need the teacher to

161
00:07:31,880 --> 00:07:34,850
 explain it and to apply it to their, to give them a

162
00:07:34,850 --> 00:07:37,280
 technique of seeing this, which you'll

163
00:07:37,280 --> 00:07:39,430
 see in the commentaries, you'll see in many stories about

164
00:07:39,430 --> 00:07:40,520
 how they apply the Buddha's

165
00:07:40,520 --> 00:07:42,820
 teaching.

166
00:07:42,820 --> 00:07:46,080
 And you may even, if we had all of the records and all of

167
00:07:46,080 --> 00:07:48,100
 the history, we might even see

168
00:07:48,100 --> 00:07:51,920
 that in the Buddhist time they were developing, they were,

169
00:07:51,920 --> 00:07:54,600
 you know, had many, many techniques.

170
00:07:54,600 --> 00:07:55,100
 Some of the techniques that we have today and we see in the

171
00:07:55,100 --> 00:07:57,740
 Vasudimagam might have even

172
00:07:57,740 --> 00:07:59,080
 been practiced in the Buddhist time.

173
00:07:59,080 --> 00:08:01,010
 There may have been six parts to the walking step in the

174
00:08:01,010 --> 00:08:02,400
 Buddhist time, whether there were

175
00:08:02,400 --> 00:08:05,280
 or not.

176
00:08:05,280 --> 00:08:13,210
 The evolution of meditation techniques as a entity came

177
00:08:13,210 --> 00:08:16,760
 about not because people were

178
00:08:16,760 --> 00:08:19,140
 discontent with the Buddha's teaching, but because people

179
00:08:19,140 --> 00:08:20,360
 can't understand it and people

180
00:08:20,360 --> 00:08:25,580
 can't put it into practice without a regimen and a

181
00:08:25,580 --> 00:08:26,760
 technique.

182
00:08:26,760 --> 00:08:30,840
 And so it's at best, if you're to give it the benefit of

183
00:08:30,840 --> 00:08:33,120
 the doubt, interpretation at

184
00:08:33,120 --> 00:08:39,160
 its best is just the extrapolation for the purpose of

185
00:08:39,160 --> 00:08:42,240
 putting into practice by people

186
00:08:42,240 --> 00:08:45,530
 especially who aren't able to, who don't have very strong

187
00:08:45,530 --> 00:08:47,320
 faculties and thus aren't able

188
00:08:47,320 --> 00:08:52,080
 to immediately grasp it.

189
00:08:52,080 --> 00:08:58,580
 Maybe the meditation techniques that were known before the

190
00:08:58,580 --> 00:09:01,800
 Buddha started to teach his

191
00:09:01,800 --> 00:09:09,540
 kind of meditation and are practiced even today still like

192
00:09:09,540 --> 00:09:12,800
 I'm thinking of yoga and

193
00:09:12,800 --> 00:09:27,460
 their breath meditation and things like that might still,

194
00:09:27,460 --> 00:09:31,760
 how to say, come together and

195
00:09:31,760 --> 00:09:40,480
 make the picture more diffuse.

196
00:09:40,480 --> 00:09:43,990
 Allowing to see what sorts of practices they had practiced

197
00:09:43,990 --> 00:09:45,800
 in the Buddhist time and also

198
00:09:45,800 --> 00:09:51,080
 why the Buddha taught and why breath came so easily because

199
00:09:51,080 --> 00:09:53,680
 why was breath so prominent

200
00:09:53,680 --> 00:09:55,440
 in the Buddhist teaching?

201
00:09:55,440 --> 00:10:01,360
 Because that's what they were practicing at the time.

