1
00:00:00,000 --> 00:00:03,280
 I have such anger towards my mom because the way she

2
00:00:03,280 --> 00:00:05,000
 treated me when I was growing up.

3
00:00:05,000 --> 00:00:08,640
 However, now she is a changed person. She is very loving

4
00:00:08,640 --> 00:00:09,000
 now.

5
00:00:09,000 --> 00:00:12,350
 How can I reconcile this anger with myself? Every time I

6
00:00:12,350 --> 00:00:16,000
 think about the past, what she did, I become very angry.

7
00:00:16,000 --> 00:00:19,470
 It consumes me when I'm driving to work, working in daily

8
00:00:19,470 --> 00:00:20,000
 chores.

9
00:00:25,000 --> 00:00:30,980
 I kind of can understand what you are going through because

10
00:00:30,980 --> 00:00:40,000
 I had similar things to fight with many years ago.

11
00:00:40,000 --> 00:00:54,210
 I remember that one time, I was not practicing meditation

12
00:00:54,210 --> 00:00:56,000
 yet. I was practicing Zen meditation, but I haven't had a

13
00:00:56,000 --> 00:00:56,000
 teacher like Bante.

14
00:00:56,000 --> 00:01:07,060
 I remembered that what has happened is past and that

15
00:01:07,060 --> 00:01:11,000
 changed very much.

16
00:01:11,000 --> 00:01:21,830
 I suddenly saw that she is now different and she was then

17
00:01:21,830 --> 00:01:26,000
 already very loving.

18
00:01:26,000 --> 00:01:32,860
 It was not the same person anymore. I was not the same

19
00:01:32,860 --> 00:01:35,000
 person anymore.

20
00:01:35,000 --> 00:01:41,760
 The whole thing that caused anger and that troubled my

21
00:01:41,760 --> 00:01:46,000
 heart had passed already.

22
00:01:46,000 --> 00:01:54,230
 I was generally quite happy at that time, although anger

23
00:01:54,230 --> 00:01:58,000
 came up again and again.

24
00:01:58,000 --> 00:02:05,740
 So when I realized that it really is past what she did to

25
00:02:05,740 --> 00:02:11,370
 me and how I reacted and what she did, that I grew up and

26
00:02:11,370 --> 00:02:15,000
 that she grew up and we both changed,

27
00:02:15,000 --> 00:02:22,700
 I started to think of really forgiving her and now I would

28
00:02:22,700 --> 00:02:29,980
 say the best thing you can do is to start to forgive her

29
00:02:29,980 --> 00:02:34,450
 because that's the only way that you will overcome your

30
00:02:34,450 --> 00:02:36,000
 anger.

31
00:02:36,000 --> 00:02:50,920
 Forgiving has to be done mindfully, of course, because it

32
00:02:50,920 --> 00:02:55,000
 has to be something that grows out of your heart.

33
00:02:55,000 --> 00:03:01,050
 It's one thing to say, "I forgive her," and then still the

34
00:03:01,050 --> 00:03:06,420
 things come up. But when you see, when you understand what

35
00:03:06,420 --> 00:03:11,000
 has happened is past and it will not happen again because

36
00:03:11,000 --> 00:03:14,000
 she changed and she is now loving,

37
00:03:14,000 --> 00:03:23,260
 then you are in the position she has been in. You are the

38
00:03:23,260 --> 00:03:29,000
 bad part now and you are creating the Aku Sala.

39
00:03:29,000 --> 00:03:34,730
 You are creating the unwholesome in having the grudge, in

40
00:03:34,730 --> 00:03:43,740
 not forgiving her. But when you cultivate the Paramis, when

41
00:03:43,740 --> 00:03:50,520
 you cultivate, it's maybe too long to talk about the 10

42
00:03:50,520 --> 00:03:51,000
 Paramis.

43
00:03:51,000 --> 00:03:59,010
 Maybe Bhanda can do that better than I can. But I just want

44
00:03:59,010 --> 00:04:00,890
 to mention that when you practice forgiveness, you are

45
00:04:00,890 --> 00:04:09,000
 developing all the Paramis and this is such a wonderful,

46
00:04:09,000 --> 00:04:16,060
 beautiful thing you can do that you will be able to

47
00:04:16,060 --> 00:04:20,000
 overcome your anger for your Mother.

48
00:04:20,000 --> 00:04:24,700
 But you have to be mindful when you go to work, when you

49
00:04:24,700 --> 00:04:29,720
 are working and when you do your daily chores. It's an

50
00:04:29,720 --> 00:04:35,000
 active process for giving or letting go of anger.

51
00:04:35,000 --> 00:04:40,120
 See, for example, when you hold something in your hand that

52
00:04:40,120 --> 00:04:45,320
 is hurting you, that is hurting your hand, what you do is

53
00:04:45,320 --> 00:04:49,000
 you open your hand and you let go of it.

54
00:04:49,000 --> 00:04:53,470
 So the same is happening in your heart where you hold all

55
00:04:53,470 --> 00:04:59,050
 that grudge and that anger. You have to open the heart and

56
00:04:59,050 --> 00:05:01,000
 you have to let go of it.

57
00:05:01,000 --> 00:05:05,730
 But before you can do so, when you open your heart you have

58
00:05:05,730 --> 00:05:11,000
 to watch it. You have to observe mindfully what there is.

59
00:05:11,000 --> 00:05:14,000
 You have to observe all the anger that's coming up.

60
00:05:14,000 --> 00:05:20,040
 You have to observe all the grudge and all the defilements

61
00:05:20,040 --> 00:05:23,000
 that are related with it.

62
00:05:23,000 --> 00:05:29,160
 Once you have seen all your defilements related to that

63
00:05:29,160 --> 00:05:35,220
 anger, then you can start to build up the Paramis like the

64
00:05:35,220 --> 00:05:40,000
 love to answer her love with love.

65
00:05:40,000 --> 00:05:49,110
 You write later that she shifted your relationship from

66
00:05:49,110 --> 00:05:54,000
 cordial to even more closer.

67
00:05:54,000 --> 00:06:01,340
 So when you see your anger and you understand that it's

68
00:06:01,340 --> 00:06:09,440
 past, you understand that it's impermanent and gone and it

69
00:06:09,440 --> 00:06:16,330
 has changed in her, then you might be able to change it

70
00:06:16,330 --> 00:06:18,000
 within yourself as well.

71
00:06:18,000 --> 00:06:32,090
 And you can possibly answer her approaches and get closer

72
00:06:32,090 --> 00:06:44,000
 to yourself. Now please say something.

73
00:06:44,000 --> 00:06:50,240
 Well, not to say something different, but just to add how

74
00:06:50,240 --> 00:06:56,300
 it kind of works, how it plays out in practice because you

75
00:06:56,300 --> 00:07:00,000
 should never feel guilty about bad deeds.

76
00:07:00,000 --> 00:07:03,490
 You should be clear that they're bad, but you can have a

77
00:07:03,490 --> 00:07:07,270
 wonderful relationship with your mother and still be angry

78
00:07:07,270 --> 00:07:08,000
 at her.

79
00:07:08,000 --> 00:07:11,100
 You can have the anger inside and be like boiling up inside

80
00:07:11,100 --> 00:07:14,760
 and still say nice things to her, how it works, because you

81
00:07:14,760 --> 00:07:17,000
 know the anger is not the answer.

82
00:07:17,000 --> 00:07:21,000
 This anger is not how I should be relating to my mother.

83
00:07:21,000 --> 00:07:23,000
 This is what I'm working on.

84
00:07:23,000 --> 00:07:28,890
 This is important because to pretend that it's not there or

85
00:07:28,890 --> 00:07:34,420
 to force it out is impossible and it's going against the

86
00:07:34,420 --> 00:07:36,000
 truth of it.

87
00:07:36,000 --> 00:07:40,550
 What you're seeing in fact is wonderful. You're seeing that

88
00:07:40,550 --> 00:07:44,000
 anger is not something you can control.

89
00:07:44,000 --> 00:07:47,570
 What you're seeing is a great thing to see. What you're

90
00:07:47,570 --> 00:07:51,410
 experiencing now, top notch, because most people never come

91
00:07:51,410 --> 00:07:56,280
 to this or when they come to this, they try to find some

92
00:07:56,280 --> 00:07:59,000
 way to turn it off or to control it.

93
00:07:59,000 --> 00:08:00,760
 They say, "I can't control my anger. What am I going to do

94
00:08:00,760 --> 00:08:03,740
?" They go to find someone who knows how to control their

95
00:08:03,740 --> 00:08:07,220
 anger or they come to us and unfortunately we say, "No, no,

96
00:08:07,220 --> 00:08:10,000
 you can't control your anger. Stop trying to control it."

97
00:08:10,000 --> 00:08:14,410
 Then you're forced to turn yourself on your head and accept

98
00:08:14,410 --> 00:08:18,360
 that what you're experiencing is the truth. You can't

99
00:08:18,360 --> 00:08:22,000
 control the anger. You can't stop it. It consumes you.

100
00:08:22,000 --> 00:08:27,170
 It takes you away from your work. Why does it do that?

101
00:08:27,170 --> 00:08:30,330
 Because you've developed it. Because you've cultivated it

102
00:08:30,330 --> 00:08:31,000
 in the past.

103
00:08:31,000 --> 00:08:34,570
 You can't change what you're doing. I can't change what you

104
00:08:34,570 --> 00:08:38,060
've done in the past. What you've done in the past is

105
00:08:38,060 --> 00:08:40,000
 finished. We can't deal with that.

106
00:08:40,000 --> 00:08:45,370
 You've developed this propensity towards getting angry. You

107
00:08:45,370 --> 00:08:47,000
 shouldn't even try to change that.

108
00:08:47,000 --> 00:08:51,210
 What you should do is watch the anger as it comes up. "Oh,

109
00:08:51,210 --> 00:08:53,000
 here I'm angry at my mother. What's that like?"

110
00:08:53,000 --> 00:08:56,630
 Pretty quickly you're going to say, "This is not something

111
00:08:56,630 --> 00:08:58,000
 I want to develop."

112
00:08:58,000 --> 00:09:03,590
 You're going to change your habit, this incredibly strong

113
00:09:03,590 --> 00:09:07,160
 habit that you've built up. When you do something once, it

114
00:09:07,160 --> 00:09:09,000
 changes the way you behave.

115
00:09:09,000 --> 00:09:12,460
 When you do something a thousand times, it becomes quite

116
00:09:12,460 --> 00:09:15,800
 difficult to do anything but that or to react in any way

117
00:09:15,800 --> 00:09:17,000
 apart from that.

118
00:09:17,000 --> 00:09:20,530
 If you've developed a habit a thousand times, you can think

119
00:09:20,530 --> 00:09:23,360
 that you probably have to undevelop it or develop a

120
00:09:23,360 --> 00:09:26,260
 different habit, a different way of reacting, maybe a

121
00:09:26,260 --> 00:09:27,000
 thousand times.

122
00:09:27,000 --> 00:09:31,350
 The anger is not going to go away in the night time. But

123
00:09:31,350 --> 00:09:36,680
 what will go away, and this is why it's so clever to talk

124
00:09:36,680 --> 00:09:40,000
 about what a sotapanen gets rid of.

125
00:09:40,000 --> 00:09:44,240
 The way the Buddha was able to explain things shows quite

126
00:09:44,240 --> 00:09:48,190
 clearly why the first thing is a sotapanen gets rid of

127
00:09:48,190 --> 00:09:51,000
 nothing to do with greed or anger.

128
00:09:51,000 --> 00:09:53,730
 The sotapanen does not get rid of greed or anger, but a

129
00:09:53,730 --> 00:09:56,110
 person who has entered into the first stage of

130
00:09:56,110 --> 00:09:58,960
 enlightenment has gotten rid of the wrong view that says

131
00:09:58,960 --> 00:10:00,000
 anger is good.

132
00:10:00,000 --> 00:10:03,080
 So a sotapanen might still be angry at their mother. But

133
00:10:03,080 --> 00:10:05,840
 when the anger comes up, they don't say, "This is because

134
00:10:05,840 --> 00:10:07,000
 my mother did this."

135
00:10:07,000 --> 00:10:09,830
 They say, "This is because I've developed anger in the past

136
00:10:09,830 --> 00:10:12,930
. This is simply a habit that is a construct, an artificial

137
00:10:12,930 --> 00:10:17,000
 construct or formation that I've developed through habit."

138
00:10:17,000 --> 00:10:20,390
 And they patiently work the habit out. They change the

139
00:10:20,390 --> 00:10:22,340
 habit. So when the anger comes up, they see it and they

140
00:10:22,340 --> 00:10:25,000
 remind themselves, "This is anger."

141
00:10:25,000 --> 00:10:27,640
 And they say, "Oh, that's not a good thing." And they see

142
00:10:27,640 --> 00:10:31,480
 clearly that it's bringing them suffering and causing their

143
00:10:31,480 --> 00:10:35,450
 minds to burn up, causing them to have problems at work, at

144
00:10:35,450 --> 00:10:37,000
 daily chores and so on.

145
00:10:37,000 --> 00:10:39,500
 How does it feel to do your daily chores angry? I mean, it

146
00:10:39,500 --> 00:10:42,360
's actually probably not as bad as you think it is. You're

147
00:10:42,360 --> 00:10:45,240
 probably just lumping guilt on there and making your life

148
00:10:45,240 --> 00:10:46,000
 miserable.

149
00:10:46,000 --> 00:10:49,020
 So you do daily chores with anger. You can still do your

150
00:10:49,020 --> 00:10:52,610
 daily chores. So you're angry at work. Well, you can still

151
00:10:52,610 --> 00:10:53,000
 do your work.

152
00:10:53,000 --> 00:10:56,410
 When you follow it, when the anger comes up and you're not

153
00:10:56,410 --> 00:10:59,000
 mindful of it, you'll snap at people.

154
00:10:59,000 --> 00:11:02,090
 So the problem, of course the anger is a problem, but the

155
00:11:02,090 --> 00:11:04,690
 bigger problem is not the anger. It's the following the

156
00:11:04,690 --> 00:11:05,000
 anger.

157
00:11:05,000 --> 00:11:09,360
 You can be angry and talk to people nicely because you know

158
00:11:09,360 --> 00:11:12,000
 intellectually that anger is wrong.

159
00:11:12,000 --> 00:11:15,510
 Not exactly intellectually, but the view that you have,

160
00:11:15,510 --> 00:11:19,260
 which is based on your realization through the practice, is

161
00:11:19,260 --> 00:11:22,000
 that the anger is your problem, not theirs.

162
00:11:22,000 --> 00:11:25,490
 So you can be really angry at someone and still say, "This

163
00:11:25,490 --> 00:11:28,000
 anger is not something I want to use."

164
00:11:28,000 --> 00:11:31,720
 And understand clearly that this anger is a dangerous thing

165
00:11:31,720 --> 00:11:33,000
. Say to yourself, "Angry, anger."

166
00:11:33,000 --> 00:11:36,030
 And then reply to the person and they think, "Oh, that

167
00:11:36,030 --> 00:11:38,000
 person's quite nice and quite kind."

168
00:11:38,000 --> 00:11:42,330
 And you're sitting there angry, dealing with your anger,

169
00:11:42,330 --> 00:11:46,000
 which you've made the determination to get rid of.

170
00:11:46,000 --> 00:11:51,820
 But that you've built up and is causing your suffering as a

171
00:11:51,820 --> 00:11:53,000
 result.

172
00:11:53,000 --> 00:12:00,460
 If you look at you as you are now, you might probably come

173
00:12:00,460 --> 00:12:06,000
 to the conclusion that it's not too bad.

174
00:12:06,000 --> 00:12:10,680
 And I think this is some very important thing to see. First

175
00:12:10,680 --> 00:12:14,000
 because it's present, it's now, it's the present moment.

176
00:12:14,000 --> 00:12:21,340
 And the second thing is it's not too bad. So there is

177
00:12:21,340 --> 00:12:27,460
 nothing that you can insult her for, that you can be angry

178
00:12:27,460 --> 00:12:28,000
 for.

179
00:12:28,000 --> 00:12:32,750
 She did what she did and she probably didn't mean it bad

180
00:12:32,750 --> 00:12:38,400
 because I don't think that any parents, even if they do the

181
00:12:38,400 --> 00:12:40,000
 worst things,

182
00:12:40,000 --> 00:12:44,640
 that they don't do it because they are evil. They do so

183
00:12:44,640 --> 00:12:47,000
 because they don't know better.

184
00:12:47,000 --> 00:12:52,570
 And they do it because they have their past and their karma

185
00:12:52,570 --> 00:12:55,000
 and they do what they do.

186
00:12:55,000 --> 00:13:03,460
 And you become what you become because of it. And you have

187
00:13:03,460 --> 00:13:07,000
 in your hands what you become.

188
00:13:07,000 --> 00:13:11,250
 And probably the outcome is not too bad. So look at

189
00:13:11,250 --> 00:13:15,000
 yourself and don't point on your mother.

190
00:13:15,000 --> 00:13:23,000
 And when there is anger within you, then it is your anger.

191
00:13:23,000 --> 00:13:25,000
 It has to do with you.

192
00:13:25,000 --> 00:13:30,000
 And you have to work on it. You have to let go of it.

193
00:13:30,000 --> 00:13:33,740
 Yeah, and it's not so bad. That's the thing is we start to

194
00:13:33,740 --> 00:13:37,860
 hate ourselves about, this was the question, that question

195
00:13:37,860 --> 00:13:39,000
 about pornography.

196
00:13:39,000 --> 00:13:47,000
 This guy was addicted to sensual, to pornography and so on.

197
00:13:47,000 --> 00:13:49,830
 I said it's ruining my life. And I said, well, it's

198
00:13:49,830 --> 00:13:51,000
 probably not ruining your life.

199
00:13:51,000 --> 00:13:53,300
 But it's very easy to think that things are ruining your

200
00:13:53,300 --> 00:13:55,600
 life because you're still breathing, you still have a

201
00:13:55,600 --> 00:13:56,000
 computer.

202
00:13:56,000 --> 00:14:00,060
 You're still able to type this message out. So your life is

203
00:14:00,060 --> 00:14:02,000
 still actually going quite well.

204
00:14:02,000 --> 00:14:05,480
 Having a mother like this, you have a great life and you

205
00:14:05,480 --> 00:14:07,000
 should feel very happy.

206
00:14:07,000 --> 00:14:11,560
 And see that this is just a small problem. One thing that

207
00:14:11,560 --> 00:14:13,850
 happens, especially when we start to meditate, is we blow

208
00:14:13,850 --> 00:14:15,000
 problems up.

209
00:14:15,000 --> 00:14:18,190
 And a little anger comes up. Whoa, I'm really angry. But

210
00:14:18,190 --> 00:14:20,780
 for ordinary people, you have to remember they would have

211
00:14:20,780 --> 00:14:22,000
 been shouting by now.

212
00:14:22,000 --> 00:14:24,990
 So we're keeping it bottled up and we have to face it and

213
00:14:24,990 --> 00:14:28,860
 just say angry, angry without blowing up and without acting

214
00:14:28,860 --> 00:14:30,000
 on it and so on.

215
00:14:30,000 --> 00:14:33,340
 So it can be a lot of suffering when you're practicing. But

216
00:14:33,340 --> 00:14:37,450
 if you step back and look at it and say, well, you're not

217
00:14:37,450 --> 00:14:41,000
 hitting your mom, you're not beating her, I hope.

218
00:14:41,000 --> 00:14:45,130
 And if you are, you should stop. And you're probably not

219
00:14:45,130 --> 00:14:47,040
 yelling at her. And from the sounds of it, you're trying

220
00:14:47,040 --> 00:14:48,000
 your best to keep it bottled up.

221
00:14:48,000 --> 00:14:52,370
 So just add on to that mindfulness and you've got a pretty

222
00:14:52,370 --> 00:14:55,000
 good relationship as a result.

223
00:14:55,000 --> 00:15:00,130
 I mean, the other thing I wanted to say that's probably the

224
00:15:00,130 --> 00:15:03,450
 negative side is that there's all this talk about people

225
00:15:03,450 --> 00:15:07,010
 changing has to be tempered because sometimes people don't

226
00:15:07,010 --> 00:15:10,000
 change or sometimes people change superficially.

227
00:15:10,000 --> 00:15:12,920
 Never be too optimistic about either yourself or other

228
00:15:12,920 --> 00:15:16,040
 people. Never think that, oh, I've changed and now I'm

229
00:15:16,040 --> 00:15:17,000
 enlightened.

230
00:15:17,000 --> 00:15:19,080
 And never think about other people that they've changed and

231
00:15:19,080 --> 00:15:20,000
 they're enlightened.

232
00:15:20,000 --> 00:15:27,000
 I would never jump into creating strong bonds with people.

233
00:15:27,000 --> 00:15:30,680
 But I suppose there's some there's some exception that has

234
00:15:30,680 --> 00:15:34,000
 to be made, but we can talk about that in a second.

235
00:15:34,000 --> 00:15:36,260
 The point being, you don't want to be disappointed because

236
00:15:36,260 --> 00:15:38,330
 that would be the worst if you think, wow, my mother's

237
00:15:38,330 --> 00:15:40,000
 perfect and the problem is all with me.

238
00:15:40,000 --> 00:15:42,650
 And then you work out your problems to some extent and go

239
00:15:42,650 --> 00:15:44,000
 back and see your mother.

240
00:15:44,000 --> 00:15:47,140
 She starts acting poorly because of course she hasn't

241
00:15:47,140 --> 00:15:50,180
 practiced or probably she hasn't practiced strong

242
00:15:50,180 --> 00:15:51,000
 meditation.

243
00:15:51,000 --> 00:15:53,480
 And then you get angry again and then you have this

244
00:15:53,480 --> 00:15:57,200
 horrible relationship because it's all still, you know, the

245
00:15:57,200 --> 00:15:59,000
 seeds are still inside of you.

246
00:15:59,000 --> 00:16:01,680
 So when they're fed by her, you get back and suddenly your

247
00:16:01,680 --> 00:16:04,000
 relationship is horrible, horrible again.

248
00:16:04,000 --> 00:16:08,760
 Be mindful and be cautious and be careful when relating to

249
00:16:08,760 --> 00:16:10,000
 any person.

250
00:16:10,000 --> 00:16:17,230
 Forgive, but don't forget. So I want to add, don't forget

251
00:16:17,230 --> 00:16:22,000
 that she's getting older and she's going to die.

252
00:16:22,000 --> 00:16:27,510
 So you want to make sure that you have forgiven her before

253
00:16:27,510 --> 00:16:29,000
 she does so.

254
00:16:29,000 --> 00:16:31,000
 And you should talk to her.

255
00:16:31,000 --> 00:16:36,060
 That's really very important that you should talk to her

256
00:16:36,060 --> 00:16:39,000
 and find your love for her again.

257
00:16:39,000 --> 00:16:45,620
 But not. So I didn't mean to say to not, especially with

258
00:16:45,620 --> 00:16:48,000
 your mother, to not make amends.

259
00:16:48,000 --> 00:16:53,160
 Because really if a person says, go from cordial to

260
00:16:53,160 --> 00:16:57,000
 something more, well, just be clear that something more

261
00:16:57,000 --> 00:17:00,000
 should be your proper relationship as a son.

262
00:17:00,000 --> 00:17:02,950
 Doing your duty and being grateful for the good things that

263
00:17:02,950 --> 00:17:05,600
 she has done for you and for the love that she does have

264
00:17:05,600 --> 00:17:06,000
 for her.

265
00:17:06,000 --> 00:17:09,120
 And relating to her as a human being who has her own

266
00:17:09,120 --> 00:17:10,000
 problems.

267
00:17:10,000 --> 00:17:13,260
 But as a very special human being. She's the only person in

268
00:17:13,260 --> 00:17:15,000
 the world who gave birth to you.

269
00:17:15,000 --> 00:17:18,000
 And no one else can do that for you. And no one else did do

270
00:17:18,000 --> 00:17:18,000
 that.

271
00:17:18,000 --> 00:17:21,000
 No one else carried you around in her womb for nine months.

272
00:17:21,000 --> 00:17:24,630
 No one else clothed you and bathed you and fed you. No

273
00:17:24,630 --> 00:17:28,000
 matter how poorly a job she did.

274
00:17:28,000 --> 00:17:31,600
 You know, you have a special relationship with her and a

275
00:17:31,600 --> 00:17:34,000
 very strong karmic relationship.

276
00:17:34,000 --> 00:17:37,070
 Especially if it was a bad relationship. It means that

277
00:17:37,070 --> 00:17:41,000
 something brought you to this relationship.

278
00:17:41,000 --> 00:17:45,400
 So yeah, definitely it's someone special who you, at the

279
00:17:45,400 --> 00:17:48,980
 same time as saying you should be cautious, you have to put

280
00:17:48,980 --> 00:17:51,000
 special emphasis on the fact I'm her son.

281
00:17:51,000 --> 00:17:54,510
 She's my mother. So regardless of this anger, regardless of

282
00:17:54,510 --> 00:17:57,510
 this, you have to, you know, your default relationship with

283
00:17:57,510 --> 00:17:58,000
 your parents.

284
00:17:58,000 --> 00:18:02,030
 And this is something every meditator will see when they

285
00:18:02,030 --> 00:18:04,000
 just start to meditate.

286
00:18:04,000 --> 00:18:07,550
 The default relationship should be one of love and caring

287
00:18:07,550 --> 00:18:10,640
 and responsibility for each other. The parents have

288
00:18:10,640 --> 00:18:12,000
 responsibilities towards the children.

289
00:18:12,000 --> 00:18:15,000
 And the children have responsibilities towards the parent.

290
00:18:15,000 --> 00:18:18,460
 This is the only way to work out this strong karmic bond

291
00:18:18,460 --> 00:18:20,000
 that has caused us.

292
00:18:20,000 --> 00:18:25,380
 You were formed inside of her body. That's not an ordinary

293
00:18:25,380 --> 00:18:26,000
 state.

294
00:18:26,000 --> 00:18:28,020
 You know, things don't just happen like that in the

295
00:18:28,020 --> 00:18:29,000
 universe everywhere.

296
00:18:29,000 --> 00:18:32,380
 You don't just see, you know, things spontaneously arising

297
00:18:32,380 --> 00:18:34,000
 in other people's bodies.

298
00:18:34,000 --> 00:18:40,840
 It's a very special and strong bond, karmically, for you to

299
00:18:40,840 --> 00:18:44,600
 make the decision to be created and formed inside of

300
00:18:44,600 --> 00:18:46,000
 someone else's body.

301
00:18:46,000 --> 00:18:49,040
 So something that you have to be very clear about that this

302
00:18:49,040 --> 00:18:51,000
 is something you can't mess with.

303
00:18:51,000 --> 00:18:53,000
 I've messed with it. We've all messed with it.

304
00:18:53,000 --> 00:18:56,100
 And you get burnt terribly because it's a strong karmic

305
00:18:56,100 --> 00:18:57,000
 relationship.

306
00:18:57,000 --> 00:19:00,320
 And I think you really only see that when you begin to

307
00:19:00,320 --> 00:19:05,000
 practice meditation, but you sure do see it.

308
00:19:05,000 --> 00:19:08,390
 So yeah, you should talk to her and try to work that out as

309
00:19:08,390 --> 00:19:10,000
 quickly as possible.

310
00:19:10,000 --> 00:19:13,870
 Don't, the only question I wanted to give is don't become

311
00:19:13,870 --> 00:19:15,000
 overconfident and think,

312
00:19:15,000 --> 00:19:18,000
 oh, she's perfect now because people are not perfect.

313
00:19:18,000 --> 00:19:21,300
 And you still have to be, you still have to guard yourself

314
00:19:21,300 --> 00:19:24,000
 when she says nasty things because she might.

315
00:19:24,000 --> 00:19:27,000
 Sometime down the line, you have to be ready for them.

316
00:19:27,000 --> 00:19:29,960
 And when you get angry at her, of course, you have to be

317
00:19:29,960 --> 00:19:31,000
 ready for that.

318
00:19:31,000 --> 00:19:33,850
 Never think that now everything's perfect and everything's

319
00:19:33,850 --> 00:19:35,000
 going to be wonderful.

320
00:19:35,000 --> 00:19:41,000
 Always be like a soldier in enemy territory at all times.

321
00:19:41,000 --> 00:19:45,130
 But, you know, at the same time, you have to, you have to,

322
00:19:45,130 --> 00:19:49,000
 because you don't want her to die feeling bad about it, no?

323
00:19:49,000 --> 00:19:53,390
 And you don't want her to die with you not having sorted

324
00:19:53,390 --> 00:19:57,000
 things out because that will cause a great amount of worry

325
00:19:57,000 --> 00:19:59,590
 and agitation in your mind, and of course in her mind as

326
00:19:59,590 --> 00:20:00,000
 well.

