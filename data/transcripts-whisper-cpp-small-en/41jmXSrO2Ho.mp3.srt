1
00:00:00,000 --> 00:00:04,830
 When I meditate I usually do it laying down. I do it this

2
00:00:04,830 --> 00:00:07,000
 way because I like lots of concentration.

3
00:00:07,000 --> 00:00:13,210
 Sometimes I do change positions about of need. Out of need?

4
00:00:13,210 --> 00:00:15,000
 Is it a bad thing I do it this way?

5
00:00:15,000 --> 00:00:19,490
 I think it's got a problem. The first key is the part where

6
00:00:19,490 --> 00:00:23,000
 you say because I like lots of concentration.

7
00:00:23,000 --> 00:00:25,890
 For this kind of person I would be kind of mean and I would

8
00:00:25,890 --> 00:00:27,000
 say stop lying down.

9
00:00:27,000 --> 00:00:31,420
 Because that's what you like to do. See we want to see what

10
00:00:31,420 --> 00:00:34,000
 happens when you do something that you don't like to do.

11
00:00:34,000 --> 00:00:38,370
 And we want to show you what you've been building up based

12
00:00:38,370 --> 00:00:40,000
 on your attachments.

13
00:00:40,000 --> 00:00:43,360
 Because an enlightened being can be happy anywhere. So if

14
00:00:43,360 --> 00:00:47,000
 you have preference for something let's see what happens

15
00:00:47,000 --> 00:00:48,000
 when you don't get it.

16
00:00:48,000 --> 00:00:55,560
 That's kind of mean I suppose but it's pertinent. Lying

17
00:00:55,560 --> 00:00:59,180
 down there's no problem with doing lying meditation

18
00:00:59,180 --> 00:01:01,000
 theoretically.

19
00:01:01,000 --> 00:01:06,270
 But in practice it's quite dangerous because it can lead to

20
00:01:06,270 --> 00:01:10,000
 falling asleep, it can lead to daydreaming.

21
00:01:10,000 --> 00:01:14,740
 It's the position where the mind is most accustomed to

22
00:01:14,740 --> 00:01:20,000
 freedom, not having any kind of troubles or difficulties.

23
00:01:20,000 --> 00:01:24,780
 So there's very little potential to develop wisdom or to

24
00:01:24,780 --> 00:01:29,800
 develop renunciation, to develop patience, to develop all

25
00:01:29,800 --> 00:01:31,000
 sorts of good qualities.

26
00:01:31,000 --> 00:01:35,330
 And to let go because there's nothing that needs to be let

27
00:01:35,330 --> 00:01:37,000
 go of at that time.

28
00:01:37,000 --> 00:01:40,800
 What you can do is focus on the liking. When you lie down

29
00:01:40,800 --> 00:01:42,000
 it can be quite pleasant.

30
00:01:42,000 --> 00:01:46,250
 And so it can be useful to focus on that experience and to

31
00:01:46,250 --> 00:01:50,000
 try to free yourself from the clinging to it.

32
00:01:50,000 --> 00:01:58,290
 Lying is best used for people who have stressful jobs or

33
00:01:58,290 --> 00:02:05,000
 lifestyles who need concentration.

34
00:02:05,000 --> 00:02:09,860
 People who have for example ADHD might benefit a lot from

35
00:02:09,860 --> 00:02:14,010
 doing lying meditation because they need the concentration,

36
00:02:14,010 --> 00:02:16,000
 they need to balance it.

37
00:02:16,000 --> 00:02:19,120
 So if your mind is racing you can try doing lying

38
00:02:19,120 --> 00:02:23,000
 meditation but if you're tired or if you're kind of relaxed

39
00:02:23,000 --> 00:02:27,000
 I would work on the energy side.

40
00:02:27,000 --> 00:02:30,900
 So do walking meditation. Walking meditation is probably

41
00:02:30,900 --> 00:02:34,180
 not as peaceful or comfortable for you but that's really

42
00:02:34,180 --> 00:02:35,000
 the point.

43
00:02:35,000 --> 00:02:40,200
 The point is to see what your mind does when the body is

44
00:02:40,200 --> 00:02:49,000
 not comfortable, when the body has to face difficulties.

45
00:02:49,000 --> 00:02:51,920
 And you can sort of see how it's kind of spoiling you

46
00:02:51,920 --> 00:02:55,710
 because the other part is where you talk about changing

47
00:02:55,710 --> 00:02:56,000
 positions.

48
00:02:56,000 --> 00:03:00,270
 So what you're experiencing is the fact that even though it

49
00:03:00,270 --> 00:03:04,000
's pleasant to lie down it's really not satisfying.

50
00:03:04,000 --> 00:03:11,240
 And if you were to lie down for say six hours without not

51
00:03:11,240 --> 00:03:14,490
 asleep, if you were to lie there and meditate for say six

52
00:03:14,490 --> 00:03:17,670
 hours, twelve hours, some people do it for twenty-four

53
00:03:17,670 --> 00:03:18,000
 hours.

54
00:03:18,000 --> 00:03:21,150
 I've heard of someone doing lying meditation for twenty-

55
00:03:21,150 --> 00:03:25,060
four hours. It becomes quite difficult to stay in one

56
00:03:25,060 --> 00:03:26,000
 posture.

57
00:03:26,000 --> 00:03:30,080
 The meaning is that it can't satisfy you. It's not really

58
00:03:30,080 --> 00:03:34,130
 pleasant. You still have to always change your posture and

59
00:03:34,130 --> 00:03:37,390
 find a new posture that's more comfortable and then get a

60
00:03:37,390 --> 00:03:42,270
 nicer pillow and switch from an ordinary mattress to a

61
00:03:42,270 --> 00:03:45,000
 water bed or so on and so on and so on.

62
00:03:45,000 --> 00:03:48,050
 Then it's too hot, then it's too cold, then you're thirsty,

63
00:03:48,050 --> 00:03:51,000
 then you're hungry, then you have to go to the washroom,

64
00:03:51,000 --> 00:03:58,830
 then you're bored and you want to get up and check Facebook

65
00:03:58,830 --> 00:04:01,000
 or YouTube.

66
00:04:01,000 --> 00:04:04,930
 So nothing wrong with lying meditation but you should

67
00:04:04,930 --> 00:04:07,000
 understand how it's used.

68
00:04:07,000 --> 00:04:09,930
 The fact that you like concentration is a bit of a problem

69
00:04:09,930 --> 00:04:12,650
 because it becomes an attachment. You should try to let go

70
00:04:12,650 --> 00:04:17,430
 of that. It would be probably better to do some walking

71
00:04:17,430 --> 00:04:19,000
 meditation.

72
00:04:19,000 --> 00:04:22,490
 You get to see things about your mind that you couldn't see

73
00:04:22,490 --> 00:04:26,400
 before and you'll be able to see your attachments to the

74
00:04:26,400 --> 00:04:28,000
 concentration, for example.

