 And we're live here in the Buddha Center, the Deer Park, on
 a wonderful Sunday afternoon,
 evening, morning, midnight, depending on your location and
 the settings you've chosen for
 Second Life.
 So everyone, let's get in the mood, no?
 And put your meditator face on.
 Sit down, make sure you're sitting in real life, turn off
 the music, shut down Facebook,
 cross your legs, straighten your back, close your eyes.
 You can open your eyes if you want, but let's get med
itative.
 The teachings of the Lord Buddha are meant to address a
 problem.
 That's really, I think, how we should look at the teachings
 of the Buddha.
 It proposes that there's a problem and offers a solution.
 I suppose you could generalize and say that's the nature of
 religion.
 And you might even be so, if you wanted to be negative
 about it, you could say it's the
 religion game.
 It's how people, how religions attract followers.
 Those by pointing out a problem, always pointing out a
 problem that people weren't aware of.
 But it seems often that actually creating a problem where
 there was none, proposing a
 problem where it isn't obvious that there is one.
 In order to make people afraid or concerned, and then
 proposing to have the answer on the
 way out.
 And I think it's true that yes, there's a great potential
 for abuse here.
 And certainly there is a lot of abuse that goes on.
 Many religious systems, cults, and so on, exploit people's
 fears and their attachments
 in order to become rich and powerful and famous and so on.
 But it's essentially the role of a physician.
 It's the way of a doctor to be able to point out a problem
 and offer a solution.
 And sometimes the patient isn't even aware there's a
 problem.
 They go to the doctor for a checkup and leave the checkup
 with some bad news.
 Other times they come to the doctor with a problem, not
 knowing what the cause is, and
 the doctor is able to explain the cause and offer a
 solution.
 So both of these examples work in comparison to the
 Buddhist teaching.
 Some people live their lives with no thought that anything
 is wrong.
 And they might go to see the Buddha or go to see a Buddhist
 teacher.
 Just for discussion or to learn something or so on.
 And find out through their dialogue and through practicing
 meditation and so on that actually
 there was a big problem that they weren't aware of, that
 they were setting themselves
 up for a great amount of suffering in the future.
 But I think for most of us, the other form is the one that
 we're best able to relate
 to.
 It's the idea that we have suffering, we have a problem, we
 know there's a problem, we know
 that life's not perfect.
 We're able to see suffering in our lives.
 I don't think many people would deny that it exists in our
 lives.
 So I think this is important because it then dictates the
 way we approach the Buddhist
 teaching and it will shape our practice, our way of
 relating and engaging in the practice
 of the Buddhist teaching.
 In the way that we ask ourselves what's the problem and we
 remind ourselves that what
 we're doing here is trying to discover the problem and find
 a way to remove the problem.
 Why I think we have to keep this in mind is because it's
 easy to lose sight of it in the
 practice of Buddhism.
 The Buddha gave several examples of this that some people
 come to the Buddhist teaching and
 simply study it, engage in intensive study, memorization,
 intellectual discussion and
 debate.
 Many people nowadays engage in reading, studying.
 This is for many people the way of practicing a religion is
 to study it.
 For many people the practice is not so important as the
 study and the intellectual appreciation
 of the teachings.
 For other people it can be the memorization of the
 teachings.
 There are in Buddhist circles there are people who are
 actually able to remember vast tracts
 of discourses and teachings of the Buddha.
 Right now we have a project over email.
 It's a group project to memorize part of the Buddha's
 teaching starting from the first
 sutta in the Pali suttantapitika.
 People do this as a religious practice.
 This is common in other religions as well.
 I know in Judaism it's common or you can find people who
 are actually able to apparently
 take a Jewish Bible, the Torah, stick a needle through it
 or theoretically pretend that if
 they stuck a needle through the Torah they would be able to
 tell you which word based
 on the first word that it punctured.
 They would be able to tell you the word on each page from
 beginning to end.
 I think this has been lost in the modern era.
 There isn't so much of this reliance on the memorization of
 texts.
 But it certainly has been and continues to be in religious
 circles an important religious
 practice as though memorizing and chanting, being able to
 chant, being able to recite
 the teachings of the Buddha, being able to parrot it back.
 I suppose in modern days you find this in what we call
 quote dropping where people are
 able to quote famous teachers.
 I find this actually quite common in many schools of
 Buddhism that you'll find people rather
 than even quoting the Buddha, they're constantly quoting
 their teachers.
 "Well, my teacher says this and my teacher says that."
 Just though somehow it made it true or it had some bearing
 on the person that they were
 talking to.
 Without perhaps the ability to explain it or even just this
 reliance on that as a religious
 practice to be able to quote scripture and to recite and to
 chant and to memorize.
 Then the Buddha said there are other people who teach and
 even become great teachers without
 or at the same time neglecting their own practice.
 People who take all of their time to help other people or
 to teach to become famous
 teachers.
 There was a story in the Buddha's time or maybe it was
 after the Buddha's time.
 There was a story in ancient time about this monk who fell
 into this trap.
 I think I've told this story before.
 He was a great teacher and all of his students became
 enlightened to some degree or other.
 Some of them became sotapanas, some of them became zakitak
ami, some of them became anagami.
 He himself had neglected his own practice and was actually
 just a guy who happened to
 be able to teach quite well.
 He was able to explain the Buddha's teaching in such a way
 that people were able to practice
 it.
 He had an intellectual appreciation of it but he himself
 had never practiced or never
 exerted himself in intensive meditation practice.
 All of his students were reaching these higher states of
 enlightenment and he himself was
 running around teaching and helping other people.
 One day one of his students asked himself, one of his
 students who was an anagami which
 means someone who has attained the third stage of
 enlightenment and is destined when they
 die to never come back to the world again but instead be
 reborn in the god realms or
 the brahma realms and from there become fully enlightened
 and fully released and enter into
 the pure abodes they're called.
 He asked himself, "Here I am, I've realized this stage of
 enlightenment and all of these
 other people have.
 I wonder what stage of enlightenment our teacher has
 realized?"
 Through his incredible strength of mind he was able to
 discern for himself just by thinking
 about it, just by examining the situation and by sending
 his mind in this direction.
 He was able to ascertain that his teacher had indeed not
 attained any level of enlightenment.
 He thought, "Oh well this won't do.
 This is not a proper state of affairs."
 He thought to himself out of gratitude for his teacher that
 he owed it to him to help
 him become enlightened as well.
 He went up to his teacher and right when his teacher was
 very, very busy, waited for when
 his teacher was busy helping people, meeting with students
 and so on.
 He came up to him and he said, "Teacher, I think I would
 like to request that it's time
 for a lesson."
 The teacher said, "I'm sorry, how can I possibly have time
 for a lesson right now?
 I have many duties and people coming to see me and so on.
 I'm far too busy for that."
 The student of his sat down, crossed his legs and went into
 an absorbed state and started
 floating off the ground.
 He opened his eyes and said to the teacher, "You don't have
 time even for yourself.
 How could you possibly have time to teach anything to me?"
 He floated out of the room, showing to the teacher that
 indeed his students were on a
 higher level than he was.
 This of course shook the teacher up visibly.
 He decided that he had enough.
 This teaching business was obviously not the proper way to
 approach the Buddha's teaching.
 As the Buddha said, "Set yourself in what is right.
 Set yourself in what is right first, only then should you
 help others."
 He decided to go off into the forest.
 He was such a famous teacher that it was very difficult.
 He had to find a forest where there were no other people
 around where he wouldn't be bothered
 by people coming to ask him about meditation practice.
 Finally, he found this place, no humans in the area.
 He started his meditation.
 He worked very hard walking back and forth, sitting down.
 Still did he know that you can't really go anywhere where
 you're going to be alone,
 whether there are humans or not.
 There are always beings around, whether they be animals or
 in this case angels.
 There were angels in the forest watching him.
 That actually came down and started meditating with him.
 When he walked, they walked.
 When he sit, they sit.
 When he sat, they sat.
 He practiced and practiced and gained nothing from it.
 He was pushing very, very hard.
 Someone said he was probably practicing too hard, pushing
 himself too hard, wanting too
 much to become enlightened.
 Of course, the wanting, the needing, the desire was
 blocking him.
 It was stopping him from attaining the very thing he was
 looking for.
 He got more and more frustrated and more and more upset
 until finally he broke down.
 He was under such pressure that, "I'm this great teacher.
 If I don't become enlightened, what will I say to my
 students?"
 Until he finally snapped and he sat down and he started
 crying.
 He was crying and suddenly he heard someone else crying.
 He opened his eyes and there's this angel sitting down
 beside him crying, crying and
 crying and crying.
 He stops crying and he says, "What are you doing?"
 The angel stops crying and said, "Oh, I was crying."
 He said, "Why?"
 He said, "Well, you're the great teacher.
 Whatever you do, that's got to be the way to become
 enlightened.
 I've just been following you.
 I walked.
 When you sat, I sat.
 When you started crying, I started crying.
 I figured that's got to be the way to become enlightened."
 This hit him quite hard as well, made him quite ashamed of
 his behavior.
 As a result of that, he was able to wake up, let go and
 practice in such a way as to become
 enlightened.
 From the point of the story being, teaching people is not
 the most important point and
 it's easy to get off track in this way, going out and
 trying to explain to other people
 how great Buddhism is.
 It's something you can remember.
 It's always a sign of immaturity when people are trying to
 teach, when people approach
 you and start teaching you uninvited, unasked, when people
 start giving advice without you
 having asked or even intimated a desire for any.
 This is very common, especially when people begin to
 practice.
 It's called, it's called, it's an uppakiles, it's a defile
ment of insight.
 It's something that arises when you start practicing
 correctly.
 You start to think, "Wow, everyone else should practice
 this way," and you lose sight of
 your own practice and start trying to teach others.
 Another thing that people often do, that the Buddha
 mentioned, that people often do in
 approaching the teachings of the Buddha is to think about
 it, to ponder on it and to
 work it out in their mind.
 People who debate or people who like to think and like to
 create.
 People who write stories or write books.
 People who relate to the Buddha's teaching intellectually.
 These are the sorts of ways that people interact with the
 Buddha's teaching that are not getting
 closer to a solution to the problem.
 This is why it's important for us to always be asking
 ourselves, "What is the problem?"
 The problem is not that we don't know enough, that we haven
't read enough of the Buddha's
 teaching.
 It isn't that we don't know how to explain to other people.
 There are some meditators who are never able to explain to
 others, who find it very difficult
 to put into words the realizations that they have.
 On the other hand, there are people who work very hard to
 be able to explain things to
 others and yet themselves are gaining nothing and are not
 progressing on the path.
 The real problem, I guess you could say, is twofold.
 The problem that we have is that our minds are not tranquil
 enough and our minds are
 lacking in wisdom and understanding.
 The Buddha's approach to the problem and to finding a
 solution is twofold.
 There's one to calm the mind through the practice of
 meditation, to focus on an object, focus
 on reality in a way that stops the mind from flitting
 around superficially.
 That allows us to grasp reality or grasp the object in
 front of us in a meaningful way
 to focus our minds and to calm our minds down.
 You can do this either with a conceptual object or a part
 of reality.
 So a conceptual object would be something that you create
 in your mind, whether it be
 a light or a sound or a concept, a being, a god, an angel,
 an idea.
 Reality would be something that is arising and ceasing and
 is coming and going in the
 in experiential reality already that isn't created by the
 mind.
 Watching the breath, watching the stomach, watching the
 feet, watching pain or thoughts
 or emotions.
 And this serves to calm the mind down.
 The solution here is that our minds no longer give rise to
 things like greed, anger, depression,
 worry, fear and so on.
 Our minds become fixed, focused, happy, calm, peaceful,
 relaxed and for all intents and
 purposes become free from suffering for a time.
 Now the reason why this is not enough of course is it's
 still a contrived state that we've
 created this state through the work that we've done,
 through the pressure of our concentration
 which is able to suppress the defilements.
 It's a good thing because it makes our mind very clear and
 very pure.
 But it's a good thing primarily because it then allows us
 to fix things decisively, completely
 and irrevocably or permanently.
 Because no matter how much focus and concentration you
 apply to reality, that focus and concentration
 alone is not going to change your mind.
 It's not going to change the way you think.
 It's not going to change your habits.
 It's not going to cut off the potential for the arising of
 defilements.
 Wisdom on the other hand is the final solution and the
 final cure and wisdom is something
 that comes about through this clarity of mind.
 Once you have a clear mind, it's imperative that you focus
 entirely solely on ultimate
 reality.
 Whether you've been practicing to calm the mind based on a
 concept or whether ultimate
 reality, when you're practicing to gain wisdom and insight
 you have to focus on ultimate
 reality.
 Because you can't come to understand reality any other way.
 You can't come to understand things as they are when you're
 creating the object of your
 attention.
 In essence then we have a twofold problem.
 We have the problem of the defilements of the negative mind
 states and then we have the
 problem of the reason why they arise.
 To stop them from arising is easy.
 You focus the mind.
 But to remove the cause of their arising you have to go
 deeper and you have to come to
 understand why they arise.
 The reason why defilements arises through a
 misunderstanding of reality for what it
 is, not seeing things as they are, a superficial grasp of
 the experience in front of us at
 every moment.
 As an example, there are many examples.
 One example is pain.
 When you're sitting and you start to feel pain.
 It's a very superficial experience where right away
 convinced that it's a negative experience,
 that it's unpleasant, it's negative.
 And we're right away acting on our habit, our habitual way
 of approaching problems.
 We've already decided and begun to react and switch
 positions or find a solution, a way
 to get rid of the pain.
 When we focus on things like pain on a deeper level, when
 we look at them clearly and objectively,
 we come to see that actually it's not so at all.
 There's nothing intrinsically negative about things like
 pain or people yelling at us or
 smells or tastes or sounds, sights.
 The experience of the sense, even when there are people
 yelling at us or attacking us or
 unpleasant sights or sounds or smells, tastes, feelings,
 thoughts, bad memories, worries
 about the future, problems in our life, not having enough
 money, not having a job, not
 having a future, and so on and so on.
 Hunger, there's hot, cold.
 When we examine these things on a deeper level, which is
 exactly what we're doing through
 meditation, we come to see things in quite a different way,
 that there actually is nothing
 negative about any of these things.
 We don't react.
 We lose this habitual reaction and tendency to compartment
alize reality into the good
 and the bad, into what is unacceptable and what is
 unacceptable.
 Some people like it hot, some like it cold.
 Some people like loud music, some people hate loud music.
 It's actually a habit that we gain in our mind, and these
 can change and develop over
 time, obviously.
 People, when you take your first sip of beer or when you
 take your first cigarette, it's
 terrible, but over time you begin to change your habits and
 decide for yourself that it's
 a positive, a pleasant experience.
 People can go to the extent of believing that or of perce
iving smoke entering their lungs
 as being a pleasant experience.
 And so really, this is all we're doing in meditation.
 We're not trying to change things.
 Once we have this level of calm, we simply start to see
 things for what they are, and
 we start to understand things on a far deeper level, to the
 point that everything that arises
 we're able to see it for what it is.
 We lose this sense of attachment to it, this habitual
 reaction, that it's good or it's
 bad, this identification of it being me or mine.
 And the mind is able to find peace and freedom simply
 through wisdom, through understanding.
 Because there's nobody that, I think we can all agree that
 nobody wants suffering.
 Nobody wants to find peace and happiness.
 And so the reason that we create suffering for us is not
 because we, on a deep down level,
 want to suffer, it's because we don't understand that that
's what we're doing to ourselves.
 And once we can teach ourselves, once we can convince our
 minds that that's what we're
 doing, that this reaction, this mental process is creating
 suffering for us, then we'll stop.
 We stop naturally.
 There's no question about that.
 It's a theory, this is the theory I'm proposing to you, but
 it's a perfectly, absolutely testable
 and verifiable theory.
 If you start to meditate, you'll see that all it takes is a
 deeper, more comprehensive
 understanding of the things that you're already
 experiencing and already reacting to.
 Once you see them for what they are, and you see the
 process, and you see the way your
 mind is reacting, slowly but surely you'll change that.
 You'll convince yourself over time and with effort that
 what you're doing is hurting yourself
 and you'll change.
 It's a natural process.
 You don't have to want to change.
 You don't have to wish for yourself to become enlightened
 or even strive in any way.
 The striving is just to see things for what they are.
 And the closer you come to seeing things as they are, the
 more free and at ease and at
 peace with yourself you become.
 There you have the problem and a fairly clear solution.
 That's enough for today.
 Give everyone some time to ask questions if you have any
 and otherwise we can just sit
 here and meditate together and you're all welcome to go on
 with your lives.
 I'd like to thank you all for coming.
 We appreciate to have such an interesting group of people
 who return again and again
 to hear these talks and to support the Buddha Center in its
 projects.
 I'd like to wish for you all to be able to put into
 practice these teachings and to be
 able to practice meditation for the development of your own
 selves and for the attainment
 of real and true peace, happiness and freedom from
 suffering for you all.
 Thank you all for coming.
 Have a great day.
 [END]
