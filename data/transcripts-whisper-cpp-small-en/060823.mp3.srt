1
00:00:00,000 --> 00:00:26,290
 Today I would like to continue talking about the Mangala

2
00:00:26,290 --> 00:00:26,400
 Sutta.

3
00:00:26,400 --> 00:00:40,800
 The 38 Blessings taught by the Lord Buddha.

4
00:00:40,800 --> 00:00:48,000
 We've gotten through several days already.

5
00:00:48,000 --> 00:01:05,500
 I'd like to continue here at Nongpaka Meditation Center in

6
00:01:05,500 --> 00:01:09,600
 Nihong San.

7
00:01:09,600 --> 00:01:19,380
 Just to recap, the Mangala Sutta, the discourse on

8
00:01:19,380 --> 00:01:26,000
 blessings was taught in Chaitawana Mahawihara

9
00:01:26,000 --> 00:01:31,720
 in Savati in India.

10
00:01:31,720 --> 00:01:40,690
 One night at the very end of the night, the darkest part of

11
00:01:40,690 --> 00:01:42,720
 the night, there suddenly

12
00:01:42,720 --> 00:01:52,190
 arose a great light in Chaitawana, a light covering the

13
00:01:52,190 --> 00:01:55,760
 whole of the park.

14
00:01:55,760 --> 00:02:00,120
 Such a thing were to come to our center, we might be quite

15
00:02:00,120 --> 00:02:02,280
 excited and surprised.

16
00:02:02,280 --> 00:02:10,170
 You might think it was a UFO, or we might be quite agitated

17
00:02:10,170 --> 00:02:13,240
 to see such a sight.

18
00:02:13,240 --> 00:02:20,220
 But in the time of the Buddha, this was a very common scene

19
00:02:20,220 --> 00:02:22,520
 in Chaitawana.

20
00:02:22,520 --> 00:02:28,760
 Because all of the angels wanted to come down and hear the

21
00:02:28,760 --> 00:02:32,320
 teaching of the Lord Buddha.

22
00:02:32,320 --> 00:02:35,650
 Even today we make an invitation to the angels when we

23
00:02:35,650 --> 00:02:37,000
 teach the Dhamma.

24
00:02:37,000 --> 00:02:43,520
 Before we do the reciting of the Dhamma, we invite the

25
00:02:43,520 --> 00:02:45,720
 angels to listen.

26
00:02:45,720 --> 00:02:53,080
 It was such a rare thing to hear.

27
00:02:53,080 --> 00:02:57,560
 Angels are very special beings, many of them are quite

28
00:02:57,560 --> 00:02:59,600
 interested to hear the teaching

29
00:02:59,600 --> 00:03:02,880
 of the Lord Buddha.

30
00:03:02,880 --> 00:03:07,750
 So then this angel came down, having lit up the whole of Ch

31
00:03:07,750 --> 00:03:10,400
aitawana, and asked the Lord

32
00:03:10,400 --> 00:03:14,400
 Buddha, "What are the blessings?"

33
00:03:14,400 --> 00:03:21,740
 Because of course in that time everyone said this was a

34
00:03:21,740 --> 00:03:24,360
 blessing or that was a blessing.

35
00:03:24,360 --> 00:03:30,000
 Some people thought it was a blessing to have lots of cows.

36
00:03:30,000 --> 00:03:38,370
 Some people thought it was a blessing when you heard an owl

37
00:03:38,370 --> 00:03:42,000
 in the morning, or when you

38
00:03:42,000 --> 00:03:45,480
 had a horse shoe, or when you had a rabbit's foot.

39
00:03:45,480 --> 00:03:53,400
 Without these things were good luck, or auspicious.

40
00:03:53,400 --> 00:03:57,720
 The blessings of the Lord Buddha were quite different from

41
00:03:57,720 --> 00:03:58,880
 all of this.

42
00:03:58,880 --> 00:04:10,940
 So now we get to the verse that goes, "Dhanancha Dhammach

43
00:04:10,940 --> 00:04:12,080
arya Dhammacharya Dhammacharya Dhammacharya

44
00:04:12,080 --> 00:04:21,230
 Dhammacharya Dhammacharya Dhammacharya Dhammacharya Dhamm

45
00:04:21,230 --> 00:04:22,480
acharya Dhammacharya Dhammacharya

46
00:04:22,480 --> 00:04:26,310
 Dhammacharya Dhammacharya Dhammacharya Dhammacharya Dhamm

47
00:04:26,310 --> 00:04:28,480
acharya Dhammacharya Dhammacharya

48
00:04:28,480 --> 00:04:37,680
 Dhammacharya is the practice of the Dhamma, living by the D

49
00:04:37,680 --> 00:04:39,280
hamma.

50
00:04:39,280 --> 00:04:47,760
 Yatakanancha Sankoho to be helpful towards one's relatives.

51
00:04:47,760 --> 00:04:54,360
 Anavachani, kamani, kamani, to have deeds which are bl

52
00:04:54,360 --> 00:04:59,360
ameless, to have actions which are blameless,

53
00:04:59,360 --> 00:05:01,360
 blameless action.

54
00:05:01,360 --> 00:05:07,100
 Eyatamang, kalamutamang, this is a blessing in the highest

55
00:05:07,100 --> 00:05:08,080
 sense.

56
00:05:08,080 --> 00:05:15,990
 Dhanana, kucharati, kucharati means to give something which

57
00:05:15,990 --> 00:05:19,440
 you have to someone else who

58
00:05:19,440 --> 00:05:21,440
 might find it useful.

59
00:05:21,440 --> 00:05:27,600
 It's a way of giving up our greed and our stinginess and

60
00:05:27,600 --> 00:05:30,720
 our attachment to self.

61
00:05:30,720 --> 00:05:38,830
 When we give it's for, not just for the benefit of the

62
00:05:38,830 --> 00:05:43,840
 person who receives, when we do it,

63
00:05:43,840 --> 00:05:46,880
 we should think of it as a benefit for ourselves as well.

64
00:05:46,880 --> 00:05:52,500
 It helps us to destroy our attachment to ourselves and our

65
00:05:52,500 --> 00:05:56,080
 attachment to our possessions and

66
00:05:56,080 --> 00:05:58,960
 so on.

67
00:05:58,960 --> 00:06:01,680
 And Dhanana is of two kinds.

68
00:06:01,680 --> 00:06:05,680
 Dhanana is amisa dhanana and dhamma dhanana.

69
00:06:05,680 --> 00:06:08,720
 Of course there are many kinds here.

70
00:06:08,720 --> 00:06:10,720
 We'll keep it short.

71
00:06:10,720 --> 00:06:12,240
 We'll say there are two kinds.

72
00:06:12,240 --> 00:06:20,100
 Amisa dhanana is when you give objects, things that you own

73
00:06:20,100 --> 00:06:26,240
, when you have something useful.

74
00:06:26,240 --> 00:06:30,240
 Normally we have as a base four things.

75
00:06:30,240 --> 00:06:36,920
 One we give clothing to other people, two we give food to

76
00:06:36,920 --> 00:06:40,800
 other people, three we give shelter

77
00:06:40,800 --> 00:06:47,640
 to other people and four we give medicines to other people.

78
00:06:47,640 --> 00:06:50,820
 These four things are things which people really can't do

79
00:06:50,820 --> 00:06:52,540
 without even monks and nuns.

80
00:06:52,540 --> 00:06:55,660
 We have these four requisites and every day we do chanting

81
00:06:55,660 --> 00:06:57,260
 to remind ourselves that we

82
00:06:57,260 --> 00:07:02,040
 should use these requisites properly.

83
00:07:02,040 --> 00:07:04,200
 To give these things is a great blessing.

84
00:07:04,200 --> 00:07:07,440
 It makes the person who gives feel happy.

85
00:07:07,440 --> 00:07:11,480
 It creates friendship between the two people.

86
00:07:11,480 --> 00:07:16,430
 It creates love and it creates harmony and it creates

87
00:07:16,430 --> 00:07:19,000
 happiness in the world.

88
00:07:19,000 --> 00:07:21,440
 This is considered to be a great blessing.

89
00:07:21,440 --> 00:07:24,440
 But the second kind of gift is called dhamma dhanana.

90
00:07:24,440 --> 00:07:27,880
 It's the gift of the truth.

91
00:07:27,880 --> 00:07:34,160
 To give a gift of the teaching of the enlightened ones.

92
00:07:34,160 --> 00:07:40,000
 We say sappatthana, dhamma dhanana, jinnati.

93
00:07:40,000 --> 00:07:47,880
 The gift of the truth is greater than all other gifts.

94
00:07:47,880 --> 00:07:52,020
 So I've always encouraged other people when someone comes

95
00:07:52,020 --> 00:07:54,040
 to understand the truth, it's

96
00:07:54,040 --> 00:07:57,080
 also very important that we go and share this gift with

97
00:07:57,080 --> 00:07:58,120
 other people.

98
00:07:58,120 --> 00:08:01,400
 Not to be stingy and just keep it for ourselves.

99
00:08:01,400 --> 00:08:05,200
 We come and meditate so that we've got the benefit and we

100
00:08:05,200 --> 00:08:07,240
're going to go back and share

101
00:08:07,240 --> 00:08:09,040
 it with anyone.

102
00:08:09,040 --> 00:08:12,960
 When you realize what is the truth, when you come to

103
00:08:12,960 --> 00:08:15,880
 understand impermanence, you come

104
00:08:15,880 --> 00:08:18,720
 to understand why we suffer all the time.

105
00:08:18,720 --> 00:08:21,960
 Why we have this suffering or that suffering.

106
00:08:21,960 --> 00:08:23,240
 You come to let go of things.

107
00:08:23,240 --> 00:08:25,880
 You have to teach other people how to let go as well.

108
00:08:25,880 --> 00:08:32,780
 If you want to really be happy, you have to bring happiness

109
00:08:32,780 --> 00:08:35,120
 to other people.

110
00:08:35,120 --> 00:08:39,340
 This doesn't mean you just go around teaching everybody

111
00:08:39,340 --> 00:08:42,040
 else and teaching people who don't

112
00:08:42,040 --> 00:08:43,040
 want to hear.

113
00:08:43,040 --> 00:08:49,140
 Other people are not interested and don't really want to

114
00:08:49,140 --> 00:08:52,160
 hear what you have to say.

115
00:08:52,160 --> 00:08:56,800
 You have to give to people who want something from you.

116
00:08:56,800 --> 00:09:03,240
 Someone asks you to teach them.

117
00:09:03,240 --> 00:09:06,400
 In brief, this is a description of dhamma dhanana.

118
00:09:06,400 --> 00:09:09,660
 When we give, we should give to suitable people, the people

119
00:09:09,660 --> 00:09:13,000
 who it's appropriate to give to.

120
00:09:13,000 --> 00:09:15,930
 When we see that someone is in need of something, maybe

121
00:09:15,930 --> 00:09:18,360
 they don't understand this or they don't

122
00:09:18,360 --> 00:09:20,120
 understand that, we can teach them.

123
00:09:20,120 --> 00:09:23,310
 Or maybe they don't have any food or they don't have any

124
00:09:23,310 --> 00:09:25,000
 shelter so you can give them

125
00:09:25,000 --> 00:09:29,400
 food or give them shelter or medicine when they need.

126
00:09:29,400 --> 00:09:32,400
 This is a great blessing.

127
00:09:32,400 --> 00:09:34,960
 A blessing in the ultimate sense.

128
00:09:34,960 --> 00:09:36,480
 Much more of a blessing than cows.

129
00:09:36,480 --> 00:09:40,320
 If you have lots of cows, not a real and true blessing.

130
00:09:40,320 --> 00:09:44,350
 But when you give away your cows, then you can consider

131
00:09:44,350 --> 00:09:45,880
 this a blessing.

132
00:09:45,880 --> 00:09:51,730
 When you give away things that other people might find

133
00:09:51,730 --> 00:09:52,960
 useful.

134
00:09:52,960 --> 00:09:59,710
 Dhamma caarya ca is the practice of the dhamma, living by

135
00:09:59,710 --> 00:10:01,520
 the dhamma.

136
00:10:01,520 --> 00:10:04,360
 The dhamma of the Lord Buddha, the 84,000 teachings.

137
00:10:04,360 --> 00:10:07,960
 I can't explain all of how we practice all the dhamma.

138
00:10:07,960 --> 00:10:10,400
 Even the Mangala Sutta, it takes many days.

139
00:10:10,400 --> 00:10:15,920
 Just one sutta takes many days to explain.

140
00:10:15,920 --> 00:10:20,280
 How to practice all of these blessings, how to have these

141
00:10:20,280 --> 00:10:21,440
 blessings.

142
00:10:21,440 --> 00:10:25,980
 But we have some guidelines as to what is the dhamma, what

143
00:10:25,980 --> 00:10:27,840
 is the vinya of the Lord

144
00:10:27,840 --> 00:10:34,170
 Buddha, what is the teaching of the Lord Buddha, of the

145
00:10:34,170 --> 00:10:36,560
 enlightened one.

146
00:10:36,560 --> 00:10:44,580
 The characteristics of the dhamma are for giving up of

147
00:10:44,580 --> 00:10:46,440
 craving.

148
00:10:46,440 --> 00:10:48,350
 Whatever it is that leads us to give up craving, you can

149
00:10:48,350 --> 00:10:49,600
 say that is the dhamma of the Lord

150
00:10:49,600 --> 00:10:52,520
 Buddha.

151
00:10:52,520 --> 00:10:54,610
 Whatever leads us to be content with what we have, or

152
00:10:54,610 --> 00:10:55,760
 content even with little.

153
00:10:55,760 --> 00:11:02,730
 When we only have very little to be content, whatever leads

154
00:11:02,730 --> 00:11:05,080
 to contentment.

155
00:11:05,080 --> 00:11:10,680
 Whatever leads to give up suffering.

156
00:11:10,680 --> 00:11:16,070
 Whatever leads us to give up suffering, this is the dhamma

157
00:11:16,070 --> 00:11:18,080
 of the Lord Buddha.

158
00:11:18,080 --> 00:11:25,280
 Whatever leads us to develop effort, to be able to work

159
00:11:25,280 --> 00:11:29,760
 harder, to be more energetic.

160
00:11:29,760 --> 00:11:35,100
 Whatever leads us to put out effort, this is the teaching

161
00:11:35,100 --> 00:11:37,200
 of the Lord Buddha.

162
00:11:37,200 --> 00:11:45,360
 Whatever makes us easy to satisfy, and so on.

163
00:11:45,360 --> 00:11:52,320
 Whatever leads us to nirvana, to freedom from suffering.

164
00:11:52,320 --> 00:11:55,160
 We want to put it all in brief.

165
00:11:55,160 --> 00:11:58,770
 We can summarize the dhamma of the Lord Buddha as the four

166
00:11:58,770 --> 00:12:00,960
 foundations of mindfulness.

167
00:12:00,960 --> 00:12:05,050
 When we practice mindfulness in the body, watching the feet

168
00:12:05,050 --> 00:12:07,360
, watching the hands, watching

169
00:12:07,360 --> 00:12:18,240
 the stomach rise, this is the teaching of the dhamma.

170
00:12:18,240 --> 00:12:19,240
 This is the practice.

171
00:12:19,240 --> 00:12:21,430
 We should all feel very proud that some people, they never

172
00:12:21,430 --> 00:12:22,640
 come to practice the dhamma.

173
00:12:22,640 --> 00:12:25,520
 They only listen and study the dhamma.

174
00:12:25,520 --> 00:12:29,020
 But when it comes to practicing, they never had the thought

175
00:12:29,020 --> 00:12:30,520
 that they should come and

176
00:12:30,520 --> 00:12:32,520
 actually practice.

177
00:12:32,520 --> 00:12:38,270
 So here we are coming to practice, all the time rising,

178
00:12:38,270 --> 00:12:40,400
 walking, right?

179
00:12:40,400 --> 00:12:45,760
 Goes that stuff, goes that stuff.

180
00:12:45,760 --> 00:12:48,260
 Practice mindfulness in the feelings when we have pain, or

181
00:12:48,260 --> 00:12:49,040
 aching, or so.

182
00:12:49,040 --> 00:12:58,940
 We say pain, pain, we feel happy, happy, happy, calm, calm,

183
00:12:58,940 --> 00:13:00,320
 we go.

184
00:13:00,320 --> 00:13:04,880
 We're thinking about the past, the future, we're thinking,

185
00:13:04,880 --> 00:13:07,040
 we're liking, disliking,

186
00:13:07,040 --> 00:13:12,300
 grousiness, distraction, doubt, we acknowledge liking, we

187
00:13:12,300 --> 00:13:15,920
're thinking, disliking, grousiness.

188
00:13:15,920 --> 00:13:20,040
 This is the teaching of the Lord Buddha.

189
00:13:20,040 --> 00:13:24,640
 When we practice this, this is a great blessing for us.

190
00:13:24,640 --> 00:13:28,880
 It leads us to see clearly, both inside of ourselves and in

191
00:13:28,880 --> 00:13:30,720
 the world around us, and

192
00:13:30,720 --> 00:13:33,880
 it leads us to find freedom.

193
00:13:33,880 --> 00:13:38,190
 Everyone wants to be free and leads us to peace, and

194
00:13:38,190 --> 00:13:41,520
 everyone wants to have peace, leads

195
00:13:41,520 --> 00:13:46,800
 us to happiness, everyone wants to be happy.

196
00:13:46,800 --> 00:13:58,400
 We have to be helpful towards our relatives.

197
00:13:58,400 --> 00:14:00,800
 Our relatives are not going to go into great detail.

198
00:14:00,800 --> 00:14:03,150
 It's obvious that when we live in the world, we have to be

199
00:14:03,150 --> 00:14:04,760
 helpful towards our relatives,

200
00:14:04,760 --> 00:14:08,260
 never give them up, never throw away or forget about our

201
00:14:08,260 --> 00:14:09,240
 relatives.

202
00:14:09,240 --> 00:14:12,560
 Even monks have to rely on their relatives.

203
00:14:12,560 --> 00:14:16,930
 Everyone, a person who has relatives, this is a very

204
00:14:16,930 --> 00:14:18,480
 special thing.

205
00:14:18,480 --> 00:14:21,170
 But here, when we're in the Dhamma, we have to consider

206
00:14:21,170 --> 00:14:22,680
 each other to be relatives.

207
00:14:22,680 --> 00:14:24,200
 We have to rely on each other.

208
00:14:24,200 --> 00:14:26,080
 How do we rely on each other?

209
00:14:26,080 --> 00:14:30,000
 Sometimes we can teach each other the right way.

210
00:14:30,000 --> 00:14:35,560
 But most important is we give a supportive environment.

211
00:14:35,560 --> 00:14:38,790
 We invite people here to practice, make it a suitable

212
00:14:38,790 --> 00:14:40,760
 environment to practice in.

213
00:14:40,760 --> 00:14:45,360
 We have everyone take on the practice of the Dhamma and the

214
00:14:45,360 --> 00:14:46,200
 Vinya.

215
00:14:46,200 --> 00:14:48,280
 We have to keep certain rules.

216
00:14:48,280 --> 00:14:55,440
 We have to make it conducive to follow these rules.

217
00:14:55,440 --> 00:14:59,250
 We have to support each other to follow the rules that we

218
00:14:59,250 --> 00:15:00,520
 have in place.

219
00:15:00,520 --> 00:15:05,900
 We have to support each other to practice the Dhamma which

220
00:15:05,900 --> 00:15:07,120
 we teach.

221
00:15:07,120 --> 00:15:15,180
 When we see each other practicing, we see each other

222
00:15:15,180 --> 00:15:16,640
 keeping to the practice which we

223
00:15:16,640 --> 00:15:17,640
 follow.

224
00:15:17,640 --> 00:15:19,160
 It's a great support for us.

225
00:15:19,160 --> 00:15:22,010
 If we just had to go and practice often the forest by

226
00:15:22,010 --> 00:15:23,960
 ourselves, it may be very difficult.

227
00:15:23,960 --> 00:15:27,200
 We don't have the support of our relatives.

228
00:15:27,200 --> 00:15:32,480
 We can say we are sisters and brothers in the Dhamma.

229
00:15:32,480 --> 00:15:35,120
 This is very important.

230
00:15:35,120 --> 00:15:38,200
 It's a great blessing.

231
00:15:38,200 --> 00:15:42,720
 Some meditators have even expressed their feeling of how

232
00:15:42,720 --> 00:15:44,480
 great it is to practice in

233
00:15:44,480 --> 00:15:48,570
 a group, how much support it gives to you when you practice

234
00:15:48,570 --> 00:15:49,400
 together.

235
00:15:49,400 --> 00:16:04,160
 Anavachani kamani nita man kamatamang.

236
00:16:04,160 --> 00:16:09,480
 Anavachani kamani means whatever we do.

237
00:16:09,480 --> 00:16:16,090
 Whatever we do has to be free from blame, free from

238
00:16:16,090 --> 00:16:21,360
 treachery, free from bad intentions.

239
00:16:21,360 --> 00:16:28,520
 That our actions before we speak, before we act, we have to

240
00:16:28,520 --> 00:16:32,200
 be sure that our mind is pure.

241
00:16:32,200 --> 00:16:38,180
 We should never act or speak with a heart which is covered

242
00:16:38,180 --> 00:16:41,320
 over with greed or with anger

243
00:16:41,320 --> 00:16:44,320
 or with delusion.

244
00:16:44,320 --> 00:16:48,600
 When we speak or when we act, our minds should be clear.

245
00:16:48,600 --> 00:16:50,650
 We should know that this is for the benefit of ourselves

246
00:16:50,650 --> 00:16:51,720
 and the benefit of the other

247
00:16:51,720 --> 00:16:52,720
 person.

248
00:16:52,720 --> 00:16:56,190
 This is for happiness and for peace, for freedom from

249
00:16:56,190 --> 00:16:57,200
 suffering.

250
00:16:57,200 --> 00:17:01,570
 This is not in order to bring hardship to the other person

251
00:17:01,570 --> 00:17:03,520
 or to bring suffering to

252
00:17:03,520 --> 00:17:04,520
 the other person.

253
00:17:04,520 --> 00:17:11,680
 You're suffering to anyone.

254
00:17:11,680 --> 00:17:14,160
 We can see how important it is to be mindful.

255
00:17:14,160 --> 00:17:18,790
 It's very easy to forget and to say and to do things which

256
00:17:18,790 --> 00:17:21,320
 are a cause for suffering.

257
00:17:21,320 --> 00:17:28,320
 There's no need to hate ourselves for bad things we've done

258
00:17:28,320 --> 00:17:31,120
 but only to wake up, wake

259
00:17:31,120 --> 00:17:37,560
 ourselves up, bring ourselves to see everything that we do,

260
00:17:37,560 --> 00:17:40,720
 to know before we act, before

261
00:17:40,720 --> 00:17:41,720
 we speak.

262
00:17:41,720 --> 00:17:45,280
 Is this a good thing to say, a good thing to do?

263
00:17:45,280 --> 00:17:46,280
 To have mindfulness.

264
00:17:46,280 --> 00:17:52,240
 Why we train ourselves every moment to be mindful?

265
00:17:52,240 --> 00:17:54,820
 Because when we practice in this way we can see when anger

266
00:17:54,820 --> 00:17:56,280
 arises, we can see when greed

267
00:17:56,280 --> 00:18:01,120
 arises.

268
00:18:01,120 --> 00:18:05,100
 At least when we practice then we can cut off the link

269
00:18:05,100 --> 00:18:07,720
 between our bad intentions and our

270
00:18:07,720 --> 00:18:12,090
 actions so that when we act it will be free from greed or

271
00:18:12,090 --> 00:18:14,080
 freedom from anger.

272
00:18:14,080 --> 00:18:19,910
 Then when we take this into our life we can also act and

273
00:18:19,910 --> 00:18:23,240
 speak in a way that is free from

274
00:18:23,240 --> 00:18:27,560
 evil intention, from bad intention.

275
00:18:27,560 --> 00:18:32,250
 This is a great blessing that leads us to be free from bad

276
00:18:32,250 --> 00:18:34,920
 karma, free from the results

277
00:18:34,920 --> 00:18:41,760
 of bad karma, free from guilt, free from suffering.

278
00:18:41,760 --> 00:18:47,890
 This means we are truly mindful and we can be free from

279
00:18:47,890 --> 00:18:49,600
 suffering.

280
00:18:49,600 --> 00:18:55,090
 So these are the latest set of blessings, among the 38

281
00:18:55,090 --> 00:18:56,440
 blessings.

282
00:18:56,440 --> 00:18:57,440
 1

283
00:18:57,440 --> 00:18:58,440
 1

284
00:18:58,440 --> 00:18:59,440
 1

285
00:18:59,440 --> 00:19:00,440
 1

