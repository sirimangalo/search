1
00:00:00,000 --> 00:00:04,620
 Okay welcome everyone to our evening session among radio.

2
00:00:04,620 --> 00:00:05,840
 First up is

3
00:00:05,840 --> 00:00:14,240
 announcements. Have any announcements? Jens did arrive and

4
00:00:14,240 --> 00:00:16,240
 became Jasa in the

5
00:00:16,240 --> 00:00:22,730
 meantime. That will be his name if he ordains, when he ord

6
00:00:22,730 --> 00:00:24,360
ains not if but when.

7
00:00:24,360 --> 00:00:35,780
 They are going to try to get a visa tomorrow for Sumedha

8
00:00:35,780 --> 00:00:39,000
 and Jasa.

9
00:00:39,000 --> 00:00:46,680
 We have some Sri Lankan meditators here. For now six yogis,

10
00:00:46,680 --> 00:00:50,200
 the top rates so far.

11
00:00:50,200 --> 00:00:58,680
 And another meditator is coming soon, Sri Lankan as well on

12
00:00:58,680 --> 00:01:02,120
 the 17th. So we are

13
00:01:02,120 --> 00:01:10,650
 booked out I would say. We're running in full speed. So

14
00:01:10,650 --> 00:01:13,600
 yeah Jasa or the yogi formerly

15
00:01:13,600 --> 00:01:18,730
 known as Jens. It's an old meditator from Thailand and a

16
00:01:18,730 --> 00:01:20,640
 good friend of Palanyani.

17
00:01:20,640 --> 00:01:29,840
 So when she went left to go back to Thailand, Jens came to

18
00:01:29,840 --> 00:01:30,400
 Wathambuthong.

19
00:01:30,400 --> 00:01:34,880
 And it was just the two of us there for a while with the

20
00:01:34,880 --> 00:01:36,880
 old monk and that's

21
00:01:36,880 --> 00:01:41,760
 when I met him. And then he left and Palanyani came and

22
00:01:41,760 --> 00:01:42,840
 ordained as a nun.

23
00:01:42,840 --> 00:01:49,050
 And since then he's been back here to Sri Lanka. He came to

24
00:01:49,050 --> 00:01:49,880
 Sri Lanka to visit

25
00:01:49,880 --> 00:01:54,070
 and liked so much that he went back to Germany, cleared up

26
00:01:54,070 --> 00:01:56,040
 his business and came

27
00:01:56,040 --> 00:02:02,030
 back and is now joining our community for long term.

28
00:02:02,030 --> 00:02:04,400
 Another announcement is we in

29
00:02:04,400 --> 00:02:09,400
 all likelihood will be going to Thailand. That's myself, J

30
00:02:09,400 --> 00:02:10,560
asa and Sumedha.

31
00:02:10,560 --> 00:02:15,160
 But we're not arranging to travel together because I can't

32
00:02:15,160 --> 00:02:15,840
 arrange to

33
00:02:15,840 --> 00:02:18,780
 travel together with a woman. So I'm arranging to travel

34
00:02:18,780 --> 00:02:20,280
 with Jasa and

35
00:02:20,280 --> 00:02:24,320
 Sumedha can arrange to travel with Jasa as well. Something

36
00:02:24,320 --> 00:02:26,640
 like that. Anyway

37
00:02:26,640 --> 00:02:32,010
 that's just it. I join you in my heart and in my thoughts.

38
00:02:32,010 --> 00:02:32,640
 Well we might have to

39
00:02:32,640 --> 00:02:37,440
 talk about that. You may be interested in coming along. We

40
00:02:37,440 --> 00:02:37,600
 should

41
00:02:37,600 --> 00:02:42,440
 talk about that. We definitely should. I'm so jealous that

42
00:02:42,440 --> 00:02:44,360
 you are going and I can't see.

43
00:02:44,360 --> 00:02:49,520
 Because they are going to see Ajahn Tong and Ajahn Supan

44
00:02:49,520 --> 00:02:54,120
 and that would be so

45
00:02:54,120 --> 00:02:58,240
 good if I ever could come. So maybe we'll stay with Ajahn

46
00:02:58,240 --> 00:03:00,680
 Tong at least 10 days

47
00:03:00,680 --> 00:03:05,550
 but probably more like two weeks and then maybe have

48
00:03:05,550 --> 00:03:06,440
 another week to to go

49
00:03:06,440 --> 00:03:09,440
 around other places. I have to tell my students in Bangkok

50
00:03:09,440 --> 00:03:10,240
 they want me to go.

51
00:03:10,240 --> 00:03:14,930
 So I might be doing a little bit of teaching there. And

52
00:03:14,930 --> 00:03:17,320
 then I will travel if

53
00:03:17,320 --> 00:03:25,970
 it would be so I would travel with Sumedha. So that's

54
00:03:25,970 --> 00:03:26,160
 another

55
00:03:26,160 --> 00:03:33,420
 announcement. We're getting more set or more structured

56
00:03:33,420 --> 00:03:36,160
 here. So we have now

57
00:03:36,160 --> 00:03:39,620
 a fairly good routine of 4.30 a.m. We have a group

58
00:03:39,620 --> 00:03:42,760
 meditation and 9 p.m. we're

59
00:03:42,760 --> 00:03:45,890
 doing and this is kind of a new thing we're doing. Abhidham

60
00:03:45,890 --> 00:03:47,480
ma chanting. But

61
00:03:47,480 --> 00:03:52,580
 we're doing it Allah Thai novice style. Probably not just

62
00:03:52,580 --> 00:03:53,240
 Thai novice but

63
00:03:53,240 --> 00:03:56,280
 novice monk style where they all get together in a room and

64
00:03:56,280 --> 00:03:56,880
 read their own

65
00:03:56,880 --> 00:04:02,250
 books. And it's the most chaotic sound you've ever heard. I

66
00:04:02,250 --> 00:04:03,360
 recorded it once. It's

67
00:04:03,360 --> 00:04:07,740
 quite interesting listening to all these novices reciting

68
00:04:07,740 --> 00:04:09,880
 Pali. So that's what we

69
00:04:09,880 --> 00:04:13,570
 do. We sit there and we focus on our own text and we do

70
00:04:13,570 --> 00:04:14,920
 that for a half an hour

71
00:04:14,920 --> 00:04:19,680
 and then we have a brief meeting and then group meditation.

72
00:04:19,680 --> 00:04:20,680
 I mean I'll be

73
00:04:20,680 --> 00:04:24,430
 there. I'll try to be there till 11. And then during the

74
00:04:24,430 --> 00:04:25,080
 day we're trying to

75
00:04:25,080 --> 00:04:28,930
 do every day almost a two o'clock group meeting and that's

76
00:04:28,930 --> 00:04:29,920
 when we'll do the

77
00:04:29,920 --> 00:04:34,330
 Dhammapada videos and other videos. Just try to give some D

78
00:04:34,330 --> 00:04:35,440
hamma every day.

79
00:04:35,440 --> 00:04:40,620
 Both for the people here and for the internet community. So

80
00:04:40,620 --> 00:04:42,320
 that's another

81
00:04:42,320 --> 00:04:45,480
 useful thing to let people know that this place is really

82
00:04:45,480 --> 00:04:46,320
 falling in many

83
00:04:46,320 --> 00:04:53,560
 ways it's falling into place. I think we could mention that

84
00:04:53,560 --> 00:04:55,000
 possibly tomorrow

85
00:04:55,000 --> 00:05:00,630
 workers will come. They're not actually. We talked about it

86
00:05:00,630 --> 00:05:01,400
. That was a false

87
00:05:01,400 --> 00:05:05,400
 alarm. But soon we'll have two more rooms. Soon they're

88
00:05:05,400 --> 00:05:06,200
 going to pick

89
00:05:06,200 --> 00:05:12,480
 that up. But it takes time. Everything here is very slow.

90
00:05:12,480 --> 00:05:14,720
 It's kind of a laid

91
00:05:14,720 --> 00:05:22,120
 back sort of culture. Okay so that's our announcements for

92
00:05:22,120 --> 00:05:22,600
 this evening.

93
00:05:22,600 --> 00:05:26,040
 You can end it there.

94
00:05:26,040 --> 00:05:36,040
 [BLANK_AUDIO]

