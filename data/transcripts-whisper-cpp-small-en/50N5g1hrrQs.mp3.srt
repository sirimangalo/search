1
00:00:00,000 --> 00:00:05,460
 Bhante, what is your opinion about sitting through pain?

2
00:00:05,460 --> 00:00:08,730
 Many teachers advocate it because it creates equanimity and

3
00:00:08,730 --> 00:00:09,660
 you learn how to deal with

4
00:00:09,660 --> 00:00:15,170
 your resistance and others say it is extreme asceticism and

5
00:00:15,170 --> 00:00:18,120
 to be avoided, many things.

6
00:00:18,120 --> 00:00:27,170
 Anyone who says that pain, that the forbearance or the

7
00:00:27,170 --> 00:00:33,280
 bearing with pain is extremism, is

8
00:00:33,280 --> 00:00:38,660
 an extreme and therefore in the sense of bad practice is

9
00:00:38,660 --> 00:00:43,080
 going against the Buddhist teaching,

10
00:00:43,080 --> 00:00:45,680
 doesn't understand the Buddhist teaching.

11
00:00:45,680 --> 00:00:50,640
 Maybe well meaning but misguided.

12
00:00:50,640 --> 00:00:56,890
 The Buddha said clearly in several places that one should,

13
00:00:56,890 --> 00:01:00,360
 true patience means forbearing

14
00:01:00,360 --> 00:01:06,600
 against pain even such that it might take your life, even

15
00:01:06,600 --> 00:01:09,760
 so extreme that it might kill

16
00:01:09,760 --> 00:01:13,160
 you.

17
00:01:13,160 --> 00:01:20,670
 And to understand this or to verify this, to argue this, we

18
00:01:20,670 --> 00:01:23,480
 can point to what do we

19
00:01:23,480 --> 00:01:32,080
 mean by what is the Buddhist teaching?

20
00:01:32,080 --> 00:01:36,720
 What is the path that the Buddha taught?

21
00:01:36,720 --> 00:01:41,870
 What is the path to my understanding is that it's the path

22
00:01:41,870 --> 00:01:44,600
 to become completely objective

23
00:01:44,600 --> 00:01:50,890
 and let's say invincible to all experience such that no

24
00:01:50,890 --> 00:01:54,360
 experience that arises could

25
00:01:54,360 --> 00:01:58,480
 possibly be a cause for suffering, that there would be no

26
00:01:58,480 --> 00:02:01,520
 clinging to any experience whatsoever,

27
00:02:01,520 --> 00:02:06,440
 no reaction, no emotional reaction to any experience in the

28
00:02:06,440 --> 00:02:08,760
 sense of being free, being

29
00:02:08,760 --> 00:02:14,780
 at complete peace, complete peace no matter what arises.

30
00:02:14,780 --> 00:02:27,140
 You can't do that for as long as painful situations disturb

31
00:02:27,140 --> 00:02:28,720
 you.

32
00:02:28,720 --> 00:02:36,110
 So as long as you are averse to any type of pain or any

33
00:02:36,110 --> 00:02:41,040
 situation or even averse to death,

34
00:02:41,040 --> 00:02:44,680
 you can't attain the goal of the Buddhist teaching.

35
00:02:44,680 --> 00:02:46,680
 This is only a part of the answer.

36
00:02:46,680 --> 00:02:51,450
 The other part of the answer is that there certainly may be

37
00:02:51,450 --> 00:02:52,880
 along the way to getting

38
00:02:52,880 --> 00:02:58,390
 to that extreme state or that ultimate state of true invinc

39
00:02:58,390 --> 00:02:59,600
ibility.

40
00:02:59,600 --> 00:03:09,500
 There may be cases where you should quite reasonably reduce

41
00:03:09,500 --> 00:03:12,160
 or change the situation.

42
00:03:12,160 --> 00:03:14,900
 So there may be situations that first of all are

43
00:03:14,900 --> 00:03:16,040
 overwhelming.

44
00:03:16,040 --> 00:03:20,480
 So for a novice meditator to insist that they sit still no

45
00:03:20,480 --> 00:03:22,920
 matter what sensations arise

46
00:03:22,920 --> 00:03:24,880
 is unreasonable.

47
00:03:24,880 --> 00:03:31,700
 It is more likely to create unwholesomeness as long as one

48
00:03:31,700 --> 00:03:34,520
 is affected by pain.

49
00:03:34,520 --> 00:03:38,480
 So when a little bit of pain comes up, they start to get

50
00:03:38,480 --> 00:03:39,280
 upset.

51
00:03:39,280 --> 00:03:42,310
 If you ask them to sit through extreme pain, instead of

52
00:03:42,310 --> 00:03:44,320
 cultivating wholesome mindfulness

53
00:03:44,320 --> 00:03:50,040
 patients, it could just cultivate aversion, anxiety, even a

54
00:03:50,040 --> 00:03:52,480
 dislike of the meditation

55
00:03:52,480 --> 00:03:53,480
 practice.

56
00:03:53,480 --> 00:03:56,470
 It can create very strong emotions, strong negative

57
00:03:56,470 --> 00:03:57,360
 emotions.

58
00:03:57,360 --> 00:04:03,240
 So for a beginner meditator, you have to know your limits

59
00:04:03,240 --> 00:04:06,880
 and be able to push them but not

60
00:04:06,880 --> 00:04:11,930
 try to break through your limits, not go beyond your limits

61
00:04:11,930 --> 00:04:12,000
.

62
00:04:12,000 --> 00:04:15,800
 So when the pain gets truly unbearable where you're just

63
00:04:15,800 --> 00:04:18,000
 not able emotionally to handle

64
00:04:18,000 --> 00:04:23,000
 it, it's fully reasonable to have the meditator move.

65
00:04:23,000 --> 00:04:26,120
 So be mindful, lift your foot up, move it.

66
00:04:26,120 --> 00:04:28,760
 Eventually through repeated practice, the meditator will

67
00:04:28,760 --> 00:04:30,080
 become more patient and more

68
00:04:30,080 --> 00:04:32,000
 able to stand with the pain.

69
00:04:32,000 --> 00:04:35,890
 As long as we understand that our goal is to become

70
00:04:35,890 --> 00:04:38,720
 invincible to any situation, any

71
00:04:38,720 --> 00:04:44,920
 state, it's up to us how quickly we're going to get there.

72
00:04:44,920 --> 00:04:48,820
 So we can admit that we're not able to deal with it, the

73
00:04:48,820 --> 00:04:51,200
 pain, without taking that as

74
00:04:51,200 --> 00:04:55,600
 a dogma, that when pain arises it is proper practice or it

75
00:04:55,600 --> 00:04:57,920
 is the path to enlightenment

76
00:04:57,920 --> 00:05:02,440
 to avoid the pain.

77
00:05:02,440 --> 00:05:06,370
 Understanding that eventually you will cultivate tolerance

78
00:05:06,370 --> 00:05:08,520
 and patience and objectivity to

79
00:05:08,520 --> 00:05:11,620
 the extent that the pain no longer bothers you and you no

80
00:05:11,620 --> 00:05:13,000
 longer have to move.

81
00:05:13,000 --> 00:05:16,160
 The second reason is that there are certain cases where

82
00:05:16,160 --> 00:05:18,000
 pain is a sign of something, where

83
00:05:18,000 --> 00:05:21,440
 it is a sign that you are about to break your leg, for

84
00:05:21,440 --> 00:05:22,320
 example.

85
00:05:22,320 --> 00:05:26,390
 You're about to sprain your ankle or your leg or you're

86
00:05:26,390 --> 00:05:29,200
 about to cause injury to yourself.

87
00:05:29,200 --> 00:05:31,800
 And obviously in those cases it's, you know, your back

88
00:05:31,800 --> 00:05:32,360
 injury.

89
00:05:32,360 --> 00:05:34,140
 Some people have back problems.

90
00:05:34,140 --> 00:05:36,750
 If they were to continue sitting, it might actually injure

91
00:05:36,750 --> 00:05:37,200
 them.

92
00:05:37,200 --> 00:05:40,380
 If you have a case for that, first of all you have to be

93
00:05:40,380 --> 00:05:42,400
 clear that you're not just making

94
00:05:42,400 --> 00:05:46,560
 excuses or you're not just a hypochondriac.

95
00:05:46,560 --> 00:05:51,910
 If you actually have some prior injury, many people come to

96
00:05:51,910 --> 00:05:54,880
 meditation with plates in their

97
00:05:54,880 --> 00:06:00,690
 legs or their joints or in their backs or some serious

98
00:06:00,690 --> 00:06:02,240
 injuries.

99
00:06:02,240 --> 00:06:07,520
 These people can in certain cases, reasonably should in

100
00:06:07,520 --> 00:06:11,520
 certain cases, change their position.

101
00:06:11,520 --> 00:06:15,840
 That's the other situation where it's actually going to

102
00:06:15,840 --> 00:06:16,840
 harm you.

103
00:06:16,840 --> 00:06:17,840
 You can make a case for it.

104
00:06:17,840 --> 00:06:20,210
 Now you can also make a case for just sitting through it,

105
00:06:20,210 --> 00:06:21,320
 letting yourself go.

106
00:06:21,320 --> 00:06:25,000
 There's the case of Chakupala who did walking and sitting

107
00:06:25,000 --> 00:06:27,080
 meditation even though he was,

108
00:06:27,080 --> 00:06:30,560
 in order to heal his eyes, he was to lie down.

109
00:06:30,560 --> 00:06:34,480
 Instead of lying down, he just did walking and sitting for

110
00:06:34,480 --> 00:06:36,200
 three months and his eyes

111
00:06:36,200 --> 00:06:37,200
 were destroyed.

112
00:06:37,200 --> 00:06:42,600
 He went blind as a result of his extreme practice, but he

113
00:06:42,600 --> 00:06:45,440
 also became enlightened.

114
00:06:45,440 --> 00:06:52,370
 As a means of letting go, one can even let go of one's own

115
00:06:52,370 --> 00:06:55,560
 physical well-being.

116
00:06:55,560 --> 00:06:59,720
 There's certainly nothing wrong with that.

117
00:06:59,720 --> 00:07:02,330
 The idea of extreme asceticism, the question is, "Well,

118
00:07:02,330 --> 00:07:03,320
 then what is that?"

119
00:07:03,320 --> 00:07:07,930
 Extreme asceticism is the concept that there's some benefit

120
00:07:07,930 --> 00:07:10,400
 to hurting yourself, that pain

121
00:07:10,400 --> 00:07:14,440
 is somehow intrinsically beneficial.

122
00:07:14,440 --> 00:07:17,490
 Pain is somehow a means of inflicting pain upon yourself

123
00:07:17,490 --> 00:07:19,440
 somehow leads to enlightenment.

124
00:07:19,440 --> 00:07:20,760
 It doesn't.

125
00:07:20,760 --> 00:07:24,640
 Objectivity leads to enlightenment.

126
00:07:24,640 --> 00:07:31,660
 The ability to stay patient, patient in regards to pleasant

127
00:07:31,660 --> 00:07:34,920
 sensations, pleasant situations

128
00:07:34,920 --> 00:07:39,060
 and patient in regards to unpleasant situations equally,

129
00:07:39,060 --> 00:07:41,480
 that is what leads to enlightenment.

130
00:07:41,480 --> 00:07:44,600
 The extreme practice is thinking that there's some benefit

131
00:07:44,600 --> 00:07:46,440
 in either pleasant or unpleasant

132
00:07:46,440 --> 00:07:48,320
 situations.

133
00:07:48,320 --> 00:07:49,920
 Both of those are extreme.

134
00:07:49,920 --> 00:07:51,120
 There's no benefit in either.

135
00:07:51,120 --> 00:07:54,520
 There's only benefit in being objective towards them.

136
00:07:54,520 --> 00:07:55,920
 It's quite a clear teaching.

137
00:07:55,920 --> 00:07:58,520
 I don't think there's any reason to be confused.

