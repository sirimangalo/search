1
00:00:00,000 --> 00:00:02,000
 You ready? Go.

2
00:00:02,000 --> 00:00:06,280
 So the question is should one wear a jacket or hat or

3
00:00:06,280 --> 00:00:08,660
 gloves during the winter?

4
00:00:08,660 --> 00:00:15,540
 Because if one wears a hat or a jacket they're creating a

5
00:00:15,540 --> 00:00:17,360
 version towards the cold.

6
00:00:17,360 --> 00:00:25,240
 Yeah, I mean there's certainly that potential and I think

7
00:00:25,240 --> 00:00:29,060
 certainly that that's a real

8
00:00:30,420 --> 00:00:32,600
 That's a real thing that really happens

9
00:00:32,600 --> 00:00:38,730
 And so to some extent we have to we have to deal with that

10
00:00:38,730 --> 00:00:40,540
 way when I was here

11
00:00:40,540 --> 00:00:44,590
 15 years ago in this same monastery. This is where I spent

12
00:00:44,590 --> 00:00:46,300
 my first year as a monk and

13
00:00:46,300 --> 00:00:51,390
 I had to go to university every day. I was still in even as

14
00:00:51,390 --> 00:00:54,060
 a monk. I was in university

15
00:00:54,060 --> 00:00:57,460
 and

16
00:00:57,460 --> 00:00:59,460
 I was in university for six weeks

17
00:00:59,460 --> 00:01:02,900
 It was darn cold even before that after I started med

18
00:01:02,900 --> 00:01:05,460
itating I came back to Canada and

19
00:01:05,460 --> 00:01:09,920
 went back to university as a layperson in this same

20
00:01:09,920 --> 00:01:11,340
 monastery and

21
00:01:11,340 --> 00:01:15,460
 I remember dealing with the cold

22
00:01:15,460 --> 00:01:19,640
 I had this funny situation where I was to meet the abbot

23
00:01:19,640 --> 00:01:23,380
 the head monk and we were going to Toronto together and

24
00:01:24,100 --> 00:01:26,850
 So we made an appointment to meet at a specific street

25
00:01:26,850 --> 00:01:27,300
 corner

26
00:01:27,300 --> 00:01:29,900
 at a specific time

27
00:01:29,900 --> 00:01:33,420
 4 o'clock I think it seems to me it was 4 o'clock and

28
00:01:33,420 --> 00:01:37,580
 At 4 o'clock I was there

29
00:01:37,580 --> 00:01:40,220
 waiting

30
00:01:40,220 --> 00:01:44,720
 You know if this is the corner I was here and I waited and

31
00:01:44,720 --> 00:01:46,920
 I could see the intersection

32
00:01:46,920 --> 00:01:50,420
 And I waited for a half an hour

33
00:01:51,420 --> 00:01:55,420
 getting colder it was freezing and I was like quite

34
00:01:55,420 --> 00:02:00,300
 Quite

35
00:02:00,300 --> 00:02:04,060
 Getting quite sick as a result, but I had this interesting

36
00:02:04,060 --> 00:02:06,900
 experience where just simply by being mindful I

37
00:02:06,900 --> 00:02:11,820
 Could lose the sense of colds. You know by acknowledging

38
00:02:11,820 --> 00:02:12,380
 cold

39
00:02:12,380 --> 00:02:17,500
 The shivering would stop and and the pain and the

40
00:02:18,300 --> 00:02:21,450
 Upset of course would stop but also the cold would just

41
00:02:21,450 --> 00:02:22,540
 sort of disappear

42
00:02:22,540 --> 00:02:27,340
 And it entered into this sort of

43
00:02:27,340 --> 00:02:31,380
 Good state where the body was comfortable

44
00:02:31,380 --> 00:02:34,900
 Of course it didn't last and I ended up getting good. It

45
00:02:34,900 --> 00:02:36,340
 was quite frustrated

46
00:02:36,340 --> 00:02:40,840
 And then I was like this is crazy and so I started walking

47
00:02:40,840 --> 00:02:42,100
 back to the towards the monastery

48
00:02:42,100 --> 00:02:45,700
 I'm just and I got I walked to the intersection I looked

49
00:02:45,700 --> 00:02:49,740
 over and he was around the corner waiting in his car and

50
00:02:49,740 --> 00:02:53,400
 He was just as frustrated as me or probably not as much

51
00:02:53,400 --> 00:02:54,980
 because he was sitting in his warm car

52
00:02:54,980 --> 00:02:58,140
 But he had been sitting in his warm car for the past half

53
00:02:58,140 --> 00:03:00,020
 hour, so we both were there on time

54
00:03:00,020 --> 00:03:03,180
 And he scolded me and I scolded him

55
00:03:03,180 --> 00:03:09,580
 And it taught us the lesson in looking around the corner

56
00:03:11,900 --> 00:03:13,900
 And

57
00:03:13,900 --> 00:03:16,920
 But definitely that's part of it, and that was something I

58
00:03:16,920 --> 00:03:19,420
 realized over time is that you part of our

59
00:03:19,420 --> 00:03:23,290
 Version to cold is just that and a version, but that's not

60
00:03:23,290 --> 00:03:25,620
 the only reason why you wear a hat

61
00:03:25,620 --> 00:03:28,500
 I mean and a coat and so on

62
00:03:28,500 --> 00:03:30,380
 there

63
00:03:30,380 --> 00:03:34,060
 There's a difference between being cold and making yourself

64
00:03:34,060 --> 00:03:34,420
 sick

65
00:03:34,420 --> 00:03:36,300
 You can't get sick from the cold

66
00:03:36,300 --> 00:03:39,400
 But as I understand it it reduces your immune system and

67
00:03:39,400 --> 00:03:42,420
 and can do that quite severely to the point where it

68
00:03:42,420 --> 00:03:45,900
 You're quite likely to catch whatever virus is going around

69
00:03:45,900 --> 00:03:46,340
 quite

70
00:03:46,340 --> 00:03:52,300
 It increases your odds of getting sick which can be a hind

71
00:03:52,300 --> 00:03:54,580
rance to your practice etc etc

72
00:03:54,580 --> 00:03:58,490
 Of course you won't be able to teach you wouldn't wouldn't

73
00:03:58,490 --> 00:03:59,980
 be able to study for example

74
00:03:59,980 --> 00:04:03,860
 When you're sick, so there's a lot of there's quite debt

75
00:04:03,860 --> 00:04:04,900
 some detriments

76
00:04:05,900 --> 00:04:07,900
 detrimental effects

77
00:04:07,900 --> 00:04:11,380
 To the cold

78
00:04:11,380 --> 00:04:14,940
 Yeah, I mean here in Canada. You can't just go out in the

79
00:04:14,940 --> 00:04:17,860
 cold you'll get really sick

80
00:04:17,860 --> 00:04:21,490
 You know so I was wondering how do you decide when to wear

81
00:04:21,490 --> 00:04:21,860
 a hat?

82
00:04:21,860 --> 00:04:27,940
 Well you should wear a hat as a matter of course I mean

83
00:04:27,940 --> 00:04:32,140
 Wearing a hat doesn't in and of itself cultivate an

84
00:04:32,140 --> 00:04:34,220
 aversion to cold

85
00:04:36,180 --> 00:04:40,860
 A version cultivates aversion it's habitual so if you

86
00:04:40,860 --> 00:04:46,460
 If you don't change the habit it will it will increase you

87
00:04:46,460 --> 00:04:47,940
 know aversion

88
00:04:47,940 --> 00:04:50,860
 Will create further aversions so?

89
00:04:50,860 --> 00:04:55,360
 By noting you know upset upset dislike disliking disliking

90
00:04:55,360 --> 00:04:56,020
 you know

91
00:04:56,020 --> 00:04:58,870
 That's what gets rid of the aversion or when you feel cold

92
00:04:58,870 --> 00:05:01,530
 saying cold cold if you never feel cold because you're

93
00:05:01,530 --> 00:05:02,620
 always wearing a hat

94
00:05:02,620 --> 00:05:04,620
 That's not going to create aversion to cold

95
00:05:04,620 --> 00:05:09,010
 It's only when you feel the cold and you allow yourself to

96
00:05:09,010 --> 00:05:12,580
 to dislike it. That's what's cultivating the aversion

97
00:05:12,580 --> 00:05:15,340
 Now the disliking of it

98
00:05:15,340 --> 00:05:19,020
 Is what leads you to put often leads you to put the hat on

99
00:05:19,020 --> 00:05:21,620
 but it's not the putting the hat on that's the problem

100
00:05:21,620 --> 00:05:23,500
 it's the

101
00:05:23,500 --> 00:05:25,500
 encouraging of the disliking

102
00:05:25,500 --> 00:05:28,800
 So should you determine to wear that hat before going

103
00:05:28,800 --> 00:05:29,420
 outside?

104
00:05:29,420 --> 00:05:31,380
 so

105
00:05:31,380 --> 00:05:34,580
 Otherwise if you go outside you realize it's cold then you

106
00:05:34,580 --> 00:05:35,420
 put on the hat

107
00:05:35,420 --> 00:05:39,380
 Then you're creating a vision aversion to the cold no no it

108
00:05:39,380 --> 00:05:40,500
's not because you put on the hat

109
00:05:40,500 --> 00:05:43,240
 It's because you dislike the cold. It's the actual disl

110
00:05:43,240 --> 00:05:44,700
iking that becomes habitual

111
00:05:44,700 --> 00:05:47,320
 So if you say to yourself cold cold, and there's no disl

112
00:05:47,320 --> 00:05:49,900
iking you say well here. I am outside. Let me put on the hat

113
00:05:49,900 --> 00:05:53,620
 There's nothing to do with necessarily with aversion

114
00:05:53,620 --> 00:05:56,620
 so good question, but

115
00:05:56,620 --> 00:05:59,180
 mechanics of it are

116
00:06:00,100 --> 00:06:02,100
 otherwise

117
00:06:02,100 --> 00:06:07,380
 It's habit. It's about what forms a habit, so it's not that

118
00:06:07,380 --> 00:06:10,210
 It seems like that it seems like by putting on the hat you

119
00:06:10,210 --> 00:06:11,220
 become more averse to it

120
00:06:11,220 --> 00:06:13,580
 And I've often said sort of that sort of thing, but

121
00:06:13,580 --> 00:06:16,060
 technically speaking. That's not what happens

122
00:06:16,060 --> 00:06:18,620
 So this is why when you feel pain

123
00:06:18,620 --> 00:06:21,980
 It's not a problem to adjust the adjusting the moving your

124
00:06:21,980 --> 00:06:24,060
 body is shifting is not the problem

125
00:06:24,060 --> 00:06:27,920
 But what is pushing you to to move that's the problem

126
00:06:28,420 --> 00:06:31,170
 And so we encourage you not to move we encourage you to

127
00:06:31,170 --> 00:06:31,700
 instead

128
00:06:31,700 --> 00:06:36,990
 Try to learn about it. It doesn't mean that the actual

129
00:06:36,990 --> 00:06:40,940
 moving is causing the the aversion

130
00:06:40,940 --> 00:06:51,700
 So in in certain cases

131
00:06:51,700 --> 00:06:55,820
 There'd be no reason you know for example when you feel

132
00:06:55,820 --> 00:06:56,980
 pain. There's no reason to move

133
00:06:57,500 --> 00:06:59,500
 So in most cases you don't have to move

134
00:06:59,500 --> 00:07:02,920
 here in this case even though

135
00:07:02,920 --> 00:07:06,700
 Part of it is even though

136
00:07:06,700 --> 00:07:10,300
 There is the potential to put on the hat out of a version

137
00:07:10,300 --> 00:07:13,520
 There's also the reason to put out that put on the hat

138
00:07:13,520 --> 00:07:16,100
 to not get sick so

139
00:07:16,100 --> 00:07:19,560
 also to not get distracted because it can be very difficult

140
00:07:19,560 --> 00:07:22,300
 to focus if you had a headache from the cold or

141
00:07:23,580 --> 00:07:27,360
 You know cold we don't function very well at at sub-zero

142
00:07:27,360 --> 00:07:29,660
 temperatures, right? So

143
00:07:29,660 --> 00:07:33,380
 And then as soon as you go inside you get a huge headache

144
00:07:33,380 --> 00:07:35,820
 and and it's not a very good

145
00:07:35,820 --> 00:07:39,400
 Resolve especially going from hot to cold. It's it's quite

146
00:07:39,400 --> 00:07:41,900
 hard on the body and that's not useful

147
00:07:41,900 --> 00:07:45,510
 It's not helpful to do that. So I mean that these are

148
00:07:45,510 --> 00:07:46,420
 excuses you can give

149
00:07:46,420 --> 00:07:49,170
 It's not always the case and sometimes it's actually useful

150
00:07:49,170 --> 00:07:51,860
 to experience all that the headaches and so on and so on so

151
00:07:52,140 --> 00:07:54,640
 There's no hard and fast rule for the most you know for a

152
00:07:54,640 --> 00:07:56,600
 hardcore meditator. You can put up with it

153
00:07:56,600 --> 00:07:59,220
 Mahasi Sayada talks about a monk

154
00:07:59,220 --> 00:08:01,900
 In one of those

155
00:08:01,900 --> 00:08:03,740
 commentaries I think

156
00:08:03,740 --> 00:08:06,980
 Who meditated on in the snow, you know, he just did sitting

157
00:08:06,980 --> 00:08:08,620
 meditation while it was snowing around him

158
00:08:08,620 --> 00:08:11,860
 Which of course we hear about with Tibet Tibetans and so on

159
00:08:11,860 --> 00:08:15,020
 meditating in the snow

160
00:08:15,020 --> 00:08:17,220
 I'll get that picture of me

161
00:08:17,700 --> 00:08:21,650
 There's quite a nice picture sitting in my parents patio

162
00:08:21,650 --> 00:08:22,460
 furniture

163
00:08:22,460 --> 00:08:25,820
 With the snow coming deep and I was sitting with my legs

164
00:08:25,820 --> 00:08:28,580
 crossed and snow shoes wearing snow shoes

165
00:08:28,580 --> 00:08:32,700
 That about sums it up you can do it

166
00:08:32,700 --> 00:08:35,930
 I mean it was to me it was sort of an experiment on my on

167
00:08:35,930 --> 00:08:39,060
 my own part to to just experience that you know

168
00:08:39,060 --> 00:08:41,300
 Here I'm going to experience in monks robes, which are not

169
00:08:41,300 --> 00:08:43,660
 very very much protection

170
00:08:44,340 --> 00:08:48,650
 But to just go through it and it's quite quite freeing

171
00:08:48,650 --> 00:08:50,860
 really to not have to avoid

172
00:08:50,860 --> 00:08:55,800
 So certainly there's that but wearing a hat there's nothing

173
00:08:55,800 --> 00:08:57,460
 wrong with it. It can be quite useful

174
00:08:57,460 --> 00:09:01,300
 Think there's many reasons to wear it. I mean

175
00:09:01,300 --> 00:09:04,950
 But it's not like it's certainly not something you should

176
00:09:04,950 --> 00:09:07,060
 obsess over. I have to wear a hat etc. I

177
00:09:07,060 --> 00:09:10,100
 Don't think you have to obsess about not wearing a hat

178
00:09:11,100 --> 00:09:14,180
 Just you understand that you can at times go with that

