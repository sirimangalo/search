1
00:00:00,000 --> 00:00:04,180
 Hello, Bhante, which meditation method do you recommend the

2
00:00:04,180 --> 00:00:05,320
 most? From which sutta

3
00:00:05,320 --> 00:00:09,760
 is this? Thanks. Very easy question with a very simple

4
00:00:09,760 --> 00:00:13,480
 answer. The practice that I

5
00:00:13,480 --> 00:00:19,880
 recommend the most, almost exclusively, is the practice of

6
00:00:19,880 --> 00:00:21,160
 the four foundations of

7
00:00:21,160 --> 00:00:29,160
 mindfulness, which is found in three general places, I mean

8
00:00:29,160 --> 00:00:29,840
 other places as

9
00:00:29,840 --> 00:00:38,080
 well, but most explicitly in the Digha Nikaya number 10, no

10
00:00:38,080 --> 00:00:38,720
, Digha Nikaya

11
00:00:38,720 --> 00:00:44,780
 number 10, Digha Nikaya number, I want to say 12, but I can

12
00:00:44,780 --> 00:00:46,200
't remember. It's a

13
00:00:46,200 --> 00:00:48,670
 shameful thing that I can't even remember. No, it's not 12.

14
00:00:48,670 --> 00:00:50,640
 Where is the Mahasatipatthana

15
00:00:50,640 --> 00:00:55,140
 sutta? 16 is Bairni bhana sutta. No, I'm terrible. Don't

16
00:00:55,140 --> 00:00:56,840
 even remember. 22? Is that it? I

17
00:00:56,840 --> 00:00:59,930
 can't remember what number it is. My mind is going, and

18
00:00:59,930 --> 00:01:01,320
 here I was bragging about

19
00:01:01,320 --> 00:01:04,480
 having a good memory. Anyway, I remember what the sutta is

20
00:01:04,480 --> 00:01:07,920
 all about. Why am I

21
00:01:07,920 --> 00:01:11,960
 getting those mixed up, no? Anyway, Majimini Nikaya number

22
00:01:11,960 --> 00:01:13,520
 10, and the third place is

23
00:01:13,520 --> 00:01:17,560
 in the Sanyutta Nikaya, the Satipatthana Sanyutta. The

24
00:01:17,560 --> 00:01:20,280
 group, a whole group of

25
00:01:20,280 --> 00:01:23,160
 discourses based on the Satipatthana sutta, based on the

26
00:01:23,160 --> 00:01:24,200
 four Satipatthanas,

27
00:01:24,200 --> 00:01:28,470
 the four foundations of mindfulness. These are the body,

28
00:01:28,470 --> 00:01:31,160
 the feelings, the

29
00:01:31,160 --> 00:01:38,280
 mind, and dhammas, which are... I asked my teacher about

30
00:01:38,280 --> 00:01:39,880
 the dhammas recently,

31
00:01:39,880 --> 00:01:43,260
 because he was still using the English. It's one of the few

32
00:01:43,260 --> 00:01:44,040
 English words that

33
00:01:44,040 --> 00:01:46,410
 he knows is the translation of the four foundations of

34
00:01:46,410 --> 00:01:47,480
 mindfulness, and the

35
00:01:47,480 --> 00:01:51,400
 translation they've given him is the usual mind objects,

36
00:01:51,400 --> 00:01:52,760
 which doesn't at all

37
00:01:52,760 --> 00:01:58,280
 encompass what is meant by it in this context. Dhamma as a

38
00:01:58,280 --> 00:01:59,200
 mind object

39
00:01:59,200 --> 00:02:07,830
 is only dhamma-ramana, the objects of thought, so the sixth

40
00:02:07,830 --> 00:02:09,920
 sense, which is

41
00:02:09,920 --> 00:02:12,430
 thinking. So when you think about something, the object of

42
00:02:12,430 --> 00:02:13,040
 your thought,

43
00:02:13,040 --> 00:02:18,120
 that's a mind object. That's called dhamma-ramana. But the

44
00:02:18,120 --> 00:02:22,080
 dhammas here are,

45
00:02:22,080 --> 00:02:29,960
 my teacher said, things that support you, things that

46
00:02:29,960 --> 00:02:33,800
 support you, that keep you

47
00:02:33,800 --> 00:02:38,400
 from falling into evil. Because dhamma comes from dhar,

48
00:02:38,400 --> 00:02:40,160
 which is to

49
00:02:40,160 --> 00:02:44,700
 hold. Dhammas are those things that hold you up, keep you

50
00:02:44,700 --> 00:02:46,480
 from falling. So

51
00:02:46,480 --> 00:02:50,040
 these are the things that... another way to look at it, a

52
00:02:50,040 --> 00:02:51,180
 way to translate it, is those

53
00:02:51,180 --> 00:02:54,650
 things that exist in reality. So reality is a good

54
00:02:54,650 --> 00:02:57,720
 translation. Anyway, fourth one,

55
00:02:57,720 --> 00:03:01,120
 the problem is it has many groups in it, and it looks to me

56
00:03:01,120 --> 00:03:01,960
 very much like

57
00:03:01,960 --> 00:03:07,440
 it's simply talking about teachings, the various aspects of

58
00:03:07,440 --> 00:03:07,520
 the

59
00:03:07,520 --> 00:03:12,080
 Buddha's teaching. Things that are important, dhammas in

60
00:03:12,080 --> 00:03:13,320
 the sense of things

61
00:03:13,320 --> 00:03:15,840
 that will help, will carry you through to enlightenment,

62
00:03:15,840 --> 00:03:16,800
 teachings that will carry

63
00:03:16,800 --> 00:03:21,600
 you through to enlightenment. Anyway, that's the teaching,

64
00:03:21,600 --> 00:03:22,400
 the Four Foundations

65
00:03:22,400 --> 00:03:24,520
 of Mindfulness. There's an excellent book out there that

66
00:03:24,520 --> 00:03:25,360
 everyone should read

67
00:03:25,360 --> 00:03:29,710
 called The Way of Mindfulness, and it's simply a

68
00:03:29,710 --> 00:03:32,280
 translation of the Maha Satipatthana

69
00:03:32,280 --> 00:03:38,220
 sutta with a translation of most, I think, or some at least

70
00:03:38,220 --> 00:03:39,000
, of the

71
00:03:39,000 --> 00:03:42,780
 commentary to the Satipatthana sutta, and even some of the

72
00:03:42,780 --> 00:03:44,320
 sub-commentary. So it's

73
00:03:44,320 --> 00:03:50,470
 an excellent look at how the ancient teachers, 2000 years

74
00:03:50,470 --> 00:03:52,960
 ago, 1500 to 2000

75
00:03:52,960 --> 00:03:58,980
 years ago, were teaching and were interpreting the Satipat

76
00:03:58,980 --> 00:03:59,800
thana sutta.

77
00:03:59,800 --> 00:04:02,560
 It's an excellent book, excellent resources, got lots of

78
00:04:02,560 --> 00:04:03,540
 neat stories in it

79
00:04:03,540 --> 00:04:08,420
 and insights in it, but even the sutta itself is a great

80
00:04:08,420 --> 00:04:09,840
 place to start to

81
00:04:09,840 --> 00:04:14,500
 learn about meditation. So very, very much worth reading. I

82
00:04:14,500 --> 00:04:16,920
 mean, on the other hand,

83
00:04:16,920 --> 00:04:20,920
 it may not be a perfect place to start because, of course,

84
00:04:20,920 --> 00:04:22,360
 the suttas are

85
00:04:22,360 --> 00:04:27,490
 coming from a whole other era designed for a whole other

86
00:04:27,490 --> 00:04:29,200
 audience in India, so

87
00:04:29,200 --> 00:04:31,350
 it's sometimes difficult, and then they're in a whole other

88
00:04:31,350 --> 00:04:31,760
 language,

89
00:04:31,760 --> 00:04:36,690
 completely different from English. So sometimes difficult

90
00:04:36,690 --> 00:04:37,920
 to really understand

91
00:04:37,920 --> 00:04:43,360
 what's being said. But if you take what I'm passing along

92
00:04:43,360 --> 00:04:44,520
 in tandem with

93
00:04:44,520 --> 00:04:49,970
 that study, I think it would be indispensable, those two

94
00:04:49,970 --> 00:04:50,880
 things together.

95
00:04:50,880 --> 00:04:53,100
 I'm not bragging about my own teaching, but I'm talking

96
00:04:53,100 --> 00:04:54,000
 about the teaching that

97
00:04:54,000 --> 00:04:57,920
 has been passed on to me. You take that teaching, and it's

98
00:04:57,920 --> 00:04:58,760
 basically an

99
00:04:58,760 --> 00:05:04,220
 explanation in modern terms of the Satipatthana sutta, take

100
00:05:04,220 --> 00:05:04,680
 those two

101
00:05:04,680 --> 00:05:09,060
 together. In this tradition, that is indispensable. Of

102
00:05:09,060 --> 00:05:11,080
 course, every tradition

103
00:05:11,080 --> 00:05:16,450
 has its own explanations, but in this tradition, I would

104
00:05:16,450 --> 00:05:18,960
 say that's where I get

105
00:05:18,960 --> 00:05:21,730
 it from. So if you're looking for something, some sort of

106
00:05:21,730 --> 00:05:22,720
 source, go there

107
00:05:22,720 --> 00:05:27,640
 and really take these two things together, the teachings

108
00:05:27,640 --> 00:05:28,800
 that are modern

109
00:05:28,800 --> 00:05:32,370
 and the teachings that they come from, the Satipatthana sut

110
00:05:32,370 --> 00:05:33,360
ta, and also read the

111
00:05:33,360 --> 00:05:39,840
 Satipatthana sutta. It's an excellent sort of additional

112
00:05:39,840 --> 00:05:41,120
 resource, something very

