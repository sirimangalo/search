1
00:00:00,000 --> 00:00:02,000
 Go ahead.

2
00:00:02,000 --> 00:00:05,550
 This is more for Yutadama, but would like the other monk's

3
00:00:05,550 --> 00:00:06,000
 opinion also.

4
00:00:06,000 --> 00:00:10,070
 Would you agree that some people ordain as monks or nuns

5
00:00:10,070 --> 00:00:12,300
 because they don't feel like they actually belong to any

6
00:00:12,300 --> 00:00:13,000
 social group,

7
00:00:13,000 --> 00:00:16,880
 and the fact that they are monks/nuns gives them some sort

8
00:00:16,880 --> 00:00:20,000
 of comfort knowing that they are part of something?

9
00:00:20,000 --> 00:00:23,210
 Also, would it be fair to suggest that it gives them some

10
00:00:23,210 --> 00:00:24,000
 sort of power,

11
00:00:24,000 --> 00:00:26,540
 and they like the status and attention they get when out in

12
00:00:26,540 --> 00:00:29,000
 public, especially in the west?

13
00:00:29,000 --> 00:00:39,000
 I don't know. I haven't encountered that many monks yet.

14
00:00:39,000 --> 00:00:41,000
 You don't know much of an answer.

15
00:00:41,000 --> 00:00:49,000
 I mean, for somebody belonging to something.

16
00:00:49,000 --> 00:00:52,210
 I think that depends because maybe it's the group that they

17
00:00:52,210 --> 00:00:53,000
 belong to.

18
00:00:53,000 --> 00:00:57,680
 It's like any social group, one person won't belong to

19
00:00:57,680 --> 00:01:00,000
 every different social group.

20
00:01:00,000 --> 00:01:04,000
 There's a certain group that they fit in with better.

21
00:01:04,000 --> 00:01:08,000
 Some people fit in with a monastic crowd.

22
00:01:08,000 --> 00:01:16,310
 I think giving any sort of opinion of why people ordain or

23
00:01:16,310 --> 00:01:19,000
 so on is, or conjecturing in this sort of way,

24
00:01:19,000 --> 00:01:23,000
 is quite difficult unless you're actually a monk yourself

25
00:01:23,000 --> 00:01:25,650
 and have a sense of what it means to be a monk and what it

26
00:01:25,650 --> 00:01:27,000
's like to be a monk.

27
00:01:27,000 --> 00:01:38,290
 The idea of ordaining to become a part of something, I don

28
00:01:38,290 --> 00:01:39,000
't know, man.

29
00:01:39,000 --> 00:01:42,860
 I think for sure some people ordain because they don't fit

30
00:01:42,860 --> 00:01:44,000
 in with society,

31
00:01:44,000 --> 00:01:47,640
 but I think in general you'd have to just say that people

32
00:01:47,640 --> 00:01:50,000
 ordain because they're suffering,

33
00:01:50,000 --> 00:01:54,950
 because society makes them suffer, because there's a lot of

34
00:01:54,950 --> 00:01:57,000
 stress and pointless stress.

35
00:01:57,000 --> 00:02:02,000
 They don't see the point of it. They feel it's pointless.

36
00:02:02,000 --> 00:02:08,000
 There's countless reasons why people ordain.

37
00:02:08,000 --> 00:02:12,340
 Some people ordain from what seems like to me because they

38
00:02:12,340 --> 00:02:16,000
 like being around other men wearing skirts.

39
00:02:16,000 --> 00:02:21,520
 There are people like this who get a kick or even more out

40
00:02:21,520 --> 00:02:24,000
 of that kind of thing.

41
00:02:24,000 --> 00:02:32,000
 Buddhism is a huge institution.

42
00:02:32,000 --> 00:02:35,000
 There are monks that have dating that date other monks.

43
00:02:35,000 --> 00:02:42,000
 There's a dating service in Japan for monks now.

44
00:02:42,000 --> 00:02:50,180
 So to say why people ordain, people ordain for various

45
00:02:50,180 --> 00:02:52,000
 different reasons.

46
00:02:52,000 --> 00:03:07,000
 For an ideal reason to ordain is for overcoming suffering,

47
00:03:07,000 --> 00:03:12,390
 for finding the way out of suffering, finding the truth

48
00:03:12,390 --> 00:03:18,000
 that allows you to become free from suffering.

49
00:03:18,000 --> 00:03:24,000
 I think what I might agree with is that once monks ordain,

50
00:03:24,000 --> 00:03:27,000
 no matter what their intentions were before they ordain,

51
00:03:27,000 --> 00:03:33,220
 let's imagine we have a monk who ordained with the best of

52
00:03:33,220 --> 00:03:35,000
 intentions.

53
00:03:35,000 --> 00:03:42,200
 In general, this is why I said you really have to ordain to

54
00:03:42,200 --> 00:03:44,000
 even make some sort of conjecture

55
00:03:44,000 --> 00:03:49,000
 or to even begin to approach what it's like to be a monk,

56
00:03:49,000 --> 00:03:52,000
 because they change once they ordain.

57
00:03:52,000 --> 00:03:54,670
 Their idea of what it was to be a monk or their idea of

58
00:03:54,670 --> 00:03:57,000
 what they were going to be like as a monk

59
00:03:57,000 --> 00:04:02,000
 is never the same as how they actually are as a monk.

60
00:04:02,000 --> 00:04:05,000
 Just as our idea of who we are in general is never the same

61
00:04:05,000 --> 00:04:07,000
 as how we actually appear to other people

62
00:04:07,000 --> 00:04:12,000
 or how we actually react or actually act in reality.

63
00:04:12,000 --> 00:04:17,000
 The majority of monks, when they ordain,

64
00:04:17,000 --> 00:04:23,000
 begin to instantly develop an ego.

65
00:04:23,000 --> 00:04:32,110
 In some cases, it's not an ego, but they take it on as a

66
00:04:32,110 --> 00:04:33,000
 role,

67
00:04:33,000 --> 00:04:39,000
 like now I'm a monk, so they start to act in a certain way.

68
00:04:39,000 --> 00:04:43,750
 To an extent, this is proper. A monk should act differently

69
00:04:43,750 --> 00:04:44,000
.

70
00:04:44,000 --> 00:04:47,510
 They should guard themselves and they should dedicate

71
00:04:47,510 --> 00:04:49,000
 themselves to the practice,

72
00:04:49,000 --> 00:04:52,000
 but it should be totally a practical move.

73
00:04:52,000 --> 00:04:55,270
 They should be guarded in their senses, not looking around

74
00:04:55,270 --> 00:04:56,000
 and so on.

75
00:04:56,000 --> 00:05:01,000
 But what happens is often, quite often, people will take it

76
00:05:01,000 --> 00:05:02,000
 into their head

77
00:05:02,000 --> 00:05:07,420
 that they are a monk and begin to develop a role and the

78
00:05:07,420 --> 00:05:10,000
 idea of I being a monk.

79
00:05:10,000 --> 00:05:16,000
 This gets worse when they are respected and given support

80
00:05:16,000 --> 00:05:17,000
 by laypeople

81
00:05:17,000 --> 00:05:21,540
 and maybe even taken up as teachers and they start to give

82
00:05:21,540 --> 00:05:25,000
 talks and teach people Buddhism and meditation.

83
00:05:25,000 --> 00:05:30,370
 Then they can become attached to the status and the

84
00:05:30,370 --> 00:05:32,000
 attention.

85
00:05:32,000 --> 00:05:35,000
 I don't know that people ordain for these reasons.

86
00:05:35,000 --> 00:05:37,300
 As I said, people ordain for all sorts of reasons, so it's

87
00:05:37,300 --> 00:05:38,000
 not really fair.

88
00:05:38,000 --> 00:05:42,000
 But the average person who ordains with good intentions,

89
00:05:42,000 --> 00:05:45,530
 I don't think it's so much in their mind until after they

90
00:05:45,530 --> 00:05:47,000
 ordain.

91
00:05:47,000 --> 00:05:50,000
 It's something that's very easy for a new monk to fall into

92
00:05:50,000 --> 00:05:50,000
.

93
00:05:50,000 --> 00:05:53,000
 Then what happens, I've talked about this before,

94
00:05:53,000 --> 00:05:56,590
 is that they slowly start to become a better monk and have

95
00:05:56,590 --> 00:05:59,000
 a lower opinion of themselves.

96
00:05:59,000 --> 00:06:02,250
 Their opinion is like this in the beginning and their

97
00:06:02,250 --> 00:06:08,000
 proficiency or their ability as a monk

98
00:06:08,000 --> 00:06:11,000
 is very low because they've just started out.

99
00:06:11,000 --> 00:06:16,360
 It levels out and eventually, if they continue on, the ego

100
00:06:16,360 --> 00:06:17,000
 disappears

101
00:06:17,000 --> 00:06:19,000
 and the proficiency as a monk increases.

102
00:06:19,000 --> 00:06:23,080
 Even monks who aren't that dedicated to the teaching or to

103
00:06:23,080 --> 00:06:24,000
 meditation,

104
00:06:24,000 --> 00:06:27,000
 once they've been ordained for 10 years, 20 years,

105
00:06:27,000 --> 00:06:30,000
 what you generally tend to see is they're quite humble,

106
00:06:30,000 --> 00:06:32,000
 quite laid back,

107
00:06:32,000 --> 00:06:37,000
 quite comfortable in just being who they are.

108
00:06:37,000 --> 00:06:43,480
 That actually is in general a fairly good, well-behaved

109
00:06:43,480 --> 00:06:47,000
 monk who fits in well with the system.

110
00:06:47,000 --> 00:06:52,740
 If they don't, then they tend to disrobe or start their own

111
00:06:52,740 --> 00:06:58,000
 new school of Buddhism.

112
00:06:58,000 --> 00:07:04,000
 I don't know if that's helpful. That's the answer.

