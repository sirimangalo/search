1
00:00:00,000 --> 00:00:03,760
 Hello and welcome back to Ask a Monk.

2
00:00:03,760 --> 00:00:08,110
 Today's question is part of the two-part question that I

3
00:00:08,110 --> 00:00:10,060
 got mixed up last time and I ended

4
00:00:10,060 --> 00:00:12,480
 up answering two different questions together.

5
00:00:12,480 --> 00:00:18,080
 So, this is answering the first part of someone's two-part

6
00:00:18,080 --> 00:00:20,640
 question that I missed.

7
00:00:20,640 --> 00:00:25,290
 And the first part was in regards to remembering one's past

8
00:00:25,290 --> 00:00:26,000
 lives.

9
00:00:26,000 --> 00:00:30,050
 Now, in regards to remembering one's past lives, we get

10
00:00:30,050 --> 00:00:32,320
 generally two questions again

11
00:00:32,320 --> 00:00:33,320
 and again.

12
00:00:33,320 --> 00:00:36,860
 The first question is about how we can believe in past

13
00:00:36,860 --> 00:00:37,580
 lives.

14
00:00:37,580 --> 00:00:38,580
 What is the proof?

15
00:00:38,580 --> 00:00:40,100
 What is the basis of past lives?

16
00:00:40,100 --> 00:00:44,570
 Because obviously in Buddhism, the boast is that we don't

17
00:00:44,570 --> 00:00:46,520
 take things on faith and so

18
00:00:46,520 --> 00:00:47,520
 on.

19
00:00:47,520 --> 00:00:51,470
 Actually, there are many things that we could take on faith

20
00:00:51,470 --> 00:00:52,640
, so to speak.

21
00:00:52,640 --> 00:00:57,140
 I think past lives is actually partially under that

22
00:00:57,140 --> 00:00:58,360
 category.

23
00:00:58,360 --> 00:01:03,910
 We take a lot of things on faith because we believe in the

24
00:01:03,910 --> 00:01:06,780
 knowledge and the wisdom of

25
00:01:06,780 --> 00:01:08,240
 those people who have realized them.

26
00:01:08,240 --> 00:01:10,910
 Like if you tell me what Australia is like, "Well, I've

27
00:01:10,910 --> 00:01:12,200
 never been to Australia, but

28
00:01:12,200 --> 00:01:13,200
 I believe you."

29
00:01:13,200 --> 00:01:14,960
 I take it on faith.

30
00:01:14,960 --> 00:01:19,570
 It's not something crucial and it's a sort of a faith that

31
00:01:19,570 --> 00:01:21,520
 is based on the fact that

32
00:01:21,520 --> 00:01:26,140
 I've never been there and I maybe don't have the ability to

33
00:01:26,140 --> 00:01:27,200
 go there.

34
00:01:27,200 --> 00:01:30,380
 If it were crucial to my practice as to whether Australia

35
00:01:30,380 --> 00:01:32,360
 was like this or like that, then

36
00:01:32,360 --> 00:01:36,490
 I'd probably want to go there and verify and see for myself

37
00:01:36,490 --> 00:01:36,800
.

38
00:01:36,800 --> 00:01:39,470
 Past lives could be the same if someone hasn't remembered

39
00:01:39,470 --> 00:01:40,480
 their past lives.

40
00:01:40,480 --> 00:01:43,960
 They might take it on faith because it's not crucial.

41
00:01:43,960 --> 00:01:48,560
 If it were crucial, now this is the second question.

42
00:01:48,560 --> 00:01:52,950
 Is it not a beneficial part of the past to remember your

43
00:01:52,950 --> 00:01:54,240
 past lives?

44
00:01:54,240 --> 00:01:57,280
 If it is, then why don't we teach it?

45
00:01:57,280 --> 00:02:00,160
 Why don't we practice to remember our past lives?

46
00:02:00,160 --> 00:02:03,840
 I think the answer is that we don't take it to be a crucial

47
00:02:03,840 --> 00:02:05,120
 part of the path.

48
00:02:05,120 --> 00:02:09,850
 The person who asked this question, their logic was that

49
00:02:09,850 --> 00:02:11,440
 there's the saying that we

50
00:02:11,440 --> 00:02:16,870
 are what we are now because of what we were before and what

51
00:02:16,870 --> 00:02:19,440
 we will be is because of who

52
00:02:19,440 --> 00:02:21,080
 we are now.

53
00:02:21,080 --> 00:02:24,960
 If we learn about the past, the mistakes we've made and

54
00:02:24,960 --> 00:02:27,720
 even the lessons we've learned, then

55
00:02:27,720 --> 00:02:31,600
 we'll be able to avoid mistakes and having to relearn the

56
00:02:31,600 --> 00:02:33,560
 same lessons again for the

57
00:02:33,560 --> 00:02:38,080
 future so that our future will be better.

58
00:02:38,080 --> 00:02:42,680
 I think there is some validity to that.

59
00:02:42,680 --> 00:02:45,840
 Remembering one's past lives is beneficial.

60
00:02:45,840 --> 00:02:51,430
 I think there's a lot of things that are beneficial to the

61
00:02:51,430 --> 00:02:55,200
 practice of becoming enlightened besides

62
00:02:55,200 --> 00:02:58,000
 simply meditation.

63
00:02:58,000 --> 00:03:02,400
 If you remember your past lives, if you are able to have

64
00:03:02,400 --> 00:03:04,880
 all these magical powers that

65
00:03:04,880 --> 00:03:09,550
 they talk about being able to see far away, see heaven, see

66
00:03:09,550 --> 00:03:11,600
 hell, to see beings being

67
00:03:11,600 --> 00:03:18,280
 born and passing away, there's so many things.

68
00:03:18,280 --> 00:03:19,280
 Therapy is useful.

69
00:03:19,280 --> 00:03:22,370
 The regression therapy where you go back to when you were a

70
00:03:22,370 --> 00:03:23,920
 young person or even now the

71
00:03:23,920 --> 00:03:27,130
 regression therapy, regressing the past lives, going back

72
00:03:27,130 --> 00:03:28,860
 and remembering your past lives

73
00:03:28,860 --> 00:03:30,880
 through hypnosis or so on.

74
00:03:30,880 --> 00:03:35,140
 It's said to allow people to become more comfortable and

75
00:03:35,140 --> 00:03:38,280
 more understanding in regards to why they

76
00:03:38,280 --> 00:03:43,780
 are the way they are so that they don't, they understand

77
00:03:43,780 --> 00:03:46,600
 better the situation that they're

78
00:03:46,600 --> 00:03:49,160
 in and they don't become upset by it.

79
00:03:49,160 --> 00:03:52,770
 They can see that there's actually a cause and effect

80
00:03:52,770 --> 00:03:53,320
 relationship.

81
00:03:53,320 --> 00:03:56,880
 Seeing that sort of cause and effect relationship is really

82
00:03:56,880 --> 00:03:59,400
 what the Buddha's teaching is about.

83
00:03:59,400 --> 00:04:02,280
 It's quite useful, I think.

84
00:04:02,280 --> 00:04:06,240
 It's not crucial though and in the end it shouldn't be

85
00:04:06,240 --> 00:04:08,840
 taken for a core practice because

86
00:04:08,840 --> 00:04:11,780
 it's too abstract.

87
00:04:11,780 --> 00:04:16,920
 The remembering of one's past lives as having a causal

88
00:04:16,920 --> 00:04:20,320
 relationship to the present is not

89
00:04:20,320 --> 00:04:23,640
 really what we mean by cause and effect.

90
00:04:23,640 --> 00:04:26,400
 The cause and effect that we're talking about is what's

91
00:04:26,400 --> 00:04:28,000
 occurring here and now at every

92
00:04:28,000 --> 00:04:33,350
 moment and you'll never become enlightened by this

93
00:04:33,350 --> 00:04:37,360
 conceptual analysis of your life that

94
00:04:37,360 --> 00:04:40,770
 you used to be like this and now you're like this and so on

95
00:04:40,770 --> 00:04:40,920
.

96
00:04:40,920 --> 00:04:45,250
 It's far too abstract and the concentration and the

97
00:04:45,250 --> 00:04:48,280
 intensity of the experience is not

98
00:04:48,280 --> 00:04:52,380
 anywhere near strong enough to allow you to really

99
00:04:52,380 --> 00:04:55,680
 understand the truth of reality, that

100
00:04:55,680 --> 00:05:00,210
 everything is impermanent and that there is nothing worth

101
00:05:00,210 --> 00:05:02,160
 clinging to and so on.

102
00:05:02,160 --> 00:05:05,660
 This is mainly because it's dealing with the past and the

103
00:05:05,660 --> 00:05:06,400
 future.

104
00:05:06,400 --> 00:05:07,400
 It's dealing with concepts.

105
00:05:07,400 --> 00:05:11,000
 It's not dealing with what you're seeing here and now.

106
00:05:11,000 --> 00:05:12,640
 The mind doesn't really get it.

107
00:05:12,640 --> 00:05:15,150
 You can tell yourself that, "Oh yes, I was a bad person in

108
00:05:15,150 --> 00:05:15,760
 the past.

109
00:05:15,760 --> 00:05:17,560
 That's why I have problems now.

110
00:05:17,560 --> 00:05:20,540
 Therefore, I shouldn't be a bad person now because I'll

111
00:05:20,540 --> 00:05:21,780
 have a bad future."

112
00:05:21,780 --> 00:05:26,450
 It really doesn't have any lasting impact on the mind or it

113
00:05:26,450 --> 00:05:28,440
 has a limited impact on

114
00:05:28,440 --> 00:05:33,610
 the mind as compared to looking to see what are the causes

115
00:05:33,610 --> 00:05:36,200
 and effects of your actions

116
00:05:36,200 --> 00:05:37,200
 in the present moment.

117
00:05:37,200 --> 00:05:38,640
 When I get angry, what's it like?

118
00:05:38,640 --> 00:05:40,240
 When I want something, what's it like?

119
00:05:40,240 --> 00:05:42,760
 When I am loving and kind, what's it like?

120
00:05:42,760 --> 00:05:46,360
 When I am clearly aware, what's it like?

121
00:05:46,360 --> 00:05:50,220
 Looking at what does it mean to see something, to hear

122
00:05:50,220 --> 00:05:52,820
 something and coming to change the

123
00:05:52,820 --> 00:05:55,400
 way we relate to the world around us.

124
00:05:55,400 --> 00:05:59,930
 This is really a lot more what the Buddha was teaching

125
00:05:59,930 --> 00:06:00,740
 about.

126
00:06:00,740 --> 00:06:04,740
 The Buddha said, "One should not go back to the past nor

127
00:06:04,740 --> 00:06:07,020
 worry about the future because

128
00:06:07,020 --> 00:06:10,700
 what's in the past is gone."

129
00:06:10,700 --> 00:06:14,950
 Looking on it and thinking over it is only going to be of

130
00:06:14,950 --> 00:06:17,280
 limited value if any and what's

131
00:06:17,280 --> 00:06:20,560
 in the future hasn't come yet.

132
00:06:20,560 --> 00:06:23,970
 If you can see clearly what's in the present moment, this

133
00:06:23,970 --> 00:06:25,800
 is where the cause and effect

134
00:06:25,800 --> 00:06:26,800
 is occurring.

135
00:06:26,800 --> 00:06:27,840
 This is empirical.

136
00:06:27,840 --> 00:06:35,180
 This is scientific, not based on faith.

137
00:06:35,180 --> 00:06:39,240
 Whether we remember our past lives or not, if we come to

138
00:06:39,240 --> 00:06:41,320
 see who we are right now, then

139
00:06:41,320 --> 00:06:44,120
 we don't have to worry about the future either.

140
00:06:44,120 --> 00:06:49,500
 In fact, I think a real part of the Buddha's teaching is

141
00:06:49,500 --> 00:06:53,040
 giving up the future, not worrying

142
00:06:53,040 --> 00:06:57,650
 about even who you're going to be in the future, not living

143
00:06:57,650 --> 00:07:00,080
 in terms of past, present,

144
00:07:00,080 --> 00:07:03,450
 future, living simply in terms of the here and now, who I

145
00:07:03,450 --> 00:07:05,160
 am here and now, what's happened

146
00:07:05,160 --> 00:07:07,340
 here and now, not worrying about where is this going to

147
00:07:07,340 --> 00:07:08,640
 lead, what's going to happen,

148
00:07:08,640 --> 00:07:11,800
 the dangers and the problems and so on.

149
00:07:11,800 --> 00:07:16,400
 Coming to see things here and now as they are.

150
00:07:16,400 --> 00:07:19,500
 I think in the end you really give up past and future and

151
00:07:19,500 --> 00:07:21,440
 you don't ever have any thoughts

152
00:07:21,440 --> 00:07:27,160
 about what the future holds and what was in the past.

153
00:07:27,160 --> 00:07:30,880
 I think it's misleading to go back to the past because it's

154
00:07:30,880 --> 00:07:32,580
 going to send you equally

155
00:07:32,580 --> 00:07:37,970
 as far into the future, worrying or wondering or planning

156
00:07:37,970 --> 00:07:40,480
 to try to create a better life

157
00:07:40,480 --> 00:07:42,920
 in the future.

158
00:07:42,920 --> 00:07:44,880
 Well that might be useful in the short term.

159
00:07:44,880 --> 00:07:48,450
 Eventually you forget it anyway because it's not empirical

160
00:07:48,450 --> 00:07:50,320
 wisdom, it's conceptual.

161
00:07:50,320 --> 00:07:52,890
 It's the same as thinking back to when we were young and

162
00:07:52,890 --> 00:07:54,320
 all the mistakes we made.

163
00:07:54,320 --> 00:08:01,010
 Well yeah, it made us more wise in a worldly sense, it made

164
00:08:01,010 --> 00:08:03,960
 us more able to live and to

165
00:08:03,960 --> 00:08:06,320
 react to the situations around us.

166
00:08:06,320 --> 00:08:09,460
 But it didn't help to stop us from getting greedy, getting

167
00:08:09,460 --> 00:08:11,120
 angry, becoming attached and

168
00:08:11,120 --> 00:08:13,200
 worried and stressed and so on.

169
00:08:13,200 --> 00:08:14,800
 It's not strong enough.

170
00:08:14,800 --> 00:08:18,240
 It's helpful but it's helpful on a limited scale.

171
00:08:18,240 --> 00:08:20,690
 If you're really interested, and I know there are a lot of

172
00:08:20,690 --> 00:08:22,040
 people who are interested in

173
00:08:22,040 --> 00:08:25,960
 learning about your past lives, there are ways of doing

174
00:08:25,960 --> 00:08:28,240
 that, ways of practicing that

175
00:08:28,240 --> 00:08:33,120
 are taught in the textbooks, really.

176
00:08:33,120 --> 00:08:36,120
 These ancient Buddhist textbooks that have been around for

177
00:08:36,120 --> 00:08:37,400
 so long and have been used

178
00:08:37,400 --> 00:08:45,200
 by people to realize their past lives and so on.

179
00:08:45,200 --> 00:08:50,990
 You're welcome to look into that but I don't think it will

180
00:08:50,990 --> 00:08:53,240
 lead you to the goal.

181
00:08:53,240 --> 00:08:56,880
 I think whether or not you've realized your past lives,

182
00:08:56,880 --> 00:08:59,120
 there's still something much more

183
00:08:59,120 --> 00:09:02,300
 important to do and especially for people who have limited

184
00:09:02,300 --> 00:09:04,000
 time and energy and effort.

185
00:09:04,000 --> 00:09:07,690
 It would be much better off to focus on what's really

186
00:09:07,690 --> 00:09:10,280
 important and that's the cause and

187
00:09:10,280 --> 00:09:13,640
 effect relationship that's going on here and now that has

188
00:09:13,640 --> 00:09:15,280
 caused you to make plenty of

189
00:09:15,280 --> 00:09:19,350
 mistakes in the past and will continue to have you make

190
00:09:19,350 --> 00:09:21,920
 mistakes into the future unless

191
00:09:21,920 --> 00:09:27,070
 you can come to see clearly on a phenomenological level

192
00:09:27,070 --> 00:09:30,040
 what is the truth of reality.

193
00:09:30,040 --> 00:09:36,400
 So beneficial, yes, necessary, no and probably not worth

194
00:09:36,400 --> 00:09:39,980
 the effort for most people unless

195
00:09:39,980 --> 00:09:44,380
 you've got a lot of time and good concentration and a

196
00:09:44,380 --> 00:09:46,960
 peaceful surrounding and so on where

197
00:09:46,960 --> 00:09:51,760
 you can really devote your time to that.

198
00:09:51,760 --> 00:09:54,110
 Okay so thanks for the question, there you have it, all the

199
00:09:54,110 --> 00:09:54,520
 best.

