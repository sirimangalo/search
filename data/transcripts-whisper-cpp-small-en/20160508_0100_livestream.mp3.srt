1
00:00:00,000 --> 00:00:14,000
 Well, good evening, everyone.

2
00:00:14,000 --> 00:00:24,000
 I'm not sure if that's any better.

3
00:00:24,000 --> 00:00:33,000
 I'm broadcasting live May 7th.

4
00:00:33,000 --> 00:00:42,000
 Today's quote is about water.

5
00:00:42,000 --> 00:00:48,000
 From the Melinda Panha, which is probably, I would say,

6
00:00:48,000 --> 00:00:53,000
 an underappreciated source of insight.

7
00:00:53,000 --> 00:00:59,000
 It's much later, many years after the Buddha passed away.

8
00:00:59,000 --> 00:01:07,000
 I think at least a few hundred, maybe more.

9
00:01:07,000 --> 00:01:12,000
 But it's very well written, very well organized,

10
00:01:12,000 --> 00:01:16,170
 and it's got much that is insightful about the Buddha's

11
00:01:16,170 --> 00:01:17,000
 teaching.

12
00:01:17,000 --> 00:01:19,000
 It's a lot of questions.

13
00:01:19,000 --> 00:01:25,000
 It's the questions of King Melinda, or Meneander, I think.

14
00:01:25,000 --> 00:01:29,000
 It's a Greek king, identified with a Greek king,

15
00:01:29,000 --> 00:01:34,000
 who conquered India or something.

16
00:01:34,000 --> 00:01:39,000
 He came in contact with these Buddhist monks

17
00:01:39,000 --> 00:01:42,000
 and asked them all sorts of questions.

18
00:01:42,000 --> 00:01:46,000
 Most of them couldn't answer the question.

19
00:01:46,000 --> 00:01:52,000
 They directed him to Nagasena,

20
00:01:52,000 --> 00:01:59,000
 a very well-learned monk who was skilled in debate

21
00:01:59,000 --> 00:02:03,000
 and skilled in oration,

22
00:02:03,000 --> 00:02:07,000
 and skilled in answering questions.

23
00:02:07,000 --> 00:02:10,000
 It's many, many questions. It's quite a large volume.

24
00:02:10,000 --> 00:02:14,000
 It's actually available on the Internet, an old translation

25
00:02:14,000 --> 00:02:14,000
,

26
00:02:14,000 --> 00:02:17,000
 but pretty good.

27
00:02:17,000 --> 00:02:23,000
 You can find it at sacred-texts.org.

28
00:02:23,000 --> 00:02:25,000
 It's worth reading.

29
00:02:25,000 --> 00:02:28,000
 But this is about water.

30
00:02:28,000 --> 00:02:32,810
 It's a comparison, and he makes a lot of comparisons in the

31
00:02:32,810 --> 00:02:34,000
 book.

32
00:02:34,000 --> 00:02:41,160
 He talks about nirvana, using a lot of different

33
00:02:41,160 --> 00:02:42,000
 comparisons, similes.

34
00:02:42,000 --> 00:02:46,000
 But here he's talking about a meditator.

35
00:02:46,000 --> 00:02:52,530
 Let's see if we can find the word for student of meditation

36
00:02:52,530 --> 00:02:53,000
.

37
00:02:53,000 --> 00:02:55,000
 So the question is,

38
00:02:55,000 --> 00:03:00,000
 "Aapasa pancha unga ningahitabhanti?"

39
00:03:00,000 --> 00:03:11,000
 "Katama nidani pancha unga ningahitabhanti?"

40
00:03:11,000 --> 00:03:13,000
 Oh, yeah.

41
00:03:13,000 --> 00:03:17,000
 I think it's part of a larger text.

42
00:03:17,000 --> 00:03:26,980
 He's asking, "What are the five ways, five things, factors

43
00:03:26,980 --> 00:03:28,000
 of water

44
00:03:28,000 --> 00:03:37,000
 that one should grasp, should take up?"

45
00:03:37,000 --> 00:03:43,000
 So how should one be like water?

46
00:03:43,000 --> 00:03:45,000
 I just want to find the word.

47
00:03:45,000 --> 00:03:49,000
 Yoga vatjara.

48
00:03:49,000 --> 00:03:54,000
 Someone who is practicing yoga, basically.

49
00:03:54,000 --> 00:03:57,000
 So this is how the word yoga was used.

50
00:03:57,000 --> 00:04:00,000
 Yoga was used to mean exertion.

51
00:04:00,000 --> 00:04:04,000
 But yoga vatjara is a word that came to mean.

52
00:04:04,000 --> 00:04:07,640
 It was probably adopted from Hinduism, but it was used in

53
00:04:07,640 --> 00:04:08,000
 Buddhism

54
00:04:08,000 --> 00:04:14,000
 to mean someone who is on the spiritual path,

55
00:04:14,000 --> 00:04:22,000
 someone who is exerting themselves.

56
00:04:22,000 --> 00:04:25,000
 We also call them yogi.

57
00:04:25,000 --> 00:04:27,000
 In Thai they use this word a lot.

58
00:04:27,000 --> 00:04:30,000
 They call meditators yogis.

59
00:04:30,000 --> 00:04:33,000
 Yogi, they change it with the word.

60
00:04:33,000 --> 00:04:37,000
 Someone who is practicing yoga is a yogi.

61
00:04:37,000 --> 00:04:40,000
 But it doesn't mean yoga in the modern sense.

62
00:04:40,000 --> 00:04:43,000
 Yoga just means one.

63
00:04:43,000 --> 00:04:46,250
 Because it comes from the word yungu, which means to be

64
00:04:46,250 --> 00:04:47,000
 connected.

65
00:04:47,000 --> 00:04:50,300
 It's actually where I think the word yuta comes from, from

66
00:04:50,300 --> 00:04:51,000
 my name.

67
00:04:51,000 --> 00:04:55,000
 Yuta meaning a yoke, so the ox would have a yoke

68
00:04:55,000 --> 00:05:02,000
 and would be tied to the work.

69
00:05:02,000 --> 00:05:04,000
 So we're comparing water to such a person.

70
00:05:04,000 --> 00:05:07,280
 How should such a person, someone who is undertaking the

71
00:05:07,280 --> 00:05:08,000
 training

72
00:05:08,000 --> 00:05:12,000
 of the Buddha, morality, concentration, wisdom,

73
00:05:12,000 --> 00:05:16,000
 how should they be like water?

74
00:05:16,000 --> 00:05:22,000
 Water is a good object for comparison.

75
00:05:22,000 --> 00:05:27,000
 Why? Because water is naturally calm.

76
00:05:27,000 --> 00:05:32,000
 Water is something that is still, smooth,

77
00:05:32,000 --> 00:05:44,700
 has a reflection in the sense of being perfectly smooth and

78
00:05:44,700 --> 00:05:48,000
 unshaken.

79
00:05:48,000 --> 00:05:51,000
 In the same way a meditator should be like a pool of water,

80
00:05:51,000 --> 00:05:56,000
 dispelling trickery, cajolary, insinuation and dissembling.

81
00:05:56,000 --> 00:06:00,370
 It should be well poised, unshaken, untroubled and quite

82
00:06:00,370 --> 00:06:03,000
 pure by nature.

83
00:06:03,000 --> 00:06:08,000
 Water to us humans, water is like the purest substance.

84
00:06:08,000 --> 00:06:16,000
 It's what we survive on. We need water to survive.

85
00:06:16,000 --> 00:06:20,000
 We're just, funny, we're just talking about water today.

86
00:06:20,000 --> 00:06:26,000
 I was at McMaster University at the Peace Studies booth.

87
00:06:26,000 --> 00:06:32,920
 We were talking to prospective students about the Peace

88
00:06:32,920 --> 00:06:35,000
 Studies program.

89
00:06:35,000 --> 00:06:39,530
 And we were talking about water because the Peace Studies

90
00:06:39,530 --> 00:06:40,000
 department

91
00:06:40,000 --> 00:06:46,000
 has gotten interested in the water situation.

92
00:06:46,000 --> 00:06:51,000
 It's the social justice aspect of Peace Studies.

93
00:06:51,000 --> 00:06:54,000
 I'm not really involved in it.

94
00:06:54,000 --> 00:06:58,600
 So they're trying to address the issue like in Flint,

95
00:06:58,600 --> 00:06:59,000
 Michigan,

96
00:06:59,000 --> 00:07:05,000
 the issue with the First Nations people here,

97
00:07:05,000 --> 00:07:08,000
 not having clean water to drink and to use,

98
00:07:08,000 --> 00:07:12,400
 having polluted water, having their water contaminated with

99
00:07:12,400 --> 00:07:15,000
 chemicals.

100
00:07:15,000 --> 00:07:23,000
 Toxins.

101
00:07:23,000 --> 00:07:26,000
 And we're talking about Taoism.

102
00:07:26,000 --> 00:07:29,410
 One of the people working at the booth had quite a

103
00:07:29,410 --> 00:07:30,000
 conversation with her

104
00:07:30,000 --> 00:07:34,950
 about many things, but one of them was water, how water is

105
00:07:34,950 --> 00:07:38,000
 basically this.

106
00:07:38,000 --> 00:07:40,000
 So I think she'd be interested in this quote.

107
00:07:40,000 --> 00:07:42,000
 She'd probably send it to her.

108
00:07:42,000 --> 00:07:47,000
 Water is pure, water is unshaken, water is calm.

109
00:07:47,000 --> 00:07:49,000
 Water is nurturing, I think she would say,

110
00:07:49,000 --> 00:07:52,000
 because she's into this idea of nurturing.

111
00:07:52,000 --> 00:07:57,000
 She sees it as sort of a feminine quality.

112
00:07:57,000 --> 00:08:00,250
 So we're talking about feminism and feminine qualities and

113
00:08:00,250 --> 00:08:02,000
 masculine qualities.

114
00:08:02,000 --> 00:08:06,000
 Compassion being feminine.

115
00:08:06,000 --> 00:08:08,000
 I mean, traditionally being understood,

116
00:08:08,000 --> 00:08:12,000
 but we're trying to avoid the words feminine and masculine,

117
00:08:12,000 --> 00:08:19,000
 but talking about the different energies in the world.

118
00:08:19,000 --> 00:08:25,000
 We live in a very "masculine world,"

119
00:08:25,000 --> 00:08:28,170
 and so there's a certain type of energy that one might call

120
00:08:28,170 --> 00:08:30,000
 masculine, hard.

121
00:08:30,000 --> 00:08:33,000
 In the end we use the words hard and soft.

122
00:08:33,000 --> 00:08:36,000
 Aggressive, maybe.

123
00:08:36,000 --> 00:08:38,000
 Water is not these things.

124
00:08:38,000 --> 00:08:43,000
 So fire and water, you might say.

125
00:08:43,000 --> 00:08:49,990
 But here it's water is purity, water is clarity, water is

126
00:08:49,990 --> 00:08:51,000
 calm.

127
00:08:51,000 --> 00:08:53,000
 So the first one is this unshaken.

128
00:08:53,000 --> 00:08:56,660
 As a meditator we should be unshaken by the vicissitudes of

129
00:08:56,660 --> 00:08:58,000
 life,

130
00:08:58,000 --> 00:09:01,940
 by the experiences in our meditation, because they'll try

131
00:09:01,940 --> 00:09:03,000
 to shake you.

132
00:09:03,000 --> 00:09:07,000
 And then they'll shake you again, and they'll shake you

133
00:09:07,000 --> 00:09:12,000
 in new and different ways all the time.

134
00:09:12,000 --> 00:09:18,110
 Things will change, and then that way that they change will

135
00:09:18,110 --> 00:09:19,000
 change.

136
00:09:19,000 --> 00:09:23,000
 You have to be adaptive and flexible.

137
00:09:23,000 --> 00:09:27,000
 You have to be clever, and you have to be present.

138
00:09:27,000 --> 00:09:34,470
 Because as soon as you become static, you get carried away

139
00:09:34,470 --> 00:09:42,000
 by the vicissitudes of life.

140
00:09:42,000 --> 00:09:48,000
 The second one is water is poised and naturally cool.

141
00:09:48,000 --> 00:09:55,000
 And so yoga vatsaras should be compassionate,

142
00:09:55,000 --> 00:09:59,610
 should be possessed of patience, should be cool in this way

143
00:09:59,610 --> 00:10:00,000
.

144
00:10:00,000 --> 00:10:04,000
 Patience, love and mercy.

145
00:10:04,000 --> 00:10:08,460
 And they should not be hot-headed, they should not be quick

146
00:10:08,460 --> 00:10:09,000
 to anger,

147
00:10:09,000 --> 00:10:12,000
 they should not be irritable, they should not be impatient.

148
00:10:12,000 --> 00:10:15,000
 Patience is very important.

149
00:10:15,000 --> 00:10:17,990
 Patience with yourself, patience with other people,

150
00:10:17,990 --> 00:10:20,000
 patience.

151
00:10:20,000 --> 00:10:23,000
 And patience with things you want as well,

152
00:10:23,000 --> 00:10:32,760
 just immediately giving up to being controlled by your

153
00:10:32,760 --> 00:10:36,000
 desires.

154
00:10:36,000 --> 00:10:46,780
 As water makes the impure pure, even so, one should be pure

155
00:10:46,780 --> 00:10:47,000
,

156
00:10:47,000 --> 00:10:54,070
 one should have no faults, one should not have any reason

157
00:10:54,070 --> 00:10:55,000
 to be reprimanded by the wise,

158
00:10:55,000 --> 00:10:58,000
 wise people should not be able to sense your one,

159
00:10:58,000 --> 00:11:02,000
 no matter where they are, in private or in public.

160
00:11:02,000 --> 00:11:11,000
 They should be pure in their deeds, in their speech.

161
00:11:11,000 --> 00:11:16,690
 As water is wanted by everyone, even so a meditator should

162
00:11:16,690 --> 00:11:20,000
 be the kind of person who is appreciated,

163
00:11:20,000 --> 00:11:26,960
 you should work to make yourself not a burden, not a source

164
00:11:26,960 --> 00:11:29,000
 of irritation for others,

165
00:11:29,000 --> 00:11:32,250
 because this is problematic both for others and for

166
00:11:32,250 --> 00:11:36,000
 yourself, it creates suffering.

167
00:11:36,000 --> 00:11:39,000
 Obviously, you should not be such a person.

168
00:11:39,000 --> 00:11:45,000
 You should think of water as being,

169
00:11:45,000 --> 00:11:48,000
 water as being valuable, as being pure.

170
00:11:48,000 --> 00:11:55,000
 Whenever someone, someone sees pure water,

171
00:11:55,000 --> 00:12:01,000
 there is no, something negative about water,

172
00:12:01,000 --> 00:12:07,000
 and it's very, very much desired and appreciated.

173
00:12:07,000 --> 00:12:14,000
 We should be like water in that way.

174
00:12:14,000 --> 00:12:19,000
 And finally, water troubles no one, it's kind of the same.

175
00:12:19,000 --> 00:12:22,000
 The first one is, people think positively,

176
00:12:22,000 --> 00:12:26,000
 and the next one is people should not think negatively.

177
00:12:26,000 --> 00:12:29,580
 Don't think negatively of water, they think positively of

178
00:12:29,580 --> 00:12:30,000
 it.

179
00:12:30,000 --> 00:12:33,720
 So a yoga vatyar I should not do anything wrong that makes

180
00:12:33,720 --> 00:12:35,000
 others unhappy.

181
00:12:35,000 --> 00:12:38,000
 Water doesn't hurt people.

182
00:12:38,000 --> 00:12:41,000
 Well, still water doesn't.

183
00:12:41,000 --> 00:12:45,860
 Water actually can be quite troublesome in certain

184
00:12:45,860 --> 00:12:47,000
 instances.

185
00:12:47,000 --> 00:12:50,000
 You can drown with water.

186
00:12:50,000 --> 00:12:58,000
 Water can scald you if it's too hot.

187
00:12:58,000 --> 00:13:03,370
 But here this, the connection, the comparison is with a

188
00:13:03,370 --> 00:13:05,000
 still forest pool,

189
00:13:05,000 --> 00:13:12,000
 calm water, pure water, it's valuable and appreciated.

190
00:13:12,000 --> 00:13:15,360
 I mean, it's a good imagery, it's the kind of thing that we

191
00:13:15,360 --> 00:13:16,000
 can think of

192
00:13:16,000 --> 00:13:20,060
 when you want to think of what kind of a person you'd like

193
00:13:20,060 --> 00:13:21,000
 to be.

194
00:13:21,000 --> 00:13:23,000
 As a meditator, what should I be?

195
00:13:23,000 --> 00:13:26,000
 And water is something.

196
00:13:26,000 --> 00:13:29,710
 It comes easily to mind, of course, because it's so much a

197
00:13:29,710 --> 00:13:31,000
 part of our life.

198
00:13:31,000 --> 00:13:33,730
 And when you think about the purity of water and the still

199
00:13:33,730 --> 00:13:37,000
ness of a pool of water,

200
00:13:37,000 --> 00:13:42,000
 the smoothness of the surface,

201
00:13:42,000 --> 00:13:46,000
 you think about the cleansing nature of water,

202
00:13:46,000 --> 00:13:52,000
 you think about how precious it is, how perfect it is.

203
00:13:52,000 --> 00:14:01,000
 Something to aspire for, aspire towards.

204
00:14:01,000 --> 00:14:04,000
 So, a little bit of Dhamma.

205
00:14:04,000 --> 00:14:09,130
 Today, as I said, I was at McMaster, and then we went to

206
00:14:09,130 --> 00:14:10,000
 see a new house.

207
00:14:10,000 --> 00:14:13,430
 We're thinking about moving to a new house just to get more

208
00:14:13,430 --> 00:14:14,000
 rooms

209
00:14:14,000 --> 00:14:19,170
 because we have many people wanting to come and meditate

210
00:14:19,170 --> 00:14:20,000
 here.

211
00:14:20,000 --> 00:14:24,000
 So we went to look at a house we're talking about.

212
00:14:24,000 --> 00:14:27,000
 We're going to try to look at several different houses

213
00:14:27,000 --> 00:14:30,000
 and see which one is most suitable.

214
00:14:30,000 --> 00:14:32,000
 One today was good.

215
00:14:32,000 --> 00:14:37,860
 Actually, maybe a little smaller than this house, but more

216
00:14:37,860 --> 00:14:40,000
 rooms.

217
00:14:40,000 --> 00:14:43,000
 So, I have to think about whether it might feel a little

218
00:14:43,000 --> 00:14:46,000
 claustrophobic because it's small.

219
00:14:46,000 --> 00:14:50,700
 People have to stay here for a long time, maybe they need

220
00:14:50,700 --> 00:14:52,000
 more space.

221
00:14:52,000 --> 00:14:56,000
 You pack a lot of people into one small house,

222
00:14:56,000 --> 00:14:59,000
 you have to think about that.

223
00:14:59,000 --> 00:15:03,000
 Anyway, any questions?

224
00:15:03,000 --> 00:15:24,000
 [Silence]

225
00:15:24,000 --> 00:15:27,000
 What are your thoughts on the new Kadampa Buddhism?

226
00:15:27,000 --> 00:15:30,000
 I don't have any thoughts on the new Kadampa Buddhism.

227
00:15:30,000 --> 00:15:33,000
 I kind of like to shy away, and I'm happy that I don't know

228
00:15:33,000 --> 00:15:34,000
 things about many things

229
00:15:34,000 --> 00:15:38,260
 because otherwise, as happened with the Lotus Sutra, I tend

230
00:15:38,260 --> 00:15:43,000
 to be somewhat opinionated.

231
00:15:43,000 --> 00:15:47,580
 So, I kind of try to avoid questions like, "What do you

232
00:15:47,580 --> 00:15:48,000
 think of X?"

233
00:15:48,000 --> 00:15:58,000
 unless X is something within our tradition.

234
00:15:58,000 --> 00:16:04,000
 Pretty sure it's Tibetan.

235
00:16:04,000 --> 00:16:07,570
 But, yeah, if a Google search returns many results claiming

236
00:16:07,570 --> 00:16:09,000
 that something is a cult,

237
00:16:09,000 --> 00:16:13,000
 it's a good reason to be wary.

238
00:16:13,000 --> 00:16:16,000
 You should start your own center, start your own group.

239
00:16:16,000 --> 00:16:20,320
 Have people invite people over to your house, start a meet

240
00:16:20,320 --> 00:16:21,000
up.

241
00:16:21,000 --> 00:16:24,000
 Go to meetup.com and start a meditation group.

242
00:16:24,000 --> 00:16:28,730
 Then you can come on our hangout, you can broadcast, and I

243
00:16:28,730 --> 00:16:30,000
 can lead you.

244
00:16:30,000 --> 00:16:33,000
 I can lead you in meditation or something.

245
00:16:33,000 --> 00:16:37,000
 I can give a talk to your group.

246
00:16:37,000 --> 00:16:39,000
 Everyone should start groups in their own area.

247
00:16:39,000 --> 00:16:42,000
 We can have this big network.

248
00:16:42,000 --> 00:16:47,000
 We all meditate together.

249
00:16:47,000 --> 00:16:52,000
 We call it the New Siri Mungalow Buddhism.

250
00:16:52,000 --> 00:17:18,000
 The Siri Mungalow tradition.

251
00:17:18,000 --> 00:17:22,000
 When we're doing walking meditation with our kids, is it as

252
00:17:22,000 --> 00:17:22,000
 effective

253
00:17:22,000 --> 00:17:27,000
 if they like to walk at a much faster pace?

254
00:17:27,000 --> 00:17:29,000
 Probably not.

255
00:17:29,000 --> 00:17:34,000
 I would say walking meditation for young kids is difficult,

256
00:17:34,000 --> 00:17:41,000
 because they tend to be impatient.

257
00:17:41,000 --> 00:17:43,510
 If you want to teach kids how to meditate, it depends on

258
00:17:43,510 --> 00:17:45,000
 their age, I suppose.

259
00:17:45,000 --> 00:17:48,000
 But absolutely, they shouldn't walk fast.

260
00:17:48,000 --> 00:17:51,580
 It's not about walking, it's about being aware of the

261
00:17:51,580 --> 00:17:53,000
 movements of the feet.

262
00:17:53,000 --> 00:17:59,000
 It's not an easy thing.

263
00:17:59,000 --> 00:18:03,570
 Kids tend to be very excited and full of energy, they have

264
00:18:03,570 --> 00:18:05,000
 a hard time being patient.

265
00:18:05,000 --> 00:18:09,000
 So if you can teach them patience, that would be awesome.

266
00:18:09,000 --> 00:18:12,000
 But walking meditation requires patience.

267
00:18:12,000 --> 00:18:15,000
 So that's really the question.

268
00:18:15,000 --> 00:18:17,000
 Can your kids develop patience?

269
00:18:17,000 --> 00:18:20,000
 Because you have to walk slowly.

270
00:18:20,000 --> 00:18:23,000
 You need to develop patience.

271
00:18:23,000 --> 00:18:33,000
 They're walking fast because they're impatient.

272
00:18:33,000 --> 00:18:53,000
 I would think.

273
00:18:53,000 --> 00:18:57,000
 When I meditate, I feel claustrophobic.

274
00:18:57,000 --> 00:19:00,000
 Well, have you read my booklet on how to meditate?

275
00:19:00,000 --> 00:19:03,000
 If you haven't, that's where I'd recommend you start.

276
00:19:03,000 --> 00:19:06,000
 It might help with your claustrophobia.

277
00:19:06,000 --> 00:19:08,000
 Because if you feel that, it might be a fear, right?

278
00:19:08,000 --> 00:19:10,000
 Phobia.

279
00:19:10,000 --> 00:19:13,890
 So then you would say afraid, afraid, or worried, worried,

280
00:19:13,890 --> 00:19:16,000
 or anxious, anxious.

281
00:19:16,000 --> 00:19:24,000
 But if you haven't read my booklet, I recommend that.

282
00:19:24,000 --> 00:19:26,000
 Why avoid these kinds?

283
00:19:26,000 --> 00:19:29,320
 Because I haven't really done so much to answer your

284
00:19:29,320 --> 00:19:30,000
 question,

285
00:19:30,000 --> 00:19:33,590
 I haven't talked about details, because it's actually quite

286
00:19:33,590 --> 00:19:36,000
 simple to fix

287
00:19:36,000 --> 00:19:41,240
 if you're practicing the meditation as I teach, as we teach

288
00:19:41,240 --> 00:19:42,000
.

289
00:19:42,000 --> 00:19:46,860
 So there's not like any special tips for things like claust

290
00:19:46,860 --> 00:19:48,000
rophobia.

291
00:19:48,000 --> 00:19:51,000
 I mean, the thing is, these conditions don't exist.

292
00:19:51,000 --> 00:19:56,330
 You feel fear, and that's the key, is that there's a

293
00:19:56,330 --> 00:19:57,000
 feeling of fear that arises.

294
00:19:57,000 --> 00:20:00,620
 And so the practice that we follow would be to say to

295
00:20:00,620 --> 00:20:01,000
 yourself,

296
00:20:01,000 --> 00:20:04,000
 afraid, afraid.

297
00:20:04,000 --> 00:20:10,120
 But to read my booklet, I hope, I think it outlines that in

298
00:20:10,120 --> 00:20:11,000
 fairly good detail.

299
00:20:13,000 --> 00:20:37,000
 [silence]

300
00:20:37,000 --> 00:20:47,000
 Any other questions?

301
00:20:47,000 --> 00:20:50,170
 I'm really serious about this starting Dhamma groups in

302
00:20:50,170 --> 00:20:51,000
 your area.

303
00:20:51,000 --> 00:20:55,000
 I think it'd be great, and we can do it like once a week.

304
00:20:55,000 --> 00:21:03,000
 We could all meet online, and I could give a talk.

305
00:21:03,000 --> 00:21:05,970
 Or you could just do it every day of the week and just come

306
00:21:05,970 --> 00:21:08,000
 on at 9 p.m. with your group

307
00:21:08,000 --> 00:21:13,000
 and join our hangout.

308
00:21:13,000 --> 00:21:15,370
 Of course, I suppose some people don't want their

309
00:21:15,370 --> 00:21:18,000
 meditation broadcast on the internet.

310
00:21:18,000 --> 00:21:21,000
 I suppose there's that.

311
00:21:21,000 --> 00:21:23,000
 Well, one day a week we could do a private hangout

312
00:21:23,000 --> 00:21:26,000
 that wouldn't have to be broadcast on the internet,

313
00:21:26,000 --> 00:21:31,000
 although it's nice to have the internet community involved.

314
00:21:31,000 --> 00:21:33,000
 Something to think about.

315
00:21:33,000 --> 00:21:35,000
 I have done that though.

316
00:21:35,000 --> 00:21:39,000
 Private Skype calls to groups.

317
00:21:39,000 --> 00:21:43,080
 There was a group in Texas who wanted me to lead them in

318
00:21:43,080 --> 00:21:44,000
 meditation once a week

319
00:21:44,000 --> 00:21:49,000
 and have done online stuff like that.

320
00:21:49,000 --> 00:21:53,000
 It's worth thinking about.

321
00:21:53,000 --> 00:21:55,000
 Okay.

322
00:21:55,000 --> 00:21:57,000
 And I'll say good night.

323
00:21:57,000 --> 00:22:02,200
 Tomorrow, Mother's Day, I'm going to Mississauga for the

324
00:22:02,200 --> 00:22:04,000
 Sri Lankan...

325
00:22:04,000 --> 00:22:06,000
 Wissaka Celebration.

326
00:22:06,000 --> 00:22:09,000
 Wissaka Poonja Celebration.

327
00:22:09,000 --> 00:22:15,000
 Wissaka Poonami, the full moon of the month of Wissaka.

328
00:22:15,000 --> 00:22:17,000
 Anyway, have a good night.

