WEBVTT

00:00:00.000 --> 00:00:09.320
 One of the questions the commentary to the Satipatana Suta

00:00:09.320 --> 00:00:16.440
 asks is, "Why did the Buddha

00:00:16.440 --> 00:00:22.400
 choose four Satipatanas?

00:00:22.400 --> 00:00:25.800
 Why are there four Satipatanas?

00:00:25.800 --> 00:00:30.720
 Why are there only four Satipatanas?"

00:00:30.720 --> 00:00:42.060
 Because the five aggregates are really the same as the four

00:00:42.060 --> 00:00:47.440
 Satipatanas.

00:00:47.440 --> 00:00:50.000
 When we're mindful of the body, this is Rupakandha,

00:00:50.000 --> 00:00:55.360
 mindfulness of Rupakandha.

00:00:55.360 --> 00:01:04.710
 When we're mindful of Vedana, this is Vedana, Vedana-kandha

00:01:04.710 --> 00:01:05.400
.

00:01:05.400 --> 00:01:12.760
 When we're mindful of the mind of Jitta, this is Vinya-nak

00:01:12.760 --> 00:01:14.080
andha.

00:01:14.080 --> 00:01:20.760
 And then we have Sanya and Sankara which are, they're in D

00:01:20.760 --> 00:01:22.480
hamma, you know, in the various

00:01:22.480 --> 00:01:24.760
 places.

00:01:24.760 --> 00:01:33.080
 The five hindrances are Sankara, for example.

00:01:33.080 --> 00:01:34.560
 And it's really all the same.

00:01:34.560 --> 00:01:39.040
 It's just a different way of explaining the same thing.

00:01:39.040 --> 00:01:44.060
 And it offers two, I think at least two answers to the

00:01:44.060 --> 00:01:45.400
 question.

00:01:45.400 --> 00:01:53.680
 One of them is quite interesting and makes for really good

00:01:53.680 --> 00:01:58.760
 food for thought, very practical

00:01:58.760 --> 00:02:01.840
 application of the four Satipatanas.

00:02:01.840 --> 00:02:13.410
 Because we have this teaching on the four vipalasas, the

00:02:13.410 --> 00:02:17.120
 four perversions.

00:02:17.120 --> 00:02:19.200
 And it fits quite nicely with the four Satipatanas.

00:02:19.200 --> 00:02:25.040
 The four perversions, vipalasa means looking at something

00:02:25.040 --> 00:02:27.880
 or seeing something not as it

00:02:27.880 --> 00:02:32.760
 is other than how it really is.

00:02:32.760 --> 00:02:37.240
 So it's like when you step on a rope, if you've ever

00:02:37.240 --> 00:02:40.480
 stepped on a rope and suddenly you jump

00:02:40.480 --> 00:02:43.890
 because you think it was a snake, then if you ever stepped

00:02:43.890 --> 00:02:47.760
 on a rope in a dark room,

00:02:47.760 --> 00:02:50.540
 it's exactly like stepping on a, as you imagine stepping on

00:02:50.540 --> 00:02:51.840
 a snake would feel like.

00:02:51.840 --> 00:02:57.440
 It would be quite scary, quite a shock.

00:02:57.440 --> 00:03:02.480
 So this kind of thing, seeing something not as it is.

00:03:02.480 --> 00:03:06.200
 Receiving something other than how it really is.

00:03:06.200 --> 00:03:10.630
 But there are four that are truly important and are at the

00:03:10.630 --> 00:03:13.400
 core of the Buddhist teaching.

00:03:13.400 --> 00:03:25.070
 These are the Subhavipalasa, Sukavipalasa, Nityavipalasa

00:03:25.070 --> 00:03:28.960
 and Atavipalasa.

00:03:28.960 --> 00:03:36.600
 So the perversion in regards to beauty, perversion in

00:03:36.600 --> 00:03:41.720
 regards to happiness, perversion in regards

00:03:41.720 --> 00:03:50.480
 to permanence and perversion in regards to self.

00:03:50.480 --> 00:03:55.240
 And these correspond perfectly with the four foundations of

00:03:55.240 --> 00:03:56.600
 mindfulness.

00:03:56.600 --> 00:04:00.940
 The best way to overcome the idea of beauty or the per

00:04:00.940 --> 00:04:03.800
version of perceiving beauty in

00:04:03.800 --> 00:04:08.140
 that which is ugly is to look at that which is ugly, which

00:04:08.140 --> 00:04:09.760
 is the physical.

00:04:09.760 --> 00:04:13.200
 The physical that we think of is beautiful.

00:04:13.200 --> 00:04:18.320
 We see beauty in the physical world.

00:04:18.320 --> 00:04:22.160
 And so here comes the Buddhists to tell you that there's no

00:04:22.160 --> 00:04:25.520
 beauty in the physical world,

00:04:25.520 --> 00:04:27.600
 unfortunately no.

00:04:27.600 --> 00:04:33.480
 Well, we use the body as a basis for our understanding, our

00:04:33.480 --> 00:04:36.840
 overcoming this perversion because the

00:04:36.840 --> 00:04:44.800
 body is certainly not something beautiful.

00:04:44.800 --> 00:04:48.200
 There's nothing objectively beautiful about anything.

00:04:48.200 --> 00:04:52.870
 Beauty is something that's subjective, it has to do with

00:04:52.870 --> 00:04:55.360
 the mind, the feelings that

00:04:55.360 --> 00:05:00.990
 it arouses in the mind, the sensations that come from

00:05:00.990 --> 00:05:04.840
 seeing certain things that perhaps

00:05:04.840 --> 00:05:10.580
 remind us of certain things or are calming to the senses

00:05:10.580 --> 00:05:11.920
 and so on.

00:05:11.920 --> 00:05:18.200
 They bring feelings of pleasure or feelings of calm to the

00:05:18.200 --> 00:05:21.720
 mind or to the brain, whatever,

00:05:21.720 --> 00:05:26.520
 as opposed to seeing jarring signs.

00:05:26.520 --> 00:05:30.870
 So the one thing that is most pleasing to the mind, the

00:05:30.870 --> 00:05:33.760
 Buddha said, is the human body.

00:05:33.760 --> 00:05:38.130
 We find the human body to be quite pleasing and it reminds

00:05:38.130 --> 00:05:40.760
 us of so many different pleasant

00:05:40.760 --> 00:05:48.250
 sensations from our parents, embrace, and of course more to

00:05:48.250 --> 00:05:52.560
 the point, the sexual attraction

00:05:52.560 --> 00:05:59.200
 that comes from the physical body.

00:05:59.200 --> 00:06:11.610
 And it comes from seeing the body as something that is

00:06:11.610 --> 00:06:17.040
 somehow beautiful.

00:06:17.040 --> 00:06:20.750
 So this isn't a teaching to take lightly, it's not a

00:06:20.750 --> 00:06:23.240
 teaching that you can present to

00:06:23.240 --> 00:06:27.000
 the world at large with much success.

00:06:27.000 --> 00:06:32.690
 And that's because the world at large has no interest in

00:06:32.690 --> 00:06:35.480
 objectivity, really.

00:06:35.480 --> 00:06:45.710
 They think of this as something dry or something unwelcome,

00:06:45.710 --> 00:06:52.000
 something uninteresting, the idea

00:06:52.000 --> 00:06:56.200
 of seeing things just as they are.

00:06:56.200 --> 00:07:02.180
 And people mostly are happy to have their likes and disl

00:07:02.180 --> 00:07:05.760
ikes and their partialities.

00:07:05.760 --> 00:07:14.770
 They like to find beauty in things and therefore ugliness

00:07:14.770 --> 00:07:17.880
 in other things.

00:07:17.880 --> 00:07:20.840
 And we can't hide the truth.

00:07:20.840 --> 00:07:24.190
 And the very truth is that happiness doesn't come from

00:07:24.190 --> 00:07:24.920
 beauty.

00:07:24.920 --> 00:07:29.800
 It doesn't come from the appreciation of beauty.

00:07:29.800 --> 00:07:32.160
 Appreciation of beauty only leads to addiction.

00:07:32.160 --> 00:07:36.390
 It's something that might for a short time, even a whole

00:07:36.390 --> 00:07:38.680
 lifetime, which is of course

00:07:38.680 --> 00:07:43.510
 a short time, might lead to pleasurable states because of

00:07:43.510 --> 00:07:48.400
 how the mind reacts to the experience.

00:07:48.400 --> 00:07:59.680
 It gives rise to pleasure.

00:07:59.680 --> 00:08:02.730
 Chemicals in the brain provide us the stimulus saying, "

00:08:02.730 --> 00:08:03.960
That's beautiful.

00:08:03.960 --> 00:08:05.160
 That's good.

00:08:05.160 --> 00:08:08.880
 That makes me feel good inside, all warm and fuzzy."

00:08:08.880 --> 00:08:13.130
 Or when you see the human body, when you see a beautiful

00:08:13.130 --> 00:08:15.600
 body, it leads to hormones and

00:08:15.600 --> 00:08:21.110
 very strong chemical reactions, which then are taken as

00:08:21.110 --> 00:08:24.520
 pleasurable and lead to addiction

00:08:24.520 --> 00:08:38.240
 and sexual desire and sensual desire.

00:08:38.240 --> 00:08:42.640
 The point is that especially with beauty, it's a cause for

00:08:42.640 --> 00:08:46.160
 great busyness and great stress.

00:08:46.160 --> 00:08:49.770
 You think of how much work we have to go through in this

00:08:49.770 --> 00:08:52.400
 world just to be able to enjoy beauty

00:08:52.400 --> 00:09:05.750
 and how our intense addiction to pleasurable stimuli in

00:09:05.750 --> 00:09:13.400
 terms of beauty, the need for immediate

00:09:13.400 --> 00:09:19.750
 beauty is actually degrading the beauty in the world around

00:09:19.750 --> 00:09:20.440
 us.

00:09:20.440 --> 00:09:23.750
 The beauty of the environment, for example, the natural

00:09:23.750 --> 00:09:25.600
 environment is something that

00:09:25.600 --> 00:09:27.000
 is very calming to the mind.

00:09:27.000 --> 00:09:30.740
 It's something that would have given our ancestors a great

00:09:30.740 --> 00:09:32.840
 amount of pleasure to behold.

00:09:32.840 --> 00:09:42.070
 But nowadays, our unending quest for immediate pleasure,

00:09:42.070 --> 00:09:47.040
 which brings us high definition

00:09:47.040 --> 00:10:00.580
 in television and movies and music videos and so on, it

00:10:00.580 --> 00:10:06.720
 brings us pornography and all

00:10:06.720 --> 00:10:12.740
 of these things causing us to lose sight of what's

00:10:12.740 --> 00:10:19.040
 important, our need for intense experience.

00:10:19.040 --> 00:10:21.870
 It's causing us, most people now don't go out and look at

00:10:21.870 --> 00:10:23.040
 the stars anymore.

00:10:23.040 --> 00:10:26.320
 They sit inside and watch television or use the computer.

00:10:26.320 --> 00:10:34.540
 Of course, all the beautiful visual stimulants on the

00:10:34.540 --> 00:10:40.880
 internet, pictures, videos, YouTube

00:10:40.880 --> 00:10:45.680
 and so on.

00:10:45.680 --> 00:10:49.930
 When I was young, we still went out and looked at the stars

00:10:49.930 --> 00:10:52.160
, pitch a tent and go and look

00:10:52.160 --> 00:10:54.200
 at the stars and watch the stars go.

00:10:54.200 --> 00:10:56.690
 We don't do that so much and we're destroying the

00:10:56.690 --> 00:10:58.960
 environment and it's becoming hotter and

00:10:58.960 --> 00:11:01.000
 so on.

00:11:01.000 --> 00:11:08.350
 You can see how our life degrades through our addictions,

00:11:08.350 --> 00:11:12.600
 especially to the human body.

00:11:12.600 --> 00:11:14.120
 How much work we have to do.

00:11:14.120 --> 00:11:19.420
 People have to now work 40, 50 hours a week, even more

00:11:19.420 --> 00:11:22.360
 sometimes, just to support their

00:11:22.360 --> 00:11:23.360
 family for example.

00:11:23.360 --> 00:11:26.280
 You get married and you have to support a family.

00:11:26.280 --> 00:11:37.210
 You need a big house and so on, all in order to pay for our

00:11:37.210 --> 00:11:40.520
 sensual addiction.

00:11:40.520 --> 00:11:46.590
 This isn't something to preach this evil or so on and how

00:11:46.590 --> 00:11:48.120
 bad we are.

00:11:48.120 --> 00:11:51.500
 It is something to give encouragement, to give up this kind

00:11:51.500 --> 00:11:52.560
 of addiction.

00:11:52.560 --> 00:11:57.350
 That, hey, maybe if we could give up whatever addiction we

00:11:57.350 --> 00:12:00.240
 have to beauty for example, maybe

00:12:00.240 --> 00:12:03.160
 we could be more content.

00:12:03.160 --> 00:12:06.240
 Certainly we could be more content and it would save us a

00:12:06.240 --> 00:12:07.680
 lot of hassle and a lot of

00:12:07.680 --> 00:12:08.680
 trouble.

00:12:08.680 --> 00:12:09.680
 We could live simpler.

00:12:09.680 --> 00:12:13.840
 We wouldn't need a high paying job.

00:12:13.840 --> 00:12:15.400
 We wouldn't need to go into debt.

00:12:15.400 --> 00:12:19.730
 We wouldn't need to feel stress and we wouldn't need to do

00:12:19.730 --> 00:12:20.960
 evil deeds.

00:12:20.960 --> 00:12:25.550
 We wouldn't need to trick others and other people and cheat

00:12:25.550 --> 00:12:27.800
 and deceive others in order

00:12:27.800 --> 00:12:29.800
 to get what we want.

00:12:29.800 --> 00:12:34.460
 We wouldn't fight with our siblings or our spouses or our

00:12:34.460 --> 00:12:36.800
 children or our parents.

00:12:36.800 --> 00:12:42.970
 We wouldn't fight so much because we wouldn't be partial to

00:12:42.970 --> 00:12:45.960
 this or that experience.

00:12:45.960 --> 00:12:49.110
 When it's time to take out the trash we wouldn't wrinkle up

00:12:49.110 --> 00:12:50.760
 our nose, we'd just take out the

00:12:50.760 --> 00:12:57.800
 trash and so on.

00:12:57.800 --> 00:13:00.920
 So giving up beauty, well first of all it doesn't mean not

00:13:00.920 --> 00:13:02.320
 having beauty in life, it

00:13:02.320 --> 00:13:06.960
 just means not being attached to it, not putting labels on

00:13:06.960 --> 00:13:07.880
 things.

00:13:07.880 --> 00:13:11.180
 Many people like to think of this as finding beauty in

00:13:11.180 --> 00:13:12.200
 everything.

00:13:12.200 --> 00:13:13.440
 It's not really the case.

00:13:13.440 --> 00:13:18.620
 You don't find beauty in anything really, but you don't

00:13:18.620 --> 00:13:20.120
 need beauty.

00:13:20.120 --> 00:13:23.760
 People always have such a big problem with teachings like

00:13:23.760 --> 00:13:25.520
 this but the fact that they

00:13:25.520 --> 00:13:32.270
 have a problem with them really is an example of the

00:13:32.270 --> 00:13:36.520
 problem, this partiality.

00:13:36.520 --> 00:13:39.220
 When someone tells you, if I were to tell you stop you're

00:13:39.220 --> 00:13:40.440
 no longer allowed to look

00:13:40.440 --> 00:13:42.280
 at beautiful things you'd be upset.

00:13:42.280 --> 00:13:45.450
 Most people would be quite upset, which is really the point

00:13:45.450 --> 00:13:45.720
.

00:13:45.720 --> 00:13:48.940
 A person who doesn't need beauty will never get upset when

00:13:48.940 --> 00:13:50.280
 they have no beauty.

00:13:50.280 --> 00:13:55.060
 It doesn't mean they're just a blank robot, but it means

00:13:55.060 --> 00:13:57.440
 they always have peace in their

00:13:57.440 --> 00:13:58.440
 minds.

00:13:58.440 --> 00:14:00.640
 When they experience beauty they have peace in their minds.

00:14:00.640 --> 00:14:05.450
 When they experience ugliness they have peace in their

00:14:05.450 --> 00:14:06.360
 minds.

00:14:06.360 --> 00:14:09.520
 So it's a useful thing to be able to free yourself.

00:14:09.520 --> 00:14:14.980
 It's not becoming flat lined, it's freeing yourself from

00:14:14.980 --> 00:14:17.520
 the need for things so that

00:14:17.520 --> 00:14:19.640
 you can still experience these things.

00:14:19.640 --> 00:14:21.990
 You can even experience all the pleasure that comes from

00:14:21.990 --> 00:14:23.400
 beauty, but you don't care for

00:14:23.400 --> 00:14:25.000
 it.

00:14:25.000 --> 00:14:26.400
 You aren't partial towards it.

00:14:26.400 --> 00:14:30.000
 Even when there's great pleasure in the mind you see it

00:14:30.000 --> 00:14:32.440
 simply as an experience that arises

00:14:32.440 --> 00:14:33.820
 and ceases.

00:14:33.820 --> 00:14:37.980
 And so the same with ugliness and the displeasure or the

00:14:37.980 --> 00:14:41.120
 disharmony that arises, the pain that

00:14:41.120 --> 00:14:43.760
 arises from seeing ugly things.

00:14:43.760 --> 00:14:51.120
 You don't see it as painful, you don't see it as bad.

00:14:51.120 --> 00:14:57.000
 So this is why the practice is to use mindfulness of the

00:14:57.000 --> 00:14:58.000
 body.

00:14:58.000 --> 00:15:03.300
 So even just watching the movements of the body helps us to

00:15:03.300 --> 00:15:05.880
 see that, to break down this

00:15:05.880 --> 00:15:10.230
 idea of there being an entity that we call the body, the

00:15:10.230 --> 00:15:12.760
 body being something concrete

00:15:12.760 --> 00:15:18.440
 or whole.

00:15:18.440 --> 00:15:22.920
 When we lose this concept of the body it really takes away

00:15:22.920 --> 00:15:25.400
 the idea of something to worry

00:15:25.400 --> 00:15:30.320
 about as being beautiful or ugly.

00:15:30.320 --> 00:15:38.340
 It helps us to see that in any case the body is a Buddhist

00:15:38.340 --> 00:15:41.640
 and a nest of sores.

00:15:41.640 --> 00:15:45.640
 It can be a cause for great suffering.

00:15:45.640 --> 00:15:49.860
 And so for partial towards the good positive sensations

00:15:49.860 --> 00:15:52.920
 will be partial against the unpleasant

00:15:52.920 --> 00:15:56.680
 ones.

00:15:56.680 --> 00:16:00.850
 So this is the idea that when we're actually mindful of the

00:16:00.850 --> 00:16:03.000
 body we won't be so attracted

00:16:03.000 --> 00:16:04.000
 to the human body.

00:16:04.000 --> 00:16:09.760
 You will see that it's just a series of experiences, it's

00:16:09.760 --> 00:16:12.840
 not really an entity of any sort.

00:16:12.840 --> 00:16:16.760
 And through watching the body you slowly lose your

00:16:16.760 --> 00:16:19.960
 attachment to beauty, to the physical

00:16:19.960 --> 00:16:27.000
 beauty, the body is a beautiful thing.

00:16:27.000 --> 00:16:33.710
 The second one is happiness, the perversion of finding

00:16:33.710 --> 00:16:35.480
 happiness.

00:16:35.480 --> 00:16:39.350
 This is overcome through the practice of Vedana and the

00:16:39.350 --> 00:16:40.160
 person.

00:16:40.160 --> 00:16:41.400
 This is of course even worse.

00:16:41.400 --> 00:16:44.560
 Now first I'm telling you to give up beauty and next we're

00:16:44.560 --> 00:16:47.320
 telling you to give up happiness.

00:16:47.320 --> 00:16:51.760
 It just gets worse and worse.

00:16:51.760 --> 00:16:56.560
 But again that's the point is when you need happiness, no

00:16:56.560 --> 00:16:58.920
 not when you need happiness,

00:16:58.920 --> 00:17:04.430
 when you believe that pleasurable feelings are happiness,

00:17:04.430 --> 00:17:06.880
 then you're always going to

00:17:06.880 --> 00:17:10.480
 be partial towards them and upset when you can't have them.

00:17:10.480 --> 00:17:15.660
 So if I told you you're not allowed to enjoy or chase after

00:17:15.660 --> 00:17:18.880
 pleasant feelings, most people

00:17:18.880 --> 00:17:20.320
 would be quite upset about that.

00:17:20.320 --> 00:17:23.640
 They think, "Well how can I be happy then?"

00:17:23.640 --> 00:17:27.140
 And if they were, most people when they're without these

00:17:27.140 --> 00:17:28.920
 feelings for some amount of

00:17:28.920 --> 00:17:31.800
 time, they become quite upset.

00:17:31.800 --> 00:17:38.500
 They become bored, they become frustrated, they become

00:17:38.500 --> 00:17:42.280
 angry, they become displeased

00:17:42.280 --> 00:17:48.800
 and hard to deal with.

00:17:48.800 --> 00:17:53.520
 So in this way we're much like drug addicts, we're addicted

00:17:53.520 --> 00:17:55.100
 to our pleasure.

00:17:55.100 --> 00:18:01.720
 So we find ways to obtain pleasurable feelings.

00:18:01.720 --> 00:18:03.040
 We get quite good at it actually.

00:18:03.040 --> 00:18:06.470
 If you study yourself, you can see where you, throughout

00:18:06.470 --> 00:18:10.360
 the day where you try to find pleasure

00:18:10.360 --> 00:18:14.440
 and where you do things for the purpose of finding pleasure

00:18:14.440 --> 00:18:14.720
.

00:18:14.720 --> 00:18:17.240
 If you watch other people you can watch them as well,

00:18:17.240 --> 00:18:19.400
 trying to find their pleasure throughout

00:18:19.400 --> 00:18:20.400
 the day.

00:18:20.400 --> 00:18:23.520
 "Okay, now it's time for this, why?

00:18:23.520 --> 00:18:26.120
 Because it's going to make you pleasure."

00:18:26.120 --> 00:18:28.150
 We take it for granted, we think, "Well of course, because

00:18:28.150 --> 00:18:29.640
 that's where happiness comes

00:18:29.640 --> 00:18:30.640
 from."

00:18:30.640 --> 00:18:37.500
 And this is the misperception, the perversion, perverted

00:18:37.500 --> 00:18:39.000
 perception.

00:18:39.000 --> 00:18:41.710
 This means not seeing things as they are because there's

00:18:41.710 --> 00:18:43.880
 nothing positive about pleasant feelings

00:18:43.880 --> 00:18:48.000
 at all.

00:18:48.000 --> 00:18:54.690
 The moment that you find them positive, that you find them

00:18:54.690 --> 00:18:58.920
 somehow better than other feelings,

00:18:58.920 --> 00:19:04.640
 you become partial towards them and you become dependent on

00:19:04.640 --> 00:19:07.480
 them to the extent that it can

00:19:07.480 --> 00:19:15.640
 lead to a real addiction.

00:19:15.640 --> 00:19:18.900
 Where when we don't get what we want we become frustrated,

00:19:18.900 --> 00:19:21.120
 we become upset, we become difficult

00:19:21.120 --> 00:19:23.280
 to deal with.

00:19:23.280 --> 00:19:26.360
 We fight with other people, we argue.

00:19:26.360 --> 00:19:30.720
 If you ever found yourself indulging in pleasure, it's

00:19:30.720 --> 00:19:33.720
 interesting how when you're indulging,

00:19:33.720 --> 00:19:35.850
 when you're finding pleasure in something, you're so

00:19:35.850 --> 00:19:37.960
 pleased by it and then you're interrupted,

00:19:37.960 --> 00:19:39.680
 how quickly we become angry.

00:19:39.680 --> 00:19:41.200
 Isn't it interesting?

00:19:41.200 --> 00:19:46.070
 If it really made you a happier person, why would you get

00:19:46.070 --> 00:19:47.920
 angry so quickly?

00:19:47.920 --> 00:19:51.840
 Compare that to a person who isn't addicted to things.

00:19:51.840 --> 00:19:55.310
 When they're interrupted, they're at peace and they weren't

00:19:55.310 --> 00:19:56.920
 interrupted, things just

00:19:56.920 --> 00:20:00.800
 changed and they're able to deal with the change.

00:20:00.800 --> 00:20:04.530
 And people who enjoy pleasure so much, very easily annoyed

00:20:04.530 --> 00:20:06.960
 when they don't get what they

00:20:06.960 --> 00:20:07.960
 want.

00:20:07.960 --> 00:20:11.680
 So we just get better and better throughout our lives at

00:20:11.680 --> 00:20:13.960
 chasing the pleasure and finding

00:20:13.960 --> 00:20:18.890
 ways to obtain pleasure throughout our day and throughout

00:20:18.890 --> 00:20:20.040
 our lives.

00:20:20.040 --> 00:20:24.640
 And so we find this comfort zone and we think we've got it

00:20:24.640 --> 00:20:26.960
 made and we forget about all

00:20:26.960 --> 00:20:30.020
 the suffering that we had to go through just to get to

00:20:30.020 --> 00:20:31.200
 where we are today.

00:20:31.200 --> 00:20:33.160
 It's very easy to forget.

00:20:33.160 --> 00:20:35.920
 We're good at forgetting things.

00:20:35.920 --> 00:20:37.920
 We're good at forgetting our pain.

00:20:37.920 --> 00:20:41.040
 We're good at remembering it as well.

00:20:41.040 --> 00:20:46.280
 It depends on the person, it depends on the experience.

00:20:46.280 --> 00:20:47.980
 But we're good at forgetting all the lessons that we had

00:20:47.980 --> 00:20:48.360
 today.

00:20:48.360 --> 00:20:53.880
 We're forgetting the fact that, forgetting how hard it was

00:20:53.880 --> 00:20:55.920
 to get where we are.

00:20:55.920 --> 00:20:58.570
 Forgetting about the future, forgetting about death,

00:20:58.570 --> 00:21:00.280
 forgetting about all the people we've

00:21:00.280 --> 00:21:05.010
 seen who died miserable deaths and how we might be one of

00:21:05.010 --> 00:21:05.800
 them.

00:21:05.800 --> 00:21:08.450
 Because that's the other thing, when you're addicted to

00:21:08.450 --> 00:21:10.320
 pleasure, it gets a lot more difficult

00:21:10.320 --> 00:21:13.840
 to deal with death and dying, old age.

00:21:13.840 --> 00:21:21.160
 Some people cry like babies when they die.

00:21:21.160 --> 00:21:24.080
 We might be lucky and we might die in peace.

00:21:24.080 --> 00:21:27.080
 But we have to come back and do it all over again.

00:21:27.080 --> 00:21:30.880
 The more lust, the more desire we have in the mind, the

00:21:30.880 --> 00:21:32.480
 more attachment to pleasant

00:21:32.480 --> 00:21:34.400
 feelings we have.

00:21:34.400 --> 00:21:37.360
 The more hungry we will be when we come back.

00:21:37.360 --> 00:21:40.180
 This is why people are born as, beings are born as ghosts

00:21:40.180 --> 00:21:41.560
 because they die with much

00:21:41.560 --> 00:21:44.960
 attachment.

00:21:44.960 --> 00:21:48.350
 This is why they haunt, ghosts haunt certain areas because

00:21:48.350 --> 00:21:50.240
 of their attachment to certain

00:21:50.240 --> 00:21:54.920
 people and so on.

00:21:54.920 --> 00:22:02.760
 There's nothing positive about pleasant feelings.

00:22:02.760 --> 00:22:04.000
 There's nothing wrong with them.

00:22:04.000 --> 00:22:08.240
 It doesn't mean that we practice again to never experience

00:22:08.240 --> 00:22:09.960
 pleasant feelings.

00:22:09.960 --> 00:22:13.840
 But we come to be free from our need for them.

00:22:13.840 --> 00:22:15.440
 We become free.

00:22:15.440 --> 00:22:19.560
 Whatever you say about a person who gives up these things,

00:22:19.560 --> 00:22:20.200
 you've got to admit that

00:22:20.200 --> 00:22:21.200
 they're free.

00:22:21.200 --> 00:22:25.040
 They're no longer a slave to their desires.

00:22:25.040 --> 00:22:30.720
 This is really the key benefit.

00:22:30.720 --> 00:22:33.600
 Freedom is, if you read the Buddha's teaching, you see it's

00:22:33.600 --> 00:22:34.880
 all about freedom and that's

00:22:34.880 --> 00:22:36.000
 really what this is.

00:22:36.000 --> 00:22:38.040
 Freedom from these things.

00:22:38.040 --> 00:22:45.600
 Not being a slave to beauty, not being a slave to pleasure.

00:22:45.600 --> 00:22:51.000
 Some of the mindful feelings will come to see this.

00:22:51.000 --> 00:22:53.080
 We acknowledge the happy, happier pleasure, pleasure.

00:22:53.080 --> 00:22:54.600
 We acknowledge the pain as well.

00:22:54.600 --> 00:22:59.200
 We acknowledge the neutral feelings, calm, calm.

00:22:59.200 --> 00:23:02.740
 We come to see, realize that feelings are all just feelings

00:23:02.740 --> 00:23:03.000
.

00:23:03.000 --> 00:23:05.120
 They come and they go.

00:23:05.120 --> 00:23:07.670
 Sometimes there's happy feelings, sometimes there's painful

00:23:07.670 --> 00:23:08.240
 feelings.

00:23:08.240 --> 00:23:16.380
 And we come to be free from any partiality or any need for

00:23:16.380 --> 00:23:21.480
 one type of feeling or another.

00:23:21.480 --> 00:23:29.440
 The third is the perversion of permanence.

00:23:29.440 --> 00:23:58.540
 Per

00:23:58.540 --> 00:24:12.800
 the

00:24:12.800 --> 00:24:32.900
 maximum

00:24:32.900 --> 00:25:02.280
 of

00:25:02.280 --> 00:25:26.160
 the

00:25:26.160 --> 00:25:27.160
 mind.

00:25:27.160 --> 00:25:28.160
 The mind is always in the mind.

00:25:28.160 --> 00:25:29.160
 The mind is always in the mind.

00:25:29.160 --> 00:25:30.160
 The mind is always in the mind.

00:25:30.160 --> 00:25:31.160
 The mind is always in the mind.

00:25:31.160 --> 00:25:32.160
 The mind is always in the mind.

00:25:32.160 --> 00:25:33.160
 The mind is always in the mind.

00:25:33.160 --> 00:25:34.160
 The mind is always in the mind.

00:25:34.160 --> 00:25:35.160
 The mind is always in the mind.

00:25:35.160 --> 00:25:36.160
 The mind is always in the mind.

00:25:36.160 --> 00:25:37.160
 The mind is always in the mind.

00:25:37.160 --> 00:25:38.160
 The mind is always in the mind.

00:25:38.160 --> 00:25:39.160
 The mind is always in the mind.

00:25:39.160 --> 00:25:40.160
 The mind is always in the mind.

00:25:40.160 --> 00:25:41.160
 The mind is always in the mind.

00:25:41.160 --> 00:25:42.160
 The mind is always in the mind.

00:25:42.160 --> 00:25:43.160
 The mind is always in the mind.

00:25:43.160 --> 00:25:44.160
 The mind is always in the mind.

00:25:44.160 --> 00:25:45.160
 The mind is always in the mind.

00:25:45.160 --> 00:25:46.160
 The mind is always in the mind.

00:25:46.160 --> 00:25:47.160
 The mind is always in the mind.

00:25:47.160 --> 00:25:48.160
 The mind is always in the mind.

00:25:48.160 --> 00:25:55.160
 The mind is always in the mind.

00:25:55.160 --> 00:25:58.160
 The mind is always in the mind.

00:25:58.160 --> 00:26:01.160
 The mind is always in the mind.

00:26:01.160 --> 00:26:03.160
 The mind is always in the mind.

00:26:03.160 --> 00:26:05.160
 The mind is always in the mind.

00:26:05.160 --> 00:26:08.160
 The mind is always in the mind.

00:26:08.160 --> 00:26:11.160
 The mind is always in the mind.

00:26:11.160 --> 00:26:14.160
 The mind is always in the mind.

00:26:14.160 --> 00:26:19.160
 The mind is always in the mind.

00:26:19.160 --> 00:26:22.160
 The mind is always in the mind.

00:26:22.160 --> 00:26:24.160
 The mind is always in the mind.

00:26:24.160 --> 00:26:26.160
 The mind is always in the mind.

00:26:26.160 --> 00:26:28.160
 The mind is always in the mind.

00:26:28.160 --> 00:26:30.160
 The mind is always in the mind.

00:26:30.160 --> 00:26:32.160
 The mind is always in the mind.

00:26:32.160 --> 00:26:34.160
 The mind is always in the mind.

00:26:34.160 --> 00:26:36.160
 The mind is always in the mind.

00:26:36.160 --> 00:26:38.160
 The mind is always in the mind.

00:26:38.160 --> 00:26:40.160
 The mind is always in the mind.

00:26:40.160 --> 00:26:42.160
 The mind is always in the mind.

00:26:42.160 --> 00:26:45.160
 This is the third one.

00:26:45.160 --> 00:26:53.160
 The fourth is perversion in regards to self.

00:26:53.160 --> 00:26:56.160
 The idea that there is not a lasting self,

00:26:56.160 --> 00:26:58.160
 but a controlling self,

00:26:58.160 --> 00:27:03.160
 the acting self.

00:27:03.160 --> 00:27:08.160
 And so the Dhamma is mindfulness of the Dhamma.

00:27:08.160 --> 00:27:10.160
 If you look at the Dhammas in the Satipatanas,

00:27:10.160 --> 00:27:14.160
 it's easy to understand how this works.

00:27:14.160 --> 00:27:17.160
 So first of all, the five hindrances,

00:27:17.160 --> 00:27:21.160
 not exactly directly related to the perception of non-self,

00:27:21.160 --> 00:27:25.160
 but are very helpful because the five hindrances

00:27:25.160 --> 00:27:29.160
 are a good part of our emotions.

00:27:29.160 --> 00:27:33.160
 And so we think of emotions as being me and mine.

00:27:33.160 --> 00:27:34.160
 I am the one who gets angry.

00:27:34.160 --> 00:27:37.160
 I am the one who wants things.

00:27:37.160 --> 00:27:45.160
 I am the one who doubts when there's doubt and so on.

00:27:45.160 --> 00:27:48.010
 And when we look at these things and see them just as they

00:27:48.010 --> 00:27:48.160
 are,

00:27:48.160 --> 00:27:53.150
 I'm saying liking, liking, disliking, drowsy, drowsy,

00:27:53.150 --> 00:27:53.160
 distracting,

00:27:53.160 --> 00:27:57.160
 doubting, worrying and afraid and so on.

00:27:57.160 --> 00:27:59.160
 All of these things that we thought were ours,

00:27:59.160 --> 00:28:02.160
 we see that they're not.

00:28:02.160 --> 00:28:03.160
 We're not giving rise to them.

00:28:03.160 --> 00:28:06.160
 We're not the ones getting angry.

00:28:06.160 --> 00:28:09.160
 Sometimes people, no matter how much they acknowledge anger

00:28:09.160 --> 00:28:09.160
,

00:28:09.160 --> 00:28:14.160
 it just bubbles up and boils over.

00:28:14.160 --> 00:28:17.160
 It's impossible to control.

00:28:17.160 --> 00:28:19.160
 When they think they're doing something wrong

00:28:19.160 --> 00:28:21.160
 or they think they're not capable,

00:28:21.160 --> 00:28:23.160
 or meditation's not helping,

00:28:23.160 --> 00:28:25.160
 sometimes come with these doubts that,

00:28:25.160 --> 00:28:26.160
 "No, this meditation's not working.

00:28:26.160 --> 00:28:30.160
 I acknowledge angry, angry, I just get more angry."

00:28:30.160 --> 00:28:34.160
 So they try to blame the noting as making them more angry,

00:28:34.160 --> 00:28:36.160
 which of course isn't the case.

00:28:36.160 --> 00:28:41.160
 It's something that is cooped up inside

00:28:41.160 --> 00:28:43.160
 and has been building up inside,

00:28:43.160 --> 00:28:49.460
 and there's this habit of getting angry or greedy or

00:28:49.460 --> 00:28:51.160
 whatever.

00:28:51.160 --> 00:28:55.160
 Habits of doubting, for example.

00:28:55.160 --> 00:28:57.160
 We're so good at tricking ourselves.

00:28:57.160 --> 00:28:59.160
 We use these five hindrances.

00:28:59.160 --> 00:29:03.160
 We trick ourselves with them.

00:29:04.160 --> 00:29:07.160
 Or actually not even we trick ourselves.

00:29:07.160 --> 00:29:12.160
 These are habits that form based on cause and effect.

00:29:12.160 --> 00:29:16.160
 They're not under our control.

00:29:16.160 --> 00:29:17.160
 They don't belong to us.

00:29:17.160 --> 00:29:20.160
 They just come and they go.

00:29:20.160 --> 00:29:21.880
 But then of course you have the five aggregates are also

00:29:21.880 --> 00:29:22.160
 here,

00:29:22.160 --> 00:29:26.160
 the six senses are also here.

00:29:26.160 --> 00:29:28.160
 In Dhamma you have many things that are useful

00:29:28.160 --> 00:29:36.160
 for giving up the idea of self.

00:29:36.160 --> 00:29:38.160
 The point of non-self,

00:29:38.160 --> 00:29:40.160
 of realizing that self is just an illusion

00:29:40.160 --> 00:29:44.160
 or a perversion of perception,

00:29:44.160 --> 00:29:49.160
 is that you can't really control things.

00:29:49.160 --> 00:29:52.160
 The mind that intends to control creates a sankara,

00:29:52.160 --> 00:29:58.160
 creates an artificial formation,

00:29:58.160 --> 00:30:00.160
 something that seems like control,

00:30:00.160 --> 00:30:04.160
 but it's actually just stress and pressure

00:30:04.160 --> 00:30:09.160
 that builds up and builds up and then collapses.

00:30:09.160 --> 00:30:10.160
 The mind that wants to control,

00:30:10.160 --> 00:30:13.160
 does not get angry, for example.

00:30:13.160 --> 00:30:16.160
 Just bottles up the anger, really.

00:30:16.160 --> 00:30:26.160
 Reacts to the anger, forces the reaction to the anger.

00:30:26.160 --> 00:30:32.160
 Instead of blowing up and yelling and implodes,

00:30:32.160 --> 00:30:37.160
 it reacts by hurting, by creating suffering for oneself.

00:30:37.160 --> 00:30:40.160
 It reacts to the anger again and again and again.

00:30:40.160 --> 00:30:44.160
 It doesn't actually stop the anger, for example,

00:30:44.160 --> 00:30:47.160
 to breathe or the wanting.

00:30:47.160 --> 00:30:50.160
 When monks don't practice meditation,

00:30:50.160 --> 00:30:52.160
 then the wanting just gets bottled up and bottled up,

00:30:52.160 --> 00:31:00.160
 and eventually it spills over and you can't escape it.

00:31:00.160 --> 00:31:03.160
 This is why we have people acknowledge the anger,

00:31:03.160 --> 00:31:07.160
 acknowledge the desire, really just live through it,

00:31:07.160 --> 00:31:11.160
 let it come up, really have to let it come up.

00:31:11.160 --> 00:31:13.160
 It's really the best thing you can do for yourself,

00:31:13.160 --> 00:31:17.160
 to just let it be and watch it and understand it.

00:31:17.160 --> 00:31:20.160
 Because as soon as you do, that moment it's gone,

00:31:20.160 --> 00:31:26.160
 that moment there's mindfulness, very quick to disappear.

00:31:26.160 --> 00:31:28.160
 The problem is it comes back and we become easily

00:31:28.160 --> 00:31:28.160
 discouraged

00:31:28.160 --> 00:31:30.160
 because we think it's having no effect,

00:31:30.160 --> 00:31:32.160
 but that's exactly the effect.

00:31:32.160 --> 00:31:34.160
 It's showing you that it's not under your control.

00:31:34.160 --> 00:31:37.160
 Of course it comes back, it's not yours.

00:31:37.160 --> 00:31:41.160
 It's not like you can control it and poof, it's gone.

00:31:41.160 --> 00:31:43.160
 It comes back because it's a habit.

00:31:43.160 --> 00:31:45.160
 And so you watch it more and more and you see,

00:31:45.160 --> 00:31:48.160
 "This isn't me, this isn't mine.

00:31:48.160 --> 00:31:52.160
 I'm not in control here." And you give it up.

00:31:52.160 --> 00:31:56.160
 You stop encouraging it, you stop chasing after it,

00:31:56.160 --> 00:31:59.160
 you stop giving it power and feeding it,

00:31:59.160 --> 00:32:02.160
 and it slowly wastes away.

00:32:02.160 --> 00:32:03.160
 Because you see that you can control it.

00:32:03.160 --> 00:32:05.160
 You see that, "Yes, when I acknowledge it,

00:32:05.160 --> 00:32:07.160
 it doesn't go, it goes away, but it doesn't,

00:32:07.160 --> 00:32:10.160
 that's not controlling it."

00:32:10.160 --> 00:32:13.160
 It comes back and you have to acknowledge again.

00:32:13.160 --> 00:32:17.910
 You have to clear your mind, stop your mind from building

00:32:17.910 --> 00:32:18.160
 it,

00:32:18.160 --> 00:32:22.160
 creating more of it,

00:32:22.160 --> 00:32:30.160
 until you see that it actually is not worth doing.

00:32:30.160 --> 00:32:33.160
 So these are the four perversions of perception.

00:32:33.160 --> 00:32:36.630
 The commentary says that those are a reason for using the

00:32:36.630 --> 00:32:37.160
 satipatthana.

00:32:37.160 --> 00:32:40.160
 I just thought this was something useful for us to think

00:32:40.160 --> 00:32:40.160
 about

00:32:40.160 --> 00:32:43.160
 when we practice, or before we practice,

00:32:43.160 --> 00:32:46.160
 clearing up what it means to practice.

00:32:46.160 --> 00:32:48.160
 Of course, when we practice we're just mindful,

00:32:48.160 --> 00:32:51.160
 but we just use the acknowledgement.

00:32:51.160 --> 00:32:57.160
 But it helps often clear up doubts to understand

00:32:57.160 --> 00:32:59.160
 why this is such an important thing,

00:32:59.160 --> 00:33:04.160
 and what it's doing for us, why it's useful.

00:33:04.160 --> 00:33:07.160
 So that's the Dhamma for today,

00:33:07.160 --> 00:33:14.160
 just more useful knowledge that we can put into practice.

00:33:14.160 --> 00:33:16.160
 So then we'll practice together.

