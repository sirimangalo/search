1
00:00:00,000 --> 00:00:03,980
 Is a monk allowed to hold jobs if a job does not break any

2
00:00:03,980 --> 00:00:05,000
 of the precepts?

3
00:00:05,000 --> 00:00:12,000
 Well, the first thing is I thought, yes, you can hold a job

4
00:00:12,000 --> 00:00:13,000
 here in the monastery,

5
00:00:13,000 --> 00:00:18,170
 and there's heaps of work, and you have to make sure that

6
00:00:18,170 --> 00:00:19,000
 you don't break any precepts.

7
00:00:19,000 --> 00:00:24,250
 But I guess the question is holding a job out in the world,

8
00:00:24,250 --> 00:00:27,000
 which is not possible.

9
00:00:27,000 --> 00:00:31,000
 It's supposed to hold it closer.

10
00:00:31,000 --> 00:00:35,220
 Yeah, I mean, the question is how far you can take that,

11
00:00:35,220 --> 00:00:37,000
 because everyone here has roles.

12
00:00:37,000 --> 00:00:39,000
 Actually, it's a good question because it helps us.

13
00:00:39,000 --> 00:00:42,000
 It only gives us a chance to talk about everyone.

14
00:00:42,000 --> 00:00:45,000
 Here we've got a camera person, for example.

15
00:00:45,000 --> 00:00:49,000
 Her job is the dedicated camera person.

16
00:00:49,000 --> 00:00:57,360
 This person on the far left is our monastery bookkeeper and

17
00:00:57,360 --> 00:01:00,000
 secretary.

18
00:01:00,000 --> 00:01:05,000
 And Kathy doesn't have a job because she refuses to ordain.

19
00:01:05,000 --> 00:01:13,130
 Yesa is our groundskeeper, so he's going to take over my

20
00:01:13,130 --> 00:01:17,350
 duty of looking and seeing what needs to be done in the

21
00:01:17,350 --> 00:01:18,000
 monastery.

22
00:01:18,000 --> 00:01:21,310
 And hopefully help with arranging workers and so on as best

23
00:01:21,310 --> 00:01:22,000
 you can.

24
00:01:22,000 --> 00:01:25,740
 I mean, neither of us speaks English, so it's a lot of fun

25
00:01:25,740 --> 00:01:29,000
 trying to explain our ideas to them.

26
00:01:29,000 --> 00:01:33,380
 And for them to explain their ideas to us is equally

27
00:01:33,380 --> 00:01:35,000
 entertaining.

28
00:01:35,000 --> 00:01:37,000
 And our camera person is not just a camera person.

29
00:01:37,000 --> 00:01:44,000
 She also does the kitchen manager.

30
00:01:44,000 --> 00:01:48,000
 And Kathy has been helping with the kitchen as well.

31
00:01:48,000 --> 00:01:50,000
 And then Torindu also.

32
00:01:50,000 --> 00:01:56,000
 But anyway, so even the monastics, we have jobs.

33
00:01:56,000 --> 00:02:00,280
 But how far you can take that, whether it means working in

34
00:02:00,280 --> 00:02:02,000
 the monastery,

35
00:02:02,000 --> 00:02:05,000
 that's most likely not what you're referring to.

36
00:02:05,000 --> 00:02:11,720
 You're referring to doing something for lay people, for

37
00:02:11,720 --> 00:02:14,000
 example, doing something not for the Buddha's teaching.

38
00:02:14,000 --> 00:02:21,760
 And I would say the Vinaya may not directly, explicitly dis

39
00:02:21,760 --> 00:02:25,000
allow it, but the Dhamma certainly does.

40
00:02:25,000 --> 00:02:31,150
 And the teachings of the Buddha, even within the Vinaya,

41
00:02:31,150 --> 00:02:35,400
 are quite clear that we're not to engage in any sort of

42
00:02:35,400 --> 00:02:36,000
 worldly pursuits

43
00:02:36,000 --> 00:02:42,000
 or assistance for lay people besides teaching.

44
00:02:42,000 --> 00:02:45,400
 I mean, there are monks who decide that they want to be

45
00:02:45,400 --> 00:02:49,000
 doctors, and so they provide cures for people.

46
00:02:49,000 --> 00:02:52,230
 And this is quite explicitly in the Buddha's teaching, not

47
00:02:52,230 --> 00:02:53,000
 allowed.

48
00:02:53,000 --> 00:02:57,000
 The performance of services other than meditation.

49
00:02:57,000 --> 00:02:59,500
 People will often ask, "Well, but if someone's a good

50
00:02:59,500 --> 00:03:04,000
 doctor, why can't they use their skills?"

51
00:03:04,000 --> 00:03:06,500
 Well, of course they can use their skills, but they should

52
00:03:06,500 --> 00:03:08,000
 be a doctor, not a monk, right?

53
00:03:08,000 --> 00:03:12,300
 You become a monk means you take on a specific life, the

54
00:03:12,300 --> 00:03:16,490
 life of a reckless meditation practitioner and perhaps

55
00:03:16,490 --> 00:03:17,000
 teacher.

56
00:03:17,000 --> 00:03:21,000
 And so you've committed yourself to that.

57
00:03:21,000 --> 00:03:25,470
 And then to use it for something totally outside of the

58
00:03:25,470 --> 00:03:29,610
 realm of the practice and the teaching of Buddhism is

59
00:03:29,610 --> 00:03:32,000
 completely inappropriate.

60
00:03:32,000 --> 00:03:35,000
 I mean, of course this is contentious and controversial.

61
00:03:35,000 --> 00:03:38,000
 There are people who will argue against this.

62
00:03:38,000 --> 00:03:42,640
 But from my point of view, anyway, this is quite clear that

63
00:03:42,640 --> 00:03:46,750
 even doing good things for people, if you want to do good

64
00:03:46,750 --> 00:03:47,000
 things,

65
00:03:47,000 --> 00:03:50,800
 like if you wanted to do whatever job you were asking about

66
00:03:50,800 --> 00:03:54,000
, if you wanted to do that job, then become that person.

67
00:03:54,000 --> 00:03:58,000
 If you want to fight legal courses, become a lawyer.

68
00:03:58,000 --> 00:04:01,000
 If you want to heal people's illnesses, become a doctor.

69
00:04:01,000 --> 00:04:04,000
 If you want to be free from suffering, become a monk.

70
00:04:04,000 --> 00:04:09,000
 Or even to teach other people meditation, become a monk.

71
00:04:09,000 --> 00:04:15,000
 I would say that's the best job that you have.

72
00:04:15,000 --> 00:04:17,860
 Aside from jobs in the monastery supporting the community,

73
00:04:17,860 --> 00:04:20,000
 which is, I think, on a different level.

74
00:04:20,000 --> 00:04:24,040
 In the Bhikkhuni Patimoka, there is even a rule for that,

75
00:04:24,040 --> 00:04:27,000
 that we are not allowed to do chores for lay people.

76
00:04:27,000 --> 00:04:29,000
 I don't think we have an explicit...

77
00:04:29,000 --> 00:04:31,000
 No monks, I don't think we have it.

78
00:04:31,000 --> 00:04:36,160
 But we have it explicitly in the Patimoka, that we are not

79
00:04:36,160 --> 00:04:39,000
 allowed to do chores for lay people.

80
00:04:39,000 --> 00:04:48,170
 And that is to have some... even to do something for a lay

81
00:04:48,170 --> 00:04:54,000
 person to get more food, or to get some benefit out of it.

82
00:04:54,000 --> 00:05:02,590
 So I can certainly say we are not allowed to do any job for

83
00:05:02,590 --> 00:05:08,000
 money, or for food, or something like that.

84
00:05:08,000 --> 00:05:11,870
 Or even, I think the point of the rule is to prevent people

85
00:05:11,870 --> 00:05:13,000
 from doing that.

86
00:05:13,000 --> 00:05:16,690
 We are not allowed to do it at all, because it gives the

87
00:05:16,690 --> 00:05:19,000
 opportunity for corruption.

88
00:05:19,000 --> 00:05:27,230
 People who are only beginning in the monastic life to get

89
00:05:27,230 --> 00:05:30,000
 on the wrong path.

90
00:05:30,000 --> 00:05:34,270
 It is very easy that people say, "Oh, if you do me this

91
00:05:34,270 --> 00:05:37,000
 favor, I give you something."

92
00:05:37,000 --> 00:05:42,000
 Because that is how the world rules, or is ruled.

93
00:05:42,000 --> 00:05:45,000
 And so it becomes a corruption. It is just not allowed.

94
00:05:45,000 --> 00:05:48,000
 We have to be very strict, and we have left the world.

95
00:05:48,000 --> 00:05:52,000
 We are not meant to get back involved in it.

96
00:05:52,000 --> 00:05:56,690
 We have to consider that the monks have left. They are not

97
00:05:56,690 --> 00:06:00,000
 in your circle, your pool of workers.

98
00:06:00,000 --> 00:06:02,630
 You can't ask monks to help you build your house or

99
00:06:02,630 --> 00:06:04,000
 something like that.

100
00:06:04,000 --> 00:06:10,000
 Help you fix your computer. We can do jobs for our family.

101
00:06:10,000 --> 00:06:12,400
 I have helped my father with his computer as a monk, and he

102
00:06:12,400 --> 00:06:15,000
 is the only person I do that for.

103
00:06:15,000 --> 00:06:16,000
 You remember my mother?

104
00:06:16,000 --> 00:06:21,830
 Yes, there is an allowance to help direct families like

105
00:06:21,830 --> 00:06:26,000
 mother and father, and brother and sisters.

106
00:06:26,000 --> 00:06:28,550
 Because you are also allowed to ask them for whatever you

107
00:06:28,550 --> 00:06:29,000
 want.

108
00:06:29,000 --> 00:06:32,000
 There is that kind of thing.

109
00:06:32,000 --> 00:06:33,000
 .

110
00:06:35,000 --> 00:06:36,000
 .

111
00:06:36,000 --> 00:06:37,000
 .

