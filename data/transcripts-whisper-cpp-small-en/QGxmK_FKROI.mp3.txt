 Hello and welcome back to our study of the Dhammatandah.
 Today we continue on with
 verse number 96 which reads as follows
 Santam tasa manan hoti santa vajaj kamah ca samadanya wimut
asa upasantasa
 tathaino which means calm or peaceful is his or is that
 person's mind peaceful
 their speech and their acts who with right knowledge is
 freed
 such a one who is such a one for such a one no such a one
 who is at peace
 themselves so one who has right right who is delivered
 through right knowledge
 who is at peace themselves something like that so talking
 about the three
 doors the three doors of karma the mind or the word speech
 door and the body
 door so the three ways we can perform karma and such a such
 a one who has
 become free is peaceful in all of these this story is one
 of the more heart
 warming stories not a long story the story about a certain
 monk who seems to
 have practiced well learned all of the rules of the monk
hood he was known as
 kosambhi wasi tisa his name was Tisa Tisa was like a common
 name like John
 back then it's a very common name but kosambhi wasi means
 he dwelled at kosambhi
 and he stayed there for the three rains for the three
 months of the rains
 retreat and at the end the lay person who looked at who was
 looking after him
 came to him and and offered him a set of robes and some
 medicines so the kind of
 medicines like sugar or honey or ghee like butter you know
 kind of medicines
 that actually you can only keep for a certain amount of
 time and so the monk
 didn't immediately receive them because monks can only have
 one set of robes and
 they can only keep these medicines for a short time and can
 only use them when
 they're sick and that kind of thing so he says something
 that's actually quite
 interesting from a monastic point of view he says he asks
 what are they in the
 label says oh these are the offering that we give to the
 monk whenever they
 whoever stays with us please accept them and he says I'm
 sorry I have no need of
 need of these because he can't reasonably take them and use
 them and
 he says why can't you take them and he says what he says is
 interesting he
 says that he has no novice to to perform the usual duties
 and I think the meaning
 here is that the novice would take the robes and offer them
 to the elder if
 they're if the need should ever should arise so they were
 using the novices
 it's kind of odd to sort of get around the world rules by
 having a novice carry
 these things for you and I'm not convinced that it's the
 best reading of
 the rules but these stories tend to have those sorts of
 practices in them that
 don't seem exactly to be how it was laid out in them by the
 Buddha but anyway who
 am I to know but that's that's sort of how it went whether
 right or wrong I
 don't know and so the layman says oh well if that's true
 take my son take my son
 with you and have him ordain as a novice and he'll look
 after you and I guess
 carry your sugar and your butter around for you because no
 misses don't have
 those kind of rules that they can't keep this can't keep
 that and so he accepted
 and this his son who was seven years old he gives him over
 to the elder and says
 please accept him as a novice so the other take other takes
 the boy wets his
 hair and teaches him the dajjapancha kamatana the fivefold
 meditation with
 the skin as its fifth meaning Kesa hair on the head Loma
 hair on the body naka
 nails than ta teeth that Joe is the skin Kesa Loma naka
 than that and Joe these
 are what you teach them a new monk it's the tradition
 because it's a basic
 meditation focusing on the parts of the body to help see
 them as they are and
 not be partial towards them or against them useful for
 monks if they find
 themselves getting into thoughts of lust about the body
 lust for the body and as
 soon as the razor it says as soon as the razor touched his
 head the novice became
 an Arhan the boy became an Arhan he was ready he was he was
 clearly someone who
 had been cultivating perfections and been cultivating good
 things for a long time
 because that was all it took just this this click with
 teaching of this
 meditation and the changing the the recognition probably he
 was a monk in his
 past life and so he became an Arhan right away and didn't
 tell the elder the
 elder had no idea because the elder actually being a good
 monk but he was
 but he wasn't enlightened he was just an ordinary world
 thing and so he stayed
 there for another two weeks and then he decided to go back
 to see the Buddha I
 thought well I'll bring this novice to go and see the
 Buddha that'll be a good
 way to introduce him to the the Buddha's Sassan and maybe
 the Buddha can teach
 him something maybe it'll help him on his own breath
 because who knows maybe
 one day this novice will become a good meditator like me
 something like that and
 so on the way they stopped off at this lodging and at a
 monastery but they only
 had one Kutti and because the novice spent all his time
 looking after the
 elder he got a Kutti for the elder and he spent his time
 doing all these duties
 that the novice would have to do is sweeping it out and
 opening windows
 closing windows putting on putting the bed down and so on
 and by the time he
 had taken care of the elders needs it was too late for him
 to get a Kutti for
 himself a hut for himself and so the elder said you know
 you don't have a
 why they said oh don't you have a Kutti for yourself no I'm
 sorry I don't know
 opportunity so the elder said well then fine you just stay
 with me you can just
 stay with me and in the morning well we'll look after it
 would be better
 better to stay with me than elsewhere
 and then the elder goes and lies down on the bed and
 immediately falls asleep
 because he's just an ordinary world thing but the novice
 thinking the novice
 realizes something and that is that this is for the past
 two nights he has also
 spent the night with novices have a rule he's not a full
 monk a novice takes on
 ten precepts so they're allowed to wear the robes but they
 haven't yet become
 monk so they haven't they are not considered to be in the
 inner circle I
 guess you could say but the point is they still don't fit
 in with the monks
 and so to be in too close association can lead to problems
 and and
 difficulties for the monks so they are only so only allowed
 to stay with the
 monks for a maximum of two nights after the third night it
's not that they're not
 allowed to it's that the monk commits an offense if he
 stays with a non-ordained
 person three nights in a row meaning so because the ideas
 monks have to stick to
 themselves have to not get too in too close association
 with people who are
 not keeping the rules because it can drag them down and so
 on and it can
 people who haven't yet been trained as monks can often
 criticize unjustly you
 know well the story went that the monks were snoring or
 something in the monks
 and the lay people started getting critical of them and
 overly critical or
 that kind of thing you know they lose any sense of respect
 so anyway there's
 some sense that they have to be a bit separate and then
 obvious realize this
 and you realize if I stay in here overnight as he told me
 he'll be
 breaking a rule can't have that so the novice sits up sits
 cross-legged and
 comfortably because he's in our hunt just does practice his
 sitting
 meditation all night because the loophole to the rule again
 it's all
 about loopholes it seems is that if he's sitting up it's
 not considered to have
 slept have stayed together overnight don't ask me but that
's somehow helps
 to fix his things and he stayed there all night and the
 elder just snored away
 totally oblivious but the elder did wake up in the morning
 and was actually
 conscientious conscientious enough to think about this and
 he woke up and it
 completely pitched dark still the night still still not a
 full night and so he
 he picks up his he's on the bed and he reaches over and he
 picks up his fan so
 in those times the monks would have a fan to blow driveway
 mosquitoes and to
 cool themselves off in the heat and that kind of thing
 different uses for a fan
 and he he took the fan and he pounded the mat where he knew
 the the novice was
 and he said hey wake up you have to go outside and then the
 novice didn't say
 anything so he threw the he threw the fan at the novice he
 didn't see him he
 didn't know where the novice was he thought the novice was
 lying down so he
 said okay oh you know maybe aim towards his feet or
 something and the novice
 because he was sitting up the fan hit him square in the eye
 and poked his eye
 out destroyed his eye
 and the other had no idea the other had no clue the novice
 knowing that his eye
 was poked out what does he do he says to the elder what did
 you say because I
 guess he was a little distracted he says get out get out it
's time to go out so
 the novice what does he do covers his eye whichever eye it
 was covers his eye
 with one hand and gets up and walks out doesn't say
 anything about it completely
 quiet and on top of that not only does he is he totally
 neutral totally composed
 having just had his eye poked out he goes and does the
 duties of a visiting
 monk so he goes and sweeps the tray it sweeps the pads
 sweeps out the the
 outhouse sets out water for the elder washing he sweeps you
 know he's got one
 hand and so he's sweeping with one hand with the broom
 sweeping out the
 house and putting out water for washing and water for
 drinking and then he even
 goes and gets a tooth a tooth they had these sticks that
 they would use to
 brush their teeth you can still get them in India it's kind
 of neat on the Buddhist
 Circuit you can I got a whole bunch of them when I was
 there last year and so
 he brought he brings a toothpick a toothbrush to the elder
 monk and it's
 kind of dark so the elder doesn't really see but he sees
 the novice come with one
 hand and offer him a toothbrush and apparently another one
 of the customs
 when offering to an elder in order to cultivate respect is
 to offer it with
 two hands so this is protocol between monks and junior
 monks senior monks
 novices and senior monks for lay people they can do as they
 like but there's a
 kind of a protocol in order to keep humility and not get
 arrogant and not so
 we go with someone senior then you offer to them with two
 hands and so the elder
 got kind of miffed at this he said well what kind of a
 novice are you not
 properly trained offering toothbrush to your teacher with
 one hand what kind of
 novice does that he's kind of I did I thought I taught you
 better than that
 novices when rebels are I know full well how to the rules
 that are to be
 performed but my one hand isn't free my one hand isn't
 disengaged what what's
 wrong what's why why can't you use your other hand and then
 the novice has to
 tell them so the novice relates to him from start to finish
 I was sitting up
 because I was sitting up when you threw the fan at me he
 poked my eye out and
 the elder is understandably upset moved shaken by this and
 he bows down he he
 goes down on his knees and says the novice please forgive
 me I'm so sorry
 would please spare me anything be my refuge is what the Pal
i says and he
 crouches down on the ground in front of him and begs him
 for forgiveness and the
 novice what does he do just perfection after perfection
 this is a wonderful way
 of describing the Erland as remember the seven-year-old kid
 isn't is a fully
 enlightened being or an enlightened being not a Buddha but
 an Arant he says
 venerable sir it isn't your fault it was not it was not it
 was not because it was
 not in order to shame you that I spoke I just wanted to let
 you know what
 happened I knew I had to ease your mind but I didn't tell
 you also to ease your
 mind I didn't it's not that I wanted to keep you it from
 you it's that I knew
 you might get upset and so in order to spare you that I
 didn't say anything it's
 not your fault you're not to blame this is the fault of
 some Sahara he says this
 is the fault of the rounds of rebirth you know if you're
 gonna be reborn these
 things are going to happen and he leaves it at that this
 seven-year-old kid can
 you imagine and so the elder is even even further moved by
 that and the
 novice tries to comfort him but he won't be covered he won
't be comforted so he
 takes the novice to the Buddha he picks up the novice's
 bowl and requisites and
 carries them for the novice and brings him to the Buddha
 shaking his head and
 and weeping the whole way and when they get to the Buddha
 that he goes straight
 straight to the Buddha hands and bows down and prepares to
 tell the Buddha and
 the Buddha says oh Biku is everything okay with you what
 kind of how do you
 answer that says I trust you have not suffered any excess
 discomfort and the
 monk says bande I I indeed have lived it quite at quite an
 ease myself but here I
 have this novice with me take a look at this this novice is
 he is beyond anything
 that I've ever seen and the Buddha said what why what has
 he done and the elder
 told the whole story to the Buddha and he said I don't
 understand when but one
 day when when I asked him to pardon when I asked him to
 forgive me he said it's
 not your fault it's the fault of some sarah don't be
 disturbed don't be upset
 he tried to comfort me when here he is with his eye poked
 out holding it with
 one hand offering me a toothbrush and the Buddha says oh
 those who have freed
 themselves from defilements have neither anger nor hatred
 towards anyone on the
 contrary they're in a state of calm their senses their mind
 their speech and
 their body and then he taught this verse his thoughts are
 calm his speech is calm
 santang dasa mananghu the mind of such a person is calm
 speech is calm his deeds
 are calm who has who what sort of person is this one who
 has freed themselves
 with right knowledge the person does that you know someone
 who is tranquil or
 peaceful so what can we learn from this it you know it it
 it sets the sets the
 bar right it holds no doesn't hold any punches this is
 telling us how far we
 plan we intend to go how far we can go how how perfect we
 expect to become
 through through the practice how far we believe it possible
 to go maybe not in
 this life but eventually through the practice our
 understanding is this isn't
 some simple thing where oh yes you've you know you don't
 complain about a
 toothache anymore when you get it have a stomach ache you
're okay or you're okay
 skipping lunch now because you've let go
 it's not like that
 where we're going is to the to the end of suffering where
 one really doesn't
 react really doesn't get upset where one is really and
 truly able to experience
 things as they are so the mind and the speech and the body
 don't become
 tranquil because you don't encounter bad things it's not
 about building up such
 good karma that you never experience bad things it's not
 that kind of invincible
 it's the invincibility of the mind where you have seen
 clearly and the wonderful
 thing is that it just it comes simply from seeing clearly
 which is that the
 point of this verse that true true peace the kind that can
 see you through losing
 an eye or a limb or even your life that kind of peace and
 tranquility is only to
 be found through wisdom and is to be found simply through
 wisdom with no
 other requirement whatsoever because when you see things as
 they are there's
 no reason to get upset there's no benefit to getting upset
 there's no
 benefit to be had from this novice yelling and screaming
 and wailing and
 crying it's reasonable if it would be reasonable if he did
 you think well he's
 only human but somehow this is the idea is that enlightened
 being becomes it
 goes beyond the ordinary state of a human
 able to attain a state of greatness stay in a state of
 profound peace and
 tranquility
 so this is again something for us to emulate but I think
 more here is
 something for us to compare how would you do fair if you
 lost an eye in that
 way sometimes it's good to think about these things it's
 good to imagine the
 situation it gives you a chance to see your own defilements
 to get a taste of
 what you have left to accomplish what is left inside of you
 that will cause you
 suffering because losing an eye doesn't cause you suffering
 it's only when you
 react negatively to it oh that's not what I wanted when you
 had expectations
 for that I wanted to use it for things but this novice had
 no use for the eye
 losing it was like losing a piece of trash
 meaningless of no consequence because his peace was his
 happiness was
 independent of the eye independent of the physical body
 independent of
 anything in the world that's where our practice leads us it
's not like we want
 to lose an eye or it's you know I mean how could you come
 how could you
 criticize this wouldn't it you know wouldn't it be great if
 when the most
 horror of the horrific thing happened to us we can be happy
 as horrific things do
 happen to people and they're horrified by them they're they
 can go crazy because
 of them wouldn't it be great if that weren't the case
 wouldn't it be great if
 we could deal with anything that we wouldn't have to live
 our lives in fear
 protecting our bodies constantly obsessing over them
 worried about them
 worried about wrinkles worried about fat worried about this
 and that well here we
 see how far we are and how far we have to go and we can see
 the need for work
 that we have work to do because we really have a long way
 to go it's not
 enough to say oh look at me I don't I had back pain and now
 I can deal with
 the back pain can you deal with losing an eye and you know
 how far away you are
 from the goal so anyway that's the Dhamma Pada for tonight
 thank you all for
 tuning in keep practicing and wishing
