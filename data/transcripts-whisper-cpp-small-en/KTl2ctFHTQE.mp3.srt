1
00:00:00,000 --> 00:00:07,000
 Hey, good evening everyone.

2
00:00:07,000 --> 00:00:15,760
 So I'm going to try to do more regular broadcasts.

3
00:00:15,760 --> 00:00:20,840
 Here's an attempt at something like a daily broadcast.

4
00:00:20,840 --> 00:00:25,840
 It would be a daily answering of questions, I think.

5
00:00:25,840 --> 00:00:27,200
 Let me know if the sound is no good.

6
00:00:27,200 --> 00:00:32,200
 If you can't hear me, I'm trying this little mic here.

7
00:00:32,200 --> 00:00:36,800
 Maybe it's better sound, I don't know.

8
00:00:36,800 --> 00:00:41,040
 I can see comments, so feel free to comment.

9
00:00:41,040 --> 00:00:43,480
 But when listening to the Dhamma, one thing to keep in mind

10
00:00:43,480 --> 00:00:45,280
 is

11
00:00:45,280 --> 00:00:48,560
 it's not about chatting.

12
00:00:48,560 --> 00:00:52,000
 Chatting, the comment section shouldn't be seen as a sort

13
00:00:52,000 --> 00:00:52,000
 of,

14
00:00:52,000 --> 00:00:54,840
 "Hey, let's chat while he's giving the talk."

15
00:00:54,840 --> 00:00:59,840
 This isn't just because it's the Internet.

16
00:00:59,840 --> 00:01:07,440
 It doesn't mean we should act Internet-ish.

17
00:01:07,440 --> 00:01:11,120
 So these videos are probably for now at least going to be

18
00:01:11,120 --> 00:01:13,640
 answering questions.

19
00:01:13,640 --> 00:01:16,240
 So we've got all these questions on our meditations,

20
00:01:16,240 --> 00:01:21,090
 and I'm not going to be answering questions in the comments

21
00:01:21,090 --> 00:01:21,440
.

22
00:01:21,440 --> 00:01:30,240
 So please don't post comments or questions there.

23
00:01:30,240 --> 00:01:34,290
 But another thing is that last night I talked about the

24
00:01:34,290 --> 00:01:36,240
 idea of having

25
00:01:36,240 --> 00:01:39,690
 sort of a live question where people could call me, and I

26
00:01:39,690 --> 00:01:40,040
 asked,

27
00:01:40,040 --> 00:01:41,440
 "What could go wrong?"

28
00:01:41,440 --> 00:01:44,540
 And sure enough, in the middle of the night, some people

29
00:01:44,540 --> 00:01:45,040
 saw the video

30
00:01:45,040 --> 00:01:50,640
 and said, "Oh, I think I'll call him."

31
00:01:50,640 --> 00:01:54,770
 And so I had people calling me on hangouts in the middle of

32
00:01:54,770 --> 00:01:55,840
 the night,

33
00:01:55,840 --> 00:01:59,640
 which I didn't quite expect, but probably should have.

34
00:01:59,640 --> 00:02:05,440
 So I think we'll go back to the drawing board on that one.

35
00:02:05,440 --> 00:02:09,640
 But tonight's question is about avoidance.

36
00:02:09,640 --> 00:02:15,690
 The question was about avoiding a friend who had betrayed

37
00:02:15,690 --> 00:02:17,440
 the person.

38
00:02:17,440 --> 00:02:23,180
 And their feeling was that they weren't strong enough to

39
00:02:23,180 --> 00:02:24,040
 deal with the person,

40
00:02:24,040 --> 00:02:27,640
 to avoid getting upset.

41
00:02:27,640 --> 00:02:32,030
 And as a result, they wonder whether it's better to avoid

42
00:02:32,030 --> 00:02:33,240
 this person

43
00:02:33,240 --> 00:02:37,440
 while they become stronger, their new meditator.

44
00:02:37,440 --> 00:02:39,040
 So it's a good general question.

45
00:02:39,040 --> 00:02:41,840
 What about avoidance?

46
00:02:41,840 --> 00:02:44,240
 And I've talked about this before.

47
00:02:44,240 --> 00:02:48,040
 It comes in the sabbasva sutta.

48
00:02:48,040 --> 00:02:53,640
 There's a short but useful guideline there.

49
00:02:53,640 --> 00:02:58,240
 It's called pariwajana pahatabhada.

50
00:02:58,240 --> 00:03:01,440
 The defilements are the taints that you get rid of,

51
00:03:01,440 --> 00:03:05,990
 the purification or the purity of mind that comes about

52
00:03:05,990 --> 00:03:08,240
 from avoiding things.

53
00:03:08,240 --> 00:03:15,620
 Because if you didn't avoid them, unwholesome states would

54
00:03:15,620 --> 00:03:16,840
 arise.

55
00:03:16,840 --> 00:03:20,440
 So let's put this in three parts.

56
00:03:20,440 --> 00:03:27,640
 There, what the sabbasva sutta advises you to avoid.

57
00:03:27,640 --> 00:03:33,840
 So there's an avoiding on principle, let's call that.

58
00:03:33,840 --> 00:03:38,470
 And the avoiding on principle, in this case, I would say,

59
00:03:38,470 --> 00:03:40,840
 relates to the fact that the person is evil.

60
00:03:40,840 --> 00:03:42,440
 It is of evil intentions.

61
00:03:42,440 --> 00:03:45,840
 You know this person betrays other people.

62
00:03:45,840 --> 00:03:49,240
 They're the sort of person who is not to be trusted.

63
00:03:49,240 --> 00:03:52,080
 It's not really a condemnation of the person, but it's an

64
00:03:52,080 --> 00:03:55,840
 awareness and an understanding.

65
00:03:55,840 --> 00:04:04,200
 And that might change, but even with an apology, you

66
00:04:04,200 --> 00:04:05,040
 forgive,

67
00:04:05,040 --> 00:04:09,440
 but I think forgiving and forgetting should not always be

68
00:04:09,440 --> 00:04:10,440
 the same.

69
00:04:10,440 --> 00:04:13,040
 People have, we do things based on habits.

70
00:04:13,040 --> 00:04:16,910
 So if a person has betrayed you, I think you're within your

71
00:04:16,910 --> 00:04:17,240
 rights

72
00:04:17,240 --> 00:04:21,450
 and it's generally a good thing to do, to be wary of that

73
00:04:21,450 --> 00:04:22,240
 person.

74
00:04:22,240 --> 00:04:25,710
 Don't take their counsel or take their counsel with a grain

75
00:04:25,710 --> 00:04:26,440
 of salt.

76
00:04:26,440 --> 00:04:29,440
 Don't seek them out.

77
00:04:29,440 --> 00:04:36,840
 And as far as associating with them, be wary and consider

78
00:04:36,840 --> 00:04:38,640
 that being in their presence

79
00:04:38,640 --> 00:04:44,700
 has the potential for getting caught up in their unwholes

80
00:04:44,700 --> 00:04:45,640
omeness.

81
00:04:45,640 --> 00:04:51,820
 And so on principle, avoiding people who, you know, to the

82
00:04:51,820 --> 00:04:54,440
 degree that you are reasonable

83
00:04:54,440 --> 00:04:58,440
 and understanding that they've got bad intentions.

84
00:04:58,440 --> 00:05:04,440
 But that says nothing yet about your reactions to them.

85
00:05:04,440 --> 00:05:11,440
 And so the idea here is that a person's evilness or level

86
00:05:11,440 --> 00:05:15,440
 of evil potential, let's say,

87
00:05:15,440 --> 00:05:23,240
 is a source of fairly strong and understandable reaction.

88
00:05:23,240 --> 00:05:28,610
 So it's, of all things, it's much more likely to give rise

89
00:05:28,610 --> 00:05:30,440
 to unwholesomeness.

90
00:05:30,440 --> 00:05:34,580
 It's on a level different from the unwholesomeness that

91
00:05:34,580 --> 00:05:37,640
 comes from an objective experience.

92
00:05:37,640 --> 00:05:41,640
 Let's say stubbing your toe on a rock, for example,

93
00:05:41,640 --> 00:05:49,860
 or feeling cold, feeling hot, hearing a sound, seeing an

94
00:05:49,860 --> 00:05:55,840
 image or an object and so on.

95
00:05:55,840 --> 00:06:00,840
 So this first avoidance, definitely avoid evil people.

96
00:06:00,840 --> 00:06:04,430
 It doesn't mean you run away from them or you see them and

97
00:06:04,430 --> 00:06:06,840
 you get all freaked out or anything.

98
00:06:06,840 --> 00:06:12,320
 It means you have a rational understanding that you sort of

99
00:06:12,320 --> 00:06:17,040
 incorporate into this rational sense of what's right to do,

100
00:06:17,040 --> 00:06:17,040
 right?

101
00:06:17,040 --> 00:06:19,290
 Because a lot of the questions I get and a lot of the

102
00:06:19,290 --> 00:06:20,040
 questions we have are,

103
00:06:20,040 --> 00:06:24,040
 "What's the right thing to do in a certain situation?"

104
00:06:24,040 --> 00:06:27,040
 And there's no easy answer to that.

105
00:06:27,040 --> 00:06:30,040
 There are a lot of factors that come into play,

106
00:06:30,040 --> 00:06:35,240
 but the main point is don't let your emotions get involved,

107
00:06:35,240 --> 00:06:35,640
 right?

108
00:06:35,640 --> 00:06:37,700
 It shouldn't just be, "I don't like this. I'm going to

109
00:06:37,700 --> 00:06:38,440
 leave."

110
00:06:38,440 --> 00:06:40,840
 Not on this level.

111
00:06:40,840 --> 00:06:43,350
 There's a level in which we do things just because they're

112
00:06:43,350 --> 00:06:46,650
 right to do or we don't do things just because they're

113
00:06:46,650 --> 00:06:48,040
 wrong to do.

114
00:06:48,040 --> 00:06:49,040
 That's the first level.

115
00:06:49,040 --> 00:06:55,090
 So the second level, where we avoid things that we can't

116
00:06:55,090 --> 00:06:56,040
 handle,

117
00:06:56,040 --> 00:07:01,330
 and this is already covered under the first one in this

118
00:07:01,330 --> 00:07:01,840
 case,

119
00:07:01,840 --> 00:07:04,840
 but it's worth pointing out the difference here, right?

120
00:07:04,840 --> 00:07:13,030
 There are qualities about a person that lead us to unwholes

121
00:07:13,030 --> 00:07:14,640
omeness.

122
00:07:14,640 --> 00:07:18,930
 And so in this example, a person who's betrayed you, just

123
00:07:18,930 --> 00:07:20,040
 being in their presence,

124
00:07:20,040 --> 00:07:23,000
 might freak you out or make you upset and make you remember

125
00:07:23,000 --> 00:07:25,240
 all the bad things that they did to you.

126
00:07:25,240 --> 00:07:29,950
 That's not necessarily a reason to avoid, a reason to do

127
00:07:29,950 --> 00:07:30,640
 anything, right?

128
00:07:30,640 --> 00:07:34,110
 To say, "Well, this is not good. I shouldn't be in their

129
00:07:34,110 --> 00:07:37,240
 presence because I get upset just seeing them."

130
00:07:37,240 --> 00:07:40,840
 Because that's in the realm of experience.

131
00:07:40,840 --> 00:07:47,160
 The same sort of thing can arise from being around someone

132
00:07:47,160 --> 00:07:49,440
 who's not doing anything unwholesome,

133
00:07:49,440 --> 00:07:54,800
 but whose behavior or appearance or so on gives rise to un

134
00:07:54,800 --> 00:07:57,240
wholesomeness in you.

135
00:07:57,240 --> 00:07:59,440
 So it could be because they're eating too loud, right?

136
00:07:59,440 --> 00:08:02,590
 I make a clicking noise in my jaw when I eat sometimes, and

137
00:08:02,590 --> 00:08:04,640
 my father has called me out on it.

138
00:08:04,640 --> 00:08:07,640
 He gets upset about it.

139
00:08:07,640 --> 00:08:12,320
 Maybe kids playing or something, or a person's voice is shr

140
00:08:12,320 --> 00:08:13,040
ill.

141
00:08:13,040 --> 00:08:15,240
 Maybe you don't like the way I talk, right?

142
00:08:15,240 --> 00:08:19,070
 Or you don't like the fact that I'm a white person teaching

143
00:08:19,070 --> 00:08:20,440
 Buddhism or so on.

144
00:08:20,440 --> 00:08:23,440
 So many things that we give rise to.

145
00:08:23,440 --> 00:08:25,440
 It's cold out here and I'm shaking.

146
00:08:25,440 --> 00:08:26,840
 Maybe you don't like the fact that I'm shaking.

147
00:08:26,840 --> 00:08:30,340
 Maybe I don't like the fact that my hand is shaking here.

148
00:08:30,340 --> 00:08:34,040
 This phone is shaking because of how cold it is.

149
00:08:34,040 --> 00:08:35,040
 That's just a feeling.

150
00:08:35,040 --> 00:08:36,940
 That's just the body.

151
00:08:36,940 --> 00:08:40,540
 Maybe you don't like it.

152
00:08:40,540 --> 00:08:43,050
 So where does that become something that you should say, "

153
00:08:43,050 --> 00:08:44,740
Well, I can't handle this."

154
00:08:44,740 --> 00:08:50,880
 And I admit that this is my failing, but I shouldn't handle

155
00:08:50,880 --> 00:08:51,340
 it.

156
00:08:51,340 --> 00:08:55,140
 And that's really the crux of the question as it was asked.

157
00:08:55,140 --> 00:09:00,540
 And I think I don't have a perfect, I don't have a well-

158
00:09:00,540 --> 00:09:04,140
based answer here, but a guideline

159
00:09:04,140 --> 00:09:09,840
 in the sort of ballpark area, I would say, is where it

160
00:09:09,840 --> 00:09:14,040
 leads to do a change in your actions.

161
00:09:14,040 --> 00:09:16,880
 So suppose this person has betrayed me and I hear them

162
00:09:16,880 --> 00:09:18,480
 talking to other people, maybe

163
00:09:18,480 --> 00:09:21,160
 just talking, or I just see them sitting there, and it

164
00:09:21,160 --> 00:09:22,800
 makes me want to do something, or at

165
00:09:22,800 --> 00:09:25,520
 least scowl at them.

166
00:09:25,520 --> 00:09:29,720
 And maybe when I talk to them, I just, I know I'm going to

167
00:09:29,720 --> 00:09:33,780
 say mean things or angry things.

168
00:09:33,780 --> 00:09:38,760
 If it leads you to do unwholesomeness, I think definitely

169
00:09:38,760 --> 00:09:41,560
 there's a line that's crossed there

170
00:09:41,560 --> 00:09:45,900
 in where you should say, "That's where I should, in that

171
00:09:45,900 --> 00:09:48,800
 case, I should just avoid the situation."

172
00:09:48,800 --> 00:09:52,620
 I mean, that's where when you're in an argument with

173
00:09:52,620 --> 00:09:55,600
 someone, I think, well, many people say

174
00:09:55,600 --> 00:09:59,180
 this, but I think in Buddhism it applies that if you're

175
00:09:59,180 --> 00:10:01,280
 angry with someone, if you just

176
00:10:01,280 --> 00:10:03,770
 get angry when you're in a conversation with someone, you

177
00:10:03,770 --> 00:10:04,720
 should walk away.

178
00:10:04,720 --> 00:10:08,870
 You should end the conversation, say, "I'm sorry, I have to

179
00:10:08,870 --> 00:10:10,480
 go," and take time to deal

180
00:10:10,480 --> 00:10:12,400
 with the anger.

181
00:10:12,400 --> 00:10:15,030
 Not then to go in your room and fume about how awful that

182
00:10:15,030 --> 00:10:16,600
 person is, but to go into your

183
00:10:16,600 --> 00:10:22,020
 room and consider how awful you are, right?

184
00:10:22,020 --> 00:10:25,360
 How wrong states have arisen in you, how the anger has

185
00:10:25,360 --> 00:10:28,500
 arisen in you, and deal with that.

186
00:10:28,500 --> 00:10:31,750
 Not beating yourself up over it, but deal with it, because

187
00:10:31,750 --> 00:10:33,320
 you can't effectively deal

188
00:10:33,320 --> 00:10:37,420
 with it if you're getting, if you're expressing, if you're

189
00:10:37,420 --> 00:10:40,000
 committing unwholesomeness speech

190
00:10:40,000 --> 00:10:44,320
 or acts.

191
00:10:44,320 --> 00:10:52,030
 But I think for the most part, we should be willing to

192
00:10:52,030 --> 00:10:57,040
 engage with our unwholesome mind

193
00:10:57,040 --> 00:10:59,640
 stains.

194
00:10:59,640 --> 00:11:02,810
 So someone, this person who's betrayed you, you're in their

195
00:11:02,810 --> 00:11:04,240
 presence and it freaks you

196
00:11:04,240 --> 00:11:05,240
 out.

197
00:11:05,240 --> 00:11:10,340
 It makes you scared, worried, anxious, or angry, sad, any

198
00:11:10,340 --> 00:11:12,080
 number of things.

199
00:11:12,080 --> 00:11:15,200
 All of those things are interesting and useful and

200
00:11:15,200 --> 00:11:18,040
 potentially productive in terms of helping

201
00:11:18,040 --> 00:11:23,690
 us understand them and become stronger than them so that

202
00:11:23,690 --> 00:11:27,240
 they don't hurt us in the future.

203
00:11:27,240 --> 00:11:33,590
 And so where we shouldn't avoid, I think, not all the time,

204
00:11:33,590 --> 00:11:36,040
 but we should be open to

205
00:11:36,040 --> 00:11:38,740
 where we're confident that it's just going to be a mental

206
00:11:38,740 --> 00:11:39,320
 state.

207
00:11:39,320 --> 00:11:42,680
 I'm just going to be miserable around this person.

208
00:11:42,680 --> 00:11:44,970
 I'm not going to say anything or I don't have to

209
00:11:44,970 --> 00:11:47,200
 communicate with them, but I'm miserable

210
00:11:47,200 --> 00:11:50,880
 and I'm unhappy, because that's interesting.

211
00:11:50,880 --> 00:11:53,610
 That's something that we can learn about, that we can study

212
00:11:53,610 --> 00:11:54,920
, that we can overcome, and

213
00:11:54,920 --> 00:11:58,040
 we can become free from.

214
00:11:58,040 --> 00:12:03,000
 Some people might even say, "It's a good thing.

215
00:12:03,000 --> 00:12:05,160
 That's a good opportunity."

216
00:12:05,160 --> 00:12:06,880
 I think there's room for that.

217
00:12:06,880 --> 00:12:10,480
 I don't mean you should go out of your way to find things

218
00:12:10,480 --> 00:12:12,320
 that make you upset, but I

219
00:12:12,320 --> 00:12:14,280
 wouldn't go that far.

220
00:12:14,280 --> 00:12:18,120
 But certainly when as a natural part of your day, there's

221
00:12:18,120 --> 00:12:20,720
 no other reason to not be there.

222
00:12:20,720 --> 00:12:24,310
 That you're put in a situation where you have to be in

223
00:12:24,310 --> 00:12:26,560
 proximity to such a person or anything

224
00:12:26,560 --> 00:12:29,480
 like that, any situation.

225
00:12:29,480 --> 00:12:32,600
 The clicking, the person eating their jaws clicking, or the

226
00:12:32,600 --> 00:12:34,000
 kids playing, or the loud

227
00:12:34,000 --> 00:12:36,640
 noises, or even loud music.

228
00:12:36,640 --> 00:12:39,920
 If you have no other reason not to be there, then that

229
00:12:39,920 --> 00:12:42,080
 shouldn't usually be a reason.

230
00:12:42,080 --> 00:12:45,370
 Certainly an environment in which you can and should med

231
00:12:45,370 --> 00:12:47,320
itate, and can and should study

232
00:12:47,320 --> 00:12:50,760
 your reactions, your likes and your dislikes.

233
00:12:50,760 --> 00:12:53,720
 It would be the same if it was someone you were attracted

234
00:12:53,720 --> 00:12:54,080
 to.

235
00:12:54,080 --> 00:12:59,190
 Should I avoid people who are beautiful, who are sexually

236
00:12:59,190 --> 00:13:03,160
 attractive, or physically attractive?

237
00:13:03,160 --> 00:13:10,470
 Do I avoid going near delicious food because I might eat

238
00:13:10,470 --> 00:13:12,120
 too much?

239
00:13:12,120 --> 00:13:13,440
 There's the point.

240
00:13:13,440 --> 00:13:17,200
 If it's something that would cause you to do something,

241
00:13:17,200 --> 00:13:19,520
 then I think there's an important

242
00:13:19,520 --> 00:13:25,160
 point where you say, "I'll just avoid that, because I know

243
00:13:25,160 --> 00:13:27,400
 I'll do bad things."

244
00:13:27,400 --> 00:13:31,460
 But if it's just going to make you really, really crave, or

245
00:13:31,460 --> 00:13:33,280
 if it's just going to make

246
00:13:33,280 --> 00:13:36,710
 you really, really upset, then those are things that

247
00:13:36,710 --> 00:13:38,720
 generally you can deal with.

248
00:13:38,720 --> 00:13:42,460
 If it's extreme, and if you feel like it's extreme, then by

249
00:13:42,460 --> 00:13:44,120
 all means, if you want my

250
00:13:44,120 --> 00:13:47,000
 advice, retreat, step back.

251
00:13:47,000 --> 00:13:48,000
 Absolutely.

252
00:13:48,000 --> 00:13:49,000
 But you don't have to.

253
00:13:49,000 --> 00:13:53,050
 A big part of our practice is learning that we don't have

254
00:13:53,050 --> 00:13:53,580
 to.

255
00:13:53,580 --> 00:13:56,390
 People talk about panic attacks, and anxiety is not

256
00:13:56,390 --> 00:13:58,520
 something you have to run away from.

257
00:13:58,520 --> 00:14:03,730
 A panic attack is just a name for panic that snowballs, and

258
00:14:03,730 --> 00:14:06,320
 mindfulness unwinds that.

259
00:14:06,320 --> 00:14:08,440
 It goes in the opposite direction.

260
00:14:08,440 --> 00:14:16,040
 So you can apply it when you're panicking.

261
00:14:16,040 --> 00:14:18,280
 And I think that's about all I have to say about that.

262
00:14:18,280 --> 00:14:19,800
 That's the answer.

263
00:14:19,800 --> 00:14:22,860
 So there's what you avoid on principle, there's what you

264
00:14:22,860 --> 00:14:26,520
 avoid because it crosses a line where

265
00:14:26,520 --> 00:14:29,230
 it's too much to deal with, and you're either going to do

266
00:14:29,230 --> 00:14:30,920
 or say something bad, or you just

267
00:14:30,920 --> 00:14:32,880
 feel like it's too much.

268
00:14:32,880 --> 00:14:35,480
 And then there's what you can deal with.

269
00:14:35,480 --> 00:14:39,160
 Just because it brings up unwholesomeness in your mind

270
00:14:39,160 --> 00:14:41,720
 doesn't mean it's a bad situation,

271
00:14:41,720 --> 00:14:42,760
 one that you can't meditate in.

272
00:14:42,760 --> 00:14:45,930
 Because unwholesomeness is, to some extent, something you

273
00:14:45,930 --> 00:14:47,560
 can meditate on, learn about

274
00:14:47,560 --> 00:14:50,490
 an overcome and change to the point that you're in the

275
00:14:50,490 --> 00:14:52,840
 situation without the unwholesomeness

276
00:14:52,840 --> 00:14:54,760
 arising.

277
00:14:54,760 --> 00:14:57,880
 So there you go.

278
00:14:57,880 --> 00:14:59,000
 There's the dhamma for tonight.

279
00:14:59,000 --> 00:15:01,320
 Again, I'm not answering questions in the comments.

280
00:15:01,320 --> 00:15:02,320
 I see all your questions.

281
00:15:02,320 --> 00:15:06,640
 And as I said in the beginning, this is not about chatting.

282
00:15:06,640 --> 00:15:07,640
 You're supposed to be listening.

283
00:15:07,640 --> 00:15:13,230
 You're supposed to be sitting there quietly listening to

284
00:15:13,230 --> 00:15:14,680
 the dhamma.

285
00:15:14,680 --> 00:15:16,760
 So keep that in mind.

286
00:15:16,760 --> 00:15:18,480
 Maybe you think, "Ah, this monk doesn't know what he's

287
00:15:18,480 --> 00:15:19,080
 talking about."

288
00:15:19,080 --> 00:15:20,080
 That's fine.

289
00:15:20,080 --> 00:15:22,430
 But I think the best answer there would be just to not come

290
00:15:22,430 --> 00:15:23,040
 and listen.

291
00:15:23,040 --> 00:15:27,160
 But if you think this monk or this teacher has something to

292
00:15:27,160 --> 00:15:29,040
 listen to, the best is to

293
00:15:29,040 --> 00:15:32,760
 not talk too much.

294
00:15:32,760 --> 00:15:35,480
 Peace, everyone.

295
00:15:35,480 --> 00:15:36,480
 Have a good night.

296
00:15:36,480 --> 00:15:37,480
 I don't know.

297
00:15:37,480 --> 00:15:40,050
 I have no promises that it's going to be every night or

298
00:15:40,050 --> 00:15:41,320
 when it's going to be.

299
00:15:41,320 --> 00:15:42,560
 I'd like to do a little earlier.

300
00:15:42,560 --> 00:15:44,840
 I can do it in daylight outside.

301
00:15:44,840 --> 00:15:51,880
 But also European people can have a chance to get involved.

302
00:15:51,880 --> 00:15:53,480
 Although this is all on YouTube, this is going to be

303
00:15:53,480 --> 00:15:53,960
 updated.

304
00:15:53,960 --> 00:15:54,960
 So everyone looking to see it.

