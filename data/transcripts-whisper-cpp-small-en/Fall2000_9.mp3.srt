1
00:00:00,000 --> 00:00:09,240
 Okay, I will be real brief today.

2
00:00:09,240 --> 00:00:16,640
 Okay, now all of you know that concentration is an

3
00:00:16,640 --> 00:00:18,600
 important factor in the practice of

4
00:00:18,600 --> 00:00:19,600
 meditation.

5
00:00:19,600 --> 00:00:27,580
 In the phrase 'ardent, clearly comprehending and mindful',

6
00:00:27,580 --> 00:00:31,240
 the sub-commentary said we

7
00:00:31,240 --> 00:00:36,930
 must take concentration also, because without concentration

8
00:00:36,930 --> 00:00:40,480
 there can be no clear comprehension.

9
00:00:40,480 --> 00:00:44,490
 So concentration is very important in the practice of both

10
00:00:44,490 --> 00:00:48,120
 samatha meditation and vipassana

11
00:00:48,120 --> 00:00:51,040
 meditation.

12
00:00:51,040 --> 00:00:57,880
 So today I will talk a little about concentration.

13
00:00:57,880 --> 00:01:01,360
 So what is concentration?

14
00:01:01,360 --> 00:01:07,400
 Now concentration is the translation of the Pali word samad

15
00:01:07,400 --> 00:01:08,000
hi.

16
00:01:08,000 --> 00:01:13,600
 Now there is another word for concentration and that is ek

17
00:01:13,600 --> 00:01:14,600
agata.

18
00:01:14,600 --> 00:01:17,900
 So both means concentration in English.

19
00:01:17,900 --> 00:01:24,850
 So concentration is a mental factor, like understanding,

20
00:01:24,850 --> 00:01:28,880
 like mindfulness, like effort,

21
00:01:28,880 --> 00:01:31,840
 like attention.

22
00:01:31,840 --> 00:01:35,600
 Concentration is also a mental factor.

23
00:01:35,600 --> 00:01:42,950
 And this is the mental factor that arises with cheda and it

24
00:01:42,950 --> 00:01:45,960
 keeps the cheda and its

25
00:01:45,960 --> 00:01:50,000
 concomitants on the object.

26
00:01:50,000 --> 00:01:56,320
 It doesn't let the consciousness be distracted to other

27
00:01:56,320 --> 00:01:57,720
 objects.

28
00:01:57,720 --> 00:02:04,520
 And also it keeps the mental concomitants coordinated or

29
00:02:04,520 --> 00:02:06,040
 together.

30
00:02:06,040 --> 00:02:10,440
 It does not let the mental states scatter away.

31
00:02:10,440 --> 00:02:16,200
 So this is the function of concentration or samadhi.

32
00:02:16,200 --> 00:02:18,840
 So there are two things.

33
00:02:18,840 --> 00:02:24,600
 Concentration puts the mind or cheda and jtica on the

34
00:02:24,600 --> 00:02:25,880
 object.

35
00:02:25,880 --> 00:02:30,310
 And when it puts the cheda and jtica on the object it does

36
00:02:30,310 --> 00:02:32,800
 not let them to be distracted

37
00:02:32,800 --> 00:02:34,160
 to other objects.

38
00:02:34,160 --> 00:02:36,560
 That is one function.

39
00:02:36,560 --> 00:02:42,400
 And then it keeps the concomitants together.

40
00:02:42,400 --> 00:02:44,780
 It does not let them be scattered.

41
00:02:44,780 --> 00:02:51,000
 So this is the function of concentration or samadhi.

42
00:02:51,000 --> 00:03:02,240
 Etymologically, the word samadhi is composed of sam, aa,

43
00:03:02,240 --> 00:03:04,600
 and d.

44
00:03:04,600 --> 00:03:11,640
 D together with aa means holding or placing, putting.

45
00:03:11,640 --> 00:03:18,600
 And sam here means evenly and correctly.

46
00:03:18,600 --> 00:03:23,800
 So correctly means not letting the concomitants to be

47
00:03:23,800 --> 00:03:27,000
 distracted to other objects.

48
00:03:27,000 --> 00:03:32,000
 And evenly means keeping the mental concomitants together.

49
00:03:32,000 --> 00:03:41,360
 So that is the function of samadhi according to the etym

50
00:03:41,360 --> 00:03:45,120
ological meaning.

51
00:03:45,120 --> 00:03:49,940
 So different mental states have different functions.

52
00:03:49,940 --> 00:03:53,730
 And although they arise together and they work together,

53
00:03:53,730 --> 00:03:57,360
 they do their own functions.

54
00:03:57,360 --> 00:04:01,680
 They will not do the function of the other mental states.

55
00:04:01,680 --> 00:04:07,280
 So their functions are well defined.

56
00:04:07,280 --> 00:04:14,100
 And it is important that when they arise together they do

57
00:04:14,100 --> 00:04:18,160
 their own functions and they do them

58
00:04:18,160 --> 00:04:19,160
 evenly.

59
00:04:19,160 --> 00:04:21,880
 That is in balance.

60
00:04:21,880 --> 00:04:27,870
 So now you know that the five faculties should be balanced

61
00:04:27,870 --> 00:04:31,440
 when you practice meditation.

62
00:04:31,440 --> 00:04:39,510
 And yogis are said to gain concentration when they are able

63
00:04:39,510 --> 00:04:44,480
 to subdue the mental hindrances.

64
00:04:44,480 --> 00:04:47,160
 Now there are five mental hindrances.

65
00:04:47,160 --> 00:04:55,060
 That is desire, ill will or anger, sloth and trouble, rest

66
00:04:55,060 --> 00:04:59,120
lessness and remorse and doubt.

67
00:04:59,120 --> 00:05:04,590
 So these are called mental hindrances and so long as they

68
00:05:04,590 --> 00:05:07,960
 are in the mind the consciousness

69
00:05:07,960 --> 00:05:10,160
 cannot be concentrated.

70
00:05:10,160 --> 00:05:16,750
 So when they are subdued, when they settle down to the

71
00:05:16,750 --> 00:05:20,360
 bottom as it were, then yogi are

72
00:05:20,360 --> 00:05:26,080
 said to get concentration.

73
00:05:26,080 --> 00:05:31,940
 When yogis get concentration their mind will be on the

74
00:05:31,940 --> 00:05:34,880
 object most of the time.

75
00:05:34,880 --> 00:05:42,440
 I said most of the time because there can be very inf

76
00:05:42,440 --> 00:05:47,360
requent wanderings now and then

77
00:05:47,360 --> 00:05:54,740
 but it is very rare and the mind is able to be on the

78
00:05:54,740 --> 00:05:59,280
 object or with the object.

79
00:05:59,280 --> 00:06:03,400
 And Buddha exhorted his disciples to develop concentration.

80
00:06:03,400 --> 00:06:07,040
 Buddha said monks develop concentration.

81
00:06:07,040 --> 00:06:11,820
 Among whose mind is concentrated sees things as they really

82
00:06:11,820 --> 00:06:12,520
 are.

83
00:06:12,520 --> 00:06:18,410
 So to see things as they really are, to see the true nature

84
00:06:18,410 --> 00:06:22,040
 of things, we need concentration.

85
00:06:22,040 --> 00:06:26,730
 And so we develop concentration with the help of

86
00:06:26,730 --> 00:06:29,500
 mindfulness and effort.

87
00:06:29,500 --> 00:06:33,050
 So actually when we practice meditation all these factors

88
00:06:33,050 --> 00:06:34,120
 are involved.

89
00:06:34,120 --> 00:06:38,560
 We make effort, we try to be mindful and when our

90
00:06:38,560 --> 00:06:42,360
 mindfulness becomes strong the mental

91
00:06:42,360 --> 00:06:47,160
 hindrances are subdued and our mind can stay on the object

92
00:06:47,160 --> 00:06:49,560
 or stay with the object most

93
00:06:49,560 --> 00:06:51,060
 of the time.

94
00:06:51,060 --> 00:06:56,790
 So that is the time when we begin to see the object clearly

95
00:06:56,790 --> 00:06:59,680
 and its characteristics and

96
00:06:59,680 --> 00:07:08,400
 so on.

97
00:07:08,400 --> 00:07:13,830
 Generally there are two kinds of concentration mentioned

98
00:07:13,830 --> 00:07:17,120
 and explained in the commentaries

99
00:07:17,120 --> 00:07:25,000
 and they are, let me use the body words first, they are up

100
00:07:25,000 --> 00:07:29,440
ajara samadhi and apna samadhi.

101
00:07:29,440 --> 00:07:36,300
 The first is upajara, u-p-a-c-a-r-a, upajara samadhi and

102
00:07:36,300 --> 00:07:44,120
 the second is apna samadhi, a-p-p-a-n-a.

103
00:07:44,120 --> 00:07:49,780
 The first samadhi is translated as excess concentration or

104
00:07:49,780 --> 00:07:52,760
 neighborhood concentration.

105
00:07:52,760 --> 00:08:06,600
 And the second is translated as absorption concentration.

106
00:08:06,600 --> 00:08:11,130
 Neighborhood concentration is so called because it is in

107
00:08:11,130 --> 00:08:15,220
 the neighborhood of absorption concentration.

108
00:08:15,220 --> 00:08:22,180
 Now absorption concentration means jhanas and in other

109
00:08:22,180 --> 00:08:26,280
 contexts it means magga and pala

110
00:08:26,280 --> 00:08:30,680
 also, I mean path and consciousness also.

111
00:08:30,680 --> 00:08:35,130
 So it is called neighborhood concentration because it is

112
00:08:35,130 --> 00:08:37,360
 close to or in the neighborhood

113
00:08:37,360 --> 00:08:44,400
 of jhana concentration.

114
00:08:44,400 --> 00:08:49,130
 Sometimes it is through this neighborhood concentration

115
00:08:49,130 --> 00:08:52,080
 that the absorption concentration

116
00:08:52,080 --> 00:08:59,230
 is reached, this neighborhood concentration is also called

117
00:08:59,230 --> 00:09:02,400
 excess concentration.

118
00:09:02,400 --> 00:09:07,820
 These two kinds of concentration can be met with in samatha

119
00:09:07,820 --> 00:09:09,360
 meditation.

120
00:09:09,360 --> 00:09:15,830
 So when a person practices samatha meditation, he first

121
00:09:15,830 --> 00:09:19,960
 tries to get excess or neighborhood

122
00:09:19,960 --> 00:09:26,650
 concentration and when again when he is able to subdue the

123
00:09:26,650 --> 00:09:29,640
 mental hindrances then he is

124
00:09:29,640 --> 00:09:34,080
 said to gain the neighborhood concentration.

125
00:09:34,080 --> 00:09:37,830
 So neighborhood concentration is a strong concentration

126
00:09:37,830 --> 00:09:41,400
 which can keep the mental defilement suppressed

127
00:09:41,400 --> 00:09:44,800
 or subdued.

128
00:09:44,800 --> 00:09:49,350
 And when this neighborhood concentration is further

129
00:09:49,350 --> 00:09:52,840
 developed then the jhana concentration

130
00:09:52,840 --> 00:09:54,960
 arises.

131
00:09:54,960 --> 00:10:03,640
 So jhana concentration is stronger than neighborhood

132
00:10:03,640 --> 00:10:05,600
 concentration.

133
00:10:05,600 --> 00:10:10,530
 There are 40 subjects of samatha meditation but not all

134
00:10:10,530 --> 00:10:13,400
 these 40 subjects or the practice

135
00:10:13,400 --> 00:10:18,440
 of these 40 subjects can lead to attainment of jhanas.

136
00:10:18,440 --> 00:10:23,240
 There are some kinds of samatha meditation which can lead

137
00:10:23,240 --> 00:10:26,000
 to neighborhood concentration

138
00:10:26,000 --> 00:10:29,400
 only and not to jhana concentration.

139
00:10:29,400 --> 00:10:34,540
 For example, contemplation on the qualities of the Buddha

140
00:10:34,540 --> 00:10:37,080
 or the Dhamma and the Sangha.

141
00:10:37,080 --> 00:10:42,200
 Now you may practice this kind of meditation, you may

142
00:10:42,200 --> 00:10:45,480
 reflect upon the qualities of the

143
00:10:45,480 --> 00:10:46,370
 Buddha in Pali, you may be saying Iti Pissau, Bhagavad Gav

144
00:10:46,370 --> 00:10:51,480
ah and so on and that can lead

145
00:10:51,480 --> 00:10:55,920
 to neighborhood concentration only.

146
00:10:55,920 --> 00:10:59,760
 It cannot lead to jhana concentration.

147
00:10:59,760 --> 00:11:06,070
 Although it cannot lead to jhana concentration, the

148
00:11:06,070 --> 00:11:10,920
 concentration that a yogi gains through

149
00:11:10,920 --> 00:11:15,000
 that kind of meditation is also called neighborhood

150
00:11:15,000 --> 00:11:16,560
 concentration.

151
00:11:16,560 --> 00:11:21,590
 It is not the real in the neighborhood of jhana because

152
00:11:21,590 --> 00:11:24,740
 there is no jhana for that subject

153
00:11:24,740 --> 00:11:26,140
 of meditation.

154
00:11:26,140 --> 00:11:31,750
 But still that concentration is called neighborhood

155
00:11:31,750 --> 00:11:36,280
 concentration because it is similar to the

156
00:11:36,280 --> 00:11:47,060
 real neighborhood concentration in being able to keep the

157
00:11:47,060 --> 00:11:52,840
 mental hindrances subdued.

158
00:11:52,840 --> 00:12:01,460
 Now when a yogi practices vipassana meditation, he has to

159
00:12:01,460 --> 00:12:06,240
 go through seven stages of purity.

160
00:12:06,240 --> 00:12:12,400
 And he cannot skip any one of these stages of purity.

161
00:12:12,400 --> 00:12:16,600
 And the first stage is called the purity of moral conduct.

162
00:12:16,600 --> 00:12:24,080
 Actually this is not meditation yet, but in preparation for

163
00:12:24,080 --> 00:12:28,000
 meditation a yogi has to purify

164
00:12:28,000 --> 00:12:30,200
 his or her moral conduct.

165
00:12:30,200 --> 00:12:35,290
 So that is the purity of moral conduct is called, it is the

166
00:12:35,290 --> 00:12:37,400
 first stage of purity a

167
00:12:37,400 --> 00:12:39,960
 yogi has to achieve.

168
00:12:39,960 --> 00:12:45,480
 And the second stage is called purity of mind or purity of

169
00:12:45,480 --> 00:12:47,440
 consciousness.

170
00:12:47,440 --> 00:12:53,260
 Purity of consciousness really means purity of samadhi or

171
00:12:53,260 --> 00:12:55,320
 concentration.

172
00:12:55,320 --> 00:13:01,260
 And that purity of mind is explained as consisting of these

173
00:13:01,260 --> 00:13:04,360
 two kinds of concentration, neighborhood

174
00:13:04,360 --> 00:13:08,580
 concentration and the jhana concentration.

175
00:13:08,580 --> 00:13:16,170
 So in most common ways we find only these two mentioned as

176
00:13:16,170 --> 00:13:19,800
 the purity of consciousness

177
00:13:19,800 --> 00:13:26,640
 or purity of mind.

178
00:13:26,640 --> 00:13:40,760
 Now when you practice vipassana meditation you do not get j

179
00:13:40,760 --> 00:13:43,000
hana, you do not get absorption

180
00:13:43,000 --> 00:13:48,800
 concentration.

181
00:13:48,800 --> 00:13:56,520
 Then do you get neighborhood concentration?

182
00:13:56,520 --> 00:14:01,190
 Because a vipassana yogi must also get some kind of

183
00:14:01,190 --> 00:14:05,080
 concentration as you know without concentration

184
00:14:05,080 --> 00:14:11,750
 there can be no clear comprehension, no penetration into

185
00:14:11,750 --> 00:14:14,560
 the nature of things.

186
00:14:14,560 --> 00:14:20,920
 So a vipassana yogi must also get concentration, but his

187
00:14:20,920 --> 00:14:25,800
 concentration is not called neighborhood

188
00:14:25,800 --> 00:14:29,660
 concentration simply because it is not in the neighborhood

189
00:14:29,660 --> 00:14:31,440
 of jhana concentration and

190
00:14:31,440 --> 00:14:35,280
 his concentration is not jhana concentration.

191
00:14:35,280 --> 00:14:41,190
 So there is another kind of concentration that is peculiar

192
00:14:41,190 --> 00:14:44,760
 to the practitioners of vipassana

193
00:14:44,760 --> 00:14:46,760
 meditation.

194
00:14:46,760 --> 00:14:55,220
 And that third kind of meditation is called, I mean

195
00:14:55,220 --> 00:15:04,400
 concentration is called momentary concentration.

196
00:15:04,400 --> 00:15:13,650
 Whenever commentaries explain the purity of mind they do

197
00:15:13,650 --> 00:15:19,840
 not include momentary concentration.

198
00:15:19,840 --> 00:15:27,020
 They always say beauty of mind consists in the neighborhood

199
00:15:27,020 --> 00:15:29,160
 concentration and excess

200
00:15:29,160 --> 00:15:37,810
 concentration, but without the momentary concentration

201
00:15:37,810 --> 00:15:43,360
 there can be no real vipassana knowledge.

202
00:15:43,360 --> 00:15:53,050
 So for the practitioners of vipassana that momentary

203
00:15:53,050 --> 00:15:59,840
 concentration is the purity of mind,

204
00:15:59,840 --> 00:16:03,120
 that the second stage of purity.

205
00:16:03,120 --> 00:16:06,100
 As you know vipassana practitioners cannot get neighborhood

206
00:16:06,100 --> 00:16:07,440
 concentration and excess

207
00:16:07,440 --> 00:16:13,240
 concentration, but they get the momentary concentration.

208
00:16:13,240 --> 00:16:15,640
 You may have got the momentary concentration when you

209
00:16:15,640 --> 00:16:16,280
 practice.

210
00:16:16,280 --> 00:16:26,000
 So for vipassana practitioners the purity of mind means the

211
00:16:26,000 --> 00:16:30,720
 attainment of that momentary

212
00:16:30,720 --> 00:16:31,720
 concentration.

213
00:16:31,720 --> 00:16:34,950
 It is said in the books that without momentary

214
00:16:34,950 --> 00:16:38,120
 concentration there can be no vipassana at

215
00:16:38,120 --> 00:16:40,920
 all.

216
00:16:40,920 --> 00:16:44,790
 No momentary concentration is not called excess or

217
00:16:44,790 --> 00:16:47,320
 neighborhood concentration.

218
00:16:47,320 --> 00:16:52,920
 It is very similar to neighborhood concentration.

219
00:16:52,920 --> 00:17:02,370
 Now it can be unmoved or it can stand firmly against the

220
00:17:02,370 --> 00:17:06,360
 opposition or mental hindrances

221
00:17:06,360 --> 00:17:14,030
 and also it can keep the mind firmly on the object as can

222
00:17:14,030 --> 00:17:18,480
 neighborhood concentration.

223
00:17:18,480 --> 00:17:23,220
 So although it is not called neighborhood concentration it

224
00:17:23,220 --> 00:17:26,240
 is almost the same as neighborhood

225
00:17:26,240 --> 00:17:30,460
 concentration.

226
00:17:30,460 --> 00:17:38,910
 But there are people who take the explanation given in the

227
00:17:38,910 --> 00:17:43,880
 commentaries literally and then

228
00:17:43,880 --> 00:17:49,960
 they do not agree with our saying that momentary

229
00:17:49,960 --> 00:17:56,440
 concentration is the purity of mind for vipassana

230
00:17:56,440 --> 00:17:59,890
 practitioners because they will always point to the

231
00:17:59,890 --> 00:18:02,520
 explanation given in the commentaries

232
00:18:02,520 --> 00:18:08,580
 that only the neighborhood concentration and jhana

233
00:18:08,580 --> 00:18:13,240
 concentration are mentioned as purity

234
00:18:13,240 --> 00:18:18,600
 of mind.

235
00:18:18,600 --> 00:18:22,470
 In order to understand this we need to understand that

236
00:18:22,470 --> 00:18:24,800
 there are two kinds of yogis.

237
00:18:24,800 --> 00:18:32,230
 One is who begins with samatha meditation and gets

238
00:18:32,230 --> 00:18:37,760
 absorption concentration first and

239
00:18:37,760 --> 00:18:43,280
 then later he would take the neighborhood concentration or

240
00:18:43,280 --> 00:18:45,800
 jhana concentration as the

241
00:18:45,800 --> 00:18:49,840
 object of his vipassana meditation.

242
00:18:49,840 --> 00:18:56,280
 Such a yogi is called a yogi who has samatha meditation as

243
00:18:56,280 --> 00:18:58,200
 his vehicle.

244
00:18:58,200 --> 00:19:02,840
 So if a samatha, let us say samatha practitioner, so a sam

245
00:19:02,840 --> 00:19:05,240
atha practitioner is one who first

246
00:19:05,240 --> 00:19:12,020
 practices samatha meditation and then practices vipassana

247
00:19:12,020 --> 00:19:13,840
 meditation.

248
00:19:13,840 --> 00:19:22,040
 And during the time of the Buddha most disciples went this

249
00:19:22,040 --> 00:19:23,040
 way.

250
00:19:23,040 --> 00:19:27,040
 First they practiced the jhana, the practice samatha

251
00:19:27,040 --> 00:19:29,840
 meditation to get jhana and then they

252
00:19:29,840 --> 00:19:32,820
 changed to vipassana meditation.

253
00:19:32,820 --> 00:19:42,480
 And the commentaries had such kind of yogis in mind when

254
00:19:42,480 --> 00:19:47,560
 they described the purity of

255
00:19:47,560 --> 00:19:51,880
 mind as consisting of two concentrations.

256
00:19:51,880 --> 00:20:00,770
 Because in Vysottimaga also, now Vysottimaga is written for

257
00:20:00,770 --> 00:20:01,040
 a person who has samatha as

258
00:20:01,040 --> 00:20:06,210
 a vehicle, not for pure vipassana practitioner that we must

259
00:20:06,210 --> 00:20:07,640
 understand.

260
00:20:07,640 --> 00:20:12,760
 So when we are talking about a person who has practiced

261
00:20:12,760 --> 00:20:15,680
 first samatha meditation and

262
00:20:15,680 --> 00:20:22,000
 then goes over to vipassana, we will just say that purity

263
00:20:22,000 --> 00:20:26,520
 of mind means these two concentrations.

264
00:20:26,520 --> 00:20:36,130
 Although momentary concentration is not mentioned as

265
00:20:36,130 --> 00:20:42,240
 belonging to purity of mind, this momentary

266
00:20:42,240 --> 00:20:48,230
 concentration is mentioned in the commentaries and sub-

267
00:20:48,230 --> 00:20:50,160
commentaries.

268
00:20:50,160 --> 00:20:55,830
 And as I said before, not just one, the sub-commentaries

269
00:20:55,830 --> 00:21:00,240
 said that without momentary concentration

270
00:21:00,240 --> 00:21:03,660
 there can be no vipassana.

271
00:21:03,660 --> 00:21:09,840
 So Mahasisara taught that we must take the momentary

272
00:21:09,840 --> 00:21:14,160
 concentration as the purity of mind

273
00:21:14,160 --> 00:21:19,960
 for vipassana yogis.

274
00:21:19,960 --> 00:21:27,320
 The pure vipassana yogi is one who does not practice sam

275
00:21:27,320 --> 00:21:31,680
atha meditation at all, but who

276
00:21:31,680 --> 00:21:38,160
 begins his vipassana meditation right at the outset.

277
00:21:38,160 --> 00:21:43,170
 So such a yogi who does not practice samatha meditation,

278
00:21:43,170 --> 00:21:46,160
 but practices vipassana, there

279
00:21:46,160 --> 00:21:53,690
 can be no excess concentration and absorption concentration

280
00:21:53,690 --> 00:21:54,040
.

281
00:21:54,040 --> 00:21:59,300
 But as you know, he must have some kind of concentration so

282
00:21:59,300 --> 00:22:01,920
 that he penetrates into the

283
00:22:01,920 --> 00:22:06,970
 nature of things and that concentration is the momentary

284
00:22:06,970 --> 00:22:09,560
 concentration mentioned in the

285
00:22:09,560 --> 00:22:13,440
 commentaries.

286
00:22:13,440 --> 00:22:21,370
 So we were always in need of a textual evidence that moment

287
00:22:21,370 --> 00:22:26,560
ary concentration is also the purity

288
00:22:26,560 --> 00:22:28,280
 of mind.

289
00:22:28,280 --> 00:22:35,540
 So I always look for this when I read books and fortunately

290
00:22:35,540 --> 00:22:39,200
 about a year ago I found such

291
00:22:39,200 --> 00:22:40,200
 a statement.

292
00:22:40,200 --> 00:22:46,920
 So it is in a commentary called "Sara Sankaha".

293
00:22:46,920 --> 00:22:50,730
 Although it is later than the Venerable Buddhaghosa's

294
00:22:50,730 --> 00:22:54,000
 commentaries, actually it is a compilation

295
00:22:54,000 --> 00:23:00,240
 of the explanations given in these commentaries.

296
00:23:00,240 --> 00:23:11,430
 So there to my joy I found that the momentary concentration

297
00:23:11,430 --> 00:23:17,240
 is explicitly included in the

298
00:23:17,240 --> 00:23:20,840
 purity of mind along with the other two kinds of

299
00:23:20,840 --> 00:23:22,380
 concentration.

300
00:23:22,380 --> 00:23:30,530
 So we can point to that commentary as a textual evidence

301
00:23:30,530 --> 00:23:35,760
 that the momentary concentration

302
00:23:35,760 --> 00:23:44,350
 constitutes purity of mind or the second stage of purity

303
00:23:44,350 --> 00:23:49,000
 for vipassana practitioners.

304
00:23:49,000 --> 00:23:59,400
 And also from the yogi's own experience they can attest to

305
00:23:59,400 --> 00:24:04,320
 it that the momentary concentration

306
00:24:04,320 --> 00:24:08,360
 is the purity of mind in practice of vipassana.

307
00:24:08,360 --> 00:24:13,580
 Now every vipassana yogi must have obtained the momentary

308
00:24:13,580 --> 00:24:15,360
 concentration.

309
00:24:15,360 --> 00:24:18,300
 If they do not get momentary concentration they will not

310
00:24:18,300 --> 00:24:20,120
 see the arising and disappearing

311
00:24:20,120 --> 00:24:23,450
 of objects, they will not see the impermanent suffering and

312
00:24:23,450 --> 00:24:25,120
 non-soul nature of things.

313
00:24:25,120 --> 00:24:29,400
 So since they are seeing these characteristics, they are

314
00:24:29,400 --> 00:24:32,000
 seeing the objects clearly and the

315
00:24:32,000 --> 00:24:37,480
 arising and disappearing of the objects and so on, that

316
00:24:37,480 --> 00:24:40,640
 means they have this momentary

317
00:24:40,640 --> 00:24:42,480
 concentration.

318
00:24:42,480 --> 00:24:47,040
 So through this momentary concentration they are able to

319
00:24:47,040 --> 00:24:49,400
 see the true nature of things

320
00:24:49,400 --> 00:24:54,660
 and so the momentary concentration is the purity of mind or

321
00:24:54,660 --> 00:24:57,120
 second stage of purity for

322
00:24:57,120 --> 00:25:01,880
 vipassana practitioners.

323
00:25:01,880 --> 00:25:09,100
 So even if we do not point to the books or read the books

324
00:25:09,100 --> 00:25:11,680
 through our own experience,

325
00:25:11,680 --> 00:25:19,120
 through the experience of yogis, we can prove that there is

326
00:25:19,120 --> 00:25:23,080
 a kind of concentration gained

327
00:25:23,080 --> 00:25:27,130
 by vipassana yogis and that concentration is not

328
00:25:27,130 --> 00:25:30,680
 neighborhood concentration, not jhana

329
00:25:30,680 --> 00:25:32,120
 concentration.

330
00:25:32,120 --> 00:25:35,300
 So if it is not neighborhood concentration and not jhana

331
00:25:35,300 --> 00:25:36,960
 concentration but a kind of

332
00:25:36,960 --> 00:25:41,700
 concentration then it must be none other than the momentary

333
00:25:41,700 --> 00:25:43,280
 concentration.

334
00:25:43,280 --> 00:25:47,970
 So momentary concentration is important for vipassana yogis

335
00:25:47,970 --> 00:25:50,280
 and we try to attain momentary

336
00:25:50,280 --> 00:25:54,280
 concentration by paying attention to the object at the

337
00:25:54,280 --> 00:25:56,880
 present moment by making effort to

338
00:25:56,880 --> 00:26:03,800
 be mindful or to develop mindfulness.

339
00:26:03,800 --> 00:26:08,950
 So would you say that in our practice concentration is

340
00:26:08,950 --> 00:26:12,920
 missing or in our practice there are only

341
00:26:12,920 --> 00:26:19,200
 seven factors of path and not right concentration?

342
00:26:19,200 --> 00:26:27,000
 I would leave you do.

343
00:26:27,000 --> 00:26:35,880
 There is one more difficulty.

344
00:26:35,880 --> 00:26:42,410
 Whenever right concentration is explained, the text always

345
00:26:42,410 --> 00:26:46,240
 says right concentration consists

346
00:26:46,240 --> 00:26:49,120
 in four jhanas.

347
00:26:49,120 --> 00:26:54,120
 No momentary concentration.

348
00:26:54,120 --> 00:26:57,040
 Always they say four jhanas, four jhanas.

349
00:26:57,040 --> 00:27:02,290
 So there are people who think that you must have four jhan

350
00:27:02,290 --> 00:27:05,200
as before you practice vipassana

351
00:27:05,200 --> 00:27:09,180
 meditation and so this insists on you practicing samadha

352
00:27:09,180 --> 00:27:11,600
 meditation to get four jhanas and

353
00:27:11,600 --> 00:27:16,520
 only after that you are to practice vipassana meditation.

354
00:27:16,520 --> 00:27:21,140
 But the way of vipassana, pure vipassana is mentioned in

355
00:27:21,140 --> 00:27:29,880
 the Bhattisambhita Maga which

356
00:27:29,880 --> 00:27:34,370
 is an authoritative text in the Theravada Buddhism and

357
00:27:34,370 --> 00:27:37,600
 following that in the commentaries.

358
00:27:37,600 --> 00:27:43,280
 So in the commentaries it is said that a person does not

359
00:27:43,280 --> 00:27:47,520
 practice samadha at all but he practices

360
00:27:47,520 --> 00:27:55,800
 vipassana and then through vipassana he gains enlightenment

361
00:27:55,800 --> 00:27:55,920
.

362
00:27:55,920 --> 00:28:04,050
 If he gains enlightenment through vipassana and he has no j

363
00:28:04,050 --> 00:28:07,960
hana there must be some kind

364
00:28:07,960 --> 00:28:13,080
 of concentration and that is the momentary concentration.

365
00:28:13,080 --> 00:28:18,870
 So in my opinion I will take that statement in the text

366
00:28:18,870 --> 00:28:22,360
 explaining that four jhanas,

367
00:28:22,360 --> 00:28:30,860
 the right concentration consists of four jhanas to be non-

368
00:28:30,860 --> 00:28:33,200
exhaustive.

369
00:28:33,200 --> 00:28:40,140
 So we can add some more to this.

370
00:28:40,140 --> 00:28:46,100
 So we can say that the momentary concentration is also

371
00:28:46,100 --> 00:28:50,160
 right concentration although it is

372
00:28:50,160 --> 00:28:57,960
 not explicitly mentioned as right concentration in the text

373
00:28:57,960 --> 00:29:01,360
 since it is through momentary

374
00:29:01,360 --> 00:29:08,080
 concentration that we come to again in vipassana knowledge.

375
00:29:08,080 --> 00:29:19,610
 We must include the momentary concentration in the right

376
00:29:19,610 --> 00:29:22,800
 concentration.

377
00:29:22,800 --> 00:29:28,390
 So I told you this for your information but when you are

378
00:29:28,390 --> 00:29:31,640
 practicing don't think of anything

379
00:29:31,640 --> 00:29:32,640
 like this.

380
00:29:32,640 --> 00:29:37,890
 Just practice, be mindful of the object, don't miss to be

381
00:29:37,890 --> 00:29:40,680
 mindful of the object at the present

382
00:29:40,680 --> 00:29:45,040
 moment so that is the most important.

383
00:29:45,040 --> 00:29:49,800
 If you are thinking about this then you will be sidetracked

384
00:29:49,800 --> 00:29:50,160
.

385
00:29:50,160 --> 00:29:53,640
 Your mind will not be on the object and so you are out of

386
00:29:53,640 --> 00:29:54,720
 meditation.

387
00:29:54,720 --> 00:30:00,180
 So when you are practicing you don't go to these spec

388
00:30:00,180 --> 00:30:03,520
ulations of thinking but just pay

389
00:30:03,520 --> 00:30:14,440
 attention to the object at the moment.

390
00:30:14,440 --> 00:30:28,520
 So today we end our Thanksgiving retreat.

391
00:30:28,520 --> 00:30:54,520
 With

392
00:30:54,520 --> 00:31:10,760
 this

393
00:31:10,760 --> 00:31:25,360
 time

394
00:31:25,360 --> 00:31:45,520
 we

395
00:31:45,520 --> 00:32:11,760
 can

396
00:32:11,760 --> 00:32:34,000
 be

397
00:32:34,000 --> 00:32:49,880
 at

398
00:32:49,880 --> 00:33:15,960
 the

399
00:33:15,960 --> 00:33:40,040
 at

400
00:33:40,040 --> 00:34:04,120
 the

401
00:34:04,120 --> 00:34:20,120
 at

402
00:34:20,120 --> 00:34:46,200
 the

403
00:34:46,200 --> 00:35:04,280
 go

404
00:35:04,280 --> 00:35:26,360
 to

405
00:35:26,360 --> 00:35:48,440
 the

406
00:35:48,440 --> 00:36:10,520
 you

407
00:36:10,520 --> 00:36:32,600
 to

408
00:36:32,600 --> 00:36:54,680
 to

409
00:36:54,680 --> 00:37:16,760
 to

410
00:37:16,760 --> 00:37:38,840
 to

411
00:37:38,840 --> 00:37:50,920
 to

412
00:37:50,920 --> 00:38:13,000
 to

413
00:38:13,000 --> 00:38:25,080
 to

414
00:38:25,080 --> 00:38:47,160
 to

415
00:38:47,160 --> 00:38:59,240
 to

416
00:38:59,240 --> 00:39:09,240
 to

417
00:39:09,240 --> 00:39:21,320
 to

418
00:39:21,320 --> 00:39:31,320
 to

419
00:39:31,320 --> 00:39:43,400
 to

420
00:39:43,400 --> 00:40:05,480
 to

421
00:40:05,480 --> 00:40:25,560
 to

422
00:40:25,560 --> 00:40:37,640
 to

423
00:40:37,640 --> 00:40:59,720
 to

424
00:40:59,720 --> 00:41:19,800
 to

425
00:41:19,800 --> 00:41:39,880
 to

426
00:41:39,880 --> 00:42:01,960
 to

427
00:42:01,960 --> 00:42:22,040
 to

428
00:42:22,040 --> 00:42:42,120
 to

429
00:42:42,120 --> 00:43:04,200
 to

430
00:43:04,200 --> 00:43:24,280
 to

431
00:43:24,280 --> 00:43:36,360
 to

432
00:43:44,360 --> 00:44:04,440
 to

433
00:44:04,440 --> 00:44:24,520
 to

434
00:44:24,520 --> 00:44:44,600
 to

435
00:44:44,600 --> 00:44:56,680
 to

436
00:44:56,680 --> 00:45:04,680
 to

437
00:45:04,680 --> 00:45:16,760
 to

438
00:45:16,760 --> 00:45:26,760
 to

439
00:45:26,760 --> 00:45:38,840
 to

440
00:45:38,840 --> 00:45:48,840
 to

441
00:45:48,840 --> 00:46:00,920
 to

442
00:46:00,920 --> 00:46:10,920
 to

443
00:46:10,920 --> 00:46:23,000
 to

444
00:46:23,000 --> 00:46:33,000
 to

445
00:46:33,000 --> 00:46:45,080
 to

446
00:46:45,080 --> 00:46:53,080
 to

447
00:46:53,080 --> 00:47:05,160
 to

448
00:47:05,160 --> 00:47:13,160
 to

449
00:47:13,160 --> 00:47:25,240
 to

450
00:47:25,240 --> 00:47:35,240
 to

451
00:47:35,240 --> 00:47:47,320
 to

452
00:47:47,320 --> 00:47:57,320
 to

453
00:47:57,320 --> 00:48:09,400
 to

454
00:48:09,400 --> 00:48:19,400
 to

455
00:48:19,400 --> 00:48:31,480
 to

456
00:48:31,480 --> 00:48:41,480
 to

457
00:48:41,480 --> 00:48:53,560
 to

458
00:48:53,560 --> 00:49:03,560
 to

459
00:49:03,560 --> 00:49:15,640
 to

460
00:49:15,640 --> 00:49:25,640
 to

461
00:49:25,640 --> 00:49:37,720
 to

462
00:49:37,720 --> 00:49:47,720
 to

463
00:49:47,720 --> 00:49:59,800
 to

464
00:49:59,800 --> 00:50:09,800
 to

465
00:50:09,800 --> 00:50:21,880
 to

466
00:50:21,880 --> 00:50:31,880
 to

467
00:50:31,880 --> 00:50:43,960
 to

468
00:50:43,960 --> 00:50:53,960
 to

469
00:50:53,960 --> 00:51:06,040
 to

470
00:51:06,040 --> 00:51:16,040
 to

471
00:51:16,040 --> 00:51:28,120
 to

472
00:51:28,120 --> 00:51:38,120
 to

473
00:51:38,120 --> 00:51:50,200
 to

474
00:51:50,200 --> 00:52:00,200
 to

475
00:52:00,200 --> 00:52:12,280
 to

476
00:52:12,280 --> 00:52:22,280
 to

477
00:52:22,280 --> 00:52:34,360
 to

478
00:52:34,360 --> 00:52:44,360
 to

479
00:52:44,360 --> 00:52:56,440
 to

480
00:52:56,440 --> 00:53:04,440
 to to

481
00:53:04,440 --> 00:53:16,520
 to

482
00:53:16,520 --> 00:53:26,520
 to to

483
00:53:26,520 --> 00:53:38,600
 to to

484
00:53:38,600 --> 00:53:46,600
 to to

485
00:53:46,600 --> 00:53:58,680
 to to

486
00:53:58,680 --> 00:54:06,680
 to to to

487
00:54:27,680 --> 00:54:39,760
 to to

488
00:54:39,760 --> 00:54:49,760
 to to to

489
00:54:51,760 --> 00:55:03,840
 to to to

490
00:55:03,840 --> 00:55:05,840
 to to

491
00:55:05,840 --> 00:55:07,840
 to to

492
00:55:07,840 --> 00:55:09,840
 to to

493
00:55:09,840 --> 00:55:11,840
 to

494
00:55:11,840 --> 00:55:13,840
 to

495
00:55:13,840 --> 00:55:15,840
 to

496
00:55:15,840 --> 00:55:17,840
 to

497
00:55:17,840 --> 00:55:19,840
 to

498
00:55:19,840 --> 00:55:21,840
 to to

499
00:55:21,840 --> 00:55:23,840
 to to

500
00:55:23,840 --> 00:55:25,840
 to to

501
00:55:25,840 --> 00:55:27,840
 to to

502
00:55:27,840 --> 00:55:29,840
 to

503
00:55:29,840 --> 00:55:31,840
 to to

504
00:55:31,840 --> 00:55:33,840
 to

505
00:55:33,840 --> 00:55:35,840
 to

506
00:55:35,840 --> 00:55:37,840
 to

507
00:55:37,840 --> 00:55:39,840
 to

508
00:55:39,840 --> 00:55:41,840
 to

509
00:55:41,840 --> 00:55:43,840
 to

510
00:55:43,840 --> 00:55:45,840
 to

511
00:55:45,840 --> 00:55:47,840
 to

512
00:55:47,840 --> 00:55:49,840
 to to

513
00:55:49,840 --> 00:55:51,840
 to to

514
00:55:51,840 --> 00:55:53,840
 to

515
00:55:53,840 --> 00:55:55,840
 to

516
00:55:55,840 --> 00:55:57,840
 to

517
00:55:57,840 --> 00:55:59,840
 to

518
00:55:59,840 --> 00:56:01,840
 to

519
00:56:01,840 --> 00:56:03,840
 to

520
00:56:03,840 --> 00:56:05,840
 to

521
00:56:05,840 --> 00:56:07,840
 to

522
00:56:07,840 --> 00:56:09,840
 to

523
00:56:09,840 --> 00:56:11,840
 to

524
00:56:11,840 --> 00:56:13,840
 to

525
00:56:13,840 --> 00:56:15,840
 to

526
00:56:15,840 --> 00:56:17,840
 to to to

527
00:56:17,840 --> 00:56:19,840
 to to to

528
00:56:19,840 --> 00:56:21,840
 to to

529
00:56:21,840 --> 00:56:23,840
 to to

530
00:56:23,840 --> 00:56:25,840
 to to

531
00:56:25,840 --> 00:56:27,840
 to to to

532
00:56:27,840 --> 00:56:29,840
 to to to

533
00:56:29,840 --> 00:56:31,840
 to to to

534
00:57:48,840 --> 00:57:50,840
 to to to to

