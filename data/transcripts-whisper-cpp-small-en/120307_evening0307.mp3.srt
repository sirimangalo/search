1
00:00:00,000 --> 00:00:23,920
 So tonight is the full moon.

2
00:00:23,920 --> 00:00:30,920
 And on the one hand you want to say it's just another day.

3
00:00:30,920 --> 00:00:36,920
 As we have in the Pali saying,

4
00:00:36,920 --> 00:00:43,920
 Sunakatang,

5
00:00:43,920 --> 00:00:49,920
 Mohutitang, Sukhanosh, Sumahutto,

6
00:00:49,920 --> 00:00:54,920
 Chah, Suyutang, Brahmacharya,

7
00:00:54,920 --> 00:01:00,920
 Supatakina,

8
00:01:00,920 --> 00:01:06,920
 Kamaan, Kamaan, Patakina, Patakina, Manokam,

9
00:01:06,920 --> 00:01:12,920
 Muniti, Patakini.

10
00:01:12,920 --> 00:01:18,920
 Whenever we live, whatever moment,

11
00:01:18,920 --> 00:01:31,920
 we are living the holy life or the spiritual life.

12
00:01:31,920 --> 00:01:37,660
 And we perform wholesome deeds of thought, wholesome deeds

13
00:01:37,660 --> 00:01:43,920
 of speech, wholesome deeds of body.

14
00:01:43,920 --> 00:01:56,920
 This is an auspicious day, this is an auspicious,

15
00:01:56,920 --> 00:02:04,270
 auspicious day, auspicious month, auspicious day, ausp

16
00:02:04,270 --> 00:02:05,920
icious moment.

17
00:02:05,920 --> 00:02:15,920
 This is an auspicious instant, that instant is auspicious.

18
00:02:15,920 --> 00:02:24,270
 We remember this and we remind ourselves that it's not the

19
00:02:24,270 --> 00:02:28,920
 time or the place or the environment,

20
00:02:28,920 --> 00:02:36,240
 but it's the actions and the speech and the deeds, the

21
00:02:36,240 --> 00:02:39,920
 actions, speech and thoughts that we perform.

22
00:02:39,920 --> 00:02:44,680
 This is what makes something auspicious, this is what makes

23
00:02:44,680 --> 00:02:46,920
 something special.

24
00:02:46,920 --> 00:03:08,920
 [silence]

25
00:03:08,920 --> 00:03:19,920
 But on the other hand, you do kind of have to,

26
00:03:19,920 --> 00:03:32,670
 have to admit that this time and this place and this

27
00:03:32,670 --> 00:03:40,920
 occasion with this environment,

28
00:03:40,920 --> 00:03:57,200
 that we're very lucky to be here and this is a very ausp

29
00:03:57,200 --> 00:04:03,920
icious time for us.

30
00:04:03,920 --> 00:04:10,380
 My teacher, when New Year came around, next month is the

31
00:04:10,380 --> 00:04:11,920
 Thai New Year.

32
00:04:11,920 --> 00:04:17,920
 And the whole country will go crazy, literally.

33
00:04:17,920 --> 00:04:22,920
 I don't know the whole country, but many parts of it.

34
00:04:22,920 --> 00:04:28,500
 We take the bus and see people getting in fist fights on

35
00:04:28,500 --> 00:04:34,920
 the street, walking drunk in the middle of traffic,

36
00:04:34,920 --> 00:04:39,020
 throwing water and ice cubes and all sorts of things at

37
00:04:39,020 --> 00:04:43,500
 each other and cars as they went by and motorcycles as they

38
00:04:43,500 --> 00:04:45,920
 went by.

39
00:04:45,920 --> 00:04:59,920
 I always used to tell my students about this in Thailand,

40
00:04:59,920 --> 00:05:11,920
 how they, when people talk about having a Happy New Year

41
00:05:11,920 --> 00:05:15,920
 and they're always wishing Happy New Year to each other.

42
00:05:15,920 --> 00:05:20,540
 But in Thailand anyway, and I think in other countries it's

43
00:05:20,540 --> 00:05:22,920
 very much the same.

44
00:05:22,920 --> 00:05:29,920
 December 31st is the most dangerous day of the year.

45
00:05:29,920 --> 00:05:41,820
 Most fatalities of the whole year, most unnatural deaths in

46
00:05:41,820 --> 00:05:44,920
 the country.

47
00:05:44,920 --> 00:05:54,440
 The second most, the first most is the period of the Thai

48
00:05:54,440 --> 00:05:56,920
 New Year in April.

49
00:05:56,920 --> 00:06:01,920
 So he would explain how this is one way of rejoicing.

50
00:06:01,920 --> 00:06:06,920
 This is one way of celebrating.

51
00:06:06,920 --> 00:06:14,700
 When something good comes up, this is one way of being

52
00:06:14,700 --> 00:06:18,920
 happy about things.

53
00:06:18,920 --> 00:06:24,860
 A lot learn, so they not learn, they rejoice or they

54
00:06:24,860 --> 00:06:27,920
 festive, they celebrate.

55
00:06:27,920 --> 00:06:33,390
 Then he asks, "How should we celebrate as Buddhists? And

56
00:06:33,390 --> 00:06:34,920
 what should we celebrate?

57
00:06:34,920 --> 00:06:39,920
 What should we be happy about? Why should we rejoice?"

58
00:06:39,920 --> 00:06:47,920
 It's a good question really, as we think sometimes.

59
00:06:47,920 --> 00:06:51,920
 Everybody else gets all the fun.

60
00:06:51,920 --> 00:06:56,760
 People in the world, they have good food to eat, they have

61
00:06:56,760 --> 00:07:02,920
 nice clothes and houses and cars.

62
00:07:02,920 --> 00:07:06,920
 So many wonderful things that they can enjoy.

63
00:07:06,920 --> 00:07:13,920
 They can go where they want, they can live where they want.

64
00:07:13,920 --> 00:07:18,320
 We have the picture of when you are a dainous monk, you

65
00:07:18,320 --> 00:07:22,620
 live in the perfect hut, on the perfect mountain with the

66
00:07:22,620 --> 00:07:26,920
 perfect sunset, the perfect forest.

67
00:07:26,920 --> 00:07:30,440
 And then when you actually come to the monastery, you see

68
00:07:30,440 --> 00:07:35,780
 the food is no good, the kuttis have leeks, the forests

69
00:07:35,780 --> 00:07:41,920
 have leeches, snakes, scorpions, and mosquitoes.

70
00:07:41,920 --> 00:07:48,920
 And the water is no good, nothing is ideal.

71
00:07:48,920 --> 00:07:58,920
 And you think here I have to live off of this cold food,

72
00:07:58,920 --> 00:08:07,300
 contaminated water, mosquitoes and snakes and scorpions and

73
00:08:07,300 --> 00:08:13,920
 leeches, leaking roofs and so on, termites.

74
00:08:13,920 --> 00:08:16,920
 You think why, why?

75
00:08:16,920 --> 00:08:21,920
 It's not much to celebrate.

76
00:08:21,920 --> 00:08:25,340
 Of course we don't think this and hopefully we are all very

77
00:08:25,340 --> 00:08:25,920
 happy.

78
00:08:25,920 --> 00:08:29,920
 If you are not, listen up.

79
00:08:29,920 --> 00:08:35,920
 Because there are many things for us to be happy.

80
00:08:35,920 --> 00:08:41,920
 We can use tonight as an example.

81
00:08:41,920 --> 00:08:47,920
 Here we are under the full moon.

82
00:08:47,920 --> 00:08:52,170
 The only thing between us and the full moon is the boli

83
00:08:52,170 --> 00:08:52,920
 tree.

84
00:08:52,920 --> 00:09:00,980
 This boli tree is a descendant from the tree under which

85
00:09:00,980 --> 00:09:04,920
 the Buddha himself sat.

86
00:09:04,920 --> 00:09:12,100
 In ancient times they brought a branch from Bodhgaya to Sri

87
00:09:12,100 --> 00:09:16,920
 Lanka and planted it in Anuradhapura.

88
00:09:16,920 --> 00:09:23,690
 And the tree in Anuradhapura has since been cultivated and

89
00:09:23,690 --> 00:09:27,920
 brought to many places around the country.

90
00:09:27,920 --> 00:09:31,920
 One branch came here.

91
00:09:31,920 --> 00:09:40,920
 So this tree is a descendant from the original tree.

92
00:09:40,920 --> 00:09:50,300
 And here we are in a Buddhist monastery in a Buddhist

93
00:09:50,300 --> 00:09:51,920
 country,

94
00:09:51,920 --> 00:10:01,550
 listening to a talk on Buddhism about the practice Buddhist

95
00:10:01,550 --> 00:10:03,920
 meditation,

96
00:10:03,920 --> 00:10:10,920
 living a life as a Buddhist meditator,

97
00:10:10,920 --> 00:10:17,920
 striving to find the truth of life, freedom from suffering,

98
00:10:17,920 --> 00:10:23,150
 striving to make ourselves better people, or to purify our

99
00:10:23,150 --> 00:10:24,920
 minds,

100
00:10:24,920 --> 00:10:30,920
 to cleanse our minds of all the family.

101
00:10:30,920 --> 00:10:42,920
 This in and of itself is something very rare in the world,

102
00:10:42,920 --> 00:10:45,920
 something very much worth rejoicing.

103
00:10:45,920 --> 00:10:53,920
 I think maybe sometimes we don't realize how lucky we are.

104
00:10:53,920 --> 00:10:57,920
 We should rejoice in that.

105
00:10:57,920 --> 00:11:04,920
 Just this. Because why are we lucky? What makes us lucky?

106
00:11:04,920 --> 00:11:08,260
 The first thing that makes us lucky is that we are born in

107
00:11:08,260 --> 00:11:11,920
 a time when the Buddha is,

108
00:11:11,920 --> 00:11:19,920
 the Buddha's teaching is here.

109
00:11:19,920 --> 00:11:24,180
 The Buddha's aren't people that come to the world every

110
00:11:24,180 --> 00:11:25,920
 three or four days.

111
00:11:25,920 --> 00:11:37,460
 The Buddha didn't work for just a week or so to become a

112
00:11:37,460 --> 00:11:39,920
 Buddha.

113
00:11:39,920 --> 00:11:42,680
 You can look around and you can ask if there is anybody

114
00:11:42,680 --> 00:11:43,920
 else since the time of the Buddha,

115
00:11:43,920 --> 00:11:47,920
 2,500 years.

116
00:11:47,920 --> 00:11:54,330
 But they say even 2,500 years is not the time that it takes

117
00:11:54,330 --> 00:11:56,920
 to become a Buddha.

118
00:11:56,920 --> 00:12:09,920
 Four uncountable eons and 100,000 epochs or countable eons.

119
00:12:09,920 --> 00:12:12,920
 Like the time it takes for the Big Bang,

120
00:12:12,920 --> 00:12:17,920
 from the Big Bang to whatever happens, the Big Crunch or

121
00:12:17,920 --> 00:12:23,920
 whatever the end of the universe is.

122
00:12:23,920 --> 00:12:27,920
 There's 100,000 of those. But that's the small part.

123
00:12:27,920 --> 00:12:31,920
 The big part is four uncountable eons.

124
00:12:31,920 --> 00:12:35,320
 From the time of the Big Bang to the Big Crunch you can

125
00:12:35,320 --> 00:12:36,920
 count, apparently.

126
00:12:36,920 --> 00:12:51,920
 But you can't count uncountable.

127
00:12:51,920 --> 00:13:01,920
 So here we are, here we are in the time of the Buddha.

128
00:13:01,920 --> 00:13:07,680
 We've missed the Buddha himself, but I'm not sure what we

129
00:13:07,680 --> 00:13:08,920
 were doing when he was teaching.

130
00:13:08,920 --> 00:13:14,800
 Maybe we were drinking or gambling or maybe we weren't even

131
00:13:14,800 --> 00:13:15,920
 humans.

132
00:13:15,920 --> 00:13:19,920
 Somehow we missed the chant.

133
00:13:19,920 --> 00:13:24,920
 So all that's left for us now is the Dhamma.

134
00:13:24,920 --> 00:13:30,330
 This is why we protect the Dhamma and we revere the Dhamma

135
00:13:30,330 --> 00:13:31,920
 as well because it's all that we have left.

136
00:13:31,920 --> 00:13:34,920
 The Dhamma and the Sangha.

137
00:13:34,920 --> 00:13:41,460
 We have teachers, we have our, the people who have passed

138
00:13:41,460 --> 00:13:43,920
 on the Buddha's teaching.

139
00:13:43,920 --> 00:13:49,920
 They've been an example to us in our practice.

140
00:13:49,920 --> 00:13:54,920
 And then we have the teaching itself. We still have this.

141
00:13:54,920 --> 00:14:03,150
 And when it's gone, when it's gone it'll just be dark water

142
00:14:03,150 --> 00:14:03,920
.

143
00:14:03,920 --> 00:14:07,920
 Now we have an island slowly sinking into the ocean.

144
00:14:07,920 --> 00:14:13,990
 And when the ocean is gone, when the island is gone, there

145
00:14:13,990 --> 00:14:17,920
 will be no refuge for being.

146
00:14:17,920 --> 00:14:24,830
 It'll be like drowning and drowning in water with no sign

147
00:14:24,830 --> 00:14:25,920
 of shore.

148
00:14:25,920 --> 00:14:41,200
 Floating around amongst the sharks and the dangers and the

149
00:14:41,200 --> 00:14:44,920
 storms of the ocean.

150
00:14:44,920 --> 00:14:48,920
 And darkness.

151
00:14:48,920 --> 00:14:53,760
 So here we are, here we are, we have the island, we're on

152
00:14:53,760 --> 00:14:54,920
 an island.

153
00:14:54,920 --> 00:14:57,920
 We're on the island of the Dhamma, the Buddha's teaching.

154
00:14:57,920 --> 00:15:03,160
 We have this teaching with us now and something we can put

155
00:15:03,160 --> 00:15:04,920
 into practice.

156
00:15:04,920 --> 00:15:08,280
 This is something that's very lucky for all people now in

157
00:15:08,280 --> 00:15:08,920
 the world.

158
00:15:08,920 --> 00:15:15,260
 For all beings now, to be and to be, this is an auspicious

159
00:15:15,260 --> 00:15:16,920
 time.

160
00:15:16,920 --> 00:15:21,920
 And we still have the Dhamma.

161
00:15:21,920 --> 00:15:30,910
 The second thing that we should rejoice in and be happy

162
00:15:30,910 --> 00:15:32,920
 about

163
00:15:32,920 --> 00:15:39,360
 is that not only were we born in the time of the Buddha,

164
00:15:39,360 --> 00:15:43,920
 but we've also been born a human being.

165
00:15:43,920 --> 00:15:47,950
 And we've been born a human being who has all of our arms

166
00:15:47,950 --> 00:15:48,920
 and all of our legs

167
00:15:48,920 --> 00:15:56,410
 and all of our body parts, including our brain, working in

168
00:15:56,410 --> 00:15:59,920
 fairly good order.

169
00:15:59,920 --> 00:16:04,540
 Good enough to be able to walk, good enough to be able to

170
00:16:04,540 --> 00:16:04,920
 sit,

171
00:16:04,920 --> 00:16:09,240
 good enough to be able to think and to read and to study,

172
00:16:09,240 --> 00:16:19,920
 good enough to practice.

173
00:16:19,920 --> 00:16:25,680
 If you're born a dog or a cat or a pig or an insect or a

174
00:16:25,680 --> 00:16:27,920
 snake, scorpion.

175
00:16:27,920 --> 00:16:30,450
 I think it goes without saying that there's not much

176
00:16:30,450 --> 00:16:34,920
 benefit that can come from your life.

177
00:16:34,920 --> 00:16:38,600
 I don't know what we did. We must have done something right

178
00:16:38,600 --> 00:16:38,920
.

179
00:16:38,920 --> 00:16:42,220
 We were wandering about in the ocean with some sara and

180
00:16:42,220 --> 00:16:43,920
 somehow the Buddha said,

181
00:16:43,920 --> 00:16:51,920
 "Somehow we managed to be born a human being."

182
00:16:51,920 --> 00:16:56,920
 The Buddha made a comparison with a turtle.

183
00:16:56,920 --> 00:16:59,920
 If there's a turtle that lives at the bottom of the ocean,

184
00:16:59,920 --> 00:17:08,250
 and every 100 years it has to come up for air, then someone

185
00:17:08,250 --> 00:17:09,920
 were to throw a yoke.

186
00:17:09,920 --> 00:17:13,200
 You know, these yokes, these things that they put, this

187
00:17:13,200 --> 00:17:14,920
 piece of wood that they put on the,

188
00:17:14,920 --> 00:17:24,920
 it has a bend in it to put around the neck of the ox.

189
00:17:24,920 --> 00:17:37,920
 Or maybe it's a ring or something. They put on the ox.

190
00:17:37,920 --> 00:17:40,920
 The Buddha said that every 100 years coming up,

191
00:17:40,920 --> 00:17:47,310
 in the middle, in the whole of the ocean there's one yoke

192
00:17:47,310 --> 00:17:48,920
 floating.

193
00:17:48,920 --> 00:17:51,360
 The Buddha said it's more likely that that turtle when he

194
00:17:51,360 --> 00:17:53,920
 comes up after 100 years

195
00:17:53,920 --> 00:18:02,480
 will come up with his neck in the yoke than it is for a

196
00:18:02,480 --> 00:18:03,920
 being to be born a,

197
00:18:03,920 --> 00:18:08,920
 for an animal to be born a human being.

198
00:18:08,920 --> 00:18:13,580
 Because as an animal there's not much you can do to

199
00:18:13,580 --> 00:18:16,920
 cultivate your mind.

200
00:18:16,920 --> 00:18:20,920
 As I said, mostly it's kill or be killed.

201
00:18:20,920 --> 00:18:24,590
 The opportunity is to practice morality, we develop

202
00:18:24,590 --> 00:18:25,920
 concentration and wisdom.

203
00:18:25,920 --> 00:18:31,600
 Somehow floating around in the ocean we managed to put our

204
00:18:31,600 --> 00:18:37,920
 neck up in the yoke.

205
00:18:37,920 --> 00:18:41,360
 Here we were born a human being. We don't know how we got

206
00:18:41,360 --> 00:18:41,920
 here.

207
00:18:41,920 --> 00:18:45,640
 I don't think any of us know how or we did that we became

208
00:18:45,640 --> 00:18:46,920
 human beings.

209
00:18:46,920 --> 00:18:51,080
 Maybe if you have special knowledge then you know what you

210
00:18:51,080 --> 00:18:51,920
 did.

211
00:18:51,920 --> 00:18:55,920
 Mostly we don't have a clue, we're just happy to be here.

212
00:18:55,920 --> 00:18:58,920
 We should be happy to be here.

213
00:18:58,920 --> 00:19:03,100
 We've met the Buddha and we've met the Buddha as a human

214
00:19:03,100 --> 00:19:03,920
 being.

215
00:19:03,920 --> 00:19:08,920
 We're human.

216
00:19:08,920 --> 00:19:13,080
 And this body is capable how long have we been alive and

217
00:19:13,080 --> 00:19:14,920
 yet we've managed to avoid death.

218
00:19:14,920 --> 00:19:19,660
 We've managed to avoid dismemberment with all the stupid

219
00:19:19,660 --> 00:19:20,920
 things that we've done,

220
00:19:20,920 --> 00:19:22,920
 all the crazy things that we've done.

221
00:19:22,920 --> 00:19:28,920
 We've still managed to avoid death and dismemberment.

222
00:19:28,920 --> 00:19:34,300
 We've managed to make it through school and job and make

223
00:19:34,300 --> 00:19:38,920
 enough money to live our lives in a way

224
00:19:38,920 --> 00:19:53,920
 that allows us to make our way here to this place.

225
00:19:53,920 --> 00:19:58,920
 So these things are very difficult to find the Buddha.

226
00:19:58,920 --> 00:20:06,920
 It's a lulabu.

227
00:20:06,920 --> 00:20:13,500
 Kichang Buddha Namukpa dang. The arising of the Buddha is a

228
00:20:13,500 --> 00:20:16,920
 difficult thing to find.

229
00:20:16,920 --> 00:20:25,920
 Kichang Machana Ji Vitaang. Kichang Kicho Manusapati Lamo.

230
00:20:25,920 --> 00:20:32,920
 Attaining of human life is difficult to find, rare to find.

231
00:20:32,920 --> 00:20:47,920
 Difficult to find is the life that is free from illness,

232
00:20:47,920 --> 00:20:50,920
 free from difficulties, free from obstacles

233
00:20:50,920 --> 00:20:55,100
 that allow you to come all the way across the world from

234
00:20:55,100 --> 00:20:55,920
 all the parts of the world.

235
00:20:55,920 --> 00:21:02,920
 Here we have people from all over the world coming.

236
00:21:02,920 --> 00:21:13,920
 Very few people have this opportunity.

237
00:21:13,920 --> 00:21:22,350
 The third thing that makes us lucky is that we actually

238
00:21:22,350 --> 00:21:26,920
 want to take the opportunity.

239
00:21:26,920 --> 00:21:30,900
 Because of all the people in the world who don't have the

240
00:21:30,900 --> 00:21:31,920
 opportunity,

241
00:21:31,920 --> 00:21:39,920
 very few are the people who have the opportunity.

242
00:21:39,920 --> 00:21:44,210
 But among those people who have the opportunity, even fewer

243
00:21:44,210 --> 00:21:47,920
 are those people who want to take the opportunity.

244
00:21:47,920 --> 00:21:50,450
 How many people, when you told them you were coming to med

245
00:21:50,450 --> 00:21:50,920
itate,

246
00:21:50,920 --> 00:21:56,920
 put confused looks on their face or gave you strange looks

247
00:21:56,920 --> 00:21:59,920
 or ridiculed you or whatever,

248
00:21:59,920 --> 00:22:04,650
 and when you decided you want to ordain, looked at you like

249
00:22:04,650 --> 00:22:05,920
 you're crazy,

250
00:22:05,920 --> 00:22:13,920
 or just abandoned you completely?

251
00:22:13,920 --> 00:22:17,920
 How many people have you heard say, "Monks are useless,

252
00:22:17,920 --> 00:22:28,060
 "monks are, meditators are wasting their time, selfish," so

253
00:22:28,060 --> 00:22:30,920
 on?

254
00:22:30,920 --> 00:22:34,840
 How many people have the opportunity but would never in a

255
00:22:34,840 --> 00:22:41,920
 million years think about coming to a meditation course,

256
00:22:41,920 --> 00:22:47,310
 let alone become a monk, let alone dedicate their lives to

257
00:22:47,310 --> 00:22:47,920
 it?

258
00:22:47,920 --> 00:22:52,160
 That's something worth rejoicing for sure, worth rejoicing

259
00:22:52,160 --> 00:22:52,920
 about.

260
00:22:52,920 --> 00:22:56,660
 How lucky we are to just have this mind that says, "That's

261
00:22:56,660 --> 00:22:57,920
 not us, no?

262
00:22:57,920 --> 00:23:00,220
 "That's not like we chose to have this mind," but suddenly

263
00:23:00,220 --> 00:23:00,920
 our mind says,

264
00:23:00,920 --> 00:23:06,920
 "I want to become a monk," or, "I want to go meditate."

265
00:23:06,920 --> 00:23:12,200
 It's kind of funny really because it's not like we chose to

266
00:23:12,200 --> 00:23:13,920
 have that mind.

267
00:23:13,920 --> 00:23:18,720
 It's not like other people chose to have the mind that didn

268
00:23:18,720 --> 00:23:19,920
't want to.

269
00:23:19,920 --> 00:23:21,920
 We cultivated this.

270
00:23:21,920 --> 00:23:26,160
 Somehow we managed to get something right. We should feel

271
00:23:26,160 --> 00:23:27,920
 happy about that.

272
00:23:27,920 --> 00:23:30,920
 We should rejoice in it.

273
00:23:30,920 --> 00:23:34,070
 How wonderful that I have this wonderful mind that wants to

274
00:23:34,070 --> 00:23:34,920
 meditate,

275
00:23:34,920 --> 00:23:39,920
 that wants to devote my life to the Buddha's teaching.

276
00:23:39,920 --> 00:23:43,700
 We maybe don't want to feel proud about it, but we can at

277
00:23:43,700 --> 00:23:45,920
 least feel proud for a second, no?

278
00:23:45,920 --> 00:23:49,480
 Give us some encouragement. Pride can be useful as

279
00:23:49,480 --> 00:23:50,920
 encouragement.

280
00:23:50,920 --> 00:23:54,920
 You have to take it only to the step of encouragement.

281
00:23:54,920 --> 00:23:57,920
 When you need encouragement, you can think of that.

282
00:23:57,920 --> 00:24:01,920
 I'm not a horrible person. I'm not a useless person.

283
00:24:01,920 --> 00:24:06,150
 I may not be the best meditator, but at least I want to med

284
00:24:06,150 --> 00:24:06,920
itate.

285
00:24:06,920 --> 00:24:11,920
 At least I do meditate.

286
00:24:11,920 --> 00:24:18,620
 So this is the third blessing that we have that we should

287
00:24:18,620 --> 00:24:19,920
 be happy about.

288
00:24:19,920 --> 00:24:23,920
 Rejoice.

289
00:24:23,920 --> 00:24:32,920
 The fourth thing is that we actually take the opportunity.

290
00:24:32,920 --> 00:24:35,920
 So this is what the Buddha said. He said,

291
00:24:35,920 --> 00:24:43,560
 "Of all the people in the world, rare are those who

292
00:24:43,560 --> 00:24:44,920
 understand

293
00:24:44,920 --> 00:24:48,090
 in regards to that which needs to be done, that it needs to

294
00:24:48,090 --> 00:24:48,920
 be done.

295
00:24:48,920 --> 00:24:51,920
 Understand that something needs to be done.

296
00:24:51,920 --> 00:24:54,920
 That we can't live like this. We can't be negligent

297
00:24:54,920 --> 00:24:58,920
 and just assume that when we die we're going to go to a

298
00:24:58,920 --> 00:24:58,920
 good place

299
00:24:58,920 --> 00:25:02,920
 or nothing bad is going to happen to us."

300
00:25:02,920 --> 00:25:04,920
 Actually, we're like walking a tightrope.

301
00:25:04,920 --> 00:25:08,920
 And any moment we could snip off and fall to whatever it is

302
00:25:08,920 --> 00:25:09,920
 that lies below.

303
00:25:09,920 --> 00:25:14,920
 Being a human being is like walking a tightrope.

304
00:25:14,920 --> 00:25:20,400
 If you were up here when the lightning struck two nights

305
00:25:20,400 --> 00:25:20,920
 ago,

306
00:25:20,920 --> 00:25:23,920
 it's quite an experience for me.

307
00:25:23,920 --> 00:25:28,920
 Realizing how thin is the rope on which we're walking.

308
00:25:28,920 --> 00:25:41,920
 Any moment, any moment, something could knock you off.

309
00:25:41,920 --> 00:25:46,920
 And if you haven't prepared yourself,

310
00:25:46,920 --> 00:25:51,240
 and done anything useful in your life, how will you be

311
00:25:51,240 --> 00:25:52,920
 ready for it?

312
00:25:52,920 --> 00:25:58,380
 And die without any, under any clarity of mind, any

313
00:25:58,380 --> 00:26:02,920
 presence, any awareness.

314
00:26:02,920 --> 00:26:08,920
 And die with a muddled, confused mind.

315
00:26:08,920 --> 00:26:12,920
 So, some people, many people have no idea.

316
00:26:12,920 --> 00:26:16,920
 Some people see this.

317
00:26:16,920 --> 00:26:21,920
 Some people see this but then they do nothing about it.

318
00:26:21,920 --> 00:26:27,290
 They think, "Yeah, it would be great if I could do some

319
00:26:27,290 --> 00:26:27,920
 meditation

320
00:26:27,920 --> 00:26:32,920
 or something spiritual anyway, but they don't do it."

321
00:26:32,920 --> 00:26:39,470
 So he said, "Among those rare people who have that good

322
00:26:39,470 --> 00:26:39,920
 intention

323
00:26:39,920 --> 00:26:46,920
 or the understanding, very, very much more rare

324
00:26:46,920 --> 00:26:54,920
 are the people who actually do something about it."

325
00:26:54,920 --> 00:26:59,310
 So here we've accumulated quite a number of blessings in

326
00:26:59,310 --> 00:27:00,920
 our lives

327
00:27:00,920 --> 00:27:04,920
 by being in this one spot at this time.

328
00:27:04,920 --> 00:27:07,920
 We've got the blessing of the Buddha and his teachings.

329
00:27:07,920 --> 00:27:09,920
 We know what are the teachings of the Buddha.

330
00:27:09,920 --> 00:27:12,920
 We know the Four Satipatahana, we know Vipassana,

331
00:27:12,920 --> 00:27:17,960
 we know the Four Noble Truths, we know the Eightfold Noble

332
00:27:17,960 --> 00:27:18,920
 Path.

333
00:27:18,920 --> 00:27:22,920
 These aren't just public schools.

334
00:27:22,920 --> 00:27:27,920
 These aren't the kind of things that anyone can teach you.

335
00:27:27,920 --> 00:27:31,920
 We take them for granted sometimes, "Ya, ya, I know them."

336
00:27:31,920 --> 00:27:35,270
 Even just the Four Foundations of Mindfulness is enough to

337
00:27:35,270 --> 00:27:37,920
 get you to Nibbana,

338
00:27:37,920 --> 00:27:42,360
 enough to free you from the entire web of samsara, all

339
00:27:42,360 --> 00:27:43,920
 suffering.

340
00:27:43,920 --> 00:27:45,920
 Just the Four Foundations of Mindfulness.

341
00:27:45,920 --> 00:27:47,920
 "Eka yano ayang gikore mago"

342
00:27:47,920 --> 00:27:56,920
 This path is the straight path, the one way.

343
00:27:57,920 --> 00:28:01,920
 Just the Four Foundations of Mindfulness.

344
00:28:01,920 --> 00:28:05,920
 And we have the ability to understand the teaching.

345
00:28:05,920 --> 00:28:08,920
 We're human, we have the ability to practice the teaching.

346
00:28:08,920 --> 00:28:15,860
 We can walk, we can sit, we can think, our mind works, our

347
00:28:15,860 --> 00:28:17,920
 body works.

348
00:28:17,920 --> 00:28:25,920
 We have the good intention to practice, we have confidence.

349
00:28:25,920 --> 00:28:30,150
 Just the knowledge that the Buddha's teaching was a good

350
00:28:30,150 --> 00:28:30,920
 thing.

351
00:28:30,920 --> 00:28:36,220
 Many people can't even come to this, either they repudiate,

352
00:28:36,220 --> 00:28:42,920
 repudiate, repudiate.

353
00:28:42,920 --> 00:28:46,920
 The Buddha's teaching, or they doubt about it.

354
00:28:46,920 --> 00:28:49,920
 Even if they think it might be good, they doubt.

355
00:28:49,920 --> 00:28:54,920
 And they have so much doubt that they can't practice.

356
00:28:54,920 --> 00:28:57,920
 Here we don't have any of that.

357
00:28:57,920 --> 00:29:05,920
 We have enough confidence to allow us to practice.

358
00:29:05,920 --> 00:29:09,630
 We have enough confidence that we decided to come to ordain

359
00:29:09,630 --> 00:29:09,920
.

360
00:29:09,920 --> 00:29:13,920
 That we've ordained.

361
00:29:13,920 --> 00:29:19,260
 Enough confidence to fly across the world, to stay in a hut

362
00:29:19,260 --> 00:29:20,920
 in a forest.

363
00:29:20,920 --> 00:29:23,920
 How rare is that?

364
00:29:23,920 --> 00:29:29,920
 We must have done something right to get here.

365
00:29:29,920 --> 00:29:33,780
 And the fact that we've actually done it and succeeded, we

366
00:29:33,780 --> 00:29:34,920
 made it.

367
00:29:34,920 --> 00:29:38,920
 We're not there yet, but physically we've made it.

368
00:29:38,920 --> 00:29:44,050
 We're here. We're where we should be. We're where we wanted

369
00:29:44,050 --> 00:29:44,920
 to be.

370
00:29:44,920 --> 00:29:49,250
 We're in a place where so many people have never had the

371
00:29:49,250 --> 00:29:51,920
 chance to be.

372
00:29:51,920 --> 00:29:53,920
 I consider this to be very lucky.

373
00:29:53,920 --> 00:29:58,920
 I think we all should consider ourselves quite lucky.

374
00:29:58,920 --> 00:30:02,920
 Consider this an auspicious time, an auspicious moment.

375
00:30:02,920 --> 00:30:06,360
 We're developing good wholesome qualities of mind, wholes

376
00:30:06,360 --> 00:30:12,560
ome qualities of speech, wholesome qualities of body, of

377
00:30:12,560 --> 00:30:19,920
 action.

378
00:30:19,920 --> 00:30:27,780
 It's quite fitting that this is what we're doing on a holy

379
00:30:27,780 --> 00:30:28,920
 day.

380
00:30:28,920 --> 00:30:31,920
 I always used to joke at D

381
00:30:31,920 --> 00:30:35,920
 So today is a holiday.

382
00:30:35,920 --> 00:30:39,920
 No, it doesn't mean you get a day off.

383
00:30:39,920 --> 00:30:43,920
 It means you get to be holy.

384
00:30:43,920 --> 00:30:53,920
 Buddhism, we take the holiness of the day on ourselves.

385
00:30:53,920 --> 00:30:56,920
 We don't leave the holiness with the day.

386
00:30:56,920 --> 00:31:00,920
 We take it to mean something for ourselves.

387
00:31:00,920 --> 00:31:05,260
 That the holy day is the time for us to become holy, for us

388
00:31:05,260 --> 00:31:09,920
 to increase our holiness.

389
00:31:09,920 --> 00:31:14,950
 As in Buddhism, we don't look for someone else to be holy

390
00:31:14,950 --> 00:31:15,920
 for us.

391
00:31:15,920 --> 00:31:20,920
 We take the responsibility on ourselves.

392
00:31:20,920 --> 00:31:25,460
 It's not that we don't believe in God or gods or angels or

393
00:31:25,460 --> 00:31:25,920
 so on.

394
00:31:25,920 --> 00:31:29,920
 But we figure they've got enough work to do for themselves.

395
00:31:29,920 --> 00:31:34,040
 So rather than asking them or praying to them, we'll do it

396
00:31:34,040 --> 00:31:35,920
 for ourselves.

397
00:31:35,920 --> 00:31:40,050
 The holiness isn't for the day, it isn't for God or an

398
00:31:40,050 --> 00:31:40,920
 angel.

399
00:31:40,920 --> 00:31:42,920
 The holiness of the day is for ourselves.

400
00:31:42,920 --> 00:31:44,920
 This is a Buddhist holiday.

401
00:31:44,920 --> 00:31:47,920
 Buddhist holy day.

402
00:31:47,920 --> 00:31:54,920
 It's not a day off.

403
00:31:54,920 --> 00:31:56,920
 So this is what we should feel lucky about.

404
00:31:56,920 --> 00:32:00,920
 This is what we should rejoice in.

405
00:32:00,920 --> 00:32:08,920
 So how do we rejoice as Buddhists?

406
00:32:08,920 --> 00:32:15,920
 I think there's only one answer to the best way to rejoice.

407
00:32:15,920 --> 00:32:22,920
 The best way to rejoice is to practice.

408
00:32:22,920 --> 00:32:26,920
 Practice to purify our minds.

409
00:32:26,920 --> 00:32:34,920
 I think it's kind of difficult really, because the

410
00:32:34,920 --> 00:32:37,920
 equivalent,

411
00:32:37,920 --> 00:32:43,440
 the equivalent in a physical sense is on the holiday or on

412
00:32:43,440 --> 00:32:46,920
 the festival or so on.

413
00:32:46,920 --> 00:32:49,670
 The best way to rejoice, someone telling you the best way

414
00:32:49,670 --> 00:32:51,920
 to rejoice is to go home

415
00:32:51,920 --> 00:32:57,920
 and clean up your bathroom or clean up your kitchen.

416
00:32:57,920 --> 00:33:04,920
 That doesn't seem like the best way to use the holy day.

417
00:33:04,920 --> 00:33:07,770
 But when you do have the day off, when you do have the time

418
00:33:07,770 --> 00:33:07,920
 off,

419
00:33:07,920 --> 00:33:13,820
 when you do have the time to spare, this is when people do

420
00:33:13,820 --> 00:33:15,920
 their house cleaning.

421
00:33:15,920 --> 00:33:21,180
 So even if this were a day off, this would be the best use

422
00:33:21,180 --> 00:33:23,920
 that we could have for it.

423
00:33:23,920 --> 00:33:26,120
 But there's another way we can understand this, the

424
00:33:26,120 --> 00:33:26,920
 practicing,

425
00:33:26,920 --> 00:33:32,920
 because the practice is like an exaltation of ourselves.

426
00:33:32,920 --> 00:33:37,330
 How difficult is it to even want to meditate, let alone to

427
00:33:37,330 --> 00:33:38,920
 meditate?

428
00:33:38,920 --> 00:33:40,920
 It's a way of rejoicing.

429
00:33:40,920 --> 00:33:43,920
 Meditation itself is a way of rejoicing.

430
00:33:43,920 --> 00:33:45,920
 It's taking a victory lap.

431
00:33:45,920 --> 00:33:50,920
 It's like boasting to the whole universe.

432
00:33:50,920 --> 00:33:56,920
 It's like proclaiming to the whole universe that we've won

433
00:33:56,920 --> 00:33:58,920
 and we're victorious.

434
00:33:58,920 --> 00:34:03,720
 We are meditating in spite of all the difficulty, in spite

435
00:34:03,720 --> 00:34:11,920
 of all the

436
00:34:11,920 --> 00:34:16,920
 improbability of us getting to this point. We made it.

437
00:34:16,920 --> 00:34:21,640
 So our walking meditation or sitting meditation is actually

438
00:34:21,640 --> 00:34:22,920
 a rejoicing.

439
00:34:22,920 --> 00:34:30,920
 Sometimes it doesn't feel like it, but it's victory.

440
00:34:30,920 --> 00:34:38,920
 It's the winning.

441
00:34:38,920 --> 00:34:46,560
 This is the path to victory, the action that makes us

442
00:34:46,560 --> 00:34:48,920
 victorious.

443
00:34:48,920 --> 00:34:55,300
 And it's the act that purifies our minds, and so it's the

444
00:34:55,300 --> 00:34:59,920
 act that brings us happiness.

445
00:34:59,920 --> 00:35:02,920
 Without purity of mind there's no happiness.

446
00:35:02,920 --> 00:35:08,390
 So this act of meditating is that which brings us true

447
00:35:08,390 --> 00:35:09,920
 happiness.

448
00:35:09,920 --> 00:35:18,890
 And so as a result it should be something that is happy for

449
00:35:18,890 --> 00:35:19,920
 us.

450
00:35:19,920 --> 00:35:24,310
 This is, I think, obviously, it goes without saying how

451
00:35:24,310 --> 00:35:26,920
 important on a Buddhist holiday it is to meditate,

452
00:35:26,920 --> 00:35:31,920
 but this is just one more way for us to think about it.

453
00:35:31,920 --> 00:35:37,010
 That was the question today, "Are we doing anything special

454
00:35:37,010 --> 00:35:37,920
 on a Buddhist holiday?"

455
00:35:37,920 --> 00:35:42,920
 And I don't think we meditate more.

456
00:35:42,920 --> 00:35:47,210
 But it's true, no? This is the time when people will

457
00:35:47,210 --> 00:35:48,920
 practice more intensely.

458
00:35:48,920 --> 00:35:55,030
 This is when the lay people come to the monastery and take

459
00:35:55,030 --> 00:35:56,920
 the precepts.

460
00:35:56,920 --> 00:36:00,970
 This is the time when people undertake the practice of

461
00:36:00,970 --> 00:36:03,920
 morality, concentration and wisdom,

462
00:36:03,920 --> 00:36:14,920
 because it's an excuse or it's a good opportunity for them.

463
00:36:14,920 --> 00:36:17,650
 So it's an even better opportunity for us who are living

464
00:36:17,650 --> 00:36:21,920
 here and who have the chance to take the opportunity

465
00:36:21,920 --> 00:36:27,920
 to make it as a joyful occasion for ourselves.

466
00:36:27,920 --> 00:36:36,710
 Here I have the opportunity to meditate and we show it, we

467
00:36:36,710 --> 00:36:40,920
 make it clear when you're on the holiday.

468
00:36:40,920 --> 00:36:45,920
 The holy day we practice with greater intention.

469
00:36:45,920 --> 00:36:48,630
 It doesn't mean you even have to practice longer periods of

470
00:36:48,630 --> 00:36:48,920
 time,

471
00:36:48,920 --> 00:36:52,070
 more to be mindful, knowing that this is the holy day,

472
00:36:52,070 --> 00:36:54,660
 knowing that there's the full moon and we're under the Bod

473
00:36:54,660 --> 00:36:54,920
hi tree,

474
00:36:54,920 --> 00:36:59,920
 thinking about the Buddha who sat under the Bodhi tree.

475
00:36:59,920 --> 00:37:06,240
 We're 20 feet away from the Buddha, because we have the Bod

476
00:37:06,240 --> 00:37:16,920
hi tree here under which the Buddha sat.

477
00:37:16,920 --> 00:37:20,370
 We can also take it as a pujja for the Buddha, a way of

478
00:37:20,370 --> 00:37:22,920
 paying homage to the Buddha as well.

479
00:37:22,920 --> 00:37:25,920
 We wonder what should we do on the holy day to pay homage

480
00:37:25,920 --> 00:37:26,920
 to the Buddha.

481
00:37:26,920 --> 00:37:30,370
 The Buddha himself was very clear on his topic when he was

482
00:37:30,370 --> 00:37:31,920
 passing away and they were bringing flowers

483
00:37:31,920 --> 00:37:37,000
 and paying homage to him. The Buddha said, "This isn't how

484
00:37:37,000 --> 00:37:38,920
 you pay homage to a Buddha."

485
00:37:38,920 --> 00:37:45,990
 He said, "Whoever practices yoko, ananda, bhikkhu, bhikkhu

486
00:37:45,990 --> 00:37:50,920
 niva, upasakhu, upasika, dhamma, nudhamma, paktipanno,

487
00:37:50,920 --> 00:37:57,360
 anudhamma jadi, sodhata, kalukaroti, sakaroti, kalukaroti,

488
00:37:57,360 --> 00:38:00,920
 manniti, pujeti, parama yaputai.

489
00:38:00,920 --> 00:38:08,430
 Whether it's a bhikkhu or a bhikkhu nahi or an upasika, up

490
00:38:08,430 --> 00:38:08,920
asika, upasika.

491
00:38:08,920 --> 00:38:12,560
 When they practice according to the teaching, realize the

492
00:38:12,560 --> 00:38:14,920
 teaching for themselves.

493
00:38:14,920 --> 00:38:20,660
 This is a person who pays proper homage, pays proper

494
00:38:20,660 --> 00:38:26,080
 respect, pays homage to the Buddha with the highest form of

495
00:38:26,080 --> 00:38:26,920
 homage.

496
00:38:26,920 --> 00:38:34,450
 "Pati pati bhansya." So every step that we take, this is

497
00:38:34,450 --> 00:38:42,920
 like offering a flower to the Buddha.

498
00:38:42,920 --> 00:38:48,380
 It's the highest form of offering that you can give to the

499
00:38:48,380 --> 00:38:50,920
 Buddha, the Buddha's teaching,

500
00:38:50,920 --> 00:38:54,920
 to our monastery, to our meditation center, to the world.

501
00:38:54,920 --> 00:38:56,920
 That one step that you take with mindfulness.

502
00:38:56,920 --> 00:39:02,560
 Clearly aware this is what it is, with morality,

503
00:39:02,560 --> 00:39:06,920
 concentration and wisdom, understanding.

504
00:39:06,920 --> 00:39:14,920
 So there's a pep talk for today, a Buddhist holiday.

505
00:39:14,920 --> 00:39:19,360
 And now on with the show. There's still a mindful

506
00:39:19,360 --> 00:39:22,920
 frustration. Then walking and then sitting.

