1
00:00:00,000 --> 00:00:14,540
 Okay, good evening. Broadcasting from Stony Creek, Ontario.

2
00:00:14,540 --> 00:00:27,000
 So, I was thinking that teaching

3
00:00:27,000 --> 00:00:30,180
 to an audience is quite different from not teaching to an

4
00:00:30,180 --> 00:00:33,720
 audience. I know I have a potential

5
00:00:33,720 --> 00:00:38,050
 audience out there on the internet. I think there's at

6
00:00:38,050 --> 00:00:40,440
 least a couple of people who listen

7
00:00:40,440 --> 00:00:45,920
 to these talks or wait to see whether I'm actually going to

8
00:00:45,920 --> 00:00:48,840
 give one in any given night.

9
00:00:48,840 --> 00:00:53,000
 But part of the reason for difficulty in actually doing it

10
00:00:53,000 --> 00:00:55,520
 every day is just thinking of what

11
00:00:55,520 --> 00:00:59,670
 to teach and I don't know who I'm actually teaching to. Not

12
00:00:59,670 --> 00:01:00,800
 having the connection with

13
00:01:00,800 --> 00:01:08,640
 an audience. So I thought we could go by a source. And so

14
00:01:08,640 --> 00:01:12,200
 the source I found is Words

15
00:01:12,200 --> 00:01:19,160
 of the Buddha by Venerable Dhammika. And it's also known as

16
00:01:19,160 --> 00:01:22,320
 Buddha Vatchanet. It's available

17
00:01:22,320 --> 00:01:32,640
 on buddhanet.net. And it's 365 days a year, dhamma. So

18
00:01:32,640 --> 00:01:36,200
 every day of the year there's

19
00:01:36,200 --> 00:01:40,700
 some quote that he gives from some part of the Buddhist

20
00:01:40,700 --> 00:01:44,800
 teacher. So we can go to July

21
00:01:44,800 --> 00:01:51,190
 8th, which is today, right? And we can find a quote which

22
00:01:51,190 --> 00:01:54,360
 would just be fairly random,

23
00:01:54,360 --> 00:01:59,600
 but insightful. Venerable Dhammika did this great service.

24
00:01:59,600 --> 00:02:00,640
 What he neglected to do is

25
00:02:00,640 --> 00:02:04,140
 actually put sources, but of course for many people that's

26
00:02:04,140 --> 00:02:06,080
 not important. For me it would

27
00:02:06,080 --> 00:02:11,830
 be probably helpful. There was a monk who went back and

28
00:02:11,830 --> 00:02:15,280
 actually found the sources and I

29
00:02:15,280 --> 00:02:19,160
 couldn't find his version, but we've incorporated it into

30
00:02:19,160 --> 00:02:21,560
 the digital Pali Reader anyway. Not

31
00:02:21,560 --> 00:02:27,320
 really important. We have those sources so I can look them

32
00:02:27,320 --> 00:02:30,760
 up. Almost all of them. But

33
00:02:30,760 --> 00:02:37,140
 anyway, today we're on page 151, July 8th, number 189. So

34
00:02:37,140 --> 00:02:40,920
 we're on day 189 of the year.

35
00:02:40,920 --> 00:02:46,650
 We're on the 189th day of the year and we can just cycle

36
00:02:46,650 --> 00:02:49,240
 through these. So July 8th

37
00:02:49,240 --> 00:02:55,340
 is about right view. I'm not going to read it out because

38
00:02:55,340 --> 00:02:58,400
 my job is to expand upon this.

39
00:02:58,400 --> 00:03:03,310
 He calls it perfect view. And this is a teaching that is

40
00:03:03,310 --> 00:03:06,320
 familiar to me, that I use often,

41
00:03:06,320 --> 00:03:10,990
 that my teacher would often use. The five things that lead

42
00:03:10,990 --> 00:03:14,640
 to right view. First a little

43
00:03:14,640 --> 00:03:23,700
 bit about right view itself. Right view is seeing things as

44
00:03:23,700 --> 00:03:27,880
 they are, seeing the truth,

45
00:03:27,880 --> 00:03:35,390
 not getting misled by falsehood. Not having a point of view

46
00:03:35,390 --> 00:03:39,720
 that is out of line with reality.

47
00:03:39,720 --> 00:03:45,590
 Out of sync with the way things actually are. So that one

48
00:03:45,590 --> 00:03:47,720
 way of, this is one way of explaining

49
00:03:47,720 --> 00:03:52,310
 why we suffer. Why we are not perfect. Why we hurt

50
00:03:52,310 --> 00:03:55,700
 ourselves and hurt others. Simply

51
00:03:55,700 --> 00:03:59,660
 because we don't get it. We don't understand the

52
00:03:59,660 --> 00:04:03,000
 consequences of our actions. We don't

53
00:04:03,000 --> 00:04:07,550
 understand the nature of our mind states, the nature of our

54
00:04:07,550 --> 00:04:10,200
 minds, the nature of reality.

55
00:04:10,200 --> 00:04:22,180
 So we have expectations that are unrealistic. This is not

56
00:04:22,180 --> 00:04:23,880
 the only reason, but one of the

57
00:04:23,880 --> 00:04:27,810
 main reasons why we suffer. Another reason would be simple

58
00:04:27,810 --> 00:04:31,000
 ignorance. This muddled state

59
00:04:31,000 --> 00:04:34,670
 of mind. So it's not that we believe something. It is we

60
00:04:34,670 --> 00:04:38,680
 don't have a clue. And as a result,

61
00:04:38,680 --> 00:04:43,150
 based on these two, this sort of blindness and

62
00:04:43,150 --> 00:04:47,200
 misconception, misunderstanding, these

63
00:04:47,200 --> 00:04:53,230
 two together contribute to all of our, to contribute or

64
00:04:53,230 --> 00:04:57,280
 work together to bring about all of the

65
00:04:57,280 --> 00:05:02,460
 problems in our life. So they lead us to believe good deeds

66
00:05:02,460 --> 00:05:05,520
 are bad deeds, bad deeds are, you

67
00:05:05,520 --> 00:05:11,490
 know, good deeds are harmful, bad deeds are beneficial.

68
00:05:11,490 --> 00:05:14,600
 That that which is beneficial is

69
00:05:14,600 --> 00:05:27,030
 harmful, that that which is harmful is beneficial. They

70
00:05:27,030 --> 00:05:34,440
 lead us to form unwholesome deeds, thinking

71
00:05:34,440 --> 00:05:38,630
 that we can benefit from them. They lead us to get caught

72
00:05:38,630 --> 00:05:42,120
 up in unwholesome deeds, to neglect

73
00:05:42,120 --> 00:06:08,760
 all the bad stuff, all our problems in life.

74
00:06:08,760 --> 00:06:17,420
 So we practice meditation to overcome this, but going in

75
00:06:17,420 --> 00:06:20,760
 more detail, there are actually

76
00:06:20,760 --> 00:06:24,840
 five things. It's not just meditation that we need to

77
00:06:24,840 --> 00:06:26,440
 undertake. And so we have these

78
00:06:26,440 --> 00:06:37,680
 five, five things all contribute to cultivating right view

79
00:06:37,680 --> 00:06:38,600
 and understanding.

80
00:06:39,400 --> 00:06:44,840
 So to clear up the fog that shrouds our minds in darkness,

81
00:06:44,840 --> 00:06:48,440
 keeps us from seeing clearly.

82
00:06:48,440 --> 00:06:57,630
 And to see clearly to actually correct our misunderstanding

83
00:06:57,630 --> 00:07:01,480
, to realign our views,

84
00:07:04,360 --> 00:07:11,720
 to give up beliefs in favor of knowledge and understanding.

85
00:07:11,720 --> 00:07:22,940
 The first one is Sila. Sila because, Sila morality, because

86
00:07:22,940 --> 00:07:24,680
 it's an immorality that

87
00:07:24,680 --> 00:07:28,600
 fogs the mind, it clouds the mind. And it's this cloud,

88
00:07:28,600 --> 00:07:32,040
 this cloud of defilements and greed of

89
00:07:32,040 --> 00:07:36,410
 anger and delusion that clouds our minds and intoxicates

90
00:07:36,410 --> 00:07:37,080
 our mind.

91
00:07:37,080 --> 00:07:44,760
 That leads us to hold wrong belief in the first place.

92
00:07:44,760 --> 00:07:49,950
 So morality is the very basis, puts the foundation down,

93
00:07:49,950 --> 00:07:54,680
 sets some pillars or fence posts,

94
00:07:58,280 --> 00:08:01,780
 sort of sets us in the right direction, provides what they

95
00:08:01,780 --> 00:08:03,160
 call moral compass, right?

96
00:08:03,160 --> 00:08:08,600
 It allows you to tell up from down, left from right,

97
00:08:08,600 --> 00:08:10,520
 forward from backwards,

98
00:08:10,520 --> 00:08:16,430
 have some direction, knowing basically what's right and

99
00:08:16,430 --> 00:08:17,800
 what's wrong,

100
00:08:17,800 --> 00:08:23,720
 and refraining from immoral activities.

101
00:08:27,400 --> 00:08:36,510
 To have a sort of a stability of behavior, to live a life

102
00:08:36,510 --> 00:08:38,440
 that is supportive and conducive

103
00:08:38,440 --> 00:08:40,360
 of clear thinking, clear seeing.

104
00:08:40,360 --> 00:08:48,760
 So this is the first one. Second one, Suta. Suta means to

105
00:08:48,760 --> 00:08:50,920
 listen.

106
00:08:53,240 --> 00:08:57,320
 So listening to the Dhamma, listening to teachings or

107
00:08:57,320 --> 00:08:58,280
 reading.

108
00:08:58,280 --> 00:09:10,280
 Suta is the passive intake of information.

109
00:09:10,280 --> 00:09:15,310
 And again, this is all about getting us on the right track

110
00:09:15,310 --> 00:09:17,640
 and in the right direction.

111
00:09:19,400 --> 00:09:22,520
 Learning things we didn't know, hearing arguments and

112
00:09:22,520 --> 00:09:31,320
 teachings on right view and wrong view, stories about

113
00:09:31,320 --> 00:09:33,720
 examples of

114
00:09:33,720 --> 00:09:40,760
 disadvantages to wrong view,

115
00:09:44,360 --> 00:09:49,680
 arguments and so on, teachings, instruction on how to med

116
00:09:49,680 --> 00:09:50,840
itate and so on.

117
00:09:50,840 --> 00:09:55,960
 All of these things contribute to get us on the right path.

118
00:09:55,960 --> 00:09:58,920
 Just hearing people say good things,

119
00:09:58,920 --> 00:10:03,960
 for example, hearing people talk about charity or morality,

120
00:10:03,960 --> 00:10:09,080
 just hearing about these things reminds us and

121
00:10:11,240 --> 00:10:17,190
 directs us. This is why hanging around people who do good

122
00:10:17,190 --> 00:10:20,680
 things is indispensable.

123
00:10:20,680 --> 00:10:30,620
 Because when we hang around others, we are inclined, we

124
00:10:30,620 --> 00:10:34,040
 have an example, we have a direction.

125
00:10:38,360 --> 00:10:41,830
 And the third one is Sagat Chah, which is no longer passive

126
00:10:41,830 --> 00:10:42,520
, but this is

127
00:10:42,520 --> 00:10:45,800
 interacting, so asking questions.

128
00:10:45,800 --> 00:10:53,110
 Rather than just holding on to our beliefs or cultivating

129
00:10:53,110 --> 00:10:54,040
 beliefs,

130
00:10:54,040 --> 00:10:57,490
 you know, we hear nowadays everyone's always saying, "I

131
00:10:57,490 --> 00:10:59,160
 believe," "Well, I believe."

132
00:10:59,160 --> 00:11:03,640
 And they were saying this in the Buddhist time and it's,

133
00:11:05,720 --> 00:11:08,290
 this is why sutta is so useful because having heard and

134
00:11:08,290 --> 00:11:09,480
 read the Buddha's teaching,

135
00:11:09,480 --> 00:11:12,320
 it's kind of funny to hear people say, "I believe, I

136
00:11:12,320 --> 00:11:14,680
 believe," because the Buddha was very,

137
00:11:14,680 --> 00:11:19,240
 the Buddha came to this as well, everyone was saying, "I

138
00:11:19,240 --> 00:11:20,120
 believe, I believe."

139
00:11:20,120 --> 00:11:23,240
 And he said, "Well, when you say I believe,

140
00:11:23,240 --> 00:11:29,940
 then the good thing is that you're still upholding the

141
00:11:29,940 --> 00:11:31,320
 truth."

142
00:11:32,600 --> 00:11:37,870
 Right? If I say, "I believe that when we die, there's

143
00:11:37,870 --> 00:11:38,840
 nothing,"

144
00:11:38,840 --> 00:11:44,320
 or, "I believe that we have an eternal soul," or, "I

145
00:11:44,320 --> 00:11:47,160
 believe that God is going to save me,"

146
00:11:47,160 --> 00:11:54,590
 then, provided you're not, provided you actually believe

147
00:11:54,590 --> 00:11:56,120
 that, then you're telling the truth.

148
00:11:56,120 --> 00:12:00,110
 You're saying, "I believe something," and that's true,

149
00:12:00,110 --> 00:12:01,480
 right? Unless you're lying

150
00:12:01,480 --> 00:12:05,830
 about the belief, then this is okay. This isn't yet a

151
00:12:05,830 --> 00:12:07,560
 problem. See, the problem is

152
00:12:07,560 --> 00:12:17,480
 that when you believe that and then you say, the problem is

153
00:12:17,480 --> 00:12:18,840
 that when you believe something,

154
00:12:18,840 --> 00:12:26,370
 then you will say, you will give rise to the idea that it's

155
00:12:26,370 --> 00:12:28,440
 actually true.

156
00:12:28,440 --> 00:12:30,820
 And that's where your problem lies, not in the fact that

157
00:12:30,820 --> 00:12:33,560
 you believe something. Well, not in

158
00:12:33,560 --> 00:12:40,530
 the statement of belief, in the belief itself and that the

159
00:12:40,530 --> 00:12:42,600
 belief itself leads to statements that

160
00:12:42,600 --> 00:12:49,530
 x is true. So, right? Because we have this idea that it's

161
00:12:49,530 --> 00:12:51,960
 okay if I believe, you know, I believe

162
00:12:51,960 --> 00:12:58,220
 x, you know, you believe that x, I believe y, you think,

163
00:12:58,220 --> 00:13:01,320
 well, the fact that you believe something

164
00:13:01,320 --> 00:13:07,430
 is not wrong. It's true that you do believe that. The

165
00:13:07,430 --> 00:13:09,320
 problem is the belief itself,

166
00:13:09,320 --> 00:13:15,190
 is that leads to statements that are false. True that you

167
00:13:15,190 --> 00:13:17,800
 believe it, but the belief itself is

168
00:13:17,800 --> 00:13:22,210
 false. It's not enough to believe something. When we get

169
00:13:22,210 --> 00:13:24,360
 this feeling nowadays that it's,

170
00:13:24,360 --> 00:13:27,420
 we just came up with something to believe and that's kind

171
00:13:27,420 --> 00:13:28,920
 of interesting. We don't

172
00:13:28,920 --> 00:13:32,440
 really realize how powerful belief is.

173
00:13:32,440 --> 00:13:37,720
 So, we have to have our beliefs challenged.

174
00:13:40,680 --> 00:13:46,420
 And moreover, we have to question our beliefs. We have to

175
00:13:46,420 --> 00:13:49,880
 seek confirmation of our beliefs.

176
00:13:49,880 --> 00:13:55,770
 So, talking with otherwise people is quite useful in this

177
00:13:55,770 --> 00:14:01,080
 regard. It's essential to ask questions.

178
00:14:01,080 --> 00:14:04,170
 When I was meditating and this happened, what does this

179
00:14:04,170 --> 00:14:06,920
 mean? Rather than I meditated and this

180
00:14:06,920 --> 00:14:09,840
 happened, that it must mean this, right? Which is what we

181
00:14:09,840 --> 00:14:11,720
 do if we don't have a teacher or someone

182
00:14:11,720 --> 00:14:18,020
 to ask questions. We just believe that it means something,

183
00:14:18,020 --> 00:14:20,120
 ascribe meaning to it.

184
00:14:20,120 --> 00:14:30,920
 Often with, you know, very little basis in reality.

185
00:14:33,080 --> 00:14:35,620
 You know, we hear voices, we hear sounds, everything,

186
00:14:35,620 --> 00:14:38,360
 ghosts, right? How many meditators

187
00:14:38,360 --> 00:14:41,440
 have come saying that there's a ghost in their room,

188
00:14:41,440 --> 00:14:43,640
 spirits in their room? Maybe it's true.

189
00:14:43,640 --> 00:14:52,420
 Maybe in certain cases it's true. This isn't the correct

190
00:14:52,420 --> 00:14:56,280
 way, the proper way to direct one's mind.

191
00:14:57,480 --> 00:15:01,270
 It's not, a sound isn't a ghost, a sound is a sound,

192
00:15:01,270 --> 00:15:03,960
 whether a ghost is causing it or not,

193
00:15:03,960 --> 00:15:13,670
 for example. So, the point being that we'll have the wrong

194
00:15:13,670 --> 00:15:15,880
 view, the wrong point of view,

195
00:15:15,880 --> 00:15:19,590
 focusing on ghosts and spirits and demons and curses and

196
00:15:19,590 --> 00:15:23,000
 all these things, heaven and hell.

197
00:15:25,080 --> 00:15:29,620
 So, our way of looking at experience is all wrong, can

198
00:15:29,620 --> 00:15:32,280
 easily become all wrong.

199
00:15:32,280 --> 00:15:38,240
 So, we need advice, discussion, we need to discuss our

200
00:15:38,240 --> 00:15:41,080
 problems. Not just listen either,

201
00:15:41,080 --> 00:15:43,590
 you know, sometimes we just listen to dhammataks and never

202
00:15:43,590 --> 00:15:45,480
 get any feedback on our own practice.

203
00:15:45,480 --> 00:15:48,600
 This is why every day when we do a course we'll have

204
00:15:48,600 --> 00:15:50,520
 feedback.

205
00:15:50,520 --> 00:16:02,550
 The number four, samatha, this is tranquility, calming the

206
00:16:02,550 --> 00:16:04,360
 mind, focusing the mind. This is

207
00:16:04,360 --> 00:16:07,770
 very useful. You could say it's the basis of proper insight

208
00:16:07,770 --> 00:16:12,200
. Part of our practice is to just

209
00:16:12,200 --> 00:16:17,790
 calm the mind, to get the mind on a good track. Not

210
00:16:17,790 --> 00:16:21,880
 necessarily with heavy focus or heavy

211
00:16:21,880 --> 00:16:26,720
 concentration, but with perfect focus. To be perfectly in

212
00:16:26,720 --> 00:16:29,000
 tune with whatever it is that we

213
00:16:29,000 --> 00:16:33,820
 experience. So, we're not reacting to it, we're just

214
00:16:33,820 --> 00:16:37,240
 experiencing it. This is the key to number

215
00:16:37,240 --> 00:16:43,170
 five, which is insight. So, tranquility or focus is what

216
00:16:43,170 --> 00:16:47,560
 leads to insight. The mind is

217
00:16:47,560 --> 00:16:55,600
 freed from the hindrances, liking, disliking, drowsiness,

218
00:16:55,600 --> 00:16:57,880
 distraction, doubt.

219
00:17:01,160 --> 00:17:09,450
 Then insight can arise. So samatha and nipasana is a high

220
00:17:09,450 --> 00:17:12,840
 level cause of right view.

221
00:17:12,840 --> 00:17:17,320
 These are the essential ones. No, all five of them.

222
00:17:20,040 --> 00:17:28,630
 You have morality, listening, discussing, calming your mind

223
00:17:28,630 --> 00:17:31,560
, finally seeing clearly. Because it's

224
00:17:31,560 --> 00:17:36,770
 seeing clearly that the mileage wrongs you. And you see

225
00:17:36,770 --> 00:17:41,560
 clearly you don't need a view at all.

226
00:17:41,560 --> 00:17:44,540
 You don't have to hold on to a belief, any belief

227
00:17:44,540 --> 00:17:46,920
 whatsoever. It's no longer belief.

228
00:17:46,920 --> 00:17:55,910
 So this is the teaching that Buddha gave, and we have on

229
00:17:55,910 --> 00:17:58,600
 July 8th, he says, "When you do these

230
00:17:58,600 --> 00:18:05,450
 things, you'll have both chitta-wimuti and panyawimuti. Ch

231
00:18:05,450 --> 00:18:09,320
itta-wimuti is freedom of mind, which means

232
00:18:10,200 --> 00:18:14,780
 jhanas entering into states of high calm and bliss, and p

233
00:18:14,780 --> 00:18:18,280
anyawimuti is freedom through wisdom,

234
00:18:18,280 --> 00:18:23,160
 because through right view comes the wisdom.

235
00:18:23,160 --> 00:18:34,280
 That prevents one from giving rise to unwholesome thoughts,

236
00:18:34,280 --> 00:18:35,880
 to give rise to wrong view,

237
00:18:37,720 --> 00:18:42,360
 give rise to any ignorance or greed or anger and so on.

238
00:18:42,360 --> 00:18:54,080
 So chitta-wimuti, it's freedom because of the concentration

239
00:18:54,080 --> 00:18:55,880
, because of one's state of mind.

240
00:18:55,880 --> 00:19:03,890
 One cultivates a state of peace and it dwells without any

241
00:19:03,890 --> 00:19:06,040
 liking, disliking, drowsiness,

242
00:19:06,040 --> 00:19:08,640
 distraction, that with a mind that is perfectly balanced.

243
00:19:08,640 --> 00:19:12,200
 And then panyawimuti is different in

244
00:19:12,200 --> 00:19:16,640
 that there's no need to enter into any state because the

245
00:19:16,640 --> 00:19:19,240
 wisdom prevents one from giving rise

246
00:19:19,240 --> 00:19:23,660
 to liking, disliking and so on. Simply just knowing and

247
00:19:23,660 --> 00:19:28,600
 clearly understanding the nature of reality.

248
00:19:28,600 --> 00:19:33,640
 And so the problem with the five ignorance prevents one.

249
00:19:35,640 --> 00:19:44,280
 The right view is key, right view is the catalyst to that

250
00:19:44,280 --> 00:19:45,480
 which

251
00:19:45,480 --> 00:19:51,190
 frees the mind from samsara. It's what leads one to attain

252
00:19:51,190 --> 00:19:52,200
 nimam.

253
00:19:52,200 --> 00:20:13,460
 So that's the dhamma for tonight. Five things that we have

254
00:20:13,460 --> 00:20:02,280
 to keep in mind that serve as a guideline

255
00:20:03,240 --> 00:20:07,450
 for how to attain this prized possession and right view

256
00:20:07,450 --> 00:20:09,640
 that leads to freedom.

257
00:20:09,640 --> 00:20:13,240
 Thanks for tuning in. Good night.

