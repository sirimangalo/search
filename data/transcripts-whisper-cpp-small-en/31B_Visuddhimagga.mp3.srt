1
00:00:00,000 --> 00:00:06,640
 explain in chapter 4 paragraph 63

2
00:00:06,640 --> 00:00:14,440
 or its proximate cause is grounds for the initiation of

3
00:00:14,440 --> 00:00:16,600
 energy so grounds for

4
00:00:16,600 --> 00:00:21,160
 the initiation of energy also I think we have met with it

5
00:00:21,160 --> 00:00:23,720
 somewhere back now if

6
00:00:23,720 --> 00:00:28,640
 you want to read all these grounds then please read book

7
00:00:28,640 --> 00:00:31,480
 dialogues of the Buddha

8
00:00:31,480 --> 00:00:43,600
 part 3 page 239 etc grounds for initiation of energy I'll

9
00:00:43,600 --> 00:00:44,080
 tell you only

10
00:00:44,080 --> 00:00:50,720
 only two there is something you have to do you have to do

11
00:00:50,720 --> 00:00:53,200
 something then before

12
00:00:53,200 --> 00:00:57,960
 doing that so you say to yourself when I am doing that that

13
00:00:57,960 --> 00:00:59,440
 work I will not get

14
00:00:59,440 --> 00:01:04,040
 opportunity to practice say meditation so I must practice

15
00:01:04,040 --> 00:01:05,400
 now before before I

16
00:01:05,400 --> 00:01:12,700
 get to that work so in this way you initiate energy you you

17
00:01:12,700 --> 00:01:13,880
 you make effort

18
00:01:13,880 --> 00:01:18,280
 and practice meditation and then after the work then you

19
00:01:18,280 --> 00:01:19,480
 practice meditation

20
00:01:19,480 --> 00:01:22,610
 to say to yourself during the work I didn't have the

21
00:01:22,610 --> 00:01:24,080
 opportunity to practice

22
00:01:24,080 --> 00:01:28,550
 meditation so now I must practice so in this way you allow

23
00:01:28,550 --> 00:01:30,880
 energy in yourself so

24
00:01:30,880 --> 00:01:37,960
 with regard to your work there are two with regard to going

25
00:01:37,960 --> 00:01:39,480
 traveling there are

26
00:01:39,480 --> 00:01:43,330
 two before before going going on travel and after coming

27
00:01:43,330 --> 00:01:45,960
 back on travel and then

28
00:01:45,960 --> 00:01:50,720
 there is sickness before before you you you get sickness

29
00:01:50,720 --> 00:01:52,120
 you practice meditation

30
00:01:52,120 --> 00:01:56,280
 and after sickness also you practice meditation and then

31
00:01:56,280 --> 00:01:58,560
 going for arms say

32
00:01:58,560 --> 00:02:03,640
 before going for arms after going for arms so actually

33
00:02:03,640 --> 00:02:05,240
 every time you have to

34
00:02:05,240 --> 00:02:09,490
 arouse energy to practice so these are called the grounds

35
00:02:09,490 --> 00:02:10,680
 for initiation of

36
00:02:10,680 --> 00:02:14,790
 energy when rightly initiated it should be regarded as the

37
00:02:14,790 --> 00:02:16,400
 root of all attainment

38
00:02:16,400 --> 00:02:20,220
 and so on and then the next one is jivita jivita is like

39
00:02:20,220 --> 00:02:21,320
 the jivita in the

40
00:02:21,320 --> 00:02:27,830
 corporeality and then samahati we come to samahati it puts

41
00:02:27,830 --> 00:02:30,160
 consciously evenly on

42
00:02:30,160 --> 00:02:35,160
 the object consciousness evenly on the object or it puts it

43
00:02:35,160 --> 00:02:37,400
 rightly on it so

44
00:02:37,400 --> 00:02:42,300
 these you know in the what samahati you have you have this

45
00:02:42,300 --> 00:02:44,960
 the prefix sam as a

46
00:02:44,960 --> 00:02:53,200
 m so that as a m can mean evenly some month or rightly some

47
00:02:53,200 --> 00:02:55,200
 month so evenly

48
00:02:55,200 --> 00:03:01,500
 rightly or it is just a mere collecting of the mind thus it

49
00:03:01,500 --> 00:03:03,360
 is concentration its

50
00:03:03,360 --> 00:03:06,880
 characteristic is non-wandering that means itself non-w

51
00:03:06,880 --> 00:03:08,800
andering or its

52
00:03:08,800 --> 00:03:12,780
 characteristic is non distracting that means with regard to

53
00:03:12,780 --> 00:03:15,000
 cognizant states so

54
00:03:15,000 --> 00:03:19,040
 it does not let good news as cognizant states to be

55
00:03:19,040 --> 00:03:21,200
 distracted and it itself is

56
00:03:21,200 --> 00:03:26,360
 non-wandering its function is to conglomerate cognizant

57
00:03:26,360 --> 00:03:26,960
 states that's

58
00:03:26,960 --> 00:03:32,240
 what does but powder so it keeps them together it is

59
00:03:32,240 --> 00:03:34,480
 manifested as peace

60
00:03:34,480 --> 00:03:40,360
 usually it it's proximate causes bliss but it is not always

61
00:03:40,360 --> 00:03:41,160
 therefore it's a

62
00:03:41,160 --> 00:03:43,680
 usually

63
00:03:45,720 --> 00:03:55,640
 because somebody can be without sukhya also right fifth J

64
00:03:55,640 --> 00:04:00,200
ana like it is to be

65
00:04:00,200 --> 00:04:04,950
 regarded as steadiness of the mind like the steadiness of

66
00:04:04,950 --> 00:04:06,080
 the lamps flame when

67
00:04:06,080 --> 00:04:11,520
 there is no drop so steadiness of the mind that is samadhi

68
00:04:11,520 --> 00:04:13,960
 so samadhi is is a

69
00:04:13,960 --> 00:04:23,760
 mental state which which puts the mind on the object evenly

70
00:04:23,760 --> 00:04:28,360
 and rightly evenly

71
00:04:28,360 --> 00:04:33,770
 means keeping the cognizant states together and rightly

72
00:04:33,770 --> 00:04:35,200
 means not itself

73
00:04:35,200 --> 00:04:41,350
 being distracted that is what we call samadhi on the object

74
00:04:41,350 --> 00:04:43,920
 it's on the object

75
00:04:43,920 --> 00:04:49,600
 and it also keeps the the other mental factors together not

76
00:04:49,600 --> 00:04:51,440
 that then be scattered

77
00:04:51,440 --> 00:04:57,620
 that is what is called samadhi and the next one is sadha

78
00:04:57,620 --> 00:05:00,640
 faith or confidence

79
00:05:00,640 --> 00:05:08,510
 and with regard to its function that is to enter into like

80
00:05:08,510 --> 00:05:10,800
 the setting out

81
00:05:10,800 --> 00:05:18,080
 across a flat and the SN the reference is given but I would

82
00:05:18,080 --> 00:05:19,720
 like to give to give

83
00:05:19,720 --> 00:05:25,300
 you another reference and that is expositor page 158 please

84
00:05:25,300 --> 00:05:27,800
 read for for

85
00:05:27,800 --> 00:05:35,270
 meaning of that setting out across a flat a brief in brief

86
00:05:35,270 --> 00:05:36,360
 a brave man could

87
00:05:36,360 --> 00:05:40,110
 take other people across the across the flood or across the

88
00:05:40,110 --> 00:05:44,040
 river I mean crossing

89
00:05:44,040 --> 00:05:48,770
 in a boat or crossing by themselves so in the same way when

90
00:05:48,770 --> 00:05:50,120
 you have sadha when

91
00:05:50,120 --> 00:05:54,180
 you have faith or confidence and you can plunge into things

92
00:05:54,180 --> 00:05:55,720
 and then you can

93
00:05:55,720 --> 00:06:01,750
 accomplish so sadha is compared to crossing the flood or

94
00:06:01,750 --> 00:06:03,040
 one who crosses the

95
00:06:03,040 --> 00:06:06,920
 flood of one who himself crosses the flood and who takes

96
00:06:06,920 --> 00:06:09,280
 others with him and

97
00:06:09,280 --> 00:06:15,440
 that is what we call sadha so please read expositor page

98
00:06:15,440 --> 00:06:24,040
 158 and then it

99
00:06:24,040 --> 00:06:27,560
 should be regarded as a hand because it takes hold of

100
00:06:27,560 --> 00:06:29,800
 profitable things as well

101
00:06:29,800 --> 00:06:40,200
 and as seed so for for regarding as a hand please read

102
00:06:40,200 --> 00:06:43,080
 gradual sayings third

103
00:06:43,080 --> 00:06:50,280
 volume page 245 if you have a hand you can pick up things

104
00:06:50,280 --> 00:06:51,320
 that are profitable

105
00:06:51,320 --> 00:06:55,920
 to you that are good for you in the same way if you have

106
00:06:55,920 --> 00:06:56,640
 faith if you have

107
00:06:56,640 --> 00:07:02,650
 confidence you can get kusala so that is why it is compared

108
00:07:02,650 --> 00:07:03,880
 to a hand sometimes

109
00:07:03,880 --> 00:07:09,330
 it is compared to wealth and sometimes to a seed and then

110
00:07:09,330 --> 00:07:10,720
 sadhis I mean

111
00:07:10,720 --> 00:07:16,260
 mindfulness you know you all of you know about mindfulness

112
00:07:16,260 --> 00:07:17,680
 it has a characteristic

113
00:07:17,680 --> 00:07:21,860
 of not wobbling that means not floating on the surface its

114
00:07:21,860 --> 00:07:22,960
 function is not to

115
00:07:22,960 --> 00:07:26,790
 forget that means not to lose the object not to forget

116
00:07:26,790 --> 00:07:27,880
 means not to lose the

117
00:07:27,880 --> 00:07:32,480
 object and it is manifested as carding and so on and it's

118
00:07:32,480 --> 00:07:34,560
 proximate cause is

119
00:07:34,560 --> 00:07:38,440
 the foundations of mindfulness and here foundations of

120
00:07:38,440 --> 00:07:39,960
 mindfulness really means

121
00:07:39,960 --> 00:07:47,040
 the objects of foundations of mindfulness the body feeling

122
00:07:47,040 --> 00:07:53,210
 consciousness and say dhamma objects it should be regarded

123
00:07:53,210 --> 00:07:54,840
 as like a pillar

124
00:07:54,840 --> 00:07:57,970
 because it is firmly founded or as like a doorkeeper

125
00:07:57,970 --> 00:07:59,680
 because it gots the idol and

126
00:07:59,680 --> 00:08:05,960
 so on and then two things he we and what about so here is

127
00:08:05,960 --> 00:08:06,920
 translated here as

128
00:08:06,920 --> 00:08:12,630
 conscience and put up by as shame I don't think this is

129
00:08:12,630 --> 00:08:14,680
 quite correct

130
00:08:14,680 --> 00:08:22,320
 what about is fear or dread here is shame so I think we

131
00:08:22,320 --> 00:08:23,440
 should translate

132
00:08:23,440 --> 00:08:27,810
 theory as shame or conscience but put up by something like

133
00:08:27,810 --> 00:08:29,560
 dread or fear and fear

134
00:08:29,560 --> 00:08:37,160
 here means moral moral fear we are ashamed to do what is

135
00:08:37,160 --> 00:08:38,760
 wrong and that is

136
00:08:38,760 --> 00:08:43,650
 that is he or shame and we are afraid to do what is wrong

137
00:08:43,650 --> 00:08:45,760
 because we don't want

138
00:08:45,760 --> 00:08:53,810
 to get the painful consequences of these actions so this is

139
00:08:53,810 --> 00:08:54,880
 a term for anxiety

140
00:08:54,880 --> 00:08:58,720
 about evil here in consensus the characteristic of disgust

141
00:08:58,720 --> 00:08:59,960
 at evil while

142
00:08:59,960 --> 00:09:03,040
 shame that is second one that's the characteristic of dread

143
00:09:03,040 --> 00:09:05,440
 of it right so

144
00:09:05,440 --> 00:09:14,290
 so shame and dread shame and fear these two things and then

145
00:09:14,290 --> 00:09:17,400
 a man rejects evil

146
00:09:17,400 --> 00:09:20,230
 through conscience out of respect for himself as the

147
00:09:20,230 --> 00:09:21,600
 daughter of a good family

148
00:09:21,600 --> 00:09:26,750
 does he rejects evil through shame or the second thing fear

149
00:09:26,750 --> 00:09:28,280
 out of respect for

150
00:09:28,280 --> 00:09:36,980
 another as a courtesan does these two states I say we will

151
00:09:36,980 --> 00:09:37,960
 strike out the but

152
00:09:37,960 --> 00:09:41,880
 these two states should be regarded as the guardians of the

153
00:09:41,880 --> 00:09:43,000
 world they are

154
00:09:43,000 --> 00:09:47,800
 described as the guardians of the world so long as these

155
00:09:47,800 --> 00:09:50,040
 two reside in the minds

156
00:09:50,040 --> 00:09:56,080
 of the minds of beings the world will go on when these two

157
00:09:56,080 --> 00:09:57,560
 left the minds of the

158
00:09:57,560 --> 00:10:02,940
 beings then the world will become a mark what about and

159
00:10:02,940 --> 00:10:05,480
 differentiated they will

160
00:10:05,480 --> 00:10:14,240
 not not act according to their conscience so there will be

161
00:10:14,240 --> 00:10:15,280
 no moral

162
00:10:15,280 --> 00:10:21,530
 moral restrictions or something so the human beings will

163
00:10:21,530 --> 00:10:22,280
 become like

164
00:10:22,280 --> 00:10:25,360
 acne males and so on and that is why they are called the

165
00:10:25,360 --> 00:10:27,680
 guardians of the world

166
00:10:27,680 --> 00:10:36,920
 moral shame and moral fear that means a shame to do immoral

167
00:10:36,920 --> 00:10:39,640
 things and fear to

168
00:10:39,640 --> 00:10:47,600
 do these things the next is aloba adosa and amoja aloba non

169
00:10:47,600 --> 00:10:50,680
 greed adosa non hate

170
00:10:50,680 --> 00:10:56,250
 and amoja non delusion so amoja non delusion means what p

171
00:10:56,250 --> 00:10:58,440
anya wisdom so amoja

172
00:10:58,440 --> 00:11:05,090
 and panya are the same and about the middle of that bar

173
00:11:05,090 --> 00:11:06,840
ograph its function is

174
00:11:06,840 --> 00:11:12,040
 not to lay hold like a liberated biku it is manifested at

175
00:11:12,040 --> 00:11:13,480
 this as a state of not

176
00:11:13,480 --> 00:11:17,430
 eating as a shelter like that of a man who has fallen into

177
00:11:17,430 --> 00:11:18,840
 field I mean not

178
00:11:18,840 --> 00:11:24,190
 attached to or not adhering to something like that so when

179
00:11:24,190 --> 00:11:25,400
 you when you fall into

180
00:11:25,400 --> 00:11:30,470
 into field and you are not attached to feel you want to get

181
00:11:30,470 --> 00:11:32,080
 rid of it as soon

182
00:11:32,080 --> 00:11:38,540
 as possible so it is not being attached and then about two

183
00:11:38,540 --> 00:11:39,720
 three lines down its

184
00:11:39,720 --> 00:11:43,020
 function is to remove annoyance or its function is to

185
00:11:43,020 --> 00:11:44,760
 remove fever that means to

186
00:11:44,760 --> 00:11:53,460
 remove heat not necessarily fever but heat as central wood

187
00:11:53,460 --> 00:11:55,760
 does now when when

188
00:11:55,760 --> 00:11:59,760
 people are hot people use the central wood paste they apply

189
00:11:59,760 --> 00:12:00,520
 central wood paste

190
00:12:00,520 --> 00:12:07,090
 to their bodies and then they become cool it is very

191
00:12:07,090 --> 00:12:10,720
 familiar thing in in in

192
00:12:10,720 --> 00:12:15,240
 Burma you may have seen Burmese girls with something like

193
00:12:15,240 --> 00:12:15,920
 praise on their

194
00:12:15,920 --> 00:12:22,600
 cheek it's only used in boys it's like what do you want

195
00:12:22,600 --> 00:12:24,880
 this made make make up

196
00:12:24,880 --> 00:12:31,390
 something you apply on your face so when it is hot that

197
00:12:31,390 --> 00:12:32,520
 they apply that kind of

198
00:12:32,520 --> 00:12:37,120
 thing especially central central wood they make it central

199
00:12:37,120 --> 00:12:37,960
 wood into a paste

200
00:12:37,960 --> 00:12:43,900
 or something how do you call this making push to it

201
00:12:43,900 --> 00:12:46,440
 grinding yeah

202
00:12:46,440 --> 00:12:56,330
 yes now something like a flat stone a flat stone and then

203
00:12:56,330 --> 00:12:57,960
 we push it on the

204
00:12:57,960 --> 00:13:02,450
 stone so that with water so they get a paste and that basis

205
00:13:02,450 --> 00:13:03,320
 applied to the face

206
00:13:03,320 --> 00:13:09,440
 or to the other parts of the body and it keeps them cool to

207
00:13:09,440 --> 00:13:10,800
 something like that

208
00:13:10,800 --> 00:13:15,020
 here non delusion has the characteristic of penetrating

209
00:13:15,020 --> 00:13:16,160
 things according to the

210
00:13:16,160 --> 00:13:21,970
 individual essence essence and so on so this is panya it

211
00:13:21,970 --> 00:13:23,960
 has a characteristic of

212
00:13:23,960 --> 00:13:28,460
 sure penetration so it will never miss panya if it is real

213
00:13:28,460 --> 00:13:29,800
 panya will never miss

214
00:13:29,800 --> 00:13:33,570
 and like the penetration of an arrow short by skillful ar

215
00:13:33,570 --> 00:13:34,760
cher right its

216
00:13:34,760 --> 00:13:39,050
 function is to illuminate the objective field so if this

217
00:13:39,050 --> 00:13:41,400
 room is dark we cannot

218
00:13:41,400 --> 00:13:44,910
 see things in this room so when there is light we see

219
00:13:44,910 --> 00:13:46,600
 things clearly in the same

220
00:13:46,600 --> 00:13:50,230
 way when there is no panya we don't see things as they are

221
00:13:50,230 --> 00:13:52,160
 when panya enters our

222
00:13:52,160 --> 00:13:56,760
 minds we see things as they are so it is like like a lamp

223
00:13:56,760 --> 00:13:58,040
 it is manifested as

224
00:13:58,040 --> 00:14:02,210
 non-bivoulement like a guide in the forest so you may be

225
00:14:02,210 --> 00:14:03,280
 lost in the forest

226
00:14:03,280 --> 00:14:08,600
 but if you have a guide you will not be you will not get

227
00:14:08,600 --> 00:14:10,160
 lost the three should

228
00:14:10,160 --> 00:14:15,140
 be regarded as a roots of all that is profitable and then

229
00:14:15,140 --> 00:14:18,600
 tranquility of body

230
00:14:18,600 --> 00:14:24,860
 tranquility of consciousness and so on now they are pairs

231
00:14:24,860 --> 00:14:26,720
 here body does not

232
00:14:26,720 --> 00:14:32,380
 mean material body here nobody means three mental aggreg

233
00:14:32,380 --> 00:14:33,740
ates feeling

234
00:14:33,740 --> 00:14:37,910
 perception and mental formations so they are here called in

235
00:14:37,910 --> 00:14:44,600
 Palika ya and the

236
00:14:44,600 --> 00:14:48,120
 next prayer lightness of the body lightness of

237
00:14:48,120 --> 00:14:52,840
 consciousness there are

238
00:14:52,840 --> 00:14:58,210
 parallel college material qualities among the 24 material

239
00:14:58,210 --> 00:15:00,320
 properties and then

240
00:15:00,320 --> 00:15:05,290
 next is what malleability of body and malleability of

241
00:15:05,290 --> 00:15:07,400
 consciousness and the

242
00:15:07,400 --> 00:15:11,410
 next pair will be nice of body will be nice of

243
00:15:11,410 --> 00:15:13,560
 consciousness and the next pair

244
00:15:13,560 --> 00:15:17,140
 proficiency of body proficiency of consciousness another

245
00:15:17,140 --> 00:15:17,880
 next where

246
00:15:17,880 --> 00:15:24,940
 rectitude of body rectitude of consciousness now we come to

247
00:15:24,940 --> 00:15:25,520
 zeal

248
00:15:25,520 --> 00:15:32,370
 desire chanda now chanda is just a desire to act the mere

249
00:15:32,370 --> 00:15:34,640
 will so that zeal

250
00:15:34,640 --> 00:15:37,560
 has a characteristic of desire to act its function is

251
00:15:37,560 --> 00:15:38,880
 scanning for an object

252
00:15:38,880 --> 00:15:42,930
 searching for an object it is manifested as need for an

253
00:15:42,930 --> 00:15:44,560
 object that same object

254
00:15:44,560 --> 00:15:47,330
 is its proximate cause it should be regarded as the

255
00:15:47,330 --> 00:15:48,720
 extending of the mental

256
00:15:48,720 --> 00:15:52,620
 hand in the every ending of an object that means if you

257
00:15:52,620 --> 00:15:53,760
 want to pick up

258
00:15:53,760 --> 00:15:57,560
 something yeah you put on your hand something like that so

259
00:15:57,560 --> 00:15:58,680
 it is not

260
00:15:58,680 --> 00:16:03,640
 attachment but it is just just the the mere will to do the

261
00:16:03,640 --> 00:16:07,920
 desire to do next is

262
00:16:07,920 --> 00:16:13,040
 resolution it has characteristic of conviction its function

263
00:16:13,040 --> 00:16:13,960
 is not to grow

264
00:16:13,960 --> 00:16:18,300
 it is manifested as decisiveness its proximate causes a

265
00:16:18,300 --> 00:16:19,680
 theme to be convinced

266
00:16:19,680 --> 00:16:23,830
 about it should be regarded like a boundary post or it is a

267
00:16:23,830 --> 00:16:25,720
 gate post going

268
00:16:25,720 --> 00:16:29,970
 to its immovable ness with respect to the object and the

269
00:16:29,970 --> 00:16:31,840
 next is in Pali

270
00:16:31,840 --> 00:16:39,380
 manasikara it is the maker of what is to be made it is the

271
00:16:39,380 --> 00:16:40,920
 maker in the mind now

272
00:16:40,920 --> 00:16:46,000
 this definition is difficult to translate actually what is

273
00:16:46,000 --> 00:16:46,760
 the Pali what

274
00:16:46,760 --> 00:16:53,420
 is manasi and kara now kara means doing or making and man

275
00:16:53,420 --> 00:16:55,720
asi mean in the mind so

276
00:16:55,720 --> 00:17:01,690
 manasi kara means doing in the mind that is one meaning of

277
00:17:01,690 --> 00:17:04,840
 making in the mind

278
00:17:06,720 --> 00:17:17,510
 that means paying attention the other meaning is making the

279
00:17:17,510 --> 00:17:19,600
 mind that means

280
00:17:19,600 --> 00:17:28,160
 making the mind different so the third line it makes the

281
00:17:28,160 --> 00:17:30,200
 mind different from

282
00:17:30,200 --> 00:17:35,190
 the previous life continuum mind thus it is attention so

283
00:17:35,190 --> 00:17:36,560
 after the what does we

284
00:17:36,560 --> 00:17:42,030
 should put also that's also it is attention because it is

285
00:17:42,030 --> 00:17:42,880
 it is a different

286
00:17:42,880 --> 00:17:46,840
 definition from the first one the first one says it is the

287
00:17:46,840 --> 00:17:48,040
 making in the mind

288
00:17:48,040 --> 00:17:52,480
 and second one says making the mind you are making the mind

289
00:17:52,480 --> 00:17:53,920
 in making the mind

290
00:17:53,920 --> 00:18:08,340
 different and this definition refers to the second and

291
00:18:08,340 --> 00:18:11,520
 third kind of attention

292
00:18:11,520 --> 00:18:16,730
 describes described later down about towards the end of the

293
00:18:16,730 --> 00:18:18,680
 paragraph now

294
00:18:18,680 --> 00:18:22,310
 there are three kinds of manasi kara let me use the Pali

295
00:18:22,310 --> 00:18:23,560
 what there are three

296
00:18:23,560 --> 00:18:26,760
 kinds of manasi kara

297
00:18:26,760 --> 00:18:40,360
 controller of objects controller of cognitive series that

298
00:18:40,360 --> 00:18:43,240
 means controller of

299
00:18:48,160 --> 00:18:53,560
 cognitive series what is cognitive series thought process

300
00:18:53,560 --> 00:19:04,320
 consular of thought process and controller of impal

301
00:19:04,320 --> 00:19:08,920
 team so five door adverting is called controller of thought

302
00:19:08,920 --> 00:19:10,120
 process because

303
00:19:10,120 --> 00:19:14,520
 the thought process real thought process begins with that

304
00:19:14,520 --> 00:19:15,840
 that movement of

305
00:19:15,840 --> 00:19:20,390
 consciousness and control of impalchants means mind or ad

306
00:19:20,390 --> 00:19:25,960
verting because after

307
00:19:25,960 --> 00:19:31,560
 the mind or adverting come impalchants

308
00:19:31,560 --> 00:19:37,580
 but they are not meant here actually what is meant is just

309
00:19:37,580 --> 00:19:39,880
 attention which is

310
00:19:39,880 --> 00:19:47,120
 called controller of controller of objects

311
00:19:47,120 --> 00:19:56,800
 so there are three ways and three three kinds of manasi k

312
00:19:56,800 --> 00:19:58,360
ara controller of

313
00:19:58,360 --> 00:20:02,890
 objects controller of cognitive series and controller of

314
00:20:02,890 --> 00:20:04,400
 impalchants the last

315
00:20:04,400 --> 00:20:10,250
 two are not not not meant here so the first one is what is

316
00:20:10,250 --> 00:20:11,920
 meant here the

317
00:20:11,920 --> 00:20:18,640
 controller of object that means just paying attention and

318
00:20:18,640 --> 00:20:19,840
 then next one is

319
00:20:19,840 --> 00:20:26,570
 specific neutrality and it is often called uppika the Pali

320
00:20:26,570 --> 00:20:28,000
 word is tatra

321
00:20:28,000 --> 00:20:32,140
 madjata neutral in regard there to that means just being in

322
00:20:32,140 --> 00:20:33,440
 the middle not

323
00:20:33,440 --> 00:20:40,080
 falling into liking or disliking it has the characteristic

324
00:20:40,080 --> 00:20:41,200
 of conveying

325
00:20:41,200 --> 00:20:44,710
 consciousness and consciousness concomitants evenly so

326
00:20:44,710 --> 00:20:45,600
 making them even

327
00:20:45,600 --> 00:20:49,250
 its function is to prevent deficiency and excess or its

328
00:20:49,250 --> 00:20:50,880
 function is to inhibit

329
00:20:50,880 --> 00:20:54,640
 partiality it is manifested as neutrality it should be

330
00:20:54,640 --> 00:20:55,160
 regarded as

331
00:20:55,160 --> 00:20:58,780
 like a conductor or a driver who looks with equanimity on

332
00:20:58,780 --> 00:20:59,800
 thoroughbreds

333
00:20:59,800 --> 00:21:06,840
 progressing evenly so I I I compare this to cruise control

334
00:21:06,840 --> 00:21:10,280
 in a car you put on

335
00:21:10,280 --> 00:21:14,390
 the cruise controller you don't have to worry about speech

336
00:21:14,390 --> 00:21:19,960
 here also when the

337
00:21:19,960 --> 00:21:25,350
 thoroughbreds mean when horses are running evenly drawing

338
00:21:25,350 --> 00:21:26,560
 the card evenly

339
00:21:26,560 --> 00:21:30,560
 then you don't have to worry about them you just look on

340
00:21:30,560 --> 00:21:32,840
 and then compression and

341
00:21:32,840 --> 00:21:36,360
 gladness karuna and mudita they are described and they

342
00:21:36,360 --> 00:21:38,960
 define abodes the

343
00:21:38,960 --> 00:21:43,520
 only difference is there they belong to jhanas and here

344
00:21:43,520 --> 00:21:44,400
 they belong to karmavajara

345
00:21:44,400 --> 00:21:54,400
 and on the next page that should not be admitted for s to

346
00:21:54,400 --> 00:21:55,800
 meaning no

347
00:21:55,800 --> 00:22:00,880
 instead of saying as to meaning we should say in reality

348
00:22:00,880 --> 00:22:02,960
 has to mean not as to

349
00:22:02,960 --> 00:22:07,400
 meaning in reality non hate itself is loving kindness and

350
00:22:07,400 --> 00:22:08,640
 specific neutrality

351
00:22:08,640 --> 00:22:14,140
 itself is equanimity and then we have absences three abs

352
00:22:14,140 --> 00:22:15,440
ences I think you

353
00:22:15,440 --> 00:22:26,710
 understand about them and then describes the mental mental

354
00:22:26,710 --> 00:22:29,520
 states which arise

355
00:22:29,520 --> 00:22:32,680
 with different types of consciousness so it may be

356
00:22:32,680 --> 00:22:34,400
 confusing for you if you do

357
00:22:34,400 --> 00:22:39,720
 not have the 89 types of consciousness in mind and also 52

358
00:22:39,720 --> 00:22:41,200
 types of consciousness

359
00:22:41,200 --> 00:22:49,020
 so they can be studied from the manual of abidama or during

360
00:22:49,020 --> 00:22:50,160
 the the abidama

361
00:22:50,160 --> 00:22:56,500
 class here I just with a handouts so you may you may look

362
00:22:56,500 --> 00:22:57,920
 at those handouts and

363
00:22:57,920 --> 00:23:05,050
 then read these passages and then we come to unprofitable

364
00:23:05,050 --> 00:23:06,920
 Aku Sala they are

365
00:23:06,920 --> 00:23:14,300
 also on page 529 paragraph 159 that's because the unprof

366
00:23:14,300 --> 00:23:15,840
itable also there are

367
00:23:15,840 --> 00:23:22,680
 constants in constants and and over the mistakes so they

368
00:23:22,680 --> 00:23:23,640
 they are clearly

369
00:23:23,640 --> 00:23:26,600
 mentioned here

370
00:23:33,200 --> 00:23:41,680
 and then down the page we have the conscience lessness and

371
00:23:41,680 --> 00:23:42,520
 shamelessness

372
00:23:42,520 --> 00:23:47,120
 here also we may say and the first one shamelessness and

373
00:23:47,120 --> 00:23:47,840
 second one

374
00:23:47,840 --> 00:23:55,730
 fearlessness a hiriga is shamelessness and a notabha is

375
00:23:55,730 --> 00:23:59,800
 fearlessness and then

376
00:23:59,800 --> 00:24:06,440
 there is Loba and Mohat described in paragraph 161 and 162

377
00:24:06,440 --> 00:24:10,880
 and then greed is

378
00:24:10,880 --> 00:24:17,560
 compared to bird lime or monkey lime and that is the

379
00:24:17,560 --> 00:24:19,680
 explanation of the

380
00:24:19,680 --> 00:24:24,500
 description of greed like monkey lime can be read in Kind

381
00:24:24,500 --> 00:24:27,200
red Sayings book 5

382
00:24:27,200 --> 00:24:38,620
 page 127 when a monkey is stuck to that lime it could not

383
00:24:38,620 --> 00:24:41,040
 get itself free from

384
00:24:41,040 --> 00:24:45,570
 that lime that sticky substance in the same way when you

385
00:24:45,570 --> 00:24:46,520
 have greed when you

386
00:24:46,520 --> 00:24:50,380
 have attachment then you cannot get away from it you are

387
00:24:50,380 --> 00:24:53,960
 stuck to the object and

388
00:24:53,960 --> 00:25:03,070
 then on paragraph 163 about five lines down it is

389
00:25:03,070 --> 00:25:04,880
 manifested as the absence of

390
00:25:04,880 --> 00:25:14,280
 right theory what is theory is it understanding

391
00:25:15,040 --> 00:25:24,610
 something that has not been proven yet I see it is the word

392
00:25:24,610 --> 00:25:28,600
 used here is petipati

393
00:25:28,600 --> 00:25:35,620
 and it means understanding or knowing so absence of right

394
00:25:35,620 --> 00:25:37,600
 understanding

395
00:25:37,600 --> 00:25:51,030
 and then wrong view agitation and so on so I think they are

396
00:25:51,030 --> 00:25:52,240
 not difficult to

397
00:25:52,240 --> 00:25:57,880
 understand and then

398
00:25:59,520 --> 00:26:15,910
 akusala now I brought these two two sheets right so the the

399
00:26:15,910 --> 00:26:17,320
 jt seekers are

400
00:26:17,320 --> 00:26:24,120
 given in the order as in the manual of a beat em and then

401
00:26:24,120 --> 00:26:26,480
 Roman numerals are

402
00:26:26,480 --> 00:26:35,200
 those given in this book and there is one one team now if

403
00:26:35,200 --> 00:26:38,080
 you look at paragraph

404
00:26:38,080 --> 00:26:45,520
 166 and of 166 you find stiffness and topper given only one

405
00:26:45,520 --> 00:26:50,360
 number 43 right

406
00:26:50,360 --> 00:27:00,620
 Roman numeral 43 and then for the here on for ika gada it

407
00:27:00,620 --> 00:27:02,640
 gives two number here

408
00:27:02,640 --> 00:27:10,810
 in this book actually that should have only one number on

409
00:27:10,810 --> 00:27:16,840
 page 532 bottom line

410
00:27:16,840 --> 00:27:22,450
 steadiness of consciousness steadiness of consciousness is

411
00:27:22,450 --> 00:27:23,440
 the same as

412
00:27:23,440 --> 00:27:31,040
 concentration which is number eight it should not be given

413
00:27:31,040 --> 00:27:32,760
 a separate number

414
00:27:32,760 --> 00:27:39,800
 because it is it is the same mental factor as concentration

415
00:27:39,800 --> 00:27:41,560
 and stiff and

416
00:27:41,560 --> 00:27:46,960
 talk a stiffness and topper should be given in number each

417
00:27:46,960 --> 00:27:49,440
 so there may be some

418
00:27:49,440 --> 00:27:55,350
 some correction to be made if you if you want it to be

419
00:27:55,350 --> 00:27:57,960
 correct but as it is you

420
00:27:57,960 --> 00:28:03,390
 may you can look at this this this sheets and then find out

421
00:28:03,390 --> 00:28:06,280
 what is meant in

422
00:28:06,280 --> 00:28:12,430
 the manual of a beat em because in this book the mental

423
00:28:12,430 --> 00:28:14,720
 states are given not not

424
00:28:14,720 --> 00:28:22,450
 not in in in the group the they belong they are here given

425
00:28:22,450 --> 00:28:24,320
 as accompanying

426
00:28:24,320 --> 00:28:29,260
 different types of consciousness so they are repeated in

427
00:28:29,260 --> 00:28:30,640
 different types of

428
00:28:30,640 --> 00:28:37,740
 consciousness for example contact is repeated on page 532 5

429
00:28:37,740 --> 00:28:46,680
33 and so on and

430
00:28:46,680 --> 00:28:56,260
 then what else we find another theory on page 533 for a

431
00:28:56,260 --> 00:28:59,720
 graph 177 it should be

432
00:28:59,720 --> 00:29:05,010
 regarded as obstructive obstructive of theory there here

433
00:29:05,010 --> 00:29:06,840
 also I think we should

434
00:29:06,840 --> 00:29:14,120
 say obstructive of understanding but in Burma we understand

435
00:29:14,120 --> 00:29:15,780
 this as meaning

436
00:29:15,780 --> 00:29:21,500
 obstructive of practice so if you have doubt then you do

437
00:29:21,500 --> 00:29:23,160
 not practice so it

438
00:29:23,160 --> 00:29:26,800
 obstruct your practice so you have doubt about the teaching

439
00:29:26,800 --> 00:29:27,320
 you have doubt

440
00:29:27,320 --> 00:29:30,370
 about the efficacy of this method then you will not

441
00:29:30,370 --> 00:29:31,600
 practice it so it is

442
00:29:31,600 --> 00:29:37,580
 obstructive to the practice so we interpret the word as

443
00:29:37,580 --> 00:29:41,080
 practice in Burma

444
00:29:41,080 --> 00:29:58,640
 so

445
00:30:05,400 --> 00:30:14,190
 and then the book describes which which mental factors

446
00:30:14,190 --> 00:30:16,720
 accompany which which type

447
00:30:16,720 --> 00:30:22,340
 of consciousness and as I said they may be confusing if you

448
00:30:22,340 --> 00:30:24,720
 do not have the the

449
00:30:24,720 --> 00:30:31,500
 89 types of consciousness in mind so it is better to to

450
00:30:31,500 --> 00:30:33,520
 read the manual of a

451
00:30:33,520 --> 00:30:37,250
 bit of iron find out where which type of consciousness as

452
00:30:37,250 --> 00:30:38,520
 accompanied by which

453
00:30:38,520 --> 00:30:42,760
 type of j

454
00:30:42,760 --> 00:30:48,600
 at the beginning

455
00:30:48,600 --> 00:30:59,430
 it is said that altogether harmony the first the first type

456
00:30:59,430 --> 00:31:00,320
 of consciousness

457
00:31:00,320 --> 00:31:10,880
 accompanied by 36 paragraph 133 here and firstly those

458
00:31:10,880 --> 00:31:12,200
 associated with the first

459
00:31:12,200 --> 00:31:19,320
 sense we are profitable consciousness amount to 36 is that

460
00:31:19,320 --> 00:31:22,800
 correct yes why

461
00:31:22,800 --> 00:31:35,360
 36 and not 38 actually they must be 38 right

462
00:31:35,360 --> 00:31:54,280
 then why 36 no

463
00:31:54,280 --> 00:32:00,280
 hmm

464
00:32:00,280 --> 00:32:08,280
 hmm

465
00:32:08,280 --> 00:32:27,280
 hmm

466
00:32:27,280 --> 00:32:35,440
 I'll give you a hint hmm what that's right we are talking

467
00:32:35,440 --> 00:32:36,400
 about formation

468
00:32:36,400 --> 00:32:40,950
 aggregate here so we deny and Sanja are not counted here

469
00:32:40,950 --> 00:32:43,160
 but in the manual we

470
00:32:43,160 --> 00:32:45,980
 don't have Sanja also counted because they are mental

471
00:32:45,980 --> 00:32:47,800
 mental factors so in the

472
00:32:47,800 --> 00:32:51,020
 main manual of a bit of my you will find that the eight

473
00:32:51,020 --> 00:32:52,080
 teacher seekers

474
00:32:52,080 --> 00:32:55,770
 accompanying this consciousness that is correct and it is

475
00:32:55,770 --> 00:32:57,600
 correct too because

476
00:32:57,600 --> 00:33:03,260
 here the father is describing the mental formation to

477
00:33:03,260 --> 00:33:05,040
 concentrate aggregate that

478
00:33:05,040 --> 00:33:11,070
 is why these two are missing here so out of the 52 so they

479
00:33:11,070 --> 00:33:14,840
 are 52 but 52 a mental

480
00:33:14,840 --> 00:33:19,760
 mental mental state I mean mental factors but Sankara only

481
00:33:19,760 --> 00:33:23,640
 50 not 52

482
00:33:30,960 --> 00:33:35,690
 this chapter is like like manual of a bit of my you are

483
00:33:35,690 --> 00:33:37,720
 really studying a bit

484
00:33:37,720 --> 00:33:49,240
 amount so with with with these I think you can you can hide

485
00:33:49,240 --> 00:33:53,360
 out which is which

486
00:33:53,360 --> 00:34:01,460
 that is why there are no no numbers for see we dinner and

487
00:34:01,460 --> 00:34:06,240
 Sanja right and

488
00:34:06,240 --> 00:34:12,590
 aesthetics are whatever stage and blood science are in

489
00:34:12,590 --> 00:34:13,760
 constant

490
00:34:19,840 --> 00:34:24,530
 in constant means they don't they don't accompany always

491
00:34:24,530 --> 00:34:26,240
 they accompany that

492
00:34:26,240 --> 00:34:31,140
 type of consciousness sometimes only now for example number

493
00:34:31,140 --> 00:34:33,080
 25 and 26 on this

494
00:34:33,080 --> 00:34:42,950
 sheet she and made up they accompany the five prompted

495
00:34:42,950 --> 00:34:44,920
 right five prompted types

496
00:34:44,920 --> 00:34:50,440
 of consciousness but not not every time

497
00:34:50,440 --> 00:34:59,010
 suppose it doesn't maybe stealing stealing something with

498
00:34:59,010 --> 00:35:00,840
 actively stealing

499
00:35:00,840 --> 00:35:06,000
 then his consciousness may not be accompanied by Tina and

500
00:35:06,000 --> 00:35:08,400
 made up so only

501
00:35:08,400 --> 00:35:15,010
 when there is see sleepiness or something our Tina and made

502
00:35:15,010 --> 00:35:17,160
 a company the

503
00:35:17,160 --> 00:35:20,140
 consciousness otherwise they don't so we are called in

504
00:35:20,140 --> 00:35:22,640
 constant or unfixed

505
00:35:22,640 --> 00:35:38,600
 adjuncts okay next week we'll go up to the end of basis see

506
00:35:38,600 --> 00:35:40,840
 paragraph six next

507
00:35:40,840 --> 00:35:47,450
 next chapter of paragraph 16 chapter 15 paragraph 16 that

508
00:35:47,450 --> 00:35:51,480
 is preach 550 to

509
00:35:51,480 --> 00:36:00,630
 that so now we have come to the end of five aggregates and

510
00:36:00,630 --> 00:36:02,840
 next week some

511
00:36:02,840 --> 00:36:08,010
 other things to know about five aggregates but the detail

512
00:36:08,010 --> 00:36:09,120
 treatment of

513
00:36:09,120 --> 00:36:18,530
 aggregates complete now so what are the five aggregates

514
00:36:18,530 --> 00:36:21,160
 aggregate of matter

515
00:36:21,160 --> 00:36:26,000
 aggregate of feeling aggregate of perception aggregate of

516
00:36:26,000 --> 00:36:27,080
 conch I mean

517
00:36:27,080 --> 00:36:31,960
 mental formations and aggregate of consciousness but here

518
00:36:31,960 --> 00:36:33,000
 aggregate of

519
00:36:33,000 --> 00:36:38,500
 consciousness is given before the three other aggregates

520
00:36:38,500 --> 00:36:40,360
 but the usual order

521
00:36:40,360 --> 00:36:47,530
 given in the sodas is matter feeling perception mental

522
00:36:47,530 --> 00:36:48,560
 formations and

523
00:36:48,560 --> 00:36:57,010
 consciousness and these these five can be reduced to truth

524
00:36:57,010 --> 00:37:01,480
 nama and ruba so first

525
00:37:01,480 --> 00:37:06,480
 corporeality aggregate is ruba and the other four nama so

526
00:37:06,480 --> 00:37:08,400
 when we say nama and

527
00:37:08,400 --> 00:37:12,810
 ruba we mean these five aggregates so whether we say five

528
00:37:12,810 --> 00:37:13,800
 aggregates or nama

529
00:37:13,800 --> 00:37:17,520
 and ruba we mean the same thing actually

530
00:37:17,520 --> 00:37:20,520
 okay

531
00:37:20,520 --> 00:37:23,520
 yeah

532
00:37:24,520 --> 00:37:26,520
 yeah

533
00:37:26,520 --> 00:37:29,520
 yeah

534
00:37:31,520 --> 00:37:32,520
 yeah

535
00:37:33,520 --> 00:37:35,520
 yeah

536
00:37:37,520 --> 00:37:38,520
 yeah

537
00:37:40,520 --> 00:37:41,520
 yeah

538
00:37:43,520 --> 00:37:44,520
 yeah

539
00:37:44,520 --> 00:37:47,520
 yeah

540
00:37:49,520 --> 00:37:50,520
 yeah

