1
00:00:00,000 --> 00:00:03,790
 How do I increase Virya? How do I increase Virya from the

2
00:00:03,790 --> 00:00:06,320
 five strengths?

3
00:00:06,320 --> 00:00:13,020
 The goal or the focus should not be to develop Virya,

4
00:00:13,020 --> 00:00:16,160
 effort, energy. The focus

5
00:00:16,160 --> 00:00:20,280
 should be to develop mindfulness because mindfulness is the

6
00:00:20,280 --> 00:00:24,000
 balancing faculty.

7
00:00:24,000 --> 00:00:27,620
 So you shouldn't ever worry about any one of the other four

8
00:00:27,620 --> 00:00:28,240
. As you develop

9
00:00:28,240 --> 00:00:33,680
 mindfulness, the five sadha, confidence, effort,

10
00:00:33,680 --> 00:00:35,120
 concentration and wisdom will

11
00:00:35,120 --> 00:00:40,320
 become balanced. Morality, concentration, wisdom will be

12
00:00:40,320 --> 00:00:41,640
 created and the hateful

13
00:00:41,640 --> 00:00:45,230
 noble path will be created and enlightenment will arise

14
00:00:45,230 --> 00:00:45,600
 based on

15
00:00:45,600 --> 00:00:52,640
 mindfulness. Virya should be balanced with Samadhi, which

16
00:00:52,640 --> 00:00:52,840
 is

17
00:00:52,840 --> 00:00:58,260
 concentration. If you have too much concentration you'll

18
00:00:58,260 --> 00:00:59,880
 become drowsy. If

19
00:00:59,880 --> 00:01:02,450
 that's the case then you should focus on the drowsiness and

20
00:01:02,450 --> 00:01:03,800
 be mindful of it. Once

21
00:01:03,800 --> 00:01:06,070
 you're mindful of it you'll clear up the drowsiness and you

22
00:01:06,070 --> 00:01:06,840
'll find that your

23
00:01:06,840 --> 00:01:09,920
 Virya is increased.

