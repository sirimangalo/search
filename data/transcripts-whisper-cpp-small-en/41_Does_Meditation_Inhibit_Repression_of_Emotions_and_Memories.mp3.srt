1
00:00:00,000 --> 00:00:05,750
 Does meditation inhibit repression of emotions and memories

2
00:00:05,750 --> 00:00:06,000
?

3
00:00:06,000 --> 00:00:08,000
 Question.

4
00:00:08,000 --> 00:00:12,560
 In the context of my daily meditation practice, I find the

5
00:00:12,560 --> 00:00:16,000
 concept of repression of emotions hard to grasp.

6
00:00:16,000 --> 00:00:20,430
 Do you think meditation inhibits people from being able to

7
00:00:20,430 --> 00:00:23,000
 repress emotions or memories?

8
00:00:23,000 --> 00:00:25,000
 Answer.

9
00:00:25,000 --> 00:00:29,000
 Repression is a bit of a difficult concept.

10
00:00:29,000 --> 00:00:32,960
 It is not dealt with the same in Buddhism as it is in

11
00:00:32,960 --> 00:00:35,000
 ordinary discourse.

12
00:00:35,000 --> 00:00:38,890
 Repression is usually seen as a negative thing, something

13
00:00:38,890 --> 00:00:41,000
 that causes problems.

14
00:00:41,000 --> 00:00:45,940
 Digging up is often seen in ordinary discourse as a good

15
00:00:45,940 --> 00:00:51,000
 thing to do, to come to terms with our repressed emotions.

16
00:00:51,000 --> 00:00:55,170
 There are some parallels to this in Buddhism, but it is not

17
00:00:55,170 --> 00:00:58,000
 understood quite the same.

18
00:00:58,000 --> 00:01:02,310
 Momentary repression of emotions is actually technically

19
00:01:02,310 --> 00:01:06,000
 what we are doing in the practice of meditation.

20
00:01:06,000 --> 00:01:09,720
 When you practice loving kindness meditation, you are rep

21
00:01:09,720 --> 00:01:13,960
ressing or suppressing the anger through the force of loving

22
00:01:13,960 --> 00:01:15,000
 kindness.

23
00:01:15,000 --> 00:01:20,120
 Through the practice of mindfulness, when you say pain,

24
00:01:20,120 --> 00:01:25,000
 pain, or so on, the anger is being repressed.

25
00:01:25,000 --> 00:01:30,130
 It is understood as prevention, stopping it from arising as

26
00:01:30,130 --> 00:01:33,960
 technically there is no emotion being suppressed and there

27
00:01:33,960 --> 00:01:36,000
 is no suppressor.

28
00:01:36,000 --> 00:01:41,000
 There are only momentary experiences that arise and cease.

29
00:01:41,000 --> 00:01:46,130
 Technically, when you repress something, all that happens

30
00:01:46,130 --> 00:01:50,020
 is you create a specific mind state that prevents another

31
00:01:50,020 --> 00:01:52,000
 mind state from arising.

32
00:01:52,000 --> 00:01:57,190
 What we call repression generally has to do with anger,

33
00:01:57,190 --> 00:02:00,000
 aversion towards the mind state.

34
00:02:00,000 --> 00:02:03,000
 Something arises and you get angry about it.

35
00:02:03,000 --> 00:02:07,390
 The anger does not actually repress the emotion, it has

36
00:02:07,390 --> 00:02:10,000
 already been replaced with the anger.

37
00:02:10,000 --> 00:02:15,130
 Whatever it was that you disliked or were afraid of, etc.,

38
00:02:15,130 --> 00:02:17,000
 was not repressed.

39
00:02:17,000 --> 00:02:21,230
 It triggered that response and ceased when the response

40
00:02:21,230 --> 00:02:22,000
 arose.

41
00:02:22,000 --> 00:02:28,480
 You might say to yourself, "No, no, no. Don't let it come

42
00:02:28,480 --> 00:02:32,520
 up," thinking that somehow you are going to prevent

43
00:02:32,520 --> 00:02:35,000
 something that has already arisen.

44
00:02:35,000 --> 00:02:40,000
 You see it as a problem, though it has already passed.

45
00:02:40,000 --> 00:02:43,860
 What this does is create a habit. Every time the emotion

46
00:02:43,860 --> 00:02:48,410
 comes up, rather than dealing with it, you react with

47
00:02:48,410 --> 00:02:55,000
 aversion, anger, fear, etc., and cut off the emotion.

48
00:02:55,000 --> 00:02:59,900
 Suppose you experience lust arising. You see a beautiful

49
00:02:59,900 --> 00:03:04,000
 woman or a beautiful man and lust arises.

50
00:03:04,000 --> 00:03:08,050
 Then you might get angry at yourself and feel guilty,

51
00:03:08,050 --> 00:03:12,910
 thinking, "How horrible. What a sin. What a negative mind

52
00:03:12,910 --> 00:03:14,000
 state."

53
00:03:14,000 --> 00:03:17,850
 When this anger arises, the lust has no potential to

54
00:03:17,850 --> 00:03:20,000
 continue for the moment.

55
00:03:20,000 --> 00:03:25,610
 Now you have a different problem, these feelings of guilt,

56
00:03:25,610 --> 00:03:29,000
 anger, self-hatred, and so on.

57
00:03:29,000 --> 00:03:33,520
 As this becomes habitual, it can seem like you are rep

58
00:03:33,520 --> 00:03:37,700
ressing the lust, but you are actually just changing the

59
00:03:37,700 --> 00:03:39,000
 focus of the mind,

60
00:03:39,000 --> 00:03:42,860
 complicating the experience rather than understanding it

61
00:03:42,860 --> 00:03:44,000
 objectively.

62
00:03:44,000 --> 00:03:48,830
 The lust in the first place is a habit that has developed

63
00:03:48,830 --> 00:03:50,000
 over time.

64
00:03:50,000 --> 00:03:54,000
 There is no reason to have attraction to the human body.

65
00:03:54,000 --> 00:03:58,800
 Science can explain attraction away based on genes,

66
00:03:58,800 --> 00:04:01,000
 hormones, and so on.

67
00:04:01,000 --> 00:04:05,020
 But from a Buddhist point of view, looking at things exper

68
00:04:05,020 --> 00:04:09,000
ientially, in the moment-to-moment mind states,

69
00:04:09,000 --> 00:04:12,480
 there is no reason why we should come to this state of

70
00:04:12,480 --> 00:04:15,000
 finding the human body pleasant.

71
00:04:15,000 --> 00:04:25,000
 It is a contrived state. The human body, genes, and

72
00:04:25,000 --> 00:04:25,000
 hormones are all part of this contrived state of existence

73
00:04:25,000 --> 00:04:25,000
 that we have built up,

74
00:04:25,000 --> 00:04:29,410
 that we have cultivated, as a part of what scientists call

75
00:04:29,410 --> 00:04:31,000
 natural selection.

76
00:04:31,000 --> 00:04:36,170
 Emotions like attraction and desire are simple habits, but

77
00:04:36,170 --> 00:04:41,210
 the evolution of human society has complicated them with

78
00:04:41,210 --> 00:04:42,000
 habits of guilt,

79
00:04:42,000 --> 00:04:46,000
 self-consciousness, etc.

80
00:04:46,000 --> 00:04:49,100
 You might say that this is something that we have

81
00:04:49,100 --> 00:04:51,000
 cultivated as a species.

82
00:04:51,000 --> 00:04:55,160
 Now humans have innately in them this feeling of guilt

83
00:04:55,160 --> 00:04:57,000
 towards sexuality.

84
00:04:57,000 --> 00:05:01,000
 This is how what we call repression works.

85
00:05:01,000 --> 00:05:04,000
 It is just a more complicated habit.

86
00:05:04,000 --> 00:05:08,160
 In meditation, we sometimes have to deal with incredibly

87
00:05:08,160 --> 00:05:11,000
 complicated habits and mind states.

88
00:05:11,000 --> 00:05:14,880
 We cannot simply deal with the lust or hatred that we have

89
00:05:14,880 --> 00:05:16,000
 in the mind.

90
00:05:16,000 --> 00:05:19,220
 We have to look at how we are reacting to these emotions

91
00:05:19,220 --> 00:05:23,700
 themselves, because our habits and our minds are so complex

92
00:05:23,700 --> 00:05:24,000
.

93
00:05:24,000 --> 00:05:29,000
 Our habits are so complex that we might first get angry,

94
00:05:29,000 --> 00:05:33,130
 and then we might be angry that anger has arisen, or afraid

95
00:05:33,130 --> 00:05:35,000
 of it, or depressed by it,

96
00:05:35,000 --> 00:05:38,270
 and then maybe we try to divert ourselves by finding

97
00:05:38,270 --> 00:05:43,000
 something pleasurable, and so on, and so on.

98
00:05:43,000 --> 00:05:47,300
 We have tricks and defense mechanisms, and all sorts of

99
00:05:47,300 --> 00:05:51,000
 habits that we have developed so haphazardly.

100
00:05:51,000 --> 00:05:54,240
 We have to muck through them piece by piece in our

101
00:05:54,240 --> 00:05:56,000
 meditation practice.

102
00:05:56,000 --> 00:06:00,000
 Repression is only a part of the complexity of our minds,

103
00:06:00,000 --> 00:06:04,000
 and it is really an inexact term.

104
00:06:04,000 --> 00:06:07,520
 What we should talk about is the complex chains of react

105
00:06:07,520 --> 00:06:11,000
ivity our minds are habitually inclined towards.

106
00:06:11,000 --> 00:06:15,830
 First we have ordinary lust or anger arising, and then we

107
00:06:15,830 --> 00:06:18,000
 have our responses to it.

108
00:06:18,000 --> 00:06:22,540
 We have to be able to deal skillfully with both and unravel

109
00:06:22,540 --> 00:06:24,000
 them together.

110
00:06:24,000 --> 00:06:27,950
 Not just dealing with the lust, or what one might call the

111
00:06:27,950 --> 00:06:32,000
 repression, but dealing with both together.

112
00:06:32,000 --> 00:06:36,260
 You must also deal with positive mind states, being mindful

113
00:06:36,260 --> 00:06:39,000
 of everything piece by piece.

114
00:06:39,000 --> 00:06:43,300
 This is a very important topic to understand from a med

115
00:06:43,300 --> 00:06:45,000
itative point of view,

116
00:06:45,000 --> 00:06:49,000
 to understand that it is not as simple as just repression.

117
00:06:49,000 --> 00:06:54,220
 It is much more complicated, and rather than trying to say,

118
00:06:54,220 --> 00:06:59,000
 "I am repressing," or, "I have this tendency,"

119
00:06:59,000 --> 00:07:03,000
 we should look at the experiences moment to moment.

120
00:07:03,000 --> 00:07:07,520
 Meditation is, in one sense, simply becoming increasingly

121
00:07:07,520 --> 00:07:12,000
 skillful at facing the convolutions and knots in the mind

122
00:07:12,000 --> 00:07:15,000
 without perpetuating them.

123
00:07:15,000 --> 00:07:18,560
 When the Buddha was asked who could untangle the inner t

124
00:07:18,560 --> 00:07:19,000
angle,

125
00:07:19,000 --> 00:07:23,620
 the complex reactions we have been discussing, and the

126
00:07:23,620 --> 00:07:25,000
 outer tangle,

127
00:07:25,000 --> 00:07:29,850
 the objects that trigger our reactions, he said that a wise

128
00:07:29,850 --> 00:07:33,000
 and energetic bhikkhu will be able to untangle the tangle

129
00:07:33,000 --> 00:07:39,000
 by means of morality, concentration, and wisdom.

130
00:07:39,000 --> 00:07:43,920
 Briefly, from a meditative point of view, morality is

131
00:07:43,920 --> 00:07:48,000
 developed by bringing the mind back to the object.

132
00:07:48,000 --> 00:07:52,000
 Concentration is the focus that arises when you do that,

133
00:07:52,000 --> 00:07:56,000
 and wisdom is what you see when your mind is in focus.

134
00:07:56,000 --> 00:08:00,910
 The meditation practice, then, is the development of

135
00:08:00,910 --> 00:08:04,000
 morality, concentration, and wisdom

136
00:08:04,000 --> 00:08:09,000
 that slowly allows you to simplify and purify your habits,

137
00:08:09,000 --> 00:08:13,680
 bringing your mind to experience reality as it is, instead

138
00:08:13,680 --> 00:08:17,000
 of reacting and then reacting to your reactions.

