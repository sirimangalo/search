1
00:00:00,000 --> 00:00:04,280
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,280 --> 00:00:07,040
 Today we continue with verse number

3
00:00:07,040 --> 00:00:09,280
 15 which goes as follows.

4
00:00:24,400 --> 00:00:30,970
 Which means, "Idda-sojati," here he grieves, "Beja-sojati,"

5
00:00:30,970 --> 00:00:31,600
 here after he grieves,

6
00:00:31,600 --> 00:00:37,600
 the evil doer grieves in both places.

7
00:00:37,600 --> 00:00:43,060
 "Sosochati, sohihanyati," he grieves, he is afflicted, he

8
00:00:43,060 --> 00:00:45,360
 is destroyed.

9
00:00:50,240 --> 00:00:55,600
 Seeing the evil or the defilement of his own actions.

10
00:00:55,600 --> 00:01:03,590
 This is the teaching on negative, on evil karma, evil needs

11
00:01:03,590 --> 00:01:03,760
.

12
00:01:03,760 --> 00:01:07,640
 We have two verses here, the next verse is in regards to a

13
00:01:07,640 --> 00:01:08,400
 different story.

14
00:01:08,400 --> 00:01:11,240
 So the story with this verse said, "In the time of the

15
00:01:11,240 --> 00:01:13,040
 Buddha there was a pig butcher

16
00:01:13,040 --> 00:01:16,460
 who lived right next to Veda-vandha where the Buddha was

17
00:01:16,460 --> 00:01:16,560
 staying in Rajagah."

18
00:01:17,600 --> 00:01:20,080
 And when the monks would go on Amsharand they would walk

19
00:01:20,080 --> 00:01:21,520
 past and they would hear the screams

20
00:01:21,520 --> 00:01:25,480
 or they would even see the pigs being killed. And the way

21
00:01:25,480 --> 00:01:29,040
 the commentary explains the killing went on

22
00:01:29,040 --> 00:01:34,740
 is this man, he would tie the pig to a post so it couldn't

23
00:01:34,740 --> 00:01:36,800
 move and then beat it with a club

24
00:01:36,800 --> 00:01:40,400
 to make its skin all tender, its flesh tender so it was

25
00:01:40,400 --> 00:01:43,680
 still alive. And then take boiling hot water

26
00:01:43,680 --> 00:01:48,440
 and pour it down the pig's throat and have all the feces

27
00:01:48,440 --> 00:01:52,400
 and undigested food pour out the bottom.

28
00:01:52,400 --> 00:01:55,720
 And he would continue to pour boiling water in until the

29
00:01:55,720 --> 00:01:57,520
 water that came out was clean.

30
00:01:57,520 --> 00:02:02,670
 And meaning that there was no feces left inside. Then he

31
00:02:02,670 --> 00:02:05,760
 would pour the rest of the water

32
00:02:05,760 --> 00:02:09,540
 over the pig's skin to peel off the outer skin and use the

33
00:02:09,540 --> 00:02:12,160
 torch to burn off all the pig's hair.

34
00:02:13,280 --> 00:02:16,040
 And then he would cut off the pig's head and then it would

35
00:02:16,040 --> 00:02:19,200
 die. And he would collect the blood from

36
00:02:19,200 --> 00:02:23,060
 the pig's neck and roast the pig in its own blood, eat as

37
00:02:23,060 --> 00:02:25,760
 much as he could with his family and sell

38
00:02:25,760 --> 00:02:29,780
 the rest. And they say for 55 years he lived his life like

39
00:02:29,780 --> 00:02:32,400
 this. And one day the monks were walking

40
00:02:32,400 --> 00:02:38,570
 by 55 years later and they heard, they saw that today all

41
00:02:38,570 --> 00:02:41,520
 of the doors of the house were closed up.

42
00:02:42,400 --> 00:02:49,050
 And they could still hear the screams of pigs and grunting

43
00:02:49,050 --> 00:02:52,400
 noises and scuffling. And so they assumed

44
00:02:52,400 --> 00:02:54,430
 that there must be something great going on. And so they

45
00:02:54,430 --> 00:02:56,240
 started talking about this, how amazing it

46
00:02:56,240 --> 00:03:01,440
 was that for 55 years this man had never learned the weight

47
00:03:01,440 --> 00:03:05,280
 of the evil of his deeds and had never

48
00:03:05,280 --> 00:03:08,480
 even come to listen to the Buddha's teaching or come to pay

49
00:03:08,480 --> 00:03:10,320
 respect to the Buddha at all.

50
00:03:10,640 --> 00:03:14,900
 And the Buddha heard them talking and asked them, "What are

51
00:03:14,900 --> 00:03:18,240
 you talking about?" They explained what

52
00:03:18,240 --> 00:03:22,420
 it was. And the Buddha said, "They're not." And they said,

53
00:03:22,420 --> 00:03:24,720
 "Oh, and today we see that he's,

54
00:03:24,720 --> 00:03:28,870
 for seven days now." He said, "Seven days later we see that

55
00:03:28,870 --> 00:03:31,440
 they have been closed up for seven days

56
00:03:31,440 --> 00:03:34,370
 so they must be working on an even bigger slaughter or a

57
00:03:34,370 --> 00:03:36,400
 special slaughter of some sort."

58
00:03:37,280 --> 00:03:40,180
 And the Buddha said, "No, that's not really what's going on

59
00:03:40,180 --> 00:03:43,600
 at all." Seven days ago it turns out that

60
00:03:43,600 --> 00:03:48,030
 the cunda, this pork butcher, became very ill and his aff

61
00:03:48,030 --> 00:03:50,480
liction went to his head and he became

62
00:03:50,480 --> 00:03:54,980
 quite deranged. And as a result of the sickness and the der

63
00:03:54,980 --> 00:03:57,280
angement he started walking around

64
00:03:57,280 --> 00:04:00,090
 or crawling around the house on his hands and knees and

65
00:04:00,090 --> 00:04:01,280
 grunting like a pig,

66
00:04:01,280 --> 00:04:04,580
 squeezing like a pig. And this is what they heard. And the

67
00:04:04,580 --> 00:04:06,400
 people in the house would try to restrain

68
00:04:06,400 --> 00:04:09,480
 him but they couldn't do it. Also because, as the

69
00:04:09,480 --> 00:04:12,400
 commentary said, he began to see his future.

70
00:04:12,400 --> 00:04:17,810
 And this brought him great derangement on his deathbed. The

71
00:04:17,810 --> 00:04:19,760
 fear and the concentration of the

72
00:04:19,760 --> 00:04:24,040
 mind at that point allowed him to see his future and he

73
00:04:24,040 --> 00:04:28,400
 began to, it drove him crazy.

74
00:04:28,400 --> 00:04:31,560
 And as a result all he could think about was the killing of

75
00:04:31,560 --> 00:04:33,840
 the pigs and as a result

76
00:04:34,800 --> 00:04:38,140
 dwelt that way for seven days. And then on that day, on the

77
00:04:38,140 --> 00:04:40,560
 seventh day, it was over and he died

78
00:04:40,560 --> 00:04:43,640
 and went to hell. So the Buddha explained what was really

79
00:04:43,640 --> 00:04:45,520
 going on. And then he said to the monks,

80
00:04:45,520 --> 00:04:48,610
 "This is how it goes. It's not just in the next life that a

81
00:04:48,610 --> 00:04:51,440
 person suffers, a person who does

82
00:04:51,440 --> 00:04:54,050
 eva-dheeds, a person who does eva-dheed suffers both in

83
00:04:54,050 --> 00:04:55,360
 this life and the next."

84
00:04:57,600 --> 00:05:02,380
 So how this relates to our practice, I think, should be

85
00:05:02,380 --> 00:05:06,720
 quite clear. And it becomes quite clear

86
00:05:06,720 --> 00:05:09,420
 when a person undertakes practice because for the first

87
00:05:09,420 --> 00:05:10,800
 time they're able to realize

88
00:05:10,800 --> 00:05:15,160
 the full magnitude of their deeds. I think, or it's clear

89
00:05:15,160 --> 00:05:18,400
 that people who don't practice meditation

90
00:05:18,400 --> 00:05:22,190
 are unable to understand the law of karma and they think of

91
00:05:22,190 --> 00:05:23,760
 it as some kind of belief

92
00:05:24,960 --> 00:05:28,130
 or some kind of theory. And they don't really understand

93
00:05:28,130 --> 00:05:29,680
 because when they do eva-dheeds they

94
00:05:29,680 --> 00:05:32,520
 look around and they don't see anything happening. It's not

95
00:05:32,520 --> 00:05:34,480
 like, "I killed someone," or "I killed an

96
00:05:34,480 --> 00:05:36,620
 animal and now I'm going to be killed." They look around

97
00:05:36,620 --> 00:05:38,240
 and they're not killed. So they think,

98
00:05:38,240 --> 00:05:40,730
 "Wow, then there's nothing wrong with doing eva-dheeds."

99
00:05:40,730 --> 00:05:42,720
 Because they can't see really what is the result

100
00:05:42,720 --> 00:05:46,690
 of a good or an eva-dheed. When a person comes to practice

101
00:05:46,690 --> 00:05:50,000
 meditation, they're watching every moment

102
00:05:50,000 --> 00:05:52,210
 and they're seeing quite clearly what are the results of

103
00:05:52,210 --> 00:05:54,080
 their deeds. When they cling to something,

104
00:05:54,080 --> 00:05:56,930
 what is the result? When they are angry or upset about

105
00:05:56,930 --> 00:05:58,960
 something, what is the result? How does it

106
00:05:58,960 --> 00:06:02,110
 affect their body? How does it affect their mind? How does

107
00:06:02,110 --> 00:06:05,280
 it affect their clarity and their character?

108
00:06:05,280 --> 00:06:10,670
 And so as a result, the law of karma becomes quite clear

109
00:06:10,670 --> 00:06:12,560
 and quite evident.

110
00:06:12,560 --> 00:06:18,410
 But for evildoers, for people who are engaged in great evil

111
00:06:18,410 --> 00:06:20,000
, it's often not

112
00:06:20,000 --> 00:06:24,240
 clear. And it can often be the fact that because of their

113
00:06:24,240 --> 00:06:27,520
 position and because of good past deeds

114
00:06:27,520 --> 00:06:32,360
 and their relative stability of mind, they're unable to see

115
00:06:32,360 --> 00:06:34,720
 the small seed growing inside.

116
00:06:34,720 --> 00:06:38,370
 And moreover, they're often able, through the power of the

117
00:06:38,370 --> 00:06:41,520
 mind, to push it away and to avoid

118
00:06:41,520 --> 00:06:44,650
 looking at it, to avoid thinking about it, so that they

119
00:06:44,650 --> 00:06:47,200
 aren't aware of the evil deeds until it becomes

120
00:06:47,200 --> 00:06:50,280
 so great and so gross that they can no longer hide it,

121
00:06:50,280 --> 00:06:52,720
 especially when they become sick and

122
00:06:52,720 --> 00:06:55,610
 are on their deathbed and their power of mind becomes much

123
00:06:55,610 --> 00:06:58,800
 weaker. An ordinary person spends

124
00:06:58,800 --> 00:07:02,800
 most of their time covering up their deeds. Now, it does

125
00:07:02,800 --> 00:07:04,800
 happen that good people do bad

126
00:07:04,800 --> 00:07:08,950
 deeds and do feel guilty and feel remorseful and are able

127
00:07:08,950 --> 00:07:11,280
 to see how it's affected them.

128
00:07:11,280 --> 00:07:14,360
 When a person does or if a person starts going down a wrong

129
00:07:14,360 --> 00:07:16,320
 path and they start to realize the

130
00:07:16,320 --> 00:07:19,450
 nature of their mind, they burst out at someone and they

131
00:07:19,450 --> 00:07:21,520
 realize that they have a lot of anger

132
00:07:21,520 --> 00:07:25,010
 inside. Then they might feel guilty and that there's

133
00:07:25,010 --> 00:07:27,360
 something that should be done. And as

134
00:07:27,360 --> 00:07:31,100
 a result, they will begin to refrain from that in the

135
00:07:31,100 --> 00:07:36,080
 future. But it can often be the case that,

136
00:07:36,080 --> 00:07:39,300
 as in the case of Chunda, the realization only comes when

137
00:07:39,300 --> 00:07:41,760
 it's too late. So people often ask,

138
00:07:41,760 --> 00:07:45,250
 "Well, if there is this law of karma, why do we see good

139
00:07:45,250 --> 00:07:48,480
 people suffering and bad people prospering?"

140
00:07:48,480 --> 00:07:52,690
 But when you practice meditation, when you actually look

141
00:07:52,690 --> 00:07:55,440
 and examine what's going on inside,

142
00:07:55,440 --> 00:07:57,840
 you'll see that it's true that good people might suffer,

143
00:07:57,840 --> 00:07:59,440
 but their suffering isn't because of their

144
00:07:59,440 --> 00:08:02,870
 goodness. And it's true that evil people may prosper, but

145
00:08:02,870 --> 00:08:04,720
 their prospering is not because of

146
00:08:04,720 --> 00:08:08,320
 their evil. It can't be. And you can see this by watching

147
00:08:08,320 --> 00:08:10,960
 clearly what, as I said, what happens when

148
00:08:11,520 --> 00:08:14,650
 these mind states arise. What do they lead to? You can see

149
00:08:14,650 --> 00:08:17,040
 that a good state can never lead to

150
00:08:17,040 --> 00:08:21,830
 suffering and an evil state can never lead to good or to

151
00:08:21,830 --> 00:08:25,120
 happiness or to prosperity.

152
00:08:25,120 --> 00:08:30,360
 It's only generally in this life is because of our position

153
00:08:30,360 --> 00:08:34,480
 and because of the static nature of the

154
00:08:34,480 --> 00:08:38,890
 material body and of the material world. So a person might

155
00:08:38,890 --> 00:08:41,200
 be born into a position of power

156
00:08:41,200 --> 00:08:44,240
 or a position of wealth, and as a result they can do many

157
00:08:44,240 --> 00:08:46,400
 evil deeds without great repercussions,

158
00:08:46,400 --> 00:08:50,870
 except in the mind, which of course is much more dynamic

159
00:08:50,870 --> 00:08:54,640
 and much quicker to pick up on the changes.

160
00:08:54,640 --> 00:08:57,240
 But eventually, even the physical will begin to pick it up.

161
00:08:57,240 --> 00:08:59,200
 You will lose your friends, you will

162
00:08:59,200 --> 00:09:03,000
 gain corrupt friends. Your whole environment will slowly

163
00:09:03,000 --> 00:09:05,280
 change, but it changes a lot slower.

164
00:09:05,920 --> 00:09:09,800
 And so the results can be a lot slower coming and quite a

165
00:09:09,800 --> 00:09:11,360
 bit less evident.

166
00:09:11,360 --> 00:09:14,860
 Now at the moment of death, all of that changes because the

167
00:09:14,860 --> 00:09:18,080
 physical is removed from the equation.

168
00:09:18,080 --> 00:09:21,360
 The mind is now only depending on the mind. There is no

169
00:09:21,360 --> 00:09:23,280
 going back to seeing, hearing,

170
00:09:23,280 --> 00:09:25,950
 smelling, tasting. There's nothing based on the body. There

171
00:09:25,950 --> 00:09:28,240
's only the mental activity.

172
00:09:28,880 --> 00:09:34,140
 So it's a repeated experience of the deeds that one has

173
00:09:34,140 --> 00:09:37,360
 done, or memories, or whatever is going

174
00:09:37,360 --> 00:09:40,080
 on in the mind. So if one has a mind that is full of

175
00:09:40,080 --> 00:09:43,120
 corruption, then this will continue repeatedly,

176
00:09:43,120 --> 00:09:45,710
 arise in the mind, thoughts of the evil deeds that we've

177
00:09:45,710 --> 00:09:48,080
 done, or memories of the evil deeds,

178
00:09:48,080 --> 00:09:51,650
 or thoughts about the future, or a feeling of where you're

179
00:09:51,650 --> 00:09:54,480
 being pulled, the future that you're

180
00:09:54,480 --> 00:09:57,000
 developing for yourself. And this is clear, many people

181
00:09:57,000 --> 00:09:58,800
 actually experience this on their death

182
00:09:58,800 --> 00:10:01,740
 end, can relate this. Some people actually leave their

183
00:10:01,740 --> 00:10:04,320
 bodies and can see their bodies, or can see

184
00:10:04,320 --> 00:10:07,260
 their loved ones, or so on, and find themselves floating

185
00:10:07,260 --> 00:10:09,440
 away. If they have a near-death experience,

186
00:10:09,440 --> 00:10:12,020
 they'll come back and they'll be able to relate good things

187
00:10:12,020 --> 00:10:13,600
 or bad things that they saw, that

188
00:10:13,600 --> 00:10:16,690
 happened to them. And it will be based on whatever's being

189
00:10:16,690 --> 00:10:18,800
 clung through in their mind, whatever their

190
00:10:18,800 --> 00:10:23,320
 mind is attaching importance to at that time. So if a

191
00:10:23,320 --> 00:10:26,480
 person is greatly engaged in evil, evil

192
00:10:26,480 --> 00:10:29,710
 deeds, then they will be born in a place that is full of

193
00:10:29,710 --> 00:10:32,720
 suffering because of their corrupt

194
00:10:32,720 --> 00:10:36,140
 state of mind, as the Buddha said. When they see the evil,

195
00:10:36,140 --> 00:10:40,400
 they will finally realize. So in some

196
00:10:40,400 --> 00:10:44,290
 sense, it's much better to actually see the evil and to

197
00:10:44,290 --> 00:10:47,280
 feel sorrow. We had a discussion about this

198
00:10:47,280 --> 00:10:50,200
 as to whether this is what the Buddha meant by "heary" and

199
00:10:50,200 --> 00:10:52,400
 "otapa." And it isn't really what he meant

200
00:10:52,400 --> 00:10:56,560
 by "heary" and "otapa" to feel shame or to feel guilt. But

201
00:10:56,560 --> 00:10:59,120
 to see that you've done bad things can

202
00:10:59,120 --> 00:11:02,980
 be a great waking up and can lead you to feel shame and to

203
00:11:02,980 --> 00:11:05,600
 feel "heary" and "otapa," meaning in the

204
00:11:05,600 --> 00:11:09,650
 future, when the opportunity presents itself to do an evil

205
00:11:09,650 --> 00:11:12,400
 deed, he will shy away from it. He will

206
00:11:13,600 --> 00:11:17,900
 recoil from it. The mind will naturally incline against it

207
00:11:17,900 --> 00:11:22,080
 because of seeing the evil of it and

208
00:11:22,080 --> 00:11:25,140
 seeing the evil nature of it. And this is what we gain very

209
00:11:25,140 --> 00:11:26,960
 much from meditation, from just

210
00:11:26,960 --> 00:11:30,140
 introspection. A person just begins to come to meditate. In

211
00:11:30,140 --> 00:11:32,400
 the very beginning, they will see

212
00:11:32,400 --> 00:11:35,960
 all of what they have carried with them and built up over

213
00:11:35,960 --> 00:11:38,240
 the years through not meditating.

214
00:11:38,240 --> 00:11:42,670
 They'll see all of the habits and tendencies, often a big

215
00:11:42,670 --> 00:11:45,120
 wave of defilement, of corruption.

216
00:11:45,120 --> 00:11:49,060
 At times it can be great anger and hatred. At times it can

217
00:11:49,060 --> 00:11:52,880
 be great lust and need and wanting.

218
00:11:52,880 --> 00:12:01,980
 So they're able to realize, as it says here, they will g

219
00:12:01,980 --> 00:12:03,840
rieve in the beginning and they will feel

220
00:12:03,840 --> 00:12:08,270
 great suffering in the beginning. But eventually it will be

221
00:12:08,270 --> 00:12:10,400
 a cause for them to change their ways.

222
00:12:10,400 --> 00:12:12,750
 And in the future they won't want to do these things

223
00:12:12,750 --> 00:12:15,680
 because they know how much it hurts and

224
00:12:15,680 --> 00:12:20,560
 affects their mind. So this is the story of "Junda the Pork

225
00:12:20,560 --> 00:12:22,960
 Butcher" and the Buddha's teaching on

226
00:12:22,960 --> 00:12:27,320
 the suffering that comes from doing evil deeds. So thanks

227
00:12:27,320 --> 00:12:28,240
 for tuning in.

228
00:12:28,240 --> 00:12:31,400
 This has been another episode of our study of the Dhammap

229
00:12:31,400 --> 00:12:31,680
ada.

