1
00:00:00,000 --> 00:00:05,850
 Welcome everyone to a special edition of Monk Radio here in

2
00:00:05,850 --> 00:00:07,000
 Thailand.

3
00:00:07,000 --> 00:00:14,000
 It doesn't look like much but we're in my uncle's office

4
00:00:14,000 --> 00:00:21,170
 and he's actually moving right now so everything's all a

5
00:00:21,170 --> 00:00:23,000
 bit disorganized.

6
00:00:23,000 --> 00:00:34,000
 We managed to secure some lighting and computer and webcam

7
00:00:34,000 --> 00:00:34,000
 and so on

8
00:00:34,000 --> 00:00:38,000
 and so we put together a special session here.

9
00:00:38,000 --> 00:00:42,710
 So what we're going to do tonight, we probably will not

10
00:00:42,710 --> 00:00:44,000
 take so much time.

11
00:00:44,000 --> 00:00:47,000
 I appreciate that some people have come up.

12
00:00:47,000 --> 00:00:50,350
 But we're just going to take this opportunity to talk a

13
00:00:50,350 --> 00:00:52,000
 little bit about what we've been through.

14
00:00:52,000 --> 00:00:57,000
 What we've done in the past couple of weeks.

15
00:00:57,000 --> 00:01:02,510
 What we're doing and where we're going in the next little

16
00:01:02,510 --> 00:01:03,000
 while.

17
00:01:03,000 --> 00:01:08,890
 Maybe answer a few questions if people have any pertinent

18
00:01:08,890 --> 00:01:10,000
 questions.

19
00:01:10,000 --> 00:01:15,000
 Maybe do a little bit of group meditation.

20
00:01:15,000 --> 00:01:18,000
 So first things first, let's go through announcements.

21
00:01:18,000 --> 00:01:28,000
 The first announcement is that we're in Thailand now

22
00:01:28,000 --> 00:01:40,370
 and we've been spending the past 10 days with my teacher,

23
00:01:40,370 --> 00:01:45,000
 our head teacher, Chan Tong.

24
00:01:45,000 --> 00:01:49,000
 We're doing a meditation course with him.

25
00:01:49,000 --> 00:01:57,310
 So far less put through a 10 day advanced course, a review

26
00:01:57,310 --> 00:02:04,000
 course with Chan Tong.

27
00:02:04,000 --> 00:02:08,000
 It was really quite special actually.

28
00:02:08,000 --> 00:02:11,370
 I had contacted them earlier to make sure that they had

29
00:02:11,370 --> 00:02:14,000
 room for us and the theater.

30
00:02:14,000 --> 00:02:17,000
 And it was my birthday on May 9th.

31
00:02:17,000 --> 00:02:23,000
 So we arrived on the 8th in the evening in Chan Tong.

32
00:02:23,000 --> 00:02:27,000
 And it turns out they didn't have room for us.

33
00:02:27,000 --> 00:02:32,000
 And Ajahn Tong wasn't doing any reporting.

34
00:02:32,000 --> 00:02:36,000
 So we went to see Ajahn Tong.

35
00:02:36,000 --> 00:02:43,000
 We managed to get almost enough rooms.

36
00:02:43,000 --> 00:02:45,000
 And then we went to see Ajahn Tong.

37
00:02:45,000 --> 00:02:48,190
 And it turns out he had stopped reporting because he had

38
00:02:48,190 --> 00:02:49,000
 been sick

39
00:02:49,000 --> 00:02:56,180
 and recovering from a condition called bladder stones or

40
00:02:56,180 --> 00:02:57,000
 something.

41
00:02:57,000 --> 00:03:01,000
 Call stones? I don't know if you follow that.

42
00:03:01,000 --> 00:03:06,720
 He wasn't doing reporting but he agreed to give us a

43
00:03:06,720 --> 00:03:09,000
 special exception

44
00:03:09,000 --> 00:03:12,000
 to meet with us once a day.

45
00:03:12,000 --> 00:03:17,110
 So we actually got to do one on one practice with Ajahn

46
00:03:17,110 --> 00:03:18,000
 Tong.

47
00:03:18,000 --> 00:03:21,000
 And finally we got enough rooms.

48
00:03:21,000 --> 00:03:23,000
 And that was great.

49
00:03:23,000 --> 00:03:27,520
 From my point of view it was a great experience the past 10

50
00:03:27,520 --> 00:03:28,000
 days.

51
00:03:28,000 --> 00:03:31,470
 Not just for myself but for all of us to have this

52
00:03:31,470 --> 00:03:32,000
 opportunity

53
00:03:32,000 --> 00:03:36,000
 to spend time and to hear his words.

54
00:03:36,000 --> 00:03:43,000
 And even though it was through translation for most people.

55
00:03:43,000 --> 00:03:48,000
 I think it was a real opportunity to learn some new things

56
00:03:48,000 --> 00:03:53,000
 and to get ourselves focused back on the practice.

57
00:03:53,000 --> 00:03:55,730
 For myself the biggest problem of course was that the past

58
00:03:55,730 --> 00:03:56,000
 month

59
00:03:56,000 --> 00:04:00,000
 has been quite hectic with ordinations.

60
00:04:00,000 --> 00:04:02,000
 So we had to answer it.

61
00:04:02,000 --> 00:04:05,000
 Should have been the first to know this.

62
00:04:05,000 --> 00:04:08,370
 With ordinations, with visas, with building, with

63
00:04:08,370 --> 00:04:13,000
 everything that has been going on the past month.

64
00:04:13,000 --> 00:04:17,580
 It was quite, on the one hand quite difficult to get back

65
00:04:17,580 --> 00:04:18,000
 into it

66
00:04:18,000 --> 00:04:23,000
 but on the other hand it was comforting to come back to

67
00:04:23,000 --> 00:04:23,000
 something

68
00:04:23,000 --> 00:04:28,000
 that makes more sense and is much more pleasant

69
00:04:28,000 --> 00:04:35,390
 or much more conducive of leading towards happiness and

70
00:04:35,390 --> 00:04:36,000
 peace

71
00:04:36,000 --> 00:04:39,000
 and some of the things that we were involved in.

72
00:04:39,000 --> 00:04:48,330
 So it was quite a reminder and something that really for

73
00:04:48,330 --> 00:04:49,000
 myself

74
00:04:49,000 --> 00:04:57,050
 sort of to stabilize things and refocus back on the

75
00:04:57,050 --> 00:05:00,000
 practice.

76
00:05:00,000 --> 00:05:04,800
 The next announcement is that we have welcomed to our two

77
00:05:04,800 --> 00:05:06,000
 new monastics.

78
00:05:06,000 --> 00:05:10,240
 Many of you have been following and are aware that they

79
00:05:10,240 --> 00:05:12,000
 were planning to ordain.

80
00:05:12,000 --> 00:05:16,000
 Some of you have even seen the ordination online.

81
00:05:16,000 --> 00:05:24,080
 So now our monk radio has multiplied, has doubled, four

82
00:05:24,080 --> 00:05:30,000
 monks or four monastics.

83
00:05:30,000 --> 00:05:35,260
 So this Yasa has ordained as a Samanera and Zumeyada has

84
00:05:35,260 --> 00:05:37,000
 ordained as a Samaneri.

85
00:05:37,000 --> 00:05:40,000
 It's the same word, it just means novice.

86
00:05:40,000 --> 00:05:45,140
 It means one who is dependent on the beak of the beak of

87
00:05:45,140 --> 00:05:46,000
 the beak of the.

88
00:05:46,000 --> 00:05:51,360
 So it is undertaken to cultivate the practice of the beak

89
00:05:51,360 --> 00:05:54,000
 of the beak of the.

90
00:05:54,000 --> 00:05:57,000
 So they are novices now.

91
00:05:57,000 --> 00:06:02,000
 Eventually we will get monks together to do the ordinations

92
00:06:02,000 --> 00:06:06,000
 of the survival of the monk.

93
00:06:06,000 --> 00:06:18,820
 The final announcement for me is that today, after this ten

94
00:06:18,820 --> 00:06:20,000
 day course,

95
00:06:20,000 --> 00:06:25,280
 we came into Chiang Mai to do some errands or to meet with

96
00:06:25,280 --> 00:06:26,000
 people.

97
00:06:26,000 --> 00:06:31,000
 I kind of got the idea to go back to this place I had been

98
00:06:31,000 --> 00:06:32,000
 in north of Chiang Mai,

99
00:06:32,000 --> 00:06:34,900
 this forest monastery where we tried to start a forest

100
00:06:34,900 --> 00:06:36,000
 meditation center.

101
00:06:36,000 --> 00:06:40,000
 Yes, I first came to practice.

102
00:06:40,000 --> 00:06:45,000
 And now I ordained as a Mh here.

103
00:06:45,000 --> 00:06:48,700
 Right, it's the place where you would use the eight percent

104
00:06:48,700 --> 00:06:49,000
.

105
00:06:49,000 --> 00:06:52,160
 And we got back there and of course we had problems with

106
00:06:52,160 --> 00:06:53,000
 the head monk,

107
00:06:53,000 --> 00:06:56,000
 as in general we had problems with head monks.

108
00:06:56,000 --> 00:06:59,000
 And it turns out that there is a new monk there,

109
00:06:59,000 --> 00:07:02,420
 and it was from Jumtong, a really great monk, and really

110
00:07:02,420 --> 00:07:03,000
 wants help.

111
00:07:03,000 --> 00:07:07,370
 So the short story is that in the future we might be doing

112
00:07:07,370 --> 00:07:09,000
 courses in Thailand again.

113
00:07:09,000 --> 00:07:13,480
 So there might be an opportunity to do some practice here

114
00:07:13,480 --> 00:07:14,000
 again.

115
00:07:14,000 --> 00:07:20,000
 So I might be back to Thailand sometime in the future.

116
00:07:20,000 --> 00:07:24,530
 So that's all my announcements. Is there anything else that

117
00:07:24,530 --> 00:07:28,000
 anyone can think of this to be said?

118
00:07:28,000 --> 00:07:31,410
 Well, we're leaving tomorrow. Tomorrow we're going to

119
00:07:31,410 --> 00:07:32,000
 Bangkok.

120
00:07:32,000 --> 00:07:36,000
 Today we went to this forest monastery.

121
00:07:36,000 --> 00:07:40,110
 Tomorrow we're going up to Doyshutet, which is the mountain

122
00:07:40,110 --> 00:07:41,000
 about Chiang Mai.

123
00:07:41,000 --> 00:07:43,450
 And then in the evening we're leaving for Bangkok and we're

124
00:07:43,450 --> 00:07:47,000
 going to do some big getting visas

125
00:07:47,000 --> 00:07:50,000
 and going to do a little bit of teaching in Bangkok.

126
00:07:50,000 --> 00:07:53,000
 And then back to Sri Lanka.

127
00:07:53,000 --> 00:08:01,000
 While I will stay here for a week or so, almost two weeks,

128
00:08:01,000 --> 00:08:09,000
 until we finally go back to Sri Lanka on the 31st of May.

129
00:08:09,000 --> 00:08:16,000
 Okay, so that's our announcements. That's our trip.

130
00:08:16,000 --> 00:08:18,000
 The trip has been great so far.

131
00:08:18,000 --> 00:08:22,150
 So until the next video I hope to have everyone say

132
00:08:22,150 --> 00:08:26,000
 something about the practice of the forest.

