1
00:00:00,000 --> 00:00:05,760
 Hello and welcome back to our study of the Dhammapada.

2
00:00:05,760 --> 00:00:10,720
 Today we continue on with verse number 94 which reads as

3
00:00:10,720 --> 00:00:12,400
 follows.

4
00:00:12,400 --> 00:00:22,310
 Yasindhriyani samatangatani asayatah saratina sudantah bhih

5
00:00:22,310 --> 00:00:30,400
inamanasa anasavasa devapitasa bhihyantitadino

6
00:00:30,400 --> 00:00:38,200
 which means whose faculties have become tranquil samatangat

7
00:00:38,200 --> 00:00:42,400
ani who have gone to tranquility

8
00:00:42,400 --> 00:00:50,400
 like a horse tamed by a saratini, a charioteer.

9
00:00:52,400 --> 00:00:57,850
 Bihinamanasa anasavasa, a mind that is dedicated, for one

10
00:00:57,850 --> 00:01:02,400
 whose mind is dedicated, who is without asava.

11
00:01:02,400 --> 00:01:06,800
 Again we have this taint without these outflowings or these

12
00:01:06,800 --> 00:01:10,400
 streams of defilement or of attachment.

13
00:01:10,400 --> 00:01:20,060
 Devapitasa bhihyantitadino, even angels are pleased or hold

14
00:01:20,060 --> 00:01:24,400
 dear such a one basically.

15
00:01:24,400 --> 00:01:30,400
 Even the angels find such a one to be dear.

16
00:01:30,400 --> 00:01:35,920
 It's quite a poetic verse. We've got the nice inventory of

17
00:01:35,920 --> 00:01:38,400
 the horse trained by the charioteer,

18
00:01:38,400 --> 00:01:42,400
 a person who trains their faculties.

19
00:01:42,400 --> 00:01:50,270
 So this was taught, we are told, in regards to Mahakajayana

20
00:01:50,270 --> 00:01:50,400
.

21
00:01:50,400 --> 00:01:58,400
 Mahakajayana, one of the 80 great disciples of the Buddha.

22
00:01:58,400 --> 00:02:04,340
 He would have lived much of his life in Awanti, we are told

23
00:02:04,340 --> 00:02:06,400
, which is not significant in this story,

24
00:02:06,400 --> 00:02:09,970
 but it's also significant if we want to know who Kachayana

25
00:02:09,970 --> 00:02:10,400
 was.

26
00:02:10,400 --> 00:02:20,660
 Kachayana is supposed to be the monk who founded the study

27
00:02:20,660 --> 00:02:22,400
 of Pali.

28
00:02:22,400 --> 00:02:26,690
 Because when he went to this country of Awanti, which is, I

29
00:02:26,690 --> 00:02:29,960
 understand, outside of the area where they spoke the Mag

30
00:02:29,960 --> 00:02:30,400
adan,

31
00:02:30,400 --> 00:02:37,380
 or what we now often refer to as Pali language, he had to

32
00:02:37,380 --> 00:02:41,850
 translate the teachings, because people didn't understand

33
00:02:41,850 --> 00:02:42,400
 the teachings.

34
00:02:42,400 --> 00:02:45,740
 So he had to first translate them into other people's

35
00:02:45,740 --> 00:02:50,400
 language, but then he had to also teach people Pali

36
00:02:50,400 --> 00:02:53,750
 so they could learn the Buddha's words for themselves, so

37
00:02:53,750 --> 00:02:56,400
 they could help with translations,

38
00:02:56,400 --> 00:03:00,090
 rather than having to translate so they could read the

39
00:03:00,090 --> 00:03:01,400
 original text.

40
00:03:01,400 --> 00:03:05,400
 So we have what is now called Kachayana, which is a grammar

41
00:03:05,400 --> 00:03:07,400
, it's dakachayana,

42
00:03:07,400 --> 00:03:10,280
 which means the book that's supposed to have been written

43
00:03:10,280 --> 00:03:13,770
 by or been handed down by the tradition of Mahakajayana

44
00:03:13,770 --> 00:03:15,400
 himself.

45
00:03:15,400 --> 00:03:20,920
 And it begins, "Ato akara sanyato," which is, according to

46
00:03:20,920 --> 00:03:23,400
 tradition, supposed to be the words of the Buddha.

47
00:03:23,400 --> 00:03:27,720
 The Buddha remarked upon a monk who was sitting by the bank

48
00:03:27,720 --> 00:03:28,400
 of a lake,

49
00:03:28,400 --> 00:03:37,020
 and he was thinking of the arising and ceasing of phenomena

50
00:03:37,020 --> 00:03:40,400
, which is "buddhaya bhaya."

51
00:03:40,400 --> 00:03:44,720
 And so he was repeating this to himself, and then he

52
00:03:44,720 --> 00:03:46,400
 started to doze off,

53
00:03:46,400 --> 00:03:51,540
 and he started mixing things up, and he said he was an

54
00:03:51,540 --> 00:03:53,400
 elderly fellow, I think,

55
00:03:53,400 --> 00:03:56,790
 and there was a swan out on the lake or some bird out on

56
00:03:56,790 --> 00:03:57,400
 the lake,

57
00:03:57,400 --> 00:04:01,100
 and so he started saying to himself, "ud," because it

58
00:04:01,100 --> 00:04:02,400
 sounded like what he was saying sounded like,

59
00:04:02,400 --> 00:04:06,400
 "udaka paka," which means bird on the lake.

60
00:04:06,400 --> 00:04:11,890
 So he started saying instead, "udaka paka, udaka paka,"

61
00:04:11,890 --> 00:04:13,400
 instead of "udaya bhaya,"

62
00:04:13,400 --> 00:04:17,690
 and the Buddha noticed what he was doing and kind of shook

63
00:04:17,690 --> 00:04:19,400
 his head and said,

64
00:04:19,400 --> 00:04:24,260
 "Ato akara sanyato," which means the meaning is known by

65
00:04:24,260 --> 00:04:27,400
 the agara, the letters or the words.

66
00:04:27,400 --> 00:04:31,400
 So the wording is important, is what he was saying,

67
00:04:31,400 --> 00:04:38,260
 and that's supposed to have been heard by Mahakajayana, who

68
00:04:38,260 --> 00:04:38,400
 thought,

69
00:04:38,400 --> 00:04:43,110
 "Yes, it's important that people get this language right,"

70
00:04:43,110 --> 00:04:46,300
 and so he started teaching Pali in the places where they

71
00:04:46,300 --> 00:04:50,400
 didn't speak Pali or in Avanti.

72
00:04:50,400 --> 00:04:54,500
 But what all this meant is that he was living far away from

73
00:04:54,500 --> 00:04:55,400
 the Buddha,

74
00:04:55,400 --> 00:05:01,370
 and even though he was living far away, because he was so

75
00:05:01,370 --> 00:05:04,400
 well-respected

76
00:05:04,400 --> 00:05:08,740
 and because he often apparently returned to hear the Buddha

77
00:05:08,740 --> 00:05:09,400
's words

78
00:05:09,400 --> 00:05:12,050
 and to bring them back and translate them and share them

79
00:05:12,050 --> 00:05:14,400
 with the people in Avanti,

80
00:05:14,400 --> 00:05:17,630
 he would often travel great distances just to hear the

81
00:05:17,630 --> 00:05:18,400
 Buddha talk,

82
00:05:18,400 --> 00:05:24,400
 so they would always leave a seat out for Mahakajayana,

83
00:05:24,400 --> 00:05:27,400
 in expectation that he might show up.

84
00:05:27,400 --> 00:05:32,390
 The story goes that Saka, who is this Buddhist angel,

85
00:05:32,390 --> 00:05:34,400
 supposed to be the king of the angels up in heaven,

86
00:05:34,400 --> 00:05:39,710
 kind of like a medieval Christian idea of the concept of

87
00:05:39,710 --> 00:05:42,400
 God up on a throne,

88
00:05:42,400 --> 00:05:50,400
 and he would come down and listen to the Buddha's teaching,

89
00:05:50,400 --> 00:05:52,940
 but he would also look out for the monks, and he was

90
00:05:52,940 --> 00:05:56,400
 apparently quite fond of Mahakajayana,

91
00:05:56,400 --> 00:06:02,400
 and would wait and see whether Mahakajayana was coming,

92
00:06:02,400 --> 00:06:07,000
 and so he was thinking to himself, "Oh, it would be great

93
00:06:07,000 --> 00:06:08,400
 if he showed up."

94
00:06:08,400 --> 00:06:11,550
 And suddenly he saw that Mahakajayana, one time he suddenly

95
00:06:11,550 --> 00:06:13,400
 saw that Mahakajayana had come,

96
00:06:13,400 --> 00:06:17,800
 and he immediately went over to Mahakajayana and bowed down

97
00:06:17,800 --> 00:06:21,400
 to him and grasped his feet

98
00:06:21,400 --> 00:06:28,660
 and then stood respectfully and gave him flowers and perf

99
00:06:28,660 --> 00:06:31,400
umes in some way.

100
00:06:31,400 --> 00:06:34,810
 Whatever they would do in India was a common thing to put

101
00:06:34,810 --> 00:06:38,400
 flowers on people's feet and respect for them.

102
00:06:38,400 --> 00:06:41,910
 Why? Because the feet are the lowest part of the body and

103
00:06:41,910 --> 00:06:43,400
 the head is the highest,

104
00:06:43,400 --> 00:06:46,830
 so the greatest way of paying the most ultimate symbolic

105
00:06:46,830 --> 00:06:49,400
 respect is to put your head at someone else's feet,

106
00:06:49,400 --> 00:06:53,400
 so that's become a tradition from olden times.

107
00:06:53,400 --> 00:07:00,550
 And again, as usual, we've got these troublemaker monks who

108
00:07:00,550 --> 00:07:04,380
 have nothing better to do than to criticize other people's

109
00:07:04,380 --> 00:07:05,400
 good deeds,

110
00:07:05,400 --> 00:07:12,660
 or other people's deeds, let's say, who are intent upon

111
00:07:12,660 --> 00:07:14,400
 criticism,

112
00:07:14,400 --> 00:07:19,790
 and more on criticism later, because criticism can be a

113
00:07:19,790 --> 00:07:21,400
 good thing and it's important to be critical,

114
00:07:21,400 --> 00:07:25,120
 but these guys seem to have been overcritical and this was

115
00:07:25,120 --> 00:07:27,400
 what was going on in the earlier,

116
00:07:27,400 --> 00:07:31,400
 we've had this in this, seems to be a theme in this chapter

117
00:07:31,400 --> 00:07:38,400
, because we're talking, this is the Arahantavagwaga,

118
00:07:38,400 --> 00:07:41,600
 so it's about Arahants, it's talking about people who are

119
00:07:41,600 --> 00:07:44,400
 enlightened and the theme seems to be,

120
00:07:44,400 --> 00:07:48,780
 even though other people don't get that, or don't get how

121
00:07:48,780 --> 00:07:53,400
 awesome that is, or try to discount that.

122
00:07:53,400 --> 00:07:58,380
 So these other monks were saying, what's going on with

123
00:07:58,380 --> 00:08:06,170
 these two, why is Saka paying respect only to Mahakachayana

124
00:08:06,170 --> 00:08:06,400
,

125
00:08:06,400 --> 00:08:09,490
 why doesn't he show that kind of respect to all the great

126
00:08:09,490 --> 00:08:10,400
 disciples?

127
00:08:10,400 --> 00:08:14,470
 So they're kind of criticizing Saka and not Mahakachayana,

128
00:08:14,470 --> 00:08:16,400
 but it's indirect criticism because they're saying,

129
00:08:16,400 --> 00:08:20,240
 what's so special about him? Why should he pay respect to

130
00:08:20,240 --> 00:08:21,400
 this one monk?

131
00:08:21,400 --> 00:08:27,230
 He's just another one of the 80 great disciples, what's so

132
00:08:27,230 --> 00:08:28,400
 special?

133
00:08:28,400 --> 00:08:34,160
 And the Buddha heard what they were saying, silenced them

134
00:08:34,160 --> 00:08:37,950
 by saying, monks, those monks who like my son, Kachayana

135
00:08:37,950 --> 00:08:38,400
 the Great,

136
00:08:38,400 --> 00:08:42,280
 keep the doors of their senses guarded, are beloved both of

137
00:08:42,280 --> 00:08:43,400
 gods and men.

138
00:08:43,400 --> 00:08:46,400
 And then he taught this verse.

139
00:08:48,400 --> 00:08:53,930
 So it's not actually an answer to the question of why he

140
00:08:53,930 --> 00:08:58,400
 singled out Mahakachayana, but it's indirectly,

141
00:08:58,400 --> 00:09:03,470
 the implication here is that Mahakachayana is worth the

142
00:09:03,470 --> 00:09:04,400
 respect.

143
00:09:04,400 --> 00:09:09,640
 Why do you care if it's irrelevant as to whether someone

144
00:09:09,640 --> 00:09:13,400
 only does it to him or does it to all of the great monks?

145
00:09:13,400 --> 00:09:18,530
 It's worth doing. This paying respect is a good thing

146
00:09:18,530 --> 00:09:21,430
 because of the qualities of Mahakachayana that are worthy

147
00:09:21,430 --> 00:09:22,400
 of respect.

148
00:09:22,400 --> 00:09:27,570
 So it's funny, you often get this in monasteries by jealous

149
00:09:27,570 --> 00:09:28,400
 monks.

150
00:09:28,400 --> 00:09:30,400
 They'll say, why did that monk get this?

151
00:09:30,400 --> 00:09:37,240
 Sometimes you just become obsessed because when your world

152
00:09:37,240 --> 00:09:41,820
 is simplified to such an extent where the biggest central

153
00:09:41,820 --> 00:09:42,400
 pleasure of the day

154
00:09:42,400 --> 00:09:45,760
 is a bowl of food, that bowl of food can become quite an

155
00:09:45,760 --> 00:09:46,400
 attachment.

156
00:09:46,400 --> 00:09:50,450
 And if your bowl of food has better food than mine, gosh,

157
00:09:50,450 --> 00:09:57,400
 some of the problems that come up in monasteries.

158
00:09:57,400 --> 00:10:03,400
 So this idea of favoritism, some people revert to childhood

159
00:10:03,400 --> 00:10:09,400
 and you got more than me and so on and so on.

160
00:10:09,400 --> 00:10:14,030
 But all the Buddha is saying, as I can see, is that it's

161
00:10:14,030 --> 00:10:15,400
 worth doing.

162
00:10:15,400 --> 00:10:20,410
 How can you criticize when people do a pay respect to

163
00:10:20,410 --> 00:10:23,400
 someone worthy of respect?

164
00:10:23,400 --> 00:10:27,400
 It's funny because people often, it seems in modern times,

165
00:10:27,400 --> 00:10:30,400
 especially in the West, have problems with paying respect

166
00:10:30,400 --> 00:10:30,400
 in general.

167
00:10:30,400 --> 00:10:33,760
 This idea that anyone should put their head at anyone else

168
00:10:33,760 --> 00:10:34,400
's feet.

169
00:10:34,400 --> 00:10:40,400
 Why should we hold someone else above ourselves?

170
00:10:40,400 --> 00:10:47,000
 I don't know that the Buddha particularly pushed this sort

171
00:10:47,000 --> 00:10:48,400
 of thing.

172
00:10:48,400 --> 00:10:52,850
 I mean, he did talk about how it's a good thing to be

173
00:10:52,850 --> 00:10:55,400
 humble and to be respectful.

174
00:10:55,400 --> 00:10:59,310
 And he seems to have followed it, but it doesn't seem to be

175
00:10:59,310 --> 00:11:00,400
 a huge deal.

176
00:11:00,400 --> 00:11:04,850
 It's sort of one of those things that the Buddha would more

177
00:11:04,850 --> 00:11:08,400
 often do this kind of thing and say, "Why wouldn't he?"

178
00:11:08,400 --> 00:11:12,400
 What's to be surprised about?

179
00:11:12,400 --> 00:11:16,180
 Because here's someone who has tamed their mind, who has

180
00:11:16,180 --> 00:11:22,000
 become free from all asa, who has a mind that is fixed and

181
00:11:22,000 --> 00:11:27,400
 focused and well bent on good things.

182
00:11:27,400 --> 00:11:32,400
 Someone whose faculties are tranquilized.

183
00:11:32,400 --> 00:11:38,400
 "Yasindriyane samatangatani" is a nice phrase.

184
00:11:38,400 --> 00:11:45,400
 So, what does this have to do with us?

185
00:11:45,400 --> 00:11:48,400
 Well, this is the sort of thing we wish to emulate.

186
00:11:48,400 --> 00:11:51,880
 We don't have to be arahants ourselves, not today. That's

187
00:11:51,880 --> 00:11:53,400
 the ultimate goal.

188
00:11:53,400 --> 00:11:59,400
 But even though we aren't arahants, we can emulate them.

189
00:11:59,400 --> 00:12:02,520
 My teacher always said "Iyeng", he used this word "Iyeng",

190
00:12:02,520 --> 00:12:03,400
 it's a Thai word.

191
00:12:03,400 --> 00:12:06,560
 When we have certain qualities, we are like them, we

192
00:12:06,560 --> 00:12:07,400
 emulate them.

193
00:12:07,400 --> 00:12:12,400
 "Iyeng", we emulate the arahants.

194
00:12:12,400 --> 00:12:16,400
 "Iyengprarahan", we emulate the arahants.

195
00:12:16,400 --> 00:12:19,400
 "Yasindriyane samatangatani"

196
00:12:19,400 --> 00:12:23,400
 Because that's an important quality to emulate.

197
00:12:23,400 --> 00:12:27,400
 It's something you should affect in a sense.

198
00:12:27,400 --> 00:12:31,460
 It can be dangerous because it can just be a pretense where

199
00:12:31,460 --> 00:12:32,400
 you just appear to be,

200
00:12:32,400 --> 00:12:37,330
 people can appear to be very calm and tranquil, but inside

201
00:12:37,330 --> 00:12:39,400
 their minds are a raging mess.

202
00:12:39,400 --> 00:12:44,400
 But guarding your faculties is a huge part of morality.

203
00:12:44,400 --> 00:12:48,240
 It's something that protects your fragile mind as you

204
00:12:48,240 --> 00:12:50,400
 cultivate it, as you're developing it.

205
00:12:50,400 --> 00:12:55,800
 So when monks go into the city, they're careful to look

206
00:12:55,800 --> 00:12:56,400
 down, look at the floor,

207
00:12:56,400 --> 00:12:59,930
 not get caught up in the world around them because it's too

208
00:12:59,930 --> 00:13:04,400
 easy to lose track of your meditation subject.

209
00:13:04,400 --> 00:13:09,400
 And so for all of us, this is a great reminder.

210
00:13:09,400 --> 00:13:14,400
 On even a superficial level, to guard our faculties.

211
00:13:14,400 --> 00:13:21,400
 It's also sort of a way of gauging our practice.

212
00:13:21,400 --> 00:13:24,400
 How tranquil are our faculties?

213
00:13:24,400 --> 00:13:27,970
 When we see something with the eye, are we immediately inc

214
00:13:27,970 --> 00:13:32,400
ensed with lust and desire or anger and aversion?

215
00:13:32,400 --> 00:13:35,780
 When we hear sounds, when we smell smells, are we

216
00:13:35,780 --> 00:13:38,400
 immediately disgusted or attracted?

217
00:13:38,400 --> 00:13:43,920
 When we have tastes, are we partials obsessed with good

218
00:13:43,920 --> 00:13:46,400
 food and good tastes?

219
00:13:46,400 --> 00:13:51,330
 This is a great way to judge your practice based on the

220
00:13:51,330 --> 00:13:52,400
 faculties.

221
00:13:52,400 --> 00:13:55,400
 We often miss this one, the senses.

222
00:13:55,400 --> 00:13:59,400
 We miss how fundamental the senses are to experience.

223
00:13:59,400 --> 00:14:03,400
 So with food, we might be clear on the chewing part, right?

224
00:14:03,400 --> 00:14:06,500
 Okay, I can do eating meditation, chewing, chewing,

225
00:14:06,500 --> 00:14:07,400
 swallowing.

226
00:14:07,400 --> 00:14:12,760
 But we're not clear on the taste and we miss the fact that

227
00:14:12,760 --> 00:14:17,400
 taste plays such an important role in enlightenment.

228
00:14:17,400 --> 00:14:21,790
 Really, people wonder, there was a question of regards to

229
00:14:21,790 --> 00:14:24,400
 food, how should we overcome our desire for food?

230
00:14:24,400 --> 00:14:27,400
 Well, here it is. What's going on?

231
00:14:27,400 --> 00:14:30,740
 You understand what's happening. That's how you deal with

232
00:14:30,740 --> 00:14:32,400
 it, how you overcome it.

233
00:14:32,400 --> 00:14:39,060
 You see the process of tasting something and being incensed

234
00:14:39,060 --> 00:14:43,400
 with desire, lust, sensual lust for that taste.

235
00:14:43,400 --> 00:14:47,400
 And you'll do anything to get that taste.

236
00:14:47,400 --> 00:14:51,400
 Even just the thought of that taste inflames the mind.

237
00:14:51,400 --> 00:14:57,030
 This is the Buddha said, "Ji-wa ad-it-a" - the tongue is on

238
00:14:57,030 --> 00:14:58,400
 fire.

239
00:14:58,400 --> 00:15:04,400
 Ganha-a, Ganha, no, that's the Pali.

240
00:15:04,400 --> 00:15:11,400
 Chaku-ad-it, Chaku-ad-it-a.

241
00:15:11,400 --> 00:15:13,400
 The grammar, I can't remember.

242
00:15:13,400 --> 00:15:17,400
 The eye is on fire, the ear is on fire.

243
00:15:17,400 --> 00:15:21,190
 On fire with greed, on fire with anger, on fire with

244
00:15:21,190 --> 00:15:25,400
 passion, on fire with anger, on fire with delusion.

245
00:15:25,400 --> 00:15:30,400
 And delusion is like arrogance, conceit.

246
00:15:30,400 --> 00:15:34,240
 I like, you know, it's conceit just to say I like this or I

247
00:15:34,240 --> 00:15:38,770
 prefer onions, I prefer carrots, I prefer salty food, sweet

248
00:15:38,770 --> 00:15:39,400
 food.

249
00:15:39,400 --> 00:15:43,970
 It's, it's, it's, it's ego because we get very kind of,

250
00:15:43,970 --> 00:15:48,400
 kind of proud in a sense, proud of having a preference.

251
00:15:48,400 --> 00:15:53,020
 You know, it's kind of like expressing our preference has

252
00:15:53,020 --> 00:15:54,400
 some meaning.

253
00:15:54,400 --> 00:15:57,720
 Like if someone says, "Oh, I'm, I'm partial to this." It's

254
00:15:57,720 --> 00:16:01,400
 almost as though it's, it's an important part of who they

255
00:16:01,400 --> 00:16:02,400
 are, right?

256
00:16:02,400 --> 00:16:07,400
 And Buddhists just shake their heads.

257
00:16:07,400 --> 00:16:12,400
 Who you are is not important.

258
00:16:12,400 --> 00:16:15,760
 So called, guarding the senses, but more tranquilizing the

259
00:16:15,760 --> 00:16:16,400
 senses.

260
00:16:16,400 --> 00:16:19,490
 So guarding is the first part, but this is talking about

261
00:16:19,490 --> 00:16:22,710
 samatangatani, having gone to tranquility means putting out

262
00:16:22,710 --> 00:16:23,400
 the fires.

263
00:16:23,400 --> 00:16:27,400
 It's really a reference to that, the idea of quenching the

264
00:16:27,400 --> 00:16:29,400
 fires, cooling the senses.

265
00:16:29,400 --> 00:16:32,570
 So that seeing is just seeing huge part of the Buddhist

266
00:16:32,570 --> 00:16:33,400
 teaching.

267
00:16:33,400 --> 00:16:37,130
 Remember the teaching from tabahiya, "dit te di tamatang bh

268
00:16:37,130 --> 00:16:37,400
ishati."

269
00:16:37,400 --> 00:16:39,400
 Let seeing just be seeing.

270
00:16:39,400 --> 00:16:41,400
 That's the goal.

271
00:16:41,400 --> 00:16:44,470
 Really, and very hard for an ordinary, for a non-meditator

272
00:16:44,470 --> 00:16:47,400
 to understand, for a newcomer to Buddhism.

273
00:16:47,400 --> 00:16:51,400
 Seeing is just seeing. Well, isn't that all it is?

274
00:16:51,400 --> 00:16:53,400
 It's the problem. That's what we think it is.

275
00:16:53,400 --> 00:16:56,400
 And we don't realize it's on fire.

276
00:16:56,400 --> 00:17:00,400
 Our eyes on fire. The ears on fire.

277
00:17:00,400 --> 00:17:03,420
 So this is the powerful teachings of the Buddha in regards

278
00:17:03,420 --> 00:17:04,400
 to the senses.

279
00:17:04,400 --> 00:17:08,240
 It's a very powerful, important, useful part of the

280
00:17:08,240 --> 00:17:09,400
 Buddhist teaching.

281
00:17:09,400 --> 00:17:12,690
 That's why the Buddha again and again talked about the

282
00:17:12,690 --> 00:17:13,400
 senses.

283
00:17:13,400 --> 00:17:18,490
 The indriya. Seeing, hearing, smelling, tasting, feeling

284
00:17:18,490 --> 00:17:20,400
 and thinking.

285
00:17:20,400 --> 00:17:25,650
 So the asa yata, saratina, sudanta. This is well trained

286
00:17:25,650 --> 00:17:27,400
 like a horse.

287
00:17:27,400 --> 00:17:34,670
 No, well, like the meditator is the, not the horse, the med

288
00:17:34,670 --> 00:17:37,400
itator is the cart person.

289
00:17:37,400 --> 00:17:40,400
 The person riding the cart. Sarati.

290
00:17:40,400 --> 00:17:45,400
 Charioteer we often translate it as.

291
00:17:45,400 --> 00:17:48,860
 So they are the person. So you tame them like you tame a

292
00:17:48,860 --> 00:17:49,400
 horse.

293
00:17:49,400 --> 00:17:53,400
 And often the way you would tame a horse is by tying it up

294
00:17:53,400 --> 00:17:57,390
 or fencing it up and, or getting on its back and letting it

295
00:17:57,390 --> 00:17:59,400
 wear itself out.

296
00:17:59,400 --> 00:18:04,220
 Just wearing itself out. You can't tie it down or you can't

297
00:18:04,220 --> 00:18:06,400
 beat it into submission.

298
00:18:06,400 --> 00:18:10,870
 You just have to, just have to, usually I think, just have

299
00:18:10,870 --> 00:18:13,690
 to outlast it and show it that you've got more patience

300
00:18:13,690 --> 00:18:14,400
 than it does.

301
00:18:14,400 --> 00:18:19,660
 So it might buck and jump and run around, but eventually it

302
00:18:19,660 --> 00:18:21,400
 will get tired.

303
00:18:21,400 --> 00:18:24,830
 The mind is sort of the same. So that's a fairly good

304
00:18:24,830 --> 00:18:28,580
 analogy because eventually the mind starts to notice that

305
00:18:28,580 --> 00:18:29,400
 its behavior is hurting itself.

306
00:18:29,400 --> 00:18:34,010
 If your objective and alert, you start to see that you're

307
00:18:34,010 --> 00:18:36,400
 causing yourself suffering.

308
00:18:36,400 --> 00:18:40,040
 Oh yeah, this obsession with sights and sounds and smells

309
00:18:40,040 --> 00:18:42,400
 and taste is like a ping pong match.

310
00:18:42,400 --> 00:18:48,050
 Everything I'm just reacting and bouncing back and forth

311
00:18:48,050 --> 00:18:50,400
 like a ping pong ball.

312
00:18:50,400 --> 00:18:55,400
 So you slowly overcome that and your senses calm down.

313
00:18:55,400 --> 00:19:02,830
 Your mind becomes bahina. Bahina means sent or like bent in

314
00:19:02,830 --> 00:19:06,590
 the right direction or focused or fixed in the right

315
00:19:06,590 --> 00:19:08,400
 direction.

316
00:19:08,400 --> 00:19:12,400
 Anasa, why we had that yesterday, right?

317
00:19:12,400 --> 00:19:18,840
 Without taints, without the outflowings or without streams

318
00:19:18,840 --> 00:19:23,350
 of defilements, without any connection, attachment to the

319
00:19:23,350 --> 00:19:24,400
 world.

320
00:19:24,400 --> 00:19:28,400
 Such a person is near even to angels. So we've got a lot of

321
00:19:28,400 --> 00:19:31,400
, well throughout this we've had a lot of talk about angels

322
00:19:31,400 --> 00:19:34,020
 and I think that turns a lot of people off, the Dhammapada

323
00:19:34,020 --> 00:19:34,400
 stories.

324
00:19:34,400 --> 00:19:37,830
 That's fine. It's not really important whether Sakka

325
00:19:37,830 --> 00:19:41,400
 actually came down and paid respect to Mahakachayana.

326
00:19:41,400 --> 00:19:45,210
 The story is not really that important. They're kind of fun

327
00:19:45,210 --> 00:19:50,400
 to read and they often have at least a good moral to them.

328
00:19:50,400 --> 00:19:54,870
 And they present an interesting framework. So if you want,

329
00:19:54,870 --> 00:19:56,400
 if you don't want to believe them, that's fine.

330
00:19:56,400 --> 00:19:59,410
 If you want to scoff and say angels, who's ever seen an

331
00:19:59,410 --> 00:20:03,850
 angel, huh? I've never seen an angel. Then fine. That's

332
00:20:03,850 --> 00:20:04,400
 fine.

333
00:20:04,400 --> 00:20:06,940
 I like the stories. I think a lot of people do. And if you

334
00:20:06,940 --> 00:20:09,400
 just put that aside, it's not important.

335
00:20:09,400 --> 00:20:12,450
 What's important is whether the teaching is sound and we

336
00:20:12,450 --> 00:20:14,400
 can argue and debate about that.

337
00:20:14,400 --> 00:20:17,460
 And so some people want to argue and debate as to whether

338
00:20:17,460 --> 00:20:19,400
 paying respect is a good thing.

339
00:20:19,400 --> 00:20:23,260
 In Buddhism it is because it leads to humility. Humility is

340
00:20:23,260 --> 00:20:28,780
 a good thing. Not being proud leads to gratitude, sense of

341
00:20:28,780 --> 00:20:31,820
 gratitude when other people have done something for you who

342
00:20:31,820 --> 00:20:34,400
 have lifted you up.

343
00:20:34,400 --> 00:20:37,820
 They've done something for you that you couldn't do for

344
00:20:37,820 --> 00:20:42,880
 them. So you respect them for that and you honor and revere

345
00:20:42,880 --> 00:20:44,400
 them for that.

346
00:20:44,400 --> 00:20:48,530
 I mean, that's fairly standard. Not just in Buddhism, not

347
00:20:48,530 --> 00:20:53,860
 just in religion, but in amongst people who have this sort

348
00:20:53,860 --> 00:20:55,400
 of sense.

349
00:20:55,400 --> 00:20:58,040
 I think having been in a society that values that sort of

350
00:20:58,040 --> 00:21:01,300
 thing, it changes you. And having been involved in Buddhism

351
00:21:01,300 --> 00:21:02,400
, it changes you.

352
00:21:02,400 --> 00:21:05,200
 I think people have it in the West. They've kind of lost it

353
00:21:05,200 --> 00:21:08,400
. You know, the whole Apple for the teacher thing, you know,

354
00:21:08,400 --> 00:21:10,650
 it became eventually people started to scoff and say it was

355
00:21:10,650 --> 00:21:13,400
 the people who are buttering up the teacher or the brown

356
00:21:13,400 --> 00:21:13,400
 noses.

357
00:21:13,400 --> 00:21:21,400
 That's such an awful saying, right?

358
00:21:21,400 --> 00:21:26,050
 But, you know, our Latin professor, his desk, the guy

359
00:21:26,050 --> 00:21:31,090
 before us uses the chalkboard, old school mathematician guy

360
00:21:31,090 --> 00:21:36,480
, and he leaves the eraser on the desk and there's a huge

361
00:21:36,480 --> 00:21:38,400
 amount of chalk.

362
00:21:38,400 --> 00:21:41,470
 He's just kind of ignorant about it. And there's a huge

363
00:21:41,470 --> 00:21:44,850
 amount of chalk left on the desk for our teacher who uses a

364
00:21:44,850 --> 00:21:47,400
 MacBook that then gets all clogged up.

365
00:21:47,400 --> 00:21:50,160
 And so before his thing, he does when he comes in, as he

366
00:21:50,160 --> 00:21:52,940
 blows the dust off and tries to get rid of as much dust as

367
00:21:52,940 --> 00:21:53,400
 he can.

368
00:21:53,400 --> 00:21:56,160
 And I've kept meaning to bring in a cloth to wipe it down

369
00:21:56,160 --> 00:22:00,660
 for him beforehand. And finally, today, yesterday,

370
00:22:00,660 --> 00:22:05,400
 yesterday, I wiped it down for him before he got there.

371
00:22:05,400 --> 00:22:09,250
 Just with some toilet paper, because I forgot to bring a

372
00:22:09,250 --> 00:22:13,540
 cloth again. But it's just a feeling like that's a good

373
00:22:13,540 --> 00:22:17,820
 thing to do because this is a person who I respect and who

374
00:22:17,820 --> 00:22:19,400
 is doing a good thing for us.

375
00:22:19,400 --> 00:22:24,400
 And as a student, I just feel moved to make his job easier.

376
00:22:24,400 --> 00:22:29,720
 It helps me as well, but it's just, you know, like respect

377
00:22:29,720 --> 00:22:31,400
 and gratitude.

378
00:22:31,400 --> 00:22:32,280
 But we can argue about that. And that's a valid point of

379
00:22:32,280 --> 00:22:33,220
 argument. We shouldn't argue about whether Saka came down

380
00:22:33,220 --> 00:22:40,400
 and paid respect to Mahakar Jayana.

381
00:22:40,400 --> 00:22:43,000
 What else can we argue about? We can argue about whether

382
00:22:43,000 --> 00:22:46,580
 these monks were good to criticize or whether criticism is

383
00:22:46,580 --> 00:22:47,400
 any good.

384
00:22:47,400 --> 00:22:50,480
 And I think we have to have a little bit of a discussion

385
00:22:50,480 --> 00:22:53,980
 about criticism, but I'll save that for the live, the rest

386
00:22:53,980 --> 00:22:55,400
 of the live session.

387
00:22:55,400 --> 00:22:59,410
 So that's all for the Dhammapada for today. Thank you for

388
00:22:59,410 --> 00:23:03,400
 tuning in. Keep practicing and wishing you all the best.

