 Last time I told you we will be with the Buddha for some
 more time.
 So today I'll tell you something more about the life of the
 Buddha.
 When we study the teachings of the Buddha, I think it is
 also important to know something
 about the life of the Buddha.
 And when we know the life of the Buddha, how he struggled
 and how he sacrificed in order
 to become the Buddha, we can have more appreciation and
 understanding of his teachings.
 And last time I told you a very brief account of the life
 of the Buddha.
 And this time we'll go into a little more detail.
 First, about his birth.
 The Buddha was born 624 BC on the full moon day of the
 month of May.
 And the time of his birth was not given in the accounts,
 but it seemed that he was born
 in the morning.
 Because in the afternoon the sage, Kala Devala, came to see
 the bodhisattva.
 So probably it was in the morning that he was born.
 And the place he was born was the Lombini Park.
 Maybe a garden or a small forest not far from the city of
 Kapilawatu.
 And his parents were King Saldodana and Queen Maha Maya.
 So he was born of King Saldodana and Queen Maha Maya on the
 full moon day of May 624
 BC in the morning in the Lombini Park near the city of Kap
ilawatu.
 And in the afternoon the ascetic named Kala Devala or Asida
 came to the palace to see
 the newborn baby.
 Kala Devala was in the habit of coming to King Saldodana
 and took food there and then talked
 with him and he went away.
 So this time he came to see the newborn baby and when he
 arrived King Saldodana made the
 newborn baby pay homage to the ascetic.
 But the feet of the newborn baby got on the head of the asc
etic and so the ascetic Kala
 Devala bowed down through the infant baby.
 And when the King Saldodana, the King himself also bowed
 down to his own son.
 When the ascetic saw the child first he laughed and then he
 cried.
 The King was alarmed and so asked the reason for his
 laughing and crying.
 And the ascetic said I laughed because this child will
 definitely become a Buddha for
 the benefit of all beings.
 And he said I cried because I will not live to see this
 child become a Buddha because
 he was to die before the child becomes a Buddha.
 So first he laughed and then he cried and he told the King
 that the child will definitely
 become a Buddha.
 On the fifth day after birth of the child the King held a
 name giving ceremony.
 Each Brahmin was invited to read the special signs in the
 child.
 Now seven older Brahmins read the signs and then they made
 a prophecy that if the child
 leads a household life then he would become a universal
 monarch.
 And if the child chose to become a monk, chose to renounce
 the world then he would become
 a Buddha.
 But the youngest of them or Kontanya by name, if only one
 prophecy and that is a person
 possessing such signs will not remain in the household life
 but definitely renounce the
 world and become the Buddha.
 And the name Siddhata was given to the child.
 Siddhata means purpose accomplished.
 Another incident important in the life of Siddhata was the
 ploughing ceremony done by
 the King himself.
 In order to promote cultivation in the olden days the King
 and his ministers and his officials
 did the ceremonial ploughing of the fields.
 So at that ceremony the King took his son with him and then
 left the son with the attendants
 and the eye tree and he went to participate in the plough
ing ceremony.
 Then the attendants, the nurses and others wanted to see
 the grand performance of the
 King.
 So they left the child under the tree and went to see the
 ploughing ceremony.
 When nobody was around, actually the child was kept in a
 cover, a curtain was put around
 him.
 So when nobody was there the child set up and then
 practiced the breathing in and breathing
 out meditation and it is said that he attained the first
 journal.
 After some time the attendants remembered that they had
 left the child and so they went
 back to the child and to their surprise they saw the child
 sitting in meditation.
 So they were surprised and they informed to the King and
 the King came to his stand when
 he saw his son in a meditative posture actually sitting in
 the first journal he bowed down
 again to the infant.
 So this was the second bowing down of the father to the son
.
 Now this incident is important because it was the
 recollection of this incident that
 the ascetic Godama after some years he would become an asc
etic Godama.
 So ascetic Godama was able to discover the right path or
 the right practice.
 Our books did not say when this ploughing ceremony took
 place but since bodhisattva
 was an infant not old enough to go to the ceremony with his
 father and also since ploughing
 ceremony was done every year we may take that this
 particular ploughing ceremony took place
 when bodhisattva was less than one year old.
 So Siddhartha grew up as a prince and at the age of 16 he
 was married to Princess Yasodara.
 In those times people married young and he enjoyed the life
 of a prince, he enjoyed the
 life of luxury for 13 years and it is said in our books
 that King Siddhartha and his father
 did not want him to become a Buddha because he wanted him
 to become a universal monarch.
 So King Siddhartha provided his son with every kind of sens
ual pleasure available in those
 times.
 So for 13 years the Prince Siddhartha enjoyed the life of a
 prince but in his 29th year
 the gods thought that it was time to remind Prince Siddhar
tha of his mission.
 So they showed him the four signs, four great signs.
 So the prince was in the habit of going to his pleasure
 garden every now and then and
 so one day he went to his pleasure garden and on his way he
 saw an old man and then
 four months later he saw a sick man and still four months
 later he saw a dead man.
 So after seeing these three signs he came to see the
 realities of life, that life is
 full of suffering.
 People have to become old, sick and at the end die and he
 cannot escape old age, disease
 and death.
 So on the last trip to the pleasure garden he saw a recluse
, maybe sitting peacefully
 under a tree meditating.
 So when he saw the recluse he understood that he must ren
ounce the walls in order to save
 beings or in order to help beings save themselves from the
 sufferings of the round of rebirths.
 So he turned back to his palace after seeing the recluse.
 Now at that time when he had got into the chariot the news
 of the birth of his son came
 to him.
 Now normally if other would be very delighted when the news
 of the birth of his son comes
 to him but Siddhartha here was not delighted because he was
 bent on renouncing the walls
 and so he considered a birth of his son as a bondage.
 So when he heard the news he said, "Oh a rahu has been born
, a bondage has been born."
 When King Siddhartha knew about what Siddhartha said at the
 news he named the child, the child
 of Siddhartha, "Rahu-la."
 So after saying that he went back to the palace and as soon
 as he arrived at the palace he
 was treated to the sensual pleasures again by his retinue.
 But when they were entertaining him he went to sleep and
 when he woke up he saw people
 especially women who are entertaining him in this area and
 sleeping here and there and
 so he saw it as a cemetery.
 So he decided then and there to renounce the world and
 before leaving the palace he wanted
 to see his wife and son so he went to the sleeping chamber
 of his wife and saw his wife
 sleeping having an arm over the head of the newborn child.
 So he wanted to pick the child up and maybe kiss him or
 something like that but he thought,
 "If I did that Yasodra will wake up and I will not be able
 to renounce the world."
 So he said to himself, "I will see him after I become the
 Buddha."
 So that night with one follower, with one servant, he left
 the palace and that was on
 the full moon day of July.
 At the city gate it is said that the city gate was very
 difficult to open about a thousand
 men needed to open the city gate but the gods opened the
 gate and so he was able to get
 out easily.
 But at that time Mara the evil one appeared to him and
 dissuaded him telling him that
 after seven days Bodhisatta would become a universal
 monarch but Bodhisatta said, "I
 don't want to become a universal monarch, I want to become
 a Buddha to help being saved
 themselves."
 So he dismissed the Mara the evil one and went on.
 In one night it is said that he reached the river Anoma and
 he crossed the horse to jump
 to the other bank of Anoma and after reaching the other
 bank he ordained, he cut his hair
 and then put on the robes of an ascetic.
 After meeting King Bimbisada he met two teachers but not at
 the same time, one after another.
 First he met the teacher called Allara Kallama and he
 practiced with that teacher and he
 attained what that teacher has attained.
 But he found out that attainment did not lead to
 emancipation or did not lead to freedom
 from suffering and so he left that teacher and went to
 another teacher called Ogdaka
 Rama Bodha.
 He learned from that teacher the practice of meditation and
 he practiced and he reached
 what that teacher taught and he found again that that
 attainment could not lead him to
 the eradication of mental defilements and to escape from
 suffering he left that teacher
 again.
 So after that he was alone and he went from place to place
 and he reached a place called
 Uru Vela, a peaceful place fit for meditation.
 So he settled there and practiced what are called practices
 difficult to practice, austere
 practices and at that time Kondanya, the person who fore
told that bodhisattva would definitely
 become the Buddha.
 So Kondanya went to the sons of other Brahmins because the
 other Brahmins had died and their
 sons only lived at that time.
 So he went to them and told them that he was going to where
 the, where the, as the Gautama
 was and whether they would like to go with him.
 And three did not want to go with him but four sons decided
 to go with him and so five
 of them went to where the Asa D. Gautama was practicing a
usterities and attended to his
 needs.
 These austere practices were related by himself where after
 he became the Buddha and we can
 read the practices in some discourses in the Majjhima Nik
aya, the collection of middle length
 sayings.
 So he practiced so severely or he practiced so much that he
 became very, very weak and
 you may have seen the pictures of Asa D. Gautama practicing
 austereities, just the bones and
 skin remains and so on.
 Now at that time Mara the evil one came to him and
 distributed him from the practice.
 He said you are going to die, you are too thin now.
 So there are pleasures to be enjoyed and so on but Bodhis
atta dismissed him and went on
 with his austere practices.
 So that went on for nearly six years.
 So towards the end of the sixth year Bodhisatta reviewed
 his practice.
 He thought to himself I have been practicing these austere
 practices for many years and
 I have not become any nearer to my goal.
 It might not be the right one and then he decided that
 these practices were not the
 real, I mean the correct practices.
 So at that critical moment he recollected the incident at
 the plowing ceremony that he as
 a child he entered into the first Jhana and he was very
 peaceful and happy at that time
 and so he thought to himself might that be the right path
 and then he decided that yeah
 that is the right path.
 So he decided to follow that path but his body was very
 weak.
 So in that bodily condition he knew that he could not carry
 on the practice.
 So he began to take food again.
 Formerly he reduced his intake of food little by little so
 that sometimes he ate only a
 handful of pea soup or sometimes he might even eat just one
 seed or one fruit a day.
 So after he decided to take food again and he did take food
 again the five disciples
 were disillusioned with him.
 They thought that the bodhisattva had abandoned the right
 path and now going after luxury.
 So they left him in disgust and went to another place.
 So bodhisattva was left alone again.
 After bodhisattva gained strength one day that is on the
 full moon day of May 589 BC
 he was sitting under a tree and at that time a lady named
 Sujata came to him and offered
 him rice porridge.
 He accepted the rice porridge along with the bull and then
 he took bath in the in the Niranjara
 river nearby and then he ate the porridge and put the bull
 in the river and it is said that
 the bull went upstream and then sank.
 During Ditam he sat under a tree and in the evening he went
 towards what is now known
 as body tree.
 Body tree is actually a banyan tree but since body or
 enlightenment was gained under that
 tree the tree was called a body tree.
 So he went towards the body tree and on his way to the body
 tree he cross-cutter met him
 and offered him eight bundles of grass.
 So he took those bundles of grass and then after reaching
 the body tree he spread the
 grass on the ground and sat at the foot of the body tree
 facing eastward and then he
 made a very firm resolution.
 I will not change this posture.
 I will not get up from this posture until I reach Buddhah
ood.
 So he made this firm resolution and at that time it was
 about sunset.
 Mara the evil one came again this time according to our
 books with his army, the great army.
 And Mara tried to frighten the bodhisattva away from the
 place but bodhisattva stood
 firm.
 I mean he said firm and in the end Mara was defeated.
 So after the defeat of Mara bodhisattva practiced
 meditation to reach Buddhahood.
 During the first watch of the night he practiced again the
 mindfulness of breathing meditation
 and he gained four jhanas in succession for a second that
 fall and also he gained the
 four immaterial jhanas and also he gained the supernormal
 knowledge by which he could
 remember all his past life.
 So during the first watch of the night he gained the super
normal knowledge of remembering
 all his past lives and during the middle watch of the night
 he gained the supernormal knowledge
 by which he could see beings dying from one existence and
 being reborn in another existence.
 And he also saw that a particular being died from one
 existence and reborn in another existence
 according to his own karma.
 This being is born in hell as a result of unwholesome deeds
 he did in the previous life
 and this being is reborn in the celestial world as a result
 of wholesome deeds he did in his
 previous life and so on.
 So Bodha, he was still a bodhisattva at that time.
 So Bodhisattva could see all these beings dying and being
 reborn as if it were on the
 screen.
 So this helps the Bodha to formulate the law of karma.
 The law of karma taught by the Buddha was not founded on
 just logic.
 Actually it was founded on his own intuition, own direct
 understanding of the law of karma.
 New dependent origination before he became the Bodha.
 So he practiced Vipassana on these 12 factors of dependent
 origination back and forth again
 and again and as a result of that Vipassana practice.
 At dawn he gained the supernormal knowledge by which he was
 able to eradicate all mental
 defilements and at the same moment he gained what we call
 omniscience, knowing everything.
 So with the attainment of the supernormal knowledge of the
 destruction of mental defilements and
 omniscience he became the Bodha.
 So Bodhisattva became the Bodha on the full moon day of May
 589 BC at the age of 35.
 After he became the Bodha he spent eight weeks under and
 near the Bodhi tree and exactly
 two months after his enlightenment he gave his first sermon
 to the five disciples at
 Isipatana near Baranasi where they were not living.
 And about midnight that night Bodha gave his second sermon
 through the Yakas, Hemavada and
 Satagiri and five days after the full moon day Bodha gave
 his third sermon, the sermon
 on the non-soul nature of all things.
 And after the first sermon one of the five disciples became
 Isotapanana and on successive
 days each one gained Sotapanahood and so on the fifth day
 Bodha taught them the sermon
 on non-soul nature of things and they became Arahans.
 And Bodha spent the first rainy season at Isipatana.
 After that Bodha visited Rajagaha where King Bambisara
 lived.
 So Bodha met Bambisara and King Bambisara offered him the
 bamboo grove.
 Bambisara accepted it and spent the second rainy season
 there.
 Then one year after his enlightenment Bodha visited his
 native city Kapilavatu to see
 his father and his relatives.
 So he met his father and relatives and he preached to his
 father and his father became
 enlightened as a Sotapanah first and then as a Sagatagami
 and then as an Anagami.
 So his father reached three stages of enlightenment on his
 first visit.
 And also his half-brother Nanda and his son Rahula were
 ordained.
 They became monks and many princes of Sakya race became
 monks under him.
 And Bodha taught for 45 years and when he taught he taught
 day and night.
 Now Bodha did not sleep as we do he just laid down on his
 right side and took rest.
 Now when we read his daily routine, his daily schedule and
 the daily schedule is given in
 the commentaries.
 So when we read the daily schedule we find that Bodha
 rested for about or not more than
 four hours a day.
 Other times he was doing something teaching lay people or
 teaching monks or teaching or
 answering questions of celestial beings.
 So about 20 hours a day he worked energetically for the
 welfare of all beings.
 Now after becoming a Bodha he could easily retire but he
 did not retire and he taught
 day and night to all beings who came to him or who met him.
 And he helped save I mean he helped countless beings save
 themselves during these 45 years.
 So Bodha was the most energetic religious teacher known to
 history.
 He worked for 45 years and those 45 years he worked day and
 night just taking rest for
 about four hours a day or not more than four hours a day.
 So when we study his daily routine we cannot but admire his
 compassion for all beings and
 his effort, his making effort to save I mean I want to say
 save right.
 And save as many beings as possible and here save means not
 saving himself but saving helping
 being save themselves.
 So he taught for 45 years for the welfare of all beings not
 only human beings but all
 beings and for lay people as well as monks.
 And at the age of 80 he passed away on a full moon day of
 May in a grove of some trees,
 salad trees near the city of Kusinara.
 So Bodha passed away at the age of 80 on the full moon day
 of May again.
 So full moon day of May comes to be very important to all
 Buddhists because it was on the full
 moon day of May that he was born and it was on the full
 moon day of May that he became
 enlightened as a Bodha and it was on this day that he
 passed away.
 So we call it thrice blessed day in the life of the Bodha.
 Now some things to understand about the Bodha.
 As I said before in my first class Bodha was not a god not
 god with capital G because according
 to Bodha there was no god who creates the universe and its
 inhabitants.
 Then was he a god with a small G?
 Was he a celestial being?
 No he was not a god or a celestial being and he was not a
 prophet or messenger of god simply
 again because according to him there was no god or god was
 non-existent.
 Then was he a saviour?
 Not in the Christian sense.
 So Bodha just helped beings save themselves so he was not a
 saviour.
 Then what was he?
 Bodha said just know me as a Bodha.
 Now he was a human being right but not an ordinary human
 being.
 He was an extraordinary human being.
 Human being so extraordinary that he excels even the gods,
 the celestial beings and Brahmās
 who are the highest celestial beings.
 So Bodha came to be known as the best among all beings not
 the best among all human beings
 but the best among all beings including gods and Brahmās.
 And Bodha possessed innumerable qualities among them we
 should understand the purity of mind
 total purity of mind and omniscience these are very
 important qualities.
 So Bodha's mind is totally pure without any trace of any
 mental defilements and Bodha
 possessed what we call omniscience that is he knows
 everything and Bodha himself claimed
 to be omniscient when he when he said to Upaka the ascetic
 whom he met on his way to Bharat
 under sea and there Bodha said I am the knower of all so
 that means I am omniscient.
 So Bodha possessed these qualities total purity of mind and
 omniscience.
 Where is the Bodha now?
 Does he exist in some form somewhere according to the
 teachings of Theravada Bodha is no
 more just as a flame goes out Bodha died and there was no
 more existence or no more rebirth
 for him.
 So according to Theravada teachings there is no Bodha now
 then how can he help us when
 he is no more with us.
 Now somebody invented electricity I mean electric bulbs and
 make use of electricity they are
 no more right but we are enjoying his the benefits of his
 invention so the man who invented
 wireless is no more now but we enjoy listening to radio and
 all these things.
 So although Bodha is not with us now he left his teachings
 before he died he said when
 I am gone my teachings will be teacher for you so we have
 his teachings now and so we
 enjoy his teachings or the benefits of his teachings so
 although Bodha is no more now
 since his teachings are available now we are as good as
 having the Bodha.
 Now can Bodha forgive sins?
 Now sin we are using this word in a Buddhist sense so unwh
olesome actions unwholesome deeds
 unwholesome speech and unwholesome thoughts these we call
 Akkusala.
 So let us call them sins can Bodha forgive sins say we do
 some Akkusala and then can
 we ask pardon from the Bodha please forgive us our sins and
 can Bodha forgive us no.
 Bodha cannot forgive the sins simply because it is
 impossible to forgive and these are
 sins not because Bodha said these were sins but because
 they are sins so it is the other
 way around because they are sins the Bodha said they are
 and Bodha did not create sins
 and when say we do sins that is just our actions now we do
 something and this is our doing
 our action and so this cannot be undone by anybody.
 So Bodha cannot give sins means not because he did not want
 to forgive but because he
 cannot forgive sins or because it is impossible to forgive
 sins that is why we say Bodha
 say do not forgive sins.
 Is Bodha capable of love and compassion?
 Bodha had great love a boundless love and compassion for
 all beings but the love Bodha
 had for being is not love mixed with or accompanied by
 attachment or ill will or delusion it is
 mitta the wholesome desire for the well-being of all beings
 and also Bodha had what we call
 the great compassion that means Bodha had compassion for
 all beings without any exception.
 So in Buddhism we do not see that Bodha has compassion for
 this his followers only and
 not for other people who are not his followers so whether
 you are a follower his follower
 or not Bodha had compassion for you.
 So Bodha's love and compassion for beings is for all beings
 without exception.
 So Bodha possessed or Bodha is capable of the greatest love
 and greatest compassion.
 Bodha capable of doing harm to others since Bodha possesses
 total purity of mind.
 Bodha never do any harm to any any being at all even to
 those who attempted his life.
 Bodha felt only compassion and not anger and Bodha did not
 harm any being at all so there
 is no no mention of Bodha killing any living being or
 killing many people or inflicting
 punishment or suffering to beings.
 We can live in safety concerning the Bodha we will not get
 any harm or any punishment
 or whatever from the Bodha.
 So Bodha is not capable of doing harm to any being.
 How many Bodhas are there?
 Is there only one Bodha?
 Yes, there is only one Bodha at a time although there were
 many many Bodhas in the past and
 there will come many Bodhas in the future.
 At one given present moment, present time, there can be
 only one Bodha in the world and
 it is explained in our books that the world cannot hold two
 Bodhas at a time and if there
 were two Bodhas then there would be some kind of
 competition among his disciples and so
 there is only one Bodha at a time although many Bodhas
 appeared in the past and many
 Bodha will appear in the future.
 Now can you become a Bodha?
 Can anybody become a Bodha?
 Yes, if he or she is willing to fulfill the perfections of
 Paramis, if he or she is willing
 to suffer in the samsara for a long long time accumulating
 the perfections.
 So in theory anybody can become a Bodha but not everybody
 becomes a Bodha in reality.
 Only very very few become Bodhas because to become a Bodha
 is not an easy job so it takes
 a very very long time to accumulate the necessary
 qualifications and also those who aspire for
 Bodha who had to practice or to sacrifice many things in
 their lives, even their lives themselves
 and so it is very difficult to fulfill these qualifications
 and so it is difficult to become
 a Bodha just as not everybody, I mean everybody, what can I
 say that?
 I cannot say every citizen right?
 So every native citizen can become a President of the
 United States.
 We have the right to become the President of the United
 States but not everybody will
 become the President.
 So only one will become a President at one time.
 So in the same way although we can say that beings have a
 potential to become a Bodha,
 not everybody will become the Bodha, only very very few
 will become.
 As anybody who is willing to suffer in the samsara and
 accumulate the perfections can
 become a Bodha.
 Must everybody try to become a Bodha?
 Now in the Tearavada you are not forced to try to become a
 Bodha.
 You are given a choice.
 You can try to become a Bodha or a Pachika Bodha or an Arah
ant.
 So at least there are three paths or three ways open to you
 and you can choose anyone,
 anyone you like.
 So according to Tearavada teachings we cannot say you must
 try to become a Bodha.
 But if you want to, if you are willing to fulfill the par
amese and willing to have been
 saved themselves then you can try to become a Bodha.
 If you don't want to spend a long long time in the samsara
 but want to get out of this
 samsara, pretty soon you may choose the path of Arahant.
 So it is up to you to try to become a Bodha or a Pachika
 Bodha or an Arahant.
 Then how long does it take for a person to become a Bodha
 after the formal aspiration
 for Bodha-hood?
 Formal aspiration means making the aspiration in the
 presence of another Bodha.
 As you know how long?
 Four incalculables and one hundred thousand ian or wall
 cycles, right?
 That is the shortest time.
 For some Bodhas, eight incalculables and one hundred
 thousand wall cycles and for another
 kind of Bodha, sixteen incalculables and one hundred
 thousand wall cycles.
 So you have to spend it, that number is for wall cycles and
 in one wall cycles there can
 be many many lives so you have to go through billions of
 lives accumulating the qualifications
 to become a Bodha.
 So if you are prepared to do that you may aspire for Bodha-
hood.
 You talk about astronomy.
 You talk not in years but not in miles, not in kilometers
 but in light years, millions
 of light years, right?
 So in the same way when we talk about the time a person
 actually emulates parameters
 we talk in incalculables.
 So incalculable means the number is so big that it is
 almost incalculable.
 It is almost cannot be counted and if you want to count
 then the number is the digit
 one followed by one hundred and forty zeros that is one inc
alculable.
 Okay these are some important things to know about the Bod
ha.
 Now sometimes I may take more time because I want you to
 understand many things and also
 this class is only once a month so I wish we could have
 this class twice a month or
 four times a month or every weekday.
 Okay.
