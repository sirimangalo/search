1
00:00:00,000 --> 00:00:02,000
 Catching distractions.

2
00:00:02,000 --> 00:00:06,960
 When I get distracted, I tell myself wandering, but when I

3
00:00:06,960 --> 00:00:07,340
 do that,

4
00:00:07,340 --> 00:00:10,480
 I find myself in a bubble in a sort of meta view of myself

5
00:00:10,480 --> 00:00:11,760
 meditating, and soon

6
00:00:11,760 --> 00:00:14,380
 I find it very difficult to go back wholeheartedly to

7
00:00:14,380 --> 00:00:16,880
 rising/fallen. How do I remedy this?

8
00:00:16,880 --> 00:00:21,860
 One of the biggest challenges in mindfulness meditation is

9
00:00:21,860 --> 00:00:24,740
 catching the essence of your present experience. When your

10
00:00:24,740 --> 00:00:25,640
 mind wanders,

11
00:00:25,640 --> 00:00:29,230
 there are probably many associated mind states arising in

12
00:00:29,230 --> 00:00:29,840
 sequence.

13
00:00:30,040 --> 00:00:33,010
 There may be enjoyment of the wandering thoughts or a worry

14
00:00:33,010 --> 00:00:34,120
, fear, stress,

15
00:00:34,120 --> 00:00:37,030
 etc., and these will contribute to your state of

16
00:00:37,030 --> 00:00:38,080
 distraction.

17
00:00:38,080 --> 00:00:41,430
 The fact that you cannot go back to the rising and falling

18
00:00:41,430 --> 00:00:44,670
 means it is probable that there is some such mind state

19
00:00:44,670 --> 00:00:45,280
 that you

20
00:00:45,280 --> 00:00:47,940
 are not clearly aware of. It is important to see what is

21
00:00:47,940 --> 00:00:49,680
 happening in the present moment,

22
00:00:49,680 --> 00:00:52,820
 rather than to think that it is distracting you from your

23
00:00:52,820 --> 00:00:54,640
 meditation and try to ignore it.

24
00:00:54,640 --> 00:00:57,960
 It will just make it more difficult to stay focused.

25
00:00:57,960 --> 00:01:00,950
 It is much more important that you watch what is really

26
00:01:00,950 --> 00:01:03,640
 holding your attention and focus on it before you can even

27
00:01:03,640 --> 00:01:05,560
 think about going on to note something else.

28
00:01:05,560 --> 00:01:08,870
 When you have noted the thought, you can return to the

29
00:01:08,870 --> 00:01:10,640
 rising and falling of the abdomen,

30
00:01:10,640 --> 00:01:13,800
 but if you feel any emotion associated with the thought,

31
00:01:13,800 --> 00:01:15,480
 you should focus on that first.

32
00:01:15,480 --> 00:01:18,800
 If it is difficult to watch the rising and the falling,

33
00:01:18,800 --> 00:01:21,140
 just focus on that state, even if it means saying to

34
00:01:21,140 --> 00:01:21,600
 yourself

35
00:01:21,600 --> 00:01:26,200
 unclear or distracted or cloudy or however it feels to you.

36
00:01:26,920 --> 00:01:29,660
 Try not to focus so much of your attention on the rising

37
00:01:29,660 --> 00:01:30,360
 and falling,

38
00:01:30,360 --> 00:01:33,130
 but focus on whatever your mind is aware of and when that

39
00:01:33,130 --> 00:01:35,600
 is gone, then go back to the rising and falling.

40
00:01:35,600 --> 00:01:45,600
 [BLANK_AUDIO]

