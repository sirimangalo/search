1
00:00:00,000 --> 00:00:10,360
 There was a monastery in Anuradhapura in Sri Lanka which

2
00:00:10,360 --> 00:00:18,080
 differs in opinion from those of the great

3
00:00:18,080 --> 00:00:24,030
 monastery. So they held some views different than the views

4
00:00:24,030 --> 00:00:27,240
 held by those of the great monastery.

5
00:00:27,240 --> 00:00:34,020
 So now those people said that the ascetic practice does not

6
00:00:34,020 --> 00:00:37,240
 belong to any of the three,

7
00:00:37,240 --> 00:00:42,130
 does not belong to kusala or akusala or abhyagata or ind

8
00:00:42,130 --> 00:00:46,400
eterminate. But the commentator or the

9
00:00:46,400 --> 00:00:51,680
 venerable Buddha of course has said, if it is so, then it

10
00:00:51,680 --> 00:00:55,520
 must be a concept. And concept,

11
00:00:55,520 --> 00:01:02,280
 according to Buddhism, is not reality, not paramata. It is

12
00:01:02,280 --> 00:01:05,800
 concept is not reality. So

13
00:01:05,800 --> 00:01:12,550
 if it is not reality, how can it be practiced? So we cannot

14
00:01:12,550 --> 00:01:22,320
 accept their opinion. And also if

15
00:01:22,320 --> 00:01:28,130
 we take the ascetic practice to be a concept, to be non-

16
00:01:28,130 --> 00:01:38,800
existing, then there will be contradiction

17
00:01:38,800 --> 00:01:45,070
 with the words said by the Buddha, proceeded to undertake

18
00:01:45,070 --> 00:01:51,320
 the ascetic qualities. So the

19
00:01:51,320 --> 00:01:57,190
 ascetic practices should not be taken as pañjati or as

20
00:01:57,190 --> 00:02:02,440
 concept also. So ascetic practices are to

21
00:02:02,440 --> 00:02:13,480
 be taken as either kusala or abhyagata. And then the

22
00:02:13,480 --> 00:02:17,040
 explanation of the words ascetic,

23
00:02:17,040 --> 00:02:24,720
 ascetic practices and those who talk about ascetic

24
00:02:24,720 --> 00:02:28,400
 practices and so on. And they are not

25
00:02:28,400 --> 00:02:35,320
 difficult to understand. A preacher of asceticism and so on

26
00:02:35,320 --> 00:02:38,520
. So some people practice themselves but

27
00:02:38,520 --> 00:02:41,910
 do not encourage others to practice. And some people only

28
00:02:41,910 --> 00:02:43,920
 encourage others to practice and

29
00:02:43,920 --> 00:02:52,620
 do not practice themselves. And the examples are given here

30
00:02:52,620 --> 00:02:57,120
 like the Venerable Bakula. On

31
00:02:57,120 --> 00:03:04,700
 paragraph 82, one who has shaken off his defilements with

32
00:03:04,700 --> 00:03:08,120
 an ascetic practice but does not

33
00:03:08,120 --> 00:03:13,160
 advise and instruct another in an ascetic practice like the

34
00:03:13,160 --> 00:03:16,560
 Elder Bakula. So he practiced himself

35
00:03:16,560 --> 00:03:22,320
 but he did not encourage others to practice. And then the

36
00:03:22,320 --> 00:03:25,560
 other one is Upananda. He encouraged

37
00:03:25,560 --> 00:03:38,150
 others to practice but he did not practice himself. And

38
00:03:38,150 --> 00:03:41,920
 then the other one who did not

39
00:03:41,920 --> 00:03:47,420
 practice himself and who did not encourage others like Lalu

40
00:03:47,420 --> 00:03:49,760
 Dai. And the last one is

41
00:03:49,760 --> 00:04:03,270
 the Dhamma Senapati. What's that? General of the Dhamma.

42
00:04:03,270 --> 00:04:07,680
 You know who? The General of the

43
00:04:07,680 --> 00:04:13,530
 Dhamma. Sariputta. So Sariputta is always called in Pali D

44
00:04:13,530 --> 00:04:24,240
hamma Senapati, General of the Dhamma. And

45
00:04:24,240 --> 00:04:37,400
 Ananda is called the Treasurer of the Dhamma. He is the

46
00:04:37,400 --> 00:04:43,840
 Keeper of the Dhamma. And then ascetic

47
00:04:43,840 --> 00:04:47,480
 states. The five states that go with the evolution of an

48
00:04:47,480 --> 00:04:50,880
 ascetic practice, that is to say, fewness

49
00:04:50,880 --> 00:04:55,510
 of wishes, contentment, effacement, seclusion, and that

50
00:04:55,510 --> 00:04:58,400
 specific quality are called ascetic

51
00:04:58,400 --> 00:05:02,100
 states because of the words depending on fewness of wishes

52
00:05:02,100 --> 00:05:08,320
 and so on. Now, I do not agree with the

53
00:05:08,320 --> 00:05:17,300
 translation that specific quality. The Pali word here is

54
00:05:17,300 --> 00:05:23,520
 strange words but it means desire for these

55
00:05:23,520 --> 00:05:31,170
 practices, desire for these wholesome states. Not that

56
00:05:31,170 --> 00:05:36,560
 specific quality, it is desire to practice or

57
00:05:36,560 --> 00:05:41,390
 desire for these wholesome states. So it is actually it is

58
00:05:41,390 --> 00:05:44,560
 knowledge. It is explained later in

59
00:05:44,560 --> 00:05:49,490
 paragraph 84. That specific quality is knowledge. Here and

60
00:05:49,490 --> 00:05:52,080
 by means of non-greet, a man shakes of

61
00:05:52,080 --> 00:05:55,520
 greed for things that are forbidden. By means of non-del

62
00:05:55,520 --> 00:05:58,320
usion, that means knowledge. He shakes of

63
00:05:58,320 --> 00:06:01,680
 the delusion that hides the dangers in those same things.

64
00:06:01,680 --> 00:06:03,920
 And by means of non-greet, he shakes of

65
00:06:03,920 --> 00:06:07,320
 indulgence and pleasure due to sense desires that occur

66
00:06:07,320 --> 00:06:09,680
 under the heading of using what is allowed

67
00:06:09,680 --> 00:06:24,240
 and so on. Now, 13 practices and who can practice which?

68
00:06:24,240 --> 00:06:33,440
 First, which is suitable for which person?

69
00:06:34,000 --> 00:06:37,010
 So for one of greedy temperament and for one of deluded

70
00:06:37,010 --> 00:06:38,160
 temperament.

71
00:06:38,160 --> 00:06:44,030
 There are six temperaments. We will study them in the next

72
00:06:44,030 --> 00:06:48,080
 chapter. Because the cultivation of

73
00:06:48,080 --> 00:06:53,430
 ascetic practices is both a difficult progress, actually it

74
00:06:53,430 --> 00:06:56,960
 means a difficult practice. It is not

75
00:06:56,960 --> 00:07:01,510
 an easy thing to take up ascetic practices. So it is a

76
00:07:01,510 --> 00:07:05,120
 difficult practice and an abiding in a face

77
00:07:05,120 --> 00:07:09,880
 in a face man and greed subsides with the difficult

78
00:07:09,880 --> 00:07:14,320
 practice. Why delusion is cordered off in those

79
00:07:14,320 --> 00:07:18,280
 diligent by a face man. The cultivation of the forest dwell

80
00:07:18,280 --> 00:07:20,480
ers practice and the tree dwellers

81
00:07:20,480 --> 00:07:27,060
 practice here are suitable for one of hating temperament.

82
00:07:27,060 --> 00:07:30,320
 That means also for one of hating

83
00:07:30,320 --> 00:07:35,600
 temperament. For hate to subsides in one who dwells there

84
00:07:35,600 --> 00:07:38,640
 without coming into conflict.

85
00:07:38,640 --> 00:07:41,520
 He lives alone.

86
00:07:41,520 --> 00:07:47,380
 He doesn't get to quarrel with anybody. He might quarrel

87
00:07:47,380 --> 00:07:48,240
 with himself.

88
00:07:48,240 --> 00:08:00,560
 And then as to groups and also singly. And now six as to

89
00:08:00,560 --> 00:08:03,360
 groups. These ascetic practices are

90
00:08:03,360 --> 00:08:07,210
 in fact only eight. That is to say three principle and five

91
00:08:07,210 --> 00:08:08,640
 individual practices.

92
00:08:08,640 --> 00:08:14,640
 The three namely the house to house seekers practice, the

93
00:08:14,640 --> 00:08:16,320
 one sessioners practice and the

94
00:08:16,320 --> 00:08:20,620
 open air dwellers practice are principle practices. For one

95
00:08:20,620 --> 00:08:23,040
 who keeps the house to house seekers

96
00:08:23,040 --> 00:08:26,450
 practice will keep the arms food eaters practice and the

97
00:08:26,450 --> 00:08:28,720
 bold food eaters practice and the later

98
00:08:28,720 --> 00:08:32,330
 food refusers practice will be well kept by one who keeps

99
00:08:32,330 --> 00:08:35,200
 the one sessioners practice. So when one

100
00:08:35,200 --> 00:08:41,270
 is taken the others are virtually taken. And what need has

101
00:08:41,270 --> 00:08:44,000
 one who keeps the open air dwellers

102
00:08:44,000 --> 00:08:47,410
 practice to keep the tree dwellers practice or the any bed

103
00:08:47,410 --> 00:08:49,760
 users practice. So there are these

104
00:08:49,760 --> 00:08:52,900
 three principle practices that together with the five

105
00:08:52,900 --> 00:08:55,120
 individual practices that is to say the

106
00:08:55,120 --> 00:08:59,190
 forest dwellers practices and so on come to eight only. So

107
00:08:59,190 --> 00:09:02,240
 13 can be counted as eight only when we

108
00:09:02,240 --> 00:09:07,680
 take the principle ones. And then two concerned with robes,

109
00:09:07,680 --> 00:09:10,800
 five concerned with arms food and so on.

110
00:09:12,160 --> 00:09:16,000
 But what what is interesting or what is important is who

111
00:09:16,000 --> 00:09:21,040
 can practice which which practices.

112
00:09:21,040 --> 00:09:33,260
 So singly that is on page 83 paragraph 90 with 13 for bikus

113
00:09:33,260 --> 00:09:36,400
. So monks can practice all 13 practices

114
00:09:37,440 --> 00:09:43,050
 eight for bikinis. The nuns can practice only eight. 12 for

115
00:09:43,050 --> 00:09:43,840
 novices

116
00:09:43,840 --> 00:09:49,710
 some years novices can practice 12. Seven for female

117
00:09:49,710 --> 00:09:52,400
 probationers and female novices.

118
00:09:52,400 --> 00:09:55,040
 There are two kinds of female

119
00:09:55,040 --> 00:09:57,920
 female

120
00:09:57,920 --> 00:10:03,360
 I don't know what to say that

121
00:10:06,720 --> 00:10:13,860
 probationers and novices if a a woman or a girl wants to

122
00:10:13,860 --> 00:10:17,920
 become a nun then he she must spend some

123
00:10:17,920 --> 00:10:23,680
 time under probation. About two years keeping only six

124
00:10:23,680 --> 00:10:28,720
 precepts and then she becomes a sambani rea

125
00:10:30,000 --> 00:10:35,800
 a novice and then after that she becomes a nun a bikuni. So

126
00:10:35,800 --> 00:10:41,360
 for for such persons seven are allowable

127
00:10:41,360 --> 00:10:45,690
 and two for male and female lay followers lay people can

128
00:10:45,690 --> 00:10:48,720
 also practice some of these practices

129
00:10:48,720 --> 00:10:52,540
 and there are two they can practice. So there are all

130
00:10:52,540 --> 00:10:55,680
 together 42. If there is a channel ground in

131
00:10:55,680 --> 00:11:00,320
 the open that complies with the forest dwellers practice

132
00:11:00,320 --> 00:11:02,720
 one bikku is able to put all the

133
00:11:02,720 --> 00:11:06,970
 ascetic practices into effect simultaneously. So a monk can

134
00:11:06,970 --> 00:11:09,360
 practice all the 13 practices

135
00:11:09,360 --> 00:11:13,440
 if there is a channel ground in the open and it is away

136
00:11:13,440 --> 00:11:17,280
 from the village by about 1000

137
00:11:17,280 --> 00:11:23,780
 1000 yards then a monk living there can practice all these

138
00:11:23,780 --> 00:11:27,760
 13 practices simultaneous. But the two

139
00:11:27,760 --> 00:11:30,700
 namely the forest dwellers practice and the later food ref

140
00:11:30,700 --> 00:11:33,120
users practice are forbidden to bikunis

141
00:11:33,120 --> 00:11:37,740
 by training precepts. Bikunis do not have to keep those

142
00:11:37,740 --> 00:11:40,880
 precepts and so they they are not they cannot

143
00:11:40,880 --> 00:11:49,140
 keep those those those ascetic practices. Bikunis must not

144
00:11:49,140 --> 00:11:52,240
 not be on their own they must live

145
00:11:52,240 --> 00:11:57,850
 not not too close but they must live close to the monks and

146
00:11:57,850 --> 00:12:00,320
 so they cannot practice forest dwellers

147
00:12:00,320 --> 00:12:03,510
 practice and it is hard for them to observe the three

148
00:12:03,510 --> 00:12:06,000
 namely the open air dwellers practice

149
00:12:06,000 --> 00:12:08,320
 the tree root dwellers practice and the channel ground

150
00:12:08,320 --> 00:12:10,560
 dwellers practice because a bikuni is not

151
00:12:10,560 --> 00:12:13,770
 allowed to live without a companion so a bikuni is not

152
00:12:13,770 --> 00:12:16,080
 allowed to live alone she must have a

153
00:12:16,080 --> 00:12:18,000
 female companion

154
00:12:18,000 --> 00:12:26,640
 and it is hard to find a female companion with like desire

155
00:12:26,640 --> 00:12:29,280
 for such a place and even if available

156
00:12:29,280 --> 00:12:31,840
 she would not escape having to live in company.

157
00:12:34,320 --> 00:12:40,820
 To keep the the purpose in keeping these practices is to

158
00:12:40,820 --> 00:12:44,640
 enjoy seclusion and if you have to live with

159
00:12:44,640 --> 00:12:48,290
 another person then you lose that this being so the purpose

160
00:12:48,290 --> 00:12:50,400
 of cultivating cultivating the

161
00:12:50,400 --> 00:12:54,310
 ascetic practice would scarcely be served it is because

162
00:12:54,310 --> 00:12:57,200
 they are reduced by five owing to this

163
00:12:57,200 --> 00:13:00,650
 inability to make use of certain of them that they are to

164
00:13:00,650 --> 00:13:03,600
 be understood as eight only for bikunis so

165
00:13:03,600 --> 00:13:07,920
 bikunis can practice eight of them except for the tripled

166
00:13:07,920 --> 00:13:11,040
 rope wearers practice all the other 12 as

167
00:13:11,040 --> 00:13:15,560
 stated should be understood to be for novices so novices

168
00:13:15,560 --> 00:13:18,960
 male novices can practice 12 of them

169
00:13:18,960 --> 00:13:22,480
 because novices

170
00:13:25,360 --> 00:13:33,360
 cannot use the third rope the double layer rope and that is

171
00:13:33,360 --> 00:13:37,920
 allowed for monks only

172
00:13:37,920 --> 00:13:44,300
 so samaliras or novices do not use the third rope so they

173
00:13:44,300 --> 00:13:45,120
 cannot practice

174
00:13:45,120 --> 00:13:49,280
 the ascetic practice of having three ropes only

175
00:13:53,280 --> 00:13:56,590
 and all the other seven for female probationers and female

176
00:13:56,590 --> 00:13:57,120
 novices

177
00:13:57,120 --> 00:14:07,040
 so female probationers and female novices can practice

178
00:14:07,040 --> 00:14:10,880
 seven out of eight for nuns

179
00:14:10,880 --> 00:14:14,800
 the two namely the one session has practiced and the bold

180
00:14:14,800 --> 00:14:16,880
 food eaters practice are proper for

181
00:14:16,880 --> 00:14:22,010
 male and female leaf followers to employ so leaf people can

182
00:14:22,010 --> 00:14:24,800
 practice one session has practiced

183
00:14:24,800 --> 00:14:30,320
 eating at once eating only or the bold food eaters practice

184
00:14:30,320 --> 00:14:34,480
 eating in one bowl only so these

185
00:14:34,480 --> 00:14:38,040
 these two leaf people can practice in this way there are

186
00:14:38,040 --> 00:14:39,600
 two ascetic practices

187
00:14:46,160 --> 00:14:48,800
 this is the commandery as to the groups and also singly

188
00:14:48,800 --> 00:14:58,480
 so these 13 practices are not much practiced nowadays

189
00:14:58,480 --> 00:15:07,360
 and those living in in villages or in towns cannot cannot

190
00:15:07,360 --> 00:15:10,560
 practice most of these practices

191
00:15:10,560 --> 00:15:14,690
 but those who live in the forest monasteries can practice

192
00:15:14,690 --> 00:15:18,480
 many of them and there are still monks who

193
00:15:18,480 --> 00:15:28,180
 practice many of them like living in the cemetery or living

194
00:15:28,180 --> 00:15:33,040
 under a tree and eating in one bowl

195
00:15:33,680 --> 00:15:39,840
 and not lying down and not and keeping only three ropes and

196
00:15:39,840 --> 00:15:42,960
 these practices are meant for

197
00:15:42,960 --> 00:15:53,360
 a basement of mental defilements we cannot do with mental

198
00:15:53,360 --> 00:15:56,160
 defilements altogether by these practices

199
00:15:56,160 --> 00:16:02,900
 but we can make them reduced we can scrape them little by

200
00:16:02,900 --> 00:16:06,240
 little by these practices and so

201
00:16:06,240 --> 00:16:14,240
 according to the visodimaga a monk must first purify his

202
00:16:14,240 --> 00:16:17,600
 moral conduct a monk must have

203
00:16:18,240 --> 00:16:23,630
 pure seeler and then he must practice some of these ascetic

204
00:16:23,630 --> 00:16:26,320
 practices and then next he will go

205
00:16:26,320 --> 00:16:34,070
 on to practicing meditation so these two chapters are for

206
00:16:34,070 --> 00:16:40,800
 the for the basic basic practice before

207
00:16:41,920 --> 00:16:46,750
 one goes to practice the calm meditation or insight

208
00:16:46,750 --> 00:16:47,920
 meditation

209
00:16:47,920 --> 00:16:53,600
 okay

210
00:16:53,600 --> 00:17:03,190
 so next week we'll move on to concentration we go to

211
00:17:03,190 --> 00:17:07,360
 concentration yes very detailed

212
00:17:09,440 --> 00:17:14,060
 instructions for for the practice for taking up the

213
00:17:14,060 --> 00:17:16,320
 practice of meditation

214
00:17:16,320 --> 00:17:23,680
 are those those ascetic practices and virtues and those

215
00:17:23,680 --> 00:17:29,600
 sort of the absolute requirement to

216
00:17:30,720 --> 00:17:37,440
 before you can you know go on to the concentration on the

217
00:17:37,440 --> 00:17:42,480
 next step is the basics how important are

218
00:17:42,480 --> 00:17:50,600
 they and also in respect to the everyday people and we

219
00:17:50,600 --> 00:17:59,040
 cannot do that what is the relevance and respect

220
00:17:59,040 --> 00:18:06,080
 purity of morals is absolute necessary

221
00:18:08,000 --> 00:18:13,120
 because without the purity of morals one cannot get

222
00:18:13,120 --> 00:18:16,240
 concentration when he practices meditation

223
00:18:16,240 --> 00:18:22,000
 but the ascetic practices are just an extra extra practices

224
00:18:22,000 --> 00:18:26,880
 so if even if you or a monk

225
00:18:26,880 --> 00:18:33,120
 do not practice the ascetic practices still it is possible

226
00:18:33,120 --> 00:18:35,280
 for for you or for that monk to practice

227
00:18:35,920 --> 00:18:41,130
 meditation provided that you have moral purity so moral

228
00:18:41,130 --> 00:18:42,640
 purity is a very

229
00:18:42,640 --> 00:18:49,880
 necessary or is essential for the practice of meditation

230
00:18:49,880 --> 00:18:51,920
 that is because

231
00:18:51,920 --> 00:18:58,920
 if there is no moral purity then we we suffer from from

232
00:18:58,920 --> 00:19:02,880
 remorse or from feelings of guilt

233
00:19:03,440 --> 00:19:09,620
 say suppose my my seela is not pure so when my seela is not

234
00:19:09,620 --> 00:19:12,000
 pure i have this feeling of guilt

235
00:19:12,000 --> 00:19:17,050
 now people think that i i'm a good monk but in fact i'm bad

236
00:19:17,050 --> 00:19:20,640
 and so when i try to practice

237
00:19:20,640 --> 00:19:24,640
 meditation this thinking comes to me again and again and

238
00:19:24,640 --> 00:19:28,000
 torment me and so when there is this

239
00:19:28,000 --> 00:19:37,600
 feeling of guilt there can be no no no happiness or no joy

240
00:19:37,600 --> 00:19:40,480
 and there can be no tranquility no

241
00:19:40,480 --> 00:19:45,790
 concentration and so on and in one of the sodas the the

242
00:19:45,790 --> 00:19:49,760
 different i mean the successive stages

243
00:19:51,120 --> 00:19:55,260
 leading to the realization is given and the first one is

244
00:19:55,260 --> 00:19:59,120
 the purely moral purity so moral purity

245
00:19:59,120 --> 00:20:04,920
 helps us to be free from remorse and the freedom from

246
00:20:04,920 --> 00:20:07,440
 remorse promotes

247
00:20:07,440 --> 00:20:14,310
 joy and joy promotes happiness happiness promotes tranqu

248
00:20:14,310 --> 00:20:17,120
ility and tranquility promotes

249
00:20:18,080 --> 00:20:22,250
 another kind of comfort or happiness and then the happiness

250
00:20:22,250 --> 00:20:25,280
 of mind and body promotes samadhi

251
00:20:25,280 --> 00:20:30,110
 concentration so in order to get concentration we need some

252
00:20:30,110 --> 00:20:32,480
 kind of comfort or happiness

253
00:20:32,480 --> 00:20:37,460
 happiness in the sense of peacefulness so moral purity is

254
00:20:37,460 --> 00:20:38,880
 very important

255
00:20:41,040 --> 00:20:47,270
 but the ascetic practices are just an extra extra practices

256
00:20:47,270 --> 00:20:52,320
 and for lay people it is not difficult to

257
00:20:52,320 --> 00:20:59,930
 to to get moral purity because even though they are they

258
00:20:59,930 --> 00:21:03,600
 are more moral habits are not pure

259
00:21:04,560 --> 00:21:08,680
 in the past so before the practice of meditation they can

260
00:21:08,680 --> 00:21:12,480
 make up their mind that they will not do

261
00:21:12,480 --> 00:21:17,920
 to break rules in the future and they will keep their moral

262
00:21:17,920 --> 00:21:21,280
 habits pure and take precepts and then

263
00:21:21,280 --> 00:21:25,340
 that's all there is to it but for monks it is not so easy

264
00:21:25,340 --> 00:21:28,480
 because they have to do some

265
00:21:29,680 --> 00:21:36,860
 some that there are some offenses which cannot be which

266
00:21:36,860 --> 00:21:41,040
 cannot be exonerated

267
00:21:41,040 --> 00:21:46,560
 just by confession some require confession only and some

268
00:21:46,560 --> 00:21:48,320
 require confession and then

269
00:21:48,320 --> 00:21:53,650
 giving up of the things involved in it and then some

270
00:21:53,650 --> 00:21:57,360
 require to stay for under provision for

271
00:21:58,000 --> 00:22:04,000
 as long as one hides his own offense suppose i touch a

272
00:22:04,000 --> 00:22:05,920
 woman with with

273
00:22:05,920 --> 00:22:13,390
 lusty thoughts so and that is an offense and if i do not

274
00:22:13,390 --> 00:22:16,640
 declare this offense to another monk

275
00:22:16,640 --> 00:22:23,950
 say for 10 days then i must be under probation for 10 days

276
00:22:23,950 --> 00:22:26,480
 and if i cover it up for one one month

277
00:22:26,480 --> 00:22:31,380
 then i must be under probation for one month and so on and

278
00:22:31,380 --> 00:22:36,240
 then it and i need monks to to assemble

279
00:22:36,240 --> 00:22:42,440
 and do some some kind of formal act to to take me back into

280
00:22:42,440 --> 00:22:47,440
 the fold of sangha so such offenses are

281
00:22:47,440 --> 00:22:52,670
 not easy to get to get rid of so for a monk it is more more

282
00:22:52,670 --> 00:22:56,080
 difficult to get purity of morals than

283
00:22:56,080 --> 00:23:03,060
 for lay people because the lay precepts are fewer lay

284
00:23:03,060 --> 00:23:09,120
 precepts are fewer very yes the minimum

285
00:23:09,120 --> 00:23:12,930
 requirement for lay people is only five precepts not

286
00:23:12,930 --> 00:23:16,320
 killing not stealing no sexual misconduct no

287
00:23:16,320 --> 00:23:21,360
 lying and no intoxicants so these are the five and some

288
00:23:21,360 --> 00:23:24,880
 people may may break one or two of these rules

289
00:23:24,880 --> 00:23:30,110
 but before the practice of meditation that person really

290
00:23:30,110 --> 00:23:36,320
 sincerely decided decides to refrain from

291
00:23:36,320 --> 00:23:40,910
 breaking these rules in the future and to keep his moral

292
00:23:40,910 --> 00:23:43,520
 conduct pure during meditation then

293
00:23:43,520 --> 00:23:47,570
 that's all right for him so he he is said to be pure in his

294
00:23:47,570 --> 00:23:50,480
 moral habits but a monk must do

295
00:23:50,480 --> 00:23:54,680
 something more than just uh making up his mind or

296
00:23:54,680 --> 00:23:58,480
 confession so it is it is more difficult for a

297
00:23:58,480 --> 00:24:04,850
 monk to get purity of morals than lay persons that doesn't

298
00:24:04,850 --> 00:24:07,120
 make some sense but if a lay person

299
00:24:07,120 --> 00:24:14,180
 can just make up his mind and then achieve peace of mind

300
00:24:14,180 --> 00:24:17,360
 because a monk has broken the rule

301
00:24:18,640 --> 00:24:22,900
 laid down by the buddha the five precepts are not laid down

302
00:24:22,900 --> 00:24:24,960
 by the buddha but they are

303
00:24:24,960 --> 00:24:30,560
 something like uh universal precepts but the rules uh to be

304
00:24:30,560 --> 00:24:33,120
 followed by monks are laid down by the

305
00:24:33,120 --> 00:24:39,220
 buddha and so when i break a rule i break the rule and also

306
00:24:39,220 --> 00:24:42,640
 uh i show some disrespect for the buddha

307
00:24:42,640 --> 00:24:46,280
 for the one who who laid down the rules so there's a double

308
00:24:46,280 --> 00:24:49,200
 something like a double offense there

309
00:24:49,200 --> 00:24:52,890
 breaking one breaking rule is one offense and then

310
00:24:52,890 --> 00:24:55,280
 disrespect for the for the buddha is another

311
00:24:55,280 --> 00:25:02,490
 two two things so monks have to to to get free from such

312
00:25:02,490 --> 00:25:06,160
 offenses by following some procedure

313
00:25:07,440 --> 00:25:12,380
 some offenses just by a confession confessing to another

314
00:25:12,380 --> 00:25:14,320
 monk but some require

315
00:25:14,320 --> 00:25:19,480
 something like being under probation for for some days or

316
00:25:19,480 --> 00:25:21,920
 some months so it is

317
00:25:21,920 --> 00:25:26,840
 more difficult for monks to be pure in more morals than lay

318
00:25:26,840 --> 00:25:29,520
 people and probation means

319
00:25:31,200 --> 00:25:35,930
 that means first you must ask the the sangha to assemble

320
00:25:35,930 --> 00:25:41,440
 and then formally formally

321
00:25:41,440 --> 00:25:48,610
 give give you or formally uh recognize you as being under

322
00:25:48,610 --> 00:25:50,880
 probation and when you are under

323
00:25:50,880 --> 00:25:57,060
 probation you are not to enjoy being given respect by

324
00:25:57,060 --> 00:26:01,680
 younger monks and at the dining dining hall you

325
00:26:01,680 --> 00:26:06,370
 have to to sit at the end of a line although you may be the

326
00:26:06,370 --> 00:26:09,920
 eldest of the monks there so something

327
00:26:09,920 --> 00:26:15,830
 like that a kind of punishment and then at the end you need

328
00:26:15,830 --> 00:26:18,560
 20 monks to assemble

329
00:26:20,160 --> 00:26:24,840
 to to perform a a formal act of sangha say to to take you

330
00:26:24,840 --> 00:26:27,440
 back into the fold of sangha

331
00:26:27,440 --> 00:26:32,670
 so well while under probation you do not enjoy all the all

332
00:26:32,670 --> 00:26:34,880
 the privilege of a monk

333
00:26:34,880 --> 00:26:43,590
 and you are not to not to not to accept respect from

334
00:26:43,590 --> 00:26:44,320
 younger monks

335
00:26:45,920 --> 00:26:50,060
 and you are not to sleep under the same roof with another

336
00:26:50,060 --> 00:26:50,480
 monk

337
00:26:50,480 --> 00:26:58,190
 so it is more difficult for a monk than a layperson to get

338
00:26:58,190 --> 00:26:59,520
 purity of morals before

339
00:26:59,520 --> 00:27:02,480
 the practice of meditation

340
00:27:08,480 --> 00:27:12,010
 that brings up another question that you have mentioned in

341
00:27:12,010 --> 00:27:16,240
 here about that concept idea

342
00:27:16,240 --> 00:27:23,840
 is that controversy or confusion arises because

343
00:27:27,280 --> 00:27:36,520
 are there some kind of practices that might be similar to

344
00:27:36,520 --> 00:27:39,120
 the ascetic practices

345
00:27:39,120 --> 00:27:44,160
 which is done merely for the purpose of

346
00:27:44,160 --> 00:27:53,120
 attaining a certain type of power or you know the psychic

347
00:27:53,120 --> 00:27:55,840
 power and magical powers and so on but

348
00:27:57,040 --> 00:28:00,390
 the person who is doing those practices which are similar

349
00:28:00,390 --> 00:28:01,360
 to the ascetic practices

350
00:28:01,360 --> 00:28:07,530
 may not have the virtue and you know for those in mind then

351
00:28:07,530 --> 00:28:11,440
 practices to to get sort of the

352
00:28:11,440 --> 00:28:15,920
 worldly type of power that's very similar and that might

353
00:28:15,920 --> 00:28:18,560
 have confused this thing about the

354
00:28:18,560 --> 00:28:23,520
 between that and the ascetic practices that I mentioned

355
00:28:23,520 --> 00:28:26,480
 here because under by the definition

356
00:28:26,480 --> 00:28:32,290
 already you know it is not done with the wholesomeness and

357
00:28:32,290 --> 00:28:34,960
 so on then it is not an ascetic practice

358
00:28:34,960 --> 00:28:40,100
 anymore is that some practice like that? I think the the

359
00:28:40,100 --> 00:28:43,040
 difference of opinion is whether

360
00:28:43,040 --> 00:28:48,910
 the ascetic practices are to be included in in in the

361
00:28:48,910 --> 00:28:52,400
 categories of wholesome and wholesome

362
00:28:53,520 --> 00:28:59,230
 and neither wholesome nor unwholesome and the those people

363
00:28:59,230 --> 00:29:01,440
 took ascetic practices to be just

364
00:29:01,440 --> 00:29:07,540
 outside of these three and so to them it is just a concept

365
00:29:07,540 --> 00:29:12,480
 so there again there is no no

366
00:29:14,240 --> 00:29:20,790
 reality to represent these practices but in according to

367
00:29:20,790 --> 00:29:25,440
 the opinion of the visodimaga and

368
00:29:25,440 --> 00:29:31,760
 so opinion of common common opinion of elders the the asc

369
00:29:31,760 --> 00:29:37,520
etic practice is reality because

370
00:29:39,120 --> 00:29:44,510
 when you practice when you take up these practices then you

371
00:29:44,510 --> 00:29:48,320
 you have the pollution in your mind and

372
00:29:48,320 --> 00:29:53,850
 also the understanding or the knowledge of it and so they

373
00:29:53,850 --> 00:29:57,360
 they are the units of reality parameter

374
00:29:58,960 --> 00:30:04,210
 but those those other monks took the practices to be just

375
00:30:04,210 --> 00:30:11,120
 concepts and so the the the argument from

376
00:30:11,120 --> 00:30:18,540
 the side of Venerable Boto Boto Gosa is if they are concept

377
00:30:18,540 --> 00:30:22,240
 then concept has no existence of its own

378
00:30:24,640 --> 00:30:29,150
 it exists only in the mind's imagination and so they cannot

379
00:30:29,150 --> 00:30:32,000
 be realities but

380
00:30:32,000 --> 00:30:38,240
 the ascetic practices are or belong to reality one of the

381
00:30:38,240 --> 00:30:41,440
 four four ultimate truths

382
00:30:41,440 --> 00:30:46,380
 the consciousness mental factors material properties and n

383
00:30:46,380 --> 00:30:47,200
irvana so

384
00:30:50,240 --> 00:30:56,670
 ascetic practices are not concepts but they are realities

385
00:30:56,670 --> 00:31:00,000
 and so they they belong to either

386
00:31:00,000 --> 00:31:07,280
 wholesome or in determinate they cannot be

387
00:31:07,280 --> 00:31:15,530
 unwholesome and they cannot be outside of wholesome and

388
00:31:15,530 --> 00:31:18,640
 wholesome and in determinate either

389
00:31:18,640 --> 00:31:32,480
 are there practices that just from a historical aspect that

390
00:31:32,480 --> 00:31:42,240
 the people do those type of very similar to that in order

391
00:31:42,240 --> 00:31:43,920
 to attain some kind of

392
00:31:43,920 --> 00:31:49,060
 yeah that's right but they they cannot be called ascetic

393
00:31:49,060 --> 00:31:52,720
 practices because they they promote

394
00:31:52,720 --> 00:31:58,850
 mental defilements they promote creed or some kind of

395
00:31:58,850 --> 00:32:01,840
 attachment and so on yeah

396
00:32:01,840 --> 00:32:07,200
 okay

397
00:32:07,200 --> 00:32:14,480
 so

398
00:32:15,920 --> 00:32:21,760
 so

399
00:32:21,760 --> 00:32:29,040
 so

400
00:32:29,040 --> 00:32:36,320
 so

401
00:32:36,320 --> 00:32:43,600
 so

402
00:32:43,600 --> 00:32:50,880
 so

403
00:32:50,880 --> 00:32:58,160
 so

404
00:32:58,160 --> 00:33:05,440
 so

405
00:33:05,440 --> 00:33:12,720
 so

