1
00:00:00,000 --> 00:00:09,120
 So, okay, this is the second in my series of how tos on

2
00:00:09,120 --> 00:00:11,280
 meditation.

3
00:00:11,280 --> 00:00:18,440
 So the first clip was on basic sitting meditation.

4
00:00:18,440 --> 00:00:24,140
 And I apologize that I'm restricted to 10 megabytes, no 10

5
00:00:24,140 --> 00:00:26,400
 minutes on YouTube, so we

6
00:00:26,400 --> 00:00:29,220
 weren't able to do a full three minutes or a few minutes of

7
00:00:29,220 --> 00:00:30,080
 meditation.

8
00:00:30,080 --> 00:00:33,540
 So in this one I'm going to have you pause when we do

9
00:00:33,540 --> 00:00:35,200
 guided meditation.

10
00:00:35,200 --> 00:00:40,320
 If you're interested you can sit, push pause, and sit for

11
00:00:40,320 --> 00:00:44,600
 maybe 30 seconds or a minute or

12
00:00:44,600 --> 00:00:45,600
 so.

13
00:00:45,600 --> 00:00:48,000
 So today I'm going to go over advanced techniques in

14
00:00:48,000 --> 00:00:49,200
 sitting meditation.

15
00:00:49,200 --> 00:00:51,840
 And they're not so advanced so that you should be scared

16
00:00:51,840 --> 00:00:53,440
 away from listening to this.

17
00:00:53,440 --> 00:00:58,060
 It just means that once you understand the basics of

18
00:00:58,060 --> 00:01:01,360
 sitting meditation already, then

19
00:01:01,360 --> 00:01:06,530
 what do you do to deal with the problems and the phenomena

20
00:01:06,530 --> 00:01:09,800
 which arise during meditation?

21
00:01:09,800 --> 00:01:14,380
 So last time we talked about the rising and the falling of

22
00:01:14,380 --> 00:01:15,720
 the abdomen.

23
00:01:15,720 --> 00:01:20,330
 And we used that as our meditation object, saying rising,

24
00:01:20,330 --> 00:01:22,840
 falling as it rises and falls.

25
00:01:22,840 --> 00:01:25,880
 But the problem with that is at the time when we're saying

26
00:01:25,880 --> 00:01:27,760
 rising, falling, or we're trying

27
00:01:27,760 --> 00:01:32,600
 to watch the abdomen, our mind starts to wander.

28
00:01:32,600 --> 00:01:37,160
 Sometimes we have pain or aching in our body.

29
00:01:37,160 --> 00:01:40,120
 Sometimes we have thoughts, thinking about the past or

30
00:01:40,120 --> 00:01:41,960
 thinking about the future, with

31
00:01:41,960 --> 00:01:44,760
 thoughts, bad thoughts.

32
00:01:44,760 --> 00:01:47,710
 And sometimes we have emotions arise, liking this, liking

33
00:01:47,710 --> 00:01:49,560
 that, disliking this, disliking

34
00:01:49,560 --> 00:01:54,840
 that, drowsiness, laziness, distraction, or doubt.

35
00:01:54,840 --> 00:01:58,640
 These sort of things will arise during the time.

36
00:01:58,640 --> 00:02:01,560
 And many other things, but this is sort of an example.

37
00:02:01,560 --> 00:02:04,920
 So all of these things I've mentioned, they come under a

38
00:02:04,920 --> 00:02:06,960
 heading which is called the Four

39
00:02:06,960 --> 00:02:07,960
 Foundations of Mindfulness.

40
00:02:07,960 --> 00:02:12,040
 And this is a Buddhist term, but it's a meditation term.

41
00:02:12,040 --> 00:02:15,280
 It's a very important thing in the practice of meditation.

42
00:02:15,280 --> 00:02:17,040
 There are four foundations.

43
00:02:17,040 --> 00:02:21,250
 The first thing is, during the time you're practicing, you

44
00:02:21,250 --> 00:02:23,200
 have to guard these four stations,

45
00:02:23,200 --> 00:02:26,200
 or these four foundations.

46
00:02:26,200 --> 00:02:28,560
 What it means, the first one is the body.

47
00:02:28,560 --> 00:02:31,380
 And this is what we're starting when we watch rising,

48
00:02:31,380 --> 00:02:32,080
 falling.

49
00:02:32,080 --> 00:02:35,280
 The point is, we can use this as our meditation object.

50
00:02:35,280 --> 00:02:36,880
 It's called the body.

51
00:02:36,880 --> 00:02:38,160
 And this is very simple.

52
00:02:38,160 --> 00:02:40,080
 This one we already understand.

53
00:02:40,080 --> 00:02:41,240
 But what happens when we have feelings?

54
00:02:41,240 --> 00:02:44,120
 Well, the second foundation of mindfulness is called

55
00:02:44,120 --> 00:02:44,880
 feelings.

56
00:02:44,880 --> 00:02:50,050
 And this means that we need to have an answer to pain, or a

57
00:02:50,050 --> 00:02:53,120
ching, or soreness that arises

58
00:02:53,120 --> 00:02:54,120
 when we're sitting.

59
00:02:54,120 --> 00:02:57,630
 And so this is our answer, is to use the pain as a

60
00:02:57,630 --> 00:02:59,440
 meditation object.

61
00:02:59,440 --> 00:03:01,890
 At the time when we feel pain in the legs, or in the back,

62
00:03:01,890 --> 00:03:03,200
 or so on, instead of saying

63
00:03:03,200 --> 00:03:06,540
 rising, falling, and focusing on the stomach, instead shift

64
00:03:06,540 --> 00:03:08,000
 your attention to the pain,

65
00:03:08,000 --> 00:03:11,690
 or the aching, to the part of the body which is causing

66
00:03:11,690 --> 00:03:12,760
 suffering.

67
00:03:12,760 --> 00:03:18,540
 And say to yourself, pain, pain, pain, say it calmly, say

68
00:03:18,540 --> 00:03:21,320
 it quietly in your mind, you

69
00:03:21,320 --> 00:03:27,610
 don't say it out loud, only to keep the mind fixed on the

70
00:03:27,610 --> 00:03:30,600
 reality of the object.

71
00:03:30,600 --> 00:03:33,200
 The pain itself is not a bad thing.

72
00:03:33,200 --> 00:03:35,710
 What creates suffering is when we start to say, this pain

73
00:03:35,710 --> 00:03:37,080
 is bad, or this pain is me,

74
00:03:37,080 --> 00:03:40,390
 or this pain is caused by sitting meditations, or sitting

75
00:03:40,390 --> 00:03:42,200
 meditation is bad, or so on.

76
00:03:42,200 --> 00:03:44,000
 The pain itself is not bad.

77
00:03:44,000 --> 00:03:49,270
 And if we learn to become friends with it, learn to retrain

78
00:03:49,270 --> 00:03:51,440
 our mind, realizing that

79
00:03:51,440 --> 00:03:55,460
 it's only pain and nothing else, then we can be happy, and

80
00:03:55,460 --> 00:03:56,960
 we can be at peace.

81
00:03:56,960 --> 00:03:57,960
 This is called feelings.

82
00:03:57,960 --> 00:04:00,730
 When we feel happy, we can also say happy, happy when we

83
00:04:00,730 --> 00:04:02,280
 feel calm, we should also say

84
00:04:02,280 --> 00:04:06,690
 calm, calm, calm, being aware that these also are transit

85
00:04:06,690 --> 00:04:07,280
ory.

86
00:04:07,280 --> 00:04:08,280
 They're impermanent.

87
00:04:08,280 --> 00:04:10,640
 They come and they go.

88
00:04:10,640 --> 00:04:15,020
 Number three, the third foundation of mindfulness, we have

89
00:04:15,020 --> 00:04:16,960
 body, we have feelings.

90
00:04:16,960 --> 00:04:17,960
 The third one is the mind.

91
00:04:17,960 --> 00:04:21,280
 So we have to have an answer for what to do when our mind

92
00:04:21,280 --> 00:04:22,680
 starts to wander.

93
00:04:22,680 --> 00:04:25,620
 When we're sitting, but our mind is not with our body, our

94
00:04:25,620 --> 00:04:27,000
 mind is not with the rising

95
00:04:27,000 --> 00:04:30,480
 and falling, it's thinking this, thinking that.

96
00:04:30,480 --> 00:04:35,450
 The answer is to say to ourselves, thinking, thinking,

97
00:04:35,450 --> 00:04:36,640
 thinking.

98
00:04:36,640 --> 00:04:41,100
 And when we say thinking, thinking, our mind will come back

99
00:04:41,100 --> 00:04:42,240
 to reality.

100
00:04:42,240 --> 00:04:46,420
 Thinking will disappear and we can come back to rising and

101
00:04:46,420 --> 00:04:47,360
 falling.

102
00:04:47,360 --> 00:04:50,760
 But this helps us to be clearly about the mind, to see

103
00:04:50,760 --> 00:04:52,880
 clearly the nature of our mind,

104
00:04:52,880 --> 00:04:55,770
 and we can start to see what is it that makes our mind,

105
00:04:55,770 --> 00:04:57,640
 what sort of activities make our

106
00:04:57,640 --> 00:05:02,670
 mind distracted, and what sort of activities are going to

107
00:05:02,670 --> 00:05:05,440
 bring focus and tranquility to

108
00:05:05,440 --> 00:05:06,440
 the mind.

109
00:05:06,440 --> 00:05:09,840
 This is the third foundation.

110
00:05:09,840 --> 00:05:13,870
 The fourth foundation is called Dhamma, and it's

111
00:05:13,870 --> 00:05:17,280
 specifically a Buddhist thing, but it

112
00:05:17,280 --> 00:05:20,720
 means anything the mind takes as an object.

113
00:05:20,720 --> 00:05:21,720
 It can mean anything.

114
00:05:21,720 --> 00:05:24,280
 But in the beginning, we're focusing on those things which

115
00:05:24,280 --> 00:05:25,840
 the mind will take as an object

116
00:05:25,840 --> 00:05:28,200
 which are important in meditation.

117
00:05:28,200 --> 00:05:30,200
 We're only teaching meditation here.

118
00:05:30,200 --> 00:05:34,800
 We're not trying to get into the Buddhist doctrine or

119
00:05:34,800 --> 00:05:38,600
 philosophy or so on, not necessarily.

120
00:05:38,600 --> 00:05:41,060
 There are five things which meditators have to keep in mind

121
00:05:41,060 --> 00:05:42,280
 in the very beginning when

122
00:05:42,280 --> 00:05:43,280
 they start to practice.

123
00:05:43,280 --> 00:05:48,860
 These are called, in simple terms, liking, disliking, drows

124
00:05:48,860 --> 00:05:51,600
iness, distraction, and doubt.

125
00:05:51,600 --> 00:05:55,870
 That's the liking, disliking, drowsiness, distraction,

126
00:05:55,870 --> 00:05:56,600
 doubt.

127
00:05:56,600 --> 00:05:59,890
 These things are important because when these come up in

128
00:05:59,890 --> 00:06:01,960
 the mind, it will not be possible

129
00:06:01,960 --> 00:06:05,260
 or it will be extremely difficult to keep the mind focused,

130
00:06:05,260 --> 00:06:06,680
 to keep the mind with the

131
00:06:06,680 --> 00:06:12,680
 present moment, with the object that we're trying to watch.

132
00:06:12,680 --> 00:06:15,150
 These things we have to be aware of, and this is the fourth

133
00:06:15,150 --> 00:06:15,920
 foundation.

134
00:06:15,920 --> 00:06:16,920
 It's called Dhamma.

135
00:06:16,920 --> 00:06:19,080
 These five things are called the five hindrances.

136
00:06:19,080 --> 00:06:22,650
 They're part of the Buddhist teaching, part of the Dhamma,

137
00:06:22,650 --> 00:06:23,800
 part of reality.

138
00:06:23,800 --> 00:06:27,000
 They're part of reality that's important for meditators.

139
00:06:27,000 --> 00:06:30,660
 If you like something, you want something, we can say to

140
00:06:30,660 --> 00:06:32,600
 ourselves, "When we're sitting,

141
00:06:32,600 --> 00:06:35,640
 rising, falling, we start to like," or "We start to want."

142
00:06:35,640 --> 00:06:38,670
 Say to ourselves, instead of saying, "Rising, falling,"

143
00:06:38,670 --> 00:06:40,560
 switch to say, "Liking, liking,

144
00:06:40,560 --> 00:06:41,560
 liking, wanting."

145
00:06:41,560 --> 00:06:45,600
 Learn, teach yourselves to let go.

146
00:06:45,600 --> 00:06:51,860
 Retrain our minds not to get fixed and get obsessed with

147
00:06:51,860 --> 00:06:52,520
 things.

148
00:06:52,520 --> 00:06:54,640
 Number one, number two, disliking.

149
00:06:54,640 --> 00:06:58,430
 We have disliking or anger or upset, so we say to ourselves

150
00:06:58,430 --> 00:07:00,280
, "Disliking, disliking,

151
00:07:00,280 --> 00:07:03,380
 upset, upset," reminding ourselves, "There's only an

152
00:07:03,380 --> 00:07:05,960
 emotion, there's no need to get overworked.

153
00:07:05,960 --> 00:07:11,040
 There's no need to get stressed out about this or freaked

154
00:07:11,040 --> 00:07:13,040
 out or upset or so on.

155
00:07:13,040 --> 00:07:14,440
 It's only an emotion."

156
00:07:14,440 --> 00:07:19,880
 We say, "Disliking, disliking," until it goes away.

157
00:07:19,880 --> 00:07:23,160
 The third one is called drowsiness.

158
00:07:23,160 --> 00:07:26,100
 This is some people when they meditate, they find

159
00:07:26,100 --> 00:07:27,960
 themselves falling asleep.

160
00:07:27,960 --> 00:07:30,010
 It's quite difficult, and in the beginning, you normally

161
00:07:30,010 --> 00:07:31,120
 fall asleep when you meditate.

162
00:07:31,120 --> 00:07:32,120
 This is normal.

163
00:07:32,120 --> 00:07:33,120
 There's nothing wrong.

164
00:07:33,120 --> 00:07:36,120
 It's the beginner mind or it's the beginning.

165
00:07:36,120 --> 00:07:39,280
 It's a start to meditation.

166
00:07:39,280 --> 00:07:45,680
 When you feel drowsy, only say to yourself, "Drowsy, drowsy

167
00:07:45,680 --> 00:07:47,280
, drowsy."

168
00:07:47,280 --> 00:07:50,160
 Try to catch the nature of the drowsiness.

169
00:07:50,160 --> 00:07:52,590
 You find if you work on it, after some time, you can

170
00:07:52,590 --> 00:07:54,440
 totally overcome the drowsiness.

171
00:07:54,440 --> 00:07:58,360
 Of course, walking meditation helps, and that will be my

172
00:07:58,360 --> 00:07:59,520
 next episode.

173
00:07:59,520 --> 00:08:03,120
 The fourth is called distraction.

174
00:08:03,120 --> 00:08:05,440
 Distraction here is, we put a specific meaning on it.

175
00:08:05,440 --> 00:08:08,500
 It means specifically when you're thinking too much, when

176
00:08:08,500 --> 00:08:09,760
 you're thinking beyond what

177
00:08:09,760 --> 00:08:12,570
 is normal, when the mind is not focused, cannot stay with

178
00:08:12,570 --> 00:08:14,720
 any one object, it's thinking here,

179
00:08:14,720 --> 00:08:17,480
 thinking there, thinking all over the place, all the time.

180
00:08:17,480 --> 00:08:18,480
 This we call distracted.

181
00:08:18,480 --> 00:08:22,080
 This is also a hindrance in meditation.

182
00:08:22,080 --> 00:08:24,580
 We say to ourselves, when we feel distracted, we say

183
00:08:24,580 --> 00:08:26,720
 distracted, distracted, distracted.

184
00:08:26,720 --> 00:08:28,560
 The fifth one is doubt.

185
00:08:28,560 --> 00:08:32,740
 Doubt about ourselves, doubt about our practice, doubt

186
00:08:32,740 --> 00:08:35,200
 about this, doubt about that.

187
00:08:35,200 --> 00:08:37,680
 In meditation practice, doubt is a hindrance.

188
00:08:37,680 --> 00:08:42,030
 We're not talking about theology or philosophy or Buddhist

189
00:08:42,030 --> 00:08:43,560
 doctrine or so on.

190
00:08:43,560 --> 00:08:45,440
 We're talking about meditation.

191
00:08:45,440 --> 00:08:48,840
 In meditation, doubt is a bad thing.

192
00:08:48,840 --> 00:08:49,840
 Why?

193
00:08:49,840 --> 00:08:54,400
 Because it disturbs the concentration and the focus of the

194
00:08:54,400 --> 00:08:55,120
 mind.

195
00:08:55,120 --> 00:09:00,970
 When we feel doubt, we have to say to ourselves, doubting,

196
00:09:00,970 --> 00:09:03,440
 doubting, doubting.

197
00:09:03,440 --> 00:09:05,640
 These together are the five hindrances.

198
00:09:05,640 --> 00:09:07,040
 The fourth foundation of mindfulness.

199
00:09:07,040 --> 00:09:10,480
 Altogether, we have body, feelings, mind and mind objects.

200
00:09:10,480 --> 00:09:14,980
 These are the basics of meditation practice in the Buddhist

201
00:09:14,980 --> 00:09:16,120
 tradition.

202
00:09:16,120 --> 00:09:17,120
 1

203
00:09:17,120 --> 00:09:18,120
 1

204
00:09:18,120 --> 00:09:19,120
 1

205
00:09:19,120 --> 00:09:20,120
 1

206
00:09:20,120 --> 00:09:21,120
 1

207
00:09:21,120 --> 00:09:22,120
 1

208
00:09:22,120 --> 00:09:23,120
 1

209
00:09:23,120 --> 00:09:24,120
 1

210
00:09:24,120 --> 00:09:25,120
 1

211
00:09:25,120 --> 00:09:26,120
 1

212
00:09:26,120 --> 00:09:27,120
 1

213
00:09:27,120 --> 00:09:28,120
 1

214
00:09:28,120 --> 00:09:29,120
 1

215
00:09:29,120 --> 00:09:30,120
 1

216
00:09:30,120 --> 00:09:31,120
 1

217
00:09:31,120 --> 00:09:32,120
 1

218
00:09:32,120 --> 00:09:33,120
 1

219
00:09:33,120 --> 00:09:34,120
 1

220
00:09:34,120 --> 00:09:35,120
 1

221
00:09:35,120 --> 00:09:36,120
 1

222
00:09:36,120 --> 00:09:37,120
 1

223
00:09:37,120 --> 00:09:38,120
 1

224
00:09:38,120 --> 00:09:39,120
 1

225
00:09:39,120 --> 00:09:40,120
 1

226
00:09:40,120 --> 00:09:41,120
 1

227
00:09:41,120 --> 00:09:42,120
 1

228
00:09:42,120 --> 00:09:43,120
 1

229
00:09:43,120 --> 00:09:44,120
 1

230
00:09:44,120 --> 00:09:45,120
 1

231
00:09:45,120 --> 00:09:46,120
 1

232
00:09:46,120 --> 00:09:47,120
 1

233
00:09:47,120 --> 00:09:48,120
 1

234
00:09:48,120 --> 00:09:49,120
 1

235
00:09:49,120 --> 00:09:50,120
 1

236
00:09:50,120 --> 00:09:51,120
 1

237
00:09:51,120 --> 00:09:52,120
 1

238
00:09:52,120 --> 00:09:53,120
 1

239
00:09:53,120 --> 00:09:54,120
 1

240
00:09:54,120 --> 00:09:55,120
 1

241
00:09:55,120 --> 00:09:56,120
 1

242
00:09:56,120 --> 00:09:57,120
 1

