1
00:00:00,000 --> 00:00:06,400
 Do Buddhists believe in Chi as Taoists do and if so, is

2
00:00:06,400 --> 00:00:10,040
 there a method of generating this energy in oneself through

3
00:00:10,040 --> 00:00:12,280
 meditation?

4
00:00:12,280 --> 00:00:17,330
 Etc. I feel like I've been seeking protection through

5
00:00:17,330 --> 00:00:18,120
 meditation

6
00:00:18,120 --> 00:00:23,340
 Until I realized that I shouldn't be seeking protection but

7
00:00:23,340 --> 00:00:27,840
 maybe rather generating this power within myself

8
00:00:29,800 --> 00:00:31,800
 What do you think Paul

9
00:00:31,800 --> 00:00:37,510
 Protection through meditation I think is definitely

10
00:00:37,510 --> 00:00:38,240
 possible

11
00:00:38,240 --> 00:00:41,560
 I

12
00:00:41,560 --> 00:00:43,560
 Why and it protects me a lot?

13
00:00:43,560 --> 00:00:50,310
 Keeps me on even cue reduces the amount of suffering I go

14
00:00:50,310 --> 00:00:51,440
 through

15
00:00:54,360 --> 00:00:57,420
 I'm not sure if Buddhist believe in Chi. I don't think I

16
00:00:57,420 --> 00:00:59,080
 personally believe in Chi

17
00:00:59,080 --> 00:01:05,760
 Some of the

18
00:01:05,760 --> 00:01:11,320
 And some of the martial arts, you know, they have some

19
00:01:11,320 --> 00:01:14,480
 people teach this Chi as some kind of energy and

20
00:01:14,480 --> 00:01:18,300
 Some of the doctors just look at it as blood flow

21
00:01:19,720 --> 00:01:23,590
 This I've seen that talk before where it's not a mystical

22
00:01:23,590 --> 00:01:25,400
 energy, but it's

23
00:01:25,400 --> 00:01:28,320
 the blood flowing in your body

24
00:01:28,320 --> 00:01:32,810
 But um, yeah, I don't ask for the energy part. I don't

25
00:01:32,810 --> 00:01:34,400
 think that would be more of a

26
00:01:34,400 --> 00:01:40,340
 Buddhist view it's almost like you're saying the soul or a

27
00:01:40,340 --> 00:01:43,840
 entity

28
00:01:43,840 --> 00:01:46,500
 All of these questions all of these questions that started

29
00:01:46,500 --> 00:01:47,560
 to Buddhists believe

30
00:01:49,440 --> 00:01:51,440
 Should be answered as

31
00:01:51,440 --> 00:01:54,400
 case freedom and under

32
00:01:54,400 --> 00:01:57,690
 There's a wonderful video teaching that is I think quite

33
00:01:57,690 --> 00:02:00,920
 famous out there talking to gave in Malaysia, I think

34
00:02:00,920 --> 00:02:03,520
 Singapore or Malaysia, I'm not sure

35
00:02:03,520 --> 00:02:07,100
 Where he's talking about because he wrote a book called

36
00:02:07,100 --> 00:02:09,000
 what Buddhists believe and

37
00:02:09,000 --> 00:02:13,480
 He was in he said I was interviewed once by someone who?

38
00:02:14,080 --> 00:02:17,040
 About this book and he said so you've written this book

39
00:02:17,040 --> 00:02:20,240
 what Buddhists believe so tell me what do Buddhists believe

40
00:02:20,240 --> 00:02:20,240
?

41
00:02:20,240 --> 00:02:23,440
 And he said absolutely nothing

42
00:02:23,440 --> 00:02:27,280
 And the man said then why did you write this book?

43
00:02:27,280 --> 00:02:30,320
 And he said and I told him

44
00:02:30,320 --> 00:02:34,450
 That's exactly why I wrote this book to show you that there

45
00:02:34,450 --> 00:02:36,200
's nothing nothing to believe

46
00:02:36,200 --> 00:02:40,580
 You don't need to believe Buddha taught Buddhism the Buddha

47
00:02:40,580 --> 00:02:42,440
 taught to know to understand

48
00:02:44,400 --> 00:02:46,660
 But I think we shouldn't miss this point that Buddhists don

49
00:02:46,660 --> 00:02:50,410
't believe anything because literally it's it's very

50
00:02:50,410 --> 00:02:51,280
 important

51
00:02:51,280 --> 00:02:55,280
 This kind of question is is

52
00:02:55,280 --> 00:02:58,540
 Kind of awkward for a Buddhist because we're like well. How

53
00:02:58,540 --> 00:03:00,200
 do you explain to this person?

54
00:03:00,200 --> 00:03:03,480
 But that's not what Buddhism is about it's not about

55
00:03:03,480 --> 00:03:07,400
 Yes, or no answers to questions of belief. It's about

56
00:03:07,400 --> 00:03:10,120
 seeing

57
00:03:10,120 --> 00:03:12,120
 the truth of mundane reality

58
00:03:12,120 --> 00:03:16,920
 It's about letting go rather than taking on some sort of

59
00:03:16,920 --> 00:03:21,640
 belief in in in something external to the six senses

60
00:03:21,640 --> 00:03:26,480
 Buddhism is in fact about reducing reality

61
00:03:26,480 --> 00:03:29,900
 to ultimate experience to seeing hearing smelling tasting

62
00:03:29,900 --> 00:03:32,720
 feeling thinking it's it's about getting rid of

63
00:03:32,720 --> 00:03:35,400
 concepts like Chi or

64
00:03:38,240 --> 00:03:41,260
 Even the breath for example the reason why we focus on the

65
00:03:41,260 --> 00:03:43,680
 breath is to get rid of the concept of the breath to come

66
00:03:43,680 --> 00:03:43,800
 to

67
00:03:43,800 --> 00:03:45,800
 See it as just physical and mental

68
00:03:45,800 --> 00:03:47,840
 experiences

69
00:03:47,840 --> 00:03:49,840
 But the breath itself doesn't exist

70
00:03:49,840 --> 00:03:56,600
 So things like the chakras things like Chi and

71
00:03:56,600 --> 00:03:59,290
 So on and so on and so on do you believe in this do you

72
00:03:59,290 --> 00:04:02,160
 believe in God you believe in ghosts you believe in

73
00:04:02,160 --> 00:04:05,320
 Buddhists don't believe in anything

74
00:04:06,840 --> 00:04:08,840
 We

75
00:04:08,840 --> 00:04:12,170
 We make every effort to stop believing in in absolutely

76
00:04:12,170 --> 00:04:14,960
 everything by being facetious because of course

77
00:04:14,960 --> 00:04:18,240
 The answer is yes, we believe in God's yes. We believe in

78
00:04:18,240 --> 00:04:20,640
 ghosts. Yes, we believe in X Y and Z so

79
00:04:20,640 --> 00:04:26,700
 Right you this the same as the question of astrology

80
00:04:26,700 --> 00:04:29,130
 I mean what you're saying you're asking is like wow now I

81
00:04:29,130 --> 00:04:30,720
 have to have an opinion on this now

82
00:04:30,720 --> 00:04:32,790
 I have to go out and study Chi energy and give you an

83
00:04:32,790 --> 00:04:34,320
 answer because it's not Buddhism

84
00:04:36,200 --> 00:04:37,290
 It's there's nothing Buddhist about me as to whether I

85
00:04:37,290 --> 00:04:40,400
 believe in cheer or not it has more to do with whether I've

86
00:04:40,400 --> 00:04:41,280
 investigated

87
00:04:41,280 --> 00:04:43,440
 It and have any reason

88
00:04:43,440 --> 00:04:46,050
 Totally apart from Buddhism to believe in it because Buddha

89
00:04:46,050 --> 00:04:46,680
 didn't teach

90
00:04:46,680 --> 00:04:50,280
 This sort of thing the Buddha taught as I said to give up

91
00:04:50,280 --> 00:04:52,000
 our beliefs to give up our

92
00:04:52,000 --> 00:04:58,350
 Obsession with concepts and to come to see ultimate reality

93
00:04:58,350 --> 00:05:00,080
 for what it is so

94
00:05:00,440 --> 00:05:03,080
 The only reason I might believe in Chi is because I had

95
00:05:03,080 --> 00:05:06,000
 studied it which wouldn't have come about in in Buddhism

96
00:05:06,000 --> 00:05:12,980
 So so I guess what I'd like to impress is is an

97
00:05:12,980 --> 00:05:15,610
 understanding of exactly what the Buddha taught and what

98
00:05:15,610 --> 00:05:15,940
 Buddhism

99
00:05:15,940 --> 00:05:18,790
 Focuses on so that people will not have to ask these sorts

100
00:05:18,790 --> 00:05:19,740
 of questions it

101
00:05:19,740 --> 00:05:24,450
 You know because it's understandable you think wow look at

102
00:05:24,450 --> 00:05:25,800
 the Buddha's teaching is so big

103
00:05:25,800 --> 00:05:27,920
 I wonder if anywhere in there he taught this or well

104
00:05:27,920 --> 00:05:28,720
 anywhere in there

105
00:05:28,720 --> 00:05:32,080
 He taught that but take it from me the well don't take it

106
00:05:32,080 --> 00:05:33,060
 from me any

107
00:05:33,060 --> 00:05:36,360
 basic

108
00:05:36,360 --> 00:05:38,360
 understanding of the Buddha's teaching

109
00:05:38,360 --> 00:05:41,390
 Or what it should give you or what you should learn from

110
00:05:41,390 --> 00:05:46,000
 the Buddha from good Buddhism is that that it just doesn't

111
00:05:46,000 --> 00:05:46,520
 apply it

112
00:05:46,520 --> 00:05:49,280
 It has no place in the Buddha dhamma

113
00:05:49,280 --> 00:05:52,320
 so

114
00:05:52,320 --> 00:05:56,000
 Could Chi exist yeah sure I mean the Buddha may have even

115
00:05:56,600 --> 00:05:58,900
 I've never come across it, but he may have even talked

116
00:05:58,900 --> 00:06:00,280
 about it, but it's not I

117
00:06:00,280 --> 00:06:03,830
 Guess the point is that it's not really a question about

118
00:06:03,830 --> 00:06:06,760
 Buddhism. Is it isn't that is that it?

119
00:06:06,760 --> 00:06:10,660
 Whether Chi energy exists, and you can use it for this or

120
00:06:10,660 --> 00:06:11,080
 that

121
00:06:11,080 --> 00:06:13,820
 It's like asking whether nuclear power exists, and you can

122
00:06:13,820 --> 00:06:14,880
 use it for this or that

123
00:06:14,880 --> 00:06:17,680
 nothing to do with

124
00:06:17,680 --> 00:06:19,680
 the path

125
00:06:19,680 --> 00:06:23,080
 So I'm not trying to criticize the answer

126
00:06:23,080 --> 00:06:25,750
 I'm just I can understand where these questions come from

127
00:06:25,750 --> 00:06:27,400
 trying to criticize the question

128
00:06:27,400 --> 00:06:30,360
 just trying to help to

129
00:06:30,360 --> 00:06:33,040
 Get back on

