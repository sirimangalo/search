WEBVTT

00:00:00.000 --> 00:00:04.720
 I don't believe in matter or rupa because I've never

00:00:04.720 --> 00:00:08.440
 experienced it directly and I don't believe in anything I

00:00:08.440 --> 00:00:11.000
 don't see with my own eyes. Is this a problem?

00:00:11.000 --> 00:00:16.540
 That's interesting. I mean, you have experienced rupa

00:00:16.540 --> 00:00:20.000
 directly. You experience it all the time.

00:00:20.000 --> 00:00:24.750
 I mean, that's what you're experiencing. You're

00:00:24.750 --> 00:00:29.650
 experiencing states of hardness and softness and stiffness

00:00:29.650 --> 00:00:34.000
 and flaccidity and hardness and...

00:00:34.000 --> 00:00:39.750
 ...hot and cool. You're empirically, objectively speaking,

00:00:39.750 --> 00:00:44.310
 unless you're a strange person you're experiencing all of

00:00:44.310 --> 00:00:48.760
 those. The only one you're not able to experience is the

00:00:48.760 --> 00:00:50.000
 water element.

00:00:50.000 --> 00:00:53.100
 But with the other three elements we're experiencing all

00:00:53.100 --> 00:00:56.000
 the time, that's what it means by rupa or matter.

00:00:56.000 --> 00:01:00.360
 Matter is this aspect of experience. Not all experience is

00:01:00.360 --> 00:01:04.770
 formed with matter. Some experiences are mental where you

00:01:04.770 --> 00:01:08.340
're thinking about something, or planning something or

00:01:08.340 --> 00:01:09.000
 whatever.

00:01:09.000 --> 00:01:14.370
 But the majority of our experiences do have a physical

00:01:14.370 --> 00:01:18.760
 component. Even when you see something that's an experience

00:01:18.760 --> 00:01:20.000
 of the physical.

00:01:20.000 --> 00:01:25.670
 I think perhaps the problem is intellectually you categor

00:01:25.670 --> 00:01:31.660
ize all of this as mental because it's the experience, right

00:01:31.660 --> 00:01:32.000
?

00:01:32.000 --> 00:01:35.390
 There's the experiencer, the mind. So you think, well, it's

00:01:35.390 --> 00:01:38.000
 just a mental experience. But that's only intellectual.

00:01:38.000 --> 00:01:42.290
 The reality is you are experiencing heat and cold and

00:01:42.290 --> 00:01:47.420
 hardness and softness and stiffness and flaccidity. These

00:01:47.420 --> 00:01:51.140
 six things everyone experiences, every human being

00:01:51.140 --> 00:01:59.610
 experiences these aspects of reality which are considered

00:01:59.610 --> 00:02:00.000
 material.

00:02:00.000 --> 00:02:07.000
 They're not mental. The experience is based on the mind.

00:02:07.000 --> 00:02:10.080
 Without the mind there is no experience. But the matter

00:02:10.080 --> 00:02:11.000
 does exist.

00:02:11.000 --> 00:02:15.280
 So it might be a bit of a problem if you don't see that

00:02:15.280 --> 00:02:18.970
 clearly as it is. On the other hand, the most important

00:02:18.970 --> 00:02:21.000
 thing is to see experience for what it is.

00:02:21.000 --> 00:02:24.020
 So if you classify it as physical or as mental, it's not

00:02:24.020 --> 00:02:27.270
 really important. The point is that you understand that

00:02:27.270 --> 00:02:30.000
 when there is heat, you understand it to be heat.

00:02:30.000 --> 00:02:34.330
 And you don't understand it to be me and to be mine and to

00:02:34.330 --> 00:02:38.410
 be I. So when you feel hot, you know this is hot. When you

00:02:38.410 --> 00:02:40.000
 feel cold, you know this is cold.

00:02:40.000 --> 00:02:45.030
 And you're clear only in the mere awareness, pati sati mat

00:02:45.030 --> 00:02:49.990
tha, as we learned yesterday. The bare and specific

00:02:49.990 --> 00:02:54.000
 awareness or recognition of the object as it is.

00:02:54.000 --> 00:02:57.160
 Whether you call that physical or mental, they're just

00:02:57.160 --> 00:03:00.430
 names and classifications. But there is something to that

00:03:00.430 --> 00:03:08.060
 because, through the classification, because the hot is not

00:03:08.060 --> 00:03:09.000
 coming from the mind.

00:03:09.000 --> 00:03:13.470
 It's coming from the world, the physical world that we say

00:03:13.470 --> 00:03:17.840
 is around us that, you know, for, on some level, is all

00:03:17.840 --> 00:03:19.000
 around us.

00:03:19.000 --> 00:03:21.570
 And so when there's fire, you feel the heat. So that's

00:03:21.570 --> 00:03:25.510
 physical. It's not coming from your mind. There actually is

00:03:25.510 --> 00:03:30.690
 heat there coming from the physical world, whatever that is

00:03:30.690 --> 00:03:31.000
.

00:03:31.000 --> 00:03:36.670
 And when you do walking meditation, you experience the

00:03:36.670 --> 00:03:42.150
 walking, you feel the foot on the floor, you experience the

00:03:42.150 --> 00:03:43.000
 movements.

00:03:43.000 --> 00:03:47.610
 When you are sitting in meditation, you have the breath

00:03:47.610 --> 00:03:52.360
 going in and out, you experience or you feel the rising and

00:03:52.360 --> 00:03:55.000
 the falling of the abdomen.

00:03:55.000 --> 00:04:01.000
 So that as well is body. That's matter.

00:04:01.000 --> 00:04:05.350
 Yeah, I think it's probably an intellectualization, which

00:04:05.350 --> 00:04:08.880
 is why it's important to give up our ideas of what we

00:04:08.880 --> 00:04:10.000
 believe in.

00:04:10.000 --> 00:04:15.000
 No, it's not a problem if you don't believe in something.

00:04:15.000 --> 00:04:17.000
 We're trying to give up all beliefs, right?

00:04:17.000 --> 00:04:24.330
 But, you know, I wouldn't attach to the view that matter

00:04:24.330 --> 00:04:30.510
 doesn't exist or so on, or that the heat that you feel is

00:04:30.510 --> 00:04:34.650
 only mental or something like that, because that's still a

00:04:34.650 --> 00:04:35.000
 view.

00:04:35.000 --> 00:04:39.280
 And you should accept it for what it is. Heat is heat. And

00:04:39.280 --> 00:04:42.000
 to think of that as being mental.

00:04:42.000 --> 00:04:50.400
 I did a video a long time ago about this, about the

00:04:50.400 --> 00:04:55.070
 argument against materialism and what the opposite is, I

00:04:55.070 --> 00:04:59.810
 guess mysticism or something, or mentalism, whatever it's

00:04:59.810 --> 00:05:01.000
 actually called.

00:05:01.000 --> 00:05:04.270
 So there are two arguments where one says that only the

00:05:04.270 --> 00:05:08.000
 material exists and one says that only the mental exists.

00:05:08.000 --> 00:05:12.360
 And these two arguments are found in the world. From the

00:05:12.360 --> 00:05:14.880
 sounds of it, you might be on the latter side thinking that

00:05:14.880 --> 00:05:16.000
 only the mind exists.

00:05:16.000 --> 00:05:21.170
 But the argument against both of these is that either one

00:05:21.170 --> 00:05:24.000
 requires something extra.

00:05:24.000 --> 00:05:27.180
 It requires a belief or a view that what you're

00:05:27.180 --> 00:05:30.000
 experiencing is something other than what it is.

00:05:30.000 --> 00:05:35.280
 Because clearly we have what appears to be physical and we

00:05:35.280 --> 00:05:40.280
 have what appears to be mental. That's empirically what we

00:05:40.280 --> 00:05:41.000
 have.

00:05:41.000 --> 00:05:45.980
 So the materialists create the argument that the mental is

00:05:45.980 --> 00:05:50.940
 created by the physical as an epiphenomenon or an illusion

00:05:50.940 --> 00:05:52.000
 or so on.

00:05:52.000 --> 00:05:55.920
 They add that in. People who say there is only the mind do

00:05:55.920 --> 00:05:58.800
 the same thing in the other direction. They say the

00:05:58.800 --> 00:06:01.000
 physical is just an illusion.

00:06:01.000 --> 00:06:04.100
 It's something that is created by the mind that appears to

00:06:04.100 --> 00:06:05.000
 be physical.

00:06:05.000 --> 00:06:10.430
 Now if you had a good reason to suggest either of these, if

00:06:10.430 --> 00:06:16.140
 you had some proof or some argument to make in favor of

00:06:16.140 --> 00:06:20.000
 them, then all well and good and we can talk about that.

00:06:20.000 --> 00:06:24.590
 But there's I think a very strong point that you don't need

00:06:24.590 --> 00:06:28.990
 to take up either extreme. There's no reason that I can see

00:06:28.990 --> 00:06:33.210
 for the need to say that the physical doesn't exist or that

00:06:33.210 --> 00:06:34.000
 the mental doesn't exist.

00:06:34.000 --> 00:06:40.420
 Because for all intents or as it appears to us, they both

00:06:40.420 --> 00:06:47.120
 do exist. And really that's I think quite key in Buddhism

00:06:47.120 --> 00:06:51.000
 that what appears is reality.

00:06:51.000 --> 00:06:55.470
 And reality is actually as it appears. It's just that we

00:06:55.470 --> 00:07:00.000
 don't spend enough time looking at what appears.

00:07:00.000 --> 00:07:07.420
 It's not that it seems different than what it is. It's that

00:07:07.420 --> 00:07:12.250
 we, rather than sticking with the reality or with what

00:07:12.250 --> 00:07:15.000
 appears, we go on and extrapolate on it.

00:07:15.000 --> 00:07:22.150
 We create an additional idea or perception or belief or

00:07:22.150 --> 00:07:30.000
 partiality towards the object, objects of experience.

00:07:30.000 --> 00:07:33.140
 But they really are as they appear. That's I think quite

00:07:33.140 --> 00:07:34.000
 important.

00:07:34.000 --> 00:07:39.050
 And this goes against really material science or many

00:07:39.050 --> 00:07:44.750
 religious traditions who say, no, reality isn't what has it

00:07:44.750 --> 00:07:46.000
 appears.

00:07:46.000 --> 00:07:51.680
 And the argument or the point is that there's no reason to

00:07:51.680 --> 00:07:56.370
 believe that. There's no reason to say that beyond this

00:07:56.370 --> 00:07:59.000
 kind of attachment to a belief.

00:07:59.000 --> 00:08:03.160
 Because what does it mean to say that things aren't as they

00:08:03.160 --> 00:08:08.000
 appear? Things appear that way. There appears to be heat.

00:08:08.000 --> 00:08:11.000
 What does it mean to say that reality is other than that?

00:08:11.000 --> 00:08:14.000
 That reality is other than experience.

00:08:14.000 --> 00:08:16.880
 It just becomes an intellectual activity where you say, no,

00:08:16.880 --> 00:08:18.000
 reality is not this.

00:08:18.000 --> 00:08:21.660
 Actually, you're not experiencing heat like this idea of

00:08:21.660 --> 00:08:24.980
 Maya in Hinduism. It's an intellectual exercise. It's not

00:08:24.980 --> 00:08:26.000
 real.

00:08:26.000 --> 00:08:28.250
 Yes, there can be states where there is no heat, there is

00:08:28.250 --> 00:08:30.750
 no cold and so on. But at the moment that you experience

00:08:30.750 --> 00:08:33.470
 heat, that you experience cold, there is heat and there is

00:08:33.470 --> 00:08:34.000
 cold.

00:08:34.000 --> 00:08:38.950
 What else could you say than that? How could you deny the

00:08:38.950 --> 00:08:46.020
 fact that what you're experiencing is real without

00:08:46.020 --> 00:08:52.000
 appealing to an intellectual activity?

00:08:52.000 --> 00:08:57.150
 To what you write, I don't see with my own eyes or I don't

00:08:57.150 --> 00:09:02.000
 believe in anything I don't see with my own eyes.

00:09:02.000 --> 00:09:09.930
 I just wanted to say that this is a good quality, I would

00:09:09.930 --> 00:09:18.440
 say, and the Buddha himself encouraged us to only believe

00:09:18.440 --> 00:09:21.000
 what we can prove.

00:09:21.000 --> 00:09:26.750
 So not to believe blindly what we are told and what others

00:09:26.750 --> 00:09:32.100
 say and so on. So this is not a problem. But if you go, you

00:09:32.100 --> 00:09:34.000
 can overdo it.

00:09:34.000 --> 00:09:41.850
 You can become too fixed upon your own opinion about things

00:09:41.850 --> 00:09:48.740
 and that might be a problem then when you are not open

00:09:48.740 --> 00:09:56.000
 anymore for what people say or for what teachers say.

00:09:56.000 --> 00:10:02.000
 You might not be able to see reality as it is anymore.

00:10:02.000 --> 00:10:06.320
 It doesn't say anything to say "I believe X" when you haven

00:10:06.320 --> 00:10:09.940
't experienced it. But it also doesn't say anything to say "

00:10:09.940 --> 00:10:12.000
I don't believe in X" when you haven't experienced it.

00:10:12.000 --> 00:10:15.710
 Just because you haven't experienced X doesn't mean it

00:10:15.710 --> 00:10:17.000
 doesn't exist.

00:10:17.000 --> 00:10:19.880
 People always take up the Kalamassrut and there is this

00:10:19.880 --> 00:10:22.780
 quote that is going around the internet that is not what

00:10:22.780 --> 00:10:25.000
 the Buddha says in the Kalamassrut.

00:10:25.000 --> 00:10:29.430
 They quote the Kalamassrut incorrectly and so it is just a

00:10:29.430 --> 00:10:33.150
 paraphrase saying don't believe anything unless it agrees

00:10:33.150 --> 00:10:35.000
 with your common sense and reason.

00:10:35.000 --> 00:10:41.160
 But both of those things can be totally wrong. Common sense

00:10:41.160 --> 00:10:45.000
 is what the sense that people have in common.

00:10:45.000 --> 00:10:50.310
 And your reason can be totally off because you can reason

00:10:50.310 --> 00:10:55.000
 anything based on logic, based on reasoning.

00:10:55.000 --> 00:11:00.710
 There is this argument as to why God doesn't exist or

00:11:00.710 --> 00:11:02.000
 something.

00:11:02.000 --> 00:11:09.000
 But you can come to any conclusion through logic.

00:11:09.000 --> 00:11:12.890
 The point of the Kalamassrut is that yes you shouldn't

00:11:12.890 --> 00:11:17.900
 believe in something but as Panyani said that doesn't mean

00:11:17.900 --> 00:11:22.000
 that you should be actively skeptical.

00:11:22.000 --> 00:11:28.340
 Buddha didn't teach skepticism. He taught balance and

00:11:28.340 --> 00:11:32.000
 impartiality.

00:11:32.000 --> 00:11:36.180
 So if someone comes and says something to you, you should

00:11:36.180 --> 00:11:39.000
 give them the 50/50% chance.

00:11:39.000 --> 00:11:42.740
 And if based on your experiences in the past there is

00:11:42.740 --> 00:11:47.020
 something that goes against what they say then you start to

00:11:47.020 --> 00:11:48.000
 adjust that.

00:11:48.000 --> 00:11:53.760
 But to say I don't give it any credence or any possibility

00:11:53.760 --> 00:11:59.260
 or any percentage of being true just because I haven't

00:11:59.260 --> 00:12:01.000
 experienced it.

00:12:01.000 --> 00:12:05.100
 I think it's quite simplistic and it's a little bit of a

00:12:05.100 --> 00:12:06.000
 cop out.

00:12:06.000 --> 00:12:09.640
 And it's a cause for a great amount of argument. How do you

00:12:09.640 --> 00:12:12.000
 know this, how do you know that?

00:12:12.000 --> 00:12:14.310
 Well there's a lot of things that I don't know but I've

00:12:14.310 --> 00:12:17.140
 heard other people talk about them or based on my own

00:12:17.140 --> 00:12:23.710
 experience I can understand how it makes the most sense to

00:12:23.710 --> 00:12:26.000
 be this or to be that.

00:12:26.000 --> 00:12:32.130
 And therefore I go with it as opposed to trying to prove

00:12:32.130 --> 00:12:34.000
 everything.

00:12:34.000 --> 00:12:40.050
 So if it were really important to believe in rupa or madr

00:12:40.050 --> 00:12:44.000
 then maybe it would have something.

00:12:44.000 --> 00:12:47.030
 But it's not really important to believe in it or not

00:12:47.030 --> 00:12:50.350
 believe in it. It's not really an important part of the

00:12:50.350 --> 00:12:51.000
 practice.

00:12:51.000 --> 00:12:54.900
 So the most important thing is to when you do experience

00:12:54.900 --> 00:12:59.020
 what appears to be madr you experience it objectively for

00:12:59.020 --> 00:13:00.000
 what it is.

