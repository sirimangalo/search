1
00:00:00,000 --> 00:00:07,730
 Hello everyone. Today is January 1st, 2014. I'd just like

2
00:00:07,730 --> 00:00:09,000
 to take this opportunity to

3
00:00:09,000 --> 00:00:14,950
 wish everyone a Happy New Year. A new year that is filled

4
00:00:14,950 --> 00:00:19,600
 with good things, most especially

5
00:00:19,600 --> 00:00:23,410
 well-being. You may all find well-being in the new year. We

6
00:00:23,410 --> 00:00:24,420
 consider well-being to be

7
00:00:24,420 --> 00:00:28,960
 more important than what we might call happiness because

8
00:00:28,960 --> 00:00:31,560
 the word happiness implies some sort

9
00:00:31,560 --> 00:00:39,770
 of a feeling that is ephemeral, subject to the changes in

10
00:00:39,770 --> 00:00:44,000
 life. You get it sometimes

11
00:00:44,000 --> 00:00:49,230
 or without it most of the time or much of the time. Whereas

12
00:00:49,230 --> 00:00:52,000
 well-being, though it implies

13
00:00:52,400 --> 00:00:58,430
 it's a sort of happiness that is lasting, it's a sense of

14
00:00:58,430 --> 00:01:02,680
 contentment. We tend to wish

15
00:01:02,680 --> 00:01:09,200
 each other well-being in the new year. The Buddha gave a

16
00:01:09,200 --> 00:01:10,840
 specific teaching on well-being.

17
00:01:10,840 --> 00:01:17,030
 He used this word to describe what he thought was the goal

18
00:01:17,030 --> 00:01:20,400
 for beings in the world, be it

19
00:01:20,400 --> 00:01:23,410
 humans or any type of being that might exist. What we're

20
00:01:23,410 --> 00:01:25,120
 all looking for is well-being.

21
00:01:25,120 --> 00:01:28,870
 You could say we're looking for happiness, but the word he

22
00:01:28,870 --> 00:01:30,720
 used was well-being. He said

23
00:01:30,720 --> 00:01:36,360
 there are four things that you need to find well-being in

24
00:01:36,360 --> 00:01:38,560
 this universe. I'd just like

25
00:01:38,560 --> 00:01:43,950
 to pass these on as a new year dhamma. The first is that we

26
00:01:43,950 --> 00:01:45,320
 need wisdom. Obviously this

27
00:01:45,320 --> 00:01:47,760
 is the most important in Buddhism. Without wisdom you can't

28
00:01:47,760 --> 00:01:48,880
 be expected to understand

29
00:01:48,880 --> 00:01:54,030
 what is for your benefit and to your detriment. Wisdom is

30
00:01:54,030 --> 00:01:57,000
 what allows you to make decisions

31
00:01:57,000 --> 00:01:59,940
 that lead to your own happiness and the happiness of others

32
00:01:59,940 --> 00:02:02,080
, your own well-being and the well-being

33
00:02:02,080 --> 00:02:07,080
 of others. It's what allows you to find the right path and

34
00:02:07,080 --> 00:02:09,120
 to find the right way for you

35
00:02:09,120 --> 00:02:12,350
 in the world to make the right decisions and so on. We

36
00:02:12,350 --> 00:02:14,480
 consider wisdom to be the highest

37
00:02:14,480 --> 00:02:19,870
 at all other qualities of mind, whether they be compassion

38
00:02:19,870 --> 00:02:22,600
 or love or joy or contentment,

39
00:02:22,600 --> 00:02:25,660
 equanimity, these kind of things, humility. They all

40
00:02:25,660 --> 00:02:27,680
 require wisdom. If they don't have

41
00:02:27,680 --> 00:02:31,510
 wisdom it's just a fake artificial construct. They don't

42
00:02:31,510 --> 00:02:34,000
 come from the heart. For something

43
00:02:34,000 --> 00:02:37,450
 to come from the heart it requires a true understanding of

44
00:02:37,450 --> 00:02:39,920
 its benefit, an innate understanding

45
00:02:39,920 --> 00:02:44,040
 of the benefit of it. Likewise to remove unwholesomeness

46
00:02:44,040 --> 00:02:47,640
 requires understanding. To abstain from anger

47
00:02:47,640 --> 00:02:53,010
 and greed and delusion requires wisdom. That's the first

48
00:02:53,010 --> 00:02:56,040
 one. The second quality is effort,

49
00:02:56,040 --> 00:03:01,150
 exertion. Nothing is to be gained through lazing around.

50
00:03:01,150 --> 00:03:03,520
 Buddha said viri yena dukam

51
00:03:03,520 --> 00:03:07,760
 adi cieti. We are able to overcome suffering through effort

52
00:03:07,760 --> 00:03:09,840
. It's not something that can

53
00:03:09,840 --> 00:03:14,250
 be done without putting out effort. Effort to be moral, to

54
00:03:14,250 --> 00:03:17,480
 avoid unwholesome deeds, to

55
00:03:17,480 --> 00:03:20,310
 avoid killing and stealing. It's easy to tell people not to

56
00:03:20,310 --> 00:03:21,600
 get angry and want to hurt each

57
00:03:21,600 --> 00:03:24,890
 other and greedy and want to steal from each other and del

58
00:03:24,890 --> 00:03:26,680
uded and want to take drugs and

59
00:03:26,680 --> 00:03:31,210
 alcohol and so on. But our addictions and our habits lead

60
00:03:31,210 --> 00:03:33,120
 us back ever and again to

61
00:03:33,120 --> 00:03:39,000
 these things. The breaking of habits requires effort and

62
00:03:39,000 --> 00:03:40,760
 the cultivation of wisdom also

63
00:03:40,760 --> 00:03:45,090
 requires effort. Effort in meditation. Obstaining from unwh

64
00:03:45,090 --> 00:03:47,920
olesome deeds, cultivating good deeds,

65
00:03:47,920 --> 00:03:52,150
 both of these require effort and the purification of the

66
00:03:52,150 --> 00:03:55,040
 mind requires effort. We're going to

67
00:03:55,040 --> 00:03:58,240
 be a good person and help others. We're going to be

68
00:03:58,240 --> 00:04:00,640
 charitable and generous and kind. We

69
00:04:00,640 --> 00:04:06,730
 have to pull ourselves out of our inertia, our habits of

70
00:04:06,730 --> 00:04:09,880
 being lazy and indulgent and

71
00:04:09,880 --> 00:04:14,560
 so on. Number three, we need to guard our senses. The eye,

72
00:04:14,560 --> 00:04:15,880
 the ear, the nose, the tongue,

73
00:04:15,880 --> 00:04:20,060
 the body and the mind. Guard here doesn't mean we have to

74
00:04:20,060 --> 00:04:22,080
 avoid seeing and hearing and

75
00:04:22,080 --> 00:04:24,720
 smelling and tasting and feeling and thinking. The point is

76
00:04:24,720 --> 00:04:26,120
 not to avoid experience. The

77
00:04:26,120 --> 00:04:31,280
 point is to guard the extrapolation of experience, so the

78
00:04:31,280 --> 00:04:36,240
 judgment, the partiality. We're not

79
00:04:36,240 --> 00:04:39,510
 trying to limit our experience, we're just trying to limit

80
00:04:39,510 --> 00:04:41,360
 our reaction to our experience,

81
00:04:41,360 --> 00:04:44,930
 to one that is objective and wise. When you see, you should

82
00:04:44,930 --> 00:04:46,640
 know that you're seeing. You

83
00:04:46,640 --> 00:04:50,410
 shouldn't know that that's bad, that's good and me and mine

84
00:04:50,410 --> 00:04:52,320
 get angry and upset or addicted

85
00:04:52,320 --> 00:04:56,260
 and attached to it. When you hear, when you smell, it's not

86
00:04:56,260 --> 00:04:58,720
 that we can't experience the

87
00:04:58,720 --> 00:05:02,140
 whole range of experience, it's that we have to be very

88
00:05:02,140 --> 00:05:04,280
 careful to stay objective so that

89
00:05:04,280 --> 00:05:06,930
 we don't cultivate addiction, we don't cultivate partiality

90
00:05:06,930 --> 00:05:09,160
, we don't cultivate these memories

91
00:05:09,160 --> 00:05:11,860
 in the mind that brought me pleasure, that was a good thing

92
00:05:11,860 --> 00:05:13,400
 or that brought me suffering,

93
00:05:13,400 --> 00:05:18,590
 that was a bad thing and therefore cultivate aversion and

94
00:05:18,590 --> 00:05:21,800
 attachment, which of course lead

95
00:05:21,800 --> 00:05:26,460
 to pushing and pulling and eventually suffering, or

96
00:05:26,460 --> 00:05:30,040
 inevitably suffering. So guarding our senses

97
00:05:30,040 --> 00:05:34,810
 is very important, it's not that we repress our experiences

98
00:05:34,810 --> 00:05:36,960
 but filtering them so that

99
00:05:36,960 --> 00:05:40,290
 the dirt stays out and the experience alone comes in.

100
00:05:40,290 --> 00:05:42,480
 Seeing is just seeing, hearing is

101
00:05:42,480 --> 00:05:46,350
 just hearing and so on. And number four is we have to give

102
00:05:46,350 --> 00:05:48,640
 up everything. This is the

103
00:05:48,640 --> 00:05:52,060
 claim in Buddhism is that happiness doesn't come from

104
00:05:52,060 --> 00:05:54,360
 clinging. The way of the world is

105
00:05:54,360 --> 00:05:57,850
 that the more you get, the more you hold on, the more

106
00:05:57,850 --> 00:06:00,440
 happiness you get, the more pleasure

107
00:06:00,440 --> 00:06:04,160
 you attain and the Buddha noticed that this isn't the case.

108
00:06:04,160 --> 00:06:05,920
 Many religious people have

109
00:06:05,920 --> 00:06:09,080
 noticed that this isn't the case, that it's through letting

110
00:06:09,080 --> 00:06:10,480
 go and giving up that you

111
00:06:10,480 --> 00:06:13,670
 find true happiness. So in Buddhism this is the claim that

112
00:06:13,670 --> 00:06:15,240
 only through letting go can

113
00:06:15,240 --> 00:06:18,420
 you be truly happy and truly free because true happiness

114
00:06:18,420 --> 00:06:20,120
 has to be what we call animisa

115
00:06:20,120 --> 00:06:28,980
 suka or anamasa, means anamasa, means niramasa, niramisa,

116
00:06:28,980 --> 00:06:36,880
 which means having nothing to do

117
00:06:36,880 --> 00:06:39,860
 with an object, not taking an object. Happiness that isn't

118
00:06:39,860 --> 00:06:41,880
 dependent on something, isn't dependent

119
00:06:41,880 --> 00:06:47,540
 on what is impermanent. Happiness should be this well-being

120
00:06:47,540 --> 00:06:50,080
, should be our contentment

121
00:06:50,080 --> 00:06:54,330
 with vicissitudes of life. If your happiness depends on

122
00:06:54,330 --> 00:06:56,840
 something, unless that thing is

123
00:06:56,840 --> 00:06:59,540
 eternal, is permanent, then it's going to create some

124
00:06:59,540 --> 00:07:01,200
 instability and the potential

125
00:07:01,200 --> 00:07:05,540
 for loss. Loss is a common experience in the world for

126
00:07:05,540 --> 00:07:09,200
 people who cling to things, people

127
00:07:09,200 --> 00:07:11,740
 who hold on to things. They will inevitably have to suffer

128
00:07:11,740 --> 00:07:13,120
 loss, if not in this life and

129
00:07:13,120 --> 00:07:17,060
 in future lives. Because of impermanence you can't hold on

130
00:07:17,060 --> 00:07:19,200
 to anything forever. So true

131
00:07:19,200 --> 00:07:22,140
 happiness, the way to go to find true happiness is not

132
00:07:22,140 --> 00:07:24,320
 clinging, it's not depending, it's

133
00:07:24,320 --> 00:07:27,090
 not hedging your bet that something is going to last and

134
00:07:27,090 --> 00:07:28,800
 hoping that you get through life

135
00:07:28,800 --> 00:07:31,720
 without losing the things that you love. True happiness is

136
00:07:31,720 --> 00:07:33,360
 through giving that up, giving

137
00:07:33,360 --> 00:07:37,600
 up the whole worry and concern about losing this or that

138
00:07:37,600 --> 00:07:40,040
 thing, it's through finding true

139
00:07:40,040 --> 00:07:44,040
 contentment. The simile is like the difference between

140
00:07:44,040 --> 00:07:45,960
 someone who clings to something being

141
00:07:45,960 --> 00:07:48,340
 afraid that they're going to fall, if you cling to the side

142
00:07:48,340 --> 00:07:49,400
 of a cliff, being afraid

143
00:07:49,400 --> 00:07:51,340
 that you're going to fall. This is how most people live

144
00:07:51,340 --> 00:07:52,720
 their lives, always worried about

145
00:07:52,720 --> 00:07:56,670
 what the consequences of letting go are going to be. As

146
00:07:56,670 --> 00:07:58,920
 opposed to a bird that lets go of

147
00:07:58,920 --> 00:08:01,740
 something knowing that they're going to fly or realizes

148
00:08:01,740 --> 00:08:03,680
 that they're able to fly and so

149
00:08:03,680 --> 00:08:07,090
 they let go. This is the change that has to come about,

150
00:08:07,090 --> 00:08:09,080
 that we have to realize that to

151
00:08:09,080 --> 00:08:14,210
 be truly happy, to fly, to not be stuck to anything. We

152
00:08:14,210 --> 00:08:16,960
 have to let go. To find true

153
00:08:16,960 --> 00:08:21,910
 happiness we have to give up our attachments. That

154
00:08:21,910 --> 00:08:23,200
 happiness that comes from letting go

155
00:08:23,200 --> 00:08:28,940
 we call well-being. It's a state of being that is content

156
00:08:28,940 --> 00:08:31,880
 and at peace with itself for

157
00:08:31,880 --> 00:08:37,150
 eternity. That is the Buddhist teaching on well-being and

158
00:08:37,150 --> 00:08:38,040
 that's the Dhamma for the New

159
00:08:38,040 --> 00:08:40,700
 Year that we may all find well-being. If we want to find

160
00:08:40,700 --> 00:08:42,320
 well-being we should cultivate

161
00:08:42,320 --> 00:08:44,750
 these four things. It's the kind of framework for

162
00:08:44,750 --> 00:08:46,600
 resolution that we should have in the

163
00:08:46,600 --> 00:08:50,390
 New Year. Some food for thought. Thank you all for tuning

164
00:08:50,390 --> 00:08:52,080
 in and wishing you all a happy

165
00:08:52,080 --> 00:08:59,620
 and well New Year 2014 or whatever year it might be in your

166
00:08:59,620 --> 00:09:02,680
 calendar. Wishing you all

167
00:09:02,680 --> 00:09:03,320
 the best. Peace and happiness.

168
00:09:03,320 --> 00:09:33,320
 [

