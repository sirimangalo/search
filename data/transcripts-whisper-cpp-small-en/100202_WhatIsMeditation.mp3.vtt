WEBVTT

00:00:00.000 --> 00:00:12.000
 So today is finally an English session.

00:00:12.000 --> 00:00:25.350
 Tonight I thought I'd go over what we mean by the word

00:00:25.350 --> 00:00:29.680
 meditation and a little bit about

00:00:29.680 --> 00:00:36.320
 how we practice meditation.

00:00:36.320 --> 00:00:42.220
 For those of you who don't speak English, you can just sit

00:00:42.220 --> 00:00:45.000
 in meditation and try to

00:00:45.000 --> 00:00:47.000
 understand what's being said.

00:00:47.000 --> 00:00:56.600
 It's a chance for us to hear.

00:00:56.600 --> 00:01:18.360
 So the word meditation, as I've said before, can mean many

00:01:18.360 --> 00:01:20.600
 different things to many different

00:01:20.600 --> 00:01:24.760
 people.

00:01:24.760 --> 00:01:30.000
 For most of us we get a sense of meditation as a kind of

00:01:30.000 --> 00:01:35.080
 altered state of mind, a heightened

00:01:35.080 --> 00:01:44.080
 state of awareness or an alternate experience.

00:01:44.080 --> 00:01:47.880
 But really the word meditation, it just means to consider

00:01:47.880 --> 00:01:49.680
 or to mull over or to keep in

00:01:49.680 --> 00:02:04.440
 mind to dwell upon, to solve problems and so on.

00:02:04.440 --> 00:02:10.540
 It means when we dwell on something or when we consider

00:02:10.540 --> 00:02:13.240
 something, it can be referring

00:02:13.240 --> 00:02:20.440
 to any object that we take into consideration.

00:02:20.440 --> 00:02:25.400
 It could be a problem that we have that we meditate on.

00:02:25.400 --> 00:02:44.320
 It could be an object of worship, an object of power.

00:02:44.320 --> 00:02:50.640
 It could be many different things.

00:02:50.640 --> 00:02:56.530
 With the understanding that this is somehow going to bring

00:02:56.530 --> 00:02:59.600
 about a resolution or a result

00:02:59.600 --> 00:03:05.680
 of some sort.

00:03:05.680 --> 00:03:11.000
 For many people, meditation simply means this state of

00:03:11.000 --> 00:03:14.240
 consideration or awareness of an

00:03:14.240 --> 00:03:19.070
 object or even without an object, simply a state where the

00:03:19.070 --> 00:03:21.840
 mind is in an altered reality.

00:03:21.840 --> 00:03:26.690
 So we understand that meditation is this practice that

00:03:26.690 --> 00:03:30.440
 makes your mind very calm and peaceful.

00:03:30.440 --> 00:03:35.470
 This is just sort of an idea that we've developed about

00:03:35.470 --> 00:03:37.000
 meditation.

00:03:37.000 --> 00:03:39.420
 And for most people it would be quite surprising for them

00:03:39.420 --> 00:03:40.960
 or it is quite surprising for them

00:03:40.960 --> 00:03:45.670
 to learn that in meditation your mind need not be calm and

00:03:45.670 --> 00:03:47.840
 peaceful all the time.

00:03:47.840 --> 00:03:59.130
 Of course, meditation just means to consider or to ponder

00:03:59.130 --> 00:04:05.360
 or to take into one's awareness.

00:04:05.360 --> 00:04:08.830
 Now this is very critical in the practice of vipassana

00:04:08.830 --> 00:04:12.720
 meditation because vipassana meditation,

00:04:12.720 --> 00:04:16.460
 the meditation to see clearly, we have to see clearly many

00:04:16.460 --> 00:04:18.760
 bad things, especially in

00:04:18.760 --> 00:04:22.360
 Buddhism, the Buddha said, we have to understand suffering.

00:04:22.360 --> 00:04:27.830
 We have to do fully and totally, completely understand

00:04:27.830 --> 00:04:29.280
 suffering.

00:04:29.280 --> 00:04:32.310
 We can't do that when we're in this peaceful, calm state,

00:04:32.310 --> 00:04:34.320
 mostly because this peaceful, calm

00:04:34.320 --> 00:04:41.180
 state has nothing to do with our ordinary state of

00:04:41.180 --> 00:04:45.120
 awareness or existence.

00:04:45.120 --> 00:04:49.040
 So in Buddhism we separate meditation out into two general

00:04:49.040 --> 00:04:50.120
 categories.

00:04:50.120 --> 00:04:52.680
 And there are different ways of understanding these two

00:04:52.680 --> 00:04:53.840
 general categories.

00:04:53.840 --> 00:04:56.740
 One way is just simply to say one way, one type of

00:04:56.740 --> 00:04:58.840
 meditation is for the purpose and

00:04:58.840 --> 00:05:06.980
 ultimate goal of calm or focus, focused, controlled, cont

00:05:06.980 --> 00:05:11.200
rived states of awareness, sort of like

00:05:11.200 --> 00:05:15.280
 we call transcendental meditation.

00:05:15.280 --> 00:05:21.520
 And the other is for the purpose of seeing clearly.

00:05:21.520 --> 00:05:27.350
 Now when we split them up in this way, it doesn't mean that

00:05:27.350 --> 00:05:31.400
 insight meditation is going to not

00:05:31.400 --> 00:05:35.080
 lead to peace and calm or that in the practice of calm you

00:05:35.080 --> 00:05:37.520
 won't come to understand things.

00:05:37.520 --> 00:05:43.160
 What it means is that this is the purpose of the meditation

00:05:43.160 --> 00:05:43.520
.

00:05:43.520 --> 00:05:53.920
 Another way of looking at these two types of meditation is

00:05:53.920 --> 00:05:56.800
 the object that they take,

00:05:56.800 --> 00:05:59.470
 because there are certain objects that when taken under

00:05:59.470 --> 00:06:01.160
 consideration can't lead to real

00:06:01.160 --> 00:06:07.060
 and true wisdom, real and true understanding.

00:06:07.060 --> 00:06:09.450
 So many people would say all meditation is the same and all

00:06:09.450 --> 00:06:12.560
 leads in the same direction,

00:06:12.560 --> 00:06:15.030
 it's either you're either meditating or you're not, there's

00:06:15.030 --> 00:06:16.000
 no different types.

00:06:16.000 --> 00:06:20.630
 But as I said meditation, it takes an object and the

00:06:20.630 --> 00:06:24.600
 different objects do flavor the meditation

00:06:24.600 --> 00:06:26.880
 in a different way.

00:06:26.880 --> 00:06:29.490
 Why are there certain objects that don't lead to wisdom,

00:06:29.490 --> 00:06:30.720
 don't lead to insight?

00:06:30.720 --> 00:06:33.780
 It's because there are certain objects in meditation, many

00:06:33.780 --> 00:06:35.400
 objects, there's an infinite

00:06:35.400 --> 00:06:39.310
 number of objects in meditation that you can take that are

00:06:39.310 --> 00:06:40.200
 not real.

00:06:40.200 --> 00:06:44.360
 They're something that you create in your mind.

00:06:44.360 --> 00:06:48.550
 And because you create them in your mind, they can't give

00:06:48.550 --> 00:06:52.160
 you any understanding of reality,

00:06:52.160 --> 00:06:57.440
 just as our dreams or our fantasies, no matter how much we

00:06:57.440 --> 00:06:59.800
 dream and fantasize, it's not

00:06:59.800 --> 00:07:07.720
 reality and it can't help us in any way in our life.

00:07:07.720 --> 00:07:12.270
 So when we focus on a light, when people focus on a ball of

00:07:12.270 --> 00:07:19.320
 light in their minds or a color,

00:07:19.320 --> 00:07:23.960
 focus on object or a deity or so on, these are all things

00:07:23.960 --> 00:07:26.160
 that we create in our minds,

00:07:26.160 --> 00:07:28.360
 even God.

00:07:28.360 --> 00:07:31.250
 Because even if there were a God, we wouldn't have any clue

00:07:31.250 --> 00:07:32.680
 what the God would be like,

00:07:32.680 --> 00:07:37.560
 so we just create this idea in our minds.

00:07:37.560 --> 00:07:41.520
 And this leads to great peace and great happiness and it

00:07:41.520 --> 00:07:44.080
 leads many people to take religion

00:07:44.080 --> 00:07:48.200
 very seriously because they interpret their experience to

00:07:48.200 --> 00:07:51.240
 be a religious experience, either

00:07:51.240 --> 00:07:56.850
 an experience of God or an experience of oneness with

00:07:56.850 --> 00:08:02.120
 everything, experience of enlightenment.

00:08:02.120 --> 00:08:04.520
 And yet these experiences are temporary.

00:08:04.520 --> 00:08:09.200
 They're something that comes and goes.

00:08:09.200 --> 00:08:13.360
 Because there's no reflection on the reality of the

00:08:13.360 --> 00:08:16.600
 situation, reflecting on the concept

00:08:16.600 --> 00:08:21.090
 that we've created in our minds, then there's not

00:08:21.090 --> 00:08:24.720
 necessarily any gaining of insight.

00:08:24.720 --> 00:08:28.020
 Now it's certainly possible to look at, say, the states of

00:08:28.020 --> 00:08:29.560
 calm and come to see that they're

00:08:29.560 --> 00:08:34.520
 impermanent and you come to realize something about these

00:08:34.520 --> 00:08:37.800
 things and then insight can begin

00:08:37.800 --> 00:08:38.800
 therefrom.

00:08:38.800 --> 00:08:42.930
 But it's important to understand that there are indeed med

00:08:42.930 --> 00:08:44.960
itations that won't lead you

00:08:44.960 --> 00:08:50.840
 to come to understand reality because they are not real.

00:08:50.840 --> 00:08:53.720
 Transcendental meditation might be very nice but it doesn't

00:08:53.720 --> 00:08:55.000
 tell you much about how to

00:08:55.000 --> 00:09:04.920
 deal with the everyday situations that we're faced with.

00:09:04.920 --> 00:09:08.550
 And yet this is how many people approach problems, of

00:09:08.550 --> 00:09:09.360
 course.

00:09:09.360 --> 00:09:13.400
 When we have a problem with a person, a place or a thing,

00:09:13.400 --> 00:09:15.680
 we try to escape that person,

00:09:15.680 --> 00:09:19.400
 that place or that thing right away.

00:09:19.400 --> 00:09:21.870
 And so this kind of meditation is actually quite appealing

00:09:21.870 --> 00:09:23.160
 to people because they think

00:09:23.160 --> 00:09:26.760
 the way to overcome something is to escape it, just to run

00:09:26.760 --> 00:09:28.520
 away from it, just to push

00:09:28.520 --> 00:09:32.400
 it away.

00:09:32.400 --> 00:09:36.200
 There's no one out there telling us that we should actually

00:09:36.200 --> 00:09:38.120
 face and come to understand

00:09:38.120 --> 00:09:43.720
 and learn something about the unpleasant situation or

00:09:43.720 --> 00:09:48.080
 unpleasant experience, the problem.

00:09:48.080 --> 00:09:50.040
 This is where insight meditation comes in.

00:09:50.040 --> 00:09:52.600
 This is where we really understand the word meditation.

00:09:52.600 --> 00:09:56.000
 When you have a problem, you should meditate on it.

00:09:56.000 --> 00:10:00.880
 This is something that they would say in worldly circles.

00:10:00.880 --> 00:10:04.570
 This is where I believe the word meditation first came into

00:10:04.570 --> 00:10:05.080
 use.

00:10:05.080 --> 00:10:09.490
 It's not a Buddhist word, it's a word that we use to convey

00:10:09.490 --> 00:10:11.440
 the meaning of kamatana,

00:10:11.440 --> 00:10:14.810
 which really doesn't mean meditation, or bhavana, which

00:10:14.810 --> 00:10:16.880
 also doesn't mean meditation.

00:10:16.880 --> 00:10:29.250
 Kamatana means sort of like a fixed story, a firmly

00:10:29.250 --> 00:10:30.520
 established or specific task that

00:10:30.520 --> 00:10:31.520
 we do.

00:10:31.520 --> 00:10:36.480
 It means localizing your activities.

00:10:36.480 --> 00:10:40.000
 Stop doing so many different things and then our actions

00:10:40.000 --> 00:10:42.320
 are fixed and focused upon a certain

00:10:42.320 --> 00:10:45.320
 object.

00:10:45.320 --> 00:10:53.400
 It has more to do with our acts and our behavior than it

00:10:53.400 --> 00:10:59.000
 has to do with any consideration.

00:10:59.000 --> 00:11:02.900
 There are words that mean consideration, but they're not

00:11:02.900 --> 00:11:04.600
 used in this sentence.

00:11:04.600 --> 00:11:11.360
 The word meditation, it would mean to mull over a problem

00:11:11.360 --> 00:11:14.000
 as I understand it.

00:11:14.000 --> 00:11:18.030
 This is really very close to what is meant by insight

00:11:18.030 --> 00:11:20.640
 meditation because we're going

00:11:20.640 --> 00:11:23.220
 to be looking at the very problems and difficulties and

00:11:23.220 --> 00:11:25.040
 issues that we would normally want to

00:11:25.040 --> 00:11:28.000
 run away from.

00:11:28.000 --> 00:11:31.960
 We have to look at all of the aspects of the problem.

00:11:31.960 --> 00:11:33.600
 We have to come to see it clearly.

00:11:33.600 --> 00:11:40.020
 This is why this type of meditation doesn't generally

00:11:40.020 --> 00:11:44.080
 guarantee calm states of mind.

00:11:44.080 --> 00:11:47.790
 You see it's after a whole different type of calm, which is

00:11:47.790 --> 00:11:49.400
 this ability to ride the

00:11:49.400 --> 00:11:54.170
 waves, the ability to go with the flow so that when these

00:11:54.170 --> 00:11:56.680
 unpleasant situations come

00:11:56.680 --> 00:12:00.200
 about the mind is not displeased.

00:12:00.200 --> 00:12:05.330
 It's like there's this analogy that was Shanti Deva, if I

00:12:05.330 --> 00:12:08.080
 remember correctly, gave.

00:12:08.080 --> 00:12:14.670
 He said, "If the world is covered in glass, you have two

00:12:14.670 --> 00:12:16.080
 choices.

00:12:16.080 --> 00:12:22.550
 You can cover it over with leather or you can wear sandals

00:12:22.550 --> 00:12:23.080
."

00:12:23.080 --> 00:12:27.080
 In a way this is an apt comparison with these two types of

00:12:27.080 --> 00:12:28.320
 meditation.

00:12:28.320 --> 00:12:35.120
 When we have unpleasant situations, we can cover them over

00:12:35.120 --> 00:12:38.080
 and change them so that we

00:12:38.080 --> 00:12:42.100
 only have a nice situation or we can change our own minds

00:12:42.100 --> 00:12:44.320
 so that the situations don't

00:12:44.320 --> 00:12:47.760
 bother us.

00:12:47.760 --> 00:12:50.910
 Instead of having to work hard to make it always be a

00:12:50.910 --> 00:12:53.040
 pleasant situation, always be

00:12:53.040 --> 00:12:57.760
 a happy situation, a happy feeling in the mind.

00:12:57.760 --> 00:13:03.030
 Instead come to be content with whatever feeling, whatever

00:13:03.030 --> 00:13:05.400
 experience comes to us.

00:13:05.400 --> 00:13:06.760
 This is like wearing sandals.

00:13:06.760 --> 00:13:11.950
 We're able to deal with every situation because our mind is

00:13:11.950 --> 00:13:12.880
 valid.

00:13:12.880 --> 00:13:15.480
 It really means we have wisdom.

00:13:15.480 --> 00:13:18.440
 We have understanding.

00:13:18.440 --> 00:13:25.840
 Understand that everything is simply arising and ceasing.

00:13:25.840 --> 00:13:28.470
 Being caught up in it is not going to lead you to happiness

00:13:28.470 --> 00:13:28.680
.

00:13:28.680 --> 00:13:32.550
 Getting attached and excited about it is not going to lead

00:13:32.550 --> 00:13:33.680
 you to peace.

00:13:33.680 --> 00:13:36.650
 It's only going to lead you to more bother because it's not

00:13:36.650 --> 00:13:38.280
 stable, it's not sure, and

00:13:38.280 --> 00:13:44.840
 it's not under your control.

00:13:44.840 --> 00:13:48.320
 So in Buddhism we really do focus on this second type of

00:13:48.320 --> 00:13:49.440
 meditation.

00:13:49.440 --> 00:13:50.840
 It doesn't mean that we won't feel calm.

00:13:50.840 --> 00:13:54.100
 It doesn't mean that there's no tranquility involved in the

00:13:54.100 --> 00:13:54.880
 practice.

00:13:54.880 --> 00:13:56.680
 It just means that's not our goal.

00:13:56.680 --> 00:13:59.930
 Even tranquility, instead of looking at, say, an object

00:13:59.930 --> 00:14:01.880
 that would bring us tranquility,

00:14:01.880 --> 00:14:03.980
 we're going to look at the tranquility itself, and it might

00:14:03.980 --> 00:14:04.440
 come up.

00:14:04.440 --> 00:14:05.640
 It should come up.

00:14:05.640 --> 00:14:08.160
 It's part of the spectrum of experience.

00:14:08.160 --> 00:14:11.300
 But when we feel tranquil, we're going to look at the

00:14:11.300 --> 00:14:12.400
 tranquility.

00:14:12.400 --> 00:14:15.760
 We're going to consider it.

00:14:15.760 --> 00:14:20.480
 We're going to think over upon it.

00:14:20.480 --> 00:14:24.240
 And we're going to be very specific about this because we

00:14:24.240 --> 00:14:26.000
're not concerned about the

00:14:26.000 --> 00:14:27.680
 very details of it.

00:14:27.680 --> 00:14:30.240
 We're just concerned about what it is.

00:14:30.240 --> 00:14:32.160
 What does it mean to say that there's pain?

00:14:32.160 --> 00:14:34.960
 What does it mean to say that there's happiness?

00:14:34.960 --> 00:14:36.520
 What does it mean to say that there's anger?

00:14:36.520 --> 00:14:38.120
 What does it mean to say that there's greed?

00:14:38.120 --> 00:14:42.050
 And so we're going to just look at these and understand

00:14:42.050 --> 00:14:44.040
 them for what they are.

00:14:44.040 --> 00:14:46.910
 When we feel happy, we just say to ourselves, "Happy, happy

00:14:46.910 --> 00:14:47.600
, happy."

00:14:47.600 --> 00:14:50.800
 We're just meditating on this happiness.

00:14:50.800 --> 00:14:54.850
 When we feel pain, we're going to meditate on the pain, and

00:14:54.850 --> 00:14:56.760
 say to ourselves, "Pain,

00:14:56.760 --> 00:14:57.760
 pain."

00:14:57.760 --> 00:14:59.830
 And our mind will just know pain, and we'll just know

00:14:59.830 --> 00:15:01.880
 happiness, and we'll just be aware

00:15:01.880 --> 00:15:04.320
 of that one object.

00:15:04.320 --> 00:15:05.320
 There's no room.

00:15:05.320 --> 00:15:07.480
 This is where we have this nice word called mindfulness.

00:15:07.480 --> 00:15:10.530
 It's kind of interesting how these words that were totally

00:15:10.530 --> 00:15:12.240
 used for something completely

00:15:12.240 --> 00:15:15.920
 different actually fit right well into Buddhist thought,

00:15:15.920 --> 00:15:16.920
 even though they don't translate

00:15:16.920 --> 00:15:21.240
 the words that they're supposed to translate directly.

00:15:21.240 --> 00:15:25.220
 Meditation isn't a direct translation for any Pali word

00:15:25.220 --> 00:15:27.320
 that we normally use, that we'd

00:15:27.320 --> 00:15:31.680
 ever translate that way, and mindfulness also is not.

00:15:31.680 --> 00:15:37.440
 But mindfulness, having this full mind, or being fully

00:15:37.440 --> 00:15:40.960
 aware, or keeping fully in mind,

00:15:40.960 --> 00:15:47.960
 or having a full mind in this one sense, of the object.

00:15:47.960 --> 00:15:50.740
 So when we say to ourselves, "Pain, pain," our mind is full

00:15:50.740 --> 00:15:50.960
.

00:15:50.960 --> 00:15:54.160
 This is why it's kind of an interesting word.

00:15:54.160 --> 00:15:58.490
 Our mind is full, and we're fully aware of this object and

00:15:58.490 --> 00:15:59.860
 nothing else.

00:15:59.860 --> 00:16:04.610
 So there's no room for the normal diversification, making

00:16:04.610 --> 00:16:07.280
 more of it than it actually is.

00:16:07.280 --> 00:16:08.280
 This is bad pain.

00:16:08.280 --> 00:16:10.920
 This is my pain.

00:16:10.920 --> 00:16:12.600
 This is a problem.

00:16:12.600 --> 00:16:16.630
 When situations arise, we are able to break them down into

00:16:16.630 --> 00:16:18.200
 their components.

00:16:18.200 --> 00:16:21.220
 We see something we don't like, it's just seeing.

00:16:21.220 --> 00:16:23.240
 We hear something we don't like, it's just hearing.

00:16:23.240 --> 00:16:28.200
 We smell something we don't like, and so on.

00:16:28.200 --> 00:16:32.440
 We have no room for anything but the direct experience.

00:16:32.440 --> 00:16:37.760
 This is what it means to be mindful in this sense.

00:16:37.760 --> 00:16:41.280
 The original word of course didn't mean that.

00:16:41.280 --> 00:16:43.290
 This is something that the Buddha was very good at, and I

00:16:43.290 --> 00:16:46.960
 think he'd very much appreciate

00:16:46.960 --> 00:16:50.020
 giving words new meaning, because language is such a

00:16:50.020 --> 00:16:52.200
 cultural thing, and it's built up

00:16:52.200 --> 00:16:57.140
 around our ways of looking at the world, that there ends up

00:16:57.140 --> 00:17:00.080
 being no good word for the quality

00:17:00.080 --> 00:17:04.320
 that you're trying to describe, and you have to make do.

00:17:04.320 --> 00:17:09.110
 And there are many words that are used to mean ultimates,

00:17:09.110 --> 00:17:11.520
 that we have to remake, like

00:17:11.520 --> 00:17:14.140
 how the Buddha changed the word dharma, because it was such

00:17:14.140 --> 00:17:15.560
 an important word, or karma, because

00:17:15.560 --> 00:17:19.080
 it was another important word.

00:17:19.080 --> 00:17:21.830
 And so he had to explain to me what was the true dharma,

00:17:21.830 --> 00:17:23.320
 what was the true karma.

00:17:23.320 --> 00:17:29.640
 Brahmana, Sammana, all these words he changed.

00:17:29.640 --> 00:17:36.120
 So here we're giving this word a new meaning.

00:17:36.120 --> 00:17:43.770
 Mindfulness means your mind is full, or you're fully encomp

00:17:43.770 --> 00:17:47.280
assed by this one object.

00:17:47.280 --> 00:17:49.860
 And because the object is impermanent, it's coming and

00:17:49.860 --> 00:17:51.560
 going, it's changing, you're going

00:17:51.560 --> 00:17:52.560
 to see that.

00:17:52.560 --> 00:17:56.020
 And you're going to say that you're going to realize to

00:17:56.020 --> 00:17:58.440
 yourself that it's not satisfying,

00:17:58.440 --> 00:18:00.890
 that whatever you thought about this, that you were going

00:18:00.890 --> 00:18:02.200
 to change it, or make it good,

00:18:02.200 --> 00:18:09.120
 or make it perfect, was an illusion, was a delusion.

00:18:09.120 --> 00:18:16.000
 And you're going to let go of any idea that it's you or

00:18:16.000 --> 00:18:17.320
 yours.

00:18:17.320 --> 00:18:21.600
 And we'll come to see that really every experience is just

00:18:21.600 --> 00:18:24.520
 a part of nature, a part of the reality

00:18:24.520 --> 00:18:27.040
 of the universe.

00:18:27.040 --> 00:18:31.680
 There's no eye involved in seeing or hearing or smelling or

00:18:31.680 --> 00:18:34.240
 tasting or feeling or thinking.

00:18:34.240 --> 00:18:37.960
 All these things just come and go and come and go.

00:18:37.960 --> 00:18:43.630
 And the mind will begin to let go and loosen up, ease up,

00:18:43.630 --> 00:18:46.520
 until it finally really lets

00:18:46.520 --> 00:18:47.520
 go.

00:18:47.520 --> 00:18:50.340
 And when the mind really lets go, this is what the Buddha

00:18:50.340 --> 00:18:51.920
 called nirvana, nirvana.

00:18:51.920 --> 00:18:54.840
 This is what we're aiming for in the practice.

00:18:54.840 --> 00:18:57.610
 Just easing up and loosening up, just coming to see things

00:18:57.610 --> 00:18:59.120
 for what they are when they're

00:18:59.120 --> 00:19:00.520
 seeing, it's only seeing.

00:19:00.520 --> 00:19:03.480
 When there's hearing, it's only hearing.

00:19:03.480 --> 00:19:07.960
 Smelling, tasting, feeling, thinking, it's only an

00:19:07.960 --> 00:19:09.280
 experience.

00:19:09.280 --> 00:19:12.740
 When the mind loosens up, all of these attachments and add

00:19:12.740 --> 00:19:14.840
ictions, that's all they are.

00:19:14.840 --> 00:19:19.160
 Misunderstanding of the reality of our experience.

00:19:19.160 --> 00:19:26.680
 And when we overcome this, the mind lets go.

00:19:26.680 --> 00:19:30.360
 When the mind lets go, the mind is free.

00:19:30.360 --> 00:19:36.450
 When the mind is free, then you can say to yourself, "I'm

00:19:36.450 --> 00:19:37.480
 free.

00:19:37.480 --> 00:19:40.440
 Nothing left to be done here."

00:19:40.440 --> 00:19:45.560
 No more running around chasing after things that are not

00:19:45.560 --> 00:19:48.360
 going to bring us happiness or

00:19:48.360 --> 00:19:49.360
 peace.

00:19:49.360 --> 00:19:52.480
 This is the true freedom that we're looking for.

00:19:52.480 --> 00:19:56.880
 This is what we really mean by meditation in a Buddhist

00:19:56.880 --> 00:19:57.760
 sense.

00:19:57.760 --> 00:20:05.720
 This consideration of reality or reflection on reality as

00:20:05.720 --> 00:20:09.520
 it is, until we finally see

00:20:09.520 --> 00:20:13.400
 it just simply for what it is.

00:20:13.400 --> 00:20:14.400
 We're not creating anything.

00:20:14.400 --> 00:20:18.840
 It's not an altered state of mind.

00:20:18.840 --> 00:20:21.080
 In fact, it's the unaltered state of mind.

00:20:21.080 --> 00:20:26.370
 The Lord Buddha said, "Bhagavasarangi dhang, pika vai jit

00:20:26.370 --> 00:20:26.840
ang."

00:20:26.840 --> 00:20:34.480
 He said, "Monks, this here mind is radiant.

00:20:34.480 --> 00:20:38.120
 The mind is originally radiant.

00:20:38.120 --> 00:20:41.160
 The unaltered state of mind is perfect.

00:20:41.160 --> 00:20:43.860
 It's knowing that we're sitting, knowing that we're walking

00:20:43.860 --> 00:20:47.040
, or so on, or even the

00:20:47.040 --> 00:20:49.600
 non-arising of any experience.

00:20:49.600 --> 00:20:52.120
 It's perfect.

00:20:52.120 --> 00:20:56.950
 It's just that when we experience, then there gives rise to

00:20:56.950 --> 00:20:58.360
 defilements.

00:20:58.360 --> 00:21:04.350
 There are these akhanduka kilesa, these defilements that

00:21:04.350 --> 00:21:08.160
 are guests, or they are visitors.

00:21:08.160 --> 00:21:13.080
 They come in on occasion from time to time.

00:21:13.080 --> 00:21:17.760
 This is what makes the mind defile.

00:21:17.760 --> 00:21:21.870
 Just like a polished gem, you can cover it up with dirt

00:21:21.870 --> 00:21:23.680
 until it looks ugly.

00:21:23.680 --> 00:21:29.800
 But when you wash it, when you clean it, it gets back to

00:21:29.800 --> 00:21:32.440
 its unaltered form.

00:21:32.440 --> 00:21:34.280
 The mind is very much the same.

00:21:34.280 --> 00:21:38.040
 Actually, the mind is perfect.

00:21:38.040 --> 00:21:40.640
 There's no defilement in the mind.

00:21:40.640 --> 00:21:45.000
 It's a visiting state.

00:21:45.000 --> 00:21:46.680
 Defilement is something that comes into the mind.

00:21:46.680 --> 00:21:48.520
 This is just one way of talking.

00:21:48.520 --> 00:21:53.090
 Actually, in ultimate reality, they say, "The mind would be

00:21:53.090 --> 00:21:54.120
 defiled."

00:21:54.120 --> 00:21:57.960
 One mind state, or one moment of defiled.

00:21:57.960 --> 00:22:01.720
 But here we're talking in a general sense, our mind.

00:22:01.720 --> 00:22:05.960
 It's a mind that we call us, or we call I.

00:22:05.960 --> 00:22:09.420
 It's actually fine when you know things, when you're

00:22:09.420 --> 00:22:11.720
 walking around, when you're talking,

00:22:11.720 --> 00:22:14.840
 when you're doing whatever.

00:22:14.840 --> 00:22:18.040
 It's just that from time to time, we take things seriously.

00:22:18.040 --> 00:22:21.960
 We take things more seriously than we should.

00:22:21.960 --> 00:22:25.520
 We make more of some of things than we really are.

00:22:25.520 --> 00:22:28.990
 In fact, most of the time we do this, because most of the

00:22:28.990 --> 00:22:31.360
 time we're in this semi-deluded

00:22:31.360 --> 00:22:32.360
 state.

00:22:32.360 --> 00:22:37.200
 We're not really aware of reality.

00:22:37.200 --> 00:22:41.020
 This should be a good guideline for us, of what we're

00:22:41.020 --> 00:22:42.160
 aiming for.

00:22:42.160 --> 00:22:45.950
 It's just to see why is it that we suffer, why is it that

00:22:45.950 --> 00:22:48.120
 we give rise to defilement.

00:22:48.120 --> 00:22:52.800
 It's no other reason than we don't see things as they are.

00:22:52.800 --> 00:22:56.800
 Here we practice meditation as to consider these things and

00:22:56.800 --> 00:22:58.440
 come to see them clearer

00:22:58.440 --> 00:23:01.520
 than we saw them before.

00:23:01.520 --> 00:23:03.640
 It doesn't mean that the defilements won't arise, but it

00:23:03.640 --> 00:23:04.720
 means it will come to understand

00:23:04.720 --> 00:23:07.720
 them more and more, until they become weaker and weaker and

00:23:07.720 --> 00:23:09.160
 they don't have a hold over

00:23:09.160 --> 00:23:10.160
 us.

00:23:10.160 --> 00:23:13.240
 In an event, the defilements will not arise, because we'll

00:23:13.240 --> 00:23:14.800
 be able to see things perfectly

00:23:14.800 --> 00:23:18.960
 crystal clear.

00:23:18.960 --> 00:23:23.630
 This is the teaching that I would like to offer tonight, a

00:23:23.630 --> 00:23:26.680
 warm-up to meditation practice.

00:23:26.680 --> 00:23:30.120
 Thanks for coming.

00:23:30.120 --> 00:23:31.120
 1

