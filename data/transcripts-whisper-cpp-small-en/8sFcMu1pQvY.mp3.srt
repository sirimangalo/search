1
00:00:00,000 --> 00:00:16,000
 [silence]

2
00:00:16,000 --> 00:00:20,870
 You can come sit closer maybe I talk quietly mostly. It's

3
00:00:20,870 --> 00:00:22,000
 hard to hear.

4
00:00:22,000 --> 00:00:51,000
 [silence]

5
00:00:51,000 --> 00:00:55,460
 So probably what we'll do tonight I'll give a short talk as

6
00:00:55,460 --> 00:00:58,000
 is the custom.

7
00:00:58,000 --> 00:01:07,150
 And then I'll invite the new people to go and show you how

8
00:01:07,150 --> 00:01:10,000
 to do the meditation and everyone else can just practice.

9
00:01:10,000 --> 00:01:28,000
 [silence]

10
00:01:28,000 --> 00:01:45,000
 Tonight is our English session together.

11
00:01:45,000 --> 00:01:50,240
 When I talk it's important that we prepare ourselves, use

12
00:01:50,240 --> 00:01:54,000
 it as a time to prepare ourselves for the meditation.

13
00:01:54,000 --> 00:02:03,480
 We're not here just to learn theory or ideas. We're here to

14
00:02:03,480 --> 00:02:05,000
 practice.

15
00:02:05,000 --> 00:02:12,940
 So here we have this introduction where I talk. You can

16
00:02:12,940 --> 00:02:19,000
 start to bring your mind back to the here and now.

17
00:02:19,000 --> 00:02:31,460
 So it may not be the most entertaining form of speech but

18
00:02:31,460 --> 00:02:37,000
 in this way it's more useful.

19
00:02:37,000 --> 00:02:44,260
 So when we listen we can close our eyes and start to become

20
00:02:44,260 --> 00:02:45,000
 more meditative.

21
00:02:45,000 --> 00:02:48,260
 If we've already begun to practice we can just continue

22
00:02:48,260 --> 00:02:51,000
 with our practice while we're listening.

23
00:02:51,000 --> 00:03:09,000
 [silence]

24
00:03:09,000 --> 00:03:19,000
 One of the ways of understanding the practice of meditation

25
00:03:19,000 --> 00:03:29,000
 is in terms of the difference between happiness and peace.

26
00:03:29,000 --> 00:03:35,540
 And I would say only when we understand and begin to prefer

27
00:03:35,540 --> 00:03:42,180
 peace to what we normally understand to be happiness can we

28
00:03:42,180 --> 00:03:51,000
 really appreciate things like meditation.

29
00:03:51,000 --> 00:03:57,260
 Because that's really what we're doing is teaching our

30
00:03:57,260 --> 00:04:03,240
 minds to give up the need to seek out pleasure again and

31
00:04:03,240 --> 00:04:04,000
 again.

32
00:04:04,000 --> 00:04:09,320
 Teaching our minds to be here, to not worry about getting

33
00:04:09,320 --> 00:04:14,000
 things, achieving things, accomplishing things.

34
00:04:14,000 --> 00:04:23,340
 To be content with the present moment, the here and the now

35
00:04:23,340 --> 00:04:24,000
.

36
00:04:24,000 --> 00:04:29,400
 Most of how we live our lives is chasing after happiness

37
00:04:29,400 --> 00:04:33,000
 and running away from suffering.

38
00:04:33,000 --> 00:04:41,000
 And so as I said before we compartmentalize reality.

39
00:04:41,000 --> 00:04:46,000
 We're no longer open to the full spectrum of experience.

40
00:04:46,000 --> 00:04:51,000
 Only a certain part of our experience is acceptable.

41
00:04:51,000 --> 00:04:52,830
 For instance, sitting here right now there's probably a lot

42
00:04:52,830 --> 00:04:54,000
 of things that are unacceptable.

43
00:04:54,000 --> 00:04:58,000
 The pain that we feel in the body by having to sit still.

44
00:04:58,000 --> 00:05:03,000
 Maybe we feel hot or cold. Maybe we're itching.

45
00:05:03,000 --> 00:05:08,000
 Maybe we're thinking about the past or the future.

46
00:05:08,000 --> 00:05:11,460
 There are many things that we can't accept about the

47
00:05:11,460 --> 00:05:14,000
 reality in front of us.

48
00:05:14,000 --> 00:05:18,300
 And on the other hand, there are many things that we

49
00:05:18,300 --> 00:05:20,000
 require to be happy.

50
00:05:20,000 --> 00:05:26,000
 So we're thinking about food or we're thinking about family

51
00:05:26,000 --> 00:05:28,000
 or we're thinking about friends.

52
00:05:28,000 --> 00:05:33,000
 We're thinking about places and things that we have to do.

53
00:05:33,000 --> 00:05:38,000
 Maybe thinking about plans that we have for the future.

54
00:05:38,000 --> 00:05:41,000
 Maybe thinking about things that happened to us in the past

55
00:05:41,000 --> 00:05:43,000
.

56
00:05:43,000 --> 00:05:48,000
 Good things or bad things.

57
00:05:48,000 --> 00:05:52,000
 And we're constantly judging.

58
00:05:52,000 --> 00:05:57,000
 We're constantly dividing the world up into good and bad.

59
00:05:57,000 --> 00:06:01,880
 And when sex goes according to our wishes, we're happy.

60
00:06:01,880 --> 00:06:09,000
 When it doesn't, we're unhappy.

61
00:06:09,000 --> 00:06:14,450
 And this is the ordinary way of approaching the idea of

62
00:06:14,450 --> 00:06:15,000
 happiness.

63
00:06:15,000 --> 00:06:20,990
 The ordinary method for human beings to find true happiness

64
00:06:20,990 --> 00:06:26,000
 is to chase after pleasures and run away from suffering.

65
00:06:26,000 --> 00:06:34,120
 The reason why we come to practice meditation is because we

66
00:06:34,120 --> 00:06:42,120
 begin to see that this is a poor model for finding true

67
00:06:42,120 --> 00:06:43,000
 happiness,

68
00:06:43,000 --> 00:06:51,220
 for life in terms of finding peace, in terms of finding

69
00:06:51,220 --> 00:06:53,000
 real and lasting happiness.

70
00:06:53,000 --> 00:06:57,640
 Because the more we get what we want, what is enjoyable to

71
00:06:57,640 --> 00:07:02,000
 us, the more we feel happy.

72
00:07:02,000 --> 00:07:08,000
 The more we like that happiness and want that happiness.

73
00:07:08,000 --> 00:07:18,840
 The more we distinguish that happiness from other states,

74
00:07:18,840 --> 00:07:30,000
 which are then labeled as unhappy or boring or unexciting.

75
00:07:30,000 --> 00:07:34,800
 And so rather than finding more happiness, our search for

76
00:07:34,800 --> 00:07:39,000
 happiness actually leads us to more suffering,

77
00:07:39,000 --> 00:07:48,000
 to a constant chase, a constant search for pleasure,

78
00:07:48,000 --> 00:07:50,000
 needing this and that and so many things,

79
00:07:50,000 --> 00:07:58,400
 and becoming angry and upset when we can't get what we want

80
00:07:58,400 --> 00:07:59,000
.

81
00:07:59,000 --> 00:08:03,840
 When things aren't the way we want, we right away have to

82
00:08:03,840 --> 00:08:09,030
 fix, have to change things, have to change our experience

83
00:08:09,030 --> 00:08:11,000
 of reality.

84
00:08:11,000 --> 00:08:16,080
 So the first thing we realize about meditation is it's not

85
00:08:16,080 --> 00:08:19,000
 really about happy feelings.

86
00:08:19,000 --> 00:08:24,000
 It's not about getting something.

87
00:08:24,000 --> 00:08:30,640
 Our goal when we sit is not to sit and feel states of bliss

88
00:08:30,640 --> 00:08:35,000
, states of pleasure.

89
00:08:35,000 --> 00:08:41,000
 What we're looking for is a state of peace.

90
00:08:41,000 --> 00:08:43,810
 We practice meditation because we realize that this peace

91
00:08:43,810 --> 00:08:46,000
 is something that we're missing in our lives.

92
00:08:46,000 --> 00:08:50,610
 No matter how much happiness we have, we rarely if ever

93
00:08:50,610 --> 00:08:52,000
 have any peace.

94
00:08:52,000 --> 00:08:59,070
 Because we're always chasing, we're always looking, we're

95
00:08:59,070 --> 00:09:02,000
 always unsatisfying.

96
00:09:02,000 --> 00:09:09,180
 And so the practice of meditation is creating contentment

97
00:09:09,180 --> 00:09:12,000
 and satisfaction with the way things are.

98
00:09:12,000 --> 00:09:15,000
 No longer needing things to be other than what they are.

99
00:09:15,000 --> 00:09:21,000
 Even when our experience of reality is painful, is unpleas

100
00:09:21,000 --> 00:09:23,000
urable, to not be upset by it.

101
00:09:23,000 --> 00:09:25,770
 Because we can't avoid these states. We can't avoid getting

102
00:09:25,770 --> 00:09:29,000
 old, we can't avoid getting sick, we can't avoid dying.

103
00:09:29,000 --> 00:09:38,020
 We can't avoid so many unpleasant experiences that are

104
00:09:38,020 --> 00:09:44,000
 going to arise for us in our lives.

105
00:09:44,000 --> 00:09:48,350
 And so when we experience the problems of life, again and

106
00:09:48,350 --> 00:09:52,800
 again we realize that there's something wrong with the way

107
00:09:52,800 --> 00:09:54,000
 we're looking at the world.

108
00:09:54,000 --> 00:09:59,000
 And our minds are not stable, our minds are not calm.

109
00:09:59,000 --> 00:10:02,940
 Our minds are not the way we'd like them to be. We don't

110
00:10:02,940 --> 00:10:05,000
 have any peace.

111
00:10:05,000 --> 00:10:09,320
 Even just sitting here listening to me talk, you'll find

112
00:10:09,320 --> 00:10:12,000
 your mind is not really at peace.

113
00:10:12,000 --> 00:10:16,730
 It's thinking about the good things that we can get and

114
00:10:16,730 --> 00:10:20,660
 becoming upset very quickly about the bad things that arise

115
00:10:20,660 --> 00:10:21,000
.

116
00:10:21,000 --> 00:10:26,000
 Unpleasantness.

117
00:10:26,000 --> 00:10:28,410
 And so we've already, even just sitting here in a few

118
00:10:28,410 --> 00:10:33,360
 minutes, we've already made judgement after judgement about

119
00:10:33,360 --> 00:10:35,000
 everything that arises.

120
00:10:35,000 --> 00:10:38,000
 The things that I'm saying, right away there's judgement.

121
00:10:38,000 --> 00:10:41,820
 The temperature in the room there's judgement. The feeling

122
00:10:41,820 --> 00:10:44,000
 of the sitting mat that you're on.

123
00:10:44,000 --> 00:10:54,000
 The pains in the body, the itching and the discomfort.

124
00:10:54,000 --> 00:11:00,000
 Thinking about the past, thinking about the future.

125
00:11:00,000 --> 00:11:05,000
 We're not really at peace. Our minds are very clingy.

126
00:11:05,000 --> 00:11:08,680
 They cling to everything. They have a judgement. They have

127
00:11:08,680 --> 00:11:11,000
 an opinion about everything.

128
00:11:11,000 --> 00:11:15,160
 They're chattering away, "This is good, this is bad, this

129
00:11:15,160 --> 00:11:17,000
 is right, this is wrong."

130
00:11:17,000 --> 00:11:22,220
 Telling us what to do, "Do this, do that, don't do this,

131
00:11:22,220 --> 00:11:24,000
 don't do that."

132
00:11:24,000 --> 00:11:29,410
 Our minds are like a little child whining and complaining

133
00:11:29,410 --> 00:11:31,000
 all the time.

134
00:11:31,000 --> 00:11:41,590
 Unable to sit still, unable to be, just be in the here and

135
00:11:41,590 --> 00:11:43,000
 now.

136
00:11:43,000 --> 00:11:48,510
 So this is the reason why we use this tool called

137
00:11:48,510 --> 00:11:53,000
 meditation, of contemplating things as they are.

138
00:11:53,000 --> 00:11:56,840
 Instead of judging things, we see them for what they are.

139
00:11:56,840 --> 00:12:00,680
 So when we feel a pain in the body, we just see it for pain

140
00:12:00,680 --> 00:12:01,000
.

141
00:12:01,000 --> 00:12:05,000
 It's totally changing the way we look at the world.

142
00:12:05,000 --> 00:12:09,150
 We've never before have thought to actually focus on a

143
00:12:09,150 --> 00:12:11,000
 painful situation.

144
00:12:11,000 --> 00:12:14,300
 Our normal reaction is to avoid it, to run away from it, to

145
00:12:14,300 --> 00:12:20,130
 cover it up, to pretend, to ignore it and pretend it doesn

146
00:12:20,130 --> 00:12:22,000
't exist.

147
00:12:22,000 --> 00:12:27,710
 The reason why we're doing the exact opposite is because we

148
00:12:27,710 --> 00:12:30,000
 want to understand.

149
00:12:30,000 --> 00:12:35,220
 We come to see that we don't really have a proper

150
00:12:35,220 --> 00:12:38,000
 understanding of reality.

151
00:12:38,000 --> 00:12:41,350
 We don't really have a proper relationship with reality and

152
00:12:41,350 --> 00:12:46,000
 we're always messing up, making mistakes.

153
00:12:46,000 --> 00:12:50,170
 We're addicted to things, we're averse to things, to

154
00:12:50,170 --> 00:12:54,570
 situations that lead us to get angry and frustrated and

155
00:12:54,570 --> 00:12:59,000
 bored and worried and scared and depressed.

156
00:12:59,000 --> 00:13:08,000
 Addicted.

157
00:13:08,000 --> 00:13:14,300
 So in meditation we want to change that. We want to come to

158
00:13:14,300 --> 00:13:19,250
 understand reality and figure out the proper way to react

159
00:13:19,250 --> 00:13:24,000
 to the phenomena that arise.

160
00:13:24,000 --> 00:13:27,120
 We want to learn about the pain. We want to come to

161
00:13:27,120 --> 00:13:29,000
 understand what is pain.

162
00:13:29,000 --> 00:13:34,690
 What happens when pain arises? How does my mind react? To

163
00:13:34,690 --> 00:13:38,000
 see where the true suffering is.

164
00:13:38,000 --> 00:13:41,240
 So we remind ourselves, we use this word to remind

165
00:13:41,240 --> 00:13:46,690
 ourselves this is pain. We say to ourselves, "Pain, pain,

166
00:13:46,690 --> 00:13:54,000
 pain," simply knowing that it's pain.

167
00:13:54,000 --> 00:13:57,400
 And we make peace with this pain. We come to understand

168
00:13:57,400 --> 00:13:59,000
 that it simply is pain.

169
00:13:59,000 --> 00:14:05,240
 There's nothing intrinsically negative about the feeling at

170
00:14:05,240 --> 00:14:06,000
 all.

171
00:14:06,000 --> 00:14:11,750
 There's nothing in it that says, "This is bad. I'm bad.

172
00:14:11,750 --> 00:14:13,000
 Hate me."

173
00:14:13,000 --> 00:14:20,000
 We do that to ourselves. We create that for ourselves.

174
00:14:20,000 --> 00:14:23,870
 When we feel pain right away, it's bad. It's a problem. It

175
00:14:23,870 --> 00:14:35,000
's my problem. My pain.

176
00:14:35,000 --> 00:14:38,450
 So instead of all of that, we understand the pain for what

177
00:14:38,450 --> 00:14:45,000
 it is. We see this is pain. We stop it there.

178
00:14:45,000 --> 00:14:50,390
 We come to force our minds to accept this, to accept the

179
00:14:50,390 --> 00:14:55,000
 reality of it, to stop denying the fact that it is pain,

180
00:14:55,000 --> 00:14:57,000
 that it's there, that it exists.

181
00:14:57,000 --> 00:15:04,760
 And we make peace with the pain. We find peace in all

182
00:15:04,760 --> 00:15:10,100
 situations, whatever arises. We stop compartmentalizing

183
00:15:10,100 --> 00:15:11,000
 reality.

184
00:15:11,000 --> 00:15:14,920
 Everything becomes peaceful for us. Everything becomes

185
00:15:14,920 --> 00:15:16,000
 acceptable.

186
00:15:16,000 --> 00:15:21,000
 Nothing that arises can cause us to suffer.

187
00:15:21,000 --> 00:15:24,480
 This is in brief what we're trying for, what we're aiming

188
00:15:24,480 --> 00:15:25,000
 for.

189
00:15:25,000 --> 00:15:28,200
 And it's, as I said, it's the highlighting of this

190
00:15:28,200 --> 00:15:31,000
 difference between happiness and peace.

191
00:15:31,000 --> 00:15:34,000
 If you're seeking, always seeking after happy experience,

192
00:15:34,000 --> 00:15:39,000
 pleasurable experiences, you'll never be at peace.

193
00:15:39,000 --> 00:15:45,000
 You're creating addiction. You're creating attachment.

194
00:15:45,000 --> 00:15:50,480
 The irony of it is this, that the more you want to be happy

195
00:15:50,480 --> 00:15:54,000
, the less happy you'll become.

196
00:15:54,000 --> 00:15:56,960
 The less happy because the less peaceful. Your mind is no

197
00:15:56,960 --> 00:15:58,000
 longer at peace.

198
00:15:58,000 --> 00:16:00,770
 In practice meditation, we're not trying to give rise to a

199
00:16:00,770 --> 00:16:04,000
 certain experience. We're trying to find peace.

200
00:16:04,000 --> 00:16:07,370
 We're trying to be at peace with ourselves, to be at peace

201
00:16:07,370 --> 00:16:11,000
 with the world around us, to be at peace with the universe,

202
00:16:11,000 --> 00:16:18,770
 to stop fighting, to stop needing and wanting, to accept

203
00:16:18,770 --> 00:16:21,000
 things for what they are.

204
00:16:21,000 --> 00:16:28,000
 This is why we practice.

205
00:16:28,000 --> 00:16:34,000
 And so without further ado, I invite everyone to begin.

206
00:16:34,000 --> 00:16:37,020
 For those of you who have practiced before, you can

207
00:16:37,020 --> 00:16:38,000
 continue on.

208
00:16:38,000 --> 00:16:42,000
 Start with the mindful frustration of walking in the city.

209
00:16:42,000 --> 00:16:44,700
 For the new people, I'll take you now to show you how to

210
00:16:44,700 --> 00:16:46,000
 practice meditation.

211
00:16:46,000 --> 00:16:51,000
 [ Sighs ]

