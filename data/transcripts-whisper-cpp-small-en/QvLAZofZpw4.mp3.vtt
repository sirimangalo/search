WEBVTT

00:00:00.000 --> 00:00:08.200
 Hi, so in this, my final video on why everyone should

00:00:08.200 --> 00:00:10.940
 practice meditation, I'm going to give

00:00:10.940 --> 00:00:14.730
 the number one reason why everyone should practice

00:00:14.730 --> 00:00:15.960
 meditation.

00:00:15.960 --> 00:00:20.330
 And the number one reason is because meditation simply

00:00:20.330 --> 00:00:21.840
 makes you free.

00:00:21.840 --> 00:00:25.320
 And this is of course the most important reason why

00:00:25.320 --> 00:00:28.320
 everyone should practice meditation.

00:00:28.320 --> 00:00:33.910
 It's because in our everyday lives we, sometimes we think

00:00:33.910 --> 00:00:36.000
 we're free, but more often than not

00:00:36.000 --> 00:00:42.200
 we have to admit to ourselves that we're trapped either in

00:00:42.200 --> 00:00:46.880
 our emotions, our needs, our wants,

00:00:46.880 --> 00:00:52.010
 trapped in our hatreds and our aversions, or we're trapped

00:00:52.010 --> 00:00:54.160
 by our actions, by the things

00:00:54.160 --> 00:00:56.000
 we do.

00:00:56.000 --> 00:00:59.030
 We've trapped ourselves by all of the things that we have

00:00:59.030 --> 00:01:00.840
 chased after, all of the things

00:01:00.840 --> 00:01:02.160
 that we have built up.

00:01:02.160 --> 00:01:06.510
 We're trapped by our debts, we're trapped by the world

00:01:06.510 --> 00:01:09.120
 around us, or we're trapped by

00:01:09.120 --> 00:01:12.120
 the results of our actions.

00:01:12.120 --> 00:01:15.190
 We're now trapped in the suffering which comes from

00:01:15.190 --> 00:01:17.880
 addiction, which comes from attachment,

00:01:17.880 --> 00:01:22.380
 which comes from hatred, which comes from animosity and so

00:01:22.380 --> 00:01:22.840
 on.

00:01:22.840 --> 00:01:25.510
 And so where we sometimes think that we're free to do

00:01:25.510 --> 00:01:27.440
 whatever we want, we think we live

00:01:27.440 --> 00:01:33.390
 in a free society for instance, that we have the freedom to

00:01:33.390 --> 00:01:36.400
 pursue all of the things that

00:01:36.400 --> 00:01:39.680
 we want to achieve, all of the things that we want to

00:01:39.680 --> 00:01:41.800
 obtain, we realize that actually

00:01:41.800 --> 00:01:45.960
 in doing so, simply going after, chasing after our hearts,

00:01:45.960 --> 00:01:48.360
 following our hearts as they say,

00:01:48.360 --> 00:01:54.440
 we have become slaves, we have become enslaved to our own

00:01:54.440 --> 00:01:58.960
 mental defilements from the unwholesome

00:01:58.960 --> 00:02:03.240
 emotions or mind states which we've cultivated, or the

00:02:03.240 --> 00:02:06.160
 actions which come from them, or the

00:02:06.160 --> 00:02:07.440
 results of those actions.

00:02:07.440 --> 00:02:13.110
 And this is in the tradition of the meditation, we say it's

00:02:13.110 --> 00:02:17.240
 a wheel, you know, first you have

00:02:17.240 --> 00:02:21.410
 the mind state arise, these unpleasant or unwholesome mind

00:02:21.410 --> 00:02:23.240
 states, and then they lead

00:02:23.240 --> 00:02:26.510
 you to do things, and those things lead you to suffering,

00:02:26.510 --> 00:02:28.200
 they lead you to do bad deeds,

00:02:28.200 --> 00:02:31.550
 to do things which are cause for your own suffering or the

00:02:31.550 --> 00:02:33.080
 suffering or others.

00:02:33.080 --> 00:02:36.620
 So meditation makes you free, once you purify your mind,

00:02:36.620 --> 00:02:38.800
 once you stop doing, stop getting

00:02:38.800 --> 00:02:42.000
 angry or once you're able to control and to see through the

00:02:42.000 --> 00:02:43.960
 anger, see through the greed,

00:02:43.960 --> 00:02:46.380
 see through the addiction to this and to that, then you

00:02:46.380 --> 00:02:47.880
 stop doing these things, you stop

00:02:47.880 --> 00:02:51.690
 chasing after things, your actions become purified, and you

00:02:51.690 --> 00:02:53.120
 become free from the need

00:02:53.120 --> 00:02:56.220
 to do this, to do that, to say this, to say that, and you

00:02:56.220 --> 00:02:58.120
 don't cause trouble or suffering

00:02:58.120 --> 00:03:00.120
 for yourself or others.

00:03:00.120 --> 00:03:02.520
 And as a result, you don't have to face the results of your

00:03:02.520 --> 00:03:03.080
 actions.

00:03:03.080 --> 00:03:06.420
 So it's this wheel, these three things which you cut off,

00:03:06.420 --> 00:03:08.000
 you cut them off at the point

00:03:08.000 --> 00:03:12.780
 where you stop creating these bad mind states, these unwh

00:03:12.780 --> 00:03:16.120
olesome, unskillful, unuseful mind

00:03:16.120 --> 00:03:19.040
 states, and as a result your action becomes pure and the

00:03:19.040 --> 00:03:20.680
 results of your action become

00:03:20.680 --> 00:03:21.960
 pure.

00:03:21.960 --> 00:03:24.400
 Once the results of your action become pure and there's no

00:03:24.400 --> 00:03:25.680
 more suffering, there's no

00:03:25.680 --> 00:03:29.410
 more unpleasantness or the things that come to you, you don

00:03:29.410 --> 00:03:31.240
't see them as unpleasant,

00:03:31.240 --> 00:03:35.280
 then the cycle continues and you're able to, you react to

00:03:35.280 --> 00:03:37.480
 the results of your actions with

00:03:37.480 --> 00:03:41.430
 mindfulness and as a result you're able to continue to pur

00:03:41.430 --> 00:03:43.320
ify and to become free from

00:03:43.320 --> 00:03:46.400
 this cycle of suffering.

00:03:46.400 --> 00:03:51.510
 So this is the number one reason meditation simply allows

00:03:51.510 --> 00:03:54.120
 you to live your life in a pure,

00:03:54.120 --> 00:03:58.150
 a simple and a peaceful fashion where whatever comes to you

00:03:58.150 --> 00:04:00.320
, you take it for what it is and

00:04:00.320 --> 00:04:03.920
 you don't make more of it than what it really is.

00:04:03.920 --> 00:04:07.860
 And as a result this allows you to be free, free from all

00:04:07.860 --> 00:04:10.040
 sufferings that can exist in

00:04:10.040 --> 00:04:11.240
 the world.

00:04:11.240 --> 00:04:14.040
 So thanks for tuning in and this is the end of my series on

00:04:14.040 --> 00:04:15.920
 why everyone should meditate.

00:04:15.920 --> 00:04:21.060
 I hope that you've taken some of these lessons to heart and

00:04:21.060 --> 00:04:23.600
 that you are able to find the

00:04:23.600 --> 00:04:28.430
 time and find the encouragement in your own heart to come

00:04:28.430 --> 00:04:31.520
 to begin to practice this teaching

00:04:31.520 --> 00:04:36.640
 which truly and sincerely leads the people who practice it

00:04:36.640 --> 00:04:39.440
 truly and sincerely to freedom

00:04:39.440 --> 00:04:42.440
 from all sufferings that exist in the world.

00:04:42.440 --> 00:04:44.520
 So thanks again and all the best.

00:04:44.520 --> 00:04:46.520
 1

00:04:46.520 --> 00:04:48.520
 1

