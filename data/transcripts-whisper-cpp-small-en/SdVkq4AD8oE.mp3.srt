1
00:00:00,000 --> 00:00:03,840
 Hi, and welcome back to Ask a Monk.

2
00:00:03,840 --> 00:00:08,360
 Today's question comes from Natcho Rochi.

3
00:00:08,360 --> 00:00:11,200
 "Hello, Yutadamo.

4
00:00:11,200 --> 00:00:14,340
 How can meditation help me overcome an addiction to drugs

5
00:00:14,340 --> 00:00:15,260
 and alcohol?

6
00:00:15,260 --> 00:00:16,960
 I would really appreciate your answer.

7
00:00:16,960 --> 00:00:17,960
 Thanks for your time."

8
00:00:17,960 --> 00:00:23,790
 Well, I've gone over addiction before, but there are some

9
00:00:23,790 --> 00:00:27,000
 specifics about drugs and alcohol,

10
00:00:27,000 --> 00:00:34,000
 about physical addictions that they're mentioning.

11
00:00:34,000 --> 00:00:38,960
 First of all, with any addiction, as I've said before,

12
00:00:38,960 --> 00:00:41,520
 there are certain parts of the

13
00:00:41,520 --> 00:00:44,610
 mind that come into play, or certain parts of reality,

14
00:00:44,610 --> 00:00:46,360
 physical and mental, that come

15
00:00:46,360 --> 00:00:47,520
 into play.

16
00:00:47,520 --> 00:00:52,080
 The first one is the object of one's addiction.

17
00:00:52,080 --> 00:00:57,960
 So in this case, we're talking about a physical substance.

18
00:00:57,960 --> 00:01:01,670
 When you see it, or when you think about it, when you smell

19
00:01:01,670 --> 00:01:03,320
 it, when you taste it, or so

20
00:01:03,320 --> 00:01:06,280
 on, however it touches you.

21
00:01:06,280 --> 00:01:10,540
 Because that's what's going to set off the memory of the

22
00:01:10,540 --> 00:01:12,680
 bliss and the pleasure.

23
00:01:12,680 --> 00:01:16,610
 Even though it's like food, when you see the food, you don

24
00:01:16,610 --> 00:01:18,400
't get the taste of it, but yet

25
00:01:18,400 --> 00:01:19,960
 your mouth starts salivating.

26
00:01:19,960 --> 00:01:24,710
 So even before you take the drug, or take the alcohol, you

27
00:01:24,710 --> 00:01:26,440
 already have this craving

28
00:01:26,440 --> 00:01:29,560
 for it, just by seeing it.

29
00:01:29,560 --> 00:01:30,960
 And that's important.

30
00:01:30,960 --> 00:01:34,050
 That's going to be one of the first steps, or that's going

31
00:01:34,050 --> 00:01:35,440
 to be one of the big steps

32
00:01:35,440 --> 00:01:41,190
 in overcoming the addiction, is breaking that chain between

33
00:01:41,190 --> 00:01:43,800
 seeing it and wanting it.

34
00:01:43,800 --> 00:01:47,760
 If you can break it off there, then you can be said to

35
00:01:47,760 --> 00:01:50,440
 really have cut the addiction off.

36
00:01:50,440 --> 00:01:55,840
 The next thing is the memory that comes from seeing it.

37
00:01:55,840 --> 00:02:00,090
 You remember that this is your drug, this is your alcohol,

38
00:02:00,090 --> 00:02:01,960
 and you remember how good

39
00:02:01,960 --> 00:02:03,800
 it is, and so on.

40
00:02:03,800 --> 00:02:07,560
 And then after that comes the happy feeling.

41
00:02:07,560 --> 00:02:09,520
 After the happy feeling comes the liking.

42
00:02:09,520 --> 00:02:11,860
 After the liking comes the wanting.

43
00:02:11,860 --> 00:02:15,200
 After the wanting comes the chasing.

44
00:02:15,200 --> 00:02:19,090
 So we can break each of these steps up and focus on any one

45
00:02:19,090 --> 00:02:21,480
 of them, whichever one becomes

46
00:02:21,480 --> 00:02:24,040
 clear and they'll become clear, different ones will become

47
00:02:24,040 --> 00:02:25,320
 clear at different times.

48
00:02:25,320 --> 00:02:28,300
 And sometimes it's going to be clear, the object is going

49
00:02:28,300 --> 00:02:29,720
 to be clear, so we see the

50
00:02:29,720 --> 00:02:31,280
 object.

51
00:02:31,280 --> 00:02:36,000
 That's the first way to cut it off, say to yourself, seeing

52
00:02:36,000 --> 00:02:37,120
, seeing.

53
00:02:37,120 --> 00:02:40,120
 Then there's the memory or the feeling, the happy feeling

54
00:02:40,120 --> 00:02:41,880
 that comes from it, thinking,

55
00:02:41,880 --> 00:02:45,950
 oh yes, this is happy, or this is good, this is pleasurable

56
00:02:45,950 --> 00:02:46,240
.

57
00:02:46,240 --> 00:02:51,280
 So saying to yourself, happy, happy, and liking, liking.

58
00:02:51,280 --> 00:02:54,030
 The liking that comes from it, saying to yourself, liking,

59
00:02:54,030 --> 00:02:55,880
 wanting, saying wanting, wanting,

60
00:02:55,880 --> 00:02:57,600
 thinking about ways to get it and so on.

61
00:02:57,600 --> 00:03:00,500
 And even when you're taking the drug, and I've heard this

62
00:03:00,500 --> 00:03:01,960
 helps with people who take

63
00:03:01,960 --> 00:03:04,750
 cigarettes, even when you're smoking the cigarette to be

64
00:03:04,750 --> 00:03:06,960
 mindful, smoking, tasting, and so on,

65
00:03:06,960 --> 00:03:11,670
 and healing, or however, can help you to see the true

66
00:03:11,670 --> 00:03:14,160
 nature of the addiction.

67
00:03:14,160 --> 00:03:16,270
 And when you can break it all up like that, you actually

68
00:03:16,270 --> 00:03:17,480
 teach your mind that there's

69
00:03:17,480 --> 00:03:21,120
 really nothing really wonderful about this.

70
00:03:21,120 --> 00:03:25,400
 There's nothing really attractive about the substance.

71
00:03:25,400 --> 00:03:29,960
 And so this is theoretically how you do away with it.

72
00:03:29,960 --> 00:03:33,590
 It does work to an extent, but with things like drugs and

73
00:03:33,590 --> 00:03:35,480
 alcohol, I think there are

74
00:03:35,480 --> 00:03:37,520
 other factors in play.

75
00:03:37,520 --> 00:03:44,200
 First of all, the physical, or most of all, the physical

76
00:03:44,200 --> 00:03:45,220
 addiction.

77
00:03:45,220 --> 00:03:48,720
 Just like things with sensual desire or sexual desire, it's

78
00:03:48,720 --> 00:03:51,280
 something that builds up in your

79
00:03:51,280 --> 00:03:53,560
 brain, builds up in your body chemistry.

80
00:03:53,560 --> 00:03:56,730
 So for instance, with cigarettes, the nicotine builds up in

81
00:03:56,730 --> 00:03:57,480
 the brain.

82
00:03:57,480 --> 00:04:01,640
 And you have to realize that you're probably going to be

83
00:04:01,640 --> 00:04:04,440
 hard pressed to do away with that.

84
00:04:04,440 --> 00:04:06,720
 The other important thing that we have to keep in mind

85
00:04:06,720 --> 00:04:08,360
 besides the meditation practice,

86
00:04:08,360 --> 00:04:11,980
 as far as breaking it up into pieces and catching each

87
00:04:11,980 --> 00:04:14,800
 piece and acknowledging this seeing,

88
00:04:14,800 --> 00:04:19,160
 seeing or liking, liking, one by one by one, is reminding

89
00:04:19,160 --> 00:04:21,400
 yourself that these things are

90
00:04:21,400 --> 00:04:25,830
 not you and are not yours and have really nothing to do

91
00:04:25,830 --> 00:04:26,920
 with you.

92
00:04:26,920 --> 00:04:28,920
 Because the addiction is going to come up.

93
00:04:28,920 --> 00:04:30,400
 You're going to want the thing.

94
00:04:30,400 --> 00:04:35,530
 You're going to have this intense desire for it that really

95
00:04:35,530 --> 00:04:38,360
 wasn't wanted, wasn't expected.

96
00:04:38,360 --> 00:04:42,920
 You don't know when it's going to come and you can't

97
00:04:42,920 --> 00:04:43,800
 control it.

98
00:04:43,800 --> 00:04:47,160
 Realizing that not being able to control it is the essence

99
00:04:47,160 --> 00:04:48,880
 of the addiction, that it's

100
00:04:48,880 --> 00:04:50,640
 nothing to do with you.

101
00:04:50,640 --> 00:04:53,880
 It's going to come up, not expecting it to go away, not

102
00:04:53,880 --> 00:04:55,760
 trying to make it go away, not

103
00:04:55,760 --> 00:04:57,720
 trying to find a trick to make it go away.

104
00:04:57,720 --> 00:05:01,760
 You think of it like someone else's problem.

105
00:05:01,760 --> 00:05:03,660
 This is very much related to the meditation.

106
00:05:03,660 --> 00:05:06,760
 This is what theoretically happens when you say to yourself

107
00:05:06,760 --> 00:05:08,640
, "Liking, liking, wanting,

108
00:05:08,640 --> 00:05:11,200
 wanting, thinking, thinking," and so on.

109
00:05:11,200 --> 00:05:15,900
 The problem is it's a fight between the two.

110
00:05:15,900 --> 00:05:17,240
 On the one hand, we really want it.

111
00:05:17,240 --> 00:05:20,480
 On the other hand, we really want to do away with it.

112
00:05:20,480 --> 00:05:25,030
 We have to as well remind ourselves that what we're seeing

113
00:05:25,030 --> 00:05:27,320
 when we do this is the truth.

114
00:05:27,320 --> 00:05:30,260
 When we say to ourselves, "Wanting, wanting," we see, "Oh

115
00:05:30,260 --> 00:05:32,760
 my gosh, I can't control it.

116
00:05:32,760 --> 00:05:34,520
 It's too much for me."

117
00:05:34,520 --> 00:05:37,110
 Realizing that that's the truth of it, yeah, it's totally

118
00:05:37,110 --> 00:05:38,100
 uncontrollable.

119
00:05:38,100 --> 00:05:41,390
 It's not wrong that when you say to yourself, "Liking,

120
00:05:41,390 --> 00:05:43,880
 liking, wanting, wanting, seeing,"

121
00:05:43,880 --> 00:05:46,620
 that you still end up wanting and the wanting just gets

122
00:05:46,620 --> 00:05:48,240
 bigger and bigger and bigger.

123
00:05:48,240 --> 00:05:49,480
 That's not a problem.

124
00:05:49,480 --> 00:05:51,630
 What you have to do is see that that wanting doesn't mean

125
00:05:51,630 --> 00:05:52,200
 anything.

126
00:05:52,200 --> 00:05:55,000
 It has no inherent essence.

127
00:05:55,000 --> 00:05:58,320
 It is just wanting.

128
00:05:58,320 --> 00:06:02,880
 It's not you, it's not yours, it's not under your control.

129
00:06:02,880 --> 00:06:05,520
 One of the most important things in Buddhism is realizing

130
00:06:05,520 --> 00:06:07,080
 this non-self, realizing that

131
00:06:07,080 --> 00:06:10,300
 there's nothing inside of ourselves or in the world around

132
00:06:10,300 --> 00:06:11,680
 us that we can really say

133
00:06:11,680 --> 00:06:15,080
 is a self, is a soul, is I.

134
00:06:15,080 --> 00:06:16,720
 Everything comes and goes.

135
00:06:16,720 --> 00:06:19,080
 When we can see like that, we don't cling to anything.

136
00:06:19,080 --> 00:06:24,370
 There's no thought that I, me, mine, and we're able to

137
00:06:24,370 --> 00:06:26,040
 float freely.

138
00:06:26,040 --> 00:06:27,040
 We're able to be free.

139
00:06:27,040 --> 00:06:32,080
 It's like if someone yells at you, they're yelling at

140
00:06:32,080 --> 00:06:34,440
 pieces of experience.

141
00:06:34,440 --> 00:06:35,440
 They're yelling at body.

142
00:06:35,440 --> 00:06:36,440
 They're yelling at the mind.

143
00:06:36,440 --> 00:06:38,320
 Who are they insulting?

144
00:06:38,320 --> 00:06:40,680
 Are they insulting your hair?

145
00:06:40,680 --> 00:06:44,640
 Are they insulting your skin?

146
00:06:44,640 --> 00:06:45,640
 The same goes with addiction.

147
00:06:45,640 --> 00:06:48,480
 I mean, who is it that wants?

148
00:06:48,480 --> 00:06:49,720
 Is it my hair that wants?

149
00:06:49,720 --> 00:06:53,080
 Is it my skin that wants or so on?

150
00:06:53,080 --> 00:06:54,080
 Is it my brain that wants?

151
00:06:54,080 --> 00:06:56,200
 I mean, these are all just pieces.

152
00:06:56,200 --> 00:07:02,170
 They're all just physical and mental phenomena that arise

153
00:07:02,170 --> 00:07:03,560
 and cease.

154
00:07:03,560 --> 00:07:06,950
 And I mean to see that that's all this is, not giving it

155
00:07:06,950 --> 00:07:07,640
 power.

156
00:07:07,640 --> 00:07:10,400
 I think that's one of the biggest things that comes from

157
00:07:10,400 --> 00:07:12,120
 this is that we're not giving power

158
00:07:12,120 --> 00:07:13,120
 to it.

159
00:07:13,120 --> 00:07:15,460
 When you remind yourself it is what it is, it is what it is

160
00:07:15,460 --> 00:07:15,640
.

161
00:07:15,640 --> 00:07:19,560
 You say to yourself, liking, it is liking, it is liking,

162
00:07:19,560 --> 00:07:21,840
 that's all it is, liking, liking,

163
00:07:21,840 --> 00:07:24,880
 wanting, wanting, happy, happy.

164
00:07:24,880 --> 00:07:28,240
 Acknowledging the pleasure is that you don't give it the

165
00:07:28,240 --> 00:07:30,040
 power that it used to have.

166
00:07:30,040 --> 00:07:33,110
 When we feel guilty about it, when we feel angry about it,

167
00:07:33,110 --> 00:07:34,440
 when we try to find a way

168
00:07:34,440 --> 00:07:39,150
 to overcome it, when we try to forget about it, try to

169
00:07:39,150 --> 00:07:41,920
 divert our own attention away from

170
00:07:41,920 --> 00:07:46,000
 it and so on, we're giving it power.

171
00:07:46,000 --> 00:07:47,920
 We're saying that this is something dangerous.

172
00:07:47,920 --> 00:07:49,560
 This is something terrible.

173
00:07:49,560 --> 00:07:50,760
 This is something big.

174
00:07:50,760 --> 00:07:54,400
 This is a problem.

175
00:07:54,400 --> 00:07:56,380
 Simply put, this is a problem.

176
00:07:56,380 --> 00:07:58,460
 When we say to ourselves, this is this, it is what it is,

177
00:07:58,460 --> 00:07:59,520
 we're letting it be what it

178
00:07:59,520 --> 00:08:00,560
 is.

179
00:08:00,560 --> 00:08:02,770
 When we feel happy because of the addiction, it brings us

180
00:08:02,770 --> 00:08:03,360
 pleasure.

181
00:08:03,360 --> 00:08:05,330
 We should acknowledge that pleasure and not feel guilty

182
00:08:05,330 --> 00:08:05,800
 about it.

183
00:08:05,800 --> 00:08:06,960
 Yes, it's pleasure.

184
00:08:06,960 --> 00:08:08,360
 It is what it is.

185
00:08:08,360 --> 00:08:10,540
 And then we can see that, yeah, it is what it is, but it's

186
00:08:10,540 --> 00:08:11,880
 not really anything special.

187
00:08:11,880 --> 00:08:13,680
 It just is pleasure.

188
00:08:13,680 --> 00:08:16,640
 It comes and it goes just like everything else.

189
00:08:16,640 --> 00:08:17,640
 And that should help.

190
00:08:17,640 --> 00:08:21,540
 But as I said, alcohol and drugs, you have to realize it's

191
00:08:21,540 --> 00:08:23,360
 something that can be very

192
00:08:23,360 --> 00:08:26,440
 physical and it's something that has to take a long time.

193
00:08:26,440 --> 00:08:30,140
 Don't expect meditation to get rid of the addiction, but

194
00:08:30,140 --> 00:08:32,000
 expect it to allow you to deal

195
00:08:32,000 --> 00:08:34,620
 with the addiction to the point where you don't need to use

196
00:08:34,620 --> 00:08:35,960
 the substance even though

197
00:08:35,960 --> 00:08:37,280
 you're still addicted.

198
00:08:37,280 --> 00:08:40,240
 You're able to live with the addiction.

199
00:08:40,240 --> 00:08:43,850
 You're able to live with the wanting, I mean, not having to

200
00:08:43,850 --> 00:08:45,600
 chase after the wanting.

201
00:08:45,600 --> 00:08:47,840
 So I hope that helps and good luck.

202
00:08:47,840 --> 00:08:49,760
 And keep the questions coming.

