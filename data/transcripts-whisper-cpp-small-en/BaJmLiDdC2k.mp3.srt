1
00:00:00,000 --> 00:00:04,200
 Hi and welcome back to Ask a Monk. Today we have a question

2
00:00:04,200 --> 00:00:07,000
 from German1184.

3
00:00:07,000 --> 00:00:10,710
 "Yutadamo, how much emphasis does your tradition put on

4
00:00:10,710 --> 00:00:16,000
 book learning? In Tibet, the practice seems to have much

5
00:00:16,000 --> 00:00:19,240
 memorization of root texts and commentaries and the

6
00:00:19,240 --> 00:00:22,240
 debating upon them. Some people approach the practice in

7
00:00:22,240 --> 00:00:24,000
 this way. What do you think?"

8
00:00:27,000 --> 00:00:34,720
 Well, I think that no one would disagree that book learning

9
00:00:34,720 --> 00:00:42,710
 has its place. I've often complained that even in the

10
00:00:42,710 --> 00:00:45,440
 tradition that I follow, in many of the traditions of

11
00:00:45,440 --> 00:00:50,830
 Buddhism, there is too much of an emphasis on textual study

12
00:00:50,830 --> 00:00:54,000
 as an end more than a means.

13
00:00:55,000 --> 00:01:01,780
 And ideally we see textual study as similar to studying a

14
00:01:01,780 --> 00:01:08,560
 road map, learning about the path, about the way we're

15
00:01:08,560 --> 00:01:10,000
 going to go.

16
00:01:10,000 --> 00:01:18,000
 Or a modern example would be learning how to drive a car.

17
00:01:19,000 --> 00:01:21,960
 So you can study a road map as much as you want. You can

18
00:01:21,960 --> 00:01:25,190
 learn how to drive. You can learn everything, all the

19
00:01:25,190 --> 00:01:28,240
 tricks of the road, everything there is to know about

20
00:01:28,240 --> 00:01:29,000
 traveling.

21
00:01:29,000 --> 00:01:32,140
 But if you never take the journey, you'll never get

22
00:01:32,140 --> 00:01:33,000
 anywhere.

23
00:01:33,000 --> 00:01:38,760
 I think this has to be said about textual study. Now there

24
00:01:38,760 --> 00:01:43,470
 is some argument to be made for the intellectual

25
00:01:43,470 --> 00:01:50,390
 appreciation of the teachings, the understanding of the

26
00:01:50,390 --> 00:01:54,000
 teaching intellectually.

27
00:01:54,000 --> 00:01:59,860
 And the ability to use the teachings directly as you're

28
00:01:59,860 --> 00:02:05,300
 studying them to examine yourself. And there is, to a

29
00:02:05,300 --> 00:02:07,000
 certain level, this is possible.

30
00:02:07,000 --> 00:02:10,970
 It's possible that as you're reading a teaching or as you

31
00:02:10,970 --> 00:02:14,610
're listening to a talk, that you're actually also

32
00:02:14,610 --> 00:02:19,000
 reflecting on the teachings and using them practically.

33
00:02:20,000 --> 00:02:24,810
 But as I said, I think there are many traditions, and not

34
00:02:24,810 --> 00:02:30,000
 exactly traditions, I would say monasteries, teachers,

35
00:02:30,000 --> 00:02:33,230
 being more specific than talking about any specific type of

36
00:02:33,230 --> 00:02:34,000
 Buddhism.

37
00:02:34,000 --> 00:02:40,140
 But there are many Buddhists who like to get together in a

38
00:02:40,140 --> 00:02:46,300
 group and debate or study or discuss a teaching and think

39
00:02:46,300 --> 00:02:52,000
 of that as a fundamental part of their practice.

40
00:02:52,000 --> 00:02:57,650
 It's useful to an extent, but if you find a group or a

41
00:02:57,650 --> 00:03:04,000
 teacher who is only going so far as to give textual study,

42
00:03:04,000 --> 00:03:07,030
 then you have to say that there is something missing from

43
00:03:07,030 --> 00:03:08,000
 the teaching.

44
00:03:08,000 --> 00:03:14,400
 In the end, studying as an end simply makes you better at

45
00:03:14,400 --> 00:03:19,860
 studying. The more you study, the better you are at memor

46
00:03:19,860 --> 00:03:24,840
izing, the better you are at logic, at being able to assimil

47
00:03:24,840 --> 00:03:29,700
ate teachings and make sense of words and the syntax and so

48
00:03:29,700 --> 00:03:30,000
 on.

49
00:03:30,000 --> 00:03:34,300
 And assimilate it into your brain and get the concepts. And

50
00:03:34,300 --> 00:03:38,510
 it can make you a good teacher, it can make you very

51
00:03:38,510 --> 00:03:43,000
 confident of yourself because of your learning.

52
00:03:43,000 --> 00:03:46,820
 If you're good at debate, when you practice debate, it just

53
00:03:46,820 --> 00:03:50,350
 makes you better at debate, it makes you more skilled at

54
00:03:50,350 --> 00:03:54,260
 argument, at winning people over, at explaining things to

55
00:03:54,260 --> 00:03:59,000
 people, at being able to reply to criticisms and so on.

56
00:03:59,000 --> 00:04:03,430
 But until you practice meditation, the only way you're

57
00:04:03,430 --> 00:04:07,970
 going to get better at understanding reality is to look at

58
00:04:07,970 --> 00:04:09,000
 reality.

59
00:04:09,000 --> 00:04:13,340
 Neither of these things, study, debate or any of the other

60
00:04:13,340 --> 00:04:18,000
 associated activities, helps you to understand reality.

61
00:04:18,000 --> 00:04:21,170
 The only way you can get better at seeing reality for what

62
00:04:21,170 --> 00:04:24,000
 it is, is to look at it, is to train yourself.

63
00:04:24,000 --> 00:04:27,300
 So the meditation is a very specific action where we're

64
00:04:27,300 --> 00:04:30,000
 training ourselves to see things as they are.

65
00:04:30,000 --> 00:04:35,640
 It's a training, it's not intellectual, it's not a

66
00:04:35,640 --> 00:04:38,000
 realization that comes to you.

67
00:04:38,000 --> 00:04:40,830
 It's a change in the way you look at things. So instead of

68
00:04:40,830 --> 00:04:44,520
 seeing things as me, as mine, as good, as bad, you actually

69
00:04:44,520 --> 00:04:45,000
 see them.

70
00:04:45,000 --> 00:04:52,200
 You're actually able to experience something simply as it

71
00:04:52,200 --> 00:04:54,000
 is. When you see something, you only see it.

72
00:04:54,000 --> 00:04:56,570
 When you hear something, you only hear it. When you feel

73
00:04:56,570 --> 00:04:58,000
 pain, there's only the pain.

74
00:04:58,000 --> 00:05:02,000
 When you feel happy, there's only the happiness and so on.

75
00:05:02,000 --> 00:05:05,650
 And so it's a skill rather than, it's nothing that can come

76
00:05:05,650 --> 00:05:09,000
 from someone else, it's nothing someone else can teach you.

77
00:05:09,000 --> 00:05:12,050
 They can only teach you the way of developing the skill and

78
00:05:12,050 --> 00:05:15,560
 it's up to you to develop this skill until you're able to

79
00:05:15,560 --> 00:05:17,000
 do it for yourself.

80
00:05:17,000 --> 00:05:23,740
 Once you're able to see reality for what it is, only then

81
00:05:23,740 --> 00:05:28,000
 can you say that you'll actually be free from suffering.

82
00:05:28,000 --> 00:05:31,840
 There's only one way to realize the truth of the Buddha's

83
00:05:31,840 --> 00:05:35,000
 teaching and that's through the practice.

84
00:05:35,000 --> 00:05:39,800
 So I hope that makes sense. I think it's generally

85
00:05:39,800 --> 00:05:41,830
 understood that this is the case, that there are three

86
00:05:41,830 --> 00:05:43,000
 parts to the Buddha's teaching.

87
00:05:43,000 --> 00:05:46,090
 There's study and you study for the purpose of practicing

88
00:05:46,090 --> 00:05:49,000
 and you practice for the purpose of realization.

89
00:05:49,000 --> 00:05:54,000
 So there's the study, the practice and the realization.

90
00:05:54,000 --> 00:06:00,570
 So I would say there's no place for the accumulation of

91
00:06:00,570 --> 00:06:05,000
 book knowledge. Knowledge is for the purpose of practice.

92
00:06:05,000 --> 00:06:08,820
 If it's not practical, if it's not something that you

93
00:06:08,820 --> 00:06:12,810
 intend to use either or to help someone else, then it

94
00:06:12,810 --> 00:06:16,000
 really isn't a Buddhist learning.

95
00:06:16,000 --> 00:06:19,560
 Buddhist learning is for the purpose of putting it into

96
00:06:19,560 --> 00:06:23,000
 practice for the purpose of realizing the truth.

97
00:06:23,000 --> 00:06:26,060
 If it's not something that leads you to realize the truth,

98
00:06:26,060 --> 00:06:28,840
 then it's also not considered to be proper learning in a

99
00:06:28,840 --> 00:06:30,000
 Buddhist sense.

100
00:06:30,000 --> 00:06:33,000
 Okay, so hope that helps. Thanks for the question.

