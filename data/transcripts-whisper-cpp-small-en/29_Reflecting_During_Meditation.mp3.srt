1
00:00:00,000 --> 00:00:04,020
 Reflecting during meditation

2
00:00:04,020 --> 00:00:09,020
 Question. Sometimes I am facing an issue, and I cheat a

3
00:00:09,020 --> 00:00:13,600
 little by using my meditation time to reflect on it.

4
00:00:13,600 --> 00:00:17,520
 I try to break it down to clearly understand it and

5
00:00:17,520 --> 00:00:22,200
 understand why it is affecting me negatively or positively.

6
00:00:22,200 --> 00:00:26,870
 Is this a valid meditation? If I just repeat, disliking,

7
00:00:26,870 --> 00:00:31,090
 disliking, the issue keeps arising, and it seems like my

8
00:00:31,090 --> 00:00:35,740
 mind is screaming for an interpretation compared to a

9
00:00:35,740 --> 00:00:38,120
 passive observation.

10
00:00:38,120 --> 00:00:42,490
 Answer. Valid meditation does not mean anything as

11
00:00:42,490 --> 00:00:45,400
 meditation would mean anything.

12
00:00:45,400 --> 00:00:49,340
 Meditation is such a vague word. Do you mean the qatis

13
00:00:49,340 --> 00:00:53,410
 meditation? There are many different kinds of activity

14
00:00:53,410 --> 00:00:55,100
 called meditation.

15
00:00:55,100 --> 00:00:58,660
 Meditating and reflecting on your problems is very

16
00:00:58,660 --> 00:01:01,900
 different from what we teach and practice.

17
00:01:01,900 --> 00:01:05,690
 If you want an answer like "What is such a meditation like

18
00:01:05,690 --> 00:01:08,960
 it leads to?" as it is not particularly based on

19
00:01:08,960 --> 00:01:09,800
 mindfulness,

20
00:01:09,800 --> 00:01:12,910
 I would say that you are more likely to be plagued by

21
00:01:12,910 --> 00:01:16,700
 judgments and reactions when you reflect upon a thought.

22
00:01:16,700 --> 00:01:21,110
 It can be a useful practice at times, but it cannot replace

23
00:01:21,110 --> 00:01:22,400
 mindfulness.

24
00:01:22,400 --> 00:01:25,800
 You can understand this when we talk about the second part

25
00:01:25,800 --> 00:01:27,100
 of your question.

26
00:01:27,100 --> 00:01:30,970
 You say that when you just repeat the mantra, your mind is

27
00:01:30,970 --> 00:01:33,300
 screaming for an interpretation.

28
00:01:33,300 --> 00:01:37,420
 This is what we expect to see. This is not a sign that your

29
00:01:37,420 --> 00:01:39,300
 practice is going wrong.

30
00:01:39,300 --> 00:01:43,300
 This is a sign that reality is not under your control.

31
00:01:43,300 --> 00:01:47,580
 When you note disliking, disliking, and the issue keeps

32
00:01:47,580 --> 00:01:48,300
 arising,

33
00:01:48,300 --> 00:01:52,000
 what you are seeing is that it is not under your control,

34
00:01:52,000 --> 00:01:53,700
 that you are not in charge,

35
00:01:53,700 --> 00:01:57,620
 and that it does not turn off just because you want it to

36
00:01:57,620 --> 00:01:58,500
 go away.

37
00:01:58,500 --> 00:02:03,170
 This is what we are trying to see if the issue keeps coming

38
00:02:03,170 --> 00:02:04,900
 back again and again,

39
00:02:04,900 --> 00:02:09,160
 and you keep staying objective, or as you say, passively

40
00:02:09,160 --> 00:02:10,100
 observing it,

41
00:02:10,100 --> 00:02:13,300
 eventually you will be fed up with it saying,

42
00:02:13,300 --> 00:02:17,270
 "Look, this is just making me suffer again and again and

43
00:02:17,270 --> 00:02:18,100
 again."

44
00:02:18,100 --> 00:02:23,300
 It is necessary for our issues to come up again and again.

45
00:02:23,300 --> 00:02:26,300
 We are not trying to get rid of problems.

46
00:02:26,300 --> 00:02:30,300
 We are trying to understand them as experiences.

47
00:02:30,300 --> 00:02:34,380
 When you understand your experiences clearly enough, your

48
00:02:34,380 --> 00:02:35,900
 mind will cease to react

49
00:02:35,900 --> 00:02:40,320
 in the ways you ordinarily react, because you will see they

50
00:02:40,320 --> 00:02:43,500
 just cause you suffering again and again.

51
00:02:43,500 --> 00:02:46,760
 The unfortunate truth is that to free yourself from

52
00:02:46,760 --> 00:02:47,500
 something,

53
00:02:47,500 --> 00:02:51,570
 you really have to go through it, experiencing it again and

54
00:02:51,570 --> 00:02:52,300
 again.

55
00:02:52,300 --> 00:02:53,900
 There is no quick fix.

56
00:02:53,900 --> 00:02:58,050
 When you talk about a more proactive practice of trying to

57
00:02:58,050 --> 00:02:59,700
 find out what is wrong,

58
00:02:59,700 --> 00:03:03,700
 it just encourages the habit of trying to fix things.

59
00:03:03,700 --> 00:03:06,300
 That habit is problematic.

60
00:03:06,300 --> 00:03:10,570
 With it, you will never become disenchanted, turn away, or

61
00:03:10,570 --> 00:03:11,100
 let go,

62
00:03:11,100 --> 00:03:15,100
 because you will always be trying to make things better.

