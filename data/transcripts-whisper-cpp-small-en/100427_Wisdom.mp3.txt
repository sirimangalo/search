 Okay, so welcome everyone to our weekly Deer Park session.
 And because we only
 have a session once a week, I thought it's probably useful
 not just to give
 talks but to also have some sort of guided introduction to
 actual meditation
 practice. So this talk is going to be a little bit more
 hands-on, so to speak.
 Today's talk, the topic is wisdom. Because of course wisdom
 in Buddhism is the
 highest virtue, the highest quality which we're hoping to
 attain, hoping to gain
 from our practice. The Buddha said, "compared wisdom like
 the moon amongst
 the stars." When you look at the night sky, you see a lot
 of bright lights and you
 say it's amazing how bright the stars are. But then when
 the moon comes up, you
 can see that it's on a whole other level. It's a whole
 other volume of
 brightness in comparison to the stars. And so we should
 keep this in mind when
 we think of what is it that we're trying to gain, trying to
 get, trying to attain
 in our practice. And we should take this as an
 understanding that what we're
 really hoping to gain is wisdom. And so this is of course a
 very important
 topic of discussion. And I think it's something that
 perhaps many people who
 surf the internet and virtual reality and so on are not
 thinking to obtain. In
 fact, often the internet and these sort of game-like worlds
 are considered to be
 sort of like a pastime or a break from our spiritual quest.
 And many people
 come here because perhaps though they know they're looking
 for something,
 they don't realize the importance of wisdom. And so it may
 have never entered
 their minds that wisdom was of any benefit. But we can see
 even in our daily
 lives how important wisdom is, how important it is simply
 to live our lives,
 simply to be successful. If we want to succeed in our work,
 we need a certain
 amount of wisdom. We need wisdom having learned things. We
 have to have
 gone to school and we have to have heard and seen and read
 many things which are
 appropriate. We have to have kept it in mind and we have to
 have thought about it
 and put it into practice. And we have to have had
 experience. And all of these
 things are a part of what it means by the word wisdom. If
 we want to be
 successful in relationships, if we want to have good
 friends, if we want to be
 surrounded by kind and caring people, then we have to know
 how to relate to
 them and we have to have social skills and we have to have
 interpersonal skills
 and we have to know how to tell the difference between a
 person who is
 looking for good things and a person who is headed towards
 bad things or
 unwholesome things. A person who is going to lead us on a
 bad path and a person
 who is going to lead us to improvement, is going to have
 all the qualities of a
 good friend. We have to know how to deal with these people
 if we're a kind of a
 person who doesn't have a certain level of wisdom, then it
's quite likely that
 people who are somehow high-minded are not going to be at
 all interested in
 associating with that person. So really everything we do
 and all of our
 achievements in this world, all of the people who we esteem
 as the greats,
 it really all has to do with some sort of level of wisdom.
 We think of Socrates
 or we think of Einstein or any of the great, even the great
 authors or the
 great scientists or philosophers. We can think of what
 makes a person great and
 we can see how wisdom is actually very important. Now, so
 what we're dealing
 with in Buddhism is a wisdom teaching, a teaching on some
 sort of knowledge or
 some sort of understanding. Similar you could think of in
 terms of Plato or
 or or Socrates where they had some kind of philosophy and
 way of looking at the
 world and they had teachings and they had a outlook and a
 method and so on. When we
 talk about Buddhism we're not talking about a faith-based
 religion or
 something based on magic or based on superstition or or
 even based on
 super mundane states. We're talking about a practice for
 understanding that
 teaches us more about ourselves and more about the world
 around us. It helps us to
 understand those things that we're confronted with on a
 daily basis. It helps
 us to understand all of those huge big questions which in
 the end turn out to
 be very very mundane such as what is the meaning of life,
 what is the meaning of
 the universe, what is the meaning of reality, and so on.
 And so this is really
 where we start in Buddhism and it's where we finish. Where
 we start in
 Buddhism is we start by teaching. We start by learning, by
 gaining that
 sort of wisdom which is theoretical, similar to going to
 school and studying
 or listening to people talk or reading books or so on. All
 wisdom, all
 attainment of understanding comes or starts from this basis
 of theoretical
 knowledge. Some people, some many Buddhists in the
 beginning they can
 mistake this and they think that theoretical knowledge is
 is is useless.
 It's unnecessary. That the only wisdom that's worth
 anything is
 experiential wisdom. And this sort of, it can seem kind of
 naive from someone who
 has really put into practice these teachings when you
 realize how
 subtle and how difficult it is to get on sort of this this
 path of
 understanding to avoid all the pitfalls of some sort of
 delusion or practice which which is leading one down to a
 dead end or so on.
 That without the theory, without a teaching, it's basically
 like having to
 reinvent the wheel or reinvent the entire path. And so it's
 possible and it's
 shown, the fact that it's possible is shown by the fact
 that the Buddha
 actually did it. He had no one to teach him.
 But it's very very difficult and it it's actually
 impossible. You can say that
 the Buddha had no teacher but he did a lot of theoretical
 study. He did a lot of
 of laying down the axioms and the basics, talking about
 what is the
 what is the basics of reality and through experiment he was
 able to
 come up with this theory. And so unless we're willing to do
 that, we
 shouldn't think that we can just go and sit and somehow
 enlightenment is going
 to come to us. That you have to do all of this preparatory
 work, at least to a
 certain extent. So what I'm going to try to give today is a
 little bit of
 theory that's important. When we talk about wisdom
 teachings, we're
 generally talking about vast amounts of information. So
 whereas
 people might think that Buddhism is all about just sitting
 still,
 it's certainly a very big part of it, but the theory is so
 profound and
 the path is so subtle that there has accumulated an
 incredible body of
 literature. Even just speaking of the Buddhist teachings
 itself, the Buddha didn't just say go and sit,
 though he said that many times. He also gave many teachings
 that were useful to
 help people to sit and to meditate in a proper manner,
 and in a manner that would allow them to see things clearly
 and not get
 lost and distracted.
 So it's perhaps a little naive to think by any stretch of
 the imagination
 on a single day or a half an hour in the next 10 minutes. I
'm somehow going to
 give you an overview of the Buddha's teaching,
 but we can come back a little ways towards this idea that
 Buddhism is just a teaching, a practical teaching.
 It's a teaching of gaining experiential wisdom. We don't
 want to give up
 theoretical knowledge all together, and in fact the more
 theoretical knowledge we have, the better equipped we are.
 But we have to find some sort of core and some sort of
 basis which we can
 then say, "Okay, if I practice according to these
 principles, I'm practicing according to the Buddhist
 teaching." Luckily for us, of course, this does exist.
 There are a few ways of approaching this, and one of the
 best ways is
 is laid down in the Buddha's own words in his teachings on
 the Four
 Foundations of Mindfulness, which is a very common teaching
 that you'll find
 both in the Buddhist teachings and in those who
 even today spread the Buddhist teachings. So not only are
 many Buddhists
 practicing this, but you can actually find it in the Buddha
's teachings as one
 of the core practical teachings, where many times the
 Buddha said that if you want a very
 condensed version of his teachings, or if you don't have a
 lot of time,
 suppose you're a person who has a busy schedule or is
 already
 grown up and gotten beyond the time where they could put
 great amounts of time into study, then what you should do
 to focus your efforts is to set yourself up in morality
 first and then start practicing the Four Foundations of
 Mindfulness,
 to get a basic understanding of what it means to be moral,
 and to get an understanding of the truth.
 You know, you get this basic doctrinal teaching, you know,
 "What shouldn't I do,
 and what is the ultimate goal?" or "What is the truth?"
 just from a theoretical level. Then start practicing the
 Four Foundations of
 Mindfulness. So this is the the basics that we have
 to give a person. The first theoretical thing we have to
 give is to teach them
 what it is to be moral, and basically this means
 the kind of morality that you would find in in most
 religious systems, at least
 in theory, if not in practice. Not to kill, not to steal,
 not to commit adultery or cheat, not to lie, and particular
 to the meditation
 teaching, not to take drugs or alcohol, because of course
 this is
 something that clouds the mind. It's the heading in the
 opposite direction for
 mindfulness. So these basic rules, these are called
 the Five Precepts, or the Five Training Rules in
 in Buddhism. If someone can keep them, then they've got the
 basics of morality.
 So from a theoretical level it's important if we're talking
 about
 gaining wisdom that we start here. Why? Because these are
 things that
 the reason that they're considered immoral is
 because of the effect that they have on one's mind, the
 effect that they have on
 one's peace of mind, the effect they have on one's clarity
 of mind. People who betray these precepts,
 though it may not seem to us readily if we're not looking
 carefully,
 they have a profound effect on your mind. If you're a
 person who's ever killed a
 substantial being, large animals, or even worse killed a
 human being, for these people they can generally
 readily appreciate the effect that it has on their mind.
 People who steal, people who cheat, people who lie,
 and people of course who take drugs and alcohol.
 If you're objective about it you can see how the effect
 they have on your mind.
 And so this is very important in the beginning.
 The second thing is this understanding of reality or the
 truth from a
 theoretical perspective. So we skip all the way to the end
 and we say what is it
 that we're trying to achieve or what is it that we're
 trying to gain?
 What is the truth? And of course this is something that
 you can explain on various levels of
 complexity, but the easiest way to understand that the
 Buddha gave is that
 all things are indeed not worth clinging to. That there is
 nothing in the world
 worth clinging to. Nothing in existence worth clinging to.
 This is what we should understand as the truth. This is
 something that if
 we want to say that we understand the Buddha's teaching,
 the Buddha said understand this point and then you have
 this
 basic understanding of the Buddha's teaching. You don't
 have to go studying
 the 84,000 teachings of the Buddha. Understand that there
 is no thing
 and it's only an intellectual or it's only a theoretical
 understanding. You
 don't yet understand if this is true or not, but you
 understand this is what the
 Buddha taught and that's enough. And you say okay this is
 going to be the mantra
 or the the motto you can say for the rest of my
 practice. If I keep this in mind it's going to lead me
 along the the path of
 the Buddha. If at some point I say no this path is not for
 me then fine,
 but at least I know this is what the Buddha taught. That
 all things,
 no thing is worth clinging to. Once we have these two
 foundations, this is where
 the Buddha said okay now go on to practice the four
 foundations of
 mindfulness. So theoretically once we have these
 these foundation teachings in our minds, all we have to do
 is learn what are the
 four foundations of mindfulness and then start practicing.
 So I'm going to give you give everyone a teaching now.
 You're welcome to follow along. You're welcome to just
 listen. But the idea
 would be for now for all of us to stop texting and
 maybe you can close your eyes, preferably close your eyes.
 You can put
 your avatar on busy if you get lots of texts
 or IMs. Let's start to
 learn about what is it that the Buddha taught or the four
 foundations of
 mindfulness and what is it that we're taught to be
 mindful of.
 The first foundation that the Lord Buddha taught is the
 foundation of
 mindfulness of the body. This is the reason it's first or
 perhaps the best
 explanation as to why it's first is because it's the
 easiest
 to grasp, to see. And this is agreed upon by
 the whole long tradition of Buddhist meditation teachers
 that
 the easiest and most coarse, most obvious
 object of attention in terms of what's really there is the
 body.
 And so the Buddha taught us that we should know the body
 for the body,
 know the body as body, know the body for what it is.
 Because as with everything else this is something that we
're going to cling to.
 As the Buddha said, nothing in the world is worth clinging
 to.
 And what we're going to at least test, we're going to test
 this theory.
 We're going to do a series of tests and and experiments to
 see whether this is
 true. And so we're going to learn about how we
 cling to things like the body. And we're going to see
 whether that's
 actually of any benefit to us. And with the idea that
 according to the
 Lord Buddha there was no benefit from clinging.
 So we're going to watch the body.
 How we do this, how we practice all four
 foundations of mindfulness according to the teaching that
 that I profess, and of course there are many
 interpretations of what is meant by being mindful of these
 four things.
 But according to the teaching that I have we use a mantra
 which is similar to any ancient Indian meditation technique
 where
 you have a special mantra which focuses your attention on
 the object
 or on the thing, on the phenomenon, which is summed up by
 the mantra.
 So it's usually something like God or some special
 experience or special state or special object.
 In Buddhist meditation of course our object is reality
 because we want to see
 things as they are and as I said we want to learn about our
 clinging to things. And we want to come to see this truth
 that nothing is worth clinging to. Because when we see that
 of course we
 won't cling to anything. When we don't cling we'll be free.
 We'll be like a bird
 flying unattached to anything.
 So our use of the mantra is the same technique but a
 different object.
 It's going to be a very ordinary object and the first one
 is the body.
 So the technique that we often give to people is to start
 watching in the
 stomach because the most obvious part of the
 body when you're sitting still not doing anything
 is the movement of the stomach that rises and falls as the
 breath goes in and out.
 For many people nowadays this isn't that obvious
 because nowadays we have a lot of stresses that probably
 weren't apparent
 in the time of the Buddha or at least for monks meditating
 off in
 the forest. But if it's not readily apparent in the
 beginning I guarantee you that it will be as you go on as
 you get more
 proficient in the practice as your breath calms down as
 your body
 loosens up it will become very very obvious
 because your breath will become more natural. If you want
 to see this is true
 you can lie on your back. If you lie on your back and just
 feel
 your breath you'll see that you're just like a baby
 again and your belly is indeed rising and falling naturally
.
 But the problem is when we sit up we become all tense and
 stressed and
 get back into this forced unnatural state.
 And so our breath becomes shallow and based on the chest
 and so on. If you
 want the best way to bridge the gap
 is to put your hand on your stomach and just watch it
 feel it rising and falling with your hand.
 As it rises we're just going to use this mantra rising.
 We just say to ourselves rising
 and when it falls we say falling
 rising
 falling. We don't say it out loud we're just saying it in
 our mind and our mind
 is at the stomach.
 As we do this we're getting into the second type of wisdom
 which is wisdom
 sort of the mental exercise putting into practice the
 things that
 we've learned and trying to understand how they work.
 So as we say to ourselves rising it's a sort of wisdom
 it's a affirmation of the truth of that experience.
 It's not the highest sort of wisdom yet but it's a
 understanding of things as
 they are this is rising.
 It's putting into practice that the theory that we've
 learned
 testing or thinking about or experimenting on
 that theory.
 [Silence]
 [Silence]
 [Silence]
 [Silence]
 [Silence]
 [Silence]
 [Silence]
 [Silence]
 Now as we start to watch the body as I said there are
 altogether four
 foundations and the reason there are is because
 our mind doesn't stay with the body all the time.
 It wanders it goes here and there and everywhere.
 Another thing about meditation is this sort of meditation
 is according to the
 teaching that nothing is worth clinging to.
 We're also not trying to cling to the meditation object or
 trying to cling to
 the idea that our mind has to be stationary. We're going to
 watch the
 mind as it is and we're going to learn and not cling.
 Because we're going to start to see that clinging is the
 reason our mind is so
 flustered and confused and scattered as it is.
 So when our mind does wander we're going to focus on the
 other
 various objects of its attention. The next one being
 feelings that arise.
 So when we're sitting watching the rising and falling we
 find our mind
 wandering away to certain sensations that arise in the body
 and in the mind. Pleasant sensations, unpleasant sensations
, or neutral calm
 sensations.
 Many times sitting cross-legged in meditation we'll find
 pain arising in the back or in the legs. Sometimes we'll
 find
 pain in the head if we're if we're if we think a lot or
 work
 hard every day.
 So instead of seeing these as a as a distraction or an
 obstacle towards our
 practice we should focus them on them and use
 them as as our meditation practice. They're part of reality
 and they're
 something that it's very common for us to cling to.
 It's generally pretty obvious that we're clinging to them.
 We're saying this is
 bad. I don't want this. I want this to leave
 me to go away.
 So here instead of doing that we're going to say to
 ourselves pain or aching
 or sore or however just reminding ourselves of the of what
 it is.
 We use this mantra. It's going to focus objectively on the
 object.
 Not seeing it as good or bad just seeing it for what it is
 with wisdom.
 So we say to ourselves pain pain pain
 pain until it goes away.
 If we feel happy we do the same. We don't want to cling to
 happiness either.
 There's nothing wrong with happiness. In fact most people
 would agree that
 happiness is a good thing but what's what's really not a
 good thing
 is our clinging to it. We're saying oh I hope this stays
 for a
 long time. I hope this is permanent because it doesn't stay
 for
 a long time. It isn't permanent. It isn't subject to our
 control.
 So we should just know that we're happy and just be happy
 not clinging to it.
 Not judging it. Not giving rise to anything
 beyond just a knowledge that this is happiness.
 Say to ourselves happy happy happy.
 [silence]
 [silence]
 [silence]
 [silence]
 [silence]
 [silence]
 [silence]
 [silence]
 [silence]
 If we feel calm, the same thing. Many people as they
 practice,
 as their practice progresses they'll feel calm. They'll
 feel quiet and they'll find
 that they aren't able to meditate any longer because all
 that's left is a
 sense of quiet and peace.
 But the Buddha was very clever and was very clear in
 pointing out absolutely everything that we were to be
 mindful of that
 that could arise. That includes peaceful feelings and so
 when we feel peaceful we should say to ourselves as well
 calm calm or peaceful peaceful.
 [silence]
 This is the second foundation. The third foundation is the
 foundation of
 of the mind focusing on the mind itself. As we practice we
'll see the next thing
 the next problem is that we're thinking.
 And we say oh this is a big problem because we're no longer
 focusing on our
 object now instead we're thinking. Our mind is wandering
 away from the
 object.
 But the great thing here as I said is that that can be that
 as well can be the
 object. The mind can whatever can be the object
 whatever arises can be the object of our intention.
 So when we think we can say to ourselves thinking thinking
 whatever we're
 thinking about instead of turning it into a huge
 issue a big deal making more of it than just what it is
 we simply see it as thinking thinking thinking.
 And the final one is our emotions. The final one actually
 has many many
 things but I'm just going to simplify it down to our
 emotions.
 It's many of the various other teachings of the Buddha
 which we should
 start to focus in on. Actually the first three are the main
 foundations the fourth
 one is things that we're going to have to focus
 in on. Our practice becomes more focused than
 simply anything. We're going to focus on on the important
 the salient points of our existence. The first one is our
 hindrances the
 things which are going to get in the way of our practice.
 When we like something when we don't like something
 when we feel angry or bored or frustrated or sad or
 depressed.
 All of these negative emotions even including liking or
 enjoy desire or lust or so on we consider them to be
 negative in the sense that they are judgments they're
 clinging.
 And we're not going to judge these things we're just going
 we should
 understand that these are five things which are actually
 going
 to change our state of mind and take us out of the med
itative state.
 If we give power to these emotions then we'll find our
 mindfulness
 dissipating. So it's important that not that we judge
 these not that we feel guilty about them or we say bad bad
 bad
 but that as with everything else we we pick up these as
 well as
 sort of especially pick these up keep them on our radar
 when they arise be very quick to catch them just like
 everything else.
 So when you like something say to yourself liking
 liking or wanting wanting.
 When you don't like something say disliking disliking
 simply
 knowing that now you're disliking we're going to see it for
 what it is if it's a
 good thing to like or dislike something then we'll
 know it we'll have wisdom we'll have understanding.
 If we feel bored if we feel distracted if we feel sad or
 depressed we should
 just say to ourselves sad sad depressed depressed.
 If we've if we feel drowsy or lazy or tired we should
 simply know it for what
 it is just see it for what it is not getting
 upset about it but just saying okay now we have this
 emotion in our minds now
 we have this state of mind we say to ourselves
 drowsy drowsy or tired tired these are things which are
 going to
 get in the way of our practice very clearly and from the
 very very beginning
 so they're important for especially beginner meditators.
 And the final one that the buddha mentioned was doubt
 if you have doubt in your mind's doubt about yourself
 whether you can do this
 practice doubt about the practice itself whether it's right
 then his advice for you is to practice doubt practice
 focusing on the doubt
 practice learning about the doubt itself give up the idea
 that you're
 practicing anything and just look at the doubt and say to
 yourself what is doubt
 make that your practice because once you learn about doubt
 and give up the doubt
 then there's nothing that you need to practice this is
 really
 in one sense this is the teaching of the buddha that you
 come to see you come to give up your doubt you come to
 see everything for what it is and not have anything left to
 doubt
 there's no other practice beside that there's nothing else
 that you have to
 achieve.
 When there's nothing left to be mindful of we just always
 come back to our base
 meditation of being mindful of the rising and the
 falling because it's always there the stomach
 rising and falling rising and falling we don't have to go
 looking for other
 objects when they come up we pay attention to
 them when they're not there we come back to the body
 it can really be any part of the body you can focus on the
 walking when you're
 walking around saying to yourself walking walking or so on
 when you're sitting still it's generally easiest to focus
 on the stomach because
 it's very clear it's always there
 so the practice is another type of wisdom it's it's a
 putting into practice
 it's getting the skills it's an intellectual exercise but
 what
 comes from the practice this is true wisdom this is really
 what we're
 trying to gain we're going to start seeing things about
 ourselves that we didn't see before we're going to learn
 things about
 ourselves that we didn't know before we're going to give up
 things that we
 thought were inherent or intrinsic to who we were
 and we'll give them up because we'll see that they're
 useless they're unhelpful
 they're unbeneficial they're not under our control and they
 have no use whatsoever and we're going to see that we're
 better off without
 them when we see that we're better off without them this is
 true wisdom
 and it's it's a fact it's a fact of life that's simply
 knowing that it's wrong for you when you really know that
 something is
 unhelpful that you do give it up the problem is we
 always say this is bad for me that's bad for me we don't
 really know
 we actually still feel pleasure and attachment to it
 we still think have this idea that it's somehow good for us
 when we see deeply that things are not worth clinging to
 then we actually don't cling to them we do
 follow our our wisdom we do follow our knowledge it's just
 that
 most of our knowledge is only theoretical it's a sort of
 false knowledge
 we say this is good and this is bad but we don't really
 have any understanding
 or wisdom that is able to tell us that this is good
 or this is bad
 so here i've i've given a talk a little bit longer than i
 normally would but
 i sort of intended to do so thinking that it was going to
 also be a meditation
 practice and therefore cut into some of our
 question and answer time but now of course we have time for
 discussion
 questions and answer and probably i'm i'm i'm happy to be
 here
 after one one o'clock if there's still people
 lagging or hanging on so i'll stop there and i'd like to
 thank
 you all for coming if you have any questions i'm happy to
 answer them via text from here on
