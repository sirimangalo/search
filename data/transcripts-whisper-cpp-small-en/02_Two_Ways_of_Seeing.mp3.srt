1
00:00:00,000 --> 00:00:05,600
 two ways of seeing. Our practice is called Vipassana Bhav

2
00:00:05,600 --> 00:00:08,600
ana or Vipassana meditation.

3
00:00:08,600 --> 00:00:13,060
 And the goal is to attain Vipassana. Vipassana means seeing

4
00:00:13,060 --> 00:00:15,220
 and 'vi' implies that it is

5
00:00:15,220 --> 00:00:19,670
 in a special or exceptional way. The word Vipassana implies

6
00:00:19,670 --> 00:00:21,000
 that there are two ways

7
00:00:21,000 --> 00:00:25,720
 of seeing and that one of the ways is superior to the other

8
00:00:25,720 --> 00:00:27,800
. Vipassana means seeing the three

9
00:00:27,800 --> 00:00:34,810
 characteristics impermanence, suffering and non-self, anika

10
00:00:34,810 --> 00:00:37,360
, dukha and anatta. The other

11
00:00:37,360 --> 00:00:41,250
 way of seeing is the opposite. It is the ordinary way of

12
00:00:41,250 --> 00:00:45,120
 seeing nika, stability or permanency,

13
00:00:45,120 --> 00:00:52,160
 sukha, happiness or satisfaction and ata, self. Our

14
00:00:52,160 --> 00:00:54,360
 ordinary way of seeing things without

15
00:00:54,360 --> 00:00:58,890
 Vipassana gives primacy to things. We could say both and

16
00:00:58,890 --> 00:01:01,080
 both concepts but we are more

17
00:01:01,080 --> 00:01:05,000
 familiar with things because we do not realize that things

18
00:01:05,000 --> 00:01:08,040
 are just concepts. People, places,

19
00:01:08,040 --> 00:01:12,160
 things, possessions, entities. The way we look at reality

20
00:01:12,160 --> 00:01:14,840
 gives primacy to these concepts

21
00:01:14,840 --> 00:01:20,300
 as impersonal, unshakable, unchanging things. Taking our

22
00:01:20,300 --> 00:01:22,000
 attention away from the truth,

23
00:01:22,000 --> 00:01:25,940
 the reality of experience. We do not trust our senses

24
00:01:25,940 --> 00:01:27,960
 because the impression we get from

25
00:01:27,960 --> 00:01:31,330
 them might be wrong. Rather than not trust our senses

26
00:01:31,330 --> 00:01:33,640
 themselves, however, we should

27
00:01:33,640 --> 00:01:37,200
 simply not trust the impressions or conclusions we draw

28
00:01:37,200 --> 00:01:39,240
 from our senses. This is an important

29
00:01:39,240 --> 00:01:43,760
 distinction. We normally mistrust our senses entirely. Let

30
00:01:43,760 --> 00:01:45,000
ting science tell us what is

31
00:01:45,000 --> 00:01:48,820
 reality. Science has wonderful ways of explaining the

32
00:01:48,820 --> 00:01:52,280
 reality of entities to us, molecules, atoms,

33
00:01:52,280 --> 00:01:56,490
 subatomic particles, etc. We think that things exist and

34
00:01:56,490 --> 00:01:58,920
 more practically we think that things

35
00:01:58,920 --> 00:02:04,460
 that we can see, people, places, objects and possessions

36
00:02:04,460 --> 00:02:07,680
 exist. Direct experience is relegated

37
00:02:07,680 --> 00:02:12,340
 to a kind of epiphenomenon. Like a bioproduct of things, it

38
00:02:12,340 --> 00:02:13,960
's no wonder that we have this

39
00:02:13,960 --> 00:02:18,010
 false sense of stability, satisfaction and control, a false

40
00:02:18,010 --> 00:02:19,800
 sense of existence. There

41
00:02:19,800 --> 00:02:25,770
 being things for ourselves, me, mine, you, him and her,

42
00:02:25,770 --> 00:02:28,320
 things. The ordinary way of looking

43
00:02:28,320 --> 00:02:32,690
 at reality is problematic because whether or not we trust

44
00:02:32,690 --> 00:02:35,280
 our senses, they take precedence

45
00:02:35,280 --> 00:02:39,620
 over things, atoms or subatomic particles as things have no

46
00:02:39,620 --> 00:02:41,400
 bearing on our stress or

47
00:02:41,400 --> 00:02:46,220
 happiness or everything that is truly important. It is not

48
00:02:46,220 --> 00:02:49,680
 important which paradigm is "correct".

49
00:02:49,680 --> 00:02:54,130
 It is important that our ordinary way of seeing is not

50
00:02:54,130 --> 00:02:56,600
 helpful. Harmful even as it relegates

51
00:02:56,600 --> 00:03:00,560
 our experience to an inferior position that we disregard

52
00:03:00,560 --> 00:03:03,440
 because of how valuable our impressions

53
00:03:03,440 --> 00:03:08,830
 of experience can be. In doing so, we ignore what is most

54
00:03:08,830 --> 00:03:11,520
 basic to our existence. The other

55
00:03:11,520 --> 00:03:15,110
 way of looking at reality is to see beyond the conceptual

56
00:03:15,110 --> 00:03:17,720
 realm of things. Reminding ourselves

57
00:03:17,720 --> 00:03:21,890
 that things do not exist. Take the body for example, does

58
00:03:21,890 --> 00:03:24,040
 the body exist? We know from

59
00:03:24,040 --> 00:03:27,180
 science that the body is made up of cells. Does the body

60
00:03:27,180 --> 00:03:28,840
 exist in the same way as cells

61
00:03:28,840 --> 00:03:33,600
 do? No matter how big or how small things are or how finely

62
00:03:33,600 --> 00:03:36,320
 they are divided into parts,

63
00:03:36,320 --> 00:03:40,930
 their existence never reaches the level of reality of

64
00:03:40,930 --> 00:03:41,760
 experiences. Everything that we

65
00:03:41,760 --> 00:03:45,870
 know relating to things is based on experience. Without

66
00:03:45,870 --> 00:03:48,080
 experience, nothing else is truly

67
00:03:48,080 --> 00:03:52,250
 meaningful. Experience is always primary. Things are

68
00:03:52,250 --> 00:03:55,600
 secondary and depend on our experience.

69
00:03:55,600 --> 00:03:59,170
 There is subjectivity with things because two people can

70
00:03:59,170 --> 00:04:00,560
 look at the same thing and

71
00:04:00,560 --> 00:04:06,360
 have very different ideas about it. Whether it is good, bad

72
00:04:06,360 --> 00:04:09,160
, beautiful, ugly, etc. Experiences

73
00:04:09,160 --> 00:04:13,260
 are objective as they maintain their nature no matter what

74
00:04:13,260 --> 00:04:16,080
 we perceive them to be. Experiential

75
00:04:16,080 --> 00:04:20,440
 reality is also more useful because all of the problems in

76
00:04:20,440 --> 00:04:23,160
 life like depression, anxiety,

77
00:04:23,160 --> 00:04:27,480
 addiction, etc. are not a part of things. They are part of

78
00:04:27,480 --> 00:04:30,280
 experiences. Putting experiences

79
00:04:30,280 --> 00:04:35,020
 first changes our entire outlook on life. That is what Vibh

80
00:04:35,020 --> 00:04:37,360
asana means. Gaining a new

81
00:04:37,360 --> 00:04:41,500
 perspective by changing what we put as primary in our

82
00:04:41,500 --> 00:04:44,260
 outlook. We still acknowledge the conceptual

83
00:04:44,260 --> 00:04:48,720
 existence of people, places and things as they are quite

84
00:04:48,720 --> 00:04:51,440
 necessary for practical life.

85
00:04:51,440 --> 00:04:54,990
 At the same time, we become in touch with the reality of

86
00:04:54,990 --> 00:04:57,040
 our experiences to the extent

87
00:04:57,040 --> 00:05:01,180
 that we are no longer distracted by conceptual thought. As

88
00:05:01,180 --> 00:05:03,320
 a result, we are able to see through

89
00:05:03,320 --> 00:05:07,500
 our conceptual misunderstandings about reality, especially

90
00:05:07,500 --> 00:05:11,020
 regarding the three characteristics.

91
00:05:11,020 --> 00:05:14,370
 The three characteristics are quite difficult to understand

92
00:05:14,370 --> 00:05:16,480
 when we think in terms of things.

93
00:05:16,480 --> 00:05:20,290
 What does it mean to say that something is unstable? The

94
00:05:20,290 --> 00:05:22,240
 very nature of things is that

95
00:05:22,240 --> 00:05:26,960
 they are stable, able to satisfy and subject to our control

96
00:05:26,960 --> 00:05:28,640
. Unfortunately, because things

97
00:05:28,640 --> 00:05:32,140
 are conceptual and experiences are real, we are never

98
00:05:32,140 --> 00:05:34,400
 actually able to find stability,

99
00:05:34,400 --> 00:05:38,940
 satisfaction and control. Experiences are not stable. They

100
00:05:38,940 --> 00:05:41,400
 arise and cease quite rapidly.

101
00:05:41,400 --> 00:05:44,660
 As a result of this, they are unable to be a source of

102
00:05:44,660 --> 00:05:46,920
 satisfaction in the way we perceive

103
00:05:46,920 --> 00:05:51,160
 things to be. Likewise, we cannot say they have selfness or

104
00:05:51,160 --> 00:05:53,160
 substance to them, nor can

105
00:05:53,160 --> 00:05:57,290
 they be taken as possessions or part of another's self.

106
00:05:57,290 --> 00:06:00,680
 They are simply experiences. When we

107
00:06:00,680 --> 00:06:05,230
 see reality as experiences, we lack of addiction because we

108
00:06:05,230 --> 00:06:07,640
 no longer conceive of things that

109
00:06:07,640 --> 00:06:11,660
 might satisfy us. We stop seeing in terms of things which

110
00:06:11,660 --> 00:06:13,800
 would have the potential in our

111
00:06:13,800 --> 00:06:17,290
 minds to satisfy us if it were only possible to experience

112
00:06:17,290 --> 00:06:20,040
 them instead of momentary experiences.

113
00:06:20,040 --> 00:06:24,250
 Things are not what we experience. We experience experience

114
00:06:24,250 --> 00:06:26,760
. We experience seeing, hearing,

115
00:06:26,760 --> 00:06:31,020
 smelling, tasting, feeling, thinking because experiences

116
00:06:31,020 --> 00:06:33,160
 are required for enjoying things

117
00:06:33,160 --> 00:06:37,740
 and experiences are unstable. There is really nothing that

118
00:06:37,740 --> 00:06:40,360
 can truly satisfy us. Worse,

119
00:06:40,360 --> 00:06:44,590
 experiences are not under our control. They do not belong

120
00:06:44,590 --> 00:06:47,480
 to us. Thus, clinging to anything becomes

121
00:06:47,480 --> 00:06:50,750
 futile and of course for great disappointment and

122
00:06:50,750 --> 00:06:53,480
 dissatisfaction. This is the simple truth

123
00:06:53,480 --> 00:06:56,640
 about the nature of reality. All questions about self and

124
00:06:56,640 --> 00:07:00,200
 non-self, does the self exist, etc.,

125
00:07:00,200 --> 00:07:04,030
 are totally off-base questions because the questions are

126
00:07:04,030 --> 00:07:06,120
 based on the perception of things.

127
00:07:06,120 --> 00:07:10,650
 Non-self simply means that experiences do not belong to you

128
00:07:10,650 --> 00:07:13,640
. They are not you. They are not

129
00:07:13,640 --> 00:07:18,750
 are you. They are not a thing, an entity. They are

130
00:07:18,750 --> 00:07:21,480
 experiences that arise and cease.

131
00:07:21,480 --> 00:07:25,000
 This understanding is not about realizing that the things

132
00:07:25,000 --> 00:07:26,760
 you thought were permanent or

133
00:07:26,760 --> 00:07:30,220
 impermanent. It is true that this is one way of explaining

134
00:07:30,220 --> 00:07:32,600
 impermanence in a very superficial way

135
00:07:32,600 --> 00:07:35,590
 but true understanding is much deeper than that or at least

136
00:07:35,590 --> 00:07:37,160
 very different from that.

137
00:07:37,160 --> 00:07:41,130
 The three characteristics mean that you change the way you

138
00:07:41,130 --> 00:07:43,640
 look at the reality from things

139
00:07:43,640 --> 00:07:48,040
 that are stable, satisfying and controllable. Which people,

140
00:07:48,040 --> 00:07:50,520
 places and things are to sing

141
00:07:50,520 --> 00:07:55,090
 in terms of experiences which are impermanent, unsatisfying

142
00:07:55,090 --> 00:07:58,120
 and uncontrollable. This may sound

143
00:07:58,120 --> 00:08:02,100
 like a bad deal. You may ask why you would want to trade in

144
00:08:02,100 --> 00:08:04,360
 a perception of reality based on

145
00:08:04,360 --> 00:08:07,690
 stability, satisfaction and control for one based on the

146
00:08:07,690 --> 00:08:11,080
 opposite. Indeed, perceiving things is good

147
00:08:11,080 --> 00:08:13,890
 in a worldly sense. As a result of our conventional

148
00:08:13,890 --> 00:08:16,920
 framework of reality, we can build nuclear reactors,

149
00:08:16,920 --> 00:08:20,800
 space stations and computers. However, this has no bearing

150
00:08:20,800 --> 00:08:23,080
 on our happiness or suffering because

151
00:08:23,080 --> 00:08:27,190
 again in terms of what is good for us, experience is

152
00:08:27,190 --> 00:08:31,000
 primary. If you live in the world of concepts

153
00:08:31,000 --> 00:08:35,060
 or things, you are going to get bored because you are

154
00:08:35,060 --> 00:08:38,120
 ignoring the underlying reality of impermanence,

155
00:08:38,120 --> 00:08:42,620
 suffering and non-self. If instead you see through the lens

156
00:08:42,620 --> 00:08:45,480
 of experience, you are invulnerable to

157
00:08:45,480 --> 00:08:49,160
 disappointment or upset as you are truly living in reality,

158
00:08:49,160 --> 00:08:51,160
 keeping up with reality instead of

159
00:08:51,160 --> 00:08:55,010
 fighting or living in fear of it. Suddenly you fit with the

160
00:08:55,010 --> 00:08:58,520
 world, with reality. You become natural.

161
00:08:58,520 --> 00:09:02,630
 You become in tune with nature. There is no dissonance

162
00:09:02,630 --> 00:09:05,480
 between you and reality. No worry

163
00:09:05,480 --> 00:09:10,020
 about the future or sadness about the past. Both are all

164
00:09:10,020 --> 00:09:12,840
 caught up in concepts, things and self.

165
00:09:13,880 --> 00:09:18,640
 Experience is always only present here and now, being born

166
00:09:18,640 --> 00:09:20,360
 and dying every moment.

167
00:09:20,360 --> 00:09:24,930
 The practice of mindfulness based on the four foundations

168
00:09:24,930 --> 00:09:27,400
 of mindfulness is meant to help us

169
00:09:27,400 --> 00:09:31,550
 see reality from the point of view of experience as follows

170
00:09:31,550 --> 00:09:35,080
. Number one, physical experience.

171
00:09:35,080 --> 00:09:39,620
 When you walk, stepping right, there is an experience there

172
00:09:39,620 --> 00:09:42,360
. There are many experiences

173
00:09:42,360 --> 00:09:45,920
 in walking. The right foot is one experience and the left

174
00:09:45,920 --> 00:09:47,800
 foot is another experience.

175
00:09:47,800 --> 00:09:52,760
 When you sit and see rising, falling, those are each

176
00:09:52,760 --> 00:09:54,600
 physical experiences.

177
00:09:54,600 --> 00:10:01,570
 Two, feelings. When you feel pain and you focus on the pain

178
00:10:01,570 --> 00:10:04,920
 and say pain, pain, for as long as it

179
00:10:04,920 --> 00:10:09,850
 lasts, you see it as experience, many moments of experience

180
00:10:09,850 --> 00:10:13,480
. Three, thoughts, thinking about past

181
00:10:13,480 --> 00:10:18,630
 or future events. You see thoughts not as my past or my

182
00:10:18,630 --> 00:10:22,440
 future, but rather as experiences of thinking.

183
00:10:22,440 --> 00:10:26,380
 Likewise, good thoughts and bad thoughts become merely

184
00:10:26,380 --> 00:10:28,520
 thoughts as you let go of any conceiving

185
00:10:28,520 --> 00:10:32,730
 about the thoughts. Four, mind states. When you like

186
00:10:32,730 --> 00:10:36,440
 something, dislike something or experience

187
00:10:36,440 --> 00:10:41,130
 other mind states like drowsiness, destruction, worry,

188
00:10:41,130 --> 00:10:44,760
 anxiety, doubt, etc. Instead of being

189
00:10:44,760 --> 00:10:47,960
 tortured or controlled by them and having to take drugs or

190
00:10:47,960 --> 00:10:49,720
 medication to alleviate them,

191
00:10:49,720 --> 00:10:54,240
 you see them just as they are. As experiences, we take

192
00:10:54,240 --> 00:10:57,720
 drugs and medication because we perceive our

193
00:10:57,720 --> 00:11:01,320
 mind states as problems. When our mind states are extreme,

194
00:11:01,320 --> 00:11:04,200
 we take medications as means of fixing

195
00:11:04,200 --> 00:11:08,390
 the perceived problem. If your CEO's mind states just as

196
00:11:08,390 --> 00:11:11,160
 part of your experience, it doesn't matter

197
00:11:11,160 --> 00:11:15,120
 how intense or overwhelming they are, they just are. They

198
00:11:15,120 --> 00:11:17,640
 are experiences. We are so accustomed

199
00:11:17,640 --> 00:11:21,410
 to reacting and thinking of emotions as my emotions, as me

200
00:11:21,410 --> 00:11:23,000
 experiencing the emotions,

201
00:11:23,640 --> 00:11:27,040
 and even as the emotions being things are a part of

202
00:11:27,040 --> 00:11:30,200
 ourselves. Like people say, I have depression,

203
00:11:30,200 --> 00:11:35,950
 I am depressed or I am clinically depressed. Depression is

204
00:11:35,950 --> 00:11:37,800
 an emotion that arises for a

205
00:11:37,800 --> 00:11:41,560
 moment and then is gone. Because it comes and goes

206
00:11:41,560 --> 00:11:45,320
 repeatedly, we perceive it to be a lasting entity,

207
00:11:45,320 --> 00:11:49,560
 a thing. It is only when we lose sight of the moment

208
00:11:49,560 --> 00:11:52,280
 reality of experiences that we suffer

209
00:11:52,280 --> 00:11:55,780
 from things like depression. The same goes for most mental

210
00:11:55,780 --> 00:11:59,480
 illnesses. Five. The senses.

211
00:11:59,480 --> 00:12:04,900
 Seeing, hearing, smelling, tasting, feeling, thinking all

212
00:12:04,900 --> 00:12:07,400
 have great power to torture us,

213
00:12:07,400 --> 00:12:11,000
 which is kind of silly because seeing is really just seeing

214
00:12:11,000 --> 00:12:13,960
. How is it that when you see a spider,

215
00:12:13,960 --> 00:12:17,880
 you freak out or when you see something ugly, you become

216
00:12:17,880 --> 00:12:20,760
 nauseous. Whatever you see,

217
00:12:20,760 --> 00:12:23,840
 it is merely an experience of seeing. We are conditioned to

218
00:12:23,840 --> 00:12:25,560
 react to our experiences or

219
00:12:25,560 --> 00:12:28,460
 rather we condition ourselves and are encouraged by

220
00:12:28,460 --> 00:12:31,160
 external influences to become conditioned.

221
00:12:31,160 --> 00:12:34,900
 Mindfulness is about becoming unconditioned, objective,

222
00:12:34,900 --> 00:12:36,440
 seeing things as they are,

223
00:12:36,440 --> 00:12:40,560
 and becoming in tune with reality. Seeing clearly, vipass

224
00:12:40,560 --> 00:12:43,320
ana and mindfulness, sati, are the two

225
00:12:43,320 --> 00:12:46,640
 most important parts of our practice. We practice

226
00:12:46,640 --> 00:12:50,520
 mindfulness to see clearly. When we see reality

227
00:12:50,520 --> 00:12:54,020
 clearly as it is, we free ourselves from all the suffering

228
00:12:54,020 --> 00:12:55,880
 of our ignorance and delusion,

229
00:12:55,880 --> 00:12:59,230
 about the nature of our minds and the experiential world

230
00:12:59,230 --> 00:13:09,960
 around us.

