1
00:00:00,000 --> 00:00:08,520
 Speaking about the reality as it is, how is it you think

2
00:00:08,520 --> 00:00:10,840
 about something or you experience

3
00:00:10,840 --> 00:00:15,080
 something in your life and you make a thought about it, I

4
00:00:15,080 --> 00:00:17,200
 mean you come to the point that

5
00:00:17,200 --> 00:00:21,680
 you create a thought in your mind which you don't know if

6
00:00:21,680 --> 00:00:24,040
 it's real or it's not real,

7
00:00:24,040 --> 00:00:28,440
 that you don't really, you can't find the reality.

8
00:00:28,440 --> 00:00:31,520
 Well, reality is what it is.

9
00:00:31,520 --> 00:00:35,400
 If it's arisen in your mind then from my point of view it's

10
00:00:35,400 --> 00:00:36,840
 real and I'm pretty sure that

11
00:00:36,840 --> 00:00:41,360
 the Buddha, that's what the Buddha taught.

12
00:00:41,360 --> 00:00:45,290
 There's no, you know, think about what's going on in your

13
00:00:45,290 --> 00:00:47,560
 mind when you ask this question,

14
00:00:47,560 --> 00:00:50,800
 when you grapple with the idea of reality.

15
00:00:50,800 --> 00:00:58,300
 There's no definition, there's no train of thought that

16
00:00:58,300 --> 00:01:02,160
 could arise to give you the definition

17
00:01:02,160 --> 00:01:04,560
 of reality.

18
00:01:04,560 --> 00:01:09,160
 It makes no sense to even hypothesize about what is and

19
00:01:09,160 --> 00:01:11,940
 what is not real apart from what

20
00:01:11,940 --> 00:01:14,140
 has arisen.

21
00:01:14,140 --> 00:01:16,720
 So if it has arisen, it is real.

22
00:01:16,720 --> 00:01:20,510
 So if a thought arises in your mind then to the extent that

23
00:01:20,510 --> 00:01:22,480
 it has arisen, then to that

24
00:01:22,480 --> 00:01:23,480
 extent it's real.

25
00:01:23,480 --> 00:01:30,190
 Other than that it really, I think it's sophism, it's

26
00:01:30,190 --> 00:01:34,440
 irrational to talk about this being

27
00:01:34,440 --> 00:01:35,440
 real or that being real.

28
00:01:35,440 --> 00:01:38,960
 Either that or it's just using logic and reason like how

29
00:01:38,960 --> 00:01:41,360
 materialists point out that you can

30
00:01:41,360 --> 00:01:44,880
 see things so therefore they must exist and of course

31
00:01:44,880 --> 00:01:47,320
 quantum physics and modern physics

32
00:01:47,320 --> 00:01:50,640
 has blown that out of the water.

33
00:01:50,640 --> 00:01:51,640
 Some theories have.

34
00:01:51,640 --> 00:01:58,150
 But the experiences are made by mind, produced by mind and

35
00:01:58,150 --> 00:02:01,840
 left by mind, produced by mind

36
00:02:01,840 --> 00:02:04,640
 and perceived by mind.

37
00:02:04,640 --> 00:02:05,640
 Right.

38
00:02:05,640 --> 00:02:12,350
 Okay, so if you imagine something and you really believe in

39
00:02:12,350 --> 00:02:15,640
 it, that becomes something

40
00:02:15,640 --> 00:02:17,760
 that actually happens.

41
00:02:17,760 --> 00:02:21,710
 Or maybe in the sense that you're thinking it just means

42
00:02:21,710 --> 00:02:24,200
 that they come in that sequence.

43
00:02:24,200 --> 00:02:27,830
 Doesn't make them any less real just because the mind was

44
00:02:27,830 --> 00:02:30,160
 the instigator or the mind allowed

45
00:02:30,160 --> 00:02:37,160
 you to allow the experience to arise.

