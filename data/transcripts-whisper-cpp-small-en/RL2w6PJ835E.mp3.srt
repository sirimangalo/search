1
00:00:00,000 --> 00:00:04,000
 Lucas will help me out.

2
00:00:04,000 --> 00:00:11,000
 Many meditation practices focus on peace and happiness.

3
00:00:11,000 --> 00:00:15,250
 But I am interested in it to understand my existence, if

4
00:00:15,250 --> 00:00:16,000
 there is any.

5
00:00:16,000 --> 00:00:21,000
 Enlightenment the rest is a byproduct in my view.

6
00:00:21,000 --> 00:00:26,000
 What do you think suggests?

7
00:00:26,000 --> 00:00:32,000
 Who wants to start?

8
00:00:32,000 --> 00:00:38,000
 I will try to understand this question.

9
00:00:38,000 --> 00:00:41,000
 Okay, let me start.

10
00:00:41,000 --> 00:00:45,000
 You look at it for a while, I will start.

11
00:00:45,000 --> 00:00:48,940
 This is something I think Clemente brought up yesterday

12
00:00:48,940 --> 00:00:53,000
 about something quite similar actually.

13
00:00:53,000 --> 00:01:03,000
 The most important point of view was knowledge.

14
00:01:03,000 --> 00:01:06,520
 That is basically what he is saying. I am interested to

15
00:01:06,520 --> 00:01:09,000
 understand my existence.

16
00:01:09,000 --> 00:01:12,000
 I would say it can go two ways here.

17
00:01:12,000 --> 00:01:16,130
 On the one hand, what you are saying could be very much the

18
00:01:16,130 --> 00:01:19,000
 attainment of peace, happiness and freedom from suffering.

19
00:01:19,000 --> 00:01:25,820
 It gives you a certain contentment, I think contentment is

20
00:01:25,820 --> 00:01:28,000
 a very good word,

21
00:01:28,000 --> 00:01:31,000
 to understand your existence.

22
00:01:31,000 --> 00:01:38,160
 Because if you actually do understand reality that brings

23
00:01:38,160 --> 00:01:43,000
 and is brought about, I would argue,

24
00:01:43,000 --> 00:01:51,330
 by a desire for peace, for happiness, for freedom from

25
00:01:51,330 --> 00:01:54,000
 suffering.

26
00:01:54,000 --> 00:01:57,460
 You can't want something without thinking that it is going

27
00:01:57,460 --> 00:02:01,090
 to bring you some form of what we call happiness or

28
00:02:01,090 --> 00:02:02,000
 something positive.

29
00:02:02,000 --> 00:02:07,000
 If you think of knowledge or understanding as positive,

30
00:02:07,000 --> 00:02:12,300
 then you are already defining it as bringing something that

31
00:02:12,300 --> 00:02:16,210
 must be of the sort of being peace, happiness and freedom

32
00:02:16,210 --> 00:02:17,000
 from suffering.

33
00:02:17,000 --> 00:02:25,480
 If understanding brought you suffering or brought you what

34
00:02:25,480 --> 00:02:30,000
 was undesirable, would you go for it?

35
00:02:30,000 --> 00:02:33,460
 I mean intrinsically, I don't mean that, well, maybe it

36
00:02:33,460 --> 00:02:36,000
 hurts for a while, but in the end it is all for the best.

37
00:02:36,000 --> 00:02:38,000
 If it is all for the best, then it brings happiness.

38
00:02:38,000 --> 00:02:40,600
 That is what is great about understanding, is that

39
00:02:40,600 --> 00:02:43,980
 intrinsically it can't help but bring you peace, happiness

40
00:02:43,980 --> 00:02:45,000
 and freedom from suffering.

41
00:02:45,000 --> 00:02:47,000
 I don't think that is much different.

42
00:02:47,000 --> 00:02:50,710
 The other thing I wanted to say before I say anything

43
00:02:50,710 --> 00:02:53,000
 before anyone else gets a chance,

44
00:02:53,000 --> 00:02:56,660
 is that you have to be careful because what do you mean by

45
00:02:56,660 --> 00:02:59,000
 understanding your existence?

46
00:02:59,000 --> 00:03:04,030
 Existence could be infinite and it can be very easy in that

47
00:03:04,030 --> 00:03:08,320
 sense to get caught up and get hung up on knowledge and

48
00:03:08,320 --> 00:03:10,000
 understanding.

49
00:03:10,000 --> 00:03:12,460
 How much understanding do you want and what sort of

50
00:03:12,460 --> 00:03:14,000
 understanding do you want?

51
00:03:14,000 --> 00:03:16,590
 If it is beyond what actually brings you peace, happiness

52
00:03:16,590 --> 00:03:20,000
 and freedom from suffering, what good is it?

53
00:03:20,000 --> 00:03:26,000
 It can be addictive and it can lead you to want more.

54
00:03:26,000 --> 00:03:29,000
 Anyway, anyone else? Go for it.

55
00:03:29,000 --> 00:03:31,000
 Yeah, I just want to really quickly.

56
00:03:31,000 --> 00:03:35,000
 That person seems to ask himself a question.

57
00:03:35,000 --> 00:03:39,000
 Who am I from my existence?

58
00:03:39,000 --> 00:03:44,000
 What I am doing here, what is my destiny?

59
00:03:44,000 --> 00:03:47,000
 Who am I supposed to be like?

60
00:03:47,000 --> 00:03:49,000
 So on, so on.

61
00:03:49,000 --> 00:03:54,450
 It says like, many meditation practices focus on peace and

62
00:03:54,450 --> 00:03:55,000
 happiness.

63
00:03:55,000 --> 00:04:01,450
 How about the passing of meditation when you just observe

64
00:04:01,450 --> 00:04:06,000
 rising and falling of all phenomena.

65
00:04:06,000 --> 00:04:12,000
 There is no, I don't understand my existence.

66
00:04:12,000 --> 00:04:15,000
 He says it's very, there is not such a thing.

67
00:04:15,000 --> 00:04:17,000
 You can't happen.

68
00:04:17,000 --> 00:04:23,640
 From my own experience, I don't try to understand myself as

69
00:04:23,640 --> 00:04:26,000
 a person anymore.

70
00:04:26,000 --> 00:04:32,000
 The person is not, there is no such thing as you really

71
00:04:32,000 --> 00:04:37,000
 because you don't own your personality.

72
00:04:37,000 --> 00:04:40,000
 Your parents gave you your personality.

73
00:04:40,000 --> 00:04:46,370
 If you were born in Poland, you will be probably Roman

74
00:04:46,370 --> 00:04:48,000
 Catholic.

75
00:04:48,000 --> 00:04:50,000
 If you were born...

76
00:04:52,000 --> 00:04:54,000
 ...you will be a Christian.

77
00:04:54,000 --> 00:04:56,000
 If you were born in Poland, you will be a Christian.

78
00:04:56,000 --> 00:04:58,000
 If you were born in Poland, you will be a Christian.

79
00:04:58,000 --> 00:05:00,000
 If you were born in Poland, you will be a Christian.

80
00:05:00,000 --> 00:05:02,000
 If you were born in Poland, you will be a Christian.

81
00:05:02,000 --> 00:05:04,000
 If you were born in Poland, you will be a Christian.

82
00:05:04,000 --> 00:05:06,000
 If you were born in Poland, you will be a Christian.

83
00:05:06,000 --> 00:05:08,000
 If you were born in Poland, you will be a Christian.

84
00:05:08,000 --> 00:05:10,000
 If you were born in Poland, you will be a Christian.

85
00:05:10,000 --> 00:05:12,000
 If you were born in Poland, you will be a Christian.

86
00:05:12,000 --> 00:05:14,000
 If you were born in Poland, you will be a Christian.

87
00:05:14,000 --> 00:05:16,000
 If you were born in Poland, you will be a Christian.

88
00:05:16,000 --> 00:05:18,000
 If you were born in Poland, you will be a Christian.

89
00:05:18,000 --> 00:05:20,000
 If you were born in Poland, you will be a Christian.

90
00:05:20,000 --> 00:05:22,000
 If you were born in Poland, you will be a Christian.

91
00:05:22,000 --> 00:05:24,000
 If you were born in Poland, you will be a Christian.

92
00:05:24,000 --> 00:05:26,000
 If you were born in Poland, you will be a Christian.

93
00:05:26,000 --> 00:05:28,000
 If you were born in Poland, you will be a Christian.

94
00:05:28,000 --> 00:05:30,000
 If you were born in Poland, you will be a Christian.

95
00:05:30,000 --> 00:05:32,000
 If you were born in Poland, you will be a Christian.

96
00:05:32,000 --> 00:05:34,000
 If you were born in Poland, you will be a Christian.

97
00:05:34,000 --> 00:05:36,000
 If you were born in Poland, you will be a Christian.

98
00:05:36,000 --> 00:05:38,000
 If you were born in Poland, you will be a Christian.

99
00:05:38,000 --> 00:05:40,000
 If you were born in Poland, you will be a Christian.

100
00:05:40,000 --> 00:05:42,000
 If you were born in Poland, you will be a Christian.

101
00:05:42,000 --> 00:05:44,000
 If you were born in Poland, you will be a Christian.

102
00:05:44,000 --> 00:05:46,000
 If you were born in Poland, you will be a Christian.

103
00:05:46,000 --> 00:05:48,000
 If you were born in Poland, you will be a Christian.

104
00:05:48,000 --> 00:05:50,000
 If you were born in Poland, you will be a Christian.

105
00:05:50,000 --> 00:05:52,000
 If you were born in Poland, you will be a Christian.

106
00:05:52,000 --> 00:05:54,000
 If you were born in Poland, you will be a Christian.

107
00:05:54,000 --> 00:05:56,000
 If you were born in Poland, you will be a Christian.

108
00:05:56,000 --> 00:05:58,000
 If you were born in Poland, you will be a Christian.

109
00:05:58,000 --> 00:06:00,000
 If you were born in Poland, you will be a Christian.

110
00:06:00,000 --> 00:06:02,000
 If you were born in Poland, you will be a Christian.

111
00:06:02,000 --> 00:06:04,000
 If you were born in Poland, you will be a Christian.

112
00:06:04,000 --> 00:06:06,000
 If you were born in Poland, you will be a Christian.

113
00:06:06,000 --> 00:06:08,000
 If you were born in Poland, you will be a Christian.

114
00:06:08,000 --> 00:06:10,000
 If you were born in Poland, you will be a Christian.

115
00:06:10,000 --> 00:06:12,000
 If you were born in Poland, you will be a Christian.

116
00:06:12,000 --> 00:06:14,000
 If you were born in Poland, you will be a Christian.

117
00:06:14,000 --> 00:06:16,000
 If you were born in Poland, you will be a Christian.

118
00:06:16,000 --> 00:06:18,000
 If you were born in Poland, you will be a Christian.

119
00:06:18,000 --> 00:06:20,000
 If you were born in Poland, you will be a Christian.

120
00:06:20,000 --> 00:06:22,000
 If you were born in Poland, you will be a Christian.

121
00:06:22,000 --> 00:06:24,000
 If you were born in Poland, you will be a Christian.

122
00:06:24,000 --> 00:06:26,000
 If you were born in Poland, you will be a Christian.

123
00:06:26,000 --> 00:06:28,000
 If you were born in Poland, you will be a Christian.

124
00:06:28,000 --> 00:06:30,000
 If you were born in Poland, you will be a Christian.

125
00:06:30,000 --> 00:06:32,000
 If you were born in Poland, you will be a Christian.

126
00:06:32,000 --> 00:06:34,000
 If you were born in Poland, you will be a Christian.

127
00:06:34,000 --> 00:06:36,000
 If you were born in Poland, you will be a Christian.

128
00:06:36,000 --> 00:06:38,000
 If you were born in Poland, you will be a Christian.

129
00:06:38,000 --> 00:06:40,000
 If you were born in Poland, you will be a Christian.

130
00:06:40,000 --> 00:06:42,000
 If you were born in Poland, you will be a Christian.

131
00:06:42,000 --> 00:06:44,000
 If you were born in Poland, you will be a Christian.

132
00:06:44,000 --> 00:06:46,000
 If you were born in Poland, you will be a Christian.

133
00:06:46,000 --> 00:06:48,000
 If you were born in Poland, you will be a Christian.

134
00:06:48,000 --> 00:06:50,000
 If you were born in Poland, you will be a Christian.

135
00:06:50,000 --> 00:06:52,000
 If you were born in Poland, you will be a Christian.

136
00:06:52,000 --> 00:06:54,000
 If you were born in Poland, you will be a Christian.

137
00:06:54,000 --> 00:06:56,000
 If you were born in Poland, you will be a Christian.

138
00:06:56,000 --> 00:06:58,000
 If you were born in Poland, you will be a Christian.

139
00:06:58,000 --> 00:07:00,000
 If you were born in Poland, you will be a Christian.

140
00:07:00,000 --> 00:07:02,000
 If you were born in Poland, you will be a Christian.

141
00:07:02,000 --> 00:07:04,000
 If you were born in Poland, you will be a Christian.

142
00:07:04,000 --> 00:07:06,000
 If you were born in Poland, you will be a Christian.

143
00:07:06,000 --> 00:07:08,000
 If you were born in Poland, you will be a Christian.

144
00:07:08,000 --> 00:07:10,000
 If you were born in Poland, you will be a Christian.

145
00:07:10,000 --> 00:07:12,000
 If you were born in Poland, you will be a Christian.

146
00:07:12,000 --> 00:07:14,000
 If you were born in Poland, you will be a Christian.

147
00:07:14,000 --> 00:07:16,000
 If you were born in Poland, you will be a Christian.

148
00:07:16,000 --> 00:07:18,000
 If you were born in Poland, you will be a Christian.

149
00:07:18,000 --> 00:07:20,000
 If you were born in Poland, you will be a Christian.

150
00:07:20,000 --> 00:07:22,000
 If you were born in Poland, you will be a Christian.

151
00:07:22,000 --> 00:07:24,000
 If you were born in Poland, you will be a Christian.

152
00:07:24,000 --> 00:07:26,000
 If you were born in Poland, you will be a Christian.

153
00:07:26,000 --> 00:07:28,000
 If you were born in Poland, you will be a Christian.

154
00:07:28,000 --> 00:07:30,000
 If you were born in Poland, you will be a Christian.

155
00:07:30,000 --> 00:07:32,000
 If you were born in Poland, you will be a Christian.

156
00:07:32,000 --> 00:07:34,000
 If you were born in Poland, you will be a Christian.

157
00:07:34,000 --> 00:07:36,000
 If you were born in Poland, you will be a Christian.

158
00:07:36,000 --> 00:07:38,000
 If you were born in Poland, you will be a Christian.

159
00:07:38,000 --> 00:07:40,000
 If you were born in Poland, you will be a Christian.

160
00:07:40,000 --> 00:07:42,000
 If you were born in Poland, you will be a Christian.

161
00:07:42,000 --> 00:07:44,000
 If you were born in Poland, you will be a Christian.

162
00:07:44,000 --> 00:07:46,000
 If you were born in Poland, you will be a Christian.

163
00:07:46,000 --> 00:07:48,000
 If you were born in Poland, you will be a Christian.

164
00:07:48,000 --> 00:07:50,000
 If you were born in Poland, you will be a Christian.

165
00:07:50,000 --> 00:07:52,000
 If you were born in Poland, you will be a Christian.

166
00:07:52,000 --> 00:07:54,000
 If you were born in Poland, you will be a Christian.

167
00:07:54,000 --> 00:07:56,000
 If you were born in Poland, you will be a Christian.

168
00:07:56,000 --> 00:07:58,000
 If you were born in Poland, you will be a Christian.

169
00:07:58,000 --> 00:08:00,000
 If you were born in Poland, you will be a Christian.

170
00:08:00,000 --> 00:08:02,000
 If you were born in Poland, you will be a Christian.

171
00:08:02,000 --> 00:08:04,000
 If you were born in Poland, you will be a Christian.

172
00:08:04,000 --> 00:08:06,000
 If you were born in Poland, you will be a Christian.

173
00:08:06,000 --> 00:08:08,000
 If you were born in Poland, you will be a Christian.

174
00:08:08,000 --> 00:08:10,000
 If you were born in Poland, you will be a Christian.

175
00:08:10,000 --> 00:08:12,000
 If you were born in Poland, you will be a Christian.

176
00:08:12,000 --> 00:08:14,000
 If you were born in Poland, you will be a Christian.

177
00:08:14,000 --> 00:08:16,000
 If you were born in Poland, you will be a Christian.

178
00:08:16,000 --> 00:08:18,000
 If you were born in Poland, you will be a Christian.

179
00:08:18,000 --> 00:08:20,000
 If you were born in Poland, you will be a Christian.

180
00:08:20,000 --> 00:08:22,000
 If you were born in Poland, you will be a Christian.

181
00:08:22,000 --> 00:08:24,000
 If you were born in Poland, you will be a Christian.

182
00:08:24,000 --> 00:08:26,000
 If you were born in Poland, you will be a Christian.

183
00:08:26,000 --> 00:08:28,000
 If you were born in Poland, you will be a Christian.

184
00:08:28,000 --> 00:08:30,000
 If you were born in Poland, you will be a Christian.

185
00:08:30,000 --> 00:08:32,000
 If you were born in Poland, you will be a Christian.

186
00:08:32,000 --> 00:08:34,000
 If you were born in Poland, you will be a Christian.

187
00:08:34,000 --> 00:08:36,000
 If you were born in Poland, you will be a Christian.

188
00:08:36,000 --> 00:08:38,000
 If you were born in Poland, you will be a Christian.

189
00:08:38,000 --> 00:08:40,000
 If you were born in Poland, you will be a Christian.

190
00:08:40,000 --> 00:08:42,000
 If you were born in Poland, you will be a Christian.

191
00:08:42,000 --> 00:08:44,000
 If you were born in Poland, you will be a Christian.

192
00:08:44,000 --> 00:08:46,000
 If you were born in Poland, you will be a Christian.

193
00:08:46,000 --> 00:08:48,000
 If you were born in Poland, you will be a Christian.

194
00:08:48,000 --> 00:08:50,000
 If you were born in Poland, you will be a Christian.

195
00:08:50,000 --> 00:08:52,000
 If you were born in Poland, you will be a Christian.

196
00:08:52,000 --> 00:08:54,000
 If you were born in Poland, you will be a Christian.

197
00:08:54,000 --> 00:08:56,000
 If you were born in Poland, you will be a Christian.

198
00:08:56,000 --> 00:08:58,000
 If you were born in Poland, you will be a Christian.

199
00:08:58,000 --> 00:09:00,000
 If you were born in Poland, you will be a Christian.

200
00:09:00,000 --> 00:09:02,000
 If you were born in Poland, you will be a Christian.

201
00:09:02,000 --> 00:09:04,000
 If you were born in Poland, you will be a Christian.

202
00:09:04,000 --> 00:09:06,000
 If you were born in Poland, you will be a Christian.

203
00:09:06,000 --> 00:09:08,000
 If you were born in Poland, you will be a Christian.

204
00:09:08,000 --> 00:09:10,000
 If you were born in Poland, you will be a Christian.

205
00:09:10,000 --> 00:09:12,000
 If you were born in Poland, you will be a Christian.

206
00:09:12,000 --> 00:09:14,000
 If you were born in Poland, you will be a Christian.

207
00:09:14,000 --> 00:09:16,000
 If you were born in Poland, you will be a Christian.

208
00:09:16,000 --> 00:09:18,000
 If you were born in Poland, you will be a Christian.

209
00:09:18,000 --> 00:09:20,000
 If you were born in Poland, you will be a Christian.

210
00:09:20,000 --> 00:09:22,000
 If you were born in Poland, you will be a Christian.

211
00:09:22,000 --> 00:09:24,000
 If you were born in Poland, you will be a Christian.

212
00:09:24,000 --> 00:09:26,000
 If you were born in Poland, you will be a Christian.

213
00:09:26,000 --> 00:09:28,000
 If you were born in Poland, you will be a Christian.

214
00:09:28,000 --> 00:09:30,000
 If you were born in Poland, you will be a Christian.

215
00:09:30,000 --> 00:09:32,000
 If you were born in Poland, you will be a Christian.

216
00:09:32,000 --> 00:09:34,000
 If you were born in Poland, you will be a Christian.

217
00:09:34,000 --> 00:09:36,000
 If you were born in Poland, you will be a Christian.

218
00:09:36,000 --> 00:09:38,000
 If you were born in Poland, you will be a Christian.

219
00:09:38,000 --> 00:09:40,000
 If you were born in Poland, you will be a Christian.

220
00:09:40,000 --> 00:09:42,000
 If you were born in Poland, you will be a Christian.

221
00:09:42,000 --> 00:09:44,000
 If you were born in Poland, you will be a Christian.

222
00:09:44,000 --> 00:09:46,000
 If you were born in Poland, you will be a Christian.

223
00:09:46,000 --> 00:09:48,000
 If you were born in Poland, you will be a Christian.

224
00:09:48,000 --> 00:09:50,000
 If you were born in Poland, you will be a Christian.

225
00:09:50,000 --> 00:09:52,000
 If you were born in Poland, you will be a Christian.

226
00:09:52,000 --> 00:09:54,000
 If you were born in Poland, you will be a Christian.

227
00:09:54,000 --> 00:09:56,000
 If you were born in Poland, you will be a Christian.

228
00:09:56,000 --> 00:09:58,000
 If you were born in Poland, you will be a Christian.

229
00:09:58,000 --> 00:10:00,000
 If you were born in Poland, you will be a Christian.

230
00:10:00,000 --> 00:10:02,000
 If you were born in Poland, you will be a Christian.

231
00:10:02,000 --> 00:10:04,000
 If you were born in Poland, you will be a Christian.

232
00:10:04,000 --> 00:10:06,000
 If you were born in Poland, you will be a Christian.

233
00:10:06,000 --> 00:10:08,000
 If you were born in Poland, you will be a Christian.

234
00:10:08,000 --> 00:10:10,000
 If you were born in Poland, you will be a Christian.

235
00:10:10,000 --> 00:10:12,000
 If you were born in Poland, you will be a Christian.

236
00:10:12,000 --> 00:10:14,000
 If you were born in Poland, you will be a Christian.

237
00:10:14,000 --> 00:10:16,000
 If you were born in Poland, you will be a Christian.

238
00:10:16,000 --> 00:10:18,000
 If you were born in Poland, you will be a Christian.

239
00:10:18,000 --> 00:10:20,000
 If you were born in Poland, you will be a Christian.

240
00:10:20,000 --> 00:10:22,000
 If you were born in Poland, you will be a Christian.

241
00:10:22,000 --> 00:10:24,000
 If you were born in Poland, you will be a Christian.

242
00:10:24,000 --> 00:10:26,000
 If you were born in Poland, you will be a Christian.

243
00:10:26,000 --> 00:10:28,000
 If you were born in Poland, you will be a Christian.

244
00:10:28,000 --> 00:10:30,000
 If you were born in Poland, you will be a Christian.

245
00:10:30,000 --> 00:10:32,000
 If you were born in Poland, you will be a Christian.

246
00:10:32,000 --> 00:10:34,000
 If you were born in Poland, you will be a Christian.

247
00:10:34,000 --> 00:10:36,000
 If you were born in Poland, you will be a Christian.

248
00:10:36,000 --> 00:10:38,000
 If you were born in Poland, you will be a Christian.

249
00:10:38,000 --> 00:10:40,000
 If you were born in Poland, you will be a Christian.

250
00:10:40,000 --> 00:10:42,000
 If you were born in Poland, you will be a Christian.

251
00:10:42,000 --> 00:10:44,000
 If you were born in Poland, you will be a Christian.

252
00:10:44,000 --> 00:10:46,000
 If you were born in Poland, you will be a Christian.

253
00:10:46,000 --> 00:10:48,000
 If you were born in Poland, you will be a Christian.

254
00:10:48,000 --> 00:10:50,000
 If you were born in Poland, you will be a Christian.

255
00:10:50,000 --> 00:10:52,000
 If you were born in Poland, you will be a Christian.

256
00:10:52,000 --> 00:10:54,000
 If you were born in Poland, you will be a Christian.

257
00:10:54,000 --> 00:10:56,000
 Which isn't possible answer with that.

258
00:10:56,000 --> 00:10:58,000
 Right.

259
00:10:58,000 --> 00:11:02,280
 So, I mean, the point is that there are many kinds of

260
00:11:02,280 --> 00:11:06,000
 knowledge that are useless.

261
00:11:06,000 --> 00:11:08,000
 You know what I mean?

262
00:11:08,000 --> 00:11:12,000
 Pi to the x digits, right?

263
00:11:12,000 --> 00:11:14,000
 Okay, well that's knowledge.

264
00:11:14,000 --> 00:11:17,550
 Yeah, but we're not striving for complete knowledge, but

265
00:11:17,550 --> 00:11:20,000
 for the ultimate truth.

266
00:11:20,000 --> 00:11:22,000
 Okay, well, yes, that's fine.

267
00:11:22,000 --> 00:11:26,000
 But I don't know.

268
00:11:26,000 --> 00:11:34,300
 To me, that's an illogical, I would say, reason for, you

269
00:11:34,300 --> 00:11:38,000
 know, or it's a meaningless statement.

270
00:11:38,000 --> 00:11:40,000
 I mean, I want knowledge for the sake of knowledge.

271
00:11:40,000 --> 00:11:44,490
 If it's not for the purpose of some benefit, and so that's

272
00:11:44,490 --> 00:11:45,000
 the thing.

273
00:11:45,000 --> 00:11:47,000
 It's utilitarian versus absolute.

274
00:11:47,000 --> 00:11:50,190
 Yeah, I guess ultimately you will be happy if you have

275
00:11:50,190 --> 00:11:55,000
 access to the ultimate truth, even if it helps you because

276
00:11:55,000 --> 00:11:58,000
 it doesn't fit with your preconceptions or whatever.

277
00:11:58,000 --> 00:12:00,990
 So you might be suffering, but ultimately you're happy

278
00:12:00,990 --> 00:12:05,000
 because you've reached your goal, which is complete.

279
00:12:05,000 --> 00:12:07,770
 And the wonderful thing is that it doesn't make you unhappy

280
00:12:07,770 --> 00:12:08,000
.

281
00:12:08,000 --> 00:12:14,000
 Ultimate truth makes you perfectly happy.

282
00:12:14,000 --> 00:12:17,000
 You're studying things.

283
00:12:17,000 --> 00:12:21,600
 Yeah, you know, what's really interesting, you do a reading

284
00:12:21,600 --> 00:12:27,040
 of the Pramajala Sutra where the Pama literally lays down

285
00:12:27,040 --> 00:12:33,050
 the 62 views of existence that most any human being is

286
00:12:33,050 --> 00:12:36,190
 going to have, one of them, even to this day, about is the

287
00:12:36,190 --> 00:12:39,000
 universe infinite, is it not?

288
00:12:39,000 --> 00:12:42,850
 Do you get reborn? Do you nod? You know, is there a creator

289
00:12:42,850 --> 00:12:44,000
? Is there not?

290
00:12:44,000 --> 00:12:50,000
 That ultimately knowledge and all this wisdom is useless.

291
00:12:50,000 --> 00:12:55,000
 It doesn't lead to enlightenment whatsoever.

292
00:12:55,000 --> 00:12:58,890
 And the Pama at least states over and over again that, you

293
00:12:58,890 --> 00:13:03,590
 know, intellectual knowledge and intellectual belief, you

294
00:13:03,590 --> 00:13:08,000
 know, we have the abhudana and it goes on and on and on.

295
00:13:08,000 --> 00:13:14,340
 It's known as and I add and fly ads, but really knowledge

296
00:13:14,340 --> 00:13:19,000
 does not equate to enlightenment.

297
00:13:19,000 --> 00:13:22,000
 Sorry, Lou, can I interrupt you just for a second?

298
00:13:22,000 --> 00:13:26,000
 The Pama clearly states it in many people.

299
00:13:26,000 --> 00:13:28,000
 Can I interrupt you just for a second?

300
00:13:28,000 --> 00:13:31,390
 Really, the only knowledge that could benefit is that with

301
00:13:31,390 --> 00:13:33,000
 alleviated suffering.

302
00:13:33,000 --> 00:13:36,280
 Lou, Clemente, you have to turn. Something's wrong with

303
00:13:36,280 --> 00:13:41,000
 your mic. You're staticking. You got heavy static there.

304
00:13:41,000 --> 00:13:45,950
 I don't know what's wrong, but you're giving us great

305
00:13:45,950 --> 00:13:47,000
 static.

306
00:13:47,000 --> 00:13:52,230
 I think that might be you. I mean, no, I can't pronounce

307
00:13:52,230 --> 00:13:56,000
 your name. My fellow Canadian.

308
00:13:56,000 --> 00:13:59,760
 Okay, sorry, Lou, continue. It's better now. Whatever it

309
00:13:59,760 --> 00:14:01,000
 was, it's good.

310
00:14:01,000 --> 00:14:04,000
 You can hear me now.

311
00:14:04,000 --> 00:14:09,130
 Oh, I was saying that, for instance, in the Brahman Jala S

312
00:14:09,130 --> 00:14:15,000
utra, the Pama lays down the 62 views of existence.

313
00:14:15,000 --> 00:14:19,540
 You know, the point that I was trying to make was that the

314
00:14:19,540 --> 00:14:26,000
 Pama laid down every possible view that people can have

315
00:14:26,000 --> 00:14:29,210
 about relative and ultimate existence, about the nature of

316
00:14:29,210 --> 00:14:30,000
 the universe and all this.

317
00:14:30,000 --> 00:14:38,000
 And his point is that knowledge does not lead to happiness.

318
00:14:38,000 --> 00:14:41,000
 It doesn't even lead to the truth.

319
00:14:41,000 --> 00:14:47,200
 Knowledge does not lead to the truth. And that all of that,

320
00:14:47,200 --> 00:14:50,450
 all of your inquiries to the nature of the universe, okay,

321
00:14:50,450 --> 00:14:53,000
 it's nice as an intellectual pursuit,

322
00:14:53,000 --> 00:15:00,000
 but it does not alleviate suffering, which is his teaching.

323
00:15:00,000 --> 00:15:03,340
 And, you know, this is really important in the Buddha

324
00:15:03,340 --> 00:15:08,280
 Dharma, is that, you know, we're not, I mean, if the point

325
00:15:08,280 --> 00:15:12,020
 of the Buddha Dharma was to be intellectual dilettante,

326
00:15:12,020 --> 00:15:14,000
 well, then we don't need the Buddha Dharma.

327
00:15:14,000 --> 00:15:19,600
 All we need is the university system. All we need to rely

328
00:15:19,600 --> 00:15:24,000
 on is the worldly system as we already have it.

329
00:15:24,000 --> 00:15:30,200
 Okay, but it's kind of a sophistic question of whether

330
00:15:30,200 --> 00:15:36,350
 ultimate truth, ultimate understanding would be worthwhile

331
00:15:36,350 --> 00:15:39,000
 even if it didn't bring happiness.

332
00:15:39,000 --> 00:15:42,430
 Here's maybe how the question should be framed, which, you

333
00:15:42,430 --> 00:15:46,000
 know, it's a bit sophistic because sophist, sophistic?

334
00:15:46,000 --> 00:15:47,000
 Sophiest?

335
00:15:47,000 --> 00:15:50,760
 I'm thinking of sophism. I don't know how you say sophistic

336
00:15:50,760 --> 00:15:51,000
. I think sophistic.

337
00:15:51,000 --> 00:15:53,040
 I know the joke related to that, but I don't know how you

338
00:15:53,040 --> 00:15:54,000
 pronounce the word.

339
00:15:54,000 --> 00:15:57,920
 Okay. Well, the point being, you know, kind of a useless

340
00:15:57,920 --> 00:16:01,000
 question because it's moot point, I guess you could say it,

341
00:16:01,000 --> 00:16:06,000
 because that would be such a messed up universe

342
00:16:06,000 --> 00:16:10,330
 where understanding of things as they are didn't lead to

343
00:16:10,330 --> 00:16:13,000
 happiness or didn't lead to peace.

344
00:16:13,000 --> 00:16:18,570
 That's, you know, it's so intrinsic that they almost mean

345
00:16:18,570 --> 00:16:21,000
 one and the same thing.

346
00:16:21,000 --> 00:16:28,980
 Ultimate realization of the wisdom of things as they are is

347
00:16:28,980 --> 00:16:33,000
 by definition happiness.

348
00:16:33,000 --> 00:16:38,270
 Well, let me correct myself if I, I didn't mean to imply

349
00:16:38,270 --> 00:16:45,100
 that. What I meant was that taking a stand that you have

350
00:16:45,100 --> 00:16:50,980
 the right view because you can elucidate facts and sort of

351
00:16:50,980 --> 00:16:52,000
 prove your theories.

352
00:16:52,000 --> 00:16:57,000
 That is the distinction from having genuine right view.

353
00:16:57,000 --> 00:16:59,000
 No, I got you. I understand.

354
00:16:59,000 --> 00:17:02,000
 Yeah, I wanted to make that distinction.

355
00:17:02,000 --> 00:17:05,270
 Yeah. No, for sure. That's, I mean, and that's really where

356
00:17:05,270 --> 00:17:08,000
 the problem comes. How do you know?

357
00:17:08,000 --> 00:17:11,690
 It's easy to say I want ultimate truth. I want ultimate

358
00:17:11,690 --> 00:17:15,000
 truth, but do you know what you're talking about? Right.

359
00:17:15,000 --> 00:17:18,970
 And until you realize it. And so the problem is that we can

360
00:17:18,970 --> 00:17:23,630
 get caught up in speculation, which has not, which is not

361
00:17:23,630 --> 00:17:25,000
 to do with ultimate truth.

362
00:17:25,000 --> 00:17:30,350
 We can get caught up in speculation about things thinking

363
00:17:30,350 --> 00:17:35,880
 this is a, this is an aspect of ultimate truth when in fact

364
00:17:35,880 --> 00:17:37,000
 it's not.

365
00:17:37,000 --> 00:17:41,890
 You know, like ideas of, you know, the universe being

366
00:17:41,890 --> 00:17:44,000
 finite and infinite.

367
00:17:44,000 --> 00:17:47,790
 Does an Arahant exist after death or do they not exist

368
00:17:47,790 --> 00:17:49,000
 after death?

369
00:17:49,000 --> 00:17:54,360
 And there's these 10 questions that are considered to be

370
00:17:54,360 --> 00:18:00,000
 fairly canonical questions that you just don't ask.

371
00:18:00,000 --> 00:18:02,000
 Because you can't know?

372
00:18:02,000 --> 00:18:06,040
 Because the more you investigate them, the more naughty and

373
00:18:06,040 --> 00:18:08,000
 complicated they become.

374
00:18:08,000 --> 00:18:10,630
 Like look at, look at physics, for example, it's kind of a

375
00:18:10,630 --> 00:18:13,630
 joke. First we had the atom and then we had subatomic

376
00:18:13,630 --> 00:18:14,000
 particles.

377
00:18:14,000 --> 00:18:19,000
 Now we don't even know what we've got. Strings, I guess.

378
00:18:19,000 --> 00:18:21,260
 But there's some interesting space between the strings and

379
00:18:21,260 --> 00:18:22,000
 something else.

380
00:18:22,000 --> 00:18:27,750
 Yeah. And it's like fractals. It's like watching a clown

381
00:18:27,750 --> 00:18:33,000
 show because it just gets more and more absurd.

382
00:18:33,000 --> 00:18:38,070
 Eleven dimensions now we need. And that's not enough. Now

383
00:18:38,070 --> 00:18:40,000
 we need the multiverse.

384
00:18:40,000 --> 00:18:42,820
 Anyway, I mean, not to joke because there's some serious

385
00:18:42,820 --> 00:18:46,670
 things going on there, but it's potentially one question

386
00:18:46,670 --> 00:18:48,000
 that is unanswerable.

387
00:18:48,000 --> 00:18:51,690
 You know, it may just keep going on for infinity. And we've

388
00:18:51,690 --> 00:18:55,000
 come to the limit of human understanding.

389
00:18:55,000 --> 00:18:58,270
 We can't go very much further, but reality could go a

390
00:18:58,270 --> 00:19:00,000
 million times further.

391
00:19:00,000 --> 00:19:04,430
 If all we can get to is strings and eleven dimensions, well

392
00:19:04,430 --> 00:19:08,670
, what if there's billions of iterations or infinite

393
00:19:08,670 --> 00:19:10,000
 iterations?

394
00:19:10,000 --> 00:19:12,000
 Well, let's never get there.

395
00:19:12,000 --> 00:19:16,820
 I mean, it may be that these sorts of things are unknowable

396
00:19:16,820 --> 00:19:18,000
 by humankind.

397
00:19:18,000 --> 00:19:21,000
 And I guess the point is that even if they were knowable,

398
00:19:21,000 --> 00:19:26,000
 they have nothing to do with the core reality.

399
00:19:26,000 --> 00:19:32,780
 So, and I guess another point that we could make is how

400
00:19:32,780 --> 00:19:36,000
 much of ultimate reality.

401
00:19:36,000 --> 00:19:39,230
 Do you need all of ultimate reality? Do you need to know

402
00:19:39,230 --> 00:19:42,000
 the ultimate reality of every being?

403
00:19:42,000 --> 00:19:46,130
 You know, do you need to know, you know, for example, do

404
00:19:46,130 --> 00:19:50,650
 you need to know the course of events for every human being

405
00:19:50,650 --> 00:19:51,000
?

406
00:19:51,000 --> 00:19:55,200
 I mean, every being has their own course, right? Do you

407
00:19:55,200 --> 00:19:57,000
 need to know whether they're going to attain nirvana?

408
00:19:57,000 --> 00:19:59,450
 Is this person going to? Is that person going to? Where are

409
00:19:59,450 --> 00:20:01,000
 they going in their next life?

410
00:20:01,000 --> 00:20:03,350
 Because all of that has to do with ultimate reality. Do you

411
00:20:03,350 --> 00:20:05,000
 need to know all of that?

412
00:20:05,000 --> 00:20:08,330
 No, but I think you're talking about knowledge there. And

413
00:20:08,330 --> 00:20:11,000
 maybe the question was more about wisdom.

414
00:20:11,000 --> 00:20:12,000
 Well, that's a good point.

415
00:20:12,000 --> 00:20:15,330
 We don't strive to know everything but to have wisdom to

416
00:20:15,330 --> 00:20:18,000
 know the ultimate truth about reality.

417
00:20:18,000 --> 00:20:21,810
 Okay. Very good point. And in that case, yeah, I would just

418
00:20:21,810 --> 00:20:24,000
 go back to saying that they're equal in my mind.

419
00:20:24,000 --> 00:20:28,490
 They're in its intrinsically peace, happiness and freedom

420
00:20:28,490 --> 00:20:30,000
 from suffering.

421
00:20:30,000 --> 00:20:34,000
 I don't know. I think that's the best I can do.

422
00:20:34,000 --> 00:20:36,000
 Anybody else?

423
00:20:36,000 --> 00:20:43,000
 Welcome on.

