 Okay, so today's talk is going to be about contentment.
 And so here's a quote to start us off.
 Health is the greatest possession. Contentment is the
 greatest wealth.
 Trustworthiness is the greatest relation. And nirvana is
 the greatest happiness.
 So today's talk is on specifically contentment and how it
 relates to the Buddha's teaching.
 I was asked about a week ago to give a talk on this subject
.
 So here we are. Thank you all for coming. Good to see you
 on this lovely Sunday afternoon.
 The great thing about second life is it never rains unless
 you want it to.
 We can always make use of this wonderful always burning
 campfire.
 So what does contentment have to do with the Buddha's
 teaching?
 How does contentment play a part in in Buddhism?
 I was asked to talk about this and so I had to do a little
 bit of research.
 What would I know about contentment in in the Buddha's
 teaching?
 Leads me to understand that there are three kinds of
 contentment.
 And each one of these three plays a different role in
 Buddhism in the Buddha's teaching.
 The first type of contentment to get it out of the way
 is the type of contentment that the Buddha taught against.
 So you read in the Buddha's teaching that the Buddha taught
 if you want to be a great being,
 if you're striving for true greatness.
 The Buddha said you have to be full of discontent.
 And he taught against a certain type of contentment.
 And so this type of contentment it's important to talk
 about
 because it often pops its rears its head
 both in the world and in in spirituality as well.
 This is a contentment with one's self.
 I've often said it's a mark of a high mind
 that they're constantly working to improve themselves.
 I would say it's a mark that someone is,
 it's a sign that someone is spiritually developed, that
 they're constantly trying to develop themselves.
 That the undeveloped person is known by their lack of
 interest in developing themselves.
 Their feeling that everything's fine the way it is.
 That there's no need to develop those things that
 wise people encourage us to develop and there's no need to
 do away with those things that wise people encourage us to
 do away with.
 So
 I
 just make sure everyone can hear me here.
 Okay that's a yes.
 If you can't hear me it's probably because you don't have
 voice on either that or you don't have
 if you can't hear me this is useless. Can someone yeah can
 someone explain
 the reason why you can't hear me? It doesn't do much good
 to speak to you and tell you why you can't hear me.
 Okay
 so it's a mark of someone who is developing that they are
 interested in developing themselves.
 Obviously.
 And so it's one easy way to distinguish between spiritual
 people and people who are
 not interested in spirituality. It's also sort of a way of
 reassuring ourselves that
 even when our mental development is progressing with
 difficulty that we should be encouraged
 by the fact that we're trying and effort in the Buddha's
 teaching is really a very core value.
 That failure in the meditation is really only possible when
 one ceases to practice
 or ceases to practice correctly. But if you when you stop
 trying or when you start putting out
 effort in another direction in a way that is antithetical
 to the path or give up your effort
 that's the only way you can really fail. As long as you're
 trying striving and you're watching the
 breath watching the stomach when it rises and falls for
 example. But you find you can't focus on it.
 You try and you try and it's easy to become frustrated and
 disheartened.
 But we shouldn't we shouldn't become
 discouraged just because we're not getting anywhere. Our
 effort is our discontent and our
 intention to improve ourselves. Self-improvement is a sign
 of Buddhism. And so in one way this
 distinguishes spiritual people between spiritual people and
 people who are content with being a
 mediocre person. It also distinguishes between two types of
 spiritual people. The type of person
 the type of spiritual person who is content with their
 level of spirituality and can often become
 content with simple states of bliss or happiness or sort of
 pseudo spirituality. Never challenging
 one's views and beliefs practicing only as it's comfortable
. So you see often people when they
 sit in meditation they'll sit against the wall or they'll
 sit in a chair or they'll sit on a bench
 or so on. And always striving to make the meditation pleas
urable. And at any time when it's not
 pleasurable one either stops practicing or adjusts one's
 practice. These are the kind of people who
 when they're sitting in meditation need everybody to be
 quiet. I had a man asking me what I thought
 about using earplugs in meditation and I don't think I
 think it should be pretty clear that
 earplugs are a sign that you have some sort of attachment
 to the sound. So this sort of
 contentment is one that we want to try to avoid resting on
 our laurels per se. If you're
 enlightened and you have no defilements left then I think
 this is the only time one should become
 content with one's spiritual development. But as long as we
 know that in ourselves we still have
 more work to do we should set ourselves on doing that work.
 So this is the first sense that we
 might miss this in thinking that the Buddha taught us to be
 content and so don't worry, don't try,
 don't do anything. Practicing meditation is too much work.
 Going on meditation treats is just
 torturing yourself. You should just live your life
 peacefully and somehow spiritually.
 The type of contentment that the Buddha did teach and
 encourage
 can be broken up into two types. So there are two other
 types of contentment that the Buddha did
 encourage. And the first type is what he generally referred
 to as contentment itself and the word
 he uses is santuti which means being contentment. When the
 Buddha used the word contentment he most
 often was referring to the conceptual objects of our
 reality. So in Buddhism we separate reality
 into two parts the conceptual and the ultimate. Ultimate
 reality is the building blocks of
 reality. Just like in physical reality in physics they talk
 about what are the building blocks,
 the atoms, and then they had atoms and suddenly there were
 subatomic particles and so on. And
 they're still not sure what is the essence of the physical
 in the scientific community.
 But in Buddhism we have a very good understanding of what
 are the essential building blocks.
 So in the physical we're talking about the four elements
 and these are simply the four aspects of
 matter as it's experienced. Matter can be experienced as
 hot or cold, it can be experienced as
 hard or soft, and it can be experienced as tense or flaccid
.
 The mental side of experience is the awareness of the
 physical or the awareness of an object.
 So when we see something and we know that we're seeing when
 we hear something we know that we're
 hearing. This is the ultimate reality. Everything else is
 conceptual. The chair that you're sitting
 in is conceptual. The word chair arises in your mind. You
 only know that it's a chair.
 Once you process the experience in your mind. So without
 the processing in the mind,
 there's simply the feelings that arise from touching the
 chair. And when you see it there's
 a scene in the same way that the seats that some of us are
 sitting on here in the virtual
 reality are only conceptual. So the seats in Second Life
 that we're seeing are our seats because we
 process the sight. The seats that we're sitting on in real
 life are conceptual. They're only seats
 because we process the feeling or the sight or so on. But
 at any rate it's still a concept. Our body
 is a concept because the reality is the experience. This is
 according to the Buddha.
 So the first type of contentment is talking about
 conceptual contentment. Contentment with things.
 Contentment with objects. This is one of those teachings
 that is not exactly core. So it's not
 a theoretical teaching. It's a practical teaching for
 everyday life. And for most of us it's a very
 important teaching. One of the criticisms of many types of
 Buddhism, modern Buddhism, is that it
 often overlooks practical but conventional teachings like
 being content with possessions,
 being content with the things that you own with your
 belongings. And often though people dive
 right into the theoretical and talking about ultimate
 reality and so on. And not that this
 isn't the most important but we have so many delusions and
 we're so attached to the conceptual
 reality that it's like overlooking a very important part of
 the path which is going to be our
 relationship with those things that we cling to. Our
 relationship with those things, those
 conceptual objects that we interact with. So on a practical
 level it's very important that we come
 to terms with our needs, our materialistic desires. Not
 being content with our clothes,
 the clothes that we wear. The Buddha said you use clothing
 to protect the body, to cover up the
 parts of the body that are best left covered. You use food
 simply for doing away with
 hunger to bring energy to the body. You use shelter simply
 to guard from the elements
 and medicines to guard from guard off sickness. And the
 Buddha said these are the four
 these are the four requisites of a human being. These four
 things are what we all cannot do
 without. But for most of us that's not nearly enough.
 Especially in modern times we're
 incredibly materialistic. So we are always needing more and
 we become slaves actually of our
 belongings and of our addictions that we always need more
 and more. And as a result our minds are
 not focused on the here and the now. We're not content with
 this state of reality. We need another
 state where there's this object or that object where we can
 see this thing or hear that thing
 or feel this or smell that or taste this or even simply
 just conceive of the fact that we own this
 and own that and we have so many belongings this and that.
 Delicious food, beautiful clothes,
 games and toys and possessions of all sorts. And because
 our mind is constantly thinking
 about getting more and more and more and is really addicted
 to the pleasure that comes from
 getting what we want, it's very difficult for us to find
 contentment. It's very difficult for us to
 stay in the present reality to accept reality for what it
 is because we're so used to being able to
 categorize reality or to compartmentalize reality.
 We don't have to accept everything. We don't have to accept
 reality for what it is because when it's
 not the way we like we have this idea that we can change it
, that we can control it. And so we
 set ourselves on quite an imbalanced state, imbalanced path
 and are always unfulfilled,
 always unsatisfied and always in a state of discontent. And
 at any time that we don't get
 what we want, we crash. The perpetual building up and
 building up of greater states of attachment
 eventually snowballs and leads us to a great disappointment
 when we can't get what we want.
 When we're unable to compartmentalize reality any further.
 So I think this is perhaps one way of
 seeing the benefits of meditation practice or one good use
 that comes, that is,
 that the meditation practice has in our lives is in looking
 at our desires for things when we want
 something, when we need something, looking at our
 possessions and coming to see that
 the important truth that these are just conceptual, that we
 say I own this and I own that and to
 realize that this entire sentence is, every word of it is
 false. The I, the owned, this,
 I is not true. It's not mine. Even our own self is just a
 conglomeration of
 mind and body and states arising and ceasing, most of which
 are very far out of our control.
 Our states of emotion, our wants and our needs, we find
 that we really can't control them as we
 think we do. The own is not true. We can't for it. We can't
 control things and say,
 let it always be like this. We can't even control the
 happiness that comes from the object because
 after a while we get bored of our possessions and we need
 more. We need the next thing, the newest
 thing. And the this or the word this, the identification of
 the thing as an object
 is also not a part of reality because the object itself is
 either seeing,
 hearing, smelling, tasting, feeling or thinking.
 When we practice meditation, we're able to
 to address this issue of materialism, of being attached to
 things and needing more and not being
 content. The Buddha said it's a mark of a noble person. It
's the lineage, which the word wangsa
 I believe it means. It's the tradition, I guess, of the
 noble ones to be content with whatever robes
 or whatever clothes, whatever food, whatever shelter,
 whatever medicines they get. To be
 content with these things is the way of the noble ones. It
's a mark of nobility or noble in the
 Buddhist sense in terms of being enlightened. So this is
 something that's very important,
 something we should always be looking at in our lives. It's
 easy to think we're practicing
 meditation and suddenly we're accumulating and accumulating
 and always getting more and more and
 new and so on. It's very difficult to control. It's very
 easy to rationalize why we need more
 and more in this and that. It's very difficult to be
 without. But the most important form of
 contentment is not actually called contentment in the
 Buddha's teaching as far as I've found.
 But to me it's the most important type of contentment and
 it's what is really being
 talked about when the Buddha talks about contentment with
 robes and food and so on.
 He's really talking about contentment with reality as it is
.
 Contentment with experience as it is.
 Being totally in tune with reality to the point that
 nothing faces you.
 And it's not called contentment I think because the word
 contentment can be quite misleading in
 this sense. It can mean it can be used to it can slip into
 enjoyment. So we say you know I'm
 enjoying smelling the flowers, stopping and smelling the
 flowers and just enjoying life,
 being content with my life. This isn't what is meant here.
 What is meant here is contentment with
 every experience in terms of not being discontent, not
 being drawn into a judgment about reality,
 not being drawn into a need. And in this sense there are
 two kinds of discontent.
 Discontent based on anger and discontent based on greed.
 You could even say third,
 discontent based on delusion. We want to go there as well.
 But what we what we notice most in our lives is these two,
 discontentment based on anger and
 discontentment based on greed. That we'll be sitting in
 meditation, even just sitting here
 listening to a talk. And it's very difficult to keep the
 mind content simply to listen,
 simply to be here and now.
 Content with what I'm saying, content with the feelings
 going on in your body,
 content with the things around you, content with the
 thoughts in your mind.
 There's so many things that make us angry and upset,
 worried, afraid, bored.
 And our ordinary way of looking at this is to immediately
 need to change,
 need to alter our reality to suit our defilement, to suit
 our defiled state of mind,
 to suit our anger, our dislike. When we don't like
 something we should get rid of it,
 we should remove it. This is the way we look at things, not
 just in terms of our innate sense,
 but also in terms of our views, our idea of what is right.
 We think that it's right that
 when you feel pain, you should adjust, you should move, you
 should do whatever necessary
 to get rid of the pain. This is one kind of discontent that
 is antithetical to what is
 teaching. And it's the purpose of the meditation practice,
 to remove this sort of discontent.
 Rather than trying to change the pain, to change the
 experience,
 we change the way we look at the experience.
 So we simply see it for what it is and when we're in pain
 or maybe too hot, too cold,
 when we're hungry, when there's loud noises or whatever,
 when we're thinking about bad things,
 that we simply see it for what it is and the word bad
 disappears. So we're no longer thinking about
 bad things or feeling bad feelings. We're just thinking or
 feeling or seeing or hearing or
 smelling and tasting. And all of our experience of reality
 is one of contentment in the sense of
 simply seeing it for what it is, not discontent in terms of
 needing it to be different.
 The other form of discontent is based on desire. It's not
 that there's anything wrong with our
 present state of being here. It's that we want something
 more or we begin thinking about
 something and it makes us want it. It makes us desire to do
 this or do that, to obtain this or
 obtain that experience. And I would say from the practice,
 looking at these two, really the only
 way to deal with them adequately is through meditation, is
 through breaking them up into
 their building blocks, the pieces, and seeing that actually
 we're not talking about an entity
 or a single experience. We're talking about several
 different experiences all jumbled up into one.
 First there's the experience of the object, then there's
 the feeling, the recognition of it
 as something good or something bad, then there's the
 feeling that comes of happiness or pain,
 then there's the liking it or the disliking it. Once there
's the liking and the disliking it,
 there's the intention to do this or to do that, to change
 the experience, to obtain
 something different or to remove something from our
 experience. And only then is there the acting
 out on it. And all of these can be broken up and separated
 from each other at any one time.
 When you see something or let's take an easy one, when you
 hear something, suppose you're listening
 to what I'm saying and you really don't like it. Maybe I'm
 saying nasty things about, maybe what
 I'm saying just seems totally diametrically opposed to what
 you believe. Or suppose I start
 yelling at you and saying nasty things to you or so on.
 Suppose someone comes into your room and
 starts complaining and so on. Right away you can say to
 yourself, hearing, hearing, remind yourself
 that it's just a sound. You can try that right now when I'm
 talking. You like it, you don't like it,
 it's not important. As a meditation exercise, simply when
 you hear my voice saying to yourself,
 hearing, hearing, hearing, it's a great way to listen to D
hamma talks.
 You can even become enlightened this way, listening to the
 Dhamma,
 realizing the truth at the same time. One teacher in
 Thailand here would always say
 best way to listen to a Dhamma talk, just say hearing,
 hearing, hearing the whole time.
 Because that's really why we're teaching, is to encourage
 people to meditate.
 And if that doesn't work, if you're not quick enough with
 that, then when the feeling arises,
 when you feel happiness or pain, you just focus on the
 happy, happy, happy, or pain, pain, or
 sad, sad, or whatever. And again, you don't let it build
 into
 real liking or disliking and the intention to change things
. You simply, when you have
 pain in the body, for instance, knowing that it's pain and
 just seeing it for what it is.
 And if you can't catch it there, then you can catch it at
 the liking or the disliking,
 saying to yourself, liking, liking, or wanting, wanting,
 disliking, angry, upset, bored, scared, sad.
 If you still can't catch it there, then you can catch the
 intention. You want to do this. You
 want to obtain something. You want to say something. You
 want to chase someone away or
 run away or so on. And you can focus on that intention,
 wanting, wanting, or intending,
 intending. In this sense, not wanting as greed or as desire
, but as an intention to do something.
 If you still can't catch it there, you're in trouble
 because at that point you're going to
 go out and do something about it. But the amazing thing is,
 is when you focus on all of these things,
 you can see the problem with this line of reaction. You see
 how when you want things to
 be different from what they are, your whole body is tense
 and your mind is in a state of upset,
 of turmoil. Even if it's a desire and you feel happy and
 you really want something, you think,
 "Wow, this is going to make me happy." When you look at it,
 when you analyze it, you see that it's
 not so pleasurable at all, really. Your mind is in a state
 of clinging. You can really feel like
 you're clinging. It's as though you're grabbing something
 in your fist and holding tightly.
 That's how your mind feels at the moment when you want
 something. When you think you have to do
 something to obtain your pleasure, the object of your
 desires, even then you can see that it's not
 really pleasurable, not to speak of anger when you want to
 hurt someone. When you look at it,
 you can see that this is not a wholesome state of mind. It
's not a proper state of mind.
 And so this is how we really develop true contentment in
 Buddhism, coming to separate
 things into their reality. When you see something instead
 of being attached to it, wanting it,
 needing it, clinging to it, simply seeing it for what it is
, if you're happy or happy,
 if you're unhappy or unhappy. Contentment in the sense of
 doing away with any need for things to
 be different, any need for reality to be other than what it
 is. This is really the ultimate state.
 It's difficult. It's dangerous to say this in a sense, just
 to leave it at that because
 it sounds as though you don't do anything then. Being
 content with things as they are,
 as I said, Buddha taught us to be discontent.
 But the key is it's not easy. This is not the ordinary
 nature of our minds. We're working hard
 striving to stop striving. We have to work in order that we
 don't have to do anything,
 in order that we no longer need to attain anything, to do
 away with our need to attain things.
 We're working hard to stop our minds from working so hard,
 in a sense.
 As we practice, as we develop the meditation practice, we
 find ourselves more and more content.
 So through working in this way, through developing
 ourselves,
 we're able to experience all things and we no longer
 compartmentalize reality. We no longer
 separate things into the bearable and the unbearable, the
 acceptable and the unacceptable.
 We're able to accept and react rationally and honestly and
 with wisdom to everything that arises.
 This is the the Buddha's teaching on contentment. So that
 was the Dhamma I would like to give today.
 And if there are any questions, I'm happy to take them.
 Otherwise, thanks everyone for coming.
