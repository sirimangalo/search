1
00:00:00,000 --> 00:00:05,140
 If mind is just a concept, what is that gets reborn,

2
00:00:05,140 --> 00:00:09,000
 reincarnated, that carries our karma, a little confused?

3
00:00:09,000 --> 00:00:15,000
 I guess I have to start, no?

4
00:00:15,000 --> 00:00:20,000
 Please speak up when you have something to say.

5
00:00:20,000 --> 00:00:25,000
 Nothing gets reborn.

6
00:00:28,000 --> 00:00:31,530
 Nothing gets reborn. There's a first statement. Nothing

7
00:00:31,530 --> 00:00:33,000
 gets reincarnated.

8
00:00:33,000 --> 00:00:39,000
 Nothing carries karma. Karma itself doesn't exist.

9
00:00:39,000 --> 00:00:42,880
 And those are all the statements that I would make to start

10
00:00:42,880 --> 00:00:44,000
 off my answer.

11
00:00:44,000 --> 00:00:47,000
 They are verifiable or deniable statements.

12
00:00:47,000 --> 00:00:50,570
 And I would add another one in there is, "Confusion exists

13
00:00:50,570 --> 00:00:51,000
."

14
00:00:51,000 --> 00:00:54,000
 Obviously what you have in your mind now is confusion.

15
00:00:55,000 --> 00:00:59,520
 And a little later on we can explain how that is much more

16
00:00:59,520 --> 00:01:02,000
 important than understanding the answer to this question.

17
00:01:02,000 --> 00:01:12,060
 In fact, something that isn't often fully appreciated is

18
00:01:12,060 --> 00:01:14,180
 that you don't really need answers to these sorts of

19
00:01:14,180 --> 00:01:15,000
 questions.

20
00:01:15,000 --> 00:01:19,000
 Often answering these questions does assuage your doubt.

21
00:01:20,000 --> 00:01:23,980
 And it's another one of these cases of just avoiding the

22
00:01:23,980 --> 00:01:27,280
 real problem is that you have this sort of doubt in the

23
00:01:27,280 --> 00:01:28,000
 first place.

24
00:01:28,000 --> 00:01:31,000
 Which we all do. I'm not trying to pick on you.

25
00:01:31,000 --> 00:01:36,550
 But it's very hard for Western people to get their minds

26
00:01:36,550 --> 00:01:39,530
 around the concept that you don't need an answer to this

27
00:01:39,530 --> 00:01:40,000
 question.

28
00:01:40,000 --> 00:01:42,940
 Because they think, "Well, I have doubt, so you have to

29
00:01:42,940 --> 00:01:45,000
 solve that to get rid of your doubt."

30
00:01:45,000 --> 00:01:47,000
 And actually you don't.

31
00:01:48,000 --> 00:01:50,000
 And you see that it's useless.

32
00:01:50,000 --> 00:01:54,000
 And you'll see that all doubt is useless.

33
00:01:54,000 --> 00:01:57,150
 And guess what? Once you see that all doubt is useless, you

34
00:01:57,150 --> 00:01:58,000
're enlightened.

35
00:01:58,000 --> 00:02:02,520
 It's not quite that simple. It's not just a reflective

36
00:02:02,520 --> 00:02:03,000
 thing.

37
00:02:03,000 --> 00:02:07,000
 But the experience of enlightenment does away with doubt.

38
00:02:07,000 --> 00:02:13,660
 The Sotapana. A Sotapana is free from the mental state of

39
00:02:13,660 --> 00:02:17,000
 doubt to a certain extent.

40
00:02:17,000 --> 00:02:20,740
 At the very least, doubt is a hindrance. It's something

41
00:02:20,740 --> 00:02:26,000
 that distracts and crushes the mind's potential.

42
00:02:26,000 --> 00:02:30,000
 It halts the mind and it strikes.

43
00:02:30,000 --> 00:02:33,760
 So it stops you from seeing clearly. It stops you from

44
00:02:33,760 --> 00:02:35,000
 finding peace and so on.

45
00:02:35,000 --> 00:02:40,810
 And so one way of understanding the practice is just the

46
00:02:40,810 --> 00:02:45,000
 overcoming of doubt or the giving up of things like doubt.

47
00:02:46,000 --> 00:02:50,080
 And if all you were to do is focus on this doubt, all kinds

48
00:02:50,080 --> 00:02:56,110
 of doubt, and overcome that, I think you would be doing

49
00:02:56,110 --> 00:02:57,000
 enough.

50
00:02:57,000 --> 00:03:00,690
 And so in that sense you don't need answers to any question

51
00:03:00,690 --> 00:03:01,000
.

52
00:03:01,000 --> 00:03:05,000
 You don't need even the ones that seem so important.

53
00:03:05,000 --> 00:03:08,180
 And this is an important question that people have. "Is

54
00:03:08,180 --> 00:03:10,000
 there such a thing as rebirth?"

55
00:03:11,000 --> 00:03:14,680
 People have even said to me, "I can't practice unless I

56
00:03:14,680 --> 00:03:18,680
 have something to convince me in the truth of reincarnation

57
00:03:18,680 --> 00:03:20,000
, of rebirth."

58
00:03:20,000 --> 00:03:23,430
 Buddhism seems to make no sense to me unless I can solve

59
00:03:23,430 --> 00:03:26,000
 this question, answer this question.

60
00:03:26,000 --> 00:03:29,000
 Which is a very pitiful state to be in.

61
00:03:29,000 --> 00:03:34,790
 Because you probably will never have the proof that you're

62
00:03:34,790 --> 00:03:36,000
 looking for.

63
00:03:36,000 --> 00:03:39,000
 Science doesn't even give proofs.

64
00:03:40,000 --> 00:03:46,700
 The only way to prove it would be to experience it for

65
00:03:46,700 --> 00:03:51,500
 yourself, which puts you in a precarious position of having

66
00:03:51,500 --> 00:03:53,000
 to wait until you pass away.

67
00:03:53,000 --> 00:03:57,090
 Either that, or what normally people are hinting at in this

68
00:03:57,090 --> 00:04:00,690
 case is that they really, really, really, really want to

69
00:04:00,690 --> 00:04:03,000
 remember their past lives because it's so cool.

70
00:04:04,000 --> 00:04:07,080
 And so they cover it up with this idea that they have to in

71
00:04:07,080 --> 00:04:12,180
 order to appreciate Buddhism, which is in that sense kind

72
00:04:12,180 --> 00:04:17,000
 of deceptive because it's just greed.

73
00:04:17,000 --> 00:04:21,660
 Way beside the point, but very much to the point. And that

74
00:04:21,660 --> 00:04:26,000
's how we should, the conclusion we should come to.

75
00:04:27,000 --> 00:04:30,920
 But to directly answer your question, because for sure that

76
00:04:30,920 --> 00:04:34,000
 will help to make the doubt easier to deal with.

77
00:04:34,000 --> 00:04:39,590
 I can't do better, I think, than to point to the video I

78
00:04:39,590 --> 00:04:43,000
 made on the nature of reality.

79
00:04:43,000 --> 00:04:47,100
 And I actually, I would think you've probably seen that

80
00:04:47,100 --> 00:04:51,080
 video, which makes me wonder if I did cover it in enough

81
00:04:51,080 --> 00:04:52,000
 detail.

82
00:04:53,000 --> 00:04:57,970
 But it's not really what the Buddha would say, but I've

83
00:04:57,970 --> 00:05:02,450
 said it several times already, is that Buddhists, it's not

84
00:05:02,450 --> 00:05:04,000
 that Buddhists believe in rebirth.

85
00:05:04,000 --> 00:05:07,000
 It's that Buddhists don't believe in death.

86
00:05:07,000 --> 00:05:10,280
 And honestly, I have to think that this is something, not

87
00:05:10,280 --> 00:05:13,560
 something the words the Buddha may have used, but it does

88
00:05:13,560 --> 00:05:16,750
 accurately reflect the Buddha's teaching as I understand it

89
00:05:16,750 --> 00:05:17,000
.

90
00:05:18,000 --> 00:05:21,250
 Because death in Buddhism, the Buddha often talked about

91
00:05:21,250 --> 00:05:24,400
 when a person dies and he used these words, and I use these

92
00:05:24,400 --> 00:05:26,000
 words, we all use this word.

93
00:05:26,000 --> 00:05:32,680
 But from a doctrinal point of view and from a core doctrine

94
00:05:32,680 --> 00:05:38,000
, in terms of core doctrine, death occurs at every moment.

95
00:05:38,000 --> 00:05:41,230
 That's what we're talking about when we say the mind doesn

96
00:05:41,230 --> 00:05:42,000
't exist.

97
00:05:43,000 --> 00:05:48,000
 It's just a designation for these momentary experiences,

98
00:05:48,000 --> 00:05:52,430
 the mental side of the momentary experiences, the awareness

99
00:05:52,430 --> 00:05:56,730
 of the object, the awareness of an object that is part of

100
00:05:56,730 --> 00:05:58,000
 an experience.

101
00:05:58,000 --> 00:06:05,080
 So when the body dies, we're totally dealing with concepts

102
00:06:05,080 --> 00:06:08,000
 at that point, because nothing died.

103
00:06:09,000 --> 00:06:13,270
 Mind and body are arising and ceasing, and physical and

104
00:06:13,270 --> 00:06:17,000
 mental are arising and ceasing, and that doesn't stop.

105
00:06:17,000 --> 00:06:20,790
 I think the video is really good. It arose out of a

106
00:06:20,790 --> 00:06:23,550
 conversation I had with my father, and eventually an

107
00:06:23,550 --> 00:06:26,930
 article I wrote about it, and then continuously getting

108
00:06:26,930 --> 00:06:29,000
 this question, I thought, this might help.

109
00:06:30,000 --> 00:06:33,750
 I don't think it helps as much as I would have liked,

110
00:06:33,750 --> 00:06:40,360
 because of the nature of the human mind. Doubt is insidious

111
00:06:40,360 --> 00:06:41,000
.

112
00:06:41,000 --> 00:06:48,080
 Doubt is not a sign that you need an answer. It's a sign

113
00:06:48,080 --> 00:06:52,000
 that you've got a problem. The problem is the doubt.

114
00:06:53,000 --> 00:06:55,880
 You can see this in people. They get an answer to one

115
00:06:55,880 --> 00:06:58,000
 question, and they have ten more.

116
00:06:58,000 --> 00:07:03,010
 The why is not always because they didn't get a sufficient

117
00:07:03,010 --> 00:07:06,840
 answer. It's because they're addicted to doubting, and

118
00:07:06,840 --> 00:07:09,000
 there's a pleasure that comes from it.

119
00:07:09,000 --> 00:07:13,240
 It can be a desire. It can be caught up in desire. They don

120
00:07:13,240 --> 00:07:14,000
't want to go meditate.

121
00:07:15,000 --> 00:07:18,190
 We didn't even meditate tonight, no. They don't want to go

122
00:07:18,190 --> 00:07:24,120
 meditate. They want to ask questions and philosophize, and

123
00:07:24,120 --> 00:07:28,230
 give rise to this rush of getting some conclusion, or

124
00:07:28,230 --> 00:07:32,000
 coming to some kind of a resolution.

125
00:07:33,000 --> 00:07:37,520
 This "I get it" moment, and become addicted to it. So we

126
00:07:37,520 --> 00:07:40,440
 want that. When we don't get it, we're not satisfied with

127
00:07:40,440 --> 00:07:41,000
 the answer.

128
00:07:41,000 --> 00:07:44,980
 But when we get it, we get a little rush. It's totally

129
00:07:44,980 --> 00:07:46,000
 chemical.

130
00:07:46,000 --> 00:07:53,540
 There may be many things wrong with the video, but it seems

131
00:07:53,540 --> 00:07:56,540
 to me, because I've explained this several times, that the

132
00:07:56,540 --> 00:07:59,870
 problem isn't in the explanations, because I'm sure the

133
00:07:59,870 --> 00:08:01,000
 explanation is sound.

134
00:08:02,000 --> 00:08:05,690
 Because that explanation isn't the answer. The answer is to

135
00:08:05,690 --> 00:08:08,000
 focus on the doubt.

136
00:08:08,000 --> 00:08:13,000
 You're doubting. Forget about what you're doubting about.

137
00:08:13,000 --> 00:08:15,700
 Focus on the doubt, because once you can get rid of the

138
00:08:15,700 --> 00:08:18,000
 doubt, you've done all that you need to do.

139
00:08:18,000 --> 00:08:20,790
 People say, "Is this meditation good for me? I'm not sure.

140
00:08:20,790 --> 00:08:22,000
 I have lots of doubts."

141
00:08:23,000 --> 00:08:25,840
 This meditation is for the purpose of giving up doubt and

142
00:08:25,840 --> 00:08:28,960
 overcoming doubt. So all you have to do is focus on the

143
00:08:28,960 --> 00:08:32,680
 doubt, and when it's gone, you've accomplished your mission

144
00:08:32,680 --> 00:08:33,000
.

145
00:08:33,000 --> 00:08:40,670
 But the explanation, for what it's worth, is that in order

146
00:08:40,670 --> 00:08:45,800
 to understand reality, we have to give up all of our precon

147
00:08:45,800 --> 00:08:47,000
ceived notions.

148
00:08:48,000 --> 00:08:50,940
 In the beginning, I think, I'd say something like, "Forget

149
00:08:50,940 --> 00:08:54,000
 about everything you've been taught. Forget everything.

150
00:08:54,000 --> 00:08:57,850
 Forget what your parents taught you. Forget what society is

151
00:08:57,850 --> 00:08:58,000
.

152
00:08:58,000 --> 00:09:03,490
 Forget that you are a human being. Forget absolutely every

153
00:09:03,490 --> 00:09:09,000
 concept you have that puts anything in any context."

154
00:09:10,000 --> 00:09:14,590
 That's really what is necessary to understand reality,

155
00:09:14,590 --> 00:09:18,000
 because context limits understanding.

156
00:09:18,000 --> 00:09:22,590
 You can see this by how people respond to the idea that

157
00:09:22,590 --> 00:09:26,000
 sexual intercourse is for nothing.

158
00:09:26,000 --> 00:09:31,400
 That was a funny discussion we had. Sex is for nothing, but

159
00:09:31,400 --> 00:09:33,000
 sex is for procreation.

160
00:09:34,000 --> 00:09:37,000
 What do you mean? If there weren't procreation, there would

161
00:09:37,000 --> 00:09:40,000
 be no...what would continue the human race?

162
00:09:40,000 --> 00:09:43,000
 What's the continuation of the human race for?

163
00:09:43,000 --> 00:09:46,430
 What's the continuation of the human race for? Well, I

164
00:09:46,430 --> 00:09:49,570
 wouldn't have been born if I hadn't been born. What's the

165
00:09:49,570 --> 00:09:51,000
 purpose of you being born?

166
00:09:54,000 --> 00:09:59,410
 The reason for these kind of questions is because we're

167
00:09:59,410 --> 00:10:03,130
 thinking in a context. In order to understand reality, you

168
00:10:03,130 --> 00:10:04,000
 have to come out of that.

169
00:10:04,000 --> 00:10:09,140
 When you do that, you start to see exactly what I said. It

170
00:10:09,140 --> 00:10:11,000
 really is just experience.

171
00:10:12,000 --> 00:10:16,740
 All you can say about reality without getting into some

172
00:10:16,740 --> 00:10:21,510
 kind of context or some kind of concept or projection is

173
00:10:21,510 --> 00:10:24,000
 that experience occurs.

174
00:10:24,000 --> 00:10:26,050
 There is seeing, there is hearing, there is smelling, there

175
00:10:26,050 --> 00:10:28,000
 is tasting, there is feeling, and there is thinking.

176
00:10:28,000 --> 00:10:33,090
 You can say that pretty reassuredly. The question of

177
00:10:33,090 --> 00:10:36,100
 whether it's real or not real, "Am I just seeing things? Am

178
00:10:36,100 --> 00:10:39,000
 I hallucinating?" is meaningless.

179
00:10:40,000 --> 00:10:44,070
 The experience occurs as it occurs or doesn't occur, one or

180
00:10:44,070 --> 00:10:45,000
 the other.

181
00:10:45,000 --> 00:10:57,370
 Then you can go in several directions. The first is to...I

182
00:10:57,370 --> 00:11:05,430
 think what logically comes next is that it's less likely to

183
00:11:05,430 --> 00:11:09,000
 think of a point where this stops, where this ceases.

184
00:11:09,000 --> 00:11:14,010
 Based on chance or based on just time, that eventually this

185
00:11:14,010 --> 00:11:15,000
 ceases.

186
00:11:15,000 --> 00:11:19,710
 Especially if you go into meditation, as you go deeper, you

187
00:11:19,710 --> 00:11:24,310
 see that mind states give rise to future mind states, which

188
00:11:24,310 --> 00:11:27,000
 is really the core of the Buddha's teaching.

189
00:11:27,000 --> 00:11:30,000
 The craving leads to further becoming.

190
00:11:31,000 --> 00:11:35,350
 It's true that it's possible for this to cease, which would

191
00:11:35,350 --> 00:11:38,000
 be a kind of a death, in a sense.

192
00:11:38,000 --> 00:11:45,110
 But that cessation can only come at the end of desire. As

193
00:11:45,110 --> 00:11:48,000
 long as there is more desire, there will be more becoming,

194
00:11:48,000 --> 00:11:50,230
 more arising, more of this stuff coming up, more

195
00:11:50,230 --> 00:11:51,000
 experiences.

196
00:11:55,000 --> 00:12:00,790
 So it becomes illogical, it goes against your experience at

197
00:12:00,790 --> 00:12:05,000
 that point to think that suddenly it will stop.

198
00:12:05,000 --> 00:12:08,660
 Because you're watching the cause and effect and you're

199
00:12:08,660 --> 00:12:12,510
 saying, "This causes, as far as I can see, they're having

200
00:12:12,510 --> 00:12:14,000
 these effects."

201
00:12:14,000 --> 00:12:19,230
 This desire leads to this result. You want something, so

202
00:12:19,230 --> 00:12:22,430
 there leads to thinking about it, developing it, something

203
00:12:22,430 --> 00:12:24,000
 that wasn't there before.

204
00:12:24,000 --> 00:12:30,050
 Suddenly born. And this is how it works. The idea of a

205
00:12:30,050 --> 00:12:34,330
 human dying and being born is actually just like waves in

206
00:12:34,330 --> 00:12:35,000
 an ocean.

207
00:12:35,000 --> 00:12:38,940
 Because we've contrived this reality of being a human being

208
00:12:38,940 --> 00:12:43,000
, or this reality, this state of being a human being.

209
00:12:43,000 --> 00:12:47,410
 And it occurs in waves. It's not always like that. There

210
00:12:47,410 --> 00:12:51,000
 are beings that don't arise and cease in this way.

211
00:12:52,000 --> 00:12:57,070
 Beings that go from one state to another, but not in terms

212
00:12:57,070 --> 00:12:59,000
 of birth and death.

213
00:12:59,000 --> 00:13:03,000
 That didn't even finish answering your question, though.

214
00:13:03,000 --> 00:13:08,000
 That has to do with the whole idea of getting reborn.

215
00:13:08,000 --> 00:13:12,000
 Because we don't believe in these things.

216
00:13:13,000 --> 00:13:18,980
 The only part of that question that doesn't answer is in

217
00:13:18,980 --> 00:13:22,000
 terms of karma. No, but it does actually.

218
00:13:22,000 --> 00:13:26,130
 Because, just to point out, what is karma? Karma is that

219
00:13:26,130 --> 00:13:29,870
 relationship where you see this craving leads to this

220
00:13:29,870 --> 00:13:31,000
 suffering.

221
00:13:31,000 --> 00:13:35,260
 This wisdom, for example, leads to this happiness or this

222
00:13:35,260 --> 00:13:36,000
 peace.

223
00:13:37,000 --> 00:13:41,120
 Seeing how mind states, momentary mind states, lead one to

224
00:13:41,120 --> 00:13:46,810
 the next. That's how karma works. Karma is a relationship

225
00:13:46,810 --> 00:13:49,000
 from one experience to the next.

226
00:13:49,000 --> 00:13:52,130
 And as such, it doesn't even exist. It's just an

227
00:13:52,130 --> 00:13:56,150
 observation that occurs. You can see that when this arises,

228
00:13:56,150 --> 00:13:57,000
 that arises.

229
00:13:57,000 --> 00:14:00,000
 With the arising of this, there is the arising of that.

230
00:14:00,000 --> 00:14:03,000
 When this doesn't arise, that doesn't arise.

231
00:14:05,000 --> 00:14:13,900
 And in the end, I would say even that realization is

232
00:14:13,900 --> 00:14:16,000
 discarded.

233
00:14:16,000 --> 00:14:22,260
 In a sense, because an arahant doesn't create karma. But

234
00:14:22,260 --> 00:14:25,720
 that's because they also don't think in terms of

235
00:14:25,720 --> 00:14:27,000
 correlation.

236
00:14:28,000 --> 00:14:28,980
 They see things as they are, moment to moment. They

237
00:14:28,980 --> 00:14:30,690
 understand intellectually, as the Buddha taught, "Pati ches

238
00:14:30,690 --> 00:14:36,000
am upada" or "dependent origination".

239
00:14:36,000 --> 00:14:43,030
 But as far as I can see, their experience is not in that

240
00:14:43,030 --> 00:14:48,000
 way. The experience is one to one.

241
00:14:49,000 --> 00:14:53,210
 There is no thought about karma on an experiential level.

242
00:14:53,210 --> 00:14:58,000
 There is only a moment to moment arising and ceasing.

243
00:14:58,000 --> 00:15:01,000
 I hope that helps.

244
00:15:01,000 --> 00:15:05,820
 I think we are looking at the karma from our materialistic

245
00:15:05,820 --> 00:15:11,400
 point of view and want to give it some form to make it

246
00:15:11,400 --> 00:15:14,000
 something we can understand.

247
00:15:15,000 --> 00:15:22,450
 That's just not appropriate for that kind of thing. But it

248
00:15:22,450 --> 00:15:24,000
's exactly not a thing.

249
00:15:24,000 --> 00:15:33,480
 In a way, there is not a fruit in the seed. It's just not

250
00:15:33,480 --> 00:15:39,000
 yet there. It becomes, maybe later, a fruit.

251
00:15:40,000 --> 00:15:44,110
 There is the seed. There is not a fruit yet. The fruit is

252
00:15:44,110 --> 00:15:49,000
 not stored in the seed. It's not stored in the tree.

253
00:15:49,000 --> 00:15:55,490
 It just grows when the conditions are there. So there is

254
00:15:55,490 --> 00:16:03,240
 just nothing that carries our karma. This is just to be

255
00:16:03,240 --> 00:16:05,000
 accepted.

256
00:16:06,000 --> 00:16:13,090
 The whole thing about DNA is encoded there to replicate.

257
00:16:13,090 --> 00:16:16,280
 The fruit comes from the tree and we have scientific

258
00:16:16,280 --> 00:16:18,000
 explanations as to why that is.

259
00:16:18,000 --> 00:16:23,250
 But no, you won't find the fruit there and you won't find a

260
00:16:23,250 --> 00:16:26,000
 thing that is the relationship.

261
00:16:27,000 --> 00:16:30,500
 You will see how the DNA leads to this or whatever and how

262
00:16:30,500 --> 00:16:34,000
 the replication can occur under these conditions.

263
00:16:34,000 --> 00:16:40,210
 Karma is a relation. It's like gravity. Generally, we think

264
00:16:40,210 --> 00:16:42,000
 it's something that exists.

265
00:16:42,000 --> 00:16:44,620
 But when you think about it, when you study physics, it

266
00:16:44,620 --> 00:16:46,000
 actually doesn't exist.

267
00:16:46,000 --> 00:16:50,170
 This whole idea of curved space is just mind-blowing. It

268
00:16:50,170 --> 00:16:54,000
 actually turns out that it's not coming back down.

269
00:16:55,000 --> 00:16:58,570
 What goes up doesn't come down or so on. It's only the fact

270
00:16:58,570 --> 00:17:04,090
 that space is curved, for example, or if that's really the

271
00:17:04,090 --> 00:17:05,000
 truth.

272
00:17:05,000 --> 00:17:13,000
 So that was to be the last question.

273
00:17:14,000 --> 00:17:19,380
 I think we have to give the hall back up because we've kind

274
00:17:19,380 --> 00:17:23,420
 of chased everyone out of the hall. This should be a

275
00:17:23,420 --> 00:17:25,000
 meditation hall, but at this moment it's still not.

276
00:17:25,000 --> 00:17:30,000
 It's now a bedroom. Oh, I'm still recording.

