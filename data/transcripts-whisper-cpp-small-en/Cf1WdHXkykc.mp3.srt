1
00:00:00,000 --> 00:00:07,400
 Hello. Good evening. Welcome to our live broadcast. Again,

2
00:00:07,400 --> 00:00:09,800
 we're starting late because I've just

3
00:00:09,800 --> 00:00:12,300
 done a Dhammapada video. So if you want to listen live to

4
00:00:12,300 --> 00:00:13,780
 that, you can listen live to

5
00:00:13,780 --> 00:00:17,870
 it, but I don't want to record a video for it twice. So we

6
00:00:17,870 --> 00:00:22,780
're starting late. If you wanted

7
00:00:22,780 --> 00:00:26,320
 to get from the beginning, you should go to meditation.siri

8
00:00:26,320 --> 00:00:27,980
-mongolo.org. And that's where

9
00:00:27,980 --> 00:00:32,350
 we have our live group. That's where you can ask questions

10
00:00:32,350 --> 00:00:34,520
 as well. And you'll have an

11
00:00:34,520 --> 00:00:41,250
 audio feed through the whole of the broadcast, including

12
00:00:41,250 --> 00:00:44,800
 the Dhammapada teacher. So a couple

13
00:00:44,800 --> 00:00:50,800
 of announcements, right? At least one, that today we had 10

14
00:00:50,800 --> 00:00:53,200
 people come to our weekly

15
00:00:53,200 --> 00:01:00,210
 meditation group, which is more than usual. Some old, we

16
00:01:00,210 --> 00:01:04,360
 had five returners, four returners.

17
00:01:04,360 --> 00:01:12,730
 We have four old, six new. And we're planning on putting up

18
00:01:12,730 --> 00:01:14,240
 posters. So we'll put up a

19
00:01:14,240 --> 00:01:17,740
 whole bunch more just black and white posters saying, what

20
00:01:17,740 --> 00:01:19,640
 are you waiting for? Because

21
00:01:19,640 --> 00:01:24,330
 the exams are coming up, so people should learn to meditate

22
00:01:24,330 --> 00:01:25,320
. And this Friday we're going

23
00:01:25,320 --> 00:01:29,620
 to have, we're going to try again with the meditation mom

24
00:01:29,620 --> 00:01:32,080
 thing. Just have a small group

25
00:01:32,080 --> 00:01:40,340
 of dedicated people in the main student center room. I

26
00:01:40,340 --> 00:01:45,920
 think I had another announcement.

27
00:01:45,920 --> 00:01:50,420
 Robin, do you have any more announcements? Any

28
00:01:50,420 --> 00:01:52,040
 announcements? No, not until we get a

29
00:01:52,040 --> 00:01:55,280
 little more information maybe on the children's home in

30
00:01:55,280 --> 00:02:02,920
 Florida. We have a meditator coming

31
00:02:02,920 --> 00:02:10,730
 to stay, Vanessa, who is an old, old meditator friend. Well

32
00:02:10,730 --> 00:02:14,880
, fairly old, from Sri Lanka.

33
00:02:14,880 --> 00:02:18,360
 She came to Sri Lanka. She's from Europe. Don't ask me

34
00:02:18,360 --> 00:02:20,880
 which country. I want to say Austria,

35
00:02:20,880 --> 00:02:25,150
 but I can't remember. She'll be here Saturday. She just

36
00:02:25,150 --> 00:02:28,280
 emailed me or messaged me or something,

37
00:02:28,280 --> 00:02:31,590
 asking if she needed to bring white clothes. And no, we're

38
00:02:31,590 --> 00:02:33,440
 not requiring white clothes.

39
00:02:33,440 --> 00:02:37,170
 Maybe we should, but we're not. Maybe we should talk about

40
00:02:37,170 --> 00:02:39,440
 that. I think it's technically,

41
00:02:39,440 --> 00:02:43,890
 it's in the rules, but we can just adapt the rules to say

42
00:02:43,890 --> 00:02:45,960
 optional if you want.

43
00:02:45,960 --> 00:02:48,900
 Well, that's true, but that was because that was a Cambod

44
00:02:48,900 --> 00:02:50,800
ian monastery. I'm not fickle.

45
00:02:50,800 --> 00:02:53,930
 I'm not pushy about that. I mean, what's the deal with

46
00:02:53,930 --> 00:02:58,000
 white clothes? It's a nice tradition.

47
00:02:58,000 --> 00:03:00,330
 Maybe it is something we should consider. It is a nice

48
00:03:00,330 --> 00:03:02,200
 tradition. It's just hard. I mean,

49
00:03:02,200 --> 00:03:04,140
 then you're forcing people to go out and buy clothes just

50
00:03:04,140 --> 00:03:05,360
 so they can practice meditation

51
00:03:05,360 --> 00:03:08,410
 because no one has white clothes. It was more of a thing in

52
00:03:08,410 --> 00:03:10,200
 India because that was accepted

53
00:03:10,200 --> 00:03:13,990
 and finding white clothes was not difficult, I think. It

54
00:03:13,990 --> 00:03:15,520
 was just they were uncolored.

55
00:03:15,520 --> 00:03:17,570
 And there is a point to that because people come with

56
00:03:17,570 --> 00:03:18,720
 bright, you know, in India, it was

57
00:03:18,720 --> 00:03:22,360
 a big thing to have brightly colored clothes like a peacock

58
00:03:22,360 --> 00:03:23,920
. Even now they have these bright

59
00:03:23,920 --> 00:03:30,240
 saris and, you know, so that's kind of a no-no. But if you

60
00:03:30,240 --> 00:03:32,040
're just using, maybe I could say

61
00:03:32,040 --> 00:03:39,080
 neutrals and it has to be the thing is it has to be what's

62
00:03:39,080 --> 00:03:42,760
 the word? Conservative? No,

63
00:03:42,760 --> 00:03:49,040
 it's the word modest. We had one in Jomtong. When I was at

64
00:03:49,040 --> 00:03:50,760
 Jomtong, the kind of people that

65
00:03:50,760 --> 00:03:54,910
 would come, we had one woman come with a skirt that had a

66
00:03:54,910 --> 00:03:58,760
 slit all the way up to her. I don't

67
00:03:58,760 --> 00:04:03,510
 know what you call it, but you could almost see her under

68
00:04:03,510 --> 00:04:05,760
foot. Quite high slit on the

69
00:04:05,760 --> 00:04:09,470
 side. And she always tried to walk around like that. A

70
00:04:09,470 --> 00:04:11,760
 young, fairly attractive woman,

71
00:04:11,760 --> 00:04:17,120
 I think. And the old nuns were not happy with that. We had

72
00:04:17,120 --> 00:04:20,000
 another man wearing some kind

73
00:04:20,000 --> 00:04:24,280
 of Middle Eastern, it's like a big night shirt, you know,

74
00:04:24,280 --> 00:04:26,880
 those night gown, a night gown kind

75
00:04:26,880 --> 00:04:30,220
 of thing. It was that kind of thing. But I think it's from

76
00:04:30,220 --> 00:04:32,120
 the Middle East. Just a white

77
00:04:32,120 --> 00:04:35,560
 one piece. And that was all he was wearing. I don't even

78
00:04:35,560 --> 00:04:37,840
 think he had underwear on underneath.

79
00:04:37,840 --> 00:04:42,240
 This is the kind of thing. I think that's also a no-no.

80
00:04:42,240 --> 00:04:44,480
 Like women wearing skin tight

81
00:04:44,480 --> 00:04:54,640
 shirts, that kind of thing. Not really appropriate. But

82
00:04:54,640 --> 00:04:56,840
 when I was in Manitoba, one of the women

83
00:04:56,840 --> 00:05:00,320
 said it was hot. Really hot in the summer. And I had one

84
00:05:00,320 --> 00:05:02,200
 meditator staying with me and

85
00:05:02,200 --> 00:05:07,340
 she wore shorts that were just above the knees. They were

86
00:05:07,340 --> 00:05:10,480
 actually quite long shorts. And

87
00:05:10,480 --> 00:05:13,600
 people got really upset about that. And I was kind of

88
00:05:13,600 --> 00:05:15,000
 thinking, you know, it would be

89
00:05:15,000 --> 00:05:18,150
 nice if she was wearing pants. But it was so hot that I

90
00:05:18,150 --> 00:05:20,360
 never said anything. It actually

91
00:05:20,360 --> 00:05:23,060
 became a bit of an issue. People were not happy about it.

92
00:05:23,060 --> 00:05:24,080
 Traditional Buddhists were

93
00:05:24,080 --> 00:05:30,660
 not happy. But, you know, in Canada, who thinks twice about

94
00:05:30,660 --> 00:05:37,560
 cargo shorts, right? It's considered

95
00:05:37,560 --> 00:05:41,060
 modest here and what is considered modest in Asia. Very

96
00:05:41,060 --> 00:05:44,600
 different things. Not that there's

97
00:05:44,600 --> 00:05:48,970
 not a point there because showing your legs off is showing

98
00:05:48,970 --> 00:05:51,800
 your legs off. There's potential.

99
00:05:51,800 --> 00:05:56,210
 Because cleavage is acceptable, it doesn't mean it's not

100
00:05:56,210 --> 00:05:58,480
 going to arouse emotions that

101
00:05:58,480 --> 00:06:03,820
 are problematic in a meditation center. That kind of thing.

102
00:06:03,820 --> 00:06:04,840
 So there are issues to talk

103
00:06:04,840 --> 00:06:12,760
 about in the month. It's regarding this. But yeah, so we'll

104
00:06:12,760 --> 00:06:15,480
 have Vanessa will be here.

105
00:06:15,480 --> 00:06:20,230
 And then we have a man named Timu coming. There was another

106
00:06:20,230 --> 00:06:22,480
 man, John, who was supposed to

107
00:06:22,480 --> 00:06:28,250
 come but I think he's got problems with his head. Well, we

108
00:06:28,250 --> 00:06:29,560
 had our first graduate, I forgot

109
00:06:29,560 --> 00:06:33,240
 to mention yesterday. Not quite graduate, but the first

110
00:06:33,240 --> 00:06:35,240
 person to go through the entire

111
00:06:35,240 --> 00:06:38,880
 online course. It means he's not finished. He still has to

112
00:06:38,880 --> 00:06:40,880
 come here and finish the course.

113
00:06:40,880 --> 00:06:46,200
 But that's the thing. He now has to come here and finish.

114
00:06:46,200 --> 00:06:50,880
 So that's my mark. Sorry.

115
00:06:50,880 --> 00:06:56,210
 Yeah, so his slot is available. If you notice, I think he's

116
00:06:56,210 --> 00:06:59,120
 taken himself off, hasn't he?

117
00:06:59,120 --> 00:07:02,540
 I'm not sure. When was that? That was yesterday, right?

118
00:07:02,540 --> 00:07:04,840
 Yeah. So yesterday there's a Tuesday

119
00:07:04,840 --> 00:07:08,860
 slot available now. But he's already planning to come up

120
00:07:08,860 --> 00:07:10,840
 here to finish the course. Next

121
00:07:10,840 --> 00:07:16,830
 January, maybe in January. So it looks like there are three

122
00:07:16,830 --> 00:07:18,920
 available now. So if anyone

123
00:07:18,920 --> 00:07:23,750
 is looking, there's three. If somebody wants to start this

124
00:07:23,750 --> 00:07:25,400
 online course, we'll have to

125
00:07:25,400 --> 00:07:28,760
 see, we'll have to get some feedback as to how useful it is

126
00:07:28,760 --> 00:07:30,560
. People seem to be having,

127
00:07:30,560 --> 00:07:35,600
 getting benefit from it. No, it's not the same as doing

128
00:07:35,600 --> 00:07:38,100
 intensive course or not the

129
00:07:38,100 --> 00:07:43,410
 same as being in the presence of meditators and being with

130
00:07:43,410 --> 00:07:45,440
 your teacher, that kind of

131
00:07:45,440 --> 00:07:53,380
 thing. But it's what we got. It seems to be useful. Just

132
00:07:53,380 --> 00:07:54,520
 having that, you know, just having

133
00:07:54,520 --> 00:07:58,120
 someone to prod you and to remind you and hanging over your

134
00:07:58,120 --> 00:07:59,560
 head, oh, I've got to meet

135
00:07:59,560 --> 00:08:04,320
 with this, meet with my teacher so I better meditate. It's

136
00:08:04,320 --> 00:08:07,960
 quite useful.

137
00:08:07,960 --> 00:08:11,500
 Maybe good preparation for going to do the last part in

138
00:08:11,500 --> 00:08:12,200
 person.

139
00:08:12,200 --> 00:08:16,260
 Yeah, I'm skeptical. I mean, it's not the best preparation.

140
00:08:16,260 --> 00:08:17,880
 I'm not sure how long it's

141
00:08:17,880 --> 00:08:21,870
 going to take people to finish it. But what we have seen is

142
00:08:21,870 --> 00:08:23,680
 people who have done that

143
00:08:23,680 --> 00:08:27,580
 take about a week and can do the course in a week. Week is

144
00:08:27,580 --> 00:08:29,880
 cutting it short. So preferably

145
00:08:29,880 --> 00:08:34,080
 it would be like 10 days. But if you had 10 days, nine days

146
00:08:34,080 --> 00:08:35,360
, right? That's what we'd prefer

147
00:08:35,360 --> 00:08:40,600
 is two weekends. So if you had nine days, really that weeks

148
00:08:40,600 --> 00:08:42,760
 and weeks of training is

149
00:08:42,760 --> 00:08:48,800
 a good preparation for the actual course. But you need

150
00:08:48,800 --> 00:08:50,840
 about nine days. We just don't

151
00:08:50,840 --> 00:08:53,880
 want to throw you right into hardcore intensive practice.

152
00:08:53,880 --> 00:08:55,040
 So we have to build it up because

153
00:08:55,040 --> 00:09:00,640
 even with all that practice, you've still never done a full

154
00:09:00,640 --> 00:09:04,220
 day of meditation theoretically.

155
00:09:04,220 --> 00:09:07,470
 We have another two questions. One is whether this online

156
00:09:07,470 --> 00:09:09,200
 course we have to get people to

157
00:09:09,200 --> 00:09:15,500
 sign a waiver. So we have to discuss that. We do, right?

158
00:09:15,500 --> 00:09:20,340
 And two, I'd like to get everyone's

159
00:09:20,340 --> 00:09:24,600
 commitment to keep rules. So at least five precepts, right?

160
00:09:24,600 --> 00:09:26,200
 Shouldn't be doing this course

161
00:09:26,200 --> 00:09:29,550
 if you're still drinking alcohol. But the five precepts you

162
00:09:29,550 --> 00:09:30,880
 should be keeping if you're

163
00:09:30,880 --> 00:09:34,390
 going to do this course. But I think I have to, I haven't

164
00:09:34,390 --> 00:09:36,440
 talked to people about that.

165
00:09:36,440 --> 00:09:42,490
 That's my bet. I should have. Maybe from now on, I'll ask

166
00:09:42,490 --> 00:09:44,320
 everyone. I'll remind them. Okay.

167
00:09:44,320 --> 00:09:48,040
 We're starting something new. But what about the waiver

168
00:09:48,040 --> 00:09:48,640
 Robin?

169
00:09:48,640 --> 00:09:52,720
 Well, we were discussing that. And currently we put on the

170
00:09:52,720 --> 00:09:54,660
 website the center rules for

171
00:09:54,660 --> 00:10:00,750
 when you're there in person and also some frequently asked

172
00:10:00,750 --> 00:10:02,800
 questions and more geared,

173
00:10:02,800 --> 00:10:05,990
 more so geared toward the residential part of it. But there

174
00:10:05,990 --> 00:10:07,440
's also a meditator application

175
00:10:07,440 --> 00:10:10,690
 and waiver again, geared toward the residential. But there

176
00:10:10,690 --> 00:10:12,600
 was discussion about maybe it would

177
00:10:12,600 --> 00:10:16,320
 be good to have the online meditator sign that as well.

178
00:10:16,320 --> 00:10:22,080
 They'll ask my father what he thinks. He's a lawyer. Yeah.

179
00:10:22,080 --> 00:10:22,920
 And he was very concerned

180
00:10:22,920 --> 00:10:24,960
 with those sorts of things. I remember we were trying to

181
00:10:24,960 --> 00:10:26,160
 play paintball on our, on his

182
00:10:26,160 --> 00:10:29,760
 land and he wouldn't let us. We almost wouldn't let us

183
00:10:29,760 --> 00:10:31,600
 because your liability.

184
00:10:31,600 --> 00:10:40,590
 Yeah. It's a big, a big thing. Liability. Anyway, that's a

185
00:10:40,590 --> 00:10:41,600
 bunch of boring stuff. Does

186
00:10:41,600 --> 00:10:45,880
 anyone have any questions? Yes. Monty, will you be doing

187
00:10:45,880 --> 00:10:47,940
 anything in Florida? Which part

188
00:10:47,940 --> 00:10:51,720
 will you be staying in? I'll be in Tampa area. Dunedin is

189
00:10:51,720 --> 00:10:53,560
 where I'll be staying, but yeah,

190
00:10:53,560 --> 00:10:57,520
 we'll be doing at least a one day course in at the Florida

191
00:10:57,520 --> 00:10:59,600
 Buddhist. We are it's on their

192
00:10:59,600 --> 00:11:02,600
 website. So if you go to the Florida Buddhist, we are a

193
00:11:02,600 --> 00:11:03,840
 website. I'm pretty sure they've

194
00:11:03,840 --> 00:11:10,810
 got an event and don't ask me which day cause I can link

195
00:11:10,810 --> 00:11:13,640
 that post.

196
00:11:13,640 --> 00:11:28,570
 Actually, this one is dated June of 2014. So that seems to

197
00:11:28,570 --> 00:11:38,120
 be the outdated one. Oh, here's

198
00:11:38,120 --> 00:12:03,960
 the one December 26th, 2015. 26th of boxing day.

199
00:12:03,960 --> 00:12:28,350
 I'm trying to send the link there and it's seems to be

200
00:12:28,350 --> 00:12:31,080
 frozen up, but I'll try that again

201
00:12:31,080 --> 00:12:34,070
 in a moment. Hello, Dante. Although we are encouraged to

202
00:12:34,070 --> 00:12:35,600
 ask questions and talk about

203
00:12:35,600 --> 00:12:38,560
 our problems. And of course we all experienced various

204
00:12:38,560 --> 00:12:40,360
 problems or difficulties, but I'm

205
00:12:40,360 --> 00:12:42,930
 interested to know as you are a very experienced

206
00:12:42,930 --> 00:12:45,760
 practitioner, are there any parts of the practice

207
00:12:45,760 --> 00:12:51,500
 that you still find difficult? Brilliant. I don't really

208
00:12:51,500 --> 00:12:54,600
 talk about my own practice.

209
00:12:54,600 --> 00:13:23,640
 Sorry. There's just too many problems associated with it.

210
00:13:23,640 --> 00:13:31,640
 I seem to be having a little trouble with my meditation

211
00:13:31,640 --> 00:13:35,080
 website here. It's frozen up.

212
00:13:35,080 --> 00:13:55,420
 I'm just going to grab my phone. Okay. That was it for the

213
00:13:55,420 --> 00:14:00,280
 questions. All right. Well,

214
00:14:00,280 --> 00:14:05,590
 we had the Dhammapada. So that's for tonight. Just stop

215
00:14:05,590 --> 00:14:10,640
 there. Okay. Thank you, Dante. Good

216
00:14:10,640 --> 00:14:14,240
 night, everyone. Good night.

217
00:14:14,240 --> 00:14:15,240
 Bye.

218
00:14:15,240 --> 00:14:16,240
 Bye.

219
00:14:16,240 --> 00:14:17,240
 Bye.

220
00:14:17,240 --> 00:14:18,240
 Bye.

221
00:14:18,240 --> 00:14:19,240
 Bye.

