1
00:00:00,000 --> 00:00:04,000
 Hello, today we continue our study of the Dhammapada,

2
00:00:04,000 --> 00:00:08,000
 hung with verse number six, which goes,

3
00:00:08,000 --> 00:00:15,000
 "Parejana vijayananti maya meta yama mase,

4
00:00:15,000 --> 00:00:23,000
 yajatata vijayananti tato samanti medaga."

5
00:00:23,000 --> 00:00:31,220
 Which means there are some who don't see clearly that we

6
00:00:31,220 --> 00:00:35,000
 will all have to die,

7
00:00:35,000 --> 00:00:41,000
 but those who do see this clearly,

8
00:00:41,000 --> 00:00:47,000
 "yajatata vijayananti," for those who do see clearly,

9
00:00:47,000 --> 00:00:54,000
 because of that, "tato samanti medaga," because of that,

10
00:00:54,000 --> 00:00:59,000
 they settle their quarrels, settle their disputes.

11
00:00:59,000 --> 00:01:04,070
 So people who don't see clearly or don't fully understand "

12
00:01:04,070 --> 00:01:05,000
vijayananti,"

13
00:01:05,000 --> 00:01:08,000
 many people don't see this.

14
00:01:08,000 --> 00:01:13,000
 For those who see it, see clearly that we all have to die,

15
00:01:13,000 --> 00:01:17,240
 that nothing is permanent, they are able to settle their

16
00:01:17,240 --> 00:01:18,000
 quarrels

17
00:01:18,000 --> 00:01:21,000
 when they realize this truth.

18
00:01:21,000 --> 00:01:25,000
 This was told in regards to one of my personal favorites,

19
00:01:25,000 --> 00:01:29,000
 which is kind of odd, but it's an interesting monk story.

20
00:01:29,000 --> 00:01:35,000
 It's about a quarrel that arose between two monks,

21
00:01:35,000 --> 00:01:40,000
 who lived in Gosidharama, a monastery in the Buddha's time.

22
00:01:40,000 --> 00:01:44,180
 Gosidharama, there were two monks, one was very famous for

23
00:01:44,180 --> 00:01:45,000
 teaching the Dhamma,

24
00:01:45,000 --> 00:01:49,270
 which is the meditation practice and the things that the

25
00:01:49,270 --> 00:01:50,000
 Buddha taught us to do,

26
00:01:50,000 --> 00:01:54,700
 we should do, and another one was very famous for the Vin

27
00:01:54,700 --> 00:01:55,000
aya,

28
00:01:55,000 --> 00:01:58,140
 the Buddha's teaching on the monk's rules and the things

29
00:01:58,140 --> 00:02:00,000
 that one shouldn't do.

30
00:02:00,000 --> 00:02:05,310
 So they were both very famous, they each had a very large

31
00:02:05,310 --> 00:02:07,000
 group of students.

32
00:02:07,000 --> 00:02:11,230
 One day the one who taught the Dhamma was coming out of the

33
00:02:11,230 --> 00:02:12,000
 washroom,

34
00:02:12,000 --> 00:02:14,730
 and the one who taught the Vinaya was going into the wash

35
00:02:14,730 --> 00:02:15,000
room,

36
00:02:15,000 --> 00:02:18,350
 and the one who taught the Vinaya looked and saw that the

37
00:02:18,350 --> 00:02:19,000
 one who teaches the Dhamma

38
00:02:19,000 --> 00:02:27,000
 had left some water in the bucket in the washroom,

39
00:02:27,000 --> 00:02:31,640
 which you're not supposed to do because mosquitoes will lay

40
00:02:31,640 --> 00:02:33,000
 their eggs.

41
00:02:33,000 --> 00:02:37,000
 So he said, "Hey, that's an offense to do that."

42
00:02:37,000 --> 00:02:40,340
 He said, "Oh, I'm sorry, I didn't realize, then please let

43
00:02:40,340 --> 00:02:42,000
 me confess it to you."

44
00:02:42,000 --> 00:02:46,000
 When there's an offense against the rules,

45
00:02:46,000 --> 00:02:49,380
 then you have to confess and say, "I promise that you try

46
00:02:49,380 --> 00:02:52,000
 your best not to do it again."

47
00:02:52,000 --> 00:02:55,350
 Just make it clear that you're aware of the fact that you

48
00:02:55,350 --> 00:02:56,000
 did it.

49
00:02:56,000 --> 00:02:59,000
 The one who taught the Vinaya, he said,

50
00:02:59,000 --> 00:03:03,530
 "If you didn't realize it was a, or if you didn't do it

51
00:03:03,530 --> 00:03:08,000
 intentionally, then it's not an offense."

52
00:03:08,000 --> 00:03:11,000
 The other monk said, "Okay."

53
00:03:11,000 --> 00:03:14,000
 And he left without confessing it.

54
00:03:14,000 --> 00:03:17,570
 The one who taught the Vinaya went around telling all his

55
00:03:17,570 --> 00:03:18,000
 students

56
00:03:18,000 --> 00:03:21,080
 that this other monk had an offense that he hadn't

57
00:03:21,080 --> 00:03:22,000
 confessed.

58
00:03:22,000 --> 00:03:27,830
 And so they went and told the students of the other teacher

59
00:03:27,830 --> 00:03:29,000
 this story.

60
00:03:29,000 --> 00:03:31,000
 And those monks told the teacher, and the teacher said,

61
00:03:31,000 --> 00:03:33,710
 "Well, first he tells me it's not an offense, now he says

62
00:03:33,710 --> 00:03:35,000
 it is an offense.

63
00:03:35,000 --> 00:03:37,000
 That monk's a liar."

64
00:03:37,000 --> 00:03:39,560
 And so his students heard this, and they went and told the

65
00:03:39,560 --> 00:03:40,000
 other students,

66
00:03:40,000 --> 00:03:43,000
 "Hey, your teacher's a liar."

67
00:03:43,000 --> 00:03:47,600
 And then they told their teacher, and the teacher got more

68
00:03:47,600 --> 00:03:49,000
 angry and said nasty things

69
00:03:49,000 --> 00:03:53,000
 until finally they were split up into two different groups.

70
00:03:53,000 --> 00:03:57,000
 As a result of that, the novices and the bhikkhunis,

71
00:03:57,000 --> 00:04:00,540
 the female monks, were split into two different groups as

72
00:04:00,540 --> 00:04:01,000
 well.

73
00:04:01,000 --> 00:04:03,460
 The lay people who supported the monks split up into two

74
00:04:03,460 --> 00:04:04,000
 groups.

75
00:04:04,000 --> 00:04:07,290
 The angels who guarded the monastery split up into two

76
00:04:07,290 --> 00:04:08,000
 groups.

77
00:04:08,000 --> 00:04:12,130
 The angels that were their friends who lived in the sky and

78
00:04:12,130 --> 00:04:13,000
 in the various heavens,

79
00:04:13,000 --> 00:04:15,000
 they all split up into two groups.

80
00:04:15,000 --> 00:04:20,000
 And the story goes that this part of the universe split up

81
00:04:20,000 --> 00:04:21,000
 all the way up into the Brahma,

82
00:04:21,000 --> 00:04:25,000
 or the God realms, and the gods themselves were split,

83
00:04:25,000 --> 00:04:28,000
 believe it or not,

84
00:04:28,000 --> 00:04:32,000
 over some water being left in a bucket.

85
00:04:32,000 --> 00:04:36,020
 Now, the story goes on and on about how the Buddha tried to

86
00:04:36,020 --> 00:04:40,000
 convince them to change their ways,

87
00:04:40,000 --> 00:04:43,000
 and they wouldn't listen, and actually they said to him,

88
00:04:43,000 --> 00:04:45,960
 "Oh, please don't bother yourself, we will take care of

89
00:04:45,960 --> 00:04:47,000
 this on our own."

90
00:04:47,000 --> 00:04:50,000
 And they wouldn't let the Buddha even get involved.

91
00:04:50,000 --> 00:04:52,630
 And so the Buddha left and went and stayed, and there's a

92
00:04:52,630 --> 00:04:53,000
 story,

93
00:04:53,000 --> 00:04:56,200
 this whole side story about how the Buddha stayed in the

94
00:04:56,200 --> 00:04:57,000
 forest with an elephant,

95
00:04:57,000 --> 00:05:02,000
 and the elephant gave him food and took care of him.

96
00:05:02,000 --> 00:05:05,280
 And then after that he came back, and then at the end he

97
00:05:05,280 --> 00:05:08,000
 got all these monks.

98
00:05:08,000 --> 00:05:10,000
 What happened, sorry, the best part of the...

99
00:05:10,000 --> 00:05:13,210
 When the Buddha left, all of the lay people said, "Well,

100
00:05:13,210 --> 00:05:14,000
 where's the Buddha going?"

101
00:05:14,000 --> 00:05:16,320
 They said, "Oh, well, we told them not to get involved with

102
00:05:16,320 --> 00:05:17,000
 our quarrel."

103
00:05:17,000 --> 00:05:20,680
 And so he left, and all the lay people got very angry and

104
00:05:20,680 --> 00:05:21,000
 upset,

105
00:05:21,000 --> 00:05:23,000
 and said, "How could you...

106
00:05:23,000 --> 00:05:27,150
 Now we don't have the opportunity to see the Buddha because

107
00:05:27,150 --> 00:05:28,000
 of you."

108
00:05:28,000 --> 00:05:31,000
 And so they stopped giving food to the monks.

109
00:05:31,000 --> 00:05:34,610
 Because of that, the monks had a very hard time getting Aum

110
00:05:34,610 --> 00:05:37,000
's food and just surviving.

111
00:05:37,000 --> 00:05:41,000
 And they went and they apologized, and the people said,

112
00:05:41,000 --> 00:05:43,280
 "Well, you have to get an apology from the... get

113
00:05:43,280 --> 00:05:46,000
 forgiveness from the Buddha."

114
00:05:46,000 --> 00:05:48,000
 And because it was the rain season, they couldn't do that,

115
00:05:48,000 --> 00:05:51,430
 so they had to wait three months, and they were very, very

116
00:05:51,430 --> 00:05:53,000
 hard up for three months.

117
00:05:53,000 --> 00:05:57,000
 And after this, the Buddha came back and...

118
00:05:57,000 --> 00:05:59,870
 And then he put all the... put the monks who were quarre

119
00:05:59,870 --> 00:06:01,000
ling in a special place

120
00:06:01,000 --> 00:06:03,550
 and didn't let any of the other monks get involved with

121
00:06:03,550 --> 00:06:04,000
 them.

122
00:06:04,000 --> 00:06:06,480
 When people came and asked, "Where are those quarreling

123
00:06:06,480 --> 00:06:07,000
 monks?"

124
00:06:07,000 --> 00:06:08,500
 The Buddha would always point to them and say, "There,

125
00:06:08,500 --> 00:06:10,000
 there, there they are."

126
00:06:10,000 --> 00:06:13,000
 And they were so ashamed that eventually they came and

127
00:06:13,000 --> 00:06:14,000
 asked forgiveness from the Buddha

128
00:06:14,000 --> 00:06:17,000
 and felt really ashamed for what they had done.

129
00:06:17,000 --> 00:06:20,000
 And that's when the Buddha told this verse.

130
00:06:20,000 --> 00:06:24,800
 Now, the verse has, I think, two important lessons that we

131
00:06:24,800 --> 00:06:26,000
 can draw from it.

132
00:06:26,000 --> 00:06:29,840
 And the first one is in regards to death, which I sort of

133
00:06:29,840 --> 00:06:31,000
 went into last time.

134
00:06:31,000 --> 00:06:33,000
 But I'd like to talk a little bit more.

135
00:06:33,000 --> 00:06:37,810
 The second one is about the importance of wisdom and

136
00:06:37,810 --> 00:06:39,000
 understanding,

137
00:06:39,000 --> 00:06:42,120
 and how understanding is really the key in overcoming

138
00:06:42,120 --> 00:06:43,000
 quarrels

139
00:06:43,000 --> 00:06:48,980
 and overcoming all suffering and all unwholesome, it's all

140
00:06:48,980 --> 00:06:50,000
 evil.

141
00:06:50,000 --> 00:06:54,690
 So first in regards to death, it's interesting how the

142
00:06:54,690 --> 00:07:00,000
 theory or the ideology,

143
00:07:00,000 --> 00:07:04,000
 the teaching on rebirth and how the mind continues,

144
00:07:04,000 --> 00:07:08,000
 actually makes death a more important event.

145
00:07:08,000 --> 00:07:10,860
 And this verse kind of makes that clear in a way that we

146
00:07:10,860 --> 00:07:12,000
 might not think of.

147
00:07:12,000 --> 00:07:15,000
 The Buddha is telling us how important death is.

148
00:07:15,000 --> 00:07:17,830
 And we think, "Well, if the mind continues, then how is it

149
00:07:17,830 --> 00:07:19,000
 really so important?"

150
00:07:19,000 --> 00:07:21,000
 And the way it works is this.

151
00:07:21,000 --> 00:07:25,000
 The body and the mind have two, they work together,

152
00:07:25,000 --> 00:07:28,000
 but they have two different characteristics.

153
00:07:28,000 --> 00:07:31,000
 The mind is very quick. The mind in one moment can be here,

154
00:07:31,000 --> 00:07:33,000
 the next moment can be there, it can arise

155
00:07:33,000 --> 00:07:37,000
 and move very quickly from one object to the next.

156
00:07:37,000 --> 00:07:40,000
 The body on the other hand is very fixed.

157
00:07:40,000 --> 00:07:44,890
 And so our state of being in this life, on a physical level

158
00:07:44,890 --> 00:07:47,000
, is quite fixed,

159
00:07:47,000 --> 00:07:49,000
 but on a mental level it's quite fluid.

160
00:07:49,000 --> 00:07:53,090
 So your mind can change, and if your mind becomes more and

161
00:07:53,090 --> 00:07:54,000
 more corrupt,

162
00:07:54,000 --> 00:07:57,690
 it can do that very quickly and it can change from good to

163
00:07:57,690 --> 00:08:01,000
 evil or from evil to good relatively quickly.

164
00:08:01,000 --> 00:08:04,000
 And there can be quite a disparity, therefore,

165
00:08:04,000 --> 00:08:08,000
 between the physical realm and the mental realm.

166
00:08:08,000 --> 00:08:12,530
 You can be surrounded by good things and get lots of good

167
00:08:12,530 --> 00:08:16,000
 and wonderful pleasures and luxuries

168
00:08:16,000 --> 00:08:19,050
 and have a very corrupt and evil mind that one would think

169
00:08:19,050 --> 00:08:23,000
 this person doesn't deserve all of that luxury.

170
00:08:23,000 --> 00:08:26,000
 But the truth is, this is because of the difference,

171
00:08:26,000 --> 00:08:31,000
 the body is a coarse construct and the amount of energy

172
00:08:31,000 --> 00:08:36,000
 that is involved in simply the creation

173
00:08:36,000 --> 00:08:40,000
 of a human being or of a physical construct is immense.

174
00:08:40,000 --> 00:08:44,000
 And so it has a great power, this power of inertia,

175
00:08:44,000 --> 00:08:49,000
 where you can't just stop that inertia.

176
00:08:49,000 --> 00:08:52,220
 Now some people, if they do really bad things or really

177
00:08:52,220 --> 00:08:53,000
 good things,

178
00:08:53,000 --> 00:08:57,000
 they might wind up cutting it off by dying.

179
00:08:57,000 --> 00:09:01,020
 You hear about really good people who are killed because of

180
00:09:01,020 --> 00:09:04,000
 their unwillingness to do evil

181
00:09:04,000 --> 00:09:07,390
 and so on or die because they're not willing to do evil in

182
00:09:07,390 --> 00:09:09,000
 order to live on and so on.

183
00:09:09,000 --> 00:09:12,540
 And you of course hear about evil, evil people who do bad

184
00:09:12,540 --> 00:09:15,000
 things and as a result are killed

185
00:09:15,000 --> 00:09:19,260
 or die in unpleasant circumstances because of the way their

186
00:09:19,260 --> 00:09:20,000
 minds are.

187
00:09:20,000 --> 00:09:24,340
 But for most people, or for many people, the inertia and

188
00:09:24,340 --> 00:09:26,000
 the power of the physical

189
00:09:26,000 --> 00:09:32,000
 prevents the repercussions until the moment of death.

190
00:09:32,000 --> 00:09:34,130
 Now at the moment of death, the body is totally

191
00:09:34,130 --> 00:09:35,000
 reconstructed.

192
00:09:35,000 --> 00:09:40,000
 There is a period where the mind leaves the body behind

193
00:09:40,000 --> 00:09:47,100
 and begins this immense activity or work required to

194
00:09:47,100 --> 00:09:48,000
 rebuild another body

195
00:09:48,000 --> 00:09:53,580
 or to create another human being starting at the moment of

196
00:09:53,580 --> 00:09:55,000
 conception.

197
00:09:55,000 --> 00:09:59,150
 Now that of course is much more mental and the nature of

198
00:09:59,150 --> 00:10:01,000
 that is going to depend

199
00:10:01,000 --> 00:10:05,510
 almost entirely on the nature of the mind or the set of the

200
00:10:05,510 --> 00:10:06,000
 mind.

201
00:10:06,000 --> 00:10:10,000
 And what sort of work is the mind going to undertake?

202
00:10:10,000 --> 00:10:13,090
 For most people, we're caught up in this programming of

203
00:10:13,090 --> 00:10:15,000
 creating a human being and a human life

204
00:10:15,000 --> 00:10:18,000
 and so as a result, we'll be reborn as a human being.

205
00:10:18,000 --> 00:10:20,630
 But you can be reborn in many different ways based on your

206
00:10:20,630 --> 00:10:21,000
 mind.

207
00:10:21,000 --> 00:10:26,230
 And therefore, the importance of, as we've seen in the past

208
00:10:26,230 --> 00:10:27,000
 stories,

209
00:10:27,000 --> 00:10:31,000
 the importance of settling our disputes in this life

210
00:10:31,000 --> 00:10:34,000
 and settling them in our mind anyway.

211
00:10:34,000 --> 00:10:38,000
 Sometimes we can't stop other people from being angry at us

212
00:10:38,000 --> 00:10:38,000
.

213
00:10:38,000 --> 00:10:43,660
 But of greatest importance is that we are able to settle

214
00:10:43,660 --> 00:10:45,000
 them in our own minds.

215
00:10:45,000 --> 00:10:51,180
 And this is the word samanti, samanti, samanti, which means

216
00:10:51,180 --> 00:10:53,000
 to stabilize

217
00:10:53,000 --> 00:10:57,000
 or to tranquilize them in our minds, to make it so that

218
00:10:57,000 --> 00:11:01,000
 they're no longer a source of clinging.

219
00:11:01,000 --> 00:11:09,000
 We no longer cling, we no longer regurgitate and go after

220
00:11:09,000 --> 00:11:12,000
 and recreate this in our minds.

221
00:11:12,000 --> 00:11:17,380
 So, which was obviously the problem in the case with these

222
00:11:17,380 --> 00:11:18,000
 two sets of monks,

223
00:11:18,000 --> 00:11:22,000
 which was a real danger and so the Buddha pointed out to

224
00:11:22,000 --> 00:11:25,000
 them that this is really useless,

225
00:11:25,000 --> 00:11:26,000
 this is really futile.

226
00:11:26,000 --> 00:11:30,000
 And the reason people do this is because they don't see.

227
00:11:30,000 --> 00:11:31,000
 They don't see clearly.

228
00:11:31,000 --> 00:11:34,290
 So, this is the importance of death because it frees the

229
00:11:34,290 --> 00:11:36,000
 mind to make choices again

230
00:11:36,000 --> 00:11:40,140
 on a level that it's not free to and it's tight because of

231
00:11:40,140 --> 00:11:47,000
 the inertia of its previous work in creating this being.

232
00:11:47,000 --> 00:11:54,000
 And the second part is that importance of understanding

233
00:11:54,000 --> 00:11:58,000
 and how understanding things like death is important.

234
00:11:58,000 --> 00:12:03,730
 On a meditative level, understanding the truth of birth and

235
00:12:03,730 --> 00:12:07,000
 death as occurring on a momentary level

236
00:12:07,000 --> 00:12:13,890
 is far more important and crucial, in fact, in freeing

237
00:12:13,890 --> 00:12:17,000
 ourselves up from strife

238
00:12:17,000 --> 00:12:21,000
 and quarrel and disputes and suffering.

239
00:12:21,000 --> 00:12:26,090
 And I've said it before, but it's worth reiterating that

240
00:12:26,090 --> 00:12:28,000
 this is really all you need to do

241
00:12:28,000 --> 00:12:32,000
 to overcome your suffering, to overcome quarrels and so on.

242
00:12:32,000 --> 00:12:35,000
 You don't have to go and apologize to the people.

243
00:12:35,000 --> 00:12:37,000
 I mean, that's always a good way to overcome it.

244
00:12:37,000 --> 00:12:42,160
 But on a general level, in regards to our clingings, in

245
00:12:42,160 --> 00:12:45,000
 regards to our cravings, in regards to our attachments,

246
00:12:45,000 --> 00:12:49,000
 all that needs to be done is to see it clearly.

247
00:12:49,000 --> 00:12:51,600
 When you're addicted to something, you don't have to force

248
00:12:51,600 --> 00:12:53,000
 yourself to stop

249
00:12:53,000 --> 00:12:55,540
 or try to convince yourself that it's wrong or remind

250
00:12:55,540 --> 00:12:58,000
 yourself again and again how bad you are for doing it.

251
00:12:58,000 --> 00:13:00,720
 All you have to do is see the addiction clearly for what it

252
00:13:00,720 --> 00:13:01,000
 is.

253
00:13:01,000 --> 00:13:04,000
 See it objectively. Let it be. Let it come up.

254
00:13:04,000 --> 00:13:07,000
 And when you see it for what it is, you'll let go of it.

255
00:13:07,000 --> 00:13:10,260
 You'll have no desire to cling to it because you'll see

256
00:13:10,260 --> 00:13:12,000
 that everything ceases.

257
00:13:12,000 --> 00:13:14,130
 So this Buddhist teaching on death was only the

258
00:13:14,130 --> 00:13:15,000
 introduction.

259
00:13:15,000 --> 00:13:18,200
 It's this reminder to bring the monks back to this idea

260
00:13:18,200 --> 00:13:20,000
 that everything arises and ceases

261
00:13:20,000 --> 00:13:23,040
 because once they can see that we die, they can see how

262
00:13:23,040 --> 00:13:24,000
 futile it is.

263
00:13:24,000 --> 00:13:26,000
 Why are we fighting? What are we hoping to gain?

264
00:13:26,000 --> 00:13:30,000
 We can come out on top, as I said in the last video.

265
00:13:30,000 --> 00:13:33,000
 But what does it mean to be on top?

266
00:13:33,000 --> 00:13:37,000
 Because eventually we all die and we're put on bottom.

267
00:13:37,000 --> 00:13:42,000
 And once they start to do that, then you can start to see

268
00:13:42,000 --> 00:13:42,000
 that really,

269
00:13:42,000 --> 00:13:45,540
 why would we cling to anything? And you start to be able to

270
00:13:45,540 --> 00:13:46,000
 look at your clinging

271
00:13:46,000 --> 00:13:49,000
 and see that when you cling to something, you hold on to it

272
00:13:49,000 --> 00:13:49,000
.

273
00:13:49,000 --> 00:13:51,460
 It maybe brings you pleasure temporarily, but then it

274
00:13:51,460 --> 00:13:53,000
 disappears.

275
00:13:53,000 --> 00:13:57,780
 And what is the benefit? What is the gain that comes from

276
00:13:57,780 --> 00:13:58,000
 that?

277
00:13:58,000 --> 00:14:03,000
 When you see on a phenomenological level, an experiential

278
00:14:03,000 --> 00:14:04,000
 or moment to moment level,

279
00:14:04,000 --> 00:14:08,470
 how everything arises and ceases, how this happiness, this

280
00:14:08,470 --> 00:14:10,000
 pleasure that you're hoping to get,

281
00:14:10,000 --> 00:14:14,000
 is really actually bound up with suffering and stress

282
00:14:14,000 --> 00:14:17,000
 because it conditions the mind

283
00:14:17,000 --> 00:14:22,000
 to want more and to strive more and to be stressed.

284
00:14:22,000 --> 00:14:29,500
 And you can make yourself sick and uncomfortable, unwell as

285
00:14:29,500 --> 00:14:33,000
 a result of the clinging.

286
00:14:33,000 --> 00:14:36,730
 So when you practice meditation, when you look at the

287
00:14:36,730 --> 00:14:38,000
 objects of awareness

288
00:14:38,000 --> 00:14:42,610
 on a moment to moment basis, you're able to see that

289
00:14:42,610 --> 00:14:45,000
 everything arises and ceases.

290
00:14:45,000 --> 00:14:49,230
 Simply that seeing allows you to, it changes your whole way

291
00:14:49,230 --> 00:14:51,000
 of looking at things.

292
00:14:51,000 --> 00:14:54,180
 You no longer look at things in terms of the universe, in

293
00:14:54,180 --> 00:14:56,000
 terms of us and them and I

294
00:14:56,000 --> 00:14:59,000
 and me and mine and so on.

295
00:14:59,000 --> 00:15:04,000
 All of the, so many of the points of reference that we take

296
00:15:04,000 --> 00:15:11,000
 on an ordinary run of the mill

297
00:15:11,000 --> 00:15:16,220
 frame of reference are gone, are lost, are given up by

298
00:15:16,220 --> 00:15:18,000
 someone who takes up this practice.

299
00:15:18,000 --> 00:15:21,000
 You really change your whole way of looking at the world.

300
00:15:21,000 --> 00:15:25,140
 And this is, I think it's important to pick up this word in

301
00:15:25,140 --> 00:15:27,000
 this verse, "Vijayananti",

302
00:15:27,000 --> 00:15:30,000
 which is really the same word as "Vipassana".

303
00:15:30,000 --> 00:15:32,610
 "Vipassana" means to see clearly, "Vijayananti" means to

304
00:15:32,610 --> 00:15:35,000
 know clearly or to understand clearly.

305
00:15:35,000 --> 00:15:39,240
 But it's really the same word and it has the exact same

306
00:15:39,240 --> 00:15:40,000
 meaning.

307
00:15:40,000 --> 00:15:42,000
 And it's really the core of Buddhism.

308
00:15:42,000 --> 00:15:44,700
 And it's so easy when you read this verse to just skip by

309
00:15:44,700 --> 00:15:45,000
 that and say,

310
00:15:45,000 --> 00:15:47,320
 "Oh yeah, yeah, we have to think about death and death is

311
00:15:47,320 --> 00:15:48,000
 the most important."

312
00:15:48,000 --> 00:15:52,000
 No, the most important is the knowing, the understanding.

313
00:15:52,000 --> 00:15:55,690
 When you see, with death as an example, when you see that

314
00:15:55,690 --> 00:15:57,000
 everything ceases,

315
00:15:57,000 --> 00:16:01,000
 when you see, you know, our quarrel, why are we quarreling,

316
00:16:01,000 --> 00:16:02,000
 we have to die,

317
00:16:02,000 --> 00:16:05,000
 it helps you see that everything ceases.

318
00:16:05,000 --> 00:16:08,690
 When you see on a momentary level, "I'm clinging to this

319
00:16:08,690 --> 00:16:09,000
 thing,

320
00:16:09,000 --> 00:16:11,000
 why would I cling to it when it has to cease?"

321
00:16:11,000 --> 00:16:14,000
 When you're able to see things arising and ceasing,

322
00:16:14,000 --> 00:16:17,190
 this is what the meaning is, when you're able to understand

323
00:16:17,190 --> 00:16:18,000
 clearly

324
00:16:18,000 --> 00:16:22,000
 that there is nothing in the world that you can hold on to

325
00:16:22,000 --> 00:16:24,000
 that lasts more than an instant.

326
00:16:24,000 --> 00:16:29,440
 When you're able to see that it's futile to cling and it's

327
00:16:29,440 --> 00:16:31,000
 useless, it's unbeneficial,

328
00:16:31,000 --> 00:16:36,130
 and actually it's a source of great stress and suffering by

329
00:16:36,130 --> 00:16:37,000
 its very nature,

330
00:16:37,000 --> 00:16:40,840
 by the stress caused from clinging, from wanting, from

331
00:16:40,840 --> 00:16:42,000
 craving and so on.

332
00:16:42,000 --> 00:16:49,000
 And the building and building up of more and more craving

333
00:16:49,000 --> 00:16:53,000
 and suffering when you don't get what you want and so on.

334
00:16:53,000 --> 00:16:57,400
 So, this is another important verse and another good lesson

335
00:16:57,400 --> 00:16:58,000
 for us.

336
00:16:58,000 --> 00:17:02,000
 This is the Dhamma for today from the Dhammapada.

337
00:17:02,000 --> 00:17:05,000
 So thanks for tuning in. All the best.

