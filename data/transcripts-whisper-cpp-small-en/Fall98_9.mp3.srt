1
00:00:00,000 --> 00:00:09,910
 The Dhammatog, delivered by Venerable Ussalananda at the

2
00:00:09,910 --> 00:00:13,440
 Tagata Meditation Center, San Jose,

3
00:00:13,440 --> 00:00:21,520
 California, Special Retreat of November 1998.

4
00:00:21,520 --> 00:00:37,030
 Okay, so our retreat has come to an end and I hope every

5
00:00:37,030 --> 00:00:45,200
 yogi gets something out of this retreat.

6
00:00:45,200 --> 00:00:53,760
 Although what you get may be not what you expected.

7
00:00:53,760 --> 00:01:01,940
 But during the retreat, you keep your minds pure, you are

8
00:01:01,940 --> 00:01:06,720
 away from mental defilements,

9
00:01:06,720 --> 00:01:16,320
 so you enjoy or you experience the mental seclusion.

10
00:01:16,320 --> 00:01:23,360
 Seclusion from mental defilements, seclusion from worries

11
00:01:23,360 --> 00:01:30,320
 and anxieties from the outside world.

12
00:01:30,320 --> 00:01:40,520
 To reach the final goal is not easy and we don't know how

13
00:01:40,520 --> 00:01:44,240
 much longer we will have to

14
00:01:44,240 --> 00:01:48,240
 make effort to reach that goal.

15
00:01:48,240 --> 00:01:57,510
 Even during the time of the Buddha, even though there were

16
00:01:57,510 --> 00:02:06,880
 people who had potential to get

17
00:02:06,880 --> 00:02:12,560
 emancipation or to get enlightenment, did not all get

18
00:02:12,560 --> 00:02:14,320
 enlightenment.

19
00:02:14,320 --> 00:02:21,940
 There are many reasons that block their way to the

20
00:02:21,940 --> 00:02:26,640
 attainment of enlightenment.

21
00:02:26,640 --> 00:02:34,490
 It is explained to us that there are two reasons why people

22
00:02:34,490 --> 00:02:37,200
 fail to get enlightenment.

23
00:02:37,200 --> 00:02:44,560
 Although they had the potential to reach enlightenment and

24
00:02:44,560 --> 00:02:50,000
 although they even get to see the Buddha.

25
00:02:50,000 --> 00:03:01,030
 The reasons they gave are one, lack of action or lack of

26
00:03:01,030 --> 00:03:09,600
 exertion and two, having a bad friend.

27
00:03:09,600 --> 00:03:18,910
 We are or whatever we do, we need a good friend. So by

28
00:03:18,910 --> 00:03:22,400
 associating with a good friend,

29
00:03:22,400 --> 00:03:32,720
 we can develop our own faculties. We can develop into

30
00:03:32,720 --> 00:03:38,640
 reaching into higher stages of spiritual

31
00:03:38,640 --> 00:03:47,640
 path. At Friends, we are influenced by these people and we

32
00:03:47,640 --> 00:03:52,960
 tend to follow their wrong activities

33
00:03:52,960 --> 00:04:00,480
 or their wrong advices. So we did very wrong things

34
00:04:00,480 --> 00:04:07,200
 following the advice of bad friends.

35
00:04:07,200 --> 00:04:15,600
 King Ajatasattu. Now, D

36
00:04:15,600 --> 00:04:24,580
 enticed him when Ajatasattu was a young prince to his side

37
00:04:24,580 --> 00:04:30,000
 and incited him to kill his own father

38
00:04:30,000 --> 00:04:35,290
 so that he could become a king. And as you know, Dvadada

39
00:04:35,290 --> 00:04:39,280
 was a very ambitious man.

40
00:04:39,280 --> 00:04:45,660
 He tried to kill the Buddha to become the Buddha himself

41
00:04:45,660 --> 00:04:48,880
 and so he incited the young

42
00:04:48,880 --> 00:04:54,380
 prince Ajatasattu to kill his own father to become a king.

43
00:04:54,380 --> 00:04:57,360
 And Ajatasattu was successful

44
00:04:57,360 --> 00:05:06,290
 so he became a king and he killed his own father. Although

45
00:05:06,290 --> 00:05:10,400
 Dvadada attempted three times to kill

46
00:05:10,400 --> 00:05:15,950
 the Buddha, he was not successful because nobody can kill

47
00:05:15,950 --> 00:05:20,640
 the Buddha. So the first time he sent

48
00:05:20,640 --> 00:05:27,500
 sharpshooters to shoot the Buddha and the second time he

49
00:05:27,500 --> 00:05:31,200
 sent the great elephant to attack the

50
00:05:31,200 --> 00:05:38,110
 Buddha. When these two failed, he himself pushed a big rock

51
00:05:38,110 --> 00:05:41,840
 onto the Buddha when Buddha was walking

52
00:05:41,840 --> 00:05:50,920
 up and down on a hill. But the rock missed the Buddha and

53
00:05:50,920 --> 00:05:54,880
 hit some rock on the hill and the

54
00:05:54,880 --> 00:06:02,170
 splendor of that rock hit the Buddha on his toes and so

55
00:06:02,170 --> 00:06:07,040
 Buddha had his blood congealed there

56
00:06:07,040 --> 00:06:15,800
 and had a severe pain. But that was the most Dvadada can do

57
00:06:15,800 --> 00:06:17,280
 to the life of the Buddha.

58
00:06:18,720 --> 00:06:26,450
 But that act of causing the blood congealed in the body of

59
00:06:26,450 --> 00:06:32,960
 the Buddha is as bad as killing one's own

60
00:06:32,960 --> 00:06:42,100
 father. It is said that after killing his father, Ajatasatt

61
00:06:42,100 --> 00:06:47,200
u was remorseful and he was always haunted

62
00:06:47,200 --> 00:06:54,240
 by this horrible memory and so he even could not go to

63
00:06:54,240 --> 00:07:02,000
 sleep well. But years later and he did not

64
00:07:02,000 --> 00:07:08,510
 dare to go to the Buddha and see him because he had killed

65
00:07:08,510 --> 00:07:14,960
 the Buddha's disciple who was his father.

66
00:07:15,920 --> 00:07:23,160
 But years later, about eight years before the Buddha's

67
00:07:23,160 --> 00:07:28,400
 death, he went to see the Buddha.

68
00:07:28,400 --> 00:07:39,200
 So he asked his trusted friend, the royal physician, Jivaka

69
00:07:39,200 --> 00:07:41,840
, to take him to the Buddha.

70
00:07:42,880 --> 00:07:46,370
 And so he went out to the Buddha to see the Buddha and he

71
00:07:46,370 --> 00:07:49,360
 went out at night. So he was always afraid

72
00:07:49,360 --> 00:07:54,480
 of people trying to kill him. Now people who had done

73
00:07:54,480 --> 00:07:57,920
 something like this is always suspicious

74
00:07:57,920 --> 00:08:03,040
 and they live in fear. So he was afraid that there might be

75
00:08:03,040 --> 00:08:06,800
 attempt on his life and so he had a great

76
00:08:10,000 --> 00:08:14,030
 number of people to guard him. It is said that he went to

77
00:08:14,030 --> 00:08:17,040
 the monastery with 500 elephants.

78
00:08:17,040 --> 00:08:23,580
 So he went to the monastery, he saw the Buddha and he asked

79
00:08:23,580 --> 00:08:27,840
 the Buddha to explain to him

80
00:08:27,840 --> 00:08:35,330
 the fruits of being a recluse that could be realized in

81
00:08:35,330 --> 00:08:38,720
 this very life. And so

82
00:08:39,680 --> 00:08:44,460
 in response to his question, Buddha delivered to him a

83
00:08:44,460 --> 00:08:49,920
 discourse now known as Samanya Pala Sota.

84
00:08:49,920 --> 00:08:55,270
 So it is a fairly long sota included in the collection of

85
00:08:55,270 --> 00:08:57,360
 long discourses.

86
00:08:57,360 --> 00:09:04,240
 So at the end of that discourse, Ajatasadu was very glad

87
00:09:04,240 --> 00:09:05,760
 and so he declared to be

88
00:09:06,960 --> 00:09:11,110
 the disciple of the Buddha who took refuge in the Buddha,

89
00:09:11,110 --> 00:09:12,960
 the Dhamma and the Sangha.

90
00:09:12,960 --> 00:09:21,440
 And then he left. So after he left, Buddha said to his

91
00:09:21,440 --> 00:09:25,520
 monks, "Monks, if this king had not killed

92
00:09:25,520 --> 00:09:30,880
 his own father, he would have become a sotapana on this

93
00:09:30,880 --> 00:09:35,360
 very seat. Now that he had killed his own

94
00:09:35,360 --> 00:09:45,850
 father, he was to suffer in hell. And so after death, King

95
00:09:45,850 --> 00:09:51,600
 Ajatasadu fell to hell. And so now he

96
00:09:51,600 --> 00:09:56,410
 is suffering in one of the great hells. So even though he

97
00:09:56,410 --> 00:09:59,840
 was very devoted to the Buddha after that,

98
00:10:03,120 --> 00:10:07,680
 that devotion to the Buddha could not save him or could not

99
00:10:07,680 --> 00:10:11,840
 prevent him from being reborn in hell.

100
00:10:11,840 --> 00:10:24,160
 Now Ajatasadu had the capability to gain enlightenment in

101
00:10:24,160 --> 00:10:27,840
 that life. And he did

102
00:10:28,480 --> 00:10:33,860
 get to see the Buddha and to listen to the Dhamma talks of

103
00:10:33,860 --> 00:10:37,120
 the Buddha. But

104
00:10:37,120 --> 00:10:46,950
 he was unable to gain enlightenment. Not only was he unable

105
00:10:46,950 --> 00:10:48,960
 to gain enlightenment,

106
00:10:49,680 --> 00:10:56,940
 but he went down to hell and is suffering there because he

107
00:10:56,940 --> 00:11:00,320
 had killed his own father.

108
00:11:00,320 --> 00:11:05,160
 And he killed his own father because he associated with a

109
00:11:05,160 --> 00:11:09,760
 bad friend, the venerable Devadatta.

110
00:11:10,640 --> 00:11:16,660
 So associating with a bad friend can be that much damaging

111
00:11:16,660 --> 00:11:20,320
 to one's prosperity, one's spiritual

112
00:11:20,320 --> 00:11:31,190
 growth, and to one's rebirth. So associating with a bad

113
00:11:31,190 --> 00:11:36,560
 friend is one reason why people do not get

114
00:11:36,560 --> 00:11:42,450
 enlightenment, although they have the capability to become

115
00:11:42,450 --> 00:11:47,520
 arahants in that life and they get to see

116
00:11:47,520 --> 00:11:58,440
 the Buddha. First reason is lack of action, lack of exert

117
00:11:58,440 --> 00:12:02,160
ion. Now lack of action

118
00:12:05,840 --> 00:12:13,040
 is of two kinds. Lack of action on the part of the teacher

119
00:12:13,040 --> 00:12:16,080
 and lack of action on the part of

120
00:12:16,080 --> 00:12:18,640
 let us say students.

121
00:12:18,640 --> 00:12:30,320
 A wandering ascetic and a layperson went to see the Buddha.

122
00:12:33,680 --> 00:12:42,330
 And when the ascetic saw the monks surrounding the Buddha

123
00:12:42,330 --> 00:12:46,400
 very calm and quiet and disciplined,

124
00:12:46,400 --> 00:12:52,260
 he was very glad and so he expressed his gladness or

125
00:12:52,260 --> 00:12:58,400
 appreciation of this tranquility of the monks

126
00:12:58,400 --> 00:13:02,960
 to the Buddha. So the Buddha told him that among the monks

127
00:13:02,960 --> 00:13:06,160
 there were arahants, sottapanas, and so

128
00:13:06,160 --> 00:13:09,960
 on, and also those who practiced the four foundations of

129
00:13:09,960 --> 00:13:14,640
 mindfulness. When he heard this,

130
00:13:14,640 --> 00:13:19,040
 the layperson, his name was Pisa, so the layperson said, "

131
00:13:19,040 --> 00:13:21,120
It's wonderful that you have

132
00:13:21,920 --> 00:13:27,740
 taught the foundations of mindfulness to all people. Even

133
00:13:27,740 --> 00:13:31,280
 we," he said, "as laypersons

134
00:13:31,280 --> 00:13:37,280
 practice the foundations of mindfulness from time to time."

135
00:13:41,520 --> 00:13:47,240
 But he talked to them, telling them that there are four

136
00:13:47,240 --> 00:13:51,760
 kinds of persons, and then asked Pisa

137
00:13:51,760 --> 00:14:03,210
 which person he pleases him. And so Pisa answered that the

138
00:14:03,210 --> 00:14:06,560
 fourth person pleases his mind.

139
00:14:09,360 --> 00:14:16,940
 And then Buddha asked him why he likes the fourth person

140
00:14:16,940 --> 00:14:19,440
 and he gave reasons. And after

141
00:14:19,440 --> 00:14:27,850
 giving the reasons, he just said, "Bhanti, I'm busy, so I'm

142
00:14:27,850 --> 00:14:30,240
 now going." So the Buddha said,

143
00:14:30,240 --> 00:14:36,130
 "Do what you think fit." So he got up and left. When he

144
00:14:36,130 --> 00:14:40,240
 left, Buddha said to the monks, "Munks,

145
00:14:40,240 --> 00:14:47,620
 if Pisa would have stayed until the end of my exposition of

146
00:14:47,620 --> 00:14:51,040
 the four types of individuals,

147
00:14:51,040 --> 00:14:56,960
 he would have been endowed with great benefits." So that

148
00:14:56,960 --> 00:14:59,520
 means the commentary explains that

149
00:14:59,520 --> 00:15:03,850
 he would have become a Sottapana. But now that he left

150
00:15:03,850 --> 00:15:06,800
 before the end of the talk,

151
00:15:06,800 --> 00:15:12,230
 that he was deprived of that opportunity. But Buddha said,

152
00:15:12,230 --> 00:15:16,240
 although he was deprived of this

153
00:15:16,240 --> 00:15:21,530
 opportunity to become a Sottapana, he got some things from

154
00:15:21,530 --> 00:15:23,520
 coming and seeing him,

155
00:15:24,240 --> 00:15:32,960
 that Pisa got faith or devotion in the Buddha. And also he

156
00:15:32,960 --> 00:15:36,400
 heard about the four foundations

157
00:15:36,400 --> 00:15:42,790
 of mindfulness for his father practice. In this story, Pisa

158
00:15:42,790 --> 00:15:46,720
 failed to become a Sottapana,

159
00:15:47,360 --> 00:15:51,660
 not because he was not endowed with the capabilities, not

160
00:15:51,660 --> 00:15:54,400
 because he was endowed with paramese,

161
00:15:54,400 --> 00:15:58,680
 and not because he did not see the Buddha, because he did

162
00:15:58,680 --> 00:16:01,120
 see the Buddha and he talked to the Buddha,

163
00:16:01,120 --> 00:16:07,200
 he listened to the Buddha, but because he left before the

164
00:16:07,200 --> 00:16:12,000
 discourse was over. So that is

165
00:16:13,440 --> 00:16:19,170
 non-action or lack of action on his part. That means he

166
00:16:19,170 --> 00:16:24,080
 should have stayed until the end of

167
00:16:24,080 --> 00:16:28,090
 the exposition. He should have listened to the exposition

168
00:16:28,090 --> 00:16:31,920
 given by the Buddha. And he did not do

169
00:16:31,920 --> 00:16:37,290
 that. He did not make that effort. And so for lack of

170
00:16:37,290 --> 00:16:41,920
 effort on his part, he was unable to

171
00:16:43,760 --> 00:16:46,720
 to become a Sottapana.

172
00:16:46,720 --> 00:16:55,380
 Now this story teaches us one good lesson. That is, when

173
00:16:55,380 --> 00:17:00,400
 someone is giving a Dhamma talk,

174
00:17:00,400 --> 00:17:07,310
 you should listen to the Dhamma talk until the end. So you

175
00:17:07,310 --> 00:17:10,240
 should not get up and leave

176
00:17:11,040 --> 00:17:12,640
 in the middle of a Dhamma talk.

177
00:17:12,640 --> 00:17:21,950
 And people are impatient. If they don't think they are not

178
00:17:21,950 --> 00:17:26,160
 getting anything out of the talk,

179
00:17:26,160 --> 00:17:32,620
 then they wanted to leave. And they had no patience to wait

180
00:17:32,620 --> 00:17:35,280
 until the end of the talk.

181
00:17:36,000 --> 00:17:44,700
 And so they lost the opportunity of getting information

182
00:17:44,700 --> 00:17:46,640
 about the teachings of the Buddha,

183
00:17:46,640 --> 00:17:52,320
 informations about how to practice meditation and so on.

184
00:17:52,320 --> 00:17:58,320
 And so they also lose a great opportunity

185
00:17:58,320 --> 00:18:03,840
 by getting up and leaving before the Dhamma talk ends.

186
00:18:03,840 --> 00:18:10,240
 People can say, "Oh, this time we are too

187
00:18:10,240 --> 00:18:14,200
 busy and so we have to leave, but we can listen to the

188
00:18:14,200 --> 00:18:23,280
 tapes later on." But listening a talk while

189
00:18:23,280 --> 00:18:28,500
 it is being given and listening the recording later are

190
00:18:28,500 --> 00:18:33,120
 different. So your frame of mind is

191
00:18:33,120 --> 00:18:39,660
 different. When a person was talking, you have more

192
00:18:39,660 --> 00:18:45,280
 attention, you pay to the talk, and also

193
00:18:46,080 --> 00:18:53,060
 you have this eagerness to listen to the talk. But when you

194
00:18:53,060 --> 00:18:56,320
 play a table and listen to it,

195
00:18:56,320 --> 00:19:03,130
 although you may be listening, you have not as much

196
00:19:03,130 --> 00:19:09,360
 attention, you pay to the talk,

197
00:19:09,360 --> 00:19:14,170
 or you have not as much respect to the talk. So although

198
00:19:14,170 --> 00:19:17,360
 you can listen to the talk later on tapes,

199
00:19:17,360 --> 00:19:24,480
 it is better to listen to the talk while it is being given.

200
00:19:24,480 --> 00:19:35,760
 So that is the lack of exertion on the part of the student

201
00:19:35,760 --> 00:19:37,760
 or the listener.

202
00:19:39,360 --> 00:19:44,200
 And the other one is lack of exertion or lack of action on

203
00:19:44,200 --> 00:19:47,360
 the part of the teacher.

204
00:19:47,360 --> 00:19:54,650
 There was a Brahmin called Jahnu Sorni. So one day the V

205
00:19:54,650 --> 00:19:59,600
enerable Saribuddha heard about him

206
00:20:00,560 --> 00:20:08,390
 and asked a monk whether that Brahmin practiced as heed

207
00:20:08,390 --> 00:20:12,400
fulness. And the answer was no.

208
00:20:12,400 --> 00:20:18,570
 So when he met that Brahmin, Saribuddha asked him, "Do you

209
00:20:18,570 --> 00:20:21,440
 practice heedfulness?"

210
00:20:22,000 --> 00:20:27,350
 He said, "No, Bandhe. We have many things to do." And he

211
00:20:27,350 --> 00:20:31,600
 gave ten excuses why he was not mindful.

212
00:20:31,600 --> 00:20:40,190
 So Saribuddha talked to him the ill consequences of heed

213
00:20:40,190 --> 00:20:44,160
lessness. And then later,

214
00:20:45,920 --> 00:20:55,920
 when that Brahmin was gravely ill, actually on his deathbed

215
00:20:55,920 --> 00:21:02,640
, he sent a man to give respects

216
00:21:02,640 --> 00:21:06,610
 to the Buddha in his name and also to invite the Venerable

217
00:21:06,610 --> 00:21:11,120
 Saribuddha to him and to come and talk

218
00:21:11,120 --> 00:21:16,130
 to him. So the Venerable Saribuddha accepted and went to

219
00:21:16,130 --> 00:21:19,600
 him and then asked him whether his

220
00:21:19,600 --> 00:21:25,110
 sickness is increasing or decreasing. And he said, "My

221
00:21:25,110 --> 00:21:27,920
 sickness is increasing."

222
00:21:30,000 --> 00:21:38,500
 And so Venerable Saribuddha taught him something, asking

223
00:21:38,500 --> 00:21:41,440
 him, "Which is better,

224
00:21:41,440 --> 00:21:47,220
 to be reborn in hell or to be reborn as an animal and then

225
00:21:47,220 --> 00:21:50,320
 to be reborn as an animal and to be reborn

226
00:21:50,320 --> 00:21:54,940
 as a human being and so on and so on until the highest

227
00:21:54,940 --> 00:21:58,480
 realm of the devas?" And then

228
00:21:59,200 --> 00:22:03,520
 Venerable Saribuddha said, "Which is better, to be reborn

229
00:22:03,520 --> 00:22:08,480
 in the highest deva-ram or in the Brahma

230
00:22:08,480 --> 00:22:13,110
 Ram?" So when the Saribuddha said the Brahma Ram, the Brah

231
00:22:13,110 --> 00:22:17,200
min was very, very heavy. Then he said,

232
00:22:17,200 --> 00:22:20,320
 "Oh, Saribuddha said Brahma Ram." Saribuddha said Brahma

233
00:22:20,320 --> 00:22:24,320
 Ram. Then the Venerable Saribuddha thought

234
00:22:24,320 --> 00:22:29,380
 that the Brahmins are attached to the realm of the Brahmins

235
00:22:29,380 --> 00:22:32,320
 because their way is to reach the

236
00:22:32,320 --> 00:22:37,390
 world of the Brahma. So the Venerable Saribuddha taught him

237
00:22:37,390 --> 00:22:39,920
 the way to reach the Brahma world.

238
00:22:39,920 --> 00:22:44,750
 That means Saribuddha taught him the four Brahma viharas,

239
00:22:44,750 --> 00:22:47,520
 the four divine abidings.

240
00:22:48,240 --> 00:22:52,180
 So after teaching him how to practice loving-kindness,

241
00:22:52,180 --> 00:22:55,680
 compassion, sympathetic joy and equanimity,

242
00:22:55,680 --> 00:23:02,110
 the Venerable Saribuddha left. So sometime after he left,

243
00:23:02,110 --> 00:23:06,400
 the Brahmin died and he was reborn as a Brahma.

244
00:23:11,840 --> 00:23:15,440
 Maybe Saribuddha was heavy when he went back to the

245
00:23:15,440 --> 00:23:18,800
 monastery with the thinking that he had shown

246
00:23:18,800 --> 00:23:24,440
 the the Brahmin the way to the Brahma world. But at that

247
00:23:24,440 --> 00:23:28,080
 time, Buddha was at the monastery

248
00:23:28,080 --> 00:23:36,840
 in the Dhamma hall giving Dhamma talk to monks and Buddha

249
00:23:36,840 --> 00:23:41,360
 said, "Here comes Saribuddha."

250
00:23:43,360 --> 00:23:49,600
 Although he could do better, he has just taught the the

251
00:23:49,600 --> 00:23:55,200
 Brahmin the way to the the Brahma world,

252
00:23:55,200 --> 00:24:02,120
 which is actually inferior to becoming an Arahant. Then Sar

253
00:24:02,120 --> 00:24:06,080
ibuddha came to the Buddha and paid

254
00:24:06,080 --> 00:24:11,050
 respect to him and said, "Bhanti, Brahmin, Dhananjani, pay

255
00:24:11,050 --> 00:24:14,480
 respects to you." Then the Buddha said,

256
00:24:14,480 --> 00:24:18,600
 "Saribuddha, although you can do better, you just put him

257
00:24:18,600 --> 00:24:22,720
 on the road to Brahma world and you come back."

258
00:24:22,720 --> 00:24:32,720
 Now the text didn't say more about this, but the commentary

259
00:24:32,720 --> 00:24:35,280
 explains that

260
00:24:35,280 --> 00:24:47,650
 that was a hint Buddha gave to Saribuddha. That means Sarib

261
00:24:47,650 --> 00:24:48,160
uddha,

262
00:24:48,160 --> 00:24:55,360
 you go to him and preach to him again. So Saribuddha knew

263
00:24:55,360 --> 00:24:57,760
 or Saribuddha got that hint.

264
00:24:58,720 --> 00:25:06,560
 And so he went up to the Brahma world and gave a discourse

265
00:25:06,560 --> 00:25:10,480
 to the to the to Charnosundi who had

266
00:25:10,480 --> 00:25:15,940
 become a great Brahma. And it is we are not told whether

267
00:25:15,940 --> 00:25:20,720
 that great Brahma became an Arahant or

268
00:25:20,720 --> 00:25:25,790
 not, but I think he became an Arahant. But it is said that

269
00:25:25,790 --> 00:25:28,720
 from that time on whenever the Venerable

270
00:25:28,720 --> 00:25:34,940
 Saribuddha gave a Dhamma talk, even if it is just a verse

271
00:25:34,940 --> 00:25:38,480
 or four lines, he always includes the four

272
00:25:39,360 --> 00:25:48,030
 noble truths. This story, the Brahmin Jainusoni possessed

273
00:25:48,030 --> 00:25:51,680
 the capability to become an Arahant.

274
00:25:51,680 --> 00:25:57,930
 And he got to see the Buddha and his chief disciple Sarib

275
00:25:57,930 --> 00:26:02,720
uddha. But he did not become an

276
00:26:02,720 --> 00:26:08,480
 Arahant because there was lack of action on the part of the

277
00:26:08,480 --> 00:26:11,840
 Venerable Saribuddha. That means

278
00:26:11,840 --> 00:26:18,680
 Venerable Saribuddha could have taught him the way to

279
00:26:18,680 --> 00:26:20,720
 enlightenment,

280
00:26:20,720 --> 00:26:28,320
 the four foundations of mindfulness and Vipassana practice.

281
00:26:28,320 --> 00:26:30,800
 But instead he thought

282
00:26:31,440 --> 00:26:34,990
 the Brahmins are attached to the Brahma world and so he

283
00:26:34,990 --> 00:26:39,520
 taught him just the way to reach the

284
00:26:39,520 --> 00:26:45,890
 world of the Brahmins. So here Jainusoni fails to become an

285
00:26:45,890 --> 00:26:51,280
 Arahant because of lack of action,

286
00:26:51,280 --> 00:26:54,570
 that the right action on the part of the Venerable Saribudd

287
00:26:54,570 --> 00:27:00,080
ha. So we must remember these stories

288
00:27:01,040 --> 00:27:07,890
 and take lessons from these stories. That is, we must not

289
00:27:07,890 --> 00:27:11,280
 associate with bad people,

290
00:27:11,280 --> 00:27:15,980
 associate with people who will detract us from this

291
00:27:15,980 --> 00:27:18,880
 spiritual path and who will

292
00:27:18,880 --> 00:27:25,250
 take us to the ways of evil things and who will eventually

293
00:27:25,250 --> 00:27:28,240
 take us to the four waffle states.

294
00:27:29,440 --> 00:27:35,040
 And also we should see to it that

295
00:27:35,040 --> 00:27:50,520
 there is no fault on our part. If we are students, then we

296
00:27:50,520 --> 00:27:51,600
 will fulfill the

297
00:27:54,080 --> 00:28:00,370
 obligations of a student. We will make effort a student

298
00:28:00,370 --> 00:28:03,440
 should make and if we are DSHAs also,

299
00:28:03,440 --> 00:28:14,190
 we will try to lead the student to a right destination. So

300
00:28:14,190 --> 00:28:18,000
 we should keep these stories

301
00:28:18,000 --> 00:28:25,440
 in mind and try to avoid both the faults on the part of the

302
00:28:25,440 --> 00:28:29,440
 student and on the part of the teachers.

303
00:28:29,440 --> 00:28:36,420
 As teachers, say we will see to it that we give you the

304
00:28:36,420 --> 00:28:39,280
 right method, we teach you

305
00:28:40,160 --> 00:28:46,370
 the right method of practice and give you correct

306
00:28:46,370 --> 00:28:50,800
 instructions. And as students,

307
00:28:50,800 --> 00:28:57,820
 now we monks are both teachers and students. So as students

308
00:28:57,820 --> 00:29:04,000
, we should not be lacking in

309
00:29:04,000 --> 00:29:09,350
 a proper effort and we should follow the instructions

310
00:29:09,350 --> 00:29:10,960
 faithfully.

311
00:29:10,960 --> 00:29:19,400
 Now sometimes people come with what is called a pride in

312
00:29:19,400 --> 00:29:22,000
 their former practices or

313
00:29:22,000 --> 00:29:30,080
 our resistance. And so when they come to a center and then

314
00:29:30,080 --> 00:29:32,080
 they are unable to

315
00:29:33,120 --> 00:29:36,630
 get rid of that resistance and with that resistance they

316
00:29:36,630 --> 00:29:37,600
 cannot hope to

317
00:29:37,600 --> 00:29:42,470
 make any progress or they cannot hope to get any benefit

318
00:29:42,470 --> 00:29:44,320
 out of that practice.

319
00:29:44,320 --> 00:29:51,020
 So it is important that as students or as meditators, we

320
00:29:51,020 --> 00:29:54,560
 try to follow the instructions

321
00:29:55,120 --> 00:30:00,470
 faithfully. So when you go to a center, then you follow

322
00:30:00,470 --> 00:30:03,920
 these instructions given at that center

323
00:30:03,920 --> 00:30:09,160
 and do not have any resistance or arguments or whatever.

324
00:30:09,160 --> 00:30:12,160
 Because the instructions given at the

325
00:30:12,160 --> 00:30:18,390
 retreats, at the standards are built up through years and

326
00:30:18,390 --> 00:30:22,480
 so they are well tested instructions.

327
00:30:24,800 --> 00:30:29,970
 So it is important that as students, say we follow the

328
00:30:29,970 --> 00:30:33,280
 instructions faithfully and closely.

329
00:30:33,280 --> 00:30:39,760
 The teachers want their students to follow the instructions

330
00:30:39,760 --> 00:30:42,880
 closely and faithfully. And so

331
00:30:42,880 --> 00:30:52,320
 the students should make effort to follow the instructions.

332
00:30:52,320 --> 00:30:54,560
 As I said, instructions

333
00:30:54,560 --> 00:30:59,460
 given are accumulated through the years and so the

334
00:30:59,460 --> 00:31:03,120
 instructions are not just one teacher's

335
00:31:03,120 --> 00:31:06,470
 instruction. It is an instruction of the whole tradition

336
00:31:06,470 --> 00:31:12,720
 which is rooted in the teachings of

337
00:31:12,720 --> 00:31:18,160
 the Buddha. So when these instructions are faithfully

338
00:31:18,160 --> 00:31:22,400
 followed, then people begin to get

339
00:31:23,360 --> 00:31:28,960
 results from following these instructions. So it is

340
00:31:28,960 --> 00:31:33,600
 important that the teachers

341
00:31:33,600 --> 00:31:44,030
 give correct guidance in the practice of meditation and the

342
00:31:44,030 --> 00:31:46,000
 students or the meditators

343
00:31:46,560 --> 00:31:52,880
 should follow the guidance of the teachers. I want you to

344
00:31:52,880 --> 00:31:57,840
 remember the instructions and that is why I

345
00:31:57,840 --> 00:32:03,510
 say the instructions again and again. It may be boring to

346
00:32:03,510 --> 00:32:04,720
 some people,

347
00:32:04,720 --> 00:32:11,420
 but many people reported to me that it is good to hear the

348
00:32:11,420 --> 00:32:15,120
 instructions every now and then

349
00:32:15,840 --> 00:32:20,160
 so that they do not forget the instructions. So

350
00:32:20,160 --> 00:32:27,040
 instructions repeated are actually for the

351
00:32:27,040 --> 00:32:34,370
 benefit of the meditators. And if the instructions are too

352
00:32:34,370 --> 00:32:41,120
 often, please be patient and take them

353
00:32:41,120 --> 00:32:48,800
 as the presence given again and again. So by practicing, by

354
00:32:48,800 --> 00:32:51,360
 acting correctly towards the

355
00:32:51,360 --> 00:32:57,890
 instructions, that is by following the instructions, the

356
00:32:57,890 --> 00:33:02,480
 yogis will be able to get the results they

357
00:33:02,480 --> 00:33:06,770
 expect from the practice of meditation. During this retreat

358
00:33:06,770 --> 00:33:08,240
, first I talked about

359
00:33:09,840 --> 00:33:15,140
 the Dhamma heritage. So we who are practicing Vibhasana

360
00:33:15,140 --> 00:33:19,120
 meditation are at least making

361
00:33:19,120 --> 00:33:24,320
 the right effort to take the Dhamma heritage of the Buddha.

362
00:33:24,320 --> 00:33:31,360
 Talk about the burden, the burden of the aggregates, the

363
00:33:31,360 --> 00:33:33,120
 heavy burden we are carrying

364
00:33:35,120 --> 00:33:44,690
 every moment of our lives and also that we can at least not

365
00:33:44,690 --> 00:33:47,680
 pick up a new burden

366
00:33:47,680 --> 00:33:55,650
 by practicing Vibhasana meditation. And by the power of Vib

367
00:33:55,650 --> 00:33:59,440
hasana meditation, we will be able to

368
00:34:01,040 --> 00:34:06,210
 lay down the heavy burden once and for all. Last night I

369
00:34:06,210 --> 00:34:11,760
 talked about what to do when we have pain

370
00:34:11,760 --> 00:34:18,070
 or when we are sick. Although I say I talked about all

371
00:34:18,070 --> 00:34:22,320
 these are the teachings of the Buddha. So

372
00:34:25,280 --> 00:34:31,360
 what you heard during this retreat are all the teachings of

373
00:34:31,360 --> 00:34:32,480
 the Buddha

374
00:34:32,480 --> 00:34:40,770
 related by me. So we have respect for these teachings, for

375
00:34:40,770 --> 00:34:43,520
 these pieces of advice,

376
00:34:43,520 --> 00:34:48,940
 and we should follow the lessons or advices given in these

377
00:34:48,940 --> 00:34:53,440
 discourses. What heritage must we take?

378
00:34:53,680 --> 00:35:02,440
 Dhamma heritage or material heritage? Dhamma heritage. We

379
00:35:02,440 --> 00:35:07,040
 must at least make effort to take

380
00:35:07,040 --> 00:35:14,840
 the Dhamma heritage. We want to put down the burden. Do you

381
00:35:14,840 --> 00:35:19,520
 really want to put down the burden?

382
00:35:19,520 --> 00:35:29,140
 If so, we have to practice Vibhasana meditation. So by the

383
00:35:29,140 --> 00:35:31,200
 practice of Vibhasana meditation little

384
00:35:31,200 --> 00:35:38,270
 by little, we make our burden less heavy. And ultimately

385
00:35:38,270 --> 00:35:42,160
 when we are totally successful with

386
00:35:42,160 --> 00:35:47,310
 our practice, then we'll be able to put down the burden

387
00:35:47,310 --> 00:35:50,960
 altogether. And when we experience pain

388
00:35:50,960 --> 00:36:00,470
 or when we are sick, what will we do? Complain? Or angry

389
00:36:00,470 --> 00:36:07,040
 with the sickness or angry with people or

390
00:36:07,920 --> 00:36:14,470
 blaming others? No, we will not do any of these things. So

391
00:36:14,470 --> 00:36:20,720
 we will try not to let our minds be

392
00:36:20,720 --> 00:36:30,130
 sick, although our body is sick. So we will make effort to

393
00:36:30,130 --> 00:36:34,240
 take the Dhamma heritage of the Buddha.

394
00:36:35,600 --> 00:36:42,770
 We will try our best to lay down this heavy burden of five

395
00:36:42,770 --> 00:36:44,320
 aggregates.

396
00:36:44,320 --> 00:36:54,640
 And although our body is sick, we will see to it that our

397
00:36:54,640 --> 00:36:56,960
 mind is not sick.

398
00:36:58,240 --> 00:37:09,360
 So may we all be successful following these determinations

399
00:37:09,360 --> 00:37:19,600
 and achieve our goal in this very life.

400
00:37:20,240 --> 00:37:20,560
 Thank you.

