1
00:00:00,000 --> 00:00:07,000
 Welcome to Ask a Monk. Today's question comes from Aisaneen

2
00:00:07,000 --> 00:00:07,000
.

3
00:00:07,000 --> 00:00:11,240
 I often find myself daydreaming, with no hope of getting

4
00:00:11,240 --> 00:00:13,000
 the situation I'm visualizing,

5
00:00:13,000 --> 00:00:17,230
 as the enjoyment from the fantasy alone can seem not to

6
00:00:17,230 --> 00:00:20,000
 carry the drawbacks of chasing material things.

7
00:00:20,000 --> 00:00:26,000
 Can you explain your philosophical approach to daydreams?

8
00:00:26,000 --> 00:00:39,670
 It's a bit of a strange sort of idea that just because

9
00:00:39,670 --> 00:00:44,000
 something's immaterial means that it doesn't have the same

10
00:00:44,000 --> 00:00:47,000
 drawbacks of chasing material things.

11
00:00:47,000 --> 00:00:52,670
 Obviously chasing after things is craving, is a clinging to

12
00:00:52,670 --> 00:00:58,100
 them, that there's not the impartial, balanced state of

13
00:00:58,100 --> 00:00:59,000
 mind.

14
00:00:59,000 --> 00:01:01,580
 The truth of the matter is that you have gotten what you

15
00:01:01,580 --> 00:01:06,000
 want, and that is the experience in the daydream.

16
00:01:06,000 --> 00:01:18,250
 Reality, according to Buddhism, is experiential. The idea

17
00:01:18,250 --> 00:01:20,120
 that something is immaterial is actually an illusion, I

18
00:01:20,120 --> 00:01:21,000
 suppose,

19
00:01:21,000 --> 00:01:24,720
 because everything is really immaterial. When you are

20
00:01:24,720 --> 00:01:28,290
 attached to a sight, you're attached to the seeing of the

21
00:01:28,290 --> 00:01:29,000
 object.

22
00:01:29,000 --> 00:01:32,100
 When you are attached to a sound, you're attached to the

23
00:01:32,100 --> 00:01:35,290
 hearing of the object, the smelling, the tasting, the

24
00:01:35,290 --> 00:01:38,000
 feeling, and/or the thinking.

25
00:01:38,000 --> 00:01:41,300
 So when you're daydreaming, there are a combination of

26
00:01:41,300 --> 00:01:44,000
 things. There's the thinking, there's maybe the seeing,

27
00:01:44,000 --> 00:01:48,480
 there's maybe even a concept of hearing something, and so

28
00:01:48,480 --> 00:01:49,000
 on.

29
00:01:49,000 --> 00:01:52,610
 The point is not the stimulus, the point is the feeling

30
00:01:52,610 --> 00:01:57,000
 that comes from it, which is a pleasure and an enjoyment.

31
00:01:57,000 --> 00:02:04,160
 The enjoyment of it, the liking of it, which takes one out

32
00:02:04,160 --> 00:02:06,000
 of balance.

33
00:02:06,000 --> 00:02:13,250
 If you admit that "material things" have drawbacks, then

34
00:02:13,250 --> 00:02:16,350
 there's no reason to think that an immaterial object would

35
00:02:16,350 --> 00:02:18,000
 not have the same drawbacks,

36
00:02:18,000 --> 00:02:21,650
 since it's the very same clinging. To say that you're not

37
00:02:21,650 --> 00:02:24,000
 getting, suppose you daydream about food,

38
00:02:24,000 --> 00:02:26,170
 and to say that you're not getting the food is really inc

39
00:02:26,170 --> 00:02:27,000
onsequential.

40
00:02:27,000 --> 00:02:34,610
 There is an attainment of a pleasurable stimulus and the

41
00:02:34,610 --> 00:02:38,000
 arising of the pleasure based on that stimulus,

42
00:02:38,000 --> 00:02:43,510
 and then the liking, the enjoyment of it, which pulls one

43
00:02:43,510 --> 00:02:46,000
 to one side out of balance,

44
00:02:46,000 --> 00:02:51,660
 and causes one to snap back into a state of upset and

45
00:02:51,660 --> 00:02:58,000
 suffering when one is without the objects of desire.

46
00:02:58,000 --> 00:03:03,170
 So I don't think there's any difference there. I hope that

47
00:03:03,170 --> 00:03:05,000
 helps to explain.

48
00:03:05,000 --> 00:03:10,060
 The idea of getting or not getting in this case is not a

49
00:03:10,060 --> 00:03:14,000
 factor, because there's the attainment of pleasure.

50
00:03:14,000 --> 00:03:16,000
 Okay, thanks.

