1
00:00:00,000 --> 00:00:19,040
 Good evening everyone.

2
00:00:19,040 --> 00:00:32,560
 Welcome to our evening meeting.

3
00:00:32,560 --> 00:00:37,960
 Saturday night, we're just about full up.

4
00:00:37,960 --> 00:00:42,670
 If you weren't able to make it and you're watching this on

5
00:00:42,670 --> 00:00:44,840
 YouTube, there's always the

6
00:00:44,840 --> 00:00:49,560
 audio stream if you want to follow live.

7
00:00:49,560 --> 00:00:57,460
 I got a limit here of 20 people, so not everyone can join

8
00:00:57,460 --> 00:01:00,560
 us in virtual reality.

9
00:01:00,560 --> 00:01:10,400
 We have two meditators here in Hamilton in somewhat less

10
00:01:10,400 --> 00:01:16,560
 virtual reality, still virtual,

11
00:01:16,560 --> 00:01:23,880
 still not ultimate reality.

12
00:01:23,880 --> 00:01:33,950
 In ultimate reality, there is only what we call paramatad

13
00:01:33,950 --> 00:01:35,240
ham.

14
00:01:35,240 --> 00:01:38,160
 We translate paramatadham as ultimate reality.

15
00:01:38,160 --> 00:01:42,720
 Paramata, paramat means highest.

16
00:01:42,720 --> 00:01:51,120
 Ata means meaning or in the highest sense.

17
00:01:51,120 --> 00:02:00,030
 I had two visitors today from Toronto and I was explaining

18
00:02:00,030 --> 00:02:04,680
 to them the four foundations

19
00:02:04,680 --> 00:02:06,870
 of mindfulness and then I came to the fourth one and I said

20
00:02:06,870 --> 00:02:08,080
, "Well, dhamma is a little

21
00:02:08,080 --> 00:02:12,560
 bit hard to explain."

22
00:02:12,560 --> 00:02:17,120
 And they nodded and said, "Yes, we know."

23
00:02:17,120 --> 00:02:22,360
 Or they made a sign of the affirmative.

24
00:02:22,360 --> 00:02:26,680
 Dhamma is a difficult word.

25
00:02:26,680 --> 00:02:29,240
 It's used in many different ways.

26
00:02:29,240 --> 00:02:31,610
 It's funny because before the Buddha came along, it wasn't

27
00:02:31,610 --> 00:02:32,840
 such a complicated word.

28
00:02:32,840 --> 00:02:34,080
 It's not actually difficult.

29
00:02:34,080 --> 00:02:39,280
 It's just complicated in Buddhism.

30
00:02:39,280 --> 00:02:44,640
 Or by the time the Buddha came around.

31
00:02:44,640 --> 00:02:51,200
 So a dhamma is something that one holds, something that is

32
00:02:51,200 --> 00:02:52,200
 held.

33
00:02:52,200 --> 00:03:02,800
 Dhar is the root, means to hold or to carry.

34
00:03:02,800 --> 00:03:09,660
 So it came to take on a figurative, in a figurative sense,

35
00:03:09,660 --> 00:03:14,280
 the idea of holding on to some doctrine

36
00:03:14,280 --> 00:03:23,760
 so there was the katiya dhamma and the brahmana dhamma.

37
00:03:23,760 --> 00:03:33,480
 That would be the code of the nobles and the priests and

38
00:03:33,480 --> 00:03:41,280
 then the merchants, the vasa they

39
00:03:41,280 --> 00:03:43,280
 called.

40
00:03:43,280 --> 00:03:48,080
 The merchant class had their own dhamma.

41
00:03:48,080 --> 00:03:52,820
 And it was sort of a code of social order.

42
00:03:52,820 --> 00:03:58,460
 Those who subscribed, those who preached this code were

43
00:03:58,460 --> 00:04:01,800
 intent upon keeping social order

44
00:04:01,800 --> 00:04:07,570
 or maintaining a sort of aristocracy or a caste system

45
00:04:07,570 --> 00:04:10,320
 whereby the nobles ruled the

46
00:04:10,320 --> 00:04:15,560
 country and they had their duties as kings and rulers and

47
00:04:15,560 --> 00:04:16,880
 warriors.

48
00:04:16,880 --> 00:04:20,490
 And the brahmanas had their role as priests to perform

49
00:04:20,490 --> 00:04:22,760
 religious ceremonies, perpetuate

50
00:04:22,760 --> 00:04:25,960
 the gods.

51
00:04:25,960 --> 00:04:35,360
 The merchants had their merchant code, their honor in

52
00:04:35,360 --> 00:04:39,920
 dealing with each other in buying

53
00:04:39,920 --> 00:04:41,400
 and selling and trading.

54
00:04:41,400 --> 00:04:45,740
 And then of course the lowest class was a means of

55
00:04:45,740 --> 00:04:47,600
 reminding them that well this is

56
00:04:47,600 --> 00:04:50,480
 just due to your karma.

57
00:04:50,480 --> 00:04:56,470
 You were born into this lower class and you should just

58
00:04:56,470 --> 00:04:58,000
 accept it.

59
00:04:58,000 --> 00:05:03,000
 Wai-sia is the Sanskrit, wai-sai is Pali.

60
00:05:03,000 --> 00:05:11,850
 The wai-sai are the merchants and the sudas are the lower

61
00:05:11,850 --> 00:05:13,440
 class.

62
00:05:13,440 --> 00:05:20,480
 So it's another means of telling the poor people that well

63
00:05:20,480 --> 00:05:22,360
 it's just part of your lot

64
00:05:22,360 --> 00:05:27,600
 in life to be poor and hungry all the time.

65
00:05:27,600 --> 00:05:29,560
 And so this works as a sort of a social order.

66
00:05:29,560 --> 00:05:33,120
 Anyway the point was dharmma was this idea.

67
00:05:33,120 --> 00:05:37,730
 And so when the Buddha came around, well there were

68
00:05:37,730 --> 00:05:41,080
 actually quite a number of religious

69
00:05:41,080 --> 00:05:44,430
 teachers who didn't really buy into or maybe never had

70
00:05:44,430 --> 00:05:46,600
 bought into the social structure

71
00:05:46,600 --> 00:05:52,240
 of rulers and kings and merchants and so on and priests.

72
00:05:52,240 --> 00:05:55,160
 They didn't buy into the religion of the brahmanas.

73
00:05:55,160 --> 00:06:01,960
 And so they would go off into the forest and preach their

74
00:06:01,960 --> 00:06:04,000
 own dharmma.

75
00:06:04,000 --> 00:06:07,120
 Because they would see that these other dharmas were

76
00:06:07,120 --> 00:06:09,560
 actually quite corrupt in many ways.

77
00:06:09,560 --> 00:06:15,250
 The dharmas of the ksattka-tiyas meant they could kill with

78
00:06:15,250 --> 00:06:17,680
 impen-t-i-m-p-e-t.

79
00:06:17,680 --> 00:06:24,400
 Impunity, kill with impunity as long as they were doing it.

80
00:06:24,400 --> 00:06:29,200
 In accordance with the rules of war killing was not wrong.

81
00:06:29,200 --> 00:06:36,210
 But the brahmanas were greedy with impunity, they were

82
00:06:36,210 --> 00:06:40,320
 cultivating all these myths and

83
00:06:40,320 --> 00:06:52,410
 beliefs based on unsubstantiated claims about reality and

84
00:06:52,410 --> 00:06:54,960
 hoarding money and hoarding possessions

85
00:06:54,960 --> 00:07:00,470
 and talking about brahma and holiness and yet having wives

86
00:07:00,470 --> 00:07:03,680
 and female slaves and concubines

87
00:07:03,680 --> 00:07:06,200
 and prostitutes and so on.

88
00:07:06,200 --> 00:07:13,560
 And eating and drinking and luxury and reaping the fruits

89
00:07:13,560 --> 00:07:15,960
 of sacrifices.

90
00:07:15,960 --> 00:07:19,520
 And the vesas would be all corrupt in their dealings with

91
00:07:19,520 --> 00:07:21,400
 each other, all about money

92
00:07:21,400 --> 00:07:32,080
 and capitalism, striving for a bigger piece of the pie.

93
00:07:32,080 --> 00:07:34,900
 And then the sudha as well, the idea that this was their d

94
00:07:34,900 --> 00:07:36,520
harmma to suffer miserably,

95
00:07:36,520 --> 00:07:44,820
 well there's had these rich people lounged around in op

96
00:07:44,820 --> 00:07:46,520
ulence.

97
00:07:46,520 --> 00:07:49,950
 There were I think a number of teachers at the time of the

98
00:07:49,950 --> 00:07:51,720
 Buddha who for this and for

99
00:07:51,720 --> 00:07:55,190
 other reasons, some just for the reasons that they wanted

100
00:07:55,190 --> 00:07:57,320
 to be teachers on their own, had

101
00:07:57,320 --> 00:08:00,330
 a desire to be famous, they would teach an alternative

102
00:08:00,330 --> 00:08:01,120
 doctrine.

103
00:08:01,120 --> 00:08:05,910
 But often it was because they saw that society was quite

104
00:08:05,910 --> 00:08:08,160
 corrupt in its nature.

105
00:08:08,160 --> 00:08:12,960
 The Buddha was one of these teachers.

106
00:08:12,960 --> 00:08:19,880
 He left home seeing that this dharmma is quite corrupt.

107
00:08:19,880 --> 00:08:23,870
 He was set to become the king but he saw this can't save me

108
00:08:23,870 --> 00:08:25,440
 from suffering.

109
00:08:25,440 --> 00:08:30,250
 This is only setting me up for great attachment and

110
00:08:30,250 --> 00:08:34,800
 addiction, setting me up for great suffering

111
00:08:34,800 --> 00:08:40,800
 and its luxury.

112
00:08:40,800 --> 00:08:47,560
 And so he left home to find a higher dharmma or dharmma we

113
00:08:47,560 --> 00:08:50,200
 would say in Pali.

114
00:08:50,200 --> 00:08:52,920
 And so he went to two different teachers and if you know

115
00:08:52,920 --> 00:08:54,560
 the story of the Buddha, they

116
00:08:54,560 --> 00:08:57,530
 taught their dharmma and he asked them what is your dharmma

117
00:08:57,530 --> 00:08:59,360
 and they taught it and he mastered

118
00:08:59,360 --> 00:09:02,600
 it and he said well this dharmma is still limited.

119
00:09:02,600 --> 00:09:08,520
 And so eventually he came up with his own dharmma.

120
00:09:08,520 --> 00:09:10,740
 But this is where it starts to get complicated is that the

121
00:09:10,740 --> 00:09:12,120
 Buddha doesn't claim to have his

122
00:09:12,120 --> 00:09:14,000
 own dharmma.

123
00:09:14,000 --> 00:09:21,240
 He claims to have realized the dharmma in a different sense

124
00:09:21,240 --> 00:09:23,120
 of the word.

125
00:09:23,120 --> 00:09:32,080
 And so here the meaning is that which holds or that which

126
00:09:32,080 --> 00:09:36,840
 is held up to investigation when

127
00:09:36,840 --> 00:09:48,270
 you learn about reality, these teachings, these truths hold

128
00:09:48,270 --> 00:09:49,400
 up.

129
00:09:49,400 --> 00:09:56,400
 So in Buddhism dharmma became to be known as reality.

130
00:09:56,400 --> 00:10:02,680
 And so it's used interchange, it's used in two different

131
00:10:02,680 --> 00:10:03,680
 ways.

132
00:10:03,680 --> 00:10:08,500
 There is the dharmma, the paramattadhamma, these realities

133
00:10:08,500 --> 00:10:10,320
 that the Buddha realized and

134
00:10:10,320 --> 00:10:17,710
 then there's the sadhamma which is the teachings of the

135
00:10:17,710 --> 00:10:21,680
 Buddha and they're distinct.

136
00:10:21,680 --> 00:10:24,900
 And it became even more complicated when he started using

137
00:10:24,900 --> 00:10:26,520
 the reality's idea in terms

138
00:10:26,520 --> 00:10:29,960
 of ideas.

139
00:10:29,960 --> 00:10:35,060
 So we have dharmaramanas, anything that is the object of

140
00:10:35,060 --> 00:10:38,800
 the mind is called the dharmaramanas,

141
00:10:38,800 --> 00:10:42,840
 an aramana is object.

142
00:10:42,840 --> 00:10:45,600
 So you have dharmas.

143
00:10:45,600 --> 00:10:48,320
 And so when it comes to explain the fourth foundation of

144
00:10:48,320 --> 00:10:49,800
 mindfulness I haven't heard

145
00:10:49,800 --> 00:10:52,240
 a satisfactory answer yet.

146
00:10:52,240 --> 00:10:55,740
 What the word dharma means there, I've asked monks, I've

147
00:10:55,740 --> 00:10:57,720
 asked lay scholars, I even asked

148
00:10:57,720 --> 00:11:11,120
 my teacher and he just said it's dharmma, he said something

149
00:11:11,120 --> 00:11:14,960
 quite simple.

150
00:11:14,960 --> 00:11:17,410
 I think he said it's tamachat because in Thai they have

151
00:11:17,410 --> 00:11:19,520
 this one, in Pali there's tamachati,

152
00:11:19,520 --> 00:11:20,520
 it's just nature.

153
00:11:20,520 --> 00:11:26,500
 So the dharmas are nature but in the context of the fourth

154
00:11:26,500 --> 00:11:29,680
 foundation of mindfulness I

155
00:11:29,680 --> 00:11:33,550
 think it just means the teachings, various teachings,

156
00:11:33,550 --> 00:11:36,280
 various important specific teachings

157
00:11:36,280 --> 00:11:43,280
 that are a part of the meditator's path.

158
00:11:43,280 --> 00:11:51,800
 Anyway, tonight I wanted to talk about the paramattadhammas

159
00:11:51,800 --> 00:11:56,800
, talk about this word dhamma,

160
00:11:56,800 --> 00:12:00,490
 because this is what we're trying to see, we have this

161
00:12:00,490 --> 00:12:02,840
 phrase when the Buddha was about

162
00:12:02,840 --> 00:12:15,550
 to pass away he said, "Whatever person dhammanudhampatipan

163
00:12:15,550 --> 00:12:19,280
au, anyone who wishes to pay homage to the Buddha

164
00:12:19,280 --> 00:12:28,840
 should practice the dhamma in line with the dhamma."

165
00:12:28,840 --> 00:12:37,770
 So this is interpreted to mean practicing the teachings to

166
00:12:37,770 --> 00:12:41,120
 realize the truth.

167
00:12:41,120 --> 00:12:45,050
 So here we have the use of the two different versions of

168
00:12:45,050 --> 00:12:48,320
 the word dhamma, dhammanudhamma,

169
00:12:48,320 --> 00:12:51,400
 practicing the dhamma in order to realize the dhamma.

170
00:12:51,400 --> 00:12:55,360
 It means practicing the four satipatanas in order to

171
00:12:55,360 --> 00:12:58,280
 realize nibhana, in order to realize

172
00:12:58,280 --> 00:13:11,160
 the end of suffering, or in order to see the dhammas.

173
00:13:11,160 --> 00:13:17,690
 And so it's this idea of seeing the dhammas that we focus

174
00:13:17,690 --> 00:13:21,800
 on in our practice of satipatana.

175
00:13:21,800 --> 00:13:27,550
 All four satipatanas involve seeing the dhammas, seeing

176
00:13:27,550 --> 00:13:28,840
 reality.

177
00:13:28,840 --> 00:13:32,450
 When we focus on the body we're not actually concerned

178
00:13:32,450 --> 00:13:34,840
 about the body, we're concerned

179
00:13:34,840 --> 00:13:41,200
 with dhammas that are physical.

180
00:13:41,200 --> 00:13:44,380
 We're concerned with experiences, things that arise and

181
00:13:44,380 --> 00:13:48,040
 cease moment after moment.

182
00:13:48,040 --> 00:13:53,290
 Movements of the body, the sensation of moving, touching,

183
00:13:53,290 --> 00:13:55,800
 the sensation of heat and cold,

184
00:13:55,800 --> 00:14:01,600
 hard, soft.

185
00:14:01,600 --> 00:14:04,270
 The tension in the various parts of the body, for example,

186
00:14:04,270 --> 00:14:05,920
 in the stomach, which is an object

187
00:14:05,920 --> 00:14:14,120
 of meditation that we use.

188
00:14:14,120 --> 00:14:20,290
 Each of these is a dhamma because it holds up to

189
00:14:20,290 --> 00:14:23,000
 investigation.

190
00:14:23,000 --> 00:14:27,360
 The idea of whether the body exists is just a matter of

191
00:14:27,360 --> 00:14:30,240
 opinion, it's just a concept.

192
00:14:30,240 --> 00:14:33,320
 When we look around in the room we see other things and

193
00:14:33,320 --> 00:14:35,240
 bodies, when we look down we see

194
00:14:35,240 --> 00:14:36,240
 our body.

195
00:14:36,240 --> 00:14:42,400
 But whether it's actually there or not, it's just a belief,

196
00:14:42,400 --> 00:14:44,680
 just a conjecture.

197
00:14:44,680 --> 00:14:48,640
 We could just be in virtual reality.

198
00:14:48,640 --> 00:14:51,720
 Instead of actually sitting here we might all be hooked up.

199
00:14:51,720 --> 00:14:54,460
 In fact this might all be a figment of our imagination.

200
00:14:54,460 --> 00:14:57,680
 I might be the only person in existence.

201
00:14:57,680 --> 00:15:04,280
 I don't know whether any of you actually exist.

202
00:15:04,280 --> 00:15:07,830
 But what I do know is that there is sensations, there is

203
00:15:07,830 --> 00:15:08,720
 tension.

204
00:15:08,720 --> 00:15:13,970
 When I make a fist there's tension, when I breathe in there

205
00:15:13,970 --> 00:15:15,360
's tension.

206
00:15:15,360 --> 00:15:22,340
 When I'm in a hot room there's heat, when I'm out in the

207
00:15:22,340 --> 00:15:25,260
 cold there's cold.

208
00:15:25,260 --> 00:15:28,200
 When I stand on the hard floor there's hardness, when I sit

209
00:15:28,200 --> 00:15:30,280
 on a soft cushion there's softness,

210
00:15:30,280 --> 00:15:32,700
 there's these experiences.

211
00:15:32,700 --> 00:15:40,710
 So they're dhammas, they actually exist for sure because

212
00:15:40,710 --> 00:15:43,480
 they're actually experiences.

213
00:15:43,480 --> 00:15:50,630
 When vedana is all of the feelings that we experience

214
00:15:50,630 --> 00:15:54,400
 whether painful or pleasant.

215
00:15:54,400 --> 00:15:58,440
 Again, these arise and cease.

216
00:15:58,440 --> 00:16:02,920
 But they're real.

217
00:16:02,920 --> 00:16:07,820
 They're real not in the sense that they exist as an entity

218
00:16:07,820 --> 00:16:10,720
 that comes and goes but is always

219
00:16:10,720 --> 00:16:11,720
 existent.

220
00:16:11,720 --> 00:16:16,880
 No, they come from cessation and they go back to cessation.

221
00:16:16,880 --> 00:16:19,120
 They don't exist permanently.

222
00:16:19,120 --> 00:16:21,560
 But during the moment of experience they're real.

223
00:16:21,560 --> 00:16:28,880
 That's what it means to be a dhamma.

224
00:16:28,880 --> 00:16:32,400
 So when we watch the pain we see it arising and ceasing.

225
00:16:32,400 --> 00:16:41,400
 When we feel happy we see it arising and ceasing.

226
00:16:41,400 --> 00:16:46,240
 When we watch the mind we see our thoughts, thoughts

227
00:16:46,240 --> 00:16:48,520
 arising and ceasing.

228
00:16:48,520 --> 00:16:51,050
 And finally the dhammas but here dhammas in the sense of

229
00:16:51,050 --> 00:16:51,720
 teaching.

230
00:16:51,720 --> 00:16:54,400
 So we have the niwarana dhamma.

231
00:16:54,400 --> 00:16:57,960
 These are dhammas that the Buddha taught.

232
00:16:57,960 --> 00:16:58,960
 And they're also real.

233
00:16:58,960 --> 00:17:02,920
 They're also dhammas in the sense of being real.

234
00:17:02,920 --> 00:17:06,680
 Liking, disliking, drowsiness, distraction, doubt.

235
00:17:06,680 --> 00:17:10,040
 These are experienced.

236
00:17:10,040 --> 00:17:12,200
 Seeing, hearing, smelling, tasting, feeling.

237
00:17:12,200 --> 00:17:16,200
 These are dhammas.

238
00:17:16,200 --> 00:17:25,080
 And so on.

239
00:17:25,080 --> 00:17:28,760
 The importance of this is that there's a distinct

240
00:17:28,760 --> 00:17:40,720
 difference between dhammas and concepts.

241
00:17:40,720 --> 00:17:45,840
 When we spend our time looking at concepts, when we see

242
00:17:45,840 --> 00:17:48,600
 people and places and things,

243
00:17:48,600 --> 00:17:51,700
 we're able to cultivate all sorts of ideas and views and

244
00:17:51,700 --> 00:17:53,680
 beliefs about these things because

245
00:17:53,680 --> 00:17:57,380
 they don't actually exist.

246
00:17:57,380 --> 00:18:01,120
 When we see a person we can have an idea of what they're

247
00:18:01,120 --> 00:18:01,800
 like.

248
00:18:01,800 --> 00:18:06,330
 We can build up a whole narrative about, "Oh there's that

249
00:18:06,330 --> 00:18:07,360
 person."

250
00:18:07,360 --> 00:18:09,000
 We might base it on experiences.

251
00:18:09,000 --> 00:18:13,320
 We might just base it on our own delusions.

252
00:18:13,320 --> 00:18:14,320
 And often do.

253
00:18:14,320 --> 00:18:17,920
 We get into a relationship with someone and we have this

254
00:18:17,920 --> 00:18:20,080
 very powerful concept of what

255
00:18:20,080 --> 00:18:31,520
 they're like and then they act in a different way and we

256
00:18:31,520 --> 00:18:35,400
 get very upset.

257
00:18:35,400 --> 00:18:39,480
 Or after some time our mind changes and our concept of them

258
00:18:39,480 --> 00:18:41,440
 is no longer pleasing to us

259
00:18:41,440 --> 00:18:50,080
 and we become bored and lose interest.

260
00:18:50,080 --> 00:18:53,920
 Because concepts don't play out in ultimate reality.

261
00:18:53,920 --> 00:19:05,040
 Reality continues according to very real patterns, laws.

262
00:19:05,040 --> 00:19:09,520
 But concepts don't.

263
00:19:09,520 --> 00:19:12,690
 So by living in a world of concepts we build up all these

264
00:19:12,690 --> 00:19:15,200
 delusions and based on our partialities

265
00:19:15,200 --> 00:19:19,880
 we build up ideas of what is good, what is bad.

266
00:19:19,880 --> 00:19:22,000
 That have no basis in ultimate reality.

267
00:19:22,000 --> 00:19:26,570
 We think of certain foods as good, certain sights as good,

268
00:19:26,570 --> 00:19:28,660
 certain sounds as good and

269
00:19:28,660 --> 00:19:34,220
 others as bad.

270
00:19:34,220 --> 00:19:42,330
 And based on these beliefs and these partialities we strive

271
00:19:42,330 --> 00:19:47,280
 to accumulate or we strive to escape

272
00:19:47,280 --> 00:19:49,960
 and we act in ways that are totally unrelated to ultimate

273
00:19:49,960 --> 00:19:50,600
 reality.

274
00:19:50,600 --> 00:19:59,460
 They're based on this illusory sense of the way things are

275
00:19:59,460 --> 00:20:03,640
 and how the world works.

276
00:20:03,640 --> 00:20:07,170
 It's like blind people, living blind in a house and

277
00:20:07,170 --> 00:20:09,560
 imagining what everything looks

278
00:20:09,560 --> 00:20:11,560
 like.

279
00:20:11,560 --> 00:20:14,190
 Meditation is like turning on the lights and suddenly you

280
00:20:14,190 --> 00:20:14,560
 see.

281
00:20:14,560 --> 00:20:17,350
 As you practice meditation you suddenly see that these

282
00:20:17,350 --> 00:20:19,040
 things that you were clinging to

283
00:20:19,040 --> 00:20:24,310
 are not actually yours, not actually satisfying or

284
00:20:24,310 --> 00:20:27,440
 beneficial or worth clinging to in any

285
00:20:27,440 --> 00:20:33,480
 way.

286
00:20:33,480 --> 00:20:37,010
 You don't see anything very esoteric or profound but what's

287
00:20:37,010 --> 00:20:39,240
 profound is the simplicity of it,

288
00:20:39,240 --> 00:20:45,840
 is that by seeing the dhammas, by seeing everything arise

289
00:20:45,840 --> 00:20:52,440
 and you straighten out all this crookedness

290
00:20:52,440 --> 00:21:01,440
 in the mind, all the attachments and addictions and all the

291
00:21:01,440 --> 00:21:05,400
 aversions and delusions and your

292
00:21:05,400 --> 00:21:10,720
 mind becomes pure.

293
00:21:10,720 --> 00:21:14,540
 There's a great power to reality in this way but it's

294
00:21:14,540 --> 00:21:18,680
 important you have to make this shift.

295
00:21:18,680 --> 00:21:31,420
 The mind has to come to understand reality in terms of dham

296
00:21:31,420 --> 00:21:36,560
mas or experiences.

297
00:21:36,560 --> 00:21:38,750
 It's important that when we think of the Buddha dhamma we

298
00:21:38,750 --> 00:21:41,240
're not actually just thinking of

299
00:21:41,240 --> 00:21:44,700
 the Buddha's teachings, learning them all and getting an

300
00:21:44,700 --> 00:21:46,520
 intellectual sense of them.

301
00:21:46,520 --> 00:21:51,370
 But we use the teachings in order to see the truth, in

302
00:21:51,370 --> 00:21:54,360
 order to see reality for what it

303
00:21:54,360 --> 00:21:58,170
 is which really is transformative, which truly does free us

304
00:21:58,170 --> 00:22:00,640
 from addiction and aversion and

305
00:22:00,640 --> 00:22:06,240
 ego and clinging and all that.

306
00:22:06,240 --> 00:22:11,730
 So there you go, there's the dhamma for tonight,

307
00:22:11,730 --> 00:22:17,280
 encouragement for us all to strive to understand

308
00:22:17,280 --> 00:22:34,380
 the dhammas, to understand reality on an experiential level

309
00:22:34,380 --> 00:22:35,400
.

310
00:22:35,400 --> 00:22:36,400
 Everyone has any questions?

311
00:22:36,400 --> 00:22:37,400
 I'm happy to answer.

312
00:22:37,400 --> 00:23:02,400
 Thank you.

313
00:23:02,400 --> 00:23:31,440
 If there are no questions then thank you all for coming.

314
00:23:32,480 --> 00:23:35,480
 Good to see you all.

315
00:23:35,480 --> 00:23:37,360
 I'm wishing you all a good night.

316
00:23:37,360 --> 00:23:38,360
 Thank you.

317
00:23:38,360 --> 00:23:39,360
 Thank you.

318
00:23:39,360 --> 00:23:40,360
 Thank you.

319
00:23:40,360 --> 00:23:41,360
 Thank you.

320
00:23:41,360 --> 00:23:42,360
 Thank you.

