1
00:00:00,000 --> 00:00:17,560
 Okay, hello everyone, we're broadcasting live.

2
00:00:17,560 --> 00:00:25,600
 Today we're going to try to do a Dhammapada video.

3
00:00:25,600 --> 00:00:52,480
 I think everything's working.

4
00:00:52,480 --> 00:01:14,960
 Let's get set up here.

5
00:01:14,960 --> 00:01:18,680
 Hello and welcome back to our study of the Dhammapada.

6
00:01:18,680 --> 00:01:25,680
 Today we continue on with verse 128 which reads as follows.

7
00:01:25,680 --> 00:01:53,880
 Which means, very similar to our last verse.

8
00:01:53,880 --> 00:02:03,580
 Not in the middle, not up on a mountain, not in the middle

9
00:02:03,580 --> 00:02:06,240
 of the sky, or in the middle

10
00:02:06,240 --> 00:02:12,160
 of, not at the end of the sky, right?

11
00:02:12,160 --> 00:02:20,230
 Not in the middle of the ocean, not in a cave in the

12
00:02:20,230 --> 00:02:26,800
 mountain, if one plunges into the cave

13
00:02:26,800 --> 00:02:29,000
 in the mountain.

14
00:02:29,000 --> 00:02:39,680
 One cannot find a place on earth, where one could stand.

15
00:02:39,680 --> 00:02:55,920
 And not be overcome by death.

16
00:02:55,920 --> 00:03:02,960
 Another fairly well-known story, part of the Buddha's life.

17
00:03:02,960 --> 00:03:06,000
 And it'd be nice if there were more stories about the

18
00:03:06,000 --> 00:03:07,160
 Buddha's life.

19
00:03:07,160 --> 00:03:10,880
 I've talked about this several times how, well that's

20
00:03:10,880 --> 00:03:13,120
 basically what we're doing here,

21
00:03:13,120 --> 00:03:15,500
 we're telling the story of the Buddha's life piece by piece

22
00:03:15,500 --> 00:03:15,720
.

23
00:03:15,720 --> 00:03:20,310
 But I think part of why several, many people are fascinated

24
00:03:20,310 --> 00:03:22,520
 by these stories is because

25
00:03:22,520 --> 00:03:26,440
 they're part of the Buddha story that people have never

26
00:03:26,440 --> 00:03:27,200
 heard.

27
00:03:27,200 --> 00:03:30,640
 And that's because the most famous Buddha stories are not

28
00:03:30,640 --> 00:03:32,320
 really about the Buddha at

29
00:03:32,320 --> 00:03:33,320
 all.

30
00:03:33,320 --> 00:03:34,320
 They're about the Bodhisattva.

31
00:03:34,320 --> 00:03:37,320
 They're about the quest to become a Buddha.

32
00:03:37,320 --> 00:03:45,120
 But the 45 years that the, where the Buddha actually taught

33
00:03:45,120 --> 00:03:47,240
 is neglected.

34
00:03:47,240 --> 00:03:50,570
 And this is partly because of the types of Buddhism that

35
00:03:50,570 --> 00:03:52,760
 have spread and become popular.

36
00:03:52,760 --> 00:03:58,440
 But it's a shame because he didn't spend 45 years teaching.

37
00:03:58,440 --> 00:04:04,820
 And if the stories are any indication, lots of exciting

38
00:04:04,820 --> 00:04:06,920
 things happen.

39
00:04:06,920 --> 00:04:10,820
 One of the exciting things that happened was his son, who

40
00:04:10,820 --> 00:04:13,480
 if you remember from the Bodhisattva

41
00:04:13,480 --> 00:04:18,560
 story that we all hear about, the Buddha left behind, some

42
00:04:18,560 --> 00:04:20,920
 versions say in the middle of

43
00:04:20,920 --> 00:04:29,860
 the night, and his wife, who he left behind as a prince,

44
00:04:29,860 --> 00:04:35,280
 eventually became, eventually

45
00:04:35,280 --> 00:04:38,680
 became monks themselves.

46
00:04:38,680 --> 00:04:42,800
 So Rahula became a novice monk.

47
00:04:42,800 --> 00:04:48,920
 And Yosodra became a bhikkhuni.

48
00:04:48,920 --> 00:04:53,820
 And lots of the Sakyans, the relatives of the Buddha also

49
00:04:53,820 --> 00:04:55,320
 became monks.

50
00:04:55,320 --> 00:05:03,880
 There was Anurunda and Ananda and Upali and Devadatta.

51
00:05:03,880 --> 00:05:08,880
 And many, many more.

52
00:05:08,880 --> 00:05:12,720
 And they were criticized for this.

53
00:05:12,720 --> 00:05:16,010
 Many people were saying, well, this monk is like the Pied

54
00:05:16,010 --> 00:05:16,640
 Viper.

55
00:05:16,640 --> 00:05:20,880
 He's leading all of our sons away.

56
00:05:20,880 --> 00:05:23,800
 He's a cult leader.

57
00:05:23,800 --> 00:05:27,910
 Of course, any community would start to get concerned if a

58
00:05:27,910 --> 00:05:30,000
 religious leader started to

59
00:05:30,000 --> 00:05:33,200
 take away their young men.

60
00:05:33,200 --> 00:05:38,000
 In the beginning, it was all young men.

61
00:05:38,000 --> 00:05:46,080
 And just kind of threatening their society.

62
00:05:46,080 --> 00:05:50,200
 So the monk came to the Buddha and asked him, and the

63
00:05:50,200 --> 00:05:52,600
 Buddha said, just tell them that they

64
00:05:52,600 --> 00:05:55,480
 go forth according to the Dharma.

65
00:05:55,480 --> 00:05:59,040
 Dharma was an important word for people.

66
00:05:59,040 --> 00:06:00,440
 They wanted to follow the Dharma.

67
00:06:00,440 --> 00:06:03,760
 So it was quite clever, actually.

68
00:06:03,760 --> 00:06:07,320
 And when they told people this, these monks go forth

69
00:06:07,320 --> 00:06:09,280
 according to the Dharma.

70
00:06:09,280 --> 00:06:13,630
 And people thought, people understood that actually this is

71
00:06:13,630 --> 00:06:14,440
...

72
00:06:14,440 --> 00:06:16,400
 It was a challenge, really.

73
00:06:16,400 --> 00:06:18,550
 And they had to then point out a flaw in the Buddha's

74
00:06:18,550 --> 00:06:20,240
 teaching, which, of course, they

75
00:06:20,240 --> 00:06:21,960
 couldn't easily find.

76
00:06:21,960 --> 00:06:24,360
 Anyway, that's not the story today.

77
00:06:24,360 --> 00:06:28,080
 Today's story, but the background is there.

78
00:06:28,080 --> 00:06:36,400
 That Yasodra's father, Supa Buddha, that's who the story is

79
00:06:36,400 --> 00:06:37,720
 about.

80
00:06:37,720 --> 00:06:42,780
 He was less than thrilled that his daughter and his

81
00:06:42,780 --> 00:06:46,280
 grandson renounced the world.

82
00:06:46,280 --> 00:06:49,000
 And he was very angry.

83
00:06:49,000 --> 00:06:55,440
 He was quite awful about it.

84
00:06:55,440 --> 00:07:02,650
 And so when the Buddha was visiting his family, when he

85
00:07:02,650 --> 00:07:05,680
 tried to go for alms with the monks,

86
00:07:05,680 --> 00:07:10,450
 Supa Buddha set up a seat in the middle of the road, in the

87
00:07:10,450 --> 00:07:13,200
 middle of the street, between

88
00:07:13,200 --> 00:07:17,030
 houses and started drinking alcohol, and I guess with his

89
00:07:17,030 --> 00:07:19,080
 friends, or just set up a sort

90
00:07:19,080 --> 00:07:20,800
 of a party.

91
00:07:20,800 --> 00:07:23,370
 And when the monks came, he refused to move, standing right

92
00:07:23,370 --> 00:07:24,560
 in the middle of the road,

93
00:07:24,560 --> 00:07:26,880
 and they couldn't go for alms.

94
00:07:26,880 --> 00:07:31,880
 Somehow he got in their way.

95
00:07:31,880 --> 00:07:42,040
 And they said to him, "Hey, the Buddha has come.

96
00:07:42,040 --> 00:07:43,040
 What's going on?"

97
00:07:43,040 --> 00:07:44,040
 And he says, "Tell him to go his way.

98
00:07:44,040 --> 00:07:45,040
 I'm older than him.

99
00:07:45,040 --> 00:07:48,040
 I'm not going to make way for him."

100
00:07:48,040 --> 00:07:49,560
 So he wouldn't get out of his way.

101
00:07:49,560 --> 00:07:54,300
 And the Buddha just turned around and walked away, made no

102
00:07:54,300 --> 00:07:55,560
 complaint.

103
00:07:55,560 --> 00:08:04,280
 He just went back to the monastery, or went another way.

104
00:08:04,280 --> 00:08:08,600
 And Supa Buddha sent someone out, one of his spies, to go

105
00:08:08,600 --> 00:08:09,440
 and see.

106
00:08:09,440 --> 00:08:12,670
 He said, "Go and see what the Buddha says, because he's got

107
00:08:12,670 --> 00:08:13,680
 more in store.

108
00:08:13,680 --> 00:08:14,680
 It's not...

109
00:08:14,680 --> 00:08:17,160
 This wasn't all he had planned.

110
00:08:17,160 --> 00:08:20,520
 He's got more plans than this."

111
00:08:20,520 --> 00:08:24,160
 So he says, "Go and spy on the Buddha.

112
00:08:24,160 --> 00:08:27,310
 See what he says about this, because we're going to use it

113
00:08:27,310 --> 00:08:28,320
 against him."

114
00:08:28,320 --> 00:08:30,320
 That's the idea.

115
00:08:30,320 --> 00:08:33,240
 And when the Buddha was on his way back, he smiled.

116
00:08:33,240 --> 00:08:36,050
 And there's several stories where the Buddha smiled, and

117
00:08:36,050 --> 00:08:37,560
 the Buddha smiling is not like

118
00:08:37,560 --> 00:08:39,200
 an ordinary person smiling.

119
00:08:39,200 --> 00:08:41,360
 It means something.

120
00:08:41,360 --> 00:08:45,800
 And so Ananda noticed that he was smiling and said, "Re

121
00:08:45,800 --> 00:08:49,760
verend Servite, why are you smiling?"

122
00:08:49,760 --> 00:08:51,720
 It's kind of perverse.

123
00:08:51,720 --> 00:08:56,780
 I want to say that smiling of a Buddha is not the smile of

124
00:08:56,780 --> 00:08:58,920
 an ordinary being.

125
00:08:58,920 --> 00:09:06,430
 They smile at extraordinary things when something is

126
00:09:06,430 --> 00:09:12,520
 extreme, because it's out of the ordinary,

127
00:09:12,520 --> 00:09:15,560
 often in a bad way.

128
00:09:15,560 --> 00:09:22,290
 And he says, Ananda, the super Buddha, he's committed a

129
00:09:22,290 --> 00:09:24,280
 grievous sin.

130
00:09:24,280 --> 00:09:28,300
 He's done something very, very terrible, very bad karma for

131
00:09:28,300 --> 00:09:30,200
 him, to block a Buddha, blocking

132
00:09:30,200 --> 00:09:33,000
 those people who wanted to give alms to the Buddha.

133
00:09:33,000 --> 00:09:37,960
 A Buddha is a fairly special being.

134
00:09:37,960 --> 00:09:40,960
 And he made a prediction.

135
00:09:40,960 --> 00:09:43,130
 And I'm going to gloss over some of this, because a lot of

136
00:09:43,130 --> 00:09:44,400
 these stories, I believe,

137
00:09:44,400 --> 00:09:46,240
 are exaggerated.

138
00:09:46,240 --> 00:09:49,200
 That might put me in bad graces with some people.

139
00:09:49,200 --> 00:09:55,700
 But I'm going to, as I've done before, I'm going to exagger

140
00:09:55,700 --> 00:09:58,480
ate some of the aspects,

141
00:09:58,480 --> 00:10:01,080
 at least talk about them.

142
00:10:01,080 --> 00:10:06,070
 But basically, he says he's going to die and go to hell in

143
00:10:06,070 --> 00:10:07,440
 seven days.

144
00:10:07,440 --> 00:10:08,440
 But he makes a prediction.

145
00:10:08,440 --> 00:10:13,960
 He says he's going to fall down the stairs in his palace.

146
00:10:13,960 --> 00:10:22,840
 And he's going to die at the bottom of the steps of his

147
00:10:22,840 --> 00:10:28,560
 seven story palace in seven days,

148
00:10:28,560 --> 00:10:36,120
 within seven days or something like that.

149
00:10:36,120 --> 00:10:39,000
 And so the spy hears this and goes back to super Buddha and

150
00:10:39,000 --> 00:10:41,960
 tells him, and super Buddha,

151
00:10:41,960 --> 00:10:48,300
 he says, well, you know, Buddha's Buddha's never never

152
00:10:48,300 --> 00:10:50,400
 would Buddha say, it's going to

153
00:10:50,400 --> 00:10:51,880
 happen, it's going to happen.

154
00:10:51,880 --> 00:10:57,080
 But he didn't say he's going to die in seven days.

155
00:10:57,080 --> 00:11:00,000
 He said, at that bottom of the stairs, he's going to die.

156
00:11:00,000 --> 00:11:03,980
 So if I don't go to the bottom of the stairs, if I don't go

157
00:11:03,980 --> 00:11:07,400
 down those stairs, I won't happen.

158
00:11:07,400 --> 00:11:12,240
 And he said, I'm going to so I'm going to what I'm going to

159
00:11:12,240 --> 00:11:15,000
 do is going to make it impossible

160
00:11:15,000 --> 00:11:17,920
 for me to go down those stairs.

161
00:11:17,920 --> 00:11:24,080
 And I'm going to show that the Buddha is a liar.

162
00:11:24,080 --> 00:11:29,640
 And so he orders his men to lock up to remove the stairs.

163
00:11:29,640 --> 00:11:32,740
 I guess he has another set of stairs or something, but he

164
00:11:32,740 --> 00:11:35,120
 removed these main stairs, seven floors

165
00:11:35,120 --> 00:11:44,540
 of them, apparently, and borrow all the doors and put

166
00:11:44,540 --> 00:11:48,720
 guards at each of the levels to stop

167
00:11:48,720 --> 00:11:54,170
 him from going through the doors where the stairs used to

168
00:11:54,170 --> 00:11:56,920
 be once they were removed,

169
00:11:56,920 --> 00:11:57,920
 making it impossible.

170
00:11:57,920 --> 00:12:00,950
 And he goes and he lives up on the on the top level and he

171
00:12:00,950 --> 00:12:02,360
 makes sure that he doesn't

172
00:12:02,360 --> 00:12:03,600
 go down these stairs.

173
00:12:03,600 --> 00:12:06,980
 He said he tells his guards, if ever I'm even thinking

174
00:12:06,980 --> 00:12:11,080
 about going through those doors,

175
00:12:11,080 --> 00:12:14,520
 you stop me.

176
00:12:14,520 --> 00:12:22,040
 He tells the guards to grab him and stop him.

177
00:12:22,040 --> 00:12:28,120
 And the teacher, the Buddha hears about this and he says,

178
00:12:28,120 --> 00:12:30,920
 this is the quote, he says, let

179
00:12:30,920 --> 00:12:35,370
 him let him not be let not he would be content with

180
00:12:35,370 --> 00:12:38,800
 ascending to the topmost floor of his

181
00:12:38,800 --> 00:12:39,800
 palace.

182
00:12:39,800 --> 00:12:43,040
 Let him soar aloft and sit in the air.

183
00:12:43,040 --> 00:12:47,380
 So even if he doesn't come, you know, doesn't stay at the

184
00:12:47,380 --> 00:12:49,960
 top and never come down, let him

185
00:12:49,960 --> 00:12:53,290
 fly through the air and let him put to sea in a boat or let

186
00:12:53,290 --> 00:12:55,000
 him enter into the bowels

187
00:12:55,000 --> 00:12:57,280
 of a mountain.

188
00:12:57,280 --> 00:12:59,360
 There's no equivocation in the words of the Buddha.

189
00:12:59,360 --> 00:13:00,360
 He will enter.

190
00:13:00,360 --> 00:13:02,600
 He will he will die.

191
00:13:02,600 --> 00:13:05,700
 He will enter into the earth, meaning he will pass away

192
00:13:05,700 --> 00:13:07,520
 right where I said he would.

193
00:13:07,520 --> 00:13:11,080
 I mean, the actual story is that the earth opened up and

194
00:13:11,080 --> 00:13:13,160
 swallowed him when he was born

195
00:13:13,160 --> 00:13:14,160
 in hell.

196
00:13:14,160 --> 00:13:16,110
 That happened to five different people, I think, in the

197
00:13:16,110 --> 00:13:16,800
 Buddhist time.

198
00:13:16,800 --> 00:13:19,600
 David Datta was one of them.

199
00:13:19,600 --> 00:13:22,410
 Not convinced that it actually happens, but that's what the

200
00:13:22,410 --> 00:13:23,160
 story says.

201
00:13:23,160 --> 00:13:24,160
 It may have happened.

202
00:13:24,160 --> 00:13:31,280
 It's just kind of incorrect, hard to believe.

203
00:13:31,280 --> 00:13:35,360
 And then he tells this verse.

204
00:13:35,360 --> 00:13:38,160
 And the verse isn't is the verse is telling something

205
00:13:38,160 --> 00:13:39,000
 different.

206
00:13:39,000 --> 00:13:44,040
 It's saying that you can't avoid death.

207
00:13:44,040 --> 00:13:47,380
 But it fits in because it's not only that you can't avoid

208
00:13:47,380 --> 00:13:49,200
 death, it's that you can't

209
00:13:49,200 --> 00:13:52,280
 avoid in general the consequences of your deeds.

210
00:13:52,280 --> 00:13:55,600
 I mean, stopping a spiritual teacher like the Buddha,

211
00:13:55,600 --> 00:13:57,440
 someone so pure and so good and

212
00:13:57,440 --> 00:14:00,920
 so wise, getting in their way.

213
00:14:00,920 --> 00:14:04,280
 I mean, if you read the text, it's a bad thing.

214
00:14:04,280 --> 00:14:12,450
 But emotionally or intuitively, you can understand how it

215
00:14:12,450 --> 00:14:15,000
 would be a bad thing to do.

216
00:14:15,000 --> 00:14:16,440
 Pretty hard karma, bad karma.

217
00:14:16,440 --> 00:14:19,080
 I mean, karma in Buddhism is like that.

218
00:14:19,080 --> 00:14:24,230
 If you hurt an evil person, it's not as bad as if you hurt

219
00:14:24,230 --> 00:14:25,920
 a pure person.

220
00:14:25,920 --> 00:14:28,720
 Hurting someone who is pure is far worse.

221
00:14:28,720 --> 00:14:31,280
 Hurting someone who doesn't deserve it, but more over

222
00:14:31,280 --> 00:14:32,760
 hurting someone who is pure, who

223
00:14:32,760 --> 00:14:39,080
 is good, who is spiritually enlightened.

224
00:14:39,080 --> 00:14:42,960
 But it's just the pinnacle of that.

225
00:14:42,960 --> 00:14:44,200
 So that's it for the verse.

226
00:14:44,200 --> 00:14:47,210
 But then it tells the end of the story kind of as an after

227
00:14:47,210 --> 00:14:49,040
thought, and it just stops.

228
00:14:49,040 --> 00:14:51,560
 It just tells the story and doesn't explain it at all.

229
00:14:51,560 --> 00:14:58,850
 It says, "In the middle of the night, his horse, one of his

230
00:14:58,850 --> 00:15:02,920
 expensive horses broke loose

231
00:15:02,920 --> 00:15:08,400
 and was running around the bottom floor of the palace."

232
00:15:08,400 --> 00:15:12,800
 I guess that was an open area where the stables were or

233
00:15:12,800 --> 00:15:13,960
 whatever.

234
00:15:13,960 --> 00:15:19,380
 And so he heard this ruckus and he asked them, "What's

235
00:15:19,380 --> 00:15:20,800
 going on?"

236
00:15:20,800 --> 00:15:24,160
 And they said, "Oh, you're a horse."

237
00:15:24,160 --> 00:15:25,440
 And so he wanted to catch him.

238
00:15:25,440 --> 00:15:32,280
 He stood up and started towards the stairs.

239
00:15:32,280 --> 00:15:34,040
 I guess he didn't have another set of stairs.

240
00:15:34,040 --> 00:15:36,740
 The idea was he was just going to not come down for seven

241
00:15:36,740 --> 00:15:38,320
 days, and at the end of seven

242
00:15:38,320 --> 00:15:42,520
 days he would open the door.

243
00:15:42,520 --> 00:15:46,480
 But he forgot, of course, and he got to the door.

244
00:15:46,480 --> 00:15:49,470
 But here's where the funny thing goes, because he was

245
00:15:49,470 --> 00:15:51,960
 supposed to have these guards watching.

246
00:15:51,960 --> 00:15:56,420
 But when he gets to the doors, it says things like the

247
00:15:56,420 --> 00:15:59,280
 stairs appear and the doors open

248
00:15:59,280 --> 00:16:01,080
 of their own accord.

249
00:16:01,080 --> 00:16:04,840
 I'm not convinced that that happens, but maybe it does.

250
00:16:04,840 --> 00:16:08,680
 I think something you can argue might happen is the angels

251
00:16:08,680 --> 00:16:10,800
 get involved and they push open

252
00:16:10,800 --> 00:16:13,440
 the doors.

253
00:16:13,440 --> 00:16:16,800
 But the kind of thing that would happen, not saying that

254
00:16:16,800 --> 00:16:18,680
 that's what happened and that

255
00:16:18,680 --> 00:16:20,720
 it's what happened in this case, but the kind of thing that

256
00:16:20,720 --> 00:16:21,800
 would happen from being such

257
00:16:21,800 --> 00:16:27,420
 an evil, evil person is that maybe the guards didn't listen

258
00:16:27,420 --> 00:16:28,400
 to him.

259
00:16:28,400 --> 00:16:31,860
 Maybe they didn't remove the staircase, especially if they

260
00:16:31,860 --> 00:16:33,960
 were all Buddhist or keen about the

261
00:16:33,960 --> 00:16:39,640
 Buddha, so they wouldn't want to get involved with this.

262
00:16:39,640 --> 00:16:42,710
 And it says that the story says that the guards actually

263
00:16:42,710 --> 00:16:44,640
 threw him down the stairs, which

264
00:16:44,640 --> 00:16:48,210
 gives me that idea that the guards were probably pretty

265
00:16:48,210 --> 00:16:50,640
 upset at Sukhumvut at this point.

266
00:16:50,640 --> 00:16:54,150
 So the doors open, the guards threw him down the first and

267
00:16:54,150 --> 00:16:56,160
 the seventh floor, they threw

268
00:16:56,160 --> 00:16:58,400
 him down to the sixth floor and he tumbled down the stairs.

269
00:16:58,400 --> 00:17:01,150
 The guy on the sixth floor threw him down to the fifth

270
00:17:01,150 --> 00:17:02,960
 floor, and they actually threw

271
00:17:02,960 --> 00:17:06,940
 him down the stairs, seven flights of stairs, at which

272
00:17:06,940 --> 00:17:09,600
 point he was swallowed by the earth.

273
00:17:09,600 --> 00:17:13,150
 Or he died at the bottom of the stairs, right where the

274
00:17:13,150 --> 00:17:14,560
 Buddha had saved.

275
00:17:14,560 --> 00:17:18,440
 And he descended therein and was reborn in Avicii's health.

276
00:17:18,440 --> 00:17:23,200
 And that's how the story ends, just like that.

277
00:17:23,200 --> 00:17:28,160
 So it's one of those sort of fantastic stories.

278
00:17:28,160 --> 00:17:33,790
 It doesn't say too much about the verse, except to say that

279
00:17:33,790 --> 00:17:36,560
 there's this idea that you can't

280
00:17:36,560 --> 00:17:37,560
 avoid karma.

281
00:17:37,560 --> 00:17:46,190
 The only way to... there are ways you can mitigate, you

282
00:17:46,190 --> 00:17:48,640
 know, like if you have negative

283
00:17:48,640 --> 00:17:55,280
 karma, positive karma will cancel it out sometimes.

284
00:17:55,280 --> 00:17:56,360
 But it does just that.

285
00:17:56,360 --> 00:18:00,000
 It's the power of the good karma that cancels it out.

286
00:18:00,000 --> 00:18:03,880
 You can't escape the karma entirely.

287
00:18:03,880 --> 00:18:07,920
 You have to do something to mitigate it.

288
00:18:07,920 --> 00:18:12,320
 Or to pass away into enlightenment first.

289
00:18:12,320 --> 00:18:16,240
 There's this idea that when you become enlightened and you

290
00:18:16,240 --> 00:18:18,920
 aren't reborn, all the future results

291
00:18:18,920 --> 00:18:22,600
 of your deeds don't have time to come to fruition.

292
00:18:22,600 --> 00:18:25,720
 But as long as you're in samsara, there's going to be room.

293
00:18:25,720 --> 00:18:27,240
 Everything has an effect, that's the point.

294
00:18:27,240 --> 00:18:30,080
 I mean, it's like physics.

295
00:18:30,080 --> 00:18:33,370
 Everything has its power, and the power doesn't just go

296
00:18:33,370 --> 00:18:35,280
 away and get forgotten about.

297
00:18:35,280 --> 00:18:38,240
 It doesn't just disappear.

298
00:18:38,240 --> 00:18:43,360
 It has an effect.

299
00:18:43,360 --> 00:18:44,360
 And that's a part of...

300
00:18:44,360 --> 00:18:46,720
 It's a way of looking at this verse.

301
00:18:46,720 --> 00:18:49,670
 I mean, the most obvious way to look at the verse is the

302
00:18:49,670 --> 00:18:51,480
 idea of death, how death comes

303
00:18:51,480 --> 00:18:55,080
 to us all, and therefore we shouldn't be negligent.

304
00:18:55,080 --> 00:18:57,280
 We shouldn't waste our lives.

305
00:18:57,280 --> 00:19:03,000
 We should work to do what we can while we're still alive.

306
00:19:03,000 --> 00:19:04,400
 Because we don't know when death is coming.

307
00:19:04,400 --> 00:19:05,400
 We don't know where.

308
00:19:05,400 --> 00:19:06,480
 We don't know how.

309
00:19:06,480 --> 00:19:11,280
 We don't know where we're going when we pass away.

310
00:19:11,280 --> 00:19:15,580
 But in the context of the story, there's another

311
00:19:15,580 --> 00:19:18,080
 interesting point here.

312
00:19:18,080 --> 00:19:25,360
 It's that the reason we die, and the reason death is

313
00:19:25,360 --> 00:19:27,240
 inevitable, is because we're not

314
00:19:27,240 --> 00:19:30,200
 in the case of the karma of being reborn.

315
00:19:30,200 --> 00:19:37,760
 The karma of clinging at the moment of death, and therefore

316
00:19:37,760 --> 00:19:41,640
 creating a new body, means we

317
00:19:41,640 --> 00:19:42,640
 have to die.

318
00:19:42,640 --> 00:19:46,930
 I mean, it's an example of karma, of the results of karma

319
00:19:46,930 --> 00:19:48,680
 that is inevitable.

320
00:19:48,680 --> 00:19:53,720
 But it's easy to see the inevitable results of karma.

321
00:19:53,720 --> 00:19:55,920
 But karma in general is like that.

322
00:19:55,920 --> 00:19:58,440
 So he's getting thrown down the stairs.

323
00:19:58,440 --> 00:20:01,600
 Why is he inevitable?

324
00:20:01,600 --> 00:20:08,500
 So we talk, an interesting question then is whether our

325
00:20:08,500 --> 00:20:12,080
 lives are deterministic.

326
00:20:12,080 --> 00:20:14,540
 And I've talked about this before, that I don't think

327
00:20:14,540 --> 00:20:16,080
 determinism is the right way to

328
00:20:16,080 --> 00:20:20,200
 look at it, because it requires a framework, a universe, a

329
00:20:20,200 --> 00:20:22,280
 four dimensional space-time

330
00:20:22,280 --> 00:20:24,400
 universe.

331
00:20:24,400 --> 00:20:26,440
 It's all really up in the mind.

332
00:20:26,440 --> 00:20:30,720
 And if you look at the universe in terms of the present

333
00:20:30,720 --> 00:20:33,800
 moment, then determinism doesn't

334
00:20:33,800 --> 00:20:39,360
 really, it's saying too much.

335
00:20:39,360 --> 00:20:42,130
 It's hard to get your mind wrapped around it, but that's

336
00:20:42,130 --> 00:20:43,560
 because we focus on the idea

337
00:20:43,560 --> 00:20:47,290
 of a four dimensional reality of a universe existing around

338
00:20:47,290 --> 00:20:47,720
 us.

339
00:20:47,720 --> 00:20:50,810
 So we can think in terms of billiard balls, hitting each

340
00:20:50,810 --> 00:20:52,840
 other and causing effects, cause

341
00:20:52,840 --> 00:20:54,960
 and effect like that.

342
00:20:54,960 --> 00:20:57,640
 And Buddhism certainly subscribes to the idea of cause and

343
00:20:57,640 --> 00:21:01,040
 effect, but I think it stops there,

344
00:21:01,040 --> 00:21:05,300
 that there is an effect of our actions in the present

345
00:21:05,300 --> 00:21:06,240
 moment.

346
00:21:06,240 --> 00:21:10,480
 There isn't, things go according to cause and effect.

347
00:21:10,480 --> 00:21:15,780
 But determinism is, to be deterministic is to set yourself

348
00:21:15,780 --> 00:21:18,080
 in the mind with a concept

349
00:21:18,080 --> 00:21:26,570
 of things being fixed, things existing that are fixed, or

350
00:21:26,570 --> 00:21:29,880
 are fixed in terms of their

351
00:21:29,880 --> 00:21:30,880
 result.

352
00:21:30,880 --> 00:21:34,720
 So, but at the same time, what there is, is it seems that

353
00:21:34,720 --> 00:21:36,760
 it is possible to predict the

354
00:21:36,760 --> 00:21:40,370
 future, the Buddha is able to do it, and people are able to

355
00:21:40,370 --> 00:21:42,160
 see things in the future.

356
00:21:42,160 --> 00:21:45,920
 And the future seems to in some way, I mean maybe a good

357
00:21:45,920 --> 00:21:47,720
 way of looking at it is that

358
00:21:47,720 --> 00:21:50,000
 the future is able to affect the past.

359
00:21:50,000 --> 00:21:53,430
 That's a way of looking at quantum physics, for example,

360
00:21:53,430 --> 00:21:55,800
 like why when you measure something,

361
00:21:55,800 --> 00:21:58,720
 does it affect something that's already been measured?

362
00:21:58,720 --> 00:22:02,740
 When you affect one thing after the fact, it affects

363
00:22:02,740 --> 00:22:05,480
 something that's already, it affects

364
00:22:05,480 --> 00:22:08,120
 something in the past.

365
00:22:08,120 --> 00:22:09,120
 Strange things happen.

366
00:22:09,120 --> 00:22:13,760
 So the future may be able to affect the past, that kind of,

367
00:22:13,760 --> 00:22:16,080
 not so important for us, but

368
00:22:16,080 --> 00:22:19,920
 what is important is this.

369
00:22:19,920 --> 00:22:21,960
 So we don't want to go too far in terms of thinking it's

370
00:22:21,960 --> 00:22:23,240
 all deterministic, it's all

371
00:22:23,240 --> 00:22:26,920
 that's wrong view, no question.

372
00:22:26,920 --> 00:22:30,860
 And it's a very bad view to have practically speaking as

373
00:22:30,860 --> 00:22:33,280
 well, because it makes you lack

374
00:22:33,280 --> 00:22:35,960
 on a lackadaisical lazy basically.

375
00:22:35,960 --> 00:22:42,540
 But at the same time, we have to understand that our

376
00:22:42,540 --> 00:22:44,400
 actions have consequences that are

377
00:22:44,400 --> 00:22:45,400
 fixed.

378
00:22:45,400 --> 00:22:48,600
 So if you do this, this is going to happen.

379
00:22:48,600 --> 00:22:54,440
 Well when you do this, this comes with it, that kind of

380
00:22:54,440 --> 00:22:55,600
 thing.

381
00:22:55,600 --> 00:22:58,630
 And so how this relates to our meditation, well, I talked

382
00:22:58,630 --> 00:23:00,280
 about how meditation teaches

383
00:23:00,280 --> 00:23:02,360
 you about karma.

384
00:23:02,360 --> 00:23:05,760
 More importantly, it purifies our karma.

385
00:23:05,760 --> 00:23:10,400
 The most important aspect of meditation is it purifies the

386
00:23:10,400 --> 00:23:11,120
 mind.

387
00:23:11,120 --> 00:23:13,120
 And so it prepares you for death.

388
00:23:13,120 --> 00:23:19,200
 Death becomes not a scary thing, not a powerful thing.

389
00:23:19,200 --> 00:23:22,200
 Death just becomes another moment, because you see that we

390
00:23:22,200 --> 00:23:24,360
're actually born and die in

391
00:23:24,360 --> 00:23:26,320
 every moment.

392
00:23:26,320 --> 00:23:31,220
 You train your mind, you purify your mind, you come to see

393
00:23:31,220 --> 00:23:34,120
 your body and your mind clearly,

394
00:23:34,120 --> 00:23:41,560
 the moments of experience.

395
00:23:41,560 --> 00:23:43,160
 And then you don't have to escape.

396
00:23:43,160 --> 00:23:47,280
 We're always looking for an escape or a shelter, whether it

397
00:23:47,280 --> 00:23:49,520
 be in the heavens or in a mountain,

398
00:23:49,520 --> 00:23:51,760
 in a palace.

399
00:23:51,760 --> 00:23:55,610
 Find a way to shut ourselves out from karma, like Supa

400
00:23:55,610 --> 00:23:57,240
 Buddha tried to do.

401
00:23:57,240 --> 00:24:02,320
 I mean, it's just one example, but we do this all the time.

402
00:24:02,320 --> 00:24:07,880
 Try and find ways to create security and safety.

403
00:24:07,880 --> 00:24:09,120
 You don't need to do that.

404
00:24:09,120 --> 00:24:14,090
 Once you're pure in the mind, clear in the mind, you can

405
00:24:14,090 --> 00:24:16,080
 live on the street.

406
00:24:16,080 --> 00:24:20,120
 You can live in poverty.

407
00:24:20,120 --> 00:24:22,920
 You can be sick.

408
00:24:22,920 --> 00:24:26,160
 You can be injured.

409
00:24:26,160 --> 00:24:30,920
 You can be hungry and thirsty and pain.

410
00:24:30,920 --> 00:24:33,440
 And you can be at war.

411
00:24:33,440 --> 00:24:41,880
 You can be a victim of violence and still be invincible.

412
00:24:41,880 --> 00:24:43,880
 Then nothing overpowers you.

413
00:24:43,880 --> 00:24:46,240
 And the Buddha said, "You actually don't die."

414
00:24:46,240 --> 00:24:49,360
 The funny thing we talk about...

415
00:24:49,360 --> 00:24:58,120
 Well, anyway, you become free from death.

416
00:24:58,120 --> 00:25:00,480
 Death cannot overpower you.

417
00:25:00,480 --> 00:25:08,590
 Death doesn't overpower the one who is free from the fear

418
00:25:08,590 --> 00:25:12,240
 of death, free from the attachment

419
00:25:12,240 --> 00:25:16,600
 to life.

420
00:25:16,600 --> 00:25:19,520
 So that's the Dhammapada for tonight.

421
00:25:19,520 --> 00:25:21,320
 Thank you all for tuning in.

422
00:25:21,320 --> 00:25:22,040
 See you all next time.

423
00:25:22,040 --> 00:25:42,400
 Thank you.

