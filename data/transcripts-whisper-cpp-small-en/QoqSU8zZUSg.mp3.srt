1
00:00:00,000 --> 00:00:05,440
 Hello, welcome back to the series of videos on the Dhammap

2
00:00:05,440 --> 00:00:07,000
ada. Today I will continue with

3
00:00:07,000 --> 00:00:13,280
 verses 3 and 4. But first I'd like to make a correction to

4
00:00:13,280 --> 00:00:14,760
 the first video about the

5
00:00:14,760 --> 00:00:19,880
 first verse wherein I said that the reason the Buddha gave

6
00:00:19,880 --> 00:00:22,320
 this teaching was because

7
00:00:23,480 --> 00:00:27,760
 of these monks killing all these insects unknowingly and

8
00:00:27,760 --> 00:00:30,480
 then the monks were saying that he had

9
00:00:30,480 --> 00:00:35,150
 done a bad deed and the Buddha said that he indeed didn't

10
00:00:35,150 --> 00:00:38,000
 and he explained that suffering

11
00:00:38,000 --> 00:00:43,420
 only follows from bad intentions. But actually going over

12
00:00:43,420 --> 00:00:45,000
 it again I realized that actually

13
00:00:45,000 --> 00:00:50,340
 specifically the Buddha gave the teaching in regards to the

14
00:00:50,340 --> 00:00:52,640
 reason why this monk went

15
00:00:52,640 --> 00:00:56,920
 blind in the first place which was because in a past life

16
00:00:56,920 --> 00:00:58,960
 he had out of revenge for a

17
00:00:58,960 --> 00:01:03,060
 woman he had treated. He was a doctor who had given this

18
00:01:03,060 --> 00:01:05,200
 treatment to a woman and out

19
00:01:05,200 --> 00:01:08,960
 of revenge for her trying to cheat him he gave her

20
00:01:08,960 --> 00:01:11,800
 something that would make her blind

21
00:01:11,800 --> 00:01:14,700
 and as a result of that bad deed it came back to him. It

22
00:01:14,700 --> 00:01:16,440
 was stuck in his mind because he

23
00:01:16,440 --> 00:01:19,390
 must have felt very guilty. As a doctor he was a good

24
00:01:19,390 --> 00:01:21,360
 doctor and feeling guilty about

25
00:01:21,360 --> 00:01:25,090
 it and it's something that he naturally would feel guilty

26
00:01:25,090 --> 00:01:27,400
 about. So it eventually caught

27
00:01:27,400 --> 00:01:32,230
 up to him and he realized what a horrible thing he had done

28
00:01:32,230 --> 00:01:34,400
 and it conditioned this

29
00:01:34,400 --> 00:01:41,800
 life for him to have to go blind. So that's the origin.

30
00:01:41,800 --> 00:01:41,800
 Then after that the Buddha said

31
00:01:41,800 --> 00:01:47,780
 "Oh you see, you can never escape your bad deeds." But it

32
00:01:47,780 --> 00:01:49,160
 does go both ways. Obviously

33
00:01:49,160 --> 00:01:54,230
 there is reference to the fact that this Chakupala wasn't

34
00:01:54,230 --> 00:01:56,160
 guilty for the killing that he had

35
00:01:56,160 --> 00:02:00,820
 done or for the death of the insects but he was on the

36
00:02:00,820 --> 00:02:03,960
 other hand guilty for something

37
00:02:03,960 --> 00:02:09,280
 that he intended to do to hurt this woman and so he got the

38
00:02:09,280 --> 00:02:10,960
 results of it. But I'm not

39
00:02:10,960 --> 00:02:14,680
 going into these stories so much. I just wanted to correct

40
00:02:14,680 --> 00:02:17,040
 something that I had said. I'm

41
00:02:17,040 --> 00:02:20,240
 going to try to stick to a very simple version of the

42
00:02:20,240 --> 00:02:22,560
 stories and place more emphasis on

43
00:02:22,560 --> 00:02:25,380
 the meaning of the verses and how they apply to our lives

44
00:02:25,380 --> 00:02:27,040
 because I think that is of more

45
00:02:27,040 --> 00:02:30,560
 importance. So this next story is the same. It's a very

46
00:02:30,560 --> 00:02:32,440
 simple story but it's extended

47
00:02:32,440 --> 00:02:37,830
 and I'm going to avoid the extension. So verses three and

48
00:02:37,830 --> 00:02:39,440
 four. Number three goes

49
00:02:39,440 --> 00:02:46,440
 "Ako ji man a wadi man a jini man aha asi me"

50
00:02:46,440 --> 00:02:53,440
 "Ako ji man a wadi man a jini man aha asi me"

51
00:02:53,440 --> 00:02:58,630
 And verse four is almost the same. "Ako ji man a wadi man a

52
00:02:58,630 --> 00:03:00,440
 jini man aha asi me"

53
00:03:00,440 --> 00:03:07,440
 "Ako ji man a wadi man a jini man aha asi me"

54
00:03:07,440 --> 00:03:09,440
 Which means

55
00:03:09,440 --> 00:03:14,600
 "Ako ji man a wadi man a jini man aha asi me" This is a

56
00:03:14,600 --> 00:03:21,440
 quote. He or she or this person scolded

57
00:03:21,440 --> 00:03:27,920
 me, they hurt me, they beat me, they defeated me, they

58
00:03:27,920 --> 00:03:32,440
 destroyed me. A person who clings

59
00:03:32,440 --> 00:03:40,310
 or grasps on or binds themselves to these thoughts. "Weir

60
00:03:40,310 --> 00:03:42,440
ang te sangna samatthi"

61
00:03:42,440 --> 00:03:46,430
 For them their quarrels never ceased, are never appeased,

62
00:03:46,430 --> 00:03:48,440
 are never tranquilized, are

63
00:03:48,440 --> 00:03:56,460
 never finished. "But ako ji man a wadi man a jini man aha

64
00:03:56,460 --> 00:03:57,440
 asi me"

65
00:03:57,440 --> 00:04:02,240
 He hurt me, he scolded me, he beat me, he hurt me, he

66
00:04:02,240 --> 00:04:04,440
 defeated me. For a person who doesn't

67
00:04:04,440 --> 00:04:08,440
 cling to these thoughts, "Weirang te su pasaamatthi"

68
00:04:08,440 --> 00:04:14,150
 Their quarrels are appeased, their quarrels are tranquil

69
00:04:14,150 --> 00:04:15,440
ized, they're finished.

70
00:04:15,440 --> 00:04:20,070
 The problems they have with people are solved, are worked

71
00:04:20,070 --> 00:04:20,440
 out.

72
00:04:20,440 --> 00:04:25,820
 These two verses were given in regards to a monk named Thi

73
00:04:25,820 --> 00:04:28,440
isa. Thiisa was very fat,

74
00:04:28,440 --> 00:04:34,440
 he was quite overweight, he had quite a lot of weight and

75
00:04:34,440 --> 00:04:36,440
 he became a monk late in life.

76
00:04:36,440 --> 00:04:41,370
 And rather than practicing meditation, he took to a very

77
00:04:41,370 --> 00:04:43,440
 indulgent monk's life. He was

78
00:04:43,440 --> 00:04:48,520
 a relative of the Buddha, so he was quite spoiled in being

79
00:04:48,520 --> 00:04:50,440
 a part of the royalty.

80
00:04:50,440 --> 00:04:54,850
 Also because of his conceit, because he was related to the

81
00:04:54,850 --> 00:04:57,440
 Buddha, he didn't bother to

82
00:04:57,440 --> 00:05:00,790
 exert himself in the practice of meditation, he just would

83
00:05:00,790 --> 00:05:04,440
 sit around and make a general

84
00:05:04,440 --> 00:05:09,270
 nuisance of himself. In fact you do find in monasteries

85
00:05:09,270 --> 00:05:12,440
 even today, believe it or not.

86
00:05:12,440 --> 00:05:19,940
 There was one occasion where he was sitting in the

87
00:05:19,940 --> 00:05:22,440
 reception sala, I guess, in the middle

88
00:05:22,440 --> 00:05:25,040
 of the monastery, maybe it wasn't a sala, but in the very

89
00:05:25,040 --> 00:05:26,440
 middle where he would expect

90
00:05:26,440 --> 00:05:29,670
 visitors to come. So these visiting monks came who were

91
00:05:29,670 --> 00:05:31,440
 actually quite senior, but were

92
00:05:31,440 --> 00:05:36,440
 younger than him, because he had ordained when he was old.

93
00:05:36,440 --> 00:05:40,790
 And they thought, well maybe he's an old monk, so they came

94
00:05:40,790 --> 00:05:43,440
 up to him and saw that he was

95
00:05:43,440 --> 00:05:45,940
 just sitting there looking proud, and so they came and paid

96
00:05:45,940 --> 00:05:47,440
 respect to him and asked politely

97
00:05:47,440 --> 00:05:52,440
 who he was and so on, how many years he had been a monk.

98
00:05:52,440 --> 00:05:55,440
 And he told them years, he said

99
00:05:55,440 --> 00:05:59,320
 years, I've just ordained, I don't have any years as a monk

100
00:05:59,320 --> 00:06:02,440
. And they said, well then

101
00:06:02,440 --> 00:06:05,370
 how can you act like this, how can you just sit there? When

102
00:06:05,370 --> 00:06:07,440
 visiting monks come, it's

103
00:06:07,440 --> 00:06:11,030
 very important, there's a protocol, if the receiving monk,

104
00:06:11,030 --> 00:06:12,440
 someone who sees a monk coming,

105
00:06:12,440 --> 00:06:15,480
 is junior to the approaching monk, then they have a

106
00:06:15,480 --> 00:06:17,440
 responsibility to take care of them,

107
00:06:17,440 --> 00:06:21,230
 to take their bowl, to bring them water to show them to a

108
00:06:21,230 --> 00:06:23,440
 room and so on, and to make

109
00:06:23,440 --> 00:06:26,850
 sure that they're well taken care of and to pay respect to

110
00:06:26,850 --> 00:06:28,440
 them. And they said, how come

111
00:06:28,440 --> 00:06:31,270
 you're not following this protocol, you're not behaving

112
00:06:31,270 --> 00:06:33,440
 like a proper monk? And they

113
00:06:33,440 --> 00:06:37,240
 said, that's not appropriate. He said, who are you? Who do

114
00:06:37,240 --> 00:06:40,440
 you think you are? He snapped

115
00:06:40,440 --> 00:06:45,470
 his fingers, this kind of rude gesture of the times in

116
00:06:45,470 --> 00:06:47,440
 India. And they said, oh we've

117
00:06:47,440 --> 00:06:51,070
 come to see the Buddha, and he said, huh, you think you can

118
00:06:51,070 --> 00:06:53,440
 say nasty things? He was

119
00:06:53,440 --> 00:06:56,850
 very upset actually, he was very spoiled. And so he said,

120
00:06:56,850 --> 00:06:58,440
 who do you think you are? You

121
00:06:58,440 --> 00:07:02,000
 know who I am, and the relative of the Buddha and his

122
00:07:02,000 --> 00:07:04,440
 cousin, something like that. And he

123
00:07:04,440 --> 00:07:07,540
 was very upset, and so he went to see the Buddha and he

124
00:07:07,540 --> 00:07:09,440
 started complaining, he started crying

125
00:07:09,440 --> 00:07:13,030
 to the Buddha, and he said, these men have scolded me, and

126
00:07:13,030 --> 00:07:15,440
 they've abused me. And the

127
00:07:15,440 --> 00:07:18,760
 Buddha said, well what did you do? He said, well I was just

128
00:07:18,760 --> 00:07:20,440
 sitting there and well, and

129
00:07:20,440 --> 00:07:23,230
 did you receive them when they came? No. Did you take their

130
00:07:23,230 --> 00:07:25,440
 bowls? No. Did you show them?

131
00:07:25,440 --> 00:07:29,630
 And so on and so on. No, no. And the Buddha said, well then

132
00:07:29,630 --> 00:07:31,440
 you yourself are to blame,

133
00:07:31,440 --> 00:07:35,120
 you're the one who was acting inappropriately. And the

134
00:07:35,120 --> 00:07:37,440
 Buddha said, you should apologize

135
00:07:37,440 --> 00:07:41,830
 to them. And he said, apologize? What do you mean? They

136
00:07:41,830 --> 00:07:44,440
 abused me, I will not apologize.

137
00:07:44,440 --> 00:07:47,920
 So he's very obstinate. So the Buddha told the story of the

138
00:07:47,920 --> 00:07:49,440
 past life of this monk, the

139
00:07:49,440 --> 00:07:51,990
 monk remarked how obstinate he was, and the obstinate he

140
00:07:51,990 --> 00:07:53,440
 was. The Buddha said, oh he's

141
00:07:53,440 --> 00:07:56,800
 been like this for a long time. It's kind of a habit of his

142
00:07:56,800 --> 00:07:59,440
 that extend, an extended habit.

143
00:07:59,440 --> 00:08:05,400
 And so he told the story of the past life, and where he was

144
00:08:05,400 --> 00:08:07,440
, they were these two ascetics

145
00:08:07,440 --> 00:08:11,130
 in a room together, and one of them was sleeping in front

146
00:08:11,130 --> 00:08:14,440
 of the door, and one of them was sleeping

147
00:08:14,440 --> 00:08:18,170
 inside, because it was a small room. And the one inside

148
00:08:18,170 --> 00:08:20,440
 noticed that this monk had put

149
00:08:20,440 --> 00:08:23,150
 his head down here. But in the middle of the night, this

150
00:08:23,150 --> 00:08:24,440
 monk got up and put his head the

151
00:08:24,440 --> 00:08:27,740
 other way, turned around to sleep the other way. And the

152
00:08:27,740 --> 00:08:29,440
 monk inside had to go outside

153
00:08:29,440 --> 00:08:32,390
 to the washroom, and so he went around where he thought the

154
00:08:32,390 --> 00:08:34,440
 monk's feet were, and ended

155
00:08:34,440 --> 00:08:39,950
 up stepping on his hair. And so the monk was very angry,

156
00:08:39,950 --> 00:08:42,440
 and the other monk said, oh I'm

157
00:08:42,440 --> 00:08:46,070
 sorry, or the ascetic, the other ascetic said, oh I'm sorry

158
00:08:46,070 --> 00:08:48,440
, I didn't mean to. And he, so

159
00:08:48,440 --> 00:08:51,220
 he went outside, and on the way, when he was out, the monk

160
00:08:51,220 --> 00:08:53,440
 said, I'm not going to let him

161
00:08:53,440 --> 00:08:56,810
 do that again. And so he flipped over and put his head the

162
00:08:56,810 --> 00:08:58,440
 other way again, back to where

163
00:08:58,440 --> 00:09:01,210
 it originally was. And the other monk came back and thought

164
00:09:01,210 --> 00:09:02,440
 to himself, okay, I'll avoid

165
00:09:02,440 --> 00:09:05,300
 stepping on his head. So he went around to the other side,

166
00:09:05,300 --> 00:09:06,440
 and ended up stepping on the

167
00:09:06,440 --> 00:09:10,550
 guy's neck. Ah, there was a big fight that ensued. And he

168
00:09:10,550 --> 00:09:12,440
 said, well look, you had your

169
00:09:12,440 --> 00:09:14,600
 head there, what are you doing changing all the time? How

170
00:09:14,600 --> 00:09:15,440
 can I know where your head is?

171
00:09:15,440 --> 00:09:18,420
 It's dark, I don't have a clue, I don't have flashlights.

172
00:09:18,420 --> 00:09:22,440
 But the monk wasn't, the ascetic

173
00:09:22,440 --> 00:09:25,910
 wasn't to be appeased, and he was very angry, and he held

174
00:09:25,910 --> 00:09:27,440
 on to this, and he made a big

175
00:09:27,440 --> 00:09:30,890
 fuss over it, and he actually cursed the other ascetic. And

176
00:09:30,890 --> 00:09:32,440
 then there was the cursing back

177
00:09:32,440 --> 00:09:35,970
 and forth, and so on and so on. It's a long story, I don't

178
00:09:35,970 --> 00:09:37,440
 want to get into it. But the

179
00:09:37,440 --> 00:09:41,150
 point being, that people do hold on to these things, and

180
00:09:41,150 --> 00:09:43,440
 this is important, because this

181
00:09:43,440 --> 00:09:47,290
 is what the Buddha emphasized. He didn't say, don't have

182
00:09:47,290 --> 00:09:50,440
 these thoughts. He doesn't say,

183
00:09:50,440 --> 00:09:55,500
 for a person who has these thoughts. Because these thoughts

184
00:09:55,500 --> 00:09:57,440
 are wrong, it's wrong for us

185
00:09:57,440 --> 00:10:01,550
 to think like this. Why is it wrong? It's unpleasant. It

186
00:10:01,550 --> 00:10:03,440
 creates suffering for us, and

187
00:10:03,440 --> 00:10:06,410
 it builds up a bad habit. But what the Buddha is saying

188
00:10:06,410 --> 00:10:08,440
 here is actually a little more specific.

189
00:10:08,440 --> 00:10:12,600
 He's saying, don't cling to them. He says, for a person who

190
00:10:12,600 --> 00:10:14,440
 is bound to them, the word

191
00:10:14,440 --> 00:10:19,030
 is the same as the word we use in Pali for a sandal, when

192
00:10:19,030 --> 00:10:22,440
 you bind together a sandal,

193
00:10:22,440 --> 00:10:27,170
 the bound, the rope of the sandal, and so on. So when you

194
00:10:27,170 --> 00:10:29,440
're tied to these things, when

195
00:10:29,440 --> 00:10:34,280
 you're caught up in these thoughts, when you cling to them,

196
00:10:34,280 --> 00:10:36,440
 basically, this is where your

197
00:10:36,440 --> 00:10:40,670
 problems never cease. Because the point here that I think

198
00:10:40,670 --> 00:10:42,440
 is important is that we do have

199
00:10:42,440 --> 00:10:46,190
 problems. We do have disagreements. I've gotten angry at

200
00:10:46,190 --> 00:10:48,440
 people. We get angry at people. We

201
00:10:48,440 --> 00:10:51,690
 have people do things we don't like, and we get angry at

202
00:10:51,690 --> 00:10:53,440
 them. We get upset at each other.

203
00:10:53,440 --> 00:10:57,450
 A good example is with our families. When our parents tell

204
00:10:57,450 --> 00:10:59,440
 us to do something or our

205
00:10:59,440 --> 00:11:02,340
 parents criticize us, we can become very angry, even to the

206
00:11:02,340 --> 00:11:03,440
 point where we yell at them and

207
00:11:03,440 --> 00:11:07,520
 where we say nasty things and really hurt our parents. It

208
00:11:07,520 --> 00:11:09,440
 can cause greater hurt for

209
00:11:09,440 --> 00:11:13,000
 them or suffering for them, and we suffer ourselves and so

210
00:11:13,000 --> 00:11:14,440
 on. But what we won't do is

211
00:11:14,440 --> 00:11:18,110
 cling, generally speaking, in an ordinary family situation.

212
00:11:18,110 --> 00:11:19,440
 We won't cling to it. So

213
00:11:19,440 --> 00:11:22,320
 the next day or the next moment, we'll forget about it.

214
00:11:22,320 --> 00:11:24,440
 Because we don't have this clinging

215
00:11:24,440 --> 00:11:28,360
 to them. But we aren't this way in all cases. And there is

216
00:11:28,360 --> 00:11:30,440
 the case where, as with this

217
00:11:30,440 --> 00:11:34,570
 monk, Vissa, he clung to it, and he couldn't get over his

218
00:11:34,570 --> 00:11:36,440
 own anger, his own upset at being

219
00:11:36,440 --> 00:11:40,210
 criticized. It's very difficult to be criticized. It's very

220
00:11:40,210 --> 00:11:42,440
 difficult for young monks, new monks,

221
00:11:42,440 --> 00:11:45,890
 to be criticized. Because they can be very proud of the

222
00:11:45,890 --> 00:11:47,440
 fact that now they're in a special

223
00:11:47,440 --> 00:11:50,620
 state, and now they have some kind of special lifestyle,

224
00:11:50,620 --> 00:11:52,440
 and people respect them and look

225
00:11:52,440 --> 00:11:56,440
 up to them and give them free food and shelter and so on.

226
00:11:56,440 --> 00:11:57,440
 And so they can be very proud of

227
00:11:57,440 --> 00:12:01,420
 that instead of putting it to good use and really making

228
00:12:01,420 --> 00:12:03,440
 themselves into something worthy

229
00:12:03,440 --> 00:12:09,700
 of it. So this is where the problem arises, with the conce

230
00:12:09,700 --> 00:12:10,440
it and with the clinging to

231
00:12:10,440 --> 00:12:14,970
 it, where you become self-righteous and you say, "I don't

232
00:12:14,970 --> 00:12:17,440
 deserve that." And so on.

233
00:12:17,440 --> 00:12:21,060
 There's not even the rational thought there where you think

234
00:12:21,060 --> 00:12:22,440
, "Are they right? Are they

235
00:12:22,440 --> 00:12:25,220
 wrong?" And if they're wrong, then you can say, "Well, I

236
00:12:25,220 --> 00:12:26,440
 don't deserve that." And you

237
00:12:26,440 --> 00:12:30,670
 can reply to them. For instance, this Tissa, he had this

238
00:12:30,670 --> 00:12:33,440
 complaint to the Buddha. And he

239
00:12:33,440 --> 00:12:36,150
 said to the Buddha, "These monks have done something nasty

240
00:12:36,150 --> 00:12:38,440
." Well, the Buddha did respond.

241
00:12:38,440 --> 00:12:41,380
 It's not that the Buddha just said, "Oh, yes, yes." Letting

242
00:12:41,380 --> 00:12:43,440
 him walk all over the Buddha,

243
00:12:43,440 --> 00:12:45,430
 walk all over these monks, letting him get away with it.

244
00:12:45,430 --> 00:12:46,440
 The Buddha was quite clear that

245
00:12:46,440 --> 00:12:49,040
 he was wrong. And we can do that if someone criticizes us

246
00:12:49,040 --> 00:12:50,440
 and says, "We did something

247
00:12:50,440 --> 00:12:53,780
 wrong." We can say, "No, I didn't do anything wrong based

248
00:12:53,780 --> 00:12:55,440
 on our rational thought." But we

249
00:12:55,440 --> 00:13:00,320
 don't do this generally. Generally, we don't think about...

250
00:13:00,320 --> 00:13:02,440
 We just... Out of anger, we

251
00:13:02,440 --> 00:13:05,620
 refuse any criticism, become upset, and we become self-

252
00:13:05,620 --> 00:13:08,440
righteous. And it festers in the

253
00:13:08,440 --> 00:13:13,460
 mind. This is what gives rise to grudges and anonymity and

254
00:13:13,460 --> 00:13:17,440
 feuds. War. When we build hate

255
00:13:17,440 --> 00:13:21,080
 in our mind, and it's a self-righteous, egotistical hate,

256
00:13:21,080 --> 00:13:24,440
 this is how religious wars are fought.

257
00:13:24,440 --> 00:13:27,390
 It's not because we're angry at each other. If you look at

258
00:13:27,390 --> 00:13:29,440
 ideological wars in the world

259
00:13:29,440 --> 00:13:33,690
 where this country comes to hate that country, and our

260
00:13:33,690 --> 00:13:36,440
 whole mindset is that this country

261
00:13:36,440 --> 00:13:40,610
 is evil, this country is bad. Many people feel this way

262
00:13:40,610 --> 00:13:43,440
 about America. They've cultivated

263
00:13:43,440 --> 00:13:47,680
 this evil, and that's what happened. All this terrorism

264
00:13:47,680 --> 00:13:50,440
 that's been a fear of the American

265
00:13:50,440 --> 00:13:53,290
 people. To some extent, it's justified because there's a

266
00:13:53,290 --> 00:13:55,440
 lot of people out there who don't

267
00:13:55,440 --> 00:14:00,470
 hate. We just have this anger towards this country as a

268
00:14:00,470 --> 00:14:03,440
 whole, as an example. And you

269
00:14:03,440 --> 00:14:06,770
 have this throughout the world. You have our prejudices

270
00:14:06,770 --> 00:14:09,440
 against people and so on. But we

271
00:14:09,440 --> 00:14:13,150
 build this up quite often, and this is what we have to be

272
00:14:13,150 --> 00:14:15,440
 careful about, because we will

273
00:14:15,440 --> 00:14:18,750
 have fights, we will have disagreements, and we will get

274
00:14:18,750 --> 00:14:20,440
 angry at each other until we get

275
00:14:20,440 --> 00:14:23,920
 to the point where we are able to see that anger is

276
00:14:23,920 --> 00:14:26,440
 unpleasant and unuseful and able

277
00:14:26,440 --> 00:14:31,760
 to get rid of it entirely. But what is dangerous and what

278
00:14:31,760 --> 00:14:34,440
 is a hindrance to our path towards

279
00:14:34,440 --> 00:14:37,940
 giving up the anger is this attachment, this clinging to it

280
00:14:37,940 --> 00:14:39,440
. It covers up the whole nature

281
00:14:39,440 --> 00:14:43,830
 of the anger. It prevents you from actually looking at the

282
00:14:43,830 --> 00:14:45,440
 anger for what it is. When

283
00:14:45,440 --> 00:14:49,160
 you're angry and then you say, "This is right that I'm

284
00:14:49,160 --> 00:14:52,440
 angry. These people deserve my wrath,"

285
00:14:52,440 --> 00:14:57,790
 and so on, then you're justifying it and you're covering it

286
00:14:57,790 --> 00:15:02,440
 up. It's like you have this great

287
00:15:02,440 --> 00:15:07,960
 ... Suppose you have this ball of hot iron or you have this

288
00:15:07,960 --> 00:15:10,440
 hot coal, and you're holding

289
00:15:10,440 --> 00:15:14,440
 it in your hand and it's hurting you. So you grab it and

290
00:15:14,440 --> 00:15:16,440
 you say, "What is it that's hurting

291
00:15:16,440 --> 00:15:20,540
 me? What is it that's hurting me?" You're looking for the

292
00:15:20,540 --> 00:15:22,440
 cause outside of yourself

293
00:15:22,440 --> 00:15:28,140
 when actually it's right here. When this person who's

294
00:15:28,140 --> 00:15:30,440
 causing you to get angry and then you

295
00:15:30,440 --> 00:15:33,260
 say, "You're the cause. You're the reason why I'm suffering

296
00:15:33,260 --> 00:15:34,440
." You're not looking at what

297
00:15:34,440 --> 00:15:37,100
 you're holding here and you're holding the anger. You're

298
00:15:37,100 --> 00:15:38,440
 holding on to these thoughts.

299
00:15:38,440 --> 00:15:40,710
 It's actually these thoughts that's making us suffer. We

300
00:15:40,710 --> 00:15:44,440
 have to be very careful because

301
00:15:44,440 --> 00:15:49,870
 the anger itself is not so dangerous. It's unpleasant, but

302
00:15:49,870 --> 00:15:52,440
 it's not the real problem.

303
00:15:52,440 --> 00:15:58,170
 The danger in the anger is with the ego that comes along

304
00:15:58,170 --> 00:16:01,440
 with it. If we have ego and we

305
00:16:01,440 --> 00:16:05,900
 attach our ego to the anger and it becomes self-righteous

306
00:16:05,900 --> 00:16:07,940
 and every time we think of

307
00:16:07,940 --> 00:16:12,020
 a person we are upset by them, we're angry, we have hate

308
00:16:12,020 --> 00:16:14,240
 towards people, or we have a

309
00:16:14,240 --> 00:16:20,160
 grudge towards people, we're biased against people and so

310
00:16:20,160 --> 00:16:22,240
 on. Every time we see them,

311
00:16:22,240 --> 00:16:25,270
 everything they do will be something we can criticize. We

312
00:16:25,270 --> 00:16:26,880
'll always be looking for they

313
00:16:26,880 --> 00:16:29,700
 did that wrong and so on. When they do things good we'll

314
00:16:29,700 --> 00:16:31,400
 feel upset. When good things are

315
00:16:31,400 --> 00:16:34,400
 done, things happen and we'll be upset. When they act in a

316
00:16:34,400 --> 00:16:36,040
 good way, we'll try to minimize

317
00:16:36,040 --> 00:16:40,240
 it and so on. We'll create great suffering for ourselves.

318
00:16:40,240 --> 00:16:42,960
 This is a very important lesson.

319
00:16:42,960 --> 00:16:46,420
 It's in general a very important lesson with the defile

320
00:16:46,420 --> 00:16:48,480
ments of the mind. They're really

321
00:16:48,480 --> 00:16:52,740
 not that bad. Greed and anger. They're not that big of a

322
00:16:52,740 --> 00:16:55,120
 deal. You can watch them and

323
00:16:55,120 --> 00:16:57,300
 you can look at them, but the problem is we don't. The

324
00:16:57,300 --> 00:16:58,880
 problem is we never learn about

325
00:16:58,880 --> 00:17:02,590
 these things. People think pleasure is such a great thing.

326
00:17:02,590 --> 00:17:04,440
 Attaching to things is so great

327
00:17:04,440 --> 00:17:07,820
 when you have sensual pleasure and sexual pleasure and so

328
00:17:07,820 --> 00:17:09,520
 on. We think it's so great

329
00:17:09,520 --> 00:17:14,360
 because we cling to it. There's the idea that I like this,

330
00:17:14,360 --> 00:17:16,320
 I want this and so on. But if

331
00:17:16,320 --> 00:17:18,430
 we actually just looked at the pleasure when it came up

332
00:17:18,430 --> 00:17:19,840
 instead of saying, "Okay, I have

333
00:17:19,840 --> 00:17:24,070
 to get this." If we just looked at the wanting of it, if we

334
00:17:24,070 --> 00:17:26,160
 just examined it for what it

335
00:17:26,160 --> 00:17:28,960
 is, we see that actually it's quite a bit of stress

336
00:17:28,960 --> 00:17:31,040
 actually. It's this tension. The

337
00:17:31,040 --> 00:17:34,090
 only way you can relieve that tension is either by seeing

338
00:17:34,090 --> 00:17:35,920
 it and letting it go or by chasing

339
00:17:35,920 --> 00:17:39,430
 after and getting what you want and then saying, "Ah, now I

340
00:17:39,430 --> 00:17:41,560
 don't need it anymore." The happiness

341
00:17:41,560 --> 00:17:45,360
 doesn't come from the wanting. It comes from giving up the

342
00:17:45,360 --> 00:17:47,200
 wanting either in one of two

343
00:17:47,200 --> 00:17:52,480
 ways. The problem of course with following it is that you

344
00:17:52,480 --> 00:17:54,760
're encouraging it and you're

345
00:17:54,760 --> 00:18:00,840
 helping it more and more. You're clinging more and more.

346
00:18:00,840 --> 00:18:02,960
 There's no understanding of

347
00:18:02,960 --> 00:18:07,050
 the problem. The same goes with anger. When you're angry at

348
00:18:07,050 --> 00:18:08,920
 someone, it's suffering,

349
00:18:08,920 --> 00:18:14,280
 yes, but it's actually not such a big deal until you do

350
00:18:14,280 --> 00:18:17,920
 what this is. You come to identify

351
00:18:17,920 --> 00:18:21,180
 with the anger and identify a source of the anger in

352
00:18:21,180 --> 00:18:23,720
 another person. They are the cause

353
00:18:23,720 --> 00:18:27,340
 of the problem. The problem is not the anger. The anger

354
00:18:27,340 --> 00:18:29,400
 problem is the other person. The

355
00:18:29,400 --> 00:18:32,260
 real problem is our delusion and our ignorance. This is why

356
00:18:32,260 --> 00:18:33,920
 the Buddha said, "Avijja parceya

357
00:18:33,920 --> 00:18:37,560
 sankhara." All sankhara, all formations in the mind, all

358
00:18:37,560 --> 00:18:39,480
 judgments. There's sankhara

359
00:18:39,480 --> 00:18:44,400
 here means our judgments, our partialities, our projections

360
00:18:44,400 --> 00:18:47,320
 that we replace on things.

361
00:18:47,320 --> 00:18:51,670
 These are all caused by ignorance. It's ignorance that is

362
00:18:51,670 --> 00:18:53,440
 the root. That's really what this

363
00:18:53,440 --> 00:18:59,280
 verse is talking about. It's the delusion. It's the

364
00:18:59,280 --> 00:19:03,120
 identification with these thoughts

365
00:19:03,120 --> 00:19:07,230
 that is the problem. Our defilements are in different

366
00:19:07,230 --> 00:19:11,360
 levels. There is the misperception

367
00:19:11,360 --> 00:19:16,560
 of something as pleasant or unpleasant, which comes to us

368
00:19:16,560 --> 00:19:20,520
 all until you get to fully enlightened.

369
00:19:20,520 --> 00:19:22,770
 You still will have this perception of something as being

370
00:19:22,770 --> 00:19:24,320
 pleasant or unpleasant. Something

371
00:19:24,320 --> 00:19:27,450
 comes, "Oh, that's nice." Something comes, "Oh, that's not

372
00:19:27,450 --> 00:19:28,920
 nice." This is just a feeling

373
00:19:28,920 --> 00:19:31,860
 that you get. Something is, "Oh, this experience is

374
00:19:31,860 --> 00:19:34,560
 pleasant. This experience is unpleasant."

375
00:19:34,560 --> 00:19:37,350
 The next level is our thoughts. This is what he's talking

376
00:19:37,350 --> 00:19:38,920
 about here, is this thought,

377
00:19:38,920 --> 00:19:42,590
 "Oh, this is unthinking. That's unpleasant thinking. This

378
00:19:42,590 --> 00:19:44,560
 is pleasant." So a person

379
00:19:44,560 --> 00:19:47,450
 says something to you and thinks, "Oh, he's hurting me." "

380
00:19:47,450 --> 00:19:49,280
Oh, he's abusing me." This

381
00:19:49,280 --> 00:19:54,340
 person is saying nasty things. This is at the thought level

382
00:19:54,340 --> 00:19:56,560
. The worst one is the views,

383
00:19:56,560 --> 00:19:59,750
 when you have the view about it. "That is good. This is bad

384
00:19:59,750 --> 00:20:01,640
. Pleasure is good. Pleasure

385
00:20:01,640 --> 00:20:10,180
 is bad." Pain is bad. This is when you identify, when you

386
00:20:10,180 --> 00:20:13,040
 build up this "I" and you're

387
00:20:13,040 --> 00:20:17,920
 like, "You're us and them, me and you hurt me," and so on.

388
00:20:17,920 --> 00:20:19,920
 When we build this up, this

389
00:20:19,920 --> 00:20:23,290
 is what really, really causes problems. Because, as I said,

390
00:20:23,290 --> 00:20:25,680
 it obscures the nature of the experience.

391
00:20:25,680 --> 00:20:28,620
 The only way we can come to be free is if we are able to

392
00:20:28,620 --> 00:20:30,400
 see the nature of the anger,

393
00:20:30,400 --> 00:20:33,350
 the nature of the greed, and be able to work out for

394
00:20:33,350 --> 00:20:35,440
 ourselves what is truly right for

395
00:20:35,440 --> 00:20:38,650
 us. The only way we can work out what is truly right is by

396
00:20:38,650 --> 00:20:40,480
 seeing the individual experience.

397
00:20:40,480 --> 00:20:46,750
 We can't do this if we're clinging. We can't do this if we

398
00:20:46,750 --> 00:20:50,560
 have some idea or identification

399
00:20:50,560 --> 00:20:56,640
 with the object as me and mine and so on. I think I've gone

400
00:20:56,640 --> 00:20:59,360
 over that enough. This is

401
00:20:59,360 --> 00:21:03,530
 a fairly important teaching. I think this will help us in

402
00:21:03,530 --> 00:21:05,840
 our practice, because it also

403
00:21:05,840 --> 00:21:10,090
 helps us to accept and to allow to come up the bad things

404
00:21:10,090 --> 00:21:12,560
 in our mind. Greed and anger

405
00:21:12,560 --> 00:21:14,980
 are bad, but the only way we're going to come to see that

406
00:21:14,980 --> 00:21:16,960
 they're really and truly bad is

407
00:21:16,960 --> 00:21:20,970
 if we let them come up. We can't just say, "It's bad. It's

408
00:21:20,970 --> 00:21:22,920
 good," until we have wisdom

409
00:21:22,920 --> 00:21:26,080
 and are able to see clearly, "What is it? Is it good? Is it

410
00:21:26,080 --> 00:21:27,600
 bad?" Not from here, thinking

411
00:21:27,600 --> 00:21:30,700
 and so on, and not because I say it or because the book

412
00:21:30,700 --> 00:21:32,680
 says it, but simply by seeing it

413
00:21:32,680 --> 00:21:35,620
 for yourself. You don't have this view that, "Well, I think

414
00:21:35,620 --> 00:21:37,200
 anger is good. Well, I think

415
00:21:37,200 --> 00:21:41,310
 greed is good," or you don't have the idea that, "Oh, Yuta

416
00:21:41,310 --> 00:21:43,520
 Damo said it's bad. Yuta Damo

417
00:21:43,520 --> 00:21:46,800
 said it's good," and so on. You know for yourself, and only

418
00:21:46,800 --> 00:21:48,600
 when you know for yourself are you

419
00:21:48,600 --> 00:21:53,270
 going to be able to let go of it and overcome it. You will

420
00:21:53,270 --> 00:21:54,600
 never be able to do this, as

421
00:21:54,600 --> 00:21:57,680
 I said again and again now, never be able to do this if you

422
00:21:57,680 --> 00:21:59,440
're clinging to it, if you're

423
00:21:59,440 --> 00:22:03,980
 holding on to it, and therefore, "Weirang deisang nesamatih

424
00:22:03,980 --> 00:22:06,440
," the quarrels, the problems that

425
00:22:06,440 --> 00:22:09,560
 that person encounters will never cease. So thanks for

426
00:22:09,560 --> 00:22:11,160
 tuning in. This has been another

427
00:22:11,160 --> 00:22:12,360
 verse of All the Best.

