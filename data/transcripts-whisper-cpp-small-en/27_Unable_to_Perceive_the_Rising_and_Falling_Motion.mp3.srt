1
00:00:00,000 --> 00:00:05,840
 unable to perceive the rising and falling motion.

2
00:00:05,840 --> 00:00:10,620
 What if one is unable to perceive the rising and falling

3
00:00:10,620 --> 00:00:12,560
 motion of the abdomen?

4
00:00:12,560 --> 00:00:16,640
 It is common for new meditators, especially those who are

5
00:00:16,640 --> 00:00:19,600
 exceptionally tense in body and mind,

6
00:00:19,600 --> 00:00:22,680
 to have difficulty breathing naturally.

7
00:00:22,680 --> 00:00:26,960
 Meditators should rest assured that with practice the body

8
00:00:26,960 --> 00:00:30,520
 and mind who relax sufficiently to allow ordinary

9
00:00:30,520 --> 00:00:33,840
 observation of the movements of the abdomen.

10
00:00:33,840 --> 00:00:37,010
 It may help to place a hand on the abdomen during

11
00:00:37,010 --> 00:00:41,120
 meditation to familiarize oneself with the movements.

12
00:00:41,120 --> 00:00:45,200
 If this does not work, one can practice meditation in the

13
00:00:45,200 --> 00:00:48,700
 lying position where the movements should be clearly

14
00:00:48,700 --> 00:00:49,760
 discernible.

15
00:00:49,760 --> 00:00:53,570
 If the movements of the abdomen are temporarily inters

16
00:00:53,570 --> 00:00:58,240
ensible, one may note sitting, sitting instead.

