1
00:00:00,000 --> 00:00:06,490
 Hello, welcome back to Ask a Monk. Next question comes from

2
00:00:06,490 --> 00:00:08,880
 Scott who asks, "Would you mind telling me what the precise

3
00:00:08,880 --> 00:00:14,530
 holidays in Theravada tradition are? I know Uposata, Makabu

4
00:00:14,530 --> 00:00:20,740
ja, Vaisak, Dhamma day, Kavarana day and Anapanasati day. Am

5
00:00:20,740 --> 00:00:22,000
 I missing others?"

6
00:00:25,000 --> 00:00:28,880
 Well, first of all, I wouldn't worry too much about it

7
00:00:28,880 --> 00:00:32,530
 because obviously every day that you're mindful that you're

8
00:00:32,530 --> 00:00:36,000
 practicing the Dhamma is a holiday, a holy day.

9
00:00:37,000 --> 00:00:43,270
 The Buddhist holidays are only a way of marking time so

10
00:00:43,270 --> 00:00:49,230
 that every so often we can get together and remember and be

11
00:00:49,230 --> 00:00:56,000
 mindful of the Buddhist teaching and practice intensively.

12
00:00:57,000 --> 00:01:01,750
 The Buddhist holiday is the Uposa today. And in regards to

13
00:01:01,750 --> 00:01:06,160
 the Uposa today, I think this is a clear example of how we

14
00:01:06,160 --> 00:01:10,960
 shouldn't be too caught up on the idea of specific holidays

15
00:01:10,960 --> 00:01:11,000
.

16
00:01:12,000 --> 00:01:17,680
 Uposata days are the lunar cycle, the full moon, the empty

17
00:01:17,680 --> 00:01:23,270
 moon, the half moon. These are more in India considered

18
00:01:23,270 --> 00:01:27,000
 special days. So this is how they mark time.

19
00:01:28,000 --> 00:01:36,660
 The Buddha's teaching is the 7 day week, the 12 month year,

20
00:01:36,660 --> 00:01:43,300
 which has nothing to do with the Buddha's teaching. There's

21
00:01:43,300 --> 00:01:45,000
 nothing in the Buddha's teaching about it.

22
00:01:46,000 --> 00:01:48,730
 There's no mention of how we should deal with this because

23
00:01:48,730 --> 00:01:51,500
 it's something that developed much later. It's a very

24
00:01:51,500 --> 00:01:53,000
 artificial system as well.

25
00:01:53,000 --> 00:01:56,600
 It's even more artificial because these months just sort of

26
00:01:56,600 --> 00:02:00,320
 sprung up and adding months, subtracting months and fitting

27
00:02:00,320 --> 00:02:03,000
 the weeks in there sort of haphazardly.

28
00:02:03,000 --> 00:02:06,650
 It's really a fairly chaotic and useless system, but it

29
00:02:06,650 --> 00:02:10,050
 happens to be the system in use and it happens to have a

30
00:02:10,050 --> 00:02:12,000
 great pull on our lives.

31
00:02:13,000 --> 00:02:18,830
 So what's the answer? Because we have a problem here. These

32
00:02:18,830 --> 00:02:25,840
 holidays sometimes, often times, arrive on weekdays when

33
00:02:25,840 --> 00:02:28,000
 most people are working.

34
00:02:28,000 --> 00:02:30,880
 So even here in Buddhist countries, it's very difficult for

35
00:02:30,880 --> 00:02:33,860
 people to come for the Buddhist holidays, the full moon day

36
00:02:33,860 --> 00:02:36,990
, which is very important here in Sri Lanka. Many people can

37
00:02:36,990 --> 00:02:38,000
't observe it.

38
00:02:39,000 --> 00:02:43,440
 So what's to be done? I think the key is what's been done

39
00:02:43,440 --> 00:02:48,240
 in non-Buddhist countries where they'll have the holiday on

40
00:02:48,240 --> 00:02:52,000
 a weekend, of course, a Saturday or a Sunday.

41
00:02:52,000 --> 00:02:56,300
 Even in non-sectarian or non-religious Buddhist circles,

42
00:02:56,300 --> 00:02:59,940
 they'll have days of mindfulness. And of course, they do it

43
00:02:59,940 --> 00:03:01,000
 on a Saturday or a Sunday.

44
00:03:02,000 --> 00:03:04,830
 They might even have a weekend course where they do Friday,

45
00:03:04,830 --> 00:03:09,050
 Saturday, Sunday. And I think that is crucial in continuing

46
00:03:09,050 --> 00:03:11,000
 the Buddhist teaching.

47
00:03:11,000 --> 00:03:15,330
 It's never going to survive this tradition of laypeople

48
00:03:15,330 --> 00:03:19,950
 coming to the temple, the monastery, excuse me, on a full

49
00:03:19,950 --> 00:03:21,000
 moon day.

50
00:03:22,000 --> 00:03:25,960
 Now, that being said, these are important from a monastic

51
00:03:25,960 --> 00:03:30,230
 point of view. Obviously, we have no such restrictions. So

52
00:03:30,230 --> 00:03:34,670
 we do observe the full moon, the empty moon, as our

53
00:03:34,670 --> 00:03:39,590
 religious day is where we're to get together and to recite

54
00:03:39,590 --> 00:03:46,510
 the rules of the monastic community and maybe listen to D

55
00:03:46,510 --> 00:03:50,000
hamma teaching and discussion and so on.

56
00:03:51,000 --> 00:03:55,020
 But for laypeople, I think it's much more profitable to fit

57
00:03:55,020 --> 00:03:57,000
 it in with their work cycle.

58
00:03:57,000 --> 00:04:00,660
 Every Sunday, people should come to the monastery or

59
00:04:00,660 --> 00:04:04,240
 Saturday or whatever you want to choose, both Saturday and

60
00:04:04,240 --> 00:04:05,000
 Sunday.

61
00:04:05,000 --> 00:04:10,710
 I know in Western countries, they'll do this. The Sri Lank

62
00:04:10,710 --> 00:04:14,220
an monasteries will have Sunday school, Buddhist Sunday

63
00:04:14,220 --> 00:04:18,000
 school, which is great. That's crucial.

64
00:04:19,000 --> 00:04:21,620
 Here in Sri Lanka, they do have a Saturday Dhamma school. I

65
00:04:21,620 --> 00:04:25,970
 go to teach at it sometimes. But they still are doing the P

66
00:04:25,970 --> 00:04:31,930
oya day, which is fine. But I think it's not the most

67
00:04:31,930 --> 00:04:35,880
 important. It was not a Buddhist concept. It was something

68
00:04:35,880 --> 00:04:41,000
 that the Buddha used because that was the tradition.

69
00:04:42,000 --> 00:04:50,000
 So that's the first and foremost is this weekly Uposata,

70
00:04:50,000 --> 00:04:54,040
 which we should adopt. But I think we should adopt it in a

71
00:04:54,040 --> 00:04:56,000
 way that's suitable for us.

72
00:04:57,000 --> 00:05:01,110
 Every Saturday you can pick or every Sunday where you are

73
00:05:01,110 --> 00:05:05,480
 going to keep the Uposata Sila, the eight precepts of celib

74
00:05:05,480 --> 00:05:09,810
acy, not eating in the afternoon, eating only in the morning

75
00:05:09,810 --> 00:05:16,540
, not listening to entertainment, not indulging in

76
00:05:16,540 --> 00:05:21,910
 entertainment or beautification and sleeping on the floor,

77
00:05:21,910 --> 00:05:24,000
 not sleeping in a bed.

78
00:05:25,000 --> 00:05:29,100
 Do that on a Saturday. Do that for 24 hours. Start Friday

79
00:05:29,100 --> 00:05:34,430
 night and Saturday night. You start Saturday morning before

80
00:05:34,430 --> 00:05:40,200
 dawn and finish Sunday morning after dawn. So you can be

81
00:05:40,200 --> 00:05:44,000
 sure that you have 24 hours. You have a full day.

82
00:05:45,000 --> 00:05:49,650
 But as far as the tradition goes, if you want to know about

83
00:05:49,650 --> 00:05:55,130
 the holidays, the ones that are generally agreed upon are

84
00:05:55,130 --> 00:05:58,850
 three, the Visaka Puja, the Full Moon of Visaka, which is

85
00:05:58,850 --> 00:06:01,000
 called Wesak, but Pali is Visaka.

86
00:06:02,000 --> 00:06:06,190
 The Full Moon of Asalha, which you might be referring to by

87
00:06:06,190 --> 00:06:10,450
 Dhamma day, it's when the Buddha first taught the Full Moon

88
00:06:10,450 --> 00:06:14,520
 of Asalha and the Full Moon of Maga, which is the day when

89
00:06:14,520 --> 00:06:18,000
 the Buddha apparently gave the Ova to Patimoka.

90
00:06:19,000 --> 00:06:24,230
 These three days, the Visaka Puja is the day when the Bod

91
00:06:24,230 --> 00:06:29,490
hisatta was born, the Bodhisatta became the Buddha, became

92
00:06:29,490 --> 00:06:34,660
 enlightened, and the day that he passed away into Parinibb

93
00:06:34,660 --> 00:06:35,000
ana.

94
00:06:36,000 --> 00:06:52,080
 Asalha has when he first taught the Dhamma, Chaka, Pawatana

95
00:06:52,080 --> 00:06:52,710
, Sudha when he gave his first discourse and someone who

96
00:06:52,710 --> 00:06:53,310
 first became a Sotapanan enlightened being or came to

97
00:06:53,310 --> 00:06:54,000
 understand the Dhamma even on a basic level.

98
00:06:55,000 --> 00:06:59,580
 Asalha Puja, the Ova to Patimoka, which is the core of the

99
00:06:59,580 --> 00:07:03,180
 Buddha's teaching, the refraining from bad deeds, the

100
00:07:03,180 --> 00:07:06,950
 development of good deeds and the purification of mind,

101
00:07:06,950 --> 00:07:09,000
 this is the teaching of all the Buddhas.

102
00:07:10,000 --> 00:07:15,630
 This was what the Buddha taught as the basic exhortation,

103
00:07:15,630 --> 00:07:21,100
 the teaching that we should keep in mind as fundamental. It

104
00:07:21,100 --> 00:07:24,000
's a little bit longer than that, but it's worth looking up.

105
00:07:25,000 --> 00:07:29,240
 Other holidays like Puwarana Day and Anapanasati Day, I

106
00:07:29,240 --> 00:07:33,700
 think they're more local and changes how they're observed,

107
00:07:33,700 --> 00:07:39,000
 whether they're observed. In Thai there's a Devo Rohana,

108
00:07:39,000 --> 00:07:44,000
 which is the day when the Buddha came down from heaven.

109
00:07:45,000 --> 00:07:48,160
 And here in Sri Lanka there's the day when Buddhism first

110
00:07:48,160 --> 00:07:51,190
 came to Sri Lanka, which is of course only observed here in

111
00:07:51,190 --> 00:07:55,080
 Sri Lanka. But I think these are cultural and it's often

112
00:07:55,080 --> 00:07:59,000
 quite disappointing to see how they're carried out.

113
00:08:00,000 --> 00:08:04,670
 There's great pomp and celebration and music and many

114
00:08:04,670 --> 00:08:09,860
 things that are actually contrary to the Buddha's teaching

115
00:08:09,860 --> 00:08:14,270
 of introspection and sobriety and the scientific

116
00:08:14,270 --> 00:08:17,000
 investigation of reality.

117
00:08:18,000 --> 00:08:22,590
 So I wouldn't concern too much about the holidays. It's

118
00:08:22,590 --> 00:08:29,560
 good to know the three so that you can use them as a yearly

119
00:08:29,560 --> 00:08:35,000
 opportunity to think about the Buddha.

120
00:08:36,000 --> 00:08:40,340
 But I would say it's much more important that we practice

121
00:08:40,340 --> 00:08:44,590
 daily the Buddha's teaching and at least weekly that we

122
00:08:44,590 --> 00:08:48,710
 have some sort of a special day where we undertake the

123
00:08:48,710 --> 00:08:52,000
 precepts on a more fundamental level.

124
00:08:53,000 --> 00:08:55,420
 So I hope that helps. I'm not fully able to answer your

125
00:08:55,420 --> 00:08:58,100
 question. I'm not able to give a list. I don't think such a

126
00:08:58,100 --> 00:09:00,860
 list exists, but for each country they do have their list.

127
00:09:00,860 --> 00:09:07,060
 It's just not high on my... it's not something that I keep

128
00:09:07,060 --> 00:09:11,050
 in my memory except for the major ones which I think you've

129
00:09:11,050 --> 00:09:14,000
 covered. So I hope that helps. All the best.

