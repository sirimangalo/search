1
00:00:00,000 --> 00:00:15,000
 [silence]

2
00:00:15,000 --> 00:00:17,000
 Good evening everyone.

3
00:00:17,000 --> 00:00:19,000
 Oh.

4
00:00:19,000 --> 00:00:21,000
 Can you hear me?

5
00:00:21,000 --> 00:00:22,000
 Hello?

6
00:00:22,000 --> 00:00:24,000
 Oh, good evening everyone.

7
00:00:28,000 --> 00:00:30,000
 Smaller group tonight.

8
00:00:30,000 --> 00:00:40,000
 I don't know last night's talk.

9
00:00:40,000 --> 00:00:42,000
 I gave a talk about dumb.

10
00:00:42,000 --> 00:00:44,000
 The word dumb.

11
00:00:44,000 --> 00:00:47,000
 I think it was particularly awful.

12
00:00:47,000 --> 00:00:52,000
 I got several down votes already on YouTube.

13
00:00:52,000 --> 00:00:55,000
 Today half of our crowd is missing.

14
00:00:56,000 --> 00:00:59,070
 I don't know what was wrong with it, but I've driven

15
00:00:59,070 --> 00:01:00,000
 everyone away.

16
00:01:00,000 --> 00:01:05,000
 There could of course be another explanation.

17
00:01:05,000 --> 00:01:12,000
 I didn't think it was particularly awful, but...

18
00:01:12,000 --> 00:01:16,000
 I don't know the internet, no?

19
00:01:16,000 --> 00:01:23,000
 The past is not important.

20
00:01:23,000 --> 00:01:26,000
 I should never dwell on the past.

21
00:01:26,000 --> 00:01:29,000
 Nor worry about the future.

22
00:01:29,000 --> 00:01:35,000
 The...

23
00:01:35,000 --> 00:01:39,000
 Simon thinks yesterday's talk was awesome, but...

24
00:01:39,000 --> 00:01:42,000
 He thinks all my talks are awesome.

25
00:01:42,000 --> 00:01:57,000
 [silence]

26
00:01:57,000 --> 00:02:00,000
 Anyway, today's talk is about the present moment.

27
00:02:00,000 --> 00:02:13,000
 [silence]

28
00:02:13,000 --> 00:02:15,000
 The present moment.

29
00:02:15,000 --> 00:02:18,740
 There's a book that everyone says I should read or everyone

30
00:02:18,740 --> 00:02:20,000
 asks me about.

31
00:02:20,000 --> 00:02:23,760
 I'm constantly asked about this book called The Power of

32
00:02:23,760 --> 00:02:24,000
 Now.

33
00:02:26,000 --> 00:02:30,000
 By...

34
00:02:30,000 --> 00:02:35,000
 By this famous author, speaker, motivation dude.

35
00:02:35,000 --> 00:02:39,000
 Or spiritual guide, I don't know, Eckhart Tolle.

36
00:02:39,000 --> 00:02:43,000
 I think he's...

37
00:02:43,000 --> 00:02:46,000
 Something to do with Oprah Winfrey or something.

38
00:02:46,000 --> 00:02:53,000
 The Power of Now.

39
00:02:53,000 --> 00:02:56,000
 He's right though, that's a good title for a book.

40
00:02:56,000 --> 00:03:00,000
 It's a good subject. It's a good phrase.

41
00:03:00,000 --> 00:03:06,000
 It's not to be...

42
00:03:06,000 --> 00:03:11,000
 Not to be taken lightly.

43
00:03:11,000 --> 00:03:27,000
 [silence]

44
00:03:27,000 --> 00:03:29,000
 There's a story...

45
00:03:29,000 --> 00:03:35,620
 Well, there's a verse that actually appears in a couple of

46
00:03:35,620 --> 00:03:37,000
 places, but...

47
00:03:38,000 --> 00:03:41,000
 One well-known place is in the Temiya Jataka.

48
00:03:41,000 --> 00:03:45,160
 If you don't know about the Temiya Jataka, it's a long

49
00:03:45,160 --> 00:03:47,000
 story. It's a really good story.

50
00:03:47,000 --> 00:03:53,000
 It's one of the ten great Jatakas. Great meaning...

51
00:03:53,000 --> 00:03:56,000
 Meaning more that it has a lot of verses, but...

52
00:03:56,000 --> 00:04:00,000
 Also great in that it's...

53
00:04:00,000 --> 00:04:03,000
 It's one of the more powerful moving stories.

54
00:04:05,000 --> 00:04:08,000
 And the story goes that there was this prince who...

55
00:04:08,000 --> 00:04:12,000
 Even before he was born, he was an angel in heaven.

56
00:04:12,000 --> 00:04:17,000
 This king on earth needed a son, and the king of the gods,

57
00:04:17,000 --> 00:04:18,000
 the king of the angels,

58
00:04:18,000 --> 00:04:21,000
 persuaded the bodhisatta to be reborn...

59
00:04:21,000 --> 00:04:25,000
 As a human being.

60
00:04:25,000 --> 00:04:29,080
 I guess angels can sort of decide where they want to be

61
00:04:29,080 --> 00:04:30,000
 reborn.

62
00:04:30,000 --> 00:04:36,000
 [silence]

63
00:04:36,000 --> 00:04:40,000
 And so he was reborn as this prince, but...

64
00:04:40,000 --> 00:04:48,000
 When he was very young, he was sitting on his father's lap,

65
00:04:48,000 --> 00:04:51,080
 and his father was sitting in judgment, and his robbers

66
00:04:51,080 --> 00:04:52,000
 came to the king,

67
00:04:52,000 --> 00:04:59,010
 and the king sentenced them all to being stabbed with

68
00:04:59,010 --> 00:05:02,000
 spears, impaled upon stakes,

69
00:05:02,000 --> 00:05:06,000
 having their heads cut off, their hands cut off, and so on.

70
00:05:06,000 --> 00:05:10,000
 Typical king stuff.

71
00:05:10,000 --> 00:05:16,000
 But the bodhisatta lying there was just horrified.

72
00:05:18,000 --> 00:05:22,000
 This young boy...

73
00:05:22,000 --> 00:05:24,000
 And as he was sitting there, he...

74
00:05:24,000 --> 00:05:27,400
 It became all very familiar to him, and he remembered his

75
00:05:27,400 --> 00:05:28,000
 past life,

76
00:05:28,000 --> 00:05:32,390
 that in the past he had been a king, and he had done this

77
00:05:32,390 --> 00:05:34,000
 same sort of thing.

78
00:05:34,000 --> 00:05:43,470
 He had engaged in sentencing robbers, and really being a

79
00:05:43,470 --> 00:05:45,000
 fairly nasty person.

80
00:05:45,000 --> 00:05:47,000
 [sniffing]

81
00:05:47,000 --> 00:05:51,550
 Whether the people deserved it or not, engaging in all

82
00:05:51,550 --> 00:05:56,000
 sorts of nasty torture and punishment.

83
00:05:56,000 --> 00:06:00,000
 And as a result, had been reborn in hell in his next life.

84
00:06:00,000 --> 00:06:07,000
 And he had lived in hell for many years, and he was just

85
00:06:07,000 --> 00:06:09,000
 starting to get on the way up,

86
00:06:09,000 --> 00:06:14,000
 when he made it to the lower angel realms.

87
00:06:15,000 --> 00:06:18,570
 And he said, "What am I doing back here? I'm back where I

88
00:06:18,570 --> 00:06:19,000
 started.

89
00:06:19,000 --> 00:06:22,240
 One day I'll be king, and I'll have to do that very same

90
00:06:22,240 --> 00:06:23,000
 thing."

91
00:06:23,000 --> 00:06:31,000
 And so he decided that he would pretend to be crippled,

92
00:06:31,000 --> 00:06:34,300
 he would pretend to be dumb, he would pretend to be an

93
00:06:34,300 --> 00:06:35,000
 invalid.

94
00:06:35,000 --> 00:06:41,100
 And the story goes on and on, and for 16 years, he made as

95
00:06:41,100 --> 00:06:43,000
 though he couldn't see or hear,

96
00:06:43,000 --> 00:06:47,000
 or he was deaf and dumb and mute and stupid.

97
00:06:47,000 --> 00:06:51,470
 But the doctors looked at him and they said there's nothing

98
00:06:51,470 --> 00:06:52,000
 wrong,

99
00:06:52,000 --> 00:06:54,710
 so they tried to test him, and they tested him in all these

100
00:06:54,710 --> 00:06:58,000
 ways by putting,

101
00:06:58,000 --> 00:07:01,280
 when he was sitting in his playhouse, they would light, set

102
00:07:01,280 --> 00:07:02,000
 fire to it,

103
00:07:02,000 --> 00:07:05,720
 put him in this playhouse and then set fire to it, thinking

104
00:07:05,720 --> 00:07:07,000
 that he'd run out.

105
00:07:07,000 --> 00:07:10,000
 But he'd think to himself, "The fires of hell are,

106
00:07:10,000 --> 00:07:13,000
 these fires are nothing compared to the fires of hell."

107
00:07:13,000 --> 00:07:16,720
 And he would lie there and be prepared to burn, and so they

108
00:07:16,720 --> 00:07:18,000
'd put the fires out.

109
00:07:18,000 --> 00:07:22,000
 Anyway, eventually he leaves home.

110
00:07:22,000 --> 00:07:28,800
 Eventually they decide they're going to have to just get

111
00:07:28,800 --> 00:07:30,000
 rid of him,

112
00:07:30,000 --> 00:07:34,080
 and so they're going to go and bury him, and kill him and

113
00:07:34,080 --> 00:07:35,000
 just bury him.

114
00:07:36,000 --> 00:07:39,870
 When he gets out of the city, where this guy's going to go

115
00:07:39,870 --> 00:07:41,000
 and kill him,

116
00:07:41,000 --> 00:07:45,700
 he says, "Oh, here I am out," and they all think I'm going

117
00:07:45,700 --> 00:07:46,000
 to die,

118
00:07:46,000 --> 00:07:49,000
 so he just wanders off and goes and lives in the forest.

119
00:07:49,000 --> 00:07:55,460
 Eventually the king and all the queen and all of their ret

120
00:07:55,460 --> 00:07:56,000
inue,

121
00:07:56,000 --> 00:07:58,000
 and the whole city follows him.

122
00:07:58,000 --> 00:08:01,600
 It's a long story. I really don't want to get into the

123
00:08:01,600 --> 00:08:02,000
 details.

124
00:08:02,000 --> 00:08:04,450
 But when they hear he's gone forth, the king says he's

125
00:08:04,450 --> 00:08:05,000
 going to go forth.

126
00:08:05,000 --> 00:08:08,910
 The queen follows him, and the quarters and the royal

127
00:08:08,910 --> 00:08:11,000
 palace all decide to follow him.

128
00:08:11,000 --> 00:08:16,000
 When the populace hears about it, they all follow the king,

129
00:08:16,000 --> 00:08:20,000
 and everybody decides to go off into the forest,

130
00:08:20,000 --> 00:08:22,000
 and they set up a monastery in Ashram.

131
00:08:22,000 --> 00:08:26,000
 But when they get to the forest to find the prince,

132
00:08:26,000 --> 00:08:31,740
 they find that he's wearing some kind of bark cloth or

133
00:08:31,740 --> 00:08:33,000
 something,

134
00:08:34,000 --> 00:08:37,000
 sitting, and it's quite alert.

135
00:08:37,000 --> 00:08:39,000
 They talk to him and they say, "You're not an invalid."

136
00:08:39,000 --> 00:08:43,000
 He says, "No, no. I just knew I couldn't be king,

137
00:08:43,000 --> 00:08:46,000
 and this was my way of getting out of it."

138
00:08:46,000 --> 00:08:48,000
 They asked him about his life.

139
00:08:48,000 --> 00:08:50,890
 Anyway, the point of this story is it comes to this verse

140
00:08:50,890 --> 00:08:52,000
 where they ask him,

141
00:08:52,000 --> 00:08:57,330
 "How is it you were a prince and we had to feed you and bat

142
00:08:57,330 --> 00:08:58,000
he you,

143
00:08:58,000 --> 00:09:00,000
 and you lived in such luxury?"

144
00:09:02,000 --> 00:09:04,880
 "But here you are living in the forest, and yet you look

145
00:09:04,880 --> 00:09:06,000
 radiant.

146
00:09:06,000 --> 00:09:08,000
 You look more alive than ever."

147
00:09:08,000 --> 00:09:11,000
 Then they asked him what he was eating,

148
00:09:11,000 --> 00:09:15,700
 and he found that he was just eating simple leaves and the

149
00:09:15,700 --> 00:09:18,000
 corsest of course food.

150
00:09:18,000 --> 00:31:56,220
 And so the king asked him, "How on this food can you subs

151
00:31:56,220 --> 00:09:29,000
ist and look so wonderful,

152
00:09:29,000 --> 00:09:31,000
 so alive?"

153
00:09:31,000 --> 00:09:39,000
 And so he told this rather poignant verse.

154
00:09:39,000 --> 00:09:42,760
 He said, "For the past I do not mourn nor for the future

155
00:09:42,760 --> 00:09:43,000
 weep.

156
00:09:43,000 --> 00:09:47,000
 I take the present as it comes and thus my color keep."

157
00:09:47,000 --> 00:10:03,000
 [silence]

158
00:10:03,000 --> 00:10:04,000
 There's another end.

159
00:10:04,000 --> 00:10:08,000
 Almost the same verse appears somewhere else where an angel

160
00:10:08,000 --> 00:10:10,000
 comes to see the Buddha, I think,

161
00:10:10,000 --> 00:10:20,000
 and asks why the monks are...

162
00:10:20,000 --> 00:10:23,000
 I don't know, maybe this is the same jataka I get.

163
00:10:23,000 --> 00:10:26,000
 Anyway, this is the verse.

164
00:10:33,000 --> 00:10:44,000
 It highlights this important concept of the present moment.

165
00:10:44,000 --> 00:10:47,000
 And then the next verse, the verse after it says,

166
00:10:47,000 --> 00:10:53,390
 "When you worry about the past or for some uncertain future

167
00:10:53,390 --> 00:10:54,000
 need,

168
00:10:54,000 --> 00:11:05,430
 it dries a fool's color up as when you cut a fresh green re

169
00:11:05,430 --> 00:11:07,000
ed."

170
00:11:07,000 --> 00:11:14,210
 So this is the other side of the equation or the other side

171
00:11:14,210 --> 00:11:17,000
 of the coin.

172
00:11:17,000 --> 00:11:23,000
 The present moment is such a powerful concept or reality

173
00:11:23,000 --> 00:11:27,560
 because it's real, because the present moment is what is

174
00:11:27,560 --> 00:11:31,000
 really and truly occurring,

175
00:11:31,000 --> 00:11:38,000
 which really and truly exists.

176
00:11:38,000 --> 00:11:40,530
 Why the past and the future are inferior, and they are

177
00:11:40,530 --> 00:11:41,000
 inferior,

178
00:11:41,000 --> 00:11:47,080
 why they are a cause of so much problem is because they don

179
00:11:47,080 --> 00:11:48,000
't exist,

180
00:11:48,000 --> 00:11:54,000
 because they arise conceptually in the mind.

181
00:11:54,000 --> 00:12:09,640
 And so the power, the amount of energy it takes to live in

182
00:12:09,640 --> 00:12:10,000
 the past

183
00:12:10,000 --> 00:12:14,140
 and to live in the future is far more, far greater than in

184
00:12:14,140 --> 00:12:15,000
 the present.

185
00:12:15,000 --> 00:12:18,610
 And on top of that, it's much more complicated or it's much

186
00:12:18,610 --> 00:12:20,000
 more open to complication

187
00:12:20,000 --> 00:12:22,000
 because it doesn't exist.

188
00:12:22,000 --> 00:12:27,450
 The nature of concepts is they can be infinite, they are

189
00:12:27,450 --> 00:12:28,000
 infinite.

190
00:12:28,000 --> 00:12:31,000
 And you can always add something to them.

191
00:12:31,000 --> 00:12:33,000
 Imagination is infinite.

192
00:12:33,000 --> 00:12:36,000
 Possibilities are endless.

193
00:12:36,000 --> 00:12:40,090
 And so all of our complicated obsessions, desires, a

194
00:12:40,090 --> 00:12:43,000
versions, all of our egos,

195
00:12:43,000 --> 00:12:46,220
 it's all caught up in these concepts, these and other

196
00:12:46,220 --> 00:12:47,000
 concepts,

197
00:12:47,000 --> 00:12:51,510
 but most especially the past and the future, what we're

198
00:12:51,510 --> 00:12:54,000
 going to be, what we used to be.

199
00:12:54,000 --> 00:12:58,000
 It can also be caught up in concepts in the present.

200
00:12:58,000 --> 00:13:01,320
 But past and future themselves are some of the worst

201
00:13:01,320 --> 00:13:02,000
 concepts,

202
00:13:02,000 --> 00:13:09,140
 worrying about the future, reminiscing or bemoaning the

203
00:13:09,140 --> 00:13:12,000
 past.

204
00:13:12,000 --> 00:13:17,000
 And so it really is like that. You uproot yourself.

205
00:13:17,000 --> 00:13:24,000
 The present moment is like this rooted experience

206
00:13:24,000 --> 00:13:31,150
 where you're well connected with reality, just like a reed

207
00:13:31,150 --> 00:13:36,000
 or grass that is well rooted in the soil.

208
00:13:36,000 --> 00:13:39,000
 And so it grows and it thrives.

209
00:13:39,000 --> 00:13:42,000
 It's just the reality of the present moment.

210
00:13:42,000 --> 00:13:45,420
 A person who's in the past and the future is cut off from

211
00:13:45,420 --> 00:13:46,000
 that.

212
00:13:46,000 --> 00:13:50,000
 It's cut off from what's real.

213
00:13:50,000 --> 00:13:56,000
 And with no grounding point, right?

214
00:13:56,000 --> 00:14:01,180
 With no anchor, gets lost and spun around and around in

215
00:14:01,180 --> 00:14:02,000
 concepts.

216
00:14:02,000 --> 00:14:06,320
 All of our theories and philosophies, all of our views and

217
00:14:06,320 --> 00:14:07,000
 opinions,

218
00:14:07,000 --> 00:14:13,120
 all of our likes and dislikes, all of the things we

219
00:14:13,120 --> 00:14:14,000
 identify with

220
00:14:14,000 --> 00:14:19,000
 or seek to escape from.

221
00:14:19,000 --> 00:14:25,000
 All of this is caught up in concepts.

222
00:14:25,000 --> 00:14:30,000
 And so we waste away.

223
00:14:30,000 --> 00:14:33,710
 Anyone who lives in the future or lives in the past, they w

224
00:14:33,710 --> 00:14:35,000
ither up,

225
00:14:35,000 --> 00:14:39,000
 they dry up, their energy is used up.

226
00:14:39,000 --> 00:14:45,000
 They don't have this color, this radiance.

227
00:14:45,000 --> 00:14:49,000
 They aren't alive. They don't even feel alive.

228
00:14:49,000 --> 00:14:51,460
 A person who's never lived in the present moment might not

229
00:14:51,460 --> 00:14:52,000
 realize it,

230
00:14:52,000 --> 00:14:55,660
 but a person who has, a person who's practiced and who's

231
00:14:55,660 --> 00:14:56,000
 seen

232
00:14:56,000 --> 00:15:03,000
 and who's experienced what it's like to live now

233
00:15:03,000 --> 00:15:06,000
 and be able to tell you the clear difference.

234
00:15:06,000 --> 00:15:08,000
 I mean, this is the real reassurance in the practice.

235
00:15:08,000 --> 00:15:11,510
 Again, talking about how you know whether your practice is

236
00:15:11,510 --> 00:15:12,000
 good,

237
00:15:12,000 --> 00:15:15,170
 how do you know whether meditation is actually beneficial,

238
00:15:15,170 --> 00:15:17,000
 how does it feel?

239
00:15:17,000 --> 00:15:20,610
 If you haven't yet felt the difference between the present

240
00:15:20,610 --> 00:15:21,000
 moment

241
00:15:21,000 --> 00:15:25,000
 and dwelling in concepts, and you haven't really meditated,

242
00:15:25,000 --> 00:15:28,000
 then you can say you don't yet get it.

243
00:15:28,000 --> 00:15:33,000
 But at the moment when you're present, just that moment,

244
00:15:33,000 --> 00:15:36,000
 in that moment you can feel the difference.

245
00:15:36,000 --> 00:15:40,430
 Suddenly you're powerful, you're strong, you're in fact

246
00:15:40,430 --> 00:15:42,000
 invincible.

247
00:15:42,000 --> 00:15:45,000
 The present moment solves every problem.

248
00:15:45,000 --> 00:15:48,000
 Problems are conceptual.

249
00:15:48,000 --> 00:15:51,360
 When you're in the present moment, any problem is

250
00:15:51,360 --> 00:15:55,000
 immediately solved.

251
00:15:57,000 --> 00:16:02,280
 For that moment there is no problem because problems are

252
00:16:02,280 --> 00:16:04,000
 not real.

253
00:16:04,000 --> 00:16:08,350
 Now, of course this conflicts with all of our conceptual

254
00:16:08,350 --> 00:16:09,000
 desires

255
00:16:09,000 --> 00:16:13,920
 and ambitions and relationships and our status and our ego

256
00:16:13,920 --> 00:16:16,000
 and our personality.

257
00:16:16,000 --> 00:16:20,000
 So for most of us it's not living in the world,

258
00:16:20,000 --> 00:15:58,440
 it's not feasible for us to always be in the present moment

259
00:15:58,440 --> 00:16:24,000
.

260
00:16:24,000 --> 00:16:28,630
 But by no means is that the fault of reality, that's our

261
00:16:28,630 --> 00:16:29,000
 fault,

262
00:16:29,000 --> 00:16:33,400
 for getting caught up, caught up in ego and identity and

263
00:16:33,400 --> 00:16:36,000
 desires and aversions.

264
00:16:36,000 --> 00:16:40,340
 You get caught up in these things, relationships with

265
00:16:40,340 --> 00:16:44,000
 people, places, things,

266
00:16:44,000 --> 00:16:48,000
 attachments, aversions and so on.

267
00:16:48,000 --> 00:16:53,000
 All conceptual, nothing to do with reality.

268
00:16:53,000 --> 00:16:58,000
 [silence]

269
00:16:58,000 --> 00:17:01,070
 It kind of makes you want to go off in the forest and just

270
00:17:01,070 --> 00:17:04,000
 live, be real.

271
00:17:04,000 --> 00:17:08,000
 And people talk about, "Oh, yes, it's a nice vacation."

272
00:17:08,000 --> 00:17:16,500
 I remember meeting the king of Uganda or something in

273
00:17:16,500 --> 00:17:17,000
 Thailand.

274
00:17:17,000 --> 00:17:20,000
 He was a friend with the prince of Thailand

275
00:17:20,000 --> 00:17:23,000
 and I was at this very famous royal monastery.

276
00:17:23,000 --> 00:17:29,000
 All the royalty came through there and I happened to meet,

277
00:17:29,000 --> 00:17:33,000
 he came to, I got to tour him around this monastery.

278
00:17:33,000 --> 00:17:40,430
 He was a bit of a jerk, very Christian and very pro-

279
00:17:40,430 --> 00:17:42,000
Christianity.

280
00:17:42,000 --> 00:17:46,060
 So he was asking me about the difference about Christianity

281
00:17:46,060 --> 00:17:47,000
 and Buddhism.

282
00:17:47,000 --> 00:17:51,520
 He said, "Well, do you believe that Jesus is the son of God

283
00:17:51,520 --> 00:17:52,000
?"

284
00:17:52,000 --> 00:17:54,000
 I said, "No, we don't have a problem with that."

285
00:17:54,000 --> 00:17:56,000
 "What do you have a problem with?"

286
00:17:56,000 --> 00:18:00,000
 "Well, we have a problem maybe with the eternal soul,

287
00:18:00,000 --> 00:18:01,000
 the idea of an eternal soul."

288
00:18:01,000 --> 00:18:03,000
 He said, "You don't believe in an eternal soul?"

289
00:18:03,000 --> 00:18:06,000
 I said, "We don't really believe in a soul at all."

290
00:18:06,000 --> 00:18:12,510
 It was really a bizarre conversation with this king of

291
00:18:12,510 --> 00:18:15,000
 another country.

292
00:18:15,000 --> 00:18:17,000
 I don't remember what my point was.

293
00:18:17,000 --> 00:18:20,000
 What was I talking about?

294
00:18:20,000 --> 00:18:22,000
 What was I talking about?

295
00:18:22,000 --> 00:18:24,000
 Yeah, I know.

296
00:18:24,000 --> 00:18:32,000
 What was the point of that?

297
00:18:32,000 --> 00:18:34,000
 Oh yeah, living in the forest?

298
00:18:34,000 --> 00:18:35,000
 I don't remember.

299
00:18:35,000 --> 00:18:42,000
 I had a point of reason for telling you about this guy.

300
00:18:42,000 --> 00:18:46,000
 He got caught up in my story.

301
00:18:46,000 --> 00:18:53,000
 Makes you want to go off and live in the forest.

302
00:18:53,000 --> 00:19:05,000
 How did I get talking about the king of Rwanda, of Uganda?

303
00:19:05,000 --> 00:19:08,000
 It'll come to me someday.

304
00:19:08,000 --> 00:19:14,090
 Maybe I'll replay this video back and try and remember what

305
00:19:14,090 --> 00:19:16,000
 I was talking about.

306
00:19:16,000 --> 00:19:20,000
 Having to punish people? No, it wasn't that.

307
00:19:20,000 --> 00:19:23,640
 There's something about him that was related to what I was

308
00:19:23,640 --> 00:19:27,000
 just talking about.

309
00:19:27,000 --> 00:19:30,000
 Concepts.

310
00:19:30,000 --> 00:19:33,000
 Anyway, stay in the present moment.

311
00:19:33,000 --> 00:19:38,000
 Deal. How wonderful it is to be in the present moment.

312
00:19:38,000 --> 00:19:42,000
 How it solves all your problems.

313
00:19:42,000 --> 00:19:44,000
 How there's a real power to it.

314
00:19:44,000 --> 00:19:49,000
 I'll figure it out. I'll tell you a good story tomorrow.

315
00:19:49,000 --> 00:19:53,000
 Anyway, there you go. That's the Dhamma for tonight.

316
00:19:53,000 --> 00:19:55,000
 That the mind is also impermanent.

317
00:19:55,000 --> 00:19:57,000
 The memory is also impermanent.

318
00:19:57,000 --> 00:19:59,000
 It just disappears like that.

319
00:19:59,000 --> 00:20:03,000
 It's non-self. Okay, control it.

320
00:20:03,000 --> 00:20:05,000
 Okay, thank you all.

321
00:20:05,000 --> 00:20:19,000
 That's the Dhamma for tonight.

322
00:20:19,000 --> 00:20:23,000
 I do have something else to talk about.

323
00:20:23,000 --> 00:20:26,000
 I'm assuming I don't...

324
00:20:26,000 --> 00:20:29,230
 I know some of you. I think I know some of you I don't

325
00:20:29,230 --> 00:20:30,000
 recognize,

326
00:20:30,000 --> 00:20:34,000
 but maybe just because of your online names.

327
00:20:34,000 --> 00:20:39,000
 But here's an idea. Is Robin here? Robin's here, no?

328
00:20:39,000 --> 00:20:41,000
 I was thinking...

329
00:20:41,000 --> 00:20:45,900
 I was talking actually with May, one of the Thai women who

330
00:20:45,900 --> 00:20:49,000
 helps us out.

331
00:20:49,000 --> 00:20:52,000
 She was going to come here to meditate in early May,

332
00:20:52,000 --> 00:20:56,000
 and I said, "Oh, early May is my birthday."

333
00:20:56,000 --> 00:20:59,620
 And I was hoping to go back to my place of birth for my

334
00:20:59,620 --> 00:21:01,000
 birthday.

335
00:21:01,000 --> 00:21:05,000
 Meaning to go back to Manantulan Island,

336
00:21:05,000 --> 00:21:11,000
 just I guess to sort of say that I've been back.

337
00:21:11,000 --> 00:21:16,000
 But I thought, "Well, why not go to where I was born?"

338
00:21:16,000 --> 00:21:19,240
 And we got talking, but I said, "Well, what if we were to

339
00:21:19,240 --> 00:21:20,000
 do a meditation?"

340
00:21:20,000 --> 00:21:22,000
 She said, "Oh, you should bring some people up with you."

341
00:21:22,000 --> 00:21:25,640
 I said, "Well, what if we did a meditation course up there

342
00:21:25,640 --> 00:21:26,000
?"

343
00:21:26,000 --> 00:21:29,000
 And I was thinking,

344
00:21:29,000 --> 00:21:33,350
 there are a bunch of really nice resorts up on Manantulan

345
00:21:33,350 --> 00:21:34,000
 Island,

346
00:21:34,000 --> 00:21:38,000
 and we could actually just book a resort for a week

347
00:21:38,000 --> 00:21:43,000
 and invite people to come and stay at this island resort.

348
00:21:43,000 --> 00:21:47,150
 Now, I was looking, and it's actually not quite as cheap as

349
00:21:47,150 --> 00:21:48,000
 I thought it would be,

350
00:21:48,000 --> 00:21:52,000
 but we could talk about it.

351
00:21:52,000 --> 00:21:54,650
 And if there's people who would like to go, you know, Man

352
00:21:54,650 --> 00:21:56,000
antulan Island's beautiful.

353
00:21:56,000 --> 00:21:59,380
 It's really--in May, it's the best time of year because it

354
00:21:59,380 --> 00:22:00,000
's not too hot.

355
00:22:00,000 --> 00:22:06,690
 There's no mosquitoes, and yet it's very much in the forest

356
00:22:06,690 --> 00:22:10,000
, sort of in nature.

357
00:22:10,000 --> 00:22:14,160
 So, you know, if people wanted to book a room at some

358
00:22:14,160 --> 00:22:15,000
 resort,

359
00:22:15,000 --> 00:22:18,760
 we could find the right resort, and we could just get

360
00:22:18,760 --> 00:22:21,000
 people to pay their room.

361
00:22:21,000 --> 00:22:24,040
 If any--talking about people who have money, I don't know,

362
00:22:24,040 --> 00:22:27,000
 maybe it's a ridiculous idea, but--

363
00:22:27,000 --> 00:22:30,000
 And there are other ways we could book--

364
00:22:30,000 --> 00:22:35,000
 One of my friends on Manantulan is looking into it.

365
00:22:35,000 --> 00:22:40,960
 Maybe we could book some cheaper space to sort of hold some

366
00:22:40,960 --> 00:22:44,000
 kind of meditation course.

367
00:22:45,000 --> 00:22:49,280
 Anyway, something to think about if anyone's interested in

368
00:22:49,280 --> 00:22:51,000
 that sort of idea,

369
00:22:51,000 --> 00:22:54,000
 coming up to meditate in Manantulan Island,

370
00:22:54,000 --> 00:22:57,000
 or to do a real forest retreat in Canada.

371
00:22:57,000 --> 00:23:00,000
 Yeah, let me know.

372
00:23:00,000 --> 00:23:08,000
 Okay, so we do have some questions.

373
00:23:08,000 --> 00:23:13,520
 We'd have to take a van, probably. We'd have to drive

374
00:23:13,520 --> 00:23:15,000
 ourselves.

375
00:23:15,000 --> 00:23:17,000
 Okay, questions from the site.

376
00:23:17,000 --> 00:23:25,000
 Ooh, a question about offering meals and food.

377
00:23:25,000 --> 00:23:27,780
 Maybe on campus with a food card, how would you go about

378
00:23:27,780 --> 00:23:29,000
 adding money to that account?

379
00:23:29,000 --> 00:23:32,460
 Well, there's really no need because there's like almost $2

380
00:23:32,460 --> 00:23:36,000
,000 on that account still, so--

381
00:23:36,000 --> 00:23:39,000
 It's already been paid up for the next couple of years.

382
00:23:39,000 --> 00:23:43,410
 And I'm getting lots of food from the Sri Lankan community

383
00:23:43,410 --> 00:23:44,000
 here.

384
00:23:44,000 --> 00:23:49,320
 More and more Sri Lankan people in the area are signing up

385
00:23:49,320 --> 00:23:51,000
 to offer food.

386
00:23:51,000 --> 00:23:56,000
 But from time to time, I still do use some way--

387
00:23:56,000 --> 00:23:59,000
 Tim Horton's Peeta Pit Starbucks card.

388
00:23:59,000 --> 00:24:04,600
 The most useful so far has been Starbucks--not so far, but

389
00:24:04,600 --> 00:24:05,000
 this year--

390
00:24:05,000 --> 00:24:12,000
 because Starbucks is just the only convenient location.

391
00:24:12,000 --> 00:24:16,000
 Starbucks is like on campus. It's on my way to campus.

392
00:24:16,000 --> 00:24:20,000
 And so a Starbucks card is--and it's not for coffee.

393
00:24:20,000 --> 00:24:23,180
 I don't drink coffee at Starbucks, but they have oatmeal,

394
00:24:23,180 --> 00:24:26,000
 and they have lunch sandwiches,

395
00:24:26,000 --> 00:24:31,000
 and they actually have juice as well.

396
00:24:31,000 --> 00:24:33,790
 Nice juices in the evening. They even have salads, but the

397
00:24:33,790 --> 00:24:35,000
 problem with the salads is

398
00:24:35,000 --> 00:24:38,000
 they leave them out, and you have to pick them up yourself,

399
00:24:38,000 --> 00:24:40,000
 which technically isn't allowed.

400
00:24:40,000 --> 00:24:42,530
 I'm only allowed to do this because theoretically they're

401
00:24:42,530 --> 00:24:43,000
 offering--

402
00:24:43,000 --> 00:24:49,050
 they're giving it on behalf of whoever has left money with

403
00:24:49,050 --> 00:24:50,000
 them.

404
00:24:50,000 --> 00:24:55,000
 But they don't give everything to you. Anyway.

405
00:24:55,000 --> 00:24:59,000
 But thank you, Kathy, for asking, but it's really fine.

406
00:24:59,000 --> 00:25:07,000
 There's lots and lots of support. I'm getting enough food.

407
00:25:07,000 --> 00:25:10,110
 Can I move my legs if they fall asleep during meditation,

408
00:25:10,110 --> 00:25:14,000
 and should the head stay in one position?

409
00:25:14,000 --> 00:25:16,890
 You don't have to, because if they fall asleep, you just be

410
00:25:16,890 --> 00:25:19,000
 mindful of it and keep sitting.

411
00:25:19,000 --> 00:25:22,000
 But if you need to move, more for pain.

412
00:25:22,000 --> 00:25:30,380
 That would be something that would really cause you to want

413
00:25:30,380 --> 00:25:32,000
 to move.

414
00:25:32,000 --> 00:25:35,000
 Then you would just say to yourself, wanting to move,

415
00:25:35,000 --> 00:25:37,000
 wanting to move, moving, moving.

416
00:25:37,000 --> 00:25:40,000
 And the head is the same if you want to move the head.

417
00:25:40,000 --> 00:25:42,940
 If it does move, you can just raise it back up, intending

418
00:25:42,940 --> 00:25:44,000
 to raise.

419
00:25:44,000 --> 00:25:53,000
 And then you can see raising.

420
00:25:53,000 --> 00:25:56,320
 Does the approach of what can this meditation give me, will

421
00:25:56,320 --> 00:25:59,000
 it make me happy, prevent progress?

422
00:25:59,000 --> 00:26:02,000
 I mean, it can.

423
00:26:02,000 --> 00:26:04,170
 Yeah, it can certainly become a hindrance when you're

424
00:26:04,170 --> 00:26:12,000
 worried about that or when you're doubting or unsure.

425
00:26:12,000 --> 00:26:15,200
 How can one ensure equanimity, simply knowing the disliking

426
00:26:15,200 --> 00:26:16,000
 frustration?

427
00:26:16,000 --> 00:26:19,140
 Is it not necessary to try to intellectualize it by trying

428
00:26:19,140 --> 00:26:22,000
 to replace by wrong attitude the right attitude?

429
00:26:22,000 --> 00:26:25,700
 No, right attitude has to come from wisdom, comes from

430
00:26:25,700 --> 00:26:27,000
 understanding.

431
00:26:27,000 --> 00:26:31,470
 You can't artificially create or intellectually create

432
00:26:31,470 --> 00:26:33,000
 right attitude.

433
00:26:33,000 --> 00:26:37,000
 It has to come from seeing things clearly as they are.

434
00:26:37,000 --> 00:26:41,910
 Once you see reality clearly as it is, the right attitude

435
00:26:41,910 --> 00:26:43,000
 follows.

436
00:26:43,000 --> 00:26:48,000
 Excuse me.

437
00:26:48,000 --> 00:26:51,000
 My question is on free will and non-self.

438
00:26:51,000 --> 00:26:53,940
 With observation, I see whatever I do is because of things

439
00:26:53,940 --> 00:26:57,840
 arising in my mind, my likes and dislikes, and judge it

440
00:26:57,840 --> 00:27:00,000
 based on knowledge and dumbness.

441
00:27:00,000 --> 00:27:08,420
 Even when I incline my mind to focus in one direction or

442
00:27:08,420 --> 00:27:12,000
 the other, it's based on these things are happening.

443
00:27:12,000 --> 00:27:18,000
 The only thing that I can do is observe.

444
00:27:18,000 --> 00:27:22,000
 I don't get it.

445
00:27:22,000 --> 00:27:24,770
 I see whatever I do is because of things arising in my mind

446
00:27:24,770 --> 00:27:25,000
.

447
00:27:25,000 --> 00:27:28,150
 Even when I incline my mind to focus in one direction,

448
00:27:28,150 --> 00:27:30,000
 these things are happening.

449
00:27:30,000 --> 00:27:33,000
 The only thing I can do is observe.

450
00:27:33,000 --> 00:27:36,000
 There is no good view about free will.

451
00:27:36,000 --> 00:27:40,000
 If you're worried about free will, you should say worried,

452
00:27:40,000 --> 00:27:43,660
 worried, or speculating, or confused, or doubting, or so on

453
00:27:43,660 --> 00:27:44,000
.

454
00:27:44,000 --> 00:27:47,000
 Just let it go.

455
00:27:48,000 --> 00:27:51,000
 I think you're overthinking.

456
00:27:51,000 --> 00:27:54,000
 You know what I'm going to say already.

457
00:27:54,000 --> 00:27:57,000
 I mean, it's fine. It's normal in meditation to think.

458
00:27:57,000 --> 00:28:02,000
 Just remind yourself thinking, thinking.

459
00:28:02,000 --> 00:28:05,000
 Prison moment.

460
00:28:05,000 --> 00:28:08,000
 Do we have some questions?

461
00:28:08,000 --> 00:28:12,000
 Oh, here. You're copying them in.

462
00:28:12,000 --> 00:28:17,000
 If you want to support the monastery, that's always good.

463
00:28:17,000 --> 00:28:20,000
 You don't have to give me food.

464
00:28:20,000 --> 00:28:26,470
 Bhante, do you think King Bimbisara wouldn't have punished

465
00:28:26,470 --> 00:28:28,000
 criminals since he was a Sotapan?

466
00:28:28,000 --> 00:28:30,000
 I don't know.

467
00:28:30,000 --> 00:28:32,000
 I don't know.

468
00:28:32,000 --> 00:28:34,000
 I don't know.

469
00:28:34,000 --> 00:28:38,620
 Do you think King Bimbisara wouldn't have punished

470
00:28:38,620 --> 00:28:42,000
 criminals since he was a Sotapan?

471
00:28:42,000 --> 00:28:46,240
 Well, yeah. King Bimbisara would never have had anyone

472
00:28:46,240 --> 00:28:48,000
 killed, of course.

473
00:28:48,000 --> 00:28:50,000
 Doesn't mean he wouldn't.

474
00:28:50,000 --> 00:28:54,000
 No, he was in... there were cases...

475
00:28:54,000 --> 00:28:58,400
 Actually, I'm not sure, but no, certainly he was a great

476
00:28:58,400 --> 00:29:00,000
 king, a noble king.

477
00:29:00,000 --> 00:29:08,000
 He would never have punished, not in that way.

478
00:29:08,000 --> 00:29:13,000
 Fortunately, his reign was cut short by his son.

479
00:29:13,000 --> 00:29:15,000
 But yeah, during the time that he...

480
00:29:15,000 --> 00:29:19,300
 I mean, he wasn't always a Buddhist. He didn't originally

481
00:29:19,300 --> 00:29:20,000
...

482
00:29:20,000 --> 00:29:24,000
 He was a king for many years before he met the Buddha.

483
00:29:24,000 --> 00:29:26,280
 But once he met the Buddha and then became a Sotapan, from

484
00:29:26,280 --> 00:29:27,000
 that time on,

485
00:29:27,000 --> 00:29:32,000
 he would have never done anything cruel.

486
00:29:32,000 --> 00:29:39,000
 Not overly cruel. Sotapan can still be mean, but not...

487
00:29:39,000 --> 00:29:46,000
 not mean in the way most of us think.

488
00:29:46,000 --> 00:29:52,600
 But they could still get angry and be somehow cruel to

489
00:29:52,600 --> 00:29:54,000
 people.

490
00:29:54,000 --> 00:30:14,080
 They wouldn't torture them, certainly not cause them to be

491
00:30:14,080 --> 00:30:23,000
 put to death.

492
00:30:23,000 --> 00:30:26,000
 Hmm, I remember what it was.

493
00:30:26,000 --> 00:30:33,000
 So this king of Uganda, he at the last minute,

494
00:30:33,000 --> 00:30:36,390
 after I've told him this thing about the soul and he's not

495
00:30:36,390 --> 00:30:40,000
 really impressed by my whole philosophy,

496
00:30:40,000 --> 00:30:42,400
 he says to me and this other monk, "I've got a picture

497
00:30:42,400 --> 00:30:43,000
 right before..."

498
00:30:43,000 --> 00:30:46,000
 They took a picture of us, and I still got this picture

499
00:30:46,000 --> 00:30:47,000
 right before he said it.

500
00:30:47,000 --> 00:30:50,990
 Then he turns to me and he says, "Well, enjoy your vacation

501
00:30:50,990 --> 00:30:51,000
."

502
00:30:51,000 --> 00:30:56,100
 In the most snide way possible. It's just totally condesc

503
00:30:56,100 --> 00:30:57,000
ending.

504
00:30:57,000 --> 00:31:03,700
 And so the point was, people think of going off into the

505
00:31:03,700 --> 00:31:06,000
 forest as a sort of vacation,

506
00:31:06,000 --> 00:31:09,000
 a means of escaping your problems.

507
00:31:09,000 --> 00:31:13,040
 And the thought was that people say it's about escaping

508
00:31:13,040 --> 00:31:14,000
 real life.

509
00:31:14,000 --> 00:31:18,000
 It's a vacation. And real life...

510
00:31:18,000 --> 00:31:21,090
 People often ask me, "Well, how can I incorporate

511
00:31:21,090 --> 00:31:23,000
 meditation into real life?"

512
00:31:23,000 --> 00:31:26,000
 Which is kind of absurd from our point of view.

513
00:31:26,000 --> 00:31:27,000
 We're like, "No, no."

514
00:31:27,000 --> 00:31:31,000
 We go off into the forest in order to live real life.

515
00:31:31,000 --> 00:31:39,000
 The life in society is totally fake and contrived.

516
00:31:39,000 --> 00:31:45,000
 So there you go. That was how the talk was supposed to end.

517
00:31:45,000 --> 00:31:48,000
 Thank you all. Have a good night.

518
00:31:49,000 --> 00:32:04,000
 [

