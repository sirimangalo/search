1
00:00:00,000 --> 00:00:03,960
 Okay, one rather simple question about eating and

2
00:00:03,960 --> 00:00:06,240
 meditation. For example, I am doing something

3
00:00:06,240 --> 00:00:09,340
 which will take me half an hour or hour to finish, and

4
00:00:09,340 --> 00:00:11,160
 after that I am thinking to do

5
00:00:11,160 --> 00:00:15,320
 some meditation, but during this half hour I become hungry.

6
00:00:15,320 --> 00:00:16,980
 Should I eat and when I finish

7
00:00:16,980 --> 00:00:20,800
 doing what I'm doing meditate or not eat and keep working,

8
00:00:20,800 --> 00:00:22,840
 then do the meditation on hungry

9
00:00:22,840 --> 00:00:28,250
 stomach or maybe just have a snack. It's not exactly a

10
00:00:28,250 --> 00:00:32,800
 simple question. There's a lot in

11
00:00:32,800 --> 00:00:36,770
 there that's a bit, I'm not quite clear on the whole of the

12
00:00:36,770 --> 00:00:45,280
 question. But one thing I would say

13
00:00:45,280 --> 00:00:48,620
 which is kind of smarmy is that your work should be

14
00:00:48,620 --> 00:00:51,720
 meditation and your eating should be meditation.

15
00:00:51,720 --> 00:00:54,860
 So that's really where your answer, that's the most

16
00:00:54,860 --> 00:00:56,840
 important part of the answer. It's not really

17
00:00:56,840 --> 00:01:01,390
 smarmy at all. It's probably not the answer that people,

18
00:01:01,390 --> 00:01:07,920
 most people would expect, but the

19
00:01:07,920 --> 00:01:11,580
 meditation starts now. It doesn't start 30 minutes from now

20
00:01:11,580 --> 00:01:13,320
 when you're going to meditate,

21
00:01:13,320 --> 00:01:17,490
 because then you're wasting 30 minutes thinking it's okay.

22
00:01:17,490 --> 00:01:19,560
 In 30 minutes I'll meditate, or in

23
00:01:19,560 --> 00:01:23,110
 an hour I'll meditate, or after this I'll meditate. And

24
00:01:23,110 --> 00:01:25,840
 during all that 30 minutes or an hour you're

25
00:01:25,840 --> 00:01:30,370
 wasting good meditation time. So when you work you should

26
00:01:30,370 --> 00:01:32,840
 try to be mindful. When you eat you

27
00:01:32,840 --> 00:01:36,940
 should try to be mindful. You should work on that. That'll

28
00:01:36,940 --> 00:01:39,600
 really, why it's not such a smarmy answer

29
00:01:39,600 --> 00:01:43,770
 after all is that that'll really help your formal

30
00:01:43,770 --> 00:01:46,840
 meditation. If a person just does formal

31
00:01:46,840 --> 00:01:49,940
 meditation and doesn't think at all about meditation during

32
00:01:49,940 --> 00:01:54,000
 their daily life, the formal

33
00:01:54,000 --> 00:01:56,750
 meditation becomes quite difficult and it's a constant

34
00:01:56,750 --> 00:01:58,320
 uphill struggle because it's out of

35
00:01:58,320 --> 00:02:02,590
 tune with your ordinary reality. So you should try to

36
00:02:02,590 --> 00:02:05,920
 constantly be bringing your mind back to

37
00:02:05,920 --> 00:02:09,830
 attention. I mean even right now here we are sitting

38
00:02:09,830 --> 00:02:12,280
 together we should try to make it a

39
00:02:12,280 --> 00:02:16,320
 mindful experience. Being aware at least of our emotions,

40
00:02:16,320 --> 00:02:19,040
 likes and dislikes, the distractions

41
00:02:19,040 --> 00:02:25,500
 in the mind, the feelings that we have. If you're just

42
00:02:25,500 --> 00:02:28,400
 listening here, because I'm talking I have to

43
00:02:28,400 --> 00:02:32,110
 do more action, but for some of you just listening you can

44
00:02:32,110 --> 00:02:34,440
 just revert back to your meditation,

45
00:02:34,440 --> 00:02:41,520
 unless you have questions. But as far as me making

46
00:02:41,520 --> 00:02:45,120
 decisions as to when you should do eating and when

47
00:02:45,120 --> 00:02:49,140
 you should do meditating, meditating on an empty stomach is

48
00:02:49,140 --> 00:02:52,640
 interesting, can be useful to help you

49
00:02:52,640 --> 00:02:59,990
 to learn about hunger. But I'd say generally you need the

50
00:02:59,990 --> 00:03:03,920
 food for the energy for the meditation.

51
00:03:03,920 --> 00:03:07,580
 So better to have a little bit of food before you meditate.

52
00:03:07,580 --> 00:03:10,760
 I don't know meditate anytime,

53
00:03:10,760 --> 00:03:14,410
 whenever you meditate, meditate, get some time for it and

54
00:03:14,410 --> 00:03:17,160
 do it. Just don't procrastinate. It

55
00:03:17,160 --> 00:03:20,080
 kind of sounds like you might be talking about procrast

56
00:03:20,080 --> 00:03:24,240
inating, which is dangerous. So you know

57
00:03:24,240 --> 00:03:28,570
 it goes work for a half an hour and then I'll do, then I'll

58
00:03:28,570 --> 00:03:30,360
 have a snack and then when I'm having

59
00:03:30,360 --> 00:03:33,800
 a snack I think about, "Oh then I have to go check Facebook

60
00:03:33,800 --> 00:03:35,640
 or email and check email and then

61
00:03:35,640 --> 00:03:38,010
 something comes up in email and I have to call this person

62
00:03:38,010 --> 00:03:39,520
 or that person or do this or I have

63
00:03:39,520 --> 00:03:44,220
 to go to the store," and then you never meditate after all.

64
00:03:44,220 --> 00:03:48,680
 That kind of thing is dangerous. So

65
00:03:48,680 --> 00:03:52,830
 better to be clear with yourself that if you really need

66
00:03:52,830 --> 00:03:56,440
 food and you're really hungry, then eat or

67
00:03:56,440 --> 00:04:03,450
 eat a little bit. But if you don't, sometimes it's just

68
00:04:03,450 --> 00:04:08,440
 procrastinating because meditation can be

69
00:04:08,440 --> 00:04:11,240
 difficult and the mind will actually develop aversion

70
00:04:11,240 --> 00:04:16,520
 towards it if you're not careful. If

71
00:04:16,520 --> 00:04:19,110
 you're not aware of the aversion, if you're not looking at

72
00:04:19,110 --> 00:04:20,560
 it and contemplating it as well,

73
00:04:20,560 --> 00:04:24,420
 it can become a great hindrance. And more than a hindrance,

74
00:04:24,420 --> 00:04:27,680
 it can actually trick you into every

75
00:04:27,680 --> 00:04:29,730
 time you think of meditation, your mind says, "Oh, he's

76
00:04:29,730 --> 00:04:32,320
 thinking of meditation again. Let's divert

77
00:04:32,320 --> 00:04:35,940
 his attention. Hey, look at that. That's nice. Hey, aren't

78
00:04:35,940 --> 00:04:40,120
 you hungry? Hey, you want to go for burgers

79
00:04:40,120 --> 00:04:43,510
 or something?" The mind will do this to you because it

80
00:04:43,510 --> 00:04:45,920
 doesn't want to meditate. It's like, "Oh no,

81
00:04:45,920 --> 00:04:52,740
 don't put me back there. That's torture." So be careful

82
00:04:52,740 --> 00:04:54,720
 with that one.

