1
00:00:00,000 --> 00:00:07,000
 Good evening.

2
00:00:07,000 --> 00:00:32,000
 Good evening.

3
00:00:32,000 --> 00:00:45,000
 This is Dheganik.

4
00:00:45,000 --> 00:01:03,000
 This is Dheganik's speech.

5
00:01:03,000 --> 00:01:07,140
 It just takes part of the section on morality from the Bra

6
00:01:07,140 --> 00:01:09,000
hmajalasuddha.

7
00:01:09,000 --> 00:01:17,250
 Brahmajalasuddha is in certain versions of the Dvitika, it

8
00:01:17,250 --> 00:01:20,000
's the first text.

9
00:01:20,000 --> 00:01:22,600
 So of all the teachings of the Buddha, it's what comes

10
00:01:22,600 --> 00:01:23,000
 first.

11
00:01:23,000 --> 00:01:28,000
 It's really a landmark discourse.

12
00:01:28,000 --> 00:01:31,000
 There's so much in it.

13
00:01:31,000 --> 00:01:36,000
 It covers so many topics.

14
00:01:36,000 --> 00:01:43,000
 It's not one single teaching.

15
00:01:43,000 --> 00:01:50,950
 In Burma they have a version of the Dvitika that picks it

16
00:01:50,950 --> 00:01:53,000
 apart word by word

17
00:01:53,000 --> 00:01:57,000
 and tries to explain almost every word of the Dvitika.

18
00:01:57,000 --> 00:02:03,000
 Well, maybe not every word, but it does start that way.

19
00:02:03,000 --> 00:02:09,710
 So the suta starts, "Ei wang mei sutang, thus has been

20
00:02:09,710 --> 00:02:11,000
 heard by me."

21
00:02:11,000 --> 00:02:15,000
 "Ei wang," and then it explains what "Ei wang" means.

22
00:02:15,000 --> 00:02:18,000
 And then it means, "What is 'mei' mean?"

23
00:02:18,000 --> 00:02:21,000
 And "What is 'sutang' mean?"

24
00:02:21,000 --> 00:02:24,000
 And so in Thailand they started doing this.

25
00:02:24,000 --> 00:02:29,000
 They started translating from the Burmese actually.

26
00:02:29,000 --> 00:02:34,000
 And of course they started with the Brahmancala suta.

27
00:02:34,000 --> 00:02:46,000
 So all in all it's an important suta.

28
00:02:46,000 --> 00:02:50,370
 And not to go into the suta really, but to go into the

29
00:02:50,370 --> 00:02:53,000
 quote and section regarding the quote.

30
00:02:53,000 --> 00:02:57,740
 It's a section of morality, but why he picked this quote is

31
00:02:57,740 --> 00:02:59,000
 pretty clear.

32
00:02:59,000 --> 00:03:02,720
 Because mostly when we talk about morality we deal with the

33
00:03:02,720 --> 00:03:04,000
 negative aspects.

34
00:03:04,000 --> 00:03:08,000
 Don't kill because killing is bad.

35
00:03:08,000 --> 00:03:10,000
 Killing is bad.

36
00:03:10,000 --> 00:03:14,000
 Don't steal, stealing is bad.

37
00:03:14,000 --> 00:03:17,000
 Don't cheat, cheating is evil.

38
00:03:17,000 --> 00:03:22,000
 Lying, lying is evil.

39
00:03:22,000 --> 00:03:28,000
 Malicious speech is evil.

40
00:03:28,000 --> 00:03:32,000
 Harsh speech.

41
00:03:32,000 --> 00:03:36,000
 What are the words they use?

42
00:03:36,000 --> 00:03:45,000
 Malicious, malicious, harsh, and useless, frivolous.

43
00:03:45,000 --> 00:03:47,000
 Because these are bad.

44
00:03:47,000 --> 00:03:52,000
 But here the Buddha does something different.

45
00:03:52,000 --> 00:03:57,000
 He says, "Gives up life."

46
00:03:57,000 --> 00:03:59,000
 And giving up life.

47
00:03:59,000 --> 00:04:05,000
 So you can find the Pali here.

48
00:04:05,000 --> 00:04:08,000
 "Bhana, di bah, dang bahaya."

49
00:04:08,000 --> 00:04:10,000
 I once actually tried to memorize.

50
00:04:10,000 --> 00:04:12,000
 I had this memorized at one point.

51
00:04:13,000 --> 00:04:18,000
 This was the first bit of the Brahma Jhava Suta.

52
00:04:18,000 --> 00:04:21,000
 "Bhana, di bah, dang bahaya.

53
00:04:21,000 --> 00:04:23,000
 Bhana, di bah, dah, bah, tivirato.

54
00:04:23,000 --> 00:04:27,000
 Samano, go to Mo."

55
00:04:27,000 --> 00:04:31,000
 "Nihita dandho, nihita sato.

56
00:04:31,000 --> 00:04:35,000
 Lanji, daya pano.

57
00:04:35,000 --> 00:04:42,000
 Sambha panambut, tahita nukampi viharati."

58
00:04:42,000 --> 00:04:44,000
 So it talks about the positive aspects.

59
00:04:44,000 --> 00:04:48,000
 Once you've given up killing,

60
00:04:48,000 --> 00:04:51,000
 when you've abandoned killing,

61
00:04:51,000 --> 00:04:58,000
 you refrain from taking the life of others.

62
00:04:58,000 --> 00:05:03,000
 "Nihita sato," having put down weapons,

63
00:05:03,000 --> 00:05:17,000
 "Lanji scrupulous, daya pano, daya pano."

64
00:05:17,000 --> 00:05:20,000
 "Daya pano, compassionate.

65
00:05:20,000 --> 00:05:28,000
 Sambha bhut, dahita nukampi viharati."

66
00:05:28,000 --> 00:05:31,000
 "Anu kampi" means trembling.

67
00:05:31,000 --> 00:05:32,000
 That's how he translates it.

68
00:05:32,000 --> 00:05:35,000
 It literally means to tremble.

69
00:05:35,000 --> 00:05:39,000
 "Anu kampi" means to be shaken by,

70
00:05:39,000 --> 00:05:48,000
 be disturbed by, be moved by, really, as I was saying.

71
00:05:48,000 --> 00:05:54,780
 "Moved by out of desire for the benefit "hita" of all

72
00:05:54,780 --> 00:05:58,000
 beings.

73
00:05:58,000 --> 00:06:06,000
 Moved by desire, moved out of, in regards to,

74
00:06:06,000 --> 00:06:12,000
 in the interest of the benefit of all beings

75
00:06:12,000 --> 00:06:17,000
 and all living beings.

76
00:06:17,000 --> 00:06:21,990
 Trembling for the welfare of all living beings, he turns

77
00:06:21,990 --> 00:06:24,000
 into.

78
00:06:24,000 --> 00:06:29,050
 So this is a positive thing, a positive state, out of

79
00:06:29,050 --> 00:06:30,000
 compassion.

80
00:06:30,000 --> 00:06:36,000
 One, we don't stop killing because we're afraid of,

81
00:06:36,000 --> 00:06:39,970
 we don't stop killing simply because we're afraid of the

82
00:06:39,970 --> 00:06:43,000
 evil consequences of killing,

83
00:06:43,000 --> 00:06:47,000
 because we're afraid people will think we're evil,

84
00:06:47,000 --> 00:06:55,000
 because we're afraid of going against the Buddha's words.

85
00:06:55,000 --> 00:06:59,820
 When you practice mindfulness, when you start to see

86
00:06:59,820 --> 00:07:01,000
 clearly,

87
00:07:01,000 --> 00:07:06,000
 you see yourself in others and in others and in yourself,

88
00:07:06,000 --> 00:07:10,000
 as we had learned with the Dhammapada recently,

89
00:07:10,000 --> 00:07:14,000
 all tremble at the rod.

90
00:07:14,000 --> 00:07:19,840
 Comparing oneself to others, one should have killed or harm

91
00:07:19,840 --> 00:07:22,000
 another.

92
00:07:22,000 --> 00:07:27,000
 So one becomes naturally compassionate

93
00:07:27,000 --> 00:07:35,970
 and at the thought of hurting others, one shies away

94
00:07:35,970 --> 00:07:40,000
 naturally.

95
00:07:40,000 --> 00:07:51,000
 Adinada nam paha ya adinada na pativirato samano go tomo

96
00:07:51,000 --> 00:07:56,000
 Not taking what is not given, having abandoned what is,

97
00:07:56,000 --> 00:08:01,000
 having abandoned the taking of what is not given.

98
00:08:01,000 --> 00:08:11,000
 Dhinada ye taking only what is given, dinapatikankin

99
00:08:11,000 --> 00:08:19,000
 awaiting what is given, patikankin

100
00:08:19,000 --> 00:08:35,000
 atte nae nasuchibhoote na atana viharati

101
00:08:35,000 --> 00:08:43,000
 It's quite a flowing sutra, sutra to chant.

102
00:08:43,000 --> 00:08:49,000
 Without stealing, abandoning, abandoning unchastity.

103
00:08:49,000 --> 00:08:53,520
 That's interesting. I don't think that's quite what it

104
00:08:53,520 --> 00:08:54,000
 means.

105
00:08:54,000 --> 00:09:03,000
 Sujibhoote na atae nae na atae nae na without stealing

106
00:09:03,000 --> 00:09:08,000
 Sujibhoote na, I have to look into that.

107
00:09:08,000 --> 00:09:13,700
 I used to know about that. I don't think it has to do its

108
00:09:13,700 --> 00:09:20,000
 chastity, but there's something there.

109
00:09:20,000 --> 00:09:23,000
 Oh wait, I'm looking at the wrong one.

110
00:09:23,000 --> 00:09:27,000
 Living purely, awaiting what is given without stealing.

111
00:09:27,000 --> 00:09:36,000
 Atte nae nasuchibhoote na, dwells, I see, dwells pure.

112
00:09:36,000 --> 00:09:45,000
 Lives purely accepting what is given, lives purely.

113
00:09:45,000 --> 00:09:55,020
 So it's not just about not stealing, it's about living with

114
00:09:55,020 --> 00:09:58,000
 what is given.

115
00:09:58,000 --> 00:10:03,820
 It's about being content with what is given, being content

116
00:10:03,820 --> 00:10:06,000
 with what comes to you.

117
00:10:06,000 --> 00:10:11,610
 And this includes work, this includes how anything comes to

118
00:10:11,610 --> 00:10:12,000
 you.

119
00:10:12,000 --> 00:10:20,000
 It's about contentment rather than means or manner.

120
00:10:20,000 --> 00:10:22,000
 It doesn't mean you have to wait for handouts.

121
00:10:22,000 --> 00:10:27,040
 I mean, if you work in a job and people pay you money,

122
00:10:27,040 --> 00:10:33,000
 being content with that, being able to make do.

123
00:10:33,000 --> 00:10:37,000
 It's interesting, we're so afraid, we're so worried about

124
00:10:37,000 --> 00:10:39,000
 how am I going to make ends meet.

125
00:10:39,000 --> 00:10:43,670
 And often we have legitimate concern that sometimes it is

126
00:10:43,670 --> 00:10:46,000
 difficult to make ends meet,

127
00:10:46,000 --> 00:10:54,290
 often our fear prevents us from coming to trust, trust in

128
00:10:54,290 --> 00:10:58,000
 the power of goodness,

129
00:10:58,000 --> 00:11:02,000
 trust in the power of our own goodness.

130
00:11:02,000 --> 00:11:09,000
 That we won't fall.

131
00:11:09,000 --> 00:11:15,940
 If anything, becoming a monk, if it's shown me anything,

132
00:11:15,940 --> 00:11:19,000
 well, one of the greatest things it's shown me is that,

133
00:11:19,000 --> 00:11:23,210
 that when you do good things, people support you,

134
00:11:23,210 --> 00:11:28,000
 supportive people surround you.

135
00:11:28,000 --> 00:11:32,000
 Being sick the past week has certainly shown me that.

136
00:11:32,000 --> 00:11:35,680
 What would I have done if I hadn't people who were

137
00:11:35,680 --> 00:11:37,000
 supporting you?

138
00:11:37,000 --> 00:11:43,000
 I remember one time in Thailand I got very sick.

139
00:11:43,000 --> 00:11:47,280
 When I was staying in Wat Thambotang, which is, if any of

140
00:11:47,280 --> 00:11:58,000
 you remember that, it was many years ago now.

141
00:11:58,000 --> 00:12:04,220
 And there were meditators there, it was back when Palanyani

142
00:12:04,220 --> 00:12:08,000
 was there before she was a bhikkhuni.

143
00:12:08,000 --> 00:12:13,810
 And so she and I think there were her and another meditator

144
00:12:13,810 --> 00:12:15,000
 were there.

145
00:12:15,000 --> 00:12:20,340
 And I had been going three and a half kilometers on Aam's

146
00:12:20,340 --> 00:12:25,000
 round, and getting enough food.

147
00:12:25,000 --> 00:12:27,600
 An old monk there told us, "You go on Aam's round, you won

148
00:12:27,600 --> 00:12:29,000
't get enough food to eat."

149
00:12:29,000 --> 00:12:32,640
 But when I went on Aam's round, walked three and a half

150
00:12:32,640 --> 00:12:38,000
 kilometers there, and then three and a half kilometers back

151
00:12:38,000 --> 00:12:38,000
,

152
00:12:38,000 --> 00:12:42,650
 I got enough food for me and the meditators. So we just fed

153
00:12:42,650 --> 00:12:47,000
 the meditators with Aam's food.

154
00:12:47,000 --> 00:12:51,790
 And so the morning I woke up and I felt dizzy and probably

155
00:12:51,790 --> 00:12:53,000
 feverish.

156
00:12:53,000 --> 00:12:55,260
 But I knew if I didn't go on Aam's round, the meditators

157
00:12:55,260 --> 00:12:56,000
 wouldn't eat.

158
00:12:56,000 --> 00:13:00,540
 So I actually walked three and a half kilometers, and I

159
00:13:00,540 --> 00:13:02,000
 could barely help.

160
00:13:02,000 --> 00:13:05,570
 Three and a half kilometers. You think about how long that

161
00:13:05,570 --> 00:13:09,000
 is. With a fever.

162
00:13:09,000 --> 00:13:14,000
 Remember that is one of the longest walks of my life.

163
00:13:14,000 --> 00:13:19,900
 And when I got to the village I had to sit down and ask one

164
00:13:19,900 --> 00:13:23,000
 of the stall people for water.

165
00:13:23,000 --> 00:13:29,080
 I can't quite remember what happened, but somehow I think I

166
00:13:29,080 --> 00:13:31,990
 got someone in the village to bring us food or something

167
00:13:31,990 --> 00:13:33,000
 like that.

168
00:13:41,000 --> 00:13:49,000
 Sometimes you have to go without. Sometimes.

169
00:13:49,000 --> 00:14:00,000
 But if you do good, we trust in the goodness.

170
00:14:00,000 --> 00:14:04,000
 We trust in the power of karma.

171
00:14:04,000 --> 00:14:10,250
 That it is real. That the universe is only mind-made. It

172
00:14:10,250 --> 00:14:12,000
 only comes from the mind.

173
00:14:12,000 --> 00:14:16,750
 So bad things are happening to us now. We just let them

174
00:14:16,750 --> 00:14:18,000
 happen.

175
00:14:18,000 --> 00:14:21,940
 We don't care about the bad things. Work towards the good

176
00:14:21,940 --> 00:14:25,000
 things. Do good things.

177
00:14:25,000 --> 00:14:29,000
 Cultivate good things. And good will come.

178
00:14:29,000 --> 00:14:36,000
 This is what we...where do we stand?

179
00:14:36,000 --> 00:14:40,000
 And so these...what I thought when I woke up that morning

180
00:14:40,000 --> 00:14:44,000
 was, these people are doing this good thing.

181
00:14:44,000 --> 00:14:47,640
 It was enough of a motivation that these people were doing

182
00:14:47,640 --> 00:14:49,000
 a meditation course.

183
00:14:49,000 --> 00:14:52,650
 For me to walk three and a half kilometers, that's how good

184
00:14:52,650 --> 00:14:56,000
 was their deed of doing their meditation courses.

185
00:14:56,000 --> 00:15:02,970
 That's how powerful meditation is. It compels sick monks to

186
00:15:02,970 --> 00:15:09,000
 walk three and a half kilometers.

187
00:15:09,000 --> 00:15:15,480
 That's adhina. That's the second one. The third one, a brah

188
00:15:15,480 --> 00:15:18,000
macarya.

189
00:15:18,000 --> 00:15:26,000
 Arachari, virato, virato mitunagama dhamma.

190
00:15:26,000 --> 00:15:33,000
 Arachari. Remember, arachari is a special word.

191
00:15:33,000 --> 00:15:42,000
 And lives far from it.

192
00:15:42,000 --> 00:15:48,110
 Arachari, living far, living high, living lofty, living the

193
00:15:48,110 --> 00:15:50,000
 lofty life, maybe.

194
00:15:50,000 --> 00:15:58,000
 Virato mitunagama dhamma. Gama dhamma is the village dhamma

195
00:15:58,000 --> 00:15:58,000
.

196
00:15:58,000 --> 00:16:04,000
 Mituna, regards to sex.

197
00:16:04,000 --> 00:16:11,000
 Village dhamma of sex. Abandoning it, abstaining from it.

198
00:16:11,000 --> 00:16:16,000
 Virato.

199
00:16:16,000 --> 00:16:20,000
 Musawa dhambahaya. And here we get into the actual quote.

200
00:16:20,000 --> 00:16:23,350
 It talks about the types of speech, which is the interest

201
00:16:23,350 --> 00:16:24,000
...

202
00:16:24,000 --> 00:16:27,420
 Well, it's interesting. And why he quoted it, because it

203
00:16:27,420 --> 00:16:29,000
 talks about positive aspects.

204
00:16:29,000 --> 00:16:37,750
 When you don't lie, you're a satyawadi. Satchasandho teito

205
00:16:37,750 --> 00:16:44,000
 bachayiko avisangwadakolokasa.

206
00:16:44,000 --> 00:16:49,270
 Dwells, refraining from false speech, a truth-speaker, one

207
00:16:49,270 --> 00:16:54,570
 to be relied on, trustworthy, dependable, not deceiver of

208
00:16:54,570 --> 00:16:57,000
 the world.

209
00:16:57,000 --> 00:17:02,000
 Someone who doesn't lie, that's a very powerful thing.

210
00:17:02,000 --> 00:17:06,460
 It gets to the point where it's shocking to hear someone

211
00:17:06,460 --> 00:17:09,000
 lie, to find out that someone lied to you.

212
00:17:09,000 --> 00:17:14,320
 If you surround yourself with good people enough, it's

213
00:17:14,320 --> 00:17:18,740
 quite shocking when someone does actually lie to you,

214
00:17:18,740 --> 00:17:22,000
 because...

215
00:17:22,000 --> 00:17:25,660
 There's such a difference with people who tell the truth,

216
00:17:25,660 --> 00:17:29,000
 and with telling the truth, and how wonderful it is.

217
00:17:29,000 --> 00:17:40,120
 It's so much more peaceful, and much more powerful, and

218
00:17:40,120 --> 00:17:47,000
 harmonious, and constructive.

219
00:17:47,000 --> 00:17:55,150
 Vaisangwadakolokasa, not one who... a person who doesn't

220
00:17:55,150 --> 00:17:58,000
 deceive the world.

221
00:18:15,000 --> 00:18:18,000
 Having heard something over there, one doesn't...

222
00:18:18,000 --> 00:18:22,250
 Having heard something here, one doesn't spread it over

223
00:18:22,250 --> 00:18:28,000
 there, in order to break these ones up.

224
00:18:28,000 --> 00:18:32,020
 Having heard something over there, one doesn't spread it

225
00:18:32,020 --> 00:18:38,000
 over here, in order to break them up with those people.

226
00:18:38,000 --> 00:18:41,700
 So that's what you don't do. But what does that do when you

227
00:18:41,700 --> 00:18:43,000
 stop that?

228
00:18:43,000 --> 00:18:46,000
 So a person doesn't make you...

229
00:18:46,000 --> 00:18:59,000
 Those who are broken up, one brings them together.

230
00:18:59,000 --> 00:19:06,000
 Those who are together, one doesn't break them up.

231
00:19:11,000 --> 00:19:22,000
 One who delights in harmony, one who rejoices in harmony.

232
00:19:22,000 --> 00:19:26,000
 These are all really ways of saying the same thing.

233
00:19:26,000 --> 00:19:31,000
 Rejoices, rejoices, rejoices in harmony.

234
00:19:31,000 --> 00:19:36,000
 Who speaks words that create harmony.

235
00:19:36,000 --> 00:19:43,000
 Samagakarani wajang bhasita.

236
00:19:43,000 --> 00:19:51,480
 Parousang wajang bahaya parousaya wajaya patibirato samanog

237
00:19:51,480 --> 00:19:53,000
oto.

238
00:19:53,000 --> 00:19:58,000
 One reference from her speech.

239
00:19:58,000 --> 00:20:06,000
 Yasa waja nila karnasukha pemaniya, pemaniya,

240
00:20:06,000 --> 00:20:13,000
 andayangamapuri bahujanakanta bahujanamunnapa,

241
00:20:13,000 --> 00:20:19,000
 tadharuping wajang bhasita.

242
00:20:19,000 --> 00:20:29,000
 Yasa waja, whatever waja, whatever speech, nila is nila.

243
00:20:29,000 --> 00:20:33,000
 I used to know all this.

244
00:20:33,000 --> 00:20:36,000
 Blameless? No.

245
00:20:36,000 --> 00:20:39,000
 Yeah, probably. Nila, blameless.

246
00:20:39,000 --> 00:20:46,280
 Kannasukha, kannasukha, kannas the ears, suka is happiness

247
00:20:46,280 --> 00:20:48,000
 or happy.

248
00:20:48,000 --> 00:20:54,000
 Whatever speech makes the ear happy.

249
00:20:54,000 --> 00:21:01,000
 Bhemaniya causes people to find you dear, endearing.

250
00:21:01,000 --> 00:21:04,000
 Hadayangama, hadaya is the heart.

251
00:21:04,000 --> 00:21:09,000
 Gamma is goes or gone.

252
00:21:09,000 --> 00:21:16,000
 Speech that goes to the heart or warms the heart.

253
00:21:16,000 --> 00:21:21,650
 Bhoori is probably the same thing, or bhain, I don't know

254
00:21:21,650 --> 00:21:23,000
 what that means even.

255
00:21:23,000 --> 00:21:28,000
 Bahujanakanta, which is pleasing to many people.

256
00:21:28,000 --> 00:21:34,000
 Bahujanamanapa, which warms the hearts of many ears.

257
00:21:34,000 --> 00:21:39,000
 Makes the mind happy.

258
00:21:39,000 --> 00:21:44,000
 Tadharupingwa, jang bhasita, one speaks such words.

259
00:21:44,000 --> 00:21:47,000
 You don't say things to hurt others.

260
00:21:47,000 --> 00:21:50,920
 You don't say things to cause pain and suffering or to make

261
00:21:50,920 --> 00:21:56,000
 people upset or to make them angry.

262
00:21:56,000 --> 00:21:59,200
 You think and you're careful with what you say and you say

263
00:21:59,200 --> 00:22:07,000
 things that bring peace and harmony.

264
00:22:07,000 --> 00:22:16,000
 And finally, sampapalapa is useless speech.

265
00:22:16,000 --> 00:22:19,000
 Even the word sounds like useless speech.

266
00:22:19,000 --> 00:22:24,000
 Sampapalapa, that's like blah, blah, blah.

267
00:22:24,000 --> 00:22:27,000
 That's really what it means.

268
00:22:27,000 --> 00:22:36,000
 Sampapalapa, sampapalapa.

269
00:22:36,000 --> 00:22:40,000
 Kala Vadhi speaks at the right time.

270
00:22:40,000 --> 00:22:43,000
 Bhutavadhi speaks what is true.

271
00:22:43,000 --> 00:22:47,000
 Athavadhi speaks what is meaningful.

272
00:22:47,000 --> 00:22:50,000
 Dhammavadhi speaks what is right.

273
00:22:50,000 --> 00:22:53,000
 Vinayavadhi speaks what is moral.

274
00:22:53,000 --> 00:23:03,000
 Nidhana Vadhi, nidhana Vadhi, nidhana Vadhi, nidhana Vadhi.

275
00:23:03,000 --> 00:23:10,000
 Nidhana, he speaks words that are to be treasured.

276
00:23:10,000 --> 00:23:14,000
 Kala ina sap bandhi sam.

277
00:23:14,000 --> 00:23:20,110
 At the right time, in the right place, baryantavating, in

278
00:23:20,110 --> 00:23:22,000
 the right measure.

279
00:23:22,000 --> 00:23:38,000
 At the samhitan connected with what is useful.

280
00:23:38,000 --> 00:23:43,000
 This is in regards to the first four precepts.

281
00:23:43,000 --> 00:23:47,770
 He's actually talking about the reasons why people praise

282
00:23:47,770 --> 00:23:49,000
 the Buddha.

283
00:23:49,000 --> 00:23:52,990
 The context of the quote is somewhat interesting, not found

284
00:23:52,990 --> 00:23:55,000
 in the actual quote.

285
00:23:55,000 --> 00:24:01,390
 He's actually speaking in sort of, well, he's belittling

286
00:24:01,390 --> 00:24:02,000
 these.

287
00:24:02,000 --> 00:24:05,000
 These are called minor morality.

288
00:24:05,000 --> 00:24:08,860
 He said this is what ordinary people would praise the

289
00:24:08,860 --> 00:24:10,000
 Buddha for.

290
00:24:10,000 --> 00:24:13,000
 So in a sense he's bragging or boasting.

291
00:24:13,000 --> 00:24:18,000
 He's saying that there's all these great things,

292
00:24:18,000 --> 00:24:23,000
 but anyone says that's why the Buddha's great?

293
00:24:23,000 --> 00:24:25,000
 Forget about it.

294
00:24:25,000 --> 00:24:28,950
 That's not why becoming a Buddha or the practice of

295
00:24:28,950 --> 00:24:30,000
 Buddhism

296
00:24:30,000 --> 00:24:37,000
 or the practice of enlightenment is a great thing.

297
00:24:37,000 --> 00:24:44,000
 We don't just practice for morality.

298
00:24:44,000 --> 00:24:47,000
 So on the one hand this is great to learn about speech,

299
00:24:47,000 --> 00:24:49,000
 you can become arrogant about it.

300
00:24:49,000 --> 00:24:53,340
 You can say, "Well, yes, we can't speak like this, we can't

301
00:24:53,340 --> 00:24:54,000
 speak like that."

302
00:24:54,000 --> 00:24:58,000
 And then you feel superior because of it.

303
00:24:58,000 --> 00:25:01,350
 You know, other people are speaking, "Pourly I'm speaking

304
00:25:01,350 --> 00:25:02,000
 well."

305
00:25:02,000 --> 00:25:07,690
 Other people speak, "Harshly I speak softly and pleasing to

306
00:25:07,690 --> 00:25:09,000
 the ear."

307
00:25:09,000 --> 00:25:12,000
 Easy to become attached to it.

308
00:25:12,000 --> 00:25:22,000
 I don't lie and you feel proud of that.

309
00:25:22,000 --> 00:25:28,000
 The Buddha said even animals can go without speaking.

310
00:25:28,000 --> 00:25:40,000
 Speech isn't the measure.

311
00:25:40,000 --> 00:25:44,000
 But nonetheless speech is important, speech and action.

312
00:25:44,000 --> 00:25:49,000
 When it comes back to hunt us, if we use wrong speech,

313
00:25:49,000 --> 00:25:54,580
 just like if we act in the wrong way, it comes back to hunt

314
00:25:54,580 --> 00:25:55,000
 us.

315
00:25:55,000 --> 00:25:59,000
 If we speak in the wrong way, it will come back to hunt us.

316
00:25:59,000 --> 00:26:03,140
 Only when we meditate, when we consider, when we're quiet

317
00:26:03,140 --> 00:26:05,000
 and mindful,

318
00:26:05,000 --> 00:26:09,350
 do we get to see the results and feel good or bad about the

319
00:26:09,350 --> 00:26:10,000
 things that we've done,

320
00:26:10,000 --> 00:26:14,000
 and be encouraged or discouraged about ourselves,

321
00:26:14,000 --> 00:26:25,000
 and be composed or decomposed by our past actions and

322
00:26:25,000 --> 00:26:28,000
 speech.

323
00:26:28,000 --> 00:26:32,000
 So speech is something you have to be careful about.

324
00:26:32,000 --> 00:26:36,000
 Anyway, that's the Dharma for tonight.

325
00:26:36,000 --> 00:26:38,000
 Thank you all for tuning in.

326
00:26:38,000 --> 00:26:40,000
 And good night.

