1
00:00:00,000 --> 00:00:05,760
 Hello and welcome back to our study of the Dhammapada.

2
00:00:05,760 --> 00:00:11,170
 Today we continue on with verse number 71 which reads as

3
00:00:11,170 --> 00:00:12,640
 follows.

4
00:00:12,640 --> 00:00:22,210
 Nahe pa pang katang kamang, sa ju ki rangva, moo chate, tah

5
00:00:22,210 --> 00:00:31,040
antang ba lamanveeti, basma channova pa vakavati.

6
00:00:31,040 --> 00:00:44,080
 Which means, he indeed, na pa pang kam, katang kamang, a

7
00:00:44,080 --> 00:00:48,480
 evil action, when an evil action is done.

8
00:00:48,480 --> 00:01:00,710
 sa ju ki rang, just like milk, kirang, just like milk, kir

9
00:01:00,710 --> 00:01:10,770
ang wa, just like milk, sa ju moo chate means changes

10
00:01:10,770 --> 00:01:13,760
 immediately, na.

11
00:01:13,760 --> 00:01:18,590
 So just, forget the grammar, just as milk doesn't go bad

12
00:01:18,590 --> 00:01:24,640
 immediately, doesn't change and spoil immediately,

13
00:01:24,640 --> 00:01:31,160
 so too are evil deeds that are done. When you do evil deeds

14
00:01:31,160 --> 00:01:36,720
, well just as milk doesn't spoil immediately,

15
00:01:36,720 --> 00:01:42,760
 evil deeds are the same in that they follow the fool, ba l

16
00:01:42,760 --> 00:01:44,640
amanveeti, dahantang burning,

17
00:01:44,640 --> 00:01:51,460
 they follow the fool burning him or her, basma channova pa

18
00:01:51,460 --> 00:01:56,240
 vakavati, just as fire covered with ashes,

19
00:01:56,240 --> 00:02:00,750
 just like fire covered over by ashes, meaning smoldering

20
00:02:00,750 --> 00:02:02,720
 underneath, so the fire doesn't

21
00:02:06,480 --> 00:02:10,280
 burn up immediately, but there's fire in the ashes, when

22
00:02:10,280 --> 00:02:11,440
 the fire burns down,

23
00:02:11,440 --> 00:02:16,000
 it's still hot and it can still rise up and burn you. So

24
00:02:16,000 --> 00:02:18,880
 evil deeds are like that.

25
00:02:18,880 --> 00:02:32,190
 This verse was given supposedly because Mahamogalana, while

26
00:02:32,190 --> 00:02:34,560
 staying on Vulture's Peak,

27
00:02:36,400 --> 00:02:40,080
 which I have now visited three times, we just recently

28
00:02:40,080 --> 00:02:44,000
 visited Vulture's Peak, it's a small

29
00:02:44,000 --> 00:02:49,000
 mountain above Rajagaha that looks somewhat like a vulture

30
00:02:49,000 --> 00:02:52,160
 at the top, or you can at least get how

31
00:02:52,160 --> 00:02:56,400
 they, you know, it was named by the locals. It also seems

32
00:02:56,400 --> 00:02:59,680
 to have had a reputation for being haunted,

33
00:03:00,640 --> 00:03:08,210
 it's where Mahamogalana saw many ghosts. And so this, on

34
00:03:08,210 --> 00:03:14,000
 this occasion, he was going on Aam's Round

35
00:03:14,000 --> 00:03:20,770
 with a, with a thousand, anyway, with a retinue of other

36
00:03:20,770 --> 00:03:24,480
 monks, I think, although it's not clear

37
00:03:24,480 --> 00:03:28,160
 whether it was monks or it was just a bunch of ascetics

38
00:03:28,160 --> 00:03:30,560
 from the area also going on Aam's Round,

39
00:03:30,560 --> 00:03:34,540
 so they were maybe just getting in line with all these guys

40
00:03:34,540 --> 00:03:37,040
. But it's also quite possible that

41
00:03:37,040 --> 00:03:40,930
 this was the group of 1000 monks who had previously been

42
00:03:40,930 --> 00:03:44,160
 ascetics, the grammar is not clear,

43
00:03:44,160 --> 00:03:48,960
 previously been ascetics and now had, these are the people

44
00:03:48,960 --> 00:03:50,800
 who the Buddha taught the,

45
00:03:52,080 --> 00:04:02,720
 the, the, the, the discourse on the fire from burning.

46
00:04:02,720 --> 00:04:10,560
 Anyway, there were a thousand of these

47
00:04:10,560 --> 00:32:18,060
 ascetics going into, into Rajagaha for Aam's, so Mahamogal

48
00:32:18,060 --> 00:04:19,920
ana went with, I think the other monk was

49
00:04:19,920 --> 00:04:28,500
 Luckin, that was his name. And along the way, Mahamogalana

50
00:04:28,500 --> 00:04:34,080
 saw a huge snake. Was it a snake or

51
00:04:34,080 --> 00:04:41,280
 if they saw a huge, yes, first they saw a huge snake and

52
00:04:41,280 --> 00:04:49,440
 with a head, head like a human and a body

53
00:04:49,440 --> 00:04:54,430
 of a snake, but just huge mini stories, big flying through

54
00:04:54,430 --> 00:05:00,720
 the air on fire and in anguish. So it was

55
00:05:00,720 --> 00:05:04,180
 one, it was a ghost that was in terrible pain and suffering

56
00:05:04,180 --> 00:05:08,160
. And this is sort of a theme that seems

57
00:05:08,160 --> 00:05:12,100
 that there are many spirits of this sort wandering around

58
00:05:12,100 --> 00:05:14,960
 the earth in terrible pain and suffering.

59
00:05:16,880 --> 00:05:20,810
 And Mahamogalana smiled and the monk was with him asked,

60
00:05:20,810 --> 00:05:21,600
 why are you smiling?

61
00:05:21,600 --> 00:05:25,620
 That's funny how you might think that Arahants have a bit

62
00:05:25,620 --> 00:05:27,440
 of a queer sense of humor.

63
00:05:27,440 --> 00:05:33,060
 Mahamogalana was an Arahant. It's interesting because they

64
00:05:33,060 --> 00:05:36,240
 do have a bit seem to have a bit of a

65
00:05:36,240 --> 00:05:41,410
 unique sense of humor. The smile is not because of seeing

66
00:05:41,410 --> 00:05:44,320
 suffering, but it's

67
00:05:46,320 --> 00:05:54,400
 a sort of amazement or sort of a happiness that comes from

68
00:05:54,400 --> 00:05:56,720
 understanding the Buddha's teaching

69
00:05:56,720 --> 00:06:02,230
 or understanding the truth and being free from the Ramsa s

70
00:06:02,230 --> 00:06:06,160
amsara. It's a difficult thing to say

71
00:06:06,160 --> 00:06:09,180
 because it sounds somewhat cruel, but it certainly is not

72
00:06:09,180 --> 00:06:11,440
 cruel. There's no sense that he wanted to

73
00:06:11,440 --> 00:06:14,900
 hurt this being or he was happy for their suffering or so

74
00:06:14,900 --> 00:06:17,360
 on. It is absolutely not happy for their

75
00:06:17,360 --> 00:06:22,830
 suffering, which would be a sort of a cruel state, but it's

76
00:06:22,830 --> 00:06:28,480
 a smile at the magnitude of the truth and

77
00:06:28,480 --> 00:06:34,880
 of one's own freedom from that suffering. But I think there

78
00:06:34,880 --> 00:06:39,200
's just a sense of sort of awe and

79
00:06:39,200 --> 00:06:43,350
 amazement at the nature of reality. Like when you see

80
00:06:43,350 --> 00:06:47,040
 something, wow, that's amazing. But there's

81
00:06:47,040 --> 00:06:50,210
 no sense of sadness, I think is an important point to get

82
00:06:50,210 --> 00:06:52,640
 across. People often think when you see

83
00:06:52,640 --> 00:06:55,500
 someone suffering, you have to feel sad, otherwise you have

84
00:06:55,500 --> 00:06:57,360
 no compassion and that's clearly not the

85
00:06:57,360 --> 00:07:01,880
 case. There's a sense of happiness involved with compassion

86
00:07:01,880 --> 00:07:04,880
. Compassion is a very happy state and

87
00:07:04,880 --> 00:07:09,430
 someone who's truly compassionate smiles and smiles at the

88
00:07:09,430 --> 00:07:11,680
 people who are suffering.

89
00:07:11,680 --> 00:07:15,940
 Sadness is anger, it's aversion to the state, it's not

90
00:07:15,940 --> 00:07:19,040
 wholesome. Whereas smiling can be wholesome,

91
00:07:19,040 --> 00:07:22,710
 as in this case. Well, it's actually not wholesome because

92
00:07:22,710 --> 00:07:24,720
 in our Maha-raha there's only functional,

93
00:07:24,720 --> 00:07:30,780
 but it's a positive state or it's a pure state. And so

94
00:07:30,780 --> 00:07:34,080
 anyway, Maha-lakkha and I, the monk who was

95
00:07:34,080 --> 00:07:37,080
 with him, was curious and asked, because he didn't see this

96
00:07:37,080 --> 00:07:38,800
 huge snake flying through the air.

97
00:07:38,800 --> 00:07:45,150
 So he asks Maha-mukalana, "Why are you smiling?" And Mughal

98
00:07:45,150 --> 00:07:47,760
ana doesn't say anything, he says,

99
00:07:47,760 --> 00:07:49,680
 "Ask me again when we're in front of the Buddha."

100
00:07:49,680 --> 00:07:56,810
 And I may be getting this wrong, but there's, he sees

101
00:07:56,810 --> 00:07:58,240
 another ghost.

102
00:07:59,520 --> 00:08:02,090
 Anyway, he sees two ghosts. Let's not get into the details.

103
00:08:02,090 --> 00:08:04,640
 He sees two ghosts. One, the first one's

104
00:08:04,640 --> 00:08:10,620
 a snake and then another day he sees a crow. Same deal,

105
00:08:10,620 --> 00:08:16,320
 huge crow, also on fire. And he's just amazed

106
00:08:16,320 --> 00:08:23,890
 by this state. And he actually, he asks the crow. Yeah, the

107
00:08:23,890 --> 00:08:27,680
 second one, he asks the crow, "What did

108
00:08:27,680 --> 00:08:31,600
 you do to get into this state?" It's not actually a crow,

109
00:08:31,600 --> 00:08:33,840
 it's some kind of ghost within the shape

110
00:08:33,840 --> 00:08:38,310
 of a crow. And the crow says that he, in a past life, he

111
00:08:38,310 --> 00:08:42,720
 stole, he was a crow, and stole some food,

112
00:08:42,720 --> 00:08:48,860
 three mouthfuls of food. Now the English version says that

113
00:08:48,860 --> 00:08:51,360
 it's just food that was left over

114
00:08:52,160 --> 00:08:57,330
 from an offering to monks. And the grammar's again not

115
00:08:57,330 --> 00:09:00,800
 clear, but I think that's an odd,

116
00:09:00,800 --> 00:09:05,900
 I think that's not a very likely reading. So I went to the

117
00:09:05,900 --> 00:09:09,760
 Thai translation, and it says,

118
00:09:09,760 --> 00:09:14,190
 points out that it's not what was left over. It's not, it

119
00:09:14,190 --> 00:09:16,400
 wasn't, so what happened was these

120
00:09:16,400 --> 00:09:21,210
 lay people got together and they were offering food to the

121
00:09:21,210 --> 00:09:23,520
 monks in the area of many, many monks

122
00:09:23,520 --> 00:09:27,760
 getting together. And in a time of a past food, I see this

123
00:09:27,760 --> 00:09:31,840
 is very far into the past. And

124
00:09:31,840 --> 00:09:37,690
 they, so they brought all this food from their homes, and

125
00:09:37,690 --> 00:09:40,560
 then the crow came and ate this food.

126
00:09:40,560 --> 00:09:44,330
 So the question is, when did he eat it? Because it's kind

127
00:09:44,330 --> 00:09:46,560
 of ethically important, the crow was

128
00:09:46,560 --> 00:09:49,260
 watching all this going on, and I guess to some extent

129
00:09:49,260 --> 00:09:53,680
 understood what it meant. But somehow the

130
00:09:53,680 --> 00:09:58,270
 power of the fact that the food was dedicated to pure

131
00:09:58,270 --> 00:10:02,720
 individuals, ostensibly pure and enlightened

132
00:10:02,720 --> 00:10:08,660
 beings, there was a heavy karmic sort of corruption in the

133
00:10:08,660 --> 00:10:13,520
 crow's mind. And the purity of, you could

134
00:10:13,520 --> 00:10:16,400
 think of it as even if the animals don't know what's going

135
00:10:16,400 --> 00:10:19,360
 on, they can feel the purity. And so the

136
00:10:19,360 --> 00:10:22,760
 corruption in the crow's mind to be able to act against

137
00:10:22,760 --> 00:10:25,360
 that, and to take food that was clearly

138
00:10:26,160 --> 00:10:34,970
 designated for other purposes, is a powerful act. So to me

139
00:10:34,970 --> 00:10:38,640
 that doesn't, isn't likely to have been

140
00:10:38,640 --> 00:10:42,150
 the case if it was just left over. So with this suspicion

141
00:10:42,150 --> 00:10:43,920
 went to the Thai translation,

142
00:10:43,920 --> 00:10:46,970
 it's actually the opposite. The Thai translation says, no,

143
00:10:46,970 --> 00:10:51,440
 it wasn't food that was already given

144
00:10:51,440 --> 00:10:55,680
 to the monks. It wasn't even food that they had said this

145
00:10:55,680 --> 00:10:58,320
 is for the monks. It also wasn't food

146
00:10:58,320 --> 00:11:01,500
 that they were taking home as leftovers. It was just food

147
00:11:01,500 --> 00:11:03,760
 that they had brought there, not yet

148
00:11:03,760 --> 00:11:07,920
 having designated it for any purpose, but brought it just

149
00:11:07,920 --> 00:11:10,160
 for the general purpose of making this

150
00:11:10,160 --> 00:11:14,810
 offering. The point is it was involved with something pure.

151
00:11:14,810 --> 00:11:17,040
 And that's an interesting case,

152
00:11:17,040 --> 00:11:19,740
 there are two interesting cases. And that's sort of the

153
00:11:19,740 --> 00:11:21,840
 thing you have to know about the Dhammapada

154
00:11:21,840 --> 00:11:25,920
 is it's, as I've said before, the stories are often a

155
00:11:25,920 --> 00:11:30,400
 little bit wild and shocking for some

156
00:11:30,400 --> 00:11:34,270
 people. It might not be what you'd expect from me and from

157
00:11:34,270 --> 00:11:36,480
 the teachings that I give, which try to

158
00:11:36,480 --> 00:11:41,260
 be very sort of, I guess secular, you could say, or non-my

159
00:11:41,260 --> 00:11:45,360
stical. But the mystical comes very much

160
00:11:45,360 --> 00:11:49,930
 from the practical and from the empirical. And so it's just

161
00:11:49,930 --> 00:11:52,960
 a matter of understanding why it is that

162
00:11:52,960 --> 00:11:56,650
 ghostly beings can exist, why it is that karmic retribution

163
00:11:56,650 --> 00:11:58,080
 like this can occur.

164
00:11:58,080 --> 00:12:07,270
 Anyway, the other ghost, so Moggallana goes with this monk

165
00:12:07,270 --> 00:12:11,920
 to see the Buddha. I think probably,

166
00:12:11,920 --> 00:12:13,980
 or maybe how it happened, although that's not how it's

167
00:12:13,980 --> 00:12:15,840
 written in the book, is that the crow

168
00:12:15,840 --> 00:12:20,390
 happened before and then he came and saw this snake and so

169
00:12:20,390 --> 00:12:23,280
 he brought, he went to the Buddha.

170
00:12:23,280 --> 00:12:27,150
 The reason being is if he had said, "Oh, I see a big snake

171
00:12:27,150 --> 00:12:29,440
 up there," it would be like bragging,

172
00:12:29,440 --> 00:12:31,990
 first of all, showing that he has some kind of magical

173
00:12:31,990 --> 00:12:35,520
 powers. Moggallana was the Buddha's chief

174
00:12:35,520 --> 00:12:39,500
 disciple in regards to magical powers. But it would also

175
00:12:39,500 --> 00:12:41,680
 mean that he would have to stand on

176
00:12:41,680 --> 00:12:47,630
 his own and stand against criticism or suspicion that he

177
00:12:47,630 --> 00:12:51,120
 was lying or making it up or pretending

178
00:12:51,120 --> 00:12:55,520
 to be some magical person. So he said, "Well, let's go and

179
00:12:55,520 --> 00:12:57,760
 wait and ask me that in front of the

180
00:12:57,760 --> 00:13:01,290
 Buddha." So they go in front of the Buddha, Moggallana

181
00:13:01,290 --> 00:13:02,880
 relates what he saw, and then the Buddha

182
00:13:02,880 --> 00:13:08,280
 says, "Oh yes, I was at Kicha Kuta as well and I saw the

183
00:13:08,280 --> 00:13:12,080
 same ghost." Thereby being a witness,

184
00:13:12,080 --> 00:13:14,040
 and the Buddha said as well, "I didn't want to say anything

185
00:13:14,040 --> 00:13:14,960
 until I had a witness."

186
00:13:14,960 --> 00:13:20,860
 It's not that you feel self-conscious or anything, it's you

187
00:13:20,860 --> 00:13:22,080
 don't want people to get

188
00:13:22,080 --> 00:13:26,130
 unwholesome thoughts and doubts in their mind. So instead,

189
00:13:26,130 --> 00:13:28,320
 when you have two people who can verify

190
00:13:28,320 --> 00:13:36,080
 each other's stories, then it has more credence anyway. So

191
00:13:36,080 --> 00:13:42,480
 the snake ghost in a past life

192
00:13:42,480 --> 00:13:46,460
 had been a man and had invited an enlightened being, a P

193
00:13:46,460 --> 00:13:49,840
acheka Buddha, to come and, no sorry,

194
00:13:49,840 --> 00:13:53,660
 a Pacheka Buddha had been invited to come and stay in a

195
00:13:53,660 --> 00:13:56,080
 sort of a temporary leaf hut

196
00:13:58,000 --> 00:14:02,290
 near this person's land. This person was a farmer. And so

197
00:14:02,290 --> 00:14:04,720
 as a result of

198
00:14:04,720 --> 00:14:10,880
 this, the purity and the fame of this enlightened being,

199
00:14:10,880 --> 00:14:18,470
 many people came to visit him in sort of this rural area.

200
00:14:18,470 --> 00:14:21,760
 And many of them would just be ordinary

201
00:14:21,760 --> 00:14:26,860
 townspeople and ignorant to the farmer's work. And so they

202
00:14:26,860 --> 00:14:29,760
'd trample all of his crops and many,

203
00:14:29,760 --> 00:14:34,030
 eventually many people came. And as a result, often his

204
00:14:34,030 --> 00:14:38,000
 crops would be damaged, perhaps even ruined.

205
00:14:38,000 --> 00:14:43,470
 And he got more and more frustrated until finally he

206
00:14:43,470 --> 00:14:46,320
 thought to himself, "You know, look,

207
00:14:47,520 --> 00:14:51,350
 the only reason these people are coming and trampling my

208
00:14:51,350 --> 00:14:53,600
 crops is because of this stupid,

209
00:14:53,600 --> 00:14:58,980
 enlightened individual. I probably didn't think of him as

210
00:14:58,980 --> 00:15:02,160
 that, but this con artist,

211
00:15:02,160 --> 00:15:07,160
 maybe he thought, anyway, this religious individual." So he

212
00:15:07,160 --> 00:15:08,000
 thought, "Well, the only

213
00:15:08,000 --> 00:15:11,280
 thing for me to do is obviously to burn down his hut and

214
00:15:11,280 --> 00:15:15,360
 chase him away." So that's what he did.

215
00:15:15,360 --> 00:15:19,350
 He burned down this hut and I'm not sure chased him away,

216
00:15:19,350 --> 00:15:21,440
 but burned down the hut and

217
00:15:21,440 --> 00:15:23,700
 destroyed the residents of the Pachacabuddha who

218
00:15:23,700 --> 00:15:30,000
 immediately left without a word or complaint.

219
00:15:30,000 --> 00:15:37,110
 But the townspeople weren't as forgiving. So they came at

220
00:15:37,110 --> 00:15:42,000
 that point and asked the farmer,

221
00:15:42,000 --> 00:15:48,290
 "What happened to this hut? It's all burned down." And the

222
00:15:48,290 --> 00:15:50,800
 farmer said, "I burned it down," or

223
00:15:50,800 --> 00:15:54,990
 something like that. "And you trampling all my crops, I had

224
00:15:54,990 --> 00:15:56,560
 to get rid of them."

225
00:15:56,560 --> 00:16:00,850
 And suffice to say, the townspeople who were just ordinary

226
00:16:00,850 --> 00:16:02,880
 individuals and not enlightened,

227
00:16:02,880 --> 00:16:07,940
 they were not happy and being incensed and furious with

228
00:16:07,940 --> 00:16:10,960
 this guy, they beat him.

229
00:16:11,760 --> 00:16:20,400
 And stabbed him and killed him. Bad karma all around. But

230
00:16:20,400 --> 00:16:22,800
 you can say it was far worse karma to,

231
00:16:22,800 --> 00:16:27,050
 it's interesting actually, not far worse karma, but pretty

232
00:16:27,050 --> 00:16:28,880
 weighty karma to do something to such

233
00:16:28,880 --> 00:16:34,440
 a pure individual. And that's important. It was awful that

234
00:16:34,440 --> 00:16:36,800
 he was murdered. And that's definitely

235
00:16:36,800 --> 00:16:41,430
 bad karma for the people who beat him to death. But there

236
00:16:41,430 --> 00:16:44,640
 is a recognition of the power of purity.

237
00:16:44,640 --> 00:16:48,970
 And that's sort of what's being presented in this tale. In

238
00:16:48,970 --> 00:16:52,480
 fact, karma depends on your own actions,

239
00:16:52,480 --> 00:16:55,880
 but it also depends on the people who suffer from it. So if

240
00:16:55,880 --> 00:16:58,400
 they deserve it, it's far less weighty,

241
00:16:58,400 --> 00:17:03,710
 in the sense that they've done bad things as well, or in

242
00:17:03,710 --> 00:17:06,400
 the sense that they're hoarding things if you

243
00:17:06,400 --> 00:17:11,020
 steal from the rich, for example. It's unwholesome because

244
00:17:11,020 --> 00:17:14,640
 you're creating hostility and you're

245
00:17:14,640 --> 00:17:24,190
 going against someone's rightful ownership. You're taking

246
00:17:24,190 --> 00:17:26,000
 something away from someone.

247
00:17:26,000 --> 00:17:31,080
 But if it's just a drop in the ocean, then it's far less

248
00:17:31,080 --> 00:17:32,320
 weighty.

249
00:17:34,400 --> 00:17:37,260
 Further, if it's someone who's corrupt and evil, making

250
00:17:37,260 --> 00:17:39,680
 them angry isn't that big of a deal,

251
00:17:39,680 --> 00:17:45,020
 or hurting them is bad. But it's far worse if the person is

252
00:17:45,020 --> 00:17:47,280
 pure because you're

253
00:17:47,280 --> 00:17:54,710
 acting against this natural inclination to like the person

254
00:17:54,710 --> 00:17:57,360
 and to appreciate their goodness.

255
00:17:57,360 --> 00:18:01,460
 You're going in the opposite direction, or you're moving

256
00:18:01,460 --> 00:18:03,040
 yourself very far away from them.

257
00:18:03,040 --> 00:18:11,160
 And also because of the greatness of their being. So in

258
00:18:11,160 --> 00:18:13,120
 this case, the greatness of that

259
00:18:13,120 --> 00:18:16,880
 person being in that location, chasing him away was a very,

260
00:18:16,880 --> 00:18:18,720
 very bad thing for many,

261
00:18:18,720 --> 00:18:21,800
 many people, not just the enlightened being himself. So

262
00:18:21,800 --> 00:18:25,920
 that's the story, basically.

263
00:18:26,640 --> 00:18:31,380
 And then the Buddha gives this first. He says, "Yes, it's

264
00:18:31,380 --> 00:18:33,280
 amazing how evil deeds..."

265
00:18:33,280 --> 00:18:36,790
 Or it wasn't amazing to the Buddha, but this is the way of

266
00:18:36,790 --> 00:18:38,080
 evil deeds, he said,

267
00:18:38,080 --> 00:18:44,290
 that they don't just flare up and then disappear. They sm

268
00:18:44,290 --> 00:18:47,840
older. And often you don't even see that

269
00:18:47,840 --> 00:18:51,170
 they're evil. As we talked about earlier, I believe that "t

270
00:18:51,170 --> 00:18:53,440
okang, tokang" is "bibhibhit."

271
00:18:54,240 --> 00:18:59,310
 They feel like drops in a cup. Until it gets overflowed,

272
00:18:59,310 --> 00:19:02,160
 you don't realize that it's full.

273
00:19:02,160 --> 00:19:12,160
 So this is the lesson in this verse, in this story.

274
00:19:12,160 --> 00:19:19,150
 And the caution always is that it leads to magical thinking

275
00:19:19,150 --> 00:19:20,720
, to the idea that karma is

276
00:19:20,720 --> 00:19:25,820
 some kind of cosmic force out there. How does it work? And

277
00:19:25,820 --> 00:19:28,560
 is it God? Is it like God? And

278
00:19:28,560 --> 00:19:31,150
 people say, "Well, you have karma, we have God, what's the

279
00:19:31,150 --> 00:19:31,920
 difference?"

280
00:19:31,920 --> 00:19:36,640
 It's quite different, really. Karma is just another

281
00:19:36,640 --> 00:19:39,760
 physical law. And so, as I said,

282
00:19:39,760 --> 00:19:42,860
 it's important for us to connect... It's not a physical law

283
00:19:42,860 --> 00:19:44,080
, it's a natural law.

284
00:19:45,040 --> 00:19:49,750
 It's important for us to connect the moment-by-moment

285
00:19:49,750 --> 00:19:52,640
 reality to the results, these big

286
00:19:52,640 --> 00:19:59,200
 results of the huge snake ghost and the huge crow ghost.

287
00:20:06,880 --> 00:20:12,640
 And so it's important to understand that karma is this

288
00:20:12,640 --> 00:20:15,840
 natural sort of law, sort of the natural...

289
00:20:15,840 --> 00:20:22,050
 It's a part of nature, like this hypothetical, probably

290
00:20:22,050 --> 00:20:25,760
 wrong, concept that if a butterfly

291
00:20:25,760 --> 00:20:30,800
 beats its wings in China, there's earthquakes in America.

292
00:20:30,800 --> 00:20:34,160
 It probably doesn't happen. But the point

293
00:20:34,160 --> 00:20:40,160
 is that we accept that in the physical world, the most

294
00:20:40,160 --> 00:20:44,800
 minor change can potentially have

295
00:20:44,800 --> 00:20:52,170
 awesome repercussions, gigantic, huge repercussions. But we

296
00:20:52,170 --> 00:20:55,440
 don't realize that the mind

297
00:20:55,440 --> 00:20:59,440
 actually works very much the same way. And actually, if you

298
00:20:59,440 --> 00:21:02,560
 think about it, it's quite

299
00:21:02,560 --> 00:21:06,440
 obvious, quite clearly the case. Our actions change

300
00:21:06,440 --> 00:21:08,400
 everything in our lives.

301
00:21:08,400 --> 00:21:13,680
 The decisions that we make are affected by our minds.

302
00:21:13,680 --> 00:21:20,400
 And that changes our lives quite significantly.

303
00:21:22,800 --> 00:21:36,730
 The impressions we make on others, the care that we take in

304
00:21:36,730 --> 00:21:40,160
 our actions, so people who are angry

305
00:21:40,160 --> 00:21:46,260
 or people who are lost in thought, are much more likely to

306
00:21:46,260 --> 00:21:49,840
 hurt themselves physically, and so on.

307
00:21:52,000 --> 00:21:57,680
 And then the habit that is formed, of course, this is the

308
00:21:57,680 --> 00:22:02,480
 connection here, is that karma works on a

309
00:22:02,480 --> 00:22:06,590
 momentary level. When you get angry, it hurts you. When you

310
00:22:06,590 --> 00:22:08,560
 get greedy, it sets you up for

311
00:22:08,560 --> 00:22:14,660
 disappointment. But it also changes you. It changes who you

312
00:22:14,660 --> 00:22:17,360
 are. And this is the sort of the meaning of

313
00:22:18,080 --> 00:22:28,660
 how it leads on to further rebirth. A very profound act,

314
00:22:28,660 --> 00:22:31,440
 like killing someone or stealing

315
00:22:31,440 --> 00:22:37,780
 something from someone, something that is cruel and unw

316
00:22:37,780 --> 00:22:40,880
arranted, even cruel like this, like burning

317
00:22:40,880 --> 00:22:46,180
 down someone's house who didn't deserve it and who was

318
00:22:46,180 --> 00:22:50,480
 actually deserving of great respect and

319
00:22:50,480 --> 00:22:57,070
 reverence even. These acts leave an impression on one's

320
00:22:57,070 --> 00:23:01,920
 mind. You can feel when you've done them,

321
00:23:01,920 --> 00:23:05,400
 if anyone in your past you've done these, you can feel how

322
00:23:05,400 --> 00:23:06,960
 it changes you. It really

323
00:23:08,240 --> 00:23:12,480
 drops your, it's like suddenly you're carrying something

324
00:23:12,480 --> 00:23:14,800
 around with you that you can't let go of.

325
00:23:14,800 --> 00:23:19,030
 And that's what leads you to be reborn in hell. That's what

326
00:23:19,030 --> 00:23:21,120
 leads to be reborn as a ghost or as

327
00:23:21,120 --> 00:23:27,390
 an animal. It's not magic. It's the change, the coarseness

328
00:23:27,390 --> 00:23:32,160
 that comes. And conversely, if you

329
00:23:32,160 --> 00:23:37,570
 work to refine yourself, work to better yourself, work to

330
00:23:37,570 --> 00:23:40,080
 practice charity and morality and

331
00:23:40,080 --> 00:23:43,500
 meditation, then your mind becomes lighter and it's like

332
00:23:43,500 --> 00:23:46,000
 relieving burdens and you can feel yourself

333
00:23:46,000 --> 00:23:51,360
 feeling lighter and happier. And so as a result, you go to

334
00:23:51,360 --> 00:23:52,640
 heaven, which is of course always

335
00:23:52,640 --> 00:23:56,720
 described as light and airy and up there in the cosmos.

336
00:23:56,720 --> 00:23:59,440
 That's not exactly up there, but it's

337
00:24:00,080 --> 00:24:08,250
 in a sense up and out and expansive, whereas hell is far

338
00:24:08,250 --> 00:24:12,640
 more repressed and constrictive.

339
00:24:12,640 --> 00:24:20,390
 So the point here is to not be negligent. And these sort of

340
00:24:20,390 --> 00:24:21,680
 stories are of course given to

341
00:24:21,680 --> 00:24:25,530
 people who accept this idea of the future consequences of

342
00:24:25,530 --> 00:24:28,080
 our deeds as a reminder of

343
00:24:28,080 --> 00:24:32,030
 just how bad it can get, just how dangerous bad deeds can

344
00:24:32,030 --> 00:24:34,160
 be. If you don't want to be a

345
00:24:34,160 --> 00:24:36,000
 10-story

346
00:24:36,000 --> 00:24:43,600
 snake or a crow the size of

347
00:24:43,600 --> 00:24:49,680
 the Skydome

348
00:24:52,400 --> 00:25:00,150
 baseball stadium, burning and crying and wailing at your

349
00:25:00,150 --> 00:25:00,800
 misfortune.

350
00:25:00,800 --> 00:25:07,240
 And just be careful with all things. Be careful is very

351
00:25:07,240 --> 00:25:08,320
 good Buddhist advice.

352
00:25:08,320 --> 00:25:11,450
 That's very much to do with what we're talking about, being

353
00:25:11,450 --> 00:25:12,000
 mindful.

354
00:25:12,000 --> 00:25:15,960
 Shows how important mindfulness is. So this kind of

355
00:25:15,960 --> 00:25:17,840
 reminder is useful. This kind of story

356
00:25:18,400 --> 00:25:21,770
 is a useful reminder whether or not you scoff at the story,

357
00:25:21,770 --> 00:25:23,760
 the idea of a ghost of that sort.

358
00:25:23,760 --> 00:25:32,950
 Who knows? The idea that such beings exist to me isn't far-

359
00:25:32,950 --> 00:25:34,960
fetched or difficult to understand.

360
00:25:34,960 --> 00:25:39,850
 Physical science hasn't found any proof and seems quite

361
00:25:39,850 --> 00:25:42,160
 clear that any evidence of

362
00:25:42,800 --> 00:25:46,440
 such things. But then of course it's always, physical

363
00:25:46,440 --> 00:25:48,400
 science is always looking for

364
00:25:48,400 --> 00:25:57,350
 verification of reality. That statement, just if we can go

365
00:25:57,350 --> 00:25:58,880
 a bit off on the tangent here,

366
00:25:58,880 --> 00:26:06,060
 try not to go too far. There was interesting work done in

367
00:26:06,060 --> 00:26:07,120
 the last century,

368
00:26:08,240 --> 00:26:12,360
 and sort of gathering how good science is, physical science

369
00:26:12,360 --> 00:26:14,560
 is, at ignoring anomalies.

370
00:26:14,560 --> 00:26:21,650
 So this is just to point out for the skeptic that even if

371
00:26:21,650 --> 00:26:22,480
 you did

372
00:26:22,480 --> 00:26:26,650
 get a blip on a radar that turned out to be a ghost, how

373
00:26:26,650 --> 00:26:27,600
 would you know it was a ghost?

374
00:26:28,960 --> 00:26:34,490
 Because history shows, you know, you can plot out the

375
00:26:34,490 --> 00:26:40,400
 statistics. It's 99% of the time it will be

376
00:26:40,400 --> 00:26:42,940
 disregarded as an anomaly. And you'll say, you know,

377
00:26:42,940 --> 00:26:45,280
 problem with the equipment, etc., etc.

378
00:26:45,280 --> 00:26:50,880
 Science is not perfect. So the potential that science could

379
00:26:50,880 --> 00:26:53,200
 detect such beings, I think,

380
00:26:53,200 --> 00:27:00,000
 is still there. Now, but I think a more important point is

381
00:27:00,000 --> 00:27:04,080
 that no matter what we know about the

382
00:27:04,080 --> 00:27:07,890
 physical realm, there's nothing that says that's it. There

383
00:27:07,890 --> 00:27:09,920
's nothing that says that there couldn't

384
00:27:09,920 --> 00:27:17,920
 be some other aspect to reality. And in fact, it seems that

385
00:27:17,920 --> 00:27:21,040
 there has to be something more,

386
00:27:21,040 --> 00:27:24,150
 because we don't have the whole picture. Quantum physics

387
00:27:24,150 --> 00:27:26,000
 shows us that there's something missing,

388
00:27:26,000 --> 00:27:31,390
 unless you believe in multi multiple universes. You have a

389
00:27:31,390 --> 00:27:34,400
 choice, you can either believe in

390
00:27:34,400 --> 00:27:37,590
 all of those theories, or you can see what's right in front

391
00:27:37,590 --> 00:27:39,600
 of you. And that's the mind,

392
00:27:39,600 --> 00:27:42,820
 to see that there is a mental aspect to reality. And in

393
00:27:42,820 --> 00:27:47,760
 fact, in some sense, reality is based on

394
00:27:47,760 --> 00:27:51,320
 experience. The only way you can talk about the physical

395
00:27:51,320 --> 00:27:53,200
 realm is from a point of view of

396
00:27:53,200 --> 00:27:57,230
 observation, which we would say is your own experience. So

397
00:27:57,230 --> 00:27:59,040
 the point being that

398
00:27:59,040 --> 00:28:05,970
 there's no real reason to believe that different states of

399
00:28:05,970 --> 00:28:08,880
 existence could exist. I mean, this

400
00:28:08,880 --> 00:28:11,610
 state of existence exists, and there's nothing special

401
00:28:11,610 --> 00:28:16,800
 about it. So anyway, that's not the

402
00:28:16,800 --> 00:28:20,850
 important point. The important point is the recognition

403
00:28:20,850 --> 00:28:23,760
 that our deeds have consequences,

404
00:28:23,760 --> 00:28:28,560
 and often those consequences are not easily foreseeable.

405
00:28:28,560 --> 00:28:31,440
 They could even be chance,

406
00:28:31,440 --> 00:28:34,200
 you can lose the opportunity of a lifetime if you have a

407
00:28:34,200 --> 00:28:36,880
 job interview or an exam, or something,

408
00:28:36,880 --> 00:28:40,570
 but you're not mentally there. It can change your life, you

409
00:28:40,570 --> 00:28:42,080
 could have all the greatest

410
00:28:42,080 --> 00:28:47,180
 opportunities in the world, and miss them. You could have

411
00:28:47,180 --> 00:28:49,440
 the worst luck in the world,

412
00:28:49,440 --> 00:28:54,260
 and by being mindful and pure in the mind, you could get

413
00:28:54,260 --> 00:28:57,520
 the greatest break, or at the very

414
00:28:57,520 --> 00:29:00,930
 least you could be happy. But more to the point here is

415
00:29:00,930 --> 00:29:03,680
 that great things could happen. I had a

416
00:29:03,680 --> 00:29:07,460
 story that I tell often now of this woman who contacted me,

417
00:29:07,460 --> 00:29:09,360
 and we were in touch for a long

418
00:29:09,360 --> 00:29:12,210
 time, and she was in a desperate, hopeless situation, sort

419
00:29:12,210 --> 00:29:13,920
 of trapped in someone else's house.

420
00:29:13,920 --> 00:29:19,150
 Long story short, I got her to do some chanting, just as a

421
00:29:19,150 --> 00:29:21,840
 sort of a backup to give her confidence

422
00:29:21,840 --> 00:29:27,470
 and reassurance, and it calmed her and it brought her peace

423
00:29:27,470 --> 00:29:30,000
. As a result of this change,

424
00:29:30,000 --> 00:29:33,770
 just a minor change in her life, she decided to bring a

425
00:29:33,770 --> 00:29:38,240
 Buddha image into her room, and it turned

426
00:29:38,240 --> 00:29:41,000
 out that this family refused to have that in their house,

427
00:29:41,000 --> 00:29:42,880
 and they ordered her to destroy it.

428
00:29:42,880 --> 00:29:47,930
 Instead of destroying it, she found someone, a friend of a

429
00:29:47,930 --> 00:29:49,920
 friend, who would come and pick it up,

430
00:29:49,920 --> 00:29:54,180
 and that friend of the friend came to pick up the Buddha

431
00:29:54,180 --> 00:29:56,960
 image and take it away, and they got talking.

432
00:30:02,480 --> 00:30:07,450
 They were quite, obviously quite impressed by her spiritual

433
00:30:07,450 --> 00:30:10,400
 practice, and they gave her a job.

434
00:30:10,400 --> 00:30:12,810
 They found her a job. They said, "Oh, so here's someone you

435
00:30:12,810 --> 00:30:14,880
 can go and live with and take care of,

436
00:30:14,880 --> 00:30:18,860
 I think, take care of an old woman or something." So she

437
00:30:18,860 --> 00:30:22,240
 did. She just up and left and was suddenly

438
00:30:22,240 --> 00:30:26,150
 freed from her situation. I always think of this as sort of

439
00:30:26,150 --> 00:30:28,080
 an example of how setting your mind in

440
00:30:28,080 --> 00:30:30,910
 the right way changes you, because she suddenly couldn't

441
00:30:30,910 --> 00:30:32,960
 live with these people. This is kind of

442
00:30:32,960 --> 00:30:37,340
 a, there's a karmic dissonance, which is actually a thing.

443
00:30:37,340 --> 00:30:40,560
 People with different karmas, no matter

444
00:30:40,560 --> 00:30:44,060
 how hard they try, they can't mesh together and they'll rep

445
00:30:44,060 --> 00:30:46,720
el, whereas people with similar karma

446
00:30:46,720 --> 00:30:52,460
 will attract. So the purity of her mind just naturally grav

447
00:30:52,460 --> 00:30:55,600
itated her towards these people.

448
00:30:55,600 --> 00:30:59,150
 When they met, there was a connection. They could feel the,

449
00:30:59,150 --> 00:31:01,200
 or see the connection, or feel this

450
00:31:01,200 --> 00:31:09,450
 similarity. So these are all sort of part of this idea of

451
00:31:09,450 --> 00:31:13,440
 how evil deeds, good deeds,

452
00:31:13,440 --> 00:31:17,440
 they have far-reaching consequences. Just like milk doesn't

453
00:31:17,440 --> 00:31:18,640
 curdle immediately,

454
00:31:18,640 --> 00:31:22,870
 if you're not careful and you leave the milk out, it will

455
00:31:22,870 --> 00:31:25,280
 spoil. The same with evil deeds.

456
00:31:25,280 --> 00:31:29,330
 If you're not careful with your mind, you keep it out in

457
00:31:29,330 --> 00:31:33,440
 the evil, in amongst the defilements,

458
00:31:33,440 --> 00:31:37,900
 your mind will eventually spoil. Your life will spoil. So

459
00:31:37,900 --> 00:31:39,680
 people who think they can get away with

460
00:31:39,680 --> 00:31:43,930
 evil and say, "No bad is happening to me." They don't

461
00:31:43,930 --> 00:31:47,120
 understand this teaching them. This is like

462
00:31:47,120 --> 00:31:49,570
 milk. You put the milk in the heat, that's still good. You

463
00:31:49,570 --> 00:31:51,200
 take a sip, it's still good, it's still

464
00:31:51,200 --> 00:31:55,500
 good, and then suddenly it's spoiled. Life can be like that

465
00:31:55,500 --> 00:31:57,840
. This farmer thought he'd done a great

466
00:31:57,840 --> 00:32:00,180
 thing, so he even boasted about it. And then suddenly he's

467
00:32:00,180 --> 00:32:03,760
 dead and in hell, and coming back as

468
00:32:03,760 --> 00:32:10,960
 a huge snake goes. So that's the Dhamma Phadpa for today

469
00:32:10,960 --> 00:32:15,520
 and our live session. So thank you for

470
00:32:15,520 --> 00:32:22,400
 tuning in and wishing you...

