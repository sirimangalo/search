1
00:00:00,000 --> 00:00:28,000
 [Silence]

2
00:00:28,000 --> 00:00:30,000
 Good evening everyone.

3
00:00:30,000 --> 00:00:35,000
 Broadcasting Live, March 24th.

4
00:00:35,000 --> 00:00:43,000
 [Silence]

5
00:00:43,000 --> 00:00:47,000
 Today's quote is from the Wakali Suta.

6
00:00:47,000 --> 00:00:52,000
 So Wakali was...

7
00:00:52,000 --> 00:01:00,000
 He was unhappy as a lay person because...

8
00:01:00,000 --> 00:01:12,000
 He was very happy to see the Buddha.

9
00:01:12,000 --> 00:01:15,700
 And as a lay person, he didn't have the opportunity to see

10
00:01:15,700 --> 00:01:20,000
 the Buddha often.

11
00:01:20,000 --> 00:01:26,000
 The Buddha was, of course, very beautiful.

12
00:01:26,000 --> 00:01:32,490
 And so Wakali was one of these people who was enamored by

13
00:01:32,490 --> 00:01:38,000
 the Buddha's features.

14
00:01:38,000 --> 00:01:42,000
 And so he...

15
00:01:42,000 --> 00:01:46,710
 He would go to see the Buddha quite often, but he wasn't

16
00:01:46,710 --> 00:01:48,000
 happy because as a lay person

17
00:01:48,000 --> 00:01:52,090
 he didn't have the opportunity to see the Buddha as often

18
00:01:52,090 --> 00:01:54,000
 as he liked.

19
00:01:54,000 --> 00:01:58,000
 So he became a monk.

20
00:01:58,000 --> 00:02:03,000
 This Suta...

21
00:02:03,000 --> 00:02:11,000
 Says that he was actually quite sick.

22
00:02:11,000 --> 00:02:15,000
 And so he asked his fellow monks, "Go find the Buddha.

23
00:02:15,000 --> 00:02:25,080
 Tell them I'm sick and please see if they'll come visit me

24
00:02:25,080 --> 00:02:26,000
."

25
00:02:26,000 --> 00:02:30,680
 And the Buddha comes and he's ecstatic and he tries to get

26
00:02:30,680 --> 00:02:31,000
 up.

27
00:02:31,000 --> 00:02:36,060
 And the Buddha says, "Don't get up. It's okay. I'll sit

28
00:02:36,060 --> 00:02:43,000
 down in one of the seats."

29
00:02:43,000 --> 00:02:46,000
 And having sat down, he said to them...

30
00:02:46,000 --> 00:02:55,000
 He gave the standard question.

31
00:02:55,000 --> 00:03:04,000
 "Kacchide Wakali Kaminiyam."

32
00:03:04,000 --> 00:03:07,000
 Can you stand it, Wakali?

33
00:03:07,000 --> 00:03:18,000
 Can you bear it?

34
00:03:18,000 --> 00:03:23,000
 Are you able to live with this sickness?

35
00:03:23,000 --> 00:03:27,000
 Kacchide Wakali Kaminiyam.

36
00:03:27,000 --> 00:03:30,000
 No, Abhi Kaminiyam.

37
00:03:30,000 --> 00:03:34,000
 How is it? Are the painful feelings going away?

38
00:03:34,000 --> 00:03:39,000
 Are they decreasing and not increasing?

39
00:03:39,000 --> 00:03:44,000
 Patikamousanangpañayatino abhi kamu.

40
00:03:44,000 --> 00:03:50,000
 Are they reducing, not increasing?

41
00:03:50,000 --> 00:03:54,000
 So he's asking, "Are you well?"

42
00:03:54,000 --> 00:04:03,000
 And Wakali says, name bante Kaminiyam nayapiniyam.

43
00:04:03,000 --> 00:04:09,000
 I can't bear it. I can't stand it.

44
00:04:09,000 --> 00:04:14,000
 Balhamidukkavidana abhi kamunthi.

45
00:04:14,000 --> 00:04:18,000
 My painful feelings are getting worse, not decreasing.

46
00:04:18,000 --> 00:04:23,000
 They're getting stronger, not weaker.

47
00:04:23,000 --> 00:04:27,980
 And the Buddha realizes, "Well, he's possibly not going to

48
00:04:27,980 --> 00:04:31,000
 get over this."

49
00:04:31,000 --> 00:04:36,930
 He says, "Well, then, Wakali, do you have anything you

50
00:04:36,930 --> 00:04:38,000
 regret?

51
00:04:38,000 --> 00:04:41,530
 Do you have any worries or regrets about the past, anything

52
00:04:41,530 --> 00:04:43,000
 that's bothering you?"

53
00:04:43,000 --> 00:04:53,000
 And he says, "Yes, bante, I have some unresolved many, not

54
00:04:53,000 --> 00:05:00,000
 a few, doubt, not a few worries."

55
00:05:00,000 --> 00:05:04,360
 And he says, "Well, Wakali, is it in regards to your

56
00:05:04,360 --> 00:05:05,000
 morality?"

57
00:05:05,000 --> 00:05:11,000
 And he says, "No, I have no problem with my morality."

58
00:05:11,000 --> 00:05:15,110
 He says, "Well, if it's not about your morality, then what

59
00:05:15,110 --> 00:05:17,000
 are you worried about?"

60
00:05:17,000 --> 00:05:20,430
 Making the point that really anything else you could be

61
00:05:20,430 --> 00:05:21,000
 worried about

62
00:05:21,000 --> 00:05:25,000
 is not really important at this point.

63
00:05:25,000 --> 00:05:30,730
 You're sick, you're on your deathbed. Is your morality pure

64
00:05:30,730 --> 00:05:31,000
?

65
00:05:31,000 --> 00:05:35,000
 Is your mind pure? Do you have good intentions?

66
00:05:35,000 --> 00:05:39,250
 What could be wrong? He says, "Well, I haven't seen the

67
00:05:39,250 --> 00:05:41,000
 Buddha in a long time.

68
00:05:41,000 --> 00:05:43,000
 I don't get to see him often.

69
00:05:43,000 --> 00:05:46,000
 Being sick, I'm not able to go and see the Buddha.

70
00:05:46,000 --> 00:05:53,230
 I want to go and see you, and my body doesn't let me do it

71
00:05:53,230 --> 00:05:54,000
."

72
00:05:54,000 --> 00:05:58,590
 And how does the Buddha respond? He says, "Alang Wakali,

73
00:05:58,590 --> 00:06:00,000
 enough Wakali.

74
00:06:00,000 --> 00:06:05,000
 King te imina putukai na deepte na."

75
00:06:05,000 --> 00:06:14,180
 What reason do you want to see this awful, repugnant,

76
00:06:14,180 --> 00:06:18,000
 rotten body?

77
00:06:18,000 --> 00:06:23,070
 What good is it for you to see this body? Wakali had it all

78
00:06:23,070 --> 00:06:24,000
 wrong.

79
00:06:24,000 --> 00:06:28,300
 He thought thinking about the Buddha meant thinking about

80
00:06:28,300 --> 00:06:30,000
 his physical form.

81
00:06:30,000 --> 00:06:35,000
 That's not what thinking about the Buddha is at all.

82
00:06:35,000 --> 00:06:41,650
 This is why we kind of have sort of an ambivalence towards

83
00:06:41,650 --> 00:06:43,000
 images.

84
00:06:43,000 --> 00:06:48,180
 People think a Buddha image allows you to think about the

85
00:06:48,180 --> 00:06:50,000
 Buddha.

86
00:06:50,000 --> 00:06:56,000
 And, yes, it can, but only as a starting point.

87
00:06:56,000 --> 00:06:59,000
 The image itself isn't the Buddha.

88
00:06:59,000 --> 00:07:02,470
 The image can remind you of the Buddha, and when you say, "

89
00:07:02,470 --> 00:07:04,000
Well, the Buddha, all right, the Buddha,"

90
00:07:04,000 --> 00:07:07,000
 then you start thinking about the qualities of the Buddha.

91
00:07:07,000 --> 00:07:11,000
 Not the physical qualities, the mental qualities.

92
00:07:11,000 --> 00:07:18,000
 He was perfectly self-enlightened, endowed with both

93
00:07:18,000 --> 00:07:19,000
 knowledge of the truth

94
00:07:19,000 --> 00:07:21,000
 and conduct according to the truth.

95
00:07:21,000 --> 00:07:23,000
 Means he practiced what he preached.

96
00:07:23,000 --> 00:07:31,000
 The incomparable trainer of humans and angels, well-gone,

97
00:07:31,000 --> 00:07:32,000
 all these kind of things,

98
00:07:32,000 --> 00:07:34,000
 worthy of gifts and offerings.

99
00:07:34,000 --> 00:07:39,000
 Just a worthy sort of person.

100
00:07:39,000 --> 00:07:43,000
 Blessed, enlightened.

101
00:07:43,000 --> 00:07:48,200
 So the Buddha says, "Yokowakali damangvasati, so damangvas

102
00:07:48,200 --> 00:07:49,000
ati."

103
00:07:49,000 --> 00:07:51,000
 It's a fairly famous quote.

104
00:07:51,000 --> 00:07:55,000
 "Who sees the damma, sees me."

105
00:07:55,000 --> 00:07:59,000
 "Yomangvasati so damangvasati."

106
00:07:59,000 --> 00:08:02,000
 "Daman nivakali passanto manvasati."

107
00:08:02,000 --> 00:08:05,000
 "Mangvasanto damangvasati."

108
00:08:05,000 --> 00:08:09,000
 Basically saying, "The Buddha is one with a damma."

109
00:08:09,000 --> 00:08:17,000
 "It's the damma that you have to get to."

110
00:08:17,000 --> 00:08:20,740
 "To see the Buddha, you have to see the damma, and it's one

111
00:08:20,740 --> 00:08:23,000
 and the same."

112
00:08:23,000 --> 00:08:27,000
 And so he tries to teach Yokowakali about the truth,

113
00:08:27,000 --> 00:08:30,240
 because Yokowakali has this idea of the beautiful body of

114
00:08:30,240 --> 00:08:31,000
 the Buddha

115
00:08:31,000 --> 00:08:35,450
 as being something pleasant, something he can find

116
00:08:35,450 --> 00:08:36,000
 happiness in.

117
00:08:36,000 --> 00:08:39,000
 And he says, "What do you think, Wakkali?

118
00:08:39,000 --> 00:08:45,000
 Is rupa, is this form permanent or impermanent?"

119
00:08:45,000 --> 00:08:49,000
 So he has to admit, "Nityam bandhke."

120
00:08:49,000 --> 00:08:52,140
 This is what you can, you can look at this as a form of

121
00:08:52,140 --> 00:08:53,000
 reporting.

122
00:08:53,000 --> 00:08:56,700
 Like we do these courses and we meet with the teacher once

123
00:08:56,700 --> 00:08:57,000
 a day.

124
00:08:57,000 --> 00:08:59,000
 The Buddha did this as well.

125
00:08:59,000 --> 00:09:03,710
 He asked the question, "Tangin manyasim wakali rupa nityam

126
00:09:03,710 --> 00:09:05,000
 wah nityam wah."

127
00:09:05,000 --> 00:09:08,910
 "What do you think of form? Is it permanent or impermanent

128
00:09:08,910 --> 00:09:09,000
?"

129
00:09:09,000 --> 00:09:12,000
 Stable or unstable? Constant or incontant?

130
00:09:12,000 --> 00:09:15,000
 "Nityam bandhke."

131
00:09:15,000 --> 00:09:17,000
 It's changing all the time.

132
00:09:17,000 --> 00:09:20,000
 Not only are we getting older and our body is changing,

133
00:09:20,000 --> 00:09:25,000
 but the experience of the body only arises and ceases.

134
00:09:25,000 --> 00:09:28,550
 What we think of as the body is made up of experiences that

135
00:09:28,550 --> 00:09:31,000
 arise and cease.

136
00:09:31,000 --> 00:09:36,000
 Those experiences are changing all the time.

137
00:09:36,000 --> 00:09:39,020
 So it's unstable, you know, there's nothing that you can

138
00:09:39,020 --> 00:09:40,000
 hold on to.

139
00:09:40,000 --> 00:09:43,720
 This is why it can change so quickly and suddenly the form

140
00:09:43,720 --> 00:09:45,000
 is changed.

141
00:09:45,000 --> 00:09:49,570
 Cut your hand, you cut yourself, you hurt yourself, you get

142
00:09:49,570 --> 00:09:51,000
 sick.

143
00:09:51,000 --> 00:09:54,000
 The whole experience can change and then you're at a loss

144
00:09:54,000 --> 00:09:56,000
 and you weren't prepared for that.

145
00:09:56,000 --> 00:09:58,000
 You're clinging to something this way.

146
00:09:58,000 --> 00:10:00,000
 You want to see the beautiful body of the Buddha.

147
00:10:00,000 --> 00:10:04,000
 Then he gets sick and he can't go and see the Buddha.

148
00:10:04,000 --> 00:10:11,000
 He loses the rupa that he wants to see.

149
00:10:11,000 --> 00:10:14,000
 And he says, "So what is yang banani changdu kang?"

150
00:10:14,000 --> 00:10:17,000
 This is actually from the Anattala Kanasuta as well.

151
00:10:17,000 --> 00:10:19,000
 It's a standard teaching.

152
00:10:19,000 --> 00:10:24,000
 "Nang banani changdu kangwa tang sukangwa."

153
00:10:24,000 --> 00:10:30,000
 What is impermanent is that suffering or is it happiness?

154
00:10:30,000 --> 00:10:36,760
 So this is the idea that the inconstancy of it means it can

155
00:10:36,760 --> 00:10:40,000
't fulfill your desires.

156
00:10:40,000 --> 00:10:42,000
 It won't go in accordance with your desires.

157
00:10:42,000 --> 00:10:45,000
 You want it to be, it doesn't come.

158
00:10:45,000 --> 00:10:49,000
 You want it to go, it doesn't go.

159
00:10:49,000 --> 00:10:51,000
 It's not in sync with our desires.

160
00:10:51,000 --> 00:11:00,000
 So we suffer as a result, we get stressed as a result.

161
00:11:00,000 --> 00:11:05,360
 Then he says, "Well, yang banani changdu kangwa yiparinaam

162
00:11:05,360 --> 00:11:08,000
adamang."

163
00:11:08,000 --> 00:11:13,730
 What is impermanent and suffering and subject to alteration

164
00:11:13,730 --> 00:11:18,000
, subject to change?

165
00:11:18,000 --> 00:11:21,000
 "Kalang utang samanupasitung."

166
00:11:21,000 --> 00:11:26,000
 Is it proper for that to say, to say about that?

167
00:11:26,000 --> 00:11:28,000
 "Etang mama," this is mine.

168
00:11:28,000 --> 00:11:31,000
 "Eso hamasumi," this I am.

169
00:11:31,000 --> 00:11:34,000
 "Eso meyata."

170
00:11:34,000 --> 00:11:36,000
 This is myself.

171
00:11:36,000 --> 00:11:40,470
 These are the three types of clinging with views, clinging

172
00:11:40,470 --> 00:11:41,000
 with conceit,

173
00:11:41,000 --> 00:11:43,000
 clinging with craving.

174
00:11:43,000 --> 00:11:46,000
 "Etang mama," this is mine.

175
00:11:46,000 --> 00:11:48,000
 That's clinging with craving.

176
00:11:48,000 --> 00:11:51,000
 Some things we cling to because we want them.

177
00:11:51,000 --> 00:11:57,000
 "Eso hamasumi," this I am.

178
00:11:57,000 --> 00:11:59,000
 That's clinging with conceit.

179
00:11:59,000 --> 00:12:01,000
 So we think of I am this, I am that.

180
00:12:01,000 --> 00:12:07,300
 I am strong, I am smart, I am beautiful, or I am ugly, I am

181
00:12:07,300 --> 00:12:12,000
 stupid, etc.

182
00:12:12,000 --> 00:12:16,000
 Weak.

183
00:12:16,000 --> 00:12:24,000
 This is conceit, clinging to characteristics.

184
00:12:24,000 --> 00:12:28,000
 "Eso meyata," "Eso meyata,"

185
00:12:28,000 --> 00:12:32,000
 this is myself, that's a view, clinging with view.

186
00:12:32,000 --> 00:12:36,000
 So we cling to the idea that this is me, this is an entity,

187
00:12:36,000 --> 00:12:37,000
 this is a thing.

188
00:12:37,000 --> 00:12:40,000
 The self exists, the soul exists.

189
00:12:40,000 --> 00:12:44,260
 I can control things, I can control my body because this

190
00:12:44,260 --> 00:12:46,000
 body is myself.

191
00:12:46,000 --> 00:12:51,000
 Those are views we cling to reviews.

192
00:12:51,000 --> 00:13:01,000
 "Etang mama," "Eso hamasumi," "Eso meyata."

193
00:13:01,000 --> 00:13:06,000
 And so then he basically repeats the Anatta Lakana Sutta.

194
00:13:06,000 --> 00:13:20,000
 And luckily having heard this,

195
00:13:20,000 --> 00:13:27,000
 asked his, well then the Buddha left,

196
00:13:27,000 --> 00:13:31,050
 and luckily asked his friends to lift him up and carry him

197
00:13:31,050 --> 00:13:40,000
 to Isigili.

198
00:13:40,000 --> 00:13:42,000
 Because he was ready to die and he said,

199
00:13:42,000 --> 00:13:45,000
 "How can one like me think of dying among the houses?"

200
00:13:45,000 --> 00:13:49,620
 He didn't want to die in the house, he wanted to die as a

201
00:13:49,620 --> 00:13:50,000
 monk.

202
00:13:50,000 --> 00:14:09,000
 So they took him to Isigili and...

203
00:14:09,000 --> 00:14:13,000
 Some stuff happened, it's not really important.

204
00:14:13,000 --> 00:14:16,000
 Let's get on to more of a lesson.

205
00:14:16,000 --> 00:14:20,560
 We have some lesson here, but I think the overall lesson is

206
00:14:20,560 --> 00:14:23,000
 about the teaching,

207
00:14:23,000 --> 00:14:25,000
 not the teacher, right?

208
00:14:25,000 --> 00:14:31,000
 It's easy to focus on person, place and thing without

209
00:14:31,000 --> 00:14:38,000
 focusing on our...

210
00:14:38,000 --> 00:14:42,000
 our own wisdom and our own understanding.

211
00:14:42,000 --> 00:14:45,000
 Nobody else can lead us to enlightenment.

212
00:14:45,000 --> 00:14:50,000
 It's kind of in general the attachment to form.

213
00:14:50,000 --> 00:14:52,000
 You can attach to the form of meditation and say,

214
00:14:52,000 --> 00:14:55,000
 "Okay, I have to do an hour of meditation,"

215
00:14:55,000 --> 00:15:01,000
 or "I have to do so many hours of meditation."

216
00:15:01,000 --> 00:15:04,000
 Without actually meditating, you know?

217
00:15:04,000 --> 00:15:07,610
 You're so focused on the idea of the form that you forget

218
00:15:07,610 --> 00:15:10,000
 the present moment.

219
00:15:10,000 --> 00:15:12,000
 So it goes with all kinds of form.

220
00:15:12,000 --> 00:15:18,000
 You can focus so much on a teacher or even on their words.

221
00:15:18,000 --> 00:15:21,720
 You can like what they say without actually keeping it in

222
00:15:21,720 --> 00:15:22,000
 mind,

223
00:15:22,000 --> 00:15:26,000
 putting it into practice, that kind of thing.

224
00:15:26,000 --> 00:15:29,280
 I mean, it's not to say that we all do this, but we have to

225
00:15:29,280 --> 00:15:31,000
 be careful not to.

226
00:15:31,000 --> 00:15:35,000
 People like Wakkalei got into an extreme of watching the

227
00:15:35,000 --> 00:15:35,000
 Buddha

228
00:15:35,000 --> 00:15:38,000
 not listening to his teachings.

229
00:15:38,000 --> 00:15:41,000
 Eventually he became enlightened.

230
00:15:41,000 --> 00:15:46,000
 But it's something about teachers and people as well.

231
00:15:46,000 --> 00:15:49,000
 You know, a lot of the Buddha's teaching is about,

232
00:15:49,000 --> 00:15:52,320
 I think it was a couple of nights ago, not being critical

233
00:15:52,320 --> 00:15:53,000
 of others.

234
00:15:53,000 --> 00:15:57,000
 Looking at your own faults.

235
00:15:57,000 --> 00:16:00,000
 Sometimes we go to a monastery or a meditation center

236
00:16:00,000 --> 00:16:03,000
 and all we can see are the faults of others.

237
00:16:03,000 --> 00:16:04,000
 It's common.

238
00:16:04,000 --> 00:16:07,000
 I mean, there's a lot of corruption in monasteries.

239
00:16:07,000 --> 00:16:13,350
 It's not without its justification, but it doesn't really

240
00:16:13,350 --> 00:16:14,000
 help.

241
00:16:14,000 --> 00:16:17,000
 On the other hand, we sometimes put people up on a pedestal

242
00:16:17,000 --> 00:16:24,000
 and when we find out they're human, well, we lose,

243
00:16:24,000 --> 00:16:27,020
 we become discouraged, which, you know, we really shouldn't

244
00:16:27,020 --> 00:16:28,000
.

245
00:16:28,000 --> 00:16:30,000
 We focus on the teachings.

246
00:16:30,000 --> 00:16:33,000
 They're much more important.

247
00:16:33,000 --> 00:16:36,000
 You can ask yourself, are these teachings good?

248
00:16:36,000 --> 00:16:37,000
 The teachings are good.

249
00:16:37,000 --> 00:16:39,000
 Stick by them.

250
00:16:39,000 --> 00:16:40,000
 See the Dhamma.

251
00:16:40,000 --> 00:16:47,000
 Don't worry too much about seeing the Buddha.

252
00:16:47,000 --> 00:16:56,000
 So, simple teaching, something to think about.

253
00:16:56,000 --> 00:16:59,000
 I'm going to post the thing out.

254
00:16:59,000 --> 00:17:04,000
 You're welcome if anybody has any questions.

255
00:17:04,000 --> 00:17:09,000
 I understand that I'm quite limiting my online presence

256
00:17:09,000 --> 00:17:11,000
 and I think some people are complaining.

257
00:17:11,000 --> 00:17:15,000
 They want to ask questions and they can't come on here.

258
00:17:15,000 --> 00:17:17,000
 I understand that.

259
00:17:17,000 --> 00:17:23,000
 I just don't have a means of, you know, sort of filtering.

260
00:17:23,000 --> 00:17:29,480
 I'm doing the best I can, but I think it can't help

261
00:17:29,480 --> 00:17:31,000
 everyone.

262
00:17:31,000 --> 00:17:35,360
 So, if you really want help from me, you've got to make the

263
00:17:35,360 --> 00:17:36,000
 effort to come here

264
00:17:36,000 --> 00:17:40,540
 or sign up for one of the slots for an online meditation

265
00:17:40,540 --> 00:17:41,000
 course

266
00:17:41,000 --> 00:17:48,000
 and actually do some meditating.

267
00:17:48,000 --> 00:17:53,360
 Anyway, I don't know if there are any questions I'm not

268
00:17:53,360 --> 00:17:54,000
 going to answer

269
00:17:54,000 --> 00:17:56,000
 if you come on the Hangout.

270
00:17:56,000 --> 00:17:59,000
 Otherwise, just say goodnight.

271
00:17:59,000 --> 00:18:06,000
 It's going to get fairly hectic the next week and a half.

272
00:18:06,000 --> 00:18:12,000
 Next, well, next 20, 30 years probably.

273
00:18:12,000 --> 00:18:15,000
 No.

274
00:18:15,000 --> 00:18:17,470
 Yeah, the next week and a half probably is going to be a

275
00:18:17,470 --> 00:18:18,000
 bit rough.

276
00:18:18,000 --> 00:18:25,000
 I may not be able to broadcast every night.

277
00:18:25,000 --> 00:18:27,000
 Okay, I'm going to say goodnight.

278
00:18:27,000 --> 00:18:28,000
 Goodnight, everyone.

279
00:18:28,000 --> 00:18:33,000
 Thank you for coming out and meditating together.

280
00:18:33,000 --> 00:18:35,000
 Have a good night.

281
00:18:35,000 --> 00:18:36,000
 Thank you.

282
00:18:37,000 --> 00:18:52,080
 [

