1
00:00:00,000 --> 00:00:18,330
 For many years, I was a regular reader of the Reader's

2
00:00:18,330 --> 00:00:23,800
 Digest magazine.

3
00:00:23,800 --> 00:00:30,650
 I remember one cartoon in one of the issues of Reader's

4
00:00:30,650 --> 00:00:32,880
 Digest magazine.

5
00:00:32,880 --> 00:00:37,640
 It may be 30 years old.

6
00:00:37,640 --> 00:00:45,810
 So that cartoon was of a picture of a religious minister

7
00:00:45,810 --> 00:00:48,880
 about to give a religious talk to

8
00:00:48,880 --> 00:00:52,280
 the congregation.

9
00:00:52,280 --> 00:00:56,800
 And the picture shows him from the back.

10
00:00:56,800 --> 00:01:10,630
 And under the podium, under the microphone stand, there was

11
00:01:10,630 --> 00:01:15,620
 a bag of golf clubs.

12
00:01:15,620 --> 00:01:27,080
 And the caption says, "Today I will be brief."

13
00:01:27,080 --> 00:01:37,280
 So following that cartoon, I think I will be brief today.

14
00:01:37,280 --> 00:01:50,680
 But I'm not going to play golf.

15
00:01:50,680 --> 00:01:56,760
 I was searching for a subject to talk about tonight.

16
00:01:56,760 --> 00:02:04,200
 And I did not know what to talk about.

17
00:02:04,200 --> 00:02:10,730
 Unfortunately, I got some topic from the interviews with

18
00:02:10,730 --> 00:02:12,260
 the yogis.

19
00:02:12,260 --> 00:02:15,640
 So some yogis asked me some questions.

20
00:02:15,640 --> 00:02:22,070
 And one yogi even asked me to give the answer to her

21
00:02:22,070 --> 00:02:26,920
 question when I gave it to my talk.

22
00:02:26,920 --> 00:02:33,130
 So today's talk will not be on one subject only, but some

23
00:02:33,130 --> 00:02:36,720
 questions the yogis asked me

24
00:02:36,720 --> 00:02:41,640
 to explain.

25
00:02:41,640 --> 00:02:48,240
 The first thing I want to talk about is about equanimity.

26
00:02:48,240 --> 00:02:54,920
 Now a yogi wants to know what equanimity is.

27
00:02:54,920 --> 00:03:06,850
 Equanimity as used in Buddhist books is a neutrality,

28
00:03:06,850 --> 00:03:11,840
 neutrality of mind.

29
00:03:11,840 --> 00:03:21,480
 Neutrality means not falling to either side.

30
00:03:21,480 --> 00:03:26,240
 So to stay in the middle and not to fall to either side.

31
00:03:26,240 --> 00:03:34,560
 And that is called equanimity in the Buddhist teachings.

32
00:03:34,560 --> 00:03:41,380
 And in the teachings of the Buddha, equanimity is a mental

33
00:03:41,380 --> 00:03:42,560
 state.

34
00:03:42,560 --> 00:03:50,540
 It is, many of you know, one of the 52 mental factors

35
00:03:50,540 --> 00:03:54,200
 taught in Abhidhamma.

36
00:03:54,200 --> 00:03:58,640
 And this is called specific neutrality.

37
00:03:58,640 --> 00:04:04,960
 So specific neutrality is a name for equanimity consisting

38
00:04:04,960 --> 00:04:09,080
 in the equal balance of the associated

39
00:04:09,080 --> 00:04:10,480
 states.

40
00:04:10,480 --> 00:04:16,200
 Now the mental states arise together.

41
00:04:16,200 --> 00:04:21,530
 There are two mental states, consciousness and mental

42
00:04:21,530 --> 00:04:22,680
 factors.

43
00:04:22,680 --> 00:04:29,660
 So consciousness and mental factors always arise together.

44
00:04:29,660 --> 00:04:35,390
 So when they arise together, they arise together and they

45
00:04:35,390 --> 00:04:38,860
 do their respective functions.

46
00:04:38,860 --> 00:04:45,420
 So for them to do their respective functions, probably

47
00:04:45,420 --> 00:04:49,100
 there is one mental state called

48
00:04:49,100 --> 00:04:51,320
 specific neutrality.

49
00:04:51,320 --> 00:05:00,420
 So this keeps the associated states or cognizant mental

50
00:05:00,420 --> 00:05:05,120
 states in equal balance.

51
00:05:05,120 --> 00:05:15,800
 This specific neutrality takes many forms.

52
00:05:15,800 --> 00:05:22,270
 Now when this specific neutrality arises in the mind of an

53
00:05:22,270 --> 00:05:26,280
 Arahant, it is called equanimity

54
00:05:26,280 --> 00:05:29,880
 with six factors.

55
00:05:29,880 --> 00:05:39,290
 That means when an Arahant sees a visible object, he is

56
00:05:39,290 --> 00:05:46,080
 neither glad nor sad about that object.

57
00:05:46,080 --> 00:05:52,500
 So he is not moved by the quality of the object.

58
00:05:52,500 --> 00:05:58,880
 So an Arahant can dwell in equanimity and be mindful and

59
00:05:58,880 --> 00:06:02,160
 fully aware of the object, but

60
00:06:02,160 --> 00:06:09,120
 he will not be clad or said when he sees the object.

61
00:06:09,120 --> 00:06:11,760
 So the same with the other kinds of objects.

62
00:06:11,760 --> 00:06:17,390
 There are six kinds of objects and that is why this equanim

63
00:06:17,390 --> 00:06:20,480
ity is called equanimity with

64
00:06:20,480 --> 00:06:23,560
 six factors.

65
00:06:23,560 --> 00:06:28,480
 Actually equanimity regarding six kinds of objects.

66
00:06:28,480 --> 00:06:33,850
 So whenever an Arahant sees or experiences any one of the

67
00:06:33,850 --> 00:06:36,560
 six kinds of objects, he is

68
00:06:36,560 --> 00:06:44,720
 able to keep his mind neutral, not liking, not disliking.

69
00:06:44,720 --> 00:06:53,300
 So that kind of equanimity is called equanimity with six

70
00:06:53,300 --> 00:06:55,200
 factors.

71
00:06:55,200 --> 00:07:02,520
 When the equanimity or neutrality is practiced towards

72
00:07:02,520 --> 00:07:07,120
 beings, then it is called a divine

73
00:07:07,120 --> 00:07:10,120
 abiding of Brahma-Vihara.

74
00:07:10,120 --> 00:07:16,840
 So you know there are four kinds of Brahma-Vihara's.

75
00:07:16,840 --> 00:07:25,150
 Loving kindness, compassion, sympathetic joy and equanimity

76
00:07:25,150 --> 00:07:25,680
.

77
00:07:25,680 --> 00:07:32,000
 Equanimity is neutrality towards all beings.

78
00:07:32,000 --> 00:07:40,430
 That means when a person practices equanimity as a divine

79
00:07:40,430 --> 00:07:45,680
 abiding, even when he sees a being

80
00:07:45,680 --> 00:07:51,560
 in suffering, he will not feel compassion.

81
00:07:51,560 --> 00:07:57,960
 Even if he sees a person in prosperity, he will not feel

82
00:07:57,960 --> 00:07:59,640
 happiness.

83
00:07:59,640 --> 00:08:05,400
 So whether he sees a person in distress or in prosperity,

84
00:08:05,400 --> 00:08:08,240
 his mind is always neutral.

85
00:08:08,240 --> 00:08:23,080
 So that kind of neutrality is one form equanimity takes.

86
00:08:23,080 --> 00:08:31,960
 And equanimity can take a form of factor of enlightenment.

87
00:08:31,960 --> 00:08:37,560
 So equanimity is one of the seven factors of enlightenment.

88
00:08:37,560 --> 00:08:46,290
 So there equanimity means again neutrality among the cogniz

89
00:08:46,290 --> 00:08:50,400
ant or associated states.

90
00:08:50,400 --> 00:08:58,240
 That means when the equanimity is developed to the level of

91
00:08:58,240 --> 00:09:02,480
 enlightenment factor, again

92
00:09:02,480 --> 00:09:11,790
 this mental state can keep other factors of enlightenment

93
00:09:11,790 --> 00:09:16,560
 in proper balance and letting

94
00:09:16,560 --> 00:09:21,040
 them do their own respective functions.

95
00:09:21,040 --> 00:09:27,230
 And when this arises in a yogi, a yogi does not have to

96
00:09:27,230 --> 00:09:30,520
 make effort, say to be mindful

97
00:09:30,520 --> 00:09:34,120
 or to watch objects.

98
00:09:34,120 --> 00:09:40,840
 So it is effortless awareness of the objects when this equ

99
00:09:40,840 --> 00:09:45,960
animity is developed to the enlightenment

100
00:09:45,960 --> 00:09:50,200
 factor.

101
00:09:50,200 --> 00:10:00,490
 And when it arises with jhana, it is called equanimity of j

102
00:10:00,490 --> 00:10:02,040
hana.

103
00:10:02,040 --> 00:10:07,600
 Now you know there are different stages of jhana, the first

104
00:10:07,600 --> 00:10:10,280
 jhana, second jhana and so

105
00:10:10,280 --> 00:10:13,140
 on.

106
00:10:13,140 --> 00:10:23,410
 In the fourth jhana, this factor is so developed that even

107
00:10:23,410 --> 00:10:28,880
 in the bliss, even in the highest

108
00:10:28,880 --> 00:10:34,200
 bliss of jhana, it is impartial.

109
00:10:34,200 --> 00:10:41,890
 That means the equanimity is not partial to the bliss

110
00:10:41,890 --> 00:10:46,000
 attained through jhana.

111
00:10:46,000 --> 00:10:51,000
 The bliss attained through jhana is said to be very refined

112
00:10:51,000 --> 00:10:52,400
 and sublime.

113
00:10:52,400 --> 00:11:06,660
 But even in such bliss, a yogi is impartial. Even among

114
00:11:06,660 --> 00:11:08,040
 these highest bliss, he is impartial.

115
00:11:08,040 --> 00:11:16,780
 He will not like or dislike that kind of bliss or he will

116
00:11:16,780 --> 00:11:18,520
 not be interested in that kind

117
00:11:18,520 --> 00:11:19,900
 of bliss.

118
00:11:19,900 --> 00:11:25,580
 So he tries to transcend bliss and enjoy what is called equ

119
00:11:25,580 --> 00:11:26,840
animity.

120
00:11:26,840 --> 00:11:31,190
 And it is said that this equanimity is on a higher level

121
00:11:31,190 --> 00:11:38,060
 than what we call sukha or bliss.

122
00:11:38,060 --> 00:11:44,790
 And when equanimity arises with the highest jhana, it is

123
00:11:44,790 --> 00:11:49,500
 called equanimity regarding purifying.

124
00:11:49,500 --> 00:11:56,140
 That means the highest jhana is the most purified of all j

125
00:11:56,140 --> 00:11:57,360
hanas.

126
00:11:57,360 --> 00:12:03,870
 And when a yogi has reached the highest, the most purified

127
00:12:03,870 --> 00:12:07,160
 jhana, he has no interest in

128
00:12:07,160 --> 00:12:10,440
 purifying it because it is already purified.

129
00:12:10,440 --> 00:12:15,820
 So the equanimity arising together with that highest jhana

130
00:12:15,820 --> 00:12:19,480
 is called equanimity about purifying.

131
00:12:19,480 --> 00:12:26,710
 That means he is indifferent in purifying because it is

132
00:12:26,710 --> 00:12:29,640
 already purified.

133
00:12:29,640 --> 00:12:39,370
 So equanimity is neutrality, impartiality, and being unm

134
00:12:39,370 --> 00:12:45,640
oved by the objects either desirable

135
00:12:45,640 --> 00:12:51,360
 or undesirable.

136
00:12:51,360 --> 00:12:57,220
 So just one equanimity is called six-factor equanimity, equ

137
00:12:57,220 --> 00:12:59,920
animity as a divine abiding

138
00:12:59,920 --> 00:13:14,880
 and so on, depending on where it arises.

139
00:13:14,880 --> 00:13:36,460
 So this equanimity or neutrality yogis experience when

140
00:13:36,460 --> 00:13:40,280
 their meditation is good, when they have

141
00:13:40,280 --> 00:13:43,200
 a good concentration.

142
00:13:43,200 --> 00:13:53,320
 Now some people want to know whether this is nibbana.

143
00:13:53,320 --> 00:13:56,080
 This is not nibbana.

144
00:13:56,080 --> 00:14:02,080
 Now in order to understand this, you need to understand

145
00:14:02,080 --> 00:14:04,240
 what nibbana is.

146
00:14:04,240 --> 00:14:09,260
 And it is very difficult to talk about nibbana because it

147
00:14:09,260 --> 00:14:12,240
 cannot be described in the terms

148
00:14:12,240 --> 00:14:15,240
 we are used to.

149
00:14:15,240 --> 00:14:24,170
 Because when we explain nibbana, we use negative terms like

150
00:14:24,170 --> 00:14:27,880
 cessation of dukkha, cessation

151
00:14:27,880 --> 00:14:36,940
 of suffering, or extinction of greed, hatred, and delusion,

152
00:14:36,940 --> 00:14:38,680
 and so on.

153
00:14:38,680 --> 00:14:45,760
 Dukkha is, let us say, a state, a state that can be taken

154
00:14:45,760 --> 00:14:47,360
 as object.

155
00:14:47,360 --> 00:14:53,400
 But it is not a subject.

156
00:14:53,400 --> 00:14:57,160
 That means it does not take any object.

157
00:14:57,160 --> 00:15:05,610
 But it is the object which is taken by the other states,

158
00:15:05,610 --> 00:15:09,040
 other mental states.

159
00:15:09,040 --> 00:15:16,620
 And it is said that nibbana is not made, not created, and

160
00:15:16,620 --> 00:15:20,800
 unborn, and not conditioned.

161
00:15:20,800 --> 00:15:23,980
 That is why it is difficult to understand and difficult to

162
00:15:23,980 --> 00:15:24,760
 describe.

163
00:15:24,760 --> 00:15:30,440
 So what is not conditioned has no beginning.

164
00:15:30,440 --> 00:15:35,850
 Only when something is made or something is conditioned, it

165
00:15:35,850 --> 00:15:37,300
 has a beginning.

166
00:15:37,300 --> 00:15:41,510
 But since nibbana is not made, not conditioned, it has no

167
00:15:41,510 --> 00:15:42,560
 beginning.

168
00:15:42,560 --> 00:15:45,640
 That is why it has no end.

169
00:15:45,640 --> 00:15:49,000
 If it has beginning, it must have an end.

170
00:15:49,000 --> 00:15:55,160
 Now, since it has no beginning, it has no end.

171
00:15:55,160 --> 00:15:59,930
 And how can we perceive or think of something that has no

172
00:15:59,930 --> 00:16:02,280
 beginning and no end, and still

173
00:16:02,280 --> 00:16:06,400
 it is something that can be taken as object?

174
00:16:06,400 --> 00:16:16,800
 So it is very difficult.

175
00:16:16,800 --> 00:16:23,200
 In a Bhidama, nibbana is classified as an external object.

176
00:16:23,200 --> 00:16:31,340
 So nibbana cannot be in the minds of beings.

177
00:16:31,340 --> 00:16:38,490
 It is an external object that can be taken as object by

178
00:16:38,490 --> 00:16:43,000
 some types of consciousness.

179
00:16:43,000 --> 00:16:51,080
 And it is also said that nibbana is not a mental state.

180
00:16:51,080 --> 00:16:53,640
 Nibbana is not made up.

181
00:16:53,640 --> 00:17:05,000
 So no mental state, no matter, no other element, and so on.

182
00:17:05,000 --> 00:17:15,220
 So it is a state of peacefulness or it is the extinction of

183
00:17:15,220 --> 00:17:19,800
 mental defilement or extinction

184
00:17:19,800 --> 00:17:25,760
 of five aggregates or extinction of suffering.

185
00:17:25,760 --> 00:17:34,690
 As the extinction of suffering, it is a separate object

186
00:17:34,690 --> 00:17:40,080
 that is taken as object by the path

187
00:17:40,080 --> 00:17:54,040
 consciousness and fruition consciousness.

188
00:17:54,040 --> 00:18:04,130
 When using English, we can say nibbana is not a mental

189
00:18:04,130 --> 00:18:06,040
 state.

190
00:18:06,040 --> 00:18:11,120
 But if we use Pali, it is different.

191
00:18:11,120 --> 00:18:17,520
 So I don't want you to be confused, but I must say this.

192
00:18:17,520 --> 00:18:27,840
 Now the word for mind in Pali is nama, and a-m-a.

193
00:18:27,840 --> 00:18:36,760
 And that nama is defined as something that inclines towards

194
00:18:36,760 --> 00:18:38,920
 the object.

195
00:18:38,920 --> 00:18:41,520
 That is one definition.

196
00:18:41,520 --> 00:18:48,650
 And the other definition is something that makes others to

197
00:18:48,650 --> 00:18:51,440
 incline towards it.

198
00:18:51,440 --> 00:18:56,480
 So the first definition is ordinary definition.

199
00:18:56,480 --> 00:19:02,640
 And the second definition involves causative sense.

200
00:19:02,640 --> 00:19:06,240
 So the first definition is something that inclines towards

201
00:19:06,240 --> 00:19:07,160
 the object.

202
00:19:07,160 --> 00:19:09,480
 That means that takes the object.

203
00:19:09,480 --> 00:19:14,110
 And the second definition is something that makes others

204
00:19:14,110 --> 00:19:16,000
 incline towards it.

205
00:19:16,000 --> 00:19:21,480
 So there are two definitions of the word nama in Pali.

206
00:19:21,480 --> 00:19:30,060
 Now there are consciousness, mental states, material,

207
00:19:30,060 --> 00:19:35,240
 properties of matter and nibbana.

208
00:19:35,240 --> 00:19:38,660
 Now consciousness can take objects.

209
00:19:38,660 --> 00:19:42,280
 That means consciousness inclines towards the object.

210
00:19:42,280 --> 00:19:46,720
 And so consciousness is a nama.

211
00:19:46,720 --> 00:19:51,360
 And mental factors always arise with consciousness.

212
00:19:51,360 --> 00:19:55,920
 And so along with consciousness, they take objects.

213
00:19:55,920 --> 00:20:05,740
 So mental factors are also called nama.

214
00:20:05,740 --> 00:20:09,590
 And consciousness, one consciousness can be the object of

215
00:20:09,590 --> 00:20:11,440
 another consciousness.

216
00:20:11,440 --> 00:20:15,640
 That means consciousness can make another consciousness

217
00:20:15,640 --> 00:20:17,240
 incline towards it.

218
00:20:17,240 --> 00:20:23,760
 So by both definitions, consciousness is a nama.

219
00:20:23,760 --> 00:20:32,160
 By both definitions, the mental factors are nama.

220
00:20:32,160 --> 00:20:35,400
 Now we come to matter.

221
00:20:35,400 --> 00:20:37,520
 We can take matter as object.

222
00:20:37,520 --> 00:20:42,200
 So that means our mind inclines towards the object.

223
00:20:42,200 --> 00:20:51,000
 But matter cannot take an object itself.

224
00:20:51,000 --> 00:20:56,920
 So matter is no nama.

225
00:20:56,920 --> 00:21:04,540
 It does not take an object.

226
00:21:04,540 --> 00:21:10,150
 But nibbana is called nama because it can make the

227
00:21:10,150 --> 00:21:14,180
 consciousness incline towards it.

228
00:21:14,180 --> 00:21:19,460
 That means it can be taken as object by consciousness and

229
00:21:19,460 --> 00:21:21,360
 mental factors.

230
00:21:21,360 --> 00:21:28,880
 So if we use English, we say nibbana is not a mental state.

231
00:21:28,880 --> 00:21:31,820
 So it's plain.

232
00:21:31,820 --> 00:21:38,040
 But if we use Pali, we say nibbana is also a nama.

233
00:21:38,040 --> 00:21:43,570
 But it is not the same nama as the consciousness and mental

234
00:21:43,570 --> 00:21:44,800
 factors.

235
00:21:44,800 --> 00:21:52,750
 So consciousness and mental factors are called nama

236
00:21:52,750 --> 00:21:55,160
 according to both definitions.

237
00:21:55,160 --> 00:21:59,060
 But nibbana is called nama according to the second

238
00:21:59,060 --> 00:22:00,720
 definition only.

239
00:22:00,720 --> 00:22:04,240
 So nibbana is included in nama.

240
00:22:04,240 --> 00:22:11,970
 But since nibbana is included in nama, we cannot say that

241
00:22:11,970 --> 00:22:15,000
 nibbana is mental.

242
00:22:15,000 --> 00:22:19,400
 So we should be careful about this.

243
00:22:19,400 --> 00:22:30,000
 We should better not talk about nibbana as nama at all.

244
00:22:30,000 --> 00:22:36,410
 Now there is a saying in one discourse that we can find all

245
00:22:36,410 --> 00:22:39,280
 four noble truths in this

246
00:22:39,280 --> 00:22:41,480
 fathom long body.

247
00:22:41,480 --> 00:22:45,780
 That means we can find all four noble truths in this body.

248
00:22:45,780 --> 00:22:50,320
 And nibbana is the third of the noble truth.

249
00:22:50,320 --> 00:22:57,180
 But actually we are not to understand that nibbana is in us

250
00:22:57,180 --> 00:22:57,520
.

251
00:22:57,520 --> 00:23:00,760
 Nibbana is in our mind or in our body.

252
00:23:00,760 --> 00:23:04,200
 But it is said that nibbana can be found in this body

253
00:23:04,200 --> 00:23:06,440
 because nibbana can be taken as

254
00:23:06,440 --> 00:23:13,220
 object by our mind when we get enlightenment.

255
00:23:13,220 --> 00:23:18,380
 So since it can be taken as object by the mind which is

256
00:23:18,380 --> 00:23:21,960
 internal to us, Buddha said

257
00:23:21,960 --> 00:23:28,920
 that all these four noble truths can be found in this body.

258
00:23:28,920 --> 00:23:35,960
 So pointing out to that discourse, we are not to say that

259
00:23:35,960 --> 00:23:38,520
 nibbana can be in us.

260
00:23:38,520 --> 00:23:42,360
 Nibbana is internal because abitama is very exact and

261
00:23:42,360 --> 00:23:45,200
 strict and in abitama it is classified

262
00:23:45,200 --> 00:23:50,960
 as an external object.

263
00:23:50,960 --> 00:23:59,540
 Although nibbana is mostly defined in negative terms, it is

264
00:23:59,540 --> 00:24:03,000
 not a negative state.

265
00:24:03,000 --> 00:24:06,340
 It is a positive state.

266
00:24:06,340 --> 00:24:11,440
 Because it is a positive state, it can be taken as object

267
00:24:11,440 --> 00:24:13,920
 by path consciousness and

268
00:24:13,920 --> 00:24:22,520
 fruition consciousness.

269
00:24:22,520 --> 00:24:26,120
 Now what is health?

270
00:24:26,120 --> 00:24:30,080
 No disease.

271
00:24:30,080 --> 00:24:34,360
 So absence of disease is what we call health.

272
00:24:34,360 --> 00:24:37,560
 But it is not just the absence of disease, it is something

273
00:24:37,560 --> 00:24:38,400
 positive.

274
00:24:38,400 --> 00:24:43,570
 So we cannot describe what health is, but we know and we

275
00:24:43,570 --> 00:24:45,820
 experience health.

276
00:24:45,820 --> 00:24:49,360
 So when we are healthy, we know that we have health and

277
00:24:49,360 --> 00:24:51,500
 when we have diseases, we know

278
00:24:51,500 --> 00:24:53,980
 that we have no health.

279
00:24:53,980 --> 00:24:58,550
 So health is a positive state, but if we are asked to

280
00:24:58,550 --> 00:25:01,600
 explain what health is, we may just

281
00:25:01,600 --> 00:25:05,560
 say it is absence of diseases.

282
00:25:05,560 --> 00:25:08,440
 So in the same way nibbana is a positive state and then we

283
00:25:08,440 --> 00:25:10,240
 say nibbana is the extinction of

284
00:25:10,240 --> 00:25:21,360
 suffering and so on.

285
00:25:21,360 --> 00:25:28,160
 Now the other question is about chitana or volition.

286
00:25:28,160 --> 00:25:33,720
 Now chitana or volition is also a mental state.

287
00:25:33,720 --> 00:25:41,360
 It is a mental state that organizes the other mental concom

288
00:25:41,360 --> 00:25:45,200
itants or that pushes or that

289
00:25:45,200 --> 00:25:52,600
 urges the other mental concomitants to do their respective

290
00:25:52,600 --> 00:25:54,480
 functions.

291
00:25:54,480 --> 00:26:00,580
 And this chitana, although it is a mental state and it

292
00:26:00,580 --> 00:26:06,400
 disappears immediately after it arises,

293
00:26:06,400 --> 00:26:11,860
 it has the potential to give results in the future.

294
00:26:11,860 --> 00:26:19,630
 So when it disappears, it leaves some kind of potential to

295
00:26:19,630 --> 00:26:23,680
 give results in the continuity

296
00:26:23,680 --> 00:26:26,160
 of a being.

297
00:26:26,160 --> 00:26:32,280
 We do not know where this potential is stored, but when the

298
00:26:32,280 --> 00:26:35,480
 conditions are met with, when

299
00:26:35,480 --> 00:26:41,750
 the conditions are favorable, then the results are produced

300
00:26:41,750 --> 00:26:41,760
.

301
00:26:41,760 --> 00:26:51,360
 So that chitana that translated in English as volition is

302
00:26:51,360 --> 00:26:56,680
 the one that produces results.

303
00:26:56,680 --> 00:27:01,280
 And that chitana is what we call kama.

304
00:27:01,280 --> 00:27:13,060
 So the word kama etymologically means a work, doing or a

305
00:27:13,060 --> 00:27:14,900
 deed.

306
00:27:14,900 --> 00:27:24,040
 But actually technically kama is this chitana.

307
00:27:24,040 --> 00:27:31,440
 But whenever you do something, this chitana always arises.

308
00:27:31,440 --> 00:27:39,920
 So this chitana is always connected with deeds or works.

309
00:27:39,920 --> 00:27:46,880
 And so the deeds come to be called kama.

310
00:27:46,880 --> 00:27:53,340
 So nowadays if you ask what is kama, you may say kama is a

311
00:27:53,340 --> 00:27:55,720
 good or bad deed.

312
00:27:55,720 --> 00:28:00,540
 But technically kama is not a good or bad deed, but the ch

313
00:28:00,540 --> 00:28:03,200
itana or volition that arises

314
00:28:03,200 --> 00:28:08,880
 in the mind when one does a good or bad deed.

315
00:28:08,880 --> 00:28:15,980
 And that chitana has, as I said, the potential to give

316
00:28:15,980 --> 00:28:19,360
 results in the future.

317
00:28:19,360 --> 00:28:26,190
 It is taught in Abhidhamma that chitana accompanies every

318
00:28:26,190 --> 00:28:29,320
 type of consciousness.

319
00:28:29,320 --> 00:28:36,500
 But not chitana concomitant with every type of

320
00:28:36,500 --> 00:28:41,760
 consciousness is called kama.

321
00:28:41,760 --> 00:28:47,450
 Chitana that accompanies only the wholesome type of

322
00:28:47,450 --> 00:28:51,520
 consciousness and unwholesome type

323
00:28:51,520 --> 00:28:56,000
 of consciousness is called kama.

324
00:28:56,000 --> 00:29:01,920
 Chitana can arise with the resultant consciousness.

325
00:29:01,920 --> 00:29:05,980
 Chitana can arise with what are called functional

326
00:29:05,980 --> 00:29:07,600
 consciousness.

327
00:29:07,600 --> 00:29:12,180
 So when chitana arises with resultant consciousness, when

328
00:29:12,180 --> 00:29:15,000
 it arises with functional consciousness,

329
00:29:15,000 --> 00:29:21,840
 it is not called kama because it has no power to produce

330
00:29:21,840 --> 00:29:23,400
 results.

331
00:29:23,400 --> 00:29:29,440
 Only when it accompanies the wholesome consciousness or un

332
00:29:29,440 --> 00:29:33,200
wholesome consciousness is it called

333
00:29:33,200 --> 00:29:35,320
 kama.

334
00:29:35,320 --> 00:29:52,440
 Only then can it produce results.

335
00:29:52,440 --> 00:29:59,480
 And the results that arise are produced by chitana, the

336
00:29:59,480 --> 00:30:02,720
 mental state, and not by the

337
00:30:02,720 --> 00:30:10,120
 things, say, in any act of giving.

338
00:30:10,120 --> 00:30:14,840
 And it is not the things that are offered that produce

339
00:30:14,840 --> 00:30:16,000
 results.

340
00:30:16,000 --> 00:30:23,330
 For example, you offer a set of rope to the sangha and

341
00:30:23,330 --> 00:30:28,120
 according to the law of kama, you

342
00:30:28,120 --> 00:30:33,980
 acquire kusala, wholesome kama when you offer a set of

343
00:30:33,980 --> 00:30:36,520
 ropes to the sangha.

344
00:30:36,520 --> 00:30:43,880
 And the chitana accompanying your consciousness is kama.

345
00:30:43,880 --> 00:30:48,030
 And it is this chitana that produces results in the future

346
00:30:48,030 --> 00:30:50,360
 and not the things you offer.

347
00:30:50,360 --> 00:30:55,160
 A rope, a set of ropes cannot produce anything.

348
00:30:55,160 --> 00:30:58,080
 It is the product.

349
00:30:58,080 --> 00:31:03,750
 But what produces results is not the thing that you offer

350
00:31:03,750 --> 00:31:07,720
 but the mental state that accompanies

351
00:31:07,720 --> 00:31:19,000
 your consciousness when you make that offering.

352
00:31:19,000 --> 00:31:26,310
 So chitana is described as the mental state that wills or

353
00:31:26,310 --> 00:31:29,920
 that encourages other mental

354
00:31:29,920 --> 00:31:33,840
 states to do their respective functions.

355
00:31:33,840 --> 00:31:40,730
 Now people want to know if chitana encourages others who

356
00:31:40,730 --> 00:31:43,760
 encourages chitana.

357
00:31:43,760 --> 00:31:49,920
 Now chitana is compared to a chief pupil in the class.

358
00:31:49,920 --> 00:31:57,530
 The chief pupil does his own work and at the same time he

359
00:31:57,530 --> 00:32:01,520
 makes the other students work.

360
00:32:01,520 --> 00:32:25,140
 So chitana encourages itself and it encourages other mental

361
00:32:25,140 --> 00:32:27,800
 states.

362
00:32:27,800 --> 00:32:43,960
 And another question is how consciousness arises in the

363
00:32:43,960 --> 00:32:47,800
 next life.

364
00:32:47,800 --> 00:32:55,960
 Now you all know that kama produces results.

365
00:32:55,960 --> 00:33:02,800
 At the beginning of one life, especially of, let us say

366
00:33:02,800 --> 00:33:06,800
 human beings, at the beginning

367
00:33:06,800 --> 00:33:13,870
 of life as a human being, there is produced a type of

368
00:33:13,870 --> 00:33:18,400
 consciousness or there arises a

369
00:33:18,400 --> 00:33:28,140
 type of consciousness which is produced by the kama in the

370
00:33:28,140 --> 00:33:29,640
 past.

371
00:33:29,640 --> 00:33:38,510
 According to the teachings of Abhidhamma, consciousness

372
00:33:38,510 --> 00:33:43,000
 needs a physical base to arise,

373
00:33:43,000 --> 00:33:51,480
 especially for human beings and other beings like animals.

374
00:33:51,480 --> 00:33:59,060
 So if there is no physical base, consciousness cannot arise

375
00:33:59,060 --> 00:34:01,520
 in these beings.

376
00:34:01,520 --> 00:34:06,470
 So at the moment of, at the first moment in the life of a

377
00:34:06,470 --> 00:34:09,360
 human being, as I said, there

378
00:34:09,360 --> 00:34:13,400
 is produced a type of consciousness.

379
00:34:13,400 --> 00:34:18,590
 But along with that type of consciousness, some material

380
00:34:18,590 --> 00:34:21,660
 properties are also produced.

381
00:34:21,660 --> 00:34:27,580
 So the arising of some material properties and the arising

382
00:34:27,580 --> 00:34:31,160
 of that particular consciousness

383
00:34:31,160 --> 00:34:36,080
 is what we call rebirth.

384
00:34:36,080 --> 00:34:43,720
 So rebirth consists of a type of consciousness and some

385
00:34:43,720 --> 00:34:47,600
 material properties, both results

386
00:34:47,600 --> 00:34:51,680
 of kama in the past.

387
00:34:51,680 --> 00:34:57,790
 Since there are material properties in the first arising of

388
00:34:57,790 --> 00:35:01,680
 matter in a new life, consciousness

389
00:35:01,680 --> 00:35:03,760
 can arise.

390
00:35:03,760 --> 00:35:20,300
 So the consciousness in the previous life ceases with death

391
00:35:20,300 --> 00:35:20,720
.

392
00:35:20,720 --> 00:35:28,240
 So no consciousness or no material properties move into the

393
00:35:28,240 --> 00:35:29,800
 new life.

394
00:35:29,800 --> 00:35:35,680
 The new life is a new life and the old life ends with death

395
00:35:35,680 --> 00:35:35,840
.

396
00:35:35,840 --> 00:35:41,840
 And then there is a new life arising of the consciousness

397
00:35:41,840 --> 00:35:45,000
 and material properties as a

398
00:35:45,000 --> 00:35:50,160
 result of kama that was presented in the past.

399
00:35:50,160 --> 00:35:58,540
 So we are not to say how can consciousness arise, why after

400
00:35:58,540 --> 00:36:03,460
 death the senses have decayed.

401
00:36:03,460 --> 00:36:08,330
 So the consciousness that arises in the new life depends on

402
00:36:08,330 --> 00:36:10,560
 a new material base in that

403
00:36:10,560 --> 00:36:12,000
 new life.

404
00:36:12,000 --> 00:36:27,560
 And it has nothing to do with the sense organs in the first

405
00:36:27,560 --> 00:36:29,720
 life.

406
00:36:29,720 --> 00:36:36,280
 Now a new life is a life that is newly produced.

407
00:36:36,280 --> 00:36:44,910
 So let us call it a new life, second life and then the

408
00:36:44,910 --> 00:36:46,680
 first life.

409
00:36:46,680 --> 00:36:52,640
 So although the second life is a new life, it is not

410
00:36:52,640 --> 00:36:56,680
 totally disconnected with the first

411
00:36:56,680 --> 00:36:59,320
 life.

412
00:36:59,320 --> 00:37:04,140
 It is the result of the kama in the past life.

413
00:37:04,140 --> 00:37:08,240
 So as cause and effect they are related.

414
00:37:08,240 --> 00:37:10,700
 They have some kind of relationship.

415
00:37:10,700 --> 00:37:18,020
 But nothing from the old life or nothing from the first

416
00:37:18,020 --> 00:37:22,780
 life moves into the second life.

417
00:37:22,780 --> 00:37:27,760
 Although nothing goes from one life to another, there is a

418
00:37:27,760 --> 00:37:30,480
 kind of relation or connection

419
00:37:30,480 --> 00:37:35,170
 between the first life and the second life that is as cause

420
00:37:35,170 --> 00:37:36,400
 and effect.

421
00:37:36,400 --> 00:37:43,960
 So that is why the kama of this person gives results to

422
00:37:43,960 --> 00:37:48,560
 this continuity only and not to

423
00:37:48,560 --> 00:37:50,960
 another continuity.

424
00:37:50,960 --> 00:37:57,640
 Otherwise there will be chaos in the law of kama.

425
00:37:57,640 --> 00:38:01,590
 My kama giving results to you or your kama giving results

426
00:38:01,590 --> 00:38:02,240
 to me.

427
00:38:02,240 --> 00:38:14,840
 That cannot happen.

428
00:38:14,840 --> 00:38:23,350
 And one thing important to note about death and rebirth is

429
00:38:23,350 --> 00:38:27,880
 that rebirth is not the result

430
00:38:27,880 --> 00:38:31,440
 of death.

431
00:38:31,440 --> 00:38:35,200
 That is important.

432
00:38:35,200 --> 00:38:44,730
 Many people wrote that death brings about rebirth or

433
00:38:44,730 --> 00:38:50,240
 sometimes they may say that as

434
00:38:50,240 --> 00:38:56,160
 a result of former life there is a new life.

435
00:38:56,160 --> 00:39:03,080
 Although rebirth immediately follows death, rebirth is not

436
00:39:03,080 --> 00:39:05,680
 the result of death.

437
00:39:05,680 --> 00:39:11,240
 As you know rebirth is the result of kama.

438
00:39:11,240 --> 00:39:14,880
 Kama in the past, not the result of death.

439
00:39:14,880 --> 00:39:21,000
 But immediately after death there is rebirth.

440
00:39:21,000 --> 00:39:30,320
 So we are never to say that rebirth is the result of death.

441
00:39:30,320 --> 00:39:37,220
 But we can say that rebirth is conditioned by death in the

442
00:39:37,220 --> 00:39:40,720
 sense that death disappears

443
00:39:40,720 --> 00:39:46,340
 so that rebirth can arise.

444
00:39:46,340 --> 00:39:50,520
 It is something like giving a place to another person.

445
00:39:50,520 --> 00:39:55,570
 So I am sitting in this place and if I vacate this place

446
00:39:55,570 --> 00:39:58,520
 then another person can sit in

447
00:39:58,520 --> 00:40:00,160
 its place.

448
00:40:00,160 --> 00:40:03,820
 So I am the condition for another person.

449
00:40:03,820 --> 00:40:10,360
 So in that sense death is a condition for rebirth.

450
00:40:10,360 --> 00:40:18,400
 But that does not mean that rebirth is caused by death.

451
00:40:18,400 --> 00:40:21,240
 For rebirth is the product of death.

452
00:40:21,240 --> 00:40:25,600
 So there are two different things.

453
00:40:25,600 --> 00:40:31,450
 Death belongs to the old life and rebirth belongs to the

454
00:40:31,450 --> 00:40:32,820
 new life.

455
00:40:32,820 --> 00:40:43,200
 So we must never say that rebirth is the result of death.

456
00:40:43,200 --> 00:40:48,240
 Death brings about rebirth.

457
00:40:48,240 --> 00:40:54,280
 It just vacates its place so that rebirth can take place.

458
00:40:54,280 --> 00:41:00,550
 Rebirth means rebirth consciousness and material properties

459
00:41:00,550 --> 00:41:02,240
 along with it.

460
00:41:02,240 --> 00:41:09,380
 If you want to know more about the process of death and

461
00:41:09,380 --> 00:41:13,560
 rebirth you study abitama.

462
00:41:13,560 --> 00:41:18,760
 So only in abitama the process of death and rebirth are

463
00:41:18,760 --> 00:41:21,160
 explained in detail.

464
00:41:21,160 --> 00:41:24,690
 And only when you have the knowledge of abitama do you

465
00:41:24,690 --> 00:41:26,520
 understand correctly.

466
00:41:26,520 --> 00:41:31,160
 Otherwise you may misunderstand or you may make mistakes.

467
00:41:31,160 --> 00:41:40,490
 So if you want to know more about the process, please study

468
00:41:40,490 --> 00:41:42,480
 abitama.

469
00:41:42,480 --> 00:41:46,520
 Okay I thought I would be brief.

470
00:41:46,520 --> 00:41:50,560
 That is why I told you about the cartoon.

471
00:41:50,560 --> 00:41:52,880
 But now it is not brief.

472
00:41:52,880 --> 00:41:54,200
 It is almost time.

473
00:41:54,200 --> 00:41:56,840
 I mean regular time.

474
00:41:56,840 --> 00:42:03,680
 Now one more not story but something about the elephants.

475
00:42:03,680 --> 00:42:09,200
 In our countries they use elephants to haul logs right?

476
00:42:09,200 --> 00:42:13,660
 So elephants are trained to push the logs and also they are

477
00:42:13,660 --> 00:42:15,360
 trained to stop when they

478
00:42:15,360 --> 00:42:20,560
 hear the siren, when they hear the bell.

479
00:42:20,560 --> 00:42:24,320
 So it is said that elephants would stop as soon as they

480
00:42:24,320 --> 00:42:25,600
 hear the bell.

481
00:42:25,600 --> 00:42:30,160
 Even if there are about only six inches of the log to push

482
00:42:30,160 --> 00:42:32,040
 they will not push it.

483
00:42:32,040 --> 00:42:39,180
 They will stop dead at the sound of the siren or the bell.

484
00:42:39,180 --> 00:42:49,400
 So today I will be like an elephant.

485
00:42:49,400 --> 00:42:50,560
 Say sorry sorry sorry.

486
00:42:50,560 --> 00:42:55,500
 Sorry sorry sorry.

487
00:42:55,500 --> 00:43:00,610
 Sorry sorry sorry sorry sorry sorry sorry sorry sorry sorry

488
00:43:00,610 --> 00:43:11,560
 sorry sorry sorry sorry

