1
00:00:00,000 --> 00:00:07,000
 Hello there YouTubers. I just wanted to make a short video

2
00:00:07,000 --> 00:00:07,000
 today because I recently got

3
00:00:07,000 --> 00:00:13,630
 a request from someone to provide them with the script for

4
00:00:13,630 --> 00:00:16,640
 my how to meditate videos.

5
00:00:16,640 --> 00:00:23,790
 And of course the videos weren't scripted, they were sort

6
00:00:23,790 --> 00:00:26,640
 of on the fly as you will,

7
00:00:27,480 --> 00:00:32,680
 if you will. And so I thought I'd put this out there as

8
00:00:32,680 --> 00:00:34,480
 sort of an experiment, experimental

9
00:00:34,480 --> 00:00:39,650
 project in case anyone was ambitious enough to take it on,

10
00:00:39,650 --> 00:00:42,040
 to provide me with a script

11
00:00:42,040 --> 00:00:49,790
 for at least one of my meditation videos. If there's anyone

12
00:00:49,790 --> 00:00:51,040
 out there who has appreciated

13
00:00:51,040 --> 00:00:55,230
 the videos and thinks they're of some benefit to the world

14
00:00:55,230 --> 00:00:57,640
 and could be of greater benefit

15
00:00:57,640 --> 00:01:02,200
 to the world by being translated, if we could provide an

16
00:01:02,200 --> 00:01:04,640
 English script for all of the videos

17
00:01:04,640 --> 00:01:09,670
 to anyone, if I could provide a link to the script for all

18
00:01:09,670 --> 00:01:12,600
 of the videos, then certainly

19
00:01:12,600 --> 00:01:20,110
 they could reach a much wider audience. So if there's

20
00:01:20,110 --> 00:01:20,840
 anyone out there who would like

21
00:01:20,840 --> 00:01:24,370
 to do that for one or even several of the videos, I'd

22
00:01:24,370 --> 00:01:26,640
 suggest that if there's anyone

23
00:01:26,640 --> 00:01:31,080
 who would like to they can take on one of the videos and

24
00:01:31,080 --> 00:01:33,640
 provide me with a transcript

25
00:01:33,640 --> 00:01:40,640
 of what's being said more or less exactly as it was said.

26
00:01:40,640 --> 00:01:40,640
 Then I'd really appreciate

27
00:01:40,640 --> 00:01:48,530
 it and it could be of great benefit to quite a few people.

28
00:01:48,530 --> 00:01:50,120
 So please let me know if you

29
00:01:50,120 --> 00:01:52,790
 have any questions or comments. Probably the best way to

30
00:01:52,790 --> 00:01:54,640
 get in touch with me is still

31
00:01:54,640 --> 00:01:57,670
 via email, though I don't check it that often, or via the

32
00:01:57,670 --> 00:01:59,240
 website. So I'll put both of those

33
00:01:59,240 --> 00:02:03,640
 up here. You should see it on the screen. My email is utat

34
00:02:03,640 --> 00:02:06,240
amo@gmail.com and the contact

35
00:02:06,240 --> 00:02:16,040
 form on the website is www.sirimongolo.org/contact. So

36
00:02:16,040 --> 00:02:16,040
 again, thanks in advance if there's anyone

37
00:02:19,180 --> 00:02:22,420
 out there who would like to take this on and I hope to hear

38
00:02:22,420 --> 00:02:24,180
 from you soon. Have a good

