1
00:00:00,000 --> 00:00:03,980
 So tell me a little bit about what made you want to come

2
00:00:03,980 --> 00:00:05,000
 and meditate with this.

3
00:00:05,000 --> 00:00:13,520
 To meditate, first of all I had a lot of fears that I'm

4
00:00:13,520 --> 00:00:17,000
 afraid of a lot of things like darkness.

5
00:00:17,000 --> 00:00:23,670
 Very often I'm afraid to stay alone in darkness or stay

6
00:00:23,670 --> 00:00:30,370
 alone at home or to be around a lot of people who I don't

7
00:00:30,370 --> 00:00:31,000
 know.

8
00:00:31,000 --> 00:00:39,270
 Also I have a lot of attachments, attachments to my family,

9
00:00:39,270 --> 00:00:41,000
 to close friends.

10
00:00:41,000 --> 00:00:44,000
 I used to have actually.

11
00:00:44,000 --> 00:00:47,440
 How did that affect your practice? Did you find the fears

12
00:00:47,440 --> 00:00:52,090
 coming up stronger in practice? Were they interfering with

13
00:00:52,090 --> 00:00:53,000
 it?

14
00:00:53,000 --> 00:00:56,000
 How was it practicing with the fear?

15
00:00:56,000 --> 00:00:59,580
 Practicing with the fear, in the beginning it was very hard

16
00:00:59,580 --> 00:01:03,000
 because everything, what is inside is coming out.

17
00:01:03,000 --> 00:01:10,350
 And you have to acknowledge it, you have to notice it

18
00:01:10,350 --> 00:01:20,760
 actually is a dessert. But once you see it, it's getting

19
00:01:20,760 --> 00:01:27,000
 better every day.

20
00:01:27,000 --> 00:01:30,890
 And all the fear is just going out and it's not bothering

21
00:01:30,890 --> 00:01:32,000
 me anymore.

22
00:01:32,000 --> 00:01:36,600
 How was it at the end? You found that you really had to

23
00:01:36,600 --> 00:01:40,000
 face really strong fear near the end?

24
00:01:40,000 --> 00:01:44,000
 Actually one night was very hard.

25
00:01:44,000 --> 00:01:48,460
 Well, when I was supposed to stay during the night and med

26
00:01:48,460 --> 00:01:53,870
itate, it was very hard because everything was what was bad

27
00:01:53,870 --> 00:01:55,000
 inside.

28
00:01:55,000 --> 00:01:59,600
 It came outside in a short period of time, in a couple of

29
00:01:59,600 --> 00:02:04,000
 hours and it was really difficult to fight with them.

30
00:02:04,000 --> 00:02:07,360
 But it is possible, everything is possible if you really

31
00:02:07,360 --> 00:02:08,000
 want it.

32
00:02:08,000 --> 00:02:10,000
 Is that fear still there?

33
00:02:10,000 --> 00:02:17,680
 I would say it is slightly present sometimes, but when I

34
00:02:17,680 --> 00:02:20,000
 acknowledge it, I'm telling myself,

35
00:02:20,000 --> 00:02:24,320
 I'm trying to meditate on it right away and it's just going

36
00:02:24,320 --> 00:02:25,000
 away.

37
00:02:25,000 --> 00:02:28,000
 So you found a tool that allows you to deal with it?

38
00:02:28,000 --> 00:02:31,000
 Yes, exactly.

39
00:02:31,000 --> 00:02:34,000
 How do you feel now?

40
00:02:34,000 --> 00:02:40,480
 I feel much better, I feel very calm, very relaxed, very

41
00:02:40,480 --> 00:02:42,000
 peaceful, I would say.

42
00:02:42,000 --> 00:02:46,450
 I'm glad to be around the people, glad to see them, to talk

43
00:02:46,450 --> 00:02:50,000
 to them, it doesn't matter what I'm about.

44
00:02:50,000 --> 00:02:56,000
 I feel happy. This course made me happy.

45
00:02:56,000 --> 00:03:00,470
 What are the main benefits to practicing meditation? What

46
00:03:00,470 --> 00:03:04,000
 do you think meditation does for you?

47
00:03:04,000 --> 00:03:13,240
 It helps me to see actually what is inside me, good and bad

48
00:03:13,240 --> 00:03:14,000
 things,

49
00:03:14,000 --> 00:03:20,000
 and also accept those good and bad things.

50
00:03:20,000 --> 00:03:30,070
 And also I learned during the course, I learned tools which

51
00:03:30,070 --> 00:03:36,000
 helped me to deal with those things.

52
00:03:36,000 --> 00:03:42,340
 What would you say is the main difference for you between

53
00:03:42,340 --> 00:03:46,000
 now and before you started practicing?

54
00:03:46,000 --> 00:03:50,950
 Before I started practicing, the first day when I came to

55
00:03:50,950 --> 00:03:54,000
 the meditation center was really hard,

56
00:03:54,000 --> 00:03:58,760
 lots of thoughts were running around in my head, I had a

57
00:03:58,760 --> 00:04:04,000
 headache and I couldn't focus on anything.

58
00:04:04,000 --> 00:04:08,000
 It caused me a pain, everything was causing me a pain.

59
00:04:08,000 --> 00:04:12,560
 I remember the first day when I came here, I just basically

60
00:04:12,560 --> 00:04:18,000
 go downstairs in the basement and spent the first two days

61
00:04:18,000 --> 00:04:18,000
 in the basement.

62
00:04:18,000 --> 00:04:22,120
 I didn't want to come out, but slowly, slowly it got better

63
00:04:22,120 --> 00:04:23,000
 and better.

64
00:04:23,000 --> 00:04:28,900
 After three days, I would say, or four days, I noticed that

65
00:04:28,900 --> 00:04:31,000
 I felt more comfortable outside

66
00:04:31,000 --> 00:04:36,420
 and I started to practice outside on the backyard and didn

67
00:04:36,420 --> 00:04:39,000
't want to go down again.

68
00:04:39,000 --> 00:04:42,000
 And now it's changed?

69
00:04:42,000 --> 00:04:47,770
 Now it's changed that this feeling of peacefulness and

70
00:04:47,770 --> 00:04:51,000
 happiness is present.

71
00:04:51,000 --> 00:05:06,610
 Also, I noticed that I didn't need so much food, sleep, and

72
00:05:06,610 --> 00:05:09,000
 coffee, tea, and something,

73
00:05:09,000 --> 00:05:12,000
 but I saw that this was very important in my life.

74
00:05:12,000 --> 00:05:15,880
 I really don't need it, we can sleep a couple of hours per

75
00:05:15,880 --> 00:05:20,000
 night and feel pretty good, feel perfect basically.

76
00:05:20,000 --> 00:05:24,980
 Everything we have is our addiction, our idea that we need

77
00:05:24,980 --> 00:05:28,000
 it, but we don't really need it.

78
00:05:28,000 --> 00:05:33,000
 What would you say to people who want to come to practice?

79
00:05:33,000 --> 00:05:38,450
 I would say that this is the best and most important thing

80
00:05:38,450 --> 00:05:40,000
 that I have done.

81
00:05:40,000 --> 00:05:44,950
 I'm really glad that I did it and I would like to continue

82
00:05:44,950 --> 00:05:45,000
 it.

83
00:05:45,000 --> 00:05:50,990
 I really recommend to other people to take a course because

84
00:05:50,990 --> 00:05:54,000
 it's pretty hard to do it at home.

85
00:05:54,000 --> 00:05:57,070
 I could not imagine that I would have done something like

86
00:05:57,070 --> 00:05:58,000
 this at home.

87
00:05:58,000 --> 00:06:02,030
 You really need some support, you really need some teachers

88
00:06:02,030 --> 00:06:08,000
 who are going to help you to do such things like meditation

89
00:06:08,000 --> 00:06:08,000
,

90
00:06:08,000 --> 00:06:14,690
 correct your movements in a necessary situation when it's

91
00:06:14,690 --> 00:06:16,000
 needed.

92
00:06:16,000 --> 00:06:19,950
 Any advice for someone thinking to do it for preparing for

93
00:06:19,950 --> 00:06:21,000
 the course?

94
00:06:21,000 --> 00:06:26,000
 Preparing for the course.

95
00:06:26,000 --> 00:06:30,000
 Maybe some kind of expectation they might have to help you

96
00:06:30,000 --> 00:06:34,650
 want to do something they should know before going into the

97
00:06:34,650 --> 00:06:36,000
 course?

98
00:06:36,000 --> 00:06:42,320
 I think it would be easier if some legal practice might be

99
00:06:42,320 --> 00:06:44,000
 at home before.

100
00:06:44,000 --> 00:06:49,020
 Everything is individual, it all depends on the person,

101
00:06:49,020 --> 00:06:55,000
 from what he or she has inside.

102
00:06:55,000 --> 00:07:00,000
 But the harder you work, the more benefits you get.

103
00:07:00,000 --> 00:07:03,000
 This is how it is.

104
00:07:03,000 --> 00:07:09,190
 I don't think that I would organize myself at home to

105
00:07:09,190 --> 00:07:14,000
 practice the whole day and sleep six hours.

106
00:07:14,000 --> 00:07:16,000
 It wouldn't be possible.

107
00:07:16,000 --> 00:07:21,000
 I don't think so, but meditation center is very easy

108
00:07:21,000 --> 00:07:23,000
 because you are not distracted by other things.

109
00:07:23,000 --> 00:07:28,920
 You are just coming here, you are receiving food, you sleep

110
00:07:28,920 --> 00:07:33,000
 for six hours and this is enough.

111
00:07:33,000 --> 00:07:37,610
 All the time you have the time to basically practice and

112
00:07:37,610 --> 00:07:41,000
 you are not bothered by other people.

113
00:07:41,000 --> 00:07:42,000
 Thank you.

114
00:07:42,000 --> 00:07:44,000
 You are welcome.

