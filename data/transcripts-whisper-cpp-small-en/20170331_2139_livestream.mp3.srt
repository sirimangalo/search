1
00:00:00,000 --> 00:00:07,000
 To teach in a way that their students can apprehend.

2
00:00:07,000 --> 00:00:19,340
 I mean, I guess I would even go so far as to say, if you

3
00:00:19,340 --> 00:00:21,000
 want to be loved by your students,

4
00:00:21,000 --> 00:00:26,000
 make sure you're cultivating mindfulness, you know,

5
00:00:26,000 --> 00:00:28,780
 because you want to be present and you don't want to come

6
00:00:28,780 --> 00:00:30,000
 across as egotistical,

7
00:00:30,000 --> 00:00:32,740
 you don't want to have an agenda, you don't want to be

8
00:00:32,740 --> 00:00:33,000
 cruel,

9
00:00:33,000 --> 00:00:41,380
 and, you know, giving overly critical and overly harsh

10
00:00:41,380 --> 00:00:44,000
 grades is not helping anybody.

11
00:00:44,000 --> 00:00:48,280
 I had one professor, it seemed like she just wanted to

12
00:00:48,280 --> 00:00:49,000
 teach me a lesson,

13
00:00:49,000 --> 00:00:52,000
 because I was Buddhist, I think it was a religious study,

14
00:00:52,000 --> 00:00:56,820
 it was really a ridiculous situation, but I felt that she

15
00:00:56,820 --> 00:00:58,000
 was actively,

16
00:00:58,000 --> 00:01:04,690
 anyway, she was very critical and many years ago, vind

17
00:01:04,690 --> 00:01:08,000
ictive it felt like.

18
00:01:17,000 --> 00:01:22,000
 But students do respond to the compassion and to kindness

19
00:01:22,000 --> 00:01:22,000
 and to

20
00:01:22,000 --> 00:01:27,000
 a professor who seems to be working for the students

21
00:01:27,000 --> 00:01:31,290
 in the sense of really understanding that they're there to

22
00:01:31,290 --> 00:01:33,000
 help the students learn.

23
00:01:33,000 --> 00:01:40,270
 Not just to pad their egos, boost their egos, not just to

24
00:01:40,270 --> 00:01:42,000
 make money.

25
00:01:45,000 --> 00:01:51,820
 Again, Saranīya Dhamma, if you want harmony, you've got to

26
00:01:51,820 --> 00:01:53,000
 work at it,

27
00:01:53,000 --> 00:01:56,000
 and cultivate it in your students as well.

28
00:01:56,000 --> 00:01:59,250
 Anyone who teaches anything is always in a position to

29
00:01:59,250 --> 00:02:03,000
 impart values,

30
00:02:03,000 --> 00:02:09,000
 impart their own personality to their students.

31
00:02:11,000 --> 00:02:15,000
 So that's a heavy burden.

32
00:02:15,000 --> 00:02:20,000
 Yeah, well I don't think you really have to respect

33
00:02:20,000 --> 00:02:27,000
 teachers who are unkind or cruel.

34
00:02:27,000 --> 00:02:32,790
 Not any more than you respect everyone, I mean, we should

35
00:02:32,790 --> 00:02:36,000
 respect everyone to some extent.

36
00:02:36,000 --> 00:02:41,310
 But there's a difference between respecting everyone and

37
00:02:41,310 --> 00:02:43,000
 esteeming them.

38
00:02:43,000 --> 00:02:48,000
 I wouldn't esteem someone who is cruel.

39
00:02:48,000 --> 00:02:51,550
 I had an argument with this recently, I mean, I'm sort of

40
00:02:51,550 --> 00:02:53,000
 of the school that if they're my teacher,

41
00:02:53,000 --> 00:03:00,000
 I'm going to respect the position and respect them as a

42
00:03:00,000 --> 00:03:00,000
 teacher

43
00:03:00,000 --> 00:03:04,990
 in the sense of giving them the honor that they deserve,

44
00:03:04,990 --> 00:03:07,000
 you know, the honor that comes.

45
00:03:07,000 --> 00:03:14,080
 So I will hold them up in a technical position of

46
00:03:14,080 --> 00:03:16,000
 superiority.

47
00:03:16,000 --> 00:03:22,770
 I won't interrupt their class and I won't disrupt their

48
00:03:22,770 --> 00:03:24,000
 class.

49
00:03:29,000 --> 00:03:32,000
 I think that's important and useful.

