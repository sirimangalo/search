1
00:00:00,000 --> 00:00:04,980
 difficulty in socializing. Question. What is the purpose

2
00:00:04,980 --> 00:00:07,760
 for seeking solitude for meditation?

3
00:00:07,760 --> 00:00:13,220
 How does one seek solitude? Answer. True solitude is the

4
00:00:13,220 --> 00:00:14,720
 state of mind. Solitude

5
00:00:14,720 --> 00:00:19,250
 is something you can find anywhere. Solitude of mind is

6
00:00:19,250 --> 00:00:21,200
 called chitta-wiweka.

7
00:00:21,200 --> 00:00:25,960
 Taya-wiweka means occlusion of the body, which is of course

8
00:00:25,960 --> 00:00:28,640
 useful and makes finding tranquility

9
00:00:28,640 --> 00:00:33,280
 easier, but if you are truly mindful, it can be in solitude

10
00:00:33,280 --> 00:00:36,240
 anywhere. Solitude means being

11
00:00:36,240 --> 00:00:41,740
 alone, solo or singular. We commonly say that a person is

12
00:00:41,740 --> 00:00:43,760
 alone when there is no other being

13
00:00:43,760 --> 00:00:49,060
 nearby. But another meaning of alone is alone in the moment

14
00:00:49,060 --> 00:00:51,440
 in terms of time and space, not

15
00:00:51,440 --> 00:00:55,110
 thinking about the past or future or some place other than

16
00:00:55,110 --> 00:00:58,240
 where you are. If we get caught up in

17
00:00:58,240 --> 00:01:02,490
 the past or future, feeling bad about past experiences,

18
00:01:02,490 --> 00:01:04,560
 worrying about the future, or if

19
00:01:04,560 --> 00:01:08,640
 we get caught up thinking about other people, places and

20
00:01:08,640 --> 00:01:12,480
 things, we are not isolated or in solitude.

