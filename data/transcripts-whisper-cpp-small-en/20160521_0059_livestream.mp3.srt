1
00:00:00,000 --> 00:00:16,560
 Good evening everyone from Broadcasting Live, May 20th.

2
00:00:16,560 --> 00:00:27,010
 Today's quote is from the Majimalika Ya, number 122, the

3
00:00:27,010 --> 00:00:35,560
 Mahasunyata Sutta, which has got a lot in it.

4
00:00:35,560 --> 00:00:47,580
 The very last thing the Buddha says is, "I'm not going to

5
00:00:47,580 --> 00:00:53,560
 treat you like wet clay."

6
00:00:53,560 --> 00:01:08,130
 "Mita vata ya sumudha jalata, masa vata vata ya, tangna s

7
00:01:08,130 --> 00:01:10,560
ana."

8
00:01:10,560 --> 00:01:22,560
 "Tatakamisaami yata kumbhaka ro amake amakamate."

9
00:01:22,560 --> 00:01:28,560
 "Nawohang ananda tatakamisaami."

10
00:01:28,560 --> 00:01:34,560
 "I will not treat you, Ananda."

11
00:01:34,560 --> 00:01:40,560
 "Yata kumbhaka ro amake amakamate."

12
00:01:40,560 --> 00:01:56,120
 "Amaka" means uncooked, the unbaked clay, because the pot

13
00:01:56,120 --> 00:01:59,560
ter has to treat their clay with care.

14
00:01:59,560 --> 00:02:07,560
 When the clay is wet, you have to be very careful with it,

15
00:02:07,560 --> 00:02:11,560
 something very fragile.

16
00:02:11,560 --> 00:02:15,890
 So the Buddha said, "I'm not going to treat you as though

17
00:02:15,890 --> 00:02:17,560
 you're fragile."

18
00:02:17,560 --> 00:02:22,470
 "It means I'm not going to go easy on you. I'm not going to

19
00:02:22,470 --> 00:02:26,560
 just keep gloves."

20
00:02:26,560 --> 00:02:36,270
 "I'm not going to pamper you. I'm not going to make it easy

21
00:02:36,270 --> 00:02:38,560
 on you."

22
00:02:38,560 --> 00:02:45,780
 That's what he's saying. By doing that, he's softening the

23
00:02:45,780 --> 00:02:48,560
 blow of what he says.

24
00:02:48,560 --> 00:02:57,560
 The teaching is quite difficult, that's what he's saying.

25
00:02:57,560 --> 00:02:57,560
 He has only our best interest, that's what he's saying.

26
00:02:57,560 --> 00:03:02,020
 Because meditation is tough. Enlightenment, the path to

27
00:03:02,020 --> 00:03:05,560
 enlightenment, is not an easy thing.

28
00:03:05,560 --> 00:03:08,560
 He says, "Repeatedly restraining you I shall speak to you,

29
00:03:08,560 --> 00:03:13,560
 repeatedly admonishing you I shall speak to you."

30
00:03:13,560 --> 00:03:20,560
 The sound core will stand the test.

31
00:03:20,560 --> 00:03:29,760
 "Yosaroso taseti." "Sarosara" means the core, but it also

32
00:03:29,760 --> 00:03:33,560
 means that which has substance.

33
00:03:33,560 --> 00:03:40,560
 If a person has substance, they will stand.

34
00:03:40,560 --> 00:03:47,420
 He's addressing monks who are spending time in society, are

35
00:03:47,420 --> 00:03:54,560
 in danger of getting caught up in the world.

36
00:03:54,560 --> 00:04:03,840
 He wants to address them in such a way that they focus on

37
00:04:03,840 --> 00:04:12,560
 things that are more "sara," more beneficial.

38
00:04:12,560 --> 00:04:20,400
 So he offers this teaching on voidness, this teaching on

39
00:04:20,400 --> 00:04:25,560
 non-self, that things are empty.

40
00:04:25,560 --> 00:04:30,760
 All of the things that we strive for, all of the things

41
00:04:30,760 --> 00:04:34,560
 that we cling to, they in the end don't have substance.

42
00:04:34,560 --> 00:04:38,700
 They come and they go. They think of our belongings, our

43
00:04:38,700 --> 00:04:42,480
 possessions, even our own bodies, our own minds are

44
00:04:42,480 --> 00:04:43,560
 unstable.

45
00:04:43,560 --> 00:04:51,430
 We can't hold on to them, we can't keep them the way they

46
00:04:51,430 --> 00:04:52,560
 are.

47
00:04:52,560 --> 00:05:00,830
 We give something that's hard to understand, but even hard

48
00:05:00,830 --> 00:05:06,560
 to accept, let alone understand.

49
00:05:06,560 --> 00:05:17,490
 We are very much interested in and attached to the things

50
00:05:17,490 --> 00:05:24,560
 in the world and the ways of the world.

51
00:05:24,560 --> 00:05:30,320
 Even with meditation, we come with the idea that it's going

52
00:05:30,320 --> 00:05:34,560
 to be pleasant and stable and controllable.

53
00:05:34,560 --> 00:05:39,560
 We're going to find a way to control our minds.

54
00:05:39,560 --> 00:05:42,230
 We didn't figure that controlling and letting go are two

55
00:05:42,230 --> 00:05:43,560
 very different things.

56
00:05:43,560 --> 00:05:50,560
 In fact, letting go is much more difficult than the two.

57
00:05:50,560 --> 00:05:56,580
 Complete control being impossible, but the act of trying to

58
00:05:56,580 --> 00:05:59,560
 control things is pretty simple.

59
00:05:59,560 --> 00:06:02,560
 We just try and force ourselves this way and that way.

60
00:06:02,560 --> 00:06:06,560
 It's not very fruitful, but it's an easy way out.

61
00:06:06,560 --> 00:06:10,560
 Just try and control your life, try and control everything,

62
00:06:10,560 --> 00:06:13,560
 force everything to be the way you want it.

63
00:06:13,560 --> 00:06:17,560
 In other words, try to always get what you want.

64
00:06:17,560 --> 00:06:20,560
 Never get what you don't want.

65
00:06:20,560 --> 00:06:22,560
 Letting go is quite different.

66
00:06:22,560 --> 00:06:24,560
 Letting go is much more difficult.

67
00:06:24,560 --> 00:06:27,560
 It's difficult just to even accomplish.

68
00:06:27,560 --> 00:06:32,560
 It means not reacting.

69
00:06:32,560 --> 00:06:34,560
 It's not judging.

70
00:06:34,560 --> 00:06:35,560
 It's not clinging.

71
00:06:35,560 --> 00:06:41,560
 It's not avoiding.

72
00:06:41,560 --> 00:06:46,560
 It means learning to open yourself up.

73
00:06:46,560 --> 00:06:49,760
 Open yourself up to what you're feeling, opening yourself

74
00:06:49,760 --> 00:06:51,560
 up to what you're thinking,

75
00:06:51,560 --> 00:06:56,560
 opening yourself up to now, what you're experiencing now,

76
00:06:56,560 --> 00:07:02,560
 and being okay with that, whatever it is.

77
00:07:02,560 --> 00:07:07,570
 Making your peace with it, so you stop trying to fix it,

78
00:07:07,570 --> 00:07:10,560
 stop trying to hold on to it,

79
00:07:10,560 --> 00:07:13,560
 stop trying to get what it is that you want.

80
00:07:13,560 --> 00:07:17,560
 My minds are screaming like little children all the time.

81
00:07:17,560 --> 00:07:20,990
 I want this, I want that, I don't want this, I don't want

82
00:07:20,990 --> 00:07:21,560
 that.

83
00:07:21,560 --> 00:07:26,560
 Meditation is about growing up.

84
00:07:26,560 --> 00:07:32,020
 It's about finally becoming mature in the sense of being

85
00:07:32,020 --> 00:07:36,560
 strong and wise and capable.

86
00:07:36,560 --> 00:07:44,560
 It's the most difficult thing you can do. There's a lot of

87
00:07:44,560 --> 00:07:44,560
 blood, sweat and tears along the way.

88
00:07:44,560 --> 00:07:48,920
 And so the Buddha says, "I'm not going to sugarcoat this

89
00:07:48,920 --> 00:07:51,560
 for you. There's no way to sugarcoat this."

90
00:07:51,560 --> 00:07:54,560
 We can soften the blow.

91
00:07:54,560 --> 00:07:59,560
 We can remind you that it's important to see.

92
00:07:59,560 --> 00:08:03,990
 Well, it's an important thing. It's a serious thing we're

93
00:08:03,990 --> 00:08:04,560
 doing.

94
00:08:04,560 --> 00:08:10,560
 We can prepare you for it mentally, emotionally.

95
00:08:10,560 --> 00:08:14,560
 But in the end, we have to go the distance.

96
00:08:14,560 --> 00:08:18,560
 If you want an experience that is pleasant and comfortable,

97
00:08:18,560 --> 00:08:19,560
 that's easy.

98
00:08:19,560 --> 00:08:23,390
 We can make a spa or something, where people come and get

99
00:08:23,390 --> 00:08:24,560
 massages,

100
00:08:24,560 --> 00:08:33,560
 or get to sit in a jacuzzi or a hot spring or something.

101
00:08:33,560 --> 00:08:39,360
 But it wouldn't solve anything. It wouldn't fix your

102
00:08:39,360 --> 00:08:40,560
 problems.

103
00:08:40,560 --> 00:08:49,640
 It wouldn't lead to true peace. It wouldn't change anything

104
00:08:49,640 --> 00:08:50,560
.

105
00:08:50,560 --> 00:08:54,270
 We can do comfortable meditation where we sit and think

106
00:08:54,270 --> 00:08:55,560
 good thoughts.

107
00:08:55,560 --> 00:09:05,950
 It's valid meditation, really. There's meditation on love,

108
00:09:05,950 --> 00:09:06,560
 meditation on compassion, and so on.

109
00:09:06,560 --> 00:09:14,630
 Good meditation. But as well, they still don't lead to true

110
00:09:14,630 --> 00:09:15,560
 change.

111
00:09:15,560 --> 00:09:20,560
 Because they don't help us understand ourselves.

112
00:09:20,560 --> 00:09:25,270
 The path to true enlightenment is not complicated, and it's

113
00:09:25,270 --> 00:09:27,560
 not hard to understand.

114
00:09:27,560 --> 00:09:36,880
 It's not esoteric or abstruse. It's quite simple. It's just

115
00:09:36,880 --> 00:09:38,560
 not easy.

116
00:09:38,560 --> 00:09:42,560
 It's something you have to really be patient with.

117
00:09:42,560 --> 00:09:45,200
 You don't have to push yourself. You don't have to force

118
00:09:45,200 --> 00:09:45,560
 things.

119
00:09:45,560 --> 00:09:49,560
 You don't have to stress about it.

120
00:09:49,560 --> 00:09:52,560
 But you don't have to be patient.

121
00:09:52,560 --> 00:09:55,870
 You have to be the kind of person who can sit through

122
00:09:55,870 --> 00:10:00,560
 anything, or bear with anything.

123
00:10:00,560 --> 00:10:04,160
 That's the key. You don't have to do anything. You just

124
00:10:04,160 --> 00:10:06,560
 have to bear with everything.

125
00:10:06,560 --> 00:10:10,060
 Once you can bear, then that means being objective. It

126
00:10:10,060 --> 00:10:17,560
 means seeing things as they are.

127
00:10:17,560 --> 00:10:20,320
 That's all it takes. You just have to see things as they

128
00:10:20,320 --> 00:10:20,560
 are.

129
00:10:20,560 --> 00:10:27,560
 You don't have to find anything new or strange or esoteric.

130
00:10:27,560 --> 00:10:31,560
 Just as they are. Quite simple.

131
00:10:31,560 --> 00:10:35,140
 And that's what's so rewarding about it. When you see

132
00:10:35,140 --> 00:10:38,370
 things as they are, there's no one can take that away from

133
00:10:38,370 --> 00:10:38,560
 you.

134
00:10:38,560 --> 00:10:44,040
 No one can make you doubt that. Once you see your mind,

135
00:10:44,040 --> 00:10:48,560
 once you see the nature of your experiences,

136
00:10:48,560 --> 00:10:52,200
 the things that you cling to, the things that you run away

137
00:10:52,200 --> 00:10:52,560
 from,

138
00:10:52,560 --> 00:10:56,520
 once you see that they're not so scary, that they're not so

139
00:10:56,520 --> 00:11:01,560
 attractive, that they just are.

140
00:11:01,560 --> 00:11:08,280
 That no one can make you cling to anything. You can't be

141
00:11:08,280 --> 00:11:17,380
 forced into fleeing or forced into fear or anxiety or

142
00:11:17,380 --> 00:11:20,560
 stress.

143
00:11:20,560 --> 00:11:24,560
 Because you have understanding.

144
00:11:24,560 --> 00:11:27,310
 That's all you have. You don't have fear. You don't have

145
00:11:27,310 --> 00:11:30,560
 anger. You don't have addiction, attachment,

146
00:11:30,560 --> 00:11:35,360
 frustration, boredom, sadness, none of that. You just have

147
00:11:35,360 --> 00:11:36,560
 understanding.

148
00:11:36,560 --> 00:11:40,560
 Just see things clearly.

149
00:11:40,560 --> 00:11:43,880
 What does that do for you? What are all the good things

150
00:11:43,880 --> 00:11:45,560
 that come? What good is it?

151
00:11:45,560 --> 00:11:57,560
 You know, to work so hard, why can't we just take it easy?

152
00:11:57,560 --> 00:12:00,870
 When your mind is pure, when your mind is clear, what is

153
00:12:00,870 --> 00:12:01,560
 the benefit?

154
00:12:01,560 --> 00:12:06,830
 The true benefit is purity. The real benefit of meditation

155
00:12:06,830 --> 00:12:07,560
 is purity.

156
00:12:07,560 --> 00:12:14,110
 Because you can avoid the problem. You can avoid your anger

157
00:12:14,110 --> 00:12:22,560
 and aversion, your greed and addiction.

158
00:12:22,560 --> 00:12:28,560
 But only when you understand, when you understand things,

159
00:12:28,560 --> 00:12:30,560
 can you become pure.

160
00:12:30,560 --> 00:12:33,720
 The mind that is objective is the mind that is pure. The

161
00:12:33,720 --> 00:12:37,560
 mind that just sees things as they are. It's quite simple.

162
00:12:37,560 --> 00:12:42,740
 Here, now, it's not theoretical, it's not philosophical, it

163
00:12:42,740 --> 00:12:43,560
 just is.

164
00:12:43,560 --> 00:12:48,560
 You're just sitting, sitting, walking, you're walking,

165
00:12:48,560 --> 00:12:49,560
 seeing, you're seeing.

166
00:12:49,560 --> 00:12:54,360
 Your mind is very pure. With a pure mind, you're able to

167
00:12:54,360 --> 00:12:59,560
 overcome sadness, sorrow, lamentation, despair.

168
00:12:59,560 --> 00:13:03,700
 You're able to become all sorts of mental sickness like

169
00:13:03,700 --> 00:13:08,560
 depression, anxiety, insomnia, phobia, anger issues,

170
00:13:08,560 --> 00:13:11,560
 addiction issues, all these things.

171
00:13:11,560 --> 00:13:16,020
 You're able to overcome them through the purity. It's like

172
00:13:16,020 --> 00:13:19,560
 pure water washing away all of these things.

173
00:13:19,560 --> 00:13:24,970
 These things that seem so difficult to overcome, so

174
00:13:24,970 --> 00:13:32,560
 complicated and so unsolvable, impossible.

175
00:13:32,560 --> 00:13:40,960
 In fact, are simply washed away with the purity of the mind

176
00:13:40,960 --> 00:13:41,560
.

177
00:13:41,560 --> 00:13:45,090
 When you do that, you don't suffer. You free yourself from

178
00:13:45,090 --> 00:13:48,480
 all the suffering that comes from these mental and the

179
00:13:48,480 --> 00:13:50,560
 system mental upsets.

180
00:13:50,560 --> 00:13:53,700
 The problems in our mind, when they're gone, there's no

181
00:13:53,700 --> 00:13:54,560
 suffering.

182
00:13:54,560 --> 00:13:57,200
 Even if you're in great physical pain, you don't react to

183
00:13:57,200 --> 00:14:00,560
 it. You're at peace even with physical pain.

184
00:14:00,560 --> 00:14:04,690
 Even that, because you can't actually get what you want all

185
00:14:04,690 --> 00:14:05,560
 the time.

186
00:14:05,560 --> 00:14:09,720
 You can't actually ensure that you won't experience

187
00:14:09,720 --> 00:14:11,560
 unpleasant things.

188
00:14:11,560 --> 00:14:15,010
 So the only way to be free, to be truly at peace, is to

189
00:14:15,010 --> 00:14:21,840
 learn to let go, to learn to stop reacting, to become pure

190
00:14:21,840 --> 00:14:23,560
 in mind.

191
00:14:23,560 --> 00:14:26,560
 This is called the right path.

192
00:14:26,560 --> 00:14:29,540
 What it does is it puts you on the right path, whatever you

193
00:14:29,540 --> 00:14:32,470
 do in your life. However you live your life, you do it

194
00:14:32,470 --> 00:14:33,560
 right.

195
00:14:33,560 --> 00:14:37,660
 You don't do it with an angry mind, or with a greedy mind,

196
00:14:37,660 --> 00:14:39,560
 or with a deluded mind.

197
00:14:39,560 --> 00:14:43,110
 You do it with a clear mind, with a pure mind, whatever you

198
00:14:43,110 --> 00:14:43,560
 do.

199
00:14:43,560 --> 00:14:46,980
 If you're a monk, or if you're a lay person, or a

200
00:14:46,980 --> 00:14:53,270
 millionaire, or a billionaire, you do it right with a pure

201
00:14:53,270 --> 00:14:54,560
 mind.

202
00:14:54,560 --> 00:14:59,060
 So you're free. You find freedom. You find freedom in this

203
00:14:59,060 --> 00:15:01,560
 life, no suffering in this life.

204
00:15:01,560 --> 00:15:08,140
 If you're born again, you're born in a place that is free

205
00:15:08,140 --> 00:15:09,560
 and pure.

206
00:15:09,560 --> 00:15:13,780
 It's the freedom of mind. Whether you're in prison, or

207
00:15:13,780 --> 00:15:18,250
 whether you're on top of a mountain, wherever you are, you

208
00:15:18,250 --> 00:15:23,560
 have the freedom of mind.

209
00:15:23,560 --> 00:15:28,560
 No one can take that away from you. No one can enslave you.

210
00:15:28,560 --> 00:15:35,560
 Because you're at peace with yourself. You're free.

211
00:15:35,560 --> 00:15:42,200
 So, I didn't talk too much about the Mahasunya, Sunyata Sut

212
00:15:42,200 --> 00:15:42,560
ta.

213
00:15:42,560 --> 00:15:47,680
 It's a bit complicated, I think. Not really my forte to go

214
00:15:47,680 --> 00:15:49,560
 through all that.

215
00:15:49,560 --> 00:15:52,390
 It goes through Samatha and Vipassana. It's an interesting

216
00:15:52,390 --> 00:15:56,560
 suttav, to be sure. Definitely worth reading.

217
00:15:56,560 --> 00:16:01,460
 But this quote sort of stands apart, and the point of the

218
00:16:01,460 --> 00:16:05,560
 quote is to remind us that this is difficult.

219
00:16:05,560 --> 00:16:08,810
 And that we have to push ourselves, and we have to be

220
00:16:08,810 --> 00:16:11,560
 pushed. We have to accept these teachings.

221
00:16:11,560 --> 00:16:16,190
 The Buddha said, "If you love me, if you care for me, it's

222
00:16:16,190 --> 00:16:21,050
 not about clinging to me, it's about following my teachings

223
00:16:21,050 --> 00:16:21,560
."

224
00:16:21,560 --> 00:16:28,040
 If you don't listen to my teachings, that's what it means

225
00:16:28,040 --> 00:16:30,560
 to not really care.

226
00:16:30,560 --> 00:16:33,760
 That's all we're concerned about, is the point. If you

227
00:16:33,760 --> 00:16:36,840
 follow the teachings, if you do the teachings, if you

228
00:16:36,840 --> 00:16:40,560
 practice them, because they're tough.

229
00:16:40,560 --> 00:16:45,940
 That's what separates someone who cares from someone who

230
00:16:45,940 --> 00:16:47,560
 doesn't care.

231
00:16:47,560 --> 00:16:54,140
 It's not whether you are Buddhist or something. It's about

232
00:16:54,140 --> 00:16:57,560
 whether you care enough to put the teachings into practice.

233
00:16:57,560 --> 00:17:07,370
 It's difficult teachings. Anyway, so that's our bit of dham

234
00:17:07,370 --> 00:17:09,560
ma for tonight.

235
00:17:09,560 --> 00:17:16,560
 See, we have a couple of questions, so I can answer them.

236
00:17:16,560 --> 00:17:20,560
 You can go. That's all.

237
00:17:20,560 --> 00:17:30,560
 I'm just going to answer people's questions.

238
00:17:30,560 --> 00:17:37,420
 Is time a rupa? Well, time doesn't exist. Time is not a

239
00:17:37,420 --> 00:17:54,560
 thing. Time is a quality of things.

240
00:17:54,560 --> 00:17:59,480
 Did the Buddha and do arahants have no need for constant

241
00:17:59,480 --> 00:18:05,560
 noting? They're being so awake and aware and pure of mind.

242
00:18:05,560 --> 00:18:11,300
 Right. Well, arahants see things objectively, so they don't

243
00:18:11,300 --> 00:18:15,560
 have to practice seeing things objectively.

244
00:18:15,560 --> 00:18:23,010
 So it just comes natural, and they see, they know it as

245
00:18:23,010 --> 00:18:24,560
 seeing.

246
00:18:24,560 --> 00:18:27,160
 I think that I occasionally recognize impermanence or no

247
00:18:27,160 --> 00:18:30,290
 self, but I assume that the recognition is only

248
00:18:30,290 --> 00:18:31,560
 intellectual.

249
00:18:31,560 --> 00:18:36,080
 So how does one know when the realization of impermanence

250
00:18:36,080 --> 00:18:39,560
 or non-self is of a supermundane quality?

251
00:18:39,560 --> 00:18:44,220
 Supermundane realization means you're entering into nirvana

252
00:18:44,220 --> 00:18:44,560
.

253
00:18:44,560 --> 00:18:48,650
 Seeing impermanent suffering and non-self means to see that

254
00:18:48,650 --> 00:18:52,560
 the things, it's what you're seeing as you practice.

255
00:18:52,560 --> 00:18:56,560
 You're seeing that you can't control, you can't force.

256
00:18:56,560 --> 00:18:59,630
 You're seeing that trying to force things leads to

257
00:18:59,630 --> 00:19:00,560
 suffering.

258
00:19:00,560 --> 00:19:04,980
 Seeing that things are unstable, that your mind is chaotic,

259
00:19:04,980 --> 00:19:08,560
 that the experiences you have are not certain.

260
00:19:08,560 --> 00:19:12,140
 There's no warning for the way things change and come and

261
00:19:12,140 --> 00:19:12,560
 go.

262
00:19:12,560 --> 00:19:16,560
 Once you start to see that, you start to let go.

263
00:19:16,560 --> 00:19:23,440
 You stop trying to control, trying to force things because

264
00:19:23,440 --> 00:19:26,560
 you see it's not feasible.

265
00:19:26,560 --> 00:19:31,050
 It's not reasonable to think that you can control, that you

266
00:19:31,050 --> 00:19:32,560
 can be in charge.

267
00:19:32,560 --> 00:19:40,230
 That you can keep things the way they are and make them

268
00:19:40,230 --> 00:19:42,560
 satisfying.

269
00:19:42,560 --> 00:19:47,560
 So once you see that enough, it becomes a truth.

270
00:19:47,560 --> 00:19:50,560
 You see that you realize that this is the truth.

271
00:19:50,560 --> 00:19:54,090
 In the beginning it's just hinting at it and you see it

272
00:19:54,090 --> 00:19:56,560
 clearer and clearer and finally it hits you that this is

273
00:19:56,560 --> 00:19:58,560
 the reality, that nothing's worth clinging to.

274
00:19:58,560 --> 00:20:04,510
 And the mind stops clinging and that's the supermundane

275
00:20:04,510 --> 00:20:09,560
 realization because then there's freedom.

276
00:20:09,560 --> 00:20:12,560
 What do you mean about not reacting to physical pain?

277
00:20:12,560 --> 00:20:14,870
 Pain can be a hindrance of the mind, it overwhelms with

278
00:20:14,870 --> 00:20:15,560
 intensity.

279
00:20:15,560 --> 00:20:21,560
 Not so much emotionally is there a difference?

280
00:20:21,560 --> 00:20:25,540
 Well no actually, pain is not a hindrance, pain is just

281
00:20:25,540 --> 00:20:26,560
 physical.

282
00:20:26,560 --> 00:20:29,760
 There's a difference between pain and disliking pain and

283
00:20:29,760 --> 00:20:31,560
 that's what you have to find.

284
00:20:31,560 --> 00:20:38,350
 Once you stop disliking the pain it's actually not a hind

285
00:20:38,350 --> 00:20:39,560
rance.

286
00:20:39,560 --> 00:20:44,560
 It overwhelms with intensity, not exactly.

287
00:20:44,560 --> 00:20:47,560
 I mean in the sense that it takes your attention, yes.

288
00:20:47,560 --> 00:20:50,000
 So if it's very intense then it'll be the only thing you

289
00:20:50,000 --> 00:20:50,560
 experience.

290
00:20:50,560 --> 00:20:53,450
 I mean I suppose you could pass out from the intensity of

291
00:20:53,450 --> 00:20:55,560
 it but that's just a physical thing.

292
00:20:55,560 --> 00:20:59,560
 If you're not passed out then just be mindful of it.

293
00:20:59,560 --> 00:21:07,560
 If you can do that you can actually be at peace with it.

294
00:21:07,560 --> 00:21:11,400
 Once the mind realizes something, how come one finds

295
00:21:11,400 --> 00:21:13,560
 oneself acting with ignorance once again?

296
00:21:13,560 --> 00:21:16,560
 Are the realizations also just a superman?

297
00:21:16,560 --> 00:21:19,560
 Yes, realizations are impermanence.

298
00:21:19,560 --> 00:21:23,960
 You need to gather enough of them so that it becomes a

299
00:21:23,960 --> 00:21:24,560
 truth.

300
00:21:24,560 --> 00:21:27,560
 So that if you realize it as a truth then your mind lets go

301
00:21:27,560 --> 00:21:32,070
 because the only thing that lasts is in a realization of n

302
00:21:32,070 --> 00:21:32,560
imbana.

303
00:21:32,560 --> 00:21:43,560
 Once you realize nimbana that changes you.

304
00:21:43,560 --> 00:21:48,560
 Would you recommend drugs for intense persistent pain?

305
00:21:48,560 --> 00:21:50,560
 I wouldn't recommend them.

306
00:21:50,560 --> 00:21:57,560
 I mean I'd understand if people take painkillers sparingly.

307
00:21:57,560 --> 00:22:01,560
 But you know ideally you deal with the pain.

308
00:22:01,560 --> 00:22:06,670
 If it's intense then yeah, use them sparingly but also try

309
00:22:06,670 --> 00:22:09,560
 to meditate as best you can.

310
00:22:09,560 --> 00:22:13,640
 If you're really into meditation you don't need the pain

311
00:22:13,640 --> 00:22:14,560
killers.

312
00:22:15,560 --> 00:22:41,560
 [Silence]

313
00:22:41,560 --> 00:22:44,560
 You're welcome.

314
00:22:44,560 --> 00:22:47,560
 Okay, let's call it a night then.

315
00:22:47,560 --> 00:22:51,560
 Wish you all good practice.

316
00:22:51,560 --> 00:22:54,560
 See you all soon.

317
00:22:54,560 --> 00:23:04,560
 [Silence]

