1
00:00:00,000 --> 00:00:03,330
 In the Visuddhi Maga there's talk of nimitas, access,

2
00:00:03,330 --> 00:00:04,960
 concentration and kasinas.

3
00:00:04,960 --> 00:00:07,920
 Are these things dubious concepts or can most monks and mon

4
00:00:07,920 --> 00:00:10,740
asteries attest to their reality and usefulness?

5
00:00:10,740 --> 00:00:13,400
 Most monks can't agree on anything.

6
00:00:13,400 --> 00:00:16,920
 So the issues that you've or the

7
00:00:16,920 --> 00:00:21,280
 topics that you've raised are some of the I would say them

8
00:00:21,280 --> 00:00:24,820
 not the more but some of the contentious ones

9
00:00:25,560 --> 00:00:28,590
 and there's prominent monks who denounce them and denounce

10
00:00:28,590 --> 00:00:32,200
 the Visuddhi Maga or denounce these sorts of teachings.

11
00:00:32,200 --> 00:00:36,120
 The Buddha himself used the word nimitta and

12
00:00:36,120 --> 00:00:39,190
 so the Visuddhi Maga is just explaining it and trying to

13
00:00:39,190 --> 00:00:40,160
 standardize it.

14
00:00:40,160 --> 00:00:43,140
 The Visuddhi Maga is not trying to give doctrine. It's

15
00:00:43,140 --> 00:00:47,200
 trying to give practice and it's a practical manual. If you

16
00:00:47,200 --> 00:00:51,520
 practice the kasinas you

17
00:00:53,960 --> 00:00:57,410
 you should be able to use the Visuddhi Maga as a practical

18
00:00:57,410 --> 00:00:59,000
 guide to develop Jhana.

19
00:00:59,000 --> 00:01:02,430
 And whether or not it's going to be exactly as is shown in

20
00:01:02,430 --> 00:01:04,240
 the Visuddhi Maga is not really

21
00:01:04,240 --> 00:01:07,290
 necessary. It's not really the most important thing whether

22
00:01:07,290 --> 00:01:10,080
 or not it's true or false. The point is whether it's

23
00:01:10,080 --> 00:01:11,200
 practical and useful

24
00:01:11,200 --> 00:01:15,670
 which it is and which there are many monks most especially

25
00:01:15,670 --> 00:01:17,320
 the Paok Saiyada

26
00:01:17,320 --> 00:01:21,510
 who apparently uses the Visuddhi Maga and goes through the

27
00:01:21,510 --> 00:01:22,960
 whole of the Visuddhi Maga.

28
00:01:22,960 --> 00:01:24,960
 We tend to skip chapters

29
00:01:24,960 --> 00:01:27,440
 in our tradition, but the Paok Saiyada

30
00:01:27,440 --> 00:01:31,400
 apparently they can remember past lives and enter into jhan

31
00:01:31,400 --> 00:01:33,000
as with all sorts of kasinas.

32
00:01:33,000 --> 00:01:36,250
 I've heard many monks talk about entering into Jhana based

33
00:01:36,250 --> 00:01:37,600
 on kasina practice.

34
00:01:37,600 --> 00:01:41,560
 But the nimitta,

35
00:01:41,560 --> 00:01:45,300
 the Patti Bhagam nimitta for example as some people say it

36
00:01:45,300 --> 00:01:46,120
 doesn't arise

37
00:01:46,120 --> 00:01:49,360
 which seems silly to me,

38
00:01:49,360 --> 00:01:53,500
 access concentration, I mean it's some of these concepts

39
00:01:53,500 --> 00:01:56,080
 are so technical that it's really just

40
00:01:56,080 --> 00:02:00,480
 argumentative and silly to even discuss them.

41
00:02:00,480 --> 00:02:04,140
 The best thing is to just accept them and say well does it

42
00:02:04,140 --> 00:02:06,800
 work? Does it enter into Jhana? If it does then you know

43
00:02:06,800 --> 00:02:08,280
 what's the problem?

44
00:02:08,280 --> 00:02:12,140
 Or for example access neighborhood concentration, access

45
00:02:12,140 --> 00:02:12,760
 concentration.

46
00:02:12,760 --> 00:02:15,160
 Does it

47
00:02:15,640 --> 00:02:20,760
 repress the defilements and the hindrances and if it does

48
00:02:20,760 --> 00:02:22,160
 then then what's wrong with it?

49
00:02:22,160 --> 00:02:25,360
 And who cares what you call it really? Even if you can't

50
00:02:25,360 --> 00:02:27,200
 say it's Jhana or is Jhana

51
00:02:27,200 --> 00:02:31,680
 or can't determine is it access concentration or is it

52
00:02:31,680 --> 00:02:37,880
 attainment concentration or is it momentary concentration?

53
00:02:37,880 --> 00:02:39,560
 That's an even more contentious one.

54
00:02:40,960 --> 00:02:44,130
 Does it work? Does it lead to peace, happiness, freedom

55
00:02:44,130 --> 00:02:46,880
 from suffering? Does it lead to wisdom? Does it lead to

56
00:02:46,880 --> 00:02:48,280
 enlightenment or not?

57
00:02:48,280 --> 00:02:50,280
 You

