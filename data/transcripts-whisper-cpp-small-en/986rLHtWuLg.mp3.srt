1
00:00:00,000 --> 00:00:04,730
 "What causes a person to have a pleasant or painful

2
00:00:04,730 --> 00:00:06,400
 experience?"

3
00:00:06,400 --> 00:00:08,910
 According to the Buddha, if we hold the view that it is all

4
00:00:08,910 --> 00:00:10,240
 caused by what we have done

5
00:00:10,240 --> 00:00:14,960
 in the past, or it is being done by a god, it is considered

6
00:00:14,960 --> 00:00:16,200
 wrong view.

7
00:00:16,200 --> 00:00:19,200
 What is the right view?

8
00:00:19,200 --> 00:00:24,490
 It's a tough one because definitely a lot of it is the past

9
00:00:24,490 --> 00:00:26,920
, but karma is both past

10
00:00:26,920 --> 00:00:29,360
 and present.

11
00:00:29,360 --> 00:00:41,600
 So the point is, it's not all karma, you see,

12
00:00:41,600 --> 00:00:44,010
 because there is what you've done, and then there's the

13
00:00:44,010 --> 00:00:45,520
 circumstances, the people around

14
00:00:45,520 --> 00:00:46,520
 you.

15
00:00:46,520 --> 00:00:49,100
 So you have past karma that you've done, and that's caused

16
00:00:49,100 --> 00:00:51,080
 people to hate you, say.

17
00:00:51,080 --> 00:00:53,730
 There's people who hate you, and therefore they're causing

18
00:00:53,730 --> 00:00:54,640
 you to suffer.

19
00:00:54,640 --> 00:00:58,930
 But the fact that they're perpetuating it in the present is

20
00:00:58,930 --> 00:01:00,200
 a part of that.

21
00:01:00,200 --> 00:01:03,680
 So they can still choose to follow their hatred or to

22
00:01:03,680 --> 00:01:06,480
 acknowledge it, be mindful, let go of

23
00:01:06,480 --> 00:01:07,800
 it.

24
00:01:07,800 --> 00:01:13,380
 And so as an example, this is a part of the cause for you

25
00:01:13,380 --> 00:01:16,320
 to feel pain in that case.

26
00:01:16,320 --> 00:01:19,900
 And if people love you, that's your past karma, the good

27
00:01:19,900 --> 00:01:22,560
 things that you've done, but they

28
00:01:22,560 --> 00:01:26,860
 may hold out or things may happen in their lives that

29
00:01:26,860 --> 00:01:29,640
 change the way they behave, the

30
00:01:29,640 --> 00:01:32,280
 way they perceive you, and so on.

31
00:01:32,280 --> 00:01:36,160
 And that affects whether you experience pleasure or pain

32
00:01:36,160 --> 00:01:37,120
 from them.

33
00:01:37,120 --> 00:01:39,360
 So karma is not everything.

34
00:01:39,360 --> 00:01:40,680
 That's what the Buddha said.

35
00:01:40,680 --> 00:01:42,520
 And so there's someone who held this view that karma was

36
00:01:42,520 --> 00:01:43,920
 everything, and then it's like

37
00:01:43,920 --> 00:01:44,920
 past karma is everything.

38
00:01:44,920 --> 00:01:47,520
 It's like, well, then it's not possible to change.

39
00:01:47,520 --> 00:01:49,440
 It's not possible to become enlightened.

40
00:01:49,440 --> 00:01:54,360
 Or it's not reasonable to practice.

41
00:01:54,360 --> 00:01:56,760
 There's no point in practicing.

42
00:01:56,760 --> 00:01:58,160
 It's a bad view to hold.

43
00:01:58,160 --> 00:02:01,970
 I mean, one way of looking at it is the idea of what's true

44
00:02:01,970 --> 00:02:03,880
 and what's not true isn't the

45
00:02:03,880 --> 00:02:05,240
 most important thing.

46
00:02:05,240 --> 00:02:08,180
 The most important thing is what is useful, what is

47
00:02:08,180 --> 00:02:09,020
 practical.

48
00:02:09,020 --> 00:02:11,960
 So even if, it's hard to get your mind around, but think of

49
00:02:11,960 --> 00:02:13,520
 it like this, even if it were

50
00:02:13,520 --> 00:02:16,750
 true that we had no free will and it was all determined by

51
00:02:16,750 --> 00:02:18,800
 the past, say, which the Buddha

52
00:02:18,800 --> 00:02:22,550
 denied, but suppose that were the case, it still would be a

53
00:02:22,550 --> 00:02:24,520
 bad idea to hold such a view.

54
00:02:24,520 --> 00:02:25,720
 It's ironic, you see.

55
00:02:25,720 --> 00:02:28,210
 And so if anyone who held that view would be in trouble

56
00:02:28,210 --> 00:02:30,080
 because they would be disinclined

57
00:02:30,080 --> 00:02:33,590
 to benefit, to better themselves, and so that such a person

58
00:02:33,590 --> 00:02:35,400
 would be more likely to suffer

59
00:02:35,400 --> 00:02:38,490
 than a person who held the view that we can change, that we

60
00:02:38,490 --> 00:02:39,960
 can benefit ourselves.

61
00:02:39,960 --> 00:02:43,500
 So you see, reality, according to Buddhism, is a bit of a

62
00:02:43,500 --> 00:02:45,400
 slippery thing, I would say,

63
00:02:45,400 --> 00:02:49,000
 because we don't go that step of creating theories.

64
00:02:49,000 --> 00:02:52,190
 I've talked about this before, the idea of free will

65
00:02:52,190 --> 00:02:53,280
 determinism.

66
00:02:53,280 --> 00:03:02,420
 It takes an assumption of a underlying universe outside of

67
00:03:02,420 --> 00:03:08,120
 our experience, which can support

68
00:03:08,120 --> 00:03:12,910
 the idea of a being who has free will or a being who is

69
00:03:12,910 --> 00:03:16,680
 deterministic or physical realities

70
00:03:16,680 --> 00:03:24,800
 or physical entities, quantums, that are deterministic.

71
00:03:24,800 --> 00:03:30,030
 So since Buddhism doesn't go for that, we're better able to

72
00:03:30,030 --> 00:03:32,560
 appreciate and to deal with

73
00:03:32,560 --> 00:03:36,440
 reality, how to act.

74
00:03:36,440 --> 00:03:38,490
 As a person who is deterministic, can't say, "Well, I'm

75
00:03:38,490 --> 00:03:39,560
 going to better myself," because

76
00:03:39,560 --> 00:03:44,120
 they're not really going to better themselves.

77
00:03:44,120 --> 00:03:48,290
 And they may better themselves, but they'll do so thinking

78
00:03:48,290 --> 00:03:50,400
 that it's deterministic.

79
00:03:50,400 --> 00:03:56,080
 And I think definitely there's a danger for these people to

80
00:03:56,080 --> 00:03:58,640
 become complacent and to be

81
00:03:58,640 --> 00:04:01,240
 less inclined to better themselves.

82
00:04:01,240 --> 00:04:03,360
 More inclined to, I mean, there's something good about it

83
00:04:03,360 --> 00:04:04,660
 because more inclined to accept

84
00:04:04,660 --> 00:04:05,660
 their faults.

85
00:04:05,660 --> 00:04:09,040
 They're less inclined to blame themselves.

86
00:04:09,040 --> 00:04:10,040
 There's something there.

87
00:04:10,040 --> 00:04:13,160
 A person who believes in free will has similar problems.

88
00:04:13,160 --> 00:04:16,100
 They tend to blame themselves, to hate themselves, to feel

89
00:04:16,100 --> 00:04:17,920
 frustrated when they can't change

90
00:04:17,920 --> 00:04:20,000
 certain things.

91
00:04:20,000 --> 00:04:24,920
 But this is why Buddhism does away with both of those.

92
00:04:24,920 --> 00:04:28,120
 So what causes, to directly answer your question in brief,

93
00:04:28,120 --> 00:04:29,980
 what causes a person to have these

94
00:04:29,980 --> 00:04:30,980
 experiences?

95
00:04:30,980 --> 00:04:35,320
 Yes, so karma is a big part of it, but a lot of it is past

96
00:04:35,320 --> 00:04:37,960
 karma and a lot of it is present

97
00:04:37,960 --> 00:04:39,600
 karma.

98
00:04:39,600 --> 00:04:45,330
 But it seems like there's also some vaguely defined

99
00:04:45,330 --> 00:04:50,520
 processes like natural physical processes,

100
00:04:50,520 --> 00:04:54,250
 maybe even you could say quantum processes, maybe even

101
00:04:54,250 --> 00:04:55,640
 random elements.

102
00:04:55,640 --> 00:04:59,960
 I don't know if that's even valid.

103
00:04:59,960 --> 00:05:03,400
 The point is not everything is karma.

104
00:05:03,400 --> 00:05:05,960
 What you can say is a lot of it is karma, past karma, and a

105
00:05:05,960 --> 00:05:07,360
 lot of it is present karma.

106
00:05:07,360 --> 00:05:10,790
 So that's where we should watch out because obviously the

107
00:05:10,790 --> 00:05:12,900
 rest of it is out of our control.

108
00:05:12,900 --> 00:05:16,000
 We should cultivate those actions that are actually going

109
00:05:16,000 --> 00:05:17,520
 to benefit us in the present

110
00:05:17,520 --> 00:05:23,160
 and be forgiving of our own past and of the pasts of others

111
00:05:23,160 --> 00:05:23,560
.

112
00:05:23,560 --> 00:05:27,710
 So be forgiving of the past and be able to let go and bear

113
00:05:27,710 --> 00:05:29,860
 with the results of our bad

114
00:05:29,860 --> 00:05:31,720
 past karma.

115
00:05:31,720 --> 00:05:35,440
 So not to become upset when bad things happen to us.

116
00:05:35,440 --> 00:05:38,220
 We should understand that there's various factors that we

117
00:05:38,220 --> 00:05:39,660
 have to take into account.

118
00:05:39,660 --> 00:05:43,680
 Those we can affect and those we cannot change.

119
00:05:43,680 --> 00:05:45,920
 I think that's it.

