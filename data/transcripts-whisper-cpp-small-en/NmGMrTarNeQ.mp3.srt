1
00:00:00,000 --> 00:00:06,000
 Good evening, everyone.

2
00:00:06,000 --> 00:00:21,560
 Broadcasting Live, April 24th, 2016.

3
00:00:21,560 --> 00:00:32,520
 Today's quote is part of a famous story about a monk who

4
00:00:32,520 --> 00:00:37,560
 had dysentery.

5
00:00:37,560 --> 00:00:48,040
 And if you read this, the Buddha asks him, "Why aren't the

6
00:00:48,040 --> 00:00:49,560
 other monks looking after you?"

7
00:00:49,560 --> 00:00:54,950
 He says, "Because I'm not useful to them. I'm of no use to

8
00:00:54,950 --> 00:00:56,560
 them."

9
00:00:56,560 --> 00:00:59,620
 And then he goes and asks the monks, and the monks say, "

10
00:00:59,620 --> 00:01:01,560
Why didn't you look after him?"

11
00:01:01,560 --> 00:01:10,560
 And they confirm it's because he's no use to them.

12
00:01:10,560 --> 00:01:17,090
 "Ati pana bhikkaveta sabhikkuna upa takoti?" Is there any

13
00:01:17,090 --> 00:01:18,560
 monk who's looking after him?

14
00:01:18,560 --> 00:01:23,560
 "Nati bhagavah" No, there is no such person.

15
00:01:23,560 --> 00:01:27,560
 "Kisatang bhikkuna upa tainti?"

16
00:01:27,560 --> 00:01:35,560
 For what reason did the bhikkus not look after?

17
00:01:35,560 --> 00:01:43,560
 "Eso bhanti bhante bhikkhu bhikkunang akarakoh?"

18
00:01:43,560 --> 00:01:49,560
 Akarakoh means he does nothing for them.

19
00:01:49,560 --> 00:01:51,560
 I'm not quite sure what that means.

20
00:01:51,560 --> 00:01:55,940
 "Bhikkhu bhikkunang" for the bhikkus akarakoh means he

21
00:01:55,940 --> 00:01:57,560
 doesn't do things for the bhikkhus.

22
00:01:57,560 --> 00:02:00,560
 "Tainatang bhikkuna upa tainti?"

23
00:02:00,560 --> 00:02:07,060
 For that reason, because of that, they don't look after him

24
00:02:07,060 --> 00:02:07,560
.

25
00:02:07,560 --> 00:02:15,560
 And the Buddha says something, "Nati wo bhikkavemata?"

26
00:02:15,560 --> 00:02:21,560
 There is no, you have no mother, monks.

27
00:02:21,560 --> 00:02:25,560
 "Nati pita?" You have no father.

28
00:02:25,560 --> 00:02:34,560
 "Yewo upata hai yung?" Who would look after you?

29
00:02:34,560 --> 00:02:42,560
 "Dum hei jay bhikkave anyaman yangna bhata upata hisita?"

30
00:02:42,560 --> 00:02:48,560
 "Atakoh jarahe upata hisiti?"

31
00:02:48,560 --> 00:02:55,560
 "Atakoh jarahe?" "Jarahe" What is "jarahe"?

32
00:02:55,560 --> 00:03:04,560
 "Koh jarahe" Now, I've never seen that before.

33
00:03:04,560 --> 00:03:08,560
 "If you don't look after each other, monks, anyaman yang,

34
00:03:08,560 --> 00:03:14,560
 one to the other, each other, atakoh jarahe."

35
00:03:14,560 --> 00:03:20,560
 "Then who now will look after you?"

36
00:03:20,560 --> 00:03:23,930
 And he says something that's quite famous, "Yewo bhikkave

37
00:03:23,930 --> 00:03:25,560
 mang upata hi ya?"

38
00:03:25,560 --> 00:03:29,560
 "Sogilanang upata hi ya?"

39
00:03:29,560 --> 00:03:36,560
 "Who would look after me should look after sick people?"

40
00:03:36,560 --> 00:03:40,240
 It doesn't actually say monks, but that's the implication

41
00:03:40,240 --> 00:03:40,560
 here,

42
00:03:40,560 --> 00:03:42,560
 and many would argue that that's what it means,

43
00:03:42,560 --> 00:03:47,210
 but it's been used to interpret the importance of looking

44
00:03:47,210 --> 00:03:48,560
 after sick people.

45
00:03:48,560 --> 00:03:52,210
 I think that stretching is probably more like, because

46
00:03:52,210 --> 00:03:55,910
 monks aren't really allowed to look after laypeople who are

47
00:03:55,910 --> 00:03:56,560
 sick.

48
00:03:56,560 --> 00:03:58,560
 That wouldn't be appropriate.

49
00:03:58,560 --> 00:04:02,810
 But it's interesting if you think of it as a layperson, if

50
00:04:02,810 --> 00:04:05,560
 you're not a monk,

51
00:04:05,560 --> 00:04:07,860
 to think about the idea of helping sick people is very, it

52
00:04:07,860 --> 00:04:08,560
's a very important thing.

53
00:04:08,560 --> 00:04:16,260
 The Buddha actually found this to be important for the

54
00:04:16,260 --> 00:04:21,560
 monks at least, if not for general purposes

55
00:04:21,560 --> 00:04:34,560
 or in society looking after sick people in general.

56
00:04:34,560 --> 00:04:38,560
 But what's of most interest to me is the part where he says

57
00:04:38,560 --> 00:04:39,560
, "Nati wo bhikkave mata."

58
00:04:39,560 --> 00:04:44,680
 "You don't have a mother or a father who would look after

59
00:04:44,680 --> 00:04:45,560
 you."

60
00:04:45,560 --> 00:04:48,820
 He's making a very good point here, and I think it goes

61
00:04:48,820 --> 00:04:50,560
 beyond monasticism.

62
00:04:50,560 --> 00:04:54,440
 Obviously for monks it's an important point, is that we don

63
00:04:54,440 --> 00:04:57,560
't have family, we don't have caretakers.

64
00:04:57,560 --> 00:05:03,560
 If we don't look after each other, who will look after us?

65
00:05:03,560 --> 00:05:14,390
 But I think on a broader level, this applies to Buddhists

66
00:05:14,390 --> 00:05:16,560
 in general.

67
00:05:16,560 --> 00:05:22,360
 In Thailand they talk about "nyati dhamma," "daman yati,"

68
00:05:22,360 --> 00:05:24,560
 relatives by the dhamma,

69
00:05:24,560 --> 00:05:31,560
 sisters and brothers and mothers and fathers in the dhamma.

70
00:05:31,560 --> 00:05:38,800
 Because beyond monastics it's true that, especially when

71
00:05:38,800 --> 00:05:40,560
 you get on a spiritual path,

72
00:05:40,560 --> 00:05:44,180
 when you start to practice Buddhism, you're less and less

73
00:05:44,180 --> 00:05:46,560
 able to rely upon your families

74
00:05:46,560 --> 00:05:51,140
 and the people around you who are not Buddhist or not

75
00:05:51,140 --> 00:05:52,560
 practicing.

76
00:05:52,560 --> 00:05:55,900
 Even in Buddhist countries, if your family is not med

77
00:05:55,900 --> 00:05:59,560
itating, in Buddhist countries they drink alcohol,

78
00:05:59,560 --> 00:06:05,560
 they kill, they lie, they cheat, all these things.

79
00:06:05,560 --> 00:06:09,560
 It's because someone calls themselves even Buddhists.

80
00:06:09,560 --> 00:06:13,890
 But much more for those of us who have become Buddhists or

81
00:06:13,890 --> 00:06:17,560
 come to practice Buddhism,

82
00:06:17,560 --> 00:06:26,760
 maybe against the hopes of our family, maybe against their

83
00:06:26,760 --> 00:06:28,560
 wishes.

84
00:06:28,560 --> 00:06:34,030
 But certainly we leave them behind, we go somewhere that

85
00:06:34,030 --> 00:06:35,560
 they don't follow.

86
00:06:35,560 --> 00:06:40,000
 So in a sense we leave them behind, meaning they're in one

87
00:06:40,000 --> 00:06:44,560
 place, we're in another spiritually.

88
00:06:44,560 --> 00:06:48,560
 So we stop killing, they still kill, we stop stealing.

89
00:06:48,560 --> 00:06:53,930
 Maybe they don't concern themselves about morality, they

90
00:06:53,930 --> 00:06:55,560
 have no concentration,

91
00:06:55,560 --> 00:07:00,810
 they don't cultivate wisdom, they don't have the same

92
00:07:00,810 --> 00:07:05,560
 understanding what we would call wisdom.

93
00:07:05,560 --> 00:07:13,920
 It's interesting because last night, I just got back from

94
00:07:13,920 --> 00:07:14,560
 New York,

95
00:07:14,560 --> 00:07:18,560
 but last night yesterday was my mother's birthday.

96
00:07:18,560 --> 00:07:25,930
 And so we had a hangout, we had one of these, basically

97
00:07:25,930 --> 00:07:26,560
 what I'm doing now,

98
00:07:26,560 --> 00:07:30,560
 not live of course, or not public.

99
00:07:30,560 --> 00:07:33,710
 But we got together from all around the world, my brother's

100
00:07:33,710 --> 00:07:34,560
 in Taiwan,

101
00:07:34,560 --> 00:07:38,620
 my other two brothers are in, well they were actually with

102
00:07:38,620 --> 00:07:39,560
 my father.

103
00:07:39,560 --> 00:07:44,560
 So I think they were only, well, they were with my father

104
00:07:44,560 --> 00:07:45,560
 and then my mother's in Florida

105
00:07:45,560 --> 00:07:54,560
 and I was in New York, so four different places.

106
00:07:54,560 --> 00:08:02,660
 And so we got together, we joined the hangout and it was

107
00:08:02,660 --> 00:08:07,560
 actually somewhat disappointing,

108
00:08:07,560 --> 00:08:10,560
 if you're gonna let yourself get disappointed.

109
00:08:10,560 --> 00:08:13,560
 I mean, what is the right word?

110
00:08:13,560 --> 00:08:20,560
 It was awkward maybe.

111
00:08:20,560 --> 00:08:26,030
 For a couple, because of the, one of the members of the

112
00:08:26,030 --> 00:08:28,560
 hangout was clearly drunk

113
00:08:28,560 --> 00:08:32,050
 and because it was Passover last night, if you know the

114
00:08:32,050 --> 00:08:32,560
 Jewish holiday,

115
00:08:32,560 --> 00:08:38,420
 my father's family's Jewish, on Passover they drink a lot

116
00:08:38,420 --> 00:08:39,560
 of wine.

117
00:08:39,560 --> 00:08:45,920
 And can get somewhat drunk and so at least one person, if

118
00:08:45,920 --> 00:08:46,560
 not more,

119
00:08:46,560 --> 00:08:54,560
 was somewhat inebriated and slurring their words.

120
00:08:54,560 --> 00:08:59,590
 So the conversation quickly turned into things like alcohol

121
00:08:59,590 --> 00:09:00,560
 and marijuana

122
00:09:00,560 --> 00:09:09,980
 and talking about how, I mean, I was quiet through most of

123
00:09:09,980 --> 00:09:10,560
 it.

124
00:09:10,560 --> 00:09:14,560
 My mother's really great, she doesn't do any of that.

125
00:09:14,560 --> 00:09:17,560
 I think she smokes some marijuana, she used to anyway.

126
00:09:17,560 --> 00:09:23,080
 But she really gave up alcohol and she's interested in a

127
00:09:23,080 --> 00:09:24,560
 clear mind.

128
00:09:24,560 --> 00:09:29,560
 I think she doesn't drink.

129
00:09:29,560 --> 00:09:36,560
 But then my brother in Taiwan, he started talking about

130
00:09:36,560 --> 00:09:39,560
 this, something hairier,

131
00:09:39,560 --> 00:09:44,560
 this hairier group and they'd play this game where one, I

132
00:09:44,560 --> 00:09:44,560
 wasn't listening really,

133
00:09:44,560 --> 00:09:48,300
 but was listening to the part where at the end of the game

134
00:09:48,300 --> 00:09:49,560
 they come together

135
00:09:49,560 --> 00:09:54,560
 and they have a champagne breakfast, I think it was called.

136
00:09:54,560 --> 00:10:00,030
 And he was showing pictures of a case like crate, no, cool

137
00:10:00,030 --> 00:10:00,560
ers full,

138
00:10:00,560 --> 00:10:05,440
 large coolers full of champagne, whiskey and beer and every

139
00:10:05,440 --> 00:10:06,560
 kind of alcohol.

140
00:10:06,560 --> 00:10:11,560
 And he said he drank for 20 hours straight.

141
00:10:11,560 --> 00:10:14,560
 20 hours.

142
00:10:14,560 --> 00:10:17,840
 And he said, I don't want to talk about it because it

143
00:10:17,840 --> 00:10:19,560
 sounds like I'm bragging.

144
00:10:19,560 --> 00:10:22,560
 And the rest of us are kind of well, you know.

145
00:10:22,560 --> 00:10:24,910
 But then he started saying something that's really

146
00:10:24,910 --> 00:10:25,560
 interesting.

147
00:10:25,560 --> 00:10:28,800
 I mean, there's a point why I'm telling this and why I'm

148
00:10:28,800 --> 00:10:29,560
 talking about things

149
00:10:29,560 --> 00:10:32,890
 that probably I shouldn't, this isn't the kind of thing you

150
00:10:32,890 --> 00:10:33,560
 should really broadcast.

151
00:10:33,560 --> 00:10:38,560
 But, you know, we take this, I'm not really criticizing,

152
00:10:38,560 --> 00:10:41,560
 I'm looking at the state of people.

153
00:10:41,560 --> 00:10:55,560
 It's interesting because then he went on about how much

154
00:10:55,560 --> 00:10:56,560
 better it is,

155
00:10:56,560 --> 00:11:01,560
 this kind of a life, how much healthier it is.

156
00:11:01,560 --> 00:11:04,560
 And how he looks at people, the rest of us back,

157
00:11:04,560 --> 00:11:08,780
 or people back in Canada, all of his friends and their hair

158
00:11:08,780 --> 00:11:09,560
 is turning gray

159
00:11:09,560 --> 00:11:16,560
 and they look old and it's because they're serious.

160
00:11:16,560 --> 00:11:21,560
 They're too serious. They take things too seriously.

161
00:11:21,560 --> 00:11:24,560
 And I mean, it was quite clear that drinking for 20 hours

162
00:11:24,560 --> 00:11:28,560
 was somehow,

163
00:11:28,560 --> 00:11:33,560
 the fountain of youth or something.

164
00:11:33,560 --> 00:11:38,110
 Living in Taiwan, I'm sorry, I don't mean to bring my

165
00:11:38,110 --> 00:11:39,560
 brother into this really,

166
00:11:39,560 --> 00:11:43,560
 but it's such a good example because we think like this,

167
00:11:43,560 --> 00:11:49,560
 we think of, we think happiness leads to happiness.

168
00:11:49,560 --> 00:11:53,630
 I mean, it sounded so much like the angels, how angels

169
00:11:53,630 --> 00:11:55,560
 think of heaven.

170
00:11:55,560 --> 00:12:00,560
 They think that it's the good life.

171
00:12:00,560 --> 00:12:04,560
 They've won. They think they won.

172
00:12:04,560 --> 00:12:08,280
 They've beaten Samsara and they found the right way to live

173
00:12:08,280 --> 00:12:08,560
.

174
00:12:08,560 --> 00:12:15,560
 They've earned it and they've found the right path

175
00:12:15,560 --> 00:12:18,610
 and they're doing the right thing when in fact they're

176
00:12:18,610 --> 00:12:19,560
 doing nothing.

177
00:12:19,560 --> 00:12:26,560
 Like we Saka said, they're eating stale food.

178
00:12:26,560 --> 00:12:33,360
 I mean, just they're eating up their, using up their good

179
00:12:33,360 --> 00:12:35,560
 karma from the past.

180
00:12:35,560 --> 00:12:38,560
 We all have this potential in life, right?

181
00:12:38,560 --> 00:12:44,560
 We have the potential to use this great opportunity we have

182
00:12:44,560 --> 00:12:45,560
 as human beings.

183
00:12:45,560 --> 00:12:50,560
 We have such power. We can do so many things as humans.

184
00:12:50,560 --> 00:12:52,560
 And people say, well, what's so great about being a human?

185
00:12:52,560 --> 00:12:54,560
 Humans can be horrible people.

186
00:12:54,560 --> 00:13:00,340
 I was just reading about some of the things that have gone

187
00:13:00,340 --> 00:13:00,560
 on,

188
00:13:00,560 --> 00:13:06,540
 like in South and Central America, the dictatorships that

189
00:13:06,540 --> 00:13:09,560
 were set up by the CIA

190
00:13:09,560 --> 00:13:13,560
 or whatever, I don't know, but those dictatorships,

191
00:13:13,560 --> 00:13:25,560
 there was this, they had this prison in a stadium

192
00:13:25,560 --> 00:13:29,560
 and they sat all the prisoners in the crowds

193
00:13:29,560 --> 00:13:33,680
 and then they just like opened fire on them, shot them with

194
00:13:33,680 --> 00:13:34,560
 machine guns.

195
00:13:34,560 --> 00:13:37,560
 But that wasn't it. It went, like they would drag,

196
00:13:37,560 --> 00:13:39,560
 every so often they would drag someone down from the stands

197
00:13:39,560 --> 00:13:43,560
 and like torture them in front of everyone, like theater.

198
00:13:43,560 --> 00:13:51,560
 And at that point, human beings can do terrible things.

199
00:13:51,560 --> 00:13:53,560
 And so we have this power.

200
00:13:53,560 --> 00:13:57,560
 We have the power to live a hedonistic lifestyle,

201
00:13:57,560 --> 00:14:04,110
 to just enjoy as much pleasure as we can, to not take

202
00:14:04,110 --> 00:14:06,560
 anything seriously.

203
00:14:06,560 --> 00:14:10,330
 And no matter what we do, it appears that we get away with

204
00:14:10,330 --> 00:14:10,560
 it.

205
00:14:10,560 --> 00:14:12,560
 Maybe for the most part, not always,

206
00:14:12,560 --> 00:14:16,560
 but much of the time for many of the people in this world,

207
00:14:16,560 --> 00:14:21,560
 it appears that we get away with whatever we do.

208
00:14:21,560 --> 00:14:24,560
 But it's quite lazy to think like that.

209
00:14:24,560 --> 00:14:26,560
 Like take alcohol, for example, someone who says,

210
00:14:26,560 --> 00:14:32,650
 look at alcohol, drink and be merry and it's a good way to

211
00:14:32,650 --> 00:14:33,560
 live.

212
00:14:33,560 --> 00:14:37,560
 Look at me, I'm doing great.

213
00:14:37,560 --> 00:14:40,560
 And this anecdotal evidence, like look at how great I am

214
00:14:40,560 --> 00:14:44,560
 and I'm doing this.

215
00:14:44,560 --> 00:14:47,560
 There was a funny story a monk once told me,

216
00:14:47,560 --> 00:14:51,560
 or I overheard him telling someone else.

217
00:14:51,560 --> 00:15:02,560
 This man walked in wrinkled, sort of with a pockmarked face

218
00:15:02,560 --> 00:15:08,560
 and looked to be like just really aged.

219
00:15:08,560 --> 00:15:15,560
 And this man sort of walked in, but he had bright eyes.

220
00:15:15,560 --> 00:15:21,560
 He looked ancient, but he had some radiance about him.

221
00:15:21,560 --> 00:15:26,920
 And he started talking about how he's never been in the

222
00:15:26,920 --> 00:15:27,560
 hospital

223
00:15:27,560 --> 00:15:33,560
 and he's never been sick in all his life.

224
00:15:33,560 --> 00:15:39,560
 He's never had any serious health issues.

225
00:15:39,560 --> 00:15:45,560
 And these young men were asking him, oh, what do you do?

226
00:15:45,560 --> 00:15:50,560
 And he says, oh, I drink a bottle of whiskey every day,

227
00:15:50,560 --> 00:15:55,560
 smoke a carton of cigarettes and another carton,

228
00:15:55,560 --> 00:16:00,760
 a pack of cigarettes a day and a bottle of whiskey and on

229
00:16:00,760 --> 00:16:01,560
 and on.

230
00:16:01,560 --> 00:16:04,560
 And I just sit around and do nothing.

231
00:16:04,560 --> 00:16:07,310
 And they're like, wow, and that works. Yeah, it works so

232
00:16:07,310 --> 00:16:07,560
 far.

233
00:16:07,560 --> 00:16:10,560
 It works for me.

234
00:16:10,560 --> 00:16:13,560
 And then someone asked him, how old are you?

235
00:16:13,560 --> 00:16:17,560
 And he said, 25.

236
00:16:17,560 --> 00:16:22,560
 The joke sets him up to seem like he's very old,

237
00:16:22,560 --> 00:16:24,560
 like this old guy is bragging about how he's older.

238
00:16:24,560 --> 00:16:29,100
 It turns out he's a young guy who looks very old because,

239
00:16:29,100 --> 00:16:29,560
 anyway,

240
00:16:29,560 --> 00:16:31,560
 it's just a silly joke.

241
00:16:31,560 --> 00:16:39,560
 But there are studies on alcohol, what it does to the brain

242
00:16:39,560 --> 00:16:39,560
.

243
00:16:39,560 --> 00:16:42,560
 As I understand, it's not good for the brain.

244
00:16:42,560 --> 00:16:46,560
 It causes long-term brain damage.

245
00:16:46,560 --> 00:16:52,030
 I mean, not severe brain damage, but it reduces your brain

246
00:16:52,030 --> 00:16:52,560
 capacity.

247
00:16:52,560 --> 00:17:01,560
 As does marijuana, according to this study.

248
00:17:01,560 --> 00:17:08,560
 But moreover, happiness doesn't lead to happiness.

249
00:17:08,560 --> 00:17:16,560
 Anybody can be happy when things are good,

250
00:17:16,560 --> 00:17:21,560
 as long as they have the ability to enjoy.

251
00:17:21,560 --> 00:17:27,560
 That's not how you measure someone's greatness.

252
00:17:27,560 --> 00:17:30,430
 You don't measure someone's greatness by how good they are

253
00:17:30,430 --> 00:17:32,560
 at enjoying pleasure.

254
00:17:32,560 --> 00:17:36,560
 You measure someone's greatness at how good they are at

255
00:17:36,560 --> 00:17:39,560
 bearing with adversity.

256
00:17:39,560 --> 00:17:42,450
 And the going gets tough. That's when you know the

257
00:17:42,450 --> 00:17:43,560
 character of the person.

258
00:17:43,560 --> 00:17:46,560
 You can't say, wow, that guy sure knows how to have fun.

259
00:17:46,560 --> 00:17:48,560
 Well, it's not that hard to have--

260
00:17:48,560 --> 00:17:51,560
 OK, yes, it's true. Some people don't know how to have fun,

261
00:17:51,560 --> 00:17:54,560
 and you could argue that.

262
00:17:54,560 --> 00:18:00,560
 But it's much easier when you're in your comfort zone.

263
00:18:00,560 --> 00:18:05,950
 So if someone is good at having fun, well, yes, drinking 20

264
00:18:05,950 --> 00:18:06,560
 hours a day,

265
00:18:06,560 --> 00:18:09,560
 that is impressive, I'll admit.

266
00:18:09,560 --> 00:18:12,560
 It's a skill that you've developed.

267
00:18:12,560 --> 00:18:14,560
 But let's see what happens.

268
00:18:14,560 --> 00:18:18,560
 As a result of developing that skill and that ability,

269
00:18:18,560 --> 00:18:25,120
 let's see what happens when you're placed in a situation

270
00:18:25,120 --> 00:18:27,560
 that is challenging to you.

271
00:18:27,560 --> 00:18:32,240
 I mean, does this behavior increase your ability to deal

272
00:18:32,240 --> 00:18:33,560
 with challenges

273
00:18:33,560 --> 00:18:38,300
 if suddenly the country that you're in becomes a fascist

274
00:18:38,300 --> 00:18:39,560
 dictatorship

275
00:18:39,560 --> 00:18:45,150
 and they start to torture people and you are being tortured

276
00:18:45,150 --> 00:18:45,560
?

277
00:18:45,560 --> 00:18:53,560
 How do you react? How do you bear with it?

278
00:18:53,560 --> 00:19:00,560
 This story of this auditorium or this stadium,

279
00:19:00,560 --> 00:19:04,740
 there was a guy, one of the leaders, he was a musician, I

280
00:19:04,740 --> 00:19:05,560
 think,

281
00:19:05,560 --> 00:19:12,010
 and he was leading people in chanting the anthem, the

282
00:19:12,010 --> 00:19:15,560
 national anthem or something.

283
00:19:15,560 --> 00:19:20,560
 And so they called him down and they asked him to sing.

284
00:19:20,560 --> 00:19:24,440
 And so he started singing, and then they strapped him to

285
00:19:24,440 --> 00:19:25,560
 this table

286
00:19:25,560 --> 00:19:29,560
 and they smashed his fingers.

287
00:19:29,560 --> 00:19:31,560
 He was playing a guitar. They got him to play the guitar.

288
00:19:31,560 --> 00:19:35,100
 Then they smashed his fingers or cut them off, cut off his

289
00:19:35,100 --> 00:19:36,560
 fingers or something like that,

290
00:19:36,560 --> 00:19:39,100
 cut off his fingers and then smashed his hands to a bloody

291
00:19:39,100 --> 00:19:39,560
 pulp.

292
00:19:39,560 --> 00:19:42,560
 They did this. This happened, apparently.

293
00:19:42,560 --> 00:19:48,560
 And then they said, "Now sing, now play your guitar."

294
00:19:48,560 --> 00:19:54,170
 And so he stood up and he turned to the crowd, to the other

295
00:19:54,170 --> 00:19:56,560
 prisoners in the stadium,

296
00:19:56,560 --> 00:20:01,560
 and he led them in the anthem or something like that.

297
00:20:01,560 --> 00:20:03,930
 It's an interesting story because of how he dealt with the

298
00:20:03,930 --> 00:20:04,560
 adversity.

299
00:20:04,560 --> 00:20:09,640
 I mean, yeah, okay, singing is not such a noble thing in

300
00:20:09,640 --> 00:20:10,560
 our books,

301
00:20:10,560 --> 00:20:16,560
 but the nobility of being able to deal with adversity.

302
00:20:16,560 --> 00:20:23,240
 I mean, I'm not saying what is it that leads to the ability

303
00:20:23,240 --> 00:20:24,560
 to deal with adversity,

304
00:20:24,560 --> 00:20:27,440
 but that's the question. What is it that leads to adversity

305
00:20:27,440 --> 00:20:27,560
?

306
00:20:27,560 --> 00:20:32,740
 If you believe that the ability, the skill of being able to

307
00:20:32,740 --> 00:20:33,560
 drink a lot

308
00:20:33,560 --> 00:20:40,560
 helps you deal with adversity, that's one theory.

309
00:20:40,560 --> 00:20:44,560
 But we can't say what the future is going to bring.

310
00:20:44,560 --> 00:20:49,950
 And certainly in Cambodia, for example, before the Pol Pot

311
00:20:49,950 --> 00:20:51,560
 massacre,

312
00:20:51,560 --> 00:20:54,560
 people were living good. There were probably a lot of

313
00:20:54,560 --> 00:20:54,560
 people who were saying,

314
00:20:54,560 --> 00:21:00,930
 "Oh, look, life is good. Everything's great. Eat, drink,

315
00:21:00,930 --> 00:21:02,560
 and be merry."

316
00:21:02,560 --> 00:21:06,090
 That doesn't help you. I mean, I would argue it probably

317
00:21:06,090 --> 00:21:06,560
 doesn't do much

318
00:21:06,560 --> 00:21:12,560
 to prepare you for being tortured.

319
00:21:12,560 --> 00:21:17,560
 It's an extreme example, but the example's abound.

320
00:21:17,560 --> 00:21:23,570
 Suppose you get cancer, is the ability to enjoy pleasure

321
00:21:23,570 --> 00:21:26,560
 going to help you with that?

322
00:21:26,560 --> 00:21:34,100
 So if you begin to practice meditation, you start to see

323
00:21:34,100 --> 00:21:35,560
 the benefits in this way.

324
00:21:35,560 --> 00:21:37,560
 You can reflect on this as a sort of benefit.

325
00:21:37,560 --> 00:21:40,560
 "Wow, I'm really able to deal with things that I wouldn't

326
00:21:40,560 --> 00:21:41,560
 have been able to deal with before."

327
00:21:41,560 --> 00:21:46,430
 That's really something quite obvious in the meditation

328
00:21:46,430 --> 00:21:47,560
 practice.

329
00:21:47,560 --> 00:21:52,750
 You learn, you see your reactions, and you learn how to

330
00:21:52,750 --> 00:21:53,560
 avoid those,

331
00:21:53,560 --> 00:22:01,560
 how to navigate an experience carefully without reacting,

332
00:22:01,560 --> 00:22:07,560
 to be careful, to be conscientious.

333
00:22:07,560 --> 00:22:13,560
 Anyway, that's a bit of a tangent, but my point being there

334
00:22:13,560 --> 00:22:13,560
,

335
00:22:13,560 --> 00:22:20,560
 in many ways, we are a family, meditators, Buddhists.

336
00:22:20,560 --> 00:22:25,560
 In many ways, we are the ones that care for each other.

337
00:22:25,560 --> 00:22:29,560
 We are the ones that, it's each and every one of us,

338
00:22:29,560 --> 00:22:37,550
 annya mannya for each other, who is a support to each other

339
00:22:37,550 --> 00:22:37,560
,

340
00:22:37,560 --> 00:22:41,560
 who we would do well to look to for support.

341
00:22:41,560 --> 00:22:44,560
 I mean, I can't look to my family so much for support.

342
00:22:44,560 --> 00:22:48,030
 I can't even visit with them half the time because they're

343
00:22:48,030 --> 00:22:49,560
 drinking alcohol.

344
00:22:49,560 --> 00:22:55,560
 But I know when I'm with meditators, when I was in New York

345
00:22:55,560 --> 00:22:56,560
 at the monastery,

346
00:22:56,560 --> 00:23:00,050
 I knew it was all good. I knew I was around people who I

347
00:23:00,050 --> 00:23:01,560
 could trust,

348
00:23:01,560 --> 00:23:11,560
 who I could count on,

349
00:23:11,560 --> 00:23:16,560
 who I could relate to, who I could sit with, be with,

350
00:23:16,560 --> 00:23:26,560
 and not feel the impetus to leave feeling awkward,

351
00:23:26,560 --> 00:23:30,560
 like everyone around me is drunk.

352
00:23:30,560 --> 00:23:33,560
 Everyone around me is lost, or it's on a different path.

353
00:23:33,560 --> 00:23:38,730
 We're not putting ourselves above anyone, but on different

354
00:23:38,730 --> 00:23:40,560
 paths.

355
00:23:40,560 --> 00:23:47,120
 So, on that note, I'd really like to thank everyone for all

356
00:23:47,120 --> 00:23:48,560
 of the support.

357
00:23:48,560 --> 00:23:55,260
 We have this house, and I'm able to live because people are

358
00:23:55,260 --> 00:23:56,560
 supporting me.

359
00:23:56,560 --> 00:24:01,560
 And that's many of you, and I'd like to thank you.

360
00:24:01,560 --> 00:24:06,610
 And also appreciate the meditation, the amount of

361
00:24:06,610 --> 00:24:07,560
 meditation

362
00:24:07,560 --> 00:24:10,790
 and the number of people involved with this community who

363
00:24:10,790 --> 00:24:12,560
 are practicing meditation.

364
00:24:12,560 --> 00:24:16,660
 It's another wonderful support because it means there's all

365
00:24:16,660 --> 00:24:17,560
 these people

366
00:24:17,560 --> 00:24:19,560
 who we can relate to.

367
00:24:19,560 --> 00:24:22,560
 We have each other, and we can relate to each other,

368
00:24:22,560 --> 00:24:27,560
 and we can talk with each other and encourage each other

369
00:24:27,560 --> 00:24:31,560
 and push each other further on the path,

370
00:24:31,560 --> 00:24:39,560
 push each other to meditate and to cultivate good things.

371
00:24:39,560 --> 00:24:44,170
 So, we care for each other, and true, true, this is true

372
00:24:44,170 --> 00:24:44,560
 family.

373
00:24:44,560 --> 00:24:46,560
 We don't depend on our father and mother.

374
00:24:46,560 --> 00:24:53,290
 We don't depend on our brothers and sisters in spiritual

375
00:24:53,290 --> 00:24:55,560
 matters, right?

376
00:24:55,560 --> 00:24:57,560
 In important matters.

377
00:24:57,560 --> 00:25:00,360
 In many ways, we can't depend on them because they just end

378
00:25:00,360 --> 00:25:02,560
 up leading us down the wrong path

379
00:25:02,560 --> 00:25:07,560
 because they don't... well, their understanding is...

380
00:25:07,560 --> 00:25:11,900
 Yes, we would say wrong, but at the very least, their

381
00:25:11,900 --> 00:25:15,560
 understanding is different from ours.

382
00:25:15,560 --> 00:25:21,560
 And so, it would be conflicting for us to depend too much

383
00:25:21,560 --> 00:25:23,560
 upon our family, right?

384
00:25:23,560 --> 00:25:25,560
 We have a problem, what do we do?

385
00:25:25,560 --> 00:25:29,560
 We'll buy a big bag of pot and smoke it all.

386
00:25:29,560 --> 00:25:31,560
 That's what they were talking about on this Hangout.

387
00:25:31,560 --> 00:25:34,390
 They've got to find a way, and they're talking about how to

388
00:25:34,390 --> 00:25:34,560
 find a pot dealer

389
00:25:34,560 --> 00:25:36,560
 where they can get a big bag of pot.

390
00:25:36,560 --> 00:25:41,560
 This is my family, you know?

391
00:25:41,560 --> 00:25:49,560
 So, this quote is somewhat apropos to my life.

392
00:25:49,560 --> 00:25:54,560
 I mean, in that part of it is.

393
00:25:54,560 --> 00:25:57,560
 Is anybody here?

394
00:25:57,560 --> 00:25:59,560
 Anybody have any questions?

395
00:25:59,560 --> 00:26:02,560
 We'll take text questions.

396
00:26:02,560 --> 00:26:04,560
 She got them.

397
00:26:04,560 --> 00:26:06,560
 24 viewers on YouTube.

398
00:26:06,560 --> 00:26:08,560
 Hello, everyone.

399
00:26:08,560 --> 00:26:16,190
 Somebody's watching, and we got a bunch of people on our

400
00:26:16,190 --> 00:26:19,560
 meditation page.

401
00:26:19,560 --> 00:26:34,560
 We got the usual suspects.

402
00:26:34,560 --> 00:26:40,560
 So, Robin, are we going to do some sort of...

403
00:26:40,560 --> 00:26:42,560
 Are we going to give people the opportunity?

404
00:26:42,560 --> 00:26:45,140
 Not obviously pushing for it, but give people the

405
00:26:45,140 --> 00:26:45,560
 opportunity

406
00:26:45,560 --> 00:26:50,560
 if they want to offer a robe to Ajahn Thanh.

407
00:26:50,560 --> 00:27:00,560
 Like, join us?

408
00:27:00,560 --> 00:27:05,560
 Because there was, I think, Aurora, I think, wait.

409
00:27:05,560 --> 00:27:08,560
 Someone, anyway, someone was interested in offering a robe,

410
00:27:08,560 --> 00:27:11,560
 set of robes to Ajahn Thanh.

411
00:27:11,560 --> 00:27:15,560
 And because it's not... I think it's quite easy for us

412
00:27:15,560 --> 00:27:18,560
 to add sets of robes to our order,

413
00:27:18,560 --> 00:27:23,180
 but it's just a matter of giving people the opportunity if

414
00:27:23,180 --> 00:27:23,560
 they want.

415
00:27:23,560 --> 00:27:25,560
 We're not coercion.

416
00:27:25,560 --> 00:27:28,130
 It's not like trying to push people to say this is a good

417
00:27:28,130 --> 00:27:28,560
 thing.

418
00:27:28,560 --> 00:27:31,560
 Somebody really would be keen to do that, I think,

419
00:27:31,560 --> 00:27:35,560
 if they want to understand.

420
00:27:35,560 --> 00:27:39,560
 I mean, I don't like to put pressure

421
00:27:39,560 --> 00:27:42,540
 and make people think like, oh, that's what we're all about

422
00:27:42,540 --> 00:27:42,560
,

423
00:27:42,560 --> 00:27:45,560
 soliciting donations. We're not.

424
00:27:45,560 --> 00:27:48,560
 But we are about giving.

425
00:27:48,560 --> 00:27:50,560
 We like to give.

426
00:27:50,560 --> 00:27:57,410
 If you want to give with us, you're welcome to give with us

427
00:27:57,410 --> 00:27:57,560
.

428
00:27:57,560 --> 00:28:01,560
 Yeah, we could... Who knows if people want...

429
00:28:01,560 --> 00:28:07,560
 See, the thing is, there was some people who supported me

430
00:28:07,560 --> 00:28:10,560
 to get a ticket, but there was lots of support.

431
00:28:10,560 --> 00:28:14,160
 So I thought, well, we can get a gift for Ajahn Thanh as

432
00:28:14,160 --> 00:28:14,560
 well,

433
00:28:14,560 --> 00:28:17,560
 because that's important.

434
00:28:17,560 --> 00:28:20,780
 And we thought about what kind of gifts, and I thought

435
00:28:20,780 --> 00:28:21,560
 robes.

436
00:28:21,560 --> 00:28:25,560
 Robes are the best, because robes symbolize monasticism.

437
00:28:25,560 --> 00:28:29,560
 So if you are interested in the idea of becoming a monk

438
00:28:29,560 --> 00:28:34,560
 or if you want to be close to the monastic life,

439
00:28:34,560 --> 00:28:39,560
 the ascetic or non-sean life,

440
00:28:39,560 --> 00:28:42,890
 robes are a really good symbolic gift and practical gift as

441
00:28:42,890 --> 00:28:43,560
 well,

442
00:28:43,560 --> 00:28:53,560
 because you're actually helping to close Buddhist monks.

443
00:28:53,560 --> 00:28:59,560
 Awesome. So there's still time we can set up a campaign.

444
00:28:59,560 --> 00:29:01,560
 Let me make it clear that this is...

445
00:29:01,560 --> 00:29:03,560
 Well, I mean, it is pretty clear.

446
00:29:03,560 --> 00:29:06,560
 We just have to...

447
00:29:06,560 --> 00:29:10,980
 We don't want it to sound like we're looking for money or

448
00:29:10,980 --> 00:29:13,560
 something.

449
00:29:13,560 --> 00:29:15,560
 I don't think we should make a big deal out of it.

450
00:29:15,560 --> 00:29:19,560
 I don't want... We've got...

451
00:29:19,560 --> 00:29:25,560
 We had a couple of other crowds funding things.

452
00:29:25,560 --> 00:29:27,560
 This is just if there was someone out there.

453
00:29:27,560 --> 00:29:31,560
 We're doing it already, so we can always have a robe,

454
00:29:31,560 --> 00:29:39,560
 a set of robes if someone happened to want to do that.

455
00:29:39,560 --> 00:29:42,560
 Okay, well, let me know how...

456
00:29:42,560 --> 00:29:51,560
 If you can work something out there.

457
00:29:51,560 --> 00:30:01,560
 Awesome. Thank you. Tomorrow.

458
00:30:01,560 --> 00:30:03,560
 Okay, so we'll talk about...

459
00:30:03,560 --> 00:30:05,560
 You can come on the Hangout tomorrow then.

460
00:30:05,560 --> 00:30:09,560
 Talk about it, maybe.

461
00:30:09,560 --> 00:30:13,300
 Anyway, we'll say good night for tonight then. No questions

462
00:30:13,300 --> 00:30:13,560
.

463
00:30:13,560 --> 00:30:15,560
 Have a good night, everyone.

464
00:30:15,560 --> 00:30:16,560
 Thanks for tuning in.

465
00:30:16,560 --> 00:30:31,560
 Wishing you all the best.

466
00:30:32,560 --> 00:30:33,560
 Thank you.

467
00:30:34,560 --> 00:30:35,560
 Thank you.

468
00:30:37,560 --> 00:30:38,560
 Thank you.

469
00:31:03,560 --> 00:31:06,560
 We're still live. Thank you. Good night.

470
00:31:06,560 --> 00:31:07,560
 Thank you.

471
00:31:09,560 --> 00:31:10,560
 Thank you.

