1
00:00:00,000 --> 00:00:03,680
 So my name is Anton and I have a couple, two questions

2
00:00:03,680 --> 00:00:04,500
 exactly for you.

3
00:00:04,500 --> 00:00:07,710
 And one of them is, do you have any practical advice for

4
00:00:07,710 --> 00:00:09,300
 getting past bad habits?

5
00:00:09,300 --> 00:00:11,800
 Like addictions, for example, like sex.

6
00:00:11,800 --> 00:00:14,560
 But I know there's like a whole like treasure trove of

7
00:00:14,560 --> 00:00:15,800
 advice on these things.

8
00:00:15,800 --> 00:00:19,540
 But I can't seem to find something that, you know, that I

9
00:00:19,540 --> 00:00:22,300
 can actually put into use that I'm not lazy about it.

10
00:00:22,300 --> 00:00:25,300
 You know, maybe it's just me, but I don't know.

11
00:00:25,300 --> 00:00:29,800
 So that's my first question, I guess.

12
00:00:29,800 --> 00:00:34,600
 Well, habits are difficult for that very reason.

13
00:00:34,600 --> 00:00:36,700
 I mean, there's no easy answer.

14
00:00:36,700 --> 00:00:40,800
 It could take you lifetimes to change your bad habits.

15
00:00:40,800 --> 00:00:41,900
 And hopefully it doesn't.

16
00:00:41,900 --> 00:00:44,900
 And it depends on you, how intensive.

17
00:00:44,900 --> 00:00:49,300
 The actual practice, I mean, clearly it's our point of view

18
00:00:49,300 --> 00:00:54,400
 is it's simply the technique that we follow.

19
00:00:54,400 --> 00:00:58,050
 But specifically for addiction, the Buddha did give certain

20
00:00:58,050 --> 00:00:58,600
 advice.

21
00:00:58,600 --> 00:01:03,100
 And I've sort of adapted it and pinpointed.

22
00:01:03,100 --> 00:01:07,520
 And so I did this video that was actually a little bit

23
00:01:07,520 --> 00:01:10,900
 probably too, I think it's a bit too long.

24
00:01:10,900 --> 00:01:12,700
 But it exploded.

25
00:01:12,700 --> 00:01:14,500
 That's one of my most popular videos.

26
00:01:14,500 --> 00:01:17,740
 So if you look up pornography, if you haven't seen my video

27
00:01:17,740 --> 00:01:21,100
 on pornography and masturbation, I think is,

28
00:01:21,100 --> 00:01:25,020
 it's of course, you know, those keywords make it a very,

29
00:01:25,020 --> 00:01:26,600
 very popular video.

30
00:01:26,600 --> 00:01:30,610
 But basically what it says is there are three things that

31
00:01:30,610 --> 00:01:33,500
 you have to focus on.

32
00:01:33,500 --> 00:01:37,340
 And the Buddha actually mentioned these three as being

33
00:01:37,340 --> 00:01:38,400
 important.

34
00:01:38,400 --> 00:01:43,130
 Anyone who is any teacher worth their salt should be

35
00:01:43,130 --> 00:01:46,200
 teaching these three things.

36
00:01:46,200 --> 00:01:52,180
 The first is the experience itself, you know, the physical

37
00:01:52,180 --> 00:01:55,900
 seeing or hearing or smelling or tasting or feeling.

38
00:01:55,900 --> 00:02:01,600
 So if it's a physical contact or if it's a seeing of

39
00:02:01,600 --> 00:02:05,500
 something, you know, sex, for example, if you see a

40
00:02:05,500 --> 00:02:07,000
 beautiful woman.

41
00:02:07,000 --> 00:02:11,010
 So the first thing is to focus on the seeing, to try and

42
00:02:11,010 --> 00:02:14,000
 experience the aspect that is just seeing.

43
00:02:14,000 --> 00:02:17,100
 Forget about the rest of it.

44
00:02:17,100 --> 00:02:21,600
 The second aspect is the feeling, pleasure.

45
00:02:21,600 --> 00:02:23,200
 With addiction, it's going to be pleasure.

46
00:02:23,200 --> 00:02:25,000
 It can sometimes be calm.

47
00:02:25,000 --> 00:02:28,360
 So you have to watch out for that as well if you feel just

48
00:02:28,360 --> 00:02:30,400
 calm, you know, peaceful.

49
00:02:30,400 --> 00:02:32,490
 But either way, one or the other, there's going to be that

50
00:02:32,490 --> 00:02:33,000
 feeling.

51
00:02:33,000 --> 00:02:38,200
 So you have to try to focus just on that feeling.

52
00:02:38,200 --> 00:02:42,700
 And the third one is the craving, the desire.

53
00:02:42,700 --> 00:02:44,800
 So this can either be liking or wanting.

54
00:02:44,800 --> 00:02:46,500
 It's either or.

55
00:02:46,500 --> 00:02:50,810
 If you like something, try to focus just on the desire, the

56
00:02:50,810 --> 00:02:51,900
 liking of it.

57
00:02:51,900 --> 00:02:55,140
 If you want something that you don't have, try to focus

58
00:02:55,140 --> 00:02:56,600
 just on the wanting.

59
00:02:56,600 --> 00:03:01,920
 Now, the point is to separate these three because the

60
00:03:01,920 --> 00:03:09,200
 ordinary experience is to take it as one addiction, you

61
00:03:09,200 --> 00:03:09,200
 know,

62
00:03:09,200 --> 00:03:12,000
 or let's say we take it as one experience.

63
00:03:12,000 --> 00:03:16,800
 You see the object of your desire and there's the desire

64
00:03:16,800 --> 00:03:19,800
 and there's the pleasure involved and wanting.

65
00:03:19,800 --> 00:03:23,600
 And you say, oh my gosh, here I go again.

66
00:03:23,600 --> 00:03:26,100
 And you think of it as a thing, an entity.

67
00:03:26,100 --> 00:03:30,200
 This is the core problem.

68
00:03:30,200 --> 00:03:33,280
 This is what a part of what is meant by non-self is that

69
00:03:33,280 --> 00:03:34,300
 that's wrong.

70
00:03:34,300 --> 00:03:35,600
 That's not just wrong.

71
00:03:35,600 --> 00:03:36,900
 It's a problem.

72
00:03:36,900 --> 00:03:39,800
 The problem is that you see it as a problem.

73
00:03:39,800 --> 00:03:41,800
 You see it as a thing.

74
00:03:41,800 --> 00:03:43,200
 It's atomic.

75
00:03:43,200 --> 00:03:47,800
 Atomic, the word atom means unbreakable, indivisible.

76
00:03:47,800 --> 00:03:50,200
 And so atoms don't actually exist.

77
00:03:50,200 --> 00:03:54,560
 The atom was proven to be made up of smaller things, so not

78
00:03:54,560 --> 00:03:57,100
 actually atomic.

79
00:03:57,100 --> 00:04:00,400
 And this is the case with experience as well.

80
00:04:00,400 --> 00:04:04,400
 It's made up of individual aspects, individual experiences.

81
00:04:04,400 --> 00:04:08,800
 And if you can break it up into its constituent parts, it

82
00:04:08,800 --> 00:04:09,900
 dissolves entirely.

83
00:04:09,900 --> 00:04:11,900
 The problem disappears.

84
00:04:11,900 --> 00:04:15,280
 So all you have to do, and you can try this and you can see

85
00:04:15,280 --> 00:04:15,500
,

86
00:04:15,500 --> 00:04:18,600
 it means stop seeing it as a problem.

87
00:04:18,600 --> 00:04:20,200
 Stop feeling guilty about it.

88
00:04:20,200 --> 00:04:21,400
 Stop analyzing it.

89
00:04:21,400 --> 00:04:31,900
 Stop rationalizing or trying to fix the problem.

90
00:04:31,900 --> 00:04:35,990
 Just look at the individual mind states based on these

91
00:04:35,990 --> 00:04:36,900
 three things.

92
00:04:36,900 --> 00:04:41,870
 The experience of seeing, when you see something, try to

93
00:04:41,870 --> 00:04:42,600
 say to yourself,

94
00:04:42,600 --> 00:04:46,800
 seeing, seeing, when you feel something, feeling, when you

95
00:04:46,800 --> 00:04:48,400
 feel contact,

96
00:04:48,400 --> 00:04:51,000
 feeling, feeling.

97
00:04:51,000 --> 00:04:55,000
 Then try to focus on just the feelings.

98
00:04:55,000 --> 00:04:57,850
 When you have a pleasurable sensation, switch to the

99
00:04:57,850 --> 00:04:59,700
 pleasure and say to yourself,

100
00:04:59,700 --> 00:05:02,850
 have pleasure, pleasure, happy, happy, or feeling, feeling,

101
00:05:02,850 --> 00:05:03,800
 calm, calm.

102
00:05:03,800 --> 00:05:06,300
 If that's the case.

103
00:05:06,300 --> 00:05:09,820
 And when you notice the desire, switch that, say to

104
00:05:09,820 --> 00:05:10,200
 yourself,

105
00:05:10,200 --> 00:05:13,000
 wanting, wanting, or liking, liking.

106
00:05:13,000 --> 00:05:15,870
 In general, pick whichever one is most prevalent at any

107
00:05:15,870 --> 00:05:16,500
 given moment,

108
00:05:16,500 --> 00:05:18,500
 and that's going to change from moment to moment.

109
00:05:18,500 --> 00:05:22,200
 So you have to be, you have to be sharp,

110
00:05:22,200 --> 00:05:25,100
 you have to be systematic about it.

111
00:05:25,100 --> 00:05:29,230
 Moving, you have to be flexible, jumping from one to the

112
00:05:29,230 --> 00:05:29,800
 other.

113
00:05:29,800 --> 00:05:31,900
 At the same time, what I didn't mention in that video,

114
00:05:31,900 --> 00:05:34,300
 there's actually other things, again, not actually

115
00:05:34,300 --> 00:05:36,400
 mentioned in this teaching of the Buddha.

116
00:05:36,400 --> 00:05:39,000
 Guilt, you're going to have to actually focus on the guilt

117
00:05:39,000 --> 00:05:40,100
 and be mindful of it.

118
00:05:40,100 --> 00:05:45,000
 Guilty, feeling, I guess, or frustrated, or so on.

119
00:05:45,000 --> 00:05:48,000
 Sad, however it is.

120
00:05:48,000 --> 00:05:52,800
 And thinking, so analyzing and obsessing and so on.

121
00:05:52,800 --> 00:05:55,300
 You have to focus on thoughts as well.

122
00:05:55,300 --> 00:05:59,600
 These are two other aspects of it that are also important,

123
00:05:59,600 --> 00:06:01,760
 but they're the ones that are going to obscure these other

124
00:06:01,760 --> 00:06:02,200
 three.

125
00:06:02,200 --> 00:06:04,300
 So if you cut through those two,

126
00:06:04,300 --> 00:06:06,900
 you'll find underneath there are basically three things

127
00:06:06,900 --> 00:06:08,500
 that you have to focus on.

128
00:06:08,500 --> 00:06:11,990
 As you do that, you'll find yourself able to deconstruct

129
00:06:11,990 --> 00:06:12,900
 the habit.

130
00:06:12,900 --> 00:06:16,020
 It's simply a matter of doing that systematically, and that

131
00:06:16,020 --> 00:06:16,800
's the problem.

132
00:06:16,800 --> 00:06:20,630
 That's why a teacher is important, a meditation center is

133
00:06:20,630 --> 00:06:21,300
 important,

134
00:06:21,300 --> 00:06:24,000
 intensive practice is important.

135
00:06:24,000 --> 00:06:28,100
 You know, this is something that is quite deep and strong.

136
00:06:28,100 --> 00:06:36,800
 So you need something deep and strong, or to counter it.

137
00:06:36,800 --> 00:06:39,600
 - Okay. - Right. Thank you so much.

138
00:06:39,600 --> 00:06:42,090
 - You're welcome. - It's a lot of advice to take at one

139
00:06:42,090 --> 00:06:42,900
 point.

140
00:06:42,900 --> 00:06:45,800
 So my second question, and I hope it's not too...

141
00:06:45,800 --> 00:06:49,260
 - Is it related? - It's a bit different. It's about

142
00:06:49,260 --> 00:06:49,800
 meditation.

143
00:06:49,800 --> 00:06:51,000
 Okay, I'm going to stop.

