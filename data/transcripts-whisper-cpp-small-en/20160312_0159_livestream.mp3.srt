1
00:00:00,000 --> 00:00:07,000
 Hey, good evening everyone.

2
00:00:07,000 --> 00:00:13,000
 We're broadcasting live.

3
00:00:13,000 --> 00:00:16,000
 March.

4
00:00:16,000 --> 00:00:19,000
 11th.

5
00:00:19,000 --> 00:00:30,000
 March.

6
00:00:30,000 --> 00:00:45,000
 Today's quote is about sand on a fingernail.

7
00:00:45,000 --> 00:00:58,000
 The Buddha was accustomed to or was...

8
00:00:58,000 --> 00:01:04,000
 He was really good at finding examples in nature

9
00:01:04,000 --> 00:01:16,600
 and coming up with comparisons between the nature of the

10
00:01:16,600 --> 00:01:18,000
 world,

11
00:01:18,000 --> 00:01:24,000
 the natural world and the nature of reality.

12
00:01:24,000 --> 00:01:33,540
 He was able to show similar... using simple examples in

13
00:01:33,540 --> 00:01:35,000
 nature.

14
00:01:35,000 --> 00:01:40,000
 So it's a very common thing, common tool.

15
00:01:40,000 --> 00:01:47,170
 So one day he picked up a few grains of sand on his fingern

16
00:01:47,170 --> 00:01:48,000
ail

17
00:01:48,000 --> 00:01:54,000
 and he asked Ananda.

18
00:01:54,000 --> 00:02:01,000
 He asked a bunch of bhikkhus.

19
00:02:01,000 --> 00:02:02,000
 He asked the bhikkhus,

20
00:02:02,000 --> 00:02:08,000
 "Tangying manyattan bhikkhu e kattamang nukhu bhautarang."

21
00:02:08,000 --> 00:02:12,000
 "Tell me what you think monks, which is more?"

22
00:02:12,000 --> 00:02:18,000
 "Yogwa yang maya parito nakasika yang bangsu."

23
00:02:18,000 --> 00:02:25,000
 "Arukitu."

24
00:02:25,000 --> 00:02:31,000
 The little bit of sand that I've put on my fingernail.

25
00:02:31,000 --> 00:02:37,000
 "Wah" or "Ayang maha patu di" or this crater.

26
00:02:37,000 --> 00:02:46,000
 He used the simile for different things.

27
00:02:46,000 --> 00:02:52,000
 In one place he talks about... well not exactly this simile

28
00:02:52,000 --> 00:02:52,000
,

29
00:02:52,000 --> 00:03:09,000
 but he says this even a little bit of...

30
00:03:09,000 --> 00:03:13,390
 There's one place where he talks about a little bit of

31
00:03:13,390 --> 00:03:14,000
 feces,

32
00:03:14,000 --> 00:03:21,000
 a little bit of manure, cow dung or human dung, excrement,

33
00:03:21,000 --> 00:03:23,000
 a little bit of excrement.

34
00:03:23,000 --> 00:03:25,000
 You see if you have a little bit of excrement,

35
00:03:25,000 --> 00:03:29,000
 is it any better than a lot of excrement?

36
00:03:29,000 --> 00:03:31,940
 So a little bit of excrement, suddenly beautiful, suddenly

37
00:03:31,940 --> 00:03:34,000
 wonderful.

38
00:03:34,000 --> 00:03:40,000
 He said no, even a little bit of excrement is a bad thing.

39
00:03:40,000 --> 00:03:43,200
 The Buddha said on the same way, even a little bit of

40
00:03:43,200 --> 00:03:44,000
 suffering,

41
00:03:44,000 --> 00:03:50,000
 a little bit of samsara, a little bit of rebirth.

42
00:03:50,000 --> 00:03:57,030
 "Samsara" is still bad, still not the way, still not a goal

43
00:03:57,030 --> 00:03:58,000
.

44
00:03:58,000 --> 00:04:01,000
 That's something you should look for, should seek for.

45
00:04:01,000 --> 00:04:07,410
 But here he uses this little bit to say that's like the

46
00:04:07,410 --> 00:04:08,000
 number of beings

47
00:04:08,000 --> 00:04:10,000
 that are born as humans.

48
00:04:10,000 --> 00:04:14,000
 Being born as a human is a rare thing.

49
00:04:14,000 --> 00:04:21,340
 The number of beings that are born as animals, as insects

50
00:04:21,340 --> 00:04:26,000
 or rodents, birds,

51
00:04:26,000 --> 00:04:35,000
 and even the higher mammals, fish,

52
00:04:35,000 --> 00:04:39,310
 all these various types of animals, it's like the great

53
00:04:39,310 --> 00:04:40,000
 earth.

54
00:04:40,000 --> 00:04:54,000
 It's far, far more than it, only a few humans.

55
00:04:54,000 --> 00:04:56,680
 And even what he doesn't say here, but what he says

56
00:04:56,680 --> 00:04:57,000
 elsewhere

57
00:04:57,000 --> 00:05:03,000
 is "apaka demonu se su ye chana targan"

58
00:05:03,000 --> 00:05:09,220
 Few are the humans who actually make it to a good

59
00:05:09,220 --> 00:05:11,000
 destination,

60
00:05:11,000 --> 00:05:14,000
 who actually headed in the right direction.

61
00:05:14,000 --> 00:05:18,000
 So being born as a human being is like winning a lottery.

62
00:05:18,000 --> 00:05:25,000
 Then most of us just squander it, waste it.

63
00:05:25,000 --> 00:05:30,000
 It's like just a coincidence and then it's gone.

64
00:05:30,000 --> 00:05:42,000
 We're back into the pool of samsara.

65
00:05:42,000 --> 00:05:51,400
 So there are many ways that we have to remember to take

66
00:05:51,400 --> 00:05:52,000
 advantage

67
00:05:52,000 --> 00:05:54,000
 of being born as a human being.

68
00:05:54,000 --> 00:05:57,000
 We have to act like human beings.

69
00:05:57,000 --> 00:06:01,000
 There are human beings that act like hell beings.

70
00:06:01,000 --> 00:06:06,000
 There are human beings that act like ghosts.

71
00:06:06,000 --> 00:06:09,000
 There are human beings that act like animals.

72
00:06:09,000 --> 00:06:12,410
 And that's where they're going, that's where they're

73
00:06:12,410 --> 00:06:13,000
 heading.

74
00:06:13,000 --> 00:06:16,000
 Someone who's full of anger and hatred.

75
00:06:16,000 --> 00:06:25,000
 It's like they're a demon on earth.

76
00:06:25,000 --> 00:06:30,000
 A person who's full of greed.

77
00:06:30,000 --> 00:06:34,000
 Wanting this, wanting that can overcome their wanting.

78
00:06:34,000 --> 00:06:37,000
 It's like a ghost.

79
00:06:37,000 --> 00:06:45,000
 A hungry ghost, wailing, wanting.

80
00:06:45,000 --> 00:06:48,000
 Never satisfied.

81
00:06:48,000 --> 00:06:57,000
 Like in the Christmas Carol, morally, Scrooge's partner,

82
00:06:57,000 --> 00:07:01,000
 who was destined to walk the earth.

83
00:07:01,000 --> 00:07:07,100
 Never satisfied because he was full of greed when he was

84
00:07:07,100 --> 00:07:09,000
 alive.

85
00:07:09,000 --> 00:07:15,000
 Beings are full of delusion.

86
00:07:15,000 --> 00:07:18,000
 Arrogant and conceited.

87
00:07:18,000 --> 00:07:23,000
 Just like cats, cats are so arrogant and conceited.

88
00:07:23,000 --> 00:07:25,000
 There are many of them.

89
00:07:25,000 --> 00:07:32,000
 And dogs are just ignorant and kind of dumb.

90
00:07:32,000 --> 00:07:39,000
 So if we're keen on ignorance and arrogance and conceit

91
00:07:39,000 --> 00:07:44,990
 and all these things, if we're caught up in drugs and

92
00:07:44,990 --> 00:07:46,000
 alcohol,

93
00:07:46,000 --> 00:07:50,000
 deluding and befuddling our minds,

94
00:07:50,000 --> 00:07:54,000
 it's just like being an animal.

95
00:07:54,000 --> 00:08:00,000
 It's like being an animal on a human animal.

96
00:08:00,000 --> 00:08:04,000
 But then there are other humans that are like angels.

97
00:08:04,000 --> 00:08:07,000
 There are other humans that are like gods.

98
00:08:07,000 --> 00:08:11,000
 There are other humans that are like Buddhas.

99
00:08:11,000 --> 00:08:15,000
 These are the ones that we came for.

100
00:08:15,000 --> 00:08:20,490
 There are ways as a human being to be like an angel or a

101
00:08:20,490 --> 00:08:22,000
 god or even a Buddha.

102
00:08:22,000 --> 00:08:28,000
 We may not be a Buddha, but we can be like one.

103
00:08:28,000 --> 00:08:32,000
 We follow the Buddha's example.

104
00:08:32,000 --> 00:08:38,000
 We cultivate mindfulness and clarity and wisdom.

105
00:08:38,000 --> 00:08:40,000
 We have to act.

106
00:08:40,000 --> 00:08:45,000
 We have to be mindful of our position as a human being.

107
00:08:45,000 --> 00:08:49,740
 I've been getting calls lately, and I have to say something

108
00:08:49,740 --> 00:08:50,000
.

109
00:08:50,000 --> 00:08:55,000
 Religion really does strange things to people.

110
00:08:55,000 --> 00:09:02,000
 Spirituality. Sometimes it's just kind of funny,

111
00:09:02,000 --> 00:09:05,560
 because people come up to me all the time and say the wack

112
00:09:05,560 --> 00:09:06,000
iest things

113
00:09:06,000 --> 00:09:13,000
 and the emails I get and now the phone calls I'm getting.

114
00:09:13,000 --> 00:09:17,000
 But sometimes it's...

115
00:09:17,000 --> 00:09:20,000
 You have to be mindful of yourself.

116
00:09:20,000 --> 00:09:25,000
 Someone called me up today, not to get upset or anything,

117
00:09:25,000 --> 00:09:28,000
 but just to talk about this.

118
00:09:28,000 --> 00:09:32,000
 He called me up today and just asked if this was the

119
00:09:32,000 --> 00:09:39,000
 International Meditation Center, I think.

120
00:09:39,000 --> 00:09:44,000
 Without introducing himself, without asking how I was,

121
00:09:44,000 --> 00:09:47,000
 if I've got time to talk, can I answer a question,

122
00:09:47,000 --> 00:09:51,000
 he just started asking me questions about monks

123
00:09:51,000 --> 00:09:53,950
 and why monks weren't keeping the rules and why I'm not

124
00:09:53,950 --> 00:09:58,000
 doing my videos.

125
00:09:58,000 --> 00:10:02,000
 I didn't want to answer him.

126
00:10:02,000 --> 00:10:07,640
 I didn't feel comfortable, because I didn't feel like he

127
00:10:07,640 --> 00:10:09,000
 was mindful.

128
00:10:09,000 --> 00:10:13,000
 That's not how you start a conversation.

129
00:10:13,000 --> 00:10:18,000
 When people come up, when someone comes to you,

130
00:10:18,000 --> 00:10:21,000
 when you approach someone, you should ask how they are,

131
00:10:21,000 --> 00:10:23,000
 you should introduce yourself,

132
00:10:23,000 --> 00:10:26,210
 and you should be mindful of the fact that you don't know

133
00:10:26,210 --> 00:10:27,000
 this person.

134
00:10:27,000 --> 00:10:32,000
 So to ask them deep questions about this,

135
00:10:32,000 --> 00:10:36,000
 not deep, but personal questions really about monastic life

136
00:10:36,000 --> 00:10:39,000
 and why monks this and that,

137
00:10:39,000 --> 00:10:44,000
 I'm not even telling me your name.

138
00:10:44,000 --> 00:10:49,000
 I asked this guy, I said, "Do I know you?"

139
00:10:49,000 --> 00:10:54,000
 Anyway, I thought afterwards, because I didn't answer,

140
00:10:54,000 --> 00:11:00,000
 and I was just quite quiet, and he started laughing at me.

141
00:11:00,000 --> 00:11:10,090
 That's past, that's not the problem. I'm not trying to come

142
00:11:10,090 --> 00:11:11,000
 on your event.

143
00:11:11,000 --> 00:11:16,380
 But I know this kind of attitude, sometimes we're not

144
00:11:16,380 --> 00:11:18,000
 mindful,

145
00:11:18,000 --> 00:11:21,000
 and we have to be mindful.

146
00:11:21,000 --> 00:11:24,000
 I think it's because it's out of our realm.

147
00:11:24,000 --> 00:11:31,540
 We're good at being human in the frame of reference that we

148
00:11:31,540 --> 00:11:32,000
 have.

149
00:11:32,000 --> 00:11:37,950
 So when we go to work or school, we get good at playing the

150
00:11:37,950 --> 00:11:39,000
 game.

151
00:11:39,000 --> 00:11:42,000
 So we get good at saying the right things.

152
00:11:42,000 --> 00:11:47,290
 As a monk, I am proficient at being a monk and proficient

153
00:11:47,290 --> 00:11:48,000
 at it.

154
00:11:48,000 --> 00:11:51,690
 But when you're taken out of your comfort zone, it's

155
00:11:51,690 --> 00:11:53,000
 familiar.

156
00:11:53,000 --> 00:11:58,000
 You can't rely upon habit.

157
00:11:58,000 --> 00:12:02,000
 I think this goes for a lot of situations.

158
00:12:02,000 --> 00:12:06,740
 When you're sick, people can be very irritable and unmind

159
00:12:06,740 --> 00:12:09,000
ful when they're sick,

160
00:12:09,000 --> 00:12:13,000
 because it's a situation that they're not accustomed to.

161
00:12:13,000 --> 00:12:23,000
 When we're surrounded by people or when we're in public.

162
00:12:23,000 --> 00:12:26,000
 This is by the importance of understanding mindfulness.

163
00:12:26,000 --> 00:12:29,030
 It's quite different from habit, it's different from

164
00:12:29,030 --> 00:12:30,000
 concentration.

165
00:12:30,000 --> 00:12:32,000
 It's not something you can build up.

166
00:12:32,000 --> 00:12:35,280
 It's something you have to be... you build it up, but you

167
00:12:35,280 --> 00:12:36,000
 build it as a skill,

168
00:12:36,000 --> 00:12:42,000
 and then you use it. You have to use it to be mindful,

169
00:12:42,000 --> 00:12:48,000
 to remember yourself.

170
00:12:48,000 --> 00:12:52,000
 So let this be a lesson, an interesting lesson,

171
00:12:52,000 --> 00:12:59,000
 acting appropriately when you talk to people,

172
00:12:59,000 --> 00:13:03,000
 when you engage with people.

173
00:13:03,000 --> 00:13:10,000
 This is kind of a thing that's not related to meditation,

174
00:13:10,000 --> 00:13:13,000
 but also very much related to meditation.

175
00:13:13,000 --> 00:13:16,000
 It means how we behave in the world around us.

176
00:13:16,000 --> 00:13:20,000
 The things we do, the things we say.

177
00:13:20,000 --> 00:13:22,000
 It's not meditation.

178
00:13:22,000 --> 00:13:25,000
 And people often ask me questions about,

179
00:13:25,000 --> 00:13:28,000
 "What should I do in this situation? What should I do?

180
00:13:28,000 --> 00:13:35,000
 How should I behave as a human being in this situation?"

181
00:13:35,000 --> 00:13:38,000
 And these are difficult questions to answer here.

182
00:13:38,000 --> 00:13:41,000
 People are expecting a solution to their problems.

183
00:13:41,000 --> 00:13:44,000
 But it's very much...

184
00:13:44,000 --> 00:13:47,350
 So it's unrelated to meditation, but very much related to

185
00:13:47,350 --> 00:13:48,000
 meditation,

186
00:13:48,000 --> 00:13:51,620
 because I can't give you the answer, that's not how it

187
00:13:51,620 --> 00:13:52,000
 works.

188
00:13:52,000 --> 00:14:01,570
 The requirement is a foundation that is capable of dealing

189
00:14:01,570 --> 00:14:03,000
 with your own problems.

190
00:14:03,000 --> 00:14:06,860
 You have to find the solution for yourself, that's the only

191
00:14:06,860 --> 00:14:08,000
 way it works.

192
00:14:08,000 --> 00:14:10,800
 I tell you the answer, and then you go and do this or do

193
00:14:10,800 --> 00:14:11,000
 that,

194
00:14:11,000 --> 00:14:15,000
 it's artificial, you see.

195
00:14:15,000 --> 00:14:17,650
 And whatever you say or whatever you do, it's not from the

196
00:14:17,650 --> 00:14:21,000
 heart, it's not from you.

197
00:14:21,000 --> 00:14:26,000
 So it requires mindfulness as a thing.

198
00:14:26,000 --> 00:14:28,000
 That's why it's so important.

199
00:14:28,000 --> 00:14:33,000
 You want to truly be a human, truly live as a human being.

200
00:14:33,000 --> 00:14:35,000
 You need mindfulness as the day is.

201
00:14:35,000 --> 00:14:39,000
 You always, always come back to mindfulness.

202
00:14:39,000 --> 00:14:43,000
 Or sati, mindfulness may not be the best word.

203
00:14:43,000 --> 00:14:51,000
 Oh, right.

204
00:14:51,000 --> 00:14:53,000
 I was...

205
00:14:53,000 --> 00:14:57,000
 I meditated earlier and then somehow the timer didn't work,

206
00:14:57,000 --> 00:14:58,000
 so I clicked it late.

207
00:14:58,000 --> 00:15:01,000
 So I think it says I'm just finishing meditation now.

208
00:15:01,000 --> 00:15:03,000
 I did my hour.

209
00:15:03,000 --> 00:15:06,270
 If you're wondering why I'm supposed to be meditating when

210
00:15:06,270 --> 00:15:07,000
 I'm doing it.

211
00:15:07,000 --> 00:15:09,000
 Because we've got...

212
00:15:09,000 --> 00:15:12,000
 Everybody signs in for when they're going to meditate.

213
00:15:12,000 --> 00:15:19,000
 I was doing meditation like an hour ago, over an hour ago.

214
00:15:19,000 --> 00:15:24,170
 And then I didn't click for some reason, so after I did

215
00:15:24,170 --> 00:15:26,000
 walking I checked it.

216
00:15:26,000 --> 00:15:28,000
 And then I clicked it.

217
00:15:28,000 --> 00:15:31,000
 So it's a half an hour late.

218
00:15:31,000 --> 00:15:37,000
 I'm not just clicking and then not meditating.

219
00:15:37,000 --> 00:15:42,000
 That's the danger with this.

220
00:15:42,000 --> 00:15:46,000
 I can't tell whether people are actually meditating.

221
00:15:46,000 --> 00:15:50,000
 I don't know why anyone would.

222
00:15:50,000 --> 00:15:52,000
 Anyway.

223
00:15:52,000 --> 00:16:02,000
 So yeah, that's the quote for this evening.

224
00:16:02,000 --> 00:16:05,000
 Let's give a little Pali. Where is the Pali?

225
00:16:05,000 --> 00:16:11,000
 Apaka dei sata ye manusi supachaya yamuki.

226
00:16:11,000 --> 00:16:18,020
 Even so monks, few are those beings that are reborn as

227
00:16:18,020 --> 00:16:19,000
 humans.

228
00:16:19,000 --> 00:16:27,600
 Atokko eitei ye wa bahutara sata ye anyatra manusi pechaya

229
00:16:27,600 --> 00:16:29,000
 jayanti.

230
00:16:29,000 --> 00:16:34,550
 Far more are those beings that are born as something other

231
00:16:34,550 --> 00:16:36,000
 than human.

232
00:16:36,000 --> 00:16:42,000
 Dasmati habikbe ei wang sikitabang. Therefore, O monks,

233
00:16:42,000 --> 00:16:44,000
 here you should train.

234
00:16:44,000 --> 00:16:50,000
 Das apamata wihari sam.

235
00:16:50,000 --> 00:17:01,280
 We will dwell with apamata, apamata, not intoxicated, not

236
00:17:01,280 --> 00:17:03,000
 confused in the mind.

237
00:17:03,000 --> 00:17:06,000
 Mindful basically.

238
00:17:06,000 --> 00:17:08,000
 Ei wan hi woh bhikkhu ei sikitabang.

239
00:17:08,000 --> 00:17:12,000
 Das should you train, obikus.

240
00:17:12,000 --> 00:17:16,000
 This is the sanyurtanikaya.

241
00:17:16,000 --> 00:17:22,000
 Naka sika suta, the fingernail.

242
00:17:22,000 --> 00:17:25,000
 Naka sika wale sika naka.

243
00:17:25,000 --> 00:17:31,000
 Naka is fingernail, sika.

244
00:17:31,000 --> 00:17:34,000
 The top, the tip.

245
00:17:34,000 --> 00:17:40,000
 Naka sika is the tip of the fingernail.

246
00:17:40,000 --> 00:17:46,000
 So that's the dhamma for tonight.

247
00:17:46,000 --> 00:17:52,600
 And if people have questions, I am going to now post the

248
00:17:52,600 --> 00:17:54,000
 hangout.

249
00:17:54,000 --> 00:17:59,000
 So you're welcome to come and ask questions there.

250
00:17:59,000 --> 00:18:02,410
 And I expect anyone who comes on to be mindful and to

251
00:18:02,410 --> 00:18:04,000
 introduce themselves

252
00:18:04,000 --> 00:18:08,990
 and to be respectful and mindful of the sorts of questions

253
00:18:08,990 --> 00:18:11,000
 that should be asked.

254
00:18:11,000 --> 00:18:17,000
 I know that I've scared everyone away.

255
00:18:17,000 --> 00:18:20,000
 No one dares come on, I'm sure.

256
00:18:20,000 --> 00:18:26,000
 It's just strange that people...

257
00:18:26,000 --> 00:18:30,400
 There's a certain familiarity where people would just come

258
00:18:30,400 --> 00:18:34,000
 up to me and start talking.

259
00:18:34,000 --> 00:18:38,790
 Someone at university, I think I mentioned this, came up to

260
00:18:38,790 --> 00:18:42,000
 me and said,

261
00:18:42,000 --> 00:18:45,000
 "Look, you can't walk around wearing that kind of clothes

262
00:18:45,000 --> 00:18:48,000
 and not have people want to come up and ask you about it

263
00:18:48,000 --> 00:18:51,000
 and expect people to not come up and ask you."

264
00:18:51,000 --> 00:18:58,000
 And I turned to him and said, "Why not?"

265
00:18:58,000 --> 00:19:01,000
 Mind your own business.

266
00:19:01,000 --> 00:19:03,000
 That guy turned out to be quite nice.

267
00:19:03,000 --> 00:19:09,000
 He was actually quite respectful.

268
00:19:09,000 --> 00:19:14,000
 Recently another person called us from a radio station

269
00:19:14,000 --> 00:19:17,000
 and they want to come by tomorrow.

270
00:19:17,000 --> 00:19:20,000
 They're going to come by at 11 tomorrow.

271
00:19:20,000 --> 00:19:24,000
 Which is bad timing, that's right, when we're eating.

272
00:19:24,000 --> 00:19:29,000
 And they want to ask us about our program.

273
00:19:29,000 --> 00:19:33,000
 Basically to give us free publicity, which is awesome.

274
00:19:33,000 --> 00:19:37,000
 Very kind of.

275
00:19:37,000 --> 00:19:41,000
 So people are starting to find out about us.

276
00:19:41,000 --> 00:19:44,000
 They just have to be ready for it.

277
00:19:44,000 --> 00:19:46,000
 And I think I have to start screening my calls

278
00:19:46,000 --> 00:19:50,000
 because this person wasn't very happy with me in the end.

279
00:19:50,000 --> 00:19:52,000
 I don't think he wasn't answering.

280
00:19:52,000 --> 00:19:57,000
 You see, it's common for, it was common,

281
00:19:57,000 --> 00:20:02,380
 there's stories of monks, arahants even, who wouldn't

282
00:20:02,380 --> 00:20:04,000
 answer questions.

283
00:20:04,000 --> 00:20:06,000
 They just wouldn't say anything.

284
00:20:06,000 --> 00:20:08,330
 And a question, or when someone was saying something to

285
00:20:08,330 --> 00:20:09,000
 them,

286
00:20:09,000 --> 00:20:12,000
 that just wasn't...

287
00:20:12,000 --> 00:20:15,000
 It's because of indifference.

288
00:20:15,000 --> 00:20:18,000
 Monks are not like the Buddha.

289
00:20:18,000 --> 00:20:21,000
 So I've done this before.

290
00:20:21,000 --> 00:20:24,680
 And people can be very upset at you for not answering their

291
00:20:24,680 --> 00:20:25,000
 questions

292
00:20:25,000 --> 00:20:29,000
 because they're so used to, I mean, social...

293
00:20:29,000 --> 00:20:33,000
 The social way is to have someone,

294
00:20:33,000 --> 00:20:37,000
 when you talk to someone you should answer them.

295
00:20:37,000 --> 00:20:41,000
 And so this person was saying today,

296
00:20:41,000 --> 00:20:46,000
 it's just an interesting point, he was saying,

297
00:20:46,000 --> 00:20:50,400
 I've talked to a lot of people and I've never had someone

298
00:20:50,400 --> 00:20:51,000
 not answer,

299
00:20:51,000 --> 00:20:54,000
 just not say anything.

300
00:20:54,000 --> 00:20:57,000
 And I didn't reply to that.

301
00:20:57,000 --> 00:21:01,540
 And then he said, I'm not sure it's what the Buddha would

302
00:21:01,540 --> 00:21:03,000
 have done.

303
00:21:03,000 --> 00:21:06,000
 Basically, he would have wanted or something,

304
00:21:06,000 --> 00:21:10,990
 basically criticizing and saying, this is what you're doing

305
00:21:10,990 --> 00:21:12,000
 is wrong.

306
00:21:12,000 --> 00:21:14,000
 But the funny thing is, why do I care?

307
00:21:14,000 --> 00:21:18,000
 Why does it matter to me what you think? It doesn't.

308
00:21:18,000 --> 00:21:20,000
 I mean, it's something about being a monk,

309
00:21:20,000 --> 00:21:24,000
 that we're very much outside of this.

310
00:21:24,000 --> 00:21:27,000
 I didn't come to you, you called me.

311
00:21:27,000 --> 00:21:31,790
 So it's an interesting point for me as a monk to be in this

312
00:21:31,790 --> 00:21:33,000
 sort of situation.

313
00:21:33,000 --> 00:21:38,000
 But one interesting aspect of it is the power that we have.

314
00:21:38,000 --> 00:21:41,000
 We're not required to...

315
00:21:41,000 --> 00:21:46,000
 You know how sometimes you play into other people's games?

316
00:21:46,000 --> 00:21:49,000
 Bait, you know, bait and switch.

317
00:21:49,000 --> 00:21:52,000
 You ever heard of this idea of bait and switch,

318
00:21:52,000 --> 00:21:55,410
 where someone baits you with something and then if you get

319
00:21:55,410 --> 00:21:57,000
 into it with them,

320
00:21:57,000 --> 00:22:02,000
 then they start on something else and they drag you on and

321
00:22:02,000 --> 00:22:02,000
 on.

322
00:22:02,000 --> 00:22:06,000
 And into something that's often quite unwholesome and

323
00:22:06,000 --> 00:22:10,000
 useless.

324
00:22:10,000 --> 00:22:17,550
 And we don't have the responsibility to play other people's

325
00:22:17,550 --> 00:22:18,000
 games,

326
00:22:18,000 --> 00:22:21,000
 I guess is the general point that I'm making.

327
00:22:21,000 --> 00:22:23,000
 That's interesting to me.

328
00:22:23,000 --> 00:22:28,000
 I think as a monk, I have that luxury not having to play

329
00:22:28,000 --> 00:22:31,000
 into people's games.

330
00:22:31,000 --> 00:22:35,000
 And it's really a power of having left the world,

331
00:22:35,000 --> 00:22:39,000
 because there's nothing you can do to me.

332
00:22:39,000 --> 00:22:42,000
 I'm happy to help.

333
00:22:42,000 --> 00:22:47,110
 I tell a told this guy, if you want to learn meditation, I

334
00:22:47,110 --> 00:22:49,000
'm happy to help.

335
00:22:49,000 --> 00:22:53,000
 That was really interesting.

336
00:22:53,000 --> 00:22:56,000
 Anyway.

337
00:22:56,000 --> 00:23:04,000
 We have a great power in mindfulness,

338
00:23:04,000 --> 00:23:17,700
 in not worrying, not clinging, not needing anything from

339
00:23:17,700 --> 00:23:22,000
 other people.

340
00:23:22,000 --> 00:23:24,000
 Anyway, enough talk.

341
00:23:24,000 --> 00:23:26,000
 Thank you all.

342
00:23:26,000 --> 00:23:28,000
 I guess there's no questions.

343
00:23:28,000 --> 00:23:30,000
 That's all for this evening.

344
00:23:30,000 --> 00:23:33,000
 I'm showing you all good practice and good night.

345
00:23:34,000 --> 00:23:35,000
 Thank you.

346
00:23:36,000 --> 00:23:37,000
 [

