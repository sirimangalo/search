 What is your view on having sex as a Buddhist? Are you, for
 example, allowed to have multiple sexual relationships?
 I put the emphasis wrong. I'm not allowed to have any
 sexual relationships, but "are you" means in general, right
?
 So in general, a lay person, put it in general because sens
ual desire is evil. It's an evil.
 This is Buddhist word evil, no? Because the meaning of the
 word evil is something that is going to lead you to suffer.
 Now, the core of Buddhism is that desire leads you to
 suffer.
 Wanting something leads you to try to get it. When you try
 to get it, it creates... it changes your life.
 We want to get something, so you have to work for it, and
 that changes your whole universe in some small way.
 And that's going to create more stress. It's going to
 create more dissatisfaction and suffering.
 So undeniably, it's a bad thing. But the question is, how
 bad? Is it really going to get in the way of your
 meditation practice?
 No, probably not. Excessive sex or multiple sexual
 relationships can get in you.
 Whether it's with one person or with many people, in fact,
 many people would be more deleterious for your practice.
 It's not a horrible thing in Buddhism to have multiple
 sexual relationships.
 The problem comes where you are deceiving people. This is
 where it gets really bad, and where it gets really evil
 when you start deceiving people.
 And that's where it really cuts your practice and starts
 heading you in the opposite direction.
 It doesn't just slow you down, it starts heading you in the
 wrong direction when you're sleeping around.
 When you have a monogamous relationship and are committed
 to it and then are cheating on that person,
 or someone else is in a monogamous relationship and you're
 sleeping with them.
 This kind of thing, this is where it really gets trouble.
 If you're in a polygamous relationship, this is not a
 corrupt evil in Buddhism.
 I don't know. I've thought about this a lot. You look at it
, it's really what never was seen as such.
 I don't know where it became, but it's kind of this
 Protestantism that turned it into such a horrible thing to
 have a polygamous relationship.
 If anything, it seems to be what God designed us for. One
 man, many women.
 Look at nature. It's not always, but with monkeys, for
 example, chimpanzees, it's very common to see polygamous
 relationships.
 We see it all the time in the forest. One big monkey and he
's going around and he's got all these women.
 The problem isn't that it's a matter of it's in a category
 of its own. It's not in a category. It's just fairly
 extreme.
 The more sex and the more diverse sex you have, the more it
's going to get in the way of your practice.
 It hasn't crossed the line of being immoral in the sense of
 leading you backwards yet. It's not leading you in the
 wrong.
 It is, but it can still be worked out through meditation.
 If you start cheating on people, if you start lying and
 deceiving people,
 breaking up relationships, that makes your meditation very,
 very difficult in a very different way.
 So that's categorically different. This has to be
 understood. But it has to also be understood.
 I don't want you to say that polygamy is fine. Go for it.
 It's really going to get in your way and it's going to make
 you no sex in general.
 But for sure, promiscuity and polygamy will for sure drive
 you crazy. Your mind will become more and more attached to
 it.
 The best thing is to give it up and start looking at the
 sexual desire. Give it up. Be celibate and start looking at
 the celibate.
 Looking at the desire. If you happen to break your celibacy
, just don't become a monk yet. Start looking at it.
 Try to be celibate for a while. See what happens. Watch the
 sexual urges and really learn about them.
 Take it as a laboratory experiment. It's much more
 conducive. There's no reason to be promiscuous. There's no
 reason to be polygamous.
 It doesn't do you any good. It doesn't make you more happy.
 What good is it? It just makes you less satisfied.
 It doesn't make you a better person. It doesn't make you a
 happier person. It doesn't make you more content.
 What good is it? It's better to go the other direction.
