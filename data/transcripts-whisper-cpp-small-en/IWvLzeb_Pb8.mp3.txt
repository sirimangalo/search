 Okay, so this is a question that's fairly practical.
 What did the Buddha, and what do you or other teachers
 recommend in order to generate greater motivation to
 practice when one's dedication and enthusiasm is waning?
 I think the biggest thing that the Buddha recommended...
 No, I don't even know if it's the Buddha...
 The biggest thing that I can think of, and it's actually
 from the commentary, is to associate yourself with people
 who have the qualities that you lack.
 I mean, this is based very much on the Buddha's famous sut
ta where he tells Ananda that living with association with
 good people is the entirety of the holy life, the entirety
 of the spiritual life.
 Ananda says he figures it's about half, and the Buddha says
, "Shaman, you don't say that, it's the entirety of it."
 So, association and...
 That doesn't just mean talking with them, but even hanging
 around such people will lead you to cultivate the same
 sorts of states.
 So, what do I do with my students? I mean, you encourage
 them.
 It often depends very much on what's getting in their way.
 It can be doubts. If doubts are getting in their way, then
 they need an explanation.
 I mean, or sorry, if it's intellectual doubts, then they
 need an explanation.
 Or they need an answer, something that at least tells them,
 that shows them that their intellectual pursuits are
 useless and unbeneficial.
 If it's doubt about themselves, or feelings of discour
agement, doubt about their practice, like, "My practice is
 not going well," or, "I'm useless, I'm not good at this,"
 then they need some reassurance, and they need an argument,
 you know, to...
 A way of helping them to see things in another way.
 Discussion is very important.
 If, or often just a Dhamma talk, giving them some kind of
 encouragement.
 So, if they feel discouraged in the sense, like, they aren
't so interested in meditation, then you have to give them a
 talk explaining about the things that they are interested
 in,
 and showing that those things are perhaps not as meaningful
 as they thought they were, and talking about the importance
 of meditation.
 So, Dhamma talks can help, you know, talks about, you know,
 the benefits of renouncing and giving up, and the
 disadvantages of clinging,
 talks that are encouraging, discussion that is encouraging.
 You know, the best thing, of course, is to meditate,
 because meditation helps you want to meditate more.
 The more you meditate, the more you become interested in it
, the more benefit you see from it.
 Anybody else? What do you think motivates people? What do
 we do to motivate ourselves and motivate others?
 What do you do when you don't want to practice?
 I think, like, when it comes to sloth and torpor rising
 into mind, a couple of simple things that you can do to
 overcome those, to give yourself a little bit more
 motivation.
 Like, go out into cold air, some water on your face, some
 changing positions during meditation, or if you're not
 sitting formally, you can just put your awareness on the
 tiredness, on the lazy feeling,
 on the feeling of not wanting to meditate, things like that
.
 Yeah, I don't know so much about, you know, if you're just
 tired, because people can really want to meditate even
 though they're tired, but it can be very discouraging.
 And I think it's more the discouragement that's being asked
 over here.
 You know, how do you deal with being discouraged? The best
 thing is to have a teacher and to have a community, because
 when you do, all of these things will come,
 you'll have talks, you'll have discussions and so on.
 But one interesting thing about what you're talking about
 is tricks, right?
 Sometimes you have tricks to make yourself more motivated.
 Vayak actually hears commenting about doing Metta
 meditation, which is, yeah, another good trick.
 Do something to give you encouragement. Think about the
 Buddha.
 We talked about this yesterday, right? Something that's
 very encouraging is to think about the Buddha.
 The Buddha said, "Whenever you feel afraid, you're off in
 the forest and suddenly you feel afraid."
 Say to yourself, "Ithipisova Bhagavat," think about the
 Buddha and the fear will go away immediately, and it does.
 It's quite impressive how the thinking about the Buddha can
 make you fearless, because you start realizing how fearless
, remembering how fearless you are.
 Okay, if anybody wants to pick through the comments, wait,
 stop this one.
