1
00:00:00,000 --> 00:00:07,000
 Good evening everyone.

2
00:00:07,000 --> 00:00:16,000
 Podcasting live January 21st.

3
00:00:16,000 --> 00:00:26,070
 So I know Thursday I'm supposed to be doing something

4
00:00:26,070 --> 00:00:28,000
 interesting.

5
00:00:28,000 --> 00:00:32,000
 I have video I think.

6
00:00:32,000 --> 00:00:40,000
 But I've just spent the day, pretty busy day today.

7
00:00:40,000 --> 00:00:44,000
 So I think I'm not going to be doing a video.

8
00:00:44,000 --> 00:00:48,000
 And I've got these TEDx talk coming up.

9
00:00:48,000 --> 00:00:57,560
 So maybe scaling back some of my activities for the next

10
00:00:57,560 --> 00:00:59,000
 week.

11
00:00:59,000 --> 00:01:03,030
 So next week I have an interview that I have to prepare for

12
00:01:03,030 --> 00:01:05,000
 this TED talk.

13
00:01:05,000 --> 00:01:09,000
 TEDx talk.

14
00:01:09,000 --> 00:01:12,000
 The talk isn't even that big of a deal to me.

15
00:01:12,000 --> 00:01:19,000
 To me it's more about me and my work than about the Dhamma.

16
00:01:19,000 --> 00:01:24,000
 So it's not like it's going to be a teaching experience.

17
00:01:24,000 --> 00:01:28,000
 The talk I'm going to be giving is about...

18
00:01:28,000 --> 00:01:31,610
 There's some of the interesting things I've found about

19
00:01:31,610 --> 00:01:35,000
 using the internet and social media

20
00:01:35,000 --> 00:01:39,250
 and modern technology to spread the ancient teachings of

21
00:01:39,250 --> 00:01:40,000
 the Buddha.

22
00:01:40,000 --> 00:01:48,000
 So juxtaposition of ancient and modern.

23
00:01:48,000 --> 00:01:53,000
 Anyway that's what I thought to talk to them about.

24
00:01:53,000 --> 00:01:57,000
 That's what I'll be talking to them about.

25
00:01:57,000 --> 00:02:06,000
 If they let me, it's not.

26
00:02:06,000 --> 00:02:10,000
 Any questions here?

27
00:02:10,000 --> 00:02:13,630
 At the end I can see the cause, at one end I can see the

28
00:02:13,630 --> 00:02:20,000
 cause and effect of my thoughts, actions.

29
00:02:20,000 --> 00:02:23,720
 At the other end it's not really so much up to me to really

30
00:02:23,720 --> 00:02:26,000
 change this habit or cycle.

31
00:02:26,000 --> 00:02:30,000
 It's truly frustrating.

32
00:02:30,000 --> 00:02:37,000
 Non-self can be frustrating when you have the idea of self.

33
00:02:37,000 --> 00:02:43,000
 The idea of control.

34
00:02:43,000 --> 00:02:46,000
 So what you're seeing is impermanent suffering in non-self.

35
00:02:46,000 --> 00:02:50,000
 You should watch my video on...

36
00:02:50,000 --> 00:02:55,000
 I think it's called...

37
00:02:55,000 --> 00:03:02,000
 I remember what it's called. Experience of a reality maybe?

38
00:03:02,000 --> 00:03:05,000
 So seeing the three characteristics is tough.

39
00:03:05,000 --> 00:03:11,000
 It's not comfortable.

40
00:03:11,000 --> 00:03:17,000
 Seeing that you're not in control makes you frustrated.

41
00:03:17,000 --> 00:03:20,380
 The real answer to your question, the simple answer to your

42
00:03:20,380 --> 00:03:22,000
 question is...

43
00:03:22,000 --> 00:03:38,000
 You should note to yourself frustrated, frustrated.

44
00:03:38,000 --> 00:03:46,000
 Seeing that you're helpless to change is a good lesson.

45
00:03:46,000 --> 00:03:50,000
 And these aren't easy questions. They're not easy issues.

46
00:03:50,000 --> 00:03:52,000
 They're not going to be solved in a day.

47
00:03:52,000 --> 00:03:55,000
 You solve them a lot better when you do intensive practice.

48
00:03:55,000 --> 00:03:59,080
 But if you're practicing just on a daily basis, it takes

49
00:03:59,080 --> 00:04:00,000
 time.

50
00:04:00,000 --> 00:04:02,000
 It can take lifetimes.

51
00:04:02,000 --> 00:04:08,390
 It took the Buddha four uncountable periods of time, plus

52
00:04:08,390 --> 00:04:10,000
 100,000 eons,

53
00:04:10,000 --> 00:04:33,560
 which are relatively short compared to the four uncountable

54
00:04:33,560 --> 00:04:35,000
.

55
00:04:35,000 --> 00:04:40,000
 Oh, and here's the thing. So Saturday...

56
00:04:40,000 --> 00:04:44,000
 Saturday I'm giving... Let's see. When am I giving a talk?

57
00:04:44,000 --> 00:04:49,000
 Saturday I'm giving a talk in Second Life.

58
00:04:49,000 --> 00:04:59,000
 Let's see.

59
00:04:59,000 --> 00:05:05,000
 I don't know what we didn't agree on.

60
00:05:05,000 --> 00:05:07,730
 Well, we didn't agree, but it sounds like I'll be giving a

61
00:05:07,730 --> 00:05:12,000
 talk at 12 p.m.

62
00:05:12,000 --> 00:05:16,000
 which is 12 noon, Second Life time.

63
00:05:16,000 --> 00:05:34,000
 Second Life...

