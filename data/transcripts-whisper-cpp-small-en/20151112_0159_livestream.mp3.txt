 Okay, good evening everyone.
 We're broadcasting live November 11th.
 So it's an especially Buddhist day today.
 Or is that only in Canada?
 Is it a special day in America as well?
 Today's Veterans Day in America.
 Oh, you call it Veterans Day.
 Well, we have a better name for it, a very Buddhist name.
 Today is Remembrance Day.
 Oh, okay.
 I mean, it's to remember veterans.
 But yeah, probably shouldn't, shouldn't steal it.
 But it's in remembrance is a good word.
 It is good to remember, not to forget.
 And on Buddhism, it has a bit of a different connotation.
 Okay, we're doing Dhammapada.
 I'm going to have to turn some stuff on.
 Test.
 Mic is good.
 Using the old mic, the one that failed on me before.
 Because the new one's failing now.
 Tomorrow we're going to have a different layout.
 I think we're going to move into the outer room,
 which has a nicer Robbins idea,
 that we move into the outer room.
 There's something on the lens.
 You guys see that?
 The outer room has a nicer atmosphere.
 So we don't have to use this fake white.
 Okay, ready.
 Hello, everyone, and welcome back to our study
 of the Dhammapada.
 Today we continue on with verse number 108,
 which reads as follows.
 Yankinciitang vahutang valloke,
 sangvacharang yajita punyapeko,
 sabampitang nachatubhagameti,
 abhivadana ujukate suseyo.
 Which means it's actually a bit difficult
 because the grammar doesn't really work in English.
 So whatever sacrifice or worship there is in the world,
 if for 100 years sangvacharang yajita punyapeko,
 one desiring punya, desiring merit,
 should perform such a sacrifice or such a worship.
 Yes, it's actually a very, very similar verse and story
 to the last two.
 Sabampitang nachatubhagameti,
 it doesn't come to a quarter of the value.
 All of that doesn't come to a quarter of the value.
 The 100 years of sacrifice or worship that one pays.
 Abhivadana ujukate suseyo.
 For the greater, in comparison to the greater,
 abhivadana or respect or reverence that one pays to.
 And today our word is ujugatta.
 In regards to those who are ujugatta,
 who have gone to or become straight,
 those who have gained rectitude, moral or ethical or mental
 straightness,
 uprightness, those who are upright.
 So same story, we have Sariputta.
 And it's becoming apparent that he was really instrumental
 in helping a lot of his relatives
 and now his friends become enlightened.
 So now we have Sariputta's friend, Sahayaka Brahmana.
 And Brahman, who was a friend of Sariputta,
 friend of Sariputta's from before.
 And so Sariputta went to him and asked him,
 "Do you do any good deeds?"
 And he says, "Oh yes, what do you do?
 Oh, I offer sacrificial slaughter."
 And of course, Sariputta doesn't see that as being a wholes
ome deed.
 This is about really the crux of these three stories,
 is the idea that good deeds,
 our concept of good deeds is often quite misled.
 And this even occurs in Buddhism.
 You'll see Buddhists performing deeds that they think are
 good deeds
 when in fact they are perhaps useless.
 So the case of...
 I've told this story before, but once we had a katina
 ceremony
 where I was staying.
 And all the villagers came together to do the katina
 ceremony.
 And so what they did is they had this very important...
 I was in a rural area.
 It was an area where people didn't have a lot of money.
 And there was one man who had built a house in the area
 from Bangkok,
 and he was quite rich.
 And so he got his rich friends from Bangkok to come up.
 And they put together a bunch of money,
 and they put the money up on a tree,
 and they brought this money tree to the monastery.
 And they wanted to do a katina ceremony.
 Well, it turns out half of them were drunk.
 They had been drinking.
 And the funniest part was, and what sort of made it a bit
 ridiculous,
 was that they didn't have a single robe with which to do
 the katina.
 So they brought this money tree thinking, "Well, that's kat
ina.
 Katina is a money tree because that's really what it's
 become.
 It's all about money trees.
 Trees that have...are literally made of money.
 The leaves are folded up bits of money.
 Or just money sticking up on sticks."
 And so I said, "OK, we're ready to do katina.
 What do we do?"
 And I said, "Well, where's the robe?"
 And they didn't have a robe.
 So I said, "Oh, well, why can we borrow one of yours?"
 And I said, "OK."
 I took off.
 I actually took off.
 Anyway, I don't want to talk about it.
 I'm sure it wasn't...
 Yeah, you know.
 Point being, not really what I would call a wholesome
 engagement,
 especially considering the alcohol involved.
 And the fact that after it was all over, they took the
 money back.
 And on some of the...half of the gifts that were supposed
 to go to the monastery,
 they took back for the village.
 It was really...
 It was a bit of a system that they had set up.
 And yeah, this happens in Buddhist circles sometimes,
 where we lose sight of, "Why are we doing this?"
 I think while we're doing it, often they're doing it just
 because
 they're excited to have their village and their community
 grow.
 So the money goes to the community, I think, or...
 The stuff goes for the community.
 And they don't realize the great benefit to actually giving
 a gift.
 And in fact, they aren't really interested in giving gifts.
 On the other hand, that very same village ended up being
 quite awesome about giving alms.
 They had another funny aspect of being there.
 I was staying with an old monk, two old monks.
 So this was later when I was staying with a second old monk
,
 but I was staying with a first old monk deeper in the
 forest.
 And he told me when he said, "Well, you can go on alms
 round to the village,
 but you won't get anything.
 You might get some dried noodles or canned fish, but that's
 about it.
 It won't be enough to eat, but you can go."
 And it was three and a half kilometers walk along a dirt
 gravel road
 that was actually somewhat painful to walk.
 But I did walk three and a half kilometers
 and often had to walk three and a half kilometers back, but
 not always.
 Sometimes someone going to the park, there was a national
 park near where the monastery was,
 would give us a ride back or give me or eventually us when
 I had followers.
 But the great thing, once I went and they could see that I
 was interested in meditation
 and wasn't some crabby old monk, they were really keen on.
 And eventually the whole village began to give
 and they really got this sense of the greatness.
 People who I think had never really gotten into it.
 And there was one man who was a really devout Buddhist and
 very interested in the Dhamma
 and very knowledgeable about the Dhamma.
 And we used to talk about the Dhamma and he was really
 impressed.
 And he said, "You know, this is a great thing that people
 who have never seen these people give before are now into
 giving."
 So there was that.
 But that's really what it's about.
 It's the difference, this verses are about the difference
 between spiritual practices.
 We have this idea of spiritual practice in many Buddhist
 cultures of paying respect to
 gods or angels and they've started making up angels.
 We've taken them from Hindu myths like Ganesha, people
 worship this monkey, no sorry, the elephant,
 god and they have all these myths about Ganesha and Hanuman
.
 In this same village, there was a woman who claimed to be
 an avatar.
 An avatar is a big thing in Thailand, in Thailand anyway.
 An avatar for Hanuman.
 And of course Hanuman is this legend, it's a story really.
 I mean there's not even any ancient religious texts I think
 that have Hanuman in them.
 It's this tale of the Ramayana which is a fairly modern
 tale.
 And yet in India as well they worship Hanuman or they seem
 to.
 They have monkeys, monkey statues in front of their fields
 to ward away evil demons and
 stuff.
 But yeah these sort of things not worth a quarter of the,
 he says, not a fourth part.
 Not Chaturbhaagamiti, Chaturbhaagamiti.
 They don't come to a quarter, they don't even come to a
 thousandth part as we were talking
 about in the earlier ones.
 Anyway so he tells him this, it's a little bit different.
 He takes him, he says, he says look you have to come to see
 the teacher.
 And he takes him to see the teacher and this time he
 actually says, Bante please tell this
 man the way to the Brahman world.
 And the Buddha, but the Buddha says the same thing.
 The Buddha asks him what he does and he says you could do
 that for a year and yet it would
 not be worth the smallest piece of offering.
 And here he says something also a bit different.
 Dokhyamahajanasa dinadhanam.
 So if you were to give, if you were instead to give to the
 populace, if you were instead
 to give to poor people for example, that would be, that was
 one thing would be of greater
 value.
 So he distinguishes there and it's an interesting point
 because here the Buddha is sort of seems
 to be advocating giving of alms.
 I'd have to, that's what the English translation says and
 that looks like what the Pali says,
 but it's something to do with giving gifts to worldly
 people.
 It's much better because here he's slaughtering animals,
 which of course is actually unwholesome.
 But then he goes on to saying to pay respect to my
 disciples.
 The Khusla Jaitana, the wholesome mind that comes is far
 more powerful.
 I mean, I think he's quite understating the truth here
 because honestly sacrificing animals
 is not only a portion, it's not only a fraction as good, it
's actually harmful.
 There's nothing good about offering slaughtered animals as
 an offering.
 So the Buddha is I think being a little bit, going a little
 bit easy on him because probably
 if he were to say, you're actually, that's actually an unwh
olesome thing, then it would
 perhaps set the guy off and make him upset at the Buddha.
 So he couches it a bit nicer.
 He doesn't say how much less good it is than a quarter, but
 it's at least not, it's less
 than a quarter as good.
 It's actually harmful and that's true of many spiritual
 practices.
 So again, as I've talked about, we have to be aware of the
 true benefit of our spiritual
 practices.
 We can't just think I'm doing this and this is what people
 tell me to do, this is spiritual
 and therefore it's good.
 Spiritual practice can actually be harmful.
 This was a spiritual practice that obviously was, offering
 killing animals in the name
 of God, that kind of thing.
 Sometimes we take people who have the idea that taking
 drugs is spiritual and I'm not
 going to go out and say that that's harmful, but my
 suspicion is that it has a great potential
 for harm if not being outright harmful.
 I mean, I think a lot of people would say that taking even
 psychedelic drugs and hoping
 that it somehow is beneficial as a spiritual practice.
 I think people, some people would say that that is outright
 harmful.
 I'm not sure that I'd go quite so far because there is an
 argument to be made that it opens
 up your mind to alternate states of reality, but I think
 there's a lot of delusion involved.
 Like the idea that those states have some meaning or
 purpose or value when in fact they're
 all messed up with delusion.
 I've been there, done that and it's just all a mess of our
 mind, what our mind can come
 up with when it's doped up and it's high.
 So, these kinds of things and you compare that, for example
, taking drugs as a spiritual
 practice to a Buddhist spiritual practice of like a giving
 charity or of even just holding
 your hands up and paying respect to someone who's worthy of
 respect.
 Who would think of that?
 It shows how far we, many people are from true spiritual
 practice.
 Many people say, "Oh, taking drugs, that's a spiritual
 practice," or doing expansive
 rituals or offering copious sacrifices or this kind of
 thing.
 Doing very complex or very mystical things, maybe dancing,
 people say.
 In the West we become very hedonistic with our spirituality
.
 So, maybe people say group orgies and karmic sex and that
 kind of thing.
 These are spiritual.
 I can contrast that to the Buddha's idea of what true
 spirituality is, holding your hands
 up and paying respect.
 That is far more spiritual.
 It's such a mundane seeming thing, but far more spiritual
 and far more beneficial, even
 just for a moment than a hundred years of dancing,
 spiritual dancing or spiritual music
 or spiritual sex, drugs, all these things.
 So, for someone looking for punya, which we all are, punya
 is goodness.
 Even if we're just set on meditation, it's important not to
 become self-centered in the
 sense of, "Me, me, me, I want to become spiritually
 enlightened."
 Enlightenment is about letting go, so it's a lot of self-
sacrifice.
 An enlightened being will be able to give up anything.
 If someone asks them for something, they would have no
 sense of self, of helping themselves.
 They have no qualms there.
 They consider what is proper to do and they do it without
 any attachment, without any
 greed.
 They're very respectful.
 They honor those who have helped them.
 They're grateful to those who have helped them, that kind
 of thing.
 And they're very down to earth.
 So noble spirituality is like this.
 With spiritual wealth, paying respect to those who are
 worthy of respect, giving gifts to
 those who are worthy of gifts, and mostly just being aware
 of being here, being present,
 being with mundane reality, understanding truth, true
 reality, and not being concerned
 with mysticism or altered states of existence or that kind
 of thing.
 Astral travel, anyone who's obsessed with astral travel, I
 mean, it's not to say enlightened
 people can't practice it, but people who are striving for
 that are really missing the point.
 Mahasya Sayadaw tells the story of a woman who he knew who
 was working very hard to try
 and see out of her ears.
 This was she was trying to gain the spiritual ability to
 see out of her ears.
 And he said, "And this is when her eyes worked perfectly
 fine."
 So we get often on the wrong path.
 True spirituality is, heart is often easy to mistake for
 ordinary reality.
 I always say that enlightened people are what we think we
 are.
 We all think of ourselves as living normally.
 We think of ourselves as normal.
 But we're not.
 Our defilements take us away from what we think we are,
 take us away from mundane reality.
 So I've made much of this very simple verse, which is very
 much like the other two verses.
 And I promise the next one is different.
 Still along the same vein, but it's a totally different
 context and story, everything.
 And it's a very, actually one of them, probably the, if not
 one of the, if not the most famous
 verses in the Dhammapada coming up next time.
 And then after that we have the story of Sankhicha, which
 is a very nice story and so on and so
 on.
 Not all the stories are long.
 Some of them are very short, as you can see.
 Some of them are very long.
 I think the longest ones have already gone by, so we're not
 going to have great storytelling.
 Not all of it is going to be great stories.
 But always something new and some new message to be found
 in the Dhammapada.
 And of course the verses themselves are things you can
 reflect upon.
 What is important?
 This is the Sahaso-waga, which is thousands.
 So it's contrasting single things to thousands of things
 for the most part.
 Thousand of something useless, better is one thing that is
 useful.
 So that's the Dhammapada for tonight.
 Thank you all for tuning in.
 Keep practicing and be well.
 Okay, hopefully that turned out.
 Now I have to turn some things off.
 Hmm.
 Looks like I had it on mute.
 Just asked me if I wanted to turn mute off.
 I wonder if I had it on mute the whole time.
 Wouldn't that be funny?
 No, the comments from the chat panel are that the audio is
 great, but the camera did go
 out of focus a little bit on the YouTube.
 That audio is different from this audio.
 The audio I was using, I don't think it was on mute, but
 that would be funny if it was.
 You know what I could do?
 For the Dhammapada portion, I don't really have to record a
 video.
 We could just do the audio stream for that part and start
 doing the live hangout after
 I do the Dhammapada.
 Because I mean, why record?
 Then otherwise I have it recorded on YouTube twice.
 It'd be nice to have the videos be entirely different,
 right?
 Would you say that again, Monty?
 Yeah, just do the audio stream during the Dhammapada so
 people could listen if they wanted.
 If they want to watch, they can wait.
 Then we could put the...
 If you could get the...
 Do you see the link in the bottom right corner?
 It says links?
 No, that's only on the page of the person who creates the
 hangout.
 So I can put the link in the...
 When people know to go there.
 Maybe we'll think about doing that in the future.
 Am I echoing you?
 I don't think so.
 Okay.
 Then I don't need you loud so I can even turn you down
 because that's not coming anymore.
 So camera's okay?
 A little bit out of focus?
 That's a little blurry.
 It kind of goes in and out.
 That's okay.
 Questions?
 Do we have questions?
 We have questions, yes.
 Why do monks chant?
 I ask because I have recently been shown a breathing
 technique where you breathe in through
 your nose and then exhale through your mouth making an ah
 sound.
 This technique has been scientifically proven to benefit
 the nervous system.
 How exactly I don't know but it got me thinking about monks
 chanting.
 Has the West finally caught up with what the East has known
 for centuries?
 Can you repeat that?
 It's been scientifically what?
 Scientifically proven to benefit the nervous system.
 Okay, right away I know there's a problem.
 Science never ever ever proves anything.
 Science can't prove anything.
 It can only disprove.
 Why I say it because you have to be careful.
 I don't think this is quite your question but be very
 careful when you believe claims
 that someone says something is scientifically proven.
 You can provide evidence in favor of something but you can
't ever prove it.
 So you can show what appears to be a link and usually
 science involves taking lots of
 people and putting them through a trial and measuring the
 benefits.
 So then you have questions about how it was performed,
 whether it was a double blind,
 so whether there was bias involved or whether there was
 some sort of a... because you know
 studies show that placebos are beneficial to people.
 Placebo meaning medicine that is just sugar.
 It's not medicine.
 But when it's given to people, so as a depression
 medication for example, placebos work, I think
 they work as good as drugs or something, sometimes even
 better.
 No, placebos don't put noxibos to them.
 Placebos being non-drugs, fake drugs that also have
 unpleasant side effects.
 So if a drug had unpleasant side effects, it actually did
 perform better I think than
 certain drugs or something.
 I mean just ridiculous studies that showed that a noxibos
 performed better than the actual
 depression drugs.
 I guess people thought they were taking real drugs.
 So the question is how much of a benefit and how reliable
 is it to say that it's just beneficial.
 That being said, your question is different, right?
 Your question is why do monks do chanting?
 How you relate it exactly to your... can you clarify that
 for me Robin?
 How is it related to this idea of saying "ah" or something?
 Yes, he was recently shown a breathing technique where you
 breathe in through your nose and
 then exhale through your mouth.
 How does that... the idea is that the act of chanting might
 be beneficial to your nervous
 system?
 Is that the point?
 I think so, yes.
 Okay, well that's not why we practice.
 We're not so worried about our nervous system.
 Not really at all anyway, actually.
 We chant in order to one, express respect and veneration
 and remembrance of the Buddha,
 but we also chant to remember the Dhamma.
 So that's to remember the teachings of the Buddha.
 So we chant and recite in order to keep them in mind.
 I'd say those are the two main reasons.
 There's another reason that people often chant today and
 why probably most Buddhist chanting
 is done today is as a protection.
 Because people want this kind of physical and mundane
 protection, which is okay.
 It's just somewhat... well, clingy really.
 False clinging if you're worried about... but they people
 are rightly worried and you know
 they have all these worries about protecting their
 belongings and stuff and their life
 and their health.
 And so we're able to... it's kind of a kusalupaya really.
 You tell people, "Well, these teachings, if you remember
 these teachings, it'll be good
 for you."
 And so they memorize, they recite these teachings.
 But actually the teachings will teach them how to be good
 people.
 Sort of a basic means of giving people an introduction to
 goodness.
 Even though they're doing it to... as a means to... for
 worldly gain for the most part.
 Like those people who pay respect to the Bodhi tree in
 order to get pregnant.
 How about chanting when you don't understand what you're
 saying?
 Is that still of benefit?
 I mean, if you're doing it for the right reason, right?
 If you got a heart full of goodness.
 Like there was this...
 Ajahn Tong always talks about this bird that went to stay
 with the bhikkhunis and they
 had it memorize "atthi-atthi" which means bones, bones.
 I didn't even know what it was saying but it kept reciting
 it and it was so powerful
 that this eagle came and grabbed it.
 And when it grabbed it, the bird said "atthi-atthi" spoke
 it out loud and the eagle was shocked.
 Somehow by the power of the word or something.
 And the bhikkhunis sort of thought that this was indicative
 of the power of good words.
 Somehow that's in the commentary to the Satipatthanas.
 Ajahn Tong always says that the bird went to heaven but I
 don't think the actual sutta
 says that or the commentary anyway says that.
 I don't know if there's a place where it does say that the
 bird went to heaven but that
 would be a better story.
 I think that's why he tells it that the bird went to heaven
 because it had memorized this
 word even though it didn't know what it was saying.
 So Ajahn Tong always gets us to memorize the four Satipatth
anas and then he said "now you're
 as good as animals.
 You're all going to go to heaven.
 Look at all these animals who didn't understand what they
 were saying but they still went
 to heaven so all of you are going to heaven but you're a
 human being so you can do better
 than that and so now we're going to teach you the meaning."
 And he gives this talk again and again and often says this.
 So he's very clear and it's quite clear that if your heart
 is in the right place it doesn't
 matter whether you understand.
 He also would bring up that story when talking about us
 back when I didn't understand and
 whenever there are meditators coming to listen to the Dham
matak he always wanted the western
 meditators to come and listen to the Thai Dhammatak.
 He said even if you don't understand you're sitting there
 and you have a respectful mind
 and you know that what's being said is Dhamma is the Buddha
's teaching.
 So good things come.
 You go to heaven.
 Hello Bante.
 What do we know sitting if it's considered a concept?
 That's a good point because sitting is a description of
 ultimate realities or can be seen as a
 description of ultimate realities.
 When you say sitting you become aware of something present
 which is the feeling of sitting or
 the experience of sitting.
 Sometimes you won't notice that you're sitting.
 There's no feeling or there's no sense when you're really
 still.
 But it comes from ultimate realities.
 During sitting meditation is the focus to be on the feeling
 in the body or noticing the
 action?
 It's the feeling.
 I mean the action is only apparent because of the feeling.
 Physical sensation.
 And that's all the questions for tonight.
 Okay.
 Well then have a good night.
 Thanks everyone for tuning in.
 Thirty four viewers on YouTube.
 Hello everyone.
 Thank you for tuning in.
 It's always nice to see that people are still interested in
 this.
 Might have more going on in the future.
 I've thought about just thinking about doing maybe a once a
 week a talk on something a
 bit more directly related to meditation.
 But that might come in the new year because I might be
 quitting university in January
 in order to do more teaching.
 So there's I could add more slots to the weekly meetings
 because it's full mostly.
 Anyway for now this is what we've got.
 So thank you for tuning in.
 Thanks Robin for your help.
 Thank you.
 Good night everyone.
 Good night.
 Good night.
 Good night.
 Good night.
 Good night.
 Good night.
 Good night.
 Good night.
 Good night.
 Good night.
 Good night.
 Good night.
 Good night.
 Good night.
