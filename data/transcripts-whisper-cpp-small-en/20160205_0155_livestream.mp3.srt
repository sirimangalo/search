1
00:00:00,000 --> 00:00:18,840
 So good evening everyone.

2
00:00:18,840 --> 00:00:33,480
 I'm broadcasting live February 4th, 2016.

3
00:00:33,480 --> 00:00:39,080
 Today's quote is interesting.

4
00:00:39,080 --> 00:00:43,440
 I had to read it a few times before I actually got a sense

5
00:00:43,440 --> 00:00:44,840
 of what it means.

6
00:00:44,840 --> 00:00:48,400
 Still I don't have it completely.

7
00:00:48,400 --> 00:00:54,400
 Sounds a little bit weird.

8
00:00:54,400 --> 00:00:55,400
 So let's break it up.

9
00:00:55,400 --> 00:01:17,670
 Concerning those internal things, I'm not convinced that's

10
00:01:17,670 --> 00:01:21,680
 a very good translation.

11
00:01:21,680 --> 00:01:27,440
 Yeah I get rid of the word "those" because I don't really

12
00:01:27,440 --> 00:01:29,400
 understand it.

13
00:01:29,400 --> 00:01:39,040
 Don't think it's a part of the actual text.

14
00:01:39,040 --> 00:01:41,320
 It's much simpler than it's actually written.

15
00:01:41,320 --> 00:02:02,570
 So it's talking about a sekha, a learner, one who is still

16
00:02:02,570 --> 00:02:04,040
 in training.

17
00:02:04,040 --> 00:02:10,630
 So there are two kinds of monks, there are the sekha, sorry

18
00:02:10,630 --> 00:02:13,800
, two kinds of Buddhists,

19
00:02:13,800 --> 00:02:17,600
 sekha and assekha.

20
00:02:17,600 --> 00:02:23,260
 Actually I guess it's two kinds of enlightened beings

21
00:02:23,260 --> 00:02:24,440
 really.

22
00:02:24,440 --> 00:02:27,160
 Sekha is someone who still is in training.

23
00:02:27,160 --> 00:02:34,720
 Sekha, sekha, sikha-di, one who trains.

24
00:02:34,720 --> 00:02:37,000
 Sekha is one who is training.

25
00:02:37,000 --> 00:02:43,800
 Sekha is the training, sekha is one who trains.

26
00:02:43,800 --> 00:02:48,050
 So it's just a standard description of someone who's still

27
00:02:48,050 --> 00:02:49,080
 striving.

28
00:02:49,080 --> 00:03:01,850
 Then we have apatamana-manasasa, anutraṁ yoga-kemam, pata

29
00:03:01,850 --> 00:03:08,240
-yamanasa.

30
00:03:08,240 --> 00:03:17,240
 One whose mind has not attained supreme freedom from yoga

31
00:03:17,240 --> 00:03:19,040
 when it doesn't practice yoga, no?

32
00:03:19,040 --> 00:03:25,220
 No, supreme freedom from yoga, yoga is a bond, something

33
00:03:25,220 --> 00:03:27,960
 that keeps you tied, something that

34
00:03:27,960 --> 00:03:36,160
 keeps you stuck.

35
00:03:36,160 --> 00:03:42,640
 So to attain supreme freedom from bondage.

36
00:03:42,640 --> 00:03:55,400
 Then there's this funny thing about internal things.

37
00:03:55,400 --> 00:04:03,600
 I read it as one who dwells.

38
00:04:03,600 --> 00:04:09,510
 And something about having made, who dwells having made

39
00:04:09,510 --> 00:04:13,040
 internal that which is inside.

40
00:04:13,040 --> 00:04:19,040
 I don't get it, my poly is not good enough.

41
00:04:19,040 --> 00:04:22,520
 I think it means focusing on internal things.

42
00:04:22,520 --> 00:04:26,440
 One who dwells focusing on internal things and it just says

43
00:04:26,440 --> 00:04:28,280
 there's one thing, there's

44
00:04:28,280 --> 00:04:34,680
 no other thing that is more useful than yoniso-manasikara.

45
00:04:34,680 --> 00:04:37,650
 Again a very common Buddhist term that you almost don't

46
00:04:37,650 --> 00:04:39,800
 want to translate, but he translates

47
00:04:39,800 --> 00:04:45,990
 as giving close attention, giving close attention to the

48
00:04:45,990 --> 00:04:46,920
 mind.

49
00:04:46,920 --> 00:04:51,720
 Not quite how I would put it.

50
00:04:51,720 --> 00:05:00,500
 Yoniso means, yonias actually means womb, the source or the

51
00:05:00,500 --> 00:05:01,760
 womb.

52
00:05:01,760 --> 00:05:07,740
 So what it means is keeping things in mind, keeping the

53
00:05:07,740 --> 00:05:11,720
 sources of things in mind or getting

54
00:05:11,720 --> 00:05:19,660
 to the source of things, analyzing things from a point of

55
00:05:19,660 --> 00:05:21,880
 view of their source.

56
00:05:21,880 --> 00:05:25,240
 So it can be used intellectually, it's usually not.

57
00:05:25,240 --> 00:05:30,440
 It's usually used in terms of describing meditation.

58
00:05:30,440 --> 00:05:34,610
 It's a really good description because insight meditation

59
00:05:34,610 --> 00:05:37,040
 is all about getting to the source

60
00:05:37,040 --> 00:05:38,040
 of things.

61
00:05:38,040 --> 00:05:42,840
 That's why we have this word that we use to remind

62
00:05:42,840 --> 00:05:46,360
 ourselves, to get to the yonit, to

63
00:05:46,360 --> 00:05:52,400
 get to the source.

64
00:05:52,400 --> 00:05:58,740
 Because normally when we experience things we create so

65
00:05:58,740 --> 00:06:01,480
 much more out of them.

66
00:06:01,480 --> 00:06:06,160
 It's all on a conceptual and abstract plane and we get

67
00:06:06,160 --> 00:06:09,240
 stuck on this abstract plane that

68
00:06:09,240 --> 00:06:17,820
 ends up being quite removed from reality, removed from the

69
00:06:17,820 --> 00:06:19,440
 source.

70
00:06:19,440 --> 00:06:31,440
 Yoniso manasikara is about returning to the source of

71
00:06:31,440 --> 00:06:33,960
 things.

72
00:06:33,960 --> 00:06:37,880
 Not grasping at the particulars, not grasping at the signs.

73
00:06:37,880 --> 00:06:45,120
 Nimita is the sign, so when you see a flower, a sign of the

74
00:06:45,120 --> 00:06:49,040
 flower is what makes you think

75
00:06:49,040 --> 00:06:52,440
 it's a flower.

76
00:06:52,440 --> 00:06:58,260
 When you hear a car, the sign of the car is what makes you

77
00:06:58,260 --> 00:07:00,640
 think of it as a car.

78
00:07:00,640 --> 00:07:03,410
 Sign, anubhianjana are the different characteristics, is it

79
00:07:03,410 --> 00:07:07,960
 good or bad or beautiful or ugly or

80
00:07:07,960 --> 00:07:10,320
 sweet or sour or so on.

81
00:07:10,320 --> 00:07:14,400
 So the characteristics that we don't cling to, we get to

82
00:07:14,400 --> 00:07:15,520
 the source.

83
00:07:15,520 --> 00:07:26,680
 When you hear a car, the truth of it is the sound, the

84
00:07:26,680 --> 00:07:32,640
 source of it is the sound.

85
00:07:32,640 --> 00:07:38,010
 So the Buddha says this is the one thing that allows us, if

86
00:07:38,010 --> 00:07:40,800
 you take nothing else, just

87
00:07:40,800 --> 00:07:42,240
 have yoniso manasikara.

88
00:07:42,240 --> 00:07:49,240
 And there's a nice verse that comes after it.

89
00:07:49,240 --> 00:08:02,080
 Yoniso manasikara is the dhamma for bhikkhu in training.

90
00:08:02,080 --> 00:08:14,250
 Natanyo e wang bhukkaru uttamatasapatiya Natanyo there is

91
00:08:14,250 --> 00:08:17,520
 no other dhamma that is of

92
00:08:17,520 --> 00:08:24,800
 such great benefit that does so much as this for the

93
00:08:24,800 --> 00:08:29,920
 attainment of the highest benefit

94
00:08:29,920 --> 00:08:33,200
 or the highest goal.

95
00:08:33,200 --> 00:08:39,240
 Yoniso bandahang bhikkhu for a bhikkhu who strives for the

96
00:08:39,240 --> 00:08:40,480
 source.

97
00:08:40,480 --> 00:08:42,920
 Very clear instruction, get to the source of things.

98
00:08:42,920 --> 00:08:46,880
 It doesn't mean go back to their causes in the past or so.

99
00:08:46,880 --> 00:08:50,160
 Get to the source of the present.

100
00:08:50,160 --> 00:08:53,840
 When you say to yourself, "Pain, pain."

101
00:08:53,840 --> 00:08:57,120
 You're with the source of the pain, hearing.

102
00:08:57,120 --> 00:09:03,120
 You're at the real true source of the experience.

103
00:09:03,120 --> 00:09:11,680
 Kayang dukha sa papune.

104
00:09:11,680 --> 00:09:18,720
 The destruction or the end of dukha, the end of suffering,

105
00:09:18,720 --> 00:09:21,520
 papune, the reach.

106
00:09:21,520 --> 00:09:22,520
 He reaches.

107
00:09:22,520 --> 00:09:28,600
 Papune, not sure about that one.

108
00:09:28,600 --> 00:09:32,360
 Papune means attain, I just don't know the tense.

109
00:09:32,360 --> 00:09:34,360
 I think it means he reach.

110
00:09:34,360 --> 00:09:36,360
 Yeah, here we are.

111
00:09:36,360 --> 00:09:39,360
 Should reach, would reach.

112
00:09:39,360 --> 00:09:48,720
 It's papune's potential.

113
00:09:48,720 --> 00:09:51,600
 So a simple but very powerful teaching.

114
00:09:51,600 --> 00:09:58,280
 Do good to explain it, to be clear about what it means.

115
00:09:58,280 --> 00:10:07,560
 This is a quote, the translation is not ideal.

116
00:10:07,560 --> 00:10:09,320
 It's not close attention to the mind.

117
00:10:09,320 --> 00:10:13,930
 It's not the mind, it's the objects that the mind keeps

118
00:10:13,930 --> 00:10:15,920
 close attention to.

119
00:10:15,920 --> 00:10:16,690
 The mind, not just close attention, but attention to the

120
00:10:16,690 --> 00:10:22,800
 source, yoniso.

121
00:10:22,800 --> 00:10:28,000
 Yoniso padahang bhikkhu.

122
00:10:28,000 --> 00:10:36,750
 A bhikkhu who strives for the source or sticks to the

123
00:10:36,750 --> 00:10:38,680
 source.

124
00:10:38,680 --> 00:10:47,160
 And get caught up in extrapolation or judgment or reaction.

125
00:10:47,160 --> 00:10:50,960
 It's amazing how it changes your outlook.

126
00:10:50,960 --> 00:10:55,100
 Just teaching students at the university tomorrow again, I

127
00:10:55,100 --> 00:10:56,480
'll be teaching.

128
00:10:56,480 --> 00:11:01,080
 Just give people a five minute meditation lesson.

129
00:11:01,080 --> 00:11:04,520
 You can show them something that they've never seen before.

130
00:11:04,520 --> 00:11:10,080
 On a door that they've never even knew was there.

131
00:11:10,080 --> 00:11:18,650
 I've been teaching hour long sessions and people say how

132
00:11:18,650 --> 00:11:20,360
 they never realized their mind

133
00:11:20,360 --> 00:11:26,360
 was so chaotic or so messed up through not meditating.

134
00:11:26,360 --> 00:11:27,560
 Teach a course on the internet.

135
00:11:27,560 --> 00:11:31,730
 I've been teaching courses on the internet and people who I

136
00:11:31,730 --> 00:11:33,640
 don't know say that people

137
00:11:33,640 --> 00:11:37,850
 who have never met in person say they've changed their

138
00:11:37,850 --> 00:11:40,280
 lives through meditation.

139
00:11:40,280 --> 00:11:44,650
 And now we have three meditators here who are really on the

140
00:11:44,650 --> 00:11:46,880
 sekha path here at our house

141
00:11:46,880 --> 00:11:52,290
 who are doing real intensive meditation practicing many

142
00:11:52,290 --> 00:11:53,680
 hours a day.

143
00:11:53,680 --> 00:11:59,200
 All of this is the good work.

144
00:11:59,200 --> 00:12:01,740
 The best part of Buddhism, the best part of the Buddhist

145
00:12:01,740 --> 00:12:03,160
 teachings is called doing the

146
00:12:03,160 --> 00:12:04,160
 Buddhist work.

147
00:12:04,160 --> 00:12:10,040
 Like in other religions have the Lord's work or so on.

148
00:12:10,040 --> 00:12:26,400
 We have the Buddha's work.

149
00:12:26,400 --> 00:12:30,400
 So that's our dhamma drop for today.

150
00:12:30,400 --> 00:12:34,320
 Thank you all for tuning in.

151
00:12:34,320 --> 00:12:36,600
 Wishing you all the best in your practice.

152
00:12:36,600 --> 00:12:37,600
 Good day.

153
00:12:37,600 --> 00:12:51,640
 Thank you.

