1
00:00:00,000 --> 00:00:05,000
 [Birds chirping]

2
00:00:05,000 --> 00:00:12,000
 The day I'll continue the series of talks on the Asavat.

3
00:00:12,000 --> 00:00:16,320
 Again, the Asavat are those mental states, those mind

4
00:00:16,320 --> 00:00:17,000
 states

5
00:00:17,000 --> 00:00:21,000
 which cause the mind to go bad.

6
00:00:21,000 --> 00:00:23,000
 They're the unwholesome mind states.

7
00:00:23,000 --> 00:00:26,000
 There are three types of Asavat.

8
00:00:26,000 --> 00:00:33,300
 The Kama Asavat, the bad things inside which we hold on to

9
00:00:33,300 --> 00:00:36,000
 in regards to sensuality,

10
00:00:36,000 --> 00:00:40,380
 so our desires for sensual pleasures for things which are

11
00:00:40,380 --> 00:00:42,000
 intoxicating,

12
00:00:42,000 --> 00:00:45,190
 and our anger and aversion towards things which are

13
00:00:45,190 --> 00:00:46,000
 unpleasant,

14
00:00:46,000 --> 00:00:50,000
 which are unpleasing to us.

15
00:00:50,000 --> 00:00:56,220
 Kama Asavat and Bha'a Asavat means those defilements, those

16
00:00:56,220 --> 00:00:57,000
 unwholesome things inside

17
00:00:57,000 --> 00:01:00,000
 which have to do with being something.

18
00:01:00,000 --> 00:01:02,710
 Wanting can be this, wanting can be that, not wanting to be

19
00:01:02,710 --> 00:01:05,000
 this, not wanting to be that.

20
00:01:05,000 --> 00:01:11,320
 And the third set, Awi Chaa Asavat, the Asavat, the unwh

21
00:01:11,320 --> 00:01:12,000
olesome mind states

22
00:01:12,000 --> 00:01:17,000
 which simply come from ignorance, which come from delusion.

23
00:01:17,000 --> 00:01:20,190
 Through not knowing, doing bad deeds or having bad

24
00:01:20,190 --> 00:01:21,000
 intentions,

25
00:01:21,000 --> 00:01:24,980
 not because of some external object, but simply out of pure

26
00:01:24,980 --> 00:01:26,000
 ignorance,

27
00:01:26,000 --> 00:01:31,530
 from getting angry or getting greedy, out of ignorance of

28
00:01:31,530 --> 00:01:35,000
 the consequences.

29
00:01:35,000 --> 00:01:39,820
 All in all, it refers to any unwholesome states which exist

30
00:01:39,820 --> 00:01:41,000
 in our hearts.

31
00:01:41,000 --> 00:01:49,300
 And the method today, the fourth method which the Lord

32
00:01:49,300 --> 00:01:50,000
 Buddha prescribed

33
00:01:50,000 --> 00:01:56,000
 for overcoming the Asavat is Ati Wa Asana, Pahat Pahat.

34
00:01:56,000 --> 00:02:00,420
 The Asavat, the unwholesome states which we can do away

35
00:02:00,420 --> 00:02:06,000
 with through patience, through forbearance.

36
00:02:06,000 --> 00:02:16,000
 Ati Wa Asana means overcoming or bearing with them.

37
00:02:16,000 --> 00:02:21,500
 And this is a very important, very practical teaching in

38
00:02:21,500 --> 00:02:26,000
 Buddhist tradition.

39
00:02:26,000 --> 00:02:30,910
 So important, the Lord Buddha said it is the highest form

40
00:02:30,910 --> 00:02:32,000
 of exertion.

41
00:02:32,000 --> 00:02:37,000
 Kanti Paramangtapo Ditika.

42
00:02:37,000 --> 00:02:43,000
 Patience is the highest form of exertion, of practice.

43
00:02:43,000 --> 00:02:47,000
 Ditika forbearance.

44
00:02:47,000 --> 00:02:52,670
 Simply being able to stay with the objects, things which we

45
00:02:52,670 --> 00:02:53,000
 like

46
00:02:53,000 --> 00:02:55,000
 or things which we don't like.

47
00:02:55,000 --> 00:03:04,000
 From a Buddhist perspective, patience takes two sides.

48
00:03:04,000 --> 00:03:06,000
 Patience has two sides to it.

49
00:03:06,000 --> 00:03:10,310
 There is the act of being patient towards something which

50
00:03:10,310 --> 00:03:11,000
 we don't like,

51
00:03:11,000 --> 00:03:13,000
 something which is unpleasant to us.

52
00:03:13,000 --> 00:03:16,220
 So when we see or we hear or when we're confronted with

53
00:03:16,220 --> 00:03:18,000
 something which is unpleasant

54
00:03:18,000 --> 00:03:22,750
 or something which is difficult, something which is hard to

55
00:03:22,750 --> 00:03:24,000
 accomplish,

56
00:03:24,000 --> 00:03:28,000
 hard to stay with, hard to bear,

57
00:03:28,000 --> 00:03:32,450
 being able to stay without getting angry, without getting

58
00:03:32,450 --> 00:03:33,000
 upset,

59
00:03:33,000 --> 00:03:38,710
 to simply remain calm and to keep our calm and to keep our

60
00:03:38,710 --> 00:03:40,000
 minds composed

61
00:03:40,000 --> 00:03:43,730
 in the face of unpleasant situations and unpleasant

62
00:03:43,730 --> 00:03:45,000
 experiences.

63
00:03:45,000 --> 00:03:50,630
 But the other side of patience is being patient with things

64
00:03:50,630 --> 00:03:52,000
 which we do like,

65
00:03:52,000 --> 00:03:57,010
 things which are pleasing to us, things which are intox

66
00:03:57,010 --> 00:03:58,000
icating,

67
00:03:58,000 --> 00:04:02,000
 things which have an intoxicating power over us,

68
00:04:02,000 --> 00:04:09,000
 which are addictive, which are a source of craving for us,

69
00:04:09,000 --> 00:04:12,000
 things which we see or hear or smell or so on,

70
00:04:12,000 --> 00:04:15,000
 or even thoughts and ideas and concepts.

71
00:04:15,000 --> 00:04:19,630
 Again, the idea of wanting to be this or wanting to be that

72
00:04:19,630 --> 00:04:20,000
.

73
00:04:20,000 --> 00:04:25,000
 So patience on this side is being able to bear with

74
00:04:25,000 --> 00:04:29,000
 and not having the things which we want,

75
00:04:29,000 --> 00:04:33,000
 being able to build contentment inside or be content

76
00:04:33,000 --> 00:04:37,000
 in the face of things which are desirable,

77
00:04:37,000 --> 00:04:41,000
 where normally we would chase after the things which we

78
00:04:41,000 --> 00:04:41,000
 desire

79
00:04:41,000 --> 00:04:45,000
 and run away from the things which are unpleasant to us.

80
00:04:45,000 --> 00:04:48,000
 And when we're unable to achieve the things which we desire

81
00:04:48,000 --> 00:04:53,160
 or unable to remove or be away from the things which we

82
00:04:53,160 --> 00:04:55,000
 find undesirable,

83
00:04:55,000 --> 00:04:59,000
 we fall into suffering.

84
00:04:59,000 --> 00:05:03,140
 And this is a very simple explanation of why it is that we

85
00:05:03,140 --> 00:05:04,000
 suffer.

86
00:05:04,000 --> 00:05:08,150
 So for this reason, the Lord Buddha taught that patience is

87
00:05:08,150 --> 00:05:10,000
 the highest form of exertion,

88
00:05:10,000 --> 00:05:13,000
 exerting yourself simply to be patient.

89
00:05:13,000 --> 00:05:15,000
 This is the highest form of taba.

90
00:05:15,000 --> 00:05:18,000
 Taba was a word used in the Buddhist time,

91
00:05:18,000 --> 00:05:22,000
 meaning some sort of practice or exerting oneself,

92
00:05:22,000 --> 00:05:27,430
 specifically used for meditation practices or spiritual

93
00:05:27,430 --> 00:05:29,000
 practices.

94
00:05:29,000 --> 00:05:32,000
 So the Lord Buddha said, "Out of all spiritual practices,

95
00:05:32,000 --> 00:05:37,000
 patience is the highest of them all."

96
00:05:37,000 --> 00:05:40,000
 And this is because at the time when we're patient,

97
00:05:40,000 --> 00:05:43,000
 we won't give rise to greed and we won't give rise to anger

98
00:05:43,000 --> 00:05:46,000
 in regards to the states around us.

99
00:05:46,000 --> 00:05:50,000
 Now patience has three levels to it.

100
00:05:50,000 --> 00:05:55,000
 The first one is called titikha kanti,

101
00:05:55,000 --> 00:06:04,000
 patience which is simply forbearing or withstanding.

102
00:06:04,000 --> 00:06:09,000
 And it's just a name, when it refers to titikha kanti,

103
00:06:09,000 --> 00:06:15,980
 which means patience which is affected simply by repressing

104
00:06:15,980 --> 00:06:17,000
 the desire

105
00:06:17,000 --> 00:06:21,000
 or repressing the aversion towards the object.

106
00:06:21,000 --> 00:06:24,290
 When we see something that we like, simply repressing the

107
00:06:24,290 --> 00:06:25,000
 desire for it,

108
00:06:25,000 --> 00:06:28,630
 repressing the desire to act, the desire to reach, the

109
00:06:28,630 --> 00:06:32,000
 desire to go for.

110
00:06:32,000 --> 00:06:36,000
 And on the other side, when we come across something

111
00:06:36,000 --> 00:06:38,640
 which is uncomfortable or unpleasant and we want to get

112
00:06:38,640 --> 00:06:39,000
 angry,

113
00:06:39,000 --> 00:06:45,000
 simply repressing, forcing ourselves not to act.

114
00:06:45,000 --> 00:06:48,000
 This is the patience of an ordinary human being.

115
00:06:48,000 --> 00:06:53,000
 So we might feel anger inside, we might still feel upset

116
00:06:53,000 --> 00:06:56,450
 or we might still want something, but forcing ourselves not

117
00:06:56,450 --> 00:06:57,000
 to act.

118
00:06:57,000 --> 00:07:02,000
 And this is patience which is based on morality.

119
00:07:02,000 --> 00:07:04,980
 So this is a start. When we first start out on the

120
00:07:04,980 --> 00:07:06,000
 spiritual path,

121
00:07:06,000 --> 00:07:10,000
 we don't have any sort of mental clarity or mental calm.

122
00:07:10,000 --> 00:07:13,000
 But we at least start by giving up things,

123
00:07:13,000 --> 00:07:16,000
 things which we hold on to, things which we want,

124
00:07:16,000 --> 00:07:20,000
 even ideas or beliefs which we want self.

125
00:07:20,000 --> 00:07:26,000
 Being able to bear with things which are addictive,

126
00:07:26,000 --> 00:07:29,000
 things which are intoxicating for us,

127
00:07:29,000 --> 00:07:32,000
 things which bring about craving in our minds,

128
00:07:32,000 --> 00:07:35,290
 things which we find desirable, or things which we find

129
00:07:35,290 --> 00:07:36,000
 undesirable.

130
00:07:36,000 --> 00:07:41,560
 Bearing with our anger, when someone says or does something

131
00:07:41,560 --> 00:07:42,000
 bad to us

132
00:07:42,000 --> 00:07:45,000
 and makes us want to say bad things back to them,

133
00:07:45,000 --> 00:07:49,000
 stopping ourselves from saying or doing bad things

134
00:07:49,000 --> 00:07:51,000
 because of greed or because of anger.

135
00:07:51,000 --> 00:07:55,000
 This is called titikha kanti, and it's patience based on,

136
00:07:55,000 --> 00:07:59,000
 it's another word for morality.

137
00:07:59,000 --> 00:08:03,000
 Morality is another word for this kind of patience,

138
00:08:03,000 --> 00:08:07,130
 for this patience is the same, is equivalent to having

139
00:08:07,130 --> 00:08:08,000
 morality.

140
00:08:08,000 --> 00:08:11,990
 At the time when the opportunity arises to do something bad

141
00:08:11,990 --> 00:08:12,000
,

142
00:08:12,000 --> 00:08:17,000
 we simply stay, keep our peace.

143
00:08:17,000 --> 00:08:20,610
 And even though inside we might be angry and we have all

144
00:08:20,610 --> 00:08:22,000
 sorts of mental suffering,

145
00:08:22,000 --> 00:08:25,000
 we don't act out on it because we know that there will be

146
00:08:25,000 --> 00:08:25,000
 more suffering

147
00:08:25,000 --> 00:08:28,000
 and repercussions to follow when we do those bad deeds.

148
00:08:28,000 --> 00:08:32,000
 This is the sort of a basic spiritual practice,

149
00:08:32,000 --> 00:08:35,000
 but of course it's not a very high spiritual practice

150
00:08:35,000 --> 00:08:37,000
 and it can't last by itself.

151
00:08:37,000 --> 00:08:41,000
 Eventually the repressed emotions simply explode

152
00:08:41,000 --> 00:08:45,140
 and we find ourselves doing and saying bad things once

153
00:08:45,140 --> 00:08:46,000
 again.

154
00:08:46,000 --> 00:08:51,000
 So the second kind of patience is called tapa kanti.

155
00:08:51,000 --> 00:08:56,650
 Tapa, and this is the word tapa, tapa, tapa, let me say it

156
00:08:56,650 --> 00:08:57,000
 in Pali.

157
00:08:57,000 --> 00:09:01,000
 Tapa means some sort of practice.

158
00:09:01,000 --> 00:09:05,350
 In this case, tapa kanti means patience based on meditation

159
00:09:05,350 --> 00:09:06,000
 practice

160
00:09:06,000 --> 00:09:11,000
 or the practice of samatha and vipassana practice.

161
00:09:11,000 --> 00:09:14,140
 When we are sitting and doing meditation and we want it

162
00:09:14,140 --> 00:09:16,000
 rising, falling,

163
00:09:16,000 --> 00:09:20,520
 or when we watch seeing, seeing or hearing, hearing or so

164
00:09:20,520 --> 00:09:21,000
 on,

165
00:09:21,000 --> 00:09:26,770
 we find that we are able to give up our anger and our greed

166
00:09:26,770 --> 00:09:27,000
.

167
00:09:27,000 --> 00:09:30,000
 We are able to create a state of concentration,

168
00:09:30,000 --> 00:09:33,680
 a state of focus which is free from greed and free from

169
00:09:33,680 --> 00:09:34,000
 anger

170
00:09:34,000 --> 00:09:36,000
 because it's free from delusion.

171
00:09:36,000 --> 00:09:39,050
 And at the time when there is no delusion, greed and anger

172
00:09:39,050 --> 00:09:40,000
 can't arise.

173
00:09:40,000 --> 00:09:42,940
 So at the time when we do walking meditation or sitting

174
00:09:42,940 --> 00:09:44,000
 meditation

175
00:09:44,000 --> 00:09:48,280
 or even mindful in our daily lives, we find ourselves at

176
00:09:48,280 --> 00:09:49,000
 great peace

177
00:09:49,000 --> 00:09:52,000
 with the things around us and the things inside of us.

178
00:09:52,000 --> 00:09:56,000
 And when pleasant, pleasurable things arise,

179
00:09:56,000 --> 00:09:59,480
 we find ourselves not reaching out for them, not needing

180
00:09:59,480 --> 00:10:00,000
 for them,

181
00:10:00,000 --> 00:10:03,000
 not even really attached to them.

182
00:10:03,000 --> 00:10:06,140
 Sometimes pleasant things come and we don't even notice

183
00:10:06,140 --> 00:10:07,000
 them as pleasant.

184
00:10:07,000 --> 00:10:10,000
 We may not even notice that they are there.

185
00:10:10,000 --> 00:10:13,000
 Unpleasant things might come before they would be things

186
00:10:13,000 --> 00:10:15,000
 which create anger inside of us.

187
00:10:15,000 --> 00:10:18,860
 We find because of our concentration we are not even

188
00:10:18,860 --> 00:10:20,000
 interested,

189
00:10:20,000 --> 00:10:25,000
 we are not upset, we are not disturbed by the phenomenon.

190
00:10:25,000 --> 00:10:28,760
 This is through the power of concentration, so this is

191
00:10:28,760 --> 00:10:31,000
 equivalent to concentration.

192
00:10:31,000 --> 00:10:34,420
 When we say concentration, we are referring to having

193
00:10:34,420 --> 00:10:38,000
 patience through,

194
00:10:38,000 --> 00:10:41,000
 through meditation practice.

195
00:10:41,000 --> 00:10:45,230
 It's a form of patience that refers to concentration. These

196
00:10:45,230 --> 00:10:46,000
 are equivalent.

197
00:10:46,000 --> 00:10:55,000
 And the third type of patience is called adiwasana kantin.

198
00:10:55,000 --> 00:10:59,000
 And this is the adiwasana, the teaching which I give today.

199
00:10:59,000 --> 00:11:03,120
 And this is patience which comes from seeing clearly the

200
00:11:03,120 --> 00:11:04,000
 object.

201
00:11:04,000 --> 00:11:09,000
 So, once we continue our practice of vipassana, we find,

202
00:11:09,000 --> 00:11:15,660
 even when we do notice things that would be desirable for

203
00:11:15,660 --> 00:11:17,000
 us,

204
00:11:17,000 --> 00:11:19,360
 through the wisdom which we have gained, whether we

205
00:11:19,360 --> 00:11:22,000
 practice meditation or don't practice meditation,

206
00:11:22,000 --> 00:11:26,000
 we find we don't desire the things which we used to desire.

207
00:11:26,000 --> 00:11:29,210
 We find that in the face of things which are terribly

208
00:11:29,210 --> 00:11:30,000
 unpleasant,

209
00:11:30,000 --> 00:11:35,180
 things which an ordinary person would find unbearable, who

210
00:11:35,180 --> 00:11:36,000
 would be unable to bear.

211
00:11:36,000 --> 00:11:41,000
 We find we are able to be at peace and be calm and be happy

212
00:11:41,000 --> 00:11:43,000
 even in the face of these things,

213
00:11:43,000 --> 00:11:46,990
 or things which are pleasurable, which are intoxicating,

214
00:11:46,990 --> 00:11:50,000
 which are enticing for the ordinary person.

215
00:11:50,000 --> 00:11:53,010
 We find them not enticing, and this time not simply through

216
00:11:53,010 --> 00:11:56,000
 the power of concentration and not noticing them.

217
00:11:56,000 --> 00:11:59,000
 In this case we notice them in all of their characteristics

218
00:11:59,000 --> 00:12:01,790
, but we realize that they really aren't pleasing in the way

219
00:12:01,790 --> 00:12:03,000
 that we thought they were.

220
00:12:03,000 --> 00:12:05,790
 When we realize that they are impermanent, we realize that

221
00:12:05,790 --> 00:12:07,000
 they are unsatisfying,

222
00:12:07,000 --> 00:12:11,000
 we realize that they are not under our control.

223
00:12:11,000 --> 00:12:13,890
 We can't keep them, we can't force them to stay, we can't

224
00:12:13,890 --> 00:12:17,000
 force the bad things to go.

225
00:12:17,000 --> 00:12:21,580
 We can't do anything except be mindful and be aware and let

226
00:12:21,580 --> 00:12:25,000
 them come and let them go and let them be.

227
00:12:25,000 --> 00:12:29,000
 And this is the practice of patience.

228
00:12:29,000 --> 00:12:35,400
 So when we are able to bear with painful feelings in the

229
00:12:35,400 --> 00:12:37,000
 practice of meditation,

230
00:12:37,000 --> 00:12:42,110
 when we are able to bear with unpleasant sounds, unpleasant

231
00:12:42,110 --> 00:12:47,000
 tastes, unpleasant feelings, unpleasant emotions,

232
00:12:47,000 --> 00:12:51,000
 or when we are able to bear with our wants and our desires,

233
00:12:51,000 --> 00:12:55,090
 sitting in the forest can be pleasant in the beginning, but

234
00:12:55,090 --> 00:12:57,610
 it might be that after some days the meditator starts to

235
00:12:57,610 --> 00:13:00,000
 think about the beaches,

236
00:13:00,000 --> 00:13:03,130
 or think about the bars, or think about the discos, or

237
00:13:03,130 --> 00:13:05,000
 think about any number of things,

238
00:13:05,000 --> 00:13:10,000
 friends and family and things which are the object of our

239
00:13:10,000 --> 00:13:11,000
 desire.

240
00:13:11,000 --> 00:13:15,930
 In the practice of meditation we are trying to build

241
00:13:15,930 --> 00:13:16,000
 patience,

242
00:13:16,000 --> 00:13:19,470
 so that wherever we go we will always be able to be at

243
00:13:19,470 --> 00:13:22,000
 peace and happy wherever we are.

244
00:13:22,000 --> 00:13:25,260
 The things that we want and we desire, we won't have to run

245
00:13:25,260 --> 00:13:27,000
 after them ever and ever again,

246
00:13:27,000 --> 00:13:30,710
 never being happy with what we have, no matter where we are

247
00:13:30,710 --> 00:13:31,000
.

248
00:13:31,000 --> 00:13:35,000
 We will be able to be happy and at peace wherever we are.

249
00:13:35,000 --> 00:13:39,720
 So the method to do this is simply through the practice of

250
00:13:39,720 --> 00:13:43,000
 meditation, through the practice of mindfulness.

251
00:13:43,000 --> 00:13:47,000
 At the time when we feel something unpleasant, perhaps a

252
00:13:47,000 --> 00:13:51,000
 painful feeling in the body, or so on,

253
00:13:51,000 --> 00:13:59,930
 we simply acknowledge it, pain, pain, pain, pain, with a

254
00:13:59,930 --> 00:14:02,000
 very high degree of patience.

255
00:14:02,000 --> 00:14:05,520
 When we acknowledge, of course, one of the most important

256
00:14:05,520 --> 00:14:08,000
 things is that we acknowledge patiently.

257
00:14:08,000 --> 00:14:12,610
 We are not acknowledging with anger, acknowledging with

258
00:14:12,610 --> 00:14:15,000
 greed, the desire for it to go away,

259
00:14:15,000 --> 00:14:18,000
 or the desire for it to stay, or so on.

260
00:14:18,000 --> 00:14:23,000
 We are simply being mindful of the reality of the object.

261
00:14:23,000 --> 00:14:27,490
 We say pain, pain, becoming comfortable with it and seeing

262
00:14:27,490 --> 00:14:28,000
 it clearly,

263
00:14:28,000 --> 00:14:35,280
 creating a clear thought which is neither greedy or angry

264
00:14:35,280 --> 00:14:39,000
 or upset about the object.

265
00:14:39,000 --> 00:14:42,000
 And this is the way that we come to be a patient person.

266
00:14:42,000 --> 00:14:45,000
 This is the way that we come to do away with unwholesome,

267
00:14:45,000 --> 00:14:47,000
 unclean mind states,

268
00:14:47,000 --> 00:14:54,000
 unpleasant mind states like craving or anger or delusion.

269
00:14:54,000 --> 00:14:58,000
 This is through the practice of patience.

270
00:14:58,000 --> 00:15:01,710
 It's one of the most important virtues and most important

271
00:15:01,710 --> 00:15:03,000
 things to keep in mind

272
00:15:03,000 --> 00:15:05,700
 when we practice the meditation, because of course the

273
00:15:05,700 --> 00:15:07,000
 results of meditation

274
00:15:07,000 --> 00:15:12,000
 don't ever come in simply one day or one sitting practice.

275
00:15:12,000 --> 00:15:15,280
 We have to be very, very patient when we practice and

276
00:15:15,280 --> 00:15:18,000
 consider that we are beginners.

277
00:15:18,000 --> 00:15:23,000
 Just as anyone training in any sport or any activity,

278
00:15:23,000 --> 00:15:27,190
 they have a very good saying in the West that practice

279
00:15:27,190 --> 00:15:29,000
 makes perfect.

280
00:15:29,000 --> 00:15:32,120
 It's important to always keep in mind that we're practicing

281
00:15:32,120 --> 00:15:33,000
 meditation.

282
00:15:33,000 --> 00:15:38,190
 We're not perfect in it. We haven't perfected the

283
00:15:38,190 --> 00:15:39,000
 meditation.

284
00:15:39,000 --> 00:15:42,000
 We're practicing it. And when we practice meditation,

285
00:15:42,000 --> 00:15:46,000
 it's possible that if we practice with enough sincerity

286
00:15:46,000 --> 00:15:48,000
 that we can make it perfect,

287
00:15:48,000 --> 00:15:52,000
 or we can at least come to the point where we are perfect

288
00:15:52,000 --> 00:15:55,000
 in the sense of not ever getting angry or getting greedy,

289
00:15:55,000 --> 00:15:58,560
 we've come to see the truth about ourselves and the world

290
00:15:58,560 --> 00:15:59,000
 around us

291
00:15:59,000 --> 00:16:04,000
 and not ever giving rise to those emotions which are based

292
00:16:04,000 --> 00:16:05,000
 on delusion

293
00:16:05,000 --> 00:16:11,000
 and based on illusion and misunderstanding about the world.

294
00:16:11,000 --> 00:16:18,700
 So my wish for everyone here and the advice that I would

295
00:16:18,700 --> 00:16:20,000
 give is,

296
00:16:20,000 --> 00:16:23,000
 we must all try to be patient in our meditation practice,

297
00:16:23,000 --> 00:16:27,660
 not wanting for something in the future or thinking about

298
00:16:27,660 --> 00:16:29,000
 something in the past,

299
00:16:29,000 --> 00:16:33,030
 not hurrying ahead and not standing still and simply

300
00:16:33,030 --> 00:16:35,000
 continuing on at an even pace

301
00:16:35,000 --> 00:16:38,000
 as slow and steady wins the race.

302
00:16:38,000 --> 00:16:43,230
 If we continue on in this way, our practice will become

303
00:16:43,230 --> 00:16:44,000
 perfect.

304
00:16:44,000 --> 00:16:48,460
 Our practice will make us perfect in the sense of not being

305
00:16:48,460 --> 00:16:51,000
 angry or greedy,

306
00:16:51,000 --> 00:16:54,000
 or deluded about the things inside or around us.

307
00:16:54,000 --> 00:16:56,000
 And this is the teaching for today.

308
00:16:56,000 --> 00:16:59,370
 Now we'll continue with meditation practice at your own

309
00:16:59,370 --> 00:17:00,000
 speed.

