1
00:00:00,000 --> 00:00:01,000
 Go ahead.

2
00:00:01,000 --> 00:00:05,680
 It seems to me that being a female, a Buddhist nun,

3
00:00:05,680 --> 00:00:10,680
 is more difficult in a practical, organizational sense

4
00:00:10,680 --> 00:00:12,600
 than being a Buddhist monk.

5
00:00:12,600 --> 00:00:15,980
 It seems that it is more difficult to find a good place to

6
00:00:15,980 --> 00:00:16,320
 stay

7
00:00:16,320 --> 00:00:20,440
 and generally organize life as a nun

8
00:00:20,440 --> 00:00:24,120
 and not to be sort of a servant in a monastery.

9
00:00:24,120 --> 00:00:26,880
 Am I under a wrong impression?

10
00:00:26,880 --> 00:00:34,320
 It depends very much on where you are

11
00:00:34,320 --> 00:00:36,520
 and with whom you are.

12
00:00:36,520 --> 00:00:44,880
 It is certainly more difficult to find a good place to stay

13
00:00:44,880 --> 00:00:47,920
 because there are not so many yet.

14
00:00:47,920 --> 00:00:49,960
 It's getting better.

15
00:00:50,080 --> 00:00:59,260
 There are bhikkhunis who are becoming senior enough to ord

16
00:00:59,260 --> 00:00:59,760
ain

17
00:00:59,760 --> 00:01:04,800
 and there are more sponsors for bhikkhuni places.

18
00:01:04,800 --> 00:01:13,610
 So it will become a little bit more simple in near future,

19
00:01:13,610 --> 00:01:14,800
 I would say.

20
00:01:14,920 --> 00:01:23,640
 For now and for me, I try not to focus on the difference

21
00:01:23,640 --> 00:01:25,720
 between male and female.

22
00:01:25,720 --> 00:01:28,920
 I try just to be a good monk.

23
00:01:28,920 --> 00:01:35,640
 And I think that's what is important.

24
00:01:35,640 --> 00:01:40,480
 For the Buddha in the Abhidhamma,

25
00:01:40,600 --> 00:01:49,000
 I read female and male as just a physical characteristic.

26
00:01:49,000 --> 00:01:55,320
 So the mind can be defiled in the male or in the female

27
00:01:55,320 --> 00:01:55,640
 body

28
00:01:55,640 --> 00:01:59,480
 or it can be liberated in the male or female body.

29
00:01:59,480 --> 00:02:06,680
 So in that sense, we are kind of in the same situation,

30
00:02:06,800 --> 00:02:12,520
 in the same bad situation that's still in samsara.

31
00:02:12,520 --> 00:02:16,960
 There are in countries like Thailand, for example,

32
00:02:16,960 --> 00:02:23,880
 eight precept nuns or in Sri Lanka here are many ten

33
00:02:23,880 --> 00:02:25,240
 precept nuns.

34
00:02:25,240 --> 00:02:34,640
 And for them, it is such a situation for being a servant in

35
00:02:34,640 --> 00:02:35,440
 a monastery.

36
00:02:35,560 --> 00:02:38,560
 They cook and clean and are really servants.

37
00:02:38,560 --> 00:02:42,470
 And sometimes in some places, like slaves of the monks, it

38
00:02:42,470 --> 00:02:46,120
's heartbreaking.

39
00:02:46,120 --> 00:02:48,520
 But in some places not.

40
00:02:48,520 --> 00:02:53,220
 There's not really an argument that I use, but there's the

41
00:02:53,220 --> 00:02:54,840
 argument in Thailand.

42
00:02:54,840 --> 00:02:56,920
 There's an example, I was staying up on the mountain

43
00:02:56,920 --> 00:03:01,400
 and I commented to one of the monks in Doi Sutheb.

44
00:03:01,520 --> 00:03:05,860
 I said, the Westerners are not really, they're kind of

45
00:03:05,860 --> 00:03:06,480
 upset at this.

46
00:03:06,480 --> 00:03:09,520
 They see the nuns all working in the kitchen.

47
00:03:09,520 --> 00:03:14,160
 And he said, "Well, what do you expect these nuns to do?

48
00:03:14,160 --> 00:03:17,880
 That's all they, their skill is to cook.

49
00:03:17,880 --> 00:03:20,560
 The monks that are working in the office,

50
00:03:20,560 --> 00:03:25,600
 it's because they've come from an office job before."

51
00:03:25,600 --> 00:03:26,960
 And in Doi Sutheb, that's the truth.

52
00:03:27,080 --> 00:03:31,640
 These monks had been in whatever government positions or so

53
00:03:31,640 --> 00:03:31,640
 on.

54
00:03:31,640 --> 00:03:36,200
 And he said, "We take people according to their abilities."

55
00:03:36,200 --> 00:03:39,230
 That's no excuse, of course, for making them work like a

56
00:03:39,230 --> 00:03:39,880
 slave,

57
00:03:39,880 --> 00:03:42,840
 which is really terrible.

58
00:03:42,840 --> 00:03:47,560
 But there is something there that just is that in Thailand

59
00:03:47,560 --> 00:03:48,800
 or in Sri Lanka,

60
00:03:48,800 --> 00:03:53,620
 the monks, in one sense, the monks and the nuns are both

61
00:03:53,620 --> 00:03:54,280
 slaves

62
00:03:54,400 --> 00:03:57,210
 because they have put themselves in a position of being

63
00:03:57,210 --> 00:03:57,520
 priests

64
00:03:57,520 --> 00:04:02,910
 and priestesses or whatever, in the sense that they work

65
00:04:02,910 --> 00:04:03,480
 for the people.

66
00:04:03,480 --> 00:04:06,880
 They will do ceremonies and go to ceremony after ceremony

67
00:04:06,880 --> 00:04:09,480
 and they become slaves not only to the lay people,

68
00:04:09,480 --> 00:04:11,760
 they become slaves to their desires.

69
00:04:11,760 --> 00:04:14,760
 Some of them have become slaves to their ambitions.

70
00:04:14,760 --> 00:04:17,540
 Some of them become slaves to their desires to such an

71
00:04:17,540 --> 00:04:17,920
 extent

72
00:04:17,920 --> 00:04:23,200
 that they abuse children and horrific things.

73
00:04:23,320 --> 00:04:27,160
 These sorts of things go on.

74
00:04:27,160 --> 00:04:31,240
 So I would say it depends largely on the community

75
00:04:31,240 --> 00:04:34,760
 because the example I would give in what Jom Tong, for

76
00:04:34,760 --> 00:04:36,040
 example,

77
00:04:36,040 --> 00:04:43,960
 the nuns are now in some of the highest positions.

78
00:04:43,960 --> 00:04:46,260
 In terms of bureaucracy, they're not, they're still under

79
00:04:46,260 --> 00:04:46,640
 the monks

80
00:04:46,640 --> 00:04:49,000
 and that's the deal with Thailand.

81
00:04:49,000 --> 00:04:52,920
 But when I go to Jom Tong, I get scolded by this.

82
00:04:53,040 --> 00:04:55,320
 I have this nun in the office because she runs the office

83
00:04:55,320 --> 00:04:58,280
 and she's like number two in charge of the monastery

84
00:04:58,280 --> 00:05:01,240
 underneath this monk who's,

85
00:05:01,240 --> 00:05:03,120
 I don't want to talk too much about the place,

86
00:05:03,120 --> 00:05:07,760
 but it's not a place I like to spend my time

87
00:05:07,760 --> 00:05:10,760
 except because my teacher is there.

88
00:05:10,760 --> 00:05:15,060
 But she has quite a bit of power and that's how it's always

89
00:05:15,060 --> 00:05:15,520
 been there.

90
00:05:15,520 --> 00:05:18,560
 The nuns have actually run the main office.

91
00:05:18,560 --> 00:05:20,160
 So people in Doy's and Deb would say,

92
00:05:20,160 --> 00:05:21,800
 "Why are the monks all in the office?"

93
00:05:21,920 --> 00:05:23,160
 In Jom Tong, the nuns are in the office.

94
00:05:23,160 --> 00:05:26,000
 In Wat Lam Pung, the nuns are in the office as well.

95
00:05:26,000 --> 00:05:30,000
 And the monks have their office, the nuns have their office

96
00:05:30,000 --> 00:05:30,080
.

97
00:05:30,080 --> 00:05:33,170
 And if you look at Wat Lam Pung, I think it's a pretty good

98
00:05:33,170 --> 00:05:34,520
 example of,

99
00:05:34,520 --> 00:05:38,220
 I don't know, maybe the nuns do have to work harder than

100
00:05:38,220 --> 00:05:39,200
 the monks.

101
00:05:39,200 --> 00:05:41,840
 Some of the monks get away with murder at Wat Lam Pung or

102
00:05:41,840 --> 00:05:43,200
 they used to.

103
00:05:43,200 --> 00:05:47,120
 Yeah. I think in general in Wat Lam Pung,

104
00:05:47,120 --> 00:05:51,600
 everybody has a two-week alternation.

105
00:05:51,720 --> 00:05:53,960
 In Jom Tong as well.

106
00:05:53,960 --> 00:05:58,240
 In fact, in Jom Tong, it's a bit weighted towards the nuns.

107
00:05:58,240 --> 00:06:02,360
 The nuns do harder work in the kitchen because those nuns

108
00:06:02,360 --> 00:06:04,000
 who have been cooked before

109
00:06:04,000 --> 00:06:07,200
 and most women in Thailand are pretty good at cooking.

110
00:06:07,200 --> 00:06:10,920
 That's what they were trained to do by the culture.

111
00:06:10,920 --> 00:06:13,960
 So those nuns who do work in the kitchen work pretty hard

112
00:06:13,960 --> 00:06:17,320
 and I can attest to that having seen it.

113
00:06:17,320 --> 00:06:19,850
 But they only do it for two weeks and then they have two

114
00:06:19,850 --> 00:06:20,400
 weeks off.

115
00:06:20,520 --> 00:06:21,960
 The monks don't get that.

116
00:06:21,960 --> 00:06:25,240
 The monks who work, some of them are working every day,

117
00:06:25,240 --> 00:06:26,440
 every day, every day.

118
00:06:26,440 --> 00:06:28,440
 And I don't know of any monks that have...

119
00:06:28,440 --> 00:06:31,760
 Some of them there are some things and it's changing now.

120
00:06:31,760 --> 00:06:35,270
 So just to say it largely, the real problem of course is

121
00:06:35,270 --> 00:06:36,200
 that the nuns in Thailand

122
00:06:36,200 --> 00:06:39,680
 are not real nuns and they're not given status as nuns.

123
00:06:39,680 --> 00:06:44,960
 There's this pretend kind of creation that we've developed

124
00:06:44,960 --> 00:06:48,080
 and it's become so normal that everyone's saying,

125
00:06:48,200 --> 00:06:50,560
 "Well, why don't you just be content with being a maitji?

126
00:06:50,560 --> 00:06:53,080
 Why don't you just be content with being this fake thing

127
00:06:53,080 --> 00:06:53,840
 that we've set up?"

128
00:06:53,840 --> 00:06:55,960
 But it's actually fake and it actually came later.

129
00:06:55,960 --> 00:07:00,390
 The truth is they should be following that which the Buddha

130
00:07:00,390 --> 00:07:00,920
 laid out

131
00:07:00,920 --> 00:07:03,920
 and all of these rules that the Buddha laid out which are

132
00:07:03,920 --> 00:07:04,960
 there for a purpose

133
00:07:04,960 --> 00:07:07,900
 to keep the communal harmony and to keep the monks and the

134
00:07:07,900 --> 00:07:08,120
 nuns

135
00:07:08,120 --> 00:07:12,520
 from getting in trouble with each other and so on.

136
00:07:12,520 --> 00:07:15,880
 I think that's more a problem than the slave thing.

137
00:07:16,000 --> 00:07:19,000
 But that's so much a culture as well.

138
00:07:19,000 --> 00:07:22,500
 I mean, Asian culture is not very friendly towards women in

139
00:07:22,500 --> 00:07:23,200
 general,

140
00:07:23,200 --> 00:07:25,200
 Buddhism aside.

141
00:07:25,200 --> 00:07:29,240
 But again, this is probably a hot button topic.

142
00:07:29,240 --> 00:07:35,610
 Yeah, but I think it is really all about liberation of the

143
00:07:35,610 --> 00:07:36,000
 mind

144
00:07:36,000 --> 00:07:42,120
 and should not be about being female or being male.

145
00:07:42,120 --> 00:07:45,000
 And...

146
00:07:45,120 --> 00:07:49,740
 But I think that there's a good point there that both of

147
00:07:49,740 --> 00:07:51,600
 them are totally lost.

148
00:07:51,600 --> 00:07:54,650
 If you're talking about monks and nuns in general in Sri

149
00:07:54,650 --> 00:07:55,120
 Lanka,

150
00:07:55,120 --> 00:07:57,350
 don't even go there. These aren't monks, these aren't nuns

151
00:07:57,350 --> 00:07:58,520
 in the sutta sense.

152
00:07:58,520 --> 00:08:01,080
 Neither one of them are at all interested in liberation

153
00:08:01,080 --> 00:08:03,680
 and their practice is not going to lead them towards

154
00:08:03,680 --> 00:08:04,360
 liberation.

155
00:08:04,360 --> 00:08:08,040
 And in fact, the nuns are often more sincere, right?

156
00:08:08,040 --> 00:08:10,010
 Because they don't... This is what the argument was in

157
00:08:10,010 --> 00:08:10,400
 Thailand,

158
00:08:10,400 --> 00:08:13,160
 that the nuns would often practice harder

159
00:08:13,280 --> 00:08:18,480
 because they didn't ordain for status,

160
00:08:18,480 --> 00:08:21,720
 they didn't ordain to become some high nun,

161
00:08:21,720 --> 00:08:24,540
 because you can't really, or you can, but it's very

162
00:08:24,540 --> 00:08:25,400
 difficult.

163
00:08:25,400 --> 00:08:27,650
 So they were the ones who were there because they really

164
00:08:27,650 --> 00:08:28,400
 wanted to practice,

165
00:08:28,400 --> 00:08:30,120
 because they saw suffering.

166
00:08:30,120 --> 00:08:32,880
 And because they felt like their role as a woman in this

167
00:08:32,880 --> 00:08:34,400
 sexist society

168
00:08:34,400 --> 00:08:37,730
 was a lot of suffering, and so they wanted to become free

169
00:08:37,730 --> 00:08:38,600
 from that.

170
00:08:38,600 --> 00:08:41,610
 Most of the meditation centres in Thailand are full of

171
00:08:41,610 --> 00:08:42,000
 women

172
00:08:42,120 --> 00:08:45,560
 and they tend to practice a lot better than the monks.

173
00:08:45,560 --> 00:08:50,560
 There's Ajahn Supan, he was saying,

174
00:08:50,560 --> 00:08:53,880
 "The monks are first in everything.

175
00:08:53,880 --> 00:08:56,920
 When it's time for food, monks first.

176
00:08:56,920 --> 00:09:01,200
 When it's time for getting a ride in the car or something,

177
00:09:01,200 --> 00:09:02,080
 monks first.

178
00:09:02,080 --> 00:09:05,000
 With everything, the monks go first, except meditation.

179
00:09:05,000 --> 00:09:08,630
 When it comes to meditation, the monks say, 'Oh, you go

180
00:09:08,630 --> 00:09:09,800
 first.'

181
00:09:09,920 --> 00:09:13,580
 He actually said that in front of a room full of monks and

182
00:09:13,580 --> 00:09:15,640
 laypeople, I think.

183
00:09:15,640 --> 00:09:18,720
 I can't remember where he said it, but I was there when he

184
00:09:18,720 --> 00:09:19,400
 said it.

185
00:09:19,400 --> 00:09:22,640
 Just... Anyway, but...

186
00:09:22,640 --> 00:09:26,200
 No, but that goes against this idea, for sure.

187
00:09:26,200 --> 00:09:30,360
 What you see in the Bhikkhuni... What is it? The Tari Gatha

188
00:09:30,360 --> 00:09:31,200
?

189
00:09:31,200 --> 00:09:32,720
 I think the Tari Gatha.

190
00:09:32,720 --> 00:09:34,320
 Mara always comes to them and says,

191
00:09:34,320 --> 00:09:37,320
 "What does a woman have to do with enlightenment, with your

192
00:09:37,320 --> 00:09:38,960
 two-finger wisdom?"

193
00:09:39,080 --> 00:09:41,640
 Two-finger wisdom apparently has to do with stirring a pot

194
00:09:41,640 --> 00:09:42,440
 or something,

195
00:09:42,440 --> 00:09:45,520
 or I don't know, measuring or something.

196
00:09:45,520 --> 00:09:49,080
 Some women, stereotypical women thing.

197
00:09:49,080 --> 00:09:51,520
 And the Bhikkhunis would always say,

198
00:09:51,520 --> 00:09:55,910
 "What is there of a woman to someone who has let go of

199
00:09:55,910 --> 00:09:56,760
 craving?"

200
00:09:56,760 --> 00:09:58,480
 They would say, "Arhan."

201
00:09:58,480 --> 00:10:02,880
 And they would answer in this way, "What is...

202
00:10:02,880 --> 00:10:06,160
 What should I be relating to the word 'woman'?

203
00:10:06,160 --> 00:10:08,720
 What is there in me that is woman?"

204
00:10:08,840 --> 00:10:11,680
 "What do I have to do with woman?"

205
00:10:11,680 --> 00:10:17,160
 And so in this way they would make Mara very sad and upset.

206
00:10:17,160 --> 00:10:21,720
 Because he couldn't trick them into becoming...

207
00:10:21,720 --> 00:10:23,760
 What's the word?

208
00:10:23,760 --> 00:10:26,280
 I don't want to say feminist, because that's probably a bad

209
00:10:26,280 --> 00:10:28,960
 thing to say, but...

210
00:10:28,960 --> 00:10:33,160
 Feminist in a way that, you know, really gung-ho about...

211
00:10:33,280 --> 00:10:39,360
 about being a male or being a female or about the genders.

212
00:10:39,360 --> 00:10:42,480
 Because in the end the mind is the same.

213
00:10:42,480 --> 00:10:45,210
 There's a story in the commentary, the Dhammapada

214
00:10:45,210 --> 00:10:46,960
 commentary, I think, of a...

215
00:10:46,960 --> 00:10:52,480
 One commentary where this man he saw...

216
00:10:52,480 --> 00:10:54,560
 I think it was Mahakasapa.

217
00:10:54,560 --> 00:10:57,200
 He saw Mahakasapa going on alms and he was...

218
00:10:57,200 --> 00:11:01,760
 He was some raunchy lay person.

219
00:11:01,880 --> 00:11:04,870
 And he looked at him and he said, "Ooh, that elder is quite

220
00:11:04,870 --> 00:11:05,880
 attractive.

221
00:11:05,880 --> 00:11:08,600
 Wouldn't it be great if he were my wife?"

222
00:11:08,600 --> 00:11:10,980
 He actually thought this. He kind of thought, "If he was a

223
00:11:10,980 --> 00:11:11,360
 woman,

224
00:11:11,360 --> 00:11:15,160
 I'd take him as my wife right away."

225
00:11:15,160 --> 00:11:18,340
 And the story goes that as a result, because Mahakasapa was

226
00:11:18,340 --> 00:11:18,920
 an arahant

227
00:11:18,920 --> 00:11:23,000
 and as a result of fashioning this...

228
00:11:23,000 --> 00:11:30,080
 idea of taking an enlightened being as their wife.

229
00:11:30,200 --> 00:11:33,800
 His gender changed. He became a woman.

230
00:11:33,800 --> 00:11:38,200
 There was such a strong attraction there, of, you know,

231
00:11:38,200 --> 00:11:41,800
 heterosexual attraction, I guess, that the commentary says

232
00:11:41,800 --> 00:11:45,160
 he changed his gender

233
00:11:45,160 --> 00:11:50,560
 to fit with the thought of a man and a woman together in

234
00:11:50,560 --> 00:11:51,720
 that way.

235
00:11:51,720 --> 00:11:54,730
 But, you know, that's just one of the commentaries that may

236
00:11:54,730 --> 00:11:56,040
 or may not have happened.

237
00:11:56,040 --> 00:11:57,840
 But it's said to have happened right then and there that he

238
00:11:57,840 --> 00:11:59,400
 actually changed genders.

239
00:11:59,520 --> 00:12:03,400
 And later on he... No, and then he gave birth to two sons.

240
00:12:03,400 --> 00:12:06,520
 He became a woman and was like, "Oh, now I'm a woman."

241
00:12:06,520 --> 00:12:08,800
 They chased him out because they didn't recognize him.

242
00:12:08,800 --> 00:12:11,160
 They said, "Who's this crazy woman here?"

243
00:12:11,160 --> 00:12:16,720
 And then he found a husband, got married, had two kids.

244
00:12:16,720 --> 00:12:19,120
 And then he met the Mahakasapa again. He saw him coming

245
00:12:19,120 --> 00:12:21,760
 and he realized how silly he had been.

246
00:12:21,760 --> 00:12:24,600
 And he went up to Mahakasapa and he asked forgiveness.

247
00:12:24,600 --> 00:12:28,040
 When he asked forgiveness, he became a man again.

248
00:12:28,160 --> 00:12:32,880
 And so here he was, the father of having given birth,

249
00:12:32,880 --> 00:12:37,640
 the only man in recorded history to ever have given birth

250
00:12:37,640 --> 00:12:38,680
 to two sons.

251
00:12:38,680 --> 00:12:42,640
 Something like that. It's one of those crazy stories.

252
00:12:42,640 --> 00:12:47,440
 Thank you, Phil. Two-finger wisdom refers to checking the

253
00:12:47,440 --> 00:12:48,360
 doneness of cooking rice.

254
00:12:48,360 --> 00:12:50,720
 That's what the commentary says, I think, no?

255
00:12:50,720 --> 00:12:54,840
 It's some kind of idiom.

256
00:12:57,080 --> 00:12:58,080
 OK?

257
00:12:59,040 --> 00:13:00,040
 Hmm.

