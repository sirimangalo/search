1
00:00:00,000 --> 00:00:05,000
 Meditation progressed before Ithipadas.

2
00:00:05,000 --> 00:00:07,000
 How to succeed.

3
00:00:07,000 --> 00:00:12,000
 How can I succeed in life and in meditation?

4
00:00:12,000 --> 00:00:15,520
 People living in the world have many questions on solving

5
00:00:15,520 --> 00:00:17,000
 problems in worldly life.

6
00:00:17,000 --> 00:00:21,200
 They want to know how to succeed when confronted with the

7
00:00:21,200 --> 00:00:23,000
 challenges of relationships,

8
00:00:23,000 --> 00:00:26,000
 livelihoods and of course, personal well-being.

9
00:00:26,000 --> 00:00:30,050
 The Buddha's teaching on success is applicable to all

10
00:00:30,050 --> 00:00:34,000
 challenges we face, both worldly and religious.

11
00:00:34,000 --> 00:00:38,000
 The Buddha taught four Ithipada, rose to power and mastery.

12
00:00:38,000 --> 00:00:42,000
 They are the four means by which to succeed in any endeavor

13
00:00:42,000 --> 00:00:44,000
, meditation included.

14
00:00:44,000 --> 00:00:47,650
 If you can understand and keep these four principles in

15
00:00:47,650 --> 00:00:48,000
 mind,

16
00:00:48,000 --> 00:00:52,000
 your meditation and your life in general will succeed.

17
00:00:52,000 --> 00:00:56,000
 The first Ithipada principle is chanda, interest.

18
00:00:56,000 --> 00:01:00,110
 A person needs to be interested in what they are doing,

19
00:01:00,110 --> 00:01:03,000
 wanting to do it, being inclined towards it.

20
00:01:03,000 --> 00:01:06,350
 It is hard to succeed at something if it is not something

21
00:01:06,350 --> 00:01:11,000
 you want inclined to do, something you like doing.

22
00:01:11,000 --> 00:01:14,280
 If you dislike a job, this of course works against your

23
00:01:14,280 --> 00:01:15,000
 success.

24
00:01:15,000 --> 00:01:19,620
 If you do not like to meditate, it is not easy to gain the

25
00:01:19,620 --> 00:01:22,000
 benefits of meditation.

26
00:01:22,000 --> 00:01:26,460
 Some people criticize mindfulness meditation because it is

27
00:01:26,460 --> 00:01:28,000
 not comfortable.

28
00:01:28,000 --> 00:01:31,910
 They talk about more comfortable types of meditation as

29
00:01:31,910 --> 00:01:33,000
 being better.

30
00:01:33,000 --> 00:01:37,420
 I do not think anyone would disagree that it is much better

31
00:01:37,420 --> 00:01:40,000
 to have a pleasant meditation.

32
00:01:40,000 --> 00:01:44,000
 It would be much better if we did not have to struggle.

33
00:01:44,000 --> 00:01:48,250
 The problem with comfortable and pleasant meditations is

34
00:01:48,250 --> 00:01:51,000
 that they do not generally challenge you

35
00:01:51,000 --> 00:01:54,000
 or bring you out of your comfort zone.

36
00:01:54,000 --> 00:01:58,000
 As a result, they do not generally lead to enlightenment.

37
00:01:58,000 --> 00:02:02,210
 But how do you practice something that challenges you,

38
00:02:02,210 --> 00:02:06,000
 something that forces you to change without hating it?

39
00:02:06,000 --> 00:02:10,620
 I think only mindfulness meditation has the power to allow

40
00:02:10,620 --> 00:02:12,000
 you to do this.

41
00:02:12,000 --> 00:02:15,860
 I think with anything else, you just have to change what

42
00:02:15,860 --> 00:02:19,250
 you are doing, make it comfortable, make it enjoyable or

43
00:02:19,250 --> 00:02:21,000
 stop doing it.

44
00:02:21,000 --> 00:02:25,000
 Mindfulness is quite different because dislike of anything,

45
00:02:25,000 --> 00:02:29,000
 including meditation, can be an object of mindfulness.

46
00:02:29,000 --> 00:02:34,000
 Disliking is independent of the actual disliked thing.

47
00:02:34,000 --> 00:02:37,870
 If you dislike meditating, it is not directly caused by the

48
00:02:37,870 --> 00:02:39,000
 meditation.

49
00:02:39,000 --> 00:02:44,090
 Your reaction and judgment is independent of the things you

50
00:02:44,090 --> 00:02:46,000
 judge and react to.

51
00:02:46,000 --> 00:02:49,000
 This is part of what we learn in meditation.

52
00:02:49,000 --> 00:02:54,570
 Meditation itself is not exempt from our reactions and is

53
00:02:54,570 --> 00:02:59,000
 not exempt from our study of our reactions.

54
00:02:59,000 --> 00:03:04,070
 If you do not like meditating, it is still a disliking just

55
00:03:04,070 --> 00:03:08,000
 like any other disliking and you can be mindful of that.

56
00:03:08,000 --> 00:03:12,320
 How to overcome the dislikes of meditation is not to force

57
00:03:12,320 --> 00:03:16,280
 yourself to meditate even though you do not want to med

58
00:03:16,280 --> 00:03:17,000
itate.

59
00:03:17,000 --> 00:03:21,000
 The answer is to meditate on the disliking.

60
00:03:21,000 --> 00:03:25,060
 When the disliking becomes the object, it is no longer a

61
00:03:25,060 --> 00:03:26,000
 problem.

62
00:03:26,000 --> 00:03:30,000
 2. The second idipada is viriya.

63
00:03:30,000 --> 00:03:34,170
 With any work you do, if you do not put in effort, you do

64
00:03:34,170 --> 00:03:39,000
 not succeed, but with meditation, it is a bit different.

65
00:03:39,000 --> 00:03:43,010
 If you just try to meditate harder, you will not really

66
00:03:43,010 --> 00:03:47,000
 benefit like you might in other kinds of activity.

67
00:03:47,000 --> 00:03:50,000
 Right effort is a momentary thing.

68
00:03:50,000 --> 00:03:53,000
 It is in the moment that you actually do it.

69
00:03:53,000 --> 00:03:55,000
 You either have it or you do not.

70
00:03:55,000 --> 00:03:58,000
 Are you mindful of the object or are you not?

71
00:03:58,000 --> 00:04:01,000
 If you are mindful, there is effort there as well.

72
00:04:01,000 --> 00:04:04,740
 You have made the choice to be mindful and that choice

73
00:04:04,740 --> 00:04:06,000
 takes effort.

74
00:04:06,000 --> 00:04:10,000
 It is not the effort to walk or sit longer.

75
00:04:10,000 --> 00:04:13,690
 If you do a lot of meditation, whether it is sitting or

76
00:04:13,690 --> 00:04:16,000
 walking and it becomes tiresome,

77
00:04:16,000 --> 00:04:20,000
 then the fatigue has to be the object of your meditation.

78
00:04:20,000 --> 00:04:25,000
 If you are truly mindful, fatigue does not bother you.

79
00:04:25,000 --> 00:04:29,000
 Practically speaking, right effort means just do it.

80
00:04:29,000 --> 00:04:32,240
 When you choose to walk, even when you may feel tired or

81
00:04:32,240 --> 00:04:37,000
 sit even when you feel tired, this is right effort.

82
00:04:37,000 --> 00:04:40,360
 Be mindful of the fatigue rather than letting the fatigue

83
00:04:40,360 --> 00:04:42,000
 control your practice.

84
00:04:42,000 --> 00:04:45,490
 When you get home from work sometimes and feel like you are

85
00:04:45,490 --> 00:04:47,000
 too tired to meditate,

86
00:04:47,000 --> 00:04:51,290
 make a decision to be mindful and focus on the fatigue as

87
00:04:51,290 --> 00:04:55,000
 the object rather than letting it control you.

88
00:04:55,000 --> 00:04:57,000
 This takes effort.

89
00:04:57,000 --> 00:05:00,000
 If you do not do it, it does not get done.

90
00:05:00,000 --> 00:05:02,000
 With any work, this is applicable.

91
00:05:02,000 --> 00:05:05,740
 As in any work, if you do not put out effort, there is no

92
00:05:05,740 --> 00:05:07,000
 hope of success.

93
00:05:07,000 --> 00:05:10,000
 The third idipada is chitta.

94
00:05:10,000 --> 00:05:11,000
 Focus.

95
00:05:11,000 --> 00:05:13,000
 Chitta means keeping your mind on your work.

96
00:05:13,000 --> 00:05:17,000
 In order to succeed, you need to pay attention.

97
00:05:17,000 --> 00:05:19,000
 Focus on what you are doing.

98
00:05:19,000 --> 00:05:23,000
 If you are not focused on your work, not paying attention,

99
00:05:23,000 --> 00:05:25,000
 you will do a sloppy and poor job.

100
00:05:25,000 --> 00:05:28,000
 This is even more true in meditation.

101
00:05:28,000 --> 00:05:31,880
 If you are not paying attention in meditation, your mind

102
00:05:31,880 --> 00:05:36,000
 distracted by abstract conceptual ideas,

103
00:05:36,000 --> 00:05:39,000
 there is no way your meditation can progress.

104
00:05:39,000 --> 00:05:42,900
 If you are not paying attention to the present real

105
00:05:42,900 --> 00:05:47,000
 experiences that present themselves at every moment,

106
00:05:47,000 --> 00:05:51,000
 there is no way to succeed in mindfulness meditation.

107
00:05:51,000 --> 00:05:54,000
 Attention is a sign of mindfulness.

108
00:05:54,000 --> 00:05:57,000
 If you are mindful, you are paying attention.

109
00:05:57,000 --> 00:06:01,150
 Mindfulness grasps the object and confronts it, which goes

110
00:06:01,150 --> 00:06:04,000
 hand in hand with paying attention.

111
00:06:04,000 --> 00:06:06,990
 Whatever you are going to do in life, if you want to

112
00:06:06,990 --> 00:06:11,000
 succeed at it, you have to be there and be present.

113
00:06:11,000 --> 00:06:15,000
 This is one of the great benefits of mindfulness meditation

114
00:06:15,000 --> 00:06:19,040
, that it improves your attention span as part of the

115
00:06:19,040 --> 00:06:20,000
 training.

116
00:06:20,000 --> 00:06:21,000
 Four.

117
00:06:21,000 --> 00:06:24,000
 The fourth idipada is vimangsa, consideration.

118
00:06:24,000 --> 00:06:28,420
 Vimangsa is not like chitta, which refers to the focus on

119
00:06:28,420 --> 00:06:30,000
 the task at hand.

120
00:06:30,000 --> 00:06:32,990
 When you focus on something without reflection or

121
00:06:32,990 --> 00:06:37,300
 consideration, it is easy to become oblivious to the

122
00:06:37,300 --> 00:06:39,000
 imperfections of your words.

123
00:06:39,000 --> 00:06:42,000
 This may even reinforce bad habits.

124
00:06:42,000 --> 00:06:47,050
 Someone might practice walking and sitting meditation for

125
00:06:47,050 --> 00:06:50,000
 many hours without great benefit,

126
00:06:50,000 --> 00:06:54,010
 as they have failed to recognize their lack of mindfulness

127
00:06:54,010 --> 00:06:58,000
 due to lack of reflection about their mental activity.

128
00:06:58,000 --> 00:07:02,490
 A part of the benefit of interviews with a teacher during a

129
00:07:02,490 --> 00:07:06,610
 meditation course is that the meditator is forced to

130
00:07:06,610 --> 00:07:08,000
 reflect on their practice

131
00:07:08,000 --> 00:07:12,000
 when the teacher asks how their practice was that day.

132
00:07:12,000 --> 00:07:16,590
 Without such questioning, it is easy to reinforce bad

133
00:07:16,590 --> 00:07:21,000
 habits and get stuck in unmindful mental activity.

134
00:07:21,000 --> 00:07:24,960
 Vimangsa is what allows a meditator to correct their

135
00:07:24,960 --> 00:07:26,000
 practice.

136
00:07:26,000 --> 00:07:31,000
 The same goes with anywhere you might undertake in life.

137
00:07:31,000 --> 00:07:33,000
 You cannot just work like an ox.

138
00:07:33,000 --> 00:07:35,000
 You have to be clever.

139
00:07:35,000 --> 00:07:39,110
 You have to be able to reflect and consider that your

140
00:07:39,110 --> 00:07:41,000
 methods might be wrong.

141
00:07:41,000 --> 00:07:43,000
 That maybe you need to change something.

142
00:07:43,000 --> 00:07:48,000
 Vimangsa involves the ability to adapt and adjust.

143
00:07:48,000 --> 00:07:52,500
 You can see this in the case where a workup performs the

144
00:07:52,500 --> 00:07:57,070
 same job every day without ever thinking that maybe they

145
00:07:57,070 --> 00:08:00,000
 could improve if they did something different.

146
00:08:00,000 --> 00:08:04,140
 And quite often, if anyone suggests that they could improve

147
00:08:04,140 --> 00:08:07,990
 by changing their methods, they become upset, thinking that

148
00:08:07,990 --> 00:08:10,000
 it works fine the way they do it.

149
00:08:10,000 --> 00:08:13,970
 This sort of inability to change and adapt based on

150
00:08:13,970 --> 00:08:18,810
 considered reflection will inevitably hinder your ability

151
00:08:18,810 --> 00:08:20,000
 to succeed.

152
00:08:20,000 --> 00:08:24,670
 Flexibility and adaptability are important both in the

153
00:08:24,670 --> 00:08:27,000
 world and in meditation.

154
00:08:27,000 --> 00:08:31,490
 They are signs of someone who is mindful, adaptable, not

155
00:08:31,490 --> 00:08:35,000
 attached, and not stuck up or egotistical.

156
00:08:35,000 --> 00:08:39,230
 When confronted with the need to change, they do not get

157
00:08:39,230 --> 00:08:40,000
 upset.

158
00:08:40,000 --> 00:08:42,000
 They change and they adapt.

159
00:08:42,000 --> 00:08:47,530
 Vimangsa is the ability to consider and adjust rather than

160
00:08:47,530 --> 00:08:51,000
 just bow on ignorantly like an ox.

