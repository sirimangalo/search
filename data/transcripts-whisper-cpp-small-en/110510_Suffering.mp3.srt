1
00:00:00,000 --> 00:00:04,500
 Hello, welcome back to Ask a Monk.

2
00:00:04,500 --> 00:00:13,290
 Today's question is on whether I think that there is more

3
00:00:13,290 --> 00:00:18,040
 suffering in America, this is

4
00:00:18,040 --> 00:00:23,100
 the question, where it is almost impossible not to find

5
00:00:23,100 --> 00:00:25,920
 food and I guess because we've

6
00:00:25,920 --> 00:00:31,100
 or the people in America have developed their society or

7
00:00:31,100 --> 00:00:34,200
 more suffering in a tribal society.

8
00:00:34,200 --> 00:00:37,240
 This is the question.

9
00:00:37,240 --> 00:00:43,340
 So first of all I'd like to sort of expand the question

10
00:00:43,340 --> 00:00:46,680
 because I think the important

11
00:00:46,680 --> 00:00:53,590
 point is whether, not whether America now or America when

12
00:00:53,590 --> 00:00:56,640
 it was owned by the Native

13
00:00:56,640 --> 00:01:00,760
 American or First Nations people is better.

14
00:01:00,760 --> 00:01:06,680
 The question is whether there's more suffering to be found

15
00:01:06,680 --> 00:01:09,920
 in a society with technological

16
00:01:09,920 --> 00:01:18,050
 advancements and politics and government and roads and cars

17
00:01:18,050 --> 00:01:21,640
 and computers and iPods and

18
00:01:21,640 --> 00:01:30,600
 so on or whether there is more suffering in a lifestyle

19
00:01:30,600 --> 00:01:38,120
 that is simple, unrefined, unadvanced

20
00:01:38,120 --> 00:01:44,920
 and unorganized, organized on a level similar to a tribe.

21
00:01:44,920 --> 00:01:48,930
 And it's a difficult question but why it's difficult is

22
00:01:48,930 --> 00:01:51,120
 because I think there's merits

23
00:01:51,120 --> 00:01:52,120
 to both sides.

24
00:01:52,120 --> 00:01:58,680
 I mean I want to be able to say that material advancement

25
00:01:58,680 --> 00:02:02,520
 in some way heightens people's

26
00:02:02,520 --> 00:02:09,770
 ability or interest in things like science, things like

27
00:02:09,770 --> 00:02:14,840
 objective investigation and analysis

28
00:02:14,840 --> 00:02:15,840
 and so on.

29
00:02:15,840 --> 00:02:18,280
 I mean the education side of things.

30
00:02:18,280 --> 00:02:22,010
 I want to be able to say that that's a good thing to some

31
00:02:22,010 --> 00:02:22,880
 degree.

32
00:02:22,880 --> 00:02:27,510
 But I think given the choice I would have to say that there

33
00:02:27,510 --> 00:02:29,960
's probably less suffering

34
00:02:29,960 --> 00:02:36,240
 to be found in a less advanced society.

35
00:02:36,240 --> 00:02:40,300
 And I think we have to advance this beyond the tribal

36
00:02:40,300 --> 00:02:42,840
 societies of say North America

37
00:02:42,840 --> 00:02:45,560
 in olden times.

38
00:02:45,560 --> 00:02:49,700
 Now there are still some tribal societies but not really to

39
00:02:49,700 --> 00:02:51,560
 the extent that you would

40
00:02:51,560 --> 00:02:53,800
 have found some time ago.

41
00:02:53,800 --> 00:02:57,330
 Because I think this is a rather limited sort of way of

42
00:02:57,330 --> 00:02:58,760
 looking at things.

43
00:02:58,760 --> 00:03:01,930
 There are many societies or cultures or groups of

44
00:03:01,930 --> 00:03:04,760
 individuals, groups of people living in

45
00:03:04,760 --> 00:03:09,530
 the world who are living in limited technological

46
00:03:09,530 --> 00:03:11,960
 advancement and so on.

47
00:03:11,960 --> 00:03:16,650
 I think living in Thailand and living in Sri Lanka I've

48
00:03:16,650 --> 00:03:19,240
 been able to see some of these

49
00:03:19,240 --> 00:03:20,240
 groups.

50
00:03:20,240 --> 00:03:24,760
 I mean Thailand for example is quite advanced in many ways.

51
00:03:24,760 --> 00:03:28,940
 But in the villages, in the countryside you'll still find

52
00:03:28,940 --> 00:03:31,560
 people who are very technologically

53
00:03:31,560 --> 00:03:38,200
 behind and living in fairly unorganized manners in terms of

54
00:03:38,200 --> 00:03:42,000
 political structure and so on.

55
00:03:42,000 --> 00:03:48,780
 And here in Sri Lanka even more so you don't see the impact

56
00:03:48,780 --> 00:03:52,040
 of modern society to such a

57
00:03:52,040 --> 00:03:55,480
 great extent as you would in the West.

58
00:03:55,480 --> 00:04:02,020
 And I think beneficially at least to some extent that it

59
00:04:02,020 --> 00:04:05,560
 has allowed people to be free

60
00:04:05,560 --> 00:04:07,720
 from some of the suffering.

61
00:04:07,720 --> 00:04:11,000
 So stacking up the sufferings I think the obvious suffering

62
00:04:11,000 --> 00:04:12,320
 that was pointed out in

63
00:04:12,320 --> 00:04:20,720
 the question is living in a tribal society or uncivilized "

64
00:04:20,720 --> 00:04:22,120
society".

65
00:04:22,120 --> 00:04:25,130
 It's going to be more difficult to find food and I don't

66
00:04:25,130 --> 00:04:26,840
 really think this is that big

67
00:04:26,840 --> 00:04:27,840
 of a deal.

68
00:04:27,840 --> 00:04:31,740
 In fact I probably would care to wager that North America

69
00:04:31,740 --> 00:04:33,600
 it was a lot easier to find

70
00:04:33,600 --> 00:04:38,340
 food before technology came into effect and now the food

71
00:04:38,340 --> 00:04:41,160
 quality has gone down and people

72
00:04:41,160 --> 00:04:44,080
 have to work harder for it and so on.

73
00:04:44,080 --> 00:04:49,000
 I would say that anyway that's maybe a bit speculative.

74
00:04:49,000 --> 00:04:53,970
 But the real suffering I think that comes, what's being

75
00:04:53,970 --> 00:04:56,720
 pointed to here is the lack of

76
00:04:56,720 --> 00:05:03,600
 amenities, living in rough conditions, having to put up

77
00:05:03,600 --> 00:05:07,880
 with insects, having to put up with

78
00:05:07,880 --> 00:05:11,520
 leeches, having to put up with snakes and scorpions, having

79
00:05:11,520 --> 00:05:13,480
 to put up with mosquitoes because you

80
00:05:13,480 --> 00:05:17,690
 don't have screens on your windows or you don't have the

81
00:05:17,690 --> 00:05:20,000
 technology to prevent these

82
00:05:20,000 --> 00:05:24,300
 things, having to put up with limited medical facilities

83
00:05:24,300 --> 00:05:25,160
 and so on.

84
00:05:25,160 --> 00:05:29,690
 There are a lot of sufferings that come with living in a

85
00:05:29,690 --> 00:05:32,320
 simple manner and I think it's

86
00:05:32,320 --> 00:05:36,790
 fair to point out that the monastic life is by its nature

87
00:05:36,790 --> 00:05:40,600
 the Buddhist monastic life is

88
00:05:40,600 --> 00:05:45,300
 of a sort of simple life and as a result there is a certain

89
00:05:45,300 --> 00:05:47,440
 amount of suffering.

90
00:05:47,440 --> 00:05:49,360
 This is why I think this is an interesting question and why

91
00:05:49,360 --> 00:05:50,240
 I'd like to take some time

92
00:05:50,240 --> 00:05:54,640
 to answer it because I have to personally go through some

93
00:05:54,640 --> 00:05:56,760
 of the sufferings that the

94
00:05:56,760 --> 00:06:00,190
 villagers here even have to go through, perhaps even more

95
00:06:00,190 --> 00:06:02,200
 so than they have to go through.

96
00:06:02,200 --> 00:06:07,570
 I might be going through some of the sufferings that you

97
00:06:07,570 --> 00:06:11,040
 might expect from a tribal society

98
00:06:11,040 --> 00:06:13,400
 because for me food is limited.

99
00:06:13,400 --> 00:06:16,590
 I only eat one meal a day so you could say that's somehow

100
00:06:16,590 --> 00:06:18,400
 similar to the suffering that

101
00:06:18,400 --> 00:06:21,020
 is brought about by having to hunt for your food not

102
00:06:21,020 --> 00:06:22,880
 knowing where you're going to get

103
00:06:22,880 --> 00:06:27,150
 your next meal not having a farmland or a grocery store or

104
00:06:27,150 --> 00:06:29,160
 money that you can easily

105
00:06:29,160 --> 00:06:34,220
 access these things with and having to put up with limited

106
00:06:34,220 --> 00:06:36,400
 medical facilities.

107
00:06:36,400 --> 00:06:43,250
 But I think the real key here is that or a real good way to

108
00:06:43,250 --> 00:06:46,680
 answer this question is to

109
00:06:46,680 --> 00:06:51,410
 point out the fact that so many people undertake this

110
00:06:51,410 --> 00:06:54,920
 lifestyle on purpose and voluntarily

111
00:06:54,920 --> 00:06:57,070
 and why would they do it if it was just going to lead to

112
00:06:57,070 --> 00:06:58,000
 their suffering.

113
00:06:58,000 --> 00:07:01,050
 I think that's a question that a lot of people ask why do

114
00:07:01,050 --> 00:07:02,640
 people decide to live in caves

115
00:07:02,640 --> 00:07:09,840
 and wear robes, a single set of robes etc etc eating only

116
00:07:09,840 --> 00:07:13,080
 one meal a day putting up with

117
00:07:13,080 --> 00:07:15,360
 all of these difficulties.

118
00:07:15,360 --> 00:07:20,920
 And I think it's because it's a trade off.

119
00:07:20,920 --> 00:07:26,870
 If you give up or if you want to be free from all of the

120
00:07:26,870 --> 00:07:30,680
 stresses and all of the sufferings

121
00:07:30,680 --> 00:07:35,620
 that are on the other side of the fence then you have to

122
00:07:35,620 --> 00:07:38,400
 you can't expect to have all the

123
00:07:38,400 --> 00:07:40,240
 amenities I mean they go hand in hand.

124
00:07:40,240 --> 00:07:43,600
 The sufferings on the other side of a technologically

125
00:07:43,600 --> 00:07:46,440
 advanced society are the stresses of having

126
00:07:46,440 --> 00:07:50,690
 to work a nine to five job, the stresses of having to think

127
00:07:50,690 --> 00:07:52,920
 a lot, of having to deal with

128
00:07:52,920 --> 00:07:56,150
 people, crowds of people, having to deal with traffic,

129
00:07:56,150 --> 00:07:58,360
 having to deal with the complications

130
00:07:58,360 --> 00:08:04,940
 that come with technology like fixing your car and your

131
00:08:04,940 --> 00:08:08,360
 house and all of the laws and

132
00:08:08,360 --> 00:08:12,590
 having to deal with laws and lawyers and people suing you

133
00:08:12,590 --> 00:08:13,560
 and so on.

134
00:08:13,560 --> 00:08:16,650
 There's a huge burden of stress having to be worried about

135
00:08:16,650 --> 00:08:18,400
 thieves, having to be worried

136
00:08:18,400 --> 00:08:24,310
 about crooks, not crook but people cheating you, having to

137
00:08:24,310 --> 00:08:27,040
 be worried about your boss,

138
00:08:27,040 --> 00:08:31,310
 your co-workers, your clients and so on and a million other

139
00:08:31,310 --> 00:08:32,160
 things.

140
00:08:32,160 --> 00:08:35,280
 I mean no matter what work you apply yourself to in an

141
00:08:35,280 --> 00:08:38,320
 advanced society there are more complications.

142
00:08:38,320 --> 00:08:44,240
 And as a result more stress and as a result more suffering.

143
00:08:44,240 --> 00:08:50,020
 So I think the trade off there is quite worth it in my mind

144
00:08:50,020 --> 00:08:53,040
 because I'd like to you know

145
00:08:53,040 --> 00:08:55,920
 the answer for me is quite clear because having put up with

146
00:08:55,920 --> 00:08:57,560
 all these sufferings of having

147
00:08:57,560 --> 00:09:02,160
 to have mosquitoes bite me and who knows what bite me and

148
00:09:02,160 --> 00:09:06,240
 having to put up with leeches and

149
00:09:06,240 --> 00:09:09,130
 you know the worries that I have to put up with, worrying

150
00:09:09,130 --> 00:09:10,640
 about stepping on a snake,

151
00:09:10,640 --> 00:09:14,480
 worrying about that four foot lizard down below me and the

152
00:09:14,480 --> 00:09:16,360
 monkey scrapping on my head

153
00:09:16,360 --> 00:09:17,760
 and so on.

154
00:09:17,760 --> 00:09:22,720
 I really don't feel upset or affected by these things.

155
00:09:22,720 --> 00:09:26,670
 They're a physical reality and they're much more physical

156
00:09:26,670 --> 00:09:28,400
 than they are mental.

157
00:09:28,400 --> 00:09:32,040
 I feel so much more at peace here living in the jungle

158
00:09:32,040 --> 00:09:34,560
 which you know might look peaceful

159
00:09:34,560 --> 00:09:38,700
 but it's actually quite dangerous and keeps you on your

160
00:09:38,700 --> 00:09:40,280
 toes in many ways.

161
00:09:40,280 --> 00:09:43,570
 Even during meditation you have to, there's these little

162
00:09:43,570 --> 00:09:45,200
 bugs that you can't even see

163
00:09:45,200 --> 00:09:46,200
 them.

164
00:09:46,200 --> 00:09:49,200
 They're worse than mosquitoes and they bite you.

165
00:09:49,200 --> 00:09:54,010
 But it's not a stress for me, not in the same way as living

166
00:09:54,010 --> 00:09:56,760
 in say the big cities of Colombo,

167
00:09:56,760 --> 00:10:02,180
 Bangkok, Los Angeles, the stresses that's involved there is

168
00:10:02,180 --> 00:10:04,920
 an order of magnitude greater.

169
00:10:04,920 --> 00:10:08,410
 So I'm not sure this exactly answers the question but this

170
00:10:08,410 --> 00:10:10,280
 is how I would prefer to approach

171
00:10:10,280 --> 00:10:11,280
 it.

172
00:10:11,280 --> 00:10:18,410
 I think there's far less stress to be had from a simple

173
00:10:18,410 --> 00:10:22,760
 lifestyle because the stresses

174
00:10:22,760 --> 00:10:28,730
 are much more physical than they are mental and the mental

175
00:10:28,730 --> 00:10:30,720
 stress is far less.

176
00:10:30,720 --> 00:10:35,600
 Another thing about living in a simple society, in a simple

177
00:10:35,600 --> 00:10:38,360
 lifestyle is that it's much more

178
00:10:38,360 --> 00:10:41,720
 in tune with our programming physically.

179
00:10:41,720 --> 00:10:46,290
 I mean we're still very much related to the monkeys and so

180
00:10:46,290 --> 00:10:48,840
 this kind of atmosphere jives

181
00:10:48,840 --> 00:10:52,100
 with us, seeing the trees, why people go to nature and they

182
00:10:52,100 --> 00:10:53,680
 feel peaceful all of a sudden

183
00:10:53,680 --> 00:10:55,760
 is because of how, it's not because there's anything

184
00:10:55,760 --> 00:10:57,360
 special there, it's because of what's

185
00:10:57,360 --> 00:10:59,440
 not there.

186
00:10:59,440 --> 00:11:02,830
 There's nothing jarring with your experience, no horns

187
00:11:02,830 --> 00:11:05,520
 blasting, no bright lights, no beautiful

188
00:11:05,520 --> 00:11:09,600
 pictures catching your attention, no ugly sights and so on.

189
00:11:09,600 --> 00:11:14,600
 It's very peaceful, very calm, very natural, right?

190
00:11:14,600 --> 00:11:17,000
 I mean that's why we're here.

191
00:11:17,000 --> 00:11:23,120
 I think you'll find that in a traditional society.

192
00:11:23,120 --> 00:11:26,840
 I think this is borne out by the Buddhist teaching, it's

193
00:11:26,840 --> 00:11:29,640
 borne out by the examples obviously

194
00:11:29,640 --> 00:11:32,920
 of those people who have ordained as monks.

195
00:11:32,920 --> 00:11:35,420
 There was a story in the Tapitaka, in the Buddhist teaching

196
00:11:35,420 --> 00:11:36,480
 that we always go back to

197
00:11:36,480 --> 00:11:38,400
 I think in the commentaries actually.

198
00:11:38,400 --> 00:11:42,740
 I'm not sure, well at least it's in the commentaries about

199
00:11:42,740 --> 00:11:46,880
 this king, a relative of a king.

200
00:11:46,880 --> 00:11:52,240
 A king who became a monk, Mahakapina, and after he became a

201
00:11:52,240 --> 00:11:55,200
 monk he would sit under this tree,

202
00:11:55,200 --> 00:11:56,960
 he used to be a king, right?

203
00:11:56,960 --> 00:12:01,000
 So after he became a monk he would sit under a tree and go,

204
00:12:01,000 --> 00:12:03,720
 "Aho sukang, aho sukang, aho

205
00:12:03,720 --> 00:12:08,710
 sukang," which means, "Oh what happiness, oh what bliss, oh

206
00:12:08,710 --> 00:12:10,400
 what happiness."

207
00:12:10,400 --> 00:12:13,030
 So you might say he was actually contemplating the

208
00:12:13,030 --> 00:12:15,480
 happiness, just looking at it and examining

209
00:12:15,480 --> 00:12:19,800
 it much in the same way that we say to ourselves, "Happy,

210
00:12:19,800 --> 00:12:20,640
 happy."

211
00:12:20,640 --> 00:12:24,270
 But the monks heard him saying this and remarking this and

212
00:12:24,270 --> 00:12:26,160
 they took it the wrong way.

213
00:12:26,160 --> 00:12:28,990
 They took it to me and he was, for some reason they took it

214
00:12:28,990 --> 00:12:30,640
 that he must be thinking about

215
00:12:30,640 --> 00:12:32,040
 his kingly happiness.

216
00:12:32,040 --> 00:12:36,590
 He was remembering, "Oh what happiness it was to be a king

217
00:12:36,590 --> 00:12:36,680
."

218
00:12:36,680 --> 00:12:38,880
 And so they took him to the Buddha and the Buddha asked him

219
00:12:38,880 --> 00:12:39,720
, "What was it?"

220
00:12:39,720 --> 00:12:44,450
 And he said, "No, when I was a king I had to worry about

221
00:12:44,450 --> 00:12:47,640
 all of these stresses and concerns.

222
00:12:47,640 --> 00:12:50,160
 My life was always in danger of assassination.

223
00:12:50,160 --> 00:12:54,500
 I had all this treasure that I had to guard, all these

224
00:12:54,500 --> 00:12:56,680
 ministers that I had to manage and

225
00:12:56,680 --> 00:12:59,170
 stop from cheating me and all the people who would come to

226
00:12:59,170 --> 00:13:00,600
 me with their problems and all

227
00:13:00,600 --> 00:13:04,720
 the laws I had to enforce and so on and so on.

228
00:13:04,720 --> 00:13:05,800
 And I have none of that now.

229
00:13:05,800 --> 00:13:10,280
 I have this tree, three robes, my bowl, my tree.

230
00:13:10,280 --> 00:13:13,970
 And this is the greatest happiness I've ever known in my

231
00:13:13,970 --> 00:13:15,320
 life," he said.

232
00:13:15,320 --> 00:13:20,840
 So there's a story to go with this question.

233
00:13:20,840 --> 00:13:24,960
 I think one final note is that regardless of the answer to

234
00:13:24,960 --> 00:13:27,440
 this question, whether there's

235
00:13:27,440 --> 00:13:31,850
 more suffering in either one, I think the point is that

236
00:13:31,850 --> 00:13:35,040
 whatever lifestyle you can undertake,

237
00:13:35,040 --> 00:13:38,530
 not that has the least amount of suffering, but which has

238
00:13:38,530 --> 00:13:40,840
 the least amount of unwholesomeness

239
00:13:40,840 --> 00:13:43,600
 involved in it, that is the best lifestyle.

240
00:13:43,600 --> 00:13:46,520
 So for those people who might be discouraged then by the

241
00:13:46,520 --> 00:13:48,080
 fact that they have to live in

242
00:13:48,080 --> 00:13:51,120
 a technologically advanced society as a Buddhist and they

243
00:13:51,120 --> 00:13:52,920
 feel like they should be living in

244
00:13:52,920 --> 00:13:56,120
 the forest, I don't mean to say that there's anything wrong

245
00:13:56,120 --> 00:13:57,560
 with living in the city.

246
00:13:57,560 --> 00:13:59,000
 I've lived in Los Angeles.

247
00:13:59,000 --> 00:14:04,340
 I've gone on alms round in North Hollywood and in places in

248
00:14:04,340 --> 00:14:06,800
 Bangkok, in Colombo.

249
00:14:06,800 --> 00:14:10,750
 I've lived in these places in fairly chaotic and stressful

250
00:14:10,750 --> 00:14:11,880
 situations.

251
00:14:11,880 --> 00:14:15,040
 But the point is to live your life in such a way that it's

252
00:14:15,040 --> 00:14:16,920
 free from unwholesomeness.

253
00:14:16,920 --> 00:14:20,850
 And this is where the question gets interesting because

254
00:14:20,850 --> 00:14:23,440
 many tribal societies are involved

255
00:14:23,440 --> 00:14:26,400
 in what Buddhists would call great unwholesomeness.

256
00:14:26,400 --> 00:14:30,280
 Their way of lifestyle is hunting as an example.

257
00:14:30,280 --> 00:14:36,110
 And though they do it to survive, it's by nature caught up

258
00:14:36,110 --> 00:14:39,600
 with these states of cruelty,

259
00:14:39,600 --> 00:14:45,680
 states of anger and delusion.

260
00:14:45,680 --> 00:14:49,590
 And as a result, it keeps them bound to the wheel of life

261
00:14:49,590 --> 00:14:51,680
 that they're going to have to

262
00:14:51,680 --> 00:14:54,040
 take turns with the deer.

263
00:14:54,040 --> 00:14:56,880
 This is what the First Nations people believe.

264
00:14:56,880 --> 00:15:00,960
 They still believe that you take turns.

265
00:15:00,960 --> 00:15:03,840
 I hunt the deer this life and in the next life I'm the deer

266
00:15:03,840 --> 00:15:05,120
 and they hunt me and so

267
00:15:05,120 --> 00:15:06,200
 on.

268
00:15:06,200 --> 00:15:08,700
 There are these kinds of beliefs and this is really in line

269
00:15:08,700 --> 00:15:10,200
 with the Buddhist teachings.

270
00:15:10,200 --> 00:15:14,300
 So I mean, if you're fine with that, having to be hunted

271
00:15:14,300 --> 00:15:16,560
 every other lifetime or every

272
00:15:16,560 --> 00:15:19,800
 few lifetimes, then so be it.

273
00:15:19,800 --> 00:15:23,650
 But from a Buddhist point of view, this isn't the way out

274
00:15:23,650 --> 00:15:24,800
 of suffering.

275
00:15:24,800 --> 00:15:26,800
 It's because there's a lot of suffering.

276
00:15:26,800 --> 00:15:30,510
 Tribes, of course, then there's tribal warfare and they're

277
00:15:30,510 --> 00:15:32,240
 not free from all of this.

278
00:15:32,240 --> 00:15:34,280
 And we're not free from it on the other side.

279
00:15:34,280 --> 00:15:37,800
 That's living in technologically advanced societies.

280
00:15:37,800 --> 00:15:41,040
 Living in Canada, I wasn't free from these things.

281
00:15:41,040 --> 00:15:42,040
 I was hunting as well.

282
00:15:42,040 --> 00:15:45,640
 I didn't need to, but I went hunting with my father.

283
00:15:45,640 --> 00:15:50,140
 We engage in a lot of unwholesomeness in advanced societies

284
00:15:50,140 --> 00:15:50,440
.

285
00:15:50,440 --> 00:15:52,880
 So that's the most important.

286
00:15:52,880 --> 00:15:54,920
 You can live in a city, you can live in nature.

287
00:15:54,920 --> 00:15:58,740
 The reason why we choose to live in nature is I think

288
00:15:58,740 --> 00:16:01,480
 overall, the amount of peace that

289
00:16:01,480 --> 00:16:04,650
 you have is the one side, but there's also a lot less unwh

290
00:16:04,650 --> 00:16:05,680
olesomeness.

291
00:16:05,680 --> 00:16:10,000
 That you're free from having to deal with other people's un

292
00:16:10,000 --> 00:16:12,040
wholesomeness and having

293
00:16:12,040 --> 00:16:16,360
 to see all these beautiful, attractive sites, wanting to

294
00:16:16,360 --> 00:16:18,760
 get this, wanting to get that,

295
00:16:18,760 --> 00:16:21,040
 being annoyed by people, being upset by people.

296
00:16:21,040 --> 00:16:25,520
 So you're able to focus much clearer on the very core

297
00:16:25,520 --> 00:16:28,720
 aspects of existence, what really

298
00:16:28,720 --> 00:16:33,080
 is reality, without having to get caught up in unwholesome

299
00:16:33,080 --> 00:16:35,640
 mind states of stress and addiction

300
00:16:35,640 --> 00:16:37,840
 and so on.

301
00:16:37,840 --> 00:16:38,840
 And I think that's clear.

302
00:16:38,840 --> 00:16:42,160
 I think being here in the forest is much better for my

303
00:16:42,160 --> 00:16:44,640
 meditation practice than being in the

304
00:16:44,640 --> 00:16:45,640
 city.

305
00:16:45,640 --> 00:16:49,470
 But in the end, it's not the biggest concern and I'm not

306
00:16:49,470 --> 00:16:51,520
 afraid to live in the city if

307
00:16:51,520 --> 00:16:55,450
 I have to, because for me the most important is to continue

308
00:16:55,450 --> 00:16:57,280
 the meditation practice and

309
00:16:57,280 --> 00:17:00,650
 to learn more about my reactions and my interactions with

310
00:17:00,650 --> 00:17:02,040
 the world around me.

311
00:17:02,040 --> 00:17:05,520
 And sometimes that can be tested by suffering.

312
00:17:05,520 --> 00:17:08,290
 Again, the Buddhist teaching is not to run away from

313
00:17:08,290 --> 00:17:10,080
 suffering, it's to learn and to

314
00:17:10,080 --> 00:17:12,160
 come to understand suffering.

315
00:17:12,160 --> 00:17:14,690
 When you understand suffering, you'll understand what is

316
00:17:14,690 --> 00:17:16,160
 really causing suffering, that it's

317
00:17:16,160 --> 00:17:19,390
 not the objects, it's your relationship and your reactions

318
00:17:19,390 --> 00:17:21,160
 to them, your attachment to

319
00:17:21,160 --> 00:17:24,260
 things being in a certain way and not being in another way,

320
00:17:24,260 --> 00:17:26,000
 your attachment to the objects

321
00:17:26,000 --> 00:17:27,960
 of the sense and so on.

322
00:17:27,960 --> 00:17:30,410
 When you can be free from that, it doesn't matter where you

323
00:17:30,410 --> 00:17:31,920
 live, whether it be in a cave

324
00:17:31,920 --> 00:17:34,120
 or in an apartment complex.

325
00:17:34,120 --> 00:17:37,150
 So there's the answer to your question, thanks and have a

326
00:17:37,150 --> 00:17:37,880
 good day.

