1
00:00:00,000 --> 00:00:07,000
 Okay, so my meditation DVD advertising.

2
00:00:07,000 --> 00:00:12,000
 We give out for free. This is what I send around the world.

3
00:00:12,000 --> 00:00:19,000
 So people sign up on my website and we give them a two-disc

4
00:00:19,000 --> 00:00:21,000
 set on how to meditate totally free.

5
00:00:21,000 --> 00:00:27,000
 This is advertising goodness, not advertising business.

6
00:00:27,000 --> 00:00:32,190
 But anyway, that's just an added. Okay, so what am I

7
00:00:32,190 --> 00:00:33,000
 reading?

8
00:00:33,000 --> 00:00:37,360
 I'm reading through this. This is the middle length disc

9
00:00:37,360 --> 00:00:39,000
ourses of the Buddha.

10
00:00:39,000 --> 00:00:44,440
 I'm not even sure if you can show that. Anyway, this is the

11
00:00:44,440 --> 00:00:46,000
 middle length discourses of the Buddha.

12
00:00:46,000 --> 00:00:49,570
 Middle length discourses, it's a collection of discourses

13
00:00:49,570 --> 00:00:51,000
 taught by the Buddha.

14
00:00:51,000 --> 00:00:56,000
 There's 152 of them. Now, I've read them all through before

15
00:00:56,000 --> 00:00:56,000
.

16
00:00:56,000 --> 00:00:59,170
 But now what I'm doing is I'm going over them and using the

17
00:00:59,170 --> 00:01:03,000
 computer with this program that translates them.

18
00:01:03,000 --> 00:01:06,000
 And it's got them in there. It's got the Pali text.

19
00:01:06,000 --> 00:01:08,710
 So I have the Pali language in the computer and then I have

20
00:01:08,710 --> 00:01:10,000
 the English language here.

21
00:01:10,000 --> 00:01:13,340
 So I'll be reading through. Generally, I'll read through

22
00:01:13,340 --> 00:01:14,000
 the Pali first.

23
00:01:14,000 --> 00:01:18,000
 And when I get stuck, I'll move back to the English.

24
00:01:18,000 --> 00:01:20,980
 So I'm using this as reference right now. I'm not really

25
00:01:20,980 --> 00:01:25,000
 reading it. I'm reading the Pali.

26
00:01:25,000 --> 00:01:30,380
 Maybe I can give you a screenshot of that. Let's go through

27
00:01:30,380 --> 00:01:32,000
 the books first.

28
00:01:32,000 --> 00:01:35,550
 The second thing I'm reading sort of on and off. I use this

29
00:01:35,550 --> 00:01:37,000
 one as reference as well.

30
00:01:37,000 --> 00:01:43,000
 This is a Thai book, "Vipassana kamataan". I'll read you.

31
00:01:43,000 --> 00:01:48,620
 "Vipassana kamataan". This book is about insight meditation

32
00:01:48,620 --> 00:01:49,000
.

33
00:01:49,000 --> 00:01:55,000
 "Vipassana kamataan" is about insight meditation.

34
00:01:55,000 --> 00:02:00,000
 "Vipassana kamataan" is about insight meditation.

35
00:02:00,000 --> 00:02:06,000
 "Vipassana kamataan" is about insight meditation.

36
00:02:06,000 --> 00:02:13,000
 "Vipassana kamataan" is about insight meditation.

37
00:02:13,000 --> 00:02:19,000
 "Juntin jurtmai paitang titon paso".

38
00:02:19,000 --> 00:02:24,560
 The meaning of this is "tula" which means business or work

39
00:02:24,560 --> 00:02:28,000
 in Buddhism.

40
00:02:28,000 --> 00:02:31,000
 So then they have the work "tula" which is a Pali word.

41
00:02:31,000 --> 00:02:33,000
 And then he's going to explain what it means.

42
00:02:33,000 --> 00:02:36,460
 So what is the meaning of the word "work"? It can be

43
00:02:36,460 --> 00:02:38,000
 translated in many ways.

44
00:02:38,000 --> 00:02:43,900
 First of all it means a "yoke". The word "tula" can be

45
00:02:43,900 --> 00:02:45,000
 translated as "yoke".

46
00:02:45,000 --> 00:02:47,000
 But it means it's something that you're yoked to.

47
00:02:47,000 --> 00:02:50,290
 So as a monk for instance where you're yoked to the

48
00:02:50,290 --> 00:02:52,000
 practice of meditation.

49
00:02:52,000 --> 00:02:54,500
 He's going to talk about what are the two "tulas". The two

50
00:02:54,500 --> 00:02:56,000
 works in Buddhism.

51
00:02:56,000 --> 00:03:02,000
 One is to study.

52
00:03:02,000 --> 00:03:09,260
 "Bante imasaming sasane kati turani" in this religion, in

53
00:03:09,260 --> 00:03:10,000
 this teaching.

54
00:03:10,000 --> 00:03:17,000
 How many works are there? How many businesses? How many...

55
00:03:17,000 --> 00:03:19,000
 I don't know the English word.

56
00:03:19,000 --> 00:03:22,690
 How many things are there that you have to do? Or how many

57
00:03:22,690 --> 00:03:24,000
 tasks?

58
00:03:24,000 --> 00:03:27,000
 That's a good word. How many tasks are there?

59
00:03:27,000 --> 00:03:34,790
 When the Buddha says, "Gandaturang vipassana turanti twe y

60
00:03:34,790 --> 00:03:36,000
ewa durani piku"

61
00:03:36,000 --> 00:03:45,000
 He says, "Look here, old monk, there are two gandatura"

62
00:03:45,000 --> 00:03:47,000
 which means study.

63
00:03:47,000 --> 00:03:50,000
 "Vipassana tura" means the task of insight.

64
00:03:50,000 --> 00:03:54,270
 These are the two. There are indeed these, there are only

65
00:03:54,270 --> 00:03:56,000
 these two tasks.

66
00:03:56,000 --> 00:04:00,210
 So in Buddhism there are only two tasks. One is to study,

67
00:04:00,210 --> 00:04:03,000
 reading the books, and the other is to practice.

68
00:04:03,000 --> 00:04:10,000
 So to understand reality, to understand things as they are.

69
00:04:10,000 --> 00:04:12,000
 And this is something else that I read.

70
00:04:12,000 --> 00:04:18,000
 The third thing that I'm reading right now is a book...

71
00:04:18,000 --> 00:04:19,740
 I won't give the title, I'm not even sure if I came out of

72
00:04:19,740 --> 00:04:20,000
 it.

73
00:04:20,000 --> 00:04:23,000
 The title is "Irreducible Mind".

74
00:04:23,000 --> 00:04:30,310
 This is a book about mind, how the mind and the brain

75
00:04:30,310 --> 00:04:31,000
 relate.

76
00:04:31,000 --> 00:04:35,350
 And giving an analysis of the idea that the mind is only a

77
00:04:35,350 --> 00:04:37,000
 part of the brain.

78
00:04:37,000 --> 00:04:41,000
 Now this is not Buddhist, but the people who wrote this,

79
00:04:41,000 --> 00:04:42,000
 these are all PhDs.

80
00:04:42,000 --> 00:04:46,000
 This is the University of Virginia.

81
00:04:46,000 --> 00:04:49,810
 This group of scientists who are all PhDs, are all

82
00:04:49,810 --> 00:04:53,660
 psychologists or whatever, accredited scientists, well

83
00:04:53,660 --> 00:04:55,000
 known in the field.

84
00:04:55,000 --> 00:04:58,350
 And they've come together to study things that can't be

85
00:04:58,350 --> 00:05:01,000
 explained by a materialist viewpoint.

86
00:05:01,000 --> 00:05:06,530
 Most especially near-death experiences and post-death

87
00:05:06,530 --> 00:05:10,000
 experiences in terms of rebirth.

88
00:05:10,000 --> 00:05:13,000
 So it actually has a lot to do with Buddhism.

89
00:05:13,000 --> 00:05:16,290
 It's useful for me in talking to people who are not

90
00:05:16,290 --> 00:05:19,000
 Buddhist and people who are materialists.

91
00:05:19,000 --> 00:05:23,410
 In being able to debate with them and understand some of

92
00:05:23,410 --> 00:05:24,000
 the issues.

93
00:05:24,000 --> 00:05:26,890
 Because often times people will say, "Well we've already

94
00:05:26,890 --> 00:05:29,000
 proven that mind is a product of the brain."

95
00:05:29,000 --> 00:05:31,000
 And that's what they say in here.

96
00:05:31,000 --> 00:05:33,330
 There are people who say that without looking at the

97
00:05:33,330 --> 00:05:34,000
 evidence.

98
00:05:34,000 --> 00:05:39,000
 And the evidence, this is huge, the evidence in here.

99
00:05:39,000 --> 00:05:43,790
 There's a really good quote that I like. It's talking about

100
00:05:43,790 --> 00:05:45,000
 near-death experiences.

101
00:05:45,000 --> 00:05:49,970
 How people talk about when you're about to die, your brain

102
00:05:49,970 --> 00:05:52,000
 stops functioning.

103
00:05:52,000 --> 00:05:57,700
 After cardiac arrest, it's something like 20 seconds, zero

104
00:05:57,700 --> 00:05:59,000
 brain activity.

105
00:05:59,000 --> 00:06:02,780
 After like 5 seconds there's almost none, but after 20

106
00:06:02,780 --> 00:06:04,000
 seconds it's zero.

107
00:06:04,000 --> 00:06:06,000
 Something like that. It says in here somewhere.

108
00:06:06,000 --> 00:06:09,610
 And so whereas people would say that that means that the

109
00:06:09,610 --> 00:06:10,000
 brain,

110
00:06:10,000 --> 00:06:12,830
 that means that there's no mental function because the

111
00:06:12,830 --> 00:06:14,000
 brain stops working.

112
00:06:14,000 --> 00:06:16,480
 Here's a quote, "Near-death experiences, these are the

113
00:06:16,480 --> 00:06:20,000
 experiences that occur after cardiac arrest."

114
00:06:20,000 --> 00:06:22,830
 "Seam instead to provide direct evidence for a type of

115
00:06:22,830 --> 00:06:26,000
 mental functioning that varies inversely

116
00:06:26,000 --> 00:06:28,930
 rather than directly with the observable activity of the

117
00:06:28,930 --> 00:06:30,000
 nervous system.

118
00:06:30,000 --> 00:06:32,360
 Such evidence we believe fundamentally conflicts with the

119
00:06:32,360 --> 00:06:34,000
 conventional doctrine

120
00:06:34,000 --> 00:06:37,550
 that brain processes produce consciousness and supports the

121
00:06:37,550 --> 00:06:38,000
 alternative view

122
00:06:38,000 --> 00:06:41,000
 that brain activity normally serves as a kind of a filter

123
00:06:41,000 --> 00:06:44,390
 which somehow constrains the material that emerges into

124
00:06:44,390 --> 00:06:46,000
 waking consciousness.

125
00:06:46,000 --> 00:06:49,070
 On this latter view, the relaxation of the filter under

126
00:06:49,070 --> 00:06:51,000
 certain still poorly understood circumstances

127
00:06:51,000 --> 00:06:55,310
 may lead to drastic alterations in the normal mind-brain

128
00:06:55,310 --> 00:06:56,000
 relation

129
00:06:56,000 --> 00:06:59,780
 and to an associated enhancement or enlargement of

130
00:06:59,780 --> 00:07:01,000
 consciousness.

131
00:07:01,000 --> 00:07:04,490
 This interpretation of near-death experiences, which we

132
00:07:04,490 --> 00:07:07,000
 favor, we now turn.

133
00:07:07,000 --> 00:07:15,080
 It's very profound to think of from the point of view of a

134
00:07:15,080 --> 00:07:16,000
 meditator

135
00:07:16,000 --> 00:07:20,000
 that there are scientists looking at these questions

136
00:07:20,000 --> 00:07:24,450
 and just the idea that the universe can be seen from the

137
00:07:24,450 --> 00:07:26,000
 point of view of the mind

138
00:07:26,000 --> 00:07:30,520
 and this brain that we have is simply a squeezing of the

139
00:07:30,520 --> 00:07:32,000
 mind into a certain shape,

140
00:07:32,000 --> 00:07:35,360
 a certain experience, which is seeing, hearing, smelling,

141
00:07:35,360 --> 00:07:37,000
 tasting, feeling, thinking.

142
00:07:37,000 --> 00:07:39,590
 I'm sure I've bored everyone half to death and this is

143
00:07:39,590 --> 00:07:42,000
 nothing that most people would be interested in.

144
00:07:42,000 --> 00:07:46,000
 But this is what I do for fun. So, welcome to my world.

145
00:07:46,000 --> 00:07:51,000
 Okay, so now I'm going to do some reading for real.

146
00:07:51,000 --> 00:07:53,000
 And that's all for now.

