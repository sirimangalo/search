1
00:00:00,000 --> 00:00:09,000
 Good evening everyone.

2
00:00:09,000 --> 00:00:21,000
 Broadcasting Live, January 30th, 2016.

3
00:00:21,000 --> 00:00:32,000
 Today's quote is about how you know a wise person.

4
00:00:32,000 --> 00:00:37,000
 Just a short quote.

5
00:00:37,000 --> 00:00:42,000
 There are three things by which you know a wise person.

6
00:00:42,000 --> 00:00:52,000
 Wisdom is an interesting thing, no?

7
00:00:52,000 --> 00:00:58,000
 Being a Buddhist monk, it's just everywhere.

8
00:00:58,000 --> 00:01:02,000
 It's such a big part of our lives,

9
00:01:02,000 --> 00:01:06,910
 but it's not something you hear the world talk too much

10
00:01:06,910 --> 00:01:08,000
 about.

11
00:01:08,000 --> 00:01:13,000
 What do we know about wisdom?

12
00:01:13,000 --> 00:01:19,000
 If you ask people, "How do you become wise?"

13
00:01:19,000 --> 00:01:26,000
 I think very few people would say, "Practice meditation."

14
00:01:26,000 --> 00:01:30,860
 If you're not Buddhist, that is, if you're not aware of

15
00:01:30,860 --> 00:01:32,000
 that.

16
00:01:32,000 --> 00:01:37,980
 No one, very few people come to me to learn meditation

17
00:01:37,980 --> 00:01:39,000
 because they want to be wise.

18
00:01:39,000 --> 00:01:44,000
 Most people want to be calm.

19
00:01:44,000 --> 00:01:50,000
 They want to learn how to control their minds,

20
00:01:50,000 --> 00:01:58,000
 how to quiet their mind.

21
00:01:58,000 --> 00:02:02,000
 I think wisdom comes from old age. You get old and you get

22
00:02:02,000 --> 00:02:02,000
 wise.

23
00:02:02,000 --> 00:02:06,000
 We pair them up, old and wise.

24
00:02:06,000 --> 00:02:10,000
 You don't hear people say young and wise, right?

25
00:02:10,000 --> 00:02:16,000
 Old and wise.

26
00:02:16,000 --> 00:02:21,000
 Or there's a sense that someone just is wise.

27
00:02:21,000 --> 00:02:26,000
 A wise man, a wise woman.

28
00:02:26,000 --> 00:02:32,000
 As though they were born with it, like a gift.

29
00:02:32,000 --> 00:02:37,000
 Or it comes from study.

30
00:02:37,000 --> 00:02:41,000
 You read lots of books, you become wise.

31
00:02:41,000 --> 00:02:45,000
 We know that's really not true, and yet there's a sense

32
00:02:45,000 --> 00:02:47,000
 that it somehow comes.

33
00:02:47,000 --> 00:02:51,000
 People can gain wisdom from study.

34
00:02:51,000 --> 00:02:57,000
 Some people.

35
00:02:57,000 --> 00:03:01,000
 But wisdom really comes from experience.

36
00:03:01,000 --> 00:03:05,600
 True wisdom doesn't come from reading, it doesn't come from

37
00:03:05,600 --> 00:03:06,000
 thinking,

38
00:03:06,000 --> 00:03:12,000
 it comes from experiencing.

39
00:03:12,000 --> 00:03:19,510
 That's why meditation is so helpful for the cultivation of

40
00:03:19,510 --> 00:03:20,000
 wisdom.

41
00:03:20,000 --> 00:03:23,140
 Because it gives you experiences that you'd never have

42
00:03:23,140 --> 00:03:24,000
 otherwise.

43
00:03:24,000 --> 00:03:27,520
 When people have been to war zones, they become very

44
00:03:27,520 --> 00:03:28,000
 experienced.

45
00:03:28,000 --> 00:03:33,000
 People have traveled, who have experienced extremes,

46
00:03:33,000 --> 00:03:36,810
 who have experienced difficult situations, who have lived

47
00:03:36,810 --> 00:03:38,000
 through difficulty.

48
00:03:38,000 --> 00:03:45,000
 They're very wise.

49
00:03:45,000 --> 00:03:53,130
 But we meditators know that the most terrible war zone is

50
00:03:53,130 --> 00:03:55,000
 in ourselves.

51
00:03:55,000 --> 00:04:00,000
 The greatest challenge.

52
00:04:00,000 --> 00:04:05,000
 It sounds like a hyperbole, but it's actually not.

53
00:04:05,000 --> 00:04:11,000
 I remember in Thailand one Israeli man,

54
00:04:11,000 --> 00:04:15,170
 he was doing a course with us near the end of the course in

55
00:04:15,170 --> 00:04:18,000
 the part where it gets quite intense.

56
00:04:18,000 --> 00:04:23,000
 He said, "Man, you should teach this to the Israeli army.

57
00:04:23,000 --> 00:04:30,630
 The training in the Israeli army is nothing compared to

58
00:04:30,630 --> 00:04:32,000
 this."

59
00:04:32,000 --> 00:04:40,800
 If you know anything about the Israeli army, it's nothing

60
00:04:40,800 --> 00:04:43,000
 to laugh at.

61
00:04:43,000 --> 00:04:47,000
 So if you want experience,

62
00:04:47,000 --> 00:04:53,000
 this is what I think in Lao Tzu he says,

63
00:04:53,000 --> 00:05:02,120
 "A wise person travels wherever they travel, they don't

64
00:05:02,120 --> 00:05:09,000
 need to leave the cart to know the world."

65
00:05:09,000 --> 00:05:12,000
 Dostoevsky says, and the idiot says,

66
00:05:12,000 --> 00:05:21,370
 "You can understand, you can know the whole world in a jail

67
00:05:21,370 --> 00:05:23,000
 cell."

68
00:05:23,000 --> 00:05:25,000
 Those are minds.

69
00:05:25,000 --> 00:05:30,000
 Mind comes first. The Buddha said, "Mino pubangamadam."

70
00:05:30,000 --> 00:05:35,000
 Mind is first, comes before all things.

71
00:05:35,000 --> 00:05:43,010
 "Mino se tamano maya" rules things, makes things, things

72
00:05:43,010 --> 00:05:49,000
 are fashioned from the mind.

73
00:05:49,000 --> 00:05:52,610
 All of the good things that happen to us, all the bad

74
00:05:52,610 --> 00:05:54,000
 things that happen to us,

75
00:05:54,000 --> 00:05:58,000
 they're fashioned from the mind.

76
00:05:58,000 --> 00:06:03,870
 So real wisdom comes from meditation, the best kind of

77
00:06:03,870 --> 00:06:05,000
 wisdom,

78
00:06:05,000 --> 00:06:09,000
 the most challenging experiences you'll ever have,

79
00:06:09,000 --> 00:06:18,000
 from sitting with your eyes closed, doing nothing.

80
00:06:18,000 --> 00:06:22,000
 But the quote is actually about how you know a wise person.

81
00:06:22,000 --> 00:06:24,000
 How do you know a wise person? By three things.

82
00:06:24,000 --> 00:06:30,000
 One, they see a shortcoming as it is.

83
00:06:30,000 --> 00:06:34,920
 They see their faults. They know the problems that they

84
00:06:34,920 --> 00:06:37,000
 have in their mind.

85
00:06:37,000 --> 00:06:53,000
 So wisdom isn't just curing your faults, your flaws.

86
00:06:53,000 --> 00:06:57,790
 It's even wisdom just to recognize them, recognize them as

87
00:06:57,790 --> 00:06:59,000
 they are.

88
00:06:59,000 --> 00:07:04,660
 So if someone's an angry person, but they feel good about

89
00:07:04,660 --> 00:07:05,000
 being angry,

90
00:07:05,000 --> 00:07:10,750
 and they think, "Well, I deserve to be angry," or, "It's

91
00:07:10,750 --> 00:07:13,000
 right for me to be angry."

92
00:07:13,000 --> 00:07:17,000
 "These people deserve my anger."

93
00:07:17,000 --> 00:07:20,340
 Or if a person is depressed and they think, "Well, there's

94
00:07:20,340 --> 00:07:23,000
 a bad thing happened to me, I should feel depressed."

95
00:07:23,000 --> 00:07:28,270
 Or if a person is afraid and they think the things that

96
00:07:28,270 --> 00:07:31,000
 they're afraid of are worth being afraid of,

97
00:07:31,000 --> 00:07:39,000
 it's not recognizing it as a problem.

98
00:07:39,000 --> 00:07:45,970
 It's not recognizing the shortcoming, not recognizing the

99
00:07:45,970 --> 00:07:47,000
 flaw.

100
00:07:47,000 --> 00:07:51,000
 So the first is recognizing it as it is.

101
00:07:51,000 --> 00:07:53,960
 It's the first step where you can't go any further unless

102
00:07:53,960 --> 00:07:58,000
 you see your flaws as flaws.

103
00:07:58,000 --> 00:08:00,850
 If you think you're not doing anything wrong, there are

104
00:08:00,850 --> 00:08:06,000
 people who think they're not in the wrong.

105
00:08:06,000 --> 00:08:09,000
 You can't fix their problems.

106
00:08:09,000 --> 00:08:12,350
 Same goes if you feel guilty about it or you feel angry

107
00:08:12,350 --> 00:08:13,000
 about it.

108
00:08:13,000 --> 00:08:16,000
 Hate yourself because of it.

109
00:08:16,000 --> 00:08:18,000
 You can't fix it that way.

110
00:08:18,000 --> 00:08:21,990
 Objectivity for your flaws is very important, your short

111
00:08:21,990 --> 00:08:23,000
coming.

112
00:08:23,000 --> 00:08:28,630
 See them as they are. That means being objective about them

113
00:08:28,630 --> 00:08:29,000
.

114
00:08:29,000 --> 00:08:32,000
 Objectively seeing what they're doing to you.

115
00:08:32,000 --> 00:08:35,000
 Because you'll see what they're doing.

116
00:08:35,000 --> 00:08:39,000
 You'll see that the emotions, the negative emotions,

117
00:08:39,000 --> 00:08:49,000
 unwholesome emotions that they are bad for you.

118
00:08:49,000 --> 00:08:54,000
 The second thing is trying to correct your shortcomings.

119
00:08:54,000 --> 00:08:56,000
 So some people know that they have problems.

120
00:08:56,000 --> 00:08:59,000
 Lots of people will say this, "Oh yeah, I'm lazy."

121
00:08:59,000 --> 00:09:03,000
 But they're too lazy to do anything about it.

122
00:09:03,000 --> 00:09:06,020
 "Yeah, I get angry. I'm an angry person. I have an anger

123
00:09:06,020 --> 00:09:07,000
 problem."

124
00:09:07,000 --> 00:09:09,000
 But they never do anything about it.

125
00:09:09,000 --> 00:09:11,540
 Sometimes we don't do anything because we don't know what

126
00:09:11,540 --> 00:09:12,000
 to do.

127
00:09:12,000 --> 00:09:15,070
 It's sad in the West here that there's not a tradition of

128
00:09:15,070 --> 00:09:17,000
 meditation, right?

129
00:09:17,000 --> 00:09:26,510
 People don't know what to do. They end up taking drugs or

130
00:09:26,510 --> 00:09:31,000
 medication.

131
00:09:31,000 --> 00:09:33,000
 The third one is kind of interesting.

132
00:09:33,000 --> 00:09:38,000
 When someone acknowledges a shortcoming, forgiving it.

133
00:09:38,000 --> 00:09:42,540
 I talked about this recently, forgiving other people's

134
00:09:42,540 --> 00:09:44,000
 shortcomings.

135
00:09:44,000 --> 00:09:47,000
 It's important.

136
00:09:47,000 --> 00:09:52,000
 Even evil people are people.

137
00:09:52,000 --> 00:09:57,000
 You don't gain anything from hating them.

138
00:09:57,000 --> 00:10:00,260
 When we see other people's faults, we immediately become

139
00:10:00,260 --> 00:10:01,000
 perfect.

140
00:10:01,000 --> 00:10:03,000
 It's really interesting.

141
00:10:03,000 --> 00:10:07,000
 We lose sight of our own faults.

142
00:10:07,000 --> 00:10:12,220
 We become self-righteous, and that's where hypocrisy comes

143
00:10:12,220 --> 00:10:13,000
 from.

144
00:10:13,000 --> 00:10:17,000
 Once you start to see your faults, you become very humble.

145
00:10:17,000 --> 00:10:22,000
 You see, I'm all messed up.

146
00:10:22,000 --> 00:10:27,450
 I'm all messed up so I don't feel like I can accuse anyone

147
00:10:27,450 --> 00:10:34,000
 else of being messed up.

148
00:10:34,000 --> 00:10:37,000
 Just work.

149
00:10:37,000 --> 00:10:39,000
 Work to see my shortcomings.

150
00:10:39,000 --> 00:10:42,000
 Work to see my flaws.

151
00:10:42,000 --> 00:10:45,000
 Work to fix them.

152
00:10:45,000 --> 00:10:50,000
 And forgive. Forgive yourself. Forgive others.

153
00:10:50,000 --> 00:10:53,000
 There's no reason to get upset about it.

154
00:10:53,000 --> 00:10:57,000
 There's only a reason to try and fix it.

155
00:10:57,000 --> 00:11:05,000
 Try and correct it. Change the way we look at it.

156
00:11:05,000 --> 00:11:12,000
 So that's the quote for tonight.

157
00:11:12,000 --> 00:11:22,310
 What's going on in internet? Do you have a lot of people

158
00:11:22,310 --> 00:11:23,000
 tonight?

159
00:11:23,000 --> 00:11:28,000
 Hmm. A few people.

160
00:11:28,000 --> 00:11:36,280
 I was late meditating tonight, and some talking to a medit

161
00:11:36,280 --> 00:11:38,000
ator.

162
00:11:38,000 --> 00:11:43,000
 Oh, good. Good day. Good group tonight.

163
00:11:43,000 --> 00:11:50,390
 Well, thanks everyone for, as usual, making our group a

164
00:11:50,390 --> 00:11:52,000
 success.

165
00:11:52,000 --> 00:11:54,000
 Have a good night.

166
00:11:56,000 --> 00:12:01,000
 [

