1
00:00:00,000 --> 00:00:02,920
 Hi, welcome back to Ask a Monk.

2
00:00:02,920 --> 00:00:08,320
 Today's question comes from T-Master Yoda T.

3
00:00:08,320 --> 00:00:10,970
 Now that I'm sitting here perceiving the world and

4
00:00:10,970 --> 00:00:12,960
 accepting what is, I was wondering

5
00:00:12,960 --> 00:00:16,440
 how one can be really proactive as a Buddhist.

6
00:00:16,440 --> 00:00:20,350
 Meditating and trying to unravel oneself's thoughts does

7
00:00:20,350 --> 00:00:22,280
 seem to refrain from taking

8
00:00:22,280 --> 00:00:24,140
 action needed to change.

9
00:00:24,140 --> 00:00:27,520
 Or not.

10
00:00:27,520 --> 00:00:29,520
 Or not.

11
00:00:29,520 --> 00:00:33,640
 Two things, I suppose.

12
00:00:33,640 --> 00:00:36,040
 The first one, three things.

13
00:00:36,040 --> 00:00:43,540
 The first one is, I'm not going to pass judgement, but it's

14
00:00:43,540 --> 00:00:46,960
 hard to believe that you could just

15
00:00:46,960 --> 00:00:50,530
 sit down and suddenly start accepting the world, accepting

16
00:00:50,530 --> 00:00:51,200
 what is.

17
00:00:51,200 --> 00:00:56,020
 You have to understand that it's not just, it's not

18
00:00:56,020 --> 00:00:59,080
 something that's easy to do or it's

19
00:00:59,080 --> 00:01:00,360
 not something that you just do.

20
00:01:00,360 --> 00:01:05,680
 You sit down, okay, now I'm accepting the world, what next?

21
00:01:05,680 --> 00:01:09,550
 The ability to accept things for what they are is

22
00:01:09,550 --> 00:01:11,160
 enlightenment.

23
00:01:11,160 --> 00:01:14,990
 And I suppose it's easy to think that suddenly you're

24
00:01:14,990 --> 00:01:17,720
 accepting things as they are, but the

25
00:01:17,720 --> 00:01:22,400
 fact that you're asking this question shows that there is

26
00:01:22,400 --> 00:01:25,080
 quite likely non-acceptance,

27
00:01:25,080 --> 00:01:26,680
 the need for something more.

28
00:01:26,680 --> 00:01:29,510
 The fact that you're sitting there and saying what next

29
00:01:29,510 --> 00:01:31,200
 shows that there's some need in

30
00:01:31,200 --> 00:01:35,080
 the mind for something greater.

31
00:01:35,080 --> 00:01:40,950
 Actually accepting what is is a state that affects a

32
00:01:40,950 --> 00:01:44,860
 profound change on who you are.

33
00:01:44,860 --> 00:01:47,680
 It's not just you sit there, everything else is the same,

34
00:01:47,680 --> 00:01:49,340
 except now you accept everything

35
00:01:49,340 --> 00:01:50,340
 for what it is.

36
00:01:50,340 --> 00:01:51,960
 It changes your whole life.

37
00:01:51,960 --> 00:01:56,620
 You have to go through great upheaval in your life and in

38
00:01:56,620 --> 00:02:01,520
 the way you look at the world.

39
00:02:01,520 --> 00:02:06,320
 It's an incredibly difficult and generally lengthy process

40
00:02:06,320 --> 00:02:08,640
 that requires you to analyze

41
00:02:08,640 --> 00:02:14,810
 and to reevaluate the whole of your life in terms of how

42
00:02:14,810 --> 00:02:18,400
 you look at the world, how you

43
00:02:18,400 --> 00:02:21,760
 interact with other people and so on.

44
00:02:21,760 --> 00:02:26,930
 So I wouldn't misunderstand that as soon as you sit down

45
00:02:26,930 --> 00:02:29,920
 and say to yourself rising, falling

46
00:02:29,920 --> 00:02:33,770
 or seeing, seeing, hearing, hearing or so on, suddenly you

47
00:02:33,770 --> 00:02:35,000
 accept things.

48
00:02:35,000 --> 00:02:37,560
 What this does is allows you to see the things that you don

49
00:02:37,560 --> 00:02:39,160
't accept, the friction that you

50
00:02:39,160 --> 00:02:42,410
 have, the tension that you have in your mind that's causing

51
00:02:42,410 --> 00:02:43,440
 you suffering.

52
00:02:43,440 --> 00:02:45,080
 That's the first thing.

53
00:02:45,080 --> 00:02:53,050
 The second thing is the whole idea of being proactive is in

54
00:02:53,050 --> 00:02:57,720
 many ways a misunderstanding

55
00:02:57,720 --> 00:03:02,650
 of what's good and what's right or misreading of the world

56
00:03:02,650 --> 00:03:04,000
 in general.

57
00:03:04,000 --> 00:03:07,130
 The idea that the problems in the world can somehow be

58
00:03:07,130 --> 00:03:08,920
 fixed if we all just work harder

59
00:03:08,920 --> 00:03:12,280
 or do more.

60
00:03:12,280 --> 00:03:15,300
 I would say that in fact most of the problems in the world

61
00:03:15,300 --> 00:03:16,960
 come from the fact that we're

62
00:03:16,960 --> 00:03:21,390
 doing too much, that we're seeking after, that we're trying

63
00:03:21,390 --> 00:03:23,080
 to fix and to force and

64
00:03:23,080 --> 00:03:25,680
 to make things according to our wishes.

65
00:03:25,680 --> 00:03:31,860
 If you look at the rise of human civilization, it has

66
00:03:31,860 --> 00:03:36,360
 mostly been because of our inability

67
00:03:36,360 --> 00:03:38,360
 to accept things as they are.

68
00:03:38,360 --> 00:03:39,940
 Things are not comfortable enough.

69
00:03:39,940 --> 00:03:41,400
 They're not convenient enough.

70
00:03:41,400 --> 00:03:44,840
 They're not satisfying enough.

71
00:03:44,840 --> 00:03:49,970
 We create greater and more immediate pleasures again and

72
00:03:49,970 --> 00:03:53,040
 again, adding to the burden that

73
00:03:53,040 --> 00:03:55,480
 we have to carry around and the burden that we're putting

74
00:03:55,480 --> 00:03:56,880
 on the world and on the natural

75
00:03:56,880 --> 00:04:01,980
 resources around us, instead of simply accepting things for

76
00:04:01,980 --> 00:04:04,840
 what they are, which would leave

77
00:04:04,840 --> 00:04:07,600
 the world in a state of harmony and peace.

78
00:04:07,600 --> 00:04:13,250
 If we were all content and we're able to follow this sort

79
00:04:13,250 --> 00:04:16,720
 of idea of simply accepting things

80
00:04:16,720 --> 00:04:22,550
 or at least understanding things for what they are, then we

81
00:04:22,550 --> 00:04:25,320
 wouldn't have the problems

82
00:04:25,320 --> 00:04:30,520
 that we see in the world today, most of them.

83
00:04:30,520 --> 00:04:34,800
 The third thing is that even in those cases where it is

84
00:04:34,800 --> 00:04:37,720
 necessary to be proactive, where

85
00:04:37,720 --> 00:04:43,170
 we have to help other people or where we should encourage

86
00:04:43,170 --> 00:04:46,520
 and support people in good things

87
00:04:46,520 --> 00:04:53,580
 and in developing themselves and in helping the world, it's

88
00:04:53,580 --> 00:04:55,600
 impossible to think that,

89
00:04:55,600 --> 00:04:59,240
 it's absurd to think that you can just go out and help and

90
00:04:59,240 --> 00:05:01,040
 you can just go out and work

91
00:05:01,040 --> 00:05:06,550
 in the world without any sort of training in terms of your

92
00:05:06,550 --> 00:05:08,760
 own purification.

93
00:05:08,760 --> 00:05:10,930
 The idea that we can just go out and help other people and

94
00:05:10,930 --> 00:05:12,000
 that's what we should be

95
00:05:12,000 --> 00:05:19,090
 doing is fallacious for the reason that the only way you

96
00:05:19,090 --> 00:05:22,720
 can help people is if you are

97
00:05:22,720 --> 00:05:30,260
 a helpful person, if you're a person who has negative

98
00:05:30,260 --> 00:05:32,040
 states of mind, what you're going

99
00:05:32,040 --> 00:05:34,290
 to pass on to other people are those negative states of

100
00:05:34,290 --> 00:05:34,680
 mind.

101
00:05:34,680 --> 00:05:37,380
 If you're a person who is stressed out, if you're a person

102
00:05:37,380 --> 00:05:38,760
 who gets angry easily, if

103
00:05:38,760 --> 00:05:42,270
 you're a person who is addicted and attached to sensual

104
00:05:42,270 --> 00:05:44,360
 pleasures, you're not going to

105
00:05:44,360 --> 00:05:47,150
 go out and help other people, you're going to go out and

106
00:05:47,150 --> 00:05:48,320
 twist and turn them for your

107
00:05:48,320 --> 00:05:55,400
 own benefit and harm them by your own mental defilements.

108
00:05:55,400 --> 00:05:57,360
 I think this is what we see.

109
00:05:57,360 --> 00:06:01,160
 There are many organizations that claim to be doing good or

110
00:06:01,160 --> 00:06:02,880
 wish for good things to arise

111
00:06:02,880 --> 00:06:06,320
 but end up suffering a lot and having great difficulty

112
00:06:06,320 --> 00:06:08,560
 doing what they're doing because

113
00:06:08,560 --> 00:06:16,180
 of the conflict and the tension, the anger, the frustration

114
00:06:16,180 --> 00:06:19,480
, the stress and so on that

115
00:06:19,480 --> 00:06:24,640
 assails them in their own minds.

116
00:06:24,640 --> 00:06:28,340
 This is like, as I've said before, to a river.

117
00:06:28,340 --> 00:06:34,660
 If the source of any river or stream, a water source is imp

118
00:06:34,660 --> 00:06:38,340
ure at the source, then all down

119
00:06:38,340 --> 00:06:42,790
 the stream, no matter what comes in contact with it, they

120
00:06:42,790 --> 00:06:45,160
 can't get any benefit from that

121
00:06:45,160 --> 00:06:49,550
 water, no matter anywhere along the stream, anywhere down

122
00:06:49,550 --> 00:06:50,560
 the river.

123
00:06:50,560 --> 00:06:55,880
 Only if the source is pure can the rest of the stream be of

124
00:06:55,880 --> 00:06:58,080
 any use and the same is with

125
00:06:58,080 --> 00:06:59,080
 our mind.

126
00:06:59,080 --> 00:07:02,400
 If our mind is impure, if we act or speak with an impure

127
00:07:02,400 --> 00:07:04,720
 mind, then we don't bring happiness,

128
00:07:04,720 --> 00:07:07,440
 we only bring suffering.

129
00:07:07,440 --> 00:07:12,720
 So meditation in that sense, it starts with a training.

130
00:07:12,720 --> 00:07:15,860
 It starts with a personal training where you close the door

131
00:07:15,860 --> 00:07:17,480
, close your eyes, block out

132
00:07:17,480 --> 00:07:20,880
 the world and start from square one.

133
00:07:20,880 --> 00:07:27,170
 Look at the very kernel, the source of all of your actions,

134
00:07:27,170 --> 00:07:30,200
 all of your manifestations

135
00:07:30,200 --> 00:07:34,180
 in the world and start to clean that, start to purify that.

136
00:07:34,180 --> 00:07:37,320
 As you get better at that, then you move outward and your

137
00:07:37,320 --> 00:07:39,440
 meditation slowly enters into your

138
00:07:39,440 --> 00:07:40,560
 daily life.

139
00:07:40,560 --> 00:07:43,920
 As you practice on and on, eventually your daily life is

140
00:07:43,920 --> 00:07:44,920
 meditation.

141
00:07:44,920 --> 00:07:46,820
 When you meet with people and you get angry, you're

142
00:07:46,820 --> 00:07:48,280
 watching the anger, you're looking

143
00:07:48,280 --> 00:07:53,400
 at it and you're trying to clean that as well, you're

144
00:07:53,400 --> 00:07:55,920
 trying to purify that.

145
00:07:55,920 --> 00:07:58,500
 On the one hand, the idea that meditation is only for the

146
00:07:58,500 --> 00:07:59,840
 sitting mat with the closed

147
00:07:59,840 --> 00:08:03,180
 door and therefore has nothing to do with the world is

148
00:08:03,180 --> 00:08:03,840
 false.

149
00:08:03,840 --> 00:08:07,800
 On the other hand, it has to start from within.

150
00:08:07,800 --> 00:08:09,640
 You can't just go out in the world and meditate.

151
00:08:09,640 --> 00:08:11,640
 You can't just walk down the road and say, "Okay, I'm going

152
00:08:11,640 --> 00:08:12,200
 to meditate.

153
00:08:12,200 --> 00:08:13,760
 I'm going to be mindful of everything.

154
00:08:13,760 --> 00:08:18,280
 I'm going to consider things carefully."

155
00:08:18,280 --> 00:08:24,010
 You're dealing with so many layers of dirt and defilement

156
00:08:24,010 --> 00:08:27,280
 in the mind and misunderstandings

157
00:08:27,280 --> 00:08:29,520
 that exist in the mind.

158
00:08:29,520 --> 00:08:33,220
 It's highly unlikely that you'll be able to do such a thing

159
00:08:33,220 --> 00:08:35,120
 and conversely, you're more

160
00:08:35,120 --> 00:08:39,160
 likely to harm others, to harm yourself and to harm others

161
00:08:39,160 --> 00:08:41,240
 and to feel bad about it later

162
00:08:41,240 --> 00:08:43,240
 on.

163
00:08:43,240 --> 00:08:46,530
 Retreating is coming back to the basics and starting off

164
00:08:46,530 --> 00:08:48,640
 from the very kernel of our being,

165
00:08:48,640 --> 00:08:49,640
 which is our mind.

166
00:08:49,640 --> 00:08:52,790
 When we close our eyes, we start to see how our mind reacts

167
00:08:52,790 --> 00:08:54,200
 to very simple things.

168
00:08:54,200 --> 00:08:56,120
 Reacts to the stomach rising and falling.

169
00:08:56,120 --> 00:08:59,600
 Reacts to having to sit still.

170
00:08:59,600 --> 00:09:00,600
 Reacts to pain.

171
00:09:00,600 --> 00:09:03,150
 Reacts to the building blocks that make up our everyday

172
00:09:03,150 --> 00:09:03,600
 life.

173
00:09:03,600 --> 00:09:06,340
 Once we get better at that, then we move out into our daily

174
00:09:06,340 --> 00:09:07,920
 life and actually incorporate

175
00:09:07,920 --> 00:09:10,880
 the meditation practice into our lives.

176
00:09:10,880 --> 00:09:14,670
 Meditation should not be something that is external to your

177
00:09:14,670 --> 00:09:16,480
 daily life, an addition or

178
00:09:16,480 --> 00:09:18,080
 a segregated part.

179
00:09:18,080 --> 00:09:20,120
 Your life should become meditative.

180
00:09:20,120 --> 00:09:23,650
 If your life is not becoming meditative, you'll never

181
00:09:23,650 --> 00:09:26,200
 succeed in the meditation practice and

182
00:09:26,200 --> 00:09:27,200
 becoming enlightened.

183
00:09:27,200 --> 00:09:29,640
 Your whole life has to be meditation.

184
00:09:29,640 --> 00:09:34,200
 Meditation is a way of life, not an addition to life.

185
00:09:34,200 --> 00:09:37,740
 In that sense, it will be of great value to you in anything

186
00:09:37,740 --> 00:09:38,400
 you do.

187
00:09:38,400 --> 00:09:40,970
 Anytime you wish to help others or do good things for the

188
00:09:40,970 --> 00:09:42,480
 world, meditation will keep

189
00:09:42,480 --> 00:09:47,430
 you balanced and reasonable and able to see things as they

190
00:09:47,430 --> 00:09:51,040
 are and react properly, appropriately

191
00:09:51,040 --> 00:09:52,320
 to every situation.

192
00:09:52,320 --> 00:09:54,720
 Okay, so I hope that helps.

193
00:09:54,720 --> 00:09:55,720
 Good question.

194
00:09:55,720 --> 00:09:56,720
 It's one that's asked often.

195
00:09:56,720 --> 00:09:58,080
 So, yeah, there's my answer.

196
00:09:58,080 --> 00:09:59,080
 Thank you.

