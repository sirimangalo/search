1
00:00:00,000 --> 00:00:02,500
 Walking Meditation Q&A

2
00:00:02,500 --> 00:00:07,200
 Why does walking meditation come first?

3
00:00:07,200 --> 00:00:11,900
 Why does walking meditation come before sitting?

4
00:00:11,900 --> 00:00:15,420
 Is it okay to reverse the order if that feels more

5
00:00:15,420 --> 00:00:16,200
 convenient?

6
00:00:16,200 --> 00:00:20,100
 It seems to me it is harder to concentrate during walking

7
00:00:20,100 --> 00:00:24,540
 and I become distracted much more easily because there is

8
00:00:24,540 --> 00:00:26,300
 one more sense included.

9
00:00:26,400 --> 00:00:30,450
 I have tried walking with closed eyes but it does not work

10
00:00:30,450 --> 00:00:31,900
 as I lose my balance.

11
00:00:31,900 --> 00:00:37,940
 First, do not walk with closed eyes as you will lose your

12
00:00:37,940 --> 00:00:38,400
 balance.

13
00:00:38,400 --> 00:00:42,700
 Beyond that, this question concerns the goal of meditation.

14
00:00:42,700 --> 00:00:46,700
 You describe your goal as concentration and that is wrong.

15
00:00:46,700 --> 00:00:49,200
 The goal should not be concentration.

16
00:00:49,200 --> 00:00:50,600
 It should be wisdom.

17
00:00:50,600 --> 00:00:54,300
 This is something that has to be repeated again and again,

18
00:00:54,500 --> 00:00:58,640
 hoping for concentration or calm in meditation is a real

19
00:00:58,640 --> 00:00:59,500
 hindrance.

20
00:00:59,500 --> 00:01:03,300
 It is an attachment and a desire, wanting your experience

21
00:01:03,300 --> 00:01:05,500
 to be other than what it is.

22
00:01:05,500 --> 00:01:08,800
 There is nothing wrong with your walking meditation.

23
00:01:08,800 --> 00:01:12,860
 What you are seeing is the impermanent suffering and non-

24
00:01:12,860 --> 00:01:14,200
self of the mind.

25
00:01:14,200 --> 00:01:16,400
 The mind is changing all the time.

26
00:01:16,400 --> 00:01:18,100
 So there is impermanence.

27
00:01:18,100 --> 00:01:21,500
 One moment you are with the foot and then all of a sudden

28
00:01:21,500 --> 00:01:23,600
 your mind is off somewhere else

29
00:01:23,800 --> 00:01:27,450
 and it seems like the two mind states have no relationship

30
00:01:27,450 --> 00:01:28,500
 with each other.

31
00:01:28,500 --> 00:01:30,200
 This results in suffering.

32
00:01:30,200 --> 00:01:33,760
 It is not the way you want it to be according to your

33
00:01:33,760 --> 00:01:34,400
 desire.

34
00:01:34,400 --> 00:01:38,770
 Because you can't control it, you start to realize that it

35
00:01:38,770 --> 00:01:39,700
 is non-self.

36
00:01:39,700 --> 00:01:42,600
 You cannot make it to be the way you want it to be.

37
00:01:42,600 --> 00:01:44,400
 You might make a resolve.

38
00:01:44,400 --> 00:01:48,720
 Now I am going to be mindful from here to the wall and two

39
00:01:48,720 --> 00:01:50,800
 steps later you have forgotten

40
00:01:51,000 --> 00:01:54,600
 because even your mind is not you or yours.

41
00:01:54,600 --> 00:01:57,600
 Destruction has to fade away naturally.

42
00:01:57,600 --> 00:02:02,800
 You should not try to suppress it or force it away as this

43
00:02:02,800 --> 00:02:05,000
 gives rise to more delusion.

44
00:02:05,000 --> 00:02:08,000
 The idea that I can control the mind.

45
00:02:08,000 --> 00:02:12,000
 Proper focus has to come naturally through mindfulness.

46
00:02:12,000 --> 00:02:16,300
 Meditators are often overly concerned with concentration,

47
00:02:16,500 --> 00:02:20,590
 thinking that their minds must be calm and quiet in order

48
00:02:20,590 --> 00:02:22,400
 to give rise to wisdom.

49
00:02:22,400 --> 00:02:26,630
 This is sometimes due to common translations of the word

50
00:02:26,630 --> 00:02:29,400
 samadhi as concentration.

51
00:02:29,400 --> 00:02:34,370
 The word samadhi is best translated as focus because it

52
00:02:34,370 --> 00:02:38,000
 comes from the same root as the word same,

53
00:02:38,000 --> 00:02:40,400
 meaning level or balanced.

54
00:02:40,400 --> 00:02:43,600
 The goal is not merely to become concentrated.

55
00:02:43,800 --> 00:02:47,000
 It is to balance concentration with effort.

56
00:02:47,000 --> 00:02:50,440
 Practicing walking and sitting together balances

57
00:02:50,440 --> 00:02:52,600
 concentration with effort.

58
00:02:52,600 --> 00:02:56,430
 Sitting meditation is a great practice, but until your

59
00:02:56,430 --> 00:02:57,900
 faculties are balanced,

60
00:02:57,900 --> 00:02:59,800
 it can make you drowsy.

61
00:02:59,800 --> 00:03:03,610
 If that happens, you should stand up and practice walking

62
00:03:03,610 --> 00:03:04,400
 instead.

63
00:03:04,400 --> 00:03:08,000
 Walking meditation is great for developing energy,

64
00:03:08,000 --> 00:03:11,400
 but it can have the problem of making you distracted.

65
00:03:11,600 --> 00:03:15,310
 When you feel distracted, you might decide to sit down and

66
00:03:15,310 --> 00:03:17,400
 do sitting meditation instead.

67
00:03:17,400 --> 00:03:20,650
 What you are seeing is that in order to balance the

68
00:03:20,650 --> 00:03:21,400
 faculties,

69
00:03:21,400 --> 00:03:23,900
 both walking and sitting are important.

70
00:03:23,900 --> 00:03:28,470
 In sitting, you can become drowsy or caught up in the calm

71
00:03:28,470 --> 00:03:29,500
 and the tranquility.

72
00:03:29,500 --> 00:03:31,300
 You might fall into a trance.

73
00:03:31,300 --> 00:03:35,200
 Getting up and walking can help you to break such habits.

74
00:03:35,400 --> 00:03:39,280
 It can also help you to overcome attachment to pleasant

75
00:03:39,280 --> 00:03:42,000
 states attained during sitting meditation,

76
00:03:42,000 --> 00:03:43,800
 forcing you to be objective.

77
00:03:43,800 --> 00:03:47,200
 We like sitting and we like lying even better,

78
00:03:47,200 --> 00:03:51,610
 normally because we can enter into very peaceful states

79
00:03:51,610 --> 00:03:52,400
 lying down,

80
00:03:52,400 --> 00:03:56,260
 including falling asleep, which many people are very much

81
00:03:56,260 --> 00:03:57,400
 attached to.

82
00:03:57,400 --> 00:04:00,200
 Why do we do walking first?

83
00:04:00,400 --> 00:04:05,400
 Doing walking meditation first is not a hard and fast rule,

84
00:04:05,400 --> 00:04:09,530
 but one of the benefits of walking meditation enumerated by

85
00:04:09,530 --> 00:04:10,600
 the Buddha himself

86
00:04:10,600 --> 00:04:14,810
 is that the focus of mind gained from walking lasts for a

87
00:04:14,810 --> 00:04:15,600
 long time.

88
00:04:15,600 --> 00:04:19,270
 Because it is dynamic, it has the ability to prepare the

89
00:04:19,270 --> 00:04:21,000
 mind for sitting meditation.

90
00:04:21,000 --> 00:04:25,110
 It is a useful segue from the dynamic behavior during

91
00:04:25,110 --> 00:04:28,000
 ordinary life into a static sitting posture.

92
00:04:28,200 --> 00:04:31,940
 If you go directly from working and acting in the world to

93
00:04:31,940 --> 00:04:32,800
 sitting still,

94
00:04:32,800 --> 00:04:35,200
 your mind hasn't had a chance to prepare.

95
00:04:35,200 --> 00:04:38,810
 This is why people find themselves nodding off during

96
00:04:38,810 --> 00:04:40,200
 sitting meditation

97
00:04:40,200 --> 00:04:42,800
 or falling into trance states.

98
00:04:42,800 --> 00:04:46,000
 Walking meditation is halfway between the two.

99
00:04:46,000 --> 00:04:50,210
 The long-lasting focus and balance of mind that comes from

100
00:04:50,210 --> 00:04:51,400
 walking meditation

101
00:04:51,400 --> 00:04:53,800
 augments sitting meditation.

102
00:04:54,000 --> 00:04:57,080
 When you are living in ordinary life, sometimes you want to

103
00:04:57,080 --> 00:04:59,400
 give up walking meditation entirely

104
00:04:59,400 --> 00:05:02,800
 because you are walking all day or you are active all day.

105
00:05:02,800 --> 00:05:06,500
 And it can be that you might just do the sitting meditation

106
00:05:06,500 --> 00:05:06,800
.

107
00:05:06,800 --> 00:05:10,320
 You do not have to maintain walking and sitting in equal

108
00:05:10,320 --> 00:05:12,400
 parts as a hard and fast rule,

109
00:05:12,400 --> 00:05:15,800
 especially when your life involves physical exertion.

110
00:05:15,800 --> 00:05:18,800
 The Buddha's advice regarding walking and sitting was

111
00:05:18,800 --> 00:05:22,400
 mostly in regards to intensive meditation practice,

112
00:05:22,600 --> 00:05:26,780
 wherein one lies down to sleep for only four hours each day

113
00:05:26,780 --> 00:05:29,400
 and splits the remaining 20 hours

114
00:05:29,400 --> 00:05:34,000
 walking, sitting, walking and sitting with minimal chores

115
00:05:34,000 --> 00:05:37,400
 of eating and attending to unspotaly needs.

