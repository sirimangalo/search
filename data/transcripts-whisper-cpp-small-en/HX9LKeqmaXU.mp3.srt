1
00:00:00,000 --> 00:00:11,520
 Okay, so this area is the place where the Bodhisatta was

2
00:00:11,520 --> 00:00:12,920
 born.

3
00:00:12,920 --> 00:00:15,300
 I was trying to figure out if you could say that the Buddha

4
00:00:15,300 --> 00:00:16,800
 was born here and of course

5
00:00:16,800 --> 00:00:21,120
 it's all semantics, but it is a point of technicality that

6
00:00:21,120 --> 00:00:23,520
 he wasn't the Buddha when he was born

7
00:00:23,520 --> 00:00:26,520
 here.

8
00:00:26,520 --> 00:00:30,770
 The commentaries talk about two births of the Buddha and

9
00:00:30,770 --> 00:00:32,840
 one is the birth of Rupakaya and

10
00:00:32,840 --> 00:00:37,440
 the other is the birth of Dhammakaya.

11
00:00:37,440 --> 00:00:42,340
 This is the place of the birth of the Rupakaya, the Buddha

12
00:00:42,340 --> 00:00:45,840
's physical manifestation or physical

13
00:00:45,840 --> 00:00:48,640
 form.

14
00:00:48,640 --> 00:00:52,450
 The second birth is at Bodhaya where we'll be heading and

15
00:00:52,450 --> 00:00:54,560
 it's the birth of the Dhamma,

16
00:00:54,560 --> 00:01:00,840
 the actual Buddha-ness of the Buddha.

17
00:01:00,840 --> 00:01:04,610
 So because his Rupakaya was born before the Dhammakaya, you

18
00:01:04,610 --> 00:01:06,360
 could say this is the birth

19
00:01:06,360 --> 00:01:14,600
 place of the Buddha in the Rupakaya.

20
00:01:14,600 --> 00:01:23,560
 In some ways it's the least inspiring of the four places.

21
00:01:23,560 --> 00:01:28,480
 In a sense it might be why are we here or what should we

22
00:01:28,480 --> 00:01:31,320
 feel when we're here because

23
00:01:31,320 --> 00:01:34,000
 again it was just the Rupakaya and you can say well the

24
00:01:34,000 --> 00:01:35,600
 Buddha was born but he wasn't

25
00:01:35,600 --> 00:01:38,360
 a Buddha yet.

26
00:01:38,360 --> 00:01:45,660
 In some sense it's not even that remarkable because it's

27
00:01:45,660 --> 00:01:47,200
 simply the very last bit of a

28
00:01:47,200 --> 00:01:56,840
 very long and arduous and difficult and incredible journey.

29
00:01:56,840 --> 00:02:04,180
 So the last life is just the very last bit of it when the

30
00:02:04,180 --> 00:02:09,680
 whole journey is quite incredible.

31
00:02:09,680 --> 00:02:14,270
 In terms of births there is something quite important about

32
00:02:14,270 --> 00:02:16,240
 the fact that this was the

33
00:02:16,240 --> 00:02:17,600
 Buddha's last birth.

34
00:02:17,600 --> 00:02:18,600
 This was it.

35
00:02:18,600 --> 00:02:24,560
 This was the culmination of births.

36
00:02:24,560 --> 00:02:29,610
 So I'd like to talk a little bit about birth, make it a

37
00:02:29,610 --> 00:02:33,440
 little deeper for us but also highlight

38
00:02:33,440 --> 00:02:41,460
 how incredible and special the Buddha's last birth was, his

39
00:02:41,460 --> 00:02:43,260
 last life.

40
00:02:43,260 --> 00:02:50,570
 So there are, besides being born as a Buddha, there are six

41
00:02:50,570 --> 00:02:53,000
 types of births.

42
00:02:53,000 --> 00:03:07,020
 Birth as in hell, birth as a ghost, birth in the animal

43
00:03:07,020 --> 00:03:12,560
 realm, birth as a human, birth

44
00:03:12,560 --> 00:03:18,520
 as a deva and birth as a brahma.

45
00:03:18,520 --> 00:03:25,200
 Those are the six types of birth.

46
00:03:25,200 --> 00:03:32,490
 If we're talking in terms of what birth means, all six of

47
00:03:32,490 --> 00:03:36,360
 those types of birth are under

48
00:03:36,360 --> 00:03:41,400
 the category of conventional birth or conceptual birth.

49
00:03:41,400 --> 00:03:44,680
 Because of course the mind is born and dies every moment.

50
00:03:44,680 --> 00:03:48,370
 So you could say there are two types of birth and all of

51
00:03:48,370 --> 00:03:50,200
 those six are one type.

52
00:03:50,200 --> 00:03:52,080
 It's worth talking about them.

53
00:03:52,080 --> 00:03:58,030
 It's worth reminding ourselves about the nature of reality,

54
00:03:58,030 --> 00:04:00,800
 that our existence as human beings

55
00:04:00,800 --> 00:04:04,280
 is contrived.

56
00:04:04,280 --> 00:04:06,000
 It's nothing special.

57
00:04:06,000 --> 00:04:12,350
 This isn't the de facto birth where we have ten fingers and

58
00:04:12,350 --> 00:04:15,360
 ten toes and two arms, two

59
00:04:15,360 --> 00:04:20,880
 legs and many other types of birth.

60
00:04:20,880 --> 00:04:33,640
 To be born in hell, if you're full of anger, then your

61
00:04:33,640 --> 00:04:35,840
 birth becomes a birth in a realm

62
00:04:35,840 --> 00:04:41,410
 of great suffering because your inclination of mind is to

63
00:04:41,410 --> 00:04:44,280
 cause suffering to yourself

64
00:04:44,280 --> 00:04:45,280
 and to others.

65
00:04:45,280 --> 00:04:48,140
 Of course it's very painful to be angry but it's also the

66
00:04:48,140 --> 00:04:49,800
 cause of, greatest cause of

67
00:04:49,800 --> 00:04:53,600
 harm to others.

68
00:04:53,600 --> 00:05:00,500
 And so the birth that becomes, that comes from anger is

69
00:05:00,500 --> 00:05:02,680
 birth in hell.

70
00:05:02,680 --> 00:05:09,160
 Birth as a ghost comes from greed.

71
00:05:09,160 --> 00:05:14,060
 Person has a great amount of greed, intense greed, stingy,

72
00:05:14,060 --> 00:05:15,840
 miserly, clingy.

73
00:05:15,840 --> 00:05:26,120
 Then the birth that comes from that is birth as a ghost.

74
00:05:26,120 --> 00:05:30,240
 The person has a great amount of delusion.

75
00:05:30,240 --> 00:05:36,680
 When the birth that comes from that is birth as an animal.

76
00:05:36,680 --> 00:05:41,220
 These are important conceptually to give us a sort of a

77
00:05:41,220 --> 00:05:43,840
 broader picture of the nature

78
00:05:43,840 --> 00:05:48,120
 of reality, the nature of defilements.

79
00:05:48,120 --> 00:05:53,710
 Give us a sense of an important distinction among mind

80
00:05:53,710 --> 00:05:56,640
 states and among beings.

81
00:05:56,640 --> 00:06:01,820
 As you see humans who are, they have words in Pali like "

82
00:06:01,820 --> 00:06:03,880
manusa peto" and a human who

83
00:06:03,880 --> 00:06:09,440
 is like a ghost, hungry, never satisfied.

84
00:06:09,440 --> 00:06:12,970
 The stories that we hear about ghosts are beings who are

85
00:06:12,970 --> 00:06:14,880
 stuck on a place, some place

86
00:06:14,880 --> 00:06:16,120
 that was very important to them.

87
00:06:16,120 --> 00:06:20,920
 So the intense clinging kept them there when they passed

88
00:06:20,920 --> 00:06:21,680
 away.

89
00:06:21,680 --> 00:06:26,930
 They are always wanting, they are always yearning for

90
00:06:26,930 --> 00:06:28,360
 something.

91
00:06:28,360 --> 00:06:33,910
 We have many ghost stories, stories of ghosts, descriptions

92
00:06:33,910 --> 00:06:36,160
 of ghosts in Buddhism.

93
00:06:36,160 --> 00:06:39,600
 But you can see humans who are like that, never satisfied.

94
00:06:39,600 --> 00:06:44,760
 Drug addicts, a good example.

95
00:06:44,760 --> 00:06:49,530
 You can see humans, manusan, nirayiko, I think, something

96
00:06:49,530 --> 00:06:52,080
 like that, someone who is a human

97
00:06:52,080 --> 00:06:56,120
 but hellish, like a hell being.

98
00:06:56,120 --> 00:07:00,030
 If you've ever gotten really angry, you know what that's

99
00:07:00,030 --> 00:07:00,680
 like.

100
00:07:00,680 --> 00:07:03,560
 It's hell to be so angry.

101
00:07:03,560 --> 00:07:07,630
 If you've ever seen people who are so angry, they look like

102
00:07:07,630 --> 00:07:08,480
 demons.

103
00:07:08,480 --> 00:07:15,580
 So we all have that in us, but a person who is intent upon

104
00:07:15,580 --> 00:07:19,040
 that, is most likely to be

105
00:07:19,040 --> 00:07:24,520
 reborn in hell if it's their life, angry about everything,

106
00:07:24,520 --> 00:07:26,920
 mostly mean and cruel.

107
00:07:26,920 --> 00:07:29,430
 Honestly, it doesn't mean just because we're angry or

108
00:07:29,430 --> 00:07:30,960
 greedy that we're likely to go to

109
00:07:30,960 --> 00:07:31,960
 these places.

110
00:07:31,960 --> 00:07:36,890
 They need to be breaking the five precepts, killing and

111
00:07:36,890 --> 00:07:38,840
 stealing and so on.

112
00:07:38,840 --> 00:07:42,080
 And the same with the animal realm.

113
00:07:42,080 --> 00:07:46,270
 You see humans who are so deluded, intent upon remaining

114
00:07:46,270 --> 00:07:48,480
 ignorant, with no interest

115
00:07:48,480 --> 00:07:52,300
 in higher qualities.

116
00:07:52,300 --> 00:07:55,940
 Humans who are just like the cows that we see, content to

117
00:07:55,940 --> 00:07:57,200
 eat and ruminate.

118
00:07:57,200 --> 00:08:03,400
 Ruminate does not do that.

119
00:08:03,400 --> 00:08:11,080
 Content to chew their cud, as it were.

120
00:08:11,080 --> 00:08:13,500
 Being a cow isn't the worst, I don't think, but a real

121
00:08:13,500 --> 00:08:15,000
 delusion, you get born in some

122
00:08:15,000 --> 00:08:18,520
 of the bad animal realms.

123
00:08:18,520 --> 00:08:21,880
 A cow can be terrible if you get slaughtered, but the life

124
00:08:21,880 --> 00:08:23,960
 isn't so bad, it's quite peaceful

125
00:08:23,960 --> 00:08:30,330
 compared to, say, being born as a rabbit being hunted by

126
00:08:30,330 --> 00:08:33,880
 wolves, a mouse being hunted by

127
00:08:33,880 --> 00:08:38,610
 cats or squirrel, or animals that fight each other, cats

128
00:08:38,610 --> 00:08:41,800
 that fight each other, dogs, wolves

129
00:08:41,800 --> 00:08:51,720
 that fight and kill each other.

130
00:09:19,060 --> 00:09:20,060
 good realm.

131
00:09:20,060 --> 00:09:24,370
 Born as a human, if you keep the five precepts and are

132
00:09:24,370 --> 00:09:29,180
 generally committed as an individual,

133
00:09:29,180 --> 00:09:33,240
 then the birth that comes from that is as a human.

134
00:09:33,240 --> 00:09:36,890
 And you know, it's more granular than that, you can say,

135
00:09:36,890 --> 00:09:38,900
 because your experience is, if

136
00:09:38,900 --> 00:09:42,600
 you get angry at someone, you're going to get into hell on

137
00:09:42,600 --> 00:09:43,340
 earth.

138
00:09:43,340 --> 00:09:45,940
 You're mean and cruel to people and so on.

139
00:09:45,940 --> 00:09:48,950
 If on the other hand you're keeping the precepts, you'll

140
00:09:48,950 --> 00:09:51,320
 find you live a fairly civilized life.

141
00:09:51,320 --> 00:09:54,940
 You don't get caught up with drunkards and murderers and

142
00:09:54,940 --> 00:09:56,960
 thieves and liars and so on.

143
00:09:56,960 --> 00:09:59,120
 It feels fairly human, humane, right?

144
00:09:59,120 --> 00:10:02,240
 It would be a good word.

145
00:10:02,240 --> 00:10:12,820
 To be born as an angel requires not just keeping the five

146
00:10:12,820 --> 00:10:18,480
 precepts, but it requires something

147
00:10:18,480 --> 00:10:23,520
 we call Mahakusala, goodness.

148
00:10:23,520 --> 00:10:26,480
 So being humane is one thing, treating other people as you

149
00:10:26,480 --> 00:10:28,000
 like to be treated, that sort

150
00:10:28,000 --> 00:10:30,920
 of thing, it's good.

151
00:10:30,920 --> 00:10:34,180
 But people who go above and beyond are the ones who are

152
00:10:34,180 --> 00:10:36,240
 born as angels, people who are

153
00:10:36,240 --> 00:10:37,640
 intent on purification.

154
00:10:37,640 --> 00:10:41,850
 I think a lot of Buddhist meditators, it's a general

155
00:10:41,850 --> 00:10:45,080
 conception that Buddhist meditators

156
00:10:45,080 --> 00:10:47,720
 tend to go to heaven because they're intent.

157
00:10:47,720 --> 00:10:49,960
 I mean, that's the whole thrust of Buddhism, it's not to

158
00:10:49,960 --> 00:10:51,320
 believe in this or believe in

159
00:10:51,320 --> 00:10:53,280
 that, believe in the Buddha.

160
00:10:53,280 --> 00:10:54,380
 What is the core of Buddhism?

161
00:10:54,380 --> 00:10:58,000
 It's really about purifying your mind.

162
00:10:58,000 --> 00:11:02,730
 And so I don't think it's bragging or kind of conceit to

163
00:11:02,730 --> 00:11:05,400
 think that Buddhists are more

164
00:11:05,400 --> 00:11:08,360
 likely to go to heaven because we're intent upon it.

165
00:11:08,360 --> 00:11:10,770
 We don't have these views like you do this and you go to

166
00:11:10,770 --> 00:11:12,120
 heaven, you do that and you

167
00:11:12,120 --> 00:11:14,120
 go to heaven.

168
00:11:14,120 --> 00:11:18,520
 You act heavenly and you go to heaven.

169
00:11:18,520 --> 00:11:22,660
 And it's very much in many ways in line with the Buddha's

170
00:11:22,660 --> 00:11:24,840
 teaching, some of the heavenly

171
00:11:24,840 --> 00:11:33,120
 stuff that, some of the things that would lead to heaven.

172
00:11:33,120 --> 00:11:44,320
 Things like renouncing entertainment and sensuality, eating

173
00:11:44,320 --> 00:11:45,600
 only in the morning as all of you are

174
00:11:45,600 --> 00:11:47,200
 doing, that sort of thing.

175
00:11:47,200 --> 00:11:50,050
 It's a very sort of dedicated, you're not doing it to

176
00:11:50,050 --> 00:11:51,900
 torture yourself or because the

177
00:11:51,900 --> 00:11:54,460
 Buddha said or because you want to feel good about yourself

178
00:11:54,460 --> 00:11:54,680
.

179
00:11:54,680 --> 00:11:57,420
 You're doing it because you understand that it's good

180
00:11:57,420 --> 00:11:58,160
 training.

181
00:11:58,160 --> 00:12:02,120
 It's really useful and it helps you purify your mind.

182
00:12:02,120 --> 00:12:03,440
 That sort of thing leads to heaven.

183
00:12:03,440 --> 00:12:06,000
 But many non-Buddhists of course also go to heaven.

184
00:12:06,000 --> 00:12:08,730
 People who are very kind and charitable, they don't have to

185
00:12:08,730 --> 00:12:11,160
, they're not just doing their

186
00:12:11,160 --> 00:12:15,040
 ordinary duty as a human being but they intend to go out of

187
00:12:15,040 --> 00:12:17,040
 their way to help people who

188
00:12:17,040 --> 00:12:20,830
 are not well off, to be kind, to think good thoughts,

189
00:12:20,830 --> 00:12:23,600
 people who practice metta, bhavana,

190
00:12:23,600 --> 00:12:26,600
 that sort of thing.

191
00:12:26,600 --> 00:12:33,580
 Mahakusala, dana, sila, bhavana, even just keeping sila,

192
00:12:33,580 --> 00:12:34,000
 someone who is set on not lying.

193
00:12:34,000 --> 00:12:35,960
 I'm never going to lie.

194
00:12:35,960 --> 00:12:38,480
 I think there was a story of someone who went to heaven

195
00:12:38,480 --> 00:12:39,520
 because of that.

196
00:12:39,520 --> 00:12:40,680
 I can't remember.

197
00:12:40,680 --> 00:12:42,320
 Maybe I'm not.

198
00:12:42,320 --> 00:12:48,640
 Siniyata precepts is a good sort of example.

199
00:12:48,640 --> 00:12:51,440
 To be born as a Brahma, which is the Buddhist equivalent of

200
00:12:51,440 --> 00:12:52,840
 God or the closest thing we

201
00:12:52,840 --> 00:12:57,560
 have to gods, only because they're different than angels.

202
00:12:57,560 --> 00:12:59,760
 They don't engage in sensuality.

203
00:12:59,760 --> 00:13:03,240
 They're more high-minded, more lofty.

204
00:13:03,240 --> 00:13:08,200
 To get there you have to practice the samatha jhana.

205
00:13:08,200 --> 00:13:12,370
 So a person who is intent upon entering into states of

206
00:13:12,370 --> 00:13:15,320
 absorption that are one-pointed,

207
00:13:15,320 --> 00:13:18,160
 that are out of any sensuality.

208
00:13:18,160 --> 00:13:21,280
 So they don't have any experience of the world.

209
00:13:21,280 --> 00:13:24,040
 Their whole world is one thing, one concept.

210
00:13:24,040 --> 00:13:31,040
 Maybe it's metta, maybe it's a candle flame.

211
00:13:31,040 --> 00:13:36,280
 Different things like that.

212
00:13:36,280 --> 00:13:40,900
 And again, all of these things, going to heaven or becoming

213
00:13:40,900 --> 00:13:43,200
 a god, you can think of them more

214
00:13:43,200 --> 00:13:44,680
 granular as well.

215
00:13:44,680 --> 00:13:48,080
 It helps you to understand and get some kind of confidence

216
00:13:48,080 --> 00:13:50,160
 that, yeah, it makes sense that

217
00:13:50,160 --> 00:13:51,480
 that would happen.

218
00:13:51,480 --> 00:13:55,320
 That's how birth would occur because you can see it in this

219
00:13:55,320 --> 00:13:55,960
 life.

220
00:13:55,960 --> 00:14:00,500
 Unless you have the view that when you die, consciousness

221
00:14:00,500 --> 00:14:01,440
 ceases.

222
00:14:01,440 --> 00:14:04,800
 You can see in this life a person who is kind and generous.

223
00:14:04,800 --> 00:14:06,000
 Maybe bad things happen to them.

224
00:14:06,000 --> 00:14:09,760
 We would say from bad karma, we would try to excuse it off

225
00:14:09,760 --> 00:14:10,520
 by that.

226
00:14:10,520 --> 00:14:14,800
 So many good things happen to them and so many people love

227
00:14:14,800 --> 00:14:16,760
 them and appreciate them.

228
00:14:16,760 --> 00:14:19,230
 And a person who is intent upon samatha meditation, they

229
00:14:19,230 --> 00:14:20,880
 are so fixed and focused, it doesn't

230
00:14:20,880 --> 00:14:25,110
 matter if they get harassed by others because they're high-

231
00:14:25,110 --> 00:14:25,920
minded.

232
00:14:25,920 --> 00:14:32,480
 Brahma is a very high-minded state.

233
00:14:32,480 --> 00:14:38,180
 But none of these six forms of birth come close to birth as

234
00:14:38,180 --> 00:14:40,720
 a Buddha, of course.

235
00:14:40,720 --> 00:14:46,150
 So the birth of the Buddha, after he became enlightened, he

236
00:14:46,150 --> 00:14:49,320
 was asked, he walked to Sarnath,

237
00:14:49,320 --> 00:14:53,240
 to Isipatana, Deer Park.

238
00:14:53,240 --> 00:14:55,400
 And on his way he met a man who asked him, "What are you?"

239
00:14:55,400 --> 00:14:58,920
 He saw, "Oh, this guy is kind of special."

240
00:14:58,920 --> 00:14:59,920
 And he said, "What are you?

241
00:14:59,920 --> 00:15:00,920
 Are you a human?

242
00:15:00,920 --> 00:15:01,920
 Are you a god?

243
00:15:01,920 --> 00:15:02,920
 Are you an angel?

244
00:15:02,920 --> 00:15:03,920
 Are you a Mara?"

245
00:15:03,920 --> 00:15:06,870
 I don't know what exactly he asked, all these different

246
00:15:06,870 --> 00:15:07,520
 things.

247
00:15:07,520 --> 00:15:08,520
 "What are you?"

248
00:15:08,520 --> 00:15:12,220
 In the Buddha roundabout way he said, "I've learned what

249
00:15:12,220 --> 00:15:13,800
 needs to be learned.

250
00:15:13,800 --> 00:15:16,400
 I've conquered all the universe."

251
00:15:16,400 --> 00:15:19,470
 I mean, you're very lofty, sort of lion's roar kind of

252
00:15:19,470 --> 00:15:20,080
 thing.

253
00:15:20,080 --> 00:15:22,440
 And then he said, "I have no teacher.

254
00:15:22,440 --> 00:15:23,800
 There's no one like me.

255
00:15:23,800 --> 00:15:26,560
 There's no one parallel to me, no one above me.

256
00:15:26,560 --> 00:15:30,080
 I am a samatha sambuddha."

257
00:15:30,080 --> 00:15:32,840
 So he differentiated, he didn't answer, "I'm a human or a

258
00:15:32,840 --> 00:15:33,360
 god."

259
00:15:33,360 --> 00:15:37,280
 And some people say, "You know, he wasn't really a human.

260
00:15:37,280 --> 00:15:39,280
 He was a Buddha."

261
00:15:39,280 --> 00:15:41,040
 I think you could say he was a human.

262
00:15:41,040 --> 00:15:44,440
 It's pretty clear that to some extent he was a human.

263
00:15:44,440 --> 00:15:53,290
 But it's also proper to say he was simply a Buddha or a sam

264
00:15:53,290 --> 00:15:56,480
atha sambuddha.

265
00:15:56,480 --> 00:16:01,840
 So we take this as an important event.

266
00:16:01,840 --> 00:16:09,840
 The birth of the Buddha was the story.

267
00:16:09,840 --> 00:16:13,850
 There's an encapsulated sense of when he was born here in

268
00:16:13,850 --> 00:16:18,560
 Lumbini something important happened.

269
00:16:18,560 --> 00:16:21,960
 This was the beginning of the Buddha story.

270
00:16:21,960 --> 00:16:28,680
 You often hear it told this way.

271
00:16:28,680 --> 00:16:34,350
 As Buddhist meditators, when we think of birth, to some

272
00:16:34,350 --> 00:16:37,560
 extent we have to look at all of these

273
00:16:37,560 --> 00:16:40,080
 places and our whole pilgrimage in a different way.

274
00:16:40,080 --> 00:16:43,610
 And that's, I think, what we're all kind of struggling with

275
00:16:43,610 --> 00:16:45,200
, is maybe too strong of a

276
00:16:45,200 --> 00:16:49,670
 word, but grappling with, trying to align ourselves in the

277
00:16:49,670 --> 00:16:50,760
 right way.

278
00:16:50,760 --> 00:16:56,290
 Because we see that simply being excited to go to see, "Oh,

279
00:16:56,290 --> 00:16:58,480
 this was the birth."

280
00:16:58,480 --> 00:17:01,920
 It's not really where it's at.

281
00:17:01,920 --> 00:17:04,490
 It's not the way to be free from suffering or to follow the

282
00:17:04,490 --> 00:17:05,760
 Buddha's teachings even,

283
00:17:05,760 --> 00:17:09,120
 to put it more strongly.

284
00:17:09,120 --> 00:17:13,640
 The Buddha didn't intend for us to go tourist-ing.

285
00:17:13,640 --> 00:17:19,280
 He didn't intend for us to come here and think, "Oh, I'm

286
00:17:19,280 --> 00:17:22,240
 excited to be in Lumbini."

287
00:17:22,240 --> 00:17:24,320
 So we have to think about births.

288
00:17:24,320 --> 00:17:29,080
 Ultimately, birth comes down to momentary birth.

289
00:17:29,080 --> 00:17:32,470
 Every time an emotion arises, that's the birth of that

290
00:17:32,470 --> 00:17:33,280
 emotion.

291
00:17:33,280 --> 00:17:40,920
 And suffering arises when the paramese arise.

292
00:17:40,920 --> 00:17:44,360
 What was most important about the Buddha is that he gave

293
00:17:44,360 --> 00:17:46,920
 birth to what we call the paramitas,

294
00:17:46,920 --> 00:17:49,640
 the ten perfections.

295
00:17:49,640 --> 00:17:52,040
 He gave birth to Dāna, charity.

296
00:17:52,040 --> 00:17:55,520
 He gave away his own eyes once.

297
00:17:55,520 --> 00:17:59,770
 What it really means is he gave up any sense of possess

298
00:17:59,770 --> 00:18:02,360
iveness, any sense of self.

299
00:18:02,360 --> 00:18:06,970
 This was a big part of why he was able to finally see non-

300
00:18:06,970 --> 00:18:09,240
self and have a sense that

301
00:18:09,240 --> 00:18:16,880
 reality doesn't admit possession and ego and so on.

302
00:18:16,880 --> 00:18:17,880
 Because he sacrificed.

303
00:18:17,880 --> 00:18:23,160
 He made a determination to not take possession of things,

304
00:18:23,160 --> 00:18:26,680
 to not claim ownership of things.

305
00:18:26,680 --> 00:18:33,480
 Sīla was intent on morality, ethics.

306
00:18:33,480 --> 00:18:40,000
 He wasn't always a monk or an ascetic even.

307
00:18:40,000 --> 00:18:41,000
 He had children.

308
00:18:41,000 --> 00:18:43,850
 He broke even the five precepts sometimes, but for the most

309
00:18:43,850 --> 00:18:45,240
 part, actually I think he'd

310
00:18:45,240 --> 00:18:49,360
 be hard pressed to find even an example.

311
00:18:49,360 --> 00:18:52,820
 And some people might say maybe the truth, maybe the

312
00:18:52,820 --> 00:18:55,040
 orthodox idea is that he kept the

313
00:18:55,040 --> 00:19:00,920
 five precepts until I can't think of an instance where he

314
00:19:00,920 --> 00:19:02,480
 broke them.

315
00:19:02,480 --> 00:19:07,470
 There was something about him being a thief, but I'm not

316
00:19:07,470 --> 00:19:09,280
 sure about that.

317
00:19:09,280 --> 00:19:13,330
 Anyway what he developed from the time of Deepankara until

318
00:19:13,330 --> 00:19:15,080
 the time of his last birth

319
00:19:15,080 --> 00:19:21,320
 was a profound sense of ethics whereby even to save his own

320
00:19:21,320 --> 00:19:25,760
 life he wouldn't break a precept.

321
00:19:25,760 --> 00:19:28,320
 He wouldn't do something immoral.

322
00:19:28,320 --> 00:19:32,960
 Even to save his own life, to save someone else's life.

323
00:19:32,960 --> 00:19:36,410
 A lot of it being beyond our ordinary sense of ethics or

324
00:19:36,410 --> 00:19:38,960
 even charity or any of the perfections

325
00:19:38,960 --> 00:19:43,700
 because he knew that he had a broader sense of trying to

326
00:19:43,700 --> 00:19:46,560
 keep people from suffering.

327
00:19:46,560 --> 00:19:50,020
 Maybe if I kill someone it will help someone else or a lot

328
00:19:50,020 --> 00:19:51,320
 of other people.

329
00:19:51,320 --> 00:19:54,470
 He had a deeper understanding of it all being samsara and

330
00:19:54,470 --> 00:19:57,440
 you can't really free people from

331
00:19:57,440 --> 00:20:03,690
 their own suffering because they pass away and they're born

332
00:20:03,690 --> 00:20:04,760
 again.

333
00:20:04,760 --> 00:20:08,140
 The only way would be to purify his mind to find the way

334
00:20:08,140 --> 00:20:10,320
 out and then he could help people

335
00:20:10,320 --> 00:20:11,640
 because there was no way out.

336
00:20:11,640 --> 00:20:15,780
 There's no way to fix people's problems or even your own

337
00:20:15,780 --> 00:20:16,720
 problem.

338
00:20:16,720 --> 00:20:20,230
 They come up arami, the renunciation, many times he gave up

339
00:20:20,230 --> 00:20:22,920
 kingship, he gave up wealth,

340
00:20:22,920 --> 00:20:23,920
 renunciation.

341
00:20:25,400 --> 00:20:28,960
 Panya, Parami, there are so many stories you hear.

342
00:20:28,960 --> 00:20:32,530
 There's a very good story, the Maha-um-Maga Jataka, where

343
00:20:32,530 --> 00:20:34,360
 he was even at seven years old

344
00:20:34,360 --> 00:20:38,180
 he was solving people's problems for them and the wisdom

345
00:20:38,180 --> 00:20:39,680
 that he displayed.

346
00:20:39,680 --> 00:20:44,950
 But his appreciation and his dedication to understanding,

347
00:20:44,950 --> 00:20:47,560
 to leaving home and finding,

348
00:20:47,560 --> 00:20:52,920
 understanding his own mind and understanding the world.

349
00:20:52,920 --> 00:20:58,220
 Vidyaparami effort, one time the bodhisatta was shipwrecked

350
00:20:58,220 --> 00:21:00,400
 and he said, he just swam

351
00:21:00,400 --> 00:21:03,730
 across the ocean and this angel in the ocean said, "What

352
00:21:03,730 --> 00:21:04,800
 are you doing?

353
00:21:04,800 --> 00:21:07,150
 You're never going to make it to the ocean, you're in the

354
00:21:07,150 --> 00:21:08,280
 middle of the ocean."

355
00:21:08,280 --> 00:21:11,500
 And he said, "Tell me what can't be, I don't know if you

356
00:21:11,500 --> 00:21:13,480
 said, one time the Buddha said,

357
00:21:13,480 --> 00:21:16,280
 "Tell me what can't be achieved by striving."

358
00:21:16,280 --> 00:21:18,640
 He said, "If I don't try then I'm lost."

359
00:21:18,640 --> 00:21:23,970
 That story, that's Maha-janaka, it's a very famous Jataka

360
00:21:23,970 --> 00:21:26,320
 and the angel picked him up and

361
00:21:26,320 --> 00:21:30,410
 took him to the edge of the ocean, was quite impressed by

362
00:21:30,410 --> 00:21:31,920
 his dedication.

363
00:21:31,920 --> 00:21:35,320
 The Buddha had never gave up.

364
00:21:35,320 --> 00:21:37,440
 That's a good way of summing it up.

365
00:21:37,440 --> 00:21:38,440
 What does effort mean?

366
00:21:38,440 --> 00:21:39,440
 He never gave up.

367
00:21:39,440 --> 00:21:41,590
 And that's an example, when we talk about effort in

368
00:21:41,590 --> 00:21:43,160
 meditation courses, it's not that

369
00:21:43,160 --> 00:21:46,270
 you have to work really hard, it's that when you fail, when

370
00:21:46,270 --> 00:21:47,800
 you feel like you just can't

371
00:21:47,800 --> 00:21:49,800
 do it, try again.

372
00:21:49,800 --> 00:21:52,640
 And if you have that capacity to try again, when you feel

373
00:21:52,640 --> 00:21:54,600
 like you just can't do it anymore,

374
00:21:54,600 --> 00:21:56,600
 that's effort.

375
00:21:56,600 --> 00:22:00,950
 Kanti, patience, we learn all about that in meditation, but

376
00:22:00,950 --> 00:22:02,760
 the Buddha's patience, he

377
00:22:02,760 --> 00:22:07,590
 had his ears cut off, his nose cut off, one life, his hands

378
00:22:07,590 --> 00:22:08,520
 cut off.

379
00:22:08,520 --> 00:22:12,590
 This king was very angry and jealous and thought he was

380
00:22:12,590 --> 00:22:15,400
 stealing his women, cut off all his

381
00:22:15,400 --> 00:22:16,400
 limbs.

382
00:22:16,400 --> 00:22:19,720
 He said, "Do you think my patience is in my ears?"

383
00:22:19,720 --> 00:22:24,400
 He said, "My patience is deeper than that."

384
00:22:24,400 --> 00:22:30,160
 And he died patient in that life.

385
00:22:30,160 --> 00:22:35,240
 Satchaparami, the Buddha, had great Satcha.

386
00:22:35,240 --> 00:22:36,520
 You could see it at the end.

387
00:22:36,520 --> 00:22:42,250
 Satcha means being true to yourself, having a vision, being

388
00:22:42,250 --> 00:22:44,560
 true to an intention.

389
00:22:44,560 --> 00:22:46,540
 So when Mara came and the Buddha was seated under the Bodhi

390
00:22:46,540 --> 00:22:47,640
 tree and he said, "You don't

391
00:22:47,640 --> 00:22:49,940
 deserve to be there," the Buddha said, "I do deserve to be

392
00:22:49,940 --> 00:22:51,080
 there, and the earth is my

393
00:22:51,080 --> 00:22:52,080
 witness."

394
00:22:52,080 --> 00:22:57,480
 And he touched the earth and Mara just got thrown away, got

395
00:22:57,480 --> 00:23:00,000
 blown away by the water.

396
00:23:00,000 --> 00:23:03,610
 But it was Satcha, it was the power of, you can say, "I don

397
00:23:03,610 --> 00:23:05,600
't deserve it all you want."

398
00:23:05,600 --> 00:23:09,760
 And the Buddha's the power of, "I do deserve to sit here."

399
00:23:09,760 --> 00:23:11,320
 That was truth.

400
00:23:11,320 --> 00:23:13,920
 It was very powerful.

401
00:23:13,920 --> 00:23:18,100
 There's many examples of monks in the stories and laypeople

402
00:23:18,100 --> 00:23:19,200
 using truth.

403
00:23:19,200 --> 00:23:22,920
 Angulimala is one example.

404
00:23:22,920 --> 00:23:25,920
 Aditāna, when the Buddha made a determination to become a

405
00:23:25,920 --> 00:23:27,640
 Buddha and then never wavered,

406
00:23:27,640 --> 00:23:30,210
 when he made a determination for his bowl to float upstream

407
00:23:30,210 --> 00:23:31,480
, which can't happen, you

408
00:23:31,480 --> 00:23:32,480
 know, right?

409
00:23:32,480 --> 00:23:33,480
 Bowls don't float upstream.

410
00:23:33,480 --> 00:23:39,440
 He made the determination that it should and it did.

411
00:23:39,440 --> 00:23:44,440
 Veda-parami, the Buddha's friendliness, the bodhisattas,

412
00:23:44,440 --> 00:23:47,080
 cultivation of friendliness even

413
00:23:47,080 --> 00:23:50,480
 when people were angry at him.

414
00:23:50,480 --> 00:23:55,000
 And equanimity, how he didn't look at one person or another

415
00:23:55,000 --> 00:23:56,920
 as greater or lesser.

416
00:23:56,920 --> 00:24:02,730
 He never, he cultivated the capacity to not be attached and

417
00:24:02,730 --> 00:24:03,960
 partial.

418
00:24:03,960 --> 00:24:07,160
 These are the ten paramitas.

419
00:24:07,160 --> 00:24:12,040
 These are the things that the Buddha gave birth to.

420
00:24:12,040 --> 00:24:16,600
 Of course he also gave birth to suffering as we all do.

421
00:24:16,600 --> 00:24:21,110
 Birth as a human is birth of suffering and birth of things

422
00:24:21,110 --> 00:24:23,840
 that can cause you suffering.

423
00:24:23,840 --> 00:24:27,630
 So a part of our practice is to be aware of birth when it

424
00:24:27,630 --> 00:24:30,160
 occurs, meaning experience.

425
00:24:30,160 --> 00:24:32,860
 When an experience arises, it doesn't need to cause you

426
00:24:32,860 --> 00:24:34,520
 suffering, even though you could

427
00:24:34,520 --> 00:24:38,030
 argue or whatever, it doesn't need to cause you suffering

428
00:24:38,030 --> 00:24:40,000
 unless you cling to it, unless

429
00:24:40,000 --> 00:24:42,080
 you get caught up in it.

430
00:24:42,080 --> 00:24:45,320
 And so we're very much concerned about birth.

431
00:24:45,320 --> 00:24:48,380
 We're also concerned about death, which is something we can

432
00:24:48,380 --> 00:24:49,840
 talk about at the next place

433
00:24:49,840 --> 00:24:51,440
 maybe.

434
00:24:51,440 --> 00:24:56,020
 But we're concerned about the birth of experiences.

435
00:24:56,020 --> 00:24:59,150
 When something comes up, the birth of problems, birth of

436
00:24:59,150 --> 00:25:01,220
 anything, because we're concerned

437
00:25:01,220 --> 00:25:03,960
 about how we are going to relate to it.

438
00:25:03,960 --> 00:25:06,960
 That's something that's very important to us.

439
00:25:06,960 --> 00:25:10,130
 You feel a pain, that something was born, and that's very

440
00:25:10,130 --> 00:25:11,640
 important because how you

441
00:25:11,640 --> 00:25:15,480
 deal with that, that thing could cause you a lot of

442
00:25:15,480 --> 00:25:17,560
 suffering if you let it.

443
00:25:17,560 --> 00:25:22,240
 Someone says something to you, birth, birth of sound, that

444
00:25:22,240 --> 00:25:23,640
 sound was born.

445
00:25:23,640 --> 00:25:25,640
 That's a very important thing because that sound could

446
00:25:25,640 --> 00:25:28,360
 cause you to do all sorts of things.

447
00:25:28,360 --> 00:25:34,680
 It could taste food, it could cause you...

448
00:25:34,680 --> 00:25:38,600
 Birth of experience is a very important thing.

449
00:25:38,600 --> 00:25:42,420
 We look at the world like this, experiences, and that's the

450
00:25:42,420 --> 00:25:44,000
 second type of birth.

451
00:25:44,000 --> 00:25:51,110
 Birth is a God or an angel or a human in hell or so, and

452
00:25:51,110 --> 00:25:54,720
 that's conventional.

453
00:25:54,720 --> 00:25:56,640
 We look at these two types of birth.

454
00:25:56,640 --> 00:26:04,080
 We come here, it's a good reason to talk about them both.

455
00:26:04,080 --> 00:26:08,910
 Even on a conventional level, putting them both together, I

456
00:26:08,910 --> 00:26:11,480
 guess, we come here to celebrate

457
00:26:11,480 --> 00:26:15,290
 the Buddha's birth in a conventional sense as a means of

458
00:26:15,290 --> 00:26:17,720
 giving birth ourselves to wholesome

459
00:26:17,720 --> 00:26:21,960
 qualities of reverence and respect and appreciation.

460
00:26:21,960 --> 00:26:27,920
 We come here to revere this very monumentous event where

461
00:26:27,920 --> 00:26:31,200
 the Buddha was finally born in

462
00:26:31,200 --> 00:26:33,950
 his last life, where he would become the Buddha, give rise

463
00:26:33,950 --> 00:26:35,520
 to the Dhamma, give birth to the

464
00:26:35,520 --> 00:26:39,940
 Dhamma, and the Dhamma would be born and then the Sangha

465
00:26:39,940 --> 00:26:41,400
 would be born.

466
00:26:41,400 --> 00:26:50,620
 We would all be able to be here to have this precious Dham

467
00:26:50,620 --> 00:26:51,720
ma.

468
00:26:51,720 --> 00:26:54,120
 Some thoughts on Lumbini.

469
00:26:54,120 --> 00:26:56,240
 We are here now.

470
00:26:56,240 --> 00:26:59,510
 That's the pond where she bathed and behind that is the

471
00:26:59,510 --> 00:27:01,560
 pillar and that big building has

472
00:27:01,560 --> 00:27:07,200
 apparently the exact spot where the Buddha was born.

473
00:27:07,200 --> 00:27:08,720
 Thank you.

