1
00:00:00,000 --> 00:00:02,940
 "When you think of the Buddha's appearance, do you think of

2
00:00:02,940 --> 00:00:09,520
 him as in the iconography

3
00:00:09,520 --> 00:00:11,640
 or in another way?

4
00:00:11,640 --> 00:00:16,640
 And what's your view on the 32 signs of a great man?"

5
00:00:16,640 --> 00:00:23,530
 No, I don't think of him really like most of the icon

6
00:00:23,530 --> 00:00:25,160
ography.

7
00:00:25,160 --> 00:00:32,330
 I don't think of him as made of brass or stone or with all

8
00:00:32,330 --> 00:00:35,320
 the weird colors that they put

9
00:00:35,320 --> 00:00:43,520
 him in.

10
00:00:43,520 --> 00:00:45,720
 I think of him in a robe.

11
00:00:45,720 --> 00:00:48,640
 Usually I think of him with me bowing down on his feet.

12
00:00:48,640 --> 00:00:53,190
 I tell you the truth, when I think of the Buddha or the San

13
00:00:53,190 --> 00:00:55,440
gha, either way I think of

14
00:00:55,440 --> 00:00:56,440
 myself.

15
00:00:56,440 --> 00:01:00,400
 I see their feet first really, and I see them in robes.

16
00:01:00,400 --> 00:01:06,480
 It's just a vision that comes.

17
00:01:06,480 --> 00:01:13,130
 The 32 signs of a great man are a source of a lot of

18
00:01:13,130 --> 00:01:17,760
 difficulty I think for Westerners.

19
00:01:17,760 --> 00:01:20,560
 I was asking about them, let's be open-minded.

20
00:01:20,560 --> 00:01:24,420
 I was arguing, playing devil's advocate, and I said, "Well,

21
00:01:24,420 --> 00:01:26,280
 how is it possible for someone's

22
00:01:26,280 --> 00:01:30,960
 mouth, someone's tongue to cover their forehead?"

23
00:01:30,960 --> 00:01:33,040
 I mean, really.

24
00:01:33,040 --> 00:01:36,880
 Getting your tongue to your ears is...

25
00:01:36,880 --> 00:01:40,920
 Or getting your tongue to your ears, how is that possible?

26
00:01:40,920 --> 00:01:46,760
 So we argued it.

27
00:01:46,760 --> 00:01:49,800
 My feeling was it really just meant that he had a long

28
00:01:49,800 --> 00:01:50,520
 tongue.

29
00:01:50,520 --> 00:01:53,520
 But no, people take it literally that his tongue could

30
00:01:53,520 --> 00:01:55,160
 cover his whole forehead and

31
00:01:55,160 --> 00:02:00,520
 touch both ears.

32
00:02:00,520 --> 00:02:04,000
 The argument that they use is that there are very strange

33
00:02:04,000 --> 00:02:05,920
 sorts of beings out there.

34
00:02:05,920 --> 00:02:10,800
 There are people with very strange body parts.

35
00:02:10,800 --> 00:02:15,250
 For example, one interesting thing I found was I was

36
00:02:15,250 --> 00:02:18,160
 recently alerted to the fact that

37
00:02:18,160 --> 00:02:21,560
 Mogulana is said to have had blue skin.

38
00:02:21,560 --> 00:02:26,080
 He's shown with blue skin in Sri Lanka.

39
00:02:26,080 --> 00:02:29,040
 Whenever you see a picture of Mogulana, he's blue.

40
00:02:29,040 --> 00:02:30,520
 So I asked, "Why is that monk blue?"

41
00:02:30,520 --> 00:02:32,520
 And I said, "It's Mogulana."

42
00:02:32,520 --> 00:02:36,040
 And they explained it's because he just came from hell.

43
00:02:36,040 --> 00:02:40,540
 The fear of hell was so sharp in him that he was born with

44
00:02:40,540 --> 00:02:41,680
 blue skin.

45
00:02:41,680 --> 00:02:42,680
 And so that's hard to believe.

46
00:02:42,680 --> 00:02:46,010
 But then I read in this book that someone in England a few

47
00:02:46,010 --> 00:02:47,960
 hundred years ago was actually

48
00:02:47,960 --> 00:02:50,760
 born with blue skin and had blue skin through their life.

49
00:02:50,760 --> 00:02:57,600
 So that's an example of how...

50
00:02:57,600 --> 00:03:00,040
 You really can't say for sure.

51
00:03:00,040 --> 00:03:05,450
 What I would say is that with anything like this, it's

52
00:03:05,450 --> 00:03:08,680
 really just a matter of fact.

53
00:03:08,680 --> 00:03:13,620
 Was it true that the Buddha had a tongue that could touch

54
00:03:13,620 --> 00:03:16,760
 his ears and many other...

55
00:03:16,760 --> 00:03:18,080
 That's really the biggest one.

56
00:03:18,080 --> 00:03:21,200
 One of the poses is the biggest problem for me.

57
00:03:21,200 --> 00:03:25,950
 I think there was another one, but the tongue one is

58
00:03:25,950 --> 00:03:28,360
 probably the biggest.

59
00:03:28,360 --> 00:03:31,960
 So did he have such a tongue or did he not?

60
00:03:31,960 --> 00:03:36,040
 Once you pose that question, you can of course ask yourself

61
00:03:36,040 --> 00:03:37,680
, "Does it matter?"

62
00:03:37,680 --> 00:03:39,840
 And of course it doesn't.

63
00:03:39,840 --> 00:03:41,200
 There's so many questions like that.

64
00:03:41,200 --> 00:03:42,620
 Was it true that the Buddha did this?

65
00:03:42,620 --> 00:03:45,080
 Was it true that the Buddha did that?

66
00:03:45,080 --> 00:03:46,200
 Is it factually correct?

67
00:03:46,200 --> 00:03:50,000
 Are all these stories actually true that we hear about dev

68
00:03:50,000 --> 00:03:52,280
as coming down and this happening

69
00:03:52,280 --> 00:03:55,480
 and that happening?

70
00:03:55,480 --> 00:03:59,040
 Did the Buddha really perform this miracle or that miracle?

71
00:03:59,040 --> 00:04:00,840
 It's just a question of historical fact.

72
00:04:00,840 --> 00:04:03,000
 Did it happen or did it not?

73
00:04:03,000 --> 00:04:06,360
 And then does it matter whether it did or not?

74
00:04:06,360 --> 00:04:10,030
 Because in Buddhism it doesn't matter whether the Buddha

75
00:04:10,030 --> 00:04:12,240
 performed any miracles or not.

76
00:04:12,240 --> 00:04:16,920
 It doesn't matter whether he was a dwarf or a hunchback.

77
00:04:16,920 --> 00:04:20,880
 I'm sorry, I don't mean to be disrespectful.

78
00:04:20,880 --> 00:04:25,310
 I'm not trying to disrespect, but it has no impact on the D

79
00:04:25,310 --> 00:04:26,160
hamma.

80
00:04:26,160 --> 00:04:27,680
 It has some impact on our faith.

81
00:04:27,680 --> 00:04:31,900
 And if the Buddha had been a parish, a thought, a dwarf or

82
00:04:31,900 --> 00:04:34,780
 a hunchback, we would think, "Well,

83
00:04:34,780 --> 00:04:40,440
 how could such a perfect person be born in such a state?"

84
00:04:40,440 --> 00:04:43,060
 Based on the teachings of the Buddha, you'd have to think

85
00:04:43,060 --> 00:04:44,400
 of him as being born in a much

86
00:04:44,400 --> 00:04:46,600
 more perfect form.

87
00:04:46,600 --> 00:04:54,910
 But it doesn't have an impact on our practice, other than

88
00:04:54,910 --> 00:04:57,560
 that, Alfred.

89
00:04:57,560 --> 00:05:04,560
 Yeah, for me, there are certain suttas, what I read, just I

90
00:05:04,560 --> 00:05:07,720
 don't look at the whole canon

91
00:05:07,720 --> 00:05:11,040
 as the inherent word of the Buddha.

92
00:05:11,040 --> 00:05:16,720
 Certain suttas just stand out as truth to me, to my inner

93
00:05:16,720 --> 00:05:19,360
 core, and some do not.

94
00:05:19,360 --> 00:05:22,820
 And so I will look at them and just put them on a back

95
00:05:22,820 --> 00:05:25,440
 burner and don't get too caught up

96
00:05:25,440 --> 00:05:26,440
 to it.

97
00:05:26,440 --> 00:05:28,520
 I realize how old these texts are.

98
00:05:28,520 --> 00:05:32,880
 And there's that one and a few others that just, when I

99
00:05:32,880 --> 00:05:35,440
 read them, jump out at me like

100
00:05:35,440 --> 00:05:39,680
 it's possibly a later edition.

101
00:05:39,680 --> 00:05:45,160
 I don't know, but I don't let it bother me because the ones

102
00:05:45,160 --> 00:05:48,000
 that ring true, ring deeply

103
00:05:48,000 --> 00:05:52,200
 true to me.

104
00:05:52,200 --> 00:05:58,990
 And the others I just read and I appreciate the fact that

105
00:05:58,990 --> 00:06:02,400
 they're there, but I realize

106
00:06:02,400 --> 00:06:10,000
 that I don't think that this whole canon is without error.

107
00:06:10,000 --> 00:06:15,030
 Yeah, but the only thing that I would say in defense of it

108
00:06:15,030 --> 00:06:17,440
 is that we are not perfect

109
00:06:17,440 --> 00:06:18,440
 either.

110
00:06:18,440 --> 00:06:23,460
 I mean, this example, I'm not going to spend too much time

111
00:06:23,460 --> 00:06:26,080
 defending it, but I have had

112
00:06:26,080 --> 00:06:28,840
 examples of that sort of thing.

113
00:06:28,840 --> 00:06:32,890
 And you have to realize that everyone, we come to the Dham

114
00:06:32,890 --> 00:06:34,560
ma with prejudices.

115
00:06:34,560 --> 00:06:37,840
 That's why it's so important for us.

116
00:06:37,840 --> 00:06:42,040
 And those prejudices are going to color what we find valid

117
00:06:42,040 --> 00:06:44,120
 and what we find invalid.

118
00:06:44,120 --> 00:06:48,520
 So I've had arguments with monks about this sutta being

119
00:06:48,520 --> 00:06:50,160
 invalid or that.

120
00:06:50,160 --> 00:06:54,920
 And once you point something out to them, they have to

121
00:06:54,920 --> 00:06:58,000
 concede the fact that their whole

122
00:06:58,000 --> 00:07:02,840
 reason for doubting it was improbable.

123
00:07:02,840 --> 00:07:06,250
 So for example, just give a simple, silly example, the fact

124
00:07:06,250 --> 00:07:07,720
 that Mughal-Ana had blue

125
00:07:07,720 --> 00:07:08,720
 skin.

126
00:07:08,720 --> 00:07:13,400
 And as I said, it isn't without precedence, even in

127
00:07:13,400 --> 00:07:15,400
 recorded history.

128
00:07:15,400 --> 00:07:20,600
 So who knows?

129
00:07:20,600 --> 00:07:24,000
 We aren't perfect that we know everything that's possible.

130
00:07:24,000 --> 00:07:26,920
 We don't know whether it could have been the fact that the

131
00:07:26,920 --> 00:07:28,400
 Buddha did have such a big tongue.

132
00:07:28,400 --> 00:07:31,060
 And of course, we don't know that it was maybe just an

133
00:07:31,060 --> 00:07:32,080
 exaggeration.

134
00:07:32,080 --> 00:07:34,040
 Of course, it might not be.

135
00:07:34,040 --> 00:07:37,350
 The point, I think, being that it's not really a core

136
00:07:37,350 --> 00:07:38,560
 Buddhist teaching.

137
00:07:38,560 --> 00:07:42,650
 I mean, it's not going to shake the foundations of Buddhism

138
00:07:42,650 --> 00:07:44,640
 if we find out that the Buddha

139
00:07:44,640 --> 00:07:51,880
 didn't really have such a tongue.

140
00:07:51,880 --> 00:07:54,860
 But definitely in this case, I'd have to say that it's not

141
00:07:54,860 --> 00:07:57,520
 one of the sutta's that I reread

142
00:07:57,520 --> 00:08:01,440
 as inspiration for my own practice.

143
00:08:01,440 --> 00:08:04,580
 Although there are some aspects of the, I believe it's the

144
00:08:04,580 --> 00:08:07,320
 Mahaprasala-Khanasutta, or

145
00:08:07,320 --> 00:08:11,240
 I don't remember what it's called, the one in the Digha Nik

146
00:08:11,240 --> 00:08:13,000
aya with the dhakanas, that

147
00:08:13,000 --> 00:08:16,500
 has some really interesting things, like how the Buddha

148
00:08:16,500 --> 00:08:18,440
 would chew every rice grain.

149
00:08:18,440 --> 00:08:22,800
 No rice grain was swallowed without having been chewed.

150
00:08:22,800 --> 00:08:25,360
 Now that is inspiring.

151
00:08:25,360 --> 00:08:28,760
 The way he sat, the way he walked, the way he talked, those

152
00:08:28,760 --> 00:08:30,440
 parts of his mannerism, and

153
00:08:30,440 --> 00:08:35,480
 then how he says, and that's a small fragment of the

154
00:08:35,480 --> 00:08:38,080
 virtues of the Buddha.

155
00:08:38,080 --> 00:08:41,140
 If you read that, it's just an incredibly inspiring passage

156
00:08:41,140 --> 00:08:41,360
.

157
00:08:41,360 --> 00:08:45,020
 Of course, he kind of spoils it when you're still thinking,

158
00:08:45,020 --> 00:08:47,000
 "But what about the tongue?"

159
00:08:47,000 --> 00:08:52,900
 But no, I think that there's something really valuable in

160
00:08:52,900 --> 00:08:56,240
 that, the one particular sutta

161
00:08:56,240 --> 00:08:57,240
 that I'm thinking.

162
00:08:57,240 --> 00:08:58,240
 I want to caution...

163
00:08:58,240 --> 00:09:01,240
 It doesn't mean that they don't exist, you see it?

164
00:09:01,240 --> 00:09:02,240
 No.

165
00:09:02,240 --> 00:09:03,240
 Like magical powers.

166
00:09:03,240 --> 00:09:11,530
 If you don't have them, it doesn't mean that they don't

167
00:09:11,530 --> 00:09:13,080
 exist.

168
00:09:13,080 --> 00:09:14,440
 Exactly.

169
00:09:14,440 --> 00:09:17,350
 Many people are totally, they will throw out all the

170
00:09:17,350 --> 00:09:19,760
 magical powers, which is ridiculous.

171
00:09:19,760 --> 00:09:23,610
 There are many people out there who have had experiences

172
00:09:23,610 --> 00:09:25,880
 that are magical, that can only

173
00:09:25,880 --> 00:09:29,720
 be described as out of body experiences, whatever.

174
00:09:29,720 --> 00:09:35,920
 And I'm convinced that that is reality.

175
00:09:35,920 --> 00:09:39,050
 Anyone who practices meditation sees that things aren't

176
00:09:39,050 --> 00:09:40,680
 exactly as they seem here.

177
00:09:40,680 --> 00:09:44,210
 One day, maybe one day, physics will catch up and start to

178
00:09:44,210 --> 00:09:45,040
 realize.

179
00:09:45,040 --> 00:09:48,520
 Look at quantum physics, how it's totally shattered our

180
00:09:48,520 --> 00:09:50,460
 concept of what reality is.

181
00:09:50,460 --> 00:09:53,080
 We can't really describe reality anymore.

182
00:09:53,080 --> 00:09:56,680
 So no one's in a position to say this can't do...

183
00:09:56,680 --> 00:09:59,960
 So another point that you might make is, from our

184
00:09:59,960 --> 00:10:03,200
 understanding of magical powers, if someone

185
00:10:03,200 --> 00:10:08,760
 wants their tongue to touch their toes, it's possible to

186
00:10:08,760 --> 00:10:11,880
 develop a power in some radical

187
00:10:11,880 --> 00:10:14,120
 way to do that.

188
00:10:14,120 --> 00:10:18,640
 There are magical things that can be done with extreme

189
00:10:18,640 --> 00:10:21,000
 concentration and focus.

190
00:10:21,000 --> 00:10:25,770
 Of course, this is totally radical talk and most people won

191
00:10:25,770 --> 00:10:27,920
't accept it, which is why,

192
00:10:27,920 --> 00:10:30,800
 in short, the best answer is put them on the back burner,

193
00:10:30,800 --> 00:10:32,360
 as Alfred says, don't pay too

194
00:10:32,360 --> 00:10:35,560
 much attention to them.

195
00:10:35,560 --> 00:10:38,310
 It shouldn't be a sticking point where you say, "I can't

196
00:10:38,310 --> 00:10:39,680
 believe in Buddhism because

197
00:10:39,680 --> 00:10:43,000
 I can't believe that the Buddha had such a tongue."

198
00:10:43,000 --> 00:10:46,920
 Now that would be a shame.

199
00:10:46,920 --> 00:10:49,790
 I think the best thing to do would be to say, "Oh, that's

200
00:10:49,790 --> 00:10:50,760
 interesting.

201
00:10:50,760 --> 00:10:52,880
 Buddha had a tongue like that?

202
00:10:52,880 --> 00:10:53,880
 Wow.

203
00:10:53,880 --> 00:10:56,320
 Because who cares?

204
00:10:56,320 --> 00:10:59,680
 What is it about us that we have to deny such things?

205
00:10:59,680 --> 00:11:04,320
 No, I won't rest until I find the truth of this one.

206
00:11:04,320 --> 00:11:08,620
 I won't rest until I can prove that the Buddha didn't have

207
00:11:08,620 --> 00:11:10,080
 such a tongue."

208
00:11:10,080 --> 00:11:13,000
 So why not just say, "Okay, Buddha had such a tongue.

209
00:11:13,000 --> 00:11:14,560
 That's cool.

210
00:11:14,560 --> 00:11:17,080
 Can I get back to meditation now?"

211
00:11:17,080 --> 00:11:18,080
 Why not?

212
00:11:18,080 --> 00:11:23,900
 There was a teacher in Thailand who said, "Buddhism puts

213
00:11:23,900 --> 00:11:26,680
 the magic back in life."

214
00:11:26,680 --> 00:11:28,560
 I'm just quoting it off the top of my head.

215
00:11:28,560 --> 00:11:32,480
 I don't particularly hold to such a quote.

216
00:11:32,480 --> 00:11:37,960
 What she was trying to say is this exact thought.

217
00:11:37,960 --> 00:11:40,790
 I wouldn't say we should try to put the magic back into

218
00:11:40,790 --> 00:11:43,000
 life particularly, but to some extent

219
00:11:43,000 --> 00:11:48,080
 we have to let go of this obsession with skepticism and the

220
00:11:48,080 --> 00:11:51,440
 desire to disprove and to doubt things,

221
00:11:51,440 --> 00:11:54,160
 basically the desire to doubt things.

222
00:11:54,160 --> 00:11:56,560
 So it's actually kind of a useful teaching in that sense.

223
00:11:56,560 --> 00:11:59,580
 It catches you and you start doubting and then the teacher

224
00:11:59,580 --> 00:12:01,280
 can say, "Look at the doubt.

225
00:12:01,280 --> 00:12:02,640
 It's useful.

226
00:12:02,640 --> 00:12:04,720
 Get people thinking."

227
00:12:04,720 --> 00:12:06,240
 It's like a Zen koan.

228
00:12:06,240 --> 00:12:09,600
 The Buddha had a tongue that could touch his ears.

229
00:12:09,600 --> 00:12:13,440
 Think about that until you let go of your doubt.

230
00:12:13,440 --> 00:12:16,880
 Maybe you could become enlightened on it even.

231
00:12:16,880 --> 00:12:18,360
 Anyway, great.

232
00:12:18,360 --> 00:12:24,840
 I'm glad we've got some conversation going on with this.

233
00:12:24,840 --> 00:12:26,750
 We're not going to get through many questions this way

234
00:12:26,750 --> 00:12:27,760
 though, which is okay.

