1
00:00:00,000 --> 00:00:16,660
 Okay, so we're continuing on with list of one things. Lists

2
00:00:16,660 --> 00:00:17,440
 of one are good because

3
00:00:17,440 --> 00:00:21,450
 it's more about the teaching than about the list. You can

4
00:00:21,450 --> 00:00:25,240
 focus on the meaning behind

5
00:00:25,240 --> 00:00:34,120
 the teaching. It's actually in pairs most of this. So here

6
00:00:34,120 --> 00:00:35,200
 we have a group of pairs

7
00:00:35,200 --> 00:00:42,290
 where the Buddha is comparing things that are beneficial

8
00:00:42,290 --> 00:00:45,000
 and things that are harmful.

9
00:00:45,000 --> 00:00:54,100
 He gives a fairly graphic example here. He says when you...

10
00:00:54,100 --> 00:00:55,760
 I won't... the Pali is too

11
00:00:57,800 --> 00:01:09,970
 much. If you set up a straw, you set it up in the wrong way

12
00:01:09,970 --> 00:01:15,640
. You set it up sideways,

13
00:01:15,640 --> 00:01:18,830
 for example. Then when you step on it, it won't cut your

14
00:01:18,830 --> 00:01:20,640
 feet. Why is that? Because

15
00:01:20,640 --> 00:01:32,660
 you didn't set it up upright. When you set it up in the

16
00:01:32,660 --> 00:01:36,120
 wrong way. But if you set it

17
00:01:36,120 --> 00:01:41,040
 up straight, then it can pierce the hand. This is if you've

18
00:01:41,040 --> 00:01:43,080
 ever been in a barn, you

19
00:01:43,080 --> 00:01:48,630
 know how it is. Sometimes when you walk on the straw, it

20
00:01:48,630 --> 00:01:52,360
 sticks into your feet. And it

21
00:01:52,360 --> 00:02:04,520
 draws blood. So it won't pierce into your feet if it's not

22
00:02:04,520 --> 00:02:06,360
 sticking up, if it's not

23
00:02:06,360 --> 00:02:15,960
 set up in the right way. And he says in the same way, this

24
00:02:15,960 --> 00:02:19,080
 example he uses to compare

25
00:02:19,080 --> 00:02:23,520
 actually to the opposite kind of thing. So he's saying it

26
00:02:23,520 --> 00:02:25,400
 won't hurt your feet, but we

27
00:02:25,400 --> 00:02:29,420
 think that's a good thing. But there's a kind of piercing

28
00:02:29,420 --> 00:02:31,380
 that you can't do and that you

29
00:02:31,380 --> 00:02:40,100
 should do. And you can't do if you set it up in the wrong

30
00:02:40,100 --> 00:02:42,760
 way. And that is if your mind

31
00:02:42,760 --> 00:02:47,670
 is set up in the wrong way. If your mind isn't upright, if

32
00:02:47,670 --> 00:02:50,040
 your mind isn't sharp, then you

33
00:02:50,040 --> 00:02:59,810
 can say, that a person whose mind isn't set up properly,

34
00:02:59,810 --> 00:03:04,200
 with a mind that is set up in

35
00:03:04,200 --> 00:03:11,830
 the wrong way, that they should pierce through ignorance

36
00:03:11,830 --> 00:03:17,080
 and reach, or knowledge, and realize

37
00:03:17,080 --> 00:03:21,410
 nimbana for themselves. This is not possible. Why? Because

38
00:03:21,410 --> 00:03:23,160
 they didn't set their mind up

39
00:03:23,160 --> 00:03:31,810
 properly. And then he says in the opposite way, if you set

40
00:03:31,810 --> 00:03:33,960
 up the straw, kind of like

41
00:03:33,960 --> 00:03:38,270
 a booby trap, I guess, and when someone steps on it, it

42
00:03:38,270 --> 00:03:41,200
 pierces their feet, or if the straw

43
00:03:41,200 --> 00:03:45,850
 is upright. If you ever cut grass, where it comes from, we

44
00:03:45,850 --> 00:03:48,240
 have these big fields, no hay

45
00:03:48,240 --> 00:03:52,100
 fields. And when they cut them, it looks very nice to walk

46
00:03:52,100 --> 00:03:54,360
 through. So we'll walk through

47
00:03:54,360 --> 00:04:01,240
 them barefoot. And it actually does cut your feet. So in

48
00:04:01,240 --> 00:04:02,160
 the Buddha's time, they would

49
00:04:02,160 --> 00:04:07,600
 be walking through these fields. Very graphic. Now for us,

50
00:04:07,600 --> 00:04:10,280
 it seems kind of silly. We don't

51
00:04:10,280 --> 00:04:13,170
 have any idea, any concept of this. But if you grew up in

52
00:04:13,170 --> 00:04:14,680
 the countryside, when they

53
00:04:14,680 --> 00:04:20,190
 cut the hay, when they cut the hay or the straw, and you

54
00:04:20,190 --> 00:04:23,680
 walk through it, very painful.

55
00:04:23,680 --> 00:04:28,000
 Even when they cut the lawn sometimes, after many days of

56
00:04:28,000 --> 00:04:30,320
 not cutting it, and the grass

57
00:04:30,320 --> 00:04:35,290
 grows really high, and then you cut it, it's very painful

58
00:04:35,290 --> 00:04:37,640
 on the feet. And so the monks

59
00:04:37,640 --> 00:04:41,370
 will be walking through these fields daily, after people

60
00:04:41,370 --> 00:04:44,600
 cut them. So just in the same

61
00:04:44,600 --> 00:04:49,060
 way as it's piercing your feet, this is how the mind pier

62
00:04:49,060 --> 00:04:51,520
ces through ignorance. If you

63
00:04:51,520 --> 00:04:55,520
 set the mind up straight, it will be able to pierce through

64
00:04:55,520 --> 00:04:58,000
 ignorance and realize nascence.

65
00:04:58,000 --> 00:05:05,800
 You'll be able to draw knowledge, because our mind is cloud

66
00:05:05,800 --> 00:05:09,640
ed, is covered by this layer

67
00:05:09,640 --> 00:05:15,920
 of ignorance, which stops us from seeing things clearly.

68
00:05:15,920 --> 00:05:18,320
 And until you penetrate that, you

69
00:05:18,320 --> 00:05:24,780
 can see in the meditation as you practice, you pierce

70
00:05:24,780 --> 00:05:29,040
 through, and you understand what's

71
00:05:29,040 --> 00:05:34,340
 going on in your mind. You understand why you're feeling,

72
00:05:34,340 --> 00:05:36,680
 why you feel suffering, and

73
00:05:36,680 --> 00:05:42,060
 why you have attachment, and so on. Why you have stress,

74
00:05:42,060 --> 00:05:44,920
 and why you have suffering. So

75
00:05:44,920 --> 00:05:50,280
 you realize the truth, you realize what's really going on

76
00:05:50,280 --> 00:05:53,840
 inside. And you see how close

77
00:05:53,840 --> 00:05:56,990
 to you this knowledge is. It's not something far away, it's

78
00:05:56,990 --> 00:05:58,520
 not something you have to look

79
00:05:58,520 --> 00:06:04,600
 up in a book. It's actually something that's quite obvious.

80
00:06:04,600 --> 00:06:10,080
 And so you start to think how

81
00:06:10,080 --> 00:06:13,200
 amazing it is that you weren't able to see this. But the

82
00:06:13,200 --> 00:06:14,440
 reason you can't see it is because

83
00:06:14,440 --> 00:06:18,000
 the mind is set up in the wrong way. The mind isn't sharp

84
00:06:18,000 --> 00:06:20,040
 enough to penetrate through the

85
00:06:20,040 --> 00:06:34,400
 ego. And so it chases after delusion, it chases after

86
00:06:34,400 --> 00:06:37,840
 illusion.

87
00:06:37,840 --> 00:06:44,780
 And he goes on to say that in fact, a mind that is set up

88
00:06:44,780 --> 00:06:48,640
 wrongly is so dangerous that

89
00:06:48,640 --> 00:06:52,610
 it's the mind that is set up wrongly that leads the person

90
00:06:52,610 --> 00:06:54,680
 to hell, and the mind that's

91
00:06:54,680 --> 00:07:01,100
 set up properly that leads the person to heaven. The mind

92
00:07:01,100 --> 00:07:06,640
 is so central in our future, in our

93
00:07:06,640 --> 00:07:11,460
 happiness, in our suffering. This is what we come to

94
00:07:11,460 --> 00:07:13,400
 realize in the meditation that actually

95
00:07:13,400 --> 00:07:16,290
 it's not the body that's causing us suffering. It's not the

96
00:07:16,290 --> 00:07:17,980
 things around us that are causing

97
00:07:17,980 --> 00:07:30,780
 us suffering. We have a real change of the way we look at

98
00:07:30,780 --> 00:07:37,660
 things. The idea that the mosquitoes

99
00:07:37,660 --> 00:07:42,540
 can cause us suffering, or the leeches, or spiders, or

100
00:07:42,540 --> 00:07:46,680
 snakes, or so on. They're so strong

101
00:07:46,680 --> 00:07:52,130
 with us. The idea that other people can cause us suffering

102
00:07:52,130 --> 00:07:54,940
 is so clear in our minds as being

103
00:07:54,940 --> 00:08:02,420
 the truth. It's quite a change for us to see that actually

104
00:08:02,420 --> 00:08:04,220
 it's our own minds that's causing

105
00:08:04,220 --> 00:08:08,120
 us suffering. All of the pain that we feel in the body and

106
00:08:08,120 --> 00:08:09,960
 all of the suffering that

107
00:08:09,960 --> 00:08:13,440
 comes from the world around us is only really the cause of

108
00:08:13,440 --> 00:08:14,900
 suffering when we take it up

109
00:08:14,900 --> 00:08:20,470
 in the mind, take it up in the wrong way. Another place the

110
00:08:20,470 --> 00:08:22,180
 Buddha says when you hold

111
00:08:22,180 --> 00:08:26,620
 grass in your hand, by the blades, for example, that it can

112
00:08:26,620 --> 00:08:28,900
 cut the hand, but if you grab

113
00:08:28,900 --> 00:08:35,230
 it by the shaft, if you grab the grass in the right way,

114
00:08:35,230 --> 00:08:39,100
 then it doesn't cut your hand.

115
00:08:39,100 --> 00:08:43,710
 When you grasp an experience as it is, an experience even

116
00:08:43,710 --> 00:08:46,420
 right now sitting here, you can grasp

117
00:08:46,420 --> 00:08:49,330
 it as it is, it can cause you suffering, even the hard

118
00:08:49,330 --> 00:08:51,300
 floor, even the mosquitoes and so

119
00:08:51,300 --> 00:08:57,380
 on. Of course the mosquitoes, you have to be careful as

120
00:08:57,380 --> 00:09:00,100
 they carry diseases. Not to

121
00:09:00,100 --> 00:09:02,700
 say that you have to put up, but you have to grasp it

122
00:09:02,700 --> 00:09:04,500
 correctly, so that even when you're

123
00:09:04,500 --> 00:09:11,500
 swatting the mosquitoes, you're doing it in mind for me.

124
00:09:11,500 --> 00:09:12,620
 Before people practiced, if they

125
00:09:12,620 --> 00:09:18,100
 were not learned in the precepts or in the importance of

126
00:09:18,100 --> 00:09:21,380
 compassion and kindness, they

127
00:09:21,380 --> 00:09:25,060
 would be killing the mosquitoes. So this is often a reflex.

128
00:09:25,060 --> 00:09:26,380
 You want to kill the mosquitoes.

129
00:09:26,380 --> 00:09:29,960
 If you're not careful, you'll go back and just quickly kill

130
00:09:29,960 --> 00:09:33,100
 the mosquitoes without thinking.

131
00:09:33,100 --> 00:09:40,380
 It's because the way the mind is set up, and this mind can

132
00:09:40,380 --> 00:09:43,940
 lead us to hell. The mind can

133
00:09:43,940 --> 00:09:54,140
 lead us to heaven. One more thing he says that I think is

134
00:09:54,140 --> 00:09:56,580
 useful as he talks about the

135
00:09:56,580 --> 00:10:06,240
 clarity of the mind. He says just like a lake, just like a

136
00:10:06,240 --> 00:10:10,620
 pond that is stirred up. Suppose

137
00:10:10,620 --> 00:10:17,500
 you have a pond, you take a stick and you stir it up or

138
00:10:17,500 --> 00:10:21,620
 when you walk through it, and

139
00:10:21,620 --> 00:10:25,820
 the Buddha says it becomes muddy. It's not just stirred up,

140
00:10:25,820 --> 00:10:27,260
 but it's muddy. Like the

141
00:10:27,260 --> 00:10:32,480
 puddles after the rain, they're mud. When you walk through

142
00:10:32,480 --> 00:10:34,980
 them, they're mud. When you

143
00:10:34,980 --> 00:10:40,120
 leave them alone for a while, they become clear and you can

144
00:10:40,120 --> 00:10:41,940
 see inside of them. So he

145
00:10:41,940 --> 00:10:45,190
 says in this pool, when it's all muddied up, you can't see

146
00:10:45,190 --> 00:10:47,580
 anything. You can't see the

147
00:10:47,580 --> 00:10:50,930
 fishes, you can't see. They're not a puddle anymore, but a

148
00:10:50,930 --> 00:10:52,500
 big lake or so on. You can't

149
00:10:52,500 --> 00:10:58,090
 see the fish, you can't see the animals, the shells and so

150
00:10:58,090 --> 00:11:02,340
 on. But when the pond is settled

151
00:11:02,340 --> 00:11:07,710
 and the water is clear, then you can see what's in it. So

152
00:11:07,710 --> 00:11:14,620
 we have to think of this. This is

153
00:11:14,620 --> 00:11:23,130
 a way of the mind as well. We're coming here to clear our

154
00:11:23,130 --> 00:11:27,420
 minds, to make our minds clear,

155
00:11:27,420 --> 00:11:33,360
 to make our minds calm. But it's a calm in a way of clarity

156
00:11:33,360 --> 00:11:35,100
. The important thing is that

157
00:11:35,100 --> 00:11:40,780
 you can see clearly. When your mind is calmed, it should be

158
00:11:40,780 --> 00:11:43,660
 calm based on the experience.

159
00:11:43,660 --> 00:11:46,280
 So another way is to just avoid the pond and go somewhere

160
00:11:46,280 --> 00:11:47,900
 else. You can't see anything

161
00:11:47,900 --> 00:11:50,880
 here, so go look somewhere else. In meditation, this is

162
00:11:50,880 --> 00:11:52,980
 often the case. People want the calm,

163
00:11:52,980 --> 00:11:55,930
 but they don't care so much about the clarity. Because

164
00:11:55,930 --> 00:11:58,100
 clarity is quite difficult. You have

165
00:11:58,100 --> 00:12:02,860
 to wait and you have to focus on the mud and actually wait

166
00:12:02,860 --> 00:12:05,460
 for the pond to become clear.

167
00:12:05,460 --> 00:12:09,110
 If you just want calm, you can avoid it and run away and

168
00:12:09,110 --> 00:12:11,180
 feel peace and calm thinking

169
00:12:11,180 --> 00:12:17,070
 about something else. This is why people, we choose always

170
00:12:17,070 --> 00:12:18,620
 the easy way. Sometimes people

171
00:12:18,620 --> 00:12:26,730
 take drugs or medication. Tarindu, for example, he's become

172
00:12:26,730 --> 00:12:30,420
 addicted to Tylenol because he

173
00:12:30,420 --> 00:12:33,430
 has some problems in his mind. His mind is not settled and

174
00:12:33,430 --> 00:12:35,780
 so he gets headaches. So he

175
00:12:35,780 --> 00:12:41,510
 said to me, he said, "Tylenol?" "Good, very good." And I

176
00:12:41,510 --> 00:12:44,260
 said to him, "This, what you're

177
00:12:44,260 --> 00:12:47,720
 experiencing now, this is Tylenol. Is this good?" "No, this

178
00:12:47,720 --> 00:12:49,140
 is not good." "Well, this

179
00:12:49,140 --> 00:12:53,230
 is from Tylenol. This is what happened." "This is the easy

180
00:12:53,230 --> 00:12:54,860
 way." "This is what the

181
00:12:54,860 --> 00:13:04,220
 easy way brings you." So the calm that we're looking for is

182
00:13:04,220 --> 00:13:05,740
 a calm with clarity. That's

183
00:13:05,740 --> 00:13:10,110
 how we get together. You can't just want calm and peace

184
00:13:10,110 --> 00:13:12,980
 otherwise there's many ways to attain

185
00:13:12,980 --> 00:13:18,350
 that but it becomes associated with clinging. You become

186
00:13:18,350 --> 00:13:20,980
 partial to that state. The idea

187
00:13:20,980 --> 00:13:26,030
 is to become calm in regards to the problems, to see the

188
00:13:26,030 --> 00:13:29,380
 problems clearly as they are. When

189
00:13:29,380 --> 00:13:32,570
 you have a problem with a person, when you have a problem

190
00:13:32,570 --> 00:13:34,500
 with a place, you have a problem

191
00:13:34,500 --> 00:13:38,440
 with a thing, you have problems with yourself, with your

192
00:13:38,440 --> 00:13:40,780
 own body, with your own mind, to

193
00:13:40,780 --> 00:13:46,140
 become calm in regards to them, to understand them, what is

194
00:13:46,140 --> 00:13:48,740
 going on now, to see them for

195
00:13:48,740 --> 00:13:54,150
 what they are and to live with them. The Buddha said, "When

196
00:13:54,150 --> 00:13:56,280
 you can calm your mind in regards

197
00:13:56,280 --> 00:13:59,660
 to reality, then you can see clearly, you can see what's

198
00:13:59,660 --> 00:14:01,380
 going on in your mind and how

199
00:14:01,380 --> 00:14:10,240
 the mind works from moment to moment." And then you can

200
00:14:10,240 --> 00:14:16,460
 penetrate, in this way you penetrate

201
00:14:16,460 --> 00:14:19,790
 through the ignorance. You can think of the mud as being

202
00:14:19,790 --> 00:14:21,780
 like the ignorance that comes

203
00:14:21,780 --> 00:14:26,030
 from stirring up the mind. All of the ignorance that we

204
00:14:26,030 --> 00:14:27,500
 have or all of the delusion, let's

205
00:14:27,500 --> 00:14:31,230
 say the delusion that we have, is because our mind is

206
00:14:31,230 --> 00:14:33,620
 constantly being stirred up, constantly

207
00:14:33,620 --> 00:14:38,740
 being perturbed by external and internal factors. Ex

208
00:14:38,740 --> 00:14:41,620
ternally we have other people and places

209
00:14:41,620 --> 00:14:44,960
 and things stirring our minds up. And internally we have

210
00:14:44,960 --> 00:14:47,140
 the defilements stirring our minds

211
00:14:47,140 --> 00:14:52,560
 up. This is why I talked about Mara. Mara can only be a

212
00:14:52,560 --> 00:14:57,100
 problem for you if you fall for

213
00:14:57,100 --> 00:15:05,300
 his trap. Mara is like the fisherman, and he's got his hook

214
00:15:05,300 --> 00:15:08,600
. But the only reason a fish

215
00:15:08,600 --> 00:15:11,610
 gets caught on the hook is because it takes the bait. So

216
00:15:11,610 --> 00:15:14,180
 the Buddha talked about Mara's

217
00:15:14,180 --> 00:15:20,000
 bait. He attracts us in so many ways. And you come to

218
00:15:20,000 --> 00:15:23,620
 realize this in meditation, you

219
00:15:23,620 --> 00:15:27,390
 realize nothing can cause me suffering. There's nothing

220
00:15:27,390 --> 00:15:29,540
 that can really hurt me. I'm doing

221
00:15:29,540 --> 00:15:38,200
 this to myself. Going again and again, chasing again and

222
00:15:38,200 --> 00:15:43,140
 again after this bait and getting

223
00:15:43,140 --> 00:15:53,650
 caught by the hook. So our practice is to see through this

224
00:15:53,650 --> 00:15:57,780
 and to be clear in our minds

225
00:15:57,780 --> 00:16:04,400
 in regards to Mara's bait, in regards to suffering and what

226
00:16:04,400 --> 00:16:07,860
 is the cause of suffering. To understand

227
00:16:07,860 --> 00:16:16,440
 clearly and to be fully alert and aware of the experience

228
00:16:16,440 --> 00:16:20,580
 in front of us. So just a little

229
00:16:20,580 --> 00:16:26,640
 bit more food for thought. We can think about what we're

230
00:16:26,640 --> 00:16:30,180
 doing as being the correct grasping

231
00:16:30,180 --> 00:16:34,610
 of something, some sharp object, and piercing through the

232
00:16:34,610 --> 00:16:36,700
 skin of ignorance and drawing

233
00:16:36,700 --> 00:16:42,530
 through the blood of wisdom. It's a very strong simile, the

234
00:16:42,530 --> 00:16:45,740
 idea of piercing and drawing blood

235
00:16:45,740 --> 00:16:48,880
 and being related to piercing through ignorance. But again,

236
00:16:48,880 --> 00:16:50,900
 it was something that would have

237
00:16:50,900 --> 00:16:54,420
 been quite visceral for them stepping on these straws,

238
00:16:54,420 --> 00:16:56,700
 walking through the fields for alms

239
00:16:56,700 --> 00:17:03,380
 round. Then we have the muddied water. Our mind's like mudd

240
00:17:03,380 --> 00:17:04,460
ied water, and we're trying

241
00:17:04,460 --> 00:17:08,420
 to clear them. As the Buddha said, this is what leads to

242
00:17:08,420 --> 00:17:10,620
 happiness here and happiness

243
00:17:10,620 --> 00:17:15,530
 in the future. It's the muddied water and the cloud of

244
00:17:15,530 --> 00:17:18,300
 delusion and ignorance that can

245
00:17:18,300 --> 00:17:22,390
 send us to hell, even here and now can cause us to do and

246
00:17:22,390 --> 00:17:24,780
 say and think things that cause

247
00:17:24,780 --> 00:17:33,310
 suffering for us for a long, long time. Some things to

248
00:17:33,310 --> 00:17:38,180
 think about. More on the mind. When

249
00:17:38,180 --> 00:17:40,300
 you talk about one thing, well, what's the most important

250
00:17:40,300 --> 00:17:41,460
 one thing? It's definitely

251
00:17:41,460 --> 00:17:46,310
 the mind. This is where we start in Angutra and Kaya. But

252
00:17:46,310 --> 00:17:47,460
 the mind is the most important

253
00:17:47,460 --> 00:17:54,400
 in our meditation as well, as I've by now said far too many

254
00:17:54,400 --> 00:17:57,580
 times. It should be well

255
00:17:57,580 --> 00:18:03,100
 established in your mind, in your thoughts, that the mind

256
00:18:03,100 --> 00:18:05,820
 is what we're focusing on.

257
00:18:05,820 --> 00:18:08,240
 One thing that's interesting that I was saying today to a

258
00:18:08,240 --> 00:18:13,860
 meditator on the internet, he was

259
00:18:13,860 --> 00:18:16,480
 saying it's nice to be focusing on the mind as well because

260
00:18:16,480 --> 00:18:17,860
 normally he only focuses on

261
00:18:17,860 --> 00:18:23,100
 the breath. Now I was explaining him to focus on the mind

262
00:18:23,100 --> 00:18:25,100
 as well and the hindrances, the

263
00:18:25,100 --> 00:18:30,270
 emotions in the mind. He said it's good to notice them. I

264
00:18:30,270 --> 00:18:31,560
 said, but it's okay really

265
00:18:31,560 --> 00:18:38,450
 to focus on the breath because he's doing breath meditation

266
00:18:38,450 --> 00:18:41,380
. When you go into the forest,

267
00:18:41,380 --> 00:18:44,630
 the hunter goes into the forest looking for a deer. He

268
00:18:44,630 --> 00:18:46,980
 doesn't go running around the forest

269
00:18:46,980 --> 00:18:50,460
 looking for the deer. He goes and sits by the waterhole

270
00:18:50,460 --> 00:18:52,380
 because he knows that's where

271
00:18:52,380 --> 00:18:58,240
 the deer have to come. In the same way when we practice

272
00:18:58,240 --> 00:19:00,300
 focusing on the stomach or focusing

273
00:19:00,300 --> 00:19:02,750
 on the breath, even though there's nothing special about

274
00:19:02,750 --> 00:19:04,060
 these things, we're going to

275
00:19:04,060 --> 00:19:07,260
 see how the mind works. So when we talk about the mind

276
00:19:07,260 --> 00:19:08,740
 being the most important thing, it

277
00:19:08,740 --> 00:19:11,510
 doesn't mean we have to go looking where's my mind, where's

278
00:19:11,510 --> 00:19:13,900
 my mind and try to find it.

279
00:19:13,900 --> 00:19:17,940
 We still focus on the body actually. Even though our intent

280
00:19:17,940 --> 00:19:19,660
 is to change the mind, we

281
00:19:19,660 --> 00:19:22,790
 focus on the body because when we focus on the body, that

282
00:19:22,790 --> 00:19:24,420
 means our mind is on the body

283
00:19:24,420 --> 00:19:33,980
 and it's this indirect observation of ourselves really.

284
00:19:33,980 --> 00:19:35,780
 Watching how we work, give us some

285
00:19:35,780 --> 00:19:40,590
 task like they do in studies and sociology, maybe they do

286
00:19:40,590 --> 00:19:42,980
 it, psychology, I know they

287
00:19:42,980 --> 00:19:46,770
 do it, they put you in some situation and see how you'll

288
00:19:46,770 --> 00:19:49,300
 react to it, see how you'll interact

289
00:19:49,300 --> 00:19:53,490
 with the situation. So we're going to see how we interact

290
00:19:53,490 --> 00:19:55,620
 with this very simple object

291
00:19:55,620 --> 00:19:58,980
 and we find that it's quite interesting how we interact,

292
00:19:58,980 --> 00:20:00,460
 how the mind works. Walking back

293
00:20:00,460 --> 00:20:03,170
 and forth we start to wonder why am I walking back and

294
00:20:03,170 --> 00:20:05,300
 forth. Watching the breath we start

295
00:20:05,300 --> 00:20:09,660
 to try to control the breath, make it always smooth and

296
00:20:09,660 --> 00:20:12,140
 make it always the same length,

297
00:20:12,140 --> 00:20:16,450
 make it always comfortable. We get fed up and frustrated

298
00:20:16,450 --> 00:20:17,820
 when it's not the way we want

299
00:20:17,820 --> 00:20:22,950
 and become attached when it is the way we want and then go

300
00:20:22,950 --> 00:20:25,220
 up and down, good and bad

301
00:20:25,220 --> 00:20:30,770
 and so on. So even though we're focusing on the mind, we

302
00:20:30,770 --> 00:20:32,220
 use the body as our tool to see

303
00:20:32,220 --> 00:20:38,140
 how the mind works. It's important to understand that this

304
00:20:38,140 --> 00:20:39,740
 is the meaning of all of these emotions

305
00:20:39,740 --> 00:20:42,840
 and thoughts that come up when we practice. When you

306
00:20:42,840 --> 00:20:44,420
 practice you start to think it's

307
00:20:44,420 --> 00:20:47,170
 quite difficult and there's a lot of problems because this

308
00:20:47,170 --> 00:20:49,660
 isn't right and that isn't right.

309
00:20:49,660 --> 00:20:51,710
 All of that, what's going on in the mind is what we're

310
00:20:51,710 --> 00:20:53,660
 looking for. We're trying to understand

311
00:20:53,660 --> 00:20:58,850
 this and we're trying to see what is causing us the

312
00:20:58,850 --> 00:21:03,100
 suffering, which states of mind which

313
00:21:03,100 --> 00:21:09,060
 reactions are causing trouble for us. So we have to realize

314
00:21:09,060 --> 00:21:10,020
 that it's not actually the

315
00:21:10,020 --> 00:21:13,240
 body that's causing the problem, it's the mind that's

316
00:21:13,240 --> 00:21:15,340
 causing the problem. To realize

317
00:21:15,340 --> 00:21:19,860
 that the body, the changing in the body, sometimes smooth,

318
00:21:19,860 --> 00:21:23,100
 sometimes rough and so on, is just

319
00:21:23,100 --> 00:21:28,030
 the way it is. It's nature. There's nothing good or bad

320
00:21:28,030 --> 00:21:31,180
 about any physical experience.

321
00:21:31,180 --> 00:21:34,390
 It's only when the mind picks it up and says, "This is me,

322
00:21:34,390 --> 00:21:36,060
 this is mine, this is good, this

323
00:21:36,060 --> 00:21:42,600
 is bad," that there becomes a problem. So mind is most

324
00:21:42,600 --> 00:21:44,940
 important and what we do is use

325
00:21:44,940 --> 00:21:48,780
 the body to come to understand the mind. This is our

326
00:21:48,780 --> 00:21:52,660
 practice. So that's enough talking,

327
00:21:52,660 --> 00:21:58,210
 now we can go on to actual practice. I guess we can go up

328
00:21:58,210 --> 00:22:01,580
 to the roof, no, and then maybe,

329
00:22:01,580 --> 00:22:03,020
 as long as it's not raining we can meet up there.

