1
00:00:00,000 --> 00:00:07,950
 So today we continue our study of the Dhammapada on with

2
00:00:07,950 --> 00:00:14,000
 verse number 24 which is as follows.

3
00:00:16,000 --> 00:00:21,000
 "Utanena pamaadena sanyameena dameena ca"

4
00:00:21,000 --> 00:00:26,000
 "Dipankayi rata medhaavi"

5
00:00:26,000 --> 00:00:31,000
 "yangogonabhikirate"

6
00:00:31,000 --> 00:00:42,000
 Which means through striving and heedfulness.

7
00:00:42,000 --> 00:00:50,000
 "Restraint and training"

8
00:00:50,000 --> 00:00:55,000
 "When a wise man should make an island for themselves"

9
00:00:55,000 --> 00:00:58,000
 "Dipankayi rata medhaavi"

10
00:00:58,000 --> 00:01:01,000
 "yangogonabhikirate"

11
00:01:01,000 --> 00:01:07,220
 That the flood will not overcome, will not wash away or

12
00:01:07,220 --> 00:01:11,000
 will not cover over.

13
00:01:11,000 --> 00:01:14,000
 Will not be carried away by the flood.

14
00:01:14,000 --> 00:01:24,960
 So this was told in regards to a monk who became an arahant

15
00:01:24,960 --> 00:01:25,000
.

16
00:01:25,000 --> 00:01:35,000
 After failing to learn a single stanza.

17
00:01:35,000 --> 00:01:40,000
 So how the story goes is he and his brother became monks.

18
00:01:40,000 --> 00:01:44,000
 They had been raised by their grandparents.

19
00:01:44,000 --> 00:01:50,370
 Their mother had run away with a servant and had lost all

20
00:01:50,370 --> 00:01:55,000
 of her repute with the family.

21
00:01:55,000 --> 00:02:00,000
 So in the end she had no money and nowhere to live.

22
00:02:00,000 --> 00:02:02,300
 So she had to send the children back to live with their

23
00:02:02,300 --> 00:02:03,000
 grandparents.

24
00:02:03,000 --> 00:02:08,000
 So the grandfather was a disciple of the Buddha.

25
00:02:08,000 --> 00:02:10,680
 He would take his grandchildren to listen to the Buddha

26
00:02:10,680 --> 00:02:11,000
 teach.

27
00:02:11,000 --> 00:02:18,000
 So the elder son Mahapantaka became a monk.

28
00:02:18,000 --> 00:02:21,730
 Through his practice the Buddha's teaching became an arah

29
00:02:21,730 --> 00:02:22,000
ant.

30
00:02:22,000 --> 00:02:24,900
 He thought to himself, "Wouldn't it be great if I could

31
00:02:24,900 --> 00:02:26,000
 give this to my younger brother?"

32
00:02:26,000 --> 00:02:33,350
 So he convinced his grandfather to let his brother become a

33
00:02:33,350 --> 00:02:34,000
 monk.

34
00:02:34,000 --> 00:02:39,000
 He taught his younger brother the stanza.

35
00:02:39,000 --> 00:02:42,190
 He tried to teach him the stanza that was basically praise

36
00:02:42,190 --> 00:02:44,000
 of the Buddha.

37
00:02:44,000 --> 00:02:49,000
 For four months every time he would learn the first line,

38
00:02:49,000 --> 00:02:52,320
 remember the first line, then he would go on to memorize

39
00:02:52,320 --> 00:02:53,000
 the second line.

40
00:02:53,000 --> 00:02:55,340
 As soon as he memorized the second line he'd forgotten the

41
00:02:55,340 --> 00:02:56,000
 first line.

42
00:02:56,000 --> 00:03:00,280
 So four lines stanza, just like the one that I gave was a

43
00:03:00,280 --> 00:03:03,000
 two line stanza.

44
00:03:03,000 --> 00:03:06,220
 So like just twice as much as that. For four months he

45
00:03:06,220 --> 00:03:08,000
 couldn't memorize it.

46
00:03:08,000 --> 00:03:14,000
 The story goes that in the past, in a past life, he had

47
00:03:14,000 --> 00:03:16,000
 been a good monk,

48
00:03:16,000 --> 00:03:19,210
 but he had always looked down upon those monks and just

49
00:03:19,210 --> 00:03:20,000
 disparaged those monks

50
00:03:20,000 --> 00:03:27,500
 who couldn't memorize things and laugh about them and brag

51
00:03:27,500 --> 00:03:29,000
 about it.

52
00:03:29,000 --> 00:03:39,000
 So as a result he became dumb in his next life as a monk.

53
00:03:39,000 --> 00:03:42,150
 He became quite discouraged and his older brother figured

54
00:03:42,150 --> 00:03:44,000
 that this monk was a dullard,

55
00:03:44,000 --> 00:03:45,000
 was useless.

56
00:03:45,000 --> 00:03:49,000
 So he said, "Look, for four months you've gained nothing."

57
00:03:49,000 --> 00:03:54,000
 He said, "You're better off not to be a monk."

58
00:03:54,000 --> 00:03:55,710
 So the younger brother was quite dejected, but he didn't

59
00:03:55,710 --> 00:03:57,000
 take his brother seriously

60
00:03:57,000 --> 00:04:01,160
 until he heard that his brother was responsible for

61
00:04:01,160 --> 00:04:02,000
 organizing meals.

62
00:04:02,000 --> 00:04:06,610
 So when they went to people's houses the older brother

63
00:04:06,610 --> 00:04:10,000
 would organize monks to go.

64
00:04:10,000 --> 00:04:14,220
 And so one day this lay person, I think Anatta Pindika,

65
00:04:14,220 --> 00:04:17,000
 came to invite them to his house

66
00:04:17,000 --> 00:04:19,880
 and he said to the older brother, "Please, how many monks

67
00:04:19,880 --> 00:04:21,000
 do you have?

68
00:04:21,000 --> 00:04:25,240
 Bring all the monks together with the Buddha for tomorrow's

69
00:04:25,240 --> 00:04:26,000
 meal."

70
00:04:26,000 --> 00:04:29,300
 And the older brother said, "Well, my younger brother is

71
00:04:29,300 --> 00:04:30,000
 not a real monk,

72
00:04:30,000 --> 00:04:34,000
 so I'll accept the invitation for everybody but him."

73
00:04:34,000 --> 00:04:37,000
 And the younger brother was sitting there and he heard this

74
00:04:37,000 --> 00:04:39,000
 and at that point he became quite dejected.

75
00:04:39,000 --> 00:04:42,000
 He said, "My brother says I'm not a real monk."

76
00:04:42,000 --> 00:04:46,000
 And so he decided that he better quit, he better disrobe.

77
00:04:46,000 --> 00:04:48,000
 But the truth is he was happy being a monk.

78
00:04:48,000 --> 00:04:52,270
 He didn't have a problem, he just, he and his brother had

79
00:04:52,270 --> 00:04:55,000
 this idea that somehow you had to memorize.

80
00:04:55,000 --> 00:04:58,000
 It may also have been that his older brother was thinking

81
00:04:58,000 --> 00:05:00,000
 that this would make him

82
00:05:00,000 --> 00:05:02,180
 go to see the Buddha and the Buddha would give him a

83
00:05:02,180 --> 00:05:05,000
 meditation object because that's what happened.

84
00:05:05,000 --> 00:05:08,180
 The Buddha found out about this and saw him walking around

85
00:05:08,180 --> 00:05:13,000
 dejected or about to leave.

86
00:05:13,000 --> 00:05:16,000
 And he went up to see him and he said, "What's wrong?"

87
00:05:16,000 --> 00:05:18,200
 And he said, "Oh, I can't memorize even a single stanza and

88
00:05:18,200 --> 00:05:20,000
 my brother says I'm useless

89
00:05:20,000 --> 00:05:24,000
 and I should leave the, leave and disrobe."

90
00:05:24,000 --> 00:05:27,000
 And the Buddha said, "That's not what makes a real monk."

91
00:05:27,000 --> 00:05:30,000
 He said, "You don't become a monk just for memorizing."

92
00:05:30,000 --> 00:05:34,000
 He said, "Here, let me help you."

93
00:05:34,000 --> 00:05:38,110
 And he took a cloth, he took this white cloth, clean, pure

94
00:05:38,110 --> 00:05:40,000
 white cloth and he gave it over to him.

95
00:05:40,000 --> 00:05:44,000
 And he said, "Go stand in the sun, hold this white cloth

96
00:05:44,000 --> 00:05:48,000
 and say to yourself, and rub it,

97
00:05:48,000 --> 00:05:53,480
 rub it between your hand and say, 'Rajjul Haranam, Rajjul

98
00:05:53,480 --> 00:05:56,000
 Haranam,' which means taking out the dirt,

99
00:05:56,000 --> 00:06:00,000
 rubbing out the dirt."

100
00:06:00,000 --> 00:06:04,180
 And he sent him off to practice this meditation, so

101
00:06:04,180 --> 00:06:05,000
 repeating to himself,

102
00:06:05,000 --> 00:06:09,000
 'Rajjul Haranam, taking out the dirt, taking out the dirt.'

103
00:06:09,000 --> 00:06:17,000
 And then the Buddha went off to accept this invitation.

104
00:06:17,000 --> 00:06:20,750
 Now, what happened was he went off into the sun and he's

105
00:06:20,750 --> 00:06:23,000
 standing there and of course the sun comes up

106
00:06:23,000 --> 00:06:26,030
 and it's getting hot and it's sweating and as his hands

107
00:06:26,030 --> 00:06:29,000
 start to sweat, the cloth becomes soiled,

108
00:06:29,000 --> 00:06:34,000
 becomes impure, loses its white color.

109
00:06:34,000 --> 00:06:39,280
 And he looks at this and he starts to become turned off, of

110
00:06:39,280 --> 00:06:41,000
 course, because it's a beautiful white cloth

111
00:06:41,000 --> 00:06:43,940
 and suddenly it starts to get ugly and so he's standing

112
00:06:43,940 --> 00:06:44,000
 there saying,

113
00:06:44,000 --> 00:06:47,090
 'Rajjul Haranam, taking out the dirt, taking out the dirt,'

114
00:06:47,090 --> 00:06:50,000
 and here's this cloth getting dirty right before his eyes.

115
00:06:50,000 --> 00:06:55,590
 And as a result, this starts to open up his mind and his

116
00:06:55,590 --> 00:07:02,160
 attachment to the cloth and to the idea of purity starts to

117
00:07:02,160 --> 00:07:03,000
 be loosened.

118
00:07:03,000 --> 00:07:07,320
 So his mind starts to calm down as a result. The cravings

119
00:07:07,320 --> 00:07:10,000
 in his mind start to go away.

120
00:07:10,000 --> 00:07:14,340
 And the Buddha finds out about this and the Buddha sends

121
00:07:14,340 --> 00:07:17,000
 him this telepathic projection.

122
00:07:17,000 --> 00:07:21,490
 So suddenly he sees the Buddha and the Buddha says, 'Chulap

123
00:07:21,490 --> 00:07:26,000
antaka, it is Raga, Raga Rajjul.'

124
00:07:26,000 --> 00:07:31,000
 He says, this is a play on words, 'Rago, Rago Rajjul.'

125
00:07:31,000 --> 00:07:35,070
 The dirt he's talking about is lust. Lust is such dirt.

126
00:07:35,070 --> 00:07:37,000
 Lust is another word for dirt.

127
00:07:37,000 --> 00:07:40,570
 And he said, by understanding this, one should take out the

128
00:07:40,570 --> 00:07:43,000
 dirt, take out the dirt that is lust.

129
00:07:43,000 --> 00:07:48,970
 And then he says, 'Doso Rajjul, Dosa, anger is dirt and m

130
00:07:48,970 --> 00:07:51,000
oha is dirt.'

131
00:07:51,000 --> 00:07:58,070
 So these three things. So he helps him to look and then so,

132
00:07:58,070 --> 00:08:00,100
 'Tulapantaka looks inside and he becomes an Arahan by

133
00:08:00,100 --> 00:08:02,000
 listening to this teaching of the Buddha.

134
00:08:02,000 --> 00:08:05,550
 The Buddha said, 'It's not this dirt on the cloth that we

135
00:08:05,550 --> 00:08:07,000
're talking about.

136
00:08:07,000 --> 00:08:09,930
 The dirt that we want to take out is our cravings, our

137
00:08:09,930 --> 00:08:11,000
 attachments.

138
00:08:11,000 --> 00:08:13,880
 So attachment to the beauty of the cloth, attachment to the

139
00:08:13,880 --> 00:08:17,760
 beauty of the body, it's also helping to see the impurity

140
00:08:17,760 --> 00:08:19,000
 in the body.

141
00:08:19,000 --> 00:08:23,000
 And so as a result of seeing that, he became an Arahan.

142
00:08:23,000 --> 00:08:27,750
 Now the Buddha was off at this invitation and Anatta Pind

143
00:08:27,750 --> 00:08:30,000
ika came to offer the food.

144
00:08:30,000 --> 00:08:34,530
 And he came over and he comes to offer this, they have this

145
00:08:34,530 --> 00:08:39,000
 ceremonial water that they pour when they give a gift.

146
00:08:39,000 --> 00:08:43,370
 But the Buddha covered his bow with his hand and said, 'An

147
00:08:43,370 --> 00:08:49,840
atta Pindika, the Sangha is not complete. Not everyone is

148
00:08:49,840 --> 00:08:51,000
 here.'

149
00:08:51,000 --> 00:08:55,600
 And Anatta Pindika looked at Mahapantika and said, 'Are all

150
00:08:55,600 --> 00:08:57,000
 the monks here?'

151
00:08:57,000 --> 00:09:00,810
 And the Buddha said, 'There's still a monk left in the

152
00:09:00,810 --> 00:09:02,000
 monastery.'

153
00:09:02,000 --> 00:09:06,430
 And so Anatta Pindika sent a man to the monastery to find

154
00:09:06,430 --> 00:09:07,000
 out.

155
00:09:07,000 --> 00:09:12,460
 Now the story goes that Tulapantika, when he became an Ar

156
00:09:12,460 --> 00:09:14,000
ahan, he also gained magical powers.

157
00:09:14,000 --> 00:09:17,170
 So he had, there were several, and also he gained knowledge

158
00:09:17,170 --> 00:09:19,000
 of the Pitikas.

159
00:09:19,000 --> 00:09:22,000
 So he gained quite an array of mental powers.

160
00:09:22,000 --> 00:09:29,000
 He lost all of this cloudiness in the mind.

161
00:09:29,000 --> 00:09:30,000
 So the stupidity left him.

162
00:09:30,000 --> 00:09:32,990
 He was no longer dullard and he was able to remember the

163
00:09:32,990 --> 00:09:35,000
 whole of the Buddha's teaching.

164
00:09:35,000 --> 00:09:38,020
 Everything that he had ever heard the Buddha teach,

165
00:09:38,020 --> 00:09:40,000
 suddenly it came back to him.

166
00:09:40,000 --> 00:09:45,000
 When he became enlightened because his mind became clear.

167
00:09:45,000 --> 00:09:47,450
 And also he had these magical powers. So he knew what his

168
00:09:47,450 --> 00:09:48,000
 brother was saying.

169
00:09:48,000 --> 00:09:50,000
 And he said, 'My brother said there's no monks here.'

170
00:09:50,000 --> 00:09:54,030
 And suddenly he made a resolve that the whole monastery

171
00:09:54,030 --> 00:09:55,000
 should be full of monks.

172
00:09:55,000 --> 00:09:57,780
 And there were a thousand, suddenly there were a thousand

173
00:09:57,780 --> 00:10:01,000
 monks filling Jetha-van, this monastery.

174
00:10:01,000 --> 00:10:03,100
 Some of them were dying their clothes, some of them were

175
00:10:03,100 --> 00:10:05,290
 washing their clothes, some of them were sweeping, some of

176
00:10:05,290 --> 00:10:06,000
 them were meditating.

177
00:10:06,000 --> 00:10:10,000
 Some of them were sitting, some of them were doing walking.

178
00:10:10,000 --> 00:10:13,050
 And this man came and saw all these monks and he went back

179
00:10:13,050 --> 00:10:16,000
 and he said, 'There's a thousand monks in the monastery.'

180
00:10:16,000 --> 00:10:18,530
 And two were not a Pindaka. The not a Pindaka told this to

181
00:10:18,530 --> 00:10:20,000
 the Buddha and the Buddha said,

182
00:10:20,000 --> 00:10:24,000
 'Go and tell them that you want Chula-pantaka.'

183
00:10:24,000 --> 00:10:27,710
 And so the man went back to Jetha-van and he said, 'Where

184
00:10:27,710 --> 00:10:30,000
 is Chula-pantaka?'

185
00:10:30,000 --> 00:10:33,490
 And all one thousand of them said, 'I am Chula-pantaka, I

186
00:10:33,490 --> 00:10:35,000
 am Chula-pantaka.'

187
00:10:35,000 --> 00:10:39,000
 And so he went back again and he told what to do.

188
00:10:39,000 --> 00:10:42,000
 They're all Chula-pantaka.

189
00:10:42,000 --> 00:10:46,340
 The whole point is to show his brother that he's not

190
00:10:46,340 --> 00:10:53,000
 useless and to make clear what the truth is.

191
00:10:53,000 --> 00:10:57,370
 And the Buddha says, 'No, no, go and say it again, ask it

192
00:10:57,370 --> 00:10:58,000
 again.'

193
00:10:58,000 --> 00:11:02,240
 And the first one who says, 'I am Chula-pantaka,' grabbed

194
00:11:02,240 --> 00:11:04,000
 onto his robe.

195
00:11:04,000 --> 00:11:07,430
 And so he went back and he grabbed and he said, 'Where's Ch

196
00:11:07,430 --> 00:11:08,000
ula-pantaka?'

197
00:11:08,000 --> 00:11:10,250
 And they all said, 'I am Chula-pantaka.' But he saw the one

198
00:11:10,250 --> 00:11:12,000
 who said it first and he grabbed him by the robe.

199
00:11:12,000 --> 00:11:14,930
 And suddenly all the rest disappeared and he said, 'Please

200
00:11:14,930 --> 00:11:19,000
 come, the Buddha is waiting for you.'

201
00:11:19,000 --> 00:11:23,000
 And so they invited him to go.

202
00:11:23,000 --> 00:11:30,190
 And then after the meal, everyone was kind of not sure, '

203
00:11:30,190 --> 00:11:33,620
Who is this? Is this guy a real monk or is he not a real

204
00:11:33,620 --> 00:11:34,000
 monk?'

205
00:11:34,000 --> 00:11:39,170
 And the Buddha said, 'At the end of the meal he said, 'Ch

206
00:11:39,170 --> 00:11:41,000
ula-pantaka, please, you give the Thanksgiving.'

207
00:11:41,000 --> 00:11:43,000
 So you give the talk after the meal.

208
00:11:43,000 --> 00:11:46,570
 Because after they had a meal they would always give a talk

209
00:11:46,570 --> 00:11:48,000
 to the main people.

210
00:11:48,000 --> 00:11:51,240
 And so the older brother and everyone was thinking, 'What's

211
00:11:51,240 --> 00:11:52,000
 he going to talk about?

212
00:11:52,000 --> 00:11:54,000
 He can't even remember a single stanza.'

213
00:11:54,000 --> 00:11:56,630
 And suddenly he gives this talk about everything the Buddha

214
00:11:56,630 --> 00:12:00,000
 taught and he goes through the whole of the three tipitaka.

215
00:12:00,000 --> 00:12:04,040
 And everyone realizes, 'Wow, something has changed and he

216
00:12:04,040 --> 00:12:06,000
 has become enlightened.'

217
00:12:06,000 --> 00:12:11,000
 And the story goes on to explain why he became like he was.

218
00:12:11,000 --> 00:12:14,220
 The story goes that in the past he was a king and he had

219
00:12:14,220 --> 00:12:17,000
 this idea of impurity.

220
00:12:17,000 --> 00:12:22,460
 Because he was going on a voyage through the city as the

221
00:12:22,460 --> 00:12:26,000
 king, going on a tour to see what was going on in the city.

222
00:12:26,000 --> 00:12:29,290
 And it was very hot and so he started sweating and he wiped

223
00:12:29,290 --> 00:12:33,000
 his face with this pure cloth and he saw it get dirty.

224
00:12:33,000 --> 00:12:35,000
 And he realized that this is the way with everything.

225
00:12:35,000 --> 00:12:39,770
 Nothing stays pure and perfect. Everything is subject to

226
00:12:39,770 --> 00:12:41,000
 change.

227
00:12:41,000 --> 00:12:43,500
 And so that's what the Buddha saw when he gave him this

228
00:12:43,500 --> 00:12:44,000
 cloth.

229
00:12:44,000 --> 00:12:48,910
 He saw that in his past he had already used this meditation

230
00:12:48,910 --> 00:12:50,000
 subject.

231
00:12:50,000 --> 00:12:52,000
 So then the monks were talking about this.

232
00:12:52,000 --> 00:12:54,470
 How could it be that he couldn't even memorize this single

233
00:12:54,470 --> 00:12:55,000
 stanza?

234
00:12:55,000 --> 00:12:59,290
 How could it be that he is able to make this refuge for

235
00:12:59,290 --> 00:13:01,000
 himself and become, you know,

236
00:13:01,000 --> 00:13:03,100
 find the ultimate refuge and become free from suffering

237
00:13:03,100 --> 00:13:06,000
 without even being able to memorize a single stanza?

238
00:13:06,000 --> 00:13:10,640
 The Buddha said, 'It's not by listening, it's not by memor

239
00:13:10,640 --> 00:13:12,000
izing, it's not by all this.'

240
00:13:12,000 --> 00:13:15,000
 The way a person is able to make a refuge for themselves,

241
00:13:15,000 --> 00:13:18,000
 an island for themselves,

242
00:13:18,000 --> 00:13:24,710
 is through four things, through effort, through heedfulness

243
00:13:24,710 --> 00:13:28,000
, through restraint and through training.

244
00:13:28,000 --> 00:13:30,250
 And he said, 'This is how a wise one makes an island for

245
00:13:30,250 --> 00:13:31,000
 themselves.'

246
00:13:31,000 --> 00:13:33,000
 And then he said the verse,

247
00:13:33,000 --> 00:13:40,560
 'UTANINA PAMADINA SANYAMINA DAME NACHA DEEPANG KAIRATAMIDHA

248
00:13:40,560 --> 00:13:44,000
VI YANG OGO NABI KIRATI.'

249
00:13:44,000 --> 00:13:47,870
 Which means precisely that, that these four things lead one

250
00:13:47,870 --> 00:13:50,000
 to make an island for oneself.

251
00:13:50,000 --> 00:13:53,000
 So that's briefly the story.

252
00:13:53,000 --> 00:13:58,000
 As to the meaning of it, this is a really important verse

253
00:13:58,000 --> 00:13:59,000
 and one that is wonderful for us,

254
00:13:59,000 --> 00:14:02,790
 if you can remember, to remember it, or at least keep in

255
00:14:02,790 --> 00:14:05,000
 mind what are these four things.

256
00:14:05,000 --> 00:14:10,000
 Because even just reminding yourself of this teaching

257
00:14:10,000 --> 00:14:15,000
 is of great use for us in meditation.

258
00:14:15,000 --> 00:14:19,000
 The first one, 'UTANINA' with effort.

259
00:14:19,000 --> 00:14:21,000
 And we're able to put out the effort in the practice.

260
00:14:21,000 --> 00:14:23,000
 But this is necessary.

261
00:14:23,000 --> 00:14:26,000
 This is the way that a person makes a refuge for themselves

262
00:14:26,000 --> 00:14:30,000
, or makes an island of themselves.

263
00:14:30,000 --> 00:14:34,140
 When a person, so what is it, first of all, what we mean by

264
00:14:34,140 --> 00:14:35,000
 island here,

265
00:14:35,000 --> 00:14:37,000
 is we're talking about a person becoming enlightened.

266
00:14:37,000 --> 00:14:41,000
 That's why we mean island, and the Buddha uses this simile

267
00:14:41,000 --> 00:14:43,000
 of the island and the flood.

268
00:14:43,000 --> 00:14:49,260
 So all beings, we understand samsara is like a flood, or is

269
00:14:49,260 --> 00:14:51,000
 like an ocean.

270
00:14:51,000 --> 00:14:55,010
 And the ocean is drowning us, or we're swimming about in

271
00:14:55,010 --> 00:14:57,000
 this ocean aimlessly.

272
00:14:57,000 --> 00:15:00,490
 The whole of the universe, the whole of samsara, you can't

273
00:15:00,490 --> 00:15:01,000
 find a purpose,

274
00:15:01,000 --> 00:15:03,790
 you can't find an aim, whatever you aim for, whatever you

275
00:15:03,790 --> 00:15:04,000
 go for,

276
00:15:04,000 --> 00:15:07,380
 whatever you reach, whatever you attain, becomes

277
00:15:07,380 --> 00:15:08,000
 meaningless.

278
00:15:08,000 --> 00:15:10,000
 And it's like carried away by the flood.

279
00:15:10,000 --> 00:15:15,190
 Whatever you build up, whatever great things, cities or pal

280
00:15:15,190 --> 00:15:16,000
aces,

281
00:15:16,000 --> 00:15:19,000
 or learning that you gain, it all disappears.

282
00:15:19,000 --> 00:15:21,000
 Anything you remember, eventually you forget.

283
00:15:21,000 --> 00:15:23,000
 Everything you create, eventually is destroyed.

284
00:15:23,000 --> 00:15:27,000
 So it's like an ocean, and there's nothing to hold on to.

285
00:15:27,000 --> 00:15:29,000
 There's nothing that is a secure refuge.

286
00:15:29,000 --> 00:15:31,000
 You build up a refuge, it disappears.

287
00:15:31,000 --> 00:15:35,000
 It leaves you without.

288
00:15:35,000 --> 00:15:38,610
 So the finding of the real refuge is like building an

289
00:15:38,610 --> 00:15:39,000
 island,

290
00:15:39,000 --> 00:15:43,000
 or finding an island that the flood can't overcome.

291
00:15:43,000 --> 00:15:47,210
 No matter how much the waves come, or tsunami, or any kind

292
00:15:47,210 --> 00:15:49,000
 of tides of any sort,

293
00:15:49,000 --> 00:15:50,000
 can't overcome this island.

294
00:15:50,000 --> 00:15:52,000
 This is what we're all looking for in life.

295
00:15:52,000 --> 00:15:55,000
 We're trying to find the stability and the security.

296
00:15:55,000 --> 00:15:57,400
 The Buddha said the way to find this is by these four

297
00:15:57,400 --> 00:15:58,000
 things.

298
00:15:58,000 --> 00:16:04,000
 So "utana" means effort, that we have to work hard.

299
00:16:04,000 --> 00:16:07,000
 This is the work that we see that Chula Pantaka did,

300
00:16:07,000 --> 00:16:15,000
 that it wasn't effort at studying, it wasn't effort in arts

301
00:16:15,000 --> 00:16:15,000
,

302
00:16:15,000 --> 00:16:17,000
 or in being a monk or so on.

303
00:16:17,000 --> 00:16:20,120
 It was effort in seeing the truth and in cleaning out the

304
00:16:20,120 --> 00:16:21,000
 stains inside.

305
00:16:21,000 --> 00:16:24,000
 So just this practice of meditation,

306
00:16:24,000 --> 00:16:28,770
 and then going through, moment by moment, going through his

307
00:16:28,770 --> 00:16:29,000
 mind,

308
00:16:29,000 --> 00:16:32,570
 seeing the lust, seeing the anger, seeing the delusion that

309
00:16:32,570 --> 00:16:34,000
 was in his mind,

310
00:16:34,000 --> 00:16:37,000
 and weeding it out, rooting it out.

311
00:16:37,000 --> 00:16:39,000
 This is the effort that he put out.

312
00:16:39,000 --> 00:16:42,000
 This is the effort that we all need.

313
00:16:42,000 --> 00:16:44,000
 We have to do the work.

314
00:16:44,000 --> 00:16:46,000
 So this is what it means to meditate.

315
00:16:46,000 --> 00:16:49,000
 When we talk about meditation, some people, they say,

316
00:16:49,000 --> 00:16:52,000
 being mindful doesn't require meditation.

317
00:16:52,000 --> 00:16:54,000
 A person can just be mindful in daily life,

318
00:16:54,000 --> 00:16:56,000
 and that's enough to become enlightened.

319
00:16:56,000 --> 00:17:00,960
 And the truth is, it's possible, but it's very, very

320
00:17:00,960 --> 00:17:03,000
 difficult.

321
00:17:03,000 --> 00:17:05,000
 It actually takes a lot of work,

322
00:17:05,000 --> 00:17:09,000
 and you have to actually do something to raise yourself up.

323
00:17:09,000 --> 00:17:11,000
 People say that just naturally it comes,

324
00:17:11,000 --> 00:17:13,000
 you can naturally become enlightened,

325
00:17:13,000 --> 00:17:15,000
 and that's quite misleading and I think quite inaccurate.

326
00:17:15,000 --> 00:17:17,000
 The person has to work.

327
00:17:17,000 --> 00:17:19,000
 This is what we mean by the word "practicing meditation,"

328
00:17:19,000 --> 00:17:22,940
 or the word "meditation" means to apply yourself to the

329
00:17:22,940 --> 00:17:24,000
 practice.

330
00:17:24,000 --> 00:17:26,000
 The second one, "apamada."

331
00:17:26,000 --> 00:17:30,000
 "Apamada" means the clarity of mind, the mindfulness.

332
00:17:30,000 --> 00:17:35,000
 So not just pushing yourself to practice meditation,

333
00:17:35,000 --> 00:17:39,000
 but actually bringing the mind back and guarding the mind,

334
00:17:39,000 --> 00:17:43,000
 applying the mind to the object.

335
00:17:43,000 --> 00:17:46,000
 When you see something clearly knowing that this is seeing,

336
00:17:46,000 --> 00:17:48,000
 when you hear something knowing this is hearing,

337
00:17:48,000 --> 00:17:52,000
 being with reality from moment to moment,

338
00:17:52,000 --> 00:17:54,000
 and not letting yourself slip away.

339
00:17:54,000 --> 00:18:00,000
 The mindfulness, or what we call "sati," reminding yourself

340
00:18:00,000 --> 00:18:00,000
,

341
00:18:00,000 --> 00:18:03,000
 this act of reminding yourself of what's going on,

342
00:18:03,000 --> 00:18:05,000
 this is what we mean by "heatfulness,"

343
00:18:05,000 --> 00:18:09,000
 being heatful, being aware of the object.

344
00:18:09,000 --> 00:18:12,000
 The third one, "sanyamena."

345
00:18:12,000 --> 00:18:15,000
 Sanyamena means keeping the mind from getting distracted.

346
00:18:15,000 --> 00:18:19,000
 So at the same time that we're watching the object,

347
00:18:19,000 --> 00:18:22,000
 we have to watch ourselves when we get distracted.

348
00:18:22,000 --> 00:18:30,000
 Sanyamena means keeping the mind within boundaries.

349
00:18:30,000 --> 00:18:33,400
 So not letting the mind wander, not following after things

350
00:18:33,400 --> 00:18:34,000
 that you see

351
00:18:34,000 --> 00:18:37,000
 or things that you hear, not getting caught up in random

352
00:18:37,000 --> 00:18:37,000
 thoughts

353
00:18:37,000 --> 00:18:39,000
 or daydreams or so on.

354
00:18:39,000 --> 00:18:43,000
 This is sanyamena, keeping the mind restrained.

355
00:18:43,000 --> 00:18:45,750
 And the fourth one that goes quite along with with sanyam

356
00:18:45,750 --> 00:18:46,000
ena

357
00:18:46,000 --> 00:18:50,000
 and means very much the same thing as "dhammena," "dhamma."

358
00:18:50,000 --> 00:18:53,470
 But dhamma is more like training yourself so that the mind

359
00:18:53,470 --> 00:18:54,000
 doesn't run away,

360
00:18:54,000 --> 00:18:56,000
 so that the mind doesn't get distracted.

361
00:18:56,000 --> 00:18:59,000
 Sanyamena is guarding it so it doesn't get distracted.

362
00:18:59,000 --> 00:19:02,690
 Dhammena is the aspect of the practice that changes the

363
00:19:02,690 --> 00:19:03,000
 mind,

364
00:19:03,000 --> 00:19:06,290
 straightens the mind, so that the mind no longer wants to

365
00:19:06,290 --> 00:19:07,000
 go away,

366
00:19:07,000 --> 00:19:10,330
 wants to be distracted, no longer wants to follow after day

367
00:19:10,330 --> 00:19:11,000
dreams

368
00:19:11,000 --> 00:19:16,000
 or addictions or desires or so on.

369
00:19:16,000 --> 00:19:18,000
 Basically this refers to the practice of meditation.

370
00:19:18,000 --> 00:19:20,000
 You put out the effort in the practice

371
00:19:20,000 --> 00:19:25,000
 and then apply yourself correctly in the practice.

372
00:19:25,000 --> 00:19:28,000
 You guard yourself from getting distracted

373
00:19:28,000 --> 00:19:31,790
 and train yourself so that in the end you don't want to

374
00:19:31,790 --> 00:19:33,000
 become distracted,

375
00:19:33,000 --> 00:19:39,030
 so that your mind is focused and resolute and intent upon

376
00:19:39,030 --> 00:19:40,000
 the object.

377
00:19:40,000 --> 00:19:43,270
 This is really how Chula Pantika, even though the text

378
00:19:43,270 --> 00:19:44,000
 doesn't go into detail,

379
00:19:44,000 --> 00:19:46,000
 this is how he became enlightened.

380
00:19:46,000 --> 00:19:49,000
 Because when the Buddha started explaining to him what he

381
00:19:49,000 --> 00:19:49,000
 was seeing,

382
00:19:49,000 --> 00:19:55,000
 this change that was going on in the cloth,

383
00:19:55,000 --> 00:19:58,510
 and also in his mind, when the mind started to lose its

384
00:19:58,510 --> 00:19:59,000
 interest

385
00:19:59,000 --> 00:20:02,000
 and its attachment to the beautiful cloth,

386
00:20:02,000 --> 00:20:07,540
 it's the greed, it's the anger, it's the delusion that we

387
00:20:07,540 --> 00:20:09,000
're trying to change,

388
00:20:09,000 --> 00:20:12,540
 that is changing here, until eventually your mind simply

389
00:20:12,540 --> 00:20:15,000
 knows the object for what it is.

390
00:20:15,000 --> 00:20:20,000
 He applied himself to this with effort, with mindfulness,

391
00:20:20,000 --> 00:20:22,410
 clearly aware this is greed, this is anger, this is

392
00:20:22,410 --> 00:20:23,000
 delusion,

393
00:20:23,000 --> 00:20:26,450
 this is seeing, this is hearing, this is smelling, this is

394
00:20:26,450 --> 00:20:29,000
 tasting and so on.

395
00:20:29,000 --> 00:20:35,000
 Guarding himself and training himself in this way.

396
00:20:35,000 --> 00:20:38,000
 As a result he became an Arahant.

397
00:20:38,000 --> 00:20:39,620
 He used another small teaching and one that's very

398
00:20:39,620 --> 00:20:42,000
 important for us as meditators,

399
00:20:42,000 --> 00:20:44,000
 something that we can all take to heart.

400
00:20:44,000 --> 00:20:47,000
 So that's our teaching on the Dhammapada for today.

401
00:20:47,000 --> 00:20:50,280
 Thank you all for tuning in and listening and for joining

402
00:20:50,280 --> 00:20:51,000
 us today,

403
00:20:51,000 --> 00:20:54,000
 where we can go back to our practice.

404
00:20:54,000 --> 00:20:56,000
 Sad, sad.

405
00:20:56,000 --> 00:21:00,000
 [ Sighs ]

