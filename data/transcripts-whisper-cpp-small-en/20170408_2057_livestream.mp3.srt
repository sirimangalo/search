1
00:00:00,000 --> 00:00:27,800
 Okay, good evening everyone.

2
00:00:27,800 --> 00:00:35,160
 Welcome to our evening Dhamma session, live here from

3
00:00:35,160 --> 00:00:42,880
 Hamilton, Ontario, Canada.

4
00:00:42,880 --> 00:00:49,330
 So tonight, we go on and talk about the third discourse of

5
00:00:49,330 --> 00:00:51,000
 the Buddha.

6
00:00:51,000 --> 00:00:56,160
 It's not actually the third discourse, but it really is the

7
00:00:56,160 --> 00:01:00,120
 third discourse that we have

8
00:01:00,120 --> 00:01:07,960
 recorded chronologically, as far as I can think of.

9
00:01:07,960 --> 00:01:14,610
 So to recap, the Buddha gave his first discourse, which was

10
00:01:14,610 --> 00:01:16,280
 called the turning of the wheel

11
00:01:16,280 --> 00:01:17,280
 of the Dhamma.

12
00:01:17,280 --> 00:01:22,670
 So at that point, he had set in motion this knowledge,

13
00:01:22,670 --> 00:01:23,720
 right?

14
00:01:23,720 --> 00:01:25,760
 He had given this to the world.

15
00:01:25,760 --> 00:01:29,520
 There was no turning back.

16
00:01:29,520 --> 00:01:32,300
 Not that there was any desire to turn back, but at that

17
00:01:32,300 --> 00:01:33,920
 point, the cat was out of the

18
00:01:33,920 --> 00:01:37,720
 bag and so to speak.

19
00:01:37,720 --> 00:01:41,260
 It was inevitable that Buddhism would spread because the

20
00:01:41,260 --> 00:01:42,880
 teaching is powerful.

21
00:01:42,880 --> 00:01:51,330
 The power was in the realization of the five monks, which

22
00:01:51,330 --> 00:01:56,520
 ensured that it would be spread.

23
00:01:56,520 --> 00:02:00,170
 Five days later, he gave the second discourse and they all

24
00:02:00,170 --> 00:02:02,400
 became arahant, which means they

25
00:02:02,400 --> 00:02:03,400
 became enlightened.

26
00:02:03,400 --> 00:02:09,400
 They were no longer subject to rebirth and freed themselves

27
00:02:09,400 --> 00:02:11,860
 from all defilement.

28
00:02:11,860 --> 00:02:15,320
 And they spent the three months of the rains in Isipatana.

29
00:02:15,320 --> 00:02:22,040
 Isipatana was this some kind of grove, some kind of park

30
00:02:22,040 --> 00:02:26,400
 where religious people hung out,

31
00:02:26,400 --> 00:02:32,920
 ascetics maybe.

32
00:02:32,920 --> 00:02:38,870
 And during the rains, there was a man called Yasa who was

33
00:02:38,870 --> 00:02:40,360
 very rich.

34
00:02:40,360 --> 00:02:43,400
 His parents were very rich.

35
00:02:43,400 --> 00:02:47,580
 And so he lived in the lap of luxury and had everything he

36
00:02:47,580 --> 00:02:49,160
 could ever want.

37
00:02:49,160 --> 00:02:52,270
 One night, and he lived in Varanasi, which is near Isipat

38
00:02:52,270 --> 00:02:53,960
ana, which is just outside of

39
00:02:53,960 --> 00:02:58,310
 Varanasi, which is a big city in India still to this day,

40
00:02:58,310 --> 00:03:00,800
 ancient city, the city of Gaya

41
00:03:00,800 --> 00:03:01,920
 really.

42
00:03:01,920 --> 00:03:09,240
 I know the city of Varanasi, but the river Ganges, it's

43
00:03:09,240 --> 00:03:12,960
 where the Ganga River is.

44
00:03:12,960 --> 00:03:13,960
 Kasi I'm thinking of.

45
00:03:13,960 --> 00:03:22,160
 It's also called Kasi, or it used to be.

46
00:03:22,160 --> 00:03:25,580
 And so one night, Yasa wakes up in the middle of the night

47
00:03:25,580 --> 00:03:27,520
 and he looks around his palace

48
00:03:27,520 --> 00:03:34,440
 and he sees his consorts, the women who are meant to please

49
00:03:34,440 --> 00:03:38,800
 him as a very, very rich man.

50
00:03:38,800 --> 00:03:43,260
 His concubines maybe, I don't know, just dancing girls

51
00:03:43,260 --> 00:03:43,960
 maybe.

52
00:03:43,960 --> 00:03:48,280
 And he sees them all asleep and they look like corpses.

53
00:03:48,280 --> 00:03:53,320
 And he gets this strong sense of death.

54
00:03:53,320 --> 00:03:54,960
 This is what it's like to die.

55
00:03:54,960 --> 00:03:57,720
 It's curious, I can empathize.

56
00:03:57,720 --> 00:04:00,920
 I had this sort of feeling myself when I was young.

57
00:04:00,920 --> 00:04:04,120
 Everyone would go to sleep and I think they were all dead.

58
00:04:04,120 --> 00:04:09,120
 It makes one wonder whether this is an echo of our

59
00:04:09,120 --> 00:04:13,320
 experiences of death, which of course

60
00:04:13,320 --> 00:04:17,850
 very strong and would be one of the stronger memories that

61
00:04:17,850 --> 00:04:20,200
 might come carry over with us

62
00:04:20,200 --> 00:04:21,760
 in some form.

63
00:04:21,760 --> 00:04:22,410
 Anyway, he became quite distraught, left his palace or his

64
00:04:22,410 --> 00:04:31,680
 mansion and wandered into Isipatana.

65
00:04:31,680 --> 00:04:35,750
 He wandered out of the city, went for a walk, ended up in

66
00:04:35,750 --> 00:04:41,160
 Isipatana muttering to himself,

67
00:04:41,160 --> 00:04:53,440
 "This place is, this world is crazy.

68
00:04:53,440 --> 00:04:54,440
 The world is all messed up."

69
00:04:54,440 --> 00:04:57,680
 The Buddha heard him and said to him, "Yasaka, I'm here.

70
00:04:57,680 --> 00:04:59,680
 Here it is not messed up.

71
00:04:59,680 --> 00:05:03,280
 Here it is not confused.

72
00:05:03,280 --> 00:05:05,280
 It's not entangled."

73
00:05:05,280 --> 00:05:09,350
 And Yasaka heard this voice and so he went to the Buddha

74
00:05:09,350 --> 00:05:11,480
 and he sat down and the Buddha

75
00:05:11,480 --> 00:05:16,080
 taught him the Dhamma and he became a Sotapana.

76
00:05:16,080 --> 00:05:19,130
 And his parents came looking for him, found him, found the

77
00:05:19,130 --> 00:05:21,120
 Buddha, the Buddha taught them,

78
00:05:21,120 --> 00:05:25,170
 they became a Sotapana and while he was teaching them, Yas

79
00:05:25,170 --> 00:05:27,680
aka became an Arahant when he heard

80
00:05:27,680 --> 00:05:43,200
 the Dhamma the second time.

81
00:05:43,200 --> 00:05:47,400
 Then Yasaka's friends, 54 of his friends became monks.

82
00:05:47,400 --> 00:05:49,840
 They heard that Yasaka had gone forth and left the home

83
00:05:49,840 --> 00:05:51,240
 life because as an Arahanti,

84
00:05:51,240 --> 00:05:53,760
 he became a monk right away.

85
00:05:53,760 --> 00:05:57,700
 And they heard this and so 54 of them, he had a large

86
00:05:57,700 --> 00:06:00,320
 society in the city, became monks

87
00:06:00,320 --> 00:06:05,080
 just on his reputation.

88
00:06:05,080 --> 00:06:06,560
 And they all became Arahants as well.

89
00:06:06,560 --> 00:06:10,520
 So at that time there was the Buddha plus 5 plus 55.

90
00:06:10,520 --> 00:06:17,270
 No, the Buddha plus 5, yes, plus 55, so there were 61 at

91
00:06:17,270 --> 00:06:23,360
 that time, 61 Arahants in the world.

92
00:06:23,360 --> 00:06:29,500
 And then the Buddha at the end of the reigns, he said to

93
00:06:29,500 --> 00:06:33,680
 the monks, Charat bhikkhu, go forth

94
00:06:33,680 --> 00:06:40,370
 monks for the benefit of all, for the benefit of the many,

95
00:06:40,370 --> 00:06:44,880
 bhucchanasukaya, bhucchanahitaya,

96
00:06:44,880 --> 00:06:47,040
 but don't go by the same road.

97
00:06:47,040 --> 00:06:51,960
 He said go your separate ways, meaning in order for this to

98
00:06:51,960 --> 00:06:55,080
 spread, you're all enlightened,

99
00:06:55,080 --> 00:06:59,200
 there's no need for you to depend on anyone, so be a refuge

100
00:06:59,200 --> 00:07:00,200
 to others.

101
00:07:00,200 --> 00:07:04,560
 And he sent them all in 60 different directions.

102
00:07:04,560 --> 00:07:05,560
 Pretty powerful.

103
00:07:05,560 --> 00:07:10,480
 That was the start of the dispensation of the Buddha, the

104
00:07:10,480 --> 00:07:13,040
 Buddhist missionaryism.

105
00:07:13,040 --> 00:07:16,230
 Some people say Buddhism is a missionary religion, which

106
00:07:16,230 --> 00:07:18,360
 isn't really, it's a bit misleading

107
00:07:18,360 --> 00:07:21,350
 because these monks didn't go out and knock on people's

108
00:07:21,350 --> 00:07:23,240
 door and say, "Hey, have you heard

109
00:07:23,240 --> 00:07:24,240
 the good word?"

110
00:07:24,240 --> 00:07:27,280
 They would just go and live in various places.

111
00:07:27,280 --> 00:07:33,570
 For example, Asaji left, and when Sariputta saw him, just

112
00:07:33,570 --> 00:07:36,200
 saw him, he knew right away

113
00:07:36,200 --> 00:07:38,970
 that this was someone worth listening to and he came and

114
00:07:38,970 --> 00:07:40,520
 listened to his teaching.

115
00:07:40,520 --> 00:07:43,530
 What happens today in Buddhism, monks don't have to go to

116
00:07:43,530 --> 00:07:44,960
 their way to teach, there's

117
00:07:44,960 --> 00:07:47,640
 so many people interested in learning.

118
00:07:47,640 --> 00:07:51,000
 If you put the teaching out there, many people, not

119
00:07:51,000 --> 00:07:53,760
 everyone, most people don't, but there

120
00:07:53,760 --> 00:08:01,150
 are always people who want to hear, always people who will

121
00:08:01,150 --> 00:08:04,240
 come and ask to learn.

122
00:08:04,240 --> 00:08:08,920
 And the Buddha himself went to Uruvela, and in Uruvela he

123
00:08:08,920 --> 00:08:11,680
 met these, well, first why he

124
00:08:11,680 --> 00:08:17,500
 was going to Uruvela is he wanted to go to Rajagaha to meet

125
00:08:17,500 --> 00:08:20,440
 Bimbisara, and Bimbisara

126
00:08:20,440 --> 00:08:24,330
 was the king of Rajagaha, and so he knew that this would be

127
00:08:24,330 --> 00:08:26,680
 a great place to start spreading

128
00:08:26,680 --> 00:08:29,520
 his teaching, a great place to live, and Bimbisara would be

129
00:08:29,520 --> 00:08:31,000
 a great ally, which of course he

130
00:08:31,000 --> 00:08:33,400
 eventually was.

131
00:08:33,400 --> 00:08:38,310
 Because Varanasi was perhaps too much entrenched in Brahman

132
00:08:38,310 --> 00:08:41,080
ism, and it would just, it would

133
00:08:41,080 --> 00:08:43,160
 be very difficult and there would be too much conflict.

134
00:08:43,160 --> 00:08:46,470
 It's like a general knows that he has to retreat and build

135
00:08:46,470 --> 00:08:48,280
 up his forces because there were

136
00:08:48,280 --> 00:08:53,340
 only 60 of them, and so he wanted to start, he wanted to

137
00:08:53,340 --> 00:08:55,280
 come in with a bang.

138
00:08:55,280 --> 00:08:58,270
 But he knew that if he went to Rajagaha, there would be a

139
00:08:58,270 --> 00:09:00,400
 similar situation, and so instead

140
00:09:00,400 --> 00:09:05,270
 of going directly to Rajagaha, he went to Uruvela where the

141
00:09:05,270 --> 00:09:07,280
 three most famous ascetics

142
00:09:07,280 --> 00:09:12,080
 were living, it was just outside of Rajagaha.

143
00:09:12,080 --> 00:09:16,080
 And I guess with the idea, or we guess with the idea that

144
00:09:16,080 --> 00:09:18,480
 if he converted these three,

145
00:09:18,480 --> 00:09:24,880
 then everyone, not just Bimbisara, but the whole of the

146
00:09:24,880 --> 00:09:28,400
 kingdom of Rajagaha would know

147
00:09:28,400 --> 00:09:32,480
 how powerful or how great the Buddha's teaching was.

148
00:09:32,480 --> 00:09:38,250
 And so he converted these three ascetics through various

149
00:09:38,250 --> 00:09:41,920
 ways, and all of their disciples,

150
00:09:41,920 --> 00:09:48,880
 which ended up being 500 followers of the oldest brother,

151
00:09:48,880 --> 00:09:51,920
 there were the three brothers,

152
00:09:51,920 --> 00:09:55,430
 the 500 followers of him, his second youngest brother,

153
00:09:55,430 --> 00:09:58,000
 second brother, 300 followers, and

154
00:09:58,000 --> 00:10:01,000
 the youngest brother, 250 according to Buddhism, or

155
00:10:01,000 --> 00:10:02,880
 according to Theravada Buddhism.

156
00:10:02,880 --> 00:10:03,880
 So, that's what we're going to do.

