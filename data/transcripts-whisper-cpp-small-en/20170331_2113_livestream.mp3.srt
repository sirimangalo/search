1
00:00:00,000 --> 00:00:04,350
 I'm very lucky and very fortunate and even very good at

2
00:00:04,350 --> 00:00:05,000
 what I do.

3
00:00:05,000 --> 00:00:11,560
 And I become very wealthy as a result. I should share.

4
00:00:11,560 --> 00:00:16,520
 You know, that's, I think many of the things the social, in

5
00:00:16,520 --> 00:00:18,880
 Canada anyway, we have a lot of, we have a social net.

6
00:00:18,880 --> 00:00:23,680
 That prevents people from suffering terribly.

7
00:00:29,320 --> 00:00:32,770
 Many of those things are what we consider to be important

8
00:00:32,770 --> 00:00:35,240
 as a society, which is a really good thing to see.

9
00:00:35,240 --> 00:00:37,280
 It's really encouraging and heartwarming.

10
00:00:37,280 --> 00:00:42,720
 We have this principle of charity and, and well, just

11
00:00:42,720 --> 00:00:45,360
 charity, but communion.

12
00:00:45,360 --> 00:00:51,450
 Or if I get very happy and very lucky and very fortunate, I

13
00:00:51,450 --> 00:00:53,280
 don't do it alone.

14
00:00:53,280 --> 00:00:56,200
 And I bring everyone else with me to some extent.

15
00:00:56,200 --> 00:01:05,620
 Communions, that means seeing, you're seeing the community

16
00:01:05,620 --> 00:01:08,480
 as important.

17
00:01:08,480 --> 00:01:13,640
 You know, on the one hand in Buddhism, we are very focused

18
00:01:13,640 --> 00:01:15,480
 on the self, right?

19
00:01:15,480 --> 00:01:18,800
 Ata anam mei wo patamang pati rupi anyways.

20
00:01:18,800 --> 00:01:21,520
 Set yourself in what's right.

21
00:01:21,520 --> 00:01:24,720
 Do what's in your own best interest.

22
00:01:24,720 --> 00:01:36,900
 But what's in your best interest, of course, is a harmon

23
00:01:36,900 --> 00:01:38,320
ious community.

24
00:01:38,320 --> 00:01:45,600
 And not only that, but how can you be concerned about your

25
00:01:45,600 --> 00:01:49,040
 own best interest if you're not supportive of other people?

26
00:01:52,600 --> 00:01:54,720
 Seeking out what's in their own best interest.

27
00:01:54,720 --> 00:01:59,470
 If you deny other people the opportunity to find peace,

28
00:01:59,470 --> 00:02:01,680
 happiness and freedom from suffering, how could you find it

29
00:02:01,680 --> 00:02:02,160
 yourself?

30
00:02:02,160 --> 00:02:08,180
 If you're not supportive, insofar as it's in your sphere of

31
00:02:08,180 --> 00:02:09,600
 activity.

32
00:02:09,600 --> 00:02:18,040
 So one of the greatest aspects of harmony is communion.

33
00:02:22,000 --> 00:02:28,190
 Community, the commune, communal resources, communal

34
00:02:28,190 --> 00:02:31,840
 benefits, communal happiness.

35
00:02:31,840 --> 00:02:45,680
 The fifth is a purity of ethics, a common purity of ethics.

36
00:02:47,080 --> 00:02:52,710
 Meaning, quite obviously, that to have a harmonious

37
00:02:52,710 --> 00:02:56,290
 community, you need laws and everyone has to be keeping

38
00:02:56,290 --> 00:02:56,960
 those laws.

39
00:02:56,960 --> 00:03:04,400
 You need good rules and the rules need to be kept.

40
00:03:04,400 --> 00:03:10,860
 You see this very clearly in Buddhist monastic circles,

41
00:03:10,860 --> 00:03:15,600
 because we have a monastery is an interesting place because

42
00:03:15,600 --> 00:03:16,520
 you have people

43
00:03:16,520 --> 00:03:24,530
 dealing with the very innermost poisons in their mind,

44
00:03:24,530 --> 00:03:25,960
 right?

45
00:03:25,960 --> 00:03:27,800
 They're dealing with their mental issues.

46
00:03:27,800 --> 00:03:31,400
 They don't have a lot of outlet for them.

47
00:03:31,400 --> 00:03:36,640
 And so it can be quite a stressful activity, as until you

48
00:03:36,640 --> 00:03:42,480
 learn to be without chasing after the things that you want

49
00:03:42,480 --> 00:03:44,920
 and avoiding and running away from the things that you don

50
00:03:44,920 --> 00:03:45,240
't want.

51
00:03:45,240 --> 00:03:56,830
 And you have such a diverse group of people whose reasons

52
00:03:56,830 --> 00:04:00,520
 for being together have nothing to do with anything like

53
00:04:00,520 --> 00:04:03,360
 friendship or common history.

54
00:04:03,360 --> 00:04:06,520
 And often in a monastery, monks will come together from

55
00:04:06,520 --> 00:04:08,400
 very different backgrounds.

56
00:04:08,400 --> 00:04:13,100
 And so there's a great in monasteries, surprisingly, to

57
00:04:13,100 --> 00:04:18,000
 some, there's a great potential for conflict and strife for

58
00:04:18,000 --> 00:04:19,560
 these reasons.

59
00:04:19,560 --> 00:04:23,280
 And everyone's dealing with emotional issues.

60
00:04:23,280 --> 00:04:26,170
 It's not a good time to be around other people who are also

61
00:04:26,170 --> 00:04:28,040
 dealing with emotional issues.

62
00:04:28,040 --> 00:04:31,280
 So rules become increasingly important.

63
00:04:33,040 --> 00:04:36,400
 And so we have this very rigid, in fact, strict set of

64
00:04:36,400 --> 00:04:38,800
 rules pretty much for that reason.

65
00:04:38,800 --> 00:04:45,260
 You have such a charged environment and such a diversity of

66
00:04:45,260 --> 00:04:49,480
 backgrounds and karmas, right?

67
00:04:50,760 --> 00:04:54,940
 It's like it's in some ways a clashing of karma because our

68
00:04:54,940 --> 00:04:59,060
 narrative that we've built up that's led us here is often

69
00:04:59,060 --> 00:05:02,720
 very different from where we find ourselves in the

70
00:05:02,720 --> 00:05:04,080
 monastery.

71
00:05:04,080 --> 00:05:07,740
 I was supposed to be, I don't know what I personally, I was

72
00:05:07,740 --> 00:05:10,560
 supposed to probably be a mathematician or something.

73
00:05:12,000 --> 00:05:16,930
 And I've cut that off, you know, like I could, I could have

74
00:05:16,930 --> 00:05:21,740
 become probably quite a successful mathematician or I don't

75
00:05:21,740 --> 00:05:23,600
 know, even an author.

76
00:05:23,600 --> 00:05:27,100
 Many of the different things in the world and cut them all

77
00:05:27,100 --> 00:05:28,920
 off when it became a monk.

78
00:05:28,920 --> 00:05:31,240
 And you see that you have stories like this.

79
00:05:31,240 --> 00:05:32,560
 Everyone had their paths.

80
00:05:32,560 --> 00:05:37,030
 Anyway, the point being that when you come together, there

81
00:05:37,030 --> 00:05:38,760
's a need for rules.

82
00:05:38,760 --> 00:05:41,080
 The same thing goes for society.

83
00:05:41,080 --> 00:05:46,160
 There's such a diversity of opinions and backgrounds and

84
00:05:46,160 --> 00:05:49,840
 potentials, capabilities.

85
00:05:49,840 --> 00:05:57,310
 And until or as long as you things aren't sorted out, you

86
00:05:57,310 --> 00:05:58,800
 need rules.

87
00:05:58,800 --> 00:06:04,770
 Harmony comes from keeping rules, obeying laws and having

88
00:06:04,770 --> 00:06:06,120
 good laws.

89
00:06:08,440 --> 00:06:11,120
 There are people who believe that laws should be abandoned.

90
00:06:11,120 --> 00:06:15,110
 They think there should be no one of the laws is there

91
00:06:15,110 --> 00:06:17,160
 should be no drug laws.

92
00:06:17,160 --> 00:06:22,310
 And I can I can sympathize with that because drug laws only

93
00:06:22,310 --> 00:06:25,180
 serve to put more people in jail because there are people

94
00:06:25,180 --> 00:06:25,920
 doing drugs.

95
00:06:25,920 --> 00:06:33,140
 But it doesn't take away the fact that doing heroin and and

96
00:06:33,140 --> 00:06:37,320
 and to a limited extent, I would say marijuana, alcohol.

97
00:06:37,320 --> 00:06:38,320
 These are not.

