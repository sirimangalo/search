1
00:00:00,000 --> 00:00:04,530
 Are those defilements that have swelled to grow in past or

2
00:00:04,530 --> 00:00:06,000
 future or present?

3
00:00:06,000 --> 00:00:09,640
 And the answer is, they are simply those described as

4
00:00:09,640 --> 00:00:12,000
 arisen by having swelled to grow in.

5
00:00:12,000 --> 00:00:13,000
 We are just that.

6
00:00:25,000 --> 00:00:30,020
 Are those defilements that have swelled to grow in past or

7
00:00:30,020 --> 00:00:32,000
 future or present?

8
00:00:32,000 --> 00:00:34,870
 And the answer is, they are simply those described as

9
00:00:34,870 --> 00:00:37,000
 arisen by having swelled to grow in.

10
00:00:37,000 --> 00:00:39,000
 We are just that.

11
00:00:39,000 --> 00:00:42,260
 We cannot say that they are present or they are future or

12
00:00:42,260 --> 00:00:43,000
 they are past.

13
00:00:43,000 --> 00:00:46,980
 But they are just those that have arisen by having swelled

14
00:00:46,980 --> 00:00:48,000
 to grow in.

15
00:00:52,000 --> 00:00:55,490
 Now there are various meanings of arisen that is to say

16
00:00:55,490 --> 00:00:57,000
 arisen as actual operating and so on.

17
00:00:57,000 --> 00:01:03,340
 I know this isn't exactly said but is this close to saying

18
00:01:03,340 --> 00:01:06,000
 that they are empty?

19
00:01:06,000 --> 00:01:10,000
 No.

20
00:01:10,000 --> 00:01:17,000
 We will explain it later.

21
00:01:17,000 --> 00:01:25,690
 So then the commentator brings in four kinds of what are

22
00:01:25,690 --> 00:01:28,000
 called arisen.

23
00:01:28,000 --> 00:01:30,000
 The Pali what is Upana.

24
00:01:30,000 --> 00:01:34,000
 So on the Sheik I gave you the Pali what also.

25
00:01:34,000 --> 00:01:38,000
 Four kinds of Upana that which is arisen.

26
00:01:38,000 --> 00:01:41,000
 Four things that are called Upana in Pali.

27
00:01:41,000 --> 00:01:46,000
 And the word Upana means that has arisen.

28
00:01:46,000 --> 00:01:53,000
 Now the first is arisen as actually occurring.

29
00:01:53,000 --> 00:01:55,580
 That means all that is reckoned to possess the three

30
00:01:55,580 --> 00:01:58,000
 moments of arising, aging and dissolution.

31
00:01:58,000 --> 00:02:02,000
 That means it is the real present things.

32
00:02:02,000 --> 00:02:06,740
 Because when we say something is present we mean it

33
00:02:06,740 --> 00:02:12,000
 possesses three stages of arising, staying and disappearing

34
00:02:12,000 --> 00:02:12,000
.

35
00:02:12,000 --> 00:02:14,000
 Or arising, aging and dissolution.

36
00:02:14,000 --> 00:02:17,000
 So this is the real present.

37
00:02:17,000 --> 00:02:21,210
 This real present is sometimes called Upana that which has

38
00:02:21,210 --> 00:02:22,000
 arisen.

39
00:02:22,000 --> 00:02:27,000
 The next one is arisen as being and gone.

40
00:02:27,000 --> 00:02:31,000
 That means they have arisen and now they are no more.

41
00:02:31,000 --> 00:02:33,000
 Arisen and gone.

42
00:02:33,000 --> 00:02:37,000
 So they are also called arisen and there are two of them.

43
00:02:37,000 --> 00:02:40,570
 Wholesome and unwholesome dhammas that is chaitas and chait

44
00:02:40,570 --> 00:02:41,000
as egas.

45
00:02:41,000 --> 00:02:44,000
 Which have experienced the stimulus of an object.

46
00:02:44,000 --> 00:02:47,000
 That means which have tasted the object.

47
00:02:47,000 --> 00:02:52,240
 Which have experienced the taste of the object and

48
00:02:52,240 --> 00:02:54,000
 disappeared.

49
00:02:54,000 --> 00:02:59,000
 So they are also called arisen as being and gone.

50
00:02:59,000 --> 00:03:03,000
 Now here being means experienced.

51
00:03:03,000 --> 00:03:07,990
 The second is also anything conditioned that has reached

52
00:03:07,990 --> 00:03:10,000
 the three moments beginning with arising and has ceased.

53
00:03:10,000 --> 00:03:16,690
 Something which has conditioned, which has come into being

54
00:03:16,690 --> 00:03:18,000
 and then disappeared.

55
00:03:18,000 --> 00:03:22,000
 So that also is called arisen.

56
00:03:22,000 --> 00:03:26,000
 It is having been and gone.

57
00:03:26,000 --> 00:03:29,000
 Now the third, arisen by opportunity made.

58
00:03:29,000 --> 00:03:33,000
 Now the past karma is called arisen by opportunity made.

59
00:03:33,000 --> 00:03:35,000
 It was really passed.

60
00:03:35,000 --> 00:03:38,000
 So although it is passed it is called arisen.

61
00:03:38,000 --> 00:03:43,000
 That means it is still with us or something like that.

62
00:03:43,000 --> 00:03:46,000
 Because it has stood.

63
00:03:46,000 --> 00:03:48,000
 That is the direct translation.

64
00:03:48,000 --> 00:03:52,000
 It has existed inhibiting the result of other karma.

65
00:03:52,000 --> 00:03:55,960
 And has made the opportunity for its own result to arise in

66
00:03:55,960 --> 00:03:57,000
 the future.

67
00:03:57,000 --> 00:04:00,000
 When there is karma and then it disappears,

68
00:04:00,000 --> 00:04:05,970
 then it leaves something like potential in the continuity

69
00:04:05,970 --> 00:04:09,000
 of beings to give results in the future.

70
00:04:09,000 --> 00:04:13,980
 So when it made the opportunity for its result to arise in

71
00:04:13,980 --> 00:04:15,000
 the future,

72
00:04:15,000 --> 00:04:19,000
 then it inhibits the result of other kammas.

73
00:04:19,000 --> 00:04:22,000
 And the second is the future result.

74
00:04:22,000 --> 00:04:25,000
 The future result is also called upana.

75
00:04:25,000 --> 00:04:27,000
 Although it has not yet arisen.

76
00:04:27,000 --> 00:04:31,000
 So although it has not yet arisen, it is called arisen.

77
00:04:31,000 --> 00:04:36,140
 Because when opportunity arises is made, it is sure to

78
00:04:36,140 --> 00:04:38,000
 arise in the future.

79
00:04:38,000 --> 00:04:43,000
 When karma is accumulated, I mean, karma is done,

80
00:04:43,000 --> 00:04:47,000
 then the fruit is sure to arise.

81
00:04:47,000 --> 00:04:53,000
 So the future result can be called arisen.

82
00:04:53,000 --> 00:04:57,000
 Although it is infected, it has not yet arisen.

83
00:04:57,000 --> 00:05:01,000
 The fourth arisen by having soil to grow in.

84
00:05:01,000 --> 00:05:05,000
 We are concerned with this, the fourth one.

85
00:05:05,000 --> 00:05:09,000
 So arisen by having soil to grow in.

86
00:05:09,000 --> 00:05:12,000
 That means unwholesome karma,

87
00:05:12,000 --> 00:05:16,560
 which has not been eradicated with respect to any given

88
00:05:16,560 --> 00:05:17,000
 soil.

89
00:05:17,000 --> 00:05:19,000
 Then what is soil here?

90
00:05:19,000 --> 00:05:22,360
 Soil here means the five aggregates in the three planes of

91
00:05:22,360 --> 00:05:23,000
 the coming,

92
00:05:23,000 --> 00:05:26,000
 which are the object of vipassana.

93
00:05:26,000 --> 00:05:29,000
 And what has soil means mental defilements,

94
00:05:29,000 --> 00:05:33,320
 which are capable of arising with respect to those aggreg

95
00:05:33,320 --> 00:05:34,000
ates.

96
00:05:34,000 --> 00:05:44,000
 So what the path eliminates is this kind of defilements,

97
00:05:44,000 --> 00:05:47,000
 arisen by having soil to grow in.

98
00:05:47,000 --> 00:05:50,540
 And here soil means the five aggregates in these planes of

99
00:05:50,540 --> 00:05:51,000
 the coming.

100
00:05:51,000 --> 00:05:54,000
 They are the object of vipassana meditation.

101
00:05:54,000 --> 00:05:57,000
 And what has soil means mental defilements,

102
00:05:57,000 --> 00:05:59,570
 which are capable of arising with respect to those aggreg

103
00:05:59,570 --> 00:06:00,000
ates.

104
00:06:00,000 --> 00:06:07,000
 That means, suppose I see an object, a desirable object,

105
00:06:07,000 --> 00:06:14,000
 and I do not practice vipassana towards that object.

106
00:06:14,000 --> 00:06:19,000
 So I take it to be beautiful.

107
00:06:19,000 --> 00:06:23,000
 Although at that moment I may have no attachment,

108
00:06:23,000 --> 00:06:27,000
 but since I have taken it to be beautiful,

109
00:06:27,000 --> 00:06:31,090
 I can have attachment with regard to that thing in the

110
00:06:31,090 --> 00:06:32,000
 future.

111
00:06:32,000 --> 00:06:36,490
 So that kind of attachment or mental defilement is what is

112
00:06:36,490 --> 00:06:37,000
 called,

113
00:06:37,000 --> 00:06:45,000
 which has soil to grow in.

114
00:06:45,000 --> 00:06:47,000
 So with regard to objects,

115
00:06:47,000 --> 00:06:53,000
 we have to practice vipassana meditation

116
00:06:53,000 --> 00:06:55,000
 so that we see their true nature,

117
00:06:55,000 --> 00:06:58,000
 so that we see that they are impermanent and so on.

118
00:06:58,000 --> 00:07:01,110
 When we have seen that, when we have practiced vipassana

119
00:07:01,110 --> 00:07:02,000
 towards that,

120
00:07:02,000 --> 00:07:08,580
 then the defilements are not said to be inherent in these

121
00:07:08,580 --> 00:07:10,000
 objects.

122
00:07:10,000 --> 00:07:17,000
 But with regard to objects which we fail to observe by vip

123
00:07:17,000 --> 00:07:18,000
assana,

124
00:07:18,000 --> 00:07:24,000
 there is always the possibility that defilements will arise

125
00:07:24,000 --> 00:07:26,000
 with regard to those things.

126
00:07:26,000 --> 00:07:34,070
 Those defilements which can arise through not having

127
00:07:34,070 --> 00:07:36,000
 observed by vipassana

128
00:07:36,000 --> 00:07:44,000
 are called having soil to grow in.

129
00:07:44,000 --> 00:07:49,860
 Actually, what the path eradicates is not the present

130
00:07:49,860 --> 00:07:52,000
 mental defilements,

131
00:07:52,000 --> 00:07:55,370
 not the past mental defilements, not the future mental def

132
00:07:55,370 --> 00:07:56,000
ilements,

133
00:07:56,000 --> 00:08:01,650
 but it is something like future, because some liability in

134
00:08:01,650 --> 00:08:05,000
 our continuity,

135
00:08:05,000 --> 00:08:09,000
 that is, when there are conditions, they can arise.

136
00:08:09,000 --> 00:08:15,810
 So that liability is what is eradicated by the path

137
00:08:15,810 --> 00:08:18,000
 consciousness,

138
00:08:18,000 --> 00:08:25,040
 and such reliability is just called here, those having the

139
00:08:25,040 --> 00:08:27,000
 soil to grow in.

140
00:08:27,000 --> 00:08:44,270
 That means, in my continuity of consciousness, they can

141
00:08:44,270 --> 00:08:46,000
 arise.

142
00:08:46,000 --> 00:08:51,000
 So they have my continuity as a soil to grow in,

143
00:08:51,000 --> 00:08:57,390
 because I have taken that object to be beautiful, to be

144
00:08:57,390 --> 00:09:00,000
 permanent, to be pleasant,

145
00:09:00,000 --> 00:09:08,000
 to be substantial or to be self.

146
00:09:08,000 --> 00:09:15,000
 So since I have taken this thing to be permanent and so on,

147
00:09:15,000 --> 00:09:22,000
 at any time the defilement can arise.

148
00:09:22,000 --> 00:09:28,000
 So that liability, those developments which can occur

149
00:09:28,000 --> 00:09:33,000
 through not being observed with vipassana,

150
00:09:33,000 --> 00:09:39,390
 what I call those that have soil to grow in, because it has

151
00:09:39,390 --> 00:09:41,000
 obtained,

152
00:09:41,000 --> 00:09:59,000
 it has got my continuity, my mind, to grow in, to arise in.

153
00:09:59,000 --> 00:10:06,000
 And that is not meant objectively.

154
00:10:06,000 --> 00:10:11,000
 That means having the soil to grow in.

155
00:10:11,000 --> 00:10:15,980
 Having the soil means having the soil not as an object, but

156
00:10:15,980 --> 00:10:20,000
 as a place to grow in, as a place.

157
00:10:20,000 --> 00:10:24,320
 That is also important, because if we say it is by way of

158
00:10:24,320 --> 00:10:26,000
 taking an object

159
00:10:26,000 --> 00:10:34,470
 that it has got the soil, then it can mean, say, there is

160
00:10:34,470 --> 00:10:37,000
 an arahant.

161
00:10:37,000 --> 00:10:41,000
 He said to be beautiful.

162
00:10:41,000 --> 00:10:48,530
 So a man saw the arahant, and then he had sexual desires

163
00:10:48,530 --> 00:10:51,000
 for that arahant.

164
00:10:51,000 --> 00:10:55,810
 I mean, he wanted that arahant to be his wife or something

165
00:10:55,810 --> 00:10:57,000
 like that.

166
00:10:57,000 --> 00:11:05,000
 And so even with regard to the body of an arahant,

167
00:11:05,000 --> 00:11:11,300
 mental defilement can arise in other persons by taking the

168
00:11:11,300 --> 00:11:14,000
 body of an arahant as an object.

169
00:11:14,000 --> 00:11:18,920
 So if we mean that having the soil means having the soil as

170
00:11:18,920 --> 00:11:20,000
 an object,

171
00:11:20,000 --> 00:11:28,520
 then it will mean that an arahant can eradicate mental def

172
00:11:28,520 --> 00:11:32,000
ilements of another person,

173
00:11:32,000 --> 00:11:40,000
 because that person has taken the arahant as an object,

174
00:11:40,000 --> 00:11:45,000
 and then he has attachment arise in his mind.

175
00:11:45,000 --> 00:11:53,000
 So having the soil does not mean having taken as an object,

176
00:11:53,000 --> 00:11:57,000
 but having the soil means having got somewhere, as a place

177
00:11:57,000 --> 00:12:06,000
 to grow in or to grow out of.

178
00:12:06,000 --> 00:12:10,000
 So like the elder who had obtained,

179
00:12:10,000 --> 00:12:17,000
 like the defilement that arose in the elder Mahatasya,

180
00:12:17,000 --> 00:12:25,000
 well, not yet. I think it is 806.

181
00:12:25,000 --> 00:12:29,120
 Like those that arose in the rich man's saurya with respect

182
00:12:29,120 --> 00:12:32,000
 to the aggregates of Mahatatya,

183
00:12:32,000 --> 00:12:38,000
 so he had wrong desires for Mahakachana.

184
00:12:38,000 --> 00:12:43,000
 And in the Brahmin student Nanda with respect to Upala Vana

185
00:12:43,000 --> 00:12:43,000
,

186
00:12:43,000 --> 00:12:57,000
 Upala Vana was an arahant, a nun, and Nanda was a dog.

187
00:12:57,000 --> 00:13:02,720
 Nanda fell in love with her, and so one day when she came

188
00:13:02,720 --> 00:13:08,000
 back from the sun,

189
00:13:08,000 --> 00:13:12,000
 then he raped the nun.

190
00:13:12,000 --> 00:13:23,210
 So having soil means not having soil by way of taking as an

191
00:13:23,210 --> 00:13:24,000
 object,

192
00:13:24,000 --> 00:13:28,000
 but by way of having a place, I think a base.

193
00:13:28,000 --> 00:13:35,740
 Because if we say the defilement which has the soil to grow

194
00:13:35,740 --> 00:13:36,000
 in

195
00:13:36,000 --> 00:13:42,700
 by way of taking object, then it may mean the defilement in

196
00:13:42,700 --> 00:13:45,000
 another person also.

197
00:13:45,000 --> 00:13:50,450
 And nobody can eradicate, even the voter cannot eradicate

198
00:13:50,450 --> 00:13:54,000
 the defilement in another person.

199
00:13:54,000 --> 00:14:01,500
 So I can eradicate defilement in my mind and not in another

200
00:14:01,500 --> 00:14:04,000
 person's mind.

201
00:14:04,000 --> 00:14:10,000
 So it is to be understood.

202
00:14:10,000 --> 00:14:16,550
 Having soil means having soil not as an object, not as

203
00:14:16,550 --> 00:14:18,000
 taking as an object,

204
00:14:18,000 --> 00:14:28,000
 but having it as its location or something like that.

205
00:14:28,000 --> 00:14:33,000
 And if what is called a reason by having soil to grow in,

206
00:14:33,000 --> 00:14:37,940
 no one could abandon the root of becoming because it would

207
00:14:37,940 --> 00:14:39,000
 be unabandonable.

208
00:14:39,000 --> 00:14:43,000
 That means because it belongs to another person.

209
00:14:43,000 --> 00:14:46,630
 But a reason by having soil should be understood subject

210
00:14:46,630 --> 00:14:47,000
ively

211
00:14:47,000 --> 00:14:49,000
 with respect to the basis for them in oneself.

212
00:14:49,000 --> 00:14:53,390
 That means they should be understood as having the place to

213
00:14:53,390 --> 00:14:58,000
 live or to exist, something like that.

214
00:14:58,000 --> 00:15:01,140
 For the defilements that are the root of the round are

215
00:15:01,140 --> 00:15:02,000
 inherent in one's own aggregate,

216
00:15:02,000 --> 00:15:05,000
 not fully understood by insight.

217
00:15:05,000 --> 00:15:09,000
 So if we do not practice we personally do much things,

218
00:15:09,000 --> 00:15:17,000
 then we do not fully understand these objects.

219
00:15:17,000 --> 00:15:21,700
 Fully understand means having seen their arising and

220
00:15:21,700 --> 00:15:23,000
 disappearing,

221
00:15:23,000 --> 00:15:36,000
 their characteristics and also being able to get rid of

222
00:15:36,000 --> 00:15:38,000
 major defilements with regard to them.

223
00:15:38,000 --> 00:15:47,000
 So for the defilements that are the root of the round,

224
00:15:47,000 --> 00:15:51,270
 the round are inherent in one's own aggregate and not fully

225
00:15:51,270 --> 00:15:54,740
 understood by insight from the instant those aggregates

226
00:15:54,740 --> 00:15:55,000
 arise.

227
00:15:55,000 --> 00:15:58,390
 And that is what should be understood as a reason by having

228
00:15:58,390 --> 00:16:03,000
 soil to grow in, in the sense of its being unabandoned.

229
00:16:03,000 --> 00:16:18,100
 So in brief what the path abandons or eliminates is just

230
00:16:18,100 --> 00:16:23,530
 the major defilements which would arise when there are

231
00:16:23,530 --> 00:16:24,000
 conditions,

232
00:16:24,000 --> 00:16:35,000
 which would arise because one has not seen them correctly,

233
00:16:35,000 --> 00:16:37,000
 one has not fully understood them.

234
00:16:37,000 --> 00:16:45,000
 So that liability is what the path consciousness eradicates

235
00:16:45,000 --> 00:16:45,000
,

236
00:16:45,000 --> 00:16:50,000
 and not the real mental defilements arising at the moment.

237
00:16:50,000 --> 00:16:52,820
 Because when there are any mental defilements there can be

238
00:16:52,820 --> 00:16:54,000
 no path consciousness,

239
00:16:54,000 --> 00:17:04,050
 and when there is path consciousness there can be no mental

240
00:17:04,050 --> 00:17:07,000
 defilements.

241
00:17:07,000 --> 00:17:13,740
 And then, if you understand this, the other procedures are

242
00:17:13,740 --> 00:17:15,000
 not difficult.

243
00:17:15,000 --> 00:17:22,620
 And then the other gives another set of four kinds of Upana

244
00:17:22,620 --> 00:17:25,000
, paragraph 89.

245
00:17:25,000 --> 00:17:27,480
 Besides these there are four other ways of classing a

246
00:17:27,480 --> 00:17:30,000
 reason, namely a reason as heavening,

247
00:17:30,000 --> 00:17:33,950
 a reason with apprehension of an object, a reason through

248
00:17:33,950 --> 00:17:38,000
 non-suppression, and a reason through non-abolition.

249
00:17:38,000 --> 00:17:41,600
 So a reason as heavening is the same as one, a reason as

250
00:17:41,600 --> 00:17:43,000
 actually occurring.

251
00:17:43,000 --> 00:17:51,980
 That means it is rightly in existence, possessing three

252
00:17:51,980 --> 00:17:54,000
 moments.

253
00:17:54,000 --> 00:17:57,640
 Now when an object has at some previous time come into

254
00:17:57,640 --> 00:18:02,000
 focus in the eye, etc., and defilement did not arise then,

255
00:18:02,000 --> 00:18:05,560
 but arose in full force later on simply because the object

256
00:18:05,560 --> 00:18:07,000
 had been apprehended,

257
00:18:07,000 --> 00:18:14,300
 that means taken firmly as permanent and so on, then that

258
00:18:14,300 --> 00:18:17,350
 defilement is called a reason with apprehension of an

259
00:18:17,350 --> 00:18:18,000
 object.

260
00:18:18,000 --> 00:18:20,370
 Like the defilement that arose in the elder Mahathis after

261
00:18:20,370 --> 00:18:23,000
 seeing the form of a person of the opposite sex

262
00:18:23,000 --> 00:18:28,700
 while wandering for arms in the village of Kaliya-ni, not n

263
00:18:28,700 --> 00:18:31,000
ah, Kaliya-ni.

264
00:18:31,000 --> 00:18:31,040
 So before there was no the mental defilement in elder Mah

265
00:18:31,040 --> 00:18:43,000
athisa, but he saw a person of the opposite sex

266
00:18:43,000 --> 00:18:49,000
 and then the defilement arose in his mind, like that.

267
00:18:49,000 --> 00:18:52,940
 As long as the defilement is not suppressed by either seren

268
00:18:52,940 --> 00:18:54,000
ity or insight,

269
00:18:54,000 --> 00:18:57,240
 though it may not have actually entered the conscious

270
00:18:57,240 --> 00:18:58,000
 continuity,

271
00:18:58,000 --> 00:19:02,000
 it is nevertheless called a reason through non-suppression.

272
00:19:02,000 --> 00:19:06,390
 That means it has not really arisen, but it is called a

273
00:19:06,390 --> 00:19:12,000
 reason because it has not been suppressed

274
00:19:12,000 --> 00:19:14,000
 because there is no cause to prevent its arising.

275
00:19:14,000 --> 00:19:17,150
 But even when they are suppressed by serenity or insight,

276
00:19:17,150 --> 00:19:20,000
 they are still called a reason through non-abolition

277
00:19:20,000 --> 00:19:23,250
 because of the necessity for the arising has not been

278
00:19:23,250 --> 00:19:27,000
 transcended unless they have been cut off by the path.

279
00:19:27,000 --> 00:19:35,710
 That is, those that are not cut off or abandoned by the

280
00:19:35,710 --> 00:19:38,000
 path consciousness,

281
00:19:38,000 --> 00:19:42,000
 so they are called a reason through non-abolition.

282
00:19:42,000 --> 00:19:45,000
 Now, abolition and suppression are different here.

283
00:19:45,000 --> 00:19:51,820
 Suppression means keeping them at bay for some time, and

284
00:19:51,820 --> 00:19:56,000
 abolition means eradicating altogether.

285
00:19:56,000 --> 00:20:03,290
 Now, like the elder who had obtained the eight attainments

286
00:20:03,290 --> 00:20:04,000
 and the defilements that arose in him

287
00:20:04,000 --> 00:20:07,340
 while he was going through the air on his hearing the sound

288
00:20:07,340 --> 00:20:09,000
 of a woman singing with the sweet voice

289
00:20:09,000 --> 00:20:13,180
 as she was gathering flowers and a grove of blossoming

290
00:20:13,180 --> 00:20:14,000
 trees.

291
00:20:14,000 --> 00:20:20,140
 So he had obtained eight attainments, so that means he has

292
00:20:20,140 --> 00:20:25,000
 suppressed the mental defilement by the eight attainments.

293
00:20:25,000 --> 00:20:28,050
 And the defilements that arose in him while he was going

294
00:20:28,050 --> 00:20:29,000
 through the air,

295
00:20:29,000 --> 00:20:32,980
 but he was going through the air, but he heard a woman

296
00:20:32,980 --> 00:20:35,000
 singing, plucking flowers,

297
00:20:35,000 --> 00:20:39,000
 and so defilements arose in him.

298
00:20:39,000 --> 00:20:45,270
 Such defilements are called and arisen through non-abol

299
00:20:45,270 --> 00:20:49,000
ition because they are not abolished,

300
00:20:49,000 --> 00:20:53,600
 they are not eradicated, they may arise when there are

301
00:20:53,600 --> 00:20:55,000
 conditions.

302
00:20:55,000 --> 00:20:57,000
 So there are these four kinds.

303
00:20:57,000 --> 00:21:01,390
 And the three kinds, namely arisen with apprehension of an

304
00:21:01,390 --> 00:21:03,000
 object, arisen through non-suppression,

305
00:21:03,000 --> 00:21:07,220
 and arisen through non-abolition should be understood as

306
00:21:07,220 --> 00:21:11,000
 included by four, arisen by having soil to grow in.

307
00:21:11,000 --> 00:21:20,400
 So the fourth of the first list corresponds to three of the

308
00:21:20,400 --> 00:21:23,000
 second list.

309
00:21:23,000 --> 00:21:26,590
 So as regards to kinds of reason, a reason stated the four

310
00:21:26,590 --> 00:21:29,000
 kinds, namely as actually occurring,

311
00:21:29,000 --> 00:21:35,100
 as been and gone, by opportunity made, and as happening,

312
00:21:35,100 --> 00:21:38,000
 cannot be abandoned by any knowledge

313
00:21:38,000 --> 00:21:41,000
 because they cannot be eliminated by the thoughts.

314
00:21:41,000 --> 00:21:44,790
 But the four kinds of reason, namely by having soil to grow

315
00:21:44,790 --> 00:21:47,000
 in with apprehension of an object,

316
00:21:47,000 --> 00:21:50,240
 through non-suppression and through non-abolition can all

317
00:21:50,240 --> 00:21:51,000
 be abandoned

318
00:21:51,000 --> 00:21:57,200
 because a given mundane or supranj

319
00:21:57,200 --> 00:21:59,290
 when it arises, nullifies a given one of these modes of

320
00:21:59,290 --> 00:22:00,000
 being arisen.

321
00:22:00,000 --> 00:22:09,140
 So when we say the path of abandon, then we mean the second

322
00:22:09,140 --> 00:22:10,000
 four.

323
00:22:10,000 --> 00:22:13,870
 I mean, the second four means here mentioned one of the

324
00:22:13,870 --> 00:22:17,000
 first and three of the second list.

325
00:22:17,000 --> 00:22:21,360
 So by having soil with apprehension of an object through

326
00:22:21,360 --> 00:22:25,000
 non-suppression and through non-abolition.

327
00:22:25,000 --> 00:22:34,110
 So what the path eradicates is the inherent tendencies or

328
00:22:34,110 --> 00:22:35,000
 latent tendencies

329
00:22:35,000 --> 00:22:41,000
 and not the ones that have arisen in the consciousness.

330
00:22:41,000 --> 00:22:44,500
 Because when they are in the consciousness, then we cannot

331
00:22:44,500 --> 00:22:51,000
 eradicate them simply because they are there.

332
00:22:51,000 --> 00:23:01,000
 That is why in the shoulders, especially in the commandries

333
00:23:01,000 --> 00:23:07,000
, it is said, anupada nirodha.

334
00:23:07,000 --> 00:23:13,000
 That means non-arising in the future is called cessation.

335
00:23:13,000 --> 00:23:18,980
 So cessation of, say, mental defilements means non-arising

336
00:23:18,980 --> 00:23:21,000
 of them in the future.

337
00:23:21,000 --> 00:23:25,970
 Because mental defilements arise and then they disappear by

338
00:23:25,970 --> 00:23:27,000
 themselves.

339
00:23:27,000 --> 00:23:30,840
 You don't have to do anything about them. They arise and

340
00:23:30,840 --> 00:23:32,000
 they disappear.

341
00:23:32,000 --> 00:23:36,580
 So the cessation of mental defilements really means not

342
00:23:36,580 --> 00:23:39,000
 letting them arise again.

343
00:23:39,000 --> 00:23:43,410
 So non-arising in the future is what is called cessation of

344
00:23:43,410 --> 00:23:45,000
 mental defilements.

345
00:23:45,000 --> 00:23:50,700
 And here also, when there is path, it has the power to

346
00:23:50,700 --> 00:24:00,000
 render them inactive or non-arising.

347
00:24:00,000 --> 00:24:03,500
 Now the four functions, the four functions in a single

348
00:24:03,500 --> 00:24:04,000
 moment,

349
00:24:04,000 --> 00:24:09,850
 it is said that the path does the four functions

350
00:24:09,850 --> 00:24:13,000
 simultaneously.

351
00:24:13,000 --> 00:24:17,000
 And what are the four?

352
00:24:17,000 --> 00:24:20,330
 Now at the times of penetrating to the truth, each one of

353
00:24:20,330 --> 00:24:23,000
 the four path knowledge is said to exercise four functions

354
00:24:23,000 --> 00:24:25,000
 in a single moment.

355
00:24:25,000 --> 00:24:32,000
 And these are four understandings of the first noble truth,

356
00:24:32,000 --> 00:24:34,000
 abandoning the second noble truth,

357
00:24:34,000 --> 00:24:38,710
 realising the third noble truth and developing the fourth

358
00:24:38,710 --> 00:24:40,000
 noble truth.

359
00:24:40,000 --> 00:24:47,150
 So at one moment, the path consciousness or path knowledge

360
00:24:47,150 --> 00:24:51,000
 exercises these four functions.

361
00:24:51,000 --> 00:24:56,490
 Not one by one, but simultaneously these four functions are

362
00:24:56,490 --> 00:24:57,000
 done.

363
00:24:57,000 --> 00:24:59,760
 For this is said by the agent, just as the lamp performs

364
00:24:59,760 --> 00:25:01,000
 four functions and so on.

365
00:25:01,000 --> 00:25:11,000
 So the simile of the lamp is brought here.

366
00:25:11,000 --> 00:25:15,260
 And then another method, 95, that's the sun when it arises

367
00:25:15,260 --> 00:25:17,000
 performs four functions and so on.

368
00:25:17,000 --> 00:25:22,000
 And then another method, it both performs four functions.

369
00:25:22,000 --> 00:25:29,000
 So these similes are given to illustrate the four functions

370
00:25:29,000 --> 00:25:39,000
 stand by path consciousness simultaneously.

371
00:25:39,000 --> 00:25:46,610
 And then in paragraph 97, so when his knowledge occurs with

372
00:25:46,610 --> 00:25:48,530
 the four functions in a single moment at the time of

373
00:25:48,530 --> 00:25:50,000
 penetrating the four truths,

374
00:25:50,000 --> 00:25:54,060
 then the four truths have a single penetration at the

375
00:25:54,060 --> 00:25:57,000
 centre of fruitness in 16 ways, as it is said.

376
00:25:57,000 --> 00:26:06,300
 So there are 16 ways or 16 meanings mentioned here, four

377
00:26:06,300 --> 00:26:09,000
 for each truth.

378
00:26:09,000 --> 00:26:12,400
 So how is there a single penetration of the four truths in

379
00:26:12,400 --> 00:26:14,000
 the sense of trueness?

380
00:26:14,000 --> 00:26:16,830
 There is a single penetration of the four truths in the

381
00:26:16,830 --> 00:26:19,000
 sense of trueness in 16 aspects.

382
00:26:19,000 --> 00:26:22,260
 Suffering has the meaning of oppressing, meaning of being

383
00:26:22,260 --> 00:26:25,000
 formed, meaning of burning or torment,

384
00:26:25,000 --> 00:26:28,940
 and meaning of change. These are the four meanings of the

385
00:26:28,940 --> 00:26:31,000
 first noble truth.

386
00:26:31,000 --> 00:26:34,720
 And then the four meanings of second noble truth, meaning

387
00:26:34,720 --> 00:26:38,550
 of accumulation, meaning of source, meaning of bondage, and

388
00:26:38,550 --> 00:26:41,000
 meaning of impediment.

389
00:26:41,000 --> 00:26:46,720
 And the four meanings of the third is the meaning of escape

390
00:26:46,720 --> 00:26:50,840
, meaning of seclusion, meaning of being not formed, and

391
00:26:50,840 --> 00:26:54,000
 meaning of deathlessness.

392
00:26:54,000 --> 00:26:57,470
 And the meaning of the fourth noble truth is meaning of

393
00:26:57,470 --> 00:27:01,370
 cause, meaning of outlet, meaning of cause, meaning of

394
00:27:01,370 --> 00:27:04,000
 seeing, and meaning of dominance.

395
00:27:04,000 --> 00:27:14,000
 So these are called the 16 ways of the four noble truths.

396
00:27:14,000 --> 00:27:17,000
 And paragraph 98 raises a question. Here it may be asked,

397
00:27:17,000 --> 00:27:21,260
 since there are other meanings of suffering, etc., too,

398
00:27:21,260 --> 00:27:22,000
 such as indices,

399
00:27:22,000 --> 00:27:25,410
 a tumor, and so on, why then are only four mentioned for

400
00:27:25,410 --> 00:27:26,000
 each?

401
00:27:26,000 --> 00:27:33,420
 Now, if you go back to chapter 20, paragraph 18, you'll

402
00:27:33,420 --> 00:27:38,690
 find that there are 40 ways of looking at things as imper

403
00:27:38,690 --> 00:27:40,000
manent and so on.

404
00:27:40,000 --> 00:27:47,000
 So the first noble truth has more than four meanings.

405
00:27:47,000 --> 00:27:52,120
 The meaning of disease, the meaning of a tumor, and so on.

406
00:27:52,120 --> 00:27:55,000
 So why are they not taken, and only these four are taken?

407
00:27:55,000 --> 00:27:58,790
 Now, we answered that in this context it is because of what

408
00:27:58,790 --> 00:28:02,430
 is evident through seeing the other three truths in each

409
00:28:02,430 --> 00:28:03,000
 case.

410
00:28:03,000 --> 00:28:05,590
 Firstly, in the passage beginning here, and what is

411
00:28:05,590 --> 00:28:08,750
 knowledge of suffering, it is the understanding, the act of

412
00:28:08,750 --> 00:28:12,000
 understanding that arises contingent upon suffering.

413
00:28:12,000 --> 00:28:14,910
 Knowledge of the truth is presented as having a single

414
00:28:14,910 --> 00:28:17,000
 truth as its object individually.

415
00:28:17,000 --> 00:28:20,590
 But in the passage beginning, because he who sees suffering

416
00:28:20,590 --> 00:28:24,260
 sees also its origin, it is presented as accomplishing its

417
00:28:24,260 --> 00:28:26,580
 function with respect to the other three truths

418
00:28:26,580 --> 00:28:30,000
 simultaneously, with its making one of them its object.

419
00:28:30,000 --> 00:28:38,990
 So sometimes it is described as seeing one each at a time.

420
00:28:38,990 --> 00:28:45,330
 But in other passages, the seeing is presented as occurring

421
00:28:45,330 --> 00:28:50,000
 at the same time, seeing of four at the same time.

422
00:28:50,000 --> 00:28:54,770
 Now, as regards these two contexts, when firstly knowledge

423
00:28:54,770 --> 00:28:59,390
 makes each truth its object seemingly, then suffering has

424
00:28:59,390 --> 00:29:03,470
 the characteristic of accomplishing as its individual

425
00:29:03,470 --> 00:29:05,000
 essence and so on.

426
00:29:05,000 --> 00:29:15,180
 In these paragraphs, we should strike out those in the

427
00:29:15,180 --> 00:29:21,600
 square brackets. It is inserted by the translator, and it

428
00:29:21,600 --> 00:29:26,000
 is not warranted by the sub-commentary.

429
00:29:26,000 --> 00:29:36,000
 Paragraph 99. 99, 100, 101, 102.

430
00:29:36,000 --> 00:29:38,000
 All those brackets.

431
00:29:38,000 --> 00:29:46,000
 After likewise. Three paragraphs after likewise.

432
00:29:46,000 --> 00:29:56,000
 99, just 100. Sorry, 99 also. After then, when suffering

433
00:29:56,000 --> 00:29:56,000
 has made the object.

434
00:29:56,000 --> 00:29:58,000
 No.

435
00:29:58,000 --> 00:30:02,960
 If suffering has made the object, then the only thing it

436
00:30:02,960 --> 00:30:08,070
 will see is the first meaning of the suffering and not the

437
00:30:08,070 --> 00:30:09,000
 others.

438
00:30:09,000 --> 00:30:17,270
 But what the other is explaining here is that when you see

439
00:30:17,270 --> 00:30:23,890
 the second noble truth, noble truth of origin, then the

440
00:30:23,890 --> 00:30:29,640
 sense of being formed of the first noble truth becomes

441
00:30:29,640 --> 00:30:31,000
 evident.

442
00:30:31,000 --> 00:30:36,040
 So when you see the second truth, then one meaning of the

443
00:30:36,040 --> 00:30:39,000
 first truth becomes evident.

444
00:30:39,000 --> 00:30:42,630
 And when you see the third noble truth, another meaning of

445
00:30:42,630 --> 00:30:45,000
 the first noble truth becomes evident.

446
00:30:45,000 --> 00:30:47,470
 And when you see the fourth noble truth, yet another

447
00:30:47,470 --> 00:30:50,000
 meaning of the first noble truth becomes evident.

448
00:30:50,000 --> 00:30:54,000
 That is why these four are mentioned here.

449
00:30:54,000 --> 00:31:00,100
 So when you see the first noble truth, then the meaning of

450
00:31:00,100 --> 00:31:03,000
 oppression is evident.

451
00:31:03,000 --> 00:31:06,650
 When you see the second noble truth, the meaning of being

452
00:31:06,650 --> 00:31:10,000
 formed of the first noble truth becomes evident.

453
00:31:10,000 --> 00:31:13,020
 When you see the third noble truth, the meaning of burning

454
00:31:13,020 --> 00:31:16,000
 or torment of the first noble truth becomes evident.

455
00:31:16,000 --> 00:31:19,600
 And when you see the fourth noble truth, the meaning of the

456
00:31:19,600 --> 00:31:23,000
 first noble truth that is of change becomes evident.

457
00:31:23,000 --> 00:31:34,000
 That is why these four meanings are given.

458
00:31:34,000 --> 00:31:43,970
 So by their own seeing them individually and also through

459
00:31:43,970 --> 00:31:50,990
 seeing other truths, the meanings of the given truth

460
00:31:50,990 --> 00:31:53,000
 becomes evident.

461
00:31:53,000 --> 00:31:59,000
 So these paragraphs explain this meaning.

462
00:31:59,000 --> 00:32:02,980
 So we do not need when suffering is made the object, when

463
00:32:02,980 --> 00:32:06,440
 origin is made the object, when cessation is made the

464
00:32:06,440 --> 00:32:13,000
 object, when the path is made the object.

465
00:32:13,000 --> 00:32:19,330
 And then in paragraph 99, an example is given that as the

466
00:32:19,330 --> 00:32:25,010
 beauty or sundry's ugliness did to the venerable Nanda

467
00:32:25,010 --> 00:32:29,000
 through seeing the celestial nymphs.

468
00:32:29,000 --> 00:32:34,000
 Now, sundry or beauty was said to be very beautiful.

469
00:32:34,000 --> 00:32:40,270
 She was very like a beauty queen. So she was very beautiful

470
00:32:40,270 --> 00:32:45,000
 and so Nanda was so much in love of her.

471
00:32:45,000 --> 00:32:51,930
 But Buddha wanted to teach him a lesson. So Buddha took him

472
00:32:51,930 --> 00:32:57,000
 to the celestial world and showed the celestial nymphs.

473
00:32:57,000 --> 00:33:01,740
 So after seeing the celestial nymphs, he asked who is more

474
00:33:01,740 --> 00:33:05,000
 beautiful sundry or celestial nymphs.

475
00:33:05,000 --> 00:33:09,070
 Then he said, "Oh, sundry is like a sheep monkey we saw on

476
00:33:09,070 --> 00:33:12,000
 our way here or something like that."

477
00:33:12,000 --> 00:33:16,470
 So after seeing the nymph, sundry becomes ugly. But

478
00:33:16,470 --> 00:33:22,000
 actually sundry was a beautiful woman.

479
00:33:22,000 --> 00:33:26,280
 So here also cooling path removes the burning of the defact

480
00:33:26,280 --> 00:33:27,000
ment.

481
00:33:27,000 --> 00:33:31,480
 And so suffering sense of burning becomes evident through

482
00:33:31,480 --> 00:33:33,000
 seeing the path.

483
00:33:33,000 --> 00:33:37,770
 When you see the cool one, then the burning of the another

484
00:33:37,770 --> 00:33:40,000
 thing becomes evident.

485
00:33:40,000 --> 00:33:44,000
 Okay.

486
00:33:44,000 --> 00:33:54,000
 That should be the end of today.

487
00:33:54,000 --> 00:34:09,000
 [clears throat]

488
00:34:09,000 --> 00:34:32,000
 [silence]

