1
00:00:00,000 --> 00:00:07,000
 Hello everyone. Happy New Year.

2
00:00:07,000 --> 00:00:18,200
 So I thought today would be an opportunity to talk about

3
00:00:18,200 --> 00:00:18,200
 meditation from the perspective

4
00:00:25,200 --> 00:00:32,200
 of providing something new. How meditation relates to

5
00:00:32,200 --> 00:00:32,200
 change. How meditation brings about

6
00:00:32,200 --> 00:00:51,960
 change. New Year is a great opportunity to talk about

7
00:00:51,960 --> 00:00:51,960
 meditation.

8
00:00:51,960 --> 00:00:58,960
 Out with the old, in with the new. And meditation has a lot

9
00:00:58,960 --> 00:00:58,960
 to say about change and about bringing

10
00:01:16,160 --> 00:01:23,160
 in the new and taking out the old. Because really all

11
00:01:23,160 --> 00:01:23,160
 meditation can be described in that way,

12
00:01:23,160 --> 00:01:32,270
 pretty much. Meditation being always related somehow to

13
00:01:32,270 --> 00:01:35,440
 mental development and mental development

14
00:01:44,520 --> 00:01:49,460
 involving habits, the change in habits, the cultivation of

15
00:01:49,460 --> 00:01:51,520
 new habits and the tearing

16
00:01:51,520 --> 00:02:01,470
 down, abandoning, weakening and doing away with old habits.

17
00:02:01,470 --> 00:02:01,480
 Usually for the better. And

18
00:02:10,860 --> 00:02:15,950
 so there are many meditations which promise the cultivation

19
00:02:15,950 --> 00:02:17,860
 of specific habits. Loving

20
00:02:17,860 --> 00:02:26,560
 kindness is a good example. There's a lot of talk in

21
00:02:26,560 --> 00:02:31,340
 Buddhism about the practice of

22
00:02:31,340 --> 00:02:35,230
 loving kindness. The Buddha recommended it himself

23
00:02:35,230 --> 00:02:38,080
 according to the text. Many Buddhist

24
00:02:38,080 --> 00:02:41,460
 teachers talk about this. So here's an example of the

25
00:02:41,460 --> 00:02:44,440
 cultivation of a new habit. The cultivation

26
00:02:44,440 --> 00:02:49,660
 of the new. Something good that is going to be new to the

27
00:02:49,660 --> 00:02:51,440
 practitioner. Maybe you've always

28
00:02:51,440 --> 00:02:57,860
 found it difficult to be kind to others. You find it too

29
00:02:57,860 --> 00:03:01,240
 easy to get upset, annoyed, frustrated,

30
00:03:03,840 --> 00:03:08,250
 hurtful, towards others. And so the cultivation of loving

31
00:03:08,250 --> 00:03:10,840
 kindness changes that. It helps

32
00:03:10,840 --> 00:03:22,080
 you cultivate this habit of being nice and kind to people.

33
00:03:22,080 --> 00:03:22,080
 A lot of meditation is just

34
00:03:22,080 --> 00:03:29,100
 designed to lead to calm. So there's some abstract object

35
00:03:29,100 --> 00:03:32,400
 or concept that one focuses

36
00:03:34,040 --> 00:03:37,780
 on or maybe just the breath. And the goal is to calm the

37
00:03:37,780 --> 00:03:41,040
 mind down, to enter into states

38
00:03:41,040 --> 00:03:46,730
 of intense and profound concentration. A new habit, a new

39
00:03:46,730 --> 00:03:50,080
 ability in the mind, a new quality

40
00:03:50,080 --> 00:03:55,110
 of mind. For many people it's simply the continuation of

41
00:03:55,110 --> 00:03:58,920
 practice that they've already done. And

42
00:03:58,920 --> 00:04:02,610
 it's simply the continuation of practice that they've

43
00:04:02,610 --> 00:04:04,760
 already undertaken in the past

44
00:04:04,760 --> 00:04:10,880
 year but always for the purpose of something new, something

45
00:04:10,880 --> 00:04:11,760
 more. And that's important.

46
00:04:11,760 --> 00:04:16,190
 It's important to never think of meditation as just a hobby

47
00:04:16,190 --> 00:04:18,240
 or something that you do to

48
00:04:18,240 --> 00:04:23,080
 maintain some sort of state. But as something to cultivate

49
00:04:23,080 --> 00:04:25,240
 a higher and a greater and a

50
00:04:27,880 --> 00:04:34,250
 better version of yourself. Now mindfulness meditation, as

51
00:04:34,250 --> 00:04:34,880
 I've said before, and just

52
00:04:34,880 --> 00:04:41,800
 to be clear about it, is a little bit different. It's

53
00:04:41,800 --> 00:04:46,360
 different in that not only does it, of

54
00:04:46,360 --> 00:04:50,990
 course, involve the cultivation of new habits and doing

55
00:04:50,990 --> 00:04:53,760
 away with old habits, but it also

56
00:04:53,760 --> 00:04:58,370
 involves a new understanding of our habits. It involves a

57
00:04:58,370 --> 00:05:00,760
 deeper, the cultivation of a

58
00:05:00,760 --> 00:05:06,790
 deeper and therefore new ability to discern what habits are

59
00:05:06,790 --> 00:05:09,640
 bad and what habits are good

60
00:05:09,640 --> 00:05:16,250
 without having to rely on a teacher to say, "Practice this

61
00:05:16,250 --> 00:05:20,000
 way and the results are good."

62
00:05:20,600 --> 00:05:25,370
 It relies rather on your own investigation and your own new

63
00:05:25,370 --> 00:05:27,600
 ability to understand and

64
00:05:27,600 --> 00:05:34,860
 to differentiate between the good and the bad. So

65
00:05:34,860 --> 00:05:36,560
 mindfulness is always about some sort

66
00:05:36,560 --> 00:05:43,630
 of objectivity rather than judging or reacting or trying to

67
00:05:43,630 --> 00:05:46,920
 fix the problems in the mind.

68
00:05:48,760 --> 00:05:52,240
 It's about observing and understanding, watching how the

69
00:05:52,240 --> 00:05:55,760
 mind works, and in many ways learning

70
00:05:55,760 --> 00:06:03,710
 to do away with the habits of judgment, the habits of

71
00:06:03,710 --> 00:06:09,040
 reaction. So rather than having

72
00:06:11,440 --> 00:06:15,840
 fixed ideas about things, it's about studying those fixed

73
00:06:15,840 --> 00:06:18,440
 ideas and beliefs, even the good

74
00:06:18,440 --> 00:06:23,360
 ones, and getting a deeper and new appreciation for the

75
00:06:23,360 --> 00:06:26,000
 reality of the situation. Something

76
00:06:26,000 --> 00:06:31,130
 is good not just because I or the Buddha or you say it's

77
00:06:31,130 --> 00:06:34,080
 good, but it's good because it

78
00:06:34,080 --> 00:06:38,190
 leads to happiness and it leads to peace. And there's no

79
00:06:38,190 --> 00:06:40,320
 doubt about that for someone

80
00:06:40,320 --> 00:06:43,910
 who has seen through the practice of mindfulness to be the

81
00:06:43,910 --> 00:06:46,120
 case. You don't have to believe

82
00:06:46,120 --> 00:06:52,500
 anything once you see for yourself. So just a short video.

83
00:06:52,500 --> 00:06:53,120
 I thought it would be a good

84
00:06:53,120 --> 00:06:57,610
 opportunity to encourage not just the practice of

85
00:06:57,610 --> 00:07:01,640
 meditation but the practice of mindfulness

86
00:07:01,640 --> 00:07:07,510
 and to stress again how meditation very much has to do with

87
00:07:07,510 --> 00:07:10,640
 the cultivation of something

88
00:07:10,640 --> 00:07:15,140
 new, the arising of something new. It shouldn't be about

89
00:07:15,140 --> 00:07:17,720
 reaffirming your beliefs. It's often

90
00:07:17,720 --> 00:07:21,330
 surprising for long time Buddhists or people who have

91
00:07:21,330 --> 00:07:23,760
 studied the Buddhist texts, even

92
00:07:23,760 --> 00:07:27,580
 for people who have practiced other types of meditation,

93
00:07:27,580 --> 00:07:29,400
 the things that they learn

94
00:07:29,400 --> 00:07:34,160
 in mindfulness meditation practice are new even to seasoned

95
00:07:34,160 --> 00:07:36,400
 meditation practitioners.

96
00:07:36,400 --> 00:07:44,150
 Should always be surprising. Mindfulness meditation, if you

97
00:07:44,150 --> 00:07:47,680
're practicing it properly, should always

98
00:07:47,680 --> 00:07:51,690
 show you something new. That's what it's meant to do. When

99
00:07:51,690 --> 00:07:54,680
 you're practicing it, it's increasing

100
00:07:54,680 --> 00:07:57,140
 the clarity of your mind. So it's always going to show you

101
00:07:57,140 --> 00:07:58,480
 something that you didn't quite

102
00:07:58,480 --> 00:08:03,830
 see before or wasn't quite clear to you before. So the

103
00:08:03,830 --> 00:08:05,480
 cultivation of good habits and the eradication

104
00:08:05,480 --> 00:08:13,600
 of bad habits becomes somewhat of a moot point because it's

105
00:08:13,600 --> 00:08:17,840
 a default. By default, one doesn't

106
00:08:17,840 --> 00:08:23,890
 engage in the cultivation of bad habits when one is clear

107
00:08:23,890 --> 00:08:27,200
 beyond any doubt that they're

108
00:08:27,960 --> 00:08:33,280
 bad. And the cultivation of good habits comes without any

109
00:08:33,280 --> 00:08:34,960
 second thought or uncertainty when

110
00:08:34,960 --> 00:08:45,340
 one sees for oneself that they're good. So wish you all a

111
00:08:45,340 --> 00:08:47,120
 Happy New Year. A New Year

112
00:08:47,120 --> 00:08:51,280
 that is filled with peace, happiness, freedom from

113
00:08:51,280 --> 00:08:54,440
 suffering, most importantly one that

114
00:08:54,440 --> 00:08:59,810
 involves an increase and continuation of the practice of

115
00:08:59,810 --> 00:09:01,440
 mindfulness. Thank you, have a

116
00:09:01,440 --> 00:09:03,080
 good day.

117
00:09:03,080 --> 00:09:19,160
 [

