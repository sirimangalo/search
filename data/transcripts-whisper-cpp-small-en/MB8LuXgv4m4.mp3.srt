1
00:00:00,000 --> 00:00:05,740
 Why do some people simply possess brilliant minds, are born

2
00:00:05,740 --> 00:00:07,000
 geniuses, etc.?

3
00:00:07,000 --> 00:00:11,000
 Actually, do you think geniuses are born or made?

4
00:00:11,000 --> 00:00:13,800
 May one of the reasons be their previous life in some of

5
00:00:13,800 --> 00:00:15,000
 the higher realms?

6
00:00:15,000 --> 00:00:21,950
 Well, not even necessarily in the higher realms, but some

7
00:00:21,950 --> 00:00:25,000
 aspiration in the past.

8
00:00:25,000 --> 00:00:29,670
 Definitely, quality of the mind, even inclination for

9
00:00:29,670 --> 00:00:31,000
 certain things,

10
00:00:31,000 --> 00:00:34,960
 so people who are inclined towards music, people who are

11
00:00:34,960 --> 00:00:37,000
 inclined towards science.

12
00:00:37,000 --> 00:00:41,320
 It's interesting, last night I went to one of my supporters

13
00:00:41,320 --> 00:00:42,000
' houses,

14
00:00:42,000 --> 00:00:46,500
 and their meditators and their two daughters are also medit

15
00:00:46,500 --> 00:00:47,000
ators.

16
00:00:47,000 --> 00:00:50,450
 And talking to their two daughters about their university

17
00:00:50,450 --> 00:00:51,000
 studies

18
00:00:51,000 --> 00:00:55,160
 and comparing, just chatting about university and about the

19
00:00:55,160 --> 00:00:56,000
 way we learn,

20
00:00:56,000 --> 00:01:01,250
 the one daughter is very, very much math oriented, pure

21
00:01:01,250 --> 00:01:02,000
 math oriented,

22
00:01:02,000 --> 00:01:07,000
 so she doesn't like practical applications.

23
00:01:07,000 --> 00:01:10,000
 There's these two sides of science studies.

24
00:01:10,000 --> 00:01:12,680
 One is people who like the pure stuff, where it's just

25
00:01:12,680 --> 00:01:13,000
 theory,

26
00:01:13,000 --> 00:01:16,000
 doesn't have any basis in reality because reality is muddy,

27
00:01:16,000 --> 00:01:21,000
 reality is organic and there's uncertainty in it and so on.

28
00:01:21,000 --> 00:01:24,810
 And then the other side hates that stuff and loves getting

29
00:01:24,810 --> 00:01:25,000
 the hands-on

30
00:01:25,000 --> 00:01:30,830
 kind of organic natural stuff that has uncertainty and is

31
00:01:30,830 --> 00:01:32,000
 practical.

32
00:01:32,000 --> 00:01:34,840
 And we're talking about it, and it's quite clear that they

33
00:01:34,840 --> 00:01:36,000
're two very, very different people.

34
00:01:36,000 --> 00:01:39,000
 So science understands that, and they have this left brain,

35
00:01:39,000 --> 00:01:40,000
 right brain kind of thing,

36
00:01:40,000 --> 00:01:43,000
 genetics and the idea that people are different.

37
00:01:43,000 --> 00:01:46,850
 But from a Buddhist point of view, it's also interesting to

38
00:01:46,850 --> 00:01:48,000
 see how different human beings,

39
00:01:48,000 --> 00:01:52,350
 how different two daughters can be, you know, two siblings

40
00:01:52,350 --> 00:01:54,000
 can be.

41
00:01:54,000 --> 00:01:57,000
 And obviously we see that in people.

42
00:01:57,000 --> 00:01:59,400
 So we're born differently and we would attribute this at

43
00:01:59,400 --> 00:02:05,000
 least partially to past karma.

44
00:02:05,000 --> 00:02:07,880
 But the idea of the quality of brilliance, like some people

45
00:02:07,880 --> 00:02:09,000
 have great memory,

46
00:02:09,000 --> 00:02:13,000
 I have great memory, it just happens to be a trait.

47
00:02:13,000 --> 00:02:19,000
 And this other monk who ordained after me,

48
00:02:19,000 --> 00:02:23,140
 but he's been a meditation teacher for quite some time in

49
00:02:23,140 --> 00:02:24,000
 our tradition,

50
00:02:24,000 --> 00:02:27,000
 he has a terrible memory and yet he's a brilliant teacher

51
00:02:27,000 --> 00:02:32,000
 and he's now, I think, world-renowned, he's a great guy.

52
00:02:32,000 --> 00:02:35,400
 He's also on YouTube a little bit. People put his videos on

53
00:02:35,400 --> 00:02:36,000
 offer.

54
00:02:36,000 --> 00:02:39,000
 It's just real, but terrible memory and he admits it.

55
00:02:39,000 --> 00:02:44,390
 So we're comparing this and it's just interesting how we

56
00:02:44,390 --> 00:02:48,000
 have different qualities of mind, different abilities.

57
00:02:48,000 --> 00:02:54,550
 And so definitely we attribute this, we speculate on the

58
00:02:54,550 --> 00:02:58,000
 fact that this is probably something to do from the past.

59
00:02:58,000 --> 00:03:00,980
 Karma really isn't, you know, that's not really the

60
00:03:00,980 --> 00:03:03,000
 important questions about karma that we ask.

61
00:03:03,000 --> 00:03:08,120
 Most important questions or knowledge that we try to,

62
00:03:08,120 --> 00:03:10,000
 understanding we try to cultivate,

63
00:03:10,000 --> 00:03:12,980
 is about karma right here and now, what it's doing to us,

64
00:03:12,980 --> 00:03:14,000
 how it's changing us.

65
00:03:14,000 --> 00:03:20,090
 The idea that you might be reborn in a better place, in a

66
00:03:20,090 --> 00:03:23,210
 better way, is kind of incentive to help you do that, to

67
00:03:23,210 --> 00:03:25,000
 help you understand.

68
00:03:25,000 --> 00:03:29,000
 If people knew what their actions were leading them to,

69
00:03:29,000 --> 00:03:31,960
 then they'd be a lot more concerned about learning about

70
00:03:31,960 --> 00:03:34,000
 them, about studying about them.

71
00:03:34,000 --> 00:03:37,210
 So the important thing is that you come to the present

72
00:03:37,210 --> 00:03:39,000
 moment and study your karma.

73
00:03:39,000 --> 00:03:42,620
 But by telling people about past and future lives, it helps

74
00:03:42,620 --> 00:03:45,000
 them see the importance of doing that.

75
00:03:45,000 --> 00:03:50,720
 So if there is a future life and if your karma, your state

76
00:03:50,720 --> 00:03:53,000
 of mind is going to influence that,

77
00:03:53,000 --> 00:03:57,090
 gee, you'd better come and look and see whether what you're

78
00:03:57,090 --> 00:04:01,670
 doing is the right thing to do and learn more about how to

79
00:04:01,670 --> 00:04:03,000
 do the right things.

80
00:04:03,000 --> 00:04:07,030
 So it's a lot more basic, preliminary stuff to talk about

81
00:04:07,030 --> 00:04:11,000
 karma in terms of past lives, future lives and so on.

82
00:04:11,000 --> 00:04:17,000
 The ultimate goal is to understand karma as it works here

83
00:04:17,000 --> 00:04:18,000
 and now,

84
00:04:18,000 --> 00:04:22,240
 how our bad deeds are making us bad people, are corrupting

85
00:04:22,240 --> 00:04:23,000
 our minds,

86
00:04:23,000 --> 00:04:25,810
 and how our good deeds are purifying our minds, are making

87
00:04:25,810 --> 00:04:32,980
 us calmer, more peaceful, more gentle and beneficial beings

88
00:04:32,980 --> 00:04:33,000
.

89
00:04:33,000 --> 00:04:38,000
 But yeah, so definitely place a place apart.

90
00:04:38,000 --> 00:04:52,010
 It's most likely to play a part, it seems reasonable to say

91
00:04:52,010 --> 00:04:53,000
 that,

92
00:04:53,000 --> 00:04:58,040
 and that's the Buddhist theory, is that past lives do

93
00:04:58,040 --> 00:05:02,720
 condition our lives in the present and present will

94
00:05:02,720 --> 00:05:04,000
 condition us.

