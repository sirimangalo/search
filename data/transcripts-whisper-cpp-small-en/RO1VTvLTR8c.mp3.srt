1
00:00:00,000 --> 00:00:04,400
 What if the Buddha was merely highly advanced in a

2
00:00:04,400 --> 00:00:07,000
 particular spiritual technique?

3
00:00:07,000 --> 00:00:10,490
 It seems in order to reap the benefits of training you have

4
00:00:10,490 --> 00:00:12,000
 to put everything into it.

5
00:00:12,000 --> 00:00:17,020
 Wouldn't this same amount of effort bring success in any

6
00:00:17,020 --> 00:00:18,000
 field?

7
00:00:18,000 --> 00:00:22,610
 Yeah, in one sense, yes. The Buddha taught the four "ithi-p

8
00:00:22,610 --> 00:00:27,000
ada", which are the roads or the paths to success.

9
00:00:27,000 --> 00:00:31,000
 One way of translating them. So whatever you want to do,

10
00:00:31,000 --> 00:00:34,000
 you have to practice these four "ithi-pada".

11
00:00:34,000 --> 00:00:39,960
 You're welcome. Obviously you're welcome to have that sort

12
00:00:39,960 --> 00:00:41,000
 of a theory.

13
00:00:41,000 --> 00:00:44,160
 The Buddha was just someone who was highly developed in a

14
00:00:44,160 --> 00:00:45,000
 specific path.

15
00:00:45,000 --> 00:00:47,730
 We had this question on the Ask form, someone was talking

16
00:00:47,730 --> 00:00:49,000
 about the pointlessness.

17
00:00:49,000 --> 00:00:53,000
 This is a really neat concept to explore because of course

18
00:00:53,000 --> 00:00:55,000
 it's very volatile to start talking about

19
00:00:55,000 --> 00:00:58,000
 life as being meaningless and pointless.

20
00:00:58,000 --> 00:01:02,180
 I was thinking about it and I do stand by my answer for

21
00:01:02,180 --> 00:01:03,000
 what it's worth.

22
00:01:03,000 --> 00:01:06,600
 It's the meaningless that gives us power. You can do

23
00:01:06,600 --> 00:01:08,000
 anything you want.

24
00:01:08,000 --> 00:01:10,230
 You want to follow this path, follow this path. You want to

25
00:01:10,230 --> 00:01:11,000
 follow that path, follow that path.

26
00:01:11,000 --> 00:01:13,600
 You want to become king of the world, follow the path to

27
00:01:13,600 --> 00:01:15,000
 become king of the world.

28
00:01:15,000 --> 00:01:19,500
 You want to follow the path to become God, follow the path

29
00:01:19,500 --> 00:01:20,000
 to become God.

30
00:01:20,000 --> 00:01:24,000
 You want to go to hell, follow the path to go to hell.

31
00:01:24,000 --> 00:01:26,520
 There's paths for everything. There's no purpose. You make

32
00:01:26,520 --> 00:01:27,000
 your purpose.

33
00:01:27,000 --> 00:01:32,000
 You want to be this, be this. You want to be that, be that.

34
00:01:32,000 --> 00:01:35,950
 So it is fair in one sense to say that the Buddha was just

35
00:01:35,950 --> 00:01:38,000
 the teacher of another path.

36
00:01:38,000 --> 00:01:45,020
 Problem is that it's all meaningless. That all paths are

37
00:01:45,020 --> 00:01:46,000
 meaningless.

38
00:01:46,000 --> 00:01:49,640
 How could they have meaning? You become God. Okay, you're

39
00:01:49,640 --> 00:01:50,000
 God.

40
00:01:50,000 --> 00:01:53,000
 There's no intrinsic meaning in that, right?

41
00:01:53,000 --> 00:01:56,240
 You become king of the world. Okay, you're king of the

42
00:01:56,240 --> 00:01:57,000
 world.

43
00:01:57,000 --> 00:02:01,000
 And this is the truth. This is an unmitigatable truth.

44
00:02:01,000 --> 00:02:05,290
 This is objective for all the paths that they all are

45
00:02:05,290 --> 00:02:06,000
 meaning.

46
00:02:06,000 --> 00:02:09,550
 You could even say that I don't want to quite go there, but

47
00:02:09,550 --> 00:02:11,000
 no, I'm not going to say it.

48
00:02:11,000 --> 00:02:14,760
 I think the one exception, I would say the one exception is

49
00:02:14,760 --> 00:02:16,000
 the Buddha's path, right?

50
00:02:16,000 --> 00:02:18,650
 And of course everyone says that about their path, but hear

51
00:02:18,650 --> 00:02:19,000
 me out.

52
00:02:19,000 --> 00:02:22,000
 Because once you realize that all paths are meaningless,

53
00:02:22,000 --> 00:02:23,000
 you let go.

54
00:02:23,000 --> 00:02:27,130
 You stop striving. You stop looking for a path. You stop

55
00:02:27,130 --> 00:02:29,000
 creating karma.

56
00:02:29,000 --> 00:02:33,140
 And that is the teaching in one way is the teaching of the

57
00:02:33,140 --> 00:02:34,000
 Buddha.

58
00:02:34,000 --> 00:02:39,070
 So this is a theory that, you know, the proposition is that

59
00:02:39,070 --> 00:02:40,000
 it is a special,

60
00:02:40,000 --> 00:02:45,000
 that the Buddha was not just another teacher in one sense.

61
00:02:45,000 --> 00:02:48,400
 And that is the sense that he taught the giving up of all

62
00:02:48,400 --> 00:02:49,000
 paths.

63
00:02:49,000 --> 00:02:54,780
 His path was the giving up of striving for anything, for

64
00:02:54,780 --> 00:02:57,000
 any state of being.

65
00:02:57,000 --> 00:03:00,170
 And so if we talk about the Buddha's path as being

66
00:03:00,170 --> 00:03:01,000
 meaningless

67
00:03:01,000 --> 00:03:04,000
 or the goal, which is Nibbana as being meaningless,

68
00:03:04,000 --> 00:03:08,630
 I think it's very difficult and it's probably inaccurate to

69
00:03:08,630 --> 00:03:09,000
 say,

70
00:03:09,000 --> 00:03:14,000
 because it's a non-path. It's not going. It's not coming.

71
00:03:14,000 --> 00:03:18,000
 It's not creating anything.

72
00:03:18,000 --> 00:03:23,000
 As Sumedha said, it's not even remembering.

73
00:03:23,000 --> 00:03:29,000
 So you'd be hard-pressed to say that it's meaningless.

74
00:03:29,000 --> 00:03:32,000
 Because there's nothing that you could grasp on and say,

75
00:03:32,000 --> 00:03:34,000
 "Look, you see? No meaning to that."

76
00:03:34,000 --> 00:03:37,000
 Or, "What is this for?" or so on.

77
00:03:37,000 --> 00:03:40,570
 There's no handle by which you could grasp and say, "This

78
00:03:40,570 --> 00:03:42,000
 is meaningless."

79
00:03:42,000 --> 00:03:44,000
 So they actually do say it's meaningful.

80
00:03:44,000 --> 00:03:48,000
 It has benefit or it has purpose.

81
00:03:48,000 --> 00:03:51,000
 I don't know if I'd go so far, but people do say that.

82
00:03:51,000 --> 00:03:54,000
 The texts do say that.

83
00:03:54,000 --> 00:03:57,000
 You got anything?

84
00:03:57,000 --> 00:03:58,000
 No.

