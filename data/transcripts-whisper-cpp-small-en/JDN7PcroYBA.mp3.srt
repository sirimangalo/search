1
00:00:00,000 --> 00:00:04,280
 Hello, today we continue our study of the Dhammapada,

2
00:00:04,280 --> 00:00:08,840
 hung with verse number six, which goes,

3
00:00:08,840 --> 00:00:15,200
 "Parejana vijayananti maya meta yama mase,

4
00:00:15,200 --> 00:00:22,200
 yejatata vijayananti tato samanti medhaga."

5
00:00:22,200 --> 00:00:31,240
 Which means, there are some who don't see clearly that we

6
00:00:31,240 --> 00:00:32,920
 will all have to die,

7
00:00:32,920 --> 00:00:44,890
 but those who do see this clearly, yejatata vijayananti,

8
00:00:44,890 --> 00:00:46,520
 for those who do see clearly,

9
00:00:46,520 --> 00:00:54,890
 because of that, tato samanti medhaga, because of that they

10
00:00:54,890 --> 00:00:57,640
 settle their quarrels,

11
00:00:57,640 --> 00:01:00,520
 settle their disputes.

12
00:01:00,520 --> 00:01:03,880
 So people who don't see clearly or don't fully understand,

13
00:01:03,880 --> 00:01:08,440
 "Vijayananti," many people don't see this.

14
00:01:08,440 --> 00:01:13,840
 For those who see it, see clearly that we all have to die,

15
00:01:13,840 --> 00:01:18,120
 nothing is permanent, they are able to settle their quarrel

16
00:01:18,120 --> 00:01:21,400
s when they realize this truth.

17
00:01:21,400 --> 00:01:25,520
 This was told in regards to one of my personal favorites,

18
00:01:25,520 --> 00:01:29,720
 which is kind of odd, but it's an interesting monk story.

19
00:01:29,720 --> 00:01:35,520
 It's about a quarrel that arose between two monks,

20
00:01:35,520 --> 00:01:41,040
 who lived in Gosidharama, a monastery in the Buddha's time.

21
00:01:41,040 --> 00:01:42,820
 Gosidharama, there were two monks,

22
00:01:42,820 --> 00:01:44,960
 one was very famous for teaching the Dhamma,

23
00:01:44,960 --> 00:01:49,210
 which is the meditation practice and the things that the

24
00:01:49,210 --> 00:01:51,280
 Buddha taught us to do,

25
00:01:51,280 --> 00:01:52,760
 we should do.

26
00:01:52,760 --> 00:01:55,280
 And another one was very famous for the Vinayat,

27
00:01:55,280 --> 00:01:57,600
 the Buddha's teaching on the monks' roles and the things

28
00:01:57,600 --> 00:02:02,400
 that one shouldn't do.

29
00:02:02,400 --> 00:02:03,400
 So they were both very famous,

30
00:02:03,400 --> 00:02:07,680
 they each had a very large group of students.

31
00:02:07,680 --> 00:02:09,440
 And one day, one of them was,

32
00:02:09,440 --> 00:02:12,200
 the one who taught the Dhamma was coming out of the wash

33
00:02:12,200 --> 00:02:12,560
room,

34
00:02:12,560 --> 00:02:14,650
 and the one who taught the Vinayat was going into the wash

35
00:02:14,650 --> 00:02:15,400
room.

36
00:02:15,400 --> 00:02:18,220
 And the one to the Vinaya looked and saw that the one who

37
00:02:18,220 --> 00:02:19,520
 teaches the Dhamma

38
00:02:19,520 --> 00:02:27,520
 had left some water in the bucket, in the washroom,

39
00:02:27,520 --> 00:02:31,670
 which you're not supposed to do because mosquitoes will lay

40
00:02:31,670 --> 00:02:33,120
 their eggs.

41
00:02:33,120 --> 00:02:35,880
 And so he said this, he said, "Hey, that's an offense, you

42
00:02:35,880 --> 00:02:36,960
 know, to do that."

43
00:02:36,960 --> 00:02:39,800
 And he said, "Oh, I'm sorry, I didn't realize,

44
00:02:39,800 --> 00:02:41,920
 then please let me confess it to you."

45
00:02:41,920 --> 00:02:46,360
 Because when there's an offense against the rules,

46
00:02:46,360 --> 00:02:48,160
 then you have to confess and say,

47
00:02:48,160 --> 00:02:52,200
 you know, promise that you try your best not to do it again

48
00:02:52,200 --> 00:02:52,200
.

49
00:02:52,200 --> 00:02:55,340
 And just make it clear that you're aware of the fact that

50
00:02:55,340 --> 00:02:56,640
 you did it.

51
00:02:56,640 --> 00:03:00,040
 And the one who taught the Vinaya, teaches the Vinaya, said

52
00:03:00,040 --> 00:03:00,080
,

53
00:03:00,080 --> 00:03:02,520
 but if you didn't realize it was a,

54
00:03:02,520 --> 00:03:08,560
 or if you didn't do it intentionally, then it's not an

55
00:03:08,560 --> 00:03:10,040
 offense.

56
00:03:10,040 --> 00:03:13,010
 And the other monk said, "Okay then," and he left without

57
00:03:13,010 --> 00:03:14,320
 confessing it.

58
00:03:14,320 --> 00:03:16,360
 And the one who taught the Vinaya,

59
00:03:16,360 --> 00:03:19,520
 went around telling all his students that this other monk

60
00:03:19,520 --> 00:03:22,520
 had an offense that he hadn't confessed.

61
00:03:22,520 --> 00:03:24,640
 And so they went and told the students of the other teacher

62
00:03:24,640 --> 00:03:29,320
 that this story,

63
00:03:29,320 --> 00:03:31,840
 and those monks told the teacher, and the teacher said,

64
00:03:31,840 --> 00:03:34,530
 "Well, first he tells me it's not an offense, now he says

65
00:03:34,530 --> 00:03:35,920
 it is an offense,

66
00:03:35,920 --> 00:03:37,840
 that monk's a liar."

67
00:03:37,840 --> 00:03:40,360
 And so his students heard this, and they went and told the

68
00:03:40,360 --> 00:03:40,920
 other students,

69
00:03:40,920 --> 00:03:43,520
 "Hey, your teacher's a liar."

70
00:03:43,520 --> 00:03:47,740
 And then they told their teacher, and the teacher got more

71
00:03:47,740 --> 00:03:49,120
 angry and said nasty things

72
00:03:49,120 --> 00:03:53,720
 until finally they were split up into two different groups.

73
00:03:53,720 --> 00:03:57,240
 As a result of that, the novices and the bhikkhunis,

74
00:03:57,240 --> 00:04:01,000
 the female monks were split into two different groups as

75
00:04:01,000 --> 00:04:01,600
 well.

76
00:04:01,600 --> 00:04:04,840
 The lay people who supported the monks split up into two

77
00:04:04,840 --> 00:04:05,080
 groups.

78
00:04:05,080 --> 00:04:07,940
 The angels who guarded the monastery split up into two

79
00:04:07,940 --> 00:04:08,560
 groups.

80
00:04:08,560 --> 00:04:12,070
 The angels that were their friends who lived in the sky and

81
00:04:12,070 --> 00:04:14,120
 in the various heavens,

82
00:04:14,120 --> 00:04:15,520
 they all split up into two groups.

83
00:04:15,520 --> 00:04:20,220
 And the story goes that this part of the universe split up

84
00:04:20,220 --> 00:04:21,840
 all the way up into the Brahma,

85
00:04:21,840 --> 00:04:27,290
 the God realms, and the gods themselves were split, believe

86
00:04:27,290 --> 00:04:28,740
 it or not,

87
00:04:28,740 --> 00:04:32,760
 over some water being left in a bucket.

88
00:04:32,760 --> 00:04:35,530
 Now the story goes on and on about how the Buddha tried to

89
00:04:35,530 --> 00:04:39,600
 convince them to change their

90
00:04:39,600 --> 00:04:42,280
 ways and they wouldn't listen.

91
00:04:42,280 --> 00:04:44,980
 And actually they said to him, "Oh, please don't bother

92
00:04:44,980 --> 00:04:45,840
 yourself.

93
00:04:45,840 --> 00:04:47,800
 We will take care of this on our own."

94
00:04:47,800 --> 00:04:50,440
 And they wouldn't let the Buddha even get involved.

95
00:04:50,440 --> 00:04:52,920
 And so the Buddha left and went and stayed.

96
00:04:52,920 --> 00:04:55,140
 And there's a story, this whole side story about how the

97
00:04:55,140 --> 00:04:56,760
 Buddha stayed in the forest

98
00:04:56,760 --> 00:05:01,650
 with an elephant, and the elephant gave him food and took

99
00:05:01,650 --> 00:05:02,440
 care of him.

100
00:05:02,440 --> 00:05:06,780
 And then after that he came back and then at the end he got

101
00:05:06,780 --> 00:05:08,360
 all these monks.

102
00:05:08,360 --> 00:05:11,160
 What happened, sorry, the best part of the…

103
00:05:11,160 --> 00:05:13,670
 When the Buddha left, all of the lay people said, "Well,

104
00:05:13,670 --> 00:05:14,440
 where's the Buddha going?"

105
00:05:14,440 --> 00:05:16,820
 They said, "Oh, well, we told them not to get involved with

106
00:05:16,820 --> 00:05:17,560
 our quarrel."

107
00:05:17,560 --> 00:05:19,200
 And so he left.

108
00:05:19,200 --> 00:05:22,150
 And all the lay people got very angry and upset and said, "

109
00:05:22,150 --> 00:05:23,360
How could you…

110
00:05:23,360 --> 00:05:27,770
 Now we don't have the opportunity to see the Buddha because

111
00:05:27,770 --> 00:05:28,600
 of you."

112
00:05:28,600 --> 00:05:31,720
 And so they stopped giving food to the monks.

113
00:05:31,720 --> 00:05:35,830
 And after that the monks had a very hard time getting Aum's

114
00:05:35,830 --> 00:05:38,120
 food and just surviving.

115
00:05:38,120 --> 00:05:41,040
 And they went and they apologized.

116
00:05:41,040 --> 00:05:43,720
 And the people said, "Well, you have to get an apology from

117
00:05:43,720 --> 00:05:44,120
 the…

118
00:05:44,120 --> 00:05:46,640
 Get forgiveness from the Buddha."

119
00:05:46,640 --> 00:05:48,760
 Because it was the rain season, they couldn't do that.

120
00:05:48,760 --> 00:05:51,900
 So they had to wait three months and they were very, very

121
00:05:51,900 --> 00:05:53,520
 hard up for three months.

122
00:05:53,520 --> 00:05:58,690
 And after this the Buddha came back and then he put all the

123
00:05:58,690 --> 00:05:59,080
…

124
00:05:59,080 --> 00:06:02,030
 The monks who were quarreling in a special place and didn't

125
00:06:02,030 --> 00:06:03,400
 let any of the other monks

126
00:06:03,400 --> 00:06:05,040
 get involved with them.

127
00:06:05,040 --> 00:06:06,590
 When people came and asked, "Where are those quarreling

128
00:06:06,590 --> 00:06:06,960
 monks?"

129
00:06:06,960 --> 00:06:09,210
 The Buddha would always point to them and say, "There they

130
00:06:09,210 --> 00:06:10,280
 are, there they are."

131
00:06:10,280 --> 00:06:12,740
 And they were so ashamed that eventually they came and

132
00:06:12,740 --> 00:06:14,400
 asked forgiveness from the Buddha

133
00:06:14,400 --> 00:06:17,720
 and felt really ashamed for what they had done.

134
00:06:17,720 --> 00:06:20,600
 And that's when the Buddha told this verse.

135
00:06:20,600 --> 00:06:25,050
 Now the verse has, I think, two important lessons that we

136
00:06:25,050 --> 00:06:26,600
 can draw from it.

137
00:06:26,600 --> 00:06:29,690
 And the first one is in regards to death, which I sort of

138
00:06:29,690 --> 00:06:31,120
 went into last time.

139
00:06:31,120 --> 00:06:32,840
 But I'd like to talk a little bit more.

140
00:06:32,840 --> 00:06:37,420
 The second one is about the importance of wisdom and

141
00:06:37,420 --> 00:06:39,160
 understanding.

142
00:06:39,160 --> 00:06:42,160
 And how understanding is really the key in overcoming

143
00:06:42,160 --> 00:06:44,180
 quarrels and overcoming all suffering

144
00:06:44,180 --> 00:06:49,840
 and all unwholesome, it's all evil.

145
00:06:49,840 --> 00:06:56,290
 So first in regards to death, it's interesting how the

146
00:06:56,290 --> 00:07:00,780
 theory or the ideology, the teaching

147
00:07:00,780 --> 00:07:05,770
 on rebirth and how the mind continues actually makes death

148
00:07:05,770 --> 00:07:08,100
 a more important event.

149
00:07:08,100 --> 00:07:11,050
 And this verse kind of makes that clear in a way that we

150
00:07:11,050 --> 00:07:12,380
 might not think of.

151
00:07:12,380 --> 00:07:14,560
 The Buddha is telling us how important death is.

152
00:07:14,560 --> 00:07:17,070
 And we think, "Well, if the mind continues then how is it

153
00:07:17,070 --> 00:07:18,240
 really so important?"

154
00:07:18,240 --> 00:07:20,880
 And the way it works is this.

155
00:07:20,880 --> 00:07:25,320
 The body and the mind have two, they work together, but

156
00:07:25,320 --> 00:07:28,160
 they have two characteristics.

157
00:07:28,160 --> 00:07:29,440
 The mind is very quick.

158
00:07:29,440 --> 00:07:32,990
 The mind in one moment can be here, the next moment can be

159
00:07:32,990 --> 00:07:34,880
 there, it can arise and move

160
00:07:34,880 --> 00:07:37,800
 very quickly from one object to the next.

161
00:07:37,800 --> 00:07:40,520
 The body on the other hand is very fixed.

162
00:07:40,520 --> 00:07:45,440
 And so our state of being in this life on a physical level

163
00:07:45,440 --> 00:07:47,140
 is quite fixed.

164
00:07:47,140 --> 00:07:49,580
 But on a mental level it's quite fluid.

165
00:07:49,580 --> 00:07:52,920
 So your mind can change and if your mind becomes more and

166
00:07:52,920 --> 00:07:55,040
 more corrupt, it can do that very

167
00:07:55,040 --> 00:07:59,740
 quickly and it can change from good to evil or from evil to

168
00:07:59,740 --> 00:08:02,180
 good relatively quickly.

169
00:08:02,180 --> 00:08:05,510
 And there can be quite a disparity therefore between the

170
00:08:05,510 --> 00:08:07,380
 physical realm and the mental

171
00:08:07,380 --> 00:08:08,380
 realm.

172
00:08:08,380 --> 00:08:12,800
 You can be surrounded by good things and get lots of good

173
00:08:12,800 --> 00:08:15,960
 and wonderful pleasures and luxuries

174
00:08:15,960 --> 00:08:19,100
 and have a very corrupt and evil mind that one would think

175
00:08:19,100 --> 00:08:20,760
 this person doesn't deserve

176
00:08:20,760 --> 00:08:23,040
 all of that luxury.

177
00:08:23,040 --> 00:08:27,310
 But the truth is this is because of the difference, the

178
00:08:27,310 --> 00:08:30,600
 body is a coarse construct and the amount

179
00:08:30,600 --> 00:08:35,600
 of energy that is involved in simply the creation of a

180
00:08:35,600 --> 00:08:39,420
 human being or of a physical construct

181
00:08:39,420 --> 00:08:41,200
 is immense.

182
00:08:41,200 --> 00:08:46,310
 And so it has a great power, this power of inertia where

183
00:08:46,310 --> 00:08:49,560
 you can't just stop that inertia.

184
00:08:49,560 --> 00:08:53,280
 Now some people if they do really bad things or really good

185
00:08:53,280 --> 00:08:55,520
 things they might wind up cutting

186
00:08:55,520 --> 00:08:57,480
 it off by dying.

187
00:08:57,480 --> 00:09:01,780
 You hear about really good people who are killed because of

188
00:09:01,780 --> 00:09:03,720
 their unwillingness to do

189
00:09:03,720 --> 00:09:07,350
 evil and so on or die because they're not willing to do

190
00:09:07,350 --> 00:09:08,900
 evil in order to live on and

191
00:09:08,900 --> 00:09:09,900
 so on.

192
00:09:09,900 --> 00:09:13,620
 You of course hear about evil, evil people who do bad

193
00:09:13,620 --> 00:09:16,000
 things and as a result are killed

194
00:09:16,000 --> 00:09:19,790
 or die in unpleasant circumstances because of the way their

195
00:09:19,790 --> 00:09:20,900
 minds are.

196
00:09:20,900 --> 00:09:25,130
 But for most people or for many people the inertia and the

197
00:09:25,130 --> 00:09:27,480
 power of the physical prevents

198
00:09:27,480 --> 00:09:31,680
 the repercussions until the moment of death.

199
00:09:31,680 --> 00:09:34,380
 Now at the moment of death the body is totally

200
00:09:34,380 --> 00:09:35,640
 reconstructed.

201
00:09:35,640 --> 00:09:40,230
 There is a period that goes where the mind leaves the body

202
00:09:40,230 --> 00:09:42,920
 behind and begins this immense

203
00:09:42,920 --> 00:09:48,020
 activity or work required to rebuild another body or to

204
00:09:48,020 --> 00:09:51,520
 create another human being starting

205
00:09:51,520 --> 00:09:55,440
 at the moment of conception.

206
00:09:55,440 --> 00:09:59,380
 Now that of course is much more mental and the nature of

207
00:09:59,380 --> 00:10:01,560
 that is going to depend almost

208
00:10:01,560 --> 00:10:06,200
 entirely on the nature of the mind or the set of the mind.

209
00:10:06,200 --> 00:10:08,920
 What sort of work is the mind going to undertake?

210
00:10:08,920 --> 00:10:12,110
 For most people we're caught up in this programming of

211
00:10:12,110 --> 00:10:14,480
 creating a human being and a human life

212
00:10:14,480 --> 00:10:17,880
 and so as a result we'll be reborn as a human being.

213
00:10:17,880 --> 00:10:21,380
 But you can be reborn in many different ways based on your

214
00:10:21,380 --> 00:10:23,600
 mind and therefore the importance

215
00:10:23,600 --> 00:10:26,820
 of you know as we've seen in the past stories the

216
00:10:26,820 --> 00:10:29,680
 importance of settling our disputes in

217
00:10:29,680 --> 00:10:34,680
 this life and settling them in our mind anyway.

218
00:10:34,680 --> 00:10:39,640
 Sometimes we can't stop other people from being angry at us

219
00:10:39,640 --> 00:10:42,040
 but of greatest importance

220
00:10:42,040 --> 00:10:45,240
 is that we are able to settle them in our own minds.

221
00:10:45,240 --> 00:10:51,430
 And this is the word samanti, samanti, samanti which means

222
00:10:51,430 --> 00:10:54,720
 to stabilize or to tranquilize

223
00:10:54,720 --> 00:10:59,180
 them in our minds to make it so that they're no longer a

224
00:10:59,180 --> 00:11:01,200
 source of clinging.

225
00:11:01,200 --> 00:11:09,770
 We no longer cling, we no longer regurgitate and go after

226
00:11:09,770 --> 00:11:14,160
 and recreate this in our minds.

227
00:11:14,160 --> 00:11:17,480
 Which was obviously the problem in the case with these two

228
00:11:17,480 --> 00:11:19,200
 sets of monks which was a real

229
00:11:19,200 --> 00:11:24,080
 danger and so the Buddha pointed out to them.

230
00:11:24,080 --> 00:11:26,790
 This is really useless, this is really futile and the

231
00:11:26,790 --> 00:11:28,760
 reason people do this is because they

232
00:11:28,760 --> 00:11:30,920
 don't see, they don't see clearly.

233
00:11:30,920 --> 00:11:35,240
 So this is the importance of death because it frees the

234
00:11:35,240 --> 00:11:37,440
 mind to make choices again on

235
00:11:37,440 --> 00:11:41,580
 a level that it's not free to in this life because of the

236
00:11:41,580 --> 00:11:43,440
 inertia of its previous work

237
00:11:43,440 --> 00:11:47,840
 in creating this being.

238
00:11:47,840 --> 00:11:53,130
 And the second part is that importance of understanding and

239
00:11:53,130 --> 00:11:55,520
 how understanding things

240
00:11:55,520 --> 00:12:00,180
 like death is important on a meditative level,

241
00:12:00,180 --> 00:12:04,320
 understanding the truth of birth and death

242
00:12:04,320 --> 00:12:10,980
 as occurring on a momentary level is far more important and

243
00:12:10,980 --> 00:12:14,520
 crucial in fact in freeing ourselves

244
00:12:14,520 --> 00:12:21,600
 up from strife and quarrel and disputes and suffering.

245
00:12:21,600 --> 00:12:25,870
 And I've said it before but it's worth reiterating that

246
00:12:25,870 --> 00:12:28,560
 this is really all you need to do to

247
00:12:28,560 --> 00:12:33,000
 overcome your suffering, to overcome quarrels and so on.

248
00:12:33,000 --> 00:12:34,950
 You don't have to go and apologize to the people, I mean

249
00:12:34,950 --> 00:12:36,200
 that's always a good way to

250
00:12:36,200 --> 00:12:40,580
 overcome it but on a general level in regards to our cling

251
00:12:40,580 --> 00:12:43,400
ings, in regards to our cravings,

252
00:12:43,400 --> 00:12:47,850
 in regards to our attachments, all that needs to be done is

253
00:12:47,850 --> 00:12:49,360
 to see it clearly.

254
00:12:49,360 --> 00:12:52,380
 When you're addicted to something you don't have to force

255
00:12:52,380 --> 00:12:54,080
 yourself to stop or try to convince

256
00:12:54,080 --> 00:12:56,970
 yourself that it's wrong or remind yourself again and again

257
00:12:56,970 --> 00:12:58,240
 how bad you are for doing

258
00:12:58,240 --> 00:12:59,240
 it.

259
00:12:59,240 --> 00:13:01,900
 All you have to do is see the addiction clearly for what it

260
00:13:01,900 --> 00:13:03,520
 is, see it objectively, let it

261
00:13:03,520 --> 00:13:06,790
 be, let it come up and when you see it for what it is you

262
00:13:06,790 --> 00:13:08,040
'll let go of it.

263
00:13:08,040 --> 00:13:10,860
 You'll have no desire to cling to it because you'll see

264
00:13:10,860 --> 00:13:12,440
 that everything ceases.

265
00:13:12,440 --> 00:13:14,690
 So this Buddhist teaching on death was only the

266
00:13:14,690 --> 00:13:16,920
 introduction, it's this reminder to bring

267
00:13:16,920 --> 00:13:19,830
 the monks back to this idea that everything arises and

268
00:13:19,830 --> 00:13:21,640
 ceases because once they can see

269
00:13:21,640 --> 00:13:24,600
 that we die they can see how futile it is.

270
00:13:24,600 --> 00:13:25,600
 Why are we fighting?

271
00:13:25,600 --> 00:13:26,600
 What are we hoping to gain?

272
00:13:26,600 --> 00:13:30,860
 We can come out on top as I said in the last video but what

273
00:13:30,860 --> 00:13:33,400
 does it mean to be on top because

274
00:13:33,400 --> 00:13:40,540
 eventually we all die and we're put on bottom.

275
00:13:40,540 --> 00:13:42,400
 And once they start to do that then you can start to see

276
00:13:42,400 --> 00:13:43,560
 that really why would we cling

277
00:13:43,560 --> 00:13:45,800
 to anything and you start to be able to look at your

278
00:13:45,800 --> 00:13:47,480
 clinging and see that when you cling

279
00:13:47,480 --> 00:13:50,430
 to something you hold on to it, it maybe brings you

280
00:13:50,430 --> 00:13:53,320
 pleasure temporarily but then it disappears.

281
00:13:53,320 --> 00:13:54,480
 And what is the benefit?

282
00:13:54,480 --> 00:13:59,320
 What is the gain that comes from that?

283
00:13:59,320 --> 00:14:03,110
 When you see on a phenomenological level an experiential or

284
00:14:03,110 --> 00:14:04,860
 moment to moment level how

285
00:14:04,860 --> 00:14:08,510
 everything arises and ceases, how this happiness, this

286
00:14:08,510 --> 00:14:10,760
 pleasure that you're hoping to get is

287
00:14:10,760 --> 00:14:15,820
 really actually bound up with suffering and stress because

288
00:14:15,820 --> 00:14:18,320
 it conditions the mind to want

289
00:14:18,320 --> 00:14:24,540
 more and to strive more and to be stressed and you can make

290
00:14:24,540 --> 00:14:28,320
 yourself sick and uncomfortable,

291
00:14:28,320 --> 00:14:33,760
 unwell as a result of the clinging.

292
00:14:33,760 --> 00:14:37,090
 So when you practice meditation, when you look at the

293
00:14:37,090 --> 00:14:39,160
 objects of awareness on a moment

294
00:14:39,160 --> 00:14:44,730
 to moment basis, you're able to see that everything arises

295
00:14:44,730 --> 00:14:46,120
 and ceases.

296
00:14:46,120 --> 00:14:50,060
 Simply that seeing allows you to, it changes your whole way

297
00:14:50,060 --> 00:14:51,720
 of looking at things.

298
00:14:51,720 --> 00:14:55,140
 You no longer look at things in terms of the universe in

299
00:14:55,140 --> 00:14:56,960
 terms of us and them and I and

300
00:14:56,960 --> 00:15:00,360
 me and mine and so on.

301
00:15:00,360 --> 00:15:07,350
 So many of the points of reference that we take on an

302
00:15:07,350 --> 00:15:11,600
 ordinary run of the meal frame

303
00:15:11,600 --> 00:15:16,250
 of reference are gone, are lost, are given up by someone

304
00:15:16,250 --> 00:15:18,760
 who takes up this practice.

305
00:15:18,760 --> 00:15:21,400
 You really change your whole way of looking at the world.

306
00:15:21,400 --> 00:15:25,160
 And I think it's important to pick up this word in this

307
00:15:25,160 --> 00:15:27,960
 verse, vijananti, which is really

308
00:15:27,960 --> 00:15:30,680
 the same word as vipassana.

309
00:15:30,680 --> 00:15:33,770
 Vipassana means to see clearly, vijananti means to know

310
00:15:33,770 --> 00:15:35,680
 clearly or to understand clearly.

311
00:15:35,680 --> 00:15:38,960
 But it's really the same word and it has the exact same

312
00:15:38,960 --> 00:15:41,080
 meaning and it's really the core

313
00:15:41,080 --> 00:15:42,080
 of Buddhism.

314
00:15:42,080 --> 00:15:44,440
 And it's so easy when you read this verse to just skip by

315
00:15:44,440 --> 00:15:45,680
 that and say, "Oh yeah, yeah,

316
00:15:45,680 --> 00:15:47,530
 we have to think about death and death is the most

317
00:15:47,530 --> 00:15:48,200
 important."

318
00:15:48,200 --> 00:15:52,320
 Now the most important is the knowing, the understanding.

319
00:15:52,320 --> 00:15:56,180
 When you see, with death as an example, when you see that

320
00:15:56,180 --> 00:15:58,920
 everything ceases, when you see,

321
00:15:58,920 --> 00:16:03,520
 "Are quarrel, why are we quarreling, we have to die?"

322
00:16:03,520 --> 00:16:06,800
 It helps you see that everything ceases.

323
00:16:06,800 --> 00:16:09,010
 When you see on a momentary level, "I'm clinging to this

324
00:16:09,010 --> 00:16:10,560
 thing, why would I cling to it when

325
00:16:10,560 --> 00:16:12,120
 it has to cease?"

326
00:16:12,120 --> 00:16:14,440
 When you're able to see things arising and ceasing, this is

327
00:16:14,440 --> 00:16:15,440
 what the meaning is, when

328
00:16:15,440 --> 00:16:19,550
 you're able to understand clearly that there is nothing in

329
00:16:19,550 --> 00:16:21,320
 the world that you can hold

330
00:16:21,320 --> 00:16:24,320
 on to that lasts more than an instant.

331
00:16:24,320 --> 00:16:28,600
 When you're able to see that it's futile to cling and it's

332
00:16:28,600 --> 00:16:31,200
 useless, it's unbeneficial,

333
00:16:31,200 --> 00:16:35,530
 and actually it's a source of great stress and suffering by

334
00:16:35,530 --> 00:16:38,080
 its very nature, by the stress

335
00:16:38,080 --> 00:16:42,720
 caused from clinging, from wanting, from craving and so on.

336
00:16:42,720 --> 00:16:48,650
 And the building and building up of more and more craving

337
00:16:48,650 --> 00:16:51,760
 and suffering when you don't

338
00:16:51,760 --> 00:16:54,160
 get what you want and so on.

339
00:16:54,160 --> 00:16:58,220
 So this is another important verse and another good lesson

340
00:16:58,220 --> 00:16:59,240
 for us.

341
00:16:59,240 --> 00:17:02,720
 This is the Dhamma for today from the Dhammapada.

342
00:17:02,720 --> 00:17:03,720
 So thanks for tuning in.

343
00:17:03,720 --> 00:17:03,720
 All the best.

344
00:17:03,720 --> 00:17:04,720
 1

