1
00:00:00,000 --> 00:00:04,970
 Okay, hello and welcome back to our study of the Tama Padam

2
00:00:04,970 --> 00:00:05,360
.

3
00:00:05,360 --> 00:00:11,100
 Today we continue on with verse number 72, which reads as

4
00:00:11,100 --> 00:00:13,000
 follows.

5
00:00:13,000 --> 00:00:23,510
 Yawadeva anata yatang balasa caayati, hanti balasa sukang

6
00:00:23,510 --> 00:00:28,720
 sang mudhamasa vipatayam.

7
00:00:28,720 --> 00:00:39,790
 Which means, yawadeva anata yat, yawadeva to the extent, to

8
00:00:39,790 --> 00:00:44,800
 which extent, anata yat,

9
00:00:44,800 --> 00:00:49,440
 yatang balasa caayati.

10
00:00:49,440 --> 00:00:58,870
 Knowledge arises to the extent that knowledge arises in a

11
00:00:58,870 --> 00:01:03,040
 fool, bala anata yat, to that

12
00:01:03,040 --> 00:01:09,840
 extent it arises to that person's disadvantage.

13
00:01:09,840 --> 00:01:15,720
 So knowledge arises to the disadvantage of a fool.

14
00:01:15,720 --> 00:01:25,550
 Hanti balasa sukang sang, for it destroys, kills, or puts

15
00:01:25,550 --> 00:01:29,840
 an end to their good luck sukang

16
00:01:29,840 --> 00:01:30,840
 sang.

17
00:01:30,840 --> 00:01:42,270
 Mudhamasa vipatayam, it crushes his or her head, crushes

18
00:01:42,270 --> 00:01:49,080
 her head, his head, their head.

19
00:01:49,080 --> 00:01:54,960
 Very important verse for all of us to keep in mind.

20
00:01:54,960 --> 00:02:00,220
 This is the sort of verse that you can think of as

21
00:02:00,220 --> 00:02:04,080
 exemplary of the Buddha's teaching,

22
00:02:04,080 --> 00:02:16,280
 or it is indicative of the nature of the Buddha's teaching.

23
00:02:16,280 --> 00:02:21,540
 And it shows how the place he gave to knowledge is not

24
00:02:21,540 --> 00:02:23,720
 being a bad thing.

25
00:02:23,720 --> 00:02:31,010
 But clearly not being enough, having its limitations and

26
00:02:31,010 --> 00:02:34,800
 potential disadvantages.

27
00:02:34,800 --> 00:02:40,040
 So what was this verse told in regards to?

28
00:02:40,040 --> 00:02:48,930
 Well it seems that there's a theme here, as with the last

29
00:02:48,930 --> 00:02:52,280
 verse, verse 71, this is told

30
00:02:52,280 --> 00:02:57,240
 about Moggallana and the things he saw on Vulture's Peak.

31
00:02:57,240 --> 00:03:01,440
 So it seems that Moggallana was coming down from Vulture's

32
00:03:01,440 --> 00:03:02,560
 Peak again.

33
00:03:02,560 --> 00:03:08,450
 Again with the same monk, Lakana, who was I guess a good

34
00:03:08,450 --> 00:03:11,440
 friend of Moggallana's.

35
00:03:11,440 --> 00:03:15,200
 And again he smiled.

36
00:03:15,200 --> 00:03:17,680
 And again Lakana asked him, "What are you smiling about?"

37
00:03:17,680 --> 00:03:21,130
 And again, just as before, Moggallana refuses to answer and

38
00:03:21,130 --> 00:03:22,760
 says, "Ask me again when we

39
00:03:22,760 --> 00:03:28,400
 get in front of the Buddha."

40
00:03:28,400 --> 00:03:31,880
 And indeed Lakana does exactly that.

41
00:03:31,880 --> 00:03:36,760
 And again the Buddha admits that he saw, no again, and

42
00:03:36,760 --> 00:03:39,680
 again Moggallana tells him what

43
00:03:39,680 --> 00:03:40,680
 he saw.

44
00:03:40,680 --> 00:03:51,770
 He saw a ghost, a huge invisible ethereal being, and this

45
00:03:51,770 --> 00:03:57,320
 time having his head crushed

46
00:03:57,320 --> 00:04:00,760
 by hammers.

47
00:04:00,760 --> 00:04:06,300
 So again and again hammers would crash upon his skull and

48
00:04:06,300 --> 00:04:07,760
 would burst.

49
00:04:07,760 --> 00:04:12,870
 And again and again his head would be rebuilt, it would

50
00:04:12,870 --> 00:04:16,640
 rebuild itself, and again the hammers

51
00:04:16,640 --> 00:04:22,880
 would crash down and repeat endlessly.

52
00:04:22,880 --> 00:04:25,880
 He said, "I smiled because I've never seen such a being

53
00:04:25,880 --> 00:04:26,640
 before."

54
00:04:26,640 --> 00:04:33,400
 This is what the text says, so it's an interesting thing.

55
00:04:33,400 --> 00:04:35,440
 And then the Buddha says that he saw such a being but didn

56
00:04:35,440 --> 00:04:37,160
't want to say anything because

57
00:04:37,160 --> 00:04:41,480
 people would, if he didn't have a witness, people wouldn't

58
00:04:41,480 --> 00:04:42,640
 believe him.

59
00:04:42,640 --> 00:04:45,510
 And I think such is the case nowadays when I tell these

60
00:04:45,510 --> 00:04:46,280
 stories.

61
00:04:46,280 --> 00:04:52,690
 I risk, we risk people disbelieving it, feeling that such a

62
00:04:52,690 --> 00:04:55,440
 being could not exist.

63
00:04:55,440 --> 00:04:57,320
 So I have a bit of a problem here.

64
00:04:57,320 --> 00:05:06,630
 And I'd like people to focus not on details, you shouldn't

65
00:05:06,630 --> 00:05:12,200
 focus on facts, historical facts.

66
00:05:12,200 --> 00:05:13,200
 Did this happen?

67
00:05:13,200 --> 00:05:14,200
 Did that not happen?

68
00:05:14,200 --> 00:05:15,480
 It's not really important.

69
00:05:15,480 --> 00:05:16,600
 Does such a being exist?

70
00:05:16,600 --> 00:05:17,960
 Does such a being not exist?

71
00:05:17,960 --> 00:05:21,910
 It's more important to understand the theory and obviously

72
00:05:21,910 --> 00:05:23,960
 more important to understand

73
00:05:23,960 --> 00:05:29,120
 this verse which doesn't require the story to be true.

74
00:05:29,120 --> 00:05:33,050
 On the other hand, it would be of course much to our

75
00:05:33,050 --> 00:05:36,000
 advantage if we could understand the

76
00:05:36,000 --> 00:05:39,240
 mechanics behind such a being.

77
00:05:39,240 --> 00:05:43,000
 Such a being can arise and be clear in our minds that there

78
00:05:43,000 --> 00:05:44,680
's also the potential for

79
00:05:44,680 --> 00:05:49,140
 us to be born as a huge hulking being, having our head

80
00:05:49,140 --> 00:05:51,160
 crushed by hammers.

81
00:05:51,160 --> 00:05:55,920
 Apparently there is such a potential.

82
00:05:55,920 --> 00:05:59,820
 Even if we can't do that, if we can't bring our minds

83
00:05:59,820 --> 00:06:02,160
 around the fact that beings of a

84
00:06:02,160 --> 00:06:08,360
 different nature than ourselves exist and the potential for

85
00:06:08,360 --> 00:06:11,280
 the mind to create existence

86
00:06:11,280 --> 00:06:20,220
 that is undetectable or seemingly undetectable by physical

87
00:06:20,220 --> 00:06:26,120
 means, ordinary physical means.

88
00:06:26,120 --> 00:06:29,570
 If we can't, we shouldn't let it get in the way of our

89
00:06:29,570 --> 00:06:31,800
 practice and our appreciation

90
00:06:31,800 --> 00:06:35,680
 of the knowledge in this verse.

91
00:06:35,680 --> 00:06:42,640
 If you can, well, there's always a benefit there.

92
00:06:42,640 --> 00:06:49,100
 The benefit would be that you can then have faith and

93
00:06:49,100 --> 00:06:53,280
 confidence in your practice.

94
00:06:53,280 --> 00:06:57,390
 It gives you encouragement in your practice to avoid such

95
00:06:57,390 --> 00:06:58,240
 things.

96
00:06:58,240 --> 00:07:00,760
 Nobody wants to be born such a being.

97
00:07:00,760 --> 00:07:04,950
 Then, the monks were all like asking the Buddha how such a

98
00:07:04,950 --> 00:07:07,560
 thing could occur, how such a being

99
00:07:07,560 --> 00:07:08,560
 could exist.

100
00:07:08,560 --> 00:07:14,680
 What did such a being do to obtain such an existence?

101
00:07:14,680 --> 00:07:18,440
 The Buddha told the story of the past.

102
00:07:18,440 --> 00:07:24,280
 It seems there was once a cripple.

103
00:07:24,280 --> 00:07:30,760
 This cripple was very good in one thing and that was using

104
00:07:30,760 --> 00:07:32,040
 a sling.

105
00:07:32,040 --> 00:07:39,270
 He had this sling and he would shoot stones at these big

106
00:07:39,270 --> 00:07:43,880
 leaves, whatever big leaves there

107
00:07:43,880 --> 00:07:45,640
 were.

108
00:07:45,640 --> 00:07:48,840
 He could cut the leaves, which is by slinging stones.

109
00:07:48,840 --> 00:07:52,550
 He was such a sharp shooter that he could cut the leaves in

110
00:07:52,550 --> 00:07:54,040
 a certain pattern.

111
00:07:54,040 --> 00:08:00,040
 He would, just for fun, he would cut them into shapes.

112
00:08:00,040 --> 00:08:02,560
 He did this to amuse the children.

113
00:08:02,560 --> 00:08:05,390
 The children would follow him around and ask him to make an

114
00:08:05,390 --> 00:08:06,760
 elephant or ask him to make

115
00:08:06,760 --> 00:08:12,200
 a bird or so on and he would oblige.

116
00:08:12,200 --> 00:08:16,270
 One day, they were in the king's garden and they were

117
00:08:16,270 --> 00:08:18,800
 telling him that he was making all

118
00:08:18,800 --> 00:08:24,180
 these shapes and cutting the leaves out in various patterns

119
00:08:24,180 --> 00:08:24,360
.

120
00:08:24,360 --> 00:08:28,490
 When all of a sudden, the king came to the garden and the

121
00:08:28,490 --> 00:08:30,520
 children ran away and left

122
00:08:30,520 --> 00:08:32,720
 the cripple behind.

123
00:08:32,720 --> 00:08:38,330
 The king found out and the king went, of course, to rest in

124
00:08:38,330 --> 00:08:41,080
 the shade and found that the shade

125
00:08:41,080 --> 00:08:44,760
 was patched, that the shade wasn't complete.

126
00:08:44,760 --> 00:08:47,850
 He looked up and he saw these shapes and he said, "What is

127
00:08:47,850 --> 00:08:48,400
 this?

128
00:08:48,400 --> 00:08:49,400
 Where is this happening?

129
00:08:49,400 --> 00:08:51,800
 How is this happening?"

130
00:08:51,800 --> 00:08:54,200
 He inquired and found out that there was this cripple.

131
00:08:54,200 --> 00:08:56,400
 He thought, "Wow.

132
00:08:56,400 --> 00:08:59,080
 I could use such a person."

133
00:08:59,080 --> 00:09:04,280
 He asked him, "Would you be able to shoot these pebbles?"

134
00:09:04,280 --> 00:09:07,290
 Speaking of the sharp shooting, do you think you'd be able

135
00:09:07,290 --> 00:09:08,800
 to shoot these pebbles into

136
00:09:08,800 --> 00:09:13,280
 someone's mouth?

137
00:09:13,280 --> 00:09:14,880
 The cripple said, "Yeah, I could do that."

138
00:09:14,880 --> 00:09:20,280
 They said, "I'd like you to shoot a pint pot of horse dung,

139
00:09:20,280 --> 00:09:22,960
 cow dung, I can't remember,

140
00:09:22,960 --> 00:09:25,720
 into someone's mouth for me."

141
00:09:25,720 --> 00:09:29,160
 Turns out the king had an advisor who couldn't keep his

142
00:09:29,160 --> 00:09:31,360
 mouth shut and would just lather

143
00:09:31,360 --> 00:09:34,080
 on about anything and everything.

144
00:09:34,080 --> 00:09:40,670
 The king was annoyed by this and thought they would teach

145
00:09:40,670 --> 00:09:43,040
 this guy a lesson.

146
00:09:43,040 --> 00:09:45,960
 The cripple said, "Yes, I could do that."

147
00:09:45,960 --> 00:09:50,940
 The king took him to the court and hid him behind a curtain

148
00:09:50,940 --> 00:09:51,320
.

149
00:09:51,320 --> 00:09:54,280
 The cripple poked a little hole in it.

150
00:09:54,280 --> 00:09:58,060
 Every time this advisor opened his mouth, he would shoot a

151
00:09:58,060 --> 00:09:59,840
 little pellet of dung into

152
00:09:59,840 --> 00:10:05,640
 the guy's mouth and shut him up.

153
00:10:05,640 --> 00:10:09,590
 Apparently he got away with this until he completely got a

154
00:10:09,590 --> 00:10:11,360
 whole pint pot full of dung

155
00:10:11,360 --> 00:10:18,160
 into the guy's mouth.

156
00:10:18,160 --> 00:10:23,430
 The king then turned to the advisor and said, "You're such

157
00:10:23,430 --> 00:10:26,000
 a blather mouth, I can't take

158
00:10:26,000 --> 00:10:27,000
 it anymore."

159
00:10:27,000 --> 00:10:31,420
 He said, "All this time you've been sitting there and you

160
00:10:31,420 --> 00:10:33,560
 couldn't even keep your mouth

161
00:10:33,560 --> 00:10:37,660
 shut even when we were shooting pellets of dung into your

162
00:10:37,660 --> 00:10:38,560
 mouth."

163
00:10:38,560 --> 00:10:41,280
 It's bizarre really.

164
00:10:41,280 --> 00:10:44,600
 I'm not getting into this story.

165
00:10:44,600 --> 00:10:47,560
 It's quite interesting.

166
00:10:47,560 --> 00:10:50,600
 This is the story that's been passed down.

167
00:10:50,600 --> 00:10:52,840
 It's a bizarre sort of story I think.

168
00:10:52,840 --> 00:10:54,800
 An interesting way to...

169
00:10:54,800 --> 00:10:57,360
 But it's a harmless way I suppose.

170
00:10:57,360 --> 00:10:59,080
 They're not hurting the guy.

171
00:10:59,080 --> 00:11:04,810
 So it is a kind of an ingenious way to get your point

172
00:11:04,810 --> 00:11:06,120
 across.

173
00:11:06,120 --> 00:11:14,900
 It is said that the advisor never was very careful to open

174
00:11:14,900 --> 00:11:19,080
 his mouth in the future.

175
00:11:19,080 --> 00:11:24,960
 The point is then this cripple became quite wealthy.

176
00:11:24,960 --> 00:11:31,520
 The king showered honor and gain upon him.

177
00:11:31,520 --> 00:11:37,000
 Our story comes in, our verse comes in when...

178
00:11:37,000 --> 00:11:40,280
 The story of our verse comes in when another man saw this

179
00:11:40,280 --> 00:11:42,560
 and was quite impressed and thought,

180
00:11:42,560 --> 00:11:48,180
 "Wow, if I could learn that skill, I could probably gain

181
00:11:48,180 --> 00:11:53,720
 favor and gain and fame as well."

182
00:11:53,720 --> 00:11:54,720
 Here's the problem.

183
00:11:54,720 --> 00:12:00,420
 When someone does something for the purpose of fame and

184
00:12:00,420 --> 00:12:02,240
 gain and all this.

185
00:12:02,240 --> 00:12:10,080
 So his desire was already unwholesome.

186
00:12:10,080 --> 00:12:11,080
 His desire to learn.

187
00:12:11,080 --> 00:12:14,310
 So he approaches this cripple and the cripple doesn't want

188
00:12:14,310 --> 00:12:16,000
 to teach him and he persuades

189
00:12:16,000 --> 00:12:19,000
 him to teach him.

190
00:12:19,000 --> 00:12:24,320
 Finally, he says, "Fine, I'll teach you what I know."

191
00:12:24,320 --> 00:12:32,210
 And the cripple, he does this, he butters up the cripple

192
00:12:32,210 --> 00:12:35,640
 and takes care of him and acts

193
00:12:35,640 --> 00:12:37,650
 as his servant for a while until finally the cripple says,

194
00:12:37,650 --> 00:12:38,760
 "Fine, I'll teach you what I

195
00:12:38,760 --> 00:12:39,760
 know."

196
00:12:39,760 --> 00:12:43,320
 And so he teaches him how to use this sling until the man

197
00:12:43,320 --> 00:12:44,960
 gets quite good at it.

198
00:12:44,960 --> 00:12:49,740
 And so the short of it is that we have this man who's

199
00:12:49,740 --> 00:12:53,240
 learned how to use a sling and is

200
00:12:53,240 --> 00:12:57,440
 keen to show off.

201
00:12:57,440 --> 00:13:00,880
 And so the cripple says, "So how are you going to test?

202
00:13:00,880 --> 00:13:02,120
 What are you going to do with this skill?"

203
00:13:02,120 --> 00:13:06,410
 And he says, "Well, I figure I'll find something to shoot

204
00:13:06,410 --> 00:13:08,600
 at so that people can know what a

205
00:13:08,600 --> 00:13:09,600
 sharpshooter I am.

206
00:13:09,600 --> 00:13:12,360
 I'll figure I'll shoot at a cow or maybe I'll shoot at a

207
00:13:12,360 --> 00:13:13,480
 man somewhere."

208
00:13:13,480 --> 00:13:16,920
 And the cripple says, "What do you talk?"

209
00:13:16,920 --> 00:13:20,840
 You can't just go around shooting things and living beings.

210
00:13:20,840 --> 00:13:24,160
 If you shoot a cow, you get fined 500 gold pieces.

211
00:13:24,160 --> 00:13:28,070
 If you shoot a human, you get fined 1,000 gold pieces or

212
00:13:28,070 --> 00:13:29,600
 something like that.

213
00:13:29,600 --> 00:13:34,020
 He says, "What you have to do is you have to find something

214
00:13:34,020 --> 00:13:36,440
 to shoot at that nobody's

215
00:13:36,440 --> 00:13:41,190
 going to miss that has no father, no mother, nobody to

216
00:13:41,190 --> 00:13:43,040
 complain about."

217
00:13:43,040 --> 00:13:51,120
 So maybe some of you can tell where this is going.

218
00:13:51,120 --> 00:13:52,120
 He says, "Good idea.

219
00:13:52,120 --> 00:13:54,240
 I'll find something like that."

220
00:13:54,240 --> 00:13:57,900
 So he looks around and he sees a cow and he says, "No, that

221
00:13:57,900 --> 00:13:59,480
 has an owner and the owner

222
00:13:59,480 --> 00:14:03,000
 is going to be concerned about it."

223
00:14:03,000 --> 00:14:05,720
 He looks and he sees a man and he thinks, "Yeah, right.

224
00:14:05,720 --> 00:14:06,720
 That's true.

225
00:14:06,720 --> 00:14:10,490
 This man has a mother and a father and a wife and children

226
00:14:10,490 --> 00:14:11,920
 and not a good idea."

227
00:14:11,920 --> 00:14:16,560
 And he looks around and he sees a child.

228
00:14:16,560 --> 00:14:23,070
 Until finally he sees a Pateka Buddha and he says, "This

229
00:14:23,070 --> 00:14:24,120
 man."

230
00:14:24,120 --> 00:14:26,980
 He sees him going on alms round and it's amazing how he

231
00:14:26,980 --> 00:14:28,800
 could just miss the fact that this

232
00:14:28,800 --> 00:14:32,490
 is an enlightened being, but that's the case with people

233
00:14:32,490 --> 00:14:34,440
 who are so blinded by their own

234
00:14:34,440 --> 00:14:40,400
 desires and their attachments.

235
00:14:40,400 --> 00:14:47,630
 And he says, "This man, no father, no mother, no wife, no

236
00:14:47,630 --> 00:14:49,360
 children.

237
00:14:49,360 --> 00:14:58,200
 This man is the perfect target and no one will miss him."

238
00:14:58,200 --> 00:15:02,710
 And so he takes this thing and he shoots the Pateka Buddha

239
00:15:02,710 --> 00:15:05,240
 right in the head and the Pateka

240
00:15:05,240 --> 00:15:06,240
 Buddha dies.

241
00:15:06,240 --> 00:15:13,440
 He thinks, "Oh, I'm a good shot with that."

242
00:15:13,440 --> 00:15:20,210
 And the people of the village, of course, are very much

243
00:15:20,210 --> 00:15:22,560
 attached to the Pateka Buddha

244
00:15:22,560 --> 00:15:24,850
 and they find out he hasn't come for alms and so they go

245
00:15:24,850 --> 00:15:26,320
 and look for him and they find

246
00:15:26,320 --> 00:15:27,320
 that he's passed away.

247
00:15:27,320 --> 00:15:33,000
 He's been killed and murdered and this silly person comes

248
00:15:33,000 --> 00:15:36,000
 up and says, "Yeah, that was

249
00:15:36,000 --> 00:15:37,000
 me.

250
00:15:37,000 --> 00:15:38,000
 I was...

251
00:15:38,000 --> 00:15:45,200
 Look at what a sharp shooter I am killing this guy."

252
00:15:45,200 --> 00:15:49,820
 The story is that the text says that he was able to shoot a

253
00:15:49,820 --> 00:15:51,920
 stone in one ear and out the

254
00:15:51,920 --> 00:15:52,920
 other ear.

255
00:15:52,920 --> 00:16:03,210
 So according to the text, yes, we can agree that's quite a

256
00:16:03,210 --> 00:16:04,720
 feat.

257
00:16:04,720 --> 00:16:10,530
 And yeah, his bragging doesn't quite have the expected

258
00:16:10,530 --> 00:16:11,720
 result.

259
00:16:11,720 --> 00:16:15,320
 They lynch him and I think they beat him and kill him and

260
00:16:15,320 --> 00:16:17,240
 he ends up going to hell as a

261
00:16:17,240 --> 00:16:18,760
 result.

262
00:16:18,760 --> 00:16:26,940
 And once he's finished his stint in hell, he is reborn as a

263
00:16:26,940 --> 00:16:30,800
 ghost on Vulture Speak and

264
00:16:30,800 --> 00:16:36,360
 has hammers crashing down upon his skull.

265
00:16:36,360 --> 00:16:43,770
 And the Buddha says, "So you see, yawadeva anataya,

266
00:16:43,770 --> 00:16:48,280
 knowledge arises in a fool to his

267
00:16:48,280 --> 00:16:50,720
 or her disadvantage.

268
00:16:50,720 --> 00:16:53,520
 It destroys their good luck.

269
00:16:53,520 --> 00:16:59,240
 It crushes their head."

270
00:16:59,240 --> 00:17:01,860
 The text, the verse is obviously metaphorical and so you

271
00:17:01,860 --> 00:17:03,320
 don't need the story of a ghost

272
00:17:03,320 --> 00:17:07,840
 having his head crushed, but that's the reference there.

273
00:17:07,840 --> 00:17:11,600
 It actually can physically crush your head if you're not

274
00:17:11,600 --> 00:17:13,800
 careful and be born with hammers

275
00:17:13,800 --> 00:17:15,600
 crushing your head.

276
00:17:15,600 --> 00:17:18,120
 But this isn't the point of the verse.

277
00:17:18,120 --> 00:17:24,840
 The point of the verse is that knowledge in the wrong hands

278
00:17:24,840 --> 00:17:27,560
 isn't useful at all.

279
00:17:27,560 --> 00:17:30,280
 We use it to our detriment.

280
00:17:30,280 --> 00:17:37,990
 In a worldly sense, this is an important philosophy, an

281
00:17:37,990 --> 00:17:41,400
 important ethical point when that knowledge

282
00:17:41,400 --> 00:17:44,320
 is ethically variable.

283
00:17:44,320 --> 00:17:48,280
 It's not always a good thing to have knowledge.

284
00:17:48,280 --> 00:17:52,640
 More knowledge isn't always a good thing.

285
00:17:52,640 --> 00:17:56,840
 And the variable is whether someone is wise or not.

286
00:17:56,840 --> 00:17:59,410
 Wisdom and intelligence are obviously two very different

287
00:17:59,410 --> 00:18:00,040
 things.

288
00:18:00,040 --> 00:18:05,860
 You can have wise people with very little knowledge or

289
00:18:05,860 --> 00:18:07,400
 intelligence.

290
00:18:07,400 --> 00:18:11,150
 In fact, you could argue that to some extent they're in

291
00:18:11,150 --> 00:18:12,600
versely related.

292
00:18:12,600 --> 00:18:18,750
 The more intelligence a person has, the harder it is for

293
00:18:18,750 --> 00:18:20,800
 them to be wise.

294
00:18:20,800 --> 00:18:24,600
 It's easier to become conceited about your knowledge.

295
00:18:24,600 --> 00:18:28,160
 It's easy to become attached to the gain that comes from

296
00:18:28,160 --> 00:18:29,520
 your knowledge.

297
00:18:29,520 --> 00:18:34,970
 And so even you find that professors of religion and even

298
00:18:34,970 --> 00:18:38,200
 professors who study Buddhism and

299
00:18:38,200 --> 00:18:41,770
 go all the way to get their PhD and even become teachers in

300
00:18:41,770 --> 00:18:43,880
 their own right can be very silly

301
00:18:43,880 --> 00:18:52,520
 at times and have strange and bizarre views and beliefs.

302
00:18:52,520 --> 00:18:57,130
 Obviously in a worldly sense, it's even more clear how

303
00:18:57,130 --> 00:18:59,140
 knowledge in the wrong hands can

304
00:18:59,140 --> 00:19:07,020
 lead to suffering, the knowledge of nuclear, nuclear, what

305
00:19:07,020 --> 00:19:10,840
 do you call it, reactivity,

306
00:19:10,840 --> 00:19:14,600
 whatever it is that led them to create atom bomb.

307
00:19:14,600 --> 00:19:19,690
 So nuclear power could be used for benefit, could be used

308
00:19:19,690 --> 00:19:21,360
 for detriment.

309
00:19:21,360 --> 00:19:26,220
 In fact, even nuclear power itself, as we can see, has the

310
00:19:26,220 --> 00:19:27,800
 potential to cause great

311
00:19:27,800 --> 00:19:32,280
 suffering.

312
00:19:32,280 --> 00:19:36,520
 Knowledge is not always a good thing.

313
00:19:36,520 --> 00:19:40,950
 Much of our knowledge even today, as we can see now, is

314
00:19:40,950 --> 00:19:43,600
 because of our lack of wisdom,

315
00:19:43,600 --> 00:19:54,850
 we're using it to gain wealth and prosperity in the present

316
00:19:54,850 --> 00:19:55,320
 moment

317
00:19:55,320 --> 00:19:59,780
 at the expense of our future, as it seems with the change

318
00:19:59,780 --> 00:20:01,880
 of the climate based on human

319
00:20:01,880 --> 00:20:06,400
 activity and so on.

320
00:20:06,400 --> 00:20:10,010
 Knowledge in fact seems to be one of those, one thing that

321
00:20:10,010 --> 00:20:11,240
's set to kill us.

322
00:20:11,240 --> 00:20:17,240
 Before we had engrossed ourselves in this knowledge that

323
00:20:17,240 --> 00:20:20,200
 has led to our technology,

324
00:20:20,200 --> 00:20:25,080
 we were never in danger of destroying our environment and

325
00:20:25,080 --> 00:20:27,520
 the environment on which we

326
00:20:27,520 --> 00:20:28,520
 depend.

327
00:20:28,520 --> 00:20:29,760
 It's an interesting example.

328
00:20:29,760 --> 00:20:30,880
 The same goes with nuclear power.

329
00:20:30,880 --> 00:20:39,360
 We were never at risk of destroying our civilizations.

330
00:20:39,360 --> 00:20:45,680
 So knowledge is power and power of course is ethically

331
00:20:45,680 --> 00:20:49,520
 variable, can be beneficial,

332
00:20:49,520 --> 00:20:56,100
 can be used to create great wholesomeness, can be used to

333
00:20:56,100 --> 00:20:59,520
 create great evil as well.

334
00:20:59,520 --> 00:21:03,030
 But on the meditation side of things, there's also a point

335
00:21:03,030 --> 00:21:04,640
 to be made here that knowledge

336
00:21:04,640 --> 00:21:07,480
 of meditation can sometimes be to our detriment.

337
00:21:07,480 --> 00:21:11,440
 If you know too much, it can actually get in the way of

338
00:21:11,440 --> 00:21:12,920
 your practice.

339
00:21:12,920 --> 00:21:17,440
 Certainly it doesn't necessarily help you practice.

340
00:21:17,440 --> 00:21:20,650
 Knowledge accompanied by practice on the other hand, again

341
00:21:20,650 --> 00:21:22,520
 the power of knowledge can be

342
00:21:22,520 --> 00:21:26,070
 a great thing if you're actually practicing to have

343
00:21:26,070 --> 00:21:28,760
 knowledge of of course the correct

344
00:21:28,760 --> 00:21:34,210
 way to practice, to have knowledge of background, Buddhist

345
00:21:34,210 --> 00:21:37,120
 theory, the abhidhamma, the nature

346
00:21:37,120 --> 00:21:42,100
 of the mind, of wholesomeness, even knowledge of these

347
00:21:42,100 --> 00:21:43,600
 stories.

348
00:21:43,600 --> 00:21:47,880
 If a person grasps them in the wrong way and becomes

349
00:21:47,880 --> 00:21:50,920
 skeptical, thinking such things could

350
00:21:50,920 --> 00:21:55,470
 never happen, could never exist, how is it possible for

351
00:21:55,470 --> 00:21:57,840
 such a ghost being to be born?

352
00:21:57,840 --> 00:22:01,520
 To focus on that aspect of it and become full of doubt.

353
00:22:01,520 --> 00:22:07,230
 If they don't have the experience to see how such a thing

354
00:22:07,230 --> 00:22:10,320
 is possible, how the mind can

355
00:22:10,320 --> 00:22:14,320
 create such an existence, then it will actually create

356
00:22:14,320 --> 00:22:16,840
 doubt in the mind and be a detriment

357
00:22:16,840 --> 00:22:17,840
 to the practice.

358
00:22:17,840 --> 00:22:21,480
 If on the other hand one grasps it correctly and sees the

359
00:22:21,480 --> 00:22:23,600
 message behind it and moreover

360
00:22:23,600 --> 00:22:28,610
 is able, even better is able to understand how such a thing

361
00:22:28,610 --> 00:22:30,960
 could come about, is able

362
00:22:30,960 --> 00:22:36,600
 to see how the mind works and experience how it's able to

363
00:22:36,600 --> 00:22:39,600
 create such an existence.

364
00:22:39,600 --> 00:22:43,310
 Then it can be to one's advantage reminding one of the

365
00:22:43,310 --> 00:22:45,840
 potential for suffering for those

366
00:22:45,840 --> 00:22:57,280
 who are bent upon the wrong path and un-wholesomeness.

367
00:22:57,280 --> 00:22:59,780
 This is just one more lesson for us to keep in mind that

368
00:22:59,780 --> 00:23:01,240
 knowledge and wisdom are two

369
00:23:01,240 --> 00:23:03,280
 very different things.

370
00:23:03,280 --> 00:23:07,780
 Simply studying the Buddha's teaching, even listening to

371
00:23:07,780 --> 00:23:10,480
 watching these videos and reading

372
00:23:10,480 --> 00:23:14,940
 about the Buddha's teaching isn't enough, it isn't a

373
00:23:14,940 --> 00:23:18,040
 substitute for actually practicing

374
00:23:18,040 --> 00:23:21,720
 the Buddha's teaching.

375
00:23:21,720 --> 00:23:26,590
 More food for thought from the Buddha and start in the Dham

376
00:23:26,590 --> 00:23:27,600
mapada.

377
00:23:27,600 --> 00:23:30,560
 That's all for today.

378
00:23:30,560 --> 00:23:33,040
 Thank you all for tuning in.

