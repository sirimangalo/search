1
00:00:00,000 --> 00:00:17,080
 Good evening everyone from Broadcasting Live, May 20th.

2
00:00:17,080 --> 00:00:27,220
 Today's quote is from the Majimalika. Number 122, the Mahas

3
00:00:27,220 --> 00:00:30,360
unyata Sutta, which is...

4
00:00:30,360 --> 00:00:35,360
 It's got a lot in it.

5
00:00:35,360 --> 00:00:40,760
 And the very last thing the Buddha says is,

6
00:00:40,760 --> 00:00:53,760
 "I'm not going to treat you like wet clay."

7
00:00:53,760 --> 00:01:12,760
 "Mita vata yas muda jalata masa vata vata yas."

8
00:01:12,760 --> 00:01:23,760
 "Nah parakamisaami yata kumbh bakar o amkhe amkamate."

9
00:01:23,760 --> 00:01:29,760
 "Nah wo han ananda tata parakamisaami."

10
00:01:29,760 --> 00:01:34,760
 "I will not treat you, Ananda."

11
00:01:34,760 --> 00:01:41,760
 "Yata kumbh bakar o amkhe amkamate."

12
00:01:41,760 --> 00:01:51,760
 "Amkha" means uncooked.

13
00:01:51,760 --> 00:01:55,760
 The uncooked, or the unbaked clay, because

14
00:01:55,760 --> 00:02:03,700
 the potter has to treat their clay with care. The clay is

15
00:02:03,700 --> 00:02:04,760
 wet.

16
00:02:04,760 --> 00:02:09,760
 You have to be very careful with it.

17
00:02:09,760 --> 00:02:11,760
 Something very fragile.

18
00:02:11,760 --> 00:02:15,760
 So the Buddha said, "I'm not going to treat you with as

19
00:02:15,760 --> 00:02:17,760
 though you're fragile."

20
00:02:17,760 --> 00:02:24,520
 "I'm not going to go easy on you. I'm not going to just

21
00:02:24,520 --> 00:02:26,760
 keep gloves."

22
00:02:26,760 --> 00:02:35,760
 "I'm not going to pamper you. I'm not going to."

23
00:02:35,760 --> 00:02:38,760
 "I'm going to make it easy on you."

24
00:02:38,760 --> 00:02:40,760
 That's what he's saying.

25
00:02:40,760 --> 00:02:48,760
 By doing that, he's softening the blow of what he says.

26
00:02:48,760 --> 00:02:51,810
 Because the teaching is quite difficult, is what he's

27
00:02:51,810 --> 00:02:52,760
 saying.

28
00:02:52,760 --> 00:02:57,760
 He has only our best interest, is what he's saying.

29
00:02:57,760 --> 00:02:59,760
 Because meditation is tough.

30
00:02:59,760 --> 00:03:05,760
 The path to enlightenment is not an easy thing.

31
00:03:05,760 --> 00:03:09,460
 He says, "Repeatedly restraining you, I shall speak to you

32
00:03:09,460 --> 00:03:09,760
."

33
00:03:09,760 --> 00:03:19,760
 "The sound core will stand the test."

34
00:03:19,760 --> 00:03:27,390
 "Yosaro sotasate," "saro," "sara" means the core, but it

35
00:03:27,390 --> 00:03:32,760
 also means that which has substance.

36
00:03:32,760 --> 00:03:40,760
 If a person has substance, they will stand.

37
00:03:40,760 --> 00:03:46,760
 So he's addressing monks who are spending time in society,

38
00:03:46,760 --> 00:04:05,050
 who are in danger of getting caught up in the world, and he

39
00:04:05,050 --> 00:04:05,760
 wants to address them in such a way

40
00:04:05,760 --> 00:04:11,020
 that they focus on things that are more sara, more

41
00:04:11,020 --> 00:04:12,760
 beneficial.

42
00:04:12,760 --> 00:04:17,760
 And so he offers them this teaching on voidless,

43
00:04:17,760 --> 00:04:23,750
 this teaching on non-self, really, that things are not,

44
00:04:23,750 --> 00:04:25,760
 that things are empty.

45
00:04:25,760 --> 00:04:31,160
 All of the things that we strive for, all of the things

46
00:04:31,160 --> 00:04:32,760
 that we cling to,

47
00:04:32,760 --> 00:04:34,760
 they in the end don't have substance.

48
00:04:34,760 --> 00:04:36,760
 They come and they go.

49
00:04:36,760 --> 00:04:40,120
 We think of our belongings, our possessions, even our own

50
00:04:40,120 --> 00:04:43,760
 bodies, our own minds are unstable.

51
00:04:43,760 --> 00:04:51,370
 We can't hold on to them. We can't keep them the way they

52
00:04:51,370 --> 00:04:52,760
 are.

53
00:04:52,760 --> 00:04:59,530
 He gives something that's hard to understand, but even

54
00:04:59,530 --> 00:05:06,760
 harder, hard to accept, let alone understand,

55
00:05:06,760 --> 00:05:16,690
 because we're very much interested in and attached to the

56
00:05:16,690 --> 00:05:24,760
 things in the world and the ways of the world.

57
00:05:24,760 --> 00:05:29,240
 And so even with meditation, we come with the idea that it

58
00:05:29,240 --> 00:05:34,760
's going to be pleasant and stable and controllable.

59
00:05:34,760 --> 00:05:39,760
 We're going to find a way to control our minds.

60
00:05:39,760 --> 00:05:42,430
 We didn't figure that controlling and letting go are two

61
00:05:42,430 --> 00:05:43,760
 very different things.

62
00:05:43,760 --> 00:05:50,760
 In fact, letting go is much more difficult than the two.

63
00:05:50,760 --> 00:05:56,140
 Well, complete control being impossible, but the act of

64
00:05:56,140 --> 00:05:59,760
 trying to control things is pretty simple.

65
00:05:59,760 --> 00:06:03,760
 We just try and force ourselves this way.

66
00:06:03,760 --> 00:06:07,760
 It's not very fruitful, but it's an easy way out.

67
00:06:07,760 --> 00:06:11,910
 Just try and control your life, try and control everything,

68
00:06:11,910 --> 00:06:14,760
 force everything to be the way you want it.

69
00:06:14,760 --> 00:06:17,760
 In other words, try to always get what you want.

70
00:06:17,760 --> 00:06:20,760
 Never get what you don't want.

71
00:06:20,760 --> 00:06:22,760
 But letting go is quite different.

72
00:06:22,760 --> 00:06:24,760
 And letting go is much more difficult.

73
00:06:24,760 --> 00:06:27,760
 It's been difficult just to even accomplish.

74
00:06:27,760 --> 00:06:32,760
 It means not reacting.

75
00:06:32,760 --> 00:06:34,760
 It's not judging.

76
00:06:34,760 --> 00:06:35,760
 It's not clinging.

77
00:06:35,760 --> 00:06:41,760
 It's not avoiding.

78
00:06:41,760 --> 00:06:46,760
 It's learning to open yourself up.

79
00:06:46,760 --> 00:06:50,290
 Open yourself up to what you're feeling, opening yourself

80
00:06:50,290 --> 00:06:52,760
 up to what you're thinking,

81
00:06:52,760 --> 00:06:56,760
 opening yourself up to now what you're experiencing now,

82
00:06:56,760 --> 00:07:04,180
 and being okay with that, whatever it is, making your peace

83
00:07:04,180 --> 00:07:04,760
 with it.

84
00:07:04,760 --> 00:07:10,760
 So you stop trying to fix it, stop trying to hold on to it,

85
00:07:10,760 --> 00:07:13,760
 stop trying to get what it is that you want.

86
00:07:13,760 --> 00:07:17,760
 Our minds are screaming like little children all the time.

87
00:07:17,760 --> 00:07:21,180
 I want this, I want that, I don't want this, I don't want

88
00:07:21,180 --> 00:07:21,760
 that.

89
00:07:21,760 --> 00:07:27,760
 Meditation is about growing up.

90
00:07:27,760 --> 00:07:33,310
 It's about finally becoming mature in the sense of being

91
00:07:33,310 --> 00:07:37,760
 strong and wise and capable.

92
00:07:37,760 --> 00:07:40,760
 It's the most difficult thing you can do.

93
00:07:40,760 --> 00:07:45,760
 A lot of blood, sweat and tears along the way.

94
00:07:45,760 --> 00:07:48,970
 And so the Buddha says, "I'm not going to sugarcoat this

95
00:07:48,970 --> 00:07:49,760
 for you.

96
00:07:49,760 --> 00:07:52,760
 There's no way to sugarcoat this."

97
00:07:52,760 --> 00:07:55,760
 We can soften the blow.

98
00:07:55,760 --> 00:08:00,760
 We can remind you that it's important to see, well,

99
00:08:00,760 --> 00:08:04,910
 that it's an important thing, it's a serious thing we're

100
00:08:04,910 --> 00:08:05,760
 doing.

101
00:08:05,760 --> 00:08:10,760
 Prepare you for it mentally, emotionally.

102
00:08:10,760 --> 00:08:14,760
 But in the end, we have to go the distance.

103
00:08:14,760 --> 00:08:18,310
 If you want an experience that is pleasant and comfortable,

104
00:08:18,310 --> 00:08:18,760
 that's easy.

105
00:08:18,760 --> 00:08:23,180
 We can make a spa or something where people come and get

106
00:08:23,180 --> 00:08:23,760
 massages

107
00:08:23,760 --> 00:08:33,760
 or get to sit in the jacuzzi or hot spring or something.

108
00:08:33,760 --> 00:08:37,760
 But it wouldn't solve anything.

109
00:08:37,760 --> 00:08:40,760
 It wouldn't fix your problems.

110
00:08:40,760 --> 00:08:42,760
 It wouldn't lead to true peace.

111
00:08:42,760 --> 00:08:50,760
 It wouldn't change anything.

112
00:08:50,760 --> 00:08:54,460
 We can do comfortable meditation where we sit and think

113
00:08:54,460 --> 00:08:55,760
 good thoughts.

114
00:08:55,760 --> 00:08:57,760
 It's valid meditation, really.

115
00:08:57,760 --> 00:09:06,260
 There's meditation on love, meditation on compassion and so

116
00:09:06,260 --> 00:09:06,760
 on.

117
00:09:06,760 --> 00:09:09,760
 Good meditation.

118
00:09:09,760 --> 00:09:15,760
 But as well, they still don't lead to true change

119
00:09:15,760 --> 00:09:19,760
 because they don't help us understand ourselves.

120
00:09:19,760 --> 00:09:24,760
 I mean, the path to true enlightenment is not complicated

121
00:09:24,760 --> 00:09:27,760
 and it's not hard to understand.

122
00:09:27,760 --> 00:09:34,760
 It's not esoteric or abstruse.

123
00:09:34,760 --> 00:09:36,760
 It's quite simple.

124
00:09:36,760 --> 00:09:39,760
 It's not easy.

125
00:09:39,760 --> 00:09:43,760
 It's something you have to really be patient with.

126
00:09:43,760 --> 00:09:44,760
 You don't have to push yourself.

127
00:09:44,760 --> 00:09:46,760
 You don't have to force things.

128
00:09:46,760 --> 00:09:50,760
 You don't have to stress about it.

129
00:09:50,760 --> 00:09:52,760
 But you have to be patient.

130
00:09:52,760 --> 00:09:57,760
 It's the kind of person who can sit through anything

131
00:09:57,760 --> 00:10:01,760
 or bear with anything.

132
00:10:01,760 --> 00:10:03,760
 That's the key.

133
00:10:03,760 --> 00:10:07,550
 You don't have to do anything. You just have to bear with

134
00:10:07,550 --> 00:10:07,760
 everything.

135
00:10:07,760 --> 00:10:10,760
 Once you can bear, then that means being objective.

136
00:10:10,760 --> 00:10:18,760
 It means seeing things as they are.

137
00:10:18,760 --> 00:10:19,760
 That's all it takes.

138
00:10:19,760 --> 00:10:21,760
 You just have to see things as they are.

139
00:10:21,760 --> 00:10:27,760
 You don't have to find anything new or strange or esoteric.

140
00:10:27,760 --> 00:10:29,760
 Just as they are.

141
00:10:29,760 --> 00:10:31,760
 Quite simple.

142
00:10:31,760 --> 00:10:35,300
 And that's what's so rewarding about it is that when you

143
00:10:35,300 --> 00:10:37,760
 see things as they are,

144
00:10:37,760 --> 00:10:39,760
 there's no one can take that away from you.

145
00:10:39,760 --> 00:10:42,760
 No one can make you doubt that.

146
00:10:42,760 --> 00:10:48,480
 Once you see your mind, once you see the nature of your

147
00:10:48,480 --> 00:10:49,760
 experiences,

148
00:10:49,760 --> 00:10:52,580
 the things that you cling to, things that you run away from

149
00:10:52,580 --> 00:10:52,760
,

150
00:10:52,760 --> 00:10:57,110
 once you see that they're not so scary, that they're not so

151
00:10:57,110 --> 00:10:57,760
 attractive,

152
00:10:57,760 --> 00:11:01,760
 that they just are.

153
00:11:01,760 --> 00:11:05,760
 That no one can make you cling to anything.

154
00:11:05,760 --> 00:11:11,990
 You can't be forced into fleeing or forced into fear or

155
00:11:11,990 --> 00:11:20,760
 anxiety or stress.

156
00:11:20,760 --> 00:11:24,760
 Because you have understanding.

157
00:11:24,760 --> 00:11:25,760
 That's all you have.

158
00:11:25,760 --> 00:11:28,270
 You don't have fear, you don't have anger, you don't have

159
00:11:28,270 --> 00:11:31,760
 addiction, attachment,

160
00:11:31,760 --> 00:11:34,760
 frustration, boredom, sadness, none of that.

161
00:11:34,760 --> 00:11:36,760
 You just have understanding.

162
00:11:36,760 --> 00:11:40,760
 You just see things clearly.

163
00:11:40,760 --> 00:11:42,760
 What does that do for you?

164
00:11:42,760 --> 00:11:44,760
 What are all the good things that come?

165
00:11:44,760 --> 00:11:45,760
 What good is it?

166
00:11:45,760 --> 00:11:47,760
 You don't have to work so hard.

167
00:11:47,760 --> 00:11:54,760
 Why can't we just take it easy?

168
00:11:54,760 --> 00:12:00,760
 When your mind is pure, when your mind is clear, what is

169
00:12:00,760 --> 00:12:00,760
 the benefit?

170
00:12:00,760 --> 00:12:03,760
 The true benefit is purity.

171
00:12:03,760 --> 00:12:07,760
 The real benefit of meditation is purity.

172
00:12:07,760 --> 00:12:14,760
 Because you can avoid the problem, you can avoid your anger

173
00:12:14,760 --> 00:12:15,760
 and aversion,

174
00:12:15,760 --> 00:12:22,760
 your greed and addiction.

175
00:12:22,760 --> 00:12:28,080
 But only when you understand, only when you understand

176
00:12:28,080 --> 00:12:28,760
 things,

177
00:12:28,760 --> 00:12:31,760
 can you become pure.

178
00:12:31,760 --> 00:12:34,760
 The mind that is objective is the mind that is pure.

179
00:12:34,760 --> 00:12:36,760
 The mind that just sees things as they are.

180
00:12:36,760 --> 00:12:37,760
 It's quite simple.

181
00:12:37,760 --> 00:12:43,760
 Here, now, it's not theoretical, it's not philosophical,

182
00:12:43,760 --> 00:12:44,760
 it just is.

183
00:12:44,760 --> 00:12:48,760
 You're just sitting, sitting, walking, you're walking,

184
00:12:48,760 --> 00:12:50,760
 seeing, you're seeing.

185
00:12:50,760 --> 00:12:52,760
 The mind is very pure.

186
00:12:52,760 --> 00:12:56,760
 With a pure mind, you're able to overcome sadness, sorrow,

187
00:12:56,760 --> 00:12:59,760
 lamentation, despair.

188
00:12:59,760 --> 00:13:02,760
 You're able to become all sorts of mental sickness,

189
00:13:02,760 --> 00:13:09,760
 like depression, anxiety, insomnia, phobia, anger issues,

190
00:13:09,760 --> 00:13:10,760
 addiction issues,

191
00:13:10,760 --> 00:13:12,760
 all these things.

192
00:13:12,760 --> 00:13:16,760
 You're able to overcome them through the purity.

193
00:13:16,760 --> 00:13:21,760
 You're like pure water washing away all of these things

194
00:13:21,760 --> 00:13:27,760
 that seem so difficult to overcome, so complicated

195
00:13:27,760 --> 00:13:33,760
 and so unsolvable, impossible.

196
00:13:33,760 --> 00:13:39,870
 In fact, they're quite simply washed away with the purity

197
00:13:39,870 --> 00:13:42,760
 of the mind.

198
00:13:42,760 --> 00:13:45,760
 When you do that, you don't suffer.

199
00:13:45,760 --> 00:13:48,760
 You don't suffer from all the suffering that comes from

200
00:13:48,760 --> 00:13:48,760
 these mental,

201
00:13:48,760 --> 00:13:52,760
 the system, mental upsets, the problems in our mind.

202
00:13:52,760 --> 00:13:54,760
 When they're gone, there's no suffering.

203
00:13:54,760 --> 00:13:58,240
 Even if you're in great physical pain, you don't react to

204
00:13:58,240 --> 00:13:58,760
 it.

205
00:13:58,760 --> 00:14:00,760
 You're at peace, even with physical pain.

206
00:14:00,760 --> 00:14:05,760
 Even that, because you can't actually get what you want all

207
00:14:05,760 --> 00:14:05,760
 the time.

208
00:14:05,760 --> 00:14:09,830
 You can't actually ensure that you won't experience

209
00:14:09,830 --> 00:14:11,760
 unpleasant things.

210
00:14:11,760 --> 00:14:15,760
 So the only way to be free, to be truly at peace,

211
00:14:15,760 --> 00:14:19,760
 is to learn to let go, to learn to stop reacting,

212
00:14:19,760 --> 00:14:24,760
 to become pure in mind.

213
00:14:24,760 --> 00:14:26,760
 This is called the right path.

214
00:14:26,760 --> 00:14:29,790
 It does as it puts you on the right path, whatever you do

215
00:14:29,790 --> 00:14:30,760
 in your life,

216
00:14:30,760 --> 00:14:33,760
 however you live your life, you do it right.

217
00:14:33,760 --> 00:14:37,760
 You don't do it with an angry mind or with a greedy mind

218
00:14:37,760 --> 00:14:39,760
 or with a deluded mind.

219
00:14:39,760 --> 00:14:43,040
 You do it with a clear and with a pure mind, whatever you

220
00:14:43,040 --> 00:14:43,760
 do.

221
00:14:43,760 --> 00:14:46,760
 If you're a monk or if you're a lay person,

222
00:14:46,760 --> 00:14:49,760
 if you're a millionaire, a millionaire,

223
00:14:49,760 --> 00:14:54,760
 you do it right with a pure mind.

224
00:14:54,760 --> 00:14:56,760
 And so you're free.

225
00:14:56,760 --> 00:14:57,760
 You find freedom.

226
00:14:57,760 --> 00:15:01,760
 You find freedom in this life, no suffering in this life.

227
00:15:01,760 --> 00:15:06,350
 If you're born again, you're born in a place that is free

228
00:15:06,350 --> 00:15:08,760
 and pure.

229
00:15:08,760 --> 00:15:12,760
 It's the freedom of mind.

230
00:15:12,760 --> 00:15:15,500
 Whether you're in prison or whether you're on top of a

231
00:15:15,500 --> 00:15:15,760
 mountain,

232
00:15:15,760 --> 00:15:23,760
 wherever you are, you have the freedom of mind.

233
00:15:23,760 --> 00:15:25,760
 No one can take that away from you.

234
00:15:25,760 --> 00:15:30,970
 No one can enslave you because you're at peace with

235
00:15:30,970 --> 00:15:32,760
 yourself.

236
00:15:32,760 --> 00:15:36,760
 You're free.

237
00:15:36,760 --> 00:15:43,760
 So I didn't talk too much about Mahasunya, Sunyata Sutta.

238
00:15:43,760 --> 00:15:45,760
 It's a bit complicated, I think.

239
00:15:45,760 --> 00:15:49,760
 Not really my forte to go through all that.

240
00:15:49,760 --> 00:15:51,760
 It goes through Samatha and Vipassana.

241
00:15:51,760 --> 00:15:53,760
 It's an interesting sutta, to be sure.

242
00:15:53,760 --> 00:15:56,760
 Definitely worth reading.

243
00:15:56,760 --> 00:15:59,760
 But this quote sort of stands apart,

244
00:15:59,760 --> 00:16:05,460
 and the point of the quote is to remind us that this is

245
00:16:05,460 --> 00:16:06,760
 difficult,

246
00:16:06,760 --> 00:16:10,760
 and that we have to push ourselves and we have to be pushed

247
00:16:10,760 --> 00:16:10,760
.

248
00:16:10,760 --> 00:16:12,760
 We have to accept these teachings.

249
00:16:12,760 --> 00:16:15,760
 The Buddha said, "If you love me, if you care for me,

250
00:16:15,760 --> 00:16:18,760
 it's not about clinging to me.

251
00:16:18,760 --> 00:16:21,760
 It's about following my teachings."

252
00:16:21,760 --> 00:16:24,760
 If you don't listen to my teachings,

253
00:16:24,760 --> 00:16:30,760
 that's what it means to not really care.

254
00:16:30,760 --> 00:16:33,760
 That's all we're concerned about is the point.

255
00:16:33,760 --> 00:16:35,760
 You follow the teachings.

256
00:16:35,760 --> 00:16:40,020
 If you do the teachings, you practice them because they're

257
00:16:40,020 --> 00:16:40,760
 tough.

258
00:16:40,760 --> 00:16:47,730
 That's what separates someone who cares from someone who

259
00:16:47,730 --> 00:16:48,760
 doesn't care.

260
00:16:48,760 --> 00:16:53,760
 It's not whether you are Buddhist or something.

261
00:16:53,760 --> 00:16:57,750
 It's about whether you care enough to put the teachings

262
00:16:57,750 --> 00:16:58,760
 into practice.

263
00:16:58,760 --> 00:17:03,760
 It's difficult teaching.

264
00:17:03,760 --> 00:17:13,760
 Anyway, so that's our bit of dhamma for tonight.

265
00:17:13,760 --> 00:17:17,760
 See, we have a couple of questions so I can answer them.

266
00:17:17,760 --> 00:17:22,760
 You can go. That's all.

267
00:17:22,760 --> 00:17:31,760
 This can answer people's questions.

268
00:17:31,760 --> 00:17:33,760
 Is time a rupa?

269
00:17:33,760 --> 00:17:37,760
 Well, time doesn't exist. Time is not a thing.

270
00:17:37,760 --> 00:17:55,760
 Time is quality of things.

271
00:17:55,760 --> 00:18:00,910
 Did the Buddha and the Arahants have no need for constant

272
00:18:00,910 --> 00:18:01,760
 noting?

273
00:18:01,760 --> 00:18:06,760
 They're being so awake and aware and pure of mind.

274
00:18:06,760 --> 00:18:09,430
 Arahants see things objectively so they don't have to

275
00:18:09,430 --> 00:18:16,760
 practice seeing things objectively.

276
00:18:16,760 --> 00:18:17,760
 It just comes natural.

277
00:18:17,760 --> 00:18:24,760
 When they see, they know it as seeing.

278
00:18:24,760 --> 00:18:27,760
 I think that I occasionally recognize impermanence or no

279
00:18:27,760 --> 00:18:27,760
 self,

280
00:18:27,760 --> 00:18:31,760
 but I assume that the recognition is only intellectual.

281
00:18:31,760 --> 00:18:36,760
 So how does one know when the realization of impermanence

282
00:18:36,760 --> 00:18:39,760
 or non-self is of a super mundane quality?

283
00:18:39,760 --> 00:18:44,080
 Super mundane realization means you're entering into nir

284
00:18:44,080 --> 00:18:44,760
vana.

285
00:18:44,760 --> 00:18:48,760
 Seeing impermanent suffering and non-solves means to see

286
00:18:48,760 --> 00:18:49,760
 that the things,

287
00:18:49,760 --> 00:18:52,760
 it's what you're seeing as you practice.

288
00:18:52,760 --> 00:18:56,760
 You're seeing that you can't control, you can't force.

289
00:18:56,760 --> 00:18:59,830
 You're seeing that trying to force things leads to

290
00:18:59,830 --> 00:19:00,760
 suffering.

291
00:19:00,760 --> 00:19:03,950
 You're seeing that things are unstable, that your mind is

292
00:19:03,950 --> 00:19:04,760
 chaotic,

293
00:19:04,760 --> 00:19:07,760
 that the experiences you have are not certain.

294
00:19:07,760 --> 00:19:13,090
 There's no warning for the way things change and come and

295
00:19:13,090 --> 00:19:13,760
 go.

296
00:19:13,760 --> 00:19:16,760
 Once you start to see that, you start to let go.

297
00:19:16,760 --> 00:19:21,280
 You stop trying to control, trying to force things because

298
00:19:21,280 --> 00:19:26,760
 you see it's not feasible.

299
00:19:26,760 --> 00:19:31,450
 It's not reasonable to think that you can control, that you

300
00:19:31,450 --> 00:19:32,760
 can be in charge.

301
00:19:32,760 --> 00:19:40,560
 That you can keep things the way they are and make them

302
00:19:40,560 --> 00:19:42,760
 satisfied.

303
00:19:42,760 --> 00:19:47,760
 So once you see that enough, it becomes a truth.

304
00:19:47,760 --> 00:19:50,760
 You see that you realize that this is the truth.

305
00:19:50,760 --> 00:19:54,130
 In the beginning, it's just hinting at it and you see it

306
00:19:54,130 --> 00:19:54,760
 clearer and clearer,

307
00:19:54,760 --> 00:19:57,900
 and finally it hits you that this is the reality, that

308
00:19:57,900 --> 00:19:58,760
 nothing's worth clinging to,

309
00:19:58,760 --> 00:20:00,760
 and the mind stops clinging.

310
00:20:00,760 --> 00:20:08,140
 That's the super mundane realization, the Cassandra's

311
00:20:08,140 --> 00:20:09,760
 freedom.

312
00:20:09,760 --> 00:20:11,760
 What do you mean about not reacting to physical pain?

313
00:20:11,760 --> 00:20:13,760
 Pain can be a hindrance of the mind.

314
00:20:13,760 --> 00:20:17,760
 It overwhelms with intensity, not so much emotionally.

315
00:20:17,760 --> 00:20:21,760
 Is there a difference?

316
00:20:21,760 --> 00:20:24,760
 Well, no, actually. Pain is not a hindrance.

317
00:20:24,760 --> 00:20:26,760
 Pain is just physical.

318
00:20:26,760 --> 00:20:29,760
 There's a difference between pain and disliking pain,

319
00:20:29,760 --> 00:20:31,760
 and that's what you have to find.

320
00:20:31,760 --> 00:20:38,620
 Once you stop disliking the pain, it's actually not a hind

321
00:20:38,620 --> 00:20:39,760
rance.

322
00:20:39,760 --> 00:20:43,760
 Think it overwhelms with intensity.

323
00:20:43,760 --> 00:20:44,760
 Not exactly.

324
00:20:44,760 --> 00:20:47,760
 I mean, in the sense that it takes your attention, yes.

325
00:20:47,760 --> 00:20:50,470
 So if it's very intense, then it'll be the only thing you

326
00:20:50,470 --> 00:20:50,760
 experience.

327
00:20:50,760 --> 00:20:53,570
 I mean, I suppose you could pass out from the intensity of

328
00:20:53,570 --> 00:20:53,760
 it,

329
00:20:53,760 --> 00:20:55,760
 but that's just a physical thing.

330
00:20:55,760 --> 00:20:59,760
 If you're not passed out, then just be mindful of it.

331
00:20:59,760 --> 00:21:08,760
 If you can do that, you can actually be at peace with it.

332
00:21:08,760 --> 00:21:11,420
 Once the mind realizes something, how come one finds

333
00:21:11,420 --> 00:21:13,760
 oneself acting with ignorance once again?

334
00:21:13,760 --> 00:21:16,760
 Are the realizations also just as impermanence?

335
00:21:16,760 --> 00:21:19,760
 Yes, realizations are impermanence.

336
00:21:19,760 --> 00:21:24,260
 You need to gather enough of them so that it becomes a

337
00:21:24,260 --> 00:21:24,760
 truth,

338
00:21:24,760 --> 00:21:27,640
 so that if you realize it as a truth, then your mind lets

339
00:21:27,640 --> 00:21:27,760
 go,

340
00:21:27,760 --> 00:21:32,150
 because the only thing that lasts is a realization of gimb

341
00:21:32,150 --> 00:21:32,760
ana.

342
00:21:32,760 --> 00:21:44,760
 Once you realize gimbana, that changes you.

343
00:21:44,760 --> 00:21:49,760
 Would you recommend drugs for intense, persistent pain?

344
00:21:49,760 --> 00:21:50,760
 I wouldn't recommend them.

345
00:21:50,760 --> 00:21:58,390
 I mean, I'd understand if people take painkillers sparingly

346
00:21:58,390 --> 00:21:58,760
,

347
00:21:58,760 --> 00:22:02,760
 but ideally, you deal with the pain.

348
00:22:02,760 --> 00:22:05,760
 If it's intense, then, yeah, use them sparingly,

349
00:22:05,760 --> 00:22:10,760
 but also try to meditate as best you can.

350
00:22:10,760 --> 00:22:34,780
 If you're really into meditation, you don't need the pain

351
00:22:34,780 --> 00:22:39,760
killers.

352
00:22:39,760 --> 00:22:46,760
 Okay, let's call it a night then.

353
00:22:46,760 --> 00:22:49,760
 Wish you all good practice.

354
00:22:49,760 --> 00:22:52,760
 See you all soon.

355
00:22:52,760 --> 00:22:58,760
 Thank you.

356
00:22:59,760 --> 00:23:00,760
 Thank you.

357
00:23:01,760 --> 00:23:02,760
 Thank you.

