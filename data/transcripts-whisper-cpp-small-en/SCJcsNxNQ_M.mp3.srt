1
00:00:00,000 --> 00:00:04,970
 Hi, welcome back to Ask a Monk. Next question comes from

2
00:00:04,970 --> 00:00:07,480
 Night Goat. Please

3
00:00:07,480 --> 00:00:11,470
 discuss the idea of God in Buddhist thinking. We are taught

4
00:00:11,470 --> 00:00:12,320
 that the Buddha

5
00:00:12,320 --> 00:00:15,580
 was not a prophet of God as in Western traditions, but to

6
00:00:15,580 --> 00:00:16,400
 whom are the prayers

7
00:00:16,400 --> 00:00:20,330
 of Buddhists offered? Clarify the meaning of pratyau and

8
00:00:20,330 --> 00:00:22,320
 how it fits with Buddhism.

9
00:00:22,320 --> 00:00:27,080
 Pratyau just means Lord or in this case God, so it's a Thai

10
00:00:27,080 --> 00:00:29,000
 word. I'm assuming

11
00:00:29,000 --> 00:00:35,060
 Night Goat is Thai or has some connection with Thailand.

12
00:00:35,060 --> 00:00:37,400
 Buddhists don't believe in

13
00:00:37,400 --> 00:00:41,400
 a creator God. The biggest problem that we have with a

14
00:00:41,400 --> 00:00:42,880
 creator God is the same

15
00:00:42,880 --> 00:00:48,030
 problem that the Greeks had and it's a really an old

16
00:00:48,030 --> 00:00:49,440
 problem that I think has

17
00:00:49,440 --> 00:00:55,200
 never, it's not possible to answer, it's not possible to

18
00:00:55,200 --> 00:00:56,080
 give an

19
00:00:56,080 --> 00:01:00,240
 adequate answer and it's the problem of suffering. There's

20
00:01:00,240 --> 00:01:02,240
 a really good book by

21
00:01:02,240 --> 00:01:06,440
 Bard Ehrman called God's Problem and I really suggest

22
00:01:06,440 --> 00:01:07,960
 anyone interested in this

23
00:01:07,960 --> 00:01:14,730
 subject take it up. The gist of the problem is if God is

24
00:01:14,730 --> 00:01:14,860
 all

25
00:01:14,860 --> 00:01:18,690
 powerful then why is there suffering? Why is there evil in

26
00:01:18,690 --> 00:01:21,000
 the world? If God is

27
00:01:21,000 --> 00:01:26,800
 all-powerful and all-loving and there are ways of twisting

28
00:01:26,800 --> 00:01:27,040
 out of

29
00:01:27,040 --> 00:01:30,600
 this, but if you read this book, this is written, this book

30
00:01:30,600 --> 00:01:30,880
 God's

31
00:01:30,880 --> 00:01:37,290
 Problem is written by an ex-pastor who is a very developed

32
00:01:37,290 --> 00:01:37,920
 Christian. He's

33
00:01:37,920 --> 00:01:44,440
 one of the leading Bible scholars in the world, but he

34
00:01:44,440 --> 00:01:45,240
 doesn't believe in God

35
00:01:45,240 --> 00:01:49,150
 and this is one of the biggest reasons, the biggest

36
00:01:49,150 --> 00:01:50,040
 problems for all of us

37
00:01:50,040 --> 00:01:54,240
 is that there's no reason for us. If there's someone who

38
00:01:54,240 --> 00:01:55,440
 created, as the

39
00:01:55,440 --> 00:01:57,840
 Buddha said, if there's someone who created the universe

40
00:01:57,840 --> 00:01:58,680
 then he's guilty

41
00:01:58,680 --> 00:02:04,830
 or that being is guilty for all of the crimes of humanity,

42
00:02:04,830 --> 00:02:05,040
 all of

43
00:02:05,040 --> 00:02:11,360
 the suffering that exists in the world. In Buddhism we

44
00:02:11,360 --> 00:02:12,720
 understand there to be

45
00:02:12,720 --> 00:02:15,820
 many levels of reality, many types of being just as there

46
00:02:15,820 --> 00:02:16,760
 are many types of

47
00:02:16,760 --> 00:02:21,000
 mind and so there are beings called Brahmas and these, I

48
00:02:21,000 --> 00:02:21,440
 would always

49
00:02:21,440 --> 00:02:23,610
 translate them as gods. I don't think there's anything

50
00:02:23,610 --> 00:02:24,840
 wrong with that sort of

51
00:02:24,840 --> 00:02:29,200
 translation because they are the higher beings and they're

52
00:02:29,200 --> 00:02:29,520
 what we

53
00:02:29,520 --> 00:02:33,720
 normally think of as a god. They haven't created the world

54
00:02:33,720 --> 00:02:35,880
 though there's an

55
00:02:35,880 --> 00:02:41,720
 interesting story that the Buddha gives that seems to be

56
00:02:41,720 --> 00:02:42,560
 quasi-

57
00:02:42,560 --> 00:02:48,080
 historical or it seems to be a historical account of why

58
00:02:48,080 --> 00:02:49,320
 people might

59
00:02:49,320 --> 00:02:54,560
 think that God created the universe and why God himself or

60
00:02:54,560 --> 00:02:55,360
 certain gods

61
00:02:55,360 --> 00:03:00,310
 might think that as well. Why this belief exists and it

62
00:03:00,310 --> 00:03:01,080
 exists on the

63
00:03:01,080 --> 00:03:06,310
 level of gods themselves or angels even because there are

64
00:03:06,310 --> 00:03:07,560
 many levels. So

65
00:03:07,560 --> 00:03:11,750
 suppose a god up at this level, gods up at this level who

66
00:03:11,750 --> 00:03:12,960
 aren't in touch with

67
00:03:12,960 --> 00:03:18,040
 the lower realms, one of them dies so to speak or moves

68
00:03:18,040 --> 00:03:19,080
 down to a lower level

69
00:03:19,080 --> 00:03:23,390
 that is in touch with the world. Suddenly, or not suddenly,

70
00:03:23,390 --> 00:03:24,360
 but over time he

71
00:03:24,360 --> 00:03:28,500
 forgets about his past lives or depending on the state no

72
00:03:28,500 --> 00:03:29,080
 longer

73
00:03:29,080 --> 00:03:34,790
 remembers the past and as a result thinks oh I'm the only

74
00:03:34,790 --> 00:03:35,640
 one and this is

75
00:03:35,640 --> 00:03:39,680
 like back before there were beings and after the Big Bang

76
00:03:39,680 --> 00:03:40,880
 you know at the end of

77
00:03:40,880 --> 00:03:45,040
 one of the last cycles and the beginning of this cycle is

78
00:03:45,040 --> 00:03:46,560
 the first one to come

79
00:03:46,560 --> 00:03:49,640
 down so he thinks I am you know in the beginning there was

80
00:03:49,640 --> 00:03:50,600
 one or there was

81
00:03:50,600 --> 00:03:53,960
 nothing and then sudden then he thinks to himself boy

82
00:03:53,960 --> 00:03:54,920
 wouldn't it be nice if

83
00:03:54,920 --> 00:03:58,750
 there was something or he gets kind of bored and after some

84
00:03:58,750 --> 00:04:01,040
 time light arises

85
00:04:01,040 --> 00:04:08,390
 or more beings come down and so he attributes himself to be

86
00:04:08,390 --> 00:04:09,600
 the cause and

87
00:04:09,600 --> 00:04:13,240
 to be the creator and this might seem like a fantastical

88
00:04:13,240 --> 00:04:14,080
 story and nothing to

89
00:04:14,080 --> 00:04:17,560
 do with reality but think about it this is how superstition

90
00:04:17,560 --> 00:04:18,560
 arises this is how

91
00:04:18,560 --> 00:04:23,470
 religion is created it's false attribution of a cause we

92
00:04:23,470 --> 00:04:24,400
 something we

93
00:04:24,400 --> 00:04:27,620
 want something to happen it happens and then we think what

94
00:04:27,620 --> 00:04:28,640
 was I doing when I

95
00:04:28,640 --> 00:04:32,200
 wanted it to happen maybe I had a horseshoe or maybe I had

96
00:04:32,200 --> 00:04:32,960
 a rabbit's foot

97
00:04:32,960 --> 00:04:35,740
 they're like well that means horseshoes are lucky horseshoe

98
00:04:35,740 --> 00:04:36,920
 rabbit if you're

99
00:04:36,920 --> 00:04:39,880
 rubbing a rabbit's foot it's lucky why because I've seen it

100
00:04:39,880 --> 00:04:40,680
 when I rub the

101
00:04:40,680 --> 00:04:43,440
 rabbits foot good things happen to me it's false

102
00:04:43,440 --> 00:04:45,040
 attribution and this is what

103
00:04:45,040 --> 00:04:48,530
 the Buddha you know he goes back and talks about how this

104
00:04:48,530 --> 00:04:49,720
 is you know this is

105
00:04:49,720 --> 00:04:52,740
 why the gods many gods think that that that they're the

106
00:04:52,740 --> 00:04:54,800
 creators human beings

107
00:04:54,800 --> 00:04:59,940
 get the same problem they they can go into meditative

108
00:04:59,940 --> 00:05:02,440
 states and suddenly

109
00:05:02,440 --> 00:05:05,280
 think they're a prophet because they can talk to angels and

110
00:05:05,280 --> 00:05:07,560
 talk to the gods who

111
00:05:07,560 --> 00:05:10,390
 then tell them you know I'm the creator I am the one I am

112
00:05:10,390 --> 00:05:11,520
 the Brahma and so on

113
00:05:11,520 --> 00:05:17,320
 and so on and they they can remember past lives many

114
00:05:17,320 --> 00:05:18,360
 prophets you know

115
00:05:18,360 --> 00:05:21,250
 remember being up in heaven and say I was sent by God I had

116
00:05:21,250 --> 00:05:22,080
 to come down my

117
00:05:22,080 --> 00:05:26,070
 mission wasn't finished and so on and so on so I would say

118
00:05:26,070 --> 00:05:27,720
 there's a lot of that

119
00:05:27,720 --> 00:05:30,530
 in the religious traditions and because these people tend

120
00:05:30,530 --> 00:05:31,560
 to be strong-minded

121
00:05:31,560 --> 00:05:35,700
 they make very charismatic leaders and they can be very

122
00:05:35,700 --> 00:05:37,800
 convincing because they

123
00:05:37,800 --> 00:05:42,130
 have some connection with the other worlds with angels and

124
00:05:42,130 --> 00:05:42,920
 with gods they

125
00:05:42,920 --> 00:05:46,170
 themselves believe it and you know these gods and angels

126
00:05:46,170 --> 00:05:47,320
 are diluted in and of

127
00:05:47,320 --> 00:05:50,420
 themselves you know thinking you know having the various

128
00:05:50,420 --> 00:05:52,440
 views of their own so

129
00:05:52,440 --> 00:05:57,320
 it all perpetuates itself this is a Buddhist explanation of

130
00:05:57,320 --> 00:05:58,840
 where God comes

131
00:05:58,840 --> 00:06:04,600
 from believe it or not a more secular explanation would be

132
00:06:04,600 --> 00:06:05,880
 in terms of false

133
00:06:05,880 --> 00:06:09,960
 attribution of cause when we you know the rain falls and we

134
00:06:09,960 --> 00:06:11,600
 attribute it to to

135
00:06:11,600 --> 00:06:14,540
 this or that action of ours and so on and then we start to

136
00:06:14,540 --> 00:06:15,400
 think well God

137
00:06:15,400 --> 00:06:17,550
 likes me and God doesn't like me and so on and this is all

138
00:06:17,550 --> 00:06:18,640
 part of it but I would

139
00:06:18,640 --> 00:06:21,400
 say from a Buddhist point of view why we believe in God in

140
00:06:21,400 --> 00:06:23,360
 general is because we

141
00:06:23,360 --> 00:06:26,270
 remember these things we remember these these other worlds

142
00:06:26,270 --> 00:06:29,000
 and we we we have some

143
00:06:29,000 --> 00:06:32,910
 kind of innate understanding of these levels we just don't

144
00:06:32,910 --> 00:06:33,220
 have an

145
00:06:33,220 --> 00:06:37,480
 understanding of the truth of them that even God is even

146
00:06:37,480 --> 00:06:40,000
 gods and angels are

147
00:06:40,000 --> 00:06:44,400
 impermanent being born and dying going up and down the

148
00:06:44,400 --> 00:06:45,280
 other part of the

149
00:06:45,280 --> 00:06:48,180
 question I think is much more interesting and that is the

150
00:06:48,180 --> 00:06:48,600
 idea of

151
00:06:48,600 --> 00:06:52,480
 prayer. Buddhists don't pray as far as I some

152
00:06:52,480 --> 00:06:56,520
 Buddhist do pray but it's not a very Buddhist thing to pray

153
00:06:56,520 --> 00:06:57,160
 what we do in

154
00:06:57,160 --> 00:07:01,820
 Buddhism and it's often mistaken for prayer is we we

155
00:07:01,820 --> 00:07:03,080
 resolve on certain

156
00:07:03,080 --> 00:07:07,920
 things we make a determination we make a wish if you will

157
00:07:07,920 --> 00:07:10,200
 and we do this to

158
00:07:10,200 --> 00:07:14,730
 establish our minds and to set our minds on on what we want

159
00:07:14,730 --> 00:07:16,240
 to occur we say to

160
00:07:16,240 --> 00:07:22,370
 ourselves may this happen may I be happy may I you know be

161
00:07:22,370 --> 00:07:24,360
 able to attain this or

162
00:07:24,360 --> 00:07:27,810
 this or that state and we can even do it with mundane

163
00:07:27,810 --> 00:07:30,760
 things may my parents you

164
00:07:30,760 --> 00:07:33,570
 know may their sickness disappear or this or that if

165
00:07:33,570 --> 00:07:35,120
 someone is sick you're

166
00:07:35,120 --> 00:07:39,720
 making something like a prayer but it the idea of a prayer

167
00:07:39,720 --> 00:07:42,000
 is it has power your

168
00:07:42,000 --> 00:07:46,300
 mind is fixed to focus the faith in your mind is is calming

169
00:07:46,300 --> 00:07:48,000
 and tranquilizing and

170
00:07:48,000 --> 00:07:54,420
 and and establishing the mind firmly so it's not the fact

171
00:07:54,420 --> 00:07:55,280
 that you're praying to

172
00:07:55,280 --> 00:07:57,920
 someone it's the fact that you're making a determination

173
00:07:57,920 --> 00:07:59,600
 you're you're resolving

174
00:07:59,600 --> 00:08:02,570
 in your mind for this to happen it's you're making a wish

175
00:08:02,570 --> 00:08:03,520
 and that wish is

176
00:08:03,520 --> 00:08:06,100
 going to form at least a part of your reality and it's

177
00:08:06,100 --> 00:08:07,280
 going to have an effect

178
00:08:07,280 --> 00:08:10,720
 and it can often help if your mind is strong enough it can

179
00:08:10,720 --> 00:08:11,920
 help to change the

180
00:08:11,920 --> 00:08:16,290
 world around you this is a very important part of Buddhist

181
00:08:16,290 --> 00:08:17,320
 practice it's something

182
00:08:17,320 --> 00:08:20,950
 that we use in our meditation you know I have this problem

183
00:08:20,950 --> 00:08:22,160
 may I become free from

184
00:08:22,160 --> 00:08:25,680
 this problem may I attain this or that if you want to

185
00:08:25,680 --> 00:08:27,960
 become a monk may I may I

186
00:08:27,960 --> 00:08:31,300
 have the opportunity to become a monk me if you want to go

187
00:08:31,300 --> 00:08:32,520
 to do a meditation

188
00:08:32,520 --> 00:08:36,850
 course you know may I may all the obstacles in my way

189
00:08:36,850 --> 00:08:39,000
 disappear and may I

190
00:08:39,000 --> 00:08:43,740
 be successful in this or that endeavor this is totally

191
00:08:43,740 --> 00:08:45,800
 valid and it's nothing

192
00:08:45,800 --> 00:08:49,370
 to do with another being coming down and giving you

193
00:08:49,370 --> 00:08:51,400
 blessing you with your the

194
00:08:51,400 --> 00:08:54,880
 results of your wish or the the answer to your prayers it

195
00:08:54,880 --> 00:08:56,040
 has to do with your

196
00:08:56,040 --> 00:08:59,520
 changing of reality and in a very mundane sense it's it's

197
00:08:59,520 --> 00:09:00,920
 simply your

198
00:09:00,920 --> 00:09:06,220
 intention in in achieving that goal which focuses your mind

199
00:09:06,220 --> 00:09:07,480
 and puts you on

200
00:09:07,480 --> 00:09:12,590
 that path and and propels you along it so that's where

201
00:09:12,590 --> 00:09:15,800
 where prayer might come

202
00:09:15,800 --> 00:09:19,920
 close to Buddhism again it's not prayer but when you see

203
00:09:19,920 --> 00:09:21,120
 people praying for this

204
00:09:21,120 --> 00:09:24,260
 wishing for this or wishing for that you can understand

205
00:09:24,260 --> 00:09:25,440
 that if they've got their

206
00:09:25,440 --> 00:09:27,520
 head on straight if they're if they're really following the

207
00:09:27,520 --> 00:09:28,280
 Buddha's teaching

208
00:09:28,280 --> 00:09:31,180
 then they're not they're not expecting the Buddha to give

209
00:09:31,180 --> 00:09:32,120
 them the wish and

210
00:09:32,120 --> 00:09:36,400
 they're not expecting anything from God they they are just

211
00:09:36,400 --> 00:09:37,840
 you know resolving

212
00:09:37,840 --> 00:09:41,880
 for that and in some way it can affect the universe as well

213
00:09:41,880 --> 00:09:43,040
 but mostly it's

214
00:09:43,040 --> 00:09:46,820
 just going to set it set their minds on it and will help

215
00:09:46,820 --> 00:09:47,600
 them to achieve their

216
00:09:47,600 --> 00:09:52,390
 goals a final note about the Buddha why we don't expect the

217
00:09:52,390 --> 00:09:53,480
 Buddha to help us is

218
00:09:53,480 --> 00:09:56,540
 because the Buddha's passed away and by passed away we mean

219
00:09:56,540 --> 00:09:57,680
 he hasn't been born

220
00:09:57,680 --> 00:10:01,280
 he wasn't born again whereas when we pass away we don't

221
00:10:01,280 --> 00:10:02,240
 really pass away we

222
00:10:02,240 --> 00:10:08,010
 just continue on but for someone who has attained nirvana

223
00:10:08,010 --> 00:10:09,640
 when they pass away they

224
00:10:09,640 --> 00:10:14,100
 they pass away and so the Buddha you could say he's still

225
00:10:14,100 --> 00:10:15,440
 in Kusinara in

226
00:10:15,440 --> 00:10:19,410
 India that was where he passed away and by passed away the

227
00:10:19,410 --> 00:10:20,480
 the technical

228
00:10:20,480 --> 00:10:25,720
 meaning is he went inside the the mind stopped coming out

229
00:10:25,720 --> 00:10:27,020
 the mind ceased to

230
00:10:27,020 --> 00:10:31,800
 arise cease to see here smell taste feel or think so in a

231
00:10:31,800 --> 00:10:32,680
 sense you could say the

232
00:10:32,680 --> 00:10:35,800
 Buddha's you know there in Kusinara but it's not coming out

233
00:10:35,800 --> 00:10:36,560
 anymore it's the

234
00:10:36,560 --> 00:10:39,960
 mind is not arising there's no more opportunity for it to

235
00:10:39,960 --> 00:10:41,360
 arise so we see

236
00:10:41,360 --> 00:10:43,650
 really passed away and there's no chance for the Buddha to

237
00:10:43,650 --> 00:10:44,960
 hear your prayers or

238
00:10:44,960 --> 00:10:52,190
 answer them at least not the Buddha who's who's not not the

239
00:10:52,190 --> 00:10:53,240
 Buddha there are

240
00:10:53,240 --> 00:10:57,360
 many beings in Buddhism that are said to answer prayers but

241
00:10:57,360 --> 00:10:59,440
 that's a whole other

242
00:10:59,440 --> 00:11:04,060
 other can of worms and I guess that does it has to be

243
00:11:04,060 --> 00:11:06,520
 admitted that yes you can

244
00:11:06,520 --> 00:11:10,500
 wish for things and ask for the angels and gods to help you

245
00:11:10,500 --> 00:11:12,440
 the problem is you

246
00:11:12,440 --> 00:11:14,370
 don't know whether they're going to help you or not and it

247
00:11:14,370 --> 00:11:15,120
's completely up to

248
00:11:15,120 --> 00:11:20,850
 them there is no being out there who is purely

249
00:11:20,850 --> 00:11:24,600
 compassionate and wise and is

250
00:11:24,600 --> 00:11:28,970
 going to answer all of your prayers there's a lot of beings

251
00:11:28,970 --> 00:11:30,080
 out there who

252
00:11:30,080 --> 00:11:36,030
 are going to going to pose in that role and instead wind

253
00:11:36,030 --> 00:11:38,040
 you around their finger

254
00:11:38,040 --> 00:11:42,860
 and you know twist your will to to theirs and you'll find

255
00:11:42,860 --> 00:11:44,440
 yourself following them

256
00:11:44,440 --> 00:11:47,440
 and that's I think the case with many meditators when they

257
00:11:47,440 --> 00:11:48,480
 get caught up with

258
00:11:48,480 --> 00:11:52,940
 angels and other beings they find that rather than rather

259
00:11:52,940 --> 00:11:54,040
 than the angels and

260
00:11:54,040 --> 00:11:59,100
 gods serving them they wind up serving these beings and and

261
00:11:59,100 --> 00:11:59,800
 and following after

262
00:11:59,800 --> 00:12:02,680
 all of their defilements and their illusions and delusions

263
00:12:02,680 --> 00:12:03,800
 as well okay so

264
00:12:03,800 --> 00:12:06,370
 I hope that gives an answer it's a very interesting

265
00:12:06,370 --> 00:12:08,000
 question and I'd like to

266
00:12:08,000 --> 00:12:10,640
 encourage everyone to make these determinations especially

267
00:12:10,640 --> 00:12:11,080
 after you

268
00:12:11,080 --> 00:12:14,170
 practice and especially in regards to your own happiness

269
00:12:14,170 --> 00:12:15,440
 and the happiness of

270
00:12:15,440 --> 00:12:20,560
 your fellow beings may all beings be happy

