1
00:00:00,000 --> 00:00:04,650
 Good evening and welcome back to our study of the Dhammap

2
00:00:04,650 --> 00:00:05,440
ada.

3
00:00:05,440 --> 00:00:10,120
 Today we continue on with verse 147, which reads as follows

4
00:00:10,120 --> 00:00:11,480
.

5
00:00:11,480 --> 00:00:18,410
 "Pasa jita katang bimbang, arukayang samusitang, patturang

6
00:00:18,410 --> 00:00:26,360
 bahusang kapang, yasanati duangditi."

7
00:00:26,360 --> 00:00:34,000
 Which means, "Pasa, come and look, come and see jita katang

8
00:00:34,000 --> 00:00:39,080
 bimbang, this painted body,

9
00:00:39,080 --> 00:00:50,160
 painted image, this painted image, the body.

10
00:00:50,160 --> 00:00:58,600
 Arukayang, that is a mass of sores, mass of festering sores

11
00:00:58,600 --> 00:01:02,560
 or wounds or sufferings.

12
00:01:02,560 --> 00:01:08,430
 Samusitang, samusitang I think should actually mean bloated

13
00:01:08,430 --> 00:01:10,760
, but compounded is the word,

14
00:01:10,760 --> 00:01:16,560
 the common term, maybe compounded is better.

15
00:01:16,560 --> 00:01:29,080
 "Patturang, diseased, bahusang kapang, bahusang kapang."

16
00:01:29,080 --> 00:01:35,520
 Which means many thoughts with many intentions or purposes.

17
00:01:35,520 --> 00:01:41,760
 It can be used for many purposes as maybe what the idea is

18
00:01:41,760 --> 00:01:42,760
 here.

19
00:01:42,760 --> 00:01:52,320
 "Yasanati duangditi," and of which there is nothing that

20
00:01:52,320 --> 00:01:54,960
 stays permanent, nothing that

21
00:01:54,960 --> 00:01:59,480
 stays stable.

22
00:01:59,480 --> 00:02:12,920
 A rather harsh announcement of the body.

23
00:02:12,920 --> 00:02:20,760
 It's the story told of Sirima, the death of Sirima.

24
00:02:20,760 --> 00:02:25,560
 Sirima has quite a colorful story.

25
00:02:25,560 --> 00:02:33,440
 She was a courtesan, which I guess means a prostitute or

26
00:02:33,440 --> 00:02:37,560
 maybe similar to a geisha.

27
00:02:37,560 --> 00:02:42,060
 People would pay money to spend the night with her or they

28
00:02:42,060 --> 00:02:44,040
'd hire her for whatever

29
00:02:44,040 --> 00:02:45,040
 purpose.

30
00:02:45,040 --> 00:02:48,480
 I think that's where the bahusang kapang comes from.

31
00:02:48,480 --> 00:03:00,270
 A beautiful body can be used for many purposes, a healthy

32
00:03:00,270 --> 00:03:02,080
 body.

33
00:03:02,080 --> 00:03:05,490
 She was bought, the backstory to this is she was bought by

34
00:03:05,490 --> 00:03:06,200
 Uttara.

35
00:03:06,200 --> 00:03:07,880
 Uttara was a sotapan, I think.

36
00:03:07,880 --> 00:03:11,690
 She was a disciple of the Buddha and she really wanted to

37
00:03:11,690 --> 00:03:13,840
 spend all of her time taking care

38
00:03:13,840 --> 00:03:17,550
 of the Buddha and the monks and listening to the Buddha's

39
00:03:17,550 --> 00:03:18,480
 teaching.

40
00:03:18,480 --> 00:03:27,400
 The problem was her husband was taking a lot of her time,

41
00:03:27,400 --> 00:03:32,400
 requiring her to please him.

42
00:03:32,400 --> 00:03:35,500
 Because she was interested in the dhamma, she wasn't really

43
00:03:35,500 --> 00:03:36,840
 interested in sleeping

44
00:03:36,840 --> 00:03:41,760
 with her or engaging in sensuality with her husband, she

45
00:03:41,760 --> 00:03:44,240
 hired this courtesan for her

46
00:03:44,240 --> 00:03:48,400
 husband, beautiful Sirima.

47
00:03:48,400 --> 00:03:55,920
 She was so beautiful that her husband didn't even complain.

48
00:03:55,920 --> 00:04:00,250
 She would go about Uttara, the other one, the wife, would

49
00:04:00,250 --> 00:04:02,240
 go about making all these

50
00:04:02,240 --> 00:04:04,960
 preparations for the Buddha and spending all of her time

51
00:04:04,960 --> 00:04:06,480
 making food for the monks and

52
00:04:06,480 --> 00:04:09,000
 so on.

53
00:04:09,000 --> 00:04:15,610
 Her husband spent all of his time with this prostitute,

54
00:04:15,610 --> 00:04:17,960
 this courtesan.

55
00:04:17,960 --> 00:04:24,560
 Eventually the courtesan came to feel like she had some

56
00:04:24,560 --> 00:04:27,960
 sort of ego in regards to her

57
00:04:27,960 --> 00:04:34,760
 situation like the husband preferred her to his wife.

58
00:04:34,760 --> 00:04:37,820
 She started to cultivate this sense of jealousy towards the

59
00:04:37,820 --> 00:04:39,280
 wife thinking, "I'm the one

60
00:04:39,280 --> 00:04:44,320
 who takes care of him," and so on.

61
00:04:44,320 --> 00:04:48,520
 One day the husband was watching Uttara, I think her name

62
00:04:48,520 --> 00:04:50,600
 was Uttara, watching her

63
00:04:50,600 --> 00:04:58,160
 prepare all this food for the monks.

64
00:04:58,160 --> 00:04:59,160
 What a ridiculous woman.

65
00:04:59,160 --> 00:05:02,400
 She spends all her time when she could be enjoying life.

66
00:05:02,400 --> 00:05:09,440
 She works really hard to give food to these beggars.

67
00:05:09,440 --> 00:05:13,080
 So he smiled, just smiled at how silly it was.

68
00:05:13,080 --> 00:05:16,910
 And Sirima saw him and thought he was smiling at his wife

69
00:05:16,910 --> 00:05:18,920
 and said, "I do all this for

70
00:05:18,920 --> 00:05:21,840
 him and still he's attracted to her.

71
00:05:21,840 --> 00:05:24,480
 Well, I'll take her out of the picture."

72
00:05:24,480 --> 00:05:29,070
 She went over into the kitchen where Uttara was boiling or

73
00:05:29,070 --> 00:05:31,440
 heating up some butter on the

74
00:05:31,440 --> 00:05:35,750
 stove and she picked up this cauldron of hot butter and

75
00:05:35,750 --> 00:05:38,960
 threw it at Sirima and poured this

76
00:05:38,960 --> 00:05:41,920
 scalding hot butter onto Uttara.

77
00:05:41,920 --> 00:05:45,500
 Uttara was of course the practitioner of the Buddha's

78
00:05:45,500 --> 00:05:47,640
 teaching and she saw, she turned

79
00:05:47,640 --> 00:05:51,940
 and saw this butter coming at her and immediately she

80
00:05:51,940 --> 00:05:55,400
 entered into a jhana of loving kindness.

81
00:05:55,400 --> 00:06:00,600
 She immediately recognized this violence against her and

82
00:06:00,600 --> 00:06:03,960
 thought to herself, "Wished her happiness

83
00:06:03,960 --> 00:06:08,240
 and well-being for Sirima."

84
00:06:08,240 --> 00:06:11,770
 And as a result of the incredible power of her mind, they

85
00:06:11,770 --> 00:06:14,000
 say the butter became completely

86
00:06:14,000 --> 00:06:22,100
 cooled and it was as though a cool water was washing over

87
00:06:22,100 --> 00:06:26,600
 her, a cool butter I guess.

88
00:06:26,600 --> 00:06:29,870
 So she wasn't hurt at all and the servants who saw this

89
00:06:29,870 --> 00:06:32,160
 happening, who were helping in

90
00:06:32,160 --> 00:06:36,040
 the kitchen immediately jumped on Sirima and started

91
00:06:36,040 --> 00:06:37,200
 beating her.

92
00:06:37,200 --> 00:06:40,290
 But Uttara pulled them off and said to Sirima and picked

93
00:06:40,290 --> 00:06:42,240
 Sirima off the floor and started

94
00:06:42,240 --> 00:06:46,560
 tending to her wounds and asking her if she was okay.

95
00:06:46,560 --> 00:06:51,890
 And Sirima became completely converted and ashamed of what

96
00:06:51,890 --> 00:06:58,200
 she had done and asked forgiveness.

97
00:06:58,200 --> 00:07:00,910
 Eventually I think Uttara sent her off to ask forgiveness

98
00:07:00,910 --> 00:07:01,760
 of the Buddha.

99
00:07:01,760 --> 00:07:03,200
 Eventually she became Buddhist.

100
00:07:03,200 --> 00:07:05,220
 She became a sotapana.

101
00:07:05,220 --> 00:07:09,160
 The story isn't actually about her, it's actually about a

102
00:07:09,160 --> 00:07:09,800
 monk.

103
00:07:09,800 --> 00:07:12,630
 So she became Buddhist and spent a lot of her time

104
00:07:12,630 --> 00:07:15,400
 listening to the Buddhist teachings,

105
00:07:15,400 --> 00:07:20,900
 practicing meditation, became a sotapana and would often

106
00:07:20,900 --> 00:07:23,720
 provide food for the monks and

107
00:07:23,720 --> 00:07:28,880
 listen to their teachings as well.

108
00:07:28,880 --> 00:07:35,030
 So the word got out and this one monk heard about Sirima

109
00:07:35,030 --> 00:07:38,920
 and he went and asked these monks,

110
00:07:38,920 --> 00:07:41,840
 "What's the deal with this Sirima?"

111
00:07:41,840 --> 00:07:45,160
 He had heard how incredible she was.

112
00:07:45,160 --> 00:07:47,770
 And this monk said, "Oh wow, the food she gives is

113
00:07:47,770 --> 00:07:48,600
 wonderful.

114
00:07:48,600 --> 00:07:51,350
 She's very good at it and she gives the best and the most

115
00:07:51,350 --> 00:07:52,960
 highest quality of food."

116
00:07:52,960 --> 00:07:55,890
 But apart from that she's also quite beautiful and pretty

117
00:07:55,890 --> 00:07:56,640
 to look at.

118
00:07:56,640 --> 00:08:04,710
 It's funny to hear monks having this conversation but that

119
00:08:04,710 --> 00:08:08,080
's what the text says.

120
00:08:08,080 --> 00:08:16,430
 And immediately he became attracted and he thought he

121
00:08:16,430 --> 00:08:20,200
 should go and see her.

122
00:08:20,200 --> 00:08:24,680
 And so he found his way to get into the queue to go and

123
00:08:24,680 --> 00:08:27,760
 receive alms from this Sirima in

124
00:08:27,760 --> 00:08:30,640
 her great alms house.

125
00:08:30,640 --> 00:08:34,880
 But the night before she got very sick, or the day before

126
00:08:34,880 --> 00:08:37,080
 she got quite sick and so she

127
00:08:37,080 --> 00:08:39,520
 took off all of her makeup and jewels and she lay down and

128
00:08:39,520 --> 00:08:40,880
 she had the servants prepare

129
00:08:40,880 --> 00:08:43,360
 the food.

130
00:08:43,360 --> 00:08:45,790
 And then as the monks were sitting to receive the food she

131
00:08:45,790 --> 00:08:47,240
 came out, they carried her out

132
00:08:47,240 --> 00:08:51,840
 on a palanquin or whatever it is, those things that you

133
00:08:51,840 --> 00:08:54,480
 carry, a stretcher I guess.

134
00:08:54,480 --> 00:08:59,880
 And she was able to, lying down, she was able to give food.

135
00:08:59,880 --> 00:09:03,420
 And this monk saw her and he thought to himself, "She's

136
00:09:03,420 --> 00:09:06,240
 this beautiful when she's sick.

137
00:09:06,240 --> 00:09:09,060
 Imagine how beautiful she would be when she's perfectly

138
00:09:09,060 --> 00:09:09,760
 healthy."

139
00:09:09,760 --> 00:09:17,900
 And he became completely enamored and the text says this

140
00:09:17,900 --> 00:09:20,200
 lust that he had accumulated

141
00:09:20,200 --> 00:09:23,360
 during many millions of years arose within him.

142
00:09:23,360 --> 00:09:28,060
 He became indifferent, was unable to take food and he went

143
00:09:28,060 --> 00:09:30,040
 back to his kuti and shut

144
00:09:30,040 --> 00:09:34,050
 the door and didn't listen to anyone, didn't go out and

145
00:09:34,050 --> 00:09:36,200
 just laid sick on his bed.

146
00:09:36,200 --> 00:09:46,800
 And sick with lust, sick with attraction to this woman.

147
00:09:46,800 --> 00:09:51,720
 Now as nature goes, as things go, nature took its course

148
00:09:51,720 --> 00:09:54,480
 and Sirima passed away that day

149
00:09:54,480 --> 00:09:59,480
 I believe, unbeknownst to this monk.

150
00:09:59,480 --> 00:10:02,910
 And because she was such a great lay disciple, she was

151
00:10:02,910 --> 00:10:05,400
 actually the sister of Jibakkha, it

152
00:10:05,400 --> 00:10:10,760
 looks like the Buddha's physician.

153
00:10:10,760 --> 00:10:13,960
 And so the king sent word to the Buddha that Sirima had

154
00:10:13,960 --> 00:10:16,120
 died and the Buddha sent word back

155
00:10:16,120 --> 00:10:21,740
 that Sirima said, "Please tell the king not to bury Sirima,

156
00:10:21,740 --> 00:10:24,160
 not to, nor cremate I guess,

157
00:10:24,160 --> 00:10:25,160
 they wouldn't bury, right?"

158
00:10:25,160 --> 00:10:29,000
 Burned, yes, right, cremate her.

159
00:10:29,000 --> 00:10:32,470
 Instead put Sirima's body out in the charnel ground and set

160
00:10:32,470 --> 00:10:34,160
 up a guard so that the crows

161
00:10:34,160 --> 00:10:40,020
 in the jackals so that the animals don't eat her and just

162
00:10:40,020 --> 00:10:42,720
 leave the body there.

163
00:10:42,720 --> 00:10:47,980
 And so the king had this done and he had a guard set up and

164
00:10:47,980 --> 00:10:50,600
 after two or three days,

165
00:10:50,600 --> 00:10:53,700
 after four days, three days passed and on the fourth day

166
00:10:53,700 --> 00:10:55,240
 the body got all bloated of

167
00:10:55,240 --> 00:10:59,830
 course and from the nine openings of her body there oozed

168
00:10:59,830 --> 00:11:02,520
 forth maggots so they couldn't

169
00:11:02,520 --> 00:11:05,740
 keep the flies away and the maggots began to pour out, pour

170
00:11:05,740 --> 00:11:07,160
 out of the nine openings

171
00:11:07,160 --> 00:11:14,560
 of the body.

172
00:11:14,560 --> 00:11:16,160
 And then the king caused a proclamation.

173
00:11:16,160 --> 00:11:17,920
 The king understood what the Buddha was doing here.

174
00:11:17,920 --> 00:11:20,950
 He didn't know about this special case of this monk but he

175
00:11:20,950 --> 00:11:22,480
 understood what the Buddha

176
00:11:22,480 --> 00:11:26,820
 was doing in general and so he called for everyone to come

177
00:11:26,820 --> 00:11:28,280
 and see Sirima.

178
00:11:28,280 --> 00:11:32,470
 He said everyone in the city besides those who are busy

179
00:11:32,470 --> 00:11:37,080
 working on some official business,

180
00:11:37,080 --> 00:11:40,040
 he called for an order that everyone should come and see.

181
00:11:40,040 --> 00:11:44,480
 If they don't, except for the watchmen, the police,

182
00:11:44,480 --> 00:11:46,120
 everybody should go.

183
00:11:46,120 --> 00:11:49,400
 If not, they would be fined eight pieces of gold.

184
00:11:49,400 --> 00:11:53,540
 And then he sent the message to the Buddha that the monks

185
00:11:53,540 --> 00:11:55,560
 should come and see Sirima

186
00:11:55,560 --> 00:11:59,320
 as well.

187
00:11:59,320 --> 00:12:02,690
 And that's how the proclamation went out and it came to the

188
00:12:02,690 --> 00:12:04,400
 monks and the monks started

189
00:12:04,400 --> 00:12:07,500
 talking about how they were going to see Sirima and this

190
00:12:07,500 --> 00:12:09,640
 monk heard and immediately thought

191
00:12:09,640 --> 00:12:11,480
 to himself, "I'll get to see Sirima.

192
00:12:11,480 --> 00:12:14,200
 I'll get to see this beautiful woman again."

193
00:12:14,200 --> 00:12:19,000
 And he just had to go and see her.

194
00:12:19,000 --> 00:12:22,950
 Or they even came and asked him, "Will you go to see Sirima

195
00:12:22,950 --> 00:12:23,360
?"

196
00:12:23,360 --> 00:12:26,280
 He said, "Of course I'll go."

197
00:12:26,280 --> 00:12:34,680
 And they set out with the monks.

198
00:12:34,680 --> 00:12:37,560
 Of course when they got there, the situation was not as he

199
00:12:37,560 --> 00:12:38,240
 expected.

200
00:12:38,240 --> 00:12:43,660
 And the king and the Buddha came up to Sirima and saw her

201
00:12:43,660 --> 00:12:47,120
 lying there and he said, "King,

202
00:12:47,120 --> 00:12:48,120
 who is this woman?"

203
00:12:48,120 --> 00:12:51,800
 And he said, "This is Sirima, Jibaka's sister.

204
00:12:51,800 --> 00:12:52,800
 Is this Sirima?"

205
00:12:52,800 --> 00:12:58,170
 And he said, "Well then, send a drum, a guy with a drum to

206
00:12:58,170 --> 00:12:59,400
..."

207
00:12:59,400 --> 00:13:01,120
 This is how they had those proclamations.

208
00:13:01,120 --> 00:13:05,840
 This is how they spread word in those times.

209
00:13:05,840 --> 00:13:12,070
 "And make a proclamation that we will give Sirima to anyone

210
00:13:12,070 --> 00:13:14,720
 for a thousand pieces of

211
00:13:14,720 --> 00:13:17,240
 gold for one night.

212
00:13:17,240 --> 00:13:22,190
 Whoever wants her can have her for a thousand pieces of

213
00:13:22,190 --> 00:13:23,080
 gold."

214
00:13:23,080 --> 00:13:24,960
 Not a man said, "Hem" or "Hem," that's the English

215
00:13:24,960 --> 00:13:25,720
 translation.

216
00:13:25,720 --> 00:13:28,880
 I don't know what the Pali is.

217
00:13:28,880 --> 00:13:29,880
 Nobody said anything.

218
00:13:29,880 --> 00:13:33,750
 Nobody even cleared their throat for fear of being

219
00:13:33,750 --> 00:13:37,360
 considered interested in taking up

220
00:13:37,360 --> 00:13:38,360
 the offer.

221
00:13:38,360 --> 00:13:43,760
 And he said, "Well then, ask for five hundred."

222
00:13:43,760 --> 00:13:47,600
 And then five hundred, nobody said anything.

223
00:13:47,600 --> 00:13:50,990
 Two hundred and fifty, two hundred, one hundred, fifty,

224
00:13:50,990 --> 00:13:52,720
 twenty-five, ten, five.

225
00:13:52,720 --> 00:13:54,970
 And they reduced it to a penny, a half penny, a quarter of

226
00:13:54,970 --> 00:13:56,480
 a penny, an eighth of a penny.

227
00:13:56,480 --> 00:14:01,180
 And finally the Buddha said, "Tell her they can have her

228
00:14:01,180 --> 00:14:02,640
 for nothing."

229
00:14:02,640 --> 00:14:05,880
 And no one said anything.

230
00:14:05,880 --> 00:14:10,320
 King said, "Bhante, no one will take her even as a gift."

231
00:14:10,320 --> 00:14:14,480
 And the teacher said, "Monks, do you see the value of a

232
00:14:14,480 --> 00:14:17,320
 woman in the eyes of the multitude,

233
00:14:17,320 --> 00:14:21,730
 the value of the female body which is so well, so sexual

234
00:14:21,730 --> 00:14:24,920
ized, right, and so highly esteemed

235
00:14:24,920 --> 00:14:30,000
 for its sexual attraction?

236
00:14:30,000 --> 00:14:32,520
 Such was her beauty who is now perished and gone."

237
00:14:32,520 --> 00:14:48,930
 Ewarupang namarupang kaya vaya patang, subject to fading

238
00:14:48,930 --> 00:14:52,520
 away, subject to loss.

239
00:14:52,520 --> 00:14:57,680
 Pasattabhik away aturangatabhavam.

240
00:14:57,680 --> 00:15:03,800
 And see the corruption, the disease, the affliction of the

241
00:15:03,800 --> 00:15:12,040
 being, of the self, of the body.

242
00:15:12,040 --> 00:15:17,440
 And then he told this verse, passajj takatang bhimang and

243
00:15:17,440 --> 00:15:18,320
 so on.

244
00:15:18,320 --> 00:15:18,320
 So for us, obviously we're not practicing Maranandu Sati or

245
00:15:18,320 --> 00:15:28,080
 Kaya Gattasati or the cemetery

246
00:15:28,080 --> 00:15:30,080
 contemplations.

247
00:15:30,080 --> 00:15:36,960
 But it's still a good reminder to us, the idea of death.

248
00:15:36,960 --> 00:15:43,560
 It's a good example of the misunderstanding that comes.

249
00:15:43,560 --> 00:15:46,200
 This monk, I mean there's many things in here.

250
00:15:46,200 --> 00:15:49,780
 There's the monk's lust and his desire which interestingly

251
00:15:49,780 --> 00:15:51,600
 he mentions which is a fairly

252
00:15:51,600 --> 00:15:56,200
 rare sort of thing to say that it's something that he'd had

253
00:15:56,200 --> 00:15:58,600
 for countless lifetimes.

254
00:15:58,600 --> 00:16:01,610
 But this is the truth, you know, a lot of our attachments,

255
00:16:01,610 --> 00:16:03,280
 a lot of our habits are habitual

256
00:16:03,280 --> 00:16:10,680
 not in this life but over lifetimes so they can be quite

257
00:16:10,680 --> 00:16:12,240
 strong.

258
00:16:12,240 --> 00:16:16,090
 And so if we're often wondering where did this come from,

259
00:16:16,090 --> 00:16:18,360
 why am I so attached or attracted

260
00:16:18,360 --> 00:16:20,360
 to this or attracted to that?

261
00:16:20,360 --> 00:16:27,920
 In fact it's just a matter of cultivation and we've

262
00:16:27,920 --> 00:16:32,280
 cultivated it for so long.

263
00:16:32,280 --> 00:16:35,400
 But the big thing here is this misunderstanding, this

264
00:16:35,400 --> 00:16:37,840
 misconception about things, about why

265
00:16:37,840 --> 00:16:41,270
 should the healthy body be any more attractive than the

266
00:16:41,270 --> 00:16:42,360
 bloated body?

267
00:16:42,360 --> 00:16:44,550
 It's only when we see the body bloated, there's nothing

268
00:16:44,550 --> 00:16:46,080
 actually different from the bloated

269
00:16:46,080 --> 00:16:48,720
 body with maggots coming out of it, right?

270
00:16:48,720 --> 00:16:49,720
 Why isn't that beautiful?

271
00:16:49,720 --> 00:16:52,650
 Why aren't we attracted to the bloated body with maggots

272
00:16:52,650 --> 00:16:54,160
 coming out of nine holes?

273
00:16:54,160 --> 00:16:59,480
 Why aren't we attracted to the smell of a bloated body?

274
00:16:59,480 --> 00:17:02,910
 I remember once in Thailand there was, I was sitting in my

275
00:17:02,910 --> 00:17:05,000
 kutti and there was this terrible

276
00:17:05,000 --> 00:17:10,720
 smell, just a smell like nothing you'd ever smelled before.

277
00:17:10,720 --> 00:17:13,980
 And then I was meditating through it and first looked

278
00:17:13,980 --> 00:17:16,280
 around the room, tried to find what

279
00:17:16,280 --> 00:17:20,460
 died and then suddenly maggots started falling through the

280
00:17:20,460 --> 00:17:22,280
 cracks in the ceiling.

281
00:17:22,280 --> 00:17:27,730
 I guess some animal, maybe a big rat or something, I don't

282
00:17:27,730 --> 00:17:30,640
 know, died in the ceiling.

283
00:17:30,640 --> 00:17:33,470
 And the maggots, all I know of it was that maggots started

284
00:17:33,470 --> 00:17:34,960
 falling through the ceiling.

285
00:17:34,960 --> 00:17:39,250
 If you've never been around a dead body it doesn't have the

286
00:17:39,250 --> 00:17:42,960
 most attractive smell, but

287
00:17:42,960 --> 00:17:44,880
 there's simply our conditioning.

288
00:17:44,880 --> 00:17:48,310
 I mean you can argue that there's something, the body is

289
00:17:48,310 --> 00:17:50,400
 conditioned to react violently

290
00:17:50,400 --> 00:17:54,240
 to certain smells, but certainly not certain sights.

291
00:17:54,240 --> 00:17:58,240
 Well, maybe in the brain somehow, but we have senses of

292
00:17:58,240 --> 00:18:00,440
 symmetry and so on and when the

293
00:18:00,440 --> 00:18:03,450
 body is bloated it loses that, but that's very much

294
00:18:03,450 --> 00:18:04,560
 conditioning.

295
00:18:04,560 --> 00:18:09,100
 Whether it's physical or mental, it all comes from the mind

296
00:18:09,100 --> 00:18:13,560
 and our build up of this life.

297
00:18:13,560 --> 00:18:18,350
 And so it takes this shock, it took this shock for this

298
00:18:18,350 --> 00:18:21,520
 monk to snap him out of that, which

299
00:18:21,520 --> 00:18:25,000
 is why watching the body, even healthy, watching the body,

300
00:18:25,000 --> 00:18:27,000
 watching when you walk, watching

301
00:18:27,000 --> 00:18:30,410
 the stomach, you start to see, you start to lose this

302
00:18:30,410 --> 00:18:32,760
 attraction, thinking originally

303
00:18:32,760 --> 00:18:35,320
 that you were so handsome or so beautiful, and as you watch

304
00:18:35,320 --> 00:18:36,440
 it to start to see that the

305
00:18:36,440 --> 00:18:43,320
 body is sagging and it's drooping and it's sweating and it

306
00:18:43,320 --> 00:18:47,000
's bleeding and it's infected

307
00:18:47,000 --> 00:18:53,480
 and it's pouring out, defiled, it's pouring out, garbage

308
00:18:53,480 --> 00:18:55,240
 constantly.

309
00:18:55,240 --> 00:18:59,590
 I mean of all things that we can be attached to, there are

310
00:18:59,590 --> 00:19:02,240
 no diamonds and jewels inside,

311
00:19:02,240 --> 00:19:06,740
 there's no sugar and spice, there's nothing inside that's

312
00:19:06,740 --> 00:19:09,480
 attractive or should be attractive

313
00:19:09,480 --> 00:19:15,120
 in any way.

314
00:19:15,120 --> 00:19:17,440
 The greatest point for us as meditators is to learn to

315
00:19:17,440 --> 00:19:19,280
 become objective, not to be repulsed

316
00:19:19,280 --> 00:19:23,420
 by the body, but through seeing how repulsed we get, it's

317
00:19:23,420 --> 00:19:25,320
 kind of hypocritical.

318
00:19:25,320 --> 00:19:30,570
 There's no reason for us to be attracted to the body in one

319
00:19:30,570 --> 00:19:31,440
 form.

320
00:19:31,440 --> 00:19:34,010
 And this is a good example of how our misconceptions arise,

321
00:19:34,010 --> 00:19:35,560
 not just about the body, we do the

322
00:19:35,560 --> 00:19:40,400
 same with feelings and we have happy feelings, why are we

323
00:19:40,400 --> 00:19:43,440
 attracted or attached to those?

324
00:19:43,440 --> 00:19:51,970
 Why do painful feelings create such a violent aversion in

325
00:19:51,970 --> 00:19:52,920
 us?

326
00:19:52,920 --> 00:19:55,830
 It's a bold claim, it's not that we should try to deny our

327
00:19:55,830 --> 00:19:57,520
 attractions or attachments,

328
00:19:57,520 --> 00:20:01,250
 we shouldn't deny the lust and the desire, we should just

329
00:20:01,250 --> 00:20:03,040
 come to see how wrong it is,

330
00:20:03,040 --> 00:20:05,800
 how silly it is.

331
00:20:05,800 --> 00:20:10,130
 The bold claim means you don't have to pre-judge, you don't

332
00:20:10,130 --> 00:20:12,760
 have to have any preconceptions,

333
00:20:12,760 --> 00:20:16,680
 you just have to look and see.

334
00:20:16,680 --> 00:20:21,240
 As you look at the body watching your own body, you start

335
00:20:21,240 --> 00:20:23,600
 to see that there's nothing

336
00:20:23,600 --> 00:20:27,480
 attractive or repulsive inherently about anything.

337
00:20:27,480 --> 00:20:30,950
 It's us who become attracted or repulsed through

338
00:20:30,950 --> 00:20:38,520
 conditioning, often through memories of pleasure

339
00:20:38,520 --> 00:20:39,520
 or pain.

340
00:20:39,520 --> 00:20:43,280
 So for example, if you eat some sort of food and it's

341
00:20:43,280 --> 00:20:46,160
 poisoned and you get food poisoning,

342
00:20:46,160 --> 00:20:51,130
 often eating that food or even looking at it will cause you

343
00:20:51,130 --> 00:20:53,320
 great nausea or aversion.

344
00:20:53,320 --> 00:21:00,970
 I remember one time getting very, very, very drunk on peach

345
00:21:00,970 --> 00:21:04,600
 snaps and after that the idea

346
00:21:04,600 --> 00:21:09,880
 of peaches, the smell of peaches was just nauseating to me.

347
00:21:09,880 --> 00:21:15,040
 I'm going to sort of detail my past.

348
00:21:15,040 --> 00:21:19,380
 But it's conditioning and so we don't deny this, we don't

349
00:21:19,380 --> 00:21:21,680
 try and deny our attractions

350
00:21:21,680 --> 00:21:23,740
 and aversions, we try to learn about them and that's the

351
00:21:23,740 --> 00:21:24,960
 power, that's the wonder of

352
00:21:24,960 --> 00:21:26,040
 it.

353
00:21:26,040 --> 00:21:31,320
 This is perfectly natural, objective.

354
00:21:31,320 --> 00:21:34,310
 There's nothing subjective or particular about the Buddhist

355
00:21:34,310 --> 00:21:35,000
 teaching.

356
00:21:35,000 --> 00:21:39,990
 It takes reality and it comes to see it as it is, boldly

357
00:21:39,990 --> 00:21:42,640
 claiming that when you look

358
00:21:42,640 --> 00:21:45,080
 at reality this is what you'll see.

359
00:21:45,080 --> 00:21:48,980
 It's a bold claim because it's fully open to investigation.

360
00:21:48,980 --> 00:21:52,570
 If you investigate and find out that the Buddhist teaching

361
00:21:52,570 --> 00:21:54,840
 was all wrong, well, that would be

362
00:21:54,840 --> 00:21:55,840
 that.

363
00:21:55,840 --> 00:21:56,840
 That is something you can do.

364
00:21:56,840 --> 00:22:00,310
 There is no need for belief, there's no need to remain in a

365
00:22:00,310 --> 00:22:02,000
 state of faith towards the

366
00:22:02,000 --> 00:22:06,000
 Buddhist teaching.

367
00:22:06,000 --> 00:22:09,880
 Once you look and see reality you will have proof and

368
00:22:09,880 --> 00:22:12,520
 evidence and perfect confidence

369
00:22:12,520 --> 00:22:17,740
 in the Buddhist teaching based on your own understanding.

370
00:22:17,740 --> 00:22:20,530
 So a little food for thought, especially about the body to

371
00:22:20,530 --> 00:22:22,040
 make us all see how ridiculous

372
00:22:22,040 --> 00:22:27,120
 we are with our attachments.

373
00:22:27,120 --> 00:22:30,320
 Reminding us, this monk reminding us of ourselves.

374
00:22:30,320 --> 00:22:35,280
 So there you go, that's the Dhammapada for tonight.

375
00:22:35,280 --> 00:22:36,360
 Thank you all for tuning in.

