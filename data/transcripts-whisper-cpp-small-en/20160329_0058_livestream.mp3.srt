1
00:00:00,000 --> 00:00:10,000
 [silence]

2
00:00:10,000 --> 00:00:20,000
 [silence]

3
00:00:20,000 --> 00:00:30,000
 [silence]

4
00:00:30,000 --> 00:00:39,000
 [silence]

5
00:00:39,000 --> 00:00:46,000
 [silence]

6
00:00:46,000 --> 00:00:48,000
 Good evening everyone.

7
00:00:48,000 --> 00:00:55,000
 Broadcasting Live, March 28th.

8
00:00:55,000 --> 00:01:02,000
 [silence]

9
00:01:02,000 --> 00:01:10,000
 Tonight's verse quote is from Majimani Kaya.

10
00:01:10,000 --> 00:01:24,000
 [silence]

11
00:01:24,000 --> 00:01:36,000
 [silence]

12
00:01:36,000 --> 00:01:37,000
 Hmm.

13
00:01:37,000 --> 00:01:38,000
 It's an interesting one.

14
00:01:38,000 --> 00:01:46,520
 Yousaru Soutasati, who is strong, who is excellent,

15
00:01:46,520 --> 00:02:06,000
 exceptional, will stand.

16
00:02:06,000 --> 00:02:13,170
 They say, "Repeatedly admonishing I shall speak, repeatedly

17
00:02:13,170 --> 00:02:14,000
 testing."

18
00:02:14,000 --> 00:02:43,000
 [silence]

19
00:02:43,000 --> 00:02:53,000
 So I will, Nigayaha is telling you what not to do.

20
00:02:53,000 --> 00:02:55,000
 I will speak to tell you what not to do.

21
00:02:55,000 --> 00:02:59,000
 Baba yah, Baba yah, I will tell you to do this, do this.

22
00:02:59,000 --> 00:03:01,000
 Urge you to do this, to do that.

23
00:03:01,000 --> 00:03:04,000
 So there's two ways of teaching.

24
00:03:04,000 --> 00:03:07,000
 One is by rebuking.

25
00:03:07,000 --> 00:03:09,000
 No, don't do that.

26
00:03:09,000 --> 00:03:15,000
 And the other is by exhorting. Do that.

27
00:03:15,000 --> 00:03:16,000
 This is what is meant here.

28
00:03:16,000 --> 00:03:21,000
 Nigayaha, nigayaha, hananda, wakami.

29
00:03:21,000 --> 00:03:23,000
 When I speak to you, I will speak.

30
00:03:23,000 --> 00:03:26,000
 Sometimes rebuking you, don't do this.

31
00:03:26,000 --> 00:03:30,000
 Baba yah, Baba yah, hananda, wakami.

32
00:03:30,000 --> 00:03:33,000
 I will speak urging you on.

33
00:03:33,000 --> 00:03:40,310
 Yosaro sotasati, and who is strong or who is exceptional,

34
00:03:40,310 --> 00:03:43,000
 who is up for the task,

35
00:03:43,000 --> 00:03:46,000
 they will bear, they will stand.

36
00:03:46,000 --> 00:03:52,000
 They will be able to withstand the teaching.

37
00:03:52,000 --> 00:03:54,430
 That's the very end of the quote, but it's the most

38
00:03:54,430 --> 00:03:56,000
 interesting part.

39
00:03:56,000 --> 00:04:00,000
 Because it's kind of unique.

40
00:04:00,000 --> 00:04:05,440
 He says, "I shall not treat you the way a potter treats

41
00:04:05,440 --> 00:04:07,000
 damp clay."

42
00:04:27,000 --> 00:04:34,000
 So how does a potter treat wet clay?

43
00:04:35,000 --> 00:04:42,000
 A potter is a potter.

44
00:04:42,000 --> 00:04:47,000
 A potter is a potter.

45
00:04:47,000 --> 00:04:52,000
 A potter is a potter.

46
00:04:52,000 --> 00:04:55,000
 A potter is a potter.

47
00:04:55,000 --> 00:05:00,000
 A potter is a potter.

48
00:05:00,000 --> 00:05:05,000
 [Hindi]

49
00:05:05,000 --> 00:05:27,000
 So a potter takes wet clay in both hands.

50
00:05:27,000 --> 00:05:35,000
 And says, "Mabhijatu, don't break."

51
00:05:35,000 --> 00:05:45,000
 I may notice his weight.

52
00:05:45,000 --> 00:05:51,000
 Apakam.

53
00:05:51,000 --> 00:05:55,000
 I don't get it.

54
00:05:55,000 --> 00:06:01,000
 Someone will have to explain that one to me.

55
00:06:01,000 --> 00:06:05,000
 So this is about a teacher, no? This quote is all about,

56
00:06:05,000 --> 00:06:11,000
 I mean that part that I mangled is extra,

57
00:06:11,000 --> 00:06:17,000
 but the main quote is about love for the teacher.

58
00:06:17,000 --> 00:06:20,930
 And the Buddha gives a specific take on the idea of love

59
00:06:20,930 --> 00:06:22,000
 for the teacher.

60
00:06:22,000 --> 00:06:25,750
 When you think of love for a teacher, you think sitting

61
00:06:25,750 --> 00:06:27,000
 there staring at your teacher,

62
00:06:27,000 --> 00:06:30,400
 or looking at pictures of your teacher, thinking about your

63
00:06:30,400 --> 00:06:31,000
 teacher,

64
00:06:31,000 --> 00:06:34,000
 "Boy, I love my teacher."

65
00:06:34,000 --> 00:06:40,010
 Remember, wakkali, no? This is not the way one pays respect

66
00:06:40,010 --> 00:06:44,000
 to the Buddha, to the teacher.

67
00:06:44,000 --> 00:06:48,000
 So how do you have love for your teacher?

68
00:06:48,000 --> 00:06:52,990
 You listen to your teacher. You listen to your teacher,

69
00:06:52,990 --> 00:06:56,000
 lend an ear,

70
00:06:56,000 --> 00:07:01,000
 and you prepare your mind for profound knowledge.

71
00:07:01,000 --> 00:07:06,000
 You do not turn aside or move away from the instruction.

72
00:07:06,000 --> 00:07:10,000
 That's how you love your teacher.

73
00:07:10,000 --> 00:07:13,310
 It's important because the teacher is all about the

74
00:07:13,310 --> 00:07:14,000
 teaching,

75
00:07:14,000 --> 00:07:20,000
 and the teacher's work is to impart the teaching.

76
00:07:20,000 --> 00:07:24,000
 Now if you don't follow it, if you don't listen to it,

77
00:07:24,000 --> 00:07:27,350
 that's really the worst thing for the teacher, makes it

78
00:07:27,350 --> 00:07:28,000
 more difficult,

79
00:07:28,000 --> 00:07:33,000
 causes stress and fatigue to the teacher.

80
00:07:33,000 --> 00:07:35,000
 It's why many arahants just don't teach.

81
00:07:35,000 --> 00:07:39,000
 They say, "You wouldn't understand if I taught you."

82
00:07:39,000 --> 00:07:49,960
 You'd probably just throw it away like an old piece of

83
00:07:49,960 --> 00:07:52,000
 trash.

84
00:07:52,000 --> 00:07:56,000
 So here we, idaeng mohita, idaeng wo sukaya.

85
00:07:56,000 --> 00:07:59,680
 Someone who teaches, "This is for your benefit, this is for

86
00:07:59,680 --> 00:08:01,000
 your happiness."

87
00:08:01,000 --> 00:08:04,000
 This is a teacher, someone who tells you this,

88
00:08:04,000 --> 00:08:07,580
 who rightly identifies what is to your benefit and teaches

89
00:08:07,580 --> 00:08:08,000
 it to you,

90
00:08:08,000 --> 00:08:11,300
 who rightly identifies what is for your happiness and

91
00:08:11,300 --> 00:08:13,000
 teaches it to you.

92
00:08:13,000 --> 00:08:18,000
 Tassasavaka, their student, su su santi.

93
00:08:18,000 --> 00:08:25,000
 Su su santi, their student, su su santi, listen well.

94
00:08:25,000 --> 00:08:30,000
 Sotang odaanti, this is the lending in ear.

95
00:08:30,000 --> 00:08:34,000
 So odaanti, I think, means incline the ear,

96
00:08:34,000 --> 00:08:41,000
 to put down, to place the ear on the object.

97
00:08:41,000 --> 00:08:44,000
 And it's just a figure of speech, of course.

98
00:08:44,000 --> 00:08:47,000
 You don't have to put the ear, but it means it's actually

99
00:08:47,000 --> 00:08:48,000
 listening,

100
00:08:48,000 --> 00:08:51,000
 trying to understand.

101
00:08:51,000 --> 00:09:00,900
 Anya jitang upat hapinti, this is anya, own right knowledge

102
00:09:00,900 --> 00:09:01,000
.

103
00:09:01,000 --> 00:09:09,000
 One sets the mind on knowledge.

104
00:09:09,000 --> 00:09:19,000
 Nacha wo kama, sattu sa, they don't, sattu sa asana odaanti

105
00:09:19,000 --> 00:09:19,000
.

106
00:09:19,000 --> 00:09:27,000
 They don't work to turn aside, they don't dwell odaanti,

107
00:09:27,000 --> 00:09:27,000
 right?

108
00:09:27,000 --> 00:09:32,990
 They don't live, go about their business having put aside

109
00:09:32,990 --> 00:09:35,000
 the sattu sa asana,

110
00:09:35,000 --> 00:09:42,000
 the dispensation or the teaching of the teacher.

111
00:09:42,000 --> 00:09:50,000
 Ey wang ku ananda, sattarang savaka, savaka mita vata ya,

112
00:09:50,000 --> 00:09:52,000
 smudha acharenti.

113
00:09:52,000 --> 00:09:55,000
 It's not actually metta.

114
00:09:55,000 --> 00:10:10,000
 They deport themselves, mita wa, no sabattva.

115
00:10:10,000 --> 00:10:14,560
 Deport them as one who has a friend, who has brought

116
00:10:14,560 --> 00:10:17,000
 themselves friendly, I think.

117
00:10:17,000 --> 00:10:26,000
 Not like an enemy, not hostile, with hostility.

118
00:10:26,000 --> 00:10:30,000
 This is the maha sunyata sutta.

119
00:10:30,000 --> 00:10:33,550
 The mujimini kaya has so many, like a lot of its repet

120
00:10:33,550 --> 00:10:34,000
itions,

121
00:10:34,000 --> 00:10:38,460
 so you might think, well, you know, it's all the same, 150

122
00:10:38,460 --> 00:10:40,000
 sutta.

123
00:10:40,000 --> 00:10:42,000
 But each sutta is unique.

124
00:10:42,000 --> 00:10:46,000
 Each sutta has something interesting to say.

125
00:10:46,000 --> 00:10:49,000
 See what Bhikkhu Bodhi says? No, he doesn't say anything.

126
00:10:49,000 --> 00:10:52,000
 He shall not treat you. Oh, wait, here we are.

127
00:10:52,000 --> 00:10:56,000
 Okay, the potter treats raw damp clay in the way he treats

128
00:10:56,000 --> 00:10:57,000
 the baked pots.

129
00:10:57,000 --> 00:11:00,000
 So after advising once, I shall not be silent.

130
00:11:00,000 --> 00:11:03,000
 I shall advise an instructive, repeatedly admonishing.

131
00:11:03,000 --> 00:11:07,410
 Just as the potter tests the baked pots, puts aside those

132
00:11:07,410 --> 00:11:08,000
 that are cracked,

133
00:11:08,000 --> 00:11:13,000
 split or faulty, and keeps only those that pass the test.

134
00:11:13,000 --> 00:11:21,000
 So I shall advise and instruct. That's not what I read.

135
00:11:21,000 --> 00:11:27,000
 No, he's avoiding the problem, I think.

136
00:11:27,000 --> 00:11:35,000
 He's talking about baked pots, but what about damp clay?

137
00:11:35,000 --> 00:11:39,290
 I don't have to study it more. Anyway, it's an interesting

138
00:11:39,290 --> 00:11:40,000
 symbol.

139
00:11:40,000 --> 00:11:44,000
 The sutta is about emptiness.

140
00:11:44,000 --> 00:11:48,000
 It's interesting, I've just been writing about emptiness.

141
00:11:48,000 --> 00:11:53,900
 Almost finished my, not actually all that long essay on the

142
00:11:53,900 --> 00:11:56,000
 lotus sutra.

143
00:11:56,000 --> 00:12:00,490
 So part of it is about how emptiness isn't really that big

144
00:12:00,490 --> 00:12:02,000
 of a part of the sutra,

145
00:12:02,000 --> 00:12:07,930
 unless you do some wrangling and finessing and reinterpre

146
00:12:07,930 --> 00:12:09,000
ting.

147
00:12:09,000 --> 00:12:15,000
 Anyway, that's neither here nor there.

148
00:12:15,000 --> 00:12:23,000
 So what lesson do we take from this sutra, this quote?

149
00:12:23,000 --> 00:12:26,870
 That if you want to respect your teacher, you should listen

150
00:12:26,870 --> 00:12:28,000
 and do what they say.

151
00:12:28,000 --> 00:12:31,040
 And if you find that when you listen and do what they say,

152
00:12:31,040 --> 00:12:33,000
 it's not working,

153
00:12:33,000 --> 00:12:36,150
 you can ask questions, it's good to ask and get

154
00:12:36,150 --> 00:12:37,000
 clarification,

155
00:12:37,000 --> 00:12:40,420
 but you just feel like this teacher doesn't know what they

156
00:12:40,420 --> 00:12:41,000
're teaching

157
00:12:41,000 --> 00:12:47,000
 and this teaching is not useful to me. And just stop.

158
00:12:47,000 --> 00:12:50,000
 Don't waste your time, don't waste their time.

159
00:12:50,000 --> 00:12:54,210
 But if you're going to do it, if you're going to follow a

160
00:12:54,210 --> 00:12:56,000
 teacher's teaching,

161
00:12:56,000 --> 00:12:59,000
 this is a good quote to keep in mind.

162
00:12:59,000 --> 00:13:03,880
 I mean, really, our teacher is the Buddha, so you should

163
00:13:03,880 --> 00:13:06,000
 think in terms of the Buddha.

164
00:13:06,000 --> 00:13:09,000
 You should prepare yourself for the Buddha's teaching

165
00:13:09,000 --> 00:13:14,490
 and you should try your best to respect the Buddha's

166
00:13:14,490 --> 00:13:16,000
 teaching.

167
00:13:16,000 --> 00:13:19,340
 In Buddhist countries, they won't even put a Dhamma book on

168
00:13:19,340 --> 00:13:20,000
 the floor.

169
00:13:20,000 --> 00:13:24,390
 They will keep the books up high as a measure of their

170
00:13:24,390 --> 00:13:26,000
 respect for it.

171
00:13:26,000 --> 00:13:30,440
 But what is meant by this quote, I mean, that's fine and

172
00:13:30,440 --> 00:13:31,000
 good.

173
00:13:31,000 --> 00:13:35,210
 You should pay these to have in your mind a sense of

174
00:13:35,210 --> 00:13:36,000
 respect.

175
00:13:36,000 --> 00:13:41,740
 But the real importance is that you take the practice

176
00:13:41,740 --> 00:13:43,000
 seriously,

177
00:13:43,000 --> 00:13:49,950
 keep it in mind, and you don't discard it, you don't think

178
00:13:49,950 --> 00:13:51,000
 lightly of it.

179
00:13:51,000 --> 00:13:55,250
 You don't read it and then say, "Oh, well, I know all that

180
00:13:55,250 --> 00:13:57,000
 already. What else is there?"

181
00:13:57,000 --> 00:14:01,460
 It's not about learning new teachings. It's about

182
00:14:01,460 --> 00:14:05,000
 practicing, learning how to practice properly,

183
00:14:05,000 --> 00:14:10,660
 how to live our lives, how to deport ourselves, how to

184
00:14:10,660 --> 00:14:15,000
 progress, move forward,

185
00:14:15,000 --> 00:14:20,190
 go forward in a way that improves our lives, that improves

186
00:14:20,190 --> 00:14:21,000
 our minds,

187
00:14:21,000 --> 00:14:27,530
 makes us better people, makes us more pure, more wise, that

188
00:14:27,530 --> 00:14:29,000
 teaches us,

189
00:14:29,000 --> 00:14:35,000
 teaches us patience, teaches us about our minds,

190
00:14:35,000 --> 00:14:39,200
 teaches us the limits of our capabilities in terms of

191
00:14:39,200 --> 00:14:43,000
 control and in terms of change

192
00:14:43,000 --> 00:14:46,190
 so that we stop trying to force everything or control

193
00:14:46,190 --> 00:14:48,000
 everything.

194
00:14:48,000 --> 00:14:52,540
 We stop trying to fix everything and we start to learn to

195
00:14:52,540 --> 00:14:59,000
 let go and let things naturally go their way,

196
00:14:59,000 --> 00:15:04,960
 take them out of what we understand to be me and mine and

197
00:15:04,960 --> 00:15:06,000
 so on.

198
00:15:06,000 --> 00:15:11,000
 Anyway, it's an interesting quote.

199
00:15:11,000 --> 00:15:14,000
 Useful to remember, love isn't about, love for a teacher

200
00:15:14,000 --> 00:15:17,000
 isn't about worshiping them or praising them.

201
00:15:17,000 --> 00:15:27,000
 It's about following their teachings, not discarding it.

202
00:15:27,000 --> 00:15:34,880
 So if anybody has any questions, I can stick around for a

203
00:15:34,880 --> 00:15:36,000
 bit.

204
00:15:36,000 --> 00:15:45,000
 Otherwise, so questions, you have to come on the Hangout.

205
00:15:45,000 --> 00:15:57,000
 I'm just close to the link to the Hangout.

206
00:15:57,000 --> 00:16:06,000
 Tomorrow, five minute meditation lessons.

207
00:16:06,000 --> 00:16:14,730
 You have to turn off the YouTube video if you come on the

208
00:16:14,730 --> 00:16:16,000
 Hangout.

209
00:16:16,000 --> 00:16:18,000
 Can you hear me now?

210
00:16:18,000 --> 00:16:20,000
 I can hear you, but I think you're echoing.

211
00:16:20,000 --> 00:16:22,000
 Oh, okay.

212
00:16:22,000 --> 00:16:27,000
 Okay, we're better now. It's better now.

213
00:16:27,000 --> 00:16:28,000
 Hello?

214
00:16:28,000 --> 00:16:30,000
 Yeah.

215
00:16:30,000 --> 00:16:32,000
 Can you hear me okay?

216
00:16:32,000 --> 00:16:33,000
 Yes.

217
00:16:33,000 --> 00:16:37,000
 Oh, sorry. So, okay.

218
00:16:37,000 --> 00:16:44,390
 I wanted to ask you, how, okay, let me see how can I phrase

219
00:16:44,390 --> 00:16:45,000
 it.

220
00:16:45,000 --> 00:16:51,000
 How relevant is the role of the teacher in one's spiritual

221
00:16:51,000 --> 00:16:56,390
 progress, and can the student get so-called attached to

222
00:16:56,390 --> 00:17:00,000
 having a teacher and they can't do it on their own?

223
00:17:00,000 --> 00:17:03,030
 Well, when we talk about teacher, there are two types or

224
00:17:03,030 --> 00:17:05,000
 two ways of understanding that word.

225
00:17:05,000 --> 00:17:08,840
 Most of it understand it like, I'm your teacher, me, this

226
00:17:08,840 --> 00:17:10,000
 monk, right?

227
00:17:10,000 --> 00:17:14,410
 But we don't really, you know, that's only a provisional

228
00:17:14,410 --> 00:17:16,000
 sense of the word.

229
00:17:16,000 --> 00:17:19,410
 So it's important to understand, first of all, that the

230
00:17:19,410 --> 00:17:21,000
 Buddha is our teacher.

231
00:17:21,000 --> 00:17:24,630
 And so when you ask how important is a teacher, you have to

232
00:17:24,630 --> 00:17:28,760
 be able to separate those two, because I'm not teaching you

233
00:17:28,760 --> 00:17:30,000
 my teaching.

234
00:17:30,000 --> 00:17:32,770
 I'm teaching you the Buddha's teaching, because my

235
00:17:32,770 --> 00:17:36,000
 understanding of it is, of course, limited.

236
00:17:36,000 --> 00:17:39,560
 Whatever I've realized for myself, that's only a portion of

237
00:17:39,560 --> 00:17:40,000
 it.

238
00:17:40,000 --> 00:17:42,580
 So rather than just trying to teach what I've learned, I

239
00:17:42,580 --> 00:17:45,000
 broaden it and teach you what the Buddha taught.

240
00:17:45,000 --> 00:17:52,630
 So why that's important to clarify is because in that sense

241
00:17:52,630 --> 00:17:56,110
, a teacher is essential, the knowledge, and I don't mean me

242
00:17:56,110 --> 00:17:57,000
 in that sense,

243
00:17:57,000 --> 00:18:01,180
 having the Buddha is essential, because if you're talking

244
00:18:01,180 --> 00:18:04,000
 in general about spiritual practice, well, first of all,

245
00:18:04,000 --> 00:18:05,000
 you're not only going to need a teacher,

246
00:18:05,000 --> 00:18:09,500
 but you need a teacher whose teaching is true and

247
00:18:09,500 --> 00:18:11,000
 beneficial.

248
00:18:11,000 --> 00:18:15,000
 And that is what we claim we have in Buddhism.

249
00:18:15,000 --> 00:18:22,810
 Now, as far as having somebody there like me, then if you

250
00:18:22,810 --> 00:18:27,000
 turn to like me as a teacher, it's a bit different.

251
00:18:27,000 --> 00:18:32,330
 And so the answer is different because you don't need me if

252
00:18:32,330 --> 00:18:34,000
 you have both.

253
00:18:34,000 --> 00:18:37,560
 Well, to some extent, you don't need me because you have

254
00:18:37,560 --> 00:18:39,000
 the Buddha's teaching.

255
00:18:39,000 --> 00:18:41,000
 You can just go and read the books or whatever.

256
00:18:41,000 --> 00:18:45,000
 So the Buddha's teaching is accessible on your own.

257
00:18:45,000 --> 00:18:47,630
 And in that sense, you don't need a teacher because you

258
00:18:47,630 --> 00:18:51,350
 just go find the teachings from the real teacher, right,

259
00:18:51,350 --> 00:18:53,000
 which we have in books and whatever.

260
00:18:53,000 --> 00:18:59,120
 But there are other reasons to have a teacher like me, the

261
00:18:59,120 --> 00:19:04,720
 small teacher, because they push you and well, they push

262
00:19:04,720 --> 00:19:08,000
 you and they're able to clarify the teachings.

263
00:19:08,000 --> 00:19:11,950
 Yeah, I was going to actually say that. I was going to say,

264
00:19:11,950 --> 00:19:15,000
 I feel accountable.

265
00:19:15,000 --> 00:19:18,000
 Someone, your students probably feel accountable to you.

266
00:19:18,000 --> 00:19:21,510
 Yeah, that's a big reason to have a and it's so different

267
00:19:21,510 --> 00:19:24,300
 practicing on your own and having someone that you're

268
00:19:24,300 --> 00:19:28,270
 accountable to, but also someone who can explain the

269
00:19:28,270 --> 00:19:32,770
 teaching and also apply it to you and be able to, as I said

270
00:19:32,770 --> 00:19:36,000
, criticize you and say, don't do that until you do that.

271
00:19:36,000 --> 00:19:39,640
 You have to focus more on this and you have to stop or

272
00:19:39,640 --> 00:19:41,000
 minimize that.

273
00:19:41,000 --> 00:19:49,000
 So now that's not essential, but it's a huge help.

274
00:19:49,000 --> 00:19:54,740
 I'd say it's a huge help. Yeah, someone. But yeah. Okay,

275
00:19:54,740 --> 00:19:56,000
 thank you.

276
00:19:56,000 --> 00:19:58,000
 What was the other question? Did you have another question?

277
00:19:58,000 --> 00:20:02,000
 No, did you read the book Saffron days in L.A.?

278
00:20:02,000 --> 00:20:03,000
 Yes.

279
00:20:03,000 --> 00:20:07,000
 And did you read the follow up Bodhi tree grows in L.A.?

280
00:20:07,000 --> 00:20:09,000
 Yes.

281
00:20:09,000 --> 00:20:12,940
 I know that I've met him. I went to Sri Lanka. The first

282
00:20:12,940 --> 00:20:16,000
 time I went to Sri Lanka was at his invitation.

283
00:20:16,000 --> 00:20:20,640
 And then he stuck me with some guy who ended up really

284
00:20:20,640 --> 00:20:25,000
 ruining my reputation in Sri Lanka for a bit.

285
00:20:25,000 --> 00:20:27,930
 Well, running my reputation with some fairly important

286
00:20:27,930 --> 00:20:31,000
 Buddhist people. It's interesting.

287
00:20:31,000 --> 00:20:33,000
 Were you a monk at the time or not?

288
00:20:33,000 --> 00:20:40,000
 Yeah, that was not so many years ago. 2009 maybe.

289
00:20:40,000 --> 00:20:43,430
 Sounds like a, I won't push, but sounds like an intriguing

290
00:20:43,430 --> 00:20:44,000
 story.

291
00:20:44,000 --> 00:20:46,000
 Yeah, I've got some stories.

292
00:20:46,000 --> 00:20:49,940
 Okay. What do you think about, okay, this is kind of random

293
00:20:49,940 --> 00:20:52,770
 and maybe you don't want to answer, but what do you think

294
00:20:52,770 --> 00:20:57,000
 about like monks that write books and stuff?

295
00:20:57,000 --> 00:21:03,390
 Like this Bhati and then Dalai Lama and anyone else? And

296
00:21:03,390 --> 00:21:05,000
 the nuns, a lot of them do.

297
00:21:05,000 --> 00:21:10,920
 I don't really judge someone by whether they write a book

298
00:21:10,920 --> 00:21:12,000
 or not.

299
00:21:12,000 --> 00:21:16,250
 I thought that book had its positives and its negatives and

300
00:21:16,250 --> 00:21:20,630
 I don't really want to get into them, but that's not what

301
00:21:20,630 --> 00:21:22,000
 you asked anyway.

302
00:21:22,000 --> 00:21:28,000
 I don't have an opinion on if someone writes a book or not.

303
00:21:28,000 --> 00:21:33,000
 Okay, thanks.

304
00:21:33,000 --> 00:21:39,790
 I mean, obviously I think the content of the book is going

305
00:21:39,790 --> 00:21:44,000
 to tell me a lot about the person.

306
00:21:44,000 --> 00:21:49,000
 I thought monks can't get into worldly pursuits though.

307
00:21:49,000 --> 00:21:51,000
 Yeah.

308
00:21:51,000 --> 00:21:55,000
 So writing a book is kind of a worldly pursuit.

309
00:21:55,000 --> 00:21:57,000
 Maybe not. Maybe not.

310
00:21:57,000 --> 00:22:00,000
 It's about worldly things.

311
00:22:00,000 --> 00:22:05,370
 Like if a monk wrote a romance novel, that's probably, well

312
00:22:05,370 --> 00:22:09,000
, it's definitely a bad problem.

313
00:22:09,000 --> 00:22:11,000
 Don't do that.

314
00:22:11,000 --> 00:22:19,000
 But the monk writes, you know, and so Saffron Dies in LA,

315
00:22:19,000 --> 00:22:19,000
 those two books are kind of somewhere in the middle.

316
00:22:19,000 --> 00:22:21,400
 He's very good at tying everything back to the Buddhist

317
00:22:21,400 --> 00:22:27,510
 teaching, but it's also, no, I mean, it's good in that it

318
00:22:27,510 --> 00:22:33,000
 relates it to everyday life.

319
00:22:33,000 --> 00:22:37,330
 I mean, I wasn't 100% on the books, but they had good

320
00:22:37,330 --> 00:22:44,000
 things about them. He's good at what he does.

321
00:22:44,000 --> 00:22:48,180
 Yeah, I found it to be a good read. I didn't read the

322
00:22:48,180 --> 00:22:52,000
 follow up though, so I'm looking forward to that.

323
00:22:52,000 --> 00:22:57,690
 Yeah, he's in, he's in, I can't remember where he is,

324
00:22:57,690 --> 00:23:01,000
 Beverly Hills, Hollywood, I can't remember.

325
00:23:01,000 --> 00:23:11,000
 The rich famous.

326
00:23:11,000 --> 00:23:15,000
 Anyway, I guess that is that all that's all your questions.

327
00:23:15,000 --> 00:23:17,550
 You know, that was just kind of the book thing was on my

328
00:23:17,550 --> 00:23:20,000
 mind. So that's why I mentioned it.

329
00:23:20,000 --> 00:23:24,160
 But I guess the proceeds would go to something else. I'm

330
00:23:24,160 --> 00:23:26,000
 sorry, I'm asking stupid questions.

331
00:23:26,000 --> 00:23:30,000
 I just thought of it as a worldly thing, kind of.

332
00:23:30,000 --> 00:23:32,310
 Well, it was selling books. I guess you could argue that

333
00:23:32,310 --> 00:23:34,000
 selling books is problematic.

334
00:23:34,000 --> 00:23:39,000
 I would agree with that. But writing a book, no.

335
00:23:39,000 --> 00:23:41,000
 No, because you've written books, too.

336
00:23:41,000 --> 00:23:44,010
 Well, I mean, I could be wrong if you want to argue writing

337
00:23:44,010 --> 00:23:46,560
 books is wrong. Just because I wrote one doesn't make it

338
00:23:46,560 --> 00:23:47,000
 right.

339
00:23:47,000 --> 00:23:53,000
 But personally, I don't really see the problem with it.

340
00:23:53,000 --> 00:23:55,660
 I mean, if you can give a dhamma talk, why can't you write

341
00:23:55,660 --> 00:23:59,000
 a book about the dhamma? What's the difference?

342
00:23:59,000 --> 00:24:04,000
 Yeah, that's true. Okay, thank you.

343
00:24:04,000 --> 00:24:10,000
 You're welcome. I'm gonna go. Have a good night, everyone.

344
00:24:10,000 --> 00:24:12,000
 Good night.

345
00:24:14,000 --> 00:24:16,000
 Okay.

346
00:24:17,000 --> 00:24:19,000
 Okay.

347
00:24:21,000 --> 00:24:23,000
 Okay.

