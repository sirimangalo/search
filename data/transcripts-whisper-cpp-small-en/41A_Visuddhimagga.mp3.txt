 When we practice Vipassana meditation,
 we try to be mindful of whatever is at the present moment,
 and that whatever is sometimes
 matter or rupa and sometimes mind or nama.
 Sometimes we are mindful of the breath or the movements,
 and so at that time we are taking the matter as an object,
 and when we are mindful of our thoughts or emotions,
 and we are mindful of the mind or the mental properties,
 so in the beginning we just try to be mindful of mind or
 matter
 whatever is at the present moment,
 and after some time as the concentration gets better,
 we are able to see the individual characteristics
 of what we observe.
 So there are two kinds of characteristics we have to
 understand
 with regard to Vipassana meditation.
 So the first thing we will see is the individual
 characteristics of things.
 So when we concentrate on the mind,
 then we will come to see that mind is that which inclines
 towards the object.
 So when we watch our mind or our consciousness,
 it seems to go to the object, it seems to incline towards
 the object,
 and that inclination towards the object is the
 characteristic of mind.
 And when we concentrate on matter,
 then we will come to see that matter has no cognizing power
,
 it doesn't cognize.
 So that also we will come to see,
 and that is the kind of the characteristic of matter.
 So during these stages,
 we have studied,
 and during these stages of designing mind and matter,
 and during this stage of designing the causes of mind and
 matter,
 a yogi sees the specific or the individual characteristics
 of things.
 Now, from that stage, in order to go to the next stage,
 a yogi needs to see the general characteristics,
 or common characteristics of things.
 Common characteristics means the impermanence,
 the suffering and soullessness of things,
 because impermanence and others are common to all phenomena
,
 all mind and matter, all formations, we call.
 So in order to actually,
 during this stage of seeing the causes,
 a yogi comes to see the arising and disappearing of what he
 observes.
 So when he observes his anger, then he knows that it has
 arisen,
 and also when he disappears, he knows it disappears.
 So arising and falling.
 And that is direct seeing.
 So from that direct seeing, he infers the impermanence and
 so on
 of those he does not observe,
 that is those of the past and those of the future.
 So this chapter, chapter 20,
 deals with that kind of, we pass on that kind of insight
 meditation.
 And in the beginning of that chapter,
 it is said that one who desires to accomplish this field
 first of all,
 applies himself to the inductive insight.
 So it is called inductive insight.
 Actually, it is inferential. It is not direct insight.
 And inferential insight can come only after direct insight.
 So if we do not have direct insight, there can be no inf
erential insight.
 First, we must see the present thing as impermanent.
 And then we infer that just as this present matter is imper
manent,
 so the past matter must be impermanent,
 and also the future matter will be impermanent.
 So from what we have seen directly,
 we infer what we do not see as impermanent suffering and so
 on.
 So in order to develop that insight,
 here it is said that we must apply to the inductive insight
 called comprehension by groups.
 So the beginning part of this chapter deals with this
 insight,
 comprehension by groups or inductive insight.
 And with regard to this insight,
 the other mentions the three kinds of full understanding.
 The full understanding as the known,
 full understanding as investigating,
 and full understanding as abandoning.
 Actually, we have gone through these parts.
 So the full understanding as the known
 is when we see the individual characteristics of things.
 So now a yogi is to go into the second stage,
 which is full understanding as investigating.
 So how does a yogi practice insight comprehension by groups
?
 Now whenever a matter or something is mentioned,
 Buddha described that as of first future, present,
 internal, external, gross, subtle and so on.
 So there are 11 of those things.
 Those are given in paragraph 6.
 "Any materiality whatever but the first future present."
 3. "Internal, external."
 5. "Gross, subtle."
 7. "Inferior, superior."
 9. "Far, near."
 11. So with regard to these 11 aspects,
 materiality is described,
 and different kinds of materiality are described.
 So now when a yogi does comprehension by group,
 he does not take each one of the material properties
 separately,
 but just the materiality as a group as one,
 and then materiality which belongs to the past is imper
manent,
 and materiality which belongs to the future is impermanent
 and so on.
 So when he takes matter as a whole and not as, say,
 not taking as an other element, other element and so on
 separately,
 then he is said to be doing the comprehension by groups.
 Actually, groups means just taking your ruba,
 I mean if it is ruba, taking ruba as a whole.
 So a person can practice this meditation or ruba as
 all ruba is impermanent.
 That is one comprehension.
 Or he can take ruba with reference to the 11 aspects.
 So ruba in the past is impermanent,
 ruba in the present is impermanent,
 ruba in the future is impermanent,
 and then internal ruba is impermanent,
 external ruba is impermanent and so on.
 If he does that way, then he is said to be doing it in 11
 different ways.
 So there can be one comprehension or there can be 11
 comprehensions.
 So we are described in the following paragraphs.
 Now if you look at paragraph 13,
 it is said there,
 "Here is the application of directions dealing with the
 aggregates.
 Any materiality whatever, whether past, future, present,
 internal,
 external, close, or subtle, inferior, superior, far, or
 near,
 he defines all materiality as impermanent.
 This is one kind of comprehension."
 And then, one word is missing there.
 Or it is stated in a brief, elliptical way.
 So you see only two there, right?
 Impermanent and not self.
 What is missing?
 Dukkha, I mean, suffering is missing.
 So there must be something like painful.
 He defines as painful.
 And then dots, he defines as not self.
 This is one kind of comprehension.
 So this is taking ruba as a whole.
 And then also taking ruba as a whole, but with reference to
 first and so on, we get 11 kinds of contemplation.
 So towards the end of paragraph 14, it is said that,
 "And all this is impermanent in the sense of destruction.
 Accordingly, there is one kind of comprehension in this way
.
 But it is affected in 11 ways.
 So it can be only one kind of contemplation or 11 kinds of
 contemplation.
 If we take ruba as a whole, then it is one kind of
 contemplation.
 If we take ruba with reference to first, future, present
 and so on,
 then we get 11 ways of contemplating on matter or material
ity.
 Just as there is one or 11 contemplations with regard to
 impermanence,
 there are one or 11 with regard to suffering or painful.
 Paragraph 15, and the same with another.
 No self in paragraph 16.
 So one contemplation or 11 contemplation.
 Now, the word another.
 In paragraph 16, we have one meaning of the word another,
 and that is what?
 In the sense of having no core.
 So this is one meaning of another.
 The word another is defined as meaning,
 as to have different meanings.
 So one meaning is that there is no core.
 That is why it is called another.
 So when we say ruba is another, we mean ruba has no inner
 core or substance or whatever.
 So this is one meaning or one interpretation of the word
 another.
 We'll meet another interpretation later.
 After doing this, and also with regard to the other aggreg
ates, feeling and so on,
 the yogi is instructed to view mind and matter in 40
 different ways.
 That is paragraph 18 and so on.
 So strengthening of comprehension of impermanence, etc., in
 40 ways.
 So they are given in brief and also each term is defined in
 paragraph 19.
 So these begin with and with the sound to in Pali.
 So aniccha to, dukkato, rogato, kandato and so on.
 So in Bhama, these are called 40 toes.
 You know, the word toe in Bhama means a forest.
 So it is something like a play of words.
 The word toe really is a Pali particle.
 It is suffix, which means "as" or "frore" or "because of"
 or here "as".
 So the Pali word toe is taken to mean something like a
 forest here.
 So we have 40 toes.
 So 40 ways of looking at mind and matter.
 And these 40 ways are later divided into meaning, imperman
ence, painfulness and no self.
 So as I said last week, we have to add...
 We have to make changes somewhere in paragraph 19.
 At the bottom of paragraph 19, and if you come up to two or
 three lines,
 you see adversity and because of being.
 So the word "and" should be wrapped out and then "as" "
terror" should be put there.
 So "as" "terror" because of being the basis for all kinds
 of terror.
 And on page 712, about 15 lines down, the word "vein"
 should be in italics.
 That's not so important, but it should be in italics.
 So we get all together 40 different ways of contemplating
 on Nama and Dhruva actually.
 And in paragraph 20, these are grouped with regard to imper
manence, suffering and no self.
 Now there are 50 kinds of contemplation of impermanence
 here
 by taking the following 10 in the case of each aggregate.
 As impermanence, as a disintegrating, as fickle and so on.
 There are 25 kinds of contemplation of not-self.
 And then there are 25 kinds of contemplation on pain
 by taking the rest beginning with the painful, acid disease
 and so on.
 These 40 are divided into three groups.
 Impermanence, pain and not-self.
 So after viewing Man and Medha in this way,
 the yogi is instructed to sharpen his faculties.
 That is, if he cannot bring this contemplation to success,
 if he does not succeed in contemplating in this way,
 then he should sharpen his faculties, by the way stated in
 paragraph 21.
 That is, when he sees only the destruction of arisen
 formations,
 and in that occupation he makes sure of working carefully
 and so on.
 So he has to sharpen his faculties by these methods.
 And there, number eight,
 wherein he overcomes pain by renunciation.
 What does that mean?
 Overcome pain by renunciation.
 Here renunciation simply means effort.
 The Pali word is sometimes misleading, nikamah.
 Nikamah means to get out of.
 So here, to get out of laziness.
 That means willier or effort.
 So to overcome pain by renunciation really means
 to overcome pain by effort, making effort.
 So he should avoid the seven unsuitable things
 in the way stated in the description of Adkasina,
 they are explained before,
 and cultivate the seven suitable things,
 and he should comprehend the material at one time
 and the immaterial at another.
 So at one time he should concentrate on Rupa,
 Medha, and at another time on Nama or immaterial things.
 Now, contemplation of the material.
 While comprehending materiality,
 he should see how materiality is generated.
 So how it arises, or it's arising, it's generation.
 So that should be the object of his meditation at that time
.
 So that is to say how this materiality is generated
 by the four causes beginning with karma.
 So there are four causes of Medha.
 I think you have met them before.
 Paragraph 19, I mean, Chapter 19, Paragraph 9.
 Those are what?
 Karma, consciousness, temperature, and nutrition.
 So when materiality is being generated in any being,
 it is first generated from karma.
 So the first Medha that arises at the beginning of a given
 life
 is generated by karma, or is a result of karma.
 For the actual movement of rebirth linking of a child in
 the womb.
 First, 30 instances of materiality,
 that means 30 material properties are generated
 in the triple continuity.
 In other words, the decades of physical heart basis, body,
 and sex.
 So the decade of physical heart basis,
 the decade of body, and the decade of sex.
 And those are generated at the actual instant
 of the rebirth linking consciousness as arising.
 And as at the instant of its arising,
 so too at the instant of its presence,
 and at the instant of its dissolution.
 Now, there are material properties generated by karma.
 And these material properties generated by karma
 arise at the very first moment in one's life.
 And each movement has three sub-moments.
 So at every sub-moment also,
 the Medha generated by karma arises.
 So at every moment, the karma bond Medha arises
 all through the life until very, very close to the moment
 of death.
 So at all three instances,
 I mean three sub-moments,
 the karma bond Medha is generated.
 So at the first moment, at the arising moment,
 let's say how many 30, right?
 So at the arising moment there are 30, right?
 And this 30 will exist for 17 thought moments.
 So now, at the moment of sub-moment of presence,
 or static moment, another 30 are produced.
 So at that moment there are 60,
 the first 30 and then the new 30, so there are 60.
 At the dissolution moment of relinking consciousness,
 another 30 are produced.
 And so at that moment there are 90.
 And then at the next moment there are 120 and so on.
 So until it goes to the 17th thought moments.
 After 17th thought moments, the number will be constant
 because three produce and three disappear.
 Something like that.
 So that means 30 produce and 30 disappear.
 Herein, the cessation of materiality is slow
 and its transformation ponder us.
 We should note this.
 This means that materiality or Medha is slow,
 slow in dissolution.
 That means the life of Medha is longer than the life of
 consciousness.
 Medha exists 17 times that of the thought moment.
 So when Medha arises, it will exist for,
 it will last for 17 thought moments before it disappears.
 So material, cessation of materiality is slow.
 That means it has a longer life than a thought moment.
 And its transformation ponder us
 while the cessation of consciousness is strict
 and its transformation quit.
 But the arising and disappearing of thought moments are
 quit.
 Just one moment or say one big moment or three small
 moments.
 Hence it is said, because I see no other one thing
 that is so quickly transformed as the mind.
 That is the word of the Buddha.
 For the life continuum consciousness arises and ceases 16
 times
 while one material instance than yours.
 Now, those that are produced at the first moment of life
 relinking
 will last until the 17th thought moment.
 So while 16 thought moments come and go, they still exist.
 With consciousness, the instant of arising,
 instant of presence, that means static stage,
 and instant of dissolution are equal.
 They are same.
 So I said that the life of the material property is 17
 thought moments.
 So how many small moments?
 51 small moments.
 17 big moments. 51 small moments.
 Now, the instant of arising, the instant of presence,
 then the instant of dissolution.
 With regard to consciousness, these three instances
 are of equal duration, equal, the same.
 But with regard to material property,
 then instance of arising is one small moment or one sub-mom
ent.
 The instance of dissolution, one sub-moment.
 Then the 49 thought moments in between
 are one instance of presence for material property.
 You follow?
 Let me say, matter.
 Matter lasts for 51 small moments.
 51 small moments of consciousness.
 So the arising, the presence, and the dissolution of matter
 is different from the arising presence and dissolution of
 consciousness.
 Now, the arising and the dissolution of both mind and
 matter are equal.
 Just one sub-moment.
 But the presence of consciousness is only one sub-moment.
 But the presence of matter is 49 sub-moments.
 So with consciousness, the instant of arising,
 instant of presence, and instant of dissolution are equal.
 But with materiality, only the instance of arising and
 dissolution are quick,
 like those of consciousness,
 while the instant of its presence is long and lasts,
 while 16 consciousnesses arise and cease.
 That means after it's arising, 16 consciousnesses.
 Now, paragraph 25, that is important.
 The second life continuum.
 But the other has not mentioned the first life continuum,
 right?
 If you read it before you come to the class,
 I was wondering what you understand by that.
 Now, the actual meaning here is,
 the life continuum as a second consciousness arises.
 Now, the relinking consciousness is a first consciousness.
 And life continuum is a second consciousness.
 So here the translation should not be second life continuum
.
 But the life continuum as a second consciousness arises.
 The Pali word, you are familiar with doo-dee-yam-beetah-dee
-yam-beetah, right?
 When you take refuge, you say doo-dee-yam-beeboo-dang-saran
anga-cham-ee and so on.
 So from the word doo-dee-yam-beetah, if you take out pee,
 you say doo-dee-yam.
 Now, that word can be an adjective or an adverb.
 Here it is an adverb, not an adjective.
 So in the Pali it is a doo-dee-yam-beowanga.
 Beowanga means life continuum.
 So doo-dee-yam-beowanga.
 That does not mean the second beowanga.
 But as a second consciousness, beowanga arises.
 Here also the life continuum as a second consciousness
 arises
 with the pre-nescent physical heart basis as its support,
 which has already reached presence and arose at the rebirth
,
 leaning consciousness as instant of arising.
 That also is not accurate.
 He put the what is to be at the end, he put it in front.
 Now, here it should be, the translation should run like
 this.
 The life continuum as a second consciousness arises
 with the pre-nescent physical heart basis as its support,
 which arose at the rebirth,
 leaning consciousness as instant of arising
 and has already reached presence.
 Sequence should be like that.
 We are going to change that sequence here
 because it must first arise and then it must reach the
 instant of presence.
 So that means when life continuum arises,
 the heart base which arose at the moment of relinking
 has reached the instant of presence.
 Now, here is relinking consciousness,
 one and two, life continuum consciousness.
 So the heart base arose with relinking consciousness.
 So at the moment of the first moment, it is arising.
 But later on, it has reached its presence or the static
 stage.
 So when it is in its static stage,
 the second consciousness or the bhavanga arises.
 That bhavanga arises depending upon this heart base.
 That is what the others are talking about here.
 So the life continuum arises with the pre-nescent physical
 heart basis as its support.
 That heart basis has already arisen and now it has reached
 the stage of presence.
 At that moment, the life continuum arises.
 So it arises taking the heart base as its support.
 So the pre-nescent physical heart basis as its support
 which arose at the rebirth leading consciousness
 as instant of arising and has already reached presence.
 Now, the third life continuum.
 The life continuum has a third consciousness arises.
 Now for your information, after relinking consciousness,
 there are how many arises of life continuum?
 16. So there are 16 life continuum consciousness arising
 after relinking consciousness.
 So here, third means not the third life continuum
 because it is really the second continuum.
 So the life continuum as the third consciousness arises
 with the pre-nescent physical basis as its support and so
 on.
 So it goes this way, that way.
 With the arising moment of first life continuum,
 there arises heart basis.
 But when the second life continuum arises,
 that heart basis has reached its present state.
 And then it forms a basis for the second life continuum.
 And like this, going the later consciousness
 taking support as the heart basis which arose
 at least one big moment before its arriving.
 If you say, put the numbers 1, 2, 3 and 4,
 let us say, number two consciousness arises
 taking heart basis which arose with number one
 consciousness as its support.
 And then number three consciousness arises
 taking the heart basis which arose with the second
 consciousness
 as its support and so on.
 So it can go all through our life.
 So it happens in this way throughout life.
 But in one who is facing death,
 16 consciousnesses arise with a single pre-nescent physical
 heart basis
 as their support which has already reached present.
 Now, when a person is about to die,
 the all-coma-born matter cease to arise
 with the 16th or maybe 17th consciousness
 recon backward from the dead consciousness.
 Suppose here is dead consciousness,
 then we recon backward taking dead consciousness as 1, 2, 3
, 4, 1, 17.
 So with the dissolution of dead consciousness,
 no more coma-born matter are produced in that life.
 But what is produced at that moment will last until the
 moment of death.
 So there is only one heart base at that time for these 16
 thought moments.
 So, but in one who is facing death,
 16 consciousnesses arise with a single pre-nescent physical
 heart basis
 as their support which has already reached present.
 These are all a bit of a...
 And the materiality that arose at the instant of arising of
 the
 rebirth-leaning consciousness ceases along with the 16th
 consciousness
 and the rebirth-leaning consciousness and so on.
 So if you understand that a material property
 lasts for 17 thought moments, then you can understand this.
 Now, next is coma-born materiality.
 These are all a bit of a...
 With regard to coma-born materiality,
 then we are to understand how many?
 Six, right?
 And what is originated by coma?
 What has coma as its condition?
 What is originated by consciousness that has coma as its
 condition?
 What is originated by nutriment that has coma as its
 condition?
 And what is originated by temperature that has coma as its
 condition?
 So there are six kinds to be understood with regard to coma
-born matter.
 Now here, coma is profitable and unprofitable for listen.
 Now that is very, very important.
 If we talk about coma, we often say coma is a deed, a good
 deed or action.
 Actions are called coma.
 But technically speaking, coma means volition.
 Profitable and unprofitable of wholesome and unwholesome
 volition.
 And what is originated by coma is a coma result and aggreg
ates
 and the 70 instances of materiality beginning with the eye-
taker and so on.
 We have to take it from Abhidhamma.
 And then what has coma as its condition is the same as the
 last,
 since coma is the condition that upholds what is originated
 by coma.
 And what is originated by consciousness that has coma as
 its condition?
 Is materiality originated by coma result and consciousness?
 Consciousness produces matter, right?
 So the matter produced by result and consciousness is
 called
 what is originated by consciousness that has coma as its
 condition.
 And then what is originated by nutriment that has coma as
 its condition
 is the so-called sense.
 The nutritive essence that has reached presence in the
 instances of materiality
 originated by coma originates a further octet with nutrit
ive essence as 8.
 That means the inseparable 8.
 And the nutritive essence there that has reached presence
 also originates a further one and so on.
 So it links up four or five occurrences of octets.
 What is originated by temperature that has coma as its
 condition is called?
 Since the coma bonfire element that has reached presence
 originates
 and octet with nutritive essence as 8 and so on.
 So with regard to coma bon materiality we have to
 understand 16.
 And then with regard to consciousness bon materiality how
 many?
 Five. Consciousness. What is originated by consciousness?
 What is consciousness as its condition?
 What is originated by nutriment that has consciousness as
 its condition?
 And what is originated by temperature that has
 consciousness as its condition?
 Here in consciousness is the 89 kinds of consciousness.
 So you take them from the chart at the end of the book
 or from the manual of a vidamat.
 So consciousness 32 and 26 and 19 to a record to give birth
 to madam and so on.
 Now among them there are some consciousnesses which produce
 not only madam
 but postures and intimations and some just postures and so
 on.
 So some types of consciousness can help us to maintain a
 posture for a long time.
 That is why when you have a good concentration
 you can sit for two, three hours without feeling any
 discomfort,
 without feeling any weakness or without feeling sleepy or
 whatever.
 So that happens to yogis when they reach to the higher
 stages of vipassana meditation.
 So their meditation is very good and their concentration is
 strong
 and so they can sit for a long time.
 Sometimes they don't sleep at all for one or two days
 but they don't feel any ill effects or lack of sleep or
 whatever.
 So that is because some types of consciousness have the
 ability
 to maintain the posture, to keep our bodies in one posture
 for a long time.
 And that is why when a person is entrenched in, we call it
 samabaddhi, in jhana
 then he could sit for seven days without changing his
 posture.
 So there are some types of consciousness which helps to
 maintain postures.
 I think there are two groups.
 The first group, right?
 The two consciousness namely the A-profitable consciousness
,
 the 12-unprofitable, the 10-functional excluding the mind
 element
 and the two direct knowledge consciousnesses are profitable
 and functional
 give rise to materiality, to posture and to intimacy.
 And the next 26 are jhavana movements of rupa, vajra, arupa
, vajra and vakupra.
 So they give rise to materiality, to posture but not to
 intimacy and so on.
 Now the next paragraph, what is originated by consciousness
 is the three other material aggregates
 that is feeling, perception and mental formation.
 And 17 full materiality, namely the sound,
 and that bodily intimation, verbal intimation and so on.
 Now, the student of abhirama.
 Do you find any discrepancy here with the manual of abhir
ama?
 In the manual of abhirama, only 15 are said to be born of
 consciousness, right?
 Here they are given as 17.
 So growth and continuity are not included in those
 originated by consciousness
 in the manual of abhirama.
 But here they are also taken to be originated by
 consciousness.
 These two will be added to other groups too.
 And what does consciousness as its condition is the
 materiality
 of full full origination stated thus?
 Post nascent states of consciousness and consciousness con
comitants
 are a condition as post nascent condition
 for its present body, a pre nascent body, from patana.
 And what is originated by nutriment that is consciousness
 as its condition?
 The nutritive essence that is reached pre nascent in
 consciousness
 originated in material instances,
 originated further octet with nutritive essence as A,
 and thus links up two or three occurrences of octets.
 These are all abhirama.
 If you want to understand them,
 please read chapter 6 of the manual of abhirama.
 After chapter 6, you come back here
 and then you will understand.
 Nutriment born material is the same as nutriment.
 What is originated by nutriment and so on.
 Here also paragraph 36, what is originated by nutriment?
 There are 14 material properties mentioned here.
 But in the manual of abhirama, only 12 are mentioned
 without growth and continuity.
 And then in paragraph 37, one thing is worthy of notice,
 and that is nutriment smeared on the body originates
 materiality.
 Now, normally we think that only food eaten through mouth
 can originate materiality.
 But here it is said that also nutriment smeared on the body
 originates materiality.
 So sometimes it may be possible to smear some food on our
 body
 and then it will cause the nutriment born matter to arise.
 And then next temperature born materiality,
 we are also in paragraph 40, that materiality...
