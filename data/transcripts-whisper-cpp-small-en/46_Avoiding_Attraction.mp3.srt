1
00:00:00,000 --> 00:00:03,280
 Avoiding Attraction

2
00:00:03,280 --> 00:00:06,310
 When I see and interact with a specific female, my mind sc

3
00:00:06,310 --> 00:00:07,760
atters away while I am trying to

4
00:00:07,760 --> 00:00:09,160
 stay mindful.

5
00:00:09,160 --> 00:00:12,100
 Would it be okay to disregard what I see and only meditate

6
00:00:12,100 --> 00:00:13,440
 on what I hear when talking

7
00:00:13,440 --> 00:00:17,170
 to her, or would that be cheating and not facing the

8
00:00:17,170 --> 00:00:18,080
 problem?

9
00:00:18,080 --> 00:00:22,460
 Yes, sometimes you will want to just focus on hearing

10
00:00:22,460 --> 00:00:23,520
 instead.

11
00:00:23,520 --> 00:00:27,180
 One of the parts of the comprehensive practice taught in

12
00:00:27,180 --> 00:00:29,400
 the Saba Saba Sutta is Samvara,

13
00:00:29,400 --> 00:00:32,660
 which means "the guarding of one's faculties," a very

14
00:00:32,660 --> 00:00:35,600
 important part of one's practice.

15
00:00:35,600 --> 00:00:38,680
 The Saba Saba Sutta says that in order to support your

16
00:00:38,680 --> 00:00:40,440
 practice of seeing things as

17
00:00:40,440 --> 00:00:42,880
 they are, you have to guard your senses.

18
00:00:42,880 --> 00:00:45,790
 Of course, seeing does not mean looking with the eyes,

19
00:00:45,790 --> 00:00:47,640
 rather it means directly understanding

20
00:00:47,640 --> 00:00:50,560
 all experiences as they are.

21
00:00:50,560 --> 00:00:53,600
 When you are unable to see certain experiences as they are,

22
00:00:53,600 --> 00:00:55,600
 and they cause immediate reaction,

23
00:00:55,600 --> 00:00:58,110
 this can be a great hindrance in the practice, so you

24
00:00:58,110 --> 00:01:00,320
 should always guard your senses, letting

25
00:01:00,320 --> 00:01:03,270
 in only those things that you are now able to deal with one

26
00:01:03,270 --> 00:01:04,840
 by one, in order to observe

27
00:01:04,840 --> 00:01:07,440
 them clearly as they are.

28
00:01:07,440 --> 00:01:10,030
 With the example of beauty, it is not reasonable to expect

29
00:01:10,030 --> 00:01:11,760
 that someone should overcome their

30
00:01:11,760 --> 00:01:14,410
 attachment to beauty while being surrounded by many

31
00:01:14,410 --> 00:01:15,480
 beautiful things.

32
00:01:15,480 --> 00:01:17,900
 They are much more likely to just give up the practice as

33
00:01:17,900 --> 00:01:19,200
 they will be overwhelmed by

34
00:01:19,200 --> 00:01:20,680
 desire.

35
00:01:20,680 --> 00:01:23,040
 You have to be able to guard your senses at least until you

36
00:01:23,040 --> 00:01:24,160
 get to the point that you

37
00:01:24,160 --> 00:01:27,960
 are able to deal with every situation mindfully.

38
00:01:27,960 --> 00:01:31,390
 The monk Ananda asked the Buddha, "What should we do in

39
00:01:31,390 --> 00:01:32,800
 regards to women?"

40
00:01:32,800 --> 00:01:36,280
 And the Buddha said, "The best thing is not see them."

41
00:01:36,280 --> 00:01:39,600
 Ananda then asked, "Well, what should we do if we have to

42
00:01:39,600 --> 00:01:40,480
 see women?"

43
00:01:40,480 --> 00:01:44,360
 And the Buddha said, "Well, then do not talk to them."

44
00:01:44,360 --> 00:01:48,060
 Then Ananda asked, "Well, what if we have to talk to them?"

45
00:01:48,060 --> 00:01:52,440
 The Buddha said, "Then be very, very mindful."

46
00:01:52,440 --> 00:01:55,380
 Some people might take the teaching on guarding the senses

47
00:01:55,380 --> 00:01:56,800
 the wrong way and think that it

48
00:01:56,800 --> 00:02:00,680
 teaches you to ignore experiences and not investigate them.

49
00:02:00,680 --> 00:02:03,580
 In truth, it simply takes into account the practical

50
00:02:03,580 --> 00:02:05,760
 reality, the impossibility of coming

51
00:02:05,760 --> 00:02:08,320
 to see things as they are and letting things be as they are

52
00:02:08,320 --> 00:02:09,560
 when you are surrounded by

53
00:02:09,560 --> 00:02:14,330
 things that immediately and violently trigger partiality of

54
00:02:14,330 --> 00:02:15,240
 any sort.

55
00:02:15,240 --> 00:02:18,010
 In order to begin to understand the things that trigger

56
00:02:18,010 --> 00:02:19,640
 partiality, first you have to

57
00:02:19,640 --> 00:02:21,280
 remove yourself from them.

58
00:02:21,280 --> 00:02:24,280
 You have to begin from an empty slate and then face them

59
00:02:24,280 --> 00:02:26,040
 one by one, to the extent that

60
00:02:26,040 --> 00:02:29,420
 you are able to observe them without being carried away.

61
00:02:29,420 --> 00:02:32,110
 You have to think like a scientist and appreciate how much

62
00:02:32,110 --> 00:02:33,760
 more difficult it is to understand

63
00:02:33,760 --> 00:02:36,520
 reality than to just live it normally.

64
00:02:36,520 --> 00:02:39,780
 In order to develop understanding, we have to observe

65
00:02:39,780 --> 00:02:41,400
 experiences one by one.

66
00:02:41,400 --> 00:02:44,380
 We cannot let everything in and expect to be able to

67
00:02:44,380 --> 00:02:46,280
 understand it all clearly as it

68
00:02:46,280 --> 00:02:48,140
 is.

69
00:02:48,140 --> 00:02:51,270
 This sounds like that is the sort of thing you are trying

70
00:02:51,270 --> 00:02:52,040
 to grasp.

71
00:02:52,040 --> 00:02:54,690
 The best thing to do from a practical standpoint is to run

72
00:02:54,690 --> 00:02:56,320
 away from this woman and not have

73
00:02:56,320 --> 00:02:57,680
 any contact with her.

74
00:02:57,680 --> 00:03:00,200
 That's most likely quite difficult.

75
00:03:00,200 --> 00:03:02,940
 So if you do not run away and break contact with her, you

76
00:03:02,940 --> 00:03:04,220
 should use your situation as

77
00:03:04,220 --> 00:03:05,220
 a laboratory.

78
00:03:05,220 --> 00:03:08,410
 Do not go out of your way to see her, nor go out of your

79
00:03:08,410 --> 00:03:09,660
 way to avoid her.

80
00:03:09,660 --> 00:03:12,720
 Try to see her in moderation and slowly develop mindfulness

81
00:03:12,720 --> 00:03:14,440
 based on the experiences you have

82
00:03:14,440 --> 00:03:16,520
 while around her.

83
00:03:16,520 --> 00:03:19,830
 Once you have set it in your mind that this is a meditation

84
00:03:19,830 --> 00:03:21,880
 object for me, then eventually

85
00:03:21,880 --> 00:03:25,730
 with practice, every time you see the object of your desire

86
00:03:25,730 --> 00:03:27,960
, you will be able to be mindful.

87
00:03:27,960 --> 00:03:31,250
 When you sit down on a meditation mat, immediately your

88
00:03:31,250 --> 00:03:33,480
 mind says, "Now I am meditating."

89
00:03:33,480 --> 00:03:36,180
 And you are on a meditation mat and your legs and hands are

90
00:03:36,180 --> 00:03:37,480
 in a special posture.

91
00:03:37,480 --> 00:03:40,430
 You are sitting in a special way, so you think this is

92
00:03:40,430 --> 00:03:41,400
 meditation.

93
00:03:41,400 --> 00:03:44,090
 On the other hand, when we do something like eating, we

94
00:03:44,090 --> 00:03:45,760
 think this is eating, and so we

95
00:03:45,760 --> 00:03:49,150
 lose our mindfulness because we don't think of it as

96
00:03:49,150 --> 00:03:50,160
 meditation.

97
00:03:50,160 --> 00:03:53,210
 If we thought of eating as a meditation practice, we would

98
00:03:53,210 --> 00:03:54,980
 eat the whole meal mindfully.

99
00:03:54,980 --> 00:03:57,630
 In some Zen Buddhist centers, they practice eating in a

100
00:03:57,630 --> 00:03:59,060
 very specific manner and as a

101
00:03:59,060 --> 00:04:01,760
 result they can eat very mindfully.

102
00:04:01,760 --> 00:04:04,790
 So you can take your relationship with this woman as being

103
00:04:04,790 --> 00:04:06,600
 a meditation object, and every

104
00:04:06,600 --> 00:04:10,480
 time you see her, it is a meditation session.

105
00:04:10,480 --> 00:04:13,230
 The other thing I would recommend is to not be too hard on

106
00:04:13,230 --> 00:04:13,940
 yourself.

107
00:04:13,940 --> 00:04:16,240
 You will fail sometimes, and you will fall into liking

108
00:04:16,240 --> 00:04:17,620
 things and people, and you may

109
00:04:17,620 --> 00:04:20,360
 wind up being totally blown away and unmindful.

110
00:04:20,360 --> 00:04:23,820
 But that is just another experience for you to learn from.

111
00:04:23,820 --> 00:04:27,130
 If you react to your failures in a negative way, you will

112
00:04:27,130 --> 00:04:28,820
 learn nothing from them.

113
00:04:28,820 --> 00:04:31,400
 There is the saying, "We learn from our mistakes."

114
00:04:31,400 --> 00:04:33,880
 But we do not always learn from our mistakes.

115
00:04:33,880 --> 00:04:36,060
 We learn from our mistakes when we actually have the

116
00:04:36,060 --> 00:04:37,680
 presence of mind to understand what

117
00:04:37,680 --> 00:04:40,620
 went wrong and accept that we have made a mistake

118
00:04:40,620 --> 00:04:42,840
 objectively without reaction.

119
00:04:42,840 --> 00:04:45,870
 We do not learn anything from mistakes if we get angry and

120
00:04:45,870 --> 00:04:47,020
 upset about them.

121
00:04:47,020 --> 00:04:49,210
 At the time when you like this woman and are attracted to

122
00:04:49,210 --> 00:04:50,560
 her beauty, you should not be

123
00:04:50,560 --> 00:04:54,200
 saying to yourself, "Oh, I am such an evil, nasty person.

124
00:04:54,200 --> 00:04:56,040
 This is sinful and bad."

125
00:04:56,040 --> 00:04:59,140
 It is important to accept when you make mistakes and to see

126
00:04:59,140 --> 00:05:01,080
 the experience of making mistakes

127
00:05:01,080 --> 00:05:02,680
 as it is.

128
00:05:02,680 --> 00:05:05,840
 Preparing yourself is one thing, but you must acknowledge

129
00:05:05,840 --> 00:05:07,320
 when you do feel desire.

130
00:05:07,320 --> 00:05:11,160
 You cannot stop yourself from experiencing it.

131
00:05:11,160 --> 00:05:14,330
 Western society is so caught up with the sin of sexuality

132
00:05:14,330 --> 00:05:16,320
 and things like masturbation.

133
00:05:16,320 --> 00:05:19,260
 Masturbation, for example, is an issue that people feel

134
00:05:19,260 --> 00:05:20,720
 utterly wretched about.

135
00:05:20,720 --> 00:05:23,070
 They feel terribly guilty as though they have committed

136
00:05:23,070 --> 00:05:24,680
 some cardinal sin by doing something

137
00:05:24,680 --> 00:05:26,960
 that they actually like doing.

138
00:05:26,960 --> 00:05:28,920
 Deep down, their heart says, "This is good.

139
00:05:28,920 --> 00:05:30,240
 This is pleasant."

140
00:05:30,240 --> 00:05:32,440
 So how can you lie to yourself in that way?

141
00:05:32,440 --> 00:05:35,520
 If you still enjoy it, how can you say it is bad?

142
00:05:35,520 --> 00:05:38,520
 This is what has led many Hindu gurus to lead their Western

143
00:05:38,520 --> 00:05:40,400
 students in the other direction.

144
00:05:40,400 --> 00:05:43,260
 There was even a Zen book that talked about a teacher who

145
00:05:43,260 --> 00:05:45,080
 taught people to find liberation

146
00:05:45,080 --> 00:05:47,280
 through sexuality.

147
00:05:47,280 --> 00:05:49,920
 Some people even teach Tantric Buddhism.

148
00:05:49,920 --> 00:05:52,600
 There are Hindu gurus who believe that Westerners are so

149
00:05:52,600 --> 00:05:54,520
 sexually repressed that they put them

150
00:05:54,520 --> 00:05:58,400
 together in a room and tell them to have sexual orgies.

151
00:05:58,400 --> 00:06:00,720
 We do not do this because it is reacting.

152
00:06:00,720 --> 00:06:04,040
 It is reacting to things as being positive.

153
00:06:04,040 --> 00:06:06,680
 Reacting to things as being negative is no good, but

154
00:06:06,680 --> 00:06:08,560
 reacting to things as being positive

155
00:06:08,560 --> 00:06:11,800
 is also not good, as it also leads to the cultivation of

156
00:06:11,800 --> 00:06:13,720
 habitual tendencies that will

157
00:06:13,720 --> 00:06:16,930
 create further expectation, clinging, craving, and

158
00:06:16,930 --> 00:06:19,520
 disappointment, depression, sadness, and

159
00:06:19,520 --> 00:06:23,410
 dissatisfaction when you are not able to obtain the objects

160
00:06:23,410 --> 00:06:24,680
 of your desire.

161
00:06:24,680 --> 00:06:27,500
 What we practice is called the middle way, neither tort

162
00:06:27,500 --> 00:06:29,080
uring ourselves nor indulging

163
00:06:29,080 --> 00:06:31,360
 in sensual pleasure.

164
00:06:31,360 --> 00:06:34,110
 Once you have experienced lust and desire, you have

165
00:06:34,110 --> 00:06:35,120
 experienced it.

166
00:06:35,120 --> 00:06:38,320
 You cannot change that in retrospect, and yet that is what

167
00:06:38,320 --> 00:06:39,880
 people often try to do, get

168
00:06:39,880 --> 00:06:42,770
 upset about it and then get the idea that they are rep

169
00:06:42,770 --> 00:06:43,640
ressing it.

170
00:06:43,640 --> 00:06:46,300
 You cannot actually repress feelings, you just react to

171
00:06:46,300 --> 00:06:47,960
 them negatively, which prevents

172
00:06:47,960 --> 00:06:51,650
 completely any learning that might have come from the

173
00:06:51,650 --> 00:06:51,880
 experience.

174
00:06:51,880 --> 00:06:54,400
 At the same time, when you are attracted to someone, it can

175
00:06:54,400 --> 00:06:56,000
 be the case where she is married

176
00:06:56,000 --> 00:06:59,140
 and you feel guilty because she is untouchable or something

177
00:06:59,140 --> 00:06:59,200
.

178
00:06:59,200 --> 00:07:01,080
 It could be that you are married or you are in a

179
00:07:01,080 --> 00:07:03,000
 relationship with someone else, but you

180
00:07:03,000 --> 00:07:06,120
 have to accept that you have these feelings.

181
00:07:06,120 --> 00:07:09,780
 The Buddha said, "There is no fire like lust."

182
00:07:09,780 --> 00:07:13,460
 You cannot say to a great raging fire, "Let it only stay

183
00:07:13,460 --> 00:07:14,160
 here."

184
00:07:14,160 --> 00:07:17,250
 Similarly, you cannot say, "Let me only have lust for my

185
00:07:17,250 --> 00:07:18,860
 wife or my girlfriend and not

186
00:07:18,860 --> 00:07:21,480
 have lust for this person or that person."

187
00:07:21,480 --> 00:07:25,400
 Where there is the potential for lust to arise, it will

188
00:07:25,400 --> 00:07:27,520
 arise and it is uncontrollable.

189
00:07:27,520 --> 00:07:30,320
 Lust and desire in general are very dangerous.

190
00:07:30,320 --> 00:07:33,150
 They are habit-forming and lead only to greater and greater

191
00:07:33,150 --> 00:07:34,840
 emotions of the same type, which

192
00:07:34,840 --> 00:07:38,280
 is uncontrollable and cannot be limited in any way, shape

193
00:07:38,280 --> 00:07:39,840
 or form to one specific set

194
00:07:39,840 --> 00:07:41,840
 of experiences.

195
00:07:41,840 --> 00:07:44,660
 Lust moves from object to object, just like how fire burns

196
00:07:44,660 --> 00:07:46,120
 from the grass to the bushes

197
00:07:46,120 --> 00:07:47,480
 to the trees.

198
00:07:47,480 --> 00:07:49,840
 Whatever is combustible will trigger fire.

199
00:07:49,840 --> 00:07:52,360
 This is why men cheat on their wives and women cheat on

200
00:07:52,360 --> 00:07:53,400
 their husbands.

201
00:07:53,400 --> 00:07:57,070
 It is why we steal and manipulate others, even why we go to

202
00:07:57,070 --> 00:07:57,600
 war.

203
00:07:57,600 --> 00:07:59,520
 It is something that you cannot contain.

204
00:07:59,520 --> 00:08:02,760
 You have to accept that fact and work to undo the habits.

205
00:08:02,760 --> 00:08:06,120
 That is really the only way to deal with this.

206
00:08:06,120 --> 00:08:09,030
 Start by limiting your interaction with this person to what

207
00:08:09,030 --> 00:08:09,800
 is natural.

208
00:08:09,800 --> 00:08:12,520
 When you know that she is there, you might not want to look

209
00:08:12,520 --> 00:08:13,560
 in the beginning.

210
00:08:13,560 --> 00:08:16,260
 You might just avert your eyes when you are not talking to

211
00:08:16,260 --> 00:08:16,680
 her.

212
00:08:16,680 --> 00:08:20,690
 Just be mindful of the yearning and agony of unrequited

213
00:08:20,690 --> 00:08:21,800
 affection.

214
00:08:21,800 --> 00:08:24,830
 The yearning and agony that comes from desire is actually

215
00:08:24,830 --> 00:08:26,680
 an intense amount of suffering.

216
00:08:26,680 --> 00:08:29,560
 It can ruin both your physical and mental health.

217
00:08:29,560 --> 00:08:33,200
 It is quite valid practice to avoid and limit your contact.

218
00:08:33,200 --> 00:08:35,980
 If something is dangerous, where you know you just cannot

219
00:08:35,980 --> 00:08:37,080
 control yourself, then you

220
00:08:37,080 --> 00:08:38,840
 should avoid that thing.

221
00:08:38,840 --> 00:08:42,690
 Mental corruptions that should be removed by avoidance are

222
00:08:42,690 --> 00:08:44,680
 called "Vinodhana Pahataba"

223
00:08:44,680 --> 00:08:51,820
 and those which are to be removed by guarding are called "

224
00:08:51,820 --> 00:08:57,880
Samvara Pahataba".

