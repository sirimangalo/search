1
00:00:00,000 --> 00:00:06,000
 Hello, good afternoon.

2
00:00:06,000 --> 00:00:13,200
 So this is going to be the beginning of restarting my Q&A

3
00:00:13,200 --> 00:00:16,120
 videos.

4
00:00:16,120 --> 00:00:19,100
 Today I'm going to be answering one of the questions from

5
00:00:19,100 --> 00:00:23,200
 our meditation site.

6
00:00:23,200 --> 00:00:31,960
 And the question today is, what is the validity of short

7
00:00:31,960 --> 00:00:35,920
 meditation sessions?

8
00:00:35,920 --> 00:00:45,420
 So the question in question here is whether doing a 10

9
00:00:45,420 --> 00:00:48,400
 minute, it's a specific example,

10
00:00:48,400 --> 00:00:57,520
 the answer will try to be more general, but specifically

11
00:00:57,520 --> 00:00:59,520
 whether doing 10 minutes of meditation

12
00:00:59,520 --> 00:01:04,320
 and then 4 minutes break in sequence.

13
00:01:04,320 --> 00:01:08,770
 So 10 minutes meditation, 4 minutes break, 10 minutes

14
00:01:08,770 --> 00:01:11,240
 meditation, 4 minutes break, whether

15
00:01:11,240 --> 00:01:14,320
 that's acceptable.

16
00:01:14,320 --> 00:01:27,570
 And so generally question becomes, how long should your

17
00:01:27,570 --> 00:01:34,000
 meditation sessions be?

18
00:01:34,000 --> 00:01:42,790
 The first thing to talk about then is the importance of a

19
00:01:42,790 --> 00:01:51,120
 meditation session at all.

20
00:01:51,120 --> 00:01:59,000
 Meditation isn't something that involves minutes or hours

21
00:01:59,000 --> 00:02:00,480
 or days.

22
00:02:00,480 --> 00:02:06,760
 Meditation as I've said, involves moments.

23
00:02:06,760 --> 00:02:13,660
 Because our experiences are momentary, seeing lasts a

24
00:02:13,660 --> 00:02:18,600
 moment, hearing lasts a moment, pain

25
00:02:18,600 --> 00:02:22,760
 is momentary, I mean the experience of it is.

26
00:02:22,760 --> 00:02:26,120
 As a result, and as you can fairly easily see with a little

27
00:02:26,120 --> 00:02:27,760
 bit of meditation practice

28
00:02:27,760 --> 00:02:32,990
 or even without it, our reactions to experiences are also

29
00:02:32,990 --> 00:02:34,360
 momentary.

30
00:02:34,360 --> 00:02:38,460
 It's only a moment, it only takes a moment to get angry

31
00:02:38,460 --> 00:02:40,120
 about something.

32
00:02:40,120 --> 00:02:43,360
 The anger appears quite quickly.

33
00:02:43,360 --> 00:02:46,990
 You don't have to spend time saying, "Okay, let's get ready

34
00:02:46,990 --> 00:02:48,240
 to be angry now."

35
00:02:48,240 --> 00:02:52,720
 Here it comes, okay, all together now, anger.

36
00:02:52,720 --> 00:02:59,280
 The reaction is momentary.

37
00:02:59,280 --> 00:03:03,230
 As a result, the habit forming occurs based on these

38
00:03:03,230 --> 00:03:06,720
 moments and changing our habits,

39
00:03:06,720 --> 00:03:11,730
 or learning even about our habits as a start, involves the

40
00:03:11,730 --> 00:03:14,680
 observation of these moments.

41
00:03:14,680 --> 00:03:18,200
 Any moment that you can do that is great.

42
00:03:18,200 --> 00:03:23,730
 So for this reason, and also because this process of

43
00:03:23,730 --> 00:03:27,720
 observation can take place no matter

44
00:03:27,720 --> 00:03:31,550
 what you're doing, a so-called session of meditation where

45
00:03:31,550 --> 00:03:34,800
 you're actually sitting still

46
00:03:34,800 --> 00:03:38,400
 isn't necessary at all.

47
00:03:38,400 --> 00:03:42,120
 And if that's what you mean, then technically speaking

48
00:03:42,120 --> 00:03:44,400
 there's no reason to have any sort

49
00:03:44,400 --> 00:03:50,280
 of rules about times of meditation sessions.

50
00:03:50,280 --> 00:03:53,260
 So if you say 10 minutes of meditation and then 4 minutes

51
00:03:53,260 --> 00:03:54,920
 of break, I would say, "No

52
00:03:54,920 --> 00:03:55,920
 breaks."

53
00:03:55,920 --> 00:03:59,360
 The only breaks should be when you're asleep because you

54
00:03:59,360 --> 00:04:01,960
 can't really very easily be mindful

55
00:04:01,960 --> 00:04:04,080
 when you're sleeping.

56
00:04:04,080 --> 00:04:07,360
 No breaks would be ideal.

57
00:04:07,360 --> 00:04:11,990
 And there's really no excuse for a meditator to take a

58
00:04:11,990 --> 00:04:14,080
 break in that sense.

59
00:04:14,080 --> 00:04:16,820
 And for people living in the world, they're going to take

60
00:04:16,820 --> 00:04:18,480
 breaks because they have other

61
00:04:18,480 --> 00:04:20,560
 things to do.

62
00:04:20,560 --> 00:04:23,000
 They want to learn something.

63
00:04:23,000 --> 00:04:24,000
 They have to read a book.

64
00:04:24,000 --> 00:04:28,480
 If you're reading the book, you're not meditating.

65
00:04:28,480 --> 00:04:29,920
 Not consistently.

66
00:04:29,920 --> 00:04:31,920
 Not constantly.

67
00:04:31,920 --> 00:04:38,780
 Because some of your time is engaged in other sorts of

68
00:04:38,780 --> 00:04:41,520
 mental activity.

69
00:04:41,520 --> 00:04:43,120
 But that's not really what's being asked here.

70
00:04:43,120 --> 00:04:44,120
 It's an important point.

71
00:04:44,120 --> 00:04:47,650
 It's important that we understand that and understand that

72
00:04:47,650 --> 00:04:49,560
 all of our rules and guidelines

73
00:04:49,560 --> 00:04:52,040
 and schedules are all convention.

74
00:04:52,040 --> 00:04:55,180
 And they don't get at the underlying principle of

75
00:04:55,180 --> 00:04:57,560
 meditation being about moments.

76
00:04:57,560 --> 00:05:00,660
 You can meditate for hours and say, "I did hours of

77
00:05:00,660 --> 00:05:02,840
 meditation days, weeks, months, years,

78
00:05:02,840 --> 00:05:04,560
 and get nothing out of it."

79
00:05:04,560 --> 00:05:08,900
 Or never become enlightened, for sure, simply because you

80
00:05:08,900 --> 00:05:10,920
 never really meditated.

81
00:05:10,920 --> 00:05:18,400
 You never really saw things clearly.

82
00:05:18,400 --> 00:05:23,920
 So then practically speaking, what's the importance of...

83
00:05:23,920 --> 00:05:25,920
 is there an importance of sitting still

84
00:05:25,920 --> 00:05:27,160
 for 10 minutes?

85
00:05:27,160 --> 00:05:31,300
 So the first thing I would say is that doing only sitting

86
00:05:31,300 --> 00:05:34,200
 meditation all day is problematic.

87
00:05:34,200 --> 00:05:36,650
 Sitting all day is something we become accustomed to in

88
00:05:36,650 --> 00:05:39,920
 modern times, but it's not very healthy.

89
00:05:39,920 --> 00:05:43,480
 It's not very natural.

90
00:05:43,480 --> 00:05:45,320
 And it's not optimal.

91
00:05:45,320 --> 00:05:49,320
 It's not optimal.

92
00:05:49,320 --> 00:05:52,770
 Walking meditation has... or walking movement has benefits

93
00:05:52,770 --> 00:05:54,680
 psychologically as well, not

94
00:05:54,680 --> 00:05:56,520
 just physically.

95
00:05:56,520 --> 00:06:00,640
 There's an energy that's created through moving meditation,

96
00:06:00,640 --> 00:06:03,360
 which is why we do walking meditation.

97
00:06:03,360 --> 00:06:06,180
 So the first thing I would recommend is the first thing to

98
00:06:06,180 --> 00:06:07,720
 switch would be to add 10 minutes

99
00:06:07,720 --> 00:06:08,720
 of walking first.

100
00:06:08,720 --> 00:06:11,580
 Do 10 minutes of walking and then 10 minutes of sitting and

101
00:06:11,580 --> 00:06:12,760
 then a 4 minute break.

102
00:06:12,760 --> 00:06:13,760
 Is that good?

103
00:06:13,760 --> 00:06:15,920
 So that's the next question.

104
00:06:15,920 --> 00:06:17,680
 Is that good?

105
00:06:17,680 --> 00:06:20,120
 The other problem about short meditations, and this really

106
00:06:20,120 --> 00:06:21,220
 I think gets to the heart

107
00:06:21,220 --> 00:06:27,720
 of this question, they're great to start with.

108
00:06:27,720 --> 00:06:34,410
 If anyone can do... just starting to meditate can do 10

109
00:06:34,410 --> 00:06:35,720
 minutes of walking and 10 minutes

110
00:06:35,720 --> 00:06:36,720
 of sitting.

111
00:06:36,720 --> 00:06:37,720
 Wow.

112
00:06:37,720 --> 00:06:38,720
 Congratulations.

113
00:06:38,720 --> 00:06:41,200
 You've done something very special.

114
00:06:41,200 --> 00:06:43,660
 The Buddha said even one moment of meditation is very

115
00:06:43,660 --> 00:06:44,280
 special.

116
00:06:44,280 --> 00:06:45,800
 Ten minutes of it?

117
00:06:45,800 --> 00:06:50,040
 Good for you.

118
00:06:50,040 --> 00:06:53,590
 But once you've become... once you've gone beyond that,

119
00:06:53,590 --> 00:06:56,120
 there are limitations to such

120
00:06:56,120 --> 00:06:59,600
 a practice of only 10 minutes.

121
00:06:59,600 --> 00:07:06,590
 Mindfulness at its very core is about facing, facing your

122
00:07:06,590 --> 00:07:10,800
 problems, but not just problems,

123
00:07:10,800 --> 00:07:14,080
 facing everything, facing reality.

124
00:07:14,080 --> 00:07:19,040
 Because we run away from it, we react to it, we forget it.

125
00:07:19,040 --> 00:07:22,360
 And sati, which means to remember it.

126
00:07:22,360 --> 00:07:31,370
 But sati has one of its important qualities is... I can't

127
00:07:31,370 --> 00:07:35,240
 remember the word now.

128
00:07:35,240 --> 00:07:38,480
 To face, it means to face.

129
00:07:38,480 --> 00:07:45,480
 To face head on.

130
00:07:45,480 --> 00:07:49,340
 Your experiences, rather than running away, being afraid of

131
00:07:49,340 --> 00:07:51,040
 them, or reacting to them

132
00:07:51,040 --> 00:07:52,040
 in any way.

133
00:07:52,040 --> 00:07:53,040
 Being with them.

134
00:07:53,040 --> 00:07:54,040
 Patience, right?

135
00:07:54,040 --> 00:07:59,080
 Patience is one way of describing the quality of an insight

136
00:07:59,080 --> 00:08:00,400
 meditator.

137
00:08:00,400 --> 00:08:07,360
 Someone who has... it's called anulomika kanti.

138
00:08:07,360 --> 00:08:08,760
 Anuloma means...

139
00:08:08,760 --> 00:08:13,380
 Anuloma is when you finally get things, when you're going

140
00:08:13,380 --> 00:08:14,880
 with the grain.

141
00:08:14,880 --> 00:08:15,880
 Anuloma means with the grain.

142
00:08:15,880 --> 00:08:20,680
 And you finally are... you get it.

143
00:08:20,680 --> 00:08:23,960
 Patience is the... kanti means patience.

144
00:08:23,960 --> 00:08:25,840
 Anulomika kanti means patience.

145
00:08:25,840 --> 00:08:26,840
 That gets it.

146
00:08:26,840 --> 00:08:33,080
 You're no longer impatient because you get it.

147
00:08:33,080 --> 00:08:35,000
 You see things as they are.

148
00:08:35,000 --> 00:08:38,840
 You see things as they are, there's no stress.

149
00:08:38,840 --> 00:08:43,300
 Ten minutes of meditation, first of all, doesn't let the

150
00:08:43,300 --> 00:08:46,640
 average meditator face their problems,

151
00:08:46,640 --> 00:08:48,600
 face reality.

152
00:08:48,600 --> 00:08:52,460
 After ten minutes, things are just starting to get

153
00:08:52,460 --> 00:08:55,400
 interesting, to put it in colloquial

154
00:08:55,400 --> 00:08:58,680
 terms.

155
00:08:58,680 --> 00:09:04,480
 You're just starting to observe challenging mind states.

156
00:09:04,480 --> 00:09:06,640
 Boredom would be a good one.

157
00:09:06,640 --> 00:09:09,640
 Pain will start to come up.

158
00:09:09,640 --> 00:09:15,360
 Wants, desires will come up.

159
00:09:15,360 --> 00:09:19,050
 Ten minutes is fine to help you understand reality because

160
00:09:19,050 --> 00:09:20,640
 reality is all there.

161
00:09:20,640 --> 00:09:24,320
 But for the average meditator, everything is too... all

162
00:09:24,320 --> 00:09:27,000
 their mind states are still too

163
00:09:27,000 --> 00:09:29,000
 subtle.

164
00:09:29,000 --> 00:09:34,160
 So for an average meditator, conditions are going to build.

165
00:09:34,160 --> 00:09:36,640
 And after an hour, it can get quite difficult.

166
00:09:36,640 --> 00:09:40,800
 And you'll have to face quite difficult conditions.

167
00:09:40,800 --> 00:09:42,800
 We consider an hour to be about good.

168
00:09:42,800 --> 00:09:47,540
 You don't have to go beyond an hour, though sometimes medit

169
00:09:47,540 --> 00:09:48,560
ators do.

170
00:09:48,560 --> 00:09:52,000
 But it's just an artificial limit that we tend to set.

171
00:09:52,000 --> 00:09:55,030
 We say, "Well, do maximum one hour walking, one hour

172
00:09:55,030 --> 00:09:55,840
 sitting."

173
00:09:55,840 --> 00:09:58,640
 That's what we try to get our meditators up to.

174
00:09:58,640 --> 00:10:04,330
 Again, there's no compelling reason to stick to one hour

175
00:10:04,330 --> 00:10:07,400
 beyond what seems about right.

176
00:10:07,400 --> 00:10:12,480
 And it's convenient because you can count hours and so on.

177
00:10:12,480 --> 00:10:16,240
 But most important is it allows you to face your problems.

178
00:10:16,240 --> 00:10:19,360
 It allows you to face your problems and it allows subtle

179
00:10:19,360 --> 00:10:21,260
 problems to become more evident

180
00:10:21,260 --> 00:10:22,840
 because they'll build.

181
00:10:22,840 --> 00:10:25,290
 If you're not mindful of something, suppose during those

182
00:10:25,290 --> 00:10:26,840
 ten minutes something is happening

183
00:10:26,840 --> 00:10:29,640
 like you're getting bored.

184
00:10:29,640 --> 00:10:32,240
 But it's too subtle and you don't catch it.

185
00:10:32,240 --> 00:10:36,160
 You're not mindful of it because you're not aware of it.

186
00:10:36,160 --> 00:10:38,590
 Well after ten minutes, if you continue sitting, it'll

187
00:10:38,590 --> 00:10:39,720
 become more apparent.

188
00:10:39,720 --> 00:10:44,800
 It'll get more severe and you'll be able to note it.

189
00:10:44,800 --> 00:10:47,760
 So it's profitable in that way.

190
00:10:47,760 --> 00:10:51,400
 Practically speaking, longer meditation sessions are more

191
00:10:51,400 --> 00:10:52,320
 profitable.

192
00:10:52,320 --> 00:10:56,680
 That being said, there's another related question.

193
00:10:56,680 --> 00:11:00,970
 It's the question of whether I should do one big meditation

194
00:11:00,970 --> 00:11:03,480
 session once a day or for example

195
00:11:03,480 --> 00:11:07,910
 split it in half and do one in the morning and one in the

196
00:11:07,910 --> 00:11:09,040
 evening.

197
00:11:09,040 --> 00:11:12,080
 And I usually tell meditators again, "Keeping in mind this

198
00:11:12,080 --> 00:11:12,960
 isn't magic.

199
00:11:12,960 --> 00:11:14,560
 I don't have any rule book."

200
00:11:14,560 --> 00:11:16,960
 Or I say, "If you do this, you'll become enlightened.

201
00:11:16,960 --> 00:11:20,000
 If you do that, you won't become enlightened."

202
00:11:20,000 --> 00:11:23,330
 It's important that we rise above magical thinking that

203
00:11:23,330 --> 00:11:25,840
 somehow there's some magic way,

204
00:11:25,840 --> 00:11:28,560
 formula that I can use that's going to get me enlightened.

205
00:11:28,560 --> 00:11:31,360
 We have to be smarter than that.

206
00:11:31,360 --> 00:11:33,800
 Meditation is like war.

207
00:11:33,800 --> 00:11:37,960
 It's like a battlefield.

208
00:11:37,960 --> 00:11:40,090
 When you've done all this training, I don't know how they

209
00:11:40,090 --> 00:11:41,640
 teach soldiers, but I would imagine

210
00:11:41,640 --> 00:11:43,230
 they would tell them, "You're doing all this training and

211
00:11:43,230 --> 00:11:44,400
 it's not going to mean crap when

212
00:11:44,400 --> 00:11:46,300
 you're out on the field because everything's going to go

213
00:11:46,300 --> 00:11:46,680
 crazy."

214
00:11:46,680 --> 00:11:48,760
 I imagine that's how it is.

215
00:11:48,760 --> 00:11:51,900
 I can imagine a battlefield being just all your training

216
00:11:51,900 --> 00:11:53,440
 goes out the window and you

217
00:11:53,440 --> 00:11:56,840
 try to use what you can and use it as best you can.

218
00:11:56,840 --> 00:11:59,080
 And that's kind of how meditation works.

219
00:11:59,080 --> 00:12:01,160
 It's messy.

220
00:12:01,160 --> 00:12:04,430
 And you try to do your best to learn about your mind and

221
00:12:04,430 --> 00:12:06,360
 learn about reality using the

222
00:12:06,360 --> 00:12:08,640
 tools that you've been given.

223
00:12:08,640 --> 00:12:12,160
 One thing you should never mess with is the technique.

224
00:12:12,160 --> 00:12:16,050
 But the technique is you can adapt it to any situation and

225
00:12:16,050 --> 00:12:18,160
 it doesn't involve minutes or

226
00:12:18,160 --> 00:12:20,160
 hours or days.

227
00:12:20,160 --> 00:12:24,510
 But so this question of whether you should do two shorter

228
00:12:24,510 --> 00:12:27,080
 meditations, I recommend more

229
00:12:27,080 --> 00:12:32,250
 because the other question here, or the other aspect of

230
00:12:32,250 --> 00:12:35,920
 this question is the idea of continuity.

231
00:12:35,920 --> 00:12:39,560
 And the Buddha talked about something, or it's at least

232
00:12:39,560 --> 00:12:41,480
 described to the Buddha as talking

233
00:12:41,480 --> 00:12:48,180
 about something called satanjak, satatjakiryawasena, which

234
00:12:48,180 --> 00:12:54,760
 means staying in the same condition continuously,

235
00:12:54,760 --> 00:12:58,040
 not changing your condition.

236
00:12:58,040 --> 00:13:11,850
 To allow this accumulation of difficulty to really test you

237
00:13:11,850 --> 00:13:12,720
.

238
00:13:12,720 --> 00:13:15,600
 It's easy to sit 10 minutes and then okay because I can

239
00:13:15,600 --> 00:13:16,440
 take a break.

240
00:13:16,440 --> 00:13:18,640
 But what if you have to sit for an hour and you have to

241
00:13:18,640 --> 00:13:20,400
 really bear with things, be patient

242
00:13:20,400 --> 00:13:21,400
 with them.

243
00:13:21,400 --> 00:13:29,680
 The ability, the potential to cultivate patience is higher.

244
00:13:29,680 --> 00:13:33,320
 And so doing shorter meditations is not better because it's

245
00:13:33,320 --> 00:13:35,040
 shorter, it's better because

246
00:13:35,040 --> 00:13:38,300
 it's more often, it's more frequent, and it's more likely

247
00:13:38,300 --> 00:13:40,120
 to encourage you to be mindful

248
00:13:40,120 --> 00:13:42,960
 when you're not meditating.

249
00:13:42,960 --> 00:13:46,580
 If you do one meditation session, even a long one, it's

250
00:13:46,580 --> 00:13:48,200
 good that you did it.

251
00:13:48,200 --> 00:13:51,110
 But then there's 24 hours where you're not doing any formal

252
00:13:51,110 --> 00:13:52,760
 meditation, and so your ability

253
00:13:52,760 --> 00:13:57,080
 to be mindful during the time you're not meditating is, I

254
00:13:57,080 --> 00:13:59,960
 would say, inhibited, reduced.

255
00:13:59,960 --> 00:14:02,440
 Whereas if you do two a day, you're thinking more about

256
00:14:02,440 --> 00:14:03,240
 meditation.

257
00:14:03,240 --> 00:14:06,080
 It's more continuous.

258
00:14:06,080 --> 00:14:09,700
 And so the 12 hours between each, or however many hours

259
00:14:09,700 --> 00:14:11,960
 between each session, you're more

260
00:14:11,960 --> 00:14:13,640
 likely to be mindful.

261
00:14:13,640 --> 00:14:18,330
 So doing more sessions is good, but doing lots of little

262
00:14:18,330 --> 00:14:20,800
 sessions gets problematic for the

263
00:14:20,800 --> 00:14:21,800
 reasons mentioned.

264
00:14:21,800 --> 00:14:24,760
 So there you go.

265
00:14:24,760 --> 00:14:27,120
 Shorter videos now, that's the idea.

266
00:14:27,120 --> 00:14:29,240
 One question, one answer per video.

267
00:14:29,240 --> 00:14:32,680
 If you want to ask questions, you can go to our meditation

268
00:14:32,680 --> 00:14:37,560
 site, meditation.siri-mongolo.org.

269
00:14:37,560 --> 00:14:39,890
 There's not going to be a, it's not going to be in the

270
00:14:39,890 --> 00:14:41,480
 description of this video, not

271
00:14:41,480 --> 00:14:46,250
 right away, I don't think, but you can find it on my

272
00:14:46,250 --> 00:14:48,360
 channel, I think.

273
00:14:48,360 --> 00:14:49,360
 I think.

274
00:14:49,360 --> 00:14:54,170
 And if you, you can, I don't know, if you can't find it,

275
00:14:54,170 --> 00:14:56,600
 well that's part of the, part of

276
00:14:56,600 --> 00:15:00,600
 your spiritual journey is to find things like this.

277
00:15:00,600 --> 00:15:03,160
 The teacher doesn't seek out the students, so good luck.

278
00:15:03,160 --> 00:15:04,840
 Wish you all the best.

279
00:15:04,840 --> 00:15:05,840
 74 viewers, wonderful.

280
00:15:05,840 --> 00:15:06,840
 Have a good day, everyone.

281
00:15:06,840 --> 00:15:16,840
 [BLANK_AUDIO]

