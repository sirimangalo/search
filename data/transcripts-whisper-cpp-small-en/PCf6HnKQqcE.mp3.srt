1
00:00:00,000 --> 00:00:05,440
 Hello and welcome back to our study of the Dhammapada.

2
00:00:05,440 --> 00:00:10,440
 Today we continue on with verse 128, which reads as follows

3
00:00:10,440 --> 00:00:11,480
.

4
00:00:11,480 --> 00:00:27,730
 Na antalike na samudamante na pambata naanang vivarang pav

5
00:00:27,730 --> 00:00:33,970
isa na vincitti so jagatthi badhe so yatakthi dang napasaay

6
00:00:33,970 --> 00:00:34,480
amacho

7
00:00:34,480 --> 00:00:40,590
 which means very similar to our last verse, not in the

8
00:00:40,590 --> 00:00:49,480
 middle, not up on a mountain, not in the middle of the sky

9
00:00:49,480 --> 00:01:07,480
 or in the middle of, not at the end of the sky, right? Ant

10
00:01:07,480 --> 00:01:07,480
alike, nor the middle of the ocean, not in a cave in the

11
00:01:07,480 --> 00:01:07,480
 mountain

12
00:01:07,480 --> 00:01:14,740
 if one plunges into the cave in the mountain, one cannot

13
00:01:14,740 --> 00:01:23,480
 find a place on earth, navi jitti so jagatthi badhe so

14
00:01:23,480 --> 00:01:39,750
 where one could stand and not be overcome by death, napasa

15
00:01:39,750 --> 00:01:42,480
ayamacho.

16
00:01:42,480 --> 00:01:47,800
 Another fairly well-known story, part of the Buddha's life,

17
00:01:47,800 --> 00:01:52,120
 and it'd be nice if there were more stories about the

18
00:01:52,120 --> 00:01:54,480
 Buddha's life.

19
00:01:54,480 --> 00:01:58,440
 I've talked about this several times how, well that's

20
00:01:58,440 --> 00:02:00,590
 basically what we're doing here, we're telling the story of

21
00:02:00,590 --> 00:02:02,480
 the Buddha's life piece by piece,

22
00:02:02,480 --> 00:02:07,150
 but I think part of why many people are fascinated by these

23
00:02:07,150 --> 00:02:11,810
 stories is because they're part of the Buddha story that

24
00:02:11,810 --> 00:02:13,480
 people have never heard.

25
00:02:13,480 --> 00:02:17,360
 And that's because the most famous Buddha stories are not

26
00:02:17,360 --> 00:02:21,300
 really about the Buddha at all, they're about the bodhisatt

27
00:02:21,300 --> 00:02:21,480
va,

28
00:02:21,480 --> 00:02:27,730
 they're about the quest to become a Buddha, but the 45

29
00:02:27,730 --> 00:02:33,480
 years where the Buddha actually taught is neglected,

30
00:02:33,480 --> 00:02:37,910
 and this is partly because of the types of Buddhism that

31
00:02:37,910 --> 00:02:40,480
 have spread and become popular,

32
00:02:40,480 --> 00:02:46,510
 but it's a shame because he did spend 45 years teaching and

33
00:02:46,510 --> 00:02:52,310
 if the stories are any indication, lots of exciting things

34
00:02:52,310 --> 00:02:53,480
 happen.

35
00:02:53,480 --> 00:02:57,710
 One of the exciting things that happened was his son, who

36
00:02:57,710 --> 00:03:02,360
 if you remember from the bodhisattva story that we all hear

37
00:03:02,360 --> 00:03:03,480
 about,

38
00:03:03,480 --> 00:03:08,420
 the Buddha left behind some version, say, in the middle of

39
00:03:08,420 --> 00:03:15,480
 the night, and his wife, who he left behind as a prince,

40
00:03:15,480 --> 00:03:25,480
 eventually became monks themselves.

41
00:03:25,480 --> 00:03:34,510
 So, Rahula became a novice monk, and Yasodara became a bhik

42
00:03:34,510 --> 00:03:35,480
khuni.

43
00:03:35,480 --> 00:03:40,830
 And lots of the Sakyans, the relatives of the Buddha, also

44
00:03:40,830 --> 00:03:42,480
 became monks.

45
00:03:42,480 --> 00:03:53,630
 There was Anuruddha, Ananda, Upali and Devadatta, and many

46
00:03:53,630 --> 00:03:55,480
 many more.

47
00:03:55,480 --> 00:03:59,930
 And they were criticized for this, but many people were

48
00:03:59,930 --> 00:04:00,480
 saying,

49
00:04:00,480 --> 00:04:05,730
 "Well, this monk is like the Pied Viper, he's leading all

50
00:04:05,730 --> 00:04:10,480
 of our sons away, he's a cult leader."

51
00:04:10,480 --> 00:04:14,860
 Of course, any community would start to get concerned if a

52
00:04:14,860 --> 00:04:19,480
 religious leader started to take away their young men.

53
00:04:19,480 --> 00:04:24,480
 In the beginning it was all young men,

54
00:04:24,480 --> 00:04:32,480
 and just kind of threatening their society.

55
00:04:32,480 --> 00:04:38,680
 So the monk came to the Buddha and asked him, and the

56
00:04:38,680 --> 00:04:41,860
 Buddha said, "Just tell them that they go forth according

57
00:04:41,860 --> 00:04:42,480
 to the Dharma."

58
00:04:42,480 --> 00:04:45,870
 Dharma was an important word for people, they wanted to

59
00:04:45,870 --> 00:04:49,480
 follow the Dharma, so it was quite clever actually.

60
00:04:49,480 --> 00:04:53,820
 And when they told people this, "These monks go forth

61
00:04:53,820 --> 00:04:55,480
 according to the Dharma,"

62
00:04:55,480 --> 00:05:01,660
 then people understood that actually this was a challenge

63
00:05:01,660 --> 00:05:02,480
 really.

64
00:05:02,480 --> 00:05:04,850
 And they had to then point out a flaw in the Buddha's

65
00:05:04,850 --> 00:05:08,480
 teaching, which of course they couldn't easily find.

66
00:05:08,480 --> 00:05:13,080
 Anyway, that's not the story today, today's story, but the

67
00:05:13,080 --> 00:05:14,480
 background is there.

68
00:05:14,480 --> 00:05:21,600
 That Yasodara's father, Supa Buddha, that's who the story

69
00:05:21,600 --> 00:05:23,480
 is about.

70
00:05:23,480 --> 00:05:29,320
 He was less than thrilled that his daughter and his

71
00:05:29,320 --> 00:05:34,480
 grandson renounced the world, and he was very angry.

72
00:05:34,480 --> 00:05:41,480
 He was quite awful about it.

73
00:05:41,480 --> 00:05:50,820
 And so when the Buddha was visiting his family, when he

74
00:05:50,820 --> 00:05:52,480
 tried to go for alms with the monks,

75
00:05:52,480 --> 00:05:58,050
 Supa Buddha set up a seat in the middle of the road, in the

76
00:05:58,050 --> 00:06:01,480
 middle of the street, between houses,

77
00:06:01,480 --> 00:06:04,480
 and started drinking alcohol, and I guess with his friends,

78
00:06:04,480 --> 00:06:07,480
 or just set up sort of a party.

79
00:06:07,480 --> 00:06:10,300
 And when the monks came, he refused to move, standing right

80
00:06:10,300 --> 00:06:11,480
 in the middle of the road,

81
00:06:11,480 --> 00:06:22,480
 and they couldn't go for alms. Somehow he got in their way.

82
00:06:22,480 --> 00:06:28,820
 And they said to him, "Hey, the Buddha has come. What's

83
00:06:28,820 --> 00:06:29,480
 going on?"

84
00:06:29,480 --> 00:06:32,580
 And he says, "Tell him to go his way. I'm older than him. I

85
00:06:32,580 --> 00:06:35,480
'm not going to make way for him."

86
00:06:35,480 --> 00:06:37,600
 And he said, "I wouldn't get out of his way." And the

87
00:06:37,600 --> 00:06:39,800
 Buddha just turned around and walked away, made no

88
00:06:39,800 --> 00:06:42,480
 complaint.

89
00:06:42,480 --> 00:06:50,480
 This went back to the monastery, or went another way.

90
00:06:50,480 --> 00:06:55,780
 And Supa Buddha sent someone out, one of his spies, to go

91
00:06:55,780 --> 00:06:56,480
 and see.

92
00:06:56,480 --> 00:06:59,260
 He said, "I'll go and see what the Buddha says, because he

93
00:06:59,260 --> 00:07:00,480
's got more in store.

94
00:07:00,480 --> 00:07:05,550
 This wasn't all he had planned. He's got more plans than

95
00:07:05,550 --> 00:07:06,480
 this."

96
00:07:06,480 --> 00:07:11,230
 So he says, "Go and spy on the Buddha, see what he says

97
00:07:11,230 --> 00:07:13,480
 about this, because we're going to use it against him."

98
00:07:13,480 --> 00:07:19,480
 And when the Buddha was on his way back, he smiled.

99
00:07:19,480 --> 00:07:22,630
 And there are several stories where the Buddha smiled, and

100
00:07:22,630 --> 00:07:25,480
 the Buddha smiling is not like an ordinary person smiling.

101
00:07:25,480 --> 00:07:31,060
 It means something. And so Ananda noticed that he was

102
00:07:31,060 --> 00:07:35,480
 smiling and said, "Reverend Sir, why are you smiling?"

103
00:07:35,480 --> 00:07:40,160
 It's kind of perverse. I want to say that smiling of a

104
00:07:40,160 --> 00:07:44,480
 Buddha is not the smile of an ordinary being.

105
00:07:44,480 --> 00:07:52,790
 They smile at extraordinary things when something is

106
00:07:52,790 --> 00:08:00,380
 extreme, because it's out of the ordinary, often in a bad

107
00:08:00,380 --> 00:08:01,480
 way.

108
00:08:01,480 --> 00:08:08,360
 And he says, Ananda, the Supa Buddha, he's committed a

109
00:08:08,360 --> 00:08:10,480
 grievous sin.

110
00:08:10,480 --> 00:08:14,210
 He's done something very, very terrible, very bad karma for

111
00:08:14,210 --> 00:08:16,480
 him, to block a Buddha,

112
00:08:16,480 --> 00:08:20,210
 blocking those people who wanted to give alms to the Buddha

113
00:08:20,210 --> 00:08:20,480
.

114
00:08:20,480 --> 00:08:27,480
 Buddha is a fairly special being. And he made a prediction.

115
00:08:27,480 --> 00:08:30,180
 And I'm going to gloss over some of this, because a lot of

116
00:08:30,180 --> 00:08:32,480
 these stories, I believe, are exaggerated.

117
00:08:32,480 --> 00:08:38,130
 That might put me in bad graces with some people, but I'm

118
00:08:38,130 --> 00:08:41,480
 going to, as I've done before,

119
00:08:41,480 --> 00:08:46,220
 I'm going to exaggerate some of the aspects, at least talk

120
00:08:46,220 --> 00:08:47,480
 about them.

121
00:08:47,480 --> 00:08:52,170
 But basically he says he's going to die and go to hell in

122
00:08:52,170 --> 00:08:53,480
 seven days.

123
00:08:53,480 --> 00:08:57,840
 But he makes a prediction. He says he's going to fall down

124
00:08:57,840 --> 00:09:00,480
 the stairs in his palace.

125
00:09:00,480 --> 00:09:09,040
 And he's going to die at the bottom of the steps of his

126
00:09:09,040 --> 00:09:17,020
 seven-story palace, in seven days, within seven days or

127
00:09:17,020 --> 00:09:22,480
 something like that.

128
00:09:22,480 --> 00:09:30,620
 And so the spy hears this and goes back to Supa Buddha and

129
00:09:30,620 --> 00:09:32,480
 tells him, and Supa Buddha, he says, well, you know,

130
00:09:32,480 --> 00:09:38,470
 what Buddha's say is going to happen, is going to happen,

131
00:09:38,470 --> 00:09:43,480
 but he didn't say he's going to die in seven days.

132
00:09:43,480 --> 00:09:46,480
 He said, at that bottom of the stairs he's going to die.

133
00:09:46,480 --> 00:09:51,620
 So if I don't go to the bottom of the stairs, if I don't go

134
00:09:51,620 --> 00:09:55,480
 down those stairs, it won't happen.

135
00:09:55,480 --> 00:09:59,160
 And he said, I'm going to, so I'm going to, what I'm going

136
00:09:59,160 --> 00:10:02,540
 to do is going to make it impossible for me to go down

137
00:10:02,540 --> 00:10:03,480
 those stairs.

138
00:10:03,480 --> 00:10:10,480
 And I'm going to show that the Buddha is a liar.

139
00:10:10,480 --> 00:10:16,480
 And so he orders his men to lock up, to remove the stairs.

140
00:10:16,480 --> 00:10:18,810
 I guess he had another set of stairs or something, but he

141
00:10:18,810 --> 00:10:25,480
 removed these main stairs, seven floors of them, apparently

142
00:10:25,480 --> 00:10:25,480
,

143
00:10:25,480 --> 00:10:33,180
 and bar up all the doors and put guards at each of the

144
00:10:33,180 --> 00:10:37,480
 levels to stop him from going through the doors

145
00:10:37,480 --> 00:10:43,770
 where the stairs used to be once they were removed, making

146
00:10:43,770 --> 00:10:44,480
 it impossible.

147
00:10:44,480 --> 00:10:47,750
 And he goes and he lives up on the top level and he makes

148
00:10:47,750 --> 00:10:50,480
 sure that he doesn't go down these stairs.

149
00:10:50,480 --> 00:10:54,820
 He said, he tells his guards, if ever I am even thinking

150
00:10:54,820 --> 00:11:02,480
 about going through those doors, he stopped me.

151
00:11:02,480 --> 00:11:07,480
 He tells the guards to grab him and stop him.

152
00:11:07,480 --> 00:11:15,530
 And the teacher, the Buddha hears about this and he says,

153
00:11:15,530 --> 00:11:17,480
 this is the quote, he says,

154
00:11:17,480 --> 00:11:22,500
 "Let him not be, let not he, Supa Buddha, be content with

155
00:11:22,500 --> 00:11:26,480
 ascending to the topmost floor of his palace.

156
00:11:26,480 --> 00:11:29,480
 Let him soar aloft and sit in the air."

157
00:11:29,480 --> 00:11:35,480
 So even if he doesn't stay at the top and never come down,

158
00:11:35,480 --> 00:11:37,480
 let him fly through the air,

159
00:11:37,480 --> 00:11:41,720
 let him put to sea in a boat, or let him enter to the bow

160
00:11:41,720 --> 00:11:43,480
els of a mountain.

161
00:11:43,480 --> 00:11:45,720
 There is no equivocation in the words of the Buddha. He

162
00:11:45,720 --> 00:11:48,480
 will enter, he will die.

163
00:11:48,480 --> 00:11:52,290
 He will enter into the earth, meaning he will pass away

164
00:11:52,290 --> 00:11:54,480
 right where I said he would.

165
00:11:54,480 --> 00:11:57,560
 I mean the actual story is that the earth opened up and

166
00:11:57,560 --> 00:12:00,480
 swallowed him and he was born in hell.

167
00:12:00,480 --> 00:12:02,680
 That happened to five different people I think in the

168
00:12:02,680 --> 00:12:03,480
 Buddhist time.

169
00:12:03,480 --> 00:12:05,480
 Dibhadatta was one of them.

170
00:12:05,480 --> 00:12:08,670
 Not convinced that it actually happens, but that's what the

171
00:12:08,670 --> 00:12:11,480
 story says. It may have happened.

172
00:12:11,480 --> 00:12:17,480
 It's just kind of incredible. Hard to believe.

173
00:12:17,480 --> 00:12:21,480
 And then he tells this verse.

174
00:12:21,480 --> 00:12:24,630
 And the verse isn't, the verse is telling something

175
00:12:24,630 --> 00:12:25,480
 different.

176
00:12:25,480 --> 00:12:30,480
 It's saying that you can't avoid death.

177
00:12:30,480 --> 00:12:34,910
 But it fits in because it's not only that you can't avoid

178
00:12:34,910 --> 00:12:37,440
 death, it's that you can't avoid in general the

179
00:12:37,440 --> 00:12:39,480
 consequences of your deeds.

180
00:12:39,480 --> 00:12:42,480
 I mean stopping a spiritual teacher like the Buddha,

181
00:12:42,480 --> 00:12:47,100
 someone so pure and so good and so wise, getting in their

182
00:12:47,100 --> 00:12:47,480
 way.

183
00:12:47,480 --> 00:12:51,480
 I mean if you read the text it's a bad thing.

184
00:12:51,480 --> 00:12:59,370
 But emotionally or intuitively you can understand how it

185
00:12:59,370 --> 00:13:01,480
 would be a bad thing to do.

186
00:13:01,480 --> 00:13:05,480
 Pretty bad karma. I mean karma in Buddhism is like that.

187
00:13:05,480 --> 00:13:10,060
 If you hurt an evil person it's not as bad as if you hurt a

188
00:13:10,060 --> 00:13:11,480
 pure person.

189
00:13:11,480 --> 00:13:16,480
 Hurting someone who is pure is far worse.

190
00:13:16,480 --> 00:13:19,390
 Hurting someone who doesn't deserve it, but more over

191
00:13:19,390 --> 00:13:23,020
 hurting someone who is pure, who is good, who is

192
00:13:23,020 --> 00:13:25,480
 spiritually enlightened.

193
00:13:25,480 --> 00:13:29,480
 The Buddha is just the pinnacle of that.

194
00:13:29,480 --> 00:13:31,820
 So that's it for the verse, but then it tells the end of

195
00:13:31,820 --> 00:13:35,480
 the story kind of as an afterthought and it just stops.

196
00:13:35,480 --> 00:13:38,480
 It just tells the story and doesn't explain it at all.

197
00:13:38,480 --> 00:13:45,680
 Because in the middle of the night his horse, one of his

198
00:13:45,680 --> 00:13:53,720
 expensive horses broke loose and was running around the

199
00:13:53,720 --> 00:13:55,480
 bottom floor of the palace.

200
00:13:55,480 --> 00:13:59,440
 I guess that was an open area where the stables were or

201
00:13:59,440 --> 00:14:00,480
 whatever.

202
00:14:00,480 --> 00:14:06,660
 So he heard this ruckus and he asked them what's going on

203
00:14:06,660 --> 00:14:10,480
 and they said, "Oh, you're a horse."

204
00:14:10,480 --> 00:14:16,790
 So he wanted to catch him. He stood up and started towards

205
00:14:16,790 --> 00:14:18,480
 the stairs.

206
00:14:18,480 --> 00:14:20,480
 I guess he didn't have another set of stairs.

207
00:14:20,480 --> 00:14:24,080
 The idea was he was just going to not come down for seven

208
00:14:24,080 --> 00:14:24,480
 days.

209
00:14:24,480 --> 00:14:28,480
 At the end of seven days he would open the door.

210
00:14:28,480 --> 00:14:32,480
 But he forgot, of course, and he got to the door.

211
00:14:32,480 --> 00:14:35,580
 But here's where the funny thing goes, because he was

212
00:14:35,580 --> 00:14:38,480
 supposed to have these guards watching.

213
00:14:38,480 --> 00:14:43,920
 But when he gets to the doors, it says things like the

214
00:14:43,920 --> 00:14:47,480
 stairs appear and the doors open of their own accord.

215
00:14:47,480 --> 00:14:51,480
 I'm not convinced that that happens, but maybe it does.

216
00:14:51,480 --> 00:14:55,460
 I think something you could argue might happen is the

217
00:14:55,460 --> 00:14:59,480
 angels get involved and they push open the doors.

218
00:14:59,480 --> 00:15:04,380
 But the kind of thing that would happen, not saying that

219
00:15:04,380 --> 00:15:06,350
 that's what happened and that it's what happened in this

220
00:15:06,350 --> 00:15:06,480
 case,

221
00:15:06,480 --> 00:15:09,490
 but the kind of thing that would happen from being such an

222
00:15:09,490 --> 00:15:13,840
 evil, evil person is that maybe the guards didn't listen to

223
00:15:13,840 --> 00:15:14,480
 him.

224
00:15:14,480 --> 00:15:17,480
 Maybe they didn't remove the staircase.

225
00:15:17,480 --> 00:15:20,760
 Especially if they were all Buddhist or keen about the

226
00:15:20,760 --> 00:15:21,480
 Buddha.

227
00:15:21,480 --> 00:15:25,480
 So they wouldn't want to get involved with this.

228
00:15:25,480 --> 00:15:31,480
 It says that the guards actually threw him down the stairs,

229
00:15:31,480 --> 00:15:34,670
 which gives me that idea that the guards were probably

230
00:15:34,670 --> 00:15:37,480
 pretty upset at Super Buddha at this point.

231
00:15:37,480 --> 00:15:41,410
 The doors open, the guards threw him down the first and the

232
00:15:41,410 --> 00:15:42,480
 seventh floor.

233
00:15:42,480 --> 00:15:44,830
 They threw him down to the sixth floor and he tumbled down

234
00:15:44,830 --> 00:15:45,480
 the stairs.

235
00:15:45,480 --> 00:15:48,480
 And the sixth floor threw him down to the fifth floor.

236
00:15:48,480 --> 00:15:51,670
 And they actually threw him down the stairs, seven flights

237
00:15:51,670 --> 00:15:52,480
 of stairs,

238
00:15:52,480 --> 00:15:55,480
 at which point he was swallowed by the earth.

239
00:15:55,480 --> 00:15:59,040
 Or he died at the bottom of the stairs right where the

240
00:15:59,040 --> 00:16:00,480
 Buddha had said.

241
00:16:00,480 --> 00:16:05,480
 And he descended therein and was reborn in Avicii Hell.

242
00:16:05,480 --> 00:16:09,480
 And that's how the story ends, just like that.

243
00:16:09,480 --> 00:16:15,480
 So it's one of those sort of fantastic stories.

244
00:16:15,480 --> 00:16:19,480
 It doesn't say too much about the verse.

245
00:16:19,480 --> 00:16:23,710
 Except to say that there's this idea that you can't avoid

246
00:16:23,710 --> 00:16:24,480
 karma.

247
00:16:24,480 --> 00:16:29,480
 The only way to...

248
00:16:29,480 --> 00:16:33,480
 There are ways you can mitigate,

249
00:16:33,480 --> 00:16:39,430
 like if you have negative karma, positive karma will cancel

250
00:16:39,430 --> 00:16:41,480
 it out sometimes.

251
00:16:41,480 --> 00:16:45,190
 But it does just that. It's the power of the good karma

252
00:16:45,190 --> 00:16:46,480
 that cancels it out.

253
00:16:46,480 --> 00:16:50,480
 You can't escape the karma entirely.

254
00:16:50,480 --> 00:16:54,480
 You have to do something to mitigate it.

255
00:16:54,480 --> 00:16:58,480
 Or to pass away into enlightenment first.

256
00:16:58,480 --> 00:17:02,430
 There's this idea that when you become enlightened and you

257
00:17:02,430 --> 00:17:03,480
 aren't reborn,

258
00:17:03,480 --> 00:17:07,860
 all the future results of your deeds don't have time to

259
00:17:07,860 --> 00:17:09,480
 come to fruition.

260
00:17:09,480 --> 00:17:12,480
 But as long as you're in samsara there's going to be room.

261
00:17:12,480 --> 00:17:15,240
 Everything has an effect, that's the point. I mean it's

262
00:17:15,240 --> 00:17:16,480
 like physics.

263
00:17:16,480 --> 00:17:20,480
 Everything has its power and the power doesn't just go away

264
00:17:20,480 --> 00:17:22,480
 and get forgotten about.

265
00:17:22,480 --> 00:17:27,480
 It doesn't just disappear, it has an effect.

266
00:17:29,480 --> 00:17:33,480
 And that's a way of looking at this verse.

267
00:17:33,480 --> 00:17:36,490
 I mean the most obvious way to look at the verse is the

268
00:17:36,490 --> 00:17:37,480
 idea of death.

269
00:17:37,480 --> 00:17:40,630
 How death comes to us all and therefore we shouldn't be

270
00:17:40,630 --> 00:17:41,480
 negligent.

271
00:17:41,480 --> 00:17:43,480
 We shouldn't waste our lives.

272
00:17:43,480 --> 00:17:49,480
 We should work to do what we can while we're still alive.

273
00:17:49,480 --> 00:17:52,100
 Because we don't know when death is coming, we don't know

274
00:17:52,100 --> 00:17:53,480
 where, we don't know how.

275
00:17:53,480 --> 00:17:57,480
 We don't know where we're going when we pass away.

276
00:17:57,480 --> 00:18:00,810
 But in the context of the story there's another interesting

277
00:18:00,810 --> 00:18:01,480
 point here.

278
00:18:01,480 --> 00:18:11,810
 It's that the reason we die and the reason death is

279
00:18:11,810 --> 00:18:13,480
 inevitable

280
00:18:13,480 --> 00:18:16,480
 is because of the karma of being reborn.

281
00:18:16,480 --> 00:18:22,480
 The karma of clinging at the moment of death

282
00:18:22,480 --> 00:18:26,480
 and therefore creating a new body.

283
00:18:26,480 --> 00:18:29,480
 That means we have to die.

284
00:18:29,480 --> 00:18:33,480
 I mean it's an example of karma, of the results of karma

285
00:18:33,480 --> 00:18:36,480
 that is inevitable.

286
00:18:36,480 --> 00:18:40,480
 That it's easy to see the inevitable results of karma.

287
00:18:40,480 --> 00:18:43,480
 But karma in general is like that.

288
00:18:43,480 --> 00:18:48,480
 So he's getting thrown down the stairs, it was inevitable.

289
00:18:48,480 --> 00:18:55,930
 So an interesting question then is whether our lives are

290
00:18:55,930 --> 00:18:58,480
 deterministic.

291
00:18:58,480 --> 00:19:02,320
 I've talked about this before that I don't think determin

292
00:19:02,320 --> 00:19:03,480
ism is the right way to look at it

293
00:19:03,480 --> 00:19:08,070
 because it requires a framework, a universe, a four

294
00:19:08,070 --> 00:19:10,480
 dimensional space time universe.

295
00:19:10,480 --> 00:19:13,480
 That's all really up in the mind.

296
00:19:13,480 --> 00:19:18,480
 If you look at the universe in terms of the present moment

297
00:19:18,480 --> 00:19:24,480
 then determinism doesn't really, it's saying too much.

298
00:19:24,480 --> 00:19:27,480
 It's hard to get your mind wrapped around it

299
00:19:27,480 --> 00:19:30,860
 but that's because we focus on the idea of a four

300
00:19:30,860 --> 00:19:31,480
 dimensional reality

301
00:19:31,480 --> 00:19:34,480
 of a universe existing around us.

302
00:19:34,480 --> 00:19:37,160
 So we can think in terms of billiard balls hitting each

303
00:19:37,160 --> 00:19:37,480
 other

304
00:19:37,480 --> 00:19:41,480
 and causing effects, cause and effect like that.

305
00:19:41,480 --> 00:19:44,090
 And Buddhism certainly subscribes to the idea of cause and

306
00:19:44,090 --> 00:19:44,480
 effect

307
00:19:44,480 --> 00:19:47,480
 but I think it stops there.

308
00:19:47,480 --> 00:19:52,130
 That there is an effect of our actions in the present

309
00:19:52,130 --> 00:19:52,480
 moment.

310
00:19:52,480 --> 00:19:56,480
 There isn't a, things go according to cause and effect.

311
00:19:56,480 --> 00:20:01,740
 But determinism, determinism is, to be deterministic is to

312
00:20:01,740 --> 00:20:03,480
 set yourself in the mind

313
00:20:03,480 --> 00:20:11,200
 with a concept of things being fixed, things existing that

314
00:20:11,200 --> 00:20:12,480
 are fixed

315
00:20:12,480 --> 00:20:17,480
 or are fixed in terms of their result.

316
00:20:17,480 --> 00:20:22,330
 So, but at the same time what there is is it seems that it

317
00:20:22,330 --> 00:20:23,480
 is possible to predict the future.

318
00:20:23,480 --> 00:20:27,510
 The Buddha is able to do it and people are able to see

319
00:20:27,510 --> 00:20:28,480
 things in the future

320
00:20:28,480 --> 00:20:32,940
 and the future seems to in some way, I mean maybe a good

321
00:20:32,940 --> 00:20:33,480
 way of looking at it

322
00:20:33,480 --> 00:20:37,480
 is that the future is able to affect the past.

323
00:20:37,480 --> 00:20:40,480
 It's a way of looking at quantum physics for example,

324
00:20:40,480 --> 00:20:43,480
 like why when you measure something does it affect

325
00:20:43,480 --> 00:20:46,480
 something that's already been measured.

326
00:20:46,480 --> 00:20:50,010
 When you affect one thing after the fact, it affects

327
00:20:50,010 --> 00:20:51,480
 something that has already,

328
00:20:51,480 --> 00:20:54,480
 it affects something in the past.

329
00:20:54,480 --> 00:20:56,480
 Strange things happen.

330
00:20:56,480 --> 00:20:59,480
 The future may be able to affect the past.

331
00:20:59,480 --> 00:21:06,380
 That can, not so important for us, but what is important is

332
00:21:06,380 --> 00:21:06,480
 this.

333
00:21:06,480 --> 00:21:08,660
 So we don't want to go too far in terms of thinking it's

334
00:21:08,660 --> 00:21:09,480
 all deterministic,

335
00:21:09,480 --> 00:21:13,480
 it's all, that's wrong view, no question.

336
00:21:13,480 --> 00:21:16,210
 And it's a very bad view to have practically speaking as

337
00:21:16,210 --> 00:21:16,480
 well

338
00:21:16,480 --> 00:21:24,480
 because it makes you lackadaisical, lazy basically.

339
00:21:24,480 --> 00:21:29,480
 But at the same time we have to understand that our actions

340
00:21:29,480 --> 00:21:31,480
 have consequences that are fixed.

341
00:21:31,480 --> 00:21:34,480
 If you do this, this is going to happen.

342
00:21:34,480 --> 00:21:40,440
 Well, when you do this, this comes with it, that kind of

343
00:21:40,440 --> 00:21:41,480
 thing.

344
00:21:41,480 --> 00:21:44,480
 And so how this relates to our meditation,

345
00:21:44,480 --> 00:21:48,070
 well, I've talked about how meditation teaches you about

346
00:21:48,070 --> 00:21:48,480
 karma.

347
00:21:48,480 --> 00:21:52,480
 More importantly, it purifies our karma.

348
00:21:52,480 --> 00:21:57,040
 The most important aspect of meditation is it purifies the

349
00:21:57,040 --> 00:21:57,480
 mind.

350
00:21:57,480 --> 00:21:59,480
 And so it prepares you for death.

351
00:21:59,480 --> 00:22:05,480
 Death becomes not a scary thing, not a powerful thing.

352
00:22:05,480 --> 00:22:09,100
 Death just becomes another moment because you see that we

353
00:22:09,100 --> 00:22:13,480
're actually born and die in every moment.

354
00:22:13,480 --> 00:22:17,480
 You train your mind, you purify your mind,

355
00:22:17,480 --> 00:22:22,290
 you come to see your body and your mind clearly, the

356
00:22:22,290 --> 00:22:27,480
 moments of experience.

357
00:22:27,480 --> 00:22:30,480
 And then you don't have to escape.

358
00:22:30,480 --> 00:22:33,480
 We're always looking for an escape or a shelter,

359
00:22:33,480 --> 00:22:36,970
 whether it be in the heavens or in the mountain, in a

360
00:22:36,970 --> 00:22:38,480
 palace.

361
00:22:38,480 --> 00:22:42,540
 Find a way to shut ourselves out from karma, like Supa

362
00:22:42,540 --> 00:22:44,480
 Buddha tried to do.

363
00:22:44,480 --> 00:22:48,480
 I mean, it's just one example, but we do this all the time.

364
00:22:48,480 --> 00:22:53,480
 Try and find ways to create security and safety.

365
00:22:53,480 --> 00:22:55,480
 So you don't need to do that.

366
00:22:55,480 --> 00:23:01,200
 Once you're pure in the mind, clear in the mind, you can

367
00:23:01,200 --> 00:23:02,480
 live on the street.

368
00:23:02,480 --> 00:23:06,480
 You can live in poverty.

369
00:23:06,480 --> 00:23:09,480
 You can be sick.

370
00:23:09,480 --> 00:23:14,480
 You can be injured.

371
00:23:14,480 --> 00:23:17,480
 You can be hungry and thirsty and pain.

372
00:23:17,480 --> 00:23:20,480
 And you can be at war.

373
00:23:20,480 --> 00:23:28,480
 You can be a victim of violence and still be invincible.

374
00:23:28,480 --> 00:23:30,480
 Then nothing overpowers you.

375
00:23:30,480 --> 00:23:32,480
 And the Buddha said, "You actually don't die."

376
00:23:32,480 --> 00:23:43,480
 The funny thing we talk about, you become free from death.

377
00:23:43,480 --> 00:23:46,480
 Death cannot overpower you.

378
00:23:46,480 --> 00:23:54,900
 Death doesn't overpower the one who is free from the fear

379
00:23:54,900 --> 00:23:56,480
 of death,

380
00:23:56,480 --> 00:24:02,480
 free from the attachment to life.

381
00:24:02,480 --> 00:24:05,480
 So that's the Dhammapada for tonight.

382
00:24:05,480 --> 00:24:07,480
 Thank you all for tuning in.

383
00:24:07,480 --> 00:24:09,480
 See you all next time.

