1
00:00:00,000 --> 00:00:02,580
 Are you saying that you analyze the act of sexual

2
00:00:02,580 --> 00:00:05,950
 activities including masturbation in order to act and stop

3
00:00:05,950 --> 00:00:06,740
 the act entirely?

4
00:00:06,740 --> 00:00:09,440
 Yes, yes, exactly. That's what I'm saying

5
00:00:09,440 --> 00:00:14,080
 But the point is that you need not engage in masturbation

6
00:00:14,080 --> 00:00:18,760
 That's not part of it and that's it's a delicate line

7
00:00:18,760 --> 00:00:21,460
 Why I say that and why I emphasize that is because

8
00:00:21,460 --> 00:00:24,900
 masturbation will actually decrease your awareness

9
00:00:24,900 --> 00:00:27,500
 It's not something you should feel guilty about

10
00:00:28,380 --> 00:00:32,120
 Guilt is a negative mind state and it's not going to help

11
00:00:32,120 --> 00:00:37,410
 But you should never get rid get the idea that if I don't

12
00:00:37,410 --> 00:00:41,100
 masturbate I'm gonna start repressing it this is I think a

13
00:00:41,100 --> 00:00:41,560
 pervasive

14
00:00:41,560 --> 00:00:44,960
 misunderstanding

15
00:00:44,960 --> 00:00:47,600
 When the desire comes up and you know you start

16
00:00:47,600 --> 00:00:51,990
 Engaging in bodily activities that are the precursor to

17
00:00:51,990 --> 00:00:53,180
 masturbation

18
00:00:54,480 --> 00:00:57,640
 Start to watch those if it happens that you start to to

19
00:00:57,640 --> 00:01:01,450
 engage in masturbation then then watch yourself and you'll

20
00:01:01,450 --> 00:01:01,860
 see

21
00:01:01,860 --> 00:01:06,590
 And it may be that in the beginning you can't stop yourself

22
00:01:06,590 --> 00:01:08,760
, but you will begin to see

23
00:01:08,760 --> 00:01:12,480
 The no you will not begin to see right away. You will see

24
00:01:12,480 --> 00:01:15,000
 the misunderstanding that there's actually no

25
00:01:15,000 --> 00:01:19,280
 Pleasure or is right no happiness to be found here

26
00:01:20,200 --> 00:01:23,360
 Once you look and you accept this pleasure because the

27
00:01:23,360 --> 00:01:26,200
 funny thing about about engaging in sensuality

28
00:01:26,200 --> 00:01:29,340
 It's we're not engaging in in the happiness indulging in

29
00:01:29,340 --> 00:01:30,600
 the happiness at all

30
00:01:30,600 --> 00:01:34,200
 We're we're engaging in the desire for the happiness

31
00:01:34,200 --> 00:01:36,350
 We're focusing much more on the desire than on the

32
00:01:36,350 --> 00:01:39,240
 happiness and when you focus on the happiness suddenly

33
00:01:39,240 --> 00:01:42,890
 you reassure yourself that you know I don't I don't need to

34
00:01:42,890 --> 00:01:45,040
 do this because the happiness is right here and

35
00:01:45,520 --> 00:01:49,250
 You look at it, and then you see it's nothing you don't

36
00:01:49,250 --> 00:01:51,160
 reject it and say this is horrible

37
00:01:51,160 --> 00:01:54,830
 This is actually suffering you see it's useless you you don

38
00:01:54,830 --> 00:01:57,760
't even really see there's uses, but you you

39
00:01:57,760 --> 00:02:01,160
 Experience it there's happiness, and that's it. There's no

40
00:02:01,160 --> 00:02:03,920
 reaction. There's no thought this is good. This is bad

41
00:02:03,920 --> 00:02:06,570
 This is me. This is mine. This is this this is that it's

42
00:02:06,570 --> 00:02:07,320
 happiness

43
00:02:07,320 --> 00:02:11,180
 And it's nothing it is what it is. It's there and then it

44
00:02:11,180 --> 00:02:13,400
 goes you don't become

45
00:02:14,520 --> 00:02:18,610
 Angry or frustrated or disgusted by it you just become dis

46
00:02:18,610 --> 00:02:19,760
enchanted with it

47
00:02:19,760 --> 00:02:23,200
 you lose this enchantment the desire disappears the

48
00:02:23,200 --> 00:02:25,120
 happiness doesn't disappear or

49
00:02:25,120 --> 00:02:28,600
 The happiness is not the issue

50
00:02:28,600 --> 00:02:31,320
 But the important thing is that the desire disappears

51
00:02:31,320 --> 00:02:34,360
 because the happiness will disappear and be gone

52
00:02:34,360 --> 00:02:38,620
 But as you watch the happiness sorry I'm meaning focusing

53
00:02:38,620 --> 00:02:39,040
 on the

54
00:02:39,040 --> 00:02:43,100
 I'm referring to the pleasure here focusing on the pleasure

55
00:02:44,040 --> 00:02:47,200
 Well first of all you allow you to stop the whole

56
00:02:47,200 --> 00:02:49,960
 seeking process and

57
00:02:49,960 --> 00:02:56,240
 And second of all will allow you to give up the desire for

58
00:02:56,240 --> 00:02:59,000
 the happiness entirely there will will allow you to

59
00:02:59,000 --> 00:03:04,830
 Do become free from yeah free from the desire we allow you

60
00:03:04,830 --> 00:03:05,580
 to become

61
00:03:05,580 --> 00:03:08,440
 Disenchanted

62
00:03:08,440 --> 00:03:10,670
 Will allow you to come to terms with the happy with the

63
00:03:10,670 --> 00:03:13,960
 pleasure and let you see that it's actually just

64
00:03:14,480 --> 00:03:15,760
 an

65
00:03:15,760 --> 00:03:17,760
 experience

