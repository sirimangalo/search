1
00:00:00,000 --> 00:00:03,840
 Restlessness, Question and Answer. Anxiety and Depression.

2
00:00:03,840 --> 00:00:08,240
 How can I free myself from anxiety and depression?

3
00:00:08,240 --> 00:00:13,260
 Mindfulness meditation helps us deal with and overcome

4
00:00:13,260 --> 00:00:15,200
 anxiety, depression, and many other

5
00:00:15,200 --> 00:00:18,640
 mental illnesses. So the answer to your question is quite

6
00:00:18,640 --> 00:00:21,280
 simple. It is one of the purposes of what

7
00:00:21,280 --> 00:00:24,990
 we teach and practice. There are, however, two issues that

8
00:00:24,990 --> 00:00:26,960
 potentially obstruct the proper

9
00:00:26,960 --> 00:00:29,880
 practice of mindfulness for one who suffers from chronic

10
00:00:29,880 --> 00:00:31,280
 anxiety or depression.

11
00:00:31,280 --> 00:00:36,230
 The first issue is the reification of depression and

12
00:00:36,230 --> 00:00:39,440
 anxiety. The term reification has great

13
00:00:39,440 --> 00:00:42,800
 importance in Buddhism. To reify means to give something

14
00:00:42,800 --> 00:00:46,080
 existence. When we say, "I have an

15
00:00:46,080 --> 00:00:49,420
 anxiety problem," we have reified that concept. We have

16
00:00:49,420 --> 00:00:51,920
 conceived of an entity that is monolithic

17
00:00:51,920 --> 00:00:55,350
 and atomic in the sense of being indivisible. It has become

18
00:00:55,350 --> 00:00:57,520
 a thing that you have to get rid of.

19
00:00:57,520 --> 00:01:01,480
 There is a story about the time after the Buddha passed

20
00:01:01,480 --> 00:01:03,280
 away, wherein the monks were gathered

21
00:01:03,280 --> 00:01:06,630
 trying to decide what the Buddha's actual teaching was.

22
00:01:06,630 --> 00:01:08,480
 Buddhism had split into a number of different

23
00:01:08,480 --> 00:01:10,910
 schools, and many of the schools were teaching things that

24
00:01:10,910 --> 00:01:12,720
 certainly did not bear any resemblance

25
00:01:12,720 --> 00:01:16,600
 to the original teachings. A king asked one well-respected

26
00:01:16,600 --> 00:01:18,320
 monk, "What did the Buddha teach?

27
00:01:18,320 --> 00:01:20,770
 What sort of teacher was the Buddha?" And the monk

28
00:01:20,770 --> 00:01:23,840
 correctly answered, "The Buddha was one

29
00:01:23,840 --> 00:01:27,480
 who split things apart. The Buddha taught the splitting of

30
00:01:27,480 --> 00:01:29,680
 atoms. You had this indivisible

31
00:01:29,680 --> 00:01:32,540
 entity you call anxiety or depression. The Buddha's

32
00:01:32,540 --> 00:01:34,560
 teaching aims to split it up into its

33
00:01:34,560 --> 00:01:38,080
 constituent parts. It is about separating the condition,

34
00:01:38,080 --> 00:01:40,080
 problem, illness, or whatever

35
00:01:40,080 --> 00:01:43,490
 into actual realities, because depression and anxiety do

36
00:01:43,490 --> 00:01:46,720
 not exist. What exist are moments of

37
00:01:46,720 --> 00:01:50,110
 experience. At an ultimate level, that is what is truly

38
00:01:50,110 --> 00:01:54,240
 real. For anxiety, there will be moments of

39
00:01:54,240 --> 00:01:58,330
 experiencing that we lump together as anxiety. Suppose that

40
00:01:58,330 --> 00:02:00,240
 you have to get up and go onto a

41
00:02:00,240 --> 00:02:03,330
 stage in front of a hundred people or a thousand people and

42
00:02:03,330 --> 00:02:05,600
 give a talk. You are sitting backstage

43
00:02:05,600 --> 00:02:08,680
 or in the audience waiting for your turn, and you think

44
00:02:08,680 --> 00:02:12,000
 about going up on stage. That thought is an

45
00:02:12,000 --> 00:02:15,600
 experience. Then immediately after that, anxiety might

46
00:02:15,600 --> 00:02:19,120
 arise, which is another experience. After

47
00:02:19,120 --> 00:02:21,960
 that initial experience of anxiety, there may be

48
00:02:21,960 --> 00:02:24,640
 experiences of butterflies in the stomach,

49
00:02:24,640 --> 00:02:28,060
 the heart beating fast, or tension in the shoulders. And

50
00:02:28,060 --> 00:02:29,600
 based on those experiences,

51
00:02:29,600 --> 00:02:33,760
 other experiences will arise. This chain of experiences can

52
00:02:33,760 --> 00:02:35,280
 create a feedback loop, which

53
00:02:35,280 --> 00:02:38,920
 is how panic attacks arise. You enter this feedback loop

54
00:02:38,920 --> 00:02:40,960
 based on moments of experience

55
00:02:40,960 --> 00:02:44,240
 that spiral so far out of control, and the habit or the

56
00:02:44,240 --> 00:02:46,960
 causal chain is so strong, quick, and ingrained

57
00:02:46,960 --> 00:02:51,010
 in you that you are unable to get yourself out of it. Exper

58
00:02:51,010 --> 00:02:53,680
iences of depression can be similar.

59
00:02:53,680 --> 00:02:56,480
 There are experiences. Maybe something bad happens to you,

60
00:02:56,480 --> 00:02:58,480
 and maybe you become sad. And then

61
00:02:58,480 --> 00:03:01,200
 reacting with sadness becomes a habit, and you become more

62
00:03:01,200 --> 00:03:02,720
 and more sad when you think about

63
00:03:02,720 --> 00:03:05,620
 that experience. And then you try to get rid of the thought

64
00:03:05,620 --> 00:03:07,600
, so the thought becomes a source of

65
00:03:07,600 --> 00:03:11,420
 aversion. You feed it. There is an energy involved with

66
00:03:11,420 --> 00:03:13,760
 avoiding things or disliking things that

67
00:03:13,760 --> 00:03:17,930
 creates more negative thoughts and habits. All of this is

68
00:03:17,930 --> 00:03:20,160
 just to say that the solution from a

69
00:03:20,160 --> 00:03:22,990
 Buddhist perspective is to break things into their

70
00:03:22,990 --> 00:03:25,840
 constituent parts. That is the most important step.

71
00:03:25,840 --> 00:03:29,400
 The second issue that gets in the way of dealing with

72
00:03:29,400 --> 00:03:31,840
 mental illness is the negative relationship

73
00:03:31,840 --> 00:03:35,070
 we have towards the chain of experiences. To call it a

74
00:03:35,070 --> 00:03:37,840
 mental illness, even to call it bad, is not

75
00:03:37,840 --> 00:03:40,660
 inherently wrong, but we have to understand the difference

76
00:03:40,660 --> 00:03:42,720
 between understanding something is bad

77
00:03:42,720 --> 00:03:46,500
 for us and feeling bad about it. We feel bad about our

78
00:03:46,500 --> 00:03:49,920
 depression. We feel averse to it. We feel bad

79
00:03:49,920 --> 00:03:53,270
 about our anxiety, and we want to get rid of it. Taking our

80
00:03:53,270 --> 00:03:55,520
 patterns of experience as a monolithic

81
00:03:55,520 --> 00:03:58,210
 condition and feeling bad about that condition are hindr

82
00:03:58,210 --> 00:04:00,400
ances to actually dealing with the condition.

83
00:04:01,680 --> 00:04:04,000
 They lead us to seek out ways to not have to deal with the

84
00:04:04,000 --> 00:04:05,520
 condition directly,

85
00:04:05,520 --> 00:04:07,920
 taking medication or other drastic measures.

86
00:04:07,920 --> 00:04:12,410
 People take medication to cover up anxiety and depression

87
00:04:12,410 --> 00:04:14,880
 by flooding their systems with

88
00:04:14,880 --> 00:04:17,550
 serotonin and other brain chemicals that prevent them from

89
00:04:17,550 --> 00:04:19,280
 following the habits and cycles that

90
00:04:19,280 --> 00:04:22,520
 they normally would get into. They see anxiety and

91
00:04:22,520 --> 00:04:26,080
 depression as problems and react negatively to them.

92
00:04:26,080 --> 00:04:29,250
 In order to begin to face our problems, we must first begin

93
00:04:29,250 --> 00:04:31,120
 by seeing that our reactions to our

94
00:04:31,120 --> 00:04:34,920
 problems simply cause us more suffering. Some of what we

95
00:04:34,920 --> 00:04:37,760
 perceive as depression and anxiety is not

96
00:04:37,760 --> 00:04:41,300
 even mental at all. For example, butterflies in the stomach

97
00:04:41,300 --> 00:04:43,200
, heart beating fast, tension and

98
00:04:43,200 --> 00:04:47,770
 headaches, etc., are distinct from actual anxiety. They are

99
00:04:47,770 --> 00:04:50,320
 not anxiety. They are merely physical

100
00:04:50,320 --> 00:04:53,830
 byproducts of anxiety. They are not a problem, and if we

101
00:04:53,830 --> 00:04:55,680
 can see them as just experiences,

102
00:04:55,680 --> 00:04:58,320
 then we will not react to them, feeding our negative mental

103
00:04:58,320 --> 00:05:00,960
 habits. There is no question

104
00:05:00,960 --> 00:05:04,210
 that depression, anxiety and any mental illness are bad,

105
00:05:04,210 --> 00:05:06,160
 but what is bad about them is that they

106
00:05:06,160 --> 00:05:09,450
 are reactions. They are bad reactions, and if you react to

107
00:05:09,450 --> 00:05:11,520
 them by disliking, hating or feeling

108
00:05:11,520 --> 00:05:14,860
 guilty about them, you are feeding your negativity, leading

109
00:05:14,860 --> 00:05:17,040
 only to more anxiety and depression.

110
00:05:17,040 --> 00:05:21,250
 You can see this in regards to schizophrenia. My

111
00:05:21,250 --> 00:05:23,200
 understanding of schizophrenia is that it

112
00:05:23,200 --> 00:05:26,560
 involves what are ordinarily referred to as hallucinations.

113
00:05:26,560 --> 00:05:28,000
 In Buddhist meditation practice,

114
00:05:28,000 --> 00:05:31,330
 however, we would simply call them experiences. Maybe you

115
00:05:31,330 --> 00:05:33,120
 hear voices, maybe you see things,

116
00:05:33,120 --> 00:05:35,360
 or maybe thoughts arise that you think come from an

117
00:05:35,360 --> 00:05:38,800
 external source. It is not to ask whether they

118
00:05:38,800 --> 00:05:42,190
 are real or hallucinations. It is important to understand

119
00:05:42,190 --> 00:05:44,240
 they are experiences, to see them for

120
00:05:44,240 --> 00:05:48,520
 what they are, and to not react to them at all. I met a

121
00:05:48,520 --> 00:05:50,960
 monk once who told me he heard voices

122
00:05:50,960 --> 00:05:53,530
 telling him to kill himself. He had difficulty

123
00:05:53,530 --> 00:05:56,000
 understanding that they were just voices.

124
00:05:56,000 --> 00:05:59,000
 Even if a real person tells you to kill yourself, it does

125
00:05:59,000 --> 00:06:01,680
 not mean you should. Without mindfulness,

126
00:06:01,680 --> 00:06:04,940
 we are carried away by our experiences, reacting before we

127
00:06:04,940 --> 00:06:06,880
 can appreciate them for what they really

128
00:06:06,880 --> 00:06:10,640
 are. Being able to see our experiences as experiences is

129
00:06:10,640 --> 00:06:13,040
 essential to mindfulness meditation.

130
00:06:13,040 --> 00:06:17,430
 The first part of the solution is to break our conditions

131
00:06:17,430 --> 00:06:19,680
 up into individual experiences and to

132
00:06:19,680 --> 00:06:22,930
 understand first conceptually and eventually through direct

133
00:06:22,930 --> 00:06:24,880
 experience the distinction between

134
00:06:24,880 --> 00:06:27,810
 entities like depression where one might think I have

135
00:06:27,810 --> 00:06:30,320
 depression and the basic building blocks of

136
00:06:30,320 --> 00:06:33,880
 experience. The second part is to take those building

137
00:06:33,880 --> 00:06:36,240
 blocks including the problem and see

138
00:06:36,240 --> 00:06:39,580
 them objectively and without reaction. Let them be just

139
00:06:39,580 --> 00:06:42,240
 what they are. Try to create this ability

140
00:06:42,240 --> 00:06:46,480
 to see experiences objectively. This is what allows wisdom

141
00:06:46,480 --> 00:06:49,280
 to arise, what allows us to see clearly,

142
00:06:49,520 --> 00:06:52,930
 and what allows us to let go. Our attachments involve

143
00:06:52,930 --> 00:06:55,360
 delusion, ignorance, and lack of

144
00:06:55,360 --> 00:06:58,700
 understanding of what real happiness and peace are.

145
00:06:58,700 --> 00:07:00,880
 Happiness and peace cannot come from trying

146
00:07:00,880 --> 00:07:04,830
 to fix our problems, and they cannot come from experiences

147
00:07:04,830 --> 00:07:07,600
 themselves. Happiness and peace have

148
00:07:07,600 --> 00:07:15,280
 to come from the way we perceive and relate to experiences.

