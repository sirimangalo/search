 Hello and welcome back to our study of the Nama Pada. Today
 we continue on with verse
 164 which reads as follows.
 Yosasanang arahatang ariyanang dhammaji vinam patikosati d
ummedho
 titing misaya pabhikam palani katakaseva ata gataya pānti
 which means yo patikosati, yo dummedho patikosati, whatever
 fool, whatever person
 insults the religion of the enlightened ones, of the noble
 religion, noble teaching.
 The dhammaji vinam, the religion of those who live the holy
 life,
 live according to righteousness, the ariyans.
 Whoever insults it, the fool, dummedha, titing misaya, tit
ing misaya pabhikam,
 because of the evil views, dependent on their evil views.
 So they have evil views, so they insult the noble way.
 Just like the fruit of the katakari, palani katakaseva, ata
 gataya pānti,
 they bear fruit, the bearing of fruit or the fruit that
 comes from their act,
 destroys themselves.
 So there's this reed, the kataka reed, that is so, when it
 bears fruit,
 the fruit is so heavy that the reed snaps.
 Such a person who insults the reviles the good way, the
 noble way,
 and has evil views, destroys themselves.
 Just like that, the fruit of their evil.
 So this was taught in regards to the elder kala.
 Kala was a monk who lived in Sawati.
 And the story goes that there was a certain woman,
 maybe an elderly woman, who looked after him
 with the tenderness of a mother for a son.
 So she took great care of him.
 And one day, her neighbors went to hear the Buddha teach
 the dhamma,
 and they said, "Oh, how wonderful is it to hear the
 teachings of the Buddha, how great is it?"
 And so hearing this, she says to the elder, she said, "What
 do you think?
 Should I go and listen to the Buddha teach someday?"
 And they said, "No, no, no, you shouldn't."
 And again, she started thinking about this, and so she
 asked him again,
 "Well, why not?" And he said, "Oh, no, no, it's not worth
 your time."
 He wouldn't give her a good reason, but he kept dissuading
 her from it.
 And we're told that his reasoning was that if she went to
 hear the Buddha teach,
 she would have no use for him.
 Here he was providing her with her only source of the dham
ma.
 Well, if she went to see the Buddha, he knew it.
 He saw what happened then when they heard the Buddha teach.
 They got great faith from the Buddha.
 And then in comparison, they would look at the other monks
 and say,
 "These other monks, they would only have eyes for the
 Buddha."
 This happened a lot in my monastery. It's kind of funny.
 Because our headmaster is just so esteemed that most of the
 other monks actually starved
 or were very poorly taken care of. It was very hard to
 survive in some ways
 as a monk in my monastery because everybody was a very rich
 monastery,
 but all the money and support went only to the top.
 So for those monks looking for gain, it was very hard to be
 well off as a monk there.
 Whereas, I mean, that might sound strange because, of
 course, monks are supposed to be very content
 with little or nothing.
 But in some places in Thailand, monks were able to be very
 well taken care of.
 So this monk was quite well taken care of and didn't want
 to lose this.
 There's also a reason why sometimes monks would leave a
 monastery
 and go off and start their own.
 It's because when you're alone, well, you're the only monk
 and the only religious teacher, it's very easy to get along
.
 And this is all, certainly I don't want to give you the
 idea that I'm condoning this
 or encouraging it. It's great danger and it's very wrong to
 be even thinking in this sort of way.
 This monk was very much in the wrong for his thought
 and much more for his actions in dissuading this woman.
 Finally, she had enough and she had her daughter bring food
 to the monk
 and take care of this monk.
 Do whatever you can to make sure he's happy.
 I'm going to go to Jaita on it and hear the Buddha teach.
 And so the elder was in this, I guess he was staying in the
 monastery in the city.
 There would be, I guess, places to stay in the city.
 And when this monk saw him coming, when this monk saw her
 coming, bringing him food,
 he asked her, "Oh, where's your mother?"
 She said, "Oh, she's gone to hear the Buddha teach."
 And as soon as the monk heard this, he got consumed with
 anger and fear
 that she had disregarded his words, disregarded his evil
 advice,
 and he ran or went quite quickly to Jaita Vana.
 And when he saw this woman listening to the Buddha teach
 the Dhamma,
 this anger and just blind range, because can you imagine
 having the audacity
 to do what he did, which is, he goes to the Buddha and says
 to the Buddha,
 "Bhante, this stupid woman does not understand your subtle
 discourse on the Dhamma."
 How awful, no?
 In front of this woman scold her like that and discouraged
 the Buddha from teaching
 as though the Buddha didn't know how to teach people to
 their own level.
 And he says, "Teacher instead," he asked the audacity to
 tell the Buddha what to teach her.
 "Teacher the duty of almsgiving and moral precepts, Dhamma
 and Sila."
 Basically what he's saying is, "Don't teach her anything
 deep."
 Why? Because I can't teach her anything deep because I'm an
 evil, vicious, foolish monk
 and I'm basically useless.
 So if you teach her the great Dhamma, what use will she
 have for me?
 I mean, he doesn't say this, but this is of course the un
spoken reason.
 It's because he's really a terrible person and he doesn't
 want the comparison.
 So if the Buddha just teaches her simple things, things
 that won't actually make her enlightened,
 of course the worst for him would be if she were to become
 enlightened
 because then she'd turn around and look at him and say,
 "Why are you teaching me, you useless man?"
 But if the Buddha were only to teach her charity and
 morality, then he'd have a chance.
 Because those are simple things that simple people can
 teach.
 It's not easy to teach about vipassana or insight
 meditation.
 It's not easy to teach people mindfulness.
 It's easy to say the word mindfulness, be mindful, but to
 actually understand it.
 You know, all these things that we talk about, the paradigm
 shift and the way of looking at the world
 in terms of ultimate reality, seeing experiential reality
 and understanding.
 It's not just intelligence. It takes vision. It takes
 insight.
 It takes concentration or focus, which means your mind has
 to be to some extent pure.
 If your mind is full of evil, unwholesome desires, it's
 very difficult to think even,
 to resonate with concepts like impermanence, suffering, non
-self.
 Not easy.
 So he compounds his evil by not only dissuading this woman
 from hearing the Buddha,
 but now reviling her in front of the Buddha and ordering
 the Buddha,
 instructing the Buddha on how to teach.
 Very evil.
 So the Buddha looks at him and says,
 "Mogapuri" is, I'm assuming, the word he uses, let's see.
 Oh, he says, "Dupanyo" you stupid person.
 "Panya" is wisdom. "Dupanya" means someone who has no
 wisdom.
 "Papikanditinisaaya buddhanamsasanampatikosati"
 He's curious that the Buddha uses these words.
 He says, "Based on your evil views, you revile the religion
 of the Buddhas."
 Because he's not actually. That's one thing he's not
 explicitly doing.
 And so this speech is meant to connect it to the verse.
 And if you look at this, you might think,
 just the commentator trying to fit a different story
 in with this verse, which is totally unrelated.
 You might think that way, because the verse is about rev
iling the Dhamma,
 which is an important concept that we'll talk about.
 But the story is about evil and the fear of losing one's
 gain, really.
 Losing one's income, the fear of the Dhamma.
 But we can be charitable to the commentator.
 And there's an easy way to understand this.
 The Buddha is trying to, and in many cases it appears that
 this is the way it was,
 the Buddha was trying to redirect things.
 Often the Buddha's response to a problem is not to address
 it head on.
 Because just arguing with someone or debating or fighting
 with someone
 may mean that in the end you're right and they're wrong.
 But it may not fix the problem.
 What is the true problem here?
 And so the Buddha hits the heart of the matter
 by saying, "In doing this you are insulting the Dhamma.
 You are reviling it. You are in some way attacking the Dham
ma."
 So rather than actually point out that he's just afraid of
 losing his gain,
 he says, "Really what's at stake, what's at work here, at
 playing, at work here,
 is your evil outlook, your corrupt way of looking at things
.
 And you're only going to hurt yourself.
 You think this is to your gain to prevent this woman from
 hearing the Dhamma,
 but it's only to your loss."
 So he that attacks the Dhamma, because that's what's going
 on,
 this woman is hearing the Dhamma, this is the transmission
 of the Dhamma,
 the spreading, the arising, the growth, the planting of the
 seed of the Dhamma.
 This is what's happening.
 And he's attacking it like a soldier attacking a fortress.
 He's attacking the spreading of the Dhamma, the Dhamma-das
ana, the teaching of the Dhamma.
 So you can say that he's attacking, that he's still hard to
 see how he's insulting the Dhamma,
 but you can think of it as an insult to...
 Well, first off, suggest that the Buddha doesn't know how
 to teach.
 Second off, to focus or to insist on the teachings only of
 morality and charity,
 which though useful and quite Buddhist are by no means deep
 or profound or conducive
 or leading them in and of themselves to enlightenment.
 And then the Buddha teaches this verse.
 So that's the story.
 So what lessons we can gain?
 Again, I think we can look at the two different sides of
 this,
 the story side and the verse side.
 In reference to the story, it's more related to evil views
 and how people make use of Buddhism.
 This monk is clearly not only greedy, but allowing his
 greed.
 I know that's the wrong word, but let's use it for now.
 Allowing his greed to inform his actions.
 So why I say allowing is probably the wrong word is because
 it's more like based on his desire.
 And this is the key point.
 This is an important point to talk about.
 It's common to think, I mean, an ordinary understanding of
 desire
 that exists independent of our other emotions.
 You want something and that's it.
 You want it and either you get it or you don't get it.
 We don't connect that the wanting, that our desires are
 very much related to our aversion
 and our fear and our jealousy, arrogance, conceit.
 That our desires corrupt our minds.
 We have this idea that I can want things and still be a
 kind and nice and generous person to others.
 That it doesn't actually diminish my purity of mind.
 We don't connect. We don't make a connection, generally
 speaking.
 The fact that I want things, the fact that I like certain
 things,
 what does that have to do with whether I'm an evil or a
 good person?
 What does that have to do with how I treat others?
 And this is an important example, or it's a good example of
 this important concept,
 that there is a connection.
 That as with all things, this is the reason for the Buddha
 to take up the concept of karma
 and apply the word karma in a Buddhist sense.
 To show that there are consequences.
 The consequences that we didn't realize, there are
 connections,
 causal connections between things like desire and cruelty.
 How awful this man is to this woman.
 Can you imagine knowing what we know about how great the
 Buddha's teaching is?
 What a horrible thing to do to prevent, to do whatever you
 can to prevent someone from hearing it.
 And how blind you have to be. It's easy to see.
 This man's focus was so strongly on the things that he
 liked.
 He had cultivated such an addiction that it blinded him.
 That he probably didn't even think about whether it was
 good or evil.
 His mind was so clouded.
 That is what you see, you know?
 It's one thing to want something.
 And to know that you're feeling that you don't want to
 share it with someone else.
 That you don't want to lose it for someone else's gain.
 Basically, to put it in general terms, if this woman gains
 something,
 then he's going to lose something.
 So, ordinarily we weigh these things.
 We think, "Oh, if this woman goes to the Buddha, I'm going
 to lose my support."
 But of course, an ordinary human being with even an ounce
 or a shred of decency would feel ashamed
 and acknowledge that it's much more important that this
 woman goes to talk to the Buddha.
 But you see this. It gets to the point where it blinds you.
 I think you have to say that it's on another level.
 And this is why the Buddha brings up the idea of views.
 Because your views blind you.
 This person's view, and it's an odd word, but if we think
 of it more as inability to see his blindness.
 He is blinded, right? He can't see.
 His view is this. This is his view.
 And in being blind, or because of his blindness,
 he does whatever he can to stop this woman from benefiting.
 But the point for us, I mean, this is a good lesson for us
 to think of this man.
 A much better lesson is to ponder and consider the
 immediate relationship between things like desire
 and our lack of compassion, stinginess, jealousy, avarice,
 envy, these kind of things, and cruelty.
 That you can't just go ahead and want and want and want and
 still be a kind and gentle person.
 I mean, actually the way it is, the more you want it, it
 starts to suck away.
 It starts to eat away at your goodness.
 You can be a good person and start to want and to like
 things.
 But your wants and your needs, the reason why we don't see
 this is because ordinary life is full of evil.
 Ordinary human life, we don't think of it this way. And it
 only really makes sense if you actually take the time to
 meditate and become sensitive.
 Because that's what it is. If you're living in a cesspool,
 you aren't sensitive to dirt, to filth.
 But once you begin to purify your mind, like a person who's
 left the cesspool, taken a shower,
 and when they go anywhere near the cesspool, they're
 completely revolted, you become more sensitive to it.
 I mean, good people are more. This is what we talked about
 last time. Good people are quite sensitive to evil.
 Evil people are not sensitive to evil. They're quite
 sensitive to good and allergic to it, we might say.
 And this is a really important point with craving.
 Craving isn't so, for desire isn't so much about the
 consequences.
 You know, in terms of, you know, later you might not get
 what you want, you'll be upset.
 That's important, but much more important is the very act,
 the very nature of the act, or the state of wanting
 something.
 And that it corrupts the mind. It has an immediate effect,
 and it has a direct relationship with your clarity, your
 purity, your goodness of heart.
 That's a part of what you see in meditation. You can see
 and feel directly how wrong it is to cling to things, to
 desire things,
 which seems so foreign to most people. There's this idea
 that it's part of being human, and it's more static, right?
 We think of it as static. I like X, as though you've always
 liked X, and you always will like X, and it's just part of
 who you are.
 Which is not at all the case. It's a habit. By reinforcing
 it, you're strengthening it.
 And it's pulling on everything else. It's taking energy
 away from good things you can be doing, and it's corrupting
 them.
 It's lowering, it's reducing your interest in being a good
 person.
 So, this is the sorts of things that are interesting in
 regards to the story.
 The verse, again related though it is, does clearly teach a
 different lesson, but related, and it's related to the idea
 of karma.
 And it offers an interesting idea that there's something
 especially bad about evil done in relation to Buddhism.
 That's the superficial explanation, the superficial
 description. But what it's really saying is that when you
 oppose good things,
 your opposition to good things is resulting, results in
 evil proportionate to the goodness of the thing that you're
 opposing.
 That's really the Buddhist dog or doctrine.
 In Buddhism, what's the most pure and beautiful and
 wonderful and good thing? Of course, it's the teaching of
 the Buddha,
 not just in its theoretical state, but in terms of the
 dissemination.
 Anyone who gets in the way of someone hearing the Buddha's
 teaching,
 I mean, is understandably thought to be doing one of the
 most horrific things possible.
 Not the most, but it's got to be up there to cut someone
 off, to purposefully prevent someone from such a powerful
 good.
 It's a special sort of evil. It takes for this man to allow
 his desires to lead him to actually ignore this woman's
 kind of prevent her to be so blind.
 It takes blindness as the point. Beyond ordinary blindness,
 it takes a mindset is really what dithi, dithi is the word
 view,
 that's what it means, but it relates to mindset. This
 person just is perverse, really.
 Their mind is corrupt or perverse. Twisted is the point.
 The way they look at the world is so mixed up, messed up.
 That's really what view is all about. That's what this is
 teaching. It's teaching about views.
 It's more than one thing. It's teaching about the special
 evil of reviling the Buddha,
 reviling the Dhamma, the Sangha, of attacking them.
 It's talking about the evil that's required to do that, the
 evil of views out-twisted you have to be.
 That's how this all relates to meditation and how views
 come into play and what views really mean in Buddhism and
 why they're such an important thing.
 The views that we're concerned about, the concept of views,
 is the twisted way of looking at the world.
 We had this question about what is right view. Theoret
ically, right view is anything that's in line with reality.
 But that's not really what we're concerned about. We're
 concerned with obtaining noble view,
 which really means a straight relationship with reality,
 where you see things, you connect with reality, this is
 this, it is what it is.
 It sounds simple. That is right view. So hard.
 We think, well, there must be something more to it than
 that. The problem is there's so much more to it than that,
 and all that more to it is what's wrong.
 We make so much out of everything and twist things.
 It's really two different things. You can make more of
 something than it is, like, "This is good, this is bad."
 That's already twisted, never mind.
 As soon as you see something is good or bad, it's twisted.
 You can make something more out of something like, say, "
This is a hand." It's not actually a hand.
 It's light touching your eye. Even that is making a little
 more out of it. It's seeing.
 This is seeing. But you think it's a hand. That's making
 more out of it than it actually is. That's not really a
 problem.
 It's potentially leading to problems. But the real problem
 is when you get twisted and you say, "Oh, what a beautiful
 hand. What an ugly hand.
 What a stupid thing for him to do to raise his hand. Or, "
Oh, I really like that. He's doing that."
 Maybe you think it's funny, or maybe you think it's clever,
 and you start to twist it.
 Maybe you think you're clever because you understand it.
 Maybe you think you're more clever because you could have
 thought of a better way to explain it, right?
 So much twisting and turning.
 Maybe we can tie this into the idea of the reed. It's like
 you have this reed, and if the reed is straight, it can
 bear all that fruit.
 But when it's twisted, it snaps.
 The twisting of the mind. What the twisting of the mind
 really does is it makes you twisted. You can't see the body
.
 It's like you become mentally a hunchback, is what I'm
 trying to say. It's like you look at someone whose body is,
 whose back is not straight.
 This is the way the mind goes. The mind, it coils in on
 itself. It recoils. Evil is like that.
 It weighs heavily on the mind, is what the Buddha says. It
 weighs so heavily on the mind that it breaks the mind.
 Here's a good example. This man was clearly so consumed by
 evil that he did the unthinkable.
 There are worse things he could have done, like he could
 have come and tried to kill the Buddha or something.
 "Oh no, maybe if I kill the Buddha, she won't be able to
 hear his teachings." That would have been worse.
 But it's still hard to fathom the gall of this man.
 And clearly caused by evil. This isn't just a fairy tale.
 This is happening all the time.
 Today there was a mass shooting in Las Vegas. I think it
 had something to do with that.
 Apparently, I don't really follow the news, but something
 to do with a gambling debt, which is very apropos to this
 sort of story.
 Can you imagine? Incredible. Incredible to think. Assuming
 that this is what I heard or what I read was what really
 happened.
 Someone became so consumed by greed and anger at their loss
 that they went out and shot hundreds of people or something
.
 This isn't a fairy tale. This is reality. There are very
 real consequences to our evil.
 And you think, "Wow, you're overstating it. My evil is not
 like that man's evil."
 It's only a matter of degree. Evil is evil. And that's why
 we call it evil.
 We don't beat around the bush and say, "It's okay. Everyone
 likes things. It's not really evil." It is. It really is
 evil.
 It's evil to want anything. It's just a matter of degree.
 And what we don't get.
 We think it's okay to like, just don't get out of hand. It
's all out of hand. It's all wrong.
 There's no good that comes from wanting or liking things.
 It's a hard pill to swallow.
 But for someone who's mindful, there's no other way. You
 can't see things that way anymore.
 That wanting could be in any way good. You can't deny the
 fact. You don't want to deny the fact.
 You free yourself from the blindness of thinking that there
's some good to be had from liking or wanting.
 So, that's the Dhammapada for tonight. Thank you all for
 tuning in.
