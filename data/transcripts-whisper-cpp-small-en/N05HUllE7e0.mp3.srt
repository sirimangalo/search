1
00:00:00,000 --> 00:00:07,270
 At least for me, Venerable Yudhada Maviku, can I make real

2
00:00:07,270 --> 00:00:09,360
 progress in the Vipassana

3
00:00:09,360 --> 00:00:14,540
 practice in regards to the Vipassana-nana without being

4
00:00:14,540 --> 00:00:18,560
 able to establish Kanika Samadhi?

5
00:00:18,560 --> 00:00:22,720
 If not, should I do Samatha first?

6
00:00:25,920 --> 00:00:28,710
 Without being able to establish, how can you not be able to

7
00:00:28,710 --> 00:00:30,240
 establish Kanika Samadhi?

8
00:00:30,240 --> 00:00:32,480
 I mean, if you killed your parents, probably you couldn't,

9
00:00:32,480 --> 00:00:35,360
 but otherwise Kanika Samadhi,

10
00:00:35,360 --> 00:00:37,920
 you have to understand it's a moment. It's a single moment.

11
00:00:37,920 --> 00:00:40,320
 It's actually quite easy to

12
00:00:40,320 --> 00:00:46,530
 establish because it's not established, it's a moment. Kan

13
00:00:46,530 --> 00:00:48,080
ika Samadhi is seeing impermanent

14
00:00:48,080 --> 00:00:51,030
 suffering in non-self. It's the moment where you see

15
00:00:51,030 --> 00:00:53,520
 something arise or you see something ceased

16
00:00:53,520 --> 00:00:56,580
 or you both see something arise and cease, where you see

17
00:00:56,580 --> 00:01:00,720
 reality occurring. And more than just see

18
00:01:00,720 --> 00:01:05,130
 it, you are clearly aware of it as being that reality. So

19
00:01:05,130 --> 00:01:07,520
 for example, when you say pain,

20
00:01:07,520 --> 00:01:11,790
 pain, you're in your mind clear that that is pain, nothing

21
00:01:11,790 --> 00:01:13,600
 more, nothing less.

22
00:01:13,600 --> 00:01:20,530
 So I don't quite get to the problem you're having. If you

23
00:01:20,530 --> 00:01:23,200
 want to do Samatha first, go for it.

24
00:01:24,240 --> 00:01:27,150
 It gives you a lot of power of mind and can be quite useful

25
00:01:27,150 --> 00:01:28,560
 in developing later

26
00:01:28,560 --> 00:01:31,810
 Kanika Samadhi or Vipassana Samadhi, where you see imper

27
00:01:31,810 --> 00:01:33,840
manent suffering in non-self.

28
00:01:33,840 --> 00:01:41,080
 So don't be too discouraged about not getting... One thing

29
00:01:41,080 --> 00:01:43,120
 is, this is a very

30
00:01:43,120 --> 00:01:47,780
 prominent theme that runs through all new meditator's

31
00:01:47,780 --> 00:01:50,720
 courses and a lot of the questions

32
00:01:50,720 --> 00:01:55,750
 that I get is this idea that your practice is futile, your

33
00:01:55,750 --> 00:01:58,480
 practice is not bearing fruit.

34
00:01:58,480 --> 00:02:01,300
 And so even today, one meditator who's been here quite a

35
00:02:01,300 --> 00:02:02,560
 while is still wondering

36
00:02:02,560 --> 00:02:08,480
 when is she or she was wondering whether she didn't say it,

37
00:02:08,480 --> 00:02:10,720
 but what she said was that she's

38
00:02:10,720 --> 00:02:13,610
 wondering whether she should spend some time thinking about

39
00:02:13,610 --> 00:02:15,440
 impermanent suffering in non-self.

40
00:02:15,440 --> 00:02:19,670
 And she's been here for quite a while, for at least two

41
00:02:19,670 --> 00:02:24,000
 weeks, I think. And I think her practice is

42
00:02:24,000 --> 00:02:27,060
 going well. It looks very clear as though her practice is

43
00:02:27,060 --> 00:02:29,440
 going quite well, but we started

44
00:02:29,440 --> 00:02:32,000
 talking about impermanent suffering in non-self and like

45
00:02:32,000 --> 00:02:34,640
 most people, she still didn't get the fact

46
00:02:34,640 --> 00:02:38,610
 that she was actually experiencing impermanent suffering in

47
00:02:38,610 --> 00:02:40,400
 non-self quite clearly.

48
00:02:43,200 --> 00:02:47,200
 And so I asked her, I said, "Well, let's take a very simple

49
00:02:47,200 --> 00:02:48,720
 object. Is the rising and falling of the

50
00:02:48,720 --> 00:02:51,870
 stomach? That's the basic object. So is the rising and the

51
00:02:51,870 --> 00:02:54,000
 falling of the stomach always

52
00:02:54,000 --> 00:02:56,970
 constant?" And she said, "No, of course it's not. Is the

53
00:02:56,970 --> 00:02:58,880
 rising and the falling of the stomach

54
00:02:58,880 --> 00:03:03,380
 pleasant?" And she said, "No, no, actually it's quite

55
00:03:03,380 --> 00:03:05,440
 suffering a lot of the time.

56
00:03:05,440 --> 00:03:09,240
 And can you control it? Can you keep it stable? Can you

57
00:03:09,240 --> 00:03:11,200
 change it? When it goes out of control,

58
00:03:11,200 --> 00:03:13,010
 can you bring it back into control?" And she said, "No,

59
00:03:13,010 --> 00:03:15,840
 actually I can't." This is really clear

60
00:03:15,840 --> 00:03:19,530
 because it's something that we tell meditators to watch as

61
00:03:19,530 --> 00:03:21,760
 their basic object. So it's an easy

62
00:03:21,760 --> 00:03:25,910
 set of questions to ask. And right away she understood the

63
00:03:25,910 --> 00:03:27,680
 point that she was seeing quite

64
00:03:27,680 --> 00:03:30,980
 clearly impermanent suffering in non-self and this idea

65
00:03:30,980 --> 00:03:32,400
 that you have to somehow

66
00:03:34,720 --> 00:03:39,750
 consider them or look for them or ponder them or apply them

67
00:03:39,750 --> 00:03:42,160
 to your practice in some way is

68
00:03:42,160 --> 00:03:46,670
 totally erroneous, totally false. If you come to me and say

69
00:03:46,670 --> 00:03:48,720
 that your practice is not going well

70
00:03:48,720 --> 00:03:51,730
 and you can't get the hang of it and so on, then it's a

71
00:03:51,730 --> 00:03:54,320
 very good sign to me that you're practicing

72
00:03:54,320 --> 00:03:57,210
 quite well. You just don't realize it. If you come to me

73
00:03:57,210 --> 00:03:59,600
 and tell me, "Oh, meditation is so wonderful

74
00:03:59,600 --> 00:04:01,610
 and I'm just feeling peace and calm all the time and I

75
00:04:01,610 --> 00:04:03,760
 really have no problems," and so on, then I

76
00:04:03,760 --> 00:04:06,650
 think you're practicing samatha and I say, "You know, it's

77
00:04:06,650 --> 00:04:10,160
 not a problem," but it could be, in some

78
00:04:10,160 --> 00:04:13,610
 cases, could be a problem where you get stuck on something

79
00:04:13,610 --> 00:04:15,920
 and fixated on something and go flying

80
00:04:15,920 --> 00:04:23,400
 off into conceptual illusion and can actually go crazy

81
00:04:23,400 --> 00:04:26,720
 because you cling to things and follow

82
00:04:26,720 --> 00:04:29,570
 after things rather than seeing them as they are. So we

83
00:04:29,570 --> 00:04:31,760
 actually had a long talk about that as well

84
00:04:32,320 --> 00:04:36,980
 where I was explaining to her that she was asking about

85
00:04:36,980 --> 00:04:39,440
 images that she saw and she was explaining

86
00:04:39,440 --> 00:04:45,120
 that the problem with the image is just seeing really. That

87
00:04:45,120 --> 00:04:47,600
's how we look at it, but if you ask,

88
00:04:47,600 --> 00:04:50,270
 she was asking, "What is it?" I said, "Well, you know, in

89
00:04:50,270 --> 00:04:53,280
 the end we see it just as seeing," but

90
00:04:53,280 --> 00:04:57,130
 the point being that whatever it is, there is no answer

91
00:04:57,130 --> 00:05:00,400
 that you can't find an end answer to what

92
00:05:00,400 --> 00:05:04,590
 is it. When you see something special, when you investigate

93
00:05:04,590 --> 00:05:07,200
 it, you change it. This Heisenberg's

94
00:05:07,200 --> 00:05:10,990
 uncertainty principle actually works in reality, so when

95
00:05:10,990 --> 00:05:14,400
 you look at the object, you change it,

96
00:05:14,400 --> 00:05:17,770
 and so what happens is you investigate and it changes and

97
00:05:17,770 --> 00:05:19,920
 it proliferates. It becomes something

98
00:05:19,920 --> 00:05:22,810
 stronger because you're putting energy into it and because

99
00:05:22,810 --> 00:05:25,440
 it's conceptual, it can just change and

100
00:05:25,440 --> 00:05:28,260
 alter and eventually you develop habits based on it and

101
00:05:28,260 --> 00:05:30,320
 this is how meditators actually go crazy.

102
00:05:30,320 --> 00:05:34,090
 Because they get totally lost and caught up in something

103
00:05:34,090 --> 00:05:37,840
 that is conceptual. The point being that

104
00:05:37,840 --> 00:05:42,700
 if you feel like your practice is stuck or not going as it

105
00:05:42,700 --> 00:05:45,520
 should be, then there's a very good

106
00:05:45,520 --> 00:05:48,170
 chance that you're actually seeing impermanent suffering

107
00:05:48,170 --> 00:05:50,080
 and non-self and should just be patient

108
00:05:50,080 --> 00:05:52,580
 and you'll find yourself letting go as a result. When

109
00:05:52,580 --> 00:05:54,720
 things don't go the way you want, you will

110
00:05:54,720 --> 00:05:58,620
 generally tend to let go of them. You'll stop fussing about

111
00:05:58,620 --> 00:06:00,880
 them, stop worrying about them. You'll

112
00:06:00,880 --> 00:06:04,200
 start to learn that that's the nature of reality, that

113
00:06:04,200 --> 00:06:07,600
 things do go against your wishes, that things

114
00:06:07,600 --> 00:06:11,760
 in general are not under your control and that's the truth

115
00:06:11,760 --> 00:06:12,720
 of non-self.

