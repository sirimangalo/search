1
00:00:00,000 --> 00:00:05,600
 What is the importance of freedom and creativity in

2
00:00:05,600 --> 00:00:07,500
 becoming an arahant or even Buddha?

3
00:00:07,500 --> 00:00:11,830
 For lack of a better example, if you had a power which

4
00:00:11,830 --> 00:00:14,500
 could instantly transform anyone into a Buddha,

5
00:00:14,500 --> 00:00:18,500
 even without their consent, would you use it? If so, why?

6
00:00:18,500 --> 00:00:20,000
 And if not, why not?

7
00:00:20,000 --> 00:00:23,840
 We've covered all your bases, so we have to answer the

8
00:00:23,840 --> 00:00:27,000
 follow-up questions, which is fine.

9
00:00:27,000 --> 00:00:40,800
 It's curious because if what you're asking is, is there

10
00:00:40,800 --> 00:00:46,000
 something wrong with bringing a person enlightenment

11
00:00:46,000 --> 00:00:51,560
 without their, without allowing them their freedom and

12
00:00:51,560 --> 00:00:56,000
 creativity, the answer is of course no,

13
00:00:56,000 --> 00:00:59,750
 because whatever can, like hypothetically, whatever it is

14
00:00:59,750 --> 00:01:04,900
 that can bring a person enlightenment is the most important

15
00:01:04,900 --> 00:01:05,000
.

16
00:01:05,000 --> 00:01:07,900
 And it sounds like maybe, like a question that shouldn't

17
00:01:07,900 --> 00:01:11,000
 even be entertained because it's so hypothetical,

18
00:01:11,000 --> 00:01:13,480
 you know, could you instantly transform a person into a

19
00:01:13,480 --> 00:01:15,000
 Buddha? It's ridiculous, of course.

20
00:01:15,000 --> 00:01:18,610
 But there are interesting ramifications here. We're talking

21
00:01:18,610 --> 00:01:22,000
 about, many people do get this kind of feeling,

22
00:01:22,000 --> 00:01:28,340
 they don't enunciate it in such a blatant or explicit

23
00:01:28,340 --> 00:01:31,000
 manner as you've done.

24
00:01:31,000 --> 00:01:37,330
 But I think I get the feeling it's that following, what

25
00:01:37,330 --> 00:01:40,000
 good is it if it's just, you know, brainwashing

26
00:01:40,000 --> 00:01:43,650
 or following blindly after someone else and you practice

27
00:01:43,650 --> 00:01:45,710
 this and we're not even supposed to know all the theory

28
00:01:45,710 --> 00:01:46,000
 behind it,

29
00:01:46,000 --> 00:01:52,100
 you just practice and you become free from suffering. And

30
00:01:52,100 --> 00:01:54,000
 so people say, you know, I want some freedom and creativity

31
00:01:54,000 --> 00:01:58,510
 and I would rather be free and creative and have my own

32
00:01:58,510 --> 00:02:01,000
 interpretation and be unenlightened

33
00:02:01,000 --> 00:02:07,780
 than to be, to have to follow after someone else or to have

34
00:02:07,780 --> 00:02:18,000
 it imposed upon me, for example, and become enlightened.

35
00:02:18,000 --> 00:02:20,880
 I mean, even when you explicitly stated like that, it

36
00:02:20,880 --> 00:02:26,120
 sounds silly again, but there's this angst that people get

37
00:02:26,120 --> 00:02:28,000
 when they have to follow things.

38
00:02:28,000 --> 00:02:33,510
 And it's something that feels really good about what we're

39
00:02:33,510 --> 00:02:37,000
 doing and that we don't have this.

40
00:02:37,000 --> 00:02:40,000
 I mean, I don't agree with that at all. And I see it quite

41
00:02:40,000 --> 00:02:44,000
 clearly as a mistake that people make, this idea of having

42
00:02:44,000 --> 00:02:45,000
 their own interpretation.

43
00:02:45,000 --> 00:02:48,770
 It's called postmodernism. And I keep bringing this word up

44
00:02:48,770 --> 00:02:52,000
. I want everyone to look it up on the Wikipedia.

45
00:02:52,000 --> 00:02:55,880
 Look up postmodernism on Wikipedia because it describes

46
00:02:55,880 --> 00:02:58,000
 modern society and we don't realize it.

47
00:02:58,000 --> 00:03:01,410
 Most people have never heard this word and don't realize

48
00:03:01,410 --> 00:03:06,000
 that there is a word for it and it has been well documented

49
00:03:06,000 --> 00:03:10,000
 and described as a phenomenon.

50
00:03:10,000 --> 00:03:17,240
 Basically, simplistically, it's that all truth is relative

51
00:03:17,240 --> 00:03:22,000
 and everyone has to find their own truth.

52
00:03:22,000 --> 00:03:25,220
 Another way of saying is whatever is right for you is right

53
00:03:25,220 --> 00:03:27,000
 because it's right for you.

54
00:03:27,000 --> 00:03:33,300
 Whatever you believe is a belief that should be respected,

55
00:03:33,300 --> 00:03:35,000
 for example.

56
00:03:35,000 --> 00:03:37,540
 It's called postmodernism. You can look it up. New Age

57
00:03:37,540 --> 00:03:39,690
 could have something to do with it, but New Age has a lot

58
00:03:39,690 --> 00:03:41,000
 of other weird stuff about it.

59
00:03:41,000 --> 00:03:44,960
 New Age is the age of Aquarius, the idea that we're coming

60
00:03:44,960 --> 00:03:48,570
 into a new spiritual age and it has to do with astrology

61
00:03:48,570 --> 00:03:50,000
 and whatever.

62
00:03:50,000 --> 00:03:55,250
 So we don't even realize that this is a phenomenon. We've

63
00:03:55,250 --> 00:03:59,220
 come to accept, we in a general sense, society has come to

64
00:03:59,220 --> 00:04:03,000
 accept that that's the way it is and that's the truth.

65
00:04:03,000 --> 00:04:11,110
 We fight against people who would suggest that there is an

66
00:04:11,110 --> 00:04:17,400
 ultimate truth or a specific method or specific path that

67
00:04:17,400 --> 00:04:19,000
 everyone has to follow.

68
00:04:19,000 --> 00:04:23,240
 Just hearing this jars with most people, just the idea that

69
00:04:23,240 --> 00:04:25,000
 there is the only way.

70
00:04:25,000 --> 00:04:27,630
 We have this talk in the Buddha, "Eka yanoa yang bhikkawe m

71
00:04:27,630 --> 00:04:30,000
ago" and people say, "No, no, it doesn't mean only way."

72
00:04:30,000 --> 00:04:32,900
 Yes, technically it probably doesn't, but the Buddha said

73
00:04:32,900 --> 00:04:36,000
 several times, "There is only one way," and so on.

74
00:04:36,000 --> 00:04:38,000
 "Oh, but people don't want to hear this."

75
00:04:38,000 --> 00:04:41,420
 So if I told you, "I have the way to become enlightened and

76
00:04:41,420 --> 00:04:45,110
 only this way leads to enlightenment," that would be so

77
00:04:45,110 --> 00:04:47,000
 horrible for anyone to hear.

78
00:04:47,000 --> 00:04:52,640
 Most people would reject that simply on the basis that it

79
00:04:52,640 --> 00:04:57,730
 had this kind of a claim, which they feel goes against what

80
00:04:57,730 --> 00:04:59,000
 is true and right.

81
00:04:59,000 --> 00:05:02,000
 In fact, it just goes against this idea of postmodernism.

82
00:05:02,000 --> 00:05:05,390
 So it very well could be the fact that there is only one

83
00:05:05,390 --> 00:05:08,000
 path to become free from suffering.

84
00:05:08,000 --> 00:05:11,210
 There's no reason for us to think that's not possible. It

85
00:05:11,210 --> 00:05:13,000
 could be the case.

86
00:05:13,000 --> 00:05:15,420
 The problem is we've tried this path and that path and none

87
00:05:15,420 --> 00:05:18,060
 of them work, so we start to think, "Maybe there is no one

88
00:05:18,060 --> 00:05:20,000
 path and maybe it all is all..."

89
00:05:20,000 --> 00:05:23,720
 It comes out of giving up religion, seeing that the

90
00:05:23,720 --> 00:05:28,000
 religions that we followed aren't really satisfactory.

91
00:05:28,000 --> 00:05:34,850
 So I would reject that sort of a... I would at least

92
00:05:34,850 --> 00:05:38,000
 encourage people to question that feeling that arises,

93
00:05:38,000 --> 00:05:41,620
 the feeling of the need to be creative and have your own

94
00:05:41,620 --> 00:05:45,000
 interpretation and your own outlook on life.

95
00:05:45,000 --> 00:05:48,680
 It may be that your outlook on life is wrong and it may be

96
00:05:48,680 --> 00:05:50,000
 objectively wrong.

97
00:05:50,000 --> 00:05:53,000
 The idea of something being objectively wrong is possible.

98
00:05:53,000 --> 00:05:57,000
 And we have this argument. Philosophers have this argument,

99
00:05:57,000 --> 00:05:59,060
 "Is it possible for something to be objectively right and

100
00:05:59,060 --> 00:06:00,000
 objectively wrong?"

101
00:06:00,000 --> 00:06:02,820
 I say, "Yes, it is possible to be objectively wrong and

102
00:06:02,820 --> 00:06:06,050
 objectively right," because I would say suffering is

103
00:06:06,050 --> 00:06:07,000
 objective.

104
00:06:07,000 --> 00:06:12,010
 Something causes suffering. When a person says there is

105
00:06:12,010 --> 00:06:15,000
 suffering, that is objective.

106
00:06:15,000 --> 00:06:17,920
 The suffering itself is not subjective. They're not happy

107
00:06:17,920 --> 00:06:20,000
 about something. They're unhappy about something.

108
00:06:20,000 --> 00:06:25,170
 You can't be unhappy about something and not suffer based

109
00:06:25,170 --> 00:06:28,000
 on it. So suffering is objective.

110
00:06:28,000 --> 00:06:32,000
 And because it's objective, it has objective causes.

111
00:06:32,000 --> 00:06:36,240
 And because it has objective causes, there's an objective

112
00:06:36,240 --> 00:06:38,000
 way out of suffering.

113
00:06:38,000 --> 00:06:40,000
 So the Four Noble Truths are objective.

114
00:06:40,000 --> 00:06:43,160
 And the path which leads to the cessation of suffering is

115
00:06:43,160 --> 00:06:46,000
 an objective path and it's the eightfold path.

116
00:06:46,000 --> 00:06:50,000
 Or it could be the sevenfold path or the ninefold path or

117
00:06:50,000 --> 00:06:52,000
 whatever, however you want to explain it.

118
00:06:52,000 --> 00:06:55,000
 But it's based on the framework that the Buddha laid down.

119
00:06:55,000 --> 00:06:57,000
 That's the way out of suffering.

120
00:06:57,000 --> 00:07:00,580
 So that's the only way to become free from suffering. There

121
00:07:00,580 --> 00:07:03,000
 is no power to become instantly a Buddha.

122
00:07:03,000 --> 00:07:07,000
 But there is no way that someone could make up a new path

123
00:07:07,000 --> 00:07:09,560
 that is outside of the framework of the eightfold noble

124
00:07:09,560 --> 00:07:10,000
 path.

125
00:07:10,000 --> 00:07:13,000
 It's not possible. That is the objective truth.

126
00:07:13,000 --> 00:07:15,680
 Now, you can believe me or not. I don't mind if you disbel

127
00:07:15,680 --> 00:07:17,000
ieve me, but that is the claim.

128
00:07:17,000 --> 00:07:19,980
 And it's worth, if anyone is interested, they're welcome to

129
00:07:19,980 --> 00:07:21,000
 check that out. Test it out.

130
00:07:21,000 --> 00:07:25,060
 Read about the eightfold noble path. Find a teacher who

131
00:07:25,060 --> 00:07:29,000
 teaches it and see for yourself whether that's the truth.

132
00:07:29,000 --> 00:07:31,740
 What you should see is as you practice it, you come to

133
00:07:31,740 --> 00:07:36,000
 understand suffering and you start to let go of suffering,

134
00:07:36,000 --> 00:07:42,140
 which means you let go, craving disappears or desire

135
00:07:42,140 --> 00:07:44,000
 disappears.

136
00:07:44,000 --> 00:07:48,560
 And you should find that as desire disappears, suffering

137
00:07:48,560 --> 00:07:52,520
 ceases to the point where all suffering ceases and one

138
00:07:52,520 --> 00:07:54,000
 enters into nirvana.

139
00:07:54,000 --> 00:07:57,500
 That's what one should see for oneself. That's the theory

140
00:07:57,500 --> 00:07:58,000
 here.

141
00:07:58,000 --> 00:08:02,000
 So I hope that is related to your question.

142
00:08:02,000 --> 00:08:04,850
 And that is really what you were talking about, this sort

143
00:08:04,850 --> 00:08:10,000
 of feeling that people have of the need to be individual.

144
00:08:10,000 --> 00:08:13,260
 It reminds me of before I became a monk, it was big in the

145
00:08:13,260 --> 00:08:16,000
 90s, I think, or even the 80s.

146
00:08:16,000 --> 00:08:21,870
 The 80s was big because of the pink and all of the everyone

147
00:08:21,870 --> 00:08:27,000
 being themselves and like Michael Jackson and all that.

148
00:08:27,000 --> 00:08:29,860
 But I guess it's probably still the case. I haven't been

149
00:08:29,860 --> 00:08:32,000
 very much in touch with society.

150
00:08:32,000 --> 00:08:39,180
 The whole being an individual, being yourself and so on. So

151
00:08:39,180 --> 00:08:42,940
 having your own ideas, it just turned into a bit of a

152
00:08:42,940 --> 00:08:45,000
 monster actually.

153
00:08:45,000 --> 00:08:48,220
 Where no one believes anything and is willing to follow

154
00:08:48,220 --> 00:08:52,740
 anything. Orthodox became, it's funny I was talking about

155
00:08:52,740 --> 00:08:57,490
 this and what's happened is being Orthodox now is

156
00:08:57,490 --> 00:08:59,000
 rebellious.

157
00:08:59,000 --> 00:09:01,990
 You get into so much flak, just as before you would get

158
00:09:01,990 --> 00:09:04,000
 into flak for being unorthodox.

159
00:09:04,000 --> 00:09:07,520
 And that was the whole punk movement and so on. Oh, they

160
00:09:07,520 --> 00:09:09,000
 were vilified and so on.

161
00:09:09,000 --> 00:09:11,010
 But now it's the opposite. As soon as you try to be

162
00:09:11,010 --> 00:09:14,000
 Orthodox and say, no, no, let's go by what the book says,

163
00:09:14,000 --> 00:09:18,000
 people will blast you, people will tear you down.

164
00:09:18,000 --> 00:09:23,220
 That's the funny thing. And it makes me feel good. I mean,

165
00:09:23,220 --> 00:09:25,000
 I used to be the big rebel. I was a punk.

166
00:09:25,000 --> 00:09:29,520
 Now we're still rebellious, but we're rebellious. I think

167
00:09:29,520 --> 00:09:31,000
 it's called avant-garde.

168
00:09:31,000 --> 00:09:35,590
 Once everyone becomes avant-garde or once everyone becomes

169
00:09:35,590 --> 00:09:39,990
 extreme, then you have to go back and take the Orthodox,

170
00:09:39,990 --> 00:09:41,000
 something like that.

171
00:09:41,000 --> 00:09:44,810
 No, that's not true. Whether it's Orthodox or not, the

172
00:09:44,810 --> 00:09:49,000
 truth is the truth. But there is an objective truth.

173
00:09:49,000 --> 00:09:51,740
 It would be horrible if there wasn't an objective truth.

174
00:09:51,740 --> 00:09:54,590
 Lucky for us, there is an objective truth. And therefore

175
00:09:54,590 --> 00:09:56,000
 there is a way out of suffering.

176
00:09:56,000 --> 00:09:58,610
 The Buddha said this, "If there wasn't a way out of

177
00:09:58,610 --> 00:10:01,410
 suffering, I wouldn't enjoin you to practice for the

178
00:10:01,410 --> 00:10:03,000
 cessation of suffering.

179
00:10:03,000 --> 00:10:06,970
 But since there is a way out of suffering, therefore I en

180
00:10:06,970 --> 00:10:10,770
join you and treat you to practice for the cessation of

181
00:10:10,770 --> 00:10:12,000
 suffering."

182
00:10:12,000 --> 00:10:14,000
 I hope that helps.

