1
00:00:00,000 --> 00:00:06,550
 Hello, today I'd like to take some time to answer a

2
00:00:06,550 --> 00:00:10,400
 question which is asked quite often

3
00:00:10,400 --> 00:00:17,460
 by meditators who have undertaken practice under my

4
00:00:17,460 --> 00:00:21,920
 guidance or also from meditators,

5
00:00:21,920 --> 00:00:26,580
 people who have come to be interested in meditation via the

6
00:00:26,580 --> 00:00:29,620
 internet and people who have watched

7
00:00:29,620 --> 00:00:33,480
 my videos on how to meditate and ask.

8
00:00:33,480 --> 00:00:38,790
 They've asked it in many different forms but in the end it

9
00:00:38,790 --> 00:00:41,120
 comes down to, I think can be

10
00:00:41,120 --> 00:00:47,530
 all summed up under one question and this is, are all med

11
00:00:47,530 --> 00:00:50,280
itations the same or is the

12
00:00:50,280 --> 00:00:53,900
 meditation which I teach something which can be used with

13
00:00:53,900 --> 00:00:56,320
 other meditations, is it congruous

14
00:00:56,320 --> 00:01:02,010
 with other meditative styles, is it something which can be

15
00:01:02,010 --> 00:01:05,320
 applied to other meditation techniques

16
00:01:05,320 --> 00:01:12,330
 and thus the question are all meditations, do they all fall

17
00:01:12,330 --> 00:01:15,120
 along the same line?

18
00:01:15,120 --> 00:01:20,000
 And so I would categorize meditation into two broad

19
00:01:20,000 --> 00:01:23,360
 categories and the first category

20
00:01:23,360 --> 00:01:28,000
 is what we call tranquility meditation.

21
00:01:28,000 --> 00:01:33,580
 It's meditation which helps the mind to become peaceful and

22
00:01:33,580 --> 00:01:38,320
 calm without any understanding,

23
00:01:38,320 --> 00:01:41,800
 necessarily understanding about ourselves or the world

24
00:01:41,800 --> 00:01:42,640
 around us.

25
00:01:42,640 --> 00:01:48,280
 It's simply any method which brings the mind to feel

26
00:01:48,280 --> 00:01:50,640
 peaceful and calm.

27
00:01:50,640 --> 00:01:53,720
 It could be reading the bible or it could be reciting the

28
00:01:53,720 --> 00:01:55,320
 name of Jesus or the name of

29
00:01:55,320 --> 00:01:59,780
 Buddha, it could be looking at a candle flame or reciting

30
00:01:59,780 --> 00:02:02,120
 the mantra om or any number of

31
00:02:02,120 --> 00:02:05,290
 different meditations which exist out there today which we

32
00:02:05,290 --> 00:02:06,880
 see in the various religious

33
00:02:06,880 --> 00:02:12,240
 traditions and outside of religious tradition as well.

34
00:02:12,240 --> 00:02:16,230
 The other type of meditation is called vipassana meditation

35
00:02:16,230 --> 00:02:19,180
 or insight meditation, means meditation

36
00:02:19,180 --> 00:02:22,330
 which leads the person who practices it to see clearly

37
00:02:22,330 --> 00:02:24,280
 about the things inside them and

38
00:02:24,280 --> 00:02:26,720
 the things in the world around them.

39
00:02:26,720 --> 00:02:30,240
 And it doesn't necessarily lead the person to feel peaceful

40
00:02:30,240 --> 00:02:31,720
 and calm during the time

41
00:02:31,720 --> 00:02:33,120
 that they're practicing it.

42
00:02:33,120 --> 00:02:37,040
 In fact people who are practicing to see clearly often have

43
00:02:37,040 --> 00:02:39,200
 to touch upon things which make

44
00:02:39,200 --> 00:02:43,510
 them feel uncomfortable, things which they would

45
00:02:43,510 --> 00:02:46,800
 emotionally rather not touch, which

46
00:02:46,800 --> 00:02:49,950
 their feeling tells them is something that they should not

47
00:02:49,950 --> 00:02:51,280
 touch, something that they

48
00:02:51,280 --> 00:02:54,200
 should avoid and should run away from.

49
00:02:54,200 --> 00:02:57,040
 But they can see over time that by running away from these

50
00:02:57,040 --> 00:02:58,280
 things it only gets worse

51
00:02:58,280 --> 00:03:01,320
 and the problem is never actually healed.

52
00:03:01,320 --> 00:03:05,180
 So when they reach this point they decide to undertake

53
00:03:05,180 --> 00:03:07,720
 insight meditation and this is

54
00:03:07,720 --> 00:03:10,900
 meditation which leads the person to see clearly about

55
00:03:10,900 --> 00:03:12,760
 oneself and about the world around

56
00:03:12,760 --> 00:03:15,080
 oneself.

57
00:03:15,080 --> 00:03:17,950
 So how are these two different meditations to be

58
00:03:17,950 --> 00:03:19,080
 distinguished?

59
00:03:19,080 --> 00:03:22,540
 The first way to distinguish these two types of meditation

60
00:03:22,540 --> 00:03:24,720
 is that by the object which

61
00:03:24,720 --> 00:03:26,280
 they take.

62
00:03:26,280 --> 00:03:29,380
 Now I said about insight meditation that it gives

63
00:03:29,380 --> 00:03:31,880
 understanding about ourselves and the

64
00:03:31,880 --> 00:03:33,400
 world around us.

65
00:03:33,400 --> 00:03:37,560
 So the object for insight meditation has to be ourselves

66
00:03:37,560 --> 00:03:39,960
 and the world around us, those

67
00:03:39,960 --> 00:03:42,940
 things which exist for real inside of ourselves and those

68
00:03:42,940 --> 00:03:44,600
 things which exist for real in the

69
00:03:44,600 --> 00:03:46,200
 world around us.

70
00:03:46,200 --> 00:03:48,960
 And they also have to be real things.

71
00:03:48,960 --> 00:03:55,530
 For instance we can't focus on our, in the human body we

72
00:03:55,530 --> 00:03:58,880
 can't focus on our swimming

73
00:03:58,880 --> 00:04:02,280
 bladder which we don't have or our gills or something in

74
00:04:02,280 --> 00:04:04,240
 the world around us we couldn't

75
00:04:04,240 --> 00:04:09,160
 focus on a horned rabbit, a rabbit with horns or something.

76
00:04:09,160 --> 00:04:12,300
 It has to be something which is real.

77
00:04:12,300 --> 00:04:15,310
 And when it all comes down to it we focus on the things

78
00:04:15,310 --> 00:04:17,080
 which are ultimately real.

79
00:04:17,080 --> 00:04:19,700
 If we want to get down to what is ultimately good and

80
00:04:19,700 --> 00:04:21,640
 ultimately bad and understand the

81
00:04:21,640 --> 00:04:25,050
 difference and come to do away with the things which are no

82
00:04:25,050 --> 00:04:27,160
 good inside of us, with the causes

83
00:04:27,160 --> 00:04:30,620
 for suffering for us, we have to focus on what is

84
00:04:30,620 --> 00:04:32,120
 ultimately real.

85
00:04:32,120 --> 00:04:35,370
 So even all of our organs or so on we have to instead focus

86
00:04:35,370 --> 00:04:37,040
 on the senses, the things

87
00:04:37,040 --> 00:04:40,070
 which we can experience as we experience them in the

88
00:04:40,070 --> 00:04:41,280
 present moment.

89
00:04:41,280 --> 00:04:43,840
 This is the object of insight meditation.

90
00:04:43,840 --> 00:04:48,340
 In tranquility meditation the distinction is that tranqu

91
00:04:48,340 --> 00:04:51,000
ility meditation takes a concept

92
00:04:51,000 --> 00:04:53,240
 as the object.

93
00:04:53,240 --> 00:04:56,520
 In tranquility meditation you can't take an ultimate

94
00:04:56,520 --> 00:04:58,560
 reality as an object for the simple

95
00:04:58,560 --> 00:05:00,920
 reason that it won't make you calm.

96
00:05:00,920 --> 00:05:03,040
 And this is because three things.

97
00:05:03,040 --> 00:05:04,040
 One it is impermanent.

98
00:05:04,040 --> 00:05:05,720
 It's changing all the time.

99
00:05:05,720 --> 00:05:08,360
 If you're focusing on something that's changing you don't

100
00:05:08,360 --> 00:05:10,040
 feel peaceful and calm right away.

101
00:05:10,040 --> 00:05:13,800
 You tend to be upset and try to look instead for something

102
00:05:13,800 --> 00:05:15,800
 which is steady which is sure.

103
00:05:15,800 --> 00:05:18,780
 And unfortunately there's nothing in this world which is

104
00:05:18,780 --> 00:05:20,320
 steady and which is sure.

105
00:05:20,320 --> 00:05:22,120
 So we pick a concept instead.

106
00:05:22,120 --> 00:05:26,570
 We pick a person or a deity or an idol or we pick a

107
00:05:26,570 --> 00:05:30,240
 conceptual object which we create in

108
00:05:30,240 --> 00:05:33,080
 our mind.

109
00:05:33,080 --> 00:05:36,650
 The next thing is that when we take the world around us as

110
00:05:36,650 --> 00:05:38,640
 our object it is unsatisfying.

111
00:05:38,640 --> 00:05:41,050
 And if we're going to make the mind peaceful and calm we

112
00:05:41,050 --> 00:05:42,240
 have to find something which is

113
00:05:42,240 --> 00:05:44,000
 satisfying.

114
00:05:44,000 --> 00:05:46,220
 And unfortunately in the world in ourselves and in the

115
00:05:46,220 --> 00:05:47,600
 world around us there's nothing

116
00:05:47,600 --> 00:05:51,200
 real which is completely satisfying because it's imper

117
00:05:51,200 --> 00:05:51,960
manent.

118
00:05:51,960 --> 00:05:54,920
 There's nothing which we can see last.

119
00:05:54,920 --> 00:05:55,920
 Everything is changing.

120
00:05:55,920 --> 00:05:58,480
 When we see things it comes and goes.

121
00:05:58,480 --> 00:06:02,940
 When we hear things it comes they come and go and so on and

122
00:06:02,940 --> 00:06:03,760
 so on.

123
00:06:03,760 --> 00:06:06,870
 And finally we can't take things which are real as the

124
00:06:06,870 --> 00:06:09,240
 object in the practice of tranquility

125
00:06:09,240 --> 00:06:13,330
 meditation because things which are real are not under our

126
00:06:13,330 --> 00:06:14,480
 control.

127
00:06:14,480 --> 00:06:16,340
 And if we're going to feel peaceful and calm we have to

128
00:06:16,340 --> 00:06:17,520
 find things which are under our

129
00:06:17,520 --> 00:06:18,520
 control.

130
00:06:18,520 --> 00:06:21,480
 Now unfortunately there's nothing which is real in this

131
00:06:21,480 --> 00:06:22,280
 world.

132
00:06:22,280 --> 00:06:25,320
 In ourselves or in the world around us which is under our

133
00:06:25,320 --> 00:06:26,080
 control.

134
00:06:26,080 --> 00:06:29,500
 We can't force our body to be the way we wanted or our mind

135
00:06:29,500 --> 00:06:31,160
 to be the way we wanted.

136
00:06:31,160 --> 00:06:34,410
 We certainly can't force the world around us to be the way

137
00:06:34,410 --> 00:06:35,280
 we wanted.

138
00:06:35,280 --> 00:06:38,800
 And so if we're going to simply feel peace and calm sit

139
00:06:38,800 --> 00:06:40,880
 down and find some space where

140
00:06:40,880 --> 00:06:44,650
 we feel peaceful and calm we have to take a conceptual

141
00:06:44,650 --> 00:06:47,280
 object as our meditation object.

142
00:06:47,280 --> 00:06:50,380
 Now what I teach and what I've been trying to teach here is

143
00:06:50,380 --> 00:06:52,040
 maybe unfortunately for some

144
00:06:52,040 --> 00:06:57,080
 people insight meditation is not tranquility meditation.

145
00:06:57,080 --> 00:07:00,520
 So if for those people looking for tranquility meditation a

146
00:07:00,520 --> 00:07:02,240
 way to just feel peaceful and

147
00:07:02,240 --> 00:07:06,040
 calm during the time that you're sitting and temporarily

148
00:07:06,040 --> 00:07:07,840
 and something which you have to

149
00:07:07,840 --> 00:07:11,130
 leave behind when you stand up and go back to your life

150
00:07:11,130 --> 00:07:13,000
 then this is probably not for

151
00:07:13,000 --> 00:07:14,000
 you.

152
00:07:14,000 --> 00:07:17,060
 But for people looking to come to understand themselves and

153
00:07:17,060 --> 00:07:18,660
 the world around them and come

154
00:07:18,660 --> 00:07:21,640
 to ultimately be free from stress and worry and to

155
00:07:21,640 --> 00:07:23,920
 ultimately find real peace and real

156
00:07:23,920 --> 00:07:28,680
 happiness both in meditation and in their normal everyday

157
00:07:28,680 --> 00:07:31,520
 life then perhaps insight meditation

158
00:07:31,520 --> 00:07:32,680
 is for them.

159
00:07:32,680 --> 00:07:36,250
 The second way to discern the difference between these two

160
00:07:36,250 --> 00:07:38,600
 types of meditation is in the results.

161
00:07:38,600 --> 00:07:42,060
 So tranquility meditation you will feel peaceful and calm

162
00:07:42,060 --> 00:07:44,560
 temporarily and when you stop practicing

163
00:07:44,560 --> 00:07:47,840
 you'll feel the effects fade away and you'll go back to

164
00:07:47,840 --> 00:07:49,880
 your everyday normal life without

165
00:07:49,880 --> 00:07:55,140
 really having gained any verifiable benefit until you go

166
00:07:55,140 --> 00:07:57,480
 back to practice again.

167
00:07:57,480 --> 00:08:01,010
 And so much you practice so much you keep your state of

168
00:08:01,010 --> 00:08:02,320
 peace and calm.

169
00:08:02,320 --> 00:08:05,140
 With insight meditation the result is understanding,

170
00:08:05,140 --> 00:08:07,620
 understanding about things which are suffering

171
00:08:07,620 --> 00:08:11,490
 and the ability to stay with things which are a cause for

172
00:08:11,490 --> 00:08:13,680
 suffering and stress and upset

173
00:08:13,680 --> 00:08:16,930
 so that when these things arise in ourselves or in the

174
00:08:16,930 --> 00:08:19,000
 world around us in our daily life

175
00:08:19,000 --> 00:08:20,000
 we're able to deal with them.

176
00:08:20,000 --> 00:08:24,160
 We're able to deal with them in a rational, a peaceful, a

177
00:08:24,160 --> 00:08:26,440
 calm and a patient way without

178
00:08:26,440 --> 00:08:30,300
 having to sit down on a meditation mat because the object

179
00:08:30,300 --> 00:08:32,760
 of our meditation is everyday real

180
00:08:32,760 --> 00:08:36,930
 life and so we come to understand and we come to become

181
00:08:36,930 --> 00:08:39,920
 comfortable even with excruciating

182
00:08:39,920 --> 00:08:43,600
 pain or intensely difficult situation.

183
00:08:43,600 --> 00:08:48,650
 This is a second way to distinguish between these two types

184
00:08:48,650 --> 00:08:50,280
 of meditation.

185
00:08:50,280 --> 00:08:54,960
 So in brief I'd like to suggest this, propose this as an

186
00:08:54,960 --> 00:08:57,760
 answer to the question first of

187
00:08:57,760 --> 00:09:02,000
 all what is it that I'm teaching when I say how to meditate

188
00:09:02,000 --> 00:09:04,000
 and how this might differ

189
00:09:04,000 --> 00:09:07,460
 or how to distinguish this type of meditation from other

190
00:09:07,460 --> 00:09:09,200
 types of meditation which you might

191
00:09:09,200 --> 00:09:13,600
 find in the world today.

192
00:09:13,600 --> 00:09:17,620
 Some questions about other types of meditation and I'd have

193
00:09:17,620 --> 00:09:19,800
 to say I'm not up on very many

194
00:09:19,800 --> 00:09:21,400
 different meditation techniques.

195
00:09:21,400 --> 00:09:24,890
 If you want to explain to me clearly what a certain

196
00:09:24,890 --> 00:09:27,280
 meditation technique is and ask

197
00:09:27,280 --> 00:09:31,780
 me how does this fit into the Buddhist scheme of what is

198
00:09:31,780 --> 00:09:34,520
 this type of meditation and that

199
00:09:34,520 --> 00:09:39,120
 type of meditation then I could try to answer.

200
00:09:39,120 --> 00:09:42,920
 And you're certainly welcome to contact me on YouTube or on

201
00:09:42,920 --> 00:09:44,520
 my web blog or via email

202
00:09:44,520 --> 00:09:47,830
 if you have my email or so on and so on and I'd be happy to

203
00:09:47,830 --> 00:09:48,600
 answer.

204
00:09:48,600 --> 00:09:52,240
 If you have any other questions please ask, I'm happy to do

205
00:09:52,240 --> 00:09:53,360
 these videos.

206
00:09:53,360 --> 00:09:56,540
 Thank you for the great responses and I hope to hear from

207
00:09:56,540 --> 00:09:57,280
 you again.

208
00:09:57,280 --> 00:09:58,000
 All the best.

209
00:09:58,000 --> 00:09:59,000
 Thank you.

