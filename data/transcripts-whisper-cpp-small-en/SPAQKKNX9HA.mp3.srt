1
00:00:00,000 --> 00:00:09,000
 Good evening everyone. I'm broadcasting live May 10th.

2
00:00:09,000 --> 00:00:23,000
 Tonight's quote is on the Buddha, the Dhamma and the Sangha

3
00:00:23,000 --> 00:00:23,000
.

4
00:00:28,000 --> 00:00:33,310
 It's from the Terigatha, actually, the verses of the elder

5
00:00:33,310 --> 00:00:34,000
 monks.

6
00:00:34,000 --> 00:00:46,500
 We have recorded verses that were spoken by various arah

7
00:00:46,500 --> 00:00:47,000
ants

8
00:00:47,000 --> 00:00:52,000
 as poetry and reflection on their attainments.

9
00:00:52,000 --> 00:01:00,000
 And we have stories behind each one of these.

10
00:01:00,000 --> 00:01:05,000
 So this one is from a monk called Tehkichakari.

11
00:01:05,000 --> 00:01:10,000
 Tehkichakari.

12
00:01:10,000 --> 00:01:14,000
 One who does...

13
00:01:16,000 --> 00:01:23,000
 Tehkichakari. Let's see if we can look at this.

14
00:01:23,000 --> 00:01:30,000
 He was the son of a Brahmin subhandhu, subhandhu,

15
00:01:30,000 --> 00:01:34,000
 so called because he brought safety into the world.

16
00:01:34,000 --> 00:01:37,960
 He was brought safely into the world with the aid of

17
00:01:37,960 --> 00:01:39,000
 physicians.

18
00:01:39,000 --> 00:01:43,000
 I don't understand the word.

19
00:01:44,000 --> 00:01:51,890
 When he was grown up, his father incurred the jealousy and

20
00:01:51,890 --> 00:01:56,000
 suspicion of a minister,

21
00:01:56,000 --> 00:02:02,000
 Chanaka, Chanaka, and Chandaguta, who had him throw...

22
00:02:02,000 --> 00:02:05,000
 His father was put in prison.

23
00:02:05,000 --> 00:02:09,000
 And so the son, Tehkichakari, in his fright fled,

24
00:02:10,000 --> 00:02:13,640
 and taking refuge with a forest dwelling monk, entered the

25
00:02:13,640 --> 00:02:14,000
 order,

26
00:02:14,000 --> 00:02:18,550
 and dwelt in the open air, never sleeping in heedless of

27
00:02:18,550 --> 00:02:20,000
 heat and cold.

28
00:02:20,000 --> 00:02:35,000
 Many are the reasons why people become monks.

29
00:02:38,000 --> 00:02:41,000
 Nagasena says...

30
00:02:41,000 --> 00:02:45,430
 He's... Melinda asks... King Melinda asks him, "Why did you

31
00:02:45,430 --> 00:02:46,000
 become a monk?"

32
00:02:46,000 --> 00:02:49,000
 And he says, "Did you become a monk to become an Arah?"

33
00:02:49,000 --> 00:02:52,370
 He says, "Oh, I was young." He kind of embarrassed the way

34
00:02:52,370 --> 00:02:53,000
 he says it.

35
00:02:53,000 --> 00:02:59,000
 Some people become monks for the wrong reason.

36
00:02:59,000 --> 00:03:04,750
 Sometimes for the wrong reason, but then find the right

37
00:03:04,750 --> 00:03:07,000
 reason after they've ordained.

38
00:03:09,000 --> 00:03:13,190
 But it's an important part of society, I think, to have

39
00:03:13,190 --> 00:03:15,000
 this...

40
00:03:15,000 --> 00:03:28,000
 This institution, this option for people who are...

41
00:03:32,000 --> 00:03:36,420
 That keen, who are at their stage, at a point in their

42
00:03:36,420 --> 00:03:40,000
 lives, or in their journey in samsara.

43
00:03:40,000 --> 00:03:46,000
 That they are able and willing.

44
00:03:46,000 --> 00:03:54,000
 That they are turned off by the world.

45
00:03:54,000 --> 00:03:59,000
 Having this ability to...

46
00:04:01,000 --> 00:04:06,100
 Choose to dedicate yourself to meditation and study and

47
00:04:06,100 --> 00:04:09,000
 teaching of...

48
00:04:09,000 --> 00:04:14,000
 spiritual truths and...

49
00:04:14,000 --> 00:04:17,000
 the path to enlightenment and so on.

50
00:04:17,000 --> 00:18:38,240
 I mean, this is... Not only are these sort of people

51
00:18:38,240 --> 00:18:38,240
 important, people who have the time and the energy and the

52
00:18:38,240 --> 00:04:28,000
 knowledge and the practice to teach,

53
00:04:28,000 --> 00:04:33,000
 but also for those who are seeking it.

54
00:04:33,000 --> 00:04:37,000
 Those who can't find a place in the world.

55
00:04:37,000 --> 00:04:43,000
 Who have an earnest wish to become a...

56
00:04:43,000 --> 00:04:49,000
 spiritual person and to live a reclusive life.

57
00:04:55,000 --> 00:04:57,940
 Anyway, so this monk's verses are about the Buddha, the D

58
00:04:57,940 --> 00:05:00,000
hammana Sangha. He says,

59
00:05:00,000 --> 00:05:06,000
 Buddha mapamayam anusarapasan...

60
00:05:06,000 --> 00:05:09,000
 One should...

61
00:05:09,000 --> 00:05:17,060
 reflect with a good heart, with a pure heart on the Buddha

62
00:05:17,060 --> 00:05:19,000
 who is immeasurable.

63
00:05:23,000 --> 00:05:29,000
 Bitya putasari roho hi si satatamudago.

64
00:05:29,000 --> 00:05:35,000
 One's sari ra is one's whole being putas.

65
00:05:35,000 --> 00:05:42,000
 Puta I think means like permeated, right?

66
00:05:42,000 --> 00:05:47,000
 Permeated with rapture, with...

67
00:05:48,000 --> 00:05:52,230
 exaltation, with this excitement, with this energy, this

68
00:05:52,230 --> 00:05:53,000
 rapture.

69
00:05:53,000 --> 00:05:58,000
 Ho hi si satatamudago. One will always be...

70
00:05:58,000 --> 00:06:02,000
 always be uplifted, will be constantly uplifted.

71
00:06:02,000 --> 00:06:06,000
 The same with the Dhammana Sangha. These are...

72
00:06:06,000 --> 00:06:08,520
 just reflecting on the Buddha, the Dhammana Sangha, just

73
00:06:08,520 --> 00:06:10,000
 thinking about these things.

74
00:06:10,000 --> 00:06:16,000
 Just reminiscing or revering and...

75
00:06:17,000 --> 00:06:21,000
 just that, not even practicing it. They're so powerful that

76
00:06:21,000 --> 00:06:22,000
 even worshipping them...

77
00:06:22,000 --> 00:06:26,000
 is of great benefit.

78
00:06:26,000 --> 00:06:32,000
 It makes you uplifted, undago, undago.

79
00:06:32,000 --> 00:06:37,000
 Makes you uplifted satatam, continuously.

80
00:06:37,000 --> 00:06:41,240
 So if you keep the Buddha, the Dhammana Sangha in mind, you

81
00:06:41,240 --> 00:06:43,000
 will never be depressed.

82
00:06:45,000 --> 00:06:50,010
 I told this story several times about this woman in the US

83
00:06:50,010 --> 00:06:51,000
 who...

84
00:06:51,000 --> 00:06:55,000
 was in a very bad situation.

85
00:06:55,000 --> 00:06:59,000
 So she was doing some meditation, but...

86
00:06:59,000 --> 00:07:02,220
 she was basically trapped in somebody else's house. Not

87
00:07:02,220 --> 00:07:04,000
 physically, but...

88
00:07:04,000 --> 00:07:07,600
 for all intents and purposes, quite trapped where she was

89
00:07:07,600 --> 00:07:10,000
 in some bad situation.

90
00:07:10,000 --> 00:07:15,690
 Sort of very fundamentalist, non-Buddhist, religious people

91
00:07:15,690 --> 00:07:16,000
.

92
00:07:16,000 --> 00:07:21,000
 And so I told her also to recite...

93
00:07:21,000 --> 00:07:26,260
 some chanting to the Buddha in the Dhammana Sangha, just

94
00:07:26,260 --> 00:07:27,000
 short chants.

95
00:07:27,000 --> 00:07:29,860
 And she had a Buddha image and she was doing this. And then

96
00:07:29,860 --> 00:07:32,000
 the family confiscated the Buddha image.

97
00:07:32,000 --> 00:07:35,430
 But the power of it, they were going to destroy this Buddha

98
00:07:35,430 --> 00:07:38,000
 image, but the power of her...

99
00:07:39,000 --> 00:07:45,000
 practice. And her devotion to the Buddha, coupled with

100
00:07:45,000 --> 00:07:48,000
 their hatred and vilification of the Buddha...

101
00:07:48,000 --> 00:07:51,700
 she had to find someone to take this Buddha image away or

102
00:07:51,700 --> 00:07:54,000
 they were going to destroy it.

103
00:07:54,000 --> 00:07:56,650
 And so she found someone to take it. And when they came to

104
00:07:56,650 --> 00:07:59,000
 take it, they talked to her and...

105
00:07:59,000 --> 00:08:05,120
 out of mutual sort of appreciation, they found a job for

106
00:08:05,120 --> 00:08:07,000
 her and she moved.

107
00:08:08,000 --> 00:08:10,380
 All because of this Buddha image and because of her

108
00:08:10,380 --> 00:08:14,000
 practice, she was able to by chance meet these people...

109
00:08:14,000 --> 00:08:19,000
 who were also, I guess, Buddhist or at least...

110
00:08:19,000 --> 00:08:24,000
 amicable towards Buddhism.

111
00:08:24,000 --> 00:08:31,100
 And was able to leave the house right away. It was quite an

112
00:08:31,100 --> 00:08:32,000
 inspiring story.

113
00:08:32,000 --> 00:08:36,000
 All because of her chanting. Indirectly, of course.

114
00:08:37,000 --> 00:08:41,000
 Oil and water can't mix good and evil.

115
00:08:41,000 --> 00:08:44,000
 A hard time staying in the same house.

116
00:08:44,000 --> 00:08:52,240
 So it's important to... this is one of the protective med

117
00:08:52,240 --> 00:08:54,000
itations.

118
00:08:54,000 --> 00:08:57,000
 Meditating on the Buddha, the Dhamma and the Sangha.

119
00:08:57,000 --> 00:08:59,000
 Protects you, keeps you safe.

120
00:08:59,000 --> 00:09:05,000
 Keeps your mind strong and vibrant, ecstatic.

121
00:09:06,000 --> 00:09:08,000
 Energetic.

122
00:09:08,000 --> 00:09:12,000
 Something worth doing.

123
00:09:12,000 --> 00:09:15,130
 Worth reading about the Buddha, reading about his life.

124
00:09:15,130 --> 00:09:18,000
 Worth going to India to see where the Buddha lived.

125
00:09:18,000 --> 00:09:22,070
 All these things to make the Buddha a part of your life and

126
00:09:22,070 --> 00:09:25,000
 a part of your conscious experience.

127
00:09:25,000 --> 00:09:33,000
 So anyway, not too much to say about that.

128
00:09:35,000 --> 00:09:37,000
 I have questions tonight.

129
00:09:37,000 --> 00:09:42,000
 I see a bunch of comments.

130
00:09:42,000 --> 00:09:51,000
 I'm going to skip questions that don't have the Q in front.

131
00:09:51,000 --> 00:09:54,290
 Because otherwise, that makes it so much easier to skin

132
00:09:54,290 --> 00:09:55,000
 through.

133
00:09:55,000 --> 00:09:58,490
 So they don't have the special Q in front. Special question

134
00:09:58,490 --> 00:09:59,000
 mark.

135
00:09:59,000 --> 00:10:03,000
 This one is. Wow, I won't call it a Q in front.

136
00:10:03,000 --> 00:10:07,000
 And Bobo says I said his question was stupid.

137
00:10:07,000 --> 00:10:09,000
 I didn't really say that today.

138
00:10:09,000 --> 00:10:12,000
 Maybe I joked about it or something.

139
00:10:12,000 --> 00:10:17,170
 Do you ever read great works from later Buddhist masters

140
00:10:17,170 --> 00:10:19,000
 like Nagarjuna?

141
00:10:19,000 --> 00:10:22,000
 No, I don't. Not so much.

142
00:10:22,000 --> 00:10:25,000
 But I have heard good things about Nagarjuna, so...

143
00:10:28,000 --> 00:10:30,000
 I wouldn't be against reading it.

144
00:10:30,000 --> 00:10:32,510
 I mean, I think that's the sort of Mahayana stuff that I'd

145
00:10:32,510 --> 00:10:34,000
 actually be interested in.

146
00:10:34,000 --> 00:10:40,770
 As opposed to East Asian Mahayana stuff, which is quite unp

147
00:10:40,770 --> 00:10:44,000
alatable to me.

148
00:10:44,000 --> 00:10:48,610
 I imagine I'd probably have some critical, be fairly

149
00:10:48,610 --> 00:10:53,000
 critical of Nagarjuna personally, but I don't know.

150
00:10:53,000 --> 00:10:56,000
 I'm sure I'd find some of it impressive.

151
00:10:57,000 --> 00:11:00,000
 As I understand, he was a fairly impressive person.

152
00:11:00,000 --> 00:11:06,000
 No other questions?

153
00:11:06,000 --> 00:11:13,000
 I had a lot last night. We must have gone through them all.

154
00:11:13,000 --> 00:11:18,000
 Michael tells me one of my videos was on Reddit.

155
00:11:18,000 --> 00:11:24,000
 It went on Reddit in the past few days or something.

156
00:11:25,000 --> 00:11:28,000
 And it's very highly ranked or something like that.

157
00:11:28,000 --> 00:11:32,000
 Under the meditation subreddit.

158
00:11:32,000 --> 00:11:38,000
 Why did Ananda take so long to become an Arahant?

159
00:11:38,000 --> 00:11:42,000
 Because he was very busy with looking after the Buddha.

160
00:11:42,000 --> 00:11:45,290
 He spent all his time caring for the Buddha that he didn't

161
00:11:45,290 --> 00:11:47,000
 actually meditate much.

162
00:11:47,000 --> 00:11:49,730
 And he was concerned about this himself, but the Buddha

163
00:11:49,730 --> 00:11:51,000
 said, "Don't be concerned.

164
00:11:51,000 --> 00:11:54,610
 After I attain Bhairni Bhaṇṭha, you will become an Arah

165
00:11:54,610 --> 00:11:55,000
ant."

166
00:11:55,000 --> 00:12:02,000
 What's the name of the chanting you mentioned?

167
00:12:02,000 --> 00:12:07,000
 Wow, reflection on the Buddha, the Dhamma and the Sangha.

168
00:12:17,000 --> 00:12:22,200
 So if you look at any good Buddha's terrible, Buddhist

169
00:12:22,200 --> 00:12:24,000
 chanting book, you'll find these three things.

170
00:12:24,000 --> 00:12:26,000
 Iti-piso, swag-bat, do-zoo-patipano.

171
00:12:26,000 --> 00:12:29,000
 If you want to look it up, it's in the Dajjika sutta.

172
00:12:29,000 --> 00:12:32,500
 It's actually from a sutta. So the Buddha actually recited

173
00:12:32,500 --> 00:12:35,000
 these as an example for us.

174
00:12:35,000 --> 00:12:41,560
 The top of the banner or the banner protection or something

175
00:12:41,560 --> 00:12:42,000
.

176
00:12:43,000 --> 00:12:47,000
 Dajjanga.

177
00:12:47,000 --> 00:12:56,000
 Little Buddha, yes, I did see it.

178
00:12:56,000 --> 00:13:02,010
 And in fact, when I was before I was a monk, my lay teacher

179
00:13:02,010 --> 00:13:08,960
 had me stitch together the Buddha story parts of Little

180
00:13:08,960 --> 00:13:10,000
 Buddha.

181
00:13:11,000 --> 00:13:13,000
 It has a good Buddha story of the Buddha.

182
00:13:13,000 --> 00:13:16,060
 But I mean, that's again, that movie stops as soon as he

183
00:13:16,060 --> 00:13:17,000
 becomes a Buddha.

184
00:13:17,000 --> 00:13:20,720
 And so it's like very, so very Mahayana in a sense that

185
00:13:20,720 --> 00:13:23,000
 doesn't care about the Buddha in his life.

186
00:13:23,000 --> 00:13:25,700
 It's all about how to become a Buddha, which is

187
00:13:25,700 --> 00:13:30,490
 disappointing from the point of view of those of us who

188
00:13:30,490 --> 00:13:33,000
 appreciate the 45 years that came after.

189
00:13:33,000 --> 00:13:37,820
 Much more, maybe not much more, but consider it to be

190
00:13:37,820 --> 00:13:41,000
 actually much more important.

191
00:13:41,000 --> 00:13:46,390
 You know, they always skipped the important part from my

192
00:13:46,390 --> 00:13:48,000
 point of view.

193
00:13:48,000 --> 00:14:03,000
 [silence]

194
00:14:03,000 --> 00:14:09,400
 Yeah, the Buddha story part was not entirely according to

195
00:14:09,400 --> 00:14:13,000
 the texts, but it wasn't bad.

196
00:14:14,000 --> 00:14:19,310
 I mean, I'm a little older, I'm 37 yesterday, so 20 years

197
00:14:19,310 --> 00:14:21,000
 ago, I wasn't all that.

198
00:14:21,000 --> 00:14:26,000
 But I guess I must have seen it after I became Buddhist.

199
00:14:26,000 --> 00:14:30,000
 So sometime in 2000, 2001 probably.

200
00:14:43,000 --> 00:14:46,000
 Keanu Reeves.

201
00:14:46,000 --> 00:14:56,000
 [silence]

202
00:14:56,000 --> 00:14:59,000
 All right, well, let's call it there.

203
00:14:59,000 --> 00:15:02,000
 Wish you all a good night.

204
00:15:02,000 --> 00:15:04,000
 Good practice.

205
00:15:04,000 --> 00:15:06,000
 Oh, I got one more.

206
00:15:06,000 --> 00:15:08,570
 I was hoping you would address the discussion on doing

207
00:15:08,570 --> 00:15:11,370
 deeds without expecting in return in regards to last night

208
00:15:11,370 --> 00:15:13,000
's quote of doers rejoicing.

209
00:15:13,000 --> 00:15:16,000
 Well, I did, didn't I? Last night I talked about it.

210
00:15:16,000 --> 00:15:21,000
 Maybe you disagree, but you can do it again.

211
00:15:21,000 --> 00:15:25,730
 If you do deeds without expecting anything in return, it's,

212
00:15:25,730 --> 00:15:28,000
 you know, what good is it?

213
00:15:28,000 --> 00:15:30,000
 Why are you doing them?

214
00:15:34,000 --> 00:15:41,190
 I guess I suppose an arahant acts in that way, but that's

215
00:15:41,190 --> 00:15:44,000
 because they've done what needs to be done.

216
00:15:44,000 --> 00:15:46,700
 For anyone who hasn't done what needs to be done, they have

217
00:15:46,700 --> 00:15:50,000
 to do things expecting a result, but looking for a result,

218
00:15:50,000 --> 00:15:51,000
 you know, purposefully.

219
00:15:51,000 --> 00:15:53,000
 So doing good deeds is purposeful.

220
00:15:53,000 --> 00:15:56,000
 An arahant has no purpose, so they do good deeds without.

221
00:15:56,000 --> 00:16:02,540
 But they don't purposefully do. They just do things as a

222
00:16:02,540 --> 00:16:04,000
 matter of course.

223
00:16:04,000 --> 00:16:07,500
 Or you could put it that way, that they do deeds without

224
00:16:07,500 --> 00:16:11,130
 expecting anything, but that's the way of someone who's

225
00:16:11,130 --> 00:16:15,000
 already done what needs to be done, katang karaniyam.

226
00:16:15,000 --> 00:16:19,390
 We have not yet done what needs to be done, so that's why

227
00:16:19,390 --> 00:16:21,000
 we do good deeds.

228
00:16:23,000 --> 00:16:26,110
 But that's not even really the point of the quote. The

229
00:16:26,110 --> 00:16:28,750
 quote isn't that you do, isn't saying that you do something

230
00:16:28,750 --> 00:16:30,000
 expecting to rejoice.

231
00:16:30,000 --> 00:16:33,000
 It's saying that when you do good deeds, you do rejoice.

232
00:16:33,000 --> 00:16:36,000
 It's not quite even, yeah, I mean you feel happy.

233
00:16:36,000 --> 00:16:39,390
 Happiness comes from doing good deeds, whether you want it

234
00:16:39,390 --> 00:16:40,000
 or not.

235
00:16:40,000 --> 00:16:48,330
 And that's what it's saying. It's not saying you should do

236
00:16:48,330 --> 00:16:52,000
 good deeds wanting to feel happy.

237
00:16:52,000 --> 00:16:56,000
 It's saying if you do good deeds, you feel happy.

238
00:16:56,000 --> 00:16:59,790
 I mean, it's a way of explaining the difference between

239
00:16:59,790 --> 00:17:01,000
 good and evil.

240
00:17:01,000 --> 00:17:03,000
 Evil leads to suffering.

241
00:17:03,000 --> 00:17:06,000
 "Idda s

242
00:17:06,000 --> 00:17:14,030
 They, they sorrow here, they're sad here, they're sad in

243
00:17:14,030 --> 00:17:15,000
 the next life.

244
00:17:15,000 --> 00:17:21,000
 Those who do evil deeds.

245
00:17:21,000 --> 00:17:28,000
 Maybe that clears it up a little?

246
00:17:28,000 --> 00:17:33,470
 You're welcome. I didn't really call your question stupid,

247
00:17:33,470 --> 00:17:35,000
 did I? I don't do that.

248
00:17:35,000 --> 00:17:41,000
 I know I can be insensitive sometimes, I apologize.

249
00:17:46,000 --> 00:17:50,000
 I blame it all on my parents. I didn't go to public school.

250
00:17:50,000 --> 00:17:53,370
 I was home schooled through my childhood, so I don't know

251
00:17:53,370 --> 00:17:55,000
 how to deal with people.

252
00:17:55,000 --> 00:18:01,000
 Never been good with social mores and norms.

253
00:18:01,000 --> 00:18:13,000
 I didn't say it was either. I don't believe you.

254
00:18:13,000 --> 00:18:17,000
 I think you're making this up.

255
00:18:17,000 --> 00:18:25,600
 Anyway, it's in the past, so I'm not going to dwell. I just

256
00:18:25,600 --> 00:18:27,000
 should you.

257
00:18:27,000 --> 00:18:31,000
 Okay, good night everyone. Thank you all for tuning in.

258
00:18:31,000 --> 00:18:34,000
 I wish you all the best.

259
00:18:34,000 --> 00:18:38,000
 Thank you.

