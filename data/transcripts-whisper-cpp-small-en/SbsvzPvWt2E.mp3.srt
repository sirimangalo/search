1
00:00:00,000 --> 00:00:08,000
 [Music]

2
00:00:08,000 --> 00:00:12,480
 Well, I lost you there. Your sound has turned off.

3
00:00:12,480 --> 00:00:17,440
 Yes. Okay. Sorry. Go ahead.

4
00:00:17,440 --> 00:00:21,140
 I am having a difficult time reconciling, not having with

5
00:00:21,140 --> 00:00:22,080
 my words,

6
00:00:22,080 --> 00:00:26,360
 while telling the truth. I know someone who asks me a

7
00:00:26,360 --> 00:00:28,800
 question, only to hear a soft

8
00:00:28,800 --> 00:00:34,400
 response and gets upset when I answer honestly.

9
00:00:34,400 --> 00:00:41,760
 Do I look fat in this dress? Yes. That was Sam Harris's

10
00:00:41,760 --> 00:00:43,040
 example.

11
00:00:43,040 --> 00:00:46,890
 Do I look fat in this dress? Yes, yes, you look fat in that

12
00:00:46,890 --> 00:00:47,920
 dress. What do you do?

13
00:00:47,920 --> 00:00:50,160
 How can you, can you really answer and tell someone that

14
00:00:50,160 --> 00:00:50,960
 they look fat in a

15
00:00:50,960 --> 00:00:53,280
 dress?

16
00:00:53,280 --> 00:01:00,560
 Um, but his answer is, is, well, I mean,

17
00:01:00,560 --> 00:01:05,040
 you have to be clever, you know, you have to be

18
00:01:05,040 --> 00:01:07,520
 perfect,

19
00:01:07,520 --> 00:01:11,440
 and you have to let go. There are two sides to this. This

20
00:01:11,440 --> 00:01:12,160
 is, this is really,

21
00:01:12,160 --> 00:01:15,940
 this is the other side of lying, no? What do you do when

22
00:01:15,940 --> 00:01:17,200
 you're

23
00:01:17,200 --> 00:01:20,240
 in a position to lie?

24
00:01:20,320 --> 00:01:24,160
 What do you do when telling the truth is going to

25
00:01:24,160 --> 00:01:29,360
 disrupt your way of life?

26
00:01:29,360 --> 00:01:33,120
 And so there's two sides. One, you become skillful,

27
00:01:33,120 --> 00:01:40,370
 so you don't disrupt your life. And two, you take it as a

28
00:01:40,370 --> 00:01:41,680
 chance to

29
00:01:41,680 --> 00:01:44,060
 point out to yourself that it's only because of your

30
00:01:44,060 --> 00:01:45,040
 clinging that there's a

31
00:01:45,040 --> 00:01:48,000
 problem here. In some cases, it's only because you're

32
00:01:48,000 --> 00:01:54,320
 clinging to a relationship that's based on lies, even, even

33
00:01:54,320 --> 00:01:56,960
 white lies.

34
00:01:56,960 --> 00:02:01,200
 You know, because people are so vain and

35
00:02:01,200 --> 00:02:03,970
 self-conscious and so on, and you really want to be around

36
00:02:03,970 --> 00:02:05,440
 people who

37
00:02:05,440 --> 00:02:09,160
 ask you, how do I look? How do I look? And so on, all the

38
00:02:09,160 --> 00:02:09,840
 time.

39
00:02:09,840 --> 00:02:14,640
 Or, or even can be even more sinister than that. People who

40
00:02:14,640 --> 00:02:20,960
 need to be told lies just to be, to, to, to be happy.

41
00:02:20,960 --> 00:02:25,230
 And so is this really the kind of relationship that you

42
00:02:25,230 --> 00:02:26,000
 want to have? Sam

43
00:02:26,000 --> 00:02:28,800
 Harris, I would look up this book by Sam Harris, Lying,

44
00:02:28,800 --> 00:02:32,480
 Lying by Sam Harris. I think it's no longer

45
00:02:32,480 --> 00:02:38,190
 for free, but you can find it for free on Scribd. I found

46
00:02:38,190 --> 00:02:39,680
 it. I've got it for

47
00:02:39,680 --> 00:02:43,840
 free. I don't know. It'd be even worth buying it.

48
00:02:43,840 --> 00:02:46,480
 You know, this is the thing. See, I don't have money, so

49
00:02:46,480 --> 00:02:47,600
 for me, getting things for

50
00:02:47,600 --> 00:02:50,220
 free is really important. But if you've got money, there's

51
00:02:50,220 --> 00:02:50,320
 no

52
00:02:50,320 --> 00:02:53,150
 problem with buying this book. I think it's two dollars,

53
00:02:53,150 --> 00:02:53,680
 actually. That's

54
00:02:53,680 --> 00:02:57,040
 ridiculous. So go and buy it on Amazon, this book

55
00:02:57,040 --> 00:03:03,440
 that he wrote, Lying by Sam Harris. And I agree with

56
00:03:03,440 --> 00:03:06,240
 pretty much everything, not pretty much, almost everything

57
00:03:06,240 --> 00:03:07,360
 he says.

58
00:03:07,360 --> 00:03:10,310
 You know, I go a little further and saying lying is never

59
00:03:10,310 --> 00:03:11,280
 good,

60
00:03:11,280 --> 00:03:15,120
 but then I also would say that torture is never,

61
00:03:15,120 --> 00:03:22,560
 never defensible, whereas he seems to, I think he's just

62
00:03:22,560 --> 00:03:25,680
 intellectualizing. So to the point where he finds that

63
00:03:25,680 --> 00:03:26,960
 torture could be useful and

64
00:03:26,960 --> 00:03:32,560
 could be beneficial and morally, more ethically,

65
00:03:32,560 --> 00:03:36,590
 necessary or something. But he's a very good writer and he

66
00:03:36,590 --> 00:03:37,200
 writes really well

67
00:03:37,200 --> 00:03:43,360
 on lying. Reality is not nearly as simple as we

68
00:03:43,360 --> 00:03:46,950
 think. That goes without saying, right? So we think it's

69
00:03:46,950 --> 00:03:47,680
 either

70
00:03:47,680 --> 00:03:51,040
 A, I hurt the person, either A, I tell the truth and hurt

71
00:03:51,040 --> 00:03:52,480
 the person, or B, I lie

72
00:03:52,480 --> 00:03:56,690
 and make them happy. It's never, it's not nearly as simple

73
00:03:56,690 --> 00:03:58,160
 as that.

74
00:03:58,160 --> 00:04:01,170
 What's the effect on your mind? What's the effect on your

75
00:04:01,170 --> 00:04:02,400
 relationship with this

76
00:04:02,400 --> 00:04:05,600
 person when you tell them a white lie? That

77
00:04:05,600 --> 00:04:10,320
 your relationship is not, you don't feel totally free,

78
00:04:10,320 --> 00:04:14,560
 you don't feel free to be totally honest with this person.

79
00:04:14,560 --> 00:04:18,550
 And you know, suppose it's your boss, right? And they want

80
00:04:18,550 --> 00:04:18,960
 you to

81
00:04:18,960 --> 00:04:22,320
 somehow lie to them or lie to other people.

82
00:04:22,320 --> 00:04:26,660
 And so somehow, let's just say it's your boss happens to be

83
00:04:26,660 --> 00:04:27,040
 a little bit

84
00:04:27,040 --> 00:04:31,440
 overweight or heavier than most people. And so

85
00:04:31,440 --> 00:04:34,480
 your boss asks you, do I look fat in this dress? And you

86
00:04:34,480 --> 00:04:34,880
 know if you're

87
00:04:34,880 --> 00:04:38,160
 saying look fat that based on your past experiences with

88
00:04:38,160 --> 00:04:40,320
 this person, they'll just fire you or they'll

89
00:04:40,320 --> 00:04:44,730
 make your life very difficult. It's a good chance for you

90
00:04:44,730 --> 00:04:45,600
 to realize

91
00:04:45,600 --> 00:04:50,320
 that you're in the wrong life. You can't avoid

92
00:04:50,320 --> 00:04:52,540
 these very, very difficult situations when you're

93
00:04:52,540 --> 00:04:53,680
 surrounded by people who

94
00:04:53,680 --> 00:04:57,200
 make things very, very difficult for you. This is why

95
00:04:57,200 --> 00:05:00,130
 the conclusion I can only ever come to is, well why don't

96
00:05:00,130 --> 00:05:01,120
 you just ordain?

97
00:05:01,120 --> 00:05:05,920
 Why don't you all just ordain? Why don't you all just

98
00:05:05,920 --> 00:05:09,630
 give it up? What use do you find in it? What good do you

99
00:05:09,630 --> 00:05:10,480
 find in it?

100
00:05:10,480 --> 00:05:14,790
 Yes, you have attachments, yes you have desires. I had them

101
00:05:14,790 --> 00:05:14,800
.

102
00:05:14,800 --> 00:05:19,840
 I'm not the perfect monk. I'm not perfect. I didn't become

103
00:05:19,840 --> 00:05:20,240
 a monk

104
00:05:20,240 --> 00:05:27,760
 because I was perfect. I'm not special. We are not special.

105
00:05:27,760 --> 00:05:32,800
 Why not just do it? This is why I said there's two sides.

106
00:05:32,800 --> 00:05:36,880
 If you have the view that there's some benefit in

107
00:05:36,880 --> 00:05:40,640
 being with such people and being in such situations,

108
00:05:40,640 --> 00:05:46,320
 then this is... I don't have much to say. We can never find

109
00:05:46,320 --> 00:05:50,800
 a common ground unless in certain cases I can

110
00:05:50,800 --> 00:05:53,680
 persuade you otherwise, but I don't have that intention. If

111
00:05:53,680 --> 00:05:55,600
 you

112
00:05:55,600 --> 00:05:59,070
 want to be free from this but you feel you're so attached

113
00:05:59,070 --> 00:05:59,920
 to the world,

114
00:05:59,920 --> 00:06:03,200
 this isn't a problem. This is why you become a monk.

115
00:06:03,200 --> 00:06:05,650
 This is why you leave the world. You don't... maybe not

116
00:06:05,650 --> 00:06:06,720
 become a monk. Why you go to a

117
00:06:06,720 --> 00:06:10,560
 meditation center? Because you know I have too many

118
00:06:10,560 --> 00:06:14,560
 defilements to live in this place. I have to... if I live

119
00:06:14,560 --> 00:06:15,280
... if I continue

120
00:06:15,280 --> 00:06:17,900
 this lifestyle, there's no way I'm going to progress in D

121
00:06:17,900 --> 00:06:19,040
hamma.

122
00:06:19,040 --> 00:06:21,240
 There's no way I'm going to become a better person. No way

123
00:06:21,240 --> 00:06:22,080
 my mind is going to

124
00:06:22,080 --> 00:06:28,080
 find clarity. I can't progress here. This is why you

125
00:06:28,080 --> 00:06:31,840
 leave and go off into the forest. You know that

126
00:06:31,840 --> 00:06:34,560
 you've got a problem. That's why you do it. That's why you

127
00:06:34,560 --> 00:06:38,000
 leave. This is the Buddhist teaching on guarding.

128
00:06:38,000 --> 00:06:42,430
 You have to guard yourself. You have to do away with these

129
00:06:42,430 --> 00:06:44,640
 complexities of life.

130
00:06:44,640 --> 00:06:47,920
 And this is why people become a monk. This is the

131
00:06:47,920 --> 00:06:51,870
 best reason to become a monk. Or you know, whatever. Find a

132
00:06:51,870 --> 00:06:52,640
 way to live

133
00:06:52,640 --> 00:06:58,240
 a more simple life as a lay person. It's perfectly valid.

134
00:06:58,240 --> 00:06:59,120
 Some lay people are

135
00:06:59,120 --> 00:07:04,930
 able to live simpler lives than many monks. So power to

136
00:07:04,930 --> 00:07:05,840
 them.

