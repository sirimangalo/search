1
00:00:00,000 --> 00:00:02,960
 Okay, good evening.

2
00:00:02,960 --> 00:00:07,560
 So tonight's question is about

3
00:00:07,560 --> 00:00:12,160
 protecting one's practice as a layperson.

4
00:00:12,160 --> 00:00:25,680
 The person asking expresses their distress

5
00:00:25,960 --> 00:00:30,960
 at seeing how easy it is to fall away from the practice,

6
00:00:30,960 --> 00:00:36,360
 even for those who have done practice before,

7
00:00:36,360 --> 00:00:41,360
 but find themselves back in worldly life

8
00:00:41,360 --> 00:00:44,560
 and find it very hard to keep up their practice.

9
00:00:44,560 --> 00:00:49,200
 So I thought about this and

10
00:00:49,200 --> 00:00:55,680
 there were, well, three things I can say about this.

11
00:00:55,680 --> 00:00:57,000
 That's what I came up with.

12
00:00:57,000 --> 00:00:59,840
 The first thing I would say is that

13
00:00:59,840 --> 00:01:03,560
 in some respects, and this is

14
00:01:03,560 --> 00:01:06,400
 probably a little bit of a disappointment,

15
00:01:06,400 --> 00:01:08,800
 but in some respects, this question is outside

16
00:01:08,800 --> 00:01:13,200
 of the sort of question I'm comfortable answering.

17
00:01:13,200 --> 00:01:18,320
 But I think there's at least a small lesson there

18
00:01:18,320 --> 00:01:23,320
 and that's in regards to our practice and our,

19
00:01:23,320 --> 00:01:28,320
 perspective as Buddhist practitioners.

20
00:01:28,320 --> 00:01:37,040
 I teach meditation.

21
00:01:37,040 --> 00:01:40,320
 I think it helps my practice.

22
00:01:40,320 --> 00:01:46,800
 It fulfills my sort of duty, I can say, to other beings.

23
00:01:46,800 --> 00:01:52,280
 It feels comfortable, something I can do for others

24
00:01:52,280 --> 00:01:56,920
 and it's not a great distraction

25
00:01:56,920 --> 00:01:59,340
 from my own spiritual development.

26
00:01:59,340 --> 00:02:03,400
 But that's the same for anyone

27
00:02:03,400 --> 00:02:07,080
 and that's for people who are interested in practicing.

28
00:02:07,080 --> 00:02:13,760
 Trying to help people who are not interested in practicing,

29
00:02:13,760 --> 00:02:16,640
 find ways to be interested in practicing,

30
00:02:16,640 --> 00:02:19,000
 has always felt a little bit outside of the realm

31
00:02:19,000 --> 00:02:22,520
 of what is useful and beneficial.

32
00:02:22,520 --> 00:02:26,600
 So we're starting to enter into that realm,

33
00:02:26,600 --> 00:02:27,960
 I think, with this question.

34
00:02:27,960 --> 00:02:31,800
 People ask me often, I often get adults

35
00:02:31,800 --> 00:02:34,920
 wanting me to teach their children how to meditate

36
00:02:34,920 --> 00:02:36,700
 and I've always found that profoundly wrong

37
00:02:36,700 --> 00:02:40,880
 or fundamentally unpleasant.

38
00:02:40,880 --> 00:02:47,640
 Like, not unpleasant, the word is improper,

39
00:02:47,640 --> 00:02:51,920
 feeling, giving an awkward sort of feeling,

40
00:02:51,920 --> 00:02:54,480
 like not quite right for this reason.

41
00:02:54,480 --> 00:02:57,240
 Because you're talking about people

42
00:02:57,240 --> 00:02:59,440
 who don't want to meditate,

43
00:02:59,440 --> 00:03:01,880
 but want to want to meditate

44
00:03:01,880 --> 00:03:05,120
 and here it's, their parents want them to want to meditate.

45
00:03:05,120 --> 00:03:10,120
 So if you take a person who is not interested

46
00:03:10,120 --> 00:03:12,400
 in meditation but really would like

47
00:03:12,400 --> 00:03:14,140
 to be interested in meditation,

48
00:03:15,280 --> 00:03:18,500
 that's the sort of problem that comes out.

49
00:03:18,500 --> 00:03:24,880
 Lay Buddhists who like the idea of meditating,

50
00:03:24,880 --> 00:03:30,280
 but find themselves distracted by so many things.

51
00:03:30,280 --> 00:03:35,280
 So there's certainly no sense of criticizing

52
00:03:35,280 --> 00:03:40,640
 or blaming anyone who gets distracted by worldly life,

53
00:03:40,960 --> 00:03:45,960
 but there should be, I think, no expectation

54
00:03:45,960 --> 00:03:50,560
 for anyone but themselves to find a way

55
00:03:50,560 --> 00:03:54,560
 to overcome that to some extent.

56
00:03:54,560 --> 00:03:58,520
 I think we have to understand

57
00:03:58,520 --> 00:04:00,800
 that the Buddha wasn't even a savior.

58
00:04:00,800 --> 00:04:02,440
 The Buddha didn't teach lay people

59
00:04:02,440 --> 00:04:05,160
 and he taught some things that were quite useful

60
00:04:05,160 --> 00:04:09,080
 for lay people and very much outside of the purview

61
00:04:09,080 --> 00:04:12,080
 or the realm or the limit or the range

62
00:04:12,080 --> 00:04:18,140
 of meditation practice.

63
00:04:18,140 --> 00:04:25,780
 But even the Buddha had very little to say

64
00:04:25,780 --> 00:04:30,780
 beyond describing right ways to live your life.

65
00:04:36,280 --> 00:04:40,520
 There's no limit to our potential to help other beings.

66
00:04:40,520 --> 00:04:44,840
 If we want to go further, we could help beings in hell.

67
00:04:44,840 --> 00:04:48,960
 We could help animals figure out how to get dogs

68
00:04:48,960 --> 00:04:52,080
 to be interested in good things that would eventually

69
00:04:52,080 --> 00:04:54,840
 perhaps allow them to be born as humans,

70
00:04:54,840 --> 00:04:58,120
 meaning there's no limit to the amount of help

71
00:04:58,120 --> 00:04:59,320
 we could give to other beings.

72
00:04:59,320 --> 00:05:02,720
 But that's not certainly my goal.

73
00:05:02,720 --> 00:05:05,640
 I don't think it's a goal that the Buddha advised

74
00:05:05,640 --> 00:05:06,820
 in his followers.

75
00:05:06,820 --> 00:05:10,640
 And so I think it's a good lesson for us

76
00:05:10,640 --> 00:05:14,920
 to remind ourselves our certain perspective and a balance.

77
00:05:14,920 --> 00:05:17,240
 And the balance always has to be weighted

78
00:05:17,240 --> 00:05:20,080
 in favor of our own spiritual development.

79
00:05:20,080 --> 00:05:23,400
 We can't really help others if our attention

80
00:05:23,400 --> 00:05:24,900
 is always on helping others.

81
00:05:24,900 --> 00:05:28,000
 There's of course helping anyone yourself

82
00:05:28,000 --> 00:05:30,080
 or another person depends upon your own

83
00:05:30,080 --> 00:05:31,720
 spiritual development.

84
00:05:31,720 --> 00:05:34,600
 And one's own spiritual development

85
00:05:34,600 --> 00:05:37,800
 has to require a preponderance of attention

86
00:05:37,800 --> 00:05:39,720
 to one's own practice.

87
00:05:39,720 --> 00:05:42,120
 First thing I'll say.

88
00:05:42,120 --> 00:05:47,680
 But, well one other thing that's also a little bit

89
00:05:47,680 --> 00:05:52,680
 discouraging I think is that fundamentally

90
00:05:52,680 --> 00:05:55,600
 we're asking, the question is asking,

91
00:05:55,600 --> 00:06:00,600
 or a part of it asking for something that is impossible

92
00:06:00,600 --> 00:06:03,120
 or contradictory.

93
00:06:04,920 --> 00:06:08,920
 It's like the expression of having your cake

94
00:06:08,920 --> 00:06:10,600
 and eating it too.

95
00:06:10,600 --> 00:06:13,560
 It's like saying, how can I be a really good meditator

96
00:06:13,560 --> 00:06:14,460
 and live in the world?

97
00:06:14,460 --> 00:06:18,160
 Well, honestly, you can't.

98
00:06:18,160 --> 00:06:20,560
 And a part of one of the most important,

99
00:06:20,560 --> 00:06:23,640
 one important support of meditation practice

100
00:06:23,640 --> 00:06:26,200
 is leaving the world physically.

101
00:06:26,200 --> 00:06:28,820
 Of course mentally much more important.

102
00:06:28,820 --> 00:06:33,820
 And technically is it possible for someone to live

103
00:06:33,820 --> 00:06:35,960
 a spiritual life in the world?

104
00:06:35,960 --> 00:06:38,160
 Technically it's possible.

105
00:06:38,160 --> 00:06:43,160
 But to find a way for an ordinary average person

106
00:06:43,160 --> 00:06:45,160
 like you or me to do that.

107
00:06:45,160 --> 00:06:51,360
 And then be distressed by how difficult it is

108
00:06:51,360 --> 00:06:54,760
 and wonder how you can make it easier.

109
00:06:54,760 --> 00:06:56,480
 It's really not the case.

110
00:06:56,480 --> 00:06:59,320
 How you can make it easier is be a lot stronger,

111
00:06:59,320 --> 00:07:01,600
 a lot more perfect and a lot more pure

112
00:07:01,600 --> 00:07:04,360
 which I'm not expecting anyone to be.

113
00:07:04,360 --> 00:07:07,200
 I don't consider myself to be like that.

114
00:07:07,200 --> 00:07:14,240
 It's always by people who practice Buddhism

115
00:07:14,240 --> 00:07:17,120
 and by the Buddha himself been described,

116
00:07:17,120 --> 00:07:23,120
 been said that lay life is a dusty road.

117
00:07:23,120 --> 00:07:28,120
 It's all confined and restricted and mixed up.

118
00:07:28,120 --> 00:07:38,440
 It's not like in the olden days in the time of the Buddha

119
00:07:38,440 --> 00:07:41,960
 when there was a lot of traffic,

120
00:07:41,960 --> 00:07:43,800
 there would be a great amount of dust.

121
00:07:43,800 --> 00:07:47,880
 You'd have horses and just people walking

122
00:07:47,880 --> 00:07:52,120
 and it would be crowded roads would be very dusty and dry

123
00:07:52,120 --> 00:07:55,400
 and not very pleasant because people would be defecating

124
00:07:55,400 --> 00:07:58,000
 on the side of the road and that sort of thing.

125
00:07:58,000 --> 00:07:59,940
 Garbage on the side of the road.

126
00:07:59,940 --> 00:08:07,000
 And they say in comparison, leaving home

127
00:08:07,000 --> 00:08:11,760
 and going to live in seclusion, a simple life

128
00:08:11,760 --> 00:08:13,760
 is the open air.

129
00:08:13,760 --> 00:08:18,640
 It feels much more fresh, much more pure and free.

130
00:08:21,920 --> 00:08:26,480
 And so to ask about how to make meditation

131
00:08:26,480 --> 00:08:30,720
 in lay life easier is problematic at its core

132
00:08:30,720 --> 00:08:35,120
 because I think you're much better served

133
00:08:35,120 --> 00:08:37,400
 understanding how difficult it is.

134
00:08:37,400 --> 00:08:39,960
 And it's for some people, it's not possible for them

135
00:08:39,960 --> 00:08:41,180
 to leave it behind.

136
00:08:41,180 --> 00:08:46,320
 And so important for them will be to understand

137
00:08:46,320 --> 00:08:47,560
 that it's not going to be easy.

138
00:08:47,560 --> 00:08:49,880
 And I think part of our practice is always

139
00:08:49,880 --> 00:08:54,880
 about understanding and coming to terms with difficulty.

140
00:08:54,880 --> 00:08:58,880
 And I think to some extent,

141
00:08:58,880 --> 00:09:03,880
 ever trying to make your practice easier is problematic.

142
00:09:03,880 --> 00:09:08,720
 So instead of looking at it as something,

143
00:09:08,720 --> 00:09:11,160
 wow, this is really difficult and I'm struggling,

144
00:09:11,160 --> 00:09:16,200
 try and reframe it and learn to be mindful of the struggle

145
00:09:16,200 --> 00:09:19,240
 and mindful of what it is that is causing the struggle

146
00:09:19,240 --> 00:09:20,080
 and so on.

147
00:09:20,080 --> 00:09:26,080
 Which brings me to the third thing, which is more hopeful,

148
00:09:26,080 --> 00:09:29,480
 but a little bit critical as well.

149
00:09:29,480 --> 00:09:31,040
 I don't mean to be entirely critical,

150
00:09:31,040 --> 00:09:35,400
 but it's critical in general of us as Buddhists,

151
00:09:35,400 --> 00:09:40,400
 as meditators, we often focus solely on meditation practice

152
00:09:40,400 --> 00:09:40,400
.

153
00:09:40,400 --> 00:09:45,400
 And so you come here to practice and you can be forgiven

154
00:09:45,400 --> 00:09:46,800
 because that's all we teach you.

155
00:09:46,800 --> 00:09:48,640
 And that's really what we give you.

156
00:09:48,640 --> 00:09:51,360
 So you come here, you learn how to meditate and you say,

157
00:09:51,360 --> 00:09:53,960
 okay, I've done meditation.

158
00:09:53,960 --> 00:09:56,240
 Then you go home and you try to continue

159
00:09:56,240 --> 00:09:59,600
 to meditate like that and it doesn't work

160
00:09:59,600 --> 00:10:00,720
 or it's problematic.

161
00:10:00,720 --> 00:10:05,720
 And this is because even here, meditation requires support.

162
00:10:05,720 --> 00:10:10,960
 Much of your support is given to you artificially, right?

163
00:10:10,960 --> 00:10:14,160
 Your livelihood is pure here

164
00:10:14,160 --> 00:10:16,040
 and you don't have to even go to work for it.

165
00:10:16,040 --> 00:10:18,480
 You don't have to demean yourself

166
00:10:18,480 --> 00:10:21,120
 doing something meaningless in order to get food

167
00:10:21,120 --> 00:10:23,360
 and shelter and so on.

168
00:10:23,360 --> 00:10:28,040
 But your environment is pure, there's no distractions,

169
00:10:28,040 --> 00:10:33,040
 there's no, there's nothing pulling you here or there.

170
00:10:33,040 --> 00:10:37,360
 Desire, there's no conflict with other people.

171
00:10:37,360 --> 00:10:40,680
 There's no manipulation or anything like that.

172
00:10:43,920 --> 00:10:46,240
 And so it feels like, wow, this is okay,

173
00:10:46,240 --> 00:10:47,400
 all I have to do is meditate,

174
00:10:47,400 --> 00:10:50,000
 but you go home and none of those supports are there.

175
00:10:50,000 --> 00:10:56,640
 And of course, even here, there is,

176
00:10:56,640 --> 00:11:00,640
 there can be the lack of support.

177
00:11:00,640 --> 00:11:03,560
 Many people come to practice and aren't able to continue

178
00:11:03,560 --> 00:11:07,520
 or finish the course because they lack the support.

179
00:11:07,520 --> 00:11:11,200
 So it's been good in general for us

180
00:11:11,200 --> 00:11:14,200
 to do this correspondence course, the online course,

181
00:11:14,200 --> 00:11:18,200
 because it first of all provides a filter

182
00:11:18,200 --> 00:11:22,520
 if you're really not ready to do a course here,

183
00:11:22,520 --> 00:11:24,480
 here you'll figure it out in the online course.

184
00:11:24,480 --> 00:11:26,560
 If you're not able to do that,

185
00:11:26,560 --> 00:11:28,440
 now then you won't make it.

186
00:11:28,440 --> 00:11:34,800
 We means it selects the people

187
00:11:34,800 --> 00:11:40,880
 who are really keen to do it, but it also prepares you.

188
00:11:40,880 --> 00:11:45,520
 It's a great way to understand the dynamics of the practice

189
00:11:45,520 --> 00:11:48,200
 and what is necessary to use as a support,

190
00:11:48,200 --> 00:11:52,640
 how to begin to organize your life

191
00:11:52,640 --> 00:11:55,380
 in a way that is supportive of meditation practice.

192
00:11:55,380 --> 00:11:57,840
 But deeper than that,

193
00:11:57,840 --> 00:12:04,840
 there are qualities of individuals that are required.

194
00:12:04,840 --> 00:12:07,800
 And there's something we call Upanishayas.

195
00:12:07,800 --> 00:12:10,440
 Upanishayas is what you come into this world with.

196
00:12:10,440 --> 00:12:15,320
 Well, that's how it's talked about in Thai.

197
00:12:15,320 --> 00:12:19,820
 I think in Pali the word is we saya, we saya means,

198
00:12:19,820 --> 00:12:23,280
 Upanishaya maybe as well.

199
00:12:23,280 --> 00:12:25,920
 Anyway, there's this idea of what you bring into this life,

200
00:12:25,920 --> 00:12:29,150
 meaning some people are just never going to be into

201
00:12:29,150 --> 00:12:30,400
 practice

202
00:12:30,400 --> 00:12:33,080
 or capable of practicing,

203
00:12:33,080 --> 00:12:35,300
 capable of being interested in practicing.

204
00:12:37,600 --> 00:12:41,120
 And for most of us, we're kind of halfway,

205
00:12:41,120 --> 00:12:44,140
 we have some interest and capability of practicing,

206
00:12:44,140 --> 00:12:46,800
 but it's rough and it's difficult.

207
00:12:46,800 --> 00:12:50,520
 That's because we haven't brought a great support

208
00:12:50,520 --> 00:12:51,380
 into this life.

209
00:12:51,380 --> 00:12:54,480
 And it can also be because we're not developing

210
00:12:54,480 --> 00:12:58,510
 or our development of support is often incomplete or

211
00:12:58,510 --> 00:12:59,480
 partial.

212
00:12:59,480 --> 00:13:05,600
 And so these are things like being a good person,

213
00:13:05,600 --> 00:13:09,280
 being kind, being generous, having a pure heart, right?

214
00:13:09,280 --> 00:13:12,240
 Because what we mean by being a good person is really that,

215
00:13:12,240 --> 00:13:14,200
 having a pure mind and a pure heart.

216
00:13:14,200 --> 00:13:19,200
 And the purer your foundation as an individual is,

217
00:13:19,200 --> 00:13:21,840
 of course, the easier it is to have pure thoughts,

218
00:13:21,840 --> 00:13:26,840
 clear thoughts, steady and coherent thoughts.

219
00:13:26,840 --> 00:13:31,600
 And so this applies to lay people.

220
00:13:31,600 --> 00:13:34,760
 It's a big reason I think why you can,

221
00:13:36,320 --> 00:13:39,900
 I can agree that much of the teaching that is given

222
00:13:39,900 --> 00:13:43,200
 to lay people, lay Buddhists is proper.

223
00:13:43,200 --> 00:13:46,260
 You know, we often hear criticism of monks

224
00:13:46,260 --> 00:13:49,440
 who teach lay people only very shallow Dhamma

225
00:13:49,440 --> 00:13:51,080
 and they should be teaching deeper Dhamma.

226
00:13:51,080 --> 00:13:53,000
 And it's true to some extent,

227
00:13:53,000 --> 00:13:58,000
 but it's also forgivable because really things

228
00:13:58,000 --> 00:14:01,540
 like being generous and keeping morality

229
00:14:01,540 --> 00:14:05,380
 and practicing basic meditation like loving kindness

230
00:14:05,380 --> 00:14:07,640
 and so on may not get you to nirvana,

231
00:14:07,640 --> 00:14:10,960
 but it certainly creates a great foundation.

232
00:14:10,960 --> 00:14:15,960
 It is wrong to stop there and never hint at something

233
00:14:15,960 --> 00:14:22,800
 where you should always be leaving the door open

234
00:14:22,800 --> 00:14:24,840
 for people who are ready to make the jump

235
00:14:24,840 --> 00:14:27,680
 because there are always of course, many lay people

236
00:14:28,600 --> 00:14:31,320
 to be a monk to want to practice meditation.

237
00:14:31,320 --> 00:14:35,000
 But the foundation is equally important

238
00:14:35,000 --> 00:14:38,120
 and it's not just a foundation, it's a support.

239
00:14:38,120 --> 00:14:41,880
 I would make this analogy of a tree.

240
00:14:41,880 --> 00:14:45,000
 If you want to grow a tree from a sapling,

241
00:14:45,000 --> 00:14:48,960
 you can't just plant it and leave it, you need support.

242
00:14:48,960 --> 00:14:50,960
 Through the winters, especially here in Canada,

243
00:14:50,960 --> 00:14:54,280
 we know this because even just the snow and the cold,

244
00:14:54,280 --> 00:14:55,820
 you have to protect it from them

245
00:14:55,820 --> 00:14:58,400
 and do other sticks to prop it up

246
00:14:58,400 --> 00:14:59,240
 and so on.

247
00:14:59,240 --> 00:15:05,520
 And so even when you're practicing meditation,

248
00:15:05,520 --> 00:15:08,080
 especially not here where you have everything

249
00:15:08,080 --> 00:15:09,360
 sort of laid out for you,

250
00:15:09,360 --> 00:15:14,080
 you need to set up your life in ways that are wholesome.

251
00:15:14,080 --> 00:15:17,600
 Conduct good works, good deeds,

252
00:15:17,600 --> 00:15:20,760
 do good things for the world around you.

253
00:15:20,760 --> 00:15:22,760
 Even in terms of teaching,

254
00:15:22,760 --> 00:15:25,260
 helping other people come to the practice

255
00:15:25,260 --> 00:15:28,840
 can be a very good way to support your own practice

256
00:15:28,840 --> 00:15:31,420
 because it creates, it gives you this environment.

257
00:15:31,420 --> 00:15:34,200
 It surrounds you with people

258
00:15:34,200 --> 00:15:35,680
 who are interested in meditation.

259
00:15:35,680 --> 00:15:40,520
 It challenges you and tests you, keeps you straight.

260
00:15:40,520 --> 00:15:48,240
 Another one, another important one is something like

261
00:15:48,240 --> 00:15:52,120
 your association with other people,

262
00:15:52,120 --> 00:15:54,160
 not something like that in and of itself

263
00:15:54,160 --> 00:15:57,100
 is one of the most important qualities.

264
00:15:57,100 --> 00:16:01,380
 Right, so having teaching other people is a part of that,

265
00:16:01,380 --> 00:16:04,600
 but just associating with other practitioners,

266
00:16:04,600 --> 00:16:07,840
 having a meditation group that you go to sit with

267
00:16:07,840 --> 00:16:10,640
 or whatever, being surrounded by other people

268
00:16:10,640 --> 00:16:14,220
 is very important for people who are in a good place.

269
00:16:14,220 --> 00:16:19,000
 Where you live, what you do, your work,

270
00:16:19,000 --> 00:16:21,520
 there's so much and it starts to get,

271
00:16:21,520 --> 00:16:25,800
 as I said, outside of the purview of what I,

272
00:16:25,800 --> 00:16:29,840
 the realm of what I consider my duty as a meditation

273
00:16:29,840 --> 00:16:30,800
 teacher.

274
00:16:30,800 --> 00:16:33,980
 But this is just the general understanding

275
00:16:33,980 --> 00:16:36,860
 that there's more to the practice than the practice you

276
00:16:36,860 --> 00:16:37,480
 need.

277
00:16:37,480 --> 00:16:39,880
 That's why I teach the sabbaso as Sutta quite often

278
00:16:39,880 --> 00:16:43,470
 because it reminds us of all the different things we need

279
00:16:43,470 --> 00:16:44,480
 to,

280
00:16:44,480 --> 00:16:48,480
 different aspects of our life that we need to consider

281
00:16:48,480 --> 00:16:50,280
 as a protection for our practice.

282
00:16:52,040 --> 00:16:54,360
 And the last part of that,

283
00:16:54,360 --> 00:16:57,720
 something that I think could really help is

284
00:16:57,720 --> 00:17:01,900
 our closeness with Buddhism,

285
00:17:01,900 --> 00:17:06,600
 which to some extent is artificial, right?

286
00:17:06,600 --> 00:17:10,160
 Our connection with concepts,

287
00:17:10,160 --> 00:17:13,640
 like the concept of the Buddha, the Dhamma, the Sangha,

288
00:17:13,640 --> 00:17:15,720
 even just the statues,

289
00:17:15,720 --> 00:17:18,040
 having some kind of image in our mind,

290
00:17:18,040 --> 00:17:20,720
 a visualization of a Buddha image,

291
00:17:20,720 --> 00:17:28,840
 the association with monks and chanting and rituals,

292
00:17:28,840 --> 00:17:31,200
 candles, flowers, incense,

293
00:17:31,200 --> 00:17:34,680
 these kinds of things,

294
00:17:34,680 --> 00:17:39,640
 and the sort of determinations that we make,

295
00:17:39,640 --> 00:17:42,600
 may I always be close to the Buddha sasana.

296
00:17:42,600 --> 00:17:44,180
 They can often be artificial,

297
00:17:44,180 --> 00:17:47,620
 but they can also be quite helpful.

298
00:17:48,340 --> 00:17:51,980
 Meaning a person could be a very good person,

299
00:17:51,980 --> 00:17:53,620
 have very good qualities of mind.

300
00:17:53,620 --> 00:17:57,300
 And to some extent, I'm gonna hedge it here

301
00:17:57,300 --> 00:18:00,440
 because it's not entirely true, but to some extent,

302
00:18:00,440 --> 00:18:02,200
 even though they have great good qualities,

303
00:18:02,200 --> 00:18:06,140
 if they've never had any connection with Buddhism

304
00:18:06,140 --> 00:18:09,860
 as a concept, as a construct, as an institution,

305
00:18:09,860 --> 00:18:11,940
 they may just never meet up with another Buddhist.

306
00:18:11,940 --> 00:18:14,580
 They may not have the karmic connection

307
00:18:14,580 --> 00:18:19,580
 or the direction to ever find a way to go further,

308
00:18:19,580 --> 00:18:23,460
 because goodness in and of itself

309
00:18:23,460 --> 00:18:26,780
 doesn't necessarily on a conventional level

310
00:18:26,780 --> 00:18:28,260
 lead to enlightenment.

311
00:18:28,260 --> 00:18:30,540
 It could just lead you to happy rebirths,

312
00:18:30,540 --> 00:18:32,420
 heaven and so on.

313
00:18:32,420 --> 00:18:34,600
 There are many non-Buddhists who are good people,

314
00:18:34,600 --> 00:18:37,980
 Christians and Muslims and Jews and Hindus,

315
00:18:37,980 --> 00:18:40,660
 and all religions really have these people.

316
00:18:40,660 --> 00:18:42,700
 And because these religions have a part of them

317
00:18:42,700 --> 00:18:46,700
 that teaches goodness, some people take them up on that

318
00:18:46,700 --> 00:18:48,860
 and cultivate it really quite well.

319
00:18:48,860 --> 00:18:51,880
 But they never make it to Buddhism often

320
00:18:51,880 --> 00:18:56,880
 because of their connection,

321
00:18:56,880 --> 00:19:00,420
 the images of the crucifix

322
00:19:00,420 --> 00:19:04,040
 or any other religious symbol artificially.

323
00:19:04,040 --> 00:19:07,100
 It sends them in that direction

324
00:19:07,100 --> 00:19:10,480
 and it inclines them to meet up with those people.

325
00:19:10,480 --> 00:19:13,420
 The dhamma wheel if you have these sort of symbols.

326
00:19:13,420 --> 00:19:15,580
 So what I'm trying, what this all means

327
00:19:15,580 --> 00:19:17,500
 and how this carries out in practice

328
00:19:17,500 --> 00:19:21,020
 is that things like getting involved with Buddhist culture

329
00:19:21,020 --> 00:19:22,140
 can be very useful.

330
00:19:22,140 --> 00:19:25,860
 Culture, Buddhist culture can be a very useful thing

331
00:19:25,860 --> 00:19:28,840
 as a shal, as a means of supporting

332
00:19:28,840 --> 00:19:31,300
 what is really and truly Buddhism.

333
00:19:31,300 --> 00:19:34,140
 And a part of this is, as I said, the determinations.

334
00:19:34,140 --> 00:19:38,500
 I think a part of this is where we make a determination.

335
00:19:38,500 --> 00:19:42,100
 For example, people make determinations to be reborn

336
00:19:42,100 --> 00:19:44,620
 in the time of meteja, the next Buddha.

337
00:19:44,620 --> 00:19:47,120
 That's a good determination to make.

338
00:19:47,120 --> 00:19:50,300
 I don't think it's in any way

339
00:19:50,300 --> 00:19:54,180
 a replacement for actual practice.

340
00:19:54,180 --> 00:19:55,400
 It should never be.

341
00:19:55,400 --> 00:19:58,300
 But as a part of our practice, to remember and to think

342
00:19:58,300 --> 00:20:02,100
 and to even people will send prayers to meteja

343
00:20:02,100 --> 00:20:05,540
 and not prayers, but in the sense of chanting

344
00:20:05,540 --> 00:20:09,780
 and making a determination, paying respect to meteja.

345
00:20:09,780 --> 00:20:13,420
 Paying respect to the Buddha, the dhamma, the sangha

346
00:20:13,420 --> 00:20:16,660
 and in the form of chanting can be very helpful

347
00:20:16,660 --> 00:20:20,340
 to set your mind in a way that's close

348
00:20:20,340 --> 00:20:21,780
 to the Buddha's teaching.

349
00:20:21,780 --> 00:20:25,660
 And it's not just for a future,

350
00:20:25,660 --> 00:20:27,940
 it's for your own present life.

351
00:20:27,940 --> 00:20:30,700
 This isn't just so that one day I might be reborn

352
00:20:30,700 --> 00:20:32,500
 and with meteja.

353
00:20:32,500 --> 00:20:34,660
 It keeps you close to the Buddha sasa

354
00:20:34,660 --> 00:20:38,420
 and it keeps you in touch with other Buddhists,

355
00:20:38,420 --> 00:20:39,820
 even if they aren't practicing,

356
00:20:39,820 --> 00:20:42,060
 maybe they've studied Buddhism

357
00:20:42,060 --> 00:20:44,080
 and can remind you of certain things.

358
00:20:44,080 --> 00:20:47,740
 We often learn a lot as meditators from scholars

359
00:20:47,740 --> 00:20:49,820
 because they have a lot of knowledge

360
00:20:49,820 --> 00:20:53,140
 and they're able to explain things in a very,

361
00:20:53,140 --> 00:20:57,100
 sometimes not always, but often

362
00:20:57,100 --> 00:21:00,200
 in a very clear and logical manner.

363
00:21:00,200 --> 00:21:03,700
 But the other side of that,

364
00:21:03,700 --> 00:21:05,620
 and it's the last thing I'll say is that

365
00:21:05,620 --> 00:21:11,580
 to some extent it's just our purity of mind.

366
00:21:11,580 --> 00:21:14,040
 And to some extent goodness is enough.

367
00:21:14,040 --> 00:21:17,040
 Really Buddhism ultimately isn't an artificial,

368
00:21:17,040 --> 00:21:19,620
 religious thing, institution.

369
00:21:19,620 --> 00:21:21,400
 It's just about seeing reality.

370
00:21:21,400 --> 00:21:26,860
 And so in regards to life practice being difficult

371
00:21:26,860 --> 00:21:28,960
 in lay life and the struggle,

372
00:21:31,060 --> 00:21:35,380
 to many, in many respects,

373
00:21:35,380 --> 00:21:38,380
 this is just our failing as Buddhist practitioners.

374
00:21:38,380 --> 00:21:48,360
 And it's what we have to recognize that we are incomplete.

375
00:21:48,360 --> 00:21:54,300
 Often we make goals, we fixate on the goal

376
00:21:54,300 --> 00:21:57,900
 and okay, I'm going to practice and it's going to work

377
00:21:57,900 --> 00:21:59,420
 and I'm going to become enlightened.

378
00:21:59,420 --> 00:22:02,140
 When in fact it may take you lifetimes

379
00:22:02,140 --> 00:22:03,620
 to become fully enlightened.

380
00:22:03,620 --> 00:22:05,940
 And it certainly may take much more

381
00:22:05,940 --> 00:22:08,100
 than just sitting down and practicing meditation,

382
00:22:08,100 --> 00:22:12,660
 not to trivialize the core importance of meditation.

383
00:22:12,660 --> 00:22:15,600
 Of course, it's by far the most important thing.

384
00:22:15,600 --> 00:22:23,840
 But rather to say that our meditation might be difficult

385
00:22:23,840 --> 00:22:27,380
 and problematic, take the example of a person

386
00:22:27,380 --> 00:22:30,700
 who stops meditating, wants to get back into meditation

387
00:22:30,700 --> 00:22:33,140
 but finds themselves distracted by so many things.

388
00:22:33,140 --> 00:22:38,260
 Their perspective, I think it's important that it changes.

389
00:22:38,260 --> 00:22:41,260
 That they, rather than think of it,

390
00:22:41,260 --> 00:22:43,900
 oh, I'm not meditating and therefore I'm failing or so on,

391
00:22:43,900 --> 00:22:45,380
 or this person has stopped meditating

392
00:22:45,380 --> 00:22:47,040
 and therefore they're failing.

393
00:22:47,040 --> 00:22:50,700
 We should look rather at our situation

394
00:22:50,700 --> 00:22:54,100
 and how we can get closer to freedom from suffering.

395
00:22:55,540 --> 00:22:57,660
 And that's not always through meditation,

396
00:22:57,660 --> 00:23:03,500
 but I think it always is through things like mindfulness,

397
00:23:03,500 --> 00:23:06,000
 walking down the street and mindfully.

398
00:23:06,000 --> 00:23:08,000
 Having a different perspective instead of saying,

399
00:23:08,000 --> 00:23:10,700
 I'm a failure so I just might as well give up.

400
00:23:10,700 --> 00:23:12,620
 To say, no, when I was walking down the street,

401
00:23:12,620 --> 00:23:14,860
 I was mindful or right now I'm walking down the street

402
00:23:14,860 --> 00:23:15,980
 and I can be mindful.

403
00:23:15,980 --> 00:23:21,340
 We don't need Buddhist rituals and they certainly don't

404
00:23:21,340 --> 00:23:24,220
 save us from our lack of meditation practice.

405
00:23:25,120 --> 00:23:28,060
 But mindfulness is much more important, of course,

406
00:23:28,060 --> 00:23:31,700
 than rituals and actually in some ways more important

407
00:23:31,700 --> 00:23:33,620
 than what we call practice.

408
00:23:33,620 --> 00:23:36,380
 A person can do hours and hours of meditation practice

409
00:23:36,380 --> 00:23:37,860
 and never get anywhere

410
00:23:37,860 --> 00:23:39,760
 if they're not actually being mindful.

411
00:23:39,760 --> 00:23:44,340
 My teacher said this and he said, in opposition,

412
00:23:44,340 --> 00:23:47,060
 there's a story of one monk who became enlightened

413
00:23:47,060 --> 00:23:48,900
 with three steps, I think it was.

414
00:23:48,900 --> 00:23:52,620
 So walking down the street, if you say walking, walking,

415
00:23:52,620 --> 00:23:54,020
 you can become enlightened just there

416
00:23:54,020 --> 00:23:55,700
 if the conditions are right.

417
00:23:55,700 --> 00:24:03,580
 So I don't, to some extent, that isn't entirely

418
00:24:03,580 --> 00:24:08,820
 satisfying answer, I don't think,

419
00:24:08,820 --> 00:24:13,820
 but I think for the reasons and based on the explanation,

420
00:24:13,820 --> 00:24:19,180
 we have to understand clearly this concept of worldly life

421
00:24:21,340 --> 00:24:26,340
 and enlightenment and all the points in between

422
00:24:26,340 --> 00:24:31,540
 and how we can connect the dots from one to the other.

423
00:24:31,540 --> 00:24:36,300
 So ultimately what I do is teach,

424
00:24:36,300 --> 00:24:39,180
 try to teach meditation practice to those who are

425
00:24:39,180 --> 00:24:39,940
 interested.

426
00:24:39,940 --> 00:24:45,320
 I think at the very least helping people understand

427
00:24:45,320 --> 00:24:48,680
 how to be mindful, which of course is applicable anywhere.

428
00:24:48,680 --> 00:24:50,140
 Practicable.

429
00:24:51,860 --> 00:24:53,260
 Practicable anywhere.

430
00:24:53,260 --> 00:24:57,340
 Is practice and believe in the word?

431
00:24:57,340 --> 00:25:02,020
 Anyway, that's the answer to that question.

432
00:25:02,020 --> 00:25:04,420
 Thank you for asking.

433
00:25:04,420 --> 00:25:05,620
 Thank you for listening.

434
00:25:05,620 --> 00:25:06,620
 (

435
00:25:06,620 --> 00:25:08,620
 music)

