1
00:00:00,000 --> 00:00:03,000
 First sex, now murder.

2
00:00:03,000 --> 00:00:06,000
 So now we have a question about killing people.

3
00:00:06,000 --> 00:00:09,470
 What would you rather do? Kill one person to save a

4
00:00:09,470 --> 00:00:13,000
 thousand or let one live and one thousand must die?

5
00:00:13,000 --> 00:00:19,540
 One of these what-if questions, one of the books full of

6
00:00:19,540 --> 00:00:21,000
 these that you're supposed to torture yourself.

7
00:00:21,000 --> 00:00:23,890
 It's really silly. I mean, there's no... I'm not causing a

8
00:00:23,890 --> 00:00:26,000
 thousand people to die if I let one live.

9
00:00:26,000 --> 00:00:32,030
 I mean, yeah, I could go around hunting out serial killers

10
00:00:32,030 --> 00:00:35,000
 and shooting them and so on.

11
00:00:35,000 --> 00:00:39,440
 I mean, it's such a long, long and complicated answer, but

12
00:00:39,440 --> 00:00:43,680
 this is the argument that we had over buying meat and so on

13
00:00:43,680 --> 00:00:44,000
.

14
00:00:44,000 --> 00:00:47,370
 The point is you're only responsible for the actions that

15
00:00:47,370 --> 00:00:48,000
 you do.

16
00:00:48,000 --> 00:00:53,280
 An action is only unwholesome, is only bad because of the

17
00:00:53,280 --> 00:00:58,000
 nature of the mind of the person who performs it.

18
00:00:58,000 --> 00:01:02,640
 That's it. There's no God telling us this is wrong. There's

19
00:01:02,640 --> 00:01:05,000
 no society telling us that.

20
00:01:05,000 --> 00:01:07,110
 Just because God tells you it's wrong, just because society

21
00:01:07,110 --> 00:01:10,000
 tells you it's wrong, doesn't make anything wrong.

22
00:01:10,000 --> 00:01:16,140
 Socrates pointed this out. Socrates said, or Plato however,

23
00:01:16,140 --> 00:01:19,580
 Socrates said that this man was going to take his parents

24
00:01:19,580 --> 00:01:23,000
 to court because he thought it's what the gods wanted.

25
00:01:23,000 --> 00:01:26,550
 And so Socrates asked him, "Well, so is it right to take

26
00:01:26,550 --> 00:01:30,360
 your parents to court because the gods say it's right or do

27
00:01:30,360 --> 00:01:34,000
 the gods say it's right because it's right?"

28
00:01:34,000 --> 00:01:38,860
 And either way you can't evoke the gods. There is no entity

29
00:01:38,860 --> 00:01:43,000
 that could tell you that something is right or wrong.

30
00:01:43,000 --> 00:01:48,530
 Murder could be perfectly right. There's nothing intrinsic

31
00:01:48,530 --> 00:01:54,000
 about the act of killing someone that is in any way wrong.

32
00:01:54,000 --> 00:01:57,190
 If one person kills another, it's meaningless to you. If

33
00:01:57,190 --> 00:02:00,660
 this person kills that person, it's totally unrelated to

34
00:02:00,660 --> 00:02:02,000
 your state of mind.

35
00:02:02,000 --> 00:02:06,380
 You can just watch it and say seeing, seeing. So there's no

36
00:02:06,380 --> 00:02:08,000
 relationship.

37
00:02:08,000 --> 00:02:11,320
 And this is scientific. This isn't Buddhist theory. I mean,

38
00:02:11,320 --> 00:02:13,950
 if you're thinking about this logically and removing your

39
00:02:13,950 --> 00:02:16,000
 emotions from it, if you're thinking of it scientifically,

40
00:02:16,000 --> 00:02:19,780
 this is why scientists have a real problem with ethics and

41
00:02:19,780 --> 00:02:24,210
 why they cut up rats and poison rats and wind up horrible

42
00:02:24,210 --> 00:02:30,350
 people because they are unable to develop a clear code of

43
00:02:30,350 --> 00:02:31,000
 ethics.

44
00:02:31,000 --> 00:02:33,540
 Simply because from a scientific point of view, there's

45
00:02:33,540 --> 00:02:36,000
 nothing wrong with killing. Killing is the end of a life.

46
00:02:36,000 --> 00:02:38,000
 What does it mean? It means nothing.

47
00:02:38,000 --> 00:02:43,840
 Their only understanding of ethics is a religious concept

48
00:02:43,840 --> 00:02:47,000
 that has no basis in reality.

49
00:02:47,000 --> 00:02:51,870
 So Buddhism just points one thing out that not only does

50
00:02:51,870 --> 00:02:56,870
 the physical exist and go by laws of physics, but the mind

51
00:02:56,870 --> 00:02:58,000
 also exists.

52
00:02:58,000 --> 00:03:03,300
 And it goes by mind, by laws of how the mind works. At the

53
00:03:03,300 --> 00:03:06,850
 moment when you kill something, you're creating a problem

54
00:03:06,850 --> 00:03:08,000
 in your mind.

55
00:03:08,000 --> 00:03:10,770
 When you let someone kill someone else, you're not creating

56
00:03:10,770 --> 00:03:16,060
 a problem in your mind. This is the only ethical principle

57
00:03:16,060 --> 00:03:21,000
 that exists in Theravada Buddhism anyway.

58
00:03:21,000 --> 00:03:24,960
 That the actions that you perform will affect your mind,

59
00:03:24,960 --> 00:03:27,000
 the choices that you make.

60
00:03:27,000 --> 00:03:30,760
 So if you're watching someone kill someone else or kill a

61
00:03:30,760 --> 00:03:34,410
 thousand people creates anguish in your mind, then you're

62
00:03:34,410 --> 00:03:36,000
 responsible for the creating of anguish.

63
00:03:36,000 --> 00:03:42,120
 If you could save those people and you choose to not save

64
00:03:42,120 --> 00:03:44,520
 them, if all you had to do was sell them run, that guy's

65
00:03:44,520 --> 00:03:46,750
 got a gun or something like that, or someone's coming with

66
00:03:46,750 --> 00:03:50,000
 a gun, go hide.

67
00:03:50,000 --> 00:03:52,870
 And you don't do that, then you're responsible for that.

68
00:03:52,870 --> 00:03:55,660
 You're not ever responsible for the killing unless you're

69
00:03:55,660 --> 00:03:58,810
 the one pulling the trigger or unless you're the one

70
00:03:58,810 --> 00:04:01,000
 telling a person to kill.

71
00:04:01,000 --> 00:04:03,620
 You're responsible for the way you say this person killed

72
00:04:03,620 --> 00:04:05,540
 that man. You're still not responsible for pulling the

73
00:04:05,540 --> 00:04:08,280
 trigger. You're responsible for telling that person to do

74
00:04:08,280 --> 00:04:09,000
 something.

75
00:04:09,000 --> 00:04:13,010
 You're responsible for the ethical qualities of mind, the

76
00:04:13,010 --> 00:04:16,850
 greed, the anger and the delusion inherent in the mind at

77
00:04:16,850 --> 00:04:18,000
 that moment.

78
00:04:18,000 --> 00:04:21,910
 So if you don't kill the person who's going to kill a

79
00:04:21,910 --> 00:04:26,750
 thousand people, then it may be that unwholesomeness arises

80
00:04:26,750 --> 00:04:29,000
 in your mind as a result.

81
00:04:29,000 --> 00:04:33,850
 You think, "Let those people die. Nothing to do, not my

82
00:04:33,850 --> 00:04:35,000
 problem."

83
00:04:35,000 --> 00:04:37,010
 And so as a result, you create some kind of worry and

84
00:04:37,010 --> 00:04:38,000
 anguish afterwards.

85
00:04:38,000 --> 00:04:42,210
 "I couldn't save them. All I had to do was kill that one

86
00:04:42,210 --> 00:04:43,000
 man."

87
00:04:43,000 --> 00:04:47,250
 But on the other hand, if you kill that one person, then

88
00:04:47,250 --> 00:04:51,890
 you have to say, "What is worse? This guilt that I have

89
00:04:51,890 --> 00:04:56,740
 over those thousand deaths which weren't a result of my

90
00:04:56,740 --> 00:04:58,000
 actions."

91
00:04:58,000 --> 00:05:01,030
 And so the only reason it comes up is because there's some

92
00:05:01,030 --> 00:05:04,300
 intellectualizing or some minor feelings of guilt or the

93
00:05:04,300 --> 00:05:06,000
 feeling of pulling a trigger.

94
00:05:06,000 --> 00:05:10,170
 I think the real reason why people have a problem with this

95
00:05:10,170 --> 00:05:14,260
 and a problem with understanding things like hunting in

96
00:05:14,260 --> 00:05:17,000
 general is because they've never done it themselves.

97
00:05:17,000 --> 00:05:24,000
 If you've never killed a significantly large being like,

98
00:05:24,000 --> 00:05:28,230
 well, my example is a deer because that's how far I went,

99
00:05:28,230 --> 00:05:31,610
 or even more a human being, then you have no real right to

100
00:05:31,610 --> 00:05:33,000
 argue this question.

101
00:05:33,000 --> 00:05:36,400
 And I don't have the right to argue, but if you've read the

102
00:05:36,400 --> 00:05:40,800
 books, if you've read accounts of people who have killed

103
00:05:40,800 --> 00:05:44,000
 and how it affects them, any of these books,

104
00:05:44,000 --> 00:05:47,180
 the book I was referring to earlier was Crime and Pun

105
00:05:47,180 --> 00:05:51,210
ishment. It's just an excellent, excellent description of

106
00:05:51,210 --> 00:05:54,750
 the torture that goes through the mind of someone who kills

107
00:05:54,750 --> 00:05:56,000
 another human being.

108
00:05:56,000 --> 00:05:58,590
 We can't even fathom most of this. We think, "Well, it's

109
00:05:58,590 --> 00:06:01,210
 just intellectualizing, so you kill someone, what's the

110
00:06:01,210 --> 00:06:02,000
 problem?"

111
00:06:02,000 --> 00:06:05,390
 Totally different when you actually try to do it. It's an

112
00:06:05,390 --> 00:06:07,000
 incredibly powerful act.

113
00:06:07,000 --> 00:06:09,770
 The only comparison I have is, as I said, with hunting,

114
00:06:09,770 --> 00:06:11,000
 with killing a deer.

115
00:06:11,000 --> 00:06:15,020
 I had no compunction, no problem with killing, and I killed

116
00:06:15,020 --> 00:06:19,000
 insects and small animals, and no problem with hunting.

117
00:06:19,000 --> 00:06:23,650
 I got my hunting license trained and so on, and then I sat

118
00:06:23,650 --> 00:06:28,530
 up in the tree, and when the deer came, raising this cross

119
00:06:28,530 --> 00:06:31,740
bow, ready to pull the trigger, and suddenly my whole body

120
00:06:31,740 --> 00:06:33,000
 started to shake.

121
00:06:33,000 --> 00:06:36,210
 The deer was no threat to me. There was no fear. There

122
00:06:36,210 --> 00:06:38,000
 should have been no fear in my mind.

123
00:06:38,000 --> 00:06:42,910
 It wasn't like there was some danger to me, but I just got

124
00:06:42,910 --> 00:06:46,700
 this incredible dread and revulsion to it, and my whole

125
00:06:46,700 --> 00:06:48,000
 body started to shake,

126
00:06:48,000 --> 00:06:51,560
 and I could barely pull the trigger. That's how powerful it

127
00:06:51,560 --> 00:06:52,000
 is.

128
00:06:52,000 --> 00:06:56,690
 This wasn't totally not intellectual, because I had no

129
00:06:56,690 --> 00:06:59,000
 feelings of hesitation.

130
00:06:59,000 --> 00:07:03,070
 I was perfectly ready to do this, and yet when it came time

131
00:07:03,070 --> 00:07:06,510
 to do that, because I had never done it, because my mind

132
00:07:06,510 --> 00:07:09,000
 was still pure of that.

133
00:07:09,000 --> 00:07:11,930
 It had an incredible effect on my mind, and this is what

134
00:07:11,930 --> 00:07:14,000
 you're looking at if you kill someone.

135
00:07:14,000 --> 00:07:16,840
 If you're just talking about the guilt feelings of not

136
00:07:16,840 --> 00:07:21,100
 having prevented other people's death, when actually in the

137
00:07:21,100 --> 00:07:24,000
 end all of those people are going to have to die anyway,

138
00:07:24,000 --> 00:07:27,620
 whether it be from a bullet wound or whether it be from old

139
00:07:27,620 --> 00:07:30,000
 age, everyone dies in horrible ways.

140
00:07:30,000 --> 00:07:35,030
 You can't possibly save even a small portion of the people

141
00:07:35,030 --> 00:07:38,000
 who have to die in horrible ways.

142
00:07:38,000 --> 00:07:42,750
 If it's just a matter of feeling that kind of guilt from

143
00:07:42,750 --> 00:07:48,850
 not having prevented the inevitable or put off the

144
00:07:48,850 --> 00:07:52,000
 inevitable,

145
00:07:52,000 --> 00:07:55,890
 then it's far preferable to the horror of having killed a

146
00:07:55,890 --> 00:08:02,000
 human being, even an evil human being.

147
00:08:02,000 --> 00:08:07,000
 I've read other accounts of this, I can't remember where,

148
00:08:07,000 --> 00:08:11,000
 but police officers who have had to kill humans,

149
00:08:11,000 --> 00:08:19,170
 the one is post-traumatic stress disorder, soldiers who go

150
00:08:19,170 --> 00:08:22,220
 to war and will come back and just don't want to ever talk

151
00:08:22,220 --> 00:08:23,000
 about it again.

152
00:08:23,000 --> 00:08:26,760
 They've just locked off that part of their mind. It's just

153
00:08:26,760 --> 00:08:28,000
 so horrific.

154
00:08:28,000 --> 00:08:31,980
 If you've never been to war, you can't imagine the horrors

155
00:08:31,980 --> 00:08:36,210
 of war and people who are not able to sleep at night

156
00:08:36,210 --> 00:08:39,000
 because of what they've seen and what they've done.

157
00:08:39,000 --> 00:08:43,480
 Sumedha was telling us about these caravans that they had

158
00:08:43,480 --> 00:08:47,000
 to drive from point A to point B and they weren't allowed

159
00:08:47,000 --> 00:08:48,000
 to stop for anything.

160
00:08:48,000 --> 00:08:52,020
 A man, woman, child standing in the middle of the road had

161
00:08:52,020 --> 00:08:54,000
 to be run over basically.

162
00:08:54,000 --> 00:08:58,000
 This kind of thing, maybe that's going quite far afield,

163
00:08:58,000 --> 00:09:05,850
 but the horror that is involved with these visceral acts or

164
00:09:05,850 --> 00:09:14,000
 these real acts of body,

165
00:09:14,000 --> 00:09:16,000
 there's an incredible power there.

166
00:09:16,000 --> 00:09:19,160
 But the point is that there's no intellectualizing in

167
00:09:19,160 --> 00:09:24,000
 Buddhism. Buddhism is based on experience.

168
00:09:24,000 --> 00:09:27,140
 You can't come up with a theory of morality and call it

169
00:09:27,140 --> 00:09:28,000
 Buddhist.

170
00:09:28,000 --> 00:09:33,230
 Buddhism is you are responsible for your own actions and

171
00:09:33,230 --> 00:09:36,780
 you can never escape that responsibility and you're

172
00:09:36,780 --> 00:09:39,000
 responsible for nothing else.

173
00:09:39,000 --> 00:09:47,180
 And that is scientific. It's verifiable. You could never

174
00:09:47,180 --> 00:09:51,060
 verify what is the guilt of letting someone kill someone

175
00:09:51,060 --> 00:09:52,000
 else.

176
00:09:52,000 --> 00:09:55,140
 You can only verify what is the guilt or what is the

177
00:09:55,140 --> 00:09:58,000
 suffering involved in your own actions.

178
00:09:58,000 --> 00:10:00,660
 So if you choose to feel guilty about it, that's going to

179
00:10:00,660 --> 00:10:02,000
 create suffering for you.

180
00:10:02,000 --> 00:10:05,980
 If you choose to kill a person, to not feel guilty, then

181
00:10:05,980 --> 00:10:10,320
 that's going to cause, I would say as well, or even more

182
00:10:10,320 --> 00:10:16,030
 intense feelings of horror and nightmares because now you

183
00:10:16,030 --> 00:10:17,000
're a murderer.

184
00:10:17,000 --> 00:10:21,000
 Whether it was a justified murder or not, it was murder.

185
00:10:21,000 --> 00:10:40,000
 [

