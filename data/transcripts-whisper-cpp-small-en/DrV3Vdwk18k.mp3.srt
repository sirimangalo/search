1
00:00:00,000 --> 00:00:02,000
 Okay, go ahead.

2
00:00:02,000 --> 00:00:06,190
 If the desires are the root of suffering, is there anything

3
00:00:06,190 --> 00:00:08,000
 inherently wrong with them?

4
00:00:08,000 --> 00:00:13,340
 How do you best acknowledge and put to good use our desire

5
00:00:13,340 --> 00:00:15,440
 without getting carried away

6
00:00:15,440 --> 00:00:18,200
 by their passions? Yeah.

7
00:00:18,200 --> 00:00:19,960
 Well,

8
00:00:19,960 --> 00:00:23,040
 that's an interesting question. I don't quite get the

9
00:00:23,040 --> 00:00:23,600
 logical

10
00:00:25,040 --> 00:00:30,050
 movement. So the premise is if desires are the, the premise

11
00:00:30,050 --> 00:00:31,120
 is that desires are the root of suffering.

12
00:00:31,120 --> 00:00:36,670
 And then somehow that leads you to the conclusion that

13
00:00:36,670 --> 00:00:38,520
 there may not be anything wrong with them.

14
00:00:38,520 --> 00:00:41,460
 I don't see the logic there. If they are the root of

15
00:00:41,460 --> 00:00:43,820
 suffering, how could there be anything good about them?

16
00:00:43,820 --> 00:00:47,320
 I mean, doesn't, doesn't, doesn't, isn't, isn't,

17
00:00:49,920 --> 00:00:53,280
 I don't, I don't get it. If something is the desire, if

18
00:00:53,280 --> 00:00:56,320
 something, if anything were the root of suffering,

19
00:00:56,320 --> 00:01:00,850
 isn't that a sign that there's something inherently wrong

20
00:01:00,850 --> 00:01:01,480
 with them?

21
00:01:01,480 --> 00:01:06,000
 I don't know. So, okay, so, so something is a little bit

22
00:01:06,000 --> 00:01:07,920
 unclear there to me.

23
00:01:07,920 --> 00:01:12,660
 But, wait, wait, let me, but, but I want to just add

24
00:01:12,660 --> 00:01:15,340
 something here because he's, he's got an, there's another

25
00:01:15,340 --> 00:01:16,880
 point involved with this and that's

26
00:01:19,400 --> 00:01:24,350
 using desires for good use without getting carried away

27
00:01:24,350 --> 00:01:25,600
 from them. Now,

28
00:01:25,600 --> 00:01:27,820
 maybe the idea that you're trying to, you're trying to

29
00:01:27,820 --> 00:01:31,320
 present is that desires can cause suffering.

30
00:01:31,320 --> 00:01:33,760
 Desires are the root of suffering in the sense that they

31
00:01:33,760 --> 00:01:36,280
 can cause suffering. That's not Buddhist theory.

32
00:01:36,280 --> 00:01:41,630
 Desires do cause suffering. If you, if you're referring to

33
00:01:41,630 --> 00:01:45,400
 the mental state of wanting something or,

34
00:01:46,480 --> 00:01:50,370
 instead of just the intention that we call desire, but if

35
00:01:50,370 --> 00:01:54,400
 it's an actual desire, it can't help but

36
00:01:54,400 --> 00:01:58,920
 bring, if it brings a result, it can't help but bring

37
00:01:58,920 --> 00:02:02,360
 suffering as a result. Now, it's possible that they can be

38
00:02:02,360 --> 00:02:07,040
 nullified. So, not all desires do bring results.

39
00:02:07,040 --> 00:02:11,660
 Sketchy may not bring results depending on what theory you

40
00:02:11,660 --> 00:02:13,200
 subscribe to, but

41
00:02:14,120 --> 00:02:17,320
 in general, let's say, they do bring results and there's no

42
00:02:17,320 --> 00:02:20,640
 way that a desire could bring a positive result.

43
00:02:20,640 --> 00:02:23,250
 The Buddha is very, very clear about this, specifically

44
00:02:23,250 --> 00:02:23,640
 clear.

45
00:02:23,640 --> 00:02:32,520
 Desire can only bring, I just lost it now, but in the, in

46
00:02:32,520 --> 00:02:33,680
 the Anguttarnikaya Book of Ones,

47
00:02:33,680 --> 00:02:37,240
 desire can only bring

48
00:02:37,240 --> 00:02:40,440
 suffering.

49
00:02:41,800 --> 00:02:48,300
 Anyway, so no, no, no way, no way to use desires for, but I

50
00:02:48,300 --> 00:02:48,640
 mean,

51
00:02:48,640 --> 00:02:52,660
 if you, if you ask someone else, they might say some, some,

52
00:02:52,660 --> 00:02:54,660
 some people I think might say,

53
00:02:54,660 --> 00:02:59,760
 desires are not so bad. Just don't, as you say, let them be

54
00:02:59,760 --> 00:03:00,720
 compassion's or

55
00:03:00,720 --> 00:03:04,360
 let yourself get carried away by them, but that's not

56
00:03:04,360 --> 00:03:05,720
 technically

57
00:03:05,720 --> 00:03:09,920
 honest.

58
00:03:09,920 --> 00:03:13,000
 You can, you know, I can couch what I said in those kind of

59
00:03:13,000 --> 00:03:14,200
 words and say, you know,

60
00:03:14,200 --> 00:03:16,990
 don't worry too much and that's a good advice. Don't worry

61
00:03:16,990 --> 00:03:18,560
 too much about your desires.

62
00:03:18,560 --> 00:03:22,600
 You know, try to learn about them and learn about the bad

63
00:03:22,600 --> 00:03:26,410
 desires and passions and that's good advice because once

64
00:03:26,410 --> 00:03:27,520
 you do that, you'll start,

65
00:03:27,520 --> 00:03:31,350
 because you can't hope, as a person who's never practiced

66
00:03:31,350 --> 00:03:32,720
 Buddhism, can't hope to

67
00:03:32,720 --> 00:03:35,320
 get close to their,

68
00:03:35,320 --> 00:03:37,720
 you might say,

69
00:03:37,720 --> 00:03:41,070
 innocent desires. Yeah, you would say innocent desires. So

70
00:03:41,070 --> 00:03:42,400
 desires that are not

71
00:03:42,400 --> 00:03:45,440
 evil, evil, evil,

72
00:03:45,440 --> 00:03:48,770
 evil in a, in a, in a conventional worldly sense. So

73
00:03:48,770 --> 00:03:51,840
 desires that don't lead you to kill, steal, lie, cheat.

74
00:03:51,840 --> 00:03:57,200
 Kill, steal, lie, cheat. Yeah, and so on.

75
00:04:03,880 --> 00:04:07,090
 Lost it totally. Yes, the innocent desires, but you can get

76
00:04:07,090 --> 00:04:07,720
 rid of them

77
00:04:07,720 --> 00:04:10,880
 because you're,

78
00:04:10,880 --> 00:04:14,160
 well, because you're very much attached to them,

79
00:04:14,160 --> 00:04:17,420
 but because there's all these coarse and, and crazy defile

80
00:04:17,420 --> 00:04:20,000
ments because you still want to kill, steal, lie, cheat.

81
00:04:20,000 --> 00:04:23,430
 And so until you get rid of those, you're not going to be

82
00:04:23,430 --> 00:04:25,560
 able to see clearly the more subtle ones.

83
00:04:25,560 --> 00:04:28,560
 You're not going to have a chance to get rid of them.

84
00:04:28,560 --> 00:04:30,920
 And

85
00:04:30,920 --> 00:04:34,740
 so, so taking it step by step, it's very good advice to

86
00:04:34,740 --> 00:04:36,040
 tell you just to

87
00:04:36,040 --> 00:04:42,620
 to deal, to try to deal with the passions and so on, and to

88
00:04:42,620 --> 00:04:46,400
 not worry about them. So in that sense, I could answer this

89
00:04:46,400 --> 00:04:46,400
.

90
00:04:46,400 --> 00:04:48,680
 How do we,

91
00:04:48,680 --> 00:04:52,090
 we can't put to good use your desires because what I mean

92
00:04:52,090 --> 00:04:55,720
 by not being honest is that all desires are negative and

93
00:04:55,720 --> 00:04:59,340
 even though we might put up with them, we certainly can't

94
00:04:59,340 --> 00:05:00,240
 put them to good use.

95
00:05:02,240 --> 00:05:04,880
 But how do we, how would we, if your question was put up

96
00:05:04,880 --> 00:05:09,280
 with them without getting carried away by them?

97
00:05:09,280 --> 00:05:13,260
 I think maybe that's just a good way of summing it up, put

98
00:05:13,260 --> 00:05:17,320
 up with them without getting carried away with them.

99
00:05:17,320 --> 00:05:21,950
 Deal with the getting carried away part first because that

100
00:05:21,950 --> 00:05:26,000
's much more important and deal with the views that

101
00:05:26,000 --> 00:05:29,080
 lead you to get carried away, first of all.

102
00:05:29,640 --> 00:05:31,640
 They'll deal with your ideas and

103
00:05:31,640 --> 00:05:36,590
 and so on. I mean a lot of these how do I deal with

104
00:05:36,590 --> 00:05:40,040
 questions are really dishonest, not dishonest, but

105
00:05:40,040 --> 00:05:44,760
 it's dishonest of me to give you a direct answer

106
00:05:44,760 --> 00:05:48,000
 because

107
00:05:48,000 --> 00:05:50,400
 that's not the point of Buddhism. The point of Buddhism is

108
00:05:50,400 --> 00:05:53,390
 not to deal with. The point of Buddhism is to get rid of,

109
00:05:53,390 --> 00:05:53,600
 to be

110
00:05:53,600 --> 00:05:54,640
 free from.

111
00:05:54,640 --> 00:05:58,240
 So the answer in all cases is don't put up with it. If you

112
00:05:58,240 --> 00:06:00,160
 can't become a monk at least

113
00:06:00,160 --> 00:06:02,720
 start meditating, do some

114
00:06:02,720 --> 00:06:06,220
 serious meditation, take time to do a meditation course if

115
00:06:06,220 --> 00:06:09,000
 you can. If you really really want to

116
00:06:09,000 --> 00:06:12,240
 have answers to all your problems, take at least a

117
00:06:12,240 --> 00:06:13,560
 meditation course.

118
00:06:13,560 --> 00:06:17,960
 You know, hey, I'll be in Canada. I think a lot of people

119
00:06:17,960 --> 00:06:20,680
 here in America, so it's not that far.

120
00:06:21,640 --> 00:06:24,270
 Let's see if we can actually run. Wouldn't it be great if

121
00:06:24,270 --> 00:06:26,280
 we can actually run courses in Canada?

122
00:06:26,280 --> 00:06:29,440
 One day we'll have a place where we can do that.

123
00:06:29,440 --> 00:06:32,920
 We'll see maybe I can have people stay in my apartment

124
00:06:32,920 --> 00:06:36,380
 or something like that. You can come and pitch a tent.

125
00:06:36,380 --> 00:06:39,280
 Maybe it looks like a really nice natural area.

126
00:06:39,280 --> 00:06:43,240
 There's one, Laurie said he would come and pitch a tent and

127
00:06:43,240 --> 00:06:45,560
 it looks like he'll be able to. I'll have to talk to the

128
00:06:45,560 --> 00:06:49,420
 to this, the people supporting me, but they're very, they

129
00:06:49,420 --> 00:06:50,680
 seem very kind and

130
00:06:52,000 --> 00:06:55,440
 they're very accommodating, so we'll see.

131
00:06:55,440 --> 00:07:22,660
 [

