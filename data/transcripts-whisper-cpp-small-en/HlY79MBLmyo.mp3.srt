1
00:00:00,000 --> 00:00:05,820
 I live with a Christian family and I'm trying to fix my

2
00:00:05,820 --> 00:00:07,000
 sleep habits.

3
00:00:07,000 --> 00:00:10,880
 When I am awake they ask me questions and bug me during my

4
00:00:10,880 --> 00:00:12,000
 meditation.

5
00:00:12,000 --> 00:00:18,000
 If I ignore them they get mad. What should I do?

6
00:00:18,000 --> 00:00:23,040
 It's an odd sort of question. I think we need a little more

7
00:00:23,040 --> 00:00:25,000
 information.

8
00:00:25,000 --> 00:00:34,280
 The feeling I'm getting is that maybe you sleep all the

9
00:00:34,280 --> 00:00:38,000
 time because when you're awake they ask you questions

10
00:00:38,000 --> 00:00:41,080
 and you want to stop that but you don't want to be awake

11
00:00:41,080 --> 00:00:47,000
 because they ask you questions.

12
00:00:47,000 --> 00:00:49,950
 I'm not quite sure how to answer it. The second part is

13
00:00:49,950 --> 00:00:51,000
 quite easy to answer.

14
00:00:51,000 --> 00:00:55,610
 But the whole thing about the sleep habits seems like a bit

15
00:00:55,610 --> 00:00:59,000
 of a non sequitur.

16
00:00:59,000 --> 00:01:02,960
 I don't quite see how the sleeping habits relates to people

17
00:01:02,960 --> 00:01:04,000
 making you mad

18
00:01:04,000 --> 00:01:10,520
 except maybe you can't sleep because they're always bugging

19
00:01:10,520 --> 00:01:11,000
 you.

20
00:01:11,000 --> 00:01:19,940
 So how do we answer this? If people bug you when you're

21
00:01:19,940 --> 00:01:21,000
 awake and when you're practicing

22
00:01:21,000 --> 00:01:25,550
 they ask you questions when you're awake and bug you during

23
00:01:25,550 --> 00:01:27,000
 your meditation.

24
00:01:27,000 --> 00:01:32,000
 If you ignore them they get mad. What should you do?

25
00:01:32,000 --> 00:01:40,040
 Well, when people ask you questions that's a wonderful

26
00:01:40,040 --> 00:01:42,000
 thing I think.

27
00:01:42,000 --> 00:01:49,000
 I think the bad part is when people stop asking questions

28
00:01:49,000 --> 00:01:52,000
 when there is no dialogue.

29
00:01:52,000 --> 00:01:55,780
 And I assume eventually you'll come to that once you're

30
00:01:55,780 --> 00:01:58,000
 able to answer their questions

31
00:01:58,000 --> 00:02:01,490
 and ask your own questions because they won't be able to

32
00:02:01,490 --> 00:02:03,000
 answer your questions

33
00:02:03,000 --> 00:02:10,980
 but through the practice you should be able to answer their

34
00:02:10,980 --> 00:02:13,000
 questions.

35
00:02:13,000 --> 00:02:16,140
 I see. You're usually up at night and trying to make it

36
00:02:16,140 --> 00:02:19,000
 during the day they are up during the day.

37
00:02:19,000 --> 00:02:21,290
 When I practice during the day they ask me questions about

38
00:02:21,290 --> 00:02:22,000
 what I'm doing

39
00:02:22,000 --> 00:02:35,000
 and why they don't like me practicing.

40
00:02:35,000 --> 00:02:40,000
 Well, you're practicing to make yourself a better person.

41
00:02:40,000 --> 00:02:43,000
 You're practicing to clear your mind, to calm your mind.

42
00:02:43,000 --> 00:02:45,620
 If they're Christians they should understand because Jesus

43
00:02:45,620 --> 00:02:48,000
 himself went off into the jungle

44
00:02:48,000 --> 00:02:57,420
 and practiced meditation but of course they have different

45
00:02:57,420 --> 00:03:03,000
 ideas and theories about it.

46
00:03:03,000 --> 00:03:05,550
 I think you can take that route with them especially if

47
00:03:05,550 --> 00:03:07,000
 they're getting angry at you.

48
00:03:07,000 --> 00:03:09,820
 I mean you stick with your answers that you're doing it to

49
00:03:09,820 --> 00:03:11,000
 become a better person

50
00:03:11,000 --> 00:03:16,230
 because you have anger inside and you have aversion towards

51
00:03:16,230 --> 00:03:19,000
 things like people bugging you.

52
00:03:19,000 --> 00:03:23,000
 You have attachments and delusions and so on.

53
00:03:23,000 --> 00:03:26,920
 I mean you admit this to them and tell them that's why you

54
00:03:26,920 --> 00:03:28,000
're practicing

55
00:03:28,000 --> 00:03:30,480
 because when you practice it helps you to learn about

56
00:03:30,480 --> 00:03:31,000
 yourself.

57
00:03:31,000 --> 00:03:41,000
 It helps you to see what's causing suffering

58
00:03:41,000 --> 00:03:44,320
 and once you see what's causing suffering to change

59
00:03:44,320 --> 00:03:48,000
 yourself and to become a better person.

60
00:03:48,000 --> 00:03:51,970
 And then if they argue with that, well, you can always

61
00:03:51,970 --> 00:03:54,000
 answer their questions to a certain point

62
00:03:54,000 --> 00:03:57,430
 if there are questions just become silly and then you can

63
00:03:57,430 --> 00:03:59,000
 just ignore them

64
00:03:59,000 --> 00:04:02,170
 and when they get angry at you, I mean that's at least you

65
00:04:02,170 --> 00:04:03,000
've made it clear that

66
00:04:03,000 --> 00:04:05,610
 this is what you're trying to do away with so then you can

67
00:04:05,610 --> 00:04:07,000
 ask them,

68
00:04:07,000 --> 00:04:10,110
 "So when you're angry at me like this, how does it feel? Do

69
00:04:10,110 --> 00:04:12,000
 you feel good being angry like that?

70
00:04:12,000 --> 00:04:20,480
 Does it make you satisfied to get angry like this? How do

71
00:04:20,480 --> 00:04:23,000
 you feel when you're angry?"

72
00:04:23,000 --> 00:04:27,110
 And the idea is that they will be able to answer that it's

73
00:04:27,110 --> 00:04:28,000
 not pleasant

74
00:04:28,000 --> 00:04:30,790
 but that's not the point. The point is you're making me

75
00:04:30,790 --> 00:04:31,000
 angry

76
00:04:31,000 --> 00:04:35,600
 and you can say, "Well, I can't make you angry just as you

77
00:04:35,600 --> 00:04:37,000
 can't make me angry."

78
00:04:37,000 --> 00:04:43,290
 The anger comes from within yourself and it comes from your

79
00:04:43,290 --> 00:04:44,000
 inability to

80
00:04:44,000 --> 00:04:52,390
 or your preference for something else, your inability to

81
00:04:52,390 --> 00:04:54,000
 bear what's going on

82
00:04:54,000 --> 00:04:56,450
 and it's a cause of suffering and that's why I'm practicing

83
00:04:56,450 --> 00:04:57,000
 meditation.

84
00:04:57,000 --> 00:05:01,000
 This is why I'm practicing meditation because I understand

85
00:05:01,000 --> 00:05:03,000
 that anger is something that brings suffering.

86
00:05:03,000 --> 00:05:06,000
 It's something that's useless, it brings conflict.

87
00:05:06,000 --> 00:05:10,130
 Anger is something that causes suffering and others, causes

88
00:05:10,130 --> 00:05:13,000
 stress and destroys friendships,

89
00:05:13,000 --> 00:05:16,000
 destroys family bonds and so on.

90
00:05:16,000 --> 00:05:19,000
 So you help them to see what's really the problem.

91
00:05:19,000 --> 00:05:21,350
 The problem isn't that you're not Christian or not

92
00:05:21,350 --> 00:05:26,000
 practicing the teachings of Jesus Christ.

93
00:05:26,000 --> 00:05:31,120
 The problem is that we all have defilements. If we didn't

94
00:05:31,120 --> 00:05:35,000
 have defilements there would be no problem.

95
00:05:35,000 --> 00:05:39,000
 But as far as your sleeping habits, it's not a real problem

96
00:05:39,000 --> 00:05:39,000
.

97
00:05:39,000 --> 00:05:43,170
 In the worst case, you could just sleep all day and stay up

98
00:05:43,170 --> 00:05:46,000
 all night and avoid argument.

99
00:05:46,000 --> 00:05:49,680
 But you should make it clear to them that from your point

100
00:05:49,680 --> 00:05:53,290
 of view the problem is not that you're practicing something

101
00:05:53,290 --> 00:05:54,000
 outside of Christianity.

102
00:05:54,000 --> 00:05:58,720
 The problem is that you and they have defilements and that

103
00:05:58,720 --> 00:06:01,000
's a real problem that this world has.

104
00:06:01,000 --> 00:06:03,000
 You can point out to them what's going on in the world.

105
00:06:03,000 --> 00:06:06,810
 Is the world's problems because people don't follow Christ

106
00:06:06,810 --> 00:06:09,000
 or is it because people have anger?

107
00:06:09,000 --> 00:06:14,000
 Is it because people have this or that defilement?

108
00:06:14,000 --> 00:06:16,880
 And you can point out that actually what it seems like the

109
00:06:16,880 --> 00:06:21,000
 teachings of Christ are very much to purify your mind,

110
00:06:21,000 --> 00:06:24,000
 to make yourself a better person.

111
00:06:24,000 --> 00:06:27,000
 So I hope that helps.

