1
00:00:00,000 --> 00:00:05,000
 Hi, so today I will continue this series on how to meditate

2
00:00:05,000 --> 00:00:06,000
.

3
00:00:06,000 --> 00:00:09,250
 In this series I will explain a little bit more about the

4
00:00:09,250 --> 00:00:11,320
 technique during sitting meditation

5
00:00:11,320 --> 00:00:14,360
 or even during walking meditation.

6
00:00:14,360 --> 00:00:19,060
 And then I will give a demonstration of how to practice

7
00:00:19,060 --> 00:00:21,200
 walking meditation.

8
00:00:21,200 --> 00:00:25,080
 First of all, the foundations of meditation practice there

9
00:00:25,080 --> 00:00:25,880
 are four.

10
00:00:25,880 --> 00:00:29,840
 And these are the foundation objects on which we learn to

11
00:00:29,840 --> 00:00:32,160
 build what is called mindfulness

12
00:00:32,160 --> 00:00:36,740
 or remembrance, being aware and creating this clear thought

13
00:00:36,740 --> 00:00:39,080
 in our minds which is not clinging

14
00:00:39,080 --> 00:00:43,360
 or running away from the objects of awareness.

15
00:00:43,360 --> 00:00:48,170
 So the foundations on which we build mindfulness are the

16
00:00:48,170 --> 00:00:52,160
 body, the feelings, the mind and something

17
00:00:52,160 --> 00:00:55,770
 called Dhamma which means reality but it means something

18
00:00:55,770 --> 00:00:57,880
 which exists in the mind or something

19
00:00:57,880 --> 00:01:00,680
 which the mind takes as an object.

20
00:01:00,680 --> 00:01:02,760
 We generally call it mind object.

21
00:01:02,760 --> 00:01:08,060
 So we have body, feelings, mind and mind objects.

22
00:01:08,060 --> 00:01:10,980
 The body is either walking or sitting and when we do the

23
00:01:10,980 --> 00:01:12,840
 walking meditation we are going

24
00:01:12,840 --> 00:01:14,920
 to pay attention to the foot.

25
00:01:14,920 --> 00:01:17,670
 Our mind should be with the foot and our mindfulness should

26
00:01:17,670 --> 00:01:19,840
 be established saying to ourselves,

27
00:01:19,840 --> 00:01:24,880
 "Step being right, step being left and this is mindfulness

28
00:01:24,880 --> 00:01:26,160
 of the body."

29
00:01:26,160 --> 00:01:29,440
 Now as we are practicing or when we do sitting meditation,

30
00:01:29,440 --> 00:01:31,840
 rising, falling, this is mindfulness

31
00:01:31,840 --> 00:01:33,120
 of the body.

32
00:01:33,120 --> 00:01:37,270
 But as we are practicing we have the other three feelings,

33
00:01:37,270 --> 00:01:39,240
 the mind and mind objects,

34
00:01:39,240 --> 00:01:42,800
 things which the mind takes as an object.

35
00:01:42,800 --> 00:01:44,300
 First of all, feelings.

36
00:01:44,300 --> 00:01:46,280
 So when we are doing sitting or when we are doing walking

37
00:01:46,280 --> 00:01:47,480
 meditation there might arise

38
00:01:47,480 --> 00:01:50,360
 pain or aching or soreness.

39
00:01:50,360 --> 00:01:53,800
 When we are walking we stop first and then say to ourselves

40
00:01:53,800 --> 00:01:55,560
, "Pain, pain, pain."

41
00:01:55,560 --> 00:01:58,970
 When we are sitting we simply switch our attention to the

42
00:01:58,970 --> 00:02:01,560
 pain or to the aching or to the soreness.

43
00:02:01,560 --> 00:02:02,560
 This is called feelings.

44
00:02:02,560 --> 00:02:05,560
 We say pain, pain, pain.

45
00:02:05,560 --> 00:02:08,600
 Or if we feel happy, happy, happy.

46
00:02:08,600 --> 00:02:12,800
 If we feel calm, we say to ourselves, "Calm, calm, calm."

47
00:02:12,800 --> 00:02:15,790
 This creates a clear thought about the feeling, not

48
00:02:15,790 --> 00:02:18,440
 attaching to it, wanting for it to continue

49
00:02:18,440 --> 00:02:21,800
 or wanting for it to stop.

50
00:02:21,800 --> 00:02:24,720
 Number three, mindfulness of the mind.

51
00:02:24,720 --> 00:02:28,000
 The mind is any sort of thinking.

52
00:02:28,000 --> 00:02:30,600
 When we are thinking about the past or thinking about the

53
00:02:30,600 --> 00:02:32,520
 future, either good thinking or bad

54
00:02:32,520 --> 00:02:34,940
 thinking, whatever kind of thinking it is, we simply say to

55
00:02:34,940 --> 00:02:37,120
 ourselves, "Thinking, thinking,

56
00:02:37,120 --> 00:02:38,120
 thinking."

57
00:02:38,120 --> 00:02:41,770
 We bring the mind back to the rising, falling or to the

58
00:02:41,770 --> 00:02:43,760
 foot when we are walking.

59
00:02:43,760 --> 00:02:46,760
 Number four is the mind objects.

60
00:02:46,760 --> 00:02:50,560
 In the beginning this refers simply to emotional states of

61
00:02:50,560 --> 00:02:51,160
 mind.

62
00:02:51,160 --> 00:02:53,280
 Here we have the five hindrances.

63
00:02:53,280 --> 00:02:57,030
 These are states of mind which will disturb the meditator's

64
00:02:57,030 --> 00:02:59,880
 practice, will become an obstacle

65
00:02:59,880 --> 00:03:01,560
 in the practice of mindfulness.

66
00:03:01,560 --> 00:03:03,600
 We want to see clearly, we don't see clearly.

67
00:03:03,600 --> 00:03:06,000
 We want to be mindful, we can't be mindful.

68
00:03:06,000 --> 00:03:08,970
 We want to sit at peace and be calm and we are not peaceful

69
00:03:08,970 --> 00:03:09,640
 or calm.

70
00:03:09,640 --> 00:03:13,480
 It's because of these five things which still certainly

71
00:03:13,480 --> 00:03:14,920
 exist in the mind.

72
00:03:14,920 --> 00:03:19,960
 These are liking, disliking, drowsiness, distraction and

73
00:03:19,960 --> 00:03:20,800
 doubt.

74
00:03:20,800 --> 00:03:24,240
 If these exist in the mind, we simply say to ourselves, "L

75
00:03:24,240 --> 00:03:26,160
iking, liking, disliking,

76
00:03:26,160 --> 00:03:31,810
 disliking, drowsy, drowsy, distracted, distracted," or doub

77
00:03:31,810 --> 00:03:33,600
ting, doubting.

78
00:03:33,600 --> 00:03:35,760
 Once they go away, then we come back again to the rising,

79
00:03:35,760 --> 00:03:37,680
 falling or when we are walking,

80
00:03:37,680 --> 00:03:41,060
 we've stopped and then we continue walking again after we

81
00:03:41,060 --> 00:03:42,320
've acknowledged.

82
00:03:42,320 --> 00:03:45,350
 These are called the four foundations of mindfulness and

83
00:03:45,350 --> 00:03:47,440
 these are the foundation on which we will

84
00:03:47,440 --> 00:03:51,920
 build our clear thought, our clear awareness, our clear

85
00:03:51,920 --> 00:03:54,480
 understanding of the object.

86
00:03:54,480 --> 00:03:58,710
 Now I'll complete this practice with a demonstration of

87
00:03:58,710 --> 00:04:00,520
 walking meditation.

88
00:04:00,520 --> 00:04:03,480
 This will complete a round of practice and then you can do

89
00:04:03,480 --> 00:04:05,200
 walking first and then sitting

90
00:04:05,200 --> 00:04:07,960
 afterwards, which is a general method.

91
00:04:07,960 --> 00:04:12,070
 So we might walk for 10 minutes and then sit for 10 minutes

92
00:04:12,070 --> 00:04:12,360
.

93
00:04:12,360 --> 00:04:16,840
 Now I'll give a demonstration.

94
00:04:16,840 --> 00:04:20,560
 Walking meditation is performed with the feet close

95
00:04:20,560 --> 00:04:23,320
 together, the right hand holding the

96
00:04:23,320 --> 00:04:32,770
 left hand, either in front or in back and the eyes looking

97
00:04:32,770 --> 00:04:36,280
 at the floor in front of

98
00:04:36,280 --> 00:04:39,160
 you about six feet or two meters out.

99
00:04:39,160 --> 00:04:43,080
 And don't practice with our eyes closed.

100
00:04:43,080 --> 00:04:45,910
 The feet start close together and we start moving with the

101
00:04:45,910 --> 00:04:46,680
 right foot.

102
00:04:46,680 --> 00:04:50,530
 We move the foot at a simple pace, one foot length and say

103
00:04:50,530 --> 00:04:52,320
 to ourselves as we move the

104
00:04:52,320 --> 00:05:02,730
 foot, step being right, step being left, step being right,

105
00:05:02,730 --> 00:05:06,920
 step being left and so on until

106
00:05:06,920 --> 00:05:10,840
 we come to the end of our path, which might be about three

107
00:05:10,840 --> 00:05:12,960
 meters, four meters ahead.

108
00:05:12,960 --> 00:05:15,940
 And then we bring the foot, which is behind up to the front

109
00:05:15,940 --> 00:05:17,360
 and say to ourselves as we

110
00:05:17,360 --> 00:05:22,520
 move the foot, stop being, stop being, stop being.

111
00:05:22,520 --> 00:05:25,860
 Once we're standing still, we say to ourselves standing,

112
00:05:25,860 --> 00:05:28,160
 standing, standing, becoming aware

113
00:05:28,160 --> 00:05:31,880
 of the standing position and then we turn around.

114
00:05:31,880 --> 00:05:35,670
 The method of turning around, just so you can see, is with

115
00:05:35,670 --> 00:05:37,320
 the feet close together,

116
00:05:37,320 --> 00:05:40,080
 turn with the right foot first, lifting it off the floor,

117
00:05:40,080 --> 00:05:41,680
 turn it 90 degrees and say

118
00:05:41,680 --> 00:05:47,370
 to yourself one time, turning and then the left foot,

119
00:05:47,370 --> 00:05:50,560
 turning and then the right foot

120
00:05:50,560 --> 00:05:55,350
 again, turning and then turning and then you're facing the

121
00:05:55,350 --> 00:05:57,280
 opposite direction.

122
00:05:57,280 --> 00:06:01,430
 Say to yourself standing, standing, standing and start

123
00:06:01,430 --> 00:06:04,800
 walking back in the opposite direction,

124
00:06:04,800 --> 00:06:09,650
 step being right, step being left and so on and you'll walk

125
00:06:09,650 --> 00:06:12,040
 back and forth, back and forth

126
00:06:12,040 --> 00:06:13,160
 in this manner.

127
00:06:13,160 --> 00:06:17,390
 When you come to the other end, when you come to the end,

128
00:06:17,390 --> 00:06:19,800
 bring the back foot up, say stop

129
00:06:19,800 --> 00:06:29,710
 being, stop being, stop being, standing, standing, standing

130
00:06:29,710 --> 00:06:36,440
, turning, turning, turning, turning,

131
00:06:36,440 --> 00:06:41,090
 standing, standing, standing, start walking again, step

132
00:06:41,090 --> 00:06:43,720
 being right, step being left.

133
00:06:43,720 --> 00:06:46,360
 When something comes that you have to be mindful of, if it

134
00:06:46,360 --> 00:06:48,440
's a thought or a feeling that is

135
00:06:48,440 --> 00:06:52,080
 interrupting your concentration, so stop in the middle of

136
00:06:52,080 --> 00:06:54,000
 your step, stop being, stop

137
00:06:54,000 --> 00:06:56,840
 being, stop being and acknowledge first.

138
00:06:56,840 --> 00:07:01,250
 If you feel distracted, so distracted, distracted, if you

139
00:07:01,250 --> 00:07:04,560
 feel upset, upset, upset, bored, bored,

140
00:07:04,560 --> 00:07:07,360
 if you have aching, so aching, aching or if you're thinking

141
00:07:07,360 --> 00:07:08,840
, so thinking, thinking and

142
00:07:08,840 --> 00:07:09,840
 so on.

143
00:07:09,840 --> 00:07:12,240
 And we do it in this manner for 10 minutes.

144
00:07:12,240 --> 00:07:15,070
 Once the walking meditation is finished, then we do sitting

145
00:07:15,070 --> 00:07:17,500
 meditation for another 10 minutes.

146
00:07:17,500 --> 00:07:20,170
 As you get comfortable, you can increase the time walking

147
00:07:20,170 --> 00:07:21,880
 15 minutes and then sitting for

148
00:07:21,880 --> 00:07:23,320
 15 minutes.

149
00:07:23,320 --> 00:07:26,110
 And yes, it's important that we do the walking meditation

150
00:07:26,110 --> 00:07:27,620
 first as it has benefits which

151
00:07:27,620 --> 00:07:30,160
 carry on into the sitting meditation.

152
00:07:30,160 --> 00:07:30,960
 So that's all for now.

