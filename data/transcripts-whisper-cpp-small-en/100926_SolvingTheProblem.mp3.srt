1
00:00:00,000 --> 00:00:06,590
 And we're live here in the Buddha Center, the Deer Park, on

2
00:00:06,590 --> 00:00:09,720
 a wonderful Sunday afternoon,

3
00:00:09,720 --> 00:00:15,510
 evening, morning, midnight, depending on your location and

4
00:00:15,510 --> 00:00:17,560
 the settings you've chosen for

5
00:00:17,560 --> 00:00:20,640
 Second Life.

6
00:00:20,640 --> 00:00:24,440
 So everyone, let's get in the mood, no?

7
00:00:24,440 --> 00:00:32,360
 And put your meditator face on.

8
00:00:32,360 --> 00:00:36,120
 Sit down, make sure you're sitting in real life, turn off

9
00:00:36,120 --> 00:00:43,520
 the music, shut down Facebook,

10
00:00:43,520 --> 00:00:47,200
 cross your legs, straighten your back, close your eyes.

11
00:00:47,200 --> 00:01:04,520
 You can open your eyes if you want, but let's get med

12
00:01:04,520 --> 00:01:08,640
itative.

13
00:01:08,640 --> 00:01:18,930
 The teachings of the Lord Buddha are meant to address a

14
00:01:18,930 --> 00:01:21,280
 problem.

15
00:01:21,280 --> 00:01:26,230
 That's really, I think, how we should look at the teachings

16
00:01:26,230 --> 00:01:27,680
 of the Buddha.

17
00:01:27,680 --> 00:01:37,400
 It proposes that there's a problem and offers a solution.

18
00:01:37,400 --> 00:01:43,390
 I suppose you could generalize and say that's the nature of

19
00:01:43,390 --> 00:01:44,800
 religion.

20
00:01:44,800 --> 00:01:52,470
 And you might even be so, if you wanted to be negative

21
00:01:52,470 --> 00:01:56,840
 about it, you could say it's the

22
00:01:56,840 --> 00:01:58,280
 religion game.

23
00:01:58,280 --> 00:02:05,520
 It's how people, how religions attract followers.

24
00:02:05,520 --> 00:02:15,880
 Those by pointing out a problem, always pointing out a

25
00:02:15,880 --> 00:02:20,480
 problem that people weren't aware of.

26
00:02:20,480 --> 00:02:24,100
 But it seems often that actually creating a problem where

27
00:02:24,100 --> 00:02:26,000
 there was none, proposing a

28
00:02:26,000 --> 00:02:33,440
 problem where it isn't obvious that there is one.

29
00:02:33,440 --> 00:02:40,100
 In order to make people afraid or concerned, and then

30
00:02:40,100 --> 00:02:43,560
 proposing to have the answer on the

31
00:02:43,560 --> 00:02:50,720
 way out.

32
00:02:50,720 --> 00:02:54,080
 And I think it's true that yes, there's a great potential

33
00:02:54,080 --> 00:02:55,200
 for abuse here.

34
00:02:55,200 --> 00:03:01,760
 And certainly there is a lot of abuse that goes on.

35
00:03:01,760 --> 00:03:15,020
 Many religious systems, cults, and so on, exploit people's

36
00:03:15,020 --> 00:03:20,160
 fears and their attachments

37
00:03:20,160 --> 00:03:31,800
 in order to become rich and powerful and famous and so on.

38
00:03:31,800 --> 00:03:46,960
 But it's essentially the role of a physician.

39
00:03:46,960 --> 00:03:58,600
 It's the way of a doctor to be able to point out a problem

40
00:03:58,600 --> 00:04:03,920
 and offer a solution.

41
00:04:03,920 --> 00:04:05,790
 And sometimes the patient isn't even aware there's a

42
00:04:05,790 --> 00:04:06,280
 problem.

43
00:04:06,280 --> 00:04:21,100
 They go to the doctor for a checkup and leave the checkup

44
00:04:21,100 --> 00:04:27,000
 with some bad news.

45
00:04:27,000 --> 00:04:40,340
 Other times they come to the doctor with a problem, not

46
00:04:40,340 --> 00:04:47,960
 knowing what the cause is, and

47
00:04:47,960 --> 00:04:51,030
 the doctor is able to explain the cause and offer a

48
00:04:51,030 --> 00:04:51,920
 solution.

49
00:04:51,920 --> 00:05:00,520
 So both of these examples work in comparison to the

50
00:05:00,520 --> 00:05:04,180
 Buddhist teaching.

51
00:05:04,180 --> 00:05:08,210
 Some people live their lives with no thought that anything

52
00:05:08,210 --> 00:05:09,120
 is wrong.

53
00:05:09,120 --> 00:05:19,360
 And they might go to see the Buddha or go to see a Buddhist

54
00:05:19,360 --> 00:05:19,600
 teacher.

55
00:05:19,600 --> 00:05:27,160
 Just for discussion or to learn something or so on.

56
00:05:27,160 --> 00:05:33,310
 And find out through their dialogue and through practicing

57
00:05:33,310 --> 00:05:36,840
 meditation and so on that actually

58
00:05:36,840 --> 00:05:38,990
 there was a big problem that they weren't aware of, that

59
00:05:38,990 --> 00:05:40,120
 they were setting themselves

60
00:05:40,120 --> 00:05:49,120
 up for a great amount of suffering in the future.

61
00:05:49,120 --> 00:05:53,580
 But I think for most of us, the other form is the one that

62
00:05:53,580 --> 00:05:55,800
 we're best able to relate

63
00:05:55,800 --> 00:05:56,800
 to.

64
00:05:56,800 --> 00:06:00,200
 It's the idea that we have suffering, we have a problem, we

65
00:06:00,200 --> 00:06:01,880
 know there's a problem, we know

66
00:06:01,880 --> 00:06:07,920
 that life's not perfect.

67
00:06:07,920 --> 00:06:10,800
 We're able to see suffering in our lives.

68
00:06:10,800 --> 00:06:19,710
 I don't think many people would deny that it exists in our

69
00:06:19,710 --> 00:06:20,720
 lives.

70
00:06:20,720 --> 00:06:26,230
 So I think this is important because it then dictates the

71
00:06:26,230 --> 00:06:28,920
 way we approach the Buddhist

72
00:06:28,920 --> 00:06:36,690
 teaching and it will shape our practice, our way of

73
00:06:36,690 --> 00:06:42,600
 relating and engaging in the practice

74
00:06:42,600 --> 00:06:50,980
 of the Buddhist teaching.

75
00:06:50,980 --> 00:07:02,600
 In the way that we ask ourselves what's the problem and we

76
00:07:02,600 --> 00:07:05,560
 remind ourselves that what

77
00:07:05,560 --> 00:07:16,050
 we're doing here is trying to discover the problem and find

78
00:07:16,050 --> 00:07:21,480
 a way to remove the problem.

79
00:07:21,480 --> 00:07:24,140
 Why I think we have to keep this in mind is because it's

80
00:07:24,140 --> 00:07:25,560
 easy to lose sight of it in the

81
00:07:25,560 --> 00:07:31,080
 practice of Buddhism.

82
00:07:31,080 --> 00:07:35,090
 The Buddha gave several examples of this that some people

83
00:07:35,090 --> 00:07:37,240
 come to the Buddhist teaching and

84
00:07:37,240 --> 00:07:43,910
 simply study it, engage in intensive study, memorization,

85
00:07:43,910 --> 00:07:47,120
 intellectual discussion and

86
00:07:47,120 --> 00:07:51,480
 debate.

87
00:07:51,480 --> 00:07:58,000
 Many people nowadays engage in reading, studying.

88
00:07:58,000 --> 00:08:08,400
 This is for many people the way of practicing a religion is

89
00:08:08,400 --> 00:08:11,080
 to study it.

90
00:08:11,080 --> 00:08:15,490
 For many people the practice is not so important as the

91
00:08:15,490 --> 00:08:19,040
 study and the intellectual appreciation

92
00:08:19,040 --> 00:08:25,440
 of the teachings.

93
00:08:25,440 --> 00:08:36,100
 For other people it can be the memorization of the

94
00:08:36,100 --> 00:08:37,280
 teachings.

95
00:08:37,280 --> 00:08:41,610
 There are in Buddhist circles there are people who are

96
00:08:41,610 --> 00:08:44,760
 actually able to remember vast tracts

97
00:08:44,760 --> 00:08:53,120
 of discourses and teachings of the Buddha.

98
00:08:53,120 --> 00:08:58,560
 Right now we have a project over email.

99
00:08:58,560 --> 00:09:05,280
 It's a group project to memorize part of the Buddha's

100
00:09:05,280 --> 00:09:08,760
 teaching starting from the first

101
00:09:08,760 --> 00:09:18,840
 sutta in the Pali suttantapitika.

102
00:09:18,840 --> 00:09:21,260
 People do this as a religious practice.

103
00:09:21,260 --> 00:09:22,920
 This is common in other religions as well.

104
00:09:22,920 --> 00:09:35,610
 I know in Judaism it's common or you can find people who

105
00:09:35,610 --> 00:09:38,280
 are actually able to apparently

106
00:09:38,280 --> 00:09:48,830
 take a Jewish Bible, the Torah, stick a needle through it

107
00:09:48,830 --> 00:09:53,960
 or theoretically pretend that if

108
00:09:53,960 --> 00:09:59,290
 they stuck a needle through the Torah they would be able to

109
00:09:59,290 --> 00:10:01,720
 tell you which word based

110
00:10:01,720 --> 00:10:03,800
 on the first word that it punctured.

111
00:10:03,800 --> 00:10:13,110
 They would be able to tell you the word on each page from

112
00:10:13,110 --> 00:10:16,680
 beginning to end.

113
00:10:16,680 --> 00:10:20,160
 I think this has been lost in the modern era.

114
00:10:20,160 --> 00:10:26,290
 There isn't so much of this reliance on the memorization of

115
00:10:26,290 --> 00:10:27,360
 texts.

116
00:10:27,360 --> 00:10:30,400
 But it certainly has been and continues to be in religious

117
00:10:30,400 --> 00:10:32,200
 circles an important religious

118
00:10:32,200 --> 00:10:37,430
 practice as though memorizing and chanting, being able to

119
00:10:37,430 --> 00:10:40,040
 chant, being able to recite

120
00:10:40,040 --> 00:10:44,920
 the teachings of the Buddha, being able to parrot it back.

121
00:10:44,920 --> 00:10:48,240
 I suppose in modern days you find this in what we call

122
00:10:48,240 --> 00:10:50,440
 quote dropping where people are

123
00:10:50,440 --> 00:10:55,400
 able to quote famous teachers.

124
00:10:55,400 --> 00:11:00,560
 I find this actually quite common in many schools of

125
00:11:00,560 --> 00:11:04,160
 Buddhism that you'll find people rather

126
00:11:04,160 --> 00:11:07,480
 than even quoting the Buddha, they're constantly quoting

127
00:11:07,480 --> 00:11:08,600
 their teachers.

128
00:11:08,600 --> 00:11:13,240
 "Well, my teacher says this and my teacher says that."

129
00:11:13,240 --> 00:11:22,090
 Just though somehow it made it true or it had some bearing

130
00:11:22,090 --> 00:11:26,520
 on the person that they were

131
00:11:26,520 --> 00:11:27,760
 talking to.

132
00:11:27,760 --> 00:11:32,010
 Without perhaps the ability to explain it or even just this

133
00:11:32,010 --> 00:11:34,320
 reliance on that as a religious

134
00:11:34,320 --> 00:11:41,220
 practice to be able to quote scripture and to recite and to

135
00:11:41,220 --> 00:11:44,280
 chant and to memorize.

136
00:11:44,280 --> 00:11:53,360
 Then the Buddha said there are other people who teach and

137
00:11:53,360 --> 00:11:58,480
 even become great teachers without

138
00:11:58,480 --> 00:12:02,280
 or at the same time neglecting their own practice.

139
00:12:02,280 --> 00:12:05,960
 People who take all of their time to help other people or

140
00:12:05,960 --> 00:12:07,680
 to teach to become famous

141
00:12:07,680 --> 00:12:08,680
 teachers.

142
00:12:08,680 --> 00:12:11,970
 There was a story in the Buddha's time or maybe it was

143
00:12:11,970 --> 00:12:14,040
 after the Buddha's time.

144
00:12:14,040 --> 00:12:18,330
 There was a story in ancient time about this monk who fell

145
00:12:18,330 --> 00:12:19,880
 into this trap.

146
00:12:19,880 --> 00:12:23,500
 I think I've told this story before.

147
00:12:23,500 --> 00:12:29,420
 He was a great teacher and all of his students became

148
00:12:29,420 --> 00:12:33,680
 enlightened to some degree or other.

149
00:12:33,680 --> 00:12:40,440
 Some of them became sotapanas, some of them became zakitak

150
00:12:40,440 --> 00:12:44,400
ami, some of them became anagami.

151
00:12:44,400 --> 00:12:48,550
 He himself had neglected his own practice and was actually

152
00:12:48,550 --> 00:12:50,680
 just a guy who happened to

153
00:12:50,680 --> 00:12:52,920
 be able to teach quite well.

154
00:12:52,920 --> 00:12:56,210
 He was able to explain the Buddha's teaching in such a way

155
00:12:56,210 --> 00:12:58,240
 that people were able to practice

156
00:12:58,240 --> 00:12:59,240
 it.

157
00:12:59,240 --> 00:13:02,880
 He had an intellectual appreciation of it but he himself

158
00:13:02,880 --> 00:13:04,840
 had never practiced or never

159
00:13:04,840 --> 00:13:15,200
 exerted himself in intensive meditation practice.

160
00:13:15,200 --> 00:13:18,540
 All of his students were reaching these higher states of

161
00:13:18,540 --> 00:13:20,600
 enlightenment and he himself was

162
00:13:20,600 --> 00:13:25,800
 running around teaching and helping other people.

163
00:13:25,800 --> 00:13:28,480
 One day one of his students asked himself, one of his

164
00:13:28,480 --> 00:13:30,320
 students who was an anagami which

165
00:13:30,320 --> 00:13:33,250
 means someone who has attained the third stage of

166
00:13:33,250 --> 00:13:35,600
 enlightenment and is destined when they

167
00:13:35,600 --> 00:13:39,820
 die to never come back to the world again but instead be

168
00:13:39,820 --> 00:13:41,960
 reborn in the god realms or

169
00:13:41,960 --> 00:13:46,370
 the brahma realms and from there become fully enlightened

170
00:13:46,370 --> 00:13:48,760
 and fully released and enter into

171
00:13:48,760 --> 00:13:52,720
 the pure abodes they're called.

172
00:13:52,720 --> 00:13:58,480
 He asked himself, "Here I am, I've realized this stage of

173
00:13:58,480 --> 00:14:01,660
 enlightenment and all of these

174
00:14:01,660 --> 00:14:02,660
 other people have.

175
00:14:02,660 --> 00:14:05,750
 I wonder what stage of enlightenment our teacher has

176
00:14:05,750 --> 00:14:06,680
 realized?"

177
00:14:06,680 --> 00:14:12,120
 Through his incredible strength of mind he was able to

178
00:14:12,120 --> 00:14:15,980
 discern for himself just by thinking

179
00:14:15,980 --> 00:14:20,320
 about it, just by examining the situation and by sending

180
00:14:20,320 --> 00:14:22,640
 his mind in this direction.

181
00:14:22,640 --> 00:14:28,130
 He was able to ascertain that his teacher had indeed not

182
00:14:28,130 --> 00:14:31,920
 attained any level of enlightenment.

183
00:14:31,920 --> 00:14:35,000
 He thought, "Oh well this won't do.

184
00:14:35,000 --> 00:14:37,360
 This is not a proper state of affairs."

185
00:14:37,360 --> 00:14:41,910
 He thought to himself out of gratitude for his teacher that

186
00:14:41,910 --> 00:14:43,540
 he owed it to him to help

187
00:14:43,540 --> 00:14:47,400
 him become enlightened as well.

188
00:14:47,400 --> 00:14:50,880
 He went up to his teacher and right when his teacher was

189
00:14:50,880 --> 00:14:53,280
 very, very busy, waited for when

190
00:14:53,280 --> 00:15:00,420
 his teacher was busy helping people, meeting with students

191
00:15:00,420 --> 00:15:01,840
 and so on.

192
00:15:01,840 --> 00:15:06,840
 He came up to him and he said, "Teacher, I think I would

193
00:15:06,840 --> 00:15:09,200
 like to request that it's time

194
00:15:09,200 --> 00:15:12,000
 for a lesson."

195
00:15:12,000 --> 00:15:15,000
 The teacher said, "I'm sorry, how can I possibly have time

196
00:15:15,000 --> 00:15:16,320
 for a lesson right now?

197
00:15:16,320 --> 00:15:19,680
 I have many duties and people coming to see me and so on.

198
00:15:19,680 --> 00:15:23,080
 I'm far too busy for that."

199
00:15:23,080 --> 00:15:30,960
 The student of his sat down, crossed his legs and went into

200
00:15:30,960 --> 00:15:35,080
 an absorbed state and started

201
00:15:35,080 --> 00:15:37,680
 floating off the ground.

202
00:15:37,680 --> 00:15:40,560
 He opened his eyes and said to the teacher, "You don't have

203
00:15:40,560 --> 00:15:41,920
 time even for yourself.

204
00:15:41,920 --> 00:15:47,320
 How could you possibly have time to teach anything to me?"

205
00:15:47,320 --> 00:15:53,600
 He floated out of the room, showing to the teacher that

206
00:15:53,600 --> 00:15:57,000
 indeed his students were on a

207
00:15:57,000 --> 00:16:00,480
 higher level than he was.

208
00:16:00,480 --> 00:16:07,360
 This of course shook the teacher up visibly.

209
00:16:07,360 --> 00:16:11,320
 He decided that he had enough.

210
00:16:11,320 --> 00:16:14,790
 This teaching business was obviously not the proper way to

211
00:16:14,790 --> 00:16:16,920
 approach the Buddha's teaching.

212
00:16:16,920 --> 00:16:21,720
 As the Buddha said, "Set yourself in what is right.

213
00:16:21,720 --> 00:16:26,280
 Set yourself in what is right first, only then should you

214
00:16:26,280 --> 00:16:27,760
 help others."

215
00:16:27,760 --> 00:16:31,600
 He decided to go off into the forest.

216
00:16:31,600 --> 00:16:34,160
 He was such a famous teacher that it was very difficult.

217
00:16:34,160 --> 00:16:37,090
 He had to find a forest where there were no other people

218
00:16:37,090 --> 00:16:39,040
 around where he wouldn't be bothered

219
00:16:39,040 --> 00:16:42,200
 by people coming to ask him about meditation practice.

220
00:16:42,200 --> 00:16:47,600
 Finally, he found this place, no humans in the area.

221
00:16:47,600 --> 00:16:49,960
 He started his meditation.

222
00:16:49,960 --> 00:17:00,160
 He worked very hard walking back and forth, sitting down.

223
00:17:00,160 --> 00:17:03,490
 Still did he know that you can't really go anywhere where

224
00:17:03,490 --> 00:17:05,160
 you're going to be alone,

225
00:17:05,160 --> 00:17:06,800
 whether there are humans or not.

226
00:17:06,800 --> 00:17:09,690
 There are always beings around, whether they be animals or

227
00:17:09,690 --> 00:17:10,880
 in this case angels.

228
00:17:10,880 --> 00:17:15,760
 There were angels in the forest watching him.

229
00:17:15,760 --> 00:17:19,300
 That actually came down and started meditating with him.

230
00:17:19,300 --> 00:17:20,400
 When he walked, they walked.

231
00:17:20,400 --> 00:17:22,960
 When he sit, they sit.

232
00:17:22,960 --> 00:17:28,960
 When he sat, they sat.

233
00:17:28,960 --> 00:17:31,080
 He practiced and practiced and gained nothing from it.

234
00:17:31,080 --> 00:17:32,640
 He was pushing very, very hard.

235
00:17:32,640 --> 00:17:35,150
 Someone said he was probably practicing too hard, pushing

236
00:17:35,150 --> 00:17:36,520
 himself too hard, wanting too

237
00:17:36,520 --> 00:17:38,400
 much to become enlightened.

238
00:17:38,400 --> 00:17:41,760
 Of course, the wanting, the needing, the desire was

239
00:17:41,760 --> 00:17:42,880
 blocking him.

240
00:17:42,880 --> 00:17:48,400
 It was stopping him from attaining the very thing he was

241
00:17:48,400 --> 00:17:50,000
 looking for.

242
00:17:50,000 --> 00:17:53,490
 He got more and more frustrated and more and more upset

243
00:17:53,490 --> 00:17:55,640
 until finally he broke down.

244
00:17:55,640 --> 00:17:59,120
 He was under such pressure that, "I'm this great teacher.

245
00:17:59,120 --> 00:18:03,660
 If I don't become enlightened, what will I say to my

246
00:18:03,660 --> 00:18:05,000
 students?"

247
00:18:05,000 --> 00:18:12,000
 Until he finally snapped and he sat down and he started

248
00:18:12,000 --> 00:18:13,480
 crying.

249
00:18:13,480 --> 00:18:16,120
 He was crying and suddenly he heard someone else crying.

250
00:18:16,120 --> 00:18:19,130
 He opened his eyes and there's this angel sitting down

251
00:18:19,130 --> 00:18:20,960
 beside him crying, crying and

252
00:18:20,960 --> 00:18:23,480
 crying and crying.

253
00:18:23,480 --> 00:18:26,920
 He stops crying and he says, "What are you doing?"

254
00:18:26,920 --> 00:18:29,800
 The angel stops crying and said, "Oh, I was crying."

255
00:18:29,800 --> 00:18:30,800
 He said, "Why?"

256
00:18:30,800 --> 00:18:34,240
 He said, "Well, you're the great teacher.

257
00:18:34,240 --> 00:18:35,920
 Whatever you do, that's got to be the way to become

258
00:18:35,920 --> 00:18:36,520
 enlightened.

259
00:18:36,520 --> 00:18:38,680
 I've just been following you.

260
00:18:38,680 --> 00:18:39,680
 I walked.

261
00:18:39,680 --> 00:18:40,880
 When you sat, I sat.

262
00:18:40,880 --> 00:18:42,440
 When you started crying, I started crying.

263
00:18:42,440 --> 00:18:50,520
 I figured that's got to be the way to become enlightened."

264
00:18:50,520 --> 00:18:55,020
 This hit him quite hard as well, made him quite ashamed of

265
00:18:55,020 --> 00:18:56,400
 his behavior.

266
00:18:56,400 --> 00:19:00,230
 As a result of that, he was able to wake up, let go and

267
00:19:00,230 --> 00:19:02,800
 practice in such a way as to become

268
00:19:02,800 --> 00:19:03,800
 enlightened.

269
00:19:03,800 --> 00:19:08,880
 From the point of the story being, teaching people is not

270
00:19:08,880 --> 00:19:11,480
 the most important point and

271
00:19:11,480 --> 00:19:14,490
 it's easy to get off track in this way, going out and

272
00:19:14,490 --> 00:19:16,520
 trying to explain to other people

273
00:19:16,520 --> 00:19:19,800
 how great Buddhism is.

274
00:19:19,800 --> 00:19:21,280
 It's something you can remember.

275
00:19:21,280 --> 00:19:24,760
 It's always a sign of immaturity when people are trying to

276
00:19:24,760 --> 00:19:26,640
 teach, when people approach

277
00:19:26,640 --> 00:19:32,210
 you and start teaching you uninvited, unasked, when people

278
00:19:32,210 --> 00:19:35,240
 start giving advice without you

279
00:19:35,240 --> 00:19:43,880
 having asked or even intimated a desire for any.

280
00:19:43,880 --> 00:19:46,620
 This is very common, especially when people begin to

281
00:19:46,620 --> 00:19:47,360
 practice.

282
00:19:47,360 --> 00:19:51,540
 It's called, it's called, it's an uppakiles, it's a defile

283
00:19:51,540 --> 00:19:52,920
ment of insight.

284
00:19:52,920 --> 00:19:55,470
 It's something that arises when you start practicing

285
00:19:55,470 --> 00:19:56,200
 correctly.

286
00:19:56,200 --> 00:19:58,920
 You start to think, "Wow, everyone else should practice

287
00:19:58,920 --> 00:20:00,480
 this way," and you lose sight of

288
00:20:00,480 --> 00:20:13,920
 your own practice and start trying to teach others.

289
00:20:13,920 --> 00:20:18,020
 Another thing that people often do, that the Buddha

290
00:20:18,020 --> 00:20:20,920
 mentioned, that people often do in

291
00:20:20,920 --> 00:20:26,500
 approaching the teachings of the Buddha is to think about

292
00:20:26,500 --> 00:20:28,800
 it, to ponder on it and to

293
00:20:28,800 --> 00:20:35,000
 work it out in their mind.

294
00:20:35,000 --> 00:20:41,350
 People who debate or people who like to think and like to

295
00:20:41,350 --> 00:20:41,800
 create.

296
00:20:41,800 --> 00:20:48,400
 People who write stories or write books.

297
00:20:48,400 --> 00:20:53,440
 People who relate to the Buddha's teaching intellectually.

298
00:20:53,440 --> 00:20:56,420
 These are the sorts of ways that people interact with the

299
00:20:56,420 --> 00:21:01,840
 Buddha's teaching that are not getting

300
00:21:01,840 --> 00:21:08,360
 closer to a solution to the problem.

301
00:21:08,360 --> 00:21:10,540
 This is why it's important for us to always be asking

302
00:21:10,540 --> 00:21:12,120
 ourselves, "What is the problem?"

303
00:21:12,120 --> 00:21:14,190
 The problem is not that we don't know enough, that we haven

304
00:21:14,190 --> 00:21:15,360
't read enough of the Buddha's

305
00:21:15,360 --> 00:21:16,360
 teaching.

306
00:21:16,360 --> 00:21:20,240
 It isn't that we don't know how to explain to other people.

307
00:21:20,240 --> 00:21:22,840
 There are some meditators who are never able to explain to

308
00:21:22,840 --> 00:21:24,560
 others, who find it very difficult

309
00:21:24,560 --> 00:21:29,240
 to put into words the realizations that they have.

310
00:21:29,240 --> 00:21:31,460
 On the other hand, there are people who work very hard to

311
00:21:31,460 --> 00:21:32,680
 be able to explain things to

312
00:21:32,680 --> 00:21:44,230
 others and yet themselves are gaining nothing and are not

313
00:21:44,230 --> 00:21:49,840
 progressing on the path.

314
00:21:49,840 --> 00:21:55,520
 The real problem, I guess you could say, is twofold.

315
00:21:55,520 --> 00:22:07,730
 The problem that we have is that our minds are not tranquil

316
00:22:07,730 --> 00:22:11,880
 enough and our minds are

317
00:22:11,880 --> 00:22:16,160
 lacking in wisdom and understanding.

318
00:22:16,160 --> 00:22:19,750
 The Buddha's approach to the problem and to finding a

319
00:22:19,750 --> 00:22:21,480
 solution is twofold.

320
00:22:21,480 --> 00:22:27,920
 There's one to calm the mind through the practice of

321
00:22:27,920 --> 00:22:31,440
 meditation, to focus on an object, focus

322
00:22:31,440 --> 00:22:46,800
 on reality in a way that stops the mind from flitting

323
00:22:46,800 --> 00:22:49,800
 around superficially.

324
00:22:49,800 --> 00:22:53,930
 That allows us to grasp reality or grasp the object in

325
00:22:53,930 --> 00:22:56,360
 front of us in a meaningful way

326
00:22:56,360 --> 00:23:00,320
 to focus our minds and to calm our minds down.

327
00:23:00,320 --> 00:23:06,730
 You can do this either with a conceptual object or a part

328
00:23:06,730 --> 00:23:08,440
 of reality.

329
00:23:08,440 --> 00:23:10,480
 So a conceptual object would be something that you create

330
00:23:10,480 --> 00:23:11,520
 in your mind, whether it be

331
00:23:11,520 --> 00:23:23,430
 a light or a sound or a concept, a being, a god, an angel,

332
00:23:23,430 --> 00:23:25,760
 an idea.

333
00:23:25,760 --> 00:23:30,880
 Reality would be something that is arising and ceasing and

334
00:23:30,880 --> 00:23:32,880
 is coming and going in the

335
00:23:32,880 --> 00:23:38,410
 in experiential reality already that isn't created by the

336
00:23:38,410 --> 00:23:39,280
 mind.

337
00:23:39,280 --> 00:23:43,020
 Watching the breath, watching the stomach, watching the

338
00:23:43,020 --> 00:23:45,200
 feet, watching pain or thoughts

339
00:23:45,200 --> 00:23:52,440
 or emotions.

340
00:23:52,440 --> 00:23:54,920
 And this serves to calm the mind down.

341
00:23:54,920 --> 00:23:59,310
 The solution here is that our minds no longer give rise to

342
00:23:59,310 --> 00:24:02,720
 things like greed, anger, depression,

343
00:24:02,720 --> 00:24:05,800
 worry, fear and so on.

344
00:24:05,800 --> 00:24:10,880
 Our minds become fixed, focused, happy, calm, peaceful,

345
00:24:10,880 --> 00:24:13,480
 relaxed and for all intents and

346
00:24:13,480 --> 00:24:23,200
 purposes become free from suffering for a time.

347
00:24:23,200 --> 00:24:27,800
 Now the reason why this is not enough of course is it's

348
00:24:27,800 --> 00:24:30,560
 still a contrived state that we've

349
00:24:30,560 --> 00:24:36,010
 created this state through the work that we've done,

350
00:24:36,010 --> 00:24:39,960
 through the pressure of our concentration

351
00:24:39,960 --> 00:24:42,040
 which is able to suppress the defilements.

352
00:24:42,040 --> 00:24:47,310
 It's a good thing because it makes our mind very clear and

353
00:24:47,310 --> 00:24:48,640
 very pure.

354
00:24:48,640 --> 00:24:53,710
 But it's a good thing primarily because it then allows us

355
00:24:53,710 --> 00:24:56,720
 to fix things decisively, completely

356
00:24:56,720 --> 00:25:08,200
 and irrevocably or permanently.

357
00:25:08,200 --> 00:25:12,440
 Because no matter how much focus and concentration you

358
00:25:12,440 --> 00:25:16,320
 apply to reality, that focus and concentration

359
00:25:16,320 --> 00:25:19,320
 alone is not going to change your mind.

360
00:25:19,320 --> 00:25:21,560
 It's not going to change the way you think.

361
00:25:21,560 --> 00:25:25,920
 It's not going to change your habits.

362
00:25:25,920 --> 00:25:34,600
 It's not going to cut off the potential for the arising of

363
00:25:34,600 --> 00:25:37,280
 defilements.

364
00:25:37,280 --> 00:25:41,090
 Wisdom on the other hand is the final solution and the

365
00:25:41,090 --> 00:25:43,280
 final cure and wisdom is something

366
00:25:43,280 --> 00:25:46,480
 that comes about through this clarity of mind.

367
00:25:46,480 --> 00:25:53,350
 Once you have a clear mind, it's imperative that you focus

368
00:25:53,350 --> 00:25:56,880
 entirely solely on ultimate

369
00:25:56,880 --> 00:25:57,880
 reality.

370
00:25:57,880 --> 00:26:00,990
 Whether you've been practicing to calm the mind based on a

371
00:26:00,990 --> 00:26:02,520
 concept or whether ultimate

372
00:26:02,520 --> 00:26:05,720
 reality, when you're practicing to gain wisdom and insight

373
00:26:05,720 --> 00:26:07,280
 you have to focus on ultimate

374
00:26:07,280 --> 00:26:10,360
 reality.

375
00:26:10,360 --> 00:26:18,600
 Because you can't come to understand reality any other way.

376
00:26:18,600 --> 00:26:21,410
 You can't come to understand things as they are when you're

377
00:26:21,410 --> 00:26:22,800
 creating the object of your

378
00:26:22,800 --> 00:26:35,200
 attention.

379
00:26:35,200 --> 00:26:36,680
 In essence then we have a twofold problem.

380
00:26:36,680 --> 00:26:39,460
 We have the problem of the defilements of the negative mind

381
00:26:39,460 --> 00:26:40,680
 states and then we have the

382
00:26:40,680 --> 00:26:44,200
 problem of the reason why they arise.

383
00:26:44,200 --> 00:26:45,760
 To stop them from arising is easy.

384
00:26:45,760 --> 00:26:46,760
 You focus the mind.

385
00:26:46,760 --> 00:26:50,850
 But to remove the cause of their arising you have to go

386
00:26:50,850 --> 00:26:53,200
 deeper and you have to come to

387
00:26:53,200 --> 00:26:56,800
 understand why they arise.

388
00:26:56,800 --> 00:26:59,320
 The reason why defilements arises through a

389
00:26:59,320 --> 00:27:01,760
 misunderstanding of reality for what it

390
00:27:01,760 --> 00:27:05,920
 is, not seeing things as they are, a superficial grasp of

391
00:27:05,920 --> 00:27:08,240
 the experience in front of us at

392
00:27:08,240 --> 00:27:10,920
 every moment.

393
00:27:10,920 --> 00:27:13,760
 As an example, there are many examples.

394
00:27:13,760 --> 00:27:15,640
 One example is pain.

395
00:27:15,640 --> 00:27:19,760
 When you're sitting and you start to feel pain.

396
00:27:19,760 --> 00:27:24,510
 It's a very superficial experience where right away

397
00:27:24,510 --> 00:27:27,960
 convinced that it's a negative experience,

398
00:27:27,960 --> 00:27:30,320
 that it's unpleasant, it's negative.

399
00:27:30,320 --> 00:27:36,710
 And we're right away acting on our habit, our habitual way

400
00:27:36,710 --> 00:27:40,000
 of approaching problems.

401
00:27:40,000 --> 00:27:45,290
 We've already decided and begun to react and switch

402
00:27:45,290 --> 00:27:49,040
 positions or find a solution, a way

403
00:27:49,040 --> 00:27:52,240
 to get rid of the pain.

404
00:27:52,240 --> 00:27:55,440
 When we focus on things like pain on a deeper level, when

405
00:27:55,440 --> 00:27:57,880
 we look at them clearly and objectively,

406
00:27:57,880 --> 00:28:00,240
 we come to see that actually it's not so at all.

407
00:28:00,240 --> 00:28:06,550
 There's nothing intrinsically negative about things like

408
00:28:06,550 --> 00:28:10,400
 pain or people yelling at us or

409
00:28:10,400 --> 00:28:16,280
 smells or tastes or sounds, sights.

410
00:28:16,280 --> 00:28:22,240
 The experience of the sense, even when there are people

411
00:28:22,240 --> 00:28:25,200
 yelling at us or attacking us or

412
00:28:25,200 --> 00:28:30,010
 unpleasant sights or sounds or smells, tastes, feelings,

413
00:28:30,010 --> 00:28:32,880
 thoughts, bad memories, worries

414
00:28:32,880 --> 00:28:38,010
 about the future, problems in our life, not having enough

415
00:28:38,010 --> 00:28:40,640
 money, not having a job, not

416
00:28:40,640 --> 00:28:45,640
 having a future, and so on and so on.

417
00:28:45,640 --> 00:28:51,400
 Hunger, there's hot, cold.

418
00:28:51,400 --> 00:28:55,370
 When we examine these things on a deeper level, which is

419
00:28:55,370 --> 00:28:57,400
 exactly what we're doing through

420
00:28:57,400 --> 00:29:03,430
 meditation, we come to see things in quite a different way,

421
00:29:03,430 --> 00:29:06,720
 that there actually is nothing

422
00:29:06,720 --> 00:29:11,640
 negative about any of these things.

423
00:29:11,640 --> 00:29:14,480
 We don't react.

424
00:29:14,480 --> 00:29:18,780
 We lose this habitual reaction and tendency to compartment

425
00:29:18,780 --> 00:29:20,800
alize reality into the good

426
00:29:20,800 --> 00:29:24,880
 and the bad, into what is unacceptable and what is

427
00:29:24,880 --> 00:29:26,420
 unacceptable.

428
00:29:26,420 --> 00:29:32,200
 Some people like it hot, some like it cold.

429
00:29:32,200 --> 00:29:35,760
 Some people like loud music, some people hate loud music.

430
00:29:35,760 --> 00:29:38,440
 It's actually a habit that we gain in our mind, and these

431
00:29:38,440 --> 00:29:39,760
 can change and develop over

432
00:29:39,760 --> 00:29:41,760
 time, obviously.

433
00:29:41,760 --> 00:29:44,130
 People, when you take your first sip of beer or when you

434
00:29:44,130 --> 00:29:45,680
 take your first cigarette, it's

435
00:29:45,680 --> 00:29:49,180
 terrible, but over time you begin to change your habits and

436
00:29:49,180 --> 00:29:50,960
 decide for yourself that it's

437
00:29:50,960 --> 00:29:53,160
 a positive, a pleasant experience.

438
00:29:53,160 --> 00:29:57,760
 People can go to the extent of believing that or of perce

439
00:29:57,760 --> 00:30:00,640
iving smoke entering their lungs

440
00:30:00,640 --> 00:30:11,480
 as being a pleasant experience.

441
00:30:11,480 --> 00:30:13,400
 And so really, this is all we're doing in meditation.

442
00:30:13,400 --> 00:30:16,280
 We're not trying to change things.

443
00:30:16,280 --> 00:30:19,150
 Once we have this level of calm, we simply start to see

444
00:30:19,150 --> 00:30:20,760
 things for what they are, and

445
00:30:20,760 --> 00:30:25,140
 we start to understand things on a far deeper level, to the

446
00:30:25,140 --> 00:30:27,160
 point that everything that arises

447
00:30:27,160 --> 00:30:29,080
 we're able to see it for what it is.

448
00:30:29,080 --> 00:30:32,790
 We lose this sense of attachment to it, this habitual

449
00:30:32,790 --> 00:30:35,120
 reaction, that it's good or it's

450
00:30:35,120 --> 00:30:44,400
 bad, this identification of it being me or mine.

451
00:30:44,400 --> 00:30:50,190
 And the mind is able to find peace and freedom simply

452
00:30:50,190 --> 00:30:55,000
 through wisdom, through understanding.

453
00:30:55,000 --> 00:30:59,460
 Because there's nobody that, I think we can all agree that

454
00:30:59,460 --> 00:31:01,640
 nobody wants suffering.

455
00:31:01,640 --> 00:31:06,920
 Nobody wants to find peace and happiness.

456
00:31:06,920 --> 00:31:11,860
 And so the reason that we create suffering for us is not

457
00:31:11,860 --> 00:31:14,520
 because we, on a deep down level,

458
00:31:14,520 --> 00:31:17,090
 want to suffer, it's because we don't understand that that

459
00:31:17,090 --> 00:31:18,720
's what we're doing to ourselves.

460
00:31:18,720 --> 00:31:21,780
 And once we can teach ourselves, once we can convince our

461
00:31:21,780 --> 00:31:23,400
 minds that that's what we're

462
00:31:23,400 --> 00:31:28,220
 doing, that this reaction, this mental process is creating

463
00:31:28,220 --> 00:31:30,960
 suffering for us, then we'll stop.

464
00:31:30,960 --> 00:31:31,960
 We stop naturally.

465
00:31:31,960 --> 00:31:34,000
 There's no question about that.

466
00:31:34,000 --> 00:31:37,530
 It's a theory, this is the theory I'm proposing to you, but

467
00:31:37,530 --> 00:31:39,960
 it's a perfectly, absolutely testable

468
00:31:39,960 --> 00:31:42,080
 and verifiable theory.

469
00:31:42,080 --> 00:31:46,830
 If you start to meditate, you'll see that all it takes is a

470
00:31:46,830 --> 00:31:49,120
 deeper, more comprehensive

471
00:31:49,120 --> 00:31:52,200
 understanding of the things that you're already

472
00:31:52,200 --> 00:31:54,920
 experiencing and already reacting to.

473
00:31:54,920 --> 00:31:57,140
 Once you see them for what they are, and you see the

474
00:31:57,140 --> 00:31:58,680
 process, and you see the way your

475
00:31:58,680 --> 00:32:05,960
 mind is reacting, slowly but surely you'll change that.

476
00:32:05,960 --> 00:32:10,190
 You'll convince yourself over time and with effort that

477
00:32:10,190 --> 00:32:13,160
 what you're doing is hurting yourself

478
00:32:13,160 --> 00:32:14,160
 and you'll change.

479
00:32:14,160 --> 00:32:15,200
 It's a natural process.

480
00:32:15,200 --> 00:32:17,480
 You don't have to want to change.

481
00:32:17,480 --> 00:32:21,330
 You don't have to wish for yourself to become enlightened

482
00:32:21,330 --> 00:32:23,240
 or even strive in any way.

483
00:32:23,240 --> 00:32:26,520
 The striving is just to see things for what they are.

484
00:32:26,520 --> 00:32:31,900
 And the closer you come to seeing things as they are, the

485
00:32:31,900 --> 00:32:34,520
 more free and at ease and at

486
00:32:34,520 --> 00:32:41,280
 peace with yourself you become.

487
00:32:41,280 --> 00:32:52,480
 There you have the problem and a fairly clear solution.

488
00:32:52,480 --> 00:32:54,840
 That's enough for today.

489
00:32:54,840 --> 00:32:57,470
 Give everyone some time to ask questions if you have any

490
00:32:57,470 --> 00:32:59,000
 and otherwise we can just sit

491
00:32:59,000 --> 00:33:03,040
 here and meditate together and you're all welcome to go on

492
00:33:03,040 --> 00:33:04,480
 with your lives.

493
00:33:04,480 --> 00:33:06,040
 I'd like to thank you all for coming.

494
00:33:06,040 --> 00:33:11,860
 We appreciate to have such an interesting group of people

495
00:33:11,860 --> 00:33:14,600
 who return again and again

496
00:33:14,600 --> 00:33:18,380
 to hear these talks and to support the Buddha Center in its

497
00:33:18,380 --> 00:33:19,320
 projects.

498
00:33:19,320 --> 00:33:22,830
 I'd like to wish for you all to be able to put into

499
00:33:22,830 --> 00:33:25,560
 practice these teachings and to be

500
00:33:25,560 --> 00:33:29,230
 able to practice meditation for the development of your own

501
00:33:29,230 --> 00:33:31,160
 selves and for the attainment

502
00:33:31,160 --> 00:33:34,840
 of real and true peace, happiness and freedom from

503
00:33:34,840 --> 00:33:36,680
 suffering for you all.

504
00:33:36,680 --> 00:33:37,680
 Thank you all for coming.

505
00:33:37,680 --> 00:33:37,680
 Have a great day.

506
00:33:37,680 --> 00:33:38,680
 [END]

