1
00:00:00,000 --> 00:00:05,520
 When I try to remind myself to be mindful when listening to

2
00:00:05,520 --> 00:00:08,440
 someone, I actually lose track of what the other person is

3
00:00:08,440 --> 00:00:09,000
 saying,

4
00:00:09,000 --> 00:00:12,990
 because then I'm not focused on the whole, but just the

5
00:00:12,990 --> 00:00:15,000
 parts of what is being said.

6
00:00:15,000 --> 00:00:19,370
 How can I be mindful in conversation and also be able to

7
00:00:19,370 --> 00:00:23,000
 process what the other person is saying?

8
00:00:28,000 --> 00:00:37,170
 I've practiced that myself, where I've been in conversation

9
00:00:37,170 --> 00:00:46,000
 situations and somebody's waffling on and I'm just noting,

10
00:00:46,000 --> 00:00:51,250
 listening, listening, or hearing, hearing, whichever one

11
00:00:51,250 --> 00:00:53,000
 you want to use to note.

12
00:00:53,000 --> 00:00:58,360
 It depends on the conversation. If you're in direct

13
00:00:58,360 --> 00:01:00,960
 communication with someone, I think it's a little bit

14
00:01:00,960 --> 00:01:02,000
 harder to do.

15
00:01:02,000 --> 00:01:05,820
 But if you're in a group conversation and there are

16
00:01:05,820 --> 00:01:09,440
 different people talking back and forth and you're not

17
00:01:09,440 --> 00:01:10,000
 really involved,

18
00:01:10,000 --> 00:01:18,090
 it's more possible. So through personal experience, it's

19
00:01:18,090 --> 00:01:26,230
 quite a task to be communicating one-to-one and noting that

20
00:01:26,230 --> 00:01:27,000
 you're hearing.

21
00:01:27,000 --> 00:01:33,030
 You could most probably regulate the noting, so you're not

22
00:01:33,030 --> 00:01:36,320
 saying in your mind, hearing, hearing, hearing, over and

23
00:01:36,320 --> 00:01:37,000
 over and over again.

24
00:01:37,000 --> 00:01:40,740
 Just know that you're hearing and then listen to the words

25
00:01:40,740 --> 00:01:42,000
 as the person talks.

26
00:01:42,000 --> 00:01:46,000
 And then if you're answering a question, then answer the

27
00:01:46,000 --> 00:01:51,000
 question. Yeah, it's tough.

28
00:01:55,000 --> 00:02:00,310
 I don't personally think or find that the noting itself

29
00:02:00,310 --> 00:02:04,750
 gets in the way of the understanding, but there's a lot

30
00:02:04,750 --> 00:02:10,000
 more that goes on when you attempt to note.

31
00:02:10,000 --> 00:02:17,650
 You can find yourself suppressing or forcing the mind into

32
00:02:17,650 --> 00:02:25,500
 certain states. You find it difficult because your mind is

33
00:02:25,500 --> 00:02:29,000
 accustomed to reacting.

34
00:02:29,000 --> 00:02:33,840
 So you find yourself reacting and then trying to repress

35
00:02:33,840 --> 00:02:38,000
 the reactions or reacting to the reactions.

36
00:02:38,000 --> 00:02:41,550
 And so because you're breaking it up and because it's

37
00:02:41,550 --> 00:02:47,690
 actually a real task, because it's not something that is

38
00:02:47,690 --> 00:02:52,000
 comfortable for someone who's not proficient in it,

39
00:02:52,000 --> 00:02:55,980
 it's easy to get off track. So suddenly you'll start to

40
00:02:55,980 --> 00:02:58,770
 think about something else and lose track of what's being

41
00:02:58,770 --> 00:02:59,000
 said.

42
00:02:59,000 --> 00:03:02,360
 You'll start to think about the meditation. "Oh, am I doing

43
00:03:02,360 --> 00:03:04,000
 it correctly?"

44
00:03:04,000 --> 00:03:08,290
 "Am I understanding what they're saying?" "Oh, I'm not

45
00:03:08,290 --> 00:03:09,720
 understanding what they're saying," and suddenly you lose

46
00:03:09,720 --> 00:03:10,000
 it.

47
00:03:10,000 --> 00:03:17,580
 But the noting itself still leaves room for processing. It

48
00:03:17,580 --> 00:03:26,300
 doesn't stop the mind from, or the brain or the mind from

49
00:03:26,300 --> 00:03:29,000
 processing the meaning of the words.

50
00:03:29,000 --> 00:03:44,870
 Not necessarily. So if your intention is to understand what

51
00:03:44,870 --> 00:03:49,000
 is being said, you can still couple that with mindfulness.

52
00:03:49,000 --> 00:03:52,930
 So you're mindful of it and then you process what is being

53
00:03:52,930 --> 00:03:56,760
 said and then you're mindful back about what they're saying

54
00:03:56,760 --> 00:03:59,000
 next and process it and so on.

55
00:03:59,000 --> 00:04:07,120
 But yeah, it's a question of what your intention is. If

56
00:04:07,120 --> 00:04:09,760
 your intention is just to meditate, then as the one said,

57
00:04:09,760 --> 00:04:13,560
 just let them rattle on and say, "Hearing, hearing, hearing

58
00:04:13,560 --> 00:04:14,000
."

59
00:04:14,000 --> 00:04:17,770
 Many times you don't really need to know what they've said

60
00:04:17,770 --> 00:04:20,000
 unless it's an important conversation.

61
00:04:20,000 --> 00:04:23,800
 Then it's a question of priorities. Well, your priority is

62
00:04:23,800 --> 00:04:27,000
 to process what they're saying, so don't give that up.

63
00:04:27,000 --> 00:04:32,870
 Don't dedicate yourself wholly to being mindful and noting

64
00:04:32,870 --> 00:04:39,000
 the mental event of processing and cancel that as well.

65
00:04:39,000 --> 00:04:44,300
 Don't get yourself too focused on the actual sound so that

66
00:04:44,300 --> 00:04:47,000
 you lose the mental aspect.

67
00:04:47,000 --> 00:04:51,730
 The key of meditation is to purify the mind, purify the

68
00:04:51,730 --> 00:04:56,200
 processing of the mind, not to stop it, not even to stop

69
00:04:56,200 --> 00:05:00,000
 the mind from thinking, but to purify that process.

70
00:05:00,000 --> 00:05:00,590
 So the thinking is only thinking, "Itte di tamata ngo risa

71
00:05:00,590 --> 00:05:10,540
 tae." So that there's no reaction to it. You understand it

72
00:05:10,540 --> 00:05:12,000
 for what it is.

73
00:05:12,000 --> 00:05:14,440
 So it's possible to get into a state where you just know it

74
00:05:14,440 --> 00:05:20,000
 as sound, but it's also possible to know it just as

75
00:05:20,000 --> 00:05:24,200
 concepts, thoughts in the mind, "This means this, this

76
00:05:24,200 --> 00:05:25,000
 means that."

77
00:05:25,000 --> 00:05:34,000
 Even our hands do that, and they're always mindful.

78
00:05:34,000 --> 00:05:39,870
 But yeah, if you focus too much on it, especially when it's

79
00:05:39,870 --> 00:05:45,000
 not a familiar practice, it can be problematic.

80
00:05:45,000 --> 00:05:49,940
 So it doesn't mean to stop. I would say do it back and

81
00:05:49,940 --> 00:05:54,500
 forth and try to, yes, of course, be mindful. You should

82
00:05:54,500 --> 00:05:57,580
 certainly not stop being mindful in conversation, but you

83
00:05:57,580 --> 00:06:00,000
 might want to alternate it.

84
00:06:00,000 --> 00:06:04,110
 So not always noting, let yourself think, let yourself

85
00:06:04,110 --> 00:06:08,970
 process. Even let yourself react, because you can't stop

86
00:06:08,970 --> 00:06:12,270
 the reactions, and when the reactions come, just watch the

87
00:06:12,270 --> 00:06:14,000
 reactions as well.

88
00:06:14,000 --> 00:06:18,140
 And of course, be mindful of them if you're angry, to angry

89
00:06:18,140 --> 00:06:23,770
, to liking, liking, and so on. And then go back to

90
00:06:23,770 --> 00:06:26,000
 listening.

91
00:06:26,000 --> 00:06:29,540
 But I think the real problem is not the mindfulness, it's

92
00:06:29,540 --> 00:06:32,000
 the over-focusing and repressing.

93
00:06:32,000 --> 00:06:35,980
 I was in that state after I finished my first course, which

94
00:06:35,980 --> 00:06:39,490
 didn't really go very well. One of the teachers was talking

95
00:06:39,490 --> 00:06:42,590
 to me, and I just said to her, I said, "I can't understand

96
00:06:42,590 --> 00:06:44,000
 what you're saying."

97
00:06:44,000 --> 00:06:48,550
 I said, "That's never happened to me since. I was really in

98
00:06:48,550 --> 00:06:52,900
 a bad way, because I was really realizing how messed up my

99
00:06:52,900 --> 00:06:54,000
 mind was."

