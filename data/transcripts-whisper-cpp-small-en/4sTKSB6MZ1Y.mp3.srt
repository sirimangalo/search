1
00:00:00,000 --> 00:00:07,000
 Hey, good evening, everyone.

2
00:00:07,000 --> 00:00:14,000
 Welcome to our weekly Dhamma talk.

3
00:00:14,000 --> 00:00:24,990
 On Saturdays I try to talk about the Dhamma, something

4
00:00:24,990 --> 00:00:28,000
 weekly for the meditators to talk

5
00:00:28,000 --> 00:00:36,780
 to listen to, to focus their attention and hopefully

6
00:00:36,780 --> 00:00:42,000
 provide direction and inspiration

7
00:00:42,000 --> 00:00:53,000
 for their practice.

8
00:00:53,000 --> 00:01:00,000
 So I'd like to talk about mindfulness tonight.

9
00:01:00,000 --> 00:01:05,000
 It's a topic, of course, that I talk about quite often.

10
00:01:05,000 --> 00:01:15,490
 It's something that we should keep coming back again to

11
00:01:15,490 --> 00:01:18,000
 thinking about,

12
00:01:18,000 --> 00:01:22,000
 clarifying our understanding about.

13
00:01:22,000 --> 00:01:25,580
 It's a word that apparently has gained some traction in

14
00:01:25,580 --> 00:01:27,000
 popular culture.

15
00:01:27,000 --> 00:01:35,000
 People talk now about mindfulness in a meditative sense.

16
00:01:35,000 --> 00:01:41,410
 The word mindful, the word mindful, of course, means

17
00:01:41,410 --> 00:01:47,000
 something like being considerate.

18
00:01:47,000 --> 00:01:52,000
 It's how it used to be used before I got into meditation.

19
00:01:52,000 --> 00:01:55,000
 That's how I thought to use it.

20
00:01:55,000 --> 00:01:58,000
 You be mindful of the time.

21
00:01:58,000 --> 00:02:02,000
 Don't lose track of the time.

22
00:02:02,000 --> 00:02:07,000
 You be mindful of other people's feelings.

23
00:02:07,000 --> 00:02:12,000
 Don't be oblivious.

24
00:02:12,000 --> 00:02:13,000
 That's a good word, no?

25
00:02:13,000 --> 00:02:19,000
 It's a good word for what we're talking about in Buddhism.

26
00:02:19,000 --> 00:02:21,000
 Don't be oblivious.

27
00:02:21,000 --> 00:02:32,000
 Be mindful.

28
00:02:32,000 --> 00:02:37,000
 And so it comes to be a bit of a buzzword.

29
00:02:37,000 --> 00:02:43,000
 And I think there's some rightful criticism of the fact

30
00:02:43,000 --> 00:02:50,000
 that it's perhaps used a little too loosely.

31
00:02:50,000 --> 00:02:53,000
 We talk about being mindful.

32
00:02:53,000 --> 00:02:58,000
 Be mindful.

33
00:02:58,000 --> 00:03:01,000
 Be mindful.

34
00:03:01,000 --> 00:03:06,510
 And it feels like sometimes we talk about it more than we

35
00:03:06,510 --> 00:03:08,000
 actually do it.

36
00:03:08,000 --> 00:03:11,000
 Because it's a general sort of buzzword you can use to say,

37
00:03:11,000 --> 00:03:13,000
 well, be mindful of your thoughts.

38
00:03:13,000 --> 00:03:19,000
 Be mindful of breathing.

39
00:03:19,000 --> 00:03:21,000
 It's easy to talk about.

40
00:03:21,000 --> 00:03:24,940
 But the question is, how do you really be mindful and what

41
00:03:24,940 --> 00:03:29,000
 does it really mean to be mindful?

42
00:03:29,000 --> 00:03:34,470
 The word sati, of course, that we translate as mindfulness,

43
00:03:34,470 --> 00:03:39,230
 has similar problems if we're to actually call these

44
00:03:39,230 --> 00:03:42,000
 problems.

45
00:03:42,000 --> 00:03:48,300
 It has problems in the sense that it's used as well in a

46
00:03:48,300 --> 00:03:52,000
 more general sense of having sati.

47
00:03:52,000 --> 00:03:57,330
 It's interesting when you go to Buddhist countries where a

48
00:03:57,330 --> 00:04:03,890
 lot of people don't have strong background in satipatthana

49
00:04:03,890 --> 00:04:05,000
 practice.

50
00:04:05,000 --> 00:04:09,220
 When they use the words, the word sati is, of course, a

51
00:04:09,220 --> 00:04:11,000
 word they use in Thai.

52
00:04:11,000 --> 00:04:16,000
 When I was in Thailand, you hear, "Haimi Sati."

53
00:04:16,000 --> 00:04:17,000
 Chai Sati.

54
00:04:17,000 --> 00:04:22,600
 No, hai misati is what my teacher used to say. Chai Sati is

55
00:04:22,600 --> 00:04:24,000
 the general expression.

56
00:04:24,000 --> 00:04:28,000
 Mot sati, that kind of thing.

57
00:04:28,000 --> 00:04:38,000
 Use it in a general sense.

58
00:04:38,000 --> 00:04:43,740
 You see it used in the sense of being able to remember

59
00:04:43,740 --> 00:04:45,000
 things.

60
00:04:45,000 --> 00:04:49,990
 It doesn't really actually mean mindfulness because even

61
00:04:49,990 --> 00:04:53,000
 though mindfulness is a good thing,

62
00:04:53,000 --> 00:05:01,000
 mindfulness means to not lose track, to not be oblivious.

63
00:05:01,000 --> 00:05:09,000
 It doesn't exactly mean to not forget.

64
00:05:09,000 --> 00:05:13,000
 But sati means to remember.

65
00:05:13,000 --> 00:05:16,000
 So if you remember things that happened a long time ago,

66
00:05:16,000 --> 00:05:17,000
 that means having sati.

67
00:05:17,000 --> 00:05:23,880
 It's not quite mindfulness, but mindfulness speaks to the

68
00:05:23,880 --> 00:05:26,000
 special usage of the word.

69
00:05:26,000 --> 00:05:32,960
 When we use sati to mean remembering the present moment,

70
00:05:32,960 --> 00:05:38,000
 then it really does relate to mindfulness.

71
00:05:38,000 --> 00:05:42,540
 Be mindful of what you're doing. Mind what you're doing.

72
00:05:42,540 --> 00:05:43,000
 Mind your steps.

73
00:05:43,000 --> 00:05:47,000
 Mind your speech.

74
00:05:47,000 --> 00:05:52,000
 Mind your manners.

75
00:05:52,000 --> 00:05:55,000
 Mind your manners.

76
00:05:55,000 --> 00:06:00,000
 Apply your mind to it.

77
00:06:00,000 --> 00:06:04,240
 If you mind your manners, it means you have some fortitude

78
00:06:04,240 --> 00:06:11,000
 of mind, that you're not just ignoring your manners.

79
00:06:11,000 --> 00:06:23,000
 You're not just loosened.

80
00:06:23,000 --> 00:06:32,000
 You're free with your behavior.

81
00:06:32,000 --> 00:06:37,470
 And so sati in a special sense, when we get right down to

82
00:06:37,470 --> 00:06:40,000
 it, it has a very specific meaning.

83
00:06:40,000 --> 00:06:44,000
 We have a definition that they give.

84
00:06:44,000 --> 00:06:54,000
 Sati means, sati has the characteristic of not wavering.

85
00:06:54,000 --> 00:07:01,430
 So sati means a certain fortitude of mind, a strength of

86
00:07:01,430 --> 00:07:03,000
 mind.

87
00:07:03,000 --> 00:07:13,000
 Sati involves the mind state that keeps the object in mind.

88
00:07:13,000 --> 00:07:17,840
 Our ordinary state of mind is wavering. It's fleeting here

89
00:07:17,840 --> 00:07:25,000
 and there, flitting from object to object.

90
00:07:25,000 --> 00:07:36,000
 We see something and we're very briefly actually seeing it.

91
00:07:36,000 --> 00:07:44,680
 The majority of our time is spent in reacting to it,

92
00:07:44,680 --> 00:07:59,000
 judging it.

93
00:07:59,000 --> 00:08:03,530
 When you watch your mind, perhaps for the first time, it

94
00:08:03,530 --> 00:08:08,000
 can be quite shocking to see how random everything is,

95
00:08:08,000 --> 00:08:13,360
 how random mental activity can be jumping here and there, a

96
00:08:13,360 --> 00:08:15,000
 mile a minute.

97
00:08:15,000 --> 00:08:20,730
 If our thoughts were speech, we would just be, we would

98
00:08:20,730 --> 00:08:24,000
 write a book in a matter of minutes.

99
00:08:24,000 --> 00:08:43,000
 It can be the kind of book you'd want to read.

100
00:08:43,000 --> 00:08:49,520
 So mindfulness is the ability to stabilize this process, so

101
00:08:49,520 --> 00:08:51,000
 that seeing is just seeing.

102
00:08:51,000 --> 00:08:57,000
 When you're seeing, your mind is with the object.

103
00:08:57,000 --> 00:09:08,000
 It's this strong state of mind that grasps the object well.

104
00:09:08,000 --> 00:09:23,330
 It has the function. What's its function? Its function is

105
00:09:23,330 --> 00:09:25,000
 to not forget, to not lose track of the object.

106
00:09:25,000 --> 00:09:32,840
 We talk about forgetting yourself. In the world when

107
00:09:32,840 --> 00:09:37,000
 someone forgets themselves,

108
00:09:37,000 --> 00:09:42,520
 maybe in the middle of a group of people and suddenly you

109
00:09:42,520 --> 00:09:46,280
 say something that's only suitable for when you're alone or

110
00:09:46,280 --> 00:09:51,000
 only suitable in your mind.

111
00:09:51,000 --> 00:09:55,000
 Like you'll be eating dinner at someone's house and you say

112
00:09:55,000 --> 00:09:59,000
, "Boy, this meal tastes awful."

113
00:09:59,000 --> 00:10:04,940
 Oh, you forgot yourself. You shouldn't have said that in

114
00:10:04,940 --> 00:10:10,000
 front of the people who cook the food.

115
00:10:10,000 --> 00:10:15,630
 No, when you forget yourself, it means you fly off the

116
00:10:15,630 --> 00:10:21,700
 collar and get angry and start shouting, "You've forgotten

117
00:10:21,700 --> 00:10:23,000
 yourself."

118
00:10:23,000 --> 00:10:27,000
 Or they say it when a person is of a low station.

119
00:10:27,000 --> 00:10:30,290
 Suppose you're a child and you start mouthing off to your

120
00:10:30,290 --> 00:10:31,000
 parents.

121
00:10:31,000 --> 00:10:36,000
 In some cultures they would say you're forgetting yourself.

122
00:10:36,000 --> 00:10:40,830
 In Western culture we tend to overlook the good that our

123
00:10:40,830 --> 00:10:44,000
 parents have done for us and so on and so on.

124
00:10:44,000 --> 00:10:47,330
 We're not that kind to them. I would say not as kind as we

125
00:10:47,330 --> 00:10:52,000
 should be, not as respectful as we should be.

126
00:10:52,000 --> 00:10:56,850
 Many of us anyway. Some are perhaps overly respectful

127
00:10:56,850 --> 00:11:02,250
 because of it being drilled into them forcefully, which is

128
00:11:02,250 --> 00:11:05,000
 also not great.

129
00:11:05,000 --> 00:11:08,620
 Anyway, we talk about forgetting yourself in the sense that

130
00:11:08,620 --> 00:11:14,290
 you should be like this and you've totally forgot how you

131
00:11:14,290 --> 00:11:16,000
 should be.

132
00:11:16,000 --> 00:11:20,660
 It's not exactly what mindfulness, but it's along the same

133
00:11:20,660 --> 00:11:24,000
 lines because the way we should be is objective.

134
00:11:24,000 --> 00:11:31,000
 We should be aware of things as they are.

135
00:11:31,000 --> 00:11:40,000
 We should be present and merely present, but we're not.

136
00:11:40,000 --> 00:11:43,410
 We're not only are we not present, we're so much more than

137
00:11:43,410 --> 00:11:44,000
 that.

138
00:11:44,000 --> 00:11:52,000
 We are reacting and judging and forgetting ourselves.

139
00:11:52,000 --> 00:12:04,740
 Mindfulness is not forgetting. Mindfulness is about

140
00:12:04,740 --> 00:12:09,000
 constantly remembering, remembering the experience.

141
00:12:09,000 --> 00:12:14,000
 This is the experience. This is remembering the essence.

142
00:12:14,000 --> 00:12:27,630
 Let seeing just be seeing. I'm not forgetting that. This is

143
00:12:27,630 --> 00:12:30,000
 seeing.

144
00:12:30,000 --> 00:12:42,000
 It manifests itself as guarding.

145
00:12:42,000 --> 00:12:48,400
 The way it manifests, this means when a person is mindful,

146
00:12:48,400 --> 00:12:54,490
 what is the key quality that you can see in them or they

147
00:12:54,490 --> 00:12:57,000
 can see in themselves when they're mindful?

148
00:12:57,000 --> 00:12:59,680
 How can you tell if you're being mindful? How does it

149
00:12:59,680 --> 00:13:01,000
 manifest itself?

150
00:13:01,000 --> 00:13:06,170
 It manifests itself as guarding, meaning guarding the

151
00:13:06,170 --> 00:13:12,010
 senses, meaning when you see things and you're just seeing,

152
00:13:12,010 --> 00:13:15,000
 there's no reaction.

153
00:13:15,000 --> 00:13:18,950
 When you feel like your senses are perfectly filtered so

154
00:13:18,950 --> 00:13:23,080
 that you're just seeing, you're just hearing, you're just

155
00:13:23,080 --> 00:13:30,000
 smelling.

156
00:13:30,000 --> 00:13:43,850
 When you're guarded, when we talk about guarding the senses

157
00:13:43,850 --> 00:13:48,510
, we talk about guarding your, in a general sense, we talk

158
00:13:48,510 --> 00:13:54,000
 about guarding your, what's the phrase that they use?

159
00:13:54,000 --> 00:13:59,180
 Guard your thoughts, guard your emotions, that kind of

160
00:13:59,180 --> 00:14:00,000
 thing.

161
00:14:00,000 --> 00:14:04,770
 So there's ways of guarding yourself by suppressing, by

162
00:14:04,770 --> 00:14:10,350
 brute force, where you just force yourself not to react

163
00:14:10,350 --> 00:14:12,000
 externally.

164
00:14:12,000 --> 00:14:15,240
 You might be seething inside, but I'm not going to say

165
00:14:15,240 --> 00:14:18,560
 anything nasty or you might be chomping at the bit to get

166
00:14:18,560 --> 00:14:22,480
 this or get that, but you hold yourself back and say, "No,

167
00:14:22,480 --> 00:14:31,000
 no. I will not get it."

168
00:14:31,000 --> 00:14:34,340
 And we do this because we understand the consequences. You

169
00:14:34,340 --> 00:14:39,310
 might repress your emotions out of a knowledge that the

170
00:14:39,310 --> 00:14:43,380
 consequences are going to be far more troublesome than they

171
00:14:43,380 --> 00:14:44,000
're worth.

172
00:14:44,000 --> 00:14:50,610
 And so you got to stop yourself. But mindfulness is more

173
00:14:50,610 --> 00:14:53,000
 successful.

174
00:14:53,000 --> 00:14:57,940
 The Buddha said, "Yaani sotani lokasming sati niwari, sati

175
00:14:57,940 --> 00:15:02,320
 te sangni warayang." Whatever streams, the literal word is

176
00:15:02,320 --> 00:15:06,560
 streams, but it means things that will get you caught up in

177
00:15:06,560 --> 00:15:10,000
 the world, will get you in trouble.

178
00:15:10,000 --> 00:15:17,670
 Whatever things there are that will get you caught up in a

179
00:15:17,670 --> 00:15:19,000
 stream.

180
00:15:19,000 --> 00:15:23,860
 Sati te sangni warayang, sati stops them. Sati is what

181
00:15:23,860 --> 00:15:28,000
 prevents that, prevents you from getting caught up.

182
00:15:28,000 --> 00:15:31,360
 Mindfulness is better. It's better than repressing. It's

183
00:15:31,360 --> 00:15:36,300
 better than forcing yourself to behave, pretending, hiding

184
00:15:36,300 --> 00:15:42,000
 who you really are, taking drugs, medication.

185
00:15:42,000 --> 00:15:48,550
 Sati is a better solution. It's not easy. It takes training

186
00:15:48,550 --> 00:15:54,050
. But it's far superior because mindfulness is like water.

187
00:15:54,050 --> 00:16:01,000
 Mindfulness dissolves the problems.

188
00:16:01,000 --> 00:16:09,910
 There's no contrivance. There's no artifice. There's just

189
00:16:09,910 --> 00:16:18,870
 objectivity. There's just awareness, seeing things as they

190
00:16:18,870 --> 00:16:20,000
 are.

191
00:16:20,000 --> 00:16:24,970
 So it guards your senses in a way that guarding your being

192
00:16:24,970 --> 00:16:30,000
 wary and being afraid of your own emotions, who never will.

193
00:16:30,000 --> 00:16:34,740
 Much better than being afraid, having to live in fear and

194
00:16:34,740 --> 00:16:42,000
 being a slave to your emotions. It's far better than that.

195
00:16:42,000 --> 00:16:48,610
 Or it also has the manifestation of confronting. Another

196
00:16:48,610 --> 00:16:52,000
 aspect of its manifestation is confronting.

197
00:16:52,000 --> 00:17:01,930
 We saya bhimukh bhava, the state of confronting the

198
00:17:01,930 --> 00:17:05,000
 experience.

199
00:17:05,000 --> 00:17:11,160
 So our ordinary state is not able to confront. We react. We

200
00:17:11,160 --> 00:17:17,270
 run away. If it's bad, we're constantly avoiding, finding

201
00:17:17,270 --> 00:17:28,000
 ways to destroy, to get rid of, or to avoid, to escape.

202
00:17:28,000 --> 00:17:34,450
 And if it's positive, we also aren't confronting. We aren't

203
00:17:34,450 --> 00:17:39,000
 with the experience. When you get something that you want,

204
00:17:39,000 --> 00:17:43,000
 the last thing you do is stay with the experience.

205
00:17:43,000 --> 00:17:48,110
 You're immediately off in the, "Oh, this is wonderful." How

206
00:17:48,110 --> 00:17:52,840
 to get it more, how to, either recursive thought about how

207
00:17:52,840 --> 00:17:56,650
 you love it, or recursive thought about how you can get it

208
00:17:56,650 --> 00:18:03,910
 more, or keep it, or cling to it, get closer to it,

209
00:18:03,910 --> 00:18:11,000
 stronger, more satisfying.

210
00:18:11,000 --> 00:18:14,630
 Sati confronts. Sati is about being with experience. It's

211
00:18:14,630 --> 00:18:19,520
 involving patience. Patience is another good word in

212
00:18:19,520 --> 00:18:24,880
 Buddhism. Patience is about sticking with good things and

213
00:18:24,880 --> 00:18:31,020
 bad things, and rather than reacting to them, being with

214
00:18:31,020 --> 00:18:32,000
 them.

215
00:18:32,000 --> 00:18:39,190
 It's the ability to be present when something pleasant or

216
00:18:39,190 --> 00:18:47,000
 unpleasant comes and not be upset by it, not be disturbed.

217
00:18:47,000 --> 00:18:51,050
 That's what sati is. The last thing is the cause of sati.

218
00:18:51,050 --> 00:18:54,000
 What is it that gives rise to sati?

219
00:18:54,000 --> 00:18:57,570
 And it's another part of the definition, how you can know

220
00:18:57,570 --> 00:19:02,010
 whether you're being mindful and how to be mindful. The

221
00:19:02,010 --> 00:19:13,000
 cause of mindfulness is something called tira sanya.

222
00:19:13,000 --> 00:19:18,570
 Sanya is, in this context, means the perception of a thing.

223
00:19:18,570 --> 00:19:23,510
 So we're, in some sense, we're immediately mindful of

224
00:19:23,510 --> 00:19:25,000
 everything.

225
00:19:25,000 --> 00:19:28,930
 When you see, there's no question that you have an

226
00:19:28,930 --> 00:19:31,000
 awareness of the seeing.

227
00:19:31,000 --> 00:19:34,370
 This is why it can be confusing as to what is different

228
00:19:34,370 --> 00:19:38,550
 about being mindful. You think, well, of course, when I see

229
00:19:38,550 --> 00:19:41,000
, of course, I know I'm seeing.

230
00:19:41,000 --> 00:19:46,010
 Satat sanya, the knowledge that, ah, this is seeing and

231
00:19:46,010 --> 00:19:49,000
 what you're seeing, and so on.

232
00:19:49,000 --> 00:19:56,000
 But tira sanya, tira means strengthened or strong or firm.

233
00:19:56,000 --> 00:20:00,000
 So there's a sense of strengthening.

234
00:20:00,000 --> 00:20:04,000
 How do you create sati by strengthening your perception?

235
00:20:04,000 --> 00:20:07,000
 What that means is when you see, you strengthen it.

236
00:20:07,000 --> 00:20:12,740
 You find a way to reinforce the state of that just being

237
00:20:12,740 --> 00:20:14,000
 seeing.

238
00:20:14,000 --> 00:20:17,320
 It's fine and good to say, let seeing be seeing and stop

239
00:20:17,320 --> 00:20:21,590
 there. But how do you stop there? How do you reinforce that

240
00:20:21,590 --> 00:20:22,000
?

241
00:20:22,000 --> 00:20:28,420
 That's the practice that leads to mindfulness, leads to sat

242
00:20:28,420 --> 00:20:29,000
i.

243
00:20:29,000 --> 00:20:32,570
 That's why we use the mantra. That's why we have a word to

244
00:20:32,570 --> 00:20:34,000
 repeat to ourselves.

245
00:20:34,000 --> 00:20:37,480
 Why we say to ourselves seeing, seeing. Why so much

246
00:20:37,480 --> 00:20:41,440
 meditation is about repeating mantras because it reaffirms

247
00:20:41,440 --> 00:20:45,000
 your perception that you're trying to focus on.

248
00:20:45,000 --> 00:20:48,730
 If it's samatha meditation, it would be the perception of a

249
00:20:48,730 --> 00:20:50,000
 specific thing.

250
00:20:50,000 --> 00:20:52,280
 And we pass into meditation, it's the perception of your

251
00:20:52,280 --> 00:20:53,000
 experience.

252
00:20:53,000 --> 00:20:56,780
 When seeing, you say to yourself, seeing, when hearing, you

253
00:20:56,780 --> 00:21:00,000
'd say hearing, when pain, you'd say pain, pain.

254
00:21:00,000 --> 00:21:11,340
 Tira sannya. This is really the practice that we teach,

255
00:21:11,340 --> 00:21:13,000
 that we practice.

256
00:21:13,000 --> 00:21:18,980
 It's the practice of strengthening our experiences,

257
00:21:18,980 --> 00:21:23,000
 strengthening our awareness of the experiences, reminding

258
00:21:23,000 --> 00:21:26,000
 ourselves so that we don't forget.

259
00:21:26,000 --> 00:21:36,000
 We don't lose ourselves.

260
00:21:36,000 --> 00:21:39,040
 Or its proximate cause is simply the four foundations of

261
00:21:39,040 --> 00:21:40,000
 mindfulness.

262
00:21:40,000 --> 00:21:43,410
 If you want to look at it from a practical perspective, it

263
00:21:43,410 --> 00:21:46,000
 means practicing the four satipatthana.

264
00:21:46,000 --> 00:21:50,000
 It's a different way of looking at it.

265
00:21:50,000 --> 00:21:55,010
 When we watch the stomach and say to ourselves, rising,

266
00:21:55,010 --> 00:21:58,800
 falling, or when we walk and say stepping right, stepping

267
00:21:58,800 --> 00:22:01,000
 left, mindfulness of the body.

268
00:22:01,000 --> 00:22:05,590
 When you have pain, you say to yourself, pain, pain, that's

269
00:22:05,590 --> 00:22:08,000
 mindfulness of the feelings.

270
00:22:08,000 --> 00:22:10,890
 When you have thoughts and you say to yourself, thinking,

271
00:22:10,890 --> 00:22:13,820
 thinking, good thoughts, bad thoughts, past, future,

272
00:22:13,820 --> 00:22:15,000
 whatever thoughts,

273
00:22:15,000 --> 00:22:19,810
 you say to yourself, thinking, thinking, that's mindfulness

274
00:22:19,810 --> 00:22:21,000
 of the mind.

275
00:22:21,000 --> 00:22:24,540
 Liking, disliking, drowsiness, distraction, doubt. When you

276
00:22:24,540 --> 00:22:29,000
 say to yourself, liking, liking, or wanting, wanting.

277
00:22:29,000 --> 00:22:37,210
 When you say disliking, disliking, bored, sad, afraid,

278
00:22:37,210 --> 00:22:41,000
 depressed, frustrated.

279
00:22:41,000 --> 00:22:50,000
 Just this reaffirmation, the practicing mindfulness.

280
00:22:50,000 --> 00:23:00,080
 This is what leads to the state of being mindful, the state

281
00:23:00,080 --> 00:23:02,000
 of sati.

282
00:23:02,000 --> 00:23:05,000
 Drowsy or tired, you say tired, tired.

283
00:23:05,000 --> 00:23:09,950
 When you're thinking a lot, when your mind is not focused,

284
00:23:09,950 --> 00:23:13,000
 you say distracted or unfocused.

285
00:23:13,000 --> 00:23:20,640
 When you're doubting or confused, you'd say doubting, doub

286
00:23:20,640 --> 00:23:25,000
ting, or confused, confused.

287
00:23:25,000 --> 00:23:34,000
 What's the cause? That's how you become mindful.

288
00:23:34,000 --> 00:23:40,000
 That's how you develop sati.

289
00:23:40,000 --> 00:23:44,750
 Mindfulness should be regarded as a pillar. Mindfulness

290
00:23:44,750 --> 00:23:55,000
 should be seen as this stake stuck in the ground.

291
00:23:55,000 --> 00:24:03,990
 Pillars being strong, pillars being a symbol of strength.

292
00:24:03,990 --> 00:24:04,000
 Mindfulness is like a pillar because a pillar,

293
00:24:04,000 --> 00:24:07,020
 the reason you have a pillar is to strengthen something.

294
00:24:07,020 --> 00:24:09,000
 That's what pillars are for.

295
00:24:09,000 --> 00:24:13,780
 They are unmoving. If you have a pillar that wobbles around

296
00:24:13,780 --> 00:24:16,000
, you don't have a pillar.

297
00:24:16,000 --> 00:24:20,700
 If your pillar moves around and jumps here and there, it's

298
00:24:20,700 --> 00:24:22,000
 not a pillar.

299
00:24:22,000 --> 00:24:26,000
 If it wavers or falls over, not a very good pillar.

300
00:24:26,000 --> 00:24:32,000
 No, a pillar is designed to be strong and firm and unmoving

301
00:24:32,000 --> 00:24:36,000
 and supportive.

302
00:24:36,000 --> 00:24:40,590
 Mindfulness should be as seen as a pillar, a pillar as

303
00:24:40,590 --> 00:24:46,000
 opposed to a pumpkin, a gourd.

304
00:24:46,000 --> 00:24:51,270
 A gourd is something very light before they had...this is

305
00:24:51,270 --> 00:24:55,000
 in ancient times, of course, things that float on water.

306
00:24:55,000 --> 00:25:00,140
 There's not a great number of natural things that float on

307
00:25:00,140 --> 00:25:02,000
 water like a gourd.

308
00:25:02,000 --> 00:25:06,060
 Nowadays it would be something like a rubber duck or

309
00:25:06,060 --> 00:25:08,000
 something like that.

310
00:25:08,000 --> 00:25:16,100
 But a gourd, because it's hollow, of course, it floats on

311
00:25:16,100 --> 00:25:18,000
 the water.

312
00:25:18,000 --> 00:25:22,140
 So when you put a gourd on the water, it's the epitome, it

313
00:25:22,140 --> 00:25:25,000
's a symbol of everything that a pillar is not.

314
00:25:25,000 --> 00:25:28,230
 It flits here and there, it's buffeted by the winds, the

315
00:25:28,230 --> 00:25:33,640
 slightest wave and it's off and moving around here and

316
00:25:33,640 --> 00:25:35,000
 there.

317
00:25:35,000 --> 00:25:39,490
 The ordinary mind is like that. The ordinary mind is like

318
00:25:39,490 --> 00:25:41,000
 the gourd on the water.

319
00:25:41,000 --> 00:25:47,920
 Mindfulness is the exact opposite. It's a pillar stuck in

320
00:25:47,920 --> 00:25:54,000
 the bottom of the ocean.

321
00:25:54,000 --> 00:26:00,610
 Mindfulness is like a guard, a doorkeep. It guards the

322
00:26:00,610 --> 00:26:03,000
 doors of the senses.

323
00:26:03,000 --> 00:26:08,530
 It should be seen as the guard. Our mind has precious

324
00:26:08,530 --> 00:26:10,000
 treasure.

325
00:26:10,000 --> 00:26:16,360
 Our mind should be a place of precious treasure, where we

326
00:26:16,360 --> 00:26:23,000
 keep all sorts of good qualities like focus and wisdom.

327
00:26:23,000 --> 00:26:27,160
 I suppose we start with this great treasure house and

328
00:26:27,160 --> 00:26:29,730
 because we don't have a very good guard set up, a lot of

329
00:26:29,730 --> 00:26:32,000
 our treasure gets stolen.

330
00:26:32,000 --> 00:26:37,000
 We maybe get some treasure, but we lose it.

331
00:26:37,000 --> 00:26:41,460
 We don't have a lot of treasure because it's not easy to

332
00:26:41,460 --> 00:26:44,000
 keep treasure without guards.

333
00:26:44,000 --> 00:26:48,760
 But if you want treasures like concentration and wisdom and

334
00:26:48,760 --> 00:26:56,000
 love and compassion, patience, peace, happiness,

335
00:26:56,000 --> 00:27:01,210
 you need a guard. Mindfulness is that guard. It guards the

336
00:27:01,210 --> 00:27:02,000
 doors,

337
00:27:02,000 --> 00:27:11,640
 guards our senses to keep defilements from stealing our

338
00:27:11,640 --> 00:27:15,000
 good qualities.

339
00:27:15,000 --> 00:27:20,950
 That's mindfulness. Again, most of this, if you're an avid

340
00:27:20,950 --> 00:27:25,000
 follower, some of you I think are here every week.

341
00:27:25,000 --> 00:27:30,000
 A lot of this is familiar, but familiar is often good.

342
00:27:30,000 --> 00:27:34,230
 What we're trying to do is not complicated. It's not

343
00:27:34,230 --> 00:27:36,000
 complex.

344
00:27:36,000 --> 00:27:44,840
 It's just difficult. It takes training and takes patience,

345
00:27:44,840 --> 00:27:48,000
 perseverance.

346
00:27:48,000 --> 00:27:51,380
 But that's the demo for tonight. Thank you all for tuning

347
00:27:51,380 --> 00:27:56,000
 in.

