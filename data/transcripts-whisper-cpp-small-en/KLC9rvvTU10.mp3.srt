1
00:00:00,000 --> 00:00:04,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,000 --> 00:00:10,480
 Today we continue on with verse number 135, which reads as

3
00:00:10,480 --> 00:00:11,000
 follows.

4
00:00:11,000 --> 00:00:18,000
 Yatha dandenagopadhu gavopajetigotarang.

5
00:00:18,000 --> 00:00:26,000
 Ivangtara chamajuja ayungpa jintipanam.

6
00:00:26,000 --> 00:00:34,380
 Which means, just as a cow herd with a stick drives the

7
00:00:34,380 --> 00:00:38,000
 cows to the cow pasture,

8
00:00:38,000 --> 00:00:42,000
 gavopadhi digotjaran.

9
00:00:42,000 --> 00:00:51,000
 So too, Ivangtara chamajuja ayungpa jintipanam.

10
00:00:51,000 --> 00:00:59,000
 Old age and death drive the life of beings.

11
00:00:59,000 --> 00:01:05,000
 Drive the age, drive our age onto old age and death.

12
00:01:05,000 --> 00:01:09,000
 Drive us on.

13
00:01:09,000 --> 00:01:11,000
 They direct us.

14
00:01:11,000 --> 00:01:15,000
 They control us.

15
00:01:15,000 --> 00:01:19,000
 They are the master, the cow herd, they herd us.

16
00:01:19,000 --> 00:01:22,780
 We have nowhere to go but to the pastures dictated by old

17
00:01:22,780 --> 00:01:24,000
 age and death.

18
00:01:24,000 --> 00:01:26,000
 We can't avoid them.

19
00:01:26,000 --> 00:01:30,000
 Like the cows can't avoid the cow herd.

20
00:01:30,000 --> 00:01:36,390
 So the story behind this, another short story, is about Vis

21
00:01:36,390 --> 00:01:41,000
akhah, the Buddhist chief female lay disciple.

22
00:01:41,000 --> 00:01:46,950
 In the time, it's about the uposata, which would be the

23
00:01:46,950 --> 00:01:48,000
 holy day.

24
00:01:48,000 --> 00:01:53,190
 So in the time of the Buddha in India, it was tradition in

25
00:01:53,190 --> 00:01:58,000
 Indian society to consider the full moon as a holy day

26
00:01:58,000 --> 00:02:01,000
 where you would undertake religious practices.

27
00:02:01,000 --> 00:02:06,010
 So for Brahmins or Hindus, as we know them, they would take

28
00:02:06,010 --> 00:02:11,000
 it on themselves to worship whatever god they followed.

29
00:02:11,000 --> 00:02:16,330
 So in Buddhism, this was modified to suit Buddhist

30
00:02:16,330 --> 00:02:21,000
 religious principles of ethics, morality.

31
00:02:21,000 --> 00:02:25,000
 So it would be a time where people would take on advanced

32
00:02:25,000 --> 00:02:30,000
 or more strict and serious ethical and moral practices

33
00:02:30,000 --> 00:02:35,260
 of abstaining from things like entertainment, romance,

34
00:02:35,260 --> 00:02:40,000
 beautification, sleep, that kind of thing.

35
00:02:40,000 --> 00:02:45,000
 So they would dedicate themselves to the Buddhist teaching.

36
00:02:45,000 --> 00:02:53,400
 So Visakhah would of course keep these precepts as a sotap

37
00:02:53,400 --> 00:02:54,000
ana.

38
00:02:54,000 --> 00:02:57,000
 She had already seen Nibbana for the first time.

39
00:02:57,000 --> 00:03:00,000
 She was on her way to enlightenment.

40
00:03:00,000 --> 00:03:03,680
 So she was keeping them for the right reason, but she saw

41
00:03:03,680 --> 00:03:06,000
 these other women,

42
00:03:06,000 --> 00:03:10,880
 these women, or women followers actually, so they kept it

43
00:03:10,880 --> 00:03:12,000
 with her.

44
00:03:12,000 --> 00:03:16,820
 And some of them were girls, young girls, some of them were

45
00:03:16,820 --> 00:03:21,000
 newlyweds, young women in the prime of life,

46
00:03:21,000 --> 00:03:27,000
 some of them were middle aged, some of them were old,

47
00:03:27,000 --> 00:03:33,720
 seniors who were already showing signs of old age, close to

48
00:03:33,720 --> 00:03:35,000
 death even.

49
00:03:35,000 --> 00:03:37,000
 So all range of ages.

50
00:03:37,000 --> 00:03:41,000
 And so she wondered why these women were keeping.

51
00:03:41,000 --> 00:03:44,000
 She was somewhat, I suppose, suspicious, you can imagine.

52
00:03:44,000 --> 00:03:46,000
 Are they doing it for the right reasons?

53
00:03:46,000 --> 00:03:50,000
 I bet she was asking in her mind.

54
00:03:50,000 --> 00:03:55,000
 And so she came to them and she asked the old women first,

55
00:03:55,000 --> 00:03:58,280
 "Why are you keeping the uposa? Why are you keeping these

56
00:03:58,280 --> 00:03:59,000
 precepts?

57
00:03:59,000 --> 00:04:02,600
 Why do you come and listen to the Buddhist teaching and so

58
00:04:02,600 --> 00:04:03,000
 on?

59
00:04:03,000 --> 00:04:07,130
 Why do you take this holiday? What does this holiday mean

60
00:04:07,130 --> 00:04:08,000
 to you?"

61
00:04:08,000 --> 00:04:11,690
 And they said, "Oh well, we're old and the pleasures of

62
00:04:11,690 --> 00:04:13,000
 life have faded

63
00:04:13,000 --> 00:04:17,000
 and so we're looking for the pleasures of heaven.

64
00:04:17,000 --> 00:04:20,090
 We're hoping that through this practice we've heard that it

65
00:04:20,090 --> 00:04:21,000
's great wholesomeness

66
00:04:21,000 --> 00:04:25,200
 and that's what leads you to heaven, so we figure that this

67
00:04:25,200 --> 00:04:27,000
 is the way to go."

68
00:04:27,000 --> 00:04:32,000
 And then she went and asked the middle aged women,

69
00:04:32,000 --> 00:04:37,000
 "Why are you coming here to practice and keep the holiday,

70
00:04:37,000 --> 00:04:40,000
 keep the precepts and so on?"

71
00:04:40,000 --> 00:04:44,300
 And they said, "Oh well, we're here to escape the control

72
00:04:44,300 --> 00:04:46,000
 of our husbands.

73
00:04:46,000 --> 00:04:51,000
 I suppose it's a temporary escape, being able to go,

74
00:04:51,000 --> 00:04:53,730
 leave the house to go to the monastery because of course

75
00:04:53,730 --> 00:04:55,000
 women in India at the time

76
00:04:55,000 --> 00:05:00,240
 and to some extent even today in traditional societies are,

77
00:05:00,240 --> 00:05:01,000
 well,

78
00:05:01,000 --> 00:05:04,850
 were often little more than servants or possessions of

79
00:05:04,850 --> 00:05:06,000
 their husbands.

80
00:05:06,000 --> 00:05:08,500
 So they're very much under their power but when they came

81
00:05:08,500 --> 00:05:10,000
 to the monastery of course

82
00:05:10,000 --> 00:05:15,940
 there was an excuse to leave and potentially there was a

83
00:05:15,940 --> 00:05:17,000
 path out.

84
00:05:17,000 --> 00:05:20,230
 So if they practiced spiritual teachings they could leave

85
00:05:20,230 --> 00:05:21,000
 the household life

86
00:05:21,000 --> 00:05:26,000
 and become nuns or female monks.

87
00:05:26,000 --> 00:05:29,390
 And so that was why they were doing it completely just to

88
00:05:29,390 --> 00:05:31,000
 get away from their husbands

89
00:05:31,000 --> 00:05:36,490
 or in many cases abusive or at least domineering,

90
00:05:36,490 --> 00:05:39,000
 controlling.

91
00:05:39,000 --> 00:05:43,680
 And then she went on, that's interesting, she went on and

92
00:05:43,680 --> 00:05:46,000
 asked various young women

93
00:05:46,000 --> 00:05:52,250
 who were in the prime of their life, married but still

94
00:05:52,250 --> 00:05:53,000
 young.

95
00:05:53,000 --> 00:05:55,350
 So she wondered what they would say and they said, "Oh, we

96
00:05:55,350 --> 00:05:56,000
're keeping it."

97
00:05:56,000 --> 00:06:03,000
 And perhaps not entirely, but by and large she found that

98
00:06:03,000 --> 00:06:04,000
 they were keeping it

99
00:06:04,000 --> 00:06:08,000
 for things like, and it gives the example of having babies.

100
00:06:08,000 --> 00:06:13,520
 So it was a big deal among many of them, that religious

101
00:06:13,520 --> 00:06:15,000
 practice was

102
00:06:15,000 --> 00:06:18,260
 and to some extent still is in traditional Buddhist

103
00:06:18,260 --> 00:06:19,000
 societies,

104
00:06:19,000 --> 00:06:23,000
 understood to facilitate childbirth.

105
00:06:23,000 --> 00:06:25,000
 There's something special about the karma.

106
00:06:25,000 --> 00:06:31,000
 You could explain it.

107
00:06:31,000 --> 00:06:34,150
 Now, I think a lot of superstition, obviously that's what

108
00:06:34,150 --> 00:06:35,000
 it sounds like,

109
00:06:35,000 --> 00:06:41,000
 but the idea that it leads to childbirth

110
00:06:41,000 --> 00:06:49,500
 because of how birth comes about, it's not just chants or

111
00:06:49,500 --> 00:06:52,000
 random acts,

112
00:06:52,000 --> 00:06:56,000
 blind luck that a woman is able to conceive.

113
00:06:56,000 --> 00:07:00,570
 Now, it requires of course the male and the female, but it

114
00:07:00,570 --> 00:07:02,000
 also requires a mind.

115
00:07:02,000 --> 00:07:06,000
 It also requires a being that is coming to be reborn.

116
00:07:06,000 --> 00:07:10,000
 And so for a being to be reborn as a human being,

117
00:07:10,000 --> 00:07:12,000
 they have to have wholesome qualities.

118
00:07:12,000 --> 00:07:15,000
 It takes something special to be born as a human being.

119
00:07:15,000 --> 00:07:21,000
 And so the idea is that it also requires something special,

120
00:07:21,000 --> 00:07:26,000
 something attractive to such a being.

121
00:07:26,000 --> 00:07:30,040
 So there's the idea that religious practice is somehow

122
00:07:30,040 --> 00:07:31,000
 attractive

123
00:07:31,000 --> 00:07:38,000
 and therefore puts one in a state where one is receptive

124
00:07:38,000 --> 00:07:44,000
 or is attractive to such a being, pleasant and peaceful.

125
00:07:44,000 --> 00:07:47,440
 You could also say that the mind that comes from spiritual

126
00:07:47,440 --> 00:07:48,000
 practice

127
00:07:48,000 --> 00:07:52,510
 is both physically more receptive to childbirth, but also

128
00:07:52,510 --> 00:07:53,000
 mentally.

129
00:07:53,000 --> 00:07:56,600
 So any being that is interested in being born as a human

130
00:07:56,600 --> 00:07:57,000
 being

131
00:07:57,000 --> 00:08:01,110
 will gravitate towards the minds that are at peace, the

132
00:08:01,110 --> 00:08:02,000
 minds that are wholesome.

133
00:08:02,000 --> 00:08:05,000
 So there is an argument, I think, to be made

134
00:08:05,000 --> 00:08:10,150
 that it probably increases your chances of not only of

135
00:08:10,150 --> 00:08:11,000
 giving birth,

136
00:08:11,000 --> 00:08:16,140
 but of giving birth to a child who has wholesome qualities,

137
00:08:16,140 --> 00:08:17,000
 obviously.

138
00:08:17,000 --> 00:08:21,000
 If you're an unwholesome person, it's more likely that your

139
00:08:21,000 --> 00:08:21,000
 children,

140
00:08:21,000 --> 00:08:25,000
 the beings that are going to be attracted to your womb,

141
00:08:25,000 --> 00:08:30,000
 are going to be equally unwholesome, some food for thought.

142
00:08:30,000 --> 00:08:32,000
 But anyway, they have this belief.

143
00:08:32,000 --> 00:08:37,450
 It's not a Buddhist. None of these are really the proper

144
00:08:37,450 --> 00:08:38,000
 reason.

145
00:08:38,000 --> 00:08:40,000
 So she's at this point starting to shake her head

146
00:08:40,000 --> 00:08:42,470
 and wondering if anybody's really doing it for the right

147
00:08:42,470 --> 00:08:43,000
 reason.

148
00:08:43,000 --> 00:08:46,000
 These are not proper reasons to practice.

149
00:08:46,000 --> 00:08:51,000
 They're not bad or unwholesome, but they're quite limited.

150
00:08:51,000 --> 00:08:53,000
 And so that's what the Buddhist is going to talk about

151
00:08:53,000 --> 00:08:55,000
 when she finally gets to him.

152
00:08:55,000 --> 00:08:59,000
 So she goes to the maidens, the young girls, and asks them,

153
00:08:59,000 --> 00:09:00,000
 "Why are you doing it?

154
00:09:00,000 --> 00:09:02,000
 "Well, these other ones have these ulterior motives.

155
00:09:02,000 --> 00:09:04,000
 "What's your reason? Maybe it's pure."

156
00:09:04,000 --> 00:09:08,000
 She found, by and large, the unmarried women,

157
00:09:08,000 --> 00:09:10,000
 why were they practicing?

158
00:09:10,000 --> 00:09:14,000
 Because they felt that spiritual practice would help them

159
00:09:14,000 --> 00:09:16,000
 to obtain a husband

160
00:09:16,000 --> 00:09:19,000
 while they were still young, which is kind of funny,

161
00:09:19,000 --> 00:09:20,650
 because the older women are trying to get away from their

162
00:09:20,650 --> 00:09:21,000
 husbands,

163
00:09:21,000 --> 00:09:25,000
 and the younger women are wishing to get husbands.

164
00:09:25,000 --> 00:09:32,000
 So it's not entirely contradictory or absurd,

165
00:09:32,000 --> 00:09:40,000
 because there was the sense that it was actually worse

166
00:09:40,000 --> 00:09:44,250
 than whatever you might face at the hands of a husband to

167
00:09:44,250 --> 00:09:46,000
 be unmarried.

168
00:09:46,000 --> 00:09:48,000
 If you're unmarried, then you're a burden on your family,

169
00:09:48,000 --> 00:09:51,000
 who would treat you as a little better than a servant.

170
00:09:51,000 --> 00:09:56,000
 An unmarried woman in this society was not very well,

171
00:09:56,000 --> 00:10:01,000
 was the lowest of the low and disrespected

172
00:10:01,000 --> 00:10:06,000
 in favor of women who were fruitful and didn't marry,

173
00:10:06,000 --> 00:10:11,000
 gave honor and wealth and status to their families.

174
00:10:11,000 --> 00:10:14,380
 So not getting married was probably a fate worse than

175
00:10:14,380 --> 00:10:15,000
 getting married,

176
00:10:15,000 --> 00:10:18,000
 and neither one is all that pleasant.

177
00:10:18,000 --> 00:10:22,000
 This was the state of women in India at the time.

178
00:10:22,000 --> 00:10:24,000
 Suffice to say, they had their reasons.

179
00:10:24,000 --> 00:10:30,610
 Now, these are not goals that are unwholesome, but they're

180
00:10:30,610 --> 00:10:32,000
 limited.

181
00:10:32,000 --> 00:10:34,000
 So shaking her head and not really understanding,

182
00:10:34,000 --> 00:10:36,100
 she went to the Buddha and said, "What's going on with

183
00:10:36,100 --> 00:10:37,000
 these people?

184
00:10:37,000 --> 00:10:41,000
 Why don't they see the true benefit of spirituality?

185
00:10:41,000 --> 00:10:42,000
 Why can't they see that?"

186
00:10:42,000 --> 00:10:50,000
 And the Buddha taught this verse, and he said,

187
00:10:50,000 --> 00:10:52,990
 "Birth" -- here, this is the quote -- "birth, old age,

188
00:10:52,990 --> 00:10:54,000
 sickness and death

189
00:10:54,000 --> 00:10:57,000
 are like cowards with staves in their hands.

190
00:10:57,000 --> 00:11:01,000
 Birth sends them to old age, old age sends them to sickness

191
00:11:01,000 --> 00:11:01,000
,

192
00:11:01,000 --> 00:11:04,000
 and sickness sends them to death.

193
00:11:04,000 --> 00:11:07,640
 These things cut life short as though they cut with an axe

194
00:11:07,640 --> 00:11:08,000
."

195
00:11:08,000 --> 00:11:10,780
 And he says, "But despite this, there are none that desire

196
00:11:10,780 --> 00:11:12,000
 absence of rebirth."

197
00:11:12,000 --> 00:11:15,000
 This is the most interesting part of the story.

198
00:11:15,000 --> 00:11:18,000
 It's an important point that he makes.

199
00:11:18,000 --> 00:11:24,450
 "None" -- it's not "none," but he's saying basically "none

200
00:11:24,450 --> 00:11:27,000
," more or less --

201
00:11:27,000 --> 00:11:31,000
 "nobody in the world desires absence of rebirth."

202
00:11:31,000 --> 00:11:33,000
 Very few. That's the idea.

203
00:11:33,000 --> 00:11:38,000
 "Rebirth is all they desire."

204
00:11:38,000 --> 00:11:40,000
 And then he says this verse.

205
00:11:40,000 --> 00:11:51,000
 So not only "rebirth," the word is --

206
00:11:51,000 --> 00:11:53,000
 it's actually not "rebirth."

207
00:11:53,000 --> 00:11:57,000
 "Watang patinti."

208
00:11:57,000 --> 00:11:59,000
 "Watang" doesn't mean "rebirth."

209
00:11:59,000 --> 00:12:09,000
 It's "existence," "becoming," I guess.

210
00:12:09,000 --> 00:12:17,000
 So the idea is that --

211
00:12:17,000 --> 00:12:25,000
 just a second.

212
00:12:25,000 --> 00:12:28,000
 Okay, now "wattah" here means "the round."

213
00:12:28,000 --> 00:12:30,000
 So it's the cycle.

214
00:12:30,000 --> 00:12:32,000
 They desire this cycle.

215
00:12:32,000 --> 00:12:33,000
 That's right.

216
00:12:33,000 --> 00:12:37,000
 So the cycle is of birth, old age, sickness and death,

217
00:12:37,000 --> 00:12:40,000
 and nobody desires an end to it.

218
00:12:40,000 --> 00:12:42,000
 They all desire this.

219
00:12:42,000 --> 00:12:44,000
 And that's kind of what you're seeing here.

220
00:12:44,000 --> 00:12:46,000
 It's kind of a cycle.

221
00:12:46,000 --> 00:12:48,000
 Everybody wants the next step.

222
00:12:48,000 --> 00:12:50,000
 So an unmarried woman wants a husband.

223
00:12:50,000 --> 00:12:54,000
 And in fact, it's not only to avoid suffering.

224
00:12:54,000 --> 00:12:58,000
 It's actually -- of course, there's attachment.

225
00:12:58,000 --> 00:13:00,000
 Young women desire husbands.

226
00:13:00,000 --> 00:13:05,000
 It's a thrill for them, the idea of having a husband.

227
00:13:05,000 --> 00:13:08,000
 It's sort of culturally bred into them.

228
00:13:08,000 --> 00:13:12,000
 And young men desire a wife, equally.

229
00:13:12,000 --> 00:13:14,000
 And then when you're married, what's the next step?

230
00:13:14,000 --> 00:13:17,000
 Well, you desire a child, and wouldn't that be wonderful?

231
00:13:17,000 --> 00:13:20,100
 Of course, there are reasons to desire a child beyond just

232
00:13:20,100 --> 00:13:21,000
 the pleasure of it,

233
00:13:21,000 --> 00:13:29,000
 because a woman who is unfruitful is a shame and is barren,

234
00:13:29,000 --> 00:13:33,780
 considered to be useless because that's all they were good

235
00:13:33,780 --> 00:13:36,000
 for in some people's eyes.

236
00:13:36,000 --> 00:13:40,160
 But also, on the other hand, women and men both desire

237
00:13:40,160 --> 00:13:41,000
 children.

238
00:13:41,000 --> 00:13:44,000
 It's something that they look forward to.

239
00:13:44,000 --> 00:13:46,700
 So it's a continuation, the next step, and then the next

240
00:13:46,700 --> 00:13:48,000
 step, and then the next step.

241
00:13:48,000 --> 00:13:52,380
 And finally, once you've squeezed all the pleasure out of

242
00:13:52,380 --> 00:13:54,000
 this life that you can get,

243
00:13:54,000 --> 00:13:57,000
 you want to do it all over again.

244
00:13:57,000 --> 00:14:01,360
 We are like Mahasi Sayadaw says, we're like ducks and

245
00:14:01,360 --> 00:14:06,000
 chickens that squawk and fight and play,

246
00:14:06,000 --> 00:14:09,000
 thinking that they have this long life ahead of them.

247
00:14:09,000 --> 00:14:11,360
 And they don't know, in fact, that they're going to be

248
00:14:11,360 --> 00:14:12,000
 slaughtered.

249
00:14:12,000 --> 00:14:15,660
 At the end of their growth, they're just going to have

250
00:14:15,660 --> 00:14:17,000
 their heads cut off

251
00:14:17,000 --> 00:14:20,000
 and be cooked for someone's dinner.

252
00:14:20,000 --> 00:14:22,000
 But they don't see it.

253
00:14:22,000 --> 00:14:25,000
 As humans, we're like that. It's a similar situation.

254
00:14:25,000 --> 00:14:30,000
 We don't pay attention to the fact that this is,

255
00:14:30,000 --> 00:14:33,000
 our pleasure and our happiness is circumscribed,

256
00:14:33,000 --> 00:14:35,900
 and we're doing nothing to prepare ourselves for the inev

257
00:14:35,900 --> 00:14:38,000
itability of suffering.

258
00:14:38,000 --> 00:14:43,000
 If it's not as we get old, then it's when we die, and if it

259
00:14:43,000 --> 00:14:43,000
's not when we die,

260
00:14:43,000 --> 00:14:45,000
 then it's when we're reborn again.

261
00:14:45,000 --> 00:14:49,000
 And we forget, we forget not only of our past deaths,

262
00:14:49,000 --> 00:14:51,620
 but we forget about in this life all the suffering we've

263
00:14:51,620 --> 00:14:53,000
 gone through from childhood.

264
00:14:53,000 --> 00:14:57,000
 So people want to be born again.

265
00:14:57,000 --> 00:15:00,480
 Most people in modern society don't think about being

266
00:15:00,480 --> 00:15:01,000
 reborn,

267
00:15:01,000 --> 00:15:04,000
 but when they hear about it, it's exciting to them.

268
00:15:04,000 --> 00:15:07,000
 Oh, I can do it all over again. Wouldn't it be great?

269
00:15:07,000 --> 00:15:09,920
 Instead of thinking, oh, that's horrific, I have to do this

270
00:15:09,920 --> 00:15:11,000
 all over again.

271
00:15:11,000 --> 00:15:15,000
 Most, or many if not most people, will think, oh, that's

272
00:15:15,000 --> 00:15:15,000
 great,

273
00:15:15,000 --> 00:15:17,000
 I get to do this all over again.

274
00:15:17,000 --> 00:15:20,460
 Which is funny, because we've gone through so much stress

275
00:15:20,460 --> 00:15:21,000
 and suffering

276
00:15:21,000 --> 00:15:24,970
 for the most part in our lives, but for the most part we've

277
00:15:24,970 --> 00:15:26,000
 forgotten all about it.

278
00:15:26,000 --> 00:15:33,000
 And so we've got this idea, this rosy idea of life,

279
00:15:33,000 --> 00:15:36,380
 that it's all fun and games, that it's all pleasure and

280
00:15:36,380 --> 00:15:37,000
 happiness.

281
00:15:37,000 --> 00:15:41,450
 Because we're not willing, no one wants to dwell on the

282
00:15:41,450 --> 00:15:43,000
 unpleasantness.

283
00:15:43,000 --> 00:15:50,000
 We're unable to see the suffering.

284
00:15:50,000 --> 00:15:52,000
 So this is the general philosophy.

285
00:15:52,000 --> 00:15:56,000
 Now how it has to do with our meditation specifically,

286
00:15:56,000 --> 00:15:58,000
 meditation helps you to see through this.

287
00:15:58,000 --> 00:16:00,000
 It helps you to see the good and the bad.

288
00:16:00,000 --> 00:16:03,000
 It helps you to see the pleasant and the unpleasant.

289
00:16:03,000 --> 00:16:05,000
 And it helps you to see these things objectively.

290
00:16:05,000 --> 00:16:09,000
 It helps you to see that the things that you cling to are

291
00:16:09,000 --> 00:16:11,000
 unstable,

292
00:16:11,000 --> 00:16:13,000
 unsatisfying and uncontrollable.

293
00:16:13,000 --> 00:16:15,000
 That they're not what you think.

294
00:16:15,000 --> 00:16:20,000
 That happiness is not the default state.

295
00:16:20,000 --> 00:16:23,000
 Happiness is something that in fact you have to work for.

296
00:16:23,000 --> 00:16:25,780
 And if you don't work for it, you can eat it up and eat it

297
00:16:25,780 --> 00:16:26,000
 up.

298
00:16:26,000 --> 00:16:29,000
 And it eventually is consumed.

299
00:16:29,000 --> 00:16:31,000
 And all you're left with is the craving for more.

300
00:16:31,000 --> 00:16:33,300
 And you're always going to be subject to this

301
00:16:33,300 --> 00:16:34,000
 disappointment,

302
00:16:34,000 --> 00:16:39,630
 this restlessness, this agitation of not getting what you

303
00:16:39,630 --> 00:16:40,000
 want

304
00:16:40,000 --> 00:16:44,000
 that impels you to seek it out further,

305
00:16:44,000 --> 00:16:48,000
 whether in this life or in being reborn somewhere else.

306
00:16:48,000 --> 00:16:50,000
 It's something we have to work.

307
00:16:50,000 --> 00:16:52,000
 It's something we have to strive for.

308
00:16:52,000 --> 00:16:56,000
 It's something that takes a lot of stress and suffering

309
00:16:56,000 --> 00:16:58,000
 just to get.

310
00:16:58,000 --> 00:17:02,000
 And so this is what we don't see, that we forget.

311
00:17:02,000 --> 00:17:11,010
 Another important point is that our attachment to things

312
00:17:11,010 --> 00:17:13,000
 that we desire is not rational.

313
00:17:13,000 --> 00:17:22,490
 There's no, if you look at it clearly, there's no

314
00:17:22,490 --> 00:17:26,000
 explanation for why we cling to these things.

315
00:17:26,000 --> 00:17:29,000
 A drug addict knows this clearly.

316
00:17:29,000 --> 00:17:31,000
 They're able to see because it's so extreme.

317
00:17:31,000 --> 00:17:34,000
 That it's not rational that they desire these drugs.

318
00:17:34,000 --> 00:17:36,000
 But they still desire them.

319
00:17:36,000 --> 00:17:38,000
 Now a person who's addicted to ordinary things,

320
00:17:38,000 --> 00:17:43,000
 who are addicted to music or food or sexuality or so on,

321
00:17:43,000 --> 00:17:49,110
 is less clear, but still upon examination sees that it all

322
00:17:49,110 --> 00:17:50,000
 falls apart.

323
00:17:50,000 --> 00:17:54,000
 And depending on how strongly they're deluding themselves,

324
00:17:54,000 --> 00:17:58,000
 they can actually acknowledge, at least temporarily,

325
00:17:58,000 --> 00:18:02,370
 that there's no rational reason for obtaining again and

326
00:18:02,370 --> 00:18:06,000
 again and again these activities.

327
00:18:06,000 --> 00:18:08,000
 Like, what is sexuality?

328
00:18:08,000 --> 00:18:12,000
 You're bumping two bags of fluid,

329
00:18:12,000 --> 00:18:20,000
 bags of disgusting fluids and matter together,

330
00:18:20,000 --> 00:18:27,000
 fat and pus and blood and sendus and tendons and so on.

331
00:18:27,000 --> 00:18:30,000
 Flapping them together because it stimulates the chemicals.

332
00:18:30,000 --> 00:18:33,000
 I mean, if you take it apart, it's actually kind of silly.

333
00:18:33,000 --> 00:18:37,000
 Why, you know, delicious food, you're just stimulating

334
00:18:37,000 --> 00:18:39,000
 these chemical reactions.

335
00:18:39,000 --> 00:18:44,000
 You look at the food and it's beautiful and so on.

336
00:18:44,000 --> 00:18:46,000
 Music, you know, what is music?

337
00:18:46,000 --> 00:18:55,000
 It's just this rhythmic pressing on the eardrum, right?

338
00:18:55,000 --> 00:18:58,090
 That creates a sort of a trance in our minds that is

339
00:18:58,090 --> 00:18:59,000
 pleasant.

340
00:18:59,000 --> 00:19:01,000
 I mean, it's all stimulation.

341
00:19:01,000 --> 00:19:03,000
 In the end, it all comes down to that.

342
00:19:03,000 --> 00:19:12,420
 And in the end, all it brings is a cultivation or a

343
00:19:12,420 --> 00:19:17,000
 stimulation of the addiction cycle in the brain,

344
00:19:17,000 --> 00:19:20,430
 which means that using the word "addiction" for all these

345
00:19:20,430 --> 00:19:21,000
 things is proper

346
00:19:21,000 --> 00:19:23,000
 because that's what they are.

347
00:19:23,000 --> 00:19:27,000
 They all come down to some sort of chemical stimulation

348
00:19:27,000 --> 00:19:31,460
 or some kind of stimulation for the brain and hence the

349
00:19:31,460 --> 00:19:32,000
 mind.

350
00:19:32,000 --> 00:19:37,350
 And thereby stimulate, you know, increase the need and the

351
00:19:37,350 --> 00:19:39,000
 desire for these things.

352
00:19:39,000 --> 00:19:43,430
 Now, them being impermanent and unpredictable, they

353
00:19:43,430 --> 00:19:45,000
 therefore can't satisfy.

354
00:19:45,000 --> 00:19:50,000
 They therefore lead to dissatisfaction at times.

355
00:19:50,000 --> 00:19:53,640
 And so this is kind of what Wieszaka is seeing because she

356
00:19:53,640 --> 00:19:55,000
's practiced meditation

357
00:19:55,000 --> 00:19:59,150
 and she practices and she sees that these things are not,

358
00:19:59,150 --> 00:20:01,000
 there's no rational reason.

359
00:20:01,000 --> 00:20:05,000
 These are not true happiness. These are not the true goal.

360
00:20:05,000 --> 00:20:08,000
 True happiness and true peace has to come from letting go.

361
00:20:08,000 --> 00:20:12,000
 It has to come from freeing yourself from any kind of need

362
00:20:12,000 --> 00:20:14,000
 so that you have no wants,

363
00:20:14,000 --> 00:20:17,000
 you have no needs, you're satisfied.

364
00:20:17,000 --> 00:20:20,000
 The only way to be satisfied is to let go.

365
00:20:20,000 --> 00:20:23,360
 It's not to always get what you want because that's of

366
00:20:23,360 --> 00:20:25,000
 course not possible.

367
00:20:25,000 --> 00:20:29,000
 So that's the Dhammapada for this evening.

368
00:20:29,000 --> 00:20:32,000
 Thank you all for tuning in. Wishing you all the best.

