 The previous method is described in paragraph 14.
 Here a metadata does not go by one by one at the time, but
 taking them all as one.
 So any kind of material, whatever, all consist of the full
 primary elements and the material
 is derived from the full primary elements.
 And he like was descends the mind base and a part of the
 mental database as mental.
 Then he defines mentality, materiality and brief thus.
 This mentality and this materiality are called mentality,
 materiality.
 So in this method just all material properties are ruba, I
 mean matter and then the mind
 base and the Dhamma base are called Nama.
 And so this is Nama, this is ruba, something like that.
 Now if the immaterial fails to become evident, this is
 important.
 Now when you practice Vipassana meditation sometimes you
 see the material things clearly,
 I mean in your mind, not with your eyes.
 You see the material things clearly but you don't see yet
 the mentality or mental states
 clearly.
 So if you do not see the mental states clearly, don't try
 to see them clearly, but try to
 see the material things clearly, more clearly.
 So as you are seeing of matter gets better and better then
 you are seeing of mental states
 will become clearer and clearer.
 So this is explained in this paragraph.
 And there is one passage which was quoted by Mahathisth Y
ala in his book on the progress
 of insight.
 So there, "For in proportion as materiality becomes quite
 definite, disentangled and quite
 clear to him, so the immaterial states that have that
 materiality as their object become
 plain of themselves."
 So if you cannot see the mentality clearly, don't try to
 see them clearly, but go back
 to the matter and try to see them, try to see matter
 clearly.
 But when you see matter clearly, then the mental states
 will become clear to you.
 And there is a simile given here.
 Just as when a man with eyes looks for the reflection of
 his face in a dirty looking
 glass and sees no reflection, he does not throw the looking
 glass away because the reflection
 does not appear.
 For the contrary, he polishes it again and again and then
 the reflection becomes plain
 of itself when the looking glass is clean.
 So try to see the matter clearly, more and more clearly.
 And just as when a man needing oil puts sesame flower in a
 basin and wets it with water and
 no oil comes out with only one or two pressing, he does not
 throw the sesame flower away and
 so on.
 It is in the commentary to Risudimaga also, it is said that
 it is sesame flower or sesame
 powder.
 But actually when you want to get oil, you do not crush s
esame seeds into powder and
 then you get oil from there.
 You just put the sesame seeds into a bowl or whatever and
 then try to press it with something.
 You put some hot water and then try to, what do you call
 that?
 Meal?
 When you actually, the grinding, but it's like a pestle and
 you sort of smash it a little
 bit.
 Squeeze, perhaps squeeze.
 Actually, yes, it is squeezed by the pestle.
 But here it is strange that sesame flower is mentioned, but
 it doesn't matter.
 But on the contrary, he wets it again and again with hot
 water and squeezes and presses
 it.
 And then just as when a man wanting to clarify water has
 taken a, it should be kataka, not
 katuka, kataka nut.
 It's your page 685, about 1, 2, 3, 4, 5, 6 or 7 lines.
 The kataka nut and put his hand inside the pot and rub it
 once or twice, the water does
 not come clear.
 He does not throw the kataka nut away.
 On the contrary, he rubs it again and again and as he does
 so, the fine mud subsides and
 the water becomes transparent and clear and so do it, etc.,
 etc.
 Now, kataka is, we cannot translate it in English, but it
 is explained in one Pali dictionary,
 not the PDS, one Pali dictionary, the nut plant, the seed
 of which is used to clear
 water, a kind of nut.
 You use it to clear water.
 And it's, let me call that, botanical name is given in that
 book.
 It is S-T-R-Y-C-H-N-O-S, one word, P-O-T-A-T-O-R-U-M.
 I don't know how to say that.
 Strict-nose potetora or something like that.
 So in a Sanskrit-English dictionary also, it is defined as,
 the same Latin name is given.
 The clearing nut plant, it sits wrapped upon the inside of
 water jars precipitating the
 earthly particles in the water.
 So it is a kind of nut.
 For inproposion, as materiality becomes quite definite, dis
entangled and quite clear to
 him, so the defilements that are opposing him subside, his
 consciousness becomes clear,
 like the water above the precipitated mark and the imm
aterial states that have not, and
 that materiality as the object become plain of themselves
 too.
 So you deal with matter again and again until you see
 matter very clearly.
 And as it becomes clearer and clearer, your seeing of
 mental states will become clearer
 and clearer.
 And then when these mental states become evident to you,
 then they become in three ways, that
 is through contact, through feeling and through
 consciousness.
 In fact, it is also difficult to see these during
 meditation.
 Through contact means, say you dwell on the earth element,
 and then when you concentrate
 on the earth element, that the earth element is the object
 and it is the heavy, here is
 your mind or your consciousness.
 So when you take earth element as object, your mind comes
 into contact with that earth
 element, and that contact becomes evident to you as the
 first conjunction and the first
 meeting, the first meeting with the object.
 Sometimes you may see that during meditation, something
 striking the object or something
 going together with the object.
 And then feeling associated with that, that is the feeling
 aggregate.
 And there is a feeling, whatever object you take as an
 object, and there is awareness
 of the object and along with the awareness of the object,
 there is always a feeling,
 sometimes pleasurable, sometimes painful, sometimes neutral
.
 This also you will see.
 The associated perception as the perception aggregate,
 sometimes you may see perception.
 The associated volition together with the full set contact
 as the formations aggregate.
 And the associated consciousness as the consciousness
 aggregate.
 So sometimes the consciousness or awareness of the object
 is more evident to you.
 Sometimes perception is evident to you, sometimes feeling,
 and sometimes the contact with the
 object becomes evident to you.
 That you see through meditation.
 Likewise one hears the sound and in this way in the head
 hair it is the earth element that
 has the characteristic of hardness and so on and so on.
 So in this way, through contact, through feeling or through
 consciousness, the mental states
 have become evident to the meditator.
 So now paragraph 23, that is important.
 Now it is only when he has become quite sure about disc
erning materiality in this way that
 immaterial states become quite evident to him in the three
 aspects.
 Therefore he should only undertake the task of discerning
 the immaterial states after he
 has completed that.
 That means after he has completed discerning the material
ity and not otherwise.
 If he leaves off discerning materiality when say one or two
 material states have become
 evident in order to begin discerning the immaterial then he
 falls from his meditation subject.
 Like the mountain cow already described under the
 development of Atka Sina.
 But if he undertakes the task of discerning the immaterial
 after he is already quite sure
 about discerning materiality does then his meditation
 subject comes to growth, increase
 and perfection.
 So for those who practice Vipassana meditation alone, it is
 imperative that they try to discern
 matter clearly first and then later on mentality.
 Otherwise as it is said here, he will fall from his
 meditation.
 Now after seeing the mind and matter clearly or after
 defining mentality and materiality
 clearly then one comes to see that there are only mentality
 and materiality as a given
 moment and there is nothing over and above these two mind
 and matter.
 This is the correct view of things.
 So this correct view is described in the following
 paragraphs.
 So there is no being apart from mere mentality and material
ity.
 Only there is mentality and there are mentality and
 materiality at any moment.
 Now when you take breath as an object and you see breath
 clearly and you see also the
 awareness of the breath clearly then you see that there are
 only these two things going
 on at that moment.
 The object which is the breath and the mind which is aware
 of this object.
 Only these two are going on and there is nothing we can
 call a person, a being or a self or
 whatever.
 So this view or this understanding comes only after you see
 mind and matter clearly or you
 have defined mentality and materiality through meditation
 practice.
 I think the rest of this chapter is not difficult to
 understand actually.
 And then the singular is given very easy to understand.
 So as with the assembly of parts the word chariot is a
 countenance.
 So when the aggregates are present a being is such a common
 usage.
 So what we call a being or a person, a man or woman is just
 a common usage.
 Just the convenience of usage or communication.
 But if we analyze it we will find only mind and matter.
 So mind and matter put together and we call man, woman and
 so on.
 Because as the assembly is put together, I mean the
 different parts put together, the
 thing with different parts put together we call a chariot
 or sometimes we call a house,
 we call a fist, a lute, an army, a city, a tree.
 So these are described in Barograph 25, 26, 27, 28.
 So a chariot is nothing but a combination of parts.
 I always use this example saying using car instead of char
iot.
 So if you take the different parts of the car apart and you
 lose the designation car,
 there is no car at all apart from the parts.
 And when you put these parts back again in the correct
 places then it becomes a car.
 So in ultimate analysis there is no car at all but just the
 different parts that are
 put together.
 So in the same way there is no house, there are only walls,
 poles, roofs and so on.
 If they are taken one by one we lose the designation house.
 And also the fist, so when do you, what do you call it?
 You make a fist when you bend your fingers and then there
 is a fist.
 But actually they are just a finger and a fist.
 And then lute with body and strings, let's say a lute or a
 harp.
 So there are strings and there is what you call the body.
 So if we take things apart and body apart then there is no
 lute or no guitar or whatever.
 And then an army, in the olden days an army is said to
 consist of four parts.
 That is elephants, horses, infantry and chariots.
 So these are called four component parts of an army.
 If it is an army there must be these four things.
 And so these four things are called an army.
 But there is no army apart from elephants, horses and so on
.
 And also a city.
 A city surrounding walls, houses and states and we call it
 a city.
 But there is no city apart from these houses and so on.
 And a tree, lute, branch, trunk, leaves and all of these
 put together and we call it tree.
 If they are taken one by one we lose the designation tree.
 So also what we think to be a person, a human being or an
 animal or a man or a woman is
 just the combination of mind and matter.
 So mind depends on matter and matter depends on mind.
 That will be just quite later.
 So they depend on each other and then function as a being,
 as a man, as a woman and so on.
 So this is the correct vision.
 But man and man rejects this correct vision and assumes
 that a permanent being exists.
 He has to conclude either that it comes to be annihilated
 or that it does not.
 So when we take there to be a being or a permanent entity,
 the self or soul, then we have to
 conclude that it is permanent or it is annihilated at death
.
 So there are these two conclusions and we cannot avoid one
 of these two conclusions
 if we do not have correct vision.
 So if he concludes that it does not come to be annihilated,
 he falls into the eternity
 view because it continues to, according to him, it
 continues to exist.
 If he concludes that it does not come to annihilation, he
 falls into the, it does not come to be
 annihilated, he falls into the annihilation view.
 So that it does come to be annihilated, he falls into the
 annihilation view.
 Why?
 Because the assumption precludes any gradual change like
 that of milk into curd.
 It is a part of phrase of the Bali sentence.
 The direct translation of Bali sentence is something like
 this, because of the non-existence
 of some other thing which follows it, which is its result,
 unlike curd which exists following
 the milk.
 Now, the curd is made from milk.
 So when it becomes curd, there is no milk.
 So the milk disappears and the result of milk, which is cur
d, exists.
 But if you take the being to be annihilated as dead, then
 it does not go.
 It is not correct.
 Because a being is the combination of mind and matter and
 among, and say in the mind,
 and there are mental factors, especially the karma there,
 and so long as there is karma,
 the result of karma will appear in the future.
 So there will always be rebirth unless and until all mental
 defilements are eradicated.
 So if you take the being to be annihilated as dead, then it
 is not in accordance with
 this.
 So it is difficult to explain the gradual change like that
 of milk into curd.
 It can be, I think it can be explained against the view of
 permanency too.
 Because if you take things to be permanent, then milk will
 not become curd.
 It will remain milk all the time because you take things to
 be permanent.
 And if you take things to be annihilated as dead or
 whatever, then with the disappearance
 of milk, there will be nothing.
 There will be no curd coming out of milk or as a result of
 milk.
 So you may fall into either one of these extremes.
 So he either holds back concluding that the assumed being
 is eternal or he overreaches,
 concluding that it comes to be annihilated.
 That is the words used in the soda, the holding back and
 overreaching.
 So if you take things to be permanent, it is like holding
 back yourself.
 And if you take things to be annihilated, it means it's
 like overreaching.
 That means going beyond the view that so long as there
 are mental developments and karma, there will always be
 results in the future.
 There will always be rebirths in the future.
 But if you don't accept that, then you are going beyond
 that view.
 And so it is stated here as overreaching.
 And then he quotes the soda.
 He just quotes.
 He is holding back, overreaching, and then neither holding
 back nor overreaching.
 So the end of the soda is.
 And how do those with eyes see?
 Here a vehicle sees what has become has become.
 That means the become here means what those that have
 become.
 And so that means the five aggregates.
 So the monk sees five aggregates, s five aggregates.
 That means they are individual essence and also they are
 common essence.
 Individual essence means taking things one by one and
 trying to see its character and
 their characteristics.
 And common characteristic is common to all phenomena.
 Something like, say, now we are assembled here and each one
 of us is different.
 So say I'm a Bummish, you are an American, there may be
 some others not here, not the
 Vietnamese or whatever.
 So we are different individually.
 But as a human being, we are saying, for all of us as human
 beings.
 So our being human beings is the common characteristic of
 all those here.
 And then I being a Bummish, you being American and so on,
 it's the individual essence or
 something like that.
 So what has become has become means the five aggregates
 according to reality, their characteristic
 of nature and their common characteristic, which actually
 is in permanency and suffering
 and soullessness.
 So when he sees this, he sees that with the fading away or
 with the eradication of mental
 developments, there will be no more rebirth.
 But so long as there are mental developments and so there
 is common, there will always
 be rebirth.
 So this is a correct view.
 And then the paragraph 32 and so on describe the interd
ependence of mentality and materiality.
 So mind depends on matter and matter depends on mind.
 They are not independent of each other.
 One has to depend on other for its existence.
 So it's described in different ways and with different sim
iles.
 And these are very, very good observations of the mind and
 matter being dependent upon
 each other.
 The one is the simile of a blind man and a cripple on page
 691.
 But the bubbles of explaining this meaning they gave this
 simile as an example, a man
 born blind and a stone crawling cripple.
 Stone crawling is the direct translation of the Bali what
 in Bali a cripple is called
 a stone crawling man or woman because since he is a cripple
 he has to walk with the help
 of a bench or a chair or whatever, stone.
 So the blind man said to the cripple, look I can do what
 should be done by legs but I
 have no eyes with which to see what is rough and smooth.
 The cripple said look I can do what should be done by eyes
 but I have no legs with which
 to go and come.
 So the blind man was lighter than he made the cripple climb
 up on his shoulder.
 Sitting on the blind man's shoulder the cripple spoke to us
, leave the left, take the right,
 leave the right, take the left and so on.
 So this is the dependence of mind and matter.
 So mind alone cannot exist in human beings and in lower
 celestial beings also.
 So mind needs a material basis for its existence.
 And material basis alone cannot function because it has no
 cognition, it has no desires and
 so on.
 But when these two come together then they can function as
 a whole.
 So blind man has no efficient power and so on.
 And there are other similes like a marionette or like two
 sheets of reeds put together and
 so on.
 So just as man depend upon a boat for traversing the sea so
 does the metal body need the matter
 body for occurrence.
 And as the boat depends upon the man for traversing the sea
 so does the matter body need the metal
 body for occurrence.
 Depending each upon the other the boat and man go on the
 sea.
 And so do mind and matter boat depend on one upon the other
.
 So first we see mind and matter clearly and then we see
 that there are only mind and matter
 and nothing else.
 And this mind and matter depends upon each other to
 function as a whole.
 So when we see this then we are said to have changed the
 defining of mentality, materiality
 or the limitation of formation of just the purification of
 view or the right correct
 view.
 And Ilhan likes to describe it as like blowing out the
 candle.
 Is that what it is?
 Like mind and matter?
 Where he no longer needs the matter?
 No, no that is different.
 Here it is just a yogi.
 And so we see the seeing mind and matter as they really are
 through practice of Vipassana
 meditation.
 You can read the book and you can sit and you can think
 about this.
 And you can tell other people also that there is only mind
 and matter and mind has the tendency
 and the characteristic of bending towards the object and
 the matter is that which does
 not cognize and so on.
 But here what is meant is not knowledge gained from reading
 or listening to others but knowledge
 gained through your own experience or practice of Vipassana
 meditation.
 So first we try to get stillness of mind or we try to clear
 our mind of hindrances.
 So as the hindrances subside our minds become still and
 tranquil and concentrated.
 So when mind has become concentrated we will not fail to
 see what is going on at the present
 moment.
 Mind matter, mind matter.
 And so when we see that there are only mind and matter and
 we cannot find any other thing
 through experience then we are said to have defined mind
 and matter clearly.
 So after defining or after seeing mind and matter clearly
 we see that there is nothing
 over and above mind and matter.
 No, no, no being and no man, no woman, no self and so on.
 And then we also see that mind and matter depend upon each
 other.
 Mind cannot exist by itself and be functional and matter
 also cannot exist by itself by
 certain function as a whole.
 So mind has to depend on matter and matter has to depend on
 mind.
 So when we have this view we are said to have the pur
ification of view or we are said to
 have reached the state of this purification.
 Okay now the next chapter is on overcoming doubt.
 Now overcoming doubt comes after a meditator sees the cost
 and effect, cost-effect relationship
 between things.
 Now here the knowledge established by overcoming doubt
 about the three divisions of time that
 is past, present and future means deciding the conditions
 of that same mentality, materiality.
 So in order to get to this stage we must first see
 mentality and materiality clearly and
 then we will see or we must find out what are the
 conditions for mind and for matter.
 And when we can see the conditions or causes of mind and
 matter then we will be able to
 overcome doubts about whether we have been in the past or
 what we have been in the past
 something like that.
 Now we will come to those later.
 So this chapter deals with the purification by overcoming
 doubt.
 That means trying to find the causes or conditions of mind
 and matter.
 Here also I think you need a little knowledge of Abhidhamma
 to understand this.
 To begin with he considers that firstly this mentality,
 materiality is not costless because
 if that was so it would follow that having no causes to
 differentiate it, it would be
 identical everywhere always and for all.
 So if mentality and materiality have no cause then it would
 be identical everywhere and
 they will always be the same and for all beings it will be
 the same but it is not the case.
 It has no overlord etc. because of the non-existence of any
 overlord etc. over and above mentality
 and materiality and because if people then argue that
 mentality, materiality itself is
 its overlord etc. then it follows that their mentality,
 materiality which they call the
 overlord etc. would itself be costless.
 Now if they say that mentality, materiality itself is over
lord then their mentality, materiality
 without cause but mentality and materiality are found to be
 with cause.
 So it is not correct to take that mentality, materiality
 itself is an overlord.
 Consequently there must be a cause and a condition for it.
 What are they?
 Having thus directed his attention to mentality, material
ity, cause and condition he first
 dissents the cause and condition for the material body in
 this way.
 When this body is born it is not born inside the blue, red
 or white lotus or water, lily
 etc. or inside the stool of jewels or paws etc.
 On the contrary like a worm in rotting fish, in a rotting
 corpse, in a rotting doe, in
 a drain, in a cesspool etc.
 It is born in between the receptacle for undigested food
 and the receptacle for digested food.
 Thank you for your advance.
 And behind the belly lining in front of the backbone, you
 know what that means?
 Now the real meaning is, what do you call it, fetus right?
 So the fetus lies in the womb with its back to the belly of
 the mother, I mean belly lining
 of the mother and with it and facing the backbone.
 That is what is described here.
 I don't know whether that is really true.
 The fetus in the mother's womb freezes the back of the
 mother.
 Is that?
 You sure?
 You don't know?
 You have to be done with him.
 Okay, it's alright.
 So I think, instead of saying behind the lining, behind the
 belly lining we should say with
 the back to the belly lining.
 Instead of saying in front we should say keeping the
 backbone in front, in front of itself.
 Instead of the bow and the entrails and in a place that is
 stinking, disgusting, repulsive
 and extremely cramped, being itself stinking, disgusting.
 When it is born, thus it causes, root causes are the food
 things namely ignorance, craving,
 clinging and calm and this is the cause.
 Now please do not confuse the word root causes with the
 conditions in the bhajana.
 You know there are 24 causal relations described in the
 chapter on the dependent origination,
 the 24 causal conditions and the first one of them is root
 cause.
 But there root cause means that the six roots, lobadose, am
or, attachment, hatred, delusion
 and then non-attachment, non-hatred and non-delusion.
 But here the Pali word haidu is used here but it is not ha
idu of the bhajana.
 So I think we can dispense with root cause here.
 When it is born, its causes are the food things namely
 ignorance, craving, clinging and calm.
 Now ignorance is one of the roots, right? Craving, one of
 the roots, clinging because
 it is craving, one of the roots but calm is different.
 So one of these four things are the cause for the material
 body to arise.
 Since it is the dead brain about its birth and nutriment is
 its condition since it is
 dead, dead consolidates it.
 So all together we get how many causes?
 Four causes, right? Ignorance, craving, clinging and calm
 and one condition which is nutriment.
 So there are five causes and conditions, four causes and
 one condition.
 So five things constitute its cause and condition and of
 these the three beginning with ignorance
 are the decisive support for this body as the mother is for
 her infant and karma begets
 it as the father does the child and nutriment sustains it
 as the witness does the infant.
 So karma produces the physical body and it produces it with
 the help of body, with the
 support of ignorance, craving and clinging.
 And then after producing it, the nutriment sustains it.
 After discerning the material body's condition in this way,
 he again dissents the mental
 body in the way beginning.
 Due to eye and to visible object, eye consciousness arises.
 Now when seeing consciousness arises, it is not without
 cause or condition.
 So here due to the eye and to visible object, eye
 consciousness arises.
 When we see something there is seeing consciousness in us.
 But if we do not have the eye we will not see and if there
 is nothing to be seen then
 there will be no seeing consciousness.
 So seeing consciousness depends on the eye and the visible
 object for its arising.
 Actually not only these two, there are some more, what are
 they?
 Light also.
 If it is in the dark we do not see anything.
 So light is also a condition.
 And then attention, attention is also a condition.
 Because sometimes even though a cup passes in front of us
 we do not see because we do
 not pay attention.
 So attention also is one of the conditions for the arising
 of seeing consciousness, hearing
 consciousness and so on.
 When he has thus seen that the occurrence of mental
 materiality is due to conditions
 then he sees that it is now so in the first two its
 occurrence was due to conditions and
 in the future to its occurrence will be due to conditions.
 Now the present, the conditions of the present he sees
 directly.
 But of the first and of the future he is in France.
 He does not see directly but here by inference he concludes
 that the past and the future
 must also have conditions.
 When he sees in this way all his uncertainty is abandoned.
 Now there are 16 kinds of doubts or uncertainty described
 in the discourses in many places.
 So they are given here.
 Five with regard to past, five with regard to future and
 six with regard to present.
 Now was I in the past?
 That is one.
 Was I not in the past?
 Number two that is.
 What was I in the past?
 Number three.
 How was I in the past?
 What was I in the past means?
 Was I a king or a Brahmin or an ordinary person or
 something like that?
 And how was I in the past means was I tall, was I short,
 was I black or was I fair or
 something like that?
 Having been what was I in the past?
 These are the five ways of doubting with regard to past
 life or past aggregates.
 Was I in the past, was I not in the past?
 And he is just doubting and he does not come to any
 conclusion.
 If he says I was in the past then there is no doubt.
 But here he was not sure whether he was in the past or he
 was not in the past or what
 was he in the past and so on.
 And similarly with the future.
 Shall I be in the future?
 Shall I not be in the future?
 What shall I be in the future?
 How shall I be in the future?
 Having been what, what shall I be in the future?
 And then six kinds of uncertainty about the present status.
 Am I or am I not?
 What am I?
 How am I?
 It's strange, how am I?
 Am I black or am I white?
 Am I tall or am I short?
 Do you have any thought about your height?
 About your color?
 No.
 So the sub-commodary explains that it is with reference to
 the actor that he doubts.
 How am I?
 I identify my other with myself.
 And how is that other?
 Is that tall or is that short?
 So is that black or is it white or so on?
 So it is not about one's own height or color or whatever,
 but about one's other.
 When all this, the impact comes.
 So from where did I come and where will it be born and
 where am I going?
 So these are the doubts regarding the present.
 So there are 16 kinds of doubts.
 So they are mentioned again and again in the sodas.
 So when you see that there is mind and matter only and mind
 and matter have their own conditions
 for the arising, then these doubts are abandoned or given
 up.
 Another sees the condition for mentality as too full
 according to what is common to all
 and to what is not common to all.
 And that for materiality as too full according to karma and
 so on.
 Now the condition for materiality is too full as that which
 is common to all and that which
 is not common to all.
 So which is common to all means there are six doors
 beginning with the eye and six objects
 beginning with the visible day that are in condition common
 to all.
 That means it is common to all wholesome and unwholesome
 consciousness.
 Whether you have a wholesome consciousness or unwholesome
 consciousness they are always
 there so they are common.
 But attention etc are not common to all.
 For wise attention hearing the good, the bad etc are in
 condition only for the profitable
 that is wholesome while the opposite kinds are in condition
 for the unprofitable.
 Now you find the wise attention and unwise attention as
 condition for wholesome and unwholesome
 thetas.
 Common etc are in condition for the resultant mentality.
 And the life continuum etc are in condition for the
 functional.
 In order to understand this you must understand.
 Thank you.
 1
 1
 1
 1
 1
 1
 1
