1
00:00:00,000 --> 00:00:17,760
 Good evening everyone.

2
00:00:17,760 --> 00:00:39,760
 Today's quote is from Brahmaayu Sutta.

3
00:00:39,760 --> 00:00:53,520
 He once was a brahmin named Brahmaayu living in Mithila.

4
00:00:53,520 --> 00:01:02,520
 Why does that name sound familiar?

5
00:01:02,520 --> 00:01:14,960
 Isn't that the capital?

6
00:01:14,960 --> 00:01:27,460
 That's where, that's the residence of King Janaka in the

7
00:01:27,460 --> 00:01:33,920
 Janaka, Mahajanaka Jataka and

8
00:01:33,920 --> 00:01:45,360
 the Mahaumag Jataka, the Mahosa Jataka.

9
00:01:45,360 --> 00:01:54,040
 See where Mithila was.

10
00:01:54,040 --> 00:01:56,240
 Generally identified with Janaka Pura.

11
00:01:56,240 --> 00:02:07,120
 There is a place called Janaka Pura, that's interesting.

12
00:02:07,120 --> 00:02:21,880
 Probably been through it.

13
00:02:21,880 --> 00:02:27,880
 Janaka Pura.

14
00:02:27,880 --> 00:02:36,400
 We'll have to look into that.

15
00:02:36,400 --> 00:02:41,080
 So Mithila is a famous place.

16
00:02:41,080 --> 00:02:43,400
 Nothing to do with her quote though.

17
00:02:43,400 --> 00:02:46,440
 Still interesting.

18
00:02:46,440 --> 00:03:01,620
 So this Brahmaayu sends his student Uttara to go and see

19
00:03:01,620 --> 00:03:05,640
 the Buddha.

20
00:03:05,640 --> 00:03:09,550
 What's neat about this sutta is it gives some details about

21
00:03:09,550 --> 00:03:11,320
 the Buddha's life, the Buddha's

22
00:03:11,320 --> 00:03:15,520
 manner that really stick with you.

23
00:03:15,520 --> 00:03:19,800
 So it talks about the 32 marks of a great man, a great

24
00:03:19,800 --> 00:03:22,240
 being which are, well they're

25
00:03:22,240 --> 00:03:24,720
 all physical.

26
00:03:24,720 --> 00:03:29,560
 So the Buddha had a specific physical shape.

27
00:03:29,560 --> 00:03:37,820
 He was not like an ordinary human being in many different

28
00:03:37,820 --> 00:03:39,100
 ways.

29
00:03:39,100 --> 00:03:42,780
 But then it goes on, after the 32 marks it gets into

30
00:03:42,780 --> 00:03:44,320
 interesting stuff.

31
00:03:44,320 --> 00:03:47,690
 And so one of them is the quote that we have today, but

32
00:03:47,690 --> 00:03:49,400
 there's several here.

33
00:03:49,400 --> 00:03:53,800
 When he walks, he steps out with the right foot first.

34
00:03:53,800 --> 00:03:57,630
 He does not extend his foot too far or put it down too near

35
00:03:57,630 --> 00:03:57,960
.

36
00:03:57,960 --> 00:04:01,170
 So when we do walking meditation, we always tell the medit

37
00:04:01,170 --> 00:04:02,760
ators to walk with the right

38
00:04:02,760 --> 00:04:06,040
 foot first.

39
00:04:06,040 --> 00:04:09,000
 And maybe this is a reason for it.

40
00:04:09,000 --> 00:04:12,650
 There's obviously nothing special about the right foot,

41
00:04:12,650 --> 00:04:14,600
 although it seems to be a habit

42
00:04:14,600 --> 00:04:15,600
 of the Buddha.

43
00:04:15,600 --> 00:04:20,140
 So every time you walk with the right foot first, you can

44
00:04:20,140 --> 00:04:22,360
 think well, I'm doing it the

45
00:04:22,360 --> 00:04:23,360
 way the Buddha did it.

46
00:04:23,360 --> 00:04:27,520
 Kind of give you some encouragement, make you think of the

47
00:04:27,520 --> 00:04:28,360
 Buddha.

48
00:04:28,360 --> 00:04:33,600
 He walks neither too quickly nor too slowly.

49
00:04:33,600 --> 00:04:37,660
 This is the answer that my teacher gives when someone asks

50
00:04:37,660 --> 00:04:39,920
 whether they should do walking

51
00:04:39,920 --> 00:04:44,080
 meditation quickly or slowly.

52
00:04:44,080 --> 00:04:45,080
 Should I slow down?

53
00:04:45,080 --> 00:04:47,000
 Should I do it quickly?

54
00:04:47,000 --> 00:04:50,160
 Not too quickly, not too slowly.

55
00:04:50,160 --> 00:04:52,440
 He walks without his knees knocking together.

56
00:04:52,440 --> 00:04:55,580
 He walks without his ankles knocking together.

57
00:04:55,580 --> 00:04:58,860
 He walks without raising or lowering his thighs or bringing

58
00:04:58,860 --> 00:05:00,560
 them together or keeping them

59
00:05:00,560 --> 00:05:01,560
 apart.

60
00:05:01,560 --> 00:05:04,880
 He walks only the lower part of his body oscillates.

61
00:05:04,880 --> 00:05:10,160
 And he does not walk with bodily effort.

62
00:05:10,160 --> 00:05:13,940
 When he turns to look, he does so with his whole body.

63
00:05:13,940 --> 00:05:18,040
 This is called the elephant gaze.

64
00:05:18,040 --> 00:05:21,470
 Or it's one way of interpreting what is called the elephant

65
00:05:21,470 --> 00:05:22,000
 gaze.

66
00:05:22,000 --> 00:05:27,440
 The Buddha, he wouldn't often turn, but when he did, he

67
00:05:27,440 --> 00:05:30,520
 would turn his whole body like an

68
00:05:30,520 --> 00:05:31,520
 elephant.

69
00:05:31,520 --> 00:05:35,010
 Because if an elephant is going to look behind itself, it

70
00:05:35,010 --> 00:05:37,120
 has to turn all the way around.

71
00:05:37,120 --> 00:05:42,840
 Another particular idiosyncrasy of a Buddha.

72
00:05:42,840 --> 00:05:47,800
 And so he did this when he came out of Vahisali, I think,

73
00:05:47,800 --> 00:05:51,080
 three months before or sometime before

74
00:05:51,080 --> 00:05:58,880
 he entered into Parinirabhana, he turns around and looks at

75
00:05:58,880 --> 00:05:59,920
 Vahisali and says, "This is the

76
00:05:59,920 --> 00:06:02,680
 last time I'm going to see this town."

77
00:06:02,680 --> 00:06:09,320
 He knew he wasn't ever going to come back.

78
00:06:09,320 --> 00:06:11,840
 He does not look straight up, he does not look straight

79
00:06:11,840 --> 00:06:13,320
 down, he does not walk looking

80
00:06:13,320 --> 00:06:16,440
 about, he looks a plow yoke's length before him.

81
00:06:16,440 --> 00:06:19,520
 Beyond that, he has unhindered knowledge and vision.

82
00:06:19,520 --> 00:06:24,520
 So he looks about six feet out.

83
00:06:24,520 --> 00:06:28,140
 When he goes indoors, he does not raise or lower his body

84
00:06:28,140 --> 00:06:30,080
 or bend it forward or back.

85
00:06:30,080 --> 00:06:34,040
 He turns neither too far from the seat nor too near it.

86
00:06:34,040 --> 00:06:36,550
 He does not lean on the seat with his hand, he does not

87
00:06:36,550 --> 00:06:38,120
 throw his body onto the seat.

88
00:06:38,120 --> 00:06:42,020
 So when he comes near a seat to sit upon, he turns to sit

89
00:06:42,020 --> 00:06:43,880
 on it, but he does it.

90
00:06:43,880 --> 00:06:48,480
 This is a guy having observed the Buddha and relating this

91
00:06:48,480 --> 00:06:50,960
 back to his teacher, just how

92
00:06:50,960 --> 00:06:53,600
 incredible the Buddha was.

93
00:06:53,600 --> 00:06:57,640
 Just how impressive he was in every little meticulous

94
00:06:57,640 --> 00:06:58,480
 detail.

95
00:06:58,480 --> 00:07:00,880
 And so it's a unique look.

96
00:07:00,880 --> 00:07:05,950
 It's I think the only place where we really have this much

97
00:07:05,950 --> 00:07:08,560
 detail about the Buddha.

98
00:07:08,560 --> 00:07:11,910
 When seated indoors, he does not fidget with his hands, he

99
00:07:11,910 --> 00:07:13,840
 does not fidget with his feet.

100
00:07:13,840 --> 00:07:16,850
 He does not sit with his knees crossed, he does not sit

101
00:07:16,850 --> 00:07:18,480
 with his ankles crossed.

102
00:07:18,480 --> 00:07:22,040
 He does not sit with his hands holding his, hand holding

103
00:07:22,040 --> 00:07:22,880
 his chin.

104
00:07:22,880 --> 00:07:26,210
 When seated indoors, he is not afraid, he does not shiver

105
00:07:26,210 --> 00:07:28,280
 and tremble, he is not nervous.

106
00:07:28,280 --> 00:07:31,800
 Being unafraid, not shivering or trembling or nervous.

107
00:07:31,800 --> 00:07:36,920
 His hair does not stand up and he is intent on seclusion.

108
00:07:36,920 --> 00:07:39,930
 Intent on seclusion even when he is in a crowd, when

109
00:07:39,930 --> 00:07:42,480
 surrounded by people, all he is thinking

110
00:07:42,480 --> 00:07:44,100
 about is seclusion.

111
00:07:44,100 --> 00:07:47,880
 His mind is focused on seclusion.

112
00:07:47,880 --> 00:07:51,480
 And part of the implication is getting out of there as soon

113
00:07:51,480 --> 00:07:52,520
 as possible.

114
00:07:52,520 --> 00:07:56,220
 And at one point the Buddha says that he teaches in

115
00:07:56,220 --> 00:07:59,560
 whatever way he can to get rid of, to make

116
00:07:59,560 --> 00:08:05,800
 the person leave as quickly as possible.

117
00:08:05,800 --> 00:08:08,680
 Which is curious because you think well that's kind of how

118
00:08:08,680 --> 00:08:10,400
 rude of him to think like that.

119
00:08:10,400 --> 00:08:19,220
 And it's really kind of special to think of you because you

120
00:08:19,220 --> 00:08:25,880
 can't, you know it's not about

121
00:08:25,880 --> 00:08:29,040
 dismissing people.

122
00:08:29,040 --> 00:08:31,280
 It's about working out your karma with them.

123
00:08:31,280 --> 00:08:33,460
 Someone comes to you, there's some kind of relationship

124
00:08:33,460 --> 00:08:33,880
 there.

125
00:08:33,880 --> 00:08:37,610
 And it's interesting how you find if you reject people, if

126
00:08:37,610 --> 00:08:39,600
 you just reject them outright,

127
00:08:39,600 --> 00:08:41,080
 you're going to come back.

128
00:08:41,080 --> 00:08:43,720
 You're building up karma with them.

129
00:08:43,720 --> 00:08:46,040
 And they will come back, if not in this life and the next.

130
00:08:46,040 --> 00:08:49,340
 But usually in this life they'll come back and there will

131
00:08:49,340 --> 00:08:50,920
 still be that tension.

132
00:08:50,920 --> 00:08:53,650
 But to really get rid of someone, you have to work

133
00:08:53,650 --> 00:08:55,640
 everything out so that you're free

134
00:08:55,640 --> 00:09:01,840
 from that karma, the debt or the concerns of people coming

135
00:09:01,840 --> 00:09:04,440
 to accuse you or come and

136
00:09:04,440 --> 00:09:08,220
 attack you or even come and ask you a question.

137
00:09:08,220 --> 00:09:12,760
 If you don't satisfy them, there's tension there.

138
00:09:12,760 --> 00:09:17,680
 You have to do your part.

139
00:09:17,680 --> 00:09:20,190
 Sometimes if someone's yelling at you, you don't have to

140
00:09:20,190 --> 00:09:21,040
 appease them.

141
00:09:21,040 --> 00:09:24,010
 Sometimes like the Buddha said, you get angry at me, I don

142
00:09:24,010 --> 00:09:25,640
't get angry at you, you get to

143
00:09:25,640 --> 00:09:27,080
 keep the anger.

144
00:09:27,080 --> 00:09:30,800
 Just like you bring me a gift, I don't take the gift.

145
00:09:30,800 --> 00:09:36,050
 But anyways, it's kind of a curious, not how people would

146
00:09:36,050 --> 00:09:38,480
 think of someone like the Buddha.

147
00:09:38,480 --> 00:09:40,820
 They think, "Oh yes, he's so compassionate and he wants to

148
00:09:40,820 --> 00:09:41,600
 help people."

149
00:09:41,600 --> 00:09:43,400
 Not really.

150
00:09:43,400 --> 00:09:52,140
 He would help people in whatever way he can to end it, to

151
00:09:52,140 --> 00:09:56,360
 be free from that burden or

152
00:09:56,360 --> 00:09:58,620
 the interaction.

153
00:09:58,620 --> 00:10:03,960
 Because it's all about working yourself to be free.

154
00:10:03,960 --> 00:10:09,490
 If you want to teach people how to be free, you have to be

155
00:10:09,490 --> 00:10:11,360
 free yourself.

156
00:10:11,360 --> 00:10:14,060
 If you don't put your own freedom first, you're not

157
00:10:14,060 --> 00:10:16,240
 teaching other people to be free, you're

158
00:10:16,240 --> 00:10:20,120
 teaching other people to be trapped.

159
00:10:20,120 --> 00:10:25,160
 You yourself have to be free to be able to teach it.

160
00:10:25,160 --> 00:10:30,040
 Someone who's concerned with seclusion.

161
00:10:30,040 --> 00:10:33,970
 When he receives water, he does not raise, it talks about

162
00:10:33,970 --> 00:10:34,680
 water.

163
00:10:34,680 --> 00:10:36,860
 Like every little thing, he washes the bullet without

164
00:10:36,860 --> 00:10:38,080
 making a splashing noise.

165
00:10:38,080 --> 00:10:40,680
 That's fine, you should read through it.

166
00:10:40,680 --> 00:10:53,520
 It's Majimani Kaia number 91.

167
00:10:53,520 --> 00:10:58,910
 When he receives rice, he does not exceed the right amount

168
00:10:58,910 --> 00:11:01,480
 of sauce in the mouthful.

169
00:11:01,480 --> 00:11:05,570
 He turns the mouthful over two or three times in his mouth

170
00:11:05,570 --> 00:11:07,280
 and then swallows it.

171
00:11:07,280 --> 00:11:11,920
 No rice kernel enters his body unchewed and no rice kernel

172
00:11:11,920 --> 00:11:13,920
 remains in his mouth.

173
00:11:13,920 --> 00:11:18,790
 Every single rice kernel is chewed before it goes down his

174
00:11:18,790 --> 00:11:19,800
 throat.

175
00:11:19,800 --> 00:11:22,270
 No rice kernel remains in his mouth and then he takes

176
00:11:22,270 --> 00:11:23,360
 another mouthful.

177
00:11:23,360 --> 00:11:27,080
 Not even a single rice kernel is left before he goes for

178
00:11:27,080 --> 00:11:28,640
 the next mouthful.

179
00:11:28,640 --> 00:11:31,190
 He takes his food experiencing the taste, though not

180
00:11:31,190 --> 00:11:33,040
 experiencing greed for the taste.

181
00:11:33,040 --> 00:11:34,840
 I wonder how this guy knew all this.

182
00:11:34,840 --> 00:11:38,930
 He must have been very observant or perhaps this has been

183
00:11:38,930 --> 00:11:41,440
 embellished or extrapolated.

184
00:11:41,440 --> 00:11:44,120
 The food he takes has eight factors.

185
00:11:44,120 --> 00:11:47,860
 It is neither for amusement nor for intoxication, nor for

186
00:11:47,860 --> 00:11:50,920
 the sake of physical beauty and attractiveness,

187
00:11:50,920 --> 00:11:53,910
 but only for the endurance and continuation of his body,

188
00:11:53,910 --> 00:11:55,360
 for the ending of discomfort,

189
00:11:55,360 --> 00:11:58,120
 for the assisting the holy life.

190
00:11:58,120 --> 00:12:01,140
 He considers thus shall I terminate old feelings without

191
00:12:01,140 --> 00:12:02,800
 arousing new feelings and I shall

192
00:12:02,800 --> 00:12:05,020
 be healthy and blameless and live in comfort.

193
00:12:05,020 --> 00:12:09,360
 This is actually the reflection that we make before we eat.

194
00:12:09,360 --> 00:12:11,380
 And Bhikkhu Bodhi says the same.

195
00:12:11,380 --> 00:12:14,480
 This is the standard reflection of the proper use of Aum's

196
00:12:14,480 --> 00:12:15,000
 food.

197
00:12:15,000 --> 00:12:22,000
 [Hindi]

198
00:12:22,000 --> 00:12:29,000
 [Hindi]

199
00:12:29,000 --> 00:12:36,000
 [Hindi]

200
00:12:36,000 --> 00:12:42,000
 [Hindi]

201
00:12:42,000 --> 00:12:48,000
 [Hindi]

202
00:12:48,000 --> 00:12:55,000
 [Hindi]

203
00:12:55,000 --> 00:13:02,000
 [Hindi]

204
00:13:02,000 --> 00:13:09,000
 [Hindi]

205
00:13:09,000 --> 00:13:16,000
 [Hindi]

206
00:13:16,000 --> 00:13:22,000
 [Hindi]

207
00:13:22,000 --> 00:13:29,000
 [Hindi]

208
00:13:29,000 --> 00:13:36,000
 [Hindi]

209
00:13:36,000 --> 00:13:46,000
 [Hindi]

210
00:13:46,000 --> 00:13:53,000
 [Hindi]

211
00:13:53,000 --> 00:14:00,000
 [Hindi]

212
00:14:00,000 --> 00:14:06,000
 [Hindi]

213
00:14:06,000 --> 00:14:13,000
 [Hindi]

214
00:14:13,000 --> 00:14:20,000
 [Hindi]

215
00:14:20,000 --> 00:14:27,000
 [Hindi]

216
00:14:27,000 --> 00:14:33,000
 [Hindi]

217
00:14:33,000 --> 00:14:40,000
 [Hindi]

218
00:14:40,000 --> 00:14:47,000
 [Hindi]

219
00:14:47,000 --> 00:14:53,000
 [Hindi]

220
00:14:53,000 --> 00:14:57,000
 That's an inspiring quote.

221
00:14:57,000 --> 00:15:03,000
 The sutta has a lot about inspiration rather than

222
00:15:03,000 --> 00:15:06,000
 instruction.

223
00:15:06,000 --> 00:15:10,920
 A lot of it is about the Buddha himself and about his great

224
00:15:10,920 --> 00:15:12,000
 qualities

225
00:15:12,000 --> 00:15:15,000
 and how impressive he was.

226
00:15:15,000 --> 00:15:23,540
 As sort of an aid to help us think and appreciate who the

227
00:15:23,540 --> 00:15:25,000
 Buddha actually was.

228
00:15:25,000 --> 00:15:28,000
 It's useful in that way.

229
00:15:28,000 --> 00:15:30,000
 It's impressive.

230
00:15:30,000 --> 00:15:35,000
 Having an impressive role model example,

231
00:15:35,000 --> 00:15:42,000
 there's a definite benefit to it.

232
00:15:42,000 --> 00:15:46,000
 That's what we use sutta like this for.

233
00:15:46,000 --> 00:15:50,000
 On the other hand, in the end,

234
00:15:50,000 --> 00:15:54,000
 the Buddha does come and teach some things.

235
00:15:54,000 --> 00:15:59,000
 What does he actually get around to teaching?

236
00:15:59,000 --> 00:16:02,000
 Here we go. He asks the Buddha some questions.

237
00:16:02,000 --> 00:16:06,040
 How does one become a Brahmin and how does one attain

238
00:16:06,040 --> 00:16:08,000
 knowledge?

239
00:16:08,000 --> 00:16:12,000
 Let's see if we can find the Pali.

240
00:16:12,000 --> 00:16:18,000
 [Hindi]

241
00:16:18,000 --> 00:16:25,000
 [Hindi]

242
00:16:25,000 --> 00:16:32,000
 [Hindi]

243
00:16:32,000 --> 00:16:38,000
 [Hindi]

244
00:16:38,000 --> 00:16:44,000
 A lot of questions.

245
00:16:44,000 --> 00:16:47,000
 Even in the Buddhist time, they had all sorts of questions.

246
00:16:47,000 --> 00:16:49,000
 How does one become a Brahmin?

247
00:16:49,000 --> 00:16:54,000
 How does one become Vaidagu, one who has gone to knowledge,

248
00:16:54,000 --> 00:16:55,000
 wisdom?

249
00:16:55,000 --> 00:17:03,000
 How does one become triple knowledge? How is one called a

250
00:17:03,000 --> 00:17:03,000
 Muni, a sage?

251
00:17:03,000 --> 00:17:06,390
 How does one become an Arahant? How does one attain comple

252
00:17:06,390 --> 00:17:07,000
teness?

253
00:17:07,000 --> 00:17:10,000
 What's that?

254
00:17:10,000 --> 00:17:15,000
 [Hindi]

255
00:17:15,000 --> 00:17:19,000
 Full accomplishment.

256
00:17:19,000 --> 00:17:23,000
 How is one a silent sage?

257
00:17:23,000 --> 00:17:26,000
 [Hindi]

258
00:17:26,000 --> 00:17:32,550
 The first one wasn't Muni, it was Soti, which is a learned

259
00:17:32,550 --> 00:17:34,000
 man.

260
00:17:34,000 --> 00:17:40,000
 Muni is a...

261
00:17:40,000 --> 00:17:45,000
 How is one a Muni? How is one called a Buddha?

262
00:17:45,000 --> 00:17:49,000
 Here is the Buddha's teaching.

263
00:17:49,000 --> 00:17:54,000
 Let's find the teaching.

264
00:17:54,000 --> 00:18:00,000
 [Hindi]

265
00:18:00,000 --> 00:18:06,000
 [Hindi]

266
00:18:06,000 --> 00:18:12,000
 One who knows their past lives, who sees Heaven and Hell,

267
00:18:12,000 --> 00:18:18,000
 who has attained the ending of birth.

268
00:18:18,000 --> 00:18:20,000
 One is Moseito.

269
00:18:20,000 --> 00:18:23,780
 One who has accomplished in higher knowledge Abhinya, such

270
00:18:23,780 --> 00:18:26,000
 a person is a Muni.

271
00:18:26,000 --> 00:18:33,000
 [Hindi]

272
00:18:33,000 --> 00:18:39,000
 They know the purification of mind, or the pure mind.

273
00:18:39,000 --> 00:18:42,000
 They know the pure mind.

274
00:18:42,000 --> 00:18:52,000
 Muthangrage, Sabaso, free from Raga entirely, altogether.

275
00:18:52,000 --> 00:18:57,000
 All together free from Raga, from passion and desire.

276
00:18:57,000 --> 00:19:04,000
 Bahi najati marano brahma caari caariyasa ke wavi

277
00:19:04,000 --> 00:19:13,160
 One is complete, who has abandoned birth and death through

278
00:19:13,160 --> 00:19:15,000
 the holy life.

279
00:19:15,000 --> 00:19:19,000
 Is that through the holy life?

280
00:19:19,000 --> 00:19:22,930
 One who has abandoned birth and death, one who has

281
00:19:22,930 --> 00:19:25,000
 completed the holy life.

282
00:19:25,000 --> 00:19:27,000
 Become complete.

283
00:19:27,000 --> 00:19:30,000
 Come to the completion of the holy life.

284
00:19:30,000 --> 00:19:35,000
 Paragu sabadhamanam buddha-dipu-jati

285
00:19:35,000 --> 00:19:39,630
 One who has gone to the farther shore, to the final

286
00:19:39,630 --> 00:19:42,000
 ultimate knowledge.

287
00:19:42,000 --> 00:19:46,000
 Paragu means literally one who has gone to the other shore.

288
00:19:46,000 --> 00:19:50,000
 But it's aware the buddhi is often in a metaphorical sense

289
00:19:50,000 --> 00:19:54,000
 of one who has gone to the end,

290
00:19:54,000 --> 00:19:57,000
 who has seen through to completion.

291
00:19:57,000 --> 00:20:02,390
 Sabadhamanam, who has come to the end of all dhammas, or

292
00:20:02,390 --> 00:20:08,040
 who has come to know and come to be complete in regards to

293
00:20:08,040 --> 00:20:09,000
 all dhammas.

294
00:20:09,000 --> 00:20:11,000
 Budhota-dipu-jati

295
00:20:11,000 --> 00:20:22,580
 Then, with us, with dasdhi, such a person is called the

296
00:20:22,580 --> 00:20:24,000
 Buddha.

297
00:20:24,000 --> 00:20:31,000
 And then the Buddha gave him more instruction, which is

298
00:20:31,000 --> 00:20:35,000
 to go anupubing katang katasi.

299
00:20:35,000 --> 00:20:39,000
 Then he gave the anupubikata, which we call the anupubikata

300
00:20:39,000 --> 00:20:41,000
, the teaching in order.

301
00:20:41,000 --> 00:20:46,000
 So he taught him bodhana, about charity, seal of morality,

302
00:20:46,000 --> 00:20:50,000
 sanga, about heaven, kama-nanga-dhinavang.

303
00:20:50,000 --> 00:20:57,000
 And the adhinavang-ukarang-sankhire-sankhi,

304
00:20:57,000 --> 00:21:03,790
 disadvantages, the degradation and the defilement, impurity

305
00:21:03,790 --> 00:21:07,000
 of sensuality.

306
00:21:07,000 --> 00:21:16,000
 Nika-me-anisang-sang, the benefits of renunciation.

307
00:21:16,000 --> 00:21:20,530
 The menjim-lika-ya is really pretty awesome. It's got 152 s

308
00:21:20,530 --> 00:21:23,000
uttas on all sorts of subjects.

309
00:21:23,000 --> 00:21:30,000
 So you find all sorts of gems about all sorts of things.

310
00:21:30,000 --> 00:21:36,000
 Anyway, there's some dhamma for this evening.

311
00:21:36,000 --> 00:21:44,010
 So we have two meditators, and they both finished today.

312
00:21:44,010 --> 00:21:46,000
 They did really well.

313
00:21:46,000 --> 00:21:50,000
 I can certify both of them as having finished the course,

314
00:21:50,000 --> 00:21:51,000
 which is awesome.

315
00:21:51,000 --> 00:21:55,580
 Not certified, but I can vouch for them having done a

316
00:21:55,580 --> 00:22:04,000
 really good job in their course.

317
00:22:04,000 --> 00:22:08,420
 And I finished two exams. I wrote about Buddhism this

318
00:22:08,420 --> 00:22:12,000
 morning, Buddhism in East Asia, the three questions.

319
00:22:12,000 --> 00:22:15,410
 The neat thing about it, our professor, I think he's a

320
00:22:15,410 --> 00:22:17,000
 practicing Buddhist.

321
00:22:17,000 --> 00:22:22,320
 And he was conscious of the amount of stress that people

322
00:22:22,320 --> 00:22:28,000
 have, so he gave us the exam in advance.

323
00:22:28,000 --> 00:22:31,140
 He did the same with the midterm, but he gave us the final

324
00:22:31,140 --> 00:22:33,000
 exam to prepare in advance.

325
00:22:33,000 --> 00:22:39,920
 So it was fairly stress-free for most of the class as far

326
00:22:39,920 --> 00:22:42,000
 as worrying about what's going to be on it,

327
00:22:42,000 --> 00:22:44,280
 or about any surprises, or about what to study. It was

328
00:22:44,280 --> 00:22:46,000
 quite clear what you had to study.

329
00:22:46,000 --> 00:22:50,000
 It's just a matter of putting in the time.

330
00:22:50,000 --> 00:22:56,000
 And so I wrote about practice and belief.

331
00:22:56,000 --> 00:23:00,860
 The first section was a short answer, so it was a shorter

332
00:23:00,860 --> 00:23:02,000
 answer.

333
00:23:02,000 --> 00:23:06,000
 And I wrote an essay about practice versus belief.

334
00:23:06,000 --> 00:23:17,060
 Because in East Asia, in most religions, of course, the

335
00:23:17,060 --> 00:23:20,140
 most obvious relationship between belief and practice is

336
00:23:20,140 --> 00:23:22,000
 how our belief informs our practice.

337
00:23:22,000 --> 00:23:25,000
 That's right. This is fairly standard.

338
00:23:25,000 --> 00:23:29,320
 But I looked at the other ways as well, or I mentioned how

339
00:23:29,320 --> 00:23:34,000
 it's possible for one's practice to influence one's beliefs

340
00:23:34,000 --> 00:23:37,980
 in the sense that one can be forced into certain practices

341
00:23:37,980 --> 00:23:42,000
 or have influences leading one to practice in other ways.

342
00:23:42,000 --> 00:23:48,130
 For example, the state in China, in Korea, in Japan had a

343
00:23:48,130 --> 00:23:54,530
 pretty heavy hand on Buddhist practitioners and monks and

344
00:23:54,530 --> 00:23:57,000
 teachers and so on.

345
00:23:57,000 --> 00:24:04,890
 And so they were often compelled to practice in certain

346
00:24:04,890 --> 00:24:09,000
 ways, and this would have influenced people's beliefs.

347
00:24:09,000 --> 00:24:15,220
 Also, worldly practices, or people's desires and

348
00:24:15,220 --> 00:24:20,330
 attachments and views and egos and so on, would have

349
00:24:20,330 --> 00:24:23,680
 affected people's belief and how they interpreted the Dham

350
00:24:23,680 --> 00:24:24,000
ma.

351
00:24:24,000 --> 00:24:28,760
 Another way of looking at these two is how belief and

352
00:24:28,760 --> 00:24:32,000
 practice can sometimes conflict.

353
00:24:32,000 --> 00:24:36,630
 We've got this interesting thing. Pure Land Buddhism is

354
00:24:36,630 --> 00:24:41,000
 really weird. I'm not really impressed.

355
00:24:41,000 --> 00:24:46,040
 But here's the idea with Pure Land. See, there was this guy

356
00:24:46,040 --> 00:24:52,270
, Genshin, who said that reciting the Buddha's name, and in

357
00:24:52,270 --> 00:24:56,550
 fact not our Buddha, but another Buddha, was the best

358
00:24:56,550 --> 00:24:58,000
 practice.

359
00:24:58,000 --> 00:25:02,830
 So he said, "There's all these other practices, but rec

360
00:25:02,830 --> 00:25:07,000
iting the name Amitabha is the best practice."

361
00:25:07,000 --> 00:25:13,120
 So this is his idea. The next guy who came along was named

362
00:25:13,120 --> 00:25:18,000
 Honan. And Honan said, "Honan privately."

363
00:25:18,000 --> 00:25:23,190
 He didn't go public with it in the beginning, but he wrote

364
00:25:23,190 --> 00:25:28,690
 a book in private that said that practice is useless, and

365
00:25:28,690 --> 00:25:35,000
 the only thing that's effective is reciting the Buddha's

366
00:25:35,000 --> 00:25:36,000
 name.

367
00:25:36,000 --> 00:25:39,000
 So he's gone another step.

368
00:25:39,000 --> 00:25:45,110
 The next guy who came along, Shinran, went even farther. If

369
00:25:45,110 --> 00:25:48,300
 you read about these guys, it's like, "Oh dear, they went

370
00:25:48,300 --> 00:25:50,000
 further and further."

371
00:25:50,000 --> 00:25:52,580
 It was like interpretation of interpretation. So Shinran

372
00:25:52,580 --> 00:25:58,360
 says, "Not only is practice is not just useless, practice

373
00:25:58,360 --> 00:26:01,000
 is actually harmful."

374
00:26:01,000 --> 00:26:21,000
 Why is practice harmful? His quote was that a good person

375
00:26:21,000 --> 00:26:23,000
 will get even a good person can get into...

376
00:26:23,000 --> 00:26:27,030
 No, that's not the quote. But it twisted it. He said, "It

377
00:26:27,030 --> 00:26:30,760
 was basically saying that evil people have an easier time

378
00:26:30,760 --> 00:26:34,000
 getting to the pure land than good people."

379
00:26:34,000 --> 00:26:40,120
 And why is that? Because good people practice Buddhism, and

380
00:26:40,120 --> 00:26:43,240
 by practicing Buddhism, they're showing that they don't

381
00:26:43,240 --> 00:26:45,000
 have faith in Amitabha.

382
00:26:45,000 --> 00:26:48,480
 Because otherwise, why would you practice? If you had

383
00:26:48,480 --> 00:26:51,550
 perfect faith in Amitabha, you're just like, "Well, wait

384
00:26:51,550 --> 00:26:54,490
 for Amitabha to save me. Amitabha will take me to the pure

385
00:26:54,490 --> 00:26:55,000
 land."

386
00:26:55,000 --> 00:27:01,450
 So his idea was that evil people, because they don't

387
00:27:01,450 --> 00:27:06,000
 practice, have really scary stuff actually.

388
00:27:06,000 --> 00:27:10,530
 This is a quote of his, and it's really not Buddhism, as I

389
00:27:10,530 --> 00:27:15,000
 understand it. So we've had some debates about this.

390
00:27:15,000 --> 00:27:18,480
 Right before the exam, I said to one of the students, I

391
00:27:18,480 --> 00:27:24,000
 said, "You know, this really isn't Buddhism anymore."

392
00:27:24,000 --> 00:27:27,290
 And she kind of smiled, and I said, "I know. What I have to

393
00:27:27,290 --> 00:27:30,000
 say is it's a different type of Buddhism."

394
00:27:30,000 --> 00:27:34,730
 And she kind of smiled again, because we understand that it

395
00:27:34,730 --> 00:27:38,000
's like the no-true Scotsman fallacy.

396
00:27:38,000 --> 00:27:40,830
 If you ever look up the no-true Scotsman fallacy, you can't

397
00:27:40,830 --> 00:27:43,440
 just say someone's not a Buddhist if they claim to be Buddh

398
00:27:43,440 --> 00:27:44,000
ists.

399
00:27:44,000 --> 00:27:48,250
 But I said, "But here's the thing. Buddhism, if a religion

400
00:27:48,250 --> 00:27:55,000
 arises, and when it arises, its core doctrine is X.

401
00:27:55,000 --> 00:28:02,930
 And then later, a new doctrine arises, not X. If the new

402
00:28:02,930 --> 00:28:09,380
 doctrine takes not X, how can you call it the same religion

403
00:28:09,380 --> 00:28:10,000
?"

404
00:28:10,000 --> 00:28:14,370
 It's like, okay, you can adapt and reinterpret things, but

405
00:28:14,370 --> 00:28:19,000
 if something is X, and then another, you know, this is very

406
00:28:19,000 --> 00:28:22,000
 much not X, pure land Buddhism.

407
00:28:22,000 --> 00:28:26,000
 Anyway, so I had to talk about these kinds of things.

408
00:28:26,000 --> 00:28:32,430
 The second, the two longer questions, two longer essays

409
00:28:32,430 --> 00:28:39,000
 were about decline and the individual versus the state.

410
00:28:39,000 --> 00:28:44,600
 So decline is, I mean, again, is much more related to East

411
00:28:44,600 --> 00:28:49,410
 Asian Buddhism than Buddhism as we practice it, or I

412
00:28:49,410 --> 00:28:51,000
 practice it.

413
00:28:51,000 --> 00:29:00,000
 So again, it's kind of scholarly, as opposed to practical.

414
00:29:00,000 --> 00:29:06,470
 Anyway, I talked about decline and how decline, the decline

415
00:29:06,470 --> 00:29:10,170
 of the Dhamma is interesting to ask whether the Dhamma is

416
00:29:10,170 --> 00:29:14,000
 actually considered to be declining,

417
00:29:14,000 --> 00:29:19,540
 or more understood to be the case of people's ability to

418
00:29:19,540 --> 00:29:22,000
 practice the Dhamma.

419
00:29:22,000 --> 00:29:24,000
 The Dhamma doesn't change.

420
00:29:24,000 --> 00:29:35,370
 But one exception is the fact that the Dhamma becomes

421
00:29:35,370 --> 00:29:38,000
 corrupt.

422
00:29:38,000 --> 00:29:40,810
 You see, because we don't really know exactly what the

423
00:29:40,810 --> 00:29:43,690
 Buddha taught. We claim that in Theravada we have the

424
00:29:43,690 --> 00:29:45,000
 original teachings.

425
00:29:45,000 --> 00:29:47,000
 You know, we may not.

426
00:29:47,000 --> 00:29:50,870
 But at any rate, supposing even that we did, well, you know

427
00:29:50,870 --> 00:29:55,040
, even those teachings are reinterpreted and are mixed with

428
00:29:55,040 --> 00:29:59,550
 new teachings and replaced by new teachings and that kind

429
00:29:59,550 --> 00:30:01,000
 of thing.

430
00:30:01,000 --> 00:30:05,000
 So this is how the Dhamma becomes corrupted.

431
00:30:05,000 --> 00:30:08,080
 And the decline of the Dhamma is when people stop paying

432
00:30:08,080 --> 00:30:13,430
 attention to the deep teachings and they promote surface

433
00:30:13,430 --> 00:30:17,000
 teachings like, I would say, like the Lotus Sutra.

434
00:30:17,000 --> 00:30:20,410
 Or even worse, there are sutras like the Sutra of Humane

435
00:30:20,410 --> 00:30:24,000
 Kings, the Golden Light Sutra that are very, very worldly

436
00:30:24,000 --> 00:30:29,000
 and much more about empire building and state building and

437
00:30:29,000 --> 00:30:29,000
 anything,

438
00:30:29,000 --> 00:30:33,000
 which is what I got into the second, the final question,

439
00:30:33,000 --> 00:30:36,000
 about individual versus state.

440
00:30:36,000 --> 00:30:42,080
 Individuals tend to affect religion in unpredictable ways,

441
00:30:42,080 --> 00:30:44,000
 idiosyncratic.

442
00:30:44,000 --> 00:30:50,130
 They come up with their own ideas and they create

443
00:30:50,130 --> 00:30:52,000
 innovation.

444
00:30:52,000 --> 00:30:56,270
 Whereas the state, on the other hand, is all about stifling

445
00:30:56,270 --> 00:31:01,700
 and innovation, systematizing, controlling, manipulating,

446
00:31:01,700 --> 00:31:05,000
 using for their own purposes.

447
00:31:05,000 --> 00:31:09,000
 So they tend to be more predictable.

448
00:31:09,000 --> 00:31:16,000
 Anyway, so that was my second exam.

449
00:31:16,000 --> 00:31:19,340
 Yeah, it did really well on both exams. Neither one was

450
00:31:19,340 --> 00:31:21,000
 that difficult.

451
00:31:21,000 --> 00:31:25,000
 As it turns out.

452
00:31:25,000 --> 00:31:29,640
 On Sunday we had this storytelling thing. I don't think I

453
00:31:29,640 --> 00:31:31,000
've talked about that yet.

454
00:31:31,000 --> 00:31:34,550
 It went really well. I told the story. I think I went way

455
00:31:34,550 --> 00:31:37,000
 over time. I think we all kind of went way over time.

456
00:31:37,000 --> 00:31:39,990
 My story was quite short, so I thought, "Oh, I'll have to

457
00:31:39,990 --> 00:31:43,000
 talk about it." And I think I ended up talking too long.

458
00:31:43,000 --> 00:31:45,000
 But it was really well appreciated.

459
00:31:45,000 --> 00:31:50,860
 The First Nations guy said good words. I would have killed

460
00:31:50,860 --> 00:32:02,000
 him and sat back down. He was quite a character.

461
00:32:02,000 --> 00:32:05,490
 And tomorrow I'm off to New York. So I actually should

462
00:32:05,490 --> 00:32:09,000
 probably go and start preparing for my trip.

463
00:32:09,000 --> 00:32:16,090
 But I'll be gone for nine days. I would think I'll probably

464
00:32:16,090 --> 00:32:18,200
 do, as long as they have internet, I'll do some audio

465
00:32:18,200 --> 00:32:19,000
 broadcasts.

466
00:32:19,000 --> 00:32:21,570
 So I'll try my best, depending on the time. I think my

467
00:32:21,570 --> 00:32:24,000
 schedule is mostly afternoon stuff.

468
00:32:24,000 --> 00:32:28,000
 I'm not even kind of taking it day by day, of course.

469
00:32:28,000 --> 00:32:32,080
 But I should look at the New York schedule. As long as I'm

470
00:32:32,080 --> 00:32:37,000
 not busy, I'll try to broadcast every night. Just audio.

471
00:32:37,000 --> 00:32:42,290
 So you can find the audio here at meditation.sarimangalow.

472
00:32:42,290 --> 00:32:43,000
org.

473
00:32:43,000 --> 00:32:48,000
 Anyway, thanks for tuning in everyone. Have a good night.

474
00:32:48,000 --> 00:32:51,000
 And be well.

475
00:32:51,000 --> 00:33:07,000
 [no audio]

