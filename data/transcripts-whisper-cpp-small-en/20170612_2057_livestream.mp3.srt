1
00:00:00,000 --> 00:00:13,780
 Welcome to our evening Dharma session.

2
00:00:13,780 --> 00:00:22,060
 Good evening everyone.

3
00:00:22,060 --> 00:00:31,630
 I often get asked by meditators both here and online what

4
00:00:31,630 --> 00:00:35,980
 more they can do.

5
00:00:35,980 --> 00:00:42,030
 Yeah I'm being mindful but it's not really working or it's

6
00:00:42,030 --> 00:00:43,240
 not really quick enough for

7
00:00:43,240 --> 00:00:54,080
 my liking.

8
00:00:54,080 --> 00:00:56,060
 What more can I do?

9
00:00:56,060 --> 00:01:01,620
 It's kind of a greedy mind state really.

10
00:01:01,620 --> 00:01:08,150
 I mean the whole idea of being able to do something is

11
00:01:08,150 --> 00:01:10,060
 dangerous.

12
00:01:10,060 --> 00:01:14,540
 Once you start to think you're going to fix things.

13
00:01:14,540 --> 00:01:17,480
 There's some trick, I mean much more common as meditators

14
00:01:17,480 --> 00:01:18,860
 will come and tell me about

15
00:01:18,860 --> 00:01:29,220
 all the tricks that they've tried to fix their problems.

16
00:01:29,220 --> 00:01:39,850
 Having little ways to make it easier or make the practice

17
00:01:39,850 --> 00:01:43,220
 more profitable.

18
00:01:43,220 --> 00:01:47,140
 So you have to be careful of that.

19
00:01:47,140 --> 00:01:49,710
 Otherwise you get into the idea that you're in charge, that

20
00:01:49,710 --> 00:01:50,740
 you're in control.

21
00:01:50,740 --> 00:01:55,420
 And in fact meditation is about letting go of control.

22
00:01:55,420 --> 00:01:57,580
 Stop trying to control.

23
00:01:57,580 --> 00:02:03,270
 Stop having anything to do with your experiences except

24
00:02:03,270 --> 00:02:05,620
 experiencing them.

25
00:02:05,620 --> 00:02:14,300
 Learn how to be present here now, pure and clear.

26
00:02:14,300 --> 00:02:19,000
 That being said there are tricks.

27
00:02:19,000 --> 00:02:31,610
 tricks that are ratified, you know it's a word, that are

28
00:02:31,610 --> 00:02:38,580
 spoken of well by the Buddha.

29
00:02:38,580 --> 00:02:41,820
 For example there are other meditations we can practice.

30
00:02:41,820 --> 00:02:44,500
 I've talked about those.

31
00:02:44,500 --> 00:02:50,910
 You can practice loving kindness if you're feeling really

32
00:02:50,910 --> 00:02:52,020
 angry.

33
00:02:52,020 --> 00:02:55,320
 And sit there and say yes I have a bad relationship with

34
00:02:55,320 --> 00:02:58,260
 this person or that person but let's

35
00:02:58,260 --> 00:03:04,740
 put that behind us may they be well.

36
00:03:04,740 --> 00:03:07,650
 And practice mindfulness of the Buddha so people sometimes

37
00:03:07,650 --> 00:03:09,060
 will do chanting or bow down

38
00:03:09,060 --> 00:03:13,690
 before the Buddha and make a wish in front of the Buddha

39
00:03:13,690 --> 00:03:16,900
 may I follow in his footsteps.

40
00:03:16,900 --> 00:03:20,100
 May I be guided by his teaching.

41
00:03:20,100 --> 00:03:25,420
 May I always be surrounded by his followers.

42
00:03:25,420 --> 00:03:47,120
 Another thing we can do is the Buddha gave a list of daily

43
00:03:47,120 --> 00:03:51,820
 reflections what he called

44
00:03:51,820 --> 00:03:56,060
 topics for frequent reflection.

45
00:03:56,060 --> 00:04:02,690
 Abhikanang, Abhinang which should be things that should be

46
00:04:02,690 --> 00:04:05,980
 reflected upon regularly.

47
00:04:05,980 --> 00:04:10,600
 So I mean the most important is for us to be mindful for us

48
00:04:10,600 --> 00:04:12,820
 to practice mindfulness

49
00:04:12,820 --> 00:04:15,820
 meditation.

50
00:04:15,820 --> 00:04:22,140
 It doesn't mean there aren't other important things that

51
00:04:22,140 --> 00:04:24,060
 help us along.

52
00:04:24,060 --> 00:04:29,440
 Things that we remind ourselves of these facts of life

53
00:04:29,440 --> 00:04:30,900
 regularly.

54
00:04:30,900 --> 00:04:33,460
 They keep us on track.

55
00:04:33,460 --> 00:04:38,580
 There's a way of many things that keep you fenced in.

56
00:04:38,580 --> 00:04:42,630
 Morality is one for example keeping the five precepts just

57
00:04:42,630 --> 00:04:44,780
 reciting the five precepts gives

58
00:04:44,780 --> 00:04:51,300
 you some framework within which to work.

59
00:04:51,300 --> 00:04:54,940
 But the Buddha gave five themes for reflection.

60
00:04:54,940 --> 00:04:59,500
 They're fairly simple but worth knowing.

61
00:04:59,500 --> 00:05:02,420
 It's not that it's going to be a deep teaching it's just

62
00:05:02,420 --> 00:05:04,420
 going to be an instruction hey

63
00:05:04,420 --> 00:05:07,980
 reflect on these every day.

64
00:05:07,980 --> 00:05:11,280
 The first one is and it doesn't matter this isn't just for

65
00:05:11,280 --> 00:05:13,360
 this one the Buddha specifically

66
00:05:13,360 --> 00:05:18,300
 says it's not just for monks this is for anybody.

67
00:05:18,300 --> 00:05:24,180
 Everybody should reflect frequently.

68
00:05:24,180 --> 00:05:35,820
 I am subject to old age I am not exempt from old age.

69
00:05:35,820 --> 00:05:41,630
 Number two I am subject to illness I am not exempt from

70
00:05:41,630 --> 00:05:43,020
 illness.

71
00:05:43,020 --> 00:05:48,490
 Number three I am subject to death I am not exempt from

72
00:05:48,490 --> 00:05:49,540
 death.

73
00:05:49,540 --> 00:05:52,170
 Number four I must be parted and separated from everyone

74
00:05:52,170 --> 00:05:53,900
 and everything dear and agreeable

75
00:05:53,900 --> 00:05:56,940
 to me.

76
00:05:56,940 --> 00:06:00,780
 And number five I am the owner of my kamma the heir of my k

77
00:06:00,780 --> 00:06:01,460
amma.

78
00:06:01,460 --> 00:06:05,600
 I have kamma as my origin kamma as my relative kamma as my

79
00:06:05,600 --> 00:06:06,460
 resort.

80
00:06:06,460 --> 00:06:20,620
 I will be the heir of whatever kamma good or bad that I do.

81
00:06:20,620 --> 00:06:34,420
 So let's go through these fairly simple they're related.

82
00:06:34,420 --> 00:06:39,140
 And we clamor for the things that we want thinking we're

83
00:06:39,140 --> 00:06:48,940
 feeling really good to be young.

84
00:06:48,940 --> 00:06:51,820
 Spend some time every day thinking about the fact that you

85
00:06:51,820 --> 00:06:53,700
're going to get old puts everything

86
00:06:53,700 --> 00:06:54,700
 in perspective.

87
00:06:54,700 --> 00:06:58,030
 You ask yourself what is important are all these things

88
00:06:58,030 --> 00:06:59,660
 really useful is this really

89
00:06:59,660 --> 00:07:01,580
 beneficial.

90
00:07:01,580 --> 00:07:07,330
 Why am I working so hard why am I striving to become you

91
00:07:07,330 --> 00:07:10,860
 know people put away money and

92
00:07:10,860 --> 00:07:16,780
 then for their retirement and then they die before they can

93
00:07:16,780 --> 00:07:19,300
 use it or they're too old

94
00:07:19,300 --> 00:07:23,020
 to enjoy it.

95
00:07:23,020 --> 00:07:34,350
 It's not necessarily true but you waste your life thinking

96
00:07:34,350 --> 00:07:37,020
 about so many useless things.

97
00:07:37,020 --> 00:07:46,260
 We're not going to be young forever old age comes to us we

98
00:07:46,260 --> 00:07:50,660
 get we lose our faculties we

99
00:07:50,660 --> 00:07:57,540
 lose our youth reflect on that regularly.

100
00:07:57,540 --> 00:08:03,300
 Number two we're going to be we're subject to sickness.

101
00:08:03,300 --> 00:08:23,180
 The human body is a field like the well fertilized field

102
00:08:23,180 --> 00:08:29,740
 just waiting for the seeds to be planted.

103
00:08:29,740 --> 00:08:35,690
 The body is a receptacle of sickness so our sickness

104
00:08:35,690 --> 00:08:39,660
 sickness rests it's a nest of nest

105
00:08:39,660 --> 00:08:49,020
 for the for the growth and accumulation of illness.

106
00:08:49,020 --> 00:08:56,100
 We get intoxicated by health very happy to be healthy.

107
00:08:56,100 --> 00:08:59,550
 And consider the fact that you might get very sick probably

108
00:08:59,550 --> 00:09:01,180
 get very sick throughout your

109
00:09:01,180 --> 00:09:04,700
 life.

110
00:09:04,700 --> 00:09:08,420
 Consider that this health is only temporary.

111
00:09:08,420 --> 00:09:11,060
 Doesn't mean it's bad to be healthy right we talked about

112
00:09:11,060 --> 00:09:12,420
 this doesn't mean there's it's

113
00:09:12,420 --> 00:09:15,100
 not good to be healthy it's quite good to be healthy.

114
00:09:15,100 --> 00:09:19,570
 The fact of the matter is our health is uncertain and the

115
00:09:19,570 --> 00:09:22,500
 sickness is certain we're going to

116
00:09:22,500 --> 00:09:31,060
 get sick.

117
00:09:31,060 --> 00:09:34,090
 And then you ask yourself what how meaningful are all these

118
00:09:34,090 --> 00:09:35,700
 things that I cling to if that's

119
00:09:35,700 --> 00:09:43,060
 the case.

120
00:09:43,060 --> 00:09:48,900
 Quite a useful way of reducing our desires for meaningless

121
00:09:48,900 --> 00:09:50,020
 things.

122
00:09:50,020 --> 00:09:56,010
 We start to see there must be something there are things

123
00:09:56,010 --> 00:09:59,100
 that are more important.

124
00:09:59,100 --> 00:10:04,420
 The solution to life is to be go beyond sense pleasure.

125
00:10:04,420 --> 00:10:11,660
 The sense pleasure can't help you when you're sick.

126
00:10:11,660 --> 00:10:14,900
 Number three we're all going to die.

127
00:10:14,900 --> 00:10:21,140
 I'm going to die.

128
00:10:21,140 --> 00:10:25,340
 It isn't a morbid thing to think about sickness or death.

129
00:10:25,340 --> 00:10:28,430
 It's really quite calming and puts everything in

130
00:10:28,430 --> 00:10:29,540
 perspective.

131
00:10:29,540 --> 00:10:36,060
 Takes us out of it stops all this conflict the Buddha said

132
00:10:36,060 --> 00:10:38,340
 you know people people picker

133
00:10:38,340 --> 00:10:42,020
 and fight as though they're going to live forever.

134
00:10:42,020 --> 00:10:44,820
 If only they would stop and realize that we're all going to

135
00:10:44,820 --> 00:10:46,340
 be dead in a hundred years why

136
00:10:46,340 --> 00:10:50,620
 are we what are we fighting about.

137
00:10:50,620 --> 00:10:52,300
 What does it all mean.

138
00:10:52,300 --> 00:10:53,300
 What's the meaning of it.

139
00:10:53,300 --> 00:10:55,300
 There is no meaning.

140
00:10:55,300 --> 00:11:01,150
 Put everything in perspective and it really makes you chill

141
00:11:01,150 --> 00:11:03,660
 out when you stop fussing

142
00:11:03,660 --> 00:11:07,150
 about everything stop chasing after things that can't

143
00:11:07,150 --> 00:11:09,100
 satisfy you things you can't take

144
00:11:09,100 --> 00:11:15,660
 with you.

145
00:11:15,660 --> 00:11:19,260
 Because number four we're going to be parted and separated

146
00:11:19,260 --> 00:11:21,060
 from everyone and everything

147
00:11:21,060 --> 00:11:38,780
 that we hold dear and agreeable.

148
00:11:38,780 --> 00:11:49,200
 We used to chant this at one monastery I was at a couple of

149
00:11:49,200 --> 00:11:51,180
 places we've chanted this.

150
00:11:51,180 --> 00:11:59,340
 Every day we would chant and then translate it into Thai.

151
00:11:59,340 --> 00:12:04,500
 We hold on to all these things that we can't take with us.

152
00:12:04,500 --> 00:12:11,320
 What are these things to us worrying about people worrying

153
00:12:11,320 --> 00:12:14,020
 about possessions.

154
00:12:14,020 --> 00:12:18,610
 The problem is we try to cling to it's like we try to cling

155
00:12:18,610 --> 00:12:21,060
 to the water in the river.

156
00:12:21,060 --> 00:12:35,180
 We're trying to cling to things that are flowing by.

157
00:12:35,180 --> 00:12:44,620
 Try to hold on to a stable snapshot of reality.

158
00:12:44,620 --> 00:12:48,130
 Reality is not like that reality is a movie constantly

159
00:12:48,130 --> 00:12:50,440
 moving moving changing changing

160
00:12:50,440 --> 00:13:00,340
 and if you don't keep up you get swept away.

161
00:13:00,340 --> 00:13:04,110
 When you reflect on this that we're going to have to lose

162
00:13:04,110 --> 00:13:06,100
 everything you start to see

163
00:13:06,100 --> 00:13:11,220
 how dangerous and unreasonable it is to cling to things.

164
00:13:11,220 --> 00:13:17,790
 cling to anything anything that you hold dear it's not a

165
00:13:17,790 --> 00:13:20,940
 source of strength or happiness

166
00:13:20,940 --> 00:13:30,290
 it's a strength of weakness and stress source of weakness

167
00:13:30,290 --> 00:13:32,100
 and stress.

168
00:13:32,100 --> 00:13:38,770
 And number five what do we really own what do we really

169
00:13:38,770 --> 00:13:42,260
 have as a possession our deeds

170
00:13:42,260 --> 00:13:43,780
 are good and bad deeds.

171
00:13:43,780 --> 00:13:48,510
 You ask yourself what are you going to carry with you what

172
00:13:48,510 --> 00:13:51,460
 is truly yours what truly belongs

173
00:13:51,460 --> 00:13:57,670
 to you the only answer is our good and our bad deeds we

174
00:13:57,670 --> 00:14:00,540
 carry them with us.

175
00:14:00,540 --> 00:14:05,130
 We never let we can never let go of them you can't sell

176
00:14:05,130 --> 00:14:08,020
 them you can't give them away you

177
00:14:08,020 --> 00:14:10,620
 can't shake them.

178
00:14:10,620 --> 00:14:14,650
 On the positive side if they're good no one can steal them

179
00:14:14,650 --> 00:14:16,780
 no one can take them away from

180
00:14:16,780 --> 00:14:19,500
 you.

181
00:14:19,500 --> 00:14:22,990
 People can take away pretty much everything including your

182
00:14:22,990 --> 00:14:24,620
 life but they can't take away

183
00:14:24,620 --> 00:14:29,140
 your good and bad deeds these things that make us who we

184
00:14:29,140 --> 00:14:31,380
 are and give us our sense of

185
00:14:31,380 --> 00:14:36,500
 worth our sense of happiness and contentment.

186
00:14:36,500 --> 00:14:42,030
 A person who is intent upon evil as evil as their

187
00:14:42,030 --> 00:14:46,060
 possession they carry it with them in

188
00:14:46,060 --> 00:14:50,310
 their mind and they carry it with them as to who they are

189
00:14:50,310 --> 00:14:52,540
 an evil person a person with

190
00:14:52,540 --> 00:15:01,250
 an evil mind a mind that is habitually unpleasant and

191
00:15:01,250 --> 00:15:06,540
 displeased cruel mean and so on.

192
00:15:06,540 --> 00:15:13,590
 Abhinhang bajuwe kitabah bajuwe kitabang these things

193
00:15:13,590 --> 00:15:18,060
 should be frequently reflected upon

194
00:15:18,060 --> 00:15:23,810
 it's easy to remember old age sickness death loss of

195
00:15:23,810 --> 00:15:27,780
 everything you hold dear and or the

196
00:15:27,780 --> 00:15:32,710
 fact that the things that you hold dear are not don't

197
00:15:32,710 --> 00:15:35,820
 belong to you and that your deeds

198
00:15:35,820 --> 00:15:42,860
 good and bad deeds are something that do belong to you.

199
00:15:42,860 --> 00:15:49,890
 So this isn't helpful for your meditation directly but a

200
00:15:49,890 --> 00:15:53,460
 real important reason for giving

201
00:15:53,460 --> 00:15:57,600
 these talks is to inspire you to continue meditating and I

202
00:15:57,600 --> 00:16:00,020
 think this is a really inspiring

203
00:16:00,020 --> 00:16:04,370
 teaching not the teaching itself maybe but the actual

204
00:16:04,370 --> 00:16:07,500
 practice if you put this into practice

205
00:16:07,500 --> 00:16:10,950
 reflecting on all five of these the Buddha goes into quite

206
00:16:10,950 --> 00:16:12,700
 detail and it's really neat

207
00:16:12,700 --> 00:16:19,820
 what he says he says as one often reflects on on this theme

208
00:16:19,820 --> 00:16:23,180
 the path is generated that's

209
00:16:23,180 --> 00:16:36,580
 the English it's not very poetic see if I can find the Pali

210
00:16:36,580 --> 00:16:36,580
.

211
00:16:36,580 --> 00:16:49,700
 The path is born the path is the path arises meaning they

212
00:16:49,700 --> 00:16:53,100
 they start to see the right way

213
00:16:53,100 --> 00:16:55,990
 they start to set themselves right and that it's really a

214
00:16:55,990 --> 00:16:57,780
 neat little saying because that's

215
00:16:57,780 --> 00:17:02,560
 how it feels when you wake up to this hey I'm gonna die oh

216
00:17:02,560 --> 00:17:04,940
 you realign your your your

217
00:17:04,940 --> 00:17:08,690
 priorities right suddenly those things that you thought

218
00:17:08,690 --> 00:17:10,860
 were so important no longer seem

219
00:17:10,860 --> 00:17:15,380
 so important old age is inevitable sickness is inevitable

220
00:17:15,380 --> 00:17:18,460
 death it's not pessimistic to

221
00:17:18,460 --> 00:17:23,620
 think about them it's a reasonable concern and the better

222
00:17:23,620 --> 00:17:26,100
 you deal with it now so that

223
00:17:26,100 --> 00:17:35,180
 you're you're okay with it in the end you're ready for it

224
00:17:35,180 --> 00:17:36,060
 because so many of us are not

225
00:17:36,060 --> 00:17:39,860
 ready for it I mean that's the thing you don't have to

226
00:17:39,860 --> 00:17:42,540
 think about the future but if you

227
00:17:42,540 --> 00:17:47,510
 do think about when you die most of us are frightened of it

228
00:17:47,510 --> 00:17:49,900
 are afraid of it and so it's

229
00:17:49,900 --> 00:17:52,720
 a good test if you think about these things and it doesn't

230
00:17:52,720 --> 00:17:54,180
 bother you and you think I'm

231
00:17:54,180 --> 00:17:59,800
 ready for all that and you're good to go and until then we

232
00:17:59,800 --> 00:18:02,620
 remind ourselves of them and

233
00:18:02,620 --> 00:18:05,940
 they help realign us they get us on the right path so they

234
00:18:05,940 --> 00:18:07,700
 say hey these things are not

235
00:18:07,700 --> 00:18:12,850
 so important the only thing that's important is good deans

236
00:18:12,850 --> 00:18:15,340
 being full of goodness both

237
00:18:15,340 --> 00:18:20,500
 in the mind and in the body good deans good speech good

238
00:18:20,500 --> 00:18:23,660
 thoughts a clear mind a pure mind

239
00:18:23,660 --> 00:18:32,260
 free mind so there you go five things to reflect upon that

240
00:18:32,260 --> 00:18:34,180
's the dhamma for tonight thank you

241
00:18:34,180 --> 00:18:34,900
 all for tuning in

242
00:18:34,900 --> 00:18:59,220
 a

243
00:18:59,220 --> 00:19:08,380
 bunch of questions here unclear about the dhammas category

244
00:19:08,380 --> 00:19:09,980
 please advise I've talked

245
00:19:09,980 --> 00:19:14,110
 about this I'm kind of unclear about it myself admittedly

246
00:19:14,110 --> 00:19:19,340
 but I think of dhammas as the sorts

247
00:19:19,340 --> 00:19:22,880
 of things that a meditation teacher would advise a

248
00:19:22,880 --> 00:19:25,380
 meditation student so dhamma here

249
00:19:25,380 --> 00:19:29,670
 I think of as the teachings of the Buddha really at the

250
00:19:29,670 --> 00:19:32,540
 same time as being sets of reality

251
00:19:32,540 --> 00:19:36,120
 but it's not all of them it's just the ones that are

252
00:19:36,120 --> 00:19:39,100
 important for meditators so the first

253
00:19:39,100 --> 00:19:42,340
 one is the hindrances and I would take just take it as an

254
00:19:42,340 --> 00:19:44,220
 expanded part of the practice

255
00:19:44,220 --> 00:19:46,820
 I mean another thing that you have to understand is this s

256
00:19:46,820 --> 00:19:48,660
utta was given to a specific audience

257
00:19:48,660 --> 00:19:51,790
 or it was also given to an audience was given with the

258
00:19:51,790 --> 00:19:54,420
 audience in mind so as they're listening

259
00:19:54,420 --> 00:19:58,490
 to it and even as we read it or as we hear it when you get

260
00:19:58,490 --> 00:20:00,620
 to the dhammas by that time

261
00:20:00,620 --> 00:20:06,010
 you've already started to set yourself on the practice and

262
00:20:06,010 --> 00:20:08,500
 so by the time you get there

263
00:20:08,500 --> 00:20:11,980
 all of these things are useful as an addition to that you

264
00:20:11,980 --> 00:20:13,940
 know if you're if you're quick

265
00:20:13,940 --> 00:20:17,080
 I mean this would have been taught to people who probably

266
00:20:17,080 --> 00:20:18,900
 had fairly advanced faculties

267
00:20:18,900 --> 00:20:21,380
 so I mean it's interesting that dhamma isn't one thing it's

268
00:20:21,380 --> 00:20:22,660
 many different things so it

269
00:20:22,660 --> 00:20:30,710
 appears to have been or I would guess it's sort of a

270
00:20:30,710 --> 00:20:33,180
 progression you know you teach them

271
00:20:33,180 --> 00:20:35,490
 in that order I mean it certainly works if you study them

272
00:20:35,490 --> 00:20:38,860
 in the order that they're given

273
00:20:38,860 --> 00:20:48,530
 they make a good progression or progressive practice for

274
00:20:48,530 --> 00:20:49,460
 beginners we just start with

275
00:20:49,460 --> 00:20:56,820
 the five hindrances and the six senses tanha pa jayupa dana

276
00:20:56,820 --> 00:20:59,620
 whose upa dana here mainly refer

277
00:20:59,620 --> 00:21:04,200
 to kamupa dana does include all four types how do you

278
00:21:04,200 --> 00:21:07,460
 explain dityupa dana being caused

279
00:21:07,460 --> 00:21:11,160
 by craving I don't like these questions what does that

280
00:21:11,160 --> 00:21:12,980
 question does the answer to that

281
00:21:12,980 --> 00:21:17,190
 question really help you maybe it does it's too tough a

282
00:21:17,190 --> 00:21:19,660
 question I want easy questions

283
00:21:19,660 --> 00:21:27,170
 easy questions only I don't know I don't think I'm the one

284
00:21:27,170 --> 00:21:29,540
 to answer that my my first guess

285
00:21:29,540 --> 00:21:38,350
 is that it probably is only kamupa dana that's not even

286
00:21:38,350 --> 00:21:43,860
 true dityupa dana I think that the

287
00:21:43,860 --> 00:21:47,980
 different ways of talking about upa dana cannot be always

288
00:21:47,980 --> 00:21:54,300
 used in the same context but it's

289
00:21:54,300 --> 00:22:00,700
 an interesting question reality non-reality has spoken of

290
00:22:00,700 --> 00:22:03,180
 constructs can you please explain

291
00:22:03,180 --> 00:22:07,480
 what they are construct well it's just a word but generally

292
00:22:07,480 --> 00:22:09,540
 we use that word to refer to

293
00:22:09,540 --> 00:22:14,790
 things that arise because anything that arises has

294
00:22:14,790 --> 00:22:18,620
 conditions that cause it to arise and

295
00:22:18,620 --> 00:22:21,020
 without those conditions it doesn't arise so in a sense it

296
00:22:21,020 --> 00:22:23,300
's constructed or it's a construct

297
00:22:23,300 --> 00:22:28,120
 sankara is the word we use formation or construct although

298
00:22:28,120 --> 00:22:30,980
 I have used the word in a different

299
00:22:30,980 --> 00:22:35,810
 way like a mental construct is a concept so you can create

300
00:22:35,810 --> 00:22:38,140
 something in your mind the

301
00:22:38,140 --> 00:22:43,350
 thing itself is is not real the thing is something you've

302
00:22:43,350 --> 00:22:45,940
 imagined in your mind like a rabbit

303
00:22:45,940 --> 00:22:49,270
 with horns say you can think of it in your mind but that

304
00:22:49,270 --> 00:22:51,420
 horned rabbit doesn't actually

305
00:22:51,420 --> 00:22:57,430
 exist it's just a thought in your picture in your mind you

306
00:22:57,430 --> 00:23:02,540
 know so I could be used in

307
00:23:02,540 --> 00:23:19,240
 different ways I suppose don't use that word that often I

308
00:23:19,240 --> 00:23:21,580
 hope when I meditate I become

309
00:23:21,580 --> 00:23:23,610
 happy in the moment don't take my overbearing

310
00:23:23,610 --> 00:23:25,860
 responsibility seriously and follow a secular

311
00:23:25,860 --> 00:23:29,930
 path of normal people when I stop meditating I want to ord

312
00:23:29,930 --> 00:23:32,180
ain how would I find the middle

313
00:23:32,180 --> 00:23:36,940
 ground between these two also with a person's extreme sense

314
00:23:36,940 --> 00:23:39,300
 of responsibility would you

315
00:23:39,300 --> 00:23:43,440
 say a person's extreme sense of responsibility is an over

316
00:23:43,440 --> 00:23:45,940
compensation or something such as

317
00:23:45,940 --> 00:23:52,480
 guilt I don't really quite understand what you're asking

318
00:23:52,480 --> 00:23:57,940
 you're happy overbearing responsibility

319
00:23:57,940 --> 00:24:03,430
 is a secular path but when you stop meditating you want to

320
00:24:03,430 --> 00:24:05,180
 ordain I don't know I'm just going

321
00:24:05,180 --> 00:24:07,940
 to say if you want my advice keep meditating keep being

322
00:24:07,940 --> 00:24:09,820
 mindful and be mindful of all these

323
00:24:09,820 --> 00:24:14,030
 things you'll find the answers for yourself it's much more

324
00:24:14,030 --> 00:24:16,260
 simple maybe than you're making

325
00:24:16,260 --> 00:24:22,700
 it we saw us at Paraman yati mean that the trustworthy

326
00:24:22,700 --> 00:24:26,980
 person is the ultimate relative

327
00:24:26,980 --> 00:24:33,750
 familiar to us but not really trustworthy intimate is the

328
00:24:33,750 --> 00:24:36,780
 word I meant not familiar

329
00:24:36,780 --> 00:24:40,510
 someone who is intimate and so as a result as a result you

330
00:24:40,510 --> 00:24:42,420
 trust where you trust them

331
00:24:42,420 --> 00:24:45,650
 someone who but it's used in a sense of being familiar in

332
00:24:45,650 --> 00:24:47,560
 the sense that you know that they

333
00:24:47,560 --> 00:24:52,950
 wouldn't mind it's not it's about trusting each other it's

334
00:24:52,950 --> 00:24:55,660
 not about being trustworthy

335
00:24:55,660 --> 00:24:58,380
 it's about trusting each other that word does mean

336
00:24:58,380 --> 00:25:00,460
 trustworthy I think it's used in that

337
00:25:00,460 --> 00:25:07,350
 sense but I don't think that's directly what the word

338
00:25:07,350 --> 00:25:10,220
 literally means when you trust each

339
00:25:10,220 --> 00:25:15,970
 other so you see you can someone who is trusted or intimate

340
00:25:15,970 --> 00:25:18,980
 so in a sense familiar that's all

341
00:25:18,980 --> 00:25:21,790
 I meant by that yeah you're right it's probably not the

342
00:25:21,790 --> 00:25:23,780
 best word I could use intimate might

343
00:25:23,780 --> 00:25:29,070
 be better but not even intimate it's yeah people you can

344
00:25:29,070 --> 00:25:31,700
 trust I guess people who you

345
00:25:31,700 --> 00:25:48,740
 trust let go of our names are they considered attachment

346
00:25:48,740 --> 00:25:52,840
 names are just concepts and they're useful tools that allow

347
00:25:52,840 --> 00:25:54,380
 us to refer to things just

348
00:25:54,380 --> 00:26:00,660
 don't get attached to it and get attached to them I think

349
00:26:00,660 --> 00:26:02,420
 you a mantras disturbs my breath

350
00:26:02,420 --> 00:26:06,730
 does it it feels less smoothly really when I focus on the

351
00:26:06,730 --> 00:26:08,900
 stomach rising without the

352
00:26:08,900 --> 00:26:12,310
 mantras is more fluent and pleasant hmm is the mantra

353
00:26:12,310 --> 00:26:14,460
 supposed to do that or am I doing

354
00:26:14,460 --> 00:26:17,650
 it somehow wrong no you're doing it right that's the mantra

355
00:26:17,650 --> 00:26:19,060
 doesn't do that it's when

356
00:26:19,060 --> 00:26:31,940
 you stop controlling and when you stop fixing it that that

357
00:26:31,940 --> 00:26:38,740
 it starts to be unpleasant you

358
00:26:38,740 --> 00:26:42,950
 get out of that groove that the comfortable groove of

359
00:26:42,950 --> 00:26:45,700
 enjoying it right because you can

360
00:26:45,700 --> 00:26:50,480
 create pleasurable sensations in the in the mind if you

361
00:26:50,480 --> 00:26:53,260
 want but all they're all you're

362
00:26:53,260 --> 00:26:56,200
 doing is creating more and more attachment what the mantra

363
00:26:56,200 --> 00:26:57,540
 does is it forces you to see

364
00:26:57,540 --> 00:27:03,290
 things without clinging to them without attaching to them

365
00:27:03,290 --> 00:27:06,780
 as a result that it doesn't feed our

366
00:27:06,780 --> 00:27:12,760
 addiction that doesn't feed our desire for pleasure our

367
00:27:12,760 --> 00:27:16,140
 desire for for tranquility and

368
00:27:16,140 --> 00:27:20,600
 so that's uncomfortable the mantra is meant to take you out

369
00:27:20,600 --> 00:27:23,340
 of your comfort zone and straighten

370
00:27:23,340 --> 00:27:29,750
 your mind so that instead of being attached to the pleasant

371
00:27:29,750 --> 00:27:32,900
 feeling you're objective and

372
00:27:32,900 --> 00:27:38,670
 flexible and able to experience things however they may be

373
00:27:38,670 --> 00:27:41,700
 it helps you see impermanence

374
00:27:41,700 --> 00:27:44,760
 and what you're seeing is impermanent suffering and non-

375
00:27:44,760 --> 00:27:46,860
self especially non-self because you

376
00:27:46,860 --> 00:28:01,710
 can't control you want it to be smooth and it's not smooth

377
00:28:01,710 --> 00:28:05,100
 suffering how can we make

378
00:28:05,100 --> 00:28:08,730
 a positive difference trying to help people with compassion

379
00:28:08,730 --> 00:28:10,540
 wisdom visiting a temple soon

380
00:28:10,540 --> 00:28:13,970
 and I have no idea how to properly offer food at this

381
00:28:13,970 --> 00:28:16,820
 ceremony to the monks is there a specific

382
00:28:16,820 --> 00:28:23,520
 way yeah I mean I can't speak for those monks but monks in

383
00:28:23,520 --> 00:28:27,020
 general are pretty laid back about

384
00:28:27,020 --> 00:28:31,670
 it they're just happy that people are giving them food I

385
00:28:31,670 --> 00:28:34,820
 mean otherwise they die so I wouldn't

386
00:28:34,820 --> 00:28:38,090
 worry too much about that you might get some lay people

387
00:28:38,090 --> 00:28:39,900
 trying to tell you how to do it

388
00:28:39,900 --> 00:28:44,910
 or even the monks maybe get caught up in this way or that

389
00:28:44,910 --> 00:28:47,460
 way but in the texts it says that

390
00:28:47,460 --> 00:28:53,010
 even if an a novice were to be sitting on the shoulders of

391
00:28:53,010 --> 00:28:55,820
 a monk and and drops a lump of

392
00:28:55,820 --> 00:29:01,640
 rice in his bowl it's considered properly offered making

393
00:29:01,640 --> 00:29:04,860
 the point that yeah it's it's much more

394
00:29:04,860 --> 00:29:08,660
 about making sure that it's given I mean we take as a rule

395
00:29:08,660 --> 00:29:11,020
 to only eat food if it's given to us

396
00:29:11,020 --> 00:29:13,910
 and that keeps us honest we don't ever go looking for food

397
00:29:13,910 --> 00:29:16,460
 or wanting for food the only way we can

398
00:29:16,460 --> 00:29:20,860
 possibly stay alive is if people want us to stay alive and

399
00:29:20,860 --> 00:29:23,100
 the only only food that we can

400
00:29:23,100 --> 00:29:25,910
 eat is food that is blamelessly given people wanted to give

401
00:29:25,910 --> 00:29:27,620
 it they were happy to support us

402
00:29:27,620 --> 00:29:34,110
 it's not coercion coerced or or bought by some favor or by

403
00:29:34,110 --> 00:29:37,940
 some exchange it's given because

404
00:29:37,940 --> 00:29:41,740
 someone genuinely wanted you to have food that keeps you

405
00:29:41,740 --> 00:29:43,860
 quite honest and quite humble

406
00:29:43,860 --> 00:29:50,650
 as far as making a positive difference I mean if you want

407
00:29:50,650 --> 00:29:52,860
 to make a positive difference that's an

408
00:29:52,860 --> 00:29:57,780
 attachment so better off to let that go just do what comes

409
00:29:57,780 --> 00:30:01,940
 naturally and mindfully and that'll be

410
00:30:01,940 --> 00:30:10,040
 good what do you think about mystic experiences that use

411
00:30:10,040 --> 00:30:15,500
 suffering use fuel to have more intense

412
00:30:15,500 --> 00:30:20,780
 experiences I don't think about them at all really some in

413
00:30:20,780 --> 00:30:24,060
 society say the future is your brain on

414
00:30:24,060 --> 00:30:29,110
 drugs what do you think about this blasphemy I don't think

415
00:30:29,110 --> 00:30:36,100
 about it at all sorry questions

416
00:30:36,100 --> 00:30:38,760
 unrelated to what I do I'm not that keen to answer them

417
00:30:38,760 --> 00:30:42,900
 there's just too many and I don't

418
00:30:42,900 --> 00:30:51,270
 really have answers so that was somewhat satisfying and if

419
00:30:51,270 --> 00:30:54,740
 it wasn't I'll go meditate and let it go

420
00:30:54,740 --> 00:30:58,180
 good night everyone thank you all for tuning in

421
00:30:58,180 --> 00:31:00,180
 you

422
00:31:00,180 --> 00:31:02,180
 you

423
00:31:02,180 --> 00:31:04,180
 you

424
00:31:04,180 --> 00:31:06,180
 you

425
00:31:06,180 --> 00:31:08,180
 you

426
00:31:08,180 --> 00:31:10,180
 you

427
00:31:10,180 --> 00:31:12,180
 you

428
00:31:12,180 --> 00:31:14,180
 you

429
00:31:14,180 --> 00:31:16,180
 you

430
00:31:16,180 --> 00:31:18,180
 you

431
00:31:18,180 --> 00:31:20,180
 you

432
00:31:20,180 --> 00:31:22,180
 you

433
00:31:22,180 --> 00:31:24,180
 you

434
00:31:24,180 --> 00:31:26,180
 you

