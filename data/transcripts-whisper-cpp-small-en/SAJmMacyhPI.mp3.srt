1
00:00:00,000 --> 00:00:04,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,000 --> 00:00:13,000
 Today we continue with verse 215, which reads as follows.

3
00:00:13,000 --> 00:00:23,000
 Kamat o Jayati Soko, Kamat o Jayati Bayang,

4
00:00:23,000 --> 00:00:30,000
 Kamat o Wipamotasa, Nati Soko Kutto Bayang.

5
00:00:33,000 --> 00:00:44,160
 Which means from desire comes sorrow, from desire comes

6
00:00:44,160 --> 00:00:45,000
 danger.

7
00:00:45,000 --> 00:00:56,000
 When one is freed from desire, there is no sorrow.

8
00:00:56,000 --> 00:01:00,000
 From where can there be danger?

9
00:01:00,000 --> 00:01:16,450
 This verse was taught in response to the story of Anithi G

10
00:01:16,450 --> 00:01:17,000
anda.

11
00:01:17,000 --> 00:01:23,840
 Anithi Ganda means, Anithi Ganda is a name, I'm not sure if

12
00:01:23,840 --> 00:01:25,000
 it was really his name,

13
00:01:25,000 --> 00:01:35,210
 but it means one who doesn't have a smell for women, I

14
00:01:35,210 --> 00:01:37,000
 think.

15
00:01:37,000 --> 00:01:45,250
 I think it's possibly some kind of colloquialism from

16
00:01:45,250 --> 00:01:46,000
 ancient India,

17
00:01:46,000 --> 00:01:50,000
 like we would say when someone has a taste for something.

18
00:01:51,000 --> 00:01:55,000
 That's only a guess, but it's based on the story.

19
00:01:55,000 --> 00:02:01,000
 Because Anithi Ganda means smell, and Iti means woman.

20
00:02:01,000 --> 00:02:05,000
 An is no or not, one who doesn't have.

21
00:02:05,000 --> 00:02:13,000
 Anithi Ganda was a man who lived in Sawati,

22
00:02:15,000 --> 00:02:21,000
 but in past life he had been a Brahma, a God,

23
00:02:21,000 --> 00:02:29,000
 a being that lives in a realm without sensuality.

24
00:02:29,000 --> 00:02:36,000
 So it means in some far and distant time in the past,

25
00:02:36,000 --> 00:02:41,310
 he had developed high states of concentration and

26
00:02:41,310 --> 00:02:43,000
 absorption.

27
00:02:44,000 --> 00:02:53,420
 And been reborn in these great profound deep realms of

28
00:02:53,420 --> 00:02:56,000
 existence,

29
00:02:56,000 --> 00:03:03,380
 and had lived there for lengths of time that end up seeming

30
00:03:03,380 --> 00:03:07,000
 very much like eternity.

31
00:03:08,000 --> 00:03:12,000
 But they aren't, of course, eternity.

32
00:03:12,000 --> 00:03:15,530
 So with the ending of those states of existence, one is

33
00:03:15,530 --> 00:03:16,000
 reborn,

34
00:03:16,000 --> 00:03:20,000
 and he was reborn as a young man, he was born as a baby,

35
00:03:20,000 --> 00:03:26,000
 grew up as a human, and as a young man, from birth,

36
00:03:26,000 --> 00:03:29,000
 wanted to have nothing to do with women.

37
00:03:29,000 --> 00:03:32,000
 Hence he got this name.

38
00:03:34,000 --> 00:03:38,140
 Whenever a woman came close to him, he would scream, he

39
00:03:38,140 --> 00:03:39,000
 would wail.

40
00:03:39,000 --> 00:03:45,600
 When they tried to breastfeed him, they had to cover

41
00:03:45,600 --> 00:03:47,000
 themselves up

42
00:03:47,000 --> 00:03:52,050
 and expose only their breasts so that he couldn't see the

43
00:03:52,050 --> 00:03:53,000
 woman.

44
00:03:53,000 --> 00:03:58,880
 Now I'm not sure how, again it's just a story, but it's

45
00:03:58,880 --> 00:04:00,000
 quite possible.

46
00:04:00,000 --> 00:04:10,300
 But the emphasis here is on the fact that his life in the

47
00:04:10,300 --> 00:04:11,000
 Brahma world

48
00:04:11,000 --> 00:04:19,000
 left him with no sense of attachment to women.

49
00:04:19,000 --> 00:04:24,000
 And it may be that, I mean this is what the story says,

50
00:04:24,000 --> 00:04:28,000
 that he actually had a strong aversion to them.

51
00:04:29,000 --> 00:04:34,000
 But the point is that he had no desire for women.

52
00:04:34,000 --> 00:04:44,210
 And so his parents tried to find, tried to encourage him to

53
00:04:44,210 --> 00:04:45,000
 get married.

54
00:04:45,000 --> 00:04:50,000
 And so they would ask him, "Son, son, please, we want you

55
00:04:50,000 --> 00:04:53,000
 to carry on our lineage,

56
00:04:53,000 --> 00:04:57,320
 have children, become a householder. When are you going to

57
00:04:57,320 --> 00:04:58,000
 get married?

58
00:04:58,000 --> 00:05:00,000
 When are you going to go find a woman?"

59
00:05:00,000 --> 00:05:04,000
 They tried. They knew that he had this aversion to women

60
00:05:04,000 --> 00:05:06,000
 and they pushed him and pushed him.

61
00:05:06,000 --> 00:05:09,000
 And finally he got fed up with it.

62
00:05:09,000 --> 00:05:17,000
 And he built a statue. He created a statue of a woman,

63
00:05:17,000 --> 00:05:21,000
 a replica of the most beautiful woman.

64
00:05:22,000 --> 00:05:29,180
 And she was just, this statue was the perfect figure of a

65
00:05:29,180 --> 00:05:30,000
 woman.

66
00:05:30,000 --> 00:05:33,460
 And so when his parents came and said, "Son, son, when are

67
00:05:33,460 --> 00:05:35,000
 you going to find a wife?"

68
00:05:35,000 --> 00:05:36,000
 He said, "Look."

69
00:05:36,000 --> 00:05:39,830
 And he showed them the statue and he said, "If you can find

70
00:05:39,830 --> 00:05:41,000
 me a woman

71
00:05:41,000 --> 00:05:47,370
 as beautiful as that, then I will get married. I will marry

72
00:05:47,370 --> 00:05:48,000
 her."

73
00:05:50,000 --> 00:05:54,000
 So I imagine some of the aversion, it probably wasn't that

74
00:05:54,000 --> 00:05:55,000
 he was homosexual,

75
00:05:55,000 --> 00:06:00,000
 it probably was just, again, his life in the Brahma realms

76
00:06:00,000 --> 00:06:06,160
 caused him to break away from the ordinary attachment to

77
00:06:06,160 --> 00:06:07,000
 sensuality

78
00:06:07,000 --> 00:06:09,000
 that most people find themselves stuck in.

79
00:06:09,000 --> 00:06:15,050
 And so he was able to see the flaws, I would say, in the

80
00:06:15,050 --> 00:06:16,000
 human body.

81
00:06:16,000 --> 00:06:21,000
 And so while everyone else was raving about the beauty,

82
00:06:21,000 --> 00:06:24,000
 all he saw was the ugliness.

83
00:06:24,000 --> 00:06:31,000
 And so hearing of this beauty, he created this statue and

84
00:06:31,000 --> 00:06:31,000
 he said,

85
00:06:31,000 --> 00:06:35,150
 "Look, there is no beauty that I see here, but if you find

86
00:06:35,150 --> 00:06:36,000
 this beauty,

87
00:06:36,000 --> 00:06:41,170
 something that is actually beautiful, this perfect woman,

88
00:06:41,170 --> 00:06:43,000
 then I will marry her."

89
00:06:44,000 --> 00:06:46,520
 So the parents said, "Okay, well, there has got to be

90
00:06:46,520 --> 00:06:48,000
 someone in the world that beautiful.

91
00:06:48,000 --> 00:06:52,000
 Our son is a very meritorious sort of person.

92
00:06:52,000 --> 00:06:57,000
 He clearly has some strong qualities of good karma,

93
00:06:57,000 --> 00:07:01,400
 so there has got to be someone out there who has the same

94
00:07:01,400 --> 00:07:02,000
 level as him

95
00:07:02,000 --> 00:07:06,000
 and perhaps has some connection with him from past lives."

96
00:07:06,000 --> 00:07:10,710
 And so they took this statue in a cart and they traveled

97
00:07:10,710 --> 00:07:12,000
 around the countryside

98
00:07:12,000 --> 00:07:14,000
 looking for such a woman.

99
00:07:14,000 --> 00:07:20,000
 There happens to have been such a woman in a city called

100
00:07:20,000 --> 00:07:22,000
 Sagara, I think.

101
00:07:22,000 --> 00:07:28,000
 And so they brought this statue there.

102
00:07:28,000 --> 00:07:32,470
 When they went to the bathing place, this woman came and

103
00:07:32,470 --> 00:07:33,000
 saw her and said,

104
00:07:33,000 --> 00:07:35,000
 "What are you doing out here?"

105
00:07:36,000 --> 00:07:42,000
 She had just bathed this... there was this young woman

106
00:07:42,000 --> 00:07:45,000
 living on the top floor

107
00:07:45,000 --> 00:07:47,000
 of a seven-story mansion.

108
00:07:47,000 --> 00:07:55,370
 There's this sort of stereotype, or I guess it's an ancient

109
00:07:55,370 --> 00:07:57,000
 tradition perhaps,

110
00:07:57,000 --> 00:08:02,000
 of keeping women, young daughters secluded.

111
00:08:02,000 --> 00:08:06,000
 And I think it's sort of the ideal of the perfect maiden,

112
00:08:06,000 --> 00:08:08,000
 the perfect virgin,

113
00:08:08,000 --> 00:08:11,750
 having no contact with men, having never had contact with

114
00:08:11,750 --> 00:08:12,000
 men,

115
00:08:12,000 --> 00:08:16,000
 so she would be kept up in a tower.

116
00:08:16,000 --> 00:08:20,000
 I think we hear about such stories like Rapunzel and so on.

117
00:08:20,000 --> 00:08:23,000
 Anyway.

118
00:08:23,000 --> 00:08:28,440
 And she had just bathed this... this was the maid of the

119
00:08:28,440 --> 00:08:29,000
 house,

120
00:08:29,000 --> 00:08:33,500
 the nurse maid or so on. She had just bathed her care, the

121
00:08:33,500 --> 00:08:34,000
 daughter,

122
00:08:34,000 --> 00:08:38,100
 and then went down and went out to the waterhole or

123
00:08:38,100 --> 00:08:39,000
 whatever.

124
00:08:39,000 --> 00:08:42,610
 And she saw this statue and she said, "What are you doing

125
00:08:42,610 --> 00:08:43,000
 out here?

126
00:08:43,000 --> 00:08:47,000
 I just bathed you, and now you're out and about."

127
00:08:47,000 --> 00:08:51,000
 And she struck the girl, I don't know, hit her or whatever,

128
00:08:51,000 --> 00:08:54,460
 and realized it was just a statue and said, "What's this

129
00:08:54,460 --> 00:08:55,000
 doing here?"

130
00:08:56,000 --> 00:09:01,000
 And the men who had been tasked to carry this around came

131
00:09:01,000 --> 00:09:01,000
 and said,

132
00:09:01,000 --> 00:09:03,980
 "What are you doing? Why do you have a statue of my

133
00:09:03,980 --> 00:09:05,000
 daughter?

134
00:09:05,000 --> 00:09:09,000
 My... it's not my daughter, my charge."

135
00:09:09,000 --> 00:09:13,730
 And they said, "Your daughter looks like this, your charge

136
00:09:13,730 --> 00:09:15,000
 looks like this."

137
00:09:15,000 --> 00:09:19,730
 And she said, "To be honest, that has nothing on my

138
00:09:19,730 --> 00:09:21,000
 mistress.

139
00:09:21,000 --> 00:09:25,000
 She is far more beautiful than that."

140
00:09:25,000 --> 00:09:27,000
 And they said, "Oh, please, can you take us to her?"

141
00:09:27,000 --> 00:09:31,180
 And they went and they saw this girl and indeed she was the

142
00:09:31,180 --> 00:09:34,000
 most beautiful girl in the realm.

143
00:09:34,000 --> 00:09:39,000
 And so they traveled back, they talked with them about

144
00:09:39,000 --> 00:09:41,000
 their plans and they traveled back

145
00:09:41,000 --> 00:09:45,660
 and the young man, they told the young man and the parents

146
00:09:45,660 --> 00:09:47,000
 about this woman.

147
00:09:49,000 --> 00:09:55,000
 And the Nithiganda became suddenly a desire arose in him

148
00:09:55,000 --> 00:09:58,000
 that had never arisen before.

149
00:09:58,000 --> 00:10:00,910
 Suddenly this desire that he had never felt when he looked

150
00:10:00,910 --> 00:10:02,000
 at another woman,

151
00:10:02,000 --> 00:10:06,020
 when he looked at other women, suddenly it arose in him,

152
00:10:06,020 --> 00:10:07,000
 this desire.

153
00:10:07,000 --> 00:10:12,000
 He was anticipating, always asking, "When is she coming?"

154
00:10:12,000 --> 00:10:16,000
 And they sent a message that she should come into carriage,

155
00:10:16,000 --> 00:10:20,000
 that she was so fragile and they had kept her so secluded,

156
00:10:20,000 --> 00:10:23,010
 that the story says quite matter of factly that she died

157
00:10:23,010 --> 00:10:24,000
 along the way.

158
00:10:24,000 --> 00:10:27,000
 She fell ill and died while she was traveling

159
00:10:27,000 --> 00:10:31,000
 because the travel was just too hard for her.

160
00:10:31,000 --> 00:10:36,000
 Simple as that.

161
00:10:36,000 --> 00:10:40,250
 And the Nithiganda kept asking, "When is she coming? Is she

162
00:10:40,250 --> 00:10:41,000
 coming today?"

163
00:10:41,000 --> 00:10:44,960
 And the parents tried to stall him but eventually they had

164
00:10:44,960 --> 00:10:46,000
 to let him know that.

165
00:10:46,000 --> 00:10:53,470
 Unfortunately she was so close to being united in heavenly

166
00:10:53,470 --> 00:10:56,000
 matrimony or whatever

167
00:10:56,000 --> 00:10:59,000
 and yet she died along the way.

168
00:10:59,000 --> 00:11:05,730
 And the Nithiganda became very sad and locked himself in

169
00:11:05,730 --> 00:11:07,000
 his room I guess.

170
00:11:08,000 --> 00:11:11,880
 The Buddha found out about this and came for breakfast for

171
00:11:11,880 --> 00:11:14,000
 food one day

172
00:11:14,000 --> 00:11:17,280
 and they fed the Buddha and the Buddha asked them, "Where

173
00:11:17,280 --> 00:11:18,000
 is the Nithiganda?"

174
00:11:18,000 --> 00:11:21,000
 And they said, "Oh, he's up in his room."

175
00:11:21,000 --> 00:11:23,420
 And so they called him down and the Buddha said, "What's

176
00:11:23,420 --> 00:11:24,000
 wrong?"

177
00:11:24,000 --> 00:11:26,000
 And he explained to the Buddha, "What's wrong?"

178
00:11:26,000 --> 00:11:31,060
 And the Buddha said, "Oh, Nithiganda, do you know why you

179
00:11:31,060 --> 00:11:32,000
 are so sad?

180
00:11:32,000 --> 00:11:35,000
 Do you know where this sadness comes from?"

181
00:11:36,000 --> 00:11:39,000
 And he said, "You tell me where it comes from."

182
00:11:39,000 --> 00:11:42,000
 And the Buddha said, "It comes from desire."

183
00:11:42,000 --> 00:11:45,000
 And then he taught this verse.

184
00:11:45,000 --> 00:11:50,280
 So there's a couple of things about this verse, about this

185
00:11:50,280 --> 00:11:52,000
 story and this verse.

186
00:11:52,000 --> 00:11:57,410
 The first is of course the obvious lesson that it gives us

187
00:11:57,410 --> 00:12:00,000
 another facet, another angle

188
00:12:02,000 --> 00:12:11,690
 on the topic of desire, on the topic of suffering coming

189
00:12:11,690 --> 00:12:13,000
 from desire.

190
00:12:13,000 --> 00:12:17,000
 So we've talked about how the striving and the fighting

191
00:12:17,000 --> 00:12:20,000
 over objects of desire can lead to suffering.

192
00:12:20,000 --> 00:12:23,000
 We've talked about the loss of objects of desire.

193
00:12:23,000 --> 00:12:33,470
 Here we have the inability to obtain the object of desire,

194
00:12:33,470 --> 00:12:36,000
 which is again a common theme in life.

195
00:12:36,000 --> 00:12:42,760
 People whose ambitions are unfulfilled, whose wishes are un

196
00:12:42,760 --> 00:12:46,000
fulfilled, whose love is unrequited.

197
00:12:49,000 --> 00:12:52,450
 And we see in the build up, in the anticipation, this

198
00:12:52,450 --> 00:12:54,000
 cultivation of desire,

199
00:12:54,000 --> 00:13:00,200
 day after day where Nithiganda is sitting at home, building

200
00:13:00,200 --> 00:13:03,000
 day after day this anticipation

201
00:13:03,000 --> 00:13:10,490
 and stoking up his desire to a fevered pitch until it is

202
00:13:10,490 --> 00:13:13,000
 finally crushed.

203
00:13:15,000 --> 00:13:18,600
 I've heard, I've talked with people who end up quite

204
00:13:18,600 --> 00:13:22,000
 depressed and quite obsessed with something in the past

205
00:13:22,000 --> 00:13:26,000
 where they feel was really their chance at happiness.

206
00:13:26,000 --> 00:13:31,760
 And they get so stuck on that, that they're unable to free

207
00:13:31,760 --> 00:13:35,000
 themselves from the cycle of depression.

208
00:13:35,000 --> 00:13:39,540
 They feel hopeless and helpless because of course with the

209
00:13:39,540 --> 00:13:41,000
 past you can't change it.

210
00:13:41,000 --> 00:13:48,100
 And the more your key to success gets stuck in the past,

211
00:13:48,100 --> 00:13:50,000
 the more desperate your situation becomes,

212
00:13:50,000 --> 00:13:55,000
 the harder it becomes to free yourself.

213
00:14:01,000 --> 00:14:07,170
 And in all we've looked at many aspects and it really is a

214
00:14:07,170 --> 00:14:11,740
 good sort of multifaceted lesson in how desire leads to

215
00:14:11,740 --> 00:14:12,000
 suffering.

216
00:14:12,000 --> 00:14:15,730
 When we look at all these facets and when we look at all

217
00:14:15,730 --> 00:14:18,000
 the ways in which we could suffer

218
00:14:18,000 --> 00:14:21,690
 based on what appears to be the cause of happiness, which

219
00:14:21,690 --> 00:14:25,000
 is obtaining the objects of your desire,

220
00:14:26,000 --> 00:14:29,550
 we do suffer in life based on them. We come to see that it

221
00:14:29,550 --> 00:14:34,090
's not insignificant for us to say that it is fraught with

222
00:14:34,090 --> 00:14:37,000
 sorrow and danger.

223
00:14:37,000 --> 00:14:46,050
 But the other lesson is a little less obvious, but also

224
00:14:46,050 --> 00:14:49,000
 quite important and profound,

225
00:14:49,000 --> 00:14:53,710
 and that is how divested from reality, if that's the

226
00:14:53,710 --> 00:14:59,000
 correct use of the term, our perceptions of desire are...

227
00:14:59,000 --> 00:15:10,150
 We think we desire people and experiences, I guess, like a

228
00:15:10,150 --> 00:15:18,000
 state of being married, of being romantically involved.

229
00:15:19,000 --> 00:15:26,770
 Experiences like sex and cuddling and so on, interactions

230
00:15:26,770 --> 00:15:30,000
 with other people.

231
00:15:30,000 --> 00:15:36,000
 We think that these are the things that we desire.

232
00:15:36,000 --> 00:15:41,180
 We think that we are desiring the beautiful shape, the

233
00:15:41,180 --> 00:15:46,750
 sight, the sight, the sound, the smell, the taste, the

234
00:15:46,750 --> 00:15:48,000
 feeling.

235
00:15:51,000 --> 00:15:59,000
 We think our desire is something... We make some idea of

236
00:15:59,000 --> 00:16:08,000
 desire that is far more complicated, complex,

237
00:16:08,000 --> 00:16:13,000
 and perhaps even far more convincing than the reality.

238
00:16:14,000 --> 00:16:19,610
 Convincing in the sense that it's easy to make a narrative

239
00:16:19,610 --> 00:16:22,000
 out of a love affair.

240
00:16:22,000 --> 00:16:26,850
 It's easy to make this story, if not given in a Buddhist

241
00:16:26,850 --> 00:16:30,000
 context, would be a great tragedy.

242
00:16:30,000 --> 00:16:33,700
 In a Buddhist context, it's a great victory in the sense,

243
00:16:33,700 --> 00:16:37,000
 not for the woman, but for the man,

244
00:16:37,000 --> 00:16:44,960
 in the sense that he's come to realize that not only A was

245
00:16:44,960 --> 00:16:52,000
 still very much susceptible to desire,

246
00:16:52,000 --> 00:16:55,840
 but also that the desire was on a deeper level than the

247
00:16:55,840 --> 00:16:59,000
 aversion that he had towards ugliness.

248
00:17:01,000 --> 00:17:10,370
 The danger in desire was the potential for loss, the

249
00:17:10,370 --> 00:17:15,000
 potential for lack of obtaining and so on.

250
00:17:29,000 --> 00:17:36,530
 But the reality is that we don't crave for any of these

251
00:17:36,530 --> 00:17:46,330
 things, all of these narratives and stories and ideas we

252
00:17:46,330 --> 00:17:48,000
 have of desire.

253
00:17:48,000 --> 00:17:50,000
 They mask what's the real truth.

254
00:17:51,000 --> 00:17:55,660
 That is, the desire is really, in some sense, much more

255
00:17:55,660 --> 00:18:00,920
 similar to a drug addiction, or desire for any kind of sens

256
00:18:00,920 --> 00:18:02,000
uality.

257
00:18:02,000 --> 00:18:08,360
 And you can see this in this story. He had never seen this

258
00:18:08,360 --> 00:18:09,000
 woman.

259
00:18:11,000 --> 00:18:15,920
 He had a statue that was supposed to look like her, but it

260
00:18:15,920 --> 00:18:21,000
's quite remarkable that his desire became so strong.

261
00:18:21,000 --> 00:18:26,030
 Not that this is surprising, but because it's the sort of

262
00:18:26,030 --> 00:18:30,000
 thing that we can probably identify with.

263
00:18:30,000 --> 00:18:36,000
 We anticipate, we build up this idea in our minds.

264
00:18:39,000 --> 00:18:44,820
 That has nothing to do with the actual liking of what we're

265
00:18:44,820 --> 00:18:46,000
 seeing.

266
00:18:46,000 --> 00:18:49,080
 We say that it's this thing that we see that we want, but

267
00:18:49,080 --> 00:18:51,000
 he's never even seen this woman.

268
00:18:51,000 --> 00:18:56,260
 What he was anticipating was the pleasure that he would get

269
00:18:56,260 --> 00:18:58,000
 from seeing her.

270
00:19:01,000 --> 00:19:03,930
 And in fact, probably during the time that he was

271
00:19:03,930 --> 00:19:07,610
 anticipating seeing her, he was actually already engaging

272
00:19:07,610 --> 00:19:11,290
 in the drug addiction, because his mind was producing the

273
00:19:11,290 --> 00:19:12,000
 chemicals.

274
00:19:12,000 --> 00:19:16,950
 As long as you can build this narrative up of beauty and of

275
00:19:16,950 --> 00:19:21,600
 pleasure, you can feel the great pleasure that comes along

276
00:19:21,600 --> 00:19:26,000
 with addiction, even without getting what you want.

277
00:19:26,000 --> 00:19:30,880
 And so the pleasure had nothing to do at that point with

278
00:19:30,880 --> 00:19:34,000
 seeing or encountering this woman.

279
00:19:34,000 --> 00:19:39,500
 It had to do with him triggering the feelings of pleasure

280
00:19:39,500 --> 00:19:41,000
 in his brain, really.

281
00:19:41,000 --> 00:19:44,000
 The brain chemicals that we become addicted to.

282
00:19:48,000 --> 00:19:52,610
 Our desires are not related to what we think of as the

283
00:19:52,610 --> 00:19:55,000
 objects of our desire.

284
00:19:55,000 --> 00:20:00,620
 They're related in a secondary sense, in terms of being a

285
00:20:00,620 --> 00:20:02,000
 catalyst.

286
00:20:02,000 --> 00:20:09,670
 So being unable to manufacture these chemicals directly, we

287
00:20:09,670 --> 00:20:14,600
 trigger them through this system of habits, where in the

288
00:20:14,600 --> 00:20:17,000
 past we've experienced pleasure through these things.

289
00:20:17,000 --> 00:20:21,060
 So when we see them, we're able to give rise to the feeling

290
00:20:21,060 --> 00:20:23,000
 of pleasure in the mind.

291
00:20:23,000 --> 00:20:32,350
 When heterosexual men see women, they become...it triggers

292
00:20:32,350 --> 00:20:38,610
 something in the brain, and they're able to experience this

293
00:20:38,610 --> 00:20:40,000
 pleasure.

294
00:20:44,000 --> 00:20:50,140
 When homosexual men see men, handsome men, they become...or

295
00:20:50,140 --> 00:20:54,420
 the sign of the man in the body, they likewise give rise to

296
00:20:54,420 --> 00:20:56,000
 this trigger.

297
00:20:56,000 --> 00:21:00,000
 We would say in Buddhism, based on habits from past lives,

298
00:21:00,000 --> 00:21:05,440
 when dogs see other dogs and they get the sign of a dog,

299
00:21:05,440 --> 00:21:10,000
 they become intoxicated.

300
00:21:10,000 --> 00:21:15,000
 The desire arises in the mind, and the pleasure arises.

301
00:21:15,000 --> 00:21:21,890
 And so this is a big part of why mindfulness is so powerful

302
00:21:21,890 --> 00:21:28,540
 in helping us overcome not just desire, but aversion in all

303
00:21:28,540 --> 00:21:35,000
 types of problems that are removed from reality.

304
00:21:35,000 --> 00:21:38,000
 Because mindfulness is focusing on what's really happening.

305
00:21:38,000 --> 00:21:42,590
 It's focusing on the very building blocks of the experience

306
00:21:42,590 --> 00:21:43,000
.

307
00:21:43,000 --> 00:21:45,950
 It's showing us, or it's allowing us to see what's

308
00:21:45,950 --> 00:21:49,000
 happening when you see something that you like.

309
00:21:49,000 --> 00:21:52,280
 That it's not the thing that you like, it's actually the

310
00:21:52,280 --> 00:21:55,280
 feeling of pleasure that arises that you're able to give

311
00:21:55,280 --> 00:21:56,000
 rise to.

312
00:21:59,000 --> 00:22:02,550
 If it was the thing that brought us pleasure, then looking

313
00:22:02,550 --> 00:22:06,280
 at it incessantly would always bring us pleasure, but we

314
00:22:06,280 --> 00:22:08,000
 see that that's also not the case.

315
00:22:08,000 --> 00:22:13,080
 And this is why often it's very difficult for romantic

316
00:22:13,080 --> 00:22:17,000
 partners to stay faithful. It's one of the reasons.

317
00:22:17,000 --> 00:22:20,350
 Because they become attracted to something else, because of

318
00:22:20,350 --> 00:22:24,590
 course the brain is...the way it works, it's very hard for

319
00:22:24,590 --> 00:22:28,000
 the same object to give rise to the same...

320
00:22:28,000 --> 00:22:31,770
 to the same amount of pleasure, and so you need something

321
00:22:31,770 --> 00:22:34,000
 new, something exciting.

322
00:22:34,000 --> 00:22:37,230
 It has to be something that actually is able to turn the

323
00:22:37,230 --> 00:22:45,000
 switch on. The mind becomes, in a sense, worn away.

324
00:22:45,000 --> 00:22:49,520
 It's not exactly, but it becomes...it's like a belt in your

325
00:22:49,520 --> 00:22:53,000
 car that becomes stretched and no longer works.

326
00:22:53,000 --> 00:22:58,090
 And so it's like the mind gets tired of the same old thing.

327
00:22:58,090 --> 00:23:00,630
 It's like this is why we feel we get bored of the same old

328
00:23:00,630 --> 00:23:01,000
 thing.

329
00:23:01,000 --> 00:23:04,500
 So something that you used to look at and be quite excited

330
00:23:04,500 --> 00:23:08,520
 by, and turned on by, and therefore pleased by, no longer

331
00:23:08,520 --> 00:23:12,000
 gives you that pleasure, and so you have to look elsewhere.

332
00:23:12,000 --> 00:23:15,620
 That's why drug addiction becomes worse and worse, because

333
00:23:15,620 --> 00:23:19,000
 the same amount of the drug can't give you the same amount

334
00:23:19,000 --> 00:23:20,000
 of pleasure.

335
00:23:20,000 --> 00:23:23,710
 When we see it with music, why we have to...if music was so

336
00:23:23,710 --> 00:23:26,000
 pleasant, why couldn't we listen to the same song?

337
00:23:26,000 --> 00:23:29,940
 Ah, that's not how it works. You can't listen to the same

338
00:23:29,940 --> 00:23:33,000
 song for hours and hours and days on end.

339
00:23:33,000 --> 00:23:36,410
 You can, but the pleasure won't be the same as the first

340
00:23:36,410 --> 00:23:39,000
 time. And so you have to vary it, go back and forth.

341
00:23:39,000 --> 00:23:45,760
 And we learn to work with the mind to get as much pleasure

342
00:23:45,760 --> 00:23:50,000
 as we can by switching objects.

343
00:23:50,000 --> 00:23:52,960
 But that's the reality of it, and that's what mindfulness

344
00:23:52,960 --> 00:23:54,000
 allows us to see.

345
00:23:54,000 --> 00:23:58,190
 That's still not an easy thing to deal with, because these

346
00:23:58,190 --> 00:24:00,000
 addictions are real.

347
00:24:00,000 --> 00:24:03,760
 But the point is that we have to recognize, if we want to

348
00:24:03,760 --> 00:24:07,680
 be honest, we have to recognize that our desires don't come

349
00:24:07,680 --> 00:24:10,000
 from people and places and things.

350
00:24:10,000 --> 00:24:14,140
 They come really from just drug addictions to the brain

351
00:24:14,140 --> 00:24:17,000
 chemicals that bring us pleasure.

352
00:24:17,000 --> 00:24:21,750
 Being stimulated in various parts of the body or in any of

353
00:24:21,750 --> 00:24:26,430
 the various senses gives rise to brain chemicals that are

354
00:24:26,430 --> 00:24:30,000
 very difficult and challenging to overcome.

355
00:24:30,000 --> 00:24:34,320
 And mindfulness isn't able immediately to do away with

356
00:24:34,320 --> 00:24:38,000
 these. So a big part of our practice has to be patience.

357
00:24:38,000 --> 00:24:41,780
 But a very important first step is being able to see that

358
00:24:41,780 --> 00:24:45,990
 that's actually the truth and not simply turning them off,

359
00:24:45,990 --> 00:24:48,000
 but becoming aware of them.

360
00:24:48,000 --> 00:24:55,220
 Becoming aware that it's not this romantic or high-minded

361
00:24:55,220 --> 00:25:01,000
 narrative or story of a love affair or so on.

362
00:25:01,000 --> 00:25:05,260
 It's merely drug addiction. Drug addiction with its many

363
00:25:05,260 --> 00:25:08,000
 sorrows, not getting what you want, losing what you like,

364
00:25:08,000 --> 00:25:12,350
 having to compete for what you want and so on, and all the

365
00:25:12,350 --> 00:25:17,000
 stories that we've heard in this chapter.

366
00:25:17,000 --> 00:25:22,450
 So with mindfulness, we cut straight to the reality. All of

367
00:25:22,450 --> 00:25:25,000
 those stories have no meaning.

368
00:25:25,000 --> 00:25:28,550
 Seeing is just seeing, hearing. When you say to yourself, "

369
00:25:28,550 --> 00:25:32,060
seeing," seeing, you're able to experience the seeing just

370
00:25:32,060 --> 00:25:33,000
 as seeing.

371
00:25:33,000 --> 00:25:39,380
 And it doesn't excite in the same way as before. When you

372
00:25:39,380 --> 00:25:42,220
 have the desire and the pleasure, you're able to see that

373
00:25:42,220 --> 00:25:45,000
 those are actually what the mind is fixating on.

374
00:25:45,000 --> 00:25:49,980
 Whatever can give it that pleasure in the brain and in the

375
00:25:49,980 --> 00:25:53,000
 mind, that's what it aims for.

376
00:25:53,000 --> 00:25:58,940
 And then you're able to focus on those and not turn them

377
00:25:58,940 --> 00:26:04,000
 off, but slowly, slowly attenuate them.

378
00:26:04,000 --> 00:26:08,740
 Attenuate them mainly by seeing. The main and the deepest

379
00:26:08,740 --> 00:26:13,000
 part of the path is by seeing them just for what they are.

380
00:26:13,000 --> 00:26:16,430
 And the importance there is that what they are is just an

381
00:26:16,430 --> 00:26:19,000
 experience, that actually pleasure.

382
00:26:19,000 --> 00:26:23,010
 And this is on a deeper level than any of these stories.

383
00:26:23,010 --> 00:26:25,990
 Pleasure is just pleasure. There's nothing good, nothing

384
00:26:25,990 --> 00:26:27,000
 bad about it.

385
00:26:27,000 --> 00:26:31,140
 But there's desire and attachment. The liking of the

386
00:26:31,140 --> 00:26:34,000
 pleasure also has no basis in reality.

387
00:26:34,000 --> 00:26:38,650
 There's nothing intrinsically likeable, lovable, desirable

388
00:26:38,650 --> 00:26:44,200
 about pleasure, just like there's nothing intrinsically

389
00:26:44,200 --> 00:26:47,000
 undesirable about pain.

390
00:26:47,000 --> 00:26:50,290
 And being able to see through these is very powerful. It's

391
00:26:50,290 --> 00:26:53,000
 what allows us to find real peace of mind.

392
00:26:53,000 --> 00:26:56,680
 It's why a meditator, someone who has even practiced the

393
00:26:56,680 --> 00:27:00,850
 way this young man had practiced, can find great states of

394
00:27:00,850 --> 00:27:06,260
 peace and calm simply by extracting themselves from that

395
00:27:06,260 --> 00:27:08,000
 process of addiction.

396
00:27:08,000 --> 00:27:12,030
 Now, as we can see in the lesson in the story is that

397
00:27:12,030 --> 00:27:17,000
 simply extracting yourself from it doesn't last forever.

398
00:27:17,000 --> 00:27:20,250
 It doesn't solve the problem because eventually when you

399
00:27:20,250 --> 00:27:24,000
 get in contact with it, something really, really enticing,

400
00:27:24,000 --> 00:27:28,000
 then you become attached again.

401
00:27:28,000 --> 00:27:34,510
 But we can see that from meditation, from mindfulness

402
00:27:34,510 --> 00:27:38,430
 practice, that by observing it, by facing it, just like

403
00:27:38,430 --> 00:27:42,090
 facing pain, by confronting the pleasure, we come to see

404
00:27:42,090 --> 00:27:45,550
 that there's nothing special about it, not in the way we

405
00:27:45,550 --> 00:27:46,000
 thought.

406
00:27:46,000 --> 00:27:50,680
 There's certainly none of this narrative about falling in

407
00:27:50,680 --> 00:27:54,870
 love and finding the girl or the boy or the person of your

408
00:27:54,870 --> 00:27:56,000
 dreams.

409
00:27:56,000 --> 00:28:01,000
 There's simply drugs and addiction.

410
00:28:01,000 --> 00:28:04,070
 So that's the Dhammapada for tonight. Thank you all for

411
00:28:04,070 --> 00:28:07,000
 listening. I wish you all the best.

