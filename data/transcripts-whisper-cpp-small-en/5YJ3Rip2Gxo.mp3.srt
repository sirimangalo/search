1
00:00:00,000 --> 00:00:03,890
 I focus on the nostrils instead of the rising and falling

2
00:00:03,890 --> 00:00:07,030
 of the belly since I've started meditation. Are there any

3
00:00:07,030 --> 00:00:08,000
 downsides?

4
00:00:08,000 --> 00:00:14,850
 I've never used the nostrils, so I'm going either by hears

5
00:00:14,850 --> 00:00:22,000
ay or by... what's the word?... conjecture.

6
00:00:23,000 --> 00:00:31,510
 By hearsay, the Vysuddhimagga says that the nostrils or

7
00:00:31,510 --> 00:00:36,910
 mindfulness of breathing becomes more and more refined as

8
00:00:36,910 --> 00:00:38,000
 you go.

9
00:00:41,000 --> 00:00:47,640
 But the only reason, categorical reason, reason for a

10
00:00:47,640 --> 00:00:55,000
 decisive reason that was given for using the stomach is

11
00:00:55,000 --> 00:01:01,550
 that in Burma, the... in Burma, mindfulness of breathing is

12
00:01:01,550 --> 00:01:05,000
 understood to take place at the nose.

13
00:01:06,000 --> 00:01:10,140
 The mindfulness of breathing is considered a samatha

14
00:01:10,140 --> 00:01:13,030
 meditation. It's considered a meditation that starts off

15
00:01:13,030 --> 00:01:16,000
 leading you through the jhanas and later on is used to...

16
00:01:16,000 --> 00:01:18,000
 can be used to bring about vipassana.

17
00:01:18,000 --> 00:01:24,000
 But technically it's the... the...

18
00:01:27,000 --> 00:01:32,840
 jetawimuti, a person who practices samatha first and then v

19
00:01:32,840 --> 00:01:35,100
ipassana. In our technique we practice starting with vipass

20
00:01:35,100 --> 00:01:40,000
ana, so in... it was simply in order to avoid the accusation

21
00:01:40,000 --> 00:01:41,000
... these are the words of Mahasya Saiyada.

22
00:01:41,000 --> 00:01:44,770
 In order to avoid the accusation that he was teaching sam

23
00:01:44,770 --> 00:01:48,160
atha, he switched to... and it may not have been him, it may

24
00:01:48,160 --> 00:01:51,520
 have been his teacher, but they switched to... he says that

25
00:01:51,520 --> 00:01:53,000
 they switched to the stomach.

26
00:01:54,000 --> 00:02:01,310
 The... the... the... the reason for it, the reason for

27
00:02:01,310 --> 00:02:05,030
 needing to use or for deciding to use the stomach is that

28
00:02:05,030 --> 00:02:08,880
 the vysuddhi maga says that a person who practices vipass

29
00:02:08,880 --> 00:02:12,880
ana should start by focusing on the four elements which are

30
00:02:12,880 --> 00:02:16,670
 the building blocks that make up the physical aspect of

31
00:02:16,670 --> 00:02:18,000
 experience.

32
00:02:19,000 --> 00:02:23,030
 So the earth element is the feeling of hardness and soft

33
00:02:23,030 --> 00:02:27,220
ness, the air element is the feeling of pressure or flacc

34
00:02:27,220 --> 00:02:31,510
idity, the fire element is the feeling of... of heat and

35
00:02:31,510 --> 00:02:35,760
 cold, and the water element is the cohesion... cohesion

36
00:02:35,760 --> 00:02:39,250
 that... that keeps things together, or that makes things

37
00:02:39,250 --> 00:02:40,000
 stick together.

38
00:02:41,000 --> 00:02:47,240
 In this case this is the air element. That's... that's

39
00:02:47,240 --> 00:02:51,150
 regards to the texts and theory and as I said hearsay, so

40
00:02:51,150 --> 00:02:54,000
... so textual arguments.

41
00:02:55,000 --> 00:02:58,810
 From conjecture, it seems to me that because this one

42
00:02:58,810 --> 00:03:03,050
 becomes more and more refined and based on, I guess also

43
00:03:03,050 --> 00:03:07,070
 hearsay what I've heard from meditators, watching the nost

44
00:03:07,070 --> 00:03:11,600
ril can be quite soothing and as a result of it getting more

45
00:03:11,600 --> 00:03:15,900
 and more refined, there's the tendency or there appears to

46
00:03:15,900 --> 00:03:18,000
 be the tendency to lead towards samatha.

47
00:03:19,000 --> 00:03:22,060
 So if I were a person who practices mindfulness at the nost

48
00:03:22,060 --> 00:03:25,170
rils, I would conjecture is more likely and from what I've

49
00:03:25,170 --> 00:03:28,320
 seen and discussed with meditators who insist on using it,

50
00:03:28,320 --> 00:03:32,540
 more likely to lead to samatha meditation. It's not wrong,

51
00:03:32,540 --> 00:03:36,590
 it's just tends to be more round about and then you have to

52
00:03:36,590 --> 00:03:38,570
 eventually get the meditator to focus it on... on it as

53
00:03:38,570 --> 00:03:41,000
 ultimate reality.

54
00:03:42,000 --> 00:03:45,570
 But it would generally be after they've already entered

55
00:03:45,570 --> 00:03:49,000
 into the jhanas which... which takes time.

56
00:03:52,000 --> 00:04:00,670
 The... the stomach on the other hand being a coarse and

57
00:04:00,670 --> 00:04:13,050
 rather large object is subject to... well, is an obvious or

58
00:04:13,050 --> 00:04:21,000
 leads to the clear awareness of ultimate reality.

59
00:04:21,000 --> 00:04:23,750
 Quite quickly, then there's no opportunity for the arising

60
00:04:23,750 --> 00:04:26,220
 of calm watching the stomach and this is a problem that

61
00:04:26,220 --> 00:04:29,000
 people have with this meditation. They think it's wrong,

62
00:04:29,000 --> 00:04:31,790
 they think it's bad, they think it's a problem because they

63
00:04:31,790 --> 00:04:35,000
 don't feel calm, because they don't feel peaceful.

64
00:04:35,000 --> 00:04:38,500
 But watching the stomach, you're going to see impermanent

65
00:04:38,500 --> 00:04:42,550
 suffering and non-self quite quickly. You're going to get

66
00:04:42,550 --> 00:04:48,000
... get quickly into issues of having to let go or suffer.

67
00:04:49,000 --> 00:04:55,380
 It's a question of it leading directly to vipassana. So

68
00:04:55,380 --> 00:04:59,370
 there's not... it's not that there's disadvantages, it's

69
00:04:59,370 --> 00:05:02,890
 just a tendency to lead to samatha first, lead to tranqu

70
00:05:02,890 --> 00:05:04,000
ility first.

71
00:05:04,000 --> 00:05:09,070
 Whereas the practice that we do is more like insight first

72
00:05:09,070 --> 00:05:14,000
 and subsequent tranquility. Okay?

