1
00:00:00,000 --> 00:00:06,010
 okay good evening everyone welcome to our evening Dhamma

2
00:00:06,010 --> 00:00:09,360
 tonight we're looking

3
00:00:09,360 --> 00:00:19,280
 at right view no right right action tonight we're looking

4
00:00:19,280 --> 00:00:20,080
 at right action

5
00:00:20,080 --> 00:00:23,520
 Samma Kamanta

6
00:00:23,520 --> 00:00:37,290
 and so again we have to separate between the preliminary

7
00:00:37,290 --> 00:00:38,680
 path the mundane

8
00:00:38,680 --> 00:00:41,370
 path the path that we're all following when we do a

9
00:00:41,370 --> 00:00:44,080
 meditation and the noble

10
00:00:44,080 --> 00:00:50,620
 path which is the moment where you're actually right you're

11
00:00:50,620 --> 00:00:51,480
 actually in the

12
00:00:51,480 --> 00:01:02,580
 right on the right path so they the as with all the factors

13
00:01:02,580 --> 00:01:03,280
 which it's

14
00:01:03,280 --> 00:01:08,710
 generally described in terms of the mundane path and the

15
00:01:08,710 --> 00:01:09,720
 idea with all of

16
00:01:09,720 --> 00:01:14,160
 this is to understand to help us understand how we get to

17
00:01:14,160 --> 00:01:15,200
 the noble path

18
00:01:15,200 --> 00:01:21,190
 and just because you're not killing or stealing or cheating

19
00:01:21,190 --> 00:01:22,800
 doesn't mean you're

20
00:01:22,800 --> 00:01:28,680
 on the noble path right but if you refrain from all kinds

21
00:01:28,680 --> 00:01:29,480
 of wrong action

22
00:01:29,480 --> 00:01:33,430
 that's what will lead you that's part of what will lead you

23
00:01:33,430 --> 00:01:34,560
 to the noble path

24
00:01:34,560 --> 00:01:38,130
 this is the point with the talking about the preliminary

25
00:01:38,130 --> 00:01:40,080
 path it's the point of

26
00:01:40,080 --> 00:01:43,870
 actually describing the hateful noble path because at the

27
00:01:43,870 --> 00:01:45,080
 moment it seems kind

28
00:01:45,080 --> 00:01:49,460
 of silly to say you're right action you're not killing or

29
00:01:49,460 --> 00:01:50,680
 stealing or

30
00:01:50,680 --> 00:01:53,790
 cheating well of course you're not when you're in the med

31
00:01:53,790 --> 00:01:55,120
itative state your mind

32
00:01:55,120 --> 00:01:59,080
 is so fixed you couldn't kill or lie you know you couldn't

33
00:01:59,080 --> 00:02:00,280
 kill or lie or do

34
00:02:00,280 --> 00:02:09,800
 anything bad but it's about how we get there so in terms of

35
00:02:09,800 --> 00:02:10,480
 the mundane path

36
00:02:10,480 --> 00:02:14,690
 there are three factors for speech there were four for

37
00:02:14,690 --> 00:02:17,080
 action there are three not

38
00:02:17,080 --> 00:02:23,120
 killing not stealing and not committing adultery or what we

39
00:02:23,120 --> 00:02:24,000
 call sexual

40
00:02:24,000 --> 00:02:29,200
 misconduct which means cheating really or or working with

41
00:02:29,200 --> 00:02:30,880
 someone else to cheat

42
00:02:30,880 --> 00:02:36,790
 on on their partner any kind of romantic and engagement

43
00:02:36,790 --> 00:02:39,920
 that is requires

44
00:02:39,920 --> 00:02:46,500
 permission but doesn't get it that is betraying someone's

45
00:02:46,500 --> 00:02:48,720
 trust so these three

46
00:02:48,720 --> 00:02:56,790
 it's quite simple no killing no stealing and no cheating

47
00:02:56,790 --> 00:02:58,280
 seems a little too

48
00:02:58,280 --> 00:03:03,220
 simple right can't be that simple and again these are the

49
00:03:03,220 --> 00:03:04,880
 indicators they're

50
00:03:04,880 --> 00:03:09,220
 like signposts or fence posts in a way because the fence

51
00:03:09,220 --> 00:03:10,720
 still has to be filled

52
00:03:10,720 --> 00:03:14,670
 in but these give you a marker if you're killing you're

53
00:03:14,670 --> 00:03:16,280
 outside of the noble path

54
00:03:16,280 --> 00:03:19,340
 you're you're not gonna get to the noble not the outside

55
00:03:19,340 --> 00:03:20,480
 you're you're not going

56
00:03:20,480 --> 00:03:24,880
 to get to the noble path you're not on a path that will

57
00:03:24,880 --> 00:03:27,320
 lead you to the noble

58
00:03:27,320 --> 00:03:32,850
 path if you're stealing refraining from these things as

59
00:03:32,850 --> 00:03:35,200
 part of what allows you

60
00:03:35,200 --> 00:03:40,440
 to concentrate to focus your mind but it's important to

61
00:03:40,440 --> 00:03:41,400
 note that it's even on

62
00:03:41,400 --> 00:03:45,800
 the mundane path it's actually not about the action itself

63
00:03:45,800 --> 00:03:47,400
 when we talk about

64
00:03:47,400 --> 00:03:50,290
 even when we talk about these things as indicators like oh

65
00:03:50,290 --> 00:03:51,280
 if you're killing

66
00:03:51,280 --> 00:03:54,800
 that's a sign that you're not going in the right direction

67
00:03:54,800 --> 00:03:56,600
 and so on but it's

68
00:03:56,600 --> 00:04:00,920
 not even about the killing when we talk about right action

69
00:04:00,920 --> 00:04:01,640
 we're not even

70
00:04:01,640 --> 00:04:05,310
 talking about the action itself we're talking about the

71
00:04:05,310 --> 00:04:07,600
 mind's intention to

72
00:04:07,600 --> 00:04:13,560
 commit the action we're talking about curbing that

73
00:04:13,560 --> 00:04:15,560
 intention so that it

74
00:04:15,560 --> 00:04:21,840
 doesn't become physical right we are talking about when the

75
00:04:21,840 --> 00:04:22,680
 intention becomes

76
00:04:22,680 --> 00:04:28,090
 physical but the physical act itself isn't important or isn

77
00:04:28,090 --> 00:04:29,600
't itself

78
00:04:29,600 --> 00:04:33,290
 important why this is important to specify is because of

79
00:04:33,290 --> 00:04:33,920
 course you can

80
00:04:33,920 --> 00:04:38,610
 kill something without meaning it meaning without meaning

81
00:04:38,610 --> 00:04:39,600
 to you can kill

82
00:04:39,600 --> 00:04:45,720
 a person without any bad intention we don't call it killing

83
00:04:45,720 --> 00:04:48,240
 in Buddhism it's

84
00:04:48,240 --> 00:04:50,840
 kind of interesting how often this question comes up if you

85
00:04:50,840 --> 00:04:51,480
 step on an ant

86
00:04:51,480 --> 00:04:55,690
 someone this morning actually I said to me he killed a

87
00:04:55,690 --> 00:04:57,360
 spider in the bathroom

88
00:04:57,360 --> 00:05:00,960
 but he didn't mean to and he wasn't clear that it wasn't it

89
00:05:00,960 --> 00:05:02,960
 was not wrong so

90
00:05:02,960 --> 00:05:08,790
 it's important to be clear on this that it's not about the

91
00:05:08,790 --> 00:05:11,460
 action it's about the

92
00:05:11,460 --> 00:05:16,840
 intention leading to the action and so of course this is

93
00:05:16,840 --> 00:05:19,960
 the first step towards

94
00:05:19,960 --> 00:05:25,560
 bringing the mind to a pure state is destroying the the

95
00:05:25,560 --> 00:05:26,840
 most coarse

96
00:05:26,840 --> 00:05:31,360
 defilements which are the actions where our defilements

97
00:05:31,360 --> 00:05:35,360
 become physical you can

98
00:05:35,360 --> 00:05:39,580
 have such anger for someone and that's a bad thing but the

99
00:05:39,580 --> 00:05:41,120
 moment you let it

100
00:05:41,120 --> 00:05:47,790
 spill over into your action it's gone to another level it's

101
00:05:47,790 --> 00:05:48,440
 categorically

102
00:05:48,440 --> 00:05:54,720
 different it's now become a physical reality you attack

103
00:05:54,720 --> 00:05:55,560
 someone even if you

104
00:05:55,560 --> 00:06:04,700
 just attempt to kill them it's become wrong action in that

105
00:06:04,700 --> 00:06:07,080
 sense and so

106
00:06:07,080 --> 00:06:12,910
 technically though it's not how the Buddha describes it but

107
00:06:12,910 --> 00:06:14,280
 for the purposes

108
00:06:14,280 --> 00:06:19,240
 of our meditation we as Buddhists we keep these sort of

109
00:06:19,240 --> 00:06:21,240
 course fence posts in

110
00:06:21,240 --> 00:06:27,570
 mind yes killing stealing cheating but as meditators it's

111
00:06:27,570 --> 00:06:28,320
 much it's it's

112
00:06:28,320 --> 00:06:31,600
 important that we have a much more refined sense of right

113
00:06:31,600 --> 00:06:33,040
 action right

114
00:06:33,040 --> 00:06:39,330
 action especially I think in the sense that we try to do

115
00:06:39,330 --> 00:06:41,800
 nothing with our hands

116
00:06:41,800 --> 00:06:50,920
 our feet term our bodies that is unmindful you know what

117
00:06:50,920 --> 00:06:51,440
 about if you

118
00:06:51,440 --> 00:06:56,730
 stub your toe and you kick the table or you smash the chair

119
00:06:56,730 --> 00:06:57,960
 you throw it over

120
00:06:57,960 --> 00:07:01,440
 you your computer isn't working and you pick up the monitor

121
00:07:01,440 --> 00:07:02,160
 and you throw it on

122
00:07:02,160 --> 00:07:09,740
 the ground that's wrong action you storm around or you or

123
00:07:09,740 --> 00:07:12,080
 worse you hit someone

124
00:07:12,080 --> 00:07:21,280
 you hurt someone right torture isn't in the precepts you

125
00:07:21,280 --> 00:07:21,940
 can keep the five

126
00:07:21,940 --> 00:07:25,970
 precepts and be a very nasty person doing lots of very

127
00:07:25,970 --> 00:07:27,840
 nasty things rape

128
00:07:27,840 --> 00:07:32,900
 rape I think some people would say it breaks the third

129
00:07:32,900 --> 00:07:35,560
 precept but either way

130
00:07:35,560 --> 00:07:39,440
 it's just a you know no matter what no matter whether it

131
00:07:39,440 --> 00:07:40,560
 does or not it's clear

132
00:07:40,560 --> 00:07:44,180
 that there are these things that are wrong and as medit

133
00:07:44,180 --> 00:07:45,400
ators we have a very

134
00:07:45,400 --> 00:07:48,620
 refined sense of this during the meditation course you

135
00:07:48,620 --> 00:07:49,840
 start to see that

136
00:07:49,840 --> 00:07:53,960
 even turning your body quickly is wrong

137
00:07:54,120 --> 00:07:58,910
 reaching for something quickly out of desire or scratching

138
00:07:58,910 --> 00:08:00,280
 your face out of a

139
00:08:00,280 --> 00:08:08,270
 version is wrong action you feel pain and adjusting

140
00:08:08,270 --> 00:08:09,880
 yourself you start to see

141
00:08:09,880 --> 00:08:13,760
 that even that is wrong because it's an it's a bad

142
00:08:13,760 --> 00:08:16,960
 intention become an action

143
00:08:16,960 --> 00:08:23,200
 which really does make a difference if you want to scratch

144
00:08:23,200 --> 00:08:24,520
 for example

145
00:08:24,520 --> 00:08:27,980
 eventually sometimes you do have to scratch I mean you just

146
00:08:27,980 --> 00:08:28,880
 can't bear it

147
00:08:28,880 --> 00:08:36,020
 right but when you when you when you train and you can bear

148
00:08:36,020 --> 00:08:37,040
 with it and you

149
00:08:37,040 --> 00:08:41,540
 say wanting wanting itching itching and that's where

150
00:08:41,540 --> 00:08:44,840
 concentration begins you

151
00:08:44,840 --> 00:08:47,950
 ruin your concentration the moment it becomes physical

152
00:08:47,950 --> 00:08:51,040
 scratching your face

153
00:08:51,040 --> 00:08:57,530
 so meditators begin to become quite careful with their

154
00:08:57,530 --> 00:08:59,640
 actions when they

155
00:08:59,640 --> 00:09:05,340
 walk Masi says they Masi Sayadaw says they they walk like

156
00:09:05,340 --> 00:09:07,760
 sick people looks

157
00:09:07,760 --> 00:09:13,360
 like kind of like a sick ward here because everyone's

158
00:09:13,360 --> 00:09:14,280
 moving around slowly

159
00:09:14,280 --> 00:09:23,280
 and very neutral expressions on their faces they're being

160
00:09:23,280 --> 00:09:26,160
 very careful because

161
00:09:26,160 --> 00:09:32,240
 they begin to see the the disturbance that's caused by not

162
00:09:32,240 --> 00:09:33,840
 being mindful

163
00:09:33,840 --> 00:09:41,140
 that's caused by acting especially unmindfully and so we

164
00:09:41,140 --> 00:09:43,680
 should think of

165
00:09:43,680 --> 00:09:46,770
 this in both ways on the one hand we always think about

166
00:09:46,770 --> 00:09:48,720
 these big big issues

167
00:09:48,720 --> 00:09:54,560
 of not killing not stealing of course not committing

168
00:09:54,560 --> 00:09:56,840
 adultery but as

169
00:09:56,840 --> 00:10:00,280
 meditators we think of it on a micro scale as well with

170
00:10:00,280 --> 00:10:01,800
 everything it's just

171
00:10:01,800 --> 00:10:05,320
 like speech right with everything you say it should be

172
00:10:05,320 --> 00:10:08,800
 mindful right it's more

173
00:10:08,800 --> 00:10:13,310
 difficult to be mindful speaking it's possible but with

174
00:10:13,310 --> 00:10:14,960
 acting it's much more

175
00:10:14,960 --> 00:10:18,420
 doable and much more a part of our practice right actually

176
00:10:18,420 --> 00:10:19,440
 the Buddha said

177
00:10:19,440 --> 00:10:22,320
 with both if you read the satipatanasuta maybe we'll go

178
00:10:22,320 --> 00:10:23,680
 through the satipatanasuta

179
00:10:23,680 --> 00:10:29,480
 eventually he said when you're speaking or when you're

180
00:10:29,480 --> 00:10:31,520
 keeping silent you should

181
00:10:31,520 --> 00:10:37,440
 be fully comprehending walking forward walking back

182
00:10:37,440 --> 00:10:38,780
 reaching extending your

183
00:10:38,780 --> 00:10:46,370
 arm flexing your arm eating drinking urinating defecating

184
00:10:46,370 --> 00:10:47,600
 everything should

185
00:10:47,600 --> 00:10:50,440
 be done mindfully

186
00:10:50,440 --> 00:11:00,190
 and then you see this difference where the the defilements

187
00:11:00,190 --> 00:11:01,880
 the unwholesome the

188
00:11:01,880 --> 00:11:06,080
 problematic intentions they don't break through to become

189
00:11:06,080 --> 00:11:08,440
 actions and

190
00:11:08,440 --> 00:11:10,920
 technically that anytime they do break through it's wrong

191
00:11:10,920 --> 00:11:13,720
 action I mean not

192
00:11:13,720 --> 00:11:16,300
 actually not technically according to the suttas it doesn't

193
00:11:16,300 --> 00:11:17,080
 go into that much

194
00:11:17,080 --> 00:11:20,720
 detail but from a perspective of the you know the core

195
00:11:20,720 --> 00:11:22,240
 teaching of the Buddha in

196
00:11:22,240 --> 00:11:26,260
 an ultimate sense it's quite clear that it becomes what we

197
00:11:26,260 --> 00:11:27,320
 might call wrong

198
00:11:27,320 --> 00:11:34,080
 action it's called the kamakilesa I think where the defiled

199
00:11:34,080 --> 00:11:36,640
 actions or actions

200
00:11:36,640 --> 00:11:43,320
 based on the found it's the most coarse type of defilement

201
00:11:43,320 --> 00:11:50,400
 so quite simple not

202
00:11:50,400 --> 00:11:59,230
 easy but but simple that's the teaching on right action do

203
00:11:59,230 --> 00:12:02,160
 we have any questions

204
00:12:05,280 --> 00:12:08,820
 again questions are all to be asked to our website and I

205
00:12:08,820 --> 00:12:09,960
 understand that means

206
00:12:09,960 --> 00:12:16,640
 you have to log in but it's a small price to pay can you

207
00:12:16,640 --> 00:12:19,160
 progress by only

208
00:12:19,160 --> 00:12:23,930
 practicing insight meditation no chanting meta etc well

209
00:12:23,930 --> 00:12:25,480
 progress is a

210
00:12:25,480 --> 00:12:32,980
 fairly vague word so we have useful and necessary and there

211
00:12:32,980 --> 00:12:34,640
 are a lot of things

212
00:12:34,640 --> 00:12:37,560
 that are useful there's not so many things that are

213
00:12:37,560 --> 00:12:43,520
 necessary so if your

214
00:12:43,520 --> 00:12:48,410
 question were do you need to practice insight meditation to

215
00:12:48,410 --> 00:12:49,720
 progress or can

216
00:12:49,720 --> 00:12:53,000
 you do it simply by chanting and doing metta then the

217
00:12:53,000 --> 00:12:56,480
 answer is no or no the

218
00:12:56,480 --> 00:13:01,730
 answer is you do need to practice insight meditation can

219
00:13:01,730 --> 00:13:03,040
 chanting and metta help

220
00:13:03,040 --> 00:13:09,000
 do they help yes they are useful helpful they can be there

221
00:13:09,000 --> 00:13:09,960
 are cases there are

222
00:13:09,960 --> 00:13:13,080
 potential problems with like chanting for example you can

223
00:13:13,080 --> 00:13:14,120
 get caught up in it

224
00:13:14,120 --> 00:13:17,420
 and it can actually become a distraction metta not so much

225
00:13:17,420 --> 00:13:18,280
 if you're actually

226
00:13:18,280 --> 00:13:22,450
 practicing metta it's pretty much always useful but if you

227
00:13:22,450 --> 00:13:24,080
 get caught up in metta

228
00:13:24,080 --> 00:13:28,320
 it can sidetrack you it's true because you take time away

229
00:13:28,320 --> 00:13:29,840
 from doing insight

230
00:13:29,840 --> 00:13:33,270
 meditation that should be a little more specific with your

231
00:13:33,270 --> 00:13:34,200
 question hopefully

232
00:13:34,200 --> 00:13:39,520
 that answered it what is Buddhism's view on playing video

233
00:13:39,520 --> 00:13:41,680
 games well there's

234
00:13:41,680 --> 00:13:44,700
 nothing in the scriptures about playing video games I've

235
00:13:44,700 --> 00:13:45,740
 answered this sort of

236
00:13:45,740 --> 00:13:48,560
 question before I mean the question of when you kill

237
00:13:48,560 --> 00:13:49,800
 someone we used to play

238
00:13:49,800 --> 00:13:53,480
 when I was young doom this game where you it's really

239
00:13:53,480 --> 00:13:57,120
 pretty awful and lots of

240
00:13:57,120 --> 00:14:03,500
 games we played so it's not killing it's an elaborate means

241
00:14:03,500 --> 00:14:05,880
 of challenging your

242
00:14:05,880 --> 00:14:12,720
 your reflexes your hand eye coordination your ability to

243
00:14:12,720 --> 00:14:14,200
 process so there's

244
00:14:14,200 --> 00:14:17,990
 actually some potentially good states I would say involved

245
00:14:17,990 --> 00:14:19,000
 mundane good states

246
00:14:19,000 --> 00:14:22,080
 not really Buddhist but mundane in the sense it's the kind

247
00:14:22,080 --> 00:14:22,520
 of thing that could

248
00:14:22,520 --> 00:14:26,540
 probably help you think better you know critical thinking

249
00:14:26,540 --> 00:14:27,840
 skills ability to

250
00:14:27,840 --> 00:14:32,960
 strategize and that kind of thing but the biggest problem

251
00:14:32,960 --> 00:14:34,560
 with video games is

252
00:14:34,560 --> 00:14:39,280
 not the violence the biggest problem in my mind anyway the

253
00:14:39,280 --> 00:14:40,720
 biggest problem is of

254
00:14:40,720 --> 00:14:45,880
 course the attachment it's this it's this pleasure you know

255
00:14:45,880 --> 00:14:46,960
 it's like classical

256
00:14:46,960 --> 00:14:50,500
 conditioning or operant conditioning you can't remember you

257
00:14:50,500 --> 00:14:51,840
 you you do something

258
00:14:51,840 --> 00:14:55,080
 you get what you wanted it makes you pleasure and so you

259
00:14:55,080 --> 00:14:55,720
 you know you keep

260
00:14:55,720 --> 00:14:59,190
 practicing keep playing the reason we play games is because

261
00:14:59,190 --> 00:14:59,840
 of these moments

262
00:14:59,840 --> 00:15:03,070
 of success that give us such a rush it's really just a

263
00:15:03,070 --> 00:15:04,760
 chemical addiction or

264
00:15:04,760 --> 00:15:10,800
 addicted to these opioids or what are they called the

265
00:15:10,800 --> 00:15:12,160
 chemicals in our brain

266
00:15:12,160 --> 00:15:15,960
 were addicted to them so we we've found a way using our

267
00:15:15,960 --> 00:15:17,120
 computer to stimulate

268
00:15:17,120 --> 00:15:21,620
 our brains to stimulate these chemicals but it's still just

269
00:15:21,620 --> 00:15:23,000
 chemical and so it's

270
00:15:23,000 --> 00:15:26,230
 addictive and if you know anything about the addiction

271
00:15:26,230 --> 00:15:27,800
 cycle it becomes less and

272
00:15:27,800 --> 00:15:33,680
 less satisfying and so you're ultimately you're constantly

273
00:15:33,680 --> 00:15:36,880
 really met with with

274
00:15:36,880 --> 00:15:41,880
 dissatisfaction I wouldn't worry about like I mean I wouldn

275
00:15:41,880 --> 00:15:42,400
't worry too much

276
00:15:42,400 --> 00:15:45,180
 about it especially if you're as a layperson it's not the

277
00:15:45,180 --> 00:15:45,680
 kind of thing

278
00:15:45,680 --> 00:15:48,830
 that's going to send you to hell if you played these games

279
00:15:48,830 --> 00:15:49,920
 and you killed all

280
00:15:49,920 --> 00:15:53,350
 these people is that gonna send you down no no I mean at

281
00:15:53,350 --> 00:15:54,680
 worst it just keeps you

282
00:15:54,680 --> 00:15:57,890
 being born and if it's not worst if you really get obsessed

283
00:15:57,890 --> 00:15:58,840
 you can be born as a

284
00:15:58,840 --> 00:16:02,870
 hungry ghost because of the amount the intense addiction

285
00:16:02,870 --> 00:16:05,080
 but generally speaking

286
00:16:05,080 --> 00:16:09,400
 it's not the worst I mean we talk about things like sexual

287
00:16:09,400 --> 00:16:12,440
 activity and and you

288
00:16:12,440 --> 00:16:15,830
 know entertainment these are not these are not things that

289
00:16:15,830 --> 00:16:17,320
 stop you from

290
00:16:17,320 --> 00:16:20,240
 attaining enlightenment they just hinder it they make it

291
00:16:20,240 --> 00:16:21,100
 slower and more

292
00:16:21,100 --> 00:16:26,090
 difficult but they're not wrong action in that broader

293
00:16:26,090 --> 00:16:27,880
 sense of really blocking

294
00:16:27,880 --> 00:16:31,520
 you from the practice so killing stealing and cheating

295
00:16:31,520 --> 00:16:32,280
 these are things

296
00:16:32,280 --> 00:16:37,120
 that will block you playing video games well it's not going

297
00:16:37,120 --> 00:16:39,880
 to help you certainly

298
00:16:39,880 --> 00:16:46,980
 why not mix different Buddhist traditions because they

299
00:16:46,980 --> 00:16:47,720
 teach different

300
00:16:47,720 --> 00:16:53,050
 things I'll often teach contradictory things that can lead

301
00:16:53,050 --> 00:16:56,280
 to doubt confusion

302
00:16:56,280 --> 00:17:00,070
 it's not to say you don't always it's not about mixing

303
00:17:00,070 --> 00:17:01,520
 traditions it's about

304
00:17:01,520 --> 00:17:04,980
 finding the truth if this tradition teaches something that

305
00:17:04,980 --> 00:17:05,480
's good and

306
00:17:05,480 --> 00:17:10,360
 beneficial then then good so it's about finding what's

307
00:17:10,360 --> 00:17:13,960
 right and true that's

308
00:17:13,960 --> 00:17:16,620
 not the whole answer that that's part of it but the other

309
00:17:16,620 --> 00:17:18,120
 thing is there are

310
00:17:18,120 --> 00:17:23,460
 different ways to reach the same goal but having two

311
00:17:23,460 --> 00:17:26,140
 different regimens can

312
00:17:26,140 --> 00:17:31,340
 conflict just in a functional sense right like our our I

313
00:17:31,340 --> 00:17:32,280
 follow the Mahasi

314
00:17:32,280 --> 00:17:35,990
 lineage but if you went to Burma the Mahasi people would

315
00:17:35,990 --> 00:17:36,920
 say no you don't

316
00:17:36,920 --> 00:17:40,410
 because well some might because they're quite sticklers for

317
00:17:40,410 --> 00:17:41,360
 a very specific way

318
00:17:41,360 --> 00:17:46,750
 of doing this type of meditation and we do it differently

319
00:17:46,750 --> 00:17:48,560
 so if you if you come

320
00:17:48,560 --> 00:17:51,140
 to our center you do it this way if you go to the Mahasi

321
00:17:51,140 --> 00:17:52,520
 Center you know you'll

322
00:17:52,520 --> 00:17:58,440
 do it a different way and if you try and do it both ways

323
00:17:58,440 --> 00:18:00,000
 within the Mahasi

324
00:18:00,000 --> 00:18:05,090
 tradition not that big of a deal but say if you then went

325
00:18:05,090 --> 00:18:06,760
 and went to a center

326
00:18:06,760 --> 00:18:14,600
 where they did anapanasati or something like that it you

327
00:18:14,600 --> 00:18:15,480
 know it it

328
00:18:15,480 --> 00:18:17,960
 complicates things because you're trying to do it two

329
00:18:17,960 --> 00:18:20,240
 different ways at once and

330
00:18:20,240 --> 00:18:24,860
 I think I would criticize people who like to try different

331
00:18:24,860 --> 00:18:27,120
 ways and it usually I

332
00:18:27,120 --> 00:18:30,670
 would argue because they simply don't want to put out the

333
00:18:30,670 --> 00:18:32,080
 effort and they it's

334
00:18:32,080 --> 00:18:35,060
 hard to put out the effort in one way so you look for

335
00:18:35,060 --> 00:18:36,640
 something new that's gonna

336
00:18:36,640 --> 00:18:40,380
 make it easy and it's just this classical avoidance which

337
00:18:40,380 --> 00:18:41,480
 is of course

338
00:18:41,480 --> 00:18:46,260
 very common in spirituality as well as ordinary life so

339
00:18:46,260 --> 00:18:48,000
 finding a tradition and

340
00:18:48,000 --> 00:18:52,900
 sticking to it well sticking to it is quite different from

341
00:18:52,900 --> 00:18:55,800
 finding and you can

342
00:18:55,800 --> 00:18:58,250
 run around and and try all sorts of different ways it's not

343
00:18:58,250 --> 00:18:58,920
 going to help

344
00:18:58,920 --> 00:19:02,500
 you and I would argue that it hinders you yeah you get lots

345
00:19:02,500 --> 00:19:03,040
 of different

346
00:19:03,040 --> 00:19:05,770
 skills from lots of different traditions and people think

347
00:19:05,770 --> 00:19:07,520
 that's a good thing but

348
00:19:07,520 --> 00:19:11,760
 you never become a master of any one skill

349
00:19:16,920 --> 00:19:26,720
 of hair loss it hurts you I mean you don't like it if you

350
00:19:26,720 --> 00:19:27,400
 don't like it then

351
00:19:27,400 --> 00:19:32,240
 you'd say disliking disliking again I hope you've read my

352
00:19:32,240 --> 00:19:33,280
 my booklet if you

353
00:19:33,280 --> 00:19:35,440
 haven't read my booklet I would encourage you to read it

354
00:19:35,440 --> 00:19:38,280
 because it

355
00:19:38,280 --> 00:19:41,040
 might help you deal with this but this isn't hurting you

356
00:19:41,040 --> 00:19:42,200
 this is a disliking

357
00:19:42,200 --> 00:19:46,060
 the hair loss isn't hurting you it's hurting you as you don

358
00:19:46,060 --> 00:19:46,800
't like it I

359
00:19:46,800 --> 00:19:49,860
 assume unless there's something weird painful that I'm not

360
00:19:49,860 --> 00:19:50,760
 understanding in

361
00:19:50,760 --> 00:19:53,880
 which case it's just pain and you would say pain pain

362
00:19:53,880 --> 00:19:57,800
 trying to be mindful of it

363
00:19:57,800 --> 00:20:04,670
 okay that's all the questions for tonight thank you all for

364
00:20:04,670 --> 00:20:05,960
 tuning in

