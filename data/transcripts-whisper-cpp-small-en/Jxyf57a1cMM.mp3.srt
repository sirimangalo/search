1
00:00:00,000 --> 00:00:03,600
 "In order to follow Buddhism, I must not cling to happiness

2
00:00:03,600 --> 00:00:05,940
 or people that give me happiness.

3
00:00:05,940 --> 00:00:09,340
 Does this mean that I shouldn't make friends and/or stop

4
00:00:09,340 --> 00:00:11,300
 associating with my friends as

5
00:00:11,300 --> 00:00:14,860
 I enjoy being around my friends and it gives me happiness?"

6
00:00:14,860 --> 00:00:20,860
 Okay, the word happiness, again the ambiguity.

7
00:00:20,860 --> 00:00:24,420
 Buddhism says that none of that brings you happiness.

8
00:00:24,420 --> 00:00:27,230
 And that's the unfortunate thing, that if these things gave

9
00:00:27,230 --> 00:00:29,000
 you happiness, true happiness,

10
00:00:29,000 --> 00:00:31,840
 then you should cling to them and hold on to them.

11
00:00:31,840 --> 00:00:34,530
 But because they don't give you happiness, clinging to them

12
00:00:34,530 --> 00:00:35,840
 is a cause of suffering.

13
00:00:35,840 --> 00:00:37,240
 That's what Buddhism says.

14
00:00:37,240 --> 00:00:41,960
 Buddhism doesn't say "must not, should not."

15
00:00:41,960 --> 00:00:44,880
 Buddhism says "if then."

16
00:00:44,880 --> 00:00:47,880
 So it's much more powerful.

17
00:00:47,880 --> 00:00:51,610
 It's not trying to convince you or coerce you into being

18
00:00:51,610 --> 00:00:53,160
 one way or another.

19
00:00:53,160 --> 00:00:56,560
 It's telling you the truth.

20
00:00:56,560 --> 00:01:00,980
 And it's up to you to realize the truth, appreciate it, or

21
00:01:00,980 --> 00:01:02,360
 deny the truth.

22
00:01:02,360 --> 00:01:11,200
 So if you cling to people, the idea that it makes you happy

23
00:01:11,200 --> 00:01:14,080
 is an illusion.

24
00:01:14,080 --> 00:01:17,510
 And as you practice meditation, you'll realize that it's

25
00:01:17,510 --> 00:01:19,520
 not actually making you happy.

26
00:01:19,520 --> 00:01:23,480
 It's actually just giving you some pleasure, which entices

27
00:01:23,480 --> 00:01:25,200
 you to cling stronger, which

28
00:01:25,200 --> 00:01:30,280
 eventually leads to repeated distress and suffering when

29
00:01:30,280 --> 00:01:32,840
 every time you don't get what

30
00:01:32,840 --> 00:01:38,120
 you want or every time you're forced to strive for the

31
00:01:38,120 --> 00:01:40,800
 pleasant states again.

32
00:01:40,800 --> 00:01:43,920
 So the enjoying being around your friends, except where it

33
00:01:43,920 --> 00:01:46,120
's involved with wholesomeness.

34
00:01:46,120 --> 00:01:49,370
 There's the case where you're helping each other, where you

35
00:01:49,370 --> 00:01:51,200
're appreciating each other,

36
00:01:51,200 --> 00:01:54,670
 where you're compassionate, and so on, where you're able to

37
00:01:54,670 --> 00:01:56,040
 support each other.

38
00:01:56,040 --> 00:01:58,520
 That is positive.

39
00:01:58,520 --> 00:02:03,010
 But the enjoyment, the liking or the attachment to pleas

40
00:02:03,010 --> 00:02:06,040
urable states is unwholesome and will

41
00:02:06,040 --> 00:02:08,250
 lead you to suffering when you have to leave your friends,

42
00:02:08,250 --> 00:02:09,800
 when you're surrounded by unpleasant

43
00:02:09,800 --> 00:02:11,640
 people, etc., etc.

44
00:02:11,640 --> 00:02:13,360
 It means you're vulnerable.

45
00:02:13,360 --> 00:02:17,380
 You're always vulnerable to change, and therefore suffering

46
00:02:17,380 --> 00:02:19,440
 is in your future because of that

47
00:02:19,440 --> 00:02:20,080
 attachment.

48
00:02:20,080 --> 00:02:45,700
 [

