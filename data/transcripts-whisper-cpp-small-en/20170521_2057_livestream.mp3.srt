1
00:00:00,000 --> 00:00:10,640
 Good evening everyone.

2
00:00:10,640 --> 00:00:18,280
 Welcome to our evening Dhamma session.

3
00:00:18,280 --> 00:00:23,800
 I have our local audience.

4
00:00:23,800 --> 00:00:26,680
 I call this the Sangha.

5
00:00:26,680 --> 00:00:30,640
 This is our community. We live together.

6
00:00:30,640 --> 00:00:33,880
 We learn together.

7
00:00:33,880 --> 00:00:42,500
 We have our virtual community, which is in many ways just

8
00:00:42,500 --> 00:00:44,800
 as much a community.

9
00:00:44,800 --> 00:00:56,680
 All of those of you on Second Life.

10
00:00:56,680 --> 00:01:01,370
 We have audio streaming for those of you who managed to

11
00:01:01,370 --> 00:01:02,360
 find it.

12
00:01:02,360 --> 00:01:11,760
 And of course we're live on YouTube, which is I think our

13
00:01:11,760 --> 00:01:15,440
 largest audience.

14
00:01:15,440 --> 00:01:23,720
 So I was asked today about Buddhism.

15
00:01:23,720 --> 00:01:25,160
 What is Buddhism?

16
00:01:25,160 --> 00:01:30,160
 Want to learn something about Buddhism?

17
00:01:30,160 --> 00:01:35,920
 It's really a hard thing to teach.

18
00:01:35,920 --> 00:01:49,520
 Buddhism is a real patchwork of many different things.

19
00:01:49,520 --> 00:01:53,880
 So much that it really does appear to be an attempt to

20
00:01:53,880 --> 00:01:59,840
 describe reality or the universe.

21
00:01:59,840 --> 00:02:02,880
 In all of its many forms.

22
00:02:02,880 --> 00:02:04,400
 Not just one form.

23
00:02:04,400 --> 00:02:06,960
 Not just the form that I talk about.

24
00:02:06,960 --> 00:02:16,390
 A meditation point of view, but cosmology, society,

25
00:02:16,390 --> 00:02:26,080
 sociology, even biology to some extent.

26
00:02:26,080 --> 00:02:35,360
 What we can do is try to describe Buddhism.

27
00:02:35,360 --> 00:02:41,760
 Because like for example when we study medicine.

28
00:02:41,760 --> 00:02:43,840
 So much to study.

29
00:02:43,840 --> 00:02:48,540
 Each disease is unique and if you start talking about the

30
00:02:48,540 --> 00:02:50,400
 various diseases.

31
00:02:50,400 --> 00:02:52,000
 It's very hard to get.

32
00:02:52,000 --> 00:02:57,600
 You see how vast is the subject you're dealing with.

33
00:02:57,600 --> 00:03:03,280
 If you try to explain what does a doctor do.

34
00:03:03,280 --> 00:03:05,200
 What is it like?

35
00:03:05,200 --> 00:03:06,200
 What is medicine?

36
00:03:06,200 --> 00:03:12,960
 What is it like to treat a sickness well?

37
00:03:12,960 --> 00:03:16,160
 It's a sort of subject that is quite complex.

38
00:03:16,160 --> 00:03:22,580
 Because each disease is different and there's many levels

39
00:03:22,580 --> 00:03:24,480
 of treatment.

40
00:03:24,480 --> 00:03:31,390
 I think Buddhism is the same but you still get an idea that

41
00:03:31,390 --> 00:03:34,000
 like a physician.

42
00:03:34,000 --> 00:03:38,360
 The Buddha was a sort of a specific sort of individual and

43
00:03:38,360 --> 00:03:40,960
 his teachings were of a specific

44
00:03:40,960 --> 00:03:44,960
 sort.

45
00:03:44,960 --> 00:03:55,750
 So we get for example after the Buddha became enlightened

46
00:03:55,750 --> 00:03:59,200
 he went to a forest.

47
00:03:59,200 --> 00:04:06,240
 Sort of a refuge of sorts.

48
00:04:06,240 --> 00:04:10,560
 A national park maybe equivalent.

49
00:04:10,560 --> 00:04:12,360
 Where there were five ascetics staying.

50
00:04:12,360 --> 00:04:16,940
 Five ascetics who had of course practiced with him when he

51
00:04:16,940 --> 00:04:19,120
 was torturing himself.

52
00:04:19,120 --> 00:04:21,630
 And they had abandoned him because he stopped torturing

53
00:04:21,630 --> 00:04:22,240
 himself.

54
00:04:22,240 --> 00:04:27,680
 And he found what we call the middle way.

55
00:04:27,680 --> 00:04:30,390
 So the Buddha's teaching is often described as the middle

56
00:04:30,390 --> 00:04:30,800
 way.

57
00:04:30,800 --> 00:04:36,430
 I don't think that's particularly insightful or all encomp

58
00:04:36,430 --> 00:04:37,520
assing.

59
00:04:37,520 --> 00:04:38,520
 But it is.

60
00:04:38,520 --> 00:04:41,840
 It does give some insight.

61
00:04:41,840 --> 00:04:45,810
 And it is a very common way of describing the Buddha's

62
00:04:45,810 --> 00:04:46,880
 teaching.

63
00:04:46,880 --> 00:04:49,000
 The Buddha himself right.

64
00:04:49,000 --> 00:04:52,160
 He said he used to torture himself.

65
00:04:52,160 --> 00:04:59,360
 He used to indulge, enjoy life, enjoy pleasure.

66
00:04:59,360 --> 00:05:03,960
 And he realized that neither one made him happy.

67
00:05:03,960 --> 00:05:07,920
 Enjoying life or enjoying sensual pleasures didn't actually

68
00:05:07,920 --> 00:05:09,120
 make him happy.

69
00:05:09,120 --> 00:05:13,560
 Didn't prepare him for old age, sickness, death.

70
00:05:13,560 --> 00:05:16,320
 It didn't make him a better person.

71
00:05:16,320 --> 00:05:18,920
 Didn't even make him a happier person.

72
00:05:18,920 --> 00:05:21,600
 Just made him a greedy person.

73
00:05:21,600 --> 00:05:26,160
 A person who wants more and more.

74
00:05:26,160 --> 00:05:31,160
 And torturing himself also didn't do much.

75
00:05:31,160 --> 00:05:34,640
 Like today how we try to sometimes do the right thing or be

76
00:05:34,640 --> 00:05:38,840
 a hard working individual.

77
00:05:38,840 --> 00:05:42,060
 Some people go out and work or some stay at home and take

78
00:05:42,060 --> 00:05:43,280
 care of children.

79
00:05:43,280 --> 00:05:48,600
 But they work really hard.

80
00:05:48,600 --> 00:05:51,430
 Like the Buddha some people go off and do religious

81
00:05:51,430 --> 00:05:53,360
 practice and push themselves and

82
00:05:53,360 --> 00:05:58,760
 force their minds and force themselves to give up.

83
00:05:58,760 --> 00:06:04,040
 To suppress their defilements.

84
00:06:04,040 --> 00:06:06,520
 Trying to get rid of them.

85
00:06:06,520 --> 00:06:08,410
 Meaning that if you force them away or if you push them

86
00:06:08,410 --> 00:06:09,440
 hard enough they'll just go

87
00:06:09,440 --> 00:06:10,440
 away.

88
00:06:10,440 --> 00:06:14,280
 I'm going to realize this wasn't the case either.

89
00:06:14,280 --> 00:06:17,040
 You could starve yourself to death.

90
00:06:17,040 --> 00:06:25,560
 You still wouldn't be free from suffering.

91
00:06:25,560 --> 00:06:28,640
 So he found the middle way.

92
00:06:28,640 --> 00:06:38,160
 But it's not how I am.

93
00:06:38,160 --> 00:06:39,160
 But after the five ascetics became enlightened.

94
00:06:39,160 --> 00:06:42,160
 I have Asanji, one of the five.

95
00:06:42,160 --> 00:06:43,160
 I am not a Christian.

