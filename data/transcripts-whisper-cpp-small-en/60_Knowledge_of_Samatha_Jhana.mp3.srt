1
00:00:00,000 --> 00:00:02,840
 Knowledge of Samatha Jhanas.

2
00:00:02,840 --> 00:00:04,360
 Question.

3
00:00:04,360 --> 00:00:07,400
 Would it be helpful to experience the Jhanas

4
00:00:07,400 --> 00:00:12,940
 so that you can help a meditator to move beyond blocks?

5
00:00:12,940 --> 00:00:14,280
 Answer.

6
00:00:14,280 --> 00:00:17,780
 I think the underlying source of questions like this

7
00:00:17,780 --> 00:00:21,320
 is that people really want to learn the Jhanas.

8
00:00:21,320 --> 00:00:26,080
 Just the thought of deep states of bliss and peace sound

9
00:00:26,080 --> 00:00:27,520
 exciting,

10
00:00:27,520 --> 00:00:33,000
 so there arises a sort of desire for such states.

11
00:00:33,000 --> 00:00:37,560
 When you more clearly see the nature of ultimate reality,

12
00:00:37,560 --> 00:00:41,480
 such experiences become less exciting,

13
00:00:41,480 --> 00:00:46,000
 as they are subject to the characteristics of impermanence,

14
00:00:46,000 --> 00:00:48,760
 suffering, and non-self.

15
00:00:48,760 --> 00:00:53,730
 What you really need is an understanding of how reality

16
00:00:53,730 --> 00:00:54,880
 works.

17
00:00:54,880 --> 00:00:59,000
 The Jhanas are just another part of reality.

18
00:00:59,000 --> 00:01:04,080
 Jhanas are not anything mysterious or extraordinary.

19
00:01:04,080 --> 00:01:08,740
 They are a state where the mind is secluded from the hindr

20
00:01:08,740 --> 00:01:09,680
ances.

21
00:01:09,680 --> 00:01:12,280
 They are just mind states.

22
00:01:12,280 --> 00:01:16,160
 They can last for extended periods of time.

23
00:01:16,160 --> 00:01:19,360
 You can cultivate them in various ways,

24
00:01:19,360 --> 00:01:23,630
 and you can use them to develop any number of magical

25
00:01:23,630 --> 00:01:24,240
 powers,

26
00:01:24,240 --> 00:01:28,800
 like remembering past lives and so on.

27
00:01:28,800 --> 00:01:32,720
 But the best way to understand them is to understand the

28
00:01:32,720 --> 00:01:33,480
 reality

29
00:01:33,480 --> 00:01:37,280
 that underlies all experiences.

30
00:01:37,280 --> 00:01:41,400
 This is done through practicing the simple meditation

31
00:01:41,400 --> 00:01:44,360
 of the four foundations of mindfulness

32
00:01:44,360 --> 00:01:48,280
 to understand the building blocks of experience,

33
00:01:48,280 --> 00:01:53,480
 because once you understand what makes up experience,

34
00:01:53,480 --> 00:01:58,240
 how experience works in terms of cause and effect,

35
00:01:58,240 --> 00:02:02,840
 then there is nothing mysterious or hard to understand

36
00:02:02,840 --> 00:02:07,760
 or really at all exciting about the Jhanas.

37
00:02:07,760 --> 00:02:10,880
 So to directly answer your question,

38
00:02:10,880 --> 00:02:15,280
 I do not think it would be at all helpful.

39
00:02:15,280 --> 00:02:18,720
 Direct experience of how the mind works

40
00:02:18,720 --> 00:02:22,560
 is far more useful than cultivating the Samatha Jhanas

41
00:02:22,560 --> 00:02:25,960
 in order to help someone who experienced them

42
00:02:25,960 --> 00:02:29,520
 and was having a block with them.

43
00:02:29,520 --> 00:02:33,890
 Attachment to peaceful and tranquil states of mind is

44
00:02:33,890 --> 00:02:34,800
 common,

45
00:02:34,800 --> 00:02:38,840
 and that is quite difficult to train someone out of.

46
00:02:38,840 --> 00:02:41,760
 It can be difficult to wean a person

47
00:02:41,760 --> 00:02:46,560
 who has practiced Samatha meditation intensively off of it,

48
00:02:46,560 --> 00:02:50,890
 especially if they are coming from a different religious

49
00:02:50,890 --> 00:02:51,720
 tradition.

50
00:02:51,720 --> 00:02:55,640
 For example, if they had practiced meditation

51
00:02:55,640 --> 00:03:01,000
 in the Hindu tradition, which has different goals and ideas

52
00:03:01,000 --> 00:03:01,000
,

53
00:03:01,000 --> 00:03:03,640
 or even Buddhist meditation traditions

54
00:03:03,640 --> 00:03:07,000
 that emphasize the practice of tranquility

55
00:03:07,000 --> 00:03:10,600
 and the states of calm that it produces,

56
00:03:10,600 --> 00:03:14,120
 which can be sometimes difficult to let go of,

57
00:03:14,120 --> 00:03:17,240
 weaning someone off of their attachment

58
00:03:17,240 --> 00:03:21,600
 to pleasant states of mind has nothing to do with knowledge

59
00:03:21,600 --> 00:03:23,240
 of the Jhanas.

60
00:03:23,240 --> 00:03:27,880
 It has to do with knowledge of attachments and views,

61
00:03:27,880 --> 00:03:31,400
 views about what is true happiness,

62
00:03:31,400 --> 00:03:33,880
 views about what is self,

63
00:03:33,880 --> 00:03:38,000
 and views about control and letting go.

64
00:03:38,000 --> 00:03:41,400
 It has to do with views about impermanence,

65
00:03:41,400 --> 00:03:43,640
 and as the Buddha said,

66
00:03:43,640 --> 00:03:48,320
 the Jhanas and any kind of pleasant or even neutral feeling

67
00:03:48,320 --> 00:03:51,000
 they bring is not permanent.

68
00:03:51,000 --> 00:03:54,720
 Understanding these things does not require you

69
00:03:54,720 --> 00:03:57,680
 to have attained the Jhanas yourself.

