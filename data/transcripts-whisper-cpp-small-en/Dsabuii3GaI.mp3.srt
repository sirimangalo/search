1
00:00:00,000 --> 00:00:03,830
 Somebody here says, very simple question, can someone who

2
00:00:03,830 --> 00:00:07,000
 commits murder achieve enlightenment in future lives?

3
00:00:07,000 --> 00:00:11,570
 Making a change, do you have enlightenment on this? Who's

4
00:00:11,570 --> 00:00:13,000
 going to pick this one up, Owen?

5
00:00:13,000 --> 00:00:16,000
 [LAUGHTER]

6
00:00:16,000 --> 00:00:18,000
 Owen from the temperature.

7
00:00:18,000 --> 00:00:25,000
 Did you not do a video or something recently on this one

8
00:00:25,000 --> 00:00:29,000
 about the guy trying to get a thousand murders?

9
00:00:29,000 --> 00:00:33,000
 And then he came across the Buddha?

10
00:00:33,000 --> 00:00:37,040
 Oh, that's good. That's good. Oh yeah, you mean that video,

11
00:00:37,040 --> 00:00:39,000
 the movie that I put the piece together?

12
00:00:39,000 --> 00:00:44,530
 I think it was brought up a while back where there was a

13
00:00:44,530 --> 00:00:50,210
 guy who was basically told to get the fingers of a thousand

14
00:00:50,210 --> 00:00:51,000
 people.

15
00:00:51,000 --> 00:00:53,000
 I mean, you don't know the story?

16
00:00:53,000 --> 00:00:56,000
 You know what I do, but... [LAUGHTER]

17
00:00:56,000 --> 00:00:59,000
 Not word for word.

18
00:00:59,000 --> 00:01:02,760
 But like you know who that's talking about? Are you not

19
00:01:02,760 --> 00:01:07,000
 familiar with the Majimini Kaisuta, the Angulimala Suta?

20
00:01:07,000 --> 00:01:10,000
 No, not...

21
00:01:10,000 --> 00:01:11,690
 You know what I should do? I should find a way to post the

22
00:01:11,690 --> 00:01:17,000
 movie. There's this movie, a Thai movie, called Angulimala.

23
00:01:17,000 --> 00:01:19,240
 And it tells the story really well, and the ending is

24
00:01:19,240 --> 00:01:20,000
 really cool.

25
00:01:20,000 --> 00:01:23,310
 If you look up the video I did called Forgiveness and

26
00:01:23,310 --> 00:01:27,920
 Redemption in Buddhism, do you remember the Tiger Woods

27
00:01:27,920 --> 00:01:29,000
 scandal?

28
00:01:29,000 --> 00:01:31,000
 Everyone does, no?

29
00:01:31,000 --> 00:01:32,000
 No.

30
00:01:32,000 --> 00:01:34,000
 The worldwide thing, no?

31
00:01:34,000 --> 00:01:35,000
 Unfortunately.

32
00:01:35,000 --> 00:01:40,290
 And you remember how the Christians started saying stuff

33
00:01:40,290 --> 00:01:44,270
 like, "Well, if he was only Christian, Jesus would forgive

34
00:01:44,270 --> 00:01:45,000
 him?"

35
00:01:45,000 --> 00:01:48,000
 And they said, "There's no forgiveness in Buddhism?"

36
00:01:48,000 --> 00:01:50,000
 Anyway, it's kind of an American thing.

37
00:01:50,000 --> 00:01:53,000
 Maybe I start from America and you don't know this?

38
00:01:53,000 --> 00:01:54,960
 I'm kind of interested in Tiger Woods, because he's

39
00:01:54,960 --> 00:01:58,000
 supposed to be Buddhist, so it's an interesting topic.

40
00:01:58,000 --> 00:02:02,390
 And so there was even a Buddhist who was interviewed about

41
00:02:02,390 --> 00:02:06,000
 this, talking about...

42
00:02:06,000 --> 00:02:08,410
 They were trying to get him to argue with this, but instead

43
00:02:08,410 --> 00:02:12,570
 of arguing, he just said how sorts of things he thinks

44
00:02:12,570 --> 00:02:14,000
 Tiger Woods should do.

45
00:02:14,000 --> 00:02:17,780
 And so I put together this video, because Angulimala, the

46
00:02:17,780 --> 00:02:21,030
 story of Angulimala is the perfect response to this

47
00:02:21,030 --> 00:02:22,000
 question.

48
00:02:22,000 --> 00:02:25,930
 So look up that video, Forgiveness and Redemption in

49
00:02:25,930 --> 00:02:27,000
 Buddhism.

50
00:02:27,000 --> 00:02:30,830
 And you'll see I stitched it together from this movie and

51
00:02:30,830 --> 00:02:33,000
 put some subtitles on it.

52
00:02:33,000 --> 00:02:39,160
 And that part of the movie, if you watch the whole part, is

53
00:02:39,160 --> 00:02:41,000
 just perfect.

54
00:02:41,000 --> 00:02:43,920
 I mean, they did a really good job and it's sad that it

55
00:02:43,920 --> 00:02:46,000
 never became very famous movie.

56
00:02:46,000 --> 00:02:50,470
 I think that whole part, not just the parts that I put in,

57
00:02:50,470 --> 00:02:56,000
 where he meets the Buddha and what the Buddha says to him

58
00:02:56,000 --> 00:02:59,700
 and the result and the impact that it has on him, is just

59
00:02:59,700 --> 00:03:01,000
 very well done.

60
00:03:01,000 --> 00:03:06,280
 Especially if you know the story of Angulimala, because it

61
00:03:06,280 --> 00:03:09,000
's a famous Buddhist story.

62
00:03:09,000 --> 00:03:13,760
 So a little bit of detail here. Why is it possible that you

63
00:03:13,760 --> 00:03:17,000
 murder someone and then become enlightened?

64
00:03:17,000 --> 00:03:23,630
 It makes it more difficult, yeah. It defiles your mind, but

65
00:03:23,630 --> 00:03:28,000
 it depends on what other good qualities you have inside.

66
00:03:28,000 --> 00:03:30,510
 I'm trying to believe, not that a person who kills someone

67
00:03:30,510 --> 00:03:32,000
 could have good qualities inside.

68
00:03:32,000 --> 00:03:35,560
 But people can do things on the spur of the moment.

69
00:03:35,560 --> 00:03:38,000
 Obviously Angulimala didn't.

70
00:03:38,000 --> 00:03:41,900
 But that's the thing. He killed 999 people and he was still

71
00:03:41,900 --> 00:03:44,000
 able to become enlightened.

72
00:03:44,000 --> 00:03:47,270
 I think the key was he wasn't doing it out of hatred or

73
00:03:47,270 --> 00:03:48,000
 malice.

74
00:03:48,000 --> 00:03:50,540
 He was doing it out of misunderstanding and out of wrong

75
00:03:50,540 --> 00:03:51,000
 view.

76
00:03:51,000 --> 00:03:54,560
 So once he was able to change that wrong view, he was able

77
00:03:54,560 --> 00:03:58,000
 to progress quite quickly and become an Arahant.

78
00:03:58,000 --> 00:04:04,000
 But he suffered horribly for it. He was beaten.

79
00:04:04,000 --> 00:04:06,720
 What the Buddha said about it, they said, "How is it

80
00:04:06,720 --> 00:04:10,000
 possible he could become enlightened?"

81
00:04:10,000 --> 00:04:13,020
 It's in the Dhammapada. If you read his story, it's in the

82
00:04:13,020 --> 00:04:14,000
 Dhammapada commentary.

83
00:04:14,000 --> 00:04:17,960
 There's a book, Buddhist Legends, if you look up this

84
00:04:17,960 --> 00:04:21,000
 translation of the Dhammapada commentary,

85
00:04:21,000 --> 00:04:35,100
 where the Buddha says that by using good deeds, they make

86
00:04:35,100 --> 00:04:39,000
 their bad deeds insignificant.

87
00:04:39,000 --> 00:04:42,810
 So you have this very, very bad deed. But the deed of

88
00:04:42,810 --> 00:04:47,000
 becoming an Arahant is so much more profound

89
00:04:47,000 --> 00:04:52,600
 than even killing lots of people that it actually makes

90
00:04:52,600 --> 00:04:55,000
 even that deed insignificant.

91
00:04:55,000 --> 00:04:57,420
 Because they ask, "How is it possible that he didn't have

92
00:04:57,420 --> 00:05:00,000
 to go to hell and suffer for countless lifetimes?"

93
00:05:00,000 --> 00:05:05,240
 And the Buddha said, "If he hadn't become an Arahant, that

94
00:05:05,240 --> 00:05:08,000
's where he would have gone.

95
00:05:08,000 --> 00:05:11,210
 There was no hope for him." But because of his ability to

96
00:05:11,210 --> 00:05:13,000
 become an Arahant, which is incredibly surprising,

97
00:05:13,000 --> 00:05:18,000
 he only had to suffer torture in this one life.

98
00:05:18,000 --> 00:05:20,960
 He doesn't really say what happened to him. He probably was

99
00:05:20,960 --> 00:05:23,000
 beaten to death or something.

100
00:05:23,000 --> 00:05:26,350
 Who knows? Probably not. Probably it says in the comment

101
00:05:26,350 --> 00:05:28,000
aries that he set himself on fire.

102
00:05:28,000 --> 00:05:31,200
 A lot of the Arahants read stories about how they

103
00:05:31,200 --> 00:05:33,000
 spontaneously combusted.

104
00:05:33,000 --> 00:05:36,810
 Ananda has said to have done that, because they don't want

105
00:05:36,810 --> 00:05:38,000
 people to have to worry about them.

106
00:05:38,000 --> 00:05:41,160
 Because they know if they... It's funny to read an

107
00:05:41,160 --> 00:05:44,000
 interesting...

108
00:05:44,000 --> 00:05:51,830
 to compare to today's methods of today among Buddhist

109
00:05:51,830 --> 00:05:53,000
 circles.

110
00:05:53,000 --> 00:05:55,350
 Because what they said is they knew that if they didn't

111
00:05:55,350 --> 00:05:56,000
 spontaneous...

112
00:05:56,000 --> 00:05:59,140
 If they died and let people have their body, people would

113
00:05:59,140 --> 00:06:02,000
 just fight over their bone relics.

114
00:06:02,000 --> 00:06:05,200
 Fight over the body, and it would be... What they say is it

115
00:06:05,200 --> 00:06:07,000
 would be a burden to people.

116
00:06:07,000 --> 00:06:10,000
 Having to bury, having to have a funeral, and so on.

117
00:06:10,000 --> 00:06:15,000
 Ananda, he had two sets of relatives.

118
00:06:15,000 --> 00:06:17,000
 Actually, it's not quite true about the relics, is it?

119
00:06:17,000 --> 00:06:19,940
 He has two sets of relatives, and he knew that they'd be

120
00:06:19,940 --> 00:06:21,000
 fighting over his body.

121
00:06:21,000 --> 00:06:24,400
 So he went over the river, I think the Rohini River

122
00:06:24,400 --> 00:06:25,000
 probably,

123
00:06:25,000 --> 00:06:27,000
 where the Buddha went over, and he floated over the river.

124
00:06:27,000 --> 00:06:31,000
 He spontaneously combusted and made a determination that

125
00:06:31,000 --> 00:06:35,380
 his relics would distribute evenly on both sides of the

126
00:06:35,380 --> 00:06:36,000
 river.

127
00:06:36,000 --> 00:06:48,450
 So Ananda's bones were half to the... I don't know, I guess

128
00:06:48,450 --> 00:06:55,000
 it's the Sakyas and the Kolias, I guess.

129
00:06:55,000 --> 00:06:58,000
 Those are the Buddha's two sides of relatives.

130
00:06:58,000 --> 00:07:02,000
 Ananda was his cousin, so something like that.

131
00:07:02,000 --> 00:07:11,000
 Anyway, that's the story I heard, Ewa Mee Suta.

132
00:07:11,000 --> 00:07:13,630
 There's a question that I've seen this a couple of times

133
00:07:13,630 --> 00:07:14,000
 now, I think, about...

134
00:07:14,000 --> 00:07:17,000
 No, I'm sorry, we should stop there.

135
00:07:17,000 --> 00:07:19,370
 You know what I mean, to say on what I just... what I was

136
00:07:19,370 --> 00:07:22,000
 just asking? Murder, no?

137
00:07:22,000 --> 00:07:25,890
 If you kill someone... If you kill your parents, you can't

138
00:07:25,890 --> 00:07:28,000
 become enlightened.

139
00:07:28,000 --> 00:07:31,000
 Killing your parents is one of the five Garuka Mas.

140
00:07:31,000 --> 00:07:33,210
 If you kill your father, if you kill your mother, if you

141
00:07:33,210 --> 00:07:37,000
 kill an Arahant, if you hurt a Buddha,

142
00:07:37,000 --> 00:07:43,990
 or if you create a schism in the Buddhist Sangha, these are

143
00:07:43,990 --> 00:07:46,000
 the five Garuka Mas.

144
00:07:46,000 --> 00:07:51,930
 The result of which a person is unable to attain even Sotap

145
00:07:51,930 --> 00:07:54,000
ana in this life.

146
00:07:54,000 --> 00:08:02,830
 So, Devadatta is one of those people. Ajata Sattu is

147
00:08:02,830 --> 00:08:06,000
 another one.

148
00:08:06,000 --> 00:08:09,000
 Those kind of people cannot...

149
00:08:09,000 --> 00:08:13,860
 Mahasya Sayadaw makes a point here. He says, in that regard

150
00:08:13,860 --> 00:08:14,000
,

151
00:08:14,000 --> 00:08:20,310
 people who euthanize their parents are guilty of very, very

152
00:08:20,310 --> 00:08:22,000
 heinous crime.

153
00:08:22,000 --> 00:08:25,000
 Something to keep in mind from the Buddhist perspective.

154
00:08:25,000 --> 00:08:28,000
 But killing a person doesn't disqualify you.

155
00:08:28,000 --> 00:08:31,120
 It makes it much more difficult, and it makes your practice

156
00:08:31,120 --> 00:08:32,000
 much more unpleasant,

157
00:08:32,000 --> 00:08:36,000
 and it does corrupt your mind. It's a very, very bad thing.

158
00:08:36,000 --> 00:08:40,000
 But to say that a person... it's not difficult,

159
00:08:40,000 --> 00:08:43,560
 it's not unthinkable for a person to become at least a Sot

160
00:08:43,560 --> 00:08:47,000
apana or a Nisakhita Kami without too much trouble.

161
00:08:47,000 --> 00:08:52,000
 A lot of suffering, but in this life certainly doable,

162
00:08:52,000 --> 00:08:57,000
 depending on the other qualities of mind that they have.

