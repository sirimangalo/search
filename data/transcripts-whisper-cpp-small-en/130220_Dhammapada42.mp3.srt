1
00:00:00,000 --> 00:00:06,000
 Okay, welcome everyone to our study of the Dhammapada.

2
00:00:06,000 --> 00:00:11,110
 Tonight we'll be continuing with verse number 42, which

3
00:00:11,110 --> 00:00:13,000
 goes as follows.

4
00:00:13,000 --> 00:00:26,730
 Dhisodhisangyamtankayira vedivapanavirinam micha panihitank

5
00:00:26,730 --> 00:00:29,000
itam papyo nam tatukare.

6
00:00:29,000 --> 00:00:32,000
 What it means?

7
00:00:32,000 --> 00:00:36,000
 Dhisodhisan, Dhiso means an enemy.

8
00:00:36,000 --> 00:00:40,280
 Dhisodhisangyamtankayira, what an enemy could do to their

9
00:00:40,280 --> 00:00:41,000
 enemy.

10
00:00:41,000 --> 00:00:44,000
 What one enemy could do to another.

11
00:00:44,000 --> 00:00:52,000
 Vedivapanavirinam, or one who is vengeful towards another.

12
00:00:52,000 --> 00:01:00,630
 Micha pan banyitankitam, the ill-directed mind, or the mind

13
00:01:00,630 --> 00:01:03,000
 that is set wrongly.

14
00:01:03,000 --> 00:01:14,310
 Papyo nam tatukare is of greater evil, does greater evil

15
00:01:14,310 --> 00:01:17,000
 than that.

16
00:01:17,000 --> 00:01:19,000
 Some pretty powerful words.

17
00:01:19,000 --> 00:01:24,180
 The meaning is what an enemy could do to an enemy or

18
00:01:24,180 --> 00:01:27,000
 someone who is seeking vengeance.

19
00:01:27,000 --> 00:01:31,000
 Anyone who could ever seek vengeance upon someone else.

20
00:01:31,000 --> 00:01:34,000
 Reek vengeance upon another person.

21
00:01:34,000 --> 00:01:38,650
 The Buddha said, some pretty strong words, that it's not

22
00:01:38,650 --> 00:01:41,000
 possible, basically,

23
00:01:41,000 --> 00:01:46,000
 for them to do as much damage as the ill-directed mind.

24
00:01:46,000 --> 00:01:49,000
 It's worth listening to, no?

25
00:01:49,000 --> 00:01:52,000
 So, but first we have a story.

26
00:01:52,000 --> 00:01:53,000
 There's not much of a story.

27
00:01:53,000 --> 00:01:56,000
 This story is quite a short one.

28
00:01:56,000 --> 00:02:00,530
 The story goes that Anattapindika, this great lay disciple

29
00:02:00,530 --> 00:02:02,000
 of the Buddha,

30
00:02:02,000 --> 00:02:06,430
 who donated so much and donated Jethavana, this Buddhist

31
00:02:06,430 --> 00:02:09,000
 monastery for the Buddha,

32
00:02:09,000 --> 00:02:22,000
 had a cattle herder, the person who looks after his cows.

33
00:02:22,000 --> 00:02:27,900
 And this cattle herder, he from time to time would go with

34
00:02:27,900 --> 00:02:29,000
 Anattapindika

35
00:02:29,000 --> 00:02:32,000
 to listen to the Buddha teach.

36
00:02:32,000 --> 00:02:39,000
 And he gained great faith in the Buddha,

37
00:02:39,000 --> 00:02:42,620
 and so he invited the Buddha to come to his house to

38
00:02:42,620 --> 00:02:47,000
 receive charity,

39
00:02:47,000 --> 00:02:51,000
 or receive some kind of gift from him.

40
00:02:51,000 --> 00:02:54,000
 And he thought that, wow, this is someone worth supporting.

41
00:02:54,000 --> 00:02:58,100
 So he decided he wanted to provide some requisites to the

42
00:02:58,100 --> 00:02:59,000
 Buddha.

43
00:02:59,000 --> 00:03:04,520
 And the Buddha, seeing that this guy had some strong good

44
00:03:04,520 --> 00:03:06,000
 qualities in him

45
00:03:06,000 --> 00:03:10,000
 and the potential to understand the Dhamma,

46
00:03:10,000 --> 00:03:14,790
 accepted the invitation and walked all the way to this man

47
00:03:14,790 --> 00:03:15,000
's,

48
00:03:15,000 --> 00:03:20,000
 this cattle herder's house.

49
00:03:20,000 --> 00:03:24,000
 And for seven days, as I understand, for seven days,

50
00:03:24,000 --> 00:03:34,130
 he spent, he went every day to receive the five products of

51
00:03:34,130 --> 00:03:35,000
 a cow.

52
00:03:35,000 --> 00:03:39,000
 So he got milk and butter and cheese and so on.

53
00:03:39,000 --> 00:03:42,000
 All of these good things that come from the cows.

54
00:03:42,000 --> 00:03:44,000
 So he had these cows that he looked after,

55
00:03:44,000 --> 00:03:51,000
 and he was able to provide the Buddha with dairy products.

56
00:03:51,000 --> 00:03:54,100
 And the story goes that he offered these to the Buddha

57
00:03:54,100 --> 00:03:56,000
 seven days in a row.

58
00:03:56,000 --> 00:04:02,000
 And on the seventh day, he was taking the Buddha back to,

59
00:04:02,000 --> 00:04:04,000
 he invited the Buddha for seven days,

60
00:04:04,000 --> 00:04:06,700
 and on the seventh day he was going to follow the Buddha

61
00:04:06,700 --> 00:04:08,000
 back to Jethavana,

62
00:04:08,000 --> 00:04:11,000
 and he carried the Buddha's bowl.

63
00:04:11,000 --> 00:04:14,000
 Now they got halfway through the forest,

64
00:04:14,000 --> 00:04:18,000
 on the way back to Sawati, I guess.

65
00:04:18,000 --> 00:04:23,000
 And suddenly the Buddha turned around and received his bowl

66
00:04:23,000 --> 00:04:24,000
 from Nanda.

67
00:04:24,000 --> 00:04:30,000
 Nanda was his name, Gopalada, the cattle herder.

68
00:04:30,000 --> 00:04:32,000
 And he received his bowl from him and said,

69
00:04:32,000 --> 00:04:37,930
 "Nanda, please turn back, don't come any further, we'll go

70
00:04:37,930 --> 00:04:40,000
 by ourselves."

71
00:04:40,000 --> 00:04:44,440
 And the Buddha turned and walked away, and Nanda turned

72
00:04:44,440 --> 00:04:46,000
 around to go home.

73
00:04:46,000 --> 00:04:51,000
 And on his way home, some of the monks were coming later,

74
00:04:51,000 --> 00:04:55,000
 they had left the house later, I guess, after the Buddha.

75
00:04:55,000 --> 00:05:00,000
 And notice that what happened was Nanda turned around,

76
00:05:00,000 --> 00:05:06,000
 and as he was walking back through the forest, he got,

77
00:05:06,000 --> 00:05:09,000
 he ran into a hunter, there was a hunter who saw him going

78
00:05:09,000 --> 00:05:09,000
 through the forest,

79
00:05:09,000 --> 00:05:11,620
 and thought he was a deer or something, and shot him and

80
00:05:11,620 --> 00:05:12,000
 killed him.

81
00:05:12,000 --> 00:05:16,000
 Shot him with a bow and arrow and killed him.

82
00:05:16,000 --> 00:05:19,200
 And some of the monks noticed this, the hunter realized he

83
00:05:19,200 --> 00:05:20,000
 was wrong,

84
00:05:20,000 --> 00:05:23,000
 and I guess just ran away.

85
00:05:23,000 --> 00:05:27,990
 But some of the monks saw this, that here was a guy who had

86
00:05:27,990 --> 00:05:31,000
 done so much good deeds.

87
00:05:31,000 --> 00:05:34,850
 And it's almost as though the Buddha knew what was going to

88
00:05:34,850 --> 00:05:35,000
 happen,

89
00:05:35,000 --> 00:05:38,000
 and it really seemed strange.

90
00:05:38,000 --> 00:05:42,400
 The reason this story is so well remembered, and it's

91
00:05:42,400 --> 00:05:43,000
 actually in the commentaries,

92
00:05:43,000 --> 00:05:46,000
 is because it's a curious story.

93
00:05:46,000 --> 00:05:51,000
 Here we have someone that the Buddha spent seven days with,

94
00:05:51,000 --> 00:05:55,000
 and it's almost as though the Buddha knew,

95
00:05:55,000 --> 00:05:58,060
 and of course there was this idea going around that the

96
00:05:58,060 --> 00:06:01,000
 Buddha was omniscient,

97
00:06:01,000 --> 00:06:04,530
 so he wouldn't have sent Nanda back if he didn't know what

98
00:06:04,530 --> 00:06:06,000
 was going to happen.

99
00:06:06,000 --> 00:06:09,000
 And yet he purposefully sent Nanda back,

100
00:06:09,000 --> 00:06:12,440
 kind of like as though he knew that he was going to get

101
00:06:12,440 --> 00:06:14,000
 shot by this hunter.

102
00:06:14,000 --> 00:06:18,490
 This is the curious part of the story, and what leads to

103
00:06:18,490 --> 00:06:20,000
 this verse.

104
00:06:20,000 --> 00:06:22,000
 And so the monks are kind of like,

105
00:06:22,000 --> 00:06:24,680
 and they heard that the Buddha had sent him back, and they

106
00:06:24,680 --> 00:06:25,000
 went,

107
00:06:25,000 --> 00:06:28,670
 and they're kind of like, "Um, I don't know how to approach

108
00:06:28,670 --> 00:06:29,000
 this,

109
00:06:29,000 --> 00:06:35,000
 but you sent Lord Buddha, you sent Nanda back,

110
00:06:35,000 --> 00:06:38,000
 knowing that, you know, that, that, that,

111
00:06:38,000 --> 00:06:41,000
 did you know that this was going to happen?

112
00:06:41,000 --> 00:06:43,000
 No, if you hadn't have sent him back,

113
00:06:43,000 --> 00:06:45,000
 he wouldn't have gotten shot by the hunter.

114
00:06:45,000 --> 00:06:48,000
 So what's that?"

115
00:06:48,000 --> 00:06:51,000
 And the Buddha said,

116
00:06:51,000 --> 00:06:55,120
 "If I hadn't told, whether I told him to go or didn't tell

117
00:06:55,120 --> 00:06:56,000
 him to go,

118
00:06:56,000 --> 00:06:59,390
 whether he had gone north, south, east, west, in any of the

119
00:06:59,390 --> 00:07:00,000
 eight,

120
00:07:00,000 --> 00:07:02,000
 the four directions or the four minor directions,

121
00:07:02,000 --> 00:07:07,000
 wherever he had gone, there's no way he could have escaped

122
00:07:07,000 --> 00:07:16,000
 from, from the effects of his bad karma."

123
00:07:16,000 --> 00:07:19,000
 And then the Buddha taught this verse.

124
00:07:19,000 --> 00:07:25,000
 He said, "It, it, as he's taught elsewhere,

125
00:07:25,000 --> 00:07:27,000
 you can't escape this karma, man.

126
00:07:27,000 --> 00:07:32,000
 The point being that you, you can't, you don't,

127
00:07:32,000 --> 00:07:37,000
 the, the blame that we might place on an enemy or on a,

128
00:07:37,000 --> 00:07:42,000
 someone seeking vengeance against us is,

129
00:07:42,000 --> 00:07:47,000
 is much better placed or is properly placed on our own mind

130
00:07:47,000 --> 00:07:47,000
,

131
00:07:47,000 --> 00:07:52,000
 which has caused us to arise in such a situation,

132
00:07:52,000 --> 00:07:54,000
 caused us to give rise to such a situation.

133
00:07:54,000 --> 00:08:03,000
 So it really an interesting story because it, it,

134
00:08:03,000 --> 00:08:07,000
 it requires, for understanding it requires a,

135
00:08:07,000 --> 00:08:12,000
 sort of a, a profound understanding of karma,

136
00:08:12,000 --> 00:08:14,000
 not just on a moment to moment level,

137
00:08:14,000 --> 00:08:17,000
 but you have to somehow see that there's a relationship

138
00:08:17,000 --> 00:08:24,000
 between this hunter killing the, the, this, this cattle her

139
00:08:24,000 --> 00:08:24,000
der

140
00:08:24,000 --> 00:08:28,000
 and something that, that, that the, the cattle herder

141
00:08:28,000 --> 00:08:31,000
 has, I guess, done in the past, probably to this hunter.

142
00:08:31,000 --> 00:08:34,000
 And the, the, the even more curious thing about this is

143
00:08:34,000 --> 00:08:36,700
 we don't know what it was because as with most of the

144
00:08:36,700 --> 00:08:37,000
 stories,

145
00:08:37,000 --> 00:08:42,000
 then the Buddha says, "In the past, this, this man did this

146
00:08:42,000 --> 00:08:45,000
 and this and this," but the curious note that we have is

147
00:08:45,000 --> 00:08:48,000
 the story, the commentary says, "But the monks never asked

148
00:08:48,000 --> 00:08:50,000
 the Buddha what he, what this guy had done."

149
00:08:50,000 --> 00:08:54,000
 So we know, so we don't know what the karma was.

150
00:08:54,000 --> 00:08:57,000
 It's actually a unique story in that way because normally

151
00:08:57,000 --> 00:08:59,000
 the monks go up to the Buddha and ask him,

152
00:08:59,000 --> 00:09:03,050
 "What did Nanda do? What did Nanda Gopala do to deserve

153
00:09:03,050 --> 00:09:04,000
 that?"

154
00:09:04,000 --> 00:09:07,000
 And in this case they didn't.

155
00:09:07,000 --> 00:09:10,000
 So first of all, it talks about karma.

156
00:09:10,000 --> 00:09:15,000
 And it's a, sort of an example, a strong example

157
00:09:15,000 --> 00:09:20,000
 of how this verse comes into play, how this verse,

158
00:09:20,000 --> 00:09:27,000
 or the truth of this verse, how this verse has meaning.

159
00:09:27,000 --> 00:09:32,000
 Because the point being that not only will your

160
00:09:32,000 --> 00:09:36,000
 ill-directed mind hurt you in, on a momentary level,

161
00:09:36,000 --> 00:09:39,000
 like if you get angry or in pain or if you cling to things

162
00:09:39,000 --> 00:09:43,500
 you're stressed out about them, but it has profound

163
00:09:43,500 --> 00:09:44,000
 reaching

164
00:09:44,000 --> 00:09:48,000
 effects that can lead over into the next life and can

165
00:09:48,000 --> 00:09:52,000
 chase you throughout your journey in samsara because

166
00:09:52,000 --> 00:09:54,000
 of how they affect the mind.

167
00:09:54,000 --> 00:10:05,000
 So, and, so it actually, you know, you would say,

168
00:10:05,000 --> 00:10:08,000
 maybe requires a little bit of faith and people will

169
00:10:08,000 --> 00:10:14,000
 start to question this idea of karma and say,

170
00:10:14,000 --> 00:10:19,000
 "Well, here we're entering into a realm that is outside

171
00:10:19,000 --> 00:10:22,000
 of the core of the Buddhist teaching because it's starting

172
00:10:22,000 --> 00:10:31,000
 to deal with faith and belief in some sort of magical,

173
00:10:31,000 --> 00:10:36,000
 karmic potency of acts that when you kill someone

174
00:10:36,000 --> 00:10:39,000
 you magically get punished for it or so on."

175
00:10:39,000 --> 00:10:45,000
 Or that the things that we have done have some connection,

176
00:10:45,000 --> 00:10:51,000
 unseen connection to things in the past, but all that

177
00:10:51,000 --> 00:10:54,000
 really shows, if you understand how Buddhism, how karma

178
00:10:54,000 --> 00:10:56,000
 works, how rebirth works, all that really shows is the

179
00:10:56,000 --> 00:11:03,000
 profound and intrinsic connection between moments of

180
00:11:03,000 --> 00:11:11,000
 consciousness, between our acts and the world around us.

181
00:11:11,000 --> 00:11:15,000
 So, like they'll say about the weather, they've come to

182
00:11:15,000 --> 00:11:17,000
 realize this about the weather, that how small things

183
00:11:17,000 --> 00:11:20,000
 in one part of the world can have a profound impact on

184
00:11:20,000 --> 00:11:22,000
 other parts of the world.

185
00:11:22,000 --> 00:11:25,000
 They have this saying which is probably not true, that

186
00:11:25,000 --> 00:11:28,000
 if a butterfly flaps its wings in China there's an

187
00:11:28,000 --> 00:11:31,000
 earthquake in America or something, or a tornado in

188
00:11:31,000 --> 00:11:33,000
 America.

189
00:11:33,000 --> 00:11:39,000
 The point being that it's very easy for things to

190
00:11:39,000 --> 00:11:44,000
 change and something very small or very specific can

191
00:11:44,000 --> 00:11:48,000
 have an effect on the long term.

192
00:11:48,000 --> 00:11:51,000
 It's not difficult to understand really at all when

193
00:11:51,000 --> 00:11:56,000
 you think about how an act affects your mind and how

194
00:11:56,000 --> 00:11:59,000
 your mind affects your future.

195
00:11:59,000 --> 00:12:03,000
 So, if you have done bad deeds in the past, if you've

196
00:12:03,000 --> 00:12:07,000
 hurt other people or if you've clung to, if you're

197
00:12:07,000 --> 00:12:12,000
 clinging to things, it's going to change your whole

198
00:12:12,000 --> 00:12:14,000
 attitude towards life.

199
00:12:14,000 --> 00:12:16,000
 It's going to affect your mind.

200
00:12:16,000 --> 00:12:19,000
 It's something you'll think about on a daily basis.

201
00:12:19,000 --> 00:12:22,000
 The Buddha said, "Whatever you think about, that's

202
00:12:22,000 --> 00:12:24,000
 what your mind inclines towards."

203
00:12:24,000 --> 00:12:26,000
 It's a very obvious sort of teaching.

204
00:12:26,000 --> 00:12:28,000
 Something we don't think about because we don't

205
00:12:28,000 --> 00:12:29,000
 really understand karma.

206
00:12:29,000 --> 00:12:32,000
 We don't really get that our thoughts affect our

207
00:12:32,000 --> 00:12:34,000
 lives.

208
00:12:34,000 --> 00:12:38,000
 And yet they absolutely do.

209
00:12:38,000 --> 00:12:41,000
 When we're stressed about something it changes all of

210
00:12:41,000 --> 00:12:43,000
 our interactions with other people throughout the

211
00:12:43,000 --> 00:12:46,000
 life, with the world around us, throughout our life.

212
00:12:46,000 --> 00:12:49,000
 It changes even the direction of our lives.

213
00:12:49,000 --> 00:12:52,000
 People who have gone through traumatic experiences,

214
00:12:52,000 --> 00:12:55,000
 of course it changes their whole life.

215
00:12:55,000 --> 00:12:58,000
 It changes their outlook.

216
00:12:58,000 --> 00:13:04,000
 It changes their potential, their possibilities

217
00:13:04,000 --> 00:13:08,000
 in their lives.

218
00:13:08,000 --> 00:13:11,000
 The things that we do, the things that we engage

219
00:13:11,000 --> 00:13:13,000
 in change us.

220
00:13:13,000 --> 00:13:15,000
 People who are addicted to things have to spend

221
00:13:15,000 --> 00:13:18,000
 money and put resources into the things that they

222
00:13:18,000 --> 00:13:20,000
 want and that they might have put elsewhere.

223
00:13:20,000 --> 00:13:23,000
 They have to put time, dedicate time to their

224
00:13:23,000 --> 00:13:30,000
 energy and brain cells and thoughts and so on.

225
00:13:30,000 --> 00:13:36,000
 And so it actually gets, the effect can actually

226
00:13:36,000 --> 00:13:39,000
 get bigger over the long term so that the effect

227
00:13:39,000 --> 00:13:43,000
 that karma has in the short term might be minuscule

228
00:13:43,000 --> 00:13:46,000
 compared to the effect that it has later on in life

229
00:13:46,000 --> 00:13:48,000
 or even into the next life.

230
00:13:48,000 --> 00:13:51,000
 Because of course as Buddhists we're not thinking

231
00:13:51,000 --> 00:13:54,000
 of the next life as something separate from this life.

232
00:13:54,000 --> 00:13:56,000
 It's a continuation.

233
00:13:56,000 --> 00:14:01,000
 There's a shift at the moment of death, but only

234
00:14:01,000 --> 00:14:04,000
 because we've entered into this state of being

235
00:14:04,000 --> 00:14:09,000
 born and die, this human state that is very coarse

236
00:14:09,000 --> 00:14:16,000
 and requires or associates with the physical body.

237
00:14:16,000 --> 00:14:19,000
 Actually at the moment of death nothing happens.

238
00:14:19,000 --> 00:14:27,000
 Life just continues and our mind just continues.

239
00:14:27,000 --> 00:14:30,000
 Now that's the part that has to do with karma.

240
00:14:30,000 --> 00:14:37,000
 When we're talking about the mind and the potency

241
00:14:37,000 --> 00:14:41,000
 of the mind, why is the mind so potent in creating

242
00:14:41,000 --> 00:14:43,000
 karma?

243
00:14:43,000 --> 00:14:47,000
 Once we accept the fact that our bad deeds have

244
00:14:47,000 --> 00:14:51,000
 an effect, why would it be that the deeds of others

245
00:14:51,000 --> 00:14:59,000
 are less potent than our own minds?

246
00:14:59,000 --> 00:15:03,000
 And further, why is it the mind that the Buddha

247
00:15:03,000 --> 00:15:05,000
 is focusing on and not the deeds?

248
00:15:05,000 --> 00:15:11,000
 So people are still asking me,

249
00:15:11,000 --> 00:15:13,000
 why is it that when you do something it doesn't

250
00:15:13,000 --> 00:15:15,000
 have karmic potency?

251
00:15:15,000 --> 00:15:18,000
 We have this idea that karma is some kind of magic

252
00:15:18,000 --> 00:15:21,000
 because we've been brought up generally in the West

253
00:15:21,000 --> 00:15:24,000
 and we've been brought up to think in terms of God,

254
00:15:24,000 --> 00:15:30,000
 to think in terms of nature as having some kind of watchful

255
00:15:30,000 --> 00:15:40,000
 or some kind of power to affect and to intervene

256
00:15:40,000 --> 00:15:44,000
 with our lives.

257
00:15:44,000 --> 00:15:47,000
 So we think when you step on an ant, bad,

258
00:15:47,000 --> 00:15:48,000
 you've done a bad deed, right?

259
00:15:48,000 --> 00:15:52,000
 Because we're used to sin being something that God decides.

260
00:15:52,000 --> 00:15:54,000
 Is it sinful or is it right?

261
00:15:54,000 --> 00:16:00,000
 And he's watching you like Santa Claus.

262
00:16:00,000 --> 00:16:03,000
 But karma is absolutely not that.

263
00:16:03,000 --> 00:16:07,000
 Karma is the effect that something has on your mind

264
00:16:07,000 --> 00:16:12,000
 or the effect that something has on the choices you make

265
00:16:12,000 --> 00:16:16,000
 and on your state of mind into the future.

266
00:16:16,000 --> 00:16:19,000
 And that which has the biggest, the most profound effect

267
00:16:19,000 --> 00:16:25,000
 or really the only effect is your own mind.

268
00:16:25,000 --> 00:16:29,000
 Another person, the harm that an enemy can do to you

269
00:16:29,000 --> 00:16:36,000
 or someone who wants to take out their vengeance on you

270
00:16:36,000 --> 00:16:42,000
 is actually absolutely zero.

271
00:16:42,000 --> 00:16:47,000
 The harm that they can perform directly upon you is none.

272
00:16:47,000 --> 00:16:49,000
 It's not even little.

273
00:16:49,000 --> 00:16:50,000
 It's not a little.

274
00:16:50,000 --> 00:16:52,000
 It's not something you can mitigate.

275
00:16:52,000 --> 00:16:53,000
 It's zero.

276
00:16:53,000 --> 00:16:55,000
 They can't, no one can hurt you.

277
00:16:55,000 --> 00:16:59,000
 No one can cause harm to another.

278
00:16:59,000 --> 00:17:01,630
 So what the Buddha says is actually an understatement of

279
00:17:01,630 --> 00:17:02,000
 the fact.

280
00:17:02,000 --> 00:17:05,990
 There's no comparison between the harm that an enemy can do

281
00:17:05,990 --> 00:17:08,000
 to you

282
00:17:08,000 --> 00:17:13,000
 and the harm that you can do to yourself.

283
00:17:13,000 --> 00:17:22,000
 And this is due to the theory or the Buddhist theory,

284
00:17:22,000 --> 00:17:25,850
 which is accepted and not accepted, not accepted by many of

285
00:17:25,850 --> 00:17:26,000
 you,

286
00:17:26,000 --> 00:17:34,280
 that the mind is apart from the body, that it's not a

287
00:17:34,280 --> 00:17:36,000
 physical process.

288
00:17:36,000 --> 00:17:42,000
 You're not locked into your environment.

289
00:17:42,000 --> 00:17:46,000
 We react to, the mind reacts to the environment.

290
00:17:46,000 --> 00:17:51,260
 It reacts either in a positive or a negative or a neutral

291
00:17:51,260 --> 00:17:52,000
 way.

292
00:17:52,000 --> 00:17:56,800
 So when we experience something painful, we generally tend

293
00:17:56,800 --> 00:17:58,000
 to react in a negative way.

294
00:17:58,000 --> 00:18:00,990
 When we experience something pleasant, we react in a

295
00:18:00,990 --> 00:18:02,000
 positive way.

296
00:18:02,000 --> 00:18:06,000
 The body reacts, but the mind also reacts.

297
00:18:06,000 --> 00:18:13,000
 And the key to understanding and to practicing Buddhism

298
00:18:13,000 --> 00:18:16,000
 is realizing that these two things are separate,

299
00:18:16,000 --> 00:18:20,340
 that the mind need not react in a negative or a positive

300
00:18:20,340 --> 00:18:21,000
 way.

301
00:18:21,000 --> 00:18:26,000
 The mind can react in an objective way to reality.

302
00:18:26,000 --> 00:18:33,000
 If and when the mind is aware of the pain as just pain,

303
00:18:33,000 --> 00:18:36,000
 then the pain will not harm the mind.

304
00:18:36,000 --> 00:18:41,200
 If when someone yells at us or scolds us and we see it

305
00:18:41,200 --> 00:18:43,000
 simply as hearing,

306
00:18:43,000 --> 00:18:51,000
 we understand it to be sound arising at the ear.

307
00:18:51,000 --> 00:18:58,000
 Then we, and observation proves this,

308
00:18:58,000 --> 00:19:03,000
 or if you test this hypothesis,

309
00:19:03,000 --> 00:19:05,360
 if you test it out for yourself, you can see this for

310
00:19:05,360 --> 00:19:06,000
 yourself.

311
00:19:06,000 --> 00:19:09,750
 If you listen to the sound of my voice and just take it for

312
00:19:09,750 --> 00:19:10,000
 sound,

313
00:19:10,000 --> 00:19:19,000
 hearing, hearing, then you find that any other judgments

314
00:19:19,000 --> 00:19:22,000
 or thoughts you might have about what I'm saying disappear.

315
00:19:22,000 --> 00:19:27,000
 Any boredom or aversion to what I'm saying,

316
00:19:27,000 --> 00:19:31,580
 or any attachment or desire or appreciation of what I'm

317
00:19:31,580 --> 00:19:32,000
 saying,

318
00:19:32,000 --> 00:19:35,000
 melts away and you simply know it for sound.

319
00:19:35,000 --> 00:19:39,000
 And you find that suddenly you're in the realm of peace

320
00:19:39,000 --> 00:19:42,000
 and of contentment.

321
00:19:42,000 --> 00:19:46,000
 You're unshaken by what the person is saying.

322
00:19:46,000 --> 00:19:49,000
 If they're saying something nasty or unpleasant,

323
00:19:49,000 --> 00:19:57,000
 even if they're hitting you or beating you.

324
00:19:57,000 --> 00:20:05,000
 It's actually the only way to be free from suffering is to

325
00:20:05,000 --> 00:20:06,000
 free yourself from within,

326
00:20:06,000 --> 00:20:09,000
 because you can't be free from enemies,

327
00:20:09,000 --> 00:20:14,000
 you can't be free from harsh speech,

328
00:20:14,000 --> 00:20:18,330
 you can't be free from the cold and heat and hunger and

329
00:20:18,330 --> 00:20:19,000
 thirst

330
00:20:19,000 --> 00:20:21,000
 and sickness and old age and death.

331
00:20:21,000 --> 00:20:24,000
 We can't be free from these things.

332
00:20:24,000 --> 00:20:29,000
 So there's only two ways to be free from suffering.

333
00:20:29,000 --> 00:20:33,000
 It's not have any of these things which are ubiquitous,

334
00:20:33,000 --> 00:20:36,000
 which are an intrinsic part of life,

335
00:20:36,000 --> 00:20:39,000
 or overcome them, be free from them.

336
00:20:39,000 --> 00:20:42,000
 Free yourself, free your mind from them.

337
00:20:42,000 --> 00:20:44,000
 And this is what the Buddha is referring to.

338
00:20:44,000 --> 00:20:47,000
 This is the key theory that we have in Buddhism.

339
00:20:47,000 --> 00:20:53,160
 If you need to accept this in order to practice Buddhism

340
00:20:53,160 --> 00:20:55,000
 correctly,

341
00:20:55,000 --> 00:20:59,690
 it's key to understand that you can change the way you

342
00:20:59,690 --> 00:21:01,000
 react to reality.

343
00:21:01,000 --> 00:21:03,000
 You need not react.

344
00:21:03,000 --> 00:21:06,000
 You can interact objectively with reality.

345
00:21:06,000 --> 00:21:12,000
 This is why the Buddha said the mind that is set wrongly,

346
00:21:12,000 --> 00:21:16,000
 the mind that is set in a bad way,

347
00:21:16,000 --> 00:21:22,330
 is of much greater or incomparably greater harm to the

348
00:21:22,330 --> 00:21:23,000
 individual

349
00:21:23,000 --> 00:21:28,000
 than anyone else could be, than an enemy could be.

350
00:21:28,000 --> 00:21:34,000
 And furthermore, what this means is,

351
00:21:34,000 --> 00:21:36,000
 this means actually more than just karma.

352
00:21:36,000 --> 00:21:39,000
 What the Buddha is talking about here is not just someone

353
00:21:39,000 --> 00:21:40,000
 doing a bad deed.

354
00:21:40,000 --> 00:21:44,000
 What he's talking about is setting the mind in a bad way,

355
00:21:44,000 --> 00:21:48,000
 which is actually even worse than doing a bad deed.

356
00:21:48,000 --> 00:21:51,000
 So up until now we're just talking about bad karma.

357
00:21:51,000 --> 00:21:53,660
 But even worse than bad karma, there's something even worse

358
00:21:53,660 --> 00:21:54,000
 than that.

359
00:21:54,000 --> 00:21:58,000
 If you kill someone, if you steal, if you do bad things,

360
00:21:58,000 --> 00:22:01,000
 it's setting your mind in a bad way.

361
00:22:01,000 --> 00:22:02,000
 "Micha panihitan."

362
00:22:02,000 --> 00:22:09,000
 Panihitan means set or sent in a wrong way.

363
00:22:09,000 --> 00:22:13,000
 You can think it refers to wrong view.

364
00:22:13,000 --> 00:22:14,000
 So there are three kinds of wrong.

365
00:22:14,000 --> 00:22:20,000
 One is wrong perception.

366
00:22:20,000 --> 00:22:22,000
 Another is wrong thought.

367
00:22:22,000 --> 00:22:24,000
 And the third is wrong view.

368
00:22:24,000 --> 00:22:26,000
 Wrong view is the worst.

369
00:22:26,000 --> 00:22:29,000
 Wrong perception is the least serious.

370
00:22:29,000 --> 00:22:31,810
 If someone says something to you and you get angry right

371
00:22:31,810 --> 00:22:32,000
 away,

372
00:22:32,000 --> 00:22:34,000
 that's wrong perception.

373
00:22:34,000 --> 00:22:36,000
 You perceive it as bad.

374
00:22:36,000 --> 00:22:37,650
 But when you start thinking about it, that starts to get

375
00:22:37,650 --> 00:22:38,000
 worse.

376
00:22:38,000 --> 00:22:40,000
 You think, "Oh, did that person do?

377
00:22:40,000 --> 00:22:43,000
 That was really mean of them," and so on.

378
00:22:43,000 --> 00:22:46,000
 Really nasty of them.

379
00:22:46,000 --> 00:22:48,290
 That's worse because then you start to think how you're

380
00:22:48,290 --> 00:22:49,000
 going to hurt them,

381
00:22:49,000 --> 00:22:51,000
 and hurt them back, and so on.

382
00:22:51,000 --> 00:22:54,000
 You have the view when you set yourself in the idea,

383
00:22:54,000 --> 00:22:57,000
 "That person is evil. I didn't deserve that.

384
00:22:57,000 --> 00:23:01,980
 They deserve to be punished," when you have the wrong views

385
00:23:01,980 --> 00:23:03,000
.

386
00:23:03,000 --> 00:23:06,780
 Because you can, we can all, even as Buddhists, we all have

387
00:23:06,780 --> 00:23:08,000
 these first two.

388
00:23:08,000 --> 00:23:10,000
 But we need not have the third.

389
00:23:10,000 --> 00:23:13,560
 And as Buddhists, we generally don't because this is what

390
00:23:13,560 --> 00:23:15,000
 we're taught.

391
00:23:15,000 --> 00:23:19,000
 We're taught that, "No, it's not good for you to hurt other

392
00:23:19,000 --> 00:23:20,000
 people."

393
00:23:20,000 --> 00:23:23,680
 Someone says, "Treat other people well, not because they

394
00:23:23,680 --> 00:23:25,000
 deserve to be happy,

395
00:23:25,000 --> 00:23:27,000
 but because you do."

396
00:23:27,000 --> 00:23:29,000
 Which I think is very pertinent.

397
00:23:29,000 --> 00:23:31,000
 I mean, of course, you can think they deserve to be happy,

398
00:23:31,000 --> 00:23:33,000
 but that's not the real reason.

399
00:23:33,000 --> 00:23:37,230
 The real reason is, "Look, if you get angry at them, it's

400
00:23:37,230 --> 00:23:38,000
 not good for you.

401
00:23:38,000 --> 00:23:41,000
 You don't get anywhere by hurting other people.

402
00:23:41,000 --> 00:23:45,000
 You don't help them, obviously. You don't help yourself."

403
00:23:45,000 --> 00:23:48,000
 And it's pertinent because it's how the mind reacts.

404
00:23:48,000 --> 00:23:50,000
 The mind is looking for happiness.

405
00:23:50,000 --> 00:23:53,000
 It's not really looking for happiness in other people.

406
00:23:53,000 --> 00:23:55,000
 We help other people because it makes us happy.

407
00:23:55,000 --> 00:24:04,000
 If it didn't make us happy, it wouldn't excite the mind.

408
00:24:04,000 --> 00:24:09,000
 The mind wouldn't be pleased to continue it.

409
00:24:09,000 --> 00:24:13,990
 So if you want to really encourage your mind and really get

410
00:24:13,990 --> 00:24:16,000
 your attention of your heart,

411
00:24:16,000 --> 00:24:19,660
 you have to put it this way and say, "Look, let's not get

412
00:24:19,660 --> 00:24:20,000
 angry,

413
00:24:20,000 --> 00:24:22,560
 not because they don't deserve it, but because we don't

414
00:24:22,560 --> 00:24:23,000
 deserve it.

415
00:24:23,000 --> 00:24:27,370
 We don't deserve to suffer from this. So let us stop

416
00:24:27,370 --> 00:24:29,000
 ourselves."

417
00:24:29,000 --> 00:24:31,570
 Because we have this idea, because we have this

418
00:24:31,570 --> 00:24:32,000
 understanding,

419
00:24:32,000 --> 00:24:35,000
 and this is the understanding we try to give to Buddhists

420
00:24:35,000 --> 00:24:36,000
 and to meditators,

421
00:24:36,000 --> 00:24:40,280
 we're free from this wrongly directive mind most of the

422
00:24:40,280 --> 00:24:41,000
 time.

423
00:24:41,000 --> 00:24:45,000
 Still, we have to always be on guard.

424
00:24:45,000 --> 00:24:48,980
 When we're in training, this is something important to

425
00:24:48,980 --> 00:24:50,000
 understand.

426
00:24:50,000 --> 00:24:54,840
 Guard against your mind, first of all, from having bad

427
00:24:54,840 --> 00:24:57,000
 perception.

428
00:24:57,000 --> 00:25:00,980
 At the very core, you're best off when you can just

429
00:25:00,980 --> 00:25:03,000
 perceive things as they are,

430
00:25:03,000 --> 00:25:08,780
 and not misperceive things as good, as bad, as me, as mine,

431
00:25:08,780 --> 00:25:11,000
 as right, as wrong.

432
00:25:11,000 --> 00:25:13,750
 But even when you do, then be careful not to let it come

433
00:25:13,750 --> 00:25:15,000
 into your thoughts.

434
00:25:15,000 --> 00:25:18,930
 Be aware of the anger, be aware of the wanting, be aware of

435
00:25:18,930 --> 00:25:20,000
 the desire.

436
00:25:20,000 --> 00:25:22,640
 Don't let your mind start thinking about how you're going

437
00:25:22,640 --> 00:25:24,000
 to get what you want,

438
00:25:24,000 --> 00:25:28,000
 how you're going to chase away what you don't want.

439
00:25:28,000 --> 00:25:30,670
 And if it does come to thoughts, try to be aware of the

440
00:25:30,670 --> 00:25:31,000
 thoughts

441
00:25:31,000 --> 00:25:33,750
 and don't let them become views and think, "Yeah, I deserve

442
00:25:33,750 --> 00:25:37,000
 that. That's right for me to get that."

443
00:25:37,000 --> 00:25:41,810
 Or, "I don't deserve that, and this is wrong, and I'm being

444
00:25:41,810 --> 00:25:44,000
 unjustly treated," and so on.

445
00:25:44,000 --> 00:25:48,160
 Not because they don't deserve it, but because you don't

446
00:25:48,160 --> 00:25:49,000
 deserve it.

447
00:25:49,000 --> 00:25:53,410
 Because we don't want to suffer, so let's stop making

448
00:25:53,410 --> 00:25:55,000
 ourselves separate.

449
00:25:55,000 --> 00:25:58,000
 We have to see that these things are causing us suffering.

450
00:25:58,000 --> 00:26:01,020
 And the worst is this third one, when you set your mind in

451
00:26:01,020 --> 00:26:02,000
 the wrong way.

452
00:26:02,000 --> 00:26:04,430
 This is what the Buddha is talking about. This is the key

453
00:26:04,430 --> 00:26:05,000
 to this verse.

454
00:26:05,000 --> 00:26:07,000
 Something for us all to remember.

455
00:26:07,000 --> 00:26:11,160
 Whenever you think your problems are other people and the

456
00:26:11,160 --> 00:26:13,000
 situation in the world around you,

457
00:26:13,000 --> 00:26:15,000
 never forget what the Buddha said.

458
00:26:15,000 --> 00:26:20,000
 "Dhisodhisangyam tangka-gira viri-vapanaviri-lang"

459
00:26:20,000 --> 00:26:24,000
 "Micha-pani-hitang-jitam-papi-o-nang-tatangkri"

460
00:26:24,000 --> 00:26:29,320
 An enemy, as an enemy would do to an enemy, or one seeking

461
00:26:29,320 --> 00:26:32,000
 vengeance would do to another.

462
00:26:32,000 --> 00:26:39,000
 The wrongly directed mind is far, far...

463
00:26:39,000 --> 00:26:43,000
 will do far, far greater evil than any of those.

464
00:26:43,000 --> 00:26:47,000
 So, that's the Dhammapada verse for today.

465
00:26:47,000 --> 00:26:52,000
 Thank you all for listening, and tune in next time.

