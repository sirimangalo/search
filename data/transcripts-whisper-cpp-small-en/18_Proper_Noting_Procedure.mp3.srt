1
00:00:00,000 --> 00:00:03,000
 Proper Noting Procedure

2
00:00:03,000 --> 00:00:06,000
 Can you cover proper noting during meditation?

3
00:00:06,000 --> 00:00:10,000
 Do you always come back to the breath?

4
00:00:10,000 --> 00:00:14,000
 If you are asking about the tradition that we follow,

5
00:00:14,000 --> 00:00:18,000
 we are not technically practicing mindfulness of the breath

6
00:00:18,000 --> 00:00:18,000
.

7
00:00:18,000 --> 00:00:22,640
 Anapanasati, mindfulness of the in-breaths and the out-b

8
00:00:22,640 --> 00:00:23,000
reaths,

9
00:00:23,000 --> 00:00:26,000
 is technically samatha meditation.

10
00:00:26,000 --> 00:00:30,000
 Vipassana cannot arise when focused on the breath

11
00:00:30,000 --> 00:00:33,000
 because breath is not an ultimate reality.

12
00:00:33,000 --> 00:00:37,500
 The ultimate reality is the sensations that lead to the

13
00:00:37,500 --> 00:00:39,000
 concept of breath,

14
00:00:39,000 --> 00:00:42,000
 whether at the nose or the stomach.

15
00:00:42,000 --> 00:00:46,000
 At the nose, there will be sensations of heat and cold.

16
00:00:46,000 --> 00:00:49,290
 At the stomach, there will be sensations of tension and

17
00:00:49,290 --> 00:00:50,000
 release.

18
00:00:50,000 --> 00:00:54,320
 You don't have to focus on sensations related to the breath

19
00:00:54,320 --> 00:00:55,000
 at all.

20
00:00:55,000 --> 00:00:59,000
 You can focus on the bodily sensations related to sitting,

21
00:00:59,000 --> 00:01:03,000
 saying to yourself, "Sitting, sitting."

22
00:01:03,000 --> 00:01:06,000
 Or when walking, "Walking, walking."

23
00:01:06,000 --> 00:01:10,230
 When practicing walking meditation, you would not focus on

24
00:01:10,230 --> 00:01:11,000
 the breath.

25
00:01:11,000 --> 00:01:14,000
 You would focus on the movements of the feet.

26
00:01:14,000 --> 00:01:16,000
 There is even prostration meditation,

27
00:01:16,000 --> 00:01:19,000
 where you focus on the movements of the hands,

28
00:01:19,000 --> 00:01:21,000
 the bending of the back, and so on.

29
00:01:21,000 --> 00:01:25,000
 When eating, you can practice eating meditation,

30
00:01:25,000 --> 00:01:30,000
 being mindful of chewing, chewing, and swallowing.

31
00:01:30,000 --> 00:01:34,210
 The easiest object to be mindful of, the most coarse and

32
00:01:34,210 --> 00:01:36,000
 most obvious, is the body.

33
00:01:36,000 --> 00:01:39,230
 The mind is more difficult to observe because it is much

34
00:01:39,230 --> 00:01:41,000
 quicker than the body.

35
00:01:41,000 --> 00:01:44,000
 The mind is likened to be a wild animal.

36
00:01:44,000 --> 00:01:47,000
 When a hunter wishes to catch a wild animal,

37
00:01:47,000 --> 00:01:50,000
 they don't run around the jungle chasing after it.

38
00:01:50,000 --> 00:01:52,930
 They wait quietly by the water, knowing that the wild

39
00:01:52,930 --> 00:01:54,000
 animal will come to them.

40
00:01:54,000 --> 00:01:58,000
 The body is like the water, the place where the mind comes

41
00:01:58,000 --> 00:01:59,000
 and can be caught.

42
00:01:59,000 --> 00:02:04,390
 In the Visuddhimagga, Venerable Bhutagosa says that as the

43
00:02:04,390 --> 00:02:06,000
 body becomes clearly understood,

44
00:02:06,000 --> 00:02:10,000
 the mind becomes clearly understood along with it.

45
00:02:10,000 --> 00:02:13,600
 By focusing on the body, you are indirectly learning about

46
00:02:13,600 --> 00:02:14,000
 the mind,

47
00:02:14,000 --> 00:02:17,550
 which should make perfect sense because the mind is what is

48
00:02:17,550 --> 00:02:19,000
 doing the focusing.

49
00:02:19,000 --> 00:02:22,610
 As you observe the stomach, for instance, the rising and

50
00:02:22,610 --> 00:02:23,000
 falling,

51
00:02:23,000 --> 00:02:26,000
 you learn all sorts of things about the mind.

52
00:02:26,000 --> 00:02:30,200
 You will see liking and disliking, boredom and aversion,

53
00:02:30,200 --> 00:02:31,000
 controlling and stressing.

54
00:02:31,000 --> 00:02:34,940
 The whole of the practice really is using the body in order

55
00:02:34,940 --> 00:02:36,000
 to see the mind,

56
00:02:36,000 --> 00:02:40,120
 and when you see the mind, whether it be feelings, thoughts

57
00:02:40,120 --> 00:02:41,000
, or emotions,

58
00:02:41,000 --> 00:02:44,680
 you focus on those, and when they are gone, you come back

59
00:02:44,680 --> 00:02:48,000
 again to the body because it is still there.

