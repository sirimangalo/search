1
00:00:00,000 --> 00:00:10,000
 Good morning from Thailand.

2
00:00:10,000 --> 00:00:20,000
 I'm now getting around to broadcasting again.

3
00:00:20,000 --> 00:00:25,000
 The timing of it's been difficult.

4
00:00:25,000 --> 00:00:32,000
 There's a lot of activities in the morning.

5
00:00:32,000 --> 00:00:38,000
 Mostly surrounding food but also surrounding travel.

6
00:00:38,000 --> 00:00:56,000
 So today I'm in Bangkok.

7
00:00:56,000 --> 00:01:04,000
 Today's quote is about teachers and students.

8
00:01:04,000 --> 00:01:08,250
 Specifically the quote is from the Vinayay, which is

9
00:01:08,250 --> 00:01:13,700
 actually talking about monastic teachers and monastic

10
00:01:13,700 --> 00:01:15,000
 students.

11
00:01:15,000 --> 00:01:22,000
 So there's an official relationship.

12
00:01:22,000 --> 00:01:27,790
 A monk has to stay with the teacher for the first five

13
00:01:27,790 --> 00:01:34,000
 years of their ordination to learn how to become a monk.

14
00:01:34,000 --> 00:01:38,110
 And they can change from one monk to another monk but they

15
00:01:38,110 --> 00:01:40,000
 have to be with somebody.

16
00:01:40,000 --> 00:01:45,100
 And that monk has to have been a monk for at least ten

17
00:01:45,100 --> 00:01:47,000
 years and so on.

18
00:01:47,000 --> 00:01:51,410
 And this describes the relationship but of course it can be

19
00:01:51,410 --> 00:01:59,410
 applied in a broader sense to teacher-student relationships

20
00:01:59,410 --> 00:02:02,000
 in general.

21
00:02:02,000 --> 00:02:09,000
 People should look upon their teacher as a parent.

22
00:02:09,000 --> 00:02:21,490
 And the teacher should look upon their student as their

23
00:02:21,490 --> 00:02:24,000
 offspring.

24
00:02:24,000 --> 00:02:32,080
 Which of course indicates the idea that parents are our

25
00:02:32,080 --> 00:02:35,000
 first teachers.

26
00:02:35,000 --> 00:02:41,000
 The first things we learn are always from our parents.

27
00:02:41,000 --> 00:02:53,370
 And much of who we are and who we become is based on the

28
00:02:53,370 --> 00:03:03,250
 knowledge and the behavior that we pick up from our parents

29
00:03:03,250 --> 00:03:04,000
.

30
00:03:04,000 --> 00:03:10,380
 And this is in general to a lesser extent true of teacher-

31
00:03:10,380 --> 00:03:18,490
student relationship that we affect, the teacher affects the

32
00:03:18,490 --> 00:03:21,000
 student, changes the student.

33
00:03:21,000 --> 00:03:27,200
 I mean obviously but it bears stressing because that's what

34
00:03:27,200 --> 00:03:30,000
 it means to be a teacher.

35
00:03:30,000 --> 00:03:35,000
 It affects some sort of change upon your student.

36
00:03:35,000 --> 00:03:42,620
 If you're a bad teacher you can actually cause damage to

37
00:03:42,620 --> 00:03:45,000
 your student.

38
00:03:45,000 --> 00:03:50,170
 You can damage them, harm them. And it's such a strong

39
00:03:50,170 --> 00:03:55,900
 connection, such a deep spiritual connection that you're

40
00:03:55,900 --> 00:03:57,000
 able to hurt.

41
00:03:57,000 --> 00:04:02,720
 Teacher is able to hurt a student far more than they have a

42
00:04:02,720 --> 00:04:08,530
 rich person because the student opens up and is receptive,

43
00:04:08,530 --> 00:04:14,000
 emulates and internalizes much of what they receive.

44
00:04:14,000 --> 00:04:22,020
 This is why a lot of people are afraid of the idea of being

45
00:04:22,020 --> 00:04:28,000
 a teacher. It seems like such a lofty thing.

46
00:04:28,000 --> 00:04:32,340
 I don't like to think of it like that personally. I'd

47
00:04:32,340 --> 00:04:39,000
 rather see more people learning how to be a proper teacher.

48
00:04:39,000 --> 00:04:45,260
 More to the point is your intentions. The only way to be a

49
00:04:45,260 --> 00:04:49,320
 really bad teacher is to have bad intentions towards your

50
00:04:49,320 --> 00:04:50,000
 student.

51
00:04:50,000 --> 00:04:54,830
 To have unwholesome intentions, a desire to manipulate and

52
00:04:54,830 --> 00:04:56,000
 to pervert.

53
00:04:56,000 --> 00:05:02,720
 You have anger and greed and delusion and you pass those on

54
00:05:02,720 --> 00:05:05,000
 to your student.

55
00:05:05,000 --> 00:05:07,890
 Just because you have those things doesn't mean you can't

56
00:05:07,890 --> 00:05:09,000
 be a good teacher.

57
00:05:09,000 --> 00:05:13,000
 It's a matter of how conscientious you are.

58
00:05:13,000 --> 00:05:17,100
 The intentions you have towards your teacher, whether they

59
00:05:17,100 --> 00:05:22,600
're to intend to encourage violence and unwholesomeness and

60
00:05:22,600 --> 00:05:27,450
 covetousness and arrogance and all these things in your

61
00:05:27,450 --> 00:05:28,000
 student.

62
00:05:28,000 --> 00:05:32,580
 You want them to become like you in terms of the good

63
00:05:32,580 --> 00:05:37,640
 things that you know or in terms of the bad things, your

64
00:05:37,640 --> 00:05:42,000
 bad habits and bad behaviors.

65
00:05:42,000 --> 00:05:46,610
 It should be taken seriously. It doesn't mean that you

66
00:05:46,610 --> 00:05:50,090
 should discard the idea that you yourself could be a

67
00:05:50,090 --> 00:05:51,000
 teacher.

68
00:05:51,000 --> 00:05:54,620
 It means if you're going to be a teacher, you're going to

69
00:05:54,620 --> 00:05:58,000
 teach someone, you should take it seriously.

70
00:05:58,000 --> 00:06:10,000
 Not do it lazily, not do it half-heartedly.

71
00:06:10,000 --> 00:06:22,160
 You should be very mindful when you're teaching. Try your

72
00:06:22,160 --> 00:06:23,240
 best to stay alert and aware and clear of mind and aware of

73
00:06:23,240 --> 00:06:24,000
 your own emotions and own biases.

74
00:06:24,000 --> 00:06:32,000
 Careful not to let them encroach upon the teaching.

75
00:06:32,000 --> 00:06:38,950
 It's a very intimate relationship. It's a very powerful

76
00:06:38,950 --> 00:06:41,000
 gift. So it's a great gift to teach someone something.

77
00:06:41,000 --> 00:06:45,000
 You should help take this lightly.

78
00:06:45,000 --> 00:06:49,840
 The person receiving the gift should also not take it

79
00:06:49,840 --> 00:06:51,000
 lightly.

80
00:06:51,000 --> 00:06:57,350
 It should be receptive and mindful and alert so that they

81
00:06:57,350 --> 00:07:05,040
 receive it properly, understand it properly and able to

82
00:07:05,040 --> 00:07:19,000
 discern the important essence of the teaching.

83
00:07:19,000 --> 00:07:23,590
 That is how we all teachers and students do the wrong thing

84
00:07:23,590 --> 00:07:28,000
, we all progress in the book of teaching.

85
00:07:28,000 --> 00:07:34,720
 When you help someone else, you help yourself, you help

86
00:07:34,720 --> 00:07:36,000
 others.

87
00:07:36,000 --> 00:07:40,030
 Teachers are not meant to be thought of as in a different

88
00:07:40,030 --> 00:07:42,000
 class as the students.

89
00:07:42,000 --> 00:07:46,140
 They also, teaching is a part of the path. It's what

90
00:07:46,140 --> 00:07:49,000
 practitioners should do as well.

91
00:07:49,000 --> 00:07:58,020
 Practitioners should also teach and share and support each

92
00:07:58,020 --> 00:07:59,000
 other.

93
00:07:59,000 --> 00:08:02,490
 So I don't know if there's anyone actually listening to

94
00:08:02,490 --> 00:08:07,860
 this, but some of you might go back and watch, listen to it

95
00:08:07,860 --> 00:08:10,000
 after it's been reported.

96
00:08:10,000 --> 00:08:14,000
 Today I'm in Bangkok, I'm here until Thursday.

97
00:08:14,000 --> 00:08:18,880
 Just staying in the house. Today we actually have a few

98
00:08:18,880 --> 00:08:23,000
 people coming here to meditate in the house, which is good.

99
00:08:23,000 --> 00:08:30,030
 It would be the first time I've taught here in this

100
00:08:30,030 --> 00:08:32,000
 location.

101
00:08:32,000 --> 00:08:40,290
 I have a whole house to myself, but today there will be

102
00:08:40,290 --> 00:08:45,000
 people coming to meditate.

103
00:08:45,000 --> 00:08:50,980
 It looks like we're moving into a new house in July in time

104
00:08:50,980 --> 00:08:53,000
 for the rains.

105
00:08:53,000 --> 00:08:58,490
 We will have six rooms in the new house, which means we

106
00:08:58,490 --> 00:09:03,890
 could theoretically hold five meditators, but I think we're

107
00:09:03,890 --> 00:09:05,000
 not going to do that.

108
00:09:05,000 --> 00:09:10,220
 Not right away anyway. We'll try and keep it at three at

109
00:09:10,220 --> 00:09:18,000
 once, and we might allow for four if necessary.

110
00:09:18,000 --> 00:09:22,000
 We'll try to keep it at three for now.

111
00:09:22,000 --> 00:09:27,190
 If eventually we get someone as a steward to come and stay,

112
00:09:27,190 --> 00:09:29,000
 then we get up to four meditators.

113
00:09:29,000 --> 00:09:36,000
 Four meditators and a steward, five and I'm six.

114
00:09:36,000 --> 00:09:42,000
 That's good news.

115
00:09:42,000 --> 00:09:47,930
 I'm going to try to take a poly course in the fall at the

116
00:09:47,930 --> 00:09:51,000
 University of Toronto.

117
00:09:51,000 --> 00:09:55,000
 It's more personal than anything.

118
00:09:55,000 --> 00:09:58,000
 That's not even a big deal. I probably won't learn much.

119
00:09:58,000 --> 00:10:06,000
 Unfortunately, I think it's actually a fairly basic level.

120
00:10:06,000 --> 00:10:12,670
 But we'll see. There will be a chance to brush up anyway on

121
00:10:12,670 --> 00:10:14,000
 my poly.

122
00:10:14,000 --> 00:10:16,470
 Is there anyone out there with any questions? Is there

123
00:10:16,470 --> 00:10:18,000
 anyone out there at all?

124
00:10:18,000 --> 00:10:22,000
 Hey, Rubbin.

125
00:10:22,000 --> 00:10:29,000
 I've got one person to say.

126
00:10:29,000 --> 00:10:36,490
 Let's see. I'll try to be here again tomorrow and Wednesday

127
00:10:36,490 --> 00:10:37,000
.

128
00:10:37,000 --> 00:10:40,540
 Of course, that's waited. So it's still only Sunday for you

129
00:10:40,540 --> 00:10:41,000
 guys.

130
00:10:41,000 --> 00:10:45,000
 So Monday, Tuesday for you guys.

131
00:10:45,000 --> 00:10:51,250
 And then Wednesday night for you will be Thursday morning

132
00:10:51,250 --> 00:10:52,000
 for me.

133
00:10:52,000 --> 00:10:55,000
 And I might probably won't broadcast Thursday morning

134
00:10:55,000 --> 00:10:58,000
 because I think Thursday

135
00:10:58,000 --> 00:11:06,000
 morning I'm travelling to Sri Lanka.

136
00:11:07,000 --> 00:11:10,000
 So I'm going to try to be here tomorrow.

137
00:11:11,000 --> 00:11:11,000
 (

138
00:11:52,840 --> 00:11:55,840
)

139
00:11:55,840 --> 00:12:11,840
 (

140
00:12:11,840 --> 00:12:17,840
 )

