1
00:00:00,000 --> 00:00:07,000
 Good evening everyone.

2
00:00:07,000 --> 00:00:15,000
 I'm broadcasting live May 24th.

3
00:00:15,000 --> 00:00:24,890
 Tonight's quote is an answer to an often voiced criticism

4
00:00:24,890 --> 00:00:30,820
 of Buddhism and meditation practice in general they call

5
00:00:30,820 --> 00:00:33,000
 meditation navel gazing.

6
00:00:33,000 --> 00:00:39,710
 When you're just obsessed with yourself navel gazing as

7
00:00:39,710 --> 00:00:46,490
 though it's a useless or potentially narcissistic, auto

8
00:00:46,490 --> 00:00:49,890
 erotic was what the Pope called it, one of the previous pop

9
00:00:49,890 --> 00:00:50,000
es.

10
00:00:50,000 --> 00:00:57,000
 Yeah, meditation gets a bad rap.

11
00:00:57,000 --> 00:01:02,560
 You think of it as an escape running away from your

12
00:01:02,560 --> 00:01:07,640
 problems, a vacation, something you do at a spa or

13
00:01:07,640 --> 00:01:14,000
 something, something like yoga.

14
00:01:14,000 --> 00:01:19,560
 Not to malign or not to speak badly of yoga, but I don't

15
00:01:19,560 --> 00:01:25,660
 think meditation or meditation that we do as much like yoga

16
00:01:25,660 --> 00:01:31,600
, even though people often equate the two or compare the two

17
00:01:31,600 --> 00:01:32,000
.

18
00:01:32,000 --> 00:01:38,780
 Nonetheless, not speaking about yoga, Buddhist meditation

19
00:01:38,780 --> 00:01:47,000
 most sincerely is not an escape or a vacation. Right?

20
00:01:47,000 --> 00:01:52,400
 My meditator is nodding. It's a hard work. It's training.

21
00:01:52,400 --> 00:01:57,240
 Anyone who's come through this course can tell you. It's a

22
00:01:57,240 --> 00:02:00,180
 lot more than you usually signed up for. Usually not what

23
00:02:00,180 --> 00:02:02,000
 we had expected coming in.

24
00:02:02,000 --> 00:02:05,370
 And that's good. If it were what you expected, then it

25
00:02:05,370 --> 00:02:09,520
 wouldn't be working. And that's an important point because

26
00:02:09,520 --> 00:02:12,390
 it's enlightened. We're talking about enlightenment. We're

27
00:02:12,390 --> 00:02:14,000
 talking about waking up.

28
00:02:14,000 --> 00:02:17,430
 We're waking up. We're waking up to truths that we didn't

29
00:02:17,430 --> 00:02:22,120
 know. We didn't know. How could it not surprise you? How

30
00:02:22,120 --> 00:02:27,000
 could it not shock you? How could it not challenge you?

31
00:02:27,000 --> 00:02:36,270
 If it didn't, you couldn't really say that it was something

32
00:02:36,270 --> 00:02:41,270
 new, something that would change you, something that would

33
00:02:41,270 --> 00:02:43,000
 bring about positive change or negative change.

34
00:02:43,000 --> 00:02:46,500
 Or negative change could be similar. It's not to say that

35
00:02:46,500 --> 00:02:50,730
 the change or the hard work is, that all hard work is

36
00:02:50,730 --> 00:02:55,000
 necessarily positive. But meditation is hard work.

37
00:02:55,000 --> 00:03:01,450
 Nonetheless, the criticism being leveled here is that he

38
00:03:01,450 --> 00:03:08,300
 makes a comparison, as Brahman does. He says they perform

39
00:03:08,300 --> 00:03:12,000
 sacrifices to gods, I guess.

40
00:03:12,000 --> 00:03:14,740
 And so that benefits many people. He's talking about

41
00:03:14,740 --> 00:03:18,250
 general religious practice, like putting aside this Brahman

42
00:03:18,250 --> 00:03:22,000
's really ridiculous ideas of what is beneficial.

43
00:03:22,000 --> 00:03:26,920
 Like they would kill, they would slaughter animals, or they

44
00:03:26,920 --> 00:03:31,000
 would sacrifice butter to the fire. A lot of silly things.

45
00:03:31,000 --> 00:03:36,810
 And say that was wholesome. But let's look at wholesome

46
00:03:36,810 --> 00:03:43,010
 activity like people who work for social justice, or people

47
00:03:43,010 --> 00:03:49,550
 who run charities, soup kitchens, teachers, these kind of

48
00:03:49,550 --> 00:03:52,000
 things. People who help the world.

49
00:03:52,000 --> 00:03:57,420
 And they say, well that's true goodness. What is this

50
00:03:57,420 --> 00:04:01,800
 meditation? And he says, the meditation, okay, we can

51
00:04:01,800 --> 00:04:05,430
 accept that it helps you, but that's all it does, is help

52
00:04:05,430 --> 00:04:06,000
 you.

53
00:04:06,000 --> 00:04:10,930
 It's only good for yourself. He says, I say that such a

54
00:04:10,930 --> 00:04:16,000
 person is practicing something that benefits only himself.

55
00:04:16,000 --> 00:04:20,830
 Which to anyone who's put sincere or serious thought into

56
00:04:20,830 --> 00:04:25,000
 the matter, certainly must sound ridiculous.

57
00:04:25,000 --> 00:04:28,900
 But for people who are unsure, who are new to spirituality,

58
00:04:28,900 --> 00:04:33,080
 it sounds kind of convincing. Yeah, why am I wasting my

59
00:04:33,080 --> 00:04:38,040
 time in this meditations and doing things that only benefit

60
00:04:38,040 --> 00:04:39,000
 myself?

61
00:04:39,000 --> 00:04:43,040
 And agreed that there are meditations that I would say, for

62
00:04:43,040 --> 00:04:47,130
 the most part, only benefit yourself. If you enter into a

63
00:04:47,130 --> 00:04:50,620
 trance, a bliss state, a peaceful state, that would be

64
00:04:50,620 --> 00:04:52,000
 really only benefiting yourself.

65
00:04:52,000 --> 00:04:56,770
 And in the end, not benefiting yourself substantially

66
00:04:56,770 --> 00:05:00,000
 anyway, because it's temporary.

67
00:05:00,000 --> 00:05:03,410
 But this is an important point, because it helps clear up

68
00:05:03,410 --> 00:05:07,240
 this misunderstanding, this misconception, this prejudice

69
00:05:07,240 --> 00:05:08,000
 we have.

70
00:05:08,000 --> 00:05:11,640
 Preconception that we have that meditation should be

71
00:05:11,640 --> 00:05:15,710
 pleasant, meditation should be enjoyable. It can be, of

72
00:05:15,710 --> 00:05:19,000
 course, even this meditation can be at times enjoyable.

73
00:05:19,000 --> 00:05:22,000
 That's not what it's supposed to be.

74
00:05:22,000 --> 00:05:26,850
 It's actually supposed to better you, make you, you could

75
00:05:26,850 --> 00:05:32,730
 say, a better person. But more technically, just to make

76
00:05:32,730 --> 00:05:39,050
 you more pure, more clear, more mindful, more wise, more

77
00:05:39,050 --> 00:05:40,000
 good.

78
00:05:40,000 --> 00:05:43,000
 And if you hear these things, and then you ask yourself, is

79
00:05:43,000 --> 00:05:47,030
 this not beneficial to other people? He could say, well,

80
00:05:47,030 --> 00:05:51,000
 how is that beneficial to other people?

81
00:05:51,000 --> 00:05:56,140
 And so we can take a look at those people who try to help

82
00:05:56,140 --> 00:06:01,420
 the world, for social justice, or all these things I

83
00:06:01,420 --> 00:06:03,000
 mentioned.

84
00:06:03,000 --> 00:06:09,110
 And they're not all equal, right? They're not equally

85
00:06:09,110 --> 00:06:12,000
 successful. Many of them burn out.

86
00:06:12,000 --> 00:06:16,910
 There's a high, in the environmental movement, which my

87
00:06:16,910 --> 00:06:21,110
 father was very much involved in. He recently told me, I

88
00:06:21,110 --> 00:06:25,000
 think there's a very high burnout rate.

89
00:06:25,000 --> 00:06:28,500
 And people who are into social justice will tell you that

90
00:06:28,500 --> 00:06:31,960
 they flame bright and burn out quick, and then they're on

91
00:06:31,960 --> 00:06:36,000
 to something else, that the passion doesn't last.

92
00:06:36,000 --> 00:06:39,760
 It's because they're not trained. It's because they don't

93
00:06:39,760 --> 00:06:44,070
 have this ability, this power. And you don't really realize

94
00:06:44,070 --> 00:06:49,170
 the power and the strength and the clarity of mind that

95
00:06:49,170 --> 00:06:51,000
 comes from meditation until you actually do it.

96
00:06:51,000 --> 00:06:55,010
 That's why I say it surprises you. It surprises us in how

97
00:06:55,010 --> 00:06:59,470
 challenging it is, but it also surprises us in how deep it

98
00:06:59,470 --> 00:07:02,000
 goes, and how fundamentally it changes us.

99
00:07:02,000 --> 00:07:06,890
 It strengthens us, straightens us out. You come out of this

100
00:07:06,890 --> 00:07:09,550
 feeling all crooked, and like you were all bent out of

101
00:07:09,550 --> 00:07:15,390
 shape, and that you've just been wrenched back into more or

102
00:07:15,390 --> 00:07:18,000
 less straight state.

103
00:07:18,000 --> 00:07:21,000
 That's how it feels. It feels like you're untying knots.

104
00:07:21,000 --> 00:07:24,000
 That's how it should feel if you're doing it properly.

105
00:07:24,000 --> 00:07:28,520
 You're untying knots, like you're working out kinks, like

106
00:07:28,520 --> 00:07:33,000
 you're straightening out the crookedness in your mind.

107
00:07:33,000 --> 00:07:36,730
 Your mind is all bent out of shape. I mean, bent out of

108
00:07:36,730 --> 00:07:41,000
 shape is a simplification. It's all messed up, mixed up.

109
00:07:41,000 --> 00:07:43,840
 Because what we do doesn't have rhyme or reason. Much of it

110
00:07:43,840 --> 00:07:49,000
 is just based on whim. Our habits are not well thought out.

111
00:07:49,000 --> 00:07:52,090
 We've been working on our habits since we were children.

112
00:07:52,090 --> 00:07:55,000
 How could we know what was right and what was wrong?

113
00:07:55,000 --> 00:07:59,270
 And so we go through life kind of with a half-assed

114
00:07:59,270 --> 00:08:01,000
 understanding.

115
00:08:01,000 --> 00:08:07,000
 Half-assed is probably not the technical term. Half-baked.

116
00:08:07,000 --> 00:08:11,240
 Not fully formed understanding of what's right and what's

117
00:08:11,240 --> 00:08:15,000
 wrong. And so we make lots of mistakes.

118
00:08:15,000 --> 00:08:19,150
 We do write sometimes. We figure some things out. We all

119
00:08:19,150 --> 00:08:22,000
 have varying degrees of wisdom, so we use that.

120
00:08:22,000 --> 00:08:28,800
 But it's not fully formed. It's not coherent. It's all

121
00:08:28,800 --> 00:08:30,000
 mixed up.

122
00:08:30,000 --> 00:08:34,090
 And so meditation is quite simple. It's not something you

123
00:08:34,090 --> 00:08:38,000
 can doubt, because it's quite a simple activity.

124
00:08:38,000 --> 00:08:43,130
 It's straightening everything out. Straightening out our

125
00:08:43,130 --> 00:08:46,300
 minds. Working to understand our desires, our aversions,

126
00:08:46,300 --> 00:08:52,000
 our conceits and our arrogance and our views and opinions.

127
00:08:52,000 --> 00:08:59,000
 And overcome all of the delusion that we have inside.

128
00:08:59,000 --> 00:09:02,420
 Meditation really straightens us out, and there's nothing

129
00:09:02,420 --> 00:09:05,990
 better for other people than to be straightened out

130
00:09:05,990 --> 00:09:07,000
 yourself.

131
00:09:07,000 --> 00:09:10,800
 If you're straight yourself, first of all, if everyone did

132
00:09:10,800 --> 00:09:14,000
 this, there would be no need to help anyone else.

133
00:09:14,000 --> 00:09:16,900
 If everyone practiced meditation, then no one would need to

134
00:09:16,900 --> 00:09:20,000
 teach meditation. No one would need to help the world.

135
00:09:20,000 --> 00:09:24,230
 We have more than enough for everyone. And even enough is

136
00:09:24,230 --> 00:09:28,130
 not really meaningful, because the human state is just an

137
00:09:28,130 --> 00:09:29,000
 artifice.

138
00:09:29,000 --> 00:09:33,170
 We don't really need all of the things that a human has. We

139
00:09:33,170 --> 00:09:37,000
 can give up this human state and go to a purer state.

140
00:09:37,000 --> 00:09:41,620
 We can change the whole fabric of reality. Maybe that's

141
00:09:41,620 --> 00:09:45,500
 going too far for most people's understanding, but we can

142
00:09:45,500 --> 00:09:50,000
 certainly change the world if we're all positive.

143
00:09:50,000 --> 00:09:54,530
 But more than just all being well inside, our interactions

144
00:09:54,530 --> 00:10:00,740
 with others, how much suffering do we cause in the name of

145
00:10:00,740 --> 00:10:07,000
 beneficence, trying to do good things?

146
00:10:07,000 --> 00:10:13,590
 I mean, the Spanish Inquisition was ostensibly meant for

147
00:10:13,590 --> 00:10:18,670
 benefit. Hitler had an idea of beneficence that he was

148
00:10:18,670 --> 00:10:26,000
 actually helping the world by culling the lesser.

149
00:10:26,000 --> 00:10:29,190
 These are extreme examples, but we're all in this way. We

150
00:10:29,190 --> 00:10:33,000
 try to help the world, and we end up yelling and screaming

151
00:10:33,000 --> 00:10:35,000
 and getting frustrated and burning out.

152
00:10:35,000 --> 00:10:42,100
 We still get addicted and attached to our desires that mess

153
00:10:42,100 --> 00:10:47,000
 up and color our work and our beliefs.

154
00:10:47,000 --> 00:10:52,690
 So this should be a really... this sort of argument is

155
00:10:52,690 --> 00:10:56,720
 voiced far too often by people who clearly, if they voice

156
00:10:56,720 --> 00:11:01,370
 it, clearly have not thought or investigated the topic at

157
00:11:01,370 --> 00:11:02,000
 all.

158
00:11:02,000 --> 00:11:06,480
 That's easily debunked, but only if you've taken the time

159
00:11:06,480 --> 00:11:11,230
 to think and to work and follow the meditation practice, to

160
00:11:11,230 --> 00:11:16,000
 see the benefit, to see that it's not just navel-gazing.

161
00:11:16,000 --> 00:11:20,380
 Although, you know, the truth is, right at our navel, if

162
00:11:20,380 --> 00:11:24,520
 you do watch your stomach rising and falling, you can

163
00:11:24,520 --> 00:11:26,000
 change the world.

164
00:11:26,000 --> 00:11:29,840
 You can become enlightened just by watching your stomach

165
00:11:29,840 --> 00:11:37,000
 rise and fall. It's quite profound how simple it is.

166
00:11:37,000 --> 00:11:43,800
 Anyway, so that's a bit of dhamma for tonight. I think that

167
00:11:43,800 --> 00:11:47,830
's all I have to say about that. Let's look at some of the

168
00:11:47,830 --> 00:11:49,000
 questions.

169
00:11:49,000 --> 00:11:55,720
 Okay, question verse 37. I have a part-time job, live at

170
00:11:55,720 --> 00:12:01,000
 home with my mom and dad, not a very complicated person.

171
00:12:01,000 --> 00:12:04,270
 Is it not advisable to live a simple life and still

172
00:12:04,270 --> 00:12:07,000
 practice meditation? Of course.

173
00:12:07,000 --> 00:12:10,270
 I mean, that's what monastic life is supposed to be. The

174
00:12:10,270 --> 00:12:14,470
 monk life is... I don't know what verse 37 was. I can't

175
00:12:14,470 --> 00:12:16,000
 think that far back.

176
00:12:16,000 --> 00:12:21,330
 But living simply is great. So the idea of becoming a monk

177
00:12:21,330 --> 00:12:26,000
 is... the claim is it's the simplest way of life.

178
00:12:26,000 --> 00:12:36,000
 You put aside everything just to cultivate spirituality.

179
00:12:36,000 --> 00:12:39,000
 Okay, more question, long question.

180
00:12:39,000 --> 00:12:41,840
 I've been having doubts as to whether I actually want to

181
00:12:41,840 --> 00:12:44,910
 free my life of suffering and desire. Well, they're two

182
00:12:44,910 --> 00:12:46,000
 different things.

183
00:12:46,000 --> 00:12:50,660
 Desire wouldn't be a problem if it didn't lead to suffering

184
00:12:50,660 --> 00:12:51,000
.

185
00:12:51,000 --> 00:12:54,220
 Sounds quite strange and ignorant. Well, it's natural. Most

186
00:12:54,220 --> 00:12:57,000
 of us... this was the dhammapada verse last night.

187
00:12:57,000 --> 00:13:00,870
 I just re-recorded it. It'll be up soon. So maybe you can

188
00:13:00,870 --> 00:13:03,000
 watch that. Maybe that'll help.

189
00:13:03,000 --> 00:13:05,470
 I feel that suffering and desire and all the things that go

190
00:13:05,470 --> 00:13:08,000
 along with them are part of the typical human experience.

191
00:13:08,000 --> 00:13:10,770
 In some way I feel that I would be missing out. I tried to

192
00:13:10,770 --> 00:13:13,000
 remove them from my life.

193
00:13:13,000 --> 00:13:15,180
 So many of the greatest human achievements have been

194
00:13:15,180 --> 00:13:18,100
 inspired and motivated by these things. What would you say

195
00:13:18,100 --> 00:13:19,000
 about this?

196
00:13:19,000 --> 00:13:22,390
 It's a very good question. I mean, it's basically the dham

197
00:13:22,390 --> 00:13:24,000
mapada from last night.

198
00:13:24,000 --> 00:13:31,050
 We do things for... well, it's not based. It's in the same

199
00:13:31,050 --> 00:13:32,000
 vein.

200
00:13:32,000 --> 00:13:34,510
 We do things in the world. We all have these things in the

201
00:13:34,510 --> 00:13:39,000
 world. We want to achieve things. We want to obtain things.

202
00:13:39,000 --> 00:13:43,000
 We have many desires.

203
00:13:43,000 --> 00:13:48,060
 I mean, the first thing I can say before getting really

204
00:13:48,060 --> 00:13:54,000
 into it is that it's not a reason to desire.

205
00:13:54,000 --> 00:13:58,660
 Your desire is not a reason to desire. You see what I mean?

206
00:13:58,660 --> 00:13:59,000
 You say,

207
00:13:59,000 --> 00:14:02,650
 "I'm not sure if I want to give up desire." It's kind of

208
00:14:02,650 --> 00:14:06,600
 funny because of course you don't. That's what desire means

209
00:14:06,600 --> 00:14:07,000
.

210
00:14:07,000 --> 00:14:12,220
 We desire it, therefore, of course we don't want to get rid

211
00:14:12,220 --> 00:14:13,000
 of it.

212
00:14:13,000 --> 00:14:18,620
 So that kind of points to the means of overcoming the

213
00:14:18,620 --> 00:14:22,000
 problem. You can't approach desire directly.

214
00:14:22,000 --> 00:14:26,940
 This is why the Buddha put desire in a special category. Aj

215
00:14:26,940 --> 00:14:29,830
ahn Tong brought this up and really drew my attention to

216
00:14:29,830 --> 00:14:30,000
 this.

217
00:14:30,000 --> 00:14:34,000
 The difference between anger, for example anger and greed.

218
00:14:34,000 --> 00:14:36,610
 Anger is something you give up. You know it's bad. It feels

219
00:14:36,610 --> 00:14:40,000
 bad. It's not something we want. We don't want to be angry.

220
00:14:40,000 --> 00:14:43,000
 But greed, greed is something that can be pleasant.

221
00:14:43,000 --> 00:14:50,080
 "Sulmanasasahagatam" can come associated with pleasure. So

222
00:14:50,080 --> 00:14:52,000
 it's something that you have to train yourself out of.

223
00:14:52,000 --> 00:14:55,130
 It's not something you can just say, "No, I don't want." It

224
00:14:55,130 --> 00:14:58,000
's not something that we can approach directly.

225
00:14:58,000 --> 00:15:01,000
 But this is why we have to separate desire and suffering.

226
00:15:01,000 --> 00:15:03,750
 Don't worry about desire. Don't worry about the things that

227
00:15:03,750 --> 00:15:04,000
 you want.

228
00:15:04,000 --> 00:15:09,220
 That's not useful. That's not where we have to focus. Let's

229
00:15:09,220 --> 00:15:10,000
 not focus on that.

230
00:15:10,000 --> 00:15:13,690
 "Okay, you want all these things? Fine. Let's look at the

231
00:15:13,690 --> 00:15:16,220
 fact that you're suffering. Why are you suffering? Let's

232
00:15:16,220 --> 00:15:18,000
 learn about that. Let's study that."

233
00:15:18,000 --> 00:15:24,930
 And this helps you not get mixed up. Because desire, when

234
00:15:24,930 --> 00:15:30,720
 the mind has desire in it, it's unable, incapable of seeing

235
00:15:30,720 --> 00:15:33,000
 the detriment, seeing the problem.

236
00:15:33,000 --> 00:15:40,000
 This is what I was saying about the Dhampa. It's not

237
00:15:40,000 --> 00:15:40,000
 rational. You can't convince yourself not to want something

238
00:15:40,000 --> 00:15:40,000
.

239
00:15:40,000 --> 00:15:45,000
 The mind is in a state that it won't hear those arguments.

240
00:15:45,000 --> 00:15:49,000
 So that's not where you put your attention.

241
00:15:49,000 --> 00:15:53,560
 When you focus on the suffering, and try to learn why you

242
00:15:53,560 --> 00:15:57,960
're suffering, and try to experience and see what's going on

243
00:15:57,960 --> 00:16:00,000
 that's causing you suffering,

244
00:16:00,000 --> 00:16:03,500
 then you really can break it apart. Then you can see things

245
00:16:03,500 --> 00:16:05,000
 rationally and clearly.

246
00:16:05,000 --> 00:16:09,180
 And you can see how your desires are causing you stress. It

247
00:16:09,180 --> 00:16:16,000
's not intellectual. You'll just feel kind of exhausted.

248
00:16:16,000 --> 00:16:20,210
 Wanting the same thing again and again, getting it and then

249
00:16:20,210 --> 00:16:22,000
 not being satisfied.

250
00:16:22,000 --> 00:16:29,840
 And again and again and again. And eventually you get bored

251
00:16:29,840 --> 00:16:31,000
 of it.

252
00:16:31,000 --> 00:16:36,070
 This is how spiritual people are. Someone who is very

253
00:16:36,070 --> 00:16:40,190
 spiritual will feel like this. They'll feel generally bored

254
00:16:40,190 --> 00:16:41,000
 of life.

255
00:16:41,000 --> 00:16:45,510
 Like they've tried everything and they saw through it. This

256
00:16:45,510 --> 00:16:49,000
 is a sign of high-mindedness.

257
00:16:49,000 --> 00:16:52,290
 People who are depressed and want to kill themselves, often

258
00:16:52,290 --> 00:16:54,000
 it comes from a sort of a wisdom.

259
00:16:54,000 --> 00:16:57,910
 An understanding that there's just nothing to life. Life is

260
00:16:57,910 --> 00:17:01,000
 in the end just a game.

261
00:17:01,000 --> 00:17:06,000
 You play the game enough, you get tired of it.

262
00:17:06,000 --> 00:17:09,670
 Now most of us are not tired of it. We're still keen on

263
00:17:09,670 --> 00:17:11,000
 going after it.

264
00:17:11,000 --> 00:17:13,000
 And that won't come unless you meditate.

265
00:17:13,000 --> 00:17:18,270
 It starts with what is most coarse and most obvious. People

266
00:17:18,270 --> 00:17:21,000
 who are addicted to really unpleasant things like killing

267
00:17:21,000 --> 00:17:24,000
 or stealing or lying or cheating or drugs or alcohol.

268
00:17:24,000 --> 00:17:28,750
 People who are addicted to those things are pretty well

269
00:17:28,750 --> 00:17:34,000
 able to let go of them because they're intense suffering.

270
00:17:34,000 --> 00:17:37,370
 Now attachment to music or food or something like that is

271
00:17:37,370 --> 00:17:40,640
 much harder to see than most of us can or won't ever see it

272
00:17:40,640 --> 00:17:41,000
.

273
00:17:41,000 --> 00:17:44,500
 But when you look, the truth is regardless of whether we'll

274
00:17:44,500 --> 00:17:47,000
 see it or not or whether we want it or not.

275
00:17:47,000 --> 00:17:52,460
 The truth is when you look clearly you will give it up

276
00:17:52,460 --> 00:17:56,000
 because you will see undeniably.

277
00:17:56,000 --> 00:17:58,700
 You don't have to be convinced and you don't have to

278
00:17:58,700 --> 00:18:00,000
 convince yourself.

279
00:18:00,000 --> 00:18:02,990
 You will see without any doubt, any shred of doubt that it

280
00:18:02,990 --> 00:18:06,000
's not worth it. It's not beneficial.

281
00:18:06,000 --> 00:18:09,270
 It's not pleasant. It doesn't lead to happiness. It doesn't

282
00:18:09,270 --> 00:18:11,440
 make you a better person. It doesn't make you a happier

283
00:18:11,440 --> 00:18:12,000
 person.

284
00:18:12,000 --> 00:18:15,220
 And the funny thing about these things is if they did in

285
00:18:15,220 --> 00:18:19,000
 general make us happier people then we would see ourselves

286
00:18:19,000 --> 00:18:22,000
 constantly getting happier as we pursued these things.

287
00:18:22,000 --> 00:18:25,620
 Which in fact is not the case. There are things in life

288
00:18:25,620 --> 00:18:29,000
 that do make us happier but they are not seeking out

289
00:18:29,000 --> 00:18:30,000
 pleasure.

290
00:18:30,000 --> 00:18:33,160
 You don't become happier the more you listen to music. When

291
00:18:33,160 --> 00:18:35,250
 you listen to the music that you like you're happy and if

292
00:18:35,250 --> 00:18:38,000
 you vary it enough so that it's not repetitive.

293
00:18:38,000 --> 00:18:42,000
 Your brain is constantly stimulated. You can maintain that.

294
00:18:42,000 --> 00:18:45,000
 But you don't become happier.

295
00:18:45,000 --> 00:18:47,770
 Food doesn't make you happier. Sex doesn't make you happier

296
00:18:47,770 --> 00:18:54,000
. It makes you happy, pleasant, pleasure in that moment.

297
00:18:54,000 --> 00:19:00,050
 Not happier as a person. Unhappier. If you, for a hidden

298
00:19:00,050 --> 00:19:05,050
ness, a person who is intent upon this, have them stop and

299
00:19:05,050 --> 00:19:08,000
 put them in a room where they no longer have these things.

300
00:19:08,000 --> 00:19:13,000
 See how they fare compared to a person, an ordinary person.

301
00:19:13,000 --> 00:19:16,000
 They behave very much like a drug addict.

302
00:19:16,000 --> 00:19:19,280
 This is why jail is torture for so many of us. There was

303
00:19:19,280 --> 00:19:23,050
 this sensory deprivation chamber that they're talking about

304
00:19:23,050 --> 00:19:27,000
 where you can't hear anything. It's a perfect silence.

305
00:19:27,000 --> 00:19:29,780
 And they were saying people couldn't stay in there for more

306
00:19:29,780 --> 00:19:34,000
 than 45 minutes. They just started to go insane.

307
00:19:34,000 --> 00:19:37,980
 Now I can imagine it has an effect on the brain that's dis

308
00:19:37,980 --> 00:19:41,880
orienting but most of that is simply because we desire

309
00:19:41,880 --> 00:19:43,000
 stimulus.

310
00:19:43,000 --> 00:19:58,000
 Anyway, so I hope that helps.

311
00:19:58,000 --> 00:20:00,840
 When a person sleeps too few hours they may feel drowsy,

312
00:20:00,840 --> 00:20:03,930
 forgetful, have difficulty being aware, have difficulty

313
00:20:03,930 --> 00:20:08,000
 being mindful. It can feel a little like being drunk.

314
00:20:08,000 --> 00:20:12,130
 But in Buddhism we are taught to limit our sleep. How do we

315
00:20:12,130 --> 00:20:15,000
 understand this apparent conflict?

316
00:20:15,000 --> 00:20:22,520
 Well, the reason we feel so drowsy and drunk is because of

317
00:20:22,520 --> 00:20:28,000
 our bodies being accustomed to sleeping.

318
00:20:28,000 --> 00:20:32,220
 But also, not even just that, also because of how tired out

319
00:20:32,220 --> 00:20:36,000
 we become from our mental activity. If you're meditating

320
00:20:36,000 --> 00:20:38,000
 intensely you don't even need to sleep.

321
00:20:38,000 --> 00:20:41,000
 There are people who go days, weeks, months without sleep.

322
00:20:41,000 --> 00:20:45,300
 Your body acclimatizes to it so that drunk feeling reduces

323
00:20:45,300 --> 00:20:48,000
 and eventually doesn't even arise.

324
00:20:48,000 --> 00:20:52,400
 And also your mind becomes more refined and more

325
00:20:52,400 --> 00:20:54,000
 streamlined.

326
00:20:54,000 --> 00:20:59,280
 In the beginning it's also quite useful, and in the long

327
00:20:59,280 --> 00:21:04,000
 term it's useful in terms of pushing yourself.

328
00:21:04,000 --> 00:21:10,000
 When you push yourself beyond what you're comfortable with,

329
00:21:10,000 --> 00:21:13,100
 and most of us are not comfortable with sleeping a little,

330
00:21:13,100 --> 00:21:14,000
 then it agitates.

331
00:21:14,000 --> 00:21:18,450
 You're able to see your reactions and you're able to assess

332
00:21:18,450 --> 00:21:21,900
 your reactions. You're able to see what reactions are

333
00:21:21,900 --> 00:21:24,000
 wholesome and what are unwholesome.

334
00:21:24,000 --> 00:21:28,700
 If you always get what you want, you'll never come to see

335
00:21:28,700 --> 00:21:34,000
 the problem with desire, with the wanting.

336
00:21:34,000 --> 00:21:35,880
 Because you're always getting it. You say, "Well, that's

337
00:21:35,880 --> 00:21:38,000
 good. I want and therefore I get when I want, I get."

338
00:21:38,000 --> 00:21:41,360
 So when you start depriving yourself of things that you

339
00:21:41,360 --> 00:21:44,000
 want, sleep being a very good example.

340
00:21:44,000 --> 00:21:49,280
 And you start to see how we're just like baby cows, crying

341
00:21:49,280 --> 00:21:52,000
 out for our mother's milk.

342
00:21:52,000 --> 00:21:56,540
 You want to train the cow to grow up to be a good... well,

343
00:21:56,540 --> 00:21:58,000
 an ox, you know.

344
00:21:58,000 --> 00:22:03,320
 In old times they used ox and they had to train the ox. But

345
00:22:03,320 --> 00:22:06,000
 a baby ox was kind of useless.

346
00:22:06,000 --> 00:22:08,420
 So we had to train it. We had to take it away from its

347
00:22:08,420 --> 00:22:11,000
 mother. Kind of cruel, I know.

348
00:22:11,000 --> 00:22:13,340
 But in the long term it was for the betterment of the ox's

349
00:22:13,340 --> 00:22:16,000
 training. Now our minds are...

350
00:22:16,000 --> 00:22:19,000
 It's maybe cruel to ox, but it's not cruel to our minds.

351
00:22:19,000 --> 00:22:21,000
 Our minds we do have to train.

352
00:22:21,000 --> 00:22:24,760
 They are something that is necessary. Without training, our

353
00:22:24,760 --> 00:22:28,000
 minds, as I said, they're all mixed up.

354
00:22:28,000 --> 00:22:31,640
 A natural mind state is a chaotic mind state. It doesn't

355
00:22:31,640 --> 00:22:34,000
 work for one's benefit at all times.

356
00:22:34,000 --> 00:22:36,580
 You might say it's natural and you say, "Well, you know,

357
00:22:36,580 --> 00:22:39,000
 isn't that the way of life? Isn't that the way of humans?"

358
00:22:39,000 --> 00:22:42,080
 Sure. And look at the world. Look at the way of humans,

359
00:22:42,080 --> 00:22:43,000
 what it's brought.

360
00:22:43,000 --> 00:22:46,450
 It doesn't have to be this way. Just because it has been

361
00:22:46,450 --> 00:22:49,420
 this way or just because it seems like evolution has made

362
00:22:49,420 --> 00:22:50,000
 it this way

363
00:22:50,000 --> 00:22:53,170
 is in no way indicative of that it should be this way or

364
00:22:53,170 --> 00:22:55,000
 that it's better this way.

365
00:22:55,000 --> 00:22:59,110
 In fact, if anything human beings have shown in terms of

366
00:22:59,110 --> 00:23:02,000
 evolution is that evolution was in no way

367
00:23:03,000 --> 00:23:13,000
 a gift from God. It was just a random, chaotic sort of way

368
00:23:13,000 --> 00:23:20,000
 of promoting certain genes, certain genetic material.

369
00:23:20,000 --> 00:23:23,660
 And being human has shown us that we can get beyond our

370
00:23:23,660 --> 00:23:28,000
 genetics, beyond evolution, beyond natural selection.

371
00:23:28,000 --> 00:23:33,760
 We can be compassionate to cripples and sick and to all

372
00:23:33,760 --> 00:23:37,920
 types of people who would, in a survival of the fittest

373
00:23:37,920 --> 00:23:42,000
 type of society, just not survive.

374
00:23:42,000 --> 00:23:46,080
 Anyway, I sort of got a little off-track there, but I think

375
00:23:46,080 --> 00:23:50,000
 that's a fairly comprehensive answer.

376
00:23:50,000 --> 00:23:54,560
 There are several reasons to challenge you, but also to

377
00:23:54,560 --> 00:23:58,760
 force you to refine your mind and that eventually those

378
00:23:58,760 --> 00:24:00,000
 things go away.

379
00:24:00,000 --> 00:24:06,000
 Is Nibbana the greatest human achievement?

380
00:24:06,000 --> 00:24:10,990
 You sound like humans, you're asking whether humans created

381
00:24:10,990 --> 00:24:12,000
 Nibbana.

382
00:24:12,000 --> 00:24:22,400
 The greatest achievement a human can achieve is Nibbana,

383
00:24:22,400 --> 00:24:31,000
 for sure. It's the only thing that's of real benefit.

384
00:24:31,000 --> 00:24:38,830
 Because it's permanent, it's satisfying. It's not controll

385
00:24:38,830 --> 00:24:43,000
able, but it's not important.

386
00:24:43,000 --> 00:24:49,030
 Okay, still we're about 50/50, people who meditate, people

387
00:24:49,030 --> 00:24:51,000
 who don't meditate.

388
00:24:51,000 --> 00:24:55,070
 If your name is an orange, that means you don't meditate. I

389
00:24:55,070 --> 00:24:58,000
'd like you to see a greater percentage.

390
00:24:58,000 --> 00:25:03,130
 Anyway, I think that's all for tonight. I'm wishing you all

391
00:25:03,130 --> 00:25:06,000
 a good night, and I'll see you all next time.

392
00:25:08,000 --> 00:25:09,000
 Bye.

393
00:25:11,000 --> 00:25:12,000
 Thank you.

394
00:25:13,000 --> 00:25:14,000
 Bye.

395
00:25:14,000 --> 00:25:15,000
 Bye.

396
00:25:17,000 --> 00:25:18,000
 Bye.

