1
00:00:00,000 --> 00:00:03,780
 It is said that the church is far different than what Jesus

2
00:00:03,780 --> 00:00:06,720
 taught. Would you say that what is taught now is different

3
00:00:06,720 --> 00:00:08,000
 than what the Buddha taught?

4
00:00:08,000 --> 00:00:12,000
 It's a valid question, no?

5
00:00:12,000 --> 00:00:19,000
 Well, you know, the church, no? What is the church?

6
00:00:19,000 --> 00:00:23,000
 And what is what Jesus taught as well?

7
00:00:24,000 --> 00:00:27,270
 But we don't know what the Buddha taught, right? We don't

8
00:00:27,270 --> 00:00:30,000
 know if what we have is what the Buddha taught.

9
00:00:30,000 --> 00:00:33,290
 But I talked about this before. The most important thing is

10
00:00:33,290 --> 00:00:36,430
 not whether it was the Buddha who taught these things. The

11
00:00:36,430 --> 00:00:39,000
 importance is whether it's true.

12
00:00:39,000 --> 00:00:42,960
 And that's what's so profound about the Buddha's teaching

13
00:00:42,960 --> 00:00:46,730
 is that it's ehi bhatsiko. The Buddha said, "You come and

14
00:00:46,730 --> 00:00:48,000
 see for yourself."

15
00:00:49,000 --> 00:00:54,250
 So this is what we have to do in the Buddha's teaching. And

16
00:00:54,250 --> 00:00:58,440
 it doesn't quite answer the question because you can see

17
00:00:58,440 --> 00:01:00,540
 that many people have come and seen very, very different

18
00:01:00,540 --> 00:01:01,000
 things.

19
00:01:01,000 --> 00:01:06,130
 Many people interpret Buddhism to be something, to be very,

20
00:01:06,130 --> 00:01:08,000
 very different things.

21
00:01:13,000 --> 00:01:17,170
 So really the best you can do if you want to be totally

22
00:01:17,170 --> 00:01:21,930
 scientific about it is investigate the various Buddhist

23
00:01:21,930 --> 00:01:26,740
 traditions and look and see if they're internally

24
00:01:26,740 --> 00:01:28,000
 consistent.

25
00:01:28,000 --> 00:01:32,950
 Is this tradition internally consistent? Like the people

26
00:01:32,950 --> 00:01:38,960
 who claim this as the truth, are they actually practicing

27
00:01:38,960 --> 00:01:39,000
 it?

28
00:01:40,000 --> 00:01:42,880
 Or are they actually seem to have realized some of what

29
00:01:42,880 --> 00:01:46,000
 they're teaching? Are there any examples of that?

30
00:01:46,000 --> 00:01:51,330
 And then look and see if it's externally consistent. You

31
00:01:51,330 --> 00:01:55,690
 know, what they're saying is fine and good, but is there

32
00:01:55,690 --> 00:02:02,000
 any observable proof that it is true?

33
00:02:03,000 --> 00:02:05,370
 So that it doesn't just become a faith thing like, "God

34
00:02:05,370 --> 00:02:07,930
 said this, God said that. If you do this, you'll go to God

35
00:02:07,930 --> 00:02:09,000
 and go to heaven."

36
00:02:09,000 --> 00:02:14,960
 That's the best you can do. I mean, obviously everyone's

37
00:02:14,960 --> 00:02:18,000
 going to say ours is the... this is what the Buddha taught.

38
00:02:18,000 --> 00:02:20,000
 There are some people who say even,

39
00:02:20,000 --> 00:02:23,200
 "Oh, there are a few traditions that I've heard this from

40
00:02:23,200 --> 00:02:26,510
 where they say the true teachings of the Buddha were hidden

41
00:02:26,510 --> 00:02:30,810
 for thousands of years and then this teacher, our teacher,

42
00:02:30,810 --> 00:02:32,000
 discovered them again."

43
00:02:32,000 --> 00:02:36,130
 And this is the lineage of the Buddha. Just totally

44
00:02:36,130 --> 00:02:39,120
 ignoring all the other traditions, like all these other

45
00:02:39,120 --> 00:02:41,000
 traditions that were there before.

46
00:02:41,000 --> 00:02:46,000
 As though they were nothing or as they didn't exist.

47
00:02:47,000 --> 00:02:53,150
 But there is some other... another aspect of this question

48
00:02:53,150 --> 00:02:58,500
 is that there are people who acknowledge that, say, the

49
00:02:58,500 --> 00:03:03,170
 tradition that we follow, or the Theravada in general, say

50
00:03:03,170 --> 00:03:06,620
 the Pali Canon, for example, is most likely what the Buddha

51
00:03:06,620 --> 00:03:07,000
 taught.

52
00:03:08,000 --> 00:03:10,670
 But then they say, "That's not good enough." You see, so it

53
00:03:10,670 --> 00:03:13,830
 gets quite complicated. Just because it's what the Buddha

54
00:03:13,830 --> 00:03:17,400
 taught doesn't mean it's what we should do. They say the

55
00:03:17,400 --> 00:03:19,300
 Buddha taught many things just because people couldn't

56
00:03:19,300 --> 00:03:20,000
 understand the truth.

57
00:03:20,000 --> 00:03:24,000
 And now later on, people can better understand the truth.

58
00:03:24,000 --> 00:03:30,360
 There's that opinion as well. And so now we have to use

59
00:03:30,360 --> 00:03:33,000
 different teachings.

60
00:03:34,000 --> 00:03:37,200
 It's very interesting. And they even claim that there are

61
00:03:37,200 --> 00:03:40,610
 some later teachings of the Buddha that were hidden. And

62
00:03:40,610 --> 00:03:44,520
 the Buddha hid them because he knew that people couldn't

63
00:03:44,520 --> 00:03:46,000
 understand them.

64
00:03:46,000 --> 00:03:49,370
 And then after he passed away, they were discovered. It's a

65
00:03:49,370 --> 00:03:53,150
 little bit suspicious, but sorry, I don't mean to make too

66
00:03:53,150 --> 00:03:54,000
 much fun.

67
00:03:55,000 --> 00:04:00,040
 But just to point out that it's not necessarily enough to

68
00:04:00,040 --> 00:04:04,790
 ask whether it's the Buddha's teaching. And so I would say

69
00:04:04,790 --> 00:04:08,240
 it's not really the most important thing. The importance is

70
00:04:08,240 --> 00:04:15,360
 that it's true, valid, and that you can understand the

71
00:04:15,360 --> 00:04:17,000
 benefit of it.

72
00:04:18,000 --> 00:04:21,230
 You can understand the goodness of it and the rightness of

73
00:04:21,230 --> 00:04:24,400
 it. Because the other thing is that there are different

74
00:04:24,400 --> 00:04:27,780
 paths. Following the Buddha's teaching will lead you to

75
00:04:27,780 --> 00:04:34,230
 become enlightened as a follower. You'll become free from

76
00:04:34,230 --> 00:04:35,000
 suffering.

77
00:04:36,000 --> 00:04:38,880
 It won't necessarily lead you to become a Buddha, where you

78
00:04:38,880 --> 00:04:42,470
 know everything. So you might become free from suffering

79
00:04:42,470 --> 00:04:46,000
 and free from all, from everything. Let go of everything.

80
00:04:46,000 --> 00:04:49,540
 But you may not come to know everything. Most likely won't,

81
00:04:49,540 --> 00:04:53,110
 because the Buddha didn't teach everything that he knew. He

82
00:04:53,110 --> 00:04:55,830
 didn't teach you how to come to know everything because he

83
00:04:55,830 --> 00:04:59,430
 realized that it wasn't really necessary. It wasn't really

84
00:04:59,430 --> 00:05:01,000
 of any benefit.

85
00:05:02,000 --> 00:05:05,870
 Or it wasn't of the most immediate benefit. So some people

86
00:05:05,870 --> 00:05:08,790
 say, well, immediate benefit is all and good, but what

87
00:05:08,790 --> 00:05:12,000
 about the next Buddha when there's nobody around to teach?

88
00:05:12,000 --> 00:05:14,000
 So then they decide they want to become a Buddha.

89
00:05:14,000 --> 00:05:17,030
 And so therefore they don't pay so much attention to the

90
00:05:17,030 --> 00:05:20,300
 Buddha's teaching. Anyway, that wasn't what you asked. What

91
00:05:20,300 --> 00:05:22,710
 you asked is what I say, what is taught now is different

92
00:05:22,710 --> 00:05:25,530
 from what the Buddha taught. And I've kind of been ignoring

93
00:05:25,530 --> 00:05:26,000
 that.

94
00:05:27,000 --> 00:05:32,160
 But I did say that we don't really know. But one more thing

95
00:05:32,160 --> 00:05:35,480
 that could be said is that when you practice, you really do

96
00:05:35,480 --> 00:05:40,390
 know. This is the claim anyway. The claim that we make, the

97
00:05:40,390 --> 00:05:45,720
 claim that is made is that a person who practices the

98
00:05:45,720 --> 00:05:50,650
 Buddha's teaching will come to realize that the person who

99
00:05:50,650 --> 00:05:56,000
 taught this was a Buddha, was enlightened, knew the truth,

100
00:05:56,000 --> 00:05:56,000
 the objective.

101
00:05:56,000 --> 00:06:02,310
 And they'll say this teaching is the truth, the objective

102
00:06:02,310 --> 00:06:06,030
 truth. They'll come to realize that for themselves because

103
00:06:06,030 --> 00:06:09,550
 they'll come to see Nibbana. I mean, Nibbana is really the

104
00:06:09,550 --> 00:06:10,000
 key.

105
00:06:10,000 --> 00:06:15,050
 When a person realizes Nibbana, they realize that there is

106
00:06:15,050 --> 00:06:19,000
 nothing in Samsara of any real benefit.

107
00:06:20,000 --> 00:06:22,580
 And it's not that they reject it, but they just stop

108
00:06:22,580 --> 00:06:25,590
 clinging to it. They stop looking for things. They stop

109
00:06:25,590 --> 00:06:27,000
 seeking after things.

110
00:06:27,000 --> 00:06:32,700
 And so it's objective. You realize it for yourself. And

111
00:06:32,700 --> 00:06:35,520
 this is the claim is that it's objective and it's objective

112
00:06:35,520 --> 00:06:38,760
 truth. So whether the Buddha taught it or not is not the

113
00:06:38,760 --> 00:06:42,630
 most important. But if you want an answer to that, you know

114
00:06:42,630 --> 00:06:45,600
, once you use the word Buddha, then what you mean by the

115
00:06:45,600 --> 00:06:48,000
 word Buddha is someone who has realized the objective truth

116
00:06:48,000 --> 00:06:48,000
.

117
00:06:49,000 --> 00:06:51,520
 And then what you mean by the teaching, you know, you have

118
00:06:51,520 --> 00:06:54,520
 this teaching in these books. If the teaching in the books

119
00:06:54,520 --> 00:06:58,170
 allows you to realize the objective truth, then yes, you

120
00:06:58,170 --> 00:07:01,000
 know that it was taught by the Buddha.

121
00:07:01,000 --> 00:07:05,000
 So that's as good of an answer as I think I can give.

122
00:07:06,000 --> 00:07:07,000
 Thank you.

