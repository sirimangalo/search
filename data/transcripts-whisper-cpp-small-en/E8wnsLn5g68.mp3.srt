1
00:00:00,000 --> 00:00:05,000
 So good evening everyone. Welcome to our live broadcast.

2
00:00:05,000 --> 00:00:10,000
 November 21st, 2015.

3
00:00:10,000 --> 00:00:20,000
 Apparently last night's Dhammapada video was too long.

4
00:00:20,000 --> 00:00:24,520
 I'm told that no one has time to sit through a 47-minute

5
00:00:24,520 --> 00:00:25,000
 video.

6
00:00:25,000 --> 00:00:29,220
 And normally, you know, comments like this will go

7
00:00:29,220 --> 00:00:31,000
 unanswered and discussed.

8
00:00:31,000 --> 00:00:34,630
 I'm not reading all comments, so please don't expect me to

9
00:00:34,630 --> 00:00:36,000
 reply to your comments.

10
00:00:36,000 --> 00:00:39,700
 But I do read the first couple on each video just to make

11
00:00:39,700 --> 00:00:42,500
 sure the video got uploaded correctly and no one's having

12
00:00:42,500 --> 00:00:43,000
 problems.

13
00:00:43,000 --> 00:00:46,020
 Sometimes the first couple of comments tell me that the

14
00:00:46,020 --> 00:00:49,000
 audio was no good or was missing or something like this.

15
00:00:49,000 --> 00:00:52,000
 But no, it was too long.

16
00:00:52,000 --> 00:00:56,330
 I just thought it interesting to comment because a 47-

17
00:00:56,330 --> 00:01:00,000
minute Dhamma talk is usually considered to be too short.

18
00:01:00,000 --> 00:01:02,690
 So if people are having trouble, and I know this is a thing

19
00:01:02,690 --> 00:01:04,000
 really, it's true.

20
00:01:04,000 --> 00:01:07,840
 YouTube actually tells you no one's going to watch a 47-

21
00:01:07,840 --> 00:01:09,000
minute video.

22
00:01:09,000 --> 00:01:12,000
 You shouldn't upload them. You should upload short videos.

23
00:01:12,000 --> 00:01:17,320
 When you learn about what YouTube has to say, I mean, they

24
00:01:17,320 --> 00:01:19,000
 know from experience.

25
00:01:19,000 --> 00:01:22,050
 I think I've even got a tool that lets me see how many

26
00:01:22,050 --> 00:01:25,000
 minutes of my video people watch on average.

27
00:01:25,000 --> 00:01:29,160
 It says out of a 47-minute video, average number of minutes

28
00:01:29,160 --> 00:01:33,000
 watched, it's probably about three.

29
00:01:33,000 --> 00:01:37,000
 I don't know. I don't use the tool that I can check.

30
00:01:37,000 --> 00:01:42,160
 But it's a bit of a shame really because, as I said, and

31
00:01:42,160 --> 00:01:48,000
 for good reason, Dhamma Dogs 47 minutes, that's short.

32
00:01:48,000 --> 00:01:51,470
 I've given a couple of hour Dhamma, two-hour Dhamma talk

33
00:01:51,470 --> 00:01:53,000
 once, I think.

34
00:01:53,000 --> 00:01:58,000
 People, after two hours, people start to complain a bit.

35
00:01:58,000 --> 00:02:02,000
 But two or three hours is not unheard of.

36
00:02:02,000 --> 00:02:06,420
 One hour is a pretty good length for a talk. That's usually

37
00:02:06,420 --> 00:02:08,000
 what we aim for.

38
00:02:08,000 --> 00:02:11,180
 Now, I know the Dhamma Padha videos have been shorter, and

39
00:02:11,180 --> 00:02:13,000
 some of the videos don't require it.

40
00:02:13,000 --> 00:02:17,000
 And I'm probably going more in-depth and more off-track.

41
00:02:17,000 --> 00:02:21,090
 And so I appreciate that. I will try to stay on track,

42
00:02:21,090 --> 00:02:25,000
 maybe a little more on track than before.

43
00:02:25,000 --> 00:02:31,540
 But, being said, shouldn't be the length. It should be the

44
00:02:31,540 --> 00:02:32,000
 content.

45
00:02:32,000 --> 00:02:37,000
 If the content is objectionable, then that's a problem.

46
00:02:37,000 --> 00:02:46,000
 Anyway, not a big deal. I just thought it was interesting.

47
00:02:46,000 --> 00:02:49,080
 We have a quote, a quote from the Buddha's Last Moments.

48
00:02:49,080 --> 00:02:51,000
 That's actually a really good quote.

49
00:02:51,000 --> 00:03:01,000
 It's one that Ajahn Tong brings up a lot.

50
00:03:01,000 --> 00:03:19,000
 We're fired at it anyway.

51
00:03:19,000 --> 00:03:32,000
 [Hindi]

52
00:03:32,000 --> 00:04:01,000
 [Hindi]

53
00:04:01,000 --> 00:04:05,000
 There's a word that's not normally there.

54
00:04:05,000 --> 00:04:13,000
 [Hindi]

55
00:04:13,000 --> 00:04:16,000
 This word isn't in all of the versions.

56
00:04:16,000 --> 00:04:26,650
 Apaciyati baramaya pujaya, which means any monk, female or

57
00:04:26,650 --> 00:04:32,000
 male, or lay disciple, male or female,

58
00:04:32,000 --> 00:04:37,440
 who practices the Dhamma in line with the Dhamma or leading

59
00:04:37,440 --> 00:04:39,000
 to the Dhamma,

60
00:04:39,000 --> 00:04:44,930
 who dwells practicing the Dhamma in order to attain the D

61
00:04:44,930 --> 00:04:46,000
hamma.

62
00:04:46,000 --> 00:04:54,000
 Sami ji pati bhanu, who practices properly.

63
00:04:54,000 --> 00:04:59,000
 Anudhammachari, one who fares according to the Dhamma,

64
00:04:59,000 --> 00:05:05,110
 so that Ajahn Tong sakaroti, such a person does right by

65
00:05:05,110 --> 00:05:08,000
 the Buddha, sakaroti.

66
00:05:08,000 --> 00:05:14,000
 Garungaroti makes reverence to the Buddha, makes homage.

67
00:05:14,000 --> 00:05:21,260
 Mahaneti holds up high, holds in their mind, honors, thinks

68
00:05:21,260 --> 00:05:22,000
 highly of.

69
00:05:22,000 --> 00:05:30,770
 Pujati, "revernances." Apaciyati, no idea what Apaciyati is

70
00:05:30,770 --> 00:05:31,000
.

71
00:05:31,000 --> 00:05:34,000
 Not in all of the versions.

72
00:05:34,000 --> 00:05:41,000
 Paramaayapujaayu, it's the highest form of reverence.

73
00:05:41,000 --> 00:05:48,680
 Tasmatihananda, dhamma, nudhamma, pati bhanu, vihali sama,

74
00:05:48,680 --> 00:05:52,000
 sami ji pati bhanu, anudhammachari no.

75
00:05:52,000 --> 00:05:59,770
 Therefore, Ananda, you all should dwell practicing the Dham

76
00:05:59,770 --> 00:06:05,000
ma in order to attain the Dhamma.

77
00:06:05,000 --> 00:06:10,520
 Should practice properly, should fare according to the Dham

78
00:06:10,520 --> 00:06:11,000
ma,

79
00:06:11,000 --> 00:06:17,000
 lest you should train yourselves.

80
00:06:17,000 --> 00:06:23,000
 So that's... Ah yes, you have that actually in English.

81
00:06:23,000 --> 00:06:42,000
 [Silence]

82
00:06:42,000 --> 00:06:46,000
 So...

83
00:06:46,000 --> 00:06:48,000
 Do we have any questions for tonight?

84
00:06:48,000 --> 00:06:50,000
 We do.

85
00:06:50,000 --> 00:06:53,540
 "Thinking about self and non-self, or what is or what is

86
00:06:53,540 --> 00:06:58,000
 not, I came to the idea that there is just happening.

87
00:06:58,000 --> 00:07:00,910
 Is this idea of just happening in line with Buddhist

88
00:07:00,910 --> 00:07:03,000
 teaching moments of happening?

89
00:07:03,000 --> 00:07:07,000
 Actions acting upon actions?"

90
00:07:07,000 --> 00:07:10,000
 Yeah.

91
00:07:10,000 --> 00:07:15,010
 Yeah, I mean that sort of insight is kind of the object

92
00:07:15,010 --> 00:07:17,000
ivity poking through.

93
00:07:17,000 --> 00:07:20,120
 It means you're starting to become more objective. That

94
00:07:20,120 --> 00:07:21,000
 sounds what it sounds like.

95
00:07:21,000 --> 00:07:24,000
 Don't hold on to it. That's a raft.

96
00:07:24,000 --> 00:07:27,800
 That raft changed your mind, so it got you across something

97
00:07:27,800 --> 00:07:28,000
.

98
00:07:28,000 --> 00:07:31,000
 Now throw the raft away and keep going.

99
00:07:31,000 --> 00:07:36,000
 [Silence]

100
00:07:36,000 --> 00:07:39,000
 Hello, Bante. Can you talk about body scanning?

101
00:07:39,000 --> 00:07:42,000
 It should always be categorized as samatha, right?

102
00:07:42,000 --> 00:07:45,680
 Many people seem to associate practicing mindfulness with

103
00:07:45,680 --> 00:07:47,000
 doing body scanning,

104
00:07:47,000 --> 00:07:51,470
 or even associate the term vipassana with body scanning,

105
00:07:51,470 --> 00:07:53,000
 maybe due to goenka.

106
00:07:53,000 --> 00:07:56,500
 The more I have practiced insight meditation, the less I

107
00:07:56,500 --> 00:07:58,000
 have done body scanning.

108
00:07:58,000 --> 00:08:00,000
 Should I stop it completely?

109
00:08:00,000 --> 00:08:04,000
 I've noticed it's easy to get attached to the tranquility.

110
00:08:04,000 --> 00:08:06,580
 How to think about this and how to talk about it to the

111
00:08:06,580 --> 00:08:10,160
 people that have the practicing mindfulness equals body

112
00:08:10,160 --> 00:08:11,000
 scanning view?

113
00:08:11,000 --> 00:08:15,790
 If one wants to practice vipassana intensively and advance

114
00:08:15,790 --> 00:08:18,000
 one's understanding of reality,

115
00:08:18,000 --> 00:08:21,000
 is it counterproductive to do body scanning once in a while

116
00:08:21,000 --> 00:08:23,000
, or can it be useful somehow?

117
00:08:23,000 --> 00:08:27,000
 Sorry for the poor articulation. I'm tired or something.

118
00:08:27,000 --> 00:08:28,000
 Thank you.

119
00:08:28,000 --> 00:08:32,000
 Right.

120
00:08:32,000 --> 00:08:35,240
 You know, the word body scanning, I mean words are really

121
00:08:35,240 --> 00:08:36,000
 problematic.

122
00:08:36,000 --> 00:08:41,000
 And people have catchphrases or labels for things.

123
00:08:41,000 --> 00:08:45,110
 Like our, for example, our technique is often talked about

124
00:08:45,110 --> 00:08:48,000
 as labeling or noting.

125
00:08:48,000 --> 00:08:52,410
 Acknowledging was a big one. Acknowledging. But

126
00:08:52,410 --> 00:08:55,000
 acknowledging is a problematic term.

127
00:08:55,000 --> 00:08:59,720
 Anyway, a little bit off track. The point being, body

128
00:08:59,720 --> 00:09:00,000
 scanning,

129
00:09:00,000 --> 00:09:03,000
 in order to understand it, we really have to break it down

130
00:09:03,000 --> 00:09:05,000
 and see what you're doing.

131
00:09:05,000 --> 00:09:10,000
 Because body scanning is not, when you say body scanning,

132
00:09:10,000 --> 00:09:12,000
 it does not immediately mean that it's samatha.

133
00:09:12,000 --> 00:09:15,000
 No, I don't think that's fair to say.

134
00:09:15,000 --> 00:09:19,420
 As to whether it is proper meditation and whether it is vip

135
00:09:19,420 --> 00:09:20,000
assana.

136
00:09:20,000 --> 00:09:25,000
 Right. The question would be whether it's proper meditation

137
00:09:25,000 --> 00:09:25,000
.

138
00:09:25,000 --> 00:09:28,680
 Because vipassana depends on the object. Now, body scanning

139
00:09:28,680 --> 00:09:29,000
.

140
00:09:29,000 --> 00:09:40,060
 Body scanning could be problematic if it, if it deals with

141
00:09:40,060 --> 00:09:47,000
 a body, like moving through the body.

142
00:09:47,000 --> 00:09:50,070
 First of all, I would say there's no reason to suggest that

143
00:09:50,070 --> 00:09:52,000
 one should practice in this way.

144
00:09:52,000 --> 00:09:57,850
 I can't think of. I can't think of a single, even say the w

145
00:09:57,850 --> 00:10:04,000
isudimaga, passage which encourages body scanning.

146
00:10:04,000 --> 00:10:07,190
 There may be. But I don't, I can't think of anything that

147
00:10:07,190 --> 00:10:08,000
 hints at it.

148
00:10:08,000 --> 00:10:14,210
 I can't even, I don't even, now maybe Lady Sayadaw says

149
00:10:14,210 --> 00:10:16,000
 something about this.

150
00:10:16,000 --> 00:10:19,000
 And maybe it is in the commentary somewhere.

151
00:10:19,000 --> 00:10:23,000
 I would imagine that even Lady Sayadaw didn't.

152
00:10:23,000 --> 00:10:26,280
 I mean, that would be a good question. Did Lady Sayadaw,

153
00:10:26,280 --> 00:10:30,000
 who was supposed to be the teacher of the teacher of Goenka

154
00:10:30,000 --> 00:10:30,000
.

155
00:10:30,000 --> 00:10:34,000
 And Lady Sayadaw is usually who they go back to.

156
00:10:34,000 --> 00:10:36,610
 So the question would be whether Lady Sayadaw or Weibu Say

157
00:10:36,610 --> 00:10:40,000
adaw, who I think is another one, a student of Lady Sayadaw.

158
00:10:40,000 --> 00:10:44,300
 These guys talked about body scanning and where they got it

159
00:10:44,300 --> 00:10:45,000
 from.

160
00:10:45,000 --> 00:10:49,200
 So, I mean, that is something. When there's no, when there

161
00:10:49,200 --> 00:10:53,110
's no long standing tradition or you can't bring it back to

162
00:10:53,110 --> 00:10:54,000
 the Buddha,

163
00:10:54,000 --> 00:10:56,890
 you can't bring it back to the sort of things that the

164
00:10:56,890 --> 00:10:58,000
 Buddha taught.

165
00:10:58,000 --> 00:11:01,760
 If you contrast that with what we do, as I've been pointing

166
00:11:01,760 --> 00:11:04,000
 out, especially in our study of the wisudimaga,

167
00:11:04,000 --> 00:11:08,000
 there is very strong precedent for this kind of practice.

168
00:11:08,000 --> 00:11:13,820
 In the wisudimaga, it literally, it directly explains to

169
00:11:13,820 --> 00:11:16,000
 practice in its way.

170
00:11:16,000 --> 00:11:18,000
 This is how you practice meditation.

171
00:11:18,000 --> 00:11:22,930
 In the satipatthana sutta, the Buddha himself appears to be

172
00:11:22,930 --> 00:11:26,000
 at least subject to interpretation,

173
00:11:26,000 --> 00:11:29,570
 but the grammar itself supports directly saying to yourself

174
00:11:29,570 --> 00:11:34,000
 things like walking, walking or angry, angry,

175
00:11:34,000 --> 00:11:38,000
 or something.

176
00:11:38,000 --> 00:11:43,480
 It's pretty easy to read that into the, why I'm saying some

177
00:11:43,480 --> 00:11:44,000
 people deny this,

178
00:11:44,000 --> 00:11:46,220
 some people say, no, that's not what it says, because it

179
00:11:46,220 --> 00:11:47,000
 depends what you mean by the grammar,

180
00:11:47,000 --> 00:11:51,000
 but it literally says, "kachantova gachamiti bhachanati."

181
00:11:51,000 --> 00:11:54,290
 "Kachamiti" means "I am walking," and "ti" is a quote. So

182
00:11:54,290 --> 00:11:56,000
 it is a quote.

183
00:11:56,000 --> 00:11:59,060
 No, but the word, the verb is "bhajanati," which means "kn

184
00:11:59,060 --> 00:12:00,000
ows clearly."

185
00:12:00,000 --> 00:12:05,000
 So one knows clearly, quote, unquote, "I am walking."

186
00:12:05,000 --> 00:12:09,960
 That coupled with the tradition of how meditation was

187
00:12:09,960 --> 00:12:15,000
 practiced is a clear indication that what we're doing is

188
00:12:15,000 --> 00:12:16,000
 not something new

189
00:12:16,000 --> 00:12:19,000
 or off the wall or far-fetched.

190
00:12:19,000 --> 00:12:24,000
 Now, body scanning, just as a precursor, I want to say that

191
00:12:24,000 --> 00:12:28,000
, that I don't think it's well supported by the text.

192
00:12:28,000 --> 00:12:31,220
 Could be wrong, and it would be interesting to talk to them

193
00:12:31,220 --> 00:12:32,000
 about that.

194
00:12:32,000 --> 00:12:35,430
 But that having been said, that's not that we shouldn't be

195
00:12:35,430 --> 00:12:36,000
 dogmatic,

196
00:12:36,000 --> 00:12:40,000
 so dogmatic as to discard it as a result of that.

197
00:12:40,000 --> 00:12:43,540
 But as I said, we have a bit of a problem because it may

198
00:12:43,540 --> 00:12:46,000
 encourage the concept of a body.

199
00:12:46,000 --> 00:12:54,110
 The other thing it may encourage is self in the sense of

200
00:12:54,110 --> 00:12:58,000
 actively seeking out.

201
00:12:58,000 --> 00:13:01,290
 Now, there's a little bit of that in what we do, but we're

202
00:13:01,290 --> 00:13:02,000
 careful to...

203
00:13:02,000 --> 00:13:05,010
 It's a sensitive subject. It's something that we have to be

204
00:13:05,010 --> 00:13:09,000
 sensitive to, that we're not actually forcing or seeking.

205
00:13:09,000 --> 00:13:12,360
 And so we tell you to focus on the stomach rising and

206
00:13:12,360 --> 00:13:15,000
 falling, and you could argue that that's, you know,

207
00:13:15,000 --> 00:13:18,330
 pushing yourself to stay with the stomach could be, and it

208
00:13:18,330 --> 00:13:24,000
 can become, somewhat forceful and based on concepts of

209
00:13:24,000 --> 00:13:25,000
 control.

210
00:13:25,000 --> 00:13:28,000
 But we're fairly careful not to let it become that.

211
00:13:28,000 --> 00:13:31,780
 We say that's where we start, but we're completely open to

212
00:13:31,780 --> 00:13:35,100
 letting the mind go into what is often called choiceless

213
00:13:35,100 --> 00:13:36,000
 awareness.

214
00:13:36,000 --> 00:13:41,000
 It is pretty choiceless. We're not exactly choosing.

215
00:13:41,000 --> 00:13:44,910
 Even though we do choose the stomach, we do it as a base

216
00:13:44,910 --> 00:13:47,000
 more than anything.

217
00:13:47,000 --> 00:13:51,000
 I mean, there's still that potential criticism.

218
00:13:51,000 --> 00:13:55,930
 I think some people would say, "Well, if you're going to be

219
00:13:55,930 --> 00:13:57,000
 totally...

220
00:13:57,000 --> 00:13:58,950
 ...you're going to be sincere about this, it should be

221
00:13:58,950 --> 00:14:00,000
 completely choiceless."

222
00:14:00,000 --> 00:14:03,000
 Of course, that's a problem, and that doesn't really work.

223
00:14:03,000 --> 00:14:04,000
 You can't do that.

224
00:14:04,000 --> 00:14:06,000
 And so as a result, we do use some control.

225
00:14:06,000 --> 00:14:10,000
 And I think the biggest argument for either practice,

226
00:14:10,000 --> 00:14:12,360
 whether it's the body scan or whether it's staying with the

227
00:14:12,360 --> 00:14:14,000
 stomach rising and falling,

228
00:14:14,000 --> 00:14:20,940
 is that you need to start from a point of control in order

229
00:14:20,940 --> 00:14:23,000
 to just, you know, get your feet,

230
00:14:23,000 --> 00:14:26,710
 in order to just get balanced enough to start to see, and

231
00:14:26,710 --> 00:14:29,000
 to see how control breaks down.

232
00:14:29,000 --> 00:14:35,000
 So it's not exactly control, but there is the potential.

233
00:14:35,000 --> 00:14:39,110
 And I think a good thing about the body scan is that it is

234
00:14:39,110 --> 00:14:42,000
...it's not partial.

235
00:14:42,000 --> 00:14:46,700
 So the idea is if you scan from head to feet, you're not

236
00:14:46,700 --> 00:14:50,000
 choosing this part or that part based on your preference.

237
00:14:50,000 --> 00:14:52,440
 You're not going to certain things and avoiding other

238
00:14:52,440 --> 00:14:53,000
 things.

239
00:14:53,000 --> 00:14:55,180
 You're forced to go through the entire body, I think. I've

240
00:14:55,180 --> 00:14:56,000
 never practiced.

241
00:14:56,000 --> 00:15:00,000
 I think they do from head to feet.

242
00:15:00,000 --> 00:15:04,610
 But that being said, apart from those minor thoughts about

243
00:15:04,610 --> 00:15:06,000
 it, I don't think about it too much.

244
00:15:06,000 --> 00:15:08,570
 And probably this is...probably I shouldn't have said even

245
00:15:08,570 --> 00:15:11,000
 that much because it's not our technique.

246
00:15:11,000 --> 00:15:16,590
 And I don't like to talk about things that don't concern us

247
00:15:16,590 --> 00:15:17,000
.

248
00:15:17,000 --> 00:15:19,000
 But it allowed me to talk about our technique.

249
00:15:19,000 --> 00:15:23,030
 And I think rather than attack other people's techniques, I

250
00:15:23,030 --> 00:15:24,000
 would say that about our technique,

251
00:15:24,000 --> 00:15:29,860
 that it's well represented in the text. And so that's why

252
00:15:29,860 --> 00:15:34,000
 we practice this way.

253
00:15:34,000 --> 00:15:39,000
 Everything was a lot of question as to what you should do.

254
00:15:39,000 --> 00:15:42,000
 Should you stop it completely?

255
00:15:42,000 --> 00:15:44,260
 You should practice one way or the other. If you're

256
00:15:44,260 --> 00:15:46,000
 practicing our technique, practice our technique.

257
00:15:46,000 --> 00:15:48,330
 If you're practicing that technique, practice that

258
00:15:48,330 --> 00:15:49,000
 technique.

259
00:15:49,000 --> 00:15:52,950
 You shouldn't mix. You shouldn't because that becomes based

260
00:15:52,950 --> 00:15:54,000
 on your partiality.

261
00:15:54,000 --> 00:15:57,830
 Mixing, apart from being confusing, it's also usually based

262
00:15:57,830 --> 00:15:59,000
 on preference.

263
00:15:59,000 --> 00:16:01,000
 I like to do this sometimes and that sometimes.

264
00:16:01,000 --> 00:16:03,000
 So when I feel like doing this, I'll do this.

265
00:16:03,000 --> 00:16:07,470
 When I feel like doing that, I'll do that. And that's

266
00:16:07,470 --> 00:16:13,000
 hugely problematic.

267
00:16:13,000 --> 00:16:17,160
 How about the idea of talking about this to people who have

268
00:16:17,160 --> 00:16:19,000
 the body scanning view?

269
00:16:19,000 --> 00:16:22,730
 We're not about changing people's views. If they have that

270
00:16:22,730 --> 00:16:24,000
 view, then power to them.

271
00:16:24,000 --> 00:16:27,000
 If they're looking to change, then let them change.

272
00:16:27,000 --> 00:16:30,000
 I'm not trying to convince the world of our practice.

273
00:16:30,000 --> 00:16:33,080
 Usually if people even want us to convince them, we say, "

274
00:16:33,080 --> 00:16:36,000
You know, it's too much trouble for me."

275
00:16:36,000 --> 00:16:40,320
 I mean, that's what sort of the example we get from the A

276
00:16:40,320 --> 00:16:44,800
rahants in the texts, for the most part, they would be, you

277
00:16:44,800 --> 00:16:45,000
 know,

278
00:16:45,000 --> 00:16:47,240
 teaching you would be too much trouble is the kind of thing

279
00:16:47,240 --> 00:16:48,000
 they would say.

280
00:16:48,000 --> 00:16:52,070
 It's important. It's important not to pander, you know, and

281
00:16:52,070 --> 00:17:02,000
 not to obsess over changing other people.

282
00:17:02,000 --> 00:17:04,290
 So look at yourself. At that point, you should look at your

283
00:17:04,290 --> 00:17:05,000
 own thoughts.

284
00:17:05,000 --> 00:17:08,700
 You know, do you understand this? Okay. Do it the way you

285
00:17:08,700 --> 00:17:10,000
 understand it.

286
00:17:10,000 --> 00:17:12,570
 And then you ask yourself, maybe I'm doing it wrong. That's

287
00:17:12,570 --> 00:17:15,000
 what you have to figure out.

288
00:17:15,000 --> 00:17:18,000
 Everyone should look at their own feet when they walk.

289
00:17:18,000 --> 00:17:21,290
 Not literally. When we do walking meditation, don't look at

290
00:17:21,290 --> 00:17:22,000
 your feet.

291
00:17:22,000 --> 00:17:29,000
 And as the Buddha said, we watch our own path. We don't

292
00:17:29,000 --> 00:17:34,000
 worry about the footsteps of others.

293
00:17:34,000 --> 00:17:38,000
 The question about the meditator list. Excuse me.

294
00:17:38,000 --> 00:17:40,940
 What does the plus one and the number shown between the two

295
00:17:40,940 --> 00:17:42,000
 hands indicate?

296
00:17:42,000 --> 00:17:45,290
 Mine has changed from five to two. I'm fairly new to the

297
00:17:45,290 --> 00:17:46,000
 layout.

298
00:17:46,000 --> 00:17:48,000
 I don't think that's possible.

299
00:17:48,000 --> 00:17:52,000
 Unless you're using the Android app, it does weird things.

300
00:17:52,000 --> 00:17:53,000
 I don't know how to fix it.

301
00:17:53,000 --> 00:17:57,050
 Yeah, the Android app sometimes shows that shows in green,

302
00:17:57,050 --> 00:18:00,000
 meaning that you've already clicked on it when you haven't.

303
00:18:00,000 --> 00:18:03,450
 So that's strange. If you don't move the list, it fixes

304
00:18:03,450 --> 00:18:05,000
 itself on the next update.

305
00:18:05,000 --> 00:18:08,790
 But if it's broken down, something goes kind of funny when

306
00:18:08,790 --> 00:18:11,000
 because there's something wrong with it.

307
00:18:11,000 --> 00:18:13,000
 It's going to be fixed.

308
00:18:13,000 --> 00:18:18,000
 You need someone who's smart.

309
00:18:18,000 --> 00:18:21,700
 The plus one means, plus one, that means you've done

310
00:18:21,700 --> 00:18:24,000
 something good in Anumodana.

311
00:18:24,000 --> 00:18:25,000
 It's likes.

312
00:18:25,000 --> 00:18:31,000
 It's likes. It's likes. It's like a Facebook like.

313
00:18:31,000 --> 00:18:36,000
 So it should, I don't think they can go down, but.

314
00:18:36,000 --> 00:18:38,000
 Who knows, huh?

315
00:18:38,000 --> 00:18:42,000
 Yeah, technical difficulties.

316
00:18:42,000 --> 00:18:45,400
 On weekends, I try to practice the precepts. I've noticed

317
00:18:45,400 --> 00:18:48,140
 strong craving, but there doesn't seem to be any object for

318
00:18:48,140 --> 00:18:49,000
 the craving.

319
00:18:49,000 --> 00:18:52,000
 It seems like the mind is just hungry.

320
00:18:52,000 --> 00:18:55,140
 I can watch this hungry mind for only so long that I end up

321
00:18:55,140 --> 00:18:56,000
 feeding it.

322
00:18:56,000 --> 00:19:00,690
 Is the idea to feed it but in the most wholesome way

323
00:19:00,690 --> 00:19:02,000
 possible?

324
00:19:02,000 --> 00:19:06,850
 I think by feed it, you're talking sort of a figurative

325
00:19:06,850 --> 00:19:08,000
 feeding.

326
00:19:08,000 --> 00:19:11,000
 I hope.

327
00:19:11,000 --> 00:19:17,000
 Talking about feeding the mind, right?

328
00:19:17,000 --> 00:19:21,270
 I mean, the best is if you can learn to overcome and let go

329
00:19:21,270 --> 00:19:22,000
 of it.

330
00:19:22,000 --> 00:19:25,610
 But yeah, if you're right, so you're trying to keep the

331
00:19:25,610 --> 00:19:27,550
 eight precepts and then you end up breaking the eight

332
00:19:27,550 --> 00:19:32,200
 precepts maybe by listening to music or something or

333
00:19:32,200 --> 00:19:35,000
 watching a movie or something.

334
00:19:35,000 --> 00:19:37,340
 I mean, there are ways to do it without breaking the

335
00:19:37,340 --> 00:19:39,840
 precepts. It would just sort of distract you, but it's kind

336
00:19:39,840 --> 00:19:41,000
 of beside the point.

337
00:19:41,000 --> 00:19:45,000
 In the end, you have to learn to overcome it.

338
00:19:45,000 --> 00:19:47,110
 If you're keeping the eight precepts, you should keep the

339
00:19:47,110 --> 00:19:50,000
 eight precepts. It's only temporary.

340
00:19:50,000 --> 00:19:54,000
 It's teaching you self-control if nothing else.

341
00:19:54,000 --> 00:19:56,320
 But it's, I mean, that's part of the reason for keeping the

342
00:19:56,320 --> 00:19:58,710
 eight precepts is to learn about these, learn about your

343
00:19:58,710 --> 00:20:00,000
 desires and to have a chance.

344
00:20:00,000 --> 00:20:09,180
 Because if you constantly have the ability to indulge, to

345
00:20:09,180 --> 00:20:12,260
 satisfy your desires, you'll never get to see what this

346
00:20:12,260 --> 00:20:13,000
 desire is like.

347
00:20:13,000 --> 00:20:17,000
 You'll never really get to understand what desire is.

348
00:20:17,000 --> 00:20:19,870
 You'll never have a chance to challenge this idea that

349
00:20:19,870 --> 00:20:23,510
 desire is worth having and that desire is a sign that you

350
00:20:23,510 --> 00:20:27,000
 should chase after what you're looking for.

351
00:20:27,000 --> 00:20:32,530
 So as you watch the desire without indulging in it, you're

352
00:20:32,530 --> 00:20:34,710
 able to see a middle way, you know, a way of just being

353
00:20:34,710 --> 00:20:38,000
 with desire without acting on it or without repressing it.

354
00:20:38,000 --> 00:20:41,000
 That's what we're looking for.

355
00:20:41,000 --> 00:20:43,600
 That's what you're talking about is not the idea. The idea

356
00:20:43,600 --> 00:20:47,390
 is not to feed it. But if you feed it, then just be as

357
00:20:47,390 --> 00:20:49,000
 mindful as you can.

358
00:20:49,000 --> 00:20:56,610
 Of course, if you're keeping eight precepts, you shouldn't

359
00:20:56,610 --> 00:20:58,000
 feed it.

360
00:20:58,000 --> 00:21:01,350
 Hello, Bante. Could you please tell whether it is true that

361
00:21:01,350 --> 00:21:04,650
 one has to cycle through the insight knowledges many, many

362
00:21:04,650 --> 00:21:07,750
 times after third path before actually arriving at fourth

363
00:21:07,750 --> 00:21:08,000
 path?

364
00:21:08,000 --> 00:21:10,890
 Or does one only have to get through one more progress of

365
00:21:10,890 --> 00:21:14,110
 insight cycle after third path to attain fourth path?

366
00:21:14,110 --> 00:21:16,000
 Thanks.

367
00:21:16,000 --> 00:21:20,750
 It only requires one cycle, but that's technically

368
00:21:20,750 --> 00:21:26,250
 realistically, it usually requires many, but there's two

369
00:21:26,250 --> 00:21:28,000
 different cycles.

370
00:21:28,000 --> 00:21:33,760
 One cycle is only going to be a review, so it's not going

371
00:21:33,760 --> 00:21:38,000
 to be all 16 stages of knowledge.

372
00:21:38,000 --> 00:21:42,730
 You can actually only go through the stages of knowledge

373
00:21:42,730 --> 00:21:44,000
 four times.

374
00:21:44,000 --> 00:21:47,160
 So you're not actually going through all the stages of

375
00:21:47,160 --> 00:21:50,000
 knowledge, not technically, because it's missing.

376
00:21:50,000 --> 00:21:52,780
 It's missing one until you reach the next path. But I think

377
00:21:52,780 --> 00:21:56,000
 that's really a technicality.

378
00:21:56,000 --> 00:21:59,810
 Every time you go through the knowledges, you reduce the

379
00:21:59,810 --> 00:22:02,000
 number of you reduce your default.

380
00:22:02,000 --> 00:22:05,430
 So they get cut off piece by piece by piece. When you reach

381
00:22:05,430 --> 00:22:07,510
 a certain point, that's called anagami and you reach

382
00:22:07,510 --> 00:22:10,000
 another and you reach the final point where there's none

383
00:22:10,000 --> 00:22:10,000
 left.

384
00:22:10,000 --> 00:22:28,000
 That's.

385
00:22:28,000 --> 00:22:33,000
 There is one more. Could you please talk about how an anag

386
00:22:33,000 --> 00:22:36,960
ami progresses to our hardship? I am at high equanimity

387
00:22:36,960 --> 00:22:38,000
 after third path.

388
00:22:38,000 --> 00:22:44,000
 Any advice? I got frustrated after practicing a long time.

389
00:22:44,000 --> 00:22:50,470
 Advice. Here's a person with yellow. Orange who hasn't done

390
00:22:50,470 --> 00:22:52,000
 any meditation with us.

391
00:22:52,000 --> 00:22:56,710
 Anagamis don't get frustrated. I'm not going to answer your

392
00:22:56,710 --> 00:22:58,630
 question. If you start meditating with us, I think I'm not

393
00:22:58,630 --> 00:23:02,000
 going to answer these sorts of questions either.

394
00:23:02,000 --> 00:23:05,290
 You want to meditate with us. You should start logging your

395
00:23:05,290 --> 00:23:08,240
 meditation. Read my booklet. Start practicing according to

396
00:23:08,240 --> 00:23:09,000
 our technique.

397
00:23:09,000 --> 00:23:11,940
 Start logging your meditation. And if you have questions

398
00:23:11,940 --> 00:23:14,000
 about your meditation, I'm happy to answer them.

399
00:23:14,000 --> 00:23:18,730
 I'm not going to talk about these. It's not something I can

400
00:23:18,730 --> 00:23:26,680
 verify and the sounds of it. There's a misunderstanding of

401
00:23:26,680 --> 00:23:30,000
 what an anagami is.

402
00:23:30,000 --> 00:23:34,000
 Sorry.

403
00:23:34,000 --> 00:23:36,940
 And with that, we are caught up. Nobody clicks on people's

404
00:23:36,940 --> 00:23:40,000
 hands anymore, do they? I used to.

405
00:23:40,000 --> 00:23:49,000
 I do sometimes. There's a lot of them. Click, click, click.

406
00:23:49,000 --> 00:23:54,030
 What we don't do very often is up on the top part, click to

407
00:23:54,030 --> 00:23:58,000
 like questions and comments and things.

408
00:23:58,000 --> 00:24:20,000
 I forget to do that.

409
00:24:20,000 --> 00:24:23,250
 OK, that's all then for tonight. Thank you all for tuning

410
00:24:23,250 --> 00:24:24,000
 in.

411
00:24:24,000 --> 00:24:27,000
 Good night. Thank you. Thank you, Bante.

412
00:24:27,000 --> 00:24:29,000
 Good night.

413
00:24:30,000 --> 00:24:31,000
 Thank you.

414
00:24:33,000 --> 00:24:34,000
 Thank you.

