1
00:00:00,000 --> 00:00:03,300
 Can meditation help cure agoraphobia?

2
00:00:03,300 --> 00:00:07,640
 Agra... Agro?

3
00:00:07,640 --> 00:00:10,980
 I think it's agrophobia, fear of outside.

4
00:00:10,980 --> 00:00:13,520
 Agra... Agrophobia.

5
00:00:13,520 --> 00:00:17,820
 I know agrophobia is fear of heights. Is she spelling it

6
00:00:17,820 --> 00:00:18,980
 correctly or am I just wrong?

7
00:00:18,980 --> 00:00:20,700
 What is agro... Agro...

8
00:00:20,700 --> 00:00:22,700
 Agrophobia is fear of going outside, huh?

9
00:00:22,700 --> 00:00:24,400
 I know agro... Agrophobia is...

10
00:00:24,400 --> 00:00:26,400
 Oh, that might be that you are...

11
00:00:26,400 --> 00:00:28,900
 Oh, could you Miriam specify?

12
00:00:30,040 --> 00:00:33,160
 Agrophobia. I know agrophobia I thought was fear of heights

13
00:00:33,160 --> 00:00:36,950
 because it's the same as the Pali word aga, which means

14
00:00:36,950 --> 00:00:38,740
 height or pinnacle.

15
00:00:38,740 --> 00:00:42,040
 Maybe I'm wrong.

16
00:00:42,040 --> 00:00:44,040
 Agro...

17
00:00:44,040 --> 00:00:48,520
 Agrophobia. I don't know.

18
00:00:48,520 --> 00:00:53,080
 Maybe we can get back to that.

19
00:00:53,080 --> 00:00:55,080
 I can't help you with your phobias.

20
00:00:55,080 --> 00:00:55,580
 Sorry.

21
00:00:55,580 --> 00:00:56,080
 I think...

22
00:00:56,080 --> 00:00:57,580
 I can't help you with any phobia.

23
00:00:57,580 --> 00:01:02,580
 I think, yeah, it can be taken more general even if we don

24
00:01:02,580 --> 00:01:06,960
't know the word agra... Agro.

25
00:01:06,960 --> 00:01:09,960
 For sure.

26
00:01:09,960 --> 00:01:11,460
 Go ahead.

27
00:01:11,460 --> 00:01:20,720
 If practiced properly, it can certainly...

28
00:01:24,780 --> 00:01:32,420
 Not all kinds of meditation can probably cure phobias.

29
00:01:32,420 --> 00:01:38,610
 And it depends, of course, of the intensity or the time

30
00:01:38,610 --> 00:01:42,360
 that you spend meditating.

31
00:01:43,360 --> 00:01:51,610
 And the most important of the willingness and the

32
00:01:51,610 --> 00:01:55,720
 capability to let go.

33
00:01:55,720 --> 00:02:05,040
 It's probably so that a phobia could be cured rather

34
00:02:05,040 --> 00:02:08,160
 quickly with meditation.

35
00:02:08,560 --> 00:02:14,860
 But when the mind is still attached and when the mind cl

36
00:02:14,860 --> 00:02:20,880
ings to this attachment or clings to being attached,

37
00:02:20,880 --> 00:02:26,880
 then a phobia cannot be let go of.

38
00:02:29,200 --> 00:02:36,220
 So you don't have to work only on the phobias, but more

39
00:02:36,220 --> 00:02:40,000
 even on the letting go part of it.

40
00:02:40,000 --> 00:02:48,470
 Because a phobia is nothing, as far as I understand the

41
00:02:48,470 --> 00:02:50,960
 term, but I'm not a specialist,

42
00:02:50,960 --> 00:02:58,760
 it's nothing that is chemical or so it is quite strong

43
00:02:58,760 --> 00:03:00,800
 neurosis.

44
00:03:00,800 --> 00:03:09,060
 And like a really deep sitting intense fear in this case of

45
00:03:09,060 --> 00:03:13,440
 going out in public places.

46
00:03:14,080 --> 00:03:24,330
 So yes, when you meditate, vipassana meditation, and note

47
00:03:24,330 --> 00:03:27,040
 every single moment,

48
00:03:27,040 --> 00:03:37,070
 and you note every fear that arises, and you know it

49
00:03:37,070 --> 00:03:39,920
 arising and seizing again,

50
00:03:40,800 --> 00:03:47,460
 then you might be able to understand it, not understand in

51
00:03:47,460 --> 00:03:49,120
 terms to question,

52
00:03:49,120 --> 00:03:53,200
 where does it comes from, why do I have it, and all that.

53
00:03:53,200 --> 00:03:58,160
 Because this is off topic, this is not important for that

54
00:03:58,160 --> 00:03:59,040
 question.

55
00:03:59,040 --> 00:04:11,660
 What's important is to understand when it is there and when

56
00:04:11,660 --> 00:04:12,880
 it is not there.

57
00:04:12,880 --> 00:04:20,750
 And even in five seconds time, let's say, or ten seconds

58
00:04:20,750 --> 00:04:21,280
 time,

59
00:04:21,280 --> 00:04:28,330
 there will probably, if you say now with your knowledge of

60
00:04:28,330 --> 00:04:29,920
 now, you would say,

61
00:04:29,920 --> 00:04:35,470
 well, all that five seconds, all that ten seconds, there

62
00:04:35,470 --> 00:04:38,320
 was the agoraphobia.

63
00:04:38,320 --> 00:04:45,180
 But in reality, there was probably one second or even less

64
00:04:45,180 --> 00:04:47,280
 of agoraphobia,

65
00:04:47,280 --> 00:04:54,770
 and another second of the thought, oh, I have fear, and

66
00:04:54,770 --> 00:04:56,640
 then even something different,

67
00:04:56,640 --> 00:04:58,640
 and another thought came up.

68
00:04:58,640 --> 00:05:05,110
 So when you are honest with this, and try to really let go

69
00:05:05,110 --> 00:05:12,080
 of the idea that I have this,

70
00:05:12,080 --> 00:05:16,640
 then I would say, yes, possible.

71
00:05:16,640 --> 00:05:22,000
 Yeah, just to expand on that, the real problem, as you said

72
00:05:22,000 --> 00:05:22,320
,

73
00:05:22,320 --> 00:05:26,320
 what you're saying is that it's the identification.

74
00:05:26,320 --> 00:05:31,230
 Fear is one thing, fear is anger based, but fear, as with

75
00:05:31,230 --> 00:05:32,480
 all of our defilements,

76
00:05:32,480 --> 00:05:40,040
 are rooted in delusion, in ignorance, in the idea that they

77
00:05:40,040 --> 00:05:41,760
 are an entity.

78
00:05:41,760 --> 00:05:46,720
 One, that they're ours, two, that they have a core.

79
00:05:46,720 --> 00:05:51,060
 For example, in this case, you will say, I have agoraphobia

80
00:05:51,060 --> 00:05:52,800
, which we now learn is

81
00:05:52,800 --> 00:05:58,200
 an anxiety disorder characterized by anxiety in situations

82
00:05:58,200 --> 00:06:00,480
 where it is perceived to be difficult

83
00:06:00,480 --> 00:06:03,600
 or embarrassing to escape.

84
00:06:03,600 --> 00:06:07,460
 Someone else is just fear of being in public places, being

85
00:06:07,460 --> 00:06:08,960
 around people and so on.

86
00:06:08,960 --> 00:06:12,160
 So you think that you have this condition.

87
00:06:12,160 --> 00:06:15,220
 So first of all, you say it's mine, second of all, you say

88
00:06:15,220 --> 00:06:16,480
 it's a condition.

89
00:06:16,480 --> 00:06:19,300
 This goes with really anything, whether it be physical

90
00:06:19,300 --> 00:06:20,640
 diseases or mental,

91
00:06:20,640 --> 00:06:24,640
 more critically, with mental sicknesses.

92
00:06:24,640 --> 00:06:28,010
 People who are depressed will say, I have clinical

93
00:06:28,010 --> 00:06:30,160
 depression, or I have this kind of,

94
00:06:30,160 --> 00:06:35,120
 I'm bipolar, or I'm autistic, or X, Y, or Z.

95
00:06:35,120 --> 00:06:38,780
 So we give it a self of its own, we give it a life of its

96
00:06:38,780 --> 00:06:40,720
 own, when in fact,

97
00:06:40,720 --> 00:06:44,200
 as Bhanyani was saying, it's just moment to moment

98
00:06:44,200 --> 00:06:45,360
 experiences.

99
00:06:45,360 --> 00:06:51,300
 So really, the fear is a very small thing and not of much

100
00:06:51,300 --> 00:06:54,240
 consequence.

101
00:06:54,240 --> 00:06:56,880
 What is a much more consequence is the clinging to it.

102
00:06:56,880 --> 00:07:00,290
 So if you're just saying to yourself, afraid, afraid, you

103
00:07:00,290 --> 00:07:04,880
'll come to see that it's just fear.

104
00:07:04,880 --> 00:07:06,560
 And it doesn't have any significance.

105
00:07:06,560 --> 00:07:09,280
 It doesn't mean that you should or shouldn't go outside.

106
00:07:09,280 --> 00:07:11,520
 I would say one thing that probably the more you meditate,

107
00:07:11,520 --> 00:07:14,190
 the less interested you are in going out into public places

108
00:07:14,190 --> 00:07:14,400
.

109
00:07:14,400 --> 00:07:18,220
 So it's not like, yes, then I'll be able to go out and be a

110
00:07:18,220 --> 00:07:21,280
 party animal or a social butterfly.

111
00:07:21,280 --> 00:07:23,200
 So no, not likely.

112
00:07:23,200 --> 00:07:25,600
 You may have less fear of going outside, but you'll have

113
00:07:25,600 --> 00:07:26,240
 much more

114
00:07:26,240 --> 00:07:30,880
 distaste for going outside or disinterest in going outside.

115
00:07:31,840 --> 00:07:36,560
 So people might not even notice the difference,

116
00:07:36,560 --> 00:07:38,780
 but you'll notice the difference yourself because not out

117
00:07:38,780 --> 00:07:39,200
 of fear,

118
00:07:39,200 --> 00:07:42,080
 it'll be out of wisdom and the realization that it's,

119
00:07:42,080 --> 00:07:44,240
 there's a reason that these fears arise because there's so

120
00:07:44,240 --> 00:07:46,560
 many defilements involved in public

121
00:07:46,560 --> 00:07:54,320
 appearances and situations, society and so on.

122
00:07:54,320 --> 00:07:58,540
 All of our defilements become multiplied when we're around

123
00:07:58,540 --> 00:07:59,520
 other people.

124
00:08:00,640 --> 00:08:05,620
 And so this is why we develop fear of them because of the

125
00:08:05,620 --> 00:08:06,880
 feeling of being inadequate

126
00:08:06,880 --> 00:08:10,800
 or memory that we did stupid things, said stupid things,

127
00:08:10,800 --> 00:08:14,560
 that we were socially inept and so on.

128
00:08:14,560 --> 00:08:17,040
 And of course, many other reasons, but it all has to do

129
00:08:17,040 --> 00:08:19,920
 with the complexity of social interaction,

130
00:08:19,920 --> 00:08:23,280
 which to some extent can be mitigated.

131
00:08:23,280 --> 00:08:26,640
 But in the end, it's generally just to be avoided because

132
00:08:26,640 --> 00:08:28,720
 you can't stop people from

133
00:08:30,000 --> 00:08:32,810
 being silly. You go to a social situation and say, "I'll

134
00:08:32,810 --> 00:08:34,080
 just be mindful," but

135
00:08:34,080 --> 00:08:38,250
 everyone's drunk and hitting on each other and fighting

136
00:08:38,250 --> 00:08:40,960
 with each other and saying silly things,

137
00:08:40,960 --> 00:08:46,660
 doing silly things. In the end, you just decide it's better

138
00:08:46,660 --> 00:08:51,440
 not to leave the house.

