1
00:00:00,000 --> 00:00:06,340
 Okay and another question today this one from BD 951.

2
00:00:06,340 --> 00:00:08,200
 Thanks for the videos can

3
00:00:08,200 --> 00:00:11,340
 you speak about working with physical pain how important is

4
00:00:11,340 --> 00:00:12,160
 it not to move

5
00:00:12,160 --> 00:00:15,720
 during meditation? Do you recommend any type of stretching

6
00:00:15,720 --> 00:00:19,760
 or yoga? Thanks. Okay

7
00:00:19,760 --> 00:00:25,560
 working with physical pain well short and sweet I think

8
00:00:25,560 --> 00:00:27,560
 there are recognized

9
00:00:27,560 --> 00:00:33,330
 to be two types of pain the kind of pain that is going to

10
00:00:33,330 --> 00:00:34,520
 hurt but not injure

11
00:00:34,520 --> 00:00:38,460
 you and the kind of pain that is not only going to hurt but

12
00:00:38,460 --> 00:00:39,320
 is going to

13
00:00:39,320 --> 00:00:45,130
 injure you or is it is associated with an injury. Pain that

14
00:00:45,130 --> 00:00:46,040
 is just going to

15
00:00:46,040 --> 00:00:49,640
 hurt it doesn't matter how long you sit with it it's just

16
00:00:49,640 --> 00:00:50,840
 going to hurt and when

17
00:00:50,840 --> 00:00:55,430
 you get up it's going to go away. Another type of pain if

18
00:00:55,430 --> 00:00:56,920
 you sit for too long it's

19
00:00:56,920 --> 00:01:00,400
 going to actually injure you or it's going to it's going to

20
00:01:00,400 --> 00:01:01,200
 have severe

21
00:01:01,200 --> 00:01:05,880
 consequences. It may be that if you're really serious about

22
00:01:05,880 --> 00:01:06,640
 meditation that

23
00:01:06,640 --> 00:01:10,520
 there isn't this sort of distinction but I think for most

24
00:01:10,520 --> 00:01:12,480
 people you have to be

25
00:01:12,480 --> 00:01:15,000
 realistic that there's only a certain level of pain that

26
00:01:15,000 --> 00:01:16,280
 you can take. If you

27
00:01:16,280 --> 00:01:20,090
 can take it and if you know that it's just pain like for

28
00:01:20,090 --> 00:01:20,920
 most of us sitting

29
00:01:20,920 --> 00:01:24,390
 sitting cross-legged leads to some pain in the legs or pain

30
00:01:24,390 --> 00:01:26,400
 in the back or

31
00:01:26,400 --> 00:01:29,830
 or in the shoulders or so on. All of that kind of pain is

32
00:01:29,830 --> 00:01:31,400
 going to go away it's

33
00:01:31,400 --> 00:01:35,280
 not going to lead to your injury unless you have say a bad

34
00:01:35,280 --> 00:01:36,760
 back and it's just so

35
00:01:36,760 --> 00:01:41,230
 excruciatingly painful that to sit there is just going to

36
00:01:41,230 --> 00:01:42,800
 whatever caused you to

37
00:01:42,800 --> 00:01:45,620
 pass out. I'm not even sure about that there are there are

38
00:01:45,620 --> 00:01:46,140
 other pains where

39
00:01:46,140 --> 00:01:49,280
 people have injuries where they've been in an accident or

40
00:01:49,280 --> 00:01:50,840
 had a sports injury or

41
00:01:50,840 --> 00:01:53,760
 something and it's I think it would think it's not

42
00:01:53,760 --> 00:01:55,720
 advisable for them to try to

43
00:01:55,720 --> 00:02:01,320
 work through the pain. For most natural pains there that

44
00:02:01,320 --> 00:02:02,360
 occur in meditation in

45
00:02:02,360 --> 00:02:06,440
 the shoulders in the back in the legs they're they're

46
00:02:06,440 --> 00:02:08,760
 almost entirely stress

47
00:02:08,760 --> 00:02:12,390
 related and so there's something that we do have to work

48
00:02:12,390 --> 00:02:14,160
 through in meditation.

49
00:02:14,160 --> 00:02:17,910
 Obviously the technique is very simple and it's just to see

50
00:02:17,910 --> 00:02:18,840
 the pain for what

51
00:02:18,840 --> 00:02:23,740
 it is where the way we overcome pain is to overcome the

52
00:02:23,740 --> 00:02:25,000
 disliking of it is to

53
00:02:25,000 --> 00:02:28,580
 learn to see it in a different way that it's just pain. To

54
00:02:28,580 --> 00:02:30,000
 be able to accept that

55
00:02:30,000 --> 00:02:33,690
 part of reality when it really is painful to be able to

56
00:02:33,690 --> 00:02:34,840
 accept it and to

57
00:02:34,840 --> 00:02:38,140
 not live our lives running away from the pain to run

58
00:02:38,140 --> 00:02:39,280
 running away from things

59
00:02:39,280 --> 00:02:42,060
 that we don't like. When we run away from things we

60
00:02:42,060 --> 00:02:43,200
 obviously we create

61
00:02:43,200 --> 00:02:46,970
 states of we create a habit of aversion where every time

62
00:02:46,970 --> 00:02:48,840
 the pain comes up it

63
00:02:48,840 --> 00:02:51,940
 becomes more and more of a problem and we become more and

64
00:02:51,940 --> 00:02:53,040
 more adverse to it

65
00:02:53,040 --> 00:02:57,020
 and less and less able to take even the smallest amount of

66
00:02:57,020 --> 00:02:59,000
 pain. So we're trying

67
00:02:59,000 --> 00:03:03,870
 to go in the other way in the other direction and be able

68
00:03:03,870 --> 00:03:04,560
 to to be able to

69
00:03:04,560 --> 00:03:08,660
 accept more and more of reality even the more uncomfortable

70
00:03:08,660 --> 00:03:09,520
 part and in this sense

71
00:03:09,520 --> 00:03:13,580
 this sort of pain is very much associated with things like

72
00:03:13,580 --> 00:03:14,280
 hunger or

73
00:03:14,280 --> 00:03:19,350
 heat cold which are all things that we can put up with but

74
00:03:19,350 --> 00:03:20,880
 we don't want to.

75
00:03:20,880 --> 00:03:28,330
 Loud noises when people are saying bad things or saying you

76
00:03:28,330 --> 00:03:28,720
 know speaking

77
00:03:28,720 --> 00:03:31,320
 loudly we're trying to meditate and they're making loud

78
00:03:31,320 --> 00:03:32,000
 noise in the

79
00:03:32,000 --> 00:03:35,420
 background. None of these things are going to hurt us you

80
00:03:35,420 --> 00:03:36,520
 know it's not you

81
00:03:36,520 --> 00:03:39,580
 can't you wouldn't sit there through the loud noise and

82
00:03:39,580 --> 00:03:40,920
 somehow be harmed by it

83
00:03:40,920 --> 00:03:43,640
 in any way but you get more and more frustrated and a lot

84
00:03:43,640 --> 00:03:44,600
 of our pain is very

85
00:03:44,600 --> 00:03:48,260
 similar and we have to be honest with ourselves and ask

86
00:03:48,260 --> 00:03:49,520
 ourselves you know is

87
00:03:49,520 --> 00:03:52,590
 this pain really going to cause a permanent injury is it

88
00:03:52,590 --> 00:03:53,520
 really going to

89
00:03:53,520 --> 00:03:57,770
 hurt me if I sit through it if it's not then it's in your

90
00:03:57,770 --> 00:03:58,760
 it's certainly in your

91
00:03:58,760 --> 00:04:01,740
 best interest to overcome the aversion to the pain the only

92
00:04:01,740 --> 00:04:02,480
 time you that

93
00:04:02,480 --> 00:04:05,640
 wouldn't be the case is if you're you're reasonably sure

94
00:04:05,640 --> 00:04:06,800
 that sitting through the

95
00:04:06,800 --> 00:04:10,320
 pain is just going to aggravate an injury or so on and that

96
00:04:10,320 --> 00:04:11,440
 can be generally

97
00:04:11,440 --> 00:04:14,450
 the case of as I said old injuries may be in the case of

98
00:04:14,450 --> 00:04:15,680
 back problems although

99
00:04:15,680 --> 00:04:18,740
 I'm not sure about that I would think that someone with a

100
00:04:18,740 --> 00:04:20,600
 bad book back could

101
00:04:20,600 --> 00:04:23,350
 eventually work it out in meditation as it's often stress

102
00:04:23,350 --> 00:04:24,600
 related but then there

103
00:04:24,600 --> 00:04:31,170
 are back injuries like you know chronic back injuries based

104
00:04:31,170 --> 00:04:32,640
 on old age and so on

105
00:04:32,640 --> 00:04:35,690
 that probably can't be worked out through meditate cannot

106
00:04:35,690 --> 00:04:36,240
 be worked out

107
00:04:36,240 --> 00:04:39,370
 through meditation and therefore you should think about

108
00:04:39,370 --> 00:04:40,800
 sitting in a chair or

109
00:04:40,800 --> 00:04:46,820
 or up against the wall which is totally fine that being

110
00:04:46,820 --> 00:04:49,560
 said even yeah the next

111
00:04:49,560 --> 00:04:52,240
 question how important is it not to move during meditation

112
00:04:52,240 --> 00:04:52,920
 it it's quite

113
00:04:52,920 --> 00:04:58,160
 important because as I said this is a sort of aversion but

114
00:04:58,160 --> 00:05:00,520
 that being said you

115
00:05:00,520 --> 00:05:03,710
 have to know your limits and if it's just driving you

116
00:05:03,710 --> 00:05:04,760
 insane and it's it's

117
00:05:04,760 --> 00:05:07,720
 too powerful for you like you consider okay in my practice

118
00:05:07,720 --> 00:05:09,120
 I'm here the pain is

119
00:05:09,120 --> 00:05:13,460
 up here I'm not ready to sit through this pain I know that

120
00:05:13,460 --> 00:05:14,720
 I probably should but

121
00:05:14,720 --> 00:05:17,680
 reason realistically speaking I'm not going to be able to

122
00:05:17,680 --> 00:05:18,560
 and then you can

123
00:05:18,560 --> 00:05:21,870
 move if you if you do move you shouldn't feel guilty about

124
00:05:21,870 --> 00:05:23,040
 it you should know

125
00:05:23,040 --> 00:05:27,290
 accept the fact that you're only able to at this point

126
00:05:27,290 --> 00:05:28,960
 accept a certain amount of

127
00:05:28,960 --> 00:05:32,460
 pain so when you do move what is important then what you

128
00:05:32,460 --> 00:05:32,920
 should be very

129
00:05:32,920 --> 00:05:36,020
 careful to do is to acknowledge the movements so as you

130
00:05:36,020 --> 00:05:36,920
 stretch say to

131
00:05:36,920 --> 00:05:41,520
 yourself stretching stretching when you slouch slouching sl

132
00:05:41,520 --> 00:05:43,080
ouching when you lift

133
00:05:43,080 --> 00:05:47,380
 your leg lifting lifting and move it out moving moving if

134
00:05:47,380 --> 00:05:49,720
 you grab grasp your leg

135
00:05:49,720 --> 00:05:53,870
 grasping lifting placing and so on being mindful of the

136
00:05:53,870 --> 00:05:54,780
 movements and then you

137
00:05:54,780 --> 00:05:58,030
 can say you're not really cheating or you're not really

138
00:05:58,030 --> 00:05:58,840
 outside of the

139
00:05:58,840 --> 00:06:01,180
 meditation at least you're being mindful and your

140
00:06:01,180 --> 00:06:03,120
 mindfulness is continuous you

141
00:06:03,120 --> 00:06:05,490
 should acknowledge from the very beginning of disliking

142
00:06:05,490 --> 00:06:06,120
 disliking and

143
00:06:06,120 --> 00:06:10,780
 wanting to move the leg wanting wanting and lifting and

144
00:06:10,780 --> 00:06:13,240
 placing but if you can

145
00:06:13,240 --> 00:06:16,550
 the best thing is to say to yourself disliking disliking

146
00:06:16,550 --> 00:06:17,760
 disliking until the

147
00:06:17,760 --> 00:06:20,920
 disliking goes away when it's gone go back to the pain and

148
00:06:20,920 --> 00:06:21,400
 say to yourself

149
00:06:21,400 --> 00:06:24,920
 pain pain reassuring yourself that it's just pain it's

150
00:06:24,920 --> 00:06:26,880
 nothing intrinsically bad

151
00:06:26,880 --> 00:06:29,960
 when it's gone which it will eventually go away and this is

152
00:06:29,960 --> 00:06:30,600
 the great thing is

153
00:06:30,600 --> 00:06:33,130
 that if you do stick with it it'll eventually go away and

154
00:06:33,130 --> 00:06:34,320
 not come back if

155
00:06:34,320 --> 00:06:37,460
 you're if you're because you're losing this stress you're

156
00:06:37,460 --> 00:06:38,360
 giving up you're

157
00:06:38,360 --> 00:06:41,200
 letting go and your body becomes less stressful your

158
00:06:41,200 --> 00:06:42,600
 shoulders your back your

159
00:06:42,600 --> 00:06:46,680
 legs they become more relaxed and as a result the the

160
00:06:46,680 --> 00:06:48,920
 stress pains don't come

161
00:06:48,920 --> 00:06:52,900
 back finally do you recommend any type of stretching or

162
00:06:52,900 --> 00:06:54,960
 yoga no I don't because I

163
00:06:54,960 --> 00:07:01,130
 think these are a means of escaping a means of finding a

164
00:07:01,130 --> 00:07:03,280
 state of being that

165
00:07:03,280 --> 00:07:06,440
 is pleasurable that is peaceful that is free from this sort

166
00:07:06,440 --> 00:07:07,440
 of thing and it's a

167
00:07:07,440 --> 00:07:10,710
 compartmentalizing of reality it's saying okay when this

168
00:07:10,710 --> 00:07:11,560
 comes I have to

169
00:07:11,560 --> 00:07:14,710
 find a way to get get away from it I have to not allow this

170
00:07:14,710 --> 00:07:15,440
 state of reality

171
00:07:15,440 --> 00:07:20,430
 to arise so stretching stretching is an artificial it's a

172
00:07:20,430 --> 00:07:21,820
 construct it's an

173
00:07:21,820 --> 00:07:24,740
 artificial bodily state it's trying to create a state of

174
00:07:24,740 --> 00:07:26,160
 body that is healthy

175
00:07:26,160 --> 00:07:30,210
 that is is perfect and so on and I think we would all have

176
00:07:30,210 --> 00:07:31,480
 to agree that this

177
00:07:31,480 --> 00:07:35,400
 body is not perfect and there's no way to create that state

178
00:07:35,400 --> 00:07:36,360
 because it's an

179
00:07:36,360 --> 00:07:39,770
 artificially constructed state it's not sustainable and it

180
00:07:39,770 --> 00:07:41,840
's not natural natural

181
00:07:41,840 --> 00:07:44,900
 is to let the body be as it is it's it's a difference of

182
00:07:44,900 --> 00:07:45,880
 opinion and I'm sure

183
00:07:45,880 --> 00:07:48,790
 yoga teachers would say something completely different and

184
00:07:48,790 --> 00:07:49,440
 you know power

185
00:07:49,440 --> 00:07:53,900
 to them we all for different ideas but I've seen clearly

186
00:07:53,900 --> 00:07:54,760
 that I don't need

187
00:07:54,760 --> 00:08:00,480
 yoga to be to be flexible to have a calm bodily state free

188
00:08:00,480 --> 00:08:01,400
 from these stress

189
00:08:01,400 --> 00:08:05,800
 and free from the the pains it's the same as a massage

190
00:08:05,800 --> 00:08:06,920
 people who give

191
00:08:06,920 --> 00:08:09,910
 massages they say yeah it's necessary to have a massage

192
00:08:09,910 --> 00:08:11,600
 every week or so on and

193
00:08:11,600 --> 00:08:14,640
 I've never had a massage in my life and I used to have lots

194
00:08:14,640 --> 00:08:15,800
 of stress problems

195
00:08:15,800 --> 00:08:18,820
 and on the shoulders and the back and the legs when I first

196
00:08:18,820 --> 00:08:19,880
 started meditating it

197
00:08:19,880 --> 00:08:23,990
 was torture but after you practice honestly you don't need

198
00:08:23,990 --> 00:08:24,960
 any of this your

199
00:08:24,960 --> 00:08:28,530
 body is very flexible is very loose your breathing is very

200
00:08:28,530 --> 00:08:30,080
 deep is very natural

201
00:08:30,080 --> 00:08:35,160
 and it comes by itself there is no creating of this state

202
00:08:35,160 --> 00:08:35,720
 it's bringing the

203
00:08:35,720 --> 00:08:39,300
 body back to a natural state a healthy state which which

204
00:08:39,300 --> 00:08:40,520
 takes no training of

205
00:08:40,520 --> 00:08:43,380
 the body whatsoever when the mind lets go of the body the

206
00:08:43,380 --> 00:08:44,720
 body naturally comes

207
00:08:44,720 --> 00:08:48,720
 back to its state so I don't recommend any sort of

208
00:08:48,720 --> 00:08:50,780
 artificial bodily even

209
00:08:50,780 --> 00:08:54,100
 exercise I don't recommend if you want to do it living in

210
00:08:54,100 --> 00:08:55,000
 the world if it's

211
00:08:55,000 --> 00:08:59,170
 necessary for your job or so on but if it's just to make

212
00:08:59,170 --> 00:09:00,280
 you look more attractive

213
00:09:00,280 --> 00:09:04,400
 well you know to each their own the most important in the

214
00:09:04,400 --> 00:09:05,920
 meditation is to free

215
00:09:05,920 --> 00:09:10,550
 the mind from clinging so especially clinging from clinging

216
00:09:10,550 --> 00:09:11,640
 to the body it's

217
00:09:11,640 --> 00:09:15,240
 one of the great clingings so in the end we do have to let

218
00:09:15,240 --> 00:09:16,560
 go and we have to be

219
00:09:16,560 --> 00:09:22,520
 prepared for whatever comes trying to be to make our mind

220
00:09:22,520 --> 00:09:23,520
 so strong that nothing

221
00:09:23,520 --> 00:09:32,370
 can ever phase us to make our mind so clearly aware and so

222
00:09:32,370 --> 00:09:35,480
 wise and and

223
00:09:35,480 --> 00:09:39,940
 understanding about reality that whatever comes we're able

224
00:09:39,940 --> 00:09:42,640
 to accept it so pain is

225
00:09:42,640 --> 00:09:45,110
 a difficult one but that's really one of the big reasons

226
00:09:45,110 --> 00:09:46,760
 why we're practicing for

227
00:09:46,760 --> 00:09:51,420
 physical and pain and men physical pain and mental pain to

228
00:09:51,420 --> 00:09:53,040
 to become free from

229
00:09:53,040 --> 00:09:58,320
 the the the power of these things the power that they have

230
00:09:58,320 --> 00:09:59,080
 over us the only

231
00:09:59,080 --> 00:10:01,950
 way they have power over us is because we give it to them

232
00:10:01,950 --> 00:10:03,320
 we say they're bad we

233
00:10:03,320 --> 00:10:06,710
 say this is bad this is unacceptable and as a result we

234
00:10:06,710 --> 00:10:08,520
 create friction we create

235
00:10:08,520 --> 00:10:12,230
 a state of aversion towards them okay so I hope that helped

236
00:10:12,230 --> 00:10:13,200
 and good luck

237
00:10:13,200 --> 00:10:16,910
 dealing with the pain whatever pain you might have and may

238
00:10:16,910 --> 00:10:18,800
 you in the end I'll

239
00:10:18,800 --> 00:10:22,560
 be free from pain and suffering

