1
00:00:00,000 --> 00:00:04,920
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,920 --> 00:00:06,240
 Today we will continue on

3
00:00:06,240 --> 00:00:14,160
 with verse number 28 which reads as follows.

4
00:00:14,160 --> 00:00:26,640
 Pamaadang apamade na yadhanudati pandito. Panya pasada maru

5
00:00:26,640 --> 00:00:29,120
ihar asoko sokini pajang.

6
00:00:29,120 --> 00:00:37,440
 Pabata teva bhuma te dheerobhale avaikati.

7
00:00:37,440 --> 00:00:46,640
 Which means yadhanudati pandito when a wise person discards

8
00:00:46,640 --> 00:00:51,360
 negligence in favor of heedfulness.

9
00:00:51,360 --> 00:00:59,720
 Panya pasada maruihar having ascended the tower of wisdom.

10
00:00:59,720 --> 00:01:07,880
 Asoko sokini pajang looks upon the sorrowing people or the

11
00:01:07,880 --> 00:01:11,400
 suffering people as one who

12
00:01:11,400 --> 00:01:16,320
 doesn't suffer anymore.

13
00:01:16,320 --> 00:01:21,040
 Pabata teva bhuma te. It's like a person who has climbed a

14
00:01:21,040 --> 00:01:22,440
 mountain and is looking at the

15
00:01:22,440 --> 00:01:29,660
 people on the mountain below, on the ground below. Sodhirob

16
00:01:29,660 --> 00:01:31,200
hale avaikati. So the wise

17
00:01:31,200 --> 00:01:39,580
 person looks upon foolish people. So like a person on top

18
00:01:39,580 --> 00:01:40,880
 of a mountain looking down

19
00:01:40,880 --> 00:01:47,540
 at the people below, so the wise person sees foolish people

20
00:01:47,540 --> 00:01:47,800
.

21
00:01:47,800 --> 00:01:51,410
 It sounds actually like what it's saying is that the wise

22
00:01:51,410 --> 00:01:53,520
 person looks down upon foolish

23
00:01:53,520 --> 00:01:58,350
 people but we should understand the context here. The story

24
00:01:58,350 --> 00:02:00,420
 is of this verse is told in

25
00:02:00,420 --> 00:02:05,380
 regards to the Venerable Mahakasapa. Mahakasapa lived

26
00:02:05,380 --> 00:02:08,520
 actually up on top of a mountain. He

27
00:02:08,520 --> 00:02:11,800
 declined to live in the city even when he got old. He would

28
00:02:11,800 --> 00:02:13,200
 walk all the way up this

29
00:02:13,200 --> 00:02:18,940
 mountain every day to live in the forest. When we were in

30
00:02:18,940 --> 00:02:20,180
 India we actually walked up

31
00:02:20,180 --> 00:02:23,220
 these stairs and it's quite a wave. Now they have stairs

32
00:02:23,220 --> 00:02:24,400
 there but I suppose in the time

33
00:02:24,400 --> 00:02:27,290
 of Mahakasapa it would have just been, well maybe there

34
00:02:27,290 --> 00:02:30,000
 were rock stairs or something.

35
00:02:30,000 --> 00:02:33,360
 But anyway quite a walk. It's amazing that he was able to

36
00:02:33,360 --> 00:02:34,880
 do it every day and go on alms

37
00:02:34,880 --> 00:02:42,020
 round every day. But this he did and on top of that he had

38
00:02:42,020 --> 00:02:45,520
 great mental powers and so

39
00:02:45,520 --> 00:02:49,280
 he was able to, as with many of the yogis in that time and

40
00:02:49,280 --> 00:02:51,520
 of course many of the Buddhist

41
00:02:51,520 --> 00:02:55,050
 disciples, he was able to see things that ordinary people

42
00:02:55,050 --> 00:02:56,920
 can't see because of the clarity

43
00:02:56,920 --> 00:03:00,970
 and the strength of his mind. And so one morning he spent

44
00:03:00,970 --> 00:03:03,320
 this morning after he went on alms

45
00:03:03,320 --> 00:03:06,860
 round and came back and had eaten his daily meal. He spent

46
00:03:06,860 --> 00:03:08,560
 the morning looking out at

47
00:03:08,560 --> 00:03:13,360
 the beings in the world, beings who were born on the earth,

48
00:03:13,360 --> 00:03:16,120
 in the water, in the air, beings

49
00:03:16,120 --> 00:03:26,800
 who lived in all sorts of births or all sorts of realms.

50
00:03:26,800 --> 00:03:29,440
 And he was looking at them watching

51
00:03:29,440 --> 00:03:33,110
 them chase after things and chase after their desires and

52
00:03:33,110 --> 00:03:35,280
 ambitions and becoming this and

53
00:03:35,280 --> 00:03:38,990
 becoming that and setting up goals and purposes and

54
00:03:38,990 --> 00:03:42,360
 societies and governments and ideas looking

55
00:03:42,360 --> 00:03:47,000
 at the people. And it was really actually the case where he

56
00:03:47,000 --> 00:03:48,040
 was sitting up on the mountain

57
00:03:48,040 --> 00:03:53,240
 and many of this was going on down below him. And so this

58
00:03:53,240 --> 00:03:54,880
 verse was told by the Buddha who

59
00:03:54,880 --> 00:03:59,840
 came to see him and commented on this state of affairs

60
00:03:59,840 --> 00:04:02,880
 where we had this person who was

61
00:04:02,880 --> 00:04:08,130
 free from suffering and who was free from negligence and

62
00:04:08,130 --> 00:04:10,680
 who was alert and aware and

63
00:04:10,680 --> 00:04:14,620
 seeing reality clearly in a way that ordinary people can't

64
00:04:14,620 --> 00:04:16,720
 see. And so he was in a peaceful

65
00:04:16,720 --> 00:04:27,410
 and straight or a pure state of mind as opposed to all of

66
00:04:27,410 --> 00:04:29,360
 these beings down below who were

67
00:04:29,360 --> 00:04:32,920
 running around chasing after so many things being born and

68
00:04:32,920 --> 00:04:34,680
 die, born and die again and

69
00:04:34,680 --> 00:04:38,830
 again, coming and going and wishing and wanting and hoping

70
00:04:38,830 --> 00:04:41,280
 and dreaming and building up their

71
00:04:41,280 --> 00:04:44,270
 life saying this is life, this is happiness and trying to

72
00:04:44,270 --> 00:04:45,840
 get the things that they want

73
00:04:45,840 --> 00:04:48,810
 and then dying again and coming back and trying it all

74
00:04:48,810 --> 00:04:51,000
 again and suffering and the anguish

75
00:04:51,000 --> 00:04:56,560
 and the work that they went through. And so it's this

76
00:04:56,560 --> 00:04:59,400
 comparison and it's an expression

77
00:04:59,400 --> 00:05:04,800
 by the Buddha and sort of a teaching in the sense of the

78
00:05:04,800 --> 00:05:08,120
 difference between these two.

79
00:05:08,120 --> 00:05:12,540
 How a person living in the world is like these creatures

80
00:05:12,540 --> 00:05:15,600
 down, running around chasing after

81
00:05:15,600 --> 00:05:21,690
 things, living in the world like people live in cities down

82
00:05:21,690 --> 00:05:24,520
 on the level ground and then

83
00:05:24,520 --> 00:05:27,800
 you have this one person up on the mountain in peace and

84
00:05:27,800 --> 00:05:30,080
 quiet and in a tranquil environment

85
00:05:30,080 --> 00:05:37,980
 like this looking down at them and saying wow, like a game

86
00:05:37,980 --> 00:05:41,760
 or like a movie, looking down

87
00:05:41,760 --> 00:05:46,570
 and just seeing like ants crawling along. So the point isn

88
00:05:46,570 --> 00:05:48,880
't that a wise person looks

89
00:05:48,880 --> 00:05:52,130
 down upon these people and says oh, I'm much better than

90
00:05:52,130 --> 00:05:54,160
 them or something but it's this

91
00:05:54,160 --> 00:05:57,680
 state of being withdrawn or being in such a better

92
00:05:57,680 --> 00:06:00,600
 situation really because objectively

93
00:06:00,600 --> 00:06:03,790
 it is a better situation. It's not that the person in the

94
00:06:03,790 --> 00:06:05,960
 situation will disdain these

95
00:06:05,960 --> 00:06:09,960
 people or look at them, oh those fools kind of thing. They

96
00:06:09,960 --> 00:06:11,960
 might be amazed or not amazed

97
00:06:11,960 --> 00:06:21,320
 but they find it quite interesting I suppose to watch. As

98
00:06:21,320 --> 00:06:23,800
 Mahakasubha just spent the morning

99
00:06:23,800 --> 00:06:28,030
 watching this, this go on watching these people, watching

100
00:06:28,030 --> 00:06:30,440
 people and animals chase and kill

101
00:06:30,440 --> 00:06:36,450
 each other of course. Well in any realm of existence we see

102
00:06:36,450 --> 00:06:39,920
 this, we see the humans building

103
00:06:39,920 --> 00:06:46,570
 up societies and rules and having wars and politics and

104
00:06:46,570 --> 00:06:51,160
 treaties and diplomacy and families

105
00:06:51,160 --> 00:06:56,400
 and societies and religions, all of these things. And then

106
00:06:56,400 --> 00:06:57,880
 you have of course the animals

107
00:06:57,880 --> 00:07:00,460
 and if you look at the animal realm it gets even worse

108
00:07:00,460 --> 00:07:02,320
 where you have life is a struggle,

109
00:07:02,320 --> 00:07:07,240
 life is a fight, eat or be eaten, kill or be killed. And

110
00:07:07,240 --> 00:07:08,600
 then you have in the angel realms

111
00:07:08,600 --> 00:07:13,550
 you have them, they're watching out over the world, some of

112
00:07:13,550 --> 00:07:15,760
 them guarding the earth and

113
00:07:15,760 --> 00:07:19,960
 guarding humans, some of them living in great luxury and

114
00:07:19,960 --> 00:07:22,840
 enjoying their time in heaven and

115
00:07:22,840 --> 00:07:31,360
 these paradise realms and so on. So looking at all of this,

116
00:07:31,360 --> 00:07:35,360
 it's this state of being removed

117
00:07:35,360 --> 00:07:38,720
 from it all because you're no longer a part of it, a person

118
00:07:38,720 --> 00:07:40,280
 who's given it all up sees

119
00:07:40,280 --> 00:07:43,590
 it as one who sees people down below up on the mountain. If

120
00:07:43,590 --> 00:07:44,200
 you've ever been up in a

121
00:07:44,200 --> 00:07:54,970
 tower you look down and you see all the beings and it looks

122
00:07:54,970 --> 00:07:56,760
 quite peaceful actually. Suddenly

123
00:07:56,760 --> 00:07:59,930
 the world with all this craziness and all of the stress and

124
00:07:59,930 --> 00:08:01,680
 anxiety looks quite peaceful

125
00:08:01,680 --> 00:08:06,030
 you see little dots moving. And this is the Buddha said

126
00:08:06,030 --> 00:08:08,120
 this is how it is for a person

127
00:08:08,120 --> 00:08:12,150
 who has become free from suffering because actually you can

128
00:08:12,150 --> 00:08:14,120
 be among this, a person who

129
00:08:14,120 --> 00:08:18,370
 has become free from suffering could walk as Mahakasapa

130
00:08:18,370 --> 00:08:18,920
 would walk through the village

131
00:08:18,920 --> 00:08:22,690
 on alms but he wouldn't get involved with anything. Like in

132
00:08:22,690 --> 00:08:23,440
 another verse the Buddha

133
00:08:23,440 --> 00:08:27,090
 says like a bee that takes the pollen and leaves the flower

134
00:08:27,090 --> 00:08:29,320
 unharmed without any connection

135
00:08:29,320 --> 00:08:34,220
 with the flower but just takes the pollen and leaves. Same

136
00:08:34,220 --> 00:08:36,440
 as with a person who is mindful

137
00:08:36,440 --> 00:08:39,550
 goes through the village. As they go through the village

138
00:08:39,550 --> 00:08:41,320
 they don't cling to anything,

139
00:08:41,320 --> 00:08:47,100
 they aren't attached to anything or delighted by anything

140
00:08:47,100 --> 00:08:49,960
 and so they come in and even there

141
00:08:49,960 --> 00:08:52,330
 might be craziness so when we go on alms ground sometimes

142
00:08:52,330 --> 00:08:54,040
 people are fighting, sometimes there's

143
00:08:54,040 --> 00:08:57,650
 yelling, sometimes there's parties, sometimes there's funer

144
00:08:57,650 --> 00:08:59,520
als, sometimes there's traffic

145
00:08:59,520 --> 00:09:03,180
 and accidents and so on and all of the stresses of the

146
00:09:03,180 --> 00:09:05,560
 world by people who have goals and

147
00:09:05,560 --> 00:09:08,660
 ambitions and who want to be rich and want to be powerful

148
00:09:08,660 --> 00:09:10,280
 and want to have stability

149
00:09:10,280 --> 00:09:15,250
 in their family and want to be healthy and seeing all of

150
00:09:15,250 --> 00:09:17,880
 this going on but at the same

151
00:09:17,880 --> 00:09:20,320
 time even walking through it being as though you're way up

152
00:09:20,320 --> 00:09:21,560
 on a mountain looking down on

153
00:09:21,560 --> 00:09:26,250
 it. It's not that you, it's not looking down in the English

154
00:09:26,250 --> 00:09:28,480
 sense of thinking badly of

155
00:09:28,480 --> 00:09:31,230
 those people but it's as though you were actually up on the

156
00:09:31,230 --> 00:09:34,120
 mountain looking at it from above

157
00:09:34,120 --> 00:09:38,200
 like the little specks as you walk through the village

158
00:09:38,200 --> 00:09:40,840
 everything is just craziness but

159
00:09:40,840 --> 00:09:45,440
 it's very peaceful to you. This is the tower of wisdom, it

160
00:09:45,440 --> 00:09:48,360
's not actually a tower but it's

161
00:09:48,360 --> 00:09:52,270
 the mind is so much, so far removed from all of that and

162
00:09:52,270 --> 00:09:54,480
 has no thoughts of desire for

163
00:09:54,480 --> 00:10:00,530
 this or that. So this is what this verse means. How it

164
00:10:00,530 --> 00:10:03,960
 relates to our practice, I think it

165
00:10:03,960 --> 00:10:10,840
 really points out the contrast between the path that we're

166
00:10:10,840 --> 00:10:13,600
 following to try to become

167
00:10:13,600 --> 00:10:18,500
 free from suffering and the path that most of the world,

168
00:10:18,500 --> 00:10:21,080
 most of the universe follows.

169
00:10:21,080 --> 00:10:25,430
 If you think of the goals and really the framework in which

170
00:10:25,430 --> 00:10:28,520
 most of the universe works, the actual

171
00:10:28,520 --> 00:10:33,180
 framework is quite different. And so this is how the Buddha

172
00:10:33,180 --> 00:10:35,280
 explained this teaching of

173
00:10:35,280 --> 00:10:41,570
 Paticca Samuppada, the dependent origination. How when a

174
00:10:41,570 --> 00:10:43,440
 person has these beliefs and these

175
00:10:43,440 --> 00:10:46,750
 ideas that there is some purpose, so people who believe in

176
00:10:46,750 --> 00:10:48,520
 God and that God has a purpose

177
00:10:48,520 --> 00:10:51,740
 for them or so on or that society is the purpose when

178
00:10:51,740 --> 00:10:54,320
 people want to build up society and believe

179
00:10:54,320 --> 00:10:57,930
 they have a duty in society to build up and to be

180
00:10:57,930 --> 00:11:01,080
 politically active and protest and so

181
00:11:01,080 --> 00:11:05,020
 on and demonstrate and express their opinions and get

182
00:11:05,020 --> 00:11:07,560
 involved and create and build and

183
00:11:07,560 --> 00:11:15,340
 organize. But this is what leads to one, it leads them to

184
00:11:15,340 --> 00:11:16,800
 do things. Sometimes they will

185
00:11:16,800 --> 00:11:19,570
 do good things, sometimes they will do bad things. When

186
00:11:19,570 --> 00:11:21,120
 they are angry at people they

187
00:11:21,120 --> 00:11:24,810
 will hurt others and there's wars and conflicts and even

188
00:11:24,810 --> 00:11:27,120
 people who are into good things can

189
00:11:27,120 --> 00:11:29,960
 get very angry and passionate about it and as a result can

190
00:11:29,960 --> 00:11:31,520
 hurt other people and create

191
00:11:31,520 --> 00:11:36,160
 conflict. And when there is greed or the desire that comes

192
00:11:36,160 --> 00:11:38,680
 from commercialism and materialism,

193
00:11:38,680 --> 00:11:42,300
 wanting this and wanting that, seeing these new electronic

194
00:11:42,300 --> 00:11:44,440
 devices and wanting them, seeing

195
00:11:44,440 --> 00:11:55,340
 beautiful people dressed attractively and seeing movies and

196
00:11:55,340 --> 00:11:59,800
 having music, these music

197
00:11:59,800 --> 00:12:04,180
 players where you can always be listening to something

198
00:12:04,180 --> 00:12:07,400
 intoxicating, something attractive

199
00:12:07,400 --> 00:12:13,000
 and then having good food and all of these things. The

200
00:12:13,000 --> 00:12:14,040
 doing of all, the creating of

201
00:12:14,040 --> 00:12:19,150
 all this builds up a huge system in order to fulfill the

202
00:12:19,150 --> 00:12:21,960
 requirements, the need for

203
00:12:21,960 --> 00:12:25,240
 food, the need for good food, the need for special food,

204
00:12:25,240 --> 00:12:26,960
 the need for good sound, the

205
00:12:26,960 --> 00:12:29,960
 need for beautiful sights. When you want to be able to see

206
00:12:29,960 --> 00:12:32,760
 things, when you want to obtain

207
00:12:32,760 --> 00:12:37,470
 the family or husband, the wife, children, when you want to

208
00:12:37,470 --> 00:12:39,760
 be rich, the things you have

209
00:12:39,760 --> 00:12:44,990
 to do to get rich, this is all creating and going on. And

210
00:12:44,990 --> 00:12:48,520
 so the clinging, the ambition

211
00:12:48,520 --> 00:12:52,150
 in order to gain all of this, this is the teaching on patic

212
00:12:52,150 --> 00:12:54,000
ca samuppala that goes from

213
00:12:54,000 --> 00:12:57,260
 one thing to the next to the next to the next where the

214
00:12:57,260 --> 00:12:59,520
 desires they lead to craving, the

215
00:12:59,520 --> 00:13:04,500
 craving leads to addiction or attachment and attachment

216
00:13:04,500 --> 00:13:07,360
 leads us to build, to create. This

217
00:13:07,360 --> 00:13:10,510
 is what we see going on when you stand up on the mountain,

218
00:13:10,510 --> 00:13:11,960
 what you see going on in the

219
00:13:11,960 --> 00:13:17,110
 city down below. You see constantly building and rushing

220
00:13:17,110 --> 00:13:20,800
 around and business, all busyness.

221
00:13:20,800 --> 00:13:25,800
 You see the markets, you see the cars rushing around and

222
00:13:25,800 --> 00:13:28,360
 you see so much activity. When

223
00:13:28,360 --> 00:13:30,770
 you go up on a mountain and you look down on it, you can

224
00:13:30,770 --> 00:13:32,120
 see how crazy actually it all

225
00:13:32,120 --> 00:13:34,640
 is and how much stress and if you look at it as a whole

226
00:13:34,640 --> 00:13:36,240
 actually, if you think about

227
00:13:36,240 --> 00:13:39,190
 it in a Buddhist sense, looking up, if you were up on a

228
00:13:39,190 --> 00:13:41,040
 real mountain looking down, it

229
00:13:41,040 --> 00:13:44,700
 would actually give you some intellectual insight, not med

230
00:13:44,700 --> 00:13:46,640
itative insight, but it would

231
00:13:46,640 --> 00:13:50,320
 allow you to say, "Wow, this really is the case that we

232
00:13:50,320 --> 00:13:52,280
 really are so much stress and

233
00:13:52,280 --> 00:13:59,080
 so much activity." And what we come to see in the

234
00:13:59,080 --> 00:14:02,080
 meditation is how this in the end just

235
00:14:02,080 --> 00:14:06,580
 leads to suffering, it leads to dissatisfaction, it leads

236
00:14:06,580 --> 00:14:08,680
 us to try and to attain and to not

237
00:14:08,680 --> 00:14:13,600
 be satisfied and to want more. And as with what Maha Kasub

238
00:14:13,600 --> 00:14:15,640
as says, it leads to old age

239
00:14:15,640 --> 00:14:19,960
 sickness and death and the end of it all. So anything that

240
00:14:19,960 --> 00:14:21,040
 you build up, anything that

241
00:14:21,040 --> 00:14:24,720
 you create, in the end you have to let go of, you have to

242
00:14:24,720 --> 00:14:26,680
 be deprived of, you have to

243
00:14:26,680 --> 00:14:30,300
 leave behind. And so when someone can see this, in these

244
00:14:30,300 --> 00:14:32,160
 special powers that no one wants

245
00:14:32,160 --> 00:14:34,780
 to hear about because it's like we don't have them and so

246
00:14:34,780 --> 00:14:36,440
 we therefore think that they probably

247
00:14:36,440 --> 00:14:40,360
 don't exist and there's no proof and so on like that. But

248
00:14:40,360 --> 00:14:44,080
 when you have these magical

249
00:14:44,080 --> 00:14:47,800
 powers, not magical, when you have this clear vision that

250
00:14:47,800 --> 00:14:49,880
 allows you to penetrate into the

251
00:14:49,880 --> 00:14:56,180
 very fabric of reality, you can see the beings being born

252
00:14:56,180 --> 00:14:59,720
 and dying and so you see, "Oh,

253
00:14:59,720 --> 00:15:02,310
 look at them building, building, building and then dying."

254
00:15:02,310 --> 00:15:03,360
 And then born again and

255
00:15:03,360 --> 00:15:11,210
 trying the same thing all over again. Now for most of us

256
00:15:11,210 --> 00:15:12,640
 this is impossible, right?

257
00:15:12,640 --> 00:15:15,360
 So as I said, people have a problem with this because they

258
00:15:15,360 --> 00:15:16,880
 don't believe, they think this

259
00:15:16,880 --> 00:15:20,990
 is silly, why talk about this? And basically, how can we

260
00:15:20,990 --> 00:15:23,560
 believe in these sorts of things?

261
00:15:23,560 --> 00:15:28,490
 So it might be good to address that as well here. If we

262
00:15:28,490 --> 00:15:31,720
 talk about understanding, say,

263
00:15:31,720 --> 00:15:35,830
 paticca samupada, it talks about getting old, sick and

264
00:15:35,830 --> 00:15:37,960
 dying and being born again, no? And

265
00:15:37,960 --> 00:15:42,480
 so people say, "Well, how can you, you're just accepting,

266
00:15:42,480 --> 00:15:44,600
 then accepting this on blind

267
00:15:44,600 --> 00:15:47,770
 faith? How can you accept this teaching when you yourself

268
00:15:47,770 --> 00:15:49,520
 haven't experienced that?" If

269
00:15:49,520 --> 00:15:53,230
 you practice meditation, maybe you'll never gain these

270
00:15:53,230 --> 00:15:55,040
 magical powers. But you see, what

271
00:15:55,040 --> 00:15:57,950
 you gain in meditation is much more important. What you

272
00:15:57,950 --> 00:15:59,640
 gain in meditation is the moment

273
00:15:59,640 --> 00:16:03,640
 to moment understanding of how this works. You see how from

274
00:16:03,640 --> 00:16:05,400
 moment to moment we cling

275
00:16:05,400 --> 00:16:09,620
 and it leads us to create. When something good comes up, we

276
00:16:09,620 --> 00:16:11,100
 hold on to it and we want

277
00:16:11,100 --> 00:16:14,890
 to gain it and so we create more. You do this every moment,

278
00:16:14,890 --> 00:16:16,480
 not just talking about in your

279
00:16:16,480 --> 00:16:20,310
 life how you have plans and ambitions and goals and hopes

280
00:16:20,310 --> 00:16:22,120
 and dreams and how you get

281
00:16:22,120 --> 00:16:28,030
 caught up in family life and in debt and so many different

282
00:16:28,030 --> 00:16:30,880
 things and conflict with others.

283
00:16:30,880 --> 00:16:35,090
 It actually occurs moment to moment. Every time you get

284
00:16:35,090 --> 00:16:37,720
 angry or you attach to something,

285
00:16:37,720 --> 00:16:44,720
 you change who you are. You become more coarse and more

286
00:16:44,720 --> 00:16:53,680
 stressed, more in a more active state

287
00:16:53,680 --> 00:16:57,070
 and so more creative really. So creating this, creating

288
00:16:57,070 --> 00:16:59,760
 that, causing trouble for yourself,

289
00:16:59,760 --> 00:17:05,240
 making plans and ambitions and wanting more and getting in

290
00:17:05,240 --> 00:17:07,680
 debt and so on and so on.

291
00:17:07,680 --> 00:17:13,510
 And this is really why I think there's really no problem

292
00:17:13,510 --> 00:17:17,600
 with even with understanding, with

293
00:17:17,600 --> 00:17:20,890
 talking about this idea of getting old, sick and die and

294
00:17:20,890 --> 00:17:22,720
 being born again and of people

295
00:17:22,720 --> 00:17:26,680
 who can actually see this and the rebirth and so on.

296
00:17:26,680 --> 00:17:28,440
 Because in the meditation practice

297
00:17:28,440 --> 00:17:31,150
 you can see reality working from moment to moment and you

298
00:17:31,150 --> 00:17:32,520
 can see how it leads in this

299
00:17:32,520 --> 00:17:36,170
 direction, how it leads to creation and then how the

300
00:17:36,170 --> 00:17:38,680
 creation leads to wanting more and

301
00:17:38,680 --> 00:17:43,610
 eventually how it all collapses and it collapses on a

302
00:17:43,610 --> 00:17:46,200
 moment to moment basis. The thing that

303
00:17:46,200 --> 00:17:50,730
 Mahakasipa saw was on a grand scale. He was able to extend

304
00:17:50,730 --> 00:17:52,720
 this out and to see how it

305
00:17:52,720 --> 00:17:57,180
 was working all around him from moment to moment and to see

306
00:17:57,180 --> 00:17:59,400
 how beings were giving rise

307
00:17:59,400 --> 00:18:03,630
 to these mind states and going on and having their dreams

308
00:18:03,630 --> 00:18:05,400
 destroyed. You don't need to

309
00:18:05,400 --> 00:18:10,910
 understand, you don't need to have a realization of a past

310
00:18:10,910 --> 00:18:14,880
 life or of a future life or so on.

311
00:18:14,880 --> 00:18:18,020
 And you don't need to have these powers yourself to

312
00:18:18,020 --> 00:18:20,760
 understand how it works because in practicing

313
00:18:20,760 --> 00:18:26,100
 meditation you're actually playing with or observing,

314
00:18:26,100 --> 00:18:29,160
 interacting with the very fabric

315
00:18:29,160 --> 00:18:32,350
 of reality. And so you can see how this works. You can see

316
00:18:32,350 --> 00:18:33,980
 how with a strong mind you can

317
00:18:33,980 --> 00:18:38,360
 penetrate into it and you can see things far away and see

318
00:18:38,360 --> 00:18:40,480
 things in the past as people

319
00:18:40,480 --> 00:18:45,650
 remember past lives and you can see other people far away

320
00:18:45,650 --> 00:18:47,880
 and so on. But the other thing

321
00:18:47,880 --> 00:18:52,010
 to say is that the magical powers isn't really the point of

322
00:18:52,010 --> 00:18:53,920
 the verse here and it's not the

323
00:18:53,920 --> 00:18:57,880
 point of any of the Dhammapada, any of the verses at all.

324
00:18:57,880 --> 00:18:58,920
 The verses aren't about the

325
00:18:58,920 --> 00:19:02,440
 magical powers. These background stories are just providing

326
00:19:02,440 --> 00:19:04,120
 some context and so whether

327
00:19:04,120 --> 00:19:08,360
 or not you believe in these powers that Mahakasipa had, it

328
00:19:08,360 --> 00:19:11,200
 has no bearing on the actual teaching

329
00:19:11,200 --> 00:19:16,230
 itself which is that through seeing this, whether it's in a

330
00:19:16,230 --> 00:19:18,640
 meditative sense or whether

331
00:19:18,640 --> 00:19:22,360
 it goes further than that and you're able to see all beings

332
00:19:22,360 --> 00:19:23,880
 arising and ceasing and

333
00:19:23,880 --> 00:19:27,410
 coming and going as the Buddha was able to do or even

334
00:19:27,410 --> 00:19:29,440
 somewhere in between where Mahakasipa

335
00:19:29,440 --> 00:19:34,900
 was able to see some beings arising and ceasing even though

336
00:19:34,900 --> 00:19:37,480
 he's not a Buddha, is that it

337
00:19:37,480 --> 00:19:42,390
 changes your ambition, it changes your drive, it changes

338
00:19:42,390 --> 00:19:44,960
 your goal and your path in life

339
00:19:44,960 --> 00:19:47,720
 and it turns you around in the opposite direction and this

340
00:19:47,720 --> 00:19:49,560
 is where the contrast comes in. That

341
00:19:49,560 --> 00:19:53,640
 most people are through their ambitions and their goals are

342
00:19:53,640 --> 00:19:55,520
 creating more and more and

343
00:19:55,520 --> 00:19:59,820
 building up their stress and suffering and going on and on

344
00:19:59,820 --> 00:20:01,760
 and again and again, chasing

345
00:20:01,760 --> 00:20:05,860
 after and building up and thinking that they've finally

346
00:20:05,860 --> 00:20:08,360
 found true peace only to have it taken

347
00:20:08,360 --> 00:20:13,380
 away from them through calamity or disaster or finally

348
00:20:13,380 --> 00:20:16,600
 through old age sickness and death

349
00:20:16,600 --> 00:20:19,840
 and then building it up again and again, having to learn

350
00:20:19,840 --> 00:20:21,720
 the same lessons again and again

351
00:20:21,720 --> 00:20:27,190
 and again and if not learning them then falling into

352
00:20:27,190 --> 00:20:30,260
 disaster and suffering as opposed to

353
00:20:30,260 --> 00:20:33,990
 going the other way where when a person sees this and when

354
00:20:33,990 --> 00:20:36,240
 a person sees what this is leading

355
00:20:36,240 --> 00:20:39,060
 to, how it's not leading to peace and happiness and freedom

356
00:20:39,060 --> 00:20:40,680
 from suffering, how they start

357
00:20:40,680 --> 00:20:45,500
 deconstructing and they give up their desires, they give up

358
00:20:45,500 --> 00:20:48,320
 their attachments, they give up

359
00:20:48,320 --> 00:20:54,260
 their aversions and they simply see everything clearly as

360
00:20:54,260 --> 00:20:55,960
 it is. They come to straighten

361
00:20:55,960 --> 00:20:59,010
 their minds so that they're able to see the experience

362
00:20:59,010 --> 00:21:01,080
 whether it be a positive experience

363
00:21:01,080 --> 00:21:06,000
 or a negative experience to see it clearly so to have a

364
00:21:06,000 --> 00:21:08,880
 powerful mind that is so sharp

365
00:21:08,880 --> 00:21:13,130
 that it's able to see just the bare reality and to cut to

366
00:21:13,130 --> 00:21:15,420
 the very core of experience.

367
00:21:15,420 --> 00:21:18,390
 So then they don't give rise to desire or addiction and

368
00:21:18,390 --> 00:21:20,040
 they don't have ambitions and

369
00:21:20,040 --> 00:21:23,600
 they don't want to become something and so they don't chase

370
00:21:23,600 --> 00:21:25,400
 after and they don't create

371
00:21:25,400 --> 00:21:31,830
 and because they don't create, they don't have all of this

372
00:21:31,830 --> 00:21:34,360
 burden to carry around and

373
00:21:34,360 --> 00:21:39,110
 all of this suffering and they aren't in amongst the

374
00:21:39,110 --> 00:21:42,480
 suffering and the stress. So when an ordinary

375
00:21:42,480 --> 00:21:45,830
 person goes into the city, a person who has never practiced

376
00:21:45,830 --> 00:21:47,600
 meditation is not interested

377
00:21:47,600 --> 00:21:51,760
 in mental development and who is really keen on materialism

378
00:21:51,760 --> 00:21:53,720
, when they go into the city,

379
00:21:53,720 --> 00:21:56,750
 they go crazy, they see beautiful things and they chase

380
00:21:56,750 --> 00:21:58,560
 after them and they want this and

381
00:21:58,560 --> 00:22:02,550
 want that, go out to the restaurants and get food and maybe

382
00:22:02,550 --> 00:22:04,800
 alcohol and dancing and buying

383
00:22:04,800 --> 00:22:11,330
 things here and there and really get caught up in it. But

384
00:22:11,330 --> 00:22:13,600
 when a wise person, as I was

385
00:22:13,600 --> 00:22:16,770
 saying, goes into the, even into the big city, they go in

386
00:22:16,770 --> 00:22:18,400
 looking at it like a person up

387
00:22:18,400 --> 00:22:22,030
 on a mountain because they're actually standing on the

388
00:22:22,030 --> 00:22:24,800
 mountain of wisdom. So for our practice

389
00:22:24,800 --> 00:22:28,250
 we understand these are the two paths, the path towards

390
00:22:28,250 --> 00:22:30,240
 accumulation and the path away

391
00:22:30,240 --> 00:22:35,830
 from accumulating, the path towards becoming busier and bus

392
00:22:35,830 --> 00:22:38,400
ier and getting involved in

393
00:22:38,400 --> 00:22:42,150
 creating and taking the reality that we have and making

394
00:22:42,150 --> 00:22:44,280
 something out of it as opposed

395
00:22:44,280 --> 00:22:48,770
 to letting it go and letting it be and not creating and

396
00:22:48,770 --> 00:22:51,040
 simply seeing it for what it

397
00:22:51,040 --> 00:22:55,560
 is. So people sometimes ask why, if they've never practiced

398
00:22:55,560 --> 00:22:56,600
 meditation, why do we just

399
00:22:56,600 --> 00:23:00,240
 do walking back and forth, what is the point of it? Why do

400
00:23:00,240 --> 00:23:02,120
 you just do sitting there? Why

401
00:23:02,120 --> 00:23:05,260
 are you wasting your time? They don't understand what is

402
00:23:05,260 --> 00:23:07,040
 the purpose of it because in their

403
00:23:07,040 --> 00:23:10,560
 mind everything has to have a purpose. You do something in

404
00:23:10,560 --> 00:23:11,760
 order to get something in

405
00:23:11,760 --> 00:23:15,990
 the future. It has to create something. What you do has to

406
00:23:15,990 --> 00:23:17,860
 bring some result. So they see

407
00:23:17,860 --> 00:23:20,810
 you're walking back and forth and there's no result. And

408
00:23:20,810 --> 00:23:22,280
 well, that's the point. The

409
00:23:22,280 --> 00:23:25,360
 point is that by walking back and forth I'm clearing out

410
00:23:25,360 --> 00:23:27,460
 the results. I'm doing something

411
00:23:27,460 --> 00:23:30,850
 that is not bringing results. And it's not just walking

412
00:23:30,850 --> 00:23:32,600
 back and forth, of course, because

413
00:23:32,600 --> 00:23:35,650
 the truth is when an ordinary person walks back and forth

414
00:23:35,650 --> 00:23:37,160
 they do create results. Walking

415
00:23:37,160 --> 00:23:39,550
 back and forth they're thinking about, "Oh, tomorrow I've

416
00:23:39,550 --> 00:23:40,720
 got to do this and wouldn't

417
00:23:40,720 --> 00:23:44,840
 it be great if I did that?" And so even just walking back

418
00:23:44,840 --> 00:23:46,920
 and forth they're still creating

419
00:23:46,920 --> 00:23:50,840
 plans and ambitions and after they finish they'll go off

420
00:23:50,840 --> 00:23:53,760
 and enact those plans. But

421
00:23:53,760 --> 00:23:56,720
 the wise person or the path that we're trying to follow

422
00:23:56,720 --> 00:23:59,240
 here is to go in the opposite direction,

423
00:23:59,240 --> 00:24:02,790
 that the more we walk the closer we come to just walking so

424
00:24:02,790 --> 00:24:04,520
 that when the foot moves we

425
00:24:04,520 --> 00:24:07,490
 know the foot is moving, when a thought arises we know that

426
00:24:07,490 --> 00:24:08,880
 a thought arises. And all of

427
00:24:08,880 --> 00:24:13,470
 our ambitions and our attachments are given up. We see the

428
00:24:13,470 --> 00:24:17,560
 stress and we see the suffering.

429
00:24:17,560 --> 00:24:21,190
 The idea is to be able to see them clearly and see them for

430
00:24:21,190 --> 00:24:23,320
 what they are, see them that

431
00:24:23,320 --> 00:24:27,810
 these are pointless and meaningless, have no benefit and no

432
00:24:27,810 --> 00:24:29,840
 purpose for us, to be able

433
00:24:29,840 --> 00:24:34,480
 to let go of them and see them for what they are. So this

434
00:24:34,480 --> 00:24:36,400
 is really the core of what we're

435
00:24:36,400 --> 00:24:44,300
 practicing. We're practicing to rise above all of the

436
00:24:44,300 --> 00:24:48,560
 reality that we find around us.

437
00:24:48,560 --> 00:24:51,990
 Because really it's all contrived, all of the beauty that

438
00:24:51,990 --> 00:24:53,480
 we see, all of the wonder

439
00:24:53,480 --> 00:24:58,720
 that we see, it's all just formations, it's all artificial,

440
00:24:58,720 --> 00:25:00,880
 all of what we call natural.

441
00:25:00,880 --> 00:25:06,110
 It's really just, century after century, as time goes on,

442
00:25:06,110 --> 00:25:08,440
 it's just been built up. And

443
00:25:08,440 --> 00:25:11,700
 it's been built up based on our desires and our attractions

444
00:25:11,700 --> 00:25:12,960
. We've built this, we've

445
00:25:12,960 --> 00:25:17,730
 created this state, this body that we've created, we've

446
00:25:17,730 --> 00:25:20,600
 created it out of a desire, out of an

447
00:25:20,600 --> 00:25:27,290
 attachment, out of partiality. And it's all just artificial

448
00:25:27,290 --> 00:25:30,120
, it's the result of the craziness

449
00:25:30,120 --> 00:25:35,170
 that we've fallen into. And so by following after it and

450
00:25:35,170 --> 00:25:37,760
 developing it, we build up more

451
00:25:37,760 --> 00:25:41,370
 and more stress and more and more suffering. What we're

452
00:25:41,370 --> 00:25:43,280
 doing here is to let go of it,

453
00:25:43,280 --> 00:25:46,650
 to give it up and to even come out of this body so that we

454
00:25:46,650 --> 00:25:48,540
 don't need any particular

455
00:25:48,540 --> 00:25:54,490
 contrivance or any construct to bring us peace and

456
00:25:54,490 --> 00:25:57,340
 happiness. Even being a human, even the

457
00:25:57,340 --> 00:26:01,140
 state of humanity, we come out of that. And in the end, you

458
00:26:01,140 --> 00:26:03,120
 can't say that an enlightened

459
00:26:03,120 --> 00:26:06,970
 person is a human or a God or an angel or anything because

460
00:26:06,970 --> 00:26:09,240
 they've come out of it and

461
00:26:09,240 --> 00:26:12,890
 they have no attachment, they have no desire or no

462
00:26:12,890 --> 00:26:15,720
 ambitions in regard to any of that.

463
00:26:15,720 --> 00:26:19,370
 So they stand above the world like a person who stands on a

464
00:26:19,370 --> 00:26:21,360
 mountain, looks down at the

465
00:26:21,360 --> 00:26:25,720
 world a lot. So that's the meaning and the significance of

466
00:26:25,720 --> 00:26:28,040
 this verse and that is another

467
00:26:28,040 --> 00:26:31,730
 teaching on the Dhammapada. Thank you all for tuning in and

468
00:26:31,730 --> 00:26:33,000
 see you next time.

469
00:26:33,000 --> 00:26:34,000
 Thank you.

