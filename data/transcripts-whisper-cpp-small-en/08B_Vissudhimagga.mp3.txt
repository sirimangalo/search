 Now, suppose you have all five jhanas and also all alubavaj
ara jhanas and also you can
 experience what we call supernormal knowledge or abhinya.
 Abhinya is a specially developed fifth jhana and abhinyas
 are those that enable yogis to
 see things far away and to hear sounds far away and so on
 or to perform some miracles.
 Now, suppose you want to hear the sounds far away.
 Now, suppose you have all five jhanas and also all alubavaj
ara jhanas and also you can
 experience what we call supernormal knowledge or abhinya.
 Abhinya is a specially developed fifth jhana and abhinyas
 are those that enable yogis to
 see things far away and to hear sounds far away and so on
 or to perform some miracles.
 Now, suppose you want to hear the sounds far away and it
 depends on how large you extend
 the kasimha sign.
 If you extend the kasimha sign as big as San Francisco City
, then you may hear sounds in
 San Francisco City and not outside.
 So the ten kasimhas among these forty meditation subjects
 need be extended for it is within
 just so much space as one is intent upon.
 That means one covers, one extends over with the kasimha
 that one can hear sounds with
 the divine ear element, see visible objects with the divine
 eye and know the minds of
 other beings with the mind.
 So before you experience the supernormal knowledge, you
 have to practice, you have to extend the
 sign, the kantabhat sign.
 That means you are defining the area within which your
 supernormal knowledge will apply.
 So they need be extended.
 Mindfulness occupied with the body and the ten kinds of
 foulness need not be extended.
 So these subjects need not be extended.
 Why?
 Because they have a definite location.
 You have to define these things and so you cannot extend
 them and if you extend them
 there is no benefit because there is no benefit in it.
 The definiteness of their location will become clear in
 explaining the method of development.
 Now when the other comes to explaining how to practice
 meditation on dead bodies and
 so on and it will become clear.
 If the letters are extended, it is only a quantity of
 corpses that is extended with regard to
 foulness meditation.
 You will extend the corpses, I mean in your mind, corpses
 but there is no benefit.
 And it is said in answer to the question of Soba Gita, "The
 perception of visible forms
 is quite clear."
 Blessed one.
 "The perception of bones is not clear.
 For here the perception of visible form is called quite
 clear in the sense of extension
 of the sign.
 When the perception of bones is called not quite clear,
 perception of bones has to do
 with foulness meditation.
 In the sense it is non-extension.
 For the words, "I was intent upon this whole earth with the
 perception of a skeleton that
 was uttered by one elder."
 Now they are said in the manner of appearance to one who
 has acquired that perception.
 That means one who has acquired that perception before and
 now he extends this perception.
 So it is alright.
 Because he is not practicing to get that perception.
 He has already got that perception and so he extends it.
 For just as in Dhammasoka's time the Karabhika but uttered
 his sweet song when it saw its
 own reflection in the looking glass walls all round and
 perceived Karabhika in every
 direction so the elder, Singala Peeta thought, when he saw
 the sign appearing in all directions
 through his acquisition of perception of a skeleton that
 the whole earth was covered
 with bones."
 And then there is the footnote on Karabhika's bird.
 It's interesting but it's difficult to believe.
 It's a kind of bird and it is said that its sound is very
 sweet and so the queen asked
 the community whose voice or whose sound is sweet as the
 community said the Karabhika
 but so the queen wanted to listen to the sound of the Karab
hika but and so she asked her
 king, King Asoka to bring a Karabhika bird.
 So what Asoka did was just send a cage.
 The cage flew through the air and they lay down near the
 bird and the bird got in the
 cage and the cage flew back to the city and so on.
 But after reaching the city he would not utter his sound
 because he was depressed.
 So the king asked why the bird did not make any sound so
 they said because he was lonely.
 If the bird had companions then he would make noise.
 So the king put the mirrors around him and so the bird saw
 his images in the mirror and
 he thought there were other birds so he was happy and so he
 made a sound.
 And the queen when she heard the sound she was so pleased
 and she was very happy and
 dwelling on that happiness or practicing meditation on that
 happiness she practiced with Vasana
 and she became a stream winner established in the fruition
 of stream entry that means
 she became a swordavanna.
 This is just an example.
 Just as the Karabhika bird saw many other birds in the
 mirror and so make sound the elder
 here when he saw the sign appearing in all directions
 through his acquisition of the
 perception of a skeleton he thought the whole earth was
 covered with bones.
 So it appeared to him as that.
 It is not that he extended the casino sign.
 And the next paragraph it is if that is so then is what is
 called the measurelessness
 of the object of Jhana produced on foulness contradicted.
 Now the Jhana produced on foulness is mentioned as measure
less or it is mentioned as with
 measure so is that contradicted then the answer is no.
 When a person looks at the small corpse then his object is
 said to be with measure and
 if he looks at a big corpse then his object is said to be
 measureless although it is not
 really measureless but measureless here means large so the
 large object and small object.
 And then paragraph 115 as regards the immaterial states as
 objects.
 It should read as regards objects of the immaterial states
 not immaterial states as objects but
 objects of the immaterial states.
 This need not be extended since it is the mere removal of
 the casino.
 So with regard to the objects of Aruba Vajrayana.
 Now the object of the first Aruba Vajrayana is what
 infinite space so that cannot be extended
 because it is nothing.
 It is obtained through the removal of casino and removal of
 casino means not paying attention
 to the casino sign.
 First there is casino sign in his mind and then he stops
 paying attention to that casino
 sign so the casino sign disappears and in the place of that
 casino sign just the space
 remains so that space is space so that cannot be extended.
 If he extends it nothing further happens so nothing will
 happen and consciousness need
 not be extended actually consciousness should not be or
 could not be extended since it is
 a state consisting in an individual essence that means it
 is a paramata it is a it is
 an ultimate reality a reality which has its own
 characteristic or individual essence.
 Only the concept can be extended not the real the ultimate
 reality ultimate reality is just
 ultimate reality and it does not lend itself to to being
 extended.
 So this consciousness or the first consciousness cannot be
 the first Aruba Vajrayana consciousness
 cannot be extended and the disappearance of consciousness
 need not be extended because
 actually it is concept and it is non-existent.
 So what is non-existent cannot be extended and then the
 last one the base consisting
 of nether perception or non-perception here also the object
 of the base consisting of
 nether perception or non-perception need not be extended
 since it is it true is a state
 consisting in an individual essence.
 Now do you remember the object of the fourth Aruba Vajray
ana?
 The object of the fourth Aruba Vajrayana is the third Aruba
 Vajrayana consciousness.
 So the third Aruba Vajrayana consciousness is again an
 ultimate reality having its own
 individual essence so it cannot be extended because it is
 not not a concept so they cannot
 be extended so this is as to whether it can be extended or
 not.
 And then as to the object paragraph 117 of this 40
 meditation subjects 22 have counterpart
 sign as object.
 Now on the chart under the column nimitta counterpart sign
 PT means counterpart sign.
 So it says 22 have counterpart signs 10 casinos 10 asubas
 or fallness meditation and these
 two.
 Counterpart sign that is to say the 10 casinos the 10 kinds
 of fallness mindfulness of breathing
 and mindfulness occupied with the body the rest do not have
 counterpart signs as object.
 And 12 have states consisting in individual essence as
 object that is to say eight of
 the 10 recollections except mindfulness of breathing and
 mindfulness occupied with the
 body the perception of the passiveness and nutriment the
 defining of the four elements
 the base consisting of boundless consciousness and the base
 consisting neither perception
 or non perception.
 So they have the ultimate reality as object and the 22 have
 counterpart signs as object
 that is to say the 10 casinos the 10 kinds of fallen as
 mindfulness of breathing and
 mindfulness of mindfulness occupied with the body while the
 remaining six have not so classifiable
 objects.
 So these are the description of the objects of meditation
 in different ways and the age
 of mobile objects in the early states through the
 counterpart though the counterpart is stationary
 that is to say the festering the bleeding the warm first
 yet mindfulness of breathing
 and water casino the fire casino the air casino and in the
 case of light casino the object
 consisting of a circle of sunlight etc.
 So they are shaking objects they can be shaking objects the
 rest have mobile objects.
 Now they have shaking objects only in the preliminary stage
 but when when the yogi reaches
 the counterpart counterpart sign states then they are
 stationary so it is only in the preliminary
 stage that they and they have shaking objects or mobile
 objects and as to the plane that
 means as to the different 31 planes of existence 12 namely
 the 10 kinds of fallen as mindfulness
 occupied with the body and perception of repulsiveness in
 new to men do not occur among deity.
 That mean that in the fourth jar the breathing stops.
 We come to that in the description of the breathing
 meditation so that's right and
 then as to apprehending that means as to taking the object
 by by sight by by hearing by seeing
 and so on so here according to sight touch and hearsay
 hearsay means just hearing something
 and these 19 that is to say nine casinos omitting the air
 casino and the 10 kinds of fallenness
 must be apprehended by sight so that means you look at
 something and practice meditation
 the meaning is that in the early stage the assigned must be
 apprehended by constantly
 looking with the eye in the case of mindfulness occupied
 with the body the five parts ending
 with skin must be apprehended by sight and the rest by
 hearsay head hair body hair nails
 teeth skin these you look at with your eyes and practice
 meditation on them and the others
 some some you cannot see like liver or intestines and other
 things so that you practice through
 hearsay mindfulness of breathing must be apprehended by
 touch so when you break this mindfulness
 meditation you keep your mind here and be mindful of the
 sensation of touch here the
 air going going in and out of the nostrils the air casino
 by sight and touch it will
 become clearer when we come to the description of how to
 practice air casino sometimes you
 look at something moving say branches of tree or a banner
 in the wind and you and you practice air
 casino on that but sometimes the wind is blowing against
 your body and you have the feeling of
 touch here and so to concentrate on this and concentrate on
 the wind air element here and so
 in that case you practice by the sense of touch the
 remaining 18 by hearsay that means just by
 hearing the divine abiding of equanimity and the four imm
aterial states are not apprehendable by a
 beginner so you cannot practice upika and the four aruba v
ajirajanas at the beginning because in
 order to get aruba vajirajanas you must have you must you
 must have got the five ruba vajirajanas
 and in order to practice real upika upika brahma vihara
 then you have to break you have to have
 practiced the first three loving kindness confession and
 sympathetic joy so as a as a real divine
 abiding equanimity cannot be practiced at the beginning
 only after you have practiced the other
 three can you practice equanimity and then as to condition
 the ten casinos I mean nine casinos
 omitting the space casino conditions for immaterial states
 that means if you want to get aruba
 vajirajanas and you practice one of the nine casinos o
mitting the space casino because you
 have to practice the removing of the casino object and get
 the space so space cannot be removed to
 space is space so space casino is exempted from from those
 that are conditions for immaterial
 state or aruba vajirajanas the ten casinos are conditions
 for the kinds of direct knowledge
 so if you want to get the direct knowledge or abhinia that
 means supernormal power then you
 practice first one of the ten casinos and actually if you
 want to get different different
 results then you you practice different different casinos
 you know if you want to shake something
 suppose you want to shake the shake the city hall building
 by your supernormal power then first you
 you must practice water casino or air casino but not the
 the earth casino if you practice earth
 casino and try to shake it you it will not shake there is a
 story of a novice who went up to heaven
 I mean the abode of God and he said I will I will shake
 your mansion and he he tried to shake it and
 he could not so he is so the the celestial names make fun
 of him and so he was ashamed and he went
 back to his teacher and he told his teacher that he was
 ashamed by the names because he he could
 not shake the shake their mansion and he asked his teacher
 why so his teacher said look at something
 there so a a cow dung was floating in the in the river so
 he got the hint and so next time he went
 he went back to the the celestial abode and this time he he
 practiced water magic I mean water
 casino first and then maybe maybe mansion shake so
 according to what you want from from the casinos
 you have to practice different kinds of casino and they are
 they are mentioned in the later chapters
 so the ten casinos are conditions for the kinds of direct
 knowledge three divine abidings and
 conditions for the fourth divine abiding so the fourth
 divine abiding cannot be practiced
 at the beginning but only after the first three each lower
 immaterial state is a condition for
 each higher one the base consisting of neither perception
 or non-perception is a condition for
 the attainment of cessation that means cessation of mental
 activities cessation of perception and
 feeling actually cessation of mental activities all are
 conditions for living in bliss that means
 living in bliss in this very life for insight and for the
 fortunate kinds of becoming that means for
 a good life in the future as to suitability to temperament
 from their importance here the
 exposition should be understood according to what is
 suitable to the temperament and then it describes
 which subjects of meditation suitable for which kind of
 temperament yes practice the third before
 the first right but the the normal procedure is to practice
 the loving kindness first and then
 practice the second one and the third one and this this
 this means you practice so that you get jhana
 from this practice if you do not get to the to the state
 state of jhana then even equanimity you can
 practice but here it is meant for jhana because the equanim
ity leads to the fifth jhana so in
 order to get the fifth jhana you need to you have the first
 again that and fourth jhana and those
 can be obtained through the practice of the lower three on
 the other three divine abiding
 you reach Jhana but not not to the fifth jhana you read to
 the fourth jhana owner and by the
 practice of equanimity you reach the fifth jhana and now
 the different kinds of meditation suitable
 for different types of temperaments and all this has been
 stated in the form of direct opposition
 and complete suitability that means if it says this this
 meditation is suitable for this temperament
 it means that this meditation is the direct opposite of
 that temperament and it is very suitable for
 it but it does not mean that you cannot practice other
 meditation so but there is actually no
 profitable development that does not suppress greed etc and
 help faith and so on so in fact
 you can practice any meditation but here the meditations
 subjects of meditation and temperaments
 are given to show that they are the direct opposite of the
 temperaments and they are the
 most suitable suppose I am of deluded temperament then the
 most suitable meditation for me is
 breathing meditation but that does not mean that I cannot
 practice any other meditation because any
 meditation will help me to suppress mental defilements and
 to develop wholesome mental states even in
 the sodas Bhumbura was advising Rahula and other persons to
 practice different kinds of meditation
 not only one meditation but different kinds of meditation
 to one and only person so any meditation
 can be practiced by anyone but if you want to get the best
 out of it then you choose the one which
 is most suitable for your temperament all casinos a object
 implied seeing consciousness
 before I'm right yes see Cassie Cassie Cassina's should be
 practiced first by looking at them so
 when you look at them then you have to be seeing
 consciousness and then you try to memorize or you
 try to to take it into your mind that means you you close
 your eyes and try to take that off the
 image so when you can get the image clearly in your mind
 then you are said to get and the
 what sign learning they call learning sign actually the
 cross sign and after you you you get the
 learning sign and you don't you you no longer need the
 actual actual disk actual object but
 you develop you you dwell on the on the sign you you get in
 your mind so at that moment at that
 time on it is not seeing consciousness but the other manor
 water I mean through mind or not through
 not through I door so first first through I door you look
 at the the Cassina and practice meditation
 and after you get the learning sign then your meditation is
 through mind or you see through
 mind but not through the eye and then say dedicating
 oneself to the blessed one or to the teacher that
 means giving or relinquishing oneself and to them do do to
 the blessed one or to the voter it's all
 right but to the teacher I do not recommend because not all
 teachers are to be trusted
 considering what's happening these days so on the page 119
 said when he dedicates himself to a
 teacher I run and push this my person to you so I give up I
 give myself up to you it may be
 dangerous if a teacher has what to call our terrier motives
 so it is better to to give yourself to
 the Buddha not to the teacher these days okay and then
 sincere inclination a resolution I think
 they are not so difficult now in paragraph 128 about four
 lines for it is one of such sincere
 inclination which arrives at one of the three kinds of
 enlightenment that means enlightenment
 as a Buddha enlightenment as a but jigabuda and
 enlightenment as an arahant so these are three
 kinds of enlightenment and then six kinds of inclination
 lead to the maturing of the enlightenment
 of the bodhisattvas these may be something like a parameter
 is found in Mahayana is non-greet
 non-delusion and so on these are the six inclinations I
 mean six regularly six qualities
 which was bodhisattvas especially develop so with the
 inclination to non-greet bodhisattvas see the
 fruit for see the fourth in greed with the inclination to
 non-hate bodhisattvas see the
 fourth in hate and so on they are not found although it is
 a quotation we cannot trace this
 quotation to any any text available nowadays so some some
 text may have been lost or it may refer
 to you some some other some sort has not belonging to Ther
avada and then the last paragraph 132
 apprehend the sign now the in in Pali the word nimitta is
 used in different different meanings
 so here apprehend the sign means just paying paying close
 attention to what you hear from the teacher
 so this is the preceding clause this is the subsequent
 clause this is the meaning this is
 the intention this is a simile and so on so paying closer
 attention and trying to understand first
 hear the words of the the the teacher and then trying to
 understand it is called here apprehending
 the sign it is not like apprehending the sign when you
 practice meditation so apprehending of
 signs will come later in chapter chapter 4 so here
 apprehending the sign means just paying
 close attention to to what the teacher said so this is
 preceding clause this is subsequent clause
 this is its meaning and so on when he listens attentively
 apprehending the sign in this way
 his meditation subject is well apprehended so he knows he
 knows what he he what he should know
 about meditation and and because of that he successfully
 attains distinction attains distinction
 means attains jhanas attains supernormal knowledge and att
ains enlightenment but not others not
 otherwise but not others he will successfully attain
 distinction but not others who do not
 listen attentively and apprehend the sign and so on so
 otherwise should be corrected to others not
 otherwise on page 121 first line he successfully attains
 distinction but not others on this page
 okay that is the end of that chapter so we are we are still
 preparing preparing is not not not
 over yet you have to you have to avoid 18 faulty monaster
ies and find a suitable place for
 meditation it's interesting about the parmita said
 something that almost works it works for
 them and the end of two is a little bit of a stretch but
 yeah interesting thank you very much
 we're going to continue in March sometime in March we'll do
 another 12 weeks and that will go from
 we hope from chapter four chapter eight through chapter
 eight is there something we can start
 reading it in the meeting yes
 okay
 yeah
 yeah
 yeah
 yeah
 yeah
