 Good evening, everyone.
 Broadcasting Live, April 3rd, 2016.
 Today's quote, again from the Anguttara Nikaya.
 I think Andy's not doing a very good job of translating, in
 my opinion.
 I don't know where he would get attention from.
 So we have to really ignore this quote, the English,
 because he's done a poor job translating.
 There's no way around it.
 So the dhammas here are the four dhammas that lead to, and
 not progress, they lead to benefit.
 Hitta, the word is hitta, which just means welfare or
 benefit.
 And it's not even worldly progress.
 So he's not even translating, he's very much paraphrasing.
 Chatta rome bhayag apancha.
 He's talking to someone named bhayag apancha.
 I don't know this in the back story, anyway, not important.
 Chatta rome dhamma, there are these four dhammas, kuluputas
adhitta dhammahitaya sangatanti.
 So ditta dhamma, again we have this phrase, ditta means
 seen, dhamma is reality.
 So in the reality that is seen.
 In other words, right now, without delay, without, not in
 the next life.
 Ditta dhamma often means referring to this life.
 So ditta dhammahitaya means benefit in this life.
 So it's not exactly worldly, but the point is, these are
 the things that lead to benefit in this life.
 Because it doesn't take practice of the Buddha's teaching
 to succeed in this life.
 You can actually be a terrible, terrible person and still
 succeed to some extent in this life.
 Your life may be shorter than others, or it may eventually
 turn around, but even evil people.
 So for that reason, benefit in this life that only touches
 this life, it's not so much related to the dhamma.
 And hence the use of the word worldly.
 But that's not exactly what it means. These are important,
 but they're more important.
 They're important in a broad sense.
 The four actually can be applied, generally.
 With some, I mean, they can be applied to a monk, they can
 be applied to meditators.
 It's mostly actually directed to lay people, but, and
 people living in the world.
 But nonetheless, they're good to keep in mind.
 And these are actually one of the core teachings in
 cultural Buddhism.
 Like in Thailand, it's a big one.
 These four are something that we used to learn in schools,
 and the novices learned in schools.
 One of the first things we learned.
 Dhammi, prayot, nailokni, dhammas that have benefit in this
 life.
 Prayot, dohni, prayot, lokna.
 So they pair with another set of dhammas, which are those
 that have samparayaka, hita.
 Samparayaka, hita, something like that.
 Which is the dhammas that have benefit in the next life.
 It's another set of four dhammas. So we'll talk about those
 as well.
 In fact, we even have a third set.
 So we can round it off and talk about all the dhammas that
 have benefit.
 This monk in Bangkok, he used to talk about this a lot.
 Prayot, lokni, prayot, lokna, prayot, sounsoun.
 The third one is sounsoun, which means the very highest
 benefit.
 Prayot, kunipan, that which leads you to niban, nirvana.
 But his first four, utthana sampada, means effort. You have
 to be endowed with effort.
 dita dhamma hita, dita dhamma sukhai. If you want benefit,
 if you're looking for your welfare and happiness in the
 here and now,
 you need effort. You need to work.
 So he explains what that means. You have to make your
 living.
 You have to be skillful and diligent, possessing judgment
 about it
 in order to carry it out and arrange it properly.
 Initiative, bhikkhu bodhi translates as initiative.
 I'm still not convinced, but he means the gumption, taking
 up the task.
 In all things, this is the case. The question is, are you
 making effort in your work?
 So this applies for meditators as well. If you want the
 meditation to be successful,
 you have to work at it. To some extent, we can apply this.
 Right. And this is actually related to, as I said, it was
 being talked to,
 Dika Jahnu, who goes by the clan name Viagapadja. Anyway.
 So he says, "We use sandalwood from Kasi. We wear garlands,
 scents and ungivens.
 We receive gold and silver." Meaning, we're not monks.
 We're not even spiritually religious people. We live in the
 world, we work,
 and we play, and we laugh, and we sing, and we eat, and we
 drink, and make merry.
 But what can we do? He says, "Teach us the Dhamma in a way
 that will lead to our
 welfare and happiness in this present life and in future
 lives."
 "Teach the Dhamma Sukhaya, deach the Dhamma Hittaya." And
 then the other one, so in future lives,
 "Samparaya Hitta." Sorry. "Samparaya Hittaya, Samparaya Su
khaya." It's the other one.
 So for the next life. So the Buddha separates them because
 they're two different things.
 As I said, in this life, it's more related to sort of
 functional things.
 So the first one is you have to work. You have to have the
 initiative.
 You can't just sit around and wait for happiness and
 welfare to come to you.
 If you want to succeed in life, you have to work.
 Number two, "Arakha Sampada." You have to guard or protect
 or save up, right?
 You have to guard your wealth, like which you have amassed.
 You have to think to yourself, "How can I prevent kings and
 thieves from taking it,
 fire from burning it, floods from shaping it off, and
 displeasing heirs from taking it?"
 You know, heirs can be displeasing. Aries, as in people who
 inherit your things,
 maybe they come looking for their inheritance early or
 something.
 So what can I do to sustain my life? Again, a worldly thing
, but it works for all of us.
 I have to concern myself about my robes and my belongings.
 But in regards to meditation, this is important as well.
 Not only do we have to work hard in meditation, but we have
 to be careful.
 Ajahn Tong talks about this similarly, I think, from the
 Visuddhi manga,
 someone rocking a cradle.
 In the olden days, they would have the baby in a hammock,
 maybe, I don't know,
 or in a cradle, I guess, and they have it on a string, so
 they could sit far away
 and they'd just pull on the string to keep the rocker going
.
 And that would keep the baby asleep. But you had to be
 careful.
 If you didn't keep your eye on it, the baby would wake up.
 So the idea is that meditation is like something very
 delicate
 that you have to keep your mind on. If you're not paying
 attention, the baby will wake up.
 You'll lose your mindfulness. It's very easy to get off
 track.
 You see, in meditation again and again, you'll get off
 track
 and you have to bring yourself back again and again,
 guarding.
 So it's analogous to the... it works on that level as well.
 But in terms of anything, you could put this on any level.
 You need to... anything you do, you need to have effort
 and you have to be careful, guard, guard what you have
 gained through your effort.
 So we don't squander. Sometimes people come to meditate
 here
 and then they go out in the world and think they're
 invincible
 because it's easier to be invincible here. There's not so
 much bothering you.
 When you go out there, you realize, "Oh, I didn't...
 maybe I didn't quite get as much out of the meditation as I
 thought.
 Maybe I got this much and you thought you got this much."
 And of course, your expectations are not met because you're
 not mindful.
 Sometimes we go out and it's a shock.
 You wake up, call and you realize you have to guard
 yourself.
 Part of it is negligence.
 But that's the thing, is you can't be negligent.
 If you want to succeed in anything meditate, spiritual or
 worldly,
 you have to be careful.
 Number three is meeting good friends, "kali anamitata."
 This of course goes with everything.
 In Thailand, in meditation, this was a problem.
 Too many people in the monastery and it's easy to get
 caught up with the wrong people.
 Easy to sit down and chat and to get sidetracked.
 And we sidetracked each other as well.
 Luckily here in Hamilton, we have very small centers
 and meditators come and they're not bothered by others.
 But by good friendship, it means people who are practicing,
 who appreciate practice, who teach practice,
 who accommodate you in your practice.
 People who care about your spiritual practice.
 People who can offer you advice and who can give you the
 space you need
 and that kind of thing.
 People who can guide you and be an example for you.
 That's a good friend. The Buddha is our good friend really.
 That's why we think about the Buddha a lot because
 as someone who when you think about him, it gives you a
 good example.
 It gives you something to relate your practice to.
 Someone who has gone the distance.
 Number four is samajivita.
 Samajivita.
 Samaji-vita.
 It should be samajivita.
 Samajivita means living according to your means.
 So this is a fairly worldly one.
 So with friendship, of course, that's obviously important
 in the world as well.
 If you don't have good friends, you won't succeed.
 Having to deal with the stress of enemies and having to
 deal with their problems,
 having to deal with people who lead you in the wrong
 direction.
 It applies in all aspects.
 Samajivita is mostly related to the world. It's explicitly.
 This means living within your means.
 So if you want to succeed in the world, you have to not
 only acquire and protect your wealth,
 but you have to not squander it yourself.
 You have to budget and you have to be content with what you
 have and that kind of thing.
 Because if you live outside your means, this is where
 people go into debt.
 It might seem like a trite sort of statement,
 but it's amazing how many people live outside their means.
 Aren't they able to stop themselves from borrowing money
 and getting into serious debt?
 And they can say, you have to be careful.
 But the simple word we can apply to meditation as well.
 Because I want to really apply this to meditation.
 We want to be talking about what is of most benefit.
 We don't have all that much time to focus too much on
 worldly things, even though they're healthy.
 And also I've got a meditator here listening, so I have to
 help him, give him something to think about.
 But samadhi vita means the way you're living to be balanced
.
 Not too much excess, but not too little.
 So the excess and the lack, if not enough, too much, both
 are problematic.
 If you don't practice enough, if you practice too much,
 you practice without taking a break, it can also be to your
 detriment.
 But mostly living throughout the day balanced with a
 balanced mind,
 balancing your faculties, confidence, effort, mindfulness,
 concentration,
 and wisdom balancing them, and not practicing too hard,
 not practicing too little.
 We know the work we have to do,
 so we do it consistently and systematically.
 That's what's important.
 Anyway, those are the benefits in this world, mostly
 worldly things.
 The ones that lead to benefit in the next life, I think he
 then goes on.
 There are four other things that lead to happiness and
 welfare in future lives.
 So these are a little bit more spiritual, but this is more
 leading to heaven.
 Let's see if I can get the Pali.
 Here we are.
 So the four, these are the samparaya, samparaya hitta,
 samparaya hitta, samparaya sukhaya.
 You need to benefit in the next life.
 So these are more spiritual, and these are, you could say,
 sort of the positive, mundane results.
 Heaven being still mundane, but these are the sort of
 things that lead you to heaven.
 So there's a positive, mundane results of spiritual
 practice, like meditation.
 But actually, these four are not all that mundane at all,
 but they're sort of relating to going to heaven,
 because again, he's telling this to a layman.
 He's not probably meditating, but probably should.
 But anyway, as Buddha gives him these four,
 sanda means confidence, or you could say faith.
 Sada, sampada means you have right faith, faith in the
 right thing,
 faith in the Buddha, the Dhammasanga, faith in spiritual
 teaching,
 faith in good people, faith in good things,
 confidence in yourself, confidence in good things about
 yourself,
 confidence in your ability to cultivate goodness.
 Number two, sila sampada, morality, endowed with morality.
 There are the five precepts, guarding your senses,
 and considering the use of your requisite so that the
 things you use,
 you don't use them for the wrong purposes,
 and right livelihood.
 And number three, jaga sampada, endowed with generosity.
 Or jaga is maybe renunciation or abandoning, giving up, I
 guess.
 We usually translate it as generosity,
 and in a mundane sense that's what it means.
 It means giving gifts and supporting others and being
 charitable.
 But on a deeper level it means giving up.
 And you give up your desire.
 Giving up your desire is actually a great gift,
 because the less desire you have,
 the more you're able to give and give up and help,
 support others and do for others.
 But if you're always obsessed with your own benefit,
 you have a very little time to give to others.
 Number four, upanya sampada, wisdom.
 One should be endowed with wisdom.
 This is a great benefit.
 It's also a benefit for this life,
 but sometimes wise people still may not do that well in
 this life.
 Nonetheless, they do well in their minds.
 Wisdom is the greatest, the most powerful weapon we have,
 the tool we have for benefit, for welfare, for happiness.
 Because with wisdom you know right from wrong.
 Wisdom, you're able to rise above your problems
 when things go wrong.
 You're able to see them as they are,
 and it's not really even about rising above them.
 It's seeing through the cloud of ignorance
 that leads us to see things as problems.
 Because there's no such thing.
 Problem is just a name that we give, a label that we give
 to something.
 The truth is there's experience.
 Wisdom helps us to see that.
 As a result, we don't get attached to things because we,
 out of wisdom we see that that's a problem.
 It doesn't lead to happiness.
 We don't get angry about things because we see that that
 leads to problems.
 When we give up delusion, wisdom is the opposite of
 delusion.
 Wisdom is like the bright light.
 When you shine the bright light, the darkness goes away.
 So the Buddha said,
 "Vinayyalokey abidja dolmanasam."
 Giving up in this world, greed and anger, desire and
 aversion.
 And Ajahn Tong, he said, "Why doesn't he ask?"
 I think the commenter actually says, "Why doesn't?"
 But I remember a talk, Ajahn Tong said,
 "Why doesn't the Buddha mention delusion?"
 It's because mindfulness, this is from the satipatanas,
 mindfulness is like a bright light.
 When you shine it in, the darkness disappears.
 So you don't even have to mention delusion.
 When you're being mindful, you're working to give up greed
 and anger.
 Because you've removed the delusion during that time, there
's wisdom.
 Wisdom can arise about the greed and the anger.
 And so those four are that which leads to benefit in future
 lives.
 And the third group that I said, the one which leads to nir
vana is actually the four satipatanas, I think.
 I'm pretty sure that's what Numbhochodong used to say.
 What are the four that lead to nirvana?
 Nirvana, well, it doesn't say in this suta, this only gives
 the two sets.
 But the third set you have to talk about, if you're looking
 for that, which is the ultimate benefit,
 it's the four satipatanas.
 Because the Buddha said, "Eka yanoi ang bika vai mango."
 This is the one way, the direct way that leads to nirvana.
 And that's the four satipatanas.
 Anyway, so another little bit of dhamma tonight.
 I still have more work to do tonight.
 We're at crunch time.
 Tomorrow, I don't know if I'll be able to broadcast.
 We have the symposium on Tuesday.
 And people have been emailing us.
 Students leave everything to the last moment.
 I should have known that they're all emailing us all days,
 apologizing for waiting for the last minute
 and hoping they can join the symposium.
 Anyway, so that's all for tonight.
 Have a good night, everyone.
 Thank you.
