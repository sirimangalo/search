1
00:00:00,000 --> 00:00:20,190
 Okay, so we're broadcasting here from Stony Creek, Ontario,

2
00:00:20,190 --> 00:00:23,640
 hopefully.

3
00:00:23,640 --> 00:00:42,270
 We have a live local audience and maybe even a live

4
00:00:42,270 --> 00:00:51,000
 internet audience.

5
00:00:51,000 --> 00:01:03,680
 Yesterday we talked about Nukkuk, the truth of suffering.

6
00:01:03,680 --> 00:01:17,280
 Today I thought I'd talk about the way out of suffering.

7
00:01:17,280 --> 00:01:20,280
 That's really it.

8
00:01:20,280 --> 00:01:24,750
 How to be free from suffering, it's fine to talk about it,

9
00:01:24,750 --> 00:01:27,520
 to theorize, philosophize

10
00:01:27,520 --> 00:01:38,610
 about the truth of life and the problem, the reason why we

11
00:01:38,610 --> 00:01:43,840
 suffer and so on.

12
00:01:43,840 --> 00:01:48,750
 But it's all just talk if we don't have a way out, so we

13
00:01:48,750 --> 00:01:51,200
 need to find the path, the

14
00:01:51,200 --> 00:01:59,760
 way out of suffering.

15
00:01:59,760 --> 00:02:06,920
 It's important that we explain this path, give you some

16
00:02:06,920 --> 00:02:10,400
 kind of outline as to the stages

17
00:02:10,400 --> 00:02:13,400
 of the path.

18
00:02:13,400 --> 00:02:23,210
 There's one important reason, it's simply for encouragement

19
00:02:23,210 --> 00:02:23,720
.

20
00:02:23,720 --> 00:02:31,160
 Meditation practice is not easy.

21
00:02:31,160 --> 00:02:36,150
 And it's sometimes hard to remind yourself, hard to

22
00:02:36,150 --> 00:02:39,560
 remember why you're doing this, hard

23
00:02:39,560 --> 00:02:44,880
 to remind yourself of the benefits of the purpose.

24
00:02:44,880 --> 00:02:47,870
 If you don't see the light at the end of the tunnel or if

25
00:02:47,870 --> 00:02:49,720
 you don't see the bigger picture,

26
00:02:49,720 --> 00:02:53,560
 if you're not able to see the path.

27
00:02:53,560 --> 00:02:58,250
 So an outline of the path is useful also to allow you to

28
00:02:58,250 --> 00:03:00,360
 see the direction we're going

29
00:03:00,360 --> 00:03:07,510
 in, allow you to stay on the path, to help you to stay on

30
00:03:07,510 --> 00:03:09,160
 the path.

31
00:03:09,160 --> 00:03:13,880
 Knowing what is the path and what is not the path.

32
00:03:13,880 --> 00:03:14,490
 The path that the Buddha taught, that he claimed lays out

33
00:03:14,490 --> 00:03:19,960
 of suffering is called the Visuddhimagga,

34
00:03:19,960 --> 00:03:32,320
 the path of purification.

35
00:03:32,320 --> 00:03:40,220
 So the theory is that when the mind is pure, there's an end

36
00:03:40,220 --> 00:03:42,600
 to suffering.

37
00:03:42,600 --> 00:03:49,600
 It's because of impurity of the mind that we suffer.

38
00:03:49,600 --> 00:03:59,410
 And so in fact the path isn't described in terms of

39
00:03:59,410 --> 00:04:04,000
 escaping suffering, even though that's

40
00:04:04,000 --> 00:04:06,000
 what it's called.

41
00:04:06,000 --> 00:04:10,800
 It's the path that leads to the cessation of suffering.

42
00:04:10,800 --> 00:04:26,080
 It's described in terms of purity.

43
00:04:26,080 --> 00:04:31,530
 And so the tradition is we recognize seven Visuddhis or

44
00:04:31,530 --> 00:04:34,240
 seven purifications that are

45
00:04:34,240 --> 00:04:38,560
 required in their sequence.

46
00:04:38,560 --> 00:04:41,440
 So you have to go from one to seven.

47
00:04:41,440 --> 00:04:46,240
 And this is what we're doing here during this course.

48
00:04:46,240 --> 00:04:51,780
 You want a broader picture of what you're doing or what you

49
00:04:51,780 --> 00:04:53,600
've been doing.

50
00:04:53,600 --> 00:04:57,040
 This should help.

51
00:04:57,040 --> 00:05:03,120
 The first Visuddhi is Sila, morality.

52
00:05:03,120 --> 00:05:07,320
 We've already started talking about this on Sunday classes

53
00:05:07,320 --> 00:05:08,720
 on the internet.

54
00:05:08,720 --> 00:05:13,830
 If you're interested, you can join the Visuddhimagga class

55
00:05:13,830 --> 00:05:17,160
 and you can listen to the sessions on

56
00:05:17,160 --> 00:05:22,520
 the internet if you haven't recorded.

57
00:05:22,520 --> 00:05:35,640
 And in brief Sila means your behavior or what is normal for

58
00:05:35,640 --> 00:05:46,520
 you, what is your habitual nature.

59
00:05:46,520 --> 00:05:51,070
 So some people are of a nature to do evil deeds, perform

60
00:05:51,070 --> 00:05:54,040
 evil deeds and hurt others and so on.

61
00:05:54,040 --> 00:06:01,080
 Some people are of a nature to do good deeds or to avoid

62
00:06:01,080 --> 00:06:03,080
 evil deeds.

63
00:06:03,080 --> 00:06:06,710
 Here we mean specifically the avoidance of unwholesome

64
00:06:06,710 --> 00:06:10,680
 deeds, things that sully the mind,

65
00:06:10,680 --> 00:06:21,560
 the means of action or speech that hurt us, hurt our minds.

66
00:06:21,560 --> 00:06:28,930
 So we look at it actually as a fairly selfish or self-

67
00:06:28,930 --> 00:06:32,720
centered sort of thing.

68
00:06:32,720 --> 00:06:34,730
 Killing is something that hurts the person who kills,

69
00:06:34,730 --> 00:06:36,040
 stealing is something that hurts

70
00:06:36,040 --> 00:06:39,880
 the person who steals.

71
00:06:39,880 --> 00:06:47,650
 It's because of what it does to the other person that's a

72
00:06:47,650 --> 00:06:51,200
 selling of your mind by hurting

73
00:06:51,200 --> 00:07:01,070
 others, reacting in such a way as to create suffering for

74
00:07:01,070 --> 00:07:02,560
 others.

75
00:07:02,560 --> 00:07:13,120
 This is unwholesome, this is something that actually hurts

76
00:07:13,120 --> 00:07:15,280
 the doer.

77
00:07:15,280 --> 00:07:19,810
 Sullies the doer's mind makes them more greedy, more angry,

78
00:07:19,810 --> 00:07:22,360
 more deluded, more perverse, more

79
00:07:22,360 --> 00:07:26,920
 agitated.

80
00:07:26,920 --> 00:07:29,920
 And so this is the basis of meditation practice.

81
00:07:29,920 --> 00:07:32,920
 But we couldn't practice.

82
00:07:32,920 --> 00:07:36,810
 If we were killing and stealing and lying and cheating,

83
00:07:36,810 --> 00:07:38,960
 taking drugs and alcohol, we

84
00:07:38,960 --> 00:07:43,680
 wouldn't be able to practice at all.

85
00:07:43,680 --> 00:07:49,240
 Sīla Vīsuddhi is most basic.

86
00:07:49,240 --> 00:07:52,920
 But deeper than that, Sīla Vīsuddhi is the beginning of

87
00:07:52,920 --> 00:07:54,840
 the meditation practice.

88
00:07:54,840 --> 00:07:58,820
 It's the walking that you do when you walk and your mind is

89
00:07:58,820 --> 00:08:01,160
 with the foot, when the effort

90
00:08:01,160 --> 00:08:04,340
 you make to bring your mind back to the foot, and when you

91
00:08:04,340 --> 00:08:05,920
 sit in the effort you make to

92
00:08:05,920 --> 00:08:09,680
 bring your mind back to the rising and the falling.

93
00:08:09,680 --> 00:08:12,960
 This is Sīla.

94
00:08:12,960 --> 00:08:19,800
 This is the ultimate Sīla, the real Sīla.

95
00:08:19,800 --> 00:08:22,960
 Not just conceptual dealing with stealing and killing and

96
00:08:22,960 --> 00:08:23,720
 talking.

97
00:08:23,720 --> 00:08:28,800
 It deals with the very basic actions.

98
00:08:28,800 --> 00:08:34,150
 Walking when we walk and our mind is clear, when we bring

99
00:08:34,150 --> 00:08:36,840
 our mind to focus on the walking

100
00:08:36,840 --> 00:08:41,280
 as a Sīla.

101
00:08:41,280 --> 00:08:43,900
 What it does is it leads to the second Vīsuddhi which is J

102
00:08:43,900 --> 00:08:48,080
yotavi Suddhi, leads to a pure mind.

103
00:08:48,080 --> 00:08:57,350
 So every moment that we walk and our mind is pure, every

104
00:08:57,350 --> 00:09:02,360
 moment we're aware of the rising,

105
00:09:02,360 --> 00:09:03,360
 our mind is pure.

106
00:09:03,360 --> 00:09:11,420
 When we say seeing, seeing, aware of the seeing, our mind

107
00:09:11,420 --> 00:09:12,960
 is pure.

108
00:09:12,960 --> 00:09:19,240
 This is Jyotavi Suddhi.

109
00:09:19,240 --> 00:09:20,680
 You begin with in the meditation.

110
00:09:20,680 --> 00:09:27,020
 The first couple of days is just learning how to do this,

111
00:09:27,020 --> 00:09:30,080
 how to purify that mind.

112
00:09:30,080 --> 00:09:39,320
 Of course this just means momentarily.

113
00:09:39,320 --> 00:09:42,630
 The work is not finished, not complete because these minds

114
00:09:42,630 --> 00:09:43,840
 are only momentary.

115
00:09:43,840 --> 00:09:48,840
 It's not the entire sequence of minds that we've purified.

116
00:09:48,840 --> 00:09:56,640
 There's still the potential for unwholesome minds to arise,

117
00:09:56,640 --> 00:09:58,360
 of course.

118
00:09:58,360 --> 00:10:01,120
 So next we have Diti-Vīsuddhi.

119
00:10:01,120 --> 00:10:06,490
 In order to sustain this purity of mind we have to change

120
00:10:06,490 --> 00:10:09,280
 our view, we have to change

121
00:10:09,280 --> 00:10:19,040
 the causes behind our experiences, behind the mind.

122
00:10:19,040 --> 00:10:24,600
 Because our view starts with view.

123
00:10:24,600 --> 00:10:38,120
 Diti-Vīsuddhi just means to see the reality of experience.

124
00:10:38,120 --> 00:10:42,560
 We start upon the, this is the beginning of wisdom.

125
00:10:42,560 --> 00:10:46,800
 We start by seeing body and mind, seeing that in every

126
00:10:46,800 --> 00:10:48,880
 experience there's only body and

127
00:10:48,880 --> 00:10:49,880
 mind.

128
00:10:49,880 --> 00:10:55,350
 And the rising, the rising, when we say rising, the rising

129
00:10:55,350 --> 00:10:58,320
 is the body, the knowing is the

130
00:10:58,320 --> 00:10:59,320
 mind.

131
00:10:59,320 --> 00:11:04,920
 But the arising sees there's no self, no soul involved.

132
00:11:04,920 --> 00:11:05,920
 It's just experience.

133
00:11:05,920 --> 00:11:15,880
 It's not the self that sees, it's only seeing.

134
00:11:15,880 --> 00:11:32,310
 And then that seeing arises and sees, the seeing is made up

135
00:11:32,310 --> 00:11:39,440
 of the eye and the mind.

136
00:11:39,440 --> 00:11:43,880
 Diti-Vīsuddhi is just this, just understanding the basis

137
00:11:43,880 --> 00:11:45,040
 of reality.

138
00:11:45,040 --> 00:11:52,920
 So it's adjusting the way we look at the world.

139
00:11:52,920 --> 00:11:56,180
 This is the beginning of insight meditation, you need this

140
00:11:56,180 --> 00:11:57,560
 in order to progress.

141
00:11:57,560 --> 00:12:00,480
 Otherwise you're focusing on entities.

142
00:12:00,480 --> 00:12:05,660
 When you hear something, you hear a bird, you think of the

143
00:12:05,660 --> 00:12:06,480
 bird.

144
00:12:06,480 --> 00:12:13,080
 If you think of the bird then you're not focused on the

145
00:12:13,080 --> 00:12:15,080
 experience.

146
00:12:15,080 --> 00:12:20,300
 Then you give rise to all sorts of ideas and judgments

147
00:12:20,300 --> 00:12:22,080
 about the bird.

148
00:12:22,080 --> 00:12:25,700
 You just focus on hearing, you can see the hearing arising,

149
00:12:25,700 --> 00:12:27,400
 the seeing in the mind that

150
00:12:27,400 --> 00:12:30,080
 knows it also arising.

151
00:12:30,080 --> 00:12:34,360
 This is Diti-Vīsuddhi.

152
00:12:34,360 --> 00:12:41,880
 Therefore, Kankavitārana-Vīsuddhi, overcoming doubt.

153
00:12:41,880 --> 00:12:46,640
 So after seeing the way the body and mind arise and sees,

154
00:12:46,640 --> 00:12:48,760
 you start to see how they

155
00:12:48,760 --> 00:12:50,640
 work together.

156
00:12:50,640 --> 00:12:59,060
 Up until this point you're still uncertain about the nature

157
00:12:59,060 --> 00:13:04,320
 of reality, what is good

158
00:13:04,320 --> 00:13:06,640
 and what is bad and so on.

159
00:13:06,640 --> 00:13:08,760
 What is good and what is evil.

160
00:13:08,760 --> 00:13:16,680
 Kankavitārana-Vīsuddhi begins you in the right direction.

161
00:13:16,680 --> 00:13:21,390
 You start to see that certain mind states lead to other

162
00:13:21,390 --> 00:13:24,160
 certain mind states, certain

163
00:13:24,160 --> 00:13:31,680
 experiences, certain reactions lead to certain results.

164
00:13:31,680 --> 00:13:38,480
 So you start to get a picture of the path, the way to go.

165
00:13:38,480 --> 00:13:46,120
 Kankavitārana-Vīsuddhi is understanding of karma.

166
00:13:46,120 --> 00:13:51,750
 So after the first couple of days of maybe three or four

167
00:13:51,750 --> 00:13:55,160
 days of understanding how body

168
00:13:55,160 --> 00:13:59,590
 and mind arise and sees, you start to see how they work

169
00:13:59,590 --> 00:14:00,480
 together.

170
00:14:00,480 --> 00:14:03,270
 You get angry and you see how that leads to suffering and

171
00:14:03,270 --> 00:14:05,080
 you get greedy and want something

172
00:14:05,080 --> 00:14:10,480
 and you see how that distracts you and pulls you in and

173
00:14:10,480 --> 00:14:14,160
 sets you up for disappointment.

174
00:14:14,160 --> 00:14:17,750
 You see how mindfulness purifies the mind and clears the

175
00:14:17,750 --> 00:14:19,760
 mind, allows you to see things

176
00:14:19,760 --> 00:14:24,360
 clearer than before.

177
00:14:24,360 --> 00:14:27,060
 This clears up all our doubts about right and wrong and

178
00:14:27,060 --> 00:14:27,920
 good and bad.

179
00:14:27,920 --> 00:14:35,120
 This is the factor of the path that does that.

180
00:14:35,120 --> 00:14:39,440
 But we continue on.

181
00:14:39,440 --> 00:14:47,830
 Number five is manga, manga, manga, manga, jnana-dasana-s

182
00:14:47,830 --> 00:14:49,200
uddhi.

183
00:14:49,200 --> 00:14:53,310
 Purification by knowledge and vision of what is and what

184
00:14:53,310 --> 00:14:54,720
 isn't the path.

185
00:14:54,720 --> 00:15:02,280
 So as you progress and after this we should be.

186
00:15:02,280 --> 00:15:06,120
 You have already an understanding of right and wrong.

187
00:15:06,120 --> 00:15:11,760
 You should be getting an understanding of right and wrong.

188
00:15:11,760 --> 00:15:16,320
 Then you start to see the way to straighten out your mind,

189
00:15:16,320 --> 00:15:18,520
 the way to attain goodness

190
00:15:18,520 --> 00:15:26,520
 and to give up unwholesomeness.

191
00:15:26,520 --> 00:15:29,670
 At this stage, this is when meditators often get off track

192
00:15:29,670 --> 00:15:31,400
 or catch themselves when they

193
00:15:31,400 --> 00:15:32,400
 get off track.

194
00:15:32,400 --> 00:15:37,410
 So up until this point they might be attracted by pleasant

195
00:15:37,410 --> 00:15:40,240
 experiences and there may arise

196
00:15:40,240 --> 00:15:42,160
 great peace and calm.

197
00:15:42,160 --> 00:15:45,310
 There may arise bright lights, colors, pictures and all

198
00:15:45,310 --> 00:15:47,280
 these things attract the meditator's

199
00:15:47,280 --> 00:15:55,010
 attention and the meditator therefore gets lost and their

200
00:15:55,010 --> 00:15:59,680
 meditation stalls because they're

201
00:15:59,680 --> 00:16:06,400
 no longer mindful of the reality of it.

202
00:16:06,400 --> 00:16:12,320
 They're absorbed, they're fascinated.

203
00:16:12,320 --> 00:16:16,160
 You're caught up in the experience.

204
00:16:16,160 --> 00:16:19,760
 This vishundhi is realizing that that's not the path,

205
00:16:19,760 --> 00:16:22,480
 realizing that even peace and calm,

206
00:16:22,480 --> 00:16:25,400
 this is not the path.

207
00:16:25,400 --> 00:16:30,520
 So as you understand what is right and wrong, the next step

208
00:16:30,520 --> 00:16:33,000
 is you clarify for yourself

209
00:16:33,000 --> 00:16:41,760
 what is the path to the ultimate good and right.

210
00:16:41,760 --> 00:16:49,920
 Number six is patibadda nyanadasum asrudhi, knowledge and

211
00:16:49,920 --> 00:16:55,240
 vision of the path, of the practice.

212
00:16:55,240 --> 00:16:59,680
 So once you understand what is the path, then you're set.

213
00:16:59,680 --> 00:17:04,620
 So after you've been here a week or maybe a couple of weeks

214
00:17:04,620 --> 00:17:07,040
, you really start to feel

215
00:17:07,040 --> 00:17:08,120
 set on the path.

216
00:17:08,120 --> 00:17:10,040
 You get a sense of what is the path.

217
00:17:10,040 --> 00:17:14,700
 And it's just a matter of practicing and it's not any

218
00:17:14,700 --> 00:17:15,640
 easier.

219
00:17:15,640 --> 00:17:20,760
 It's not easy, but it's easier in the sense that you're no

220
00:17:20,760 --> 00:17:23,800
 longer on the wrong path, you're

221
00:17:23,800 --> 00:17:27,480
 no longer confused about the right and wrong path.

222
00:17:27,480 --> 00:17:31,330
 It's at this stage that you go through many different

223
00:17:31,330 --> 00:17:34,040
 stages of insight, seeing things

224
00:17:34,040 --> 00:17:44,880
 deeper and understanding the, deeper the nature of your

225
00:17:44,880 --> 00:17:46,560
 mind.

226
00:17:46,560 --> 00:17:51,590
 Start clearing out old habits and adjusting your viewpoint,

227
00:17:51,590 --> 00:17:56,800
 adjusting your ways of thinking.

228
00:17:56,800 --> 00:18:02,720
 It's the path of practice.

229
00:18:02,720 --> 00:18:06,760
 After this there is nyanadasum asrudhi, the final

230
00:18:06,760 --> 00:18:09,040
 realization.

231
00:18:09,040 --> 00:18:12,580
 As you practice on and on, your mind sees clearer and

232
00:18:12,580 --> 00:18:15,120
 clearer until there finally arises

233
00:18:15,120 --> 00:18:18,770
 what we talked about yesterday, this moment of truth, where

234
00:18:18,770 --> 00:18:20,480
 your mind becomes clear and

235
00:18:20,480 --> 00:18:25,580
 perfectly refined and there's the cessation, the experience

236
00:18:25,580 --> 00:18:28,200
 of the cessation of suffering.

237
00:18:28,200 --> 00:18:33,890
 It could be just for a moment, it could be for an hour, it

238
00:18:33,890 --> 00:18:36,080
 could be for a day.

239
00:18:36,080 --> 00:18:39,380
 Complete cessation, no thinking, seeing, hearing, smelling,

240
00:18:39,380 --> 00:18:40,600
 tasting, thinking.

241
00:18:40,600 --> 00:18:47,420
 And the mind is free from suffering just for a moment or

242
00:18:47,420 --> 00:18:48,840
 longer.

243
00:18:48,840 --> 00:18:51,680
 But the result is a deep sense of peace.

244
00:18:51,680 --> 00:18:59,030
 There's a sense that something is different, that one has

245
00:18:59,030 --> 00:19:02,960
 realized something about life.

246
00:19:02,960 --> 00:19:08,540
 And as one experiences this state again and again, one's

247
00:19:08,540 --> 00:19:11,680
 mind settles into it, becomes

248
00:19:11,680 --> 00:19:17,680
 less distracted, less caught up in the world.

249
00:19:17,680 --> 00:19:27,120
 The mind becomes more at peace.

250
00:19:27,120 --> 00:19:29,790
 This is the path, this is what we call the path of pur

251
00:19:29,790 --> 00:19:30,680
ification.

252
00:19:30,680 --> 00:19:32,680
 The mind becomes pure.

253
00:19:32,680 --> 00:19:37,120
 The mind gives up its attachments, gives up its aversions.

254
00:19:37,120 --> 00:19:40,970
 It's no longer interested in the world, no longer

255
00:19:40,970 --> 00:19:43,840
 interested in finding happiness in

256
00:19:43,840 --> 00:19:49,200
 the arising and ceasing phenomena around us.

257
00:19:49,200 --> 00:19:52,200
 It's at peace within itself.

258
00:19:52,200 --> 00:20:02,120
 It needs no external stimulus for satisfaction.

259
00:20:02,120 --> 00:20:08,840
 So that's I think pretty deep.

260
00:20:08,840 --> 00:20:09,840
 Good enough for tonight.

261
00:20:09,840 --> 00:20:14,840
 There you have been, Dhamma, for today.

262
00:20:14,840 --> 00:20:15,840
 Thank you all for listening.

263
00:20:15,840 --> 00:20:15,840
 Keep practicing.

264
00:20:15,840 --> 00:20:16,840
 1

