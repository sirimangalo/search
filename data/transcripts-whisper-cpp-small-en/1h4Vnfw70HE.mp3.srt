1
00:00:00,000 --> 00:00:06,680
 Good evening everyone.

2
00:00:06,680 --> 00:00:13,840
 Broadcasting live, October 26th, 2015.

3
00:00:13,840 --> 00:00:16,480
 Tonight we're not going to do the Dhammapada.

4
00:00:16,480 --> 00:00:18,560
 I'm feeling a little bit under the weather.

5
00:00:18,560 --> 00:00:22,580
 I don't think I'm sick, but I didn't get around to looking

6
00:00:22,580 --> 00:00:24,160
 at the Dhammapada.

7
00:00:24,160 --> 00:00:27,680
 I'm probably going to call it an early night.

8
00:00:27,680 --> 00:00:32,340
 And there's a couple of people visiting here and downstairs

9
00:00:32,340 --> 00:00:32,680
.

10
00:00:32,680 --> 00:00:35,160
 Nothing tonight and tomorrow night as well.

11
00:00:35,160 --> 00:00:37,800
 Tomorrow is the full moon.

12
00:00:37,800 --> 00:00:41,600
 So officially end of the rains.

13
00:00:41,600 --> 00:00:46,520
 We enter into the cold season officially.

14
00:00:46,520 --> 00:00:52,190
 And tomorrow evening I will be at Stony Creek doing an

15
00:00:52,190 --> 00:00:54,640
 official ceremony with the other

16
00:00:54,640 --> 00:00:58,320
 monks there to leave the rains.

17
00:00:58,320 --> 00:01:01,480
 It's not even a ceremony to leave the rains, it's just we

18
00:01:01,480 --> 00:01:03,120
 ask forgiveness from each other

19
00:01:03,120 --> 00:01:04,120
 basically.

20
00:01:04,120 --> 00:01:08,080
 We give a permission to the others to criticize us.

21
00:01:08,080 --> 00:01:13,520
 It's kind of a nice ceremony.

22
00:01:13,520 --> 00:01:16,140
 Maybe I'll take them up on it and start criticizing them.

23
00:01:16,140 --> 00:01:18,040
 Something you never do.

24
00:01:18,040 --> 00:01:20,950
 No one ever actually goes ahead and criticizes us, but I

25
00:01:20,950 --> 00:01:22,680
 think I'll maybe joke about it or

26
00:01:22,680 --> 00:01:26,320
 something with them.

27
00:01:26,320 --> 00:01:28,240
 Start telling them all their faults.

28
00:01:28,240 --> 00:01:32,320
 We're talking all this talk about criticism.

29
00:01:32,320 --> 00:01:34,730
 Tomorrow's the one day of the year where it's open season

30
00:01:34,730 --> 00:01:35,480
 on everybody.

31
00:01:35,480 --> 00:01:38,300
 You're supposed to be allowed to criticize each other

32
00:01:38,300 --> 00:01:40,080
 because you invite each other to

33
00:01:40,080 --> 00:01:41,080
 criticize.

34
00:01:41,080 --> 00:01:43,440
 But no, they're good guys.

35
00:01:43,440 --> 00:01:49,480
 There's nothing major to criticize.

36
00:01:49,480 --> 00:01:51,240
 So we'll take some questions.

37
00:01:51,240 --> 00:01:53,240
 I don't think there's that many.

38
00:01:53,240 --> 00:01:56,040
 We've got a few here.

39
00:01:56,040 --> 00:02:05,280
 I have a question on where you ask forgiveness.

40
00:02:05,280 --> 00:02:10,160
 Is it actually what your purpose is or is it more important

41
00:02:10,160 --> 00:02:12,680
 for the monk to criticize?

42
00:02:12,680 --> 00:02:18,040
 We've got a lot of reasons.

43
00:02:18,040 --> 00:02:21,640
 You're using open speakers as well?

44
00:02:21,640 --> 00:02:25,180
 I didn't really catch what you said, but I need to have it

45
00:02:25,180 --> 00:02:26,880
 on because I need to record

46
00:02:26,880 --> 00:02:28,880
 what you're asking.

47
00:02:28,880 --> 00:02:31,240
 Let me turn mine down.

48
00:02:31,240 --> 00:02:37,460
 I'm just wondering what the intention is when you're asking

49
00:02:37,460 --> 00:02:39,880
 for when you're asking for

50
00:02:39,880 --> 00:02:47,680
 forgiveness and what is the intention?

51
00:02:47,680 --> 00:02:51,830
 To end the reigns on a good note so that there's no hard

52
00:02:51,830 --> 00:02:56,000
 feelings and no pent up resentment.

53
00:02:56,000 --> 00:03:03,360
 It's to be accountable to each other and to clear the air.

54
00:03:03,360 --> 00:03:05,080
 Because you've spent three months together.

55
00:03:05,080 --> 00:03:11,400
 Theoretically, I actually haven't.

56
00:03:11,400 --> 00:03:14,770
 Having spent three months together, usually a lot of stuff

57
00:03:14,770 --> 00:03:15,640
 can happen.

58
00:03:15,640 --> 00:03:20,440
 You can build up animosity.

59
00:03:20,440 --> 00:03:25,400
 So in the end, you invite each other to clear the air.

60
00:03:25,400 --> 00:03:28,200
 You accept the problems other people have had.

61
00:03:28,200 --> 00:03:32,200
 You listen and you accept and let it go.

62
00:03:32,200 --> 00:03:36,920
 It seems like a really good idea.

63
00:03:36,920 --> 00:03:44,200
 We do have some questions.

64
00:03:44,200 --> 00:03:45,200
 I only have you.

65
00:03:45,200 --> 00:03:46,200
 I have an Apple iPad.

66
00:03:46,200 --> 00:03:47,200
 But I do have Chrome.

67
00:03:47,200 --> 00:03:48,200
 Chrome.

68
00:03:48,200 --> 00:03:52,040
 Would I be able to use this?

69
00:03:52,040 --> 00:03:55,080
 Would I be able to use this?

70
00:03:55,080 --> 00:04:00,080
 Would I also need you?

71
00:04:00,080 --> 00:04:05,800
 I think you can get hangouts for iOS.

72
00:04:05,800 --> 00:04:07,200
 You don't use Chrome.

73
00:04:07,200 --> 00:04:08,800
 You'll use Hangouts.

74
00:04:08,800 --> 00:04:09,800
 Google Hangouts.

75
00:04:09,800 --> 00:04:15,700
 As long as they have that for Chrome, which I think there

76
00:04:15,700 --> 00:04:16,400
 is.

77
00:04:16,400 --> 00:04:25,640
 Oh, Nikki posted.

78
00:04:25,640 --> 00:04:32,640
 But I did talk to her, so that worked out fine.

79
00:04:32,640 --> 00:04:33,640
 Hello, Bunty.

80
00:04:33,640 --> 00:04:41,640
 Is it normal?

81
00:04:41,640 --> 00:04:53,400
 It's not normal.

82
00:04:53,400 --> 00:04:56,240
 We're not concerned so much about normal.

83
00:04:56,240 --> 00:04:57,240
 It's real.

84
00:04:57,240 --> 00:04:59,600
 And that's all that's important.

85
00:04:59,600 --> 00:05:00,600
 So it's a sound.

86
00:05:00,600 --> 00:05:02,600
 And you should say hearing, hearing.

87
00:05:02,600 --> 00:05:04,240
 It's not really a normal.

88
00:05:04,240 --> 00:05:07,440
 What would that mean?

89
00:05:07,440 --> 00:05:09,280
 Everyone gets it.

90
00:05:09,280 --> 00:05:12,960
 Everyone has different conditions.

91
00:05:12,960 --> 00:05:14,680
 So everyone will ask that sort of question.

92
00:05:14,680 --> 00:05:15,680
 Is this normal?

93
00:05:15,680 --> 00:05:17,000
 There really is no normal.

94
00:05:17,000 --> 00:05:19,000
 There's you and there's what you're experiencing.

95
00:05:19,000 --> 00:05:22,760
 So it doesn't matter whether it's normal.

96
00:05:22,760 --> 00:05:27,720
 A normal, abnormal is just a judgment call.

97
00:05:27,720 --> 00:05:31,320
 When you hear, you should say hearing, hearing.

98
00:05:31,320 --> 00:05:33,560
 Just let it go.

99
00:05:33,560 --> 00:05:34,560
 Knowledge it for some time.

100
00:05:34,560 --> 00:05:36,520
 If it doesn't go away, then just ignore it.

101
00:05:36,520 --> 00:05:39,080
 And then if it drags your attention back later, you can

102
00:05:39,080 --> 00:05:43,200
 always go back and acknowledge it

103
00:05:43,200 --> 00:05:45,320
 again.

104
00:05:45,320 --> 00:05:48,360
 If you dislike it or you like it or whatever, worried about

105
00:05:48,360 --> 00:05:50,520
 it, afraid of it, should acknowledge

106
00:05:50,520 --> 00:05:56,480
 all of that as well.

107
00:05:56,480 --> 00:06:01,780
 Could the monks who live in the forest or the animals

108
00:06:01,780 --> 00:06:02,960
 attack?

109
00:06:02,960 --> 00:06:05,320
 Yeah.

110
00:06:05,320 --> 00:06:10,520
 Pretty obvious, isn't it?

111
00:06:10,520 --> 00:06:14,200
 I guess is there more to that?

112
00:06:14,200 --> 00:06:18,200
 Like because a monk, sense that monks, their state of mind

113
00:06:18,200 --> 00:06:20,280
 is more calm, so they're less

114
00:06:20,280 --> 00:06:24,760
 liable to be attacked by animals, but it certainly happens.

115
00:06:24,760 --> 00:06:34,310
 Crushed by an elephant, gored by a wild pig, eaten by a

116
00:06:34,310 --> 00:06:41,080
 lion or a tiger, killed by mosquitoes.

117
00:06:41,080 --> 00:06:46,600
 Yeah, a lot of death by mosquitoes.

118
00:06:46,600 --> 00:06:49,040
 Does that happen to the forest monks?

119
00:06:49,040 --> 00:06:54,470
 There aren't that many forest monks, but there's also not

120
00:06:54,470 --> 00:06:57,800
 that many wild animals anymore.

121
00:06:57,800 --> 00:07:01,860
 So depends which country Sri Lanka still has some, but

122
00:07:01,860 --> 00:07:04,120
 snakes would probably, number one

123
00:07:04,120 --> 00:07:06,120
 would probably be mosquitoes.

124
00:07:06,120 --> 00:07:09,620
 I don't know about death through mosquitoes, but yeah,

125
00:07:09,620 --> 00:07:12,200
 there's probably death by mosquitoes,

126
00:07:12,200 --> 00:07:17,580
 death by snake bite, and death by disease, probably the

127
00:07:17,580 --> 00:07:18,520
 worst.

128
00:07:18,520 --> 00:07:20,840
 There's a lot of bacteria in the rainforests.

129
00:07:20,840 --> 00:07:26,080
 Maybe not the most, but it is up there, like infection and

130
00:07:26,080 --> 00:07:29,160
 fungus, just really awful stuff

131
00:07:29,160 --> 00:07:32,160
 in the rainforest.

132
00:07:32,160 --> 00:07:38,500
 In this area we have ticks that are part of Lyme disease,

133
00:07:38,500 --> 00:07:42,080
 originally in this area, Lyme

134
00:07:42,080 --> 00:07:43,080
 Connecticut.

135
00:07:43,080 --> 00:07:44,080
 Oh, I see.

136
00:07:44,080 --> 00:07:51,070
 Bante eti eti, and second life, and I've been spreading the

137
00:07:51,070 --> 00:07:53,000
 word about our group and study

138
00:07:53,000 --> 00:07:55,000
 group.

139
00:07:55,000 --> 00:07:58,210
 Since they go to the village, it would be a good thing to

140
00:07:58,210 --> 00:07:59,280
 let them know.

141
00:07:59,280 --> 00:08:02,440
 Is this a group of members?

142
00:08:02,440 --> 00:08:05,920
 Do not adhere to us.

143
00:08:05,920 --> 00:08:10,840
 Also would it be able to stream the live stream so they can

144
00:08:10,840 --> 00:08:12,480
 listen to them?

145
00:08:12,480 --> 00:08:13,480
 Thank you.

146
00:08:13,480 --> 00:08:20,760
 I don't really think we want people from other traditions

147
00:08:20,760 --> 00:08:24,320
 coming to meditate here.

148
00:08:24,320 --> 00:08:27,880
 It really should be our tradition.

149
00:08:27,880 --> 00:08:29,880
 That's my feeling.

150
00:08:29,880 --> 00:08:34,890
 I think I'd rather stick to our tradition, but if they want

151
00:08:34,890 --> 00:08:37,680
 to listen to the live stream,

152
00:08:37,680 --> 00:08:43,680
 that's absolutely welcome to.

153
00:08:43,680 --> 00:08:46,130
 We haven't required it, but I think we'd encourage that if

154
00:08:46,130 --> 00:08:48,000
 you're coming here, you're practicing

155
00:08:48,000 --> 00:08:49,000
 our tradition.

156
00:08:49,000 --> 00:08:56,000
 That seems, I don't see a reason to go outside of that.

157
00:08:56,000 --> 00:09:01,780
 Because it will just get weird otherwise, especially in the

158
00:09:01,780 --> 00:09:03,000
 chat box.

159
00:09:03,000 --> 00:09:12,320
 Yeah, you'd have to be the students who are watching.

160
00:09:12,320 --> 00:09:16,560
 The great students.

161
00:09:16,560 --> 00:09:21,280
 Mixed traditions, weird stuff happens.

162
00:09:21,280 --> 00:09:26,880
 Can you talk about it?

163
00:09:26,880 --> 00:09:31,040
 Sorry, I gotta go.

164
00:09:31,040 --> 00:09:35,200
 I just came on to say hello tonight.

165
00:09:35,200 --> 00:09:36,200
 Thank you, Van.

166
00:09:36,200 --> 00:09:37,200
 Thank you, Van.

167
00:09:37,200 --> 00:09:40,280
 Sorry guys, but thanks for showing up.

168
00:09:40,280 --> 00:09:42,360
 Thanks for meditating with us.

169
00:09:42,360 --> 00:09:43,840
 Keep up the good work.

170
00:09:43,840 --> 00:09:46,640
 Darwin, I'll try tomorrow.

171
00:09:46,640 --> 00:09:50,760
 Maybe tomorrow you have a session, right?

172
00:09:50,760 --> 00:09:55,480
 What time is that?

173
00:09:55,480 --> 00:10:01,880
 Everyone go to Darwin's session in Second Life.

174
00:10:01,880 --> 00:10:11,360
 If you can post it in the chat there, let us know.

175
00:10:11,360 --> 00:10:12,360
 I'll try to make it.

176
00:10:12,360 --> 00:10:13,360
 Okay, anyway, good night everyone.

177
00:10:13,360 --> 00:10:14,360
 Good night, Van.

178
00:10:14,360 --> 00:10:15,360
 Good night.

179
00:10:15,360 --> 00:10:21,080
 Thank you.

