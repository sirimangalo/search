1
00:00:00,000 --> 00:00:06,470
 All right, good evening everyone. Welcome to our evening D

2
00:00:06,470 --> 00:00:08,500
hamma session.

3
00:00:08,500 --> 00:00:27,480
 Both. All together we have our meditators here in Hamilton.

4
00:00:29,480 --> 00:00:33,010
 Doing their best to struggle through the difficulty of

5
00:00:33,010 --> 00:00:39,480
 intensive meditation practice, very much to be commended.

6
00:00:39,480 --> 00:27:49,340
 And we have the group of attendees in Second Life who are

7
00:27:49,340 --> 00:00:28,000
 brave in another way, struggling through the trials of

8
00:00:28,000 --> 00:00:56,480
 getting virtual reality up and running.

9
00:00:58,480 --> 00:01:06,840
 And taking the time every day, some of you, to come here

10
00:01:06,840 --> 00:01:12,480
 and to listen to the Dhamma.

11
00:01:12,480 --> 00:01:19,430
 Then we have the audio, the live audio if anyone's

12
00:01:19,430 --> 00:01:21,480
 listening.

13
00:01:22,480 --> 00:01:27,040
 The audio is easy because you can listen on your phone or

14
00:01:27,040 --> 00:01:30,480
 quite low tech, relatively low tech.

15
00:01:30,480 --> 00:01:34,530
 You just need the URL and you can listen into the live

16
00:01:34,530 --> 00:01:38,480
 stream or you can listen to the mp3 later on.

17
00:01:38,480 --> 00:01:43,480
 And finally YouTube of course, which is the easiest of all.

18
00:01:44,480 --> 00:01:48,520
 People just find these videos I think through a search or

19
00:01:48,520 --> 00:01:52,500
 through a related video or people following my YouTube

20
00:01:52,500 --> 00:01:53,480
 channel.

21
00:01:53,480 --> 00:01:57,480
 So we have a large audience, which is always nice.

22
00:01:57,480 --> 00:02:04,480
 Thank you all for tuning in and for watching these videos.

23
00:02:05,480 --> 00:02:12,680
 Tonight, tonight I wanted to talk to you all about right

24
00:02:12,680 --> 00:02:14,480
 and wrong.

25
00:02:14,480 --> 00:02:22,480
 This question of what is right and what is wrong.

26
00:02:22,480 --> 00:02:27,010
 It's a big question in our meditation. It's really what we

27
00:02:27,010 --> 00:02:31,480
 claim to be able to figure out through the practice.

28
00:02:32,480 --> 00:02:38,210
 The first question we have to ask is not what is right and

29
00:02:38,210 --> 00:02:40,480
 what is wrong, but is there right and wrong?

30
00:02:40,480 --> 00:02:44,550
 What does it mean to say something is right and something

31
00:02:44,550 --> 00:02:45,480
 is wrong?

32
00:02:45,480 --> 00:02:51,920
 A lot of people will have you believe that there is no

33
00:02:51,920 --> 00:02:56,360
 right and wrong or more commonly will say that it's

34
00:02:56,360 --> 00:02:57,480
 relative.

35
00:02:57,480 --> 00:03:01,480
 There is no absolute right and wrong.

36
00:03:01,480 --> 00:03:14,660
 So there are two ways we can understand that there exists

37
00:03:14,660 --> 00:03:18,480
 right and wrong.

38
00:03:19,480 --> 00:03:26,340
 And from a purely materialistic or sign physicalist point

39
00:03:26,340 --> 00:03:31,500
 of view, sure there is no right and wrong, but that really

40
00:03:31,500 --> 00:03:37,000
 points more to the problem with the physicalist outlook on

41
00:03:37,000 --> 00:03:42,480
 life or more commonly understand and impersonal.

42
00:03:42,480 --> 00:04:01,570
 A non-first person or a third person impersonal point of

43
00:04:01,570 --> 00:04:04,480
 view.

44
00:04:05,480 --> 00:04:09,350
 The real problem is that it's an abstraction and that's

45
00:04:09,350 --> 00:04:13,480
 what you find curious about, that is curious thing about

46
00:04:13,480 --> 00:04:21,480
 the concept of there being no right or wrong.

47
00:04:22,480 --> 00:04:27,510
 So the idea goes that atoms of course have no right or

48
00:04:27,510 --> 00:04:32,480
 wrong state and rocks don't know right or wrong.

49
00:04:32,480 --> 00:04:40,180
 And going by that, building the human beings up from these

50
00:04:40,180 --> 00:04:45,480
 entities, atoms and molecules and cells.

51
00:04:48,480 --> 00:04:51,790
 It's hard to say that there could ever be any right or

52
00:04:51,790 --> 00:04:52,480
 wrong.

53
00:04:52,480 --> 00:04:54,940
 Looking at it from that point of view, a superficial

54
00:04:54,940 --> 00:04:57,650
 understanding of Buddhism might have you seeing things in

55
00:04:57,650 --> 00:04:58,480
 the same way.

56
00:04:58,480 --> 00:05:02,250
 If you look at the Abhidhamma, you just see a bunch of mind

57
00:05:02,250 --> 00:05:05,650
 states, body states, it seems very much, very similar to

58
00:05:05,650 --> 00:05:09,480
 the physicalist outlook. It's just giving a description.

59
00:05:10,480 --> 00:05:12,130
 But the Buddha was quite clear that there is right and

60
00:05:12,130 --> 00:05:16,970
 wrong. He was quite often, or he's portrayed in the texts

61
00:05:16,970 --> 00:05:22,480
 as describing what is right and what is wrong quite often.

62
00:05:25,480 --> 00:05:29,170
 So the first way we can understand right and wrong is, I

63
00:05:29,170 --> 00:05:33,180
 think, very much still in line with this physicalist point

64
00:05:33,180 --> 00:05:37,010
 of view or the third person impersonal concept of there

65
00:05:37,010 --> 00:05:40,480
 being nothing intrinsically right and wrong.

66
00:05:40,480 --> 00:05:44,120
 There still is right and wrong and we shouldn't gloss over

67
00:05:44,120 --> 00:05:45,480
 this or miss this.

68
00:05:48,480 --> 00:05:56,210
 And that's the right and wrong of claims. So if you say God

69
00:05:56,210 --> 00:06:03,930
 created the universe, it may not be investigatable, but it

70
00:06:03,930 --> 00:06:07,660
 certainly is either true or false, or true and false, or

71
00:06:07,660 --> 00:06:11,270
 neither true nor false, but much more rarely those last two

72
00:06:11,270 --> 00:06:11,480
.

73
00:06:12,480 --> 00:06:17,280
 Most claims are usually either true or false. Sometimes

74
00:06:17,280 --> 00:06:21,320
 they're more complicated and a claim can't actually be

75
00:06:21,320 --> 00:06:27,390
 discerned. But there are some pretty simple claims that are

76
00:06:27,390 --> 00:06:33,410
 pretty easily categorized claims as being either right or

77
00:06:33,410 --> 00:06:34,480
 wrong.

78
00:06:36,480 --> 00:06:41,900
 Another way of looking this is true or false. But if a

79
00:06:41,900 --> 00:06:45,410
 claim is true, then it's also right and that's an important

80
00:06:45,410 --> 00:06:46,480
 distinction.

81
00:06:47,480 --> 00:06:50,490
 For example, if you say that craving is the cause of

82
00:06:50,490 --> 00:06:53,730
 suffering or more practically, desire will lead me to

83
00:06:53,730 --> 00:07:06,350
 suffer. Desire leads one to suffer. That's right, according

84
00:07:06,350 --> 00:07:09,480
 to Buddhism. Some people might say that's wrong.

85
00:07:13,480 --> 00:07:18,300
 But these are important because they define how we look at

86
00:07:18,300 --> 00:07:22,880
 the world. They clarify our position, our understanding of

87
00:07:22,880 --> 00:07:24,480
 how the world works.

88
00:07:24,480 --> 00:07:27,840
 This important part of the meditation practice, as I was

89
00:07:27,840 --> 00:07:32,590
 talking about yesterday, what is reality? Reality is body

90
00:07:32,590 --> 00:07:34,480
 and mind. Well, that's a claim.

91
00:07:35,480 --> 00:07:39,510
 Some people would claim that reality is made up of atoms

92
00:07:39,510 --> 00:07:43,720
 and subatomic particles and those are made up of who knows

93
00:07:43,720 --> 00:07:44,480
 what.

94
00:07:44,480 --> 00:07:49,040
 So we would say that our claim is right and the claim that

95
00:07:49,040 --> 00:07:53,890
 reality is made up of atoms or subatomic particles is wrong

96
00:07:53,890 --> 00:07:55,480
, we would say.

97
00:07:56,480 --> 00:07:59,620
 Of course, then it gets complicated. We wouldn't be quite

98
00:07:59,620 --> 00:08:02,610
 so cut and dried, we would say. It really depends how you

99
00:08:02,610 --> 00:08:09,810
 look at it. But regardless, there's something more right

100
00:08:09,810 --> 00:08:14,470
 about saying that reality is made up of experience because

101
00:08:14,470 --> 00:08:17,480
 experience is something that can be known.

102
00:08:18,480 --> 00:08:21,900
 You might say, well, maybe there exist things that we can't

103
00:08:21,900 --> 00:08:26,560
 know. And so the argument is that the very fact that we can

104
00:08:26,560 --> 00:08:31,430
't, cannot possibly, never ever will be able to know these

105
00:08:31,430 --> 00:08:32,480
 things.

106
00:08:33,480 --> 00:08:39,210
 It's by definition impossible to know these things. It

107
00:08:39,210 --> 00:08:44,490
 renders them less real and therefore less right as a means

108
00:08:44,490 --> 00:08:47,480
 of understanding reality.

109
00:08:47,480 --> 00:08:50,980
 You see where I'm going with this? We're not yet talking

110
00:08:50,980 --> 00:08:54,330
 about the quality of things, we're talking about claims or

111
00:08:54,330 --> 00:08:56,480
 the way we understand reality.

112
00:08:57,480 --> 00:09:01,520
 We could argue that there is a right and a wrong way to

113
00:09:01,520 --> 00:09:07,480
 understand reality. Because if you, to put a point on it,

114
00:09:07,480 --> 00:09:14,370
 if you understand reality in a certain way, the results are

115
00:09:14,370 --> 00:09:19,380
 more or less likely to be in accordance with that

116
00:09:19,380 --> 00:09:21,480
 understanding.

117
00:09:22,480 --> 00:09:25,630
 So if your understanding is that craving will lead you to

118
00:09:25,630 --> 00:09:29,190
 happiness, clinging to things, desiring things, will lead

119
00:09:29,190 --> 00:09:33,670
 you to happiness. Well, when after some time it leads you

120
00:09:33,670 --> 00:09:38,130
 only to suffering, you'll have to say, well, that was wrong

121
00:09:38,130 --> 00:09:38,480
.

122
00:09:38,480 --> 00:09:43,530
 I was wrong to think that. Regardless of whether you say it

123
00:09:43,530 --> 00:09:45,480
's right or wrong to suffer.

124
00:09:46,480 --> 00:09:49,010
 And you were wrong because your claim turned out to be

125
00:09:49,010 --> 00:09:52,460
 false, your understanding, your outlook. This is a very

126
00:09:52,460 --> 00:09:54,480
 important part of Buddhism.

127
00:09:54,480 --> 00:09:59,090
 For example, looking at the three characteristics. When we

128
00:09:59,090 --> 00:10:04,440
 talk about impermanent suffering and non-self, what we mean

129
00:10:04,440 --> 00:10:07,980
 is that we have a wrong understanding of reality as being

130
00:10:07,980 --> 00:10:12,480
 stable, satisfying and controllable and belonging to us.

131
00:10:13,480 --> 00:10:18,030
 We have to be proper to be clung to or having some

132
00:10:18,030 --> 00:10:21,480
 intrinsic nature of selfness.

133
00:10:21,480 --> 00:10:28,480
 And so as we meditate, we start to see this isn't true.

134
00:10:29,480 --> 00:10:34,070
 We can see that our mind is chaotic, that the body is

135
00:10:34,070 --> 00:10:39,630
 chaotic, that nothing is ever predictable, that things are

136
00:10:39,630 --> 00:10:43,480
 not stable or predictable or constant.

137
00:10:43,480 --> 00:10:47,360
 And realizing that the way we look at the world and the way

138
00:10:47,360 --> 00:10:51,370
 we expect things to be predictable and stable is causing us

139
00:10:51,370 --> 00:10:52,480
 suffering.

140
00:10:53,480 --> 00:10:56,500
 We're saying that we were wrong. It causes us suffering

141
00:10:56,500 --> 00:11:01,380
 because we were simply wrong about what we thought about

142
00:11:01,380 --> 00:11:02,480
 reality.

143
00:11:02,480 --> 00:11:05,790
 We thought it was like this and it's not like that. Well,

144
00:11:05,790 --> 00:11:07,480
 that causes stress and suffering.

145
00:11:07,480 --> 00:11:10,560
 Not yet saying whether stress and suffering is wrong,

146
00:11:10,560 --> 00:11:13,480
 though we probably want to say that eventually.

147
00:11:13,480 --> 00:11:16,570
 We're just saying we were wrong. We thought that it was

148
00:11:16,570 --> 00:11:19,480
 permanent and it wasn't permanent. We thought it was stable

149
00:11:19,480 --> 00:11:20,480
 and it was unstable.

150
00:11:22,480 --> 00:11:23,930
 And we think things are satisfying and they turn out to be

151
00:11:23,930 --> 00:11:27,690
 unsatisfying. We think we can find happiness by clinging to

152
00:11:27,690 --> 00:11:28,480
 things.

153
00:11:28,480 --> 00:11:32,900
 This will make me happy. If I get this, if I cling to it,

154
00:11:32,900 --> 00:11:37,480
 if I build up this, then I'll be happy. We're wrong.

155
00:11:40,480 --> 00:11:45,540
 And finally, self. We try to control things. We possess

156
00:11:45,540 --> 00:11:50,480
 things. We identify with things. This is me, this is mine.

157
00:11:50,480 --> 00:11:56,540
 And so we have the idea that things are controllable,

158
00:11:56,540 --> 00:12:01,800
 things are me and mine. We start to realize, well, it's not

159
00:12:01,800 --> 00:12:05,480
 really reasonable to suggest that these are me or mine.

160
00:12:06,480 --> 00:12:10,130
 And we suffer as a result, but we suffer. And we suffer

161
00:12:10,130 --> 00:12:12,480
 because they go against our understanding.

162
00:12:12,480 --> 00:12:15,440
 When our understanding is that we can control things and

163
00:12:15,440 --> 00:12:18,760
 then they aren't under our control, we realize we were

164
00:12:18,760 --> 00:12:19,480
 wrong.

165
00:12:19,480 --> 00:12:24,790
 We were wrong. They're not under our control. We thought

166
00:12:24,790 --> 00:12:28,500
 they belonged to us. We realized, well, you could conceive

167
00:12:28,500 --> 00:12:32,400
 of them as being yours. That's not wrong. Yes, I conceive

168
00:12:32,400 --> 00:12:33,480
 of it as being so.

169
00:12:35,480 --> 00:12:36,990
 But you could still say it's wrong because there's not

170
00:12:36,990 --> 00:12:40,360
 really much reason. At the best you might say it's

171
00:12:40,360 --> 00:12:44,480
 unreasonable to suggest that these things are me or mine.

172
00:12:44,480 --> 00:12:49,560
 Because they come and they go in their own accord, causes

173
00:12:49,560 --> 00:12:54,740
 and conditions, having nothing to do with me or mine in any

174
00:12:54,740 --> 00:12:55,480
 way.

175
00:12:55,480 --> 00:12:59,620
 So we have to conclude, I think, that it's wrong to say

176
00:12:59,620 --> 00:13:01,480
 these are me and mine.

177
00:13:01,480 --> 00:13:08,950
 It's a very important part of Buddhism. You see, because

178
00:13:08,950 --> 00:13:12,990
 modern times it's all about things being relative. If it's

179
00:13:12,990 --> 00:13:16,810
 right for me or it's right for you, just because you think

180
00:13:16,810 --> 00:13:20,650
 it's wrong, doesn't mean it's wrong for me, which is so

181
00:13:20,650 --> 00:13:22,480
 problematic.

182
00:13:23,480 --> 00:13:26,040
 As you can see, hopefully, from what I've been saying, my

183
00:13:26,040 --> 00:13:28,530
 claim is that there are very clearly things that are wrong

184
00:13:28,530 --> 00:13:31,480
 for everybody. They're wrong because they're wrong.

185
00:13:31,480 --> 00:13:40,740
 Wrong understanding, wrong logical assumption, logical

186
00:13:40,740 --> 00:13:43,480
 conclusion.

187
00:13:48,480 --> 00:13:51,200
 And then in general we'd have to agree that there is wrong.

188
00:13:51,200 --> 00:13:55,470
 And then there is right. It's right to say that everything

189
00:13:55,470 --> 00:14:00,080
 that arises will cease. Nothing that arises is permanent or

190
00:14:00,080 --> 00:14:04,480
 stable or satisfying or controllable and belonging to us.

191
00:14:06,480 --> 00:14:11,840
 But then we get into the interesting, more interesting and

192
00:14:11,840 --> 00:14:16,890
 more difficult, I think, this is the more contentious

193
00:14:16,890 --> 00:14:22,560
 question of whether something, some action or some thought,

194
00:14:22,560 --> 00:14:24,480
 some mind state.

195
00:14:25,480 --> 00:14:29,040
 We're going to stick mostly to mind states, I suppose, with

196
00:14:29,040 --> 00:14:34,690
 Buddhism. It can be right or wrong. Is it wrong to kill? Is

197
00:14:34,690 --> 00:14:39,650
 it wrong to steal? Is anger wrong? Is greed wrong? These

198
00:14:39,650 --> 00:14:40,480
 are different questions.

199
00:14:40,480 --> 00:14:43,570
 This is a different question from whether saying that our

200
00:14:43,570 --> 00:14:46,450
 understanding of the world is right or wrong, though they

201
00:14:46,450 --> 00:14:47,480
 have to do with that.

202
00:14:48,480 --> 00:14:51,290
 We kill and steal with the understanding that it's going to

203
00:14:51,290 --> 00:14:55,960
 bring us happiness. It doesn't. It's going to eliminate

204
00:14:55,960 --> 00:14:57,480
 suffering. It doesn't.

205
00:14:57,480 --> 00:15:03,480
 But there's the question, is it right or wrong to suffer?

206
00:15:05,480 --> 00:15:10,010
 And I think the real problem here is that we've cornered

207
00:15:10,010 --> 00:15:14,200
 ourselves or we've shot ourselves in the foot, I suppose

208
00:15:14,200 --> 00:15:20,190
 you could say, with our scientific, our impersonal

209
00:15:20,190 --> 00:15:23,480
 conception of reality.

210
00:15:24,480 --> 00:15:29,250
 It's curious, really, because logically you want to think,

211
00:15:29,250 --> 00:15:33,570
 well, yes, a rock doesn't know right or wrong, and so if

212
00:15:33,570 --> 00:15:37,740
 reality is based on all this, then there is no right and

213
00:15:37,740 --> 00:15:38,480
 wrong.

214
00:15:39,480 --> 00:15:43,180
 That's what's curious, is that right and wrong, in this

215
00:15:43,180 --> 00:15:47,480
 sense, only have anything to do with experiential reality.

216
00:15:47,480 --> 00:15:51,920
 And the concept that there is no right or wrong has only to

217
00:15:51,920 --> 00:15:54,480
 do with conceptual reality.

218
00:15:57,480 --> 00:16:01,450
 And it's important to see this distinction, to see that

219
00:16:01,450 --> 00:16:05,850
 experience very much has right or wrong, because experience

220
00:16:05,850 --> 00:16:10,160
 is very much, I would say, intrinsically tied to happiness

221
00:16:10,160 --> 00:16:11,480
 or suffering.

222
00:16:11,480 --> 00:16:16,870
 Intrinsically tied to getting what you want, not getting

223
00:16:16,870 --> 00:16:18,480
 what you want.

224
00:16:18,480 --> 00:16:25,480
 It's intrinsically tied to right or wrong.

225
00:16:26,480 --> 00:16:29,480
 There's an intrinsic right or wrong in experience.

226
00:16:29,480 --> 00:16:32,790
 So the reason why it's difficult for us to come to this

227
00:16:32,790 --> 00:16:36,480
 conclusion that there is right or wrong is because we think

228
00:16:36,480 --> 00:16:38,480
 of reality as impermanent.

229
00:16:44,480 --> 00:16:47,710
 That's the most egregious form of saying there is no right

230
00:16:47,710 --> 00:16:52,120
 or wrong. I think it's egregious in the sense that it mixes

231
00:16:52,120 --> 00:16:57,280
 up reality and abstraction, because any concept we might

232
00:16:57,280 --> 00:17:04,970
 have of rocks and atoms as conceptual as abstract, it's not

233
00:17:04,970 --> 00:17:08,480
 based, it's not really experienced.

234
00:17:10,480 --> 00:17:15,660
 Our experience is very much caught up with good and bad and

235
00:17:15,660 --> 00:17:17,480
 right and wrong.

236
00:17:17,480 --> 00:17:22,100
 Another way that it's denied that there is any right and

237
00:17:22,100 --> 00:17:26,480
 wrong is to say, "Right for me, right for you."

238
00:17:26,480 --> 00:17:32,950
 And that's fine. I think that's important because there are

239
00:17:32,950 --> 00:17:39,480
 many things that are not right or wrong.

240
00:17:40,480 --> 00:17:46,240
 Like we can say, "Lighting incense or candles in this way

241
00:17:46,240 --> 00:17:50,480
 or that way, this way is right."

242
00:17:50,480 --> 00:17:55,380
 So if you look at religions, most people look at religions

243
00:17:55,380 --> 00:18:00,380
 in terms of the outward expressions of lighting candles and

244
00:18:00,380 --> 00:18:05,180
 incense, bowing down and praying and thinking about God

245
00:18:05,180 --> 00:18:06,480
 generally.

246
00:18:07,480 --> 00:18:13,230
 We could even argue that Buddhism has a God. Buddhism has a

247
00:18:13,230 --> 00:18:19,040
 God in the sense of karma, in the sense of samsara. Samsara

248
00:18:19,040 --> 00:18:20,480
 is sort of our God.

249
00:18:21,480 --> 00:18:25,050
 Bear with me. I know Buddhism is atheistic. We don't, of

250
00:18:25,050 --> 00:18:28,870
 course, pray. In fact, get me away from this thing. But

251
00:18:28,870 --> 00:18:30,480
 that's God.

252
00:18:30,480 --> 00:18:36,930
 It's God because it's the system, it's the all, it's the

253
00:18:36,930 --> 00:18:38,480
 entirety.

254
00:18:39,480 --> 00:18:43,400
 And so my point being here is that all religions relate

255
00:18:43,400 --> 00:18:47,820
 somehow to this concept. And people look at this and want

256
00:18:47,820 --> 00:18:52,800
 to say, "Oh, well, the differences are merely cosmetic and

257
00:18:52,800 --> 00:18:54,480
 to each their own.

258
00:18:54,480 --> 00:19:01,160
 This is right for me and this is right for you." And that's

259
00:19:01,160 --> 00:19:05,350
 fine. Lighting candles this way, lighting candles that way,

260
00:19:05,350 --> 00:19:07,480
 speaking this way, speaking that way.

261
00:19:09,480 --> 00:19:12,570
 It's all superficial. There's no right or wrong involved

262
00:19:12,570 --> 00:19:13,480
 there.

263
00:19:13,480 --> 00:19:19,340
 But we go further than that in Buddhism. And this is the

264
00:19:19,340 --> 00:19:25,090
 idea of praying to God in the first place or revering God

265
00:19:25,090 --> 00:19:27,480
 in the first place.

266
00:19:28,480 --> 00:19:32,720
 Buddhism will acknowledge, we could acknowledge, based on

267
00:19:32,720 --> 00:19:36,570
 some fairly broad, vague conception of what God is, we

268
00:19:36,570 --> 00:19:38,480
 could say, "Okay, yes, there's God."

269
00:19:38,480 --> 00:19:43,530
 Yes, you have the universe and you have laws and you have,

270
00:19:43,530 --> 00:19:47,480
 I mean, laws of nature in that sense.

271
00:19:48,480 --> 00:19:57,620
 You have an orderly sense of reality and it is real. It

272
00:19:57,620 --> 00:19:58,480
 does exist.

273
00:19:58,480 --> 00:20:05,810
 But it's not right to worship it in any way. We worship God

274
00:20:05,810 --> 00:20:07,480
 in so many ways.

275
00:20:08,480 --> 00:20:12,440
 There are the theists who worship the concept of God and

276
00:20:12,440 --> 00:20:16,550
 then there are the materialists and by here I mean like the

277
00:20:16,550 --> 00:20:19,480
 consumerists who worship pleasure.

278
00:20:19,480 --> 00:20:23,290
 We worship the body as being and the be-all end-all of

279
00:20:23,290 --> 00:20:27,710
 happiness. We worship sights and sounds and smells. We

280
00:20:27,710 --> 00:20:31,480
 worship money. We worship power.

281
00:20:34,480 --> 00:20:39,560
 And this is all wrong. It's wrong to do so. That's what we

282
00:20:39,560 --> 00:20:41,480
 would say in Buddhism.

283
00:20:41,480 --> 00:20:49,400
 We would argue for a sense of wrong here. It's wrong to do

284
00:20:49,400 --> 00:20:50,480
 so.

285
00:21:01,480 --> 00:21:05,380
 So the Buddhist sense of wrong, and of course everyone, yes

286
00:21:05,380 --> 00:21:08,970
, different people have their ideas of what is right and

287
00:21:08,970 --> 00:21:10,480
 wrong in this context.

288
00:21:10,480 --> 00:21:17,110
 But in Buddhism we have our own specific ideas of what is

289
00:21:17,110 --> 00:21:19,480
 right and wrong.

290
00:21:19,480 --> 00:21:24,180
 The ideas are based on that which brings you suffering is

291
00:21:24,180 --> 00:21:28,480
 wrong and that which brings happiness is right.

292
00:21:28,480 --> 00:21:37,440
 And so the argument here is not merely the idea that

293
00:21:37,440 --> 00:21:43,320
 certain things do bring you happiness or suffering but that

294
00:21:43,320 --> 00:21:44,480
 intrinsically,

295
00:21:44,480 --> 00:21:48,850
 reality, experiential reality intrinsically has something

296
00:21:48,850 --> 00:21:50,480
 to say on this topic.

297
00:21:50,480 --> 00:21:54,230
 That intrinsically there is something wrong about suffering

298
00:21:54,230 --> 00:21:55,480
. It's the very definition of wrong.

299
00:21:56,480 --> 00:21:59,410
 And you might disagree with this. This is something that I

300
00:21:59,410 --> 00:22:00,480
 suppose is controversial.

301
00:22:00,480 --> 00:22:04,470
 But I don't think it should be controversial. I think we

302
00:22:04,470 --> 00:22:07,480
 have this concept of suffering,

303
00:22:07,480 --> 00:22:13,050
 this category of experience, this category of reality, that

304
00:22:13,050 --> 00:22:19,190
 which is suffering. And I think that is wrong intrinsically

305
00:22:19,190 --> 00:22:19,480
.

306
00:22:20,480 --> 00:22:24,990
 I don't think there is any argument. There should be any

307
00:22:24,990 --> 00:22:26,480
 argument here.

308
00:22:26,480 --> 00:22:29,370
 I was talking to my friend about this and she was the one

309
00:22:29,370 --> 00:22:33,480
 who got me onto this and said there is no right and wrong.

310
00:22:33,480 --> 00:22:37,860
 And she was talking about letting her son suffer. Her son

311
00:22:37,860 --> 00:22:42,480
 was suffering and she let him. She didn't console him.

312
00:22:44,480 --> 00:22:47,910
 And so it's important to understand what we mean here. We

313
00:22:47,910 --> 00:22:52,340
're not just talking about simple suffering like pain for

314
00:22:52,340 --> 00:22:53,480
 example.

315
00:22:53,480 --> 00:23:00,930
 We're talking about that which betters a person, that which

316
00:23:00,930 --> 00:23:03,280
 leads a person to greater happiness, that which leads a

317
00:23:03,280 --> 00:23:04,480
 person to greater suffering.

318
00:23:05,480 --> 00:23:09,880
 That which is pure of mind and the sense of having

319
00:23:09,880 --> 00:23:15,490
 intentions to bring happiness, having intentions to bring

320
00:23:15,490 --> 00:23:16,480
 peace.

321
00:23:16,480 --> 00:23:24,540
 We would argue that these are right. No matter whether it's

322
00:23:24,540 --> 00:23:28,480
 through tough love, sometimes you have to let people suffer

323
00:23:28,480 --> 00:23:31,960
 in order to learn, in order to grow, in order to be free

324
00:23:31,960 --> 00:23:33,480
 from suffering.

325
00:23:35,480 --> 00:23:37,970
 But there's nothing intellectual about this. This is what

326
00:23:37,970 --> 00:23:40,480
 we start to see in meditation.

327
00:23:40,480 --> 00:23:43,980
 And this is how we can know that this is right, that there

328
00:23:43,980 --> 00:23:47,680
 really is a right and wrong. Because as you meditate, you

329
00:23:47,680 --> 00:23:50,480
 naturally, you can't avoid the truth.

330
00:23:51,480 --> 00:23:54,850
 You can't avoid the truth that suffering is wrong and

331
00:23:54,850 --> 00:23:59,480
 whatever causes suffering is wrong. It's not intellectual.

332
00:23:59,480 --> 00:24:02,480
 You just can't possibly convince yourself.

333
00:24:02,480 --> 00:24:06,920
 The only way you can convince yourself is by doing what we

334
00:24:06,920 --> 00:24:11,350
 do and running away from the truth or chasing our tails to

335
00:24:11,350 --> 00:24:14,480
 avoid the reality of the situation.

336
00:24:15,480 --> 00:24:18,860
 As you meditate and you start to see reality, you can't

337
00:24:18,860 --> 00:24:23,800
 avoid it and you can't force yourself. Find yourself unable

338
00:24:23,800 --> 00:24:29,480
 to force yourself to suffer, to cause suffering.

339
00:24:29,480 --> 00:24:35,480
 You start to realize that there is something wrong with me.

340
00:24:35,480 --> 00:24:38,860
 There's a lot of things wrong with me and they're truly

341
00:24:38,860 --> 00:24:39,480
 wrong.

342
00:24:40,480 --> 00:24:43,480
 And the great thing of course is that we can change them.

343
00:24:43,480 --> 00:24:47,630
 The power, the great power that we have is that we can

344
00:24:47,630 --> 00:24:51,480
 right these wrongs. Intrinsic wrong.

345
00:24:51,480 --> 00:24:57,090
 They're intrinsically wrong but they're not intrinsic to us

346
00:24:57,090 --> 00:24:59,480
. We can change.

347
00:25:00,480 --> 00:25:03,970
 I think another reason why people shy away from the idea of

348
00:25:03,970 --> 00:25:07,510
 their being right and wrong is because they don't have this

349
00:25:07,510 --> 00:25:09,480
 concept that we can change.

350
00:25:09,480 --> 00:25:13,160
 If you were to say something is wrong, we'd have to say a

351
00:25:13,160 --> 00:25:16,270
 person is wrong. You're wrong. There's something wrong with

352
00:25:16,270 --> 00:25:18,480
 you. Of course, no one wants to hear that.

353
00:25:18,480 --> 00:25:22,560
 And even to suggest that that's a good thing to tell people

354
00:25:22,560 --> 00:25:27,050
, right? If I tell you there's something wrong with you, you

355
00:25:27,050 --> 00:25:29,480
 might get very angry at me.

356
00:25:29,480 --> 00:25:32,560
 Or at least feel very sad and feel bad about yourself. My

357
00:25:32,560 --> 00:25:34,870
 teacher told me there's something wrong with me. There's

358
00:25:34,870 --> 00:25:37,620
 something wrong with all of us. There's lots of things

359
00:25:37,620 --> 00:25:38,480
 wrong with us.

360
00:25:38,480 --> 00:25:43,340
 But it's empowering to know that. Because if there's

361
00:25:43,340 --> 00:25:45,860
 something wrong with the engine, well you want to know

362
00:25:45,860 --> 00:25:49,570
 about it. It's empowering because you don't want to drive

363
00:25:49,570 --> 00:25:51,480
 the car without fixing it.

364
00:25:52,480 --> 00:25:56,390
 So what we're doing is we're making mechanics out of us all

365
00:25:56,390 --> 00:26:00,690
 to use this analogy again. And we're going to fix what's

366
00:26:00,690 --> 00:26:01,480
 wrong.

367
00:26:01,480 --> 00:26:08,490
 And this is why the Buddha taught the Eightfold Noble Path.

368
00:26:08,490 --> 00:26:12,480
 We call it the Noble Path but it could easily be called,

369
00:26:12,480 --> 00:26:14,480
 that's what it's called, it's the name the Buddha gave it.

370
00:26:15,480 --> 00:26:18,930
 Another name for it could be called the Right Path because

371
00:26:18,930 --> 00:26:23,480
 that's the word the Buddha uses, Samma. We have Right View.

372
00:26:23,480 --> 00:26:27,860
 Right View is important. It's important to not have Wrong

373
00:26:27,860 --> 00:26:30,480
 View, Michad, Titya, Wrong View.

374
00:26:30,480 --> 00:26:35,700
 Wrong View because it causes suffering. It's wrong. It's

375
00:26:35,700 --> 00:26:37,480
 not good for you. It's not right.

376
00:26:41,480 --> 00:26:47,140
 Wrong Thought. Wrong Speech. Wrong Action. Wrong Livelihood

377
00:26:47,140 --> 00:26:48,480
. They're all wrong, wrong, wrong.

378
00:26:48,480 --> 00:26:56,480
 Wrong Effort. Wrong Mindfulness. Wrong Concentration.

379
00:26:59,480 --> 00:27:03,730
 So we cultivate what is right. We don't have to ask. You

380
00:27:03,730 --> 00:27:07,610
 don't have to ask me what is right. The point here, the

381
00:27:07,610 --> 00:27:10,540
 point with all of this is not to be able to even discern

382
00:27:10,540 --> 00:27:13,810
 intellectually what is right and what is wrong. You have to

383
00:27:13,810 --> 00:27:14,480
 be able to decide.

384
00:27:15,480 --> 00:27:18,610
 It's to look and to see. And through looking and seeing you

385
00:27:18,610 --> 00:27:22,900
 will start to develop right view, right thought. That which

386
00:27:22,900 --> 00:27:29,620
 is good for you, that which is in line with reality and

387
00:27:29,620 --> 00:27:31,850
 that which leads to peace, happiness and freedom from

388
00:27:31,850 --> 00:27:35,480
 suffering. That's what's meant by right and wrong.

389
00:27:36,480 --> 00:27:40,910
 So there you go. That's the Dhamma for tonight. Thank you

390
00:27:40,910 --> 00:27:45,510
 all for tuning in. I wish you all the best. Hope you all

391
00:27:45,510 --> 00:27:47,480
 find the right path.

