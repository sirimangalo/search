1
00:00:00,000 --> 00:00:06,880
 Hi, so this is a video in response to someone's question

2
00:00:06,880 --> 00:00:10,840
 about death and funeral.

3
00:00:10,840 --> 00:00:16,640
 The question was in regards to one's Catholic family and

4
00:00:16,640 --> 00:00:21,560
 there's the concern that if one,

5
00:00:21,560 --> 00:00:24,840
 this person hasn't let their family know that they're

6
00:00:24,840 --> 00:00:26,960
 Buddhist and he's afraid that he or

7
00:00:26,960 --> 00:00:31,030
 she is afraid that if they don't let them know then the

8
00:00:31,030 --> 00:00:32,760
 family is going to give him a

9
00:00:32,760 --> 00:00:36,720
 Catholic funeral and they don't know how to let, they're

10
00:00:36,720 --> 00:00:38,480
 wondering how they can let

11
00:00:38,480 --> 00:00:42,820
 these, their family know that they're Buddhist and they

12
00:00:42,820 --> 00:00:44,760
 want to do it in a way that it's not

13
00:00:44,760 --> 00:00:50,000
 seen as boastful or bragging or just trying to be different

14
00:00:50,000 --> 00:00:51,040
 or so on.

15
00:00:51,040 --> 00:00:55,310
 First of all, I wanted to say and what I mentioned

16
00:00:55,310 --> 00:00:59,560
 initially in my response was that the most

17
00:00:59,560 --> 00:01:02,200
 important people at a funeral are not the dead people, they

18
00:01:02,200 --> 00:01:04,480
're the living people, the people

19
00:01:04,480 --> 00:01:06,940
 who are left behind because they're the ones who are really

20
00:01:06,940 --> 00:01:08,800
 suffering and are generally unable to

21
00:01:08,800 --> 00:01:12,060
 cope with the experience. The dead person's dead and it's

22
00:01:12,060 --> 00:01:14,520
 true that sometimes they hang around.

23
00:01:14,520 --> 00:01:18,900
 After a person dies they can hang around for several days

24
00:01:18,900 --> 00:01:21,720
 or it seems even years are possible.

25
00:01:21,720 --> 00:01:25,520
 But you know especially if you're Buddhist and if you're

26
00:01:25,520 --> 00:01:28,000
 practicing Buddhism it's more,

27
00:01:28,000 --> 00:01:31,040
 it should be a more concern or at least some concern as to

28
00:01:31,040 --> 00:01:32,560
 how your family responds and

29
00:01:32,560 --> 00:01:36,770
 certainly you don't want to hang around and watch your

30
00:01:36,770 --> 00:01:40,960
 family suffering. So if you force them to do

31
00:01:40,960 --> 00:01:43,140
 a Buddhist funeral for you then you have to consider

32
00:01:43,140 --> 00:01:45,520
 whether that's going to cause more suffering,

33
00:01:45,520 --> 00:01:49,990
 more harm than good. Sometimes you might want to let them

34
00:01:49,990 --> 00:01:53,600
 give you a Catholic funeral if it's

35
00:01:53,600 --> 00:01:56,760
 what makes them happy. In that regard, before I get on to

36
00:01:56,760 --> 00:01:59,200
 actually letting people know that you're

37
00:01:59,200 --> 00:02:01,760
 Buddhist, just in regard to funerals there is no such thing

38
00:02:01,760 --> 00:02:04,560
 as a Buddhist funeral. Buddhism was not

39
00:02:04,560 --> 00:02:09,730
 a religion specifically for society. It's obviously a

40
00:02:09,730 --> 00:02:14,960
 religion to leave behind society and to go

41
00:02:14,960 --> 00:02:19,440
 inside yourself. So the idea of a Buddhist funeral is a bit

42
00:02:19,440 --> 00:02:23,240
 misleading. Any sort of culture is fine.

43
00:02:23,240 --> 00:02:25,600
 If you're going to do a Brahmin funeral that's what they

44
00:02:25,600 --> 00:02:27,200
 would have done in the Buddhist time,

45
00:02:27,200 --> 00:02:31,510
 they burned the body and put it in the river or so on. That

46
00:02:31,510 --> 00:02:34,440
 being said, there are three things

47
00:02:34,440 --> 00:02:38,010
 three principles to a Buddhist funeral that one could

48
00:02:38,010 --> 00:02:40,720
 consider. This is a funeral based on Buddhist

49
00:02:40,720 --> 00:02:45,160
 principles. These three principles are things that people

50
00:02:45,160 --> 00:02:47,520
 should keep in mind. I've always

51
00:02:47,520 --> 00:02:50,680
 taught these as three things to keep in mind at funeral.

52
00:02:50,680 --> 00:02:52,640
 The first one is that you should never

53
00:02:52,640 --> 00:02:54,980
 grieve for the person who has passed away. This is

54
00:02:54,980 --> 00:02:57,600
 something that you can let people know before

55
00:02:57,600 --> 00:03:00,390
 you die that you forbid them to grieve for you or not

56
00:03:00,390 --> 00:03:02,560
 forbid them. You don't want them to.

57
00:03:02,560 --> 00:03:06,850
 The reason for this is because it doesn't help. There's no

58
00:03:06,850 --> 00:03:09,040
 benefit that is gained from crying

59
00:03:09,040 --> 00:03:13,110
 and wailing and thinking fondly back over the times that

60
00:03:13,110 --> 00:03:15,480
 you had. The most important thing

61
00:03:15,480 --> 00:03:19,660
 you can do while you're still alive is entreat people to

62
00:03:19,660 --> 00:03:22,200
 understand the transiency of life.

63
00:03:22,200 --> 00:03:27,540
 In the end, we all pass away. We can say we're family, but

64
00:03:27,540 --> 00:03:30,040
 this is only a temporary span of

65
00:03:30,040 --> 00:03:34,510
 time that eventually we have to leave each other behind. Em

66
00:03:34,510 --> 00:03:37,080
phasizing this is very important. It's

67
00:03:37,080 --> 00:03:41,270
 something that should be emphasized at the funeral. Maybe

68
00:03:41,270 --> 00:03:43,680
 there's a way to let people know

69
00:03:43,680 --> 00:03:47,430
 that and to ensure that they keep that in mind at your

70
00:03:47,430 --> 00:03:50,480
 funeral. It's not supposed to be a grieving

71
00:03:50,480 --> 00:03:53,160
 process. It's supposed to be a saying goodbye and moving on

72
00:03:53,160 --> 00:03:55,960
 to the next part of life. The second

73
00:03:55,960 --> 00:04:00,610
 thing is to take death as an example and to use it as a

74
00:04:00,610 --> 00:04:03,560
 reason to think of your own mortality.

75
00:04:03,560 --> 00:04:06,890
 Everyone at the funeral should be understanding that they

76
00:04:06,890 --> 00:04:09,320
 as well will pass away. If they're

77
00:04:09,320 --> 00:04:12,140
 going to sit around and cry and moan, then everybody who

78
00:04:12,140 --> 00:04:13,840
 they leave behind will also sit

79
00:04:13,840 --> 00:04:17,080
 around and cry and moan. This is something that we should

80
00:04:17,080 --> 00:04:19,080
 keep in mind. It's something that we

81
00:04:19,080 --> 00:04:23,310
 should use to wake ourselves up rather than to dwell upon.

82
00:04:23,310 --> 00:04:25,720
 Death is something that should make

83
00:04:25,720 --> 00:04:28,540
 us think that I also have to pass away. A better thing to

84
00:04:28,540 --> 00:04:31,060
 do than to sit around crying is to

85
00:04:31,060 --> 00:04:34,930
 actually use it as an impetus to get something done to make

86
00:04:34,930 --> 00:04:37,160
 your life meaningful and to go ahead

87
00:04:37,160 --> 00:04:41,530
 and ready yourself for the eventuality of death and to

88
00:04:41,530 --> 00:04:44,680
 prepare yourself for the reality of life,

89
00:04:44,680 --> 00:04:50,010
 that we're born and that we die. The third thing is to do

90
00:04:50,010 --> 00:04:52,520
 good deeds on behalf of the deceased

91
00:04:52,520 --> 00:04:55,900
 person. One thing that you can really emphasize for the

92
00:04:55,900 --> 00:05:00,600
 people who are leaving behind is that

93
00:05:00,600 --> 00:05:04,830
 they should do something good on your behalf. When people

94
00:05:04,830 --> 00:05:07,880
 die, Buddhists will often use it as an

95
00:05:07,880 --> 00:05:10,340
 opportunity to do good things in their name. For people who

96
00:05:10,340 --> 00:05:12,120
 are not Buddhist, you can just explain

97
00:05:12,120 --> 00:05:15,370
 all of the good that this person has done, all of the

98
00:05:15,370 --> 00:05:17,560
 things that you love about them,

99
00:05:17,560 --> 00:05:25,190
 all of the good qualities that they had inside. You should

100
00:05:25,190 --> 00:05:26,600
 do whatever you can to ensure that

101
00:05:26,600 --> 00:05:31,010
 those qualities are passed on or carried on and persist. If

102
00:05:31,010 --> 00:05:33,480
 you do good things in the name of this

103
00:05:33,480 --> 00:05:37,770
 person, it's like they're still alive. It's like their

104
00:05:37,770 --> 00:05:42,520
 legacy has been passed on. This is how you'd

105
00:05:42,520 --> 00:05:45,370
 run a Buddhist funeral. The other part of the question, if

106
00:05:45,370 --> 00:05:47,000
 supposing you want to let people

107
00:05:47,000 --> 00:05:50,760
 know that you're Buddhist, I don't think there's anything

108
00:05:50,760 --> 00:05:53,480
 wrong with that. I think in fact it would

109
00:05:53,480 --> 00:05:57,850
 be quite useful to do that before you pass away. As far as

110
00:05:57,850 --> 00:06:00,840
 dealing with people who are not Buddhist,

111
00:06:00,840 --> 00:06:06,040
 you have to take it carefully and try to help them

112
00:06:06,040 --> 00:06:09,720
 understand in a way that doesn't alienate

113
00:06:09,720 --> 00:06:12,820
 you from them or make them look at you as an outsider. You

114
00:06:12,820 --> 00:06:15,080
 don't have to say you're Buddhist.

115
00:06:15,080 --> 00:06:18,360
 You can start by saying that you're meditating. Say that

116
00:06:18,360 --> 00:06:22,440
 you're starting to understand that

117
00:06:22,440 --> 00:06:26,000
 happiness and peace is inside. You can even quote the Bible

118
00:06:26,000 --> 00:06:28,600
, the kingdom of God is within and so on

119
00:06:28,600 --> 00:06:31,290
 and start to explain to them the things that you've started

120
00:06:31,290 --> 00:06:33,560
 to realize as you practice. Buddhism is

121
00:06:33,560 --> 00:06:36,500
 just a word. You don't have to say that you're Buddhist or

122
00:06:36,500 --> 00:06:38,280
 tell people that you're Buddhist and

123
00:06:38,280 --> 00:06:41,900
 certainly it doesn't matter. It shouldn't matter what the

124
00:06:41,900 --> 00:06:45,800
 nature of your funeral is for a Buddhist.

125
00:06:45,800 --> 00:06:49,570
 I'd encourage you to try to stick to the fundamentals. If

126
00:06:49,570 --> 00:06:51,080
 you look at my meditation

127
00:06:51,080 --> 00:06:54,000
 videos, I never talk about Buddhism and you don't have to.

128
00:06:54,000 --> 00:06:58,360
 You just practice and teach and study.

129
00:06:58,360 --> 00:07:02,370
 If you do that, you should be able to deal with just about

130
00:07:02,370 --> 00:07:04,840
 anybody and relate to just about anybody.

131
00:07:04,840 --> 00:07:07,330
 You'll find that there's actually a lot less difference

132
00:07:07,330 --> 00:07:09,560
 between you and them and you'll be

133
00:07:09,560 --> 00:07:14,240
 able to keep your relationship with them on positive terms.

134
00:07:14,240 --> 00:07:16,200
 Try to explain to them again

135
00:07:16,200 --> 00:07:19,440
 what are the fundamental things that you've found helpful

136
00:07:19,440 --> 00:07:21,240
 and useful in Buddhism and just talk

137
00:07:21,240 --> 00:07:23,710
 about those. Let people know that you're meditating and

138
00:07:23,710 --> 00:07:26,120
 that when you die, as I said, you don't want

139
00:07:26,120 --> 00:07:29,560
 people to grieve. You want them to know that they have to

140
00:07:29,560 --> 00:07:31,480
 die and this is a part of life.

141
00:07:31,480 --> 00:07:36,580
 When you pass away, you will expect them to act according

142
00:07:36,580 --> 00:07:41,320
 to your principles. If they love you,

143
00:07:41,320 --> 00:07:45,390
 if they care for you, to take seriously the nature of your

144
00:07:45,390 --> 00:07:47,800
 life and the way of your being.

145
00:07:47,800 --> 00:07:53,360
 I hope that helps. It's an interesting question. It's one

146
00:07:53,360 --> 00:07:56,200
 that I've often had to talk on.

147
00:07:56,200 --> 00:07:58,500
 I hope that's of use. If you have any more questions,

148
00:07:58,500 --> 00:07:59,480
 please send them along.

