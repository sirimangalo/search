1
00:00:00,000 --> 00:00:19,840
 So today we continue on talking about karawat, respect, the

2
00:00:19,840 --> 00:00:24,440
 blessing or the auspiciousness,

3
00:00:24,440 --> 00:00:30,150
 the omen, the sign of good things to come, which is called

4
00:00:30,150 --> 00:00:31,400
 respect.

5
00:00:31,400 --> 00:00:34,240
 Respect is a sign of good things to come.

6
00:00:34,240 --> 00:00:38,120
 It shows that a person is sincere in what they're doing.

7
00:00:38,120 --> 00:00:43,110
 We've talked about respect for the Buddha, respect for the

8
00:00:43,110 --> 00:00:45,720
 Buddha's teaching, respect

9
00:00:45,720 --> 00:00:55,520
 for the people who have practiced the Buddha's teaching.

10
00:00:55,520 --> 00:01:02,120
 And on we have three more objects of respect.

11
00:01:02,120 --> 00:01:09,920
 The next one is the respect for the training.

12
00:01:09,920 --> 00:01:14,200
 Respect for the training is respect for those things which

13
00:01:14,200 --> 00:01:15,700
 we have to study and we have

14
00:01:15,700 --> 00:01:16,700
 to train ourselves in.

15
00:01:16,700 --> 00:01:25,320
 We have to train ourselves with a respectful and a reverent

16
00:01:25,320 --> 00:01:30,080
ial attitude, with a sincere

17
00:01:30,080 --> 00:01:34,080
 attitude, with our whole heart.

18
00:01:34,080 --> 00:01:43,480
 When we train we can try to train our minds.

19
00:01:43,480 --> 00:01:52,740
 We should be careful to train our minds with a sincere,

20
00:01:52,740 --> 00:01:58,680
 sincerely and wholeheartedly.

21
00:01:58,680 --> 00:02:08,270
 Lord Buddha gave a simile a comparison with a man who was

22
00:02:08,270 --> 00:02:13,560
 instructed to carry a pot full

23
00:02:13,560 --> 00:02:19,060
 of oil, full to the brim, means completely full of oil, so

24
00:02:19,060 --> 00:02:21,800
 that not one drop of oil could

25
00:02:21,800 --> 00:02:24,280
 fit in the bowl.

26
00:02:24,280 --> 00:02:32,630
 He was instructed to carry the bowl of oil through a crowd,

27
00:02:32,630 --> 00:02:36,040
 through a parade, through

28
00:02:36,040 --> 00:02:40,990
 a carnival, through the city in a time when there was a

29
00:02:40,990 --> 00:02:43,920
 carnival and there was dancing

30
00:02:43,920 --> 00:02:51,650
 and singing and music and festivities and all sorts of

31
00:02:51,650 --> 00:02:57,880
 displays, beautiful women, beautiful

32
00:02:57,880 --> 00:03:03,410
 sounds and dancing and this and that and shows and displays

33
00:03:03,410 --> 00:03:06,400
 and acrobats and so many things

34
00:03:06,400 --> 00:03:09,160
 and elephants.

35
00:03:09,160 --> 00:03:13,820
 He was instructed to carry this bowl full of oil through

36
00:03:13,820 --> 00:03:15,880
 the city and they said they would

37
00:03:15,880 --> 00:03:27,310
 have another man follow behind him with a sword and if the

38
00:03:27,310 --> 00:03:30,320
 man holding the oil were to

39
00:03:30,320 --> 00:03:36,930
 spill even a drop, the man walking behind him was ordered

40
00:03:36,930 --> 00:03:42,240
 to cut his head off with the sword.

41
00:03:42,240 --> 00:03:45,840
 And the Lord Buddha asked if that were the case, do you

42
00:03:45,840 --> 00:03:47,840
 think the man would waste any

43
00:03:47,840 --> 00:03:51,970
 time looking at the shows or looking at the beautiful women

44
00:03:51,970 --> 00:03:53,880
 or looking at the acrobats

45
00:03:53,880 --> 00:04:03,750
 and the elephants and the very various sights and sounds

46
00:04:03,750 --> 00:04:07,800
 and smells and so on.

47
00:04:07,800 --> 00:04:09,400
 Would he waste any time doing so?

48
00:04:09,400 --> 00:04:15,600
 And of course the monk said no, no he wouldn't do so.

49
00:04:15,600 --> 00:04:22,720
 His mind would be fully intent upon the bowl of oil or fear

50
00:04:22,720 --> 00:04:24,600
 of his life.

51
00:04:24,600 --> 00:04:27,190
 The Lord Buddha instructed the monks that they should treat

52
00:04:27,190 --> 00:04:29,440
 the training in this way.

53
00:04:29,440 --> 00:04:34,950
 They should have such sincere dedication to the training

54
00:04:34,950 --> 00:04:38,000
 and always remember the simile

55
00:04:38,000 --> 00:04:43,920
 of the man with the pot of oil.

56
00:04:43,920 --> 00:04:46,460
 The things which are necessary to train in the Buddha's

57
00:04:46,460 --> 00:04:48,040
 teaching, there's not many.

58
00:04:48,040 --> 00:04:50,800
 In fact there's three things.

59
00:04:50,800 --> 00:04:54,910
 The Lord Buddha taught that we have to train and these are

60
00:04:54,910 --> 00:05:01,680
 the base of the Buddhist practice.

61
00:05:01,680 --> 00:05:05,810
 They are the training which comes from the practice of

62
00:05:05,810 --> 00:05:07,160
 mindfulness.

63
00:05:07,160 --> 00:05:09,440
 When we practice the four foundations of mindfulness we are

64
00:05:09,440 --> 00:05:12,240
 training ourselves in three things.

65
00:05:12,240 --> 00:05:17,760
 One we are training ourselves in morality to avoid all evil

66
00:05:17,760 --> 00:05:21,320
 deeds of body, speech and even

67
00:05:21,320 --> 00:05:23,520
 bad deeds of mind.

68
00:05:23,520 --> 00:05:29,430
 Number two, training ourselves in concentration to cut off

69
00:05:29,430 --> 00:05:32,840
 those hindrances in the mind which

70
00:05:32,840 --> 00:05:37,160
 lead us to do bad deeds.

71
00:05:37,160 --> 00:05:44,060
 Three wisdom to develop the understanding which will cut

72
00:05:44,060 --> 00:05:47,720
 off all bad deeds so they will never

73
00:05:47,720 --> 00:05:49,360
 arise again.

74
00:05:49,360 --> 00:05:54,770
 Cut off all ignorance which leads to the arising of greed,

75
00:05:54,770 --> 00:05:58,080
 of anger which leads to the arising

76
00:05:58,080 --> 00:06:04,050
 of the hindrances which internally lead to the arising of

77
00:06:04,050 --> 00:06:07,120
 bad deeds of body, of speech

78
00:06:07,120 --> 00:06:13,720
 and of mind.

79
00:06:13,720 --> 00:06:18,810
 Developing morality we develop at the time when we are

80
00:06:18,810 --> 00:06:20,360
 practicing.

81
00:06:20,360 --> 00:06:28,540
 First we have mundane morality as keeping the precepts.

82
00:06:28,540 --> 00:06:33,140
 Keeping the precepts is not synonymous with the training of

83
00:06:33,140 --> 00:06:35,600
 morality which the Lord Buddha

84
00:06:35,600 --> 00:06:36,600
 taught.

85
00:06:36,600 --> 00:06:42,070
 Keeping the precepts is simply a means of encouraging one

86
00:06:42,070 --> 00:06:44,920
 to develop true morality.

87
00:06:44,920 --> 00:06:49,750
 Whether we have five precepts or eight precepts or ten

88
00:06:49,750 --> 00:06:53,120
 precepts or 227 precepts.

89
00:06:53,120 --> 00:06:58,110
 These are things which lead to peace and happiness in the

90
00:06:58,110 --> 00:06:59,040
 world.

91
00:06:59,040 --> 00:07:05,160
 They can be considered essential for leading a good life,

92
00:07:05,160 --> 00:07:08,360
 for living a proper life in the

93
00:07:08,360 --> 00:07:09,360
 world.

94
00:07:09,360 --> 00:07:16,720
 They are essential for undertaking the practice but they in

95
00:07:16,720 --> 00:07:20,960
 themselves don't lead to freedom

96
00:07:20,960 --> 00:07:24,120
 from suffering.

97
00:07:24,120 --> 00:07:28,330
 Until we develop morality on a practical level we can keep

98
00:07:28,330 --> 00:07:31,280
 the precepts for our whole life

99
00:07:31,280 --> 00:07:35,880
 and still never reach freedom from suffering.

100
00:08:02,100 --> 00:08:05,080
 or wonder or worry.

101
00:08:05,080 --> 00:08:13,080
 Stop our minds from becoming distracted by these thoughts.

102
00:08:13,080 --> 00:08:18,360
 It means when we think these thoughts we acknowledge them

103
00:08:18,360 --> 00:08:19,720
 with a word.

104
00:08:19,720 --> 00:08:29,760
 Angry, angry, liking, liking, wanting, distracted, doubting

105
00:08:29,760 --> 00:08:32,040
, worrying.

106
00:08:32,040 --> 00:08:36,180
 Bring our minds back to the present moment, back to the

107
00:08:36,180 --> 00:08:37,600
 object at hand.

108
00:08:37,600 --> 00:08:41,360
 When we bring our minds back to the present moment this is

109
00:08:41,360 --> 00:08:43,440
 morality on a practical level.

110
00:08:43,440 --> 00:08:46,970
 Forbidding the mind from indulging in these thoughts or

111
00:08:46,970 --> 00:08:47,880
 that time.

112
00:08:47,880 --> 00:08:50,560
 Using mindfulness to stop.

113
00:08:50,560 --> 00:08:56,280
 Stop the mind from wandering.

114
00:08:56,280 --> 00:09:01,920
 This is the practice of morality.

115
00:09:01,920 --> 00:09:04,600
 This is the morality which leads to the badminton.

116
00:09:04,600 --> 00:09:08,060
 Because when we practice this morality it leads to

117
00:09:08,060 --> 00:09:09,160
 concentration.

118
00:09:09,160 --> 00:09:12,120
 Concentration is of two kinds as well.

119
00:09:12,120 --> 00:09:19,380
 Mundane concentration and concentration which leads to

120
00:09:19,380 --> 00:09:22,560
 freedom from suffering.

121
00:09:22,560 --> 00:09:26,850
 Mundane concentration we have concentration which fixes on

122
00:09:26,850 --> 00:09:28,920
 a conceptual object from the

123
00:09:28,920 --> 00:09:38,360
 Lord Buddha taught up to 40 conceptual objects.

124
00:09:38,360 --> 00:09:42,960
 Focusing on a white disc, a blue disc, a red disc.

125
00:09:42,960 --> 00:09:45,840
 Focusing on a bowl of earth.

126
00:09:45,840 --> 00:09:48,800
 Focusing on a bowl of water.

127
00:09:48,800 --> 00:09:50,120
 Focusing on a flame.

128
00:09:50,120 --> 00:09:55,500
 Focusing on many things which are concepts which we take up

129
00:09:55,500 --> 00:09:55,720
.

130
00:09:55,720 --> 00:09:59,500
 We take up the concept of white, the concept of red, the

131
00:09:59,500 --> 00:10:01,760
 concept of blue, of yellow, of

132
00:10:01,760 --> 00:10:07,720
 fire, of earth, of air, of water and so on.

133
00:10:07,720 --> 00:10:12,830
 We take up the Buddha, we take up the Dhamma, the Sangha,

134
00:10:12,830 --> 00:10:15,040
 all together 40.

135
00:10:15,040 --> 00:10:23,280
 They recognize 40 subjects which the Lord Buddha taught.

136
00:10:23,280 --> 00:10:26,880
 Which all have a concept as their object.

137
00:10:26,880 --> 00:10:36,200
 But because the object of contemplation here is a concept,

138
00:10:36,200 --> 00:10:37,680
 the contemplation can't lead

139
00:10:37,680 --> 00:10:41,320
 to freedom from suffering.

140
00:10:41,320 --> 00:10:43,730
 One cannot see about the object that it is impermanent,

141
00:10:43,730 --> 00:10:45,120
 that it is suffering, that it

142
00:10:45,120 --> 00:10:47,080
 is non-self.

143
00:10:47,080 --> 00:10:53,450
 Because as a concept it can be permanent, it can be happy,

144
00:10:53,450 --> 00:10:55,320
 it can be self.

145
00:10:55,320 --> 00:11:00,400
 We can control it, we can keep it forever and so on.

146
00:11:00,400 --> 00:11:05,800
 Because it's only a concept, it's not real.

147
00:11:05,800 --> 00:11:09,160
 The concentration which leads us out of suffering is

148
00:11:09,160 --> 00:11:11,960
 concentration which focuses on reality,

149
00:11:11,960 --> 00:11:14,560
 focuses on the body, focuses on the mind.

150
00:11:14,560 --> 00:11:22,190
 We have the four foundations of mindfulness, these are one

151
00:11:22,190 --> 00:11:25,640
 description of reality.

152
00:11:25,640 --> 00:11:31,250
 Mindfulness of the body, the feelings, the mind and the D

153
00:11:31,250 --> 00:11:32,280
hamma.

154
00:11:32,280 --> 00:11:35,950
 The Dhammas, but Dhammas in this case are the Paramata Dham

155
00:11:35,950 --> 00:11:37,920
mas, those Dhammas which are

156
00:11:37,920 --> 00:11:38,920
 real.

157
00:11:38,920 --> 00:11:43,720
 And when we focus on these, acknowledging rising, falling,

158
00:11:43,720 --> 00:11:45,680
 and the mind is focused on

159
00:11:45,680 --> 00:11:50,610
 the rising and on the falling, or focused on the feelings

160
00:11:50,610 --> 00:11:53,200
 of pain or aching and so on.

161
00:11:53,200 --> 00:11:57,670
 Focused on the mind, focused on the mind object, on the D

162
00:11:57,670 --> 00:11:58,600
hammas.

163
00:11:58,600 --> 00:12:05,540
 In the time of the Buddha there was one man who became a

164
00:12:05,540 --> 00:12:09,920
 monk simply to learn the magical

165
00:12:09,920 --> 00:12:17,030
 powers of the Buddha that the Lord Buddha was supposed to

166
00:12:17,030 --> 00:12:18,160
 have.

167
00:12:18,160 --> 00:12:20,840
 And then he was going to disrobe.

168
00:12:20,840 --> 00:12:24,940
 He wanted to steal the fame of the Lord Buddha and bring it

169
00:12:24,940 --> 00:12:27,560
 back to his own religious teachers

170
00:12:27,560 --> 00:12:29,320
 once he had learned the magic.

171
00:12:29,320 --> 00:12:34,010
 In the time of the Buddha there were many people who did

172
00:12:34,010 --> 00:12:36,600
 this, trying to steal away

173
00:12:36,600 --> 00:12:42,330
 the fame and the renown of the Lord Buddha by gaining the

174
00:12:42,330 --> 00:12:45,560
 same magical powers which the

175
00:12:45,560 --> 00:12:49,640
 Lord Buddha was supposed to have.

176
00:12:49,640 --> 00:12:53,130
 And he went to, once he ordained he heard about there were

177
00:12:53,130 --> 00:12:54,960
 some arahants in the area,

178
00:12:54,960 --> 00:12:57,620
 some enlightened ones, so he thought, "Oh then I'll go ask

179
00:12:57,620 --> 00:12:59,080
 them for the magical powers

180
00:12:59,080 --> 00:13:04,360
 and the attainments which they have."

181
00:13:04,360 --> 00:13:18,990
 And so he went to ask them, "So, do you have this magical

182
00:13:18,990 --> 00:13:20,600
 power or that magical power?

183
00:13:20,600 --> 00:13:25,600
 Can you see far away or you're far away?

184
00:13:25,600 --> 00:13:27,520
 You have this attainment or that attainment?"

185
00:13:27,520 --> 00:13:33,080
 And they kept saying, "No, no, no."

186
00:13:33,080 --> 00:13:35,480
 All sorts of special attainments they had, man.

187
00:13:35,480 --> 00:13:39,620
 And he said, "Well then, how can you claim to be free from

188
00:13:39,620 --> 00:13:40,840
 suffering?"

189
00:13:40,840 --> 00:13:51,970
 All the group of them they explained to him, "We are free

190
00:13:51,970 --> 00:13:56,080
 through wisdom."

191
00:13:56,080 --> 00:13:59,350
 The Lord Buddha's teaching, there are two ways to become

192
00:13:59,350 --> 00:13:59,880
 free.

193
00:13:59,880 --> 00:14:08,510
 There are freedom through concentration and freedom through

194
00:14:08,510 --> 00:14:10,040
 wisdom.

195
00:14:10,040 --> 00:14:14,050
 Some people practice samatha, they practice tranquilizing

196
00:14:14,050 --> 00:14:15,880
 the mind first and then they

197
00:14:15,880 --> 00:14:20,520
 develop wisdom and impermanent suffering in oneself.

198
00:14:20,520 --> 00:14:26,630
 Some people just practice to see impermanent suffering in

199
00:14:26,630 --> 00:14:27,920
 oneself.

200
00:14:27,920 --> 00:14:29,920
 And this man said, "I don't understand that meaning.

201
00:14:29,920 --> 00:14:31,720
 I don't understand the meaning of that."

202
00:14:31,720 --> 00:14:35,630
 And they said, "Whether you understand the meaning or not,

203
00:14:35,630 --> 00:14:37,720
 we're free through wisdom."

204
00:14:37,720 --> 00:14:43,130
 It means the concentration which they gain was not enough

205
00:14:43,130 --> 00:14:46,040
 to gain magical powers or high

206
00:14:46,040 --> 00:14:49,490
 spiritual attainments, but it was enough for them to give

207
00:14:49,490 --> 00:14:51,400
 up greed, to give up anger, to

208
00:14:51,400 --> 00:14:54,490
 give up delusion, which is the true goal of the Buddha's

209
00:14:54,490 --> 00:14:55,280
 teaching.

210
00:14:55,280 --> 00:14:58,880
 Whether one gains magical powers or high spiritual

211
00:14:58,880 --> 00:15:01,600
 attainment and peace and calm which would

212
00:15:01,600 --> 00:15:07,060
 lead to being reborn in the Brahma realms, whether one

213
00:15:07,060 --> 00:15:10,000
 gains these things or not.

214
00:15:10,000 --> 00:15:12,690
 The goal of the Buddha's teaching, the concentration which

215
00:15:12,690 --> 00:15:14,520
 we need is momentary concentration.

216
00:15:14,520 --> 00:15:22,020
 It means that every moment we have to be focused on the

217
00:15:22,020 --> 00:15:24,840
 present moment.

218
00:15:24,840 --> 00:15:30,670
 This is the training in concentration, the training in

219
00:15:30,670 --> 00:15:31,840
 wisdom.

220
00:15:31,840 --> 00:15:35,810
 Training in wisdom is, of course, the wisdom is of two

221
00:15:35,810 --> 00:15:36,600
 kinds.

222
00:15:36,600 --> 00:15:41,830
 There's mundane wisdom and wisdom which leads to freedom

223
00:15:41,830 --> 00:15:43,640
 from suffering.

224
00:15:43,640 --> 00:15:49,120
 Mundane wisdom is the wisdom we use in the world to make

225
00:15:49,120 --> 00:15:52,400
 money, to keep our livelihood

226
00:15:52,400 --> 00:15:57,920
 going.

227
00:15:57,920 --> 00:16:03,540
 For monks it's the wisdom which allows us to live our lives

228
00:16:03,540 --> 00:16:06,160
 and to teach other people

229
00:16:06,160 --> 00:16:21,280
 and so on, to help other people, to have the wisdom to

230
00:16:21,280 --> 00:16:24,400
 study and to live, to keep the precepts

231
00:16:24,400 --> 00:16:26,800
 and so on.

232
00:16:26,800 --> 00:16:32,090
 For lay people that means the wisdom to live their lives

233
00:16:32,090 --> 00:16:35,040
 properly in the world without

234
00:16:35,040 --> 00:16:39,820
 making mistakes in their work, knowing their duties to

235
00:16:39,820 --> 00:16:44,080
 their father and mother, their children,

236
00:16:44,080 --> 00:16:49,650
 their duties to their friends and their teachers, their

237
00:16:49,650 --> 00:16:53,680
 duties to their employees and so on.

238
00:16:53,680 --> 00:16:58,620
 Knowing what are good things, what are bad things, knowing

239
00:16:58,620 --> 00:17:02,720
 to stay away from bad friends,

240
00:17:02,720 --> 00:17:05,320
 knowing how to find good friends and so on.

241
00:17:05,320 --> 00:17:08,260
 This is all wisdom in the world, things which lead to peace

242
00:17:08,260 --> 00:17:09,200
 and happiness.

243
00:17:09,200 --> 00:17:12,440
 The Lord Buddha also taught this kind of wisdom.

244
00:17:12,440 --> 00:17:15,040
 Make no mistake that mundane morality, mundane

245
00:17:15,040 --> 00:17:18,360
 concentration, mundane wisdom, these are things

246
00:17:18,360 --> 00:17:22,120
 which the Lord Buddha taught over and over again.

247
00:17:22,120 --> 00:17:28,810
 They are very important if we are going to develop the path

248
00:17:28,810 --> 00:17:32,240
 which leads out of suffering.

249
00:17:32,240 --> 00:17:35,120
 But they are never enough to lead us out of suffering.

250
00:17:35,120 --> 00:17:39,200
 We simply have to be able to tell the difference and

251
00:17:39,200 --> 00:17:42,600
 develop both mundane morality, concentration

252
00:17:42,600 --> 00:17:48,460
 and wisdom and super mundane morality, concentration and

253
00:17:48,460 --> 00:17:49,560
 wisdom.

254
00:17:49,560 --> 00:17:54,340
 Super mundane wisdom or the wisdom which leads out of

255
00:17:54,340 --> 00:18:01,040
 suffering is a very simple wisdom.

256
00:18:01,040 --> 00:18:08,390
 One past teacher in Thailand said it was like frog wisdom,

257
00:18:08,390 --> 00:18:12,200
 the wisdom of a frog, meaning

258
00:18:12,200 --> 00:18:17,600
 that it is not some high intellectual wisdom.

259
00:18:17,600 --> 00:18:20,860
 It is not the wisdom which you gain from going to

260
00:18:20,860 --> 00:18:23,960
 university or studying or even memorizing

261
00:18:23,960 --> 00:18:26,280
 the whole of the Buddha's teaching.

262
00:18:26,280 --> 00:18:29,410
 The wisdom which comes when you sit and acknowledge rising

263
00:18:29,410 --> 00:18:31,480
 and falling, even knowing the belly

264
00:18:31,480 --> 00:18:33,640
 rising and knowing the belly falling.

265
00:18:33,640 --> 00:18:40,770
 One time, the Lord Buddha said knowing rising one time is

266
00:18:40,770 --> 00:18:43,960
 of incredible benefit.

267
00:18:43,960 --> 00:18:48,150
 It is a far more beneficial wisdom than memorizing the

268
00:18:48,150 --> 00:18:50,200
 whole of the tikitaka.

269
00:18:50,200 --> 00:18:56,270
 You know one time rise and one time fall, knowing the

270
00:18:56,270 --> 00:18:58,000
 rising and falling.

271
00:18:58,000 --> 00:19:00,000
 This is a far better wisdom.

272
00:19:00,000 --> 00:19:02,320
 It is the wisdom of the present moment.

273
00:19:02,320 --> 00:19:05,980
 It is a taste of the present moment.

274
00:19:05,980 --> 00:19:09,100
 Even just one time allows one to see what is the present

275
00:19:09,100 --> 00:19:09,760
 moment.

276
00:19:09,760 --> 00:19:13,240
 We should never be discouraged when we think we are not

277
00:19:13,240 --> 00:19:14,840
 mindful all the time.

278
00:19:14,840 --> 00:19:18,600
 Even being mindful one time rising and falling.

279
00:19:18,600 --> 00:19:19,840
 Knowing the rising and falling.

280
00:19:19,840 --> 00:19:20,840
 This is wisdom.

281
00:19:20,840 --> 00:19:31,440
 It is the very base of the soil of understanding.

282
00:19:31,440 --> 00:19:34,790
 Because when we acknowledge rising and falling, when we

283
00:19:34,790 --> 00:19:37,000
 know the rising and know the falling,

284
00:19:37,000 --> 00:19:41,030
 afterwards we will come to know impermanent suffering and

285
00:19:41,030 --> 00:19:42,000
 non-self.

286
00:19:42,000 --> 00:19:45,080
 We will come to find the way out of suffering.

287
00:19:45,080 --> 00:19:51,880
 We will come to see how can we be free from our attachments

288
00:19:51,880 --> 00:19:53,760
, free from our addictions,

289
00:19:53,760 --> 00:19:57,440
 free from our cravings.

290
00:19:57,440 --> 00:19:59,480
 How can we come to be truly content?

291
00:19:59,480 --> 00:20:05,490
 You see, oh, when we see impermanence, we won't hold on to

292
00:20:05,490 --> 00:20:08,040
 things as permanent.

293
00:20:08,040 --> 00:20:11,890
 When we see suffering, we won't hold on to things as pleas

294
00:20:11,890 --> 00:20:12,840
urable.

295
00:20:12,840 --> 00:20:17,560
 When we see non-self, we won't hold on to things as self.

296
00:20:17,560 --> 00:20:20,040
 We won't hold on to anything in the world.

297
00:20:20,040 --> 00:20:26,120
 Naccha kinti lo ke yupadi yateen.

298
00:20:26,120 --> 00:20:29,000
 We won't attach to anything in the world.

299
00:20:29,000 --> 00:20:31,000
 Not one thing.

300
00:20:31,000 --> 00:20:33,000
 Nacchinti.

301
00:20:33,000 --> 00:20:37,000
 Nothing in the world.

302
00:20:37,000 --> 00:20:40,000
 Nacchinti lo ke.

303
00:20:40,000 --> 00:20:43,440
 Upadi yateen, we won't attach to anything.

304
00:20:43,440 --> 00:20:46,240
 We're attached to nothing.

305
00:20:46,240 --> 00:20:48,120
 This is the way out of suffering.

306
00:20:48,120 --> 00:20:51,410
 And then we'll come to see the highest wisdom, which is the

307
00:20:51,410 --> 00:20:53,080
 freedom from suffering.

308
00:20:53,080 --> 00:20:55,480
 The realization of freedom from suffering.

309
00:20:55,480 --> 00:21:06,860
 This is the highest because it frees one from attachment to

310
00:21:06,860 --> 00:21:08,480
 the world.

311
00:21:08,480 --> 00:21:15,640
 It allows one to see the true happiness and true peace, so

312
00:21:15,640 --> 00:21:22,800
 one never is confused.

313
00:21:22,800 --> 00:21:26,200
 Mistakenly trying to find happiness in the world.

314
00:21:26,200 --> 00:21:30,200
 One will never try to find happiness in the world again.

315
00:21:30,200 --> 00:21:35,860
 One will be able to let go of all things at all times

316
00:21:35,860 --> 00:21:36,960
 because one knows true peace and

317
00:21:36,960 --> 00:21:39,120
 true happiness.

318
00:21:39,120 --> 00:21:45,200
 This is respect for the training.

319
00:21:45,200 --> 00:21:50,400
 Again, going very slowly through this blessing.

320
00:21:50,400 --> 00:21:54,920
 Showing respect for the Buddhist teaching on respect.

321
00:21:54,920 --> 00:21:57,960
 You can see how important it is.

322
00:21:57,960 --> 00:21:58,960
 Thank you.

