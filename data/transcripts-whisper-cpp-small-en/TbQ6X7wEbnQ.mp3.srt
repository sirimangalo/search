1
00:00:00,000 --> 00:00:07,990
 The second and the third I can probably deal with together.

2
00:00:07,990 --> 00:00:08,960
 The second corralary

3
00:00:08,960 --> 00:00:12,200
 to the existence of the mind is the non-existence of the

4
00:00:12,200 --> 00:00:13,240
 self, the third is

5
00:00:13,240 --> 00:00:16,140
 the non-existence of God. Now of course these are not

6
00:00:16,140 --> 00:00:18,120
 directly inferred from the

7
00:00:18,120 --> 00:00:20,920
 existence of the mind. You could postulate the existence of

8
00:00:20,920 --> 00:00:21,760
 the mind and

9
00:00:21,760 --> 00:00:23,850
 postulate the existence of the soul, postulate the

10
00:00:23,850 --> 00:00:25,720
 existence of God, postulate

11
00:00:25,720 --> 00:00:28,820
 the existence of the tooth fairy and so on. You could have

12
00:00:28,820 --> 00:00:30,280
 all sorts of...they're

13
00:00:30,280 --> 00:00:34,120
 not mutually exclusive. What we're talking about here is

14
00:00:34,120 --> 00:00:35,340
 placing the mind in

15
00:00:35,340 --> 00:00:40,610
 a scientific context or in the context of reality. In

16
00:00:40,610 --> 00:00:41,840
 reality the mind is

17
00:00:41,840 --> 00:00:47,000
 very much interconnected, interdependent with the body.

18
00:00:47,000 --> 00:00:48,160
 They affect one another,

19
00:00:48,160 --> 00:00:51,260
 they work together. They're very much a part of the same

20
00:00:51,260 --> 00:00:53,000
 reality, two sides to

21
00:00:53,000 --> 00:00:59,440
 the same coin you could say. Once we understand that to be

22
00:00:59,440 --> 00:01:01,480
 reality there is

23
00:01:01,480 --> 00:01:05,730
 no room to postulate things like a self or things like a

24
00:01:05,730 --> 00:01:06,560
 God or so on.

25
00:01:06,560 --> 00:01:12,840
 In fact many of the things which we would care to postulate

26
00:01:12,840 --> 00:01:13,800
, many of the

27
00:01:13,800 --> 00:01:17,560
 things we would care to believe in are totally thrown out

28
00:01:17,560 --> 00:01:18,480
 of the window.

29
00:01:18,480 --> 00:01:21,510
 Understanding why there could not possibly be a self, could

30
00:01:21,510 --> 00:01:22,200
 not possibly

31
00:01:22,200 --> 00:01:24,840
 be a God, we have to come back and understand what is

32
00:01:24,840 --> 00:01:26,320
 reality. Now if we

33
00:01:26,320 --> 00:01:29,180
 talk in terms of the physical, this is something that I

34
00:01:29,180 --> 00:01:32,560
 think scientists don't

35
00:01:32,560 --> 00:01:37,240
 often fully understand. I'm not sure actually. I'm going to

36
00:01:37,240 --> 00:01:37,640
 assume that

37
00:01:37,640 --> 00:01:39,800
 it's often not understood and I'll explain it. If it's

38
00:01:39,800 --> 00:01:40,600
 already understood

39
00:01:40,600 --> 00:01:43,400
 then no problem. I think one of the things that is not

40
00:01:43,400 --> 00:01:44,640
 fully understood is

41
00:01:44,640 --> 00:01:48,070
 the idea of reductionism. Now I do think that the most

42
00:01:48,070 --> 00:01:49,480
 scientists would consider

43
00:01:49,480 --> 00:01:52,540
 themselves to be reductionists, but an understanding of

44
00:01:52,540 --> 00:01:53,520
 what it means to be

45
00:01:53,520 --> 00:01:56,170
 reductionists is opposed to Hollis. The Hollis is someone

46
00:01:56,170 --> 00:01:56,920
 who believes that the

47
00:01:56,920 --> 00:02:00,690
 hole is more than the sum of its parts. Reductionism says

48
00:02:00,690 --> 00:02:02,440
 what is there is all

49
00:02:02,440 --> 00:02:07,000
 that's real. Hollism says that once you put something

50
00:02:07,000 --> 00:02:08,480
 together, poof, something

51
00:02:08,480 --> 00:02:11,500
 else arises. I think that scientists don't understand this

52
00:02:11,500 --> 00:02:13,360
 because this is

53
00:02:13,360 --> 00:02:15,950
 what the position they're coming to in terms of the mind,

54
00:02:15,950 --> 00:02:16,560
 mostly as

55
00:02:16,560 --> 00:02:18,610
 materialists, they're coming to say that put the body

56
00:02:18,610 --> 00:02:19,720
 together, poof, the mind

57
00:02:19,720 --> 00:02:24,130
 arises. And reductionism says that what is there is what is

58
00:02:24,130 --> 00:02:24,840
 real. If it's not

59
00:02:24,840 --> 00:02:27,080
 there already, there's nothing that's going to bring it

60
00:02:27,080 --> 00:02:29,720
 into into existence.

61
00:02:29,720 --> 00:02:33,400
 This is why they're looking for this idea of what are the

62
00:02:33,400 --> 00:02:33,720
 building

63
00:02:33,720 --> 00:02:37,000
 blocks of reality because that's what's there and it's not

64
00:02:37,000 --> 00:02:37,800
 going to change.

65
00:02:37,800 --> 00:02:39,670
 You're not going to put them together and poof, suddenly

66
00:02:39,670 --> 00:02:40,560
 something else appears

67
00:02:40,560 --> 00:02:45,890
 out of out of nothing. Things don't arise out of nothing,

68
00:02:45,890 --> 00:02:48,560
 of course. So what

69
00:02:48,560 --> 00:02:52,060
 this means, suppose we take this table I'm sitting on or

70
00:02:52,060 --> 00:02:53,760
 this robe I'm wearing or

71
00:02:53,760 --> 00:02:58,430
 or you know any of the things in this room. Suppose we take

72
00:02:58,430 --> 00:02:59,480
 a chair and this is

73
00:02:59,480 --> 00:03:03,560
 usually the most obvious example. A chair we say it exists

74
00:03:03,560 --> 00:03:04,520
 and scientists I

75
00:03:04,520 --> 00:03:07,390
 think in general will say it exists, but this is where we

76
00:03:07,390 --> 00:03:08,440
 lose the scientific

77
00:03:08,440 --> 00:03:11,640
 nature of it. The chair doesn't exist. In reality the chair

78
00:03:11,640 --> 00:03:13,360
 is made up of things

79
00:03:13,360 --> 00:03:16,020
 which are real and we don't know quite what those are in

80
00:03:16,020 --> 00:03:17,240
 science yet. In

81
00:03:17,240 --> 00:03:21,880
 terms of Buddhism, it's actually quite simple. It's made up

82
00:03:21,880 --> 00:03:22,720
 of the four elements

83
00:03:22,720 --> 00:03:25,270
 and people thought these were Greek. Well actually it came

84
00:03:25,270 --> 00:03:26,080
 from earlier from

85
00:03:26,080 --> 00:03:29,130
 India. In the times of India they were already talking

86
00:03:29,130 --> 00:03:30,600
 about the four elements

87
00:03:30,600 --> 00:03:34,160
 earth, air, water, fire and these are just the four

88
00:03:34,160 --> 00:03:36,400
 properties of experienced

89
00:03:36,400 --> 00:03:40,350
 reality. But okay so putting that aside there is some

90
00:03:40,350 --> 00:03:43,000
 physical reality. Now the

91
00:03:43,000 --> 00:03:45,670
 mind is exactly the same. The mind is made up of building

92
00:03:45,670 --> 00:03:46,960
 blocks. Mostly we

93
00:03:46,960 --> 00:03:48,880
 don't quite know what those are. I can give you my

94
00:03:48,880 --> 00:03:50,020
 understanding of what they

95
00:03:50,020 --> 00:03:52,420
 are. It's not really important. We understand that they're

96
00:03:52,420 --> 00:03:52,840
 made up of

97
00:03:52,840 --> 00:03:56,460
 building blocks. Some examples are emotions. We have anger.

98
00:03:56,460 --> 00:03:57,080
 Anger is very

99
00:03:57,080 --> 00:03:59,980
 much a building block of reality. Greed. Greed is very much

100
00:03:59,980 --> 00:04:01,040
 a building block of

101
00:04:01,040 --> 00:04:06,960
 the mental reality. Wanting things exactly. Happiness. Unh

102
00:04:06,960 --> 00:04:08,240
appiness. Liking.

103
00:04:08,240 --> 00:04:15,500
 Disliking and so on. Conceit. Jealousy. These are some of

104
00:04:15,500 --> 00:04:16,240
 the building blocks of

105
00:04:16,240 --> 00:04:23,440
 the mind. Thinking. Worrying or so on. Many of the factors

106
00:04:23,440 --> 00:04:24,400
 of the mind.

107
00:04:24,400 --> 00:04:28,880
 Experiencing the reality of seeing or hearing or judging

108
00:04:28,880 --> 00:04:29,680
 something or being

109
00:04:29,680 --> 00:04:32,900
 able to measure something. Association. Being able to

110
00:04:32,900 --> 00:04:35,320
 remember things. All of

111
00:04:35,320 --> 00:04:36,850
 these different functions in the mind. These are the

112
00:04:36,850 --> 00:04:37,560
 building blocks of the

113
00:04:37,560 --> 00:04:41,880
 mind. Nowhere in here is it possible to say that when you

114
00:04:41,880 --> 00:04:42,400
 put these things

115
00:04:42,400 --> 00:04:45,280
 together there arises a self. And so this is what reduction

116
00:04:45,280 --> 00:04:46,320
ism means. It says that

117
00:04:46,320 --> 00:04:49,300
 once you have these building blocks there's no room for the

118
00:04:49,300 --> 00:04:50,120
 chair. There's

119
00:04:50,120 --> 00:04:53,000
 no room for the self. And there's no room for God. Now God

120
00:04:53,000 --> 00:04:54,920
 is a special example. Why

121
00:04:54,920 --> 00:04:56,880
 is there no room for God? Well you know we've got all these

122
00:04:56,880 --> 00:04:58,000
 things here. Why isn't

123
00:04:58,000 --> 00:05:01,320
 God something else? And this is something that often comes

124
00:05:01,320 --> 00:05:02,640
 up in meditation. People

125
00:05:02,640 --> 00:05:05,000
 who believe in God they come to meditate and they say they

126
00:05:05,000 --> 00:05:05,840
 still want to believe

127
00:05:05,840 --> 00:05:09,600
 in God. They say well belief you have to understand that

128
00:05:09,600 --> 00:05:10,880
 where belief arises

129
00:05:10,880 --> 00:05:14,400
 belief is just a building block. It's a part of the mind

130
00:05:14,400 --> 00:05:16,440
 that arises when you

131
00:05:16,440 --> 00:05:19,860
 when you you know your mind works in a certain way or maybe

132
00:05:19,860 --> 00:05:20,600
 it's even you know

133
00:05:20,600 --> 00:05:24,130
 built up with mixed up with a physical it surely is. It's

134
00:05:24,130 --> 00:05:25,560
 something that arises

135
00:05:25,560 --> 00:05:28,880
 based on on the functioning of the brain, the functioning

136
00:05:28,880 --> 00:05:29,920
 of the body, the

137
00:05:29,920 --> 00:05:32,760
 functioning of the mind and how they function together.

138
00:05:32,760 --> 00:05:33,720
 They give rise to

139
00:05:33,720 --> 00:05:37,330
 belief. Even if you were to see God and many people claim

140
00:05:37,330 --> 00:05:38,460
 to have seen God what

141
00:05:38,460 --> 00:05:41,080
 is that? It's an experience of seeing which is built up

142
00:05:41,080 --> 00:05:42,160
 with the physical and

143
00:05:42,160 --> 00:05:44,880
 the mental and the way the brain works, the way the mind

144
00:05:44,880 --> 00:05:46,600
 works. Suppose you're

145
00:05:46,600 --> 00:05:49,240
 sitting in and in prayer and suddenly you feel rapture.

146
00:05:49,240 --> 00:05:50,360
 Well again that's simply

147
00:05:50,360 --> 00:05:55,720
 rapture. And Sam Harris of course is an atheist who is very

148
00:05:55,720 --> 00:05:56,920
 good at explaining

149
00:05:56,920 --> 00:06:00,110
 this and he said you know you sit there and you you say

150
00:06:00,110 --> 00:06:01,640
 this is Jesus well you

151
00:06:01,640 --> 00:06:05,130
 know the Hindus are saying it's Krishna and the Buddhists

152
00:06:05,130 --> 00:06:06,080
 are saying well you

153
00:06:06,080 --> 00:06:08,210
 know it's the way the mind works and the scientists should

154
00:06:08,210 --> 00:06:08,960
 be saying the same

155
00:06:08,960 --> 00:06:10,750
 thing as well. The scientists will be saying it's where the

156
00:06:10,750 --> 00:06:12,520
 brain works. So I

157
00:06:12,520 --> 00:06:16,870
 think the Buddhism and science in general are on the same

158
00:06:16,870 --> 00:06:18,080
 wavelength. It's

159
00:06:18,080 --> 00:06:20,940
 just we've you know we're coming from different sides of

160
00:06:20,940 --> 00:06:22,200
 the coin. Buddhists

161
00:06:22,200 --> 00:06:25,580
 understand the existence of the mind and I think it's it's

162
00:06:25,580 --> 00:06:26,920
 a shame that in

163
00:06:26,920 --> 00:06:32,040
 general modern science doesn't. But at any rate once you

164
00:06:32,040 --> 00:06:32,960
 understand

165
00:06:32,960 --> 00:06:37,280
 the existence of the mind you completely blow the the idea

166
00:06:37,280 --> 00:06:38,080
 of the existence of

167
00:06:38,080 --> 00:06:41,570
 God out of the water. Even if you were to experience

168
00:06:41,570 --> 00:06:42,680
 something that you could

169
00:06:42,680 --> 00:06:45,720
 call God. Well I mean what could that possibly be? When I

170
00:06:45,720 --> 00:06:46,480
 experience another

171
00:06:46,480 --> 00:06:49,080
 person I still don't believe that's a person it's still not

172
00:06:49,080 --> 00:06:49,880
 a person. The

173
00:06:49,880 --> 00:06:52,410
 reality of it is there's just body and there's mind working

174
00:06:52,410 --> 00:06:53,920
 together. What if I

175
00:06:53,920 --> 00:06:57,000
 were to see God what what more could it be? And then the

176
00:06:57,000 --> 00:06:57,880
 experience and the

177
00:06:57,880 --> 00:07:02,470
 object of the experience. So these are just some brief

178
00:07:02,470 --> 00:07:03,680
 ideas and and this is

179
00:07:03,680 --> 00:07:06,440
 again very superficial. It's something that has to be

180
00:07:06,440 --> 00:07:07,080
 experienced through

181
00:07:07,080 --> 00:07:09,900
 meditation and it's not some kind of hokey-pokey meditation

182
00:07:09,900 --> 00:07:10,720
. It's simply

183
00:07:10,720 --> 00:07:13,470
 watching and learning and understanding the way the mind

184
00:07:13,470 --> 00:07:14,400
 works. It's a very

185
00:07:14,400 --> 00:07:18,070
 scientific process and you can't use physical tools to do

186
00:07:18,070 --> 00:07:19,240
 it because it's not

187
00:07:19,240 --> 00:07:21,920
 a physical thing. When you use mental tools to look at the

188
00:07:21,920 --> 00:07:22,840
 mind you just look

189
00:07:22,840 --> 00:07:25,520
 at your own mind and there are ways of doing this that are

190
00:07:25,520 --> 00:07:26,400
 very scientific.

191
00:07:26,400 --> 00:07:29,220
 You'll see for yourself what are the building blocks of the

192
00:07:29,220 --> 00:07:30,360
 mind and God and

193
00:07:30,360 --> 00:07:34,920
 the self are not in there. But again this is something that

194
00:07:34,920 --> 00:07:35,520
 takes a lot more

195
00:07:35,520 --> 00:07:39,360
 explanation and a lot more realization to fully understand.

196
00:07:39,360 --> 00:07:40,480
 But anyway thanks

197
00:07:40,480 --> 00:07:44,760
 for tuning in. This is just some more thoughts on on the

198
00:07:44,760 --> 00:07:47,560
 nature of reality.

