1
00:00:00,000 --> 00:00:06,590
 Hello and welcome back to Ask a Monk. Next question is one

2
00:00:06,590 --> 00:00:08,560
 that's probably

3
00:00:08,560 --> 00:00:13,850
 quite difficult to give an appropriate answer to, only

4
00:00:13,850 --> 00:00:16,040
 because it's a question

5
00:00:16,040 --> 00:00:23,130
 that if I give my honest opinion about, which I'm probably

6
00:00:23,130 --> 00:00:25,840
 going to do, there's

7
00:00:25,840 --> 00:00:28,850
 going to be a lot of people who disagree with me. So

8
00:00:28,850 --> 00:00:30,400
 hopefully I can minimize

9
00:00:30,400 --> 00:00:36,000
 that and explain myself and be as open-minded as possible

10
00:00:36,000 --> 00:00:36,680
 and I hope that

11
00:00:36,680 --> 00:00:43,280
 my audience is as open-minded as they can be as well. The

12
00:00:43,280 --> 00:00:44,060
 question is about

13
00:00:44,060 --> 00:00:51,600
 humor. What place does humor have in the Buddhist teaching

14
00:00:51,600 --> 00:00:52,760
 and what's wrong with,

15
00:00:52,760 --> 00:00:56,430
 is there anything wrong with innocent or harmless, harmless

16
00:00:56,430 --> 00:00:57,560
 jokes, I think is the

17
00:00:57,560 --> 00:01:05,480
 question, in Buddhism. So as with everything, you know, I'm

18
00:01:05,480 --> 00:01:05,920
 not, I have no

19
00:01:05,920 --> 00:01:11,240
 dogma in regards to humor or in regards to anything. Any

20
00:01:11,240 --> 00:01:13,760
 dogma should be thrown

21
00:01:13,760 --> 00:01:16,530
 out. We should analyze things scientifically. We use the

22
00:01:16,530 --> 00:01:17,080
 Buddhist

23
00:01:17,080 --> 00:01:22,020
 teaching as a guide, but we do so because we've analyzed it

24
00:01:22,020 --> 00:01:22,740
 and we think it's

25
00:01:22,740 --> 00:01:25,860
 the best way. We've also practiced it and through our

26
00:01:25,860 --> 00:01:27,960
 practice we come to see that

27
00:01:27,960 --> 00:01:33,680
 it truly is a way that leads to peace, happiness, and

28
00:01:33,680 --> 00:01:37,000
 freedom from suffering. So

29
00:01:37,000 --> 00:01:40,950
 looking scientifically at humor, we have to ask ourselves

30
00:01:40,950 --> 00:01:42,160
 what good is it and

31
00:01:42,160 --> 00:01:45,470
 what bad is it, what good comes from it, what bad comes

32
00:01:45,470 --> 00:01:48,360
 from it. And the first

33
00:01:48,360 --> 00:01:51,840
 thing to say that people often miss in Buddhism is that

34
00:01:51,840 --> 00:01:53,000
 there's nothing

35
00:01:53,000 --> 00:01:59,820
 intrinsically wrong with happy feelings. When they hear the

36
00:01:59,820 --> 00:02:00,440
 Buddhist teaching,

37
00:02:00,440 --> 00:02:05,160
 they think we're trying to get rid of all happy feelings or

38
00:02:05,160 --> 00:02:06,560
 if we practice the

39
00:02:06,560 --> 00:02:09,310
 Buddhist teaching successfully, we'll never feel happy

40
00:02:09,310 --> 00:02:10,560
 again. We'll never

41
00:02:10,560 --> 00:02:14,900
 feel sad but we'll also never feel happy and that's not

42
00:02:14,900 --> 00:02:17,400
 true. Happy feelings are

43
00:02:17,400 --> 00:02:29,990
 not on the same level as sad feelings. A happy feeling is a

44
00:02:29,990 --> 00:02:31,160
 feeling of

45
00:02:31,160 --> 00:02:35,400
 pleasure and a feeling of pleasure is on the same level as

46
00:02:35,400 --> 00:02:36,000
 a feeling of

47
00:02:36,000 --> 00:02:40,900
 pain. And through the Buddhist teaching, it's not the case

48
00:02:40,900 --> 00:02:41,960
 that you will in the

49
00:02:41,960 --> 00:02:48,640
 here and now be free from physical pain or be relieved of

50
00:02:48,640 --> 00:02:51,280
 pleasurable feelings.

51
00:02:51,280 --> 00:02:57,440
 Even enlightened beings still experience both physical pain

52
00:02:57,440 --> 00:02:58,720
 and physical pleasure.

53
00:02:58,720 --> 00:03:05,160
 They still experience these. They may also experience

54
00:03:05,160 --> 00:03:06,520
 mental pleasure as well

55
00:03:06,520 --> 00:03:10,670
 but what they will not experience is liking or disliking.

56
00:03:10,670 --> 00:03:11,240
 There will be no

57
00:03:11,240 --> 00:03:19,920
 attachment to the pleasant or the unpleasant sensation

58
00:03:19,920 --> 00:03:21,160
 which makes the

59
00:03:21,160 --> 00:03:29,730
 person attached and addicted. So with humor here, like so

60
00:03:29,730 --> 00:03:30,560
 many different things,

61
00:03:30,560 --> 00:03:36,930
 I think you can't disagree with the fact that it is

62
00:03:36,930 --> 00:03:39,280
 creating in most

63
00:03:39,280 --> 00:03:43,500
 cases a state of liking. Now I don't know whether Arahant's

64
00:03:43,500 --> 00:03:45,280
 enlightened beings

65
00:03:45,280 --> 00:03:50,160
 laugh or not. I've been told that they don't. That when

66
00:03:50,160 --> 00:03:52,000
 there's something that

67
00:03:52,000 --> 00:03:59,060
 they find humorous, they smile. And from what I've seen or

68
00:03:59,060 --> 00:04:00,240
 read or studied about,

69
00:04:00,240 --> 00:04:03,920
 they smile at the oddest sort of things. Like there's a

70
00:04:03,920 --> 00:04:06,400
 case of a monk who saw a

71
00:04:06,400 --> 00:04:11,880
 monk who had very advanced meditation and he'd done a lot

72
00:04:11,880 --> 00:04:12,400
 of yogic

73
00:04:12,400 --> 00:04:17,750
 meditations, very advanced meditations, and he was able to

74
00:04:17,750 --> 00:04:19,160
 see things that most

75
00:04:19,160 --> 00:04:22,480
 people couldn't see. And he was able to see ghosts, he was

76
00:04:22,480 --> 00:04:23,760
 able to see angels and

77
00:04:23,760 --> 00:04:28,440
 so on. And one day he saw a ghost flying through the air

78
00:04:28,440 --> 00:04:29,640
 and he saw many

79
00:04:29,640 --> 00:04:35,160
 actually but one he saw I think was being chased by crows.

80
00:04:35,160 --> 00:04:36,280
 It was a skeleton

81
00:04:36,280 --> 00:04:39,690
 flying through the air on fire being chased by crows or

82
00:04:39,690 --> 00:04:40,960
 something. It was in

83
00:04:40,960 --> 00:04:45,800
 terrible suffering and he smiled. And this monk next to him

84
00:04:45,800 --> 00:04:46,360
 said, "Why are you

85
00:04:46,360 --> 00:04:50,350
 smiling?" And he said, "Oh, I won't tell you." He said, "

86
00:04:50,350 --> 00:04:51,080
Let's go

87
00:04:51,080 --> 00:04:54,250
 talk to the Buddha." And so they went and talked to the

88
00:04:54,250 --> 00:04:55,760
 Buddha and he

89
00:04:55,760 --> 00:04:58,280
 explained it in front of the Buddha. And why he didn't tell

90
00:04:58,280 --> 00:04:58,760
 the monk is

91
00:04:58,760 --> 00:05:01,340
 because he knew that this monk would get angry and wouldn't

92
00:05:01,340 --> 00:05:02,720
 believe him. But in

93
00:05:02,720 --> 00:05:06,740
 front of the Buddha, the Buddha said, "Yeah, I saw that

94
00:05:06,740 --> 00:05:09,480
 same ghost and I didn't

95
00:05:09,480 --> 00:05:11,700
 say anything because I was waiting for a witness because

96
00:05:11,700 --> 00:05:12,640
 people wouldn't believe

97
00:05:12,640 --> 00:05:16,460
 me either." So they saw these fantastical things and the

98
00:05:16,460 --> 00:05:19,960
 reason they found it, they

99
00:05:19,960 --> 00:05:22,580
 didn't find it humorous, but the reason that they smiled it

100
00:05:22,580 --> 00:05:23,960
 is said is because

101
00:05:23,960 --> 00:05:26,830
 they knew for themselves that they were free from it. I

102
00:05:26,830 --> 00:05:28,320
 think the thought that

103
00:05:28,320 --> 00:05:32,620
 going through this monk's mind at the time was, "Wow, it's

104
00:05:32,620 --> 00:05:33,600
 amazing how far

105
00:05:33,600 --> 00:05:37,410
 I've come and how free I am from these sorts of states

106
00:05:37,410 --> 00:05:39,000
 because of not doing any

107
00:05:39,000 --> 00:05:45,970
 bad karma." The Buddha himself is said to have smiled when

108
00:05:45,970 --> 00:05:49,880
 he saw a young pig and

109
00:05:49,880 --> 00:05:54,490
 his attendant Ananda asked him, "Why did you smile?" And

110
00:05:54,490 --> 00:05:55,280
 the Buddha said, "You

111
00:05:55,280 --> 00:06:00,660
 saw that pig over there?" He said, "That pig used to be a

112
00:06:00,660 --> 00:06:05,440
 Brahma, a God, up in one

113
00:06:05,440 --> 00:06:15,520
 of the God realms." And on passing away from there, he, she

114
00:06:15,520 --> 00:06:18,440
 or it, is now a pig,

115
00:06:18,440 --> 00:06:21,680
 has now become a pig. And so the Buddha smiled. So they

116
00:06:21,680 --> 00:06:23,080
 have the oddest sort of

117
00:06:23,080 --> 00:06:28,040
 sense of humor, if you want to call it that. But this isn't

118
00:06:28,040 --> 00:06:29,000
 really the question.

119
00:06:29,000 --> 00:06:36,190
 The question of whether we find things humorous or not, I

120
00:06:36,190 --> 00:06:37,360
 think, is an

121
00:06:37,360 --> 00:06:41,110
 much easier one to answer. I think finding things humorous

122
00:06:41,110 --> 00:06:45,240
 as a Buddhist is

123
00:06:45,240 --> 00:06:51,330
 certainly possible. And I mean, I just think it's also

124
00:06:51,330 --> 00:06:53,200
 possible to find

125
00:06:53,200 --> 00:06:56,370
 something humorous and then become attached to it because

126
00:06:56,370 --> 00:06:58,120
 pleasure in

127
00:06:58,120 --> 00:07:02,190
 the body, there's a lot of chemical reactions and endorph

128
00:07:02,190 --> 00:07:03,760
ins and all of

129
00:07:03,760 --> 00:07:09,880
 these chemicals that are released, dopamine, whatever they

130
00:07:09,880 --> 00:07:10,100
 have

131
00:07:10,100 --> 00:07:14,940
 found in the brain. And it's easy to become addicted to

132
00:07:14,940 --> 00:07:16,160
 that as a bit of a

133
00:07:16,160 --> 00:07:20,220
 drug. But the question here is whether cracking jokes is

134
00:07:20,220 --> 00:07:22,120
 wholesome or

135
00:07:22,120 --> 00:07:29,810
 unwholesome thing. And I think it's probably unwholesome,

136
00:07:29,810 --> 00:07:32,520
 actually, to be

137
00:07:32,520 --> 00:07:40,250
 cracking jokes for the most part. But in the same way as

138
00:07:40,250 --> 00:07:40,880
 saying something

139
00:07:40,880 --> 00:07:45,220
 useless is, if we're sitting around talking about sports,

140
00:07:45,220 --> 00:07:46,720
 the Buddha would

141
00:07:46,720 --> 00:07:52,710
 say this is unwholesome because we're polluting our minds

142
00:07:52,710 --> 00:07:53,680
 with these

143
00:07:53,680 --> 00:07:56,380
 useless topics. But on the other hand, so many people like

144
00:07:56,380 --> 00:07:57,360
 to sit around and talk

145
00:07:57,360 --> 00:08:00,550
 about sports. I think a lot of Buddhists and Buddhist medit

146
00:08:00,550 --> 00:08:02,440
ators, this country

147
00:08:02,440 --> 00:08:07,050
 now has a cricket match going on the World Cup of Cricket

148
00:08:07,050 --> 00:08:08,320
 in Sri Lanka. And

149
00:08:08,320 --> 00:08:13,280
 most people here are Buddhists, but that certainly doesn't

150
00:08:13,280 --> 00:08:15,480
 stop them from cheering

151
00:08:15,480 --> 00:08:19,250
 on the Sri Lankan cricket team and talking about sports. I

152
00:08:19,250 --> 00:08:20,480
 mean, the point

153
00:08:20,480 --> 00:08:24,230
 is it's such a small thing to worry about. It's akin to

154
00:08:24,230 --> 00:08:27,160
 people worrying about

155
00:08:27,160 --> 00:08:33,590
 cursing. And someone asked that recently and I said, "You

156
00:08:33,590 --> 00:08:35,280
 know, I think it is a

157
00:08:35,280 --> 00:08:38,420
 technically it is an unwholesome thing or probably unwholes

158
00:08:38,420 --> 00:08:39,720
ome because it's

159
00:08:39,720 --> 00:08:45,220
 saying something that is unpleasant to other people and it

160
00:08:45,220 --> 00:08:45,680
's

161
00:08:45,680 --> 00:08:48,180
 brought about by mind states that are probably unwholesome

162
00:08:48,180 --> 00:08:49,880
." But this is really

163
00:08:49,880 --> 00:08:53,670
 the point is what is the intention in the mind? And I think

164
00:08:53,670 --> 00:08:54,760
 a lot of people

165
00:08:54,760 --> 00:09:03,760
 justify humor by the fact that it breaks tension. And so

166
00:09:03,760 --> 00:09:04,080
 when people are

167
00:09:04,080 --> 00:09:06,380
 upset, you know, you try to make them laugh, you find

168
00:09:06,380 --> 00:09:08,040
 something that

169
00:09:08,040 --> 00:09:15,150
 relaxes them. And I don't know actually. I know this is

170
00:09:15,150 --> 00:09:16,080
 even a technique that

171
00:09:16,080 --> 00:09:22,640
 meditation teachers use. Now I'm, you know, meditation

172
00:09:22,640 --> 00:09:24,200
 teachers are just

173
00:09:24,200 --> 00:09:26,130
 because someone's a meditation teacher doesn't mean they're

174
00:09:26,130 --> 00:09:26,760
 teaching the right

175
00:09:26,760 --> 00:09:31,510
 thing. And even though they might be famous, it doesn't

176
00:09:31,510 --> 00:09:33,160
 mean that they are on

177
00:09:33,160 --> 00:09:37,440
 the right track. In fact, in the Buddhist time, there were

178
00:09:37,440 --> 00:09:38,440
 several very famous

179
00:09:38,440 --> 00:09:42,480
 teachers who it seems were very much on the wrong track.

180
00:09:42,480 --> 00:09:43,400
 They had very strange

181
00:09:43,400 --> 00:09:52,260
 ideas. And the point is to get things and to get things

182
00:09:52,260 --> 00:09:53,400
 right and to get things

183
00:09:53,400 --> 00:09:59,080
 scientifically correct. So from a scientific point of view,

184
00:09:59,080 --> 00:10:00,040
 from a Buddhist

185
00:10:00,040 --> 00:10:02,660
 point of view, from a practical point of view, is humor

186
00:10:02,660 --> 00:10:04,120
 something that leads us

187
00:10:04,120 --> 00:10:07,780
 out of suffering. Now the problem I see with it is this, is

188
00:10:07,780 --> 00:10:10,160
 that if you're using,

189
00:10:10,160 --> 00:10:12,670
 suppose you're a meditation teacher, suppose my student

190
00:10:12,670 --> 00:10:13,320
 comes to me and

191
00:10:13,320 --> 00:10:17,040
 they're all upset and I start telling them jokes or trying

192
00:10:17,040 --> 00:10:18,120
 to divert their

193
00:10:18,120 --> 00:10:22,720
 attention because that's really what, well, that's one way

194
00:10:22,720 --> 00:10:23,760
 of using humor.

195
00:10:23,760 --> 00:10:27,480
 Suppose I bring up something humorous and make them laugh.

196
00:10:27,480 --> 00:10:28,520
 I've diverted their

197
00:10:28,520 --> 00:10:32,180
 attention. I've taken them away from the problem. And that

198
00:10:32,180 --> 00:10:34,320
's really the opposite

199
00:10:34,320 --> 00:10:37,290
 of what the Buddha would have us do. It's the opposite of

200
00:10:37,290 --> 00:10:38,640
 what I teach people to do,

201
00:10:38,640 --> 00:10:43,070
 which is to look at the problem and come to overcome the

202
00:10:43,070 --> 00:10:43,880
 fact that it's a

203
00:10:43,880 --> 00:10:50,220
 problem for us, to overcome the assumption in our mind that

204
00:10:50,220 --> 00:10:50,440
 there's a

205
00:10:50,440 --> 00:10:53,950
 problem, the disliking for the problem, the attachment to

206
00:10:53,950 --> 00:10:56,520
 the problem. Now if we

207
00:10:56,520 --> 00:11:03,960
 just divert our attention every time a problem arises, then

208
00:11:03,960 --> 00:11:04,480
 we're never

209
00:11:04,480 --> 00:11:08,550
 going to actually deal with it. I think another way of

210
00:11:08,550 --> 00:11:10,800
 using humor that might be

211
00:11:10,800 --> 00:11:14,310
 more in line with the Buddha's teaching is to come to see

212
00:11:14,310 --> 00:11:15,760
 the problem that you

213
00:11:15,760 --> 00:11:19,820
 have as humorous. If you come to see, you know, what a

214
00:11:19,820 --> 00:11:22,440
 stupid way of looking at it,

215
00:11:22,440 --> 00:11:26,740
 you know, what a silly thing that we've come to to see this

216
00:11:26,740 --> 00:11:28,520
 as a problem. If we

217
00:11:28,520 --> 00:11:37,840
 are, you know, all upset about the nature of things, we

218
00:11:37,840 --> 00:11:38,400
 have some,

219
00:11:38,400 --> 00:11:46,220
 someone sends us a very nice present or something and then

220
00:11:46,220 --> 00:11:46,920
 the ants get

221
00:11:46,920 --> 00:11:53,880
 added and we feel all upset. Being able to see that this is

222
00:11:53,880 --> 00:11:54,520
 just a silly

223
00:11:54,520 --> 00:11:57,200
 thing, there's no substance to it, it's not something

224
00:11:57,200 --> 00:11:58,960
 important. To be

225
00:11:58,960 --> 00:12:02,350
 able to look at the human condition and to look at our

226
00:12:02,350 --> 00:12:04,000
 problems and to find them

227
00:12:04,000 --> 00:12:09,640
 humorous. I think that's sort of what the Buddha came to

228
00:12:09,640 --> 00:12:10,880
 see and the sort of

229
00:12:10,880 --> 00:12:14,220
 things that the Buddha would smile at and the Arahants

230
00:12:14,220 --> 00:12:15,440
 would smile at. So I

231
00:12:15,440 --> 00:12:20,080
 think if the humor is allowing us to see things in a more

232
00:12:20,080 --> 00:12:22,360
 open-minded way and to

233
00:12:22,360 --> 00:12:27,710
 be more open-minded, then I think that could be beneficial

234
00:12:27,710 --> 00:12:29,200
 humor because it's

235
00:12:29,200 --> 00:12:34,120
 it's associated with wisdom. But if it's just humor for the

236
00:12:34,120 --> 00:12:36,080
 purpose of making

237
00:12:36,080 --> 00:12:41,120
 people laugh, the Buddha had a specific teaching, this man

238
00:12:41,120 --> 00:12:42,600
 who was a jester I

239
00:12:42,600 --> 00:12:46,460
 think and he was, his job was to make the king and the

240
00:12:46,460 --> 00:12:47,400
 queen and the court

241
00:12:47,400 --> 00:12:50,570
 laugh and so he would do all sorts of stupid things like

242
00:12:50,570 --> 00:12:51,440
 the three stujas or

243
00:12:51,440 --> 00:12:54,840
 whatever and he said I've heard that by doing this I'm

244
00:12:54,840 --> 00:12:56,000
 going to go to the

245
00:12:56,000 --> 00:13:00,500
 laughing, the heaven of laughter, this laughter, this

246
00:13:00,500 --> 00:13:02,080
 heaven that is called the

247
00:13:02,080 --> 00:13:05,420
 heaven of laughter when I die. Is that true? And the Buddha

248
00:13:05,420 --> 00:13:06,540
 said oh I'm sorry

249
00:13:06,540 --> 00:13:10,640
 don't ask me that question and this jester he asked him

250
00:13:10,640 --> 00:13:11,560
 again and again and

251
00:13:11,560 --> 00:13:14,340
 prompted again and again the Buddha said no you know the

252
00:13:14,340 --> 00:13:15,680
 truth is if you if

253
00:13:15,680 --> 00:13:19,260
 you're so caught up in diverting these people's attention

254
00:13:19,260 --> 00:13:20,480
 from what is serious

255
00:13:20,480 --> 00:13:29,340
 and just dedicated to making them laugh, you go to the hell

256
00:13:29,340 --> 00:13:30,960
 of laughter which

257
00:13:30,960 --> 00:13:36,390
 what that is I'm not sure but it's I believe it's a place

258
00:13:36,390 --> 00:13:38,600
 of torture and it's

259
00:13:38,600 --> 00:13:43,520
 just called the hell of laughter. And the point being that

260
00:13:43,520 --> 00:13:46,880
 it's we're wasting a

261
00:13:46,880 --> 00:13:52,270
 greater part of our life, our lives in useless humor. If it

262
00:13:52,270 --> 00:13:53,240
's humor that it's

263
00:13:53,240 --> 00:13:58,110
 slapstick or whatever that has no real benefit in terms of

264
00:13:58,110 --> 00:13:59,840
 opening the mind and

265
00:13:59,840 --> 00:14:04,920
 allowing you to see the true nature of reality then I would

266
00:14:04,920 --> 00:14:05,400
 say it's not

267
00:14:05,400 --> 00:14:10,070
 beneficial. So I guess that's it there there are two kinds

268
00:14:10,070 --> 00:14:11,040
 of humor that I can

269
00:14:11,040 --> 00:14:17,570
 see. One that is is siding with wisdom and one that is s

270
00:14:17,570 --> 00:14:19,240
iding with ignorance and

271
00:14:19,240 --> 00:14:24,100
 delusion and addiction and attachment really because the

272
00:14:24,100 --> 00:14:25,320
 point of it is simply

273
00:14:25,320 --> 00:14:32,680
 to bring about a state of pleasure in the mind which makes

274
00:14:32,680 --> 00:14:33,960
 it no better than

275
00:14:33,960 --> 00:14:38,040
 any drug out there. It's something that leads to addiction

276
00:14:38,040 --> 00:14:39,280
 and attachment and

277
00:14:39,280 --> 00:14:43,430
 ultimately you know people most people in the world this is

278
00:14:43,430 --> 00:14:44,760
 why they get bored

279
00:14:44,760 --> 00:14:47,110
 when they have to sit down and do meditation because it's

280
00:14:47,110 --> 00:14:47,680
 like it's not

281
00:14:47,680 --> 00:14:51,550
 exciting and they want to go and watch sitcoms or cartoons

282
00:14:51,550 --> 00:14:52,720
 or whatever it is

283
00:14:52,720 --> 00:14:57,400
 that people watch these days. So hope that helps, hope that

284
00:14:57,400 --> 00:14:59,160
's a proper answer.

285
00:14:59,160 --> 00:15:04,000
 Thanks for the question really a good one. All the best.

