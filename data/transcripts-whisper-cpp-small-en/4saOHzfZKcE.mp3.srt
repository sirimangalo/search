1
00:00:00,000 --> 00:00:08,570
 Ajahn Tong wakes up. I don't know how long he sleeps. I'm

2
00:00:08,570 --> 00:00:10,320
 told he sleeps three, four hours a day.

3
00:00:10,320 --> 00:00:16,560
 He's up at around three, I suppose. And what he's doing, I

4
00:00:16,560 --> 00:00:17,880
'm not sure because you're not allowed to

5
00:00:17,880 --> 00:00:20,550
 get close to him at that time, but there was a time where I

6
00:00:20,550 --> 00:00:22,980
 would meet him at his door. So I

7
00:00:22,980 --> 00:00:27,330
 would get up at 2am and go and meditate outside of his door

8
00:00:27,330 --> 00:00:30,880
. And at 4.30 he would come out and I

9
00:00:30,880 --> 00:00:34,380
 would walk with him to chanting. And was it 4.30 in the

10
00:00:34,380 --> 00:00:38,600
 morning he would go to chanting. And then he'd

11
00:00:38,600 --> 00:00:41,030
 give his talk and we do a group meditation, just 10 minutes

12
00:00:41,030 --> 00:00:44,920
 walking, 10 minutes sitting. But

13
00:00:44,920 --> 00:00:50,640
 everything he did was measured. There'd be times where he'd

14
00:00:50,640 --> 00:00:51,640
 be sitting there and

15
00:00:51,640 --> 00:00:54,920
 there'd suddenly be a whirlwind of activity around him. His

16
00:00:54,920 --> 00:00:56,320
 driver would come and say,

17
00:00:56,320 --> 00:01:01,030
 "Okay Ajahn, time to go." Walk in, walk out. Ajahn Tong

18
00:01:01,030 --> 00:01:04,920
 would sit there. Secretary would come in,

19
00:01:04,920 --> 00:01:08,640
 "Okay Ajahn, time to go. I'm getting ready." And he's so

20
00:01:08,640 --> 00:01:10,720
 clear he knows exactly what's going on.

21
00:01:10,720 --> 00:01:12,750
 And he doesn't have to listen to them because when the time

22
00:01:12,750 --> 00:01:13,960
 comes he'll get up and he'll go.

23
00:01:13,960 --> 00:01:17,760
 And then at the exact right time he looks up at the clock,

24
00:01:17,760 --> 00:01:21,120
 looks down, says to himself,

25
00:01:21,120 --> 00:01:25,980
 I'm assuming says, "Intending to stand, intending to stand,

26
00:01:25,980 --> 00:01:27,440
 noting his intention,

27
00:01:27,440 --> 00:01:31,340
 and then standing up, standing, standing." With perfect

28
00:01:31,340 --> 00:01:33,400
 measured calm. I've never seen

29
00:01:33,400 --> 00:01:37,550
 him off guard where he suddenly does something without

30
00:01:37,550 --> 00:01:39,960
 being aware, clearly aware of what's

31
00:01:39,960 --> 00:01:44,450
 going on. You can see it. There's no mistaking it. It's

32
00:01:44,450 --> 00:01:46,560
 something that is clearly visible in

33
00:01:46,560 --> 00:01:50,860
 everything he does, in how he eats, in how he walks, in how

34
00:01:50,860 --> 00:01:56,360
 he talks. I've come to him asking

35
00:01:56,360 --> 00:02:01,730
 a question about something fairly trivial, a naughty point

36
00:02:01,730 --> 00:02:03,960
 or something where I just wanted

37
00:02:03,960 --> 00:02:08,410
 an answer. And he'll give me like a thousand times what I

38
00:02:08,410 --> 00:02:11,440
 was looking for. He'll hit me to the heart

39
00:02:11,440 --> 00:02:15,230
 with something because he knows it's good for me, because

40
00:02:15,230 --> 00:02:18,280
 he knows what I need to hear. He wouldn't

41
00:02:18,280 --> 00:02:21,380
 just tell someone out of a desire for them to have

42
00:02:21,380 --> 00:02:24,280
 knowledge. He will always say it in a way that

43
00:02:24,280 --> 00:02:27,680
 helps the person because he sees clearly what is beneficial

44
00:02:27,680 --> 00:02:31,640
 to the person. I once wrote down in my

45
00:02:31,640 --> 00:02:34,260
 book, I would write everything he said down in a book or

46
00:02:34,260 --> 00:02:36,080
 everything I noticed, and I once wrote

47
00:02:36,080 --> 00:02:42,090
 down, "Everyone who comes here gets something good." And

48
00:02:42,090 --> 00:02:45,680
 that may not mean so much, but if you saw

49
00:02:45,680 --> 00:02:51,060
 the kind of people who came to see him, from politicians to

50
00:02:51,060 --> 00:02:56,360
 crazy old monks and nuns, village

51
00:02:56,360 --> 00:03:02,340
 people who had no interest in meditation, who wanted like

52
00:03:02,340 --> 00:03:05,640
 crazy things, monks coming to him from all

53
00:03:05,640 --> 00:03:10,900
 over the country and big monks, powerful monks, everyone

54
00:03:10,900 --> 00:03:14,400
 who came to him left with a smile or

55
00:03:14,400 --> 00:03:17,880
 left with, if they weren't smiling, they had been hit

56
00:03:17,880 --> 00:03:22,080
 really hard and they were still reeling in

57
00:03:22,080 --> 00:03:24,780
 surprise really. Because you go there expecting, "Yeah,

58
00:03:24,780 --> 00:03:26,520
 yeah, we'll just go and talk to him and

59
00:03:26,520 --> 00:03:31,360
 we'll get this or that." And it's a shock for many people

60
00:03:31,360 --> 00:03:34,000
 to get what you get from him.

61
00:03:34,000 --> 00:03:43,560
 He doesn't teach theory. He's got a surprising lack of

62
00:03:43,560 --> 00:03:51,720
 theoretical doctrine that he teaches.

63
00:03:51,720 --> 00:03:57,830
 Hardly ever hear him go on and detail the abhidhamma. I've

64
00:03:57,830 --> 00:04:00,360
 rarely ever heard him talk about the

65
00:04:00,360 --> 00:04:03,750
 abhidhamma, yet he tells people to learn it. His way of

66
00:04:03,750 --> 00:04:08,800
 teaching is, for that reason, people often

67
00:04:08,800 --> 00:04:15,710
 miss what he's talking about. People who are deep in study

68
00:04:15,710 --> 00:04:23,000
 and so on will often, in the beginning,

69
00:04:23,000 --> 00:04:27,010
 they feel like they haven't gotten what they came for.

70
00:04:27,010 --> 00:04:29,680
 Because they're coming looking for a technical

71
00:04:29,680 --> 00:04:32,890
 answer to their problem, a theoretical answer, and they don

72
00:04:32,890 --> 00:04:35,840
't get that. So he'll start talking about,

73
00:04:35,840 --> 00:04:40,910
 well not exactly, but some of the things he talks about, he

74
00:04:40,910 --> 00:04:44,480
 harps on about food. He says that you

75
00:04:44,480 --> 00:04:47,410
 have to know moderation in food. And for the longest time,

76
00:04:47,410 --> 00:04:49,140
 I thought this was really kind of

77
00:04:49,140 --> 00:04:53,610
 well mundane. Why aren't there more deep and profound

78
00:04:53,610 --> 00:04:56,320
 things to talk about? But he always

79
00:04:56,320 --> 00:05:02,970
 says how important moderation in eating is, for example. Of

80
00:05:02,970 --> 00:05:05,080
 course, I found out later, and I

81
00:05:05,080 --> 00:05:07,810
 thought about it later, and I realized that actually it was

82
00:05:07,810 --> 00:05:09,160
 probably partially because a lot

83
00:05:09,160 --> 00:05:13,340
 of the monks in the monastery weren't keeping the rules on

84
00:05:13,340 --> 00:05:17,520
 eating. And there was a lot of eating in

85
00:05:17,520 --> 00:05:21,600
 the evening and cooking in the evening, and a lot of fat

86
00:05:21,600 --> 00:05:25,020
 monks, some fat monks, and fat novices and so

87
00:05:25,020 --> 00:05:30,130
 on. It's hard to imagine how you can get fat on this diet,

88
00:05:30,130 --> 00:05:35,120
 but they have a different diet. But it

89
00:05:35,120 --> 00:05:38,100
 would be very simple, and not only the eating, but just

90
00:05:38,100 --> 00:05:41,880
 about everything. And often, the teachings

91
00:05:41,880 --> 00:05:47,150
 he'll give are just, there are things that you could learn

92
00:05:47,150 --> 00:05:49,920
 from the very basics of Buddhism.

93
00:05:49,920 --> 00:05:53,300
 They have these basic texts, and he'll go on about them. He

94
00:05:53,300 --> 00:05:56,600
'll talk about the very beginning teaching.

95
00:05:56,600 --> 00:06:00,370
 So say at the very beginning of the Vinaya. I even have

96
00:06:00,370 --> 00:06:02,520
 some of these things he said memorized,

97
00:06:02,520 --> 00:06:06,010
 because to me, they're just great to hear. "Klinton pah-wi-

98
00:06:06,010 --> 00:06:08,880
nay." He says at the very beginning of the

99
00:06:08,880 --> 00:06:13,010
 Vinaya, and in this he means the Vinaya as taught in

100
00:06:13,010 --> 00:06:15,200
 Thailand. They have this textbook,

101
00:06:15,200 --> 00:06:21,380
 "Sinti-kwan-sik-sa." The things that we should study. There

102
00:06:21,380 --> 00:06:26,840
's only three. But you see how profound it is

103
00:06:26,840 --> 00:06:31,660
 to reiterate this, because here we are talking about things

104
00:06:31,660 --> 00:06:35,280
 up in the sky, and he brings us down to

105
00:06:35,280 --> 00:06:43,940
 earth. "Sinti-lau-ja. Sinti-kwan-sik-sa. Misaam. Sin-sikha.

106
00:06:43,940 --> 00:06:51,200
 Sik-sa-nay-vung-sin." The morality, training and

107
00:06:51,200 --> 00:06:56,450
 morality. "Samadhi-sikha. Sik-sa-nay-lung-samadhi."

108
00:06:56,450 --> 00:07:01,120
 Training and moral in concentration or focus. And

109
00:07:01,120 --> 00:07:06,890
 "Panyasikha." Training and wisdom. And then he talks about,

110
00:07:06,890 --> 00:07:09,920
 I can't remember in that talk or in other

111
00:07:09,920 --> 00:07:12,730
 talks. Normally he'll talk about what is morality. Well,

112
00:07:12,730 --> 00:07:14,760
 morality is when you bring the mind back.

113
00:07:14,760 --> 00:07:19,260
 Keeping the mind from running away. Keeping the mind from

114
00:07:19,260 --> 00:07:23,040
 chasing after our desires. This is

115
00:07:23,040 --> 00:07:26,010
 morality. Because when you let your mind go, this is where

116
00:07:26,010 --> 00:07:29,800
 immorality comes from. So bringing it back,

117
00:07:29,800 --> 00:07:33,970
 what does that lead to? It leads to concentration. What is

118
00:07:33,970 --> 00:07:36,200
 concentration? Concentration is keeping

119
00:07:36,200 --> 00:07:40,200
 your mind on the object. Focusing on the object. What is

120
00:07:40,200 --> 00:07:43,960
 wisdom? Wisdom is knowing the object. In

121
00:07:43,960 --> 00:07:46,160
 fact, the funny thing that teachers will always say is that

122
00:07:46,160 --> 00:07:49,320
 wisdom is actually just, when you say

123
00:07:49,320 --> 00:07:52,380
 rising and falling to yourself, that's actually wisdom.

124
00:07:52,380 --> 00:07:54,800
 Funny, isn't it? Because it seems so stupid.

125
00:07:54,800 --> 00:07:58,220
 It's like, well, I know it's rising, right? The problem is

126
00:07:58,220 --> 00:08:00,400
 you really don't. Maybe for a second,

127
00:08:00,400 --> 00:08:03,270
 you experience the rising, but then you know, hey, my

128
00:08:03,270 --> 00:08:05,680
 stomach is rising. Isn't that great? Or,

129
00:08:05,680 --> 00:08:11,490
 oh, it's all stuck. It's not deep enough. It's not smooth

130
00:08:11,490 --> 00:08:15,280
 enough. And so on, and so on, and so on,

131
00:08:15,280 --> 00:08:19,270
 and so on. We don't really know it. When you know it, that

132
00:08:19,270 --> 00:08:21,560
's in a sense, "panya." Panya means to know

133
00:08:21,560 --> 00:08:25,480
 thoroughly. When you know thoroughly that this is rising,

134
00:08:25,480 --> 00:08:27,800
 that's wisdom, because that's dwelling in

135
00:08:27,800 --> 00:08:32,290
 the present moment. It's being with reality. And an Arahant

136
00:08:32,290 --> 00:08:34,720
 is an enlightened being is like that all

137
00:08:34,720 --> 00:08:40,780
 the time. They're aware of the reality at all times. Ajahn

138
00:08:40,780 --> 00:08:43,640
 Tong is a sort of no-nonsense teacher.

139
00:08:43,640 --> 00:08:48,330
 In fact, I would say the theory that you get from him is

140
00:08:48,330 --> 00:08:51,920
 not, there's not much, but it's incredibly

141
00:08:51,920 --> 00:08:57,130
 clear. I had teachers before Ajahn Tong, and I was able to

142
00:08:57,130 --> 00:09:00,080
 grasp what they were teaching,

143
00:09:00,080 --> 00:09:02,940
 but I couldn't make head or tail of it. I couldn't tell

144
00:09:02,940 --> 00:09:05,120
 what was the first step, what was the second

145
00:09:05,120 --> 00:09:08,760
 step. It seemed all jumbled and kind of ad hoc. For this

146
00:09:08,760 --> 00:09:11,160
 person, I'm going to say this thing based

147
00:09:11,160 --> 00:09:14,120
 on my experiences and so on. Ajahn Tong, when I started

148
00:09:14,120 --> 00:09:16,200
 practicing with him, everything was

149
00:09:16,200 --> 00:09:20,610
 crystal clear to the extent that I knew exactly how to

150
00:09:20,610 --> 00:09:23,640
 teach people, and I knew what steps to lead

151
00:09:23,640 --> 00:09:27,030
 people through from one day to the next. His teaching made

152
00:09:27,030 --> 00:09:29,080
 perfect sense and went in perfect

153
00:09:29,080 --> 00:09:34,300
 order. And really everything that he taught was like that,

154
00:09:34,300 --> 00:09:37,240
 or is like that. I'm assuming he's still

155
00:09:37,240 --> 00:09:40,510
 teaching. I haven't been in touch for a while. That's the

156
00:09:40,510 --> 00:09:42,680
 thing about being a penniless monkey.

157
00:09:42,680 --> 00:09:47,560
 You can't just fly and go see your teacher. So now here I

158
00:09:47,560 --> 00:09:50,560
 am in another country. Someday,

159
00:09:50,560 --> 00:09:53,130
 hopefully, I'll get back there. I should probably keep in

160
00:09:53,130 --> 00:09:56,760
 touch. Anyway, I assume that he's still

161
00:09:56,760 --> 00:10:01,680
 there teaching, and his teaching is just, everything is the

162
00:10:01,680 --> 00:10:05,680
 same. He will say the same thing a thousand

163
00:10:05,680 --> 00:10:08,730
 times without ever getting bored or ever having to alter it

164
00:10:08,730 --> 00:10:11,040
. He does alter it, and he does change,

165
00:10:11,040 --> 00:10:16,460
 but he's consistent. He's incredibly consistent, and

166
00:10:16,460 --> 00:10:22,400
 sometimes to the point where it's like he's

167
00:10:22,400 --> 00:10:25,830
 not trying to sell you himself. He's telling you the truth,

168
00:10:25,830 --> 00:10:27,760
 and he'll say it to you a thousand times.

169
00:10:27,760 --> 00:10:30,020
 He'll tell you it again and again and again, because there

170
00:10:30,020 --> 00:10:31,680
's nothing else. You're not going

171
00:10:31,680 --> 00:10:36,290
 to get him to change his way of teaching. He tells you the

172
00:10:36,290 --> 00:10:39,920
 truth, and he gives people very simple

173
00:10:39,920 --> 00:10:45,220
 teachings, and they won't get the theory out of it. They

174
00:10:45,220 --> 00:10:47,880
 won't get an incredible amount of theory,

175
00:10:47,880 --> 00:10:50,560
 and you won't hear him reciting long discourses or talking

176
00:10:50,560 --> 00:10:53,640
 about the Abhidhamma or so on. But he'll

177
00:10:53,640 --> 00:10:56,100
 give you what you need at that moment, and that's a

178
00:10:56,100 --> 00:10:58,680
 thousand percent clear. Anyone can vouch for that.

179
00:10:58,680 --> 00:11:01,500
 When they go to him, they get what they need, and it takes

180
00:11:01,500 --> 00:11:04,720
 them the next step. It's a lot like the

181
00:11:04,720 --> 00:11:09,270
 practice. When you practice meditation, if you haven't

182
00:11:09,270 --> 00:11:11,880
 practiced intensively, you might not have

183
00:11:11,880 --> 00:11:15,100
 felt this yet, but when you enter into the meditation

184
00:11:15,100 --> 00:11:17,720
 practice, you have a lot of expectations

185
00:11:17,720 --> 00:11:20,090
 of what you're going to get out of it, and those

186
00:11:20,090 --> 00:11:22,720
 expectations are, for the most part, blown out of

187
00:11:22,720 --> 00:11:26,050
 the water. The greatest part of meditation is realizing

188
00:11:26,050 --> 00:11:28,040
 that your purpose for meditating,

189
00:11:28,040 --> 00:11:34,190
 is wrong, and actually changing that, altering your path.

190
00:11:34,190 --> 00:11:38,480
 It's a very profound part of the practice.

191
00:11:38,480 --> 00:11:44,710
 It's not just learning more. It's learning what to look for

192
00:11:44,710 --> 00:11:48,480
, learning what to learn, and he does

193
00:11:48,480 --> 00:11:52,300
 that. He changes people. He doesn't just give people the

194
00:11:52,300 --> 00:11:55,120
 answers. He changes their questions.

195
00:11:55,120 --> 00:11:58,460
 He changes what they're looking for. He's got to be the

196
00:11:58,460 --> 00:12:00,600
 best teacher by far that I've ever met,

197
00:12:00,600 --> 00:12:04,320
 and maybe I have met and I just haven't learned from them,

198
00:12:04,320 --> 00:12:08,960
 but I think I've been fairly observant,

199
00:12:08,960 --> 00:12:11,010
 and I've watched a lot of different monks and a lot of

200
00:12:11,010 --> 00:12:13,320
 different teachers, and I've never met

201
00:12:13,320 --> 00:12:17,090
 anyone that I could say from my knowledge, from what I know

202
00:12:17,090 --> 00:12:21,160
 of them, compares to him. Just in

203
00:12:21,160 --> 00:12:26,940
 terms of his practice, I've only seen him get agitated once

204
00:12:26,940 --> 00:12:29,760
, and to me it was like a perfect

205
00:12:29,760 --> 00:12:32,860
 sign that he never gets agitated, because his way of

206
00:12:32,860 --> 00:12:35,360
 getting agitated was like to say something,

207
00:12:35,360 --> 00:12:40,220
 and then go back to total peace and calm. What it was is I

208
00:12:40,220 --> 00:12:43,040
 gave him these CDs of one of his teachers,

209
00:12:43,040 --> 00:12:47,630
 these really neat set of mp3 teachings. It was just like a

210
00:12:47,630 --> 00:12:50,040
 ton of teachings from one of his

211
00:12:50,040 --> 00:12:56,800
 teachers in Bangkok, who is very intellectual, and so has a

212
00:12:56,800 --> 00:13:00,360
 lot of long lengthy teachings,

213
00:13:00,360 --> 00:13:04,100
 but they're really good. Immediately he gets up and takes

214
00:13:04,100 --> 00:13:06,080
 me into his room and he wants to listen

215
00:13:06,080 --> 00:13:07,770
 to them, and they're like, "No, no, he's got no way to

216
00:13:07,770 --> 00:13:09,360
 listen to them," and I hooked him up and he

217
00:13:09,360 --> 00:13:11,990
 was able to listen, and they were very angry at me, because

218
00:13:11,990 --> 00:13:14,120
 they didn't want him to listen to the

219
00:13:14,120 --> 00:13:18,350
 people who take care of him, who I don't have much good to

220
00:13:18,350 --> 00:13:21,080
 say about, didn't want him to listen at

221
00:13:21,080 --> 00:13:23,060
 that time, and he wanted to listen, so there I was

222
00:13:23,060 --> 00:13:25,520
 listening. I got in real trouble for this. I was

223
00:13:25,520 --> 00:13:29,260
 yelled at and scolded, not knowing the right time, and so

224
00:13:29,260 --> 00:13:32,480
 on, but Ajahn Tong got to listen to the

225
00:13:32,480 --> 00:13:35,840
 Dhamma, so I was happy, and so I sat there, and they came

226
00:13:35,840 --> 00:13:38,480
 in and said, "Okay, Ajahn, time to go," and

227
00:13:38,480 --> 00:13:43,030
 this was his secretary, and Ajahn said, "Five minutes, give

228
00:13:43,030 --> 00:13:47,000
 me five minutes," and then he went out and

229
00:13:47,000 --> 00:13:50,320
 stormed out, boom, boom, boom, and then the driver comes in

230
00:13:50,320 --> 00:13:53,000
, "Okay, Ajahn, time to go," and Ajahn goes,

231
00:13:53,000 --> 00:13:56,970
 "One minute, can't I just have one minute?" and then he

232
00:13:56,970 --> 00:13:59,680
 said, "Please guys, they don't have a clue,"

233
00:13:59,680 --> 00:14:04,040
 and then he went back to listening, and we sat there and we

234
00:14:04,040 --> 00:14:06,240
 listened for a couple of minutes,

235
00:14:06,240 --> 00:14:08,790
 boom, boom, boom, storming in and out, these guys who were

236
00:14:08,790 --> 00:14:11,560
 totally furious, and then he said, "Okay,

237
00:14:11,560 --> 00:14:16,730
 thank you very much," and he got up and he went on, and so

238
00:14:16,730 --> 00:14:19,400
 I tell that not to give you a sign that

239
00:14:19,400 --> 00:14:22,980
 he, because I don't, I didn't, you know, it increased my

240
00:14:22,980 --> 00:14:24,920
 respect for him to see him do that.

241
00:14:24,920 --> 00:14:27,920
 He wasn't a pushover, but he certainly wasn't interested in

242
00:14:27,920 --> 00:14:29,520
 these guys. He was just like,

243
00:14:29,520 --> 00:14:33,480
 "I'm listening, one minute it was really, his whole body

244
00:14:33,480 --> 00:14:38,520
 shook, it was really funny." Yeah,

245
00:14:38,520 --> 00:14:43,360
 I really love him. He's a wonderful person. I think of him

246
00:14:43,360 --> 00:14:45,480
 as my father in the Dhamma,

247
00:14:45,480 --> 00:14:49,380
 or grandfather, maybe he's old enough. Okay, that's an

248
00:14:49,380 --> 00:14:52,160
 answer, hopefully, it's 15 minutes.

