1
00:00:00,000 --> 00:00:03,320
 Could you really relate to this question about the art, or

2
00:00:03,320 --> 00:00:04,480
 I don't feel like...

3
00:00:04,480 --> 00:00:08,480
 Okay, let's put this one in. Art, I don't know, I don't...

4
00:00:08,480 --> 00:00:11,740
 How art is interpreted in Buddhism? For me, art is a

5
00:00:11,740 --> 00:00:14,000
 subjective manifestation of the self.

6
00:00:14,000 --> 00:00:17,440
 But what about art in Buddhism?

7
00:00:17,440 --> 00:00:20,960
 Yeah, I wish to ask you the same question, actually.

8
00:00:20,960 --> 00:00:23,320
 Alright, well then let's answer it. You ask it, I'll answer

9
00:00:23,320 --> 00:00:25,760
 it. Go ahead, ask again.

10
00:00:25,760 --> 00:00:31,300
 Okay, slowly and carefully. How art... I'm gonna use this

11
00:00:31,300 --> 00:00:33,760
 same question.

12
00:00:33,760 --> 00:00:35,600
 That's the idea, go for it.

13
00:00:35,600 --> 00:00:41,260
 Yeah, how art is interpreted in Buddhism? For me, art is a

14
00:00:41,260 --> 00:00:43,840
 subjective manifestation of the self.

15
00:00:43,840 --> 00:00:47,360
 But what about art in Buddhism? Okay, and one more question

16
00:00:47,360 --> 00:00:49,200
. There is no self, right?

17
00:00:54,560 --> 00:00:59,220
 I don't know. It doesn't really apply. It's not a very good

18
00:00:59,220 --> 00:01:00,640
 question, that's all.

19
00:01:00,640 --> 00:01:07,030
 Experience is not self, and everything that arises is not

20
00:01:07,030 --> 00:01:10,320
 self. So you could say...

21
00:01:10,320 --> 00:01:15,110
 But there is no anything, entities are not a part of

22
00:01:15,110 --> 00:01:16,320
 reality.

23
00:01:16,320 --> 00:01:20,050
 Something arises, therefore it exists, or therefore it's

24
00:01:20,050 --> 00:01:20,640
 real.

25
00:01:20,640 --> 00:01:23,990
 I know what the author meant, really, because you have your

26
00:01:23,990 --> 00:01:26,960
 personality, and you have your expression.

27
00:01:26,960 --> 00:01:29,760
 Your work becomes your expression.

28
00:01:29,760 --> 00:01:35,040
 Yeah, so it's a manifestation of one's self, in that sense.

29
00:01:35,040 --> 00:01:39,520
 One's being, or one's state at that time.

30
00:01:39,520 --> 00:01:43,000
 And it can be... in that sense, it can show you things

31
00:01:43,000 --> 00:01:44,800
 about yourself that you didn't know,

32
00:01:44,800 --> 00:01:47,920
 and it can show people things that are far more profound

33
00:01:47,920 --> 00:01:50,000
 than just, say, looking in someone's eyes,

34
00:01:50,000 --> 00:01:54,020
 or watching someone's superficial behavior. Especially

35
00:01:54,020 --> 00:01:56,560
 since we put on such airs, right?

36
00:01:56,560 --> 00:01:58,530
 We all look like ordinary people, but who knows what's

37
00:01:58,530 --> 00:01:59,440
 going on in our mind?

38
00:01:59,440 --> 00:02:02,800
 Owen could be a serial killer for all we know.

39
00:02:02,800 --> 00:02:05,360
 On the Buddha.

40
00:02:05,360 --> 00:02:10,170
 Right. He can be a Buddha for all we know. We don't know

41
00:02:10,170 --> 00:02:12,560
 what's going on inside people's minds.

42
00:02:12,560 --> 00:02:16,480
 It depends on what you think art is, of course.

43
00:02:16,480 --> 00:02:22,670
 Art can bring out some of these inner workings. So

44
00:02:22,670 --> 00:02:24,800
 certainly I would give it that.

45
00:02:24,800 --> 00:02:29,920
 Expression, music as well.

46
00:02:29,920 --> 00:02:36,720
 But, you know, there's that other aspect of it that is the

47
00:02:36,720 --> 00:02:38,880
 addiction aspect, or the

48
00:02:40,080 --> 00:02:45,390
 attraction aspect. So you become addicted to these states.

49
00:02:45,390 --> 00:02:47,600
 The work itself is interesting,

50
00:02:47,600 --> 00:02:50,290
 especially from the point of view of a Buddhist or someone

51
00:02:50,290 --> 00:02:52,320
 who's looking objectively at it,

52
00:02:52,320 --> 00:02:55,030
 to see what's going on in someone's mind, for example.

53
00:02:55,030 --> 00:02:56,800
 Psychologists use art,

54
00:02:56,800 --> 00:03:00,360
 art therapy. My aunt is a leader in art therapy in the

55
00:03:00,360 --> 00:03:02,000
 world, apparently.

56
00:03:02,000 --> 00:03:07,560
 She's a PhD at Loyola Mary Mountain. She was one of the

57
00:03:07,560 --> 00:03:08,720
 pioneers in art therapy.

58
00:03:10,640 --> 00:03:14,570
 And so you can use it in that way. But the act of making

59
00:03:14,570 --> 00:03:19,120
 art, on the other hand, making music,

60
00:03:19,120 --> 00:03:22,420
 for example, but art in general, even if it's not just the

61
00:03:22,420 --> 00:03:24,400
 addiction to pleasure, as it is

62
00:03:24,400 --> 00:03:28,040
 generally in music, or the addiction to the strong emotion,

63
00:03:28,040 --> 00:03:29,920
 it's addiction to the states,

64
00:03:29,920 --> 00:03:32,810
 to addiction to self, to whatever it is that makes of

65
00:03:32,810 --> 00:03:35,280
 yourself. Whatever is going on inside,

66
00:03:35,840 --> 00:03:41,410
 you become intoxicated quite easily. And so, you know,

67
00:03:41,410 --> 00:03:44,160
 artists can be fairly egotistical and

68
00:03:44,160 --> 00:03:49,030
 self-absorbed, not necessarily in the way you think of it,

69
00:03:49,030 --> 00:03:51,760
 but very much interested in the self,

70
00:03:51,760 --> 00:03:54,100
 and interested in who they are, and I am this sort of

71
00:03:54,100 --> 00:03:56,000
 person, and I am that sort of person.

72
00:03:56,000 --> 00:03:59,920
 Maybe that's a gross generalization. Well, because all

73
00:03:59,920 --> 00:04:01,760
 artists looking for the answer,

74
00:04:01,760 --> 00:04:12,580
 who am I? They won't want to define themselves. And so, for

75
00:04:12,580 --> 00:04:15,920
 this person to clarify, because

76
00:04:15,920 --> 00:04:22,470
 this is not the start, the work you see. It's a mirror of

77
00:04:22,470 --> 00:04:25,040
 your mind. It's not yourself.

78
00:04:25,040 --> 00:04:28,850
 It's not your, there's no such thing as self. It's just

79
00:04:28,850 --> 00:04:30,080
 what you think, really.

80
00:04:30,080 --> 00:04:32,000
 So there's nothing wrong with the art itself, and it can be

81
00:04:32,000 --> 00:04:33,360
 interesting for other people, but

82
00:04:33,360 --> 00:04:42,320
 the act of creating art is suspect. Oh, yeah. So the act of

83
00:04:42,320 --> 00:04:45,440
 creating it's negative, isn't it?

84
00:04:45,440 --> 00:04:51,760
 Well, it's danger. It's subject to problems. I mean,

85
00:04:51,760 --> 00:04:53,840
 obviously taking a paintbrush, an elephant

86
00:04:53,840 --> 00:04:56,480
 can paint them. You know, in Thailand, the elephants create

87
00:04:56,480 --> 00:04:58,720
 pictures, make pictures of flowers

88
00:04:59,520 --> 00:05:03,170
 with their trunks. So there's nothing intrinsically wrong

89
00:05:03,170 --> 00:05:07,360
 with making art. But when it becomes art,

90
00:05:07,360 --> 00:05:09,750
 there's something there, right? You're putting something

91
00:05:09,750 --> 00:05:10,880
 into it. You're putting an

92
00:05:10,880 --> 00:05:14,290
 emotive state into it, or something into it, which is

93
00:05:14,290 --> 00:05:16,720
 generally based on addiction, attachment,

94
00:05:16,720 --> 00:05:24,070
 to either self, or emotion, and so on. There's the danger

95
00:05:24,070 --> 00:05:24,960
 there.

96
00:05:26,080 --> 00:05:28,640
 I mean, the other thing you might say is it's an inexact

97
00:05:28,640 --> 00:05:31,360
 science. You know, art is not

98
00:05:31,360 --> 00:05:36,700
 an objective. It's very, very subjective, right? So I'll

99
00:05:36,700 --> 00:05:39,040
 tell you, what do you think about using

100
00:05:39,040 --> 00:05:46,560
 art as a tool, and your subject, as a reality, you use

101
00:05:46,560 --> 00:05:49,840
 reality as a subject for your art to

102
00:05:49,840 --> 00:05:53,760
 investigate reality? I understand that. I just think it's

103
00:05:53,760 --> 00:05:55,760
 inexact. I mean, why not use mindfulness?

104
00:05:56,000 --> 00:05:59,340
 Much more reliable. That's what I was trying to get at just

105
00:05:59,340 --> 00:06:01,600
 now, is that the inexactitude of it

106
00:06:01,600 --> 00:06:04,350
 means that it's very easy to get off track. You don't

107
00:06:04,350 --> 00:06:06,560
 really know where it's leading. You can

108
00:06:06,560 --> 00:06:09,160
 be all touchy-feely about it, but you don't really know

109
00:06:09,160 --> 00:06:11,600
 what it's... and you get a sense of what it's

110
00:06:11,600 --> 00:06:16,540
 doing to you. But it's, you know, the word art is loaded

111
00:06:16,540 --> 00:06:20,160
 with subjectivity and inexactitude.

112
00:06:21,040 --> 00:06:26,870
 So to get some... to go in a specific direction, it's like

113
00:06:26,870 --> 00:06:32,000
 driving a car with poor alignment or

114
00:06:32,000 --> 00:06:35,910
 something. No, it's like... yeah, driving a car without

115
00:06:35,910 --> 00:06:37,840
 touching the steering wheel.

116
00:06:37,840 --> 00:06:41,560
 If you are driving a car with your knees or something, it's

117
00:06:41,560 --> 00:06:42,960
 not, you know, you're not

118
00:06:42,960 --> 00:06:47,450
 not very easy to stay on the road. Yeah, I understand that.

119
00:06:47,450 --> 00:06:49,120
 We don't have many examples

120
00:06:49,120 --> 00:06:54,470
 of art which is quite... no, seriously, we don't have much

121
00:06:54,470 --> 00:06:57,120
 example that we could say

122
00:06:57,120 --> 00:07:02,650
 it's actually beneficial. Well, they do use it for... you

123
00:07:02,650 --> 00:07:04,960
 know, psychologists use it and therapists use

124
00:07:04,960 --> 00:07:09,170
 it and they seem to have good results. But there was, you

125
00:07:09,170 --> 00:07:12,080
 know, good results is subjective as well.

126
00:07:12,080 --> 00:07:14,040
 What do you mean by good results? Do people become

127
00:07:14,040 --> 00:07:15,680
 enlightened making art? I would say no.

128
00:07:17,360 --> 00:07:21,680
 Or rarely, if they do. I haven't heard of it. But the

129
00:07:21,680 --> 00:07:24,720
 results they get is they come to

130
00:07:24,720 --> 00:07:28,800
 in tune with themselves and become, you know, if anything

131
00:07:28,800 --> 00:07:33,200
 become more self, more...

132
00:07:33,200 --> 00:07:38,560
 not centered, but self-assured, more... you know, people

133
00:07:38,560 --> 00:07:40,800
 are all on about being confident and so on.

134
00:07:40,800 --> 00:07:43,790
 Confident can be very dangerous, as we were talking about

135
00:07:43,790 --> 00:07:45,360
 earlier. Sometimes it's better to

136
00:07:45,360 --> 00:07:48,990
 not be sure of yourself, like Socrates, for example. Yeah,

137
00:07:48,990 --> 00:07:54,480
 that's right. Yeah. Someone says,

138
00:07:54,480 --> 00:07:57,280
 "Art is the root of artificial," which from Buddhist point

139
00:07:57,280 --> 00:07:58,960
 is not a very beneficial thing.

140
00:07:58,960 --> 00:08:04,000
 No, art is actually created in illusion, right? I would say

141
00:08:04,000 --> 00:08:04,880
 the art is more...

142
00:08:04,880 --> 00:08:09,840
 got much more to do. Not necessarily. It's like writing a

143
00:08:09,840 --> 00:08:12,160
 letter, for example. If I write a paper,

144
00:08:12,960 --> 00:08:15,450
 I could be talking about ultimate reality. If I make a

145
00:08:15,450 --> 00:08:17,200
 piece of art, I could be

146
00:08:17,200 --> 00:08:21,170
 expressing a feeling or a part of myself. It's a

147
00:08:21,170 --> 00:08:25,600
 communication, really. Art is communication.

148
00:08:25,600 --> 00:08:29,040
 It's a medium. Yeah, but expression is a matter of...

149
00:08:29,040 --> 00:08:33,360
 expression is a matter of your personal

150
00:08:33,360 --> 00:08:37,190
 feelings while you're gonna express you. Or you're gonna...

151
00:08:37,190 --> 00:08:41,520
 if you're gonna... is satisfaction gonna

152
00:08:41,520 --> 00:08:47,720
 express yourself, because, you know, like if you're looking

153
00:08:47,720 --> 00:08:49,520
 for satisfaction,

154
00:08:49,520 --> 00:08:58,320
 then that's one part of expression. The other is, like,

155
00:08:58,320 --> 00:09:00,000
 just to show your emotions,

156
00:09:00,000 --> 00:09:04,210
 like anger, that's another thing. Like, you know, if you're

157
00:09:04,210 --> 00:09:05,760
 angry or something, you're drawn,

158
00:09:05,760 --> 00:09:09,150
 you express yourself. Someone says you can show a

159
00:09:09,150 --> 00:09:13,760
 manifestation of the Buddha. In art, you can show

160
00:09:13,760 --> 00:09:16,580
 a manifestation of Buddha to help someone realize something

161
00:09:16,580 --> 00:09:19,760
. Yeah, that's... I think that's the point

162
00:09:19,760 --> 00:09:25,260
 I was making. It is a communication, but I'm skeptical,

163
00:09:25,260 --> 00:09:30,880
 because it's not really just a

164
00:09:30,880 --> 00:09:37,440
 communication. The medium is so overwhelming, because you

165
00:09:37,440 --> 00:09:40,640
've defined it as art. If you write,

166
00:09:40,640 --> 00:09:43,700
 you know, why aren't you using letters? Something that is

167
00:09:43,700 --> 00:09:45,840
 very exact and is communicating in meaning.

168
00:09:45,840 --> 00:09:53,120
 Art, the whole... the whole definition of art is to evoke

169
00:09:53,120 --> 00:09:56,240
 something beyond just

170
00:09:57,520 --> 00:10:00,780
 an understanding of what you're trying to say. If you want

171
00:10:00,780 --> 00:10:02,560
 to get the point across...

172
00:10:02,560 --> 00:10:09,530
 you know, I'm suspicious that inherent in art is some emot

173
00:10:09,530 --> 00:10:10,800
ive

174
00:10:10,800 --> 00:10:22,440
 intent to evoke a response, which, you know, I would tend

175
00:10:22,440 --> 00:10:24,480
 to think it's a negative thing,

176
00:10:25,120 --> 00:10:29,510
 as opposed to just evoke understanding. I'm skeptical that

177
00:10:29,510 --> 00:10:31,120
 you could look at a painting

178
00:10:31,120 --> 00:10:34,030
 and realize, "Wow, that's what the Buddha meant." No, I

179
00:10:34,030 --> 00:10:37,120
 think... I think just... I would say it could

180
00:10:37,120 --> 00:10:39,580
 no longer be called art, it would have to be called simply

181
00:10:39,580 --> 00:10:43,040
 communication. I don't think art is capable,

182
00:10:43,040 --> 00:10:48,480
 in my meaning of the word, of objective communication. But,

183
00:10:48,480 --> 00:10:50,880
 you know, I'm sure

184
00:10:50,880 --> 00:10:53,940
 many people disagree with it. Yeah, I don't know what kind

185
00:10:53,940 --> 00:10:55,760
 of art this person is talking about

186
00:10:55,760 --> 00:11:02,070
 in question. But, it really is art. It wouldn't be art if

187
00:11:02,070 --> 00:11:04,720
 it was really a communication.

188
00:11:04,720 --> 00:11:09,660
 No, I think it's everything but not communication, really.

189
00:11:09,660 --> 00:11:09,920
 I mean,

190
00:11:09,920 --> 00:11:13,680
 if you want... if you've got something to say, just say it.

191
00:11:16,240 --> 00:11:19,320
 What are you going to like draw off? I think the fact that

192
00:11:19,320 --> 00:11:22,000
 it becomes art is not a positive thing,

193
00:11:22,000 --> 00:11:26,080
 from a Buddhist point of view. But, no, there is... But if

194
00:11:26,080 --> 00:11:28,320
 you study reality, like

195
00:11:28,320 --> 00:11:32,730
 anatomy... Yeah, I mean, from an inside meditation point of

196
00:11:32,730 --> 00:11:33,280
 view, it's...

197
00:11:33,280 --> 00:11:36,550
 What you might say is that, you know, when you're dealing

198
00:11:36,550 --> 00:11:39,680
 with complex individuals who aren't

199
00:11:39,680 --> 00:11:44,380
 meditating yet, then sure, it can be incredibly beneficial

200
00:11:44,380 --> 00:11:46,960
 on a worldly level. Dealing with kids,

201
00:11:46,960 --> 00:11:50,900
 for example, who have been abused, you know, using art to

202
00:11:50,900 --> 00:11:53,440
 help them to cope with their emotions is

203
00:11:53,440 --> 00:11:57,380
 great. It's very helpful, very useful. So, I was just

204
00:11:57,380 --> 00:12:00,320
 thinking, technically, deep, deep down,

205
00:12:00,320 --> 00:12:04,670
 it's got some problem to it. But I think you'd have to

206
00:12:04,670 --> 00:12:06,400
 admit that

207
00:12:08,880 --> 00:12:12,000
 on, you know, even in the example of the Buddha image,

208
00:12:12,000 --> 00:12:13,840
 Buddha images are... could be considered

209
00:12:13,840 --> 00:12:16,720
 to be a type of art, or they're very closely related to, I

210
00:12:16,720 --> 00:12:19,200
 think, what we're talking about as art.

211
00:12:19,200 --> 00:12:22,060
 And when you see a Buddha image, when a meditator, for

212
00:12:22,060 --> 00:12:23,360
 example, sees a Buddha image,

213
00:12:23,360 --> 00:12:33,550
 it can be incredibly inspiring. And maybe that's not such a

214
00:12:33,550 --> 00:12:34,880
 good example, because I just...

215
00:12:34,880 --> 00:12:40,770
 Yeah, I wouldn't talk about how people react in art. It's

216
00:12:40,770 --> 00:12:42,560
 just not about what artists,

217
00:12:42,560 --> 00:12:45,480
 how we relate to his art. There's people, doesn't matter,

218
00:12:45,480 --> 00:12:46,880
 really. It's just not...

219
00:12:46,880 --> 00:12:55,010
 I mean, yeah, it's just... it's a marketing thing that you

220
00:12:55,010 --> 00:12:56,080
 go to museum.

221
00:12:56,080 --> 00:13:00,870
 Be careful, there's artists out there who are going to tear

222
00:13:00,870 --> 00:13:02,000
 you apart.

223
00:13:02,000 --> 00:13:06,320
 Of course! Everybody who knows the history of art, they

224
00:13:06,320 --> 00:13:10,800
 know that the church

225
00:13:10,800 --> 00:13:15,330
 founded most of the artists. Just they were making the art

226
00:13:15,330 --> 00:13:16,400
 for money.

227
00:13:16,400 --> 00:13:22,720
 Right? So that was just a marketing thing, really.

228
00:13:22,720 --> 00:13:23,520
 But in...

229
00:13:23,520 --> 00:13:26,820
 Artists will tell you they don't do it for the money, or

230
00:13:26,820 --> 00:13:27,680
 they do it...

231
00:13:27,680 --> 00:13:30,360
 No, we're talking about Buddhist artists here. We're asking

232
00:13:30,360 --> 00:13:31,520
 the question of

233
00:13:31,520 --> 00:13:37,280
 whether a Buddhist artist is actually capable of teaching

234
00:13:37,280 --> 00:13:39,840
 Buddhism through art, basically.

235
00:13:39,840 --> 00:13:46,640
 No, I think it's just a really bad concept.

236
00:13:46,640 --> 00:13:50,090
 Yeah, I think it's a bad idea. But what about for kids?

237
00:13:50,090 --> 00:13:51,520
 What about kids who have been abused?

238
00:13:51,520 --> 00:13:52,800
 How do you bring them to Buddhism?

239
00:13:52,800 --> 00:13:55,040
 Oh, maybe by cartoons, right?

240
00:13:55,040 --> 00:13:59,360
 Well, what's better about cartoons than art? Why isn't art

241
00:13:59,360 --> 00:13:59,760
 a valid

242
00:14:00,880 --> 00:14:02,800
 form of communication as well?

243
00:14:02,800 --> 00:14:05,360
 Yeah, I noticed that blocks downstairs were different.

244
00:14:05,360 --> 00:14:12,000
 Especially for the kids themselves. If they create art,

245
00:14:12,000 --> 00:14:13,360
 sometimes it helps them to

246
00:14:13,360 --> 00:14:17,440
 recognize what's... I mean, that's another thing. It can

247
00:14:17,440 --> 00:14:17,920
 help you to...

248
00:14:17,920 --> 00:14:19,120
 Yeah, yeah.

249
00:14:19,120 --> 00:14:28,720
 I remember writing poetry as a means of understanding what

250
00:14:28,720 --> 00:14:29,520
 was it...

251
00:14:29,520 --> 00:14:32,750
 Understanding my feelings on something. I just don't think

252
00:14:32,750 --> 00:14:34,160
 that in the long term,

253
00:14:34,160 --> 00:14:38,150
 in the past, to enlightenment, I think it is something that

254
00:14:38,150 --> 00:14:39,760
 you have to give up eventually.

255
00:14:39,760 --> 00:14:45,350
 But in the beginning, there's some... It can still be used,

256
00:14:45,350 --> 00:14:46,720
 especially for people who are

257
00:14:46,720 --> 00:14:50,060
 addicted to that, who are like children, who are attracted

258
00:14:50,060 --> 00:14:51,360
 to that sort of thing,

259
00:14:51,360 --> 00:14:54,800
 and who would react to it, and would be engaged in it. If

260
00:14:54,800 --> 00:14:56,240
 you've got kids to just sit down and

261
00:14:58,640 --> 00:15:01,680
 practice meditation, it's very difficult most of the time.

262
00:15:01,680 --> 00:15:04,320
 But if you have them paint a picture about

263
00:15:04,320 --> 00:15:08,320
 their feelings, for example, or whenever art therapists do,

264
00:15:08,320 --> 00:15:11,260
 you probably have them go a lot further to help them to

265
00:15:11,260 --> 00:15:13,120
 understand their feelings.

266
00:15:13,120 --> 00:15:14,320
 So I think...

267
00:15:14,320 --> 00:15:15,680
 Hi, I'm...

268
00:15:15,680 --> 00:15:16,480
 Yeah.

269
00:15:16,480 --> 00:15:20,960
 How do you think... What do you think if you are kind of

270
00:15:20,960 --> 00:15:24,720
 like creating art, let's say painting,

271
00:15:24,720 --> 00:15:27,830
 or drawing, whatever, and you kind of like... Your mind

272
00:15:27,830 --> 00:15:32,080
 comes into state of sort of concentration

273
00:15:32,080 --> 00:15:41,830
 and freedom. So how are you... Is it delusion? Is it

274
00:15:41,830 --> 00:15:45,520
 actually... Is it bad? Is it positive?

275
00:15:45,520 --> 00:15:50,640
 Sorry, is that possible?

276
00:15:51,920 --> 00:15:56,000
 Yeah, it's like people... You can describe the state when

277
00:15:56,000 --> 00:15:57,440
 you're creating your mind,

278
00:15:57,440 --> 00:16:05,970
 if you are creating an art, that you are in sort of a state

279
00:16:05,970 --> 00:16:11,920
 of freedom. You can express yourself,

280
00:16:11,920 --> 00:16:15,680
 right? So anything that's...

281
00:16:16,480 --> 00:16:20,370
 Expressing yourself is not the path. This is a fallacy of

282
00:16:20,370 --> 00:16:22,560
 modern psychology that we don't agree

283
00:16:22,560 --> 00:16:27,030
 with. Think of the basic principle of quantum physics, that

284
00:16:27,030 --> 00:16:31,120
... Or of the Heisenberg uncertainty

285
00:16:31,120 --> 00:16:34,590
 principle. When you act, you're already changing it. When

286
00:16:34,590 --> 00:16:36,800
 you express your feeling, you're already

287
00:16:36,800 --> 00:16:42,320
 changing that feeling, or you're creating that feeling. How

288
00:16:42,320 --> 00:16:43,200
 often... How do we know

289
00:16:43,200 --> 00:16:45,600
 that when we put together this piece of art based on our

290
00:16:45,600 --> 00:16:48,640
 feelings, we're not actually augmenting and

291
00:16:48,640 --> 00:16:55,690
 reinforcing and actually even molding and shaping that

292
00:16:55,690 --> 00:16:58,480
 feeling? How do we know that that's really

293
00:16:58,480 --> 00:17:03,590
 there? We don't, because it's not an exact investigation.

294
00:17:03,590 --> 00:17:06,240
 It's art. I mean, the word says

295
00:17:06,240 --> 00:17:10,850
 it all already. Yeah, I said it wrong with a question. It's

296
00:17:10,850 --> 00:17:13,200
 like maybe not expression.

297
00:17:13,200 --> 00:17:17,720
 It's something like you creating your mind. But what I

298
00:17:17,720 --> 00:17:23,920
 meant is that you are in sort of

299
00:17:23,920 --> 00:17:29,990
 harmony. You feel like you're in harmony. And what is it?

300
00:17:29,990 --> 00:17:32,800
 Is it delusion? I mean, I heard...

301
00:17:34,240 --> 00:17:37,840
 I experienced it myself. I heard about other people like...

302
00:17:37,840 --> 00:17:41,840
 If you kind of like... What they call it?

303
00:17:41,840 --> 00:17:47,390
 They call it some sort of... You know, that you have to

304
00:17:47,390 --> 00:17:50,240
 find the point in your life that's going to

305
00:17:50,240 --> 00:17:55,040
 give you some happiness or something like that. Why do you

306
00:17:55,040 --> 00:17:57,760
 think about it? Because I have...

307
00:17:57,760 --> 00:18:00,790
 You're talking about the harmony that comes from art? Do

308
00:18:00,790 --> 00:18:02,400
 people feel like they get something out of it?

309
00:18:02,400 --> 00:18:10,220
 Yeah, like the process of making art. The output is not

310
00:18:10,220 --> 00:18:16,240
 really... The process is important.

311
00:18:16,240 --> 00:18:19,520
 Well, but see, that's the thing. As soon as you say that,

312
00:18:19,520 --> 00:18:21,280
 you're placing a value judgment on an

313
00:18:21,280 --> 00:18:26,400
 experience. To me, that's the whole problem with art. The

314
00:18:26,400 --> 00:18:28,640
 word art to me is so loaded

315
00:18:28,640 --> 00:18:35,440
 with value judgment and subjectivity as opposed to seeing

316
00:18:35,440 --> 00:18:37,040
 experiences for what they are that I

317
00:18:37,040 --> 00:18:42,420
 can't help but feel like it's got its own inherent problems

318
00:18:42,420 --> 00:18:44,800
. If you say it can be useful as a tool,

319
00:18:44,800 --> 00:18:48,110
 well fine. Yeah, there can be benefit to it, but that's how

320
00:18:48,110 --> 00:18:49,920
 it is. Karma is not always white or

321
00:18:49,920 --> 00:18:53,890
 black. Sometimes it's white and black. So someone asks, "Do

322
00:18:53,890 --> 00:18:56,080
 you think we should all give up

323
00:18:56,080 --> 00:19:03,860
 all forms of art and music?" I mean, on a level of one to

324
00:19:03,860 --> 00:19:06,960
 ten, art and music are not very high

325
00:19:06,960 --> 00:19:11,080
 on the list of things that you have to give up. But if you

326
00:19:11,080 --> 00:19:13,280
 can, power to you. It's a wonderful,

327
00:19:13,280 --> 00:19:16,520
 liberating experience. It's an empowering thing to be able

328
00:19:16,520 --> 00:19:18,080
 to be free, to not need such

329
00:19:18,080 --> 00:19:23,680
 crutches. Most people, even when they hear that the kind of

330
00:19:23,680 --> 00:19:25,520
 things that we're saying,

331
00:19:27,120 --> 00:19:30,750
 become quite upset as a result. Their idea of what is

332
00:19:30,750 --> 00:19:34,800
 important is offended. The idea that music

333
00:19:34,800 --> 00:19:42,320
 might be unbeneficial can make people, can put people quite

334
00:19:42,320 --> 00:19:46,400
 upset, which of course is a sign

335
00:19:46,400 --> 00:19:53,020
 that there is attachment there, that there is a limiting

336
00:19:53,020 --> 00:19:56,640
 factor there or a crippling factor there.

337
00:19:56,640 --> 00:20:00,760
 Because when someone talks against the things that you like

338
00:20:00,760 --> 00:20:03,520
, you become upset. Now for a person

339
00:20:03,520 --> 00:20:07,160
 who doesn't need music to make them happy, it doesn't

340
00:20:07,160 --> 00:20:09,840
 really matter what people say about music.

341
00:20:09,840 --> 00:20:14,470
 People can say it's great or it's horrible. It doesn't

342
00:20:14,470 --> 00:20:17,440
 bother them. If you've given up music and

343
00:20:17,440 --> 00:20:19,950
 someone says music is a horrible, horrible thing, it's the

344
00:20:19,950 --> 00:20:22,320
 work of Satan, that doesn't bother you

345
00:20:22,320 --> 00:20:24,340
 because you don't have anything to do with it anymore. As

346
00:20:24,340 --> 00:20:26,720
 with everything else. So if you can

347
00:20:26,720 --> 00:20:29,190
 do it, it's great. When I finished my first meditation

348
00:20:29,190 --> 00:20:30,880
 course, one of the hardest things I

349
00:20:30,880 --> 00:20:34,800
 did was give up music. I had to stop playing the piano. No

350
00:20:34,800 --> 00:20:37,200
 more classical guitar. I had to give

351
00:20:37,200 --> 00:20:45,070
 away my Led Zeppelin CD collection. But I did it and it was

352
00:20:45,070 --> 00:20:49,760
 empowering. But certainly,

353
00:20:49,760 --> 00:20:54,320
 ask more important things. Can you give up sex? Can you

354
00:20:54,320 --> 00:20:57,520
 give up killing mosquitoes and insects?

355
00:20:57,520 --> 00:21:00,240
 Can you give up drinking alcohol and taking drugs? These

356
00:21:00,240 --> 00:21:01,920
 are more important questions.

357
00:21:03,520 --> 00:21:08,880
 Art and music are not such. Go for it. Not a big deal.

