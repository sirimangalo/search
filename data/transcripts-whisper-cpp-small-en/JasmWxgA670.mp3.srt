1
00:00:00,000 --> 00:00:26,000
 Good evening everyone.

2
00:00:26,000 --> 00:00:42,000
 Welcome to our evening dhamma.

3
00:00:42,000 --> 00:00:47,000
 I was sent a question today,

4
00:00:47,000 --> 00:00:59,000
 sent a question today by someone who lives in the area.

5
00:00:59,000 --> 00:01:02,000
 This internet thing, no?

6
00:01:02,000 --> 00:01:05,600
 Back in my day when you wanted to ask your teacher a

7
00:01:05,600 --> 00:01:06,000
 question,

8
00:01:06,000 --> 00:01:11,000
 you had to actually go see your teacher.

9
00:01:11,000 --> 00:01:16,000
 Now they just think they can Facebook me.

10
00:01:16,000 --> 00:01:20,000
 Not impressed.

11
00:01:20,000 --> 00:01:25,000
 I have to make the dhamma a little bit difficult.

12
00:01:25,000 --> 00:01:29,000
 You have to be, can't be too convenient.

13
00:01:29,000 --> 00:01:32,710
 You can't just pull out your phone and message your teacher

14
00:01:32,710 --> 00:01:33,000
.

15
00:01:33,000 --> 00:01:35,000
 That's not cool.

16
00:01:35,000 --> 00:01:37,000
 You can, I don't mind.

17
00:01:37,000 --> 00:01:40,000
 I'm just not going to answer.

18
00:01:40,000 --> 00:01:44,000
 Instead I'm going to come here and answer.

19
00:01:44,000 --> 00:01:47,000
 Use it to talk about the dhamma tonight.

20
00:01:47,000 --> 00:01:52,000
 That's an interesting question.

21
00:01:52,000 --> 00:01:55,000
 It's an interesting situation.

22
00:01:55,000 --> 00:02:07,000
 The situation is...

23
00:02:07,000 --> 00:02:12,000
 When we first learn about Buddhism,

24
00:02:12,000 --> 00:02:14,000
 it's quite simple, no?

25
00:02:14,000 --> 00:02:18,000
 It's quite a simple concept and some very simple truths,

26
00:02:18,000 --> 00:02:20,000
 some fairly simple practices.

27
00:02:20,000 --> 00:02:23,000
 It's not easy.

28
00:02:23,000 --> 00:02:26,900
 We undertake to practice meditation and we find it quite

29
00:02:26,900 --> 00:02:28,000
 difficult.

30
00:02:28,000 --> 00:02:32,250
 But we see the relation, the correlation between the

31
00:02:32,250 --> 00:02:35,000
 teachings and the practice.

32
00:02:35,000 --> 00:02:42,400
 Yes indeed as I practice I sort of start to realize the

33
00:02:42,400 --> 00:02:46,000
 truth of these teachings.

34
00:02:46,000 --> 00:02:52,030
 And I certainly am able to free myself up from some of the

35
00:02:52,030 --> 00:02:56,000
 bad habits that I've carried around.

36
00:02:56,000 --> 00:03:01,000
 I'm able to give up my addictions and so on.

37
00:03:01,000 --> 00:03:08,550
 And become quite confident and secure in the results that

38
00:03:08,550 --> 00:03:12,000
 you've gained from the practice.

39
00:03:12,000 --> 00:03:14,000
 This has changed me.

40
00:03:14,000 --> 00:03:18,650
 You might even start to think you're a little bit

41
00:03:18,650 --> 00:03:20,000
 enlightened.

42
00:03:20,000 --> 00:03:28,360
 And so for a while you think your life has changed,

43
00:03:28,360 --> 00:03:34,000
 meditation has changed your life.

44
00:03:34,000 --> 00:03:44,000
 And then after some time, short time, long time,

45
00:03:44,000 --> 00:03:47,000
 all of those things that you thought you'd freed yourself,

46
00:03:47,000 --> 00:03:53,000
 the changes you thought you'd made begin to unravel.

47
00:03:53,000 --> 00:03:57,670
 So suddenly you find yourself confronted by the same old

48
00:03:57,670 --> 00:03:59,000
 problems again.

49
00:03:59,000 --> 00:04:04,000
 Addiction, aversion, they start to come back

50
00:04:04,000 --> 00:04:20,000
 and they start to realize they were not really eradicated.

51
00:04:20,000 --> 00:04:26,520
 It's an interesting case because someone can practice quite

52
00:04:26,520 --> 00:04:28,000
 strenuously

53
00:04:28,000 --> 00:04:32,540
 and really change the state of their mind, purify the state

54
00:04:32,540 --> 00:04:33,000
 of their mind.

55
00:04:33,000 --> 00:04:44,650
 But without the depth of intensive practice or long term

56
00:04:44,650 --> 00:04:46,000
 practice,

57
00:04:46,000 --> 00:04:51,000
 most of the benefits one gains are temporary.

58
00:04:51,000 --> 00:04:57,000
 They're the suppression of the defilements.

59
00:04:57,000 --> 00:05:04,000
 So what it means is it's easy to become, to overestimate

60
00:05:04,000 --> 00:05:09,000
 the results one gains in the practice.

61
00:05:09,000 --> 00:05:13,480
 To overestimate yourself and your attainments and your

62
00:05:13,480 --> 00:05:17,000
 progress in the practice.

63
00:05:17,000 --> 00:05:21,000
 And so in the beginning, one meditator put it well recently

64
00:05:21,000 --> 00:05:24,000
 before he left.

65
00:05:24,000 --> 00:05:33,000
 It's like the tip of the iceberg.

66
00:05:33,000 --> 00:05:36,000
 When you first come, you only see the tip and you think,

67
00:05:36,000 --> 00:05:38,000
 "Oh, well that's quite big."

68
00:05:38,000 --> 00:05:42,000
 And then you're okay and you tackle this problem

69
00:05:42,000 --> 00:05:46,000
 and you conquer the tip of the iceberg thinking,

70
00:05:46,000 --> 00:05:49,000
 "Oh yes, this was a big thing deal."

71
00:05:49,000 --> 00:05:52,360
 And then once you conquer the tip of the iceberg, you look

72
00:05:52,360 --> 00:05:54,000
 under the water and you see,

73
00:05:54,000 --> 00:06:04,000
 "Oh, that was just the tip of the iceberg."

74
00:06:04,000 --> 00:06:07,580
 This is what the meditators here are realizing through

75
00:06:07,580 --> 00:06:14,000
 their intensive hours and hours a day.

76
00:06:14,000 --> 00:06:18,620
 Through the course they're tackling the iceberg and by the

77
00:06:18,620 --> 00:06:20,000
 end of the course,

78
00:06:20,000 --> 00:06:23,600
 they've finally reached the surface of the water and they

79
00:06:23,600 --> 00:06:32,000
 looked underneath and realized the magnitude of the problem

80
00:06:32,000 --> 00:06:32,000
.

81
00:06:32,000 --> 00:06:36,000
 Because who we are is just habits.

82
00:06:36,000 --> 00:06:39,000
 It's a simple thing to say and it sounds like it should be

83
00:06:39,000 --> 00:06:40,000
 easy to change,

84
00:06:40,000 --> 00:06:46,700
 but our habits are far more, far more, many orders of

85
00:06:46,700 --> 00:06:51,000
 magnitude greater than we're aware,

86
00:06:51,000 --> 00:06:57,640
 spanning into past lives and the mind is far more

87
00:06:57,640 --> 00:07:08,000
 complicated, complex than we originally understand.

88
00:07:08,000 --> 00:07:11,000
 And so in the beginning we have a simplistic understanding

89
00:07:11,000 --> 00:07:12,000
 of the problem,

90
00:07:12,000 --> 00:07:25,000
 of the depth of our neurotic behavior or mental problems.

91
00:07:25,000 --> 00:07:27,990
 And we're looking for always a quick fix and so we think

92
00:07:27,990 --> 00:07:30,000
 there should be some simple cure

93
00:07:30,000 --> 00:07:36,000
 and we're always hoping that it will go away soon.

94
00:07:36,000 --> 00:07:40,000
 And so in this slightest hint of it easing, we get excited

95
00:07:40,000 --> 00:07:43,000
 and we begin to think that maybe we've made a breakthrough

96
00:07:43,000 --> 00:07:51,000
 and now it's going to be gone and not come back.

97
00:07:51,000 --> 00:07:58,790
 I think this is a big, well a bit of a concern for people

98
00:07:58,790 --> 00:08:01,000
 meditating at home on a daily basis

99
00:08:01,000 --> 00:08:06,010
 who have never done intensive meditation practice is that

100
00:08:06,010 --> 00:08:09,000
 it's easy to lull yourself into a false sense of security

101
00:08:09,000 --> 00:08:12,490
 that you've somehow gained, "Oh I've been practicing for

102
00:08:12,490 --> 00:08:15,000
 years" or so on.

103
00:08:15,000 --> 00:08:19,900
 And it's not to trivialize that. Of course it's a great

104
00:08:19,900 --> 00:08:21,000
 thing to do daily practice

105
00:08:21,000 --> 00:08:28,000
 and you will realize all sorts of things, but

106
00:08:28,000 --> 00:08:33,380
 it's also very easy to lull yourself into a false sense of

107
00:08:33,380 --> 00:08:37,470
 security thinking that you've actually eradicated the

108
00:08:37,470 --> 00:08:38,000
 problems

109
00:08:38,000 --> 00:08:44,000
 and then be disappointed when they start to come back

110
00:08:44,000 --> 00:08:53,000
 or when you find out that they're still there.

111
00:08:53,000 --> 00:08:58,510
 Defilements serve three kinds of understanding, technical

112
00:08:58,510 --> 00:09:02,000
 understanding of how the mind works

113
00:09:02,000 --> 00:09:06,360
 or the types of defilements to sort of clearly lay out what

114
00:09:06,360 --> 00:09:08,000
 we mean by defilement.

115
00:09:08,000 --> 00:09:21,490
 Kilesa. We have the kamma kilesa, the actions, defiled

116
00:09:21,490 --> 00:09:23,000
 actions.

117
00:09:23,000 --> 00:09:31,090
 This is speech and bodily actions that is based on unwholes

118
00:09:31,090 --> 00:09:32,000
omeness.

119
00:09:32,000 --> 00:09:36,060
 This is the worst. This is when you think of what makes a

120
00:09:36,060 --> 00:09:40,000
 person evil or what makes a person do things that cause

121
00:09:40,000 --> 00:09:44,000
 suffering to themselves and others.

122
00:09:44,000 --> 00:09:47,580
 We think of the actions and the speech, the things they say

123
00:09:47,580 --> 00:09:50,000
 and the things they do.

124
00:09:50,000 --> 00:09:52,860
 This is how we judge people normally. It's very hard to

125
00:09:52,860 --> 00:09:57,000
 judge people based on their state of mind because

126
00:09:57,000 --> 00:10:02,010
 we're not privy to that. So instead we are able to see and

127
00:10:02,010 --> 00:10:08,000
 we pay attention to people's behavior.

128
00:10:08,000 --> 00:10:13,160
 So if we kill and steal and lie and cheat, this is the real

129
00:10:13,160 --> 00:10:18,000
 defilement. This is what defiles a person.

130
00:10:18,000 --> 00:10:25,880
 It's not by water that one becomes pure. It's not by

131
00:10:25,880 --> 00:10:28,000
 bathing.

132
00:10:28,000 --> 00:10:34,000
 One is pure according to one's actions and one's speech.

133
00:10:34,000 --> 00:10:39,500
 This is very real. I mean, these are a very real concern

134
00:10:39,500 --> 00:10:41,000
 for Buddhists.

135
00:10:41,000 --> 00:10:46,340
 And so most Buddhists are able to, to some extent, overcome

136
00:10:46,340 --> 00:10:50,000
 this sort of defilement fairly easily.

137
00:10:50,000 --> 00:10:53,030
 In fact, there are a lot of, I think, cultural Buddhists

138
00:10:53,030 --> 00:10:56,570
 who tend to think that they're following the Buddhist

139
00:10:56,570 --> 00:11:00,250
 teaching just because they don't kill and steal and lie and

140
00:11:00,250 --> 00:11:02,000
 cheat and take drugs and alcohol.

141
00:11:02,000 --> 00:11:06,100
 Of course, there are many Buddhists who aren't even able to

142
00:11:06,100 --> 00:11:09,000
 keep that yet call themselves Buddhists.

143
00:11:09,000 --> 00:11:13,210
 But it's not really enough. It's not really, I mean, this

144
00:11:13,210 --> 00:11:18,000
 was the support. These were supportive measures.

145
00:11:18,000 --> 00:11:23,570
 A person can keep all the five precepts and still not be

146
00:11:23,570 --> 00:11:28,000
 free from any sort of defilement.

147
00:11:28,000 --> 00:11:34,490
 And the potential to commit a moral activity to kill or

148
00:11:34,490 --> 00:11:40,000
 steal or lie or cheat is still always present.

149
00:11:40,000 --> 00:11:47,930
 It's a constant effort to remain vigilant and to refrain

150
00:11:47,930 --> 00:11:56,170
 from unwholesome acts and speech because the mind is still

151
00:11:56,170 --> 00:11:58,000
 defiled.

152
00:11:58,000 --> 00:12:04,620
 And so the second level of defilement is what's going on

153
00:12:04,620 --> 00:12:07,000
 inside the mind.

154
00:12:07,000 --> 00:12:14,950
 These are the five hindrances, liking and disliking, desire

155
00:12:14,950 --> 00:12:23,070
 and aversion, worry, restlessness, fear, doubt, confusion,

156
00:12:23,070 --> 00:12:27,000
 guilt.

157
00:12:27,000 --> 00:12:30,290
 Even feeling guilty about your defilement, this is more def

158
00:12:30,290 --> 00:12:39,010
ilement. It's one of the unwholesomeness is to worry about

159
00:12:39,010 --> 00:12:44,000
 the bad things you've done.

160
00:12:44,000 --> 00:12:49,670
 Now, this is yet more real than the first type because a

161
00:12:49,670 --> 00:12:55,410
 person can refrain and yet be full of anger and greed and

162
00:12:55,410 --> 00:13:01,000
 yet refrain from killing and stealing and so on.

163
00:13:01,000 --> 00:13:06,650
 Person can be sitting on a meditation mat with their eyes

164
00:13:06,650 --> 00:13:12,000
 closed and be full of greed and anger and delusion.

165
00:13:12,000 --> 00:13:15,340
 I'm sure our meditators here can relate at times. This is

166
00:13:15,340 --> 00:13:19,000
 the state. Our minds are full of these defilements.

167
00:13:19,000 --> 00:13:38,000
 This is what we wrestle with here in the center.

168
00:13:38,000 --> 00:13:42,110
 And so this is what we normally understand as defilements.

169
00:13:42,110 --> 00:13:45,000
 This is the arisen defilements.

170
00:13:45,000 --> 00:13:48,420
 And there's still a problem with that because when these

171
00:13:48,420 --> 00:13:51,860
 are gone, we think our minds are pure and we've become

172
00:13:51,860 --> 00:13:53,000
 enlightened.

173
00:13:53,000 --> 00:13:57,600
 There are cases where people enter into states that are

174
00:13:57,600 --> 00:14:01,360
 free from these and, as most of us do when we practice

175
00:14:01,360 --> 00:14:02,000
 meditation,

176
00:14:02,000 --> 00:14:07,130
 but then think to themselves that they've freed themselves

177
00:14:07,130 --> 00:14:09,000
 from defilement.

178
00:14:09,000 --> 00:14:11,950
 It's quite common in a meditation center to feel that way

179
00:14:11,950 --> 00:14:13,000
 to some extent.

180
00:14:13,000 --> 00:14:17,750
 You've done a long course in meditation and on the day you

181
00:14:17,750 --> 00:14:21,000
 leave, you feel quite different.

182
00:14:21,000 --> 00:14:30,000
 You feel like you have no greed, no anger, no delusion.

183
00:14:30,000 --> 00:14:33,820
 And it may be true that at that moment you don't. You're

184
00:14:33,820 --> 00:14:38,000
 very mindful and no greed, no anger.

185
00:14:38,000 --> 00:14:41,260
 But there's still something missing because, you see, when

186
00:14:41,260 --> 00:14:43,000
 you stay at a meditation center,

187
00:14:43,000 --> 00:14:46,000
 there's very little caused for greed, anger, and delusion.

188
00:14:46,000 --> 00:14:52,320
 Sure, on a basic level there's lots, but there's none of

189
00:14:52,320 --> 00:14:56,000
 the very strong instigators.

190
00:14:56,000 --> 00:15:03,000
 There's no angry people or attractive objects of desire.

191
00:15:03,000 --> 00:15:12,000
 There's no pressing needs and duties.

192
00:15:12,000 --> 00:15:14,730
 There's nothing to worry about. There's nothing to stress

193
00:15:14,730 --> 00:15:15,000
 over.

194
00:15:15,000 --> 00:15:21,520
 And so you become quite relaxed and have a feeling that

195
00:15:21,520 --> 00:15:23,000
 your mind is very pure

196
00:15:23,000 --> 00:15:26,800
 and your mind will be relatively quite pure just by staying

197
00:15:26,800 --> 00:15:28,000
 in a meditation center

198
00:15:28,000 --> 00:15:35,000
 because there's very little to make it impure.

199
00:15:35,000 --> 00:15:39,990
 You might start lusting over the food in the kitchen or

200
00:15:39,990 --> 00:15:47,000
 eyeing your bed with attraction or aversion.

201
00:15:47,000 --> 00:15:52,260
 You might get angry about the guy next door who snores or

202
00:15:52,260 --> 00:15:57,000
 who opens the door loudly or so on.

203
00:15:57,000 --> 00:16:00,990
 But there's very little of any significance to make you

204
00:16:00,990 --> 00:16:03,000
 really angry or greedy.

205
00:16:03,000 --> 00:16:07,160
 And so it's quite easy at times. There still be many defile

206
00:16:07,160 --> 00:16:08,000
ments that come up,

207
00:16:08,000 --> 00:16:13,520
 but at times it's easy to enter into a state that's free

208
00:16:13,520 --> 00:16:16,000
 from a risen defilement.

209
00:16:16,000 --> 00:16:20,360
 And so the final level, there is a third level and it's

210
00:16:20,360 --> 00:16:22,000
 called the Anu Saya.

211
00:16:22,000 --> 00:16:24,440
 And I'm sure many of you have heard of this and many of you

212
00:16:24,440 --> 00:16:27,000
 heard me talk about these levels.

213
00:16:27,000 --> 00:16:36,000
 But the Anu Saya or the Anu means under or Anu is a prefix.

214
00:16:36,000 --> 00:16:43,000
 Saya means to lie, to lie down or to rest.

215
00:16:43,000 --> 00:16:48,330
 So these are the underlying or the latent they say or the

216
00:16:48,330 --> 00:16:52,000
 proclivities, the tendencies.

217
00:16:52,000 --> 00:17:01,000
 The Anu Saya means the potential, potential for defilement.

218
00:17:01,000 --> 00:17:03,280
 And there are a whole list of these and in some places

219
00:17:03,280 --> 00:17:08,000
 there are five and in other places there are seven.

220
00:17:08,000 --> 00:17:12,410
 It's not really important what they are. I mean I can list

221
00:17:12,410 --> 00:17:14,510
 them, but you miss the point if you list them or if you

222
00:17:14,510 --> 00:17:16,000
 focus too much on the list.

223
00:17:16,000 --> 00:17:21,710
 There's kamara-ga which is desire for sensuality, there's

224
00:17:21,710 --> 00:17:24,000
 patinga which is aversion.

225
00:17:24,000 --> 00:17:29,330
 And these are the bases for greed, the base, the underlying

226
00:17:29,330 --> 00:17:34,000
 kernel, the root for greed and anger.

227
00:17:34,000 --> 00:17:39,290
 Then there's diti and mana, diti which is views, mano which

228
00:17:39,290 --> 00:17:44,000
 is conceit, dvichika which is doubt.

229
00:17:44,000 --> 00:17:49,720
 Bhawara-ga there's desire for becoming, so ambitions and so

230
00:17:49,720 --> 00:17:50,000
 on.

231
00:17:50,000 --> 00:17:54,000
 And avicya which is ignorance.

232
00:17:54,000 --> 00:17:59,210
 But none of these should be understood as the arisen defile

233
00:17:59,210 --> 00:18:00,000
ment.

234
00:18:00,000 --> 00:18:07,470
 It means it's the tendency for these and for all defile

235
00:18:07,470 --> 00:18:10,000
ments to arise.

236
00:18:10,000 --> 00:18:19,210
 It's the improper conception of reality, the ignorance, the

237
00:18:19,210 --> 00:18:23,000
 misunderstanding, the delusion.

238
00:18:23,000 --> 00:18:28,120
 That allows the potential for all of these things and

239
00:18:28,120 --> 00:18:30,000
 others to arise.

240
00:18:30,000 --> 00:18:32,000
 And this is what we're really getting at.

241
00:18:32,000 --> 00:18:34,740
 Meditation as I've said quite a bit recently is about

242
00:18:34,740 --> 00:18:38,480
 changing who you are, the very fundamental core of who you

243
00:18:38,480 --> 00:18:39,000
 are.

244
00:18:39,000 --> 00:18:43,250
 Much of who we are just has to be thrown out or discarded

245
00:18:43,250 --> 00:18:45,000
 or discounted anyway.

246
00:18:45,000 --> 00:18:53,000
 And trivialized, minimized, ignored, not given power.

247
00:18:53,000 --> 00:18:57,000
 We can no longer rely on who we are.

248
00:18:57,000 --> 00:19:00,350
 Doesn't mean we have to throw it all out just because it's

249
00:19:00,350 --> 00:19:04,000
 who we are or discard everything about ourselves.

250
00:19:04,000 --> 00:19:13,290
 But it all has to be subject to discrimination, to scrutiny

251
00:19:13,290 --> 00:19:14,000
.

252
00:19:14,000 --> 00:19:21,900
 We have to scrutinize every aspect of who we are and change

253
00:19:21,900 --> 00:19:24,000
 very much about who we are.

254
00:19:24,000 --> 00:19:32,440
 And this is how the Anu Saya, how our potentials, our

255
00:19:32,440 --> 00:19:37,000
 potential for defilement is uprooted.

256
00:19:37,000 --> 00:19:43,000
 This takes a lot more work and is a lot more difficult than

257
00:19:43,000 --> 00:19:49,240
 simply getting rid of the defilements in the mind which are

258
00:19:49,240 --> 00:19:51,000
 momentary.

259
00:19:51,000 --> 00:19:54,160
 So it's important to understand these three levels, to be

260
00:19:54,160 --> 00:19:57,180
 able to differentiate and to not be lulled into a false

261
00:19:57,180 --> 00:19:58,000
 sense of security.

262
00:19:58,000 --> 00:20:02,100
 Not aim only to stop greed and anger and delusion from

263
00:20:02,100 --> 00:20:06,210
 arising, but to aim to change who you are and to really

264
00:20:06,210 --> 00:20:14,810
 understand deeply the nature of our habits, the nature of

265
00:20:14,810 --> 00:20:16,000
 who we are.

266
00:20:16,000 --> 00:20:24,250
 Break it down and transform this being, whatever it is,

267
00:20:24,250 --> 00:20:32,890
 using positive habit forming, the formation of good habits

268
00:20:32,890 --> 00:20:37,000
 and wholesomeness in the mind.

269
00:20:37,000 --> 00:20:45,300
 The practice of charity, the practice of morality, the

270
00:20:45,300 --> 00:20:53,000
 practice of tranquility and insight meditation.

271
00:20:53,000 --> 00:20:56,000
 And this is the most difficult task, of course.

272
00:20:56,000 --> 00:20:59,750
 It doesn't end when you stop, when you achieve the state of

273
00:20:59,750 --> 00:21:06,180
 no greed, anger and delusion. It continues until you have

274
00:21:06,180 --> 00:21:12,110
 wisdom, until you have sufficient wisdom to prevent any

275
00:21:12,110 --> 00:21:17,000
 arising of further greed, anger and delusion.

276
00:21:17,000 --> 00:21:21,000
 And it's a big iceberg.

277
00:21:21,000 --> 00:21:28,000
 So, a little bit of thought on defilements.

278
00:21:28,000 --> 00:21:31,180
 And so this person's question was why these things come

279
00:21:31,180 --> 00:21:34,500
 back when they thought they were eradicated. Sort of, I

280
00:21:34,500 --> 00:21:37,350
 mean, it was a little bit different, I suppose. A little

281
00:21:37,350 --> 00:21:38,000
 bit specific.

282
00:21:38,000 --> 00:21:47,100
 But that's the thing. Don't rest content just because you

283
00:21:47,100 --> 00:21:51,000
're able to enter into meditative states.

284
00:21:51,000 --> 00:21:57,250
 You really have to change quite a bit fundamental aspects

285
00:21:57,250 --> 00:22:03,000
 of who we are. You have to dig deep.

286
00:22:03,000 --> 00:22:08,000
 So there you go. There's the Dhamma for tonight.

287
00:22:08,000 --> 00:22:21,000
 There are questions I'm happy to answer.

288
00:22:21,000 --> 00:22:48,000
 Okay, we got a few online questions here.

289
00:22:48,000 --> 00:22:51,360
 Does withstanding psychical discomfort, such as taking a

290
00:22:51,360 --> 00:22:54,000
 cold shower, aid in meditation practice?

291
00:22:54,000 --> 00:23:03,950
 No, because, not likely because it includes the intentional

292
00:23:03,950 --> 00:23:08,000
 causing of suffering.

293
00:23:08,000 --> 00:23:13,140
 And so, it's counterproductive because you're creating the

294
00:23:13,140 --> 00:23:17,000
 intention to cause suffering to yourself.

295
00:23:17,000 --> 00:23:22,110
 It's caused by impatience. Even asking this question is a

296
00:23:22,110 --> 00:23:26,430
 sign that you want to push the process along somehow, to

297
00:23:26,430 --> 00:23:28,000
 aid the process.

298
00:23:28,000 --> 00:23:30,440
 There's no aiding meditation practice. You're either med

299
00:23:30,440 --> 00:23:38,670
itate or you don't. And if you're doing something else, you

300
00:23:38,670 --> 00:23:43,000
're not meditating.

301
00:23:43,000 --> 00:23:47,540
 I was thinking of doing a mock retreat. I tried meditating

302
00:23:47,540 --> 00:23:51,000
 intensively, but it got very disturbed.

303
00:23:51,000 --> 00:23:59,000
 Well, you shouldn't really do intensive meditation.

304
00:23:59,000 --> 00:24:04,010
 You shouldn't really do intensive meditation without, as a

305
00:24:04,010 --> 00:24:10,000
 newcomer, without the support of a teacher.

306
00:24:10,000 --> 00:24:15,420
 It can be dangerous. Usually, as you say, you'll get

307
00:24:15,420 --> 00:24:18,000
 disturbed and you'll just stop.

308
00:24:18,000 --> 00:24:25,550
 That's the most common thing, but it's not particularly

309
00:24:25,550 --> 00:24:32,000
 beneficial without a teacher to support you.

310
00:24:32,000 --> 00:24:35,300
 I mean, as a beginner, because you're trying to go through

311
00:24:35,300 --> 00:24:39,800
 a forest and you've never been in the forest. You're not

312
00:24:39,800 --> 00:24:49,000
 familiar with the landscape, let alone knowing the way out.

313
00:24:49,000 --> 00:24:54,080
 Once you've gone through intensive meditation with a

314
00:24:54,080 --> 00:24:59,590
 teacher, you have a much better layout of the land and then

315
00:24:59,590 --> 00:25:03,000
 it's easy to go through it again.

316
00:25:03,000 --> 00:25:07,270
 What do you think about an approach of avoiding bad and

317
00:25:07,270 --> 00:25:08,000
 good?

318
00:25:08,000 --> 00:25:18,000
 The good will come because of the effort to avoid the bad.

319
00:25:18,000 --> 00:25:20,000
 A good idea can be a similar approach. Focus.

320
00:25:20,000 --> 00:25:23,700
 It seems that when I focus on avoiding bad, good comes out

321
00:25:23,700 --> 00:25:25,000
 automatically.

322
00:25:25,000 --> 00:25:32,680
 Speaking of tonight's talk, this is an example of a way to

323
00:25:32,680 --> 00:25:38,630
 free your mind, potentially, temporarily, of defilement so

324
00:25:38,630 --> 00:25:40,000
 you can avoid bad.

325
00:25:40,000 --> 00:25:44,000
 But it's a good example of what won't work in the long run,

326
00:25:44,000 --> 00:25:47,000
 to actually change your habits because it's avoiding.

327
00:25:47,000 --> 00:25:54,370
 Avoiding isn't sustainable. You have to work and work. It's

328
00:25:54,370 --> 00:25:59,000
 sankhara dukha. You have to work and work to avoid the evil

329
00:25:59,000 --> 00:25:59,000
.

330
00:25:59,000 --> 00:26:03,350
 And all you do is build the habit of avoiding. And then

331
00:26:03,350 --> 00:26:13,000
 when you stop, whatever you do to avoid unwholesomeness,

332
00:26:13,000 --> 00:26:16,100
 it's eventually going to come to an end and then it's going

333
00:26:16,100 --> 00:26:18,390
 to come back. So here's a good example of this person who

334
00:26:18,390 --> 00:26:20,000
 asked me this question.

335
00:26:20,000 --> 00:26:24,000
 Not the question just now, but the earlier question.

336
00:26:24,000 --> 00:26:27,740
 They were doing what they could to avoid engaging in

337
00:26:27,740 --> 00:26:34,590
 certain addictive activities and then eventually they just

338
00:26:34,590 --> 00:26:37,000
 lost their...

339
00:26:37,000 --> 00:26:42,020
 the effort that it took to stop and suddenly they're

340
00:26:42,020 --> 00:26:46,000
 engaging in those old activities again.

341
00:26:46,000 --> 00:26:49,130
 Avoiding is not the answer. Avoiding is not the way out of

342
00:26:49,130 --> 00:26:50,000
 suffering.

343
00:26:50,000 --> 00:26:54,580
 The Four Noble Truths, that's what the Four Noble Truths,

344
00:26:54,580 --> 00:26:57,000
 they answer this question.

345
00:26:57,000 --> 00:27:05,000
 Suffering is to be fully understood. That is the path.

346
00:27:05,000 --> 00:27:08,250
 And when you fully understand suffering, then you don't

347
00:27:08,250 --> 00:27:11,000
 have to avoid craving or unwholesomeness.

348
00:27:11,000 --> 00:27:26,000
 You abandon it as a matter of course, naturally.

349
00:27:26,000 --> 00:27:29,000
 And that's all our questions then.

350
00:27:29,000 --> 00:27:36,360
 So thank you again for coming out. Wishing you all a good

351
00:27:36,360 --> 00:27:37,000
 night.

352
00:27:38,000 --> 00:27:39,000
 Thank you.

353
00:27:40,000 --> 00:27:41,000
 Thank you.

354
00:27:41,000 --> 00:27:42,000
 Thank you.

355
00:27:44,000 --> 00:27:45,000
 Thank you.

