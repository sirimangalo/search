 Okay, good evening everyone.
 I guess there's no one watching yet, but you'll see this in
 the recording as well.
 So again, we're looking at the relationship between
 Buddhism and nature.
 And so the second way of looking at nature from a Buddhist
 perspective is I think in
 terms of what we say in English, the nature of things or
 human nature or even just things
 as having a nature, as having an existence, a presence.
 And so I'll explain what I mean by that one.
 It's maybe a little hard to understand.
 But the nature of things, the nature of things relates to
 our understanding of things being
 stable, being constant, predictable, dependable, static.
 So what is the nature of things?
 Physics will tell you things about the nature of things and
 so on.
 Buddhism has much to say.
 But we go far beyond that is I think an issue that we have
 to face and understand in Buddhism,
 is that we take well nature.
 And again, the inspiration of these few talks was the
 concern for our environment, for the
 climate, for nature as a concept, as a thing to be
 protected and concerned about, and the
 thing that is being destroyed by human greed and corruption
.
 By us, we're destroying nature in many ways.
 But that's, so another thing that we have to look at in
 that regard is that being a
 part of nature.
 The nature of things is to be changeable, is that this
 concept of nature, the trees,
 the mountains, the weather, the air, the water, is all mal
leable.
 It doesn't have a nature.
 If anyone asks you about nature, when you go out into the
 forests or to the seaside
 and you talk about how peaceful and how calming and how
 wonderful nature is, that kind of
 nature, this nature as a thing as opposed to the human, is
 just a concept in our minds
 and the reality is changing.
 Reality is susceptible to change.
 There's no such thing as the nature of things in that sense
.
 Our ideas of the nature of things give us a sense of
 stability that is illusory, that
 isn't there.
 I mean, part of the suffering from climate change is going
 to be the simple change, having
 to deal with change.
 And sometimes the change is so drastic that it leads to
 direct suffering, flooding, wildfires,
 tornadoes, simple drought and loss of livelihood and so on.
 But in general, speaking in general, our illusions about
 the nature of things, things having
 a nature, is a cause for great suffering, needless
 suffering.
 Whereas if we could see the changing nature of things.
 Human nature as well, I mentioned that.
 It's important to talk about because we think of human
 nature as being stable, static.
 I'm an angry person.
 I'm a depressed person.
 What did I read about depression?
 No, one of our professors or someone in the university
 recently said that there's only
 a 25% difference between the effects of medication and the
 effects of a placebo for people with
 moderate or mild depression.
 So we talk about being depressed.
 I have depression and we think of it as an illness, as a
 disease, as something to be
 treated with medication due to what we call a chemical
 imbalance.
 Chemical imbalance is something static.
 You can't wish it away, right?
 It's a part of who you are.
 And yet we find that these medications that are meant to
 change that chemical imbalance
 are not nearly as effective as we would think.
 Or they're not much more effective than in some sense just
 wishing it away.
 No, the placebo effect.
 The effects of meditation would be quite similar, I think,
 to the placebo effect in terms of
 changing the state of mind.
 The human nature is knowledgeable.
 Someone else recently said that up until recently we
 thought the mind was static, that after
 seven years old the brain didn't change.
 Of course we know that's false.
 You adult brains can change are malleable.
 And so calling it a disease and thinking of it as a part of
 you, a part of who you are
 is incorrect.
 Who you are, your human nature is malleable, is changeable,
 is impermanent.
 And even our physical nature is impermanent.
 This body is just a shell or a husk or like a sand castle
 that we build up.
 Think of those nine months in the womb that we spend knead
ing and molding and tweaking.
 No, not of course.
 You can't say that the mind created to feed us.
 But certainly had a, played a part those nine months in
 tweaking it.
 And of course throughout life tweaking as well, changing.
 And this physical form is also temporary.
 So the true nature of things.
 The question is, you know, is there a true nature of things
?
 Is there something that is stable and constant?
 And there is something that is, you can say is always the
 nature of things.
 And the nature of things is to change.
 And that's more than just a clever thing to say because of
 course it's basically saying
 that the nature of things is to not have a constant nature,
 right?
 It's more than that.
 Because understanding of impermanence, understanding of the
 unpredictable nature of things is a
 stable state.
 The understanding of it.
 When you understand that law of nature, that aspect of
 nature, it's a very powerful thing.
 It changes the way you look at the world.
 It changes the way you interact with the world.
 It allows you to let go, to be free from the suffering that
 comes from change, right?
 So it is an important, stable reality to understand
 thoroughly so that you gain the stability
 of understanding.
 The stability that comes from being able to adapt.
 The stability of flexibility in a sense.
 Understanding impermanence, understanding suffering.
 The suffering that comes from clinging to things, right?
 If everything is changing then clinging to stability,
 clinging to something.
 It's like clinging to a raft in the fast moving river.
 It just gets swept away.
 Clinging to, clinging to a sinking ship.
 Something about being flexible is not clinging.
 Flexibility implies, or the adaptability or the
 understanding of impermanence implies a
 lack of attachment.
 So we hear about non-attachment in Buddhism and not
 clinging and not wanting or craving
 even liking anything.
 And yes it implies that.
 It implies that happiness can't come from clinging or
 craving, holding onto anything,
 liking anything.
 It can't come from things.
 Your happiness has to be independent of things.
 Things whose only nature is to change.
 Understanding of non-self that you can't control things.
 You can't stop this process.
 The process of change is not me, it's not mine.
 In fact, in fact our struggle to be satisfied, to find
 pleasure changes things themselves,
 changes things, creates impermanence.
 Not only does it create suffering because of change, but it
 creates change.
 I'm thinking now again back to the daily environment, as I
 mentioned in the last video.
 Our craving, our clinging.
 Think of an addict, an addict gains pleasure from a drug,
 which of course changes the brain,
 makes it harder to get pleasure from the same drug and
 leads to greater and greater addiction.
 This is true with so many things.
 I mean it's true with any addiction to even simple things
 like food, television, music.
 The brain is elastic and you change the brain.
 Not only do we change our brains, we change our
 environments.
 Our incessant drive to find pleasure and satisfaction is
 changing the world around us.
 It's destroying the environment.
 Even our consumerism, our consumption of water, our
 consumption of oil, plastic, all of these
 things.
 That's why I think it's fair to say Buddhists should be
 environmentally conscious in terms
 of understanding the relationship between our greed and our
 suffering.
 The suffering caused by an environment that's degraded.
 Our clinging, our craving, even our control, our intent to
 drive, to control, to cling,
 to maintain, cling to stability.
 All of that is creating destruction, it's creating stress,
 it creates systems that oppress
 other beings, oppress other humans.
 We create suffering because we don't see the nature of
 things.
 We don't see that our behavior causes suffering.
 There is a nature of things.
 It's quite simple in that way.
 That's with impermanence, really, is the cornerstone of it
 all.
 Being impermanent, nothing is satisfying.
 You can't find satisfaction or happiness in things.
 And that being so, they are not me, they are not mine, they
 are not under my control.
 The idea of self is just a delusion, the idea of me and
 mine are a trap that leads us to
 only greater suffering.
 So there you go, more on the nature of things, more on
 Buddhism and nature.
 One more video, one more aspect of nature, I think.
 And then we'll be done with that.
 And I'm not making videos that often, I don't know when the
 next one will be.
 And I expect that in the new year I'll be making more
 videos, maybe even next month,
 but this month is somewhat occupied with other things.
 Thank you all, have a good night.
 Thank you all for tuning in, 43 people, spur of the moment
 on a Wednesday night.
 I very much appreciate it.
 So have a good night, wish you all the best.
 Have a good night, wish you all the best.
