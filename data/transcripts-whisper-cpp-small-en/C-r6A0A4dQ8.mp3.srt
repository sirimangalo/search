1
00:00:00,000 --> 00:00:04,510
 Hi, today I'd like to talk to you all about a project that

2
00:00:04,510 --> 00:00:07,000
 we've begun here in Ontario,

3
00:00:07,000 --> 00:00:12,390
 Canada. As many of you may know, I've returned to Canada

4
00:00:12,390 --> 00:00:15,640
 with the intention of continuing

5
00:00:15,640 --> 00:00:22,190
 to spread the Dhamma here in my own country. Reasons for

6
00:00:22,190 --> 00:00:24,600
 that include the fact that it's

7
00:00:24,600 --> 00:00:29,290
 easier and more convenient for me in a place where first of

8
00:00:29,290 --> 00:00:32,160
 all I own a passport, where

9
00:00:32,160 --> 00:00:35,620
 I don't have to worry about visas, and also where the

10
00:00:35,620 --> 00:00:37,840
 culture is familiar and where I'm

11
00:00:37,840 --> 00:00:45,240
 able to sort of integrate with the society. So that was the

12
00:00:45,240 --> 00:00:46,720
 idea and I've been back in

13
00:00:46,720 --> 00:00:51,610
 Canada for a while now and we've decided I've got a few

14
00:00:51,610 --> 00:00:54,400
 people here who have gotten into

15
00:00:54,400 --> 00:01:00,160
 helping so we've decided to take it to the next level. Our

16
00:01:00,160 --> 00:01:06,040
 goal is to move into a house

17
00:01:06,040 --> 00:01:13,730
 of our own nearby to the university here in Hamilton,

18
00:01:13,730 --> 00:01:16,520
 Ontario. In September I'll be going

19
00:01:16,520 --> 00:01:21,340
 back to university and part of the reason for that is to be

20
00:01:21,340 --> 00:01:23,840
 closer to the sort of people

21
00:01:23,840 --> 00:01:27,280
 who would be interested in the things that we do because

22
00:01:27,280 --> 00:01:29,160
 one of the things that I found

23
00:01:29,160 --> 00:01:35,730
 difficult is actually getting involved or getting into back

24
00:01:35,730 --> 00:01:38,400
 into the society here, back

25
00:01:38,400 --> 00:01:43,490
 in with fellow Buddhists because of our location. Right now

26
00:01:43,490 --> 00:01:46,340
 I'm in an area where there are not

27
00:01:46,340 --> 00:01:51,620
 so many people and certainly not so many Buddhists. And so

28
00:01:51,620 --> 00:01:54,640
 by going back to university the idea

29
00:01:54,640 --> 00:01:58,890
 is to get back into the swing of things. I'm going to be

30
00:01:58,890 --> 00:02:01,600
 involved with the religious studies

31
00:02:01,600 --> 00:02:06,400
 department there and we've applied to start a Buddhism

32
00:02:06,400 --> 00:02:09,320
 society. The idea will be that

33
00:02:09,320 --> 00:02:13,470
 there will be more of a local community here interested in

34
00:02:13,470 --> 00:02:15,820
 the meditation practice. I know

35
00:02:15,820 --> 00:02:19,700
 I've got a lot of people following on the internet but the

36
00:02:19,700 --> 00:02:22,080
 difficulty with that is arranging

37
00:02:22,080 --> 00:02:27,980
 things like food for meditators and the lodging and sort of

38
00:02:27,980 --> 00:02:31,840
 local coordination even just things

39
00:02:31,840 --> 00:02:35,650
 like recording videos. I mostly have to do by myself

40
00:02:35,650 --> 00:02:37,880
 because there's no one here who

41
00:02:37,880 --> 00:02:45,810
 is of a mind to help. So the goal is that in January I'll

42
00:02:45,810 --> 00:02:48,440
 be halfway done the year at McMaster

43
00:02:48,440 --> 00:02:53,890
 University. By January 1st we're going to try to rent a

44
00:02:53,890 --> 00:02:56,800
 house near the university. There's

45
00:02:56,800 --> 00:02:59,990
 a lot of student housing available and so we'll find a

46
00:02:59,990 --> 00:03:02,000
 house that's not too expensive

47
00:03:02,000 --> 00:03:07,410
 and begin by renting and we will establish that as our

48
00:03:07,410 --> 00:03:11,360
 meditation center and monastery.

49
00:03:11,360 --> 00:03:17,380
 And it will be a place that we can run and use to set up a

50
00:03:17,380 --> 00:03:20,520
 studio. Right now I'm in a

51
00:03:20,520 --> 00:03:25,020
 single room here that's what I've got. But we would be able

52
00:03:25,020 --> 00:03:26,760
 to set up a studio and have

53
00:03:26,760 --> 00:03:31,070
 someone from our new Buddhism Society at McMaster help out

54
00:03:31,070 --> 00:03:33,800
 with recording videos, maybe even

55
00:03:33,800 --> 00:03:39,240
 have someone move in to take care of the place. But so the

56
00:03:39,240 --> 00:03:41,400
 reason for making this video is

57
00:03:41,400 --> 00:03:46,000
 that to let you know about the project and how it's being

58
00:03:46,000 --> 00:03:48,760
 run by some very kind and devoted

59
00:03:48,760 --> 00:03:52,770
 people who are interested in making this happen. So there's

60
00:03:52,770 --> 00:03:56,680
 information on one of the crowdsourcing

61
00:03:56,680 --> 00:04:00,700
 websites where this video is going to be placed and they're

62
00:04:00,700 --> 00:04:03,040
 looking for support. If you would

63
00:04:03,040 --> 00:04:09,050
 like to support this project please get in contact with the

64
00:04:09,050 --> 00:04:11,440
 organizers and help out,

65
00:04:11,440 --> 00:04:14,310
 help us to make this possible. It's most likely going to

66
00:04:14,310 --> 00:04:15,920
 happen. What we're looking for now

67
00:04:15,920 --> 00:04:22,870
 is the support to make it happen in January to be able to

68
00:04:22,870 --> 00:04:26,800
 move into a place and take over

69
00:04:26,800 --> 00:04:32,070
 a house for a year. You get a year's lease on a building

70
00:04:32,070 --> 00:04:34,960
 where we can hold meditation

71
00:04:34,960 --> 00:04:38,560
 courses and that will be close enough to people, close

72
00:04:38,560 --> 00:04:40,960
 enough to the university that we'll

73
00:04:40,960 --> 00:04:46,630
 be able to hold daily sessions, give daily talks live to a

74
00:04:46,630 --> 00:04:49,340
 live audience and to teach

75
00:04:49,340 --> 00:04:55,100
 meditation on a daily basis and just sort of begin to

76
00:04:55,100 --> 00:04:58,120
 create a presence where we can

77
00:04:58,120 --> 00:05:04,940
 eventually turn into a real and lasting resource both

78
00:05:04,940 --> 00:05:10,560
 locally and by extension internationally.

79
00:05:10,560 --> 00:05:13,640
 So there you are. I don't want to say too much. I obviously

80
00:05:13,640 --> 00:05:15,160
 can't get too much involved

81
00:05:15,160 --> 00:05:20,190
 in the workings of it, but thank you all for your support

82
00:05:20,190 --> 00:05:22,800
 and wishing you all the best

