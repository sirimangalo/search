1
00:00:00,000 --> 00:00:07,250
 Okay, welcome back to Ask a Monk. Next question is about

2
00:00:07,250 --> 00:00:07,920
 divorce and I was

3
00:00:07,920 --> 00:00:14,300
 asked the question about how to deal with a divorce when

4
00:00:14,300 --> 00:00:16,120
 you are a third

5
00:00:16,120 --> 00:00:22,240
 party or I guess in this case your parents are divorced or

6
00:00:22,240 --> 00:00:23,580
 divorcing in the

7
00:00:23,580 --> 00:00:31,610
 process of it. How do you deal with it? And I guess as

8
00:00:31,610 --> 00:00:32,700
 always we should put the

9
00:00:32,700 --> 00:00:36,720
 question in context and because here we're talking about

10
00:00:36,720 --> 00:00:37,600
 several different

11
00:00:37,600 --> 00:00:41,600
 things. We're talking about the idea of family and and one

12
00:00:41,600 --> 00:00:43,120
's parents. We're

13
00:00:43,120 --> 00:00:48,770
 talking about the concept of marriage and we're talking

14
00:00:48,770 --> 00:00:51,280
 about suffering. So

15
00:00:51,280 --> 00:00:57,040
 these are all these are three of the issues that come up in

16
00:00:57,040 --> 00:00:57,640
 in relation to

17
00:00:57,640 --> 00:01:04,510
 divorce. The concept of a family has to do with all of

18
00:01:04,510 --> 00:01:08,280
 those concepts that most

19
00:01:08,280 --> 00:01:14,510
 of us seem most of us find to seem or that seem to most of

20
00:01:14,510 --> 00:01:16,440
 us to be ultimately

21
00:01:16,440 --> 00:01:22,470
 real. For instance being human. We think that humanity is

22
00:01:22,470 --> 00:01:25,320
 something real. We think

23
00:01:25,320 --> 00:01:34,330
 of our gender as being something real and as a result these

24
00:01:34,330 --> 00:01:35,400
 reality or these

25
00:01:35,400 --> 00:01:42,490
 concepts form our opinion, our ideas about reality, our

26
00:01:42,490 --> 00:01:44,280
 ideas of family, our

27
00:01:44,280 --> 00:01:49,190
 idea, the concept of having parents. And I see these are

28
00:01:49,190 --> 00:01:50,160
 concepts and I think they

29
00:01:50,160 --> 00:01:53,200
 should be broken apart. They should be broken down and

30
00:01:53,200 --> 00:01:55,160
 understood to be simply

31
00:01:55,160 --> 00:02:02,140
 concepts. Reality doesn't admit of any of these concepts

32
00:02:02,140 --> 00:02:03,520
 that in an

33
00:02:03,520 --> 00:02:07,950
 ultimate sense there is no human, there is no gender, there

34
00:02:07,950 --> 00:02:09,840
 is no family, there is no

35
00:02:09,840 --> 00:02:14,500
 mother and father and so on. I'm gonna get in trouble for

36
00:02:14,500 --> 00:02:16,240
 that because on a

37
00:02:16,240 --> 00:02:19,220
 conventional level people understand that there is mother

38
00:02:19,220 --> 00:02:20,320
 and father. If I say

39
00:02:20,320 --> 00:02:23,200
 there is no mother and father then people are going to say

40
00:02:23,200 --> 00:02:23,800
 what that means.

41
00:02:23,800 --> 00:02:26,700
 We shouldn't respect our parents or we shouldn't be

42
00:02:26,700 --> 00:02:28,160
 thankful for the things

43
00:02:28,160 --> 00:02:33,410
 they've done and obviously that's not the case. But on an

44
00:02:33,410 --> 00:02:35,000
 ultimate reality

45
00:02:35,000 --> 00:02:39,680
 doesn't admit of these things and when we can break away

46
00:02:39,680 --> 00:02:40,040
 from

47
00:02:40,040 --> 00:02:43,150
 these concepts then we can break away from a lot of the

48
00:02:43,150 --> 00:02:44,520
 suffering that comes

49
00:02:44,520 --> 00:02:52,380
 from them. So for instance, in this case our attachment to

50
00:02:52,380 --> 00:02:53,380
 our parents we

51
00:02:53,380 --> 00:02:57,590
 understand them to be a specific entity. Our father is thus

52
00:02:57,590 --> 00:02:59,400
, our mother is thus, our

53
00:02:59,400 --> 00:03:03,500
 family is this unit. There is an entity that we call our

54
00:03:03,500 --> 00:03:05,000
 family, there is a

55
00:03:05,000 --> 00:03:08,320
 relationship that we call family, that we call blood

56
00:03:08,320 --> 00:03:10,560
 relationship and so on. But

57
00:03:10,560 --> 00:03:15,760
 simply looking at if we examine these from a scientific

58
00:03:15,760 --> 00:03:16,120
 point of view

59
00:03:16,120 --> 00:03:21,940
 it's easy to see the falsehood of them. I mean as human

60
00:03:21,940 --> 00:03:22,560
 beings our

61
00:03:22,560 --> 00:03:26,240
 understanding is that by blood we're related to each and

62
00:03:26,240 --> 00:03:27,280
 every one, each and

63
00:03:27,280 --> 00:03:33,120
 every human on the planet by blood ties. So we're all

64
00:03:33,120 --> 00:03:36,880
 family. Obviously this is a

65
00:03:36,880 --> 00:03:41,720
 beneficial realization as well because of all the, you know

66
00:03:41,720 --> 00:03:42,760
, it breaks down all of

67
00:03:42,760 --> 00:03:49,110
 the cultural barriers, all of the racism and prejudice and

68
00:03:49,110 --> 00:03:52,200
 so on, the us and them

69
00:03:52,200 --> 00:03:57,370
 mentalities that pervade the world. But whether it's useful

70
00:03:57,370 --> 00:03:58,480
 or not it's the

71
00:03:58,480 --> 00:04:04,400
 truth. The reality is that there is no barrier or boundary

72
00:04:04,400 --> 00:04:04,640
 where we

73
00:04:04,640 --> 00:04:07,640
 can say this is family and this is not. It's just a matter

74
00:04:07,640 --> 00:04:09,440
 of degree and that

75
00:04:09,440 --> 00:04:13,580
 degree is important in one sense in terms of our

76
00:04:13,580 --> 00:04:15,240
 understanding of each other,

77
00:04:15,240 --> 00:04:19,160
 our ability to relate to each other, our knowledge of each

78
00:04:19,160 --> 00:04:20,840
 other's past and so on.

79
00:04:20,840 --> 00:04:24,760
 And this is the relationship we have with our parents.

80
00:04:24,760 --> 00:04:32,160
 There are many

81
00:04:32,160 --> 00:04:37,550
 emotions and many memories and many ultimate realities that

82
00:04:37,550 --> 00:04:38,400
 are tied up

83
00:04:38,400 --> 00:04:44,500
 with this concept of family. Once we have parents then we

84
00:04:44,500 --> 00:04:45,720
 have many, many

85
00:04:45,720 --> 00:04:51,130
 attachments that go along with that and we have many com

86
00:04:51,130 --> 00:04:53,280
forts and supports.

87
00:04:53,280 --> 00:04:55,920
 There's many beneficial things that come obviously from

88
00:04:55,920 --> 00:04:57,120
 having a stable family

89
00:04:57,120 --> 00:05:02,370
 unit. But something that will help us to deal with divorce

90
00:05:02,370 --> 00:05:06,240
 is to understand that

91
00:05:06,240 --> 00:05:13,180
 there is really no change occurring. That apart from the

92
00:05:13,180 --> 00:05:15,840
 actual suffering that

93
00:05:15,840 --> 00:05:19,370
 comes from a divorce where you lose the stability, where

94
00:05:19,370 --> 00:05:23,400
 you lose the cohesion,

95
00:05:23,400 --> 00:05:28,580
 where you lose the support of having two parents and a

96
00:05:28,580 --> 00:05:30,840
 family unit, there's

97
00:05:30,840 --> 00:05:38,600
 nothing wrong. There's no problem except in your own mind

98
00:05:38,600 --> 00:05:38,920
 where

99
00:05:38,920 --> 00:05:44,360
 you've had your life disrupted, you've had your way of

100
00:05:44,360 --> 00:05:44,760
 looking at

101
00:05:44,760 --> 00:05:49,400
 reality change and in fact it's in a way that seems like

102
00:05:49,400 --> 00:05:50,280
 something

103
00:05:50,280 --> 00:05:55,200
 that was very real and whole has broken into pieces. When

104
00:05:55,200 --> 00:05:55,680
 in fact that's

105
00:05:55,680 --> 00:05:59,720
 not the case because the idea of a family unit is in our

106
00:05:59,720 --> 00:06:00,240
 mind, it's a

107
00:06:00,240 --> 00:06:05,040
 concept. If our parents split up it just means that this

108
00:06:05,040 --> 00:06:05,800
 entity, this

109
00:06:05,800 --> 00:06:11,620
 mass of physical and mental realities has separated, has

110
00:06:11,620 --> 00:06:12,560
 gone here and this one

111
00:06:12,560 --> 00:06:15,970
 has gone here and they're relating to each other in a

112
00:06:15,970 --> 00:06:18,200
 different way. The

113
00:06:18,200 --> 00:06:23,910
 problem comes with our attachment and so this gets into our

114
00:06:23,910 --> 00:06:25,520
 understanding of

115
00:06:25,520 --> 00:06:28,280
 suffering. The first, the other concept I'd like to talk

116
00:06:28,280 --> 00:06:30,220
 about is the one of

117
00:06:30,220 --> 00:06:37,020
 marriage because it relates to the first one, it relates to

118
00:06:37,020 --> 00:06:37,660
 the idea of having

119
00:06:37,660 --> 00:06:44,200
 parents. So our reliance on our parents is one attachment

120
00:06:44,200 --> 00:06:45,160
 that we have. It's

121
00:06:45,160 --> 00:06:51,080
 something that we've misled ourselves from our very birth.

122
00:06:51,080 --> 00:06:51,580
 We've

123
00:06:51,580 --> 00:06:54,020
 been misleading ourselves into thinking that there was

124
00:06:54,020 --> 00:06:55,320
 something stable about

125
00:06:55,320 --> 00:06:59,720
 the family unit and throughout our lives that stability

126
00:06:59,720 --> 00:07:01,200
 will be eroded or

127
00:07:01,200 --> 00:07:05,930
 shattered or taken away from us and it will generally lead

128
00:07:05,930 --> 00:07:06,920
 to a great amount of

129
00:07:06,920 --> 00:07:12,140
 suffering and as a result learning, understanding that what

130
00:07:12,140 --> 00:07:12,760
 we thought was

131
00:07:12,760 --> 00:07:16,420
 stable is not in fact stable and it will allow us to open

132
00:07:16,420 --> 00:07:17,600
 our minds up to the

133
00:07:17,600 --> 00:07:23,480
 idea that life is transient, that we shouldn't cling to

134
00:07:23,480 --> 00:07:24,560
 things and as a

135
00:07:24,560 --> 00:07:27,220
 result we'll be able to accept change better. Most people

136
00:07:27,220 --> 00:07:28,520
 come to this so they

137
00:07:28,520 --> 00:07:32,000
 come to a realization and they come to wisdom through the

138
00:07:32,000 --> 00:07:32,680
 suffering and

139
00:07:32,680 --> 00:07:35,640
 realizing that they were setting themselves up for this

140
00:07:35,640 --> 00:07:36,560
 through their

141
00:07:36,560 --> 00:07:38,880
 ignorance, through their misunderstanding that there was

142
00:07:38,880 --> 00:07:40,720
 stability.

143
00:07:40,720 --> 00:07:48,920
 Marriage is another concept and it's also tied up with a

144
00:07:48,920 --> 00:07:49,320
 lot of

145
00:07:49,320 --> 00:08:03,040
 clinging obviously. So the idea of a divorce is one that we

146
00:08:03,040 --> 00:08:03,640
 think of as

147
00:08:03,640 --> 00:08:08,320
 breaking something up, that there is something that has

148
00:08:08,320 --> 00:08:08,440
 been

149
00:08:08,440 --> 00:08:12,520
 shattered with the marriage and we feel bad about that even

150
00:08:12,520 --> 00:08:13,480
 if we're not

151
00:08:13,480 --> 00:08:18,860
 involved in it. We also feel like there is something

152
00:08:18,860 --> 00:08:20,680
 incomplete, there's some

153
00:08:20,680 --> 00:08:22,960
 knot that has been untied and you have to tie it back

154
00:08:22,960 --> 00:08:25,360
 together and so on. But

155
00:08:25,360 --> 00:08:28,220
 marriage is simply a contract, it's a conventional

156
00:08:28,220 --> 00:08:29,760
 agreement between two

157
00:08:29,760 --> 00:08:34,800
 people and that agreement has to do with morality, it has

158
00:08:34,800 --> 00:08:36,440
 to do with not cheating

159
00:08:36,440 --> 00:08:38,920
 on each other, it has to do with supporting each other and

160
00:08:38,920 --> 00:08:41,160
 so on. So there

161
00:08:41,160 --> 00:08:45,780
 isn't that mutual support anymore, these people have come

162
00:08:45,780 --> 00:08:46,800
 to an agreement that

163
00:08:46,800 --> 00:08:55,030
 they are not going to act in those same ways anymore. It's

164
00:08:55,030 --> 00:08:55,680
 really

165
00:08:55,680 --> 00:08:59,130
 divorce is nothing, it's just a change of agreement, a

166
00:08:59,130 --> 00:09:00,760
 change of our relationship.

167
00:09:00,760 --> 00:09:06,760
 It's like leaving one job and going to another job, it's

168
00:09:06,760 --> 00:09:07,400
 simply a change.

169
00:09:07,400 --> 00:09:11,900
 So again a lot of the suffering that comes from divorce

170
00:09:11,900 --> 00:09:13,400
 just comes from our

171
00:09:13,400 --> 00:09:17,360
 attachment to concepts, the idea of the solid family unit,

172
00:09:17,360 --> 00:09:18,760
 the idea of stability

173
00:09:18,760 --> 00:09:25,360
 which is a false one. In the end we have to accept the fact

174
00:09:25,360 --> 00:09:25,640
 that

175
00:09:25,640 --> 00:09:29,600
 reality is impermanent, everything is changing and if

176
00:09:29,600 --> 00:09:30,640
 whatever we cling to

177
00:09:30,640 --> 00:09:36,470
 it's only going to lead us to suffering because it's not

178
00:09:36,470 --> 00:09:37,880
 stable. So this

179
00:09:37,880 --> 00:09:40,910
 is one side of things and if this is causing you suffering

180
00:09:40,910 --> 00:09:41,520
 then you

181
00:09:41,520 --> 00:09:45,150
 should immediately wipe that out of your mind and move on.

182
00:09:45,150 --> 00:09:47,240
 The real suffering

183
00:09:47,240 --> 00:09:50,590
 that comes from divorce and this is one that is much more

184
00:09:50,590 --> 00:09:51,920
 difficult to overcome

185
00:09:51,920 --> 00:09:56,600
 is the negative emotions that are caused by divorce, the

186
00:09:56,600 --> 00:09:59,720
 anger when the greed

187
00:09:59,720 --> 00:10:10,280
 when there's jealousy involved with the divorce. So anger

188
00:10:10,280 --> 00:10:11,480
 comes when you are

189
00:10:11,480 --> 00:10:15,600
 blaming each other for the divorce and so on. There's guilt

190
00:10:15,600 --> 00:10:16,880
 involved with it,

191
00:10:16,880 --> 00:10:22,210
 the guilt in relation to one's children and so on. There is

192
00:10:22,210 --> 00:10:23,760
 a lot of stress

193
00:10:23,760 --> 00:10:29,920
 involved with the work that's required to split up and

194
00:10:29,920 --> 00:10:32,080
 there's a lot of greed

195
00:10:32,080 --> 00:10:37,120
 involved in terms of splitting up the belongings of the

196
00:10:37,120 --> 00:10:37,840
 family,

197
00:10:37,840 --> 00:10:41,680
 who gets what and how much and then lawyers get involved

198
00:10:41,680 --> 00:10:44,480
 and there's more stress.

199
00:10:44,480 --> 00:10:52,360
 There is jealousy in terms of competing for the love of the

200
00:10:52,360 --> 00:10:56,640
 children, trying to

201
00:10:56,640 --> 00:11:01,410
 not be seen as the bad guy and so you'll have both parents

202
00:11:01,410 --> 00:11:02,160
 vying for the

203
00:11:02,160 --> 00:11:07,760
 love of their children saying how great they are and how

204
00:11:07,760 --> 00:11:08,320
 trying to show

205
00:11:08,320 --> 00:11:14,150
 off and because they're afraid that the other parent is

206
00:11:14,150 --> 00:11:14,360
 doing the

207
00:11:14,360 --> 00:11:17,230
 same thing and it's a competition because they don't want

208
00:11:17,230 --> 00:11:17,760
 to look like the

209
00:11:17,760 --> 00:11:21,950
 bad guy and they care what their children think and they're

210
00:11:21,950 --> 00:11:22,560
 afraid their

211
00:11:22,560 --> 00:11:28,880
 children are going to judge them and it's like this when

212
00:11:28,880 --> 00:11:29,540
 two

213
00:11:29,540 --> 00:11:33,620
 people are vying for the love of one person so the children

214
00:11:33,620 --> 00:11:34,200
 get caught in the

215
00:11:34,200 --> 00:11:41,520
 middle and the parents fight using the children and that

216
00:11:41,520 --> 00:11:41,800
 can be a

217
00:11:41,800 --> 00:11:45,880
 great amount of suffering for both the parents and the

218
00:11:45,880 --> 00:11:47,520
 children obviously.

219
00:11:47,520 --> 00:11:49,470
 The children feel terrible having to be put in the middle

220
00:11:49,470 --> 00:11:50,480
 and obviously they don't

221
00:11:50,480 --> 00:11:56,270
 want to choose between their parents. So how do you deal

222
00:11:56,270 --> 00:11:57,440
 with these sufferings?

223
00:11:57,440 --> 00:12:02,240
 Once you can come to terms with the idea of the divorce and

224
00:12:02,240 --> 00:12:03,040
 overcoming the

225
00:12:03,040 --> 00:12:08,160
 concept of the life that you had that has now been

226
00:12:08,160 --> 00:12:11,080
 shattered, this is where

227
00:12:11,080 --> 00:12:16,770
 obviously meditation is of great benefit and so if you

228
00:12:16,770 --> 00:12:18,440
 begin to practice

229
00:12:18,440 --> 00:12:22,960
 meditation, if you're able to look at the experience as it

230
00:12:22,960 --> 00:12:24,520
 is, I think it'll be

231
00:12:24,520 --> 00:12:29,850
 essential and the incredibly beneficial during that time.

232
00:12:29,850 --> 00:12:30,880
 You have to understand

233
00:12:30,880 --> 00:12:33,450
 that it's going to be a specific length of time. Eventually

234
00:12:33,450 --> 00:12:34,200
 your parents are

235
00:12:34,200 --> 00:12:36,900
 going to move on. They may already have moved on. It's been

236
00:12:36,900 --> 00:12:37,720
 a while since this

237
00:12:37,720 --> 00:12:43,680
 question was posted but whatever suffering comes from the

238
00:12:43,680 --> 00:12:46,280
 divorce or even

239
00:12:46,280 --> 00:12:48,430
 long-term suffering in terms of not having the same

240
00:12:48,430 --> 00:12:49,440
 stability that you had

241
00:12:49,440 --> 00:12:54,490
 before has to be ameliorated through understanding, through

242
00:12:54,490 --> 00:12:55,400
 the ability to

243
00:12:55,400 --> 00:12:58,470
 accept the reality in front of you. Just because it's

244
00:12:58,470 --> 00:12:59,840
 different doesn't mean it's

245
00:12:59,840 --> 00:13:05,370
 bad. Just because one is stressed and has headaches and is

246
00:13:05,370 --> 00:13:07,000
 having to deal with

247
00:13:07,000 --> 00:13:12,420
 a more difficult situation than before, even having to work

248
00:13:12,420 --> 00:13:14,440
 more or work to

249
00:13:14,440 --> 00:13:19,000
 support one's family, work to support one's self and so on.

250
00:13:19,000 --> 00:13:19,680
 That doesn't mean

251
00:13:19,680 --> 00:13:24,360
 that one has to suffer. If you can accept and live with the

252
00:13:24,360 --> 00:13:25,640
 reality and not cling

253
00:13:25,640 --> 00:13:34,440
 to your idea of how it should be or be upset by

254
00:13:34,440 --> 00:13:35,800
 disappointment, by not

255
00:13:35,800 --> 00:13:39,130
 getting things the way you think they should be, when you

256
00:13:39,130 --> 00:13:40,600
're able to simply

257
00:13:40,600 --> 00:13:43,680
 understand things as they are and to see that this is what

258
00:13:43,680 --> 00:13:45,000
's happening, when your

259
00:13:45,000 --> 00:13:47,990
 relationship with reality is not "I wish it could be some

260
00:13:47,990 --> 00:13:49,560
 other way" or "I hope it

261
00:13:49,560 --> 00:13:53,920
 stays this way", your relationship with reality is "it is

262
00:13:53,920 --> 00:13:56,040
 this way" and in every

263
00:13:56,040 --> 00:13:59,320
 moment knowing that now it is this way, now it is this way,

264
00:13:59,320 --> 00:14:00,360
 when you're able to

265
00:14:00,360 --> 00:14:05,790
 think in that way that this is reality, this is reality and

266
00:14:05,790 --> 00:14:06,720
 you see things and

267
00:14:06,720 --> 00:14:08,370
 you know that you're seeing, you hear things and you know

268
00:14:08,370 --> 00:14:09,120
 that you're hearing

269
00:14:09,120 --> 00:14:14,290
 and your relationship with reality is an understanding and

270
00:14:14,290 --> 00:14:17,400
 an acceptance and a

271
00:14:17,400 --> 00:14:21,720
 clear awareness of it as it is with no thoughts of the past

272
00:14:21,720 --> 00:14:23,400
 or the future in

273
00:14:23,400 --> 00:14:30,470
 terms of regret or worry or feelings of loss and so on,

274
00:14:30,470 --> 00:14:31,680
 then you will be, obviously

275
00:14:31,680 --> 00:14:35,370
 you will be far better off. This is clearly the the

276
00:14:35,370 --> 00:14:38,040
 preferable path and so

277
00:14:38,040 --> 00:14:42,650
 this is really the way out of suffering. This is the path

278
00:14:42,650 --> 00:14:43,680
 that the Buddha taught.

279
00:14:43,680 --> 00:14:46,690
 So you can take a look at some of the videos I've posted on

280
00:14:46,690 --> 00:14:47,560
 how to practice

281
00:14:47,560 --> 00:14:50,070
 meditation. I think they would be of great help to anyone

282
00:14:50,070 --> 00:14:51,640
 suffering from this

283
00:14:51,640 --> 00:14:55,440
 sort of stress or any other stress and I hope those are

284
00:14:55,440 --> 00:14:57,560
 views. Quickly, if you

285
00:14:57,560 --> 00:15:01,260
 haven't looked at those videos then it's a very simple

286
00:15:01,260 --> 00:15:02,800
 practice to see things as

287
00:15:02,800 --> 00:15:05,820
 they are instead of wishing they were some other way or

288
00:15:05,820 --> 00:15:06,640
 holding on to these

289
00:15:06,640 --> 00:15:09,140
 things and hoping that they stay this way. You simply

290
00:15:09,140 --> 00:15:10,320
 remind yourself that it

291
00:15:10,320 --> 00:15:13,940
 is what it is. You don't put any value judgment on it. When

292
00:15:13,940 --> 00:15:14,600
 you see something

293
00:15:14,600 --> 00:15:18,070
 you say to yourself "seeing", knowing that this is seeing.

294
00:15:18,070 --> 00:15:19,040
 When you hear "hearing",

295
00:15:19,040 --> 00:15:22,110
 when you smell, you say to yourself "smelling". When you

296
00:15:22,110 --> 00:15:23,200
 feel angry, you say to

297
00:15:23,200 --> 00:15:25,580
 yourself "angry". When you feel sad, you say to yourself "s

298
00:15:25,580 --> 00:15:27,400
ad". When you feel pain or

299
00:15:27,400 --> 00:15:34,150
 headache or pain or aching, simply knowing it for what it

300
00:15:34,150 --> 00:15:34,600
 is, this

301
00:15:34,600 --> 00:15:38,630
 teaches you how to accept and to understand reality for

302
00:15:38,630 --> 00:15:39,760
 what it is and

303
00:15:39,760 --> 00:15:43,710
 instead of wishing it were some other way or holding on to

304
00:15:43,710 --> 00:15:45,280
 and clinging to it and

305
00:15:45,280 --> 00:15:50,360
 setting yourself up for greater suffering. I'd encourage

306
00:15:50,360 --> 00:15:50,560
 you to

307
00:15:50,560 --> 00:15:53,990
 look at the videos that I've posted and if you have time to

308
00:15:53,990 --> 00:15:54,920
 read the book, I

309
00:15:54,920 --> 00:15:58,490
 have a book on my web blog that's better than the videos,

310
00:15:58,490 --> 00:16:00,240
 at least in terms of the

311
00:16:00,240 --> 00:16:03,340
 information it contains and it should give a basic

312
00:16:03,340 --> 00:16:04,640
 understanding of how to

313
00:16:04,640 --> 00:16:08,540
 practice meditation in the tradition that I follow. Thanks

314
00:16:08,540 --> 00:16:09,320
 for the question.

315
00:16:09,320 --> 00:16:12,600
 Hope that helped. All the best.

