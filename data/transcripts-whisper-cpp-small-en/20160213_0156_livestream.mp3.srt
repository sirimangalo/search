1
00:00:00,000 --> 00:00:22,000
 [ Silence ]

2
00:00:22,000 --> 00:00:27,610
 >> Well, good evening everyone. Broadcasting live in Bureau

3
00:00:27,610 --> 00:00:29,000
 12.

4
00:00:29,000 --> 00:00:36,000
 [ Silence ]

5
00:00:36,000 --> 00:00:45,000
 We are broadcasting a few minutes early.

6
00:00:45,000 --> 00:00:57,000
 [ Silence ]

7
00:00:57,000 --> 00:01:15,000
 Today's quote is about the senses.

8
00:01:15,000 --> 00:01:20,210
 It's a good quote. The translation as always is, the

9
00:01:20,210 --> 00:01:22,000
 translation's, you know, it's really fine.

10
00:01:22,000 --> 00:01:29,030
 It's just being someone who knows Pali, you've got to

11
00:01:29,030 --> 00:01:34,000
 wonder what he means by unruffled.

12
00:01:34,000 --> 00:01:53,000
 [ Foreign Language ]

13
00:01:53,000 --> 00:02:03,000
 Unsullied. I don't know about the whole unruffled thing.

14
00:02:03,000 --> 00:02:06,760
 Happiness of being unruffled. I think it's the happiness

15
00:02:06,760 --> 00:02:08,000
 that is unsullied.

16
00:02:08,000 --> 00:02:11,860
 It's an unsullied happiness. That's what Bhikkhu Bodhi

17
00:02:11,860 --> 00:02:13,000
 translates.

18
00:02:13,000 --> 00:02:25,000
 So we'll go with Bhikkhu Bodhi's translation. On seeing a

19
00:02:25,000 --> 00:02:25,000
 form with the eye he does not grasp but its signs and

20
00:02:25,000 --> 00:02:25,000
 features.

21
00:02:25,000 --> 00:02:35,000
 This is an important quote that I often bring up.

22
00:02:35,000 --> 00:03:03,000
 [ Foreign Language ]

23
00:03:03,000 --> 00:03:08,270
 So what does this mean? Well, it's actually a criticism

24
00:03:08,270 --> 00:03:17,000
 that we get from time to time about using a mantra,

25
00:03:17,000 --> 00:03:21,800
 using a word to remind ourselves of the experience because

26
00:03:21,800 --> 00:03:26,640
 it feels like you're not really getting deep into the

27
00:03:26,640 --> 00:03:29,000
 experience.

28
00:03:29,000 --> 00:03:32,300
 But when you see something and you see seeing, seeing it

29
00:03:32,300 --> 00:03:36,340
 actually stops you from getting deep into it, from actually

30
00:03:36,340 --> 00:03:38,000
 experiencing it.

31
00:03:38,000 --> 00:03:41,000
 And it's a criticism by other Buddhists who think that,

32
00:03:41,000 --> 00:03:44,080
 well, you can't really understand something unless you get

33
00:03:44,080 --> 00:03:49,000
 deep into it and the words get in the way.

34
00:03:49,000 --> 00:03:52,350
 But the words are designed to get in the way and that's

35
00:03:52,350 --> 00:03:56,800
 based on what the Buddha is saying here in the Majjhmani Ka

36
00:03:56,800 --> 00:03:58,000
aya.

37
00:03:58,000 --> 00:04:04,000
 So when seeing a form with the eye, one doesn't grasp at

38
00:04:04,000 --> 00:04:08,650
 the sign. And I've talked about this before. We had this a

39
00:04:08,650 --> 00:04:13,000
 couple of days ago, I think.

40
00:04:13,000 --> 00:04:21,000
 So a nimitta is a characteristic or a sign is a good word.

41
00:04:21,000 --> 00:04:23,000
 It's just we don't use it in English really.

42
00:04:23,000 --> 00:04:27,490
 But when you see a human being, as I said, how you know it

43
00:04:27,490 --> 00:04:31,510
's a man or it's a woman is by the sign of a man or the sign

44
00:04:31,510 --> 00:04:36,000
 of a woman, the hint or the characteristic,

45
00:04:36,000 --> 00:04:40,750
 something that creates or induces recognition. That's a man

46
00:04:40,750 --> 00:04:43,000
. That's a woman.

47
00:04:43,000 --> 00:04:47,380
 When you see an apple, that which tells you it's an apple

48
00:04:47,380 --> 00:04:51,580
 and not an orange, when the moment you get that it's an

49
00:04:51,580 --> 00:04:54,000
 apple, that's a nimitta.

50
00:04:54,000 --> 00:05:03,860
 And beyond that, there is the sign of beauty and the sign

51
00:05:03,860 --> 00:05:06,000
 of ugliness.

52
00:05:06,000 --> 00:05:12,270
 And so there's the sign of that which is desirable. And

53
00:05:12,270 --> 00:05:16,340
 when you grasp that, when you get yourself to the point of

54
00:05:16,340 --> 00:05:21,000
 seeing it as beautiful, then desire will arise.

55
00:05:21,000 --> 00:05:25,000
 So this is what we want to avoid.

56
00:05:25,000 --> 00:05:29,030
 When you see something, you see a person and you say to

57
00:05:29,030 --> 00:05:33,530
 yourself, seeing, seeing, the person doesn't get to the

58
00:05:33,530 --> 00:05:38,000
 point of a person, doesn't get further to the point of

59
00:05:38,000 --> 00:05:41,000
 beautiful or ugly.

60
00:05:41,000 --> 00:05:47,230
 Someone you like or dislike when you say seeing, seeing,

61
00:05:47,230 --> 00:05:53,470
 that part of the chain doesn't arise because you've broken

62
00:05:53,470 --> 00:05:55,000
 the chain.

63
00:05:55,000 --> 00:06:00,440
 Anubhyanjana is another aspect of this. Anubhyanjana means

64
00:06:00,440 --> 00:06:02,980
 the particulars. It's really saying the same thing in two

65
00:06:02,980 --> 00:06:04,000
 different ways.

66
00:06:04,000 --> 00:06:08,340
 Anubhyanjana means particulars. So we don't want to know

67
00:06:08,340 --> 00:06:11,720
 the particulars. Not interesting because the particulars

68
00:06:11,720 --> 00:06:13,000
 are where the danger comes.

69
00:06:13,000 --> 00:06:18,760
 So the Buddha says next, "Syatva Dikaranam Meenang, Chakund

70
00:06:18,760 --> 00:06:21,000
rayang Asamudang."

71
00:06:21,000 --> 00:06:32,540
 Having not, when the I faculty has not guarded Asamudang,

72
00:06:32,540 --> 00:06:43,870
 Meenangdang dwelling with the I unguarded, one dwells with

73
00:06:43,870 --> 00:06:47,000
 the mind unguarded.

74
00:06:47,000 --> 00:06:52,530
 Evil unwholesome states of covetousness and grief might

75
00:06:52,530 --> 00:06:55,000
 invade him. It's the key.

76
00:06:55,000 --> 00:06:58,510
 This is where likes and dislikes come. This is where

77
00:06:58,510 --> 00:07:03,000
 pleasure and displeasure come from.

78
00:07:03,000 --> 00:07:07,300
 This is where addiction and aversion, these habits that we

79
00:07:07,300 --> 00:07:11,000
 cultivate that make us greedy, that make us angry,

80
00:07:11,000 --> 00:07:15,770
 that create frustration and boredom, that create fear, that

81
00:07:15,770 --> 00:07:20,000
 create worry, that create addiction,

82
00:07:20,000 --> 00:07:25,230
 that create withdrawal, disappointment, not getting what

83
00:07:25,230 --> 00:07:27,000
 you want, getting what you don't want.

84
00:07:27,000 --> 00:07:33,220
 All this trouble that we have in our lives, it's all

85
00:07:33,220 --> 00:07:39,000
 created by grasping at signs in particular.

86
00:07:39,000 --> 00:07:41,160
 So when we try, when you see something, we try to say to

87
00:07:41,160 --> 00:07:45,000
 ourselves, see, remind ourselves, at the seeing.

88
00:07:45,000 --> 00:07:50,000
 Otherwise it becomes beautiful or ugly.

89
00:07:50,000 --> 00:07:54,000
 And sounds at the ear.

90
00:07:54,000 --> 00:07:57,680
 The problem is that we're taught in society to look for the

91
00:07:57,680 --> 00:08:00,000
 good sounds and the good sights.

92
00:08:00,000 --> 00:08:08,270
 So it's actually, this kind of talk is highly unwelcome for

93
00:08:08,270 --> 00:08:10,000
 most people.

94
00:08:10,000 --> 00:08:12,430
 To talk about beauty as though there was something wrong

95
00:08:12,430 --> 00:08:16,240
 with that, as though we should see things look beyond

96
00:08:16,240 --> 00:08:17,000
 beauty.

97
00:08:17,000 --> 00:08:21,410
 See beautiful things without noticing how beautiful they

98
00:08:21,410 --> 00:08:22,000
 are.

99
00:08:22,000 --> 00:08:28,970
 Spirituality, spiritual teachers or spiritual practitioners

100
00:08:28,970 --> 00:08:32,000
 often make this mistake.

101
00:08:32,000 --> 00:08:34,510
 They think, you know, this whole idea of stopping to smell

102
00:08:34,510 --> 00:08:41,970
 the roses, as though there was something beneficial about

103
00:08:41,970 --> 00:08:45,000
 smelling roses.

104
00:08:45,000 --> 00:08:50,310
 At the university it's surprising, I guess it speaks to the

105
00:08:50,310 --> 00:08:55,400
 age of age group, but I would say somewhere around 50% of

106
00:08:55,400 --> 00:08:58,000
 the undergraduate students at the university

107
00:08:58,000 --> 00:09:01,920
 have these earbuds in their ears, like in between classes

108
00:09:01,920 --> 00:09:03,000
 all the time.

109
00:09:03,000 --> 00:09:07,360
 They come to do meditation, today I did a five minute

110
00:09:07,360 --> 00:09:09,000
 meditation all day.

111
00:09:09,000 --> 00:09:12,370
 So people would come and I'd give them a five minute

112
00:09:12,370 --> 00:09:14,000
 meditation lesson.

113
00:09:14,000 --> 00:09:17,240
 And most of them, most of them had to take the earbud out

114
00:09:17,240 --> 00:09:19,000
 of their ear before they sat down.

115
00:09:19,000 --> 00:09:23,000
 It's really, really a thing.

116
00:09:23,000 --> 00:09:26,420
 We can't live in the world, it's bizarre for those of us

117
00:09:26,420 --> 00:09:31,460
 who walk down the street and are in the world hearing the

118
00:09:31,460 --> 00:09:34,000
 ordinary sounds.

119
00:09:34,000 --> 00:09:40,000
 One of my classmates said she, she, what did she say?

120
00:09:40,000 --> 00:09:44,000
 Something about singing, singing in the shower.

121
00:09:44,000 --> 00:09:46,020
 And I said, you sing in the shower? And he said, oh yeah, I

122
00:09:46,020 --> 00:09:47,000
 have to have music.

123
00:09:47,000 --> 00:09:51,000
 And I said, you have music in the bathroom?

124
00:09:51,000 --> 00:09:55,000
 It's apparently a thing.

125
00:09:55,000 --> 00:09:58,080
 And she was like, yes, I think music in the bathroom is

126
00:09:58,080 --> 00:10:00,000
 important for a,

127
00:10:00,000 --> 00:10:06,000
 she may end up watching this, she's a really good person.

128
00:10:06,000 --> 00:10:08,810
 And she said, we're working together, this is the person

129
00:10:08,810 --> 00:10:12,000
 who's helping me work on the Peace Symposium,

130
00:10:12,000 --> 00:10:15,140
 which I haven't mentioned, I don't think. We're doing a

131
00:10:15,140 --> 00:10:18,000
 Peace Symposium at McMaster.

132
00:10:18,000 --> 00:10:20,820
 People have been asking me, I don't really know what the

133
00:10:20,820 --> 00:10:25,000
 word symposium means, but sort of like a peace fair.

134
00:10:25,000 --> 00:10:28,450
 And in my mind it is actually, I don't think that's where

135
00:10:28,450 --> 00:10:31,740
 the direction that the Peace Studies program wants us to go

136
00:10:31,740 --> 00:10:32,000
.

137
00:10:32,000 --> 00:10:37,670
 But I want it to be a healing experience where people come

138
00:10:37,670 --> 00:10:42,000
 together and, you know, I mean like a peace, an active

139
00:10:42,000 --> 00:10:43,000
 peace process

140
00:10:43,000 --> 00:10:47,000
 where people come and they leave knowing more about peace,

141
00:10:47,000 --> 00:10:51,000
 having skills, learned skills about cultivating peace.

142
00:10:51,000 --> 00:10:55,000
 Anyway, sorry, a little bit off track there.

143
00:10:55,000 --> 00:11:02,210
 But it's a thing, no? We want to hear beautiful music,

144
00:11:02,210 --> 00:11:05,000
 beautiful sounds.

145
00:11:05,000 --> 00:11:09,110
 A lot of people ask whether you can meditate to music,

146
00:11:09,110 --> 00:11:13,880
 which, you know, it shows this misunderstanding about

147
00:11:13,880 --> 00:11:16,000
 spirituality.

148
00:11:16,000 --> 00:11:18,000
 I mean from a Buddhist point of view, anyway.

149
00:11:18,000 --> 00:11:23,020
 Of course, a lot of people would say the Buddhists have got

150
00:11:23,020 --> 00:11:26,000
 it wrong, so to each their own.

151
00:11:26,000 --> 00:11:31,000
 Smells, we want to smell good things.

152
00:11:31,000 --> 00:11:36,170
 It's the nature of wanting to experience good things that

153
00:11:36,170 --> 00:11:39,000
 you cultivate partiality.

154
00:11:39,000 --> 00:11:45,300
 It's like a pendulum. If you pull it one way, you're

155
00:11:45,300 --> 00:11:50,000
 creating the potential energy.

156
00:11:50,000 --> 00:11:53,220
 The more you pull, the more potential, until finally you

157
00:11:53,220 --> 00:11:55,000
 let go and it will swing back.

158
00:11:55,000 --> 00:12:00,890
 You create the opposite. You create the aversion with the

159
00:12:00,890 --> 00:12:02,000
 attraction.

160
00:12:02,000 --> 00:12:10,360
 You become more irritable, more dissatisfied, more prone to

161
00:12:10,360 --> 00:12:17,000
 disappointment, more prone to anger.

162
00:12:17,000 --> 00:12:19,000
 This is why people fight.

163
00:12:19,000 --> 00:12:21,970
 People who are steeped in sensual pleasure have so much

164
00:12:21,970 --> 00:12:25,000
 sensual pleasure end up being the ones who fight the most.

165
00:12:25,000 --> 00:12:29,000
 Fight with each other, bicker with each other, argue with

166
00:12:29,000 --> 00:12:30,000
 each other.

167
00:12:30,000 --> 00:12:36,460
 So much conflict comes from sensuality, comes from that

168
00:12:36,460 --> 00:12:40,000
 which is supposed to make us happy.

169
00:12:40,000 --> 00:12:50,000
 We go to war over our happiness. It's the truth of it.

170
00:12:50,000 --> 00:12:57,590
 So when we smell good smells, we want to smell the roses,

171
00:12:57,590 --> 00:13:02,000
 we don't want to smell bad smells.

172
00:13:02,000 --> 00:13:05,250
 And it seems so ingrained in the experience where you think

173
00:13:05,250 --> 00:13:10,000
 a bad smell is a bad smell. It's bad.

174
00:13:10,000 --> 00:13:15,190
 This is the first myth that we have to dispel. The bad is a

175
00:13:15,190 --> 00:13:20,000
 product of the particulars of the smell.

176
00:13:20,000 --> 00:13:24,830
 If it's just smell, if you say smelling, smelling, it's

177
00:13:24,830 --> 00:13:26,000
 just smell.

178
00:13:26,000 --> 00:13:33,000
 Same goes with taste. Good taste, bad taste.

179
00:13:33,000 --> 00:13:37,800
 It's easy to get caught up in tastes. But it's also very

180
00:13:37,800 --> 00:13:42,130
 easy, if you know how, to free yourself, to avoid that

181
00:13:42,130 --> 00:13:46,000
 whole world of addiction and aversion.

182
00:13:46,000 --> 00:13:50,770
 If you say to yourself, tasting, tasting. All of this,

183
00:13:50,770 --> 00:13:54,210
 seeing, seeing with sensuality, with sexuality, with

184
00:13:54,210 --> 00:13:55,000
 romance.

185
00:13:55,000 --> 00:14:00,930
 See a beautiful person, seeing, seeing, it's just seeing.

186
00:14:00,930 --> 00:14:03,000
 You cut it off totally.

187
00:14:03,000 --> 00:14:09,810
 First you have to be diligent, of course. It's not

188
00:14:09,810 --> 00:14:13,000
 something that just, you can break off with one goal.

189
00:14:13,000 --> 00:14:16,180
 It's a habit that you build. Eventually it becomes a habit

190
00:14:16,180 --> 00:14:18,000
 to see things just as they are.

191
00:14:18,000 --> 00:14:25,840
 And that's the habit we want to cultivate. When you can do

192
00:14:25,840 --> 00:14:31,000
 that, you move towards letting go.

193
00:14:31,000 --> 00:14:38,000
 So, a really good quote. Not so much more to say.

194
00:14:38,000 --> 00:14:43,830
 Except we could talk about this happiness that is unsullied

195
00:14:43,830 --> 00:14:46,000
 or unadulterated.

196
00:14:46,000 --> 00:14:49,240
 So it's the difference between happiness that is based on

197
00:14:49,240 --> 00:14:56,000
 partiality, because it's dependent on x and not y, or not x

198
00:14:56,000 --> 00:14:56,000
.

199
00:14:56,000 --> 00:15:03,130
 As opposed to the happiness that is free from reliance or

200
00:15:03,130 --> 00:15:07,000
 dependence on experience.

201
00:15:07,000 --> 00:15:14,140
 Happiness that comes from being free from letting go. From

202
00:15:14,140 --> 00:15:20,420
 freedom from stress. Freedom from the concern about the

203
00:15:20,420 --> 00:15:22,000
 experience.

204
00:15:22,000 --> 00:15:28,000
 Anyway, so that's our Dhamma for today.

205
00:15:28,000 --> 00:15:31,080
 Nobody joined me on the hangout tonight. It must be because

206
00:15:31,080 --> 00:15:35,000
 last night I didn't have one. You can go ahead.

207
00:15:35,000 --> 00:15:38,450
 Oh, I know, because I didn't post the hangout. You can't

208
00:15:38,450 --> 00:15:46,000
 join me. Right, there's a step missing here. Sorry.

209
00:15:46,000 --> 00:15:52,460
 There's the hangout. Is there anybody around who wants to

210
00:15:52,460 --> 00:15:55,000
 join you? Only join if you want to ask a question.

211
00:15:55,000 --> 00:16:24,000
 If you have a question, join me. Ask it in the hangout.

212
00:16:24,000 --> 00:16:35,000
 Larry, do you have a question?

213
00:16:35,000 --> 00:16:41,650
 No, I've been listening to the Dhamma talk and I just

214
00:16:41,650 --> 00:16:48,270
 noticed that the link to this call in showed up. So I

215
00:16:48,270 --> 00:16:49,000
 clicked on it.

216
00:16:49,000 --> 00:16:54,000
 I've been wondering if I should have seen the link earlier.

217
00:16:54,000 --> 00:16:57,000
 Yeah, I forgot to post it.

218
00:16:57,000 --> 00:17:05,260
 And I presume that a prior link to a previous call would

219
00:17:05,260 --> 00:17:09,000
 not work for this call.

220
00:17:09,000 --> 00:17:10,000
 No.

221
00:17:10,000 --> 00:17:15,020
 Okay. Everyone is uniquely coded or whatever for the

222
00:17:15,020 --> 00:17:21,190
 internet. Okay. So I listened to your talk and clicked on

223
00:17:21,190 --> 00:17:27,000
 the link to the call in as soon as I saw it.

224
00:17:27,000 --> 00:17:37,000
 But I do have a question. You sparked a question in me.

225
00:17:37,000 --> 00:17:44,580
 When we're just essentially noting seeing, seeing, smelling

226
00:17:44,580 --> 00:17:46,000
, hearing.

227
00:17:46,000 --> 00:17:53,140
 And of course those are based on the sense doors. The eyes,

228
00:17:53,140 --> 00:17:57,000
 ears, nose, mouth, touching.

229
00:17:57,000 --> 00:18:04,150
 So is it, is it inappropriate to just say something like

230
00:18:04,150 --> 00:18:11,530
 sensing, sensing, and try to get, try to tie the noting to

231
00:18:11,530 --> 00:18:17,980
 the specific sense door is that there's evidently value in

232
00:18:17,980 --> 00:18:19,000
 that.

233
00:18:19,000 --> 00:18:22,000
 And I'm just speculating.

234
00:18:22,000 --> 00:18:25,780
 Right. I mean, the word isn't that important. Whatever

235
00:18:25,780 --> 00:18:28,000
 brings you close to the experience.

236
00:18:28,000 --> 00:18:31,290
 But sensing to me is too abstract. It's not likely to bring

237
00:18:31,290 --> 00:18:35,660
 you close to the experience. I mean, to the bare experience

238
00:18:35,660 --> 00:18:36,000
.

239
00:18:36,000 --> 00:18:37,000
 Right.

240
00:18:37,000 --> 00:18:42,110
 Not, it's a bit abstract because you don't sense something.

241
00:18:42,110 --> 00:18:46,000
 You hear something.

242
00:18:46,000 --> 00:18:51,080
 Good. Appreciate the clarification. I've wondered that. And

243
00:18:51,080 --> 00:18:54,600
, and you know, like, like you say, just say, try to, try to

244
00:18:54,600 --> 00:18:59,000
 boil it down to the very sensual sensing.

245
00:18:59,000 --> 00:19:06,720
 It isn't very satisfying. And, you know, notionally or

246
00:19:06,720 --> 00:19:12,000
 intellectually, it's not a very satisfying term to use.

247
00:19:12,000 --> 00:19:19,000
 But yeah, I appreciate that.

248
00:19:19,000 --> 00:19:22,190
 One thing I did want to talk about, well, I got your

249
00:19:22,190 --> 00:19:27,060
 attention, everybody. Tomorrow, tomorrow I'm having a

250
00:19:27,060 --> 00:19:33,050
 weekly, we're started doing these weekly talks in Second

251
00:19:33,050 --> 00:19:36,000
 Life at the Buddha Center.

252
00:19:36,000 --> 00:19:39,470
 So if you know what Second Life is and you know where the

253
00:19:39,470 --> 00:19:42,000
 Buddha Center is, it can come out.

254
00:19:42,000 --> 00:19:45,250
 That's at 3 p.m. or you can just come to the meditation

255
00:19:45,250 --> 00:19:50,600
 site, meditation.siri-mongolo.org and listen in because I

256
00:19:50,600 --> 00:19:53,000
'll be simulcasting.

257
00:19:53,000 --> 00:20:00,150
 Is that a word? Simultaneously broadcasting audio there. So

258
00:20:00,150 --> 00:20:03,000
 you can listen to the audio at the same time.

259
00:20:03,000 --> 00:20:07,230
 I don't know what I'm talking about yet. Anybody have

260
00:20:07,230 --> 00:20:12,000
 anything they want me to give a talk on tomorrow?

261
00:20:12,000 --> 00:20:16,470
 If you come on the Hangout and give me a topic, I'll

262
00:20:16,470 --> 00:20:18,000
 consider it.

263
00:20:18,000 --> 00:20:21,000
 Can you give a talk on dependent origination?

264
00:20:21,000 --> 00:20:24,230
 I actually gave one in Second Life, on dependent orig

265
00:20:24,230 --> 00:20:29,060
ination. I've given several on dependent origination. You

266
00:20:29,060 --> 00:20:34,000
 can look it up.

267
00:20:34,000 --> 00:20:37,440
 I don't know if they're any good, but I think there's one

268
00:20:37,440 --> 00:20:41,210
 on practical dependent origination, not Second Life, but

269
00:20:41,210 --> 00:20:45,000
 there's one that's called practical dependent origination.

270
00:20:45,000 --> 00:20:48,990
 It seems to me. Maybe it's one I did in Sri Lanka. I think

271
00:20:48,990 --> 00:20:53,000
 it's one I did on the Buddhist television in Sri Lanka.

272
00:20:53,000 --> 00:20:59,000
 I remember seeing a YouTube that you had published quite

273
00:20:59,000 --> 00:21:04,660
 some back. I think you were in Sri Lanka somewhere overseas

274
00:21:04,660 --> 00:21:08,000
 about dependent origination.

275
00:21:08,000 --> 00:21:15,000
 I have done a couple of these.

276
00:21:15,000 --> 00:21:21,000
 Another thing, can you talk about right view?

277
00:21:21,000 --> 00:21:27,000
 Sure.

278
00:21:27,000 --> 00:21:34,160
 I have to think about that. There's different ways of

279
00:21:34,160 --> 00:21:39,890
 talking about right view. I can go over different ways of

280
00:21:39,890 --> 00:21:41,000
 looking at it.

281
00:21:41,000 --> 00:21:50,280
 Would the different ways of talking about be associated

282
00:21:50,280 --> 00:21:56,900
 with different sects of Buddhism, like the forced tradition

283
00:21:56,900 --> 00:22:05,580
, or this or that, the way they define the Noble Eightfold

284
00:22:05,580 --> 00:22:07,000
 Path?

285
00:22:07,000 --> 00:22:11,770
 The different ways of talking about right view is because

286
00:22:11,770 --> 00:22:16,000
 the Buddha talked about it in different ways.

287
00:22:16,000 --> 00:22:30,000
 There are basically two kinds, mundane and noble. Mundane

288
00:22:30,000 --> 00:22:30,000
 right view is useful, but it's not enough.

289
00:22:30,000 --> 00:22:42,740
 Mundane right view has to do with the law of karma. Mostly

290
00:22:42,740 --> 00:22:47,000
 the law of karma.

291
00:22:47,000 --> 00:22:55,190
 I'll take a look at it, see, because there is right view of

292
00:22:55,190 --> 00:23:00,680
 non-self, there is right view of the truth of suffering, or

293
00:23:00,680 --> 00:23:05,000
 the Four Noble Truths.

294
00:23:05,000 --> 00:23:12,620
 There is right view in terms of karma. Those are three that

295
00:23:12,620 --> 00:23:20,560
 I can think of. They're not exclusively different, but

296
00:23:20,560 --> 00:23:27,000
 three ways of approaching right view.

297
00:23:27,000 --> 00:23:31,000
 Thank you all. That sounds like a good talk.

298
00:23:31,000 --> 00:23:36,000
 You're welcome.

299
00:23:36,000 --> 00:23:39,000
 Bobby, did you have a question? Is that why you're here?

300
00:23:39,000 --> 00:23:47,960
 I was just going to ask about the Four Nutriments in the

301
00:23:47,960 --> 00:23:55,990
 Samadhi Tisuta, how they relate to dependent origination,

302
00:23:55,990 --> 00:23:59,000
 because I believe that that satsuta is kind of...

303
00:23:59,000 --> 00:24:04,820
 It talks about patricius samutata as well as the Four Nut

304
00:24:04,820 --> 00:24:09,000
riments and the Four Noble Truths and so on.

305
00:24:09,000 --> 00:24:11,000
 Did you say the Samadhi Tisuta?

306
00:24:11,000 --> 00:24:15,000
 Yeah.

307
00:24:15,000 --> 00:24:17,000
 What are the Four Nutriments?

308
00:24:17,000 --> 00:24:18,000
 In the Maja Mysaya.

309
00:24:18,000 --> 00:24:21,000
 What are the Four Nutriments?

310
00:24:21,000 --> 00:24:29,940
 It was food, so physical food, and then mental volition, so

311
00:24:29,940 --> 00:24:34,000
 sankara, and then contact.

312
00:24:34,000 --> 00:24:39,300
 Manosanjaitana, it's not actually, it's manosanjaitana. It

313
00:24:39,300 --> 00:24:42,000
's actually intention.

314
00:24:42,000 --> 00:24:45,000
 Oh yeah.

315
00:24:45,000 --> 00:24:50,080
 And then there's passa har and minya nahara, consciousness,

316
00:24:50,080 --> 00:24:51,000
 right?

317
00:24:51,000 --> 00:24:52,000
 Yeah.

318
00:24:52,000 --> 00:24:55,220
 I didn't realize that came from the Samadhi Tisuta. It

319
00:24:55,220 --> 00:24:57,000
 shows what a scholar I am.

320
00:24:57,000 --> 00:25:03,000
 Which one is Samadhi Tisuta, Majimini Kaya number 12?

321
00:25:03,000 --> 00:25:07,000
 I think it's 9. Yeah, number 9.

322
00:25:07,000 --> 00:25:15,000
 This is Sutra by Suryputa. There we are, a new instrument.

323
00:25:15,000 --> 00:25:24,000
 The writhing of craving there causes it, right?

324
00:25:24,000 --> 00:25:27,080
 See, I mean, this whole Sutra, Sutra is about different

325
00:25:27,080 --> 00:25:30,000
 ways of expressing the Four Noble Truths, right?

326
00:25:30,000 --> 00:25:31,000
 Yeah.

327
00:25:31,000 --> 00:25:35,380
 I think it says in the commentary that he's 24 different

328
00:25:35,380 --> 00:25:39,000
 ways, he is, or maybe he even says something.

329
00:25:39,000 --> 00:25:44,210
 In 24 different ways, or some number of ways, he's, it's

330
00:25:44,210 --> 00:25:46,370
 the most number of times anyone's gone through the Four

331
00:25:46,370 --> 00:25:48,000
 Noble Truths or something like that.

332
00:25:48,000 --> 00:25:50,000
 It's just a special symbol.

333
00:25:50,000 --> 00:25:54,370
 Yeah, it always relates to the topic that he's talking

334
00:25:54,370 --> 00:25:57,000
 about back to the Noble Truth.

335
00:25:57,000 --> 00:26:04,410
 I mean, the Bhattichya Samupada is basically the Four Noble

336
00:26:04,410 --> 00:26:06,000
 Truths, is kind of the Four Noble Truths expanded upon.

337
00:26:06,000 --> 00:26:10,340
 But even it, even Bhattichya Samupada is still a cond

338
00:26:10,340 --> 00:26:13,000
ensation of the Mahapatana.

339
00:26:13,000 --> 00:26:19,220
 If you really want to know about causality, there are 24 bh

340
00:26:19,220 --> 00:26:23,000
ajya, 24 conditionalities.

341
00:26:23,000 --> 00:26:27,000
 And it's much more complicated than just the line.

342
00:26:27,000 --> 00:26:29,000
 I mean, Bhattichya Samupada sounds like a line, right?

343
00:26:29,000 --> 00:26:32,510
 It sounds like it's in a chain, but it's not really. It's

344
00:26:32,510 --> 00:26:35,000
 quite complicated.

345
00:26:35,000 --> 00:26:39,400
 People try to think of it as simple as just, and some

346
00:26:39,400 --> 00:26:42,000
 people say Bhattichya Samupada only relates to one moment,

347
00:26:42,000 --> 00:26:44,000
 it's only this life.

348
00:26:44,000 --> 00:26:46,990
 It's nothing to do with past lives, nothing to do with

349
00:26:46,990 --> 00:26:48,000
 future lives.

350
00:26:48,000 --> 00:26:57,880
 Bizarrely, they try to say that Bhattichya Samupada doesn't

351
00:26:57,880 --> 00:27:02,000
 talk about past lives.

352
00:27:02,000 --> 00:27:06,690
 But it's quite, you know, the first, "Avijapachya Sankara"

353
00:27:06,690 --> 00:27:08,000
 is past lives.

354
00:27:08,000 --> 00:27:14,410
 It's just a way of expressing how we are born again and

355
00:27:14,410 --> 00:27:18,000
 again based on ignorance.

356
00:27:18,000 --> 00:27:21,420
 But ignorance is also involved with tanha. So we say, "Tan

357
00:27:21,420 --> 00:27:24,000
ha bhatti, vaidana bhattiya tanha."

358
00:27:24,000 --> 00:27:29,000
 Well, vaidana only gives rise to tanha if there's a vijya.

359
00:27:29,000 --> 00:27:37,000
 But vaidana doesn't require a vijya, not in this life.

360
00:27:37,000 --> 00:27:41,620
 So you experience vaidana even without a vijya, but not

361
00:27:41,620 --> 00:27:42,000
 originally.

362
00:27:42,000 --> 00:27:48,480
 Originally you needed a vijya to be born. So it's not

363
00:27:48,480 --> 00:27:51,000
 linear.

364
00:27:51,000 --> 00:27:56,390
 So if you really want to learn about it, you should read

365
00:27:56,390 --> 00:28:00,000
 the matikha of the Maha Bhattana.

366
00:28:00,000 --> 00:28:03,220
 There's a really good chant that I actually put up on my

367
00:28:03,220 --> 00:28:04,000
 website.

368
00:28:04,000 --> 00:28:08,870
 It makes me think I was Burmese in a past life because the

369
00:28:08,870 --> 00:28:11,000
 first time I heard this chant,

370
00:28:11,000 --> 00:28:14,620
 I was just struck by it. I've never been struck by chanting

371
00:28:14,620 --> 00:28:17,000
 before, but it was something so...

372
00:28:17,000 --> 00:28:20,210
 It just resonated with me. And these monks from Burma were

373
00:28:20,210 --> 00:28:21,000
 visiting Thailand

374
00:28:21,000 --> 00:28:27,000
 and they went around this, the Jaidya, the Indoy Sutheb.

375
00:28:27,000 --> 00:28:29,260
 And I just had to know what that was, so later I asked them

376
00:28:29,260 --> 00:28:37,000
, "Would you like a monk, a Burmese monk?"

377
00:28:37,000 --> 00:28:43,510
 And he explained it to me. He said, "Oh, that's the Maha B

378
00:28:43,510 --> 00:28:45,000
hattana."

379
00:28:45,000 --> 00:28:50,010
 So, "Heidupa Jayyoti, heidu heidu sampir takananda manan d

380
00:28:50,010 --> 00:28:56,000
angsmutana najarupanang heidupa jayena bhajayo."

381
00:28:56,000 --> 00:29:00,770
 Something like that. So that's the first one, "Heidupa Jaya

382
00:29:00,770 --> 00:29:03,000
," when something is its cause.

383
00:29:03,000 --> 00:29:24,390
 So a cause of... something is a cause of form together with

384
00:29:24,390 --> 00:29:28,000
 its dhammas.

385
00:29:28,000 --> 00:29:31,740
 I can't translate this. I used to know how this was

386
00:29:31,740 --> 00:29:33,000
 translated.

387
00:29:33,000 --> 00:29:36,180
 Something is a cause when it's... I mean, this is just very

388
00:29:36,180 --> 00:29:38,000
, very basic, very, very brief,

389
00:29:38,000 --> 00:29:42,020
 going through how something is a cause of something else.

390
00:29:42,020 --> 00:29:48,000
 You have to read the translation.

391
00:29:48,000 --> 00:29:50,000
 I'll definitely check that out.

392
00:29:50,000 --> 00:29:58,670
 There is a translation of the Maha Bhattana, at least

393
00:29:58,670 --> 00:30:03,000
 abbreviated, I think.

394
00:30:03,000 --> 00:30:07,190
 Anyway, that's all for tonight. Then, if there's no

395
00:30:07,190 --> 00:30:10,000
 questions, I'm going to head off.

396
00:30:10,000 --> 00:30:19,410
 Soty, manky. Soty is for "hello." When we say goodbye, it's

397
00:30:19,410 --> 00:30:22,000
 sad who... sad who is having a say goodbye.

398
00:30:22,000 --> 00:30:25,230
 I tried to sort that out, like I wasn't successful the

399
00:30:25,230 --> 00:30:26,000
 other day.

400
00:30:26,000 --> 00:30:29,030
 You know, honestly, it's probably okay, just the tradition.

401
00:30:29,030 --> 00:30:31,000
 I mean, it's only the tradition I know.

402
00:30:31,000 --> 00:30:41,000
 It's Soty to say hello. Sad who is when you finish. Sad who

403
00:30:41,000 --> 00:30:41,000
? Sad who? Have a good night. Good night.

404
00:30:41,000 --> 00:30:44,000
 Good night, all. Thank you very much.

405
00:30:44,000 --> 00:30:45,000
 Thank you.

406
00:30:45,000 --> 00:30:58,000
 Thank you.

