1
00:00:00,000 --> 00:00:04,330
 Sometimes when I feel sleepy in the morning I do walking

2
00:00:04,330 --> 00:00:06,000
 meditation before sitting.

3
00:00:06,000 --> 00:00:10,060
 Do you recommend it for every session? How can I understand

4
00:00:10,060 --> 00:00:13,710
 the best moment to attain, to shift from walking to sitting

5
00:00:13,710 --> 00:00:14,000
?

6
00:00:14,000 --> 00:00:22,130
 Yes, we recommend walking before every round of sitting, a

7
00:00:22,130 --> 00:00:23,000
 formal meditation.

8
00:00:23,000 --> 00:00:28,510
 Now in daily life it's sometimes inconvenient and even

9
00:00:28,510 --> 00:00:33,000
 undesirable to do walking before each round.

10
00:00:33,000 --> 00:00:36,230
 Potentially because you've been walking during the day, you

11
00:00:36,230 --> 00:00:39,860
've been physically active or you're drained at the end of

12
00:00:39,860 --> 00:00:45,940
 the day so often it can be often the case that you'd rather

13
00:00:45,940 --> 00:00:47,000
 just do sitting.

14
00:00:47,000 --> 00:00:51,340
 On the other hand you'll find that sometimes walking is

15
00:00:51,340 --> 00:00:54,890
 more helpful for you. After a long day of stress it's

16
00:00:54,890 --> 00:00:58,140
 sometimes difficult to sit down and you'll find that

17
00:00:58,140 --> 00:01:01,330
 walking allows you to calm down, allows you to focus

18
00:01:01,330 --> 00:01:04,000
 yourself, center yourself before you have to sit.

19
00:01:04,000 --> 00:01:08,000
 And it sets you up for good meditation.

20
00:01:08,000 --> 00:01:12,520
 Our rule of thumb is to do half walking and half sitting

21
00:01:12,520 --> 00:01:17,230
 except for, in the case for an advanced meditator who is

22
00:01:17,230 --> 00:01:20,000
 deeply involved in meditation.

23
00:01:20,000 --> 00:01:24,000
 Eventually, and this is for someone who is practicing

24
00:01:24,000 --> 00:01:28,430
 intensively, it means all day, potentially all night, they

25
00:01:28,430 --> 00:01:33,450
 can come to a point where they decide for themselves to do

26
00:01:33,450 --> 00:01:37,000
 walking as long as they wish and then sitting.

27
00:01:37,000 --> 00:01:41,980
 This is in our tradition. In our tradition we almost always

28
00:01:41,980 --> 00:01:46,000
 prefer to do half walking, half sitting.

29
00:01:46,000 --> 00:01:49,080
 One good reason because otherwise you just follow your

30
00:01:49,080 --> 00:01:52,850
 partiality. You're partial to one and so you do it and in

31
00:01:52,850 --> 00:01:55,000
 fact that is the wrong reason.

32
00:01:55,000 --> 00:01:58,360
 That's the reason you should switch to be able to break and

33
00:01:58,360 --> 00:02:00,000
 let go of the partiality.

34
00:02:00,000 --> 00:02:07,000
 Wanting to sit so you walk, wanting to walk and so you sit.

35
00:02:07,000 --> 00:02:12,820
 And so the half and half thing, equalizing, doing them

36
00:02:12,820 --> 00:02:18,000
 equal amounts helps to break that attachment.

