1
00:00:00,000 --> 00:00:05,000
 So that was my alms round.

2
00:00:05,000 --> 00:00:10,000
 Not a usual alms round, I suppose.

3
00:00:10,000 --> 00:00:15,000
 I don't always have people stop me.

4
00:00:15,000 --> 00:00:18,000
 I don't always have police officers stop me.

5
00:00:18,000 --> 00:00:22,000
 So this was doubly unusual.

6
00:00:22,000 --> 00:00:25,000
 It's almost as if I planned it, having talked this morning

7
00:00:25,000 --> 00:00:28,260
 about one of the things I'm quote-unquote "afraid" of being

8
00:00:28,260 --> 00:00:29,000
 police officers.

9
00:00:29,000 --> 00:00:32,460
 And then right after that, being stopped by a police

10
00:00:32,460 --> 00:00:33,000
 officer.

11
00:00:33,000 --> 00:00:36,000
 But he was really nice.

12
00:00:36,000 --> 00:00:40,000
 He probably has heard from people, or he's probably himself

13
00:00:40,000 --> 00:00:40,000
,

14
00:00:40,000 --> 00:00:43,330
 and he claimed that there were other people wondering or

15
00:00:43,330 --> 00:00:46,000
 concerned about who I am.

16
00:00:46,000 --> 00:00:50,600
 But it's probably more him interested and maybe even

17
00:00:50,600 --> 00:00:52,000
 suspicious.

18
00:00:52,000 --> 00:00:59,000
 He was very friendly, overtly, ostensibly, or how do you

19
00:00:59,000 --> 00:00:59,000
 say, on the outside.

20
00:00:59,000 --> 00:01:03,000
 He seemed friendly.

21
00:01:03,000 --> 00:01:07,000
 He just wanted to know what it is that I'm carrying around,

22
00:01:07,000 --> 00:01:10,000
 and so I showed him my office ball.

23
00:01:10,000 --> 00:01:15,000
 He said he didn't have a problem with what I'm doing.

24
00:01:15,000 --> 00:01:18,910
 But he said I have to understand that people are going to

25
00:01:18,910 --> 00:01:21,000
 be interested in anything new.

26
00:01:21,000 --> 00:01:24,000
 People become curious.

27
00:01:24,000 --> 00:01:26,000
 And so I don't think it's a big problem.

28
00:01:26,000 --> 00:01:29,570
 I'm really going to write this letter to the local

29
00:01:29,570 --> 00:01:30,000
 newspaper.

30
00:01:30,000 --> 00:01:33,310
 Hopefully I'll write it this afternoon, and then you'll get

31
00:01:33,310 --> 00:01:35,000
 a chance to...

32
00:01:35,000 --> 00:01:39,000
 I'll read it to you this afternoon.

33
00:01:39,000 --> 00:01:44,000
 Why we do Aam's Round?

34
00:01:44,000 --> 00:01:47,000
 Well, it's something that came from the Buddha's time.

35
00:01:47,000 --> 00:01:52,340
 But without dragging up the past or talking about the

36
00:01:52,340 --> 00:01:54,000
 tradition of it,

37
00:01:54,000 --> 00:01:58,020
 there are some really obvious reasons as to why it's a

38
00:01:58,020 --> 00:02:01,000
 useful thing or a beneficial thing to do.

39
00:02:01,000 --> 00:02:07,240
 Obviously the benefits to the monk himself should be quite

40
00:02:07,240 --> 00:02:09,000
 obvious.

41
00:02:09,000 --> 00:02:13,000
 There's incredible benefits to the recipient.

42
00:02:13,000 --> 00:02:19,100
 I don't have to work for food, or I don't have to make

43
00:02:19,100 --> 00:02:20,000
 money.

44
00:02:20,000 --> 00:02:22,000
 I don't touch money.

45
00:02:22,000 --> 00:02:25,000
 I don't have to go cooking.

46
00:02:25,000 --> 00:02:27,000
 I don't have to keep food.

47
00:02:27,000 --> 00:02:30,530
 It seems a lot like taking advantage of people in that

48
00:02:30,530 --> 00:02:31,000
 sense.

49
00:02:31,000 --> 00:02:33,000
 And a lot of people think of it like that.

50
00:02:33,000 --> 00:02:38,000
 I've heard people say that monks are lazy, or so on.

51
00:02:38,000 --> 00:02:41,300
 And so if we're just asking what are the benefits, why do

52
00:02:41,300 --> 00:02:42,000
 you do that,

53
00:02:42,000 --> 00:02:46,000
 then obviously I don't think there's any question as to why

54
00:02:46,000 --> 00:02:46,000
 one would do it

55
00:02:46,000 --> 00:02:49,000
 if they were just concerned about their own benefit.

56
00:02:49,000 --> 00:02:52,140
 I think the benefits for the people who are giving is

57
00:02:52,140 --> 00:02:53,000
 probably less clear,

58
00:02:53,000 --> 00:02:57,800
 especially for those people who aren't accustomed to giving

59
00:02:57,800 --> 00:02:59,000
 something on a daily basis

60
00:02:59,000 --> 00:03:02,000
 or giving to people that they might not know.

61
00:03:02,000 --> 00:03:06,950
 But you really have to look at it in terms of being a

62
00:03:06,950 --> 00:03:08,000
 religion.

63
00:03:08,000 --> 00:03:12,400
 Because we see in all other religions people give exorbit

64
00:03:12,400 --> 00:03:14,000
ant amounts of money to,

65
00:03:14,000 --> 00:03:19,250
 or seemingly exorbitant amounts of money to priests and

66
00:03:19,250 --> 00:03:21,000
 ministers and so on

67
00:03:21,000 --> 00:03:24,000
 without a second thought.

68
00:03:24,000 --> 00:03:28,000
 There's people who think that's wasteful as well.

69
00:03:28,000 --> 00:03:34,230
 But if you compare the two, the difference between giving

70
00:03:34,230 --> 00:03:35,000
 10% of your salary

71
00:03:35,000 --> 00:03:40,000
 to a priest or a minister, and just giving food,

72
00:03:40,000 --> 00:03:44,320
 like a portion of the food that you make for yourself

73
00:03:44,320 --> 00:03:45,000
 anyway,

74
00:03:45,000 --> 00:03:49,460
 or in the case of restaurants, a portion of the food that

75
00:03:49,460 --> 00:03:51,000
 they have on hand,

76
00:03:51,000 --> 00:03:55,000
 which really doesn't cost that much to them anyway,

77
00:03:55,000 --> 00:04:00,630
 it's really putting it on that sort of a scale makes it

78
00:04:00,630 --> 00:04:01,000
 seem like

79
00:04:01,000 --> 00:04:07,000
 it's not really of any burden to the people.

80
00:04:07,000 --> 00:04:12,140
 On the other hand, it's in fact a wonderful thing for them

81
00:04:12,140 --> 00:04:13,000
 to do.

82
00:04:13,000 --> 00:04:17,000
 It's charity. And supporting someone who you believe in.

83
00:04:17,000 --> 00:04:19,330
 If you believe in a monk, if you believe that they're doing

84
00:04:19,330 --> 00:04:20,000
 something good,

85
00:04:20,000 --> 00:04:23,000
 then to give them something is a wonderful thing for you.

86
00:04:23,000 --> 00:04:26,000
 I've done it before myself. I do it often.

87
00:04:26,000 --> 00:04:28,000
 When I see monks, I put food in them.

88
00:04:28,000 --> 00:04:33,090
 I do all my birthday every year. I try to give alms to a

89
00:04:33,090 --> 00:04:35,000
 group of monks.

90
00:04:35,000 --> 00:04:39,000
 It's something that makes you feel good, giving to people.

91
00:04:39,000 --> 00:04:43,000
 The Buddha himself was very clear about this.

92
00:04:43,000 --> 00:04:47,000
 He said the benefits of getting food are one day of food.

93
00:04:47,000 --> 00:04:52,000
 But the benefits of giving food are immeasurable.

94
00:04:52,000 --> 00:04:55,000
 Hundreds or thousands of times better,

95
00:04:55,000 --> 00:04:58,110
 because the benefit that you gain, you keep with you and

96
00:04:58,110 --> 00:04:59,000
 you keep it in your heart.

97
00:04:59,000 --> 00:05:01,690
 When you do something good, when you help someone, when you

98
00:05:01,690 --> 00:05:03,000
 give something,

99
00:05:03,000 --> 00:05:05,780
 you get a much greater benefit than the person who receives

100
00:05:05,780 --> 00:05:06,000
.

101
00:05:06,000 --> 00:05:09,840
 So the almshadow is something that I've done for my whole

102
00:05:09,840 --> 00:05:11,000
 monk's life.

103
00:05:11,000 --> 00:05:14,490
 I've done it everywhere. I've done it in forests, I've done

104
00:05:14,490 --> 00:05:15,000
 it in cities,

105
00:05:15,000 --> 00:05:20,000
 I've done it on a mountain, going up and down the mountain.

106
00:05:20,000 --> 00:05:26,810
 I've walked many, many miles each day, even today, walking

107
00:05:26,810 --> 00:05:28,000
 in the sun.

108
00:05:28,000 --> 00:05:31,000
 I think it's hard to say that monks are lazy,

109
00:05:31,000 --> 00:05:36,000
 considering the rigid set of rules that we have to abide by

110
00:05:36,000 --> 00:05:36,000
.

111
00:05:36,000 --> 00:05:39,000
 That's part of the reason why we abide by the rules.

112
00:05:39,000 --> 00:05:43,910
 Because we acknowledge that we don't want to be caught up

113
00:05:43,910 --> 00:05:45,000
 in the world.

114
00:05:45,000 --> 00:05:50,000
 And yet, we have no right to expect anything.

115
00:05:50,000 --> 00:05:53,200
 We have no right to expect the luxuries of people who live

116
00:05:53,200 --> 00:05:55,000
 in the world as a result.

117
00:05:55,000 --> 00:06:00,360
 Sometimes it looks like I'm living in luxury, even as a

118
00:06:00,360 --> 00:06:01,000
 monk.

119
00:06:01,000 --> 00:06:05,000
 I have a computer and so on.

120
00:06:05,000 --> 00:06:09,000
 But there are so many rules that I have to abide by

121
00:06:09,000 --> 00:06:13,660
 that make it really impossible for me to become caught up

122
00:06:13,660 --> 00:06:15,000
 in these things,

123
00:06:15,000 --> 00:06:22,000
 or to become, to live a luxurious life.

124
00:06:22,000 --> 00:06:24,000
 The clothes I own are the clothes that you see on me.

125
00:06:24,000 --> 00:06:26,000
 I don't have a second set.

126
00:06:26,000 --> 00:06:28,000
 The bed I sleep on is the floor.

127
00:06:28,000 --> 00:06:30,000
 The food I eat I eat one meal a day.

128
00:06:30,000 --> 00:06:39,000
 I really think it's not much that you could point a finger

129
00:06:39,000 --> 00:06:39,000
 at

130
00:06:39,000 --> 00:06:42,760
 to say that, "Oh, look at the monkeys getting some free

131
00:06:42,760 --> 00:06:44,000
 food every day."

132
00:06:44,000 --> 00:06:47,000
 Because that's the only food I eat.

133
00:06:47,000 --> 00:06:49,000
 And it's being given out of faith.

134
00:06:49,000 --> 00:06:55,150
 It's being given purely with no desire for anything in

135
00:06:55,150 --> 00:06:56,000
 return.

136
00:06:56,000 --> 00:06:59,250
 Simply for the sheer pleasure of being able to give

137
00:06:59,250 --> 00:07:00,000
 something,

138
00:07:00,000 --> 00:07:02,940
 being able to help someone, being able to support them for

139
00:07:02,940 --> 00:07:04,000
 another day.

140
00:07:04,000 --> 00:07:09,630
 The Buddha said it's like a honey bee that goes and poll

141
00:07:09,630 --> 00:07:11,000
inates the flower

142
00:07:11,000 --> 00:07:17,050
 and takes the pollen, harms neither themselves nor the

143
00:07:17,050 --> 00:07:18,000
 flower.

144
00:07:18,000 --> 00:07:21,000
 The arms round is not something exorbitant

145
00:07:21,000 --> 00:07:27,000
 where we're asking for anything huge or out of hand.

146
00:07:27,000 --> 00:07:30,680
 But it's enough to keep us alive, and it's something that

147
00:07:30,680 --> 00:07:32,000
 keeps other people spiritually alive.

148
00:07:32,000 --> 00:07:34,380
 It's something that's a support for their own spiritual

149
00:07:34,380 --> 00:07:35,000
 development

150
00:07:35,000 --> 00:07:38,000
 and a support for our spiritual development.

151
00:07:38,000 --> 00:07:42,000
 So that's in brief what it means to go on arms round.

152
00:07:42,000 --> 00:07:45,000
 I often have my students follow me on arms round

153
00:07:45,000 --> 00:07:47,000
 so they can understand it and appreciate it much more.

154
00:07:47,000 --> 00:07:52,000
 I've had people from countries around the world follow me

155
00:07:52,000 --> 00:07:53,000
 on arms round

156
00:07:53,000 --> 00:07:58,260
 and literally break down and cry when they see the

157
00:07:58,260 --> 00:08:04,000
 wonderful, heartfelt gesture given by people.

158
00:08:04,000 --> 00:08:08,000
 Sometimes people who don't have much at all.

159
00:08:08,000 --> 00:08:12,580
 I've been to places where people are themselves living in

160
00:08:12,580 --> 00:08:14,000
 difficult situations.

161
00:08:14,000 --> 00:08:17,000
 And yet they always give something, give a little bit.

162
00:08:17,000 --> 00:08:23,000
 Something that's the food that they give is not a lot.

163
00:08:23,000 --> 00:08:25,000
 The food that they have is not a lot.

164
00:08:25,000 --> 00:08:27,000
 But they want to share.

165
00:08:27,000 --> 00:08:31,000
 And they feel great and they feel happy and they're smiling

166
00:08:31,000 --> 00:08:31,000
.

167
00:08:31,000 --> 00:08:35,000
 And it's really a wonderful thing, so I wouldn't change it.

168
00:08:35,000 --> 00:08:38,630
 I myself am a generous person. I don't feel ashamed to say

169
00:08:38,630 --> 00:08:39,000
 that.

170
00:08:39,000 --> 00:08:41,570
 So I don't feel bad when I receive the gifts from other

171
00:08:41,570 --> 00:08:42,000
 people

172
00:08:42,000 --> 00:08:45,000
 and they say to myself, "Give gifts."

173
00:08:45,000 --> 00:08:47,000
 I don't think there's anything.

174
00:08:47,000 --> 00:08:51,140
 I just somehow think that people often misunderstand or,

175
00:08:51,140 --> 00:08:53,000
 you know, it's a foreign thing.

176
00:08:53,000 --> 00:08:55,000
 This is why I'm being stopped by the police.

177
00:08:55,000 --> 00:08:57,000
 So I think it's good to explain these sort of things.

178
00:08:57,000 --> 00:09:00,530
 I hope that helped to clear some things up and make it

179
00:09:00,530 --> 00:09:02,000
 clear a little bit more,

180
00:09:02,000 --> 00:09:05,540
 a little bit more clear about what I'm doing and how I live

181
00:09:05,540 --> 00:09:06,000
 my life.

182
00:09:06,000 --> 00:09:13,000
 So that's the latest in this life in a day of a month.

