1
00:00:00,000 --> 00:00:04,000
 Hi, and welcome back to Ask a Monk.

2
00:00:04,000 --> 00:00:07,840
 Today I'd like to address several questions that have come

3
00:00:07,840 --> 00:00:11,280
 in in regards to the issue

4
00:00:11,280 --> 00:00:19,580
 specifically of celibacy and of sensuality in general.

5
00:00:19,580 --> 00:00:23,880
 Because it seems like a lot of people have a hard time

6
00:00:23,880 --> 00:00:26,700
 accepting or understanding the

7
00:00:26,700 --> 00:00:33,560
 reason for monks in particular, or Buddhists in general,

8
00:00:33,560 --> 00:00:35,940
 for giving up things like beauty

9
00:00:35,940 --> 00:00:42,080
 or sensuality or even sexuality.

10
00:00:42,080 --> 00:00:48,900
 And I think the reason for this comes from people's

11
00:00:48,900 --> 00:00:52,760
 understanding that what is natural

12
00:00:52,760 --> 00:01:01,030
 or understanding that the human state is natural is equal

13
00:01:01,030 --> 00:01:03,880
 to what is right.

14
00:01:03,880 --> 00:01:07,440
 Something is natural, therefore it is right.

15
00:01:07,440 --> 00:01:12,280
 And so we look at the human state and we see all of these

16
00:01:12,280 --> 00:01:15,480
 emotions like lust and attraction

17
00:01:15,480 --> 00:01:17,880
 and appreciation and so on.

18
00:01:17,880 --> 00:01:22,620
 And we say that while these are natural and this is a part

19
00:01:22,620 --> 00:01:25,000
 of nature and therefore it

20
00:01:25,000 --> 00:01:27,280
 is right.

21
00:01:27,280 --> 00:01:31,520
 And I think right away if we look at this idea of nature

22
00:01:31,520 --> 00:01:34,040
 equals right, we can see that

23
00:01:34,040 --> 00:01:39,510
 it's not really valid considering that much of what occurs

24
00:01:39,510 --> 00:01:41,880
 in nature is considered by

25
00:01:41,880 --> 00:01:48,030
 human standards to be immoral and improper, incorrect,

26
00:01:48,030 --> 00:01:48,680
 wrong.

27
00:01:48,680 --> 00:01:52,740
 If we look in the natural world we see things like rape,

28
00:01:52,740 --> 00:01:55,360
 murders, theft, and we see that

29
00:01:55,360 --> 00:02:02,930
 the natural world has no problem with these things to a

30
00:02:02,930 --> 00:02:04,680
 certain extent.

31
00:02:04,680 --> 00:02:07,540
 And I would further say that there's no reason to believe

32
00:02:07,540 --> 00:02:09,240
 this, that what is natural is what

33
00:02:09,240 --> 00:02:10,400
 is right.

34
00:02:10,400 --> 00:02:15,010
 In fact, according to Buddhism, our natural state is one

35
00:02:15,010 --> 00:02:17,440
 that arises from delusion.

36
00:02:17,440 --> 00:02:20,100
 And the difference in understanding here is that for most

37
00:02:20,100 --> 00:02:21,320
 people we're like a fish in

38
00:02:21,320 --> 00:02:22,520
 water.

39
00:02:22,520 --> 00:02:26,880
 All we know is the environment that we've grown up with.

40
00:02:26,880 --> 00:02:29,760
 And this is how we look at the world, how we see the world

41
00:02:29,760 --> 00:02:31,160
 is like through the eyes

42
00:02:31,160 --> 00:02:33,680
 of a fish when it looks in the water.

43
00:02:33,680 --> 00:02:37,460
 And if you try to talk to a fish or if a fish were to try

44
00:02:37,460 --> 00:02:40,000
 to understand something like dry

45
00:02:40,000 --> 00:02:48,520
 land, you can imagine that it would be totally foreign to

46
00:02:48,520 --> 00:02:52,360
 this sort of life form.

47
00:02:52,360 --> 00:02:54,940
 And human beings are in general the same.

48
00:02:54,940 --> 00:02:57,660
 We have this set way of being and we think, "Well, this is

49
00:02:57,660 --> 00:02:58,600
 a natural way."

50
00:02:58,600 --> 00:03:01,470
 And this is why, for instance, many people have a hard time

51
00:03:01,470 --> 00:03:02,840
 believing in things like

52
00:03:02,840 --> 00:03:07,160
 angels or gods or ghosts or beings which are considered to

53
00:03:07,160 --> 00:03:09,760
 be unnatural or supernatural.

54
00:03:09,760 --> 00:03:12,640
 And the only thing that's unnatural or supernatural about

55
00:03:12,640 --> 00:03:14,200
 them is that they're not human.

56
00:03:14,200 --> 00:03:19,320
 And for some reason we seem to believe that this experience

57
00:03:19,320 --> 00:03:21,800
 is somehow more natural.

58
00:03:21,800 --> 00:03:24,900
 It's reasonable because it's clearly observable, whereas

59
00:03:24,900 --> 00:03:26,920
 these other things are not observable

60
00:03:26,920 --> 00:03:29,340
 to us.

61
00:03:29,340 --> 00:03:31,900
 What Buddhism does or what the Buddha did was go beyond

62
00:03:31,900 --> 00:03:33,260
 this and the Buddha was able

63
00:03:33,260 --> 00:03:36,510
 to see things that most people aren't able to see, just as

64
00:03:36,510 --> 00:03:38,160
 a physicist is often in general

65
00:03:38,160 --> 00:03:41,940
 to, is in general often able to understand things that

66
00:03:41,940 --> 00:03:44,100
 ordinary people are unable to

67
00:03:44,100 --> 00:03:49,760
 understand and to therefore see things that ordinary people

68
00:03:49,760 --> 00:03:51,820
 are not able to see.

69
00:03:51,820 --> 00:03:56,760
 And in Buddhism, so we go beyond the human experience.

70
00:03:56,760 --> 00:04:01,290
 We go beyond this idea of what is natural to understand

71
00:04:01,290 --> 00:04:04,080
 that there's a lot more to existence

72
00:04:04,080 --> 00:04:10,480
 and to reality than this limited experience.

73
00:04:10,480 --> 00:04:14,270
 And what we come to see about this experience, when we look

74
00:04:14,270 --> 00:04:16,280
 at it objectively and we stop

75
00:04:16,280 --> 00:04:17,840
 saying, "Well, this is natural.

76
00:04:17,840 --> 00:04:22,050
 This is, therefore, this is the right way of being," what

77
00:04:22,050 --> 00:04:23,680
 we see is that in fact the

78
00:04:23,680 --> 00:04:31,380
 human state of being, of existence, is one of attraction,

79
00:04:31,380 --> 00:04:35,840
 addiction, gratification, and

80
00:04:35,840 --> 00:04:39,920
 future and further addiction or greater addiction.

81
00:04:39,920 --> 00:04:42,240
 If we look at the chemistry in the mind, if we look at how

82
00:04:42,240 --> 00:04:43,520
 addiction works, we can see

83
00:04:43,520 --> 00:04:47,300
 that in everything we do, the whole of our existence and

84
00:04:47,300 --> 00:04:49,560
 our gratification and the human

85
00:04:49,560 --> 00:04:56,620
 happiness is one of addiction, is one of continued

86
00:04:56,620 --> 00:05:00,280
 increased attachment.

87
00:05:00,280 --> 00:05:03,600
 If we look at how the receptors in the brain work, these

88
00:05:03,600 --> 00:05:05,880
 chemicals arise and they're received

89
00:05:05,880 --> 00:05:09,680
 by the receptors when we have a pleasant feeling.

90
00:05:09,680 --> 00:05:13,650
 And so that gives us this sense of calm, of peace, of

91
00:05:13,650 --> 00:05:14,920
 contentment.

92
00:05:14,920 --> 00:05:18,950
 So what happens is that reaction or that coupling of the

93
00:05:18,950 --> 00:05:22,240
 chemical and the receptor weakens the

94
00:05:22,240 --> 00:05:23,240
 receptor.

95
00:05:23,240 --> 00:05:27,080
 So next time, it takes more of the chemical and hence more

96
00:05:27,080 --> 00:05:29,080
 of the phenomena, more of the

97
00:05:29,080 --> 00:05:32,520
 experience to make us happy.

98
00:05:32,520 --> 00:05:36,240
 This is why for many people marriage is often not

99
00:05:36,240 --> 00:05:40,280
 satisfying and people will engage in extramarital

100
00:05:40,280 --> 00:05:41,840
 affairs.

101
00:05:41,840 --> 00:05:45,510
 That's why we find ourselves having to seek out new

102
00:05:45,510 --> 00:05:48,360
 pleasures all the time, greater and

103
00:05:48,360 --> 00:05:52,260
 greater and more exotic pleasures, why we can't be content

104
00:05:52,260 --> 00:05:54,000
 with the same stimulus over

105
00:05:54,000 --> 00:05:55,680
 and over again.

106
00:05:55,680 --> 00:05:58,000
 It's a well-documented fact.

107
00:05:58,000 --> 00:06:01,540
 In meditation, this becomes even more clear and from a more

108
00:06:01,540 --> 00:06:03,360
 objective point of view that

109
00:06:03,360 --> 00:06:05,760
 we can actually understand for ourselves.

110
00:06:05,760 --> 00:06:08,540
 We can see that when we get what we want, we only want it

111
00:06:08,540 --> 00:06:09,120
 more.

112
00:06:09,120 --> 00:06:11,840
 If we sit in meditation and we feel some happy feeling, we

113
00:06:11,840 --> 00:06:13,320
'll find the next time when we

114
00:06:13,320 --> 00:06:19,770
 said we are looking for that feeling, we come to attach to

115
00:06:19,770 --> 00:06:20,480
 it.

116
00:06:20,480 --> 00:06:25,640
 And so why do monks maintain celibacy?

117
00:06:25,640 --> 00:06:33,160
 Because a monk is someone who has vowed or set themselves

118
00:06:33,160 --> 00:06:38,240
 on becoming free from all sensuality,

119
00:06:38,240 --> 00:06:39,760
 from all addiction.

120
00:06:39,760 --> 00:06:42,290
 A monk is someone who has decided that what the Buddha

121
00:06:42,290 --> 00:06:44,000
 taught was correct, that there's

122
00:06:44,000 --> 00:06:46,620
 nothing in the world worth clinging to and therefore they

123
00:06:46,620 --> 00:06:47,840
're going to give it up.

124
00:06:47,840 --> 00:06:52,470
 Now, obviously someone who's engaging in sexuality or even

125
00:06:52,470 --> 00:06:54,920
 most forms of sensuality is going

126
00:06:54,920 --> 00:06:57,040
 in the opposite direction.

127
00:06:57,040 --> 00:07:00,950
 They've affirmed in themselves that this is a good thing,

128
00:07:00,950 --> 00:07:03,560
 that this sort of thing is beneficial

129
00:07:03,560 --> 00:07:07,400
 and therefore it's totally inappropriate for someone who's

130
00:07:07,400 --> 00:07:09,720
 on the path, even for meditators.

131
00:07:09,720 --> 00:07:13,790
 On a more practical level, just in brief, you can

132
00:07:13,790 --> 00:07:17,440
 understand that it makes it very difficult

133
00:07:17,440 --> 00:07:21,790
 for you to meditate when you're engaging in sensuality,

134
00:07:21,790 --> 00:07:24,160
 even listening to music or watching

135
00:07:24,160 --> 00:07:26,160
 movies or so on.

136
00:07:26,160 --> 00:07:28,870
 These sorts of things you'll find are totally contrary to

137
00:07:28,870 --> 00:07:30,440
 your meditation practice and if

138
00:07:30,440 --> 00:07:34,040
 you engage in both together, you'll find that you have to

139
00:07:34,040 --> 00:07:35,800
 choose one or the other, that you

140
00:07:35,800 --> 00:07:38,910
 won't be able to at the same time meditate and enjoy sens

141
00:07:38,910 --> 00:07:40,760
uality because sensuality is

142
00:07:40,760 --> 00:07:43,440
 something that intoxicates the mind.

143
00:07:43,440 --> 00:07:46,420
 It's something that gives rise to this chemical reaction

144
00:07:46,420 --> 00:07:48,280
 and then makes you very content for

145
00:07:48,280 --> 00:07:49,280
 a short time.

146
00:07:49,280 --> 00:07:51,800
 And so you don't have any thought that, "Oh boy, I have to

147
00:07:51,800 --> 00:07:54,120
 do something to get rid of this

148
00:07:54,120 --> 00:07:56,640
 desire or this addiction or so on."

149
00:07:56,640 --> 00:07:57,640
 You feel gratified.

150
00:07:57,640 --> 00:07:59,880
 You feel like the addiction is a good thing.

151
00:07:59,880 --> 00:08:02,750
 And then of course it creates more addiction and makes it

152
00:08:02,750 --> 00:08:04,160
 harder for you to attain the

153
00:08:04,160 --> 00:08:08,120
 gratification in the future and it's a cycle.

154
00:08:08,120 --> 00:08:13,390
 It also becomes habit forming so that we find ourselves

155
00:08:13,390 --> 00:08:17,360
 more and more requiring these things.

156
00:08:17,360 --> 00:08:22,440
 So for a monk, it's antithetical to our practice and it's

157
00:08:22,440 --> 00:08:24,440
 considered very bad form.

158
00:08:24,440 --> 00:08:30,550
 In fact, if a monk commits a sexual act, they're no longer

159
00:08:30,550 --> 00:08:31,680
 a monk.

160
00:08:31,680 --> 00:08:33,640
 They can't disrobe because they're no longer a monk.

161
00:08:33,640 --> 00:08:35,640
 They just take the robes off and go.

162
00:08:35,640 --> 00:08:41,750
 But if they continue to wear the robes, they're considered

163
00:08:41,750 --> 00:08:45,080
 to be wearing the robes improperly

164
00:08:45,080 --> 00:08:46,940
 and they're no longer a monk.

165
00:08:46,940 --> 00:08:49,840
 So I hope that clarifies it a little bit.

166
00:08:49,840 --> 00:08:54,280
 I think for many people they have no desire when they start

167
00:08:54,280 --> 00:08:56,960
 to meditate to give up sensuality

168
00:08:56,960 --> 00:08:59,560
 or sexuality.

169
00:08:59,560 --> 00:09:00,760
 And I think that's fine.

170
00:09:00,760 --> 00:09:03,440
 There's various levels of commitment.

171
00:09:03,440 --> 00:09:08,220
 It should be clear that a monk has committed themselves 100

172
00:09:08,220 --> 00:09:10,480
% to attaining full freedom from

173
00:09:10,480 --> 00:09:14,700
 addiction and therefore that's why there are these rules in

174
00:09:14,700 --> 00:09:16,440
 place to make sure that only

175
00:09:16,440 --> 00:09:21,390
 people who are really working for full freedom from

176
00:09:21,390 --> 00:09:25,600
 suffering should be able to, should ordain

177
00:09:25,600 --> 00:09:28,120
 and should become monks.

178
00:09:28,120 --> 00:09:30,680
 For those people who believe that this is wrong or

179
00:09:30,680 --> 00:09:32,640
 incorrect, then you're welcome to

180
00:09:32,640 --> 00:09:34,280
 go and seek another path.

181
00:09:34,280 --> 00:09:37,820
 If you're just looking to find some state of peace and

182
00:09:37,820 --> 00:09:39,920
 comfort or even states of basic

183
00:09:39,920 --> 00:09:41,970
 wisdom and understanding, you're welcome to meditate and

184
00:09:41,970 --> 00:09:43,640
 you don't have to become a monk

185
00:09:43,640 --> 00:09:46,720
 and you sure don't have to become celibate.

186
00:09:46,720 --> 00:09:49,120
 So I hope that helps to clear things up.

187
00:09:49,120 --> 00:09:50,120
 Thanks for the questions.

188
00:09:50,120 --> 00:09:50,620
 Keep them coming.

189
00:09:50,620 --> 00:09:51,620
 1

