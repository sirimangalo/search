1
00:00:00,000 --> 00:00:03,780
 Hi, welcome back to Ask a Monk. Next question comes from

2
00:00:03,780 --> 00:00:06,900
 @philosophia312

3
00:00:06,900 --> 00:00:10,810
 Who asks, "Is it possible to locate a person in their next

4
00:00:10,810 --> 00:00:11,560
 life?"

5
00:00:11,560 --> 00:00:16,880
 Very difficult

6
00:00:16,880 --> 00:00:21,300
 Yeah, technically possible

7
00:00:21,300 --> 00:00:27,120
 But I would say you know if this is in the realm of

8
00:00:28,480 --> 00:00:32,360
 very strong meditators or people who have

9
00:00:32,360 --> 00:00:36,400
 special abilities from past lives, you know, there are

10
00:00:36,400 --> 00:00:37,880
 people out there claiming to be psychics

11
00:00:37,880 --> 00:00:40,600
 able to communicate with the dead and

12
00:00:40,600 --> 00:00:45,830
 You know, obviously most of them are fakes and but there's

13
00:00:45,830 --> 00:00:48,920
 the potential that someone is actually able to do this

14
00:00:48,920 --> 00:00:52,500
 even more so if they're practicing meditation, but

15
00:00:52,500 --> 00:00:56,180
 You will find people who just have this ability carried

16
00:00:56,180 --> 00:00:57,500
 over from past lives

17
00:00:57,940 --> 00:01:00,140
 I wouldn't rely on that too much

18
00:01:00,140 --> 00:01:03,260
 Because obviously most people are fakes

19
00:01:03,260 --> 00:01:07,280
 Can you do it? I mean obviously if you turn if you practice

20
00:01:07,280 --> 00:01:10,180
 meditation for a while develop yourself

21
00:01:10,180 --> 00:01:12,420
 It's certainly possible

22
00:01:12,420 --> 00:01:15,600
 I'd like to propose some alternatives though

23
00:01:15,600 --> 00:01:20,930
 First of all because it's it's quite likely that you won't

24
00:01:20,930 --> 00:01:21,900
 be able to

25
00:01:22,900 --> 00:01:26,110
 Contact the person and that you might also delude yourself

26
00:01:26,110 --> 00:01:29,090
 into thinking that somehow you have made contact when in

27
00:01:29,090 --> 00:01:30,060
 fact you haven't

28
00:01:30,060 --> 00:01:33,300
 And

29
00:01:33,300 --> 00:01:34,900
 also because

30
00:01:34,900 --> 00:01:38,340
 It's I mean maybe the question to be asked you can ask

31
00:01:38,340 --> 00:01:41,410
 yourself is whether it's really necessary and this is a

32
00:01:41,410 --> 00:01:42,420
 challenge

33
00:01:42,420 --> 00:01:45,540
 because we cling to people

34
00:01:45,540 --> 00:01:48,140
 but

35
00:01:48,140 --> 00:01:51,360
 This is a part of I was talking about in Second Life if you

36
00:01:51,360 --> 00:01:53,780
 watch the my latest video in Second Life

37
00:01:53,780 --> 00:01:56,340
 October

38
00:01:56,340 --> 00:02:00,620
 10th or something 10th 9th 10th 10th

39
00:02:00,620 --> 00:02:09,040
 You know our perception of reality is so narrow and so as a

40
00:02:09,040 --> 00:02:11,580
 result we cling to things that

41
00:02:12,860 --> 00:02:16,600
 Can seem right quite ridiculous after you start getting

42
00:02:16,600 --> 00:02:19,220
 into meditation and objective

43
00:02:19,220 --> 00:02:25,900
 Observation of reality you start to realize that

44
00:02:25,900 --> 00:02:29,260
 humankind

45
00:02:29,260 --> 00:02:36,780
 You know we're a part of a much larger group of beings and

46
00:02:39,020 --> 00:02:43,140
 In a sense we're all family and we're we've all

47
00:02:43,140 --> 00:02:48,830
 Come from the same place and we're all in the same boat so

48
00:02:48,830 --> 00:02:52,660
 to focus on a single person who's left you is

49
00:02:52,660 --> 00:02:59,320
 Is really counterproductive because it's going to create

50
00:02:59,320 --> 00:03:01,080
 some sort of stress in your life

51
00:03:03,460 --> 00:03:08,000
 necessarily to no benefit except to appease our addiction

52
00:03:08,000 --> 00:03:09,500
 to that one person

53
00:03:09,500 --> 00:03:15,520
 When in fact happiness and and true and lasting happiness

54
00:03:15,520 --> 00:03:16,180
 and peace

55
00:03:16,180 --> 00:03:21,300
 Is much better found in

56
00:03:21,300 --> 00:03:24,620
 the friendship and

57
00:03:24,620 --> 00:03:28,820
 The harmony with the people who are around us. I mean ask

58
00:03:28,820 --> 00:03:29,380
 yourself

59
00:03:30,220 --> 00:03:32,720
 What's wrong with the people around me? What's wrong with

60
00:03:32,720 --> 00:03:35,240
 my relationship to them because actually they are perfect

61
00:03:35,240 --> 00:03:35,540
 as well

62
00:03:35,540 --> 00:03:40,050
 They are on the same level as whoever you've left behind.

63
00:03:40,050 --> 00:03:41,220
 They're just

64
00:03:41,220 --> 00:03:47,100
 Well well not not at the same level they're there they're

65
00:03:47,100 --> 00:03:50,220
 equally important, but they're at a different level of

66
00:03:50,220 --> 00:03:51,620
 development and

67
00:03:51,620 --> 00:03:55,140
 so

68
00:03:55,140 --> 00:03:56,740
 try to

69
00:03:56,740 --> 00:04:01,190
 to align yourself with a certain type of people rather than

70
00:04:01,190 --> 00:04:01,340
 a

71
00:04:01,340 --> 00:04:07,900
 Specific person try to find people who are caring and

72
00:04:07,900 --> 00:04:11,180
 Respond to all people

73
00:04:11,180 --> 00:04:15,540
 with love and compassion and harmony with the idea that

74
00:04:15,540 --> 00:04:19,900
 That they are people as well and

75
00:04:19,900 --> 00:04:25,020
 Finding find a way to be in harmony with the people around

76
00:04:25,020 --> 00:04:25,900
 you so that

77
00:04:26,700 --> 00:04:28,660
 your relationships

78
00:04:28,660 --> 00:04:33,540
 with them is is is what's most important because

79
00:04:33,540 --> 00:04:38,160
 when it's not then you're you're removing yourself from

80
00:04:38,160 --> 00:04:39,140
 reality and

81
00:04:39,140 --> 00:04:43,070
 What happens then is is generally our relationships with

82
00:04:43,070 --> 00:04:45,020
 the people around us go to?

83
00:04:45,020 --> 00:04:49,760
 Our ruined in favor of a relationship that we're not having

84
00:04:49,760 --> 00:04:51,300
 with someone else

85
00:04:51,820 --> 00:04:55,300
 We are unable to stand the behavior of other people. We don

86
00:04:55,300 --> 00:04:58,460
't put any effort into the reality the real

87
00:04:58,460 --> 00:05:02,300
 relationships that we're in the people who are really there

88
00:05:02,300 --> 00:05:02,940
 with us and

89
00:05:02,940 --> 00:05:08,470
 Our attention is focused on on our sorrow and sadness and

90
00:05:08,470 --> 00:05:11,140
 our clinging to someone who's not there and

91
00:05:11,140 --> 00:05:14,500
 someone who even if we

92
00:05:14,500 --> 00:05:17,580
 If we could contact them is still not with us

93
00:05:20,300 --> 00:05:23,930
 Then this is one part of it. I think it's very important

94
00:05:23,930 --> 00:05:24,900
 and quite difficult

95
00:05:24,900 --> 00:05:27,490
 This is a challenge to the way of our way of looking at

96
00:05:27,490 --> 00:05:27,900
 things

97
00:05:27,900 --> 00:05:30,900
 So I hope you can accept the challenge and and you can

98
00:05:30,900 --> 00:05:33,690
 decide for yourself whether I'm right or not whether this

99
00:05:33,690 --> 00:05:33,820
 is

100
00:05:33,820 --> 00:05:38,300
 an important thing or not and the other part of it is that

101
00:05:38,300 --> 00:05:43,180
 You know accepting our limitations because I can say that

102
00:05:43,180 --> 00:05:47,200
 but it doesn't make it true that no suddenly you you forget

103
00:05:47,200 --> 00:05:48,100
 about that person

104
00:05:48,100 --> 00:05:51,250
 And you're okay with accepting everybody people come and go

105
00:05:51,250 --> 00:05:52,700
 and you're able that's

106
00:05:52,700 --> 00:05:56,230
 Probably you know it's generally a long way away from where

107
00:05:56,230 --> 00:05:58,420
 we are when we start meditating

108
00:05:58,420 --> 00:06:00,780
 So there are tricks

109
00:06:00,780 --> 00:06:03,490
 I mean there are there are things that we can do to to help

110
00:06:03,490 --> 00:06:05,100
 ourselves help

111
00:06:05,100 --> 00:06:08,810
 alleviate the pain during the time of loss or during the

112
00:06:08,810 --> 00:06:09,380
 time of

113
00:06:09,380 --> 00:06:13,980
 Adaptation to the loss and while we're still clinging

114
00:06:15,420 --> 00:06:18,760
 And that is better than to find out where the person is is

115
00:06:18,760 --> 00:06:21,740
 to think of the person and to wish them peace

116
00:06:21,740 --> 00:06:25,020
 happiness and freedom from suffering because that is

117
00:06:25,020 --> 00:06:28,540
 Love and that is the most important part of our

118
00:06:28,540 --> 00:06:33,060
 relationship with them when we focus on that we realize we

119
00:06:33,060 --> 00:06:37,540
 Lose the other part which is causing us suffering because

120
00:06:37,540 --> 00:06:38,400
 love doesn't cling

121
00:06:38,400 --> 00:06:41,250
 Love is not something that needs to think about the person

122
00:06:41,250 --> 00:06:42,540
 the more you love someone

123
00:06:44,580 --> 00:06:47,350
 It's not the case that the more you love someone the more

124
00:06:47,350 --> 00:06:48,580
 you need to be near them

125
00:06:48,580 --> 00:06:51,540
 What we're talking about in the case of needing to be near

126
00:06:51,540 --> 00:06:53,850
 someone is attachment and I've talked about this before

127
00:06:53,850 --> 00:06:54,980
 love is wishing for

128
00:06:54,980 --> 00:06:57,640
 Them to be happy attachment is wanting them to make you

129
00:06:57,640 --> 00:06:59,830
 happy because it makes you happy when they're around and

130
00:06:59,830 --> 00:07:00,580
 you cling to that

131
00:07:00,580 --> 00:07:04,680
 When when we focus on love wishing for them to be happy

132
00:07:04,680 --> 00:07:07,260
 wishing for them to find peace thinking about them and the

133
00:07:07,260 --> 00:07:07,680
 best time

134
00:07:07,680 --> 00:07:10,000
 To do this is after you meditate once you've finished med

135
00:07:10,000 --> 00:07:13,220
itating before you open your eyes make a wish may I be happy

136
00:07:13,220 --> 00:07:14,300
 free from pain and suffering

137
00:07:14,700 --> 00:07:18,190
 May I may all the beings in in this house in this building

138
00:07:18,190 --> 00:07:20,260
 in this city in this country

139
00:07:20,260 --> 00:07:22,540
 They all beings when they all be happy

140
00:07:22,540 --> 00:07:26,660
 Find peace happiness freedom from suffering but but specify

141
00:07:26,660 --> 00:07:29,260
 that person as well somewhere in there

142
00:07:29,260 --> 00:07:32,610
 Even at the beginning made that person be happy wherever

143
00:07:32,610 --> 00:07:35,740
 they are now. May they be happy may they find peace

144
00:07:35,740 --> 00:07:39,420
 May they be free from suffering and you'll find that's

145
00:07:40,300 --> 00:07:44,220
 Probably the best weapon to fight the the craving

146
00:07:44,220 --> 00:07:47,300
 besides of course meditation itself and

147
00:07:47,300 --> 00:07:50,540
 the eventual realization of

148
00:07:50,540 --> 00:07:54,100
 objectivity and

149
00:07:54,100 --> 00:07:56,860
 Taking things as they come and realizing that we're all in

150
00:07:56,860 --> 00:08:00,560
 this together that that person is is on their own now

151
00:08:00,560 --> 00:08:02,560
 they're in a whole different place and

152
00:08:02,560 --> 00:08:06,210
 From a theoretical point of view. It's because that their

153
00:08:06,210 --> 00:08:07,500
 term with us has ended

154
00:08:08,260 --> 00:08:12,330
 Our relationship only went so far. They have other business

155
00:08:12,330 --> 00:08:14,940
 to take care of and there are a whole other group of people

156
00:08:14,940 --> 00:08:18,120
 Maybe we're included in that maybe they've already been

157
00:08:18,120 --> 00:08:19,440
 born and there are they've already been

158
00:08:19,440 --> 00:08:22,320
 Conceived and they're in someone's womb near nearby and we

159
00:08:22,320 --> 00:08:23,700
're gonna see them again as a child

160
00:08:23,700 --> 00:08:26,400
 We might not even recognize them might not even know it's

161
00:08:26,400 --> 00:08:26,740
 them

162
00:08:26,740 --> 00:08:30,120
 They might have been born as a dog in our house. They might

163
00:08:30,120 --> 00:08:31,900
 be an angel in our house

164
00:08:31,900 --> 00:08:34,530
 They might be somewhere nearby and they might be a part of

165
00:08:34,530 --> 00:08:35,060
 our reality

166
00:08:35,060 --> 00:08:37,260
 but

167
00:08:37,260 --> 00:08:40,820
 You know, they've changed and they're in a new mode and

168
00:08:40,820 --> 00:08:43,900
 that's fine. We have to move on as well our

169
00:08:43,900 --> 00:08:46,500
 story our play our

170
00:08:46,500 --> 00:08:51,800
 life is moving is is impermanent and so I'm being able to

171
00:08:51,800 --> 00:08:53,060
 deal with this and

172
00:08:53,060 --> 00:08:58,940
 Remove ourselves from from this

173
00:08:58,940 --> 00:09:02,650
 Idea false idea of the way things should be to an

174
00:09:02,650 --> 00:09:04,660
 understanding and the ability to adapt

175
00:09:05,180 --> 00:09:07,180
 to the way things actually are

176
00:09:07,180 --> 00:09:10,240
 That will bring us real and true peace happiness and

177
00:09:10,240 --> 00:09:13,670
 freedom from suffering. So I hope that helps. Thanks for

178
00:09:13,670 --> 00:09:15,100
 the question. Keep practicing

179
00:09:15,100 --> 00:09:17,100
 You

