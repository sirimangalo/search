1
00:00:00,000 --> 00:00:06,120
 I'm 18 and feel that I'm too young to follow the monk path

2
00:00:06,120 --> 00:00:08,520
 and work towards nirvana.

3
00:00:08,520 --> 00:00:11,860
 Wouldn't it be best for me to follow the materialistic path

4
00:00:11,860 --> 00:00:15,600
 now temporarily until I reach old age,

5
00:00:15,600 --> 00:00:22,680
 before walking the nirvana path as then I'd be more mature?

6
00:00:22,680 --> 00:00:34,640
 Right. First of all, there's no need to ever walk the monk

7
00:00:34,640 --> 00:00:35,200
 path.

8
00:00:35,200 --> 00:00:38,280
 If you're really serious about it, then the monk path is a

9
00:00:38,280 --> 00:00:39,040
 great one.

10
00:00:39,040 --> 00:00:48,640
 Second of all, I think there's a misleading premise here

11
00:00:48,640 --> 00:01:01,640
 that somehow physical maturity or age somehow implies

12
00:01:01,640 --> 00:01:02,720
 better practice.

13
00:01:02,720 --> 00:01:09,880
 I think in certain aspects of the practice are benefited by

14
00:01:09,880 --> 00:01:15,680
 physical maturity in the sense of greater experience of the

15
00:01:15,680 --> 00:01:16,240
 world.

16
00:01:16,240 --> 00:01:23,110
 But a great portion or a great part of the path is actually

17
00:01:23,110 --> 00:01:25,400
 hindered by age,

18
00:01:25,400 --> 00:01:29,800
 because especially in someone who has spent most of their

19
00:01:29,800 --> 00:01:33,120
 life following the materialistic path.

20
00:01:33,120 --> 00:01:40,730
 So the materialistic path, if I understand it, if you're

21
00:01:40,730 --> 00:01:42,280
 meaning it as I understand it,

22
00:01:42,280 --> 00:01:49,850
 is an unwholesome one. It's one that leads to clinging, it

23
00:01:49,850 --> 00:01:51,680
 leads to conflict,

24
00:01:51,680 --> 00:01:56,650
 it leads to delusion and attachment and identification, all

25
00:01:56,650 --> 00:01:58,280
 sorts of bad stuff.

26
00:01:58,280 --> 00:02:01,720
 And so when that becomes your habit throughout your life,

27
00:02:01,720 --> 00:02:08,160
 the idea that that would somehow make you a better medit

28
00:02:08,160 --> 00:02:12,320
ator is unreasonable, is unlikely.

29
00:02:12,320 --> 00:02:17,030
 Now if you are talking about not becoming a monk for a long

30
00:02:17,030 --> 00:02:21,640
 time, the idea of practicing meditation and doing good

31
00:02:21,640 --> 00:02:22,080
 deeds,

32
00:02:22,080 --> 00:02:25,480
 now to slowly, slowly cultivate enough goodness to become a

33
00:02:25,480 --> 00:02:27,760
 monk, well that's a different argument,

34
00:02:27,760 --> 00:02:30,910
 but I still don't think it holds because being a monk is

35
00:02:30,910 --> 00:02:34,160
 even more difficult than practicing as a layperson

36
00:02:34,160 --> 00:02:36,480
 physically and as a lifestyle.

37
00:02:36,480 --> 00:02:41,180
 A person who has lived their whole life not used to the mon

38
00:02:41,180 --> 00:02:45,560
astic discipline, the life of being a monk,

39
00:02:45,560 --> 00:02:50,330
 will have a very difficult time in their old age practicing

40
00:02:50,330 --> 00:02:52,280
 all of the many rules,

41
00:02:52,280 --> 00:02:57,040
 eating only once a day and so on. It doesn't get easier

42
00:02:57,040 --> 00:03:01,280
 because you've waited a long time to begin it.

43
00:03:01,280 --> 00:03:05,370
 So even if it's about becoming a monk, absolutely the

44
00:03:05,370 --> 00:03:09,280
 Buddha himself recommended that it's for young people to do

45
00:03:09,280 --> 00:03:09,440
.

46
00:03:09,440 --> 00:03:13,280
 Ordaining when you're 20 is a perfect time, the best time.

47
00:03:13,280 --> 00:03:19,850
 Even if it's difficult at first, for a person who's able to

48
00:03:19,850 --> 00:03:26,080
 practice it, it's really the best time of one's life

49
00:03:26,080 --> 00:03:31,240
 because one's able to grow up as a monk, grow up surrounded

50
00:03:31,240 --> 00:03:35,280
 by the Dhamma, surrounded by the Buddha's teaching.

51
00:03:35,280 --> 00:03:38,880
 So certainly no benefit to walking the materialistic path.

52
00:03:38,880 --> 00:03:42,000
 I don't think any benefit if you want to become a monk for

53
00:03:42,000 --> 00:03:47,040
 waiting until you're very old or old at all really.

54
00:03:47,040 --> 00:03:50,170
 Eighteen while you have to be 20 to ordain and you might

55
00:03:50,170 --> 00:03:51,920
 want to wait a little bit longer,

56
00:03:51,920 --> 00:03:56,000
 but certainly don't ever follow the materialistic path, it

57
00:03:56,000 --> 00:03:58,800
's not useful for anyone in any situation.

58
00:03:58,800 --> 00:04:02,850
 But as for following a lay person's path, that's reasonable

59
00:04:02,850 --> 00:04:02,880
.

60
00:04:02,880 --> 00:04:05,320
 But don't think that it's going to make it easier to become

61
00:04:05,320 --> 00:04:06,480
 a monk when you're old.

62
00:04:06,480 --> 00:04:10,380
 It just might make you a bad monk, one of those monks who

63
00:04:10,380 --> 00:04:12,800
 just sits in their room and watches television all day.

