1
00:00:00,000 --> 00:00:04,130
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,130 --> 00:00:05,480
 Today we continue on with

3
00:00:05,480 --> 00:00:10,960
 verse 123 which reads as follows.

4
00:00:10,960 --> 00:00:17,000
 "Wani joa bayam mangaapasa to maha dano"

5
00:00:17,000 --> 00:00:24,760
 "wisang ji whitaka moa pa pa nipali wan ji"

6
00:00:24,760 --> 00:00:35,320
 Which means just as a merchant relates to dangers on the

7
00:00:35,320 --> 00:00:36,760
 path or a dangerous

8
00:00:36,760 --> 00:00:45,180
 path when he has apa sattu, few weapons or few defenses, m

9
00:00:45,180 --> 00:00:46,640
aha dano but great wealth.

10
00:00:46,640 --> 00:01:01,370
 Or just as a person ji whitaka maa, one who desires to live

11
00:01:01,370 --> 00:01:03,480
, relates to poison.

12
00:01:03,480 --> 00:01:11,280
 "papa nipali wan ji" one should avoid evil.

13
00:01:11,280 --> 00:01:24,090
 So we have two analogies here. The first is a merchant, how

14
00:01:24,090 --> 00:01:25,640
 a merchant avoids a

15
00:01:25,640 --> 00:01:29,000
 dangerous path, a merchant who has little defenses but

16
00:01:29,000 --> 00:01:32,960
 great wealth. And the second

17
00:01:32,960 --> 00:01:41,790
 is a person desiring to live, avoids poison, doesn't eat or

18
00:01:41,790 --> 00:01:43,800
 drink poison. In the same

19
00:01:43,800 --> 00:01:51,880
 way we should avoid evil. Simple verse, it's also a fairly

20
00:01:51,880 --> 00:01:52,920
 simple story, it's

21
00:01:52,920 --> 00:01:56,350
 actually not so much related. This is another example of

22
00:01:56,350 --> 00:01:57,600
 how the Buddha used a

23
00:01:57,600 --> 00:02:04,410
 story to seem as an excuse to give a teaching relating some

24
00:02:04,410 --> 00:02:08,000
 fairly unmeaningful

25
00:02:08,000 --> 00:02:13,710
 discussion, turning it into meaningful teaching. So the

26
00:02:13,710 --> 00:02:14,600
 story goes that there

27
00:02:14,600 --> 00:02:21,620
 were these thieves who were trying to rob this very rich

28
00:02:21,620 --> 00:02:23,720
 merchant and he went out

29
00:02:23,720 --> 00:02:27,860
 on travels but he took monks with him. So he had lots of

30
00:02:27,860 --> 00:02:29,480
 supplies and he invited

31
00:02:29,480 --> 00:02:33,950
 monks to travel with him which was convenient for the monks

32
00:02:33,950 --> 00:02:35,960
 because he would

33
00:02:35,960 --> 00:02:39,960
 have supplies for them. When traveling it's hard to find a

34
00:02:39,960 --> 00:02:40,880
 place and to know

35
00:02:40,880 --> 00:02:44,500
 where to go for alms. So when a monk is staying in a

36
00:02:44,500 --> 00:02:47,080
 village they acquire people

37
00:02:47,080 --> 00:02:54,360
 who support them, who treat them as a part of their society

38
00:02:54,360 --> 00:02:55,880
 and give them food.

39
00:02:55,880 --> 00:02:58,340
 But when a monk goes to a new place, I mean even in a

40
00:02:58,340 --> 00:02:59,560
 Buddhist country this is

41
00:02:59,560 --> 00:03:02,740
 still the case, when you go to a new place it's hard to

42
00:03:02,740 --> 00:03:03,920
 expect that you're

43
00:03:03,920 --> 00:03:09,360
 going to receive alms or more especially when you're

44
00:03:09,360 --> 00:03:14,120
 traveling. So they

45
00:03:14,120 --> 00:03:17,600
 went with his merchant who said that he would care for them

46
00:03:17,600 --> 00:03:19,160
 and feed them along

47
00:03:19,160 --> 00:03:23,600
 the way. So they went together, there was a whole bunch of

48
00:03:23,600 --> 00:03:24,080
 monks, the

49
00:03:24,080 --> 00:03:29,160
 commentaries was 500 but I think 500 is just code for a lot

50
00:03:29,160 --> 00:03:33,980
, a large group. And he

51
00:03:33,980 --> 00:03:41,310
 got to a certain village and was resting there when these

52
00:03:41,310 --> 00:03:43,480
 thieves caught up with

53
00:03:43,480 --> 00:03:47,940
 him and lay and wait so that when he left the village they

54
00:03:47,940 --> 00:03:49,160
 would be able to

55
00:03:49,160 --> 00:03:53,770
 rob him. And they waited and they didn't see him coming

56
00:03:53,770 --> 00:03:54,720
 through the village so

57
00:03:54,720 --> 00:03:57,730
 they assumed he was there. They wanted to find out when he

58
00:03:57,730 --> 00:03:59,020
 was going to leave so

59
00:03:59,020 --> 00:04:04,850
 they could prepare an ambush and rob him. And so one of the

60
00:04:04,850 --> 00:04:06,000
 thieves went to a

61
00:04:06,000 --> 00:04:09,780
 friend he had in the city and asked him about it and the

62
00:04:09,780 --> 00:04:11,320
 man found out for him.

63
00:04:11,320 --> 00:04:19,720
 I went to the merchant and asked him

64
00:04:19,720 --> 00:04:25,570
 and found out and told him that in two days the merchant

65
00:04:25,570 --> 00:04:27,240
 was going to set out

66
00:04:27,240 --> 00:04:32,420
 and so this thief went back to his friends and got them all

67
00:04:32,420 --> 00:04:33,920
 ready.

68
00:04:40,360 --> 00:04:47,760
 And then the merchant, this friend of his somehow, it's

69
00:04:47,760 --> 00:04:48,520
 kind of strange actually,

70
00:04:48,520 --> 00:04:51,990
 maybe you should read the Pali but the English as it's

71
00:04:51,990 --> 00:04:54,520
 written he goes to the

72
00:04:54,520 --> 00:04:59,550
 merchant and lets the merchant know which is reasonable to

73
00:04:59,550 --> 00:05:00,640
 that extent it's

74
00:05:00,640 --> 00:05:04,530
 understandable because it's like he feels guilty especially

75
00:05:04,530 --> 00:05:05,400
 he mentions that

76
00:05:05,400 --> 00:05:08,840
 there's 500 monks with this merchant so there's a large

77
00:05:08,840 --> 00:05:10,000
 group of monks with this

78
00:05:10,000 --> 00:05:12,860
 merchant so if he doesn't warn the merchant it's not just

79
00:05:12,860 --> 00:05:13,720
 going to hurt the

80
00:05:13,720 --> 00:05:17,950
 merchant but it's also going to impact hurt and maybe kill

81
00:05:17,950 --> 00:05:20,120
 some of these monks.

82
00:05:20,120 --> 00:05:23,120
 So he goes and tells the merchant that there's a band of

83
00:05:23,120 --> 00:05:24,400
 thieves laying and wait

84
00:05:24,400 --> 00:05:28,780
 for him. So all well and good but then the thieves come

85
00:05:28,780 --> 00:05:30,920
 back to the guy and he

86
00:05:30,920 --> 00:05:35,200
 tells them basically that the merchant has changed his mind

87
00:05:35,200 --> 00:05:35,720
. The merchant

88
00:05:35,720 --> 00:05:39,770
 decides to head home so he turns around and get prepared to

89
00:05:39,770 --> 00:05:40,720
 head out the other

90
00:05:40,720 --> 00:05:45,520
 gate and he tells the thieves this which is kind of strange

91
00:05:45,520 --> 00:05:47,560
 how he's talking to

92
00:05:47,560 --> 00:05:51,310
 both sides so the thieves turn around go to the other side

93
00:05:51,310 --> 00:05:52,040
 of the village and

94
00:05:52,040 --> 00:05:55,450
 they prepare an ambush on the road back and the man goes

95
00:05:55,450 --> 00:05:56,760
 back to the merchant

96
00:05:56,760 --> 00:06:01,350
 and lets him know that. Maybe something's lost in the

97
00:06:01,350 --> 00:06:02,520
 translation it seems like

98
00:06:02,520 --> 00:06:10,320
 God behaved to me but anyway it's just a story. Finally the

99
00:06:10,320 --> 00:06:12,960
 merchant not being

100
00:06:12,960 --> 00:06:15,240
 able to go forward and now knowing that if he goes back

101
00:06:15,240 --> 00:06:16,040
 same thing's going to

102
00:06:16,040 --> 00:06:18,890
 happen and that he really doesn't have the defenses says to

103
00:06:18,890 --> 00:06:21,240
 the monks look I

104
00:06:21,240 --> 00:06:24,270
 think there's no reason for me to move on I'm just going to

105
00:06:24,270 --> 00:06:25,240
 stay here in this

106
00:06:25,240 --> 00:06:29,720
 village set up and sell my wares here see how I can do

107
00:06:29,720 --> 00:06:31,360
 because clearly there's

108
00:06:31,360 --> 00:06:38,180
 danger waiting for me if I leave. And so the monks hearing

109
00:06:38,180 --> 00:06:39,640
 this discussed among

110
00:06:39,640 --> 00:06:42,510
 themselves and decided there was no way they could continue

111
00:06:42,510 --> 00:06:43,440
 along the path and

112
00:06:43,440 --> 00:06:46,610
 the best thing for them was to return back to the Buddha

113
00:06:46,610 --> 00:06:49,200
 which they did. When

114
00:06:49,200 --> 00:06:52,280
 they came to the Buddha they told him the story and the

115
00:06:52,280 --> 00:06:54,080
 Buddha used this as an

116
00:06:54,080 --> 00:07:03,080
 excuse or as an example a comparison to say look here's a

117
00:07:03,080 --> 00:07:05,160
 guy who knows danger

118
00:07:05,160 --> 00:07:10,120
 when he sees it and so he doesn't set out on the path when

119
00:07:10,120 --> 00:07:10,880
 there's danger on

120
00:07:10,880 --> 00:07:19,920
 the path and likewise you all who desire good and who

121
00:07:19,920 --> 00:07:22,800
 understand the danger of

122
00:07:22,800 --> 00:07:27,410
 evil should refrain from evil just as and then he gives a

123
00:07:27,410 --> 00:07:28,880
 second example a

124
00:07:28,880 --> 00:07:36,190
 person who is who desires to live would avoid poison so

125
00:07:36,190 --> 00:07:37,480
 that's the backstory

126
00:07:37,480 --> 00:07:42,620
 small back short backstory and that's the verse so how does

127
00:07:42,620 --> 00:07:43,640
 this what does this

128
00:07:43,640 --> 00:07:47,560
 mean how does it relate to our practice

129
00:07:48,160 --> 00:07:52,640
 but the word by is interesting because we talk about the

130
00:07:52,640 --> 00:07:54,800
 dangers and we're

131
00:07:54,800 --> 00:07:57,760
 always talking about how bad evil is right it's doing evil

132
00:07:57,760 --> 00:07:58,520
 is a bad thing

133
00:07:58,520 --> 00:08:05,180
 doing good is a good thing but if you it may not be clear

134
00:08:05,180 --> 00:08:06,800
 exactly what is wrong

135
00:08:06,800 --> 00:08:10,830
 with doing bad things what is wrong with being an evil

136
00:08:10,830 --> 00:08:12,720
 person what is wrong with

137
00:08:12,720 --> 00:08:17,410
 taking advantage of people and maybe even more difficult

138
00:08:17,410 --> 00:08:18,920
 what is wrong with

139
00:08:18,920 --> 00:08:24,650
 indulging in pleasure sensual pleasure in general what's

140
00:08:24,650 --> 00:08:26,360
 wrong with it why do

141
00:08:26,360 --> 00:08:30,480
 we consider that to be quote-unquote evil

142
00:08:30,480 --> 00:08:36,640
 that's because of the dangers there are four dangers in the

143
00:08:36,640 --> 00:08:38,680
 performance of evil

144
00:08:38,680 --> 00:08:41,540
 deeds or if you like if you don't like the word evil though

145
00:08:41,540 --> 00:08:42,200
 it is the word they

146
00:08:42,200 --> 00:08:45,340
 use you could understand by the definition of evil in

147
00:08:45,340 --> 00:08:46,280
 Buddhism it just

148
00:08:46,280 --> 00:08:51,210
 means unwholesomeness or that that which leads to suffering

149
00:08:51,210 --> 00:08:53,680
 that which has danger

150
00:08:53,680 --> 00:08:59,250
 that which is not worth doing so there are four dangers

151
00:08:59,250 --> 00:09:00,480
 that we talk about

152
00:09:00,480 --> 00:09:03,250
 there's actually 12 dangers but four of them are

153
00:09:03,250 --> 00:09:05,040
 specifically related to people

154
00:09:05,040 --> 00:09:08,200
 doing evil deeds there's four dangers that all beings have

155
00:09:08,200 --> 00:09:09,440
 to face these are

156
00:09:09,440 --> 00:09:16,330
 birth old age sickness and death birth means if you you

157
00:09:16,330 --> 00:09:18,200
 have to if you die you

158
00:09:18,200 --> 00:09:23,800
 have to be born again and there's four dangers that people

159
00:09:23,800 --> 00:09:24,680
 performing good

160
00:09:24,680 --> 00:09:27,460
 deeds have to deal with so people who have engaged in

161
00:09:27,460 --> 00:09:28,600
 spiritual practice

162
00:09:28,600 --> 00:09:37,130
 these are the evils of anger the evils of sense sensual

163
00:09:37,130 --> 00:09:40,160
 pleasure the evil of

164
00:09:40,160 --> 00:09:46,420
 sexual desire and the evil of laziness I believe but the

165
00:09:46,420 --> 00:09:47,240
 four that we're concerned

166
00:09:47,240 --> 00:09:52,400
 with here are the evil the dangers that are that present

167
00:09:52,400 --> 00:09:54,000
 themselves to people

168
00:09:54,000 --> 00:09:59,720
 who perform evil what are the problems the problem with it

169
00:09:59,720 --> 00:10:00,440
 the big one that we

170
00:10:00,440 --> 00:10:04,290
 always point to is when you pass away if you kill someone

171
00:10:04,290 --> 00:10:05,680
 in this life people

172
00:10:05,680 --> 00:10:09,150
 understand in Buddhism that it means someone's going to

173
00:10:09,150 --> 00:10:10,320
 come back and kill

174
00:10:10,320 --> 00:10:13,760
 you in the next life it's not always like that but we

175
00:10:13,760 --> 00:10:14,720
 understand there's some

176
00:10:14,720 --> 00:10:18,780
 future births consequences and there's some ultimate

177
00:10:18,780 --> 00:10:20,920
 consequences without that

178
00:10:20,920 --> 00:10:24,960
 it's true that there's a potential problem with Buddhist

179
00:10:24,960 --> 00:10:26,320
 theory because you

180
00:10:26,320 --> 00:10:31,400
 could outrun the consequences if you do all sorts of unwh

181
00:10:31,400 --> 00:10:33,040
olesome deeds or

182
00:10:33,040 --> 00:10:36,750
 cultivate all sorts of addictions to sensuality and

183
00:10:36,750 --> 00:10:38,960
 pleasure then just when

184
00:10:38,960 --> 00:10:43,420
 you turn like 65 or as you start taking to age you can just

185
00:10:43,420 --> 00:10:44,360
 say enough of that

186
00:10:44,360 --> 00:10:49,980
 and kill yourself or whatever you you you can live out your

187
00:10:49,980 --> 00:10:52,280
 life and not have to

188
00:10:52,280 --> 00:10:57,960
 face the consequences if you if there were no future

189
00:10:57,960 --> 00:11:00,120
 existence if desire didn't

190
00:11:00,120 --> 00:11:07,290
 have the potential to relink to create more if desire didn

191
00:11:07,290 --> 00:11:08,280
't have the potential

192
00:11:08,280 --> 00:11:14,050
 to create really but specifically the potential to create

193
00:11:14,050 --> 00:11:15,680
 more life then you'd

194
00:11:15,680 --> 00:11:18,250
 be able to get off scot-free there wouldn't be too much

195
00:11:18,250 --> 00:11:20,040
 danger but even

196
00:11:20,040 --> 00:11:24,080
 still before we look at that there are three other types of

197
00:11:24,080 --> 00:11:25,440
 dangers that only

198
00:11:25,440 --> 00:11:31,800
 relate to this life the first is self-sensure

199
00:11:31,800 --> 00:11:42,890
 ata no vada the blaming yourself so the Buddha said just

200
00:11:42,890 --> 00:11:44,000
 like a blanket our

201
00:11:44,000 --> 00:11:50,810
 deeds cover us when we're alone if we do good deeds there's

202
00:11:50,810 --> 00:11:52,280
 a warm comfortable

203
00:11:52,280 --> 00:11:57,800
 blanket if we do bad deeds it's a heavy uncomfortable

204
00:11:58,040 --> 00:12:04,500
 shroud covering us and this is certainly true something

205
00:12:04,500 --> 00:12:05,680
 that you can see for

206
00:12:05,680 --> 00:12:08,270
 yourself based on the needs that you do it's something that

207
00:12:08,270 --> 00:12:09,120
 we see very clearly

208
00:12:09,120 --> 00:12:12,080
 in meditation that the good deeds that we do and the bad

209
00:12:12,080 --> 00:12:13,140
 deeds that we do do

210
00:12:13,140 --> 00:12:19,580
 cover us and go along with us they follow us the bad deeds

211
00:12:19,580 --> 00:12:20,560
 follow us like a

212
00:12:20,560 --> 00:12:26,050
 ball and chain good deeds lift us up light in our load

213
00:12:26,050 --> 00:12:28,560
 light in our step they

214
00:12:28,560 --> 00:12:32,590
 change us there are change in direction every ethical act

215
00:12:32,590 --> 00:12:33,960
 that we perform

216
00:12:33,960 --> 00:12:38,050
 changes the direction that we're going it affects our life

217
00:12:38,050 --> 00:12:39,440
 it sends out ripples

218
00:12:39,440 --> 00:12:42,990
 into the universe changes how people look at us well first

219
00:12:42,990 --> 00:12:43,840
 of all it changed

220
00:12:43,840 --> 00:12:46,420
 how we look at ourselves the second one is how how other

221
00:12:46,420 --> 00:12:47,840
 people look at us so

222
00:12:47,840 --> 00:12:55,120
 the other thing it changes evil deeds are a pass so one day

223
00:12:55,120 --> 00:12:56,480
 they lead other

224
00:12:56,480 --> 00:13:00,730
 people to scold us and blame us and to censure us to look

225
00:13:00,730 --> 00:13:02,840
 down upon us it leads

226
00:13:02,840 --> 00:13:08,600
 to tension and friction even just sensuality and greed when

227
00:13:08,600 --> 00:13:09,040
 we're greedy

228
00:13:09,040 --> 00:13:12,660
 it means we need a portion of something we need something

229
00:13:12,660 --> 00:13:14,360
 for ourselves if

230
00:13:14,360 --> 00:13:17,330
 someone else wants it then we have a conflict and we have

231
00:13:17,330 --> 00:13:18,240
 to resolve it either

232
00:13:18,240 --> 00:13:22,840
 by fighting over it or by suppressing our desires and

233
00:13:22,840 --> 00:13:24,560
 relinquishing it but

234
00:13:24,560 --> 00:13:27,930
 either way it leads to suffering even to give other people

235
00:13:27,930 --> 00:13:29,120
 something that you

236
00:13:29,120 --> 00:13:32,890
 want is a cause for suffering it torments you because you

237
00:13:32,890 --> 00:13:33,560
 want it and you

238
00:13:33,560 --> 00:13:41,680
 can't have it and the third one is punishment that there is

239
00:13:41,680 --> 00:13:42,760
 punishment that

240
00:13:42,760 --> 00:13:47,720
 comes so that when we break laws or when we killed when we

241
00:13:47,720 --> 00:13:49,280
 when we break ethical

242
00:13:49,280 --> 00:13:54,790
 rules and maxims not only do is there censure but there's

243
00:13:54,790 --> 00:13:57,280
 also punishment so

244
00:13:57,280 --> 00:14:01,130
 we might lose friendships or it might lead to fighting it

245
00:14:01,130 --> 00:14:02,480
 might lead even to

246
00:14:02,480 --> 00:14:09,000
 criminal punishment there are many dangers to doing evil

247
00:14:09,000 --> 00:14:09,840
 deeds that you can

248
00:14:09,840 --> 00:14:15,150
 see in the here and now now about the fourth one the fourth

249
00:14:15,150 --> 00:14:16,680
 danger which is in

250
00:14:16,680 --> 00:14:22,440
 the future life it really bears examining and it helps us

251
00:14:22,440 --> 00:14:23,840
 it gives us a

252
00:14:23,840 --> 00:14:28,320
 chance to talk about how this all relates to our meditation

253
00:14:28,320 --> 00:14:29,120
 practice

254
00:14:29,120 --> 00:14:34,010
 because the big problem with unwholesome deeds is that they

255
00:14:34,010 --> 00:14:36,640
're productive and

256
00:14:36,640 --> 00:14:40,320
 that's how we look at our life life isn't a thing it's not

257
00:14:40,320 --> 00:14:41,720
 atomic like hey

258
00:14:41,720 --> 00:14:45,540
 I'm a being and I'm alive life is caused by it's it's

259
00:14:45,540 --> 00:14:48,040
 continued it's a it's a

260
00:14:48,040 --> 00:14:55,090
 conservation as they say not only are we do we are we alive

261
00:14:55,090 --> 00:14:56,200
 but we're we're

262
00:14:56,200 --> 00:15:01,400
 being born every moment right because there really is no no

263
00:15:01,400 --> 00:15:02,920
 logically inherent

264
00:15:02,920 --> 00:15:08,950
 reason to think a priori that that just because we are

265
00:15:08,950 --> 00:15:10,880
 alive now we're going to

266
00:15:10,880 --> 00:15:13,200
 be alive in the next moment so there's something that's

267
00:15:13,200 --> 00:15:14,080
 keeping us alive and

268
00:15:14,080 --> 00:15:16,750
 not only alive but there's something that there are many

269
00:15:16,750 --> 00:15:17,560
 things that are

270
00:15:17,560 --> 00:15:21,330
 determining which direction we take in our lives is some of

271
00:15:21,330 --> 00:15:22,240
 the choice some are

272
00:15:22,240 --> 00:15:26,200
 choices that we make some are that which is imposed upon us

273
00:15:26,200 --> 00:15:28,360
 and one of the things

274
00:15:28,360 --> 00:15:33,360
 that influences that is our desire now without desire you

275
00:15:33,360 --> 00:15:34,120
 would still be alive

276
00:15:34,120 --> 00:15:38,290
 without greed anger and delusion one continues to live on

277
00:15:38,290 --> 00:15:40,720
 but many of the

278
00:15:40,720 --> 00:15:45,570
 aspects of one light one's life that would be created with

279
00:15:45,570 --> 00:15:46,800
 greed and based on

280
00:15:46,800 --> 00:15:49,480
 greed anger and illusion will not be created so an

281
00:15:49,480 --> 00:15:51,480
 enlightened being lives by

282
00:15:51,480 --> 00:15:55,770
 necessity a very simple life they aren't able to go out and

283
00:15:55,770 --> 00:15:57,360
 make money and work

284
00:15:57,360 --> 00:16:03,760
 and so on because that part of life that the impetus that

285
00:16:03,760 --> 00:16:06,880
 creates that part of

286
00:16:06,880 --> 00:16:12,710
 life isn't there and so that's how we look at in general

287
00:16:12,710 --> 00:16:14,160
 and unwholesome and

288
00:16:14,160 --> 00:16:20,910
 wholesome deeds when you when you want something you create

289
00:16:20,910 --> 00:16:24,000
 a whole new aspect

290
00:16:24,000 --> 00:16:27,170
 of your life involved with getting that and it can often be

291
00:16:27,170 --> 00:16:28,560
 quite complex the

292
00:16:28,560 --> 00:16:32,710
 drive to attain and all the work that you have to do and

293
00:16:32,710 --> 00:16:34,120
 the planning and

294
00:16:34,120 --> 00:16:37,830
 manipulation of others even and all of the many things you

295
00:16:37,830 --> 00:16:39,360
 have to do you end

296
00:16:39,360 --> 00:16:43,470
 up committing yourself like when you want a big house then

297
00:16:43,470 --> 00:16:44,320
 you commit

298
00:16:44,320 --> 00:16:49,420
 yourself to alone if you want a car when you fall in love

299
00:16:49,420 --> 00:16:51,640
 with another person you

300
00:16:51,640 --> 00:16:56,260
 get married and you start a whole new there's a whole new

301
00:16:56,260 --> 00:16:57,840
 thing that is born

302
00:16:57,840 --> 00:17:04,920
 and so the idea of rebirth is really really that's that's I

303
00:17:04,920 --> 00:17:06,360
 don't think it's

304
00:17:06,360 --> 00:17:09,950
 it would be acceptable evidence but it is the evidence if

305
00:17:09,950 --> 00:17:12,000
 you understand reality

306
00:17:12,000 --> 00:17:18,700
 it's the evidence for rebirth is this productive potential

307
00:17:18,700 --> 00:17:21,040
 for karma when you

308
00:17:21,040 --> 00:17:26,040
 when you have desire it creates it manifests something in

309
00:17:26,040 --> 00:17:28,120
 your life and

310
00:17:28,120 --> 00:17:32,980
 that's all that's left on the other side the brain and the

311
00:17:32,980 --> 00:17:35,480
 body and the an

312
00:17:35,480 --> 00:17:40,400
 external stimulus altogether is also capable of creating

313
00:17:40,400 --> 00:17:43,240
 life or thought or

314
00:17:43,240 --> 00:17:47,760
 experience you can when you hear something the sound

315
00:17:47,760 --> 00:17:49,760
 triggers the

316
00:17:49,760 --> 00:17:54,260
 experience of hearing in the mind the consciousness to

317
00:17:54,260 --> 00:17:57,320
 arise in at the ear

318
00:17:57,320 --> 00:18:04,840
 now when you die all of that becomes unproductive unable to

319
00:18:04,840 --> 00:18:06,960
 produce experience

320
00:18:06,960 --> 00:18:11,860
 but the desire is still a cape still perfectly capable of

321
00:18:11,860 --> 00:18:13,080
 creating new

322
00:18:13,080 --> 00:18:19,040
 experience it's all that's left and so that is what the

323
00:18:19,040 --> 00:18:21,280
 fact is that that is

324
00:18:21,280 --> 00:18:24,960
 what creates the next life I mean there's no traveling to

325
00:18:24,960 --> 00:18:26,000
 be born in a womb

326
00:18:26,000 --> 00:18:32,220
 or something there's a restarting with just a single two

327
00:18:32,220 --> 00:18:34,680
 cells starting to

328
00:18:34,680 --> 00:18:37,410
 create something so the mind and the body working again

329
00:18:37,410 --> 00:18:38,660
 that's for humans now

330
00:18:38,660 --> 00:18:43,120
 for angels for other types of beings in hell and so on it's

331
00:18:43,120 --> 00:18:44,600
 spontaneous but it's

332
00:18:44,600 --> 00:18:51,590
 spontaneous based on our desires our attachments person

333
00:18:51,590 --> 00:18:53,360
 without desires when

334
00:18:53,360 --> 00:18:57,200
 they pass away all the productive capability of the body

335
00:18:57,200 --> 00:18:58,160
 ceases the

336
00:18:58,160 --> 00:19:01,750
 productive capability of the mind has already ceased in

337
00:19:01,750 --> 00:19:03,920
 terms of karma and so

338
00:19:03,920 --> 00:19:12,250
 they pass away and there is no rebirth so that's a little

339
00:19:12,250 --> 00:19:14,800
 bit of off track but

340
00:19:14,800 --> 00:19:18,670
 that's the danger the dangers that will have to be reborn

341
00:19:18,670 --> 00:19:19,960
 again now for many

342
00:19:19,960 --> 00:19:22,560
 people that sounds quite exciting they think wow great I

343
00:19:22,560 --> 00:19:23,520
 can be reborn again

344
00:19:23,520 --> 00:19:28,720
 and when we talk about heaven and so on that's great as

345
00:19:28,720 --> 00:19:30,880
 well it's that's that's

346
00:19:30,880 --> 00:19:33,930
 a good reason to be reborn because hey we can be reborn in

347
00:19:33,930 --> 00:19:35,840
 heaven but it ignores

348
00:19:35,840 --> 00:19:41,040
 the danger or we must never forget about the danger of evil

349
00:19:41,040 --> 00:19:43,280
 and so that is what

350
00:19:43,280 --> 00:19:50,160
 this verse reminds us of that there are consequences just

351
00:19:50,160 --> 00:19:51,280
 as we would avoid

352
00:19:51,280 --> 00:19:55,290
 other things that cause that have danger and have great

353
00:19:55,290 --> 00:19:56,720
 potential to cause harm

354
00:19:56,720 --> 00:20:03,920
 like poison or a dangerous road for a rich merchant we

355
00:20:03,920 --> 00:20:05,080
 should guard ourselves

356
00:20:05,080 --> 00:20:13,960
 and we should avoid the path of evil how we do this is how

357
00:20:13,960 --> 00:20:15,320
 it relates to our

358
00:20:15,320 --> 00:20:18,540
 meditation it's because the way that we do this is through

359
00:20:18,540 --> 00:20:19,520
 the practice I mean

360
00:20:19,520 --> 00:20:22,810
 this is very much what Buddhist practice is based on

361
00:20:22,810 --> 00:20:25,240
 avoiding evil Buddhism isn't

362
00:20:25,240 --> 00:20:30,230
 a philosophy where you're just trying to explore the

363
00:20:30,230 --> 00:20:32,360
 universe or become some

364
00:20:32,360 --> 00:20:38,960
 spiritual being with all sorts of high-minded thoughts it's

365
00:20:38,960 --> 00:20:40,320
 specifically

366
00:20:40,320 --> 00:20:45,910
 to be free from evil be free from suffering and to

367
00:20:45,910 --> 00:20:47,940
 cultivate good so this

368
00:20:47,940 --> 00:20:52,230
 is very this is completely the this is the basis of

369
00:20:52,230 --> 00:20:54,600
 Buddhist practice we give

370
00:20:54,600 --> 00:20:59,240
 charity to help us overcome greed we practice morality it's

371
00:20:59,240 --> 00:20:59,720
 to help us

372
00:20:59,720 --> 00:21:07,410
 overcome anger mostly also greed and we practice meditation

373
00:21:07,410 --> 00:21:08,400
 insight meditation

374
00:21:08,400 --> 00:21:15,390
 to overcome delusion the clarity of mind it frees us from

375
00:21:15,390 --> 00:21:18,040
 our ego and conceit and

376
00:21:18,040 --> 00:21:24,120
 arrogance and wrong views and beliefs of all sorts all of

377
00:21:24,120 --> 00:21:26,960
 this is the practice of

378
00:21:26,960 --> 00:21:31,940
 the Buddhist teaching it's how we avoid evil when when so

379
00:21:31,940 --> 00:21:34,200
 when you give gifts

380
00:21:34,200 --> 00:21:40,360
 you're training yourself in generosity and moreover you're

381
00:21:40,360 --> 00:21:41,680
 seeing the benefits

382
00:21:41,680 --> 00:21:45,540
 of generosity you're able to see your attachments and how

383
00:21:45,540 --> 00:21:46,000
 much suffering

384
00:21:46,000 --> 00:21:49,400
 comes from it when you give something often you don't want

385
00:21:49,400 --> 00:21:50,120
 to and you cringe

386
00:21:50,120 --> 00:21:53,070
 and think oh I want that for myself or you feel like you

387
00:21:53,070 --> 00:21:54,960
 want but you overcome

388
00:21:54,960 --> 00:21:59,560
 that by overcoming it you see what the real problem is the

389
00:21:59,560 --> 00:22:00,240
 problem is not

390
00:22:00,240 --> 00:22:03,530
 losing what you want the problem is this one thing that is

391
00:22:03,530 --> 00:22:04,400
 really an evil

392
00:22:04,400 --> 00:22:11,160
 unpleasant unwholesome state when you practice morality you

393
00:22:11,160 --> 00:22:12,000
 start to see how

394
00:22:12,000 --> 00:22:15,480
 frustrated we get when when something doesn't go our way

395
00:22:15,480 --> 00:22:16,360
 and now we want to

396
00:22:16,360 --> 00:22:23,350
 lash out or hurt others and so it helps us cultivate

397
00:22:23,350 --> 00:22:25,720
 patience and

398
00:22:25,720 --> 00:22:28,630
 helps us overcome anger we get to see the problem with

399
00:22:28,630 --> 00:22:29,960
 anger because we don't

400
00:22:29,960 --> 00:22:35,720
 follow it and we get to change and become more patient

401
00:22:35,720 --> 00:22:38,200
 rather than attacking

402
00:22:38,200 --> 00:22:42,550
 or hurting others or hurting ourselves and when we practice

403
00:22:42,550 --> 00:22:43,940
 meditation we see

404
00:22:43,940 --> 00:22:47,500
 all of this in much more clarity we see moment by moment

405
00:22:47,500 --> 00:22:49,640
 how things arise we

406
00:22:49,640 --> 00:22:53,450
 start to see patterns we see how our behavior is hurting

407
00:22:53,450 --> 00:22:54,480
 ourselves very

408
00:22:54,480 --> 00:22:58,240
 clearly this meditation is actually quite a simple thing

409
00:22:58,240 --> 00:22:59,640
 but it can be the

410
00:22:59,640 --> 00:23:04,850
 most traumatizing unpleasant experience because our minds

411
00:23:04,850 --> 00:23:07,600
 are quite messed up

412
00:23:07,600 --> 00:23:12,560
 for the moment if we've never trained them our minds become

413
00:23:12,560 --> 00:23:13,760
 lazy and

414
00:23:13,760 --> 00:23:22,920
 chaotic messy cluttered and so through the meditation

415
00:23:22,920 --> 00:23:23,880
 practice we sort these

416
00:23:23,880 --> 00:23:26,820
 out reminding ourselves this is this this is this this is

417
00:23:26,820 --> 00:23:27,880
 this and it's about

418
00:23:27,880 --> 00:23:32,940
 sorting things into what they really are and seeing the

419
00:23:32,940 --> 00:23:35,000
 chaos of the mind and the

420
00:23:35,000 --> 00:23:40,560
 automatic adjustment that comes from seeing the danger and

421
00:23:40,560 --> 00:23:42,160
 the problems and

422
00:23:42,160 --> 00:23:48,710
 things so this is our teaching tonight from the Dhammapada

423
00:23:48,710 --> 00:23:51,400
 we think we'll stop

424
00:23:51,400 --> 00:23:56,520
 it there basic teaching avoid evil avoid it like the plague

425
00:23:56,520 --> 00:23:57,960
 avoid it like poison

426
00:23:57,960 --> 00:24:04,720
 avoid it like an evil and it's an easy just one more little

427
00:24:04,720 --> 00:24:06,800
 thing the how it

428
00:24:06,800 --> 00:24:09,680
 does relate to the story of the merchant it's about staying

429
00:24:09,680 --> 00:24:10,640
 still about being

430
00:24:10,640 --> 00:24:13,660
 centered this village it's kind of a nice analogy of

431
00:24:13,660 --> 00:24:15,480
 staying in the village where

432
00:24:15,480 --> 00:24:19,340
 peace and happiness rain instead of being greedy and going

433
00:24:19,340 --> 00:24:20,440
 off and looking

434
00:24:20,440 --> 00:24:25,130
 for more so that's about living our lives existentially in

435
00:24:25,130 --> 00:24:26,720
 a sense instead

436
00:24:26,720 --> 00:24:35,180
 of seeking out pleasure or or chasing it or or following

437
00:24:35,180 --> 00:24:36,640
 after our version no

438
00:24:36,640 --> 00:24:42,260
 following after desire or a version stay stay present don't

439
00:24:42,260 --> 00:24:43,400
 follow an evil path

440
00:24:43,400 --> 00:24:49,170
 don't go looking for trouble so that's all the Dhammapada

441
00:24:49,170 --> 00:24:50,760
 for tonight thank you

