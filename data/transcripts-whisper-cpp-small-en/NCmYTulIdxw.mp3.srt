1
00:00:00,000 --> 00:00:05,560
 Hello and welcome back to our study of the Dhammapada.

2
00:00:05,560 --> 00:00:09,600
 Today we continue on with first number ninety, which reads

3
00:00:09,600 --> 00:00:12,400
 as follows.

4
00:00:12,400 --> 00:00:27,450
 katadino vuiso kasa vipumutasa sabadi sabagantha pahinasa

5
00:00:27,450 --> 00:00:31,840
 parilaho nujati

6
00:00:31,840 --> 00:00:37,970
 katadino, for one who has completed their journey,

7
00:00:37,970 --> 00:00:49,350
 completed the going, viso kasa, one who is free from sorrow

8
00:00:49,350 --> 00:00:49,360
,

9
00:00:49,360 --> 00:00:58,960
 vipumutasa sabadi, one who is liberated everywhere, who is

10
00:00:58,960 --> 00:01:04,560
 everywhere free, everywhere they go.

11
00:01:04,560 --> 00:01:10,920
 sabagantha pahinasa, one who has abandoned all fetters, all

12
00:01:10,920 --> 00:01:16,480
 forms of clinging, every object of attachment,

13
00:01:16,480 --> 00:01:25,610
 parilaho nujati, no fever can be found, no distress,

14
00:01:25,610 --> 00:01:37,840
 distress is probably the best translation, parilaha.

15
00:01:37,840 --> 00:01:45,180
 katadino vuiso kasa, very beautiful little verse, it talks

16
00:01:45,180 --> 00:01:50,880
 about fever, parilaha is often translated as fever,

17
00:01:50,880 --> 00:01:56,280
 that's probably etymologically fairly correct, burning

18
00:01:56,280 --> 00:02:01,280
 right, but in this specific instance,

19
00:02:01,280 --> 00:02:05,870
 because of the story and because of the context of the

20
00:02:05,870 --> 00:02:09,600
 verse itself, it's not a fever, it's a mental

21
00:02:09,600 --> 00:02:15,960
 fever, a mental burning, so we would translate it as

22
00:02:15,960 --> 00:02:18,720
 distress or fever of passion,

23
00:02:18,720 --> 00:02:23,810
 but literally it means burning I think, daha, but in daha

24
00:02:23,810 --> 00:02:28,320
 becomes parilaha.

25
00:02:28,320 --> 00:02:31,970
 So the story, this was told in regards to a question posed

26
00:02:31,970 --> 00:02:36,000
 by Dibhaka, who was a doctor,

27
00:02:36,000 --> 00:02:39,630
 the doctor who looked after the Buddha, and his story is

28
00:02:39,630 --> 00:02:41,920
 not told here, I can't remember if it's

29
00:02:41,920 --> 00:02:47,660
 actually told in the Dhammapada, I don't think so, maybe in

30
00:02:47,660 --> 00:02:51,440
 the Jataka it occurs, but in the Vinaya

31
00:02:51,440 --> 00:02:58,040
 definitely you find his story, he was a master doctor, an

32
00:02:58,040 --> 00:03:04,000
 exceptional medicine man, they say that

33
00:03:04,000 --> 00:03:14,800
 the final task given to him by his teacher was to stake out

34
00:03:14,800 --> 00:03:21,120
 a 16 kilometer by 16 kilometer square,

35
00:03:22,240 --> 00:03:24,800
 piece of land which is a league, a square league,

36
00:03:24,800 --> 00:03:34,030
 yojana of land, and bring to his teacher every plant in the

37
00:03:34,030 --> 00:03:37,520
 entire square league

38
00:03:37,520 --> 00:03:41,520
 that had no medicinal properties.

39
00:03:46,800 --> 00:03:49,710
 And this was the task, so he set out to accomplish this

40
00:03:49,710 --> 00:03:52,640
 task, came back some time later and said to

41
00:03:52,640 --> 00:03:56,260
 the teacher, there are no such plants, there are no plants

42
00:03:56,260 --> 00:03:58,480
 in that 16, I couldn't find any plants

43
00:03:58,480 --> 00:04:01,840
 that were without medicinal properties, and that's how his

44
00:04:01,840 --> 00:04:03,440
 teacher knew that he passed,

45
00:04:03,440 --> 00:04:07,090
 because he was able to identify medicinal properties in all

46
00:04:07,090 --> 00:04:07,600
 plants,

47
00:04:07,600 --> 00:04:13,360
 it's the story, so he was exceptional, he got,

48
00:04:15,840 --> 00:04:18,920
 he caused a little bit of trouble, or he got into a little

49
00:04:18,920 --> 00:04:20,320
 bit of trouble, not of his own,

50
00:04:20,320 --> 00:04:24,270
 not on his own, he was actually an awesome, wonderful

51
00:04:24,270 --> 00:04:28,640
 person, he donated a mango grove

52
00:04:28,640 --> 00:04:32,070
 to the Buddha, and you can still go and see the place that

53
00:04:32,070 --> 00:04:33,200
's supposed to be this,

54
00:04:33,200 --> 00:04:36,140
 where this mango grove was, right at the bottom of Vulture

55
00:04:36,140 --> 00:04:36,960
's Peak, because

56
00:04:36,960 --> 00:04:40,960
 Vulture's Peak was difficult to go up constantly to take

57
00:04:40,960 --> 00:04:43,520
 care of the Buddha, so in order to

58
00:04:43,520 --> 00:04:45,940
 make it easier for him to take care of the Buddha when the

59
00:04:45,940 --> 00:04:46,720
 Buddha was sick,

60
00:04:46,720 --> 00:04:50,340
 he would, and also to make it easier for the Buddha to go

61
00:04:50,340 --> 00:04:52,240
 on alms around when the Buddha was

62
00:04:52,240 --> 00:04:55,600
 ill, he gave him this mango grove at the bottom of the hill

63
00:04:55,600 --> 00:04:57,600
, the bottom of the mountain.

64
00:04:57,600 --> 00:05:05,400
 But as he was looking after the Buddha, he also took on the

65
00:05:05,400 --> 00:05:07,280
 role of looking after the monks,

66
00:05:08,480 --> 00:05:14,410
 and as a result of taking, paying so much attention, taking

67
00:05:14,410 --> 00:05:16,640
 so much time to look after the Sangha,

68
00:05:16,640 --> 00:05:20,560
 his other patients started to miss him, and so people who

69
00:05:20,560 --> 00:05:23,200
 were sick would actually ordain as

70
00:05:23,200 --> 00:05:34,060
 monks, just to try and get this level of care, and this was

71
00:05:34,060 --> 00:05:35,760
 actually the reason why the Buddha

72
00:05:35,760 --> 00:05:38,180
 stated the rule that someone who is sick, someone who has

73
00:05:38,180 --> 00:05:40,640
 this sickness, has any

74
00:05:40,640 --> 00:05:46,740
 terrible sickness, shouldn't be allowed to ordain. I mean

75
00:05:46,740 --> 00:05:48,160
 there are other reasons for it,

76
00:05:48,160 --> 00:05:51,760
 obviously, it's problematic, but it's one of those things

77
00:05:51,760 --> 00:05:53,920
 that leads people to ordain for the wrong

78
00:05:53,920 --> 00:05:57,390
 reasons, and this happens in Buddhist countries where they

79
00:05:57,390 --> 00:06:00,880
 become lax about this, and really

80
00:06:00,880 --> 00:06:03,820
 these people become a burden on the monastic Sangha rather

81
00:06:03,820 --> 00:06:07,120
 than actually promoting Buddhism

82
00:06:07,120 --> 00:06:11,510
 and doing good things. So there's a few stories about him,

83
00:06:11,510 --> 00:06:16,320
 but this story actually concerns

84
00:06:16,320 --> 00:06:22,600
 Devadatta in tangentially, because the story goes that Dev

85
00:06:22,600 --> 00:06:25,520
adatta was trying all sorts of ways to

86
00:06:25,520 --> 00:06:28,870
 kill the Buddha. Devadatta was the Buddha's cousin, and he

87
00:06:28,870 --> 00:06:31,840
 got upset and jealous of the Buddha, and he

88
00:06:31,840 --> 00:06:35,520
 wanted to be the leader of the Sangha, just kind of an all-

89
00:06:35,520 --> 00:06:38,240
around mean and nasty sort of fellow.

90
00:06:38,240 --> 00:06:49,120
 So he sent an elephant after the Buddha, and the Buddha t

91
00:06:49,120 --> 00:06:51,520
amed the elephant, it was his mad

92
00:06:51,520 --> 00:06:54,310
 elephant that was supposed to trample the Buddha, and the

93
00:06:54,310 --> 00:06:56,400
 Buddha tamed him with loving kindness.

94
00:06:56,400 --> 00:07:00,890
 He sent some archers after the Buddha, some mercenaries

95
00:07:00,890 --> 00:07:02,320
 after the Buddha, and they all

96
00:07:02,320 --> 00:07:07,300
 converted to become monks. And finally he just got fed up,

97
00:07:07,300 --> 00:07:09,760
 and as the Buddha was coming down

98
00:07:09,760 --> 00:07:13,540
 from Vulture's Peak, he dropped a rock, a big boulder on

99
00:07:13,540 --> 00:07:15,600
 the Buddha trying to crush him

100
00:07:16,560 --> 00:07:22,160
 from up on high. And as the rock was falling down, it hit

101
00:07:22,160 --> 00:07:25,280
 some sort of outcropping, of course,

102
00:07:25,280 --> 00:07:27,500
 because you can't kill the Buddha, it just doesn't happen.

103
00:07:27,500 --> 00:07:30,160
 This karma is too good. But a little

104
00:07:30,160 --> 00:07:34,440
 splinter, a small splinter, broke off. The rest of the rock

105
00:07:34,440 --> 00:07:37,120
 veered out of the way and didn't come

106
00:07:37,120 --> 00:07:39,280
 close to the Buddha, but a small splinter came and hit the

107
00:07:39,280 --> 00:07:42,880
 Buddha's foot. And Jivaka looked after the

108
00:07:42,880 --> 00:07:48,320
 Buddha, the Buddha lay down and Jivaka bandaged him up or

109
00:07:48,320 --> 00:07:54,240
 put some poultice on the wound. I think

110
00:07:54,240 --> 00:07:57,240
 the commentaries tried to say that he didn't actually bleed

111
00:07:57,240 --> 00:07:58,800
 because it's impossible to make

112
00:07:58,800 --> 00:08:02,160
 the Buddha bleed, but it's about the translation as to what

113
00:08:02,160 --> 00:08:04,400
 exactly is meant to happen anyway.

114
00:08:04,400 --> 00:08:10,850
 Whatever, the Buddha was injured and he put some herbs on

115
00:08:10,850 --> 00:08:12,560
 it, something strong that would

116
00:08:13,200 --> 00:08:15,190
 take the pain away and make the swelling go down, but it

117
00:08:15,190 --> 00:08:16,320
 was quite strong.

118
00:08:16,320 --> 00:08:21,070
 And then Jivaka had to go into the city to look after his

119
00:08:21,070 --> 00:08:23,280
 patients in the city.

120
00:08:23,280 --> 00:08:28,320
 And he was late, he was looking after some rich person or

121
00:08:28,320 --> 00:08:31,680
 maybe even the king, and he was late

122
00:08:31,680 --> 00:08:34,780
 coming back and they closed the gates to the city before he

123
00:08:34,780 --> 00:08:36,960
 could get out. And so he had to stay all

124
00:08:36,960 --> 00:08:40,850
 night in the city and he was worried all night and

125
00:08:40,850 --> 00:08:44,560
 concerned because he had to go back and take this

126
00:08:44,560 --> 00:08:48,140
 poultice off of the Buddha's ankle or else it would cause

127
00:08:48,140 --> 00:08:50,800
 harm. It would actually cause harm to the

128
00:08:50,800 --> 00:08:56,520
 Buddha. And so he was unable to let the Buddha know.

129
00:08:56,520 --> 00:09:01,440
 Fortunately, that's what, that's the benefit

130
00:09:01,440 --> 00:09:05,400
 of having all sorts of supernatural powers. The Buddha was

131
00:09:05,400 --> 00:09:07,920
 able to discern that it was time to

132
00:09:07,920 --> 00:09:15,280
 take the poultice off and had Ananda help him take it off

133
00:09:15,280 --> 00:09:18,880
 and waited there for Jivaka to come.

134
00:09:18,880 --> 00:09:23,250
 So no harm done. But harm that was done to Jivaka who was

135
00:09:23,250 --> 00:09:25,440
 worrying and fretting, who was distressed.

136
00:09:25,440 --> 00:09:29,520
 Now Jivaka was actually a Sottapan. I think at this time he

137
00:09:29,520 --> 00:09:31,040
 was already a Sottapan,

138
00:09:31,040 --> 00:09:34,000
 although I'm not sure of the actual timeline to have to

139
00:09:34,000 --> 00:09:36,160
 look deeper into that. But I think

140
00:09:36,160 --> 00:09:40,020
 he was already a Sottapan at this point. So he was already

141
00:09:40,020 --> 00:09:42,960
 in an Ariya Bhugala, but he was still

142
00:09:42,960 --> 00:09:49,120
 subject to distress. And so he fretted about this for the

143
00:09:49,120 --> 00:09:53,120
 long time that he was kept in the city.

144
00:09:53,120 --> 00:09:56,560
 And finally when the gates opened in the morning, he ran

145
00:09:56,560 --> 00:10:00,800
 over, ran, sprinted his way over to Vulture's

146
00:10:00,800 --> 00:10:06,020
 Peak and came to the Buddha and asked him, I'm sorry Vener

147
00:10:06,020 --> 00:10:09,040
able Sir, that I wasn't able to come,

148
00:10:09,040 --> 00:10:13,230
 were you distressed by the pain? Were you distressed by

149
00:10:13,230 --> 00:10:14,560
 your illness?

150
00:10:14,560 --> 00:10:18,440
 And that's where this verse comes from. Jivaka asked him

151
00:10:18,440 --> 00:10:19,920
 this simple question

152
00:10:21,280 --> 00:10:28,130
 and the Buddha said, "For one who has gone to the end, who

153
00:10:28,130 --> 00:10:30,800
 is free from sorrow,

154
00:10:30,800 --> 00:10:36,940
 one who is free everywhere, who has become liberated

155
00:10:36,940 --> 00:10:42,080
 everywhere, who has abandoned all

156
00:10:42,080 --> 00:10:53,500
 ties, parilahu namichati, there is no fever to be found, no

157
00:10:53,500 --> 00:10:53,600
 distress to be found." The word fever is

158
00:10:53,600 --> 00:10:58,590
 too ambiguous, but whenever becomes distressed. So this was

159
00:10:58,590 --> 00:11:00,640
, I think last night we had the question

160
00:11:00,640 --> 00:11:05,370
 whether an Arahant can suffer, whether it can actually be

161
00:11:05,370 --> 00:11:07,600
 called suffering when an Arahant

162
00:11:07,600 --> 00:11:10,820
 feels pain because they don't have, and this is what they

163
00:11:10,820 --> 00:11:12,880
 don't have. The translation that we've

164
00:11:12,880 --> 00:11:15,750
 got in the book actually says it translates parilaha as

165
00:11:15,750 --> 00:11:17,840
 suffering, but that's probably not

166
00:11:17,840 --> 00:11:23,540
 accurate. It's again too ambiguous, too general. The

167
00:11:23,540 --> 00:11:30,160
 specific meaning is burning up, so it's often

168
00:11:30,160 --> 00:11:34,500
 referring to the body, but here it means burning up in the

169
00:11:34,500 --> 00:11:37,520
 mind. Were you distressed? Did it

170
00:11:37,520 --> 00:11:42,310
 distress you? Upset? Did it upset you? Were you upset by it

171
00:11:42,310 --> 00:11:45,120
? Were you distressed by it? Were you

172
00:11:45,120 --> 00:11:49,440
 vexed by it? And the Buddha said there is no vexation for

173
00:11:49,440 --> 00:11:51,200
 one who has become free.

174
00:11:56,400 --> 00:11:59,200
 And so that's the answer to that question, that they do

175
00:11:59,200 --> 00:12:02,480
 actually feel dukkavedana in the body,

176
00:12:02,480 --> 00:12:05,840
 but they're not vexed by it. A very important point that

177
00:12:05,840 --> 00:12:07,760
 all of us as meditators, hopefully by

178
00:12:07,760 --> 00:12:10,850
 this time, have heard or have come to be familiar with, but

179
00:12:10,850 --> 00:12:12,560
 it's a point that has to be made

180
00:12:12,560 --> 00:12:16,100
 especially for people who are new to the meditation. The

181
00:12:16,100 --> 00:12:20,080
 difference between not only physical suffering,

182
00:12:20,080 --> 00:12:24,710
 but external conditions like the state of of ji-vaka who

183
00:12:24,710 --> 00:12:29,040
 was stuck in the city was incredibly vexed

184
00:12:29,040 --> 00:12:31,810
 and disturbed and distressed. You know, he had a good heart

185
00:12:31,810 --> 00:12:34,160
, but he wasn't helping the Buddha by

186
00:12:34,160 --> 00:12:39,170
 being vexed and distressed by being upset. And in fact he

187
00:12:39,170 --> 00:12:42,240
 was simply doing damage to his own mind.

188
00:12:43,520 --> 00:12:48,030
 And so to some extent the Buddha was using this as a lesson

189
00:12:48,030 --> 00:12:50,240
 to ji-vaka as well, not only

190
00:12:50,240 --> 00:12:54,450
 to reassure him, but also to remind him not to get upset. I

191
00:12:54,450 --> 00:12:57,360
 wasn't upset, why did you get upset?

192
00:12:57,360 --> 00:13:03,890
 And so this is because of the difference between the

193
00:13:03,890 --> 00:13:07,760
 physical, the external and our reactions.

194
00:13:07,760 --> 00:13:11,040
 Pain doesn't have to be a bad thing, excruciating pain

195
00:13:11,040 --> 00:13:12,640
 doesn't have to be a bad thing.

196
00:13:14,240 --> 00:13:17,470
 Situations don't have to cause us anger and frustration and

197
00:13:17,470 --> 00:13:20,480
 upset or boredom or worry or fear.

198
00:13:20,480 --> 00:13:25,510
 And you know when people do things to us, when others hurt

199
00:13:25,510 --> 00:13:26,000
 us,

200
00:13:26,000 --> 00:13:31,590
 we do the worst thing when we respond with anger. When

201
00:13:31,590 --> 00:13:36,080
 someone gets angry at us and we respond with

202
00:13:36,080 --> 00:13:41,860
 anger, we're the worst. We are creating the worst evil,

203
00:13:41,860 --> 00:13:43,440
 Buddha said.

204
00:13:43,440 --> 00:13:51,280
 But this is a clear indication of the difference between

205
00:13:51,280 --> 00:13:58,340
 our experiences and our reactions to them. So what do we

206
00:13:58,340 --> 00:14:00,400
 have? One who has gone to the end

207
00:14:00,400 --> 00:14:04,410
 means, actually I don't know, gat adino, that's translated

208
00:14:04,410 --> 00:14:05,760
 as one who has gone to the end of the

209
00:14:05,760 --> 00:14:08,820
 journey. Yeah, well that's what they say here, but let's

210
00:14:08,820 --> 00:14:10,320
 see exactly what it means.

211
00:14:10,320 --> 00:14:21,000
 Adin, one who has gone the distance, so who has completed

212
00:14:21,000 --> 00:14:21,680
 the path.

213
00:14:21,680 --> 00:14:25,080
 I think a meditator is asking how you know how far you're

214
00:14:25,080 --> 00:14:27,120
 progressing and it's all this idea of

215
00:14:27,120 --> 00:14:31,270
 the path and I'm often cynical about whether we should

216
00:14:31,270 --> 00:14:34,480
 really focus too much on the idea of a path.

217
00:14:35,200 --> 00:14:41,710
 It's misleading I think, you know. I think it's fine to

218
00:14:41,710 --> 00:14:44,640
 talk about the idea of a path,

219
00:14:44,640 --> 00:14:47,380
 but when we are concerned about our progress it can become

220
00:14:47,380 --> 00:14:48,240
 quite obsessive

221
00:14:48,240 --> 00:14:51,670
 as we're worried about where we are, wondering when we're

222
00:14:51,670 --> 00:14:53,680
 going to get there, that kind of thing.

223
00:14:53,680 --> 00:14:59,520
 On the other hand, having the idea of the path is a

224
00:14:59,520 --> 00:15:02,560
 reminder that we can't be complacent,

225
00:15:02,560 --> 00:15:06,630
 that we do have to walk and journey, but it's important as

226
00:15:06,630 --> 00:15:09,440
 a teacher and as a meditator to

227
00:15:09,440 --> 00:15:14,680
 not be too focused on progress. So this idea of the path or

228
00:15:14,680 --> 00:15:17,360
 the journey, it's poetic and it's

229
00:15:17,360 --> 00:15:20,680
 useful to know that there is a journey, but when you're

230
00:15:20,680 --> 00:15:23,120
 walking a journey you don't want to be the

231
00:15:23,120 --> 00:15:25,290
 person in the back seat saying, "Are we there yet? Are we

232
00:15:25,290 --> 00:15:29,440
 there yet?" That part of the mind should be

233
00:15:29,440 --> 00:15:33,710
 silenced. The walking has to continue. All you have to do

234
00:15:33,710 --> 00:15:36,400
 is put one foot in front of the other.

235
00:15:36,400 --> 00:15:39,690
 You know that there's a path and you follow it. It's much

236
00:15:39,690 --> 00:15:42,320
 simpler than we think. We think of it

237
00:15:42,320 --> 00:15:47,470
 as being some kind of like a paved road with road signs

238
00:15:47,470 --> 00:15:51,200
 telling you 10 kilometers left or something

239
00:15:51,200 --> 00:15:54,190
 like that, but it's not really. It's a path we've never

240
00:15:54,190 --> 00:15:57,760
 gone and the path that has no clear signs

241
00:15:57,760 --> 00:16:02,740
 that we can distinguish because we have nothing, no frame

242
00:16:02,740 --> 00:16:04,000
 of reference.

243
00:16:04,000 --> 00:16:09,070
 So our job is yes to follow the path, but that simply means

244
00:16:09,070 --> 00:16:10,880
 putting one foot in front of the

245
00:16:10,880 --> 00:16:16,130
 other, metaphorically speaking. We Soka, so an enlightened

246
00:16:16,130 --> 00:16:19,280
 being has no Soka, no sadness,

247
00:16:19,280 --> 00:16:24,380
 no sorrow. They don't long for anything. They don't pine

248
00:16:24,380 --> 00:16:25,440
 away at loss.

249
00:16:27,200 --> 00:16:31,050
 They don't worry about losing the things that they have. No

250
00:16:31,050 --> 00:16:33,120
 Soka, no sadness.

251
00:16:33,120 --> 00:16:38,550
 "Uipamutta" says somebody in all directions, on all sides,

252
00:16:38,550 --> 00:16:41,120
 everywhere. "Uipamutta"

253
00:16:41,120 --> 00:21:04,940
 freed, liberated, completely liberated. "Sabhaganta" bahin

254
00:21:04,940 --> 00:16:53,920
asa, having abandoned or destroyed

255
00:16:53,920 --> 00:17:05,120
 or gotten rid of all binds or ties, severed all ties.

256
00:17:05,120 --> 00:17:14,650
 So people talk a lot about detachment, how Buddhism is a

257
00:17:14,650 --> 00:17:18,000
 clear indication of how Buddhism does

258
00:17:21,040 --> 00:17:27,940
 encourage the idea of detachment. But people get the wrong

259
00:17:27,940 --> 00:17:30,640
 idea that it somehow translates into a

260
00:17:30,640 --> 00:17:36,590
 sense of or a state of zombie-like nothingness or meaning

261
00:17:36,590 --> 00:17:41,680
lessness. All it means is to not cling to

262
00:17:41,680 --> 00:17:46,190
 things as they go by, because the problem is we have this

263
00:17:46,190 --> 00:17:51,680
 idea that things exist constantly or in

264
00:17:51,680 --> 00:17:55,820
 some stable form. And so we're talking about shying away

265
00:17:55,820 --> 00:17:58,720
 from them, that people think non-attachment

266
00:17:58,720 --> 00:18:02,730
 means it's there, don't go near it. But that's not it. It's

267
00:18:02,730 --> 00:18:05,120
 not there actually. What is there is a

268
00:18:05,120 --> 00:18:09,000
 moment and that arises and ceases. That's fine. Be as close

269
00:18:09,000 --> 00:18:11,280
 and as clearly aware of that as you can

270
00:18:11,280 --> 00:18:16,140
 be. That's the point. But when it's gone, don't go with it,

271
00:18:16,140 --> 00:18:19,120
 you know. Don't let it pull you into the

272
00:18:19,120 --> 00:18:23,560
 past. Don't let thoughts about the future pull you into the

273
00:18:23,560 --> 00:18:26,320
 future. You end up like this, being

274
00:18:26,320 --> 00:18:30,730
 pulled to both sides, past and future. And you're never

275
00:18:30,730 --> 00:18:34,080
 really here and now. That's what non-attachment

276
00:18:34,080 --> 00:18:38,320
 means. It would make sense if there were things here that

277
00:18:38,320 --> 00:18:40,480
 were stable to attach to them.

278
00:18:41,200 --> 00:18:43,530
 Because you could depend upon them, right? But there's

279
00:18:43,530 --> 00:18:45,680
 nothing like that. Nothing is like,

280
00:18:45,680 --> 00:18:49,720
 is of that sort. Everything arises and ceases, comes and

281
00:18:49,720 --> 00:18:52,960
 goes, uncertain. Even all the possessions

282
00:18:52,960 --> 00:18:56,310
 that we have, that we think we have constantly throughout

283
00:18:56,310 --> 00:18:58,720
 our lives, the people who are with us.

284
00:18:58,720 --> 00:19:04,060
 The reason we're so sad is because we take them as some

285
00:19:04,060 --> 00:19:08,080
 stable entity that can be controlled and

286
00:19:08,080 --> 00:19:13,090
 that is long lasting and so on. And so we bind ourselves to

287
00:19:13,090 --> 00:19:15,440
 them with this attachment.

288
00:19:15,440 --> 00:19:20,190
 Non-attachment or detachment. Non-attachment is a better

289
00:19:20,190 --> 00:19:24,000
 way of phrasing it. Non-attachment means

290
00:19:24,000 --> 00:19:28,600
 experiencing. But experiencing as experience. Seeing people

291
00:19:28,600 --> 00:19:30,960
 as individual experiences. Seeing

292
00:19:30,960 --> 00:19:33,630
 things as individual experiences because that's what they

293
00:19:33,630 --> 00:19:35,440
 are. That's what you really have.

294
00:19:36,400 --> 00:19:38,760
 How can you say you have a car when you don't see it, when

295
00:19:38,760 --> 00:19:39,520
 you don't hear it,

296
00:19:39,520 --> 00:19:42,560
 when you're not in front of it, when it's not in front of

297
00:19:42,560 --> 00:19:45,680
 you? How can you say that you even have

298
00:19:45,680 --> 00:19:48,630
 people in your life when they're not here and now? All you

299
00:19:48,630 --> 00:19:50,880
 have is a thought that arises and ceases,

300
00:19:50,880 --> 00:19:53,950
 even when they're with you. All you have is seeing, hearing

301
00:19:53,950 --> 00:19:57,280
, smelling, tasting, feeling and thinking.

302
00:20:00,480 --> 00:20:05,120
 So non-attachment just means seeing things as they are and

303
00:20:05,120 --> 00:20:07,040
 being with things. So being with,

304
00:20:07,040 --> 00:20:09,590
 there's people around you, there's people around you. If

305
00:20:09,590 --> 00:20:11,200
 there's no one around you, there's no one

306
00:20:11,200 --> 00:20:14,990
 around you. It's not pining away after something that isn't

307
00:20:14,990 --> 00:20:17,440
 there or clinging to something that is.

308
00:20:17,440 --> 00:20:24,790
 For such a person who is of this sort, there is no fever.

309
00:20:24,790 --> 00:20:26,160
 So what it means is they have no

310
00:20:26,720 --> 00:20:31,530
 likes or dislikes, they have no vexation and no upset when

311
00:20:31,530 --> 00:20:34,240
 things don't go as expected because

312
00:20:34,240 --> 00:20:38,990
 they have no expectations. They're perfectly at peace and

313
00:20:38,990 --> 00:20:41,440
 perfectly flexible. I think flexible

314
00:20:41,440 --> 00:20:45,790
 is a very important aspect. Just naturally flexible because

315
00:20:45,790 --> 00:20:47,760
 they have no preference.

316
00:20:49,040 --> 00:20:54,430
 They're content the way things are however they are. So

317
00:20:54,430 --> 00:20:57,360
 that's the Dhammapada for tonight.

318
00:20:57,360 --> 00:21:04,640
 Thank you all for tuning in and keep practicing and be well

319
00:21:04,640 --> 00:21:04,720
.

