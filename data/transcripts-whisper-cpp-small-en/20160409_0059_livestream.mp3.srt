1
00:00:00,000 --> 00:00:15,000
 Good evening everyone.

2
00:00:15,000 --> 00:00:29,000
 We're broadcasting live some time in April.

3
00:00:29,000 --> 00:00:35,410
 Today's quote is an important quote, so it doesn't say a

4
00:00:35,410 --> 00:00:42,000
 lot.

5
00:00:42,000 --> 00:01:02,000
 But it said, "Yantjabikvayrating tathagato anuttarang sama

6
00:01:02,000 --> 00:01:02,000
 sambuding abyssambu janti."

7
00:01:02,000 --> 00:01:09,010
 If you take the night when the prathagata became

8
00:01:09,010 --> 00:01:16,000
 enlightened to unexcelled self-enlightenment,

9
00:01:16,000 --> 00:01:19,000
 yantjab and the ratinganupadhisesaya nibbanadhatuya parinib

10
00:01:19,000 --> 00:01:37,000
ayati, and the night of his becoming fully unbound

11
00:01:37,000 --> 00:01:46,000
 through the element of unbinding that is without remainder.

12
00:01:46,000 --> 00:01:55,000
 So the night when he passed away.

13
00:01:55,000 --> 00:02:00,700
 So the Bodhisattva, after making a determination at the

14
00:02:00,700 --> 00:02:08,330
 foot of the Buddha Deepankara, so many countless number of

15
00:02:08,330 --> 00:02:15,000
 eons and world periods ago,

16
00:02:15,000 --> 00:02:21,010
 he made this determination and then entered on the path to

17
00:02:21,010 --> 00:02:27,560
 become a Buddha, and he spent four asankaya, four uncount

18
00:02:27,560 --> 00:02:31,000
able periods of time.

19
00:02:31,000 --> 00:02:36,000
 Not just one uncountable period of time, but four of them.

20
00:02:36,000 --> 00:02:43,040
 So many periods of time that have some meaning but really

21
00:02:43,040 --> 00:02:46,000
 can't measure them. They're just too long.

22
00:02:46,000 --> 00:02:57,000
 And then 100,000 mahakappa, the great eons on top of that.

23
00:02:57,000 --> 00:03:00,000
 So this is being born and die again and again.

24
00:03:00,000 --> 00:03:05,240
 So we have all these stories, only a very small number of

25
00:03:05,240 --> 00:03:10,220
 stories, but a number of useful stories about the time that

26
00:03:10,220 --> 00:03:13,000
 the Buddha spent from birth to birth,

27
00:03:13,000 --> 00:03:21,000
 perfecting the qualities of mind needed to become a Buddha.

28
00:03:21,000 --> 00:03:28,000
 And then he was born in his last life as Siddhata Gautama.

29
00:03:28,000 --> 00:03:37,020
 And at the age of 29, he left home having seen that in life

30
00:03:37,020 --> 00:03:40,000
 there are certain inevitabilities.

31
00:03:40,000 --> 00:03:45,000
 We have to get old, we have to get sick, we have to die.

32
00:03:45,000 --> 00:03:49,000
 And no matter what we do, no matter how we succeed in life,

33
00:03:49,000 --> 00:03:53,330
 no matter whether we're good or bad, right or wrong, rich

34
00:03:53,330 --> 00:03:57,000
 or poor, famous or infamous,

35
00:03:57,000 --> 00:04:01,620
 in the end these things come to us all and they wash away

36
00:04:01,620 --> 00:04:04,000
 everything we've done.

37
00:04:04,000 --> 00:04:11,000
 And so he left home to try and find a way out of this mess.

38
00:04:11,000 --> 00:04:14,570
 And for six years he tortured himself and tried all

39
00:04:14,570 --> 00:04:18,000
 different meditations and ways of practicing.

40
00:04:18,000 --> 00:04:23,060
 But he found that none of them led to freedom, they only

41
00:04:23,060 --> 00:04:28,000
 led him to places he'd been before from birth to birth.

42
00:04:28,000 --> 00:04:34,460
 Until finally he cleared his mind enough to be able to

43
00:04:34,460 --> 00:04:40,000
 understand that he had to find the middle way,

44
00:04:40,000 --> 00:04:46,430
 he had to find an ordinary way, a natural way, where he was

45
00:04:46,430 --> 00:04:52,000
 accepting of pain but also accepting of pleasure.

46
00:04:52,000 --> 00:04:56,000
 And where he was able to see things objectively.

47
00:04:56,000 --> 00:05:01,990
 And he looked at things for what they are, not with any

48
00:05:01,990 --> 00:05:08,000
 goal or ulterior motive, not with any reaction.

49
00:05:08,000 --> 00:05:11,890
 And so he began to see, his mind began to clear and he saw

50
00:05:11,890 --> 00:05:16,250
 his past lives, and he saw the arising and ceasing of

51
00:05:16,250 --> 00:05:20,000
 beings being born here and born there.

52
00:05:20,000 --> 00:05:25,720
 Finally he was able to make sense of everything and he saw

53
00:05:25,720 --> 00:05:33,490
 how our birth and our life and our death is just part of a

54
00:05:33,490 --> 00:05:37,000
 much greater

55
00:05:37,000 --> 00:05:54,000
 system of a cycle of rebirth.

56
00:05:54,000 --> 00:05:57,620
 And then he began to look at what causes the cycle of

57
00:05:57,620 --> 00:06:02,000
 rebirth and he saw his own mind giving rise to attachment,

58
00:06:02,000 --> 00:06:08,000
 giving rise to desire, giving rise to further rebirth.

59
00:06:08,000 --> 00:06:12,670
 And as he saw the truth he came to let it go, and when he

60
00:06:12,670 --> 00:06:16,000
 let it go he was freed for himself.

61
00:06:16,000 --> 00:06:19,000
 He became Buddha. So that's the night of his enlightenment.

62
00:06:19,000 --> 00:06:23,660
 Now, a lot of stories of the Buddha stop there and they are

63
00:06:23,660 --> 00:06:27,000
 only interested in that time, that day.

64
00:06:27,000 --> 00:06:30,000
 But that's the day, the first day he's talking about.

65
00:06:30,000 --> 00:06:33,000
 That's an important day.

66
00:06:33,000 --> 00:06:36,000
 But it's funny how a lot of stories of the Buddha stop

67
00:06:36,000 --> 00:06:38,000
 there and say, "Well, that was most important."

68
00:06:38,000 --> 00:06:41,000
 End of story.

69
00:06:41,000 --> 00:06:45,950
 That's actually not. And that part of the story is actually

70
00:06:45,950 --> 00:06:49,000
 the less important part.

71
00:06:49,000 --> 00:06:52,050
 If that was it then you'd think, "Wow, okay." So he became

72
00:06:52,050 --> 00:06:59,000
 Buddha. Big deal.

73
00:06:59,000 --> 00:07:05,000
 The other day is 45 years later.

74
00:07:05,000 --> 00:07:12,350
 He lay down between the two sala trees. Sala trees are a

75
00:07:12,350 --> 00:07:16,730
 flowering tree in northern India, actually all throughout

76
00:07:16,730 --> 00:07:19,000
 India, even in Sri Lanka.

77
00:07:19,000 --> 00:07:25,220
 There's even at least one that I know of in Thailand, but

78
00:07:25,220 --> 00:07:28,000
 it's in a colder area.

79
00:07:28,000 --> 00:07:35,050
 And he lay down and entered into final nirvana. The body

80
00:07:35,050 --> 00:07:37,000
 passed away.

81
00:07:37,000 --> 00:07:40,410
 And with the passing of the body there was no more arising

82
00:07:40,410 --> 00:07:42,000
 of mental constructs.

83
00:07:42,000 --> 00:07:47,000
 I mean no more rebirth.

84
00:07:47,000 --> 00:07:52,000
 Which is also a very important day.

85
00:07:52,000 --> 00:07:59,480
 And those two together really describe what it is that we

86
00:07:59,480 --> 00:08:05,000
're not even aiming for, but it's the ultimate.

87
00:08:05,000 --> 00:08:10,100
 Because Buddhism is all about letting go. It's about

88
00:08:10,100 --> 00:08:14,000
 freeing yourself from suffering.

89
00:08:14,000 --> 00:08:18,040
 And it sometimes scares people, I think, when they think of

90
00:08:18,040 --> 00:08:22,000
 the Buddha's enlightenment, the Buddha's final freedom.

91
00:08:22,000 --> 00:08:29,000
 They think, "Wow, you know, never coming back."

92
00:08:29,000 --> 00:08:31,820
 So it's not really our goal, because most people aren't

93
00:08:31,820 --> 00:08:33,000
 interested in that.

94
00:08:33,000 --> 00:08:35,870
 It's hard for us to think of. Of course, we don't want that

95
00:08:35,870 --> 00:08:40,000
. We want to be born again, for the most part.

96
00:08:40,000 --> 00:08:43,310
 Even though we may say, "Oh man, I don't want to be born

97
00:08:43,310 --> 00:08:44,000
 again. I have to go through this."

98
00:08:44,000 --> 00:08:48,170
 But still we have desire, and so still we'll be reborn

99
00:08:48,170 --> 00:08:49,000
 again.

100
00:08:49,000 --> 00:08:53,270
 Until we attain freedom from all that, then we're not born

101
00:08:53,270 --> 00:08:54,000
 again.

102
00:08:54,000 --> 00:08:58,320
 But the point is, that's the end. That's the final. Final

103
00:08:58,320 --> 00:09:00,000
 letting go, the final freedom.

104
00:09:00,000 --> 00:09:05,000
 Until that point you're still not free. That's the point.

105
00:09:05,000 --> 00:09:07,910
 And so it's not that we have to wish for it, but that's

106
00:09:07,910 --> 00:09:10,000
 where you're going eventually.

107
00:09:10,000 --> 00:09:12,160
 You let go of more, and so wherever you're born, you're

108
00:09:12,160 --> 00:09:13,000
 born in a good way.

109
00:09:13,000 --> 00:09:16,500
 You let go of even more, and you're born in an even better

110
00:09:16,500 --> 00:09:17,000
 way.

111
00:09:17,000 --> 00:09:21,230
 Until you let go of everything, and you're born without it,

112
00:09:21,230 --> 00:09:23,000
 then you're not born.

113
00:09:23,000 --> 00:09:28,000
 There's no suffering. No having to come back.

114
00:09:28,000 --> 00:09:32,380
 So those two are two important days, but this quote is

115
00:09:32,380 --> 00:09:34,680
 important because it talks about something entirely

116
00:09:34,680 --> 00:09:35,000
 different.

117
00:09:35,000 --> 00:09:38,980
 It talks about those two as the boundary, and it places

118
00:09:38,980 --> 00:09:42,000
 emphasis on what happened in between those two.

119
00:09:42,000 --> 00:09:47,490
 Which, as I say, it's a shame that most stories about the

120
00:09:47,490 --> 00:09:50,000
 Buddha miss entirely.

121
00:09:50,000 --> 00:09:54,820
 Part of the reason why they do, of course, is because that

122
00:09:54,820 --> 00:10:00,000
 part of the Buddha's life has unfortunately been minimized.

123
00:10:00,000 --> 00:10:06,000
 The Buddha spent 45 years teaching. 45 years.

124
00:10:06,000 --> 00:10:11,340
 The Thai version of what's now claimed to be his teachings

125
00:10:11,340 --> 00:10:14,000
 takes up 45 volumes.

126
00:10:14,000 --> 00:10:18,260
 So they joke it's like he made a volume a year, but it's

127
00:10:18,260 --> 00:10:19,000
 huge.

128
00:10:19,000 --> 00:10:24,610
 He spent 45 years, and there's so much that we have of his

129
00:10:24,610 --> 00:10:26,000
 teachings.

130
00:10:26,000 --> 00:10:30,860
 And then you have the Lotus Sutra, which I've been talking

131
00:10:30,860 --> 00:10:35,000
 about quite a bit recently, trivializing it.

132
00:10:35,000 --> 00:10:40,460
 And so you have a lot of later Buddhists and trivializing

133
00:10:40,460 --> 00:10:41,000
 that.

134
00:10:41,000 --> 00:10:53,000
 Saying what he taught there, that was just conditional.

135
00:10:53,000 --> 00:10:56,000
 I can't remember the word. There's a word for it.

136
00:10:56,000 --> 00:11:04,000
 It was just a part of the path.

137
00:11:04,000 --> 00:11:08,510
 He wasn't teaching. In fact, it says he wasn't teaching the

138
00:11:08,510 --> 00:11:10,000
 truth, basically.

139
00:11:10,000 --> 00:11:14,770
 He was teaching an expedient means, something that was not

140
00:11:14,770 --> 00:11:16,000
 really true.

141
00:11:16,000 --> 00:11:20,910
 But if you follow it, it will help you see clear enough to

142
00:11:20,910 --> 00:11:23,000
 see the whole truth.

143
00:11:23,000 --> 00:11:31,000
 Anyway, there's generally a marginalization.

144
00:11:31,000 --> 00:11:36,000
 For our purposes, those 45 years are most important.

145
00:11:36,000 --> 00:11:38,750
 And the question is, do we have what the Buddha taught

146
00:11:38,750 --> 00:11:40,000
 during those times?

147
00:11:40,000 --> 00:11:45,040
 Do we still have it? Do we know what the Buddha actually

148
00:11:45,040 --> 00:11:46,000
 taught?

149
00:11:46,000 --> 00:11:53,730
 The Buddha said, "Young, eta-sming, antare bhassati, lapati

150
00:11:53,730 --> 00:11:56,000
, nidhissati."

151
00:11:56,000 --> 00:12:06,110
 Those, whatever eta-sming between these two antare, these

152
00:12:06,110 --> 00:12:09,000
 two boundaries,

153
00:12:09,000 --> 00:12:19,260
 bhassati said, "Lapati is spoken, nidhissati is pointed out

154
00:12:19,260 --> 00:12:20,000
."

155
00:12:20,000 --> 00:12:27,000
 "Sabang tang tathay wahoti no anyata."

156
00:12:27,000 --> 00:12:34,000
 All of that is thus, just thus.

157
00:12:34,000 --> 00:12:44,000
 "Tathay wahoti," it is just as thus.

158
00:12:44,000 --> 00:12:51,650
 Just in that way. "No anyata," not in another way, not

159
00:12:51,650 --> 00:12:52,000
 otherwise.

160
00:12:52,000 --> 00:12:59,000
 So whatever he says, it's true. It's thus.

161
00:12:59,000 --> 00:13:03,270
 "Tasmat tathay wahoti wuchati," thus he is called "tathag

162
00:13:03,270 --> 00:13:04,000
ata."

163
00:13:04,000 --> 00:13:10,000
 "Tatha" means "thus," "katha" means "gone."

164
00:13:10,000 --> 00:13:16,680
 "Yathawali bhikkhuet tathagatot tathagari," that's

165
00:13:16,680 --> 00:13:18,000
 something else.

166
00:13:18,000 --> 00:13:24,000
 But he says, "As he speaks, so he does."

167
00:13:24,000 --> 00:13:30,000
 "Yathawagari tathawali," as he does, so he speaks.

168
00:13:30,000 --> 00:13:35,000
 "Tasmat tathagatot wuchati," thus he is called "tathagata."

169
00:13:35,000 --> 00:13:42,000
 But the question, sorry, relates to the quote.

170
00:13:42,000 --> 00:13:45,000
 All of what he taught was truth for 45 years.

171
00:13:45,000 --> 00:13:49,000
 So the question is, what do we have? Do we have that truth?

172
00:13:49,000 --> 00:13:54,110
 Some people say, "It's been changed, it's been altered, it

173
00:13:54,110 --> 00:13:58,000
's been recreated by sectarians."

174
00:13:58,000 --> 00:14:02,000
 Everybody arguing that they have the truth.

175
00:14:02,000 --> 00:14:07,000
 So it's hard to be sure, do you have the truth?

176
00:14:07,000 --> 00:14:11,090
 It's a bold claim here, it's a great thing that the Buddha,

177
00:14:11,090 --> 00:14:13,000
 great that the Buddha actually taught the truth.

178
00:14:13,000 --> 00:14:18,150
 The question is, do we have the truth now? Where do we find

179
00:14:18,150 --> 00:14:20,000
 the truth?

180
00:14:20,000 --> 00:14:26,790
 And the curious thing that I find which gives great faith

181
00:14:26,790 --> 00:14:33,080
 in what we claim to be the Buddha's teaching, for the most

182
00:14:33,080 --> 00:14:35,000
 part anyway,

183
00:14:35,000 --> 00:14:38,000
 is that if you take a practice like the one we practice,

184
00:14:38,000 --> 00:14:42,000
 which I have perfect faith in it,

185
00:14:42,000 --> 00:14:50,000
 there's no question that it is objective.

186
00:14:50,000 --> 00:14:54,050
 You can argue what you want. You can say this practice is

187
00:14:54,050 --> 00:14:57,000
 dumb or wrong or misguided.

188
00:14:57,000 --> 00:15:03,000
 But you can't say that it's partial or biased. Not exactly.

189
00:15:03,000 --> 00:15:08,670
 Unless you want to say it's biased in a pedantic sort of

190
00:15:08,670 --> 00:15:13,000
 way or it's overly technical or so on.

191
00:15:13,000 --> 00:15:15,700
 But it's systematic and it's subjective. When you sit and

192
00:15:15,700 --> 00:15:22,000
 say, "Pain, pain," there's no bias there.

193
00:15:22,000 --> 00:15:27,900
 You're seeing pain as pain. You're reminding yourself that

194
00:15:27,900 --> 00:15:29,000
's pain.

195
00:15:29,000 --> 00:15:33,680
 You say, "Rising," there's no bias here. So what I'm trying

196
00:15:33,680 --> 00:15:35,950
 to say is this is an objective practice, the practice of

197
00:15:35,950 --> 00:15:37,000
 objectivity.

198
00:15:37,000 --> 00:15:40,960
 You can dislike it. You can say, "It's not for me. It's not

199
00:15:40,960 --> 00:15:43,000
 going to lead me anywhere."

200
00:15:43,000 --> 00:15:45,660
 But you can't argue that it's not objective. And the

201
00:15:45,660 --> 00:15:49,210
 interesting thing is, if you practice this way without any

202
00:15:49,210 --> 00:15:55,000
 instruction in the greater context of the Buddha's teaching

203
00:15:55,000 --> 00:15:55,000
,

204
00:15:55,000 --> 00:15:59,580
 if you practice diligently for some time, you can go and

205
00:15:59,580 --> 00:16:02,320
 read all the Buddha's teaching and you can find for

206
00:16:02,320 --> 00:16:05,000
 yourself the truth.

207
00:16:05,000 --> 00:16:10,230
 I would argue. What I mean to say is you react differently

208
00:16:10,230 --> 00:16:13,000
 to different kinds of teaching.

209
00:16:13,000 --> 00:16:16,700
 If you focus on objectivity, if you focus on a practice

210
00:16:16,700 --> 00:16:20,000
 that is objective, not a practice that focuses on some God

211
00:16:20,000 --> 00:16:24,000
 or some angel or some kind of love or compassion,

212
00:16:24,000 --> 00:16:28,890
 if you focus on truth, this is pain. If you say to yourself

213
00:16:28,890 --> 00:16:32,370
, "Pain, pain," you'll see you can argue what you want, but

214
00:16:32,370 --> 00:16:35,000
 you're seeing pain as pain.

215
00:16:35,000 --> 00:16:38,000
 You're telling yourself, "Pain is pain." Is that bias? No.

216
00:16:38,000 --> 00:16:41,000
 Is that arbitrary? No.

217
00:16:41,000 --> 00:16:44,290
 So then when you go and look at any kind of teachings, you

218
00:16:44,290 --> 00:16:49,000
 don't need someone to tell you that that's the truth.

219
00:16:49,000 --> 00:16:55,000
 You can look at it and you can say, "Hmm, that is true."

220
00:16:55,000 --> 00:17:00,970
 You can tell the difference between a teaching that is just

221
00:17:00,970 --> 00:17:07,530
 saying nice things or saying things that are just sophism

222
00:17:07,530 --> 00:17:10,000
 or intellectualism,

223
00:17:10,000 --> 00:17:14,000
 versus teachings that actually teach something important.

224
00:17:14,000 --> 00:17:18,080
 You can tell the difference. So the interesting thing is

225
00:17:18,080 --> 00:17:20,000
 you react to teachings.

226
00:17:20,000 --> 00:17:22,680
 Like if you read the Dibhittika, for example, without

227
00:17:22,680 --> 00:17:26,240
 having practiced this kind of meditation, I think you have

228
00:17:26,240 --> 00:17:28,000
 a hard time with it.

229
00:17:28,000 --> 00:17:33,840
 I think it's hard to find it even interesting, let alone

230
00:17:33,840 --> 00:17:38,000
 comprehendable, understandable.

231
00:17:39,000 --> 00:17:47,000
 But meditation is that powerful.

232
00:17:47,000 --> 00:17:43,670
 This shows the importance, the power and the purity of

233
00:17:43,670 --> 00:18:05,000
 meditation, the depth, the profundity of it.

234
00:18:05,000 --> 00:18:13,000
 That it allows you to see.

235
00:18:13,000 --> 00:18:19,000
 It allows you to see in your life problems that you had.

236
00:18:19,000 --> 00:18:24,020
 It allows you to see through them, to solve them, without

237
00:18:24,020 --> 00:18:25,000
 doubt.

238
00:18:25,000 --> 00:18:31,250
 And teachings, views, it allows you to see right view from

239
00:18:31,250 --> 00:18:36,000
 wrong view without bias, without doubt.

240
00:18:36,000 --> 00:18:39,000
 You practice in this way.

241
00:18:39,000 --> 00:18:43,000
 And that really goes for all sorts of kinds of practice.

242
00:18:43,000 --> 00:18:47,000
 If you practice in that way, you'll see in that way.

243
00:18:47,000 --> 00:18:49,830
 So that's why it's important that our practice be objective

244
00:18:49,830 --> 00:18:52,000
, our practice be based on objectivity.

245
00:18:52,000 --> 00:18:55,030
 Because if you practice in a different religious tradition,

246
00:18:55,030 --> 00:18:57,280
 well, you'll look at the world in a different way and you

247
00:18:57,280 --> 00:18:59,000
'll gravitate towards different texts.

248
00:18:59,000 --> 00:19:01,550
 If you practice Christianity, you'll gravitate towards the

249
00:19:01,550 --> 00:19:02,000
 Bible.

250
00:19:02,000 --> 00:19:04,790
 You'll say, "Oh yes, the Bible is the truth," because it

251
00:19:04,790 --> 00:19:06,000
 resonates with you.

252
00:19:06,000 --> 00:19:08,800
 But it resonates, we would argue, because you become

253
00:19:08,800 --> 00:19:15,000
 partial and biased, and you've taken up views and beliefs.

254
00:19:15,000 --> 00:19:22,920
 So this is my test for text, is see through the lens of

255
00:19:22,920 --> 00:19:25,000
 objectivity.

256
00:19:25,000 --> 00:19:32,000
 You don't take any belief. You see things as they are.

257
00:19:32,000 --> 00:19:36,130
 See pain as pain, see thoughts as thought. See anger as

258
00:19:36,130 --> 00:19:39,000
 anger, see greed as greed.

259
00:19:39,000 --> 00:19:43,000
 See delusion as delusion.

260
00:19:43,000 --> 00:19:52,460
 Anyway, then you can see the truth. Then you can find the

261
00:19:52,460 --> 00:19:53,000
 Buddhist teaching.

262
00:19:53,000 --> 00:19:56,410
 And that relates to the other quotes that we've had, you

263
00:19:56,410 --> 00:19:57,000
 know.

264
00:19:57,000 --> 00:20:02,770
 "Yodhamang passati, soulmang passati," who sees the Dhamma,

265
00:20:02,770 --> 00:20:06,000
 sees the Buddha, sees the Dutahad.

266
00:20:06,000 --> 00:20:09,000
 If you don't see the Dhamma, you can't see the Buddha.

267
00:20:09,000 --> 00:20:12,380
 You can't find the Buddha's teaching. Even you have the

268
00:20:12,380 --> 00:20:14,000
 whole of the Tipitaka.

269
00:20:14,000 --> 00:20:18,150
 You'll never find the teaching there, not unless you see

270
00:20:18,150 --> 00:20:22,000
 the Dhamma, not unless you're practicing the Dhamma.

271
00:20:22,000 --> 00:20:27,130
 You can memorize the 45 volumes, and you still won't come

272
00:20:27,130 --> 00:20:29,000
 close to the truth.

273
00:20:29,000 --> 00:20:36,060
 But as soon as you practice, it's like it's a cipher that

274
00:20:36,060 --> 00:20:39,000
 deciphers the code.

275
00:20:39,000 --> 00:20:42,870
 Suddenly you understand. Otherwise people I think would

276
00:20:42,870 --> 00:20:44,000
 find it boring.

277
00:20:44,000 --> 00:20:50,000
 Maybe interesting intellectually, but incomprehensible.

278
00:20:50,000 --> 00:20:58,000
 Anyway, that's our quote for tonight.

279
00:20:59,000 --> 00:21:06,000
 Anybody have any questions?

280
00:21:06,000 --> 00:21:12,000
 We're doing text questions again, so welcome to ask.

281
00:21:12,000 --> 00:21:14,000
 You guys can go.

282
00:21:14,000 --> 00:21:21,000
 Two meditators here. We can say hi to our meditators.

283
00:21:21,000 --> 00:21:28,000
 And the spotters staying here tonight.

284
00:21:29,000 --> 00:21:47,000
 [

285
00:21:47,000 --> 00:22:04,000
 Pause ]

286
00:22:04,000 --> 00:22:33,000
 [ Pause ]

287
00:22:33,000 --> 00:22:41,000
 [ Pause ]

288
00:22:41,000 --> 00:22:48,000
 Hmm. No questions? Alright.

289
00:22:48,000 --> 00:22:53,000
 And good night everyone. Thanks for tuning in.

290
00:22:53,000 --> 00:23:11,000
 [ Pause ]

