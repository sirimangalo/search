1
00:00:00,000 --> 00:00:07,360
 Okay, so do the realizations of impermanence, suffering and

2
00:00:07,360 --> 00:00:09,920
 non-self have to be, or are

3
00:00:09,920 --> 00:00:14,320
 they supposed to be intellectual?

4
00:00:14,320 --> 00:00:26,050
 No, and in fact, in a sense, the experience of impermanent

5
00:00:26,050 --> 00:00:30,920
 suffering and non-self is the

6
00:00:30,920 --> 00:00:32,640
 absence, right?

7
00:00:32,640 --> 00:00:38,490
 Because they're all negative aspects or negative

8
00:00:38,490 --> 00:00:41,040
 characteristics.

9
00:00:41,040 --> 00:00:48,800
 It's not like non-self comes out and hits you in the face,

10
00:00:48,800 --> 00:00:51,440
 but what it is, is it's the

11
00:00:51,440 --> 00:00:57,520
 giving up of the belief that something is permanent.

12
00:00:57,520 --> 00:01:02,680
 It may come in your meditation that you intellectualize, or

13
00:01:02,680 --> 00:01:06,440
 not exactly intellectualize, but the thought

14
00:01:06,440 --> 00:01:07,440
 arises.

15
00:01:07,440 --> 00:01:12,720
 Wow, this is all impermanent.

16
00:01:12,720 --> 00:01:18,480
 This thought arises, like a realization.

17
00:01:18,480 --> 00:01:21,020
 But that's not the insight.

18
00:01:21,020 --> 00:01:22,520
 That's not the moment of insight.

19
00:01:22,520 --> 00:01:35,200
 It often follows, it's preceded by a moment of insight.

20
00:01:35,200 --> 00:01:41,320
 The moment of insight is the seeing for yourself.

21
00:01:41,320 --> 00:01:44,000
 For instance, in the case of impermanence, it's seeing

22
00:01:44,000 --> 00:01:45,040
 something cease.

23
00:01:45,040 --> 00:01:51,190
 It's the continued realization, the continued observation

24
00:01:51,190 --> 00:01:54,240
 of the cessation of phenomena.

25
00:01:54,240 --> 00:02:02,750
 For suffering, it's seeing the intolerability of the

26
00:02:02,750 --> 00:02:04,520
 phenomena.

27
00:02:04,520 --> 00:02:06,450
 Especially in terms of seeing that the things you thought

28
00:02:06,450 --> 00:02:07,520
 were going to make you happy are

29
00:02:07,520 --> 00:02:08,660
 not going to make you happy.

30
00:02:08,660 --> 00:02:11,680
 You see them arising and ceasing, arising and ceasing.

31
00:02:11,680 --> 00:02:16,000
 You lose the idea that they are sukha.

32
00:02:16,000 --> 00:02:22,960
 So in a sense, it's giving up, rather than it's giving up

33
00:02:22,960 --> 00:02:25,800
 of the belief of the opposite

34
00:02:25,800 --> 00:02:29,520
 characteristic.

35
00:02:29,520 --> 00:02:32,730
 When you see things as they are, you give up the belief in

36
00:02:32,730 --> 00:02:34,600
 permanence, in satisfaction,

37
00:02:34,600 --> 00:02:40,840
 or that things can be pleasant when you cling to something

38
00:02:40,840 --> 00:02:44,680
 that it can bring you happiness.

39
00:02:44,680 --> 00:02:46,800
 Number three, giving up the idea that you can control.

40
00:02:46,800 --> 00:02:50,240
 So we try to control our breath, like the stomach, for

41
00:02:50,240 --> 00:02:51,240
 example.

42
00:02:51,240 --> 00:02:53,100
 So in the beginning, you're just going to keep controlling

43
00:02:53,100 --> 00:02:54,040
 the stomach no matter what

44
00:02:54,040 --> 00:02:55,040
 you do.

45
00:02:55,040 --> 00:02:57,720
 You'll be practicing rising, falling.

46
00:02:57,720 --> 00:03:04,250
 Here you are forcing the breath at every in-breath, every

47
00:03:04,250 --> 00:03:08,720
 out-breath, forcing the stomach.

48
00:03:08,720 --> 00:03:11,770
 Really you start to see that it's not really forcing the

49
00:03:11,770 --> 00:03:12,720
 breath at all.

50
00:03:12,720 --> 00:03:14,640
 There's this conception of the forcing.

51
00:03:14,640 --> 00:03:19,130
 But eventually what you'll see is that all you're doing is

52
00:03:19,130 --> 00:03:20,800
 creating stress.

53
00:03:20,800 --> 00:03:24,860
 You're not actually controlling the breath, you're just

54
00:03:24,860 --> 00:03:26,040
 creating more stress.

55
00:03:26,040 --> 00:03:29,390
 The breath is arising based on, or the stomach is arising

56
00:03:29,390 --> 00:03:31,880
 based on many causes, many conditions,

57
00:03:31,880 --> 00:03:35,320
 only one of which is the mind.

58
00:03:35,320 --> 00:03:37,910
 But the mind is responsible for creating stress, which is

59
00:03:37,910 --> 00:03:39,520
 why when you try to control it, you

60
00:03:39,520 --> 00:03:43,290
 just feel more and more terrible, more and more unpleasant,

61
00:03:43,290 --> 00:03:44,960
 until finally you realize

62
00:03:44,960 --> 00:03:48,950
 that it's not really, you can't just say, because, I mean,

63
00:03:48,950 --> 00:03:50,220
 think about it.

64
00:03:50,220 --> 00:03:55,500
 This is what we don't realize, if you could make it, if it

65
00:03:55,500 --> 00:03:58,320
 was under your control, then

66
00:03:58,320 --> 00:04:00,990
 why can't you just say, "Okay, my breath is just going to

67
00:04:00,990 --> 00:04:02,380
 rise smoothly, it's going

68
00:04:02,380 --> 00:04:06,000
 to fall smoothly."

69
00:04:06,000 --> 00:04:07,640
 That's what we think we can do.

70
00:04:07,640 --> 00:04:14,210
 But it's totally off the wall, this idea that we can do

71
00:04:14,210 --> 00:04:19,000
 that, because it's totally unrelated

72
00:04:19,000 --> 00:04:22,080
 to the facts.

73
00:04:22,080 --> 00:04:24,400
 And eventually you realize this.

74
00:04:24,400 --> 00:04:26,590
 So it's not intellectualizing like I just did, I just had

75
00:04:26,590 --> 00:04:28,800
 you intellectualize and say,

76
00:04:28,800 --> 00:04:31,680
 as the Buddha did, well, if you could, then why can't you

77
00:04:31,680 --> 00:04:33,160
 control it to be like this or

78
00:04:33,160 --> 00:04:34,160
 like that?

79
00:04:34,160 --> 00:04:36,160
 It's not like that at all.

80
00:04:36,160 --> 00:04:42,080
 The realization is visceral, it's actually seeing the

81
00:04:42,080 --> 00:04:46,920
 breath arise and cease on its own.

82
00:04:46,920 --> 00:04:49,470
 The clear perception of non-self is when you actually see

83
00:04:49,470 --> 00:04:50,140
 like that.

84
00:04:50,140 --> 00:04:52,570
 It actually appears to you that the body is moving on its

85
00:04:52,570 --> 00:04:53,920
 own, it appears to you that

86
00:04:53,920 --> 00:04:55,860
 thoughts are arising on their own.

87
00:04:55,860 --> 00:04:58,070
 You're able to see this, and you're able to realize that

88
00:04:58,070 --> 00:05:00,120
 really that's all there is,

89
00:05:00,120 --> 00:05:04,150
 is phenomena arising and ceasing, and you give up any

90
00:05:04,150 --> 00:05:05,800
 desire to control.

91
00:05:05,800 --> 00:05:11,840
 The whole idea of control just goes out the window, and

92
00:05:11,840 --> 00:05:15,160
 that's when the realization of

93
00:05:15,160 --> 00:05:16,160
 non-self.

94
00:05:16,160 --> 00:05:19,500
 Let's go, in the mind, let's go, then there's freedom from

95
00:05:19,500 --> 00:05:20,360
 suffering.

96
00:05:20,360 --> 00:05:21,040
 That's how it should be.

