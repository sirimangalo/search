1
00:00:00,000 --> 00:00:04,760
 Hello everyone. I know I've been away from the camera for a

2
00:00:04,760 --> 00:00:12,000
 while so I thought I'd just post a bit of a video blog.

3
00:00:12,000 --> 00:00:17,820
 Who knows, maybe I can get back into regular video blogging

4
00:00:17,820 --> 00:00:18,000
.

5
00:00:18,000 --> 00:00:25,470
 I'm going to be back in Canada on April 2nd or 4th, around

6
00:00:25,470 --> 00:00:27,000
 the beginning of April.

7
00:00:27,000 --> 00:00:35,790
 So I should begin to do more regular video teachings from

8
00:00:35,790 --> 00:00:39,000
 then on back into the Dhammapada videos.

9
00:00:39,000 --> 00:00:43,570
 I'd like to finish up the introduction to Buddhism series

10
00:00:43,570 --> 00:00:47,000
 that we kind of left very much in the middle.

11
00:00:47,000 --> 00:00:54,520
 So I haven't abandoned the internet or YouTube and I will

12
00:00:54,520 --> 00:00:56,000
 be back for more teachings.

13
00:00:56,000 --> 00:00:59,380
 But this is just kind of a hello and a couple of

14
00:00:59,380 --> 00:01:04,000
 announcements in case people out there are interested.

15
00:01:04,000 --> 00:01:08,600
 The first announcement is that we've been working on

16
00:01:08,600 --> 00:01:13,000
 translations to my booklet on how to meditate.

17
00:01:13,000 --> 00:01:18,860
 So we have eight translations that have been completed for

18
00:01:18,860 --> 00:01:20,000
 a while now.

19
00:01:20,000 --> 00:01:24,500
 And I've gathered together a number of people to make

20
00:01:24,500 --> 00:01:27,000
 translations into their language.

21
00:01:27,000 --> 00:01:31,510
 We have Russian which is now being edited and Thai which is

22
00:01:31,510 --> 00:01:34,000
 in the process of being edited.

23
00:01:34,000 --> 00:01:41,400
 There's a Polish translation underway and there's a couple

24
00:01:41,400 --> 00:01:43,000
 of others.

25
00:01:43,000 --> 00:01:47,000
 So quite a few people have responded actually.

26
00:01:47,000 --> 00:01:50,020
 If you have a language that you'd like to see the booklet

27
00:01:50,020 --> 00:01:54,000
 translated in, please feel free to get in touch with me.

28
00:01:54,000 --> 00:01:57,840
 And by all means, if someone else hasn't already started

29
00:01:57,840 --> 00:02:02,000
 that language, we'd be happy to have it translated.

30
00:02:02,000 --> 00:02:09,000
 It's only so long, I think, 20, 30 pages long.

31
00:02:09,000 --> 00:02:12,600
 So if you have the time and the expertise and feel

32
00:02:12,600 --> 00:02:17,240
 comfortable translating English into your own language,

33
00:02:17,240 --> 00:02:19,000
 please check out the link in the description.

34
00:02:19,000 --> 00:02:22,280
 If you haven't already read the booklet, if you have and

35
00:02:22,280 --> 00:02:26,000
 would like to translate it, please just send me an email

36
00:02:26,000 --> 00:02:27,000
 and let me know.

37
00:02:27,000 --> 00:02:30,140
 And I'll confirm that whether or not someone has already

38
00:02:30,140 --> 00:02:32,000
 started that language or not.

39
00:02:32,000 --> 00:02:38,740
 The second announcement is that I'm tentatively planning to

40
00:02:38,740 --> 00:02:40,000
 make a trip.

41
00:02:40,000 --> 00:02:44,010
 This will be only interesting for certain people down the

42
00:02:44,010 --> 00:02:47,000
 East Coast of the United States in June.

43
00:02:47,000 --> 00:02:56,230
 So June 2014, if you're somewhere between, roughly between

44
00:02:56,230 --> 00:03:02,460
 New York and Florida and would like to host me and a couple

45
00:03:02,460 --> 00:03:06,000
 other people to give teachings

46
00:03:06,000 --> 00:03:11,990
 or to just meet up and talk about meditation on the way,

47
00:03:11,990 --> 00:03:16,000
 please let me know because we'll be taking a Greyhound bus.

48
00:03:16,000 --> 00:03:20,320
 Down the coast, someone's already talked about us going to

49
00:03:20,320 --> 00:03:23,240
 Vermont, so we might head up there first and then head down

50
00:03:23,240 --> 00:03:27,000
 to New York and New Jersey and wherever,

51
00:03:27,000 --> 00:03:31,180
 anywhere along that area, we'll be setting up tents in

52
00:03:31,180 --> 00:03:35,210
 people's backyards, me and at least one other person, maybe

53
00:03:35,210 --> 00:03:36,000
 a couple more.

54
00:03:36,000 --> 00:03:40,840
 And hopefully doing a little bit of spreading of the Dhamma

55
00:03:40,840 --> 00:03:45,280
 at the time, we'll try to keep some kind of an active

56
00:03:45,280 --> 00:03:47,000
 internet presence during the trip

57
00:03:47,000 --> 00:03:50,350
 so people know where we are and it can kind of be an

58
00:03:50,350 --> 00:03:59,000
 interactive, live sort of Dhamma tour.

59
00:03:59,000 --> 00:04:02,550
 And there's a website for that, or I'm using an old website

60
00:04:02,550 --> 00:04:06,610
 that we'd used for something similar at jarika.syrimongalo.

61
00:04:06,610 --> 00:04:07,000
org.

62
00:04:07,000 --> 00:04:10,000
 I'll put the link as well in the description if I can.

63
00:04:10,000 --> 00:04:14,250
 And you can use, there's not much there yet because I'm not

64
00:04:14,250 --> 00:04:19,000
 at a computer right now, but when I get back to Canada,

65
00:04:19,000 --> 00:04:20,610
 I will try to put more information and kind of solidify

66
00:04:20,610 --> 00:04:21,000
 things.

67
00:04:21,000 --> 00:04:24,440
 But if you're interested and if you're in that area, please

68
00:04:24,440 --> 00:04:27,760
 let me know and there's a contact form there that you can

69
00:04:27,760 --> 00:04:28,000
 use,

70
00:04:28,000 --> 00:04:30,950
 or if you have my email or have some other way of getting

71
00:04:30,950 --> 00:04:34,000
 in touch with me via Facebook or Google or whatever.

72
00:04:34,000 --> 00:04:38,690
 And let us know because we'll try to quickly put out the

73
00:04:38,690 --> 00:04:43,200
 announcement for people who are in the various areas if

74
00:04:43,200 --> 00:04:45,000
 they want to join up

75
00:04:45,000 --> 00:04:49,790
 and meet up and listen to the Dhamma or talk about the Dham

76
00:04:49,790 --> 00:04:51,000
ma or so on.

77
00:04:51,000 --> 00:04:57,700
 So that's all. I have those two announcements to give and

78
00:04:57,700 --> 00:05:02,300
 hopefully these things will come to fruition and there will

79
00:05:02,300 --> 00:05:03,000
 be more in the future.

80
00:05:03,000 --> 00:05:08,840
 That's all. I'm well. I'm here in Bangkok. Just had my eyes

81
00:05:08,840 --> 00:05:10,000
 fixed.

82
00:05:10,000 --> 00:05:14,680
 So I won't be wearing glasses anymore, I don't think, not

83
00:05:14,680 --> 00:05:17,000
 until I go blind from old age.

84
00:05:17,000 --> 00:05:21,930
 I've been doing a little bit of teaching here in Bangkok

85
00:05:21,930 --> 00:05:25,660
 yesterday and the day before I was giving talks in the

86
00:05:25,660 --> 00:05:27,000
 downtown area.

87
00:05:27,000 --> 00:05:32,300
 And today I'll give the third talk. Then next Sunday, this

88
00:05:32,300 --> 00:05:37,270
 Sunday, I'll be giving a one-day course somewhere near the

89
00:05:37,270 --> 00:05:40,000
 Vik, the Democracy Monument.

90
00:05:40,000 --> 00:05:46,590
 Otherwise, just hanging out and getting ready. I've also

91
00:05:46,590 --> 00:05:49,000
 started doing the Pali Studies in Thai.

92
00:05:49,000 --> 00:05:52,870
 So that's going to be taking up a bunch of my time for the

93
00:05:52,870 --> 00:05:57,400
 next two years while I take these two exams to try and get

94
00:05:57,400 --> 00:06:01,000
 certified as a Thai Pali scholar.

95
00:06:01,000 --> 00:06:09,390
 Which should be helpful for recognition purposes. People

96
00:06:09,390 --> 00:06:12,000
 won't be able to say that I don't know my Pali.

97
00:06:12,000 --> 00:06:14,650
 Lots of things. So lots of things going on. I have many

98
00:06:14,650 --> 00:06:16,000
 projects on the go.

99
00:06:16,000 --> 00:06:19,440
 So you can keep in touch here. On YouTube, keep up by

100
00:06:19,440 --> 00:06:21,000
 subscribing to my channel.

101
00:06:21,000 --> 00:06:25,030
 I'll try to, as I said, maybe post updates every so often

102
00:06:25,030 --> 00:06:27,000
 along with my regular videos.

103
00:06:27,000 --> 00:06:31,550
 Or follow my web blog or Facebook or Google Plus. I'm

104
00:06:31,550 --> 00:06:36,080
 pretty active these days. At least posting, if not checking

105
00:06:36,080 --> 00:06:37,000
 responses.

106
00:06:37,000 --> 00:06:40,210
 So thanks for keeping up. It's good to see we have over 4

107
00:06:40,210 --> 00:06:44,180
 million views, I think, on YouTube now. 25,000 subscribers

108
00:06:44,180 --> 00:06:45,000
 is great.

109
00:06:45,000 --> 00:06:48,840
 Pretty cool. So thanks for all your interest and keep

110
00:06:48,840 --> 00:06:51,000
 practicing. All the best.

