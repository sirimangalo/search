1
00:00:00,000 --> 00:00:04,270
 Bhante, for those of us who live in areas that have never

2
00:00:04,270 --> 00:00:06,920
 had a Buddhist monastic presence,

3
00:00:06,920 --> 00:00:09,770
 what is important to know when meeting, greeting,

4
00:00:09,770 --> 00:00:12,200
 interacting with a bhikkhu or bhikkhuni?

5
00:00:12,200 --> 00:00:16,710
 Also, what is meant when people say sad, who is sad, who is

6
00:00:16,710 --> 00:00:17,160
 sad?

7
00:00:17,160 --> 00:00:19,080
 Easy question.

8
00:00:19,080 --> 00:00:23,480
 Okay, how to deal with bhikkhus, bhikkhunis?

9
00:00:23,480 --> 00:00:24,480
 Don't call them bhikkhu.

10
00:00:24,480 --> 00:00:27,200
 I've had people call me bhikkhu.

11
00:00:27,200 --> 00:00:32,200
 You can call them what you like, but it's like saying monk.

12
00:00:32,200 --> 00:00:34,720
 Excuse me monk, don't say that.

13
00:00:34,720 --> 00:00:37,640
 Well, it's such an etiquette.

14
00:00:37,640 --> 00:00:42,490
 It's not that they'll be necessarily offended, but there's

15
00:00:42,490 --> 00:00:44,200
 etiquette for you.

16
00:00:44,200 --> 00:00:50,480
 Just as you wouldn't say, hey monk, don't say, hey bhikkhu.

17
00:00:50,480 --> 00:00:58,200
 You can call them bhante or ayay.

18
00:00:58,200 --> 00:01:02,000
 Actually, bhante would be more proper, but they don't seem

19
00:01:02,000 --> 00:01:04,200
 to use that, or they do actually,

20
00:01:04,200 --> 00:01:06,200
 but nowadays they don't.

21
00:01:06,200 --> 00:01:11,160
 In the texts, it seems like bhante and bhante.

22
00:01:11,160 --> 00:01:17,780
 Bhante was the female, but the bhikkkhuni seem to prefer ay

23
00:01:17,780 --> 00:01:19,360
ah or ayay.

24
00:01:19,360 --> 00:01:24,640
 I would say ayah is probably more common.

25
00:01:24,640 --> 00:01:29,040
 So find out, venerable also work.

26
00:01:29,040 --> 00:01:32,030
 Just like you would call a priest/father, even if you're

27
00:01:32,030 --> 00:01:33,400
 not particularly religious

28
00:01:33,400 --> 00:01:37,790
 out of respect and deference, you call a doctor/doctor, you

29
00:01:37,790 --> 00:01:40,240
 call a priest/father, you call a monk

30
00:01:40,240 --> 00:01:43,760
 venerable or something like that.

31
00:01:43,760 --> 00:01:46,590
 Even if you don't respect them, it's a courtesy, it's

32
00:01:46,590 --> 00:01:47,440
 etiquette.

33
00:01:47,440 --> 00:01:49,760
 So that's something.

34
00:01:49,760 --> 00:01:52,040
 Don't try to get too close or friendly to them.

35
00:01:52,040 --> 00:01:55,690
 The reason we've ordained is to get away, is to be in

36
00:01:55,690 --> 00:01:57,920
 solitude, to have the time to

37
00:01:57,920 --> 00:01:58,920
 ourselves.

38
00:01:58,920 --> 00:02:03,280
 So trying to be buddy-buddy with a monk and thinking of

39
00:02:03,280 --> 00:02:05,960
 yourself as buddies or something

40
00:02:05,960 --> 00:02:06,960
 like that.

41
00:02:06,960 --> 00:02:10,690
 If you're not a monk, it's like an ordinary person and a

42
00:02:10,690 --> 00:02:11,600
 soldier.

43
00:02:11,600 --> 00:02:17,020
 The soldier has to remain aloof and has to have some se

44
00:02:17,020 --> 00:02:18,280
clusion.

45
00:02:18,280 --> 00:02:19,280
 There is a difference.

46
00:02:19,280 --> 00:02:21,430
 I mean people don't realize that because they haven't

47
00:02:21,430 --> 00:02:22,960
 experienced the life, but we have

48
00:02:22,960 --> 00:02:28,040
 a very strict life with lots of rules.

49
00:02:28,040 --> 00:02:31,640
 So it can be harmful for the monk and it can be a source of

50
00:02:31,640 --> 00:02:33,680
 misunderstanding for the lay

51
00:02:33,680 --> 00:02:40,020
 person when the monk is, as a result of their interaction,

52
00:02:40,020 --> 00:02:43,080
 they might break rules and as

53
00:02:43,080 --> 00:02:46,510
 a result of them refusing to break rules, the lay person

54
00:02:46,510 --> 00:02:48,360
 might become upset as a cause

55
00:02:48,360 --> 00:02:53,490
 for misunderstanding when it seems like the monk is acting

56
00:02:53,490 --> 00:02:56,800
 a bit standoffish or whatever.

57
00:02:56,800 --> 00:02:58,990
 Actually it's usually just trying to keep the rules and

58
00:02:58,990 --> 00:03:00,520
 concerned that it's very difficult

59
00:03:00,520 --> 00:03:11,280
 to keep the rules with these people so close to me.

60
00:03:11,280 --> 00:03:13,480
 So yeah, that's about it.

61
00:03:13,480 --> 00:03:18,840
 Men shouldn't touch bhikkhunis, women shouldn't touch bhikk

62
00:03:18,840 --> 00:03:19,440
hus.

63
00:03:19,440 --> 00:03:21,160
 And you shouldn't really touch them in general.

64
00:03:21,160 --> 00:03:24,360
 Touching is not a good thing.

65
00:03:24,360 --> 00:03:27,560
 We're not touchy-feely sort of people.

66
00:03:27,560 --> 00:03:28,560
 Monks.

67
00:03:28,560 --> 00:03:29,640
 Give them some space.

68
00:03:29,640 --> 00:03:33,120
 Give them their own space.

69
00:03:33,120 --> 00:03:36,000
 Don't be afraid of them and don't be afraid to ask.

70
00:03:36,000 --> 00:03:39,120
 If you're not sure about something, ask, "Is this

71
00:03:39,120 --> 00:03:40,240
 appropriate?

72
00:03:40,240 --> 00:03:42,520
 Is this inappropriate?"

73
00:03:42,520 --> 00:03:46,150
 Most bhikkhus and bhikkkhunis like to have people ask

74
00:03:46,150 --> 00:03:47,120
 questions.

75
00:03:47,120 --> 00:03:48,560
 It's not really a greed thing.

76
00:03:48,560 --> 00:03:54,090
 It's just they're bursting with knowledge and practice and

77
00:03:54,090 --> 00:03:57,160
 quite willing to answer people's

78
00:03:57,160 --> 00:03:58,160
 questions.

79
00:03:58,160 --> 00:03:59,920
 Ask them lots of questions.

80
00:03:59,920 --> 00:04:00,920
 It's a good thing.

81
00:04:00,920 --> 00:04:03,220
 It's a good way to disfigure if they're a real monk or a

82
00:04:03,220 --> 00:04:04,760
 real bhikkhu or bhikkhuni because

83
00:04:04,760 --> 00:04:10,680
 if they get angry at you, then you know what they're like.

84
00:04:10,680 --> 00:04:14,290
 If they brush you off, if they give you silly answers, if a

85
00:04:14,290 --> 00:04:15,940
 monk is very buddy-buddy with

86
00:04:15,940 --> 00:04:21,170
 you then you should know that they are not really dedicated

87
00:04:21,170 --> 00:04:23,120
 to their practice.

88
00:04:23,120 --> 00:04:29,120
 They just want to tell jokes and so on, this kind of thing.

89
00:04:29,120 --> 00:04:38,680
 Engage in useless things.

90
00:04:38,680 --> 00:04:39,680
 What does sadhu?

91
00:04:39,680 --> 00:04:44,160
 Sadhu means it's good.

92
00:04:44,160 --> 00:04:46,360
 It's an expression of appreciation.

93
00:04:46,360 --> 00:04:48,980
 When you appreciate something someone has done, when you

94
00:04:48,980 --> 00:04:50,240
 appreciate something someone

95
00:04:50,240 --> 00:04:55,550
 has said or when you appreciate a good deed, this is what

96
00:04:55,550 --> 00:04:56,560
 we say.

97
00:04:56,560 --> 00:04:57,560
 We say sad.

98
00:04:57,560 --> 00:04:59,280
 We say it three times.

