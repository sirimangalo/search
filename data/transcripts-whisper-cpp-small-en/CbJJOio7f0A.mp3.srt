1
00:00:00,000 --> 00:00:08,720
 Good evening everyone.

2
00:00:08,720 --> 00:00:10,720
 Happy Maga Bhuja.

3
00:00:10,720 --> 00:00:12,720
 Manga Punami.

4
00:00:12,720 --> 00:00:19,000
 In Thailand they call it Maga Puja.

5
00:00:19,000 --> 00:00:24,560
 Puja is paying respect or homage.

6
00:00:24,560 --> 00:00:36,760
 Maga because it's the full moon of Maga, the month of Maga.

7
00:00:36,760 --> 00:00:42,160
 So what's important about this full moon you ask?

8
00:00:42,160 --> 00:00:49,290
 Well tradition has it that on the full moon of Maga the

9
00:00:49,290 --> 00:00:51,720
 Munda taught what is now known

10
00:00:51,720 --> 00:01:01,400
 as the Awadapati Moka.

11
00:01:01,400 --> 00:01:10,840
 It's sort of a summary of Buddhism.

12
00:01:10,840 --> 00:01:13,960
 It's well known.

13
00:01:13,960 --> 00:01:24,780
 And so the story behind it is there was a group of monks

14
00:01:24,780 --> 00:01:29,360
 who were meditating and it

15
00:01:29,360 --> 00:01:35,450
 may have been the fire ascetics, the fire worshipping asc

16
00:01:35,450 --> 00:01:38,600
etics in Rajagaha because there

17
00:01:38,600 --> 00:01:52,500
 were 1,250 of them which coincides with the number of fire

18
00:01:52,500 --> 00:01:57,520
 worshippers there were.

19
00:01:57,520 --> 00:02:01,680
 So they were meditating on their own and one of them was

20
00:02:01,680 --> 00:02:04,520
 practicing strenuously and became

21
00:02:04,520 --> 00:02:10,220
 an Arhat and he realized that he had attained the goal and

22
00:02:10,220 --> 00:02:13,000
 so he went to see the Buddha

23
00:02:13,000 --> 00:02:15,120
 where the Buddha was sitting.

24
00:02:15,120 --> 00:02:17,860
 But when he got to the Buddha he looked and saw there was

25
00:02:17,860 --> 00:02:19,520
 another monk coming and he said

26
00:02:19,520 --> 00:02:26,310
 well I'll wait until this monk sits down and the other monk

27
00:02:26,310 --> 00:02:28,520
 came and sat down and the

28
00:02:28,520 --> 00:02:34,760
 other monk came up and turned and saw another monk coming.

29
00:02:34,760 --> 00:02:37,970
 And so he decided he wouldn't talk to the Buddha he'd wait

30
00:02:37,970 --> 00:02:40,280
 for the third monk to come

31
00:02:40,280 --> 00:02:41,600
 and so he sat down.

32
00:02:41,600 --> 00:02:45,580
 The third monk came and then a fourth monk came and one by

33
00:02:45,580 --> 00:02:47,560
 one all the monks came together

34
00:02:47,560 --> 00:02:56,390
 until there was 1,250 monks sitting there all who had just

35
00:02:56,390 --> 00:02:59,640
 attained Arhat and then the Buddha

36
00:02:59,640 --> 00:03:01,080
 taught the Ovada Bhatimoka.

37
00:03:01,080 --> 00:03:11,040
 That's the tradition that's what they say happened.

38
00:03:11,040 --> 00:03:19,000
 And we're going to look up here.

39
00:03:19,000 --> 00:03:38,720
 Look up the Pali.

40
00:03:38,720 --> 00:03:43,720
 So it starts

41
00:03:43,720 --> 00:04:00,720
 with

42
00:04:00,720 --> 00:04:22,720
 the

43
00:04:22,720 --> 00:04:34,200
 three verses or three stanzas and it's in the Dighanikaya

44
00:04:34,200 --> 00:04:46,960
 it's also in the Dhammapada.

45
00:04:46,960 --> 00:04:55,590
 It's one of those really well condensed summaries of the

46
00:04:55,590 --> 00:04:57,400
 Buddha's teaching.

47
00:04:57,400 --> 00:05:05,920
 We always summarize it with the second verse.

48
00:05:05,920 --> 00:05:11,560
 This is the Buddha and the assassin and the teaching of the

49
00:05:11,560 --> 00:05:13,680
 Buddha's plural.

50
00:05:13,680 --> 00:05:19,880
 All Buddhas teach not doing any evil.

51
00:05:19,880 --> 00:05:32,600
 And the purification of one's own mind.

52
00:05:32,600 --> 00:05:37,880
 So not doing evil and doing good and purifying one's mind.

53
00:05:37,880 --> 00:05:43,040
 This is the teaching of all the Buddhas.

54
00:05:43,040 --> 00:05:45,530
 I think in the Thai version that verse is the first of the

55
00:05:45,530 --> 00:05:46,040
 three.

56
00:05:46,040 --> 00:05:49,040
 I can't remember though.

57
00:05:49,040 --> 00:05:53,330
 Anyway, so the first it starts Kanti Brahmungta Bodhicika

58
00:05:53,330 --> 00:05:56,200
 which is also very important teaching.

59
00:05:56,200 --> 00:05:59,760
 Patience is the highest form of austerity.

60
00:05:59,760 --> 00:06:03,940
 It's in the time of the Buddha ascetics were big into tort

61
00:06:03,940 --> 00:06:05,720
uring themselves.

62
00:06:05,720 --> 00:06:09,400
 So they said those kind of torture are not really useful.

63
00:06:09,400 --> 00:06:11,040
 What's the best kind of torture?

64
00:06:11,040 --> 00:06:14,160
 The most the highest form of asceticism?

65
00:06:14,160 --> 00:06:15,160
 Patience.

66
00:06:15,160 --> 00:06:19,950
 You know bearing with not only unpleasant feelings but also

67
00:06:19,950 --> 00:06:21,840
 pleasant feelings.

68
00:06:21,840 --> 00:06:28,980
 So not reacting to desires or not reacting to appealing

69
00:06:28,980 --> 00:06:33,320
 experiences and not acting out

70
00:06:33,320 --> 00:06:37,720
 in regards to unappealing experiences.

71
00:06:37,720 --> 00:06:45,320
 Nimbanang Paramangvadanti Buddha.

72
00:06:45,320 --> 00:06:48,360
 Nibbana is the highest.

73
00:06:48,360 --> 00:06:55,230
 There's nothing in samsara that can compare to nibbana, nir

74
00:06:55,230 --> 00:06:56,280
vana.

75
00:06:56,280 --> 00:07:00,280
 Nahepa bhajito parupa ghati.

76
00:07:00,280 --> 00:07:03,320
 One is not a mendicant.

77
00:07:03,320 --> 00:07:08,710
 I don't know. Papaji, Papaji is one who has left the home

78
00:07:08,710 --> 00:07:08,840
 life.

79
00:07:08,840 --> 00:07:13,620
 When it's not a prop, when it's not properly left the home

80
00:07:13,620 --> 00:07:16,320
 life who attacks others, who

81
00:07:16,320 --> 00:07:21,480
 is violent towards others, mean towards others.

82
00:07:21,480 --> 00:07:25,200
 Na samano ho di parang we hae dai yanto.

83
00:07:25,200 --> 00:07:29,720
 When it's not a reckless who we hate dai yanto.

84
00:07:29,720 --> 00:07:33,520
 I can't remember it. Something like who attacks others or

85
00:07:33,520 --> 00:07:41,840
 hurts others or scolds, abuses others.

86
00:07:41,840 --> 00:07:44,640
 Anupavadho, anupaghato.

87
00:07:44,640 --> 00:07:46,640
 Not scolding others.

88
00:07:46,640 --> 00:07:53,000
 Anupavada means not speaking harshly towards others.

89
00:07:53,000 --> 00:07:58,710
 Anupavaghato means not acting harshly towards others or not

90
00:07:58,710 --> 00:08:00,600
 hurting others.

91
00:08:00,600 --> 00:08:05,000
 Paatimoke jasangwaro.

92
00:08:05,000 --> 00:08:11,360
 Being restrained by the paatimoka, by a code of conduct you

93
00:08:11,360 --> 00:08:13,600
 could say.

94
00:08:13,600 --> 00:08:22,320
 Matanyutta jabhata swim. Knowing moderation in regards to

95
00:08:22,320 --> 00:08:23,600
 food.

96
00:08:23,600 --> 00:08:27,600
 Pantan ca seyanasanang.

97
00:08:27,600 --> 00:08:32,600
 Having a secluded dwelling.

98
00:08:32,600 --> 00:08:34,600
 Dwelling in seclusion.

99
00:08:34,600 --> 00:08:42,600
 Adi ji te jaya yoga being intent upon adi jita.

100
00:08:42,600 --> 00:08:44,600
 Higher jita mind, the higher mind.

101
00:08:44,600 --> 00:08:47,600
 So this is meditation, meditative state.

102
00:08:47,600 --> 00:08:52,600
 It's being fixed on it, focused on it.

103
00:08:52,600 --> 00:08:54,600
 E tangbundan saasanam.

104
00:08:54,600 --> 00:08:56,600
 This is the teaching of all the buddhas.

105
00:08:56,600 --> 00:09:00,620
 So Maga Buddha is normally in Thailand, not in Sri Lanka I

106
00:09:00,620 --> 00:09:01,600
 don't think.

107
00:09:01,600 --> 00:09:05,600
 I'm pretty sure they don't know about it in Sri Lanka.

108
00:09:05,600 --> 00:09:11,110
 It's a very important holiday in Thailand, though, through

109
00:09:11,110 --> 00:09:14,600
 Thai Buddhism.

110
00:09:14,600 --> 00:09:16,600
 That's just an excuse to celebrate, I suppose.

111
00:09:16,600 --> 00:09:23,330
 An excuse to meditate, an excuse to have some kind of

112
00:09:23,330 --> 00:09:26,600
 religious activity.

113
00:09:26,600 --> 00:09:31,600
 So happy Maga Buddha everyone.

114
00:09:32,600 --> 00:09:35,600
 Thank you.

115
00:09:36,600 --> 00:09:39,600
 Thank you.

116
00:09:41,600 --> 00:09:43,600
 Thank you.

117
00:10:07,600 --> 00:10:10,600
 We also have a quote today.

118
00:10:10,600 --> 00:10:13,600
 I don't think I'll go into it.

119
00:10:13,600 --> 00:10:22,070
 Maybe I'll post the hangout if anybody wants, if anyone has

120
00:10:22,070 --> 00:10:25,600
 any questions they want to come on.

121
00:10:25,600 --> 00:10:29,380
 Otherwise, I have a midterm in a couple of days on Buddhism

122
00:10:29,380 --> 00:10:29,600
.

123
00:10:29,600 --> 00:10:32,600
 We're studying Mahayana Buddhism.

124
00:10:32,600 --> 00:10:37,600
 So I have to go for that as well.

125
00:10:37,600 --> 00:10:42,400
 Here's the hangout link if anyone wants to come on and ask

126
00:10:42,400 --> 00:10:43,600
 a question.

127
00:10:44,600 --> 00:10:46,600
 Thank you.

128
00:10:48,600 --> 00:10:50,600
 Thank you.

129
00:11:14,600 --> 00:11:17,600
 I don't even know if I have a sound.

130
00:11:17,600 --> 00:11:19,600
 Hello, Bante.

131
00:11:19,600 --> 00:11:22,600
 Aren't you coming here today?

132
00:11:22,600 --> 00:11:24,600
 Aren't you coming to...

133
00:11:24,600 --> 00:11:27,600
 April 4th.

134
00:11:27,600 --> 00:11:30,600
 That's why I'm not there yet.

135
00:11:30,600 --> 00:11:33,600
 We have like four toms coming to meditate.

136
00:11:33,600 --> 00:11:37,600
 I think actually four toms, not like four toms.

137
00:11:37,600 --> 00:11:39,600
 Chinese, Europe, and Tom, of course.

138
00:11:39,600 --> 00:11:41,600
 Right.

139
00:11:42,600 --> 00:11:44,600
 I do have a question.

140
00:11:44,600 --> 00:11:46,600
 Go ahead.

141
00:11:46,600 --> 00:11:53,600
 What's your question?

142
00:11:53,600 --> 00:11:56,600
 My question is, I've been thinking...

143
00:11:56,600 --> 00:12:00,920
 You mentioned last night, I believe it was, about you have

144
00:12:00,920 --> 00:12:01,600
 the meditators there.

145
00:12:01,600 --> 00:12:05,160
 You stick them in a room, so to speak, and they're med

146
00:12:05,160 --> 00:12:05,600
itating.

147
00:12:05,600 --> 00:12:10,740
 I've heard you mention it any number of times about no more

148
00:12:10,740 --> 00:12:13,600
 than six hours of sleep in a 24-hour period.

149
00:12:13,600 --> 00:12:16,600
 So I'm curious.

150
00:12:16,600 --> 00:12:21,620
 I notice many things since I've been seriously meditating

151
00:12:21,620 --> 00:12:23,600
 for a number of months.

152
00:12:23,600 --> 00:12:26,600
 Transformations, I would say.

153
00:12:26,600 --> 00:12:30,920
 I'm curious about the possibility that you can actually

154
00:12:30,920 --> 00:12:33,600
 replace sleep with meditation.

155
00:12:33,600 --> 00:12:39,600
 Is that something that's ever done by serious meditators?

156
00:12:39,600 --> 00:12:44,530
 Were you really not doing much actual sleep but meditating

157
00:12:44,530 --> 00:12:45,600
 instead?

158
00:12:45,600 --> 00:12:48,600
 Oh yeah. It's quite funny.

159
00:12:48,600 --> 00:12:50,600
 Okay.

160
00:12:50,600 --> 00:12:59,590
 In the text, there are monks who would not sleep for months

161
00:12:59,590 --> 00:12:59,600
.

162
00:12:59,600 --> 00:13:02,600
 I got the first story of the Dhammapada.

163
00:13:02,600 --> 00:13:10,600
 Chakupala spent three months not lying down.

164
00:13:10,600 --> 00:13:13,600
 With no ill effect?

165
00:13:13,600 --> 00:13:18,600
 He lost his eyesight. He went blind.

166
00:13:18,600 --> 00:13:21,600
 But it didn't have a deal with not lying...

167
00:13:21,600 --> 00:13:23,600
 Well, it wasn't exactly because he didn't lie down.

168
00:13:23,600 --> 00:13:27,140
 It was some condition of the eyes, and he had to lie down

169
00:13:27,140 --> 00:13:29,600
 in order to cure it, in order to get better.

170
00:13:29,600 --> 00:13:34,600
 And he decided not to.

171
00:13:34,600 --> 00:13:37,340
 When you don't sleep, one of the things that happens is you

172
00:13:37,340 --> 00:13:38,600
 start to hallucinate.

173
00:13:38,600 --> 00:13:43,600
 You start to lose some sense of balance.

174
00:13:43,600 --> 00:13:47,600
 The effects, if you look on...

175
00:13:47,600 --> 00:13:50,600
 If you look on the internet...

176
00:13:50,600 --> 00:13:53,600
 What happened to my voice?

177
00:13:53,600 --> 00:13:59,200
 If you look on the internet, the effects of not sleeping

178
00:13:59,200 --> 00:14:00,600
 are...

179
00:14:00,600 --> 00:14:04,810
 There are effects of not sleeping that they'll tell you

180
00:14:04,810 --> 00:14:05,600
 about.

181
00:14:05,600 --> 00:14:11,600
 I want just to say that none of those effects are

182
00:14:11,600 --> 00:14:15,600
 necessarily deleterious or problematic.

183
00:14:15,600 --> 00:14:18,650
 And that being said, once you really get into the

184
00:14:18,650 --> 00:14:20,600
 meditation, if you get into the groove,

185
00:14:20,600 --> 00:14:26,770
 you can sustain it for at least several days, on little to

186
00:14:26,770 --> 00:14:28,600
 no sleep.

187
00:14:28,600 --> 00:14:33,140
 Most people can do that. To do it for months, you'd need to

188
00:14:33,140 --> 00:14:33,600
...

189
00:14:33,600 --> 00:14:37,600
 I think you'd need to prepare. You'd have to leave society.

190
00:14:37,600 --> 00:14:42,270
 Because the mind is so caught up in so much information and

191
00:14:42,270 --> 00:14:43,600
 stimuli

192
00:14:43,600 --> 00:14:48,600
 that it just works too hard to be without sleep.

193
00:14:48,600 --> 00:14:52,050
 But if you're living in the forest for some time, your mind

194
00:14:52,050 --> 00:14:52,600
 starts to work,

195
00:14:52,600 --> 00:14:56,980
 not have to work so hard. It becomes relaxed, habitually

196
00:14:56,980 --> 00:14:57,600
 relaxed.

197
00:14:57,600 --> 00:15:05,000
 And so you can spend your days without building up the need

198
00:15:05,000 --> 00:15:05,600
 to sleep,

199
00:15:05,600 --> 00:15:07,600
 without tiring yourself out.

200
00:15:07,600 --> 00:15:10,600
 And then when you don't sleep, your mind is already calm.

201
00:15:10,600 --> 00:15:13,770
 So the first night or two when you stop sleeping, you

202
00:15:13,770 --> 00:15:14,600
 hallucinate.

203
00:15:14,600 --> 00:15:20,790
 You'll see things in the floor walking around. You'll feel

204
00:15:20,790 --> 00:15:22,600
 dizzy.

205
00:15:22,600 --> 00:15:26,650
 And you get that overtired feeling. But none of that is

206
00:15:26,650 --> 00:15:28,600
 really all that deleterious.

207
00:15:28,600 --> 00:15:32,320
 I mean, they'll say things like, "We repair our body cells

208
00:15:32,320 --> 00:15:33,600
 when we sleep."

209
00:15:33,600 --> 00:15:37,090
 I don't know how that works or whether there's proof as to

210
00:15:37,090 --> 00:15:38,600
 the need for it

211
00:15:38,600 --> 00:15:42,160
 or whether you can show that through meditation you're

212
00:15:42,160 --> 00:15:43,600
 doing some kind of cell rebuilding.

213
00:15:43,600 --> 00:15:45,600
 I don't really understand that part of it.

214
00:15:45,600 --> 00:15:50,510
 But I know it's been done and it is done. It is possible,

215
00:15:50,510 --> 00:15:52,600
 very possible.

216
00:15:52,600 --> 00:15:58,600
 Well, if I just... sleep is important for me.

217
00:15:58,600 --> 00:16:03,600
 And quite frankly, I got back into my meditation practice

218
00:16:03,600 --> 00:16:06,600
 because I wanted to cure myself of insomnia.

219
00:16:06,600 --> 00:16:09,600
 And that's pretty much taken care of now.

220
00:16:09,600 --> 00:16:12,600
 But now when I wake up in the middle of the night,

221
00:16:12,600 --> 00:16:16,150
 and I always meditate when I wake up in the middle of the

222
00:16:16,150 --> 00:16:16,600
 night,

223
00:16:16,600 --> 00:16:23,540
 I want to release the anxiety of the fear of not being able

224
00:16:23,540 --> 00:16:24,600
 to go back to sleep.

225
00:16:24,600 --> 00:16:29,970
 And if I understand that the meditation takes over for the

226
00:16:29,970 --> 00:16:30,600
 sleep,

227
00:16:30,600 --> 00:16:34,600
 then I was hoping for that answer.

228
00:16:34,600 --> 00:16:38,180
 Yeah, absolutely. I mean, that's a big part of getting over

229
00:16:38,180 --> 00:16:38,600
 insomnia.

230
00:16:38,600 --> 00:16:40,600
 Like I did this once.

231
00:16:40,600 --> 00:16:45,600
 Besides, of course, I've done many days without sleep.

232
00:16:45,600 --> 00:16:50,600
 But one time, it wasn't even intentional.

233
00:16:50,600 --> 00:16:55,600
 I had been on a flight from Thailand to Los Angeles,

234
00:16:55,600 --> 00:16:59,600
 and then I got in in the evening and they gave me a coffee.

235
00:16:59,600 --> 00:17:02,190
 Someone gave me a coffee to drink. I didn't even think

236
00:17:02,190 --> 00:17:02,600
 about it.

237
00:17:02,600 --> 00:17:07,600
 I just drank the coffee. Back then, I was less --

238
00:17:07,600 --> 00:17:10,600
 actually, I got quite less strict in the rules as well.

239
00:17:10,600 --> 00:17:13,600
 And now I wouldn't really have coffee in the evening.

240
00:17:13,600 --> 00:17:16,600
 So I had this coffee, and then I realized,

241
00:17:16,600 --> 00:17:18,600
 what am I doing drinking coffee in the evening?

242
00:17:18,600 --> 00:17:22,600
 Because that was just what they were offering.

243
00:17:22,600 --> 00:17:24,600
 But I ended up not being able to sleep at all.

244
00:17:24,600 --> 00:17:26,410
 There was no way I was going to sleep because of the jet

245
00:17:26,410 --> 00:17:28,600
 lag, the time change.

246
00:17:28,600 --> 00:17:32,830
 And so I just lay in bed all night, being mindful, med

247
00:17:32,830 --> 00:17:33,600
itating.

248
00:17:33,600 --> 00:17:35,600
 And when I got up in the morning, I really felt fine.

249
00:17:35,600 --> 00:17:38,600
 I hadn't slept a wink that I remember.

250
00:17:38,600 --> 00:17:45,600
 What I remember of that night is I was up all night.

251
00:17:45,600 --> 00:17:52,450
 But because of being mindful, in the morning there was no

252
00:17:52,450 --> 00:17:54,600
 ill effect.

253
00:17:54,600 --> 00:17:58,600
 Well, thank you for that.

254
00:17:58,600 --> 00:18:04,600
 Yeah, absolutely, to some extent you can replace.

255
00:18:04,600 --> 00:18:06,600
 I don't want to say, because I know people are going to say

256
00:18:06,600 --> 00:18:06,600
,

257
00:18:06,600 --> 00:18:12,060
 "Oh, no, science has evidence to the contrary, but whatever

258
00:18:12,060 --> 00:18:12,600
."

259
00:18:12,600 --> 00:18:15,600
 I'm skeptical of such evidence.

260
00:18:15,600 --> 00:18:18,600
 Because such evidence is always taken from people who need

261
00:18:18,600 --> 00:18:18,600
 sleep,

262
00:18:18,600 --> 00:18:21,600
 because their minds are not focused.

263
00:18:21,600 --> 00:18:25,600
 If we need to do a study on meditators who are going

264
00:18:25,600 --> 00:18:26,600
 without sleep,

265
00:18:26,600 --> 00:18:29,240
 and to see the effects, and to see if there was any

266
00:18:29,240 --> 00:18:29,600
 difference

267
00:18:29,600 --> 00:18:37,600
 between them and people who weren't meditating.

268
00:18:37,600 --> 00:18:41,600
 But it doesn't take a genius to realize the difference

269
00:18:41,600 --> 00:18:47,600
 when the mind is calm, when the mind is focused,

270
00:18:47,600 --> 00:18:54,600
 when the mind is clear.

271
00:18:54,600 --> 00:18:57,600
 I have a question, Bante.

272
00:18:57,600 --> 00:19:01,600
 We were reading today, because we were discussing,

273
00:19:01,600 --> 00:19:05,600
 we were in Second Life, how the Buddhist enlightenment

274
00:19:05,600 --> 00:19:09,600
 is distinguished from Arahant's enlightenment.

275
00:19:09,600 --> 00:19:13,600
 So we read Pekubori's translation.

276
00:19:13,600 --> 00:19:17,600
 And then I was thinking, in the teachings of the Buddha,

277
00:19:17,600 --> 00:19:21,600
 it is often said that if you come and practice,

278
00:19:21,600 --> 00:19:23,600
 you are going to see for yourself.

279
00:19:23,600 --> 00:19:26,600
 And we were kind of getting on the topic of,

280
00:19:26,600 --> 00:19:29,600
 if there might be enlightened beings,

281
00:19:29,600 --> 00:19:31,900
 and if it is still possible for beings to become

282
00:19:31,900 --> 00:19:32,600
 enlightened,

283
00:19:32,600 --> 00:19:35,600
 while the Buddha Sāsana is still alive,

284
00:19:35,600 --> 00:19:39,600
 and in some places thriving even.

285
00:19:39,600 --> 00:19:46,600
 So I would love to hear your thoughts on that.

286
00:19:46,600 --> 00:19:50,600
 Well, it's not a matter of whether it's still possible.

287
00:19:50,600 --> 00:19:53,340
 I mean, the teachings are there, the teachings are still

288
00:19:53,340 --> 00:19:53,600
 here.

289
00:19:53,600 --> 00:19:57,570
 The only way it wouldn't be possible is if there were

290
00:19:57,570 --> 00:19:57,600
 either

291
00:19:57,600 --> 00:20:00,600
 no teachings or no one practicing them.

292
00:20:00,600 --> 00:20:05,600
 So if you practice them, the results are there.

293
00:20:05,600 --> 00:20:09,600
 You also need people teaching them and guiding through them

294
00:20:09,600 --> 00:20:09,600
,

295
00:20:09,600 --> 00:20:13,600
 because many of us, teachings themselves,

296
00:20:13,600 --> 00:20:22,600
 aren't enough to push you to practice them.

297
00:20:22,600 --> 00:20:23,600
 Wonderful.

298
00:20:23,600 --> 00:20:27,600
 Yeah, that was the direction of my thought as well.

299
00:20:27,600 --> 00:20:31,600
 If one visits where the teaching of the Buddha is live,

300
00:20:31,600 --> 00:20:34,600
 and there are good teachers.

301
00:20:34,600 --> 00:20:36,600
 I was just like, of course it's possible.

302
00:20:36,600 --> 00:20:39,600
 It must be possible. So thanks.

303
00:20:39,600 --> 00:20:44,600
 Absolutely. The doors are not closed.

304
00:20:44,600 --> 00:20:48,600
 Wonderful.

305
00:20:48,600 --> 00:20:51,600
 Okay. Have a good night.

306
00:20:51,600 --> 00:20:53,600
 Good night, everyone.

307
00:20:53,600 --> 00:20:54,600
 Bye, Bhāgavatthi.

308
00:20:54,600 --> 00:20:55,600
 Bye.

309
00:20:55,600 --> 00:20:56,600
 Thank you.

310
00:20:58,600 --> 00:20:59,600
 Thank you.

