1
00:00:00,000 --> 00:00:03,990
 Hello, I'm Chris, a volunteer for Serum-Anglo International

2
00:00:03,990 --> 00:00:06,240
.

3
00:00:06,240 --> 00:00:08,760
 Across the world organizations are making adjustments to

4
00:00:08,760 --> 00:00:10,200
 aid in preventing the spread

5
00:00:10,200 --> 00:00:11,920
 of the novel coronavirus.

6
00:00:11,920 --> 00:00:14,660
 Serum-Anglo International is no different.

7
00:00:14,660 --> 00:00:16,710
 The board of directors has decided for the safety of

8
00:00:16,710 --> 00:00:18,120
 everyone and in keeping with the

9
00:00:18,120 --> 00:00:20,800
 best advice available to us to cancel all meditation

10
00:00:20,800 --> 00:00:22,680
 courses at our centre in Ontario,

11
00:00:22,680 --> 00:00:24,640
 Canada for the time being.

12
00:00:24,640 --> 00:00:27,110
 Our tentative date for reopening is May 16th, subject to

13
00:00:27,110 --> 00:00:29,960
 new information and as things evolve.

14
00:00:29,960 --> 00:00:32,600
 We recognize that this is inconvenient to many who have

15
00:00:32,600 --> 00:00:34,360
 already been approved for courses

16
00:00:34,360 --> 00:00:36,950
 and it's unfortunate to pass up on this opportunity to

17
00:00:36,950 --> 00:00:37,920
 share the Tama.

18
00:00:37,920 --> 00:00:40,680
 However, we believe that it is the most responsible choice

19
00:00:40,680 --> 00:00:42,320
 not to contribute to the spread of

20
00:00:42,320 --> 00:00:44,040
 the virus.

21
00:00:44,040 --> 00:00:47,220
 We urge any who have already made travel plans to reverse

22
00:00:47,220 --> 00:00:47,760
 them.

23
00:00:47,760 --> 00:00:50,390
 Our volunteers will happily aid you in rescheduling your in

24
00:00:50,390 --> 00:00:52,040
-person course for a future date when

25
00:00:52,040 --> 00:00:54,200
 conditions have improved.

26
00:00:54,200 --> 00:00:57,110
 Serum-Anglo offers online meditation courses with venerable

27
00:00:57,110 --> 00:00:58,600
 Yutta Tama scheduled through

28
00:00:58,600 --> 00:01:03,420
 the Meditation Plus application at meditation.serum-anglo.

29
00:01:03,420 --> 00:01:05,680
org/schedule with Google Hangouts providing live

30
00:01:05,680 --> 00:01:08,480
 communication between student and teacher.

31
00:01:08,480 --> 00:01:10,720
 Only headphones or speakers and a microphone as well as a

32
00:01:10,720 --> 00:01:12,120
 modest internet connection are

33
00:01:12,120 --> 00:01:15,320
 required to participate in this way.

34
00:01:15,320 --> 00:01:17,220
 Any person who has been scheduled to visit the centre

35
00:01:17,220 --> 00:01:18,920
 itself may absolutely instead participate

36
00:01:18,920 --> 00:01:19,920
 in an online course.

37
00:01:19,920 --> 00:01:23,710
 For more information, please visit serum-anglo.org/med

38
00:01:23,710 --> 00:01:24,520
itation.

39
00:01:24,520 --> 00:01:26,920
 We appreciate your understanding and hope that your

40
00:01:26,920 --> 00:01:28,480
 practice continues and provides

41
00:01:28,480 --> 00:01:31,080
 much benefit to you and to the whole world.

42
00:01:31,080 --> 00:01:31,480
 May you be happy.

43
00:01:31,480 --> 00:01:41,480
 [BLANK_AUDIO]

