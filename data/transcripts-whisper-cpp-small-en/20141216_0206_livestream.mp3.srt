1
00:00:00,000 --> 00:00:07,000
 Okay, good morning.

2
00:00:07,000 --> 00:00:22,000
 Again, recording live from Bangkok, Thailand.

3
00:00:22,000 --> 00:00:33,000
 Yesterday I tried to give a talk on the seven saprohisadham

4
00:00:33,000 --> 00:00:33,000
,

5
00:00:33,000 --> 00:00:41,520
 the qualities of a good person or a good fellow, a

6
00:00:41,520 --> 00:00:43,000
 gentleman.

7
00:00:43,000 --> 00:00:47,000
 The literal translation is probably "gentleman".

8
00:00:47,000 --> 00:00:51,000
 Gentleman.

9
00:00:51,000 --> 00:00:55,000
 But given that that's a gender-specific word,

10
00:00:55,000 --> 00:00:59,000
 just a saprohisa is actually gender-specific as well.

11
00:00:59,000 --> 00:01:05,250
 It's more comfortable to make a different translation like

12
00:01:05,250 --> 00:01:13,000
 "good fellow" or something.

13
00:01:13,000 --> 00:01:22,000
 But unfortunately the recording cut out after 15 minutes.

14
00:01:22,000 --> 00:01:32,000
 So I figured today I'll just start over.

15
00:01:32,000 --> 00:01:35,000
 Maybe go a little quicker.

16
00:01:35,000 --> 00:01:41,000
 Just give it in brief.

17
00:01:41,000 --> 00:01:44,000
 Discussion of these dhammas,

18
00:01:44,000 --> 00:01:52,000
 our daily dosa dhamma, daily dosa, the Buddhist teaching.

19
00:01:52,000 --> 00:01:57,610
 So I had given this talk on Sunday about the seven saproh

20
00:01:57,610 --> 00:02:00,000
isa dhammas in Thai

21
00:02:00,000 --> 00:02:05,000
 and I thought it went over quite well.

22
00:02:05,000 --> 00:02:13,920
 It's quite interesting for Thailand because there is some

23
00:02:13,920 --> 00:02:17,000
 political instability

24
00:02:17,000 --> 00:02:24,000
 or there has been political instability here.

25
00:02:24,000 --> 00:02:27,520
 The idea of what's going to solve the problems is high on

26
00:02:27,520 --> 00:02:31,000
 people's minds.

27
00:02:31,000 --> 00:02:36,000
 From a Buddhist point of view, one of the main solutions is

28
00:02:36,000 --> 00:02:39,000
 to cultivate these dhammas,

29
00:02:39,000 --> 00:02:51,000
 to find people who are good,

30
00:02:51,000 --> 00:02:58,240
 who have the qualities required to make this country a

31
00:02:58,240 --> 00:02:59,000
 better place,

32
00:02:59,000 --> 00:03:11,000
 to make this world a better place.

33
00:03:11,000 --> 00:03:16,050
 These seven qualities don't describe all aspects of

34
00:03:16,050 --> 00:03:17,000
 goodness

35
00:03:17,000 --> 00:03:26,180
 but they're particularly relating to one's position in

36
00:03:26,180 --> 00:03:27,000
 society.

37
00:03:27,000 --> 00:03:33,690
 Why it's interesting also for meditator is it gives another

38
00:03:33,690 --> 00:03:36,000
 aspect of the qualities

39
00:03:36,000 --> 00:03:47,360
 that come from practice of goodness in terms of how it

40
00:03:47,360 --> 00:03:56,000
 allows us to live in the world

41
00:03:56,000 --> 00:04:03,000
 and to skillfully navigate our way through life.

42
00:04:03,000 --> 00:04:06,000
 It involves a relationship with other people,

43
00:04:06,000 --> 00:04:16,500
 it involves our own organization of our own lives and our

44
00:04:16,500 --> 00:04:24,000
 own mind.

45
00:04:24,000 --> 00:04:35,000
 It describes the way in which we should act

46
00:04:35,000 --> 00:04:45,000
 and we should organize the qualities that we should have

47
00:04:45,000 --> 00:04:50,000
 in order to live in peace in the world.

48
00:04:50,000 --> 00:04:54,000
 There are seven of them.

49
00:04:54,000 --> 00:04:59,000
 Make one a gentle person.

50
00:04:59,000 --> 00:05:08,620
 The first one is dhamma-nyuta, that one should know the d

51
00:05:08,620 --> 00:05:10,000
hamma.

52
00:05:10,000 --> 00:05:14,890
 The second one is ata-nyuta, that one should know the

53
00:05:14,890 --> 00:05:18,000
 meaning of the dhamma.

54
00:05:18,000 --> 00:05:24,000
 Number three is ata-nyuta, one should know oneself.

55
00:05:24,000 --> 00:05:29,680
 Number four, kala-nyuta, matan-nyuta, number four, one

56
00:05:29,680 --> 00:05:32,000
 should know moderation.

57
00:05:32,000 --> 00:05:40,000
 Kala-nyuta number five means one should know time,

58
00:05:40,000 --> 00:05:46,000
 be knowledgeable in regards to time.

59
00:05:46,000 --> 00:05:50,000
 Number six, pari-san-nyuta, one should be knowledgeable

60
00:05:50,000 --> 00:05:52,000
 about company,

61
00:05:52,000 --> 00:05:59,000
 the company one keeps, the company one is in, different

62
00:05:59,000 --> 00:06:01,000
 companies.

63
00:06:01,000 --> 00:06:05,000
 Number seven, pukalappa-ropa-ran-nyuta,

64
00:06:05,000 --> 00:06:11,800
 one should be knowledgeable in regards to discerning or

65
00:06:11,800 --> 00:06:16,000
 differentiating between different types of people.

66
00:06:16,000 --> 00:06:22,330
 Good people, bad people, people who have wisdom, people who

67
00:06:22,330 --> 00:06:26,000
 are without wisdom, people who have goodness, people.

68
00:06:26,000 --> 00:06:30,000
 These are the seven sappurisa dhammas.

69
00:06:30,000 --> 00:06:37,900
 Another interesting list for us to consider, something

70
00:06:37,900 --> 00:06:39,000
 worth hearing.

71
00:06:39,000 --> 00:06:42,000
 Just a little bit of dhamma today.

72
00:06:42,000 --> 00:06:51,000
 So in brief, we'll discuss these.

73
00:06:51,000 --> 00:06:54,960
 Dhamma-nyuta means to know the truth or to know the dhamma,

74
00:06:54,960 --> 00:06:59,000
 whether you're talking about the teachings of the Buddha,

75
00:06:59,000 --> 00:07:03,140
 or in terms of actual dhammas, in terms of reality, it

76
00:07:03,140 --> 00:07:05,000
 really means the same thing,

77
00:07:05,000 --> 00:07:10,100
 it means an understanding of the truth or knowledge of the

78
00:07:10,100 --> 00:07:11,000
 truth.

79
00:07:11,000 --> 00:07:14,190
 It's actually, you have to separate because there's two

80
00:07:14,190 --> 00:07:15,000
 aspects here.

81
00:07:15,000 --> 00:07:20,500
 You have to specify dhamma-nyuta here, dhamma-nyuta and ata

82
00:07:20,500 --> 00:07:24,000
-nyuta are complementary.

83
00:07:24,000 --> 00:07:29,630
 Dhamma-nyuta means knowing the truth, and ata-nyuta means

84
00:07:29,630 --> 00:07:35,000
 understanding it, or knowing the meaning of the truth.

85
00:07:35,000 --> 00:07:41,000
 So in terms of simple study, this is easy to understand,

86
00:07:41,000 --> 00:07:45,250
 but because there's the memorization and the knowledge of

87
00:07:45,250 --> 00:07:46,000
 the terms,

88
00:07:46,000 --> 00:07:48,350
 for example, knowing what are the four foundations of

89
00:07:48,350 --> 00:07:52,000
 mindfulness, or knowing what are the five precepts.

90
00:07:52,000 --> 00:07:56,170
 Ata-nyuta, in terms of study, just means to understand what

91
00:07:56,170 --> 00:08:00,260
 it means, like first precept not to kill, what does that

92
00:08:00,260 --> 00:08:01,000
 mean?

93
00:08:01,000 --> 00:08:07,000
 Understanding the meaning of it.

94
00:08:07,000 --> 00:08:12,740
 So the details and just the overall meaning of the

95
00:08:12,740 --> 00:08:17,000
 different aspects of the dhamma.

96
00:08:17,000 --> 00:08:20,450
 But there's a deeper point here in terms of practice, and

97
00:08:20,450 --> 00:08:25,000
 that is that simply knowing the dhamma is not enough,

98
00:08:25,000 --> 00:08:31,030
 but the true understanding of the dhamma requires actual

99
00:08:31,030 --> 00:08:36,510
 experience, experience of the truth, realization of the

100
00:08:36,510 --> 00:08:37,000
 truth,

101
00:08:37,000 --> 00:08:40,000
 and that's what leads to ata-nyuta.

102
00:08:40,000 --> 00:08:44,470
 Someone can know the various truths in terms, because they

103
00:08:44,470 --> 00:08:47,000
've heard it, because they've thought about it,

104
00:08:47,000 --> 00:08:50,640
 but if they don't have the true understanding of the

105
00:08:50,640 --> 00:08:53,000
 meaning behind it,

106
00:08:53,000 --> 00:08:56,630
 like anityang, for example, the commentary says imperman

107
00:08:56,630 --> 00:09:00,000
ence, one can understand impermanence in terms of the fact

108
00:09:00,000 --> 00:09:02,000
 that in a hundred years I'll be dead,

109
00:09:02,000 --> 00:09:05,000
 that life is impermanent, life is uncertain.

110
00:09:05,000 --> 00:09:09,300
 But then another person will understand it in terms of that

111
00:09:09,300 --> 00:09:12,000
 they were young and now they're old,

112
00:09:12,000 --> 00:09:15,000
 and they're going to get older and they're going to change,

113
00:09:15,000 --> 00:09:19,000
 and so life is impermanent, and so on and so on,

114
00:09:19,000 --> 00:09:23,120
 and so forth, and some people will understand it in terms

115
00:09:23,120 --> 00:09:28,000
 of not being the same person that they were yesterday.

116
00:09:28,000 --> 00:09:33,910
 But the true meditator, the true Buddha, the truly wise

117
00:09:33,910 --> 00:09:36,000
 person, let's say,

118
00:09:36,000 --> 00:09:41,640
 understands every moment as being a new moment, a new

119
00:09:41,640 --> 00:09:46,000
 person, a new being, arising and seizing.

120
00:09:46,000 --> 00:09:48,870
 That impermanence is in regard to experience, in regard to

121
00:09:48,870 --> 00:09:50,000
 moment by moment.

122
00:09:50,000 --> 00:10:01,000
 So the meaning of it is understood differently by different

123
00:10:01,000 --> 00:10:03,000
 people.

124
00:10:03,000 --> 00:10:06,330
 So simply understanding good things, understanding the

125
00:10:06,330 --> 00:10:12,000
 truth is, in a general sense,

126
00:10:12,000 --> 00:10:16,000
 is not the same as truly understanding it on a deep,

127
00:10:16,000 --> 00:10:21,000
 fundamental, ultimate reality level,

128
00:10:21,000 --> 00:10:24,000
 which of course only comes through practice.

129
00:10:24,000 --> 00:10:28,280
 These two things make one a sapuvisa, a special being in

130
00:10:28,280 --> 00:10:30,000
 the world,

131
00:10:30,000 --> 00:10:35,590
 and make one ideally suited for leadership and instruction

132
00:10:35,590 --> 00:10:39,000
 and support, guidance.

133
00:10:39,000 --> 00:10:44,000
 All good things come from one who knows the Dhamma.

134
00:10:44,000 --> 00:10:48,160
 This is the key. We talk too much about systems of

135
00:10:48,160 --> 00:10:52,000
 government and checks and balances.

136
00:10:52,000 --> 00:10:57,000
 We should talk much more about goodness and evil,

137
00:10:57,000 --> 00:11:02,000
 and cultivating goodness and good friendship and so on,

138
00:11:02,000 --> 00:11:07,160
 finding such people, becoming such people, focusing on good

139
00:11:07,160 --> 00:11:13,000
 things, focusing on helping each other.

140
00:11:13,000 --> 00:11:16,000
 If we had such people as this, who understood the Dhamma,

141
00:11:16,000 --> 00:11:20,070
 if these people were in positions of authority, the world

142
00:11:20,070 --> 00:11:24,000
 would be a much different place.

143
00:11:24,000 --> 00:11:27,420
 So that's the first two. The third one, atanjuta, also very

144
00:11:27,420 --> 00:11:30,000
 important, knowing oneself.

145
00:11:30,000 --> 00:11:35,870
 The second one was atanjuta, this one is atanjuta, without

146
00:11:35,870 --> 00:11:37,000
 the "h".

147
00:11:37,000 --> 00:11:41,000
 Atanjuta, "atta" means self, the atman.

148
00:11:41,000 --> 00:11:44,070
 But it doesn't mean to know the self, to know the soul, it

149
00:11:44,070 --> 00:11:45,000
 has nothing to do with that.

150
00:11:45,000 --> 00:11:48,560
 It means to know oneself in a colloquial sense of not

151
00:11:48,560 --> 00:11:50,000
 forgetting one's position,

152
00:11:50,000 --> 00:11:54,330
 not forgetting one's duties, not forgetting one's roles in

153
00:11:54,330 --> 00:11:55,000
 life,

154
00:11:55,000 --> 00:12:00,180
 towards one's parents, one's children, one's spouse, or

155
00:12:00,180 --> 00:12:08,000
 friends, or employees, employers, and so on.

156
00:12:08,000 --> 00:12:12,190
 From a worldly perspective, this is important to maintain

157
00:12:12,190 --> 00:12:14,000
 harmony in the world.

158
00:12:14,000 --> 00:12:18,320
 People who are accomplished and supportive and helpful in

159
00:12:18,320 --> 00:12:19,000
 society

160
00:12:19,000 --> 00:12:23,010
 have this quality of knowing their duty and knowing where

161
00:12:23,010 --> 00:12:36,000
 they fit in, knowing how to, knowing their position.

162
00:12:36,000 --> 00:12:39,960
 From the point of view of the Dhamma, this is of course

163
00:12:39,960 --> 00:12:41,000
 even more important,

164
00:12:41,000 --> 00:12:45,480
 refers to understanding your experiences, knowing yourself

165
00:12:45,480 --> 00:12:49,000
 in terms of knowing what you're doing right now,

166
00:12:49,000 --> 00:12:56,230
 knowing yourself, knowing your habits, knowing the problems

167
00:12:56,230 --> 00:12:59,000
 that you have that you have to work on,

168
00:12:59,000 --> 00:13:04,750
 knowing your strengths, knowing how your mind works so you

169
00:13:04,750 --> 00:13:08,000
 can use it effectively.

170
00:13:08,000 --> 00:13:10,940
 Not being caught off guard or not being overwhelmed or not

171
00:13:10,940 --> 00:13:19,000
 being carried away by your mind because you understand it.

172
00:13:19,000 --> 00:13:25,110
 Understanding of the mind is crucial, both in meditation

173
00:13:25,110 --> 00:13:30,000
 practice and in the world.

174
00:13:30,000 --> 00:13:32,800
 If you don't understand your own mind, it's the first thing

175
00:13:32,800 --> 00:13:34,000
 that will catch you off guard,

176
00:13:34,000 --> 00:13:41,150
 your emotions, your judgments, your views, they will cripp

177
00:13:41,150 --> 00:13:45,000
le you because you don't know how to deal with them.

178
00:13:45,000 --> 00:13:47,360
 You want something, you have to chase after it. You don't

179
00:13:47,360 --> 00:13:55,000
 like something, you have to destroy it or chase it away.

180
00:13:55,000 --> 00:14:03,000
 It makes it very difficult to accomplish your goals in life

181
00:14:03,000 --> 00:14:08,000
 or sets you on the wrong goal, creating disharmony,

182
00:14:08,000 --> 00:14:12,000
 creating suffering.

183
00:14:12,000 --> 00:14:17,570
 Atanjuta, knowing yourself, meditation practice helps in

184
00:14:17,570 --> 00:14:22,000
 all aspects of life because you know yourself,

185
00:14:22,000 --> 00:14:26,080
 you understand the source of everything, the source of your

186
00:14:26,080 --> 00:14:29,000
 whole life, it's your mind, your own self.

187
00:14:29,000 --> 00:14:34,170
 You understand it and so you can take things with

188
00:14:34,170 --> 00:14:41,480
 equilibrium, with balance, with focus, with wisdom, with

189
00:14:41,480 --> 00:14:45,000
 mindfulness, all good things.

190
00:14:45,000 --> 00:14:49,770
 Atanjuta, number three, number four, matanjuta, knowing

191
00:14:49,770 --> 00:14:51,000
 moderation.

192
00:14:51,000 --> 00:14:57,730
 So a good person, a gentle person, is someone who knows

193
00:14:57,730 --> 00:15:03,430
 moderation, doesn't overindulge and also doesn't deny

194
00:15:03,430 --> 00:15:04,000
 oneself,

195
00:15:04,000 --> 00:15:10,320
 so doesn't starve oneself or hurt oneself, working too hard

196
00:15:10,320 --> 00:15:16,000
 or getting lazy, knowing balance and moderation.

197
00:15:16,000 --> 00:15:19,570
 It doesn't mean moderation in everything. People use this

198
00:15:19,570 --> 00:15:23,290
 kind of dhamma to talk about moderation in things like

199
00:15:23,290 --> 00:15:25,000
 alcohol, for example.

200
00:15:25,000 --> 00:15:28,000
 I was talking yesterday about how that's kind of ludicrous.

201
00:15:28,000 --> 00:15:30,000
 I don't know if that actually got through yesterday,

202
00:15:30,000 --> 00:15:37,430
 but the idea of drinking alcohol, people talk about, "Well,

203
00:15:37,430 --> 00:15:40,490
 I only had one or two drinks, I was following the middle

204
00:15:40,490 --> 00:15:41,000
 way."

205
00:15:41,000 --> 00:15:44,140
 Think of this as the middle way of the Buddha, only having

206
00:15:44,140 --> 00:15:50,980
 a few drinks, only having half as many drinks as they

207
00:15:50,980 --> 00:15:54,000
 normally would.

208
00:15:54,000 --> 00:15:57,490
 Of course, that doesn't work. Moderation is in regards to

209
00:15:57,490 --> 00:16:00,540
 things that are necessary, things that are required for

210
00:16:00,540 --> 00:16:01,000
 life.

211
00:16:01,000 --> 00:16:06,640
 Alcohol is not one of those things, it's intrinsically

212
00:16:06,640 --> 00:16:09,000
 harmful to the mind.

213
00:16:09,000 --> 00:16:12,570
 I don't know if that can be said, but it impedes the mind's

214
00:16:12,570 --> 00:16:15,000
 ability to cultivate good states.

215
00:16:15,000 --> 00:16:17,730
 If someone's already enlightened, then I suppose alcohol

216
00:16:17,730 --> 00:16:19,000
 wouldn't have an effect,

217
00:16:19,000 --> 00:16:27,000
 but it prevents one from actively cultivating goodness.

218
00:16:27,000 --> 00:16:30,270
 When there's the potential for the arising of evil, one

219
00:16:30,270 --> 00:16:36,300
 doesn't have the ability to choose against it, to reject it

220
00:16:36,300 --> 00:16:37,000
.

221
00:16:37,000 --> 00:16:40,500
 One loses one's ability to discriminate between good and

222
00:16:40,500 --> 00:16:49,000
 evil, so one loses one's ability to be mindful, really.

223
00:16:49,000 --> 00:16:54,000
 But moderation in many aspects of life is very important.

224
00:16:54,000 --> 00:17:02,030
 You have too much, or you work too much, or you do too many

225
00:17:02,030 --> 00:17:06,000
 different things.

226
00:17:06,000 --> 00:17:10,000
 Even in those things that are important for life,

227
00:17:10,000 --> 00:17:15,040
 moderation is a sign of someone who is mindful, someone who

228
00:17:15,040 --> 00:17:16,000
 is wise.

229
00:17:16,000 --> 00:17:19,000
 It's of course something that would help this world greatly

230
00:17:19,000 --> 00:17:20,000
 if we all knew moderation

231
00:17:20,000 --> 00:17:24,010
 and didn't want and need more and more and more, bigger and

232
00:17:24,010 --> 00:17:30,000
 better and faster and stronger and on and on and on.

233
00:17:30,000 --> 00:17:34,150
 We could teach ourselves contentment with what we have,

234
00:17:34,150 --> 00:17:39,300
 contentment in our requisites, food and clothing and

235
00:17:39,300 --> 00:17:42,000
 lodging and medicine,

236
00:17:42,000 --> 00:17:49,100
 contentment in our situation, our experiences, seeking out

237
00:17:49,100 --> 00:17:57,000
 new things, not rejecting our reality.

238
00:17:57,000 --> 00:18:00,840
 Not rejecting it in a way, but rejecting it in the sense of

239
00:18:00,840 --> 00:18:04,830
 rejecting all of experience, letting go of all of

240
00:18:04,830 --> 00:18:08,000
 attachment to any experience.

241
00:18:08,000 --> 00:18:14,890
 But content and moderate, in terms of just taking what is

242
00:18:14,890 --> 00:18:19,000
 necessary, no more, no less.

243
00:18:19,000 --> 00:18:26,000
 Something that would change the world quite profoundly.

244
00:18:26,000 --> 00:18:29,000
 Solve a lot of the world's problems.

245
00:18:29,000 --> 00:18:33,000
 Matanyutta, number 5, kala nyutta.

246
00:18:33,000 --> 00:18:36,000
 Kala nyutta means to know time.

247
00:18:36,000 --> 00:18:38,490
 So in a worldly sense it means knowing the right time for

248
00:18:38,490 --> 00:18:45,000
 things, not being late, not being early.

249
00:18:45,000 --> 00:18:50,000
 Knowing the proper time to speak, the proper time to act.

250
00:18:50,000 --> 00:18:56,100
 These are all signs of someone who has foresight, who has

251
00:18:56,100 --> 00:19:00,900
 wisdom, intelligence, who has culture, a cultured

252
00:19:00,900 --> 00:19:02,000
 individual,

253
00:19:02,000 --> 00:19:07,280
 someone who is on time and who is timely in the sense of,

254
00:19:07,280 --> 00:19:12,000
 does things at the appropriate time and so on.

255
00:19:12,000 --> 00:19:16,530
 But from a meditative point of view, and of course always

256
00:19:16,530 --> 00:19:18,000
 more important,

257
00:19:18,000 --> 00:19:23,000
 here it refers to understanding the present moment,

258
00:19:23,000 --> 00:19:26,400
 understanding the difference between ultimate time and

259
00:19:26,400 --> 00:19:29,000
 conventional time.

260
00:19:29,000 --> 00:19:32,000
 Real time.

261
00:19:32,000 --> 00:19:37,660
 Real time and whatever the obvious, unreal time I guess,

262
00:19:37,660 --> 00:19:39,000
 fake time.

263
00:19:39,000 --> 00:19:44,650
 In video editing they use this term, real time, I guess in

264
00:19:44,650 --> 00:19:50,000
 different fields they use the word real time,

265
00:19:50,000 --> 00:19:57,000
 but I know my uncle does video editing so I know this term.

266
00:19:57,000 --> 00:20:01,000
 It means that it's happening as it happens, right?

267
00:20:01,000 --> 00:20:02,000
 It's just real time.

268
00:20:02,000 --> 00:20:04,000
 But it's an interesting term.

269
00:20:04,000 --> 00:20:06,000
 I guess it's a good term for Buddhism.

270
00:20:06,000 --> 00:20:08,000
 That's what we mean here, the difference.

271
00:20:08,000 --> 00:20:11,000
 Real time and Buddhism is exactly that, it's as it happens.

272
00:20:11,000 --> 00:20:16,210
 And the meaning of the word real is, there's a deeper

273
00:20:16,210 --> 00:20:20,000
 meaning to the word real because that is all that's real,

274
00:20:20,000 --> 00:20:23,000
 that is real time.

275
00:20:23,000 --> 00:20:26,630
 The past is not real time, the future is also not real time

276
00:20:26,630 --> 00:20:28,000
, it's a concept.

277
00:20:28,000 --> 00:20:34,060
 They don't exist, you can't find the future anywhere or the

278
00:20:34,060 --> 00:20:35,000
 past.

279
00:20:35,000 --> 00:20:48,080
 This is why they say someday never comes or tomorrow will

280
00:20:48,080 --> 00:20:56,000
 never come, it's always today.

281
00:20:56,000 --> 00:21:01,950
 This is important because reality only exists in the

282
00:21:01,950 --> 00:21:05,000
 present moment, that's where you have to apply.

283
00:21:05,000 --> 00:21:09,520
 If you want to understand reality, if you want to overcome

284
00:21:09,520 --> 00:21:11,000
 your real problems,

285
00:21:11,000 --> 00:21:14,100
 if you want to understand what the real problem is when you

286
00:21:14,100 --> 00:21:18,000
 have suffering, you need to do it in real time.

287
00:21:18,000 --> 00:21:22,430
 You can't do it in the past which is already gone or in the

288
00:21:22,430 --> 00:21:24,000
 future which hasn't come yet.

289
00:21:24,000 --> 00:21:30,450
 This is crucial in Buddhism, the Buddha was repeatedly

290
00:21:30,450 --> 00:21:35,000
 admonished the monks to not go back to the past,

291
00:21:35,000 --> 00:21:38,080
 to not worry about the future, to just see clearly in the

292
00:21:38,080 --> 00:21:39,000
 present moment.

293
00:21:39,000 --> 00:21:48,060
 Vipassati, vipassati, vipassana, tatatatat, vipassati.

294
00:21:48,060 --> 00:21:56,000
 Number five. Number six means understanding companies.

295
00:21:56,000 --> 00:22:00,140
 So this is more in terms of the world, worldly sense, it

296
00:22:00,140 --> 00:22:03,080
 means knowing how to deal with different groups of people,

297
00:22:03,080 --> 00:22:07,000
 different types of people.

298
00:22:07,000 --> 00:22:12,060
 It's a very important skill and it's aided very much by

299
00:22:12,060 --> 00:22:17,380
 meditation practice, being able to adapt, not being stuck

300
00:22:17,380 --> 00:22:20,000
 in our own habits.

301
00:22:20,000 --> 00:22:24,790
 So people who are very cultured in a specific company, a

302
00:22:24,790 --> 00:22:28,550
 specific group of people, often have a hard time fitting in

303
00:22:28,550 --> 00:22:30,000
 with other groups of people

304
00:22:30,000 --> 00:22:36,350
 and their reality or their ability, their capability is

305
00:22:36,350 --> 00:22:40,000
 circumscribed by the situation.

306
00:22:40,000 --> 00:22:46,870
 A person who has become open and flexible and has let go of

307
00:22:46,870 --> 00:22:55,000
 a lot of their prejudices and predilections and stuff.

308
00:22:55,000 --> 00:22:58,160
 It will be a much better student to fit in and to adapt to

309
00:22:58,160 --> 00:23:00,000
 different groups of people.

310
00:23:00,000 --> 00:23:03,000
 It's a sign of a good person that they are able to adapt.

311
00:23:03,000 --> 00:23:05,880
 So if you come to a group of people you've never met or are

312
00:23:05,880 --> 00:23:09,580
 quite different, have a different culture, your ability to

313
00:23:09,580 --> 00:23:17,000
 fit in with them is seen by the world as a sign of culture,

314
00:23:17,000 --> 00:23:27,000
 a sign of mental cultivation, being a cultivated individual

315
00:23:27,000 --> 00:23:27,000
.

316
00:23:27,000 --> 00:23:31,390
 And number seven, "parisa" - "pugla pero paranutta" means

317
00:23:31,390 --> 00:23:34,000
 knowing the difference between people,

318
00:23:34,000 --> 00:23:38,260
 and of course obviously crucial in both the world and in

319
00:23:38,260 --> 00:23:42,700
 our meditation practice as well, to know the difference

320
00:23:42,700 --> 00:23:47,380
 between people, to associate only with certain types of

321
00:23:47,380 --> 00:23:48,000
 people,

322
00:23:48,000 --> 00:23:52,720
 to not get too involved with certain types of people if we

323
00:23:52,720 --> 00:23:54,000
 can help it.

324
00:23:54,000 --> 00:23:58,000
 That's not so much rejecting these people as individuals,

325
00:23:58,000 --> 00:24:02,000
 it's about...

326
00:24:02,000 --> 00:24:06,450
 It's about having the best result we can and helping the

327
00:24:06,450 --> 00:24:07,000
 world.

328
00:24:07,000 --> 00:24:11,550
 If we spend our time around people who are really not

329
00:24:11,550 --> 00:24:14,000
 interested in cultivating goodness,

330
00:24:14,000 --> 00:24:16,920
 we're just going to wear ourselves out and we won't help

331
00:24:16,920 --> 00:24:19,000
 them and we won't help themselves.

332
00:24:19,000 --> 00:24:26,340
 But if we spend time with people who are highly cultivated,

333
00:24:26,340 --> 00:24:32,040
 we'll be much better able to help them and help ourselves

334
00:24:32,040 --> 00:24:35,990
 and help the people around them work together to bring

335
00:24:35,990 --> 00:24:39,000
 goodness to the world.

336
00:24:39,000 --> 00:24:44,830
 This is why community is so important and discrimination is

337
00:24:44,830 --> 00:24:49,120
 equally important, not just having a community but having a

338
00:24:49,120 --> 00:24:55,000
 good community and having the right community.

339
00:24:55,000 --> 00:24:58,000
 Being able to discriminate.

340
00:24:58,000 --> 00:25:02,000
 That's what it's all about, being discriminatory.

341
00:25:02,000 --> 00:25:08,550
 It's not any kind of discrimination but good discrimination

342
00:25:08,550 --> 00:25:09,000
.

343
00:25:09,000 --> 00:25:11,270
 It's discriminatory in a good way, being able to

344
00:25:11,270 --> 00:25:13,990
 discriminate between right and wrong and good and bad,

345
00:25:13,990 --> 00:25:17,000
 proper and proper and so on.

346
00:25:17,000 --> 00:25:22,060
 And just in the sense of being able to discern the truth,

347
00:25:22,060 --> 00:25:25,590
 discriminate between the truth and delusion, to see through

348
00:25:25,590 --> 00:25:30,000
 illusion, see through delusion,

349
00:25:30,000 --> 00:25:34,600
 to see through our own biases, to see what's really going

350
00:25:34,600 --> 00:25:35,000
 on.

351
00:25:35,000 --> 00:25:38,000
 That's what makes you a good person.

352
00:25:38,000 --> 00:25:42,000
 You notice that all of these end with anjuta.

353
00:25:42,000 --> 00:25:49,150
 Anjuta means knowledge or understanding or being someone

354
00:25:49,150 --> 00:25:51,000
 who understands actually.

355
00:25:51,000 --> 00:25:56,850
 Anya means knowledge, anyu means one who has knowledge, anj

356
00:25:56,850 --> 00:26:01,000
uta means being someone who has knowledge.

357
00:26:01,000 --> 00:26:04,370
 This is how Pali works. You've got three aspects, three

358
00:26:04,370 --> 00:26:06,000
 parts of that word, four actually.

359
00:26:06,000 --> 00:26:09,480
 The first part is dhamma, for example. Dhamma means the d

360
00:26:09,480 --> 00:26:11,000
hamma or truth.

361
00:26:11,000 --> 00:26:15,000
 Anya means knowledge, so knowledge of the truth.

362
00:26:15,000 --> 00:26:19,920
 When you add an u on the end, you get dhammanu, one who has

363
00:26:19,920 --> 00:26:22,000
 knowledge of the truth.

364
00:26:22,000 --> 00:26:27,020
 And then when you add the ta on the end, it becomes being,

365
00:26:27,020 --> 00:26:32,000
 the state of being one who has knowledge of the truth.

366
00:26:32,000 --> 00:26:36,480
 But the point, rather than just giving you a quick Pali

367
00:26:36,480 --> 00:26:43,000
 grammar lesson, the point is the anya, the knowledge.

368
00:26:43,000 --> 00:26:46,620
 Being a good person is all about your knowledge, your

369
00:26:46,620 --> 00:26:48,000
 understanding.

370
00:26:48,000 --> 00:26:54,740
 Wisdom. Wisdom is far and away the most important quality

371
00:26:54,740 --> 00:26:58,000
 of mind for us to develop.

372
00:26:58,000 --> 00:27:10,000
 And it's special in that you can't simply develop wisdom.

373
00:27:10,000 --> 00:27:13,340
 You can't just find wisdom or say, "I'm going to now be

374
00:27:13,340 --> 00:27:14,000
 wise."

375
00:27:14,000 --> 00:27:18,000
 Wisdom is not something you can cultivate directly.

376
00:27:18,000 --> 00:27:22,110
 It's special in that it has to come from the cultivation of

377
00:27:22,110 --> 00:27:24,000
 all other good states.

378
00:27:24,000 --> 00:27:27,950
 It's the culmination of goodness. If your practice isn't

379
00:27:27,950 --> 00:27:29,000
 leading you to wisdom,

380
00:27:29,000 --> 00:27:33,700
 if it's just leading you to, for example, peace or calmness

381
00:27:33,700 --> 00:27:37,000
, which would be temporary actually,

382
00:27:37,000 --> 00:27:40,480
 but if it's just leading to that and not leading to

383
00:27:40,480 --> 00:27:44,000
 understanding of the times when you're not calm,

384
00:27:44,000 --> 00:27:46,800
 then you haven't reached the culmination yet. You're

385
00:27:46,800 --> 00:27:52,000
 missing out on this most important of all dhammas.

386
00:27:52,000 --> 00:27:57,000
 So there you have it. Those are the seven sappulisa dhammas

387
00:27:57,000 --> 00:27:57,000
.

388
00:27:57,000 --> 00:28:01,000
 And that's the dhamma for today. Thanks for tuning in.

