 This is my latest project. It's called Ask a Monk. And this
 is something that I think
 I mentioned already. I put it together based on these many
 other Ask Somebody or Ask Somebody.
 And so here's a list of the questions that people have
 asked. And what they do is they just post
 questions and then people can rate them. So they have all
 these questions that they want to ask
 me about Buddhism and meditation and so on. And then I just
 look at them and I see which ones are
 are getting a lot of votes or else I look and see which
 ones I think are appropriate to answer and
 I answer them. And how I answer them is I make YouTube
 videos. So the reply to this, I haven't
 put any responses yet, but I would post a response and then
 just put a YouTube video and a link to
 the YouTube video in here. And then I make the video,
 YouTube video, post it and then put the
 response here. I've just started this and already I've got
 eight questions. You see how many questions?
 Eight questions so far. And 43 votes. So people are voting
 on them as well, which means I better
 get at it and start answering them. Here's the first video.
 I'm the internet slow because I'm
 also uploading my life in a day videos. Okay, so this is
 just one more thing that I do. And that's that.
 [
