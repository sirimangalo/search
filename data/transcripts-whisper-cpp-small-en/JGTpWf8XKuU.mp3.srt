1
00:00:00,000 --> 00:00:05,410
 Go ahead. How come right concentration is always portrayed

2
00:00:05,410 --> 00:00:10,160
 as jhana? Because right concentration is the jhanas.

3
00:00:10,160 --> 00:00:18,800
 But this is an obviously fairly controversial subject.

4
00:00:20,720 --> 00:00:27,050
 But first of all, the question starts with an incorrect

5
00:00:27,050 --> 00:00:35,920
 premise because the jhanas are not always portrayed as the

6
00:00:35,920 --> 00:00:37,520
 jhanas.

7
00:00:37,520 --> 00:00:42,510
 There is the case, at least one place, where the Buddha

8
00:00:42,510 --> 00:00:47,920
 says any single-pointedness of mind that has the ability to

9
00:00:47,920 --> 00:00:52,590
 suppress the hindrances, that is free from the five hindr

10
00:00:52,590 --> 00:00:54,720
ances, this is right concentration.

11
00:00:54,720 --> 00:00:59,330
 So by making that admission, he's obviously, I would say at

12
00:00:59,330 --> 00:01:03,870
 the very least, he's broadening what our understanding of

13
00:01:03,870 --> 00:01:08,560
 the word jhana should be, that it's not this or that state,

14
00:01:08,560 --> 00:01:11,520
 but it's any state that is free from the hindrances.

15
00:01:11,520 --> 00:01:15,650
 Because when you look at ultimate reality, a jhana is still

16
00:01:15,650 --> 00:01:19,960
 only moment-to-moment mind states. So you could enter into

17
00:01:19,960 --> 00:01:24,320
 something that could be considered a jhana in one moment.

18
00:01:24,320 --> 00:01:25,280
 That moment could be a wholesome moment.

19
00:01:25,280 --> 00:01:30,910
 If your mind has wholesomeness in it, you could say, well,

20
00:01:30,910 --> 00:01:36,670
 you've entered into a jhana. In an ultimate sense, it boils

21
00:01:36,670 --> 00:01:41,120
 down to what's the components of that mind.

22
00:01:41,120 --> 00:01:44,360
 How the path works is, well, there are obviously different

23
00:01:44,360 --> 00:01:46,500
 ways. If you read the suttas, you can see the Buddha

24
00:01:46,500 --> 00:01:49,430
 explaining this, that some people start with heavy

25
00:01:49,430 --> 00:01:51,120
 concentrations or not.

26
00:01:51,120 --> 00:01:55,070
 They have strong concentrations, so they enter into what

27
00:01:55,070 --> 00:01:57,760
 could clearly be considered a jhana.

28
00:01:57,760 --> 00:02:00,420
 And then after that, they either take the jhana factors,

29
00:02:00,420 --> 00:02:03,120
 they start to pull it apart and they can feel the happiness

30
00:02:03,120 --> 00:02:05,880
 and they become aware of the happiness as being impermanent

31
00:02:05,880 --> 00:02:07,360
, suffering and non-self.

32
00:02:07,360 --> 00:02:11,100
 And then they let go, they're able to let go as a result.

33
00:02:11,100 --> 00:02:14,920
 Either that or they go back and look at how the jhana is

34
00:02:14,920 --> 00:02:16,880
 based on the rupa and nama.

35
00:02:16,880 --> 00:02:20,140
 So if it's mindfulness and breathing, they become aware of

36
00:02:20,140 --> 00:02:23,120
 the body movements of the body, the stomach rising and

37
00:02:23,120 --> 00:02:23,920
 falling.

38
00:02:23,920 --> 00:02:27,480
 So this is one way is to go with samatha first and then go

39
00:02:27,480 --> 00:02:28,960
 on with vipassana.

40
00:02:28,960 --> 00:02:31,960
 If you're practicing vipassana, then it's kind of like they

41
00:02:31,960 --> 00:02:35,460
 develop together instead of first developing samatha and

42
00:02:35,460 --> 00:02:36,480
 then vipassana.

43
00:02:36,480 --> 00:02:40,190
 They come together and the jhana moment, because see the

44
00:02:40,190 --> 00:02:43,520
 eightfold noble path technically is only one moment.

45
00:02:43,520 --> 00:02:47,440
 That's magganyana, which is only one thought moment.

46
00:02:47,440 --> 00:02:51,620
 The eightfold noble path in a technical sense is only one

47
00:02:51,620 --> 00:02:56,590
 moment. It's the path moment and that moment is what

48
00:02:56,590 --> 00:03:00,200
 separates the mind, which breaks the mind away, where the

49
00:03:00,200 --> 00:03:03,520
 mind breaks away, pulls away from samsara.

50
00:03:03,520 --> 00:03:08,090
 And then the next moment is palanyana, where the mind is in

51
00:03:08,090 --> 00:03:09,120
 nirvana.

52
00:03:09,120 --> 00:03:15,200
 So at that path moment, that's jhana. It's either the first

53
00:03:15,200 --> 00:03:16,700
 jhana, the second jhana, the third jhana or the fourth jh

54
00:03:16,700 --> 00:03:17,040
ana.

55
00:03:17,040 --> 00:03:21,600
 But it's a lokutra jhana. Everyone has to enter into jhana.

56
00:03:21,600 --> 00:03:24,350
 There's no way to go to nibhana without entering into the j

57
00:03:24,350 --> 00:03:28,800
hana, but that's the ultimate extreme.

58
00:03:28,800 --> 00:03:33,640
 At the very least, at that moment, you have to enter into j

59
00:03:33,640 --> 00:03:34,400
hana.

60
00:03:34,400 --> 00:03:37,310
 So consider, either way there's a build up to jhana,

61
00:03:37,310 --> 00:03:40,980
 whether you build up to jhana first and then develop wisdom

62
00:03:40,980 --> 00:03:43,440
, or you build up to jhana until that very moment when

63
00:03:43,440 --> 00:03:49,250
 wisdom and concentration are both perfect and morality as

64
00:03:49,250 --> 00:03:52,760
 well, where morality, concentration and wisdom come to

65
00:03:52,760 --> 00:03:56,820
 perfection at the same time and then enter into jhana at

66
00:03:56,820 --> 00:03:57,840
 that moment.

67
00:03:57,840 --> 00:04:02,900
 So it's misleading for someone to say that one way teaches

68
00:04:02,900 --> 00:04:07,960
 that you can enter into nibhana without jhana, and another

69
00:04:07,960 --> 00:04:13,800
 way teaches that you have to enter into jhana first or so

70
00:04:13,800 --> 00:04:14,320
 on.

71
00:04:14,320 --> 00:04:17,260
 The texts are quite clear that first of all, there are many

72
00:04:17,260 --> 00:04:19,920
 ways of developing one first and then the other.

73
00:04:19,920 --> 00:04:23,270
 First you gain wisdom and then your mind quiets down, or

74
00:04:23,270 --> 00:04:26,390
 first your mind quiets down and then there's wisdom, or you

75
00:04:26,390 --> 00:04:27,760
 do them together or so on.

76
00:04:27,760 --> 00:04:31,200
 But in the end, at that moment, everyone enters into jhana.

77
00:04:31,200 --> 00:04:35,620
 It's just not a samatha jhana, and it's also not a vipass

78
00:04:35,620 --> 00:04:38,650
ana jhana, because the commentaries understand there to be

79
00:04:38,650 --> 00:04:39,760
 three types of jhana.

80
00:04:39,760 --> 00:04:43,550
 Actually samatha jhana, vipassana jhana and lokutta jhana,

81
00:04:43,550 --> 00:04:47,130
 a supermandane. Supermandane meaning having nibhana as its

82
00:04:47,130 --> 00:04:47,760
 object.

83
00:04:47,760 --> 00:04:51,350
 So at that moment, one enters into a lokutta jhana. That's

84
00:04:51,350 --> 00:04:54,700
 why right concentration, that's probably the easiest

85
00:04:54,700 --> 00:04:57,580
 explanation as to why the eightfold noble path

86
00:04:57,580 --> 00:05:01,760
 concentration factor is considered to be the jhanas because

87
00:05:01,760 --> 00:05:05,760
 it's a lokutta jhana that is being talked about.

88
00:05:05,760 --> 00:05:08,630
 The eightfold noble path can also be understood as a pup

89
00:05:08,630 --> 00:05:11,810
anga-manga, the precursor path, which is the path that you

90
00:05:11,810 --> 00:05:13,760
're developing up to that point.

91
00:05:13,760 --> 00:05:15,990
 But all you're doing up to that point is developing the

92
00:05:15,990 --> 00:05:19,480
 eight factors. I think we talked about this before, that

93
00:05:19,480 --> 00:05:21,760
 developing morality and seclusion or so on.

94
00:05:21,760 --> 00:05:24,900
 You can do that, but eventually they all have to come

95
00:05:24,900 --> 00:05:29,540
 together. It's called magasamagi, which means they become

96
00:05:29,540 --> 00:05:30,760
 harmonious.

97
00:05:30,760 --> 00:05:33,940
 So in the beginning someone may have strong morality, but

98
00:05:33,940 --> 00:05:37,150
 weak wisdom and weak concentration or developing one or the

99
00:05:37,150 --> 00:05:37,760
 other.

100
00:05:37,760 --> 00:05:40,670
 Eventually they all have to come together. So the pupanga-m

101
00:05:40,670 --> 00:05:43,860
anga, you can enter into samatha jhana, you can enter into v

102
00:05:43,860 --> 00:05:47,980
ipassana jhana, back and forth, as long as you're

103
00:05:47,980 --> 00:05:52,190
 cultivating all eight of these qualities until the point

104
00:05:52,190 --> 00:05:55,760
 where you enter into a lokutta jhana.

105
00:05:55,760 --> 00:06:02,760
 The quote that I gave actually may have been incorrect

106
00:06:02,760 --> 00:06:07,400
 because I think there's a quote of that sort, but the quote

107
00:06:07,400 --> 00:06:08,760
 I'm thinking of actually says,

108
00:06:08,760 --> 00:06:11,220
 "Right concentration is any concentration that is

109
00:06:11,220 --> 00:06:14,420
 accompanied with the other seven path factors." I'm sorry,

110
00:06:14,420 --> 00:06:15,760
 that's what it says.

111
00:06:15,760 --> 00:06:18,690
 But the commentaries are all talking about how the five

112
00:06:18,690 --> 00:06:20,760
 hindrances have to be suppressed.

113
00:06:20,760 --> 00:06:24,500
 I believe the sutta says, the one that I'm thinking of says

114
00:06:24,500 --> 00:06:26,290
, "It has to be accompanied with the other seven path

115
00:06:26,290 --> 00:06:26,760
 factors."

116
00:06:26,760 --> 00:06:55,760
 If it's accompanied by these seven factors,

117
00:06:55,760 --> 00:07:01,290
 I just counted off eight there, seven factors, then it's

118
00:07:01,290 --> 00:07:02,760
 right concentration.

119
00:07:02,760 --> 00:07:06,760
 So samatha jhana actually is not right concentration.

120
00:07:06,760 --> 00:07:10,390
 If you're talking about the jhanas that arise according to

121
00:07:10,390 --> 00:07:13,760
 the visuddhi-manga when it talks about samatha meditation,

122
00:07:13,760 --> 00:07:17,300
 that's not right concentration in this sense because it

123
00:07:17,300 --> 00:07:18,760
 doesn't have the other seven factors.

124
00:07:18,760 --> 00:07:24,760
 Samatha jhana doesn't have the rest of these perfected.

125
00:07:24,760 --> 00:07:27,880
 Otherwise, a person who practices samatha alone would enter

126
00:07:27,880 --> 00:07:28,760
 into nirvana.

127
00:07:28,760 --> 00:07:35,430
 This is a lokya jhana. Only at that moment of maga nyana

128
00:07:35,430 --> 00:07:39,760
 are all eight path factors fulfilled.

129
00:07:39,760 --> 00:07:43,340
 I don't know if you have anything to add to that. It's a

130
00:07:43,340 --> 00:07:44,760
 fairly technical question.

131
00:07:44,760 --> 00:07:45,760
 No?

