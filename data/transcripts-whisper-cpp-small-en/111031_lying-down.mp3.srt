1
00:00:00,000 --> 00:00:04,860
 When I meditate I usually do it laying down. I do it this

2
00:00:04,860 --> 00:00:07,000
 way because I like lots of concentration.

3
00:00:07,000 --> 00:00:13,190
 Sometimes I do change positions about of need. Out of need?

4
00:00:13,190 --> 00:00:15,000
 Is it a bad thing I do it this way?

5
00:00:15,000 --> 00:00:19,370
 I think it's got a problem. The first key is the part where

6
00:00:19,370 --> 00:00:23,000
 you say because I like lots of concentration.

7
00:00:23,000 --> 00:00:26,680
 This kind of person I would be kind of mean and I would say

8
00:00:26,680 --> 00:00:30,000
 stop lying down because that's what you like to do.

9
00:00:30,000 --> 00:00:33,660
 See we want to see what happens when you do something that

10
00:00:33,660 --> 00:00:35,000
 you don't like to do.

11
00:00:35,000 --> 00:00:38,640
 And we want to show you what you've been building up based

12
00:00:38,640 --> 00:00:40,000
 on your attachments.

13
00:00:40,000 --> 00:00:44,380
 Because an enlightened being can be happy anywhere. So if

14
00:00:44,380 --> 00:00:47,050
 you have preference for something let's see what happens

15
00:00:47,050 --> 00:00:48,000
 when you don't get it.

16
00:00:48,000 --> 00:00:55,000
 That's kind of mean I suppose but it's pertinent.

17
00:00:55,000 --> 00:00:59,940
 Lying down there's no problem with doing lying meditation

18
00:00:59,940 --> 00:01:01,000
 theoretically.

19
00:01:01,000 --> 00:01:06,270
 But in practice it's quite dangerous because it can lead to

20
00:01:06,270 --> 00:01:10,000
 falling asleep, it can lead to daydreaming.

21
00:01:10,000 --> 00:01:14,570
 It's the position where the mind is most accustomed to

22
00:01:14,570 --> 00:01:20,000
 freedom, not having any kind of troubles or difficulties.

23
00:01:20,000 --> 00:01:24,780
 So there's very little potential to develop wisdom or to

24
00:01:24,780 --> 00:01:29,800
 develop renunciation, to develop patience, to develop all

25
00:01:29,800 --> 00:01:31,000
 sorts of good qualities

26
00:01:31,000 --> 00:01:35,330
 and to let go because there's nothing that needs to be let

27
00:01:35,330 --> 00:01:37,000
 go of at that time.

28
00:01:37,000 --> 00:01:40,800
 What you can do is focus on the liking. When you lie down

29
00:01:40,800 --> 00:01:44,000
 it can be quite pleasant and so it can be useful to focus

30
00:01:44,000 --> 00:01:45,000
 on that experience

31
00:01:45,000 --> 00:01:50,000
 and to try to free yourself from the clinging to it.

32
00:01:50,000 --> 00:01:58,260
 Lying is best used for people who have stressful jobs or

33
00:01:58,260 --> 00:02:05,000
 lifestyles who need concentration.

34
00:02:05,000 --> 00:02:10,370
 People who have for example ADHD might benefit a lot from

35
00:02:10,370 --> 00:02:14,200
 doing lying meditation because they need the concentration,

36
00:02:14,200 --> 00:02:16,000
 they need to balance it.

37
00:02:16,000 --> 00:02:19,120
 So if your mind is racing you can try doing lying

38
00:02:19,120 --> 00:02:22,990
 meditation but if you're tired or if you're kind of relaxed

39
00:02:22,990 --> 00:02:27,000
 I would work on the energy side.

40
00:02:27,000 --> 00:02:31,250
 So do walking meditation. Walking meditation is probably

41
00:02:31,250 --> 00:02:34,640
 not as peaceful or comfortable for you but that's really

42
00:02:34,640 --> 00:02:35,000
 the point.

43
00:02:35,000 --> 00:02:40,280
 The point is to see what your mind does when the body is

44
00:02:40,280 --> 00:02:49,000
 not comfortable, when the body has to face difficulties.

45
00:02:49,000 --> 00:02:52,860
 You can sort of see how it's kind of spoiling you because

46
00:02:52,860 --> 00:02:57,000
 the other part is where you talk about changing positions.

47
00:02:57,000 --> 00:03:01,280
 So what you're experiencing is the fact that even though it

48
00:03:01,280 --> 00:03:05,000
's pleasant to lie down it's really not satisfying.

49
00:03:05,000 --> 00:03:11,970
 If you were to lie down for say six hours without not

50
00:03:11,970 --> 00:03:14,630
 asleep, if you were to lie there and meditate for say six

51
00:03:14,630 --> 00:03:17,000
 hours, twelve hours,

52
00:03:17,000 --> 00:03:19,920
 you'll do it for twenty-four hours. I've heard of someone

53
00:03:19,920 --> 00:03:23,000
 doing lying meditation for twenty-four hours.

54
00:03:23,000 --> 00:03:27,160
 It becomes quite difficult to stay in one posture. The

55
00:03:27,160 --> 00:03:29,000
 meaning is that it can't satisfy you.

56
00:03:29,000 --> 00:03:33,080
 It's not really pleasant. You still have to always change

57
00:03:33,080 --> 00:03:36,850
 your posture and find a new posture that's more comfortable

58
00:03:36,850 --> 00:03:38,000
 and then get a nicer pillow

59
00:03:38,000 --> 00:03:43,810
 and switch from an ordinary mattress to a water bed or so

60
00:03:43,810 --> 00:03:46,720
 on and so on and so on. Then it's too hot, then it's too

61
00:03:46,720 --> 00:03:49,000
 cold, then you're thirsty, then you're hungry.

62
00:03:49,000 --> 00:03:53,410
 Then you have to go to the washroom. Then you're bored and

63
00:03:53,410 --> 00:03:57,000
 you want to get up and check Facebook or YouTube.

64
00:04:01,000 --> 00:04:04,890
 Nothing wrong with lying meditation, but you should

65
00:04:04,890 --> 00:04:07,540
 understand how it's used. The fact that you like

66
00:04:07,540 --> 00:04:10,510
 concentration is a bit of a problem because it becomes an

67
00:04:10,510 --> 00:04:11,000
 attachment.

68
00:04:11,000 --> 00:04:14,980
 You should try to let go of that. It would be probably

69
00:04:14,980 --> 00:04:19,000
 better to do some walking meditation.

70
00:04:19,000 --> 00:04:21,610
 You get to see things about your mind that you couldn't see

71
00:04:21,610 --> 00:04:26,370
 before and you'll be able to see your attachments to the

72
00:04:26,370 --> 00:04:28,000
 concentration, for example.

