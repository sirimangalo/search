1
00:00:00,000 --> 00:00:03,780
 Okay, good evening. Welcome back to our study of the Dhamph

2
00:00:03,780 --> 00:00:04,000
ana.

3
00:00:04,000 --> 00:00:12,000
 Today we're looking at verse 171, which reads as follows.

4
00:00:26,000 --> 00:00:34,800
 Which means, "Come look at this world decked out just like

5
00:00:34,800 --> 00:00:38,000
 a king's chariot,

6
00:00:44,000 --> 00:00:56,190
 wherein the fools fall or get caught up in. Not the wise,

7
00:00:56,190 --> 00:01:00,000
 there is no connection for the wise."

8
00:01:05,000 --> 00:01:13,510
 This is another one of these famous verses in the Buddhist

9
00:01:13,510 --> 00:01:15,000
 world.

10
00:01:15,000 --> 00:01:21,000
 So the story, not much of a story, it's in fact a repeat.

11
00:01:21,000 --> 00:01:25,710
 You wonder about these repeats, why it is that two people

12
00:01:25,710 --> 00:01:27,000
 have the same story,

13
00:01:27,000 --> 00:01:34,000
 and it's acknowledged that they have the same story.

14
00:01:34,000 --> 00:01:37,000
 I'm not sure how that works.

15
00:01:37,000 --> 00:01:42,770
 But the story is the same as Santati. Santati was a

16
00:01:42,770 --> 00:01:44,000
 minister of the king,

17
00:01:44,000 --> 00:01:49,000
 and Ambaya Kumar is actually a prince, the son of the king.

18
00:01:49,000 --> 00:01:55,000
 But he pleased the king and the king gave him great reward,

19
00:01:55,000 --> 00:02:00,000
 and part of the reward was a dancing girl.

20
00:02:02,000 --> 00:02:09,000
 So I don't know how old Prince Ambaya was at the time,

21
00:02:09,000 --> 00:02:13,000
 but it may have been his first experience.

22
00:02:13,000 --> 00:02:17,000
 Maybe he was a teenager or something, and he was,

23
00:02:17,000 --> 00:02:20,700
 I assume he's a little older than that, but maybe he was

24
00:02:20,700 --> 00:02:21,000
 young,

25
00:02:21,000 --> 00:02:27,500
 and he fell in love with this dancing girl, this woman who

26
00:02:27,500 --> 00:02:31,000
 had been hired to dance for him.

27
00:02:31,000 --> 00:02:39,440
 And she, if it's the same in the story of Santati and I

28
00:02:39,440 --> 00:02:42,000
 guess in the story of Ambaya as well,

29
00:02:42,000 --> 00:02:47,000
 she didn't take good care of herself.

30
00:02:47,000 --> 00:02:51,000
 There was sort of a strict regimen for dancing women.

31
00:02:51,000 --> 00:02:55,000
 They had to starve themselves, I think, is the...

32
00:02:56,000 --> 00:03:02,000
 Yes, she fasted for seven days to have a graceful body.

33
00:03:02,000 --> 00:03:06,000
 It sounds a bit extreme, but there you have it.

34
00:03:06,000 --> 00:03:09,080
 People do extreme things and are forced into extreme

35
00:03:09,080 --> 00:03:10,000
 situations.

36
00:03:10,000 --> 00:03:16,000
 And so she died, same thing as with Santati.

37
00:03:16,000 --> 00:03:22,000
 And he grieved over that maybe it was a common thing,

38
00:03:22,000 --> 00:03:26,150
 because it was so common for them to starve themselves that

39
00:03:26,150 --> 00:03:30,000
 it was also a common thing for them to drop dead.

40
00:03:30,000 --> 00:03:33,760
 Makes you wonder why, how the system continued when all

41
00:03:33,760 --> 00:03:37,000
 these dancing women kept dying.

42
00:03:37,000 --> 00:03:39,000
 But there you have it.

43
00:03:45,000 --> 00:03:51,230
 And Ambaya immediately thought, he was grieved at the loss

44
00:03:51,230 --> 00:03:53,000
 of this woman.

45
00:03:53,000 --> 00:03:57,000
 He immediately thought of the Buddha.

46
00:03:57,000 --> 00:04:00,950
 He was the son of Bimbisara. Bimbisara was a great patron

47
00:04:00,950 --> 00:04:02,000
 of Buddhism,

48
00:04:02,000 --> 00:04:06,850
 a great supporter of the Buddha, one of the first famous

49
00:04:06,850 --> 00:04:09,000
 lay disciples,

50
00:04:09,000 --> 00:04:16,660
 famously in that time. He was a great patron and sort of

51
00:04:16,660 --> 00:04:22,000
 the first major support of Buddhism in a worldly sense.

52
00:04:22,000 --> 00:04:27,450
 In terms of providing a monastery, he was the one who

53
00:04:27,450 --> 00:04:30,000
 donated the bamboo grove.

54
00:04:30,000 --> 00:04:36,000
 And so immediately Ambaya thought of the Buddha.

55
00:04:36,000 --> 00:04:39,730
 And this is a common thing as well, when the going gets

56
00:04:39,730 --> 00:04:40,000
 tough.

57
00:04:40,000 --> 00:04:47,270
 It's common for ordinary people, ordinary in a neutral

58
00:04:47,270 --> 00:04:48,000
 sense,

59
00:04:48,000 --> 00:04:51,430
 people who had no special thoughts about religion or

60
00:04:51,430 --> 00:04:54,000
 religious practice or meditation.

61
00:04:54,000 --> 00:04:58,660
 It's a common thing for them to suddenly become, you might

62
00:04:58,660 --> 00:05:00,000
 say, exceptional,

63
00:05:00,000 --> 00:05:03,800
 because they do think about things that people normally don

64
00:05:03,800 --> 00:05:05,000
't think about.

65
00:05:05,000 --> 00:05:10,060
 Someone who has suffered a great disturbance in their life

66
00:05:10,060 --> 00:05:13,000
 is more likely to,

67
00:05:13,000 --> 00:05:18,000
 obviously more likely to consider a larger picture,

68
00:05:18,000 --> 00:05:29,000
 or consider a more nuanced or profound answer to life.

69
00:05:29,000 --> 00:05:34,260
 If the going is, if it's going well, if everything is going

70
00:05:34,260 --> 00:05:35,000
 well,

71
00:05:35,000 --> 00:05:40,990
 we're more likely to be content with a simple explanation

72
00:05:40,990 --> 00:05:46,000
 or simple answers, a simple philosophy.

73
00:05:46,000 --> 00:05:52,000
 The simplest is try your best to always get what you want.

74
00:05:52,000 --> 00:05:55,000
 And of course in cases like this, it becomes clear that

75
00:05:55,000 --> 00:05:58,000
 that's not only not possible,

76
00:05:58,000 --> 00:06:02,700
 but highly problematic. It's a cause of great stress and

77
00:06:02,700 --> 00:06:04,000
 suffering.

78
00:06:04,000 --> 00:06:09,000
 And so when you lose something that you hold dear,

79
00:06:09,000 --> 00:06:13,000
 especially when that something is actually a someone,

80
00:06:13,000 --> 00:06:21,410
 which makes it so much more stressful, more upsetting, to

81
00:06:21,410 --> 00:06:24,000
 put it lightly.

82
00:06:24,000 --> 00:06:32,580
 When this happens, it's a real cause for reflection and

83
00:06:32,580 --> 00:06:34,000
 questioning,

84
00:06:34,000 --> 00:06:41,210
 questioning your outlook, realizing that it's not really

85
00:06:41,210 --> 00:06:43,000
 going to work.

86
00:06:43,000 --> 00:06:46,300
 This is a bad choice of philosophy, and so you ask

87
00:06:46,300 --> 00:06:47,000
 questions.

88
00:06:47,000 --> 00:06:51,000
 And so it's the sort of thing that obviously it happens,

89
00:06:51,000 --> 00:06:57,000
 and it's a major cause for people to look towards religion,

90
00:06:57,000 --> 00:07:04,000
 or more appropriately, meditation and mental development,

91
00:07:04,000 --> 00:07:08,740
 realizing that their minds are ill-equipped to deal with

92
00:07:08,740 --> 00:07:10,000
 suffering.

93
00:07:10,000 --> 00:07:14,810
 And so Advaya thought, now's a good time to go see the

94
00:07:14,810 --> 00:07:16,000
 Buddha.

95
00:07:16,000 --> 00:07:22,000
 And the Buddha said to him, "Yes, it's true.

96
00:07:22,000 --> 00:07:25,110
 This was a great loss for a human being to lose another

97
00:07:25,110 --> 00:07:30,000
 human being when they held dear."

98
00:07:30,000 --> 00:07:35,390
 But the truth is, this has been happening since time immem

99
00:07:35,390 --> 00:07:36,000
orial.

100
00:07:36,000 --> 00:07:43,000
 Every lifetime we cry over loss.

101
00:07:43,000 --> 00:07:55,000
 How many tears we shed if you think in terms of the,

102
00:07:55,000 --> 00:08:03,000
 or within the framework of Buddhist cosmology or theory,

103
00:08:03,000 --> 00:08:08,000
 with the idea that we're reborn through infinity,

104
00:08:08,000 --> 00:08:13,000
 with no discernible beginning or end,

105
00:08:13,000 --> 00:08:17,370
 then the number of tears that we've shed is greater than

106
00:08:17,370 --> 00:08:20,000
 all the water and all the ocean.

107
00:08:20,000 --> 00:08:23,000
 Imagine that.

108
00:08:23,000 --> 00:08:26,000
 Think of how many tears you shed in this life so far.

109
00:08:26,000 --> 00:08:29,000
 How many tears have I shed so far?

110
00:08:29,000 --> 00:08:33,000
 I wonder how much that would be. Would it fill a cup?

111
00:08:33,000 --> 00:08:36,000
 Let's see.

112
00:08:36,000 --> 00:08:39,000
 One cup of tears. How many?

113
00:08:39,000 --> 00:08:42,000
 How much sadness does it take for one cup of tears?

114
00:08:42,000 --> 00:08:49,000
 Probably more than that, even in my short life.

115
00:08:49,000 --> 00:08:53,000
 Probably more than a cup. But let's say one cup.

116
00:08:53,000 --> 00:08:57,070
 I don't know. We'll just guess. The average person cries

117
00:08:57,070 --> 00:08:58,000
 one cup.

118
00:08:58,000 --> 00:09:03,000
 Does that seem like not enough? Two cups? Ten cups?

119
00:09:03,000 --> 00:09:06,000
 How many cups of tears do you cry in a lifetime?

120
00:09:06,000 --> 00:09:09,000
 It's a very good question, I think.

121
00:09:09,000 --> 00:09:14,000
 You may not think of it, but if you think, wow,

122
00:09:14,000 --> 00:09:19,000
 those tears represent, it's not such an important question,

123
00:09:19,000 --> 00:09:29,000
 but in the context it's important because then,

124
00:09:29,000 --> 00:09:33,000
 think of an ocean full of tears.

125
00:09:33,000 --> 00:09:38,000
 How much sadness is required for an ocean?

126
00:09:38,000 --> 00:09:41,000
 Well, how much sadness is required for a small pond?

127
00:09:41,000 --> 00:09:44,380
 It's already quite impressive, but imagine an ocean of

128
00:09:44,380 --> 00:09:45,000
 tears.

129
00:09:45,000 --> 00:09:50,000
 How much sadness? And that's how much, that's less than,

130
00:09:50,000 --> 00:09:54,400
 that still doesn't even approach the amount of sadness that

131
00:09:54,400 --> 00:09:56,000
 we've gone through.

132
00:09:56,000 --> 00:09:59,000
 Lifetime after lifetime after lifetime.

133
00:09:59,000 --> 00:10:02,000
 Which isn't supposed to be depressing.

134
00:10:02,000 --> 00:10:05,000
 It's in fact supposed to be liberating to make you realize,

135
00:10:05,000 --> 00:10:07,000
 what are we doing?

136
00:10:07,000 --> 00:10:10,000
 Is it really going to help me to keep doing this?

137
00:10:10,000 --> 00:10:14,000
 Is this really the way to approach life?

138
00:10:14,000 --> 00:10:18,000
 Crying about, well, or not just crying about,

139
00:10:18,000 --> 00:10:23,000
 but putting myself in a situation where I'm going to cry

140
00:10:23,000 --> 00:10:23,000
 about,

141
00:10:23,000 --> 00:10:26,000
 where I'm liable to tears.

142
00:10:26,000 --> 00:10:29,000
 And what is said, basically this, he said,

143
00:10:29,000 --> 00:10:33,000
 you've cried so much, lifetime after lifetime,

144
00:10:33,000 --> 00:10:37,000
 over this woman you've cried, every lifetime.

145
00:10:37,000 --> 00:10:43,000
 And then he says, only foolish people.

146
00:10:43,000 --> 00:10:47,000
 And it's okay because we're all foolish people,

147
00:10:47,000 --> 00:10:49,000
 that's how we start in life.

148
00:10:49,000 --> 00:10:53,480
 It's okay, this isn't meant to be critical, or it's

149
00:10:53,480 --> 00:10:54,000
 critical,

150
00:10:54,000 --> 00:10:57,000
 but it's not something we should take personally.

151
00:10:57,000 --> 00:10:59,000
 Not something he should take personally.

152
00:10:59,000 --> 00:11:02,000
 He said, don't be a fool basically.

153
00:11:02,000 --> 00:11:04,000
 Maasuchi, don't cry.

154
00:11:04,000 --> 00:11:11,000
 Bala jnanam, sang siddana tan amitam.

155
00:11:11,000 --> 00:11:18,000
 What does that mean?

156
00:11:18,000 --> 00:11:23,000
 Sea of grief, right?

157
00:11:23,000 --> 00:11:25,000
 Bala jnanam.

158
00:11:25,000 --> 00:11:28,000
 Anyway, this is the sea of grief,

159
00:11:28,000 --> 00:11:33,000
 this ocean of grief is the realm of fools.

160
00:11:33,000 --> 00:11:36,000
 And then he taught this verse.

161
00:11:36,000 --> 00:11:39,000
 So it's a famous verse, I think,

162
00:11:39,000 --> 00:11:44,000
 it's one that I know quite well.

163
00:11:44,000 --> 00:11:48,000
 It's quite powerful, the imagery of the royal chariot.

164
00:11:48,000 --> 00:11:51,000
 This is how we see the world.

165
00:11:51,000 --> 00:11:55,000
 This is the epitome of intoxication,

166
00:11:55,000 --> 00:12:01,000
 of investment, excitement.

167
00:12:01,000 --> 00:12:03,000
 We get so invested in things.

168
00:12:03,000 --> 00:12:08,000
 When we're born, we begin to make sense of the world,

169
00:12:08,000 --> 00:12:11,000
 and then we're presented with all these bright and colorful

170
00:12:11,000 --> 00:12:15,000
 and wonderful sensations,

171
00:12:15,000 --> 00:12:19,000
 and we quickly learn pleasure and pain

172
00:12:19,000 --> 00:12:27,000
 and are encouraged and latch on to the concept of clinging,

173
00:12:27,000 --> 00:12:32,000
 the concept of chasing after,

174
00:12:32,000 --> 00:12:37,000
 of seeking out what will make you happy.

175
00:12:37,000 --> 00:12:41,000
 So as we grow up, it's our toys,

176
00:12:41,000 --> 00:12:44,000
 and remember a kid when you get your first toy

177
00:12:44,000 --> 00:12:47,000
 and then your next toy,

178
00:12:47,000 --> 00:12:50,000
 and then when you hear that Santa Claus gives you toys

179
00:12:50,000 --> 00:12:54,000
 and then you send away a letter to this hole,

180
00:12:54,000 --> 00:12:57,230
 Santa Claus thing, again coming back to it, it's all

181
00:12:57,230 --> 00:12:59,000
 rubbish.

182
00:12:59,000 --> 00:13:02,000
 It's the wrong way of going about things.

183
00:13:02,000 --> 00:13:06,000
 It's nice that we want to give things to our loved ones,

184
00:13:06,000 --> 00:13:09,000
 we want to share with them,

185
00:13:09,000 --> 00:13:14,000
 but really we should be much more concerned about helping

186
00:13:14,000 --> 00:13:14,000
 each other

187
00:13:14,000 --> 00:13:17,000
 and giving each other what we need

188
00:13:17,000 --> 00:13:19,000
 and giving each other what we want,

189
00:13:19,000 --> 00:13:22,000
 or encouraging, especially in children,

190
00:13:22,000 --> 00:13:32,000
 encouraging the desire for possessions.

191
00:13:32,000 --> 00:13:35,000
 It's funny, you know, I mean Christmas isn't,

192
00:13:35,000 --> 00:13:38,000
 anyway, I don't want to get too sidetracked,

193
00:13:38,000 --> 00:13:41,000
 but it's only a Christian concept, I think.

194
00:13:41,000 --> 00:13:45,000
 You go to Thailand, for example,

195
00:13:45,000 --> 00:13:50,000
 and they talk about Christmas, but they don't celebrate it,

196
00:13:50,000 --> 00:13:54,000
 they've started to a little bit, but not in the same way.

197
00:13:54,000 --> 00:13:57,000
 And growing up Jewish, it was, you know,

198
00:13:57,000 --> 00:13:59,000
 maybe you got some money for Hanukkah,

199
00:13:59,000 --> 00:14:03,000
 just because all the Christian kids were getting gifts,

200
00:14:03,000 --> 00:14:12,000
 but anyway, the point, why this is interesting,

201
00:14:12,000 --> 00:14:17,000
 is because this cultivation and this encouragement

202
00:14:17,000 --> 00:14:22,630
 of excitement over possession, over belongings, over the

203
00:14:22,630 --> 00:14:24,000
 world,

204
00:14:24,000 --> 00:14:27,000
 wanting this, wanting that,

205
00:14:27,000 --> 00:14:32,000
 it starts with toys, games,

206
00:14:32,000 --> 00:14:38,000
 and then we hit puberty, and then it becomes sexuality,

207
00:14:38,000 --> 00:14:43,000
 objects of sexuality, the human body,

208
00:14:43,000 --> 00:14:46,000
 becomes a huge attraction,

209
00:14:46,000 --> 00:14:49,000
 and that's where we find Abaya,

210
00:14:49,000 --> 00:14:55,000
 fell into this realm of intense attraction,

211
00:14:55,000 --> 00:14:58,000
 that is, must be so ingrained in us.

212
00:14:58,000 --> 00:15:03,000
 Think about how many times you've had sexual intercourse,

213
00:15:03,000 --> 00:15:06,000
 lifetime after lifetime after lifetime,

214
00:15:06,000 --> 00:15:14,000
 how refined our sense of appreciation of sexual pleasure,

215
00:15:14,000 --> 00:15:18,340
 or sexual stimulation, or physical, yeah, sexual

216
00:15:18,340 --> 00:15:19,000
 stimulation,

217
00:15:19,000 --> 00:15:22,000
 lifetime after lifetime after lifetime,

218
00:15:22,000 --> 00:15:25,650
 that we just jump right into it as soon as the body's ready

219
00:15:25,650 --> 00:15:26,000
.

220
00:15:26,000 --> 00:15:28,000
 It's like we're just waiting for puberty,

221
00:15:28,000 --> 00:15:32,000
 and then puberty comes, "Okay, I remember this."

222
00:15:32,000 --> 00:15:38,000
 It's like you planted a plant,

223
00:15:38,000 --> 00:15:43,000
 and now it's grown fruit, and now you can eat the fruit,

224
00:15:43,000 --> 00:15:45,000
 because we just jump right into it,

225
00:15:45,000 --> 00:15:51,000
 immediately forming romances and falling in love,

226
00:15:51,000 --> 00:15:54,000
 and being taught that this is life, this is the world.

227
00:15:54,000 --> 00:15:57,000
 We teach each other, we teach our children,

228
00:15:57,000 --> 00:16:01,000
 our society is composed of this,

229
00:16:01,000 --> 00:16:04,000
 and society survives.

230
00:16:04,000 --> 00:16:08,000
 Our culture, our religion, our nationality depends upon it.

231
00:16:08,000 --> 00:16:13,000
 If people stopped having romance and sexual intercourse,

232
00:16:13,000 --> 00:16:21,620
 you know, it helps us grow and populate and become more

233
00:16:21,620 --> 00:16:23,000
 powerful.

234
00:16:23,000 --> 00:16:27,000
 They talk about having sons, well, having children,

235
00:16:27,000 --> 00:16:30,000
 but it's always generally been sons,

236
00:16:30,000 --> 00:16:35,000
 because physical strength, I guess, was a thing.

237
00:16:35,000 --> 00:16:40,720
 And so sons were generally more physically strong, whatever

238
00:16:40,720 --> 00:16:41,000
.

239
00:16:41,000 --> 00:16:47,000
 And so it becomes a narrative that we teach ourselves,

240
00:16:47,000 --> 00:16:52,000
 yes, fall in love and get married,

241
00:16:52,000 --> 00:17:02,000
 and all these customs we have, like marriage and so on.

242
00:17:02,000 --> 00:17:06,000
 So we become caught up in so many ways.

243
00:17:06,000 --> 00:17:09,000
 Romance, as with this story, is the biggest one,

244
00:17:09,000 --> 00:17:15,000
 and we're becoming so attracted to worldly pleasures,

245
00:17:15,000 --> 00:17:17,000
 that there are so many.

246
00:17:17,000 --> 00:17:20,000
 Some people become attached to their car,

247
00:17:20,000 --> 00:17:24,360
 and you have this beautiful new car, and then it gets in an

248
00:17:24,360 --> 00:17:25,000
 accident.

249
00:17:25,000 --> 00:17:27,000
 We get attached to our children,

250
00:17:27,000 --> 00:17:31,000
 how strong is the attachment of parents to their children?

251
00:17:31,000 --> 00:17:39,000
 And then when your child changes, simply changes,

252
00:17:39,000 --> 00:17:43,000
 not even gets sick or dies, of course that happens as well,

253
00:17:43,000 --> 00:17:45,000
 but simply changes.

254
00:17:45,000 --> 00:17:49,390
 How angry, how hard it is for us to deal with when our

255
00:17:49,390 --> 00:17:51,000
 children

256
00:17:51,000 --> 00:17:56,000
 no longer look up to us or respect us,

257
00:17:56,000 --> 00:17:59,000
 or if they become wicked and bad people,

258
00:17:59,000 --> 00:18:03,080
 how hurtful it is, how hurtful it can be when our children

259
00:18:03,080 --> 00:18:04,000
 are reckless,

260
00:18:04,000 --> 00:18:06,000
 when our children hurt themselves,

261
00:18:06,000 --> 00:18:09,700
 when our children refuse to follow the path we set out for

262
00:18:09,700 --> 00:18:10,000
 them,

263
00:18:10,000 --> 00:18:13,000
 how angry we become.

264
00:18:13,000 --> 00:18:17,290
 We could talk for days about the ways we get caught up in

265
00:18:17,290 --> 00:18:18,000
 the world,

266
00:18:18,000 --> 00:18:22,000
 we get caught up in these narratives and these stories,

267
00:18:22,000 --> 00:18:26,000
 and often just caught up in the shininess of it all,

268
00:18:26,000 --> 00:18:28,000
 how beautiful it all is.

269
00:18:28,000 --> 00:18:31,980
 So in this case, it may very well have been the case that

270
00:18:31,980 --> 00:18:33,000
 this,

271
00:18:33,000 --> 00:18:36,000
 you have to ask Abhaya, "Did he really know this woman?"

272
00:18:36,000 --> 00:18:38,850
 It's interesting, we never hear the story of the dancing

273
00:18:38,850 --> 00:18:39,000
 girl,

274
00:18:39,000 --> 00:18:42,000
 what was she like?

275
00:18:42,000 --> 00:18:44,000
 We don't hear anything about her personality,

276
00:18:44,000 --> 00:18:46,000
 just that she, what is she?

277
00:18:46,000 --> 00:18:50,430
 She was a woman who danced, that's what was important about

278
00:18:50,430 --> 00:18:51,000
 her.

279
00:18:51,000 --> 00:18:53,000
 That's all we hear.

280
00:18:53,000 --> 00:18:56,000
 And so it's quite likely that that's all he knew.

281
00:18:56,000 --> 00:18:59,000
 He didn't know her personality or her likes or dislikes,

282
00:18:59,000 --> 00:19:02,000
 he was just caught up in,

283
00:19:02,000 --> 00:19:04,000
 he was in love with her body most likely,

284
00:19:04,000 --> 00:19:13,000
 and her skill in the way of dancing to tempt the eye,

285
00:19:13,000 --> 00:19:17,000
 and to be suggestive, to tempt the mind.

286
00:19:17,000 --> 00:19:22,370
 There are ways of dancing that are seductive, we would call

287
00:19:22,370 --> 00:19:23,000
 it.

288
00:19:23,000 --> 00:19:29,000
 So he was seduced quite ably by this woman.

289
00:19:29,000 --> 00:19:40,000
 And so this is what this verse is about.

290
00:19:40,000 --> 00:19:43,000
 So the basic lesson is talking about,

291
00:19:43,000 --> 00:19:46,000
 I think it's important to talk about how we get caught up,

292
00:19:46,000 --> 00:19:48,000
 how we fall into the world.

293
00:19:48,000 --> 00:19:53,000
 We see dantir, which I think means fall, sink down,

294
00:19:53,000 --> 00:19:57,000
 get sunk in the world, mired in it.

295
00:19:57,000 --> 00:20:01,000
 So caught up, we get spun around and then we get old

296
00:20:01,000 --> 00:20:05,000
 and wonder how we got here in the first place.

297
00:20:05,000 --> 00:20:09,820
 But by then it's time to die and then we do it all over

298
00:20:09,820 --> 00:20:11,000
 again.

299
00:20:11,000 --> 00:20:17,000
 And the second part of the lesson is,

300
00:20:17,000 --> 00:20:21,000
 which I think is equally important,

301
00:20:21,000 --> 00:20:23,000
 especially for us as meditators,

302
00:20:23,000 --> 00:20:27,000
 that the eyes don't have any connection with the world.

303
00:20:27,000 --> 00:20:29,000
 And that's a simple statement.

304
00:20:29,000 --> 00:20:32,000
 You can take it in a conventional sense

305
00:20:32,000 --> 00:20:37,380
 to just be a supporter of the Buddhist philosophy, not to

306
00:20:37,380 --> 00:20:38,000
 cling.

307
00:20:38,000 --> 00:20:45,000
 But I think it's important to stress exactly how,

308
00:20:45,000 --> 00:20:47,000
 I mean this takes special importance in Buddhism,

309
00:20:47,000 --> 00:20:54,000
 which places such a high emphasis on wisdom.

310
00:20:54,000 --> 00:20:57,000
 And the point is that it's not because you agree with the

311
00:20:57,000 --> 00:20:57,000
 Buddha

312
00:20:57,000 --> 00:20:59,000
 that it makes you wise.

313
00:20:59,000 --> 00:21:04,000
 The point is that people, that clinging to things

314
00:21:04,000 --> 00:21:10,000
 is not just because you've decided to make that your life

315
00:21:10,000 --> 00:21:14,000
 and you've decided that that's your path,

316
00:21:14,000 --> 00:21:16,000
 it's that you don't understand it.

317
00:21:16,000 --> 00:21:22,000
 We cling to things because we don't understand them.

318
00:21:22,000 --> 00:21:24,000
 It's not that you haven't studied Buddhism

319
00:21:24,000 --> 00:21:31,000
 or you have to learn that the Buddha said it's wrong.

320
00:21:31,000 --> 00:21:33,400
 But a desire for a thing is based on a lack of

321
00:21:33,400 --> 00:21:35,000
 understanding of that thing,

322
00:21:35,000 --> 00:21:39,320
 proper understanding, objective understanding, not Buddhist

323
00:21:39,320 --> 00:21:41,000
 understanding.

324
00:21:41,000 --> 00:21:47,000
 And so this is one way of describing the very core of

325
00:21:47,000 --> 00:21:51,000
 Buddhist mental development.

326
00:21:51,000 --> 00:21:56,500
 It's not to take on beliefs or views that it's wrong to

327
00:21:56,500 --> 00:21:57,000
 cling.

328
00:21:57,000 --> 00:22:01,560
 It's about studying, objectively studying the things that

329
00:22:01,560 --> 00:22:02,000
 you cling to,

330
00:22:02,000 --> 00:22:09,000
 studying your clinging, studying your desire for things.

331
00:22:09,000 --> 00:22:12,000
 And coming to the logical conclusion, which happens to be

332
00:22:12,000 --> 00:22:13,000
 that they're not worth clinging to.

333
00:22:13,000 --> 00:22:17,000
 There's nothing in the world worth clinging to.

334
00:22:17,000 --> 00:22:21,610
 It's not something intellectual, it's something you have to

335
00:22:21,610 --> 00:22:24,000
 spend time

336
00:22:24,000 --> 00:22:30,000
 debating with yourself or analyzing rationally.

337
00:22:30,000 --> 00:22:32,510
 It's something that you see quite clearly through the

338
00:22:32,510 --> 00:22:33,000
 practice.

339
00:22:33,000 --> 00:22:39,000
 From watching yourself engage in desire watching yourself.

340
00:22:39,000 --> 00:22:42,000
 You acquire the things you desire.

341
00:22:42,000 --> 00:22:46,000
 Observing mindfully the process of clinging

342
00:22:46,000 --> 00:22:50,000
 shows you without any doubt that it's not worth it.

343
00:22:50,000 --> 00:22:53,000
 It's not worth clinging to.

344
00:22:53,000 --> 00:22:57,000
 This is what is meant by the wise to have no connection.

345
00:22:57,000 --> 00:23:02,000
 It's not belief, it's not conviction, it's not effort.

346
00:23:02,000 --> 00:23:05,000
 It's not about pushing yourself so hard that you break free

347
00:23:05,000 --> 00:23:05,000
.

348
00:23:05,000 --> 00:23:08,000
 It's not about breaking your habits.

349
00:23:08,000 --> 00:23:11,000
 It's about studying your habits, analyzing them.

350
00:23:11,000 --> 00:23:19,180
 Analyzing them by means methodically observing them again

351
00:23:19,180 --> 00:23:23,000
 and again.

352
00:23:23,000 --> 00:23:31,000
 If you're intent enough upon that sort of practice,

353
00:23:31,000 --> 00:23:35,000
 you'll see for yourself the nature of reality.

354
00:23:35,000 --> 00:23:38,000
 You'll become free from suffering.

355
00:23:38,000 --> 00:23:40,430
 The idea is that it's not the world that's causing us

356
00:23:40,430 --> 00:23:41,000
 suffering,

357
00:23:41,000 --> 00:23:45,000
 it's not the loss of a loved one that causes us suffering.

358
00:23:45,000 --> 00:23:52,940
 It's our reaction to our experience, it's our investment in

359
00:23:52,940 --> 00:23:54,000
 them.

360
00:23:54,000 --> 00:23:58,810
 It's not even just our reaction, it's our setting ourselves

361
00:23:58,810 --> 00:24:00,000
 up to react.

362
00:24:00,000 --> 00:24:05,000
 That's the thing, when sadness comes, you can't say,

363
00:24:05,000 --> 00:24:09,270
 "Okay, I know this is going to be a bad thing, so I'm just

364
00:24:09,270 --> 00:24:10,000
 not going to react this time."

365
00:24:10,000 --> 00:24:12,000
 You can't do that.

366
00:24:12,000 --> 00:24:16,470
 You can't say, "No, no. This thing I loved, yes, reacting

367
00:24:16,470 --> 00:24:18,000
 to its loss would be bad,

368
00:24:18,000 --> 00:24:20,000
 I'm not going to do that."

369
00:24:20,000 --> 00:24:23,000
 It doesn't even work that way.

370
00:24:23,000 --> 00:24:26,350
 You'll become overwhelmed by it, and that's the nature of

371
00:24:26,350 --> 00:24:27,000
 clinging.

372
00:24:27,000 --> 00:24:31,000
 So the learning process is learning this truth,

373
00:24:31,000 --> 00:24:36,710
 that clinging is not to your benefit, it doesn't lead to

374
00:24:36,710 --> 00:24:38,000
 greater good,

375
00:24:38,000 --> 00:24:45,000
 it leads only to stress and suffering.

376
00:24:45,000 --> 00:24:56,240
 So that's the benefit of what a wisdom, a wise person doesn

377
00:24:56,240 --> 00:24:58,000
't suffer quite simply.

378
00:24:58,000 --> 00:25:05,180
 They free themselves, no more tears, free themselves from

379
00:25:05,180 --> 00:25:06,000
 suffering.

380
00:25:06,000 --> 00:25:10,080
 So that's the Dhammapada for tonight. Thank you all for

381
00:25:10,080 --> 00:25:11,000
 listening.

