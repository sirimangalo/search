1
00:00:00,000 --> 00:00:07,000
 Okay, well here goes. Good morning. So I'm downtown this

2
00:00:07,000 --> 00:00:07,000
 morning. Downtown Bangkok.

3
00:00:07,000 --> 00:00:20,870
 At the Thai Airways building. So probably gonna get some

4
00:00:20,870 --> 00:00:22,160
 funny looks. I guess it kind

5
00:00:25,920 --> 00:00:32,920
 of looks just like I'm using my telephone. It's kind of a

6
00:00:32,920 --> 00:00:32,920
 part of being a monk I suppose

7
00:00:49,520 --> 00:00:56,520
 to have this sense of disconnect or disjointedness. This

8
00:00:56,520 --> 00:00:56,520
 jarring sense of something out of the

9
00:00:56,520 --> 00:01:12,310
 ordinary. I mean sitting anywhere near this day and age

10
00:01:12,310 --> 00:01:14,040
 with a row with wearing a mask

11
00:01:14,040 --> 00:01:21,040
 and carrying around an arms bowl is a shock in its own. And

12
00:01:21,040 --> 00:01:21,040
 rightly so makes people do

13
00:01:21,040 --> 00:01:38,960
 a double take.

14
00:01:38,960 --> 00:01:45,960
 In I think a good way because it alerts the mind. It

15
00:01:45,960 --> 00:01:45,960
 reminds the mind of something different

16
00:02:07,880 --> 00:02:12,330
 of another way. From the day to day drudgery of feeling

17
00:02:12,330 --> 00:02:14,880
 like there's no way out that all

18
00:02:14,880 --> 00:02:25,380
 you've got is this, the rat race, the endless cycle of sams

19
00:02:25,380 --> 00:02:26,720
ara. To see or to experience

20
00:02:30,920 --> 00:02:36,370
 something outside of that is I think a good sort of

21
00:02:36,370 --> 00:02:37,920
 surprise. It's good to be reminded

22
00:02:37,920 --> 00:02:52,800
 that another path exists. And another thing about the

23
00:02:52,800 --> 00:02:52,800
 juxtaposition of the mind.

24
00:02:52,800 --> 00:02:59,800
 Another thing about the ordinary and the out of ordinary or

25
00:02:59,800 --> 00:02:59,800
 the odd, like the appearance

26
00:03:17,000 --> 00:03:21,010
 of a monk downtown in an office building. It reminds us

27
00:03:21,010 --> 00:03:24,000
 that the dhamma is really everywhere.

28
00:03:24,000 --> 00:03:32,110
 So it's not really odd to have a monk sitting here or it's

29
00:03:32,110 --> 00:03:36,320
 not out of place to be giving

30
00:03:36,320 --> 00:03:45,980
 a dhamma talk amidst all this hustle and bustle. It's so

31
00:03:45,980 --> 00:03:46,800
 hard to be able to do that. It's not

32
00:03:46,800 --> 00:03:53,800
 a thing. It's certainly a novel experience. On Sunday I was

33
00:03:53,800 --> 00:03:53,800
 giving a talk in Thai. If

34
00:03:53,800 --> 00:03:59,220
 you want to hear it I think it should be available on my

35
00:03:59,220 --> 00:04:03,520
 website. I should check that actually

36
00:04:03,520 --> 00:04:07,820
 but it's under my talks. It's my latest in the Thai

37
00:04:07,820 --> 00:04:10,520
 directory or the Thai album. But

38
00:04:12,200 --> 00:04:17,380
 much different scene there. I'm sitting up on this big seat

39
00:04:17,380 --> 00:04:19,200
 and as I said I've got to

40
00:04:19,200 --> 00:04:25,260
 say this verse first and speak in formal tones and so on.

41
00:04:25,260 --> 00:04:28,800
 And everyone's holding up their

42
00:04:28,800 --> 00:04:34,390
 hands and listening attentively. Not just walking past and

43
00:04:34,390 --> 00:04:37,160
 ignoring me as though I didn't exist.

44
00:04:41,880 --> 00:04:46,360
 But the dhamma is everywhere. The dhamma isn't just in a

45
00:04:46,360 --> 00:04:48,880
 meditation hall or a monastery or

46
00:04:48,880 --> 00:04:52,870
 it isn't just there on Sundays or on the days that we have

47
00:04:52,870 --> 00:04:55,880
 our celebrations or our ceremonies.

48
00:04:55,880 --> 00:05:04,710
 The dhamma means the truth. The dhamma, one literal meaning

49
00:05:04,710 --> 00:05:08,840
 of the word dhamma or literal

50
00:05:09,280 --> 00:05:15,820
 usage of the word dhamma is for nature. For example they

51
00:05:15,820 --> 00:05:16,280
 say, "Yoy, ye dhamma hai tu pabhawa."

52
00:05:16,280 --> 00:05:22,060
 Oh no, sorry. "Yangkin chisamuteya dhammang sabantangmirodh

53
00:05:22,060 --> 00:05:23,280
 dhammang." Which means whatever

54
00:05:23,280 --> 00:05:31,780
 is of whatever has the dhamma to arise. Whatever thing, the

55
00:05:31,780 --> 00:05:35,320
 dhamma of whatever thing is of

56
00:05:35,320 --> 00:05:42,320
 whatever thing is to arise. Which makes it mean nature. The

57
00:05:42,320 --> 00:05:42,320
 thing about Pali is it's

58
00:05:42,320 --> 00:05:52,690
 simplified from the actual root. So if you look at dhamma

59
00:05:52,690 --> 00:05:57,240
 you can't see the root but

60
00:05:58,400 --> 00:06:03,170
 the root is dharar. Dharar which means in regards to

61
00:06:03,170 --> 00:06:05,400
 carrying or holding. So the common

62
00:06:05,400 --> 00:06:14,530
 usage of the word dhamma in the time of the Buddha was what

63
00:06:14,530 --> 00:06:19,480
 eventually became expressed

64
00:06:19,480 --> 00:06:25,080
 in the Bhagavad Gita or the Mahabharata. The dhamma is the

65
00:06:25,080 --> 00:06:27,840
 Hindu dhamma which means that

66
00:06:27,840 --> 00:06:32,300
 which you carry or that which you hold. So if a teacher

67
00:06:32,300 --> 00:06:34,840
 held something to be whatever

68
00:06:34,840 --> 00:06:40,650
 teachings or truths a teacher held to be true that would be

69
00:06:40,650 --> 00:06:43,040
 that teacher's dharma. What

70
00:06:43,040 --> 00:06:50,970
 they carry, dharar. And the four classes in Hindu society

71
00:06:50,970 --> 00:06:51,800
 each one had its dharama which

72
00:06:57,560 --> 00:07:02,060
 meant what they had to carry, sort of the burden or the

73
00:07:02,060 --> 00:07:04,560
 role that they had to fulfill.

74
00:07:04,560 --> 00:07:08,870
 So it was their dharma. A warrior had to fight and even

75
00:07:08,870 --> 00:07:11,780
 kill even his fellow countrymen.

76
00:07:11,780 --> 00:07:16,250
 And he or she should do that without compunction because it

77
00:07:16,250 --> 00:07:19,200
 was their dharma. It was the burden

78
00:07:21,240 --> 00:07:26,200
 that they were given to carry by God. So that's where the

79
00:07:26,200 --> 00:07:28,240
 word, maybe not where it came from

80
00:07:28,240 --> 00:07:34,710
 but where it was used, seems to have been used in the time

81
00:07:34,710 --> 00:07:36,760
 of the Buddha. So the Buddha

82
00:07:36,760 --> 00:07:41,350
 had his own dharma and when we talk about it like that we

83
00:07:41,350 --> 00:07:43,680
 mean that which the Buddha

84
00:07:43,680 --> 00:07:49,000
 held to be true. But you know when people would say this is

85
00:07:49,000 --> 00:07:50,680
 the dharma of the Buddha

86
00:07:51,320 --> 00:07:55,230
 he would just say this is not my dharma, this is dharma.

87
00:07:55,230 --> 00:07:58,320
 And so he explained dharma in another

88
00:07:58,320 --> 00:08:03,020
 way. He said dharma is what reality holds. That's the best

89
00:08:03,020 --> 00:08:05,320
 way of explaining it. What

90
00:08:13,760 --> 00:08:18,260
 reality holds, it means something has a dharma if it really

91
00:08:18,260 --> 00:08:20,760
 exists. It really exists, it's

92
00:08:20,760 --> 00:08:28,600
 a dharma in the sense that it holds some qualities.

93
00:08:28,600 --> 00:08:28,600
 Concepts could be considered dharmas but the

94
00:08:28,600 --> 00:08:36,610
 only qualities they really hold are the quality of not

95
00:08:36,610 --> 00:08:40,160
 being real. Whereas real dharmas are

96
00:08:41,040 --> 00:08:45,450
 like the paramatadhamas, the ultimate reality. The dhammas

97
00:08:45,450 --> 00:08:48,040
 that have ultimate meaning, these

98
00:08:48,040 --> 00:08:52,580
 are ones that hold certain characteristics and properties

99
00:08:52,580 --> 00:08:55,200
 because they're real. So they

100
00:08:55,200 --> 00:09:00,380
 carry something. They carry some information or some

101
00:09:00,380 --> 00:09:02,200
 quality to them.

102
00:09:02,200 --> 00:09:09,200
 So the dharma can be found everywhere. You can find it here

103
00:09:09,200 --> 00:09:09,200
 in downtown, you can find

104
00:09:09,200 --> 00:09:30,380
 it in front of your computer. This is worth noting that the

105
00:09:30,380 --> 00:09:30,760
 dharma is not a dharma. It

106
00:09:32,000 --> 00:09:35,920
 is worth stressing that insight meditation is quite unique

107
00:09:35,920 --> 00:09:39,000
 from all other types of meditation

108
00:09:39,000 --> 00:09:44,160
 because it's universal, which means first of all that it

109
00:09:44,160 --> 00:09:47,000
 can be practiced anywhere.

110
00:09:47,000 --> 00:09:55,020
 And second of all that it has an effect, an effect that can

111
00:09:55,020 --> 00:09:59,320
 be practiced anywhere. And

112
00:09:59,840 --> 00:10:04,410
 effect everywhere. It affects or it is useful everywhere.

113
00:10:04,410 --> 00:10:06,840
 The knowledge you gain about a

114
00:10:06,840 --> 00:10:15,230
 certain subset of experiences because of the nature of your

115
00:10:15,230 --> 00:10:19,520
 observation, having to deal

116
00:10:22,520 --> 00:10:27,970
 with the building blocks of reality can then be used in all

117
00:10:27,970 --> 00:10:29,520
 situations. So the truth that

118
00:10:29,520 --> 00:10:38,340
 you gain, the understanding that you gain sitting on your

119
00:10:38,340 --> 00:10:42,720
 meditation mat will help you

120
00:10:42,720 --> 00:10:47,740
 in your job, will help you in your life, will help you in

121
00:10:47,740 --> 00:10:49,720
 everything you do.

122
00:10:49,720 --> 00:10:54,660
 It helps you deal with other people, helps you deal with

123
00:10:54,660 --> 00:10:56,720
 other experiences. People say

124
00:10:56,720 --> 00:11:07,760
 what good is it to go sit and stare at your navel, gaze at

125
00:11:07,760 --> 00:11:13,600
 your navel? What good is it?

126
00:11:13,600 --> 00:11:20,600
 What good does it do to sit around all day like that? Well

127
00:11:20,600 --> 00:11:20,600
 this is the answer. You're

128
00:11:20,600 --> 00:11:27,950
 looking at the dharma and the dharma is everywhere. The d

129
00:11:27,950 --> 00:11:32,480
harma is something that you can find

130
00:11:32,480 --> 00:11:38,460
 wherever you are, not just on the meditation mat or in a

131
00:11:38,460 --> 00:11:40,040
 monastery.

132
00:11:41,040 --> 00:11:45,510
 The dharma is everywhere. Anyway just some food for thought

133
00:11:45,510 --> 00:11:48,040
. I didn't really have any

134
00:11:48,040 --> 00:11:53,640
 much more planned for today. I'll be traveling today and

135
00:11:53,640 --> 00:11:56,720
 then tomorrow and we'll see if I

136
00:11:56,720 --> 00:12:40,550
 get settled in in time, maybe it will be Sunday morning

137
00:12:40,550 --> 00:12:09,280
 here. I'll give a little bit of a

138
00:12:10,400 --> 00:12:14,600
 monk radio session, which will be Saturday evening UTC. I

139
00:12:14,600 --> 00:12:17,400
 can't remember what time, but

140
00:12:17,400 --> 00:12:23,870
 sometime around 10 or 11 PM, 2200, 2300, whatever it says

141
00:12:23,870 --> 00:12:26,880
 on the website. Maybe I'll get around

142
00:12:26,880 --> 00:12:37,320
 to it. Until then, wishing you all the best. Keep practice.

143
00:12:37,320 --> 00:13:07,320
 (

