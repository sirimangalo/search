1
00:00:00,000 --> 00:00:04,320
 Okay, I'm thinking about teaching elementary students

2
00:00:04,320 --> 00:00:06,080
 meditation, especially those who

3
00:00:06,080 --> 00:00:10,350
 have emotional disturbances such as strong anger, defiance,

4
00:00:10,350 --> 00:00:11,680
 hyperactivity.

5
00:00:11,680 --> 00:00:12,680
 What is the best technique?

6
00:00:12,680 --> 00:00:15,600
 Are they too young to learn?

7
00:00:15,600 --> 00:00:19,860
 Okay, I probably should do a separate video just to say

8
00:00:19,860 --> 00:00:22,160
 this, but maybe I'll just say

9
00:00:22,160 --> 00:00:26,110
 it again and again in all of them because it's something

10
00:00:26,110 --> 00:00:28,520
 that I believe quite strongly,

11
00:00:28,520 --> 00:00:32,360
 and I've said it now tonight a few times, is that don't go

12
00:00:32,360 --> 00:00:35,840
 where the real problems

13
00:00:35,840 --> 00:00:38,440
 are.

14
00:00:38,440 --> 00:00:44,370
 I would recommend against trying to deal with people who

15
00:00:44,370 --> 00:00:47,040
 have serious problems.

16
00:00:47,040 --> 00:00:52,200
 It's very altruistic to do so, but it's missing one

17
00:00:52,200 --> 00:00:56,160
 important, very important point.

18
00:00:56,160 --> 00:01:08,320
 The amount of effort you put in to helping someone who is

19
00:01:08,320 --> 00:01:12,480
 in a very bad state is compared

20
00:01:12,480 --> 00:01:17,360
 with the benefit of helping them, the potential benefit.

21
00:01:17,360 --> 00:01:22,480
 The effort level is incredibly high.

22
00:01:22,480 --> 00:01:27,700
 The amount of benefit that you can give to them is very low

23
00:01:27,700 --> 00:01:27,920
.

24
00:01:27,920 --> 00:01:28,920
 There's no miracles.

25
00:01:28,920 --> 00:01:36,840
 A person who has a lot of mental trouble is not likely to,

26
00:01:36,840 --> 00:01:40,880
 in most cases, become a fully

27
00:01:40,880 --> 00:01:43,880
 enlightened being in this life.

28
00:01:43,880 --> 00:01:44,880
 It just doesn't happen.

29
00:01:44,880 --> 00:01:47,880
 This monk that I was talking about is an example of that.

30
00:01:47,880 --> 00:01:49,320
 He practiced meditation.

31
00:01:49,320 --> 00:01:50,320
 He's been practicing.

32
00:01:50,320 --> 00:01:53,080
 He's still alive.

33
00:01:53,080 --> 00:01:56,920
 He's still taking baby steps.

34
00:01:56,920 --> 00:01:58,440
 He's still blowing up.

35
00:01:58,440 --> 00:02:00,960
 He's still got the same problems.

36
00:02:00,960 --> 00:02:03,320
 It's a very, very slow path.

37
00:02:03,320 --> 00:02:07,480
 Everyone who practices meditation is going to go through

38
00:02:07,480 --> 00:02:09,720
 the same conditions again and

39
00:02:09,720 --> 00:02:15,970
 again and again until finally they're worn down, broken

40
00:02:15,970 --> 00:02:18,920
 apart, and discarded.

41
00:02:18,920 --> 00:02:28,440
 Until that point, they will come back again and again and

42
00:02:28,440 --> 00:02:32,120
 again until they become weaker

43
00:02:32,120 --> 00:02:39,800
 and weaker and weaker until they disappear.

44
00:02:39,800 --> 00:02:43,920
 You get a far less return for your effort.

45
00:02:43,920 --> 00:02:48,230
 If you take the same amount of effort and you apply it to

46
00:02:48,230 --> 00:02:49,920
 people who are exceptional,

47
00:02:49,920 --> 00:02:58,550
 gifted, brilliant, focused, people who have potential, and

48
00:02:58,550 --> 00:03:02,400
 you teach them meditation,

49
00:03:02,400 --> 00:03:12,090
 first of all, very little effort, second of all, incredible

50
00:03:12,090 --> 00:03:16,240
 results, that they will grasp

51
00:03:16,240 --> 00:03:22,400
 it much easier, pick it up, and in many cases it's like

52
00:03:22,400 --> 00:03:24,400
 planting a seed.

53
00:03:24,400 --> 00:03:27,760
 You don't have to make it grow.

54
00:03:27,760 --> 00:03:31,200
 It grows by itself.

55
00:03:31,200 --> 00:03:37,440
 It goes viral because they pick it up and they become you.

56
00:03:37,440 --> 00:03:43,760
 They become the person who's spreading the teaching.

57
00:03:43,760 --> 00:03:52,180
 You help such people who are teachers and those teachers go

58
00:03:52,180 --> 00:03:55,360
 and help, go and teach.

59
00:03:55,360 --> 00:04:01,570
 So at the very least, what you'll get, theoretically, is a

60
00:04:01,570 --> 00:04:07,120
 bunch of people who are very gifted,

61
00:04:07,120 --> 00:04:11,080
 who will then go out and pass along what you've passed

62
00:04:11,080 --> 00:04:13,800
 along to people who are less gifted

63
00:04:13,800 --> 00:04:16,840
 and on and on down the line.

64
00:04:16,840 --> 00:04:21,390
 There will be people who are actually working with these

65
00:04:21,390 --> 00:04:24,160
 kind of people who have serious

66
00:04:24,160 --> 00:04:28,840
 problems and they will do this.

67
00:04:28,840 --> 00:04:32,350
 They will have, based on the teachings that you have been

68
00:04:32,350 --> 00:04:34,000
 giving on a higher level, they

69
00:04:34,000 --> 00:04:44,670
 will do the grunt work of dealing with these people and it

70
00:04:44,670 --> 00:04:46,800
 will create more benefit in

71
00:04:46,800 --> 00:04:50,140
 the long run, even for those people who are in a bad way

72
00:04:50,140 --> 00:04:52,200
 because you are helping people

73
00:04:52,200 --> 00:04:59,080
 who are in a position to help others will create a chain.

74
00:04:59,080 --> 00:05:02,400
 It's like a pyramid scheme.

75
00:05:02,400 --> 00:05:07,200
 It will multiply and everyone will benefit.

76
00:05:07,200 --> 00:05:09,390
 What I'm saying is that people who come to practice

77
00:05:09,390 --> 00:05:10,920
 meditation with me end up becoming

78
00:05:10,920 --> 00:05:15,280
 social workers or teachers.

79
00:05:15,280 --> 00:05:19,080
 So I don't have to go out and teach those people.

80
00:05:19,080 --> 00:05:23,050
 I teach the people who will go out and teach them, which is

81
00:05:23,050 --> 00:05:25,440
, to me, far better use of one's

82
00:05:25,440 --> 00:05:26,440
 time.

83
00:05:26,440 --> 00:05:30,060
 The basic point being to try to pick the gifted, pick the

84
00:05:30,060 --> 00:05:32,160
 ones who are you going to get the

85
00:05:32,160 --> 00:05:38,000
 best return for your effort because it's not being selfish

86
00:05:38,000 --> 00:05:41,400
 or it's not being discriminatory.

87
00:05:41,400 --> 00:05:45,020
 It's being realistic and understanding that we're really in

88
00:05:45,020 --> 00:05:45,880
 a war here.

89
00:05:45,880 --> 00:05:50,320
 We don't have the luxury to pick and choose.

90
00:05:50,320 --> 00:05:55,830
 We have to go for the most benefit because death waits for

91
00:05:55,830 --> 00:05:58,560
 no one and nothing waits.

92
00:05:58,560 --> 00:06:02,920
 The universe will continue rolling on in its way and if we

93
00:06:02,920 --> 00:06:05,240
 don't keep up, we're going to

94
00:06:05,240 --> 00:06:06,960
 get left behind.

95
00:06:06,960 --> 00:06:10,050
 This is why people get burnt out when they deal with such

96
00:06:10,050 --> 00:06:10,680
 people.

97
00:06:10,680 --> 00:06:13,440
 This is only half of the answer.

98
00:06:13,440 --> 00:06:16,980
 My answer is don't teach children.

99
00:06:16,980 --> 00:06:20,740
 This is only in relation to the part of especially those

100
00:06:20,740 --> 00:06:23,160
 who have emotional disturbances.

101
00:06:23,160 --> 00:06:24,160
 You're welcome to do it.

102
00:06:24,160 --> 00:06:28,640
 I'm not going to try to tell you what to do.

103
00:06:28,640 --> 00:06:31,950
 Just give you some idea and maybe it will help to change

104
00:06:31,950 --> 00:06:32,760
 your focus.

105
00:06:32,760 --> 00:06:37,000
 I would think it would be much, from my point of view, much

106
00:06:37,000 --> 00:06:38,560
 more inspiring to teach the

107
00:06:38,560 --> 00:06:41,480
 gifted children.

108
00:06:41,480 --> 00:06:47,390
 The way that you teach children, the question as to whether

109
00:06:47,390 --> 00:06:50,240
 they're too young, the tradition

110
00:06:50,240 --> 00:06:53,200
 goes seven years.

111
00:06:53,200 --> 00:06:54,200
 Everything's seven.

112
00:06:54,200 --> 00:06:57,930
 At seven years, they're old enough to understand, which is

113
00:06:57,930 --> 00:07:00,080
 most school children already.

114
00:07:00,080 --> 00:07:04,040
 Anything below seven years is considered to be too young.

115
00:07:04,040 --> 00:07:08,840
 At seven years, this is the cutoff and then they can start

116
00:07:08,840 --> 00:07:10,020
 learning.

117
00:07:10,020 --> 00:07:13,740
 The way to teach children is to introduce to them the

118
00:07:13,740 --> 00:07:16,600
 concept of mindfulness, the concept

119
00:07:16,600 --> 00:07:29,270
 of recognition, teaching them to recognize and to clearly

120
00:07:29,270 --> 00:07:32,560
 perceive the experience in

121
00:07:32,560 --> 00:07:38,040
 front of them, or anything really.

122
00:07:38,040 --> 00:07:43,560
 I tried an experiment once with some kids in a Thai

123
00:07:43,560 --> 00:07:46,880
 monastery in Los Angeles.

124
00:07:46,880 --> 00:07:48,400
 I had them focus on a cat.

125
00:07:48,400 --> 00:07:52,010
 I had them think of a cat and say to themselves in their

126
00:07:52,010 --> 00:07:53,680
 mind, "Cat, cat."

127
00:07:53,680 --> 00:07:58,690
 This was a room of like a hundred kids and they were unruly

128
00:07:58,690 --> 00:07:59,080
.

129
00:07:59,080 --> 00:08:03,900
 These were kids who come to the temple on Saturdays and

130
00:08:03,900 --> 00:08:06,200
 Sundays and get whatever lesson

131
00:08:06,200 --> 00:08:08,520
 that is possible to give them.

132
00:08:08,520 --> 00:08:11,120
 It's mainly not whatever.

133
00:08:11,120 --> 00:08:17,800
 It is what it is.

134
00:08:17,800 --> 00:08:19,960
 They were like anything I would say.

135
00:08:19,960 --> 00:08:23,480
 They would repeat it and they would make fun of it and

136
00:08:23,480 --> 00:08:24,280
 laugh at it.

137
00:08:24,280 --> 00:08:31,710
 I wasn't upset by that, but I was prepared in advance

138
00:08:31,710 --> 00:08:35,920
 because I was aware of the situation.

139
00:08:35,920 --> 00:08:38,720
 You can't lead these people directly into meditation.

140
00:08:38,720 --> 00:08:41,760
 You can't say, "Start watching the stomach, say rising."

141
00:08:41,760 --> 00:08:43,400
 Give them something that's going to appeal to them.

142
00:08:43,400 --> 00:08:47,360
 I thought, "Let's do some simple kid stuff."

143
00:08:47,360 --> 00:09:00,680
 Think of a cat and say to yourself, "Cat, cat, cat."

144
00:09:00,680 --> 00:09:01,680
 Then dog, dog.

145
00:09:01,680 --> 00:09:04,720
 I had to do dog, dog.

146
00:09:04,720 --> 00:09:05,720
 It is kind of fun at first.

147
00:09:05,720 --> 00:09:09,160
 It seems like I'm just playing a game with them, but

148
00:09:09,160 --> 00:09:11,120
 actually they start to conceive

149
00:09:11,120 --> 00:09:15,610
 and they start to focus and their mind starts to quiet down

150
00:09:15,610 --> 00:09:15,960
.

151
00:09:15,960 --> 00:09:16,960
 It did work.

152
00:09:16,960 --> 00:09:18,880
 I think it worked quite well.

153
00:09:18,880 --> 00:09:23,200
 It's something that you'd have to try long-term, but the

154
00:09:23,200 --> 00:09:25,560
 point of it is to get them around

155
00:09:25,560 --> 00:09:28,660
 slowly to the point of being able to watch and observe

156
00:09:28,660 --> 00:09:30,240
 their own experience.

157
00:09:30,240 --> 00:09:33,940
 First a cat and then a dog, and then I had them focus on

158
00:09:33,940 --> 00:09:35,320
 their parents.

159
00:09:35,320 --> 00:09:36,920
 This is important.

160
00:09:36,920 --> 00:09:39,880
 This really got me brownie points with the parents.

161
00:09:39,880 --> 00:09:47,840
 The parents are all sitting listening as well.

162
00:09:47,840 --> 00:09:52,320
 I said, "Think of your mother and say to yourself, 'Mother,

163
00:09:52,320 --> 00:09:54,000
 mother, mother.'

164
00:09:54,000 --> 00:09:59,880
 And then your father and say to yourself, 'Father, father

165
00:09:59,880 --> 00:10:00,560
.'"

166
00:10:00,560 --> 00:10:03,250
 Then I came to what is actually the beginnings of Buddhist

167
00:10:03,250 --> 00:10:04,040
 meditation.

168
00:10:04,040 --> 00:10:05,760
 It's the focusing on the Buddha.

169
00:10:05,760 --> 00:10:09,580
 You have this famous Buddhist meditation of saying to

170
00:10:09,580 --> 00:10:12,120
 yourself, "Buddha, Buddha."

171
00:10:12,120 --> 00:10:13,120
 So I had them do that.

172
00:10:13,120 --> 00:10:16,060
 I said, "Picture the Buddha now, because the Buddha is a

173
00:10:16,060 --> 00:10:17,560
 big golden Buddha image."

174
00:10:17,560 --> 00:10:23,550
 I said, "Think of the Buddha and say Buddha, Buddha, Buddha

175
00:10:23,550 --> 00:10:23,800
."

176
00:10:23,800 --> 00:10:26,520
 So they did this.

177
00:10:26,520 --> 00:10:29,220
 Then eventually I got around, I think after the Buddha,

178
00:10:29,220 --> 00:10:30,680
 then it was right to focusing

179
00:10:30,680 --> 00:10:38,690
 on the body, and then it was watching the stomach, watching

180
00:10:38,690 --> 00:10:40,840
 your breath.

181
00:10:40,840 --> 00:10:41,840
 I brought it to them that way.

182
00:10:41,840 --> 00:10:45,150
 I think that it's just a simple point there, the idea of

183
00:10:45,150 --> 00:10:47,280
 starting with something cartoonish

184
00:10:47,280 --> 00:10:52,560
 that they can relate to.

185
00:10:52,560 --> 00:10:59,620
 Of course it depends on the age level, but the important

186
00:10:59,620 --> 00:11:03,400
 point is to bring the concept

187
00:11:03,400 --> 00:11:05,520
 to them.

188
00:11:05,520 --> 00:11:09,320
 Find a way to explain mindfulness to them.

189
00:11:09,320 --> 00:11:12,860
 I think that's clearly explained by using the cat example,

190
00:11:12,860 --> 00:11:14,000
 the dog example.

191
00:11:14,000 --> 00:11:18,070
 You're just focusing on something and you're seeing this is

192
00:11:18,070 --> 00:11:19,720
 a cat, this is a dog.

193
00:11:19,720 --> 00:11:25,440
 And then when you're doing this, they're building up this

194
00:11:25,440 --> 00:11:26,800
 ability.

195
00:11:26,800 --> 00:11:31,640
 And all you do is you bring that ability, that proficiency,

196
00:11:31,640 --> 00:11:33,840
 or however, back to focus

197
00:11:33,840 --> 00:11:35,120
 on reality.

198
00:11:35,120 --> 00:11:36,120
 So that's what I think.

199
00:11:36,120 --> 00:12:01,700
 [

