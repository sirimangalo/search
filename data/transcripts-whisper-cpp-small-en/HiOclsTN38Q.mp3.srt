1
00:00:00,000 --> 00:00:03,760
 An example of family conflict would be addressing their use

2
00:00:03,760 --> 00:00:05,440
 of bad language.

3
00:00:05,440 --> 00:00:09,080
 Should I have just ignored it or was I right to show my

4
00:00:09,080 --> 00:00:12,960
 disapproval?

5
00:00:12,960 --> 00:00:16,040
 No, that sounds very, very,

6
00:00:16,040 --> 00:00:18,220
 very, very

7
00:00:18,220 --> 00:00:20,160
 evangelical.

8
00:00:20,160 --> 00:00:22,760
 Is that the right word? I don't want to say Christian

9
00:00:22,760 --> 00:00:26,350
 because it's not what I mean. I really don't. I really know

10
00:00:26,350 --> 00:00:26,520
, I really

11
00:00:26,520 --> 00:00:29,630
 am personally aware that there are good Christians out

12
00:00:29,630 --> 00:00:31,720
 there. It's just a word that happens to

13
00:00:31,720 --> 00:00:34,960
 roll off the tongue for bad things.

14
00:00:34,960 --> 00:00:38,000
 There are really good Christians, Catholics, Protestants,

15
00:00:38,000 --> 00:00:40,040
 people out there, Mormons, Jehovah's

16
00:00:40,040 --> 00:00:41,400
 Witnesses,

17
00:00:41,400 --> 00:00:44,850
 but there's something in there that's not, nothing to do

18
00:00:44,850 --> 00:00:46,120
 with Christianity. So what is

19
00:00:46,120 --> 00:00:52,080
 it? Is it Evangelism or Proselytism?

20
00:00:52,080 --> 00:00:56,720
 What's the word?

21
00:00:56,720 --> 00:00:57,720
 Zealous.

22
00:00:57,720 --> 00:01:00,720
 Zealous? No? Overzealous?

23
00:01:00,720 --> 00:01:02,520
 What do you call it when you try to impress your views on

24
00:01:02,520 --> 00:01:05,720
 other people?

25
00:01:05,720 --> 00:01:10,120
 Anyone?

26
00:01:10,120 --> 00:01:15,080
 I think you're right. Zellotry, I think you call it.

27
00:01:15,080 --> 00:01:17,120
 Zellotry.

28
00:01:17,120 --> 00:01:22,000
 I mean, first of all, there's no such thing as bad words.

29
00:01:22,000 --> 00:01:26,160
 They're just words. There was a monk once who went around

30
00:01:26,160 --> 00:01:27,200
 calling

31
00:01:27,200 --> 00:01:31,240
 his friends wassala. I think wassala is it?

32
00:01:31,240 --> 00:01:36,210
 Which is a word that means dog. It doesn't mean dog, but it

33
00:01:36,210 --> 00:01:37,480
's equivalent to in English

34
00:01:37,480 --> 00:01:42,600
 how these people call each other. Hey, dog.

35
00:01:42,600 --> 00:01:45,590
 Wassala, I believe it was the word was wassala, and it

36
00:01:45,590 --> 00:01:49,840
 means like

37
00:01:49,840 --> 00:02:01,120
 servant or, yeah, like a very low class person.

38
00:02:01,120 --> 00:02:03,720
 But it was because he was just used to referring to people

39
00:02:03,720 --> 00:02:05,360
 in that way. And so they went to

40
00:02:05,360 --> 00:02:07,470
 the Buddha and they said, look, this guy's calling us wass

41
00:02:07,470 --> 00:02:11,240
ala. It's really insulting.

42
00:02:11,240 --> 00:02:13,640
 And the Buddha said, well, that's just the way he, it's

43
00:02:13,640 --> 00:02:15,120
 just wohara. It's just the words

44
00:02:15,120 --> 00:02:17,200
 that he uses.

45
00:02:17,200 --> 00:02:21,740
 So it's a pretty good example of overreacting because even

46
00:02:21,740 --> 00:02:25,240
 if there was something good about

47
00:02:25,240 --> 00:02:28,420
 disapproving of other people's behavior, I wouldn't say it

48
00:02:28,420 --> 00:02:29,860
's a good example of what should

49
00:02:29,860 --> 00:02:32,440
 be disapproved of.

50
00:02:32,440 --> 00:02:38,940
 But as you'll probably learn the hard way, generally no

51
00:02:38,940 --> 00:02:40,800
 good comes from disapproving at

52
00:02:40,800 --> 00:02:47,030
 all anyway. I've always felt I kind of have a duty to my

53
00:02:47,030 --> 00:02:54,000
 parents to find some way to alert

54
00:02:54,000 --> 00:03:01,260
 them to their deleterious behavior for their benefit. But

55
00:03:01,260 --> 00:03:02,320
 other members of the family I've

56
00:03:02,320 --> 00:03:07,920
 never felt that way towards. I don't think it's held up by

57
00:03:07,920 --> 00:03:09,440
 the Buddha's teaching, and

58
00:03:09,440 --> 00:03:13,340
 I don't think it's really logical to think that we have

59
00:03:13,340 --> 00:03:15,600
 some kind of, just because we

60
00:03:15,600 --> 00:03:19,530
 were born before the person or because we have the same

61
00:03:19,530 --> 00:03:21,800
 blood as the person, the same

62
00:03:21,800 --> 00:03:29,870
 gene pool or whatever, that we have any reason to admonish

63
00:03:29,870 --> 00:03:32,000
 them. They're not our students.

64
00:03:32,000 --> 00:03:35,320
 They didn't ask for our teachings.

65
00:03:35,320 --> 00:03:39,730
 So even when they're doing bad deeds, it's not our, first

66
00:03:39,730 --> 00:03:41,880
 of all it's not our duty, second

67
00:03:41,880 --> 00:03:46,790
 of all it's not helpful. The disapproval of other people's

68
00:03:46,790 --> 00:03:49,240
 bad deeds, the open disapproval,

69
00:03:49,240 --> 00:03:53,680
 is rarely if ever, unless they're your students and they

70
00:03:53,680 --> 00:03:56,920
 really, really trust you, it's rarely

71
00:03:56,920 --> 00:04:03,540
 beneficial, even if they're your students, unless they are

72
00:04:03,540 --> 00:04:06,680
 really, I would like to say,

73
00:04:06,680 --> 00:04:11,250
 advanced or special people, they tend to get very, very,

74
00:04:11,250 --> 00:04:13,720
 lose faith very, very quickly

75
00:04:13,720 --> 00:04:20,630
 when you start criticizing them. So except in special cases

76
00:04:20,630 --> 00:04:25,760
, criticism is very dangerous.

77
00:04:25,760 --> 00:04:29,640
 I'm showing your disapproval now, that's really, really a

78
00:04:29,640 --> 00:04:32,200
 bad idea. You have to show your happiness.

79
00:04:32,200 --> 00:04:35,470
 It's like the fight that you have to fight and that you

80
00:04:35,470 --> 00:04:37,320
 have to win is who's happier,

81
00:04:37,320 --> 00:04:42,720
 them who are engaged in all of their pursuit of sensuality

82
00:04:42,720 --> 00:04:45,120
 or you who have given it up.

83
00:04:45,120 --> 00:04:47,270
 And if you're not happier, if they're the ones sitting

84
00:04:47,270 --> 00:04:50,320
 around cussing and swearing and

85
00:04:50,320 --> 00:04:57,210
 enjoying it and you're all on their back about it, you're

86
00:04:57,210 --> 00:05:01,160
 not winning, you're losing, you're

87
00:05:01,160 --> 00:05:04,980
 losing the happiness battle. They're happier than you are.

88
00:05:04,980 --> 00:05:07,160
 Yeah, they are happy, you're

89
00:05:07,160 --> 00:05:09,680
 like miserable. That was the big problem for me, is that I

90
00:05:09,680 --> 00:05:11,360
 was miserable. I knew I was

91
00:05:11,360 --> 00:05:14,170
 doing the right thing but I was like, this is like, the

92
00:05:14,170 --> 00:05:16,000
 toughest nails. It was the hardest

93
00:05:16,000 --> 00:05:22,110
 thing that I'd ever done, of course. And so that kind of

94
00:05:22,110 --> 00:05:23,800
 clicked with me at one point

95
00:05:23,800 --> 00:05:27,100
 and I said, you know, I can't win this battle. I can't

96
00:05:27,100 --> 00:05:29,320
 prove to my family that what I'm doing

97
00:05:29,320 --> 00:05:34,190
 is well. I can't convince them that what I'm doing is right

98
00:05:34,190 --> 00:05:36,640
 because I'm not happy. I know

99
00:05:36,640 --> 00:05:40,720
 that what I'm doing is right but, sorry, I wouldn't say

100
00:05:40,720 --> 00:05:43,280
 miserable but really struggling

101
00:05:43,280 --> 00:05:47,450
 because there's no question that I totally, the suffering

102
00:05:47,450 --> 00:05:49,960
 that I'd had before, after practicing,

103
00:05:49,960 --> 00:05:54,100
 after finding meditation and realizing the bad, it was

104
00:05:54,100 --> 00:05:56,800
 incredibly liberating. The meditation

105
00:05:56,800 --> 00:06:01,540
 is the way to go but it was still like, this is not easy

106
00:06:01,540 --> 00:06:03,960
 for me. I have all these conflicting

107
00:06:03,960 --> 00:06:08,460
 desires and emotions that I really don't want and have to

108
00:06:08,460 --> 00:06:13,240
 work to get rid of but I knew

109
00:06:13,240 --> 00:06:15,490
 there was no question that this is the right path, of

110
00:06:15,490 --> 00:06:18,760
 course, right. But until you get

111
00:06:18,760 --> 00:06:21,360
 to the point where you're actually able to express it, you

112
00:06:21,360 --> 00:06:22,760
 know, when you're fighting

113
00:06:22,760 --> 00:06:26,900
 with these things, there's very little you can do to

114
00:06:26,900 --> 00:06:32,240
 convince other people. So if you've

115
00:06:32,240 --> 00:06:37,220
 gained happiness in meditation, the only way to convince

116
00:06:37,220 --> 00:06:39,920
 people that you have gained that,

117
00:06:39,920 --> 00:06:43,480
 meditation is the better way or your way is the better way,

118
00:06:43,480 --> 00:06:45,080
 is to show them and to give

119
00:06:45,080 --> 00:06:49,770
 them proof that it is the better way. Because if your path

120
00:06:49,770 --> 00:06:54,360
 leads you to be judgmental and

121
00:06:54,360 --> 00:06:59,580
 disapproving, it's not really appealing, you know, is it?

122
00:06:59,580 --> 00:07:02,280
 It's not really how anyone else

123
00:07:02,280 --> 00:07:04,240
 would ever wish to be.

