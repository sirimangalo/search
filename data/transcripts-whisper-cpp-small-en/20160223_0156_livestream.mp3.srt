1
00:00:00,000 --> 00:00:26,040
 Good evening everyone.

2
00:00:26,040 --> 00:00:31,040
 Happy Maga Bhuja.

3
00:00:31,040 --> 00:00:34,040
 Maga Poona Me.

4
00:00:34,040 --> 00:00:47,610
 Maga Puja. Puja is paying respect or homage. Maga because

5
00:00:47,610 --> 00:00:54,040
 it's the full moon of Maga. The month of Maga.

6
00:00:54,040 --> 00:01:06,110
 So what's important about this full moon you ask? Well

7
00:01:06,110 --> 00:01:07,040
 tradition has it that on the full moon of Maga the Buddha

8
00:01:07,040 --> 00:01:14,040
 taught what is now known as the Awadapatimoka.

9
00:01:14,040 --> 00:01:31,040
 It's sort of a summary of Buddhism. It's well known.

10
00:01:31,040 --> 00:01:42,050
 And so the story behind it is there was so much of a story.

11
00:01:42,050 --> 00:01:47,520
 There was a group of monks who were meditating and it may

12
00:01:47,520 --> 00:01:54,040
 have been the fire ascetics, the fire worshipping ascetics

13
00:01:54,040 --> 00:02:00,040
 in Rajgah because there were 1,250 of them.

14
00:02:00,040 --> 00:02:13,100
 Which coincides with the number of fire worshippers there

15
00:02:13,100 --> 00:02:15,040
 were.

16
00:02:15,040 --> 00:02:19,390
 So they were meditating on their own and one of them was

17
00:02:19,390 --> 00:02:24,300
 practicing strenuously and became an arahat and he realized

18
00:02:24,300 --> 00:02:30,440
 that he had attained the goal. And so he went to see the

19
00:02:30,440 --> 00:02:32,040
 Buddha where the Buddha was sitting.

20
00:02:32,040 --> 00:02:36,380
 But when he got to the Buddha he looked and saw there was

21
00:02:36,380 --> 00:02:40,930
 another monk coming and he said well I'll wait until this

22
00:02:40,930 --> 00:02:46,080
 monk sits down and the other monk came and sat down and the

23
00:02:46,080 --> 00:02:52,040
 other monk came up and turned and saw another monk coming.

24
00:02:52,040 --> 00:02:55,960
 And so he decided he wouldn't talk to the Buddha, he'd wait

25
00:02:55,960 --> 00:02:59,820
 for the third monk to come and so he sat down. The third

26
00:02:59,820 --> 00:03:03,880
 monk came and then a fourth monk came and one by one all

27
00:03:03,880 --> 00:03:09,410
 the monks came together until there was 1,250 monks sitting

28
00:03:09,410 --> 00:03:10,040
 there.

29
00:03:10,040 --> 00:03:18,170
 All who had just attained arahantship. And then the Buddha

30
00:03:18,170 --> 00:03:24,960
 taught the other bhatimoka, that's the tradition, that's

31
00:03:24,960 --> 00:03:28,040
 what they say happened.

32
00:03:28,040 --> 00:03:55,120
 But what we're going to look up here, we'll look up the Pal

33
00:03:55,120 --> 00:03:56,040
i.

34
00:03:56,040 --> 00:03:58,040
 So it starts

35
00:04:00,040 --> 00:04:02,040
 So it starts

36
00:04:02,040 --> 00:04:04,040
 So it starts

37
00:04:06,040 --> 00:04:08,040
 So it starts

38
00:04:08,040 --> 00:04:10,040
 So it starts

39
00:04:12,040 --> 00:04:14,040
 So it starts

40
00:04:16,040 --> 00:04:18,040
 So it starts

41
00:04:18,040 --> 00:04:20,040
 So it starts

42
00:04:20,040 --> 00:04:22,040
 So it starts

43
00:04:22,040 --> 00:04:24,040
 So it starts

44
00:04:24,040 --> 00:04:26,040
 So it starts

45
00:04:26,040 --> 00:04:28,040
 So it starts

46
00:04:28,040 --> 00:04:30,040
 So it starts

47
00:04:30,040 --> 00:04:32,040
 So it starts

48
00:04:32,040 --> 00:04:34,040
 So it starts

49
00:04:34,040 --> 00:04:36,040
 So it starts

50
00:04:36,040 --> 00:04:38,040
 So it starts

51
00:04:38,040 --> 00:04:40,040
 So it starts

52
00:04:40,040 --> 00:04:42,040
 So it starts

53
00:04:42,040 --> 00:04:44,040
 So it starts

54
00:04:44,040 --> 00:04:46,040
 So it starts

55
00:04:46,040 --> 00:04:48,040
 So it starts

56
00:04:48,040 --> 00:04:50,040
 So it starts

57
00:04:50,040 --> 00:04:52,040
 So it starts

58
00:04:52,040 --> 00:04:54,040
 So it starts

59
00:04:54,040 --> 00:04:56,040
 So it starts

60
00:04:56,040 --> 00:04:58,040
 So it starts

61
00:04:58,040 --> 00:05:00,040
 So it starts

62
00:05:00,040 --> 00:05:02,040
 So it starts

63
00:05:02,040 --> 00:05:04,040
 So it starts

64
00:05:04,040 --> 00:05:06,040
 So it starts

65
00:05:06,040 --> 00:05:08,040
 So it starts

66
00:05:08,040 --> 00:05:10,040
 So it starts

67
00:05:10,040 --> 00:05:12,040
 Well condensed

68
00:05:12,040 --> 00:05:14,040
 Summaries of the Buddha's teaching

69
00:05:14,040 --> 00:05:16,040
 We always summarize it with the second verse

70
00:05:16,040 --> 00:05:18,040
 Saba Pappa Sa'akaranaan

71
00:05:18,040 --> 00:05:20,040
 Koussala Sa'upa Sa'upa Sa'upa Dasa

72
00:05:20,040 --> 00:05:22,040
 Jittabryo Dapanang

73
00:05:22,040 --> 00:05:24,040
 Itang Buddha Na Sa'asanaan

74
00:05:24,040 --> 00:05:26,040
 So this is the Buddha Na Sa'asanaan, the teaching

75
00:05:26,040 --> 00:05:28,040
 of the Buddha's plural.

76
00:05:28,040 --> 00:05:30,040
 All

77
00:05:30,040 --> 00:05:32,040
 Buddha's teach

78
00:05:32,040 --> 00:05:34,040
 Saba Pappa Sa'akaranaan

79
00:05:34,040 --> 00:05:36,040
 Not doing any evil

80
00:05:36,040 --> 00:05:38,040
 Koussala Sa'upa Sa'upa

81
00:05:38,040 --> 00:05:40,040
 Sa'upa Sa'upa Dasa

82
00:05:40,040 --> 00:05:42,040
 Be full of good

83
00:05:42,040 --> 00:05:44,040
 Sathyattaparyodapanang

84
00:05:44,040 --> 00:05:46,040
 and the purification

85
00:05:46,040 --> 00:05:48,040
 of one's own mind

86
00:05:48,040 --> 00:05:52,040
 So not doing evil

87
00:05:52,040 --> 00:05:54,040
 and doing good

88
00:05:54,040 --> 00:05:56,040
 and purifying one's mind

89
00:05:56,040 --> 00:05:58,040
 This is the teaching of all the Buddha

90
00:05:58,040 --> 00:06:02,040
 I think in the Thai version

91
00:06:02,040 --> 00:06:04,040
 that verse is the first of the three

92
00:06:04,040 --> 00:06:06,040
 I can't remember that

93
00:06:06,040 --> 00:06:08,040
 Anyway, so the first

94
00:06:08,040 --> 00:06:10,040
 it starts Kanti Brahmungta Bodhicak

95
00:06:10,040 --> 00:06:12,040
 which is also very important teaching

96
00:06:12,040 --> 00:06:14,040
 Patience

97
00:06:14,040 --> 00:06:16,040
 is the highest form of austerity

98
00:06:16,040 --> 00:06:18,040
 It's in the time of the

99
00:06:18,040 --> 00:06:20,040
 Buddha ascetics were big into

100
00:06:20,040 --> 00:06:22,040
 torturing themselves

101
00:06:22,040 --> 00:06:24,040
 So they said those kind of

102
00:06:24,040 --> 00:06:26,040
 torture are not really useful

103
00:06:26,040 --> 00:06:28,040
 What's the best kind of torture?

104
00:06:28,040 --> 00:06:30,040
 The most, the highest form

105
00:06:30,040 --> 00:06:32,040
 of asceticism? Patience

106
00:06:32,040 --> 00:06:34,040
 You know, bearing with

107
00:06:34,040 --> 00:06:36,040
 not only unpleasant

108
00:06:36,040 --> 00:06:38,040
 feelings, but also pleasant feelings

109
00:06:38,040 --> 00:06:40,040
 So not reacting to

110
00:06:40,040 --> 00:06:42,040
 desires, not reacting to

111
00:06:42,040 --> 00:06:44,040
 appealing

112
00:06:44,040 --> 00:06:48,040
 experiences and not acting

113
00:06:48,040 --> 00:06:50,040
 out in regards to

114
00:06:50,040 --> 00:06:52,040
 unappealing experiences

115
00:06:52,040 --> 00:07:00,040
 Nibaanang Paramangwa Danti Buddha

116
00:07:00,040 --> 00:07:02,040
 Nibaan is the highest

117
00:07:02,040 --> 00:07:04,040
 Nibaanang is the highest

118
00:07:04,040 --> 00:07:06,040
 There's nothing in Samsara

119
00:07:06,040 --> 00:07:08,040
 that can compare to Nibaanang

120
00:07:08,040 --> 00:07:10,040
 Nibaanang

121
00:07:10,040 --> 00:07:12,040
 Nibaanang

122
00:07:12,040 --> 00:07:14,040
 Nahepa Pajito Parupagahdi

123
00:07:14,040 --> 00:07:16,040
 One is not a

124
00:07:16,040 --> 00:07:18,040
 mendicant

125
00:07:18,040 --> 00:07:20,040
 I don't know

126
00:07:20,040 --> 00:07:22,040
 Papaji

127
00:07:22,040 --> 00:07:24,040
 Papaji is one who has left the home life

128
00:07:24,040 --> 00:07:26,040
 One is not

129
00:07:26,040 --> 00:07:28,040
 one is not properly left the home life

130
00:07:28,040 --> 00:07:30,040
 who

131
00:07:30,040 --> 00:07:32,040
 attacks others

132
00:07:32,040 --> 00:07:34,040
 who is violent towards others

133
00:07:34,040 --> 00:07:36,040
 mean towards others

134
00:07:36,040 --> 00:07:38,040
 Na Samanoho Deeparang

135
00:07:38,040 --> 00:07:40,040
 We hate Dayantho

136
00:07:40,040 --> 00:07:42,040
 One is not a reckless

137
00:07:42,040 --> 00:07:44,040
 who

138
00:07:44,040 --> 00:07:46,040
 we hate Dayantho

139
00:07:46,040 --> 00:07:48,040
 I can't remember it, something like

140
00:07:48,040 --> 00:07:50,040
 who attacks others or hurts others or scolds

141
00:07:50,040 --> 00:07:52,040
 abuses others

142
00:07:58,040 --> 00:08:00,040
 Anupavadho Anupagahto

143
00:08:00,040 --> 00:08:02,040
 Not scolding others

144
00:08:02,040 --> 00:08:04,040
 Anupavada means

145
00:08:04,040 --> 00:08:06,040
 not speaking harshly towards others

146
00:08:06,040 --> 00:08:08,040
 Anupavada means not acting harshly towards others

147
00:08:08,040 --> 00:08:10,040
 or not hurting others

148
00:08:10,040 --> 00:08:12,040
 Pati Mokhe Jasa Murau

149
00:08:12,040 --> 00:08:14,040
 being restrained by the Pati Moka

150
00:08:14,040 --> 00:08:16,040
 by a code of law

151
00:08:16,040 --> 00:08:20,040
 and the

152
00:08:22,760 --> 00:08:32,760
 knowing moderation in regards to food

153
00:08:32,760 --> 00:08:34,760
 Pantanchasayana Asanang

154
00:08:34,760 --> 00:08:36,760
 having a

155
00:08:36,760 --> 00:08:38,760
 secluded dwelling

156
00:08:38,760 --> 00:08:40,760
 dwelling in seclusion

157
00:08:40,760 --> 00:08:42,760
 and each of these

158
00:08:42,760 --> 00:08:44,760
 is a

159
00:08:49,000 --> 00:09:01,000
 being intent upon the higher

160
00:09:01,000 --> 00:09:03,000
 the higher mind

161
00:09:03,000 --> 00:09:05,000
 so this is meditation

162
00:09:05,000 --> 00:09:07,000
 meditative states being fixed on it

163
00:09:07,000 --> 00:09:09,000
 focused on it

164
00:09:09,000 --> 00:09:11,000
 E Tungu Dhaa Nasa Asanang

165
00:09:11,000 --> 00:09:13,000
 This is the teaching of all the

166
00:09:13,000 --> 00:09:15,000
 Buddhas

167
00:09:15,000 --> 00:09:17,000
 So Maga Buddha is normally in Thailand

168
00:09:17,000 --> 00:09:19,000
 not in Sri Lanka I don't think

169
00:09:19,000 --> 00:09:23,000
 I'm pretty sure they don't know about it in Sri Lanka

170
00:09:23,000 --> 00:09:25,000
 It's a very important

171
00:09:25,000 --> 00:09:27,000
 holiday in Thailand

172
00:09:27,000 --> 00:09:29,000
 or Thai Buddhism

173
00:09:29,000 --> 00:09:35,000
 It's just an excuse to celebrate I suppose

174
00:09:35,000 --> 00:09:37,000
 an excuse to meditate, an excuse to

175
00:09:37,000 --> 00:09:39,000
 have some kind of

176
00:09:39,000 --> 00:09:43,000
 religious activity

177
00:09:45,000 --> 00:09:49,000
 So Happy Maga Boon Chai everyone

178
00:09:51,000 --> 00:09:53,000
 So

179
00:09:55,000 --> 00:09:57,000
 So

180
00:09:59,000 --> 00:10:01,000
 So

181
00:10:25,000 --> 00:10:27,000
 We also have a quote today

182
00:10:27,000 --> 00:10:31,000
 I don't think I'll go into it

183
00:10:31,000 --> 00:10:39,000
 Maybe I'll post the hangout if anybody wants

184
00:10:39,000 --> 00:10:41,000
 if anyone has any questions they want to come on

185
00:10:41,000 --> 00:10:45,000
 Otherwise

186
00:10:45,000 --> 00:10:47,000
 I have a midterm in a couple of days on Buddhism

187
00:10:47,000 --> 00:10:51,000
 We're studying Mahayana Buddhism

188
00:10:51,000 --> 00:10:53,000
 So I have to

189
00:10:53,000 --> 00:10:55,000
 prepare for that as well

190
00:10:55,000 --> 00:10:57,000
 Here's the hangout link

191
00:10:57,000 --> 00:10:59,000
 if anyone wants to come on

192
00:10:59,000 --> 00:11:01,000
 and ask a question

193
00:11:03,000 --> 00:11:05,000
 So

194
00:11:07,000 --> 00:11:09,000
 So

195
00:11:33,000 --> 00:11:35,000
 I don't even know if I have sound

196
00:11:35,000 --> 00:11:37,000
 that makes me...

197
00:11:37,000 --> 00:11:39,000
 Hey Tom

198
00:11:39,000 --> 00:11:41,000
 Aren't you coming here today?

199
00:11:41,000 --> 00:11:43,000
 weren't you coming to

200
00:11:43,000 --> 00:11:45,000
 like I'm not confused

201
00:11:45,000 --> 00:11:47,000
 April 4th

202
00:11:47,000 --> 00:11:49,000
 That's why I'm not there yet

203
00:11:49,000 --> 00:11:51,000
 We have like four Toms coming to meditate

204
00:11:51,000 --> 00:11:53,000
 and it's quite bizarre

205
00:11:53,000 --> 00:11:55,000
 I think actually four Toms not like

206
00:11:55,000 --> 00:11:56,000
 four Toms

207
00:11:56,000 --> 00:11:58,000
 The Chinese year of the Tom of course

208
00:11:58,000 --> 00:12:00,000
 Is that right?

209
00:12:01,000 --> 00:12:03,000
 I do have a question

210
00:12:03,000 --> 00:12:05,000
 Go ahead

211
00:12:05,000 --> 00:12:11,000
 What's your question?

212
00:12:11,000 --> 00:12:13,000
 My question is

213
00:12:13,000 --> 00:12:15,000
 I've been thinking... you mentioned last night

214
00:12:15,000 --> 00:12:17,000
 I believe it was

215
00:12:17,000 --> 00:12:19,000
 about you have the meditators there

216
00:12:19,000 --> 00:12:21,000
 you stick them in a room so to speak

217
00:12:21,000 --> 00:12:23,000
 and they're meditating

218
00:12:23,000 --> 00:12:25,000
 and I've heard you mention

219
00:12:25,000 --> 00:12:27,000
 any number of times

220
00:12:27,000 --> 00:12:29,000
 about no more than six hours

221
00:12:29,000 --> 00:12:31,000
 of sleep in a 24 hour period

222
00:12:31,000 --> 00:12:33,000
 So I'm curious

223
00:12:33,000 --> 00:12:37,000
 I noticed many things

224
00:12:37,000 --> 00:12:39,000
 since I've been seriously meditating

225
00:12:39,000 --> 00:12:41,000
 for a number of months

226
00:12:41,000 --> 00:12:43,000
 Transformations I would say

227
00:12:43,000 --> 00:12:45,000
 I'm curious about the possibility

228
00:12:45,000 --> 00:12:47,000
 that you can

229
00:12:47,000 --> 00:12:49,000
 actually replace sleep

230
00:12:49,000 --> 00:12:51,000
 with meditation

231
00:12:51,000 --> 00:12:53,000
 Is that something that's

232
00:12:53,000 --> 00:12:55,000
 ever done by serious meditators

233
00:12:55,000 --> 00:12:57,000
 where you're really

234
00:12:57,000 --> 00:12:59,000
 not doing much actual sleep

235
00:12:59,000 --> 00:13:01,000
 but meditating instead

236
00:13:01,000 --> 00:13:03,000
 Oh yeah, it's quite common

237
00:13:03,000 --> 00:13:05,000
 Okay

238
00:13:05,000 --> 00:13:07,000
 It's in the text

239
00:13:07,000 --> 00:13:09,000
 there are monks who

240
00:13:09,000 --> 00:13:11,000
 would not sleep for months

241
00:13:11,000 --> 00:13:15,000
 got the first story of the Dhammapada

242
00:13:15,000 --> 00:13:17,000
 Chakupala

243
00:13:17,000 --> 00:13:19,000
 spent three months

244
00:13:19,000 --> 00:13:21,000
 not lying down

245
00:13:21,000 --> 00:13:23,000
 I think that's a good question

246
00:13:23,000 --> 00:13:27,000
 three months not lying down

247
00:13:27,000 --> 00:13:31,000
 with no ill effect

248
00:13:31,000 --> 00:13:33,000
 He lost his eyes

249
00:13:33,000 --> 00:13:35,000
 He lost his eyesight

250
00:13:35,000 --> 00:13:37,000
 he went blind

251
00:13:37,000 --> 00:13:39,000
 but it didn't have to do with not lying

252
00:13:39,000 --> 00:13:41,000
 well it wasn't exactly because he didn't lie down

253
00:13:41,000 --> 00:13:43,000
 there was some

254
00:13:43,000 --> 00:13:45,000
 some condition of the eyes and he had to lie down

255
00:13:45,000 --> 00:13:47,000
 in order to cure it

256
00:13:47,000 --> 00:13:49,000
 in order to get better and he decided not to

257
00:13:52,000 --> 00:13:54,000
 when you don't sleep

258
00:13:54,000 --> 00:13:56,000
 one of the things that happens is you start to

259
00:13:56,000 --> 00:13:58,000
 hallucinate, you start to lose

260
00:13:58,000 --> 00:14:00,000
 some sense of balance

261
00:14:00,000 --> 00:14:02,000
 the effects

262
00:14:02,000 --> 00:14:04,000
 if you look on

263
00:14:04,000 --> 00:14:06,000
 if you look on the internet

264
00:14:06,000 --> 00:14:08,000
 what happened to my voice?

265
00:14:08,000 --> 00:14:10,000
 if you look on the internet

266
00:14:10,000 --> 00:14:12,000
 the effects of not sleeping are

267
00:14:12,000 --> 00:14:14,000
 there are effects of not sleeping that they'll tell you

268
00:14:14,000 --> 00:14:14,000
 about

269
00:14:14,000 --> 00:14:24,000
 I want just to say that none of those effects are

270
00:14:24,000 --> 00:14:24,000
 necessarily deleterious or problematic

271
00:14:24,000 --> 00:14:28,000
 and that being said once you really get into the meditation

272
00:14:28,000 --> 00:14:31,350
 if you get into the groove you can sustain it for at least

273
00:14:31,350 --> 00:14:32,000
 several days

274
00:14:32,000 --> 00:14:34,000
 on the day that you're sleeping

275
00:14:34,000 --> 00:14:36,000
 and then you can get into the meditation

276
00:14:36,000 --> 00:14:38,000
 and then you can get into the meditation

277
00:14:38,000 --> 00:14:40,000
 and then you can get into the meditation

278
00:14:40,000 --> 00:14:42,000
 and then you can get into the meditation

279
00:14:42,000 --> 00:14:46,000
 on little to no sleep

280
00:14:46,000 --> 00:14:48,000
 most people can do that

281
00:14:48,000 --> 00:14:50,000
 to do it for months

282
00:14:50,000 --> 00:14:52,000
 you'd need to, I think you'd need to prepare

283
00:14:52,000 --> 00:14:54,000
 you'd have to leave society

284
00:14:54,000 --> 00:14:58,000
 because the mind is so caught up in so much information

285
00:14:58,000 --> 00:15:00,000
 and stimuli

286
00:15:00,000 --> 00:15:04,000
 that it just works too hard

287
00:15:04,000 --> 00:15:06,000
 to be without sleep

288
00:15:06,000 --> 00:15:08,000
 but if you're living in a forest for some time

289
00:15:08,000 --> 00:15:10,000
 your mind starts to work

290
00:15:10,000 --> 00:15:12,000
 not have to work so hard

291
00:15:12,000 --> 00:15:16,000
 it becomes relaxed, habitually relaxed

292
00:15:16,000 --> 00:15:18,000
 and so you can spend your days

293
00:15:18,000 --> 00:15:22,000
 without building up the need to sleep

294
00:15:22,000 --> 00:15:24,000
 without tiring yourself out

295
00:15:24,000 --> 00:15:26,000
 and then when you don't sleep

296
00:15:26,000 --> 00:15:28,000
 your mind is already calm

297
00:15:28,000 --> 00:15:30,000
 so the first night or two when you stop sleeping

298
00:15:30,000 --> 00:15:32,000
 you hallucinate

299
00:15:32,000 --> 00:15:34,000
 you'll see things in the floor

300
00:15:34,000 --> 00:15:36,000
 walking around

301
00:15:36,000 --> 00:15:38,000
 you'll feel dizzy

302
00:15:38,000 --> 00:15:44,000
 and you get that overtired feeling

303
00:15:44,000 --> 00:15:47,000
 but none of that is really all that deleterious

304
00:15:47,000 --> 00:15:49,000
 I mean they'll say things like

305
00:15:49,000 --> 00:15:52,000
 we repair our body cells when we sleep

306
00:15:52,000 --> 00:15:54,000
 I don't know how that works

307
00:15:54,000 --> 00:15:57,000
 or whether there's proof as to the need for it

308
00:15:57,000 --> 00:16:00,000
 or whether you can show that through meditation

309
00:16:00,000 --> 00:16:02,000
 you're doing some kind of cell rebuilding

310
00:16:02,000 --> 00:16:04,000
 I don't really understand that part of it

311
00:16:04,000 --> 00:16:07,000
 but I know it's been done and it is done

312
00:16:07,000 --> 00:16:09,000
 it is possible, very possible

313
00:16:09,000 --> 00:16:13,000
 Well, if I just

314
00:16:13,000 --> 00:16:17,000
 sleep is important for me

315
00:16:17,000 --> 00:16:19,000
 and quite frankly I

316
00:16:19,000 --> 00:16:21,000
 got back into my meditation practice

317
00:16:21,000 --> 00:16:25,000
 because I wanted to cure myself of insomnia

318
00:16:25,000 --> 00:16:28,000
 and that's pretty much taken care of now

319
00:16:28,000 --> 00:16:31,000
 but now when I wake up in the middle of the night

320
00:16:31,000 --> 00:16:34,750
 and I always meditate when I wake up in the middle of the

321
00:16:34,750 --> 00:16:35,000
 night

322
00:16:35,000 --> 00:16:39,000
 I want to release the anxiety of

323
00:16:39,000 --> 00:16:42,000
 the fear of not being able to go back to sleep

324
00:16:42,000 --> 00:16:44,000
 and if I understand that

325
00:16:44,000 --> 00:16:49,000
 the meditation takes over for the sleep

326
00:16:49,000 --> 00:16:52,000
 then I was hoping for that answer

327
00:16:52,000 --> 00:16:53,000
 Yeah, absolutely

328
00:16:53,000 --> 00:16:56,000
 I mean that's a big part of getting over insomnia

329
00:16:56,000 --> 00:16:58,000
 like I did this once

330
00:16:58,000 --> 00:17:01,000
 and besides, of course I've done days

331
00:17:01,000 --> 00:17:03,000
 many days without sleep

332
00:17:03,000 --> 00:17:08,000
 but one time it wasn't even intentional

333
00:17:08,000 --> 00:17:14,000
 I had been on a flight from Thailand to Los Angeles

334
00:17:14,000 --> 00:17:16,000
 and then I got in in the evening

335
00:17:16,000 --> 00:17:18,000
 and they gave me a coffee

336
00:17:18,000 --> 00:17:19,000
 someone gave me a coffee to drink

337
00:17:19,000 --> 00:17:20,000
 I didn't even think about it

338
00:17:20,000 --> 00:17:22,000
 I just drank the coffee

339
00:17:22,000 --> 00:17:24,000
 and back then I was less

340
00:17:24,000 --> 00:17:28,000
 actually quite less strict in the rules as well

341
00:17:28,000 --> 00:17:31,000
 and now I wouldn't really have coffee in the evening

342
00:17:31,000 --> 00:17:33,000
 so I had this coffee

343
00:17:33,000 --> 00:17:36,060
 and then I realized, you know, what am I doing drinking

344
00:17:36,060 --> 00:17:37,000
 coffee in the evening

345
00:17:37,000 --> 00:17:40,000
 because that was just what they were offering

346
00:17:40,000 --> 00:17:43,000
 but I ended up not being able to sleep at all

347
00:17:43,000 --> 00:17:44,810
 there was no way I was going to sleep because of the jet

348
00:17:44,810 --> 00:17:46,000
 lag, the time change

349
00:17:46,000 --> 00:17:50,000
 and so I just lay in bed all night

350
00:17:50,000 --> 00:17:52,000
 being mindful, meditating

351
00:17:52,000 --> 00:17:54,000
 and when I got up in the morning I really felt fine

352
00:17:54,000 --> 00:17:56,000
 I hadn't slept a wink

353
00:17:56,000 --> 00:17:59,000
 that I remember, what I remember of that night is

354
00:17:59,000 --> 00:18:01,000
 I was up all night

355
00:18:01,000 --> 00:18:07,000
 but because of being mindful

356
00:18:07,000 --> 00:18:10,000
 in the morning there was no ill effect

357
00:18:10,000 --> 00:18:14,000
 well thank you for that

358
00:18:14,000 --> 00:18:21,000
 yeah, absolutely to some extent you can replace

359
00:18:21,000 --> 00:18:25,000
 I don't want to say because I know people are going to say

360
00:18:25,000 --> 00:18:29,270
 I don't know science has evidence to the contrary but

361
00:18:29,270 --> 00:18:30,000
 whatever

362
00:18:30,000 --> 00:18:34,000
 I'm skeptical of such evidence

363
00:18:34,000 --> 00:18:36,760
 because such evidence is always taken from people who need

364
00:18:36,760 --> 00:18:37,000
 sleep

365
00:18:37,000 --> 00:18:40,000
 because their minds are not focused

366
00:18:40,000 --> 00:18:43,000
 it would be neat to do a study on meditators

367
00:18:43,000 --> 00:18:45,000
 who were going without sleep

368
00:18:45,000 --> 00:18:47,980
 and to see the effects and to see whether there was any

369
00:18:47,980 --> 00:18:48,000
 difference

370
00:18:48,000 --> 00:18:52,000
 between them and people who weren't meditating

371
00:18:52,000 --> 00:19:00,000
 but it doesn't take a genius to realize the difference

372
00:19:00,000 --> 00:19:03,000
 when the mind is calm, when the mind is focused

373
00:19:03,000 --> 00:19:07,000
 when the mind is clear

374
00:19:07,000 --> 00:19:14,000
 I have a question Banté

375
00:19:14,000 --> 00:19:15,000
 okay

376
00:19:15,000 --> 00:19:18,000
 okay so we were reading today

377
00:19:18,000 --> 00:19:20,000
 because we were discussing

378
00:19:20,000 --> 00:19:22,000
 over in Second Life

379
00:19:22,000 --> 00:19:26,650
 how the Buddha's enlightenment is distinguished from Arah

380
00:19:26,650 --> 00:19:28,000
ant's enlightenment

381
00:19:28,000 --> 00:19:31,000
 and so we read Bhikkhu Bodhi's translation

382
00:19:31,000 --> 00:19:34,000
 and then I was thinking

383
00:19:34,000 --> 00:19:36,000
 in the teachings of the Buddha

384
00:19:36,000 --> 00:19:39,000
 it is often said that if you come and practice

385
00:19:39,000 --> 00:19:41,000
 you are going to see for yourself

386
00:19:41,000 --> 00:19:45,000
 and we were kind of getting on the topic of

387
00:19:45,000 --> 00:19:48,000
 if there might be enlightened beings

388
00:19:48,000 --> 00:19:50,590
 and if it is still possible for beings to become

389
00:19:50,590 --> 00:19:51,000
 enlightened

390
00:19:51,000 --> 00:19:54,000
 while the Buddha's asana is still alive

391
00:19:54,000 --> 00:19:58,000
 and in some places thriving even

392
00:19:58,000 --> 00:20:02,000
 so I would love to hear your thoughts on that

393
00:20:02,000 --> 00:20:09,000
 well it's not a matter of whether it's still possible

394
00:20:09,000 --> 00:20:11,820
 I mean the teachings are there, the teachings are still

395
00:20:11,820 --> 00:20:12,000
 here

396
00:20:12,000 --> 00:20:15,000
 the only way it wouldn't be possible is if there were

397
00:20:15,000 --> 00:20:18,000
 either no teachings or no one practicing them

398
00:20:18,000 --> 00:20:21,000
 so if you practice them

399
00:20:21,000 --> 00:20:24,000
 the results are there

400
00:20:24,000 --> 00:20:28,000
 you also need people teaching them and guiding through them

401
00:20:28,000 --> 00:20:33,000
 because many of us teachings themselves aren't enough to

402
00:20:33,000 --> 00:20:37,000
 push you to practice them

403
00:20:37,000 --> 00:20:42,000
 wonderful

404
00:20:42,000 --> 00:20:45,810
 yeah that was my kind of the direction of my thought as

405
00:20:45,810 --> 00:20:46,000
 well

406
00:20:46,000 --> 00:20:50,000
 if one visits where the teaching of the Buddha is live

407
00:20:50,000 --> 00:20:52,000
 and there are good teachers

408
00:20:52,000 --> 00:20:55,000
 I mean I was just like of course it's possible

409
00:20:55,000 --> 00:20:58,000
 it must be possible so thanks

410
00:20:58,000 --> 00:21:01,000
 absolutely the doors are not closed

411
00:21:01,000 --> 00:21:05,000
 wonderful

412
00:21:05,000 --> 00:21:08,000
 okay

413
00:21:08,000 --> 00:21:10,000
 have a good night

414
00:21:10,000 --> 00:21:12,000
 good night everyone

415
00:21:12,000 --> 00:21:13,000
 bye bye

416
00:21:13,000 --> 00:21:15,000
 good

417
00:21:17,000 --> 00:21:18,000
 good night

418
00:21:18,000 --> 00:21:19,000
 good night

419
00:21:19,000 --> 00:21:20,000
 good night

420
00:21:20,000 --> 00:21:21,000
 good night

421
00:21:21,000 --> 00:21:22,000
 good night

422
00:21:22,000 --> 00:21:23,000
 good night

423
00:21:23,000 --> 00:21:24,000
 good night

