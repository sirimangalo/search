1
00:00:00,000 --> 00:00:03,840
 Hi, and welcome back to Ask a Monk.

2
00:00:03,840 --> 00:00:08,560
 Today's question is from Aisanim.

3
00:00:08,560 --> 00:00:12,370
 Meditation seems investigative and empirical, but Buddhist

4
00:00:12,370 --> 00:00:14,600
 texts claim supernatural entities,

5
00:00:14,600 --> 00:00:17,040
 realms and laws exist.

6
00:00:17,040 --> 00:00:20,360
 Are these claims relevant to Buddhist practice today?

7
00:00:20,360 --> 00:00:23,720
 How do you reconcile seeing things as they are with taking

8
00:00:23,720 --> 00:00:24,880
 things on faith?

9
00:00:24,880 --> 00:00:30,390
 Well, first a note that as I understand the word

10
00:00:30,390 --> 00:00:34,840
 supernatural, Buddhist texts don't claim

11
00:00:34,840 --> 00:00:38,400
 the existence of anything supernatural.

12
00:00:38,400 --> 00:00:43,720
 What Buddhist texts claim is that these entities, realms

13
00:00:43,720 --> 00:00:46,920
 and laws are perfectly natural.

14
00:00:46,920 --> 00:00:51,680
 And the claim is then that the laws of physics are only a

15
00:00:51,680 --> 00:00:55,080
 small part of the natural reality.

16
00:00:55,080 --> 00:01:00,140
 And this is something that we often take on faith, and I

17
00:01:00,140 --> 00:01:02,840
 want to explain how we answer

18
00:01:02,840 --> 00:01:08,310
 this idea of taking things on faith versus seeing things as

19
00:01:08,310 --> 00:01:09,520
 they are.

20
00:01:09,520 --> 00:01:12,280
 There are two answers to it.

21
00:01:12,280 --> 00:01:18,880
 The first one obviously is that when we understand these

22
00:01:18,880 --> 00:01:22,500
 things to be natural, we are not taking

23
00:01:22,500 --> 00:01:25,560
 them on faith and we are investigating them.

24
00:01:25,560 --> 00:01:31,640
 So much of the claims that seem to be supernatural or are

25
00:01:31,640 --> 00:01:35,800
 extraordinary in Buddhism are actually

26
00:01:35,800 --> 00:01:40,650
 investigatable that you can empirically investigate things

27
00:01:40,650 --> 00:01:43,680
 like past lives, you can investigate

28
00:01:43,680 --> 00:01:47,110
 heaven, you can investigate magical powers, so-called

29
00:01:47,110 --> 00:01:48,280
 magical powers.

30
00:01:48,280 --> 00:01:52,740
 All of these super mundane or extraordinary experiences and

31
00:01:52,740 --> 00:01:55,360
 realities can all be investigated

32
00:01:55,360 --> 00:01:57,320
 through practice.

33
00:01:57,320 --> 00:02:01,330
 And so the reason why we might take those things on faith

34
00:02:01,330 --> 00:02:03,640
 is in the beginning is because

35
00:02:03,640 --> 00:02:06,840
 we are studying to realize some of them.

36
00:02:06,840 --> 00:02:09,820
 Now obviously most of these things are not necessary to

37
00:02:09,820 --> 00:02:11,520
 become free from suffering or

38
00:02:11,520 --> 00:02:15,310
 to see things clearly or to reach the goal of Buddhism, but

39
00:02:15,310 --> 00:02:17,040
 there are many people who

40
00:02:17,040 --> 00:02:18,920
 do practice to realize them.

41
00:02:18,920 --> 00:02:22,840
 And there are many things which are a part of the path as

42
00:02:22,840 --> 00:02:24,880
 far as gaining high states

43
00:02:24,880 --> 00:02:27,360
 of concentration.

44
00:02:27,360 --> 00:02:31,650
 And even things like remembering one's past lives could be

45
00:02:31,650 --> 00:02:32,600
 helpful.

46
00:02:32,600 --> 00:02:36,360
 Although none of these things are necessary for the path.

47
00:02:36,360 --> 00:02:40,660
 But we can take them on faith and if we are interested in

48
00:02:40,660 --> 00:02:43,180
 them, then we take them on faith

49
00:02:43,180 --> 00:02:48,870
 in the same way as a physics student will take on with

50
00:02:48,870 --> 00:02:52,120
 faith the theories and laws proposed

51
00:02:52,120 --> 00:02:55,910
 by those above them, those who have come before them while

52
00:02:55,910 --> 00:02:57,920
 they study them and while they

53
00:02:57,920 --> 00:03:00,820
 examine them and do experiments on them and while they try

54
00:03:00,820 --> 00:03:01,960
 to understand them.

55
00:03:01,960 --> 00:03:06,640
 Many of these things, once you come to understand the basic

56
00:03:06,640 --> 00:03:09,240
 building blocks of reality and you

57
00:03:09,240 --> 00:03:12,560
 come to realize that the best way to approach what is the

58
00:03:12,560 --> 00:03:14,360
 meaning of reality or the best

59
00:03:14,360 --> 00:03:17,870
 way to understand reality is from a basis of the mind, a

60
00:03:17,870 --> 00:03:19,720
 basis of consciousness and

61
00:03:19,720 --> 00:03:23,410
 that our consciousness actually creates our reality which

62
00:03:23,410 --> 00:03:25,200
 is perfectly in line with the

63
00:03:25,200 --> 00:03:28,960
 laws of quantum physics for example.

64
00:03:28,960 --> 00:03:32,190
 When you start to open up to such things and start to

65
00:03:32,190 --> 00:03:34,900
 realize that these things are perfectly

66
00:03:34,900 --> 00:03:37,400
 in line with reality, there's no reason why they shouldn't

67
00:03:37,400 --> 00:03:38,240
 be able to exist.

68
00:03:38,240 --> 00:03:42,230
 Though we may not have proof, we can take them on faith in

69
00:03:42,230 --> 00:03:44,440
 the meantime or we can suspend

70
00:03:44,440 --> 00:03:49,680
 our judgment and then practice to realize them.

71
00:03:49,680 --> 00:03:54,270
 I think the other more important and more immediately

72
00:03:54,270 --> 00:03:57,080
 applicable or practical answer

73
00:03:57,080 --> 00:03:59,660
 to this question is that we take a lot of these things on

74
00:03:59,660 --> 00:04:01,080
 faith because they're not

75
00:04:01,080 --> 00:04:03,000
 of much meaning to us.

76
00:04:03,000 --> 00:04:07,360
 For instance, when someone asks, "Do you believe in

77
00:04:07,360 --> 00:04:08,720
 Australia?"

78
00:04:08,720 --> 00:04:10,850
 And if you're from America, you've probably never been to

79
00:04:10,850 --> 00:04:12,120
 Australia so you can say, "Yes,

80
00:04:12,120 --> 00:04:13,120
 I believe in Australia."

81
00:04:13,120 --> 00:04:14,120
 And you say, "What?

82
00:04:14,120 --> 00:04:15,120
 You've never been there?

83
00:04:15,120 --> 00:04:16,120
 You've never seen it?"

84
00:04:16,120 --> 00:04:18,260
 And we can say, "Well, I believe people because they've

85
00:04:18,260 --> 00:04:19,880
 been there and they've told me about

86
00:04:19,880 --> 00:04:20,880
 it."

87
00:04:20,880 --> 00:04:23,150
 But we also believe it because we don't have any reason not

88
00:04:23,150 --> 00:04:23,960
 to believe it.

89
00:04:23,960 --> 00:04:26,670
 We don't have any reason to be concerned about its

90
00:04:26,670 --> 00:04:28,520
 existence or non-existence.

91
00:04:28,520 --> 00:04:31,840
 If we're wrong, it doesn't influence our life in any way.

92
00:04:31,840 --> 00:04:37,760
 It doesn't mean anything for the daily...

93
00:04:37,760 --> 00:04:42,720
 It isn't a big problem for us if we turn out to be wrong.

94
00:04:42,720 --> 00:04:45,250
 So we say, "Yeah, I believe these people, they've said that

95
00:04:45,250 --> 00:04:45,280
."

96
00:04:45,280 --> 00:04:47,800
 And we don't investigate it because we say to ourselves, "

97
00:04:47,800 --> 00:04:49,120
If it's wrong, then it's not

98
00:04:49,120 --> 00:04:50,120
 a big deal."

99
00:04:50,120 --> 00:04:53,140
 So most of these things, the belief in gods, the belief in

100
00:04:53,140 --> 00:04:54,560
 angels, the belief in other

101
00:04:54,560 --> 00:04:57,890
 realms of existence and so on, well, on the one hand, they

102
00:04:57,890 --> 00:04:59,520
 make sense from the point of

103
00:04:59,520 --> 00:05:03,480
 view of the creation of existence or the progress of

104
00:05:03,480 --> 00:05:06,740
 existence based on the mind that if we can

105
00:05:06,740 --> 00:05:10,920
 exist as human beings, there's nothing special about this

106
00:05:10,920 --> 00:05:13,360
 reality that makes it more likely

107
00:05:13,360 --> 00:05:16,000
 than say the angel realms or so on.

108
00:05:16,000 --> 00:05:19,800
 So we can understand it in terms of the probability of its

109
00:05:19,800 --> 00:05:20,840
 existence.

110
00:05:20,840 --> 00:05:22,900
 But then we can also say, "Well, even if they don't exist,

111
00:05:22,900 --> 00:05:24,800
 it doesn't mean that much to me.

112
00:05:24,800 --> 00:05:27,580
 I see that the meditation practice is helping me and I'm

113
00:05:27,580 --> 00:05:29,280
 here to practice meditation and

114
00:05:29,280 --> 00:05:32,780
 I'm following the Buddha's teaching, not specifically

115
00:05:32,780 --> 00:05:35,640
 because maybe one day I'll gain magic powers

116
00:05:35,640 --> 00:05:38,900
 or because I've been born before or so on and so on, but

117
00:05:38,900 --> 00:05:40,720
 because when I practice meditation,

118
00:05:40,720 --> 00:05:42,320
 I see that it does good things for me.

119
00:05:42,320 --> 00:05:44,040
 I see that it helps me."

120
00:05:44,040 --> 00:05:48,410
 And so we don't worry or concern ourselves too much about

121
00:05:48,410 --> 00:05:49,800
 these things.

122
00:05:49,800 --> 00:05:52,500
 So I would say that's two ways of answering this question.

123
00:05:52,500 --> 00:05:55,460
 On the one hand, they're not that supernatural and they're

124
00:05:55,460 --> 00:05:57,000
 not that hard to understand if

125
00:05:57,000 --> 00:06:00,240
 you practice meditation.

126
00:06:00,240 --> 00:06:04,780
 And on the other hand, they're not of that great importance

127
00:06:04,780 --> 00:06:05,600
 for us.

128
00:06:05,600 --> 00:06:09,540
 It's important for us to have some sort of willingness to

129
00:06:09,540 --> 00:06:11,800
 investigate some of the claims

130
00:06:11,800 --> 00:06:16,120
 that the Buddha made in terms of the cause of suffering and

131
00:06:16,120 --> 00:06:18,000
 the nature of the mind so

132
00:06:18,000 --> 00:06:20,340
 that we can then experiment and say, "Is it true?

133
00:06:20,340 --> 00:06:22,660
 Is it real?" and come to see it clearly.

134
00:06:22,660 --> 00:06:25,030
 But there's many things that the Buddha said that we may

135
00:06:25,030 --> 00:06:26,420
 choose not to investigate and

136
00:06:26,420 --> 00:06:29,400
 we may just take on faith and say, "If it were a big deal

137
00:06:29,400 --> 00:06:31,320
 for me, I'd investigate it."

138
00:06:31,320 --> 00:06:34,780
 But for my benefit, for my practice, it's not that

139
00:06:34,780 --> 00:06:35,840
 important.

140
00:06:35,840 --> 00:06:36,840
 Okay?

141
00:06:36,840 --> 00:06:39,860
 So I hope that helps to clear up my understanding of the

142
00:06:39,860 --> 00:06:42,120
 Buddha's position on these issues.

143
00:06:42,120 --> 00:06:43,120
 Okay?

