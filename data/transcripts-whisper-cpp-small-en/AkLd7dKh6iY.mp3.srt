1
00:00:00,000 --> 00:00:06,000
 I don't think it's your fault.

2
00:00:06,000 --> 00:00:08,000
 I think it's just me.

3
00:00:08,000 --> 00:00:22,480
 Right, because that's not what it's supposed to do.

4
00:00:22,480 --> 00:00:23,960
 The thought isn't the problem.

5
00:00:23,960 --> 00:00:26,320
 Your reaction to the thought is the problem.

6
00:00:26,320 --> 00:00:32,000
 There's no problem with the thought staying, going, turning

7
00:00:32,000 --> 00:00:35,080
 around in circles.

8
00:00:35,080 --> 00:00:36,080
 That's inconsequential.

9
00:00:36,080 --> 00:00:38,000
 What is consequential is how you react to it.

10
00:00:38,000 --> 00:00:39,440
 The fact that you don't like the thought.

11
00:00:39,440 --> 00:00:42,310
 The fact that you want the thought to go away, those are

12
00:00:42,310 --> 00:00:43,280
 the problems.

13
00:00:43,280 --> 00:00:44,880
 Thought is just reality.

14
00:00:44,880 --> 00:00:47,400
 Once you see thought for what it is, you reject it.

15
00:00:47,400 --> 00:00:54,520
 You have no use for it.

16
00:00:54,520 --> 00:00:57,770
 So the fact that you want the thought to go away is the

17
00:00:57,770 --> 00:00:58,520
 problem.

18
00:00:58,520 --> 00:01:03,080
 That's what's keeping it from actually going away.

19
00:01:03,080 --> 00:01:09,110
 You have some tension there, which creates all sorts of

20
00:01:09,110 --> 00:01:11,640
 neural pathways, strengthens

21
00:01:11,640 --> 00:01:25,640
 neural pathways and actually brings the thought back again.

