1
00:00:00,000 --> 00:00:14,000
 [silence]

2
00:00:14,000 --> 00:00:35,000
 [silence]

3
00:00:35,000 --> 00:00:49,000
 Good evening everyone. I'm broadcasting live April 1st.

4
00:00:49,000 --> 00:00:56,720
 Today's quote, interestingly enough, isn't it Buddha Whach

5
00:00:56,720 --> 00:00:58,000
ana?

6
00:00:58,000 --> 00:01:08,000
 It isn't even... it isn't even Arya Saba Bhagavatana.

7
00:01:08,000 --> 00:01:12,000
 I don't think. I can't remember now.

8
00:01:12,000 --> 00:01:29,840
 Piseyinadi, it's the words of Piseyinadi, King Piseyinadi K

9
00:01:29,840 --> 00:01:31,000
osa.

10
00:01:31,000 --> 00:01:35,000
 Piseyinadi was a king, right?

11
00:01:35,000 --> 00:01:41,080
 The Anagata Whangsa says that he is a bodhisattva and he

12
00:01:41,080 --> 00:01:50,000
 will be the fourth future Buddha from now.

13
00:01:50,000 --> 00:01:56,000
 So he wasn't even enlightened, these are his words.

14
00:01:56,000 --> 00:02:04,100
 But how they can stand is once he said them, the Buddha

15
00:02:04,100 --> 00:02:09,000
 reaffirmed the king's words.

16
00:02:09,000 --> 00:02:16,000
 Anyway, they're spot on and they're worth repeating.

17
00:02:16,000 --> 00:02:18,930
 But we also have the Buddha's response to him which is

18
00:02:18,930 --> 00:02:20,000
 maybe of interest.

19
00:02:20,000 --> 00:02:23,710
 So we'll look at the verses that the Buddha used to respond

20
00:02:23,710 --> 00:02:25,000
 to Piseyinadi.

21
00:02:25,000 --> 00:02:31,840
 Piseyinadi basically says, he was wondering while he was

22
00:02:31,840 --> 00:02:33,000
 sitting,

23
00:02:33,000 --> 00:02:39,000
 "Raho gatasa" while I was in seclusion.

24
00:02:39,000 --> 00:02:44,250
 "Who treat themselves as dear and who treat themselves as a

25
00:02:44,250 --> 00:02:45,000
 foe?

26
00:02:45,000 --> 00:03:03,000
 Kesang nukko pyo atta" of whom, for whom are Kesang.

27
00:03:03,000 --> 00:03:08,000
 Pyo atta.

28
00:03:08,000 --> 00:03:14,000
 Anyway, it's something like whose self is dear.

29
00:03:14,000 --> 00:03:18,000
 For whom is the self dear? I don't quite get no Kesang.

30
00:03:18,000 --> 00:03:26,000
 For whom is the self dear?

31
00:03:26,000 --> 00:03:31,000
 For whom is the self not dear?

32
00:03:31,000 --> 00:03:37,900
 I guess it's just plural because for which people is the

33
00:03:37,900 --> 00:03:39,000
 self?

34
00:03:39,000 --> 00:03:45,150
 Singular, is the self dear? For which people is the self

35
00:03:45,150 --> 00:03:47,000
 not dear?

36
00:03:47,000 --> 00:03:53,000
 One's self.

37
00:03:53,000 --> 00:03:58,000
 Kesang nukko pyo atta.

38
00:03:58,000 --> 00:04:02,000
 Who treats their self? That's how it's translated, right?

39
00:04:02,000 --> 00:04:09,000
 Who treats themselves well?

40
00:04:09,000 --> 00:04:12,000
 Because you can say everyone holds themselves dear, right?

41
00:04:12,000 --> 00:04:17,000
 Everyone cares about themselves.

42
00:04:17,000 --> 00:04:21,000
 But he thought to himself, even if some people say,

43
00:04:21,000 --> 00:04:31,000
 "Pyo no atta, our self is dear."

44
00:04:31,000 --> 00:04:36,000
 Still, to them the self is not dear.

45
00:04:36,000 --> 00:04:39,000
 "Tangi sahito," what is the cause?

46
00:04:39,000 --> 00:04:44,000
 "Yani apyo apya sah kalaya,"

47
00:04:44,000 --> 00:04:50,370
 what a person who is the opposite of dear, un-dear, not

48
00:04:50,370 --> 00:04:51,000
 dear,

49
00:04:51,000 --> 00:04:55,600
 might do to someone who is another person who they're not

50
00:04:55,600 --> 00:04:57,000
 dear towards,

51
00:04:57,000 --> 00:05:01,690
 they're not kindly disposed towards. So an enemy might do

52
00:05:01,690 --> 00:05:03,000
 to an enemy.

53
00:05:03,000 --> 00:05:13,000
 "Tangte atana wa atano karonti."

54
00:05:13,000 --> 00:05:22,000
 Some people, sorry, they do that to themselves.

55
00:05:22,000 --> 00:05:26,290
 What an enemy might do to an enemy, some people do that to

56
00:05:26,290 --> 00:05:28,000
 themselves.

57
00:05:28,000 --> 00:05:30,000
 They themselves do it to themselves.

58
00:05:30,000 --> 00:05:35,270
 What this means is people who have evil thoughts, is what

59
00:05:35,270 --> 00:05:37,000
 he says,

60
00:05:37,000 --> 00:05:46,760
 who do evil deeds, who say evil things, and who have evil

61
00:05:46,760 --> 00:05:48,000
 thoughts.

62
00:05:48,000 --> 00:06:01,000
 "Yogye chakho ke chikahe na ducharitang chalanti."

63
00:06:01,000 --> 00:06:07,000
 "Wajayah ducharitang chalanti." "Manasa ducharitang chal

64
00:06:07,000 --> 00:06:07,000
anti."

65
00:06:07,000 --> 00:06:14,000
 "Chalanti" means they do what they act.

66
00:06:14,000 --> 00:06:20,000
 "Ducharitang," they act in a poor way, in an evil way.

67
00:06:20,000 --> 00:06:25,000
 "Kahye na waspadi." "Wajayah wispid."

68
00:06:25,000 --> 00:06:29,000
 "Manasa wispamay."

69
00:06:29,000 --> 00:06:32,000
 Again, these are the three doors.

70
00:06:32,000 --> 00:06:35,140
 Everything good that we do, everything evil that we do, we

71
00:06:35,140 --> 00:06:36,000
 do through these doors.

72
00:06:36,000 --> 00:06:40,100
 Everything that we do, it's either through body, speech, or

73
00:06:40,100 --> 00:06:41,000
 thought.

74
00:06:41,000 --> 00:06:46,000
 But of interest is the ethical and unethical deeds.

75
00:06:46,000 --> 00:06:51,870
 And so the important point here is that someone who does

76
00:06:51,870 --> 00:06:53,000
 bad deeds,

77
00:06:53,000 --> 00:06:59,530
 well, the important point here is how Buddhist ethics works

78
00:06:59,530 --> 00:07:00,000
.

79
00:07:00,000 --> 00:07:04,000
 That it's not because someone says it's bad,

80
00:07:04,000 --> 00:07:09,000
 it's not because you believe something is bad,

81
00:07:09,000 --> 00:07:13,000
 it's because certain things are inherently contradictory.

82
00:07:13,000 --> 00:07:18,660
 As I've said again and again, you do something thinking

83
00:07:18,660 --> 00:07:20,000
 that it'll make you happy,

84
00:07:20,000 --> 00:07:23,490
 and it doesn't make you happy. You do something with a

85
00:07:23,490 --> 00:07:24,000
 certain intention,

86
00:07:24,000 --> 00:07:27,510
 with a certain intended goal, and that intended goal is not

87
00:07:27,510 --> 00:07:30,000
 reached by that action,

88
00:07:30,000 --> 00:07:32,000
 which is inherently contradictory.

89
00:07:32,000 --> 00:07:37,020
 There's a problem, I think, in a very sort of objective

90
00:07:37,020 --> 00:07:38,000
 sense.

91
00:07:38,000 --> 00:07:49,000
 There's an inconsistency or a lack of logic or reason.

92
00:07:49,000 --> 00:07:54,000
 That's inherent in evil deeds.

93
00:07:54,000 --> 00:07:59,070
 The point being, we do and say and think things, certain

94
00:07:59,070 --> 00:08:00,000
 things,

95
00:08:00,000 --> 00:08:05,610
 that go counter to our own intentions, that lead to results

96
00:08:05,610 --> 00:08:08,000
 counter to our own intentions.

97
00:08:08,000 --> 00:08:12,040
 By their very nature, now you can have good intentions

98
00:08:12,040 --> 00:08:14,000
 wanting to do something good for someone,

99
00:08:14,000 --> 00:08:16,000
 and it doesn't work out, so that's not the point.

100
00:08:16,000 --> 00:08:20,050
 But there are certain things that they themselves lead to

101
00:08:20,050 --> 00:08:22,000
 the opposite,

102
00:08:22,000 --> 00:08:26,000
 or are conducive to the opposite of one's intentions.

103
00:08:26,000 --> 00:08:33,280
 So we kill and steal and lie and cheat intrinsically with

104
00:08:33,280 --> 00:08:36,000
 the thought that

105
00:08:36,000 --> 00:08:41,570
 it's going to bring us some benefit or ameliorate some

106
00:08:41,570 --> 00:08:44,000
 disadvantage.

107
00:08:44,000 --> 00:08:47,000
 Something bad is happening, we're in a bad situation,

108
00:08:47,000 --> 00:08:51,530
 we do bad things to get out of a bad situation, for example

109
00:08:51,530 --> 00:08:52,000
.

110
00:08:52,000 --> 00:08:56,000
 We hurt others. Now they're called bad in Buddhism,

111
00:08:56,000 --> 00:09:01,560
 not because God or Buddha or monks or teachers say they're

112
00:09:01,560 --> 00:09:02,000
 bad,

113
00:09:02,000 --> 00:09:05,000
 but they're bad because it's a bad choice.

114
00:09:05,000 --> 00:09:14,000
 It's akusala, unskillful, inept.

115
00:09:14,000 --> 00:09:19,000
 It's something that doesn't lead to a solution.

116
00:09:19,000 --> 00:09:24,000
 And so you end up unhappy with the results.

117
00:09:24,000 --> 00:09:29,540
 By their very nature, not just by chance or because your

118
00:09:29,540 --> 00:09:32,000
 good plans are foiled,

119
00:09:32,000 --> 00:09:35,630
 but killing and stealing and lying and cheating and taking

120
00:09:35,630 --> 00:09:36,000
 drugs and alcohol

121
00:09:36,000 --> 00:09:40,000
 and even thinking bad thoughts of wanting to hurt others

122
00:09:40,000 --> 00:09:50,330
 or thoughts of greed and so on, thoughts of arrogance and

123
00:09:50,330 --> 00:09:53,000
 conceit.

124
00:09:53,000 --> 00:09:55,000
 These things can do to our suffering.

125
00:09:55,000 --> 00:10:02,000
 They can do to the disruption of our plans.

126
00:10:02,000 --> 00:10:06,820
 Anytime you incorporate these into a plan, it can only go

127
00:10:06,820 --> 00:10:08,000
 sour.

128
00:10:08,000 --> 00:10:11,000
 When you do something, you have a good intention,

129
00:10:11,000 --> 00:10:13,150
 but then you get greedy about it or you get angry or

130
00:10:13,150 --> 00:10:14,000
 frustrated

131
00:10:14,000 --> 00:10:18,370
 or you become arrogant or conceited, even if you're good

132
00:10:18,370 --> 00:10:19,000
 plans

133
00:10:19,000 --> 00:10:23,000
 to help yourself, to help others.

134
00:10:23,000 --> 00:10:26,000
 The greed, anger and delusion, whether by body,

135
00:10:26,000 --> 00:10:30,700
 you do things to hurt others or to manipulate others or to

136
00:10:30,700 --> 00:10:33,000
 oppress others.

137
00:10:33,000 --> 00:10:36,000
 You say things to hurt others.

138
00:10:36,000 --> 00:10:41,000
 You say things out of selfishness and so on.

139
00:10:41,000 --> 00:10:47,000
 Or even you think things that are harmful.

140
00:10:47,000 --> 00:10:50,140
 These things disturb your plans because of the results that

141
00:10:50,140 --> 00:10:51,000
 they have with other people,

142
00:10:51,000 --> 00:10:55,000
 because of the effect they have on your mind,

143
00:10:55,000 --> 00:11:06,280
 because of the disruption that they cause on so many levels

144
00:11:06,280 --> 00:11:07,000
.

145
00:11:07,000 --> 00:11:12,000
 And he says conversely that someone who has good thoughts,

146
00:11:12,000 --> 00:11:19,730
 someone whose mind is good bodily and verbal and mental

147
00:11:19,730 --> 00:11:21,000
 actions,

148
00:11:21,000 --> 00:11:24,900
 whose body, speech and mind appear, even if they were to

149
00:11:24,900 --> 00:11:26,000
 say,

150
00:11:26,000 --> 00:11:30,000
 "Kinjaapitei wang wadei yong piyo no ata."

151
00:11:30,000 --> 00:11:33,000
 Wait a second.

152
00:11:33,000 --> 00:11:34,000
 Oh no, okay.

153
00:11:34,000 --> 00:11:37,000
 "Aap piyo no ata."

154
00:11:37,000 --> 00:11:39,000
 "Ata kotei sang piyo ata."

155
00:11:39,000 --> 00:11:43,000
 Even still they would be dear to themselves. Why?

156
00:11:43,000 --> 00:11:47,590
 Because "yan hi piyo piyo sa kareya" would a dear one may

157
00:11:47,590 --> 00:11:51,000
 do to one who is dear to them.

158
00:11:51,000 --> 00:12:08,000
 "Tang de atana wa atano karo

159
00:12:08,000 --> 00:12:09,000
 takareya."

160
00:12:09,000 --> 00:12:11,000
 And the Buddha does this.

161
00:12:11,000 --> 00:12:15,010
 I mean it's a critic or a skeptic would say it's probably

162
00:12:15,010 --> 00:12:17,000
 just a way of,

163
00:12:17,000 --> 00:12:22,000
 this was probably just added or manipulated

164
00:12:22,000 --> 00:12:25,650
 because you couldn't include the words of the king without

165
00:12:25,650 --> 00:12:28,000
 having the Buddha repeat them.

166
00:12:28,000 --> 00:12:31,000
 Otherwise it would not be Buddha vatjana.

167
00:12:31,000 --> 00:12:32,000
 So I don't know about that.

168
00:12:32,000 --> 00:12:36,000
 But the text has it that the Buddha repeated what he said.

169
00:12:36,000 --> 00:12:38,000
 It doesn't really matter.

170
00:12:38,000 --> 00:12:43,000
 The point is it's a useful teaching.

171
00:12:43,000 --> 00:12:45,000
 Something proper to say.

172
00:12:45,000 --> 00:12:48,090
 And it shows that the king for all his faults did have some

173
00:12:48,090 --> 00:12:53,000
 good ideas from listening to the Buddha.

174
00:12:53,000 --> 00:12:55,000
 And so we take wisdom wherever it is.

175
00:12:55,000 --> 00:12:58,110
 Even the Buddha said, "I agree when another teacher in

176
00:12:58,110 --> 00:13:01,670
 another religion or another tradition says something that's

177
00:13:01,670 --> 00:13:04,000
 right, I would agree with it.

178
00:13:04,000 --> 00:13:06,540
 If they say something that's wrong, I would disagree with

179
00:13:06,540 --> 00:13:07,000
 that."

180
00:13:07,000 --> 00:13:11,000
 This isn't sectarian.

181
00:13:11,000 --> 00:13:14,000
 Wisdom doesn't know religions.

182
00:13:14,000 --> 00:13:20,000
 Truth doesn't stay within one religion.

183
00:13:20,000 --> 00:13:23,430
 It doesn't mean that everything every religion teaches is

184
00:13:23,430 --> 00:13:24,000
 right.

185
00:13:24,000 --> 00:13:30,600
 It just means that the truth is not interested in such

186
00:13:30,600 --> 00:13:32,000
 things.

187
00:13:32,000 --> 00:13:37,280
 So anyway, the Buddha gives some verses sort of as an

188
00:13:37,280 --> 00:13:40,000
 addition to this teaching.

189
00:13:40,000 --> 00:13:47,000
 Ata nan jey piyang janya nan nang papi na sang yu jey

190
00:13:47,000 --> 00:13:55,290
 nahi tang sala bang ho ti, sula bang ho ti, sukang du kat t

191
00:13:55,290 --> 00:13:57,000
akarina

192
00:13:57,000 --> 00:14:01,000
 It is not easy. It is not easily gained.

193
00:14:01,000 --> 00:14:09,000
 Happiness for one who does evil.

194
00:14:09,000 --> 00:14:11,000
 There is a good Buddha quote.

195
00:14:11,000 --> 00:14:17,000
 Nahi tang sala bang ho ti, sukang du kat takarina

196
00:14:17,000 --> 00:14:21,000
 For indeed it is not easily done.

197
00:14:21,000 --> 00:14:28,000
 It is not easily gained. Happiness for one who does evil.

198
00:14:28,000 --> 00:14:39,000
 anta key na di pan nasa, jahatoma nus sang babang

199
00:14:39,000 --> 00:14:42,000
 When one is seized, I'll read Bhikkhu Bodhi's translation.

200
00:14:42,000 --> 00:14:45,820
 When one is seized by the end-maker as one discards the

201
00:14:45,820 --> 00:14:47,000
 human state,

202
00:14:47,000 --> 00:14:51,620
 what can one truly call one's own? What does one take when

203
00:14:51,620 --> 00:14:52,000
 one goes?

204
00:14:52,000 --> 00:14:58,000
 What follows one along like a shadow that never departs?

205
00:14:58,000 --> 00:15:02,300
 So here is a question the Buddha often asks, what do we

206
00:15:02,300 --> 00:15:04,000
 take with us?

207
00:15:04,000 --> 00:15:07,000
 What follows along like a shadow that never departs,

208
00:15:07,000 --> 00:15:12,000
 which is a reference to the first Dhammapandavars, right?

209
00:15:12,000 --> 00:15:18,530
 The second Dhammapandavars, actually. But our good and evil

210
00:15:18,530 --> 00:15:19,000
 deeds

211
00:15:19,000 --> 00:15:24,000
 are what follow us.

212
00:15:24,000 --> 00:15:26,000
 Actually, no, that's not the verse.

213
00:15:26,000 --> 00:15:31,000
 But they use the word, it's the imagery just of the shadow.

214
00:15:31,000 --> 00:15:36,000
 But he uses this idea of the shadow in other ways.

215
00:15:36,000 --> 00:15:38,000
 And in this way it's in other places.

216
00:15:38,000 --> 00:15:49,000
 Chayava anapa yini, it's the same anapa yini, it's the same

217
00:15:49,000 --> 00:15:49,000
,

218
00:15:49,000 --> 00:15:52,000
 it's the same wording as the second Dhammapandavars.

219
00:15:52,000 --> 00:15:53,000
 But it's a different, similarly.

220
00:15:53,000 --> 00:15:56,000
 This is talking about good deeds following us around.

221
00:15:56,000 --> 00:15:59,220
 Actually, in the same way, good and evil deeds following us

222
00:15:59,220 --> 00:16:00,000
 around.

223
00:16:00,000 --> 00:16:10,000
 Ubo punyanchapa pancha yang macho kurute idha.

224
00:16:10,000 --> 00:16:23,000
 Both good and evil, these that a mortal does right here.

225
00:16:23,000 --> 00:16:24,000
 The mortal does here.

226
00:16:24,000 --> 00:16:26,000
 The good and evil that we do here.

227
00:16:26,000 --> 00:16:34,000
 Tanhita sasa kang hoti tancha aadai yagacchiti.

228
00:16:34,000 --> 00:16:42,000
 This is, this is our own, right? This indeed is one's own.

229
00:16:42,000 --> 00:16:46,000
 The good and evil that yang macho kurute idha.

230
00:16:46,000 --> 00:16:52,000
 The good and evil that we do here, that indeed is our own.

231
00:16:52,000 --> 00:16:56,000
 Tancha aadai yagacchiti, that goes with us.

232
00:16:56,000 --> 00:17:00,640
 That is what we take with us. That is what we carry with us

233
00:17:00,640 --> 00:17:01,000
.

234
00:17:01,000 --> 00:17:09,000
 Tancha sasa anugang hoti chayava anupai.

235
00:17:09,000 --> 00:17:16,600
 That follows after us, just like a shadow that never leaves

236
00:17:16,600 --> 00:17:17,000
.

237
00:17:17,000 --> 00:17:23,000
 Tasmakareya kalyanang nityayang samparaika.

238
00:17:23,000 --> 00:17:28,000
 Punyani paraloka smingpatitahuntypana.

239
00:17:28,000 --> 00:17:33,000
 This is a verse, that part is a verse that we chant,

240
00:17:33,000 --> 00:17:35,000
 actually.

241
00:17:35,000 --> 00:17:39,000
 That's part of a blessing, when we give blessings.

242
00:17:39,000 --> 00:17:46,000
 Tasmakareya kalyanang nityayang samparaika.

243
00:17:46,000 --> 00:17:49,000
 For that reason, one should do beautiful things.

244
00:17:49,000 --> 00:17:55,000
 Nityayang samparaika.

245
00:17:55,000 --> 00:18:03,000
 The accumulation, an accumulation for the next world.

246
00:18:03,000 --> 00:18:12,000
 Punyani paraloka smingpatitahuntypana.

247
00:18:12,000 --> 00:18:20,000
 Good deeds in the next world are what beings stand on,

248
00:18:20,000 --> 00:18:25,000
 are a foundation of beings.

249
00:18:25,000 --> 00:18:27,000
 As Bhikkhu Bodhi translates it,

250
00:18:27,000 --> 00:18:30,000
 "Marits are the support for living beings when they arise

251
00:18:30,000 --> 00:18:36,000
 in the other world."

252
00:18:36,000 --> 00:18:43,000
 And that's key for a comprehensive understanding,

253
00:18:43,000 --> 00:18:46,660
 because it is possible to do evil deeds and to be an evil

254
00:18:46,660 --> 00:18:47,000
 person

255
00:18:47,000 --> 00:18:50,000
 and still benefit in this world.

256
00:18:50,000 --> 00:18:54,640
 You're just adding to the burden that you carry around with

257
00:18:54,640 --> 00:18:55,000
 you,

258
00:18:55,000 --> 00:19:02,000
 and it follows you.

259
00:19:02,000 --> 00:19:06,000
 It's important to understand that death isn't an escape.

260
00:19:06,000 --> 00:19:10,000
 You don't get the carte blanche at death.

261
00:19:10,000 --> 00:19:14,000
 When you die, it's not a clean slate.

262
00:19:14,000 --> 00:19:18,000
 Suicide isn't the answer. Death isn't the answer.

263
00:19:18,000 --> 00:19:24,000
 Death isn't the end. It's the point.

264
00:19:24,000 --> 00:19:31,000
 It extends the potency of karma.

265
00:19:31,000 --> 00:19:33,950
 If karma is just in this life, there are other ways of

266
00:19:33,950 --> 00:19:35,000
 escaping it temporarily.

267
00:19:35,000 --> 00:19:39,000
 You can die without having to really pay back your karma.

268
00:19:39,000 --> 00:19:41,000
 That's, I think, true.

269
00:19:41,000 --> 00:19:44,000
 I mean, it's true, but I think that's obviously true.

270
00:19:44,000 --> 00:19:51,000
 The question is whether it's actually viable.

271
00:19:51,000 --> 00:19:56,600
 Of course, Buddhism says it's not, that death isn't the end

272
00:19:56,600 --> 00:19:57,000
.

273
00:19:57,000 --> 00:20:02,450
 Anyway, relating it to our meditation practice, because

274
00:20:02,450 --> 00:20:07,000
 that's what we always try to do.

275
00:20:07,000 --> 00:20:11,000
 There's an appreciation in Buddhism that some people don't.

276
00:20:11,000 --> 00:20:15,400
 Maybe appreciate that you are your own best friend and you

277
00:20:15,400 --> 00:20:17,000
 are your own worst enemy.

278
00:20:17,000 --> 00:20:21,910
 No one can hurt us the way we do ourselves, and no one can

279
00:20:21,910 --> 00:20:24,000
 help us the way we do ourselves.

280
00:20:24,000 --> 00:20:28,000
 There's two corallaries there.

281
00:20:28,000 --> 00:20:34,000
 One is that the importance of looking after yourself

282
00:20:34,000 --> 00:20:39,490
 and the supremacy of looking after yourself overlooking

283
00:20:39,490 --> 00:20:41,000
 after others.

284
00:20:41,000 --> 00:20:44,340
 The answer to the world's problems is not to help each

285
00:20:44,340 --> 00:20:45,000
 other.

286
00:20:45,000 --> 00:20:48,000
 It's just for everyone to help themselves.

287
00:20:48,000 --> 00:20:50,880
 If you're going to help others, it should be to help them

288
00:20:50,880 --> 00:20:52,000
 help themselves.

289
00:20:52,000 --> 00:20:54,600
 If you help others trying to fix their problems, anyone who

290
00:20:54,600 --> 00:20:59,500
 has ever tried to fix other people's problems knows that it

291
00:20:59,500 --> 00:21:03,000
's a wild goose chase.

292
00:21:03,000 --> 00:21:07,540
 It's a never-ending battle. You end up being their

293
00:21:07,540 --> 00:21:09,000
 dependency.

294
00:21:09,000 --> 00:21:12,000
 You never really fix anything.

295
00:21:12,000 --> 00:21:15,000
 Because you can't fix other people. We can fix ourselves.

296
00:21:15,000 --> 00:21:18,000
 We do much more for ourselves.

297
00:21:18,000 --> 00:21:21,560
 So this is where meditation fits in. Why meditation is so

298
00:21:21,560 --> 00:21:22,000
 important?

299
00:21:22,000 --> 00:21:26,000
 It's not betraying or abandoning others.

300
00:21:26,000 --> 00:21:31,000
 It's doing the right thing, and that everyone should do.

301
00:21:31,000 --> 00:21:35,590
 We should work to purify our body and our acts and our

302
00:21:35,590 --> 00:21:38,000
 speech and our thought.

303
00:21:38,000 --> 00:21:44,000
 It's the duty of all beings. We can't do that one thing.

304
00:21:44,000 --> 00:21:48,600
 We become our worst enemy. We can never truly say that we

305
00:21:48,600 --> 00:21:50,000
 care for ourselves.

306
00:21:50,000 --> 00:21:53,000
 And the world will never be at peace.

307
00:21:53,000 --> 00:21:55,430
 The world's not at peace because we don't do enough for

308
00:21:55,430 --> 00:21:56,000
 others.

309
00:21:56,000 --> 00:21:59,030
 The world's not at peace because no one does enough for

310
00:21:59,030 --> 00:22:00,000
 themselves.

311
00:22:00,000 --> 00:22:07,460
 Very few people do their duty to become stand-up human

312
00:22:07,460 --> 00:22:09,000
 beings.

313
00:22:09,000 --> 00:22:13,860
 And so we've got lots of problems inside. That's where the

314
00:22:13,860 --> 00:22:15,000
 real problems are.

315
00:22:15,000 --> 00:22:17,000
 That's one thing that religion understands.

316
00:22:17,000 --> 00:22:19,550
 You can criticize religion all you want, but one thing it

317
00:22:19,550 --> 00:22:23,000
 understands is that problems are inside our minds.

318
00:22:23,000 --> 00:22:25,240
 And it's in our minds that we have to fix things. It's not

319
00:22:25,240 --> 00:22:26,000
 in the world out there.

320
00:22:26,000 --> 00:22:30,000
 It's not in the physical. It's not even in the brain.

321
00:22:30,000 --> 00:22:33,270
 You can't fix things by medicating. You can't fix things by

322
00:22:33,270 --> 00:22:37,000
 social work or psychology or whatever.

323
00:22:37,000 --> 00:22:39,870
 You can't really fix them in your mind. The world, the

324
00:22:39,870 --> 00:22:42,000
 physical world is not the answer.

325
00:22:42,000 --> 00:22:49,590
 It doesn't work. And I think the evidence is there, whether

326
00:22:49,590 --> 00:22:53,000
 it's ever been collected in a modern scientific context.

327
00:22:53,000 --> 00:22:57,950
 But of course there have been some studies. They're not all

328
00:22:57,950 --> 00:22:59,000
 that impressive.

329
00:22:59,000 --> 00:23:04,180
 That's more a testament to the limitations of the modern

330
00:23:04,180 --> 00:23:09,000
 scientific method and the limitations of funding and so on.

331
00:23:09,000 --> 00:23:13,000
 But getting people to actually meditate in a study.

332
00:23:13,000 --> 00:23:21,290
 But the evidence is overwhelming that the benefits, say, of

333
00:23:21,290 --> 00:23:27,000
 any kind of physical solution to a simple mental solution,

334
00:23:27,000 --> 00:23:30,000
 meditation changes your life.

335
00:23:30,000 --> 00:23:32,300
 How many people have told me that their life has been

336
00:23:32,300 --> 00:23:34,000
 changed through the meditation?

337
00:23:34,000 --> 00:23:38,070
 People have never even met. And it's not because of me or

338
00:23:38,070 --> 00:23:39,000
 anything special.

339
00:23:39,000 --> 00:23:42,360
 I've done it. It's just the meditation practice to see

340
00:23:42,360 --> 00:23:46,000
 things clearly as they are, of reminding yourself,

341
00:23:46,000 --> 00:23:49,000
 "This is this. It is what it is. It's nothing else."

342
00:23:49,000 --> 00:23:56,300
 It's constantly working to better yourself, to do right by

343
00:23:56,300 --> 00:23:58,000
 yourself.

344
00:23:58,000 --> 00:24:06,000
 This is the most powerful force.

345
00:24:06,000 --> 00:24:15,510
 So that's the demo for tonight. And this is why we meditate

346
00:24:15,510 --> 00:24:21,000
, working on our one true task.

347
00:24:21,000 --> 00:24:27,740
 So, you're supposed to hang out. You should probably just

348
00:24:27,740 --> 00:24:32,000
 start taking text questions again.

349
00:24:32,000 --> 00:24:37,290
 No one's coming on the hangout. It's a bit much to ask

350
00:24:37,290 --> 00:24:41,430
 people to come on a live, on the air hangout and ask their

351
00:24:41,430 --> 00:24:43,000
 questions.

352
00:24:43,000 --> 00:24:47,920
 It's just that the questions were piling up and they were

353
00:24:47,920 --> 00:24:49,000
 backlogged and so on.

354
00:24:49,000 --> 00:24:52,960
 And a lot of them are questions that have already been

355
00:24:52,960 --> 00:24:54,000
 answered.

356
00:24:54,000 --> 00:24:59,730
 If anybody has any pressing questions and you don't want to

357
00:24:59,730 --> 00:25:03,290
 come on the hangout, you can text it there at meditation.s

358
00:25:03,290 --> 00:25:04,000
iri-mungala.org.

359
00:25:05,000 --> 00:25:33,000
 [Silence]

360
00:25:33,000 --> 00:25:48,000
 [Silence]

361
00:25:48,000 --> 00:25:57,460
 All right. Well, let there... Oh, here we are. Tom. Oh, hi,

362
00:25:57,460 --> 00:26:00,000
 Tom.

363
00:26:00,000 --> 00:26:04,490
 You have a question for me? Yes, I do. Okay. You were

364
00:26:04,490 --> 00:26:06,000
 talking about...

365
00:26:06,000 --> 00:26:10,030
 Wait just a second. I should make sure that this is working

366
00:26:10,030 --> 00:26:11,000
 properly.

367
00:26:11,000 --> 00:26:15,000
 Hmm. Yeah, okay. Go ahead.

368
00:26:15,000 --> 00:26:22,000
 Yes. Last night you were talking about the... the peace...

369
00:26:22,000 --> 00:26:24,740
 well, I'll call it the peace movement. I don't remember the

370
00:26:24,740 --> 00:26:26,000
 exact name.

371
00:26:26,000 --> 00:26:34,130
 And I'm an American. I became a young adult when the

372
00:26:34,130 --> 00:26:40,000
 Vietnam War was just really ramping up.

373
00:26:40,000 --> 00:26:49,110
 I've had a lot of difficulty dealing with the actions of

374
00:26:49,110 --> 00:26:54,000
 our government over the decades.

375
00:26:54,000 --> 00:26:57,670
 And, you know, to anybody who's paying attention, it's

376
00:26:57,670 --> 00:27:01,000
 obviously getting worse and worse, not better.

377
00:27:01,000 --> 00:27:07,210
 So I have a lot of personal difficulty dealing with the

378
00:27:07,210 --> 00:27:12,000
 activities that are going on in the world.

379
00:27:12,000 --> 00:27:17,350
 Mainly our involvement in military actions around the globe

380
00:27:17,350 --> 00:27:21,000
 and what the repercussions have been.

381
00:27:21,000 --> 00:27:28,920
 And this has a great deal of... it has a major impact on me

382
00:27:28,920 --> 00:27:31,000
 personally.

383
00:27:31,000 --> 00:27:37,000
 So I just wanted your perspective on that, please.

384
00:27:37,000 --> 00:27:40,640
 Well, recalling what I said last night about... you're

385
00:27:40,640 --> 00:27:45,000
 talking about how the trouble maybe I was having,

386
00:27:45,000 --> 00:27:48,300
 the conflict or the way they looked at peace as opposed to

387
00:27:48,300 --> 00:27:51,380
 how I looked at peace, that kind of thing. Is that what I

388
00:27:51,380 --> 00:27:52,000
 was referring to?

389
00:27:52,000 --> 00:28:00,620
 Well, just that you mentioned, I guess you're active in a

390
00:28:00,620 --> 00:28:11,000
 group at the university there that's trying to bring about

391
00:28:11,000 --> 00:28:14,000
 a more peaceful world or whatever.

392
00:28:14,000 --> 00:28:21,710
 And to me, it's not really that in particular. It's just to

393
00:28:21,710 --> 00:28:24,000
 me right now.

394
00:28:24,000 --> 00:28:30,830
 I don't want to say it seems hopeless, but we seem to be on

395
00:28:30,830 --> 00:28:39,000
 a trajectory that is getting us, the US,

396
00:28:39,000 --> 00:28:44,000
 and the world into a deeper and deeper state of conflict.

397
00:28:44,000 --> 00:28:49,530
 And this has been something that I've looked at since I was

398
00:28:49,530 --> 00:28:51,000
 a young man.

399
00:28:51,000 --> 00:29:02,470
 And I just feel like it's completely out of hand and I feel

400
00:29:02,470 --> 00:29:11,520
 that things could easily get... I mean, it could get very

401
00:29:11,520 --> 00:29:12,000
 bad.

402
00:29:12,000 --> 00:29:18,800
 Everybody knows what the capacity is of humans to make war

403
00:29:18,800 --> 00:29:21,000
 on one another.

404
00:29:21,000 --> 00:29:28,790
 So anyway, my point is very specifically because it's, I'll

405
00:29:28,790 --> 00:29:32,000
 call it an obsession of mine.

406
00:29:32,000 --> 00:29:37,390
 How do I deal with that? Or how does anybody deal with that

407
00:29:37,390 --> 00:29:41,750
 when you live in a time and a place where you can easily

408
00:29:41,750 --> 00:29:48,340
 become overwhelmed with the activities of our political

409
00:29:48,340 --> 00:29:50,000
 leaders and so on?

410
00:29:50,000 --> 00:30:01,000
 Michael, you don't have to stay here.

411
00:30:01,000 --> 00:30:06,190
 Well, there's lots of sort of important points to keep in

412
00:30:06,190 --> 00:30:07,000
 mind.

413
00:30:07,000 --> 00:30:10,000
 The first is that the human race is doomed.

414
00:30:10,000 --> 00:30:15,890
 I don't think there's anyone who doubts the fact that

415
00:30:15,890 --> 00:30:21,950
 eventually it's going to be wiped out in a blaze of fire or

416
00:30:21,950 --> 00:30:23,000
 whatnot.

417
00:30:23,000 --> 00:30:28,210
 Or even just more generally, everything changes. There's no

418
00:30:28,210 --> 00:30:36,000
 solution besides release from samsara, which is individual.

419
00:30:36,000 --> 00:30:40,340
 And the second thing is the... So that's about the futility

420
00:30:40,340 --> 00:30:46,190
 of... The ultimate futility of making that a goal, making

421
00:30:46,190 --> 00:30:52,000
 the peace on earth or so on a goal.

422
00:30:52,000 --> 00:30:58,200
 But the other part is the actual... Again, it comes down to

423
00:30:58,200 --> 00:31:02,700
 what is leading to your goals and what is actually

424
00:31:02,700 --> 00:31:05,000
 counterproductive.

425
00:31:05,000 --> 00:31:11,000
 So obsession and frustration and depression and despair.

426
00:31:11,000 --> 00:31:19,350
 Bernie Sanders said something. He said, "Don't go into that

427
00:31:19,350 --> 00:31:22,000
 pit of despair."

428
00:31:22,000 --> 00:31:26,300
 He's been around for a long time and on the right side of

429
00:31:26,300 --> 00:31:31,150
 things for a long time and met with interesting people. He

430
00:31:31,150 --> 00:31:34,000
's really got an interesting perspective.

431
00:31:34,000 --> 00:31:39,470
 It's a lot like Pasenadi, I think, one of these guys who,

432
00:31:39,470 --> 00:31:46,100
 well, we wouldn't say he's pure by Buddhist standards, but

433
00:31:46,100 --> 00:31:53,000
 he's a really good sort of politician with a wise mind.

434
00:31:53,000 --> 00:31:56,000
 I mean, it seems anyway.

435
00:31:56,000 --> 00:31:59,970
 So, yeah, negative emotions are not only futile in the

436
00:31:59,970 --> 00:32:04,210
 sense like, why are you worrying about something that's in

437
00:32:04,210 --> 00:32:09,060
 the end never going to be a solution, but also actually

438
00:32:09,060 --> 00:32:11,000
 detrimental.

439
00:32:11,000 --> 00:32:16,920
 And so it's important on the one hand to think of... Always

440
00:32:16,920 --> 00:32:22,000
 think in terms of your spirituality.

441
00:32:22,000 --> 00:32:27,310
 But on the other hand, to be aware of how your civic duty

442
00:32:27,310 --> 00:32:32,600
 and good deeds towards your fellow human beings are

443
00:32:32,600 --> 00:32:36,300
 actually supportive of spirituality and so therefore are

444
00:32:36,300 --> 00:32:37,000
 important.

445
00:32:37,000 --> 00:32:42,110
 But you've got to understand how do you affect real change

446
00:32:42,110 --> 00:32:47,080
 and benefit, beneficence in the world. It's not by

447
00:32:47,080 --> 00:32:52,890
 obsession or frustration or depression or getting angry

448
00:32:52,890 --> 00:32:56,000
 about things isn't useful.

449
00:32:56,000 --> 00:33:02,000
 So there's some sort of perspective, I think.

450
00:33:02,000 --> 00:33:06,450
 Obviously, meditation is important, staying mindful. But

451
00:33:06,450 --> 00:33:12,000
 another important point is, which I kind of said already,

452
00:33:12,000 --> 00:33:17,350
 is I think there is room for civic duty and an importance

453
00:33:17,350 --> 00:33:21,000
 for lay people to undertake their civic duty.

454
00:33:21,000 --> 00:33:26,130
 I kind of made a remark that may have sounded like I was

455
00:33:26,130 --> 00:33:32,180
 condescending or disparaging of worldly peace work, but I'm

456
00:33:32,180 --> 00:33:33,000
 not.

457
00:33:33,000 --> 00:33:35,980
 I really think it's awesome that people are working in a

458
00:33:35,980 --> 00:33:38,000
 worldly way to bring about peace.

459
00:33:38,000 --> 00:33:42,620
 Of course, I would argue that it's inferior to meditation,

460
00:33:42,620 --> 00:33:46,930
 but you could also say it's an important part of spiritual

461
00:33:46,930 --> 00:33:48,000
 practice.

462
00:33:48,000 --> 00:33:52,260
 But it's not for monks, is really the point. As a monk, I

463
00:33:52,260 --> 00:33:58,140
 have some problems getting too involved with worldly peace

464
00:33:58,140 --> 00:34:00,000
 or world peace.

465
00:34:00,000 --> 00:34:03,920
 I have limits personally that you shouldn't take as a model

466
00:34:03,920 --> 00:34:07,000
 unless you're looking to become monastic.

467
00:34:07,000 --> 00:34:13,260
 Because I think non-monastics have certain civic duties

468
00:34:13,260 --> 00:34:20,600
 that you could argue they should take seriously. Does that

469
00:34:20,600 --> 00:34:22,000
 help?

470
00:34:22,000 --> 00:34:28,310
 I'd like to say yes. I contemplate all of these things. I

471
00:34:28,310 --> 00:34:35,000
 just really shut it out.

472
00:34:35,000 --> 00:34:42,710
 But since you brought up, because I look at peace activity

473
00:34:42,710 --> 00:34:48,000
 at a university is best to be expected.

474
00:34:48,000 --> 00:34:51,790
 I was both in the service and at a university in the late

475
00:34:51,790 --> 00:34:54,000
 60s and early 70s.

476
00:34:54,000 --> 00:35:01,430
 And look where it got us. But thank you. I appreciate your

477
00:35:01,430 --> 00:35:07,000
 thoughts on that. I'll let you go.

478
00:35:07,000 --> 00:35:13,000
 Nice to talk to you. I guess that's all for tonight then.

479
00:35:13,000 --> 00:35:15,000
 See you all next time.

480
00:35:16,000 --> 00:35:18,000
 Thank you.

481
00:35:18,000 --> 00:35:20,000
 Thank you.

482
00:35:20,000 --> 00:35:22,000
 Thank you.

483
00:35:22,000 --> 00:35:24,000
 Thank you.

484
00:35:25,000 --> 00:35:27,000
 Thank you.

485
00:35:28,000 --> 00:35:30,000
 Thank you.

486
00:35:32,000 --> 00:35:34,000
 Thank you.

