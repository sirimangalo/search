1
00:00:00,000 --> 00:00:16,180
 [

2
00:00:16,180 --> 00:00:18,200
]

3
00:00:18,200 --> 00:00:28,200
 Good evening everyone.

4
00:00:29,900 --> 00:00:31,400
 I know,

5
00:00:31,400 --> 00:00:33,400
 actually a little early,

6
00:00:33,400 --> 00:00:37,400
 I had to stop my meditation to come online.

7
00:00:37,400 --> 00:00:42,400
 No matter.

8
00:00:42,400 --> 00:00:58,600
 [

9
00:00:58,600 --> 00:01:12,600
 Good evening.

10
00:01:12,600 --> 00:01:19,600
 So tonight's quote is

11
00:01:19,600 --> 00:01:23,600
 again from the Angutt Purani Kaya.

12
00:01:23,600 --> 00:01:28,600
 Angutt Purani Kaya is a good source of

13
00:01:28,600 --> 00:01:33,600
 dogma or doctrine.

14
00:01:33,600 --> 00:01:39,600
 It's the book that you go to if you want to get

15
00:01:39,600 --> 00:01:43,600
 doctrine on a topic.

16
00:01:43,600 --> 00:01:51,600
 Good for teachers to refer to when they're giving talks.

17
00:01:52,600 --> 00:01:56,600
 Because the Angutt Purani Kaya is mainly lists.

18
00:01:56,600 --> 00:02:00,600
 So you've got separated into books.

19
00:02:00,600 --> 00:02:05,600
 And this is the...

20
00:02:05,600 --> 00:02:14,600
 Why is this the book of 11s? I don't know.

21
00:02:14,600 --> 00:02:19,600
 For some reason I thought it would be the book of 6s.

22
00:02:19,600 --> 00:02:23,600
 Let's see,

23
00:02:23,600 --> 00:02:27,600
 a big commodity often has some good insight on the why,

24
00:02:27,600 --> 00:02:29,600
 how they do the numbering.

25
00:02:29,600 --> 00:02:47,600
 [

26
00:02:47,600 --> 00:03:01,600
 Pause ]

27
00:03:01,600 --> 00:03:03,600
 Oh, I see.

28
00:03:03,600 --> 00:03:08,600
 This sutta is actually a part of a bigger set of sutta's.

29
00:03:08,600 --> 00:03:12,010
 So let's not worry about the numbering because it seems a

30
00:03:12,010 --> 00:03:14,600
 little bit complicated.

31
00:03:16,600 --> 00:03:20,600
 So he starts by mentioning five.

32
00:03:20,600 --> 00:03:26,600
 This is actually to Mahanama.

33
00:03:26,600 --> 00:03:30,600
 So Mahanama comes to see the Buddha and

34
00:03:41,600 --> 00:03:44,600
 the Buddha was going to set out wandering.

35
00:03:44,600 --> 00:03:48,600
 Mahanama was the brother of Anuruddha.

36
00:03:48,600 --> 00:03:51,600
 If you remember the story of Anuruddha.

37
00:03:51,600 --> 00:03:59,600
 Anuruddha, because when the Buddha went back to his home,

38
00:03:59,600 --> 00:04:04,600
 many of the young men and even young women

39
00:04:04,600 --> 00:04:08,600
 went forth and became monks under the Buddha.

40
00:04:10,600 --> 00:04:13,600
 And so from every family it seemed that they were going.

41
00:04:13,600 --> 00:04:15,600
 And some people complained and said,

42
00:04:15,600 --> 00:04:18,600
 "Oh, this ascetic Gautama is,

43
00:04:18,600 --> 00:04:21,600
 Siddhartha, whatever they called them,

44
00:04:21,600 --> 00:04:25,600
 is taking away our sons, stealing away our sons."

45
00:04:25,600 --> 00:04:28,600
 Mostly it was the men in the beginning.

46
00:04:28,600 --> 00:04:32,600
 And they told the Buddhists and the Buddhists said,

47
00:04:32,600 --> 00:04:37,370
 "Oh, tell them that they're ordaining according to the Dham

48
00:04:37,370 --> 00:04:37,600
ma,

49
00:04:37,600 --> 00:04:40,600
 according to the Dhamma it would have been."

50
00:04:40,600 --> 00:04:43,600
 And when the monks told the people,

51
00:04:43,600 --> 00:04:46,600
 "Oh, they're ordaining according to the Dhamma,"

52
00:04:46,600 --> 00:04:50,600
 then people were, "Oh," because they respected the Dhamma,

53
00:04:50,600 --> 00:04:51,600
 the way.

54
00:04:51,600 --> 00:04:54,600
 Dhamma is, it took on a lot of meaning.

55
00:04:54,600 --> 00:04:57,600
 This word has, is laden with much meaning.

56
00:04:57,600 --> 00:05:00,600
 Originally it just meant what people held.

57
00:05:00,600 --> 00:05:05,600
 It became that which is held to be true, kind of thing.

58
00:05:06,600 --> 00:05:09,600
 So it came to be the right.

59
00:05:09,600 --> 00:05:14,600
 Anyway, from Mahanama and Anuruddha,

60
00:05:14,600 --> 00:05:16,600
 no one from their family became monks.

61
00:05:16,600 --> 00:05:18,600
 And so they were kind of embarrassed.

62
00:05:18,600 --> 00:05:21,600
 Mahanama said, "One of us has to become a monk."

63
00:05:21,600 --> 00:05:25,190
 And Anuruddha said, "Oh, well, becoming a monk sounds

64
00:05:25,190 --> 00:05:25,600
 difficult.

65
00:05:25,600 --> 00:05:28,600
 I'll stay as a lay person. You go become a monk."

66
00:05:28,600 --> 00:05:34,600
 And so Mahanama had to explain to Anuruddha

67
00:05:34,600 --> 00:05:36,600
 how to be a lay person.

68
00:05:36,600 --> 00:05:39,600
 We have estates that you have to care for.

69
00:05:39,600 --> 00:05:44,600
 So you have to go and oversee the planting of the grain,

70
00:05:44,600 --> 00:05:47,600
 the tilling of the fields, the planting of the grain,

71
00:05:47,600 --> 00:05:51,880
 the carrying of the fields, the guarding of the grain from

72
00:05:51,880 --> 00:05:52,600
 the animals.

73
00:05:52,600 --> 00:05:56,270
 You have to oversee taking it to the mill, grinding the

74
00:05:56,270 --> 00:05:57,600
 grain, and so on.

75
00:05:57,600 --> 00:06:04,600
 And all the affairs having to do with the farm.

76
00:06:04,600 --> 00:06:07,600
 And then you have to give wages to all the workers,

77
00:06:07,600 --> 00:06:11,950
 and you have to oversee and make sure that they're doing

78
00:06:11,950 --> 00:06:12,600
 their jobs.

79
00:06:12,600 --> 00:06:15,600
 And then you have to look after the money.

80
00:06:15,600 --> 00:06:17,600
 And he went on and on and on.

81
00:06:17,600 --> 00:06:21,600
 And Anuruddha's eyes got wider and wider until finally he

82
00:06:21,600 --> 00:06:21,600
 said,

83
00:06:21,600 --> 00:06:25,600
 "Stop, stop. I'll become a monk. You stay as a lay person."

84
00:06:27,600 --> 00:06:31,210
 And so Anuruddha became a monk. Mahanama stayed as a layman

85
00:06:31,210 --> 00:06:31,600
.

86
00:06:31,600 --> 00:06:35,490
 And he's a subject, he's actually one of the great lay

87
00:06:35,490 --> 00:06:38,600
 people in the Buddha's dispensation.

88
00:06:38,600 --> 00:06:41,600
 He often came to the Buddha and asked him questions.

89
00:06:41,600 --> 00:06:44,600
 So he found out that the Buddha was going away.

90
00:06:44,600 --> 00:06:48,600
 The monks were making a robe for him after the rains

91
00:06:51,600 --> 00:06:57,600
 because he was going to go wandering to teach.

92
00:06:57,600 --> 00:07:02,920
 And so Mahanama heard this and he wanted to get some

93
00:07:02,920 --> 00:07:05,600
 instruction before the Buddha left.

94
00:07:05,600 --> 00:07:07,600
 So he went to the Buddha and he said, "Is this true?

95
00:07:07,600 --> 00:07:10,600
 So I've heard that you're leaving.

96
00:07:10,600 --> 00:07:15,600
 With all our various engagements, how should we dwell?"

97
00:07:15,600 --> 00:07:19,600
 Oh, this is Bhikkhu Bodhi's transiting.

98
00:07:19,600 --> 00:07:22,890
 "Among the various ways in which we dwell, how should we

99
00:07:22,890 --> 00:07:23,600
 dwell?"

100
00:07:23,600 --> 00:07:35,600
 "Kenasa viharina viharatbang"

101
00:07:35,600 --> 00:07:39,600
 By which means should?

102
00:07:39,600 --> 00:07:45,600
 Oh, of the many ways we can dwell, this word vihara.

103
00:07:45,600 --> 00:07:47,600
 This is a good quote.

104
00:07:47,600 --> 00:07:50,600
 "Tesang no bande na na viharihi"

105
00:07:50,600 --> 00:07:55,600
 "Viharatang kenasa viharina viharatbang"

106
00:07:55,600 --> 00:07:57,600
 This is the word vihar.

107
00:07:57,600 --> 00:08:01,600
 Har. Har means to carry.

108
00:08:01,600 --> 00:08:05,600
 Vihar. Har means to take, I think.

109
00:08:05,600 --> 00:08:10,600
 To take. Vihar. Vihar means to dwell.

110
00:08:10,600 --> 00:08:16,600
 Of all the many ways we can dwell, how should we dwell?

111
00:08:18,600 --> 00:08:22,600
 And the Buddha says, "Sadhu sadhu mahana"

112
00:08:22,600 --> 00:08:28,990
 "It is proper that you should ask the Buddha this question

113
00:08:28,990 --> 00:08:29,600
."

114
00:08:29,600 --> 00:08:33,600
 And so first, before he tells him how you should dwell,

115
00:08:33,600 --> 00:08:38,770
 he offers these five qualities that he's going to require

116
00:08:38,770 --> 00:08:41,600
 in order to dwell properly.

117
00:08:41,600 --> 00:08:45,230
 In order to accomplish the dwelling that the Buddha is

118
00:08:45,230 --> 00:08:46,600
 going to give to him,

119
00:08:46,600 --> 00:08:49,600
 he needs five qualities first.

120
00:08:49,600 --> 00:08:51,600
 Maybe this is where the eleven comes from.

121
00:08:51,600 --> 00:08:54,600
 There's the five qualities and the six dwellings.

122
00:08:54,600 --> 00:08:56,600
 Altogether it makes eleven.

123
00:08:56,600 --> 00:09:00,960
 It's not how bhikkhu bodhi numbers it, but we have the five

124
00:09:00,960 --> 00:09:03,600
 things you should make.

125
00:09:03,600 --> 00:09:04,600
 Right? Okay.

126
00:09:04,600 --> 00:09:07,600
 When you have established these five things in yourself,

127
00:09:07,600 --> 00:09:11,600
 you should also make six other things grow within you.

128
00:09:11,600 --> 00:09:17,600
 So, one who is serious...

129
00:09:17,600 --> 00:09:22,600
 Ah, that's not how bhikkhu bodhi numbers it.

130
00:09:22,600 --> 00:09:32,600
 "Sadhu koh mahana ma aradha kohoti na asadhoh."

131
00:09:32,600 --> 00:09:35,600
 So someone who is energetic.

132
00:09:35,600 --> 00:09:38,600
 Someone who is...

133
00:09:38,600 --> 00:09:43,600
 Someone who is successful.

134
00:09:43,600 --> 00:09:45,600
 Eager.

135
00:09:45,600 --> 00:09:48,600
 One is successful. How is one successful?

136
00:09:48,600 --> 00:09:51,600
 First, "sadhoh."

137
00:09:51,600 --> 00:09:56,600
 No "asadhoh." One needs to have confidence.

138
00:09:56,600 --> 00:09:59,600
 They can't be faithless.

139
00:10:03,600 --> 00:10:07,600
 So the first thing you need is you have to have confidence.

140
00:10:07,600 --> 00:10:10,800
 You should have confidence in the Buddha, the Dhamma, the

141
00:10:10,800 --> 00:10:11,600
 Sangha.

142
00:10:11,600 --> 00:10:13,600
 You should have confidence in the practice,

143
00:10:13,600 --> 00:10:18,600
 confidence in your teacher, confidence in yourself.

144
00:10:18,600 --> 00:10:21,410
 If you don't have confidence in all these things, you have

145
00:10:21,410 --> 00:10:22,600
 to remedy it.

146
00:10:22,600 --> 00:10:25,600
 Maybe you need a new teacher.

147
00:10:25,600 --> 00:10:28,590
 Maybe you need to study about the Buddha, study the Dhamma,

148
00:10:28,590 --> 00:10:29,600
 study...

149
00:10:29,600 --> 00:10:34,710
 You have to get to know the Sangha so you have faith in

150
00:10:34,710 --> 00:10:35,600
 them.

151
00:10:35,600 --> 00:10:40,600
 You have to study the practice and find the right practice.

152
00:10:40,600 --> 00:10:44,030
 You have to look at yourself and you have to see the good

153
00:10:44,030 --> 00:10:44,600
 qualities in yourself

154
00:10:44,600 --> 00:10:48,600
 and work on the good qualities and focus on good qualities

155
00:10:48,600 --> 00:10:50,600
 so that you have confidence.

156
00:10:50,600 --> 00:10:57,600
 "Aradha viryo no kusito."

157
00:10:58,600 --> 00:11:01,600
 They are accomplished in effort.

158
00:11:01,600 --> 00:11:05,600
 "Aradha" means firm effort.

159
00:11:05,600 --> 00:11:08,600
 "No kusito," not lazy.

160
00:11:08,600 --> 00:11:13,600
 You have to work. You have to commit yourself.

161
00:11:13,600 --> 00:11:17,040
 It doesn't mean you have to run around or you have to push

162
00:11:17,040 --> 00:11:18,600
 yourself very hard.

163
00:11:18,600 --> 00:11:24,600
 It just means you have to have firm effort, strong effort,

164
00:11:24,600 --> 00:11:29,600
 consistent and sustained effort.

165
00:11:29,600 --> 00:11:32,600
 Because you can have effort at any given moment,

166
00:11:32,600 --> 00:11:40,090
 but it's easy to fall into laziness out of greed or out of

167
00:11:40,090 --> 00:11:43,600
 anger, out of delusion.

168
00:11:43,600 --> 00:11:46,230
 But as soon as you're mindful, then you can have effort

169
00:11:46,230 --> 00:11:46,600
 again.

170
00:11:46,600 --> 00:11:50,090
 So you can rebuild effort every moment. You can cultivate

171
00:11:50,090 --> 00:11:50,600
 it.

172
00:11:50,600 --> 00:11:54,600
 This is how people are able to practice day and night.

173
00:11:54,600 --> 00:11:59,290
 They're able to find effort in the present moment again and

174
00:11:59,290 --> 00:12:00,600
 again and again.

175
00:12:00,600 --> 00:12:04,600
 And just continuously stay with that.

176
00:12:04,600 --> 00:12:10,600
 "No patita sati, no muta sati."

177
00:12:10,600 --> 00:12:15,600
 They have established mindfulness or remembrance.

178
00:12:15,600 --> 00:12:18,600
 They're not forgetful.

179
00:12:18,600 --> 00:12:25,600
 "No muta sati." They're doing a muddled sati.

180
00:12:25,600 --> 00:12:29,600
 They're able to see things arising and ceasing.

181
00:12:29,600 --> 00:12:38,600
 And they're able to recognize this, this, this, this, this.

182
00:12:38,600 --> 00:12:43,100
 They're able to practice body, sky, a vedana, jitta and d

183
00:12:43,100 --> 00:12:43,600
hamma.

184
00:12:43,600 --> 00:12:47,120
 They're able to see what is kaya, what is vedana, what is j

185
00:12:47,120 --> 00:12:48,600
itta, what is dhamma.

186
00:12:48,600 --> 00:12:52,600
 Before satipatana.

187
00:12:52,600 --> 00:12:57,600
 "Samahito no asamahito."

188
00:12:57,600 --> 00:13:02,600
 They are concentrated or focused, not unfocused.

189
00:13:02,600 --> 00:13:06,250
 So again, this is not quite concentrated. It's more focused

190
00:13:06,250 --> 00:13:07,600
 like a lens.

191
00:13:07,600 --> 00:13:12,600
 Seeing things, sharpening your vision.

192
00:13:12,600 --> 00:13:16,600
 Seeing things as they are.

193
00:13:16,600 --> 00:13:21,600
 And number five, "panyoa no du panyo."

194
00:13:21,600 --> 00:13:31,600
 They are possessing of wisdom, not of low or base wisdom,

195
00:13:31,600 --> 00:13:35,600
 base understanding.

196
00:13:35,600 --> 00:13:45,600
 You have to have wisdom.

197
00:13:45,600 --> 00:13:51,800
 Wisdom about right view, wisdom about body and mind, three

198
00:13:51,800 --> 00:13:52,600
 characteristics.

199
00:13:52,600 --> 00:13:55,600
 Wisdom about karma.

200
00:13:55,600 --> 00:13:59,600
 You have to have wisdom about right and wrong.

201
00:13:59,600 --> 00:14:02,600
 You have to have wisdom about cause and effect.

202
00:14:02,600 --> 00:14:07,060
 And to know these things intellectually first, but then you

203
00:14:07,060 --> 00:14:11,600
 have to see them through the practice.

204
00:14:11,600 --> 00:14:16,050
 So true panyoa, true wisdom is first you hear, then you

205
00:14:16,050 --> 00:14:21,600
 think, and then you study, and you experience.

206
00:14:21,600 --> 00:14:25,150
 Real wisdom is you see nama-rupa, you see the physical and

207
00:14:25,150 --> 00:14:26,600
 mental things arising.

208
00:14:26,600 --> 00:14:29,340
 You see, it's quite obvious actually, but you start to

209
00:14:29,340 --> 00:14:31,600
 realize that that's the nature of reality.

210
00:14:31,600 --> 00:14:35,210
 Not this room that we're in or this world that we live in,

211
00:14:35,210 --> 00:14:36,600
 that's not real.

212
00:14:36,600 --> 00:14:40,450
 What's real is experience is a physical and mental

213
00:14:40,450 --> 00:14:41,600
 phenomena.

214
00:14:41,600 --> 00:14:47,070
 You see like that, and then you see cause and effect, this

215
00:14:47,070 --> 00:14:50,600
 causes this, that causes that.

216
00:14:50,600 --> 00:14:55,600
 See, when you don't give rise to this, that doesn't arise.

217
00:14:55,600 --> 00:15:00,600
 When you give rise to this, that arises.

218
00:15:00,600 --> 00:15:04,350
 You see the three characteristics that everything you think

219
00:15:04,350 --> 00:15:09,600
 was stable, satisfying, controllable, it's not actually.

220
00:15:09,600 --> 00:15:14,600
 You start to lose your passion, your desire.

221
00:15:14,600 --> 00:15:18,080
 And then you see the path, you start to see how you can

222
00:15:18,080 --> 00:15:22,600
 live, how you can exist without falling into suffering,

223
00:15:22,600 --> 00:15:28,130
 without cultivating unwholesomeness and falling into

224
00:15:28,130 --> 00:15:29,600
 suffering.

225
00:15:29,600 --> 00:15:31,600
 And then you attain the fruit.

226
00:15:31,600 --> 00:15:35,630
 Finally you pick the fruit and you taste the fruit of

227
00:15:35,630 --> 00:15:36,600
 freedom.

228
00:15:36,600 --> 00:15:38,600
 That's wisdom.

229
00:15:38,600 --> 00:15:44,990
 So those five, and then he talks about the six recol

230
00:15:44,990 --> 00:15:48,600
lections that you should keep in mind.

231
00:15:48,600 --> 00:15:50,600
 And these are the six anusati.

232
00:15:50,600 --> 00:15:54,360
 Remember, if those of you who helped or studied the Visudd

233
00:15:54,360 --> 00:15:56,600
hi manga with us, we went through these.

234
00:15:56,600 --> 00:16:00,600
 There's ten recollections. The first six are chhanusati,

235
00:16:00,600 --> 00:16:05,600
 they're called the six recollections.

236
00:16:05,600 --> 00:16:09,600
 So we have the Buddha, the Dhamma, the Sangha.

237
00:16:09,600 --> 00:16:14,600
 We have Sila, Dhanma, the Jhaga, right?

238
00:16:14,600 --> 00:16:19,600
 Sila, Jhaga, and Dehva, Dehvada.

239
00:16:19,600 --> 00:16:23,620
 So the Buddha, the Dhamma, and the Sangha, we recollect on

240
00:16:23,620 --> 00:16:24,600
 the Buddha.

241
00:16:24,600 --> 00:16:27,600
 We think about the Buddha, Iti Piso Bhagavao.

242
00:16:27,600 --> 00:16:29,600
 Indeed he is the Blessed One.

243
00:16:29,600 --> 00:16:33,550
 Subhati, Suha Khatto, we think of the Dhamma, well taught

244
00:16:33,550 --> 00:16:34,600
 is the Dhamma.

245
00:16:34,600 --> 00:16:37,600
 There's actually a mantra for that.

246
00:16:37,600 --> 00:16:41,690
 Subhati Paano, we think about the Sangha being well

247
00:16:41,690 --> 00:16:42,600
 practiced.

248
00:16:42,600 --> 00:16:45,600
 And there's a mantra for all three of those.

249
00:16:45,600 --> 00:16:49,600
 The other three are reflecting on our Sila, our morality.

250
00:16:49,600 --> 00:16:51,600
 We think of how pure our morality is.

251
00:16:51,600 --> 00:16:55,080
 How we're not killing, we're not stealing, we're not

252
00:16:55,080 --> 00:16:56,600
 cheating, we're not lying,

253
00:16:56,600 --> 00:16:59,600
 we're not taking drugs or alcohol.

254
00:16:59,600 --> 00:17:03,150
 Even when we're practicing meditation, our morality is

255
00:17:03,150 --> 00:17:04,600
 quite pure.

256
00:17:04,600 --> 00:17:06,600
 Our behavior is quite pure.

257
00:17:06,600 --> 00:17:08,600
 That's why meditation is so wonderful.

258
00:17:08,600 --> 00:17:11,100
 That's why sitting on a mat and actually doing formal med

259
00:17:11,100 --> 00:17:13,600
itations is very important.

260
00:17:13,600 --> 00:17:18,600
 Because it's that moment or that period of purity

261
00:17:18,600 --> 00:17:24,600
 where you're not engaged in any impure activity.

262
00:17:24,600 --> 00:17:29,300
 And then jaga, so you reflect on that and you feel happy

263
00:17:29,300 --> 00:17:30,600
 about that.

264
00:17:30,600 --> 00:17:34,600
 Jaga means reflecting on your generosity.

265
00:17:34,600 --> 00:17:39,780
 So those of us who, those of you who have joined with us to

266
00:17:39,780 --> 00:17:41,600
 give robes to Ajahn Tong,

267
00:17:41,600 --> 00:17:43,600
 it's an awesome thing.

268
00:17:43,600 --> 00:17:44,600
 That's great.

269
00:17:44,600 --> 00:17:47,600
 That kind of thing, it's just as an example.

270
00:17:47,600 --> 00:17:50,730
 The good deeds that you do, any gift that you've given, any

271
00:17:50,730 --> 00:17:52,600
 help that you've given to someone,

272
00:17:52,600 --> 00:17:55,290
 any, if you've given the Dhamma, maybe you've taught

273
00:17:55,290 --> 00:17:56,600
 someone how to meditate,

274
00:17:56,600 --> 00:18:08,600
 all these things, reflecting on those, remembering them.

275
00:18:08,600 --> 00:18:13,030
 And then finally, Devata, you recollect on the angels, on

276
00:18:13,030 --> 00:18:13,600
 heaven.

277
00:18:13,600 --> 00:18:16,800
 Again, this is advice to a layman, so you might wonder, why

278
00:18:16,800 --> 00:18:18,600
 are we thinking about angels?

279
00:18:18,600 --> 00:18:22,600
 It's actually thinking about the qualities that exist in us

280
00:18:22,600 --> 00:18:26,190
 and rejoicing about our good qualities that will lead us to

281
00:18:26,190 --> 00:18:28,600
 good things in the future.

282
00:18:28,600 --> 00:18:32,600
 Now, obviously heaven isn't where we're aiming for,

283
00:18:32,600 --> 00:18:37,600
 but for a layman, for someone who is sort of, again,

284
00:18:37,600 --> 00:18:39,600
 practicing,

285
00:18:39,600 --> 00:18:44,600
 not necessarily to attain Vairinibhana in this life,

286
00:18:44,600 --> 00:18:50,600
 but to purify their minds and to see the truth.

287
00:18:50,600 --> 00:18:58,960
 There was this person who came to visit a couple of days

288
00:18:58,960 --> 00:18:59,600
 ago,

289
00:18:59,600 --> 00:19:06,600
 said to me, she's married, she has a husband,

290
00:19:06,600 --> 00:19:11,770
 she said, "Oh, I saw your video, I watched your video on

291
00:19:11,770 --> 00:19:13,600
 how you became a monk."

292
00:19:13,600 --> 00:19:16,600
 And I said, "Oh yeah, are you interested?"

293
00:19:16,600 --> 00:19:19,600
 And she said, "No."

294
00:19:19,600 --> 00:19:25,600
 Which is so good, you know, honest of her.

295
00:19:25,600 --> 00:19:29,340
 But that kind of thing, you know, not everyone is going to

296
00:19:29,340 --> 00:19:30,600
 go all the way.

297
00:19:30,600 --> 00:19:34,110
 I've gotten that answer before when I asked people if they

298
00:19:34,110 --> 00:19:35,600
 want to become a monk.

299
00:19:35,600 --> 00:19:37,600
 No, absolutely not.

300
00:19:37,600 --> 00:19:41,600
 Someone even said, "Oh, I hope not."

301
00:19:41,600 --> 00:19:44,980
 So maybe one day you'll become a monk, I said, "Oh, I hope

302
00:19:44,980 --> 00:19:45,600
 not."

303
00:19:45,600 --> 00:19:49,600
 So not everyone's headed in that direction.

304
00:19:49,600 --> 00:19:52,640
 But you know, going to heaven is a great thing because

305
00:19:52,640 --> 00:19:55,600
 there's lots of Buddhists up in heaven.

306
00:19:55,600 --> 00:19:56,600
 And so think about that.

307
00:19:56,600 --> 00:20:02,600
 So the Buddha says when you think about these things,

308
00:20:02,600 --> 00:20:06,600
 the mind becomes, let's say,

309
00:20:06,600 --> 00:20:09,600
 gains inspiration,

310
00:20:09,600 --> 00:20:12,600
 gains joy,

311
00:20:12,600 --> 00:20:15,600
 when a joyful rapture arises,

312
00:20:15,600 --> 00:20:19,600
 for one with a rapturous mind, the body becomes tranquil.

313
00:20:19,600 --> 00:20:22,600
 When tranquil in body feels pleasure,

314
00:20:22,600 --> 00:20:26,600
 for one feeling pleasure, the mind becomes concentrated.

315
00:20:26,600 --> 00:20:29,600
 So it's the kind of pleasure that's based on wholesomeness.

316
00:20:29,600 --> 00:20:33,600
 Not all pleasure is based on unwholesomeness.

317
00:20:33,600 --> 00:20:36,600
 It's a pleasure that comes from thinking of good things,

318
00:20:36,600 --> 00:20:39,600
 rejoicing in good things.

319
00:20:39,600 --> 00:20:41,600
 The mind becomes concentrated.

320
00:20:41,600 --> 00:20:44,600
 This is called a noble disciple who dwells in balance,

321
00:20:44,600 --> 00:20:47,600
 I mean an unbalanced populace,

322
00:20:47,600 --> 00:20:53,600
 who dwells unafflicted amid an unafflicted populace.

323
00:20:53,600 --> 00:20:55,600
 As one who has entered the stream of the Dhamma,

324
00:20:55,600 --> 00:21:03,600
 develops recollection of these things.

325
00:21:03,600 --> 00:21:09,600
 It's a way of dwelling in the world without suffering.

326
00:21:09,600 --> 00:21:11,600
 So these are protection.

327
00:21:11,600 --> 00:21:14,600
 These six are useful as protection.

328
00:21:14,600 --> 00:21:16,710
 The Buddha is not saying that these are what leads you to

329
00:21:16,710 --> 00:21:17,600
 enlightenment,

330
00:21:17,600 --> 00:21:19,960
 he's saying these are things that allow you to have a

331
00:21:19,960 --> 00:21:20,600
 balanced mind,

332
00:21:20,600 --> 00:21:25,600
 stay focused and balanced and not get lost in samsara.

333
00:21:25,600 --> 00:21:29,600
 Think about these six things from time to time.

334
00:21:29,600 --> 00:21:34,600
 So first you notice how he preposes it with actual core,

335
00:21:34,600 --> 00:21:40,600
 hardcore teachings, the five faculties,

336
00:21:40,600 --> 00:21:48,600
 confidence, effort, mindfulness, concentration and wisdom.

337
00:21:48,600 --> 00:21:52,600
 But then before talking about these other things,

338
00:21:52,600 --> 00:21:56,600
 like if you're already practicing well,

339
00:21:56,600 --> 00:21:57,600
 how do you protect it?

340
00:21:57,600 --> 00:22:00,820
 You protect it in these ways by thinking about things like

341
00:22:00,820 --> 00:22:01,600
 this group of six,

342
00:22:01,600 --> 00:22:05,600
 the Anusati.

343
00:22:05,600 --> 00:22:08,600
 They're useful meditations.

344
00:22:08,600 --> 00:22:11,600
 So we still practice mindfulness as our main meditation,

345
00:22:11,600 --> 00:22:14,600
 but from time to time we think about the Buddha, the Dhamma

346
00:22:14,600 --> 00:22:14,600
, the Sangha,

347
00:22:14,600 --> 00:22:16,600
 especially living in the world.

348
00:22:16,600 --> 00:22:22,600
 Easy to get lost when you have to go to work and so on.

349
00:22:22,600 --> 00:22:27,760
 So this keeps you focused and reminds you of what's

350
00:22:27,760 --> 00:22:29,600
 important.

351
00:22:29,600 --> 00:22:36,600
 So that's the Dhamma for tonight.

352
00:22:36,600 --> 00:22:52,600
 Do you have any questions tonight?

353
00:22:52,600 --> 00:22:59,600
 We have a question from last night.

354
00:22:59,600 --> 00:23:04,600
 Why are human beings forced to live when suffering and fut

355
00:23:04,600 --> 00:23:04,600
ility

356
00:23:04,600 --> 00:23:08,600
 are the only things we know or read?

357
00:23:08,600 --> 00:23:13,600
 This is the funny thing about humans.

358
00:23:13,600 --> 00:23:17,600
 We have this idea that there should be a purpose,

359
00:23:17,600 --> 00:23:20,600
 everything happens for a purpose, etc.

360
00:23:20,600 --> 00:23:26,090
 But we also have the idea that there's some rhyme or reason

361
00:23:26,090 --> 00:23:26,600
 to things.

362
00:23:26,600 --> 00:23:30,600
 So your question is kind of weird because you acknowledge

363
00:23:30,600 --> 00:23:32,600
 that there's no purpose,

364
00:23:32,600 --> 00:23:36,230
 but then you assume that there's something forcing us to

365
00:23:36,230 --> 00:23:36,600
 live,

366
00:23:36,600 --> 00:23:40,310
 like there's something like a god or something which

367
00:23:40,310 --> 00:23:43,600
 Buddhism doesn't acknowledge.

368
00:23:43,600 --> 00:23:46,600
 So what we know is that we live.

369
00:23:46,600 --> 00:23:51,600
 There's no answer to why we live ignorance.

370
00:23:51,600 --> 00:24:07,600
 There's no root cause.

371
00:24:07,600 --> 00:24:25,600
 If there are no questions, I'm going to go.

372
00:24:25,600 --> 00:24:47,600
 Alright.

373
00:24:47,600 --> 00:25:05,600
 Have a good night everyone. I'll try to be back tomorrow.

