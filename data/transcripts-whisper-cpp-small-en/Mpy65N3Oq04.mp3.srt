1
00:00:00,000 --> 00:00:10,600
 Okay, good evening everyone.

2
00:00:10,600 --> 00:00:24,080
 Welcome to our evening Dhamma session.

3
00:00:24,080 --> 00:00:31,080
 So I've been invited by a friend, a friend of mine.

4
00:00:31,080 --> 00:00:37,450
 One of the monks here is putting on a big conference in a

5
00:00:37,450 --> 00:00:40,280
 couple weeks.

6
00:00:40,280 --> 00:00:43,800
 And they needed people to give, to speak at it.

7
00:00:43,800 --> 00:00:46,720
 I'm giving a talk.

8
00:00:46,720 --> 00:00:50,950
 I don't want to make it sound like somehow they sought me

9
00:00:50,950 --> 00:00:52,040
 out for it.

10
00:00:52,040 --> 00:00:58,040
 I think I was just available.

11
00:00:58,040 --> 00:01:09,320
 But one of the talks is going to be on wrong mindfulness.

12
00:01:09,320 --> 00:01:16,280
 And the other is going to be on Buddhism in a digital age.

13
00:01:16,280 --> 00:01:22,210
 So I thought maybe in the next week or so I can go over

14
00:01:22,210 --> 00:01:24,720
 some of the ideas, test them

15
00:01:24,720 --> 00:01:28,960
 out on you.

16
00:01:28,960 --> 00:01:35,400
 My captive audience.

17
00:01:35,400 --> 00:01:38,680
 So tonight I thought I'd talk about modern Buddhism.

18
00:01:38,680 --> 00:01:44,160
 Buddhism in a digital age.

19
00:01:44,160 --> 00:01:49,720
 Even just generally a modern age.

20
00:01:49,720 --> 00:02:03,560
 There's one fairly clear debate or division in modern

21
00:02:03,560 --> 00:02:07,200
 Buddhism and that's between engagement

22
00:02:07,200 --> 00:02:11,800
 and renunciation.

23
00:02:11,800 --> 00:02:17,120
 I think most Buddhism is renunciant.

24
00:02:17,120 --> 00:02:20,160
 Is on the side of renunciation.

25
00:02:20,160 --> 00:02:24,240
 Leaving society going off in the forest.

26
00:02:24,240 --> 00:02:27,260
 When you think of Buddhism, I think it's still more common

27
00:02:27,260 --> 00:02:29,000
 to think of the Buddha off under

28
00:02:29,000 --> 00:02:32,840
 a tree in the forest.

29
00:02:32,840 --> 00:02:40,080
 But there are those who struggle with that and argue for a

30
00:02:40,080 --> 00:02:44,680
 Buddhism or feel more comfortable

31
00:02:44,680 --> 00:02:49,480
 practicing a Buddhism that is engaged.

32
00:02:49,480 --> 00:02:56,360
 That stays in the world and works to better the world.

33
00:02:56,360 --> 00:03:08,280
 Works to engage with the world rather than disengage.

34
00:03:08,280 --> 00:03:14,200
 It's a struggle that Buddhists individually go through and

35
00:03:14,200 --> 00:03:17,000
 it's a division between various

36
00:03:17,000 --> 00:03:29,360
 Buddhist institutions, some very social oriented and some

37
00:03:29,360 --> 00:03:35,240
 very reclusive oriented.

38
00:03:35,240 --> 00:03:42,140
 And so this is seen as sort of a modern dilemma I think but

39
00:03:42,140 --> 00:03:46,200
 it really is actually an ancient

40
00:03:46,200 --> 00:03:47,200
 dilemma.

41
00:03:47,200 --> 00:03:51,290
 In the time of the Buddha there was this question of

42
00:03:51,290 --> 00:03:54,880
 whether one should be engaged or one should

43
00:03:54,880 --> 00:04:02,120
 be secluded.

44
00:04:02,120 --> 00:04:06,940
 And so you have Devadatta, the Buddhist cousin who made all

45
00:04:06,940 --> 00:04:09,240
 sorts of trouble and did all

46
00:04:09,240 --> 00:04:12,480
 sorts of wicked things.

47
00:04:12,480 --> 00:04:21,520
 He tried to entrap the Buddha by proposing five rules, one

48
00:04:21,520 --> 00:04:22,640
 of which was that monks should

49
00:04:22,640 --> 00:04:26,560
 always live in the forest.

50
00:04:26,560 --> 00:04:29,840
 And he knew that the Buddha wouldn't accept these rules and

51
00:04:29,840 --> 00:04:31,600
 so by proposing them he wanted

52
00:04:31,600 --> 00:04:37,060
 to show that the Buddha was soft, was unfit to be the

53
00:04:37,060 --> 00:04:38,320
 leader.

54
00:04:38,320 --> 00:04:48,800
 That Devadatta was the true hardcore dedicated leader.

55
00:04:48,800 --> 00:04:51,560
 But the Buddha said of course no, he said no monks don't.

56
00:04:51,560 --> 00:04:54,600
 If monks live in the city they should practice Buddhism.

57
00:04:54,600 --> 00:05:10,440
 If monks live in the forest they should also practice the D

58
00:05:10,440 --> 00:05:12,120
hamma.

59
00:05:12,120 --> 00:05:16,780
 And this isn't even originally a Buddhist debate, this is

60
00:05:16,780 --> 00:05:19,160
 in the context of Brahmanism

61
00:05:19,160 --> 00:05:23,970
 which had this sort of question as well of how do you fit

62
00:05:23,970 --> 00:05:26,440
 in this idea of going off in

63
00:05:26,440 --> 00:05:27,440
 the forest.

64
00:05:27,440 --> 00:05:32,380
 And it has sort of this, India is interesting because there

65
00:05:32,380 --> 00:05:34,800
's this fairly clear meeting

66
00:05:34,800 --> 00:05:39,650
 of cultures, the indigenous culture and the Aryan culture,

67
00:05:39,650 --> 00:05:42,240
 wherever the Aryans came from,

68
00:05:42,240 --> 00:05:49,760
 these fire-worshipping, horse-riding barbarians.

69
00:05:49,760 --> 00:05:54,330
 And so it appears that there's this sort of, there arose

70
00:05:54,330 --> 00:05:57,120
 this high brow cultured society

71
00:05:57,120 --> 00:06:07,080
 that was very metropolitan, very urban.

72
00:06:07,080 --> 00:06:11,440
 And then there were these mystics, shamans, this is where

73
00:06:11,440 --> 00:06:13,800
 the word shaman probably comes

74
00:06:13,800 --> 00:06:21,730
 from, shramana, who were from a fairly indigenous culture

75
00:06:21,730 --> 00:06:26,720
 but they were these wise men mostly

76
00:06:26,720 --> 00:06:30,110
 who went off in the forest and had deep mystical

77
00:06:30,110 --> 00:06:34,520
 experiences or maybe they were just crazy.

78
00:06:34,520 --> 00:06:40,920
 But were looked upon as holy men.

79
00:06:40,920 --> 00:06:45,150
 And so as society developed there was a question of how

80
00:06:45,150 --> 00:06:49,880
 these men fit in and what was the proper,

81
00:06:49,880 --> 00:06:54,550
 as everything, as all these indigenous cultures go they

82
00:06:54,550 --> 00:06:57,880
 eventually become institutionalized.

83
00:06:57,880 --> 00:07:03,610
 And so eventually they tried at least, it seems, to set up

84
00:07:03,610 --> 00:07:06,640
 some kind of system because it was

85
00:07:06,640 --> 00:07:09,660
 disruptive for men to just decide and women sometimes that

86
00:07:09,660 --> 00:07:11,240
 they were going to leave home

87
00:07:11,240 --> 00:07:17,080
 and go off and live in the forest.

88
00:07:17,080 --> 00:07:20,240
 And so they made these four ages.

89
00:07:20,240 --> 00:07:24,630
 The proper way to do it was to wait until you're old and

90
00:07:24,630 --> 00:07:25,560
 retire.

91
00:07:25,560 --> 00:07:33,420
 You're a student and then you're a householder and then you

92
00:07:33,420 --> 00:07:36,920
 retire but live at home and then

93
00:07:36,920 --> 00:07:40,430
 once everything is settled, passed on to your children, you

94
00:07:40,430 --> 00:07:42,000
'll even go off and live in the

95
00:07:42,000 --> 00:07:44,400
 forest.

96
00:07:44,400 --> 00:07:51,880
 The sannyasa stage.

97
00:07:51,880 --> 00:07:55,040
 And so this is what the Buddha came into and the Buddha was

98
00:07:55,040 --> 00:07:56,560
 of course one of these men

99
00:07:56,560 --> 00:07:59,880
 although he didn't wait to retire.

100
00:07:59,880 --> 00:08:03,360
 He was quite the rebel.

101
00:08:03,360 --> 00:08:08,440
 He left home when he was 29 apparently.

102
00:08:08,440 --> 00:08:25,360
 Still just preparing to become leader of his clan.

103
00:08:25,360 --> 00:08:29,680
 But I think what you can see from the Buddhist teaching and

104
00:08:29,680 --> 00:08:31,680
 maybe you can get a sense of

105
00:08:31,680 --> 00:08:36,440
 where I'm going with this is that there isn't a clear

106
00:08:36,440 --> 00:08:39,240
 division in the Buddhist teaching.

107
00:08:39,240 --> 00:08:44,640
 There isn't a decision made on one way or the other.

108
00:08:44,640 --> 00:08:52,820
 I think clearly the goal is renouncing the world in one

109
00:08:52,820 --> 00:08:57,880
 sense but in another sense it's

110
00:08:57,880 --> 00:09:08,440
 to some extent taking other people with you in general.

111
00:09:08,440 --> 00:09:14,840
 And thereby working with the world.

112
00:09:14,840 --> 00:09:17,520
 Freedom from the world doesn't come without the world.

113
00:09:17,520 --> 00:09:21,760
 It's not like a jail cell you can just break out of.

114
00:09:21,760 --> 00:09:27,040
 This is what you're learning in the meditation course.

115
00:09:27,040 --> 00:09:30,240
 You can't just get away from your problems.

116
00:09:30,240 --> 00:09:32,880
 That's not the way out of suffering.

117
00:09:32,880 --> 00:09:37,880
 It's your problems that free you from your problems.

118
00:09:37,880 --> 00:09:43,020
 The satipatthana sutta is a very good, it's a very special

119
00:09:43,020 --> 00:09:47,880
 teaching, very powerful teaching.

120
00:09:47,880 --> 00:09:51,360
 Just simple words when you're angry know that you're angry.

121
00:09:51,360 --> 00:09:54,920
 When you're in pain know that it's pain.

122
00:09:54,920 --> 00:09:58,280
 When you're walking know that you're walking.

123
00:09:58,280 --> 00:10:02,880
 Not running away from anything.

124
00:10:02,880 --> 00:10:05,160
 And the world either.

125
00:10:05,160 --> 00:10:07,600
 To some extent the world is part of our practice.

126
00:10:07,600 --> 00:10:10,840
 Other people are part of our practice.

127
00:10:10,840 --> 00:10:15,760
 And so engagement is a part of our practice.

128
00:10:15,760 --> 00:10:20,360
 Renunciation and engagement.

129
00:10:20,360 --> 00:10:25,320
 And so when I look at Buddhism, I look at this division and

130
00:10:25,320 --> 00:10:26,480
 I think it's an important

131
00:10:26,480 --> 00:10:27,480
 one.

132
00:10:27,480 --> 00:10:32,060
 But rather than a division among Buddhists, it's a division

133
00:10:32,060 --> 00:10:34,360
 among Buddhist practices.

134
00:10:34,360 --> 00:10:37,540
 There are social practices and there are spiritual practice

135
00:10:37,540 --> 00:10:37,640
.

136
00:10:37,640 --> 00:10:42,640
 They're engaged and they're renunciate practices.

137
00:10:42,640 --> 00:10:46,960
 And both I think arguably are, should be, or at least can

138
00:10:46,960 --> 00:10:49,840
 be part of our past enlightenment.

139
00:10:49,840 --> 00:10:56,800
 So, what I'm going to try to do in my talk, in fact this

140
00:10:56,800 --> 00:11:00,760
 might be exciting, I'm going

141
00:11:00,760 --> 00:11:04,910
 to try to figure out the exact time and see if we can do a

142
00:11:04,910 --> 00:11:07,320
 second life session live.

143
00:11:07,320 --> 00:11:10,450
 So have all of you here with me and then about halfway

144
00:11:10,450 --> 00:11:12,680
 through just pop it up and show that

145
00:11:12,680 --> 00:11:19,080
 you've all been sitting there listening.

146
00:11:19,080 --> 00:11:23,370
 Just to show the power of second life really as a

147
00:11:23,370 --> 00:11:26,080
 communication platform.

148
00:11:26,080 --> 00:11:34,820
 But I'm going to try to highlight some of the tools that I

149
00:11:34,820 --> 00:11:38,880
've created or some of the

150
00:11:38,880 --> 00:11:47,080
 means by which I've used the internet, used technology.

151
00:11:47,080 --> 00:11:52,660
 And as some other people have created, I mean talk about

152
00:11:52,660 --> 00:11:56,080
 the Buddha Center for example.

153
00:11:56,080 --> 00:12:01,890
 But I want to make clear this distinction that our social

154
00:12:01,890 --> 00:12:05,320
 practice, I mean a good example

155
00:12:05,320 --> 00:12:08,640
 of a social practice is Metta.

156
00:12:08,640 --> 00:12:16,800
 Metta is a meditative practice.

157
00:12:16,800 --> 00:12:26,520
 You just sit quietly alone and send love to the whole world

158
00:12:26,520 --> 00:12:26,720
.

159
00:12:26,720 --> 00:12:32,150
 But it's social as well in the sense of it changes the way

160
00:12:32,150 --> 00:12:34,120
 we react to others.

161
00:12:34,120 --> 00:12:45,400
 It changes the way we interact with others.

162
00:12:45,400 --> 00:12:47,720
 We approach other people with love as a result.

163
00:12:47,720 --> 00:12:51,770
 I mean when we talk about engaged Buddhism we mean changing

164
00:12:51,770 --> 00:12:52,760
 the world.

165
00:12:52,760 --> 00:12:56,390
 We mean doing something to improve the world not just

166
00:12:56,390 --> 00:12:58,760
 running away and freeing ourselves.

167
00:12:58,760 --> 00:13:04,240
 The fact is improving your own mind does improve the world.

168
00:13:04,240 --> 00:13:08,680
 Metta is a very social practice.

169
00:13:08,680 --> 00:13:11,080
 Changing yourself is a very social practice.

170
00:13:11,080 --> 00:13:14,600
 To that end renunciation in a Buddhist sense which of

171
00:13:14,600 --> 00:13:16,200
 course is internal.

172
00:13:16,200 --> 00:13:23,640
 It has nothing really to do with running away to the forest

173
00:13:23,640 --> 00:13:24,160
.

174
00:13:24,160 --> 00:13:27,000
 Is in fact a social practice.

175
00:13:27,000 --> 00:13:32,050
 I mean part of it is social in the sense that it changes

176
00:13:32,050 --> 00:13:35,000
 your social interactions.

177
00:13:35,000 --> 00:13:36,000
 It makes you wiser.

178
00:13:36,000 --> 00:13:41,880
 I mean the most important thing it does is teaches you.

179
00:13:41,880 --> 00:13:45,280
 Teaches you right from wrong, good from bad.

180
00:13:45,280 --> 00:13:54,340
 It makes your mind clear when dealing with challenges,

181
00:13:54,340 --> 00:13:59,440
 difficulties, conflicts.

182
00:13:59,440 --> 00:14:05,880
 I don't know if there really is this division in fact.

183
00:14:05,880 --> 00:14:10,240
 Maybe two sides of the practice.

184
00:14:10,240 --> 00:14:13,420
 The Buddha answering this question of helping yourself or

185
00:14:13,420 --> 00:14:14,520
 helping others.

186
00:14:14,520 --> 00:14:17,800
 He says when you help yourself you help other people.

187
00:14:17,800 --> 00:14:23,240
 When you help other people you help yourself.

188
00:14:23,240 --> 00:14:25,640
 I don't think there was an equivalence there.

189
00:14:25,640 --> 00:14:30,700
 It's very much clear that helping yourself is a much better

190
00:14:30,700 --> 00:14:32,440
 thing to focus on.

191
00:14:32,440 --> 00:14:36,840
 There are certainly teachings to that extent.

192
00:14:36,840 --> 00:14:39,730
 And renunciation and going off to live in the forest is a

193
00:14:39,730 --> 00:14:41,400
 great and wonderful thing.

194
00:14:41,400 --> 00:14:44,560
 But it's certainly not all of Buddhism.

195
00:14:44,560 --> 00:14:53,500
 And I don't think there's a need to create a division of

196
00:14:53,500 --> 00:14:55,160
 sorts.

197
00:14:55,160 --> 00:14:58,040
 That's the first part of my talk I think.

198
00:14:58,040 --> 00:15:01,400
 The talk about all the technology I use.

199
00:15:01,400 --> 00:15:05,930
 Of course that's very interesting but I don't know that it

200
00:15:05,930 --> 00:15:09,080
's a topic for tonight's Dhamma.

201
00:15:09,080 --> 00:15:11,840
 So that's sort of the direction I'm heading to talk about.

202
00:15:11,840 --> 00:15:14,840
 This idea of engaged versus renunciate.

203
00:15:14,840 --> 00:15:15,840
 Renunciant.

204
00:15:15,840 --> 00:15:22,520
 I don't know the actual word.

205
00:15:22,520 --> 00:15:27,080
 But for us I think that's important.

206
00:15:27,080 --> 00:15:29,240
 It's important to be clear.

207
00:15:29,240 --> 00:15:30,600
 This isn't running away.

208
00:15:30,600 --> 00:15:32,800
 Coming here isn't running away.

209
00:15:32,800 --> 00:15:36,320
 Your practice doesn't end here in the meditation center.

210
00:15:36,320 --> 00:15:40,000
 This is a tool that you use in your life.

211
00:15:40,000 --> 00:15:42,440
 And it's a tool that you use here to help you with your

212
00:15:42,440 --> 00:15:42,880
 life.

213
00:15:42,880 --> 00:15:49,920
 To better deal with your challenges and conflicts and so on

214
00:15:49,920 --> 00:15:50,400
.

215
00:15:50,400 --> 00:15:52,200
 So a little bit of food for thought.

216
00:15:52,200 --> 00:15:54,400
 Dhamma for tonight.

217
00:15:54,400 --> 00:16:00,720
 Thank you all for coming up.

218
00:16:00,720 --> 00:16:02,320
 And I've got the question page up.

219
00:16:02,320 --> 00:16:09,920
 So I can answer some questions here.

220
00:16:09,920 --> 00:16:13,520
 So completely coincidental.

221
00:16:13,520 --> 00:16:16,280
 The top question and it is completely coincidental.

222
00:16:16,280 --> 00:16:20,060
 The top question is about what my thoughts are on important

223
00:16:20,060 --> 00:16:21,840
 issues like climate change

224
00:16:21,840 --> 00:16:24,630
 and how realization could help ease people's bias against

225
00:16:24,630 --> 00:16:25,480
 these crises.

226
00:16:25,480 --> 00:16:33,320
 In fact it's a good point to bring up in my talk.

227
00:16:33,320 --> 00:16:36,420
 Because of course things like climate change are brought

228
00:16:36,420 --> 00:16:38,280
 about by nothing more than greed.

229
00:16:38,280 --> 00:16:39,280
 At the root.

230
00:16:39,280 --> 00:16:44,160
 You know if we weren't so greedy for consumption.

231
00:16:44,160 --> 00:16:46,080
 Even just greedy to procreate.

232
00:16:46,080 --> 00:16:47,080
 Too many babies.

233
00:16:47,080 --> 00:16:49,760
 As the Dalai Lama said we need more monks.

234
00:16:49,760 --> 00:16:52,920
 There's too many people in this world.

235
00:16:52,920 --> 00:16:55,280
 I'm not sure about that.

236
00:16:55,280 --> 00:16:59,920
 I don't know what the ethics are there but.

237
00:16:59,920 --> 00:17:06,880
 I don't know about population.

238
00:17:06,880 --> 00:17:12,600
 I won't touch that one but certainly for consumption.

239
00:17:12,600 --> 00:17:16,080
 Our consumption is way out of hand.

240
00:17:16,080 --> 00:17:23,950
 And our need for products regardless of the cost to the

241
00:17:23,950 --> 00:17:26,480
 environment.

242
00:17:26,480 --> 00:17:32,300
 Our need for instant gratification is completely.

243
00:17:32,300 --> 00:17:35,560
 Even just our need for meat for example.

244
00:17:35,560 --> 00:17:36,600
 We don't need to eat meat.

245
00:17:36,600 --> 00:17:39,720
 We have no reason to eat meat.

246
00:17:39,720 --> 00:17:46,620
 And it's incredibly expensive to the climate and even to

247
00:17:46,620 --> 00:17:49,200
 our individuals.

248
00:17:49,200 --> 00:17:55,080
 So yeah I think our practice certainly improves these.

249
00:17:55,080 --> 00:17:58,120
 And I've talked about this before but I think things like

250
00:17:58,120 --> 00:18:00,120
 climate change are not incredibly

251
00:18:00,120 --> 00:18:04,080
 worth focusing on because it's like a band-aid solution.

252
00:18:04,080 --> 00:18:07,720
 Sure you get people to be concerned about the climate.

253
00:18:07,720 --> 00:18:10,360
 And if you don't.

254
00:18:10,360 --> 00:18:14,830
 Well I mean looking at the climate does make people more

255
00:18:14,830 --> 00:18:17,360
 aware of their greed and force

256
00:18:17,360 --> 00:18:19,080
 people to give up their greed.

257
00:18:19,080 --> 00:18:26,790
 But I mean let's be clear that the real problem, the most

258
00:18:26,790 --> 00:18:30,920
 important thing to address is our

259
00:18:30,920 --> 00:18:31,920
 greed.

260
00:18:31,920 --> 00:18:44,720
 Did the Buddha possess any supernatural powers or miracles?

261
00:18:44,720 --> 00:18:49,660
 I mean I've never met the Buddha not in this life obviously

262
00:18:49,660 --> 00:18:50,040
.

263
00:18:50,040 --> 00:18:56,900
 But I have no problem believing that the Buddha had powers

264
00:18:56,900 --> 00:19:01,280
 beyond what most of us would believe

265
00:19:01,280 --> 00:19:10,520
 possible or powers beyond what most of us are capable of.

266
00:19:10,520 --> 00:19:14,680
 We have to hold our hands up to our stomach as opposed to

267
00:19:14,680 --> 00:19:16,760
 laying them on our laps.

268
00:19:16,760 --> 00:19:17,920
 Is either position okay?

269
00:19:17,920 --> 00:19:18,920
 Yes.

270
00:19:18,920 --> 00:19:24,320
 The body position in our tradition not so important.

271
00:19:24,320 --> 00:19:25,320
 It's all about the mind.

272
00:19:25,320 --> 00:19:29,000
 The mind is what's more important.

273
00:19:29,000 --> 00:19:32,570
 And Theravāda recently read that only those who practice

274
00:19:32,570 --> 00:19:34,760
 meditative monastic life can attain

275
00:19:34,760 --> 00:19:37,160
 spiritual perfection.

276
00:19:37,160 --> 00:19:39,520
 Enlightenment is not thought possible for those living in

277
00:19:39,520 --> 00:19:40,440
 the secular life.

278
00:19:40,440 --> 00:19:43,760
 Of course it's easier that way but could it be possible?

279
00:19:43,760 --> 00:19:45,820
 Yes whoever wrote that doesn't know what they're talking

280
00:19:45,820 --> 00:19:46,240
 about.

281
00:19:46,240 --> 00:19:49,120
 There's a queen who became an arahant.

282
00:19:49,120 --> 00:19:50,120
 Very famous example.

283
00:19:50,120 --> 00:19:53,680
 It's very Theravāda Buddha's the queen.

284
00:19:53,680 --> 00:19:56,400
 And the Buddha said "Ah well either she becomes a bhikkhuni

285
00:19:56,400 --> 00:19:57,840
 or she just wastes away because

286
00:19:57,840 --> 00:20:01,560
 there's no way she's going to be able to be a queen."

287
00:20:01,560 --> 00:20:08,340
 And so the king said "Well then let her become a bhikkhuni

288
00:20:08,340 --> 00:20:09,000
."

289
00:20:09,000 --> 00:20:10,000
 Okay this is a long question.

290
00:20:10,000 --> 00:20:14,050
 I'm not going to read it all but well you know maybe it's

291
00:20:14,050 --> 00:20:14,760
 nice.

292
00:20:14,760 --> 00:20:17,650
 Person who suffered extreme stress for many years, ten plus

293
00:20:17,650 --> 00:20:19,280
 years, withdrawn into serious

294
00:20:19,280 --> 00:20:22,970
 depression, debilitating anxiety, been seriously studying

295
00:20:22,970 --> 00:20:25,080
 your YouTube videos for a few months

296
00:20:25,080 --> 00:20:28,160
 but am new to consistent daily practice.

297
00:20:28,160 --> 00:20:31,060
 And today I don't know if you've picked up my booklet so

298
00:20:31,060 --> 00:20:32,840
 there are many ways to practice

299
00:20:32,840 --> 00:20:36,120
 but I recommend reading my booklet if you haven't.

300
00:20:36,120 --> 00:20:38,760
 Today experience something wonderful.

301
00:20:38,760 --> 00:20:45,160
 So a sudden burst of warm intense happiness.

302
00:20:45,160 --> 00:20:47,080
 Tears of joy.

303
00:20:47,080 --> 00:20:49,400
 Can't recall such an overwhelming sensation.

304
00:20:49,400 --> 00:20:52,080
 Came out of nowhere.

305
00:20:52,080 --> 00:20:54,100
 Half of me just wants to share my excitement and the other

306
00:20:54,100 --> 00:20:55,240
 half curious wants to know is

307
00:20:55,240 --> 00:20:59,480
 this normal.

308
00:20:59,480 --> 00:21:00,480
 So well great.

309
00:21:00,480 --> 00:21:04,610
 I mean it's great for you to be able to see that you're not

310
00:21:04,610 --> 00:21:10,080
 cursed, you're not stuck.

311
00:21:10,080 --> 00:21:19,490
 I would say you can see that now that your brain is capable

312
00:21:19,490 --> 00:21:22,160
 of pleasure and that the

313
00:21:22,160 --> 00:21:29,020
 meditation clearly helps you break out of this rut because

314
00:21:29,020 --> 00:21:32,720
 our behaviors become habits.

315
00:21:32,720 --> 00:21:36,600
 And so things like depression for ten years that's got you

316
00:21:36,600 --> 00:21:38,400
 stuck in a fairly deep rut

317
00:21:38,400 --> 00:21:41,880
 but miracle of miracles just a little bit of meditation and

318
00:21:41,880 --> 00:21:43,240
 you can see you can pop

319
00:21:43,240 --> 00:21:45,280
 yourself back out of it.

320
00:21:45,280 --> 00:21:48,910
 Why because those ten years they aren't it's not like you

321
00:21:48,910 --> 00:21:51,160
 add up those ten years and you've

322
00:21:51,160 --> 00:21:55,160
 got ten years worth of depression.

323
00:21:55,160 --> 00:21:56,600
 Old stuff fades.

324
00:21:56,600 --> 00:21:58,520
 We're still only dealing with the present moment.

325
00:21:58,520 --> 00:22:01,690
 So if in this moment you start meditating people wonder how

326
00:22:01,690 --> 00:22:03,400
 can a few weeks of meditation

327
00:22:03,400 --> 00:22:07,130
 counteract years of depression or anxieties because those

328
00:22:07,130 --> 00:22:08,520
 years don't exist.

329
00:22:08,520 --> 00:22:11,520
 Their past it's just you counting.

330
00:22:11,520 --> 00:22:14,080
 The truth is well what are you now and it's based on what

331
00:22:14,080 --> 00:22:15,320
 you've done before.

332
00:22:15,320 --> 00:22:17,670
 There's no question that all of that had an effect but you

333
00:22:17,670 --> 00:22:18,820
're still just dealing with

334
00:22:18,820 --> 00:22:22,160
 you and your brain right.

335
00:22:22,160 --> 00:22:27,880
 And so creating new habits of mindfulness I mean it works.

336
00:22:27,880 --> 00:22:29,860
 It pops you out of it.

337
00:22:29,860 --> 00:22:33,410
 So your next step of course is to learn to become mindful

338
00:22:33,410 --> 00:22:35,240
 of the joy because that's not

339
00:22:35,240 --> 00:22:36,240
 the goal.

340
00:22:36,240 --> 00:22:37,640
 It's a good sign.

341
00:22:37,640 --> 00:22:39,980
 It's a sign that you're learning to let go of the

342
00:22:39,980 --> 00:22:41,720
 depression and so on and find ways

343
00:22:41,720 --> 00:22:47,640
 to be more centered more more peaceful.

344
00:22:47,640 --> 00:22:49,680
 But the joy itself is something that you have to note

345
00:22:49,680 --> 00:22:51,280
 otherwise you get attached to it and

346
00:22:51,280 --> 00:22:55,510
 you'll be looking for it and you'll even get upset when it

347
00:22:55,510 --> 00:22:56,800
 doesn't come.

348
00:22:56,800 --> 00:22:59,890
 I mean you'll see this as you practice but just to give you

349
00:22:59,890 --> 00:23:01,320
 a bit of a shortcut so you

350
00:23:01,320 --> 00:23:02,880
 don't get caught up in it.

351
00:23:02,880 --> 00:23:04,720
 You don't get caught up in anything.

352
00:23:04,720 --> 00:23:08,080
 The Buddha said nothing is worth clinging to so

353
00:23:08,080 --> 00:23:10,840
 congratulations good for you but the

354
00:23:10,840 --> 00:23:15,690
 next step when and maybe it won't come again maybe not for

355
00:23:15,690 --> 00:23:18,080
 a long time maybe never but

356
00:23:18,080 --> 00:23:22,810
 learn to deal with it when it comes pleasure when it comes

357
00:23:22,810 --> 00:23:25,280
 and desire for it excitement

358
00:23:25,280 --> 00:23:28,680
 about it all of that is part of the practice.

359
00:23:28,680 --> 00:23:33,160
 It's not to take away from this experience it's a great

360
00:23:33,160 --> 00:23:33,920
 sign.

361
00:23:33,920 --> 00:23:36,200
 It's just onward and upward.

362
00:23:36,200 --> 00:23:37,200
 There's more to do.

363
00:23:37,200 --> 00:23:38,200
 Good for you.

364
00:23:38,200 --> 00:23:39,200
 Really good.

365
00:23:39,200 --> 00:23:40,680
 Really nice to hear.

366
00:23:40,680 --> 00:23:44,120
 Thank you for writing that out.

367
00:23:44,120 --> 00:23:46,950
 I suffer from low self-esteem and it can only be boosted

368
00:23:46,950 --> 00:23:48,280
 through activities.

369
00:23:48,280 --> 00:23:50,680
 How can I cultivate it permanently?

370
00:23:50,680 --> 00:23:54,480
 Well we don't need self-esteem we're worthless.

371
00:23:54,480 --> 00:23:59,200
 You are worthless I am worthless.

372
00:23:59,200 --> 00:24:02,390
 If you can figure that out then you don't have to worry

373
00:24:02,390 --> 00:24:03,840
 about self-esteem.

374
00:24:03,840 --> 00:24:04,840
 Stop esteeming yourself.

375
00:24:04,840 --> 00:24:05,840
 It's useless.

376
00:24:05,840 --> 00:24:06,840
 We are worthless.

377
00:24:06,840 --> 00:24:11,300
 We are worthless means all of this is worthless meaningless

378
00:24:11,300 --> 00:24:13,320
 doesn't it's what you make of

379
00:24:13,320 --> 00:24:15,040
 it.

380
00:24:15,040 --> 00:24:19,570
 You can pretend that this house and your job and your face

381
00:24:19,570 --> 00:24:22,160
 and your body has some meaning.

382
00:24:22,160 --> 00:24:26,120
 Your brain your intelligence your memory.

383
00:24:26,120 --> 00:24:31,080
 You can claim that it all has meaning and worth but it's

384
00:24:31,080 --> 00:24:33,560
 all just subjective it's what

385
00:24:33,560 --> 00:24:35,200
 you make of it.

386
00:24:35,200 --> 00:24:38,630
 And that frees you seeing that frees you because then you

387
00:24:38,630 --> 00:24:41,160
 can look at well what's really important

388
00:24:41,160 --> 00:24:48,720
 and what's really important is happiness and suffering

389
00:24:48,720 --> 00:24:51,520
 peace and stress.

390
00:24:51,520 --> 00:24:54,900
 I have a sense of always being alone even when with others

391
00:24:54,900 --> 00:24:56,320
 in groups not a sense of

392
00:24:56,320 --> 00:24:59,220
 loneliness but it's like I'm the only one there is this

393
00:24:59,220 --> 00:24:59,840
 normal.

394
00:24:59,840 --> 00:25:02,980
 So these questions of what is normal are not I've talked

395
00:25:02,980 --> 00:25:05,000
 about this before it's not proper

396
00:25:05,000 --> 00:25:08,200
 to ask and I should put it in the things please don't ask

397
00:25:08,200 --> 00:25:10,080
 is it normal or please you know

398
00:25:10,080 --> 00:25:13,110
 rephrase it and ask yourself what do you really want to

399
00:25:13,110 --> 00:25:15,080
 know and you want to know whether

400
00:25:15,080 --> 00:25:19,310
 you are a freak and is that what you're asking because you

401
00:25:19,310 --> 00:25:21,480
 are you and that's most important.

402
00:25:21,480 --> 00:25:25,960
 It's most important to see what is asking what's normal

403
00:25:25,960 --> 00:25:28,520
 doesn't do us any good because

404
00:25:28,520 --> 00:25:35,340
 well who cares maybe normal is a horrible thing to be right

405
00:25:35,340 --> 00:25:35,840
.

406
00:25:35,840 --> 00:25:38,810
 We're not trying to be normal we're trying to be good and

407
00:25:38,810 --> 00:25:40,600
 pure and perfect to some extent

408
00:25:40,600 --> 00:25:43,720
 free we're trying to be free.

409
00:25:43,720 --> 00:25:47,230
 So good question would be is that is that a positive state

410
00:25:47,230 --> 00:25:49,040
 is it a negative state those

411
00:25:49,040 --> 00:25:50,480
 would be better questions to ask.

412
00:25:50,480 --> 00:25:54,380
 I mean I'm not criticizing this people ask this all the

413
00:25:54,380 --> 00:25:56,440
 time but it's just I think it

414
00:25:56,440 --> 00:25:59,480
 may be a little lazy because we you know is this normal.

415
00:25:59,480 --> 00:26:03,200
 It doesn't it's not a useful question to got it twice

416
00:26:03,200 --> 00:26:05,520
 tonight and I'm not I'm not attacking

417
00:26:05,520 --> 00:26:06,520
 you for it.

418
00:26:06,520 --> 00:26:09,370
 I think I would probably ask the same thing but it's it's

419
00:26:09,370 --> 00:26:10,920
 you have to think it through

420
00:26:10,920 --> 00:26:13,160
 what do you really want to know.

421
00:26:13,160 --> 00:26:17,880
 So good question is is it is it good is it bad is it

422
00:26:17,880 --> 00:26:20,960
 probably more important.

423
00:26:20,960 --> 00:26:24,440
 So it's neither I mean it's an experience.

424
00:26:24,440 --> 00:26:29,110
 So try to note it and be mindful of your feeling feeling or

425
00:26:29,110 --> 00:26:30,280
 something.

426
00:26:30,280 --> 00:26:38,920
 If you like it or dislike it you know that and so on.

427
00:26:38,920 --> 00:26:43,920
 Okay that's all the questions for tonight.

428
00:26:43,920 --> 00:26:44,920
 1

429
00:26:44,920 --> 00:26:45,920
 1

