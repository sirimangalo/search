1
00:00:00,000 --> 00:00:12,100
 How did you come to the practice of meditation? How did it

2
00:00:12,100 --> 00:00:14,880
 change your life?

3
00:00:14,880 --> 00:00:20,870
 So I first came across the concept of meditation when I was

4
00:00:20,870 --> 00:00:24,000
 about 14. I was walking from school

5
00:00:24,000 --> 00:00:27,800
 and then there was this poster which said "Learn to achieve

6
00:00:27,800 --> 00:00:29,120
 inner peace". I thought,

7
00:00:29,120 --> 00:00:34,140
 "Oh, that sounds really nice", but I was much more

8
00:00:34,140 --> 00:00:38,800
 interested with playing with my friends.

9
00:00:38,800 --> 00:00:44,780
 So I never actually pursued that, but I remember it very

10
00:00:44,780 --> 00:00:47,760
 distinctly. I thought this sounds

11
00:00:47,760 --> 00:00:52,270
 very interesting. Who doesn't want inner peace, right? But

12
00:00:52,270 --> 00:00:56,960
 it wasn't until I was about 19.

13
00:00:58,480 --> 00:01:02,800
 I experienced this very severe sort of depression.

14
00:01:02,800 --> 00:01:09,460
 I was coming into adulthood and there was so much confusion

15
00:01:09,460 --> 00:01:11,920
. I was thinking about careers,

16
00:01:11,920 --> 00:01:17,360
 relationships, meaning of life, purpose of life.

17
00:01:17,360 --> 00:01:25,840
 And I was thoroughly overwhelmed. So I thought, "Well,

18
00:01:25,840 --> 00:01:28,790
 it must be something out there that can help me. I must

19
00:01:28,790 --> 00:01:30,400
 have just googled, which is very

20
00:01:30,400 --> 00:01:38,610
 wonderful that we can learn about meditation through the

21
00:01:38,610 --> 00:01:40,720
 internet."

22
00:01:40,720 --> 00:01:47,200
 So through this I came across one term, Vada, Mok, Vener

23
00:01:47,200 --> 00:01:49,520
able Ajahn Brahm.

24
00:01:54,080 --> 00:02:00,250
 And from then on I started to practice mindfulness of

25
00:02:00,250 --> 00:02:01,760
 breathing.

26
00:02:01,760 --> 00:02:07,890
 I didn't really have proper instructions and I was sort of

27
00:02:07,890 --> 00:02:09,760
 just swinging it.

28
00:02:09,760 --> 00:02:16,380
 But I think it was a form of samatha practice. I felt I

29
00:02:16,380 --> 00:02:20,160
 could calm myself when I needed to

30
00:02:20,160 --> 00:02:25,550
 after a long day of work or if I was just feeling upset, I

31
00:02:25,550 --> 00:02:30,320
 could sit down and feel good again.

32
00:02:30,320 --> 00:02:38,240
 So it became this crutch and I think within a few years I

33
00:02:38,240 --> 00:02:39,680
 didn't feel this

34
00:02:39,680 --> 00:02:42,640
 depression anymore that I once had felt.

35
00:02:46,480 --> 00:02:50,970
 However, something was bothering me. I thought, "Well, when

36
00:02:50,970 --> 00:02:53,680
 I'm sitting I feel this

37
00:02:53,680 --> 00:03:03,190
 niceness, but during everyday activities when I'm at work

38
00:03:03,190 --> 00:03:05,280
 or interacting with family and friends,

39
00:03:05,280 --> 00:03:11,590
 a lot of negative emotions come up within me and I wish I

40
00:03:11,590 --> 00:03:14,640
 could just take this meditation everywhere."

41
00:03:14,640 --> 00:03:25,860
 So I was very desperate actually just to find something

42
00:03:25,860 --> 00:03:27,120
 which would do that

43
00:03:27,120 --> 00:03:32,690
 because I thought, "What good is it if it's only localised

44
00:03:32,690 --> 00:03:33,680
 calm?"

45
00:03:38,080 --> 00:03:42,990
 I even tried maybe what they call kasina meditation where I

46
00:03:42,990 --> 00:03:45,360
 would visualise a candle flame

47
00:03:45,360 --> 00:03:50,030
 and try to maintain that throughout the day. But it seemed

48
00:03:50,030 --> 00:03:51,200
 like I was just trying to

49
00:03:51,200 --> 00:03:56,390
 patch things together and I didn't have a well thought out

50
00:03:56,390 --> 00:03:59,920
 detailed way of doing it.

51
00:03:59,920 --> 00:04:04,880
 So I searched that walking meditation. I thought walking is

52
00:04:04,880 --> 00:04:06,720
 simple. Surely there's a way to walk

53
00:04:06,720 --> 00:04:09,990
 and meditate that someone's explained quite well. And Bh

54
00:04:09,990 --> 00:04:14,400
ante Yuta Damopikush you know turned up in

55
00:04:14,400 --> 00:04:19,570
 these results and I was really glad. There was a sense of

56
00:04:19,570 --> 00:04:23,040
 relief because I thought, "Okay, now I can

57
00:04:23,040 --> 00:04:29,440
 have someone who knows what they're talking about instruct

58
00:04:29,440 --> 00:04:31,600
 me properly."

59
00:04:31,600 --> 00:04:43,480
 And this particular way of practice, we pass on our

60
00:04:43,480 --> 00:04:45,520
 practice with Bhante Yuta Damopikush,

61
00:04:45,520 --> 00:04:53,050
 changed my life in the sense that the calm that I was

62
00:04:53,050 --> 00:04:59,120
 really wanting to take over with me, I

63
00:05:00,960 --> 00:05:05,470
 learned to stop looking for that. I realised it wasn't

64
00:05:05,470 --> 00:05:08,640
 quite the point and there was something

65
00:05:08,640 --> 00:05:14,730
 much more deeper than this sense of calm that I was chasing

66
00:05:14,730 --> 00:05:17,200
, I was dependent on.

67
00:05:24,320 --> 00:05:29,360
 Why do you think the Meditation Centre project is important

68
00:05:29,360 --> 00:05:32,320
? What courses have you taken through

69
00:05:32,320 --> 00:05:37,520
 Surimangalu and what did you get out of it? Well,

70
00:05:37,520 --> 00:05:42,320
 unfortunately for me I haven't had the opportunity

71
00:05:42,320 --> 00:05:48,400
 to go on retreat and be instructed in person by Bhante Yuta

72
00:05:48,400 --> 00:05:50,160
 Damopikush.

73
00:05:52,560 --> 00:05:56,320
 However, what I've done is the at-home meditation course

74
00:05:56,320 --> 00:06:03,280
 and this course I felt has been tremendously

75
00:06:03,280 --> 00:06:08,580
 helpful. Now, right now we're in the middle of a pandemic.

76
00:06:08,580 --> 00:06:12,480
 There's this virus which is

77
00:06:12,480 --> 00:06:17,320
 taking away people's lives and it's a deeply, deeply

78
00:06:17,320 --> 00:06:21,360
 troubled world but I have never felt

79
00:06:21,360 --> 00:06:24,640
 less worried and more grounded than I do now.

80
00:06:24,640 --> 00:06:33,930
 And that's because of this at-home meditation course kind

81
00:06:33,930 --> 00:06:35,360
 of guiding me to

82
00:06:35,360 --> 00:06:41,920
 relate to a world which is troubled in a different sense.

83
00:06:41,920 --> 00:06:47,920
 So that's, I think that's all I can say about that.

84
00:06:47,920 --> 00:06:53,920
 But with that in mind,

85
00:06:53,920 --> 00:07:01,480
 if a meditation course at home can do that, imagine what a

86
00:07:01,480 --> 00:07:03,920
 meditation course in a sense,

87
00:07:03,920 --> 00:07:09,050
 in a supportive environment with a teacher in person,

88
00:07:09,050 --> 00:07:11,280
 imagine what that would do.

89
00:07:15,200 --> 00:07:19,440
 It's literally something which would be amazing to put into

90
00:07:19,440 --> 00:07:23,280
 reality. So that's my take on that.

91
00:07:23,280 --> 00:07:31,930
 Can you tell the story about how mindfulness served you in

92
00:07:31,930 --> 00:07:34,160
 daily life?

93
00:07:37,520 --> 00:07:46,320
 Mindfulness has been really good for me in terms of

94
00:07:46,320 --> 00:07:55,650
 how responsible I am and the amount of stress that I

95
00:07:55,650 --> 00:07:58,320
 experience.

96
00:08:00,720 --> 00:08:07,830
 Because when you are having mindfulness throughout the day,

97
00:08:07,830 --> 00:08:08,640
 your outlets, you're

98
00:08:08,640 --> 00:08:12,380
 watching over yourself, you are able to adhere to your

99
00:08:12,380 --> 00:08:15,040
 moral principles, to your duties,

100
00:08:15,040 --> 00:08:19,890
 to your responsibilities much more consistently and it's so

101
00:08:19,890 --> 00:08:22,480
 hard to forget them. So

102
00:08:22,480 --> 00:08:27,280
 I guess you could say my life has been more successful,

103
00:08:27,280 --> 00:08:28,720
 that's for sure.

104
00:08:28,720 --> 00:08:37,610
 How has your practice helped make life better for those

105
00:08:37,610 --> 00:08:40,000
 around you?

106
00:08:40,000 --> 00:08:50,320
 In a related sense, the practice has made life better for

107
00:08:50,320 --> 00:08:51,840
 those around me because they

108
00:08:52,560 --> 00:08:57,750
 no longer have to help me with my issues, they don't have

109
00:08:57,750 --> 00:09:00,640
 to support me as much financially and

110
00:09:00,640 --> 00:09:05,490
 emotionally because I'm much more of a contributor. I help

111
00:09:05,490 --> 00:09:07,760
 them more and I need less help from them,

112
00:09:07,760 --> 00:09:11,040
 which means that they can focus on their lives and enjoy

113
00:09:11,040 --> 00:09:14,800
 their lives more. You become this much

114
00:09:14,800 --> 00:09:20,310
 more reliable and effective person in your in your set of

115
00:09:20,310 --> 00:09:22,720
 people that you know.

116
00:09:22,720 --> 00:09:29,520
 What has been challenging about your path?

117
00:09:41,040 --> 00:09:44,240
 I think one of the things that really challenged me was

118
00:09:44,240 --> 00:09:48,480
 learning about more aspects of myself than

119
00:09:48,480 --> 00:09:53,850
 I had ever thought about. With Samatha practice, I wasn't

120
00:09:53,850 --> 00:09:56,640
 aware of all these feelings and thoughts

121
00:09:56,640 --> 00:10:01,390
 that I have that come up within me and what was really

122
00:10:01,390 --> 00:10:04,560
 challenging was for me to learn about all

123
00:10:04,560 --> 00:10:09,600
 these aspects of myself because it can be really dist

124
00:10:09,600 --> 00:10:12,960
ressing to see the full spectrum of

125
00:10:12,960 --> 00:10:24,480
 unwholesomeness that's within you, like how much of a

126
00:10:24,480 --> 00:10:28,880
 terrible person you are capable of being.

127
00:10:28,880 --> 00:10:35,320
 So I found that distressing challenging because when I

128
00:10:35,320 --> 00:10:37,920
 looked at that and even now when I look at

129
00:10:37,920 --> 00:10:44,010
 that it's the process of also where I react to it when I

130
00:10:44,010 --> 00:10:47,040
 don't like what I'm seeing and I don't

131
00:10:47,040 --> 00:10:52,290
 really want to look at it. So that's been challenging but

132
00:10:52,290 --> 00:10:55,920
 what's even more challenging than that is

133
00:10:57,280 --> 00:11:05,460
 remembering to be mindful. I think Bante Utada Mupikku says

134
00:11:05,460 --> 00:11:08,880
 that mindfulness sati means to remember

135
00:11:08,880 --> 00:11:13,630
 but I deeply feel remembering to remember is very really

136
00:11:13,630 --> 00:11:16,160
 challenging. There's so many

137
00:11:16,160 --> 00:11:22,080
 things that we're addicted to food, entertainment, the

138
00:11:22,080 --> 00:11:26,400
 feelings that we get from other people

139
00:11:27,280 --> 00:11:32,060
 and it's so much easier to escape using those than to

140
00:11:32,060 --> 00:11:36,000
 consistently look at yourself and

141
00:11:36,000 --> 00:11:41,140
 and learn about yourself. So you have to work very hard to

142
00:11:41,140 --> 00:11:43,440
 be consistently mindful

143
00:11:43,440 --> 00:11:48,790
 but it is challenging in that sense. Yeah, remembering to

144
00:11:48,790 --> 00:11:51,600
 remember is a very challenging

145
00:11:53,120 --> 00:11:56,480
 it's a very challenging process.

146
00:11:56,480 --> 00:12:12,760
 So the Sre Mangalo community has many features to it. It's

147
00:12:12,760 --> 00:12:13,920
 got this wonderful

148
00:12:13,920 --> 00:12:17,060
 discord server where people can meet up read and ask

149
00:12:17,060 --> 00:12:20,000
 questions. It's got another platform

150
00:12:20,000 --> 00:12:28,180
 usremangalo.com to ask questions and it's really wonderful

151
00:12:28,180 --> 00:12:30,160
 in the sense that when you hear other

152
00:12:30,160 --> 00:12:33,000
 people asking questions you learn a lot from their

153
00:12:33,000 --> 00:12:35,280
 questions and of course you also get to

154
00:12:35,280 --> 00:12:39,950
 ask questions yourself. So it's a massive learning process

155
00:12:39,950 --> 00:12:42,560
 and it's the kind of things that you learn

156
00:12:42,560 --> 00:12:50,420
 and they really are life-changing. Now the community itself

157
00:12:50,420 --> 00:12:52,240
 is also very welcoming.

158
00:12:52,240 --> 00:12:57,820
 I've been in other spiritual communities before but in the

159
00:12:57,820 --> 00:13:00,000
 Sre Mangalo community you really get

160
00:13:00,000 --> 00:13:03,950
 the sense that people are not separated. What I mean is in

161
00:13:03,950 --> 00:13:06,800
 other communities I felt like things

162
00:13:06,800 --> 00:13:10,720
 like gender, ethnicity, race, age, matters. You know you

163
00:13:10,720 --> 00:13:13,120
 get people banding together because of

164
00:13:13,120 --> 00:13:19,110
 those similarities but in the Sre Mangalo community these

165
00:13:19,110 --> 00:13:22,160
 characteristics that normally separate

166
00:13:22,160 --> 00:13:26,990
 people you really get a sense that they're not they're not

167
00:13:26,990 --> 00:13:31,120
 relevant. So it's a very healthy,

168
00:13:31,120 --> 00:13:42,370
 balanced and supportive spiritual family. Yeah and people

169
00:13:42,370 --> 00:13:45,280
 that are always so helpful

170
00:13:45,280 --> 00:13:47,840
 that's really nice and

171
00:13:47,840 --> 00:13:54,890
 yeah you really get the sense that you're not you're not un

172
00:13:54,890 --> 00:13:57,760
-welcomed or inconvenient in any fashion.

173
00:13:58,720 --> 00:14:02,560
 Is there anything else you'd like to share?

174
00:14:02,560 --> 00:14:09,440
 I guess I could say to anybody who's watching this out of

175
00:14:09,440 --> 00:14:14,160
 curiosity or just considering taking

176
00:14:14,160 --> 00:14:19,390
 up this way of practice that it doesn't have to try it. You

177
00:14:19,390 --> 00:14:22,160
 might be very surprised about

178
00:14:23,760 --> 00:14:29,360
 where it takes you so

179
00:14:29,360 --> 00:14:39,140
 so that's all I can say about that. Thank you for giving me

180
00:14:39,140 --> 00:14:41,440
 the opportunity to share

181
00:14:41,440 --> 00:14:45,760
 some of what I know and my perspective.

182
00:14:46,320 --> 00:14:51,360
 Thank you.

