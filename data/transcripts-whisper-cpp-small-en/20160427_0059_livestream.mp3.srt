1
00:00:00,000 --> 00:00:16,320
 Good evening everyone.

2
00:00:16,320 --> 00:00:38,160
 Broadcasting live April 26th.

3
00:00:38,160 --> 00:00:50,550
 Today is a quote again about anger. This time from Nwisudhi

4
00:00:50,550 --> 00:00:51,680
 Maga.

5
00:00:51,680 --> 00:00:54,980
 It's under the teaching about metta. So in order to

6
00:00:54,980 --> 00:00:56,680
 cultivate metta you have to be

7
00:00:56,680 --> 00:01:04,480
 skillful and overcoming anger. And yet there's this set of

8
00:01:04,480 --> 00:01:05,800
 verses that

9
00:01:05,800 --> 00:01:15,960
 form tonight's quote. So the first stanza is, "If someone

10
00:01:15,960 --> 00:01:22,280
 hurts you,

11
00:01:22,280 --> 00:01:26,720
 hurts you insofar as they can, they've done whatever they

12
00:01:26,720 --> 00:01:31,320
 can to hurt you.

13
00:01:31,320 --> 00:01:37,480
 That makes you hurt once. You hurt because of the things

14
00:01:37,480 --> 00:01:44,320
 that they've done."

15
00:01:44,320 --> 00:01:47,180
 And then the question is, the question you should ask

16
00:01:47,180 --> 00:01:47,960
 yourself.

17
00:01:47,960 --> 00:01:54,080
 So this quote is actually supposed to be directed to

18
00:01:54,080 --> 00:01:55,760
 yourself.

19
00:01:55,760 --> 00:01:59,360
 When you get angry about something someone has done to you,

20
00:01:59,360 --> 00:02:03,910
 you should ask yourself, "Why do I want to hurt myself

21
00:02:03,910 --> 00:02:05,640
 again?

22
00:02:05,640 --> 00:02:11,190
 Why am I sitting here hurting myself because someone has

23
00:02:11,190 --> 00:02:12,640
 hurt me?

24
00:02:12,640 --> 00:02:17,760
 Why am I doing it again? Why am I making it twice as bad?

25
00:02:17,760 --> 00:02:24,820
 Why am I helping my enemies? For people who want to bring

26
00:02:24,820 --> 00:02:25,640
 me suffering,

27
00:02:25,640 --> 00:02:33,160
 why do I suffer?" You think that anger is a valid response,

28
00:02:33,160 --> 00:02:38,360
 a proper response to anything really. Buddha said if you're

29
00:02:38,360 --> 00:02:39,160
 in pain and then

30
00:02:39,160 --> 00:02:43,840
 you get upset about it, it's like you have a thorn or a spl

31
00:02:43,840 --> 00:02:48,960
inter in your skin

32
00:02:48,960 --> 00:02:53,200
 and you take another thorn and you try to gouge it out.

33
00:02:53,200 --> 00:03:00,800
 You make it twice as bad.

34
00:03:00,800 --> 00:03:05,400
 The second stanza is, "In tears you left your family.

35
00:03:05,400 --> 00:03:10,510
 They had been kind and helpful too. So why not leave your

36
00:03:10,510 --> 00:03:11,480
 enemy the anger

37
00:03:11,480 --> 00:03:16,440
 that brings harm to you?"

38
00:03:16,440 --> 00:03:19,350
 So we left behind all that was dear to us to come to med

39
00:03:19,350 --> 00:03:19,880
itate,

40
00:03:19,880 --> 00:03:25,480
 to cultivate spirituality. We give up all the good things

41
00:03:25,480 --> 00:03:35,880
 and all the good things or pleasurable things in the world.

42
00:03:35,880 --> 00:03:39,080
 And yet we bring our enemies with us.

43
00:03:39,080 --> 00:03:43,880
 Now when we get angry, why are we holding on to bad things?

44
00:03:43,880 --> 00:03:53,380
 This is an admonishment, a self-admonishment for someone

45
00:03:53,380 --> 00:03:55,880
 who is angry.

46
00:03:55,880 --> 00:04:00,080
 Actually it's generally easier to give up anger than it is

47
00:04:00,080 --> 00:04:04,880
 to give up greed or craving.

48
00:04:04,880 --> 00:04:08,210
 And to this kind of admonishment it's actually quite, you

49
00:04:08,210 --> 00:04:10,880
 know, it's fairly simple.

50
00:04:10,880 --> 00:04:14,630
 Just remind yourself that this is hurting you. This is

51
00:04:14,630 --> 00:04:15,880
 harmful.

52
00:04:15,880 --> 00:04:25,880
 The anger, the reaction, the judging.

53
00:04:25,880 --> 00:04:30,680
 This anger that you entertain, the third stanza, this anger

54
00:04:30,680 --> 00:04:31,880
 that you entertain,

55
00:04:31,880 --> 00:04:35,880
 and again I'm reading actually Nyanamoli's translation

56
00:04:35,880 --> 00:04:39,880
 so it's a bit different from the one on our website.

57
00:04:39,880 --> 00:04:42,880
 This anger that you entertain is gnawing at the very roots

58
00:04:42,880 --> 00:04:46,880
 of all the virtue that you guard.

59
00:04:46,880 --> 00:04:50,880
 Who is there such a fool as you?

60
00:04:50,880 --> 00:04:56,880
 Anger, greed, they gnaw away at our goodness.

61
00:04:56,880 --> 00:05:04,300
 No, they chew at it. They eat away at all the good that we

62
00:05:04,300 --> 00:05:04,880
've done.

63
00:05:04,880 --> 00:05:10,570
 It gets spoiled. You can be helpful to people and

64
00:05:10,570 --> 00:05:12,880
 charitable and kind.

65
00:05:12,880 --> 00:05:17,690
 But you can be, then you're angry one time and you lose

66
00:05:17,690 --> 00:05:20,880
 your reputation as a good person.

67
00:05:20,880 --> 00:05:25,370
 And it inflames your mind and it prevents you from enjoying

68
00:05:25,370 --> 00:05:33,880
 the peace and happiness that come from goodness.

69
00:05:33,880 --> 00:05:37,880
 And remind ourselves of how it's eating away at our virtue.

70
00:05:37,880 --> 00:05:47,120
 It's making us more coarse. It helps us to give up the

71
00:05:47,120 --> 00:05:51,880
 inclination to be angry, to get angry.

72
00:05:51,880 --> 00:05:55,880
 And the next stanza, another does ignoble deeds.

73
00:05:55,880 --> 00:06:02,090
 So you are angry. How is this? Do you then want to copy to

74
00:06:02,090 --> 00:06:05,880
 the sort of acts that he commits?

75
00:06:05,880 --> 00:06:10,880
 So this person has done something evil and it is evil.

76
00:06:10,880 --> 00:06:16,380
 And so why in the world are you now, you who believe that

77
00:06:16,380 --> 00:06:17,880
 it was wrong,

78
00:06:17,880 --> 00:06:22,880
 why are you now doing the same thing?

79
00:06:22,880 --> 00:06:25,740
 That's a curious point because when you get angry at

80
00:06:25,740 --> 00:06:29,880
 someone who's evil, you're joining them in their evil.

81
00:06:29,880 --> 00:06:34,410
 In fact, the Buddha said, and it mentions in this up above

82
00:06:34,410 --> 00:06:37,880
 that it's worse to get angry back at someone.

83
00:06:37,880 --> 00:06:43,050
 It's a worse evil. Someone's angry at you and you return

84
00:06:43,050 --> 00:06:45,880
 the anger. That's worse.

85
00:06:45,880 --> 00:06:52,160
 Because we all get angry. Anger comes and if we love each

86
00:06:52,160 --> 00:06:56,880
 other then we let each other vent.

87
00:06:56,880 --> 00:07:01,060
 When someone gets angry at us, we appreciate what they're

88
00:07:01,060 --> 00:07:01,880
 saying.

89
00:07:01,880 --> 00:07:06,130
 We listen and we try to calm them down and appease them in

90
00:07:06,130 --> 00:07:06,880
 some way.

91
00:07:06,880 --> 00:07:10,360
 But when you get angry back, then you start the fight. You

92
00:07:10,360 --> 00:07:12,880
 start conflict.

93
00:07:12,880 --> 00:07:16,530
 That's when the real problem is. So, and this is a good

94
00:07:16,530 --> 00:07:16,880
 lesson, you know.

95
00:07:16,880 --> 00:07:20,110
 We judge each other very much and if someone gets angry, we

96
00:07:20,110 --> 00:07:21,880
 get angry back.

97
00:07:21,880 --> 00:07:24,880
 If someone is greedy, we get upset at them.

98
00:07:24,880 --> 00:07:29,060
 If someone is arrogant and conceited, we get angry and

99
00:07:29,060 --> 00:07:31,880
 upset about the bad things in others.

100
00:07:31,880 --> 00:07:35,880
 And then that makes us just as bad.

101
00:07:35,880 --> 00:07:40,710
 Moreover, we're too critical. Critical of ourselves and

102
00:07:40,710 --> 00:07:42,880
 critical of each other.

103
00:07:42,880 --> 00:07:46,910
 In monasteries, it's quite common for new monks to come and

104
00:07:46,910 --> 00:07:48,880
 just criticize roundly.

105
00:07:48,880 --> 00:07:52,960
 Criticize every other monk, all the old monks, all the

106
00:07:52,960 --> 00:07:57,420
 monks who have been there for a long time because they're

107
00:07:57,420 --> 00:07:58,880
 not perfect.

108
00:07:58,880 --> 00:08:02,480
 With this kind of conception that if you're not perfect,

109
00:08:02,480 --> 00:08:04,880
 then you deserve to be criticized.

110
00:08:04,880 --> 00:08:08,670
 But really, if you're not perfect, you deserve to be helped

111
00:08:08,670 --> 00:08:14,480
, to be supported and directed and guided and pushed and pro

112
00:08:14,480 --> 00:08:16,880
dded in the right direction, sure.

113
00:08:16,880 --> 00:08:21,390
 But to denounce someone because they're imperfect, that's

114
00:08:21,390 --> 00:08:22,880
 not the Buddhist way.

115
00:08:22,880 --> 00:08:28,270
 For more reasons than one. Because it's useless, it's un

116
00:08:28,270 --> 00:08:33,880
helpful, and because it's harmful to yourself as well.

117
00:08:35,880 --> 00:08:38,600
 The next stanza just says that when you get angry at

118
00:08:38,600 --> 00:08:44,260
 something someone has done, you do what they would have you

119
00:08:44,260 --> 00:08:44,880
 do.

120
00:08:44,880 --> 00:08:48,880
 They want you to suffer, and bingo, you're suffering.

121
00:08:48,880 --> 00:08:52,990
 And that's key because really other people can't make us

122
00:08:52,990 --> 00:08:53,880
 suffer.

123
00:08:53,880 --> 00:08:58,020
 They can hit you, they can denounce you, they can scold you

124
00:08:58,020 --> 00:08:59,880
, they can insult you.

125
00:08:59,880 --> 00:09:04,020
 But they can't make you suffer. They can't make your mind

126
00:09:04,020 --> 00:09:04,880
 suffer.

127
00:09:04,880 --> 00:09:09,880
 They can't make you get angry.

128
00:09:09,880 --> 00:09:16,510
 I mean, it depends how you look at it, but it's possible to

129
00:09:16,510 --> 00:09:20,880
 be free from anger no matter what someone else does.

130
00:09:20,880 --> 00:09:27,880
 But once you get angry, that's when the suffering comes.

131
00:09:27,880 --> 00:09:30,050
 There's no question you suffer when you're angry. You

132
00:09:30,050 --> 00:09:36,860
 suffer when you dislike something. That's where suffering

133
00:09:36,860 --> 00:09:39,880
 comes from.

134
00:09:39,880 --> 00:09:45,400
 Next stanza. If you get angry, then maybe you make him

135
00:09:45,400 --> 00:09:46,880
 suffer, maybe not.

136
00:09:46,880 --> 00:09:50,690
 Though with the hurt that anger brings, you certainly are

137
00:09:50,690 --> 00:09:51,880
 punished now.

138
00:09:51,880 --> 00:09:56,240
 So when you get angry back, usually it's because you are

139
00:09:56,240 --> 00:10:00,880
 thinking of ways to retaliate, to seek revenge.

140
00:10:00,880 --> 00:10:04,880
 And so there's like, well, maybe you can find revenge.

141
00:10:04,880 --> 00:10:07,770
 Maybe you'll actually end up harming the other person and

142
00:10:07,770 --> 00:10:08,880
 getting revenge.

143
00:10:08,880 --> 00:10:10,880
 We don't know. It's possible.

144
00:10:10,880 --> 00:10:14,980
 But what's certain is that you're punishing yourself with

145
00:10:14,980 --> 00:10:19,660
 your anger, and you punish yourself more if you seek

146
00:10:19,660 --> 00:10:20,880
 revenge.

147
00:10:20,880 --> 00:10:27,020
 If anger blinded enemies set out to tread the path of woe,

148
00:10:27,020 --> 00:10:31,880
 do you, by getting angry too, intend to follow heel to toe?

149
00:10:31,880 --> 00:10:36,880
 You intend to follow these anger blinded enemies.

150
00:10:36,880 --> 00:10:41,120
 If hurt is done, you buy a foe because of anger on your

151
00:10:41,120 --> 00:10:41,880
 part.

152
00:10:41,880 --> 00:10:53,110
 Then put your anger down for why should you be harassed

153
00:10:53,110 --> 00:10:56,880
 groundlessly?

154
00:10:56,880 --> 00:11:02,440
 Yes. Why? You weren't the one that did the evil. Why are

155
00:11:02,440 --> 00:11:03,880
 you suffering?

156
00:11:03,880 --> 00:11:09,240
 Someone hurts you or attacks you or criticizes you or so on

157
00:11:09,240 --> 00:11:11,880
. That's their problem.

158
00:11:11,880 --> 00:11:15,880
 Why are you suffering?

159
00:11:15,880 --> 00:11:19,720
 Since states last but a moment's time, those aggregates by

160
00:11:19,720 --> 00:11:22,880
 which was done, the odious act have ceased.

161
00:11:22,880 --> 00:11:25,880
 So now, what is it you are angry with?

162
00:11:25,880 --> 00:11:31,830
 This is very much in meditation. The person who we're angry

163
00:11:31,830 --> 00:11:33,880
 at doesn't exist.

164
00:11:33,880 --> 00:11:38,300
 The states that arose, that gave rise to the event that we

165
00:11:38,300 --> 00:11:40,880
're angry at, those have ceased.

166
00:11:40,880 --> 00:11:44,020
 When we're angry at something, worried about something, or

167
00:11:44,020 --> 00:11:47,880
 when we're attached to something, we want something.

168
00:11:47,880 --> 00:11:52,500
 That thing is only a concept. The experiences arise and

169
00:11:52,500 --> 00:11:52,880
 cease.

170
00:11:52,880 --> 00:11:55,850
 If it's a good thing, well, that good thing is not really

171
00:11:55,850 --> 00:11:56,880
 good in any way.

172
00:11:56,880 --> 00:12:01,310
 It just arises and ceases, comes and goes, and the same

173
00:12:01,310 --> 00:12:02,880
 with bad things.

174
00:12:02,880 --> 00:12:06,930
 There's no person who is our enemy. What are we angry at?

175
00:12:06,930 --> 00:12:08,880
 What really are we angry at?

176
00:12:08,880 --> 00:12:13,750
 What we're angry at is a memory, a thought, a thought that

177
00:12:13,750 --> 00:12:17,880
 arises in our mind that is an echo of some past deed.

178
00:12:17,880 --> 00:12:23,880
 And we get angry at that thought.

179
00:12:23,880 --> 00:12:27,810
 Whom shall he hurt, who seeks to hurt another in the other

180
00:12:27,810 --> 00:12:28,880
's absence?

181
00:12:28,880 --> 00:12:32,790
 Your presence is the cause of hurt. Why are you angry then

182
00:12:32,790 --> 00:12:33,880
 with him?

183
00:12:33,880 --> 00:12:37,140
 Yes, we're angry at ourselves, actually. It's our own mind

184
00:12:37,140 --> 00:12:40,880
 states, the thoughts that arise in our own mind.

185
00:12:40,880 --> 00:12:44,940
 The person who did this deed, if someone sits and is very

186
00:12:44,940 --> 00:12:46,880
 angry at another person,

187
00:12:46,880 --> 00:12:50,020
 it's nothing to do with the other person. It's you yourself

188
00:12:50,020 --> 00:12:51,880
 who are hurting yourself.

189
00:12:51,880 --> 00:12:55,880
 You're angry at thoughts that arise in your own mind.

190
00:12:55,880 --> 00:12:59,880
 Nothing to do with the other person.

191
00:12:59,880 --> 00:13:07,240
 So this has to do with anger. But more generally, it's the

192
00:13:07,240 --> 00:13:10,880
 idea that only we can hurt ourselves.

193
00:13:10,880 --> 00:13:17,410
 And happiness as well has to come from within. Happiness

194
00:13:17,410 --> 00:13:20,880
 can't be based on a thing.

195
00:13:20,880 --> 00:13:24,380
 And suffering doesn't come from things. Happiness and

196
00:13:24,380 --> 00:13:27,020
 suffering don't come from the outside. They come from

197
00:13:27,020 --> 00:13:27,880
 within.

198
00:13:27,880 --> 00:13:33,110
 It's quite simple. Our state of mind determines our state

199
00:13:33,110 --> 00:13:37,880
 of happiness, not the world around us.

200
00:13:37,880 --> 00:13:42,920
 This is crucial. It's a specific Buddhist doctrine that

201
00:13:42,920 --> 00:13:46,880
 happiness and suffering come from the mind.

202
00:13:46,880 --> 00:13:54,880
 This is clear. So a big part of our practice is learning to

203
00:13:54,880 --> 00:14:00,540
 be objective and to see the experiences that come from

204
00:14:00,540 --> 00:14:01,880
 external,

205
00:14:01,880 --> 00:14:14,820
 or that come from the body and the mind, to see them with

206
00:14:14,820 --> 00:14:22,880
 wisdom, to cultivate a relationship with experience that is

207
00:14:22,880 --> 00:14:27,880
 free from likes and dislikes,

208
00:14:27,880 --> 00:14:33,090
 to be objective and to see things as they are. Because when

209
00:14:33,090 --> 00:14:40,880
 you do, there's not intrinsic likeability or dislikeability

210
00:14:40,880 --> 00:14:40,880
,

211
00:14:40,880 --> 00:14:54,300
 attraction, attractiveness, or undetracktiveness. It's not

212
00:14:54,300 --> 00:14:58,880
 a quality of things.

213
00:14:58,880 --> 00:15:02,710
 And so when we look at things as they are, we find that

214
00:15:02,710 --> 00:15:07,270
 they're not attractive. We don't give rise to the

215
00:15:07,270 --> 00:15:08,880
 attraction.

216
00:15:08,880 --> 00:15:12,040
 Attraction and aversion, they come from not seeing things

217
00:15:12,040 --> 00:15:15,430
 as they are. That's key as well. Because it could be

218
00:15:15,430 --> 00:15:15,880
 otherwise.

219
00:15:15,880 --> 00:15:19,610
 Mostly we think it's otherwise. We think that some things

220
00:15:19,610 --> 00:15:22,880
 are intrinsically beautiful or desirable.

221
00:15:22,880 --> 00:15:30,660
 Some other things are intrinsically dislikeable, a direct

222
00:15:30,660 --> 00:15:33,880
 cause for disliking. That's not true.

223
00:15:33,880 --> 00:15:39,690
 When you see things as they are, it's only because of

224
00:15:39,690 --> 00:15:48,620
 delusion that we, because of misconception that we have

225
00:15:48,620 --> 00:15:52,880
 desire and aversion.

226
00:15:52,880 --> 00:16:02,880
 So that's our dhamma for tonight.

227
00:16:02,880 --> 00:16:06,470
 If you're interested in the Vysuddhi manga, we're actually

228
00:16:06,470 --> 00:16:12,050
 studying it on Sundays, but we're most of the way through

229
00:16:12,050 --> 00:16:13,880
 it already.

230
00:16:13,880 --> 00:16:24,140
 I appreciate the Vysuddhi manga. It's more like a manual or

231
00:16:24,140 --> 00:16:30,940
 a reference guide. It's good for teachers, also good for

232
00:16:30,940 --> 00:16:34,880
 meditators, experienced meditators, I think.

233
00:16:34,880 --> 00:16:39,050
 There's just so much in it, so many different, like this

234
00:16:39,050 --> 00:16:45,470
 set of verses, it's quite useful. That's from the Vysuddhi

235
00:16:45,470 --> 00:16:46,880
 manga.

236
00:16:46,880 --> 00:16:59,220
 You too can go. You don't have to stay. I'm just going to

237
00:16:59,220 --> 00:17:06,880
 answer questions if people have.

238
00:17:06,880 --> 00:17:15,680
 I was in New York. The monk I was staying with in New York

239
00:17:15,680 --> 00:17:22,990
 doesn't like the Vysuddhi manga, or the commentaries, or

240
00:17:22,990 --> 00:17:27,140
 the abhidhamma, which is very common with Western monks,

241
00:17:27,140 --> 00:17:30,880
 especially in the Ajahn Chah group.

242
00:17:30,880 --> 00:17:34,870
 Although he's not exactly with the Ajahn Chah group. They

243
00:17:34,870 --> 00:17:39,860
're so keen on the suttas, which is great. The suttas are

244
00:17:39,860 --> 00:17:42,880
 just the most pure.

245
00:17:42,880 --> 00:17:53,180
 But the suttas are not the only source of Buddhism. The s

246
00:17:53,180 --> 00:17:56,970
uttas are talks that were given, mostly that were given to

247
00:17:56,970 --> 00:18:00,880
 the monks, or given to certain people at a certain time.

248
00:18:00,880 --> 00:18:05,130
 And so they have to be interpreted, especially because we

249
00:18:05,130 --> 00:18:09,390
're so far removed from that time and that place. So no

250
00:18:09,390 --> 00:18:12,880
 matter what, you have to explain the suttas.

251
00:18:12,880 --> 00:18:16,190
 And so either you follow the commentary explanation, the V

252
00:18:16,190 --> 00:18:19,170
ysuddhi manga explanation, or you follow some modern

253
00:18:19,170 --> 00:18:20,880
 commentator's explanation.

254
00:18:20,880 --> 00:18:24,120
 And it turns out to be no better. Turns out to be actually

255
00:18:24,120 --> 00:18:27,730
 usually worse, I would say, I would argue. Worse in the

256
00:18:27,730 --> 00:18:32,020
 sense that whether you agree with it or not, far less

257
00:18:32,020 --> 00:18:39,130
 rigorous, far less objective, far less comprehensive, far

258
00:18:39,130 --> 00:18:40,880
 less in-depth.

259
00:18:40,880 --> 00:18:44,120
 The Vysuddhi manga just gets incredible work if you

260
00:18:44,120 --> 00:18:47,880
 disagree with it or disagree with it or disagree with it.

261
00:18:47,880 --> 00:18:52,520
 It's amazing that someone actually wrote it because it's

262
00:18:52,520 --> 00:18:56,880
 huge and it's so in-depth and it's got so much in it.

263
00:18:56,880 --> 00:19:01,150
 I remember I was giving a talk when I was in Sri Lanka last

264
00:19:01,150 --> 00:19:04,810
 time. And the man who was translating for me refused to

265
00:19:04,810 --> 00:19:08,320
 translate because I was quoting the Vysuddhi manga and he

266
00:19:08,320 --> 00:19:12,880
 said, "Oh, we don't follow the Vysuddhi manga here."

267
00:19:12,880 --> 00:19:15,680
 That was a first for me. I was giving a talk and the

268
00:19:15,680 --> 00:19:20,370
 translator wouldn't translate. I think I was saying

269
00:19:20,370 --> 00:19:23,880
 something mean or evil.

270
00:19:23,880 --> 00:19:27,940
 So it's a contentious, the Vysuddhi manga tradition is

271
00:19:27,940 --> 00:19:33,220
 fairly contentious to some people. Most of the, most Therav

272
00:19:33,220 --> 00:19:38,890
ada societies, for the most part, they follow the Vysuddhi

273
00:19:38,890 --> 00:19:39,880
 manga.

274
00:19:39,880 --> 00:19:43,910
 In Thailand, it forms the latter part of one's Pali studies

275
00:19:43,910 --> 00:19:47,850
. So any monk whose studies higher level Pali will have to

276
00:19:47,850 --> 00:19:51,790
 translate the Vysuddhi manga into Thai and then translate

277
00:19:51,790 --> 00:19:55,970
 Thai passages, Thai translations of the Vysuddhi manga back

278
00:19:55,970 --> 00:19:56,880
 into Pali.

279
00:19:56,880 --> 00:20:01,080
 So they have to really memorize. Many of them basically, I

280
00:20:01,080 --> 00:20:05,360
 think, will memorize the Vysuddhi manga in Pali in English,

281
00:20:05,360 --> 00:20:15,430
 which is impressive in itself. Maybe not memorize, but come

282
00:20:15,430 --> 00:20:17,880
 close.

283
00:20:17,880 --> 00:20:24,530
 Any requirements for taking a meditation course at our

284
00:20:24,530 --> 00:20:31,460
 center? Yeah, probably. I mean, if you're coming alone and

285
00:20:31,460 --> 00:20:37,080
 you're under 16, I think we'd have to have a talk about

286
00:20:37,080 --> 00:20:37,880
 that.

287
00:20:37,880 --> 00:20:42,280
 I don't know. I would imagine having people under 16 coming

288
00:20:42,280 --> 00:20:46,050
 and doing courses is problematic unless we talk to their

289
00:20:46,050 --> 00:20:48,880
 parents. I think 16 is probably.

290
00:20:48,880 --> 00:20:53,760
 And as for upper age limit, it's more to do with your

291
00:20:53,760 --> 00:20:58,280
 health, you know. If you're able to do walking and sitting

292
00:20:58,280 --> 00:21:01,970
 meditation for hours a day, it's fine. You know, you can

293
00:21:01,970 --> 00:21:03,880
 sit on a chair if you have to.

294
00:21:04,880 --> 00:21:11,270
 I don't know. Robin, I haven't really thought about the

295
00:21:11,270 --> 00:21:19,180
 future. I mean, there are some other things I'm looking at,

296
00:21:19,180 --> 00:21:20,880
 actually.

297
00:21:20,880 --> 00:21:27,040
 But like, actually, the Anguttranikai is a good volume and

298
00:21:27,040 --> 00:21:38,880
 I've started maybe collecting some good suttas from it.

299
00:21:38,880 --> 00:21:41,690
 A new set of robes for my trip to Asia. Now, the funny

300
00:21:41,690 --> 00:21:45,180
 thing about these robes, they're fairly new, right? But

301
00:21:45,180 --> 00:21:49,170
 they came with holes in them. Like, I think they were mouse

302
00:21:49,170 --> 00:21:50,880
 eaten or they were ripped or something.

303
00:21:50,880 --> 00:21:54,770
 Because in my back, there's actually little holes that I

304
00:21:54,770 --> 00:22:00,880
 probably shouldn't sew up. Not that that's here or there.

305
00:22:00,880 --> 00:22:02,880
 But I know these are fine in Asia.

306
00:22:02,880 --> 00:22:06,240
 It'll be a little hot, but you know, this upper robe I don

307
00:22:06,240 --> 00:22:10,570
't wear a lot of the time. When I'm in my room, I rarely

308
00:22:10,570 --> 00:22:11,880
 wear this.

309
00:22:11,880 --> 00:22:15,900
 Sometimes I just go bare chest or sometimes I have a basic

310
00:22:15,900 --> 00:22:20,930
 what's called an angsa. It's just a very thin shoulder robe

311
00:22:20,930 --> 00:22:22,880
, shoulder cloth.

312
00:22:31,880 --> 00:22:35,540
 But these aren't that hot. It's just cotton. So it's

313
00:22:35,540 --> 00:22:41,030
 actually not that hot. But it's going to be hot in June. So

314
00:22:41,030 --> 00:22:43,880
 I'll try to go up and live on the mountain.

315
00:22:43,880 --> 00:22:49,880
 I'm going to try to go up on the mountain in Sri Lanka.

316
00:22:49,880 --> 00:22:53,960
 I'm arguing with Sangha about that. Sangha wants me to stay

317
00:22:53,960 --> 00:22:58,270
 in Colombo for two weeks. I'm trying to push to take a week

318
00:22:58,270 --> 00:23:01,880
 out. He says a week isn't enough.

319
00:23:01,880 --> 00:23:05,380
 I know it isn't. I mean, a month isn't enough. I could

320
00:23:05,380 --> 00:23:09,380
 spend months in Sri Lanka giving talks every day, but not

321
00:23:09,380 --> 00:23:10,880
 really keen on it.

322
00:23:13,880 --> 00:23:21,670
 And then in Thailand, maybe go back up on the mountain as

323
00:23:21,670 --> 00:23:22,880
 well.

324
00:23:22,880 --> 00:23:25,990
 Probably not staying Jumdong very long because it's

325
00:23:25,990 --> 00:23:32,300
 probably pretty hot in June. Unless it started raining, it

326
00:23:32,300 --> 00:23:34,880
 should be okay.

327
00:23:34,880 --> 00:23:40,130
 Do we only have a few months left? Have we really gone

328
00:23:40,130 --> 00:23:45,680
 through most of the year? When do we start? August or

329
00:23:45,680 --> 00:23:46,880
 something?

330
00:23:46,880 --> 00:23:53,880
 July maybe? Time flies.

331
00:23:53,880 --> 00:24:09,880
 Tempest fuget. Time flies.

332
00:24:09,880 --> 00:24:30,880
 Okay. Good night everyone. See you all next time.

