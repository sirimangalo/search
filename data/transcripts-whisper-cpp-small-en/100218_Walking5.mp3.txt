 Meditation is something that we use just like we would to
 train the body,
 except we're using it to train the mind.
 So everything we do should be seen as some kind of
 development, some kind of exercise.
 And just like with physical exercise, we start with
 something very simple,
 because our body is not yet accustomed to the work, to the
 task.
 The same when we train the mind, when we give the mind a
 workout.
 We start with something very simple, say the rising and the
 falling of the abdomen.
 Or just walking, stepping, right, stepping left.
 And as we go on, we give more and more intricate, more and
 more complicated exercises.
 Because the mind becomes sharp.
 So this is for two reasons.
 One, because the mind becomes sharper and therefore wanders
,
 if not given a more difficult exercise to contemplate.
 And also because we want to develop the mind that much
 further.
 There is a simile given in the Risuddhi Maga, the Path of
 Purification,
 which is an ancient sort of summary of the teachings of the
 Buddha
 and explanation of the teachings of the Buddha.
 And he says, just like when you look at a candle,
 and you see the candle, it starts burning from the top,
 and when it gets to the bottom, it's gone.
 And you say to yourself, wow, that's impermanent.
 And you say, that's the burning of a candle.
 It's gone, it starts and it ends.
 But that's really looking at it in a very coarse way.
 When you want to understand the rising and ceasing,
 you want to understand impermanence, you have to look at
 the candle in more fine detail.
 So another time you might look at the candle and see that,
 oh, now it's a third of the way gone, now it's two-thirds
 gone, now it's gone.
 So you look at it in pieces, or you might look at it in
 smaller pieces,
 in little bits, until finally you look at it very, very
 close,
 and you see the strands burning.
 And you see the strands, as you pull the rope,
 the wick being pulled apart into its little pieces,
 and burning up moment by moment by moment.
 The simile here is with one's life.
 So someone might say, wow, look at that person, they were
 alive and now they're dead,
 or look at me, I'm going to be dead someday, and therefore
 life is impermanent.
 But that doesn't really give you any insight or any wisdom.
 It's some sort of coarse wisdom, but it's not strong enough
 to really help you understand impermanence.
 Again, when you think about how before you were young,
 and now you're an adult, and later you're going to be old,
 this is a much more detailed, or a bit more detailed way of
 looking at it.
 And so you break it up further and further and further,
 and you can break it up all the way to yesterday and today
 and tomorrow,
 and then you can go moment to moment to moment,
 and they say until finally you break the walking step up
 into six parts.
 And so this is where we get these parts of walking step.
 For beginners we give you the first walking step, which is
 stepping right, stepping left,
 and then as you go on we're going to give you more and more
 complicated walking steps
 until there's altogether six pieces, which is
 udharana, adiharanana, vidiharanana, usachana, sanikeepana,
 sanirampana,
 which are the six words in Pali given to the six parts of
 the step.
 Sitting meditation is the same. We just give you more
 exercises as a training
 in something which helps you to develop your mind that much
, that much more.
 So I don't think anyone should find any greater meaning
 than that.
 Why they broke it up into six, I don't know.
 I think it was just sort of a logical classification of the
 different parts.
 Like when you lift the heel, that's the first part.
 When you lift the rest of the foot, that's the second part.
 When you move it out, that's the third part.
 Actually the six steps in the vesuti mag are not exactly as
 we teach them,
 but I don't think that I have to go into detail about why
 that is.
 The second question is, why do you write the rule about not
 discussing practices with co-students?
 This is because, well, discussing practices, well, if this
 is referring to teaching other meditators,
 then certainly this is totally forbidden and this is
 something that can even possibly get you kicked out
 if you get into teaching others and giving them advice
 about their practice and so on.
 It's something that we strictly don't allow for many
 reasons.
 I don't think this is exactly what's being asked here, but
 I'll go into this first.
 First of all, it's very unlikely that you know exactly how
 to teach
 if you haven't been trained as a teacher.
 Being trained as a meditator and trained as a teacher are
 two very different things.
 We have to spend many, many hours sitting and listening to
 another teacher teach
 before we can actually go off and teach by ourselves.
 You have to sit and listen for hours and hours and days and
 days and months and months
 before you can actually be qualified just to begin to give
 people advice.
 So, I mean, it's just so easy to give people the wrong
 advice.
 You think this is the right advice because you know the
 answer.
 But just because you know the answer doesn't mean you know
 how to give it to the person
 and how to give them something useful and how to give them
 what they need.
 Sometimes it's quite different from what they want.
 The other thing is even if you do know what you're talking
 about,
 they still don't... any good teacher would never advise a
 student who is practicing under a different teacher.
 They would say, "Go and ask your teacher."
 Just like a doctor would never prescribe medicine to a
 patient of another doctor.
 You have to choose one doctor or the other because
 otherwise,
 even though the advice might be right, it's a different
 path and a different technique or it's a different style.
 And so this for sure is something that should be
 discouraged.
 Unless it's a very technical issue like you see someone
 closing their eyes when they walk
 and so they're swaying all over the place.
 You can mention to them that you're... as you understand it
,
 you're supposed to keep your eyes open when you walk.
 But even that is... you might find that the results are not
 as you expect.
 But here talking about your own practice with other people,
 this is what's much more common.
 We find meditators telling other meditators about their own
 practice,
 which is still quite harmful to the other person's practice
,
 because in general it creates expectation in their mind and
 comparison and worry and distraction.
 Oftentimes it will lead them to try and create the same
 state that they hear that you are experiencing.
 Often it will lead them to misunderstand their own state.
 It will lead them to become confused and unsure about what
 is the path.
 If you're on the wrong path, it's very easy to lead someone
 else on the wrong path.
 And so we have a rule against this because of the effect
 that it has on the other meditator.
 And because of the fact that they're not your teacher and
 they're not someone who you should trust with your practice
 and seek advice from, and because you can spoil their
 practice, these two things.
 So I hope that answers those questions. That's about as
 best as I can do.
 Again, if you have more questions, please leave them in the
 box, either in Thai or in English.
 Just make sure I can read it. If I can't read it, I won't
 answer it.
 I can read Thai, that's no problem.
 So today's talk, I actually wanted to talk about walking
 meditation and sitting meditation.
 I give this talk often about what are the benefits of
 walking and sitting meditation specifically.
 The benefits of walking meditation, the Buddha gave five
 different benefits.
 And this is something that we should understand. We will
 wonder why do we do walking as well.
 First a word on that, many people think that the Buddha
 didn't practice walking meditation.
 They think this is something modern that has been invented.
 Actually in the Buddhist time, not only did the Buddha walk
, practice walking meditation,
 other wanderers of other sects and other religions also
 practiced walking meditation.
 I mean, as far as we have written in the text, this is the
 case.
 We have the Buddha giving his own routine of how he would
 walk and sit all day.
 When he didn't have other things to do, he would just walk
 and sit all day in alteration.
 And then at night he would spend the first four hours from
 6 to 10 pm walking and sitting.
 From 10 to 2 he would do lying meditation, with a bit of
 sleep perhaps,
 sometimes giving talks or teaching in the lying position.
 And then at 2 am you would get up and start walking again
 and walking and sitting for the rest of the night.
 And then in the morning you can go for alms and continue
 with walking and sitting.
 And he gave the same advice to meditators. He said, "A monk
 should practice in this way,
 walking and sitting for the whole day, and then at night
 you separate it into three parts,
 and then at 10, same thing, walking, sitting, 10 to 2, that
's your lying down period,
 where if you want to sleep you can sleep. Four hours, and
 you keep in mind the time when you're going to wake up.
 And then at 2 am you wake up and start doing walking again.
 This is for someone who is very serious about the practice.
 This is sort of the ultimate goal of a meditator, is to get
 to this point.
 So walking meditation did play a big part. The Buddha said,
 and we have his words,
 what we understand to be his words, recorded, that there
 are five benefits of walking meditation,
 why he didn't have monks just do sitting meditation.
 He even had prescribed the size of walking paths and how to
 make a walking path
 and how it should be made, how it shouldn't be made and so
 on.
 The five benefits that he said are, one, it helps you to be
 patient and endure walking long distances.
 So nowadays, this might not be a big thing, but at that
 time it was big.
 The first one, walking long distances. The second one,
 enduring hard work,
 or just creating overall endurance.
 Number three, the food that has been eaten will be able to
 be digested easier.
 Number four, sicknesses and ailments in the body will be
 worked out and aided and even may disappear.
 And number five, the concentration that comes from walking
 meditation lasts a long time,
 as a strength which lasts for a long time.
 These are the five benefits that the Buddha gave.
 So as I said, the first one is something that might not be
 of great benefit nowadays to many people.
 We think that walking long distances is a pain anyway and
 something that we wouldn't ever consider doing.
 In the Buddhist time it was a big deal and it's kind of a
 shame that nowadays we feel this way
 because actually walking is something very special.
 If anyone's ever done long walking, it actually changes the
 way you look at the world.
 Instead of just whizzing by in a car.
 I know there were many philosophers who've written about
 this.
 Well, one philosopher anyway, the philosopher I'm thinking
 of is Thoreau.
 Henry David Thoreau was a naturalist who talked a lot about
 walking and he walked long distances.
 When I was in Thailand I did a lot of walking.
 Because I didn't touch money, I still don't touch money.
 So in Thailand sometimes it was the case that I had to walk
 from here to there,
 even in the city, on the side of the highway, in the forest
, walked in many different situations.
 And you really look at the world in a whole different way.
 You see people working on the side of the road, they smile,
 you get to see cows,
 all sorts of many different things.
 And you look at the world in a whole different way and it
 helps you to come back to reality.
 Instead of just whizzing around, always thinking about
 where you're going next,
 it brings you very much back to reality.
 Something that we should all consider an important part of
 the human life.
 Walking is something, it's one part of what we're built for
.
 This body is built to walk.
 So it's something that, as I'm going to talk about it, it
 helps the systems in the body.
 But walking meditation is something that allows you to do
 this sort of thing,
 allows you to walk long distances, where now we might
 always consider getting in the car
 and just to go down to the end of the road.
 Once you do walking meditation you find that walking long
 distances is actually a pleasure
 and something that you're happy to do,
 which is great because it helps the environment, it helps
 our lives, it makes things easier,
 we don't have to rely on these machines.
 In the Buddhist time, of course, it was necessary, and for
 monks it is necessary.
 Oftentimes during their lives they would have to walk long
 distances to see their teacher,
 just to go on alms round and so on.
 So we can consider that nowadays this is maybe one of the
 lesser advantages.
 A greater advantage is the overall endurance that it gives
 us.
 And this is something that's useful for people, not just
 monks,
 people living in the world who have to do hard work, who
 have to work every day.
 Walking meditation gives you endurance because you're
 forced to do the same repetitious motion again and again.
 Often people think that walking meditation is so silly
 because you're just walking back and forth for hours and
 hours a day.
 You're not getting anywhere.
 Actually this is a funny one because I read somewhere,
 there was a teacher, he said,
 "You can think that where you're going, the answer is you
're walking to nibhana, you're walking to freedom."
 And as you walk back and forth people say, "Where do you
 think you're going to go? You're walking to freedom."
 And he said, "Suppose you pretend that freedom from
 suffering is like a thousand kilometers away,
 and then you count how many kilometers you walk in a week
 or so, when walking meditation.
 And you can really feel happy because it's not actually
 that hard, you can actually get there.
 I don't know, a thousand kilometers or how long, how far?
 As opposed to a thousand kilometers is quite a bit.
 If you consider how much you walk, if it was a certain
 number of kilometers away,
 then after a while, maybe a year or two years or something,
 you could eventually become totally free from suffering.
 Buddha said seven years at max.
 But on a more worldly level, it gives you a great amount of
 endurance, because you're doing this repetitious motion.
 So it helps you do away with things like boredom, agitation
, distraction.
 It helps your mind focus and calm down.
 These people who say, "When you're all steamed up, all r
iled up, you should go and take a walk."
 Well, there's a reason for that, because this walking
 motion, it's something that calms you down and gets you set
 on just walking,
 and allows you to give up many, many different things.
 It's something that's actually quite difficult to do, and
 meditators who come here,
 they find it much more difficult than the sitting
 meditation in the beginning.
 But then as they practice on, sometimes they even find it
 easier than the sitting in the end,
 because it's something that develops your endurance and
 gives you a great amount of energy, a great amount of
 strength.
 It allows you to do any other thing. There's nothing that
 can be as, I guess, boring or uninteresting as just walking
 back and forth,
 so that any work that you have to do, whether it be at a
 job or at home or at school, you're able to do it much
 easier.
 Without tiring, without becoming bored, without becoming
 disinterested.
 So this is a training. Again, these things are not meant to
 be fun, they're meant to be interesting,
 they're meant to be a training that changes the mind and
 gives the mind certain qualities that it doesn't yet have,
 that it's lacking.
 The third benefit is that the food that we've eaten will be
 digested better,
 and so for this reason we often encourage meditators to do
 walking meditation after they eat,
 because of course if we just sit around all the time, then
 our digestive system is hard pressed to work properly.
 Many people don't realize this. In the world, many people,
 after eating, will lie down to sleep, especially we hear
 about people who eat dinner at 9pm, 10pm,
 and then don't do anything with it, and they lie down and
 it just sits and rots in their stomach.
 Whereas if you eat only in the morning and then you do
 walking afterwards,
 you'll find that your body is able to process it very
 quickly and make the best use of it,
 and not just let it sit in the stomach and become rotten.
 It's something that allows you to use the food to the best
 of its best use.
 And so for this reason often you find you don't need to eat
 as much when you do meditation,
 especially when you do this kind of walking and sitting
 meditation,
 because you find that your body is able to use the food
 quite efficiently,
 and you find that you don't have constipation, that you don
't have this fatigue after you eat.
 So you're able to eat quite a great amount at once and then
 it lasts you for the whole 24 hours.
 The fourth reason is because walking meditation helps to do
 away with sicknesses.
 So this of course is things like constipation, but this is
 just sort of a by-product.
 We're not here practicing because we have physical ailments
,
 although many people use that as a reason to start med
itating.
 It's not a terribly good reason, but meditation can be said
 to help the body in many ways.
 There are even, I've heard, reported cases of meditation
 helping to cure cancer,
 and I'm kind of shying away from that now because I don't
 have my own proof or anything,
 and obviously any proof would only be anecdotal.
 But the rationale is, and I think it's quite sound, is that
 meditation affects the body very much.
 It's something that loosens up the body.
 You can notice it, that the tension that you used to have
 in your shoulders or in your back is slowly done.
 It becomes very strong during meditation until finally it's
 totally done away with.
 It's like it's working at the kinks out.
 Just as when you'd have a massage, you'd find that it works
 the kinks out.
 The problem with a massage, the reason why it comes back is
 because the mind just creates the kinks again.
 As you work them out in the mind, the body becomes very,
 very, very, very healthy
 and very, very balanced and very, very much centered and
 back to its natural state.
 So I think there's nothing strange about the idea that it
 should cure many, many different sicknesses,
 of course starting with things like high blood pressure,
 maybe heart disease.
 I even had a woman who had multiple sclerosis, and I taught
 her how to do the walking meditation,
 and she did it lying on her walker back and forth, back and
 forth,
 just pushing her legs as though she were swimming.
 Until finally she could walk without the help of any...
 She was very adamant about doing it.
 So in the end she was able to walk without a cane, without
 anything, back and forth in her living room.
 So it's something that trains the body and brings the body
 back to a much more natural state.
 Of course, many things can't be cured and will not be cured
 through meditation,
 but I think it's possible that in some cases these things
 can be ameliorated or even done away with.
 But again, you can't quote me on that, it's just...
 It makes perfect sense that a great many diseases would
 have and have been proven to be cured by meditation or
 helped by meditation.
 And many other diseases should be possible to help in
 certain instances as well,
 simply because of the effect that the body has on the mind
 to take it out of that state,
 which can sometimes create things like cancer or so on.
 So these different types of cancer are created by this
 tension or are built up by this...
 Even they say cancer can be made worse by eating the wrong
 foods because it gets stuck in the stomach and rots.
 And so how walking meditation helps to get rid of that
 rotten food and digest it out,
 and expel it out quickly rather than just sitting around
 all the time and rotting in the stomach,
 which some people say helps to make cancer worse, or I've
 heard things about this.
 So it could also be used to prevent certain diseases, maybe
 even prevent these incurable diseases.
 At any rate, it has been clinically proven to help with
 things like high blood pressure, which is pretty obvious.
 And most meditators can verify that it helps with stress
 and bodily stress and aches and pains.
 The fifth reason why we practice meditation is... walking
 meditation is because walking meditation gives strong
 concentration.
 Stronger than the sitting meditation, they say. Stronger in
 the sense that it has great energy or it has great power.
 The walking meditation is like an easing into the sitting
 meditation,
 and the power that we gain from the walking, the sort of
 static charge, then carries on into the sitting meditation.
 One way of looking at this is like a segue into the sitting
. If you're just to sit down,
 it's like you don't have this chance to put the body in the
 right state, or sorry, put the mind in the right state.
 And so when you sit down, you might find that you're
 falling asleep because you have too much concentration and
 not enough effort,
 or you find yourself becoming distracted and not being able
 to focus because you have too much energy and not enough
 concentration.
 When you walk, the walking helps to balance this out.
 Because when you're sitting, if you have too much
 concentration, you'll just fall asleep.
 But when you're walking, you find that your concentration
 is balanced with energy, with effort.
 And when you feel tired, you're able to acknowledge it and
 not fall asleep.
 Also when you're distracted, because you have the freedom
 to move, and because you're moving, your mind has more to
 think about,
 and so it doesn't wander as much.
 Walking can be a great benefit to one's concentration for
 this reason.
 So these are the Buddha's words on walking meditation.
 I don't want to get into sitting meditation, but I do want
 to keep these talks short,
 so I think we'll wait until next time to talk about the
 benefits of sitting meditation.
 And again, I encourage you to give me your questions or
 suggestions for talks if you have any.
 It doesn't mean you should sit there thinking of what to
 suggest,
 but if there's something you'd like to hear about, please
 do write it either in Thai or in English, and put it in the
 box.
 So that's all for tonight.
 Now we'll continue with walking and sitting meditation.
 First mindful prostration, then walking, and then sitting.
