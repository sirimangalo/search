1
00:00:00,000 --> 00:00:06,350
 Hello, welcome back to Ask a Monk. This next question is a

2
00:00:06,350 --> 00:00:07,360
 pretty good example, I think,

3
00:00:07,360 --> 00:00:12,690
 of how not to ask questions on this forum. And that's

4
00:00:12,690 --> 00:00:18,360
 because it's specifically, well,

5
00:00:18,360 --> 00:00:23,520
 it seems quite clearly to be a presentation of one's own

6
00:00:23,520 --> 00:00:26,440
 philosophy in the form of a question,

7
00:00:26,440 --> 00:00:29,710
 because here we have a premise and a question. I'll read it

8
00:00:29,710 --> 00:00:31,280
 to you. "I want to ask about

9
00:00:31,280 --> 00:00:34,630
 nonduality. When one doesn't distinguish the two realms of

10
00:00:34,630 --> 00:00:37,320
 samsara and nirvana, then there

11
00:00:37,320 --> 00:00:41,080
 is no goal. So my question is, is wanting to transcend

12
00:00:41,080 --> 00:00:43,280
 rebirth selfish? But what you

13
00:00:43,280 --> 00:00:46,970
 have here is a complete philosophy that exists in Buddhism.

14
00:00:46,970 --> 00:00:49,480
 In Buddhism there are many philosophies

15
00:00:49,480 --> 00:00:55,410
 and they're not all compatible. This one I happen to not

16
00:00:55,410 --> 00:00:58,000
 agree with. And this is a complete

17
00:00:58,000 --> 00:01:05,060
 presentation of this philosophy. And it's clearly so

18
00:01:05,060 --> 00:01:10,080
 because the premise and the conclusion

19
00:01:10,080 --> 00:01:14,540
 that is presented in the premise, which the implicit

20
00:01:14,540 --> 00:01:17,360
 conclusion being that striving for

21
00:01:17,360 --> 00:01:19,520
 the end of rebirth is selfish, it has nothing to do, they

22
00:01:19,520 --> 00:01:20,920
 have nothing to do with each other,

23
00:01:20,920 --> 00:01:26,940
 unless except in the greater framework of this philosophy,

24
00:01:26,940 --> 00:01:29,800
 this specific philosophy.

25
00:01:29,800 --> 00:01:34,810
 So it seems clear that the purpose of this post is not to

26
00:01:34,810 --> 00:01:37,600
 ask a question, it's to present

27
00:01:37,600 --> 00:01:41,380
 a philosophy, which is not really appropriate. Now, I'm not

28
00:01:41,380 --> 00:01:42,240
 sure this is the case. It may

29
00:01:42,240 --> 00:01:46,290
 be that the person asking is really interested and really

30
00:01:46,290 --> 00:01:48,600
 thinks that I believe that these

31
00:01:48,600 --> 00:01:54,820
 two things are indistinguishable and somehow thinks that

32
00:01:54,820 --> 00:01:58,680
 that has some effect on the question

33
00:01:58,680 --> 00:02:04,170
 as to whether sending rebirth is selfish. I don't see it.

34
00:02:04,170 --> 00:02:06,480
 So briefly I'll go over this,

35
00:02:06,480 --> 00:02:10,360
 but this isn't the kind of question that I'm interested in

36
00:02:10,360 --> 00:02:11,920
 asking. I would like to be answering

37
00:02:11,920 --> 00:02:17,580
 questions about meditation and about, you know, that really

38
00:02:17,580 --> 00:02:20,080
 help you understand the Buddha's

39
00:02:20,080 --> 00:02:24,600
 teaching and understand the meditation practice and improve

40
00:02:24,600 --> 00:02:27,000
 your meditation practice and help

41
00:02:27,000 --> 00:02:31,860
 you along the path. So, but anyway, the premise, why I

42
00:02:31,860 --> 00:02:37,160
 disagree with this philosophy, the idea

43
00:02:37,160 --> 00:02:41,730
 that samsara and nirvana are indistinguishable is just a

44
00:02:41,730 --> 00:02:44,840
 sophism. It's a theory, it's a view.

45
00:02:44,840 --> 00:02:47,930
 It has something to do with reality. If you experience sams

46
00:02:47,930 --> 00:02:49,440
ara, samsara is one thing. This

47
00:02:49,440 --> 00:02:52,110
 is samsara, seeing, the hearing, the smelling, the tasting,

48
00:02:52,110 --> 00:02:53,880
 the feeling and thinking. It's

49
00:02:53,880 --> 00:02:58,050
 the arising of suffering and the pursuit of suffering.

50
00:02:58,050 --> 00:03:00,600
 Nirvana is the opposite. It's the

51
00:03:00,600 --> 00:03:06,360
 non arising of suffering, the non pursuit of suffering. And

52
00:03:06,360 --> 00:03:08,600
 the experience of it is completely

53
00:03:08,600 --> 00:03:11,260
 different. The reason why people are unable to distinguish

54
00:03:11,260 --> 00:03:12,440
 it is because they haven't

55
00:03:12,440 --> 00:03:15,910
 experienced it. They, everything they've experienced is the

56
00:03:15,910 --> 00:03:17,880
 same. They practice meditation and

57
00:03:17,880 --> 00:03:21,140
 their experiences come and go and so they relate that back

58
00:03:21,140 --> 00:03:22,800
 to samsara and it also comes

59
00:03:22,800 --> 00:03:25,050
 and goes and everything comes and goes and they say nothing

60
00:03:25,050 --> 00:03:26,180
 is different. So they're

61
00:03:26,180 --> 00:03:29,210
 unable to distinguish one from the other, which is correct

62
00:03:29,210 --> 00:03:31,640
 really because everything is indistinguishable

63
00:03:31,640 --> 00:03:36,250
 except nirvana. Everything else arises and ceases and this

64
00:03:36,250 --> 00:03:38,240
 is why the Buddha said that

65
00:03:38,240 --> 00:03:43,670
 nothing's worth clinging to because it all arises and

66
00:03:43,670 --> 00:03:48,120
 ceases. So it's basically equating

67
00:03:48,120 --> 00:03:53,560
 two opposites or confusing two opposites. It's like saying

68
00:03:53,560 --> 00:03:57,560
 that night and day are indistinguishable.

69
00:03:57,560 --> 00:04:03,260
 Light and darkness are indistinguishable, which

70
00:04:03,260 --> 00:04:08,460
 theoretically is, you can come up with a theory

71
00:04:08,460 --> 00:04:11,480
 that says that is the case, but doesn't affect the reality

72
00:04:11,480 --> 00:04:13,120
 that light is quite different

73
00:04:13,120 --> 00:04:18,340
 from darkness and that's objectively so. And nirvana and s

74
00:04:18,340 --> 00:04:21,400
amsara are objectively so as well,

75
00:04:21,400 --> 00:04:24,210
 no matter what theory or philosophy you come up with

76
00:04:24,210 --> 00:04:26,640
 because you can come up with any philosophy

77
00:04:26,640 --> 00:04:31,560
 you like and it doesn't affect, it doesn't have any effect

78
00:04:31,560 --> 00:04:33,920
 on reality. Now as to the

79
00:04:33,920 --> 00:04:39,890
 question, I'm assuming that the implication here is that

80
00:04:39,890 --> 00:04:42,920
 somehow that premise effect,

81
00:04:42,920 --> 00:04:49,600
 implies that or leads to the conclusion, supports the

82
00:04:49,600 --> 00:04:54,880
 conclusion that freedom from or the desire

83
00:04:54,880 --> 00:04:59,990
 wanting to transcend rebirth is selfish. I'm assuming that

84
00:04:59,990 --> 00:05:02,320
's what's implied here. So I

85
00:05:02,320 --> 00:05:06,180
 can talk about that. Selfishness only really comes into

86
00:05:06,180 --> 00:05:08,640
 play in Buddhism as I understand

87
00:05:08,640 --> 00:05:13,050
 it in terms of clinging. So if you cling to something and

88
00:05:13,050 --> 00:05:14,480
 you don't want someone else

89
00:05:14,480 --> 00:05:17,410
 to have it, if you ask me for something and I don't want to

90
00:05:17,410 --> 00:05:18,880
 give it to you, it's only

91
00:05:18,880 --> 00:05:23,360
 because I'm clinging to the object. There's no true selfish

92
00:05:23,360 --> 00:05:26,360
ness that arises. It's a clinging,

93
00:05:26,360 --> 00:05:29,920
 it's a function of the mind that wants an object. You don't

94
00:05:29,920 --> 00:05:31,960
 think about the other person.

95
00:05:31,960 --> 00:05:36,490
 What we call selfishness is this absorption in one's

96
00:05:36,490 --> 00:05:39,200
 attachment and only by overcoming

97
00:05:39,200 --> 00:05:42,700
 that can you possibly give up something that is a benefit

98
00:05:42,700 --> 00:05:44,520
 to you and we're able to do that

99
00:05:44,520 --> 00:05:48,000
 as we grow up. It's not because we become, well it could be

100
00:05:48,000 --> 00:05:49,160
 because we become what we

101
00:05:49,160 --> 00:05:56,620
 call more altruistic but that is not the way of the Buddha,

102
00:05:56,620 --> 00:06:00,120
 that's not the answer because

103
00:06:00,120 --> 00:06:02,780
 this idea of altruism is just another theory, it's another

104
00:06:02,780 --> 00:06:04,160
 mind game and there are people

105
00:06:04,160 --> 00:06:07,190
 who even refute this theory that say how can you possibly

106
00:06:07,190 --> 00:06:08,840
 work for the benefit of other

107
00:06:08,840 --> 00:06:14,600
 people but the Buddha's teaching doesn't go either way.

108
00:06:14,600 --> 00:06:16,880
 There's no idea that working

109
00:06:16,880 --> 00:06:20,500
 for one's benefit, another person's benefit is better than

110
00:06:20,500 --> 00:06:22,580
 working for one's own benefit.

111
00:06:22,580 --> 00:06:25,610
 You act in such a way that brings harmony. You act in such

112
00:06:25,610 --> 00:06:27,160
 a way that leads to the least

113
00:06:27,160 --> 00:06:31,670
 conflict. You act in such a way that is going to be most

114
00:06:31,670 --> 00:06:34,440
 appropriate at any given time and

115
00:06:34,440 --> 00:06:37,880
 it's not an intellectual exercise. You don't have to study

116
00:06:37,880 --> 00:06:39,400
 the books and learn what is

117
00:06:39,400 --> 00:06:44,020
 most appropriate though that can help for people who don't

118
00:06:44,020 --> 00:06:46,600
 have the experience. You

119
00:06:46,600 --> 00:06:50,290
 understand the nature of reality through your practice and

120
00:06:50,290 --> 00:06:52,120
 therefore are able to clearly

121
00:06:52,120 --> 00:06:54,830
 see what is of the greatest benefit, what is the most

122
00:06:54,830 --> 00:06:56,480
 appropriate at any given time

123
00:06:56,480 --> 00:06:58,950
 and it might be acting for your own benefit, it might be

124
00:06:58,950 --> 00:07:00,600
 acting for someone else's benefit

125
00:07:00,600 --> 00:07:06,970
 but the point is not either or, the point is the appropriat

126
00:07:06,970 --> 00:07:11,080
eness of the act. So if someone

127
00:07:11,080 --> 00:07:14,290
 comes and asks you for something many things might come

128
00:07:14,290 --> 00:07:16,640
 into play. It depends on the situation.

129
00:07:16,640 --> 00:07:19,670
 If you ask me for something I might give it to you even

130
00:07:19,670 --> 00:07:22,200
 though it might cause me suffering.

131
00:07:22,200 --> 00:07:24,530
 There may be many reasons for that. It could be because you

132
00:07:24,530 --> 00:07:25,840
've helped me before, it could

133
00:07:25,840 --> 00:07:29,840
 be because I am able to deal with suffering and I know that

134
00:07:29,840 --> 00:07:31,960
 you're not. I have practiced

135
00:07:31,960 --> 00:07:34,840
 meditation and I am able to be patient and I can see that

136
00:07:34,840 --> 00:07:36,320
 you're not, I can see that

137
00:07:36,320 --> 00:07:39,390
 I'm giving to you will lead to less suffering overall

138
00:07:39,390 --> 00:07:41,380
 because for me it's only physical

139
00:07:41,380 --> 00:07:44,220
 suffering for you, it's mental and so on. I might not give

140
00:07:44,220 --> 00:07:45,560
 you any of it because I might

141
00:07:45,560 --> 00:07:48,520
 see that you are not going to use it or I might see that if

142
00:07:48,520 --> 00:07:49,960
 you are without it you're

143
00:07:49,960 --> 00:07:54,040
 going to learn something, you're going to learn patience

144
00:07:54,040 --> 00:07:57,400
 and so on. There are many responses.

145
00:07:57,400 --> 00:07:59,830
 This is talking about an enlightened person. If a person is

146
00:07:59,830 --> 00:08:01,120
 enlightened they will act in

147
00:08:01,120 --> 00:08:05,360
 this way. They will never rise to the my benefit, your

148
00:08:05,360 --> 00:08:08,080
 benefit because there's no clinging and

149
00:08:08,080 --> 00:08:11,340
 that's the only time that selfishness arises. So the idea

150
00:08:11,340 --> 00:08:14,440
 that wanting to transcend rebirth

151
00:08:14,440 --> 00:08:19,570
 is selfish really doesn't, the idea of selfishness doesn't

152
00:08:19,570 --> 00:08:21,680
 come into play. It has nothing to

153
00:08:21,680 --> 00:08:24,080
 do with anyone else and the only way you could call it

154
00:08:24,080 --> 00:08:25,880
 selfish is if you start playing these

155
00:08:25,880 --> 00:08:30,620
 mind games and logic and it's an intellectual exercise

156
00:08:30,620 --> 00:08:33,480
 where you say well if you did stay

157
00:08:33,480 --> 00:08:37,630
 around you could help so many people and therefore it is

158
00:08:37,630 --> 00:08:40,560
 selfish to, but it's just a theory right?

159
00:08:40,560 --> 00:08:43,110
 It has nothing to do with the state of mind. The person

160
00:08:43,110 --> 00:08:44,640
 could be totally unselfish and

161
00:08:44,640 --> 00:08:47,300
 if anyone asked them for something they would help them,

162
00:08:47,300 --> 00:08:48,880
 but they don't ever think hmm why

163
00:08:48,880 --> 00:08:51,120
 I should stick around otherwise it's going to be selfish

164
00:08:51,120 --> 00:08:52,600
 because there's no clinging,

165
00:08:52,600 --> 00:08:57,010
 there's no attachment to this state or that state. The

166
00:08:57,010 --> 00:08:59,360
 person simply acts as is appropriate

167
00:08:59,360 --> 00:09:06,680
 in that instance. So the realization of freedom from

168
00:09:06,680 --> 00:09:09,360
 rebirth or the attainment of freedom

169
00:09:09,360 --> 00:09:12,280
 from rebirth is of the greatest benefit to a person. If a

170
00:09:12,280 --> 00:09:14,120
 person wants to strive for

171
00:09:14,120 --> 00:09:20,450
 that then that's a good thing, it creates benefit. If a

172
00:09:20,450 --> 00:09:23,880
 person decides to extend their,

173
00:09:23,880 --> 00:09:26,760
 or not work for the, towards the freedom from rebirth and

174
00:09:26,760 --> 00:09:28,280
 they want to be reborn again and

175
00:09:28,280 --> 00:09:32,260
 again then that's their prerogative. The Buddha never laid

176
00:09:32,260 --> 00:09:34,880
 down any you know laws of nature

177
00:09:34,880 --> 00:09:39,130
 in this regard because there are none. There is simply the

178
00:09:39,130 --> 00:09:41,640
 cause and effect. If you, if

179
00:09:41,640 --> 00:09:45,000
 a person decides that they want to extend their existence

180
00:09:45,000 --> 00:09:46,760
 and come back again and again

181
00:09:46,760 --> 00:09:48,680
 and be reborn again and again thinking that they're going

182
00:09:48,680 --> 00:09:50,480
 to help people then that's fine.

183
00:09:50,480 --> 00:09:54,910
 Or that's their choice, that's their path. There's no need

184
00:09:54,910 --> 00:09:56,800
 for any sort of judgment at

185
00:09:56,800 --> 00:10:02,760
 all. If a person decides that they want to become free from

186
00:10:02,760 --> 00:10:04,000
 suffering in this life and

187
00:10:04,000 --> 00:10:08,000
 not come back and be free from rebirth then that's their

188
00:10:08,000 --> 00:10:10,240
 path as well. There's only the

189
00:10:10,240 --> 00:10:13,090
 cause and effect so a person who is reborn again and again

190
00:10:13,090 --> 00:10:14,520
 and again will have to come

191
00:10:14,520 --> 00:10:18,370
 back again and again and suffer again and again. And you

192
00:10:18,370 --> 00:10:20,380
 know it's questionable as to

193
00:10:20,380 --> 00:10:23,900
 how much help they can possibly be given that they are not

194
00:10:23,900 --> 00:10:25,960
 yet free from clinging because

195
00:10:25,960 --> 00:10:37,200
 a person who is free from clinging would not be reborn. And

196
00:10:37,200 --> 00:10:38,600
 the person, actually the person

197
00:10:38,600 --> 00:10:44,330
 who does become free from rebirth, who does transcend

198
00:10:44,330 --> 00:10:47,640
 rebirth so to speak, is able to

199
00:10:47,640 --> 00:10:51,760
 help people because their mind is free from clinging and

200
00:10:51,760 --> 00:10:54,320
 therefore they are able to provide

201
00:10:54,320 --> 00:10:56,840
 great support and help and great insight to people. The

202
00:10:56,840 --> 00:11:03,240
 Buddha is a good example. Now

203
00:11:03,240 --> 00:11:06,990
 there is the question of whether the Buddha and enlightened

204
00:11:06,990 --> 00:11:09,020
 beings do come back and because

205
00:11:09,020 --> 00:11:11,380
 there are even philosophies that believe a person who is

206
00:11:11,380 --> 00:11:12,620
 free from clinging still is

207
00:11:12,620 --> 00:11:17,360
 reborn but that's not really the question here. Or it's

208
00:11:17,360 --> 00:11:19,240
 getting a little bit too much

209
00:11:19,240 --> 00:11:23,760
 into other people's philosophies and I don't want to create

210
00:11:23,760 --> 00:11:26,440
 more confusion than is necessary.

211
00:11:26,440 --> 00:11:31,460
 So the point here is that we work for benefit, we work for

212
00:11:31,460 --> 00:11:34,800
 welfare, we do what's appropriate

213
00:11:34,800 --> 00:11:39,130
 or we try to. This is our practice and an enlightened being

214
00:11:39,130 --> 00:11:41,040
 is someone who is perfect

215
00:11:41,040 --> 00:11:46,710
 at it, who doesn't have any thought of self or other and is

216
00:11:46,710 --> 00:11:49,480
 not subject to suffering.

217
00:11:49,480 --> 00:11:55,560
 When they pass away from this life they are free from all

218
00:11:55,560 --> 00:11:58,880
 suffering and there is no more

219
00:11:58,880 --> 00:12:06,680
 coming back, no more arising. So basically I just wanted to

220
00:12:06,680 --> 00:12:08,680
 say that this is not the

221
00:12:08,680 --> 00:12:13,590
 sort of question that I'm hoping to entertain and I think

222
00:12:13,590 --> 00:12:16,160
 probably in the future I'm just

223
00:12:16,160 --> 00:12:19,040
 going to skip over because I got a lot of questions and I'm

224
00:12:19,040 --> 00:12:20,600
 going to try to be selective.

225
00:12:20,600 --> 00:12:24,050
 I've been quite random actually in choosing which questions

226
00:12:24,050 --> 00:12:25,800
 to answer because it's difficult

227
00:12:25,800 --> 00:12:32,940
 to sort them. I've got a lot of them and Google doesn't

228
00:12:32,940 --> 00:12:36,280
 make it easy by any means. So I'm

229
00:12:36,280 --> 00:12:41,240
 going to try to skip and go directly to those ones that I

230
00:12:41,240 --> 00:12:44,200
 think are going to be useful for

231
00:12:44,200 --> 00:12:48,640
 people. So anyway thanks for tuning in, this has been

232
00:12:48,640 --> 00:12:51,960
 another episode of Ask a Monk and

233
00:12:51,960 --> 00:12:54,880
 wishing you all peace, happiness and freedom from suffering

234
00:12:54,880 --> 00:12:54,880
.

