1
00:00:00,000 --> 00:00:05,160
 Okay, so what is the difference between metta and

2
00:00:05,160 --> 00:00:06,840
 compassion?

3
00:00:06,840 --> 00:00:07,840
 Simple question.

4
00:00:07,840 --> 00:00:09,280
 It does have a simple answer.

5
00:00:09,280 --> 00:00:14,200
 I think people are not so clear on the difference between

6
00:00:14,200 --> 00:00:15,440
 these two.

7
00:00:15,440 --> 00:00:19,100
 Not realizing that they are actually different sides of the

8
00:00:19,100 --> 00:00:19,980
 same coin.

9
00:00:19,980 --> 00:00:23,320
 And there is quite a bit of over, there is some sense of

10
00:00:23,320 --> 00:00:26,400
 overlap, but they are opposites.

11
00:00:26,400 --> 00:00:27,920
 They're opposite sides of the same coin.

12
00:00:27,920 --> 00:00:32,920
 So metta, which we translate as actually friendliness.

13
00:00:32,920 --> 00:00:34,920
 Metta comes from the word "mita".

14
00:00:34,920 --> 00:00:40,210
 And then you strengthen the "i" and it becomes an "e" and

15
00:00:40,210 --> 00:00:42,160
 so you get metta.

16
00:00:42,160 --> 00:00:44,360
 Mita means friendliness.

17
00:00:44,360 --> 00:00:50,760
 What it refers to is the intention, the mind state which is

18
00:00:50,760 --> 00:00:54,400
 intent upon bringing happiness

19
00:00:54,400 --> 00:00:55,400
 to other beings.

20
00:00:55,400 --> 00:00:57,480
 Or not exactly bringing happiness.

21
00:00:57,480 --> 00:01:02,530
 intent upon a wish or a desire or an inclination because it

22
00:01:02,530 --> 00:01:05,480
 doesn't have anything to do with

23
00:01:05,480 --> 00:01:09,080
 desire or attachment, not necessarily.

24
00:01:09,080 --> 00:01:14,200
 But an inclination that beings should be happy.

25
00:01:14,200 --> 00:01:17,440
 So you have a being who is in a neutral state.

26
00:01:17,440 --> 00:01:26,760
 It is the wish, the volition, the mindset that that person

27
00:01:26,760 --> 00:01:31,220
 or people or other all beings

28
00:01:31,220 --> 00:01:33,320
 should be happy.

29
00:01:33,320 --> 00:01:36,120
 So it's a positive one.

30
00:01:36,120 --> 00:01:41,260
 Compassion, karuna, which we translate as compassion, is

31
00:01:41,260 --> 00:01:42,580
 the opposite.

32
00:01:42,580 --> 00:01:48,960
 You have a neutral person and you wish for them not to fall

33
00:01:48,960 --> 00:01:51,200
 into suffering.

34
00:01:51,200 --> 00:01:52,680
 May this person be free from suffering.

35
00:01:52,680 --> 00:01:58,290
 Or conversely you have a person who is in suffering and the

36
00:01:58,290 --> 00:02:00,640
 wish for them to be free

37
00:02:00,640 --> 00:02:01,920
 from suffering.

38
00:02:01,920 --> 00:02:08,650
 The intention, the mindset that beings should be free from

39
00:02:08,650 --> 00:02:10,320
 suffering.

40
00:02:10,320 --> 00:02:13,080
 So it doesn't actually require you to do anything.

41
00:02:13,080 --> 00:02:17,400
 Neither one of these actually requires actions.

42
00:02:17,400 --> 00:02:21,260
 It's these mindsets that will inform your actions.

43
00:02:21,260 --> 00:02:25,040
 So as a result of a desire for beings to be happy or a

44
00:02:25,040 --> 00:02:27,600
 mindset that is good for beings

45
00:02:27,600 --> 00:02:31,980
 that you wish for beings to be happy, may beings be happy.

46
00:02:31,980 --> 00:02:35,120
 As a result you will then act in such, you will act

47
00:02:35,120 --> 00:02:36,280
 accordingly.

48
00:02:36,280 --> 00:02:38,860
 So you'll do things to bring happiness and peace and

49
00:02:38,860 --> 00:02:40,720
 freedom from, happiness and peace

50
00:02:40,720 --> 00:02:42,840
 to people.

51
00:02:42,840 --> 00:02:48,610
 Now compassion, when you have this mindset it will inform

52
00:02:48,610 --> 00:02:51,200
 your actions in such a way

53
00:02:51,200 --> 00:02:53,940
 that you work to relieve suffering.

54
00:02:53,940 --> 00:02:58,860
 So that you, when someone is in pain you will immediately

55
00:02:58,860 --> 00:03:01,220
 act to try to free them from that.

56
00:03:01,220 --> 00:03:06,080
 When they are in mental anguish or despair you will incline

57
00:03:06,080 --> 00:03:08,320
 towards freeing them from

58
00:03:08,320 --> 00:03:09,320
 that.

59
00:03:09,320 --> 00:03:13,230
 So the problem is that in the end it all comes down to the

60
00:03:13,230 --> 00:03:14,400
 same thing.

61
00:03:14,400 --> 00:03:16,480
 That true happiness is freedom from suffering.

62
00:03:16,480 --> 00:03:18,560
 They are one and the same.

63
00:03:18,560 --> 00:03:25,680
 But the feeling in one's mind is different.

64
00:03:25,680 --> 00:03:29,920
 So when one has love for beings as friendliness it's a

65
00:03:29,920 --> 00:03:31,320
 positive wish.

66
00:03:31,320 --> 00:03:33,600
 When one has compassion it's a wish to take away.

67
00:03:33,600 --> 00:03:36,640
 It's a wish to take away someone's suffering.

68
00:03:36,640 --> 00:03:40,160
 But the kind of two sides of the same coin.

69
00:03:40,160 --> 00:03:42,480
 And so it's an interesting question.

70
00:03:42,480 --> 00:03:48,130
 But I think it's a clear answer that compassion is desire

71
00:03:48,130 --> 00:03:50,840
 to take away suffering.

72
00:03:50,840 --> 00:03:55,960
 It's Mita is desire to bestow happiness.

73
00:03:55,960 --> 00:03:58,320
 Thank you.

