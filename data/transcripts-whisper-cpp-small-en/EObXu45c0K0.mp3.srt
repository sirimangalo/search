1
00:00:00,000 --> 00:00:03,630
 Okay, sometimes during the meditation there are very short

2
00:00:03,630 --> 00:00:07,000
 thoughts which when noticed disappear very quickly.

3
00:00:07,000 --> 00:00:10,770
 Should one still stop and pronounce to one's self thinking

4
00:00:10,770 --> 00:00:13,000
 or just move on with the breath?

5
00:00:13,000 --> 00:00:16,110
 Absolutely stop and say to yourself thinking, "We never are

6
00:00:16,110 --> 00:00:19,560
 mindful. We cannot possibly be mindful of something as it

7
00:00:19,560 --> 00:00:21,000
 occurs."

8
00:00:21,000 --> 00:00:25,120
 The moment after it occurs is when we are mindful of it,

9
00:00:25,120 --> 00:00:27,000
 when we're aware of it.

10
00:00:27,000 --> 00:00:29,550
 So in that moment we say to ourselves thinking, and it

11
00:00:29,550 --> 00:00:33,000
 doesn't even have to be quite so technical as that.

12
00:00:33,000 --> 00:00:37,320
 Even once you know that it's disappeared, you can

13
00:00:37,320 --> 00:00:39,000
 acknowledge something to yourself.

14
00:00:39,000 --> 00:00:42,000
 You can say to yourself knowing that it's disappeared.

15
00:00:42,000 --> 00:00:44,380
 You can say to yourself disappeared, knowing that it's

16
00:00:44,380 --> 00:00:45,000
 disappeared.

17
00:00:45,000 --> 00:00:47,450
 You can say to yourself thinking, knowing that there was

18
00:00:47,450 --> 00:00:49,000
 thinking that has disappeared.

19
00:00:49,000 --> 00:00:52,830
 The Buddha said, "Somudaya dhamma nupasi viharati" or "Vaya

20
00:00:52,830 --> 00:00:55,000
 dhamma nupasi viharati"

21
00:00:55,000 --> 00:00:59,090
 He is aware of the arising of the object or he is aware of

22
00:00:59,090 --> 00:01:01,000
 the ceasing of the object.

23
00:01:01,000 --> 00:01:03,000
 Either one is considered mindfulness.

24
00:01:03,000 --> 00:01:06,450
 And why you would even bother is because if you don't

25
00:01:06,450 --> 00:01:09,000
 bother there are going to arise,

26
00:01:09,000 --> 00:01:14,190
 or there are the potential to arise, views, conceits, c

27
00:01:14,190 --> 00:01:15,000
ravings,

28
00:01:15,000 --> 00:01:18,040
 these three things that will arise at the time when you're

29
00:01:18,040 --> 00:01:20,000
 not clearly aware of the object.

30
00:01:20,000 --> 00:01:23,770
 You'll have the idea that this is my thought, or this is I

31
00:01:23,770 --> 00:01:25,000
 that is thinking.

32
00:01:25,000 --> 00:01:27,540
 You'll have the conceit that was a good thought or a bad

33
00:01:27,540 --> 00:01:32,000
 thought, or I am this thought, or so on.

34
00:01:32,000 --> 00:01:35,140
 And you'll have craving, the idea that you're liking the

35
00:01:35,140 --> 00:01:37,000
 thought or disliking the thought.

36
00:01:37,000 --> 00:01:40,000
 You'll be happy about it or upset about it.

37
00:01:40,000 --> 00:01:43,840
 So for sure, rather than, and at the very least you'll have

38
00:01:43,840 --> 00:01:45,000
 this doubt arise.

39
00:01:45,000 --> 00:01:48,000
 Should I, shouldn't I, did I do it right, or so on.

40
00:01:48,000 --> 00:01:49,640
 Just take it as a rule that if you know that you were

41
00:01:49,640 --> 00:01:52,000
 thinking at least say to yourself, knowing, knowing,

42
00:01:52,000 --> 00:01:56,520
 becoming aware of the knowing, you'll find that that helps

43
00:01:56,520 --> 00:01:58,000
 very much in the beginning

44
00:01:58,000 --> 00:02:03,570
 and eventually you're able to be more precise so that when

45
00:02:03,570 --> 00:02:05,000
 you know the thought arises

46
00:02:05,000 --> 00:02:07,630
 you catch it right away as thinking, thinking, and then it

47
00:02:07,630 --> 00:02:09,000
 disappears and that's it.

48
00:02:09,000 --> 00:02:11,590
 You only have to say thinking once, just to know that you

49
00:02:11,590 --> 00:02:12,000
're thinking,

50
00:02:12,000 --> 00:02:14,300
 or say it a couple of times to know that it's come and gone

51
00:02:14,300 --> 00:02:15,000
, and so on.

52
00:02:15,000 --> 00:02:19,670
 The idea is, as I said, that it fixes the mind and keeps

53
00:02:19,670 --> 00:02:22,000
 the mind with a clear thought.

54
00:02:22,000 --> 00:02:29,000
 The acknowledgement is only a replacement for thought.

55
00:02:29,000 --> 00:02:32,000
 It's not an addition because if you don't, as I said, if

56
00:02:32,000 --> 00:02:32,000
 you don't,

57
00:02:32,000 --> 00:02:34,000
 then they're going to raise other thoughts.

58
00:02:34,000 --> 00:02:37,560
 This is me, this is mine, this is good, this is bad, and so

59
00:02:37,560 --> 00:02:38,000
 on.

60
00:02:38,000 --> 00:02:46,000
 Yes, the more you meditate, the more the mind calms down

61
00:02:46,000 --> 00:02:53,280
 and these short millisecond thoughts are not coming up so

62
00:02:53,280 --> 00:02:55,000
 often anymore

63
00:02:55,000 --> 00:03:01,000
 so it will get much easier by the time and you will have

64
00:03:01,000 --> 00:03:04,000
 more time to note thoughts and so on

65
00:03:04,000 --> 00:03:08,000
 because there's just not so much coming up anymore.

66
00:03:08,000 --> 00:03:10,000
 Anybody?

