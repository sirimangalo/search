1
00:00:00,000 --> 00:00:10,240
 Okay, good evening everyone.

2
00:00:10,240 --> 00:00:16,240
 We are broadcasting live and the sound is going out.

3
00:00:16,240 --> 00:00:22,480
 I can almost guarantee it.

4
00:00:22,480 --> 00:00:33,390
 Tonight's quote is curious because the source doesn't have

5
00:00:33,390 --> 00:00:35,120
 this part where it says they

6
00:00:35,120 --> 00:00:38,120
 are of great help to one who has become human.

7
00:00:38,120 --> 00:00:42,690
 I'm somewhat inclined to assume that the author or the

8
00:00:42,690 --> 00:00:45,720
 translator has added that passage

9
00:00:45,720 --> 00:00:50,600
 in completely, which would be kind of shameful if he just

10
00:00:50,600 --> 00:00:53,240
 goes about adding stuff in.

11
00:00:53,240 --> 00:00:57,480
 Maybe that it exists somewhere else because it wasn't the

12
00:00:57,480 --> 00:00:59,920
 author or the translator who

13
00:00:59,920 --> 00:01:01,240
 found these quotes.

14
00:01:01,240 --> 00:01:04,520
 So the person who found these quotes is a friend of mine.

15
00:01:04,520 --> 00:01:08,440
 He was a Buddhist monk who spent a lot of time finding

16
00:01:08,440 --> 00:01:11,320
 painstakingly each of these quotes.

17
00:01:11,320 --> 00:01:16,120
 So he may have found it in the wrong place anyway.

18
00:01:16,120 --> 00:01:24,120
 Let's see if it is the right quote.

19
00:01:24,120 --> 00:01:25,640
 It's at least the four right dhammas.

20
00:01:25,640 --> 00:01:29,760
 So the Buddha says, "Chattar ume bhikkhu vay dhamma."

21
00:01:29,760 --> 00:01:35,240
 There are these four dhammas monks.

22
00:01:35,240 --> 00:01:37,960
 This was a common way for him to teach.

23
00:01:37,960 --> 00:01:42,820
 And if you think this isn't an actual thing that the Buddha

24
00:01:42,820 --> 00:01:45,280
 probably didn't teach this

25
00:01:45,280 --> 00:01:48,200
 way, who would teach this way?

26
00:01:48,200 --> 00:01:51,720
 A lot of monks do teach according to this tradition.

27
00:01:51,720 --> 00:01:52,880
 It works really well.

28
00:01:52,880 --> 00:01:55,480
 My teacher always does this.

29
00:01:55,480 --> 00:01:59,660
 He basically says, he'll talk for five minutes about four

30
00:01:59,660 --> 00:02:00,440
 things.

31
00:02:00,440 --> 00:02:03,600
 Go over one and the second.

32
00:02:03,600 --> 00:02:05,440
 Pretty much exactly how the Buddha does it here.

33
00:02:05,440 --> 00:02:11,320
 But then as the Buddha would do in other places, he says, "

34
00:02:11,320 --> 00:02:13,160
Where is this?"

35
00:02:13,160 --> 00:02:17,760
 And he talks about them in detail one by one by one.

36
00:02:17,760 --> 00:02:23,120
 Which is the way I go through these in this way as well.

37
00:02:23,120 --> 00:02:24,840
 So there are these four dhammas.

38
00:02:24,840 --> 00:02:27,080
 And this list of four dhammas.

39
00:02:27,080 --> 00:02:28,680
 What are these dhammas for?

40
00:02:28,680 --> 00:02:32,800
 Panya udiyasang bhattanti.

41
00:02:32,800 --> 00:02:37,790
 They exist or they lead to the increase in wisdom, growth

42
00:02:37,790 --> 00:02:39,000
 in wisdom.

43
00:02:39,000 --> 00:02:41,520
 They cause someone to grow in wisdom.

44
00:02:41,520 --> 00:02:43,160
 And that's all it says.

45
00:02:43,160 --> 00:02:44,640
 Katami chataru.

46
00:02:44,640 --> 00:02:46,640
 Which four?

47
00:02:46,640 --> 00:02:49,640
 Excuse me.

48
00:02:49,640 --> 00:02:54,360
 Sapurisa samsi wu.

49
00:02:54,360 --> 00:02:58,560
 Sapurisa samsi wu.

50
00:02:58,560 --> 00:03:00,520
 Association with sapurisa.

51
00:03:00,520 --> 00:03:02,040
 Good people.

52
00:03:02,040 --> 00:03:05,040
 Good fellows.

53
00:03:05,040 --> 00:03:07,920
 Sadam savanam.

54
00:03:07,920 --> 00:03:09,120
 Listening to the dhamma.

55
00:03:09,120 --> 00:03:11,560
 Listening to the good dhamma.

56
00:03:11,560 --> 00:03:17,200
 Yoni somnasi karo.

57
00:03:17,200 --> 00:03:19,280
 Wise attention.

58
00:03:19,280 --> 00:03:26,920
 Or wise mental activity.

59
00:03:26,920 --> 00:03:31,480
 Cultivating wisdom in the mind or paying wise attention.

60
00:03:31,480 --> 00:03:38,600
 Dhamma nudhamma pati pati.

61
00:03:38,600 --> 00:03:43,880
 Practicing the dhamma in order to realize the dhamma.

62
00:03:43,880 --> 00:03:49,080
 It's an interesting word.

63
00:03:49,080 --> 00:03:51,080
 Dhamma nudhamma pati pati.

64
00:03:51,080 --> 00:03:57,120
 First of all pati pati according to the tradition means,

65
00:03:57,120 --> 00:03:59,240
 and there's this one teacher in Bangkok.

66
00:03:59,240 --> 00:04:02,630
 He's passed away but he was very smart and wise and a

67
00:04:02,630 --> 00:04:05,160
 pretty awesome meditation teacher

68
00:04:05,160 --> 00:04:06,160
 too.

69
00:04:06,160 --> 00:04:11,800
 And he said, he always translated this word pati pati.

70
00:04:11,800 --> 00:04:14,880
 Pati means specifically.

71
00:04:14,880 --> 00:04:19,280
 And the first pati means specifically pati.

72
00:04:19,280 --> 00:04:22,900
 The second part means to attain.

73
00:04:22,900 --> 00:04:27,300
 So pati pati means an activity by which you are

74
00:04:27,300 --> 00:04:30,600
 specifically going for a goal.

75
00:04:30,600 --> 00:04:36,960
 So your intention is focused on a goal.

76
00:04:36,960 --> 00:04:38,840
 That's what pati pati means practice.

77
00:04:38,840 --> 00:04:39,840
 We translate it usually.

78
00:04:39,840 --> 00:04:45,660
 But it has a bit of a deeper meaning in terms of something

79
00:04:45,660 --> 00:04:49,320
 that you're doing for a reason.

80
00:04:49,320 --> 00:04:50,320
 So what are we doing?

81
00:04:50,320 --> 00:04:56,650
 We're practicing the dhamma, dhamma nudhamma, which means

82
00:04:56,650 --> 00:04:58,560
 practicing the dhamma.

83
00:04:58,560 --> 00:05:03,520
 Specific focus on dhamma.

84
00:05:03,520 --> 00:05:07,820
 So focus on the five aggregates, focus on the six senses,

85
00:05:07,820 --> 00:05:10,060
 focus on the hindrances, focus

86
00:05:10,060 --> 00:05:12,930
 on the faculties, focus on the noble truths and the sinful,

87
00:05:12,930 --> 00:05:14,280
 noble path and all these good

88
00:05:14,280 --> 00:05:16,120
 dhammas.

89
00:05:16,120 --> 00:05:17,120
 Why are we doing it?

90
00:05:17,120 --> 00:05:18,360
 What are we hoping to attain?

91
00:05:18,360 --> 00:05:20,400
 What is this pati?

92
00:05:20,400 --> 00:05:22,520
 What do we do?

93
00:05:22,520 --> 00:05:27,300
 In order to pati, in order to attain pati means we are

94
00:05:27,300 --> 00:05:30,520
 specifically practicing the dhamma.

95
00:05:30,520 --> 00:05:32,560
 Pati means to attain the dhamma.

96
00:05:32,560 --> 00:05:36,520
 It's a very interesting word, self-referential.

97
00:05:36,520 --> 00:05:41,580
 But the first dhamma, the second in terms of the

98
00:05:41,580 --> 00:05:45,880
 explanation, but the first in the word

99
00:05:45,880 --> 00:05:49,160
 refers to the truth.

100
00:05:49,160 --> 00:05:50,880
 It's the higher dhamma, the result.

101
00:05:50,880 --> 00:05:53,280
 So dhamma is in two parts.

102
00:05:53,280 --> 00:06:01,910
 There's the pubangamaga or there is the practice, the kus

103
00:06:01,910 --> 00:06:06,800
ala dhamma and then there is the vipaka

104
00:06:06,800 --> 00:06:07,800
 dhamma.

105
00:06:07,800 --> 00:06:15,690
 Or there is more than vipaka even there is the super

106
00:06:15,690 --> 00:06:18,800
 mundane result.

107
00:06:18,800 --> 00:06:22,200
 I guess which is vipaka in a sense.

108
00:06:22,200 --> 00:06:24,720
 There's the super mundane vipaka I think.

109
00:06:24,720 --> 00:06:26,720
 It's called vipaka.

110
00:06:26,720 --> 00:06:31,370
 Anyway there's the attainment of nirvana which is the pati

111
00:06:31,370 --> 00:06:36,600
 which we're trying to gain.

112
00:06:36,600 --> 00:06:39,280
 So these are the four imaiko, bhikkhu, etataro, dhamma, bh

113
00:06:39,280 --> 00:06:41,280
anya, udhyasamudanti.

114
00:06:41,280 --> 00:06:44,920
 These four dhammas lead to a growth in wisdom.

115
00:06:44,920 --> 00:06:47,560
 Pretty simple.

116
00:06:47,560 --> 00:06:49,590
 What's neat about these kind of lists is you can just

117
00:06:49,590 --> 00:06:50,960
 remember these four things and it

118
00:06:50,960 --> 00:06:53,160
 gives you a framework.

119
00:06:53,160 --> 00:06:55,040
 You've got a framework by which to practice.

120
00:06:55,040 --> 00:06:56,760
 So what's the first one?

121
00:06:56,760 --> 00:06:59,040
 Associate with good people.

122
00:06:59,040 --> 00:07:03,840
 Don't listen to people who are saying bad things, who are

123
00:07:03,840 --> 00:07:06,160
 saying harmful things that

124
00:07:06,160 --> 00:07:11,440
 lead you to your detriment.

125
00:07:11,440 --> 00:07:15,400
 It's St. Patrick's Day coming up soon.

126
00:07:15,400 --> 00:07:19,020
 And I know this because at university there is a table set

127
00:07:19,020 --> 00:07:20,960
 up by the Wellness Centre.

128
00:07:20,960 --> 00:07:23,360
 This Wellness Centre at McMaster is just awesome.

129
00:07:23,360 --> 00:07:25,720
 I should go visit them again.

130
00:07:25,720 --> 00:07:28,440
 They're coming to our peace symposium but they're just neat

131
00:07:28,440 --> 00:07:29,040
 people.

132
00:07:29,040 --> 00:07:32,230
 So they had this table set up and I stopped to ask what's

133
00:07:32,230 --> 00:07:33,360
 this all about?

134
00:07:33,360 --> 00:07:37,920
 It was all alcohol paraphernalia and stuff.

135
00:07:37,920 --> 00:07:45,210
 The person manning it said to me, "We're trying to talk to

136
00:07:45,210 --> 00:07:49,480
 people about drinking alcohol."

137
00:07:49,480 --> 00:07:52,840
 She said, at first she was sort of beating it around, but

138
00:07:52,840 --> 00:07:55,120
 she said, "We're trying to

139
00:07:55,120 --> 00:07:57,960
 teach them how to consume alcohol in moderation."

140
00:07:57,960 --> 00:08:01,110
 She said kind of apologetically, "Because we can't tell

141
00:08:01,110 --> 00:08:02,840
 them not to drink alcohol."

142
00:08:02,840 --> 00:08:04,600
 And I wanted to ask her, "Do you drink alcohol?"

143
00:08:04,600 --> 00:08:08,520
 Because from the sounds of it she probably didn't.

144
00:08:08,520 --> 00:08:11,860
 There are lots of people out there who just don't drink

145
00:08:11,860 --> 00:08:14,120
 alcohol for cultural or religious

146
00:08:14,120 --> 00:08:19,190
 or just reasons from their upbringing, which is pretty

147
00:08:19,190 --> 00:08:20,320
 awesome.

148
00:08:20,320 --> 00:08:24,200
 I wasn't one of those people.

149
00:08:24,200 --> 00:08:25,200
 I was on the other side.

150
00:08:25,200 --> 00:08:28,620
 I was trying to convince my roommate and all my friends to

151
00:08:28,620 --> 00:08:32,120
 drink with me and do drugs and

152
00:08:32,120 --> 00:08:33,120
 all that stuff.

153
00:08:33,120 --> 00:08:39,680
 Very bad karma.

154
00:08:39,680 --> 00:08:41,720
 So yeah, don't hang out with people like me.

155
00:08:41,720 --> 00:08:45,040
 I'm the kind of person I was.

156
00:08:45,040 --> 00:08:49,840
 I think I'm sufficiently or substantially changed.

157
00:08:49,840 --> 00:08:55,000
 I think some people would still not consider that.

158
00:08:55,000 --> 00:08:56,000
 Consider me a...

159
00:08:56,000 --> 00:08:58,440
 Well, there's... takes all kinds, right?

160
00:08:58,440 --> 00:09:01,870
 But hang out with people and listen to people and talk with

161
00:09:01,870 --> 00:09:03,880
 people and practice with people

162
00:09:03,880 --> 00:09:06,480
 who you consider to be good fellows.

163
00:09:06,480 --> 00:09:12,850
 Sāpurīsa means people who are on a good path, people who

164
00:09:12,850 --> 00:09:19,560
 appreciate and encourage and work

165
00:09:19,560 --> 00:09:23,040
 towards good things.

166
00:09:23,040 --> 00:09:26,320
 Hang out with those people, associate with them.

167
00:09:26,320 --> 00:09:29,260
 This leads to an increase in wisdom because you're going to

168
00:09:29,260 --> 00:09:30,720
 hear wise things and because

169
00:09:30,720 --> 00:09:34,620
 you're going to practice good things and that's going to

170
00:09:34,620 --> 00:09:36,840
 make you wiser and make you... lead

171
00:09:36,840 --> 00:09:43,200
 you to better understanding, better wisdom.

172
00:09:43,200 --> 00:09:44,200
 Number one.

173
00:09:44,200 --> 00:09:45,200
 Number two, sadhamma savanam.

174
00:09:45,200 --> 00:09:47,280
 So don't just hang out with such people.

175
00:09:47,280 --> 00:09:48,760
 Listen to what they have to say.

176
00:09:48,760 --> 00:09:52,720
 Listen to the Buddha's teaching or read the Buddha's

177
00:09:52,720 --> 00:09:53,760
 teaching.

178
00:09:53,760 --> 00:09:58,440
 Listen to talks on the Dhamma and so on.

179
00:09:58,440 --> 00:10:01,320
 Of course, this leads to wisdom.

180
00:10:01,320 --> 00:10:05,070
 Not only does it lead to information, which is called sutta

181
00:10:05,070 --> 00:10:07,040
 mayapanya, it also leads you

182
00:10:07,040 --> 00:10:08,400
 to think in different ways.

183
00:10:08,400 --> 00:10:10,080
 So it leads to jinta mayapanya.

184
00:10:10,080 --> 00:10:12,740
 You start to think about things you never thought about

185
00:10:12,740 --> 00:10:13,360
 before.

186
00:10:13,360 --> 00:10:16,040
 You say, "Oh yeah, it's like that.

187
00:10:16,040 --> 00:10:18,360
 Never thought of that before."

188
00:10:18,360 --> 00:10:22,400
 And then finally, of course, it leads you to change the way

189
00:10:22,400 --> 00:10:24,200
 you see the world, which is

190
00:10:24,200 --> 00:10:25,200
 bhavana mayapanya.

191
00:10:25,200 --> 00:10:29,610
 It leads you to practice meditation and just in general

192
00:10:29,610 --> 00:10:32,760
 leads you to see things differently.

193
00:10:32,760 --> 00:10:34,400
 It's to a different outlook.

194
00:10:34,400 --> 00:10:38,320
 It leads to a real visceral change in the way you

195
00:10:38,320 --> 00:10:41,800
 understand the world and that's bhavana

196
00:10:41,800 --> 00:10:45,080
 mayapanya.

197
00:10:45,080 --> 00:10:47,880
 So saddhamma sabhanam, listening to the Dhamma, that's a

198
00:10:47,880 --> 00:10:49,560
 good way to cultivate wisdom.

199
00:10:49,560 --> 00:10:50,960
 Number two.

200
00:10:50,960 --> 00:10:54,200
 Number three, yoni soma nasi karo.

201
00:10:54,200 --> 00:10:56,000
 Yoni soma nasi karo, I think.

202
00:10:56,000 --> 00:10:57,320
 How does he translate it here?

203
00:10:57,320 --> 00:11:00,120
 I don't think I'm happy with his translation.

204
00:11:00,120 --> 00:11:02,480
 Oh no, why is attention?

205
00:11:02,480 --> 00:11:03,480
 That's good.

206
00:11:03,480 --> 00:11:04,480
 Yeah.

207
00:11:04,480 --> 00:11:05,480
 So why is attention?

208
00:11:05,480 --> 00:11:07,280
 Yoni, so I've talked about this before.

209
00:11:07,280 --> 00:11:09,280
 Yoni means to the womb.

210
00:11:09,280 --> 00:11:10,280
 Boom.

211
00:11:10,280 --> 00:11:14,960
 Yoni is womb, like a woman's womb where the baby is born.

212
00:11:14,960 --> 00:11:20,910
 But what it means is getting back to the root, back to the

213
00:11:20,910 --> 00:11:22,120
 source.

214
00:11:22,120 --> 00:11:24,600
 Yoni, it can also just mean source.

215
00:11:24,600 --> 00:11:27,280
 So yoni soma means to the source.

216
00:11:27,280 --> 00:11:32,400
 And it's just an idiomatic way of saying with wisdom.

217
00:11:32,400 --> 00:11:34,800
 But in the context of meditation, it's quite interesting.

218
00:11:34,800 --> 00:11:37,760
 So yoni soma means to the source.

219
00:11:37,760 --> 00:11:39,120
 Manasi means in the mind.

220
00:11:39,120 --> 00:11:46,000
 Kara means to make or to hold, to make, to form.

221
00:11:46,000 --> 00:11:50,920
 Kara is just karma, it just means action.

222
00:11:50,920 --> 00:11:56,370
 So to act in such a way or to make something be in the mind

223
00:11:56,370 --> 00:11:59,640
 to the source, which is very,

224
00:11:59,640 --> 00:12:02,970
 very interesting from the point of view of insight

225
00:12:02,970 --> 00:12:05,200
 meditation, where we do this, right?

226
00:12:05,200 --> 00:12:09,800
 When you focus on pain and you say pain, you're keeping

227
00:12:09,800 --> 00:12:12,960
 that thing in mind or you're establishing

228
00:12:12,960 --> 00:12:17,680
 it in your mind at the source.

229
00:12:17,680 --> 00:12:19,560
 So only the source.

230
00:12:19,560 --> 00:12:23,710
 You're not worried about is it good pain, bad pain, problem

231
00:12:23,710 --> 00:12:25,080
 pain, my pain?

232
00:12:25,080 --> 00:12:26,320
 Did you cause this pain?

233
00:12:26,320 --> 00:12:28,160
 That kind of thing.

234
00:12:28,160 --> 00:12:29,560
 All we know is the source.

235
00:12:29,560 --> 00:12:34,940
 We get to the source and we see it just as pain, which of

236
00:12:34,940 --> 00:12:38,320
 course is described elsewhere.

237
00:12:38,320 --> 00:12:40,280
 But this word itself is very interesting.

238
00:12:40,280 --> 00:12:42,640
 So proper attention.

239
00:12:42,640 --> 00:12:44,930
 It can also refer to just proper attention when you're

240
00:12:44,930 --> 00:12:46,200
 listening to the Dhamma.

241
00:12:46,200 --> 00:12:49,640
 I mean, it has worldly definitions as well.

242
00:12:49,640 --> 00:12:51,650
 Like when you're listening to the Dhamma, you should pay

243
00:12:51,650 --> 00:12:52,240
 attention.

244
00:12:52,240 --> 00:12:53,640
 Keep it in mind with wisdom.

245
00:12:53,640 --> 00:12:55,840
 Reflect wisely on what's being said.

246
00:12:55,840 --> 00:12:58,120
 A lot of people interpret it that way.

247
00:12:58,120 --> 00:13:01,360
 But if you look at the texts, that's not enough.

248
00:13:01,360 --> 00:13:05,440
 Yoni Somana Sikara has to be a meditative state.

249
00:13:05,440 --> 00:13:10,440
 But you can apply it thinking wisely, listening wisely.

250
00:13:10,440 --> 00:13:14,520
 Paying attention can be thought of just as that.

251
00:13:14,520 --> 00:13:17,200
 So it does relate back to the other one.

252
00:13:17,200 --> 00:13:22,520
 But more importantly, you should pay attention to the Dham

253
00:13:22,520 --> 00:13:25,520
ma, the teachings, meaning the

254
00:13:25,520 --> 00:13:27,840
 realities that are being taught about.

255
00:13:27,840 --> 00:13:32,600
 You should pay attention to those realities.

256
00:13:32,600 --> 00:13:37,560
 Put the teaching to work.

257
00:13:37,560 --> 00:13:40,970
 But of course, you could also say, well, that's fine with Y

258
00:13:40,970 --> 00:13:42,280
oni Somana Sikara.

259
00:13:42,280 --> 00:13:44,400
 Just pay attention when you're listening to the Dhamma.

260
00:13:44,400 --> 00:13:47,560
 But number four is where you actually practice.

261
00:13:47,560 --> 00:13:49,360
 We've already talked about what this one means.

262
00:13:49,360 --> 00:13:52,960
 Dhamma, nudhamma, pati pati.

263
00:13:52,960 --> 00:13:59,640
 It's practicing specifically the Dhamma in order to attain

264
00:13:59,640 --> 00:14:02,920
 specifically the Dhamma.

265
00:14:02,920 --> 00:14:07,830
 So practicing four foundations of mindfulness and so on in

266
00:14:07,830 --> 00:14:11,040
 order to attain nirvana, supermundane

267
00:14:11,040 --> 00:14:12,040
 dhammas.

268
00:14:12,040 --> 00:14:16,720
 So this is a teaching.

269
00:14:16,720 --> 00:14:18,760
 These four lead to wisdom.

270
00:14:18,760 --> 00:14:20,320
 Wisdom has to come from practice.

271
00:14:20,320 --> 00:14:23,200
 You can't have just the first three unless you count the

272
00:14:23,200 --> 00:14:24,720
 third one as meditation.

273
00:14:24,720 --> 00:14:27,810
 But you can't just associate with good people and listen to

274
00:14:27,810 --> 00:14:28,720
 good things.

275
00:14:28,720 --> 00:14:32,420
 If you're not paying attention and practicing the Dhamma,

276
00:14:32,420 --> 00:14:33,880
 wisdom won't arise.

277
00:14:33,880 --> 00:14:35,040
 But these four altogether.

278
00:14:35,040 --> 00:14:38,100
 On the other hand, if you just practice without listening,

279
00:14:38,100 --> 00:14:39,800
 it's very easy to get on the wrong

280
00:14:39,800 --> 00:14:40,800
 path.

281
00:14:40,800 --> 00:14:44,580
 And many people, a huge part of what I do is just

282
00:14:44,580 --> 00:14:47,120
 correcting people's practice.

283
00:14:47,120 --> 00:14:50,470
 Most people, well, all people who practice, until you

284
00:14:50,470 --> 00:14:52,680
 become enlightened, you're doing

285
00:14:52,680 --> 00:14:54,440
 it wrong.

286
00:14:54,440 --> 00:14:56,840
 You're not doing it perfectly.

287
00:14:56,840 --> 00:14:58,400
 And that's an important point.

288
00:14:58,400 --> 00:15:01,160
 It's not just, "If I do this moral, become enlightened."

289
00:15:01,160 --> 00:15:02,160
 That's not the case.

290
00:15:02,160 --> 00:15:04,760
 That's not how enlightenment is based on wisdom.

291
00:15:04,760 --> 00:15:07,120
 It's not based on work.

292
00:15:07,120 --> 00:15:09,280
 It's not effort.

293
00:15:09,280 --> 00:15:13,220
 Of course it takes effort to change your view, but it's not

294
00:15:13,220 --> 00:15:14,360
 just effort.

295
00:15:14,360 --> 00:15:15,600
 You have to change your view.

296
00:15:15,600 --> 00:15:17,200
 You have to look at things differently.

297
00:15:17,200 --> 00:15:21,120
 This is actually where Yoni Somanasi-kara comes in.

298
00:15:21,120 --> 00:15:26,920
 And wisdom actually has to take over in the practice.

299
00:15:26,920 --> 00:15:28,880
 You have to be clever and wise.

300
00:15:28,880 --> 00:15:32,260
 In this word, "vimangsa," that I talk about often, it's not

301
00:15:32,260 --> 00:15:33,600
 enough to just keep your

302
00:15:33,600 --> 00:15:35,480
 attention on something.

303
00:15:35,480 --> 00:15:38,560
 You have to be discriminating and discerning and saying, "I

304
00:15:38,560 --> 00:15:39,760
'm doing this wrong.

305
00:15:39,760 --> 00:15:41,600
 I'm not paying attention to that.

306
00:15:41,600 --> 00:15:45,200
 Oh, I'm not really being mindful here."

307
00:15:45,200 --> 00:15:49,840
 And meditation is a constant adapting, refining.

308
00:15:49,840 --> 00:15:51,120
 That's what it's really all about.

309
00:15:51,120 --> 00:15:53,160
 And that's what leads to success.

310
00:15:53,160 --> 00:15:56,710
 Not just plowing through it and saying, "I did so many

311
00:15:56,710 --> 00:15:57,800
 hours a day."

312
00:15:57,800 --> 00:16:00,850
 You can do lots and lots of hours of walking, sitting and

313
00:16:00,850 --> 00:16:02,520
 getting nothing out of it.

314
00:16:02,520 --> 00:16:03,520
 That's possible.

315
00:16:03,520 --> 00:16:07,840
 If you're not doing it right, you have to constantly...

316
00:16:07,840 --> 00:16:10,200
 And I can't tell you how to do it.

317
00:16:10,200 --> 00:16:12,180
 The teachings are very simple, but you have to look and you

318
00:16:12,180 --> 00:16:13,200
 have to say, "Am I doing it

319
00:16:13,200 --> 00:16:14,200
 the way it said?"

320
00:16:14,200 --> 00:16:16,240
 You have to go back to the teachings and say, "Oh, wait.

321
00:16:16,240 --> 00:16:17,720
 I'm not actually doing that."

322
00:16:17,720 --> 00:16:20,120
 And you need a teacher who...

323
00:16:20,120 --> 00:16:22,440
 It's good to have a teacher who can tell you.

324
00:16:22,440 --> 00:16:26,920
 You have to adjust that.

325
00:16:26,920 --> 00:16:30,780
 You need people, good people, and you need to listen to the

326
00:16:30,780 --> 00:16:31,560
 Dhamma.

327
00:16:31,560 --> 00:16:33,480
 You have to keep it in mind.

328
00:16:33,480 --> 00:16:36,120
 You have to practice it.

329
00:16:36,120 --> 00:16:39,960
 It's basically what's being said here.

330
00:16:39,960 --> 00:16:44,400
 That's the Dhamma for tonight.

331
00:16:44,400 --> 00:16:51,490
 I'm going to copy-paste the Hangout if anybody wants to

332
00:16:51,490 --> 00:16:56,280
 come on and ask questions.

333
00:16:56,280 --> 00:17:02,280
 Do that for a few minutes to see if anybody has questions.

334
00:17:02,280 --> 00:17:05,440
 If there are, we'll stay on.

335
00:17:05,440 --> 00:17:11,950
 If there aren't, we'll consider that our Dhamma for tonight

336
00:17:11,950 --> 00:17:12,440
.

337
00:17:12,440 --> 00:17:38,600
 All right.

338
00:17:38,600 --> 00:17:43,720
 Looks like there are no questioners.

339
00:17:43,720 --> 00:17:50,720
 I'm just going to say goodnight then.

340
00:17:50,720 --> 00:18:06,720
 See you all tomorrow.

