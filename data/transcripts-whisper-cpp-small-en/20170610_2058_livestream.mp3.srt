1
00:00:00,000 --> 00:00:15,080
 Okay, good evening everyone.

2
00:00:15,080 --> 00:00:27,480
 Welcome to our evening Dhamma session.

3
00:00:27,480 --> 00:00:30,480
 Last night we talked about having one good night.

4
00:00:30,480 --> 00:00:37,920
 The night I thought we'd talk about one exceptionally good

5
00:00:37,920 --> 00:00:38,480
 night,

6
00:00:38,480 --> 00:00:47,480
 which is the night the Buddha became enlightened.

7
00:00:47,480 --> 00:00:55,480
 Both for simply how inspiring it is to think of the Buddha,

8
00:00:55,480 --> 00:01:03,480
 and to think of how we're following in his footsteps.

9
00:01:03,480 --> 00:01:09,390
 And because of how instructive it is, what the Buddha's

10
00:01:09,390 --> 00:01:14,480
 experience teaches us.

11
00:01:14,480 --> 00:01:23,480
 So a little bit of background for those who don't know.

12
00:01:23,480 --> 00:01:29,830
 When the Buddha was born in India, or what was at the time

13
00:01:29,830 --> 00:01:32,480
 the Indus Valley.

14
00:01:32,480 --> 00:01:37,480
 It's now modern Nepal actually.

15
00:01:37,480 --> 00:01:43,480
 About 2500 years ago.

16
00:01:43,480 --> 00:01:47,480
 So the world was quite different back then.

17
00:01:47,480 --> 00:01:57,480
 And he wandered around on foot throughout the Indus Valley.

18
00:01:57,480 --> 00:02:01,850
 He left home at 29 years of age after living a life of

19
00:02:01,850 --> 00:02:02,480
 luxury,

20
00:02:02,480 --> 00:02:06,670
 and then wandered around India until he finally decided to

21
00:02:06,670 --> 00:02:09,480
 just spend his time

22
00:02:09,480 --> 00:02:13,480
 pushing his body to its limit.

23
00:02:13,480 --> 00:02:16,680
 And purposefully torturing himself in order to burn away

24
00:02:16,680 --> 00:02:19,480
 defilements didn't work.

25
00:02:19,480 --> 00:02:26,910
 And then after six years of that, he gave up torturing

26
00:02:26,910 --> 00:02:27,480
 himself

27
00:02:27,480 --> 00:02:33,560
 and he went back to eating solid food and taking better

28
00:02:33,560 --> 00:02:34,480
 care of himself

29
00:02:34,480 --> 00:02:39,480
 and practicing meditation.

30
00:02:39,480 --> 00:02:43,770
 Living a balanced life, and this is why he came to talk

31
00:02:43,770 --> 00:02:45,480
 about the middle way.

32
00:02:45,480 --> 00:02:50,150
 Because the ascetics who were practicing with him abandoned

33
00:02:50,150 --> 00:02:50,480
 him.

34
00:02:50,480 --> 00:02:54,470
 They said, "Oh, you've given up the holy life of torturing

35
00:02:54,470 --> 00:02:55,480
 yourself."

36
00:02:55,480 --> 00:03:03,480
 So they gave him up.

37
00:03:03,480 --> 00:03:06,010
 I don't think the Buddha was disturbed by that, because at

38
00:03:06,010 --> 00:03:06,480
 this point

39
00:03:06,480 --> 00:03:09,480
 he was pretty set on his own meditation.

40
00:03:09,480 --> 00:03:13,030
 And I imagine them leaving helped in the end because it

41
00:03:13,030 --> 00:03:14,480
 gave him focus

42
00:03:14,480 --> 00:03:24,480
 and allowed him to focus on his own practice.

43
00:03:24,480 --> 00:03:33,120
 And he went and sat under the Bodhi tree, the tree that was

44
00:03:33,120 --> 00:03:33,480
 to become,

45
00:03:33,480 --> 00:03:37,680
 it's a very big, it's even there today, or a descendant of

46
00:03:37,680 --> 00:03:38,480
 the original,

47
00:03:38,480 --> 00:03:47,590
 they say, is still there to this day in the forest in Bodhg

48
00:03:47,590 --> 00:03:48,480
aya.

49
00:03:48,480 --> 00:03:53,480
 And he sat under this tree and spent the night there.

50
00:03:53,480 --> 00:03:55,480
 So this is his one good night.

51
00:03:55,480 --> 00:03:57,480
 Didn't sleep.

52
00:03:57,480 --> 00:04:01,150
 He said to himself, he wasn't going to get up from that

53
00:04:01,150 --> 00:04:01,480
 seat

54
00:04:01,480 --> 00:04:04,480
 until he became enlightened.

55
00:04:04,480 --> 00:04:13,480
 And there are many stories about his time under the tree.

56
00:04:13,480 --> 00:04:20,110
 Of course, the most flowery one that many people have heard

57
00:04:20,110 --> 00:04:20,480
 about

58
00:04:20,480 --> 00:04:25,480
 or read about or watched in various depictions

59
00:04:25,480 --> 00:04:32,480
 is where Mara comes down with all of his armies

60
00:04:32,480 --> 00:04:36,480
 and attacks the Buddha and confronts the Buddha

61
00:04:36,480 --> 00:04:39,220
 and says to the Buddha, you don't deserve to sit under the

62
00:04:39,220 --> 00:04:39,480
 tree of enlightenment

63
00:04:39,480 --> 00:04:43,480
 when you're doing, thinking you can become enlightenment.

64
00:04:43,480 --> 00:04:49,480
 And the Buddha reflected back on all of his perfections

65
00:04:49,480 --> 00:04:54,480
 and all of the work he had done, time after lifetime,

66
00:04:54,480 --> 00:04:59,480
 to become a Buddha.

67
00:04:59,480 --> 00:05:06,480
 And he put his hand, and all right, so the story goes that

68
00:05:06,480 --> 00:05:16,480
 Mara says, all of these people back me, all of his armies,

69
00:05:16,480 --> 00:05:23,870
 many demons, the idea is it's just a representation of all

70
00:05:23,870 --> 00:05:24,480
 evil.

71
00:05:24,480 --> 00:05:28,480
 Mara is in this legend, it's just the armies of Mara

72
00:05:28,480 --> 00:05:31,480
 are actually defilements in the mind.

73
00:05:31,480 --> 00:05:34,480
 The daughters of Mara that come and tempt the Buddha,

74
00:05:34,480 --> 00:05:36,480
 the daughters of Mara are just defilements.

75
00:05:36,480 --> 00:05:40,480
 They're just attachment, addiction and so on.

76
00:05:40,480 --> 00:05:43,480
 But Mara says, all of these people are my witness.

77
00:05:43,480 --> 00:05:46,480
 You're not worthy of being here. Who is your witness?

78
00:05:46,480 --> 00:05:48,480
 And the Buddha looked around and he had no witness

79
00:05:48,480 --> 00:05:51,480
 and so he put his hand on the earth and he said,

80
00:05:51,480 --> 00:05:53,480
 the earth is my witness.

81
00:05:53,480 --> 00:05:57,350
 As the earth is my witness, I am worthy to become

82
00:05:57,350 --> 00:05:58,480
 enlightened.

83
00:05:58,480 --> 00:06:02,480
 And the earth shook and sent forth water that swept away

84
00:06:02,480 --> 00:06:04,480
 the armies of Mara.

85
00:06:04,480 --> 00:06:07,780
 I get the feeling that it was embellished over time to get

86
00:06:07,780 --> 00:06:08,480
 to that state,

87
00:06:08,480 --> 00:06:12,470
 that the original idea was that the Buddha was assailed by

88
00:06:12,470 --> 00:06:13,480
 defilements

89
00:06:13,480 --> 00:06:18,480
 as we all are when we practice.

90
00:06:18,480 --> 00:06:23,480
 Meditation centers are often hotbeds of defilement

91
00:06:23,480 --> 00:06:28,550
 because we're letting down our guards and we're living in

92
00:06:28,550 --> 00:06:29,480
 close quarters.

93
00:06:29,480 --> 00:06:32,480
 It's quite amazing that all six of us can live here

94
00:06:32,480 --> 00:06:36,480
 without tearing each other's heads off, right?

95
00:06:36,480 --> 00:06:40,020
 Imagine a hundred people living in a meditation center and

96
00:06:40,020 --> 00:06:41,480
 long term,

97
00:06:41,480 --> 00:06:46,480
 fights do break out from time to time.

98
00:06:46,480 --> 00:06:49,480
 These are the armies of Mara.

99
00:06:49,480 --> 00:06:53,480
 It's the armies of Mara that cause conflict.

100
00:06:53,480 --> 00:06:57,850
 So with them gone, once the Buddha's mind was purified of

101
00:06:57,850 --> 00:07:00,480
 the defilements,

102
00:07:00,480 --> 00:07:02,480
 and understand that when your mind,

103
00:07:02,480 --> 00:07:05,630
 just because your mind is purified from defilements, doesn

104
00:07:05,630 --> 00:07:06,480
't mean you're enlightened.

105
00:07:06,480 --> 00:07:08,480
 You can have a very...

106
00:07:08,480 --> 00:07:10,480
 And this is why the texts are quite clear.

107
00:07:10,480 --> 00:07:13,860
 If you talk about enlightenment as being freedom from def

108
00:07:13,860 --> 00:07:15,480
ilements,

109
00:07:15,480 --> 00:07:19,290
 you can't quite say that because it's possible for a medit

110
00:07:19,290 --> 00:07:19,480
ator to think

111
00:07:19,480 --> 00:07:27,480
 they have no defilements because they don't at that time.

112
00:07:27,480 --> 00:07:32,710
 Once the Buddha's mind was pure and he was sitting in

113
00:07:32,710 --> 00:07:38,480
 states of clarity and bliss and calm,

114
00:07:38,480 --> 00:07:41,480
 then he started to take stock of...

115
00:07:41,480 --> 00:07:45,250
 And he finally was in a position to understand the universe

116
00:07:45,250 --> 00:07:47,480
, to understand the truth.

117
00:07:47,480 --> 00:07:49,480
 And so he started by looking at the universe.

118
00:07:49,480 --> 00:07:54,890
 Well, he started by looking at himself actually, looking at

119
00:07:54,890 --> 00:07:56,480
 the past.

120
00:07:56,480 --> 00:07:59,480
 He remembered all the way back to his birth, recollecting,

121
00:07:59,480 --> 00:08:01,480
 reflecting on his life,

122
00:08:01,480 --> 00:08:06,480
 the indulgence, the self-torture.

123
00:08:06,480 --> 00:08:10,680
 And then he broke through, went through back through the

124
00:08:10,680 --> 00:08:11,480
 six years of torture,

125
00:08:11,480 --> 00:08:14,480
 back through the 29 years of self-indulgment,

126
00:08:14,480 --> 00:08:19,060
 all the way back to when he was six years old, six months

127
00:08:19,060 --> 00:08:19,480
 old.

128
00:08:19,480 --> 00:08:22,480
 How many months old? I don't remember.

129
00:08:22,480 --> 00:08:26,180
 Sitting and lying under a tree and he entered into the jh

130
00:08:26,180 --> 00:08:28,480
ana when he was just a baby.

131
00:08:28,480 --> 00:08:34,880
 Back to when he was five days old and the ascetic asita

132
00:08:34,880 --> 00:08:38,480
 came and proclaimed that he would be a Buddha.

133
00:08:38,480 --> 00:08:42,820
 Back to when he was born, the day he was born in Lungbini

134
00:08:42,820 --> 00:08:43,480
 garden.

135
00:08:43,480 --> 00:08:49,500
 And legend says he actually took seven steps and proclaimed

136
00:08:49,500 --> 00:08:52,480
 himself to be a future Buddha.

137
00:08:52,480 --> 00:08:56,450
 And he went back before that day into his mother's womb and

138
00:08:56,450 --> 00:08:58,480
 back into his past lives.

139
00:08:58,480 --> 00:09:01,680
 The point is, he went, "This is if you want to remember

140
00:09:01,680 --> 00:09:02,480
 your past lives.

141
00:09:02,480 --> 00:09:04,480
 You need to remember like that.

142
00:09:04,480 --> 00:09:07,630
 Enter into high states of tranquility and then send your

143
00:09:07,630 --> 00:09:09,480
 mind back and back and back."

144
00:09:09,480 --> 00:09:12,480
 We don't teach that here.

145
00:09:12,480 --> 00:09:16,070
 But now I've just taught it so someday if you ever want to

146
00:09:16,070 --> 00:09:18,480
 try it when you're alone and you have time.

147
00:09:18,480 --> 00:09:22,850
 I know a man who actually tried it once and said he was

148
00:09:22,850 --> 00:09:25,480
 able to remember some things.

149
00:09:25,480 --> 00:09:27,680
 And so the Buddha remembered all the way back. He

150
00:09:27,680 --> 00:09:30,480
 remembered all of his past lives.

151
00:09:30,480 --> 00:09:34,340
 Not all of them, of course, is infinite, but he remembered

152
00:09:34,340 --> 00:09:35,480
 as far back as he wished.

153
00:09:35,480 --> 00:09:40,470
 He remembered things and imagined the history of that were

154
00:09:40,470 --> 00:09:42,480
 actually possible.

155
00:09:42,480 --> 00:09:45,900
 Imagine what you could learn if you could remember not just

156
00:09:45,900 --> 00:09:48,480
 this life, but lifetime after lifetime

157
00:09:48,480 --> 00:09:55,480
 and really get an incredible sense of cause and effect.

158
00:09:55,480 --> 00:09:59,160
 We don't have that luxury. We can read about the Buddha's

159
00:09:59,160 --> 00:10:00,480
 past life stories.

160
00:10:00,480 --> 00:10:07,080
 Luckily, 500 or so of them have been written down and ret

161
00:10:07,080 --> 00:10:07,480
old,

162
00:10:07,480 --> 00:10:13,110
 perhaps with a little embellishment, but interesting anyway

163
00:10:13,110 --> 00:10:13,480
.

164
00:10:13,480 --> 00:10:17,480
 What the Buddha was able to actually remember.

165
00:10:17,480 --> 00:10:22,700
 There are modern day gurus who write books about these

166
00:10:22,700 --> 00:10:23,480
 things

167
00:10:23,480 --> 00:10:26,620
 and claim to be able to hypnotize people into remembering

168
00:10:26,620 --> 00:10:27,480
 past lives.

169
00:10:27,480 --> 00:10:31,140
 It's interesting to read. You might want to take it with a

170
00:10:31,140 --> 00:10:31,480
 grain of salt.

171
00:10:31,480 --> 00:10:36,610
 Certainly an interesting thing to explore, the idea of past

172
00:10:36,610 --> 00:10:37,480
 lives.

173
00:10:37,480 --> 00:10:39,480
 The real problem is, of course, it's in the past.

174
00:10:39,480 --> 00:10:43,240
 And as we talked about last night, it ends up being not

175
00:10:43,240 --> 00:10:45,480
 intrinsically beneficial.

176
00:10:45,480 --> 00:10:49,610
 It's quite easy to get lost in the past, caught up by it

177
00:10:49,610 --> 00:10:51,480
 and distracted by it.

178
00:10:51,480 --> 00:10:55,480
 So he moved on. I mean, that was interesting and useful.

179
00:10:55,480 --> 00:11:02,680
 But a part of his is coming to be a Buddha, understanding

180
00:11:02,680 --> 00:11:05,480
 this about rebirth.

181
00:11:05,480 --> 00:11:10,480
 He moved on and he started to consider other beings.

182
00:11:10,480 --> 00:11:14,480
 And he entered into, or he started to see through,

183
00:11:14,480 --> 00:11:18,480
 I mean, this is all through quite powerful states of mind.

184
00:11:18,480 --> 00:11:23,030
 He was able to apparently see beings arising and passing

185
00:11:23,030 --> 00:11:23,480
 away.

186
00:11:23,480 --> 00:11:25,480
 So being born and dying.

187
00:11:25,480 --> 00:11:30,480
 He was able to send his mind out and to watch,

188
00:11:30,480 --> 00:11:37,470
 to monitor the minds of others and watch them die and watch

189
00:11:37,470 --> 00:11:40,480
 them be reborn.

190
00:11:40,480 --> 00:11:46,090
 By knowing their minds, he could watch them pass away from

191
00:11:46,090 --> 00:11:46,480
 here and die there.

192
00:11:46,480 --> 00:11:54,110
 So he was able to see the, on a broader sense, the nature

193
00:11:54,110 --> 00:11:55,480
 of cause and effect.

194
00:11:55,480 --> 00:11:58,480
 Simply remembering his own past lives was interesting.

195
00:11:58,480 --> 00:12:01,650
 But watching all the many people and watching the

196
00:12:01,650 --> 00:12:02,480
 tendencies,

197
00:12:02,480 --> 00:12:06,060
 this is what gave him this understanding of karma, cause

198
00:12:06,060 --> 00:12:07,480
 and effect.

199
00:12:07,480 --> 00:12:09,480
 Again, it's very much all about cause and effect.

200
00:12:09,480 --> 00:12:13,300
 And this is why the Buddha would all the time talk about

201
00:12:13,300 --> 00:12:14,480
 cause and effect

202
00:12:14,480 --> 00:12:19,990
 and why the Four Noble Truths are framed in terms of cause

203
00:12:19,990 --> 00:12:21,480
 and effect.

204
00:12:21,480 --> 00:12:24,480
 And then finally in the third watch of the night,

205
00:12:24,480 --> 00:12:26,480
 so this was three different things that happened.

206
00:12:26,480 --> 00:12:30,490
 The third thing that happened is he started to look closer

207
00:12:30,490 --> 00:12:33,480
 at the general picture,

208
00:12:33,480 --> 00:12:35,480
 or the bigger picture.

209
00:12:35,480 --> 00:12:38,380
 And so this is where he came up with what we call dependent

210
00:12:38,380 --> 00:12:39,480
 origination

211
00:12:39,480 --> 00:12:49,560
 or paticca samuppada, where he saw that in general it's

212
00:12:49,560 --> 00:12:50,480
 people's cravings,

213
00:12:50,480 --> 00:12:53,480
 people's desires that lead them to be reborn again.

214
00:12:53,480 --> 00:12:57,770
 People want something and then they die wanting that and

215
00:12:57,770 --> 00:12:59,480
 they go on in that way

216
00:12:59,480 --> 00:13:01,480
 or they're attached to something.

217
00:13:01,480 --> 00:13:05,480
 Maybe they're afraid of something or averse to that thing,

218
00:13:05,480 --> 00:13:14,440
 but that's a power, that's the tension, the fuel that leads

219
00:13:14,440 --> 00:13:17,480
 to rebirth.

220
00:13:17,480 --> 00:13:22,020
 And he saw that it was because they thought those things

221
00:13:22,020 --> 00:13:24,480
 were going to bring them happiness and peace.

222
00:13:24,480 --> 00:13:28,050
 But he also saw that none of these people ever found

223
00:13:28,050 --> 00:13:29,480
 happiness or peace.

224
00:13:29,480 --> 00:13:34,480
 And he realized that it was ignorance, that this was wrong,

225
00:13:34,480 --> 00:13:36,480
 that this was where the problem was.

226
00:13:36,480 --> 00:13:39,810
 The problem lay in thinking, that wanting things or

227
00:13:39,810 --> 00:13:42,480
 clinging to things,

228
00:13:42,480 --> 00:13:48,480
 either positive clinging, negative clinging, that was the

229
00:13:48,480 --> 00:13:49,480
 problem.

230
00:13:49,480 --> 00:13:52,480
 So that's where he came up with the Four Noble Truths,

231
00:13:52,480 --> 00:13:54,480
 that craving is the cause of suffering,

232
00:13:54,480 --> 00:13:58,480
 thirst is the cause of suffering.

233
00:13:58,480 --> 00:14:00,480
 And he came up with paticca samuppada,

234
00:14:00,480 --> 00:14:04,910
 this very important teaching that I often talk about, which

235
00:14:04,910 --> 00:14:06,480
 is avijja-pachaya-sankhara.

236
00:14:06,480 --> 00:14:15,540
 Sankhara are all of our ambitions, you might say, so karma

237
00:14:15,540 --> 00:14:16,480
 really,

238
00:14:16,480 --> 00:14:18,740
 when you want to do something, do something good, do

239
00:14:18,740 --> 00:14:20,480
 something evil,

240
00:14:20,480 --> 00:14:27,480
 but all of our ambitions are caused by ignorance.

241
00:14:27,480 --> 00:14:34,170
 You realize that there's no ambition, there's no

242
00:14:34,170 --> 00:14:37,480
 inclination,

243
00:14:37,480 --> 00:14:45,110
 not inclination, but no desire that can possibly satisfy

244
00:14:45,110 --> 00:14:45,480
 you,

245
00:14:45,480 --> 00:14:49,480
 or lead to satisfaction.

246
00:14:49,480 --> 00:14:51,480
 And you realize that that's all it took,

247
00:14:51,480 --> 00:14:54,480
 that once you got rid of that ignorance,

248
00:14:54,480 --> 00:14:57,480
 once you got rid of your ignorance about reality,

249
00:14:57,480 --> 00:15:01,050
 thinking that there was something you could find that was

250
00:15:01,050 --> 00:15:01,480
 stable,

251
00:15:01,480 --> 00:15:07,600
 satisfying, controllable, me, mine, going to hold on to

252
00:15:07,600 --> 00:15:08,480
 this,

253
00:15:08,480 --> 00:15:19,480
 once you give that up, you stop suffering, you're free.

254
00:15:19,480 --> 00:15:23,480
 And I guess technically how it worked is,

255
00:15:23,480 --> 00:15:29,090
 I mean not guess, but how it works is, you see, this

256
00:15:29,090 --> 00:15:30,480
 ignorance,

257
00:15:30,480 --> 00:15:33,690
 how the Buddha came to understand this is because he got

258
00:15:33,690 --> 00:15:35,480
 rid of his ignorance.

259
00:15:35,480 --> 00:15:40,480
 He came to see, "Hey, these things that I'm clinging to,

260
00:15:40,480 --> 00:15:45,620
 these things are not stable, not satisfying, not controll

261
00:15:45,620 --> 00:15:46,480
able."

262
00:15:46,480 --> 00:15:48,480
 Which is what you're all doing here, right?

263
00:15:48,480 --> 00:15:51,480
 Every moment when you see the things that you cling to,

264
00:15:51,480 --> 00:15:54,480
 this is good, this is bad, this is right, this is wrong,

265
00:15:54,480 --> 00:15:57,480
 this is me, this is mine,

266
00:15:57,480 --> 00:16:04,480
 all of our judgments, when you see how stressful they are,

267
00:16:04,480 --> 00:16:07,480
 when you see that they're not really you,

268
00:16:07,480 --> 00:16:11,480
 you're not the one in control of them,

269
00:16:11,480 --> 00:16:17,480
 they're unwieldy, uncontrollable, unpredictable,

270
00:16:18,480 --> 00:16:22,480
 you're the one who's going to go of them.

271
00:16:22,480 --> 00:16:29,480
 And that knowledge that over-dispel is the ignorance,

272
00:16:29,480 --> 00:16:36,480
 that's enough to free you from all suffering.

273
00:16:36,480 --> 00:16:42,480
 The Buddha had a really good night.

274
00:16:42,480 --> 00:16:45,480
 And this is our leader, right?

275
00:16:45,480 --> 00:16:48,170
 Many of us call ourselves Buddhists, it doesn't really

276
00:16:48,170 --> 00:16:48,480
 matter,

277
00:16:48,480 --> 00:16:53,480
 but whether you just think of the Buddha as a great guy,

278
00:16:53,480 --> 00:16:56,480
 or whether you'd really take the Buddha as your refuge,

279
00:16:56,480 --> 00:17:02,610
 in the sense that you really feel that his teachings are

280
00:17:02,610 --> 00:17:04,480
 something to dedicate your life to,

281
00:17:04,480 --> 00:17:10,480
 it doesn't really matter, but nonetheless, this is the guy,

282
00:17:10,480 --> 00:17:16,480
 this is our point of reference,

283
00:17:16,480 --> 00:17:20,630
 and it's quite inspiring to think of, right, because like

284
00:17:20,630 --> 00:17:23,480
 other religions have their person,

285
00:17:23,480 --> 00:17:33,680
 Jesus, Mohammed, Hindus have Krishna, and we have the

286
00:17:33,680 --> 00:17:34,480
 Buddha,

287
00:17:34,480 --> 00:17:41,480
 and this is the story. But also, the Buddha's enlightenment

288
00:17:41,480 --> 00:17:42,480
 is the enlightenment,

289
00:17:42,480 --> 00:17:45,480
 in many ways, the enlightenment that we're striving for,

290
00:17:45,480 --> 00:17:48,480
 not completely.

291
00:17:48,480 --> 00:17:51,680
 We may never remember our past lives or be able to see

292
00:17:51,680 --> 00:17:53,480
 other people being born and dying,

293
00:17:53,480 --> 00:17:58,420
 but we sure can understand the cause of suffering and free

294
00:17:58,420 --> 00:18:00,480
 ourselves from it,

295
00:18:00,480 --> 00:18:04,480
 through understanding suffering.

296
00:18:04,480 --> 00:18:09,480
 So there you go, a little bit of dharma for tonight.

297
00:18:09,480 --> 00:18:25,480
 Thank you all for coming out.

298
00:18:25,480 --> 00:18:54,480
 I have a few questions here.

299
00:18:54,480 --> 00:19:17,480
 [silence]

300
00:19:17,480 --> 00:19:21,120
 Can you be totally free from attachments if you still want

301
00:19:21,120 --> 00:19:27,480
 to eat enough to live and have the intention to meditate?

302
00:19:27,480 --> 00:19:32,370
 Want to eat enough to live? Probably not, because that's

303
00:19:32,370 --> 00:19:35,480
 kind of an attachment.

304
00:19:35,480 --> 00:19:39,480
 So an enlightened being doesn't have that want, which means

305
00:19:39,480 --> 00:19:39,480
,

306
00:19:39,480 --> 00:19:43,480
 I remember talking to someone who's gone to the end, right,

307
00:19:43,480 --> 00:19:47,480
 because enlightenment is sort of a gradual thing.

308
00:19:47,480 --> 00:19:50,480
 Once you've seen nibbana, you still have attachments.

309
00:19:50,480 --> 00:19:53,250
 It's just they're quite weakened because there's no wrong

310
00:19:53,250 --> 00:19:54,480
 view to support them.

311
00:19:54,480 --> 00:19:58,890
 Wrong view is gone. So you don't think it's right that you

312
00:19:58,890 --> 00:20:00,480
 want to eat and want to live,

313
00:20:00,480 --> 00:20:04,480
 but you still do. You still haven't worked it all out.

314
00:20:04,480 --> 00:20:10,400
 Because you don't think it's right, it'll eventually peter

315
00:20:10,400 --> 00:20:12,480
 out and fade away.

316
00:20:12,480 --> 00:20:16,130
 But once you become completely enlightened, then in many

317
00:20:16,130 --> 00:20:16,480
 cases, yes,

318
00:20:16,480 --> 00:20:19,310
 there is just a starving to death because you really aren't

319
00:20:19,310 --> 00:20:20,480
 concerned with it.

320
00:20:20,480 --> 00:20:23,480
 I mean, this was a thing in India already.

321
00:20:23,480 --> 00:20:27,480
 Starving to death is kind of a religious thing in India,

322
00:20:27,480 --> 00:20:30,480
 because before the Buddha they were thinking,

323
00:20:30,480 --> 00:20:34,480
 "This is a way to free yourself from attachment."

324
00:20:34,480 --> 00:20:37,760
 But because they still had the attachment to food, it was

325
00:20:37,760 --> 00:20:40,480
 just a forcing, and that doesn't work.

326
00:20:40,480 --> 00:20:45,480
 But it's interesting, horrific for most people, I think,

327
00:20:45,480 --> 00:20:48,480
 how could you possibly do such a thing?

328
00:20:48,480 --> 00:20:51,480
 Quite interesting from a philosophical point of view.

329
00:20:51,480 --> 00:20:54,980
 I mean, if you're free from suffering, why would you worry

330
00:20:54,980 --> 00:21:04,480
 about eating to live?

331
00:21:04,480 --> 00:21:07,550
 These strong emotions are dominating during my formal

332
00:21:07,550 --> 00:21:08,480
 meditation.

333
00:21:08,480 --> 00:21:11,480
 Should I stay with them until they go away?

334
00:21:11,480 --> 00:21:14,480
 As I'm doing now, the cost of doing much less walking

335
00:21:14,480 --> 00:21:14,480
 during a session,

336
00:21:14,480 --> 00:21:16,480
 or ignore them and get back to focusing.

337
00:21:16,480 --> 00:21:22,480
 If they don't go away, you can ignore them.

338
00:21:22,480 --> 00:21:25,770
 But it's often good to change postures if they're really

339
00:21:25,770 --> 00:21:26,480
 strong.

340
00:21:26,480 --> 00:21:28,480
 I mean, it's something you generally just have to...

341
00:21:28,480 --> 00:21:31,480
 There's no answer. I don't have a solution for you.

342
00:21:31,480 --> 00:21:34,480
 So don't expect me to be able to fix that for you.

343
00:21:34,480 --> 00:21:36,480
 It's something you have to be patient with.

344
00:21:36,480 --> 00:21:40,480
 It's great that you're seeing them and learning about them.

345
00:21:40,480 --> 00:21:44,980
 Just take your time to learn more about them and understand

346
00:21:44,980 --> 00:21:47,480
 them better.

347
00:21:47,480 --> 00:21:50,480
 So, I mean, in the end, it's not going to matter so much

348
00:21:50,480 --> 00:21:50,480
 what you do.

349
00:21:50,480 --> 00:21:54,480
 Just try to be flexible, and over time they will recede.

350
00:21:54,480 --> 00:21:57,480
 They'll become more manageable.

351
00:21:57,480 --> 00:22:00,720
 Because a lot of those kind of things have to do with the

352
00:22:00,720 --> 00:22:02,480
 time before we were mindful,

353
00:22:02,480 --> 00:22:05,480
 before we were practicing meditation.

354
00:22:05,480 --> 00:22:09,480
 Just don't feed them.

355
00:22:09,480 --> 00:22:11,480
 Would you say mindfulness is the same?

356
00:22:11,480 --> 00:22:14,480
 I've already given a talk on mindfulness.

357
00:22:14,480 --> 00:22:19,480
 So I'm not going to answer that because I want you to...

358
00:22:19,480 --> 00:22:22,980
 I gave my answer to what mindfulness is, so I'm not going

359
00:22:22,980 --> 00:22:23,480
 to say mindfulness.

360
00:22:23,480 --> 00:22:26,480
 Because you're the second person who's asked this recently.

361
00:22:26,480 --> 00:22:29,760
 It makes me wonder whether I actually explained it well

362
00:22:29,760 --> 00:22:30,480
 enough.

363
00:22:30,480 --> 00:22:32,480
 Maybe I didn't.

364
00:22:32,480 --> 00:22:34,770
 But I hope that you go and read that, and I hope it does

365
00:22:34,770 --> 00:22:35,480
 explain it.

366
00:22:35,480 --> 00:22:39,280
 If not, you could read my booklet because that should give

367
00:22:39,280 --> 00:22:40,480
 you some idea.

368
00:22:40,480 --> 00:22:43,480
 Hopefully.

369
00:22:43,480 --> 00:22:51,480
 It feels like there's no excuse to not drop everything.

370
00:22:51,480 --> 00:22:53,480
 No excuse to drop.

371
00:22:53,480 --> 00:22:56,480
 No excuse to not drop everything and follow a monk life.

372
00:22:56,480 --> 00:22:58,480
 However, this feels wrong somehow.

373
00:22:58,480 --> 00:23:01,480
 Is this something you work towards as a result of craving,

374
00:23:01,480 --> 00:23:05,480
 or is this sense of extremism an wholesome?

375
00:23:05,480 --> 00:23:09,480
 Thumb up, check, remove circle.

376
00:23:09,480 --> 00:23:22,200
 It's a bit vague what you're saying, but I'm assuming that

377
00:23:22,200 --> 00:23:23,480
 you mean...

378
00:23:23,480 --> 00:23:30,560
 I'm assuming what's going on here is this conflicting sense

379
00:23:30,560 --> 00:23:32,480
 of what's right.

380
00:23:32,480 --> 00:23:34,950
 Because of course it's probably, I would assume it's fairly

381
00:23:34,950 --> 00:23:35,480
 new to you,

382
00:23:35,480 --> 00:23:38,480
 this idea that becoming a monk is the right thing to do.

383
00:23:38,480 --> 00:23:42,200
 And what's been ingrained into your mind is that the right

384
00:23:42,200 --> 00:23:42,480
 thing to do

385
00:23:42,480 --> 00:23:45,480
 is be a responsible member of society.

386
00:23:45,480 --> 00:23:48,690
 So it's just a question of when you can let go of that

387
00:23:48,690 --> 00:23:49,480
 wrong view,

388
00:23:49,480 --> 00:23:52,480
 or wrong perception.

389
00:23:52,480 --> 00:23:54,480
 Because there's no reason to support society.

390
00:23:54,480 --> 00:23:58,480
 Society is just a bunch of people running around doing

391
00:23:58,480 --> 00:24:02,480
 meaningless things.

392
00:24:02,480 --> 00:24:05,480
 Well, maybe not entirely meaningless.

393
00:24:05,480 --> 00:24:10,480
 But in the end, I mean, in the end it's all meaningless.

394
00:24:10,480 --> 00:24:14,630
 There's no, I mean, meaningless in the sense that there's

395
00:24:14,630 --> 00:24:18,480
 no reason to support it, right?

396
00:24:18,480 --> 00:24:22,040
 The only reason one could have is in terms of helping each

397
00:24:22,040 --> 00:24:22,480
 other,

398
00:24:22,480 --> 00:24:26,480
 in terms of helping each other be free from suffering,

399
00:24:26,480 --> 00:24:32,480
 which is a fairly big and complicated goal.

400
00:24:32,480 --> 00:24:35,260
 So I mean, that's the sense that I think society is

401
00:24:35,260 --> 00:24:36,480
 important, useful.

402
00:24:36,480 --> 00:24:40,560
 We have Buddhist society, monastic society, and it's

403
00:24:40,560 --> 00:24:42,480
 important.

404
00:24:47,480 --> 00:24:50,480
 But to that end, I mean, what helps people the most,

405
00:24:50,480 --> 00:24:52,480
 what helps you or anyone else the most?

406
00:24:52,480 --> 00:24:56,480
 It's obviously becoming enlightened.

407
00:24:56,480 --> 00:24:58,480
 I mean, there should be no conflict here.

408
00:24:58,480 --> 00:25:02,190
 It's just that most likely you're conflicted as a non-Budd

409
00:25:02,190 --> 00:25:04,480
hist become Buddhist.

410
00:25:04,480 --> 00:25:07,150
 And I mean, it's interesting how even in Asia that's the

411
00:25:07,150 --> 00:25:07,480
 case.

412
00:25:07,480 --> 00:25:11,480
 People who grew up as "Buddhists" have the same conflict

413
00:25:11,480 --> 00:25:13,480
 because they're also taught the wrong way.

414
00:25:13,480 --> 00:25:18,480
 They're also taught that the way to do the right thing

415
00:25:18,480 --> 00:25:22,480
 is to make lots of money and be successful

416
00:25:22,480 --> 00:25:26,480
 and maybe support your parents as well.

417
00:25:26,480 --> 00:25:30,020
 But become a monk? No, that's not responsible. That's

418
00:25:30,020 --> 00:25:31,480
 irresponsible.

419
00:25:31,480 --> 00:25:39,480
 Hopefully you get over that.

420
00:25:40,480 --> 00:25:43,480
 Okay, that's the demo for tonight.

421
00:25:43,480 --> 00:25:46,480
 Thank you all for coming out.

422
00:25:46,480 --> 00:25:48,480
 Thank you.

423
00:25:50,480 --> 00:25:52,480
 Thank you.

