1
00:00:00,000 --> 00:00:09,750
 Some people are born with all these good qualities from day

2
00:00:09,750 --> 00:00:14,000
 one. Is that purely due to their samsaric training?

3
00:00:14,000 --> 00:00:20,020
 I think it's going to be a mixture because they'll be

4
00:00:20,020 --> 00:00:26,030
 influenced by their past karma, but then what's currently

5
00:00:26,030 --> 00:00:29,000
 happening is going to influence them too.

6
00:00:29,000 --> 00:00:37,190
 So, a person could be seen as what makes it hard if you say

7
00:00:37,190 --> 00:00:45,360
 day one because you don't know how a person is from day one

8
00:00:45,360 --> 00:00:46,350
. You're not going to know how a person is until they're

9
00:00:46,350 --> 00:00:46,650
 older and have been raised by their parents, by their

10
00:00:46,650 --> 00:00:48,600
 teachers, by their friends, by all these different factors

11
00:00:48,600 --> 00:00:50,000
 that come into play.

12
00:00:50,000 --> 00:00:56,640
 So it's going to be a mix of what is currently coming in

13
00:00:56,640 --> 00:01:02,470
 now in the past. And this formulates whole new things. That

14
00:01:02,470 --> 00:01:08,260
's what keeps the becoming happening over and over again is

15
00:01:08,260 --> 00:01:11,830
 that they're constantly feeding all these new things into

16
00:01:11,830 --> 00:01:12,000
 it.

17
00:01:12,000 --> 00:01:15,510
 Just like with the thinking question. Formulating all these

18
00:01:15,510 --> 00:01:18,450
 different thoughts and ideas and opinions about this or

19
00:01:18,450 --> 00:01:23,270
 that. You're constantly feeding something new to arise. So

20
00:01:23,270 --> 00:01:27,000
 I think it's going to be a mix.

21
00:01:27,000 --> 00:01:34,270
 Buddhism is not deterministic. This person wasn't like that

22
00:01:34,270 --> 00:01:38,790
 from day one. They did develop those qualities throughout

23
00:01:38,790 --> 00:01:40,000
 their life.

24
00:01:40,000 --> 00:01:44,150
 Another thing I wanted to say just quickly is day one is

25
00:01:44,150 --> 00:01:47,840
 actually a bit of a, is not early enough actually because

26
00:01:47,840 --> 00:01:51,000
 they're already developing in the womb.

27
00:01:51,000 --> 00:01:57,960
 There was an interesting study done about babies in the

28
00:01:57,960 --> 00:02:03,360
 womb that when they came out, when they were first born,

29
00:02:03,360 --> 00:02:08,590
 they would cry in the accent of their parents because they

30
00:02:08,590 --> 00:02:09,000
 had heard this sound,

31
00:02:09,000 --> 00:02:11,830
 the voice of their mother, and they would cry in that

32
00:02:11,830 --> 00:02:15,340
 accent, according to that accent. And the study went on and

33
00:02:15,340 --> 00:02:18,990
 on about the various things, you know, based on the foods

34
00:02:18,990 --> 00:02:21,680
 that the parents ate while they were in the womb, based on

35
00:02:21,680 --> 00:02:27,000
 the stress or relaxation of the parents and so on.

36
00:02:27,000 --> 00:02:38,640
 The other thing I wanted to say, I don't know if Balanyani

37
00:02:38,640 --> 00:02:40,150
 has anything you can add after, is that karma, I don't

38
00:02:40,150 --> 00:02:42,000
 think we should understand karma to be something magical.

39
00:02:42,000 --> 00:02:44,770
 Like you kill someone and then in the next life you're

40
00:02:44,770 --> 00:02:47,850
 going to be killed. It's pretty clear that the Buddha was,

41
00:02:47,850 --> 00:02:50,000
 the Buddha tried to point this out as well.

42
00:02:50,000 --> 00:02:55,350
 He said sometimes a person can do a very simple negative

43
00:02:55,350 --> 00:02:59,930
 act and go to hell for it. Someone can do a lot of horrible

44
00:02:59,930 --> 00:03:02,000
 deeds and not go to hell for it.

45
00:03:02,000 --> 00:03:05,140
 The Buddha said, "I see this. I see how some people do a

46
00:03:05,140 --> 00:03:08,050
 lot of bad deeds and still in the end possible that they

47
00:03:08,050 --> 00:03:10,000
 could even wind up in heaven."

48
00:03:10,000 --> 00:03:13,700
 And a person could do a very trifling evil deed and still

49
00:03:13,700 --> 00:03:15,000
 wind up in hell.

50
00:03:15,000 --> 00:03:17,710
 Part of this is because of course because of the nature of

51
00:03:17,710 --> 00:03:21,020
 how the mind takes it. If someone does a lot of bad deeds

52
00:03:21,020 --> 00:03:25,000
 and then is remorseful and sees the danger in it, or the

53
00:03:25,000 --> 00:03:30,000
 harm in what they've done, and strives to change themselves

54
00:03:30,000 --> 00:03:32,680
, they can overcome it and they can end up becoming a good

55
00:03:32,680 --> 00:03:40,000
 person and actually blocking out the karma or reducing it

56
00:03:40,000 --> 00:03:40,000
 or so on.

57
00:03:40,000 --> 00:03:43,380
 Of course this is a speculative, this is a controversial

58
00:03:43,380 --> 00:03:47,000
 subject of whether you can actually cut off karma or so on.

59
00:03:47,000 --> 00:03:51,430
 But the other part of it is that first of all karma is only

60
00:03:51,430 --> 00:03:55,000
 one of the forces at work in the universe.

61
00:03:55,000 --> 00:04:00,640
 And second of all, karma is natural. It's a part of nature.

62
00:04:00,640 --> 00:04:05,710
 It only talks about the inclination or the tendency or the

63
00:04:05,710 --> 00:04:11,000
 effect, the addition that an act will put into the pot.

64
00:04:11,000 --> 00:04:15,550
 But when you have all these different factors at play, this

65
00:04:15,550 --> 00:04:19,350
 one act may not appear to have a negative consequence at

66
00:04:19,350 --> 00:04:20,000
 all.

67
00:04:20,000 --> 00:04:23,150
 It's like if you put salt into a dish, it still may turn

68
00:04:23,150 --> 00:04:26,580
 out that the dish doesn't taste salty at all. Depending on

69
00:04:26,580 --> 00:04:29,050
 what else is in the dish and when you taste it, you may not

70
00:04:29,050 --> 00:04:30,000
 notice the salt.

71
00:04:30,000 --> 00:04:33,800
 It may not be the first thing that you notice. There may be

72
00:04:33,800 --> 00:04:37,000
 so much sugar that it actually tastes sweet.

73
00:04:37,000 --> 00:04:39,480
 And it may be that there's so much water that you can't

74
00:04:39,480 --> 00:04:43,270
 actually taste the salt. However, the many different

75
00:04:43,270 --> 00:04:46,090
 factors, it can also happen that the salt reacts with

76
00:04:46,090 --> 00:04:49,000
 something and turns into something totally different.

77
00:04:49,000 --> 00:04:53,920
 So, I mean, obviously, the obvious answer to your question

78
00:04:53,920 --> 00:04:58,490
 is that yes, that person's probably done some pretty good

79
00:04:58,490 --> 00:05:05,000
 things to be born, you know, a genius or an angel or so on.

80
00:05:05,000 --> 00:05:08,810
 Why some children are angelic, some other children are ter

81
00:05:08,810 --> 00:05:12,640
rors and seems to have nothing to do with their upbringing,

82
00:05:12,640 --> 00:05:16,590
 nothing to do with their parents, even their genetics or so

83
00:05:16,590 --> 00:05:17,000
 on.

84
00:05:17,000 --> 00:05:21,420
 In certain cases, why different children act. You know,

85
00:05:21,420 --> 00:05:22,840
 some children come out of the womb and aren't crying at all

86
00:05:22,840 --> 00:05:26,540
. Some of them cry all night. Some cry when they get in the

87
00:05:26,540 --> 00:05:27,000
 car.

88
00:05:27,000 --> 00:05:30,060
 My brother and I were like this. My older brother, the only

89
00:05:30,060 --> 00:05:32,700
 way they could put him to sleep was to get him in the car

90
00:05:32,700 --> 00:05:34,000
 and drive him around.

91
00:05:34,000 --> 00:05:36,850
 As soon as I got in the car, I would start wailing and

92
00:05:36,850 --> 00:05:40,220
 crying and I hated to be in the car. Totally different. My

93
00:05:40,220 --> 00:05:44,000
 older brother, two years separated.

94
00:05:44,000 --> 00:05:50,270
 This kind of thing surely has to do with some karmic or

95
00:05:50,270 --> 00:05:56,060
 some inherent qualities they call vasana. Vasana means

96
00:05:56,060 --> 00:06:00,440
 those things that you carry with you or that you bring from

97
00:06:00,440 --> 00:06:02,000
 one life to the next.

98
00:06:02,000 --> 00:06:06,240
 But it doesn't have to be totally that way. Things can

99
00:06:06,240 --> 00:06:10,850
 twist and turn in all sorts of ways. It's just the way

100
00:06:10,850 --> 00:06:14,890
 physics works or how science works. It happens quite

101
00:06:14,890 --> 00:06:16,000
 scientifically.

102
00:06:16,000 --> 00:06:18,560
 This is why the Buddha said karma is something that you

103
00:06:18,560 --> 00:06:21,490
 really can't understand. Only a Buddha could possibly

104
00:06:21,490 --> 00:06:24,520
 understand and know what deed leads to what result and what

105
00:06:24,520 --> 00:06:27,000
 has led this person to be like this and so on.

106
00:06:27,000 --> 00:06:32,030
 Very difficult to see. An example is how difficult it is to

107
00:06:32,030 --> 00:06:37,010
 predict the weather seven days in advance. Just so many

108
00:06:37,010 --> 00:06:40,000
 different factors to consider.

109
00:06:40,000 --> 00:06:49,560
 But the simple answer is yeah, it most likely has to do

110
00:06:49,560 --> 00:06:59,000
 with their samsaric training, but not entirely. As Nagasena

111
00:06:59,000 --> 00:06:59,000
 pointed out quite correctly, people can take lemons and

112
00:06:59,000 --> 00:06:59,000
 make lemonade. People with bad lives can turn out to be

113
00:06:59,000 --> 00:06:59,000
 great people.

114
00:06:59,000 --> 00:07:03,000
 And people with great lives can turn out to be bad people.

115
00:07:03,000 --> 00:07:09,990
 People can change. There's an example as the taimya jataka.

116
00:07:09,990 --> 00:07:14,680
 Taimya came from hell and was born a prince in the same

117
00:07:14,680 --> 00:07:18,000
 kingdom where he had been a king.

118
00:07:18,000 --> 00:07:22,270
 And so he had been a terrible person before. He had gone to

119
00:07:22,270 --> 00:07:26,960
 hell. But as a result of that, he realized that he had to

120
00:07:26,960 --> 00:07:31,610
 do something to change himself or else he was going to grow

121
00:07:31,610 --> 00:07:36,000
 up and be the king again and start killing people.

122
00:07:36,000 --> 00:07:38,920
 So he was sitting on his king's lap when he was like what,

123
00:07:38,920 --> 00:07:42,390
 six months old or three months old? And the king, they

124
00:07:42,390 --> 00:07:46,570
 brought these prisoners to the king and the king said chop

125
00:07:46,570 --> 00:07:50,820
 off that man's head, give this man ten lashes and excommun

126
00:07:50,820 --> 00:07:53,000
icate this man or whatever.

127
00:07:53,000 --> 00:07:56,340
 So take all his money, throw him in the dungeon, torture

128
00:07:56,340 --> 00:08:03,620
 him. And this baby, who is the bodhisatta, became quite

129
00:08:03,620 --> 00:08:05,000
 scared.

130
00:08:05,000 --> 00:08:10,650
 There was an angel there in the king's white umbrella and

131
00:08:10,650 --> 00:08:16,090
 the angel told him that it was his fate or something to

132
00:08:16,090 --> 00:08:18,000
 become the king.

133
00:08:18,000 --> 00:08:22,900
 And he got totally scared and the angel said, "But if you

134
00:08:22,900 --> 00:08:27,530
 pretend to be deaf and if you pretend to be dumb, they won

135
00:08:27,530 --> 00:08:30,860
't make you king though. They'll cast you away and you won't

136
00:08:30,860 --> 00:08:33,180
 have to become a king and then you won't have to do all

137
00:08:33,180 --> 00:08:34,000
 sorts of bad deeds and go to hell."

138
00:08:34,000 --> 00:08:38,970
 And so thinking about hell, it was like he was still young

139
00:08:38,970 --> 00:08:44,450
 enough to remember it. He hadn't gone through the horrors

140
00:08:44,450 --> 00:08:50,000
 of being a young child and forgetting about all that.

141
00:08:50,000 --> 00:08:57,140
 And so he took this to heart and from that point on, if you

142
00:08:57,140 --> 00:08:59,550
 read the Tami Ajataka, it's wonderful to hear that they

143
00:08:59,550 --> 00:09:00,000
 thought, "This baby doesn't cry out."

144
00:09:00,000 --> 00:09:05,000
 "Well, is there something wrong with him? Maybe he's mute."

145
00:09:05,000 --> 00:09:09,490
 And so they put lots of sweets around. And the bodhisatta

146
00:09:09,490 --> 00:09:13,030
 thought to himself, "If I cry for those sweets, I'm going

147
00:09:13,030 --> 00:09:16,000
 to wind up in hell and so he wouldn't take the sweets."

148
00:09:16,000 --> 00:09:20,660
 And so then they tried to starve him and make him cry. And

149
00:09:20,660 --> 00:09:25,200
 he felt so much hunger, but he thought to himself, "The

150
00:09:25,200 --> 00:09:30,000
 pain of hell is far worse than the pain of this hunger."

151
00:09:30,000 --> 00:09:33,280
 So he forbear with it. And for 16 years, they tested him

152
00:09:33,280 --> 00:09:36,450
 again and again and again, thinking, "There's nothing wrong

153
00:09:36,450 --> 00:09:42,000
 with him. The doctors came and said he's perfectly fine."

154
00:09:42,000 --> 00:09:46,890
 But for 16 years, you can say he took a very bad situation

155
00:09:46,890 --> 00:09:51,280
 that he had got himself into and turned it around, made

156
00:09:51,280 --> 00:09:53,000
 some new good, excellent karma.

157
00:09:53,000 --> 00:09:55,860
 And in the end, they threw him out. They decided they were

158
00:09:55,860 --> 00:09:57,000
 going to kill him.

159
00:09:57,000 --> 00:10:01,950
 So they sent him off with his caretaker to be hit over the

160
00:10:01,950 --> 00:10:07,000
 head and put in a shallow grave and buried there and said,

161
00:10:07,000 --> 00:10:08,000
 "This son is useless.

162
00:10:08,000 --> 00:10:12,830
 He's inauspicious and he's going to create trouble for the

163
00:10:12,830 --> 00:10:14,000
 kingdom."

164
00:10:14,000 --> 00:10:18,600
 So when he gets to the forest, he stands up, tests out his

165
00:10:18,600 --> 00:10:21,000
 strength, finds out he still can move, he's still strong.

166
00:10:21,000 --> 00:10:25,630
 And he says to the guy, he says, "Thanks for taking me here

167
00:10:25,630 --> 00:10:28,000
. You can go home now."

168
00:10:28,000 --> 00:10:30,750
 The guy looks up and, "Who's that? Who are you?" He didn't

169
00:10:30,750 --> 00:10:33,000
 ever see him talk before.

170
00:10:33,000 --> 00:10:36,680
 He says, "I'm your prince. I've decided not to become a

171
00:10:36,680 --> 00:10:39,000
 king. I'm going to go forth."

172
00:10:39,000 --> 00:10:41,380
 And so he sends him back home and goes off and becomes an

173
00:10:41,380 --> 00:10:42,000
 ascetic.

174
00:10:42,000 --> 00:10:44,210
 Then the whole kingdom follows him and eventually they all

175
00:10:44,210 --> 00:10:45,000
 become ascetic.

176
00:10:45,000 --> 00:10:48,730
 This is the taimiya jhana, a very famous point being, he

177
00:10:48,730 --> 00:10:53,000
 takes a very bad situation, even bad karma, you might say,

178
00:10:53,000 --> 00:10:57,000
 and turns it into a wonderful thing and now he winds up

179
00:10:57,000 --> 00:11:01,000
 emptying a whole kingdom and turning them all into ascetics

180
00:11:01,000 --> 00:11:06,340
 instead of following his karma and becoming king again and

181
00:11:06,340 --> 00:11:11,000
 carrying on the horrible tradition of his father.

182
00:11:11,000 --> 00:11:14,000
 I don't have anything to add to that.

