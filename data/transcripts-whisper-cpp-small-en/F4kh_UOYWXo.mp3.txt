 Where to place observation while watching the stomach
 during meditation?
 Where to place observer?
 What are you watching while you're just watching the
 stomach?
 An easy way is to lie on your back if it's really painful
 to do this because our bodies are so stressed out and tense
.
 You have to lie down on your back just to confirm it and
 you'll see it can be quite surprising for people.
 People will come to ask for advice and say, "I really can't
 do the rising and falling, it just doesn't happen for me."
 And you explain to them, "Well, this is because your body
 is in an unnatural state."
 And they're, "Yeah, right. Why should I believe you?"
 Have them lie down on their back and suddenly boom, their
 stomach goes like a baby.
 It's like magic all of a sudden because then they're
 relaxed.
 And so you're able to show them that it's only because of
 your stress and tension that it's difficult.
 The rising and falling, it's part of, I suppose, part of
 the problem why nowadays it's so much...
 What's the word?
 People don't really like watching the stomach thing, which
 wouldn't have been the case in times gone by when we would
 naturally breathe through the stomach.
 And it isn't the case for people living in the forest who
 live simple, peaceful lives because they breathe naturally.
 Like a baby, they'll breathe from the abdomen. It's quite a
 natural thing.
 You just observe that movement, but maybe the reason why
 you're asking the question is because you're not able to
 observe it and it's kind of confusing.
 So try lying on your back is a good way to start until
 eventually you can do it sitting up.
 You can also, of course, put your hand on the stomach,
 which helps you in the beginning.
 Part of it is going to be going through the stage of
 feeling the tension.
 Eventually the tension will come up in your shoulders, in
 your back, and you'll be able to see what the real problem
 is.
 From sitting still so long, the tension builds up and
 builds up.
 And then as you acknowledge the tension, tens, tens, the
 body releases and lets go.
 And then you'll find that actually the stomach is much
 easier to be mindful of.
 And also as you stop forcing the stomach and start letting
 it rise and fall as it will, it becomes easier to
 acknowledge.
 [silence]
