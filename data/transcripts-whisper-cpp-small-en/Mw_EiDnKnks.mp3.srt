1
00:00:00,000 --> 00:00:02,000
 Okay, go ahead.

2
00:00:02,000 --> 00:00:07,560
 I find that my meditation practice has progressed where I

3
00:00:07,560 --> 00:00:10,470
 can see no need to think about the stresses I've had in the

4
00:00:10,470 --> 00:00:11,000
 past,

5
00:00:11,000 --> 00:00:15,000
 but now I'm seeing them pop up in my dreams that night.

6
00:00:15,000 --> 00:00:23,000
 Yeah.

7
00:00:23,000 --> 00:00:26,000
 Well, it couldn't be that you're still suppressing them,

8
00:00:26,000 --> 00:00:29,000
 but that's not necessarily the case.

9
00:00:29,000 --> 00:00:32,590
 They can't just be, you know, the brain keeps echoes of

10
00:00:32,590 --> 00:00:33,000
 stuff,

11
00:00:33,000 --> 00:00:38,000
 so you've got lots of backup copies of your emotions.

12
00:00:38,000 --> 00:00:41,620
 Once you give them up, your brain still holds onto them and

13
00:00:41,620 --> 00:00:45,000
 pops up with them.

14
00:00:45,000 --> 00:00:48,450
 If you find that in your dreams you're stressed, you're

15
00:00:48,450 --> 00:00:50,000
 stressed about them.

16
00:00:50,000 --> 00:00:51,740
 Right? Is that what you're saying? I know you think about

17
00:00:51,740 --> 00:00:52,000
 the stress,

18
00:00:52,000 --> 00:00:54,000
 but okay, the question is, are you stressed in your dreams?

19
00:00:54,000 --> 00:00:56,790
 Because if you're stressed in your dreams, then you're

20
00:00:56,790 --> 00:00:58,000
 still missing.

21
00:00:58,000 --> 00:01:00,000
 You still haven't dealt with them.

22
00:01:00,000 --> 00:01:02,000
 It means you still have problems.

23
00:01:02,000 --> 00:01:10,070
 In fact, you should find that you dream less the further

24
00:01:10,070 --> 00:01:13,000
 you get in the practice.

25
00:01:13,000 --> 00:01:17,000
 But I can say it can still come up.

26
00:01:17,000 --> 00:01:19,000
 The question is how you relate to it in your dream.

27
00:01:19,000 --> 00:01:22,180
 If you still have fear and worry and stress and anger and

28
00:01:22,180 --> 00:01:23,000
 lust and so on,

29
00:01:23,000 --> 00:01:31,020
 even in the dream, that's a sign most things are still in

30
00:01:31,020 --> 00:01:33,000
 your mind.

