1
00:00:00,000 --> 00:00:05,640
 Hi, so in this video I will be discussing the number two

2
00:00:05,640 --> 00:00:06,920
 reason why everyone should

3
00:00:06,920 --> 00:00:08,840
 practice meditation.

4
00:00:08,840 --> 00:00:11,370
 And the number two in the top five reasons why everyone

5
00:00:11,370 --> 00:00:12,880
 should practice meditation is

6
00:00:12,880 --> 00:00:15,440
 that meditation puts one on the right path.

7
00:00:15,440 --> 00:00:18,560
 It allows one to live one's life in a way which can be

8
00:00:18,560 --> 00:00:20,720
 considered moral, which can be

9
00:00:20,720 --> 00:00:24,670
 considered noble, which can be considered truly an

10
00:00:24,670 --> 00:00:26,800
 honorable way of living.

11
00:00:26,800 --> 00:00:30,670
 So we understand in the meditation tradition that there are

12
00:00:30,670 --> 00:00:32,400
 many ways that we can live

13
00:00:32,400 --> 00:00:33,400
 our lives.

14
00:00:33,400 --> 00:00:35,520
 We can live our lives angry.

15
00:00:35,520 --> 00:00:37,240
 We can live our lives greedy.

16
00:00:37,240 --> 00:00:40,640
 We can live our lives in delusion or ignorance.

17
00:00:40,640 --> 00:00:43,520
 We can live our lives as an ordinary human being.

18
00:00:43,520 --> 00:00:49,260
 We can live our lives in a sort of a generous or charitable

19
00:00:49,260 --> 00:00:52,520
 way, doing good deeds, doing

20
00:00:52,520 --> 00:00:56,060
 unto others as you wish they would do unto you. We can even

21
00:00:56,060 --> 00:00:57,720
 live our lives in some kind

22
00:00:57,720 --> 00:01:03,640
 of meditative trance where we transcend ordinary reality.

23
00:01:03,640 --> 00:01:09,360
 All of these are individual and unique ways of living one's

24
00:01:09,360 --> 00:01:11,920
 life and unique paths which

25
00:01:11,920 --> 00:01:13,460
 one can take.

26
00:01:13,460 --> 00:01:17,270
 The meditation practice as we discussed it on this channel

27
00:01:17,270 --> 00:01:19,200
 is yet one other way in which

28
00:01:19,200 --> 00:01:22,160
 one can live one's life and we consider it to be the way to

29
00:01:22,160 --> 00:01:23,640
 live correctly or the way

30
00:01:23,640 --> 00:01:24,840
 to live properly.

31
00:01:24,840 --> 00:01:25,840
 Why this is so?

32
00:01:25,840 --> 00:01:28,920
 Well, we compare it to other ways of living.

33
00:01:28,920 --> 00:01:33,690
 The first three ways of living, ways of living in anger, in

34
00:01:33,690 --> 00:01:36,360
 hatred, ways of living in greed

35
00:01:36,360 --> 00:01:40,840
 or addiction, and ways of living in delusion or ignorance.

36
00:01:40,840 --> 00:01:43,560
 These are obviously ways which are unwholesome, ways which

37
00:01:43,560 --> 00:01:45,120
 don't lead to happiness, don't

38
00:01:45,120 --> 00:01:47,040
 lead to peace.

39
00:01:47,040 --> 00:01:49,800
 And the practice of meditation is clearly a way which, as I

40
00:01:49,800 --> 00:01:51,600
've explained in other videos,

41
00:01:51,600 --> 00:01:56,890
 allows one to move away from these paths, to move away from

42
00:01:56,890 --> 00:01:59,200
 states of being which put

43
00:01:59,200 --> 00:02:01,920
 one into a state of real suffering.

44
00:02:01,920 --> 00:02:04,480
 We can say that a person who is angry all the time is like

45
00:02:04,480 --> 00:02:05,880
 a person living in hell.

46
00:02:05,880 --> 00:02:07,480
 Your life is hell.

47
00:02:07,480 --> 00:02:12,240
 A person who is in greed and addiction all the time is just

48
00:02:12,240 --> 00:02:14,960
 like a ghost, just like someone

49
00:02:14,960 --> 00:02:19,440
 who is always looking for things, who is never satisfied.

50
00:02:19,440 --> 00:02:22,300
 And living in delusion, we say it's like living as an

51
00:02:22,300 --> 00:02:24,560
 animal, living as an ordinary dog or

52
00:02:24,560 --> 00:02:30,020
 a cat or a pig or a goat or a cow or any of the life forms

53
00:02:30,020 --> 00:02:33,360
 out there which we might think

54
00:02:33,360 --> 00:02:37,280
 are perhaps less advanced or less able to develop

55
00:02:37,280 --> 00:02:38,600
 themselves.

56
00:02:38,600 --> 00:02:41,920
 Living as an ordinary human being is one other way we can

57
00:02:41,920 --> 00:02:43,080
 live our lives.

58
00:02:43,080 --> 00:02:45,970
 It's a way which doesn't help the world very much but

59
00:02:45,970 --> 00:02:47,840
 perhaps we can say it doesn't hurt

60
00:02:47,840 --> 00:02:49,520
 the world either.

61
00:02:49,520 --> 00:02:52,200
 The practice of meditation is a way which is noble.

62
00:02:52,200 --> 00:02:55,350
 It's not an ordinary way of living, though we focus and we

63
00:02:55,350 --> 00:02:57,000
're always paying attention

64
00:02:57,000 --> 00:02:58,880
 to the ordinary reality.

65
00:02:58,880 --> 00:03:01,580
 It actually is a way which brings one out of ordinary,

66
00:03:01,580 --> 00:03:03,160
 everyday reality, allows one

67
00:03:03,160 --> 00:03:07,120
 to rise above the simple dreary and mundane existence, sort

68
00:03:07,120 --> 00:03:08,840
 of going with the flow, "When

69
00:03:08,840 --> 00:03:10,200
 things are good, you feel good.

70
00:03:10,200 --> 00:03:12,240
 When things are bad, you feel bad."

71
00:03:12,240 --> 00:03:15,000
 It's a way of rising above all of this and coming to see it

72
00:03:15,000 --> 00:03:16,240
 for what it is and living

73
00:03:16,240 --> 00:03:18,840
 in a state of real and true peace and happiness.

74
00:03:18,840 --> 00:03:21,040
 So it's not this path either.

75
00:03:21,040 --> 00:03:23,710
 It's a path which we consider to be better than simply

76
00:03:23,710 --> 00:03:24,340
 living.

77
00:03:24,340 --> 00:03:27,160
 It's also not simply the path of doing good deeds because

78
00:03:27,160 --> 00:03:28,860
 it's clear that people can do

79
00:03:28,860 --> 00:03:31,180
 deeds with ulterior motives.

80
00:03:31,180 --> 00:03:35,200
 People can do deeds out of delusion or simply do deeds

81
00:03:35,200 --> 00:03:37,800
 because it hits them to do them at

82
00:03:37,800 --> 00:03:38,800
 a certain time.

83
00:03:38,800 --> 00:03:41,800
 They can do good deeds sometimes and then forget to do good

84
00:03:41,800 --> 00:03:42,360
 deeds.

85
00:03:42,360 --> 00:03:45,880
 But when one's mind is pure, when one lives one's life in

86
00:03:45,880 --> 00:03:47,960
 the present moment, one is able

87
00:03:47,960 --> 00:03:51,260
 to approach everything with a certain amount of wisdom and

88
00:03:51,260 --> 00:03:52,960
 can see the need, what is the

89
00:03:52,960 --> 00:03:54,200
 need in every situation.

90
00:03:54,200 --> 00:03:57,590
 And it's able to approach every situation, finding the way

91
00:03:57,590 --> 00:03:59,160
 to bring help and to bring

92
00:03:59,160 --> 00:04:01,680
 benefit to other people to do good deeds.

93
00:04:01,680 --> 00:04:05,580
 So it's not exactly the way of being a good person, just

94
00:04:05,580 --> 00:04:07,980
 doing good deeds, but in a sense

95
00:04:07,980 --> 00:04:12,160
 it makes one's heart truly good.

96
00:04:12,160 --> 00:04:15,840
 And as a result, everything one does then becomes good.

97
00:04:15,840 --> 00:04:19,130
 So rather than intending to do good deeds, one just does

98
00:04:19,130 --> 00:04:20,840
 them as a part of one's nature

99
00:04:20,840 --> 00:04:22,760
 because one's mind is pure.

100
00:04:22,760 --> 00:04:25,560
 When someone asks for something, there's no stinginess.

101
00:04:25,560 --> 00:04:29,070
 Someone approaches you with anger and so on, there's no

102
00:04:29,070 --> 00:04:29,920
 reaction.

103
00:04:29,920 --> 00:04:33,760
 And so we live our lives, you could say, as good people,

104
00:04:33,760 --> 00:04:36,140
 simply because the mind is pure.

105
00:04:36,140 --> 00:04:41,680
 It's also not the path of meditative trance or absorption.

106
00:04:41,680 --> 00:04:44,180
 We're not looking for this heavy state of concentration

107
00:04:44,180 --> 00:04:45,640
 where we forget about the world

108
00:04:45,640 --> 00:04:47,320
 because this of course is temporary.

109
00:04:47,320 --> 00:04:51,290
 You can work very, very hard to take the mind away from

110
00:04:51,290 --> 00:04:53,480
 reality, but in the end it only

111
00:04:53,480 --> 00:04:58,240
 lasts for as long as the power of the concentration.

112
00:04:58,240 --> 00:05:02,450
 So if you work to X amount to bring your mind to a state of

113
00:05:02,450 --> 00:05:04,840
 tranquility, well it only lasts

114
00:05:04,840 --> 00:05:07,960
 the very same amount of time.

115
00:05:07,960 --> 00:05:12,110
 It's very, very much dependent on how much work you put

116
00:05:12,110 --> 00:05:13,000
 into it.

117
00:05:13,000 --> 00:05:15,920
 The practice of meditation as we describe it here is not

118
00:05:15,920 --> 00:05:17,920
 because it involves understanding.

119
00:05:17,920 --> 00:05:20,110
 And once you understand, you can't say that the

120
00:05:20,110 --> 00:05:21,880
 understanding is going to run out.

121
00:05:21,880 --> 00:05:24,940
 This is understanding something you can use in every

122
00:05:24,940 --> 00:05:25,800
 situation.

123
00:05:25,800 --> 00:05:30,040
 When you understand something, you can never, you won't

124
00:05:30,040 --> 00:05:32,560
 eventually forget that it's such

125
00:05:32,560 --> 00:05:35,080
 and such is the nature of it.

126
00:05:35,080 --> 00:05:37,550
 You come to understand the nature of all of the things

127
00:05:37,550 --> 00:05:39,080
 around you and inside of you.

128
00:05:39,080 --> 00:05:43,100
 And so you react to things with wisdom and with peace and

129
00:05:43,100 --> 00:05:45,360
 with happiness at all times.

130
00:05:45,360 --> 00:05:48,360
 So your mind just never falls into suffering.

131
00:05:48,360 --> 00:05:52,400
 This is considered to be the way, the right way.

132
00:05:52,400 --> 00:05:54,640
 And it's important to understand that here we don't mean,

133
00:05:54,640 --> 00:05:55,920
 it means everyone has to become

134
00:05:55,920 --> 00:05:59,970
 a monk or everyone has to become this or that religious

135
00:05:59,970 --> 00:06:02,520
 tradition or move here or move there.

136
00:06:02,520 --> 00:06:05,550
 You can do whatever it is that you're doing right now as

137
00:06:05,550 --> 00:06:06,920
 long as it's a moral wholesome

138
00:06:06,920 --> 00:06:10,530
 thing to do and it's not creating states of suffering for

139
00:06:10,530 --> 00:06:12,460
 yourself or others and still

140
00:06:12,460 --> 00:06:13,800
 be on the right path.

141
00:06:13,800 --> 00:06:16,440
 We can be doing, all be living completely different lives

142
00:06:16,440 --> 00:06:17,760
 and still all be on the right

143
00:06:17,760 --> 00:06:20,300
 path because our minds are pure.

144
00:06:20,300 --> 00:06:22,960
 Whatever we do, we do it with a pure mind and this is

145
00:06:22,960 --> 00:06:24,640
 considered to be the right path.

146
00:06:24,640 --> 00:06:27,430
 This is the number two reason why everyone should practice

147
00:06:27,430 --> 00:06:28,240
 meditation.

148
00:06:28,240 --> 00:06:31,350
 So thanks for tuning in and please look forward to number

149
00:06:31,350 --> 00:06:32,600
 one in the future.

150
00:06:32,600 --> 00:06:33,100
 Thank you.

