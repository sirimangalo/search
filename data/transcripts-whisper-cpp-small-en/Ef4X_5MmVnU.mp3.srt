1
00:00:00,000 --> 00:00:05,000
 Okay, good evening. Welcome to our evening Dhamma.

2
00:00:05,000 --> 00:00:12,000
 Tonight we're looking at the fourth noble truth.

3
00:00:12,000 --> 00:00:19,100
 And I think this gives us a good point to segue into a

4
00:00:19,100 --> 00:00:20,000
 second list,

5
00:00:20,000 --> 00:00:24,000
 which is of course the eightfold noble path.

6
00:00:24,000 --> 00:00:28,000
 The fourth noble truth has eight parts.

7
00:00:28,000 --> 00:00:35,000
 And because it's another list, we could do one path factor

8
00:00:35,000 --> 00:00:38,000
 every night.

9
00:00:38,000 --> 00:00:42,000
 Start doing series and get into our second series.

10
00:00:42,000 --> 00:00:47,000
 The thing about the Buddha's teaching is it all connects.

11
00:00:47,000 --> 00:00:54,000
 I mean none of these things are really real.

12
00:00:54,000 --> 00:00:58,000
 And so it's not like we're putting...

13
00:00:58,000 --> 00:01:01,000
 I mean many of them are not real.

14
00:01:01,000 --> 00:01:04,000
 Many of these constructs are not real, not all of them.

15
00:01:04,000 --> 00:01:05,000
 Some are real.

16
00:01:05,000 --> 00:01:09,080
 But the construct of the eightfold noble path is one

17
00:01:09,080 --> 00:01:10,000
 construct.

18
00:01:10,000 --> 00:01:15,000
 And so it becomes quite complicated.

19
00:01:15,000 --> 00:01:18,000
 Everything sort of melds into each other.

20
00:01:18,000 --> 00:01:22,320
 So the fourth noble truth is actually the eightfold noble

21
00:01:22,320 --> 00:01:23,000
 path.

22
00:01:23,000 --> 00:01:26,000
 But the first noble truth is knowledge of the...

23
00:01:26,000 --> 00:01:29,230
 Or the first factor of the eightfold noble path is

24
00:01:29,230 --> 00:01:33,000
 knowledge of the four noble truths.

25
00:01:33,000 --> 00:01:39,000
 So everything connects together because they're concepts.

26
00:01:39,000 --> 00:01:43,000
 And so it might seem quite complicated at times.

27
00:01:43,000 --> 00:01:46,760
 It's important to understand that it's not really

28
00:01:46,760 --> 00:01:50,000
 describing ultimate reality in many cases.

29
00:01:50,000 --> 00:01:54,000
 These are constructs that the Buddha put together.

30
00:01:54,000 --> 00:01:56,000
 So when we talk about the eightfold noble path,

31
00:01:56,000 --> 00:02:02,260
 we're talking about a means of organizing reality and

32
00:02:02,260 --> 00:02:04,000
 organizing experience.

33
00:02:04,000 --> 00:02:10,000
 And more importantly, directing it into a way.

34
00:02:10,000 --> 00:02:18,310
 We're describing reality and the essential aspects of those

35
00:02:18,310 --> 00:02:19,000
 experiences

36
00:02:19,000 --> 00:02:25,200
 that incline towards peace, happiness, and freedom from

37
00:02:25,200 --> 00:02:26,000
 suffering.

38
00:02:26,000 --> 00:02:29,000
 That we call the way.

39
00:02:29,000 --> 00:02:32,000
 So tonight I'll just talk about the way in general.

40
00:02:32,000 --> 00:02:37,350
 And then I'll give a short description of each of the eight

41
00:02:37,350 --> 00:02:46,000
 path factors, one per night.

42
00:02:46,000 --> 00:02:49,000
 So we have...

43
00:02:49,000 --> 00:02:53,280
 The four noble truths are laid out in terms of effect,

44
00:02:53,280 --> 00:02:57,000
 cause, effect, cause.

45
00:02:57,000 --> 00:03:02,000
 So suffering is the effect, craving is the cause.

46
00:03:02,000 --> 00:03:05,000
 The cessation of suffering is the effect.

47
00:03:05,000 --> 00:03:12,000
 The path to the cessation of suffering is the cause.

48
00:03:12,000 --> 00:03:14,000
 Roughly speaking.

49
00:03:14,000 --> 00:03:17,940
 I mean, the path is that which leads to cessation of

50
00:03:17,940 --> 00:03:19,000
 suffering.

51
00:03:19,000 --> 00:03:23,360
 It's not actually the cause because cessation can't be

52
00:03:23,360 --> 00:03:24,000
 caused.

53
00:03:24,000 --> 00:03:29,000
 But it's what leads to the cessation experience.

54
00:03:29,000 --> 00:03:43,000
 It causes the experience of cessation, you might say.

55
00:03:43,000 --> 00:03:50,170
 The first thing to know about the way on the path is to

56
00:03:50,170 --> 00:03:51,000
 understand

57
00:03:51,000 --> 00:03:57,000
 what sort of a path it is, how it functions as a path.

58
00:03:57,000 --> 00:04:00,000
 When you think of a path, of course, a path is something

59
00:04:00,000 --> 00:04:07,000
 that leads to a place, not where you are.

60
00:04:07,000 --> 00:04:15,000
 And so we kind of think of it as going somewhere, at least

61
00:04:15,000 --> 00:04:18,000
 in a symbolic way.

62
00:04:18,000 --> 00:04:22,000
 Buddhism isn't a path in the sense of the path to heaven.

63
00:04:22,000 --> 00:04:24,760
 When we think of religious paths, we're usually thinking of

64
00:04:24,760 --> 00:04:27,000
 this, how to go somewhere else.

65
00:04:27,000 --> 00:04:29,000
 How to get out of this place.

66
00:04:29,000 --> 00:04:32,000
 So we talk about getting out of samsara.

67
00:04:32,000 --> 00:04:36,620
 And it becomes this sort of idea of here samsara nirbhanas

68
00:04:36,620 --> 00:04:38,000
 is outside of it.

69
00:04:38,000 --> 00:04:43,000
 Like in the same way that heaven is outside of earth.

70
00:04:43,000 --> 00:04:47,000
 But it's not really like that.

71
00:04:47,000 --> 00:04:52,000
 The way is much more like a way of life.

72
00:04:52,000 --> 00:04:55,000
 It's a way of living your life rather than going somewhere.

73
00:04:55,000 --> 00:04:58,000
 Not like a way to New York City.

74
00:04:58,000 --> 00:05:01,000
 It's a way to live.

75
00:05:01,000 --> 00:05:03,630
 And so when we think about the way, it's important to

76
00:05:03,630 --> 00:05:05,000
 remember what is it a way towards.

77
00:05:05,000 --> 00:05:08,000
 It's a way to freedom from suffering.

78
00:05:08,000 --> 00:05:11,000
 We're talking about the suffering that we have.

79
00:05:11,000 --> 00:05:15,000
 And when we talk about the way, what we mean is the means

80
00:05:15,000 --> 00:05:18,000
 to become free from that.

81
00:05:18,000 --> 00:05:23,770
 When you practice according to this way, your suffering

82
00:05:23,770 --> 00:05:25,000
 ceases.

83
00:05:25,000 --> 00:05:29,030
 I mean, it becomes reduced and knowledge and understanding

84
00:05:29,030 --> 00:05:31,000
 about suffering comes about.

85
00:05:31,000 --> 00:05:36,000
 And eventually there's a cessation of suffering.

86
00:05:36,000 --> 00:05:37,000
 That's what it leads to.

87
00:05:37,000 --> 00:05:39,000
 But it doesn't go anywhere else.

88
00:05:39,000 --> 00:05:41,000
 It doesn't happen somewhere else.

89
00:05:41,000 --> 00:05:43,000
 It doesn't lead you somewhere else.

90
00:05:43,000 --> 00:05:46,520
 The cessation happens right here and it's the mind not

91
00:05:46,520 --> 00:05:48,000
 going anywhere.

92
00:05:48,000 --> 00:05:56,000
 It's when the mind stops going.

93
00:05:56,000 --> 00:06:00,030
 The Buddha talked about the Eightfold Noble Path as the

94
00:06:00,030 --> 00:06:01,000
 essence.

95
00:06:01,000 --> 00:06:05,710
 He said in whatever religion, it's one of the last things

96
00:06:05,710 --> 00:06:07,000
 he said apparently,

97
00:06:07,000 --> 00:06:12,420
 whatever teaching, whatever dispensation that you find the

98
00:06:12,420 --> 00:06:14,000
 Eightfold Noble Path,

99
00:06:14,000 --> 00:06:18,420
 in that religion you will find Sotapana, Sakadagami, Anag

100
00:06:18,420 --> 00:06:20,000
ami and Arahant.

101
00:06:20,000 --> 00:06:24,000
 You will find enlightened beings.

102
00:06:24,000 --> 00:06:28,560
 Whatever dispensation does not have the Eightfold Noble

103
00:06:28,560 --> 00:06:29,000
 Path,

104
00:06:29,000 --> 00:06:32,000
 you will not find enlightened beings.

105
00:06:32,000 --> 00:06:33,000
 This is important.

106
00:06:33,000 --> 00:06:37,290
 It answers the question of whether all religions are the

107
00:06:37,290 --> 00:06:38,000
 same.

108
00:06:38,000 --> 00:06:44,610
 The Buddha was clearly of the opinion that no, no, they're

109
00:06:44,610 --> 00:06:47,000
 quite different.

110
00:06:47,000 --> 00:06:54,490
 We think of religions generally as being of a certain

111
00:06:54,490 --> 00:06:56,000
 nature.

112
00:06:56,000 --> 00:06:59,800
 So if you think religion is beneficial, you think that all

113
00:06:59,800 --> 00:07:02,000
 religions are beneficial.

114
00:07:02,000 --> 00:07:05,280
 And we tend to lump them together and say all religions

115
00:07:05,280 --> 00:07:07,000
 teach people to be good.

116
00:07:07,000 --> 00:07:11,180
 It's the corrupt, the corrupt or the corruptions of

117
00:07:11,180 --> 00:07:15,000
 religion that are the problem.

118
00:07:15,000 --> 00:07:18,000
 And then on the other hand if you see that religion is

119
00:07:18,000 --> 00:07:19,000
 generally corrupt

120
00:07:19,000 --> 00:07:22,680
 then you would tend to lump them all together and say all

121
00:07:22,680 --> 00:07:24,000
 religion is bad.

122
00:07:24,000 --> 00:07:26,000
 Religion is a poison.

123
00:07:26,000 --> 00:07:32,000
 Religion ruins everything, that kind of thing.

124
00:07:32,000 --> 00:07:37,000
 But both of these are very simplistic ways of looking.

125
00:07:37,000 --> 00:07:51,000
 And it has a lot more to do with our desire to simplify.

126
00:07:51,000 --> 00:07:55,000
 You might say laziness, it's not the word I'm looking for,

127
00:07:55,000 --> 00:07:58,890
 but we just don't have the energy to nuance our

128
00:07:58,890 --> 00:08:01,000
 understanding of religion.

129
00:08:01,000 --> 00:08:07,000
 It's much simpler and harmonious for us to just say,

130
00:08:07,000 --> 00:08:09,000
 "Hey, you are of a different religion."

131
00:08:09,000 --> 00:08:11,730
 Well that's okay because we know that all religions are

132
00:08:11,730 --> 00:08:15,000
 good and basically the same.

133
00:08:15,000 --> 00:08:17,000
 And that's fine.

134
00:08:17,000 --> 00:08:22,000
 I think that's actually positive in a worldly sense.

135
00:08:22,000 --> 00:08:27,000
 We accept that people's beliefs are their own.

136
00:08:27,000 --> 00:08:34,000
 And in a general sense it's what allows them to be social

137
00:08:34,000 --> 00:08:44,280
 and connected to a sense of purpose and a sense of normalcy

138
00:08:44,280 --> 00:08:45,000
.

139
00:08:45,000 --> 00:08:49,000
 So in a general sort of societal sense that's fine.

140
00:08:49,000 --> 00:08:51,000
 We get along with all religions.

141
00:08:51,000 --> 00:08:53,000
 I think that's a good thing.

142
00:08:53,000 --> 00:08:56,000
 It's very useful anyway for people living in the world.

143
00:08:56,000 --> 00:09:01,400
 But when we talk about the truth, we have to call a spade a

144
00:09:01,400 --> 00:09:02,000
 spade.

145
00:09:02,000 --> 00:09:06,050
 And all religions are very different, some more similar

146
00:09:06,050 --> 00:09:07,000
 than others.

147
00:09:07,000 --> 00:09:11,110
 I mean most religions deal with God and so they tend to be

148
00:09:11,110 --> 00:09:12,000
 fairly similar.

149
00:09:12,000 --> 00:09:14,000
 God being somehow omnipotent.

150
00:09:14,000 --> 00:09:20,000
 But even that, you know, even take Christianity or Islam.

151
00:09:20,000 --> 00:09:28,950
 Many different Christians think many different things about

152
00:09:28,950 --> 00:09:30,000
 God.

153
00:09:30,000 --> 00:09:34,340
 Buddhism is specifically a religion of the eightfold noble

154
00:09:34,340 --> 00:09:35,000
 path.

155
00:09:35,000 --> 00:09:37,000
 That's what it is.

156
00:09:37,000 --> 00:09:42,300
 It's nothing more, anything else is perhaps useful and

157
00:09:42,300 --> 00:09:43,000
 helpful,

158
00:09:43,000 --> 00:09:50,000
 but ultimately ancillary, auxiliary, or even superfluous.

159
00:09:50,000 --> 00:09:53,400
 If it falls outside of the eightfold noble path, then it's

160
00:09:53,400 --> 00:09:54,000
 one of two things.

161
00:09:54,000 --> 00:09:59,340
 It's either contrary to Buddhism or it's superfluous,

162
00:09:59,340 --> 00:10:00,000
 unnecessary,

163
00:10:00,000 --> 00:10:04,000
 unessential to Buddhism.

164
00:10:04,000 --> 00:10:07,640
 So there are things that are outside of the eightfold noble

165
00:10:07,640 --> 00:10:10,000
 path like charity.

166
00:10:10,000 --> 00:10:13,100
 You might say charity doesn't actually fall in the eight

167
00:10:13,100 --> 00:10:14,000
fold noble path,

168
00:10:14,000 --> 00:10:17,270
 but charity is quite useful if you're a charitable kind

169
00:10:17,270 --> 00:10:18,000
 person.

170
00:10:18,000 --> 00:10:22,760
 It certainly helps cultivate the eightfold noble path, so

171
00:10:22,760 --> 00:10:26,000
 it's a good thing.

172
00:10:26,000 --> 00:10:30,510
 You might say study of Buddhism doesn't fall in the eight

173
00:10:30,510 --> 00:10:33,000
fold noble path, etc.

174
00:10:33,000 --> 00:10:38,780
 Many things, many aspects of religious activity, even mon

175
00:10:38,780 --> 00:10:44,000
asticism, you might say.

176
00:10:44,000 --> 00:10:52,070
 Anyway, when we talk about the eightfold noble path, there

177
00:10:52,070 --> 00:10:56,000
 are two paths.

178
00:10:56,000 --> 00:10:58,920
 So it's important to understand what we mean by the eight

179
00:10:58,920 --> 00:11:00,000
fold noble path.

180
00:11:00,000 --> 00:11:03,840
 In Buddhism there are two paths, and this is sort of the

181
00:11:03,840 --> 00:11:05,000
 orthodox understanding.

182
00:11:05,000 --> 00:11:14,000
 There's what's called the pubangamaga and the aryamaga.

183
00:11:14,000 --> 00:11:18,000
 The pubangamaga is mundane.

184
00:11:18,000 --> 00:11:21,000
 All of us are practicing the pubangamaga.

185
00:11:21,000 --> 00:11:24,000
 Every time we meditate, we're on the pubanga.

186
00:11:24,000 --> 00:11:31,410
 The pubangamaga means belonging to, and puba means prior or

187
00:11:31,410 --> 00:11:34,000
 preliminary.

188
00:11:34,000 --> 00:11:37,560
 The pubangamaga is that which comes before, that which

189
00:11:37,560 --> 00:11:40,000
 leads up to the eightfold noble path.

190
00:11:40,000 --> 00:11:47,000
 You can think of it like the preparation or the training.

191
00:11:47,000 --> 00:11:51,290
 What we're working up to is like when you rub two sticks

192
00:11:51,290 --> 00:11:53,000
 together for fire.

193
00:11:53,000 --> 00:11:57,000
 The moment that it sparks, then you're on the path.

194
00:11:57,000 --> 00:12:01,000
 Up until that point, you're on the preliminary path.

195
00:12:01,000 --> 00:12:04,200
 So when we talk about the noble eightfold path, we're

196
00:12:04,200 --> 00:12:07,000
 actually just talking about this moment

197
00:12:07,000 --> 00:12:13,000
 when you are on the path, when you're following the way.

198
00:12:13,000 --> 00:12:18,000
 Because that moment is the moment before nirvana.

199
00:12:18,000 --> 00:12:23,000
 It's truly the path because it's truly what causes nirvana.

200
00:12:23,000 --> 00:12:26,410
 Up until that point, we don't know if we're going to get to

201
00:12:26,410 --> 00:12:27,000
 nirvana.

202
00:12:27,000 --> 00:12:32,000
 When you practice for days and days on end, still not sure.

203
00:12:32,000 --> 00:12:35,090
 You still can't say, "Well, this path is going to lead me

204
00:12:35,090 --> 00:12:36,000
 to nirvana."

205
00:12:36,000 --> 00:12:38,000
 Well, maybe not. Maybe you stop early.

206
00:12:38,000 --> 00:12:41,000
 Maybe something gets in your way.

207
00:12:41,000 --> 00:12:44,000
 So it's preliminary. It's not yet noble.

208
00:12:44,000 --> 00:12:49,910
 You can't yet say, "This is what causes one to realize nir

209
00:12:49,910 --> 00:12:51,000
vana."

210
00:12:51,000 --> 00:12:56,000
 Because it doesn't yet. It's leading towards it.

211
00:12:56,000 --> 00:12:59,000
 And so it's important. It's necessary.

212
00:12:59,000 --> 00:13:01,000
 But it's called the pubangamagga.

213
00:13:01,000 --> 00:13:05,090
 And that still involves the eightfold noble, the eight path

214
00:13:05,090 --> 00:13:06,000
 factors.

215
00:13:06,000 --> 00:13:10,000
 So these eight path factors are preliminary.

216
00:13:10,000 --> 00:13:14,120
 They have a preliminary aspect, and they have a noble

217
00:13:14,120 --> 00:13:15,000
 aspect.

218
00:13:15,000 --> 00:13:18,000
 I hope that's not too confusing.

219
00:13:18,000 --> 00:13:22,130
 But it means when we talk about cultivating right view and

220
00:13:22,130 --> 00:13:24,000
 right thought and so on and so on,

221
00:13:24,000 --> 00:13:28,000
 we usually mean in a mundane sense.

222
00:13:28,000 --> 00:13:31,000
 And so when we normally talk about the eightfold path,

223
00:13:31,000 --> 00:13:34,000
 we're normally not talking about the noble eightfold path.

224
00:13:34,000 --> 00:13:42,000
 We're talking about the preliminary path.

225
00:13:42,000 --> 00:13:45,000
 The path has eight factors.

226
00:13:45,000 --> 00:13:49,000
 There's different ideas about, even the Buddha proposes

227
00:13:49,000 --> 00:13:53,000
 different ideas about how these eight factors interact.

228
00:13:53,000 --> 00:13:56,600
 I'm not going to go into detail. I don't want these talks

229
00:13:56,600 --> 00:13:59,000
 to be simple stuff.

230
00:13:59,000 --> 00:14:01,600
 If you want more detail, there's lots of books written

231
00:14:01,600 --> 00:14:03,000
 about this sort of thing.

232
00:14:03,000 --> 00:14:07,000
 But there's the idea that it goes in order.

233
00:14:07,000 --> 00:14:10,250
 So you can say, hey, you need right view first, and it

234
00:14:10,250 --> 00:14:13,390
 leads to right thought, and it leads to, and so on and so

235
00:14:13,390 --> 00:14:14,000
 on.

236
00:14:14,000 --> 00:14:17,000
 And the Buddha even adds two on the end.

237
00:14:17,000 --> 00:14:19,450
 He says, well, right concentration will lead to right

238
00:14:19,450 --> 00:14:20,000
 knowledge,

239
00:14:20,000 --> 00:14:24,530
 which leads to right sammawimutti, which is right

240
00:14:24,530 --> 00:14:26,000
 liberation.

241
00:14:26,000 --> 00:14:29,000
 And then there's the idea that it goes in a circle.

242
00:14:29,000 --> 00:14:31,150
 So when you get right view all the way down to right

243
00:14:31,150 --> 00:14:32,000
 concentration,

244
00:14:32,000 --> 00:14:35,900
 well, right concentration, when you're focused, cultivates

245
00:14:35,900 --> 00:14:37,000
 right view.

246
00:14:37,000 --> 00:14:39,000
 And so you see, well, it's actually a circle.

247
00:14:39,000 --> 00:14:41,640
 And then there are small circles, like these three go

248
00:14:41,640 --> 00:14:43,000
 together, and they work.

249
00:14:43,000 --> 00:14:47,000
 These two support this one, and so on.

250
00:14:47,000 --> 00:14:50,750
 The path factor is working together, and you can make it

251
00:14:50,750 --> 00:14:53,000
 quite complicated.

252
00:14:53,000 --> 00:14:55,470
 So I think understanding these two different paths is

253
00:14:55,470 --> 00:14:56,000
 important.

254
00:14:56,000 --> 00:14:59,860
 In the preliminary stage, you are working on the factors

255
00:14:59,860 --> 00:15:01,000
 individually.

256
00:15:01,000 --> 00:15:05,000
 Some factors you focus on more at different times.

257
00:15:05,000 --> 00:15:07,000
 Mostly we try to focus on mindfulness.

258
00:15:07,000 --> 00:15:09,000
 It's really the most important.

259
00:15:09,000 --> 00:15:12,710
 But, you know, right view, sometimes studying will help

260
00:15:12,710 --> 00:15:14,000
 clarify your view.

261
00:15:14,000 --> 00:15:17,000
 Asking questions will help to purify your view.

262
00:15:17,000 --> 00:15:20,800
 Practicing loving kindness will help to purify your thought

263
00:15:20,800 --> 00:15:21,000
, and so on.

264
00:15:21,000 --> 00:15:24,310
 Right livelihood is something, when we talk about right

265
00:15:24,310 --> 00:15:25,000
 livelihood,

266
00:15:25,000 --> 00:15:27,000
 we're normally talking about the preliminary path.

267
00:15:27,000 --> 00:15:34,690
 Okay, so I'm in wrong livelihood, I have to change my

268
00:15:34,690 --> 00:15:37,000
 livelihood.

269
00:15:37,000 --> 00:15:40,630
 But when it comes to the final on the path, all that it

270
00:15:40,630 --> 00:15:43,000
 means is that

271
00:15:43,000 --> 00:15:46,000
 you're in the perfect state of mind.

272
00:15:46,000 --> 00:15:49,000
 Your being, your essence is perfect.

273
00:15:49,000 --> 00:15:52,590
 I mean, a person can go from being a murderer, a liar, a

274
00:15:52,590 --> 00:15:54,000
 cheat, theoretically,

275
00:15:54,000 --> 00:15:58,000
 quite quickly to practicing the Eightfold Noble Path.

276
00:15:58,000 --> 00:16:02,000
 It's not likely, but it's certainly possible.

277
00:16:02,000 --> 00:16:08,320
 Because it's actually just, the path is actually, it's just

278
00:16:08,320 --> 00:16:10,000
 a mind state.

279
00:16:10,000 --> 00:16:14,000
 And so when we talk about these eight factors,

280
00:16:14,000 --> 00:16:17,570
 normally we're talking about working on them in a mundane

281
00:16:17,570 --> 00:16:18,000
 sense.

282
00:16:18,000 --> 00:16:20,000
 But when we talk about the Eightfold Noble Path,

283
00:16:20,000 --> 00:16:24,000
 what we mean is when every aspect of our being is perfect.

284
00:16:24,000 --> 00:16:28,000
 And usually that comes from years and years, or at least

285
00:16:28,000 --> 00:16:30,000
 days or weeks,

286
00:16:30,000 --> 00:16:34,730
 of working on them individually and putting them all

287
00:16:34,730 --> 00:16:36,000
 together.

288
00:16:36,000 --> 00:16:39,000
 But it doesn't have to.

289
00:16:39,000 --> 00:16:44,000
 It just means that in that moment, we have no inclination

290
00:16:44,000 --> 00:16:45,000
 towards wrong view,

291
00:16:45,000 --> 00:16:55,000
 and so we have nothing in us that is wrong in any way.

292
00:16:55,000 --> 00:16:59,660
 So I'll go through all eight of these one by one, see how

293
00:16:59,660 --> 00:17:00,000
 much,

294
00:17:00,000 --> 00:17:03,000
 I don't know if I'll be able to talk a lot about them,

295
00:17:03,000 --> 00:17:06,420
 but hopefully I'll have enough to give a talk for each of

296
00:17:06,420 --> 00:17:07,000
 them.

297
00:17:07,000 --> 00:17:10,680
 The last thing I'll say tonight about the Eightfold Noble

298
00:17:10,680 --> 00:17:11,000
 Path

299
00:17:11,000 --> 00:17:18,000
 is that it's basically organized into three parts,

300
00:17:18,000 --> 00:17:20,000
 and many of you are probably aware of this,

301
00:17:20,000 --> 00:17:22,700
 but if you want to simply understand the Eightfold Noble

302
00:17:22,700 --> 00:17:23,000
 Path,

303
00:17:23,000 --> 00:17:28,910
 we understand it in terms of morality, concentration, and

304
00:17:28,910 --> 00:17:30,000
 wisdom.

305
00:17:30,000 --> 00:17:34,000
 And I think this is probably the best way of organizing it.

306
00:17:34,000 --> 00:17:38,020
 If you put morality first, I mean this is the orthodox way

307
00:17:38,020 --> 00:17:39,000
 of doing it.

308
00:17:39,000 --> 00:17:42,000
 The Eightfold Noble Path has right view at the beginning,

309
00:17:42,000 --> 00:17:44,000
 and that's because it kind of works in a circle.

310
00:17:44,000 --> 00:17:47,000
 Well, you need right view to really have right morality,

311
00:17:47,000 --> 00:17:53,580
 but to start off on the path, the best place to start is

312
00:17:53,580 --> 00:17:55,000
 morality.

313
00:17:55,000 --> 00:17:59,000
 Because what's going to allow you to see things clearly

314
00:17:59,000 --> 00:18:04,620
 is moral and ethical behavior when you stop chasing after

315
00:18:04,620 --> 00:18:06,000
 all the things

316
00:18:06,000 --> 00:18:14,000
 that distract you and that sully the mind.

317
00:18:14,000 --> 00:18:16,600
 And so when we study the Visuddhi manga, this is where he

318
00:18:16,600 --> 00:18:17,000
 starts.

319
00:18:17,000 --> 00:18:20,000
 He starts with morality because we have the Buddha's word,

320
00:18:20,000 --> 00:18:26,000
 "Sili Patitaya," established on morality,

321
00:18:26,000 --> 00:18:28,000
 "Chitang Panyan Chambhavayang,"

322
00:18:28,000 --> 00:18:33,410
 and then develops concentration and wisdom based on the

323
00:18:33,410 --> 00:18:35,000
 morality.

324
00:18:35,000 --> 00:18:40,000
 So the first two factors are wisdom, right view and right

325
00:18:40,000 --> 00:18:40,000
 thought.

326
00:18:40,000 --> 00:18:42,000
 Those are the wisdom ones.

327
00:18:42,000 --> 00:18:45,400
 But they actually come at the end, really, in an ultimate

328
00:18:45,400 --> 00:18:46,000
 sense.

329
00:18:46,000 --> 00:18:48,000
 They're the goal.

330
00:18:48,000 --> 00:18:51,780
 And they come when you're focused, when your mind is

331
00:18:51,780 --> 00:18:54,000
 focused clearly.

332
00:18:54,000 --> 00:18:58,860
 And so the focus aspects are right effort, right

333
00:18:58,860 --> 00:19:00,000
 mindfulness,

334
00:19:00,000 --> 00:19:02,000
 and right concentration.

335
00:19:02,000 --> 00:19:04,520
 And those, of course, you can only be focused when you have

336
00:19:04,520 --> 00:19:05,000
 morality.

337
00:19:05,000 --> 00:19:08,990
 So those come from right morality, which is right speech,

338
00:19:08,990 --> 00:19:11,000
 right action,

339
00:19:11,000 --> 00:19:13,000
 and right livelihood.

340
00:19:13,000 --> 00:19:14,000
 Great.

341
00:19:14,000 --> 00:19:16,750
 So it starts with right speech, right action, and right

342
00:19:16,750 --> 00:19:18,000
 livelihood.

343
00:19:18,000 --> 00:19:22,000
 You put those together, and you'll start to become focused.

344
00:19:22,000 --> 00:19:26,000
 And as you're focused, right effort, right mindfulness,

345
00:19:26,000 --> 00:19:30,000
 right concentration, or right focus, these arise,

346
00:19:30,000 --> 00:19:32,000
 and then you start to see clearly.

347
00:19:32,000 --> 00:19:34,000
 And once you start to see clearly,

348
00:19:34,000 --> 00:19:43,000
 there will be right thought and ultimately right view.

349
00:19:43,000 --> 00:19:44,000
 This is the path.

350
00:19:44,000 --> 00:19:46,000
 This is Buddhism, really.

351
00:19:46,000 --> 00:19:49,000
 It's the core, the heart of when we talk about,

352
00:19:49,000 --> 00:19:51,000
 what do we do as Buddhists?

353
00:19:51,000 --> 00:19:53,000
 It's really not much, you know?

354
00:19:53,000 --> 00:19:55,000
 It's sometimes easy to get lost.

355
00:19:55,000 --> 00:19:58,410
 You read too much, read so much that you can't really

356
00:19:58,410 --> 00:20:00,000
 describe Buddhism.

357
00:20:00,000 --> 00:20:01,000
 It's quite simple.

358
00:20:01,000 --> 00:20:02,000
 We all know this.

359
00:20:02,000 --> 00:20:04,000
 There's the Eightfold Noble Path.

360
00:20:04,000 --> 00:20:05,000
 Don't lose sight of that.

361
00:20:05,000 --> 00:20:07,000
 Don't lose sight of the Four Noble Truths.

362
00:20:07,000 --> 00:20:09,000
 Don't lose sight of the Eightfold Noble Path.

363
00:20:09,000 --> 00:20:12,000
 The Eightfold Noble Path is what we do as Buddhists.

364
00:20:12,000 --> 00:20:13,000
 That's it.

365
00:20:13,000 --> 00:20:15,000
 Eight things.

366
00:20:15,000 --> 00:20:17,000
 Simplified, there's only three things.

367
00:20:17,000 --> 00:20:21,000
 Right, right, right morality, right effort.

368
00:20:21,000 --> 00:20:27,000
 Right morality, right focus, and right wisdom.

369
00:20:27,000 --> 00:20:31,000
 So that's the path.

370
00:20:31,000 --> 00:20:33,000
 And the next days, the next eight days, I guess,

371
00:20:33,000 --> 00:20:38,000
 I'll try to say something about each factor.

372
00:20:38,000 --> 00:20:40,000
 I think that'll be good.

373
00:20:40,000 --> 00:20:41,000
 So that's all for tonight.

374
00:20:41,000 --> 00:20:45,000
 Thank you all for tuning in.

375
00:20:45,000 --> 00:20:56,000
 I guess if we have questions, I'll take a look.

376
00:20:56,000 --> 00:21:00,000
 Two questions tonight.

377
00:21:00,000 --> 00:21:02,690
 Shaking uncontrollably, kind of like when you're about to

378
00:21:02,690 --> 00:21:03,000
 fall asleep,

379
00:21:03,000 --> 00:21:07,000
 but I'm wide awake while in meditation.

380
00:21:07,000 --> 00:21:11,000
 It's an enjoyable feeling, but I just try to observe it.

381
00:21:11,000 --> 00:21:16,000
 This is called "bhi-ti," rapture.

382
00:21:16,000 --> 00:21:22,000
 It's actually, it's not universal, but it's fairly common.

383
00:21:22,000 --> 00:21:24,000
 It comes in different forms.

384
00:21:24,000 --> 00:21:26,000
 Some people rock back and forth.

385
00:21:26,000 --> 00:21:28,000
 Some people will shake, that kind of thing.

386
00:21:28,000 --> 00:21:30,000
 Some people feel this uplifting feeling.

387
00:21:30,000 --> 00:21:32,000
 Some people feel really light.

388
00:21:32,000 --> 00:21:36,000
 It's many different states.

389
00:21:36,000 --> 00:21:37,000
 You can tell it to stop.

390
00:21:37,000 --> 00:21:41,000
 Just say to yourself, "Stop," and shake yourself out of it.

391
00:21:41,000 --> 00:21:43,000
 That sometimes helps.

392
00:21:43,000 --> 00:21:46,000
 But otherwise, yeah, just note it, and if you like it, say,

393
00:21:46,000 --> 00:21:46,000
 "Liking."

394
00:21:46,000 --> 00:21:48,000
 If you feel happy, say, "Happy."

395
00:21:48,000 --> 00:21:50,000
 Eventually, it'll go away.

396
00:21:50,000 --> 00:21:52,000
 It's not the path.

397
00:21:52,000 --> 00:21:55,000
 It's not the way to freedom from suffering.

398
00:21:55,000 --> 00:22:00,000
 It's just another experience that comes and goes.

399
00:22:00,000 --> 00:22:04,000
 It's impermanent, unsatisfying, uncontrollable.

400
00:22:04,000 --> 00:22:07,580
 When we meditate, we become very good at controlling

401
00:22:07,580 --> 00:22:08,000
 ourselves

402
00:22:08,000 --> 00:22:11,000
 and not overreacting to emotional situations.

403
00:22:11,000 --> 00:22:15,000
 But how do we know that we are not repressing emotions?

404
00:22:15,000 --> 00:22:19,000
 Yeah, so we don't try to control ourselves.

405
00:22:19,000 --> 00:22:21,000
 We try to learn about ourselves.

406
00:22:21,000 --> 00:22:24,390
 How we know is because we become more familiar with our

407
00:22:24,390 --> 00:22:25,000
 emotions.

408
00:22:25,000 --> 00:22:28,660
 And if anger were to arise or greed were to arise, we would

409
00:22:28,660 --> 00:22:30,000
 understand it.

410
00:22:30,000 --> 00:22:33,000
 We would not chase after it.

411
00:22:33,000 --> 00:22:35,750
 We would see it and say, "Oh, look, here's me getting angry

412
00:22:35,750 --> 00:22:36,000
,"

413
00:22:36,000 --> 00:22:39,330
 until eventually we just don't get angry because the idea

414
00:22:39,330 --> 00:22:42,000
 of getting angry is just silly.

415
00:22:42,000 --> 00:22:46,000
 We've seen that anger is useless.

416
00:22:46,000 --> 00:22:49,000
 We've seen it doesn't lead to any benefit.

417
00:22:49,000 --> 00:22:52,940
 We've stopped hankering after those things that we get

418
00:22:52,940 --> 00:22:57,000
 angry about or greedy about.

419
00:22:57,000 --> 00:23:00,000
 So if you're meditating to control yourself,

420
00:23:00,000 --> 00:23:04,000
 that's quite possibly just suppressing.

421
00:23:04,000 --> 00:23:07,000
 And you have to maybe... I mean, it's fine in the beginning

422
00:23:07,000 --> 00:23:07,000
,

423
00:23:07,000 --> 00:23:10,000
 but it's not sustainable in the long term.

424
00:23:10,000 --> 00:23:13,000
 Eventually you have to open up, let the emotions come up,

425
00:23:13,000 --> 00:23:19,000
 and learn about them, and let go of them.

426
00:23:19,000 --> 00:23:22,000
 Oh, I forgot about the task. Yes.

427
00:23:22,000 --> 00:23:26,000
 The task associated with the fourth noble truth.

428
00:23:26,000 --> 00:23:29,570
 So the task associated with the fourth noble truth is bhav

429
00:23:29,570 --> 00:23:33,000
ana, means development.

430
00:23:33,000 --> 00:23:39,000
 The fourth noble truth is to be developed, bhavitaba.

431
00:23:39,000 --> 00:23:42,140
 And once you have developed it, then you know it has been

432
00:23:42,140 --> 00:23:43,000
 developed.

433
00:23:43,000 --> 00:23:50,000
 Bhavitam. Thanks, Fernando. Forgot.

434
00:23:50,000 --> 00:23:56,440
 So the tasks in order then are... the first noble truth is

435
00:23:56,440 --> 00:23:57,000
 to be fully understood.

436
00:23:57,000 --> 00:24:00,000
 The second noble truth is to be abandoned.

437
00:24:00,000 --> 00:24:03,000
 The third noble truth is to be experienced for oneself.

438
00:24:03,000 --> 00:24:06,840
 And the fourth noble truth is to be cultivated, to be

439
00:24:06,840 --> 00:24:08,000
 developed.

440
00:24:08,000 --> 00:24:15,000
 The eightfold noble path is to be developed.

441
00:24:15,000 --> 00:24:17,000
 Okay.

442
00:24:17,000 --> 00:24:20,000
 That's all for tonight. Thank you for tuning in. Have a

443
00:24:20,000 --> 00:24:20,000
 good night.

