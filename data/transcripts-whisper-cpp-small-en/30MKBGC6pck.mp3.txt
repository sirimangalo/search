 Good evening everyone.
 Welcome to evening Dhamma.
 Today we're looking at purification through knowledge and
 vision of the practice.
 So to recap, we've just gone through the knowledge and
 vision discerning what is the path of
 practice and what is not.
 What is the path and what is not the path.
 So it's at this point that the meditator really begins to
 practice what we call vipassana.
 When we talk about vipassana we mean this portion of the
 practice.
 Vipassana as we understand it traditionally is completely
 mundane.
 By mundane we mean excluding nirvana.
 Realization of freedom from suffering is not vipassana.
 Vipassana is the practice that leads one to freedom from
 suffering.
 Freedom from suffering is in the final purification.
 That's the true purification really.
 This number six is still the preliminary path.
 Vipassana is still the preliminary path.
 So what does vipassana mean?
 Vipassana means to see clearly.
 Vipassana means to see or seeing, clear seeing.
 What are we trying to see clearly?
 We're trying to see clearly three things.
 Three things should be familiar to many of you.
 I talk about them hopefully quite a bit.
 Only because they really are the key and they really are
 the essence of the practice.
 But likely I don't talk about them enough.
 I can never really talk about them enough.
 Impermanence, suffering and non-self.
 We're practicing to see that everything inside of us and in
 the world around us is impermanent,
 unstable, unpredictable.
 We're practicing to see that everything inside of us and in
 the world around us, unsatisfying,
 stressful, a cause for suffering if you cling to it.
 And we're practicing to see that everything inside of us
 and in the world around us is
 not ours, is not me, is not mine.
 Doesn't belong to us, isn't a part of us.
 Isn't ourself.
 Three fairly weighty realizations.
 And they may seem actually somewhat simple at first glance
 but they have some fairly
 dramatic repercussions as a person will attest to is going
 through the insight meditation
 course.
 It has some profound impact on how we look at the world.
 First of all because if everything is impermanent then
 nothing is a refuge.
 Nothing can be your stable, secure refuge.
 It can't be there for you all the time.
 It's not something we can rely upon.
 Which may not sound too bad but it's quite disconcerting
 when you start to realize this
 is the nature of reality.
 We rely very much upon stability.
 We depend upon it.
 We live our lives trying to find it.
 We grow up thinking we have it in our parents, in our
 position in life, even in our bodies,
 in who we are.
 It's a very stable thing that we start out with until we
 start to experience suffering.
 We start to experience change and disappointment.
 We start to realize that the world isn't under our control.
 It isn't predictable.
 We're not sure what's going to happen next.
 This is where trauma comes from.
 People are traumatized because of the shock and the
 inability to deal with change.
 When I say change and I stick by it even suffering, great
 suffering is a sort of a change.
 If you suffer enough, long enough, it loses the shock value
.
 But shock is due to an inability to accept change.
 This leads to a sense of dread and the meditator begins to
 become quite uneasy about the things
 that we normally rely upon and think of as our refuge, our
 body, realizing that we can't
 even control our own bodies, our thoughts, our emotions,
 our experiences.
 Being first hand that everything ceases is quite a
 disturbing experience.
 It takes us out of our comfort zone.
 We begin to see not only that this is a sort of a scary
 state, but it's also quite unpleasant.
 The meditator begins to, as they progress through the
 practice, they begin to see the body and
 mind as not the friendly, comfortable things that one
 originally thought.
 The body begins to betray one.
 They say the body is the body betraying the meditator.
 The stomach won't rise and fall like it's supposed to.
 The pains and aches in this and that part of the body, not
 really working as it should.
 It feels kind of negative, unpleasant and the mind as well,
 this crazy mind.
 I thought it was our mind.
 I thought this was me.
 You start to see how this body and the mind are conspiring
 against us, all these random
 thoughts and images and songs running through our head,
 movies playing behind our eyes,
 memories we thought we'd long forgotten coming up again and
 again and again to annoy and distract
 us, worries and fears about the future, any number of
 things.
 We start to see that the mind is not really the refuge that
 we thought it was.
 We see a lot of suffering, a lot of suffering because we're
 clinging to the body and the
 mind, trying to make it stable, trying to bring it under
 control, expecting it to be
 like this, like that.
 Eventually the meditator becomes fed up with the body and
 the mind.
 It really takes patience because there's a lot of
 negativity generally associated with
 vipassana in science, a lot of negative experiences as we
 realize we're doing things wrong.
 It's negative because we're wrong.
 If we weren't wrong, we'd be enlightened.
 That's the whole point.
 Vipassana is to help us see what we don't see.
 There are repercussions to not seeing and that's what we
 start to learn.
 We start to see how not seeing that these things are imper
manent leads us to take impermanent
 things as permanent or stable.
 Not seeing that phenomena are stressful suffering and cause
 for suffering leads us to cling
 to them and think they're going to make us happy.
 Not seeing that phenomena are non-self leads us to take
 them as self, controllable, me,
 mine.
 All of this leads us to suffer when they aren't as we think
 they are.
 There's a lot of negativity involved because we're
 wrestling with our old wrong views and
 wrong ideas about reality, wrong perceptions about
 phenomena.
 Through patience the meditator begins to give things up,
 begins to let go, get fed up.
 No, I'm not going to cling to these things.
 These things aren't worth clinging to anymore.
 Search to recoil and in fact become repulsed by experiences
 thinking, "If only I could
 be free from all of this."
 Feeling trapped.
 At some point in the practice the meditator will feel
 trapped.
 Want to run away, sometimes meditators do run away.
 This isn't the same as running away a couple of days into
 the course.
 That happens because it's difficult.
 This is a person who is actually able to practice, who is
 broken through to be passing a practice
 and has really come to terms with a very deep problem
 inherent in samsara and will often
 run away because, not often but will sometimes run away
 because it's hard to admit that life
 is of this nature.
 It's easier to say, "Well this is just because I'm med
itating, right?
 This never happened to me before, it must be because I'm
 here in the meditation center."
 Often meditators who do leave will realize the mistake once
 they've left because they
 didn't leave their problems behind them in the monastery.
 They get to take all their problems with them.
 Come here and you think this is such a dreadful place full
 of problems.
 It's actually quite a happy place.
 People come with their problems but they have to take them
 home with them too.
 You don't get to leave your problems with us.
 We don't have any problems here.
 If the meditator sticks with it, they realize it's nothing
 to do with meditation practice
 or the meditation center or the meditation teacher.
 No matter how awful the teacher is, it's not his fault or
 her fault.
 It's the fault of samsara.
 It's the nature of samsara.
 It's our fault really for misconceiving samsara to be
 something it's not.
 Once the meditator reaches this point, then they're on the
 home stretch because this is
 when the real practice is way near the end of the vipassana
.
 They slowly work things out and then they finally get it.
 They say, "Look, this is the nature of samsara."
 Then they get really down to work.
 Then there's just this methodical going over and refining
 and saying, "No, no, see?
 Wrong.
 No, it's not satisfying.
 It's not stable.
 It's not controllable."
 With enough of that, enough of that, the meditator enters
 into the highest, the pinnacle of vipassana.
 Purification of the practice comes about when the meditator
 is finally objective.
 The meditator gets to the point where they see all
 phenomena as equal, simply arising
 and ceasing, no judgment.
 Pain isn't bad.
 Pleasure isn't good.
 Sights and sounds and smells and tastes and feelings and
 thoughts are all phenomena that
 arise and cease.
 It seems kind of insipid or dull or dreadful even.
 In fact, nothing can be further from the truth.
 It's at this point where the meditator is living, is here,
 is present, is no longer
 caught up in what it could to be, how it should be, how it
 was, how it will be.
 They're here and now.
 They're so alive and so free and so peaceful in the mind,
 so clear in the mind with a perfect
 vipassana practice that they can penetrate, penetrate this
 cloud of ignorance, see things
 as they truly are and become free from suffering.
 It's a very quick abbreviated version of the vipassana
 practice.
 There's a whole list of knowledges that I've just gone
 through fairly rapidly and purposely
 because learning about the knowledges can be quite
 detrimental.
 It's quite common for people to want to investigate and
 study and if they hear about this practice
 of vipassana, they immediately go and find all the theory
 and that can be quite to the
 detriment of the meditator.
 As I've said, they then expect or anticipate or
 intellectualize the practice.
 We've never gone through the course. It's hard to
 understand the difference between intelligence
 and wisdom, but there's a vast chasm separating the two.
 They're quite different.
 It's important that we never rely upon even teachings that
 I give that are intentionally
 based, focused around pushing people to practice, that we
 actually practice on our own.
 Otherwise we end up just paying lip service to the Buddhist
 teaching, talking about it,
 not actually tasting or experiencing it for ourselves.
 That's vipassana in a nutshell.
 Basically seeing the three characteristics. If it's very
 difficult and you're seeing everything
 fall apart, well, that's what's meant to happen.
 The idea is to be left standing when everything falls apart
, to not be shaken and have fallen
 apart.
 To rather be composed and to learn to extricate yourself,
 extricate, to pull yourself out
 of the meaning to not be dependent upon samsara.
 If it changes like this, I'll be happy.
 If it changes like this, I'll be unhappy.
 To be free from that, not have your happiness depend upon
 the vicissitudes of life, the
 changes.
 It's quite liberating when you no longer rely upon things
 that are unreliable.
 Try to find satisfaction in things that can't satisfy.
 Cling to things that aren't you, aren't yours, aren't under
 your control.
 So there you go.
 That's the demo for tonight.
 Thank you all for coming up.
 Let's see if we can get to the questions.
 Looks like we can, it's working.
 Yes, we've got a bunch of questions tonight.
 Is the mind mine?
 To me it seems to be just another form of existence like
 the body, just not physical.
 And then the ultimate question, if I am not my body and not
 my mind, who, what is observing
 all that is happening in the body and mind?
 The mind is not yours because you have to understand the
 difference between a entity
 based view of reality and an experiential based view of
 reality.
 If you look at reality from a point of view of entities,
 this room and everything in it,
 then of course I am, this is my computer, my microphone, my
 mind.
 But these are concepts, they're just entities.
 It's not really, it doesn't have any bearing on experience.
 In terms of experience, there is no me, there is no mine.
 It just doesn't, these are not concepts that play any role
 in experience.
 When we talk about things not being me and mine, we mean
 that experience doesn't work
 in terms of ownership.
 You can't own an experience.
 Experiences don't have existence independent of the chain
 of experience.
 They're a part of a causal chain.
 So when you talk about the mind, it doesn't actually exist.
 There's no such thing as the mind from a point of view of
 experience.
 There's only moments of awareness.
 And those moments of awareness are aware of things or have
 an object.
 So there's the awareness and there's the object of the
 awareness that are a pair.
 And the awareness and the object both arise and cease
 momentarily.
 So these questions only have bearing in regards to this
 conceptual view of reality, which
 is not how Buddhism looks at the world because it's not
 useful.
 It's practically useful.
 If we go to the store, it's useful to know that we're
 talking to the clerk and so on.
 When we go to school, talking to the teacher, knowing the
 people and places and concepts,
 getting on the bus, eating food, it's good to know what
 entities are important, but only
 conceptually, only in a mundane sense.
 In terms of becoming free from suffering and leaving,
 freeing ourselves from suffering,
 it's not useful.
 These things don't actually play a part in that experience.
 Is it rude to ask advanced practitioners about their
 personal attainments or stage of the
 path?
 I don't know about rude, but it just doesn't know where you
 are and if so obviously forget
 it.
 Knowing how far along someone is could help you decide.
 Someone who's been practicing for 30 years and hasn't got
 anywhere, then maybe I may
 want to look elsewhere.
 I understand.
 I mean, rude is an interesting word.
 I don't really know how to respond to that.
 Rude to me signifies a person who has a disregard for
 another person's well-being, so there's
 usually anger or ignorance involved.
 So it has nothing to do with the actual act and everything
 to do with one state of mind.
 Then Western people can be quite rude in that they have
 expectations that don't have any
 consideration for the other person.
 I think that is a problem.
 It's a problem not a Western problem.
 It's a problem with people who lack culture, I think in
 general.
 I have a lot to say bad about culture and how it can be
 overbearing, but it often teaches
 you to be considerate of each other.
 It doesn't, but there is a consideration culture that is
 often lacking in modern society.
 If you look on the internet, there's an incredible lack of
 consideration.
 It's funny, in Thailand, I've heard many people say, "The
 problem with Western is you don't
 have a word for 'geng jai'.
 They have this word 'geng jai'.
 My uncle, who's lived there for many years, said to me, "
Yeah, in English there is no
 word for 'geng jai'."
 I said, "Of course there is."
 I thought about it myself.
 I heard this and I said, "Of course there is.
 We just don't use it that much.
 We're just not very considerate."
 Often.
 It's not true.
 There are many considerate people outside of Asia.
 It's a problem with modern society and certainly with the
 internet culture.
 People want to rebel against this need to be considerate of
 other people's feelings.
 There's this insult that people use, "SJW, social justice
 warrior."
 I've talked about this because to me, fighting in a sense,
 fighting, I mean, warrior is obviously
 a problem, but fighting for social justice can be a very
 good thing.
 Most people are not, there are people who are very militant
 and angry about it, but
 for many people it's just about being considerate.
 Someone has this outlook on life or this way of looking,
 way of seeing, and you disagree
 with it, but you're considerate of them.
 You don't purposefully trod on the things that they are
 sensitive to, thinking, "Oh,
 they should just grow up and stop being so sensitive."
 We should rather learn to be sensitive, compassionate,
 understanding.
 If for no other reason than because it's a much more
 profitable means of bringing people
 out of their sensitivity or their oversensitivity.
 Consideration is, I think, very important and rudeness, I
 think, is a sign of low class
 and a sign of laziness in a sense, a person who just can't
 be bothered to actually take
 the time to understand the nuances of interpersonal
 relationships and the delicacy that goes along
 with trying to help people become to heal and to become
 more stronger and so on.
 I think if anyone understands that, the meditation teacher
 understands that because you're constantly
 trying to help people and you can't help everyone, but you
 certainly don't help people by being
 rude and inconsiderate.
 It's not the question you asked.
 So I can't really answer in general whether it's rude, but
 I certainly can't answer these
 questions myself and I think it often could be rude and
 inconsiderate by people who just
 don't consider why it might be wrong.
 I mean, the fact that you have to ask, maybe you have to
 ask yourself, "Why would it be
 rude and inconsiderate?"
 If you have to ask me, I mean, it's not me who's going to
 ... I guess this is the thing
 is that rudeness isn't because I say rude of you to ask
 that.
 It's nothing to do with it.
 This is what people rebel against because often they say, "
No, no, that's wrong.
 This is wrong," and they start to see how ridiculous it is.
 Like in Asia, it's quite like that in many cases.
 So many things, I watch the Thai lay people show the
 Western lay people, "Don't do that.
 That's rude.
 Don't do this.
 That's rude."
 It's funny because it's not really rude and it's kind of
 rude to just push people.
 "Okay, now you have to come and bow before the monk."
 I mean, how rude is that?
 I don't want people to bow.
 It's not improper to tell someone you have to bow when they
're not even Buddhist, for
 example.
 But the point being that if it's really rude, you will know
 for yourself.
 You have to ask yourself, "Is it rude?
 Am I being rude here?"
 If you don't know, then I can't tell you because you have
 to learn how to be considerate, how
 to understand the other person's feeling.
 How are they going to feel about this?
 So I think there's a difference between culturally
 inappropriate, I suppose, and actually rude
 because there are, of course, cultural norms.
 You can't go to Thailand and just pat a monk on the head.
 That would be very culturally inappropriate.
 It might not be rude.
 Although it's probably a bit condescending, but patting
 people on the head in Thailand
 if you didn't know is very, very culturally inappropriate
 and considered to be rude.
 It's not rude, though.
 It's not necessarily rude at all.
 Pointing to someone with your foot.
 It's a big no-no.
 I remember picking up a pen off the floor with my foot.
 One of the Thai monks said, "Well, Western people's hands."
 He was calling me a monkey, basically, saying I was using
 my feet as a hand.
 Think of us as boars.
 It's important because in the tropics, there's a lot of
 bacteria, so actually using your
 feet to pick things up is a very bad idea because your feet
 tend to be attractors of
 a lot of diseases and bacteria.
 There's reason for it.
 I can't answer these questions because I'm a monk, and it's
 nothing to do with being
 rude.
 It has to do with the fact that it's easily exploited.
 If monks started to brag about their attainments, then it
 easily gets out of hand because people
 want to support the monks when we rely upon laypeople for
 support, and then people support
 those monks who are attained, and then those monks who aren
't start to lie about it, etc.,
 etc.
 Those monks who are becoming gautistical about their attain
ments, etc., etc., so it's a big
 rule.
 It's an important rule that we don't break.
 As for non-monks, to answer your question, I can see how it
 can be useful, but I don't
 think it's as useful as people think it to be useful.
 My teacher is clearly a very powerful person, and many
 people think he's an arahant, and
 so on, but I don't see that helping a lot of people.
 That knowledge doesn't really ... I mean, yes, it brings
 people to practice.
 For sure, it brings people to practice at his center, but
 the simple knowledge that
 he's this or that, or he's this or that, attainment, no one
 really knows because he doesn't say
 ... He doesn't bring people.
 What brings people is who he is, what people know about him
, how they feel about him, how
 they feel when they're in his presence, and that doesn't
 take any admission.
 But even still, he always says, "Going to see enlightened
 people or being in their presence
 has nothing on actually practicing."
 The only reason why you would take up a practice is because
 someone else has attained something
 from it, or says they've attained something from it.
 I'd say you're barking up the wrong tree.
 You really haven't any reason to continue that practice,
 first of all, because the person
 could easily be lying or overestimating their own practice,
 but second of all, because it's
 meaningless to you.
 It doesn't actually psychologically do it.
 Not nearly in the same way that actually practicing and
 realizing that the practice is beneficial.
 If you think about it, you don't need that verification.
 All you need is to sit down and practice and say to
 yourself, "Whoa, this practice is really
 where it's at," or, "This practice is useless either way."
 If it's taking you a long time to come to either of those
 conclusions, then well, probably
 it's the latter and it's useless.
 This shouldn't take a long time for you to see the efficacy
 of it.
 "I keep having trouble keeping my back straight.
 I sit straight and then I start to slump down.
 Most of the sitting meditation in between I do the mantras.
 My lower back probably isn't strong enough."
 No, you don't need to have a straight back.
 If you're slouching, that's fine.
 It is a concentration problem.
 With certain types of meditation, you have a great amount
 of power because you're very
 focused on one object.
 You can sit perfectly straight.
 Insight meditation of this sort isn't like that because you
're letting your mind and
 body ebb and flow.
 You won't have the power of concentration, at least not in
 the beginning.
 It's needed to keep a perfectly erect body.
 It's something I wouldn't worry about.
 The Buddhists view the concept of desert.
 I think you mean dessert.
 The idea of who deserves what, similar to that of merit, be
 considered as delusional.
 When they think such a view is delusional because it's
 sustained on some.
 I don't understand.
 Dessert.
 A person getting what they deserve because it involves an
 enduring ego.
 The idea that the person in the past ... It's just an
 inaccurate ... It's just an imprecision.
 It's not actually wrong.
 You say, "Look, this person is sick.
 They're getting what they deserve."
 Usually it doesn't happen in this life.
 But it can.
 Quite simply, if you kill someone, the police are going to
 come after you.
 That's cause and effect.
 You're going to feel a lot of fear.
 You might even get physically sick.
 Nosebleeds, that kind of thing.
 There's lots of problems that come from being an evil, evil
 person.
 To say that person got their just desserts, it's just an
 imprecise way of saying, "This
 cause led to that effect."
 I mean, yes, it's true that the person doesn't exist, but
 in a conventional sense, that person
 got their just desserts.
 I mean, we all do eventually.
 More or less.
 It's a bit more complicated than that.
 But generally speaking, we get our just desserts.
 Long questions.
 Rather suspicious of long questions.
 Sending comments on what order is it best to delve deeper.
 No, you can't.
 Reading texts is not a supplement or is not a replacement
 for actual practice.
 You say, "There are no meditation centers of this tradition
 next to my area."
 Tough.
 I mean, studying books, it can potentially.
 No, I mean, tough.
 I mean, it's an awful thing to say.
 Why I say it is because you have to find one.
 Maybe you have to leave your area.
 Yeah, that might be impractical now, but there is no real
 practical alternative.
 Reading books isn't going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 It's not going to do it for you.
 I really recommend reading for those people who are also
 practicing.
 I've set up, if you've gotten this far, I'd recommend maybe
 doing an online course.
 We've set up this online course system whereby you can at
 least have a weekly meeting with
 a teacher.
 So you're welcome to take part in that.
 But I guess my point is I wouldn't worry too much about
 going deeper.
 You can't replace practice with a manual on the practice.
 It just doesn't work that way.
 The manual on the practice can't supplement a teacher.
 A teacher keeps many things that are in the manuals from
 the students purposefully because
 it would be putting the cart before the horse and giving
 the person the information before
 they understand it, before they're ready to accept it and
 to understand the implications
 of it.
 You can be persistent.
 If you're persistent, I would say read the Buddha's
 original teachings because his teachings
 are purposefully fairly general.
 These later manuals, they go into quite detail.
 But the Buddha, when he actually taught students, a lot
 like, well, I mean, quite general.
 So read his teachings and he will encourage you to practice
.
 We're going to do sitting meditation.
 Very concentrated.
 Feel the need to swallow.
 But when I'm not mindful, I swallow automatically.
 Because my eyes feel dry.
 Mindfully blink and mindfully swallow.
 Yeah, that's correct.
 How do you see dependent origination fitting into the
 material world?
 What does each nidhana actually pertain to?
 And after death and before becoming?
 I don't know.
 I don't really understand what you're asking.
 I have a few theories regarding the stages of development
 of the Vedas.
 Well, that's not the way I roll.
 Mahasya Sayadaw has some theories.
 No, the Buddha had some theories.
 And if you want to learn the Buddha's theories, I really
 recommend you read Mahasya Sayadaw's
 book on dependent origination.
 But if you got your own theories, empower to you.
 But I'm not going to get caught up in that.
 I'm a very much an orthodox sort of person.
 I follow the theories that people wiser than me have given.
 Because I agree with them, I suppose.
 But also just because I'm in awe at the fact that they are
 able to explain these.
 I don't have any bone of contention with them.
 So I don't have my own theories.
 And if I ever did, I would be rather suspicious of them and
 wonder where I was getting these
 theories from that were different from people who are
 clearly, clearly wiser than me.
 I'm not going to poke at you.
 I'm sorry, that might sound a bit like a poke.
 It's more of a reflection on my part.
 And I'm sorry if it sounds like I'm saying, "Hey, you're
 not very wise."
 And people like me or people like Mahasya Sayadaw are wiser
.
 It's totally up to you.
 If you have your own theories, power to you.
 I don't...
 I'm not really into alternative theories.
 I may have a little bit misunderstood what you're saying.
 I just don't get how... what the question about how it fits
 in with the physical world.
 I mean, "Buddhicha Samupada" is a thing.
 It has one interpretation or, well, potentially two, this
 life or past lives.
 But it still is the only... it's quite simple and clearly
 spelled out.
 It's not simple.
 It's actually quite complicated, but...
 Mosquito season.
 Go away.
 I caught a mosquito.
 Did the Buddha ever describe his specific meditation
 techniques?
 Well, he talked a lot about "Ana Pana Sati" and mindfulness
 of death, mindfulness of the
 body, that kind of thing.
 So yeah, he did.
 We don't have a lot of it in the suttas.
 It's a lot more general talks.
 So we don't know whether there's any extra scriptural
 teachings the Buddha gave on actual
 mechanics of the practice.
 We kind of have the idea that way back then people were
 just so good at meditation, you
 didn't have to go into detail.
 Whether that was true or not, I don't know.
 Probably not.
 But we hear a lot of stories about how the Buddha and his
 disciples would teach people
 and spend lots of time teaching them actual meditation
 techniques.
 So it's not all written down in the suttas, I think.
 Okay, I've got to go free this mosquito.
 That's all the questions.
 Thank you all for coming out.
 Have a good night.
 [
 1
 1
 2
 3
 4
 5
 6
 7
 8
 9
 9
 10
 11
 12
 13
 14
 15
 16
 17
 18
 19
 19
 20
 21
 22
 23
 24
 25
 26
 28
 29
 30
 31
 32
 33
 34
 35
 36
 37
 38
 39
 40
 41
 42
 43
