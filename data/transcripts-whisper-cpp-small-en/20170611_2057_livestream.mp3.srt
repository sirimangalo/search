1
00:00:00,000 --> 00:00:09,120
 Okay, good evening everyone.

2
00:00:09,120 --> 00:00:13,200
 Welcome to our evening, Dhamma.

3
00:00:13,200 --> 00:00:22,200
 Hmm, that didn't work.

4
00:00:22,200 --> 00:00:29,000
 Okay, trying again.

5
00:00:29,000 --> 00:00:38,200
 Looks good.

6
00:00:38,200 --> 00:00:46,360
 So tonight I was thinking about a verse, a Dhamma-Pandha

7
00:00:46,360 --> 00:00:49,000
 verse, I think.

8
00:00:49,000 --> 00:00:54,000
 Arogya paramalabha.

9
00:00:54,000 --> 00:00:56,040
 Freedom from sickness.

10
00:00:56,040 --> 00:01:03,040
 Freedom from illness is the greatest gain.

11
00:01:03,040 --> 00:01:08,040
 Santuti paramang dhanang.

12
00:01:08,040 --> 00:01:11,040
 Contentment is the greatest wealth.

13
00:01:11,040 --> 00:01:23,510
 Wissasa paramang yati, paramang yati, the familiarity is

14
00:01:23,510 --> 00:01:29,040
 the greatest relation.

15
00:01:29,040 --> 00:01:41,040
 Nibaanang paramang sukang, Nibaan is the highest happiness.

16
00:01:41,040 --> 00:01:46,040
 I'm thinking about this verse.

17
00:01:46,040 --> 00:01:49,040
 You can really get a sense of the Buddha saying this,

18
00:01:49,040 --> 00:01:55,040
 looking out on the community of monks living in the forest,

19
00:01:55,040 --> 00:02:00,040
 reflecting on all that he'd been through to become a Buddha

20
00:02:00,040 --> 00:02:00,040
,

21
00:02:00,040 --> 00:02:18,140
 and expressing his perception, his outlook on the world in

22
00:02:18,140 --> 00:02:20,040
 this way.

23
00:02:20,040 --> 00:02:25,040
 Arogya means not having roga. Arogya means that which ruch

24
00:02:25,040 --> 00:02:25,040
ati,

25
00:02:25,040 --> 00:02:35,040
 that which disturbs you, afflicts you, affliction.

26
00:02:35,040 --> 00:02:45,040
 It's the greatest gain, the greatest possession.

27
00:02:45,040 --> 00:02:53,040
 Greatest gain, actually. Laba means gain.

28
00:02:53,040 --> 00:02:57,040
 Ask anyone who's physically sick, right? Physically ill.

29
00:02:57,040 --> 00:03:01,040
 Do they want money? No.

30
00:03:01,040 --> 00:03:11,040
 Fame, glory, mansion, power.

31
00:03:11,040 --> 00:03:14,160
 All the riches in all the world won't save you from

32
00:03:14,160 --> 00:03:15,040
 sickness.

33
00:03:15,040 --> 00:03:18,040
 Four kinds of sickness in Buddhism.

34
00:03:18,040 --> 00:03:22,040
 There's sickness that comes from food,

35
00:03:22,040 --> 00:03:27,040
 sickness that comes from the environment,

36
00:03:27,040 --> 00:03:35,290
 sickness that comes from karma, and sickness that comes

37
00:03:35,290 --> 00:03:38,040
 from the mind.

38
00:03:38,040 --> 00:03:41,410
 So actually it's quite likely that the Buddha wasn't

39
00:03:41,410 --> 00:03:42,040
 thinking of

40
00:03:42,040 --> 00:03:46,040
 physical illness when he said this.

41
00:03:46,040 --> 00:03:51,040
 I think we can all agree that if you're physically unwell,

42
00:03:51,040 --> 00:03:55,230
 all the material possessions in the world are not going to

43
00:03:55,230 --> 00:04:01,040
 compare to

44
00:04:01,040 --> 00:04:07,040
 the gain of becoming free from that sickness.

45
00:04:07,040 --> 00:04:10,560
 We spend a lot of money and a lot of effort just to free

46
00:04:10,560 --> 00:04:14,040
 ourselves from illness.

47
00:04:14,040 --> 00:04:16,770
 Everything else becomes meaningless because you can't of

48
00:04:16,770 --> 00:04:18,040
 course enjoy

49
00:04:18,040 --> 00:04:21,860
 any of the pleasures or riches or gains, accomplishments in

50
00:04:21,860 --> 00:04:22,040
 life,

51
00:04:22,040 --> 00:04:25,040
 if you're physically ill.

52
00:04:25,040 --> 00:04:29,450
 But I don't think that was exactly what the Buddha was

53
00:04:29,450 --> 00:04:31,040
 talking about.

54
00:04:31,040 --> 00:04:36,790
 It actually isn't the greatest gain, not in the Buddhist

55
00:04:36,790 --> 00:04:37,040
 sense,

56
00:04:37,040 --> 00:04:41,040
 because you have to understand sickness of the mind

57
00:04:41,040 --> 00:04:46,040
 and to understand the full range of sickness.

58
00:04:46,040 --> 00:04:51,870
 A person can be perfectly healthy in the body, but if their

59
00:04:51,870 --> 00:04:54,040
 mind is ill,

60
00:04:54,040 --> 00:04:59,070
 the physical body, the physical health does them no good

61
00:04:59,070 --> 00:05:00,040
 either.

62
00:05:00,040 --> 00:05:07,290
 So sickness from food, sickness from the environment, these

63
00:05:07,290 --> 00:05:10,040
 are physical.

64
00:05:10,040 --> 00:05:14,940
 Sickness from karma, well that can be physical, can be

65
00:05:14,940 --> 00:05:16,040
 mental.

66
00:05:16,040 --> 00:05:20,050
 But sometimes we're born with diseases, with illnesses,

67
00:05:20,050 --> 00:05:21,040
 physical illnesses.

68
00:05:21,040 --> 00:05:28,040
 Some people are born deficient in some way in the body.

69
00:05:28,040 --> 00:05:35,040
 Some people are born with all kinds of illness.

70
00:05:35,040 --> 00:05:39,040
 Some people die, some humans die shortly after childbirth

71
00:05:39,040 --> 00:05:44,040
 because they're so ill.

72
00:05:44,040 --> 00:05:47,040
 This can be because of karma.

73
00:05:47,040 --> 00:05:50,040
 I mean sickness because of food is quite obvious.

74
00:05:50,040 --> 00:05:53,670
 You eat too much, you eat the wrong kinds of food, this is

75
00:05:53,670 --> 00:05:55,040
 quite clear.

76
00:05:55,040 --> 00:06:00,270
 The environment could be anything from bacteria, viruses,

77
00:06:00,270 --> 00:06:04,040
 infections,

78
00:06:04,040 --> 00:06:08,040
 radiation perhaps, chemicals.

79
00:06:08,040 --> 00:06:12,690
 Now everything's got these terrible chemicals in it, making

80
00:06:12,690 --> 00:06:16,040
 us all sick.

81
00:06:16,040 --> 00:06:19,040
 But karma, karma, that one's not so clear.

82
00:06:19,040 --> 00:06:23,040
 Karma is the Buddhist answer to why we're born certain ways

83
00:06:23,040 --> 00:06:23,040
.

84
00:06:23,040 --> 00:06:27,040
 In fact not even karma, not just karma, right?

85
00:06:27,040 --> 00:06:30,590
 Because while the environment can have a factor, play a

86
00:06:30,590 --> 00:06:31,040
 part,

87
00:06:31,040 --> 00:06:36,710
 karma certainly plays a part but it's much more complicated

88
00:06:36,710 --> 00:06:38,040
 than that.

89
00:06:38,040 --> 00:06:43,240
 Sometimes we perform certain karmas and as a byproduct bad

90
00:06:43,240 --> 00:06:44,040
 or good things happen

91
00:06:44,040 --> 00:06:48,420
 that are totally, I mean things that we like but totally

92
00:06:48,420 --> 00:06:51,040
 have nothing to do with the karma.

93
00:06:51,040 --> 00:06:53,040
 Right?

94
00:06:53,040 --> 00:06:59,040
 Like perhaps you did some very bad things and as a result

95
00:06:59,040 --> 00:07:02,040
 you have this bad karma in your life

96
00:07:02,040 --> 00:07:09,040
 but as a result like let's say a person who is very poor

97
00:07:09,040 --> 00:07:15,040
 and so they have to go and live off in the countryside

98
00:07:15,040 --> 00:07:18,040
 and then there's a fire that burns down the city, right?

99
00:07:18,040 --> 00:07:22,040
 I mean just the point is complicated is the point.

100
00:07:22,040 --> 00:07:28,330
 But karma certainly plays a part in this life and from life

101
00:07:28,330 --> 00:07:32,040
 to life as an extension.

102
00:07:32,040 --> 00:07:35,040
 But it's, I think we all...

