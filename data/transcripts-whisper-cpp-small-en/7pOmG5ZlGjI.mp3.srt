1
00:00:00,000 --> 00:00:03,480
 During meditation I often notice myself thinking about

2
00:00:03,480 --> 00:00:07,720
 memories or events that have happened or things I

3
00:00:07,720 --> 00:00:10,560
 have to do or want to do.

4
00:00:10,560 --> 00:00:14,520
 Is it best to pursue the thoughts while acknowledging them?

5
00:00:24,440 --> 00:00:27,440
 You don't gain so much even if it's something that you have

6
00:00:27,440 --> 00:00:30,640
 to do. You really don't gain much from thinking about it.

7
00:00:30,640 --> 00:00:36,340
 It's best not to pursue the thoughts. It's best to let the

8
00:00:36,340 --> 00:00:39,680
 thoughts come and then be mindful of them. That's the best

9
00:00:39,680 --> 00:00:40,240
 you can do.

10
00:00:40,240 --> 00:00:46,680
 It will change your life. It will change your course. It

11
00:00:46,680 --> 00:00:51,000
 will change the course of your work and so on.

12
00:00:54,120 --> 00:00:56,320
 It will help you in many ways.

13
00:00:56,320 --> 00:01:02,720
 Certainly the things that have happened before

14
00:01:02,720 --> 00:01:06,040
 there's not much benefit that comes from there's very

15
00:01:06,040 --> 00:01:09,280
 little benefit and much potential detriment to

16
00:01:09,280 --> 00:01:12,400
 thinking too much about them.

17
00:01:12,400 --> 00:01:14,880
 So it's certainly best not to pursue the thoughts.

18
00:01:14,880 --> 00:01:18,880
 And not much benefit ever comes from pursuing thoughts.

19
00:01:21,400 --> 00:01:24,080
 Thoughts are best left alone, left to come up,

20
00:01:24,080 --> 00:01:27,280
 acknowledged and

21
00:01:27,280 --> 00:01:33,220
 let them go without allowing engraving and the desire to

22
00:01:33,220 --> 00:01:33,840
 continue.

23
00:01:33,840 --> 00:01:41,090
 Even in a work sense, I don't see much benefit that comes

24
00:01:41,090 --> 00:01:42,960
 from thinking and planning and so on.

25
00:01:42,960 --> 00:01:45,980
 It may seem that sometimes you do have to plan but the best

26
00:01:45,980 --> 00:01:49,360
 work doesn't come from planning. It comes from mindful

27
00:01:49,360 --> 00:01:51,360
 processing.

28
00:01:51,360 --> 00:01:56,870
 So letting the thoughts come up and then acting on them or

29
00:01:56,870 --> 00:01:58,280
 writing them down or

30
00:01:58,280 --> 00:02:01,400
 working them out and they should just come by themselves

31
00:02:01,400 --> 00:02:04,600
 without any pursuit or any worrying or any

32
00:02:04,600 --> 00:02:07,400
 cultivating.

