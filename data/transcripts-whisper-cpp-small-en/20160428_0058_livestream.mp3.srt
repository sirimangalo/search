1
00:00:00,000 --> 00:00:20,560
 Good evening everyone.

2
00:00:20,560 --> 00:00:33,360
 It's something live, April 27, 2016.

3
00:00:33,360 --> 00:00:40,080
 Tonight's quote from the Sanyutta Nikaya is again not the

4
00:00:40,080 --> 00:00:42,400
 Buddha himself.

5
00:00:42,400 --> 00:00:49,680
 This is a man named Jitta.

6
00:00:49,680 --> 00:00:56,560
 Jitta was a rather remarkable person, not a monk.

7
00:00:56,560 --> 00:01:06,630
 He was a rich man and he was declared by the Buddha to be

8
00:01:06,630 --> 00:01:09,440
 preeminent among laymen who preached

9
00:01:09,440 --> 00:01:14,160
 the Dhamma.

10
00:01:14,160 --> 00:01:19,860
 And so there's a whole section of the Jitta Sanyutta is a

11
00:01:19,860 --> 00:01:22,680
 record of conversations between

12
00:01:22,680 --> 00:01:25,800
 him and monks.

13
00:01:25,800 --> 00:01:33,930
 So he actually cleared up some debates between the monks

14
00:01:33,930 --> 00:01:38,200
 and that's what this quote doesn't

15
00:01:38,200 --> 00:01:41,600
 actually show.

16
00:01:41,600 --> 00:01:51,760
 So the quote is giving an example and it sounds like he's

17
00:01:51,760 --> 00:01:53,240
 repeating something that the monks

18
00:01:53,240 --> 00:01:55,240
 already know.

19
00:01:55,240 --> 00:02:00,380
 But the truth is that the monks were divided, these elder

20
00:02:00,380 --> 00:02:01,960
 monks in fact.

21
00:02:01,960 --> 00:02:07,780
 And the question is actually an interesting question for

22
00:02:07,780 --> 00:02:09,440
 meditators.

23
00:02:09,440 --> 00:02:17,840
 Question is whether, let me read the Pali.

24
00:02:17,840 --> 00:02:25,110
 Sangyo jananthiwa, I will show some, Sangyo janiya dhamma t

25
00:02:25,110 --> 00:02:26,240
ihwa.

26
00:02:26,240 --> 00:02:38,310
 Sangyo jana, a fetter or a bond or a bind and the dhammas

27
00:02:38,310 --> 00:02:42,640
 which are bound.

28
00:02:42,640 --> 00:02:51,080
 The dhammas which are joined, conjoined.

29
00:02:51,080 --> 00:03:12,280
 Are these two things, are these two concepts?

30
00:03:12,280 --> 00:03:23,800
 Are they one in, are they separate and different in meaning

31
00:03:23,800 --> 00:03:30,400
 and in letter, in name or are they

32
00:03:30,400 --> 00:03:38,920
 one in meaning and different in letter or by name?

33
00:03:38,920 --> 00:03:45,700
 So the meaning is you've got the bond, the fetter, that

34
00:03:45,700 --> 00:03:49,320
 which binds things and then you've

35
00:03:49,320 --> 00:03:51,840
 got the things that are bound together.

36
00:03:51,840 --> 00:03:54,580
 And the question is, it's quite a deep question actually,

37
00:03:54,580 --> 00:03:56,220
 the question is whether these are

38
00:03:56,220 --> 00:03:59,800
 one in the same and just different in name.

39
00:03:59,800 --> 00:04:02,370
 And some of the elders thought that the things that were

40
00:04:02,370 --> 00:04:04,000
 bound together, the things that

41
00:04:04,000 --> 00:04:09,920
 are bound together are actually the bind and some thought

42
00:04:09,920 --> 00:04:12,320
 they were separate.

43
00:04:12,320 --> 00:04:15,940
 And then Jitta heard about this and so he went to see the

44
00:04:15,940 --> 00:04:17,840
 monks and he said, "Sutta

45
00:04:17,840 --> 00:04:26,210
 metang bande, I've heard that many monks are arguing about

46
00:04:26,210 --> 00:04:30,880
 this or split on this matter."

47
00:04:30,880 --> 00:04:39,240
 And he says, they say, "Ewang gahapati," yes, householder,

48
00:04:39,240 --> 00:04:40,720
 it is so.

49
00:04:40,720 --> 00:04:43,160
 And Jitta says that they're separate.

50
00:04:43,160 --> 00:04:48,040
 He says, "Nana ta ji wa naan bhi anjana," they're different

51
00:04:48,040 --> 00:04:50,120
 in letter and different

52
00:04:50,120 --> 00:04:54,080
 in meaning.

53
00:04:54,080 --> 00:04:59,670
 And then he gives this simile of a black cow and a white

54
00:04:59,670 --> 00:05:02,800
 cow and they're tied together

55
00:05:02,800 --> 00:05:06,280
 by a rope.

56
00:05:06,280 --> 00:05:12,130
 And when the white cow pulls, the black cow has to go as

57
00:05:12,130 --> 00:05:13,120
 well.

58
00:05:13,120 --> 00:05:16,010
 So the question is whether the black cow is fetter is

59
00:05:16,010 --> 00:05:17,980
 holding the white cow or the white

60
00:05:17,980 --> 00:05:20,360
 cow is holding the black cow.

61
00:05:20,360 --> 00:05:24,260
 And the monks say, "Well, no, the rope is holding them both

62
00:05:24,260 --> 00:05:24,640
."

63
00:05:24,640 --> 00:05:28,800
 And he says, "Just the same.

64
00:05:28,800 --> 00:05:34,660
 The eye is not a fetter, lights, visions are not a fetter

65
00:05:34,660 --> 00:05:37,680
 for the eye, nor is the eye a

66
00:05:37,680 --> 00:05:40,760
 fetter for visions."

67
00:05:40,760 --> 00:05:46,040
 So it's not because we see things, just seeing things is

68
00:05:46,040 --> 00:05:49,240
 not a fetter, it's not getting caught

69
00:05:49,240 --> 00:05:51,120
 up.

70
00:05:51,120 --> 00:05:53,240
 You don't get attached just because you see basically.

71
00:05:53,240 --> 00:05:57,000
 I mean, this is very deep and sort of, these are the kind

72
00:05:57,000 --> 00:05:59,440
 of things that they would debate.

73
00:05:59,440 --> 00:06:04,380
 And it goes to show how deep was their thought process,

74
00:06:04,380 --> 00:06:07,520
 what they were thinking about.

75
00:06:07,520 --> 00:06:12,600
 And here when you hear something, it's not the sound that

76
00:06:12,600 --> 00:06:15,320
 is the bond, the clinging isn't

77
00:06:15,320 --> 00:06:18,000
 in the sound.

78
00:06:18,000 --> 00:06:23,430
 When you smell, taste, feel, think, none of these are the

79
00:06:23,430 --> 00:06:24,560
 problem.

80
00:06:24,560 --> 00:06:28,340
 Very simply, it's basically what we talk about in

81
00:06:28,340 --> 00:06:29,560
 meditation.

82
00:06:29,560 --> 00:06:31,910
 Just because you experience something, this isn't the

83
00:06:31,910 --> 00:06:32,440
 problem.

84
00:06:32,440 --> 00:06:36,290
 And when I was giving five minute meditation lessons, this

85
00:06:36,290 --> 00:06:37,840
 is how I started off.

86
00:06:37,840 --> 00:06:42,840
 I said, "This meditation is based on the premise that it's

87
00:06:42,840 --> 00:06:45,560
 not our experiences that cause us

88
00:06:45,560 --> 00:06:49,160
 suffering, it's our reactions to them."

89
00:06:49,160 --> 00:06:51,160
 So it's the craving.

90
00:06:51,160 --> 00:06:59,320
 He says, "Yen chattata tandumbayang paticha upachati chand

91
00:06:59,320 --> 00:07:03,200
arago dang tattasan yojana."

92
00:07:03,200 --> 00:07:11,390
 Whatever chandaraga, appreciation or desire and lust,

93
00:07:11,390 --> 00:07:15,560
 arises based on them both, based

94
00:07:15,560 --> 00:07:19,400
 on the paradigm.

95
00:07:19,400 --> 00:07:23,400
 That's what binds them.

96
00:07:23,400 --> 00:07:33,880
 That's the bind in this case.

97
00:07:33,880 --> 00:07:37,570
 Even when you feel pain, it's not the pain that's the

98
00:07:37,570 --> 00:07:38,480
 problem.

99
00:07:38,480 --> 00:07:40,760
 When someone's yelling at you, it's not the yelling that's

100
00:07:40,760 --> 00:07:41,440
 the problem.

101
00:07:41,440 --> 00:07:44,570
 When you think bad thoughts, the thoughts are not bad, they

102
00:07:44,570 --> 00:07:45,760
're just thoughts.

103
00:07:45,760 --> 00:07:48,400
 They're in fact quite neutral.

104
00:07:48,400 --> 00:07:52,690
 We once had a monk in Thailand, I don't know if I've

105
00:07:52,690 --> 00:07:56,600
 recently mentioned him, but I ... No,

106
00:07:56,600 --> 00:08:02,480
 it was in New York, I was just talking about him.

107
00:08:02,480 --> 00:08:09,880
 He told me, he was really crazy.

108
00:08:09,880 --> 00:08:13,490
 He thought we were all conspiring against him, it was

109
00:08:13,490 --> 00:08:14,680
 interesting.

110
00:08:14,680 --> 00:08:17,760
 He came up to me and said, they're talking about me over

111
00:08:17,760 --> 00:08:19,960
 the loudspeakers, they're saying

112
00:08:19,960 --> 00:08:20,960
 things.

113
00:08:20,960 --> 00:08:25,730
 I said, "Well, I really don't think," and he said, "Oh, you

114
00:08:25,730 --> 00:08:27,280
're in with them."

115
00:08:27,280 --> 00:08:31,780
 He really thought we were all conspiring to drive him crazy

116
00:08:31,780 --> 00:08:34,960
 at the monastery, it was quite

117
00:08:34,960 --> 00:08:35,960
 interesting.

118
00:08:35,960 --> 00:08:37,850
 But he wants to send to me, he said, "I'm having these

119
00:08:37,850 --> 00:08:38,480
 thoughts."

120
00:08:38,480 --> 00:08:40,080
 I said, "Well, they're just thoughts."

121
00:08:40,080 --> 00:08:43,160
 And he said, "Oh, no, they're just the most horrible

122
00:08:43,160 --> 00:08:45,240
 thoughts that you could possibly

123
00:08:45,240 --> 00:08:46,240
 have."

124
00:08:46,240 --> 00:08:49,840
 In the end, a thought is a thought.

125
00:08:49,840 --> 00:08:52,660
 And he drove himself crazy, ended up cutting his wrists and

126
00:08:52,660 --> 00:08:53,920
 lighting himself on fire, did

127
00:08:53,920 --> 00:08:58,960
 all sorts of crazy things, ended up disrobing.

128
00:08:58,960 --> 00:09:02,280
 But we do this, we all do this to some extent, we beat

129
00:09:02,280 --> 00:09:04,880
 ourselves up over our thoughts, don't

130
00:09:04,880 --> 00:09:06,640
 think that, can't think that.

131
00:09:06,640 --> 00:09:08,720
 I'm so evil for thinking that.

132
00:09:08,720 --> 00:09:11,840
 And actually you're not, the thoughts are just thoughts.

133
00:09:11,840 --> 00:09:19,080
 That's not where evil comes from.

134
00:09:19,080 --> 00:09:22,000
 So actually, Jitta was teaching the monks something here.

135
00:09:22,000 --> 00:09:26,250
 He was teaching them, and so they don't say, "Oh, very good

136
00:09:26,250 --> 00:09:28,720
, very good," like patronizing,

137
00:09:28,720 --> 00:09:34,020
 they say, "Wow, labhati gapati, su ladhanthi, it is a great

138
00:09:34,020 --> 00:09:36,880
 gain for you, householder."

139
00:09:36,880 --> 00:09:47,730
 That you have this deep, that you have the eye of wisdom,

140
00:09:47,730 --> 00:10:00,480
 that by you the eye of wisdom

141
00:10:00,480 --> 00:10:03,920
 goes to the deep.

142
00:10:03,920 --> 00:10:06,950
 You have a deep, you have a eye of wisdom in the deep

143
00:10:06,950 --> 00:10:09,320
 teaching of the Buddhas, deep words

144
00:10:09,320 --> 00:10:16,880
 of the Buddhas, something like that.

145
00:10:16,880 --> 00:10:23,520
 So yeah, the bondage, what is it that binds things then?

146
00:10:23,520 --> 00:10:29,280
 We start with, we start with ignorance.

147
00:10:29,280 --> 00:10:33,960
 And the base of all attachment is ignorance.

148
00:10:33,960 --> 00:10:35,400
 Just not seeing clearly.

149
00:10:35,400 --> 00:10:38,240
 Ignorance isn't something hard to understand.

150
00:10:38,240 --> 00:10:42,720
 Just means you didn't see it clearly enough.

151
00:10:42,720 --> 00:10:46,240
 And that leads to misconception.

152
00:10:46,240 --> 00:10:53,330
 It leads us to like things that are not worth liking, and

153
00:10:53,330 --> 00:10:57,520
 it leads to habits of preference,

154
00:10:57,520 --> 00:10:59,720
 because we're just guessing.

155
00:10:59,720 --> 00:11:04,700
 When we grow up, we do this even in this life to some

156
00:11:04,700 --> 00:11:05,720
 extent.

157
00:11:05,720 --> 00:11:11,190
 When you ask a kid which one they want, in many cases it's

158
00:11:11,190 --> 00:11:12,800
 all the same.

159
00:11:12,800 --> 00:11:16,020
 When you ask a meditator this, it's hard to ask them, "

160
00:11:16,020 --> 00:11:17,920
Would you like this or would you

161
00:11:17,920 --> 00:11:18,920
 like that?"

162
00:11:18,920 --> 00:11:21,970
 They're not really able to make a decision because there's

163
00:11:21,970 --> 00:11:23,360
 no reason to pick one over

164
00:11:23,360 --> 00:11:24,440
 the other.

165
00:11:24,440 --> 00:11:32,620
 But we do. We build up likes and dislikes, often just by

166
00:11:32,620 --> 00:11:33,880
 chance, you know?

167
00:11:33,880 --> 00:11:36,800
 Just because of that's how the chips fall.

168
00:11:36,800 --> 00:11:39,760
 That's where our life leads us.

169
00:11:39,760 --> 00:11:43,800
 We acquire tastes.

170
00:11:43,800 --> 00:11:48,260
 Some of it's genetic, some of it's organic, but some of it

171
00:11:48,260 --> 00:11:49,680
's just random.

172
00:11:49,680 --> 00:12:02,600
 And so then the ignorance is a breeding ground.

173
00:12:02,600 --> 00:12:05,760
 It's like darkness.

174
00:12:05,760 --> 00:12:12,040
 Ignorance is a direct parallel to the darkness.

175
00:12:12,040 --> 00:12:13,240
 It's mental darkness.

176
00:12:13,240 --> 00:12:18,940
 So if you think about darkness, not only is it impossible

177
00:12:18,940 --> 00:12:21,320
 to see what is right, but it's

178
00:12:21,320 --> 00:12:24,720
 a breeding ground of all sorts of things.

179
00:12:24,720 --> 00:12:28,200
 It's a breeding ground of bacteria.

180
00:12:28,200 --> 00:12:35,410
 It's a breeding ground of viruses and bacteria, rotten

181
00:12:35,410 --> 00:12:36,880
 things.

182
00:12:36,880 --> 00:12:38,960
 Things rot in the dark.

183
00:12:38,960 --> 00:12:45,880
 Mold grows, fungus grows in the dark.

184
00:12:45,880 --> 00:12:49,210
 And so all these rotten things in our minds grow based on

185
00:12:49,210 --> 00:12:50,360
 our ignorance.

186
00:12:50,360 --> 00:12:52,240
 And all it takes is to shine a light in.

187
00:12:52,240 --> 00:12:56,690
 When you shine a powerful light in, the darkness disappears

188
00:12:56,690 --> 00:12:59,040
 so you don't have to worry about

189
00:12:59,040 --> 00:13:02,600
 getting rid of the ignorance.

190
00:13:02,600 --> 00:13:06,600
 But the bad things start to shrivel up as well.

191
00:13:06,600 --> 00:13:13,950
 The rotten things start to dry up and wither away because

192
00:13:13,950 --> 00:13:18,920
 they rely on darkness for support.

193
00:13:18,920 --> 00:13:22,280
 So how do we shine this light in?

194
00:13:22,280 --> 00:13:25,450
 It's a little more complicated than just shining a light, a

195
00:13:25,450 --> 00:13:27,540
 little bit more complicated because

196
00:13:27,540 --> 00:13:30,320
 there's different aspects.

197
00:13:30,320 --> 00:13:36,580
 I did a video, if you know my video, on pornography and

198
00:13:36,580 --> 00:13:41,400
 masturbation, I think, and addiction in

199
00:13:41,400 --> 00:13:47,240
 general.

200
00:13:47,240 --> 00:13:55,250
 For a long time I had this concept of three things, three

201
00:13:55,250 --> 00:13:59,280
 aspects to an addiction.

202
00:13:59,280 --> 00:14:04,210
 The object, which is either seeing, hearing, smelling,

203
00:14:04,210 --> 00:14:07,720
 tasting, feeling, or thinking, a

204
00:14:07,720 --> 00:14:08,720
 thought.

205
00:14:08,720 --> 00:14:12,220
 It can be something you see that leads to desire, something

206
00:14:12,220 --> 00:14:13,720
 you hear, something you

207
00:14:13,720 --> 00:14:17,970
 smell, something you taste, something you feel, or

208
00:14:17,970 --> 00:14:19,920
 something you think.

209
00:14:19,920 --> 00:14:21,240
 That's the first thing.

210
00:14:21,240 --> 00:14:25,440
 The second thing is the pleasure that comes from it.

211
00:14:25,440 --> 00:14:28,840
 You could do the same with pain as well.

212
00:14:28,840 --> 00:14:31,200
 When you see something and it makes you happy, when you

213
00:14:31,200 --> 00:14:32,760
 hear something it makes you happy.

214
00:14:32,760 --> 00:14:34,920
 So there's that aspect of it.

215
00:14:34,920 --> 00:14:40,940
 And then the third is the desire, which is not the feeling,

216
00:14:40,940 --> 00:14:43,720
 but it's this attraction,

217
00:14:43,720 --> 00:14:52,390
 like a magnet, a clinging, like a pressure in the mind, the

218
00:14:52,390 --> 00:14:54,680
 stickiness.

219
00:14:54,680 --> 00:14:57,940
 And going back and forth between those three, this is what

220
00:14:57,940 --> 00:14:59,680
 I talked about in this video,

221
00:14:59,680 --> 00:15:01,520
 and I found this in the Buddha's teaching.

222
00:15:01,520 --> 00:15:05,540
 The Buddha said some teachers teach one or the other, but

223
00:15:05,540 --> 00:15:07,640
 the best teacher teaches all

224
00:15:07,640 --> 00:15:08,640
 three.

225
00:15:08,640 --> 00:15:14,980
 He teaches the base, teaches the feeling, and teaches the

226
00:15:14,980 --> 00:15:16,680
 attachment.

227
00:15:16,680 --> 00:15:19,720
 So this is how we deal with addiction specifically.

228
00:15:19,720 --> 00:15:25,590
 If you want to deal with your attachments, go back and

229
00:15:25,590 --> 00:15:28,960
 forth between these three.

230
00:15:28,960 --> 00:15:32,190
 Every time it comes up, just be methodical, systematic, and

231
00:15:32,190 --> 00:15:33,760
 you'll start to change those

232
00:15:33,760 --> 00:15:34,760
 habits.

233
00:15:34,760 --> 00:15:36,960
 You'll start to override this.

234
00:15:36,960 --> 00:15:40,040
 The thing is, because you'll see that, "Oh, yeah, well,

235
00:15:40,040 --> 00:15:42,280
 there really isn't anything desirable

236
00:15:42,280 --> 00:15:44,040
 about that at all."

237
00:15:44,040 --> 00:15:47,260
 Eventually once your wisdom gets stronger, you just don't

238
00:15:47,260 --> 00:15:48,920
 have any desire for the things

239
00:15:48,920 --> 00:15:49,920
 you used to desire.

240
00:15:49,920 --> 00:15:55,480
 You look at it and you rightly see that it's not worth des

241
00:15:55,480 --> 00:15:56,560
iring.

242
00:15:56,560 --> 00:16:03,560
 So that's the dhamma for tonight.

243
00:16:03,560 --> 00:16:10,800
 Do you have any questions?

244
00:16:10,800 --> 00:16:13,800
 You guys can go ahead.

245
00:16:13,800 --> 00:16:23,570
 Is there anything you can say about mindfulness or falling

246
00:16:23,570 --> 00:16:25,400
 asleep?

247
00:16:25,400 --> 00:16:30,120
 Similar experience to dying, but find it very difficult.

248
00:16:30,120 --> 00:16:31,120
 You can try.

249
00:16:31,120 --> 00:16:35,320
 You try to be mindful up until the moment you fall asleep.

250
00:16:35,320 --> 00:16:39,660
 If you're really good, you can know whether you fell asleep

251
00:16:39,660 --> 00:16:43,400
 on the rising or the falling.

252
00:16:43,400 --> 00:16:51,360
 You know, you just be mindful until you fall asleep.

253
00:16:51,360 --> 00:16:54,120
 It's not something you should worry about or strive for.

254
00:16:54,120 --> 00:16:55,680
 Just work at it.

255
00:16:55,680 --> 00:16:58,550
 When you lie down at night, try to take it as lying

256
00:16:58,550 --> 00:16:59,480
 meditation.

257
00:16:59,480 --> 00:17:01,480
 Don't just fall asleep.

258
00:17:01,480 --> 00:17:04,400
 But it works better when you're on a meditation course.

259
00:17:04,400 --> 00:17:14,400
 In life, of course, it's very difficult to be that mindful.

260
00:17:14,400 --> 00:17:15,400
 So, I'm going to try to do that.

261
00:17:15,400 --> 00:17:16,400
 I'm going to try to do that.

262
00:17:16,400 --> 00:17:17,400
 I'm going to try to do that.

263
00:17:17,400 --> 00:17:18,400
 I'm going to try to do that.

264
00:17:18,400 --> 00:17:19,400
 I'm going to try to do that.

265
00:17:19,400 --> 00:17:20,400
 I'm going to try to do that.

266
00:17:20,400 --> 00:17:21,400
 I'm going to try to do that.

267
00:17:21,400 --> 00:17:22,400
 I'm going to try to do that.

268
00:17:22,400 --> 00:17:23,400
 I'm going to try to do that.

269
00:17:23,400 --> 00:17:24,400
 I'm going to try to do that.

270
00:17:24,400 --> 00:17:25,400
 I'm going to try to do that.

271
00:17:25,400 --> 00:17:26,400
 I'm going to try to do that.

272
00:17:26,400 --> 00:17:27,400
 I'm going to try to do that.

273
00:17:27,400 --> 00:17:28,400
 I'm going to try to do that.

274
00:17:28,400 --> 00:17:29,400
 I'm going to try to do that.

275
00:17:29,400 --> 00:17:30,400
 I'm going to try to do that.

276
00:17:30,400 --> 00:17:31,400
 I'm going to try to do that.

277
00:17:31,400 --> 00:17:32,400
 I'm going to try to do that.

278
00:17:32,400 --> 00:17:33,400
 I'm going to try to do that.

279
00:17:33,400 --> 00:17:58,400
 I'm going to try to do that.

280
00:17:58,400 --> 00:18:09,400
 Any other questions?

281
00:18:09,400 --> 00:18:11,970
 I think there's a bit of a lag between what I say and what

282
00:18:11,970 --> 00:18:15,400
 comes up in the chat.

283
00:18:15,400 --> 00:18:19,600
 Anyway, if you have questions, I'll come to post them

284
00:18:19,600 --> 00:18:20,400
 tomorrow.

285
00:18:20,400 --> 00:18:23,400
 I should be back.

286
00:18:23,400 --> 00:18:38,400
 Have a good night, everyone.

