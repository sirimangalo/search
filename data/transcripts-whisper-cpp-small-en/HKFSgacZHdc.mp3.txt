 Okay.
 When I meditate, I always want to investigate my thought.
 Why I'm feeling that, what is the reason of my fear, what
 is the cause of this.
 Is it a good thing to do?
 You have to understand what the purpose of meditation is.
 Finding out the cause of things is not the true purpose of
 meditation.
 It's interesting.
 But you have to see the flaw in what you're saying.
 The reality is like a river.
 Everything goes in order, right?
 But on the other hand, it's quite complex.
 So to say x is the cause of y is a simplistic understanding
 of how things work.
 If you sit there and what you're talking about is saying,
 okay, I'm angry this morning.
 And the cause is that I was unmindful.
 And I allowed my anger to come to the processing of an
 experience to lead to anger.
 That's one part of the story.
 The question is, why were you unmindful in the first place?
 And what happened in between the time when you were unmind
ful and the time when you got angry?
 There's many things going on.
 So I'm pointing one specific thing as saying this is a
 cause.
 Intellectually, it has some value then because you say, oh,
 well, then I should be more mindful.
 And that sort of thing does happen in meditation, but that
's not meditation.
 It doesn't solve the problem.
 In fact, meditation does begin by identifying momentary
 causes.
 So from one moment to the next, seeing what mind precedes,
 what cause precedes what result.
 But it's still a very limited, a very preliminary basic
 understanding.
 That understanding is also not enough.
 It's not the true reason for practicing.
 It's not the highest insight.
 The highest insight is to see that things arise based on a
 cause.
 To see things are dependently risen.
 That's it.
 To not what was the cause, but to see the things arising
 and ceasing.
 It comes and then it goes.
 So that's to see impermanence.
 It's also to see sadhukha, which means inability to satisfy
 because it's impermanent.
 And non-self, to see that there's no essence to it.
 It's about the Dhamma itself, about the experience itself.
 That's what we're most interested in.
 Because the cause of suffering is simply attachment to
 things.
 So the cause in effect doesn't directly, indirectly can be
 useful, but directly does not tie into that.
 The craving is simply caused by seeing that things are
 stable, satisfying and controllable.
 Once you see that things are not thus, then you let go of
 them.
 And when you let go of them, you become free.
 When you're free, you say, "I'm free."
 Nothing more needs to be done.
 So is it useful?
 Yes, arguably that has some intellectual benefit because it
's going to say,
 "Well, then I shouldn't do that," or "I should adjust
 myself."
 It allows you to be sort of a meta-analysis, M-E-T-A, that
 allows you to adjust your practice.
 This we would call wimangsa, it allows you to succeed.
 So there's benefit to it.
 Don't think that it's a replacement to meditation.
 It's not.
 It's a non-meditative process.
 At that moment, you're actually not meditating because you
're not observing things simply as they are.
 You're reflecting on them, you're intellectualizing about
 them,
 you're reflecting processing your memories of things, which
 in fact can be flawed.
 But that's not the biggest problem.
 The biggest problem is its memories.
 It's, "Okay, this happened like this, happened like this,"
 which is all in the past
 and cannot be a replacement for true insight.
 So, not wrong, not bad, but not insight-meditative.
