 [birds chirping]
 Well welcome everyone to a beautiful Sunday afternoon in
 the deer park.
 We've got a wonderful turnout.
 Thank everyone for coming today.
 Please settle down.
 Settle yourselves.
 Turn off all the distractions.
 Focus your mind.
 Prepare to listen.
 [birds chirping]
 For today's talk I'd like to talk, I'd like to go over some
 of the,
 some of the ways in which the Buddha's teaching is,
 plays a part in changing who we are.
 Or really how we should look at Buddhism as something that
 is going to have an impact on,
 on who we are.
 So I think it's easy to slip into the intellectual mode
 when we approach
 things like meditation practice and Buddhism.
 Where we understand the teachings, we accept them,
 we're even able to pass the teachings on to others.
 But we don't practice them ourselves.
 We don't take them into our reality.
 You can catch yourself doing it.
 You'll be listening, sitting here listening to a talk and
 suddenly your mind
 wanders, maybe even your hand wanders to the mouse and
 suddenly you're
 switching channels.
 Back to Facebook, then on to Twitter, then on to YouTube
 and so on and so on.
 It's very easy to get, to get lost in the,
 or to deceive ourselves to think that an intellectual
 understanding of
 the Buddha's teaching, an acceptance of it, is enough.
 What Buddhism really is, is a,
 a means of using the mind to change the reality in front of
 you.
 Of training the mind to,
 to fix the, to adjust the reality around us,
 to bring it back into a state of harmony and peace.
 So we have this reality in front of us, the physical and
 the mental reality,
 and it's going on seemingly like an automaton.
 And for about the past 300 years, this is really how we've
 viewed reality
 as a deterministic closed system.
 The discovery of evolution and classical physics.
 Made and Cartesian three-dimensional space, sort of built
 up this,
 this idea in our minds that we're, we're creatures of habit
.
 And this is, this is a very much a part of reality.
 It's clear that this is what's happening.
 We follow after our wants and our needs.
 We follow after our habits, our character, most of the time
.
 But what the Buddha discovered is that this,
 this system is not closed in the way that we think it is.
 It's not deterministic.
 It's basically, from my understanding,
 sort of the realization that quantum physics has come to.
 And people keep yelling at me for comparing the two.
 And quantum physics has nothing to do with the mind and
 Buddhism and so on.
 But of course Buddhists keep telling me, yes, yes,
 it has everything to do with the mind and Buddhism.
 I've actually been reading some of the work of a very well-
known quantum physicist on the topic.
 And basically what he's, the things he's saying are exactly
 what we're saying,
 what the Buddha said and what we come to realize in
 meditation,
 that reality is much better understood as probabilities
 rather than,
 rather than dimensions or,
 I think they're particles in space-time.
 We're actually talking about when we,
 when we talk about physics and the way things work,
 the nature of reality, we're talking about probabilities.
 We're talking about potentials,
 the potential for something to happen.
 So there are potential, certain things, certain,
 certain realities or certain experiences have a potential.
 And it's not set in stone.
 It's not a physical reality that one potential is going to
 come and another is not.
 There's room for a mental intervention.
 And this is basically what we're doing.
 We have this reality in front of us that all of us can
 verify.
 If I tell you there's a reality in front of you of seeing,
 hearing,
 smelling, tasting, feeling, thinking of the physical and
 the mental,
 you can readily agree that that's at least what appears to
 be the case.
 And the practice of Buddhism is changing that in a certain
 way,
 in the way of coming to understand it and coming to see it
 clearer
 in a more precise fashion, kind of like fine tuning an
 automobile.
 So instead of just driving it and driving it and driving it
 until it breaks down,
 we're adjusting the reality.
 We're taking a closer look at the way we deal with our
 experiences at every moment.
 So I want to stress this because it helps us to understand
 why we have to do,
 why we have to practice meditation, why we have to avoid
 certain things,
 why there are certain things that we have to adjust in our
 life,
 why morality is so important in spiritual practice.
 If we understand things from a deterministic point of view
 or even a point of view of chance, where everything happens
 just by chance or so on,
 or really the way most of us look at it, we just don't
 understand reality.
 We just think of reality as this big unknown.
 It's a mystery.
 Then it's really difficult to understand why you should
 stop killing or stealing
 or lying or cheating or taking drugs and alcohol.
 It seems to most of us that we have a certain free will and
 we can do what we want.
 The funny thing is our sense of free will comes from our
 understanding
 that we're creatures of habit and this system is deter
ministic,
 so it really doesn't matter what you do.
 It's a very uneducated way of looking at reality.
 And yet we fall into this. We don't see the problems with
 things like drugs and alcohol.
 We don't understand how killing a fly is supposed to be bad
 karma,
 putting out poison for ants or mice or so on.
 We understand how lying to people is a problem,
 how stealing from the government or stealing from
 corporations or large businesses is a problem,
 how these things are bad karma.
 But when we approach reality from a Buddhist point of view
 or even a scientific point of view,
 we come to see that actually everything we do has a casual
 efficacy,
 meaning everything we do has a power to affect the future,
 to affect our reality at every moment.
 This is why we're always pushing people to stay in the
 present moment,
 because things are happening at every moment.
 Right now you're creating karma.
 At every moment that you like something, dislike something,
 judge something,
 you're building up a tendency in your mind. You're
 affecting the brain.
 The mind has an effect on the brain at every moment.
 It's to the order of something like hundreds or even
 thousands of times per second.
 When you practice meditation, you start to see the
 incredible power that our actions have,
 that in fact the act of killing is so blatantly, obviously
 harmful to the mind,
 that it's really not a question. It's not one of those deep
, esoteric or difficult to understand concepts.
 At least it shouldn't be.
 When you even just start to practice meditation,
 it becomes clear that this isn't one of those difficult
 questions,
 to stop killing, to stop stealing or so on.
 These are the core systems of defilements.
 It's much more difficult to see how sexual activity can be
 unwholesome,
 how our attachment to other people can be, or is a cause
 for suffering and so on.
 This is more difficult to understand, giving up things like
 music and so on.
 That's difficult.
 But on a basic level, the morality on a basic level is not
 difficult to understand or shouldn't be.
 It's something that we should realize in the very beginning
.
 This is an important point for all of us.
 If you're going to undertake meditation, it's not something
 you can do stoned or drunk.
 It's not something you can do.
 If you continue to perform immoral acts of killing and
 stealing and so on,
 and they're immoral only from the standpoint of their
 effect on your mind.
 This is what's great about Buddhism and this is what's
 great about this stuff I've been reading by this quantum
 physicist Henry Stott.
 He has this wonderful quote saying how incredible it would
 be if we could derive our morality from science.
 How we wouldn't have to rely on impossible to prove sources
 like God or old books.
 A book says this is right or this is wrong.
 When we base it on science, we're talking about something
 that's universal, that's testable, that's empirical, that's
 observable,
 that you can do a study on, that you can show with result
 or with experiment.
 That's really what morality means in Buddhism. There is no
 sense of feeling guilt for things or shame,
 hating yourself and feeling of being a wicked person.
 There's no such thing as a bad person in Buddhism. There's
 only the bad act, the bad deeds.
 They're bad because they hurt. They cause suffering for
 oneself and for others.
 This is something we realize in meditation.
 But it's important to talk about first because if we can't
 at least put these aside for the time being,
 we're never going to practice, we're never going to get far
 enough in our meditation to see the problem with these
 things.
 We have to take them as a first step.
 We have to understand that these things are going to get in
 our way.
 You have to take it on faith or at least an intellectual
 understanding in the beginning.
 Otherwise it can be very difficult to practice. You'll find
 it almost impossible to sit still
 because of the mental upset that's caused by these things.
 When we see reality from a point of view of this moment to
 moment effect that the mind has on our environment,
 our surroundings, our experience of reality, then it
 becomes really easy to understand.
 It becomes even possible to understand how things like sens
ual attraction, music, even food,
 our attachment to food or to taste can be a hindrance or
 can be a cause for suffering.
 When we start practicing, we have to understand this as an
 important principle in what we're about to do.
 It's a very real thing and it's an experiential thing where
 we're trying to see things as they are
 and we have to be able to give up killing, stealing, lying,
 cheating, drugs, alcohol,
 and we have to be able to moderate our attachments to
 things.
 We have to be able to let go, even on an intellectual level
, we have to be able to say to ourselves,
 "Okay, now I'm going to undertake the practice, at least
 for this period, I'm going to try my best to see things as
 they are
 and to give up, you know, you give up many things in the
 practice of meditation.
 Trying not to eat too much or chase after delicious foods,
 trying not to become distracted in entertainment
 and amusement, wasting our time with these things and
 affecting our mind by them.
 You know, there's, if you don't look at it from a physics
 point of view, you look at it from a chemical point of view
.
 Brain chemistry is very well documented, the addiction
 cycle, which occurs every time you like something.
 There's the release of certain chemicals in the brain and
 the development of a cycle of addiction.
 Once you've established this understanding and you've begun
 to understand at least the principle behind meditation,
 that we're going to be examining reality, we're going to be
 studying it and developing an understanding
 and developing an approach to reality that's wholesome,
 that's wise and based on reason, based on clear
 understanding.
 Then we can actually start to sit down and to meditate and
 to develop this realization, this understanding.
 So the Buddha said that before you even start to meditate,
 it's important to get an understanding of these things,
 that we're not just going to sit there, close our eyes and
 expect for something to happen to us.
 We're going to examine reality and this can be done anyway,
 it can be done right now, even as you're listening here.
 What's going on in your mind? Where is your mind going? It
's wandering, it's going here and there, what's occurring in
 your reality.
 There's pain in the body or there's a happy feeling or a
 calm feeling, maybe you feel hot, maybe you feel cold.
 And then there's judgments about everything that goes on,
 liking, disliking, sad, bored, worried, depressed and so on
.
 So this is where we're going to start when we meditate, we
're going to start by looking at reality.
 So the method of looking at reality, what I wanted to focus
 on today besides talking about morality is the things that
 we should do
 and how our meditation should proceed if we expect to gain
 real and concrete results through the practice.
 And I talk about in my videos four fundamental qualities of
 meditation or guidelines.
 And these are not mine, they're something that my teacher
 taught to us again and again and again.
 And it really makes sense as sort of the basic foundations
 of meditation practice.
 So the meditation I teach, first of all, for those of you
 who aren't aware, is based on the four foundations of
 mindfulness.
 If you want information on that, you can check out the
 videos I have on YouTube about how to meditate.
 And the technique is to use a mantra, but it's a mantra
 based on reality.
 A mantra is just a word that focuses the mind on something.
 And here we're using the mantra to focus on our experiences
, to focus on the reality that we experience at every moment
.
 So when you see something, the mantra is seeing.
 When you hear something, the mantra is hearing.
 When you feel pain, the mantra is pain. When you feel angry
, the mantra is angry.
 And the way of doing this, there are four parts to it.
 This is the means of developing this type of meditation
 practice.
 The first is that your mind is in the present moment.
 And this I've already mentioned a little bit.
 But it's really the most important factor here is that we
 are here and now.
 That we understand the framework that we're going to
 approach the practice in.
 And it's our reality. It's not an intellectual thing.
 We're not sitting here trying to accept the words that I
 say intellectually, except the Buddha's teaching
 intellectually.
 We're trying to approach our reality right now as we
 experience it.
 So when you use this mantra, it has to be based on
 something that's happening here and now.
 If you're thinking about something in the past, you
 remember something.
 Say you remember talking with someone.
 You wouldn't say to yourself, "Talking, talking."
 You say to yourself, "Thinking, thinking," because that's
 what's going on.
 When you hear a car or you hear the birds in the background
, you won't say to yourself, "Birds, birds."
 You'll say to yourself, "Hearing, hearing," because that's
 here and that's now.
 The basic meditation object that we take is the body.
 When your stomach rises as you breathe, you say to yourself
, "Rising."
 And when it falls, "Falling."
 And you do it exactly when it happens.
 When it starts, you start to say, "Rising."
 And when it finishes, "Sing."
 When it falls, "Falling."
 When you walk, you walk, step, being right, step, being
 left in time and in sync with the movement itself.
 The second important quality is that you're practicing
 continuously.
 Because essentially we've come a long way from clear
 understanding of reality.
 We have many preconceived notions and ideas about reality
 that are patently false.
 Our way of approaching things is leading us to suffering.
 It builds up and builds up and causes us an incredible
 amount of suffering,
 much of which we're not aware of, because we don't have
 anything to compare it to.
 We think of ourselves as doing okay, as not suffering
 tremendously.
 But when we start to look at it, we see that actually it's
 a terribly uncomfortable position that we're in,
 of always chasing after things that we want, running away
 from things that we don't want,
 and stressing and stressing about adjusting things and
 forming things to be the way that we want them to be,
 and to always be according to our wishes.
 And so it's not enough for us to practice, say, once a day,
 twice a day,
 or come to these talks once a week, twice a week,
 and expect to develop in the practice, because the mind
 keeps going back again.
 It's like we're stuck in the mud, and if you pull yourself
 out of,
 if someone helps pull you out of the mud and you climb out
 a little bit,
 and then you stop, you slide back down into the mud,
 and you don't get, you don't progress in the practice.
 They liken meditation practice to boiling water or creating
 a fire.
 When you want to create a fire, you have to rub two sticks
 together,
 but it's important not just to rub the sticks together for
 a while,
 stop, and come back and try again. You have to do it
 continuously,
 because we're trying to change the essential way we look at
 reality.
 We're trying to train our minds to get it, to realize that
 this is the way reality is.
 That's not the way that we thought it was, to come out of
 this judgmental way of approaching things.
 In the beginning, all we're doing is pulling ourselves out
 of the mud,
 and it's going to be a long time before we can actually
 stand on solid ground
 and take a breather, in a sense, and not fall back into the
 mud.
 So our practice should be continuous.
 During the practice, we should always be reminding
 ourselves
 to come back to the present moment.
 We shouldn't say to ourselves, "Here I am practicing for an
 hour, or half an hour,
 or even longer than that."
 We should say to ourselves, "Here I am in the present
 moment,
 and how many moments am I meditating?"
 This mantra really makes that clear.
 When you clearly grasp the reality, saying to yourself,
 "Rising," and you know that it's rising, and you're clear
 that that's all it is,
 it's not me, it's not mine, it's not good, it's not bad,
 and you're just aware, and you let it go.
 When it's gone, it's gone, and then falling.
 You cut it off so that that's all it is, and it stops when
 the object stops.
 Your judgments, your thinking, your reaction to the object
 stops when the object stops.
 So it doesn't continue on wondering, worrying, liking, disl
iking, wanting, and so on.
 When you hear something, hearing, hearing, hearing,
 and you don't get angry when people yell at you, or become
 attached to beautiful sounds,
 or so on, when the object stops, you move on.
 When you create this awareness at every moment, that's
 truly meditating.
 If you can create this observation, this clear thought,
 again and again and again,
 it starts to have an effect on your mind.
 It's like pulling, pulling, pulling, pulling, pulling,
 pulling,
 and eventually your mind makes a shift, and it starts to
 get it,
 and it starts to see things in a way that you weren't able
 to see them before.
 It actually changes your reality.
 I mean, this is what I want to get through here,
 is that you will actually see your reality changing,
 your relationship with other people, your appreciation of
 the situation that you're in,
 your mood, everything that is real about your situation
 right now will change,
 and it's gradual, but it's perceivable.
 You can see that things are changing as you practice.
 This is the way it should be.
 But for this to occur, you have to be in the present moment
.
 Practice has to be continuous.
 So not just on the meditation mat, not just for an hour.
 During the meditation, you should try to be as mindful as
 you can.
 Once you get off the meditation mat and go on with your
 life,
 you should try your best to incorporate the practice into
 your daily life
 after you've gotten an appreciation of the practice
 and it's working for you.
 Start to incorporate it into your life so that when you're
 brushing your teeth,
 it's brushing meditation, brushing, brushing or feeling.
 You taste the toothpaste, tasting, tasting.
 You eat your food scooping, opening your mouth,
 opening, chewing, chewing, swallowing, swallowing,
 turning everything into a meditation.
 I mean, ideally, of course, in the beginning it's
 incredibly difficult,
 and if you can get walking, walking, walking when you walk
 down the street
 or when you're in the car sitting, driving, turning,
 screaming, swearing,
 acknowledging the emotions, angry, stressed, whatever you
 can get,
 it will have a profound effect on your meditation
 as opposed to just scheduling a sitting practice.
 The third fundamental quality is the quality of the mantra,
 the clear thought that you create.
 So it's not just a word.
 In some people in the beginning, they find themselves just
 saying the words
 or they find that it's actually disturbing their minds.
 They feel that it's making them uncomfortable.
 And in a sense, it will make you uncomfortable.
 It will help you to see that the way you're approaching
 reality
 is an intensely suffering experience.
 Many people don't like this sort of meditation
 because you'll start to see yourself clinging.
 When you say to yourself, for instance, "rising, falling,
 watching the stomach rise and fall,"
 you'll see it's quite uncomfortable,
 and it feels like by saying these words you're forcing the
 breath to rise and fall.
 And of course, that's not the way the mind should be
 approaching the task,
 but it's how we approach everything naturally.
 Our nature is to force things, is to make things be smooth,
 be exact, be perfect.
 So when the breathing is not the way we want it to be,
 we try to adjust it, we try to force it.
 We have this stress that arises in the mind.
 And that's what we're learning to see through the
 meditation.
 That's not wrong.
 And you should understand that once you train yourself,
 once you become really proficient in this technique,
 there won't be that suffering, there won't be that stress,
 there won't be that tension when you practice.
 So the correct way to practice has three parts,
 and this is according to the Buddha's teaching.
 The Buddha said when practicing the Four Foundations of
 Mindfulness,
 you need three qualities, and these are atapi, sampacano,
 and satima.
 These three are the basis of a correct practice of the Four
 Foundations of Mindfulness.
 Atapi means to have effort.
 So when we practice meditation,
 we have to understand that it's in essence a work that we
're doing.
 We're not sitting down to practice just to zone out,
 to transcend reality, or to relax.
 Meditation should not be thought of as a relaxation.
 There are meditations, or work that you can do,
 that is a sort of a relaxation.
 But that's not what we're trying to do.
 What we mean by meditation is to meditate on reality,
 to meditate on an object that will help us to see clearer,
 that will help us to understand ourselves and understand
 the world around us.
 And so we need to put out effort.
 We have to understand it's going to be hard work.
 Sometimes when you meditate, you'll find yourself sweating,
 you'll find yourself shaking,
 you'll find a great resistance rising up in your body,
 in your whole self, anger coming up, frustration coming up,
 boredom, this restlessness where you find yourself,
 you can't sit still, you have to stop.
 And this is what we have to change.
 This is what we're coming to alter, to break up and to work
 through.
 We do this by holding strong in our reaction to the object.
 So when we feel restless, instead of using it as a reason
 to just get up
 and do the easy thing and stop meditating,
 we remind ourselves that it's just restlessness.
 It's not a reason to do anything, to get up, to stop, to
 get angry or so on,
 to get frustrated.
 It's only restlessness.
 And we say to ourselves, "Restless, restless."
 We feel bored, bored, bored and so on.
 We need a certain effort in our practice.
 We have to be strong in the face of the phenomena.
 When you feel pain in the body, immediately you want to
 move.
 And even when you say to yourself, "Pain, pain,"
 it's very easy to be weak about it and to just move your
 body.
 But the "attapi," this is the strength.
 And "attapi" means literally burning up or heating up.
 And that's really how it feels.
 You are burning up this resistance to the object,
 as you say to yourself, with strength and fortitude.
 Pain, pain, pain.
 Being firm about it, that this is pain.
 Being that you are angry about it, you are upset about it,
 and not chasing after that.
 The second one, "sampatthano," means to see the object
 clearly,
 to be fully aware, fully comprehending the object in its
 entirety.
 And this sort of addresses the complaint that some people
 have
 in the beginning that when they say to themselves,
 "Rising, falling, or pain, pain,"
 they are just mouthing the words.
 And this is clearly not the way we should be practicing,
 though it's common in the beginning, until you get it.
 Until you get that you should be focusing on the object,
 that your mind should be with the object of your awareness.
 So you should actually send the mind to the object.
 And when you say to yourself, "Pain,"
 it's only reminding yourself about something that you're
 observing.
 You should send yourself, "Pain, pain, pain," like a magic
 spell
 that's going to make the pain go away.
 It's a reminder of something for what it is.
 So you should be clearly aware of the pain,
 and say to yourself at the same time, "Pain, pain, pain."
 Your mind should be there with the object.
 It should be strong, and it should be clearly aware
 of the object for what it is.
 The third one, Sati Ma, is the practice, the use of this
 mantra.
 This is the meaning of mindfulness.
 And I've talked about mindfulness before.
 I have videos about it, and audio talks about my
 understanding of mindfulness.
 Mindfulness is the word Sati Ma means to remember or to
 remind oneself.
 It's the recognition and the firm and clear understanding
 of something for what it is.
 The specifying out of all of the judgments that you could
 have about the object,
 all the things you could recognize it as, recognizing it
 for what it is.
 So instead of recognizing something as bad, as good, as
 pleasant, as unpleasant,
 recognizing it for what it is, when you say to yourself,
 "Pain, pain, pain," that's the meaning of the word Sati Ma.
 It's this recognition.
 And when you're clearly recognizing the object for what it
 is,
 this is what is meant by Sati Ma.
 Many people say that this is their first criticism,
 especially if they've practiced other types of meditation,
 where the teachers tell you just to watch the object, just
 to look at it,
 is that you're adding something.
 This word is an addition to the reality.
 It's an artificial construct that you create in the mind,
 and has nothing to do with the reality.
 It's adding a thought, and it's filling your mind up with
 thought.
 And this is why I always explain it to people in English as
 creating a clear thought.
 Because we're always thinking about things.
 This is Sankara, the fourth aggregate of our being.
 Out of the five parts of who we are, one part is the Sank
ara aggregate,
 which means mental formations.
 It means our thoughts, what we think of things, our
 judgments.
 And we're always doing this.
 When you feel the pain, you're thinking already.
 You're thinking, "This is bad. This sucks. This is mine. My
 pain. I feel the pain."
 And you're already thinking about what to do.
 "Yes, I'm going to move." And so on and so on.
 And this is an unclear thought.
 It's a distracted thought. It's a thought which is diffused
.
 It's superficial.
 You have a very limited understanding of the pain.
 You know it for a brief instant, and immediately you're off
,
 totally disconnected from the pain in a world of your own,
 saying, "I've got to move. I've got to get up. I've got to
 stop meditating.
 Hey, why don't I do that? Why don't I do this?"
 That would be more comfortable.
 And you've forgotten about the pain, but you're creating
 this whole world of illusion based on it.
 So we specify, this is the meaning of the word sati here.
 When the Buddha talked about it, in this sense he was
 talking about pati sati.
 He said pati sati mataya anisito jimhata.
 Mata means specifically, specific remembrance of just,
 based on the object, just as it is.
 Mata means just, and pati means specific.
 So we know something for what it is.
 Instead of saying, "This is bad. This is good. This is me.
 This is mine."
 We say, "This is this."
 All we're doing is changing our minds.
 And this is the mental intervention, which is talked about
 in Buddhism.
 And as I understand, it's talked about by quantum
 physicists.
 No matter what certain quantum physicists will say, there
 are others who understand that this intervention of the
 mind has a casual efficacy on reality.
 And this is what you'll see, that when you change your mind
 like this, it has a profound effect.
 So this is the third fundamental, that your use of the
 mantra is actually creating a clear thought, where you have
 effort, you have clear awareness,
 and you have this recognition of the object just as it is.
 The fourth fundamental is that your practice should be
 balanced.
 And balancing in the Buddha's teaching is based on the five
 mental faculties, based on four of the five mental
 faculties.
 It's understood that in our mind we have five qualities
 that we need to develop.
 These five qualities are confidence, effort, mindfulness,
 concentration, and wisdom.
 And we have to develop all five of them.
 These five are the basic faculties that are required to
 become enlightened, to see things as they are, to realize
 the truth of existence.
 But it doesn't do for us just to build and develop them as
 we will.
 We have to balance these, and we balance them as follows.
 Faith, or confidence, has to be balanced with wisdom.
 Concentration has to be balanced with effort.
 Mindfulness is considered to be the balancing factor.
 So these other four we have to balance them.
 And this is an important aspect of our practice, an
 important concept that we have to understand.
 Because it's easy to get caught up in one or the others,
 and you'll see this in Buddhist meditators,
 in many cases that we easily get caught up in developing
 one or another of these, and not balancing our practice, or
 balancing our faculties.
 So what is meant by balancing confidence with wisdom?
 Well, it's easy to know a lot, and to understand things,
 and to think about them, and to intellectualize.
 But until we actually see the things for ourselves, all it
 leads to is doubt.
 So if you don't have any confidence, which is based on
 meditation practice,
 if you're just developing wisdom, which many people do,
 they'll study Pali,
 they'll study the Buddha's teaching, memorizing and rec
iting,
 and thinking about and talking about and teaching about the
 Buddha's teaching.
 All they do is sit around and wonder and doubt, and their
 minds are never stable,
 even though they know quite a lot about reality, and they
're able to appreciate it and understand it.
 Without the understanding that comes from meditation
 practice,
 and our stability of mind that comes from our confidence.
 Many people require that they understand something
 intellectually before they'll put it into practice.
 So when I say, "You have to give up immoral acts," and so
 on, people say, "Well, prove it to me.
 I'm not going to do that until I can see it for myself.
 I'm going to continue to do all these things until I can
 see for myself that they're wrong."
 So it's important to actually experiment and to test things
 out.
 If someone tells you that this is a way to do things, try
 it and see what happens.
 It's important to actually test things so that your wisdom
 is based in reality.
 On the other hand, there's many people who have a great
 amount of confidence,
 and maybe they practice meditation a lot,
 and they can become so confident that they think they're
 enlightened.
 When in fact, in many cases, all they've gained is a quiet
ude of mind,
 repressing the defilements that exist in the mind,
 repressing the negative states simply with the strength of
 mind.
 They have a great amount of confidence, and they just push
 themselves,
 and push and push and push, and they attain to the state of
 peace and quietude
 and think they're enlightened.
 So in our practice, it's not enough to just push and push
 and push and practice
 and practice and practice.
 We have to, as I said, examine reality.
 Meditation is not just pushing yourself.
 "Okay, I believe you. I'll practice meditation. Go for it."
 It's an examination of the realities that you're
 experiencing.
 It's not just anger is bad, get rid of it. Greed is bad,
 get rid of it.
 And we tend to do this a lot as Buddhists, because we read
 the texts,
 and we say, "Okay, Buddha said anger is bad, greed is bad.
 This is bad, that is bad."
 And so we repress it.
 This happens a lot with monks.
 We have to maintain celibacy, and this can be very
 difficult,
 because we end up, many times, just repressing the urges.
 And you see this a lot with Buddhist monks who become
 meditation teachers.
 They look really good, and they're able to talk really well
,
 and they teach for a few years, and then suddenly they've
 decided to disrobe,
 chasing after one of their female students.
 Because all they've done to make themselves look,
 or to appear even to themselves as enlightened,
 is repress the essential urges,
 and haven't taken the time to look and to examine and to
 understand these things,
 understand the true reality of them.
 This is what it means to balance our faith with our wisdom,
 that our meditation should be an examination.
 We should be always examining the states that we attain.
 When you attain states of peace and calm,
 they have to become an object of your awareness and the
 object of your meditation.
 Don't let confidence lead you astray.
 Balancing of effort with concentration.
 If we have a lot of effort, and we're just pushing
 ourselves in the meditation,
 it leads us to become distracted.
 There's many people who are very good at doing walking and
 sitting,
 walking and sitting for many hours, but their minds are not
 quiet.
 They're not focusing on an object.
 This is really what the mantra does.
 It allows you to focus, even when your mind is totally out
 of whack
 and not focused at all.
 You can focus on that state and say to yourself,
 "Focusing, focusing, focusing."
 I just got distracted myself. Someone's calling me.
 You say to yourself, "Distracted, distracted, distracted,"
 which I just was. Good timing.
 On the other hand, if you have too much concentration, it
 puts you to sleep.
 It dulls the mind.
 If you have too much concentration, you're not able to keep
 up with the rhythm of life
 and you find yourself missing things.
 You find yourself repressing rather than experiencing.
 The correct use of the mantra, instead of just focusing on
 one object
 and saying again and again and again, "Pain, Princess, pain
, pain, pain."
 We can focus on the pain so strenuously that we block out
 everything else.
 We don't see that actually we're angry about the pain.
 Actually, we have doubts about the practice because of it
 and so on.
 We're missing the bigger picture of what's really going on.
 Concentration is not enough.
 Effort is not enough.
 We shouldn't let either of them pull us to one side or the
 other.
 If we have too much effort, we can't focus on anything.
 If we have too much concentration, we become too focused.
 We're focusing on the pain and then suddenly we're off
 yelling at our teacher
 saying, "This is a stupid meditation. I say pain, pain,
 pain, and it doesn't go away
 and I hate it," and so on.
 We don't realize that there's so much more to the practice
 and we need the effort as well, effort and concentration
 balance,
 to first say, "Pain, pain, pain," and when we don't like it
, say, "Angry, anger, anger," and so on.
 This is the fourth fundamental.
 It's the balancing of the faculties.
 These are the four fundamentals in the practice.
 This is my understanding of how we should approach the
 practice
 to really make it real and to not just have an intellectual
 acceptance
 and then practicing blindly, but that our practice is
 constantly changing
 and adjusting and influencing the reality around us at
 every moment.
 First, with some things that we have to stop doing,
 we have to focus ourselves and do away with immoral
 activity
 and that we actually have to approach reality in our
 practice,
 the things that we have to do.
 That's to focus our mind on the present moment, to practice
 continuously,
 and to understand that meditation is a way of approaching
 our lives,
 not just an occasional activity that we perform and then go
 back to our lives.
 So that was the Dhamma that I wish to offer today.
 Thank you all for coming.
 If you have any questions, I'm happy to take them.
 Otherwise, I wish for the benefits of this teaching
 and for your practice of the Buddhist teaching to lead you
 all to peace,
 happiness and freedom from suffering.
 Have a good day.
