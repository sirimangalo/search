1
00:00:00,000 --> 00:00:04,000
 While sitting, the effort to come back to the abdomen

2
00:00:04,000 --> 00:00:05,480
 eventually ruins my vigilance

3
00:00:05,480 --> 00:00:10,450
 and concentration, whereas following experiences mindfully

4
00:00:10,450 --> 00:00:12,860
 as they naturally unfold helps me

5
00:00:12,860 --> 00:00:14,820
 remain mindful.

6
00:00:14,820 --> 00:00:16,460
 Isn't it contrary to the teachings?

7
00:00:16,460 --> 00:00:17,860
 What should I do?

8
00:00:17,860 --> 00:00:23,620
 No, it's not contrary to the teachings necessarily.

9
00:00:23,620 --> 00:00:28,700
 Now again, you have to be honest with yourself, which we

10
00:00:28,700 --> 00:00:31,000
 often aren't, I'm sorry to say,

11
00:00:31,000 --> 00:00:39,870
 when the problem is actually bringing the mind back or when

12
00:00:39,870 --> 00:00:42,080
 the problem is simply the

13
00:00:42,080 --> 00:00:45,530
 suffering that you have in the mind and the disinclination

14
00:00:45,530 --> 00:00:46,680
 to deal with it.

15
00:00:46,680 --> 00:00:49,980
 So sometimes when we come back to the present moment, when

16
00:00:49,980 --> 00:00:51,560
 we come back to the here and

17
00:00:51,560 --> 00:00:58,710
 now it's unpleasant because our mind is inclined to go with

18
00:00:58,710 --> 00:01:03,680
 it, to enjoy things and to be fluid

19
00:01:03,680 --> 00:01:10,260
 and to just let the mind go after, chase after objects,

20
00:01:10,260 --> 00:01:14,380
 which is disrupted by the practice

21
00:01:14,380 --> 00:01:15,460
 of mindfulness.

22
00:01:15,460 --> 00:01:18,520
 So there's two aspects.

23
00:01:18,520 --> 00:01:23,210
 When you force your mind back to the stomach, that's wrong

24
00:01:23,210 --> 00:01:25,720
 because it's not being honest

25
00:01:25,720 --> 00:01:28,640
 with yourself about the nature of reality.

26
00:01:28,640 --> 00:01:33,050
 You're deluding yourself about the idea that you have some

27
00:01:33,050 --> 00:01:35,480
 kind of control and can benefit

28
00:01:35,480 --> 00:01:38,220
 from trying to force things.

29
00:01:38,220 --> 00:01:44,460
 But by letting your mind go and just following after

30
00:01:44,460 --> 00:01:49,000
 whatever comes up, then you incline

31
00:01:49,000 --> 00:01:54,480
 instead towards laziness and indulgence.

32
00:01:54,480 --> 00:01:59,640
 There's the potential to indulge and to enjoy experiences

33
00:01:59,640 --> 00:02:03,420
 and give rise to liking and attachment.

34
00:02:03,420 --> 00:02:11,270
 So one experience that comes up is not ruining your

35
00:02:11,270 --> 00:02:17,880
 vigilance, but it's going to ruin your

36
00:02:17,880 --> 00:02:20,960
 sense of enjoyment, which we call concentration.

37
00:02:20,960 --> 00:02:24,220
 So it's quite common for people to say, "This practice is

38
00:02:24,220 --> 00:02:26,000
 ruining my concentration."

39
00:02:26,000 --> 00:02:31,330
 It's designed to do that because your concentration is not

40
00:02:31,330 --> 00:02:33,400
 necessarily wholesome, you see.

41
00:02:33,400 --> 00:02:36,320
 You can be concentrated on a bad thing.

42
00:02:36,320 --> 00:02:40,400
 Angry people can be, maybe not from an Abhidhamma point of

43
00:02:40,400 --> 00:02:42,800
 view, but can be in some way very,

44
00:02:42,800 --> 00:02:45,160
 very much focused.

45
00:02:45,160 --> 00:02:49,040
 You can be very much focused on sensual desire.

46
00:02:49,040 --> 00:02:54,220
 You get caught up in watching a movie or listening to a

47
00:02:54,220 --> 00:02:58,600
 song and you can become very much entranced

48
00:02:58,600 --> 00:03:01,160
 by it in a bad way.

49
00:03:01,160 --> 00:03:05,800
 Because there's a whole bunch of sensual attachment, of def

50
00:03:05,800 --> 00:03:07,440
ilement in there.

51
00:03:07,440 --> 00:03:09,720
 And so by breaking that up, it's unpleasant.

52
00:03:09,720 --> 00:03:13,080
 It's like crying like a child.

53
00:03:13,080 --> 00:03:17,370
 When the child gets what it wants, it's totally at peace

54
00:03:17,370 --> 00:03:19,720
 and it's totally focused.

55
00:03:19,720 --> 00:03:23,340
 When you take its toy away from it, it starts to whine and

56
00:03:23,340 --> 00:03:24,280
 complain.

57
00:03:24,280 --> 00:03:26,230
 That's essentially what the mind does when you bring it

58
00:03:26,230 --> 00:03:27,360
 back to the present moment.

59
00:03:27,360 --> 00:03:30,240
 It whines and complains.

60
00:03:30,240 --> 00:03:33,670
 So I would say oftentimes you're not actually forcing the

61
00:03:33,670 --> 00:03:35,800
 mind back, you're just choosing

62
00:03:35,800 --> 00:03:38,560
 something neutral, which the mind doesn't like.

63
00:03:38,560 --> 00:03:40,480
 The mind doesn't want to come back to the stomach.

64
00:03:40,480 --> 00:03:44,320
 That's boring, that's useless, that's uninteresting.

65
00:03:44,320 --> 00:03:45,320
 And that's the point.

66
00:03:45,320 --> 00:03:49,060
 The point is to keep you on something that's not going to

67
00:03:49,060 --> 00:03:51,360
 intoxicate you, which the mind

68
00:03:51,360 --> 00:03:52,360
 doesn't like.

69
00:03:52,360 --> 00:03:54,200
 So the mind is like, "I'd much more prefer to go after

70
00:03:54,200 --> 00:03:55,880
 these interesting things and enjoyable

71
00:03:55,880 --> 00:03:56,880
 things."

72
00:03:56,880 --> 00:03:58,320
 And that's mostly what you're feeling.

73
00:03:58,320 --> 00:04:01,660
 Now, if and when you're actually forcing them, not you, but

74
00:04:01,660 --> 00:04:03,320
 that's mostly what people feel

75
00:04:03,320 --> 00:04:06,840
 and I'd imagine much of what you're feeling is that.

76
00:04:06,840 --> 00:04:08,360
 That's my guess.

77
00:04:08,360 --> 00:04:11,600
 But there are people who force themselves.

78
00:04:11,600 --> 00:04:14,790
 So when something comes up, they ignore it and instead go

79
00:04:14,790 --> 00:04:16,680
 back, force themselves to stay

80
00:04:16,680 --> 00:04:17,760
 with one object.

81
00:04:17,760 --> 00:04:19,400
 So that's not good.

82
00:04:19,400 --> 00:04:21,880
 When something does come up, you're to acknowledge it.

83
00:04:21,880 --> 00:04:25,750
 Now, once you've acknowledged it, best advice is to go back

84
00:04:25,750 --> 00:04:28,000
 to the rising and falling without

85
00:04:28,000 --> 00:04:30,800
 paying attention to what's going to come next.

86
00:04:30,800 --> 00:04:33,870
 It will be uncomfortable to do that in many cases because

87
00:04:33,870 --> 00:04:35,480
 the mind isn't interested in

88
00:04:35,480 --> 00:04:36,520
 the rising and falling.

89
00:04:36,520 --> 00:04:40,140
 The mind wants to chase after more enjoyable things, which

90
00:04:40,140 --> 00:04:41,960
 is not beneficial, which is

91
00:04:41,960 --> 00:04:48,080
 a problem because it encourages defilement and delusion and

92
00:04:48,080 --> 00:04:50,640
 the idea of permanence of

93
00:04:50,640 --> 00:04:53,720
 stability and so on, which we're all trying to, all of

94
00:04:53,720 --> 00:04:55,840
 which we're trying to overcome.

