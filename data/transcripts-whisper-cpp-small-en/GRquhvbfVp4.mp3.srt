1
00:00:00,000 --> 00:00:08,490
 Okay, it says what happens with the mind if someone becomes

2
00:00:08,490 --> 00:00:10,720
 a once-returner or

3
00:00:10,720 --> 00:00:17,760
 non-returner? Just talk a little bit about these two stages

4
00:00:17,760 --> 00:00:19,960
. What happens to

5
00:00:19,960 --> 00:00:22,480
 the mind?

6
00:00:28,400 --> 00:00:33,440
 How do you answer? If someone becomes a once-returner or

7
00:00:33,440 --> 00:00:34,560
 non-returner? I see the

8
00:00:34,560 --> 00:00:37,080
 question.

9
00:00:37,080 --> 00:00:53,360
 What's that fat-imperse

10
00:00:53,360 --> 00:01:03,040
 a once-returner?

11
00:01:03,040 --> 00:01:12,200
 A once-returner is someone who has

12
00:01:16,880 --> 00:01:23,300
 really begun to turn away from sensuality, has become

13
00:01:23,300 --> 00:01:24,120
 totally

14
00:01:24,120 --> 00:01:31,270
 comfortable, not just comfortable, but has explored all

15
00:01:31,270 --> 00:01:33,240
 avenues of sensual

16
00:01:33,240 --> 00:01:37,560
 pleasure. Maybe explored isn't the right word. Has come to

17
00:01:37,560 --> 00:01:40,240
 understand all avenues

18
00:01:40,240 --> 00:01:46,120
 of sensual pleasure, has come to see them objectively for

19
00:01:46,120 --> 00:01:46,440
 what they

20
00:01:46,440 --> 00:01:52,880
 are, and is no longer frightened by their sexual or their

21
00:01:52,880 --> 00:01:53,880
 sensual

22
00:01:53,880 --> 00:01:59,920
 attractions, is no longer disturbed by them, and sees them

23
00:01:59,920 --> 00:02:03,320
 simply as maybe an

24
00:02:03,320 --> 00:02:11,400
 old bad habit, or as one would see the remnants of an old

25
00:02:11,400 --> 00:02:11,960
 bad habit.

26
00:02:11,960 --> 00:02:17,280
 You know, like our usage of certain slang words that we

27
00:02:17,280 --> 00:02:18,120
 just can't

28
00:02:18,120 --> 00:02:21,700
 kick. They're just kind of hanging around and every so

29
00:02:21,700 --> 00:02:27,720
 often they come out. And

30
00:02:27,720 --> 00:02:32,460
 they've done the same with anger. And so as a result these

31
00:02:32,460 --> 00:02:34,680
 things are not strong

32
00:02:34,680 --> 00:02:41,320
 in them. They still exist and this person is not fully

33
00:02:41,320 --> 00:02:45,320
 developed in their

34
00:02:45,320 --> 00:02:51,620
 state of mind. The mind is still subject to, I guess you

35
00:02:51,620 --> 00:02:53,720
 could say, even hypocrisy

36
00:02:53,720 --> 00:02:58,550
 in the sense of doing things that are against one's own

37
00:02:58,550 --> 00:03:01,520
 best wishes. So a

38
00:03:01,520 --> 00:03:04,600
 once-returner can still be married. Of course they will

39
00:03:04,600 --> 00:03:05,680
 never break the five

40
00:03:05,680 --> 00:03:10,120
 precepts, but that's a sotapan also will not.

41
00:03:10,120 --> 00:03:19,080
 They will seem, I mean, it's difficult to give this answer

42
00:03:19,080 --> 00:03:23,520
 really because once

43
00:03:23,520 --> 00:03:29,700
 you stray outside of the texts you're in difficult

44
00:03:29,700 --> 00:03:32,560
 territory. But they will

45
00:03:32,560 --> 00:03:39,680
 seem to be quite pure and it's common to think that

46
00:03:39,680 --> 00:03:42,240
 everyone who has

47
00:03:42,240 --> 00:03:46,320
 been practicing meditation for any time, any length of time

48
00:03:46,320 --> 00:03:47,920
, is an arahant, right?

49
00:03:47,920 --> 00:03:55,240
 Or eventually to think this, or to think that they must be

50
00:03:55,240 --> 00:03:56,400
 a non-returner or

51
00:03:56,400 --> 00:04:02,730
 something. It's very difficult I would say for most people

52
00:04:02,730 --> 00:04:03,120
 to tell the

53
00:04:03,120 --> 00:04:07,120
 difference between a once-returner and a non-returner. A

54
00:04:07,120 --> 00:04:08,720
 once-returner still has

55
00:04:08,720 --> 00:04:18,110
 remnants of greed and anger. A non-returner has no remnants

56
00:04:18,110 --> 00:04:21,600
 of aversion or

57
00:04:21,600 --> 00:04:27,760
 attraction, sensual attraction. They still have

58
00:04:27,760 --> 00:04:36,550
 desire for becoming and desire for non-becoming. So desire

59
00:04:36,550 --> 00:04:37,440
 to be this or

60
00:04:37,440 --> 00:04:41,920
 to become that. So maybe they might go out of their way to

61
00:04:41,920 --> 00:04:44,080
 teach people still.

62
00:04:44,080 --> 00:04:49,170
 A non-returner, what you would still sense in them is the

63
00:04:49,170 --> 00:04:50,560
 desire to do good

64
00:04:50,560 --> 00:04:56,720
 deeds, help other people.

65
00:05:03,440 --> 00:05:08,760
 You would still sense in them a sort of activity that would

66
00:05:08,760 --> 00:05:09,680
 be absent from

67
00:05:09,680 --> 00:05:14,840
 the arahant. An arahant would not have any feeling, you

68
00:05:14,840 --> 00:05:15,760
 would not get any feeling

69
00:05:15,760 --> 00:05:18,710
 from them that they were actively seeking anything out. It

70
00:05:18,710 --> 00:05:20,080
 would be, you

71
00:05:20,080 --> 00:05:23,190
 know, it's fairly blatantly obvious if you spend some time

72
00:05:23,190 --> 00:05:24,120
 with them that they

73
00:05:24,120 --> 00:05:28,880
 have no activity. I mean I guess the word I'm looking for

74
00:05:28,880 --> 00:05:31,160
 is intention. They have

75
00:05:31,160 --> 00:05:34,540
 no karmic intention in the sense of trying to seek out

76
00:05:34,540 --> 00:05:36,520
 results, whereas an

77
00:05:36,520 --> 00:05:39,520
 anagami would still have this subtle sense of trying to

78
00:05:39,520 --> 00:05:42,800
 seek out results. But

79
00:05:42,800 --> 00:05:47,790
 the difference between an anagami and a sakiragami, a once-

80
00:05:47,790 --> 00:05:48,200
returner and

81
00:05:48,200 --> 00:05:55,590
 a non-returner, is that you would probably see the once-

82
00:05:55,590 --> 00:05:57,640
returner laughing

83
00:05:57,640 --> 00:06:01,680
 in a different way than anagami might still laugh. I mean

84
00:06:01,680 --> 00:06:03,200
 they say an arahant,

85
00:06:03,200 --> 00:06:06,290
 some people say an arahant would never laugh. I don't know

86
00:06:06,290 --> 00:06:08,200
 about that. I don't

87
00:06:08,200 --> 00:06:12,340
 like to say such things. It's too... someone even said that

88
00:06:12,340 --> 00:06:12,880
 an arahant

89
00:06:12,880 --> 00:06:15,880
 would not yawn, which I think is going too far. We shouldn

90
00:06:15,880 --> 00:06:17,280
't attribute any

91
00:06:17,280 --> 00:06:24,040
 physical characteristics as requirements to be enlightened.

92
00:06:26,480 --> 00:06:29,960
 But you might have a different quality of laughter as a

93
00:06:29,960 --> 00:06:32,080
 once-returner.

94
00:06:32,080 --> 00:06:40,420
 There would be more physical manifestation of desires, so

95
00:06:40,420 --> 00:06:41,960
 you would

96
00:06:41,960 --> 00:06:47,340
 find them getting excited and even flustered because of

97
00:06:47,340 --> 00:06:51,800
 their desires still.

98
00:06:53,800 --> 00:06:58,840
 You'd find them more irrational than an anagami.

99
00:06:58,840 --> 00:07:04,280
 And so from time to time seeking out pleasures or enjoying

100
00:07:04,280 --> 00:07:08,590
 pleasures or expressing their partiality for certain things

101
00:07:08,590 --> 00:07:09,640
, it might still

102
00:07:09,640 --> 00:07:16,280
 come up. Now they've gone far in terms of seeing the

103
00:07:16,280 --> 00:07:16,840
 problem with

104
00:07:16,840 --> 00:07:21,050
 these things and they could never hold the belief that sens

105
00:07:21,050 --> 00:07:22,800
ual pleasures are a

106
00:07:22,800 --> 00:07:28,880
 good thing by any means. And it would be very subtle in a s

107
00:07:28,880 --> 00:07:30,680
aketagami.

108
00:07:30,680 --> 00:07:35,010
 But it still might be evident, it should be still evident,

109
00:07:35,010 --> 00:07:37,560
 to a conscious and a

110
00:07:37,560 --> 00:07:43,360
 careful observer that this person still has partialities,

111
00:07:43,360 --> 00:07:45,120
 still even has desires

112
00:07:45,120 --> 00:07:48,370
 for beautiful sights and sounds and smells and so on. It

113
00:07:48,370 --> 00:07:49,200
 can still get

114
00:07:49,200 --> 00:07:52,680
 irritated when they see or hear or smell or taste or feel

115
00:07:52,680 --> 00:07:53,920
 or think something that

116
00:07:53,920 --> 00:07:58,600
 is unpleasant. So I would say that is the some of the

117
00:07:58,600 --> 00:07:59,920
 differences.

118
00:07:59,920 --> 00:08:02,910
 The best thing to do would be to get an anagami here and a

119
00:08:02,910 --> 00:08:04,040
 saketagami here and

120
00:08:04,040 --> 00:08:09,840
 then spend some time watching them and studying them.

121
00:08:09,840 --> 00:08:13,840
 This is the best we can do.

