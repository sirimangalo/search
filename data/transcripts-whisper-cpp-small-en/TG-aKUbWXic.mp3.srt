1
00:00:00,000 --> 00:00:03,710
 Hi, just a short video here on a couple of projects I've

2
00:00:03,710 --> 00:00:06,720
 been working on in terms of

3
00:00:06,720 --> 00:00:10,580
 helping to make it easier to interact with people who are

4
00:00:10,580 --> 00:00:12,720
 interested in meditation.

5
00:00:12,720 --> 00:00:15,860
 The first one, as most of you have probably noticed, is

6
00:00:15,860 --> 00:00:17,100
 called Ask a Monk.

7
00:00:17,100 --> 00:00:22,070
 It's just a way for me to make it easier to answer a lot of

8
00:00:22,070 --> 00:00:24,720
 the reoccurring questions

9
00:00:24,720 --> 00:00:28,520
 that people have about the meditation and about Buddhism in

10
00:00:28,520 --> 00:00:29,320
 general.

11
00:00:29,320 --> 00:00:35,310
 So the way it works is you go to my channel at youtube.com/

12
00:00:35,310 --> 00:00:37,680
yutadhamo and scroll down to

13
00:00:37,680 --> 00:00:42,120
 the moderator panel, enter your question.

14
00:00:42,120 --> 00:00:45,400
 You have to be logged in to enter questions.

15
00:00:45,400 --> 00:00:48,480
 So if you haven't signed up for YouTube, sign up.

16
00:00:48,480 --> 00:00:53,640
 And enter your question and click submit.

17
00:00:53,640 --> 00:00:55,800
 And then just hang on.

18
00:00:55,800 --> 00:00:59,720
 You can see or wait for my answer.

19
00:00:59,720 --> 00:01:00,880
 Seems like a reasonable question.

20
00:01:00,880 --> 00:01:03,720
 I'll try to answer as many as I can.

21
00:01:03,720 --> 00:01:06,190
 If you want to see which ones have been answered, you can

22
00:01:06,190 --> 00:01:07,760
 go to the link which is on the right

23
00:01:07,760 --> 00:01:11,490
 hand side which leads you to Google moderator and then you

24
00:01:11,490 --> 00:01:13,580
 get a list of all the questions

25
00:01:13,580 --> 00:01:17,720
 that have been asked and the ones that have a response, say

26
00:01:17,720 --> 00:01:19,080
 view response.

27
00:01:19,080 --> 00:01:22,380
 Click on the view response and you'll see my answer.

28
00:01:22,380 --> 00:01:25,530
 You can also subscribe to my channel if you want to just

29
00:01:25,530 --> 00:01:27,280
 get all the updates to all the

30
00:01:27,280 --> 00:01:28,960
 questions.

31
00:01:28,960 --> 00:01:34,810
 Just click on the subscribe button or click on the link and

32
00:01:34,810 --> 00:01:37,160
 you should get an update in

33
00:01:37,160 --> 00:01:42,240
 your email or on your home screen, your home page, YouTube

34
00:01:42,240 --> 00:01:44,760
 home page whenever I place a

35
00:01:44,760 --> 00:01:48,240
 new video on YouTube.

36
00:01:48,240 --> 00:01:52,160
 So this is something I encourage everyone to make use of.

37
00:01:52,160 --> 00:01:54,620
 If you don't have any questions yourself, you can vote on

38
00:01:54,620 --> 00:01:55,880
 some of the questions that

39
00:01:55,880 --> 00:02:00,890
 have already been asked to try to give me a good idea of

40
00:02:00,890 --> 00:02:04,020
 which ones are more interesting

41
00:02:04,020 --> 00:02:09,360
 to people, more of an audience.

42
00:02:09,360 --> 00:02:13,040
 And just take part.

43
00:02:13,040 --> 00:02:16,460
 Be happy to see people interested in the meditation and

44
00:02:16,460 --> 00:02:18,560
 people who appreciate the work.

45
00:02:18,560 --> 00:02:21,510
 The other thing I'm involved in doesn't have to do with

46
00:02:21,510 --> 00:02:23,240
 YouTube so a lot of people maybe

47
00:02:23,240 --> 00:02:25,480
 missed it.

48
00:02:25,480 --> 00:02:29,200
 It's something to do with a website called Ustream.tv.

49
00:02:29,200 --> 00:02:34,480
 That's the letter Ustream.tv.

50
00:02:34,480 --> 00:02:40,560
 And I've put up a channel there for real-time broadcasting

51
00:02:40,560 --> 00:02:43,760
 where when we do our real-life

52
00:02:43,760 --> 00:02:50,130
 meditation where we're here in California doing our daily

53
00:02:50,130 --> 00:02:53,280
 meditation, you can watch.

54
00:02:53,280 --> 00:02:56,150
 So first of all, give a talk and you can listen to the talk

55
00:02:56,150 --> 00:02:57,720
 in real-time as I'm giving it

56
00:02:57,720 --> 00:03:02,320
 and then once I finish the talk, you can meditate with us.

57
00:03:02,320 --> 00:03:05,370
 It's besides the fact that it gives you a chance to listen

58
00:03:05,370 --> 00:03:06,800
 to the Buddha's teaching

59
00:03:06,800 --> 00:03:10,080
 or listen to the teaching on the meditation, it also gives

60
00:03:10,080 --> 00:03:11,720
 in sort of a structure to our

61
00:03:11,720 --> 00:03:14,910
 meditation practice where you feel like you're actually

62
00:03:14,910 --> 00:03:16,320
 practicing in a group.

63
00:03:16,320 --> 00:03:18,440
 I don't know how successful this is.

64
00:03:18,440 --> 00:03:21,110
 I can see there's a lot of people listening to the talks,

65
00:03:21,110 --> 00:03:22,760
 maybe not so many who are actually

66
00:03:22,760 --> 00:03:24,520
 meditating with us.

67
00:03:24,520 --> 00:03:27,780
 But it does give you that human connection where you can

68
00:03:27,780 --> 00:03:29,520
 actually connect with other

69
00:03:29,520 --> 00:03:32,970
 meditators in a way and feel like you're meditating with us

70
00:03:32,970 --> 00:03:33,200
.

71
00:03:33,200 --> 00:03:35,200
 It's an excuse to meditate.

72
00:03:35,200 --> 00:03:39,240
 So not sure how that's going to go, but at least it also

73
00:03:39,240 --> 00:03:41,360
 gives you an idea of what we

74
00:03:41,360 --> 00:03:44,430
 do here at the Meditation Center and lets you see that we

75
00:03:44,430 --> 00:03:46,560
 actually are conducting meditation

76
00:03:46,560 --> 00:03:47,560
 courses.

77
00:03:47,560 --> 00:03:50,300
 It lets you see what's going on here.

78
00:03:50,300 --> 00:03:53,060
 So if you want to check that out, you're welcome to go to

79
00:03:53,060 --> 00:03:53,680
 the link.

80
00:03:53,680 --> 00:04:01,200
 It's ustream.tv/channel/monkradio, all one word.

81
00:04:01,200 --> 00:04:04,170
 So thanks for tuning in everyone and keep the questions

82
00:04:04,170 --> 00:04:06,120
 coming and the comments as well.

