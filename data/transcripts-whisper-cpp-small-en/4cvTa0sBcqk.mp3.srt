1
00:00:00,000 --> 00:00:05,180
 There's a video where you talk about how to be calm and

2
00:00:05,180 --> 00:00:06,680
 avoid anxiety and you say that

3
00:00:06,680 --> 00:00:09,370
 the important thing is how you react, but does this help

4
00:00:09,370 --> 00:00:10,800
 remove the anxiety from our

5
00:00:10,800 --> 00:00:12,720
 heads?

6
00:00:12,720 --> 00:00:14,560
 Yes.

7
00:00:14,560 --> 00:00:24,080
 Yes, the important thing is to separate the anxiety from

8
00:00:24,080 --> 00:00:28,960
 the result, the resultant physical

9
00:00:28,960 --> 00:00:32,820
 manifestations of anxiety, the butterflies in your stomach,

10
00:00:32,820 --> 00:00:34,640
 the tension in your shoulders

11
00:00:34,640 --> 00:00:37,900
 and the headache and the fluttering of the heart, the

12
00:00:37,900 --> 00:00:39,720
 beating of the heart and so on.

13
00:00:39,720 --> 00:00:43,160
 All of that is not anxiety and that's really the problem

14
00:00:43,160 --> 00:00:45,280
 even with meditation because you'll

15
00:00:45,280 --> 00:00:48,880
 say to yourself anxious, anxious and the effects don't

16
00:00:48,880 --> 00:00:49,840
 disappear.

17
00:00:49,840 --> 00:00:52,290
 You still have butterflies in your stomach, you still have

18
00:00:52,290 --> 00:00:53,440
 a heart beating fast and so

19
00:00:53,440 --> 00:00:56,040
 on.

20
00:00:56,040 --> 00:00:57,840
 And so as a result you think it did nothing and then you

21
00:00:57,840 --> 00:00:58,680
 get anxious again.

22
00:00:58,680 --> 00:01:00,520
 You say, "Oh no, it's not working.

23
00:01:00,520 --> 00:01:02,240
 Oh no, I'm still anxious."

24
00:01:02,240 --> 00:01:05,480
 And then you get more and more anxious and it snowballs.

25
00:01:05,480 --> 00:01:07,340
 This is what it does if you're not meditating.

26
00:01:07,340 --> 00:01:09,680
 You feel the effects in the body, you get more anxious.

27
00:01:09,680 --> 00:01:11,820
 You get anxious and the effects in the body get worse.

28
00:01:11,820 --> 00:01:16,860
 You feel the effects in the body and you get totally

29
00:01:16,860 --> 00:01:19,400
 paralyzed as a result.

30
00:01:19,400 --> 00:01:23,160
 If you focus on the anxiety and focus on the effects of the

31
00:01:23,160 --> 00:01:25,400
 body objectively, seeing them

32
00:01:25,400 --> 00:01:31,530
 for what they are, you don't need the bodily manifestations

33
00:01:31,530 --> 00:01:33,280
 to disappear.

34
00:01:33,280 --> 00:01:35,320
 This is something that we don't get.

35
00:01:35,320 --> 00:01:38,870
 You can be totally calm in the mind and have your body

36
00:01:38,870 --> 00:01:40,080
 freaking out.

37
00:01:40,080 --> 00:01:45,120
 This is logically logical.

38
00:01:45,120 --> 00:01:49,920
 So you don't have to worry about what's going on in my

39
00:01:49,920 --> 00:01:50,920
 heart.

40
00:01:50,920 --> 00:01:54,600
 My heart is beating very fast, I must be anxious because no

41
00:01:54,600 --> 00:01:56,240
, you can't stop that.

42
00:01:56,240 --> 00:01:59,730
 It's something that's going to take a few minutes to calm

43
00:01:59,730 --> 00:02:00,320
 down.

44
00:02:00,320 --> 00:02:02,780
 But immediately you can do away with the anxiety in the

45
00:02:02,780 --> 00:02:03,240
 head.

46
00:02:03,240 --> 00:02:05,610
 It's important to see that and it's important to make the

47
00:02:05,610 --> 00:02:07,080
 distinction because I've given

48
00:02:07,080 --> 00:02:10,120
 talks before where my heart was beating really, really fast

49
00:02:10,120 --> 00:02:13,560
 and I had butterflies in the stomach.

50
00:02:13,560 --> 00:02:17,450
 One might say anxious, but where I had caught the anxiety

51
00:02:17,450 --> 00:02:19,980
 and was acknowledging the feelings

52
00:02:19,980 --> 00:02:22,650
 in the body, I was clearly aware of them and gave a great

53
00:02:22,650 --> 00:02:24,160
 talk and was perfectly clear

54
00:02:24,160 --> 00:02:28,620
 and perfectly calm and happy even though my voice was even

55
00:02:28,620 --> 00:02:31,360
 shaking because of the fluttering

56
00:02:31,360 --> 00:02:32,360
 in the body.

57
00:02:32,360 --> 00:02:38,330
 That's really funny which is going along with what you're

58
00:02:38,330 --> 00:02:41,280
 saying is what I've noticed

59
00:02:41,280 --> 00:02:47,320
 and is myself and other people say, "But I'm not supposed

60
00:02:47,320 --> 00:02:50,280
 to be feeling like this."

61
00:02:50,280 --> 00:02:54,320
 You're not supposed to be feeling like this.

62
00:02:54,320 --> 00:02:55,320
 This is how you're feeling.

63
00:02:55,320 --> 00:03:00,500
 It's like people with lifelong depression, they're trying

64
00:03:00,500 --> 00:03:02,760
 to always find that drug or

65
00:03:02,760 --> 00:03:08,170
 that therapy that's going to cure them, but maybe if they

66
00:03:08,170 --> 00:03:10,760
 were to just realize, "Oh,

67
00:03:10,760 --> 00:03:16,320
 this is the way I've always felt.

68
00:03:16,320 --> 00:03:18,760
 Maybe they can adjust with it better and go on."

69
00:03:18,760 --> 00:03:23,790
 Actually, that's what had taken place with myself was, "Oh,

70
00:03:23,790 --> 00:03:25,760
 but I'm not supposed to be

71
00:03:25,760 --> 00:03:27,520
 feeling like this."

72
00:03:27,520 --> 00:03:31,120
 My own sister had to say, "Hey, dummy, you've been feeling

73
00:03:31,120 --> 00:03:32,680
 like this since you've been

74
00:03:32,680 --> 00:03:34,280
 two years old.

75
00:03:34,280 --> 00:03:35,280
 So have I.

76
00:03:35,280 --> 00:03:37,280
 We inherited it from our parents.

77
00:03:37,280 --> 00:03:38,280
 It's genetic."

78
00:03:38,280 --> 00:03:43,580
 It's like, "Oh, you mean so I am supposed to be feeling

79
00:03:43,580 --> 00:03:44,960
 like this?

80
00:03:44,960 --> 00:03:45,960
 Yeah?"

81
00:03:45,960 --> 00:03:47,960
 It's like, "Oh, okay.

82
00:03:47,960 --> 00:03:52,480
 Well, I'm going to choose to start feeling other things."

83
00:03:52,480 --> 00:03:57,000
 It allowed me that opening to acknowledging it.

84
00:03:57,000 --> 00:03:59,990
 It's like the idea of like, "Oh, you're not supposed to

85
00:03:59,990 --> 00:04:00,840
 feel pain."

86
00:04:00,840 --> 00:04:04,320
 What do you mean you're not supposed to feel pain?

87
00:04:04,320 --> 00:04:07,870
 The more you're saying that, the more you're going to feel

88
00:04:07,870 --> 00:04:09,440
 the pain, obviously.

89
00:04:09,440 --> 00:04:14,150
 When it comes to physical pain, we all know that when you

90
00:04:14,150 --> 00:04:16,480
 start not resisting it, but

91
00:04:16,480 --> 00:04:20,270
 settle into it, allow yourself to acknowledge it, you

92
00:04:20,270 --> 00:04:22,520
 realize that the pain starts alleviating

93
00:04:22,520 --> 00:04:24,520
 itself on its own.

94
00:04:24,520 --> 00:04:30,010
 I just had to make that comment there because when you can

95
00:04:30,010 --> 00:04:32,920
 sit back and just say, "Oh, the

96
00:04:32,920 --> 00:04:38,720
 chaos going on around me, well, that's actually normal.

97
00:04:38,720 --> 00:04:39,720
 That's samsara.

98
00:04:39,720 --> 00:04:46,600
 Okay, let me just work on not thinking that this is all out

99
00:04:46,600 --> 00:04:48,080
 of order."

100
00:04:48,080 --> 00:04:52,680
 No, I just have to direct my own mind and just be calm.

101
00:04:52,680 --> 00:04:54,520
 Because really, that's the case.

102
00:04:54,520 --> 00:04:58,730
 We're living in chaos constantly, whether it's internal or

103
00:04:58,730 --> 00:05:00,560
 external, whether it's our

104
00:05:00,560 --> 00:05:06,690
 own perception of our own pain or the perception out around

105
00:05:06,690 --> 00:05:07,560
 us.

106
00:05:07,560 --> 00:05:10,700
 Maybe for a moment there, we have that little bubble of

107
00:05:10,700 --> 00:05:12,520
 where we feel everything's all right

108
00:05:12,520 --> 00:05:16,160
 but then burst.

109
00:05:16,160 --> 00:05:22,400
 Part of the joy of practicing dharma is to go with those

110
00:05:22,400 --> 00:05:25,160
 ups and downs and the bursts

111
00:05:25,160 --> 00:05:31,160
 and the deaths of all these different experiences.

112
00:05:31,160 --> 00:05:35,000
 I probably said a little too much, but I just had to...

113
00:05:35,000 --> 00:05:36,000
 No, that's exactly...

114
00:05:36,000 --> 00:05:37,000
 I'm with you there.

115
00:05:37,000 --> 00:05:40,850
 I think one of the best examples of this that I can think

116
00:05:40,850 --> 00:05:43,240
 of, especially as it relates to

117
00:05:43,240 --> 00:05:46,640
 anxiety, is insomnia.

118
00:05:46,640 --> 00:05:51,640
 People are only insomniac because they want to sleep.

119
00:05:51,640 --> 00:05:55,000
 That's obvious, right?

120
00:05:55,000 --> 00:05:57,960
 Because I should be sleeping.

121
00:05:57,960 --> 00:05:59,800
 It's the worst form.

122
00:05:59,800 --> 00:06:06,240
 I used to be insomniac and I'd be up until 3am because I

123
00:06:06,240 --> 00:06:10,120
 thought I should be sleeping.

124
00:06:10,120 --> 00:06:13,670
 The quickest things that Buddhism is able to cure, as far

125
00:06:13,670 --> 00:06:15,280
 as things that need to be

126
00:06:15,280 --> 00:06:20,640
 cured, is insomnia.

127
00:06:20,640 --> 00:06:24,910
 All it takes is for the teacher to say, "You don't need to

128
00:06:24,910 --> 00:06:25,760
 sleep.

129
00:06:25,760 --> 00:06:28,510
 You would be far better off if you were to stay awake all

130
00:06:28,510 --> 00:06:29,840
 night and be mindful.

131
00:06:29,840 --> 00:06:34,200
 So do it."

132
00:06:34,200 --> 00:06:38,850
 It comes into play, especially when they do an intensive

133
00:06:38,850 --> 00:06:40,760
 meditation course.

134
00:06:40,760 --> 00:06:42,860
 Even people in daily life, when you tell them, "Well, then

135
00:06:42,860 --> 00:06:43,480
 don't sleep.

136
00:06:43,480 --> 00:06:45,240
 Stay up all night and meditate."

137
00:06:45,240 --> 00:06:46,400
 You say, "Good for you.

138
00:06:46,400 --> 00:06:48,480
 I'm so happy for you.

139
00:06:48,480 --> 00:06:52,540
 I have to argue with my students to get them down to 6

140
00:06:52,540 --> 00:06:54,720
 hours of sleep at night."

141
00:06:54,720 --> 00:06:58,600
 "Oh, I could never do 6 hours of sleep."

142
00:06:58,600 --> 00:07:01,190
 And then these insomniacs are what I get 2-3 hours and I

143
00:07:01,190 --> 00:07:02,040
 say, "Great.

144
00:07:02,040 --> 00:07:03,040
 Good for you.

145
00:07:03,040 --> 00:07:04,720
 More time for meditation.

146
00:07:04,720 --> 00:07:07,280
 Meditate all night."

147
00:07:07,280 --> 00:07:13,320
 And immediately, of course, they fall asleep and have no

148
00:07:13,320 --> 00:07:15,400
 problems at all.

149
00:07:15,400 --> 00:07:18,100
 But the idea that you need to sleep more and so on, that

150
00:07:18,100 --> 00:07:19,320
 you should be asleep.

151
00:07:19,320 --> 00:07:20,320
 I thought that's a...

152
00:07:20,320 --> 00:07:21,320
 That conflict.

153
00:07:21,320 --> 00:07:22,320
 Yeah.

154
00:07:22,320 --> 00:07:23,320
 Stress.

155
00:07:23,320 --> 00:07:26,320
 I mean anxiety, it certainly is.

156
00:07:26,320 --> 00:07:29,820
 Someone adds to this and says, someone's saying, "Can you

157
00:07:29,820 --> 00:07:31,560
 talk about anxiety and how to deal

158
00:07:31,560 --> 00:07:32,560
 with it?"

159
00:07:32,560 --> 00:07:33,560
 Which I think we've done.

160
00:07:33,560 --> 00:07:37,700
 And the second one right above it says, "Anxiety and

161
00:07:37,700 --> 00:07:39,120
 insecurity."

162
00:07:39,120 --> 00:07:40,120
 Insecurity might be an interesting topic.

163
00:07:40,120 --> 00:07:41,120
 That's a good one.

164
00:07:41,120 --> 00:07:42,120
 Yeah.

165
00:07:42,120 --> 00:07:45,640
 It's a little bit different than anxiety, isn't it?

166
00:07:45,640 --> 00:07:46,640
 It's a...

167
00:07:46,640 --> 00:07:50,090
 I don't know if it's a Western world problem largely, but

168
00:07:50,090 --> 00:07:51,880
 there's a lot of insecurity I

169
00:07:51,880 --> 00:07:53,800
 find in the social circles that I run in.

170
00:07:53,800 --> 00:07:54,800
 Yeah.

171
00:07:54,800 --> 00:07:57,400
 The feeling that you are not...

172
00:07:57,400 --> 00:08:01,120
 You're inadequate or worrying that you might be inadequate.

173
00:08:01,120 --> 00:08:02,120
 Right.

174
00:08:02,120 --> 00:08:03,520
 Yeah.

175
00:08:03,520 --> 00:08:06,920
 Remembering things that you've done and, "Oh, I was such an

176
00:08:06,920 --> 00:08:08,200
 idiot to do that."

177
00:08:08,200 --> 00:08:14,600
 And what they must think of me and so on.

178
00:08:14,600 --> 00:08:17,040
 We're so much harder on ourselves than other people are,

179
00:08:17,040 --> 00:08:17,680
 aren't we?

180
00:08:17,680 --> 00:08:19,510
 You know, you've ever been to one of those situations where

181
00:08:19,510 --> 00:08:20,400
 you just did something you

182
00:08:20,400 --> 00:08:21,400
 thought must have...

183
00:08:21,400 --> 00:08:24,460
 Everyone must think you're an idiot and you just really

184
00:08:24,460 --> 00:08:26,240
 ruined your relationship with

185
00:08:26,240 --> 00:08:28,200
 everyone and no one else really noticed.

186
00:08:28,200 --> 00:08:31,280
 And they're kind of like, "Oh yeah, whatever."

187
00:08:31,280 --> 00:08:32,280
 And we're like killing ourselves.

188
00:08:32,280 --> 00:08:33,280
 They're thinking about themselves.

189
00:08:33,280 --> 00:08:34,280
 Yeah, exactly.

190
00:08:34,280 --> 00:08:36,600
 We're thinking about ourselves, they're thinking...

191
00:08:36,600 --> 00:08:37,600
 So they don't really care.

192
00:08:37,600 --> 00:08:41,320
 It's an interesting thing that that tends to be the case.

193
00:08:41,320 --> 00:08:44,200
 Our insecurity is thinking, "How are they looking at us?

194
00:08:44,200 --> 00:08:45,640
 They must really be judging us."

195
00:08:45,640 --> 00:08:46,640
 People don't care.

196
00:08:46,640 --> 00:08:48,200
 People don't really judge you.

197
00:08:48,200 --> 00:08:50,110
 They judge you for a second and then they go back to their

198
00:08:50,110 --> 00:08:51,120
 own problems because they

199
00:08:51,120 --> 00:08:55,040
 got lots of them.

200
00:08:55,040 --> 00:08:56,640
 So how do we deal with insecurity?

201
00:08:56,640 --> 00:08:58,640
 Is it just almost like insomnia?

202
00:08:58,640 --> 00:09:01,850
 Is it a similar situation where it's okay to feel insecure

203
00:09:01,850 --> 00:09:02,600
 or...

204
00:09:02,600 --> 00:09:05,490
 No, I mean, obviously you do, but I think it's much more

205
00:09:05,490 --> 00:09:06,960
 difficult because it has to

206
00:09:06,960 --> 00:09:08,440
 do with ego.

207
00:09:08,440 --> 00:09:12,240
 Insecurity is the core.

208
00:09:12,240 --> 00:09:15,480
 It's part of the core which is to give up self.

209
00:09:15,480 --> 00:09:21,870
 Until you can be totally fine with being an idiot, you'll

210
00:09:21,870 --> 00:09:25,560
 never be free from suffering.

211
00:09:25,560 --> 00:09:30,560
 Until you can say something that just really makes you look

212
00:09:30,560 --> 00:09:32,200
 stupid and be fine with that

213
00:09:32,200 --> 00:09:35,360
 because my teacher once did that.

214
00:09:35,360 --> 00:09:41,400
 He said something that was really wrong and it didn't faze

215
00:09:41,400 --> 00:09:42,160
 him.

216
00:09:42,160 --> 00:09:43,160
 He was like, "Yes.

217
00:09:43,160 --> 00:09:44,160
 Oh, yes.

218
00:09:44,160 --> 00:09:45,160
 Sorry.

219
00:09:45,160 --> 00:09:47,360
 That was wrong," and just went on with it.

220
00:09:47,360 --> 00:09:52,840
 Well, I got no problem saying stupid things.

221
00:09:52,840 --> 00:09:56,390
 Of course, I guess we would have to say, based on how you

222
00:09:56,390 --> 00:09:58,320
 said that, it's kind of like a

223
00:09:58,320 --> 00:09:59,320
 joke.

224
00:09:59,320 --> 00:10:01,920
 I wonder if it's possible to be saying lots of stupid

225
00:10:01,920 --> 00:10:02,520
 things.

226
00:10:02,520 --> 00:10:05,580
 There are people who say lots of stupid things and are fine

227
00:10:05,580 --> 00:10:07,200
 with it and aren't in life.

228
00:10:07,200 --> 00:10:09,480
 That's different, isn't it?

229
00:10:09,480 --> 00:10:10,480
 Yeah.

230
00:10:10,480 --> 00:10:12,080
 But they don't have insecurity.

231
00:10:12,080 --> 00:10:16,520
 There are people who...

232
00:10:16,520 --> 00:10:17,640
 That's not true, no?

233
00:10:17,640 --> 00:10:20,080
 Some people who...

234
00:10:20,080 --> 00:10:23,250
 I'm thinking of jokers, for example, people who just class

235
00:10:23,250 --> 00:10:24,840
 clowns and so on, who thrive

236
00:10:24,840 --> 00:10:25,840
 off of it.

237
00:10:25,840 --> 00:10:29,040
 Deep down, are they really secure?

238
00:10:29,040 --> 00:10:32,000
 Interesting topic.

239
00:10:32,000 --> 00:10:33,000
 Interesting question.

240
00:10:33,000 --> 00:10:35,280
 That's interesting because I'm very much like that.

241
00:10:35,280 --> 00:10:37,360
 Sorry, when does it work?

242
00:10:37,360 --> 00:10:42,030
 I was just kind of like had that being in school still, a

243
00:10:42,030 --> 00:10:44,240
 lot of people around this

244
00:10:44,240 --> 00:10:49,440
 age are very, very insecure, self-conscious of just every

245
00:10:49,440 --> 00:10:51,520
 little thing they do.

246
00:10:51,520 --> 00:10:54,800
 They're always thinking, "What do people think of me?

247
00:10:54,800 --> 00:10:57,840
 How do I look right now?"

248
00:10:57,840 --> 00:10:59,600
 Those thoughts come to my mind as well.

249
00:10:59,600 --> 00:11:03,850
 I think a fun, easy way to deal with that is just focus on

250
00:11:03,850 --> 00:11:05,840
 what you're doing at the

251
00:11:05,840 --> 00:11:06,840
 moment.

252
00:11:06,840 --> 00:11:07,840
 You're thinking.

253
00:11:07,840 --> 00:11:09,080
 It's really what you're doing.

254
00:11:09,080 --> 00:11:11,640
 You're overthinking actually.

255
00:11:11,640 --> 00:11:12,640
 What else are you doing?

256
00:11:12,640 --> 00:11:14,560
 Walking, sitting, whatever else.

257
00:11:14,560 --> 00:11:18,510
 I think that's a good way to deal with being insecure or

258
00:11:18,510 --> 00:11:22,120
 self-conscious because in reality,

259
00:11:22,120 --> 00:11:24,880
 you're making up this big story about what other people

260
00:11:24,880 --> 00:11:26,000
 would think of you.

261
00:11:26,000 --> 00:11:27,000
 Really, yeah.

262
00:11:27,000 --> 00:11:29,480
 They do have their own problems.

263
00:11:29,480 --> 00:11:34,710
 Of course, you'll see it as you meditate more that people

264
00:11:34,710 --> 00:11:37,160
 really don't care about other

265
00:11:37,160 --> 00:11:39,240
 people at all.

266
00:11:39,240 --> 00:11:43,080
 They care more about themselves.

267
00:11:43,080 --> 00:11:47,400
 Some people are very altruistic and empathic.

268
00:11:47,400 --> 00:11:48,400
 Some people actually do.

269
00:11:48,400 --> 00:11:51,360
 Yeah, some people are.

270
00:11:51,360 --> 00:11:55,160
 People aren't even caring even about themselves.

271
00:11:55,160 --> 00:12:00,000
 What they're doing is they're just perpetuating this myth

272
00:12:00,000 --> 00:12:01,280
 in their mind.

273
00:12:01,280 --> 00:12:06,100
 Part of their myth could even be masochism or constantly

274
00:12:06,100 --> 00:12:08,640
 putting themselves down.

275
00:12:08,640 --> 00:12:13,410
 The idea that they're really caring about themselves, well,

276
00:12:13,410 --> 00:12:15,640
 the term care, maybe that's

277
00:12:15,640 --> 00:12:20,900
 not the case, but they're definitely constantly playing

278
00:12:20,900 --> 00:12:22,680
 this mythology.

279
00:12:22,680 --> 00:12:26,860
 In that mythology, well, of course, other people really don

280
00:12:26,860 --> 00:12:28,880
't matter because they're not really

281
00:12:28,880 --> 00:12:30,360
 dealing with the real person.

282
00:12:30,360 --> 00:12:35,120
 They're dealing with the image of that person in contact in

283
00:12:35,120 --> 00:12:36,960
 their myth of them.

284
00:12:36,960 --> 00:12:37,960
 You're my daughter.

285
00:12:37,960 --> 00:12:38,960
 You're my son.

286
00:12:38,960 --> 00:12:39,960
 You're my mother.

287
00:12:39,960 --> 00:12:43,580
 Very rarely are they saying, "Oh, I'm a person and you're a

288
00:12:43,580 --> 00:12:44,400
 person."

289
00:12:44,400 --> 00:12:48,120
 It's always in context with this fabrication of how they

290
00:12:48,120 --> 00:12:50,400
 want to create reality as opposed

291
00:12:50,400 --> 00:12:53,400
 to the natural order of reality.

292
00:12:53,400 --> 00:12:55,400
 Very well said.

293
00:12:55,400 --> 00:12:58,560
 It's interesting, too, we talk about caring and insecurity.

294
00:12:58,560 --> 00:12:59,560
 They don't care.

295
00:12:59,560 --> 00:13:02,450
 In some situations where you find someone really does care,

296
00:13:02,450 --> 00:13:03,880
 it helps with insecurity.

297
00:13:03,880 --> 00:13:07,960
 In fact, it doesn't seem to have as much ground.

298
00:13:07,960 --> 00:13:10,590
 It seems like if you care as much as someone might not

299
00:13:10,590 --> 00:13:12,600
 circumstance, it also seems to have

300
00:13:12,600 --> 00:13:13,600
 an effect on insecurity.

301
00:13:13,600 --> 00:13:17,720
 It doesn't seem to remove a perfect way.

302
00:13:17,720 --> 00:13:21,250
 That person that comes in your life and they really, really

303
00:13:21,250 --> 00:13:23,240
 care, what's your response?

304
00:13:23,240 --> 00:13:25,240
 What the hell do they want from me?

305
00:13:25,240 --> 00:13:26,240
 Sometimes.

306
00:13:26,240 --> 00:13:29,280
 Well, what do they really want?

307
00:13:29,280 --> 00:13:35,840
 I don't like them because they're too direct.

308
00:13:35,840 --> 00:13:37,560
 They're too honest to me.

309
00:13:37,560 --> 00:13:43,840
 I fear that.

310
00:13:43,840 --> 00:13:45,840
 That's a common response I've seen.

311
00:13:45,840 --> 00:13:46,840
 Yeah.

312
00:13:46,840 --> 00:13:56,400
 Isn't insecurity a part of the symptom of anxiety?

313
00:13:56,400 --> 00:13:59,640
 No, I think it has ego more.

314
00:13:59,640 --> 00:14:02,800
 I think it's very much of anxiety.

315
00:14:02,800 --> 00:14:06,360
 What they think about me, "Oh, I feel bad.

316
00:14:06,360 --> 00:14:07,960
 What he might think about me.

317
00:14:07,960 --> 00:14:10,040
 I know that I suffer from anxiety."

318
00:14:10,040 --> 00:14:13,280
 It's a very part of anxiety.

319
00:14:13,280 --> 00:14:17,050
 But I think the insecurity comes first and the anxiety

320
00:14:17,050 --> 00:14:19,360
 comes after, or the insecurity

321
00:14:19,360 --> 00:14:23,620
 is talking about something bigger than anxiety, much more

322
00:14:23,620 --> 00:14:24,600
 ego based.

323
00:14:24,600 --> 00:14:31,460
 The real problem there is, as Lou said, much better than I

324
00:14:31,460 --> 00:14:32,600
 could.

325
00:14:32,600 --> 00:14:35,490
 The myths that we're playing, it's all just whether you

326
00:14:35,490 --> 00:14:36,440
 care about it.

327
00:14:36,440 --> 00:14:39,130
 I don't think I was totally correct to say that no one

328
00:14:39,130 --> 00:14:39,720
 cares.

329
00:14:39,720 --> 00:14:42,150
 We don't normally care, but we care more about ourselves

330
00:14:42,150 --> 00:14:43,840
 and others, or we're more focused

331
00:14:43,840 --> 00:14:45,880
 on it because some people are not.

332
00:14:45,880 --> 00:14:49,980
 But regardless, as Lou said, if they're focused on

333
00:14:49,980 --> 00:14:53,400
 themselves, focused on others, the problem

334
00:14:53,400 --> 00:14:55,040
 is simply the myth.

335
00:14:55,040 --> 00:14:59,320
 It's that we don't relate to each other as we really are.

336
00:14:59,320 --> 00:15:03,210
 I was talking today to one of the meditators, and she was

337
00:15:03,210 --> 00:15:05,640
 just asking about something just

338
00:15:05,640 --> 00:15:12,620
 curious offhand about this seeming kind of special power

339
00:15:12,620 --> 00:15:16,040
 that she had to see auras.

340
00:15:16,040 --> 00:15:18,880
 So she could look at someone and she could tell ... It wasn

341
00:15:18,880 --> 00:15:20,720
't their emotions, but she

342
00:15:20,720 --> 00:15:21,840
 explained it.

343
00:15:21,840 --> 00:15:25,130
 One example was how she was able to tell a person's sphere

344
00:15:25,130 --> 00:15:26,760
 of influence, basically how

345
00:15:26,760 --> 00:15:30,240
 powerful a person was, how big they were.

346
00:15:30,240 --> 00:15:32,740
 She got this sense of a person being big and a person being

347
00:15:32,740 --> 00:15:33,240
 small.

348
00:15:33,240 --> 00:15:35,560
 I've heard a monk tell me the same thing in Thailand.

349
00:15:35,560 --> 00:15:38,500
 He said he was able to see some people just filled the room

350
00:15:38,500 --> 00:15:38,760
.

351
00:15:38,760 --> 00:15:41,760
 When they walked into the room, the whole room was full,

352
00:15:41,760 --> 00:15:43,360
 and other people just seemed

353
00:15:43,360 --> 00:15:47,000
 very, very small to them.

354
00:15:47,000 --> 00:15:49,270
 So she was asking about this in general, and how it relates

355
00:15:49,270 --> 00:15:51,920
 to what we're talking about.

356
00:15:51,920 --> 00:15:56,310
 The answer I gave was the idea that it doesn't matter

357
00:15:56,310 --> 00:15:59,360
 whether you can see people's state

358
00:15:59,360 --> 00:16:01,970
 of mind, their emotions, or who they are, because that's

359
00:16:01,970 --> 00:16:05,680
 only who they are at that moment.

360
00:16:05,680 --> 00:16:08,810
 I said to her, "You're going through this intensive course,

361
00:16:08,810 --> 00:16:10,040
 and you can see your own

362
00:16:10,040 --> 00:16:11,040
 mind.

363
00:16:11,040 --> 00:16:12,160
 It's not one train.

364
00:16:12,160 --> 00:16:15,920
 It's not one coherent message.

365
00:16:15,920 --> 00:16:19,600
 Who you are changes from moment to moment, and in totally

366
00:16:19,600 --> 00:16:20,760
 chaotic ways.

367
00:16:20,760 --> 00:16:24,540
 So you might have built up this sankara, this formation,

368
00:16:24,540 --> 00:16:26,800
 this state, this habit, and you

369
00:16:26,800 --> 00:16:29,240
 might have a totally contradictory habit.

370
00:16:29,240 --> 00:16:31,120
 People can suddenly do wonderful things.

371
00:16:31,120 --> 00:16:36,840
 Why is that?"

372
00:16:36,840 --> 00:16:43,540
 I guess the point is just how important it is to shift our

373
00:16:43,540 --> 00:16:47,440
 understanding of beings, and

374
00:16:47,440 --> 00:16:52,190
 to point out how different it is, our ideas of people, and

375
00:16:52,190 --> 00:16:54,920
 the subsequent insecurity that

376
00:16:54,920 --> 00:16:58,840
 comes from thinking of people as people, or as immutable

377
00:16:58,840 --> 00:17:01,360
 entities, including ourselves,

378
00:17:01,360 --> 00:17:06,790
 like I am this sort of person, to seeing the things as a

379
00:17:06,790 --> 00:17:10,280
 moment to moment, as aggregates

380
00:17:10,280 --> 00:17:14,590
 of moment to moment experience, and to see people in that

381
00:17:14,590 --> 00:17:15,160
 way.

382
00:17:15,160 --> 00:17:18,650
 Is it related to listening to the person carefully and not

383
00:17:18,650 --> 00:17:20,600
 having an idea of the person?

384
00:17:20,600 --> 00:17:24,270
 Like Andrew Liu or yourself spoke about mother, child,

385
00:17:24,270 --> 00:17:25,200
 daughter.

386
00:17:25,200 --> 00:17:26,200
 For sure.

387
00:17:26,200 --> 00:17:27,200
 Is that related?

388
00:17:27,200 --> 00:17:31,800
 I think so.

389
00:17:31,800 --> 00:17:35,470
 Because the reason for listening closely should be that you

390
00:17:35,470 --> 00:17:37,760
 know, you understand, this isn't

391
00:17:37,760 --> 00:17:39,440
 the same person that they were last time.

392
00:17:39,440 --> 00:17:45,400
 I can't rely on my idea of who they are.

393
00:17:45,400 --> 00:17:47,920
 They could be totally different at this time, and we are.

394
00:17:47,920 --> 00:17:54,450
 This is something that is very underappreciated, how change

395
00:17:54,450 --> 00:17:56,800
able we really are.

396
00:17:56,800 --> 00:17:58,120
 We could come up with something totally different.

397
00:17:58,120 --> 00:18:00,130
 The person who you see, "Oh, this is this person," and you

398
00:18:00,130 --> 00:18:01,200
 expect them to act in a certain

399
00:18:01,200 --> 00:18:04,780
 way, and they may suddenly come up with some crazy past

400
00:18:04,780 --> 00:18:07,560
 life habit that they've never exhibited

401
00:18:07,560 --> 00:18:08,560
 before.

402
00:18:08,560 --> 00:18:09,560
 They may.

403
00:18:09,560 --> 00:18:13,780
 I mean, of course, talking about past lives, which of

404
00:18:13,780 --> 00:18:16,680
 course is taboo, but I don't care.

405
00:18:16,680 --> 00:18:20,280
 I'm not breaking taboos, so that's what I'm all about.

406
00:18:20,280 --> 00:18:23,040
 That's your past life, talking.

407
00:18:23,040 --> 00:18:24,720
 One of them.

408
00:18:24,720 --> 00:18:26,880
 One of the many.

409
00:18:26,880 --> 00:18:29,840
 Okay, so did we get anxiety?

410
00:18:29,840 --> 00:18:30,840
 Are we all anxiety-doubt?

411
00:18:30,840 --> 00:18:32,840
 I'm going to quit.

