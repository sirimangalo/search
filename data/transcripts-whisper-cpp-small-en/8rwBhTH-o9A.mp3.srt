1
00:00:00,000 --> 00:00:07,950
 okay so welcome everyone to monk radio on Sunday March

2
00:00:07,950 --> 00:00:13,520
 something so announcements

3
00:00:13,520 --> 00:00:19,970
 just one announcement that we want to express our

4
00:00:19,970 --> 00:00:23,440
 appreciation and gratitude

5
00:00:23,440 --> 00:00:28,990
 and the encouragement that it brings to see people getting

6
00:00:28,990 --> 00:00:29,720
 involved with our

7
00:00:29,720 --> 00:00:35,450
 organization we've started delegating and organizing people

8
00:00:35,450 --> 00:00:36,360
 to keep the

9
00:00:36,360 --> 00:00:41,010
 organization going and help with administrative tasks and

10
00:00:41,010 --> 00:00:41,480
 various things

11
00:00:41,480 --> 00:00:45,170
 that can be done over the internet so we started a web sub

12
00:00:45,170 --> 00:00:49,080
 site called org.seriamongolo.org

13
00:00:49,080 --> 00:00:52,960
 and that will have more detailed information about our

14
00:00:52,960 --> 00:00:54,000
 organization

15
00:00:54,000 --> 00:00:59,480
 sort of an administrative site so people want to know how

16
00:00:59,480 --> 00:01:01,520
 our organization works

17
00:01:01,520 --> 00:01:07,160
 and how it runs and it's great to see the the best thing

18
00:01:07,160 --> 00:01:07,960
 from my point of view

19
00:01:07,960 --> 00:01:11,760
 is that it's going to give me more time personally to focus

20
00:01:11,760 --> 00:01:14,000
 on teaching and

21
00:01:14,000 --> 00:01:21,230
 less time on running things so delegating is always a good

22
00:01:21,230 --> 00:01:22,000
 thing the

23
00:01:22,000 --> 00:01:26,920
 plan I think is to start doing more get back to do more

24
00:01:26,920 --> 00:01:28,720
 videos I'm going to try

25
00:01:28,720 --> 00:01:32,610
 to do daily teachings here just audio teachings in the

26
00:01:32,610 --> 00:01:34,920
 evening and then maybe

27
00:01:34,920 --> 00:01:39,260
 once or twice a week we'll make dhammapada videos or maybe

28
00:01:39,260 --> 00:01:39,880
 even some

29
00:01:39,880 --> 00:01:45,570
 ask a monk videos out in the out in the wilderness one

30
00:01:45,570 --> 00:01:46,960
 problem I have now is

31
00:01:46,960 --> 00:01:52,440
 the cameras dying the battery is not working and it's not

32
00:01:52,440 --> 00:01:53,680
 switching modes and

33
00:01:53,680 --> 00:01:59,270
 there's fungus growing in the lens so at least I have to

34
00:01:59,270 --> 00:02:00,280
 get in if we're going to

35
00:02:00,280 --> 00:02:03,900
 do any outdoor videos now I have to get a new battery for

36
00:02:03,900 --> 00:02:05,560
 it so I'll have to work

37
00:02:05,560 --> 00:02:13,330
 on that but anyway good to thanks everyone and if anyone

38
00:02:13,330 --> 00:02:14,280
 else is interested

39
00:02:14,280 --> 00:02:17,040
 in getting involved and helping out to keep the

40
00:02:17,040 --> 00:02:18,960
 organization going please do go

41
00:02:18,960 --> 00:02:24,840
 to see our new site and please get involved

42
00:02:24,920 --> 00:02:27,420
 No?

