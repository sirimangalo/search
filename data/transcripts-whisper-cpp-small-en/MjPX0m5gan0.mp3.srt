1
00:00:00,000 --> 00:00:18,180
 Okay, good evening everyone.

2
00:00:18,180 --> 00:00:44,200
 I'm live February 13th.

3
00:00:44,200 --> 00:00:49,720
 Sorry.

4
00:00:49,720 --> 00:00:55,840
 Okay so I'm going to post this Hangout.

5
00:00:55,840 --> 00:00:58,140
 Actually let's not, we'll post it later.

6
00:00:58,140 --> 00:01:08,480
 But joining the Hangout, the idea was for people who, if

7
00:01:08,480 --> 00:01:14,000
 you have questions, you have

8
00:01:14,000 --> 00:01:22,720
 a question you can join the Hangout and ask it.

9
00:01:22,720 --> 00:01:29,960
 I got someone sent me a note saying, complaining about

10
00:01:29,960 --> 00:01:36,160
 somehow being dismissive of Larry I

11
00:01:36,160 --> 00:01:37,520
 think last night.

12
00:01:37,520 --> 00:01:43,640
 So I apologize if that was the case, but probably was in

13
00:01:43,640 --> 00:01:47,120
 some way the case because I was kind

14
00:01:47,120 --> 00:01:54,680
 of hoping to encourage not like a panel like we had set up

15
00:01:54,680 --> 00:01:58,720
 in the past, but just people

16
00:01:58,720 --> 00:01:59,720
 asking questions.

17
00:01:59,720 --> 00:02:04,500
 So if you don't have a question, the ideas come on the Hang

18
00:02:04,500 --> 00:02:06,200
out for a question.

19
00:02:06,200 --> 00:02:09,880
 If you don't have a question, just listen at home.

20
00:02:09,880 --> 00:02:19,930
 It's not, it doesn't have to be authoritarian or anything,

21
00:02:19,930 --> 00:02:22,040
 but I think it's better to just

22
00:02:22,040 --> 00:02:25,600
 keep that for questions.

23
00:02:25,600 --> 00:02:46,560
 I apologize if it seems somewhat cold or harsh or whatever.

24
00:02:46,560 --> 00:02:51,310
 What's the thing when you teach and you put yourself out

25
00:02:51,310 --> 00:02:54,280
 there and get, you open yourself

26
00:02:54,280 --> 00:02:55,680
 up for criticism.

27
00:02:55,680 --> 00:03:00,320
 It's a reason why a lot of people I think are afraid of

28
00:03:00,320 --> 00:03:02,280
 teaching, afraid of putting

29
00:03:02,280 --> 00:03:07,800
 themselves out there in a thick skin.

30
00:03:07,800 --> 00:03:12,480
 You also get good, good feedback, positive feedback, but

31
00:03:12,480 --> 00:03:14,720
 you'll get negative feedback

32
00:03:14,720 --> 00:03:18,710
 and you'll make mistakes and people will call you out on

33
00:03:18,710 --> 00:03:19,360
 them.

34
00:03:19,360 --> 00:03:26,260
 If you hide away in your room, you don't have to face the

35
00:03:26,260 --> 00:03:28,200
 criticism.

36
00:03:28,200 --> 00:03:35,000
 So tonight we have a quote about gold.

37
00:03:35,000 --> 00:03:46,160
 Gold is of two kinds, unrefined and refined.

38
00:03:46,160 --> 00:03:51,360
 So we come into the meditation unrefined, like unrefined

39
00:03:51,360 --> 00:03:55,080
 gold with all sorts of defilements.

40
00:03:55,080 --> 00:04:03,120
 It has iron, copper, tin, lead and silver.

41
00:04:03,120 --> 00:04:07,580
 These are five other types of metal that are mixed in with

42
00:04:07,580 --> 00:04:09,280
 gold and keep it from being

43
00:04:09,280 --> 00:04:16,440
 pure pliant and keep it from being workable.

44
00:04:16,440 --> 00:04:26,520
 Can't do much with ordinary gold or you need to purify it.

45
00:04:26,520 --> 00:04:38,680
 In the same way, the mind is defiled by five impurities and

46
00:04:38,680 --> 00:04:45,240
 these are the five hindrances.

47
00:04:45,240 --> 00:04:55,050
 When I first started meditating, there was this British ex-

48
00:04:55,050 --> 00:04:59,800
meditator who some years before

49
00:04:59,800 --> 00:05:02,680
 that made a cartoon of the five hindrances.

50
00:05:02,680 --> 00:05:05,470
 And so when I first got there, they were handing out these

51
00:05:05,470 --> 00:05:07,200
 pages with the cartoons of the five

52
00:05:07,200 --> 00:05:12,200
 hindrances and cartoons of the five faculties.

53
00:05:12,200 --> 00:05:20,360
 Five mental faculties being confidence, effort, mindfulness

54
00:05:20,360 --> 00:05:24,480
, concentration and wisdom.

55
00:05:24,480 --> 00:05:29,170
 And they're quite useful cartoons, quite useful to have

56
00:05:29,170 --> 00:05:32,120
 those and have a visual reminder of

57
00:05:32,120 --> 00:05:33,120
 them.

58
00:05:33,120 --> 00:05:38,580
 I don't know if they still give them out or if they still

59
00:05:38,580 --> 00:05:40,600
 even have them.

60
00:05:40,600 --> 00:05:43,080
 When I was working there, we used to photocopy them.

61
00:05:43,080 --> 00:05:53,320
 The five hindrances are the first set of dhammas that we

62
00:05:53,320 --> 00:05:55,160
 have to look at in the meditation

63
00:05:55,160 --> 00:05:56,160
 practice.

64
00:05:56,160 --> 00:06:02,460
 So this is under that catch-all category of dhammas, which

65
00:06:02,460 --> 00:06:06,360
 never had a successful explanation

66
00:06:06,360 --> 00:06:10,360
 of how to translate dhamma in that context.

67
00:06:10,360 --> 00:06:13,900
 It seems like it means more than one thing, something that

68
00:06:13,900 --> 00:06:15,920
 you couldn't really translate.

69
00:06:15,920 --> 00:06:25,760
 You just have to understand it as a category of things.

70
00:06:25,760 --> 00:06:30,400
 But the first category is the hindrances, which makes it

71
00:06:30,400 --> 00:06:32,520
 seem like a real meditation

72
00:06:32,520 --> 00:06:33,520
 teaching.

73
00:06:33,520 --> 00:06:39,490
 The dhammas in the Satipatthana Sutta refer to those things

74
00:06:39,490 --> 00:06:41,960
 that you have to understand

75
00:06:41,960 --> 00:06:48,920
 or you have to know about or that you encounter on your

76
00:06:48,920 --> 00:06:51,960
 progress, on your path.

77
00:06:51,960 --> 00:06:57,550
 So as you're mindful of reality, the objective reality, so

78
00:06:57,550 --> 00:07:00,280
 this is the body and the mind,

79
00:07:00,280 --> 00:07:04,750
 the ordinary reality that's not caught up in goodness or

80
00:07:04,750 --> 00:07:07,080
 evil or so on, not ethically

81
00:07:07,080 --> 00:07:08,080
 charged.

82
00:07:08,080 --> 00:07:13,440
 You're going to react.

83
00:07:13,440 --> 00:07:17,410
 When you experience through the senses, you're going to

84
00:07:17,410 --> 00:07:20,040
 react, see things, hear things.

85
00:07:20,040 --> 00:07:22,680
 When you think things, you're going to react to your

86
00:07:22,680 --> 00:07:23,400
 thoughts.

87
00:07:23,400 --> 00:07:29,330
 When you experience physical sensations of pleasure and

88
00:07:29,330 --> 00:07:32,360
 pain, you're going to react.

89
00:07:32,360 --> 00:07:33,880
 And this is the five hindrances.

90
00:07:33,880 --> 00:07:37,970
 So as you're practicing, the five hindrances will come up

91
00:07:37,970 --> 00:07:40,240
 mainly as reaction, in reaction

92
00:07:40,240 --> 00:07:41,240
 to experiences.

93
00:07:41,240 --> 00:07:46,640
 They're hindrances because they get in the way of your

94
00:07:46,640 --> 00:07:48,000
 progress.

95
00:07:48,000 --> 00:07:50,360
 You like certain things, dislike certain things.

96
00:07:50,360 --> 00:07:55,040
 But I've gotten used to using simplified versions of these.

97
00:07:55,040 --> 00:07:59,630
 The five hindrances, the technical names of them are kamach

98
00:07:59,630 --> 00:08:02,120
anda, meaning sense desire.

99
00:08:02,120 --> 00:08:04,400
 So he's got them here.

100
00:08:04,400 --> 00:08:08,680
 Sense desire, ill will.

101
00:08:08,680 --> 00:08:12,360
 And you have sloth and torpor.

102
00:08:12,360 --> 00:08:14,370
 Sloth and torpor are two words that we just don't use

103
00:08:14,370 --> 00:08:14,920
 anymore.

104
00:08:14,920 --> 00:08:18,480
 So it's a shame that we're still using sloth and torpor.

105
00:08:18,480 --> 00:08:21,880
 Laziness, it's because there's two words.

106
00:08:21,880 --> 00:08:32,020
 Basically laziness, inertia, muddled, unwieldiness of the

107
00:08:32,020 --> 00:08:33,440
 mind.

108
00:08:33,440 --> 00:08:39,320
 And then restlessness and worry, that's a good definition.

109
00:08:39,320 --> 00:08:41,510
 Restlessness and worry are put together, but they're

110
00:08:41,510 --> 00:08:43,000
 actually two different things.

111
00:08:43,000 --> 00:08:46,050
 Restlessness is when the mind is not focused, when you're

112
00:08:46,050 --> 00:08:47,720
 thinking lots of different things.

113
00:08:47,720 --> 00:08:50,840
 And worry is a little bit different.

114
00:08:50,840 --> 00:08:58,120
 Worry is a sort of a fear or anxiety.

115
00:08:58,120 --> 00:09:01,220
 Concern about, I mean the technical descriptions are

116
00:09:01,220 --> 00:09:03,600
 concerned about things that you've done

117
00:09:03,600 --> 00:09:07,640
 in the past, bad deeds that you've done.

118
00:09:07,640 --> 00:09:13,000
 Worried about consequences, that kind of thing.

119
00:09:13,000 --> 00:09:20,760
 And we chikicha which means doubt.

120
00:09:20,760 --> 00:09:24,570
 The five hindrances are one of the first things that we

121
00:09:24,570 --> 00:09:26,200
 teach meditators.

122
00:09:26,200 --> 00:09:28,280
 They're always going to be important.

123
00:09:28,280 --> 00:09:32,080
 They're what you focus on or they're a big part of your

124
00:09:32,080 --> 00:09:32,840
 focus.

125
00:09:32,840 --> 00:09:36,280
 And they always have to be in your mind from beginning to

126
00:09:36,280 --> 00:09:37,640
 end of the course.

127
00:09:37,640 --> 00:09:41,330
 If you don't understand the five hindrances, if you don't

128
00:09:41,330 --> 00:09:42,920
 get them, they'll destroy you

129
00:09:42,920 --> 00:09:45,480
 in the practice.

130
00:09:45,480 --> 00:09:48,580
 As it gets harder, as it gets more intensive in a

131
00:09:48,580 --> 00:09:51,280
 meditation course, not practicing in

132
00:09:51,280 --> 00:09:54,960
 daily life, but of course they come into play there.

133
00:09:54,960 --> 00:09:58,220
 But during a meditation course, they really make or break

134
00:09:58,220 --> 00:09:59,280
 your practice.

135
00:09:59,280 --> 00:10:02,320
 As soon as you let them get to you and you forget to be

136
00:10:02,320 --> 00:10:04,440
 mindful of them, or if you don't

137
00:10:04,440 --> 00:10:07,240
 work hard to learn how to be mindful of them and how to

138
00:10:07,240 --> 00:10:09,160
 overcome them, if you don't become

139
00:10:09,160 --> 00:10:17,350
 comfortable dealing with them, then they'll start to weasel

140
00:10:17,350 --> 00:10:20,040
 their way in there into your

141
00:10:20,040 --> 00:10:32,160
 mind and they'll start eating away at your confidence,

142
00:10:32,160 --> 00:10:33,440
 eating away at your resolve, eating

143
00:10:33,440 --> 00:10:36,440
 away at your effort.

144
00:10:36,440 --> 00:10:43,510
 And they'll drag you down and they'll make you discouraged

145
00:10:43,510 --> 00:10:48,120
 and they can cause you to fail,

146
00:10:48,120 --> 00:10:51,780
 which should be really encouraging because that's all that

147
00:10:51,780 --> 00:10:53,560
 is going to make you fail.

148
00:10:53,560 --> 00:10:56,240
 We say that it gets tough or it is difficult to meditate or

149
00:10:56,240 --> 00:10:57,840
 there are times where you feel

150
00:10:57,840 --> 00:11:03,280
 like you can't meditate or you're just overwhelmed and you

151
00:11:03,280 --> 00:11:05,600
 don't know what to do.

152
00:11:05,600 --> 00:11:09,400
 But it's not the situation that makes it impossible.

153
00:11:09,400 --> 00:11:13,760
 There's no situation in the course that makes it impossible

154
00:11:13,760 --> 00:11:14,960
 to meditate.

155
00:11:14,960 --> 00:11:20,680
 It's only your own mind, your own emotions, your own

156
00:11:20,680 --> 00:11:23,360
 reactions to things.

157
00:11:23,360 --> 00:11:28,460
 And so it's actually quite easy to figure out the solution

158
00:11:28,460 --> 00:11:30,760
 to the problems that come

159
00:11:30,760 --> 00:11:34,200
 up not only actually in meditation, but in our lives.

160
00:11:34,200 --> 00:11:38,880
 It's quite encouraging for figuring out a succeed in life.

161
00:11:38,880 --> 00:11:45,390
 Not easy, but at least you have a clear picture of what you

162
00:11:45,390 --> 00:11:47,080
 need to do.

163
00:11:47,080 --> 00:11:48,920
 And that's to just deal with the five hindrances.

164
00:11:48,920 --> 00:11:52,880
 They're really the only things standing between us and

165
00:11:52,880 --> 00:11:55,960
 success in anything because they destroy

166
00:11:55,960 --> 00:11:59,840
 the minds, they destroy the faculties of the mind.

167
00:11:59,840 --> 00:12:02,200
 So they destroy your confidence.

168
00:12:02,200 --> 00:12:03,360
 They destroy your effort.

169
00:12:03,360 --> 00:12:09,650
 They destroy your mindfulness, your concentration and your

170
00:12:09,650 --> 00:12:10,800
 wisdom.

171
00:12:10,800 --> 00:12:12,640
 They distract you from these things.

172
00:12:12,640 --> 00:12:15,880
 They keep you from developing them.

173
00:12:15,880 --> 00:12:21,420
 They keep them from these faculties from growing strong,

174
00:12:21,420 --> 00:12:24,680
 from working together to free the

175
00:12:24,680 --> 00:12:30,800
 mind.

176
00:12:30,800 --> 00:12:38,280
 So for that reason, it's a very important dumbness.

177
00:12:38,280 --> 00:12:41,580
 Meditation in our tradition, we simplify them, liking, disl

178
00:12:41,580 --> 00:12:43,560
iking, drowsiness, distraction,

179
00:12:43,560 --> 00:12:44,560
 doubt.

180
00:12:44,560 --> 00:12:45,920
 I'm the one who came up with those words.

181
00:12:45,920 --> 00:12:47,920
 I don't know that anyone else is using those.

182
00:12:47,920 --> 00:12:53,040
 Why I use them is because it's liking and then four D's.

183
00:12:53,040 --> 00:12:55,560
 Liking, disliking, drowsiness, distraction, doubt.

184
00:12:55,560 --> 00:12:56,560
 It's easy to say.

185
00:12:56,560 --> 00:12:58,640
 It's easy to remember.

186
00:12:58,640 --> 00:13:01,200
 But it's very similar to how Ajahn Tong, my teacher,

187
00:13:01,200 --> 00:13:02,520
 teaches them in Thai.

188
00:13:02,520 --> 00:13:06,720
 "Chop na, mai chaap na, wong fu, song sai.

189
00:13:06,720 --> 00:13:08,960
 Chop mai chaap, wong fu, song sai."

190
00:13:08,960 --> 00:13:11,120
 As you say, it gives them very simple words.

191
00:13:11,120 --> 00:13:13,680
 Even if you don't know what those mean, you can hear that

192
00:13:13,680 --> 00:13:15,040
 they're very easy to say.

193
00:13:15,040 --> 00:13:17,480
 "Chop mai chaap, mai" means no.

194
00:13:17,480 --> 00:13:19,080
 "Chop" means like.

195
00:13:19,080 --> 00:13:24,080
 "Chop mai chaap, wong" is just tired or drowsy.

196
00:13:24,080 --> 00:13:26,720
 "Fung" means "fung san."

197
00:13:26,720 --> 00:13:34,000
 "Fung san" is distracted or flustered.

198
00:13:34,000 --> 00:13:37,880
 And "song sai" is doubt.

199
00:13:37,880 --> 00:13:41,130
 We need these simple words because we're on the front lines

200
00:13:41,130 --> 00:13:41,400
.

201
00:13:41,400 --> 00:13:44,080
 We need to be able to use these as weapons.

202
00:13:44,080 --> 00:13:47,840
 We need to have a weapon to combat these.

203
00:13:47,840 --> 00:13:50,320
 So it's like knowing your enemy.

204
00:13:50,320 --> 00:13:55,280
 You have to know that way of getting to them.

205
00:13:55,280 --> 00:13:58,990
 If you have these words like "central desire, ill will,"

206
00:13:58,990 --> 00:14:01,120
 and so on, "sloth and lacing,"

207
00:14:01,120 --> 00:14:07,600
 then that doesn't really work, practically speaking.

208
00:14:07,600 --> 00:14:13,400
 It's important that we memorize these.

209
00:14:13,400 --> 00:14:17,120
 When people come to practice in Chantong, Ajahn Tong will

210
00:14:17,120 --> 00:14:18,320
 remind them of these.

211
00:14:18,320 --> 00:14:20,480
 It starts with the four foundations of mindfulness.

212
00:14:20,480 --> 00:14:24,150
 You have to memorize the four sati batanas, "kaayo idana j

213
00:14:24,150 --> 00:14:25,240
ita dhamma."

214
00:14:25,240 --> 00:14:27,560
 And then under "dhamma," you have to memorize the five hind

215
00:14:27,560 --> 00:14:28,000
rances.

216
00:14:28,000 --> 00:14:31,920
 "Chop mai chaap," and it'll have you repeat after him.

217
00:14:31,920 --> 00:14:35,040
 "Chop, chaap, mai chaap, mai chaap."

218
00:14:35,040 --> 00:14:38,560
 "Chop, chaap, mai chaap."

219
00:14:38,560 --> 00:14:44,720
 And then you have to say it yourself.

220
00:14:44,720 --> 00:14:48,920
 They're that important.

221
00:14:48,920 --> 00:14:54,510
 As you can see, the Buddha says, "When you give up these

222
00:14:54,510 --> 00:14:57,080
 five debasements, it's five

223
00:14:57,080 --> 00:15:04,480
 defilements."

224
00:15:04,480 --> 00:15:07,840
 One can then direct the mind to the realization by psychic

225
00:15:07,840 --> 00:15:10,120
 knowledge of whatever can be realized

226
00:15:10,120 --> 00:15:11,120
 by psychic knowledge.

227
00:15:11,120 --> 00:15:14,200
 You can see it directly, whatever its range might be.

228
00:15:14,200 --> 00:15:18,280
 So he's talking about psychic powers, magical powers.

229
00:15:18,280 --> 00:15:21,510
 It's possible to gain these when your mind is free from

230
00:15:21,510 --> 00:15:24,920
 hindrance, but it's also possible.

231
00:15:24,920 --> 00:15:31,620
 The sixth of the abhinya of the psychic powers is the

232
00:15:31,620 --> 00:15:35,560
 destruction of defilement.

233
00:15:35,560 --> 00:15:40,400
 So the highest psychic power is the power of wisdom of

234
00:15:40,400 --> 00:15:43,280
 overcoming the defilement.

235
00:15:43,280 --> 00:15:46,920
 So that's the "dhamma" for tonight.

236
00:15:46,920 --> 00:15:51,350
 I'm going to post the link to the Hangout, but it's only

237
00:15:51,350 --> 00:15:53,720
 really if you want to come on

238
00:15:53,720 --> 00:15:57,000
 and ask questions.

239
00:15:57,000 --> 00:16:05,840
 So if you have a question, come on to Hangout.

240
00:16:05,840 --> 00:16:08,840
 Don't be shy.

241
00:16:08,840 --> 00:16:14,400
 Come and ask.

242
00:16:14,400 --> 00:16:17,960
 This is our new way of doing things.

243
00:16:17,960 --> 00:16:21,190
 You've got to actually be brave enough to come on and ask

244
00:16:21,190 --> 00:16:22,320
 your question.

245
00:16:22,320 --> 00:16:26,920
 You also need a mic in that case, which I guess is a bit of

246
00:16:26,920 --> 00:16:27,840
 a thing.

247
00:16:27,840 --> 00:16:37,200
 Some people might not have a mic.

248
00:16:37,200 --> 00:16:46,240
 Huh.

249
00:16:46,240 --> 00:16:48,280
 This person has posted this several times.

250
00:16:48,280 --> 00:16:52,200
 I guess I never actually answered it.

251
00:16:52,200 --> 00:16:57,830
 Someone's asking about whether they should learn more from

252
00:16:57,830 --> 00:17:00,280
 normal how-to videos.

253
00:17:00,280 --> 00:17:01,280
 You're 17 years old.

254
00:17:01,280 --> 00:17:02,920
 Don't worry about the children's videos.

255
00:17:02,920 --> 00:17:04,200
 I mean, adults have liked them.

256
00:17:04,200 --> 00:17:06,720
 Didn't they answer this already?

257
00:17:06,720 --> 00:17:12,050
 Adults have found those videos useful as well, and they're

258
00:17:12,050 --> 00:17:14,840
 kind of useful because they simplify

259
00:17:14,840 --> 00:17:15,840
 everything.

260
00:17:15,840 --> 00:17:18,760
 But I recommend my booklet.

261
00:17:18,760 --> 00:17:22,680
 If you haven't read the booklet on how to meditate, that's

262
00:17:22,680 --> 00:17:24,480
 where you should start.

263
00:17:24,480 --> 00:17:38,080
 Most of those kids' videos are just about non-insight

264
00:17:38,080 --> 00:17:40,880
 things.

265
00:17:40,880 --> 00:17:43,480
 So no questions?

266
00:17:43,480 --> 00:17:55,760
 No questions that I'm going to head out?

267
00:17:55,760 --> 00:18:06,440
 All right.

268
00:18:06,440 --> 00:18:08,440
 Have a good night, everyone.

269
00:18:08,440 --> 00:18:10,280
 See you all tomorrow.

270
00:18:10,280 --> 00:18:11,280
 Bye-bye.

271
00:18:11,280 --> 00:18:12,280
 Bye-bye.

272
00:18:12,280 --> 00:18:12,280
 Bye-bye.

273
00:18:12,280 --> 00:18:13,280
 Bye-bye.

274
00:18:13,280 --> 00:18:14,280
 Bye-bye.

275
00:18:14,280 --> 00:18:14,280
 Bye-bye.

276
00:18:14,280 --> 00:18:15,280
 Bye-bye.

