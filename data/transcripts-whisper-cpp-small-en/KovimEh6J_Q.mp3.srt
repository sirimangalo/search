1
00:00:00,000 --> 00:00:06,310
 Okay, so the question is, what is the meaning of proper

2
00:00:06,310 --> 00:00:09,360
 faith in Buddhism?

3
00:00:09,360 --> 00:00:18,640
 It's an interesting topic because it's highly polarizing in

4
00:00:18,640 --> 00:00:22,120
 Buddhism.

5
00:00:22,120 --> 00:00:30,600
 There are certain modern Buddhists, modern meaning, let's

6
00:00:30,600 --> 00:00:32,120
 say, western Buddhists in the

7
00:00:32,120 --> 00:00:37,500
 sense that they've come to Buddhism without any cultural

8
00:00:37,500 --> 00:00:40,480
 background or any traditional

9
00:00:40,480 --> 00:00:41,480
 background.

10
00:00:41,480 --> 00:00:46,830
 So they've come to it directly through the texts or through

11
00:00:46,830 --> 00:00:48,760
 a modern teacher.

12
00:00:48,760 --> 00:00:53,330
 And in general, there's a trend among such Buddhists to

13
00:00:53,330 --> 00:00:56,200
 discount the concept of faith.

14
00:00:56,200 --> 00:01:01,330
 They will say things like the Buddha taught us to question

15
00:01:01,330 --> 00:01:04,200
 everything, not believe even

16
00:01:04,200 --> 00:01:08,480
 him.

17
00:01:08,480 --> 00:01:16,460
 And so give the general impression that doubt, skepticism,

18
00:01:16,460 --> 00:01:21,000
 and by extension, doubt is useful

19
00:01:21,000 --> 00:01:27,760
 and faith is useless or even harmful.

20
00:01:27,760 --> 00:01:30,240
 Faith is against the Buddhist teaching.

21
00:01:30,240 --> 00:01:36,400
 There's another extreme, you have traditional Buddhists who

22
00:01:36,400 --> 00:01:39,280
 are also highly educated but

23
00:01:39,280 --> 00:01:47,680
 coming from another approach who feel that it's actually

24
00:01:47,680 --> 00:01:54,560
 more through their intense or

25
00:01:54,560 --> 00:01:57,960
 intensive study of the Buddhist teaching.

26
00:01:57,960 --> 00:02:03,860
 But I'd say you have to admit that it is also partially

27
00:02:03,860 --> 00:02:06,720
 coming from their faith in the Buddhist

28
00:02:06,720 --> 00:02:11,210
 teaching that they've had since they were young, most

29
00:02:11,210 --> 00:02:13,280
 likely in most cases.

30
00:02:13,280 --> 00:02:17,380
 Who will say that you need to establish faith first in the

31
00:02:17,380 --> 00:02:19,840
 Buddhist teaching, that faith

32
00:02:19,840 --> 00:02:20,840
 is essential.

33
00:02:20,840 --> 00:02:26,340
 If a person doesn't have faith, they cannot progress in the

34
00:02:26,340 --> 00:02:28,480
 Buddhist teaching.

35
00:02:28,480 --> 00:02:33,860
 So to deal with this, let's look at those two extremes and

36
00:02:33,860 --> 00:02:35,880
 then try to find something

37
00:02:35,880 --> 00:02:41,150
 that I would say is more in the middle, more moderate and

38
00:02:41,150 --> 00:02:44,600
 maybe a little more appropriate.

39
00:02:44,600 --> 00:02:49,090
 So these are kind of straw men or straw people, straw

40
00:02:49,090 --> 00:02:52,400
 arguments that I've set up but I don't

41
00:02:52,400 --> 00:02:55,670
 think they're far off the mark of characterizing the

42
00:02:55,670 --> 00:02:57,920
 opinions of certain Buddhists.

43
00:02:57,920 --> 00:03:04,610
 So first of all, by posting the first straw man, it shows

44
00:03:04,610 --> 00:03:08,280
 the danger of discarding faith

45
00:03:08,280 --> 00:03:14,320
 because doubt is a hindrance and faith is a profitable

46
00:03:14,320 --> 00:03:16,560
 mental faculty.

47
00:03:16,560 --> 00:03:19,940
 Now, this is important to understand and it's important to

48
00:03:19,940 --> 00:03:21,880
 properly understand because in

49
00:03:21,880 --> 00:03:26,920
 Buddhism we don't think of things conceptually.

50
00:03:26,920 --> 00:03:31,070
 It's not an intellectual argument whether faith is useful

51
00:03:31,070 --> 00:03:32,120
 or harmful.

52
00:03:32,120 --> 00:03:35,580
 If you say, "But faith makes people believe in the wrong

53
00:03:35,580 --> 00:03:37,500
 things," if you have faith in

54
00:03:37,500 --> 00:03:41,200
 something that is untrue, it can lead to disaster.

55
00:03:41,200 --> 00:03:42,520
 That's an intellectual argument.

56
00:03:42,520 --> 00:03:47,000
 It doesn't say whether faith is profitable or harmful.

57
00:03:47,000 --> 00:03:50,490
 Faith is profitable according to Buddhism for the simple

58
00:03:50,490 --> 00:03:52,240
 reason that it supports the

59
00:03:52,240 --> 00:03:53,920
 mind.

60
00:03:53,920 --> 00:04:00,300
 If all other things considered equal, you're undertaking

61
00:04:00,300 --> 00:04:04,000
 something and you put aside whether

62
00:04:04,000 --> 00:04:06,600
 that thing is right or wrong.

63
00:04:06,600 --> 00:04:11,220
 The fact that you have faith in it is going to make it

64
00:04:11,220 --> 00:04:14,200
 easier for you to succeed in that

65
00:04:14,200 --> 00:04:15,200
 endeavor.

66
00:04:15,200 --> 00:04:16,200
 Why?

67
00:04:16,200 --> 00:04:17,600
 Because faith is a profitable mind state.

68
00:04:17,600 --> 00:04:21,380
 Faith is something that supports the mind.

69
00:04:21,380 --> 00:04:23,580
 That's what is meant by profitable in Buddhism.

70
00:04:23,580 --> 00:04:25,600
 We don't look at the intellectual argument.

71
00:04:25,600 --> 00:04:27,520
 It's the same thing with eating meat.

72
00:04:27,520 --> 00:04:30,200
 Eating meat is karmically neutral.

73
00:04:30,200 --> 00:04:33,250
 It's ethically neutral because it's not an intellectual

74
00:04:33,250 --> 00:04:33,920
 argument.

75
00:04:33,920 --> 00:04:37,090
 The point is that during the moment that you're eating meat

76
00:04:37,090 --> 00:04:38,880
, there's no reason why you should

77
00:04:38,880 --> 00:04:46,800
 have to be either wholesome or unwholesome of intention.

78
00:04:46,800 --> 00:04:53,990
 Doubt is considered to be unprofitable because it harms the

79
00:04:53,990 --> 00:04:55,000
 mind.

80
00:04:55,000 --> 00:04:57,040
 It scatters the mind.

81
00:04:57,040 --> 00:04:59,600
 It prevents the mind, hinders the mind.

82
00:04:59,600 --> 00:05:01,960
 It's a hindrance because it hinders the mind.

83
00:05:01,960 --> 00:05:12,800
 If you are acting, you're undertaking some activity, all

84
00:05:12,800 --> 00:05:14,280
 other things considered equal,

85
00:05:14,280 --> 00:05:19,030
 doubt is going to prevent you from succeeding in that

86
00:05:19,030 --> 00:05:19,560
 activity.

87
00:05:19,560 --> 00:05:21,880
 It's kind of a narrow definition.

88
00:05:21,880 --> 00:05:25,820
 Broadly speaking, obviously faith can be dangerous and

89
00:05:25,820 --> 00:05:29,160
 obviously doubt can be quite useful.

90
00:05:29,160 --> 00:05:32,720
 Broad is something that intellectually speaking is useful

91
00:05:32,720 --> 00:05:34,720
 in establishing what is true and

92
00:05:34,720 --> 00:05:36,920
 good.

93
00:05:36,920 --> 00:05:44,100
 But practically speaking, from a point of view of actual

94
00:05:44,100 --> 00:05:48,040
 progress, it's the opposite.

95
00:05:48,040 --> 00:05:55,200
 That faith is actually useful and shouldn't be discounted.

96
00:05:55,200 --> 00:05:57,560
 The reason why faith is so dangerous is because it's so

97
00:05:57,560 --> 00:05:58,160
 powerful.

98
00:05:58,160 --> 00:06:04,400
 If you have faith in the wrong thing, it can destroy you,

99
00:06:04,400 --> 00:06:07,600
 it can lead you to destroy and

100
00:06:07,600 --> 00:06:09,760
 harm other people greatly.

101
00:06:09,760 --> 00:06:12,370
 But on the other hand, if you have faith in the right thing

102
00:06:12,370 --> 00:06:13,360
, the power of it.

103
00:06:13,360 --> 00:06:14,720
 So it's like a power tool.

104
00:06:14,720 --> 00:06:18,800
 It's something that is quite powerful.

105
00:06:18,800 --> 00:06:21,120
 Doubt on the other hand is something that is harmful.

106
00:06:21,120 --> 00:06:25,040
 But in that sense, if you have faith in the wrong things, s

107
00:06:25,040 --> 00:06:27,140
owing seeds of doubt can allow

108
00:06:27,140 --> 00:06:30,160
 you to destroy something that is harmful.

109
00:06:30,160 --> 00:06:34,060
 So if you're talking about faith in wrong view and things

110
00:06:34,060 --> 00:06:35,960
 that are faith in God or the

111
00:06:35,960 --> 00:06:41,080
 self or so on, sowing seeds of doubt is harmful in a good

112
00:06:41,080 --> 00:06:42,080
 way.

113
00:06:42,080 --> 00:06:43,080
 That's the point here.

114
00:06:43,080 --> 00:06:48,540
 So it's important to understand what you're talking about

115
00:06:48,540 --> 00:06:51,880
 and to be able to differentiate.

116
00:06:51,880 --> 00:06:57,270
 Actually it is quite clear that the Buddha did not want to

117
00:06:57,270 --> 00:06:58,800
 have us...

118
00:06:58,800 --> 00:07:04,410
 Well, let's look at the other side then before I explain it

119
00:07:04,410 --> 00:07:04,440
.

120
00:07:04,440 --> 00:07:09,850
 On the other side, if anyone who claims that you need

121
00:07:09,850 --> 00:07:13,200
 perfect faith in the teachings or

122
00:07:13,200 --> 00:07:16,860
 that faith in the teachings is necessary before you

123
00:07:16,860 --> 00:07:21,840
 practice, I would say from my admittedly

124
00:07:21,840 --> 00:07:28,080
 partial study of the Buddhist teachings.

125
00:07:28,080 --> 00:07:31,870
 I haven't studied absolutely everything that the Buddha

126
00:07:31,870 --> 00:07:34,720
 taught and commentaries and sub-commentaries

127
00:07:34,720 --> 00:07:38,840
 and so on.

128
00:07:38,840 --> 00:07:42,990
 But I have quite a broad based education in the Buddhist

129
00:07:42,990 --> 00:07:45,320
 teaching and it's quite clear

130
00:07:45,320 --> 00:07:48,520
 to me that that is not what he taught.

131
00:07:48,520 --> 00:07:55,790
 In fact, his teachings on faith, even when he said it's a

132
00:07:55,790 --> 00:07:59,360
 good thing, are by no means

133
00:07:59,360 --> 00:08:02,980
 front and center, are by no means core to the actual

134
00:08:02,980 --> 00:08:03,960
 practice.

135
00:08:03,960 --> 00:08:07,440
 Right faith isn't in the Eightfold Noble Path.

136
00:08:07,440 --> 00:08:10,790
 It's not one of the things that the Buddha singled out as

137
00:08:10,790 --> 00:08:11,760
 necessary.

138
00:08:11,760 --> 00:08:16,100
 And in fact, as we know in the Kalama Sutta, for example,

139
00:08:16,100 --> 00:08:18,240
 there are cases where he was

140
00:08:18,240 --> 00:08:27,120
 critical of faith and pointed out the dangers of faith,

141
00:08:27,120 --> 00:08:30,160
 which are obvious.

142
00:08:30,160 --> 00:08:31,900
 This came to my attention recently because someone was

143
00:08:31,900 --> 00:08:33,000
 saying, "Well, this is what they

144
00:08:33,000 --> 00:08:34,000
 were told.

145
00:08:34,000 --> 00:08:35,980
 They were told that the first thing you have to study the

146
00:08:35,980 --> 00:08:37,160
 Buddha's teachings until you

147
00:08:37,160 --> 00:08:43,240
 gain this perfect faith in them through study."

148
00:08:43,240 --> 00:08:45,130
 After thinking about it for a while, it wasn't the first

149
00:08:45,130 --> 00:08:46,280
 thing, but at one point it came

150
00:08:46,280 --> 00:08:48,920
 to me.

151
00:08:48,920 --> 00:08:52,240
 As I was typing a response to this person's question, after

152
00:08:52,240 --> 00:08:53,720
 I typed it out, I read it,

153
00:08:53,720 --> 00:08:55,920
 what I had replied and I said, "Well, that's exactly it."

154
00:08:55,920 --> 00:08:59,610
 What I'd said is, "There's no reason to believe someone

155
00:08:59,610 --> 00:09:02,360
 when they tell you you have to believe

156
00:09:02,360 --> 00:09:03,360
 something."

157
00:09:03,360 --> 00:09:04,360
 Right?

158
00:09:04,360 --> 00:09:07,180
 So a person is telling you, "You have to believe in the

159
00:09:07,180 --> 00:09:08,560
 Buddha's teaching."

160
00:09:08,560 --> 00:09:10,920
 And they're saying, "Believe me.

161
00:09:10,920 --> 00:09:12,240
 Believe what I say.

162
00:09:12,240 --> 00:09:15,040
 You have to believe what the Buddha said."

163
00:09:15,040 --> 00:09:17,680
 So it's actually kind of a form of question begging.

164
00:09:17,680 --> 00:09:23,030
 Well, in order to believe what the Buddha said, I have to

165
00:09:23,030 --> 00:09:25,520
 first accept that believing

166
00:09:25,520 --> 00:09:31,160
 what someone says completely is a good thing.

167
00:09:31,160 --> 00:09:33,810
 If I'm the sort of person who doesn't believe what people

168
00:09:33,810 --> 00:09:35,480
 say without reason, then there's

169
00:09:35,480 --> 00:09:38,680
 no reason why I would accept what you say.

170
00:09:38,680 --> 00:09:41,110
 It's like, "Why should I believe you when you say I should

171
00:09:41,110 --> 00:09:42,200
 believe the Buddha?"

172
00:09:42,200 --> 00:09:43,840
 It was basically it.

173
00:09:43,840 --> 00:09:47,700
 And I think it forms a kind of question begging, circular

174
00:09:47,700 --> 00:09:48,720
 reasoning.

175
00:09:48,720 --> 00:09:53,440
 Why should I believe you in the first place, let alone

176
00:09:53,440 --> 00:09:55,520
 believe the Buddha?

177
00:09:55,520 --> 00:09:57,840
 And I think that's...

178
00:09:57,840 --> 00:10:01,440
 It's obviously...

179
00:10:01,440 --> 00:10:03,360
 It's a valid argument.

180
00:10:03,360 --> 00:10:06,200
 Why should I?

181
00:10:06,200 --> 00:10:10,960
 Why you say this, what are your reasons for saying I should

182
00:10:10,960 --> 00:10:13,240
 have faith in the Buddha,

183
00:10:13,240 --> 00:10:14,240
 for example?

184
00:10:14,240 --> 00:10:17,870
 And I think that's really how the Buddha would have us

185
00:10:17,870 --> 00:10:19,160
 approach this.

186
00:10:19,160 --> 00:10:23,040
 Obviously, the Buddha said right view is important, and it

187
00:10:23,040 --> 00:10:25,440
's important from the beginning of the

188
00:10:25,440 --> 00:10:26,440
 practice.

189
00:10:26,440 --> 00:10:32,710
 But the question is whether right view means belief in the

190
00:10:32,710 --> 00:10:37,120
 things that the Buddha was teaching.

191
00:10:37,120 --> 00:10:40,510
 And so it's not that you shouldn't believe anything or you

192
00:10:40,510 --> 00:10:42,360
 go into it with an open mind.

193
00:10:42,360 --> 00:10:47,070
 It's not that you should come and practice thinking that

194
00:10:47,070 --> 00:10:49,360
 everything is possible.

195
00:10:49,360 --> 00:10:55,450
 There are certain arguments and teachings that have to be

196
00:10:55,450 --> 00:10:57,480
 presented to a person for

197
00:10:57,480 --> 00:10:58,600
 their evaluation.

198
00:10:58,600 --> 00:11:00,560
 But here is how I would say it has to be approached.

199
00:11:00,560 --> 00:11:04,280
 It has to be approached proportionately.

200
00:11:04,280 --> 00:11:11,750
 A person should have faith proportionately to the reasons

201
00:11:11,750 --> 00:11:15,000
 presented for having or the

202
00:11:15,000 --> 00:11:19,760
 evidence, proportion to the evidence.

203
00:11:19,760 --> 00:11:23,800
 So if someone comes up to you and says that there's a God

204
00:11:23,800 --> 00:11:26,260
 who is going to judge everything

205
00:11:26,260 --> 00:11:29,960
 you do and send you to hell or heaven based on those, based

206
00:11:29,960 --> 00:11:31,160
 on your deeds.

207
00:11:31,160 --> 00:11:36,440
 But you should examine your reasons for believing that.

208
00:11:36,440 --> 00:11:40,480
 If someone says to you, "Hey, if you practice the way I

209
00:11:40,480 --> 00:11:43,000
 teach, it leads you to complete

210
00:11:43,000 --> 00:11:48,630
 enlightenment and nirvana and true freedom from suffering

211
00:11:48,630 --> 00:11:51,160
 and you never have any, it

212
00:11:51,160 --> 00:11:54,440
 leads you to not be born again," and so on and so on.

213
00:11:54,440 --> 00:11:56,360
 Then you have to examine why you should believe that.

214
00:11:56,360 --> 00:12:00,480
 Both of these statements are difficult to believe.

215
00:12:00,480 --> 00:12:05,100
 And so I would say our appropriate response is to be

216
00:12:05,100 --> 00:12:08,880
 skeptical towards those statements.

217
00:12:08,880 --> 00:12:10,880
 I don't think there's anything wrong with that.

218
00:12:10,880 --> 00:12:15,500
 I think someone who is skeptical about the reality of nir

219
00:12:15,500 --> 00:12:18,040
vana, that's reasonable in the

220
00:12:18,040 --> 00:12:21,680
 beginning.

221
00:12:21,680 --> 00:12:25,230
 Because the alternative is to believe something

222
00:12:25,230 --> 00:12:28,080
 disproportionate to the evidence.

223
00:12:28,080 --> 00:12:33,590
 And so this is the other thing that I think needs to be

224
00:12:33,590 --> 00:12:37,120
 pointed out, is the consequences

225
00:12:37,120 --> 00:12:47,060
 of believing something disproportionate to the evidence.

226
00:12:47,060 --> 00:12:53,110
 As long as our belief is proportionate to the evidence, we

227
00:12:53,110 --> 00:12:56,840
 will have a sort of an equilibrium

228
00:12:56,840 --> 00:13:01,120
 in our mind, a certain sense of contentment.

229
00:13:01,120 --> 00:13:07,160
 So you'll never have a feeling of conflict internally.

230
00:13:07,160 --> 00:13:09,440
 If you believe in something, and this is the curious thing

231
00:13:09,440 --> 00:13:10,820
 because obviously if you believe

232
00:13:10,820 --> 00:13:14,710
 in something like the supreme creator God who's going to

233
00:13:14,710 --> 00:13:16,600
 send you to heaven or hell,

234
00:13:16,600 --> 00:13:20,200
 reality is going to come back and hit you hard.

235
00:13:20,200 --> 00:13:24,280
 And you're going to be constantly bombarded by evidence to

236
00:13:24,280 --> 00:13:25,520
 the contrary.

237
00:13:25,520 --> 00:13:30,580
 For example, the widespread suffering in the world or the

238
00:13:30,580 --> 00:13:33,200
 lack of any sort of evidence.

239
00:13:33,200 --> 00:13:37,180
 You're going to be constantly bombarded with doubt because

240
00:13:37,180 --> 00:13:38,960
 it's patently not true.

241
00:13:38,960 --> 00:13:41,160
 It goes against reality.

242
00:13:41,160 --> 00:13:43,580
 But the question is, what if you believe, and this is how

243
00:13:43,580 --> 00:13:44,880
 this question arose, what

244
00:13:44,880 --> 00:13:50,310
 if you believe disproportionately in something that is true

245
00:13:50,310 --> 00:13:50,400
?

246
00:13:50,400 --> 00:13:52,200
 What happens?

247
00:13:52,200 --> 00:13:54,880
 And what's wrong is what was presented to me.

248
00:13:54,880 --> 00:13:57,360
 What's wrong with, as I said, believing completely the

249
00:13:57,360 --> 00:13:59,080
 Buddha's teaching before you have any

250
00:13:59,080 --> 00:14:02,680
 evidence to support it?

251
00:14:02,680 --> 00:14:08,800
 If all you had was other people's say so, what is wrong

252
00:14:08,800 --> 00:14:13,640
 with believing 100% in everything

253
00:14:13,640 --> 00:14:15,880
 that the Buddha said?

254
00:14:15,880 --> 00:14:24,110
 Now, more likely or more common is the idea that textual

255
00:14:24,110 --> 00:14:26,640
 study is enough.

256
00:14:26,640 --> 00:14:28,720
 So it's not actually without any evidence.

257
00:14:28,720 --> 00:14:31,600
 But the evidence is arguments that have been presented by

258
00:14:31,600 --> 00:14:32,360
 the Buddha.

259
00:14:32,360 --> 00:14:35,140
 So through reading the Buddha's teaching and the comment

260
00:14:35,140 --> 00:14:36,800
aries and sub-commentaries, it

261
00:14:36,800 --> 00:14:41,270
 is often claimed that that is enough evidence to provide

262
00:14:41,270 --> 00:14:43,800
 reason to have perfect faith in

263
00:14:43,800 --> 00:14:44,800
 the Buddha's teaching.

264
00:14:44,800 --> 00:14:48,160
 And that's also patently untrue.

265
00:14:48,160 --> 00:14:50,200
 But let's talk about that after.

266
00:14:50,200 --> 00:14:53,200
 First let's talk about what if you had no evidence?

267
00:14:53,200 --> 00:14:56,590
 But suppose someone told you, okay, you practice insight

268
00:14:56,590 --> 00:14:59,000
 meditation and you become enlightened.

269
00:14:59,000 --> 00:15:03,160
 What would be wrong with having total faith in that?

270
00:15:03,160 --> 00:15:08,410
 The first thing that comes to mind is it actually, faith

271
00:15:08,410 --> 00:15:13,640
 actually acts as an inhibitor to investigation.

272
00:15:13,640 --> 00:15:16,990
 I investigate something if you already know it to be true

273
00:15:16,990 --> 00:15:19,200
 and you find this with Buddhists.

274
00:15:19,200 --> 00:15:23,240
 Often people read, so let's just lump it in there, people

275
00:15:23,240 --> 00:15:25,280
 will do the reading and come

276
00:15:25,280 --> 00:15:28,610
 up with these reasons to believe or maybe that's not even

277
00:15:28,610 --> 00:15:31,200
 with reasons to believe, but

278
00:15:31,200 --> 00:15:33,490
 they read about these teachings and it sounds good and the

279
00:15:33,490 --> 00:15:34,880
 Buddha is so good at presenting

280
00:15:34,880 --> 00:15:38,800
 it and so they believe it.

281
00:15:38,800 --> 00:15:42,480
 They come to this faith and they feel the rapture and the

282
00:15:42,480 --> 00:15:44,800
 pleasure of hearing the Buddha's

283
00:15:44,800 --> 00:15:50,190
 arguments and seeing how he debates and wins debates and so

284
00:15:50,190 --> 00:15:50,760
 on.

285
00:15:50,760 --> 00:15:54,650
 And they just feel how great it is and so they just believe

286
00:15:54,650 --> 00:15:56,520
 what the Buddha taught.

287
00:15:56,520 --> 00:16:00,800
 And you'll find that these people are quite often disincl

288
00:16:00,800 --> 00:16:03,200
ined to actually practice the

289
00:16:03,200 --> 00:16:04,200
 teachings.

290
00:16:04,200 --> 00:16:06,880
 They feel content in their knowledge.

291
00:16:06,880 --> 00:16:12,410
 And in fact you find this with scholars quite often that

292
00:16:12,410 --> 00:16:16,080
 the more they study, the less inclined

293
00:16:16,080 --> 00:16:19,200
 they are to, or maybe not the less inclined, but it

294
00:16:19,200 --> 00:16:22,200
 certainly doesn't make them more inclined.

295
00:16:22,200 --> 00:16:25,120
 It certainly doesn't bring them to the practice.

296
00:16:25,120 --> 00:16:29,840
 They can actually become complacent.

297
00:16:29,840 --> 00:16:36,220
 But beyond that, a second problem is that even though,

298
00:16:36,220 --> 00:16:39,600
 suppose you have X and it's in line

299
00:16:39,600 --> 00:16:43,350
 with reality and you have perfect faith in it without any

300
00:16:43,350 --> 00:16:45,560
 knowledge, the problem is that

301
00:16:45,560 --> 00:16:48,240
 faith isn't enough to support that.

302
00:16:48,240 --> 00:16:50,920
 It isn't enough even to bring you to that realization.

303
00:16:50,920 --> 00:16:53,680
 So what you're dealing with is not only the faith in it,

304
00:16:53,680 --> 00:16:55,200
 but you're dealing with wrong

305
00:16:55,200 --> 00:16:59,040
 view that actually contradicts it in the case of Buddhism.

306
00:16:59,040 --> 00:17:03,180
 So suppose you have the idea of non-self and you have a

307
00:17:03,180 --> 00:17:06,280
 person who believes 100% in the

308
00:17:06,280 --> 00:17:08,320
 truth or they say they do anyway.

309
00:17:08,320 --> 00:17:13,880
 They put their faith in and they suppress any kind of doubt

310
00:17:13,880 --> 00:17:14,300
.

311
00:17:14,300 --> 00:17:21,600
 The problem is that this doubt is going to act very similar

312
00:17:21,600 --> 00:17:25,680
 to contradictory evidence.

313
00:17:25,680 --> 00:17:29,620
 So even though there is no contradictory evidence, the more

314
00:17:29,620 --> 00:17:31,800
 you look or reality supports the

315
00:17:31,800 --> 00:17:35,680
 idea of non-self, your own doubts and your own wrong views

316
00:17:35,680 --> 00:17:37,640
 are going to conflict with

317
00:17:37,640 --> 00:17:38,640
 your faith.

318
00:17:38,640 --> 00:17:47,090
 And so you're constantly in a state of upset, of distress

319
00:17:47,090 --> 00:17:51,140
 because of the conflict.

320
00:17:51,140 --> 00:17:52,840
 It doesn't actually work.

321
00:17:52,840 --> 00:18:02,480
 Faith isn't sufficient to propel a person towards truth.

322
00:18:02,480 --> 00:18:04,840
 And in fact the idea that faith is necessary from the

323
00:18:04,840 --> 00:18:06,640
 beginning is I don't think supported

324
00:18:06,640 --> 00:18:08,720
 at all in the texts.

325
00:18:08,720 --> 00:18:18,040
 Right view should be an understanding that one gains

326
00:18:18,040 --> 00:18:22,560
 through the practice.

327
00:18:22,560 --> 00:18:28,310
 So preliminary right view for a person who hasn't yet

328
00:18:28,310 --> 00:18:31,240
 practiced meditation is not the

329
00:18:31,240 --> 00:18:34,800
 view that nirvana is true or so on.

330
00:18:34,800 --> 00:18:39,020
 It's right view in a general sense of understanding reality

331
00:18:39,020 --> 00:18:39,320
.

332
00:18:39,320 --> 00:18:43,110
 It's actually in relation to the building blocks of reality

333
00:18:43,110 --> 00:18:43,160
.

334
00:18:43,160 --> 00:18:50,400
 It has to do with a vision of what is the nature and what

335
00:18:50,400 --> 00:18:54,320
 is the makeup of experience.

336
00:18:54,320 --> 00:19:00,080
 And so what a person should have faith in in the beginning

337
00:19:00,080 --> 00:19:03,400
 is the basic nature of reality.

338
00:19:03,400 --> 00:19:07,430
 And that's only important because otherwise you can't

339
00:19:07,430 --> 00:19:09,680
 actually approach reality.

340
00:19:09,680 --> 00:19:12,830
 So if a person is approaching reality from the point of

341
00:19:12,830 --> 00:19:14,880
 view of people, places and things

342
00:19:14,880 --> 00:19:17,850
 or the point of view of an external three dimensional

343
00:19:17,850 --> 00:19:19,640
 reality and their thinking in

344
00:19:19,640 --> 00:19:26,210
 terms of concepts and in terms of dimensions and so on,

345
00:19:26,210 --> 00:19:30,600
 they'll never be able to experience

346
00:19:30,600 --> 00:19:31,600
 their experiences.

347
00:19:31,600 --> 00:19:34,270
 They'll never be able to observe their experiences

348
00:19:34,270 --> 00:19:35,840
 objectively and clearly.

349
00:19:35,840 --> 00:19:38,320
 Person has to shift their view.

350
00:19:38,320 --> 00:19:41,540
 And so a preliminary right view that the Buddha talks about

351
00:19:41,540 --> 00:19:43,520
 is shifting that view to see things

352
00:19:43,520 --> 00:19:45,940
 in terms of experience.

353
00:19:45,940 --> 00:19:50,490
 When you move to be aware of the movement, to not think of

354
00:19:50,490 --> 00:19:52,560
 it as your foot moving or

355
00:19:52,560 --> 00:19:55,840
 moving out, moving here, moving there or so on.

356
00:19:55,840 --> 00:20:00,000
 In terms of the experience of the movement.

357
00:20:00,000 --> 00:20:03,480
 When the stomach rises, it's the experience of the rising.

358
00:20:03,480 --> 00:20:07,530
 When you feel pain, it's the experience of the pain and so

359
00:20:07,530 --> 00:20:07,980
 on.

360
00:20:07,980 --> 00:20:11,900
 And there is no need beyond that for faith in nirvana,

361
00:20:11,900 --> 00:20:14,360
 faith in this or that or the other

362
00:20:14,360 --> 00:20:15,360
 thing.

363
00:20:15,360 --> 00:20:20,970
 But proportionately speaking, that sort of faith, there

364
00:20:20,970 --> 00:20:23,360
 should be an amount of faith,

365
00:20:23,360 --> 00:20:29,690
 a certain amount of faith proportionate to the reasonable

366
00:20:29,690 --> 00:20:32,320
 nature of the claims.

367
00:20:32,320 --> 00:20:37,120
 And at that point, there is also the idea of authority.

368
00:20:37,120 --> 00:20:43,130
 So a person teaches you meditation and you begin to realize

369
00:20:43,130 --> 00:20:44,560
 the truth.

370
00:20:44,560 --> 00:20:49,100
 And the more they advise you, the more you understand.

371
00:20:49,100 --> 00:20:54,920
 And so you begin to believe the things that they say.

372
00:20:54,920 --> 00:20:56,850
 If they make claims about what's going to come next, they

373
00:20:56,850 --> 00:20:58,180
'll say, "Well, if you practice

374
00:20:58,180 --> 00:21:00,780
 along this path, this happens."

375
00:21:00,780 --> 00:21:04,280
 So you have some measure of faith.

376
00:21:04,280 --> 00:21:07,510
 And again, it's not all or none, but you have some measure

377
00:21:07,510 --> 00:21:10,520
 of faith because they're reliable.

378
00:21:10,520 --> 00:21:11,920
 And then it turns out to be true.

379
00:21:11,920 --> 00:21:15,460
 So you have greater faith in the things that they say.

380
00:21:15,460 --> 00:21:17,780
 It doesn't mean that you ever have to have full faith and

381
00:21:17,780 --> 00:21:19,060
 you shouldn't and you can't

382
00:21:19,060 --> 00:21:22,880
 because full faith in what someone says isn't enough.

383
00:21:22,880 --> 00:21:26,470
 Again, even if it doesn't conflict with reality, it will

384
00:21:26,470 --> 00:21:29,400
 conflict with your own lack of understanding

385
00:21:29,400 --> 00:21:33,040
 and realization until you actually realize it for yourself.

386
00:21:33,040 --> 00:21:35,770
 That's why there's a difference between what we call Bhav

387
00:21:35,770 --> 00:21:37,440
ana santa, which means faith or

388
00:21:37,440 --> 00:21:43,040
 confidence based on meditation or actual development.

389
00:21:43,040 --> 00:21:47,000
 That kind of faith is unshakable.

390
00:21:47,000 --> 00:21:49,460
 And that's really what the Buddha was talking about when he

391
00:21:49,460 --> 00:21:50,760
 talked about right faith.

392
00:21:50,760 --> 00:21:53,710
 It's something that is balanced with wisdom or combined

393
00:21:53,710 --> 00:21:55,760
 with wisdom because it comes through

394
00:21:55,760 --> 00:21:57,840
 seeing things as they are.

395
00:21:57,840 --> 00:22:01,040
 You don't ever really have to believe things

396
00:22:01,040 --> 00:22:03,920
 disproportionate to the evidence.

397
00:22:03,920 --> 00:22:08,610
 But again, the danger being a disproportionate doubt, which

398
00:22:08,610 --> 00:22:10,480
 is also quite common.

399
00:22:10,480 --> 00:22:11,480
 People think it's one or the other.

400
00:22:11,480 --> 00:22:14,960
 So if you don't believe something, you're skeptical of it.

401
00:22:14,960 --> 00:22:18,530
 And so they'll have disproportionate doubt, constantly doub

402
00:22:18,530 --> 00:22:20,840
ting the things that they experience.

403
00:22:20,840 --> 00:22:23,480
 Anyway, just some thoughts on faith.

404
00:22:23,480 --> 00:22:26,840
 I certainly don't believe one extreme or the other.

405
00:22:26,840 --> 00:22:31,400
 I think I've said what I meant to say that faith is useful,

406
00:22:31,400 --> 00:22:33,680
 beneficial, but dangerous

407
00:22:33,680 --> 00:22:35,880
 if it's disproportionate.

