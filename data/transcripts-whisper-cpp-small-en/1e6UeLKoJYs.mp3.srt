1
00:00:00,000 --> 00:00:14,960
 Hello and welcome to our evening session.

2
00:00:14,960 --> 00:00:20,920
 Welcome.

3
00:00:20,920 --> 00:00:30,880
 Welcome.

4
00:00:30,880 --> 00:00:59,720
 Welcome.

5
00:00:59,720 --> 00:01:25,800
 We often talk about

6
00:01:25,800 --> 00:01:33,100
 the sorts of things we're looking for in insight meditation

7
00:01:33,100 --> 00:01:34,320
 practice.

8
00:01:34,320 --> 00:01:42,540
 How we're looking to understand impermanence, suffering non

9
00:01:42,540 --> 00:01:44,880
-self, to cultivate insight and

10
00:01:44,880 --> 00:02:00,280
 understanding about reality deeper than before.

11
00:02:00,280 --> 00:02:05,390
 What we should find, what we're looking for in the practice

12
00:02:05,390 --> 00:02:05,760
.

13
00:02:05,760 --> 00:02:12,610
 At the same time, we need also to be aware of the things

14
00:02:12,610 --> 00:02:16,240
 that we're not looking for,

15
00:02:16,240 --> 00:02:24,440
 those things that are a distraction from the practice.

16
00:02:24,440 --> 00:02:29,680
 So we call these the vipassanupa-kilesa, the imperfections

17
00:02:29,680 --> 00:02:31,000
 of insight.

18
00:02:31,000 --> 00:02:34,040
 They're not bad.

19
00:02:34,040 --> 00:02:41,840
 They're not unwholesome, how most of them aren't.

20
00:02:41,840 --> 00:02:43,960
 But they're distracting.

21
00:02:43,960 --> 00:02:52,730
 They take you away from your investigation, your objective

22
00:02:52,730 --> 00:02:56,720
 observation of reality.

23
00:02:56,720 --> 00:02:59,490
 So we have to be aware of what these are and we have to be

24
00:02:59,490 --> 00:03:00,640
 mindful of them when they come

25
00:03:00,640 --> 00:03:03,640
 up.

26
00:03:03,640 --> 00:03:14,120
 The first one is called obasa.

27
00:03:14,120 --> 00:03:18,910
 It's this interesting phenomenon that occurs when you med

28
00:03:18,910 --> 00:03:21,440
itate to begin to see things.

29
00:03:21,440 --> 00:03:23,800
 It's not just mental images.

30
00:03:23,800 --> 00:03:30,360
 You actually really see lights, colors, pictures, a lot of

31
00:03:30,360 --> 00:03:31,920
 strange things.

32
00:03:31,920 --> 00:03:36,640
 For some meditators, even though their eyes will close, it

33
00:03:36,640 --> 00:03:39,080
'll feel like the room has suddenly

34
00:03:39,080 --> 00:03:43,760
 become brighter, like the door is open.

35
00:03:43,760 --> 00:03:45,960
 It's a common one.

36
00:03:45,960 --> 00:03:49,250
 Meditator will be in their hut in the forest and suddenly

37
00:03:49,250 --> 00:03:51,000
 they'll see bright light and

38
00:03:51,000 --> 00:03:53,800
 it'll feel like someone has, or the door has swung open and

39
00:03:53,800 --> 00:03:55,120
 they'll actually open their

40
00:03:55,120 --> 00:03:57,480
 eyes to look.

41
00:03:57,480 --> 00:04:02,180
 Some it feels like the roof has disappeared and the sun is

42
00:04:02,180 --> 00:04:04,600
 pouring down, bright light

43
00:04:04,600 --> 00:04:10,080
 like this, brightness.

44
00:04:10,080 --> 00:04:12,840
 Some people will see colors or pictures.

45
00:04:12,840 --> 00:04:22,680
 They'll be quite distracting and for some people, not just

46
00:04:22,680 --> 00:04:26,960
 a distraction, but will be

47
00:04:26,960 --> 00:04:33,200
 misunderstood as being a part of the path.

48
00:04:33,200 --> 00:04:37,660
 Some people will see lights and colors and pictures and

49
00:04:37,660 --> 00:04:40,080
 want to investigate them.

50
00:04:40,080 --> 00:04:45,080
 That really goes for all of 10 of these.

51
00:04:45,080 --> 00:04:48,630
 The vipassanupakiles, so they're things that are not bad

52
00:04:48,630 --> 00:04:51,200
 per se, but they're not the path.

53
00:04:51,200 --> 00:04:57,570
 If you mistake them, if you get caught up in them, they'll

54
00:04:57,570 --> 00:05:00,240
 take you off the path.

55
00:05:00,240 --> 00:05:02,200
 This one comes for some people.

56
00:05:02,200 --> 00:05:04,560
 Comes for people who have strong concentration.

57
00:05:04,560 --> 00:05:07,400
 For others, they might never see anything.

58
00:05:07,400 --> 00:05:09,600
 It's not obviously not what we're looking for.

59
00:05:09,600 --> 00:05:11,880
 It's a distraction.

60
00:05:11,880 --> 00:05:14,190
 When you see, you have to know to yourself seeing, seeing

61
00:05:14,190 --> 00:05:15,440
 whatever you see, no matter

62
00:05:15,440 --> 00:05:22,160
 how long you see it for.

63
00:05:22,160 --> 00:05:27,440
 Important theme here is that none of this is any special.

64
00:05:27,440 --> 00:05:29,360
 It's all just experience.

65
00:05:29,360 --> 00:05:31,120
 It comes and it goes.

66
00:05:31,120 --> 00:05:35,120
 There's nothing special about seeing things.

67
00:05:35,120 --> 00:05:38,420
 When you take it as special, you lose track of what's

68
00:05:38,420 --> 00:05:40,720
 really important and that's cultivating

69
00:05:40,720 --> 00:05:46,680
 insight and understanding.

70
00:05:46,680 --> 00:05:48,800
 Second one is peti.

71
00:05:48,800 --> 00:05:53,160
 Peti can come in many different forms.

72
00:05:53,160 --> 00:06:03,440
 For some people, it's rushes of energy through their body.

73
00:06:03,440 --> 00:06:05,080
 Some people, it'll be crying.

74
00:06:05,080 --> 00:06:12,760
 Just cry for an hour or two hours even.

75
00:06:12,760 --> 00:06:16,040
 Cry for a long time anyway.

76
00:06:16,040 --> 00:06:18,680
 Some it's laughing or yawning or that kind of thing.

77
00:06:18,680 --> 00:06:21,640
 That's somewhat rare, but it does happen.

78
00:06:21,640 --> 00:06:28,600
 A common one is feeling very light.

79
00:06:28,600 --> 00:06:31,290
 Your whole body might feel light and you feel as if you're

80
00:06:31,290 --> 00:06:32,000
 floating.

81
00:06:32,000 --> 00:06:34,120
 Some people apparently do actually float.

82
00:06:34,120 --> 00:06:37,000
 It's apparently possible.

83
00:06:37,000 --> 00:06:44,080
 I'm sure scientists would beg to differ, but meditators

84
00:06:44,080 --> 00:06:48,160
 have reported that they've actually

85
00:06:48,160 --> 00:06:50,920
 levitated off their pillows.

86
00:06:50,920 --> 00:06:59,070
 If it ever happens to you, you're supposed to keep a pencil

87
00:06:59,070 --> 00:07:02,360
 or a pen by your sitting

88
00:07:02,360 --> 00:07:05,590
 mat so that if you float up and float all the way to the

89
00:07:05,590 --> 00:07:07,240
 ceiling, make a mark on the

90
00:07:07,240 --> 00:07:12,520
 ceiling just to prove that you were there.

91
00:07:12,520 --> 00:07:18,680
 There was one guy who actually did this, so I don't know.

92
00:07:18,680 --> 00:07:22,630
 At any rate, it's quite common for there to be a feeling of

93
00:07:22,630 --> 00:07:23,640
 floating.

94
00:07:23,640 --> 00:07:27,630
 Another really common one is the swaying where you rock

95
00:07:27,630 --> 00:07:29,000
 back and forth.

96
00:07:29,000 --> 00:07:32,840
 You feel your body rocking.

97
00:07:32,840 --> 00:07:37,380
 When I first did my course, my body was really strongly

98
00:07:37,380 --> 00:07:38,360
 rocking.

99
00:07:38,360 --> 00:07:42,240
 I thought, "Oh, this is fun."

100
00:07:42,240 --> 00:07:45,520
 It's easy to sit for a long time that way.

101
00:07:45,520 --> 00:07:51,790
 You get into this pattern, but you aren't able to see

102
00:07:51,790 --> 00:07:53,600
 suffering.

103
00:07:53,600 --> 00:07:57,960
 You're not able to see things clearly because you're

104
00:07:57,960 --> 00:07:59,160
 enjoying it.

105
00:07:59,160 --> 00:08:03,520
 There's pleasure associated with the rocking.

106
00:08:03,520 --> 00:08:10,520
 I went to my teacher and he wasn't too happy.

107
00:08:10,520 --> 00:08:13,320
 These aren't bad things, but they're distracting.

108
00:08:13,320 --> 00:08:17,400
 If you don't stop them, this one, Ajay Lompon Chodok, this

109
00:08:17,400 --> 00:08:19,760
 guy in Bangkok, he's a very famous

110
00:08:19,760 --> 00:08:20,760
 teacher.

111
00:08:20,760 --> 00:08:23,520
 He said, "If it doesn't stop, you have to tell it to stop."

112
00:08:23,520 --> 00:08:26,520
 He said, "Stop."

113
00:08:26,520 --> 00:08:31,160
 He said, "Kilei kodu."

114
00:08:31,160 --> 00:08:36,400
 Kilei says, "Dudu" means stubborn.

115
00:08:36,400 --> 00:08:37,600
 The defilements are stubborn.

116
00:08:37,600 --> 00:08:58,560
 This is what we have.

117
00:08:58,560 --> 00:09:01,840
 Then Pasadi, next one.

118
00:09:01,840 --> 00:09:05,000
 Maybe these aren't in order, but Pasadi is another one.

119
00:09:05,000 --> 00:09:07,720
 Pasadi is calm or quiet.

120
00:09:07,720 --> 00:09:09,080
 Pasadi is quiet.

121
00:09:09,080 --> 00:09:13,540
 Up until this point, especially right before these begin to

122
00:09:13,540 --> 00:09:15,880
 arise, the mind will have been

123
00:09:15,880 --> 00:09:17,640
 tortured and chaotic.

124
00:09:17,640 --> 00:09:20,440
 Then suddenly the mind calms down, quiets down.

125
00:09:20,440 --> 00:09:23,110
 This is a common one for those of you who aren't practicing

126
00:09:23,110 --> 00:09:24,120
 in our tradition.

127
00:09:24,120 --> 00:09:27,520
 This is a common one if you're meditating on your own.

128
00:09:27,520 --> 00:09:29,920
 It's a common one to fall into because you get to a very

129
00:09:29,920 --> 00:09:31,280
 quiet state and you don't know

130
00:09:31,280 --> 00:09:34,240
 how to proceed further.

131
00:09:34,240 --> 00:09:38,680
 This quiet state is about the limit of samatha meditation.

132
00:09:38,680 --> 00:09:44,380
 Without guidance in vipassana meditation, you get stuck

133
00:09:44,380 --> 00:09:45,360
 there.

134
00:09:45,360 --> 00:09:48,020
 I've often given talks in places where people practice

135
00:09:48,020 --> 00:09:49,720
 meditation and someone would ask

136
00:09:49,720 --> 00:09:53,490
 this question, "What do you do when you feel very calm or

137
00:09:53,490 --> 00:09:54,960
 feel very quiet?"

138
00:09:54,960 --> 00:09:55,960
 There's just nothing.

139
00:09:55,960 --> 00:09:59,640
 Nothing to note, nothing to be mindful of.

140
00:09:59,640 --> 00:10:04,480
 Well, then simply you would note quiet, quiet.

141
00:10:04,480 --> 00:10:10,740
 I've often instructed in this way and it's really inspiring

142
00:10:10,740 --> 00:10:13,760
 to see people be able to

143
00:10:13,760 --> 00:10:18,040
 get past that by noting quiet, quiet.

144
00:10:18,040 --> 00:10:21,610
 Then of course the mind starts up again because they get

145
00:10:21,610 --> 00:10:22,640
 out of this.

146
00:10:22,640 --> 00:10:26,470
 The thing about these states is they become sort of like a

147
00:10:26,470 --> 00:10:28,240
 broken record where you get

148
00:10:28,240 --> 00:10:29,560
 stuck in them.

149
00:10:29,560 --> 00:10:48,290
 So when you pull out of them, you get back into a more

150
00:10:48,290 --> 00:10:55,360
 ordinary state.

151
00:10:55,360 --> 00:11:00,400
 Then we have suka and upeka.

152
00:11:00,400 --> 00:11:06,240
 Some people feel very happy, intense bliss.

153
00:11:06,240 --> 00:11:07,240
 That's a good thing, right?

154
00:11:07,240 --> 00:11:10,160
 There's nothing wrong with bliss or happiness.

155
00:11:10,160 --> 00:11:11,880
 Problem is we become caught up in it.

156
00:11:11,880 --> 00:11:13,800
 A meditator isn't mindful.

157
00:11:13,800 --> 00:11:17,600
 It doesn't say to themselves happy, happy.

158
00:11:17,600 --> 00:11:25,680
 Or if they feel calm, equanimist, they don't say calm, calm

159
00:11:25,680 --> 00:11:25,760
.

160
00:11:25,760 --> 00:11:32,040
 They'll get caught up in it and they can get lost in it.

161
00:11:32,040 --> 00:11:34,040
 They won't progress.

162
00:11:34,040 --> 00:11:39,680
 I knew a monk once.

163
00:11:39,680 --> 00:11:43,000
 I still know him, but I haven't seen him in a long time.

164
00:11:43,000 --> 00:11:49,040
 Totally and unabashedly, unashamedly addicted to happiness.

165
00:11:49,040 --> 00:12:00,030
 He'd find anyway he ended up getting a CD player and he

166
00:12:00,030 --> 00:12:06,480
 would play Pink Floyd or something.

167
00:12:06,480 --> 00:12:09,810
 When he meditated he would play some very, all sorts of

168
00:12:09,810 --> 00:12:11,680
 different kinds of music I even

169
00:12:11,680 --> 00:12:13,920
 think, which is not really allowed for monks.

170
00:12:13,920 --> 00:12:16,810
 He'd put on these big headphones and he'd just lie there

171
00:12:16,810 --> 00:12:18,440
 and meditate or sit there.

172
00:12:18,440 --> 00:12:20,160
 He's actually a very strong meditator.

173
00:12:20,160 --> 00:12:28,040
 He's this guy who could sit rigid with a huge smile on his

174
00:12:28,040 --> 00:12:30,720
 face for hours.

175
00:12:30,720 --> 00:12:37,240
 I'm not too sure he got into insight.

176
00:12:37,240 --> 00:12:39,120
 Nowadays he does yoga, I think.

177
00:12:39,120 --> 00:12:48,880
 So I don't know.

178
00:12:48,880 --> 00:12:59,360
 Then we have Jnana.

179
00:12:59,360 --> 00:13:04,600
 Jnana I think comes earlier, but Jnana is knowledge.

180
00:13:04,600 --> 00:13:05,600
 This is a common one.

181
00:13:05,600 --> 00:13:08,400
 It's not usually that difficult for meditators, but it can

182
00:13:08,400 --> 00:13:10,000
 be distracting nonetheless.

183
00:13:10,000 --> 00:13:14,600
 Jnana means knowledge about any number of things.

184
00:13:14,600 --> 00:13:16,200
 It can be worldly knowledge.

185
00:13:16,200 --> 00:13:19,520
 You're able to figure out all your problems in life, come

186
00:13:19,520 --> 00:13:21,160
 up with all sorts of plans and

187
00:13:21,160 --> 00:13:22,160
 solutions.

188
00:13:22,160 --> 00:13:27,120
 I had one guy who came up with a perfect business plan.

189
00:13:27,120 --> 00:13:30,310
 He started to do a meditation course and he just said he'd

190
00:13:30,310 --> 00:13:32,080
 come up with the perfect business

191
00:13:32,080 --> 00:13:36,500
 plan and he said in five years he was sure to be a

192
00:13:36,500 --> 00:13:39,840
 millionaire or a billionaire.

193
00:13:39,840 --> 00:13:44,340
 He said in five years I'll come back and I'll buy you,

194
00:13:44,340 --> 00:13:46,720
 build you a meditation.

195
00:13:46,720 --> 00:13:50,680
 He made me all these promises.

196
00:13:50,680 --> 00:13:54,160
 I tried to keep him, tried to get him to stay.

197
00:13:54,160 --> 00:13:56,000
 He left and I never saw him again.

198
00:13:56,000 --> 00:13:58,800
 I'm pretty sure he never became a millionaire.

199
00:13:58,800 --> 00:14:01,600
 Although if he didn't he's listening out there.

200
00:14:01,600 --> 00:14:08,360
 You owe me some goodies.

201
00:14:08,360 --> 00:14:09,360
 Not likely.

202
00:14:09,360 --> 00:14:10,600
 You'll get caught up.

203
00:14:10,600 --> 00:14:15,610
 It's very easy to get caught up in the mind because you're

204
00:14:15,610 --> 00:14:16,880
 all alone.

205
00:14:16,880 --> 00:14:18,840
 Reality.

206
00:14:18,840 --> 00:14:24,480
 You get caught, you get pulled away from reality.

207
00:14:24,480 --> 00:14:29,030
 You end up in fantasy, solving all your problems only to

208
00:14:29,030 --> 00:14:32,120
 find that the solutions don't pan out

209
00:14:32,120 --> 00:14:35,520
 in reality.

210
00:14:35,520 --> 00:14:38,080
 But whether they do or they don't is not the point there.

211
00:14:38,080 --> 00:14:39,560
 This is not the practice.

212
00:14:39,560 --> 00:14:56,160
 This gets you away from the practice.

213
00:14:56,160 --> 00:15:01,000
 Yana.

214
00:15:01,000 --> 00:15:07,040
 Then we have Bhagaha.

215
00:15:07,040 --> 00:15:08,040
 Which is energy.

216
00:15:08,040 --> 00:15:10,520
 Bhagaha means very strong energy.

217
00:15:10,520 --> 00:15:13,920
 Bhagaha is a good word.

218
00:15:13,920 --> 00:15:18,480
 Very strong energy.

219
00:15:18,480 --> 00:15:21,080
 This one comes in to various degrees.

220
00:15:21,080 --> 00:15:24,800
 These things are only a problem for certain people.

221
00:15:24,800 --> 00:15:28,290
 I always tell this story about this woman who just couldn't

222
00:15:28,290 --> 00:15:29,240
 stop herself.

223
00:15:29,240 --> 00:15:32,040
 She just in the middle of it, she just got up and she

224
00:15:32,040 --> 00:15:34,240
 started walking, rapidly walking

225
00:15:34,240 --> 00:15:37,020
 around the monastery, around and around and around and she

226
00:15:37,020 --> 00:15:38,320
 couldn't stop herself.

227
00:15:38,320 --> 00:15:40,280
 She just so much energy.

228
00:15:40,280 --> 00:15:45,160
 That's maybe the extreme case.

229
00:15:45,160 --> 00:15:49,140
 But it is common for people to have bouts of intense energy

230
00:15:49,140 --> 00:15:51,600
, feeling like they can practice

231
00:15:51,600 --> 00:15:53,920
 all day and all night.

232
00:15:53,920 --> 00:15:54,920
 Just not a bad thing.

233
00:15:54,920 --> 00:15:56,760
 It's not a bad thing to have good energy.

234
00:15:56,760 --> 00:15:59,280
 But it's said, "Viriyeṇa dukkha-majidhi."

235
00:15:59,280 --> 00:16:01,760
 Through effort, you overcome suffering.

236
00:16:01,760 --> 00:16:03,920
 But that effort has to be well-directed.

237
00:16:03,920 --> 00:16:10,800
 If you get caught up in the energy and just be pleased, if

238
00:16:10,800 --> 00:16:14,400
 you're pleased by the energy,

239
00:16:14,400 --> 00:16:15,560
 you get lost in it.

240
00:16:15,560 --> 00:16:22,330
 You lose track of your practice and you just sit there,

241
00:16:22,330 --> 00:16:25,040
 energetic and all.

242
00:16:25,040 --> 00:16:31,350
 And we have adi mokha, which is sadhā, which is confidence

243
00:16:31,350 --> 00:16:32,680
 or faith.

244
00:16:32,680 --> 00:16:35,240
 This is a common one.

245
00:16:35,240 --> 00:16:37,360
 Meditator will come and tell you that they're just...

246
00:16:37,360 --> 00:16:40,160
 I had a meditator today come and tell me that I'm very

247
00:16:40,160 --> 00:16:42,160
 impressed with the practice and how

248
00:16:42,160 --> 00:16:43,920
 great it is.

249
00:16:43,920 --> 00:16:49,640
 Some people, the common one here as a meditator will start

250
00:16:49,640 --> 00:16:52,800
 to make plans to ordain or leave

251
00:16:52,800 --> 00:16:53,800
 home.

252
00:16:53,800 --> 00:16:58,760
 They'll start thinking about family and friends and wanting

253
00:16:58,760 --> 00:17:01,560
 to spread the teaching to them.

254
00:17:01,560 --> 00:17:03,400
 This one's common.

255
00:17:03,400 --> 00:17:07,640
 When you get to this certain stage, these things arise for

256
00:17:07,640 --> 00:17:09,000
 the meditator.

257
00:17:09,000 --> 00:17:15,280
 A big one is confidence.

258
00:17:15,280 --> 00:17:18,600
 They get really caught up in this great faith.

259
00:17:18,600 --> 00:17:21,820
 Not even faith, but perfect confidence because of the

260
00:17:21,820 --> 00:17:23,360
 results they've seen.

261
00:17:23,360 --> 00:17:27,150
 Even though it's only preliminary results, they gain great

262
00:17:27,150 --> 00:17:28,840
 confidence and get caught

263
00:17:28,840 --> 00:17:32,640
 up in that.

264
00:17:32,640 --> 00:17:35,590
 And during that time, they get caught up and you get over

265
00:17:35,590 --> 00:17:37,960
confident and then you stop meditating.

266
00:17:37,960 --> 00:17:42,510
 You're very sure of yourself and so you fail to be careful

267
00:17:42,510 --> 00:17:47,160
 and meticulous about the practice.

268
00:17:47,160 --> 00:17:50,800
 It actually takes away from your practice.

269
00:17:50,800 --> 00:17:55,720
 And they have upatāna.

270
00:17:55,720 --> 00:17:57,560
 The upatāna is a little bit interesting.

271
00:17:57,560 --> 00:18:01,060
 Upatāna actually means mindfulness, which you would think

272
00:18:01,060 --> 00:18:02,360
 would be a good one.

273
00:18:02,360 --> 00:18:06,340
 But this will be the first taste that the meditator has of

274
00:18:06,340 --> 00:18:08,760
 being really and truly mindful.

275
00:18:08,760 --> 00:18:16,260
 It will be easy for them to get caught up in that in the

276
00:18:16,260 --> 00:18:18,080
 sense of thinking suddenly

277
00:18:18,080 --> 00:18:24,470
 that they're perfectly mindful and thus losing or giving up

278
00:18:24,470 --> 00:18:27,400
 the effort or failing to be against

279
00:18:27,400 --> 00:18:29,880
 again meticulous and careful.

280
00:18:29,880 --> 00:18:35,050
 It can also refer potentially to being mindful about the

281
00:18:35,050 --> 00:18:37,160
 past and the future.

282
00:18:37,160 --> 00:18:40,020
 So some meditators will have memories about the past, even

283
00:18:40,020 --> 00:18:41,520
 remembering past lives, but

284
00:18:41,520 --> 00:18:44,720
 mostly about early childhood and that kind of thing.

285
00:18:44,720 --> 00:18:47,680
 Memories that they thought were buried and were gone

286
00:18:47,680 --> 00:18:48,440
 actually.

287
00:18:48,440 --> 00:18:53,080
 They didn't realize they still kept around.

288
00:18:53,080 --> 00:18:56,300
 Or again it will be planning about the future, making plans

289
00:18:56,300 --> 00:18:58,280
 and just this really sharp ability

290
00:18:58,280 --> 00:19:17,200
 to construct plans or memories about the past.

291
00:19:17,200 --> 00:19:19,720
 And the last one is nikanti.

292
00:19:19,720 --> 00:19:23,560
 Nikanti is where you get…

293
00:19:23,560 --> 00:19:25,560
 Nikanti means desire.

294
00:19:25,560 --> 00:19:28,160
 It's when you like something.

295
00:19:28,160 --> 00:19:31,000
 A very subtle sense of liking.

296
00:19:31,000 --> 00:19:34,510
 Liking can be any of the other nine upakilesa, but it can

297
00:19:34,510 --> 00:19:36,600
 also be something you see or hear,

298
00:19:36,600 --> 00:19:42,040
 smell or taste or feel or think.

299
00:19:42,040 --> 00:19:45,690
 Nikanti because the meditator is suddenly happy and so they

300
00:19:45,690 --> 00:19:47,280
 have this subtle desire

301
00:19:47,280 --> 00:19:50,920
 or liking of their situation.

302
00:19:50,920 --> 00:19:53,080
 Because it's subtle they may not note it.

303
00:19:53,080 --> 00:19:58,600
 They may go with it and get lost in it.

304
00:19:58,600 --> 00:20:04,600
 Nikanti, subtle desire that you'd be very hard to see, but

305
00:20:04,600 --> 00:20:05,200
 if you're sitting there

306
00:20:05,200 --> 00:20:08,780
 very pleased with yourself, very happy about your practice,

307
00:20:08,780 --> 00:20:09,880
 you have to remember to note

308
00:20:09,880 --> 00:20:10,880
 liking, liking.

309
00:20:10,880 --> 00:20:13,360
 It's a common one at this stage.

310
00:20:13,360 --> 00:20:17,610
 It's not liking something strongly, but it's a liking

311
00:20:17,610 --> 00:20:19,680
 meditation, feeling happy about your

312
00:20:19,680 --> 00:20:23,440
 practice and enjoying the practice.

313
00:20:23,440 --> 00:20:29,210
 Because it takes away the sharp edge of mindfulness, the

314
00:20:29,210 --> 00:20:30,480
 clarity.

315
00:20:30,480 --> 00:20:36,870
 You start to get lazy and you start to get complacent and

316
00:20:36,870 --> 00:20:40,480
 you lose the present moment.

317
00:20:40,480 --> 00:20:46,990
 So for all of these, they're not an obstacle, but they're a

318
00:20:46,990 --> 00:20:50,000
 distraction and they have the

319
00:20:50,000 --> 00:20:52,120
 potential to distract the meditator.

320
00:20:52,120 --> 00:20:55,510
 As with everything, the bad things, the good things, it's

321
00:20:55,510 --> 00:20:57,120
 all up to your mind, how you

322
00:20:57,120 --> 00:21:04,640
 interact with them, how you respond to them.

323
00:21:04,640 --> 00:21:09,890
 Meditation is about changing the way we look at things,

324
00:21:09,890 --> 00:21:11,960
 changing us, changing the core

325
00:21:11,960 --> 00:21:13,120
 of who we are.

326
00:21:13,120 --> 00:21:16,940
 I was thinking, trying to think of an analogy and it made

327
00:21:16,940 --> 00:21:19,320
 me think of something and at first

328
00:21:19,320 --> 00:21:23,760
 I couldn't think of what it was, but then I realized it was

329
00:21:23,760 --> 00:21:24,800
 the Borg.

330
00:21:24,800 --> 00:21:28,280
 There was this show, probably some of you heard of it,

331
00:21:28,280 --> 00:21:30,200
 called Star Trek and they had

332
00:21:30,200 --> 00:21:33,440
 several versions of it, but in one version there were these

333
00:21:33,440 --> 00:21:34,960
 groups of beings called the

334
00:21:34,960 --> 00:21:38,720
 Borg and they would assimilate all the other beings.

335
00:21:38,720 --> 00:21:43,130
 Well, that's what I was thinking of when meditation is kind

336
00:21:43,130 --> 00:21:45,960
 of like brainwashing or assimilating.

337
00:21:45,960 --> 00:21:49,960
 It's kind of like a parasite.

338
00:21:49,960 --> 00:21:54,780
 You have this host and this host has all sorts of, has a

339
00:21:54,780 --> 00:21:56,840
 real personality.

340
00:21:56,840 --> 00:21:59,180
 How it relates is because we're not interested in the

341
00:21:59,180 --> 00:22:00,000
 personality.

342
00:22:00,000 --> 00:22:02,080
 We're not interested in your experiences.

343
00:22:02,080 --> 00:22:07,010
 It's not like we're going to totally eradicate them, but

344
00:22:07,010 --> 00:22:09,880
 meditation is a whole new aspect

345
00:22:09,880 --> 00:22:11,840
 of who you are.

346
00:22:11,840 --> 00:22:16,000
 It's creating habits that you may never have developed

347
00:22:16,000 --> 00:22:16,880
 before.

348
00:22:16,880 --> 00:22:25,600
 So in a sense it's creating a new entity in the host.

349
00:22:25,600 --> 00:22:30,660
 And so everyone comes to meditation with quite different

350
00:22:30,660 --> 00:22:32,800
 characters and personalities and

351
00:22:32,800 --> 00:22:35,360
 none of that's all that important.

352
00:22:35,360 --> 00:22:38,120
 None of it.

353
00:22:38,120 --> 00:22:43,100
 It's not going to change completely or disappear completely

354
00:22:43,100 --> 00:22:45,840
, but we're using this host like

355
00:22:45,840 --> 00:22:49,380
 planting a seed in the earth, but the seed is something

356
00:22:49,380 --> 00:22:50,800
 quite different.

357
00:22:50,800 --> 00:22:56,860
 A seed of meditation for many people is something quite new

358
00:22:56,860 --> 00:22:57,000
.

359
00:22:57,000 --> 00:23:01,880
 And so you end up assimilated in a sense.

360
00:23:01,880 --> 00:23:04,630
 Everyone will still look different, speak different, act

361
00:23:04,630 --> 00:23:06,280
 different, but we have something

362
00:23:06,280 --> 00:23:11,960
 in common and that's all we're looking for.

363
00:23:11,960 --> 00:23:15,270
 All of these different experiences one might have, because

364
00:23:15,270 --> 00:23:17,120
 every meditator is different.

365
00:23:17,120 --> 00:23:20,020
 You see a thousand meditators, you'll have a thousand

366
00:23:20,020 --> 00:23:21,480
 different conditions.

367
00:23:21,480 --> 00:23:24,280
 Everybody's a little bit different.

368
00:23:24,280 --> 00:23:27,270
 But none of that's really all that important in the

369
00:23:27,270 --> 00:23:28,880
 meditation practice.

370
00:23:28,880 --> 00:23:32,250
 So as a meditation teacher you're not really concerned with

371
00:23:32,250 --> 00:23:33,840
 any of the particulars of the

372
00:23:33,840 --> 00:23:34,840
 individual.

373
00:23:34,840 --> 00:23:43,600
 You're concerned with creating a seed, planting a seed.

374
00:23:43,600 --> 00:23:49,130
 And growing and cultivating this seed, which is the seed of

375
00:23:49,130 --> 00:23:54,920
 mindfulness, the seed of insight.

376
00:23:54,920 --> 00:23:59,810
 That says meditators, how we should approach ourselves or

377
00:23:59,810 --> 00:24:02,160
 approach our experience.

378
00:24:02,160 --> 00:24:07,740
 To not get sidetracked by specific experiences or thoughts

379
00:24:07,740 --> 00:24:09,840
 or character types.

380
00:24:09,840 --> 00:24:12,280
 To not let our personality take over.

381
00:24:12,280 --> 00:24:16,370
 As our personality can be good and it can be bad, but it's

382
00:24:16,370 --> 00:24:18,720
 always, if we've never practiced

383
00:24:18,720 --> 00:24:25,580
 insight meditation, it's always made up of worldly half-

384
00:24:25,580 --> 00:24:26,880
truths.

385
00:24:26,880 --> 00:24:30,510
 So no matter how good and useful it might be, practically

386
00:24:30,510 --> 00:24:32,520
 speaking, it can never truly

387
00:24:32,520 --> 00:24:37,680
 be in touch with reality until we cultivate this practice

388
00:24:37,680 --> 00:24:39,480
 of mindfulness.

389
00:24:39,480 --> 00:24:44,760
 Where we start to look perfectly objectively at things.

390
00:24:44,760 --> 00:24:54,190
 I'm just trying to explain, the key here is not get caught

391
00:24:54,190 --> 00:24:57,440
 up in who you are.

392
00:24:57,440 --> 00:25:01,240
 Not get caught up in your views, your opinions, your

393
00:25:01,240 --> 00:25:05,240
 emotions, your conceits, your identifications.

394
00:25:05,240 --> 00:25:08,040
 Not get caught up in your experiences, not get caught up in

395
00:25:08,040 --> 00:25:09,400
 any of the good things that

396
00:25:09,400 --> 00:25:16,000
 come.

397
00:25:16,000 --> 00:25:23,950
 Try to be objective and have a pure state of mind during

398
00:25:23,950 --> 00:25:26,520
 the practice.

399
00:25:26,520 --> 00:25:28,480
 So there you go.

400
00:25:28,480 --> 00:25:30,400
 Some caution, some direction.

401
00:25:30,400 --> 00:25:33,590
 I think that's one of the most important aspects of the

402
00:25:33,590 --> 00:25:38,080
 practice is providing direction and

403
00:25:38,080 --> 00:25:41,880
 correction when we start to get off track.

404
00:25:41,880 --> 00:25:46,040
 So there you go.

405
00:25:46,040 --> 00:26:11,640
 If there are any questions, I'm happy to answer them now.

406
00:26:11,640 --> 00:26:18,640
 Okay.

407
00:26:18,640 --> 00:26:25,640
 Okay.

408
00:26:25,640 --> 00:26:32,640
 Okay.

409
00:26:32,640 --> 00:26:39,640
 Okay.

410
00:26:39,640 --> 00:26:46,640
 Okay.

411
00:26:46,640 --> 00:26:53,640
 Okay.

412
00:26:53,640 --> 00:27:00,640
 Okay.

413
00:27:00,640 --> 00:27:07,640
 Okay.

414
00:27:07,640 --> 00:27:14,640
 Okay.

415
00:27:14,640 --> 00:27:24,640
 Okay.

416
00:27:24,640 --> 00:27:32,640
 Okay.

417
00:27:32,640 --> 00:27:44,640
 Okay.

418
00:27:44,640 --> 00:27:52,640
 Do we have a full house tonight?

419
00:27:52,640 --> 00:27:55,640
 It's probably our fullest yet.

420
00:27:55,640 --> 00:28:00,640
 Good turnout.

421
00:28:00,640 --> 00:28:04,180
 All right. Well, if there are no questions, I wish you all

422
00:28:04,180 --> 00:28:06,640
 a good night and thank you for coming.

