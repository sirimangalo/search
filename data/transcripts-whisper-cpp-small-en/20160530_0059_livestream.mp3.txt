 Good evening everyone.
 I'm broadcasting live on May 29th.
 Tonight's quote is simple enough.
 It's got a couple of parts to it.
 The Buddha says, "Those who you have sympathy for,
 ye tibhikvay anukam peyata, those who are who you,
 you are sympathetic towards,
 those who are family or friends or loved ones,
 those who you know are in need of support and help,
 ye jasotabang manye yung."
 And here he gets the translation wrong.
 Ye jasotabang manye yung means those who think it worth
 listening to,
 those who think you are worth listening to.
 Those who consider...
 those who consider who would think to listen,
 those who think to listen.
 I'm not quite sure about that myself.
 So either those who think to listen or those who you think.
 Those who will listen.
 Those people who you think will listen.
 You care for those who listen to you,
 those who think you are worth listening to.
 Not everyone is like that of course.
 The point being here, some people you care about,
 but they won't listen.
 Be your parent, sometimes it's very hard to give any advice
 to your parents of course.
 "Mitta wa amatya wa nyati wa salohita wa..."
 Whether they be a friend or a colleague,
 a relative, or salohita is also a relative.
 I don't know what nyati is.
 Maybe it just means...
 Nyati usually means relative.
 It would just be...
 But nyati maybe doesn't mean by blood.
 Nyati could just mean...
 Nyati wa salohita wa just a relative.
 All of these people, these people who you are your friends,
 who you care for,
 and who you think will listen to you,
 is something you should do.
 "Nyivikvey cha tu su sota patiyange su samadapetaba, nyivas
etaba, patitha petaba..."
 There are four things that they should be established in,
 that they should be set in,
 that should be made to be accomplished,
 brought to fulfillment in the four sota patiyange,
 the four factors involving stream entry,
 involved in regards to.
 They're saying if you love someone, if you care for someone
,
 what's the best thing you can do for them?
 Establish them in four things.
 Basically bring them to...
 It's not clear whether he's saying he should teach them to
 practice
 or help them to practice to become a sota panna
 or just help them with four things that are like a sota p
anna.
 A sota panna is someone who's seen nirvana or nirvana for
 the first time,
 someone who you can call enlightened.
 So there are four factors.
 So the first part of this quote is how you should help
 someone.
 What is the best thing you can do for someone?
 If you care for them, what can you give to them?
 The second part explains what those four things are.
 I just wanted to sort of pause on this idea of guiding
 people to your religion
 because it should sound quite familiar.
 This is the kind of thing every religion tells you to do.
 If you care for someone, convert them.
 Convert them to our religion.
 So I think it's something that people are wary of,
 this idea of trying to convert your family
 and any religion that encourages you to go out and convert
 everyone.
 It's an interesting concept because most religions talk
 about it.
 I would say Judaism, maybe not so much as one of the major
 religions.
 But Christianity, definitely, I think Islam to some extent.
 There's an idea of encouraging your family to practice,
 encouraging others.
 I think it's a natural inclination for any spiritual or
 religious practitioner
 because religion tends to make you happy.
 Not always, but for the majority, I would say religion
 provides,
 as I think Mao Zedong said, an opiate.
 It makes you feel good.
 Being in that community and having such a lofty mind state
 of faith and devotion,
 no matter what it's for, these states are pleasant.
 You could say exalted in the sense of having a power to
 them,
 having a composure to them.
 The mind that is focused on religion of any kind tends to
 be fairly focused and powerful.
 And as a result, pleasant, enjoyable, peaceful, sublime,
 exalted, all these things.
 And when that comes, humans are, beings are interesting in
 this way that
 we don't at the very core distinguish between self and
 others.
 This is why when we see someone else hurting, it affects us
.
 Now, it's possible to ignore this and it's possible to lose
 this sort of sense of
 desire to see other beings happy and desire for them not to
 be in suffering.
 We can pervert that and we often do and even as humans, we
 can be cruel to others.
 But we do this through delusion.
 This is a religious person, Buddhist or otherwise, tends to
 have a more refined mindset,
 not always, of course, but tends to have a sense or tends
 to be able to tap into these mind states
 of compassion and desire for the benefit of others.
 And I think to be charitable, that's part of the reason why
 people, religious people,
 want to share their religion because they feel such
 happiness and they want others to be happy.
 At least those who they care for, right?
 Those who you care for, who you are sympathetic towards,
 want them to be happy, if no one else.
 So I wouldn't say that's wrong. I wouldn't say proselytism
 is wrong.
 The problem is if you're deluded and if you're following a
 path that is problematic,
 and encouraging others to follow it, you may not be in the
 long run the wisest.
 It may actually be causing more harm than good. This is the
 problem.
 Because if your religion is based on false premises or
 shaky premises, blind faith,
 then it might be right, it might be wrong.
 But if you don't have good reason to believe that it's
 right, then often this is the case.
 We go by feelings, we go by belief, view, we don't go by
 evidence so much.
 It's very dangerous. You could be leading everybody to the
 edge of the cliff.
 I think that's important to say.
 I think religion as a concept is a very powerful thing and
 has a potential for great good.
 Leading other people to religion has the potential for
 great good.
 I think it does bring great good in most religions.
 I think Christianity, when practiced properly, brings great
 good to society.
 It cultivates good people. There's so much bad, of course,
 that we can point to.
 There's so much bad in Buddhism as well that we could point
 to in Buddhist societies.
 But overall, in general, there's a lot of good that comes
 from this basic religious act of cultivation
 and sharing, bringing it to others.
 I think that's an interesting thing for us to consider when
 we think about our practice and those who we care about.
 There was one monk who once said, he was listening to a
 talk,
 and he said, "Someone once told me that I practice Buddhism
, but I don't want to push it on my children.
 I'll let them make up their own mind."
 He said, "How irresponsible of you. You believe what the
 Buddha said was right.
 You believe these teachings, and for you they are right.
 Why don't you teach your children what is right?
 Are you going to let them fall into the wrong path?"
 That comes to the point of simply teaching people about
 religion or bringing people closer to religion.
 Whatever religion is not really enough.
 The important point is the second half, what it actually
 means to...
 What is it actually that you're bringing people to? Because
 you bring people closer to the belief that Jesus is the
 only path to heaven.
 For Muslims, you're going to paradise or this kind of thing
.
 You're not really doing much of a favor, and it's on very
 shaky ground.
 There's no reason behind it.
 With Buddhism, it's talking about faith as well.
 In Buddhism, faith is considered to be a good thing. Faith
 in Jesus is a positive mind state.
 Faith in a snowman is a...
 Faith in Santa Claus is a wholesome mind state.
 In the sense that, to the extent that faith is powerful, it
 creates a state of mind that is sublime and focused and
 calm.
 Now, as I've said, as I've repeated, the delusion behind
 Santa Claus or Jesus, all these things,
 from a Buddhist point of view, is unwholesome and
 problematic, but the actual moments of faith.
 In Buddhism, we try to find and we consider and we focus on
 things that are worth having faith in.
 We make a claim that there are things that it's worth
 having faith in.
 And we explain that there are actually two kinds of faith.
 You can have faith based on a supposition or based on
 evidence,
 which is different from proof. So evidence can be... it's
 usually insufficient.
 Evidence is sort of a partial knowledge. Evidence can be
 the way someone acts.
 You see the Buddha walking into the village and you get
 great faith in him. You think, "He must be enlightened."
 Because there's evidence. What's the evidence? Well, his
 composure, his behavior.
 And then you hear the Buddha give a talk and you get more
 faith because you have more evidence.
 It's not proof, but all of that is evident. It's usually
 not enough to make a conclusion.
 You could still very much be wrong, but there's a sense
 that that's a good thing.
 And it, of course, better the more evidence you have.
 There's a sense that it's good to have faith in the Buddha,
 whether you have a lot of evidence or not.
 It's funny because it's just by chance. You've happened to
 put your faith in the right thing.
 It's like if you choose an airline, you put your faith in
 one airline.
 Well, maybe just pick and choose an airline and it happens
 to be a good one.
 Then pick and choose an airline and it happens to be the
 wrong airline.
 And you get in a plane crash because the airplanes are not
 good.
 Then you put your faith in something.
 If you pick the right one, you're lucky.
 It's funny. You know, we were talking about this, I think,
 today, about how
 all it takes is to be born into the wrong religion.
 To good people. We were talking about good people who,
 by virtue of being born into a religion that says killing
 is okay or drugs and alcohol are okay.
 Good people can get very much on the wrong path.
 They may have great goodness inside, but based on their
 views, based on their culture and their religion,
 they get on the wrong path.
 So there's that, but that should be taken with a grain of
 salt, of course, because you don't know.
 And it could very much be wrong. So it's good if people
 have faith in the Buddha,
 but it's probably not wise.
 And it certainly isn't how you could sell Buddhism.
 Yeah, the Buddha was the best, so just believe that he was
 the best.
 For most people, that's not nearly enough, and rightly it
 shouldn't,
 because they have no reason to believe it.
 So we have this other kind of faith, and that's based on
 proof.
 And proof is something that science doesn't like to talk
 about.
 It's something that science is very afraid of, rightly so,
 because people think they have proof of something, and you
 can easily prove anything.
 You can only provide evidence, and if you have overwhelming
 evidence, then you have no reason to doubt.
 But you still don't have... it's very hard to find proof of
 pretty much anything.
 Except the interesting thing we would argue is that from a
 first-person point of view,
 you can find proof in a lot of things. Proof is actually
 fairly easy to find.
 So if I give you a simple example, does seeing exist?
 See, science doesn't talk about things like seeing, it
 talks about light, and the eye, and the brain.
 And so if I asked you instead, "Do you see me?"
 "You see this person?"
 "You could say, 'Yes, I see you,' and I said, 'But do you
 have proof?'"
 You'd have to say, "No, you don't have proof."
 "I could be just some machine-generated code made to look
 like a human being very convincingly."
 "I could be wearing a mask that I'll suddenly pull off."
 So you can't prove that you're seeing. And this is
 important, this is essential in terms of Buddhism and
 understanding what is real.
 Because you can't deny that you're seeing.
 You can argue that you're not seeing what you think you're
 seeing.
 You can argue that it's not a real thing that you're seeing
.
 But seeing is seeing. There's nothing else it possibly
 could be.
 Seeing exists. Hearing exists. Smelling, tasting, feeling,
 thinking exists.
 Basically, if you want to argue, you could nitpick and say,
 "Well, what's the difference between these senses?"
 In the end, it probably doesn't come down to any real
 difference.
 But what is real, what definitely exists, is experience.
 You have proof. Why? Because you have experience.
 No one can deny the existence of experience. It couldn't be
 anything else.
 It's what Descartes came to. He said, "Suppose there's a...
 suppose instead of a benevolent god, there is a malevolent
 demon tricking us."
 What is the one thing that this malevolent creator being
 couldn't trick us into?
 He couldn't trick us about the fact that we're experiencing
. Because you have to experience in order to be tricked.
 Kogito ergo sum, he said, "I think, therefore, I am."
 There is the thinking. When he was focusing on this, the
 fact that the thinking exists.
 There is something that exists. I'm not nothing. Because I
 think.
 So that's just a simple example. I mean, that's really...
 but it seems banal. It seems perhaps useless.
 But it's actually essential because that is then the
 framework for understanding reality.
 You can't base your framework of understanding reality on
 concepts, on people, places, things.
 Anything that modern material science focuses on cannot
 help you to understand reality.
 Because it's not based on reality. It's based on conjecture
 in the mind. It's based on mental activity.
 That you can't prove. You can't prove that this universe
 exists.
 Science doesn't ever try to prove anything for that reason.
 But the reason why is because it's not real. It's abstract.
 The only thing that's real and that you can prove exists is
 experience.
 But it's a good example because it shows that from a first-
person point of view you can start to prove things.
 And you can prove a lot by seeing how things work. You can
 see cause and effect.
 Although cause and effect you can't prove.
 Not exactly because who knows if it... maybe it just will
 change.
 But what you can do when this gets back to our quote is you
 can prove to yourself beyond reasonable doubt about things
 like cause and effect.
 Now this isn't actually the important proof. But
 practically speaking it's enough.
 So when you practice meditation you start by finding the
 right framework.
 You start by understanding seeing, hearing, smelling,
 tasting, thinking, body and mind as being the basis of
 reality.
 That's important. It's important to see that, to make that
 realization.
 As you go on what you're doing is you're starting to see
 cause and effect.
 And eventually you see cause and effect so clearly that you
 accept it without proof.
 But you accept it because if you see the same thing this is
 how science works.
 You see the same thing again and again and again. It's
 enough evidence to be overwhelming.
 And in this instance you don't exactly need proof. You just
 need to see it enough so that your mind appreciates it.
 Your mind accepts it. Yes, it must be like this.
 Because at that point the mind shifts and the mind lets go.
 It sees more than cause and effect. It sees impermanent
 suffering and non-soxities.
 But all these things that we cling to, trying to fix them,
 trying to keep them,
 trying to make them our slave, our servant, amenable to our
 control.
 All of these things are impermanent, unsatisfying and
 controllable.
 When you get that, when you appreciate that, you say there
's nothing.
 It's like a bird when looking through the tree for fruit.
 It looks everywhere.
 As long as it thinks there's still fruit in the tree it won
't fly away.
 So as long as the mind is looking for something, you know,
 when you're meditating,
 well this is no good but maybe if I practice this way,
 maybe if I switch,
 you're still looking for something to satisfy.
 The mind is still looking for pleasure, something that will
 make me happy.
 Until finally the mind gives up without proof, but with
 enough evidence to make,
 to convince the mind that there's nothing. There really is
 nothing.
 At that moment the mind lets go and when the mind lets go
 there's an experience of cessation
 we call nirvana or nirvana. And that experience is proof.
 That's the experience that makes one a sotapana.
 That literally is exactly what makes one a sotapana.
 Nothing else.
 You can't just become a sotapana by reading the Buddhist
 teaching or appreciating it
 or having faith in the Buddha for example.
 A sotapana is only one thing they've realized nirvana.
 But there are four qualities that come from becoming a sot
apana.
 That's what the Buddha says here. It's the second part of
 our quote.
 He says the first one is that they have "bundhe awicapasad
he"
 They have unshakable faith in the Buddha.
 In what way? And then it's "ithipiso bhagavad"
 This very well known chant. Indeed he is the bhagavad, the
 lucky one, the blessed one.
 And then there's all the what the Buddha is. They have
 perfect faith in the Buddha.
 They realize that he's telling the truth.
 They realize that he has taught the truth.
 So they have perfect faith in him because they hear him
 saying things that they know for themselves.
 That they've seen for themselves.
 "Dhammi awicapasadhe"
 They have, number two, they have unwavering faith in the D
hamma.
 And this one is more direct.
 This is like if you look at a map and you see that the map
 leads to the right direction.
 You say, "Oh, I've been there." And you can confirm that
 the map heads in the right direction.
 It leads you out of the forest. Then you have perfect faith
 in that map.
 You don't need to have faith in it. You don't even have to
 call it faith.
 But you're completely sure what that map is telling you.
 Why? Because you've been there. You know for yourself.
 So when you read the Dhamma, when you listen to the Dhamma,
 a person who has attained sodapana has perfect faith in him
.
 Because they know it for themselves.
 "Sange passadi na samana awicapasadhe"
 "Sange awicapasadhe"
 Perfect faith in the Sangha.
 This is in people who have practiced.
 So if someone, this is probably more theoretical than
 anything.
 Meaning, they understand that a person who has attained nir
vana is enlightened, is worthy of respect.
 Has attained something special. Because they themselves
 have attained it.
 So they have, because they don't know whether a person is
 the Sangha.
 It's not like they have faith in people. That's not what it
 means.
 But they have faith in someone, in the idea that someone
 who has attained nirvana is worth appreciating.
 And number four, "Aryakante su syle su samadha petebah"
 If you would care for these people, so this is the things
 you should establish with them.
 So an enlightened being is established in "Sila" morality
 or ethical behavior, ethics, that is dear to the noble one.
 "Aryakanta"
 What that means is the five precepts.
 So the "pana" is intent upon keeping the five precepts.
 They don't kill, they don't steal, they don't cheat, they
 don't lie, they don't take drugs or alcohol.
 So these are the four things that we, the Buddha says we
 should give.
 And you can look at this more.
 People, when they come to meditate, they want to give these
 things to others.
 It becomes quite important for them.
 And during the course they start to think of all their
 loved ones and they wish to share it with them.
 And so it is useful teaching to think about.
 I mean it's useful for us to understand where we're going,
 but it's also useful for us to understand what we should do
 to share with others.
 It may seem somewhat secondary to the meditation practice,
 but I don't think it should be seen that way.
 If you don't help others, if you don't change the people
 around you, if you can't succeed in that,
 well, if you ignore and just keep to yourself, it makes it
 more difficult to practice.
 I mean I think that's quite obvious.
 You're surrounded by people who aren't meditating, who aren
't interested in meditating.
 Who don't appreciate Buddhism and the Buddha's teaching.
 Who don't have Arya Kante, if their ethics are not dear to
 the noble ones.
 The kill, they steal, they cheat, they lie, they take drugs
 and alcohol.
 It's difficult to be around them.
 Or it's difficult to keep your mindfulness when you're
 around them.
 So the best way is not to always run away from people.
 That's one way some people go and live in the forest,
 because they can't stand all these crazy people.
 But another way is to just help people, surround yourself
 with good people.
 Problem here, and this goes back to the crucial part of the
 first part of the quote,
 has to be people who will listen.
 And it's funny how those aren't always the people you think
 they are.
 Sometimes you have to change the people you associate with,
 your friends change.
 When I think back to all my friends when I was younger,
 it was probably the ones who I alienated, who I would have
 most gotten along with now.
 And those who I was very close friends with,
 ended up alienating them once I became a Buddhist, because
 it turned out that our reasons for being friends was not in
 line with goodness.
 So for all of us it's different, but it has to be the
 people who will listen.
 If you're going to share with people, if you care about
 people,
 and if they care about you and if they appreciate you and
 if they think you're worth listening to,
 this is what you should give to them.
 So we can take this as advice in that regard,
 but most importantly for ourselves we should take this as a
 reminder of what's important for us,
 what's important for us to attain.
 We are to gain perfect confidence in what we're doing.
 Where are we headed? We're headed to have to gain proof,
 proof about something, to be able to something that we can
 hold on to,
 in the sense of something that is stable, is sure, is
 certain, is permanent really.
 It's not going to change, it's not going to waver.
 That reality is based on experience, that everything in
 this world is impermanent, that's permanent.
 And the cessation of suffering, this experience of
 cessation,
 the letting go, the freedom, the peace, the silence that we
've had.
 So anyway, that's our quote for tonight. Looks like we got
 a bunch of questions, so we're going to scroll through them
.
 It's okay to ask questions, but if they're too off topic, B
ante may call you out on it. I may.
 I'm good at scaring people, I think. I bet there's lots of
 people who would ask questions,
 but they're scared I'm going to call them out on it. I
 think that's okay though.
 Okay, if you want to post questions, please use the
 question mark. It helps us.
 He said you're looking for a steward. It's spelled S-T-E-W-
A-R-D. We're not looking for a steward.
 But we are looking for a steward. Aruna has left. We're
 hoping that, well he's hoping to get a job soon.
 I haven't heard back, but assuming he's getting a full-time
 job with a high paying salary, he's doing well for himself
 it sounds like, which is good to hear.
 But he's gone, been gone for a while. Michael is here now,
 but he's leaving when I leave, or just after I leave, which
 is in a few days.
 So we'll see, yeah, if there's someone out there who would
 like to come and be our steward, it would be much
 appreciated.
 On that note, we're always looking for volunteers, online
 volunteers, for those of you who are far away, but would
 like to get involved with the organization.
 We can always use more people to do online things. There's
 things that can be organized, many different kinds of
 things that we do.
 We have an online organization group where we chat, and we
 have a platform, we use Slack to organize, and so we talk
 about all sorts of things.
 The website, the graphics, we talk about photos, we talk
 about meditators coming.
 The big one recently is rental, we've been talking about
 renting a new place with more room for meditators, so that
 kind of thing.
 If you want to get involved, let us know.
 Here, Austin says he would like to... Austin wants to get
 involved.
 Here I am talking about it, Robin's already ahead of me, as
 usual.
 So yeah, she'll get you connected there.
 Okay, a couple of questions.
 I have been meditating for many months. I don't feel the
 abdomen rise and fall. My abdomen feels tense, and in
 general I feel stressed.
 For the duration of the meditation, I'm mainly noting my
 emotions and physical sensation because they are most
 prevalent.
 You should try to get to the breath. I mean, the fact that
 your stomach is not moving, as you say, it's due to tension
.
 So the tense feeling is something you should know.
 If you feel tense, you should say to yourself, "Tense,
 tense," and then look for the rising and falling again.
 You'll find if you do that enough, you'll start to relax.
 Try doing lying meditation. Lie down on your back. Don't
 make a habit of this, but do an experiment.
 Lie down on your back and then watch the stomach rise and
 fall.
 Then you'll realize how prevalent it is, how coarse the
 subject really is, how obvious and how clear it is.
 This would have been easier for people who are living in
 the forest, monks, who live peaceful lives.
 We don't have all this stress that people in the world have
.
 It is a bit of a problem when we try to give this to people
 who are very stressed.
 But the thing is, you'll find if you keep with it, after
 some time, you relax and it will come to you.
 Don't give up on it. It's really just because you're
 stressed.
 There will be a point where suddenly your breath will shift
, you'll feel it, and suddenly...
 I mean, maybe not exactly like that, but it will get easier
.
 If you need, you can put your hand on your stomach. That
 can also help in the beginning.
 But it's really a great object because it's not permanent,
 it's not satisfying, it's not controllable.
 And so it's teaching you. The tension is also teaching you
 something. It's teaching you that you can't force it.
 Just watching the stomach is such a great thing, it's such
 a great object, because it'll rule you.
 You know, it really rocks you. It seems such a simple thing
.
 Well, of course I can watch my stomach. No, you can't. No,
 it'll mess you up.
 It'll shake your foundations of what you believe in about
 the world.
 It's a very profound object of contemplation. Because it's
 not permanent, it's not predictable.
 It's not satisfying, you can't make it calm and smooth. It
's not controllable, you can't force it to be long or short.
 It just makes you suffer more. So don't give up the abdomen
. It's a great object of meditation.
 Is it right to view suffering as beneficial to the extent
 it teaches nudges toward the right view?
 If there's no suffering, would there be no need to seek out
, refine the right view over and over again?
 Well, everything that arises is dukkha.
 But if you're talking about painful or something that is d
ukkha vedana,
 everything that arises is dukkha in the sense of unsatisf
ying, not a cause of happiness,
 not a source that you can hold on to it and say this is
 going to make you happy.
 But if you're talking about is it good to be in pain, is it
 good to meditate?
 It actually is generally considered to be a good thing
 because it's much easier to let go and it's much more
 challenging.
 So it really forces you to let go.
 So people who are sick, people who are in pain, tend to be
 pretty quick to let go and often have a better time.
 People when they're dying, when you're dying, it's a common
 thing to let go because there's much less clinging.
 There's much less to cling to. You don't want to stay alive
. You don't want to be here.
 That can lead to aversion, which is not good and equally
 problematic.
 But traditionally it's considered to be easier when you're
 suffering pain.
 Now of course that wouldn't include self-inflicted pain
 because if you're inflicting pain upon yourself, you're
 almost up.
 And that also touches on your question. You're asking
 whether suffering is beneficial.
 Anyone who thinks suffering is beneficial has an intrinsic
 goodness to it. That's the problem.
 So if you hurt yourself, thinking that that's going to help
 you, the act of hurting yourself and thinking that it's
 beneficial is problematic.
 And that's going to take you away from Nibana because you
 no longer want to be happy. You want to hurt yourself.
 There's a fine line there. It's an easy one to see because
 it's about bearing with suffering.
 When there's suffering and you can't change it, it's a
 great tool.
 It helps you let go.
 The meditator begins to see reality clearly.
 They come to see that what they thought was reality is not
 actually real at all. It is conceptual.
 Can this ever happen too quickly leaving the meditator
 overwhelmed or it's destabilized or on a psychological
 level?
 No. No. No, because it simplifies things.
 It can shake you up. It disturbs you. But this is so
 different from most types of meditation that have the great
 potential to drive you crazy because they're conceptual.
 There's nothing to hold on to.
 To some extent, in this meditation there is something to
 hold on to. Like with concepts, you can hold on to the
 concept but it can quickly shift.
 It can quickly change into something else. It's not real so
 it can become anything.
 And so it drives you further and further. And if you're not
 careful, if you're not controlled with it, you can get into
 real trouble.
 With vipassana, sure there's nothing to hold on to.
 Everything is impermanent. But you can depend upon that.
 You can depend upon the fact that it's always going to be
 the same. There's nothing weird. There's nothing exotic.
 Everything is very mundane, very simple. And all you're
 doing is learning to let go.
 This isn't a kaita meditation that is dangerous. It's a lot
 safer.
 We just talk about safety and whether meditators in this
 tradition go crazy.
 Sure, you can go crazy because you don't practice what you
're told. You're due to practicing crazy stuff.
 But practicing this meditation is very much safer than, say
, samatha meditation.
 Samatha meditation is usually safe if done in a Buddhist
 way.
 But the general conceptual-based meditation has dangers to
 it, especially if done without a teacher.
 If you inflict suffering on yourself, I suppose you could
 argue that there's going to be wholesome states in there, a
 great potential.
 You could argue that those six years of torturing
 themselves was eye-opening and helped them to let go of...
 It probably prepared them to become enlightened pretty
 quickly. There's got to be some good to it.
 He said it was completely useless. It would never have led
 to enlightenment.
 But understanding pain, any experience that helps you
 understand reality, understand what's possible, is somehow
 roundaboutly beneficial.
 I would argue that there's probably moments when you
 inflict suffering on yourself that's really problematic
 because it messes with your view.
 But the moments when you're just dealing with the pain, it
 cultivates patience.
 Like these guys, if you talk about all of us sitting in
 meditation for a half an hour or an hour, it's very
 stressful for most of us.
 These guys, no problem, they'll sit still all day. Great
 patience from it. So there would be great benefit to that.
 So they could argue that there could still be good come
 from hurting yourself.
 It's just that it's very much outweighed by the fact that
 you're hurting yourself, that you're intentionally
 inflicting pain upon yourself.
 Okay, I've been here long enough and there weren't that
 many questions after all.
 So I'm going to say goodnight. Lots of oranges tonight.
 Those of you who are orange, it means you didn't meditate
 with us.
 And if you didn't meditate with us, I'm less inclined to
 answer your question.
 Green means you meditated with us in the past three hours.
 So not enough greens. I'm going to say goodnight.
 Goodnight, everyone.
 How many minutes are you doing?
