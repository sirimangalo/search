1
00:00:00,000 --> 00:00:17,440
 Hello everyone. Welcome to our Q&A series. Today's question

2
00:00:17,440 --> 00:00:21,600
 is in regards to dealing

3
00:00:21,600 --> 00:00:31,170
 with the effects of meditation practice, specifically the

4
00:00:31,170 --> 00:00:32,880
 unexpected, undesired,

5
00:00:32,880 --> 00:00:41,770
 unpleasant effects of meditation. So the questioner was

6
00:00:41,770 --> 00:00:49,040
 asking about dealing with people when they feel

7
00:00:49,040 --> 00:00:53,050
 unable to communicate with them after meditating. They feel

8
00:00:53,050 --> 00:00:56,240
 strong states of concentration after

9
00:00:56,240 --> 00:01:04,290
 meditating and overwhelming physical sensations. How do you

10
00:01:04,290 --> 00:01:10,000
 deal with those experiences and

11
00:01:12,800 --> 00:01:19,210
 stop them from getting in the way of dealing with other

12
00:01:19,210 --> 00:01:20,560
 people?

13
00:01:20,560 --> 00:01:24,640
 So the first thing of course to mention is, to talk about,

14
00:01:24,640 --> 00:01:30,720
 is the fact that

15
00:01:36,560 --> 00:01:42,240
 there are as many types of meditation as there are mind

16
00:01:42,240 --> 00:01:46,960
 states. And so when you ask a question

17
00:01:46,960 --> 00:01:53,680
 about meditation, the answer is going to be often highly

18
00:01:53,680 --> 00:01:57,040
 dependent on the sort of meditation that

19
00:01:57,040 --> 00:02:05,590
 you're practicing. There are types of meditation that will

20
00:02:05,590 --> 00:02:09,840
 and do lead people to go insane.

21
00:02:09,840 --> 00:02:14,890
 Usually they just temporarily, but it does happen.

22
00:02:14,890 --> 00:02:19,040
 Meditation means training your mind.

23
00:02:19,040 --> 00:02:22,930
 You can train your mind in ways that wind you too tight and

24
00:02:22,930 --> 00:02:25,040
 drive you crazy.

25
00:02:25,200 --> 00:02:32,120
 Most meditation doesn't, but a lot of meditation does lead

26
00:02:32,120 --> 00:02:32,480
 to

27
00:02:32,480 --> 00:02:40,090
 overwhelming states and inhibiting states, states that get

28
00:02:40,090 --> 00:02:41,600
 in the way of

29
00:02:41,600 --> 00:02:46,000
 living your life in any ordinary way.

30
00:02:49,360 --> 00:02:54,140
 So the first thing to check is your meditation practice

31
00:02:54,140 --> 00:02:56,960
 itself. If you're having difficulty,

32
00:02:56,960 --> 00:03:00,620
 sounds like fairly extreme difficulty dealing with people

33
00:03:00,620 --> 00:03:02,880
 after you meditate, not just

34
00:03:02,880 --> 00:03:06,150
 feeling awkward or like you want to be alone, but actually

35
00:03:06,150 --> 00:03:09,200
 physically unable to communicate,

36
00:03:09,200 --> 00:03:14,030
 feeling that your brain is on fire or so on. Too much

37
00:03:14,030 --> 00:03:17,280
 concentration is a very common one.

38
00:03:19,280 --> 00:03:23,600
 You want to check your

39
00:03:23,600 --> 00:03:28,640
 technique. What technique are you practicing?

40
00:03:28,640 --> 00:03:32,520
 Check the posture. Are you just doing sitting meditation or

41
00:03:32,520 --> 00:03:34,640
 are you doing walking as well?

42
00:03:34,640 --> 00:03:37,810
 Because walking can help to break up some of those overly

43
00:03:37,810 --> 00:03:40,000
 strong states of concentration.

44
00:03:44,240 --> 00:03:48,800
 And also importantly, you want to

45
00:03:48,800 --> 00:03:57,040
 check your progress and your direction in that meditation.

46
00:03:57,040 --> 00:04:01,440
 The easiest way to do this, of course, is with the teacher.

47
00:04:01,440 --> 00:04:06,160
 You have a teacher and it allows you to

48
00:04:09,280 --> 00:04:13,110
 to let go of some of the doubt and fear in favor of the

49
00:04:13,110 --> 00:04:15,920
 sort of psychological reassurance of someone

50
00:04:15,920 --> 00:04:19,120
 who knows what they're doing and knows what you're doing.

51
00:04:19,120 --> 00:04:21,200
 You know, we don't know what we're doing.

52
00:04:21,200 --> 00:04:23,650
 Someone else who's a teacher often does know what we're

53
00:04:23,650 --> 00:04:26,240
 doing and can help us.

54
00:04:26,240 --> 00:04:35,680
 But it also allows us to give up control in terms of

55
00:04:35,680 --> 00:04:38,720
 controlling the direction of our practice.

56
00:04:39,840 --> 00:04:44,820
 Because self-directed practice is often like trying to

57
00:04:44,820 --> 00:04:50,000
 perform surgery on yourself, for example.

58
00:04:50,000 --> 00:04:54,400
 Let's say self-therapy, for example. It is a sort of self-

59
00:04:54,400 --> 00:04:56,000
therapy, right?

60
00:04:56,000 --> 00:05:02,240
 It's like trying to see yourself without a mirror.

61
00:05:07,360 --> 00:05:09,920
 Because we're biased, because we're too close to the

62
00:05:09,920 --> 00:05:11,760
 situation, it's our situation.

63
00:05:11,760 --> 00:05:18,960
 We often, with self-directed practice, we miss things. We

64
00:05:18,960 --> 00:05:19,680
're not objective.

65
00:05:19,680 --> 00:05:24,290
 So a teacher giving control to the teacher. And then by

66
00:05:24,290 --> 00:05:25,920
 control, I just mean,

67
00:05:25,920 --> 00:05:28,010
 "What am I going to do today? How am I going to react to

68
00:05:28,010 --> 00:05:30,160
 this? What is the direction?

69
00:05:30,160 --> 00:05:33,760
 What is the instruction?" Having it come from someone else,

70
00:05:33,760 --> 00:05:34,800
 especially someone who

71
00:05:34,800 --> 00:05:39,520
 knows what they're doing, it makes it much more objective

72
00:05:39,520 --> 00:05:42,880
 and much less likely to lead to

73
00:05:42,880 --> 00:05:46,890
 problematic states of mind. So all those things, I would

74
00:05:46,890 --> 00:05:50,560
 say, probably address this person's

75
00:05:50,560 --> 00:05:55,260
 concerns. I would recommend taking a meditation course. We

76
00:05:55,260 --> 00:05:57,200
 do online meditation courses kind of

77
00:05:57,200 --> 00:06:00,810
 for this reason, because there's a lot of people who don't,

78
00:06:00,810 --> 00:06:04,640
 who follow my videos and don't come

79
00:06:04,640 --> 00:06:08,150
 to do meditation courses. So we do an online course where

80
00:06:08,150 --> 00:06:10,880
 they can at least get some direction and get

81
00:06:10,880 --> 00:06:14,850
 some idea of a proper way to practice that doesn't have the

82
00:06:14,850 --> 00:06:17,360
 potential to lead to extreme states.

83
00:06:17,360 --> 00:06:22,270
 But the other thing about meditation, and I don't think it

84
00:06:22,270 --> 00:06:22,960
 really

85
00:06:22,960 --> 00:06:27,810
 is involved with this particular question, but it's

86
00:06:27,810 --> 00:06:30,800
 important to note, is that

87
00:06:33,040 --> 00:06:36,140
 I can't say that mindfulness meditation when practiced

88
00:06:36,140 --> 00:06:39,120
 properly doesn't interfere with your

89
00:06:39,120 --> 00:06:42,810
 life. I think the conditions described by the person posing

90
00:06:42,810 --> 00:06:46,560
 this question have a lot to do with

91
00:06:46,560 --> 00:06:49,180
 how they were practicing and can be fixed. And I don't

92
00:06:49,180 --> 00:06:50,800
 think mindfulness meditation

93
00:06:50,800 --> 00:06:56,660
 should have drastic impact on your interactions with other

94
00:06:56,660 --> 00:06:59,680
 people. But that being said, it does

95
00:06:59,680 --> 00:07:02,090
 change a lot of things. If you practice properly, it

96
00:07:02,090 --> 00:07:04,000
 changes a lot of things about your life.

97
00:07:04,000 --> 00:07:10,210
 The first one being ethics and morality. Maybe the people

98
00:07:10,210 --> 00:07:14,720
 you're surrounding yourself with are

99
00:07:14,720 --> 00:07:19,980
 quite out of touch with the pure state of mind that comes

100
00:07:19,980 --> 00:07:22,960
 from meditation. So when you interact

101
00:07:22,960 --> 00:07:26,090
 with them, it can be quite distressing for both of you,

102
00:07:26,090 --> 00:07:28,400
 really. Why are you a zombie? Why are you

103
00:07:28,400 --> 00:07:31,700
 no longer exciting and entertaining and interested in drugs

104
00:07:31,700 --> 00:07:34,160
 and alcohol and partying and music and

105
00:07:34,160 --> 00:07:40,340
 sex and so on? Distressing for both sides. And when you

106
00:07:40,340 --> 00:07:44,000
 stop killing and stealing and lying and

107
00:07:44,000 --> 00:07:47,910
 cheating and taking drugs and alcohol and you let go of

108
00:07:47,910 --> 00:07:51,120
 entertainment, reduce entertainment and

109
00:07:53,600 --> 00:07:59,090
 beautification and your sexual and romantic desire, it's

110
00:07:59,090 --> 00:08:02,320
 hard to get along with the majority

111
00:08:02,320 --> 00:08:05,450
 of the people in the world. So you do find yourself

112
00:08:05,450 --> 00:08:09,600
 becoming more inclusive. Life does change as a

113
00:08:09,600 --> 00:08:13,040
 result of meditation. And so this person says, "I don't

114
00:08:13,040 --> 00:08:15,120
 want to go and live in a cave." Well,

115
00:08:15,120 --> 00:08:19,050
 living in a cave would be great, you know, if you could. If

116
00:08:19,050 --> 00:08:19,600
 you're really

117
00:08:21,280 --> 00:08:24,240
 barring all the problems that come from practicing alone,

118
00:08:24,240 --> 00:08:27,200
 living in seclusion is a great thing and

119
00:08:27,200 --> 00:08:30,350
 it's not something to be disregarded. When you complain

120
00:08:30,350 --> 00:08:32,560
 about the trouble interacting with other

121
00:08:32,560 --> 00:08:35,260
 people, you have to keep that in mind that meditation is

122
00:08:35,260 --> 00:08:36,800
 not going to allow you to live

123
00:08:36,800 --> 00:08:40,810
 your life as normal. It's going to change many things about

124
00:08:40,810 --> 00:08:46,640
 your life. The upside is that

125
00:08:48,400 --> 00:08:53,970
 what we claim, believe it or not as you like, is that the

126
00:08:53,970 --> 00:08:59,440
 changes are natural and are based on

127
00:08:59,440 --> 00:09:03,310
 the purity of mind that comes from meditation. I don't want

128
00:09:03,310 --> 00:09:06,640
 to sound like a cult leader or that

129
00:09:06,640 --> 00:09:10,620
 you have to give up all your old friends and become part of

130
00:09:10,620 --> 00:09:12,240
 this meditation cult.

131
00:09:12,960 --> 00:09:19,970
 But anything that changes your mind is going to change your

132
00:09:19,970 --> 00:09:24,320
 environment as well. It's going to

133
00:09:24,320 --> 00:09:26,770
 change the people you want to hang out with. So any type of

134
00:09:26,770 --> 00:09:28,240
 meditation is going to do that.

135
00:09:28,240 --> 00:15:40,060
 Any type of mental cultivation is going to do that no

136
00:15:40,060 --> 00:09:38,560
 matter what form it takes.

137
00:09:40,000 --> 00:09:42,400
 If you decide to become a business person and you're going

138
00:09:42,400 --> 00:09:44,080
 to change the people you hang out with,

139
00:09:44,080 --> 00:09:46,840
 you're probably not going to hang around Buddhist medit

140
00:09:46,840 --> 00:09:50,160
ators very much. Or you won't hang out with

141
00:09:50,160 --> 00:09:53,220
 people who are anti-capitalists, for example, if you're a

142
00:09:53,220 --> 00:09:54,320
 business person.

143
00:09:54,320 --> 00:09:59,800
 So if you become a meditator, being around people who are

144
00:09:59,800 --> 00:10:01,200
 not meditating is

145
00:10:01,200 --> 00:10:05,490
 there's going to be some dissonance. Some of them you'll be

146
00:10:05,490 --> 00:10:08,560
 able to interact with, some of them not.

147
00:10:09,840 --> 00:10:12,290
 So two sides to this problem, I would say. It's quite

148
00:10:12,290 --> 00:10:15,600
 simply laid out in that way. First, there's

149
00:10:15,600 --> 00:10:19,710
 the meditation side. Changing your meditation, changing the

150
00:10:19,710 --> 00:10:21,760
 meditation technique you do.

151
00:10:21,760 --> 00:10:25,930
 Sometimes maybe you're just doing a meditation technique

152
00:10:25,930 --> 00:10:28,560
 that is simply forcing the mind into

153
00:10:28,560 --> 00:10:32,390
 high states of concentration. Sometimes that can be

154
00:10:32,390 --> 00:10:35,920
 profitable if done properly. But it's also the

155
00:10:35,920 --> 00:10:41,800
 kind of meditation that should be done in seclusion, in a

156
00:10:41,800 --> 00:10:46,000
 cave or a forest or a mountain or something,

157
00:10:46,000 --> 00:10:50,950
 because it doesn't translate well to dealing with people.

158
00:10:50,950 --> 00:10:53,920
 On the other hand, mindfulness as a

159
00:10:53,920 --> 00:10:58,610
 meditation practice does translate fairly well with dealing

160
00:10:58,610 --> 00:11:03,360
 with people. It takes skill and training.

161
00:11:04,800 --> 00:11:08,160
 But when you feel high states of concentration, you would

162
00:11:08,160 --> 00:11:10,560
 note them. When you feel sweating,

163
00:11:10,560 --> 00:11:15,510
 I think this person mentioned, and something else hard to

164
00:11:15,510 --> 00:11:17,600
 breathe, or I can't remember,

165
00:11:17,600 --> 00:11:22,290
 you would note all these sensations. When done properly,

166
00:11:22,290 --> 00:11:26,480
 when done well, it doesn't need to

167
00:11:26,480 --> 00:11:32,040
 get in the way of your daily life. But also, even once you

168
00:11:32,040 --> 00:11:34,480
've chosen a proper meditation technique,

169
00:11:36,240 --> 00:11:40,230
 it has to be done properly. And so you often need guidance.

170
00:11:40,230 --> 00:11:42,560
 You need to keep ethical

171
00:11:42,560 --> 00:11:46,110
 precepts. If you're not, your meditation is going to get

172
00:11:46,110 --> 00:11:47,200
 all messed up.

173
00:11:47,200 --> 00:11:52,830
 So that's on the meditation side. And on the other side,

174
00:11:52,830 --> 00:11:56,960
 recognizing that your interactions with

175
00:11:56,960 --> 00:11:59,770
 people are going to change, most likely. Over time, you're

176
00:11:59,770 --> 00:12:01,520
 going to be less interested and less

177
00:12:01,520 --> 00:12:04,910
 interesting. So people are going to be less interested in

178
00:12:04,910 --> 00:12:07,120
 you because you're no longer the person...

179
00:12:07,120 --> 00:12:13,040
 I mean, sometimes it's just the stress of change. Some good

180
00:12:13,040 --> 00:12:14,880
 people will be still very shocked by

181
00:12:14,880 --> 00:12:18,130
 the change, and sometimes distressed by the fact that you

182
00:12:18,130 --> 00:12:20,160
've changed until they realize that,

183
00:12:20,160 --> 00:12:23,770
 or admit, hopefully, that the change is for the better. If

184
00:12:23,770 --> 00:12:26,000
 they can't, well, then you have to

185
00:12:26,000 --> 00:12:29,040
 ask yourself, "Is that because of them or because of me?

186
00:12:29,040 --> 00:12:31,040
 Maybe I'm not changing people better." And

187
00:12:31,040 --> 00:12:33,960
 if you're not, well, you shouldn't continue doing that.

188
00:12:33,960 --> 00:12:36,000
 This shouldn't be something that you put

189
00:12:36,000 --> 00:12:39,790
 belief in, and people start seeing you as some kind of cult

190
00:12:39,790 --> 00:12:42,240
 member. It should be something that

191
00:12:42,240 --> 00:12:45,460
 you can see the benefit in, and hopefully eventually are

192
00:12:45,460 --> 00:12:47,520
 able to show other people the benefit.

193
00:12:47,520 --> 00:12:50,890
 What does it do? Does it make you a better person? Does it

194
00:12:50,890 --> 00:12:52,720
 make you happier, more peaceful?

195
00:12:54,640 --> 00:12:59,610
 Or does it turn you into a mindless zombie? To some extent,

196
00:12:59,610 --> 00:13:02,400
 it's going to be perceived as a boat,

197
00:13:02,400 --> 00:13:08,370
 because people who are deeply invested in entertainment and

198
00:13:08,370 --> 00:13:10,080
 excitement and

199
00:13:10,080 --> 00:13:12,640
 sensual pleasure, basically, they're not going to be

200
00:13:12,640 --> 00:13:14,240
 entertained by you anymore. They're not

201
00:13:14,240 --> 00:13:19,140
 going to be interested in you anymore. You don't light

202
00:13:19,140 --> 00:13:22,000
 their fire anymore.

203
00:13:24,000 --> 00:13:31,590
 But on the other hand, you should be able to be more to

204
00:13:31,590 --> 00:13:40,000
 people. You'll meet your bonds with people

205
00:13:40,000 --> 00:13:43,760
 who are headed in positive directions, who are intrigued.

206
00:13:43,760 --> 00:13:46,160
 You'll be better able to relate to good

207
00:13:46,160 --> 00:13:57,870
 people. You'll be more pleasing and more of a positive for

208
00:13:57,870 --> 00:14:02,240
 people who are on a good path.

209
00:14:02,240 --> 00:14:11,640
 If you can understand what is a good path, it depends on

210
00:14:11,640 --> 00:14:14,080
 your understanding.

211
00:14:14,720 --> 00:14:19,590
 For some people, meditation and mindfulness are not so

212
00:14:19,590 --> 00:14:20,800
 interesting.

213
00:14:20,800 --> 00:14:25,440
 And so they're going to find it hard to relate to.

214
00:14:25,440 --> 00:14:33,690
 And you just have to find those people who have an

215
00:14:33,690 --> 00:14:35,040
 understanding similar to yours.

216
00:14:35,040 --> 00:14:40,580
 That peace is good, and contentment is good, and quiet can

217
00:14:40,580 --> 00:14:42,800
 be quite beneficial for

218
00:14:43,840 --> 00:14:47,760
 understanding oneself and finding the truth in life.

219
00:14:47,760 --> 00:14:55,700
 So short, I would say find a teacher, reassess your

220
00:14:55,700 --> 00:14:57,760
 meditation practice,

221
00:14:57,760 --> 00:15:00,780
 do some walking meditation, often a good cure to those

222
00:15:00,780 --> 00:15:01,920
 sorts of states.

223
00:15:01,920 --> 00:15:10,200
 Don't worry too much about what other people think. Don't

224
00:15:10,200 --> 00:15:12,800
 worry too much about fitting in with

225
00:15:13,920 --> 00:15:16,480
 society because the Buddha said the world is blind.

226
00:15:16,480 --> 00:15:19,960
 Unfortunately, you're not going to be able to please

227
00:15:19,960 --> 00:15:20,880
 everyone.

228
00:15:20,880 --> 00:15:26,140
 The meditator is not going to suddenly make you feel

229
00:15:26,140 --> 00:15:28,800
 healing for everyone.

230
00:15:28,800 --> 00:15:33,200
 So that is the answer for today.

231
00:15:33,600 --> 00:15:35,600
 Take a good day, everyone.

232
00:15:35,600 --> 00:15:37,600
 [water flowing]

