1
00:00:00,000 --> 00:00:05,000
 Good evening everyone.

2
00:00:05,000 --> 00:00:12,000
 I'm broadcasting live July 8th.

3
00:00:12,000 --> 00:00:21,000
 Today's quote is about sickness.

4
00:00:21,000 --> 00:00:26,000
 Two kinds of sickness.

5
00:00:26,000 --> 00:00:31,000
 It's a nice little quote.

6
00:00:31,000 --> 00:00:33,000
 Two kinds of sickness.

7
00:00:33,000 --> 00:00:35,000
 Physical and mental.

8
00:00:35,000 --> 00:00:49,000
 Kaikou jarouko jeda sikou jarouko.

9
00:00:49,000 --> 00:01:01,000
 And it says there are differences with physical sickness.

10
00:01:01,000 --> 00:01:07,440
 Well, you see people without physical sickness for days,

11
00:01:07,440 --> 00:01:17,000
 weeks, months, years on end.

12
00:01:17,000 --> 00:01:25,000
 But nobody escapes mental sickness even for a moment.

13
00:01:25,000 --> 00:01:29,000
 Except for those who are in light.

14
00:01:29,000 --> 00:01:35,070
 Mental illness is something that most people are plagued by

15
00:01:35,070 --> 00:01:37,000
 moment after moment.

16
00:01:37,000 --> 00:01:41,000
 Day in and day out.

17
00:01:41,000 --> 00:01:56,000
 A type of sickness that everyone carries with them.

18
00:01:56,000 --> 00:02:01,510
 So in Buddhism, mental illness isn't simply the extreme

19
00:02:01,510 --> 00:02:10,000
 cases of clinical depression or anxiety or OCD or phobias.

20
00:02:10,000 --> 00:02:16,000
 Psychoses, all of these things are just extreme.

21
00:02:16,000 --> 00:02:21,670
 Extreme to the extent that an ordinary, un-instructed

22
00:02:21,670 --> 00:02:31,000
 person can recognize them.

23
00:02:31,000 --> 00:02:39,000
 And latch onto them and identify them as a problem.

24
00:02:39,000 --> 00:02:45,280
 But most of the time we don't recognize and identify how

25
00:02:45,280 --> 00:02:49,000
 our mental illness is for what they are.

26
00:02:49,000 --> 00:03:04,730
 Just illness and discord, distress, until we come to med

27
00:03:04,730 --> 00:03:06,000
itate.

28
00:03:06,000 --> 00:03:09,910
 There are many people when they begin to meditate for the

29
00:03:09,910 --> 00:03:11,000
 first time.

30
00:03:11,000 --> 00:03:16,210
 It's like a revelation, a realization that our minds aren't

31
00:03:16,210 --> 00:03:20,000
 quite as healthy as we thought they were.

32
00:03:20,000 --> 00:03:29,000
 That our minds are plagued by...

33
00:03:29,000 --> 00:03:37,180
 ...the illness of greed, the illness of anger, the illness

34
00:03:37,180 --> 00:03:49,000
 of delusion, and the illnesses.

35
00:03:49,000 --> 00:03:55,710
 That really is the illness and the problem that we should

36
00:03:55,710 --> 00:04:02,710
 focus on and should understand Buddhism to focus on more so

37
00:04:02,710 --> 00:04:06,000
 than simply suffering.

38
00:04:06,000 --> 00:04:09,000
 More important is the cause of suffering.

39
00:04:09,000 --> 00:04:13,000
 If we are suffering, well, we can live with it.

40
00:04:13,000 --> 00:04:17,620
 It is possible to be at peace with what we would normally

41
00:04:17,620 --> 00:04:20,000
 term suffering.

42
00:04:20,000 --> 00:04:23,000
 There is no peace for one who has mental illness.

43
00:04:23,000 --> 00:04:29,000
 The suffering comes because we have mental illness.

44
00:04:29,000 --> 00:04:32,000
 And it's really suffering.

45
00:04:32,000 --> 00:04:40,830
 It becomes suffering because of our mental illness, because

46
00:04:40,830 --> 00:04:47,000
 of our greed, our anger, our delusion.

47
00:04:47,000 --> 00:04:50,150
 I will put in another place, let's go on the subject of

48
00:04:50,150 --> 00:04:51,000
 sickness.

49
00:04:51,000 --> 00:04:54,000
 There are about four types of sickness.

50
00:04:54,000 --> 00:04:59,000
 Sickness from utu, which means the environment.

51
00:04:59,000 --> 00:05:03,000
 Sickness from food, hahara.

52
00:05:03,000 --> 00:05:12,000
 Sickness from the mind, jitta, or jeda-sikamim.

53
00:05:12,000 --> 00:05:22,000
 And sickness from karma, kamma.

54
00:05:22,000 --> 00:05:24,000
 Four types of sickness.

55
00:05:24,000 --> 00:05:27,700
 So in a little more detail, physical sickness is either

56
00:05:27,700 --> 00:05:33,760
 from the environment or from what is around you or from

57
00:05:33,760 --> 00:05:37,000
 what you take inside of you.

58
00:05:37,000 --> 00:05:43,720
 So things like viruses and radiation and environmental

59
00:05:43,720 --> 00:05:50,320
 distress and the environment, discomfort, these would be ut

60
00:05:50,320 --> 00:05:51,000
u.

61
00:05:51,000 --> 00:05:58,000
 Ahara from food, what we eat and drink causes of sickness.

62
00:05:58,000 --> 00:06:03,000
 These are the causes of physical sickness.

63
00:06:03,000 --> 00:06:08,000
 Mental sickness is caused by the mind or caused by karma,

64
00:06:08,000 --> 00:06:12,660
 which is basically the same thing, but karma means in the

65
00:06:12,660 --> 00:06:14,000
 past.

66
00:06:14,000 --> 00:06:22,000
 So you've done nasty things in the past.

67
00:06:22,000 --> 00:06:28,000
 As a result, you have physical and mental impairment.

68
00:06:28,000 --> 00:06:36,130
 Why people are born with certain disabilities, why we

69
00:06:36,130 --> 00:06:40,000
 suddenly get cancer,

70
00:06:40,000 --> 00:06:48,000
 any debilitating disease, multiple sclerosis, etc.

71
00:06:48,000 --> 00:06:54,000
 Not to mention the diseases that come from our greed and

72
00:06:54,000 --> 00:06:58,000
 those are more than the food type.

73
00:06:58,000 --> 00:07:02,780
 But the fourth one from the mind means simply the sickness

74
00:07:02,780 --> 00:07:08,000
 that comes from being mentally sick, from being angry.

75
00:07:08,000 --> 00:07:13,000
 Anytime we're angry, this is considered a mental illness.

76
00:07:13,000 --> 00:07:16,220
 The fact that we have anger is considered to be a mental

77
00:07:16,220 --> 00:07:17,000
 illness.

78
00:07:17,000 --> 00:07:20,300
 When we are addicted to things, when we want things, just

79
00:07:20,300 --> 00:07:24,000
 wanting something is considered to be a mental illness.

80
00:07:24,000 --> 00:07:28,340
 It's a wrong in the mind. It's a cause of suffering for the

81
00:07:28,340 --> 00:07:29,000
 mind.

82
00:07:29,000 --> 00:07:35,000
 It's a cause of stress and distress.

83
00:07:35,000 --> 00:07:42,580
 Anytime we have delusion, arrogance, conceit, or the fact

84
00:07:42,580 --> 00:07:47,490
 that we have these in our mind, this is a cause for illness

85
00:07:47,490 --> 00:07:48,000
.

86
00:07:48,000 --> 00:07:55,000
 It's a type of illness.

87
00:07:55,000 --> 00:08:00,280
 This is the Dhamma of the Buddha. This is how we look at

88
00:08:00,280 --> 00:08:01,000
 things.

89
00:08:01,000 --> 00:08:05,510
 It sounds kind of depressing, I suppose. I've actually had

90
00:08:05,510 --> 00:08:09,270
 people say to me that it's not really fair to call these

91
00:08:09,270 --> 00:08:14,000
 things illness when they're all manageable.

92
00:08:14,000 --> 00:08:17,360
 As though there's a categorical difference between someone

93
00:08:17,360 --> 00:08:20,200
 who is clinically depressed and someone who is just

94
00:08:20,200 --> 00:08:21,000
 depressed.

95
00:08:21,000 --> 00:08:25,620
 There isn't. There's not a categorical difference until you

96
00:08:25,620 --> 00:08:27,000
 create a category.

97
00:08:27,000 --> 00:08:31,550
 Because it's just a matter of degree and eventually the

98
00:08:31,550 --> 00:08:38,760
 perpetuation, the feedback loop that becomes so strong that

99
00:08:38,760 --> 00:08:42,000
 it becomes unmanageable.

100
00:08:42,000 --> 00:08:44,420
 So you dislike something or you're depressed about

101
00:08:44,420 --> 00:08:48,130
 something and that makes you depressed and more depressed

102
00:08:48,130 --> 00:08:49,000
 and depressed.

103
00:08:49,000 --> 00:08:53,490
 You enter this feedback loop where you're actually feeding

104
00:08:53,490 --> 00:08:56,000
 the depression with depression.

105
00:08:56,000 --> 00:08:58,520
 You get angry and then you're angry at the fact that

106
00:08:58,520 --> 00:09:00,000
 someone's made you angry.

107
00:09:00,000 --> 00:09:07,000
 You stew in your anger and it becomes unmanageable.

108
00:09:07,000 --> 00:09:11,390
 Anxiety, you become anxious that you're anxious, afraid of

109
00:09:11,390 --> 00:09:13,000
 your fear and so on.

110
00:09:13,000 --> 00:09:22,000
 You just feed them and by feeding them you end up clinical.

111
00:09:22,000 --> 00:09:25,730
 It's just a matter of degree and so certainly something

112
00:09:25,730 --> 00:09:29,570
 that we should be concerned about and we should be with

113
00:09:29,570 --> 00:09:31,000
 religion towards.

114
00:09:31,000 --> 00:09:35,000
 So it doesn't become unmanageable.

115
00:09:35,000 --> 00:09:45,000
 [Silence]

116
00:09:45,000 --> 00:09:49,000
 No questions today.

117
00:09:49,000 --> 00:10:12,000
 Do we have questions from Australia?

118
00:10:12,000 --> 00:10:20,000
 Why is entertainment dangerous? Understanding the nature of

119
00:10:20,000 --> 00:10:20,000
 it being impermanent and suffering a non-self, what makes

120
00:10:20,000 --> 00:10:20,000
 it dangerous?

121
00:10:20,000 --> 00:10:30,000
 Attachment.

122
00:10:30,000 --> 00:10:33,000
 Got some good talk I suppose.

123
00:10:33,000 --> 00:10:37,690
 Not going to read through it all. Really think you should

124
00:10:37,690 --> 00:10:44,020
 put the cue before the question otherwise it's hard to go

125
00:10:44,020 --> 00:10:46,000
 through here.

126
00:10:46,000 --> 00:10:51,740
 I'm going to have to insist. I'm not going to go back

127
00:10:51,740 --> 00:10:57,000
 through all that talk to find questions.

128
00:10:57,000 --> 00:11:01,550
 So to make a question click on the yellow or green question

129
00:11:01,550 --> 00:11:02,000
 mark.

130
00:11:02,000 --> 00:11:06,270
 That puts the right tag before your question and I can

131
00:11:06,270 --> 00:11:08,000
 easily identify.

132
00:11:08,000 --> 00:11:11,220
 Could my unsatisfying lack of progress in meditation, my

133
00:11:11,220 --> 00:11:20,270
 lack of confidence in seeing the hindrances be due to kamma

134
00:11:20,270 --> 00:11:21,000
?

135
00:11:21,000 --> 00:11:26,550
 Do you have doubt about your practice? Doubt about your

136
00:11:26,550 --> 00:11:29,000
 progress? Because that's a hindrance.

137
00:11:29,000 --> 00:11:33,000
 You can say doubting, doubting.

138
00:11:33,000 --> 00:11:36,220
 It may be not easy to understand what is the progress but

139
00:11:36,220 --> 00:11:39,580
 if you're practicing correctly it's hard to imagine that

140
00:11:39,580 --> 00:11:41,000
 you're not progressing.

141
00:11:41,000 --> 00:11:55,210
 You're not learning to see yourself clearly and to refine

142
00:11:55,210 --> 00:12:00,000
 your character.

143
00:12:00,000 --> 00:12:06,190
 When faced with such continued adversary, violence, racism

144
00:12:06,190 --> 00:12:11,180
 in the world today, how do we teach others to come to

145
00:12:11,180 --> 00:12:16,000
 understand the same benefits of meditation?

146
00:12:16,000 --> 00:12:29,000
 Without seeing unconcerned with our non-reaction.

147
00:12:29,000 --> 00:12:33,090
 If you find that it's considered unjust or cruel not to

148
00:12:33,090 --> 00:12:41,130
 have a fighting opinion when existing in a world that

149
00:12:41,130 --> 00:12:54,000
 demands outrage over every negative situation that arises.

150
00:12:54,000 --> 00:13:00,000
 Well yeah society is messed up, nothing new there.

151
00:13:00,000 --> 00:13:05,000
 We esteem all the wrong things.

152
00:13:05,000 --> 00:13:11,000
 We esteem addiction for what it's worth.

153
00:13:11,000 --> 00:13:15,000
 We certainly esteem opinions, views.

154
00:13:15,000 --> 00:13:20,000
 We esteem outrage.

155
00:13:20,000 --> 00:13:27,170
 We have esteem for things that in the end don't turn out to

156
00:13:27,170 --> 00:13:30,000
 be all that useful.

157
00:13:30,000 --> 00:13:35,240
 We get caught up in things that end up being inconsequ

158
00:13:35,240 --> 00:13:36,000
ential.

159
00:13:36,000 --> 00:13:42,000
 Asa re sara dasinho sare jasara matinoh.

160
00:13:42,000 --> 00:13:49,000
 Asa re sara matinoh sare jasara dasinho.

161
00:13:49,000 --> 00:13:55,110
 We see what is fundamental or what is essential as being un

162
00:13:55,110 --> 00:14:01,000
essential and what is unessential as being essential.

163
00:14:01,000 --> 00:14:07,400
 Dei saarang nadi kath chei nadi kath chit nadi kath chei ya

164
00:14:07,400 --> 00:14:08,000
.

165
00:14:08,000 --> 00:14:13,000
 Mi ca go ca ra sankap.

166
00:14:13,000 --> 00:14:18,000
 Tati kath chantin.

167
00:14:18,000 --> 00:14:25,870
 Such people don't come to it as essential because they're

168
00:14:25,870 --> 00:14:33,000
 always feeding in the wrong pasture.

169
00:14:33,000 --> 00:14:48,000
 They let their minds wander in the wrong pastures.

170
00:14:48,000 --> 00:14:58,790
 So to answer your question, it's not easy to teach yourself

171
00:14:58,790 --> 00:15:03,000
, let alone others.

172
00:15:03,000 --> 00:15:06,000
 This is why we focus on our own well-being.

173
00:15:06,000 --> 00:15:08,000
 You become an example to others.

174
00:15:08,000 --> 00:15:15,250
 People see a better way, and see a way to free themselves

175
00:15:15,250 --> 00:15:18,000
 and to help the world.

176
00:15:18,000 --> 00:15:21,000
 But only if you become an example.

177
00:15:21,000 --> 00:15:31,000
 So work on yourself.

178
00:15:31,000 --> 00:15:38,050
 If you hold fast, if you hold fast to the fact that you're

179
00:15:38,050 --> 00:15:41,000
 not outraged at things,

180
00:15:41,000 --> 00:15:45,540
 there will be people who appreciate that and who esteem

181
00:15:45,540 --> 00:15:49,000
 that and who are affected positively by that.

182
00:15:49,000 --> 00:15:55,190
 Those people who get angry at you, well, it says more about

183
00:15:55,190 --> 00:15:59,000
 them than about you.

184
00:15:59,000 --> 00:16:05,920
 It's sometimes better to let people who are raging let them

185
00:16:05,920 --> 00:16:07,000
 rage.

186
00:16:07,000 --> 00:16:20,000
 Yes, but to be patient with them, unmoved.

187
00:16:20,000 --> 00:16:23,000
 It's really kind of silly to get outraged about everything

188
00:16:23,000 --> 00:16:25,000
 when this sort of thing,

189
00:16:25,000 --> 00:16:30,000
 outrageous things have been sort of the norm of humanity.

190
00:16:30,000 --> 00:16:34,000
 To be surprised by them is naive.

191
00:16:34,000 --> 00:16:40,830
 And to fight for that to change, it's much better to accept

192
00:16:40,830 --> 00:16:43,000
 it as part of the human condition

193
00:16:43,000 --> 00:16:48,190
 and to work with it in the sense of actually trying to

194
00:16:48,190 --> 00:16:49,000
 change,

195
00:16:49,000 --> 00:16:52,000
 not getting angry at the fact that that's part of humanity,

196
00:16:52,000 --> 00:16:54,000
 that there's evil in humanity.

197
00:16:54,000 --> 00:16:59,060
 But to work on good, to better yourself, to better those

198
00:16:59,060 --> 00:17:00,000
 around you,

199
00:17:00,000 --> 00:17:04,000
 to help people to come to let go of their evil inside,

200
00:17:04,000 --> 00:17:07,430
 to help people to see that getting angry at bad things is

201
00:17:07,430 --> 00:17:09,000
 making things worse.

202
00:17:09,000 --> 00:17:15,000
 It's just cultivating a habit of anger.

203
00:17:15,000 --> 00:17:20,000
 You just end up burnt out. You don't believe this theory.

204
00:17:20,000 --> 00:17:23,380
 Look at those people who get burnt out from activism, who

205
00:17:23,380 --> 00:17:27,000
 get burnt out from social justice.

206
00:17:27,000 --> 00:17:31,640
 They don't end up helping people, not in any meaningful way

207
00:17:31,640 --> 00:17:32,000
.

208
00:17:32,000 --> 00:17:36,000
 Well, it's maybe not true. They do often end up,

209
00:17:36,000 --> 00:17:40,680
 but not because of the anger, they often end up, because of

210
00:17:40,680 --> 00:17:42,000
 their determination,

211
00:17:42,000 --> 00:17:44,000
 they'll end up changing the system.

212
00:17:44,000 --> 00:17:49,000
 You don't even be angry to change the system.

213
00:17:49,000 --> 00:17:52,870
 Some of the civil rights movement in America, for example,

214
00:17:52,870 --> 00:17:54,000
 that we hear about,

215
00:17:54,000 --> 00:17:56,000
 a lot of it wasn't very angry.

216
00:17:56,000 --> 00:17:59,900
 It was a matter of just having sit-ins and sitting in the

217
00:17:59,900 --> 00:18:05,000
 wrong place on the bus.

218
00:18:05,000 --> 00:18:07,000
 Just to be adamant.

219
00:18:07,000 --> 00:18:14,530
 I would argue from a Buddhist point of view, but that sort

220
00:18:14,530 --> 00:18:15,000
 of thing is much more,

221
00:18:15,000 --> 00:18:20,520
 much more, a much more, much more, much more acceptable to

222
00:18:20,520 --> 00:18:22,000
 a Buddhist.

223
00:18:22,000 --> 00:18:24,000
 The Buddha himself gave silent protests.

224
00:18:24,000 --> 00:18:28,350
 There's this one case where this king was going, it's a

225
00:18:28,350 --> 00:18:29,000
 long story,

226
00:18:29,000 --> 00:18:35,330
 but actually this guy was a bastard son of one of the

227
00:18:35,330 --> 00:18:38,000
 Buddhist relatives,

228
00:18:38,000 --> 00:18:42,000
 and so they wouldn't recognize him.

229
00:18:42,000 --> 00:18:45,150
 They didn't want to anger him, so they pretended to

230
00:18:45,150 --> 00:18:46,000
 recognize him.

231
00:18:46,000 --> 00:18:48,890
 In the end, they wouldn't even let him eat at the same

232
00:18:48,890 --> 00:18:50,000
 table with them.

233
00:18:50,000 --> 00:18:52,000
 They were so proud.

234
00:18:52,000 --> 00:18:59,750
 He got so angry that he vowed to kill all the Buddhist

235
00:18:59,750 --> 00:19:02,000
 relatives.

236
00:19:02,000 --> 00:19:06,480
 So he went with his army one time, and the Buddha

237
00:19:06,480 --> 00:19:07,000
 intercepted him,

238
00:19:07,000 --> 00:19:10,950
 and the Buddha was sitting there under a tree, but the tree

239
00:19:10,950 --> 00:19:12,000
 had no leaves,

240
00:19:12,000 --> 00:19:14,000
 or very few leaves.

241
00:19:14,000 --> 00:19:18,000
 It was an almost dead tree.

242
00:19:18,000 --> 00:19:20,950
 The king was marching by, and he was told that the Buddha

243
00:19:20,950 --> 00:19:22,000
 was sitting there,

244
00:19:22,000 --> 00:19:28,000
 and so he got down and went to see the Buddha.

245
00:19:28,000 --> 00:19:32,800
 He actually had some faith in the Buddha, even though he

246
00:19:32,800 --> 00:19:39,000
 was a warmongering king.

247
00:19:39,000 --> 00:19:41,000
 He said to the Buddha, "What are you doing?

248
00:19:41,000 --> 00:19:43,000
 Venerable stars sitting under this tree with no shade.

249
00:19:43,000 --> 00:19:46,000
 Why don't you find a better tree?"

250
00:19:46,000 --> 00:19:48,000
 The Buddha said, "Oh, that's okay.

251
00:19:48,000 --> 00:19:53,000
 I live under the cool shade of my relatives."

252
00:19:53,000 --> 00:19:55,460
 So he wouldn't even go to the king and tell him, "Look, you

253
00:19:55,460 --> 00:19:56,000
're wrong.

254
00:19:56,000 --> 00:19:58,000
 Stop it."

255
00:19:58,000 --> 00:20:07,000
 Instead, he sought to remind the king that his relatives

256
00:20:07,000 --> 00:20:09,000
 had good in them.

257
00:20:09,000 --> 00:20:15,620
 They may have been pompous jerks, but they were still

258
00:20:15,620 --> 00:20:17,000
 people.

259
00:20:17,000 --> 00:20:21,380
 They were still a support to humanity in some way, support

260
00:20:21,380 --> 00:20:23,000
 to the Buddha in some way.

261
00:20:23,000 --> 00:20:32,590
 So the king realized, "Oh, the Buddha benefits from his

262
00:20:32,590 --> 00:20:34,000
 relatives."

263
00:20:34,000 --> 00:20:38,000
 So he turned around, called off the war.

264
00:20:38,000 --> 00:20:41,050
 Except he couldn't keep down his anger, so he ended up

265
00:20:41,050 --> 00:20:43,000
 heading off to war again.

266
00:20:43,000 --> 00:20:45,000
 Again, the Buddha did the same thing.

267
00:20:45,000 --> 00:20:48,120
 Then a third time, it's a really interesting story, the

268
00:20:48,120 --> 00:20:49,000
 third time the king says,

269
00:20:49,000 --> 00:20:52,000
 "Look, I'm just going to do it."

270
00:20:52,000 --> 00:20:57,610
 The Buddha sort of reflected on the situation and didn't go

271
00:20:57,610 --> 00:20:58,000
.

272
00:20:58,000 --> 00:21:02,000
 He decided it wasn't going to come to a good end.

273
00:21:02,000 --> 00:21:04,000
 There was nothing he could do to stop it.

274
00:21:04,000 --> 00:21:07,000
 In the end, many of the Buddha's relatives were wiped out,

275
00:21:07,000 --> 00:21:10,000
 the majority of them,

276
00:21:10,000 --> 00:21:13,000
 because the Buddha just let it happen.

277
00:21:13,000 --> 00:21:15,000
 In the end, he felt he couldn't...

278
00:21:15,000 --> 00:21:17,640
 He would have had to get angry, he would have had to get

279
00:21:17,640 --> 00:21:19,000
 upset.

280
00:21:19,000 --> 00:21:26,000
 In a sense, that's...

281
00:21:26,000 --> 00:21:28,000
 You can't stop people from dying.

282
00:21:28,000 --> 00:21:32,000
 You can't stop bad things from happening.

283
00:21:32,000 --> 00:21:36,000
 What we have to stop is things like anger.

284
00:21:36,000 --> 00:21:38,000
 So getting angry doesn't help.

285
00:21:38,000 --> 00:21:42,000
 It sends the wrong message.

286
00:21:42,000 --> 00:21:45,000
 Much more important is to send a message.

287
00:21:45,000 --> 00:21:47,000
 Stop killing each other.

288
00:21:47,000 --> 00:21:53,160
 To actually try and stop one killing or one catastrophe is

289
00:21:53,160 --> 00:21:55,000
 not all that important.

290
00:21:55,000 --> 00:22:01,140
 Those beings are in cycle, they're born and die and born

291
00:22:01,140 --> 00:22:02,000
 and die.

292
00:22:02,000 --> 00:22:08,000
 It's part of a circle of life.

293
00:22:08,000 --> 00:22:12,000
 But the mind, the mind doesn't die.

294
00:22:12,000 --> 00:22:24,000
 It dies every moment, but the mind of a person keeps going.

295
00:22:24,000 --> 00:22:28,000
 Anyway, sickness.

296
00:22:28,000 --> 00:22:32,000
 All right, we're into questions.

297
00:22:32,000 --> 00:22:35,000
 Only one or two questions.

298
00:22:35,000 --> 00:22:39,000
 Must have answered all the questions last night.

299
00:22:39,000 --> 00:22:47,000
 Well, in that case, have a good evening, everyone.

300
00:22:47,000 --> 00:22:50,000
 Tomorrow I might be busy, I might not...

301
00:22:50,000 --> 00:22:52,000
 I should be here.

302
00:22:52,000 --> 00:22:56,000
 But if I'm not here some days, my apologies.

303
00:22:56,000 --> 00:22:58,000
 I should be here.

304
00:22:58,000 --> 00:23:00,000
 Anyway, have a good night.

