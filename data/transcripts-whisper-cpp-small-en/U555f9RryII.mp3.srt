1
00:00:00,000 --> 00:00:06,790
 Basically what we want to know is what you feel you gained

2
00:00:06,790 --> 00:00:07,000
 from the practice.

3
00:00:07,000 --> 00:00:13,010
 What's different now that you finished the course from your

4
00:00:13,010 --> 00:00:16,350
 feeling before you started the course or before you started

5
00:00:16,350 --> 00:00:17,000
 the course?

6
00:00:17,000 --> 00:00:26,000
 Well, a lot of things changed actually.

7
00:00:26,000 --> 00:00:30,390
 Before I came here, well actually the reason why I came

8
00:00:30,390 --> 00:00:35,990
 here is because I was usually feeling anxious and it wasn't

9
00:00:35,990 --> 00:00:38,000
 sort of like strong, anxious.

10
00:00:38,000 --> 00:00:42,090
 I knew that there was a subtle sense of underlying anxiety

11
00:00:42,090 --> 00:00:43,000
 inside of me.

12
00:00:43,000 --> 00:00:48,000
 And there are bouts of feelings of anger and depression,

13
00:00:48,000 --> 00:00:50,320
 you know, things that people usually feel, but I wasn't

14
00:00:50,320 --> 00:00:51,000
 really okay with it.

15
00:00:51,000 --> 00:00:55,370
 I realized that I wasn't going to live a life where I just

16
00:00:55,370 --> 00:00:59,000
 had to cope with things constantly.

17
00:00:59,000 --> 00:01:03,710
 I wanted to be not to cope with those things. I wanted to

18
00:01:03,710 --> 00:01:07,000
 live and coexist peacefully with my emotions.

19
00:01:07,000 --> 00:01:11,840
 I didn't want to view them as if I was against them or as

20
00:01:11,840 --> 00:01:15,000
 if they were some sort of enemy.

21
00:01:15,000 --> 00:01:24,150
 So, you know, I came here to meditate and one of the major

22
00:01:24,150 --> 00:01:28,230
 things that changed me was that the fact that I couldn't do

23
00:01:28,230 --> 00:01:29,000
 anything.

24
00:01:29,000 --> 00:01:32,500
 Which is basically, I think that was the hardest thing that

25
00:01:32,500 --> 00:01:36,000
 I have ever done, ironically speaking, to do nothing.

26
00:01:36,000 --> 00:01:40,000
 Because at home you have all these things that you can do.

27
00:01:40,000 --> 00:01:44,060
 You go to school, you go to work, you do volunteer, you

28
00:01:44,060 --> 00:01:46,000
 hang out with friends.

29
00:01:46,000 --> 00:01:50,580
 At home you have your computer and basically that takes

30
00:01:50,580 --> 00:01:52,000
 away everything.

31
00:01:52,000 --> 00:01:57,000
 And you have all these sorts of distractions.

32
00:01:57,000 --> 00:02:02,220
 So when you have emotional problems, you don't really have

33
00:02:02,220 --> 00:02:06,210
 the time or have the attention to really scrutinize them in

34
00:02:06,210 --> 00:02:09,000
 a way, to really confront them,

35
00:02:09,000 --> 00:02:12,000
 to see what they really are in their truest nature.

36
00:02:12,000 --> 00:02:15,430
 And so when you come here, that's the reason why it's so

37
00:02:15,430 --> 00:02:18,000
 difficult for me to do nothing.

38
00:02:18,000 --> 00:02:23,120
 It's that I have all these emotions, all of these negative

39
00:02:23,120 --> 00:02:27,440
 thoughts that I never thought, I never knew that they were

40
00:02:27,440 --> 00:02:28,000
 so serious.

41
00:02:28,000 --> 00:02:34,000
 I never knew that they were that extreme, in a sense.

42
00:02:34,000 --> 00:02:39,950
 And so, you know, you spend time here and sometimes they

43
00:02:39,950 --> 00:02:43,000
 come up, sometimes they go.

44
00:02:43,000 --> 00:02:46,480
 But the thing you realize is that you start to adapt to

45
00:02:46,480 --> 00:02:49,000
 this certain kind of environment.

46
00:02:49,000 --> 00:02:53,600
 You adapt to the fact that since there is nothing you can

47
00:02:53,600 --> 00:02:57,000
 do, you might as well be okay with it.

48
00:02:57,000 --> 00:03:01,310
 I think that's the biggest thing that I learned here, is

49
00:03:01,310 --> 00:03:05,000
 that once you have absolutely no distractions,

50
00:03:05,000 --> 00:03:12,430
 nothing can take you away from what's the raw form of

51
00:03:12,430 --> 00:03:14,000
 yourself.

52
00:03:14,000 --> 00:03:19,000
 And you really start to confront who you are as a person.

53
00:03:19,000 --> 00:03:23,000
 You start to realize there's nothing really bad about it.

54
00:03:23,000 --> 00:03:27,000
 I realized I don't know why I was running away from them.

55
00:03:27,000 --> 00:03:33,000
 I honestly don't know why I was trying to distract myself,

56
00:03:33,000 --> 00:03:37,420
 clinging on to positive thoughts, clinging on to positive

57
00:03:37,420 --> 00:03:39,000
 emotions and experiences,

58
00:03:39,000 --> 00:03:43,000
 so that I don't have to experience those negative ones.

59
00:03:43,000 --> 00:03:49,310
 Since I came here, I realized, in meditation, I've sat

60
00:03:49,310 --> 00:03:54,000
 through some of my worst emotions and thoughts ever.

61
00:03:54,000 --> 00:03:58,970
 Just feelings of shame and guilt and sadness and anger and

62
00:03:58,970 --> 00:04:01,000
 extreme panic attacks.

63
00:04:01,000 --> 00:04:08,000
 I wanted to just punch a wall or do something.

64
00:04:08,000 --> 00:04:12,560
 Many times I wanted to run away to home and just distract

65
00:04:12,560 --> 00:04:14,000
 myself again.

66
00:04:14,000 --> 00:04:18,000
 But you can do that, and that's one of the best things.

67
00:04:18,000 --> 00:04:22,820
 But you sit through them eventually, and you realize that I

68
00:04:22,820 --> 00:04:24,000
'm still here.

69
00:04:24,000 --> 00:04:29,990
 That despite the suffering that you go through, despite the

70
00:04:29,990 --> 00:04:35,000
 discomfort and negativity that you experience,

71
00:04:35,000 --> 00:04:38,330
 you're still here, and you take a look around you, reality

72
00:04:38,330 --> 00:04:39,000
 is still here.

73
00:04:39,000 --> 00:04:42,890
 Reality hasn't changed. All that has changed is what's

74
00:04:42,890 --> 00:04:44,000
 inside of you.

75
00:04:44,000 --> 00:04:48,000
 Inside of you, it constantly fluctuates.

76
00:04:48,000 --> 00:04:56,410
 And so you learn to be mindful, and you learn to let go,

77
00:04:56,410 --> 00:04:58,000
 really.

78
00:04:58,000 --> 00:05:05,680
 You realize that your emotions and your thoughts are not

79
00:05:05,680 --> 00:05:09,000
 really that harmful, whether they're good or not.

80
00:05:09,000 --> 00:05:13,600
 They're just natural. They're like nature. They're like the

81
00:05:13,600 --> 00:05:16,000
 weather, as I was thinking the other day.

82
00:05:16,000 --> 00:05:19,000
 Sometimes it rains, sometimes it doesn't.

83
00:05:19,000 --> 00:05:21,030
 It doesn't mean that you have to get upset every single

84
00:05:21,030 --> 00:05:22,000
 time you're in a rain.

85
00:05:22,000 --> 00:05:24,000
 You can just go outside and say, "Oh, it's raining."

86
00:05:24,000 --> 00:05:26,520
 So what? Tomorrow it might be sunny or not? It doesn't

87
00:05:26,520 --> 00:05:29,000
 matter. You just enjoy the moment.

88
00:05:29,000 --> 00:05:36,000
 So that's one of the biggest things that I've ever learned.

89
00:05:36,000 --> 00:05:42,570
 And it made me have this sense of fearlessness, that I

90
00:05:42,570 --> 00:05:50,210
 could go into several situations that I wasn't able to

91
00:05:50,210 --> 00:05:51,000
 before,

92
00:05:51,000 --> 00:05:55,620
 because I was afraid of the thoughts and emotions that

93
00:05:55,620 --> 00:05:58,360
 would generate if I wasn't in those experiences and

94
00:05:58,360 --> 00:05:59,000
 situations.

95
00:05:59,000 --> 00:06:02,980
 But now, since I've spent so many days realizing the fact

96
00:06:02,980 --> 00:06:05,000
 that my emotions fluctuate,

97
00:06:05,000 --> 00:06:07,690
 and the worst way to deal with them is to cling on to

98
00:06:07,690 --> 00:06:12,000
 positive experiences and to avoid negative ones,

99
00:06:12,000 --> 00:06:15,000
 because ultimately you can't really control them.

100
00:06:15,000 --> 00:06:17,390
 You can't control your thoughts. You can't control your

101
00:06:17,390 --> 00:06:18,000
 emotions.

102
00:06:18,000 --> 00:06:23,000
 They're just a part of whatever is happening.

103
00:06:23,000 --> 00:06:25,000
 I don't know whatever really is happening.

104
00:06:25,000 --> 00:06:31,200
 Sometimes I'm moody, sometimes I'm not, sometimes I feel

105
00:06:31,200 --> 00:06:32,000
 anxiety, sometimes I'm not.

106
00:06:32,000 --> 00:06:36,280
 But the content of those things, it's irrelevant. That's

107
00:06:36,280 --> 00:06:37,000
 what I've learned.

108
00:06:37,000 --> 00:06:43,030
 You start to realize there are certain actions that you

109
00:06:43,030 --> 00:06:44,000
 make.

110
00:06:44,000 --> 00:06:49,740
 But the content of those things, they're just sort of an

111
00:06:49,740 --> 00:06:53,000
 illusion that you create for yourself.

112
00:06:53,000 --> 00:06:58,210
 Everyone has stories and histories and dramas about their

113
00:06:58,210 --> 00:07:02,000
 lives, but they're not part of reality.

114
00:07:02,000 --> 00:07:06,000
 That's what I've learned.

115
00:07:06,000 --> 00:07:13,320
 It's like when I was saying that it fluctuates. Whatever's

116
00:07:13,320 --> 00:07:15,000
 inside it fluctuates, it changes,

117
00:07:15,000 --> 00:07:20,000
 but whatever's outside, it's still there.

118
00:07:20,000 --> 00:07:24,580
 And you realize that. And as you become more mindful, you

119
00:07:24,580 --> 00:07:27,000
 start to realize that,

120
00:07:27,000 --> 00:07:32,430
 "Oh, I'm just thinking. I'm just feeling things. It doesn't

121
00:07:32,430 --> 00:07:35,000
 really matter."

122
00:07:35,000 --> 00:07:37,970
 "Oh, it matters because you want to gravitate towards the

123
00:07:37,970 --> 00:07:39,000
 peacefulness."

124
00:07:39,000 --> 00:07:44,590
 But you do so in a way that you just naturally gravitate

125
00:07:44,590 --> 00:07:47,000
 towards peacefulness

126
00:07:47,000 --> 00:07:54,130
 as you don't become involved in the thoughts and emotions,

127
00:07:54,130 --> 00:08:01,000
 especially the contents of them and the details of them.

128
00:08:01,000 --> 00:08:10,380
 And another thing that I learned here was that before I

129
00:08:10,380 --> 00:08:12,000
 arrived here,

130
00:08:12,000 --> 00:08:15,460
 I was thinking to myself, "Well, I'm going on a spiritual

131
00:08:15,460 --> 00:08:19,000
 journey to achieve happiness, to get it."

132
00:08:19,000 --> 00:08:22,000
 I'm so proud of myself for wanting to do so.

133
00:08:22,000 --> 00:08:26,180
 And I came here, and that's one of the biggest hindrances

134
00:08:26,180 --> 00:08:29,000
 that blocked my path for getting,

135
00:08:29,000 --> 00:08:32,200
 or not getting, but just simply being with happiness, being

136
00:08:32,200 --> 00:08:33,000
 with peacefulness.

137
00:08:33,000 --> 00:08:37,930
 It's that I wanted to get it. I wanted to obtain it. It's

138
00:08:37,930 --> 00:08:41,000
 like an item.

139
00:08:41,000 --> 00:08:44,920
 It's like I have my education, I have my family, I have my

140
00:08:44,920 --> 00:08:48,000
 happiness. That's how I viewed it.

141
00:08:48,000 --> 00:08:53,500
 And it doesn't work out that way. It never works out that

142
00:08:53,500 --> 00:08:54,000
 way.

143
00:08:54,000 --> 00:09:00,750
 And happiness is not... I don't think... Once you get on

144
00:09:00,750 --> 00:09:02,000
 the pursuit of happiness,

145
00:09:02,000 --> 00:09:07,000
 happiness is not in your reach anymore.

146
00:09:07,000 --> 00:09:13,950
 I think it's once you start to begin to be open to your

147
00:09:13,950 --> 00:09:15,000
 suffering,

148
00:09:15,000 --> 00:09:19,870
 when you become open to your negativity, that you're not

149
00:09:19,870 --> 00:09:21,000
 afraid anymore.

150
00:09:21,000 --> 00:09:25,000
 We're not trying to avoid these kinds of situations,

151
00:09:25,000 --> 00:09:30,000
 and only try to get these ones, like happiness.

152
00:09:30,000 --> 00:09:36,000
 It's when you embrace all sorts of experiences,

153
00:09:36,000 --> 00:09:40,690
 when you realize that all experiences are just basically

154
00:09:40,690 --> 00:09:44,000
 experiences, they're basically phenomena.

155
00:09:44,000 --> 00:09:51,000
 That's when you experience a sense of peacefulness.

156
00:09:51,000 --> 00:09:56,460
 Because when I was trying to meditate, I was always telling

157
00:09:56,460 --> 00:09:57,000
 myself,

158
00:09:57,000 --> 00:10:05,000
 you have to get to a state, a specific meditative state.

159
00:10:05,000 --> 00:10:08,000
 You have to do that. Then you can experience happiness.

160
00:10:08,000 --> 00:10:11,150
 Because then, if you don't get into meditative state, you

161
00:10:11,150 --> 00:10:12,000
 don't get wisdom.

162
00:10:12,000 --> 00:10:14,630
 If you don't get wisdom, you don't know what's really going

163
00:10:14,630 --> 00:10:15,000
 on.

164
00:10:15,000 --> 00:10:17,380
 You don't realize things that you came here, you wasted

165
00:10:17,380 --> 00:10:18,000
 your time.

166
00:10:18,000 --> 00:10:22,150
 And that was an obstacle. That's why I couldn't do it for

167
00:10:22,150 --> 00:10:23,000
 so long.

168
00:10:23,000 --> 00:10:27,910
 And it's when I realized that I can't control everything

169
00:10:27,910 --> 00:10:29,000
 anymore.

170
00:10:29,000 --> 00:10:33,000
 I can't dominate. I just have to be.

171
00:10:33,000 --> 00:10:39,780
 That you start to realize that peacefulness just arises

172
00:10:39,780 --> 00:10:41,000
 naturally.

173
00:10:41,000 --> 00:10:44,450
 That you're okay. Even if you feel anxiety, even if you

174
00:10:44,450 --> 00:10:46,000
 feel anger, sadness,

175
00:10:46,000 --> 00:10:49,000
 there's a sense of peacefulness in terms of...

176
00:10:49,000 --> 00:10:53,000
 I don't know how to describe it, but it's like you're okay.

177
00:10:53,000 --> 00:10:56,000
 And you're not...

178
00:10:56,000 --> 00:11:00,000
 It's also like a form of detachment from your emotions.

179
00:11:00,000 --> 00:11:06,000
 You just feel it, but you're not actually there to be

180
00:11:06,000 --> 00:11:10,000
 resistant of it.

181
00:11:10,000 --> 00:11:16,000
 You accept it. It's like you accept a friend who's sad.

182
00:11:16,000 --> 00:11:20,360
 You don't try to push her away. You just accept another

183
00:11:20,360 --> 00:11:21,000
 person.

184
00:11:21,000 --> 00:11:25,400
 And I realized this before, it's always easier to accept

185
00:11:25,400 --> 00:11:28,000
 someone else than to accept yourself.

186
00:11:28,000 --> 00:11:32,330
 So, when you become mindful, you view your emotions as

187
00:11:32,330 --> 00:11:35,000
 something that's not part of yourself.

188
00:11:35,000 --> 00:11:38,000
 And I think that's why it's so easy to accept them.

189
00:11:38,000 --> 00:11:41,510
 It's because once they're not part of yourself, you can

190
00:11:41,510 --> 00:11:43,000
 just see them for who they are.

191
00:11:43,000 --> 00:11:46,790
 You can give compassion to them, embrace them, be open to

192
00:11:46,790 --> 00:11:48,000
 them, whatever.

193
00:11:48,000 --> 00:11:54,280
 But it's much more easier to just let go of that personal

194
00:11:54,280 --> 00:11:55,000
 connection,

195
00:11:55,000 --> 00:11:57,000
 but just to realize that they're just natural.

196
00:11:57,000 --> 00:12:00,000
 I mean, we're all human beings. We experience emotions.

197
00:12:00,000 --> 00:12:03,000
 You can't be peaceful all the time.

198
00:12:03,000 --> 00:12:09,000
 And once you accept that, you can be peaceful.

199
00:12:09,000 --> 00:12:14,000
 I think that's the irony behind that.

200
00:12:14,000 --> 00:12:19,100
 But it took me a lot of patience, which is another thing

201
00:12:19,100 --> 00:12:20,000
 that I learned.

202
00:12:20,000 --> 00:12:23,000
 A lot of patience.

203
00:12:23,000 --> 00:12:25,930
 In the first few days, Yu Tai Damo told me that patience is

204
00:12:25,930 --> 00:12:27,000
 your best friend.

205
00:12:27,000 --> 00:12:30,000
 Apparently, I was too impatient to listen to his advice.

206
00:12:30,000 --> 00:12:33,500
 But over time, there's nothing you can do. You have to be

207
00:12:33,500 --> 00:12:34,000
 patient.

208
00:12:34,000 --> 00:12:37,810
 And to be honest, the conditions here are not the best

209
00:12:37,810 --> 00:12:39,000
 conditions.

210
00:12:39,000 --> 00:12:45,000
 I was living in a cave, lots of mosquitoes.

211
00:12:45,000 --> 00:12:49,260
 Sounds like I'm whining, which I am, but because I was

212
00:12:49,260 --> 00:12:54,000
 brought up in modern, more developed countries,

213
00:12:54,000 --> 00:12:56,000
 so I was not used to it.

214
00:12:56,000 --> 00:12:59,540
 But it's because of those annoyances that you start to

215
00:12:59,540 --> 00:13:01,000
 adapt to situations

216
00:13:01,000 --> 00:13:05,950
 that you force yourself to be more patient. Otherwise, you

217
00:13:05,950 --> 00:13:07,000
 can't survive.

218
00:13:07,000 --> 00:13:14,900
 I think naturally, your mind just tries to be more peaceful

219
00:13:14,900 --> 00:13:16,000
.

220
00:13:16,000 --> 00:13:22,990
 And in whatever situation, because that's the way that you

221
00:13:22,990 --> 00:13:25,000
 have to survive.

222
00:13:25,000 --> 00:13:30,000
 And so as a result, you just learn to be okay.

223
00:13:30,000 --> 00:13:33,910
 You learn not to slap yourself in the face over and over

224
00:13:33,910 --> 00:13:35,000
 and over again.

225
00:13:35,000 --> 00:13:38,000
 Because you can see it clearly.

226
00:13:38,000 --> 00:13:41,430
 And you didn't realize that before, because you never had

227
00:13:41,430 --> 00:13:43,000
 the time to observe it.

228
00:13:43,000 --> 00:13:46,820
 But now you have the time to observe it, to actually sit

229
00:13:46,820 --> 00:13:49,000
 down and train your mind,

230
00:13:49,000 --> 00:13:52,030
 sharpen your mind, and let you make your mind see things

231
00:13:52,030 --> 00:13:53,000
 more clearly.

232
00:13:53,000 --> 00:13:58,000
 You start to see links between thoughts and emotions,

233
00:13:58,000 --> 00:14:02,000
 and between things that's going on internally.

234
00:14:02,000 --> 00:14:05,000
 And you can see so clearly that when something happens,

235
00:14:05,000 --> 00:14:08,150
 and when you think about something, it's literally like you

236
00:14:08,150 --> 00:14:10,000
're slapping yourself in the face.

237
00:14:10,000 --> 00:14:17,000
 And in times outside of meditation, you do something bad,

238
00:14:17,000 --> 00:14:19,720
 like you trip on the ground, you literally slap yourself in

239
00:14:19,720 --> 00:14:20,000
 the face.

240
00:14:20,000 --> 00:14:23,000
 Nobody would do that. Why would you do that?

241
00:14:23,000 --> 00:14:27,000
 You might get angry, but people don't know that being angry

242
00:14:27,000 --> 00:14:29,000
 is like hurting yourself.

243
00:14:29,000 --> 00:14:32,730
 It's hurting your own mind. It's like hurting yourself

244
00:14:32,730 --> 00:14:33,000
 physically.

245
00:14:33,000 --> 00:14:36,190
 But I think it's even worse, because hurting your mind, it

246
00:14:36,190 --> 00:14:38,000
's your well-being.

247
00:14:38,000 --> 00:14:41,000
 It's everything that is your existence.

248
00:14:41,000 --> 00:14:44,690
 Everything outside of your mind is just sort of an

249
00:14:44,690 --> 00:14:46,000
 environment.

250
00:14:46,000 --> 00:14:56,000
 And that's how I really start to be okay with my emotions,

251
00:14:56,000 --> 00:15:00,000
 is that you start to realize how to be kind to yourself.

252
00:15:00,000 --> 00:15:06,410
 You start to realize how to just accept things, accept

253
00:15:06,410 --> 00:15:07,000
 yourself.

254
00:15:07,000 --> 00:15:12,530
 Because before coming here, I realized I was very critical

255
00:15:12,530 --> 00:15:15,000
 of my thoughts, very judgmental.

256
00:15:15,000 --> 00:15:18,000
 I would constantly tell myself, "Oh, you shouldn't do this.

257
00:15:18,000 --> 00:15:20,000
 You shouldn't think this. You shouldn't feel this.

258
00:15:20,000 --> 00:15:26,290
 This isn't good. You should feel ashamed. You should feel

259
00:15:26,290 --> 00:15:27,000
 anxious.

260
00:15:27,000 --> 00:15:30,080
 You need to change it. You need to make a drastic change.

261
00:15:30,080 --> 00:15:32,000
 Otherwise, it's bad for you.

262
00:15:32,000 --> 00:15:37,360
 Otherwise, you will lose career opportunities. You will

263
00:15:37,360 --> 00:15:39,000
 lose relationship opportunities.

264
00:15:39,000 --> 00:15:43,960
 You will lose things that society would grant you if your

265
00:15:43,960 --> 00:15:47,000
 personality isn't like this."

266
00:15:47,000 --> 00:15:51,460
 And so that trained me to think in a way that I have to be

267
00:15:51,460 --> 00:15:53,000
 a certain person.

268
00:15:53,000 --> 00:15:55,000
 So I would not accept parts of myself.

269
00:15:55,000 --> 00:16:00,370
 But once you come here, you start to learn that that's how

270
00:16:00,370 --> 00:16:03,000
 dangerous that is to you.

271
00:16:03,000 --> 00:16:07,000
 That you're really unhappy living like that.

272
00:16:07,000 --> 00:16:14,000
 It's really unhappy. It's an imprisonment.

273
00:16:14,000 --> 00:16:19,000
 You're a prisoner to society.

274
00:16:19,000 --> 00:16:22,000
 Society tells you that you need to feel like this.

275
00:16:22,000 --> 00:16:26,000
 And it's basically telling you, "Don't accept yourself."

276
00:16:26,000 --> 00:16:30,000
 You have to be like this. Otherwise, you can't be happy.

277
00:16:30,000 --> 00:16:36,920
 In order to be happy, you have to not accept yourself,

278
00:16:36,920 --> 00:16:38,000
 which is contradictive.

279
00:16:38,000 --> 00:16:44,690
 And I came here and I realized whether or not I think about

280
00:16:44,690 --> 00:16:45,000
 bad things,

281
00:16:45,000 --> 00:16:49,000
 whether or not I feel bad emotions or positive even.

282
00:16:49,000 --> 00:16:55,000
 They just come and they go. They never last forever.

283
00:16:55,000 --> 00:16:59,050
 Sometimes they will feel extreme just anger or extreme

284
00:16:59,050 --> 00:17:01,000
 depression or isolation or loneliness.

285
00:17:01,000 --> 00:17:06,460
 And at that very moment, I will always think that they will

286
00:17:06,460 --> 00:17:08,000
 last forever.

287
00:17:08,000 --> 00:17:11,250
 I will always think that. I will always think, "This is the

288
00:17:11,250 --> 00:17:14,000
 worst emotion. I wish it would just go away."

289
00:17:14,000 --> 00:17:19,560
 I will believe that it doesn't go away. But eventually, it

290
00:17:19,560 --> 00:17:21,000
 always does.

291
00:17:21,000 --> 00:17:25,500
 And I think that's the best opportunity. When you do

292
00:17:25,500 --> 00:17:27,000
 nothing, you realize that it does go away.

293
00:17:27,000 --> 00:17:31,340
 When you do something, you don't realize that. Your

294
00:17:31,340 --> 00:17:33,000
 attention is something else.

295
00:17:33,000 --> 00:17:36,990
 And you're always kept in this illusion that when you have

296
00:17:36,990 --> 00:17:40,000
 bad emotions, it's like a red alert.

297
00:17:40,000 --> 00:17:43,760
 You have to do something. Otherwise, it will just keep

298
00:17:43,760 --> 00:17:46,000
 coming in and destroy everything.

299
00:17:46,000 --> 00:17:52,340
 But with patience and with mindfulness, with the

300
00:17:52,340 --> 00:17:56,000
 opportunity to have no distractions

301
00:17:56,000 --> 00:18:01,090
 but to fully introspect and look at yourself and confront

302
00:18:01,090 --> 00:18:02,000
 yourself,

303
00:18:02,000 --> 00:18:07,170
 you realize that thoughts and emotions, they come and go,

304
00:18:07,170 --> 00:18:10,000
 which is impermanence.

305
00:18:10,000 --> 00:18:18,690
 And sometimes I will feel extremely happy. Sometimes I feel

306
00:18:18,690 --> 00:18:22,000
 like, "This is it. I found my happiness. I'm ready to go.

307
00:18:22,000 --> 00:18:25,000
 I don't need to be here. I know all the answers."

308
00:18:25,000 --> 00:18:28,760
 And the next day, I will feel extremely, just reach the

309
00:18:28,760 --> 00:18:31,000
 other end of the emotional scale.

310
00:18:31,000 --> 00:18:34,950
 I feel just hopelessness, frustration, like everything I've

311
00:18:34,950 --> 00:18:36,000
 learned was lost.

312
00:18:36,000 --> 00:18:39,440
 Sometimes I would think to myself, "This is too difficult

313
00:18:39,440 --> 00:18:43,000
 for me. I will never reach that stage of happiness.

314
00:18:43,000 --> 00:18:47,710
 I will just always be imprisoned by negativity and

315
00:18:47,710 --> 00:18:51,000
 suffering. This is ridiculous."

316
00:18:51,000 --> 00:18:56,710
 I would sleep in a cave with more than, I think, more than

317
00:18:56,710 --> 00:19:00,000
 100 mosquito bites by now

318
00:19:00,000 --> 00:19:05,410
 with the chance of actually getting some diseases and

319
00:19:05,410 --> 00:19:09,000
 sleeping less than eight hours,

320
00:19:09,000 --> 00:19:14,000
 which is actually, it actually isn't that bad by now.

321
00:19:14,000 --> 00:19:18,000
 But at first, it was too much of a challenge for me.

322
00:19:18,000 --> 00:19:22,080
 But you realize whether if it's positive experiences or

323
00:19:22,080 --> 00:19:25,000
 negative experiences, nothing stays forever.

324
00:19:25,000 --> 00:19:30,480
 So why bother trying to do those things that make you feel

325
00:19:30,480 --> 00:19:33,630
 like you've reached a certain stage where you're just fully

326
00:19:33,630 --> 00:19:34,000
 content?

327
00:19:34,000 --> 00:19:38,500
 Because that stage, I don't think that stage ever exists in

328
00:19:38,500 --> 00:19:40,000
 society anyway.

329
00:19:40,000 --> 00:19:44,130
 Society tells you that you do these things, and once you

330
00:19:44,130 --> 00:19:46,000
 reach them, there's this eternal happiness,

331
00:19:46,000 --> 00:19:49,310
 and you just reach it, and then you're just content. That's

332
00:19:49,310 --> 00:19:50,000
 your ticket.

333
00:19:50,000 --> 00:19:53,420
 But that never happens. You do all those things, but

334
00:19:53,420 --> 00:19:56,000
 internally you don't know the truth.

335
00:19:56,000 --> 00:20:02,360
 You still feel this sort of disharmony and this gap and

336
00:20:02,360 --> 00:20:05,000
 this sense of lack.

337
00:20:05,000 --> 00:20:13,290
 And it's when you realize that certain emotions and

338
00:20:13,290 --> 00:20:19,000
 thoughts, they're just not satisfying in a sense.

339
00:20:19,000 --> 00:20:25,000
 Yeah, that's the thing. They're not satisfying.

340
00:20:25,000 --> 00:20:29,470
 So once you stay here for a while, you let go of some

341
00:20:29,470 --> 00:20:30,000
 things,

342
00:20:30,000 --> 00:20:35,090
 because you realize the things that you're clinging onto

343
00:20:35,090 --> 00:20:36,000
 back home.

344
00:20:36,000 --> 00:20:39,920
 I mean, sure, they're good, but they're also using up a lot

345
00:20:39,920 --> 00:20:44,000
 of your attention and energy that are not necessary,

346
00:20:44,000 --> 00:20:48,390
 that are not ultimately conducive to your happiness. They

347
00:20:48,390 --> 00:20:51,000
 don't add any more happiness.

348
00:20:51,000 --> 00:20:55,100
 They don't really make it eternal. In fact, they actually

349
00:20:55,100 --> 00:20:56,000
 add more addiction,

350
00:20:56,000 --> 00:20:59,040
 because the more you experience them, the more you want it,

351
00:20:59,040 --> 00:21:00,000
 because you get used to,

352
00:21:00,000 --> 00:21:03,950
 you know, some of the things that I would do at home or

353
00:21:03,950 --> 00:21:06,000
 back in the West, I would do them,

354
00:21:06,000 --> 00:21:10,320
 and sure, they make me happy, but I would expect them to

355
00:21:10,320 --> 00:21:12,000
 make me happier the next time.

356
00:21:12,000 --> 00:21:15,740
 And it's just a constant cycle, and I would get tired of

357
00:21:15,740 --> 00:21:17,000
 them, and I'd move on.

358
00:21:17,000 --> 00:21:23,000
 And that's kind of like the tug of war of life.

359
00:21:23,000 --> 00:21:28,170
 You know, this happiness, you try to pull it towards you,

360
00:21:28,170 --> 00:21:30,000
 but then it keeps on going away,

361
00:21:30,000 --> 00:21:32,530
 you try to pull it towards you again, it keeps on going

362
00:21:32,530 --> 00:21:33,000
 away.

363
00:21:33,000 --> 00:21:37,500
 You never just sort of relax. You always want something for

364
00:21:37,500 --> 00:21:38,000
 yourself,

365
00:21:38,000 --> 00:21:41,000
 and you're resistant to something else that you don't want.

366
00:21:41,000 --> 00:21:44,900
 Once you come here, you just let go. You just tell yourself

367
00:21:44,900 --> 00:21:46,000
, "Stop playing the game."

368
00:21:46,000 --> 00:21:48,990
 You don't want to play the tug of war anymore, you just...

369
00:21:48,990 --> 00:21:51,000
 whatever.

370
00:21:51,000 --> 00:21:54,620
 And it's not a sense of carelessness or apathy, but it's a

371
00:21:54,620 --> 00:21:56,000
 sense of clarity.

372
00:21:56,000 --> 00:22:00,850
 You're really clear on the true nature of your emotions and

373
00:22:00,850 --> 00:22:02,000
 thoughts.

374
00:22:02,000 --> 00:22:08,790
 And you realize what's good for you, what's good for others

375
00:22:08,790 --> 00:22:09,000
,

376
00:22:09,000 --> 00:22:13,000
 what's bad for you and what's bad for others.

377
00:22:13,000 --> 00:22:16,000
 And that's it.

378
00:22:16,000 --> 00:22:23,000
 So those are the things that I've learned majorly.

379
00:22:23,000 --> 00:22:30,960
 And I really hope that I don't forget them when I go back

380
00:22:30,960 --> 00:22:33,000
 home, which I won't.

381
00:22:33,000 --> 00:22:36,530
 I don't think I will, because a lot of the things that I

382
00:22:36,530 --> 00:22:38,000
 learned,

383
00:22:38,000 --> 00:22:42,940
 it's too major for me to forget. It's too big for me to

384
00:22:42,940 --> 00:22:44,000
 forget.

385
00:22:44,000 --> 00:22:51,000
 I think it's already being internalized inside of me.

386
00:22:51,000 --> 00:22:57,000
 The fact that emotions come and go, thoughts come and go.

387
00:22:57,000 --> 00:23:02,000
 To actually have that experience, it's much more different

388
00:23:02,000 --> 00:23:05,000
 than to read about it in a book.

389
00:23:05,000 --> 00:23:10,350
 Because I knew about it. I knew everything that I know now

390
00:23:10,350 --> 00:23:12,000
 before I came here.

391
00:23:12,000 --> 00:23:16,700
 Everything I'm telling you now, I had the ability to tell

392
00:23:16,700 --> 00:23:20,000
 you back at home before anything happened.

393
00:23:20,000 --> 00:23:23,000
 Because I had read about it. I knew everything that...

394
00:23:23,000 --> 00:23:28,100
 I knew the basics of Buddhism. I knew the basic results

395
00:23:28,100 --> 00:23:31,000
 that you get from meditation.

396
00:23:31,000 --> 00:23:34,000
 I read a lot about philosophy, about society.

397
00:23:34,000 --> 00:23:39,000
 I knew a lot of things about how to live a good life.

398
00:23:39,000 --> 00:23:43,000
 But obviously it's different. Something is different here.

399
00:23:43,000 --> 00:23:45,000
 You get the actual experience.

400
00:23:45,000 --> 00:23:50,000
 To know something intellectually, you don't really know it.

401
00:23:50,000 --> 00:23:52,780
 You just have the information, but you don't internalize it

402
00:23:52,780 --> 00:23:53,000
.

403
00:23:53,000 --> 00:23:56,620
 You don't digest it completely. You can just regurgitate

404
00:23:56,620 --> 00:23:58,000
 just from memory.

405
00:23:58,000 --> 00:24:04,200
 But when you actually experience it, you... it's a sort of

406
00:24:04,200 --> 00:24:09,000
 merge, sort of fusion with that knowledge.

407
00:24:09,000 --> 00:24:11,000
 You embody that knowledge now.

408
00:24:11,000 --> 00:24:17,000
 You're the personification of what you've learned.

409
00:24:17,000 --> 00:24:23,150
 And so, yeah, I mean, I would really urge the people who

410
00:24:23,150 --> 00:24:28,000
 are really interested in Buddhism not to just read about it

411
00:24:28,000 --> 00:24:28,000
.

412
00:24:28,000 --> 00:24:30,710
 Because you can read a million books about Buddhism, know

413
00:24:30,710 --> 00:24:32,000
 every single little thing.

414
00:24:32,000 --> 00:24:34,310
 But I would guarantee you that the results that you get

415
00:24:34,310 --> 00:24:37,660
 from reading it would not be as good as someone who never

416
00:24:37,660 --> 00:24:38,000
 knew anything about Buddhism,

417
00:24:38,000 --> 00:24:42,630
 but just meditated for 20 days or even 10 days and had the

418
00:24:42,630 --> 00:24:45,000
 direct experience of what you read.

419
00:24:45,000 --> 00:24:48,000
 That's the most important thing I've learned.

420
00:24:48,000 --> 00:24:50,500
 Well, one of the most important things I've learned is that

421
00:24:50,500 --> 00:24:52,000
 you have to get direct training.

422
00:24:52,000 --> 00:24:56,000
 You have to get meditation.

423
00:24:56,000 --> 00:25:01,110
 And you have to have a teacher to guide you, which I was

424
00:25:01,110 --> 00:25:05,000
 fortunate enough to be with a very good one.

425
00:25:05,000 --> 00:25:11,000
 And, yeah, that's basically it.

426
00:25:11,000 --> 00:25:14,780
 That's basically the answer that I would give in terms of

427
00:25:14,780 --> 00:25:20,070
 what I've learned and the experiences and the things I've

428
00:25:20,070 --> 00:25:21,000
 gained here.

429
00:25:21,000 --> 00:25:26,000
 So, yeah, thank you for listening in, if you are.

