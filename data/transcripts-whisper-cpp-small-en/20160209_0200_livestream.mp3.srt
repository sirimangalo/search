1
00:00:00,000 --> 00:00:07,000
 Good evening everyone.

2
00:00:07,000 --> 00:00:35,000
 Just going to pull up the video.

3
00:00:35,000 --> 00:00:42,000
 Hang out is live. Good evening everyone.

4
00:00:42,000 --> 00:00:57,000
 We're broadcasting live sometime in February.

5
00:00:57,000 --> 00:01:02,610
 Interestingly, today we were just talking about filial

6
00:01:02,610 --> 00:01:04,000
 piety.

7
00:01:04,000 --> 00:01:12,120
 When Buddhism went to China, it was of course a big deal at

8
00:01:12,120 --> 00:01:14,000
 the time.

9
00:01:14,000 --> 00:01:19,050
 The other religions in China, Confucianism, you know, was

10
00:01:19,050 --> 00:01:21,000
 very much into filial piety.

11
00:01:21,000 --> 00:01:26,710
 And so they had actually adapted the teachings and said

12
00:01:26,710 --> 00:01:38,800
 things like, "Claim somehow that the Buddha believed filial

13
00:01:38,800 --> 00:01:41,000
 piety was the way."

14
00:01:41,000 --> 00:01:44,000
 So it really, really blew it out of proportion.

15
00:01:44,000 --> 00:01:48,910
 So we were talking about that and we addressed the question

16
00:01:48,910 --> 00:01:53,640
 of if filial piety is so important, how do they justify

17
00:01:53,640 --> 00:01:55,000
 becoming monks?

18
00:01:55,000 --> 00:02:01,240
 And there's actually a defense of it that filial piety, it

19
00:02:01,240 --> 00:02:06,920
 is respect for your parents to become a monk because you're

20
00:02:06,920 --> 00:02:10,000
 working to liberate.

21
00:02:10,000 --> 00:02:14,000
 You're working to attain the ultimate state.

22
00:02:14,000 --> 00:02:17,700
 Or even more, you're working to help others attain the

23
00:02:17,700 --> 00:02:26,000
 ultimate state, working to better the world.

24
00:02:26,000 --> 00:02:36,200
 Which in fact is an early teaching as well in the text that

25
00:02:36,200 --> 00:02:38,000
 we follow.

26
00:02:38,000 --> 00:02:41,980
 And there's the idea that becoming a monk or even

27
00:02:41,980 --> 00:02:48,320
 practicing meditation is actually a great benefit to your

28
00:02:48,320 --> 00:02:50,000
 whole family.

29
00:02:50,000 --> 00:02:53,280
 In the most obvious sense because it changes your

30
00:02:53,280 --> 00:02:57,610
 relationships with them as you become more peaceful, more

31
00:02:57,610 --> 00:03:02,000
 content, more compassionate and so on.

32
00:03:02,000 --> 00:03:13,000
 More loving.

33
00:03:13,000 --> 00:03:18,980
 So here we have a quote today about this. It's actually

34
00:03:18,980 --> 00:03:22,000
 quite a strong, strongly worded quote.

35
00:03:22,000 --> 00:03:25,810
 Some people might find it objectionable, especially those

36
00:03:25,810 --> 00:03:29,000
 who have bad relationships with their parents.

37
00:03:29,000 --> 00:03:33,000
 And I can understand that.

38
00:03:33,000 --> 00:03:42,000
 Again, the Buddha is most often not so much prescriptive or

39
00:03:42,000 --> 00:03:42,000
...

40
00:03:42,000 --> 00:03:44,820
 I don't know what the right word is, but it's not about

41
00:03:44,820 --> 00:03:50,000
 denouncing people who have familial problems.

42
00:03:50,000 --> 00:03:54,800
 But there is a sense that when parents are looked up to,

43
00:03:54,800 --> 00:04:00,030
 and it doesn't mean unworthy, whether they're worthy of it

44
00:04:00,030 --> 00:04:01,000
 or not.

45
00:04:01,000 --> 00:04:06,660
 Although there is a sense of it being good order and good

46
00:04:06,660 --> 00:04:12,320
 proper etiquette and useful for the order of society to

47
00:04:12,320 --> 00:04:19,000
 respect your parents regardless of whether they deserve it.

48
00:04:19,000 --> 00:04:23,100
 But I think there's much room to say that part of the

49
00:04:23,100 --> 00:04:27,720
 greatness is when parents do deserve it, when parents do

50
00:04:27,720 --> 00:04:32,530
 act honorably and do shelter their children, care for their

51
00:04:32,530 --> 00:04:41,200
 children, teach their children, act as a good example for

52
00:04:41,200 --> 00:04:47,000
 their children.

53
00:04:47,000 --> 00:04:50,000
 But definitely children.

54
00:04:50,000 --> 00:05:00,380
 I've argued with people in the past about this idea of

55
00:05:00,380 --> 00:05:04,000
 whether we should feel grateful to our parents.

56
00:05:04,000 --> 00:05:13,210
 And their argument was that, "Well, my parents wanted to

57
00:05:13,210 --> 00:05:20,650
 have me. I didn't choose to be born." Right? It was their

58
00:05:20,650 --> 00:05:22,000
 choice.

59
00:05:22,000 --> 00:05:24,220
 Now Buddhism doesn't actually believe that. It's

60
00:05:24,220 --> 00:05:30,000
 interesting. It actually is our choice to be born.

61
00:05:30,000 --> 00:05:34,840
 It's a joint effort. Parents have to have sexual

62
00:05:34,840 --> 00:05:36,000
 intercourse.

63
00:05:36,000 --> 00:05:42,960
 And oftentimes are aware that it's going to lead to

64
00:05:42,960 --> 00:05:45,000
 pregnancy.

65
00:05:45,000 --> 00:05:55,350
 So it is partly their fault. But without us being keen to

66
00:05:55,350 --> 00:06:03,000
 be reborn, there wouldn't be rebirth.

67
00:06:03,000 --> 00:06:14,110
 But more importantly, it is quite besides the point who

68
00:06:14,110 --> 00:06:18,570
 made the effort. Like if someone does a nice thing for you

69
00:06:18,570 --> 00:06:19,380
 without you asking, does that mean you shouldn't feel

70
00:06:19,380 --> 00:06:20,000
 grateful if it helps you?

71
00:06:20,000 --> 00:06:24,840
 So if someone tries to do a good deed and you don't need

72
00:06:24,840 --> 00:06:28,000
 their help, that's one thing.

73
00:06:28,000 --> 00:06:31,410
 But if you say, "Well, my parents wanted to look after me.

74
00:06:31,410 --> 00:06:34,390
 They wanted to do all these good things." It's really

75
00:06:34,390 --> 00:06:35,000
 beside the point.

76
00:06:35,000 --> 00:06:38,780
 If I want to do something nice for you when you need it, it

77
00:06:38,780 --> 00:06:42,000
 doesn't mean you shouldn't feel grateful.

78
00:06:42,000 --> 00:06:47,270
 And this isn't a logic. It isn't about logic. It's very

79
00:06:47,270 --> 00:06:50,000
 much about karma.

80
00:06:50,000 --> 00:06:57,220
 When someone does good things for you, when they care for

81
00:06:57,220 --> 00:07:01,220
 you, this changes. It just changes the scales, tips the

82
00:07:01,220 --> 00:07:02,000
 balance.

83
00:07:02,000 --> 00:07:06,230
 It's funny. Nowadays, you often have people who don't like

84
00:07:06,230 --> 00:07:09,000
 it when others do good deeds for them.

85
00:07:09,000 --> 00:07:13,930
 And as I've talked about, this can be karmically based as

86
00:07:13,930 --> 00:07:19,000
 well because they haven't done good things for others.

87
00:07:19,000 --> 00:07:23,830
 But the fear that I've heard is there's a sense that if I

88
00:07:23,830 --> 00:07:27,000
 do something for you, then I'm going to owe you something.

89
00:07:27,000 --> 00:07:29,260
 I'm going to have to do something. If you do something for

90
00:07:29,260 --> 00:07:32,000
 me, I'm going to have to do something back for you.

91
00:07:32,000 --> 00:07:35,350
 I don't want me to do it because there's always strings

92
00:07:35,350 --> 00:07:43,000
 attached.

93
00:07:43,000 --> 00:07:47,230
 Which is really a shame. It's a shame that people don't

94
00:07:47,230 --> 00:07:52,720
 want to do good things for others. It's kind of what it

95
00:07:52,720 --> 00:07:55,000
 sounds like.

96
00:07:55,000 --> 00:08:01,620
 But part of being a good person is being able to accept

97
00:08:01,620 --> 00:08:07,000
 good deeds that other people do for you.

98
00:08:07,000 --> 00:08:13,060
 So allowing other people to help you. Why I think I can say

99
00:08:13,060 --> 00:08:16,000
 that is because it ties in with karma.

100
00:08:16,000 --> 00:08:18,980
 If you've done good deeds to others, the only way you can

101
00:08:18,980 --> 00:08:22,240
 be comfortable, to put it this way, with other people doing

102
00:08:22,240 --> 00:08:23,000
 good deeds for you,

103
00:08:23,000 --> 00:08:25,460
 is if you've done good deeds for others or if you're a good

104
00:08:25,460 --> 00:08:26,000
 person.

105
00:08:26,000 --> 00:08:29,790
 A person who's not of a wholesome mind will have a lot of

106
00:08:29,790 --> 00:08:34,000
 trouble accepting good deeds from others, which is fine.

107
00:08:34,000 --> 00:08:38,000
 I mean, it's not fine, but it's understandable.

108
00:08:38,000 --> 00:08:40,500
 Most of us are going to be like that. We're going to be

109
00:08:40,500 --> 00:08:46,990
 uncomfortable because if you haven't engaged in actively

110
00:08:46,990 --> 00:08:52,000
 seeking out and dedicating yourself to a life of good deeds

111
00:08:52,000 --> 00:08:54,000
,

112
00:08:54,000 --> 00:08:56,590
 it's hard to accept gifts from others. You'll find it vis

113
00:08:56,590 --> 00:08:57,000
cerally.

114
00:08:57,000 --> 00:09:00,020
 I mean, again, it's not logic. It's not reason. It's karma.

115
00:09:00,020 --> 00:09:03,000
 It's the world.

116
00:09:03,000 --> 00:09:07,000
 So when people do things for you, it does tip the scales.

117
00:09:07,000 --> 00:09:13,090
 And it is something that ordinary people will have rightly

118
00:09:13,090 --> 00:09:15,000
 should...

119
00:09:15,000 --> 00:09:19,270
 Not fear, but it's something stirring. Like, yeah, you will

120
00:09:19,270 --> 00:09:20,000
 owe them.

121
00:09:20,000 --> 00:09:23,710
 Not because they think you'll owe them, but it changes this

122
00:09:23,710 --> 00:09:26,000
. It tips the scales.

123
00:09:26,000 --> 00:09:30,090
 I think it's fair to say that. I think it's fair that the

124
00:09:30,090 --> 00:09:34,060
 only rational way to deal with that is to go crazy doing

125
00:09:34,060 --> 00:09:35,000
 good deeds.

126
00:09:35,000 --> 00:09:43,960
 If someone helps you feeling grateful for sure, and oft

127
00:09:43,960 --> 00:09:48,000
entimes working to settle the score.

128
00:09:48,000 --> 00:09:50,320
 I mean, it does sound kind of cold and calculated, but I

129
00:09:50,320 --> 00:09:52,000
 don't think it need be.

130
00:09:52,000 --> 00:09:54,790
 I don't think it should be because if it's just to settle

131
00:09:54,790 --> 00:09:57,000
 the score, it's not really a good deed. Right?

132
00:09:57,000 --> 00:10:01,270
 I guess that's the thing. I do something good for you to

133
00:10:01,270 --> 00:10:04,000
 you out of the goodness of my heart.

134
00:10:04,000 --> 00:10:08,000
 And you say, oh, I don't want to owe this person something.

135
00:10:08,000 --> 00:10:10,000
 So I should pay them back.

136
00:10:10,000 --> 00:10:12,160
 You're not doing it out of the goodness of your heart. So

137
00:10:12,160 --> 00:10:15,000
 you still haven't managed to even the scales.

138
00:10:15,000 --> 00:10:18,590
 It has to be because you want to. It has to be because you

139
00:10:18,590 --> 00:10:21,000
 out of love, out of compassion.

140
00:10:21,000 --> 00:10:26,000
 It's quite scary, actually. I think about it.

141
00:10:26,000 --> 00:10:30,340
 It could be quite scary if you're not prepared to do good

142
00:10:30,340 --> 00:10:32,000
 deeds for others.

143
00:10:32,000 --> 00:10:37,210
 It's kind of funny. Ordinary people should be afraid of

144
00:10:37,210 --> 00:10:39,000
 good people because they're good when they do good deeds.

145
00:10:39,000 --> 00:10:41,910
 No, it's certainly not like that because it's not something

146
00:10:41,910 --> 00:10:44,000
 that you actually have to think about.

147
00:10:44,000 --> 00:10:49,000
 It just means you'll have a relationship with that person.

148
00:10:49,000 --> 00:10:51,000
 And if you're not a very good person,

149
00:10:51,000 --> 00:10:56,080
 you're going to end up in a position where the best thing

150
00:10:56,080 --> 00:10:58,000
 you can do is to help that person.

151
00:10:58,000 --> 00:11:01,440
 I mean, just karmically talking. So why I'm talking about

152
00:11:01,440 --> 00:11:03,000
 this is in regards to our parents,

153
00:11:03,000 --> 00:11:06,290
 because it really is that way. And you find that when you

154
00:11:06,290 --> 00:11:07,000
 meditate.

155
00:11:07,000 --> 00:11:09,310
 Meditators often miss their parents if they have a good

156
00:11:09,310 --> 00:11:11,000
 relationship with their parents.

157
00:11:11,000 --> 00:11:14,180
 If they have a bad relationship with their parents, it's

158
00:11:14,180 --> 00:11:15,000
 going to hit them hard.

159
00:11:15,000 --> 00:11:18,000
 If their parents have been abusive.

160
00:11:18,000 --> 00:11:22,610
 And I'm not saying that they will eventually learn to

161
00:11:22,610 --> 00:11:25,000
 respect their parents.

162
00:11:25,000 --> 00:11:30,290
 I mean, if someone's abusive, that's not something to brush

163
00:11:30,290 --> 00:11:32,000
 off or to ignore.

164
00:11:32,000 --> 00:11:36,260
 But I would still say that will hit them hardest because if

165
00:11:36,260 --> 00:11:40,000
 a stranger is abusive towards you,

166
00:11:40,000 --> 00:11:45,000
 that's one thing. But your parents, right? It's so much

167
00:11:45,000 --> 00:11:53,000
 more deeply embedded in the heart.

168
00:11:53,000 --> 00:11:58,370
 So in all ways, it's one of the parents or one of the

169
00:11:58,370 --> 00:12:03,000
 hardest things to deal with in meditation.

170
00:12:03,000 --> 00:12:06,000
 And as a result, it's good to make sure that before you

171
00:12:06,000 --> 00:12:07,000
 come to meditate,

172
00:12:07,000 --> 00:12:09,000
 you have a good relationship with your parents.

173
00:12:09,000 --> 00:12:11,350
 If not, then expect to have to deal with it during your

174
00:12:11,350 --> 00:12:13,000
 meditation and deal with it.

175
00:12:13,000 --> 00:12:16,360
 I didn't have a very good relationship with my parents when

176
00:12:16,360 --> 00:12:18,000
 I started meditating.

177
00:12:18,000 --> 00:12:21,770
 And as a result, I remember walking out of my... I remember

178
00:12:21,770 --> 00:12:23,000
 I started crying.

179
00:12:23,000 --> 00:12:27,300
 And someone found out and they called the teacher and one

180
00:12:27,300 --> 00:12:29,000
 of the teachers came to my room

181
00:12:29,000 --> 00:12:31,890
 and knocked on the door. And I opened the door and she said

182
00:12:31,890 --> 00:12:32,000
, "What's wrong?"

183
00:12:32,000 --> 00:12:37,030
 I said, "I miss my parents." And she goes, "Oh, very good,

184
00:12:37,030 --> 00:12:38,000
 very good."

185
00:12:38,000 --> 00:12:43,000
 That was a sign of... She said, "That's right view."

186
00:12:43,000 --> 00:12:46,230
 I mean, it is. It's something coming up from your

187
00:12:46,230 --> 00:12:48,000
 meditation naturally.

188
00:12:48,000 --> 00:12:50,690
 I hadn't thought about my parents. I had no thought that

189
00:12:50,690 --> 00:12:53,000
 this was going to...

190
00:12:53,000 --> 00:12:57,000
 They were going to play a part in my meditation at all.

191
00:12:57,000 --> 00:13:02,060
 But I had done a fairly acrimonious relationship with my

192
00:13:02,060 --> 00:13:04,000
 parents at times.

193
00:13:04,000 --> 00:13:08,000
 A lot of people talk about that.

194
00:13:08,000 --> 00:13:12,840
 So I think that gives some insight into why this quote is

195
00:13:12,840 --> 00:13:15,000
 saying what it's saying.

196
00:13:15,000 --> 00:13:20,620
 How when parents are respected in the house, that is what

197
00:13:20,620 --> 00:13:27,000
 it means to worship the gods, to worship Brahma.

198
00:13:27,000 --> 00:13:34,320
 Worshiping your parents is worshiping Brahma. And it's kind

199
00:13:34,320 --> 00:13:38,000
 of a dig at Hinduism as well.

200
00:13:38,000 --> 00:13:43,000
 Like the language or Brahmanism, the religion of the time.

201
00:13:43,000 --> 00:13:46,380
 Because they worshiped... They were talking about God and

202
00:13:46,380 --> 00:13:47,000
 the yada yada.

203
00:13:47,000 --> 00:13:50,660
 So the Buddha is saying, "Real God, people who are really

204
00:13:50,660 --> 00:13:54,000
 gods because they do create you."

205
00:13:54,000 --> 00:13:57,720
 In a sense, they create your body and they do answer your

206
00:13:57,720 --> 00:14:00,000
 prayers, answer your wishes.

207
00:14:00,000 --> 00:14:04,230
 This is if they do again. But for many of us, they do. And

208
00:14:04,230 --> 00:14:06,000
 they have for many years.

209
00:14:06,000 --> 00:14:09,770
 That's God. That's what God is supposed to do. They're

210
00:14:09,770 --> 00:14:11,000
 powerful.

211
00:14:11,000 --> 00:14:16,000
 They have complete power over you when you're a child.

212
00:14:16,000 --> 00:14:22,000
 And they exercise benevolence if they do as many do.

213
00:14:22,000 --> 00:14:25,000
 They care for you. Many of them do.

214
00:14:25,000 --> 00:14:31,570
 So if that's the case, if they do care for you and they are

215
00:14:31,570 --> 00:14:33,000
 benevolent and so on,

216
00:14:33,000 --> 00:14:36,400
 then that's like Brahma and they should be worshipped. And

217
00:14:36,400 --> 00:14:39,000
 if they are worshipped...

218
00:14:39,000 --> 00:14:42,950
 Maybe not as gods, but there is a sense that your parents

219
00:14:42,950 --> 00:14:45,000
 are holy for that reason.

220
00:14:45,000 --> 00:14:47,000
 Isn't that funny, no?

221
00:14:47,000 --> 00:14:49,760
 Parents are more like God than God is because of course any

222
00:14:49,760 --> 00:14:51,000
 gods that there might be,

223
00:14:51,000 --> 00:14:53,000
 they don't spend much time...

224
00:14:53,000 --> 00:14:56,190
 Obviously they don't spend much time worrying about humans,

225
00:14:56,190 --> 00:14:59,000
 no matter what the theists say.

226
00:14:59,000 --> 00:15:02,560
 Apparently you have as much... Someone was saying you have

227
00:15:02,560 --> 00:15:04,000
 as much, statistically speaking,

228
00:15:04,000 --> 00:15:09,980
 or they've done experiments on prayer, and you have as much

229
00:15:09,980 --> 00:15:11,000
...

230
00:15:11,000 --> 00:15:17,000
 I mean, it's kind of funny, I guess, but what is it?

231
00:15:17,000 --> 00:15:21,820
 You'd have as much success praying to a jug of milk as you

232
00:15:21,820 --> 00:15:24,000
 would praying to God.

233
00:15:24,000 --> 00:15:28,000
 That was the quote, I think.

234
00:15:28,000 --> 00:15:34,860
 But to your parents, in cases, there are parents who love

235
00:15:34,860 --> 00:15:38,000
 their children so much that they do pretty much,

236
00:15:38,000 --> 00:15:41,290
 well, they do many things for them, whatever's in their

237
00:15:41,290 --> 00:15:46,000
 power to make them happy, that kind of thing.

238
00:15:46,000 --> 00:15:50,980
 So again, not exactly a deep teaching, but I think what I

239
00:15:50,980 --> 00:15:55,000
've touched upon shows that it's interesting for meditators.

240
00:15:55,000 --> 00:15:58,700
 It is a part of the meditation that our relationships with

241
00:15:58,700 --> 00:16:00,000
 people will come up.

242
00:16:00,000 --> 00:16:02,860
 You have to deal with them, and a good way to deal with

243
00:16:02,860 --> 00:16:05,000
 them is send love, ask forgiveness,

244
00:16:05,000 --> 00:16:08,080
 just as a mental note after you meditate, even during your

245
00:16:08,080 --> 00:16:09,000
 meditation.

246
00:16:09,000 --> 00:16:13,190
 Obviously not to make it our core meditation, we're focused

247
00:16:13,190 --> 00:16:14,000
 on insight,

248
00:16:14,000 --> 00:16:18,010
 but insight will evoke these feelings and dig up old

249
00:16:18,010 --> 00:16:21,000
 emotions, that kind of thing,

250
00:16:21,000 --> 00:16:25,000
 old bad things we've done, things that have been done to us

251
00:16:25,000 --> 00:16:29,000
, that we still carry around as a grudge.

252
00:16:29,000 --> 00:16:31,000
 And it's important we work them out.

253
00:16:31,000 --> 00:16:34,620
 It doesn't mean we have to pretend that our parents are

254
00:16:34,620 --> 00:16:40,000
 gods if they weren't godly or holy or worthy.

255
00:16:40,000 --> 00:16:43,320
 But it doesn't mean we should work out, especially since we

256
00:16:43,320 --> 00:16:49,180
 have a very deep and long-standing relationship with our

257
00:16:49,180 --> 00:16:50,000
 parents.

258
00:16:50,000 --> 00:16:55,670
 And work them out may just mean to let them go, and to move

259
00:16:55,670 --> 00:16:56,000
 on,

260
00:16:56,000 --> 00:16:59,200
 stop obsessing over the bad things they've done to us, if

261
00:16:59,200 --> 00:17:02,000
 they've done bad things to us.

262
00:17:02,000 --> 00:17:06,000
 So, there you are, that's the quote for this evening.

263
00:17:06,000 --> 00:17:09,980
 I've got four people joining me in the hangout, welcome

264
00:17:09,980 --> 00:17:11,000
 everyone.

265
00:17:11,000 --> 00:17:14,000
 Well that's all for the dhamma for tonight.

266
00:17:14,000 --> 00:17:17,130
 So you're welcome to go home, if there's any questions I'm

267
00:17:17,130 --> 00:17:18,000
 happy to answer them.

268
00:17:18,000 --> 00:17:20,250
 Only on the hangout though, so that means you've got to

269
00:17:20,250 --> 00:17:23,000
 join the hangout and ask me live.

270
00:17:23,000 --> 00:17:27,000
 That's the new rules. You guys can go.

271
00:17:27,000 --> 00:17:31,000
 I've got three people sitting here as my audience.

272
00:17:31,000 --> 00:17:37,000
 Record numbers, record turnout.

273
00:17:37,000 --> 00:17:40,580
 Oh I don't think I have audio, just a second. My computer's

274
00:17:40,580 --> 00:17:42,000
 doing something really weird.

275
00:17:42,000 --> 00:17:44,000
 Just a second.

276
00:17:44,000 --> 00:17:49,000
 Every time I load up I have to turn on the volume.

277
00:17:49,000 --> 00:17:52,000
 It's okay.

278
00:17:52,000 --> 00:17:57,000
 Now I should be able to hear you.

279
00:17:57,000 --> 00:18:01,950
 I should be able to hear you, you should be able to hear

280
00:18:01,950 --> 00:18:05,000
 each other on the live stream.

281
00:18:05,000 --> 00:18:09,000
 Testing, I don't have a question about that.

282
00:18:09,000 --> 00:18:11,000
 I have a question about that.

283
00:18:11,000 --> 00:18:13,000
 Alright.

284
00:18:13,000 --> 00:18:16,000
 Let he or she who has a question speak.

285
00:18:16,000 --> 00:18:21,070
 Okay, so it's kind of, it's not really a complicated

286
00:18:21,070 --> 00:18:22,000
 question.

287
00:18:22,000 --> 00:18:27,760
 I was thinking about those people today who are prolonging

288
00:18:27,760 --> 00:18:29,000
 old schisms.

289
00:18:29,000 --> 00:18:35,000
 Would they as well go to the deepest hole? How is that?

290
00:18:35,000 --> 00:18:42,000
 Or are they just in their own minds in the bad direction?

291
00:18:42,000 --> 00:18:46,000
 The people who what? Who cause schisms?

292
00:18:46,000 --> 00:18:50,350
 Well, I mean, old schisms and old schisms and then they

293
00:18:50,350 --> 00:18:52,000
 hold to them for long.

294
00:18:52,000 --> 00:18:57,000
 So they're clinging to them, so keeping them around.

295
00:18:57,000 --> 00:19:01,000
 I see. Are you thinking specifically of Buddhism?

296
00:19:01,000 --> 00:19:02,000
 Yep.

297
00:19:02,000 --> 00:19:05,000
 Like Mahayana Theravada, all that stuff?

298
00:19:05,000 --> 00:19:07,000
 Yep.

299
00:19:07,000 --> 00:19:10,360
 Well, off the top of my head it doesn't sound like it

300
00:19:10,360 --> 00:19:14,000
 because they've been told this is Buddhism, right?

301
00:19:14,000 --> 00:19:16,000
 Yeah.

302
00:19:16,000 --> 00:19:20,330
 So if they were presented with inconvertible, true, incon

303
00:19:20,330 --> 00:19:24,000
vertible, incontrovertible,

304
00:19:24,000 --> 00:19:30,000
 there's a word that means non-controversial, unable to,

305
00:19:30,000 --> 00:19:33,000
 like absolute proof,

306
00:19:33,000 --> 00:19:37,090
 or maybe overwhelming at least, proof that this was not

307
00:19:37,090 --> 00:19:38,000
 Buddhism,

308
00:19:38,000 --> 00:19:44,000
 or this isn't what this is lies, say.

309
00:19:44,000 --> 00:19:47,460
 If they could, if they found something, oh, this is lies

310
00:19:47,460 --> 00:19:49,000
 and they still held to it,

311
00:19:49,000 --> 00:19:53,000
 then I'd say there's start to be a problem.

312
00:19:53,000 --> 00:19:57,190
 But it's quite different from a group being in harmony and

313
00:19:57,190 --> 00:19:59,000
 following the Buddhist teaching

314
00:19:59,000 --> 00:20:03,000
 and then breaking them up.

315
00:20:03,000 --> 00:20:10,000
 That's the big deal because that destroys Buddhism.

316
00:20:10,000 --> 00:20:14,000
 I see.

317
00:20:14,000 --> 00:20:20,620
 I mean, though there is work, and it is good work to try

318
00:20:20,620 --> 00:20:23,000
 and bring Buddhists of all kinds together

319
00:20:23,000 --> 00:20:26,820
 and find our commonalities, that seems like a good thing to

320
00:20:26,820 --> 00:20:27,000
 do

321
00:20:27,000 --> 00:20:33,000
 without accepting what we'd consider wrong view.

322
00:20:33,000 --> 00:20:46,410
 But I don't think it's schismatism to perpetuate what one

323
00:20:46,410 --> 00:20:50,000
 understands to be Buddhism as Buddhism.

324
00:20:50,000 --> 00:20:54,220
 It's unfortunate that some of what is not Buddhism gets

325
00:20:54,220 --> 00:20:55,000
 wrong.

326
00:20:55,000 --> 00:21:01,000
 We're learning a lot about in China, take this for example,

327
00:21:01,000 --> 00:21:04,000
 in China they put words in the Buddha's mouth.

328
00:21:04,000 --> 00:21:09,550
 It's pretty clear they just, because in order for their

329
00:21:09,550 --> 00:21:13,000
 views to gain legitimacy,

330
00:21:13,000 --> 00:21:17,370
 the only legitimate source of Buddhism was the Buddha for

331
00:21:17,370 --> 00:21:22,000
 many schools, for most of us.

332
00:21:22,000 --> 00:21:29,140
 And so they just wrote things, like clearly, as I said,

333
00:21:29,140 --> 00:21:31,000
 they put in the Buddha's mouth

334
00:21:31,000 --> 00:21:33,000
 right after he became enlightened, he was sitting there and

335
00:21:33,000 --> 00:21:33,000
 he said,

336
00:21:33,000 --> 00:21:36,000
 "Phileopiety is the way."

337
00:21:36,000 --> 00:21:39,000
 I'm like, "What?"

338
00:21:39,000 --> 00:21:43,000
 It's like, "Well, that's so Chinese, right?"

339
00:21:43,000 --> 00:21:45,000
 Definitely not what the Buddha said.

340
00:21:45,000 --> 00:21:49,280
 But pretty clearly, we don't actually have proof, but

341
00:21:49,280 --> 00:21:52,000
 pretty clearly, it came from China.

342
00:21:52,000 --> 00:21:54,000
 And that's bad.

343
00:21:54,000 --> 00:21:58,000
 I would say that, whoever did that, bad.

344
00:21:58,000 --> 00:22:03,800
 Now, you could argue that maybe they didn't really have a

345
00:22:03,800 --> 00:22:05,000
 sense of who the Buddha was,

346
00:22:05,000 --> 00:22:07,000
 or maybe they thought everyone did this.

347
00:22:07,000 --> 00:22:09,270
 Maybe they thought, "Well, the Buddha's just an imaginary

348
00:22:09,270 --> 00:22:10,000
 figure anyway.

349
00:22:10,000 --> 00:22:11,000
 He didn't really exist."

350
00:22:11,000 --> 00:22:13,000
 So people think that.

351
00:22:13,000 --> 00:22:15,000
 Then they would think, "Well, then it's all right.

352
00:22:15,000 --> 00:22:17,000
 I'll just make up my own stories.

353
00:22:17,000 --> 00:22:18,000
 They have their stories from India.

354
00:22:18,000 --> 00:22:20,000
 I'll make my stories about the Buddha."

355
00:22:20,000 --> 00:22:23,000
 It's just stories.

356
00:22:23,000 --> 00:22:25,570
 So you could argue that, that there wasn't malevolent

357
00:22:25,570 --> 00:22:28,000
 intent, or there wasn't the intent.

358
00:22:28,000 --> 00:22:31,760
 It's not like putting quotes in Gandhi's mouth, who we know

359
00:22:31,760 --> 00:22:34,000
 did exist.

360
00:22:34,000 --> 00:22:37,000
 So that could be a mitigating factor.

361
00:22:37,000 --> 00:22:40,000
 But as a Buddhist, I would never do that.

362
00:22:40,000 --> 00:22:47,000
 Put words in the Buddha's mouth.

363
00:22:47,000 --> 00:22:51,000
 Okay, thank you, Bhakti.

364
00:22:51,000 --> 00:23:01,000
 Welcome.

365
00:23:01,000 --> 00:23:02,000
 So who do we have?

366
00:23:02,000 --> 00:23:04,000
 Tom?

367
00:23:04,000 --> 00:23:05,000
 This is what?

368
00:23:05,000 --> 00:23:07,000
 Simon?

369
00:23:07,000 --> 00:23:08,000
 Lisa?

370
00:23:08,000 --> 00:23:12,000
 I don't know Lisa, do I?

371
00:23:12,000 --> 00:23:14,000
 And Douglas.

372
00:23:14,000 --> 00:23:19,000
 Do I know Douglas?

373
00:23:19,000 --> 00:23:23,000
 Who are Lisa and Douglas?

374
00:23:23,000 --> 00:23:32,000
 Do you guys have mics?

375
00:23:32,000 --> 00:23:35,000
 Why are you on the hangout if you don't talk?

376
00:23:35,000 --> 00:23:36,000
 Hello?

377
00:23:36,000 --> 00:23:37,000
 Hi.

378
00:23:37,000 --> 00:23:38,000
 I'm Doug.

379
00:23:38,000 --> 00:23:39,000
 Hi, Doug.

380
00:23:39,000 --> 00:23:40,000
 Hi.

381
00:23:40,000 --> 00:23:43,000
 Oh, that's not Doug from Australia, is it?

382
00:23:43,000 --> 00:23:45,000
 No, this is my first time.

383
00:23:45,000 --> 00:23:46,000
 Okay.

384
00:23:46,000 --> 00:23:50,000
 Do you have a question?

385
00:23:50,000 --> 00:23:51,000
 Sure.

386
00:23:51,000 --> 00:23:57,000
 I have a question about meditation.

387
00:23:57,000 --> 00:24:02,650
 I have a lot of intense anxiety at work, and I've tried med

388
00:24:02,650 --> 00:24:05,000
itating during my breaks at work,

389
00:24:05,000 --> 00:24:08,000
 and I've tried mindfulness and breathing.

390
00:24:08,000 --> 00:24:11,450
 But my anxiety and maybe my adrenaline is so high that I

391
00:24:11,450 --> 00:24:14,000
 don't seem to be able to calm myself down

392
00:24:14,000 --> 00:24:16,000
 with my mindfulness and breathing.

393
00:24:16,000 --> 00:24:17,000
 Is there another question?

394
00:24:17,000 --> 00:24:19,000
 Don't use mindfulness and breathing.

395
00:24:19,000 --> 00:24:20,000
 Okay.

396
00:24:20,000 --> 00:24:24,710
 Look, I mean, if you want, don't recommend it, because it's

397
00:24:24,710 --> 00:24:26,000
 summertime meditation.

398
00:24:26,000 --> 00:24:27,000
 Oh, I don't know.

399
00:24:27,000 --> 00:24:29,000
 Depends on what you ask.

400
00:24:29,000 --> 00:24:30,000
 Okay.

401
00:24:30,000 --> 00:24:31,000
 You want my advice?

402
00:24:31,000 --> 00:24:32,000
 Because you're asking me.

403
00:24:32,000 --> 00:24:33,000
 Yeah.

404
00:24:33,000 --> 00:24:37,000
 I'm allowed to be partisan, because you've asked me.

405
00:24:37,000 --> 00:24:39,000
 But just know that I'm being a bit partisan.

406
00:24:39,000 --> 00:24:42,000
 When I say don't use that.

407
00:24:42,000 --> 00:24:46,870
 Look up, because you're asking me, look up, utadamo,

408
00:24:46,870 --> 00:24:48,000
 anxiety.

409
00:24:48,000 --> 00:24:52,000
 I'm going to look it up as well and make sure.

410
00:24:52,000 --> 00:24:57,000
 Removing anxiety completely, meditation for anxious people,

411
00:24:57,000 --> 00:24:59,000
 and there's three right there.

412
00:24:59,000 --> 00:25:01,000
 My first three results on Google.

413
00:25:01,000 --> 00:25:02,000
 Okay.

414
00:25:02,000 --> 00:25:08,000
 I mean, I could go over it again, but it's all been said.

415
00:25:08,000 --> 00:25:12,000
 I mean, basically, the basic thing is it's not...

416
00:25:12,000 --> 00:25:15,000
 Let me see.

417
00:25:15,000 --> 00:25:17,750
 Anxiety is both physical and mental, and the physical

418
00:25:17,750 --> 00:25:19,000
 aspects aren't anxiety.

419
00:25:19,000 --> 00:25:22,000
 Anxiety isn't the physical, but they bounce off each other.

420
00:25:22,000 --> 00:25:26,530
 So being anxious creates physical experiences, physical

421
00:25:26,530 --> 00:25:29,000
 effects, like the butterflies in your stomach,

422
00:25:29,000 --> 00:25:33,000
 the heart beating, the tension, etc, etc, headaches even.

423
00:25:33,000 --> 00:25:35,450
 And that makes you anxious, because you say, oh my gosh, I

424
00:25:35,450 --> 00:25:36,000
'm anxious.

425
00:25:36,000 --> 00:25:37,000
 You're not actually.

426
00:25:37,000 --> 00:25:39,000
 Anxiety is just a moment.

427
00:25:39,000 --> 00:25:41,780
 And then there's the physical experience, and then you get

428
00:25:41,780 --> 00:25:43,000
 anxious about that.

429
00:25:43,000 --> 00:25:50,000
 I was even realizing that, you know what makes you anxious?

430
00:25:50,000 --> 00:25:54,000
 You get anxious that you're going to be anxious.

431
00:25:54,000 --> 00:25:57,000
 You think about something and say, oh, I can't...

432
00:25:57,000 --> 00:26:00,000
 You say, what if I freeze up?

433
00:26:00,000 --> 00:26:02,000
 I have to go and talk, give a talk.

434
00:26:02,000 --> 00:26:05,000
 Oh man, am I going to mess it up?

435
00:26:05,000 --> 00:26:08,840
 Am I going to freeze because I'm so anxious and that makes

436
00:26:08,840 --> 00:26:10,000
 you anxious?

437
00:26:10,000 --> 00:26:15,610
 So it's the snowballing, and the insight meditation breaks

438
00:26:15,610 --> 00:26:16,000
 that.

439
00:26:16,000 --> 00:26:18,680
 When you're able to note anxious, anxious, or note the

440
00:26:18,680 --> 00:26:20,000
 physical as well.

441
00:26:20,000 --> 00:26:22,000
 I mean, watch those videos that I've done.

442
00:26:22,000 --> 00:26:24,000
 I'm sure one of them is good.

443
00:26:24,000 --> 00:26:26,000
 On YouTube, could you say?

444
00:26:26,000 --> 00:26:30,000
 Yeah, well just Google "Yutadamo anxiety".

445
00:26:30,000 --> 00:26:37,000
 I've also got just a shameless plug.

446
00:26:37,000 --> 00:26:45,000
 Video.ceremongolo.org has many of my videos categorized.

447
00:26:45,000 --> 00:26:48,110
 So under mental, it's probably under mental issues or

448
00:26:48,110 --> 00:26:49,000
 something.

449
00:26:49,000 --> 00:26:53,000
 Mental issues, there's anxiety.

450
00:26:53,000 --> 00:26:57,000
 I bet someone in the chat has already put something up.

451
00:26:57,000 --> 00:27:00,000
 Nobody.

452
00:27:00,000 --> 00:27:02,000
 But yeah, look up.

453
00:27:02,000 --> 00:27:08,000
 If you ever looking for something about a specific subject,

454
00:27:08,000 --> 00:27:14,000
 I don't see anxiety there.

455
00:27:14,000 --> 00:27:16,000
 Panic attack?

456
00:27:16,000 --> 00:27:17,000
 Is there?

457
00:27:17,000 --> 00:27:18,000
 I don't know.

458
00:27:18,000 --> 00:27:21,000
 I'm guessing for another name for it.

459
00:27:21,000 --> 00:27:23,000
 Yeah, it's funny.

460
00:27:23,000 --> 00:27:29,000
 I'm surprised there isn't one.

461
00:27:29,000 --> 00:27:37,000
 Well, we better add it.

462
00:27:37,000 --> 00:27:40,000
 Maybe it's in a different spot.

463
00:27:40,000 --> 00:27:44,000
 Health?

464
00:27:44,000 --> 00:27:46,000
 Okay, well you can Google it anyway.

465
00:27:46,000 --> 00:27:50,000
 Okay.

466
00:27:50,000 --> 00:27:54,000
 Thank you.

467
00:27:54,000 --> 00:27:56,000
 Oh, and under fear.

468
00:27:56,000 --> 00:27:58,000
 For some reason it's all lumped under fear.

469
00:27:58,000 --> 00:28:00,000
 It shouldn't be probably.

470
00:28:00,000 --> 00:28:01,000
 But there's room.

471
00:28:01,000 --> 00:28:17,000
 Yeah, the videos are all under fear.

472
00:28:17,000 --> 00:28:19,000
 Something's broken.

473
00:28:19,000 --> 00:28:25,000
 Okay.

474
00:28:25,000 --> 00:28:27,000
 So that answer that did answer.

475
00:28:27,000 --> 00:28:30,000
 Like it was just about anxiety, right?

476
00:28:30,000 --> 00:28:32,000
 And what type of meditation I could use.

477
00:28:32,000 --> 00:28:33,000
 Right.

478
00:28:33,000 --> 00:28:35,000
 So you were so yeah, use read.

479
00:28:35,000 --> 00:28:38,000
 If you haven't yet read my booklet on how to meditate.

480
00:28:38,000 --> 00:28:39,000
 Okay.

481
00:28:39,000 --> 00:28:42,130
 Yeah, maybe that's even better than the videos that I did

482
00:28:42,130 --> 00:28:44,000
 because if you haven't read the booklet,

483
00:28:44,000 --> 00:28:46,820
 that's where you got to start with this community or with

484
00:28:46,820 --> 00:28:48,000
 with talking to me.

485
00:28:48,000 --> 00:28:49,000
 Okay.

486
00:28:49,000 --> 00:28:54,230
 It's it's not so much doctrine as it is giving you

487
00:28:54,230 --> 00:28:58,000
 meditation techniques to use.

488
00:28:58,000 --> 00:29:02,450
 But then specifically watch the videos on anxiety because

489
00:29:02,450 --> 00:29:05,240
 they're also very practical explaining how you'd use the

490
00:29:05,240 --> 00:29:07,000
 technique to deal with anxiety,

491
00:29:07,000 --> 00:29:10,330
 which is an interesting case because as I said, much of it

492
00:29:10,330 --> 00:29:12,000
 is physical when we think it's all anxiety.

493
00:29:12,000 --> 00:29:14,000
 We think I'm anxious.

494
00:29:14,000 --> 00:29:15,000
 But a lot of it is not anxiety.

495
00:29:15,000 --> 00:29:18,290
 A lot of it is the physical effects of anxiety, which then

496
00:29:18,290 --> 00:29:19,000
 perpetuate the cycle.

497
00:29:19,000 --> 00:29:21,000
 They make you anxious.

498
00:29:21,000 --> 00:29:23,000
 Oh my gosh, I'm anxious.

499
00:29:23,000 --> 00:29:33,000
 I'm getting more anxious.

500
00:29:33,000 --> 00:29:35,000
 What about Lisa?

501
00:29:35,000 --> 00:29:41,000
 Who is Lisa?

502
00:29:41,000 --> 00:29:49,000
 Has no mic, I guess.

503
00:29:49,000 --> 00:29:50,000
 All right.

504
00:29:50,000 --> 00:29:55,000
 Well, thanks, guys, for actually joining the hang up.

505
00:29:55,000 --> 00:30:21,000
 Thank you.

506
00:30:21,000 --> 00:30:25,000
 Thank you for being so so supportive of our community.

507
00:30:25,000 --> 00:30:28,000
 I can say that it's really awesome.

508
00:30:28,000 --> 00:30:30,000
 I mean, it looks like I did mention it didn't I?

509
00:30:30,000 --> 00:30:38,000
 I mean, it looks like we're we're we're moving ahead.

510
00:30:38,000 --> 00:30:39,000
 So good night.

511
00:30:39,000 --> 00:30:40,000
 Good night.

512
00:30:41,000 --> 00:30:42,000
 Good night.

513
00:30:44,000 --> 00:30:45,000
 Good night.

514
00:30:47,000 --> 00:30:48,000
 Good night.

515
00:30:49,000 --> 00:30:50,000
 Good night.

516
00:30:51,000 --> 00:30:52,000
 Good night.

517
00:30:54,000 --> 00:30:55,000
 Good night.

