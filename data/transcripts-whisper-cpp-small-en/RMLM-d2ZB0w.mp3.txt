 Welcome everyone to our daily meditation session. Please
 sit in the chair. It's fine.
 Oh no, don't worry about it. Sit comfortably.
 Tonight we have, this is our largest audience yet I think.
 There are seven of us here and we've got five people
 watching from the internet.
 They're a live broadcast.
 So tonight is the English session.
 I've been, I missed most of the Thai sessions lately.
 But we'll try to get up so that we're here every day.
 Tonight I wanted to talk about dealing with the truth.
 Because Buddhism deals specifically with the truth.
 I'm just going to talk here. You're welcome to close your
 eyes and meditate.
 Normally when I talk I close my eyes. I'm not trying to be
 entertaining.
 I'm trying to be meditative. So even when I talk I try to
 stay in touch with the meditation practice.
 When you listen you should also try to stay in touch with
 the meditation practice.
 You can use it as an opportunity to practice yourself.
 So Buddhism deals specifically with the truth.
 It's a very, it's the core issue in Buddhism is the
 realization of the truth
 or the realization of specific truths.
 In brief the realization of the truth of reality
 or coming to understand the truth about reality as it is.
 And by reality we're talking here not about theoretical
 reality or the reality of the physicists,
 the reality of philosophers. We're talking about experient
ial reality.
 The reality which you can experience. Some people call it
 subjective reality.
 But actually in Buddhism we're not dealing with subjective
 reality.
 And we deny the claim that first person experience of
 reality is necessarily subjective.
 Subjective here meaning particularly different for everyone
 or unscientific.
 Because in a sense reality for us is subjective. We are the
 subject or there is a subject and that's the mind.
 As opposed to looking at things objectively in terms of the
 object or in terms of a third person perspective.
 But it's not subjective in the sense that my reality is not
 different from your reality.
 It is the truth that we try to come to realize. It's an
 objective truth about subjective reality.
 So a scientist would look at meditation and say you can't
 get truth from that. It's subjective.
 How do you know you're seeing the things you see? How do
 you know you're hearing the things you hear?
 How do you know it's real? It's subjective.
 There's no way of telling whether it's real or not.
 And that's the kind of reality that they're talking about
 is not the reality we're talking about.
 When we talk about reality in Buddhism we're talking about
 experience.
 And in that we claim that my experience is the same as your
 experience.
 Because no matter what you see, you still see.
 No matter what I see it's still seeing and my seeing and
 your seeing is the same in the sense that it's both seeing.
 Meaning there are some essential building blocks of
 experience which are common to all beings, the experience
 of all beings.
 When we break experience down into those building blocks
 then we have an objective understanding of experiential
 reality.
 This is what we mean by reality.
 So what is the truth that we're trying to realize?
 Well, there's a lot of truth that can come from experient
ial reality.
 There are many truths that we're going to realize in the
 course of the practice.
 But in particular we're focusing on those truths that are
 going to free our mind.
 Those truths that are going to allow us to react
 appropriately to reality.
 We're not interested in what is it like to taste ice cream
 or what is it like to see a supernova or what is it like to
 hear an angel or so on.
 Any mystical or magical experience which is very much a
 part of reality.
 We're focusing on those truths that are particularly useful
 and have to do with the building blocks of experience, have
 to do with our interaction with the world around us.
 Because our experience of reality is not always pleasant.
 We want to be happy. Everyone wants to be happy.
 And yet among the many, many people who wish to be happy,
 most of us are less than 50% happy, happy less than 50% of
 the time.
 Which means we're losing.
 Our reality is contrary to our wishes.
 So in brief something's wrong.
 You can never say this is a proper state of being where we
 want to be happy and yet we're happy less than 50% of the
 time.
 Some people live most of their lives unhappy.
 Even just sitting here you can see that your experience of
 reality is not entirely pleasant.
 Maybe it's too hot, maybe it's too cold, maybe your head
 hurts, maybe your back hurts, maybe your legs hurt, maybe
 you're itching and scratching, maybe you're thinking about
 unpleasant things, maybe you don't like what I'm eating or
 up in the air, philosophical.
 Doctrine or theory. We're talking about the building blocks
 of our experience.
 You're hot. Well heat is a building block. Heat is reality.
 It's really hot.
 How do you feel about the heat? You don't like it? Well
 disliking is part of reality.
 It's part of your reality. That's your reaction to the heat
 or the cold or the itching or the pain.
 [silence]
 In the meditation we're going to see these things clearer
 than we... we're going to see the way reality works clearer
 than before in a way that we couldn't see it before.
 Normally when it's... when there's pain in the body for
 example, right away we don't like it. Right away we've made
 up our mind it's bad.
 And not only that we immediately take action without even
 thinking, without even really looking at it and examining
 it to see what is this pain?
 Is it really bad? What's bad about it?
 And it can get to the point where even just hearing a
 simple sound, once we get a hatred or a dislike for a
 phenomenon, we become so react... we can become reactionary
 towards it.
 Even just hearing someone's voice, when we get it in our
 minds that this person is unwelcome or unappreciated, is an
 unpleasant person, as soon as we hear the voice there's
 nothing wrong with the voice.
 It's a sound that arises in the ear. We right away get
 angry. We've already made up our mind and we're ready to
 block them out or do whatever we can to not have to hear
 the sound.
 This is our ordinary experience of reality.
 The truth of reality that we're trying to realize, to cut
 to the chase, to do your work for you so you don't have to
 practice, I give you the answer. The answer is there's
 nothing good or bad about reality.
 The realization that we gain from meditation is that there
's no positive or negative experience whatsoever. It's our
 reaction to things based on an accumulated preference and
 this habit of addiction, of needing things to be a certain
 way of compartmentalizing reality, of thinking that somehow
 these things are going to make us happy.
 Somehow we're going to arrange our lives so that everything
 that we like is going to come to us and everything we don't
 like is going to stay away from us.
 We compartmentalize reality. We build up these false
 characterizations, judgments of innocent experience.
 And it's to the point that we've stopped looking, we've
 stopped examining, we've stopped appreciating reality for
 what it is.
 We already know the answer. Pain bad, okay, run. Change
 position. Go see a doctor. Get rid of it.
 And when we look at it, when we look at the pain, we see
 that it's totally not what we thought. It's something
 completely different from what we thought of it.
 When we look at it, we watch it and we remind ourselves of
 what it is. We teach ourselves, okay, we see this, this is
 pain.
 Okay, that wasn't so bad. When we look at it again, we see
 pain. We just remind ourselves of this pain, pain, pain.
 And as we open up to the experience, we see that there's
 nothing negative about pain at all.
 It's one of the most liberating experiences that come in
 the meditation practice during the time that you're
 practicing, is the ability to conquer the pain.
 You can get to the point where pains that have been
 haunting you through your whole practice for days and days
 suddenly vanish.
 Suddenly you conquer the pain. Your mind changes. Your mind
 shifts. And it gives in. You know, you keep going back to
 it. You say, you know, it's only pain.
 Reminding yourself, it's only pain. It is what it is. It's
 not bad, it's not good, it's not me, it's not mine. It's
 not a problem. It's pain.
 And if you do this enough, your mind shifts and your mind
 says, "Oh, it's pain."
 And it's like you've conquered an enemy. Suddenly there's
 no fear, there's no anger or upset.
 Towards that pain there's a confidence, there's an
 understanding and a surety. You're certain in your mind.
 You're not worried about it anymore. The pain comes, let it
 come. And then it just disappears, like it was never there
 in the first place.
 Just as an example, all of our experience is like this.
 Pain is a difficult one because it's ingrained in our minds
 that pain means there's a problem. The body is trying to
 tell us something, we say.
 But there's so much more that we can point out that's in
 the very same vein. When we hear things, as I said, we can
 get so angry.
 When children come and they're bothering us, children can
 be very annoying sometimes.
 But there's nothing annoying about the child, it's in our
 own mind we get annoyed, we get angry.
 We don't really hear the sound, maybe we're busy with
 something else.
 And so we've compartmentalized reality, this sound is bad,
 this work that I'm focusing on is good.
 And you can only accept part of reality, the part that you
 can't accept you have to stop, you have to block, you have
 to shout at the child or hit it or so on.
 We work this way.
 One of the interesting things people often, we have this ad
age, this idiom about pain, about truth, sorry.
 And we say truth hurts.
 And it doesn't take much explaining to understand where
 this comes from.
 We do believe this, truth hurts.
 And it's kind of like a necessary evil sometimes to tell
 people the truth.
 We say it would hurt someone if you said you're fat, by the
 way you're fat, or you're ugly, or you're too tall, or this
 or that.
 I mean there's certain truths that we say you would never
 say to someone, boy you're old, or so on.
 Look at all those wrinkles on your face or whatever.
 You look very bad with those glasses or whatever.
 These truths, that dress looks terrible on you.
 Sometimes we say telling the truth is painful.
 But in all of these examples, even these examples that are
 so taboo, and in fact it's funny because in Thailand many
 of them are not taboo.
 It's funny how culture defines what is right and what is
 wrong.
 I mean in America if you call someone fat, it's just so, I
 don't know in America, in Canada it would be just,
 it would be a total disgrace to call someone fat.
 But in Thailand it's not such a big deal and they're often,
 it's kind of funny in that way.
 And you know in Thailand they ask women how old they are,
 and you're supposed to tell them how old you are, things
 like that.
 So we build up these, in some cultures it's not wrong to be
 large.
 It's not such a big deal, or at least it's not a very
 negative thing.
 I saw these two monks in Thailand.
 I think it was kind of going over the edge, but I think it
 was going over the edge.
 And especially as a Western it was kind of terrible to
 listen to.
 But this one monk, he was making fun of this dark skin monk
.
 The one monk had light skin, the other monk had dark skin.
 And I guess in Thailand it's okay to make fun of people
 because of their skin color.
 Because we were up on this mountain and he said, "Oh we'll
 leave you up here and when we come back the clouds will all
 be dark."
 The clouds will all be dark because your skin color rubbed
 off on them.
 And he was making jokes like this and that was horrifying.
 But the point is, in some cases making fun of people I
 think is always wrong.
 I don't think we should ever go that way.
 The other funny thing is the other monk didn't seem
 offended by it.
 Whereas if you said something like that in the West you
 might get hit for it.
 And so on. If you call someone a buffalo in Thailand you
 might get hit.
 In America people laugh at it.
 I think a buffalo is strong, that's kind of a good thing.
 The point is that our perception is cloud reality. We don't
 see things for what they are.
 My teacher said, "If someone ever calls you a buffalo just
 turn around and see if you've got a tail.
 If you don't have a tail you're not a buffalo."
 And that really sums it all up.
 There's no reason to be upset. If it's true it's true, if
 it's false it's false.
 And so even when someone calls you a nasty name or points
 out even a real flaw,
 you know, you say, "Oh, the truth hurts."
 Sometimes people say, "Look, I hate to tell you but..."
 And so on. So we say, "Oh, the truth hurts." But the truth
 doesn't hurt.
 And this is one thing I want to stress.
 And it's important to stress I think because Buddhism is
 probably...
 I have to think about it, but I think Buddhism is one of
 the most boring religions out there.
 Buddhism in its essence is the ultimate boredom.
 It's not, but that's how it's easily perceived.
 I mean, there's nothing fancy, there's nothing...
 It's the core of Buddhism. Now the cultural expression of
 Buddhism is often very fancy
 and flowery and magical and mystical.
 But when you get deeper into it and start talking to monks
 and start hanging out in monasteries,
 it's actually... it can be quite dull.
 You're expected to sit still.
 You're expected to look at very mundane things.
 Most of my Thai students would rather be seeing bright
 lights and angels or ghosts.
 It's exciting for them. And for them that's Buddhism. But
 it's not Buddhism.
 It's not the Buddhist teaching.
 And many people are turned off by this.
 I've had people write away when they... in some of my
 videos I think I used the wrong word
 or I used a word that's easily taken the wrong word and
 that's the word enjoy.
 Because the word enjoy is such a positive thing for us to
 enjoy something.
 To enjoy an experience.
 I would say things like instead of enjoying it.
 But the problem with the word enjoyment is it means to...
 it means it carries a connotation of experiencing something
 to its fullest.
 But that's not what the word means. The word means to like
 something.
 But we give it this connotation of experiencing it to the
 fullest.
 So people would say, "Look at what you're telling us to
 block things out,
 to avoid things, to not experience things to the fullest."
 Because this is what the word enjoy means.
 And so people say that this is dull, it's dry.
 And people are so afraid, like afraid of things like nir
vana.
 I remember when I first heard about it, when I first
 started practicing Buddhism,
 that was the major hurdle for me, was this idea of nirvana.
 Because that's like no more. No more sensuality, no more
 pleasure, nothing.
 I know theoretically it made some sense but I was like, "
Man, that's so hard to grasp, so hard to accept."
 And we think of non-enjoyment of pleasures.
 People look at the monk's life and they think of it as such
 a terrible austere life,
 must be full of pain and suffering.
 Many people even look at meditation this way.
 I think of meditation as torture.
 The idea of sitting still with your eyes closed.
 And they make up all sorts of, I think it's just rational
izing when they say,
 "How pointless it is," and so on. I think what they really
 mean is, "I can't do it."
 It's torture. It's unpleasant. It's boring.
 So I think this idiom applies to Buddhism, "The truth hurts
."
 It applies in the sense that people think this about
 practices like Buddhism.
 And again, we're claiming to understand the truth.
 And I would say that's the way it is. People would much
 rather wander around in illusion.
 I teach on the internet in many different arenas, and one
 of them is virtual reality.
 So we go in and there's the Deer Park and we're sitting in
 front of the Buddha.
 And people are coming in with such costumes.
 Men dressed up as women, women not wearing clothes, or very
 little clothes.
 We've got people with furry heads and tails and wings.
 Why we would rather have this is because it's exactly what
 we want.
 We can get what we want. We can control it.
 You can't control reality.
 You can't compartmentalize reality as they said.
 You can say, "This is good, this is bad."
 But you can't say, "Let the bad not come and only the good
 come."
 In illusion you can do that.
 Your illusion can be whatever you want.
 You go into this virtual reality and you say, "I want this,
 I want that, I don't want that."
 And 90% of the experience is exactly what you'd like it to
 be.
 You have to work for it, you have to pay for it.
 It becomes an addiction, but you can get it.
 So we'd rather be in illusion as we say, "Truth hurts."
 But I said it before, I'll say it again, truth doesn't hurt
.
 What hurts is our inability to accept the truth.
 Buddhism isn't a boring religion. It isn't dry.
 Being a monk, being a meditator isn't a painful experience.
 There's nothing painful about sitting still.
 What's painful is our inability to accept things for what
 they are.
 And that comes from our inability to understand things as
 they are.
 Once we understand things for what they are, we see they're
 not scary.
 They're not negative, they're not unpleasant.
 We see that all of that pleasure and happiness we were
 trying to attain
 and chasing after and spending so much of our effort,
 just to get the very smallest piece of,
 all of that happiness could have been amplified hundreds
 and hundreds,
 thousands of times simply by coming to understand the
 reality in front of us,
 turning this reality into heaven.
 Look at things as they are.
 We see something, that's seeing.
 We hear something, that's hearing.
 It's not good, it's not bad, it's not me, it's not mine.
 It's not us and them and good, right and wrong and so on.
 It is what it is.
 Right now there's experience going on every moment.
 Once we understand this experience and we can live with it
 and accept it for what it is, there's nothing anywhere,
 at any time that can cause suffering for us.
 We're said to be freed, we're said to be released from
 bondage
 and released from suffering.
 So this is why we practice, why we torture ourselves,
 go through all this pain and suffering.
 Because we realize that it's not the meditation that's
 causing our suffering,
 it's our own minds, our inability to accept.
 This very ordinary, very simple reality.
 The truth doesn't hurt.
 There's another idiom and that goes,
 "The truth will set you free."
 And that's exactly what is meant by the practice of the
 Buddhist teaching.
 When you realize the truth, it will set you free.
 So that's the Dhamma I thought to give today.
 Now we'll go on to the more practical part of the evening
 and that's to do the meditation practice.
 So first we'll do mindful prostration, then walking and
 sitting.
 Thank you.
