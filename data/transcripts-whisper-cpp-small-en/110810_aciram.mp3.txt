 anicca vata sankhara upadvayadham mino upadjitvani rujanti
 desangupasumosukho
 impermanent indeed are all formations of a nature to arise
 and pass away.
 having arisen they all cease.
 their stilling is happiness.
 achirangvata yanka yo padthavingadhi sezati chudho apetav
inyano niratangvakalangarang
 before long indeed this body will lie on the earth,
 discarded, left by the mind, as useless
 as a wooden log.
