1
00:00:00,000 --> 00:00:14,600
 So tonight's question is about fun.

2
00:00:14,600 --> 00:00:19,830
 I don't think it's really about fun, but my answer is going

3
00:00:19,830 --> 00:00:20,680
 to be about fun.

4
00:00:20,680 --> 00:00:25,720
 So the person asks whether they're practicing correctly.

5
00:00:25,720 --> 00:00:31,800
 They say they've been doing this for some time, noting,

6
00:00:31,800 --> 00:00:33,800
 using the technique, but they're

7
00:00:33,800 --> 00:00:37,970
 not sure how to know whether they're doing it right because

8
00:00:37,970 --> 00:00:39,160
 it's not fun.

9
00:00:39,160 --> 00:00:45,640
 So I thought I would expand this answer to discuss the

10
00:00:45,640 --> 00:00:47,000
 topic of fun.

11
00:00:47,000 --> 00:00:50,720
 It won't take a long time, hopefully.

12
00:00:50,720 --> 00:01:00,720
 Let's go over some of the issues around meditation and fun.

13
00:01:00,720 --> 00:01:05,070
 So the reason why your meditation might not be fun, I can

14
00:01:05,070 --> 00:01:07,200
 think of three reasons.

15
00:01:07,200 --> 00:01:09,960
 That's what I'll talk about.

16
00:01:09,960 --> 00:01:14,450
 First reason why your meditation might not be fun is

17
00:01:14,450 --> 00:01:17,480
 because you're doing it wrong.

18
00:01:17,480 --> 00:01:21,770
 It's quite easy to practice incorrectly, especially if you

19
00:01:21,770 --> 00:01:23,520
 don't have a teacher.

20
00:01:23,520 --> 00:01:26,640
 You're just going by my booklet on how to meditate.

21
00:01:26,640 --> 00:01:30,840
 It's quite possible that you're practicing wrong.

22
00:01:30,840 --> 00:01:36,200
 And by wrong, I mean getting something wrong.

23
00:01:36,200 --> 00:01:39,920
 You haven't quite understood perhaps how to do the noting,

24
00:01:39,920 --> 00:01:41,560
 and so you're trying to make

25
00:01:41,560 --> 00:01:43,120
 the things go away.

26
00:01:43,120 --> 00:01:46,540
 Sometimes you have extreme problems to the extent that

27
00:01:46,540 --> 00:01:48,440
 someone, rather than when they

28
00:01:48,440 --> 00:01:52,640
 have pain saying to themselves, "Pain, pain," they'll

29
00:01:52,640 --> 00:01:55,360
 repeat to themselves, "No pain, no

30
00:01:55,360 --> 00:01:59,640
 pain," or if they're afraid, they'll say, "Calm, calm,"

31
00:01:59,640 --> 00:02:02,400
 because the point, their misunderstanding

32
00:02:02,400 --> 00:02:07,800
 is that they should repeat what they want to happen.

33
00:02:07,800 --> 00:02:12,400
 A less extreme but still wrong perspective is to say to

34
00:02:12,400 --> 00:02:15,440
 yourself, for example, "Pain,

35
00:02:15,440 --> 00:02:18,320
 pain," and want the pain to go away, or to say, "Thinking,

36
00:02:18,320 --> 00:02:20,000
 thinking," and be frustrated

37
00:02:20,000 --> 00:02:21,000
 when it comes back.

38
00:02:21,000 --> 00:02:26,430
 And without any input from a teacher, feedback from a

39
00:02:26,430 --> 00:02:29,680
 teacher, it can be quite discouraging

40
00:02:29,680 --> 00:02:33,590
 because you have a perspective, an attitude of trying to

41
00:02:33,590 --> 00:02:35,800
 control things and make things

42
00:02:35,800 --> 00:02:40,500
 change based on your whim, which really is the whole point

43
00:02:40,500 --> 00:02:42,680
 of the meditation, to see

44
00:02:42,680 --> 00:02:45,120
 that that's not possible.

45
00:02:45,120 --> 00:02:50,590
 So practicing wrongly can make the meditation painful,

46
00:02:50,590 --> 00:02:54,200
 unpleasant, especially if you have

47
00:02:54,200 --> 00:02:59,300
 a wrong perspective, if you have a wrong attitude, a wrong

48
00:02:59,300 --> 00:03:00,400
 outlook.

49
00:03:00,400 --> 00:03:03,750
 I think I could suggest that wanting your meditation to be

50
00:03:03,750 --> 00:03:05,440
 fun, though I don't really

51
00:03:05,440 --> 00:03:11,180
 think what this person was saying is they wanted it to be

52
00:03:11,180 --> 00:03:12,000
 fun.

53
00:03:12,000 --> 00:03:14,840
 Fun is not really inherently different from any other kind

54
00:03:14,840 --> 00:03:16,080
 of pleasure seeking.

55
00:03:16,080 --> 00:03:19,510
 We call things fun, we seek out fun as a part of our

56
00:03:19,510 --> 00:03:21,040
 pleasure seeking.

57
00:03:21,040 --> 00:03:25,480
 It's really just another word for a type of pleasure

58
00:03:25,480 --> 00:03:27,840
 seeking, similar to eating or having

59
00:03:27,840 --> 00:03:31,760
 sex or listening to music.

60
00:03:31,760 --> 00:03:36,310
 Something we say is fun, but deep down what we really mean

61
00:03:36,310 --> 00:03:38,960
 or what we're really responding

62
00:03:38,960 --> 00:03:43,240
 to is the pleasure involved.

63
00:03:43,240 --> 00:03:48,600
 So the idea that your meditation should be pleasurable is

64
00:03:48,600 --> 00:03:52,440
 wrong and has problematic consequences

65
00:03:52,440 --> 00:03:56,400
 because you'll be frustrated when the things that you find

66
00:03:56,400 --> 00:03:58,600
 pleasurable are mixed with the

67
00:03:58,600 --> 00:04:08,080
 things you find unpleasure, displeasure, or displeasing.

68
00:04:08,080 --> 00:04:12,250
 The second reason why your practice might not be fun is

69
00:04:12,250 --> 00:04:16,400
 because you're not good at it.

70
00:04:16,400 --> 00:04:21,090
 And really being not good at meditation is essentially

71
00:04:21,090 --> 00:04:24,640
 doing it wrong, but it's a different

72
00:04:24,640 --> 00:04:30,110
 kind of doing it wrong, I think, because you say someone's

73
00:04:30,110 --> 00:04:32,680
 doing it wrong when there's

74
00:04:32,680 --> 00:04:38,280
 something fundamental about their technique, about their

75
00:04:38,280 --> 00:04:41,480
 perspective, about their approach

76
00:04:41,480 --> 00:04:47,580
 that is problematic that can be easily corrected through

77
00:04:47,580 --> 00:04:50,800
 explanation, through advice.

78
00:04:50,800 --> 00:04:54,260
 Not even exactly advice, but just pointing it out when you

79
00:04:54,260 --> 00:04:55,760
're doing that wrong.

80
00:04:55,760 --> 00:04:59,960
 Take weightlifting, for example.

81
00:04:59,960 --> 00:05:03,910
 You might try to lift too much or you might try to lift the

82
00:05:03,910 --> 00:05:05,960
 wrong way and you might hurt

83
00:05:05,960 --> 00:05:11,440
 yourself as a result if you're training in sports.

84
00:05:11,440 --> 00:05:13,810
 Golfing is one thing to not be good at golfing, but it's

85
00:05:13,810 --> 00:05:15,280
 another to maybe hold the stick

86
00:05:15,280 --> 00:05:20,480
 the wrong way, the club the wrong way.

87
00:05:20,480 --> 00:05:23,720
 Use the wrong club.

88
00:05:23,720 --> 00:05:24,720
 That's doing it wrong.

89
00:05:24,720 --> 00:05:28,690
 If you're trying to hit it very far and you use the putter,

90
00:05:28,690 --> 00:05:30,560
 you're doing it wrong.

91
00:05:30,560 --> 00:05:36,200
 And in meditation there are similar problems, of course.

92
00:05:36,200 --> 00:05:39,000
 But not being good at something means you have an

93
00:05:39,000 --> 00:05:41,200
 understanding of the theory, which

94
00:05:41,200 --> 00:05:42,200
 hopefully you do.

95
00:05:42,200 --> 00:05:46,070
 If you've read the booklet and we've gone over it and

96
00:05:46,070 --> 00:05:47,880
 reassured ourselves together that

97
00:05:47,880 --> 00:05:50,560
 you're doing it, you understand the theory.

98
00:05:50,560 --> 00:05:52,950
 That shouldn't be something you should worry about because

99
00:05:52,950 --> 00:05:54,120
 there's no deep theory that

100
00:05:54,120 --> 00:05:56,240
 you have to understand.

101
00:05:56,240 --> 00:06:00,100
 And I think you have to make a break, make a line, because

102
00:06:00,100 --> 00:06:02,200
 you'll drive yourself crazy

103
00:06:02,200 --> 00:06:06,440
 if you think, "Is there some theory that I don't know?

104
00:06:06,440 --> 00:06:07,440
 Am I doing it wrong?"

105
00:06:07,440 --> 00:06:09,760
 It's a common, common question.

106
00:06:09,760 --> 00:06:11,040
 And you have to separate.

107
00:06:11,040 --> 00:06:15,060
 No, you're not doing it wrong, per se.

108
00:06:15,060 --> 00:06:18,060
 You're doing it wrong in the sense that you're not good at

109
00:06:18,060 --> 00:06:18,480
 it.

110
00:06:18,480 --> 00:06:22,130
 And it's important to understand that that's true about

111
00:06:22,130 --> 00:06:24,440
 meditation, that you have to get

112
00:06:24,440 --> 00:06:25,440
 good at it.

113
00:06:25,440 --> 00:06:27,200
 It is a training.

114
00:06:27,200 --> 00:06:29,000
 It's not a switch that you turn on.

115
00:06:29,000 --> 00:06:33,990
 It's not just a time out where, "Okay, all that craziness

116
00:06:33,990 --> 00:06:35,960
 in my life I'm going to put

117
00:06:35,960 --> 00:06:41,180
 aside and do nothing, and poof, I'm going to be peaceful."

118
00:06:41,180 --> 00:06:45,030
 It is in fact like that, meditation is in the end to let go

119
00:06:45,030 --> 00:06:46,920
 of all the things that cause

120
00:06:46,920 --> 00:06:49,760
 us suffering, stop doing the things that cause us suffering

121
00:06:49,760 --> 00:06:51,240
, but your mind's not going to

122
00:06:51,240 --> 00:06:56,700
 let you do that because we habitually seek out suffering,

123
00:06:56,700 --> 00:06:58,900
 not intentionally.

124
00:06:58,900 --> 00:07:03,000
 We think it's going to make us happy, but it doesn't.

125
00:07:03,000 --> 00:07:06,430
 And so because of those habits having been built up, we're

126
00:07:06,430 --> 00:07:07,480
 in a real bind.

127
00:07:07,480 --> 00:07:09,960
 We try to sit still and we're not going to sit still.

128
00:07:09,960 --> 00:07:12,600
 We try to focus and we're not going to focus.

129
00:07:12,600 --> 00:07:21,580
 We try to just observe something and our mind's got a whole

130
00:07:21,580 --> 00:07:23,920
 other idea.

131
00:07:23,920 --> 00:07:27,690
 So not being good at it when you say to yourself, "Pain,

132
00:07:27,690 --> 00:07:30,580
 pain," but your mind is maybe angry

133
00:07:30,580 --> 00:07:33,970
 about the pain, or you try to say it and then you're

134
00:07:33,970 --> 00:07:36,400
 distracted by something else.

135
00:07:36,400 --> 00:07:41,400
 You try to focus on the stomach but your mind is distracted

136
00:07:41,400 --> 00:07:41,440
.

137
00:07:41,440 --> 00:07:45,150
 You try to sit still but you're restless or agitated or you

138
00:07:45,150 --> 00:07:46,760
're falling asleep.

139
00:07:46,760 --> 00:07:50,320
 There's much about the imbalance of the faculties, right?

140
00:07:50,320 --> 00:07:54,360
 Just effort and concentration are very common.

141
00:07:54,360 --> 00:07:58,800
 There's not much you can do about that except get good.

142
00:07:58,800 --> 00:08:03,880
 As you practice more, you're going to get better at it.

143
00:08:03,880 --> 00:08:09,160
 And part of that, it's not something you can push or pull.

144
00:08:09,160 --> 00:08:14,020
 It's just through repetition and application and diligence,

145
00:08:14,020 --> 00:08:16,260
 you become more energetic and

146
00:08:16,260 --> 00:08:17,360
 less distracted.

147
00:08:17,360 --> 00:08:20,540
 So concentration and energy will start to work together

148
00:08:20,540 --> 00:08:21,640
 rather than fight each other

149
00:08:21,640 --> 00:08:28,050
 or sometimes you're restless, sometimes you're falling

150
00:08:28,050 --> 00:08:29,280
 asleep.

151
00:08:29,280 --> 00:08:32,040
 So it's important to understand that.

152
00:08:32,040 --> 00:08:34,250
 You can be doing everything right and it can still be a

153
00:08:34,250 --> 00:08:35,920
 horrible experience because you're

154
00:08:35,920 --> 00:08:38,240
 not very good at it and that's like any training.

155
00:08:38,240 --> 00:08:41,360
 You're playing golf, it's a horrible experience in the

156
00:08:41,360 --> 00:08:42,280
 beginning.

157
00:08:42,280 --> 00:08:48,180
 But meditation is just like that when you finally hit the

158
00:08:48,180 --> 00:08:51,280
 ball and it goes up the grass.

159
00:08:51,280 --> 00:08:54,970
 When you finally, even just for a moment, "Oh, I was really

160
00:08:54,970 --> 00:08:56,600
 mindful there and it totally

161
00:08:56,600 --> 00:08:57,760
 didn't bother me."

162
00:08:57,760 --> 00:09:04,480
 You realize you're starting to get good at it.

163
00:09:04,480 --> 00:09:09,300
 And the third reason why meditation might not be fun is

164
00:09:09,300 --> 00:09:12,160
 because the goal of meditation

165
00:09:12,160 --> 00:09:14,800
 is not to have fun.

166
00:09:14,800 --> 00:09:20,720
 Fun is a, as I said already, an inferior state.

167
00:09:20,720 --> 00:09:24,790
 Fun is not the goal nor should it be the goal of meditation

168
00:09:24,790 --> 00:09:26,080
 or one's life.

169
00:09:26,080 --> 00:09:30,280
 Being fun is just another means of seeking pleasure.

170
00:09:30,280 --> 00:09:34,680
 Consider anything we call fun or we think of as fun.

171
00:09:34,680 --> 00:09:40,800
 How long can you engage in that activity before it becomes

172
00:09:40,800 --> 00:09:42,000
 boring?

173
00:09:42,000 --> 00:09:45,860
 Of course the flip side of something being fun is something

174
00:09:45,860 --> 00:09:47,000
 being boring.

175
00:09:47,000 --> 00:09:49,400
 Meditation is very boring.

176
00:09:49,400 --> 00:09:53,160
 But it's only boring because you see it that way.

177
00:09:53,160 --> 00:09:55,500
 And the only reason you see it that way is because you

178
00:09:55,500 --> 00:09:57,120
 stuck to certain things as being

179
00:09:57,120 --> 00:10:01,680
 fun and you're addicted to those things.

180
00:10:01,680 --> 00:10:04,120
 This is the addiction cycle.

181
00:10:04,120 --> 00:10:08,440
 Fun does not escape and cannot escape the addiction cycle.

182
00:10:08,440 --> 00:10:12,440
 And the addiction cycle cannot escape suffering.

183
00:10:12,440 --> 00:10:17,160
 It's possible to engage in something without suffering for

184
00:10:17,160 --> 00:10:17,960
 a time.

185
00:10:17,960 --> 00:10:21,330
 But the build up, the lead up, the result is a

186
00:10:21,330 --> 00:10:23,560
 susceptibility to suffering.

187
00:10:23,560 --> 00:10:25,120
 There's no question.

188
00:10:25,120 --> 00:10:27,120
 You might say, "I don't suffer.

189
00:10:27,120 --> 00:10:29,440
 I have fun and I don't suffer."

190
00:10:29,440 --> 00:10:32,520
 First of all you're deluding yourself most likely because

191
00:10:32,520 --> 00:10:34,120
 half the time we don't realize

192
00:10:34,120 --> 00:10:36,720
 how stressful and unpleasant our lives are.

193
00:10:36,720 --> 00:10:37,720
 We try to forget.

194
00:10:37,720 --> 00:10:41,370
 We've cultivated the ability to ignore and forget how much

195
00:10:41,370 --> 00:10:42,760
 suffering we have.

196
00:10:42,760 --> 00:10:46,700
 Second of all, it's not even so much that you do suffer, it

197
00:10:46,700 --> 00:10:48,400
's that you're caught up

198
00:10:48,400 --> 00:10:49,400
 in suffering.

199
00:10:49,400 --> 00:10:51,600
 You're vulnerable to it.

200
00:10:51,600 --> 00:10:53,400
 What happens if you get sick?

201
00:10:53,400 --> 00:10:59,640
 You're a sports person, an athlete.

202
00:10:59,640 --> 00:11:03,200
 And then you get sick and you get an illness.

203
00:11:03,200 --> 00:11:08,120
 I used to do rock climbing and I was really good at it.

204
00:11:08,120 --> 00:11:13,910
 But then I got tendinitis in my elbow, tendinitis in my

205
00:11:13,910 --> 00:11:18,200
 fingers and I can no longer do it anymore.

206
00:11:18,200 --> 00:11:21,880
 And that wasn't even a big deal but people can like Tiger

207
00:11:21,880 --> 00:11:22,560
 Woods.

208
00:11:22,560 --> 00:11:26,320
 He hurt himself very bad I think.

209
00:11:26,320 --> 00:11:33,520
 I only know that because he's his mother's Buddhist and so

210
00:11:33,520 --> 00:11:36,880
 I hear things about him.

211
00:11:36,880 --> 00:11:40,920
 You can't escape the potential for suffering.

212
00:11:40,920 --> 00:11:44,320
 And what about getting old and sick and dying?

213
00:11:44,320 --> 00:11:46,560
 How are you going to be when you die?

214
00:11:46,560 --> 00:11:51,700
 Your state of mind predicts your rebirth, predicts your

215
00:11:51,700 --> 00:11:54,440
 future, predicts your peace

216
00:11:54,440 --> 00:11:57,240
 of mind when you die.

217
00:11:57,240 --> 00:12:00,490
 And because of the addiction cycle we're constantly seeking

218
00:12:00,490 --> 00:12:00,560
.

219
00:12:00,560 --> 00:12:04,290
 When you die you'll see these bright lights and chase after

220
00:12:04,290 --> 00:12:06,320
 them and oh you'll be reborn.

221
00:12:06,320 --> 00:12:10,600
 It's funny how other religions talk about the bright light.

222
00:12:10,600 --> 00:12:14,920
 I saw the light and that was a sign of heaven.

223
00:12:14,920 --> 00:12:16,600
 Well maybe.

224
00:12:16,600 --> 00:12:22,700
 We have other stories where people who chased after those

225
00:12:22,700 --> 00:12:27,120
 lights ended up in hell and suffering.

226
00:12:27,120 --> 00:12:29,670
 Meditation isn't supposed to be for the purpose of fun and

227
00:12:29,670 --> 00:12:30,960
 it isn't even for the purpose

228
00:12:30,960 --> 00:12:32,500
 of pleasure.

229
00:12:32,500 --> 00:12:37,100
 You have to understand that it's kind of a paradox where

230
00:12:37,100 --> 00:12:41,960
 the end goal of course is peace.

231
00:12:41,960 --> 00:12:49,000
 But it's not the peace that you enjoy.

232
00:12:49,000 --> 00:12:52,880
 And really the whole idea of enjoying things has to be done

233
00:12:52,880 --> 00:12:53,840
 away with.

234
00:12:53,840 --> 00:12:56,640
 And that's the profound and radical aspect of Buddhism

235
00:12:56,640 --> 00:12:58,320
 because it seems horrific that

236
00:12:58,320 --> 00:13:03,360
 you should even suggest to get rid of enjoying things.

237
00:13:03,360 --> 00:13:07,990
 Our whole concept and system of enjoyment is wrong minded,

238
00:13:07,990 --> 00:13:11,040
 wrong headed, wrong intentioned.

239
00:13:11,040 --> 00:13:13,840
 Happiness doesn't work that way.

240
00:13:13,840 --> 00:13:16,860
 Enjoying involves liking, it involves wanting, it involves

241
00:13:16,860 --> 00:13:18,560
 needing, it involves addiction

242
00:13:18,560 --> 00:13:23,550
 and so it never frees you from the vulnerability of

243
00:13:23,550 --> 00:13:24,560
 suffering.

244
00:13:24,560 --> 00:13:29,600
 Happiness, peace, have to be independent of things, events,

245
00:13:29,600 --> 00:13:32,000
 people, places, things and

246
00:13:32,000 --> 00:13:34,920
 events, experiences.

247
00:13:34,920 --> 00:13:38,150
 If your happiness is dependent on sitting there peaceful

248
00:13:38,150 --> 00:13:39,680
 and calm, you'll never be

249
00:13:39,680 --> 00:13:40,680
 peaceful and calm.

250
00:13:40,680 --> 00:13:43,720
 It's a paradox sort of or whatever it is.

251
00:13:43,720 --> 00:13:47,560
 It's ironic, no, it's something.

252
00:13:47,560 --> 00:13:48,560
 It's unfortunate.

253
00:13:48,560 --> 00:13:51,550
 It would be nice that I could want to be peaceful and just

254
00:13:51,550 --> 00:13:52,400
 be peaceful.

255
00:13:52,400 --> 00:13:55,960
 But wanting of course disturbs your peace.

256
00:13:55,960 --> 00:14:00,000
 Catch 22 maybe, I don't know, it's a problem.

257
00:14:00,000 --> 00:14:03,300
 And so our focus can never be on happiness, I've talked

258
00:14:03,300 --> 00:14:04,680
 about this before.

259
00:14:04,680 --> 00:14:08,900
 You can never focus on peace and happiness.

260
00:14:08,900 --> 00:14:11,920
 Your focus has to be on goodness.

261
00:14:11,920 --> 00:14:16,410
 And really by goodness in this deep meditative sense it

262
00:14:16,410 --> 00:14:19,680
 means a good state of mind, purity.

263
00:14:19,680 --> 00:14:23,260
 So when we say to ourselves pain, pain or even just rising,

264
00:14:23,260 --> 00:14:25,320
 falling, thinking, thinking,

265
00:14:25,320 --> 00:14:28,130
 we're trying to cultivate what we consider to be the purest

266
00:14:28,130 --> 00:14:29,520
 state of mind, the purest

267
00:14:29,520 --> 00:14:32,480
 state of mind.

268
00:14:32,480 --> 00:14:36,120
 Purest arisen state of mind is the one that knows an object

269
00:14:36,120 --> 00:14:38,280
 just as it is without judgment,

270
00:14:38,280 --> 00:14:40,400
 without reaction.

271
00:14:40,400 --> 00:14:41,400
 It is what it is.

272
00:14:41,400 --> 00:14:42,400
 Pain is what?

273
00:14:42,400 --> 00:14:44,920
 Not good, not bad, not me, not mine.

274
00:14:44,920 --> 00:14:47,960
 Not a problem, not something to fix.

275
00:14:47,960 --> 00:14:49,600
 Pain is pain.

276
00:14:49,600 --> 00:14:50,760
 Rising is rising.

277
00:14:50,760 --> 00:14:53,240
 It's a training that we're doing.

278
00:14:53,240 --> 00:14:55,160
 Why are we focusing on the stomach and the feet?

279
00:14:55,160 --> 00:14:58,240
 There's nothing special about them.

280
00:14:58,240 --> 00:15:01,320
 But there's nothing worth it, they shouldn't be dismissed

281
00:15:01,320 --> 00:15:01,960
 either.

282
00:15:01,960 --> 00:15:04,640
 There's nothing inferior about them.

283
00:15:04,640 --> 00:15:07,000
 This is an experience moving the foot.

284
00:15:07,000 --> 00:15:13,400
 This is an experience.

285
00:15:13,400 --> 00:15:17,220
 Our focus is not on having fun absolutely, but it's also

286
00:15:17,220 --> 00:15:19,720
 not even on peace and happiness.

287
00:15:19,720 --> 00:15:21,640
 And it never can be.

288
00:15:21,640 --> 00:15:26,040
 Our focus always has to be on goodness and purity.

289
00:15:26,040 --> 00:15:27,960
 And that shouldn't be so bad.

290
00:15:27,960 --> 00:15:29,480
 I mean that shouldn't sound so bad.

291
00:15:29,480 --> 00:15:32,680
 Imagine having a perfectly pure mind.

292
00:15:32,680 --> 00:15:40,440
 The only result could ever be peace, happiness.

293
00:15:40,440 --> 00:15:42,520
 That's an important distinction because we'd go the other

294
00:15:42,520 --> 00:15:43,160
 way, right?

295
00:15:43,160 --> 00:15:47,400
 Who in this world thinks of, "Oh, I'll..."

296
00:15:47,400 --> 00:15:51,010
 Well, I shouldn't put us down too much, but where in

297
00:15:51,010 --> 00:15:53,640
 society, in modern society, are we

298
00:15:53,640 --> 00:15:55,640
 taught about goodness?

299
00:15:55,640 --> 00:15:57,960
 I think in religion we often are.

300
00:15:57,960 --> 00:16:02,070
 And it's not to say many people do have ethics and ideas of

301
00:16:02,070 --> 00:16:04,040
 helping others and so on.

302
00:16:04,040 --> 00:16:05,400
 But what are we told?

303
00:16:05,400 --> 00:16:10,560
 What is much more prevalent and in front of us?

304
00:16:10,560 --> 00:16:11,560
 Seek out pleasure.

305
00:16:11,560 --> 00:16:12,560
 Seek out happiness.

306
00:16:12,560 --> 00:16:15,760
 That's what makes you happy.

307
00:16:15,760 --> 00:16:20,680
 And so our focus is on the result.

308
00:16:20,680 --> 00:16:24,700
 But the only thing that brings that result truly and

309
00:16:24,700 --> 00:16:27,600
 sustainably brings it is goodness,

310
00:16:27,600 --> 00:16:31,600
 is the purity of life.

311
00:16:31,600 --> 00:16:36,320
 So I don't think this person was...

312
00:16:36,320 --> 00:16:40,200
 It wasn't literally saying it should be having fun, like

313
00:16:40,200 --> 00:16:41,480
 playing a game.

314
00:16:41,480 --> 00:16:44,960
 But they certainly did seem to be discouraged.

315
00:16:44,960 --> 00:16:46,280
 And many people are discouraged.

316
00:16:46,280 --> 00:16:49,110
 I don't think people think of meditation as a game very

317
00:16:49,110 --> 00:16:49,640
 much.

318
00:16:49,640 --> 00:16:54,340
 If you've got this far, you're not here to play a game and

319
00:16:54,340 --> 00:16:55,440
 have fun.

320
00:16:55,440 --> 00:16:57,490
 We say something is not fun and by that we mean it's not

321
00:16:57,490 --> 00:16:58,880
 pleasurable, which really is

322
00:16:58,880 --> 00:16:59,880
 the same thing.

323
00:16:59,880 --> 00:17:04,300
 Again, it's just another fun, it's just another type of

324
00:17:04,300 --> 00:17:05,440
 pleasure.

325
00:17:05,440 --> 00:17:08,910
 So don't ever try and find peace and happiness in

326
00:17:08,910 --> 00:17:10,160
 meditation.

327
00:17:10,160 --> 00:17:16,150
 Try and find purity and goodness and clarity of mind,

328
00:17:16,150 --> 00:17:18,800
 strength as well of mind.

329
00:17:18,800 --> 00:17:22,880
 Because happiness is a result that's not a practice.

330
00:17:22,880 --> 00:17:25,560
 It's not the cause.

331
00:17:25,560 --> 00:17:27,840
 Happiness is not the cause of happiness, right?

332
00:17:27,840 --> 00:17:30,520
 Goodness is.

333
00:17:30,520 --> 00:17:34,840
 So some thoughts on fun in meditation.

334
00:17:34,840 --> 00:17:40,720
 Leave it to the Buddhist to take the fun out of everything.

335
00:17:40,720 --> 00:17:43,510
 That should be our motto, we take the fun out of everything

336
00:17:43,510 --> 00:17:44,840
 because it's not good for

337
00:17:44,840 --> 00:17:45,840
 you.

338
00:17:45,840 --> 00:17:46,840
 It's inferior.

339
00:17:46,840 --> 00:17:51,920
 So that's the answer to that.

340
00:17:51,920 --> 00:17:52,840
 Thank you for listening.

341
00:17:52,840 --> 00:17:54,840
 1

342
00:17:54,840 --> 00:17:55,840
 1

343
00:17:55,840 --> 00:17:56,840
 1

