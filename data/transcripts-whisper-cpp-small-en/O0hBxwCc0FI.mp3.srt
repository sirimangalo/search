1
00:00:00,000 --> 00:00:04,480
 I have a terrible fear of death. I won't be able to take it

2
00:00:04,480 --> 00:00:06,720
 off my mind. I'm scared

3
00:00:06,720 --> 00:00:11,010
 I will no longer exist when I die. It keeps my heart

4
00:00:11,010 --> 00:00:12,020
 raising all day

5
00:00:12,020 --> 00:00:15,720
 Makes me so anxious. Can you help me? I

6
00:00:15,720 --> 00:00:19,640
 Can help you help yourself

7
00:00:19,640 --> 00:00:24,000
 There's

8
00:00:24,000 --> 00:00:27,120
 Many ways I can do that

9
00:00:27,760 --> 00:00:31,490
 You could start by reading the book. I wrote a little book

10
00:00:31,490 --> 00:00:33,360
 on how to meditate

11
00:00:33,360 --> 00:00:37,000
 You could contact me by email

12
00:00:37,000 --> 00:00:39,870
 The best thing is you could come out here and start

13
00:00:39,870 --> 00:00:40,640
 practicing

14
00:00:40,640 --> 00:00:49,880
 But the question is help you do what help you

15
00:00:49,880 --> 00:00:56,520
 Remove the fear or help you to

16
00:00:58,240 --> 00:01:00,240
 Rise above it

17
00:01:00,240 --> 00:01:06,360
 I mean that's kind of kind of a

18
00:01:06,360 --> 00:01:12,680
 Kind of a silly way to put it no

19
00:01:12,680 --> 00:01:16,410
 What I mean to say is you're better off not trying if your

20
00:01:16,410 --> 00:01:19,000
 intention is to do away with the fear

21
00:01:19,000 --> 00:01:21,440
 Then you're going to have a problem

22
00:01:22,320 --> 00:01:26,330
 and so rather than recommending rather than trying to help

23
00:01:26,330 --> 00:01:27,960
 you get rid of the fear I

24
00:01:27,960 --> 00:01:31,000
 would help you to

25
00:01:31,000 --> 00:01:34,240
 See the fear and to live with the fear

26
00:01:34,240 --> 00:01:40,030
 Until you can understand the fear and overcome the fear

27
00:01:40,030 --> 00:01:43,840
 because we cling to things like fear we

28
00:01:43,840 --> 00:01:48,240
 Cling to all of our emotions

29
00:01:48,240 --> 00:01:50,240
 And

30
00:01:50,240 --> 00:01:57,430
 Use the clinging as as substitute for actually doing

31
00:01:57,430 --> 00:01:58,120
 something

32
00:01:58,120 --> 00:02:02,720
 As a solution I

33
00:02:02,720 --> 00:02:05,960
 Get stuck on the fear stuck on the emotions

34
00:02:05,960 --> 00:02:09,240
 So look at them

35
00:02:09,240 --> 00:02:11,960
 Stop clinging to them. You have to change the way you think

36
00:02:11,960 --> 00:02:12,400
 about this

37
00:02:12,880 --> 00:02:15,820
 We always have in every case we have to change the way we

38
00:02:15,820 --> 00:02:18,480
 think about it stop thinking of it as a problem

39
00:02:18,480 --> 00:02:21,150
 Stop thinking about the problem at all stop thinking about

40
00:02:21,150 --> 00:02:21,480
 death

41
00:02:21,480 --> 00:02:25,280
 Start thinking about the fear

42
00:02:25,280 --> 00:02:28,660
 Which is the last thing we look at we we never think to

43
00:02:28,660 --> 00:02:30,080
 look at the reaction

44
00:02:30,080 --> 00:02:33,800
 And that's where the problem is the problem is not in death

45
00:02:33,800 --> 00:02:36,680
 the problem is in

46
00:02:36,680 --> 00:02:38,800
 Your reaction to it

47
00:02:38,800 --> 00:02:41,880
 There's no problem with death

48
00:02:42,160 --> 00:02:44,160
 It's a problem with fear

49
00:02:44,160 --> 00:02:48,200
 So instead of thinking about death or trying to figure out

50
00:02:48,200 --> 00:02:51,280
 How to solve this problem of death?

51
00:02:51,280 --> 00:02:54,200
 Figure out how to solve the problem of fear

52
00:02:54,200 --> 00:02:58,000
 That's really what you want them

53
00:02:58,000 --> 00:03:01,840
 No, I mean the problem is that probably you want to not die

54
00:03:01,840 --> 00:03:02,680
, right?

55
00:03:02,680 --> 00:03:08,380
 You can't have that but what you should want is to not fear

56
00:03:08,380 --> 00:03:10,160
 die without fear

57
00:03:12,000 --> 00:03:14,000
 So

58
00:03:14,000 --> 00:03:17,810
 Try to learn about the fear don't get rid of it learn about

59
00:03:17,810 --> 00:03:21,040
 it understand it once you understand the fear

60
00:03:21,040 --> 00:03:24,500
 Your problem would be solved. Let the fear be let it be

61
00:03:24,500 --> 00:03:27,760
 there. So you're afraid of death deathly afraid

62
00:03:27,760 --> 00:03:30,920
 deathly afraid of death

63
00:03:30,920 --> 00:03:34,120
 It's not really a problem

64
00:03:34,120 --> 00:03:54,320
 When you look at it, you'll see it's just useless it's

65
00:03:54,320 --> 00:03:39,760
 meaningless it's pointless

66
00:03:40,400 --> 00:03:43,380
 You see you'll ask yourself. Why am I afraid? What a silly

67
00:03:43,380 --> 00:03:44,040
 thing?

68
00:03:44,040 --> 00:03:49,740
 How pointless how absurd how irrational and you'll keep it

69
00:03:49,740 --> 00:03:50,360
 up?

70
00:03:52,040 --> 00:03:54,040
 So

