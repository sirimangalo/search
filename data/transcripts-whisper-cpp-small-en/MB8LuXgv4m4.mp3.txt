 Why do some people simply possess brilliant minds, are born
 geniuses, etc.?
 Actually, do you think geniuses are born or made?
 May one of the reasons be their previous life in some of
 the higher realms?
 Well, not even necessarily in the higher realms, but some
 aspiration in the past.
 Definitely, quality of the mind, even inclination for
 certain things,
 so people who are inclined towards music, people who are
 inclined towards science.
 It's interesting, last night I went to one of my supporters
' houses,
 and their meditators and their two daughters are also medit
ators.
 And talking to their two daughters about their university
 studies
 and comparing, just chatting about university and about the
 way we learn,
 the one daughter is very, very much math oriented, pure
 math oriented,
 so she doesn't like practical applications.
 There's these two sides of science studies.
 One is people who like the pure stuff, where it's just
 theory,
 doesn't have any basis in reality because reality is muddy,
 reality is organic and there's uncertainty in it and so on.
 And then the other side hates that stuff and loves getting
 the hands-on
 kind of organic natural stuff that has uncertainty and is
 practical.
 And we're talking about it, and it's quite clear that they
're two very, very different people.
 So science understands that, and they have this left brain,
 right brain kind of thing,
 genetics and the idea that people are different.
 But from a Buddhist point of view, it's also interesting to
 see how different human beings,
 how different two daughters can be, you know, two siblings
 can be.
 And obviously we see that in people.
 So we're born differently and we would attribute this at
 least partially to past karma.
 But the idea of the quality of brilliance, like some people
 have great memory,
 I have great memory, it just happens to be a trait.
 And this other monk who ordained after me,
 but he's been a meditation teacher for quite some time in
 our tradition,
 he has a terrible memory and yet he's a brilliant teacher
 and he's now, I think, world-renowned, he's a great guy.
 He's also on YouTube a little bit. People put his videos on
 offer.
 It's just real, but terrible memory and he admits it.
 So we're comparing this and it's just interesting how we
 have different qualities of mind, different abilities.
 And so definitely we attribute this, we speculate on the
 fact that this is probably something to do from the past.
 Karma really isn't, you know, that's not really the
 important questions about karma that we ask.
 Most important questions or knowledge that we try to,
 understanding we try to cultivate,
 is about karma right here and now, what it's doing to us,
 how it's changing us.
 The idea that you might be reborn in a better place, in a
 better way, is kind of incentive to help you do that, to
 help you understand.
 If people knew what their actions were leading them to,
 then they'd be a lot more concerned about learning about
 them, about studying about them.
 So the important thing is that you come to the present
 moment and study your karma.
 But by telling people about past and future lives, it helps
 them see the importance of doing that.
 So if there is a future life and if your karma, your state
 of mind is going to influence that,
 gee, you'd better come and look and see whether what you're
 doing is the right thing to do and learn more about how to
 do the right things.
 So it's a lot more basic, preliminary stuff to talk about
 karma in terms of past lives, future lives and so on.
 The ultimate goal is to understand karma as it works here
 and now,
 how our bad deeds are making us bad people, are corrupting
 our minds,
 and how our good deeds are purifying our minds, are making
 us calmer, more peaceful, more gentle and beneficial beings
.
 But yeah, so definitely place a place apart.
 It's most likely to play a part, it seems reasonable to say
 that,
 and that's the Buddhist theory, is that past lives do
 condition our lives in the present and present will
 condition us.
