1
00:00:00,000 --> 00:00:07,400
 Okay, so do the realizations of impermanence, suffering and

2
00:00:07,400 --> 00:00:09,960
 non-self have to be, or are

3
00:00:09,960 --> 00:00:14,400
 they supposed to be intellectual?

4
00:00:14,400 --> 00:00:25,970
 No, and in fact, in a sense, the experience of impermanent

5
00:00:25,970 --> 00:00:30,960
 suffering and non-self is the

6
00:00:30,960 --> 00:00:32,680
 absence, right?

7
00:00:32,680 --> 00:00:38,530
 Because they're all negative aspects or negative

8
00:00:38,530 --> 00:00:41,080
 characteristics.

9
00:00:41,080 --> 00:00:48,920
 It's not like non-self comes out and hits you in the face.

10
00:00:48,920 --> 00:00:55,930
 But what it is, is it's the giving up of the belief that

11
00:00:55,930 --> 00:00:59,600
 something is permanent.

12
00:00:59,600 --> 00:01:03,280
 That may come in your meditation that you intellectualize

13
00:01:03,280 --> 00:01:05,520
 or not exactly intellectualize,

14
00:01:05,520 --> 00:01:07,720
 but the thought arises.

15
00:01:07,720 --> 00:01:12,760
 Wow, this is all impermanent.

16
00:01:12,760 --> 00:01:18,520
 This thought arises, like a realization.

17
00:01:18,520 --> 00:01:21,080
 But that's not the insight.

18
00:01:21,080 --> 00:01:22,560
 That's not the moment of insight.

19
00:01:22,560 --> 00:01:35,240
 It often follows, it's preceded by a moment of insight.

20
00:01:35,240 --> 00:01:41,360
 The moment of insight is the seeing for yourself.

21
00:01:41,360 --> 00:01:44,040
 For instance, in the case of impermanence, it's seeing

22
00:01:44,040 --> 00:01:45,080
 something cease.

23
00:01:45,080 --> 00:01:51,230
 It's the continued realization, the continued observation

24
00:01:51,230 --> 00:01:54,280
 of the cessation of phenomena.

25
00:01:54,280 --> 00:02:02,790
 For suffering, it's seeing the intolerability of the

26
00:02:02,790 --> 00:02:04,560
 phenomena.

27
00:02:04,560 --> 00:02:06,390
 Especially in terms of seeing that the things you thought

28
00:02:06,390 --> 00:02:07,440
 that were going to make you happy

29
00:02:07,440 --> 00:02:08,680
 are not going to make you happy.

30
00:02:08,680 --> 00:02:11,720
 You see them arising and ceasing, arising and ceasing.

31
00:02:11,720 --> 00:02:16,040
 You lose the idea that they are sukha.

32
00:02:16,040 --> 00:02:22,730
 So in a sense, it's giving up, rather than it's giving up

33
00:02:22,730 --> 00:02:25,840
 of a belief of the opposite

34
00:02:25,840 --> 00:02:29,560
 characteristic.

35
00:02:29,560 --> 00:02:32,760
 When you see things as they are, you give up the belief in

36
00:02:32,760 --> 00:02:34,640
 permanence, in satisfaction,

37
00:02:34,640 --> 00:02:41,020
 or that things can be pleasant when you cling to something

38
00:02:41,020 --> 00:02:44,720
 that can bring you happiness.

39
00:02:44,720 --> 00:02:46,840
 Number three, giving up the idea that you can control.

40
00:02:46,840 --> 00:02:51,280
 So we try to control our breath, the stomach for example.

41
00:02:51,280 --> 00:02:53,140
 So in the beginning, you're just going to keep controlling

42
00:02:53,140 --> 00:02:54,080
 the stomach no matter what

43
00:02:54,080 --> 00:02:55,080
 you do.

44
00:02:55,080 --> 00:02:57,760
 You'll be practicing rising, falling.

45
00:02:57,760 --> 00:03:02,170
 Here you are forcing the breath at every in-breath, every

46
00:03:02,170 --> 00:03:05,200
 out-breath, forcing the stomach.

47
00:03:05,200 --> 00:03:11,810
 You start to see that it's not really forcing the breath at

48
00:03:11,810 --> 00:03:12,720
 all.

49
00:03:12,720 --> 00:03:14,680
 There's this conception of the forcing.

50
00:03:14,680 --> 00:03:19,170
 But eventually what you'll see is that all you're doing is

51
00:03:19,170 --> 00:03:20,840
 creating stress.

52
00:03:20,840 --> 00:03:24,900
 You're not actually controlling the breath, you're just

53
00:03:24,900 --> 00:03:26,080
 creating more stress.

54
00:03:26,080 --> 00:03:29,380
 The breath is arising based, or the stomach is arising

55
00:03:29,380 --> 00:03:31,920
 based on many causes, many conditions,

56
00:03:31,920 --> 00:03:35,360
 only one of which is the mind.

57
00:03:35,360 --> 00:03:37,960
 But the mind is responsible for creating stress, which is

58
00:03:37,960 --> 00:03:39,600
 why when you try to control it, you

59
00:03:39,600 --> 00:03:44,160
 just feel more and more terrible, more and more unpleasant.

60
00:03:44,160 --> 00:03:47,310
 Until finally you realize that it's not really, you can't

61
00:03:47,310 --> 00:03:49,320
 just say, because, I mean, think

62
00:03:49,320 --> 00:03:50,320
 about it.

63
00:03:50,320 --> 00:03:55,460
 This is what we don't realize if you could make it, if it

64
00:03:55,460 --> 00:03:58,400
 was under your control, then

65
00:03:58,400 --> 00:04:01,070
 why can't you just say, "Okay, my breath is just going to

66
00:04:01,070 --> 00:04:02,440
 rise smoothly, it's going

67
00:04:02,440 --> 00:04:06,080
 to fall smoothly."

68
00:04:06,080 --> 00:04:07,680
 That's what we think we can do.

69
00:04:07,680 --> 00:04:14,250
 But it's totally off the wall, this idea that we can do

70
00:04:14,250 --> 00:04:19,080
 that, because it's totally unrelated

71
00:04:19,080 --> 00:04:22,080
 to the facts.

72
00:04:22,080 --> 00:04:24,400
 And eventually you realize this.

73
00:04:24,400 --> 00:04:26,620
 So it's not intellectualizing like I just did, I just had

74
00:04:26,620 --> 00:04:28,800
 you intellectualize and say,

75
00:04:28,800 --> 00:04:31,720
 as the Buddha did, well, if you could, then why can't you

76
00:04:31,720 --> 00:04:33,160
 control it to be like this or

77
00:04:33,160 --> 00:04:34,160
 like that?

78
00:04:34,160 --> 00:04:36,160
 It's not like that at all.

79
00:04:36,160 --> 00:04:42,120
 The realization is visceral, it's actually seeing the

80
00:04:42,120 --> 00:04:46,900
 breath arise and cease on its own.

81
00:04:46,900 --> 00:04:49,380
 And the clear perception of non-self is when you actually

82
00:04:49,380 --> 00:04:50,160
 see like that.

83
00:04:50,160 --> 00:04:52,590
 It actually appears to you that the body is moving on its

84
00:04:52,590 --> 00:04:53,920
 own, it appears to you that

85
00:04:53,920 --> 00:04:55,880
 thoughts are arising on their own.

86
00:04:55,880 --> 00:04:58,130
 You're able to see this and you're able to realize that

87
00:04:58,130 --> 00:05:00,120
 really that's all there is,

88
00:05:00,120 --> 00:05:04,150
 is phenomena arising and ceasing, and you give up any

89
00:05:04,150 --> 00:05:05,800
 desire to control.

90
00:05:05,800 --> 00:05:13,520
 The whole idea of control just goes out the window.

91
00:05:13,520 --> 00:05:15,800
 And that's when the realization of non-self.

92
00:05:15,800 --> 00:05:18,840
 So the mind lets go, when the mind lets go, then there's

93
00:05:18,840 --> 00:05:20,400
 freedom from suffering.

94
00:05:20,400 --> 00:05:21,040
 That's how it should be.

