 This is the classic question on the fourth precept.
 Say you were strictly following the precepts, you see a
 terrified man run past you,
 then another man comes up to you with a knife and asks if
 you seen where the first man went.
 Although you know, although you know, would it be
 appropriate to lie and say you don't know never seen him,
 or would you have to tell the truth?
 This is always given as an example of how quote-unquote the
 precepts are a guy
 not a rule.
 People believe that
 there are instances where breaking the precepts is
 appropriate and they have all sorts of,
 there's even better ones than this you could come up with,
 but this is the classic one.
 But it was a silly one
 and please don't take this personally because this is the
 classic question.
 And so someone, this was explained, I was sitting in a
 group and this monk was explaining this and
 I said to him well,
 couldn't you just, couldn't you just not say anything? And
 he was like, oh, yeah, I guess you could.
 Because keeping the precepts doesn't mean you have to say
 stuff.
 Keeping the precepts means
 not doing things. I mean, so suppose this guy even says
 tell me where he went or I'll beat you up.
 And you say,
 you're welcome to beat me up if you like.
 It's really none of my concern.
 Something like that and then they might beat you up and
 that's it. They might kill you and so on.
 It doesn't mean that you have to tell a lie.
 Doesn't mean you have to tell the truth.
 That's really the proper thing to do. There's an
 interesting article I read recently
 by Sam Harris and probably some of you know who Sam Harris
 is.
 He's an atheist.
 They don't like to be called that. I'm an atheist too.
 But by atheist, I mean he's someone who talks a lot about
 atheism among other things.
 But it's something that he's well known for his views on
 theism and the silliness of it all.
 And he wrote an article recently about self-defense and it
 got me thinking because
 he's going on and on about how you have to,
 you have to react and you have to
 don't go along with things.
 When you're being attacked, don't go along with it. And at
 first I didn't agree with it.
 But I think some of it's agreeable because self-defense is
 for sure allowable. You don't want to
 if someone's trying to rape you or so and you don't want to
 have them rape you because
 you're not going to agree with it.
 The reason why you don't want them to rape you is because
 it's bad for them.
 Which many people find difficult to understand but
 the reason why you don't want people to murder you is
 because it's bad for them.
 That's I think a really good excuse not to let it happen.
 Another good excuse is because you want to live yourself so
 that you can do good stuff.
 And you think if I die that I don't know where I'm going to
 go.
 I'm not enlightened yet and so better I stick around for a
 while to do more good deeds.
 There's lots of good reasons for defending yourself.
 But
 I think there still is some
 some case for
 especially an extreme example. The example that I thought
 of was of this monk who was in
 Rajagaha.
 There's a Theravada monk who was going walking through
 India barefoot.
 And he had his lay attendant with him
 carrying the money and
 they decided that they would camp out on
 Gijakuta.
 Gijakuta Vulture's Peak, which is a very famous place where
 the Buddha lived and
 many of the great disciples lived.
 It's kind of like the mount in the Sermon on the Mount. It
's a very important place in Buddhism.
 So I thought cool. We'll just camp out up here. Big mistake
. Don't camp in Bihar.
 I don't know if it's better now, but the idea of camping in
 Bihar is ridiculous.
 Tourists get
 murdered in Bihar. It's a very poor province and
 lots of bandits and so on. And so indeed
 they were confronted by bandits. Actually Gijakuta is safe
 now, I think.
 There's guys with guns guarding Gijakuta maybe because of
 things like this happening. But you know anyway at night
 I don't think the guards stay, stick around. And so at
 night they were confronted by these bandits.
 And
 the
 lay attendant, the guy who was walking with him,
 charged them and
 ran right, broke right through them, scuffled with them a
 little bit and ran away and jumped down from the top of Gij
akuta into the ravine.
 You know like
 not breaking anything but scratching himself terribly and
 bruising himself.
 And
 and the monk,
 the monk sat there and they said we're going to kill you
 now.
 What the monk said that he did is he just,
 they had a knife pointed at his neck and he just lifted up
 his neck like this to give them the,
 to give them his flesh like okay, if you're going to do it,
 go ahead and do it. And
 the point is that they didn't kill him.
 They,
 the point is that there's a power in that. There's a power
 in letting go.
 That very often makes you immune to, to,
 to evil.
 And as a result it was, it would have been quite difficult
 for them to hurt him.
 It would have been quite easy for them to hurt the other
 guy because he was totally unmindful and he had lost his
 presence of mind.
 They didn't kill him and the, the guy that jumped down in
 the ravine, the story goes that he
 realized that he had left this monk defenseless and so he
 climbed back up and
 surrendered himself and they had to give up all their
 clothes and robes and bowls and money and
 camera and everything. All they left him, the monk with was
 his lower robe so like a sarong.
 But they left them alive and somehow they, they survived
 and got out of it and
 wanted to stay in a Thai monastery and had a happy ending
 in the end.
 But
 anyway, that's not exactly
 the question you're asking but
 you certainly don't have to lie and
 in the case where you're forced to do something unethical,
 just don't do it
 regardless of the consequences because people always make
 up these things. Well, what if, if they, if you, if you don
't do it,
 they're going to do this.
 But, you know, the point is what you're saying is that if
 you don't do something unethical, somebody else is going to
 do something unethical and
 you're not responsible for their, their
 unethical behavior. It's not your
 bad karma if someone else does something bad. It would only
 be an intellectual exercise if you said
 "I
 should do something bad because otherwise these people are
 going to do something worse."
 The, the ethics of it are quite clear. You're not doing
 anything wrong and
 so from a point of view of reality,
 there's no need ever to do something bad in order to avert
 badness.
 You, you're not responsible for other people's acts.
 If, and knowing that people go according to their karma and
 so on. It's, it's quite a difficult,
 I mean, it's quite clear in my mind, but it's, I have
 trouble expressing this because I know people feel quite
 strongly like if your
 family was involved, people would be very much attached to
 their family, but it goes totally against the, against
 reality actually and it falls into this
 conventional reality of, of
 relationships and our attachment to other people
 missing the whole point of how karma works and how people
 go according to their deeds and their relationships with
 each other.
 If someone's going to kill someone else, it has much more
 to do with their relationship as two individuals than your
 intervention and so on. So for sure
 there, there's, there's never any reason to do
 unwholesomeness
 because the, the,
 that is your action and that is your
 that is your input into the universe, regardless of what
 someone else is going to do.
 Anyway, so.
