 Okay, then let's get started.
 Welcome everyone to a fine Sunday afternoon in Second Life.
 Everybody get yourself comfortable both here in virtual
 world and at home.
 I'd say that it's equally important that you're focused at
 home.
 It's more important that you're focused at home than that
 you're focused here.
 If your avatar here is shifting and shuffling, that's not
 really a problem.
 But at home you should try to.
 If you can while you're watching this, try to settle
 yourself down.
 Turn Facebook off.
 Get yourself too busy if it helps.
 I'm just going to talk for a little while and then if
 anyone has any questions, I'm
 happy to answer them.
 The topic I'd like to talk about today has to do with the
 very core of the Buddhist
 teaching.
 No use beating around the bush and no use giving
 complicated roundabout or exotic teachings.
 It's cut right to the chase.
 When we talk about the core of Buddhism, we always think of
 the Four Noble Truths.
 I think because it's a good summary of the Buddhist
 teaching.
 But there's a teaching that goes even more to the core than
 that.
 That's the Buddhist teaching on dependent origination.
 That's what I'd like to talk about today.
 I know I've talked about it before elsewhere.
 I can't remember if I've talked about it here on Second
 Life, but it's always good
 to go over things again.
 We take the Buddhist teaching in its entirety as an
 instruction in the practice of meditation,
 in observing reality and coming to understand it.
 When you listen to me give a talk here, it's important that
 you're mimicking or following
 after the example of the people who listen to the Buddha
 himself teach.
 That is, when they listen to the Buddha's teaching, they
 would also practice meditation.
 They would take it as an opportunity to look inside
 themselves and to apply the teachings
 as a reminder for how to approach reality.
 Here we can do the same.
 You don't have to look at your computer screen.
 You can close your eyes.
 If it helps you to keep on track to look at the screen,
 fine.
 But try to really appreciate the teachings on a more
 practical level.
 Because in the time of the Buddha, many people, the example
 that they said is that they could
 gain states of realization during the time that they were
 listening to the Buddha's teaching.
 Simply by applying it and using it to calm the mind, to
 restrain the mind and calm the
 mind and to eventually understand the workings of the mind.
 They could even enter into a state of enlightenment, go
 into the realization of nibbana and so
 on.
 Take that as an example here.
 This teaching especially is an incredibly useful and
 practical teaching, the teaching
 of dependent origination.
 It's the most profound, I would say the most profound
 statement of reality that exists.
 I can't think of anything else that I've heard more
 profound, even within the Buddha's teaching.
 For me the most profound part is the very beginning of the
 teaching.
 It's these three words in the Pali that really brought
 light to the world.
 Before the uttering of these three words, "Avitja" by Jaya
 Sankara.
 Before the Buddha realized this, that "Avitja" or "ignor
ance" is a cause for the arising
 of formations or with ignorance as a cause there arise
 formations.
 Anybody enlightened yet?
 It's probably not that easy.
 It's a profound teaching and it's something that's very
 difficult for us to understand,
 very difficult to comprehend this teaching.
 It often goes over our heads.
 We think of it as some sort of philosophical teaching.
 Ignorance leads to formations.
 Formations here, just to explain, the word "formations,"
 what we're talking about here
 is our mental formations, our ideas about things, our
 thoughts, what we think of something,
 our mental volition.
 When we want to hurt someone, when we want to attain
 something, when we get angry, when
 we get greedy, when we get attached, addicted, when we're
 afraid or worried, and so on.
 All of these mental states that are a reaction to something
, these arise, the Buddha said,
 based on ignorance.
 And this is really a profound teaching that deserves the
 full of our attention.
 If we can understand just this, just these three words, if
 we can realize this in our
 meditation practice, this is the state of enlightenment
 that we're looking for.
 And the problem is that for most of us, we don't think this
 way.
 When we get angry or when we become addicted to something,
 we say to ourselves or we say
 to other people, "I know it's wrong.
 I know it's not good to get angry, but I can't help myself.
 I know it's not good to become addicted to sweet foods or
 so on, but I can't help myself."
 It's not that I don't know, it's that I'm unable to change
 it.
 I'm unable to avoid the judging, the emotion.
 And so the Buddha denied this.
 He denied that this is the case.
 He taught the exact opposite.
 He said, "No, you don't know.
 You don't know that it's wrong."
 You say, "Yes, I know that it's wrong."
 What you really mean is that someone told you that it's
 wrong, you don't like the results
 that come from it, but you don't really understand that it
's wrong.
 This is how we approach everything in our lives with
 ignorance, with an incredibly superficial
 awareness.
 Even imagine yourself, look at yourself, how you're sitting
 right now, listening to my
 talk.
 You're seeing things, you're hearing things, you're
 smelling, you're tasting, you're feeling,
 you're thinking.
 And all of this is happening very quickly.
 And when you see something, you immediately start to judge
 it, immediately start to assess
 it.
 "This is beautiful, this is nice," or, "This is ugly," or,
 "This is terrible, horrible,"
 whatever it is.
 When you hear something, you immediately start to judge it.
 Maybe you like the birds in the background, maybe they're
 too loud, too noisy.
 Maybe that repetitive cricket noise is driving you crazy.
 We don't really see and we don't really hear, we don't
 really understand the experience.
 And we don't really understand our reaction to the
 experience.
 When we see something, we think, "I see.
 I'm seeing this," and we think, "I like it," and we think,
 "It is good."
 We have all of these preconceived notions that are totally
 disconnected from the reality
 of the experience.
 They're generally bound up in our habits, our accustomed
 way of responding to things.
 We remember that certain things bring us pleasure and so we
 respond in that manner.
 We think something's going to make us happy.
 And it's a habitual response, the response of, say, an
 ordinary animal.
 So all we're trying to do in meditation is to look deeper
 at things.
 When you're sitting here listening to the talk, someone
 walks into the room and starts
 making loud noise, right away you get angry at it.
 Maybe there's a little kid making noises, yelling, pester
ing you for this or that.
 And you're ready to get angry, get irritated.
 You can even get to the point where you want to yell at
 them.
 It's very quick, it's very easy to do that.
 And the only reason that you do it, it's not that you're a
 bad person, it's that you're
 ignorant.
 You don't really understand what happened.
 You weren't watching.
 You weren't clear on the experience.
 You misunderstood it.
 And so you followed after it.
 You reacted inappropriately and you caused suffering for
 other people and for yourself.
 You feel guilty, you feel upset, you feel angry.
 So this is the most important point of the Buddhist
 teaching is that ignorance is the
 cause of our reactions to things, our judgments, our
 improper reactions, our improper
 modes of behavior, modes of responding to the stimulus that
 come to us.
 And so the Buddha tried to describe to us in his teaching
 what it was that he realized,
 the detailed explanation of what's going on so that when we
 practice meditation we can
 see things clearer.
 You ask, "Well, okay, so I'm ignorant.
 What is it that I'm ignorant of?
 What is it that I don't understand?"
 And the truth is if you spend some time looking at reality,
 you'll see there's a lot that
 you don't understand.
 There's a lot that you weren't aware of.
 You'll see that if you just took the time to really see
 what's going on when this young
 child is pestering you, when this loud noise is bothering
 you, when it's too hot, when
 it's too cold, when you have pain in the body and so on.
 If you just took the time to look at it, you'd see there's
 so much more going on than you
 thought.
 And at the same time, the experience is so much less than
 you thought.
 There's nothing unpleasant about it at all, that we've got
 a totally wrong understanding
 of the experience, that surprisingly there's nothing
 unpleasant about it at all.
 You can be in incredible pain.
 And when you really understand the pain, when you really
 see what's going on, it doesn't
 bother you at all.
 You become surprised that you were ever upset by it in the
 first place.
 You say to yourself, you can't believe that you were
 addicted to this.
 It's an epiphany of sorts, that you suddenly realize that
 there's nothing wrong with reality.
 There's nothing wrong with the way things are.
 It is the way it is.
 What's wrong is the way we respond, the way we react to it.
 So the Buddha taught us to go into more detail and he
 explained what's really going on.
 And this is in the rest of the exposition on the dependent
 origination.
 So to go through it in brief, what's really going on is
 that in the world, in the universe,
 in the ultimate reality, there are two things, there are
 two aspects of experience.
 And they're sort of like two sides of the same coin.
 They're distinct, but they're a pair.
 And these are the physical and the mental.
 That in the universe, all of our experience can be summed
 up under the physical and the
 mental.
 When we see something, this is the light touching the eye.
 The eye is physical, the light is physical.
 When we hear something, this is the sound touching the ear
 and these are both physical.
 Smells and the nose, tastes and the tongue, feelings in the
 body, these are all physical.
 And the mental side is the knowing of the object, the
 perception of it.
 When our mind is at the eye, then we see.
 When our mind is at the ear, then we hear.
 But sometimes the ear might be there and the sound might be
 there, but our mind is somewhere
 else and so we fail to hear the things that people say to
 us.
 It's difficult to see if you're not really focusing, but
 sometimes when you're using
 the computer, you can find that.
 You're focusing so much on something that you don't hear
 someone talking to you.
 You don't know what it was that they said.
 And these things in and of themselves are not a problem
 obviously.
 The mind knows the object, the object arises, the mind
 knows it.
 But what happens next is there arises a feeling.
 The body and the mind, it comes together at the eye, the
 ear, the nose, the tongue, the
 body or the mind.
 In the mind there's only the thought, there's only the mind
.
 But we have the body and the mind coming together or else
 just the mind thinking itself.
 At the moment of experience, there arises a feeling.
 You can verify this.
 When you see something, if it's a good thing, right away
 you feel happy about it.
 And you can see this if you're really focusing on it.
 So for instance, when we see something and we say to
 ourselves, seeing, seeing, seeing,
 we can catch when we feel happy about it or when we feel
 unhappy about it.
 When we hear something hearing, hearing, we can catch the
 feeling that there's a feeling
 first.
 There's a pleasant feeling or an unpleasant feeling or a
 neutral feeling.
 And these feelings in and of themselves aren't a problem
 either.
 There's nothing inherently unwholesome about a happy
 feeling or an unhappy feeling.
 A pleasant or an unpleasant feeling, it's a physical
 response to a stimulus.
 Since we feel pain in the body, there's nothing wrong with
 that.
 There's nothing unpleasant about it and nothing unwholesome
 about it.
 And by the same token, there's nothing unwholesome about a
 pleasant feeling.
 So many people when they hear that they're instructed to
 acknowledge the happy feelings,
 they get the wrong impression that we're trying to do away
 with happiness.
 It's wrong to feel happiness.
 And this isn't at all the case, but we want to understand
 the happiness.
 We want to see it for what it is.
 Because it's the feelings when unacknowledged, when
 misunderstood.
 If there's ignorance about the feeling that this is what's
 going to give rise to craving.
 This is what gives rise to our likes and our dislikes.
 This teaching, if you haven't ever practiced meditation, it
 might seem quite foreign.
 It might seem quite even uninteresting.
 It's very difficult to understand.
 But this is an incredibly useful teaching when you're
 practicing meditation.
 When meditators will be at a loss as to how to deal with
 strong emotions that come up.
 When they really are attached to something or when they're
 really angry about something.
 When they're really distracted and unfocused.
 When they're worried or stressed, depressed, bored, afraid,
 whatever.
 And they don't know how to deal with it.
 What the Buddha is doing here is breaking that experience
 up.
 What happens when you're angry?
 What happens when you're attached to something?
 And when you break it up, you can see that there's nothing
 really worth attaching to
 at all.
 When you feel happy, it's just a happy feeling.
 There's nothing positive or negative about it.
 It is what it is.
 You can see that when you cling to it, when you say this is
 good, you're not going to
 prolong it.
 You're just going to create a need for it.
 An attachment to it.
 It's not like you can say, "Oh, I like this.
 Therefore it's going to stay longer.
 It's going to stay longer than if I didn't like it."
 Because it's exactly the case with negative emotions.
 That you can't make them go away just because you don't
 want them to be there.
 Negative experience doesn't disappear just because you want
 it to go.
 Negative experience doesn't stay just because you want it
 to stay.
 When we come to see this, we come to see the nature of
 these things is that they're impermanent.
 They're unsure, uncertain.
 They come and go according to their own nature, according
 to the causes and effects that created
 them.
 You can pick any one of these parts, the object of your
 desire or the object of your aversion.
 You can pick the feeling that it gives rise to inside of
 you or you can pick the emotion
 that arises.
 The important thing is that you pick it apart and see it
 clearly and you're focusing on
 something that's real.
 Because just saying, "I'm addicted and that's that and I
 can't stop myself," isn't at all
 useful in any way.
 To simply say that I'm an angry person also isn't useful.
 It's not really understanding what's happening.
 It's not seeing clearly what's going on.
 Once you can pick it apart, if you can catch yourself at
 the emotion, at the feeling, if
 you feel pain or so on.
 Once you see it clearly, then you find no reason to get
 upset about it.
 Instead of saying, "This is bad.
 This is painful," you just say, "This is this.
 This is what it is."
 When there's pain, you know that there's pain.
 When there's a pleasant feeling, instead of getting
 addicted to it, suppose it's good
 food or a beautiful sight, you're simply aware that it is
 what it is.
 It's a sight and it's a happy feeling that arises.
 You don't see any reason to become addicted or attached to
 it.
 It doesn't make it last, as I said.
 It doesn't do you any good and all it does is lead to
 suffering when it's gone.
 Because the alternative is to live our lives as ordinary
 people who are uninterested in
 mental development, live their lives, happy sometimes,
 miserable sometimes, even to the
 point where they try to kill themselves sometimes, having
 to go through incredible stress and
 suffering because they don't understand the experience of
 reality in front of them.
 It's not because their situation, there's anything wrong
 with it.
 It's that they don't understand what's happening.
 They don't understand the nature of their experience.
 They attribute it to being me and mine and they attribute
 the idea that it somehow should
 be forced and controlled and changed.
 We segregate reality into the good and the bad, the
 acceptable and the unacceptable.
 And actually all there is is the physical and the mental
 and the feelings that arise.
 The problem that comes is when we react.
 The problem is not in the objects themselves.
 When we crave for something, when we need for something,
 when we require that things
 be other than what they are or when we require that things
 stay the way they are and not
 change.
 Simply put, when we require things to be other than reality
 dictates.
 When reality dictates that things must change and we
 require that it to be otherwise, this
 is where suffering comes from.
 We cling to it.
 We say that it must be, we require it to be other than this
.
 We're not satisfied the way things are.
 We have to go and seek out more.
 We don't understand and see it for what it is.
 We think it's unpleasant or it's bad or we think that this
 is going to make me happy.
 If I can just attain this, get this or that object.
 And so we cling.
 We refuse to accept change.
 Refuse to accept things the way they are.
 And this is what gives rise to suffering.
 This is what sets us on a cycle of addiction or obsession.
 This may be a better word.
 Needing it to be like this, needing it not to be like that.
 And the suffering that comes when it's not the way we want
 it to be.
 We don't see this in ordinary everyday life.
 We don't see this when we're not observing, when we're not
 meditating.
 All we see is the suffering that comes from things not
 being the way we want.
 Even right now, I'm sure there's many things going on in
 your experience that are unpleasant.
 You're trying to change them.
 Sitting here, maybe it's too hot, maybe it's too cold.
 Maybe the seat is too hard and you have to shift your
 position.
 Maybe you don't like what I'm saying and it makes you upset
 and gives you a headache
 or so on.
 It's our inability to see these things clearly, to see what
's really going on that leads us
 to obsession and to suffering.
 Once we look at it, we see how amazing reality really is
 and how amazing mindfulness really
 is.
 Simply seeing things for what they are.
 Seeing things for what they are.
 In a moment, you can do away with any suffering that arises
.
 You feel stressed.
 You focus on the stress.
 Just penetrate into it.
 What's going on here?
 What's happening?
 What does it mean to say, "I am stressed.
 I'm upset.
 Where's the eye?
 Where's the stress?
 What's really going on?"
 You just say to yourself, "Stressed, stressed, stressed."
 Keeping your mind with it and seeing it simply for what it
 is.
 You realize there is no eye involved.
 There's only a feeling of stress that arises.
 When you see that there's nothing intrinsically wrong with
 this tense state, it is what it
 is.
 It's something that's arisen and after some time will
 disappear.
 When you want something or when you're angry about
 something, whatever the emotion is,
 whatever is causing you stress and suffering, whatever is
 getting in the way of your clear
 understanding, your peace, your peaceful harmony with
 reality, you penetrate into it.
 You see it for what it is.
 You see that there's many things going on.
 You have happy feelings.
 You have negative, unpleasant feelings.
 You have these states of greed and anger.
 They come and they go.
 When you can see and understand these things, then you can
 say to yourself, "I know it's
 wrong and that's why I don't do it."
 You'll never say to yourself, "Again, I know it's wrong but
 I still do it."
 You'll come to realize that you really don't know what's
 wrong with it.
 You really don't know the true nature of the experience and
 why it's wrong to get angry
 because when you really know that it's wrong to get angry
 or greedy or so on, you won't
 do it.
 When you know that it's wrong to carry out some behavior,
 when you truly have rizya or
 knowledge, understand the situation, you'll never cause
 suffering for yourself again.
 So that was the teaching that I thought to discuss today.
 I hope that was useful for some people, sort of as a guide
 for where you should be going
 in your meditation.
 Thanks to everyone for coming and if you have any questions
, I'm happy to take them now.
