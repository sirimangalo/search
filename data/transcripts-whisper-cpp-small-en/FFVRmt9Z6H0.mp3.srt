1
00:00:00,000 --> 00:00:05,080
 Hi, in this video I will close my series on how to meditate

2
00:00:05,080 --> 00:00:08,040
 with an explanation of the

3
00:00:08,040 --> 00:00:11,540
 fundamentals of meditation practice, the things which it's

4
00:00:11,540 --> 00:00:13,520
 necessary to keep in mind during

5
00:00:13,520 --> 00:00:16,600
 the time we practice, to ensure that our practice is

6
00:00:16,600 --> 00:00:19,120
 correct and that our practice will indeed

7
00:00:19,120 --> 00:00:21,580
 bring fruit.

8
00:00:21,580 --> 00:00:25,460
 The four fundamentals of meditation practice as described

9
00:00:25,460 --> 00:00:27,320
 in this series of videos are

10
00:00:27,320 --> 00:00:34,240
 one, the present moment, and two, continuity, three, the

11
00:00:34,240 --> 00:00:38,680
 acknowledgement, the clear thought,

12
00:00:38,680 --> 00:00:44,040
 and four, balancing the mental faculties.

13
00:00:44,040 --> 00:00:47,420
 These four fundamentals are necessary parts of the

14
00:00:47,420 --> 00:00:49,680
 meditation practice so it's not enough

15
00:00:49,680 --> 00:00:52,550
 that we simply do the prostration, we do the walking, we do

16
00:00:52,550 --> 00:00:54,000
 the sitting and expect somehow

17
00:00:54,000 --> 00:00:59,050
 to become free from suffering or free from the unpleasant

18
00:00:59,050 --> 00:01:02,200
 stressful condition which exists

19
00:01:02,200 --> 00:01:04,600
 inside of ourselves.

20
00:01:04,600 --> 00:01:07,410
 The first one, the present moment, means that at the time

21
00:01:07,410 --> 00:01:08,960
 that we practice we have to have

22
00:01:08,960 --> 00:01:12,080
 presence, we have to be here and be now.

23
00:01:12,080 --> 00:01:14,680
 We can't be acknowledging after the fact or acknowledging

24
00:01:14,680 --> 00:01:15,600
 before the fact.

25
00:01:15,600 --> 00:01:19,710
 So when we walk or when we do the prostration or when we

26
00:01:19,710 --> 00:01:22,160
 sit we have to acknowledge as it

27
00:01:22,160 --> 00:01:23,160
 happens.

28
00:01:23,160 --> 00:01:27,270
 So when the belly rises we have to say to ourselves as it

29
00:01:27,270 --> 00:01:29,560
 rises, rising, and when it

30
00:01:29,560 --> 00:01:33,960
 falls we have to say to ourselves, falling as it falls.

31
00:01:33,960 --> 00:01:36,500
 Whether we're doing prostration, doing walking, or doing

32
00:01:36,500 --> 00:01:37,920
 sitting, we have to keep our mind

33
00:01:37,920 --> 00:01:41,920
 in the present moment and acknowledge as the object appears

34
00:01:41,920 --> 00:01:44,040
, as the object occurs in front

35
00:01:44,040 --> 00:01:45,360
 of us.

36
00:01:45,360 --> 00:01:46,800
 This is the first fundamental.

37
00:01:46,800 --> 00:01:51,840
 The second fundamental is that we practice continuously.

38
00:01:51,840 --> 00:01:54,600
 So it's not enough to say that in the morning we're going

39
00:01:54,600 --> 00:01:56,440
 to do 10 minutes walk, mindful

40
00:01:56,440 --> 00:01:59,490
 prostration, then 10 minutes walking, 10 minutes sitting,

41
00:01:59,490 --> 00:02:00,960
 and in the evening at night we're

42
00:02:00,960 --> 00:02:01,960
 going to do the same.

43
00:02:01,960 --> 00:02:04,840
 But during the day we're not going to be mindful.

44
00:02:04,840 --> 00:02:09,220
 Now for this technique to work this has to be a basis for

45
00:02:09,220 --> 00:02:10,320
 our life.

46
00:02:10,320 --> 00:02:14,590
 Our life has to be a continuous series of creating clear

47
00:02:14,590 --> 00:02:17,080
 thought about the situation.

48
00:02:17,080 --> 00:02:20,170
 So when we meet with situations in our daily life, when

49
00:02:20,170 --> 00:02:21,960
 people are getting upset at us

50
00:02:21,960 --> 00:02:24,780
 or people saying things which make us upset, that we're

51
00:02:24,780 --> 00:02:26,520
 able to say to ourselves, "Upset,

52
00:02:26,520 --> 00:02:30,170
 upset," or simply hearing, hearing, hearing, we have to

53
00:02:30,170 --> 00:02:31,720
 practice continuously.

54
00:02:31,720 --> 00:02:35,510
 But most important is during the time we're practicing that

55
00:02:35,510 --> 00:02:37,160
 it is also continuous.

56
00:02:37,160 --> 00:02:41,030
 So when we do the mindful prostration, we're continuously

57
00:02:41,030 --> 00:02:43,080
 mindful of the movement of the

58
00:02:43,080 --> 00:02:44,080
 hands.

59
00:02:44,080 --> 00:02:47,360
 So that we go right away to do walking meditation.

60
00:02:47,360 --> 00:02:50,000
 Walking meditation should be done maybe 10 minutes, 15

61
00:02:50,000 --> 00:02:51,520
 minutes, sitting meditation also

62
00:02:51,520 --> 00:02:53,360
 10 minutes, 15 minutes.

63
00:02:53,360 --> 00:02:55,320
 Have them equal.

64
00:02:55,320 --> 00:02:56,640
 But have them connected.

65
00:02:56,640 --> 00:02:59,640
 So continuity means that after we do the prostration, we do

66
00:02:59,640 --> 00:03:00,440
 the walking.

67
00:03:00,440 --> 00:03:01,920
 After we do the walking, we do the sitting.

68
00:03:01,920 --> 00:03:05,360
 If we want to continue, we can do walking, sitting, walking

69
00:03:05,360 --> 00:03:07,000
, sitting continuously if we

70
00:03:07,000 --> 00:03:08,280
 have time.

71
00:03:08,280 --> 00:03:11,960
 But from the time we start to the time that we finish, we

72
00:03:11,960 --> 00:03:14,040
 are continuously mindful.

73
00:03:14,040 --> 00:03:16,920
 This is another important foundation.

74
00:03:16,920 --> 00:03:19,840
 And another part of this is, as I mentioned, in daily life.

75
00:03:19,840 --> 00:03:22,380
 We have to be mindful during the time that we're not

76
00:03:22,380 --> 00:03:24,440
 practicing for it to be truly fruitful

77
00:03:24,440 --> 00:03:27,400
 and truly have an impact on our life.

78
00:03:27,400 --> 00:03:28,920
 This is number two.

79
00:03:28,920 --> 00:03:32,760
 Number three, we have to create the clear thought.

80
00:03:32,760 --> 00:03:36,140
 So it's not enough for us to use the word as a mantra as

81
00:03:36,140 --> 00:03:38,080
 though it were "Om" or as though

82
00:03:38,080 --> 00:03:41,730
 it were "Buddha", "Buddha" or "Allah", "Allah" or "Jesus"

83
00:03:41,730 --> 00:03:43,800
 as we use these words as mantras

84
00:03:43,800 --> 00:03:46,040
 to make ourselves peaceful and calm.

85
00:03:46,040 --> 00:03:48,770
 This meditation, the purpose is not simply to make the mind

86
00:03:48,770 --> 00:03:49,800
 peaceful and calm.

87
00:03:49,800 --> 00:03:51,840
 The purpose is to see clearly.

88
00:03:51,840 --> 00:03:55,240
 To see clearly the causes and conditions which are creating

89
00:03:55,240 --> 00:03:56,560
 suffering inside.

90
00:03:56,560 --> 00:03:59,160
 And it can be very unpleasant to face.

91
00:03:59,160 --> 00:04:02,240
 But it requires this clear thought.

92
00:04:02,240 --> 00:04:05,010
 We can't simply say to ourselves, "Rising, falling," and

93
00:04:05,010 --> 00:04:06,400
 not be aware of the rising and

94
00:04:06,400 --> 00:04:07,400
 falling.

95
00:04:07,400 --> 00:04:10,840
 There are three parts to the clear thought.

96
00:04:10,840 --> 00:04:13,080
 One, we need to have the effort.

97
00:04:13,080 --> 00:04:16,720
 We need to put energy into it, put effort into sending the

98
00:04:16,720 --> 00:04:18,560
 mind out to the object again

99
00:04:18,560 --> 00:04:20,160
 and again and again and again.

100
00:04:20,160 --> 00:04:23,110
 Not simply saying, "Rising, falling," at the mouth, but not

101
00:04:23,110 --> 00:04:25,080
 actually watching the stomach.

102
00:04:25,080 --> 00:04:27,610
 Or saying it in the mind, but not actually watching, not

103
00:04:27,610 --> 00:04:29,320
 with the mind down in the stomach.

104
00:04:29,320 --> 00:04:31,720
 When we do mindful frustration, our mind should be with a

105
00:04:31,720 --> 00:04:32,080
 hand.

106
00:04:32,080 --> 00:04:34,280
 When we walk, our mind should be with a foot.

107
00:04:34,280 --> 00:04:37,020
 When we sit, our mind should be with a rising and a falling

108
00:04:37,020 --> 00:04:37,280
.

109
00:04:37,280 --> 00:04:38,280
 And so on.

110
00:04:38,280 --> 00:04:41,480
 So this is the first part, we need effort.

111
00:04:41,480 --> 00:04:43,800
 The second part is we need to focus.

112
00:04:43,800 --> 00:04:45,680
 We need to know the rising and the falling.

113
00:04:45,680 --> 00:04:47,760
 Know the object of our attention.

114
00:04:47,760 --> 00:04:49,920
 Once we have the effort to put the mind there, we have to

115
00:04:49,920 --> 00:04:51,200
 know from the beginning to the

116
00:04:51,200 --> 00:04:52,200
 end.

117
00:04:52,200 --> 00:04:53,800
 From the beginning to the end.

118
00:04:53,800 --> 00:04:57,200
 Know this is rising, know this is falling.

119
00:04:57,200 --> 00:05:00,150
 And the third part is that we need to actually say the word

120
00:05:00,150 --> 00:05:00,440
.

121
00:05:00,440 --> 00:05:03,000
 We have to, not with the mouth, say the word, but with the

122
00:05:03,000 --> 00:05:04,080
 mind, say the word.

123
00:05:04,080 --> 00:05:07,010
 Our mind should be in the stomach and we should be saying

124
00:05:07,010 --> 00:05:08,680
 at the stomach, "rising in our

125
00:05:08,680 --> 00:05:09,680
 mind."

126
00:05:09,680 --> 00:05:15,320
 Falling, rising, falling.

127
00:05:15,320 --> 00:05:17,480
 These three together mean making a clear thought.

128
00:05:17,480 --> 00:05:21,120
 One effort, two, knowing, knowing the object, and three,

129
00:05:21,120 --> 00:05:23,360
 making the acknowledgment, saying

130
00:05:23,360 --> 00:05:27,080
 to ourselves, creating the clear thought like a mantra.

131
00:05:27,080 --> 00:05:28,880
 This is the third fundamental.

132
00:05:28,880 --> 00:05:31,820
 The fourth fundamental in meditation practice is that we

133
00:05:31,820 --> 00:05:33,600
 balance our mental faculties.

134
00:05:33,600 --> 00:05:37,300
 Now according to the teaching, which is a foundation for

135
00:05:37,300 --> 00:05:39,240
 this meditation practice, all

136
00:05:39,240 --> 00:05:42,440
 people in the world have five mental faculties.

137
00:05:42,440 --> 00:05:45,670
 But because they're not balanced correctly, it leads us to

138
00:05:45,670 --> 00:05:47,560
 fall into all sorts of hindrances

139
00:05:47,560 --> 00:05:48,560
 in our lives.

140
00:05:48,560 --> 00:05:52,520
 That we meet with all sorts of obstacles in our own minds.

141
00:05:52,520 --> 00:05:59,620
 Obstacles like greed or craving, doubt, distraction, drows

142
00:05:59,620 --> 00:06:01,920
iness, and so on.

143
00:06:01,920 --> 00:06:04,570
 But when we are able to practice using mindfulness to

144
00:06:04,570 --> 00:06:06,680
 balance these out, when we use the clear

145
00:06:06,680 --> 00:06:10,790
 thought to balance out our mental faculties, they will gain

146
00:06:10,790 --> 00:06:12,960
 a power which is able to overcome

147
00:06:12,960 --> 00:06:16,950
 stress, overcome worry, overcome fear, overcome depression,

148
00:06:16,950 --> 00:06:18,720
 is able to overcome even anger

149
00:06:18,720 --> 00:06:22,020
 and greed so that we're able to throw away, do away with

150
00:06:22,020 --> 00:06:23,960
 our addictions and do away with

151
00:06:23,960 --> 00:06:27,400
 our frustration.

152
00:06:27,400 --> 00:06:32,680
 The five mental faculties are faith, confidence, effort,

153
00:06:32,680 --> 00:06:36,520
 mindfulness, concentration, and wisdom.

154
00:06:36,520 --> 00:06:39,080
 And they balance together as follows.

155
00:06:39,080 --> 00:06:42,360
 Confidence and wisdom have to balance together.

156
00:06:42,360 --> 00:06:45,360
 If one has too much confidence, or lots of confidence but

157
00:06:45,360 --> 00:06:46,920
 not a lot of wisdom, it leads

158
00:06:46,920 --> 00:06:49,410
 one to be greedy, believing that whatever one wants one

159
00:06:49,410 --> 00:06:51,320
 should go for, one should take.

160
00:06:51,320 --> 00:06:53,120
 This leads to greed.

161
00:06:53,120 --> 00:06:57,150
 If there's strong wisdom but weak confidence, this leads to

162
00:06:57,150 --> 00:06:57,840
 doubt.

163
00:06:57,840 --> 00:07:01,400
 So we may know a lot but still doubt the things that we

164
00:07:01,400 --> 00:07:02,080
 know.

165
00:07:02,080 --> 00:07:05,490
 If we have effort and concentration have to balance each

166
00:07:05,490 --> 00:07:06,120
 other.

167
00:07:06,120 --> 00:07:09,480
 If we have strong effort but weak concentration, well we

168
00:07:09,480 --> 00:07:11,960
 tend to become distracted in the mind.

169
00:07:11,960 --> 00:07:15,920
 If we have strong concentration but weak effort, we'll tend

170
00:07:15,920 --> 00:07:17,840
 to become drowsy or lazy.

171
00:07:17,840 --> 00:07:22,290
 In mindfulness, the fifth faculty is the one which balances

172
00:07:22,290 --> 00:07:23,680
 the other side.

173
00:07:23,680 --> 00:07:25,400
 When these are balanced, they gain a power.

174
00:07:25,400 --> 00:07:28,760
 They become one and they have a power to cut off all sorts

175
00:07:28,760 --> 00:07:31,320
 of unwholesome, unpleasant states

176
00:07:31,320 --> 00:07:34,340
 so that our minds are able to feel peaceful and calm all

177
00:07:34,340 --> 00:07:36,040
 the time, so that we're able

178
00:07:36,040 --> 00:07:41,920
 to live our lives rationally and maturely and with wisdom.

179
00:07:41,920 --> 00:07:45,370
 If we can put together these four foundations or four

180
00:07:45,370 --> 00:07:47,800
 fundamentals of meditation practice

181
00:07:47,800 --> 00:07:51,300
 in the practice according to as I've described in the other

182
00:07:51,300 --> 00:07:53,440
 videos, then surely through time

183
00:07:53,440 --> 00:07:57,050
 over time we'll be able to do away with all sorts of

184
00:07:57,050 --> 00:08:00,000
 unpleasant, stressful, suffering

185
00:08:00,000 --> 00:08:02,760
 states of being.

186
00:08:02,760 --> 00:08:03,760
 That's all for now.

187
00:08:03,760 --> 00:08:06,510
 I wish you all the best and may the meditation practice

188
00:08:06,510 --> 00:08:08,640
 bring you fruit and bring you happiness

189
00:08:08,640 --> 00:08:09,640
 and peace in your life.

190
00:08:09,640 --> 00:08:09,640
 Thank you.

191
00:08:09,640 --> 00:08:10,640
 1

192
00:08:10,640 --> 00:08:11,640
 1

193
00:08:11,640 --> 00:08:12,640
 1

194
00:08:12,640 --> 00:08:13,640
 1

