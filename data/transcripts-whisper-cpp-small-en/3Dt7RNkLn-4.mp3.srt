1
00:00:00,000 --> 00:00:04,830
 Hi, welcome back to Ask a Monk. Today's question comes from

2
00:00:04,830 --> 00:00:08,880
 Fiskdams Consulting.

3
00:00:08,880 --> 00:00:11,680
 What is really the mind and how did the Buddha use the last

4
00:00:11,680 --> 00:00:13,860
 four parts of the five aggregates

5
00:00:13,860 --> 00:00:18,450
 and nama in nama-rupa to explain the mind? Did the Buddha

6
00:00:18,450 --> 00:00:19,900
 use any other ways of explaining

7
00:00:19,900 --> 00:00:24,480
 the mind? Understanding what the mind is is hard. Please

8
00:00:24,480 --> 00:00:26,080
 help.

9
00:00:26,080 --> 00:00:30,360
 What is really the mind? The mind, the definition of mind,

10
00:00:30,360 --> 00:00:32,780
 and here this is the meaning of nama

11
00:00:32,780 --> 00:00:37,380
 or vinyana or dita, which in the end are all just different

12
00:00:37,380 --> 00:00:39,760
 ways of saying basically the

13
00:00:39,760 --> 00:00:45,800
 same thing. It's that which knows. In reality we understand

14
00:00:45,800 --> 00:00:48,760
 there are two parts. One part

15
00:00:48,760 --> 00:00:52,240
 one part of reality isn't aware of anything. It's the

16
00:00:52,240 --> 00:00:54,400
 unconscious part of reality. That

17
00:00:54,400 --> 00:00:58,810
 part we call rupa and in English we translate that into

18
00:00:58,810 --> 00:01:01,880
 form or material or the physical.

19
00:01:01,880 --> 00:01:04,360
 The other part knows something. It's aware of things,

20
00:01:04,360 --> 00:01:05,880
 generally aware of the physical

21
00:01:05,880 --> 00:01:13,540
 but also aware of other mental states. This knowing we call

22
00:01:13,540 --> 00:01:16,320
 nama. This is all that really

23
00:01:16,320 --> 00:01:21,120
 exists in the mind and the other aspects of it are the

24
00:01:21,120 --> 00:01:24,120
 quality of knowing because not

25
00:01:24,120 --> 00:01:27,250
 every knowing is the same. Sometimes this knowing, this

26
00:01:27,250 --> 00:01:29,520
 awareness, this observation of things

27
00:01:29,520 --> 00:01:33,160
 is based on the judgment, anger, greed. Sometimes it's

28
00:01:33,160 --> 00:01:36,000
 impartial. Sometimes it's happy. Sometimes

29
00:01:36,000 --> 00:01:40,060
 it's unhappy. Not every knowledge is the same. These are

30
00:01:40,060 --> 00:01:41,840
 the various qualities of the mind.

31
00:01:41,840 --> 00:01:46,470
 But essentially the meaning of mind is that which knows. In

32
00:01:46,470 --> 00:01:49,300
 this sense it's really not

33
00:01:49,300 --> 00:01:53,210
 very difficult to understand at all. It's very much a part

34
00:01:53,210 --> 00:01:55,080
 of meditation practice and

35
00:01:55,080 --> 00:02:00,790
 if you practice sincere meditation it's quite easy to see

36
00:02:00,790 --> 00:02:03,920
 that which is the mind. For instance

37
00:02:03,920 --> 00:02:07,090
 when you're watching the stomach, if you watch it rising

38
00:02:07,090 --> 00:02:09,080
 and falling, there is the reality

39
00:02:09,080 --> 00:02:11,200
 of the stomach rising and falling but there is also the

40
00:02:11,200 --> 00:02:12,480
 knowing of it and these are two

41
00:02:12,480 --> 00:02:15,310
 distinct things. We know this because you're not always

42
00:02:15,310 --> 00:02:17,960
 aware of the rising and falling.

43
00:02:17,960 --> 00:02:21,310
 If the mind doesn't go out to the object then there's no

44
00:02:21,310 --> 00:02:23,600
 awareness. Sometimes the mind is

45
00:02:23,600 --> 00:02:26,430
 elsewhere. The rising and falling is still occurring but

46
00:02:26,430 --> 00:02:27,880
 the mind is not aware of it.

47
00:02:27,880 --> 00:02:32,480
 So until the mind and the body come in contact there's no

48
00:02:32,480 --> 00:02:35,480
 awareness. This is what is meant

49
00:02:35,480 --> 00:02:39,650
 by the mind. The body is one thing, the mind is another and

50
00:02:39,650 --> 00:02:41,840
 when they come together that's

51
00:02:41,840 --> 00:02:47,790
 called experience. As far as the five aggregates, this is

52
00:02:47,790 --> 00:02:50,880
 one very important way of understanding

53
00:02:50,880 --> 00:02:57,620
 what is the body and mind and essentially what is reality.

54
00:02:57,620 --> 00:02:59,360
 The way the Buddha explained

55
00:02:59,360 --> 00:03:03,450
 the five aggregates, they are something that arises at what

56
00:03:03,450 --> 00:03:05,480
 are called the six senses.

57
00:03:05,480 --> 00:03:08,360
 So we always talk about the five senses but actually there

58
00:03:08,360 --> 00:03:10,200
 are six, the five that we normally

59
00:03:10,200 --> 00:03:14,320
 understand and the sixth one is the mind, the purely mental

60
00:03:14,320 --> 00:03:16,360
, the thinking, our mental

61
00:03:16,360 --> 00:03:20,360
 landscape. So when you see something there are five things

62
00:03:20,360 --> 00:03:22,840
 that arise. There's the physical

63
00:03:22,840 --> 00:03:26,850
 which is the eye and the light that touches it, that's a

64
00:03:26,850 --> 00:03:28,740
 physical reality. Then there

65
00:03:28,740 --> 00:03:32,260
 is number two, the feeling which is in the beginning

66
00:03:32,260 --> 00:03:34,840
 neutral but can evolve into a pleasant

67
00:03:34,840 --> 00:03:39,690
 or an unpleasant feeling based on what we see whether it's

68
00:03:39,690 --> 00:03:41,680
 beautiful or ugly or how

69
00:03:41,680 --> 00:03:46,050
 we react to it in the mind. Number three is our recognition

70
00:03:46,050 --> 00:03:47,360
 of it, our perception of it

71
00:03:47,360 --> 00:03:53,310
 as yellow, white, blue, as a car, a bird, a tree. Remember

72
00:03:53,310 --> 00:03:56,160
 recognizing it as this person

73
00:03:56,160 --> 00:03:59,310
 or that person. Number four is what we think of it, our

74
00:03:59,310 --> 00:04:01,480
 judgments liking it, disliking,

75
00:04:01,480 --> 00:04:08,100
 getting angry or becoming attached to it and so on. Number

76
00:04:08,100 --> 00:04:10,200
 five is the basic awareness

77
00:04:10,200 --> 00:04:14,120
 which governs all of the other, which governs the whole of

78
00:04:14,120 --> 00:04:16,360
 the experience. These five things

79
00:04:16,360 --> 00:04:18,450
 are called the five aggregates. That's how the Buddha

80
00:04:18,450 --> 00:04:19,920
 explained it and that's what happens

81
00:04:19,920 --> 00:04:21,840
 when you meditate. When you see something and you say to

82
00:04:21,840 --> 00:04:23,440
 yourself seeing, seeing, seeing,

83
00:04:23,440 --> 00:04:25,990
 you're aware of all of this and sometimes one is clearer,

84
00:04:25,990 --> 00:04:28,200
 sometimes the feeling is clearer,

85
00:04:28,200 --> 00:04:31,550
 sometimes the memory, the recognition that this is

86
00:04:31,550 --> 00:04:33,600
 something I've seen before. Sometimes

87
00:04:33,600 --> 00:04:36,880
 it's the, what you think of it, the judgment of it.

88
00:04:36,880 --> 00:04:38,240
 Sometimes it's just the clear awareness,

89
00:04:38,240 --> 00:04:41,880
 the fact that you're aware of things, you know that you're

90
00:04:41,880 --> 00:04:43,560
 seeing this knowledge of

91
00:04:43,560 --> 00:04:46,960
 it. When you hear it's the same, when you smell, when you

92
00:04:46,960 --> 00:04:48,400
 taste, when you feel and when

93
00:04:48,400 --> 00:04:51,320
 you think. When you think there's the physical but it's not

94
00:04:51,320 --> 00:04:53,200
 really in play there, the physical

95
00:04:53,200 --> 00:04:58,620
 is required as a base for the human experience but then

96
00:04:58,620 --> 00:05:01,600
 there are the, basically the four

97
00:05:01,600 --> 00:05:06,850
 experiences based on thought, based on a thought that has

98
00:05:06,850 --> 00:05:09,960
 arisen which can arise because of

99
00:05:09,960 --> 00:05:16,560
 the physical or because of the mental. So that's, did the

100
00:05:16,560 --> 00:05:18,960
 Buddha use other ways of explaining

101
00:05:18,960 --> 00:05:23,450
 the mind? Yeah, this is the most standard experience of

102
00:05:23,450 --> 00:05:26,120
 explanation of how to understand

103
00:05:26,120 --> 00:05:29,820
 the mind but there's, the Buddha used a lot of ways to

104
00:05:29,820 --> 00:05:31,960
 explain both body and mind and

105
00:05:31,960 --> 00:05:34,710
 the most comprehensive, if you're really interested in that

106
00:05:34,710 --> 00:05:36,280
 sort of thing is in what is called

107
00:05:36,280 --> 00:05:44,280
 the Abhidhamma which is the most detailed exposition on the

108
00:05:44,280 --> 00:05:47,400
 body and the mind which

109
00:05:47,400 --> 00:05:51,620
 includes 121 different types of mind state and it's

110
00:05:51,620 --> 00:05:53,880
 basically a permutation of the various

111
00:05:53,880 --> 00:05:58,200
 types of mind states that can come together. For instance,

112
00:05:58,200 --> 00:06:00,320
 there are eight types of greed,

113
00:06:00,320 --> 00:06:04,170
 of consciousness based on greed or craving and you get the

114
00:06:04,170 --> 00:06:06,080
 number eight because there

115
00:06:06,080 --> 00:06:09,930
 are three factors. There are greed states that are

116
00:06:09,930 --> 00:06:12,720
 associated with pleasure and those

117
00:06:12,720 --> 00:06:17,870
 that are associated with equanimity. There are greed states

118
00:06:17,870 --> 00:06:20,320
 that are accompanied by wrong

119
00:06:20,320 --> 00:06:25,000
 view, by the understanding that it's good to be greedy and

120
00:06:25,000 --> 00:06:27,280
 there are greed states that

121
00:06:27,280 --> 00:06:29,900
 are not associated with view so the understanding that it's

122
00:06:29,900 --> 00:06:31,520
 wrong but still wanting something,

123
00:06:31,520 --> 00:06:35,960
 the understanding that it's not useful for you but still

124
00:06:35,960 --> 00:06:38,160
 wanting something. And the third

125
00:06:38,160 --> 00:06:42,970
 one whether it is prompted or unprompted, whether it comes

126
00:06:42,970 --> 00:06:45,360
 from itself or whether there's an

127
00:06:45,360 --> 00:06:50,000
 external stimulus like someone urging you to steal or so on

128
00:06:50,000 --> 00:06:52,240
 or some kind of other factor

129
00:06:52,240 --> 00:06:55,530
 that it doesn't spontaneously arise. So with these three

130
00:06:55,530 --> 00:06:56,720
 factors you have two times two

131
00:06:56,720 --> 00:07:00,920
 times two and you get eight different kinds of greed minds.

132
00:07:00,920 --> 00:07:02,560
 There are two kinds of anger

133
00:07:02,560 --> 00:07:05,990
 minds, two kinds of delusion minds that are simply based on

134
00:07:05,990 --> 00:07:07,600
 delusion and those are the

135
00:07:07,600 --> 00:07:10,240
 twelve unwholesome minds and then it goes on and on and on.

136
00:07:10,240 --> 00:07:14,440
 There are eight great wholesoms

137
00:07:14,440 --> 00:07:19,400
 or eight sensual sphere wholesoms and eight, I can't even

138
00:07:19,400 --> 00:07:22,120
 remember, it's been a long time

139
00:07:22,120 --> 00:07:24,790
 but I've got the book and you can go through it and there's

140
00:07:24,790 --> 00:07:26,560
 121 of them and some of them

141
00:07:26,560 --> 00:07:32,670
 are super mundane, some of them are based on meditative

142
00:07:32,670 --> 00:07:35,320
 states. And then there are 52,

143
00:07:35,320 --> 00:07:38,350
 I'm probably getting it wrong, there's a whole bunch of

144
00:07:38,350 --> 00:07:40,400
 mental states that are involved here

145
00:07:40,400 --> 00:07:43,310
 like the ones that are involved in all of these minds, the

146
00:07:43,310 --> 00:07:44,480
 mental qualities that are

147
00:07:44,480 --> 00:07:47,510
 only in some of the minds, only in the wholesome minds,

148
00:07:47,510 --> 00:07:49,560
 only in the unwholesome minds and so

149
00:07:49,560 --> 00:07:56,450
 on and so on. So it's a very complex breakdown of the

150
00:07:56,450 --> 00:07:59,880
 mental landscape. All of that's really

151
00:07:59,880 --> 00:08:05,290
 theoretical but it does give a good understanding of the

152
00:08:05,290 --> 00:08:08,680
 various details of the things that

153
00:08:08,680 --> 00:08:11,740
 you're going to run into in your meditation practice and in

154
00:08:11,740 --> 00:08:13,520
 your life, understanding what

155
00:08:13,520 --> 00:08:16,740
 a mind state is and it also helps you to break down some of

156
00:08:16,740 --> 00:08:18,800
 the, as I said, entities where

157
00:08:18,800 --> 00:08:21,430
 we think this is a problem, something's going on, to be

158
00:08:21,430 --> 00:08:22,720
 able to break it down and see, oh

159
00:08:22,720 --> 00:08:25,700
 it's actually just states of greed, states of anger, things

160
00:08:25,700 --> 00:08:27,160
 that are arising in my mind,

161
00:08:27,160 --> 00:08:36,080
 it's my extrapolation of something very simple. But

162
00:08:36,080 --> 00:08:37,760
 certainly too much for a short video,

163
00:08:37,760 --> 00:08:41,540
 I'm not going into detail about that but just so you

164
00:08:41,540 --> 00:08:44,400
 understand there are many different

165
00:08:44,400 --> 00:08:49,750
 aspects or ways that the Buddha used of explaining the mind

166
00:08:49,750 --> 00:08:51,840
. And as you say, in the end understanding

167
00:08:51,840 --> 00:08:54,730
 the mind is hard. There's no getting around that. You can

168
00:08:54,730 --> 00:08:56,240
 theorize, you can read texts

169
00:08:56,240 --> 00:08:59,620
 and so on but until you sit down and practice meditation,

170
00:08:59,620 --> 00:09:01,600
 you'll never really understand

171
00:09:01,600 --> 00:09:06,010
 the mind. And once you sit down and practice meditation, it

172
00:09:06,010 --> 00:09:07,920
 may still not be clear how

173
00:09:07,920 --> 00:09:10,820
 that relates to the five aggregates or how the Buddha

174
00:09:10,820 --> 00:09:12,880
 talked about mind or consciousness

175
00:09:12,880 --> 00:09:17,120
 or so on. But it's not really important because you simply

176
00:09:17,120 --> 00:09:19,400
 need someone like me to explain

177
00:09:19,400 --> 00:09:22,040
 to you what is happening here and here and here as I've

178
00:09:22,040 --> 00:09:23,640
 just done hopefully helps with

179
00:09:23,640 --> 00:09:26,380
 that. But the point is that you're seeing reality. It's not

180
00:09:26,380 --> 00:09:27,280
 important that you understand

181
00:09:27,280 --> 00:09:29,830
 the Buddha's concept of mind. It's important you understand

182
00:09:29,830 --> 00:09:31,120
 the truth of mind, the truth

183
00:09:31,120 --> 00:09:33,920
 of reality. It's not so important that you can say that's

184
00:09:33,920 --> 00:09:35,960
 mind, that's body, that's this,

185
00:09:35,960 --> 00:09:39,020
 that's this. It's much more important that you see it for

186
00:09:39,020 --> 00:09:40,600
 what it is. It is what it is

187
00:09:40,600 --> 00:09:44,570
 at that moment. You see it clearly and you're not judging

188
00:09:44,570 --> 00:09:46,840
 it. You don't have to be clearly

189
00:09:46,840 --> 00:09:50,190
 aware of what part of the Buddha's teaching that fits into

190
00:09:50,190 --> 00:09:51,760
 in terms of which one of the

191
00:09:51,760 --> 00:09:56,440
 121 minds it is, for example. When you can see that certain

192
00:09:56,440 --> 00:09:58,640
 mind states are unwholesome,

193
00:09:58,640 --> 00:10:01,400
 then you give those up. When you see that certain mind

194
00:10:01,400 --> 00:10:03,120
 states are wholesome, then you

195
00:10:03,120 --> 00:10:07,260
 follow those. And this comes naturally through the practice

196
00:10:07,260 --> 00:10:10,520
 and that's most important. But

197
00:10:10,520 --> 00:10:13,710
 certainly it's not easy and as far as the fact that the

198
00:10:13,710 --> 00:10:15,960
 mind is difficult to understand,

199
00:10:15,960 --> 00:10:20,080
 there's a quote of the Buddha that's often given and in the

200
00:10:20,080 --> 00:10:22,960
 Buddha's language it goes,

201
00:10:22,960 --> 00:10:28,890
 "Dulangamang ekachalang asari rang guhasayang yeh chitang s

202
00:10:28,890 --> 00:10:32,920
anyami santi mukhanti mala bandhana,"

203
00:10:32,920 --> 00:10:38,760
 which means basically, "Dulangamang" it travels far, "ekach

204
00:10:38,760 --> 00:10:42,560
alang" it travels alone, "asari

205
00:10:42,560 --> 00:10:47,610
 rang" it has no form, "guhasayang" it dwells in a cave. And

206
00:10:47,610 --> 00:10:48,600
 the point of this first part

207
00:10:48,600 --> 00:10:51,370
 is to give you the idea of some terrible beast or demon or

208
00:10:51,370 --> 00:10:53,040
 ghost, something that is very

209
00:10:53,040 --> 00:10:58,290
 mysterious because it travels far and wide alone without

210
00:10:58,290 --> 00:11:00,600
 form in living in a cave. And

211
00:11:00,600 --> 00:11:04,870
 then the next part is "yeh chitang sanyami santi mukhanti m

212
00:11:04,870 --> 00:11:07,160
ala bandhana," which means

213
00:11:07,160 --> 00:11:13,880
 a person who can calm the mind, this person becomes free

214
00:11:13,880 --> 00:11:17,200
 from the bonds of "mara," the

215
00:11:17,200 --> 00:11:22,010
 bonds of evil. So the mind is considered something that

216
00:11:22,010 --> 00:11:25,520
 goes far, travels far and can't be controlled.

217
00:11:25,520 --> 00:11:30,110
 In a moment you can be thinking of something a million

218
00:11:30,110 --> 00:11:33,000
 miles or a thousand miles away,

219
00:11:33,000 --> 00:11:36,220
 and it travels alone means that you are only aware of one

220
00:11:36,220 --> 00:11:37,840
 thing at a time, so the mind

221
00:11:37,840 --> 00:11:41,030
 is always on one thing or another, it's never in two places

222
00:11:41,030 --> 00:11:43,320
 at once and it's certainly not

223
00:11:43,320 --> 00:11:46,770
 able to comprehend everything. If you don't catch it and

224
00:11:46,770 --> 00:11:48,520
 fix it and focus on one thing,

225
00:11:48,520 --> 00:11:53,340
 it's going to jump back and forth between objects. "Asari

226
00:11:53,340 --> 00:11:55,600
 rang" means it's very difficult

227
00:11:55,600 --> 00:11:58,390
 to understand, and this is why in Buddhist meditation we

228
00:11:58,390 --> 00:11:59,840
 always have you focus on the

229
00:11:59,840 --> 00:12:03,630
 body. And many people don't understand this, they'll say, "

230
00:12:03,630 --> 00:12:05,040
What's the point? I really

231
00:12:05,040 --> 00:12:07,310
 want to understand the mind, I want to understand how my

232
00:12:07,310 --> 00:12:09,160
 mind works," and that's where the problem

233
00:12:09,160 --> 00:12:13,260
 is. True, but it doesn't have a physical form, "Asari rang

234
00:12:13,260 --> 00:12:14,600
," the mind is not something

235
00:12:14,600 --> 00:12:19,280
 you can grab at and say, "There it is, it's something that

236
00:12:19,280 --> 00:12:23,640
 relies on the physical." And

237
00:12:23,640 --> 00:12:26,680
 so we have you focus on the physical, and this is another

238
00:12:26,680 --> 00:12:28,120
 thing that the texts talk

239
00:12:28,120 --> 00:12:34,540
 about, saying that we have you always focus on the physical

240
00:12:34,540 --> 00:12:37,640
 first, and the point is that

241
00:12:37,640 --> 00:12:41,320
 when you focus on the physical, the mind becomes clear by

242
00:12:41,320 --> 00:12:44,000
 itself. Once the body becomes clear,

243
00:12:44,000 --> 00:12:48,010
 then the mind becomes clear automatically, and you don't

244
00:12:48,010 --> 00:12:50,240
 have to do anything special.

245
00:12:50,240 --> 00:12:53,560
 You don't have to be chasing after the mind, just like a

246
00:12:53,560 --> 00:12:55,400
 hunter when they want to catch

247
00:12:55,400 --> 00:12:59,070
 a deer or a wild animal. They don't go running through the

248
00:12:59,070 --> 00:13:01,000
 forest after the wild animal,

249
00:13:01,000 --> 00:13:03,530
 they sit by the place where they know the deer is going to

250
00:13:03,530 --> 00:13:04,960
 come, either the water hole

251
00:13:04,960 --> 00:13:08,360
 or the fruit trees or someplace where they know that the

252
00:13:08,360 --> 00:13:10,120
 deer are bound to come at some

253
00:13:10,120 --> 00:13:14,190
 point and they sit there and they wait, and sure enough the

254
00:13:14,190 --> 00:13:16,040
 mind will come to them and

255
00:13:16,040 --> 00:13:20,000
 they'll be able to catch the, do what they want. The same

256
00:13:20,000 --> 00:13:21,240
 with in our meditation, we

257
00:13:21,240 --> 00:13:23,700
 don't have to go chasing after the mind. When we focus on

258
00:13:23,700 --> 00:13:25,640
 the rising and the falling, well

259
00:13:25,640 --> 00:13:28,300
 that's the mind going out to the rising and to the stomach,

260
00:13:28,300 --> 00:13:29,520
 and we're going to see how

261
00:13:29,520 --> 00:13:32,050
 the mind works. We're going to get angry sometimes,

262
00:13:32,050 --> 00:13:33,800
 frustrated at it sometimes, we're going to

263
00:13:33,800 --> 00:13:36,740
 be happy sometimes, unhappy sometimes, we're going to see

264
00:13:36,740 --> 00:13:38,360
 how our mind reacts to things,

265
00:13:38,360 --> 00:13:43,120
 we're going to see the quality and the characteristics of

266
00:13:43,120 --> 00:13:48,480
 our mind, our habits and so on. So it's

267
00:13:48,480 --> 00:13:51,210
 very difficult to catch the mind without this sort of a

268
00:13:51,210 --> 00:13:52,840
 technique. This is why we use the

269
00:13:52,840 --> 00:13:57,090
 body first, because it's a saree lang, it has no physical

270
00:13:57,090 --> 00:13:58,960
 form. And guha sa yang means

271
00:13:58,960 --> 00:14:02,860
 it is the other part of this, it dwells in a cave and that

272
00:14:02,860 --> 00:14:04,760
 cave is the body, that this

273
00:14:04,760 --> 00:14:08,850
 mind is somehow trapped in the cage of the body. It always

274
00:14:08,850 --> 00:14:11,200
 has to come back and be dependent

275
00:14:11,200 --> 00:14:14,380
 on this. So when you focus on any part of the physical

276
00:14:14,380 --> 00:14:16,000
 reality as it arises, you're

277
00:14:16,000 --> 00:14:20,140
 going to be able to see the mind, you're going to be able

278
00:14:20,140 --> 00:14:21,840
 to catch it. It's like finding

279
00:14:21,840 --> 00:14:25,610
 the cage of the YMVs, where its limits are, you just watch

280
00:14:25,610 --> 00:14:27,800
 those limits, which is in this

281
00:14:27,800 --> 00:14:33,790
 case the body. Okay, so good luck training the mind,

282
00:14:33,790 --> 00:14:39,360
 calming the mind and keeping the

283
00:14:39,360 --> 00:14:43,290
 mind clear and out of difficulty. This is the way, as the

284
00:14:43,290 --> 00:14:45,280
 Buddha said, to overcome the

285
00:14:45,280 --> 00:14:49,050
 bonds of Mara, which is like the Buddhist version of Satan,

286
00:14:49,050 --> 00:14:50,680
 but it just means the evil

287
00:14:50,680 --> 00:14:53,560
 that exists in our minds, and it's the way to overcome

288
00:14:53,560 --> 00:14:55,160
 these things. Okay, so thanks

289
00:14:55,160 --> 00:14:56,520
 for the question, hope that helps.

