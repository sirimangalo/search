 Welcome everyone. Thank you for coming. It's nice to see
 that there is still interest.
 It's been almost a week and still there are people coming.
 It's a good sign for the future. I'm going to take it week
 by week and see how much,
 see how long it takes for people to forget about us.
 I don't expect that's going to happen. We're too proactive
 about it, I guess.
 There's a lot of people following what we do here, which is
 nice.
 It's nice not particularly, not just because it's what we
're doing.
 It's always nice to have people interested in the things
 you're doing, of course.
 But on a more spiritual level, it's nice to see people
 interested in good things.
 We have a debate as a species, really, whether evil and
 good actually exist.
 Some people say evil and good are just conventions.
 Other people say evil and good are manifestations or
 principles set down by a higher power, God.
 They may say that good exists, but evil is just the absence
 of good.
 Because if they admit that evil exists, then they've got a
 problem, because why did God create evil?
 So they say it's just the absence of good.
 Many people say that good and evil are just relative in
 their products of human thought, human culture.
 There's really nothing good or evil.
 So what do Buddhists believe about good and evil? Do Buddh
ists believe that good and evil exist?
 I think for most people who have grown up Buddhist or spent
 a lot of time studying Buddhist teaching,
 you can see that the Buddha did talk quite a bit about good
 and evil.
 He wasn't one of the people who said good and evil don't
 exist.
 He did talk sometimes about rising above good and evil.
 So you hear about the Buddha saying that an Arahant has
 gone beyond good and evil,
 because they don't have…
 This is an interesting point. Why an enlightened being is
 exceptional in this?
 Why does everyone else live in a realm of good and evil?
 And why does an Arahant free? And it points to what is the
 Buddhist understanding of good and evil.
 The Buddhist concept of good and evil rests in this idea of
 karma, that there are deeds,
 there is the fruit of deeds. The Buddha called this right
 view,
 a person who believes that there is no result from doing a
 certain type of deed.
 This is a wrong view if a person doesn't understand this or
 doesn't accept this.
 A person has the idea that you can do bad deeds,
 deeds that are evil and not receive an evil result or a bad
 result.
 You can do good deeds and not receive a good result.
 And it's kind of, therefore, a bit of a subjective
 definition of good and evil.
 And it shows that we have this specific definition of good
 and evil that says,
 good is anything that leads to a good result, evil is
 anything that leads to a bad result.
 And so on a conventional level you actually have deeds that
 are good and evil.
 A person might, you know, if you kill a mass murderer or
 something,
 someone's trying to kill someone and you stop them, you
 beat them up.
 This you might say is good and evil.
 But the philosophy is the same, whether you look at it
 conventionally in this way
 or whether you say that no, certain things are
 intrinsically good
 and certain things are intrinsically evil.
 The point is that the meaning of good and the meaning of
 evil have to do with the result.
 So nothing is, in one sense, nothing is intrinsically good
 or evil.
 It depends on the result.
 So this is a part of the reason why, or one way of
 explaining the reason why the Buddha
 denied karma in the Hindu sense in terms of action leading
 to results.
 If you step on an ant without knowing that the ant is there
 or if you hurt someone's feelings without intending to with
 pure intentions
 and they misunderstand you or maybe they mishear what you
 said
 and they think you said something that you didn't and they
 get upset about it, for example.
 If your intentions were pure, it's not the actions, it's
 the effect that it has.
 And so the question here is, well, if you step on the ant
 and that's a really bad effect for the ant, isn't it?
 So if you're talking about the effect, karma is all based
 on effect,
 well then actually how can you say that stepping on an ant
 isn't karmically active
 or isn't a bad thing, isn't evil.
 Stepping on an ant without knowing it would be evil.
 How do you come to this conclusion?
 So you're forced into this understanding of good and evil
 which actually turns out to be quite practical and quite
 useful.
 And that is that good and evil, good is something that
 brings a good result for the individual,
 for the person who commits the act.
 And evil is something that brings a bad result to the
 person who commits it.
 So you take out the victim or the beneficiary completely
 from the equation
 which is quite radical.
 It's something that you wouldn't normally find in anyone's
 idea of what is good and evil.
 Most people say good is when you help someone out,
 when you do something good for someone else, something that
 benefits another person.
 This is good.
 Evil is when you do something that hurts someone else.
 So we base it totally on the effect that it creates for
 other people.
 If people have the idea of good and evil, but then you have
 to ask them,
 well, then so if you hurt someone without, as a mistake,
 if you hurt someone unintentionally, is that evil then
 because it causes harm for the person?
 So you have to think deeper here.
 And on the face of it, it seems like doing good for
 yourself
 would necessitate actually hurting other people.
 This is an incredibly self-centered sort of teaching.
 Buddhism sounds very self-centered.
 And in fact, in many ways it is, but this is the key to it
 is that actually, as the Buddha said, when you help someone
 else, you help yourself.
 And when you help yourself, you help others.
 It's impossible to, or hurting other people doesn't help
 you.
 When you intentionally hurt others or when you act in a
 selfish manner,
 you actually hurt yourself.
 That there is no good that comes from being selfish.
 That actually anything that does good for you is also doing
 good for other people.
 Or is free from any intentional harm towards other people.
 Because of course, coincidental harm is inevitable.
 You can't talk about coincidental harm in terms of evil.
 If you're driving your car and someone happened to run
 someone over
 because they weren't looking or because they tripped and
 fell or something,
 this isn't an evil act.
 So the key is in the intention.
 So the idea that you could somehow steal a rob a bank or
 something
 and that would somehow be good for you.
 This is what we reject in Buddhism.
 We reject the idea that being selfish, that harming other
 people
 or acting to disrupt or to hurt others, that this would
 actually benefit the person.
 And this has to do with the idea of, or the understanding
 of karma and Buddhism,
 that karma is a mental thing, it's a mental quality.
 When you have a mental state that wishes to harm or wishes
 to hurt
 or is in any way disruptive, or you might say in any way cl
ings,
 is any way partial, in any way creates stress in the mind,
 this is bad karma without even doing anything, without even
 acting or speaking.
 It's called mano karma, mental karma, even at the moment
 where you have,
 give rise to greed or anger or delusions, conceit and
 arrogance.
 When you have fear in the mind, when you have boredom in
 the mind,
 all of these things are evil because they hurt you, they
 disrupt the mind's peace,
 they create stress in the mind.
 And it's only because of these mind states that our actions
 become evil,
 without anger or greed or delusion, you can't do bad things
.
 So why robbing a bank couldn't be seen as a good thing
 because it brings you happiness,
 because it doesn't, it creates states of greed, states of
 anger as well,
 because you have a lack of empathy, you know that you're
 going to hurt other people
 and you crush that, the goodness or their well-being,
 wanting to be well yourself, knowing that they're going to
 suffer,
 you crush, you create a dichotomy, you create this double
 standard in the mind
 and you feel it, you feel the dissonance, the stress that
 comes from this.
 So people who kill and steal and rob and cheat and lie and
 so on,
 their minds become disturbed as a result, you can't, there
's no loophole, you see,
 it is airtight and this is key to the theory,
 because if it were possible to hurt other people and
 benefit from it,
 really and truly benefit from it, then you wouldn't be able
 to say that
 good is something that helps yourself and evil is something
 that hurts yourself.
 But because of reality, this is the amazing quality of
 reality,
 it's kind of, well, very difficult to see and it's
 something that we miss a lot
 and as a result we become selfish.
 The reason why we are selfish and why we are treacherous to
 each other,
 the reason why we fight is because we don't see that
 hurting others,
 we hurt ourselves and helping others, we help ourselves.
 So the Buddhist concept of good and evil is very much about
 helping yourself
 and hurting yourself.
 You don't have to look outside of this.
 If you understand what is in your best interest,
 then you don't have to worry about helping other people.
 You will always be kind, generous, compassionate, patient,
 caring.
 You will always be just what everyone expects you to be,
 what people who look for you to not harm others,
 who say good is in helping others and harm is in hurting
 others.
 All of this is accomplished by knowing what is in your own
 best interest
 and by seeing what is causing you harm, you don't actually
 have to
 consider other people.
 You don't actually have to be considerate.
 Once you are calm in your mind and clear in your mind
 and once you are kind and compassionate to yourself,
 again it's this double standard.
 You would never create a double standard.
 You don't feel the need to hurt others.
 When you don't have a need for money,
 when you don't have greed in your mind and addiction to
 pleasure,
 you will never go around trying to cheat people out of
 their possessions
 or out of their pleasures.
 You will never try to take advantage of others at all.
 You won't have any reason to.
 You see that this is hurting you.
 This is why the Buddha said when you help yourself,
 you help others, when you help others, you help yourself,
 because you remove this double standard where
 I want to be happy but I'm going to hurt someone else,
 the mind, this mind state can't arise.
 The person doesn't have the desire to bring pleasure to
 themselves
 at the expense of others.
 One is not able to create this dichotomy.
 So as a result, you're helping yourself, you help others.
 So that's something interesting for us to know.
 It kind of gives us an understanding of what the Buddha
 meant by,
 or what the Buddhist idea of good and evil is.
 But what I really wanted to talk about, of course, is
 meditation.
 That's why everyone is here.
 So this talk on good and evil is, actually the point was
 that
 I'm happy to see all of you coming because this is a good
 thing.
 Meditation is something that is the highest good.
 We think, our understanding in Buddhism is that meditation
 is the highest good
 because it is the purification of the mind.
 It is the highest form of self-help.
 It's the best thing you can do for yourself.
 It's like if you have a river, if you consider yourself as
 being a spring
 of giving forth water or giving forth sustenance to the
 world,
 to the beings around you, providing something.
 Then just like a spring, it's very much dependent on the
 quality of the water in the spring.
 If the quality of the water is tainted, then it doesn't
 matter how much water
 or how far the water spreads, it only serves to destroy it.
 It can never be potable, it can never be drinkable, it can
 never be nourishing.
 But if the source is pure, then whether it be a little or a
 lot,
 anywhere that the water spreads, anything that a person
 does is blameless.
 This is the point.
 You can't blame a person who is pure in mind because
 anything they do is pure.
 If they're asked to help, they will help.
 If they ask to teach, they will teach.
 If they're asked to get lost, they'll get lost.
 They don't have the partiality in the mind.
 They don't have the taint in the mind, their mind is pure.
 So everything that it touches, it benefits.
 If we don't practice meditation, we can do as many good
 deeds as we want,
 but if our minds are still sullied, even if we're generous
 and helping and so on,
 we easily become annoyed and impatient with others
 and actually cause harm to others in our attempt to help.
 We're arrogant, we're conceited, we are pushy and domine
ering and so on,
 even in our quest to help others.
 So if our minds are not pure, goodness has a very great
 difficulty arising,
 or can't arise at the moment when your mind is impure.
 This is the importance of meditation, as the highest good
 and as the best way to root out evil.
 And remember, by evil we just mean the things that hurt us,
 the things that cause us stress and suffering,
 that cause us to lash out at others, that cause us to be
 discontent with what we have
 and need more, that cause us to cling to things that can't
 satisfy us,
 to build up things that can't last.
 So for this reason we practice meditation.
 Now what is meditation? This is kind of what I wanted to
 talk about.
 Meditation in Buddhism generally we separate it into two
 types.
 And you can practice one first and then the other,
 or you can kind of practice them both together.
 But there's two aspects of meditation that you need to
 become free from suffering.
 The first aspect is tranquility.
 And so there are some meditations that are solely for the
 purpose
 of cultivating tranquility, cultivating a sense of peace in
 the mind
 or stillness in the mind, to bring the mind to a state that
 has no distraction.
 Where the mind is focused and clear and able to be malle
able or wieldy,
 it can be useful or usable.
 As the mind in its ordinary state is, you see when you
 start to meditate,
 it's quite useless.
 It's like this tool, but it's not straight, it's not strong
 or so on.
 And it's all over the place.
 You're trying to get the mind to focus in.
 Like you have a hammer, you have to hit the nail.
 But if it's waving and shaking, the handle is shaking,
 and it's a rubber hand, rubber hand, but it's like you can
 never get it on the nail.
 This is the mind, it's all over.
 It's not useful.
 You can't learn anything, you can't understand anything,
 you can't see the truth.
 We know that inside of ourselves there are things that
 shouldn't be there,
 that we'd be better off without.
 They're not helping us, they're not helping other people.
 And yet we still have them.
 So we start meditating and saying, "Okay, how am I going to
 get rid of this?"
 And then we find, "I can't even use my own mind.
 I want to use my mind to observe, to understand these
 things.
 I can't do it."
 So tranquility creates this state of mind.
 If you practice that first, so you practice maybe watching
 the breath go in and out,
 counting the breaths, or maybe you see a light in your
 forehead
 and you focus on the light, or maybe you practice loving
 kindness,
 like we just did, sending love to all the beings in the
 world,
 friendliness, if you want to say, sending our friendly
 thoughts to the world.
 These are the sort of things that calm the mind down
 depending on what sort of person you are.
 Of course you have to make sure that you're using the right
 meditation technique.
 A person who is full of lust, for example, should be
 careful with sending love out.
 A person who is full of anger should be careful about
 sending out,
 or be careful about practicing loathsome-ness in the body.
 So if a person already hates themselves, they shouldn't
 look at the body as being loathsome
 because they might hurt themselves or even kill themselves.
 And a person who has lots of lust shouldn't send love out
 because they will become attracted to the objects of their
 desire.
 But these sorts of things have this benefit.
 So if you're a person full of lust, you should practice
 looking at the loathsome parts of them.
 Look at the body seeing it for this and realizing that
 there's nothing beautiful about it,
 letting go of your attachment to the body.
 If you're a person with great anger, you should practice
 great hatred.
 You should practice loving kindness continuously until you
 can overcome that,
 until you can love the people who you hate because that
 hatred is bad for you.
 These sorts of things have different powers to calm the
 mind.
 Anapanasati, mindfulness of breathing is just, in general,
 a great one to calm the mind.
 The kasinas are very good looking at a light and saying
 light, light, or looking at a whiteness and saying white,
 white.
 These are great for calming down the mind.
 So this is one type of meditation, and it's the cultivation
 of a certain aspect, the one side of the meditation.
 So one way to practice Buddhism is to build this up first,
 and you enter into great states of peace and calm and
 tranquility.
 And then using that tranquility, you start to look at
 reality.
 You say, "Okay, using this, you forget about the disc,
 forget about the breath or so on, forget about whatever,"
 and instead focus on the body and the mind.
 So focus on what it is that's causing you greed, causing
 you anger, causing you delusion.
 What are the things that are causing you suffering?
 So, see, because at this point, now you can use the mind,
 you see.
 So now when you say, "Okay, there are these things I don't
 want. Let's look at them."
 Well, your mind is sharp, it's strong like a hammer, and
 you can break apart the delusion and the illusion
 that is the self and these entities, or these problems that
 we have.
 You are able to break them up and see them piece by piece
 by piece because of the power of your mind
 and the strength of mind.
 So the other aspect of meditation is insight, this wisdom
 that arises when you look and you see.
 As I said, it's very difficult to do if your mind is not
 calm,
 but another way of practicing is to just begin to cultivate
 insight right away.
 And how does that work when your mind is broken?
 Well, at the same time as you're developing insight, you're
 also developing tranquility.
 So, for example, we use this word when we focus on the
 stomach rising.
 We say to ourselves, "Rising," and when the stomach falls,
 we say to ourselves, "Falling."
 Just being aware in the mind, "This is rising, this is
 falling,"
 and this mantra or this word, this label, it fixes the mind
, it focuses the mind,
 and it in a sense calms the mind down even though in the
 beginning it doesn't feel very calm.
 You find that eventually you have this dynamic
 concentration
 where anything that comes up, you're able to see it as it
 is and it disappears.
 There's no liking, no disliking.
 The mind is just as calm as if you were focusing on a
 concept,
 if you were focusing on the breath or focusing on a light
 or so on.
 You are able to at the same time cultivate insight as you
 are also cultivating calm.
 And it's not to say that it's any easier.
 It's actually a lot more stressful because obviously you're
 cultivating,
 you don't have all the calm first.
 If you go off and take an easy object like the breath, it
 can be a lot more comfortable.
 But generally it takes longer because first you have to
 focus on one thing
 and then you have to build that up and build that up
 and then finally come back and focus on another thing.
 Either way is doable.
 The way we teach here, the way we practice here is probably
 for the reasons that it's,
 well one reason is that it's more practical for ordinary
 people.
 So a good reason for teaching this is, well, you can use it
 in your daily life.
 If someone starts shouting at you, you can say to yourself,
 "Angry, angry, or hearing, hearing."
 You can use it in your daily life.
 It would be a lot more difficult to just start focusing on
 the breath.
 Someone shouting at you, you go in one, out one, in two,
 out two.
 It doesn't really work.
 So it's the kind of practice that's much better suited to
 living in the forest.
 And in general it takes longer to, it will take longer to
 cultivate
 because you're having to first develop deep states of
 tranquility
 and then look at reality.
 But it depends on the individual.
 So there are these two ways.
 It's important that we understand.
 But most importantly, either way, the key is understanding.
 The greatest good that we can hope for, that hope to
 cultivate is wisdom.
 All the virtues, just like the moon, just like all the
 stars are,
 as bright in the sky as the moon.
 When you look up in the sky in the full moon day,
 nothing's nearly as bright as the moon.
 So wisdom, nothing is as bright as wisdom.
 No good quality is as good as wisdom.
 This is because wisdom, wisdom unties the knots that are
 the evil in our mind.
 It unties the delusion.
 It removes the basis for all evil states.
 Remember how I explained how, what is evil?
 Does anyone remember what is evil?
 Is anyone listening?
 What is evil? What did I say? Buddhism.
 What did I say, Jeff?
 States of suffering.
 The cause of suffering.
 The cause of suffering.
 Right.
 So, what a person intends, if a person knew that something
 was going to cause them suffering,
 would they ever do that? Do it?
 Would they ever engage in it? If they knew in advance.
 No. Would a person intentionally say, "Oh, let me do this."
 Would a person ever do that? Think, "Hey, I'm going to do
 this."
 If they knew it was going to cause them suffering.
 Because yeah, you might hesitate and say, "Well, I know a
 lot of things are suffering, but I still do them." Right?
 But the point is, you don't intentionally do that.
 So, the difference here, why we do certain things that we
 know are causing us suffering,
 the theory is that we don't really know that they're
 causing us suffering.
 That all it takes is an understanding of that thing to stop
 you from doing it.
 Enough understanding applied to anything, any evil,
 anything that causes you suffering,
 will remove the habit from you.
 The more you see, and this isn't theoretical, you look, you
 practice and you sit,
 and you see yourself doing the same thing over and over
 again,
 the same mind states coming up again and again, causing you
 the same sorrow, the same suffering.
 That's all it takes. Look at it enough, eventually your
 mind is going to say,
 "Enough. Why am I doing this? What good is it?"
 It naturally gives it up. This is an intrinsic quality of
 the mind.
 You might even say it's a law of the mental nature.
 They have all these physical laws. It'd be nice if we could
 come up with some for the mind.
 I think one would be, we should write them down, one would
 be,
 the mind will never, or the mind will reject, will
 immediately or inherently reject evil,
 or reject that which causes suffering once it sees that it
 causes suffering.
 That this is an inherent quality of the mind. The problem
 is that we don't see clearly.
 Once you see that something is not worth clinging to, you
 won't cling to it.
 I mean, obvious examples when you pick up a red-hot ember.
 As soon as you feel that it's causing your suffering, you
 let it go.
 Nobody wants to suffer. But there's just a gross example.
 The subtle nature of the mind is the same.
 The mind doesn't intentionally cause suffering.
 The only reason it causes it suffering is because it thinks
 that this is going to bring me happiness.
 It thinks that there's some good to it.
 Why do we cling to sensual pleasures?
 Well, we think very much that these are going to bring us
 happiness, or we feel very much, you might say.
 It's a very habitual thing, because intellectually we can
 start to say, "Hey, this is causing me trouble."
 But because of the habit, this mind state doesn't last.
 The mind state arises, "Oh, this is good."
 And they're like, "But wait, wait, wait. It's addictive. It
's not helping it."
 But this is good. The mind states are competing.
 So even though we know intellectually, the habit is still
 there, and the habit can overpower us.
 If we stop thinking like that, stop reminding ourselves,
 and stop looking, we will go right back to our...
 The habit will overtake us, and it will proliferate.
 It will multiply. It will become stronger as we focus on
 that instead.
 So it's not to say that just because we give rise to one
 moment of knowing,
 that suddenly we're going to change our minds about
 something.
 Suddenly our mind is going to give something up.
 It's the case that eventually the mind becomes convinced,
 and it changes its habit.
 It gives up the desire for suffering.
 As the Buddha said, "Ata nimindati du ke," the mind becomes
 bored or disenchanted with suffering.
 So this is why wisdom is considered to be beyond the doubt,
 the greatest thing that we can ever acquire in our lives.
 Not just intellectual knowledge, but the wisdom that comes
 from meditation,
 which is what we try to gain in whatever way we practice,
 that eventually we're trying to understand and give rise to
 wisdom,
 to see the things that are causing the suffering.
 Just to me, it's not some theoretical or esoteric or super-
mundane realization.
 It's actually quite mundane.
 By seeing the truth of the mundane, this is how we attain
 the super-mundane.
 We leave behind the mundane because we give it up.
 Once we see that this isn't a thing, that's not clean,
 nothing is worth clinging to,
 without going at all.
 Our minds become free, and this freedom is called super-
mundane.
 When the mind lets go of the mundane and leaves it behind.
 So this is why we practice the way we do.
 The reason why we use the mantra, besides just focusing the
 mind,
 there's another reason to it as well.
 As I said, the mind is broken.
 You're using this tool, and now we're using this tool to
 fix itself.
 We're using the broken mind to fix the broken mind, which
 is quite dangerous.
 It's easy to go astray.
 So many people, they practice meditation without a teacher,
 without any guidance whatsoever, will actually drive
 themselves crazy.
 Or it depends.
 Some people will get to a point where something strange
 happens,
 and they get afraid, and they stop practicing.
 This is one type of person.
 Another person gets to that point where they see something
 strange,
 and they become attracted to it, and they follow it, and
 they make more of it,
 and they drive themselves crazy.
 These two ways it happens.
 So you'll meet both kinds of person, or both kinds of medit
ator.
 People who have gotten very far off track, and have started
 talking to angels and ghosts,
 and even might commit suicide because a voice has told them
 to this.
 We had a monk who did this one, trying to commit suicide.
 And then you have people who just stop meditating.
 Don't ever think about meditating.
 "I got somewhere, it just frightened me so much, I don't
 ever want to do that again."
 So yeah, the key is to cultivate wisdom.
 So without this guide, the mantra is kind of this guide for
 us.
 So when you see something, instead of getting afraid of it,
 you know right away,
 "What is it? What do I do? What is this?"
 You know it's seeing.
 When you hear something, when you hear voices, you know it
's hearing.
 And you remind yourself of it being hearing.
 This is the tool which keeps you objective.
 It's like forcing yourself to be objective.
 Any crookedness in the mind, any brokenness in the mind is
 fixed
 because of this artificial interference, or this external
 interference.
 You're interested in the light, so you say to yourself,
 " interested, interested, or liking, liking, or curious,
 curious."
 And you cut it off.
 Instead of saying, "Oh, I'm interested in this, maybe that
's the way."
 Yes, this interest is a good thing.
 So you can never get off track.
 The idea with the mantra is that you'll never get off track
.
 When you have a name for everything,
 and as long as your names are in line with reality,
 you want to say, "Angel, angel," and you see an angel.
 This is not what's really there.
 When you use the mantra to focus on reality,
 when you see anything and say to yourself, "Seeing, seeing
,"
 it keeps you objective.
 It keeps you from becoming subjective,
 from identifying with or projecting on the experience.
 When you're thinking, it's only thinking.
 When you're feeling pain or aching, it's only pain or a
ching.
 When you like, it's only liking.
 When you dislike, it's only disliking.
 They don't mean that the object is good or bad.
 It just means that you like or dislike.
 So the use of the mantra keeps you not only focused but
 objective.
 It's kind of like the ultimate security.
 It's the only meditation that I can see that is perfectly
 sure for the meditator
 because it's very easy using many people who do go off
 track.
 You can actually keep track of your own mind and keep your
 own mind on track
 if you use this meditation correctly.
 It's not to say it's perfect and it's infallible.
 Some people do start saying, "Angel, angel," or "Wisdom,
 wisdom,"
 and drive themselves crazy because they're not practicing.
 They're not actually noting reality.
 They start using the word and driving them crazy just by
 using words for things.
 But the correct practice of this is incredibly beneficial
 in terms of reminding yourself, which is the core of what
 we mean by mindfulness.
 Mindfulness, the word sati means to remind yourself.
 So if I ask you, "Are you aware of what's going on right
 now?
 What's going on in your minds right now?"
 Often we're not.
 Often we don't really know what emotions we have in our
 minds.
 Are we happy?
 Are we bored?
 Are we unhappy?
 Are we worried or stressed or so on?
 We forget what's really going on.
 This mindfulness reminds us of what's really going on.
 This mantra, when you look and ask yourself,
 "What's going on in my mind?"
 and you see that there's stress and you say to yourself,
 "Stress, stress," or bored, bored, or liking, liking, or
 disliking, or so on.
 It brings you back.
 It reminds you, "Oh yes, this isn't a problem or this isn't
 a good thing.
 This is just emotions in my mind.
 My mind is reacting to the experience.
 It doesn't mean the experience is good or bad."
 So this is the purpose of the technique.
 And the idea is that this technique leads us, therefore, to
 wisdom and understanding
 because at that moment, at the moment when we note, we're
 objective.
 We don't see things with partiality.
 We don't put on rose-colored glasses or project our own
 ideas on things.
 We are just totally and brutally honest with ourselves.
 And we benefit from it.
 We reap the rewards.
 You see the benefits immediately that you're actually
 objective.
 You're no longer playing games with yourself.
 You're no longer lying to yourself.
 You're no longer cheating yourself or hurting yourself.
 You're no longer hating yourself.
 You're just watching yourself and studying yourself.
 The best way to look at meditation is it's a study.
 Well, there's two ways.
 I suppose it's either a cure, if you think of it in terms
 of the cure for suffering,
 or it's a study if you think of it as a study of what
 causes suffering,
 the study of the causes of suffering.
 So here we are studying ourselves for the purpose of
 affecting a cure for suffering.
 We're not just studying for the fun of it.
 We're studying because that's the cure.
 When you understand yourself, you free yourself from
 suffering.
 So this was what I wanted to say tonight.
 I think that's enough.
 I don't know.
 Does anyone have any questions they'd like to ask?
 Otherwise we'll just go into meditation.
 If you have questions, you can ask after meditation as well
.
 Anyway, we'll do a half an hour together now.
