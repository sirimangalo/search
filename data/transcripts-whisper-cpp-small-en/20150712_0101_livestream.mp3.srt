1
00:00:00,000 --> 00:00:14,000
 Okay, good evening. Broadcasting live. Live 11th.

2
00:00:14,000 --> 00:00:31,680
 Today's quote is about teaching. And it's interesting, as I

3
00:00:31,680 --> 00:00:38,000
 said, we've got the monk who looked up all the sources for

4
00:00:38,000 --> 00:00:39,000
 all these quotes.

5
00:00:39,000 --> 00:00:45,600
 And so when I read this one, I immediately thought there

6
00:00:45,600 --> 00:00:49,000
 was something fishy about it.

7
00:00:49,000 --> 00:00:55,000
 And so I looked it up, and indeed, it's not a direct quote.

8
00:00:55,000 --> 00:01:02,840
 It's actually an example of how dangerous it is to travel,

9
00:01:02,840 --> 00:01:10,000
 to trust, or how easy it is to misquote the Buddha.

10
00:01:10,000 --> 00:01:17,050
 It's actually almost correct, and that's the most dangerous

11
00:01:17,050 --> 00:01:18,000
 part.

12
00:01:18,000 --> 00:01:23,380
 But if you read this, I mean basically it says that only

13
00:01:23,380 --> 00:01:28,040
 when one is a teacher can one be a complete dhamma

14
00:01:28,040 --> 00:01:30,000
 practitioner.

15
00:01:30,000 --> 00:01:35,210
 Well, this quote actually comes from the Book of Tens, and

16
00:01:35,210 --> 00:01:39,000
 Guptarunikaya, where the Buddha gives ten qualities.

17
00:01:39,000 --> 00:01:46,110
 And so he says, "A person can have faith, but not be

18
00:01:46,110 --> 00:01:48,000
 immoral."

19
00:01:48,000 --> 00:01:54,740
 And in that case, one should practice morality, and then

20
00:01:54,740 --> 00:02:00,000
 they are complete in regards to that.

21
00:02:00,000 --> 00:02:06,800
 "Tang Angang, Dena Tang Angang Paripuritamam." Therefore,

22
00:02:06,800 --> 00:02:16,840
 then they should become full, or that factor should be

23
00:02:16,840 --> 00:02:22,000
 fulfilled by them.

24
00:02:22,000 --> 00:02:27,630
 Once it is fulfilled by them, then once they are faithful,

25
00:02:27,630 --> 00:02:36,270
 confident, and moral, then, "So, Ten Angang, Dena Paripurit

26
00:02:36,270 --> 00:02:37,000
am."

27
00:02:37,000 --> 00:02:41,410
 So they are complete, but they are complete Ten Angang, D

28
00:02:41,410 --> 00:02:45,000
ena, in regards to that Anga, that factor.

29
00:02:45,000 --> 00:02:45,930
 That's the crucial part that's left out of this quote,

30
00:02:45,930 --> 00:02:51,180
 because this quote in "Buddha Vatchana," which is the book

31
00:02:51,180 --> 00:02:53,000
 we're going by,

32
00:02:53,000 --> 00:02:59,460
 that quote simply states that when one is a teacher, one is

33
00:02:59,460 --> 00:03:01,000
 complete.

34
00:03:01,000 --> 00:03:06,550
 But what the text actually says is, when one is also a

35
00:03:06,550 --> 00:03:10,480
 teacher of dhamma, then one is complete in regards to that

36
00:03:10,480 --> 00:03:11,000
 factor,

37
00:03:11,000 --> 00:03:14,250
 which means they've got an extra factor that was lacking

38
00:03:14,250 --> 00:03:16,000
 when they were incomplete.

39
00:03:16,000 --> 00:03:21,000
 And teaching the dhamma is only in a list of them.

40
00:03:21,000 --> 00:03:24,010
 We have the believer, the virtuous, learned. It's only the

41
00:03:24,010 --> 00:03:26,000
 fourth in a list of ten.

42
00:03:26,000 --> 00:03:33,200
 So the actual text goes on and says, "One can be a teacher

43
00:03:33,200 --> 00:03:41,000
 of the dhamma, but is not one who frequents assemblies."

44
00:03:41,000 --> 00:03:44,000
 Pari-sawat-caro.

45
00:03:44,000 --> 00:03:51,210
 And it doesn't, not frequent, but it's one who doesn't get

46
00:03:51,210 --> 00:03:54,000
 involved with the community, you know,

47
00:03:54,000 --> 00:04:00,570
 like helping out and keeping up with work in the community

48
00:04:00,570 --> 00:04:02,000
 and so on.

49
00:04:02,000 --> 00:04:05,790
 Or maybe, no, Pari-sawat-caro goes about with the community

50
00:04:05,790 --> 00:04:12,000
, so has a retinue, has companions who they live with

51
00:04:12,000 --> 00:04:15,000
 and they practice with.

52
00:04:15,000 --> 00:04:19,590
 So this list is actually quite interesting, but it's not

53
00:04:19,590 --> 00:04:22,000
 saying that one has to be a teacher.

54
00:04:22,000 --> 00:04:28,510
 It's talking about a bhikkhu, in fact, talking about a monk

55
00:04:28,510 --> 00:04:29,000
.

56
00:04:29,000 --> 00:04:32,000
 So that's another thing that's missing here.

57
00:04:32,000 --> 00:04:38,000
 So there's something about the duty of a monk to teach,

58
00:04:38,000 --> 00:04:43,990
 and I think that is reasonable to assume, to suggest that

59
00:04:43,990 --> 00:04:56,000
 monks should try to teach, to be fully perfected, I guess,

60
00:04:56,000 --> 00:04:57,000
 as a monk.

61
00:04:57,000 --> 00:04:59,000
 One should also teach, speak the dhamma.

62
00:04:59,000 --> 00:05:08,000
 But the word is dhamma-kati-ko, one who speaks on a dhamma.

63
00:05:08,000 --> 00:05:13,110
 It's not one who, well, yeah, it is one who teaches the d

64
00:05:13,110 --> 00:05:18,660
hamma, who gives dhamma talks, basically, gives the dhamma

65
00:05:18,660 --> 00:05:20,000
 days.

66
00:05:20,000 --> 00:05:24,450
 So to be a perfect bhikkhu, one should be endowed with

67
00:05:24,450 --> 00:05:27,000
 faith, virtuous, learned,

68
00:05:27,000 --> 00:05:32,250
 a speaker of the dhamma, one who frequents assemblies, one

69
00:05:32,250 --> 00:05:36,000
 who confidently teaches the dhamma to an assembly,

70
00:05:36,000 --> 00:05:40,470
 an expert on the discipline, a forest dweller who resorts

71
00:05:40,470 --> 00:05:42,000
 to remote lodgings,

72
00:05:42,000 --> 00:05:45,000
 should be able to gain a will without trouble or difficulty

73
00:05:45,000 --> 00:05:46,000
, the four jhanas

74
00:05:46,000 --> 00:05:49,830
 that constitute the higher mind and are pleasant drawings

75
00:05:49,830 --> 00:05:51,000
 in this very life.

76
00:05:51,000 --> 00:05:54,560
 And finally, should, with the destruction of the taints,

77
00:05:54,560 --> 00:05:58,420
 realize for themselves, with direct knowledge in this very

78
00:05:58,420 --> 00:05:59,000
 life,

79
00:05:59,000 --> 00:06:03,200
 the taintless liberation of mind, some be free from the d

80
00:06:03,200 --> 00:06:04,000
hamma.

81
00:06:04,000 --> 00:06:06,460
 So number nine is one should be perfected in samatha

82
00:06:06,460 --> 00:06:07,000
 practice,

83
00:06:07,000 --> 00:06:12,000
 and number ten is one should be perfected in vipassana or

84
00:06:12,000 --> 00:06:17,000
 more directly enlightened in the destruction of dhammas.

85
00:06:17,000 --> 00:06:23,050
 So ten things here that, it's a really useful list, shows

86
00:06:23,050 --> 00:06:25,000
 what a perfect monk is.

87
00:06:25,000 --> 00:06:28,470
 And I don't think you could argue that this is a list that

88
00:06:28,470 --> 00:06:32,460
 is meant for all people because it's talking about the pat

89
00:06:32,460 --> 00:06:33,000
imoka, right?

90
00:06:33,000 --> 00:06:35,150
 It's talking about the discipline, it's talking about

91
00:06:35,150 --> 00:06:41,490
 living in the forest, which is not expected of even of all

92
00:06:41,490 --> 00:06:43,000
 monks.

93
00:06:43,000 --> 00:06:45,990
 But the Buddha says, "Pikko possesses these ten qualities

94
00:06:45,990 --> 00:06:50,990
 as one who inspires confidence in all respects and who is

95
00:06:50,990 --> 00:06:54,000
 complete in all aspects."

96
00:06:54,000 --> 00:06:57,350
 So there's lots of monks even who don't fulfill all ten of

97
00:06:57,350 --> 00:06:58,000
 these.

98
00:06:58,000 --> 00:07:01,430
 There were arahants who wouldn't necessarily have ten of

99
00:07:01,430 --> 00:07:04,000
 these, wouldn't have all ten of these.

100
00:07:04,000 --> 00:07:06,590
 So frequenting assemblies, having assemblies. There were

101
00:07:06,590 --> 00:07:08,000
 monks who lived alone.

102
00:07:08,000 --> 00:07:12,000
 But they would not be considered, even arahants, but those

103
00:07:12,000 --> 00:07:15,000
 arahants would not be considered perfect.

104
00:07:15,000 --> 00:07:18,000
 They would not be considered perfect in all aspects.

105
00:07:18,000 --> 00:07:24,480
 There's nothing blameworthy about it. It's just less

106
00:07:24,480 --> 00:07:28,000
 inspiring.

107
00:07:28,000 --> 00:07:32,340
 But anyway, let's go through them and we can talk about

108
00:07:32,340 --> 00:07:34,000
 these ten qualities.

109
00:07:34,000 --> 00:07:38,000
 Or maybe we should just focus on the idea of teaching.

110
00:07:38,000 --> 00:07:44,980
 I think the thought behind, or the intention behind this

111
00:07:44,980 --> 00:07:52,000
 misquote of the Buddha is fine, as noble.

112
00:07:52,000 --> 00:07:57,010
 What seems to be the intention is to suggest that teaching

113
00:07:57,010 --> 00:07:59,000
 makes one complete.

114
00:07:59,000 --> 00:08:03,710
 And without teaching, without sharing the Dhamma, one is

115
00:08:03,710 --> 00:08:05,000
 incomplete.

116
00:08:05,000 --> 00:08:13,260
 So it's a useful quote, if it were a quote, to show that we

117
00:08:13,260 --> 00:08:15,000
're not selfish.

118
00:08:15,000 --> 00:08:20,000
 We don't practice meditation just to help ourselves.

119
00:08:20,000 --> 00:08:25,600
 Helping others is a means of becoming a complete individual

120
00:08:25,600 --> 00:08:26,000
.

121
00:08:26,000 --> 00:08:30,540
 And in some sense, you can expand it more generally to say

122
00:08:30,540 --> 00:08:35,000
 that helping others is a part of our practice.

123
00:08:35,000 --> 00:08:38,530
 Doing good for others is something that helps yourself as a

124
00:08:38,530 --> 00:08:41,000
 part of your spiritual development.

125
00:08:41,000 --> 00:08:44,030
 So anyone who says spiritual development, people focus on

126
00:08:44,030 --> 00:08:46,000
 their spiritual development,

127
00:08:46,000 --> 00:08:50,660
 or they're only focused on themselves, it's not really true

128
00:08:50,660 --> 00:08:51,000
.

129
00:08:51,000 --> 00:08:56,000
 But someone who is truly spiritually developed is someone

130
00:08:56,000 --> 00:08:59,000
 who is at least willing to help others

131
00:08:59,000 --> 00:09:03,000
 at any chance they get. They may not have the opportunity.

132
00:09:03,000 --> 00:09:08,000
 They may find themselves living off in the forest.

133
00:09:08,000 --> 00:09:13,380
 But they have a willingness, and for the most part, in most

134
00:09:13,380 --> 00:09:17,000
 cases do help others greatly

135
00:09:17,000 --> 00:09:24,000
 and spend much of their time helping others.

136
00:09:24,000 --> 00:09:30,300
 Teaching is, I think a lot of people are afraid to teach a

137
00:09:30,300 --> 00:09:34,000
 lot of Buddhas,

138
00:09:34,000 --> 00:09:36,000
 thinking that you have to be someone great.

139
00:09:36,000 --> 00:09:43,000
 There was an interesting question posted on the internet,

140
00:09:43,000 --> 00:09:49,100
 on a stack exchange, Buddhism.stack exchange, just Denaya

141
00:09:49,100 --> 00:09:51,000
 yesterday.

142
00:09:51,000 --> 00:09:55,070
 Someone says, "I don't know if it was a meditation teacher,

143
00:09:55,070 --> 00:09:56,000
 but in a very brief question,

144
00:09:56,000 --> 00:09:59,360
 they say, 'My students' formal feedback about teaching

145
00:09:59,360 --> 00:10:01,000
 makes me feel defeated.'

146
00:10:01,000 --> 00:10:06,000
 I didn't reply to it, but immediately I can relate to that.

147
00:10:06,000 --> 00:10:11,090
 Teaching is an interesting thing. Your students teach you a

148
00:10:11,090 --> 00:10:12,000
 lot.

149
00:10:12,000 --> 00:10:17,000
 So people are afraid and have this belief that you need

150
00:10:17,000 --> 00:10:21,000
 some kind of formal training to be a teacher.

151
00:10:21,000 --> 00:10:24,250
 And the other people maybe take it too lightly to call

152
00:10:24,250 --> 00:10:29,000
 themselves teachers without having proper training.

153
00:10:29,000 --> 00:10:34,980
 I think there's no, the problem is you often hear about

154
00:10:34,980 --> 00:10:38,000
 this idea of being a qualified teacher,

155
00:10:38,000 --> 00:10:42,710
 as though there was a category, you could put yourself in a

156
00:10:42,710 --> 00:10:45,000
 category where you qualified.

157
00:10:45,000 --> 00:10:48,780
 And I think that's ridiculous. Teaching is about, teaching

158
00:10:48,780 --> 00:10:52,000
 meditation anyway is about experience.

159
00:10:52,000 --> 00:10:56,000
 So there's better teachers and there's worse teachers.

160
00:10:56,000 --> 00:11:01,000
 It's not a categorical leap to become a teacher.

161
00:11:01,000 --> 00:11:08,120
 I remember when I did my first course in meditation, the

162
00:11:08,120 --> 00:11:09,000
 layman who was my teacher,

163
00:11:09,000 --> 00:11:13,290
 a couple of days after I started, or a day after I started

164
00:11:13,290 --> 00:11:17,000
 maybe, he had me teach the next student

165
00:11:17,000 --> 00:11:20,130
 how to meditate, show him how to do the mindful prostration

166
00:11:20,130 --> 00:11:22,000
, the walking and the sitting.

167
00:11:22,000 --> 00:11:25,400
 So from my very first meditation course I was already a

168
00:11:25,400 --> 00:11:26,000
 teacher.

169
00:11:26,000 --> 00:11:33,000
 And that never stopped and it increased.

170
00:11:33,000 --> 00:11:39,660
 He was quite good in getting his students to teach, to give

171
00:11:39,660 --> 00:11:41,000
 them that.

172
00:11:41,000 --> 00:11:45,320
 I ended up leaving this teacher, got kind of turned off by

173
00:11:45,320 --> 00:11:47,000
 the way that he taught.

174
00:11:47,000 --> 00:11:52,530
 But he was great in organizing people. And he still is, he

175
00:11:52,530 --> 00:11:55,000
's got good organizations.

176
00:11:55,000 --> 00:12:03,250
 And so he taught me how to teach, told me stuff about the

177
00:12:03,250 --> 00:12:07,000
 16 stages of knowledge and how to recognize them.

178
00:12:07,000 --> 00:12:10,000
 It was quite helpful in the beginning.

179
00:12:10,000 --> 00:12:13,000
 I say now he's not my teacher anymore.

180
00:12:13,000 --> 00:12:16,600
 People got really upset about that because they're not

181
00:12:16,600 --> 00:12:18,000
 supposed to say that I guess.

182
00:12:18,000 --> 00:12:22,000
 But no, he's not my teacher. He's no longer my teacher.

183
00:12:22,000 --> 00:12:25,980
 But even when I went to practice, when I first started,

184
00:12:25,980 --> 00:12:29,360
 even before I switched to having to take Ajahn Tong as my

185
00:12:29,360 --> 00:12:30,000
 teacher,

186
00:12:30,000 --> 00:12:37,520
 how it started is exactly that. I was invited to sit in on

187
00:12:37,520 --> 00:12:40,000
 his teaching sessions.

188
00:12:40,000 --> 00:12:46,390
 And that's when I realized what I was missing, what a great

189
00:12:46,390 --> 00:12:50,000
 teacher Ajahn Tong really is.

190
00:12:50,000 --> 00:12:59,000
 Wonderful, amazing, hard to put into words.

191
00:12:59,000 --> 00:13:06,180
 A sort of person that you only meet once, less than once in

192
00:13:06,180 --> 00:13:12,000
 a lifetime. Not someone you meet every lifetime, for sure.

193
00:13:12,000 --> 00:13:16,000
 Any people would not meet such a great person.

194
00:13:16,000 --> 00:13:23,060
 Anyway, not to my own horn or something, but was lucky to

195
00:13:23,060 --> 00:13:27,000
 spend time with him.

196
00:13:27,000 --> 00:13:31,570
 But the point being that, from what I've seen, it's just

197
00:13:31,570 --> 00:13:34,000
 always been about learning how to teach more and better.

198
00:13:34,000 --> 00:13:40,460
 And so, when I first went back to university, I learned a

199
00:13:40,460 --> 00:13:42,000
 little bit about the knowledge.

200
00:13:42,000 --> 00:13:46,920
 I wasn't a monk yet, but some of my fellow students from

201
00:13:46,920 --> 00:13:51,000
 when I was at university the first time,

202
00:13:51,000 --> 00:13:56,110
 one of them came up to me and asked me if I could tell him

203
00:13:56,110 --> 00:13:59,000
 something about meditation.

204
00:13:59,000 --> 00:14:03,000
 And I actually led him almost through an entire course.

205
00:14:03,000 --> 00:14:07,610
 I'm doing it over a period of time. He didn't actually come

206
00:14:07,610 --> 00:14:10,000
 and stay in the center or something,

207
00:14:10,000 --> 00:14:12,400
 but he was practicing at home and I led him through the

208
00:14:12,400 --> 00:14:15,000
 stages. And it changed his life.

209
00:14:15,000 --> 00:14:19,780
 He was a biologist and he was cutting live mice and

210
00:14:19,780 --> 00:14:23,000
 performing experiments on them.

211
00:14:23,000 --> 00:14:26,690
 It really affected his meditation. He came to me and he

212
00:14:26,690 --> 00:14:29,350
 said, "Look, I don't know what to do. I just feel awful

213
00:14:29,350 --> 00:14:30,000
 about this.

214
00:14:30,000 --> 00:14:38,360
 I feel like I'm stuck. I don't want to be killing animals

215
00:14:38,360 --> 00:14:40,000
 anymore.

216
00:14:40,000 --> 00:14:43,040
 And I know it's affecting my meditation. I said, "Well, you

217
00:14:43,040 --> 00:14:44,000
 have to choose."

218
00:14:44,000 --> 00:14:48,000
 I told him straight up that he has to choose if he wants to

219
00:14:48,000 --> 00:14:51,000
, because there are two different paths.

220
00:14:51,000 --> 00:14:55,270
 And so he had to drop out of biology, switch his major over

221
00:14:55,270 --> 00:14:58,000
 to political science or something.

222
00:14:58,000 --> 00:15:00,370
 He now works for the Canadian government, I think. Last I

223
00:15:00,370 --> 00:15:07,000
 heard him. I'm not sure, actually.

224
00:15:07,000 --> 00:15:13,090
 But that from my own basic, basic experience, knowledge and

225
00:15:13,090 --> 00:15:15,000
 thought I teach.

226
00:15:15,000 --> 00:15:18,340
 From then on, and there was another man as well who did the

227
00:15:18,340 --> 00:15:21,000
 same thing with these two guys.

228
00:15:21,000 --> 00:15:26,940
 And this other guy, he'd gone all over Ontario, which is

229
00:15:26,940 --> 00:15:34,000
 admittedly not a hotbed of Buddhism or wasn't 15 years ago.

230
00:15:34,000 --> 00:15:38,980
 But he went to all different centers, maybe even outside of

231
00:15:38,980 --> 00:15:40,000
 Ontario.

232
00:15:40,000 --> 00:15:44,550
 But he told me this. He said, "You know, I haven't found

233
00:15:44,550 --> 00:15:48,190
 anything that is so clear and simple as what you're

234
00:15:48,190 --> 00:15:50,000
 teaching.

235
00:15:50,000 --> 00:15:54,200
 And I'm not trying to brag. It's nothing like that. And

236
00:15:54,200 --> 00:15:57,000
 that's the opposite of the intention here.

237
00:15:57,000 --> 00:16:02,020
 The intention is to show that if a teaching is good, it

238
00:16:02,020 --> 00:16:05,000
 gives itself to some extent.

239
00:16:05,000 --> 00:16:09,850
 The important point is to know your limits, not to reject

240
00:16:09,850 --> 00:16:12,000
 the concept of teaching.

241
00:16:12,000 --> 00:16:15,250
 That's my point. It's not categorical where you say, "I'm

242
00:16:15,250 --> 00:16:18,000
 not a teacher, so I will not teach anything."

243
00:16:18,000 --> 00:16:22,000
 There were two women who came to practice with Ajahn Tong,

244
00:16:22,000 --> 00:16:24,000
 and I was translating for them, I think.

245
00:16:24,000 --> 00:16:28,140
 And they stayed maybe five days, maybe five to seven days,

246
00:16:28,140 --> 00:16:29,000
 I think.

247
00:16:29,000 --> 00:16:33,720
 I'll never forget them because when they left, Ajahn turned

248
00:16:33,720 --> 00:16:35,000
 to them and said,

249
00:16:35,000 --> 00:16:40,120
 "I want you to," or I translated, so he said to them, "I

250
00:16:40,120 --> 00:16:43,000
 want you to teach exactly as you've been taught.

251
00:16:43,000 --> 00:16:46,340
 So take what you've learned here and go back and teach it

252
00:16:46,340 --> 00:16:50,000
 to people in your country just as it's been taught to you."

253
00:16:50,000 --> 00:16:56,000
 And that's sort of the attitude, as far as I've seen.

254
00:16:56,000 --> 00:16:59,650
 It's not a problem to let someone teach or to encourage

255
00:16:59,650 --> 00:17:01,000
 people to teach.

256
00:17:01,000 --> 00:17:05,810
 The point is to have people know their limits. It's easy to

257
00:17:05,810 --> 00:17:07,000
 go overboard.

258
00:17:07,000 --> 00:17:11,200
 I've seen people who put themselves up as teachers and

259
00:17:11,200 --> 00:17:13,000
 claim to know many, many things,

260
00:17:13,000 --> 00:17:20,930
 but are really unqualified to do what they do and end up

261
00:17:20,930 --> 00:17:24,000
 misleading their students.

262
00:17:24,000 --> 00:17:29,480
 Another thing is to be able to be willing to learn from

263
00:17:29,480 --> 00:17:31,000
 your students

264
00:17:31,000 --> 00:17:38,020
 and not to not get upset and feel defeated when your

265
00:17:38,020 --> 00:17:44,000
 students hand you your ego on a platter.

266
00:17:44,000 --> 00:17:51,000
 I had one student throw candy at me.

267
00:17:51,000 --> 00:17:54,370
 I've had students yell at me. I had to student write me a

268
00:17:54,370 --> 00:17:55,000
 letter.

269
00:17:55,000 --> 00:18:01,850
 I had a whole group of students. I was very intent upon

270
00:18:01,850 --> 00:18:03,000
 having my students not talk.

271
00:18:03,000 --> 00:18:08,000
 They were so awful. All these tourist kids in Tsing Mai,

272
00:18:08,000 --> 00:18:11,000
 and they were just talking all the time.

273
00:18:11,000 --> 00:18:17,000
 I was very strict trying to stop them from talking.

274
00:18:17,000 --> 00:18:20,000
 And I had a whole group of them, ten of them, I think.

275
00:18:20,000 --> 00:18:25,000
 It's just all decided that they'd had enough of me. They

276
00:18:25,000 --> 00:18:25,000
 didn't even know each other.

277
00:18:25,000 --> 00:18:29,040
 But they all came together and decided that they were

278
00:18:29,040 --> 00:18:30,000
 leaving.

279
00:18:30,000 --> 00:18:35,000
 And it was devastating to have this happen.

280
00:18:35,000 --> 00:18:40,000
 I've been there, done that, had it all happened.

281
00:18:40,000 --> 00:18:44,000
 But the point is it helps people.

282
00:18:44,000 --> 00:18:50,900
 I'm not the number one teacher out there. Certainly not. I

283
00:18:50,900 --> 00:18:52,000
'm not in the top ten.

284
00:18:52,000 --> 00:18:56,100
 But I can see that teaching helps people. That's what's

285
00:18:56,100 --> 00:18:58,000
 always kept me going.

286
00:18:58,000 --> 00:19:01,000
 And I can also see that it helps me.

287
00:19:01,000 --> 00:19:04,000
 Teaching is something that helps the individual as well.

288
00:19:04,000 --> 00:19:07,420
 It helps them in their own practice. It keeps them on their

289
00:19:07,420 --> 00:19:08,000
 toes.

290
00:19:08,000 --> 00:19:11,000
 It keeps them training. It keeps them accountable.

291
00:19:11,000 --> 00:19:15,000
 It can. It doesn't have to. It's all in how you look at it,

292
00:19:15,000 --> 00:19:20,000
 as with everything.

293
00:19:20,000 --> 00:19:23,000
 It's all in your sincerity.

294
00:19:23,000 --> 00:19:26,030
 Why are you teaching? Are you teaching because you want to

295
00:19:26,030 --> 00:19:27,000
 be a teacher?

296
00:19:27,000 --> 00:19:31,020
 Admittedly, I guess some part of me teaching is as a duty

297
00:19:31,020 --> 00:19:32,000
 because if I don't teach,

298
00:19:32,000 --> 00:19:35,480
 I probably won't get fed and I probably won't have a place

299
00:19:35,480 --> 00:19:38,000
 to stay and that kind of thing.

300
00:19:38,000 --> 00:19:42,930
 That's not necessarily true, but teaching allows me some

301
00:19:42,930 --> 00:19:44,000
 support.

302
00:19:44,000 --> 00:19:49,000
 So there is that.

303
00:19:49,000 --> 00:19:52,380
 If you're teaching to just become a big teacher or if you

304
00:19:52,380 --> 00:19:55,000
're teaching to become rich and famous,

305
00:19:55,000 --> 00:19:59,000
 then that's a problem.

306
00:19:59,000 --> 00:20:01,000
 It's a good reason to stop.

307
00:20:01,000 --> 00:20:05,720
 But there's no good reason to say, I mean, anyone can teach

308
00:20:05,720 --> 00:20:06,000
.

309
00:20:06,000 --> 00:20:09,570
 Everyone's practice can teach. They should teach as they've

310
00:20:09,570 --> 00:20:10,000
 learned.

311
00:20:10,000 --> 00:20:11,920
 To the extent that you've learned it, to the extent that

312
00:20:11,920 --> 00:20:13,000
 you understand it,

313
00:20:13,000 --> 00:20:16,000
 to that extent you should teach it.

314
00:20:16,000 --> 00:20:21,000
 The story of Asaji, one of the first five Arahants,

315
00:20:21,000 --> 00:20:25,000
 disciples of the Buddha.

316
00:20:25,000 --> 00:20:29,000
 And Sariputta, Upatissa, who was to become Sariputta,

317
00:20:29,000 --> 00:20:33,000
 came up to him in the streets when he was unarmed

318
00:20:33,000 --> 00:20:36,000
 and asked him to teach.

319
00:20:36,000 --> 00:20:39,000
 And Asaji said, "No, I can't teach. I don't know anything.

320
00:20:39,000 --> 00:20:43,000
 I'm new to this religion."

321
00:20:43,000 --> 00:20:48,000
 And Sariputta said, "I won't be able to teach in detail."

322
00:20:48,000 --> 00:20:50,690
 He said, "Just teach me something brief. That's all I need

323
00:20:50,690 --> 00:20:51,000
."

324
00:20:51,000 --> 00:20:54,000
 And so he taught him this very famous verse.

325
00:20:54,000 --> 00:21:10,440
 "Dhamma hitubhavavahi, teisang hitu

326
00:21:10,440 --> 00:21:15,000
 dattaagata ahu teisanchayo nirodo ja evangwadi mahasamam."

327
00:21:15,000 --> 00:21:18,000
 I forgot that quote exactly right.

328
00:21:18,000 --> 00:21:21,770
 No, he was an Arahant, so he's fully qualified to speak

329
00:21:21,770 --> 00:21:26,000
 about the dhamma as he's understood it.

330
00:21:26,000 --> 00:21:30,690
 But there's a pattern there, the idea of teaching what you

331
00:21:30,690 --> 00:21:31,000
 know,

332
00:21:31,000 --> 00:21:33,000
 teaching what you understand.

333
00:21:33,000 --> 00:21:36,000
 And even in the way of the Sudhyamaga it says,

334
00:21:36,000 --> 00:21:39,000
 "You don't have to be enlightened to teach."

335
00:21:39,000 --> 00:21:41,440
 Someone who knows a lot of the dhamma, who is an

336
00:21:41,440 --> 00:21:42,000
 enlightened,

337
00:21:42,000 --> 00:21:45,000
 can often help others to become enlightened.

338
00:21:45,000 --> 00:21:49,000
 He can help themselves. There's a story of a teacher who,

339
00:21:49,000 --> 00:21:52,060
 all his students became enlightened, and he was still an

340
00:21:52,060 --> 00:21:53,000
 ordinary person,

341
00:21:53,000 --> 00:21:55,000
 so he ran off into the forest.

342
00:21:55,000 --> 00:21:58,000
 He was such a great teacher and so famous and well known

343
00:21:58,000 --> 00:22:01,480
 that the angels started practicing with him while he was in

344
00:22:01,480 --> 00:22:02,000
 the forest.

345
00:22:02,000 --> 00:22:04,760
 When he broke down and cried, one of the angels started

346
00:22:04,760 --> 00:22:05,000
 crying,

347
00:22:05,000 --> 00:22:07,000
 and he said, "Why are you crying?"

348
00:22:07,000 --> 00:22:09,330
 And he just said, "Well, I'm doing it because I know you're

349
00:22:09,330 --> 00:22:10,000
 such a great teacher.

350
00:22:10,000 --> 00:22:15,320
 I thought that must be the path to enlightenment." So he

351
00:22:15,320 --> 00:22:18,000
 cried.

352
00:22:18,000 --> 00:22:21,940
 So it's about a perspective. No one should be afraid of

353
00:22:21,940 --> 00:22:23,000
 teaching.

354
00:22:23,000 --> 00:22:26,020
 The problem only comes when we teach beyond our

355
00:22:26,020 --> 00:22:27,000
 capabilities

356
00:22:27,000 --> 00:22:39,000
 or when we teach for the wrong reasons, these two things.

357
00:22:39,000 --> 00:22:43,000
 So when we are all made teaching and not practicing, for

358
00:22:43,000 --> 00:22:43,000
 example.

359
00:22:43,000 --> 00:22:46,000
 Anyway, I think I'm just going to talk there.

360
00:22:46,000 --> 00:22:48,520
 That's a little bit about teaching, the idea of teaching as

361
00:22:48,520 --> 00:22:50,000
 an encouragement for people.

362
00:22:50,000 --> 00:22:54,000
 Something to think about.

363
00:22:54,000 --> 00:22:57,680
 So that's the dhamma for this evening. Thanks for tuning in

364
00:22:57,680 --> 00:22:58,000
.

