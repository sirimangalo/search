 Hello and welcome back to our study of the Dhammapada.
 Today we continue on with first number ninety, which reads
 as follows.
 katadino vuiso kasa vipumutasa sabadi sabagantha pahinasa
 parilaho nujati
 katadino, for one who has completed their journey,
 completed the going, viso kasa, one who is free from sorrow
,
 vipumutasa sabadi, one who is liberated everywhere, who is
 everywhere free, everywhere they go.
 sabagantha pahinasa, one who has abandoned all fetters, all
 forms of clinging, every object of attachment,
 parilaho nujati, no fever can be found, no distress,
 distress is probably the best translation, parilaha.
 katadino vuiso kasa, very beautiful little verse, it talks
 about fever, parilaha is often translated as fever,
 that's probably etymologically fairly correct, burning
 right, but in this specific instance,
 because of the story and because of the context of the
 verse itself, it's not a fever, it's a mental
 fever, a mental burning, so we would translate it as
 distress or fever of passion,
 but literally it means burning I think, daha, but in daha
 becomes parilaha.
 So the story, this was told in regards to a question posed
 by Dibhaka, who was a doctor,
 the doctor who looked after the Buddha, and his story is
 not told here, I can't remember if it's
 actually told in the Dhammapada, I don't think so, maybe in
 the Jataka it occurs, but in the Vinaya
 definitely you find his story, he was a master doctor, an
 exceptional medicine man, they say that
 the final task given to him by his teacher was to stake out
 a 16 kilometer by 16 kilometer square,
 piece of land which is a league, a square league,
 yojana of land, and bring to his teacher every plant in the
 entire square league
 that had no medicinal properties.
 And this was the task, so he set out to accomplish this
 task, came back some time later and said to
 the teacher, there are no such plants, there are no plants
 in that 16, I couldn't find any plants
 that were without medicinal properties, and that's how his
 teacher knew that he passed,
 because he was able to identify medicinal properties in all
 plants,
 it's the story, so he was exceptional, he got,
 he caused a little bit of trouble, or he got into a little
 bit of trouble, not of his own,
 not on his own, he was actually an awesome, wonderful
 person, he donated a mango grove
 to the Buddha, and you can still go and see the place that
's supposed to be this,
 where this mango grove was, right at the bottom of Vulture
's Peak, because
 Vulture's Peak was difficult to go up constantly to take
 care of the Buddha, so in order to
 make it easier for him to take care of the Buddha when the
 Buddha was sick,
 he would, and also to make it easier for the Buddha to go
 on alms around when the Buddha was
 ill, he gave him this mango grove at the bottom of the hill
, the bottom of the mountain.
 But as he was looking after the Buddha, he also took on the
 role of looking after the monks,
 and as a result of taking, paying so much attention, taking
 so much time to look after the Sangha,
 his other patients started to miss him, and so people who
 were sick would actually ordain as
 monks, just to try and get this level of care, and this was
 actually the reason why the Buddha
 stated the rule that someone who is sick, someone who has
 this sickness, has any
 terrible sickness, shouldn't be allowed to ordain. I mean
 there are other reasons for it,
 obviously, it's problematic, but it's one of those things
 that leads people to ordain for the wrong
 reasons, and this happens in Buddhist countries where they
 become lax about this, and really
 these people become a burden on the monastic Sangha rather
 than actually promoting Buddhism
 and doing good things. So there's a few stories about him,
 but this story actually concerns
 Devadatta in tangentially, because the story goes that Dev
adatta was trying all sorts of ways to
 kill the Buddha. Devadatta was the Buddha's cousin, and he
 got upset and jealous of the Buddha, and he
 wanted to be the leader of the Sangha, just kind of an all-
around mean and nasty sort of fellow.
 So he sent an elephant after the Buddha, and the Buddha t
amed the elephant, it was his mad
 elephant that was supposed to trample the Buddha, and the
 Buddha tamed him with loving kindness.
 He sent some archers after the Buddha, some mercenaries
 after the Buddha, and they all
 converted to become monks. And finally he just got fed up,
 and as the Buddha was coming down
 from Vulture's Peak, he dropped a rock, a big boulder on
 the Buddha trying to crush him
 from up on high. And as the rock was falling down, it hit
 some sort of outcropping, of course,
 because you can't kill the Buddha, it just doesn't happen.
 This karma is too good. But a little
 splinter, a small splinter, broke off. The rest of the rock
 veered out of the way and didn't come
 close to the Buddha, but a small splinter came and hit the
 Buddha's foot. And Jivaka looked after the
 Buddha, the Buddha lay down and Jivaka bandaged him up or
 put some poultice on the wound. I think
 the commentaries tried to say that he didn't actually bleed
 because it's impossible to make
 the Buddha bleed, but it's about the translation as to what
 exactly is meant to happen anyway.
 Whatever, the Buddha was injured and he put some herbs on
 it, something strong that would
 take the pain away and make the swelling go down, but it
 was quite strong.
 And then Jivaka had to go into the city to look after his
 patients in the city.
 And he was late, he was looking after some rich person or
 maybe even the king, and he was late
 coming back and they closed the gates to the city before he
 could get out. And so he had to stay all
 night in the city and he was worried all night and
 concerned because he had to go back and take this
 poultice off of the Buddha's ankle or else it would cause
 harm. It would actually cause harm to the
 Buddha. And so he was unable to let the Buddha know.
 Fortunately, that's what, that's the benefit
 of having all sorts of supernatural powers. The Buddha was
 able to discern that it was time to
 take the poultice off and had Ananda help him take it off
 and waited there for Jivaka to come.
 So no harm done. But harm that was done to Jivaka who was
 worrying and fretting, who was distressed.
 Now Jivaka was actually a Sottapan. I think at this time he
 was already a Sottapan,
 although I'm not sure of the actual timeline to have to
 look deeper into that. But I think
 he was already a Sottapan at this point. So he was already
 in an Ariya Bhugala, but he was still
 subject to distress. And so he fretted about this for the
 long time that he was kept in the city.
 And finally when the gates opened in the morning, he ran
 over, ran, sprinted his way over to Vulture's
 Peak and came to the Buddha and asked him, I'm sorry Vener
able Sir, that I wasn't able to come,
 were you distressed by the pain? Were you distressed by
 your illness?
 And that's where this verse comes from. Jivaka asked him
 this simple question
 and the Buddha said, "For one who has gone to the end, who
 is free from sorrow,
 one who is free everywhere, who has become liberated
 everywhere, who has abandoned all
 ties, parilahu namichati, there is no fever to be found, no
 distress to be found." The word fever is
 too ambiguous, but whenever becomes distressed. So this was
, I think last night we had the question
 whether an Arahant can suffer, whether it can actually be
 called suffering when an Arahant
 feels pain because they don't have, and this is what they
 don't have. The translation that we've
 got in the book actually says it translates parilaha as
 suffering, but that's probably not
 accurate. It's again too ambiguous, too general. The
 specific meaning is burning up, so it's often
 referring to the body, but here it means burning up in the
 mind. Were you distressed? Did it
 distress you? Upset? Did it upset you? Were you upset by it
? Were you distressed by it? Were you
 vexed by it? And the Buddha said there is no vexation for
 one who has become free.
 And so that's the answer to that question, that they do
 actually feel dukkavedana in the body,
 but they're not vexed by it. A very important point that
 all of us as meditators, hopefully by
 this time, have heard or have come to be familiar with, but
 it's a point that has to be made
 especially for people who are new to the meditation. The
 difference between not only physical suffering,
 but external conditions like the state of of ji-vaka who
 was stuck in the city was incredibly vexed
 and disturbed and distressed. You know, he had a good heart
, but he wasn't helping the Buddha by
 being vexed and distressed by being upset. And in fact he
 was simply doing damage to his own mind.
 And so to some extent the Buddha was using this as a lesson
 to ji-vaka as well, not only
 to reassure him, but also to remind him not to get upset. I
 wasn't upset, why did you get upset?
 And so this is because of the difference between the
 physical, the external and our reactions.
 Pain doesn't have to be a bad thing, excruciating pain
 doesn't have to be a bad thing.
 Situations don't have to cause us anger and frustration and
 upset or boredom or worry or fear.
 And you know when people do things to us, when others hurt
 us,
 we do the worst thing when we respond with anger. When
 someone gets angry at us and we respond with
 anger, we're the worst. We are creating the worst evil,
 Buddha said.
 But this is a clear indication of the difference between
 our experiences and our reactions to them. So what do we
 have? One who has gone to the end
 means, actually I don't know, gat adino, that's translated
 as one who has gone to the end of the
 journey. Yeah, well that's what they say here, but let's
 see exactly what it means.
 Adin, one who has gone the distance, so who has completed
 the path.
 I think a meditator is asking how you know how far you're
 progressing and it's all this idea of
 the path and I'm often cynical about whether we should
 really focus too much on the idea of a path.
 It's misleading I think, you know. I think it's fine to
 talk about the idea of a path,
 but when we are concerned about our progress it can become
 quite obsessive
 as we're worried about where we are, wondering when we're
 going to get there, that kind of thing.
 On the other hand, having the idea of the path is a
 reminder that we can't be complacent,
 that we do have to walk and journey, but it's important as
 a teacher and as a meditator to
 not be too focused on progress. So this idea of the path or
 the journey, it's poetic and it's
 useful to know that there is a journey, but when you're
 walking a journey you don't want to be the
 person in the back seat saying, "Are we there yet? Are we
 there yet?" That part of the mind should be
 silenced. The walking has to continue. All you have to do
 is put one foot in front of the other.
 You know that there's a path and you follow it. It's much
 simpler than we think. We think of it
 as being some kind of like a paved road with road signs
 telling you 10 kilometers left or something
 like that, but it's not really. It's a path we've never
 gone and the path that has no clear signs
 that we can distinguish because we have nothing, no frame
 of reference.
 So our job is yes to follow the path, but that simply means
 putting one foot in front of the
 other, metaphorically speaking. We Soka, so an enlightened
 being has no Soka, no sadness,
 no sorrow. They don't long for anything. They don't pine
 away at loss.
 They don't worry about losing the things that they have. No
 Soka, no sadness.
 "Uipamutta" says somebody in all directions, on all sides,
 everywhere. "Uipamutta"
 freed, liberated, completely liberated. "Sabhaganta" bahin
asa, having abandoned or destroyed
 or gotten rid of all binds or ties, severed all ties.
 So people talk a lot about detachment, how Buddhism is a
 clear indication of how Buddhism does
 encourage the idea of detachment. But people get the wrong
 idea that it somehow translates into a
 sense of or a state of zombie-like nothingness or meaning
lessness. All it means is to not cling to
 things as they go by, because the problem is we have this
 idea that things exist constantly or in
 some stable form. And so we're talking about shying away
 from them, that people think non-attachment
 means it's there, don't go near it. But that's not it. It's
 not there actually. What is there is a
 moment and that arises and ceases. That's fine. Be as close
 and as clearly aware of that as you can
 be. That's the point. But when it's gone, don't go with it,
 you know. Don't let it pull you into the
 past. Don't let thoughts about the future pull you into the
 future. You end up like this, being
 pulled to both sides, past and future. And you're never
 really here and now. That's what non-attachment
 means. It would make sense if there were things here that
 were stable to attach to them.
 Because you could depend upon them, right? But there's
 nothing like that. Nothing is like,
 is of that sort. Everything arises and ceases, comes and
 goes, uncertain. Even all the possessions
 that we have, that we think we have constantly throughout
 our lives, the people who are with us.
 The reason we're so sad is because we take them as some
 stable entity that can be controlled and
 that is long lasting and so on. And so we bind ourselves to
 them with this attachment.
 Non-attachment or detachment. Non-attachment is a better
 way of phrasing it. Non-attachment means
 experiencing. But experiencing as experience. Seeing people
 as individual experiences. Seeing
 things as individual experiences because that's what they
 are. That's what you really have.
 How can you say you have a car when you don't see it, when
 you don't hear it,
 when you're not in front of it, when it's not in front of
 you? How can you say that you even have
 people in your life when they're not here and now? All you
 have is a thought that arises and ceases,
 even when they're with you. All you have is seeing, hearing
, smelling, tasting, feeling and thinking.
 So non-attachment just means seeing things as they are and
 being with things. So being with,
 there's people around you, there's people around you. If
 there's no one around you, there's no one
 around you. It's not pining away after something that isn't
 there or clinging to something that is.
 For such a person who is of this sort, there is no fever.
 So what it means is they have no
 likes or dislikes, they have no vexation and no upset when
 things don't go as expected because
 they have no expectations. They're perfectly at peace and
 perfectly flexible. I think flexible
 is a very important aspect. Just naturally flexible because
 they have no preference.
 They're content the way things are however they are. So
 that's the Dhammapada for tonight.
 Thank you all for tuning in and keep practicing and be well
.
