 Good evening everyone.
 Welcome to our live broadcast.
 His quote is from the commentary to the Dariya Pitika,
 which means it's not actually the
 Buddha's teaching.
 It's not the Buddha himself, it's not his words.
 It's describing the quality of love from the point of view
 of someone trying to become
 a Buddha, which means with the aim to become enlightened by
 one's own power and then teach
 a world that is unaware, unable to practice, unfamiliar
 with the Buddha's teaching.
 We teach in a time when there is no Buddha to be the Buddha
.
 That's the Dariya Pitika is about.
 Someone wants to become a Buddha, one needs love and he
 says because love, they say because
 love is the basis of compassion and compassion is really
 the basis of Buddhahood.
 What they mean by that is it's not actually the basis of
 Buddhahood is wisdom.
 Buddha means one who knows, one who is awake, but either
 way one who sees clearly.
 But the difference between someone who strives to become a
 Buddha and someone who just strives
 to become enlightened is the Buddha puts aside their own
 enlightenment for the benefit
 of all beings.
 Fully enlightened Buddha is someone who has put aside their
 own benefit.
 They have undertaken to really understand the teaching
 without the help of others to
 be able to become a Buddha in a time when there is no
 Buddha and therefore be of maximum
 benefit to all beings.
 That requires compassion.
 Compassion is the reaction to someone's suffering or the
 relation to someone's suffering.
 Love is similar but technically in relation to a person's
 happiness.
 So if you wish someone to be happy, that's love.
 If you wish someone to not suffer, that's compassion.
 That's why they're so similar.
 That's why love is the basis for compassion.
 They're very similar.
 Anyway, let's jump right into questions.
 Don't deal with love too much.
 I know everyone loves to talk about it and a lot of these
 quotes are about love but it
 seems we get them again and again.
 Really love is not all you need and it's not the focus.
 So we'll focus on it.
 Thank you.
 I'm going to go back as far as we can here.
 First of all, let's look at some comments.
 Make sure we're actually broadcasting.
 People are actually hearing us.
 Okay, questions.
 I'm only going to go through questions that are actually
 using the proper notation.
 There's a lot to go through.
 When I see things clearly, I notice patterns.
 Is this samsara?
 Samsara means wandering on or transmigration.
 Samsara means, actually I'm not sure exactly, samsara, the
 means to wander.
 But samsara is everything we experience.
 All sankaras are included in the understanding of what sams
ara.
 The classic quote is, samsaditang chattu nang, chattu nang,
 reh satchanang, yata bhutang,
 samsaditang, dheegamatanang, tasutasvevajati.
 We're not seeing the Four Noble Truths.
 Samsaditang, samsaditang.
 Samsaditang is the verb for dheegamatanang.
 When one wanders on, I have wandered on.
 Dheegamatanang means for a long time.
 Tasutasvevajati.
 From that life to the next.
 That's what samsara is.
 Samsara is this wandering one.
 Because we don't see the Noble Truths.
 We cling and we cling again and again and again and we
 cling and as a result we're
 born again and again.
 Why is metta considered a summit of practice when it
 focuses on many things?
 It's not about one thing or many things.
 It's about reality or concept.
 Metta is focused on a concept.
 I mean the purpose of metta is not to gain wisdom.
 The purpose of metta is to cultivate love.
 Samatha just makes you calm and that's what metta does.
 Metta makes you calm and focused and happy.
 It doesn't lead to understanding about reality because it's
 not focused on reality.
 It's focused on beings which are concepts.
 Beings don't exist.
 Beings are just a thought in our mind.
 You look at something and you say, "That's a being there."
 You think about yourself and you think, "Oh, I'm a being."
 But actually a being is not real.
 All that's real is experience.
 Why is a big stuck on samsara if you have children?
 It sounds like something I said, you know?
 When I said that, when you have children, it's big stuck,
 big stuck, really stuck.
 I mean technically having children is just an experience
 and you can do it without getting
 attached to it but it doesn't usually happen that way,
 right?
 Even if you are unattached, I mean it's a lot of work and
 effort that it takes away from
 your spiritual practice to have to deal with beings who
 there's no reassurance that they're
 going to be spiritually inclined and for the most part they
're not spiritually inclined
 because they don't understand spirituality.
 Those results, you're dealing with people who are craving
 and intense craving and intense
 wickedness at times.
 You have to deal with that and you have to live with that.
 You have to love that which is difficult.
 Being children is a very difficult thing.
 I have one student, a long time student from Thailand and
 she got pregnant in between meditation
 courses.
 She was even on birth control.
 Her husband was cheating on her and found a new wife and
 wanted divorce.
 She was a very powerful meditator, a strong meditator and
 she had to raise this child
 and she's had to go through a lot.
 She's done it mindfully and she's done quite well I think
 but she had to deal with a child
 who was very much her own being.
 It's a lot of work.
 What are your thoughts on true kindness?
 It seems to be taken as unkind if you point out a mistake.
 Some people I'm around.
 So kindness seems to be based heavily on sugarcoating and
 not being direct and upfront when they're
 scrubbing your mistake.
 It's your nature of being kind.
 There is tough love.
 Buddhism isn't about solving other people's problems per se
.
 You can't really fix other people.
 As you see the attempt to try and fix other people turns
 out to have a negative consequence.
 People react badly to your, I suppose, kindness.
 Kindness is one of these things like love and compassion.
 It's not enough and it's not always going to solve your
 problems.
 It's certainly not going to solve all your problems or
 solve other people's problems.
 If you have compassion for someone else it doesn't mean you
're going to fix their problems.
 Wisdom is really what we are focused on and what we require
.
 Wisdom then you're able to see things much clearer and more
 clearly.
 You don't get caught up in trying to fix your own problems
 or other people's problems.
 You learn to let go and not see things as a problem but see
 them as reality.
 See them as something to be left alone rather than fixed
 and controlled and so on.
 I don't know.
 It doesn't really answer your question, I suppose, but I
 wouldn't be too inclined to
 try to fix other people's problems or do anything that
 might upset them.
 You should have said you should upset people at times but
 you have to know when to upset
 people.
 There are times when it's proper for someone to be upset
 but it would have to be something
 that's beneficial to them and true.
 Beneficial and true but upsetting.
 This is a dangerous one.
 It's easy to hurt someone by upsetting them.
 It's easier to do harm to someone.
 It's more likely that if you're upsetting them you're going
 to do them harm no matter
 if you're well-intentioned or not.
 You have to be careful with that one.
 Something is pleasant and is true and beneficial and
 pleasant, well then, there's no question.
 It's something that they'll take to heart and it will be
 pleasing to them.
 Then go ahead.
 If you're caught in their head it's pleasing but not
 beneficial and don't go there either.
 Is placation, sugarcoating and/or babying actually
 beneficial or true kindness?
 Can be.
 If you give people confidence, if you give people, if you
 calm people down, you know,
 there was this guy once, Sariputta went to, I think we've
 done this in the Dhammapada,
 this executioner and Sariputta came to teach him the Dham
mani, wasn't able to listen.
 Sariputta asked him, "Well, did you kill all these beings
 on your own or were you told
 to do it?"
 Then he was the executioner, the state executioner, so he
 said, "Well, I didn't do it on my own
 accord.
 I did it because I was told to do it."
 Sariputta said, "Well, then what blame do you have?"
 Because he actually tricked him, I mean this is a story in
 the Dhammapada whether it's actually
 true or it actually happened or not, but supposing it did
 happen, it was a means of not lying
 to him but asking him this question to sort of comfort him
 even erroneously to the point
 because he couldn't even listen to what Sariputta was
 saying.
 He was too concerned.
 Which happens, good people come to teach you and all you
 can do is think about what an
 awful person you are.
 When you think about the bad things you've done and feel
 guilty when people start talking
 on good things.
 He placated him and as a result he was able to listen and
 able to cultivate wholesome mind
 states while listening to Sariputta's teaching.
 So yes, I think just because you're right doesn't mean it's
 right to force your opinions
 on others.
 And just because what you say is true doesn't mean it's
 going to be of any benefit.
 If it's true but not beneficial, then why would you say it?
 What good is it?
 It's the question.
 On the other hand, you don't want to say things that are
 not true but just because it's true
 doesn't mean you, if it's true and then you end up
 upsetting people through it and making
 them angry and making them hate you.
 In what way was that any good?
 Well yes, definitely.
 It's not enough to be right.
 It's enough to be right but if you want to explain to other
 people or convince other
 people you're right, well there's more to it than just
 being right.
 Okay, now lots of comments.
 I'm just going to scroll down to any questions.
 Don't see any.
 No, here's some new ones.
 If you gain insight and maturity as you meditate about life
, then when someone maybe swears
 you in anger and instead of responding with anger, you have
 compassion for them to just
 lead you to never being angry or expressing anger as it
 would either not arise or vanish
 or be expressed.
 Well, I'd encourage you, if you haven't already, to read my
 booklet on how to meditate and
 come and meditate with us and then you'll see what happens.
 You'll come to understand anger in a new light.
 You'll see it more clearly and you'll see what happens when
 you meditate.
 As you meditate, you don't meditate about life.
 You meditate about anger, for example.
 You meditate about your emotions.
 You meditate about the things that make you angry and you
 come to see that they're not
 worth getting angry about.
 As a result, compassion arises naturally.
 I mean, it's not that we directly cultivate compassion but
 indirectly compassion arises
 because you can see suffering, you see the cause of
 suffering, you see the people that
 are causing themselves suffering.
 You know how awful it is to suffer because you've seen it
 for yourself.
 So yes, the anger does not arise.
 Or if it arises, you're able to see it as anger and not
 follow it.
 Sort of the answer to your question is yes.
 Do we always note stepping right, stepping left and walking
, meditation, or can we change
 it to, you know, does lifting, stepping, placing?
 You can certainly change it.
 I mean, normally in a meditation course, we go through a
 series of steps.
 Stepping right, stepping left is just the first step.
 So every day we'll give you a new exercise and over the
 course of the course, we'll go
 through several different, we'll make it more complex.
 But that's usually something we do in a meditation course.
 I give you after, after you've practiced intensively.
 Can you explain non-self?
 Does it mean we shouldn't be saying me, I, mine because
 things are important?
 No, that's not what it means.
 Non-self is a characteristic of things, everything you
 experience.
 I mean, you have to, in order to understand non-self, you
 have to switch the way you look
 at the world from a universe of things to universe of
 experiences.
 So the basis of experiences is, the basis of reality is
 experience.
 And experiences are non-self.
 Every aspect of experience, the physical aspect, the mental
 aspect are not self.
 Means, first of all, they don't have a self, they don't
 have an entity that exists from
 moment to moment.
 So when you, when you look at an object, that object, the
 thing of the object doesn't exist
 from moment to moment.
 What exists is experience, moment after moment.
 I mean, it's just a way of looking at reality.
 Those experiences are non-self.
 They're also not controllable, is the other thing, is that
 you, you can't, you can't force
 things to be the way you want to stay, to go, that kind of
 thing.
 There's no, there's no master or controller, lord over
 these things.
 So it's, I mean, it's all about, non-self is all about
 letting go of the idea of self,
 letting go of the idea that things exist as things, objects
, the things in our lives actually
 exist, including our, our entity, the being.
 And also letting go of control, stopping to try to control
 things.
 And the point being that that's a large part of what causes
 us suffering, our, our trying
 to control and possess things.
 Not being able to see that what's real about the experience
, about the object is the experience
 that arises and ceases and is very much based on causes and
 conditions.
 Would it be fair to say beneficial results are a
 consequence of some past good done,
 getting the benefit of being in the company of wise beings,
 for example?
 I mean, it's much more complicated than that.
 What we can say is that being in the company of wise beings
, et cetera, anything that's
 good, if it has a result, will have a good result.
 It's much more proper way of saying it, rather than trying
 to interpret what we have now.
 It's much more important to understand the results of what
 we do.
 If we do do something, good, good will come from it, if
 anything comes from it.
 And if we do something bad, well, if anything comes from
 that, it'll be bad.
 That's the point.
 It's on the basis of momentary experiences.
 So if you get angry, well, that's a moment of bad karma.
 It's going to have a bad result.
 It's going to affect you negatively, affect your life
 negatively in a small way, and moment
 after moment, of course, it builds up.
 Is karma only dependent on intention or can mindlessness or
 ignorance, et cetera, affect
 it too?
 Yeah, it's not about exactly intention.
 It's more the mind state.
 Jaitana doesn't really mean intention so much as it means
 the state of mind when you respond
 to something.
 And it's momentary.
 So you can't think in the large terms, like someone talks
 to you, you get angry.
 Well, what's really going on there is a moment after moment
.
 And each moment there's a decision being made.
 Each experience of hearing, of seeing, of smelling, of
 taste, and feeling, of thinking
 has a reaction associated with it.
 And that reaction is karma, momentary.
 Karma is every, every moment.
 The movement well and your sleep patterns have returned to
 a more peaceful match with
 your surroundings.
 No, it's interesting that this time is still the only time
 that I actually feel drowsy.
 I think it's very hot up here as well.
 But yeah, the move is still ongoing.
 I won't be moved out of here until Tuesday, Wednesday.
 Wednesday of course is the start of the rains.
 So Tuesday will be the full moon, I think.
 And Wednesday is the rains, if I'm not mistaken.
 So Wednesday is the last day I can officially move into the
 new location to start a three-month
 rains retreat.
 Is it okay to feel pity for someone or express it?
 Yeah, I mean, it's just a word.
 The question is, are you sad?
 Are you upset?
 Or do you just incline to help them?
 If you really get upset about it, then it's a problem.
 But if you're just inclined to help, well that's fine.
 It's great to help people.
 It's a part of your own practice.
 Helping others, you help yourself.
 Helping yourself to help others.
 Understanding you can't control things, how far should one
 take this?
 To do nothing and settle for a miserable situation or to
 work towards a better life, and you
 need to do things to make you feel good.
 No, much more the former.
 If you're just trying to feel good, you're going to become
 addicted to feeling good.
 That's how addiction works.
 So striving to do things that make you feel good is a
 recipe for disaster, for failure.
 Once you learn to let go and not try to control things, you
 feel much more at peace, much
 more natural, much more free from suffering.
 So do you always have a positive outlook on life?
 You should have a wise outlook on life.
 If you always have a positive outlook, it's just as
 ignorant as if you always have a negative
 outlook.
 It's not realistic.
 Realistic is probably the best way.
 If you have a positive outlook, then you're always aiming
 for good things to happen.
 When they don't, you're unable to process them and tend to
 ignore them.
 It can make you happy for a while, but it doesn't help you
 understand reality.
 It doesn't help you let go.
 It just leads to more clinging.
 It's supposed to some extent.
 Some extent it isn't good to see the positive and not worry
 so much about the negative, like
 in other people.
 Pollyanna is a good story.
 She does all sorts of good things and just doesn't see the
 evil.
 It discards the evil in people and just only looks at the
 good in people, which I think
 is fairly kosher from a Buddhist point of view.
 That's a good thing in principle.
 It's important not to miss, I think perhaps the problem
 with Pollyanna is she couldn't
 see her, didn't really understand the evil in people.
 To some extent we have to put it aside.
 If someone does a bad thing, they're not obsessed about it.
 If you focus on good in that sense, then the people around
 you are inclined to do good
 because they see that that's what you're focused on.
 Focus on good and sort of brushing aside the evil allows
 people the opportunity to change
 to become better people.
 I think to some extent that's important.
 To some extent, you can't ignore evil entirely.
 In that sense, focusing on good can be beneficial.
 People don't feel guilty and awful about the bad things
 they've done and they're able to
 move on and become better.
 Focus on the good.
 Lots of questions from just a few people.
 Oh no, quite a few people.
 Mandy is back, Mandy, Mandy is an old meditator.
 Lots of old timers coming back.
 Good to see.
 In the Hangout we have 37 viewers, which is down from
 before.
 Sunday evening.
 Thanks for tuning in.
 Okay, we have another question.
 "Titta, you have lots of questions, which is somewhat
 troublesome because the more you
 meditate the fewer questions you should have.
 So I'm hoping that this is a temporary thing and you're not
 becoming addicted to asking
 questions."
 They're good questions actually, the ones you've been
 asking.
 I'm happy about them.
 "How should you approach things that people may say to you?
 Should you approach it being a bit skeptical or?"
 Yeah, I mean I think that's proper.
 It's proper to examine.
 I think rather than skeptical, the problem with skeptical
 is it often has the implication
 of doubting.
 You don't have to doubt things people say.
 You have to be aware of simply believing people.
 You have to examine things with a neutral mind.
 I think that's a very proper outlook.
 Don't just believe anything anyone tells you, but it doesn
't mean you have to immediately
 doubt.
 You know, doubt would be say 50/50.
 You give someone 50/50 that's neutral.
 I think that's where we should start.
 If you doubt, it's less than 50.
 There should be a reason for that.
 It shouldn't be your default.
 You shouldn't start at zero and say, "You have to prove it
 to me."
 Someone says something and you say, "Oh yeah, prove it."
 No, you should say, "Okay, 50/50," and then you examine it
 and you think, "Well, that's
 unlikely for this reason."
 Then you start to doubt it.
 Or you say, "Oh, that's likely.
 There's reason to believe that.
 Give it greater than 50."
 I think that's a good outlook.
 It isn't foolproof.
 You can still doubt things that turn out to be true, but it
's important to start with
 a neutral, unbiased approach.
 Not blindly believing anything, but also not simply doub
ting it for the sake of doubting
 everything.
 Just take everything at face value and then examine it.
 A person says, "Do aliens land it in the backyard?"
 You shouldn't immediately dismiss it.
 You should be able to think of a case where that might be
 true and then start to examine
 how unlikely it is and then say, "It's unlikely that that
 really happened, but we can investigate."
 One is able, should one go on Buddhist pilgrimage to at
 least Lumbanim, B
 Gaya, Sarnath, and Kṛṣṇāra.
 Yeah, sure.
 It's not necessary in your Buddhist practice, but it's a
 great thing.
 I've been three times.
 I can't say I wouldn't go again if I had the chance.
 It would be great.
 You should put together a pilgrimage of our online
 community.
 I'm not going to go on the same page.
 I'm going to go on the same page.
 I'm going to go on the same page.
 It would be great.
 I'm going to go on the same page.
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "I'm not going to go on the same page." ]
 [
 audience member
 "When you went on Buddhist pilgrimage, did you find it very
 spiritual and uplifting? How
 would you describe it as far as your feelings at the time?"
 ]
 [
 audience member
 "It's great. I mean, it really makes things real. It's
 great when you've studied the Buddha's
 teaching and you've read all these stories and just read
 about the Buddha's life and
 then you actually get to go and see it. It really makes it
 real, especially unless you
 live in India or Sri Lanka. Even if you live in a Buddhist
 country that's unlike India,
 like Thailand, living in Thailand as a Buddhist and
 learning all these things, it's just not
 Thai. It's not based on the Thai culture. Thai culture is
 very dissimilar to Indian culture.
 So there's something missing, sort of the location. It's
 not that the teachings are
 any different. It's just that the flavor, the environment,
 the surroundings, what's
 going on, it only really makes sense when you go to India
 and see where the Buddha lived,
 sort of following the Buddha's footsteps. That's one
 important thing that came to me.
 It actually came more when I left the Buddhist holy sites
 because the holy sites are just
 big tourist traps. They're very commercialized. They've
 gotten better, more organized anyway,
 but they're not really India as the Buddha would have seen
 it. But when you go off into
 the countryside somewhere that's not Buddhist, talk to the
 locals and make an effort to really
 get into the culture, then you really feel it. It feels
 much more Buddhist. But the other
 thing about the holy sites is just the awe that you get
 from thinking the Buddha sat
 here, the Buddha walked here, the Buddha was born here,
 this kind of thing. That's a great
 thing, especially if you're a devout Buddhist. For me it
 was always great to go to think,
 "Wow, this was really it. This was where it happened." It's
 kind of like fans who go
 to Graceland and that kind of thing, Elvis fans. Is it
 guaranteed that meditation will
 change you in some way? Or can another factor block that
 probability out? Well, everything
 changes you. But I think what you mean is in the way
 expected, in the way that we're
 hoping to change. Is meditation necessarily going to
 enlighten you kind of thing? Yes,
 there are factors that can block you, definitely. I think
 it's rare for there to be factors
 to block you. It's much more common for people to think and
 to worry that they're being blocked
 in their practice and they're just not able to see the
 benefits. That's very common. It's
 very hard to see the benefits in the beginning, especially
 if you're not practicing intensively.
 If you're doing intensive course, much easier to see the
 benefits and the changes. But if
 you're doing it non-intensively, like a daily practice,
 very slow and easy to forget any
 benefits. You forget from day to day. You minimalize the
 benefits that you get one day
 of it, "Wow, this really is changing." Then the next day
 you'll be like, "This meditation
 is doing nothing for me." Totally having minimalized or
 even forgotten the benefit that you got
 the day before. We're like that. We do this. We have
 selective memory with poor memory.
 Our memory is tainted by our judgments, that kind of thing.
 It's much more common for people
 to think it's not changing them, to not be able to see the
 changes. The other thing is
 if it's you changing, how can you really compare? You're
 always like this. It's just that this
 maybe changes. It's like you can't see when your face is
 dirty. It's the analogy. But
 other people see it in you. A good way to judge is how
 other people treat you, how your
 interactions with others are. If you find your
 relationships improving, your ability
 to deal with other people improving, the way people look at
 you. If that's changing, I
 mean actually in the beginning it can be somewhat dis
concerting for people who are close to
 you and it's quite common for people to react negatively to
 meditators who are actually
 benefiting from meditation. They're becoming less engaged,
 less attached, and as a result
 everyone else gets upset because they're still very much
 attached. But there will be that
 sort of change in your interactions with other people in
 the environment and that kind of
 thing. Your work and yourself more efficient, more patient,
 that kind of thing. But yeah,
 there are things that can block you. If you have a perverse
 view, if you have a lot of
 bad karma, it happens. But it's usually temporary and it
 usually just makes it more difficult.
 But for most of us it's not like most of meditation is just
 difficult, period. And that doesn't
 mean you've got some problem and there's something blocking
 you. It just means meditation is
 difficult. It's meant to be. It's meant to challenge you.
 It wouldn't be enlightening
 if it didn't challenge you. When I see things clearly, I
 see patterns in things. Is this
 too much analysis? No, seeing them is not. Analyzing them
 is. If you're looking for patterns
 or if you're thinking about them and investigating them,
 that's a problem. If you see a pattern,
 well that's an experience. You say thinking, thinking, or
 even if they're seeing, knowing,
 knowing, that kind of thing. The patterns you will see
 patterns. Just don't go looking
 for them or trying to find them. That's not really the goal
. It will be a part of the
 goal to see them. That seeing will come by itself. If you
're looking for them, it's not
 really what we're looking for. It's going too far. Should
 Buddhists have compassion for
 racial racist people or just ignore them? Yeah, to some
 extent compassion. Compassion
 is great to help you see the truth of the situation, that
 they're the ones that are
 going to suffer from being racist. Equanimity is also
 useful to not get upset. Equanimity
 allows you to see that when this is the way it goes, people
 do bad things, they get bad
 results. Something that we have to worry about or try to
 fix. It's just the nature of reality.
 Do bad things, get bad results, suffer. Do more bad things,
 get more bad results, suffer
 again and again and again. Why get upset about it? That's
 equanimity. Firm belief in permanence
 soul or nihilism could block the best in the progress. Yeah
, perverse wrong view. It has
 to be somewhat pernicious. There's lots of views that can
 get in your way. But not just
 views. I mean someone's a very angry person that can get in
 the way. Someone's done a
 lot of evil deeds that can really prevent them. But views
 is probably the big one. Any
 of the five hindrances will go. Any of the five hindrances
 will get in your way. And
 if you're not able to be mindful, you're not able to deal
 with them, to come to terms
 with them. They'll prevent you. I had an encounter when I
 was younger in a haunted historic battlefield
 with a ghost. It was very much like blending in with nature
. After the formation ceased,
 there was this, a hungry, a hungry ghost. Someone's on a
 phone, I think. Is this a hungry
 ghost? A ghost from Hungary? Someone's using auto correct.
 They vanished into nature. Sounds
 like it. Whatever it is, it's impermanent suffering and non
-self. It's not worth clinging
 to in its past. I didn't write too much about it. It was
 interesting to see things. It was
 interesting to make, to meet with things that are, and open
 your mind and allow you to see
 that there's much more to reality than what meets the eyes.
 I recently started to become
 more drowsy and drop my head over and over while meditating
. No matter how many times
 I repeat drowsy or sleeping. I do walk in meditation
 beforehand. In the past, this usually
 wouldn't happen. You don't stop it. Try to be mindful of it
. There are ways to deal with
 it. Get up and walk when it happens. Splash water on your
 face. Do some chanting. It'll
 help, but it's a temporary condition that comes when you
 begin to meditate. You say
 it's recently cropped up, but it's a sign that there's some
 change going on and there's
 some conflict in terms of your state of mind and the
 meditation practice. In intensive
 practice it would work itself out in a day or two, but in
 daily life it may be something
 you just have to deal with depending on your work situation
. Now, it probably would get
 better if you were to do a regular practice, say, with a
 teacher and to go through one of
 our online courses. I've had people who, through the online
 course, have seen it change and
 get better just by doing daily practice, but usually that's
 temporary. As you can see,
 it's something that didn't happen before. Well, that means
 it's not going to last forever.
 You just be mindful of it when it is. It's the challenge.
 It's part of the challenge.
 It's part of the challenge that is meditation. Try to stop
 it if you didn't have any of these
 challenges. What would you learn? What good would it be to
 you? Just be comfortable. You
 don't get much from comfortable. Not usually. It doesn't
 pull you out of your comfort zone.
 If it doesn't force you to re-examine the way you look at
 things and to change and to
 adapt and to build new and better habits, then how can it
 be enlightening? Just take
 it as a challenge. Enough questions. That's too many. You
 can't answer so many questions
 in one night. There's got to be a rule against that or
 something. Enough questions for one
 night. No, good questions. Thank you. I would encourage you
 all to meditate and see if some
 of the answers come through meditation. It's not quite fair
. Good questions. Thank you
 for good questions, but enough for tonight. Save some for
 tomorrow. Thank you all for
 tuning in. Have a good night, everyone.
 [
