1
00:00:00,000 --> 00:00:05,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:05,000 --> 00:00:11,890
 Today we continue with verses 85 and 86 which read as

3
00:00:11,890 --> 00:00:13,000
 follows.

4
00:00:13,000 --> 00:00:15,500
 Apakati Manu Seisu, yejana par gaminau, ata yang itarap j

5
00:00:15,500 --> 00:00:33,050
ati ramivanudhavati, yejakosamadakati dami dhammanu atino,

6
00:00:33,050 --> 00:00:39,000
 tejana par mei santi machudhe yang sudutaram.

7
00:00:39,000 --> 00:00:45,940
 These verses are a common chant in Thailand, so quite

8
00:00:45,940 --> 00:00:47,000
 familiar.

9
00:00:47,000 --> 00:00:50,000
 These are the next ones in the chapter.

10
00:00:50,000 --> 00:00:59,840
 So these two mean, apakati manu seisu, few are they among

11
00:00:59,840 --> 00:01:05,000
 humans, yejana par gaminau, who go to the farther shore.

12
00:01:05,000 --> 00:01:15,010
 Ata yang itarap jati, these other, here these other humans,

13
00:01:15,010 --> 00:01:16,000
 other people,

14
00:01:16,000 --> 00:01:18,030
 dheerame wa anudhavati, simply run up and down the shore,

15
00:01:18,030 --> 00:01:37,000
 yejakosamadakati, but those who, samadakati dami,

16
00:01:37,000 --> 00:01:46,220
 in regards to the well-taught dama, damanu atino, are those

17
00:01:46,220 --> 00:01:51,000
 who go according to the dama.

18
00:01:51,000 --> 00:02:10,000
 Tejana par mei santi, these people go to the farther shore,

19
00:02:10,000 --> 00:02:13,000
 machudhe yang sudutaram, these cross to the other side,

20
00:02:13,000 --> 00:02:18,530
 machudhe yang to the king of the kingdom of death, which is

21
00:02:18,530 --> 00:02:23,000
 hard to cross, which is very hard to cross.

22
00:02:23,000 --> 00:02:28,390
 So we have some imagery here, the imagery of crossing an

23
00:02:28,390 --> 00:02:33,000
 ocean, and now most people just run up and down the shore.

24
00:02:33,000 --> 00:02:43,000
 Dheeram is this shore, anudhavati is running up and down.

25
00:02:43,000 --> 00:02:47,970
 So this was given in regards to another very short story,

26
00:02:47,970 --> 00:02:51,450
 it's rather more of a remark that the Buddha gave in

27
00:02:51,450 --> 00:02:57,520
 regards to the habits of those who would come and listen to

28
00:02:57,520 --> 00:02:58,000
 the dama.

29
00:02:58,000 --> 00:03:02,850
 It seems that there was a specific group of people who came

30
00:03:02,850 --> 00:03:06,000
 to listen to the Buddha's teaching.

31
00:03:06,000 --> 00:03:10,080
 They had done good deeds, or they had supported the

32
00:03:10,080 --> 00:03:14,000
 monastery, and so they came and gave alms to the monks,

33
00:03:14,000 --> 00:03:18,230
 and then stuck around, intending to listen to the dama all

34
00:03:18,230 --> 00:03:19,000
 night.

35
00:03:19,000 --> 00:03:22,040
 There would be teachings of the dama where the monks would

36
00:03:22,040 --> 00:03:25,440
 take turns giving talks all night, or maybe even one monk

37
00:03:25,440 --> 00:03:28,000
 would talk all night, probably the former.

38
00:03:28,000 --> 00:03:31,000
 Probably it would be several monks taking turns.

39
00:03:31,000 --> 00:03:38,000
 We once did this in Thailand, something like this.

40
00:03:38,000 --> 00:03:44,000
 There were some ancient Buddha statues that were stolen.

41
00:03:44,000 --> 00:03:48,200
 They were actually kept behind bars, they were quite

42
00:03:48,200 --> 00:03:49,000
 valuable.

43
00:03:49,000 --> 00:03:52,470
 Someone actually cut through the bars, or bent the bars or

44
00:03:52,470 --> 00:03:55,000
 something to steal these Buddha images.

45
00:03:55,000 --> 00:03:59,000
 Cut through the bars, I think, to steal the Buddha images.

46
00:03:59,000 --> 00:04:06,620
 Somewhat miraculously, one of them was found at the bottom

47
00:04:06,620 --> 00:04:13,530
 of one of the rivers, I guess, like a small river, when it

48
00:04:13,530 --> 00:04:16,000
 dried up or something in the dry season.

49
00:04:16,000 --> 00:04:18,000
 It was found.

50
00:04:18,000 --> 00:04:23,570
 They pulled it up and brought it back and had an all night

51
00:04:23,570 --> 00:04:25,000
 ceremony.

52
00:04:25,000 --> 00:04:30,000
 It was something like this. The details are imprecise.

53
00:04:30,000 --> 00:04:33,080
 I maybe never even paid too much attention to the details,

54
00:04:33,080 --> 00:04:35,000
 but it was something like that.

55
00:04:35,000 --> 00:04:39,600
 We had an all night ceremony to honor the Buddha, or maybe

56
00:04:39,600 --> 00:04:44,000
 to reestablish its sanctity or something, its holiness.

57
00:04:44,000 --> 00:04:46,820
 It was a very cultural sort of thing that I didn't pay too

58
00:04:46,820 --> 00:04:48,000
 much attention to.

59
00:04:48,000 --> 00:04:50,400
 It may have been broken or something, and there was a

60
00:04:50,400 --> 00:04:52,000
 fixing that had to go on.

61
00:04:52,000 --> 00:04:55,750
 At the end of fixing it, you have to re-bless it or

62
00:04:55,750 --> 00:04:57,000
 something.

63
00:04:57,000 --> 00:04:59,550
 But it was kind of considered a miracle that they got it

64
00:04:59,550 --> 00:05:00,000
 back.

65
00:05:00,000 --> 00:05:02,000
 It was a big deal.

66
00:05:02,000 --> 00:05:06,280
 All of the monks, I was a part of it, we would take turns

67
00:05:06,280 --> 00:05:10,480
 chanting or giving talks or that kind of thing all night

68
00:05:10,480 --> 00:05:12,000
 until the sun rose.

69
00:05:12,000 --> 00:05:17,000
 So the spirit of this still goes on, even today.

70
00:05:17,000 --> 00:05:21,000
 This idea of an all night dhamma thing.

71
00:05:21,000 --> 00:05:24,190
 I think it was Ajahn Tong that put it together. I don't

72
00:05:24,190 --> 00:05:25,000
 know how often it happens.

73
00:05:25,000 --> 00:05:28,220
 I know in Burma they do all night chantings and all night

74
00:05:28,220 --> 00:05:32,000
 teachings, from what I hear.

75
00:05:32,000 --> 00:05:36,360
 Anyway, the situation here was all of them were trying to

76
00:05:36,360 --> 00:05:41,000
 listen to the dhamma, but none of them lasted.

77
00:05:41,000 --> 00:05:46,170
 Or maybe not none of them, but many of them were unable to

78
00:05:46,170 --> 00:05:47,000
 last.

79
00:05:47,000 --> 00:05:50,360
 None of them, they were unable to listen to the dhamma all

80
00:05:50,360 --> 00:05:51,000
 night.

81
00:05:51,000 --> 00:05:57,070
 Some of them were overcome by lust, sexual passion, and

82
00:05:57,070 --> 00:06:01,000
 went home.

83
00:06:01,000 --> 00:06:09,000
 Some of them were overcome by anger, boredom maybe.

84
00:06:09,000 --> 00:06:13,000
 Anger is of very different kind, many different kinds.

85
00:06:13,000 --> 00:06:16,880
 It can be boredom, it can be dislike of the dhamma

86
00:06:16,880 --> 00:06:18,000
 teachings,

87
00:06:18,000 --> 00:06:24,000
 it can be self-hatred at the idea that every time you hear

88
00:06:24,000 --> 00:06:28,000
 something that hits too close to home,

89
00:06:28,000 --> 00:06:32,110
 maybe they're talking about things that are unwholesome and

90
00:06:32,110 --> 00:06:34,000
 you realize that you engage in some of those

91
00:06:34,000 --> 00:06:37,000
 and so you get angry at yourself and feel guilty and all

92
00:06:37,000 --> 00:06:38,000
 these things.

93
00:06:38,000 --> 00:06:41,140
 So being overcome with that, they were unable to stay and

94
00:06:41,140 --> 00:06:42,000
 they went home.

95
00:06:42,000 --> 00:06:53,550
 Some of them were overcome by mana, conceit, so that would

96
00:06:53,550 --> 00:06:58,000
 be the getting, feeling self-righteous

97
00:06:58,000 --> 00:07:02,000
 and displeased by the dhamma, but not an angry thing,

98
00:07:02,000 --> 00:07:05,900
 just sort of feeling like they knew better than the monks

99
00:07:05,900 --> 00:07:09,000
 or these monks don't know what they're talking about,

100
00:07:09,000 --> 00:07:12,000
 being attached to views and that kind of thing.

101
00:07:12,000 --> 00:07:15,770
 Some of them got caught up with tinamida, so they got tired

102
00:07:15,770 --> 00:07:18,000
, that's a common one,

103
00:07:18,000 --> 00:07:21,630
 and were nodding off and said, "I realize that they had to

104
00:07:21,630 --> 00:07:23,000
 go home is better."

105
00:07:23,000 --> 00:07:34,460
 So they went home in the middle of the teaching and so they

106
00:07:34,460 --> 00:07:35,000
 went off.

107
00:07:35,000 --> 00:07:48,000
 The next day, the monks gathered and commented on this

108
00:07:48,000 --> 00:07:53,350
 and just were sort of remarking on how difficult it is for

109
00:07:53,350 --> 00:07:56,000
 people to listen to the dhamma

110
00:07:56,000 --> 00:08:01,070
 and how people aren't all that keen to take the opportunity

111
00:08:01,070 --> 00:08:04,000
 to listen to the dhamma.

112
00:08:04,000 --> 00:08:08,000
 The Buddha said monks for the most of the Buddha came in

113
00:08:08,000 --> 00:08:09,000
 and asked them what they were talking about

114
00:08:09,000 --> 00:08:15,160
 and when they told him, he remarked that this is common for

115
00:08:15,160 --> 00:08:17,000
 people.

116
00:08:17,000 --> 00:08:23,300
 He said, "For the most part, ye bu yena bhavani sita, for

117
00:08:23,300 --> 00:08:27,000
 the most part they are dependent upon

118
00:08:27,000 --> 00:08:33,000
 or they are attached to existence, bhava, becoming.

119
00:08:33,000 --> 00:08:43,980
 Bhavaisu eva langa viharanthi, they dwell stuck in Baba,

120
00:08:43,980 --> 00:08:47,000
 stuck in becoming."

121
00:08:47,000 --> 00:08:50,420
 The meaning here, Baba is a sort of a neutral term, that

122
00:08:50,420 --> 00:08:52,000
 sort of blanket statement

123
00:08:52,000 --> 00:08:56,000
 for anything that leads to further becoming.

124
00:08:56,000 --> 00:09:00,320
 So when you want something, on a very basic level, it leads

125
00:09:00,320 --> 00:09:01,000
 to becoming here

126
00:09:01,000 --> 00:09:06,970
 and now you give rise to something new, which is the plan

127
00:09:06,970 --> 00:09:09,000
 to get what you want.

128
00:09:09,000 --> 00:09:14,180
 You want to eat and so suddenly the plan to make food

129
00:09:14,180 --> 00:09:15,000
 arises.

130
00:09:15,000 --> 00:09:20,000
 Maybe you want to watch a movie, so you plan to go out and

131
00:09:20,000 --> 00:09:23,000
 to the, I guess people don't go to the video store anymore.

132
00:09:23,000 --> 00:09:32,320
 Whatever you want, you want to go to Thailand and so you

133
00:09:32,320 --> 00:09:35,000
 give rise to something, this.

134
00:09:35,000 --> 00:09:41,170
 But you give rise to further becoming something that's not

135
00:09:41,170 --> 00:09:43,000
 just functional

136
00:09:43,000 --> 00:09:49,600
 but it's actually a desire-based, a whole new set of

137
00:09:49,600 --> 00:09:51,000
 variables.

138
00:09:51,000 --> 00:09:55,150
 Or if you're angry, you give rise to something new as well,

139
00:09:55,150 --> 00:09:57,000
 at the very least a headache.

140
00:09:57,000 --> 00:10:02,290
 But you can also out of anger give rise to argument,

141
00:10:02,290 --> 00:10:06,000
 friction, conflict, war even.

142
00:10:06,000 --> 00:10:09,630
 Of course war can be caused by greed and often it's caused

143
00:10:09,630 --> 00:10:11,000
 simply by greed.

144
00:10:11,000 --> 00:10:18,150
 Concede you give rise to this competition or this sense of

145
00:10:18,150 --> 00:10:24,000
 this conflict based on power struggle

146
00:10:24,000 --> 00:10:29,000
 and desire for dominance, oppression, this kind of thing.

147
00:10:29,000 --> 00:10:32,000
 Oppressing others, belittling others, that kind of thing.

148
00:10:32,000 --> 00:10:38,550
 So you create something new or you'd simply create a new

149
00:10:38,550 --> 00:10:40,000
 plan for yourself.

150
00:10:40,000 --> 00:10:45,120
 I deserve to be king so I'm going to fight my way to become

151
00:10:45,120 --> 00:10:46,000
 king.

152
00:10:46,000 --> 00:10:52,000
 I deserve this or that or I don't deserve this or that.

153
00:10:52,000 --> 00:10:56,000
 And so you strive accordingly.

154
00:10:56,000 --> 00:11:03,920
 Sometimes it's just out of laziness, this gives rise to

155
00:11:03,920 --> 00:11:04,000
 sleep

156
00:11:04,000 --> 00:11:09,050
 but it also gives rise to sickness, it gives rise to

157
00:11:09,050 --> 00:11:10,000
 conflict and problems

158
00:11:10,000 --> 00:11:14,580
 when you don't act according to your duties where you just

159
00:11:14,580 --> 00:11:18,000
 consume but don't produce.

160
00:11:18,000 --> 00:11:21,400
 And as a result people become upset because you're simply a

161
00:11:21,400 --> 00:11:28,000
 consumer, a mooch, lazy, etc.

162
00:11:28,000 --> 00:11:30,000
 And so on.

163
00:11:30,000 --> 00:11:32,000
 So this is the idea of becoming.

164
00:11:32,000 --> 00:11:34,000
 It's really anything that leads to...

165
00:11:34,000 --> 00:11:38,000
 So there was a question recently about what's wrong with...

166
00:11:38,000 --> 00:11:41,820
 There's always questions about what's wrong with seeking

167
00:11:41,820 --> 00:11:44,000
 out pleasure, ordinary pleasure

168
00:11:44,000 --> 00:11:46,000
 because you do get some.

169
00:11:46,000 --> 00:11:49,000
 And it's really this...

170
00:11:49,000 --> 00:11:57,900
 These two verses point to the mindset or the outlook of the

171
00:11:57,900 --> 00:12:01,000
 Buddha and of Buddhists in general

172
00:12:01,000 --> 00:12:05,070
 is that it's not enough and it's not sustainable because

173
00:12:05,070 --> 00:12:06,000
 the more you want, as I said,

174
00:12:06,000 --> 00:12:11,260
 the more you suffer, the less you get or the less satisfied

175
00:12:11,260 --> 00:12:12,000
 you are.

176
00:12:12,000 --> 00:12:16,000
 And in fact it's the wrong way, it's unsustainable.

177
00:12:16,000 --> 00:12:19,150
 You find yourself bouncing back and forth or for a time you

178
00:12:19,150 --> 00:12:21,000
're able to balance things

179
00:12:21,000 --> 00:12:24,260
 before you fall one way or the other and then it's back and

180
00:12:24,260 --> 00:12:25,000
 forth again.

181
00:12:25,000 --> 00:12:30,010
 And it's building up disappointment, building up attachment

182
00:12:30,010 --> 00:12:32,000
 and stress

183
00:12:32,000 --> 00:12:42,040
 or it's working for a time to either repress, often it's

184
00:12:42,040 --> 00:12:45,000
 simply to repress our desires.

185
00:12:45,000 --> 00:12:48,220
 And we fluctuate back and forth and go through life or we

186
00:12:48,220 --> 00:12:51,000
 go through lives again and again and again

187
00:12:51,000 --> 00:12:58,000
 or born old sick die, born old sick die.

188
00:12:58,000 --> 00:13:04,100
 So this is sort of the outlook that the Buddha's basing

189
00:13:04,100 --> 00:13:05,000
 this on.

190
00:13:05,000 --> 00:13:08,000
 These two verses are actually...

191
00:13:08,000 --> 00:13:12,350
 One thing that stands out for me, besides the obvious

192
00:13:12,350 --> 00:13:18,000
 symbolism or imagery which is quite powerful

193
00:13:18,000 --> 00:13:22,000
 is sort of congratulatory.

194
00:13:22,000 --> 00:13:27,380
 It seemed like a good opportunity to congratulate all of

195
00:13:27,380 --> 00:13:31,000
 the people involved with this community,

196
00:13:31,000 --> 00:13:35,510
 all of the listeners, all of you who are listening to the D

197
00:13:35,510 --> 00:13:36,000
hamma, right?

198
00:13:36,000 --> 00:13:39,000
 Taking the time, some of you every day too.

199
00:13:39,000 --> 00:13:41,000
 It's not something I never thought would happen.

200
00:13:41,000 --> 00:13:44,000
 I thought, okay, once a week maybe.

201
00:13:44,000 --> 00:13:48,130
 But some people actually come out every day to listen to

202
00:13:48,130 --> 00:13:49,000
 the Dhamma.

203
00:13:49,000 --> 00:13:54,000
 You take the time to come online and sit still for an hour,

204
00:13:54,000 --> 00:13:57,000
 half an hour, an hour,

205
00:13:57,000 --> 00:14:01,470
 even to ask questions, so to get involved, to perform this

206
00:14:01,470 --> 00:14:07,000
 act of good karma of asking questions.

207
00:14:07,000 --> 00:14:12,150
 Many of you are very respectful and so being respectful and

208
00:14:12,150 --> 00:14:15,000
 all sorts of good karma going on.

209
00:14:15,000 --> 00:14:20,720
 And that's something to be proud of because even in the

210
00:14:20,720 --> 00:14:23,000
 context of the story,

211
00:14:23,000 --> 00:14:25,000
 many people...

212
00:14:25,000 --> 00:14:27,570
 I mean in the context of the story, these people are still

213
00:14:27,570 --> 00:14:31,000
 to be graduated because they tried.

214
00:14:31,000 --> 00:14:34,000
 And it's not like they didn't hear some of the Dhamma.

215
00:14:34,000 --> 00:14:36,270
 It's just they tried to do something beyond their

216
00:14:36,270 --> 00:14:37,000
 capabilities.

217
00:14:37,000 --> 00:14:43,000
 But all of the people here should be congratulated as well

218
00:14:43,000 --> 00:14:46,000
 for having the good intentions.

219
00:14:46,000 --> 00:14:53,000
 Many people don't even think about crossing.

220
00:14:53,000 --> 00:14:56,080
 Or if they think about crossing, they never do anything to

221
00:14:56,080 --> 00:14:57,000
 try to cross.

222
00:14:57,000 --> 00:14:59,000
 They would never come and meditate.

223
00:14:59,000 --> 00:15:01,000
 If you look at the list of meditators that we have,

224
00:15:01,000 --> 00:15:06,000
 you can see how many we have tonight.

225
00:15:06,000 --> 00:15:10,000
 We've got actually a fairly small list today.

226
00:15:10,000 --> 00:15:13,680
 Uh-oh, and a bunch of orange people. Maybe I spoke too soon

227
00:15:13,680 --> 00:15:14,000
.

228
00:15:14,000 --> 00:15:17,000
 What happened today?

229
00:15:17,000 --> 00:15:21,110
 Well, still. We have many, many people and we have people

230
00:15:21,110 --> 00:15:26,000
 meditating on this side around the clock.

231
00:15:26,000 --> 00:15:33,000
 People coming to listen to the Dhamma every day.

232
00:15:33,000 --> 00:15:36,000
 So this is a sign of at least wanting to cross.

233
00:15:36,000 --> 00:15:38,800
 Maybe some people just listen to the talks or watch my

234
00:15:38,800 --> 00:15:40,000
 YouTube videos

235
00:15:40,000 --> 00:15:45,000
 and maybe dip their foot in the water but don't ever cross.

236
00:15:45,000 --> 00:15:48,000
 But at the very least, there's some thought.

237
00:15:48,000 --> 00:15:50,000
 And this is the first step.

238
00:15:50,000 --> 00:15:53,000
 The first step is thinking about it.

239
00:15:53,000 --> 00:16:00,000
 Having the, um, giving rise to the intention or the desire

240
00:16:00,000 --> 00:16:03,000
 to better oneself

241
00:16:03,000 --> 00:16:05,000
 as opposed to just running up and down the shore.

242
00:16:05,000 --> 00:16:08,000
 Because that's what, that's what it's like.

243
00:16:08,000 --> 00:16:11,000
 That's what the Buddha likens most of our activity to.

244
00:16:11,000 --> 00:16:14,000
 Just running up and down the shore.

245
00:16:14,000 --> 00:16:18,000
 Sometimes running up and down out of, out of desire.

246
00:16:18,000 --> 00:16:23,900
 Sometimes running up and down out of anger, fear, worry,

247
00:16:23,900 --> 00:16:25,000
 conceit.

248
00:16:25,000 --> 00:16:35,000
 We have many, many ways of running around in circles.

249
00:16:35,000 --> 00:16:37,000
 Running around in circles in the kingdom of death.

250
00:16:37,000 --> 00:16:40,520
 And this is where we find ourselves being born again and

251
00:16:40,520 --> 00:16:45,000
 again, dying again and again.

252
00:16:45,000 --> 00:16:55,000
 Learning and forgetting.

253
00:16:55,000 --> 00:16:57,000
 Not really learning from our mistakes.

254
00:16:57,000 --> 00:17:01,360
 Our intention here, our practice here is to break that, to

255
00:17:01,360 --> 00:17:03,000
 cross over,

256
00:17:03,000 --> 00:17:07,000
 to go beyond, to go beyond death.

257
00:17:07,000 --> 00:17:11,000
 To figure the system out, to come to understand the system.

258
00:17:11,000 --> 00:17:15,000
 And to understand becoming.

259
00:17:15,000 --> 00:17:17,000
 And as a result, not be dependent upon it.

260
00:17:17,000 --> 00:17:20,390
 One way of explaining how, why we're dependent upon it is

261
00:17:20,390 --> 00:17:22,000
 because we don't understand it.

262
00:17:22,000 --> 00:17:26,000
 We're dependent upon it because it runs us.

263
00:17:26,000 --> 00:17:27,000
 We don't run it.

264
00:17:27,000 --> 00:17:30,000
 We don't lord over death.

265
00:17:30,000 --> 00:17:32,000
 Death lords over us.

266
00:17:32,000 --> 00:17:35,000
 Becoming as well.

267
00:17:35,000 --> 00:17:44,210
 We act in such a way usually that we don't fully understand

268
00:17:44,210 --> 00:17:46,000
 the system.

269
00:17:46,000 --> 00:17:50,980
 We don't fully grasp what we're dealing with or what's in

270
00:17:50,980 --> 00:17:57,000
 store for us when we chase after things.

271
00:17:57,000 --> 00:18:01,600
 We're not clear in our minds as to the nature of our add

272
00:18:01,600 --> 00:18:06,000
ictions and our aversions and our conceits and our views.

273
00:18:06,000 --> 00:18:08,060
 And so we hold on to them often without a clear

274
00:18:08,060 --> 00:18:09,000
 understanding.

275
00:18:09,000 --> 00:18:11,000
 Or usually without a clear understanding.

276
00:18:11,000 --> 00:18:15,000
 Once we understand them clearly, this is the crossing over.

277
00:18:15,000 --> 00:18:24,000
 This is the rising above.

278
00:18:24,000 --> 00:18:27,000
 So it's rather a poetic verse.

279
00:18:27,000 --> 00:18:28,000
 Simple story.

280
00:18:28,000 --> 00:18:29,000
 Simple verse really.

281
00:18:29,000 --> 00:18:31,000
 Simple meaning.

282
00:18:31,000 --> 00:18:36,000
 It's kind of hard teaching.

283
00:18:36,000 --> 00:18:41,980
 As it lays bare the fact that most of what we do is just

284
00:18:41,980 --> 00:18:44,000
 running up and down the shore.

285
00:18:44,000 --> 00:18:48,000
 It doesn't really accomplish anything in the end.

286
00:18:48,000 --> 00:18:50,740
 But we should, on the other hand, it should be an

287
00:18:50,740 --> 00:18:53,000
 encouraging teaching because whatever the good things that

288
00:18:53,000 --> 00:18:53,000
 we do,

289
00:18:53,000 --> 00:18:58,310
 this is our coming closer to be one of those few people who

290
00:18:58,310 --> 00:19:01,000
 is actually able to cross over.

291
00:19:01,000 --> 00:19:05,230
 Actually able to rise above and free themselves from the

292
00:19:05,230 --> 00:19:09,000
 wheel of suffering, from the kingdom of death.

293
00:19:09,000 --> 00:19:14,370
 So good work everyone and thank you all for tuning in and

294
00:19:14,370 --> 00:19:18,000
 for supporting these teachings with your practice.

295
00:19:18,000 --> 00:19:22,000
 For practicing and continuing on this practice.

296
00:19:22,000 --> 00:19:26,000
 So that's the Dhammapada teaching for tonight.

297
00:19:26,000 --> 00:19:28,000
 Thank you all for tuning in.

