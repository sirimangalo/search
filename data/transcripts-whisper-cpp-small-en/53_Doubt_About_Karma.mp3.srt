1
00:00:00,000 --> 00:00:02,600
 Doubt, question and answer.

2
00:00:02,600 --> 00:00:04,720
 Doubt about karma.

3
00:00:04,720 --> 00:00:06,800
 If mind is just a concept,

4
00:00:06,800 --> 00:00:09,200
 what is it that gets reborn or reincarnated

5
00:00:09,200 --> 00:00:10,880
 that carries our karma?

6
00:00:10,880 --> 00:00:12,220
 I'm a little confused.

7
00:00:12,220 --> 00:00:15,200
 Nothing gets reborn.

8
00:00:15,200 --> 00:00:16,920
 Nothing gets reincarnated.

9
00:00:16,920 --> 00:00:18,800
 Nothing carries karma.

10
00:00:18,800 --> 00:00:21,000
 Karma itself does not exist.

11
00:00:21,000 --> 00:00:22,680
 Confusion exists.

12
00:00:22,680 --> 00:00:24,520
 Confusion arises in the mind,

13
00:00:24,520 --> 00:00:25,960
 and this is much more important

14
00:00:25,960 --> 00:00:28,360
 than getting an answer to the question.

15
00:00:28,360 --> 00:00:31,280
 You do not really need answers to these sorts of questions.

16
00:00:31,280 --> 00:00:33,880
 Getting answers to questions can assuage doubt,

17
00:00:33,880 --> 00:00:36,240
 but it can also lead to avoiding the real problem

18
00:00:36,240 --> 00:00:37,800
 of habitual doubting.

19
00:00:37,800 --> 00:00:38,840
 It is natural to think,

20
00:00:38,840 --> 00:00:40,000
 "Well, I have doubt,

21
00:00:40,000 --> 00:00:42,720
 so I need an answer to remove the doubt."

22
00:00:42,720 --> 00:00:44,360
 If you look at the doubt, however,

23
00:00:44,360 --> 00:00:47,080
 you can see that it is unhelpful, useless.

24
00:00:47,080 --> 00:00:51,440
 The experience of enlightenment does away with doubt.

25
00:00:51,440 --> 00:00:54,760
 Asotapana is freed from the mental hindrance of doubt.

26
00:00:54,760 --> 00:00:55,960
 Doubt is a hindrance.

27
00:00:55,960 --> 00:00:58,560
 It distracts and crushes the mind's potential,

28
00:00:58,560 --> 00:01:01,120
 halts the spiritual progress in its tracks.

29
00:01:01,120 --> 00:01:03,040
 Doubt stops you from seeing clearly

30
00:01:03,040 --> 00:01:05,440
 and stops you from finding peace.

31
00:01:05,440 --> 00:01:07,040
 Overcoming doubt is one way

32
00:01:07,040 --> 00:01:09,640
 of understanding the goal of our practice,

33
00:01:09,640 --> 00:01:12,080
 giving up the need for answers and solutions

34
00:01:12,080 --> 00:01:13,760
 to questions and problems.

35
00:01:13,760 --> 00:01:16,760
 In that sense, you do not need answers to any question,

36
00:01:16,760 --> 00:01:19,760
 even the ones that seem so important.

37
00:01:19,760 --> 00:01:22,480
 One common question that people seem to think is important

38
00:01:22,480 --> 00:01:25,040
 is whether there is such a thing as rebirth.

39
00:01:25,040 --> 00:01:27,800
 People have even told me that they cannot practice

40
00:01:27,800 --> 00:01:30,120
 unless they have something to convince themselves

41
00:01:30,120 --> 00:01:33,120
 of the truth of reincarnation and rebirth.

42
00:01:33,120 --> 00:01:35,320
 They say that Buddhism makes no sense to them

43
00:01:35,320 --> 00:01:37,520
 unless they can answer this question.

44
00:01:37,520 --> 00:01:39,640
 This is a very pitiable state to be in

45
00:01:39,640 --> 00:01:42,520
 because they probably will never acquire the proof

46
00:01:42,520 --> 00:01:43,960
 that they are looking for.

47
00:01:43,960 --> 00:01:46,960
 Even material science does not provide proofs.

48
00:01:46,960 --> 00:01:49,480
 The only way to prove what happens after this life

49
00:01:49,480 --> 00:01:51,720
 would be to experience it for yourself,

50
00:01:51,720 --> 00:01:53,600
 which puts you in the impossible position

51
00:01:53,600 --> 00:01:55,600
 of having to wait until you pass away

52
00:01:55,600 --> 00:01:57,640
 before you can practice.

53
00:01:57,640 --> 00:02:00,000
 I think that often people simply are excited

54
00:02:00,000 --> 00:02:02,280
 by the idea of remembering their past lives,

55
00:02:02,280 --> 00:02:04,920
 which is of course just craving.

56
00:02:04,920 --> 00:02:06,680
 To directly answer your question,

57
00:02:06,680 --> 00:02:09,000
 it is not that Buddhists believe in rebirth,

58
00:02:09,000 --> 00:02:12,160
 it is that Buddhists do not believe in death.

59
00:02:12,160 --> 00:02:14,400
 This accurately reflects the Buddhist teaching

60
00:02:14,400 --> 00:02:16,960
 as I understand them because death and Buddhism

61
00:02:16,960 --> 00:02:20,880
 from an orthodox point of view occurs at every moment.

62
00:02:20,880 --> 00:02:24,280
 This is what we mean when we say the mind does not exist.

63
00:02:24,280 --> 00:02:27,080
 Mind is just a designation for the mental aspect

64
00:02:27,080 --> 00:02:29,000
 of these momentary experiences,

65
00:02:29,000 --> 00:02:32,680
 the awareness of an object that is part of any experience.

66
00:02:32,680 --> 00:02:34,960
 When we talk about the moment of physical death,

67
00:02:34,960 --> 00:02:38,400
 we are referring to a concept because in ultimate reality,

68
00:02:38,400 --> 00:02:40,160
 nothing dies at that moment.

69
00:02:40,160 --> 00:02:42,600
 The mind and body are constantly arising

70
00:02:42,600 --> 00:02:44,200
 and ceasing together.

71
00:02:44,200 --> 00:02:46,960
 The physical and mental are arising and ceasing,

72
00:02:46,960 --> 00:02:50,360
 arising and ceasing, and that does not stop ever

73
00:02:50,360 --> 00:02:52,360
 until you free yourself from craving.

74
00:02:52,360 --> 00:02:55,120
 Doubt is insidious.

75
00:02:55,120 --> 00:02:57,320
 Doubt is not a sign that you need an answer,

76
00:02:57,320 --> 00:02:59,320
 it is a sign that you have a problem,

77
00:02:59,320 --> 00:03:02,200
 and that problem is the doubt.

78
00:03:02,200 --> 00:03:04,840
 Even if you are given answers to all your questions,

79
00:03:04,840 --> 00:03:06,440
 you will likely still have doubt.

80
00:03:06,440 --> 00:03:08,600
 The reason for this is not always

81
00:03:08,600 --> 00:03:10,680
 that you were not given the right answers,

82
00:03:10,680 --> 00:03:14,480
 but that rather that being given answers is not the answer.

83
00:03:14,480 --> 00:03:17,000
 The answer is to focus on the doubt.

84
00:03:17,000 --> 00:03:20,080
 You know you are in doubt, so focus on the doubt.

85
00:03:20,080 --> 00:03:22,120
 Forget about what you are doubting about

86
00:03:22,120 --> 00:03:24,280
 and focus on the doubt itself,

87
00:03:24,280 --> 00:03:26,600
 because once you free yourself from doubt,

88
00:03:26,600 --> 00:03:29,200
 you have done all that you need to do.

89
00:03:29,200 --> 00:03:31,560
 That being said, this specific question

90
00:03:31,560 --> 00:03:33,400
 does have a specific answer,

91
00:03:33,400 --> 00:03:35,640
 and it is an answer that can be helpful

92
00:03:35,640 --> 00:03:38,280
 in leading one to focusing on experiences,

93
00:03:38,280 --> 00:03:39,960
 of which doubt is one.

94
00:03:39,960 --> 00:03:41,720
 In order to understand reality,

95
00:03:41,720 --> 00:03:44,960
 we have to give up all of our preconceived notions about it

96
00:03:44,960 --> 00:03:44,960
,

97
00:03:44,960 --> 00:03:47,360
 because all you can say for certain about reality

98
00:03:47,360 --> 00:03:50,440
 without resorting to abstract conceptualization

99
00:03:50,440 --> 00:03:52,840
 is that experience occurs.

100
00:03:52,840 --> 00:03:54,520
 There is seeing, there is hearing,

101
00:03:54,520 --> 00:03:56,240
 there is smelling, there is tasting,

102
00:03:56,240 --> 00:03:58,400
 there is feeling, and there is thinking.

103
00:03:58,400 --> 00:04:00,520
 You can say that pretty reassuredly.

104
00:04:00,520 --> 00:04:02,720
 The question of whether what you experience

105
00:04:02,720 --> 00:04:04,280
 is real or not real,

106
00:04:04,280 --> 00:04:07,360
 or whether you are hallucinating, is meaningless.

107
00:04:07,360 --> 00:04:09,920
 The experience occurs as it occurs

108
00:04:09,920 --> 00:04:11,960
 or does not occur at all.

109
00:04:11,960 --> 00:04:14,240
 From this basic truth, you can extrapolate

110
00:04:14,240 --> 00:04:16,400
 and say that it goes against your experience

111
00:04:16,400 --> 00:04:19,400
 to suggest that experience ceases without remainder.

112
00:04:19,400 --> 00:04:22,080
 When you watch your experiences arise and cease,

113
00:04:22,080 --> 00:04:24,080
 you must admit that as far as you can see,

114
00:04:24,080 --> 00:04:26,760
 experience continues indefinitely.

115
00:04:26,760 --> 00:04:28,280
 As for what leads to rebirth,

116
00:04:28,280 --> 00:04:31,560
 you can see that experience forms a sort of causal chain.

117
00:04:31,560 --> 00:04:35,320
 Causes have effects, desire leads to fruition.

118
00:04:35,320 --> 00:04:37,000
 For example, you want something,

119
00:04:37,000 --> 00:04:38,440
 which leads to thinking about it,

120
00:04:38,440 --> 00:04:41,120
 which develops into actions and situations

121
00:04:41,120 --> 00:04:42,760
 based on the desire.

122
00:04:42,760 --> 00:04:46,200
 We can see in this life how craving leads to becoming.

123
00:04:46,200 --> 00:04:49,600
 At the moment of death, it is not categorically different.

124
00:04:49,600 --> 00:04:52,480
 Karma is the relationship between a state of mind,

125
00:04:52,480 --> 00:04:55,240
 like craving and subsequent experiences.

126
00:04:55,240 --> 00:04:58,440
 Wisdom, for example, leads to happiness and peace

127
00:04:58,440 --> 00:05:01,280
 through seeing how craving leads to suffering.

128
00:05:01,280 --> 00:05:04,720
 Karma is a relationship from one experience to the next.

129
00:05:04,720 --> 00:05:06,880
 And as such, it does not ultimately exist

130
00:05:06,880 --> 00:05:08,600
 as more than a concept.

131
00:05:08,600 --> 00:05:11,400
 You can see for yourself that when this arises,

132
00:05:11,400 --> 00:05:12,440
 that arises.

133
00:05:12,440 --> 00:05:15,280
 With the arising of this, there is the arising of that.

134
00:05:15,280 --> 00:05:18,560
 And when this does not arise, that does not arise.

