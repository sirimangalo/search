1
00:00:00,000 --> 00:00:06,540
 Hi, today I'll be talking about the number four reason why

2
00:00:06,540 --> 00:00:09,560
 everyone should practice meditation.

3
00:00:09,560 --> 00:00:11,890
 The number four reason why everyone should practice

4
00:00:11,890 --> 00:00:13,480
 meditation is because meditation

5
00:00:13,480 --> 00:00:18,240
 allows us to overcome all sorts of mental sickness.

6
00:00:18,240 --> 00:00:20,060
 Now we have to understand what we mean here by mental

7
00:00:20,060 --> 00:00:20,600
 sickness.

8
00:00:20,600 --> 00:00:23,730
 We're not talking about those things which lead people to

9
00:00:23,730 --> 00:00:25,720
 enter psychiatry wards or insane

10
00:00:25,720 --> 00:00:26,720
 asylums.

11
00:00:26,720 --> 00:00:31,290
 We're talking about states of mind which are understood by

12
00:00:31,290 --> 00:00:33,800
 people to be some sort of mental

13
00:00:33,800 --> 00:00:36,000
 distress or disease.

14
00:00:36,000 --> 00:00:38,610
 So when we understand disease in this sense, we understand

15
00:00:38,610 --> 00:00:39,960
 it as dis-ease, as not being

16
00:00:39,960 --> 00:00:41,160
 at ease.

17
00:00:41,160 --> 00:00:48,350
 So this could be from all sorts of states like depression

18
00:00:48,350 --> 00:00:52,120
 or stress, anxiety, all of

19
00:00:52,120 --> 00:00:55,130
 these things which actually nowadays people are trying to

20
00:00:55,130 --> 00:00:56,920
 find medication as a solution.

21
00:00:56,920 --> 00:01:00,040
 They're trying to use pills, they're trying to use

22
00:01:00,040 --> 00:01:02,300
 sometimes therapy or trying to find

23
00:01:02,300 --> 00:01:06,440
 all sorts of external ways of removing these mental

24
00:01:06,440 --> 00:01:07,760
 conditions.

25
00:01:07,760 --> 00:01:10,800
 And they never go at the root which is in the mind.

26
00:01:10,800 --> 00:01:13,880
 So in the meditation practice we understand that the great

27
00:01:13,880 --> 00:01:15,600
 thing about meditation is that

28
00:01:15,600 --> 00:01:18,850
 we don't ever have to take medication for something which

29
00:01:18,850 --> 00:01:20,120
 is purely mental.

30
00:01:20,120 --> 00:01:23,800
 States of mind like depression, stress, anxiety, insomnia,

31
00:01:23,800 --> 00:01:25,720
 all of these states which we tend

32
00:01:25,720 --> 00:01:29,940
 to go to see a clinical therapist or a clinical doctor for,

33
00:01:29,940 --> 00:01:32,120
 we can actually remove on our

34
00:01:32,120 --> 00:01:33,120
 own.

35
00:01:33,120 --> 00:01:35,520
 And this is one thing that people maybe don't ever realize

36
00:01:35,520 --> 00:01:36,920
 that they have this incredible

37
00:01:36,920 --> 00:01:40,650
 tool which allows you to get rid of things like insomnia,

38
00:01:40,650 --> 00:01:43,320
 depression, stress, anxiety,

39
00:01:43,320 --> 00:01:49,590
 worry, fear, phobias in really a very short time and on a

40
00:01:49,590 --> 00:01:52,840
 very real and profound level.

41
00:01:52,840 --> 00:01:57,270
 That actually when we practice meditation creating this

42
00:01:57,270 --> 00:01:59,440
 clear thought it creates a sort

43
00:01:59,440 --> 00:02:02,630
 of series of mind states which come to be a new way of

44
00:02:02,630 --> 00:02:04,080
 looking at the world.

45
00:02:04,080 --> 00:02:08,100
 So instead of looking at things as stressful, instead of

46
00:02:08,100 --> 00:02:11,240
 looking at our situation as depressing,

47
00:02:11,240 --> 00:02:13,330
 instead of looking at something as fearful we simply see it

48
00:02:13,330 --> 00:02:14,000
 for what it is.

49
00:02:14,000 --> 00:02:17,200
 If it's something we're afraid of when we say to ourselves

50
00:02:17,200 --> 00:02:18,960
 seeing, seeing, hearing,

51
00:02:18,960 --> 00:02:22,220
 hearing or thinking, thinking or even liking, liking or

52
00:02:22,220 --> 00:02:24,760
 afraid, afraid that mind state disappears

53
00:02:24,760 --> 00:02:27,520
 and it doesn't then continue.

54
00:02:27,520 --> 00:02:30,320
 It doesn't become a tendency or become a habit.

55
00:02:30,320 --> 00:02:32,900
 We get this new habit in our mind of seeing things simply

56
00:02:32,900 --> 00:02:33,640
 as they are.

57
00:02:33,640 --> 00:02:36,280
 When we are depressed we have a situation which is

58
00:02:36,280 --> 00:02:37,160
 depressing.

59
00:02:37,160 --> 00:02:40,220
 We simply say to ourselves thinking, thinking, disliking or

60
00:02:40,220 --> 00:02:41,320
 stressed or so on.

61
00:02:41,320 --> 00:02:44,300
 We remind ourselves of the true nature of it and we don't

62
00:02:44,300 --> 00:02:45,800
 give it the power to create

63
00:02:45,800 --> 00:02:49,630
 this mental sickness or mental disease or mental upset

64
00:02:49,630 --> 00:02:51,800
 however you want to call it.

65
00:02:51,800 --> 00:02:54,420
 The state of depression which can actually lead people to

66
00:02:54,420 --> 00:02:55,400
 kill themselves.

67
00:02:55,400 --> 00:02:58,770
 Actually if people were to practice meditation there would

68
00:02:58,770 --> 00:03:00,440
 be no need to ever consider to

69
00:03:00,440 --> 00:03:01,440
 try to end your life.

70
00:03:01,440 --> 00:03:06,020
 There would be no idea that something was unpleasant or

71
00:03:06,020 --> 00:03:09,040
 something was bad or depressing.

72
00:03:09,040 --> 00:03:11,960
 States like insomnia, people who suffer from insomnia.

73
00:03:11,960 --> 00:03:14,900
 I've had many people who have cured themselves of insomnia,

74
00:03:14,900 --> 00:03:16,400
 lifelong insomnia in a week of

75
00:03:16,400 --> 00:03:18,040
 meditation.

76
00:03:18,040 --> 00:03:20,940
 People who have been alcoholics are able to cure themselves

77
00:03:20,940 --> 00:03:22,360
, give up alcoholism, give

78
00:03:22,360 --> 00:03:25,420
 up drugs, give up all sorts of addictions, all sorts of

79
00:03:25,420 --> 00:03:27,200
 states which we would normally

80
00:03:27,200 --> 00:03:31,180
 consider to be a mental sickness or mental disease which we

81
00:03:31,180 --> 00:03:32,880
 go to see a doctor for.

82
00:03:32,880 --> 00:03:35,300
 We can actually cure by ourselves or with the help, if not

83
00:03:35,300 --> 00:03:36,600
 by ourselves, at least with

84
00:03:36,600 --> 00:03:39,640
 the help of a qualified meditation teacher.

85
00:03:39,640 --> 00:03:42,380
 This is the number four reason why everyone should practice

86
00:03:42,380 --> 00:03:43,140
 meditation.

87
00:03:43,140 --> 00:03:45,360
 Not just those people who are suffering from clinical

88
00:03:45,360 --> 00:03:47,040
 depression but those people who suffer

89
00:03:47,040 --> 00:03:53,070
 from these general sense of disease or discomfort which

90
00:03:53,070 --> 00:03:56,520
 arises really for everybody.

91
00:03:56,520 --> 00:03:58,970
 Thank you for tuning in, this is number four and please

92
00:03:58,970 --> 00:04:00,360
 look forward to the rest of my

93
00:04:00,360 --> 00:04:01,360
 videos in the future.

