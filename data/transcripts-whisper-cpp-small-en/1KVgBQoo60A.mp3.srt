1
00:00:00,000 --> 00:00:07,000
 Okay, good evening everyone.

2
00:00:07,000 --> 00:00:21,000
 Welcome to our evening, Dhamma.

3
00:00:25,000 --> 00:00:29,770
 So last night, one of the things that came up was

4
00:00:29,770 --> 00:00:31,000
 friendship.

5
00:00:31,000 --> 00:00:37,000
 Friendship was a good friendship with the evil.

6
00:00:37,000 --> 00:00:41,000
 Good friends, bad friends.

7
00:00:41,000 --> 00:00:47,000
 I thought it'd do to spend a little bit of time talking

8
00:00:47,000 --> 00:00:51,000
 about friendship according to Buddhism.

9
00:00:51,000 --> 00:00:56,000
 Friendship's a curious thing.

10
00:00:56,000 --> 00:01:09,280
 I would say friendship, in many cases, is something that

11
00:01:09,280 --> 00:01:17,000
 misleads us from right and wrong, from good and bad.

12
00:01:18,000 --> 00:01:22,170
 We find ourselves caught up with people because they're my

13
00:01:22,170 --> 00:01:23,000
 friend.

14
00:01:23,000 --> 00:01:27,400
 It's a curious phrase, it's a curious excuse or reason for

15
00:01:27,400 --> 00:01:29,000
 doing something.

16
00:01:29,000 --> 00:01:35,980
 Now if we mean that we owe them something, a debt of

17
00:01:35,980 --> 00:01:41,000
 gratitude perhaps, that's reasonable.

18
00:01:41,000 --> 00:01:46,190
 But it tends to be more than that, we identify with

19
00:01:46,190 --> 00:01:48,000
 individuals.

20
00:01:48,000 --> 00:01:52,460
 From a Buddhist perspective, it's quite possible that we've

21
00:01:52,460 --> 00:01:58,090
 had some connection coming from past lives with certain

22
00:01:58,090 --> 00:01:59,000
 people.

23
00:01:59,000 --> 00:02:03,740
 You notice how sometimes you're just unable to make a

24
00:02:03,740 --> 00:02:06,000
 connection with people,

25
00:02:06,000 --> 00:02:08,650
 and you think, "Well, that's someone I'd like to get to

26
00:02:08,650 --> 00:02:11,440
 know, but maybe they don't want to have anything to do with

27
00:02:11,440 --> 00:02:12,000
 you."

28
00:02:12,000 --> 00:02:20,000
 Or you look at someone and there's just no connection.

29
00:02:20,000 --> 00:02:23,000
 He just faces in the crowd.

30
00:02:23,000 --> 00:02:25,000
 When you talk to them, there's no connection.

31
00:02:25,000 --> 00:02:30,510
 But other people, you find yourself caught up with them

32
00:02:30,510 --> 00:02:33,000
 perhaps your whole life,

33
00:02:33,000 --> 00:02:41,000
 and you identify with them as being a friend.

34
00:02:41,000 --> 00:02:47,320
 Even with a debt of gratitude though, I don't think there's

35
00:02:47,320 --> 00:02:52,830
 any reason we should let that color the way we relate to

36
00:02:52,830 --> 00:02:55,000
 others.

37
00:02:55,000 --> 00:02:59,640
 Let people drag us down the wrong path just because they've

38
00:02:59,640 --> 00:03:05,520
 done good for us in the past, we don't want to abandon them

39
00:03:05,520 --> 00:03:06,000
.

40
00:03:06,000 --> 00:03:11,670
 Certainly not the right way, it doesn't help us, it doesn't

41
00:03:11,670 --> 00:03:13,000
 help them.

42
00:03:13,000 --> 00:03:20,140
 Even spend all our time and energy trying to help an

43
00:03:20,140 --> 00:03:26,000
 individual who doesn't want to be helped.

44
00:03:26,000 --> 00:03:29,170
 They're going down a destructive path and spend all your

45
00:03:29,170 --> 00:03:32,000
 energy trying to dissuade them,

46
00:03:32,000 --> 00:03:42,520
 even when the evidence clearly shows that they're not to be

47
00:03:42,520 --> 00:03:48,000
 dissuaded from their path.

48
00:03:48,000 --> 00:03:54,030
 Friendship is taken quite seriously, it's something

49
00:03:54,030 --> 00:04:00,040
 important in Buddhism, something we have to see in its

50
00:04:00,040 --> 00:04:02,000
 proper light.

51
00:04:02,000 --> 00:04:07,990
 Friendship is, well two things, first of all friendship

52
00:04:07,990 --> 00:04:10,000
 involves friendliness.

53
00:04:10,000 --> 00:04:13,690
 And ideally we're friendly towards everyone, it would be

54
00:04:13,690 --> 00:04:17,370
 nice if we could all be friends, there's no question about

55
00:04:17,370 --> 00:04:18,000
 that.

56
00:04:18,000 --> 00:04:23,580
 We should be a good friend, really to everyone, and having

57
00:04:23,580 --> 00:04:26,960
 the attitude of being friendly towards all people I think

58
00:04:26,960 --> 00:04:30,000
 is perfectly valid.

59
00:04:30,000 --> 00:04:39,610
 There's never a reason to be cruel or mean or unkind to

60
00:04:39,610 --> 00:04:41,000
 others.

61
00:04:41,000 --> 00:04:47,680
 Sometimes your kindness takes the form of limiting your

62
00:04:47,680 --> 00:04:55,160
 engagement with people for their benefit, for your benefit,

63
00:04:55,160 --> 00:05:00,000
 for the general goodness of you both.

64
00:05:00,000 --> 00:05:06,420
 But it should never be cruel or mean, and sometimes karma

65
00:05:06,420 --> 00:05:13,400
 and past life relationships means, most of us, it means we

66
00:05:13,400 --> 00:05:19,550
 have to be involved with people who we wouldn't otherwise

67
00:05:19,550 --> 00:05:22,000
 be inclined to be involved with.

68
00:05:22,000 --> 00:05:26,920
 And in fact that makes it all the more important to be kind

69
00:05:26,920 --> 00:05:31,840
 and to be friendly because it'll be a source for constant

70
00:05:31,840 --> 00:05:34,000
 or common conflict.

71
00:05:34,000 --> 00:05:38,860
 If we have bad karma with someone and our inclination is to

72
00:05:38,860 --> 00:05:43,850
 constantly be in conflict with them, then that should be a

73
00:05:43,850 --> 00:05:47,130
 very important part of our meditation practice, is learning

74
00:05:47,130 --> 00:05:50,000
 to overcome that conflict.

75
00:05:50,000 --> 00:05:57,840
 Learning to change the course of our journey in samsara so

76
00:05:57,840 --> 00:06:05,390
 that we don't get caught up in the same cycles of vengeance

77
00:06:05,390 --> 00:06:12,000
 and conflict that we were caught up in before.

78
00:06:12,000 --> 00:06:20,160
 But the second thing is that it's important to be careful,

79
00:06:20,160 --> 00:06:27,850
 careful who we, who is company and whose friendship we

80
00:06:27,850 --> 00:06:30,000
 cultivate.

81
00:06:30,000 --> 00:06:34,550
 For some people we be a good friend and that can be great

82
00:06:34,550 --> 00:06:38,910
 if we have something to provide to them and if they are

83
00:06:38,910 --> 00:06:44,000
 keen to improve through our friendship, that's great.

84
00:06:44,000 --> 00:06:51,240
 But then there are people who seek only to drag us down,

85
00:06:51,240 --> 00:06:59,180
 whose very nature is detrimental to themselves and to those

86
00:06:59,180 --> 00:07:03,000
 who would call them friends.

87
00:07:03,000 --> 00:07:09,850
 And those people you have to be careful with. You can be

88
00:07:09,850 --> 00:07:15,510
 compassionate and kind but you should never consider them

89
00:07:15,510 --> 00:07:18,000
 as proper friends.

90
00:07:18,000 --> 00:07:26,690
 So the Buddha talked about, in the Digha Nikāya, he talked

91
00:07:26,690 --> 00:07:30,540
 about different types of friends, those who appear to be

92
00:07:30,540 --> 00:07:33,000
 friends but are not really friends.

93
00:07:33,000 --> 00:07:38,390
 Those are the most common mistaken friends that we come

94
00:07:38,390 --> 00:07:44,500
 across. If someone is unfriendly and someone is clearly not

95
00:07:44,500 --> 00:07:49,000
 your friend, you're not likely to see them as your friend.

96
00:07:49,000 --> 00:07:54,890
 But there are ways by which, as I said, we mistakenly come

97
00:07:54,890 --> 00:07:57,000
 into friendship.

98
00:07:57,000 --> 00:08:21,000
 And so you put an outline to a number of these. You talked

99
00:08:21,000 --> 00:08:21,000
 about the friend who only takes the taker.

100
00:08:21,000 --> 00:08:41,010
 So people who are constantly after you for their own

101
00:08:41,010 --> 00:08:43,000
 benefit.

102
00:08:43,000 --> 00:08:49,580
 And you can see this, you can see the insincerity and the

103
00:08:49,580 --> 00:08:57,580
 mental illness involved. It's quite pitiful really when

104
00:08:57,580 --> 00:09:01,000
 people are not able to give.

105
00:09:01,000 --> 00:09:08,230
 It's a common mental illness of sorts. I phrase it in this

106
00:09:08,230 --> 00:09:10,990
 way because normally we think, "Well that person is just

107
00:09:10,990 --> 00:09:13,000
 selfish and mean and we get very upset at them."

108
00:09:13,000 --> 00:09:22,660
 But you really shouldn't be. You should pity such a person.

109
00:09:22,660 --> 00:09:31,000
 A person who constantly wants, who constantly takes.

110
00:09:31,000 --> 00:09:38,800
 They take everything, they give you little useless things,

111
00:09:38,800 --> 00:09:45,000
 useless help and expect much more in response.

112
00:09:45,000 --> 00:09:49,000
 When they do help you, they help you out of fear.

113
00:09:49,000 --> 00:09:55,080
 They do it not because they're keen at all or interested at

114
00:09:55,080 --> 00:10:01,380
 all in your benefit, but because they know they feel that

115
00:10:01,380 --> 00:10:05,000
 they have to in order to get from you.

116
00:10:05,000 --> 00:10:10,340
 And the mooch, someone to be pitied. Not pitied by giving

117
00:10:10,340 --> 00:10:15,360
 them whatever they want. Of course the worst thing you can

118
00:10:15,360 --> 00:10:20,000
 do for someone, anyone, is give them whatever they want.

119
00:10:20,000 --> 00:10:24,770
 But to be understood that this is a person who needs a

120
00:10:24,770 --> 00:10:30,000
 different kind of help, probably need to be cut off.

121
00:10:30,000 --> 00:10:35,940
 It's only a person that drags you down. You don't help them

122
00:10:35,940 --> 00:10:39,000
 by giving them everything.

123
00:10:39,000 --> 00:10:44,770
 The second one is a person who talks. In Thai they say, "D

124
00:10:44,770 --> 00:10:46,000
hi tapur."

125
00:10:46,000 --> 00:10:54,160
 Which means they're only good in speech. They never

126
00:10:54,160 --> 00:11:03,000
 actually do anything friendly or helpful to their friends.

127
00:11:03,000 --> 00:11:06,710
 They talk about good deeds they've done for you in the past

128
00:11:06,710 --> 00:11:11,950
. They remind you of all the good things they've done for

129
00:11:11,950 --> 00:11:12,000
 you.

130
00:11:12,000 --> 00:11:18,000
 And they make grandois promises about the future.

131
00:11:18,000 --> 00:11:21,000
 It's funny, there was a man who did this to me in Thailand.

132
00:11:21,000 --> 00:11:30,000
 We were in Thailand and this was how many years ago?

133
00:11:30,000 --> 00:11:34,370
 Not so many years ago. And he talked such a good game. He

134
00:11:34,370 --> 00:11:38,480
 told me about all the millions of dollars that he used to

135
00:11:38,480 --> 00:11:40,000
 have and that he was going to have.

136
00:11:40,000 --> 00:11:45,820
 And he promised. I helped him. I was helping him get into

137
00:11:45,820 --> 00:11:49,000
 being a monk. He was a temporary monk.

138
00:11:49,000 --> 00:11:51,430
 And he said, "Oh, when I disrobe, I'm going to make all

139
00:11:51,430 --> 00:11:55,640
 this money and I'm going to build you a meditation center

140
00:11:55,640 --> 00:11:58,000
 in Canada and talk to him."

141
00:11:58,000 --> 00:12:00,410
 And I only really half believed him, but I thought he was

142
00:12:00,410 --> 00:12:03,350
 sincere. I just thought, "Oh yeah, well, who knows the

143
00:12:03,350 --> 00:12:04,000
 future?"

144
00:12:04,000 --> 00:12:08,410
 But he said, "Great, well, if it happens, it happens." But

145
00:12:08,410 --> 00:12:14,000
 it turns out he was just a total scam, really.

146
00:12:14,000 --> 00:12:17,540
 He was really just, I don't know, it was bizarre because I

147
00:12:17,540 --> 00:12:21,010
 never ended up giving him and I didn't have anything great

148
00:12:21,010 --> 00:12:22,000
 to give him.

149
00:12:22,000 --> 00:12:29,980
 In the end, it just turned out to be his way of using me.

150
00:12:29,980 --> 00:12:33,490
 It seems like the sort of thing that he does. Really

151
00:12:33,490 --> 00:12:35,000
 interesting case.

152
00:12:35,000 --> 00:12:41,820
 Pityful, really. It's unpleasant to think that people get

153
00:12:41,820 --> 00:12:46,880
 in this state where they will manipulate their fellow

154
00:12:46,880 --> 00:12:48,000
 humans.

155
00:12:48,000 --> 00:12:50,270
 I mean, this is of course common for me living in a

156
00:12:50,270 --> 00:12:55,350
 monastery. I don't see it nearly as often as most, but we

157
00:12:55,350 --> 00:13:02,000
 get so inured to it or desensitized to it.

158
00:13:02,000 --> 00:13:08,670
 We think it's somehow just the way people behave. You can't

159
00:13:08,670 --> 00:13:15,230
 get by in the world without it, which is really unfortunate

160
00:13:15,230 --> 00:13:20,000
 because it's such an insane way to live.

161
00:13:20,000 --> 00:13:24,840
 How can you possibly think that this is the right way to be

162
00:13:24,840 --> 00:13:27,000
, the right way to go?

163
00:13:27,000 --> 00:13:34,000
 Clearly people who don't have a sense of clarity of mind.

164
00:13:34,000 --> 00:13:38,130
 Some people just seem to be unable, even though they might

165
00:13:38,130 --> 00:13:45,470
 pick up the practice of meditation, they're unable to

166
00:13:45,470 --> 00:13:55,000
 straighten out their minds.

167
00:13:55,000 --> 00:14:00,180
 It's like those Nigerian prince scams. They say, "Give me

168
00:14:00,180 --> 00:14:05,000
 some money and I'll give you a lot more."

169
00:14:05,000 --> 00:14:09,370
 I mean, our first inclination is to get angry at these

170
00:14:09,370 --> 00:14:14,030
 people, but it's just so incredibly sad and pitiful that

171
00:14:14,030 --> 00:14:16,000
 they live that way.

172
00:14:16,000 --> 00:14:25,320
 The third is the flatterer. It should be recognizable to

173
00:14:25,320 --> 00:14:28,470
 many of you. This is a person who tells you what a great

174
00:14:28,470 --> 00:14:31,990
 friend you are and how wonderful you are and praises you

175
00:14:31,990 --> 00:14:34,130
 and somehow thinks that that's worth something and

176
00:14:34,130 --> 00:14:36,000
 therefore they should get something in return.

177
00:14:36,000 --> 00:14:42,630
 Usually that's how this goes. They feel like flattering you

178
00:14:42,630 --> 00:14:49,130
. By flattering you, by flattering you, that makes them a

179
00:14:49,130 --> 00:14:51,000
 good friend.

180
00:14:51,000 --> 00:14:53,550
 So they'll praise your good deeds, they'll praise your evil

181
00:14:53,550 --> 00:14:56,790
 deeds. The good things you do, they'll praise. It's very

182
00:14:56,790 --> 00:14:58,000
 dangerous.

183
00:14:58,000 --> 00:15:01,970
 It's clearly a sign of some mental imbalance or some

184
00:15:01,970 --> 00:15:05,200
 problem. You might even recognize this in yourselves. We do

185
00:15:05,200 --> 00:15:06,000
 this sometimes.

186
00:15:06,000 --> 00:15:11,090
 Flattering others, feeling that it makes us a better friend

187
00:15:11,090 --> 00:15:16,000
, feeling that somehow that'll make them like us more.

188
00:15:16,000 --> 00:15:19,650
 It doesn't mean you shouldn't compliment people. In fact,

189
00:15:19,650 --> 00:15:23,720
 it is a great thing to appreciate other people's good deeds

190
00:15:23,720 --> 00:15:27,000
, but it should only be their good deeds, right?

191
00:15:27,000 --> 00:15:32,960
 You know you're a flatterer. You know someone's a flatterer

192
00:15:32,960 --> 00:15:37,000
 when they praise the bad things you do.

193
00:15:37,000 --> 00:15:40,500
 When they praise you to your face and disparage you behind

194
00:15:40,500 --> 00:15:44,000
 your back, it's a sign that they're just a flatterer.

195
00:15:44,000 --> 00:15:49,430
 There's no substance or any wisdom behind what they say.

196
00:15:49,430 --> 00:15:56,000
 There's no goodness behind it either.

197
00:15:56,000 --> 00:16:08,000
 The fourth is the person who...

198
00:16:08,000 --> 00:16:24,660
 The person who follows you or leads you or is your fellow

199
00:16:24,660 --> 00:16:28,000
 in ruin.

200
00:16:28,000 --> 00:16:34,230
 This is the person who, the friend that we can all remember

201
00:16:34,230 --> 00:16:41,000
 having, who was our friend in crime, our partner in crime.

202
00:16:41,000 --> 00:16:44,390
 When we get drunk, they get drunk. They encourage us to get

203
00:16:44,390 --> 00:16:49,610
 drunk. We encourage them to get drunk and they encourage us

204
00:16:49,610 --> 00:16:50,000
.

205
00:16:50,000 --> 00:17:00,060
 And we go wasting our energy and our lives away in gambling

206
00:17:00,060 --> 00:17:10,000
 and drinking and carrying on friends and debauchery.

207
00:17:10,000 --> 00:17:14,120
 That's probably the one that's the worst, the most

208
00:17:14,120 --> 00:17:18,620
 dangerous. It's what we see probably most often where

209
00:17:18,620 --> 00:17:22,000
 friendship leads the friends to ruin.

210
00:17:22,000 --> 00:17:26,660
 They lead each other down the wrong path. It's a very

211
00:17:26,660 --> 00:17:28,000
 dangerous...

212
00:17:28,000 --> 00:17:42,810
 It's a problem. It's a danger, right? Because it's very

213
00:17:42,810 --> 00:17:46,000
 easy to pick up such friends.

214
00:17:46,000 --> 00:17:52,360
 How do we know? This is the power of friendship. Friendship

215
00:17:52,360 --> 00:17:56,000
 is about encouraging each other.

216
00:17:56,000 --> 00:18:00,300
 It's very easy to get a friendship, to cultivate a

217
00:18:00,300 --> 00:18:04,950
 friendship, to have friends who will lead you and encourage

218
00:18:04,950 --> 00:18:09,000
 you to lead them down the wrong path.

219
00:18:09,000 --> 00:18:12,330
 And if and when you decide that you've got a moral compass

220
00:18:12,330 --> 00:18:15,470
 and you start to get an idea of what's right and what's

221
00:18:15,470 --> 00:18:19,480
 wrong, what's good and what's bad, what's to your benefit

222
00:18:19,480 --> 00:18:21,000
 and to your detriment.

223
00:18:21,000 --> 00:18:25,000
 This is something that shakes up a lot of friendships.

224
00:18:25,000 --> 00:18:30,000
 Practicing meditation is sure to shake up old friendships.

225
00:18:30,000 --> 00:18:33,200
 As you start to realize that you weren't helping each other

226
00:18:33,200 --> 00:18:36,400
, you weren't bringing happiness, peace, freedom from

227
00:18:36,400 --> 00:18:38,000
 suffering to each other.

228
00:18:38,000 --> 00:18:48,000
 We are only entangling each other up in conflict and evil

229
00:18:48,000 --> 00:18:53,000
 for lack of a better word.

230
00:18:53,000 --> 00:18:57,030
 So those are bad friends. Now, positively, the Buddha

231
00:18:57,030 --> 00:19:01,000
 offered some advice on where how to find good friends.

232
00:19:01,000 --> 00:19:07,000
 He gave a list of four good friends.

233
00:19:07,000 --> 00:19:11,000
 First is someone who's helpful. And this is a great friend.

234
00:19:11,000 --> 00:19:16,000
 Someone who supports you.

235
00:19:16,000 --> 00:19:25,000
 When you don't notice something's wrong, they protect you.

236
00:19:25,000 --> 00:19:31,230
 They protect you, they protect your life, they protect your

237
00:19:31,230 --> 00:19:33,000
 possessions.

238
00:19:33,000 --> 00:19:38,900
 They will be conscious of your benefit. I mean, that's a

239
00:19:38,900 --> 00:19:43,860
 rare thing, a great thing. Something to be emulated,

240
00:19:43,860 --> 00:19:46,000
 something to be respected.

241
00:19:46,000 --> 00:19:50,900
 Someone who looks after you, someone who thinks of you and

242
00:19:50,900 --> 00:19:54,000
 your benefit and your detriment.

243
00:19:54,000 --> 00:20:00,860
 And in times of danger, they are a refuge, a person who you

244
00:20:00,860 --> 00:20:05,000
 can go to when you have problems.

245
00:20:05,000 --> 00:20:09,430
 Someone who goes the extra mile when you ask for help, they

246
00:20:09,430 --> 00:20:12,000
 give more than what was asked.

247
00:20:12,000 --> 00:20:17,460
 This is a sign of a good friend, a sign of someone who is

248
00:20:17,460 --> 00:20:22,780
 really rare and to be emulated, to be praised, to be

249
00:20:22,780 --> 00:20:25,000
 associated with.

250
00:20:25,000 --> 00:20:28,510
 Not just because of what you can get from them, but because

251
00:20:28,510 --> 00:20:31,000
 it's someone with a good heart that should be emulated.

252
00:20:31,000 --> 00:20:36,360
 And if friends, any two friends are like this, then they

253
00:20:36,360 --> 00:20:39,000
 both, of course, gain.

254
00:20:39,000 --> 00:20:44,000
 We're always helping each other, we both gain.

255
00:20:44,000 --> 00:20:50,000
 The second is someone who is unmoved in good times and bad,

256
00:20:50,000 --> 00:20:51,000
 right?

257
00:20:51,000 --> 00:20:53,580
 Because you've got the fair weather friend, I think is the

258
00:20:53,580 --> 00:20:54,000
 word.

259
00:20:54,000 --> 00:20:58,450
 Someone who, in good times they're with you, but when

260
00:20:58,450 --> 00:21:04,670
 things go wrong, someone who doesn't stick with you and the

261
00:21:04,670 --> 00:21:07,000
 going gets tough.

262
00:21:07,000 --> 00:21:10,000
 Someone who doesn't stick with you when problems arise.

263
00:21:10,000 --> 00:21:14,000
 Well, this is a person who's not like that.

264
00:21:14,000 --> 00:21:22,000
 A person who tells you their secrets, guards your secrets.

265
00:21:22,000 --> 00:21:26,000
 Who does not let you down in misfortune.

266
00:21:26,000 --> 00:21:33,000
 Someone who is with you through thick and thin.

267
00:21:33,000 --> 00:21:37,000
 Who might even sacrifice their life for you.

268
00:21:37,000 --> 00:21:41,000
 And that's the sort of friend that is hard to find.

269
00:21:41,000 --> 00:21:47,000
 Someone who cares for you.

270
00:21:47,000 --> 00:21:50,000
 Someone who has this wish to see you happy.

271
00:21:50,000 --> 00:21:55,000
 And that's what friendship is for, right?

272
00:21:55,000 --> 00:21:59,800
 That's what true friendship should be for, not like any

273
00:21:59,800 --> 00:22:01,000
 relationship.

274
00:22:01,000 --> 00:22:06,000
 No, what can I get from this person? That doesn't work.

275
00:22:06,000 --> 00:22:10,910
 Any two friends who are inclined to wonder what the other

276
00:22:10,910 --> 00:22:13,000
 person can give to them

277
00:22:13,000 --> 00:22:23,000
 will be a friendship of always lack or wanting.

278
00:22:23,000 --> 00:22:24,980
 Whereas if both friends are thinking, what can I do for

279
00:22:24,980 --> 00:22:26,000
 this person?

280
00:22:26,000 --> 00:22:31,500
 How can I make them happy? What can I do to bring good to

281
00:22:31,500 --> 00:22:32,000
 them?

282
00:22:32,000 --> 00:22:39,000
 Then both gain.

283
00:22:39,000 --> 00:22:46,000
 The third is a friend who points out what is good.

284
00:22:46,000 --> 00:22:49,610
 This is the best friend, number three here, but it's really

285
00:22:49,610 --> 00:22:51,000
 the one that in Buddhism

286
00:22:51,000 --> 00:22:55,540
 when we talk about a good friend, someone who points out

287
00:22:55,540 --> 00:22:56,000
 good,

288
00:22:56,000 --> 00:23:01,000
 someone who teaches or instructs, advises you,

289
00:23:01,000 --> 00:23:05,450
 someone who you can go to for advice, who has a good head

290
00:23:05,450 --> 00:23:06,000
 on their shoulders

291
00:23:06,000 --> 00:23:09,310
 and much to say about right and wrong, good and bad, that

292
00:23:09,310 --> 00:23:14,000
 is of benefit, that is of value.

293
00:23:14,000 --> 00:23:17,710
 Perhaps not someone who preaches, but someone who is good

294
00:23:17,710 --> 00:23:19,000
 for giving advice,

295
00:23:19,000 --> 00:23:24,000
 someone who has their head on straight, who knows,

296
00:23:24,000 --> 00:23:28,000
 who has things to tell.

297
00:23:28,000 --> 00:23:31,720
 They keep you from wrongdoing. They support you in doing

298
00:23:31,720 --> 00:23:32,000
 good.

299
00:23:32,000 --> 00:23:38,000
 They give you encouragement when you do good things.

300
00:23:38,000 --> 00:23:41,000
 They tell you things you don't know. That's a rare friend,

301
00:23:41,000 --> 00:23:42,000
 right?

302
00:23:42,000 --> 00:23:50,120
 Someone who has, who is able to teach you, to give you

303
00:23:50,120 --> 00:23:56,000
 knowledge that you didn't have.

304
00:23:56,000 --> 00:24:01,000
 And finally they lead you to happiness.

305
00:24:01,000 --> 00:24:06,000
 They lead you on to what is good.

306
00:24:06,000 --> 00:24:10,820
 It's the measure of wisdom, right? It's the measure of a

307
00:24:10,820 --> 00:24:14,000
 friend who is wise.

308
00:24:14,000 --> 00:24:17,250
 It's that the things they tell you bring you peace and

309
00:24:17,250 --> 00:24:23,000
 happiness and freedom from suffering.

310
00:24:23,000 --> 00:24:28,870
 And the fourth type of true friend is the sympathetic

311
00:24:28,870 --> 00:24:30,000
 friend,

312
00:24:30,000 --> 00:24:32,000
 one who sympathizes with you.

313
00:24:32,000 --> 00:24:36,520
 They don't rejoice at your misfortune. They rejoice in your

314
00:24:36,520 --> 00:24:37,000
 good fortune.

315
00:24:37,000 --> 00:24:43,460
 Someone who has mudita, true mudita, not just someone who

316
00:24:43,460 --> 00:24:44,000
 says it,

317
00:24:44,000 --> 00:24:47,470
 but someone who is really happy when they hear, someone who

318
00:24:47,470 --> 00:24:50,000
 you know when you tell them good things that you've done,

319
00:24:50,000 --> 00:24:52,650
 good things that have happened to you that you know that

320
00:24:52,650 --> 00:24:54,000
 they're happy for you.

321
00:24:54,000 --> 00:24:56,810
 They stop others who speak against you and someone says

322
00:24:56,810 --> 00:24:58,000
 something bad about you,

323
00:24:58,000 --> 00:25:02,000
 they protect you, they correct them.

324
00:25:02,000 --> 00:25:08,290
 And when others speak in praise, they agree, they commend

325
00:25:08,290 --> 00:25:10,000
 such people.

326
00:25:10,000 --> 00:25:16,000
 It's a fairly simple and not a terribly deep teaching, but

327
00:25:16,000 --> 00:25:17,000
 you know,

328
00:25:17,000 --> 00:25:21,170
 I think it's important we go over various aspects of

329
00:25:21,170 --> 00:25:22,000
 Buddhism.

330
00:25:22,000 --> 00:25:26,270
 I mean, this is a very practical teaching, I think one that

331
00:25:26,270 --> 00:25:28,000
 many of us can relate to.

332
00:25:28,000 --> 00:25:32,000
 But the deeper teaching I think is about how this mind,

333
00:25:32,000 --> 00:25:36,580
 how the mind works in cultivating friendship, both being

334
00:25:36,580 --> 00:25:38,000
 friendly

335
00:25:38,000 --> 00:25:43,120
 and in clinging to people, identifying this is my friend,

336
00:25:43,120 --> 00:25:45,000
 which is of course problematic.

337
00:25:45,000 --> 00:25:47,620
 Of course, it's great to be friendly. It's good. It's

338
00:25:47,620 --> 00:25:49,000
 wholesome to be friendly towards all.

339
00:25:49,000 --> 00:25:54,280
 But to cling to someone just because they're our friend, it

340
00:25:54,280 --> 00:25:56,000
's never a good reason for any,

341
00:25:56,000 --> 00:26:01,750
 it's never a good reason at any rate, at any rate, in any

342
00:26:01,750 --> 00:26:03,000
 case.

343
00:26:03,000 --> 00:26:05,940
 Even good people, we shouldn't cling to them just because

344
00:26:05,940 --> 00:26:07,000
 they're our friends.

345
00:26:07,000 --> 00:26:15,740
 We should cultivate their friendship because it's too, for

346
00:26:15,740 --> 00:26:18,000
 the ultimate gain,

347
00:26:18,000 --> 00:26:22,000
 cultivating peace, happiness, freedom from suffering.

348
00:26:22,000 --> 00:26:26,870
 So there you go a little bit on friendship. Thank you all

349
00:26:26,870 --> 00:26:28,000
 for tuning in.

