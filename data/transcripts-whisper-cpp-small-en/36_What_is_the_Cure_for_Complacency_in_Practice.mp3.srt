1
00:00:00,000 --> 00:00:04,400
 Difficulties in Meditation Q&A

2
00:00:04,400 --> 00:00:08,240
 What is the cure for complacency in practice?

3
00:00:08,240 --> 00:00:13,200
 Question. What is the cure for complacency in practice?

4
00:00:13,200 --> 00:00:18,600
 Answer. A sense of urgency is a cure for complacency.

5
00:00:18,600 --> 00:00:22,000
 The easiest way to develop a sense of urgency is

6
00:00:22,000 --> 00:00:26,240
 association with people who have a sense of urgency.

7
00:00:26,240 --> 00:00:29,910
 When you are alone, it is very easy to become complacent,

8
00:00:29,910 --> 00:00:34,400
 even resign to failure, because the practice is difficult.

9
00:00:34,400 --> 00:00:38,050
 Without encouragement and support from others on the path,

10
00:00:38,050 --> 00:00:40,880
 it is easy to get weighed down by the central world and

11
00:00:40,880 --> 00:00:45,580
 pulled away from the path by those who are disinclined to

12
00:00:45,580 --> 00:00:45,920
 practice.

13
00:00:45,920 --> 00:00:50,260
 Associating with good people is the most important thing in

14
00:00:50,260 --> 00:00:54,850
 the spiritual life. Listening to talks, reading books, or

15
00:00:54,850 --> 00:00:58,320
 studying can also be a part of association.

16
00:00:58,320 --> 00:01:02,410
 Another useful practice is meditative reflections on death

17
00:01:02,410 --> 00:01:06,560
 and the free characteristics of impermanence, suffering,

18
00:01:06,560 --> 00:01:09,280
 and non-self in a conventional context.

19
00:01:09,280 --> 00:01:12,810
 Like the idea that all of the things we cling to will one

20
00:01:12,810 --> 00:01:14,560
 day change and disappear.

21
00:01:14,560 --> 00:01:17,920
 The realization that nothing is stable and that at any

22
00:01:17,920 --> 00:01:21,320
 moment we could be subject to great suffering from any

23
00:01:21,320 --> 00:01:25,860
 number of causes, like sickness, accidents, natural and

24
00:01:25,860 --> 00:01:32,000
 human-made disaster, robbery, crime and punishment, etc.

25
00:01:32,000 --> 00:01:35,850
 Thinking about these sorts of things, meditating on the

26
00:01:35,850 --> 00:01:40,030
 inevitability of suffering and death, meditating on the rep

27
00:01:40,030 --> 00:01:44,500
ulsiveness of the body, meditating on the nature of the body

28
00:01:44,500 --> 00:01:47,440
 parts as being not as desirable, etc.

29
00:01:47,440 --> 00:01:51,440
 Allow us to cultivate a sense of urgency and a sense of the

30
00:01:51,440 --> 00:01:54,240
 importance of spiritual practice.

31
00:01:54,240 --> 00:01:58,230
 Find a meditation center, do a meditation course, stay with

32
00:01:58,230 --> 00:02:01,770
 the teacher, and do the best you can. Do not expect

33
00:02:01,770 --> 00:02:04,400
 yourself to be the perfect meditator.

34
00:02:04,400 --> 00:02:07,740
 Sometimes you may be discouraged or overwhelmed by worldly

35
00:02:07,740 --> 00:02:11,570
 affairs, so just try your best to cultivate as much impetus

36
00:02:11,570 --> 00:02:13,840
 to practice and do what you can.

37
00:02:13,840 --> 00:02:17,430
 Ultimately, the path to enlightenment is a difficult and

38
00:02:17,430 --> 00:02:19,360
 sometimes roundabout one.

39
00:02:19,360 --> 00:02:24,040
 Our teacher Ajahn Tong was asked the same question. He said

40
00:02:24,040 --> 00:02:27,180
, "You can't succeed in the world if you don't work hard.

41
00:02:27,180 --> 00:02:30,640
 How could it be any different in the practice?"

42
00:02:30,640 --> 00:02:33,480
 Sometimes we might think of the Dharma as some kind of

43
00:02:33,480 --> 00:02:37,470
 hobby or pastime, something that we do on the weekends or

44
00:02:37,470 --> 00:02:40,510
 holidays, by not taking it seriously, just like any worldly

45
00:02:40,510 --> 00:02:41,200
 pursuit.

46
00:02:41,200 --> 00:02:46,110
 If you don't cultivate it, if you don't work hard at it,

47
00:02:46,110 --> 00:02:48,800
 you cannot hope to succeed.

