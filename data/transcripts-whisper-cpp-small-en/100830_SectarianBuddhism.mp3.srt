1
00:00:00,000 --> 00:00:10,210
 Okay, welcome everyone. Today I thought I'd start with a

2
00:00:10,210 --> 00:00:14,240
 quote. And this is a quote from

3
00:00:14,240 --> 00:00:26,130
 an English poet, I believe. "So often theologic wars, the

4
00:00:26,130 --> 00:00:30,680
 disputants I wean, rail on in utter

5
00:00:30,680 --> 00:00:34,730
 ignorance of what each other means, and prate about an

6
00:00:34,730 --> 00:00:37,240
 elephant not one of them has seen."

7
00:00:37,240 --> 00:00:54,080
 This quote is taken from some poem from medieval England, I

8
00:00:54,080 --> 00:01:00,480
 believe. But it's original sources

9
00:01:00,480 --> 00:01:05,180
 from India. This is an old Indian legend. And if you want

10
00:01:05,180 --> 00:01:07,320
 to read about the Buddhist

11
00:01:07,320 --> 00:01:16,400
 version of it, it's on the internet as well. And the story

12
00:01:16,400 --> 00:01:18,720
 goes that there was once a king

13
00:01:18,720 --> 00:01:25,020
 who wanted to have some fun, and so he called together a

14
00:01:25,020 --> 00:01:28,080
 bunch of blind men. And he had

15
00:01:28,080 --> 00:01:38,730
 them examine an elephant. And they each looked at, they

16
00:01:38,730 --> 00:01:42,600
 each came up to the elephant and being

17
00:01:42,600 --> 00:01:48,300
 blind, of course, they had to grab a piece of the elephant

18
00:01:48,300 --> 00:01:53,640
 with their hands. And so one

19
00:01:53,640 --> 00:01:56,860
 of the blind men looked at the grab the head of the

20
00:01:56,860 --> 00:01:59,560
 elephant. And they said to him, Oh,

21
00:01:59,560 --> 00:02:02,840
 this is an elephant. Some got the ear and they said, this

22
00:02:02,840 --> 00:02:04,560
 is the elephant. Some got

23
00:02:04,560 --> 00:02:13,020
 a task, the body, the foot got different pieces of the

24
00:02:13,020 --> 00:02:15,680
 elephant. And then they came back to

25
00:02:15,680 --> 00:02:17,160
 the king and the king said, Well, have you seen the

26
00:02:17,160 --> 00:02:18,360
 elephant? And they said, Yes, we've

27
00:02:18,360 --> 00:02:23,310
 seen the elephant. And he said, Well, then tell me what so

28
00:02:23,310 --> 00:02:25,720
 what's an elephant like? And

29
00:02:25,720 --> 00:02:28,020
 the ones who had touched the head of the elephant said, Oh,

30
00:02:28,020 --> 00:02:29,320
 an elephant is just like a water

31
00:02:29,320 --> 00:02:34,000
 jar. The ones who had touched the ear, they said, Oh, no,

32
00:02:34,000 --> 00:02:36,600
 no, no, no, an elephant is like

33
00:02:36,600 --> 00:02:44,320
 a winnowing basket. Big basket that they winnow rice in.

34
00:02:44,320 --> 00:02:45,320
 And another, the other group that

35
00:02:45,320 --> 00:02:48,430
 had been shown the been touched the task said, No, no, no.

36
00:02:48,430 --> 00:02:51,880
 The task is is like a plowshare.

37
00:02:51,880 --> 00:02:55,950
 An elephant is like a plowshare. And the one that touched

38
00:02:55,950 --> 00:02:57,600
 the trunk said, No, no, no, it's

39
00:02:57,600 --> 00:03:01,940
 like a plow pool. And the other said it's like a store room

40
00:03:01,940 --> 00:03:04,160
. It's like a post. It's

41
00:03:04,160 --> 00:03:11,010
 like a mortar. It's like a room and so on. Based on the

42
00:03:11,010 --> 00:03:11,960
 pieces of the elephant that they

43
00:03:11,960 --> 00:03:17,200
 had touched. Because none of them their hands are are only

44
00:03:17,200 --> 00:03:20,040
 so big and can only touch a piece

45
00:03:20,040 --> 00:03:24,860
 of the elephant. And so as a result, they came to

46
00:03:24,860 --> 00:03:28,080
 understand the elephant to be just

47
00:03:28,080 --> 00:03:35,640
 a piece of the elephant. And this, this teaching is so

48
00:03:35,640 --> 00:03:38,640
 popular, because this is exactly how

49
00:03:38,640 --> 00:03:46,120
 humans behave in in what is most important in life. What is

50
00:03:46,120 --> 00:03:46,960
 most important to people

51
00:03:46,960 --> 00:03:52,650
 when one of the things that's most important being religion

52
00:03:52,650 --> 00:03:54,320
. And this is especially true

53
00:03:54,320 --> 00:04:05,680
 in religion. And so it's passed along with with with many

54
00:04:05,680 --> 00:04:12,960
 different religious traditions.

55
00:04:12,960 --> 00:04:17,830
 As a teaching for people who would would spend all of their

56
00:04:17,830 --> 00:04:19,920
 time denouncing other sects

57
00:04:19,920 --> 00:04:28,950
 and other religions and promoting their own. And I think

58
00:04:28,950 --> 00:04:33,200
 this is incredibly apt for Buddhism.

59
00:04:33,200 --> 00:04:39,040
 And it shows some foresight of the Buddha in relating this

60
00:04:39,040 --> 00:04:40,320
 story. Even in the time of

61
00:04:40,320 --> 00:04:43,330
 the Buddha, there, there were many different religious

62
00:04:43,330 --> 00:04:45,440
 groups who were fighting and wrangling

63
00:04:45,440 --> 00:04:49,960
 and unable to agree on what was the truth. But even

64
00:04:49,960 --> 00:04:55,120
 nowadays, even in Buddhism, we find

65
00:04:55,120 --> 00:05:03,100
 sometimes that we're unable to agree on even unable to

66
00:05:03,100 --> 00:05:07,320
 agree on what Buddhism is or find

67
00:05:07,320 --> 00:05:14,430
 ourselves separating the various practitioners of Buddhism

68
00:05:14,430 --> 00:05:20,000
 and separating into us and them

69
00:05:20,000 --> 00:05:23,260
 and trying to distinguish ourselves from the other groups

70
00:05:23,260 --> 00:05:26,240
 of Buddhism. So we have the Theravada,

71
00:05:26,240 --> 00:05:31,410
 we have the Mahayana, we have Zen Buddhism, we have Vajray

72
00:05:31,410 --> 00:05:36,560
ana Buddhism. We have many different

73
00:05:36,560 --> 00:05:46,160
 names for Buddhism in many different places. And one of the

74
00:05:46,160 --> 00:05:47,680
 things that really strikes

75
00:05:47,680 --> 00:05:53,320
 me when you look at all of these traditions is that really

76
00:05:53,320 --> 00:05:56,640
 they're just like the elephant.

77
00:05:56,640 --> 00:06:00,870
 It's as though each of these schools has picked a piece of

78
00:06:00,870 --> 00:06:03,680
 the elephant, in this case, a piece

79
00:06:03,680 --> 00:06:07,680
 of Buddhism and has said this is Buddhism. And then they

80
00:06:07,680 --> 00:06:10,040
 develop that piece to the extent

81
00:06:10,040 --> 00:06:16,640
 that they blot out or forget about or deny the importance

82
00:06:16,640 --> 00:06:20,120
 of the other pieces as though

83
00:06:20,120 --> 00:06:27,660
 an elephant were only its trunk or only its tail. I think

84
00:06:27,660 --> 00:06:29,160
 this is one mistake that Buddhists

85
00:06:29,160 --> 00:06:34,630
 make in this regard. The other mistake that we often make

86
00:06:34,630 --> 00:06:37,000
 is one of, I guess the word

87
00:06:37,000 --> 00:06:44,530
 you could use is bigotry, where we say only my school does

88
00:06:44,530 --> 00:06:47,920
 X or my school focuses on Y

89
00:06:47,920 --> 00:06:56,700
 or this or that. And we make the claim that other schools

90
00:06:56,700 --> 00:06:58,600
 don't concern themselves with

91
00:06:58,600 --> 00:07:04,220
 this. And the funny thing is you can find just about every

92
00:07:04,220 --> 00:07:05,240
 school of Buddhism saying

93
00:07:05,240 --> 00:07:14,140
 the same thing about the same subject. You can find various

94
00:07:14,140 --> 00:07:16,400
 groups and if you go around

95
00:07:16,400 --> 00:07:19,010
 and talk, especially coming from one group and talking to

96
00:07:19,010 --> 00:07:20,480
 people of another group, they

97
00:07:20,480 --> 00:07:35,150
 will say, "Yeah, you can turn that off." They will say, "Oh

98
00:07:35,150 --> 00:07:39,280
, yes, I know your school, but

99
00:07:39,280 --> 00:07:47,800
 in my school we focus on this. I know your school focuses

100
00:07:47,800 --> 00:07:51,280
 on that." And it's quite interesting

101
00:07:51,280 --> 00:07:56,120
 to hear these sorts of things because they're rarely, if

102
00:07:56,120 --> 00:07:58,760
 ever true, and they most often

103
00:07:58,760 --> 00:08:23,760
 have to do with things like…

