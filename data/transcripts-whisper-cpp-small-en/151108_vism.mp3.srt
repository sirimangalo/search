1
00:00:00,000 --> 00:00:07,280
 Thank you.

2
00:00:07,280 --> 00:00:11,640
 So we are on page 311 in the Visuddhi Maga Chapter 9, The

3
00:00:11,640 --> 00:00:13,600
 Divine Abidings, and we're

4
00:00:13,600 --> 00:00:15,800
 on Section 91.

5
00:00:15,800 --> 00:00:18,560
 And Aurora, would you start us off?

6
00:00:18,560 --> 00:00:19,560
 Yes.

7
00:00:19,560 --> 00:00:23,880
 In general, now having thus known these divine abidings,

8
00:00:23,880 --> 00:00:26,640
 told by the Divine One, supremely

9
00:00:26,640 --> 00:00:33,560
 wise, there is this general explanation to concerning them

10
00:00:33,560 --> 00:00:36,920
 that he should recognize.

11
00:00:36,920 --> 00:00:41,060
 Now as to the meaning, firstly, of loving kindness,

12
00:00:41,060 --> 00:00:44,960
 compassion, gladness, and equanimity,

13
00:00:44,960 --> 00:00:52,920
 it fattens, fattens, machati, mityati.

14
00:00:52,920 --> 00:00:59,910
 Thus it is loving kindness, metta, is the solvent, sanyah

15
00:00:59,910 --> 00:01:03,400
ati, is the meaning, also it

16
00:01:03,400 --> 00:01:08,640
 comes about with respect to a friend, mitta, or it is

17
00:01:08,640 --> 00:01:12,280
 behavior towards a friend, thus it

18
00:01:12,280 --> 00:01:15,720
 is loving kindness, metta.

19
00:01:15,720 --> 00:01:21,410
 When there is suffering in others, it causes karupdi, good

20
00:01:21,410 --> 00:01:25,120
 people's heart, to be moved.

21
00:01:25,120 --> 00:01:35,170
 Kampana, thus it is compassion, karuna, or alternatively it

22
00:01:35,170 --> 00:01:39,080
 combats ginnati, others'

23
00:01:39,080 --> 00:01:45,960
 suffering attacks, and demolishes it.

24
00:01:45,960 --> 00:01:53,080
 It is compassion, or alternatively it is scattered karianti

25
00:01:53,080 --> 00:01:55,660
 upon those who suffered.

26
00:01:55,660 --> 00:02:01,780
 It is extended to them by privation, thus it is compassion,

27
00:02:01,780 --> 00:02:04,720
 karuna, those endowed with

28
00:02:04,720 --> 00:02:13,220
 it are glad, mudanti, or it so is glad, mudati, or it is

29
00:02:13,220 --> 00:02:18,600
 the mere act of being glad, mudana,

30
00:02:18,600 --> 00:02:24,720
 thus it is gladness, mudita, mudita.

31
00:02:24,720 --> 00:02:39,220
 It looks on at upikanti, abandoning such an interestedness

32
00:02:39,220 --> 00:02:45,760
 as thinking may they be free

33
00:02:45,760 --> 00:02:53,870
 from enmity and having recourse to neutrality, thus it is

34
00:02:53,870 --> 00:02:57,280
 equanimity, upika.

35
00:02:57,280 --> 00:03:13,940
 When we pay respect to the Buddha, is that an instance of

36
00:03:13,940 --> 00:03:16,840
 metta?

37
00:03:16,840 --> 00:03:21,520
 How do we identify that quality of the mind?

38
00:03:21,520 --> 00:03:24,520
 Which quality?

39
00:03:24,520 --> 00:03:27,960
 We are talking about respect to the Buddha.

40
00:03:27,960 --> 00:03:37,640
 Here it also comes about with respect to a friend.

41
00:03:37,640 --> 00:03:40,860
 This is the use of the word respect in an indiomatically

42
00:03:40,860 --> 00:03:42,720
 English way, means in regards

43
00:03:42,720 --> 00:03:47,680
 to a friend, in relation to.

44
00:03:47,680 --> 00:03:50,600
 With respect to means in relation to.

45
00:03:50,600 --> 00:03:55,840
 There is nothing to do with paying respect.

46
00:03:55,840 --> 00:04:01,080
 So the feeling of respect has nothing to do with the pure

47
00:04:01,080 --> 00:04:02,040
 words.

48
00:04:02,040 --> 00:04:05,040
 Is that correct Madhav?

49
00:04:05,040 --> 00:04:07,630
 I wouldn't say it has nothing to do with it, but it is not

50
00:04:07,630 --> 00:04:17,560
 one of the four brahma-vihayras.

51
00:04:17,560 --> 00:04:21,380
 As to the characteristic, etc., loving kindness is

52
00:04:21,380 --> 00:04:24,480
 characterized here as promoting the aspect

53
00:04:24,480 --> 00:04:26,040
 of welfare.

54
00:04:26,040 --> 00:04:28,640
 Its function is to prefer welfare.

55
00:04:28,640 --> 00:04:33,120
 It is manifested as the removal of annoyance.

56
00:04:33,120 --> 00:04:37,240
 Ex proximate cause is seeing loveableness in beings.

57
00:04:37,240 --> 00:04:41,810
 It succeeds when it makes ill will subside and it fails

58
00:04:41,810 --> 00:04:45,200
 when it produces selfless affection.

59
00:04:45,200 --> 00:04:51,730
 Compassion is characterized as promoting the aspect of all

60
00:04:51,730 --> 00:04:53,600
aying suffering.

61
00:04:53,600 --> 00:04:56,960
 Its function resides in not bearing others suffering.

62
00:04:56,960 --> 00:05:00,800
 It is manifested as non-cruelty.

63
00:05:00,800 --> 00:05:04,270
 Its proximate cause is to see helplessness in those

64
00:05:04,270 --> 00:05:06,400
 overwhelmed by suffering.

65
00:05:06,400 --> 00:05:10,340
 It succeeds when it makes cruelty subside and it fails when

66
00:05:10,340 --> 00:05:11,960
 it produces sorrow.

67
00:05:11,960 --> 00:05:16,200
 I can't remember whether we've had this yet.

68
00:05:16,200 --> 00:05:21,600
 You see that he's talking about four different things.

69
00:05:21,600 --> 00:05:24,190
 We're going to see this throughout the script, the text, if

70
00:05:24,190 --> 00:05:25,400
 we haven't seen it already.

71
00:05:25,400 --> 00:05:30,800
 There are four aspects in the Abhidhamma to each Dhamma.

72
00:05:30,800 --> 00:05:34,120
 So each of these Dhammas is now being discussed.

73
00:05:34,120 --> 00:05:38,580
 If you study the Abhidhamma, you already have a familiarity

74
00:05:38,580 --> 00:05:39,240
 with this.

75
00:05:39,240 --> 00:05:45,270
 But it's the function, the manifestation, the proximate

76
00:05:45,270 --> 00:05:48,880
 cause and I think maybe the characteristic,

77
00:05:48,880 --> 00:05:52,790
 the first one as to whether it succeeds and fails, that's

78
00:05:52,790 --> 00:05:53,480
 extra.

79
00:05:53,480 --> 00:05:58,840
 So the characteristic, the function, the manifestation and

80
00:05:58,840 --> 00:06:02,280
 the proximate cause is the four aspects

81
00:06:02,280 --> 00:06:04,960
 that make up a description in the Abhidhamma.

82
00:06:04,960 --> 00:06:06,720
 So that's quite standard and we'll see that again.

83
00:06:06,720 --> 00:06:22,790
 So Banthe, regarding Karuna, now if we see someone here,

84
00:06:22,790 --> 00:06:27,240
 should it always be that we

85
00:06:27,240 --> 00:06:32,030
 should go and help if we are to successfully practice Kar

86
00:06:32,030 --> 00:06:32,600
una?

87
00:06:32,600 --> 00:06:37,260
 Or if we don't help, does that mean we don't have Karuna at

88
00:06:37,260 --> 00:06:38,640
 that moment?

89
00:06:38,640 --> 00:06:40,960
 Yeah, I think that's correct.

90
00:06:40,960 --> 00:06:45,550
 But we have to qualify that by saying it's not a sign of un

91
00:06:45,550 --> 00:06:47,840
wholesomeness if you don't

92
00:06:47,840 --> 00:06:48,840
 cultivate Karuna.

93
00:06:48,840 --> 00:06:53,980
 But for someone who is dedicated to this practice, I think

94
00:06:53,980 --> 00:06:56,920
 it would be appropriate to go and

95
00:06:56,920 --> 00:06:59,280
 help at every instance, every occasion.

96
00:06:59,280 --> 00:07:02,070
 Now if someone's not that keen on it or that interested in

97
00:07:02,070 --> 00:07:03,360
 it, they have no need to go

98
00:07:03,360 --> 00:07:06,080
 and help just anyone.

99
00:07:06,080 --> 00:07:11,050
 But there are cases where not going to help someone leads

100
00:07:11,050 --> 00:07:12,400
 to cruelty.

101
00:07:12,400 --> 00:07:16,890
 When you decide not to, that's out of cruelty or cruelty

102
00:07:16,890 --> 00:07:19,360
 arises or Karuna is often used

103
00:07:19,360 --> 00:07:22,360
 to counteract cruelty that's innate in us.

104
00:07:22,360 --> 00:07:24,660
 But it doesn't mean that just because you don't help

105
00:07:24,660 --> 00:07:26,480
 someone means you're cruel.

106
00:07:26,480 --> 00:07:29,240
 I mean, we'd have to see exactly what's going on, but I

107
00:07:29,240 --> 00:07:30,560
 think that's the case.

108
00:07:30,560 --> 00:07:33,590
 There's also a certain aspect of wisdom when it's

109
00:07:33,590 --> 00:07:35,920
 appropriate to help, when it's actually

110
00:07:35,920 --> 00:07:39,670
 going to be helpful, when it's of greater benefit to the

111
00:07:39,670 --> 00:07:41,040
 other person than it is of

112
00:07:41,040 --> 00:07:48,000
 detriment to you, that kind of thing.

113
00:07:48,000 --> 00:07:53,870
 It may also help to subside cruelty within you, the kind of

114
00:07:53,870 --> 00:07:56,960
 cruelty that, for instance,

115
00:07:56,960 --> 00:08:01,140
 if you as a child, you might feel like stepping on the

116
00:08:01,140 --> 00:08:04,680
 hands of playing with magnifying glasses.

117
00:08:04,680 --> 00:08:07,200
 That is also a cruelty, right?

118
00:08:07,200 --> 00:08:11,360
 Well, that's outright torture.

119
00:08:11,360 --> 00:08:15,860
 What I mean by cruelty is just the kind of anger feeling

120
00:08:15,860 --> 00:08:18,240
 that arises when you refuse

121
00:08:18,240 --> 00:08:23,860
 to help someone, kind of disregard in the sense you get

122
00:08:23,860 --> 00:08:26,720
 kind of angry at the thought

123
00:08:26,720 --> 00:08:28,720
 of helping them.

124
00:08:28,720 --> 00:08:32,400
 I see.

125
00:08:32,400 --> 00:08:37,080
 But anger is the opposite of anger is metta, right, Bantu?

126
00:08:37,080 --> 00:08:39,640
 So cruelty should be a little different?

127
00:08:39,640 --> 00:08:42,000
 No, it's still a kind of anger.

128
00:08:42,000 --> 00:08:46,880
 For example, the opposite of metta is hatred.

129
00:08:46,880 --> 00:08:49,540
 Hatred is one type of anger, so there's hatred and cruelty,

130
00:08:49,540 --> 00:08:51,120
 but they're both anger-based.

131
00:08:51,120 --> 00:08:56,350
 For example, a kid playing with hands may not necessarily

132
00:08:56,350 --> 00:08:59,080
 have anger towards the hands.

133
00:08:59,080 --> 00:09:01,960
 He just wants to have some fun.

134
00:09:01,960 --> 00:09:04,760
 Right, yeah, that's a good point.

135
00:09:04,760 --> 00:09:07,080
 You can be cruel for other reasons.

136
00:09:07,080 --> 00:09:09,760
 It could be explained that way.

137
00:09:09,760 --> 00:09:11,960
 I guess I'm not entirely clear.

138
00:09:11,960 --> 00:09:12,960
 They're a bit different.

139
00:09:12,960 --> 00:09:17,600
 Metta, if I remember correctly, is called the adosa.

140
00:09:17,600 --> 00:09:20,840
 So it is actually the exact opposite of anger.

141
00:09:20,840 --> 00:09:23,840
 Yeah, so you may be right.

142
00:09:23,840 --> 00:09:27,240
 There's a bit of a difference there.

143
00:09:27,240 --> 00:09:31,320
 But I still think actual cruelty is...

144
00:09:31,320 --> 00:09:40,440
 No, I guess that is a good point, that cruelty could be mo

145
00:09:40,440 --> 00:09:42,560
ja-based.

146
00:09:42,560 --> 00:09:43,560
 Thank you.

147
00:09:43,560 --> 00:09:46,760
 Marilyn, are you able to read 95-4s?

148
00:09:46,760 --> 00:09:56,200
 Oh, sorry, I just noticed you were muted there.

149
00:09:56,200 --> 00:10:03,800
 Penelope, are you able to read 95-4s?

150
00:10:03,800 --> 00:10:10,410
 Okay, yeah, it takes a little bit to get the microphone syn

151
00:10:10,410 --> 00:10:12,600
ced and all that.

152
00:10:12,600 --> 00:10:17,070
 95, gladness is characterized as gladdening, produced by

153
00:10:17,070 --> 00:10:18,320
 other success.

154
00:10:18,320 --> 00:10:21,280
 Its function resides in being unenvious.

155
00:10:21,280 --> 00:10:25,440
 It is manifested as the elimination of aversion, boredom.

156
00:10:25,440 --> 00:10:29,560
 Its proximate cause is seeing beings' success.

157
00:10:29,560 --> 00:10:33,990
 It succeeds when it makes aversion, boredom, subside, and

158
00:10:33,990 --> 00:10:39,680
 it fails when it produces merriment.

159
00:10:39,680 --> 00:10:42,850
 Equanimity is characterized as promoting the aspect of

160
00:10:42,850 --> 00:10:44,680
 neutrality towards beings.

161
00:10:44,680 --> 00:10:47,840
 Its function is to see equality in beings.

162
00:10:47,840 --> 00:10:55,010
 It is manifested as the quieting of resentment and approval

163
00:10:55,010 --> 00:10:55,240
.

164
00:10:55,240 --> 00:10:58,950
 Its proximate cause is seeing ownership of deeds, comma,

165
00:10:58,950 --> 00:11:01,240
 thus beings are owners of their

166
00:11:01,240 --> 00:11:05,900
 deeds, whose if not theirs is the choice by which they will

167
00:11:05,900 --> 00:11:07,840
 become happy, or will get

168
00:11:07,840 --> 00:11:12,630
 free from suffering, or will not fall away from the success

169
00:11:12,630 --> 00:11:14,360
 they have reached.

170
00:11:14,360 --> 00:11:18,730
 It occurs when it makes resentment and approval subside and

171
00:11:18,730 --> 00:11:21,000
 it fails when it produces the

172
00:11:21,000 --> 00:11:24,540
 continuity of unknowing, which is that world-minded

173
00:11:24,540 --> 00:11:31,640
 indifference of ignorance based on the house-type.

174
00:11:31,640 --> 00:11:37,590
 Purpose, the general purpose of these four divine bindings

175
00:11:37,590 --> 00:11:40,160
 is the bliss of insight and

176
00:11:40,160 --> 00:11:44,080
 excellent form of future existence.

177
00:11:44,080 --> 00:11:49,810
 That particular to each is respectively the warding of ill-

178
00:11:49,810 --> 00:11:51,480
will and so on.

179
00:11:51,480 --> 00:11:55,340
 For here loving-kindness has the purpose of warding of ill-

180
00:11:55,340 --> 00:11:57,360
will, while others have the

181
00:11:57,360 --> 00:12:03,270
 respective purposes of warding of cruelty, aversion,

182
00:12:03,270 --> 00:12:06,800
 boredom, and greed or resentment.

183
00:12:06,800 --> 00:12:13,440
 And this is said too, for this is the escape from ill-will,

184
00:12:13,440 --> 00:12:15,520
 friends, that is to say, the

185
00:12:15,520 --> 00:12:19,360
 mind deliverance of loving-kindness.

186
00:12:19,360 --> 00:12:24,470
 For this is the escape from cruelty, friends, that is to

187
00:12:24,470 --> 00:12:28,960
 say, the mind deliverance of compassion.

188
00:12:28,960 --> 00:12:33,410
 For this is the escape from boredom, friends, that is to

189
00:12:33,410 --> 00:12:36,480
 say, the mind deliverance of gladness.

190
00:12:36,480 --> 00:12:43,830
 For this is the escape from greed, friends, that is to say,

191
00:12:43,830 --> 00:12:48,720
 the mind deliverance of equanimity.

192
00:12:48,720 --> 00:12:55,880
 The near and far enemies, and here each one has two enemies

193
00:12:55,880 --> 00:12:58,880
, one near and one far.

194
00:12:58,880 --> 00:13:06,460
 The divine abiding of loving-kindness has greed as its near

195
00:13:06,460 --> 00:13:07,760
 enemy.

196
00:13:07,760 --> 00:13:16,420
 Since both share in seeing virtues, greed behaves like a

197
00:13:16,420 --> 00:13:21,360
 foe who keeps close by a man, and it

198
00:13:21,360 --> 00:13:25,240
 easily finds an opportunity.

199
00:13:25,240 --> 00:13:33,150
 So loving-kindness should be well protected from it, and

200
00:13:33,150 --> 00:13:38,080
 ill-will, which is this similar

201
00:13:38,080 --> 00:13:42,080
 to the similar greed, is its far enemy, like a foe in sc

202
00:13:42,080 --> 00:13:57,120
ones, in a rug, widen, widenness.

203
00:13:57,120 --> 00:14:04,500
 So loving-kindness must be practiced free from fear of that

204
00:14:04,500 --> 00:14:04,600
.

205
00:14:04,600 --> 00:14:18,220
 For it is not possible to practice loving-kindness and feel

206
00:14:18,220 --> 00:14:24,280
 anger simultaneously.

207
00:14:24,280 --> 00:14:31,620
 Compassion has grief based on the home life for its near

208
00:14:31,620 --> 00:14:32,840
 enemy, since both share in seeing

209
00:14:32,840 --> 00:14:35,200
 failure.

210
00:14:35,200 --> 00:14:38,490
 Such grief has been described in the way beginning, when a

211
00:14:38,490 --> 00:14:40,720
 man either regards a sub-privation,

212
00:14:40,720 --> 00:14:44,790
 failure to obtain visual objects cognizable by the eye that

213
00:14:44,790 --> 00:14:46,920
 are sought after, desired,

214
00:14:46,920 --> 00:14:50,940
 agreeable, gratifying, and associated with worldliness, or

215
00:14:50,940 --> 00:14:52,920
 when he recalls those formerly

216
00:14:52,920 --> 00:14:58,150
 obtained that are past, ceased, and changed, then grief

217
00:14:58,150 --> 00:14:59,680
 arises in him.

218
00:14:59,680 --> 00:15:03,480
 Such grief as this is called grief based on the home life,

219
00:15:03,480 --> 00:15:05,560
 and cruelty, which is dissimilar

220
00:15:05,560 --> 00:15:09,600
 to the similar grief, is its far enemy.

221
00:15:09,600 --> 00:15:12,980
 So compassion must be practiced free from fear of that, for

222
00:15:12,980 --> 00:15:14,800
 it is not possible to practice

223
00:15:14,800 --> 00:15:22,400
 compassion and be cruel to breathing things simultaneously.

224
00:15:22,400 --> 00:15:26,220
 Gladness has joy based on home life as its near enemy,

225
00:15:26,220 --> 00:15:29,060
 since both share in seeing success.

226
00:15:29,060 --> 00:15:32,160
 Such joy has been described in the way beginning, when a

227
00:15:32,160 --> 00:15:34,680
 man either regards his gain, the obtaining

228
00:15:34,680 --> 00:15:38,980
 of visible objects cognizable by the eye that are sought,

229
00:15:38,980 --> 00:15:41,440
 and associated with worldliness,

230
00:15:41,440 --> 00:15:46,160
 or recalls those formerly obtained that are past, ceased,

231
00:15:46,160 --> 00:15:48,520
 and changed, then joy arises

232
00:15:48,520 --> 00:15:50,160
 in him.

233
00:15:50,160 --> 00:15:55,150
 Such joy as this is called joy based on the home life, and

234
00:15:55,150 --> 00:15:57,200
 aversion, boredom, which is

235
00:15:57,200 --> 00:16:01,840
 dissimilar to the similar joy as its far enemy.

236
00:16:01,840 --> 00:16:04,970
 So gladness should be practiced free from fear of that, for

237
00:16:04,970 --> 00:16:06,580
 it is not possible to practice

238
00:16:06,580 --> 00:16:11,160
 gladness and be discontented with remote abodes and things

239
00:16:11,160 --> 00:16:14,000
 connected with a higher profitable

240
00:16:14,000 --> 00:16:37,640
ness simultaneously.

241
00:16:37,640 --> 00:16:44,270
 Meaning on seeing a visible object with the eye, equanimity

242
00:16:44,270 --> 00:16:50,320
 arises in the foolish, infatuated,

243
00:16:50,320 --> 00:16:53,040
 ordinary man.

244
00:16:53,040 --> 00:16:57,760
 In the untaught, ordinary man who has not conquered his

245
00:16:57,760 --> 00:17:00,800
 limitation, who has not conquered

246
00:17:00,800 --> 00:17:09,340
 future, gamma, result, who is unperceiving of danger, such

247
00:17:09,340 --> 00:17:13,760
 equanimity, as this does not

248
00:17:13,760 --> 00:17:19,730
 surmount the visible object, such equanimity, as this is

249
00:17:19,730 --> 00:17:22,560
 called equanimity based on the

250
00:17:22,560 --> 00:17:30,460
 home life, as greed and sentiment, which are dissimilar to

251
00:17:30,460 --> 00:17:34,640
 the similar unknowing and its

252
00:17:34,640 --> 00:17:38,280
 far enemies.

253
00:17:38,280 --> 00:17:44,170
 Therefore equanimity must be practiced free from fear of

254
00:17:44,170 --> 00:17:47,120
 that, for it is not possible

255
00:17:47,120 --> 00:17:58,710
 to look on with equanimity and be inflamed with greed or be

256
00:17:58,710 --> 00:18:03,080
 resentful simultaneously.

257
00:18:03,080 --> 00:18:13,480
 Now zeal, consisting in desire to act, is the beginning of

258
00:18:13,480 --> 00:18:15,600
 all these things.

259
00:18:15,600 --> 00:18:19,400
 One of the hindrances, etc., is the middle.

260
00:18:19,400 --> 00:18:21,360
 Absorption is the end.

261
00:18:21,360 --> 00:18:24,930
 Their object is a single living being or many living beings

262
00:18:24,930 --> 00:18:27,040
, as a mental object consisting

263
00:18:27,040 --> 00:18:30,640
 in a concept.

264
00:18:30,640 --> 00:18:32,680
 The order in extension.

265
00:18:32,680 --> 00:18:37,090
 The extension of the object takes place either in excess or

266
00:18:37,090 --> 00:18:38,520
 in absorption.

267
00:18:38,520 --> 00:18:40,520
 Here is the order of it.

268
00:18:40,520 --> 00:18:45,220
 Just as a skilled plowman first delimits an area and then

269
00:18:45,220 --> 00:18:47,640
 does his plowing, so first a

270
00:18:47,640 --> 00:18:51,290
 single dwelling should be delimited and loving kindness

271
00:18:51,290 --> 00:18:53,560
 develops towards all beings there

272
00:18:53,560 --> 00:18:58,000
 in the way beginning, in this dwelling may all beings be

273
00:18:58,000 --> 00:18:59,680
 free from enmity.

274
00:18:59,680 --> 00:19:03,920
 When his mind has become malleable and wieldy with respect

275
00:19:03,920 --> 00:19:06,020
 to that, he can then delimit

276
00:19:06,020 --> 00:19:08,120
 two dwellings.

277
00:19:08,120 --> 00:19:12,540
 Next he can successively delimit three, four, five, six,

278
00:19:12,540 --> 00:19:15,600
 seven, eight, nine, ten, one street,

279
00:19:15,600 --> 00:19:20,050
 half the village, the whole village, the district, the

280
00:19:20,050 --> 00:19:23,080
 kingdom, one direction and so on up to

281
00:19:23,080 --> 00:19:27,220
 one world's wear or even beyond that and develop loving

282
00:19:27,220 --> 00:19:29,800
 kindness towards the beings in such

283
00:19:29,800 --> 00:19:31,640
 areas.

284
00:19:31,640 --> 00:19:34,680
 Likewise with compassion and so on.

285
00:19:34,680 --> 00:19:36,560
 This is the order in extending here.

286
00:19:36,560 --> 00:19:47,010
 The outcome, just as the immaterial states are the outcome

287
00:19:47,010 --> 00:19:48,760
 of the casinos and the base

288
00:19:48,760 --> 00:19:52,530
 consisting of neither perception nor non-perception is the

289
00:19:52,530 --> 00:19:55,280
 outcome of all concentration and fruition

290
00:19:55,280 --> 00:19:58,370
 attainment is the outcome of insight and the attainment of

291
00:19:58,370 --> 00:19:59,960
 succession is the outcome of

292
00:19:59,960 --> 00:20:02,460
 serenity coupled with insight.

293
00:20:02,460 --> 00:20:06,240
 So the divinability is the outcome of the first three

294
00:20:06,240 --> 00:20:07,760
 divine abidings.

295
00:20:07,760 --> 00:20:10,120
 For just as the gable rafters cannot be placed in the air

296
00:20:10,120 --> 00:20:11,440
 without having first set up the

297
00:20:11,440 --> 00:20:15,220
 scaffolding and built the framework of beams, so it is not

298
00:20:15,220 --> 00:20:17,360
 possible to develop the fourth

299
00:20:17,360 --> 00:20:20,790
 Jhana in the fourth divine abiding without having already

300
00:20:20,790 --> 00:20:22,560
 developed the third Jhana in

301
00:20:22,560 --> 00:20:27,800
 the earlier three divine abidings.

302
00:20:27,800 --> 00:20:31,180
 And here it may be asked by why, but why are loving

303
00:20:31,180 --> 00:20:34,000
 kindness, compassion, gladness and

304
00:20:34,000 --> 00:20:39,030
 equanimity called divine abidings and why are they only

305
00:20:39,030 --> 00:20:41,400
 four and what is their order and

306
00:20:41,400 --> 00:20:55,280
 why are they called special states in the abidama?

307
00:20:55,280 --> 00:21:00,780
 It may be replied, the divineness of the abiding Brahma Vih

308
00:21:00,780 --> 00:21:03,880
a Ratha should be understood here

309
00:21:03,880 --> 00:21:08,090
 in the sense of west and in the sense of immaculate for

310
00:21:08,090 --> 00:21:10,520
 these abidings are the best in being the

311
00:21:10,520 --> 00:21:13,400
 right attitude towards beings.

312
00:21:13,400 --> 00:21:17,750
 And just as Brahma gods abide with immaculate minds, so the

313
00:21:17,750 --> 00:21:20,640
 meditators who associate themselves

314
00:21:20,640 --> 00:21:25,810
 with these abidings abide on an equal footing with Brahma

315
00:21:25,810 --> 00:21:26,960
 gods.

316
00:21:26,960 --> 00:21:34,160
 So they are called divine abidings in the sense of best and

317
00:21:34,160 --> 00:21:37,560
 in the sense of immaculate.

318
00:21:37,560 --> 00:21:42,750
 Here are the answers to the questions beginning with why

319
00:21:42,750 --> 00:21:44,800
 are they only four?

320
00:21:44,800 --> 00:21:51,240
 Their number four is due to path to purity and other sets

321
00:21:51,240 --> 00:21:54,760
 of four, their order to their

322
00:21:54,760 --> 00:21:56,560
 aim.

323
00:21:56,560 --> 00:22:03,460
 As welfare and the rest, their scope is found to be immeas

324
00:22:03,460 --> 00:22:07,680
urable, so measureless states their

325
00:22:07,680 --> 00:22:14,440
 name.

326
00:22:14,440 --> 00:22:17,950
 For among these, loving kindness is the way to purity for

327
00:22:17,950 --> 00:22:20,380
 one who has much ill will, compassion

328
00:22:20,380 --> 00:22:24,770
 is that for one who has much cruelty, gladness is that for

329
00:22:24,770 --> 00:22:27,760
 one who has much aversion or boredom,

330
00:22:27,760 --> 00:22:32,000
 and equanimity is that for one who has much greed.

331
00:22:32,000 --> 00:22:36,120
 Also attention given to beings is only fourfold, that is to

332
00:22:36,120 --> 00:22:38,920
 say as bringing welfare, as removing

333
00:22:38,920 --> 00:22:42,860
 suffering, as being glad at their success, and as uncon

334
00:22:42,860 --> 00:22:45,280
cerned, that is to say impartial

335
00:22:45,280 --> 00:22:47,440
 neutrality.

336
00:22:47,440 --> 00:22:50,040
 And one abiding in the measureless states should practice

337
00:22:50,040 --> 00:22:51,400
 loving kindness and the rest

338
00:22:51,400 --> 00:22:56,820
 like a mother with four sons, namely a child, an invalid,

339
00:22:56,820 --> 00:22:59,160
 one in the flush of youth, and

340
00:22:59,160 --> 00:23:01,480
 one busy with his own affairs.

341
00:23:01,480 --> 00:23:05,030
 For she wants the child to grow up, wants the invalid to

342
00:23:05,030 --> 00:23:06,840
 get well, wants the one in

343
00:23:06,840 --> 00:23:10,620
 the flush of youth to enjoy for long the benefits of youth,

344
00:23:10,620 --> 00:23:12,680
 and is not at all bothered about

345
00:23:12,680 --> 00:23:15,440
 the one who is busy with his own affairs.

346
00:23:15,440 --> 00:23:19,240
 That is why the measureless states are only four, as due to

347
00:23:19,240 --> 00:23:20,880
 paths to purity and other

348
00:23:20,880 --> 00:23:21,880
 sets of four.

349
00:23:21,880 --> 00:23:24,480
 That's a really good passage, something to quote.

350
00:23:24,480 --> 00:23:29,310
 I had a question on the third one, they, you know, they Kar

351
00:23:29,310 --> 00:23:31,800
una, they're referring to it

352
00:23:31,800 --> 00:23:35,240
 strictly as aversion and boredom.

353
00:23:35,240 --> 00:23:41,610
 I've seen it described as, you know, combating jealousy as

354
00:23:41,610 --> 00:23:43,440
 well and envy.

355
00:23:43,440 --> 00:23:46,840
 Does that come into this as well?

356
00:23:46,840 --> 00:23:49,080
 You can, Karuna is number two.

357
00:23:49,080 --> 00:23:50,240
 Which one are you talking about?

358
00:23:50,240 --> 00:23:55,120
 Oh, I'm sorry, I meant, Mudita, number three.

359
00:23:55,120 --> 00:24:02,320
 Yeah, it's interesting that aversion and boredom are the

360
00:24:02,320 --> 00:24:05,040
 two translations.

361
00:24:05,040 --> 00:24:10,960
 I can take a look and see what the actual Pali is.

362
00:24:10,960 --> 00:24:11,960
 Thank you.

363
00:24:11,960 --> 00:24:20,950
 I bet it's badika, which would mean sort of not aversion,

364
00:24:20,950 --> 00:24:22,240
 but being against someone, how

365
00:24:22,240 --> 00:24:24,080
 do you say that?

366
00:24:24,080 --> 00:24:27,370
 I mean, I'm not saying that I'm not saying that I'm not

367
00:24:27,370 --> 00:24:31,080
 saying that I'm not saying that

368
00:24:31,080 --> 00:24:34,560
 I'm not saying that I'm not saying that I'm not saying that

369
00:24:34,560 --> 00:24:36,080
 I'm not saying that I'm not

370
00:24:36,080 --> 00:24:39,270
 saying that I'm not saying that I'm not saying that I'm

371
00:24:39,270 --> 00:24:44,080
 saying that I'm saying that I'm saying

372
00:24:44,080 --> 00:24:47,830
 that I'm saying that I'm saying that I'm saying that I'm

373
00:24:47,830 --> 00:24:51,080
 saying that I'm saying that I'm saying

374
00:24:51,080 --> 00:24:55,110
 that I'm saying that I'm saying that I'm saying that I'm

375
00:24:55,110 --> 00:24:58,080
 saying that I'm saying that I'm saying

376
00:24:58,080 --> 00:25:02,110
 that I'm saying that I'm saying that I'm saying that I'm

377
00:25:02,110 --> 00:25:05,080
 saying that I'm saying that I'm saying

378
00:25:05,080 --> 00:25:09,740
 that I'm saying that I'm saying that I'm saying that I'm

379
00:25:09,740 --> 00:25:13,080
 saying that I'm saying that I'm saying

380
00:25:13,080 --> 00:25:17,110
 that I'm saying that I'm saying that I'm saying that I'm

381
00:25:17,110 --> 00:25:20,080
 saying that I'm saying that I'm saying

382
00:25:20,080 --> 00:25:24,110
 that I'm saying that I'm saying that I'm saying that I'm

383
00:25:24,110 --> 00:25:27,080
 saying that I'm saying that I'm saying

384
00:25:27,080 --> 00:25:31,120
 that I'm saying that I'm saying that I'm saying that I'm

385
00:25:31,120 --> 00:25:34,080
 saying that I'm saying that I'm saying

386
00:25:34,080 --> 00:25:36,940
 that I'm saying that I'm saying that I'm saying that I'm

387
00:25:36,940 --> 00:25:39,080
 saying that I'm saying that I'm saying

388
00:25:39,080 --> 00:25:42,540
 that I'm saying that I'm saying that I'm saying that I'm

389
00:25:42,540 --> 00:25:45,080
 saying that I'm saying that I'm saying

390
00:25:45,080 --> 00:25:48,540
 that I'm saying that I'm saying that I'm saying that I'm

391
00:25:48,540 --> 00:25:51,080
 saying that I'm saying that I'm saying that I'm

392
00:25:51,080 --> 00:25:55,120
 saying that I'm saying that I'm saying that I'm saying that

393
00:25:55,120 --> 00:25:57,080
 I'm saying that I'm saying that I'm saying that I'm

394
00:25:57,080 --> 00:26:01,120
 saying that I'm saying that I'm saying that I'm saying that

395
00:26:01,120 --> 00:26:03,080
 I'm saying that I'm saying that I'm saying that I'm

396
00:26:03,080 --> 00:26:06,930
 saying that I'm saying that I'm saying that I'm saying that

397
00:26:06,930 --> 00:26:09,080
 I'm saying that I'm saying that I'm saying that I'm

398
00:26:09,080 --> 00:26:12,780
 saying that I'm saying that I'm saying that I'm saying that

399
00:26:12,780 --> 00:26:15,080
 I'm saying that I'm saying that I'm saying that I'm

400
00:26:15,080 --> 00:26:18,510
 saying that I'm saying that I'm saying that I'm saying that

401
00:26:18,510 --> 00:26:21,080
 I'm saying that I'm saying that I'm saying that I'm

402
00:26:21,080 --> 00:26:23,270
 saying that I'm saying that I'm saying that I'm saying that

403
00:26:23,270 --> 00:26:27,080
 I'm saying that I'm saying that I'm saying that I'm

404
00:26:27,080 --> 00:26:28,900
 saying that I'm saying that I'm saying that I'm saying that

405
00:26:28,900 --> 00:26:32,080
 I'm saying that I'm saying that I'm saying that I'm saying

406
00:26:32,080 --> 00:26:32,080
 that I'm

407
00:26:32,080 --> 00:26:34,310
 saying that I'm saying that I'm saying that I'm saying that

408
00:26:34,310 --> 00:26:36,080
 I'm saying that I'm saying that I'm saying that I'm saying

409
00:26:36,080 --> 00:26:36,080
 that I'm

410
00:26:36,080 --> 00:26:38,290
 saying that I'm saying that I'm saying that I'm saying that

411
00:26:38,290 --> 00:26:41,080
 I'm saying that I'm saying that I'm saying that I'm saying

412
00:26:41,080 --> 00:26:41,080
 that I'm

413
00:26:41,080 --> 00:26:43,840
 saying that I'm saying that I'm saying that I'm saying that

414
00:26:43,840 --> 00:26:45,080
 I'm saying that I'm saying that I'm saying that I'm saying

415
00:26:45,080 --> 00:26:45,080
 that I'm

416
00:26:45,080 --> 00:26:49,350
 saying that I'm saying that I'm saying that I'm saying that

417
00:26:49,350 --> 00:26:50,080
 I'm saying that I'm saying that I'm saying that I'm saying

418
00:26:50,080 --> 00:26:50,080
 that I'm

419
00:26:50,080 --> 00:26:53,380
 saying that I'm saying that I'm saying that I'm saying that

420
00:26:53,380 --> 00:26:54,080
 I'm saying that I'm saying that I'm saying that I'm saying

421
00:26:54,080 --> 00:26:54,080
 that I'm saying that I'm saying that I'm

422
00:26:59,080 --> 00:27:02,760
 saying that I'm saying that I'm saying that I'm saying that

423
00:27:02,760 --> 00:27:06,080
 I'm saying that I'm saying that I'm saying that I'm saying

424
00:27:06,080 --> 00:27:06,080
 that I'm saying that I'm

425
00:27:06,080 --> 00:27:09,780
 saying that I'm saying that I'm saying that I'm saying that

426
00:27:09,780 --> 00:27:12,070
 I'm saying that I'm saying that I'm saying that I'm saying

427
00:27:12,070 --> 00:27:12,080
 that I'm saying that I'm

428
00:27:12,080 --> 00:27:15,080
 saying that I'm saying that I'm saying that I'm saying that

429
00:27:15,080 --> 00:27:18,260
 I'm saying that I'm saying that I'm saying that I'm saying

430
00:27:18,260 --> 00:27:19,080
 that I'm saying that I'm saying that I'm

431
00:27:50,080 --> 00:27:53,320
 saying that I'm saying that I'm saying that I'm saying that

432
00:27:53,320 --> 00:27:56,430
 I'm saying that I'm saying that I'm saying that I'm saying

433
00:27:56,430 --> 00:27:57,080
 that I'm saying that I'm

434
00:27:57,080 --> 00:27:59,350
 saying that I'm saying that I'm saying that I'm saying that

435
00:27:59,350 --> 00:28:01,560
 I'm saying that I'm saying that I'm saying that I'm saying

436
00:28:01,560 --> 00:28:04,080
 that I'm saying that I'm

437
00:28:04,080 --> 00:28:09,290
 saying that I'm saying that I'm saying that I'm saying that

438
00:28:09,290 --> 00:28:11,080
 I'm saying that I'm saying that I'm saying that I'm saying

439
00:28:11,080 --> 00:28:11,080
 that I'm saying that I'm saying that I'm saying that I'm

440
00:28:11,080 --> 00:28:11,080
 saying that I'm saying that I'm

441
00:33:36,080 --> 00:33:38,910
 saying that I'm saying that I'm saying that I'm saying that

442
00:33:38,910 --> 00:33:41,680
 I'm saying that I'm saying that I'm saying that I'm saying

443
00:33:41,680 --> 00:33:43,080
 that I'm saying that I'm saying that I'm saying that I'm

444
00:33:43,080 --> 00:33:43,080
 saying that I'm saying that I'm

445
00:33:43,080 --> 00:33:45,840
 saying that I'm saying that I'm saying that I'm saying that

446
00:33:45,840 --> 00:33:48,530
 I'm saying that I'm saying that I'm saying that I'm saying

447
00:33:48,530 --> 00:33:52,820
 that I'm saying that I'm saying that I'm saying that I'm

448
00:33:52,820 --> 00:33:57,080
 saying that I'm saying that I'm saying that I'm

449
00:37:06,080 --> 00:37:08,910
 saying that I'm saying that I'm saying that I'm saying that

450
00:37:08,910 --> 00:37:11,680
 I'm saying that I'm saying that I'm saying that I'm saying

451
00:37:11,680 --> 00:37:17,930
 that I'm saying that I'm saying that I'm saying that I'm

452
00:37:17,930 --> 00:37:19,080
 saying that I'm saying that I'm

453
00:37:19,080 --> 00:37:22,330
 saying that I'm saying that I'm saying that I'm saying that

454
00:37:22,330 --> 00:37:25,660
 I'm saying that I'm saying that I'm saying that I'm saying

455
00:37:25,660 --> 00:37:31,900
 that I'm saying that I'm saying that I'm saying that I'm

456
00:37:31,900 --> 00:37:32,080
 saying that I'm saying that I'm saying that I'm saying that

457
00:37:32,080 --> 00:37:32,080
 I'm

458
00:52:12,080 --> 00:52:16,060
 saying that I'm not quite sure what is meant there because

459
00:52:16,060 --> 00:52:21,080
 in the Arupa Janas they have different specific objects.

460
00:52:21,080 --> 00:52:27,490
 But there is a sense that you're using the original object

461
00:52:27,490 --> 00:52:33,080
 as a base I don't know. It's not quite clear to me.

462
00:52:33,080 --> 00:52:41,080
 Thank you, Bante.

463
00:52:41,080 --> 00:52:44,690
 Okay, thanks everyone. See you again next week. We'll get

464
00:52:44,690 --> 00:52:46,080
 on to the next chapter.

465
00:52:46,080 --> 00:52:56,080
 Bye, everyone.

466
00:52:57,080 --> 00:53:03,080
 Thank you.

