1
00:00:00,000 --> 00:00:04,440
 Hello, if one is committed to the Theravada tradition but

2
00:00:04,440 --> 00:00:05,840
 only has the possibility to

3
00:00:05,840 --> 00:00:10,640
 join Mahayana-vadrayana meditation centers in the area,

4
00:00:10,640 --> 00:00:12,240
 should he or she continue practicing

5
00:00:12,240 --> 00:00:18,840
 along the Theravada lines of practice or join the center

6
00:00:18,840 --> 00:00:21,040
 nevertheless?

7
00:00:21,040 --> 00:00:22,840
 I don't think that it's an either/or.

8
00:00:22,840 --> 00:00:29,170
 Of course, you can continue practicing Theravada and still

9
00:00:29,170 --> 00:00:32,280
 attend a Buddhist center in a different

10
00:00:32,280 --> 00:00:34,800
 tradition if it's in your area.

11
00:00:34,800 --> 00:00:38,470
 I know this question comes up a lot because Theravada

12
00:00:38,470 --> 00:00:40,920
 Buddhist centers aren't as prevalent

13
00:00:40,920 --> 00:00:46,810
 as those from Tibet or Zen or however, but you'd be

14
00:00:46,810 --> 00:00:50,880
 surprised how often you find a sympathetic

15
00:00:50,880 --> 00:00:53,280
 ear at some of these places.

16
00:00:53,280 --> 00:00:57,660
 First of all, you might find a teacher who is versed in the

17
00:00:57,660 --> 00:01:00,040
 practice that you've done.

18
00:01:00,040 --> 00:01:06,470
 Aaron, in fact, and I were attending a meditation group in

19
00:01:06,470 --> 00:01:10,680
 the Tibetan tradition, but the head

20
00:01:10,680 --> 00:01:14,320
 Lama was teaching from the Visuddhi manga.

21
00:01:14,320 --> 00:01:18,120
 Now, that may be a little bit rare, but he was quite am

22
00:01:18,120 --> 00:01:20,780
enable to our practice and actually

23
00:01:20,780 --> 00:01:24,380
 came and did a meditation, the head Lama came and did a

24
00:01:24,380 --> 00:01:26,680
 meditation course with me and was

25
00:01:26,680 --> 00:01:31,170
 looking to learn to teach the meditation practice in this

26
00:01:31,170 --> 00:01:32,360
 tradition.

27
00:01:32,360 --> 00:01:38,340
 But even that not being the case, I think it's quite

28
00:01:38,340 --> 00:01:42,800
 possible for you to attend these centers

29
00:01:42,800 --> 00:01:48,320
 or at least ask permission to use these centers.

30
00:01:48,320 --> 00:01:50,240
 I know some places it depends on the place.

31
00:01:50,240 --> 00:01:55,050
 Some can probably be quite xenophobic or controlling and

32
00:01:55,050 --> 00:01:58,680
 will maybe even forbid you from practicing

33
00:01:58,680 --> 00:02:00,640
 other meditations in their center.

34
00:02:00,640 --> 00:02:04,770
 But if you're just sitting quietly alone, I know at least

35
00:02:04,770 --> 00:02:07,120
 in the Theravada tradition,

36
00:02:07,120 --> 00:02:10,660
 people are generally quite amenable to have you use their

37
00:02:10,660 --> 00:02:12,520
 center as a place to practice

38
00:02:12,520 --> 00:02:15,080
 your own teachings.

39
00:02:15,080 --> 00:02:20,440
 So you can go along with it and join in on the group

40
00:02:20,440 --> 00:02:23,880
 chanting or whatever and keep up

41
00:02:23,880 --> 00:02:27,680
 your own practice.

42
00:02:27,680 --> 00:02:30,780
 I mean, I guess it's obviously not a replacement for a

43
00:02:30,780 --> 00:02:32,880
 teacher in the tradition that you're

44
00:02:32,880 --> 00:02:37,360
 following, but there's certainly no harm in it.

45
00:02:37,360 --> 00:02:41,070
 And often you'll meet people of like mind and you find that

46
00:02:41,070 --> 00:02:42,720
 other people who attend

47
00:02:42,720 --> 00:02:47,660
 these centers may have experience in the practice that you

48
00:02:47,660 --> 00:02:50,480
 do and are in a similar situation

49
00:02:50,480 --> 00:02:53,150
 where they're just looking for a center, but they actually

50
00:02:53,150 --> 00:02:54,480
 practice something else.

51
00:02:54,480 --> 00:03:02,340
 And so you'll often find a sympathetic ear and community,

52
00:03:02,340 --> 00:03:05,480
 which is important.

53
00:03:05,480 --> 00:03:06,840
 You'll find something there.

54
00:03:06,840 --> 00:03:09,980
 So definitely I recommend people go to these centers, check

55
00:03:09,980 --> 00:03:11,480
 them out, and you'll find that

56
00:03:11,480 --> 00:03:12,880
 they vary in quality.

57
00:03:12,880 --> 00:03:16,100
 Some of them might be quite authentic and some might be

58
00:03:16,100 --> 00:03:18,120
 simply commercial ventures and

59
00:03:18,120 --> 00:03:19,120
 so on.

60
00:03:19,120 --> 00:03:22,360
 So check it out.

61
00:03:22,360 --> 00:03:24,800
 And for sure, I would recommend that sort of thing.

