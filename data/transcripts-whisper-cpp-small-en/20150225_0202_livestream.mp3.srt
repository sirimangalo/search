1
00:00:00,000 --> 00:00:13,140
 Okay, back broadcasting this evening from Stony Creek,

2
00:00:13,140 --> 00:00:16,000
 Ontario, Canada.

3
00:00:16,000 --> 00:00:41,000
 Back in the cold, great white birth.

4
00:00:41,000 --> 00:00:50,000
 Different here, many things different in the West.

5
00:00:50,000 --> 00:00:57,170
 We have to... sometimes it feels like fitting, trying to

6
00:00:57,170 --> 00:01:07,000
 fit round pegs in square holes, square pegs in round holes.

7
00:01:07,000 --> 00:01:14,700
 With the monastic life, with the meditation practice, with

8
00:01:14,700 --> 00:01:18,000
 the philosophy of the Buddha.

9
00:01:18,000 --> 00:01:30,290
 Of course, constantly much easier in the dedicated Buddhist

10
00:01:30,290 --> 00:01:32,000
 culture.

11
00:01:32,000 --> 00:01:41,920
 But the key, I think, is to pare the Buddha's teaching down

12
00:01:41,920 --> 00:01:49,000
 to the essentials and then rebuild it from there.

13
00:01:49,000 --> 00:01:57,170
 Sorry, not remove anything from the Buddha's teaching, so

14
00:01:57,170 --> 00:02:04,000
 not pare it down, but to begin with the essentials.

15
00:02:04,000 --> 00:02:10,020
 As long as you're not altering them, the rest to some

16
00:02:10,020 --> 00:02:13,000
 extent can be adapted.

17
00:02:13,000 --> 00:02:25,000
 Or it's not important, not as essential or concerned.

18
00:02:25,000 --> 00:02:30,270
 So when you look at the Buddha's teaching in its textual

19
00:02:30,270 --> 00:02:35,630
 form, it seems like it's quite stuck in a very specific

20
00:02:35,630 --> 00:02:36,000
 society.

21
00:02:36,000 --> 00:02:39,230
 And of course, many of the Buddha's teachings were given in

22
00:02:39,230 --> 00:02:40,000
 context.

23
00:02:40,000 --> 00:02:44,690
 So if you try to apply everything all at once and try to

24
00:02:44,690 --> 00:02:48,000
 just import Buddhism into your life,

25
00:02:48,000 --> 00:02:54,000
 you end up in a very weird situation, or awkward situation,

26
00:02:54,000 --> 00:03:01,000
 where much of it doesn't immediately fit with your life.

27
00:03:01,000 --> 00:03:05,950
 If on the other hand you figure out what is the essence,

28
00:03:05,950 --> 00:03:11,000
 then you can start there and can slowly apply the rest,

29
00:03:11,000 --> 00:03:16,000
 and you add on as you go the rest of the teachings.

30
00:03:16,000 --> 00:03:22,370
 So to do that, of course, is necessary that we find the

31
00:03:22,370 --> 00:03:26,000
 essence of the Buddha's teaching.

32
00:03:26,000 --> 00:03:32,660
 The Theravada that is fairly simple, and by Theravada, we

33
00:03:32,660 --> 00:03:40,240
 mean the commentarial tradition that grew up surrounding

34
00:03:40,240 --> 00:03:42,000
 the Pali Canon,

35
00:03:42,000 --> 00:03:49,000
 what we call the Tipitaka.

36
00:03:49,000 --> 00:03:51,710
 It's simple because the commentaries are quite clear about

37
00:03:51,710 --> 00:03:54,000
 what is the essence of the Buddha's teaching.

38
00:03:54,000 --> 00:03:59,740
 The Buddha himself was quite clear, but the commentaries

39
00:03:59,740 --> 00:04:07,510
 sort of spell it out for us in one term, pointing out what

40
00:04:07,510 --> 00:04:19,000
 the Buddha emphasized or showed to be the core.

41
00:04:19,000 --> 00:04:25,000
 So we have this quote in the commentary somewhere that says

42
00:04:25,000 --> 00:04:25,000
,

43
00:04:25,000 --> 00:04:33,000
 "Sakalampi hi tehipitakam bhutavatsanam oharitva."

44
00:04:33,000 --> 00:04:38,490
 When you pare down the Buddha's teaching in the three pitik

45
00:04:38,490 --> 00:04:42,000
as, so tehipitakam,

46
00:04:42,000 --> 00:04:47,390
 the words of the Buddha are found in the three pitikas, the

47
00:04:47,390 --> 00:04:53,000
 three baskets, or the three sections.

48
00:04:53,000 --> 00:05:04,600
 When you break it down, "kati amanang apamada padami wo o

49
00:05:04,600 --> 00:05:08,000
haritva."

50
00:05:08,000 --> 00:05:16,000
 It boils down to the path of "apamada."

51
00:05:16,000 --> 00:05:22,900
 This is how the commentaries phrase it, and they must be

52
00:05:22,900 --> 00:05:31,000
 thinking specifically of the Buddha's last words.

53
00:05:31,000 --> 00:05:40,000
 The Buddha said, "apamadhi na sampadhi."

54
00:05:40,000 --> 00:06:01,000
 Which is an injunction, "eta tat." "eta" at the end means

55
00:06:01,000 --> 00:06:01,000
 it's an order, a command, or an injunction.

56
00:06:01,000 --> 00:06:18,000
 "Apamada" means come to fulfillment or reach the goal.

57
00:06:18,000 --> 00:06:26,780
 How do you reach the goal? How do you come to fulfillment?

58
00:06:26,780 --> 00:06:31,000
 "apamadhi na sampamada."

59
00:06:31,000 --> 00:06:35,170
 It's a difficult word to translate, not to understand, but

60
00:06:35,170 --> 00:06:36,000
 to translate.

61
00:06:36,000 --> 00:06:47,000
 You don't really have a good equivalent in English.

62
00:06:47,000 --> 00:06:51,870
 Nothing that's quite satisfying, because it's a negative "

63
00:06:51,870 --> 00:06:53,000
apamada."

64
00:06:53,000 --> 00:06:56,000
 "A" means it's a negative.

65
00:06:56,000 --> 00:07:07,000
 "Pamada" means negligence or heatlessness.

66
00:07:07,000 --> 00:07:19,410
 Maybe the opposite would be vigilance or non-negligence, if

67
00:07:19,410 --> 00:07:25,000
 you want to stay literal.

68
00:07:25,000 --> 00:07:34,050
 So the meaning is to not get lost, to not be lazy or heat

69
00:07:34,050 --> 00:07:44,000
less and let our minds wander from reality or from truth.

70
00:07:44,000 --> 00:07:55,280
 "Pamada" means to keep our minds focused on reality, on the

71
00:07:55,280 --> 00:07:59,000
 present moment.

72
00:07:59,000 --> 00:08:01,420
 If you want to understand the word and its meaning, you can

73
00:08:01,420 --> 00:08:04,000
 look in the context of the five precepts.

74
00:08:04,000 --> 00:08:14,670
 The Buddha says about the fifth precept. He gives an

75
00:08:14,670 --> 00:08:19,560
 injunction that we should encourage us to take the rule not

76
00:08:19,560 --> 00:08:25,000
 to take alcohol or strong drink,

77
00:08:25,000 --> 00:08:33,320
 any kind of spirits or liquor or any kind of intoxicating

78
00:08:33,320 --> 00:08:35,000
 beverages.

79
00:08:35,000 --> 00:08:44,810
 The word for intoxicating is "pamada tana," that are a base

80
00:08:44,810 --> 00:08:50,000
 or a cause for "pamada."

81
00:08:50,000 --> 00:08:57,200
 So what alcohol does to you is a kind of "pamada." It makes

82
00:08:57,200 --> 00:09:11,280
 you negligent, it makes you less alert, it makes you less

83
00:09:11,280 --> 00:09:14,000
 mindful.

84
00:09:14,000 --> 00:09:18,510
 That's really the key of "pamada" as the Buddha said in

85
00:09:18,510 --> 00:09:20,000
 mindfulness.

86
00:09:20,000 --> 00:09:31,300
 In the brief, he said, "Satiya-vipalas avipalaso apamado-d

87
00:09:31,300 --> 00:09:33,000
ivujti,"

88
00:09:33,000 --> 00:09:40,710
 when one is never without mindfulness, never lacking in

89
00:09:40,710 --> 00:09:45,000
 mindfulness. This is what is meant by "pamada."

90
00:09:45,000 --> 00:09:48,790
 So what does it mean to not be negligent? It means to be

91
00:09:48,790 --> 00:09:52,000
 always mindful, or to always have "satiya,"

92
00:09:52,000 --> 00:10:01,470
 and mindfulness isn't, again, exactly a perfect translation

93
00:10:01,470 --> 00:10:06,000
. "Satiya-vipalas avipamado-divujti."

94
00:10:06,000 --> 00:10:10,060
 We try to have mindfulness all the time. Whenever you have

95
00:10:10,060 --> 00:10:14,000
 mindfulness, you're considered to be a "pamada."

96
00:10:14,000 --> 00:10:17,670
 This is all just speech. If you want to really understand

97
00:10:17,670 --> 00:10:20,570
 the "pamada," you don't have to understand it

98
00:10:20,570 --> 00:10:22,000
 intellectually.

99
00:10:22,000 --> 00:10:26,920
 If you try practicing "sati," you can see what is with

100
00:10:26,920 --> 00:10:28,000
 meaning.

101
00:10:28,000 --> 00:10:31,150
 When you practice "sati," you can see that you have some

102
00:10:31,150 --> 00:10:33,000
 different quality of mind.

103
00:10:33,000 --> 00:10:43,220
 There's an alert quality, clarity of mind, an objectivity,

104
00:10:43,220 --> 00:10:47,000
 a freshness to the mind.

105
00:10:47,000 --> 00:10:54,000
 There's missing from our ordinary, unmindful state.

106
00:10:54,000 --> 00:10:59,180
 It's this state that the Buddha is referring to. This is

107
00:10:59,180 --> 00:11:03,000
 really it. You can cultivate that state

108
00:11:03,000 --> 00:11:12,000
 and develop the qualities of mind that support that state.

109
00:11:12,000 --> 00:11:15,000
 You can keep with that.

110
00:11:15,000 --> 00:11:23,700
 This path is the essence of Buddhism. There's one more

111
00:11:23,700 --> 00:11:26,000
 teaching that's interesting.

112
00:11:26,000 --> 00:11:31,000
 The Buddha explains "apamada" in detail.

113
00:11:31,000 --> 00:11:39,090
 He explains how to practice "apamada," what it means to be

114
00:11:39,090 --> 00:11:54,000
 a "pamada," to be vigilant, to be not negligent.

115
00:11:54,000 --> 00:12:05,000
 He gives four qualities that lead to be not, to be "apamada

116
00:12:05,000 --> 00:12:06,000
."

117
00:12:06,000 --> 00:12:24,880
 They're kind of interesting. They're quite simple. "Akoedan

118
00:12:24,880 --> 00:12:25,000
" "ambyapano," to have no ill will or anger.

119
00:12:25,000 --> 00:12:34,690
 "Ambiapano sadasato," to be always mindful. "Ajatang susum

120
00:12:34,690 --> 00:12:39,000
ahito," to be internally well composed.

121
00:12:39,000 --> 00:12:44,900
 "Ajatang" is internally "su-sumahito." "Sumahito" is

122
00:12:44,900 --> 00:12:47,000
 related to the word "sumati,"

123
00:12:47,000 --> 00:12:52,620
 which means concentrated or focused. "Su" means good, "su-

124
00:12:52,620 --> 00:12:54,000
sumahito."

125
00:12:54,000 --> 00:13:01,000
 "Sumahito" means level-headed or "equanimous" in a sense,

126
00:13:01,000 --> 00:13:05,000
 but it means balanced, so well balanced.

127
00:13:05,000 --> 00:13:11,000
 "Ajatang susumahito," internally well composed.

128
00:13:11,000 --> 00:13:18,990
 And number four, "Abinja vinaye sikan," "sikan" is training

129
00:13:18,990 --> 00:13:28,000
, "vinaye," to be free from or to lead oneself out,

130
00:13:28,000 --> 00:13:37,920
 to undertake the training for leading oneself out. "Abinja"

131
00:13:37,920 --> 00:13:45,000
 from greed or desire or craving.

132
00:13:45,000 --> 00:13:52,360
 And this fourth one is kind of curious. So anyway, in order

133
00:13:52,360 --> 00:13:57,000
, "Abyapano" means quite simple, the anger side.

134
00:13:57,000 --> 00:14:03,000
 The anger and greed are here. So we start with anger.

135
00:14:03,000 --> 00:14:06,730
 Anger is sort of a... it's different from desire because

136
00:14:06,730 --> 00:14:08,000
 anger is painful.

137
00:14:08,000 --> 00:14:13,430
 So it is a good one to start with. It's quite easy for

138
00:14:13,430 --> 00:14:16,000
 people to see how anger is a bad thing.

139
00:14:16,000 --> 00:14:20,260
 So it's easy to agree for the most part that it is

140
00:14:20,260 --> 00:14:24,000
 something that we should do away with.

141
00:14:24,000 --> 00:14:30,220
 Easier anyway, because it's unpleasant. No one really likes

142
00:14:30,220 --> 00:14:32,000
 being angry.

143
00:14:32,000 --> 00:14:35,840
 You might for a second feel good about the fact that you're

144
00:14:35,840 --> 00:14:37,000
 upset at someone,

145
00:14:37,000 --> 00:14:41,700
 but it always leaves a foul taste in your mouth. It's

146
00:14:41,700 --> 00:14:46,000
 painful to actually be angry.

147
00:14:46,000 --> 00:14:56,000
 It should be upset, it's stressful, harmful.

148
00:14:56,000 --> 00:14:58,260
 When it's harmful over the long term as well, it's

149
00:14:58,260 --> 00:15:01,000
 something that is all around not good for us.

150
00:15:01,000 --> 00:15:07,000
 It makes us bitter people and loses us friends and respect.

151
00:15:07,000 --> 00:15:12,940
 It gains us friends with bad intentions, it gains us

152
00:15:12,940 --> 00:15:14,000
 enemies.

153
00:15:14,000 --> 00:15:20,110
 It hurts our health. When we die, it makes us die with fear

154
00:15:20,110 --> 00:15:22,000
 and anger and resentment,

155
00:15:22,000 --> 00:15:26,390
 but in a bad way. So anywhere we go in the future, anything

156
00:15:26,390 --> 00:15:31,000
 that might happen in the future life is tainted.

157
00:15:31,000 --> 00:15:33,000
 So easy one to give up, "apayapano".

158
00:15:33,000 --> 00:15:36,000
 "Sada sato" means to always have sati.

159
00:15:36,000 --> 00:15:43,000
 So always be clear about the nature of our experience.

160
00:15:43,000 --> 00:15:47,000
 Never be at all at... have any misunderstanding about it.

161
00:15:47,000 --> 00:15:51,000
 Never be at odds with it.

162
00:15:51,000 --> 00:15:54,920
 You're aware when we stand, when we walk, when we sit, when

163
00:15:54,920 --> 00:15:56,000
 we lie down.

164
00:15:56,000 --> 00:15:59,590
 When we see, when we hear, when we smell, taste, feel,

165
00:15:59,590 --> 00:16:04,000
 think, being aware of it as it is.

166
00:16:04,000 --> 00:16:07,710
 Not just aware of it, but being aware that it is what it is

167
00:16:07,710 --> 00:16:08,000
.

168
00:16:08,000 --> 00:16:12,000
 So we remind ourselves of seeing, hearing.

169
00:16:12,000 --> 00:16:16,750
 We use this word to remind ourselves. It creates this

170
00:16:16,750 --> 00:16:19,000
 clarity of mind.

171
00:16:19,000 --> 00:16:25,000
 Sati.

172
00:16:25,000 --> 00:16:31,030
 "Ajatangsu samahito" means to focus our minds and to

173
00:16:31,030 --> 00:16:33,000
 compose our minds.

174
00:16:33,000 --> 00:16:36,000
 And this comes through mindfulness.

175
00:16:36,000 --> 00:16:39,500
 As you're mindful, your mind is going to sort itself out

176
00:16:39,500 --> 00:16:40,000
 now.

177
00:16:40,000 --> 00:16:45,260
 This may take days, weeks, months, years, depending how

178
00:16:45,260 --> 00:16:50,000
 intensively you practice.

179
00:16:50,000 --> 00:17:00,000
 But it will come. If you dedicate yourself to practice.

180
00:17:00,000 --> 00:17:05,000
 Your mind becomes more orderly.

181
00:17:05,000 --> 00:17:13,590
 You might still have likes and dislikes or upset in the

182
00:17:13,590 --> 00:17:15,000
 mind.

183
00:17:15,000 --> 00:17:17,520
 Your mind becomes orderly or it's not so much likes and

184
00:17:17,520 --> 00:17:18,000
 dislikes.

185
00:17:18,000 --> 00:17:28,360
 Your mind might still be loud in the sense that thoughts

186
00:17:28,360 --> 00:17:30,000
 come.

187
00:17:30,000 --> 00:17:34,910
 If you still interfere, you still interact with the world

188
00:17:34,910 --> 00:17:36,000
 around you.

189
00:17:36,000 --> 00:17:41,840
 You still experience pain and pleasure. You still

190
00:17:41,840 --> 00:17:44,000
 experience the world.

191
00:17:44,000 --> 00:17:49,000
 But once you're well composed, you become more objective

192
00:17:49,000 --> 00:17:53,000
 and less effective.

193
00:17:53,000 --> 00:17:57,000
 "Ajatangsu samahito" internally well composed.

194
00:17:57,000 --> 00:18:00,450
 And number four, this interesting fourth one, is

195
00:18:00,450 --> 00:18:06,000
 interesting in that it actually points out that you...

196
00:18:06,000 --> 00:18:09,000
 Craving is something that you have to train to overcome.

197
00:18:09,000 --> 00:18:11,570
 It's not like anger where you can just decide to be free

198
00:18:11,570 --> 00:18:13,000
 from it.

199
00:18:13,000 --> 00:18:16,000
 For the most part, you can set yourself on that path.

200
00:18:16,000 --> 00:18:18,680
 Craving is something that takes a little more care because

201
00:18:18,680 --> 00:18:22,000
 we don't really want to be free from it.

202
00:18:22,000 --> 00:18:25,000
 It's scary for us to think of letting go.

203
00:18:25,000 --> 00:18:30,450
 We think that, to some extent, that craving is all that

204
00:18:30,450 --> 00:18:32,000
 allows us to keep our happiness.

205
00:18:32,000 --> 00:18:35,000
 The only way you can be happy is to want things.

206
00:18:35,000 --> 00:18:38,550
 If I didn't like things anymore, how could I possibly be

207
00:18:38,550 --> 00:18:42,000
 happy or lose my happiness?

208
00:18:42,000 --> 00:18:47,080
 Actually, this liking thing is great. It allows me to

209
00:18:47,080 --> 00:18:49,000
 experience pleasure.

210
00:18:49,000 --> 00:18:55,000
 Liking can actually be quite pleasurable.

211
00:18:55,000 --> 00:18:58,350
 But the curious thing is, is we find no matter how much

212
00:18:58,350 --> 00:19:00,000
 liking or craving we have,

213
00:19:00,000 --> 00:19:05,000
 we're never really happy. We're never really satisfied.

214
00:19:05,000 --> 00:19:07,000
 We're never really at peace with ourselves.

215
00:19:07,000 --> 00:19:12,350
 In fact, we become less and less at peace with life, with

216
00:19:12,350 --> 00:19:15,000
 experience, with reality.

217
00:19:15,000 --> 00:19:18,000
 The more liking and wanting we have.

218
00:19:18,000 --> 00:19:26,000
 Eventually we come to a crisis where we have to choose.

219
00:19:26,000 --> 00:19:29,810
 Although we have to do something about it. We have to fix

220
00:19:29,810 --> 00:19:32,000
 the problem.

221
00:19:32,000 --> 00:19:34,850
 This is where most people find themselves when they're

222
00:19:34,850 --> 00:19:36,000
 turned to meditation,

223
00:19:36,000 --> 00:19:40,000
 they're no longer satisfied their addiction habits.

224
00:19:40,000 --> 00:19:46,520
 So they make a desperate attempt to change the world that

225
00:19:46,520 --> 00:19:48,000
 they're on.

226
00:19:48,000 --> 00:19:51,490
 When they begin to realize that the problem is the craving

227
00:19:51,490 --> 00:19:52,000
 itself,

228
00:19:52,000 --> 00:19:55,260
 not the fact that the problem isn't that they're not

229
00:19:55,260 --> 00:19:57,000
 getting what they want.

230
00:19:57,000 --> 00:20:01,000
 That's unavoidable. You can never, you can't always get

231
00:20:01,000 --> 00:20:02,000
 what you want.

232
00:20:02,000 --> 00:20:08,000
 The problem is the wanting itself.

233
00:20:08,000 --> 00:20:12,000
 And so that's where we find ourselves in the practice.

234
00:20:12,000 --> 00:20:15,000
 Looking to train ourselves to be free from craving.

235
00:20:15,000 --> 00:20:22,000
 It's a long, potentially long path.

236
00:20:22,000 --> 00:20:26,000
 Something you have to be patient with, but persistent.

237
00:20:26,000 --> 00:20:32,980
 Just because it's difficult and lengthy doesn't mean you

238
00:20:32,980 --> 00:20:35,000
 can slack off.

239
00:20:35,000 --> 00:20:43,000
 So we should always try to forge on, push on.

240
00:20:43,000 --> 00:20:47,510
 This is why communities are so helpful and friends are so

241
00:20:47,510 --> 00:20:48,000
 useful.

242
00:20:48,000 --> 00:20:51,000
 To keep in touch with other meditators is a great thing.

243
00:20:51,000 --> 00:20:54,000
 We encourage ourselves in the practice.

244
00:20:54,000 --> 00:20:58,080
 So it's always nice to see people coming together here on

245
00:20:58,080 --> 00:20:59,000
 the internet to practice,

246
00:20:59,000 --> 00:21:02,000
 and I'm happy to encourage.

247
00:21:02,000 --> 00:21:11,150
 I will be trying to give more online teachings now that I'm

248
00:21:11,150 --> 00:21:12,000
 back.

249
00:21:12,000 --> 00:21:16,000
 As long as I'm back here in Canada.

250
00:21:16,000 --> 00:21:20,000
 Maybe not every night, but I'll try and see.

251
00:21:20,000 --> 00:21:24,000
 We now have local meditators to think about as well.

252
00:21:24,000 --> 00:21:27,000
 So maybe you can combine the two.

253
00:21:27,000 --> 00:21:30,650
 Probably I should have been downstairs today giving this

254
00:21:30,650 --> 00:21:35,000
 talk to my one meditator.

255
00:21:35,000 --> 00:21:37,000
 Try again tomorrow maybe.

256
00:21:37,000 --> 00:21:39,000
 Anyway, that's the Dhamma for today.

257
00:21:39,000 --> 00:21:45,000
 The path of Appamada, these four dhammas.

258
00:21:45,000 --> 00:21:48,680
 Just something for us to think about reminding ourselves of

259
00:21:48,680 --> 00:21:50,000
 what's important.

260
00:21:50,000 --> 00:21:53,000
 So thank you all for tuning in and keep practicing.

