 Okay, so today we have Ian, he's here from the south of the
 border, no?
 You're from America?
 Virginia Beach.
 You have to turn on the mic.
 Did you turn the switch?
 Okay.
 Virginia Beach, USA.
 Tell us about yourself.
 Tell us about...
 You don't need to know about me.
 You don't have to give up personal information.
 What we want to know is before the practice, before this
 course, and after this course,
 what has changed?
 Before, I want to say I knew these things, but it was only
 an intellect.
 I was telling my friends, like, "Oh, you know, you're
 suffering.
 This is why you're suffering.
 This is why we all suffer."
 But after spending the time here and going step by step,
 moment by moment, you begin
 to know exactly why it is.
 It's something that you can only know through direct
 experience.
 Words can't really explain. Language is insufficient.
 How do you feel now?
 What's your feeling today?
 Been today?
 Overwhelmed?
 Very calm.
 I feel like I can handle a lot more.
 Strong.
 Invincible on some might say.
 No, maybe not invincible.
 You also see one of the common things.
 I'm not trying to put words in your mouth, but just to ask
 about it.
 It's also common to have a better picture of what you have
 yet to...
 the work you have yet to do.
 Definitely.
 It's been said before, but the iceberg, you know, what I
 thought I had to do was...
 just the tip.
 And there's a lot more.
 I see more work to be done.
 You know, today I was getting my ticket and I was having to
 get my password.
 I was getting frustrated with that.
 I was like, "Oh my goodness, it's still here.
 Still things that need to be uprooted and understood."
 Okay, well thank you, Ian.
 Ian's actually already...
 He's going to do the second course, though.
 Expert camera skills.
 This is...
 All right.
 Ian's sticking around, already asking about the instructor
 course.
 So we may actually have a candidate, first candidate here
 in Canada.
 I haven't taught an instructor course in meditation in a
 long time.
 The idea of being an instructor is to...
 well, talk about that some other time.
 It's not like you suddenly become a teacher,
 but you learn the technical skills necessary to lead
 someone through basic practice.
 Anyway, tonight I wanted to talk about this, the benefits
 of the practice.
 More specifically, why we practice.
 It's a bit of a trick question, actually.
 I'm not actually...
 I want to distinguish between benefits and reasons.
 Maybe not even reasons. It's not quite the right word.
 Because we have this problem.
 Buddhism is about freeing ourself from desire, and yet isn
't that a desire?
 I mean, to some extent it's the problem with semantics.
 But to some extent it's really not.
 It really is an apt question.
 Isn't the desire to be free from desire also a desire?
 I would say, yes, it is. And therefore it's problematic.
 So we have this dilemma.
 We can talk about the benefits of the practice.
 There's lots of them.
 And they're great. They're great for encouraging meditators
.
 That's what they're good for.
 So we have the five reasons why the Buddha taught Satipat
ana.
 He said, "What does Satipatana, what is the practice of Sat
i, of mindfulness, what does it lead to?"
 It leads to purity. Your mind becomes more pure.
 Satanam wisudiya, wisudiya. Satanam wisudiya.
 It allows you to go beyond mental illness.
 Sorrow, lamentation, despair, depression, anxiety, all
 these problems that we have.
 It helps you overcome suffering, physical suffering and
 mental suffering.
 And it leads you to the right path. It leads you to the
 goal, to freedom, to nibhana.
 So we have all of these. These are great reasons to
 practice.
 Who wouldn't want a pure mind? Who wouldn't want to be able
 to say that their mind is pure?
 You don't hear people saying, "Oh, I'm so glad I'm such a
 corrupt individual."
 I don't know. Maybe such people do exist.
 I don't meet such people. I'm blessed to meet mostly people
 who are very much interested in a pure mind.
 And so on. Being free from suffering sounds great.
 But the problem is when you cling to it, when you even
 strive for it, you get stuck.
 Because you're clinging, and clinging is the cause of
 suffering.
 You're craving, and craving is the cause of suffering.
 Ajahn Tong always talks about four benefits.
 These are good four things to tell those of you who have
 finished the course.
 So listen up, Ian, this is for you.
 There are four things that you get from the practice, from
 doing, let's say, the foundation course,
 or a foundation course in insight meditation.
 The first is mindfulness. Mindfulness is actually a goal.
 It's a great, wonderful outcome.
 This is what I meant by invincible.
 No, you're not invincible, but when you're mindful, that
 mind state is an invincible mind state.
 It's an incredibly powerful tool.
 Because suddenly there's no you, there's nothing to be hurt
.
 There's no problem.
 Problems disappear. There's only events, there's only
 experiences.
 You know, if I were giving a talk in real life, we wouldn't
 allow for all this chatter.
 In Sri Lanka, they're really all about asking questions
 during the Dhamma talk.
 But save the chatter for after, please. I don't mind, but
 it's got to be distracting for the audience.
 Sorry, on second life, they're chattering.
 It's all wholesome. But let's save the talk for afterwards.
 There's a time, "Kali na Dhamma Savanang", there's a time
 to listen to the Dhamma.
 "Kali na Dhamma Sagatya", there's a time to talk about,
 discuss the Dhamma, discussion after.
 So sati, mindfulness is the first benefit. It's actually a
 benefit of the practice.
 Why? Because you have this wonderful tool.
 You can practice it throughout your life. Any problems that
 come up, you can find challenges, conflicts, anything.
 Work, study, relationships, physical suffering, mental
 suffering, loss.
 Mindfulness is this wonderful tool that you get walking
 down the street.
 What do you do? Walking, walking, and this tool that's
 applicable everywhere.
 Brushing your teeth is brushing, brushing.
 The second one, in sort of a corollary, is happiness.
 As a result of being mindful, you find happiness.
 Ajahn Tong talks about this as heaven, going to heaven.
 But that's just one form of happiness. Heaven is of course
 a great happiness.
 But it's more general than that.
 There's so many happinesses that come from letting go, from
 being free, from seeing clearly, from the pure mind.
 The third is, they say upanisaya, which means you plant a
 seed.
 Upanisaya means different things.
 In Thai, they talk about it as being what you bring into
 your next life, what you carry on with you.
 So, I describe this as a start.
 The third benefit is you get a start on the path.
 You're no longer a beginner meditator, you're no longer a
 neophyte, you're no longer a luddite, a muggle.
 Isn't this word muggle?
 I don't know, Harry Potter has this big thing written. Is
 it muggle? Is that the word?
 A muggle is the right word, isn't it?
 Muggles are, I'm told, they're non-magic folk.
 And then there's whatever the opposite of muggle is, I don
't know.
 You're no longer one of those, you're no longer in the out
 crowd.
 It's not about belonging to a crowd, it's about having
 something wonderful.
 You can go and practice on your own.
 You can continue on, find other meditation groups.
 You are now a veteran, a graduate.
 You've got a start.
 And you take that with you.
 Upanisaya, as you take it with you into your next life,
 everything here, you forget people, places,
 and so on.
 But mindfulness is different, it changes your core, it
 changes who you are.
 It's a very deep, seated change to very basic habits of our
 being.
 So it changes the whole trajectory of our journey in sams
ara.
 And the fourth is a finish.
 Now it's possible through the basic course that you become
 a sotapana, this is always possible.
 It's possible that you, through many years of practice, you
 become a sotapana, sakatagami, anagami, arahant, it's all
 there.
 It's all possible.
 It's just a question of your effort and your merit and
 perfections, your goodness and your perfections.
 How much goodness you have, how much stored up purity you
 already have, just waiting to be used, put to use, and
 actualized in the meditation practice.
 You can become enlightened through this practice.
 So, for very good reasons of practice.
 But again, the problem, what if you sit there saying, "Am I
 a sotapana yet? How do I become a sotapana? When am I going
 to become a sotapana?
 I want so much to become a sotapana."
 That's a bad idea.
 As our meditators know, when they get to the end of the
 course, we have to make these resolves and
 the resolve, the resolution can be such a hindrance to the
 practice because you're waiting for it to happen,
 expecting for something special to happen.
 It destroys your practice.
 So it's more than just a quandary. It speaks to the very
 nature of samsara versus the nature of nimbana.
 This is where this problem comes from.
 Samsara is cause and effect. Everything in every part of
 this universe that we know is conditionally formed, sankara
, sankatta.
 Everything that is conditioned, everything that is cause
 and effect is samsara.
 Nimbana is unconditioned. You can't bring about nimbana.
 You can't cause it to evoke it, cause it to arise.
 It doesn't arise. There is no arising of nimbana.
 It is permanent. Permanence may be the wrong word, but it
 is stable.
 It is stable and dissatisfying. It is eternal, undying,
 unchanging.
 If it's unchanging, how could you cause it to come, cause
 it to arise?
 Because nimbana is not cause and effect, the way we
 approach it and the way we gradually work to bring
 ourselves closer to this, to freedom,
 has to be quite different. It can't be cause and effect.
 You can't be working towards some goal. Working towards
 goals, that's samsara.
 This is why this emphasis on the present moment, it's a
 very good reason why we have to focus on the present moment
,
 because we can't be about cause and effect. We can't be
 striving for some goal.
 Even though we often use those words, when you practice it
 can't be about that.
 It has to be putting down the burden, giving up, giving up
 our ambitions.
 So when we look to our practice, we have to be much more
 about the quality of the practice, the purity, the goodness
, rather than the happiness, the peace.
 All of the benefits that we talk about, even listening to
 meditators describe the outcome of the practice,
 has great encouragement and reassurance to remove your
 doubts.
 Yes, this is a good path, but it won't help you in the
 practice. You can't think, "Am I there yet? Okay, what do I
 have to do to become like that person?"
 That's not how it works. And you get so caught up, hung up,
 held up, held back, when you try to look for results.
 "Am I getting anything out of this practice?" It's a few
 doubts.
 It's not that you couldn't see, it's that it wouldn't be
 useful if you did.
 When you're doing that, you're no longer meditating.
 So be about quality, quality in the present moment. It's
 like, put your nose to the grindstone, don't ever look up.
 Do your work, your bank account fills up by itself.
 The Dhamma checks are what do you call it? Auto deposit?
 What's the word?
 You get paid automatically. No need to receive a check.
 Your bank account, your Dhamma bank fills up by itself. You
 just do the work.
 Direct deposit, yeah, that's it.
 Alright. So, a little bit of talk about benefits
 nonetheless.
 It's always good to talk about because it's a reassurance,
 but a reminder and a warning that this is not how we should
 approach our practice.
 Practice should always be about the practice itself. Why we
 practice?
 Why we practice is because not practicing is a cause of
 suffering.
 Why we practice is because we have ignorance.
 Why we practice is because there's a problem.
 Why we practice is it's very much about the rightness and
 the propriety of it, the goodness of it, rather than we do
 it for x-wall.
 We can't always be thinking about this.
 We have to give up the idea of motivation and being
 motivated.
 Our motivation, if we use the word, should be for purity,
 for quality, for rightness, goodness here and now, and let
 the results come.
 It's like this whole God thing, right?
 People say you have to do x or else God will be angry at
 you.
 If you do x, God will be pleased.
 I just think, if God's going to be angry at me, isn't that
 his problem?
 If God's going to be happy for me, is this really a carrot
 in the stick sort of thing?
 What an awful way to live.
 Just another reason to never believe in any God.
 I think we've covered just about everything.
 That's the time of our tonight. Thank you all for tuning in
. Have a good night.
