1
00:00:00,000 --> 00:00:09,000
 There's not a lot to talk about in Buddhism.

2
00:00:09,000 --> 00:00:19,680
 There's a lot to say, but not a lot to talk about.

3
00:00:19,680 --> 00:00:32,900
 We don't have gods. I mean, there are beings that you might

4
00:00:32,900 --> 00:00:37,360
 call gods, but they're not an object of worship or even

5
00:00:37,360 --> 00:00:41,680
 really consideration in Buddhism.

6
00:00:41,680 --> 00:00:49,070
 We don't have a lot to accept in passing as beings who have

7
00:00:49,070 --> 00:01:01,680
 followed a certain path, reached a certain result.

8
00:01:01,680 --> 00:01:16,680
 We don't have rituals that you must perform.

9
00:01:16,680 --> 00:01:23,680
 Theories you must believe.

10
00:01:23,680 --> 00:01:39,680
 Buddhism is about simplification. It's about reality.

11
00:01:39,680 --> 00:01:46,460
 An important part of reality is how simple it is. We talk

12
00:01:46,460 --> 00:01:52,680
 about reality as opposed to conceptualization.

13
00:01:52,680 --> 00:02:05,490
 It turns out that pretty much everything we focus on in

14
00:02:05,490 --> 00:02:06,680
 life is

15
00:02:06,680 --> 00:02:12,750
 at least mostly conceptualization. There's a concept in

16
00:02:12,750 --> 00:02:17,680
 Buddhism called bhapanja.

17
00:02:17,680 --> 00:02:28,380
 Bhapanja means making more out of something than it

18
00:02:28,380 --> 00:02:31,680
 actually is.

19
00:02:31,680 --> 00:02:36,290
 There's not a lot to talk about because there's not a lot

20
00:02:36,290 --> 00:02:45,680
 that's real. Everything else is bhapanja.

21
00:02:45,680 --> 00:02:55,300
 Take the five aggregates. These are one way of talking

22
00:02:55,300 --> 00:02:59,680
 about what's real.

23
00:02:59,680 --> 00:03:04,220
 You can see how our ordinary perception of the five aggreg

24
00:03:04,220 --> 00:03:11,680
ates is almost entirely our conception of them.

25
00:03:11,680 --> 00:03:18,300
 When we talk about body, we don't think immediately of the

26
00:03:18,300 --> 00:03:24,680
 tactile sensations of tension or hardness or softness.

27
00:03:24,680 --> 00:03:29,160
 Body, what is body? Body is this body with the hands and

28
00:03:29,160 --> 00:03:37,820
 legs, arms and legs and trunk and head, with its organs,

29
00:03:37,820 --> 00:03:44,680
 its systems, its health, its life, its heat.

30
00:03:44,680 --> 00:03:50,860
 The body is what walks and talks. This is how we think of

31
00:03:50,860 --> 00:03:52,680
 the body.

32
00:03:52,680 --> 00:04:03,680
 This isn't real in Buddhism. This is all bhapanja.

33
00:04:03,680 --> 00:04:15,410
 How do you know you have a body? You don't know you have a

34
00:04:15,410 --> 00:04:23,680
 body. You extrapolate based on your experiences.

35
00:04:23,680 --> 00:04:30,960
 When you open your eyes, not that you have eyes, but

36
00:04:30,960 --> 00:04:39,680
 speaking conceptually, they're seeing, you see your hands.

37
00:04:39,680 --> 00:04:45,960
 You know that these are your hands. When you squeeze your

38
00:04:45,960 --> 00:04:51,960
 hands, you feel the pressure. You look down and you see

39
00:04:51,960 --> 00:04:54,680
 your trunk and your legs.

40
00:04:54,680 --> 00:05:00,070
 But the reality is all that is just seeing. You can turn on

41
00:05:00,070 --> 00:05:05,680
 a television and see all sorts of things that aren't real.

42
00:05:05,680 --> 00:05:10,740
 Just because you see them doesn't mean they're actually

43
00:05:10,740 --> 00:05:11,680
 there.

44
00:05:11,680 --> 00:05:17,050
 It doesn't matter. This isn't a discussion of whether your

45
00:05:17,050 --> 00:05:25,680
 body exists or not. It exists, but it's not real.

46
00:05:25,680 --> 00:05:29,870
 It exists as a concept, as an idea, as a description, if

47
00:05:29,870 --> 00:05:34,230
 you will. It's just a way of describing it. And there are

48
00:05:34,230 --> 00:05:38,680
 lots of different ways of describing our experiences,

49
00:05:38,680 --> 00:05:41,680
 extrapolating from our experiences.

50
00:05:41,680 --> 00:05:45,930
 But none of them touch on the base reality of seeing,

51
00:05:45,930 --> 00:05:51,300
 hearing, smelling, tasting, feeling, thinking. That's what

52
00:05:51,300 --> 00:05:52,680
's actually happening.

53
00:05:52,680 --> 00:05:58,850
 That's the reality of it. All the rest is no better than an

54
00:05:58,850 --> 00:06:14,680
 illusion. It has no more substance than a mirage.

55
00:06:14,680 --> 00:06:20,470
 The physicists have found that the majority of, the vast

56
00:06:20,470 --> 00:06:26,060
 majority of almost everything we see and touch and feel

57
00:06:26,060 --> 00:06:32,000
 physically that solid is almost entirely made up of empty

58
00:06:32,000 --> 00:06:32,680
 space.

59
00:06:32,680 --> 00:06:38,080
 Which flies very much in the face of our conception of

60
00:06:38,080 --> 00:06:43,920
 things. But they say it's real. I absolutely believe that

61
00:06:43,920 --> 00:06:52,870
 that's the truth. No, I'm not doubting physicists by any

62
00:06:52,870 --> 00:06:54,680
 means.

63
00:06:54,680 --> 00:07:06,490
 It just goes to show that there's no substance in

64
00:07:06,490 --> 00:07:10,680
 perceptions.

65
00:07:10,680 --> 00:07:17,890
 We look at Vedana feelings. We make more out of these than

66
00:07:17,890 --> 00:07:32,680
 they actually are. We take pain to be a problem.

67
00:07:32,680 --> 00:07:38,440
 We take pleasure to be a blessing. We have all sorts of

68
00:07:38,440 --> 00:07:44,680
 narratives. Oh, I have arthritis, chronic pain, headaches.

69
00:07:44,680 --> 00:07:47,680
 I get migraines, we will say.

70
00:07:47,680 --> 00:07:53,810
 And we have an identification with the headache. I get

71
00:07:53,810 --> 00:07:59,250
 headaches. I have a headache. Even just calling it a

72
00:07:59,250 --> 00:08:06,680
 headache is already too much. It's already something extra.

73
00:08:06,680 --> 00:08:10,810
 Because what is a headache? It's a thing. You've made it

74
00:08:10,810 --> 00:08:14,680
 into a thing when it's actually just an experience.

75
00:08:14,680 --> 00:08:19,900
 It's in fact moments of experience. And if you look closely

76
00:08:19,900 --> 00:08:24,080
 enough and are careful enough, you see that even the

77
00:08:24,080 --> 00:08:27,680
 moments of experience of the headache are individual.

78
00:08:27,680 --> 00:08:34,680
 They come and they go and new ones arise.

79
00:08:34,680 --> 00:08:40,140
 And our pleasure, oh, our pleasure, what we make out of it,

80
00:08:40,140 --> 00:08:45,410
 the stories we make, the things we love and like, how many

81
00:08:45,410 --> 00:08:50,110
 poems and books have been written about romance or

82
00:08:50,110 --> 00:08:59,680
 friendship, family, food, sex, music.

83
00:08:59,680 --> 00:09:06,190
 How we, what we make out of music. Think of the theory that

84
00:09:06,190 --> 00:09:12,090
 behind music, the musical theory and the work that goes

85
00:09:12,090 --> 00:09:13,680
 into music.

86
00:09:13,680 --> 00:09:17,680
 Think of the theater.

87
00:09:17,680 --> 00:09:24,990
 The theater that was designed to evoke emotion as a cathars

88
00:09:24,990 --> 00:09:30,070
is. This had meaning. So we give meaning to things beyond

89
00:09:30,070 --> 00:09:36,190
 what they actually have. And the truth is, these are just

90
00:09:36,190 --> 00:09:37,680
 feelings.

91
00:09:37,680 --> 00:09:44,680
 Pain is no worse than pleasure. Pleasure is no better than

92
00:09:44,680 --> 00:09:45,680
 pain.

93
00:09:45,680 --> 00:09:49,680
 And the better or worse are just ideas.

94
00:09:49,680 --> 00:09:56,100
 What are they good for? Someone asked, recently expressed

95
00:09:56,100 --> 00:10:01,600
 the idea that they couldn't understand how, how can

96
00:10:01,600 --> 00:10:04,680
 pleasure not be good?

97
00:10:04,680 --> 00:10:09,630
 In what sense is it good? What does it mean to say that

98
00:10:09,630 --> 00:10:14,280
 something is good? I think the obvious answer would be,

99
00:10:14,280 --> 00:10:15,680
 well, I like it.

100
00:10:15,680 --> 00:10:21,360
 Liking something makes it good. Obviously not, but is

101
00:10:21,360 --> 00:10:26,680
 liking any good? What good is liking?

102
00:10:26,680 --> 00:10:29,060
 Well, liking makes you get the things you want. And then

103
00:10:29,060 --> 00:10:31,840
 you get more pleasure, which of course you like, and that's

104
00:10:31,840 --> 00:10:34,130
 good somehow. There's no meaning behind this. There's no

105
00:10:34,130 --> 00:10:39,700
 intrinsic meaning to any of this. It's illusion and

106
00:10:39,700 --> 00:10:42,680
 delusion.

107
00:10:42,680 --> 00:10:47,680
 It's not real.

108
00:10:47,680 --> 00:10:52,680
 So before we get into, before we ever get into talking

109
00:10:52,680 --> 00:10:58,720
 about the importance of this, the importance of

110
00:10:58,720 --> 00:10:59,680
 understanding and the...

111
00:10:59,680 --> 00:11:09,680
 [BLANK_AUDIO]

112
00:11:11,680 --> 00:11:19,680
 [BLANK_AUDIO]

113
00:11:21,680 --> 00:11:29,680
 [BLANK_AUDIO]

114
00:11:31,680 --> 00:11:39,680
 [BLANK_AUDIO]

115
00:11:41,680 --> 00:11:49,680
 [BLANK_AUDIO]

116
00:11:51,680 --> 00:11:59,680
 [BLANK_AUDIO]

117
00:12:01,680 --> 00:12:09,680
 [BLANK_AUDIO]

118
00:12:11,680 --> 00:12:19,680
 [BLANK_AUDIO]

119
00:12:21,680 --> 00:12:29,680
 [BLANK_AUDIO]

120
00:12:31,680 --> 00:12:39,680
 [BLANK_AUDIO]

121
00:12:41,680 --> 00:12:49,680
 [BLANK_AUDIO]

122
00:12:51,680 --> 00:12:59,680
 [BLANK_AUDIO]

123
00:13:18,680 --> 00:13:26,680
 This is like that.

124
00:13:26,680 --> 00:13:32,240
 But the actual recognition, that's where we make more out

125
00:13:32,240 --> 00:13:39,670
 of it than it actually is. Oh yes, this is a beautiful

126
00:13:39,670 --> 00:13:44,680
 woman, a beautiful man.

127
00:13:44,680 --> 00:13:49,630
 Where we create a story, give it meaning, rather than

128
00:13:49,630 --> 00:13:53,330
 simply appreciating the recognition or seeing the

129
00:13:53,330 --> 00:13:56,680
 recognition for what it is, we make more out of it.

130
00:13:56,680 --> 00:14:02,680
 And again, leads of course to many, many problems.

131
00:14:02,680 --> 00:14:09,680
 Especially relating to Sankara, the next one.

132
00:14:09,680 --> 00:14:14,680
 So Sankara is a bit of a catch all, it has a lot in it.

133
00:14:14,680 --> 00:14:18,600
 So Sankara is, there is at least a bit to talk about, but

134
00:14:18,600 --> 00:14:23,760
 it's not that complicated, it just means our mental factors

135
00:14:23,760 --> 00:14:25,680
, mental qualities.

136
00:14:25,680 --> 00:14:30,680
 It includes thoughts, judgments,

137
00:14:30,680 --> 00:14:35,080
 proliferations, this is where all the proliferations are,

138
00:14:35,080 --> 00:14:38,680
 talking about proliferations, they're real.

139
00:14:38,680 --> 00:14:42,320
 The proliferations themselves are real, but that's all they

140
00:14:42,320 --> 00:14:45,750
 are, they are just making more out of things than things

141
00:14:45,750 --> 00:14:48,680
 actually are, than making more.

142
00:14:48,680 --> 00:14:53,680
 Meaning making or whatever is real, it really happens.

143
00:14:53,680 --> 00:14:57,310
 But the meaning that we make is not real, the meaning

144
00:14:57,310 --> 00:15:01,680
 itself is just an illusion, but that creation of illusion,

145
00:15:01,680 --> 00:15:03,680
 that's in Sankara.

146
00:15:03,680 --> 00:15:07,960
 And so this is of course, this is directly where the making

147
00:15:07,960 --> 00:15:08,680
 more is.

148
00:15:08,680 --> 00:15:14,970
 So we give rise to thoughts about things, and just to, on a

149
00:15:14,970 --> 00:15:17,680
 basic level, these are useful.

150
00:15:17,680 --> 00:15:21,490
 It's useful to recognize, ah yes, that's my mother, that's

151
00:15:21,490 --> 00:15:23,680
 my father, that's my teacher.

152
00:15:23,680 --> 00:15:29,720
 This is a poisonous plant, this is an edible plant, that's

153
00:15:29,720 --> 00:15:32,680
 a poisonous snake.

154
00:15:32,680 --> 00:15:41,900
 Being able to analyze and consider things, being able to

155
00:15:41,900 --> 00:15:44,680
 judge things.

156
00:15:44,680 --> 00:15:47,150
 Being able to make conclusions based on our recognition of

157
00:15:47,150 --> 00:15:47,680
 things.

158
00:15:47,680 --> 00:15:51,940
 I recognize that spider, okay, that one I'm going to stay

159
00:15:51,940 --> 00:15:54,680
 away from because it's poisonous.

160
00:15:54,680 --> 00:15:58,380
 On a basic level they're useful, but we make more out of

161
00:15:58,380 --> 00:15:59,680
 them than they actually are.

162
00:15:59,680 --> 00:16:04,460
 Rather than use them just for practical purposes, we use

163
00:16:04,460 --> 00:16:06,680
 them to create stories.

164
00:16:06,680 --> 00:16:14,370
 We use them to feed our defilements, our unwholesome mental

165
00:16:14,370 --> 00:16:19,680
 states, our mental illness, if you will, our neuroses.

166
00:16:19,680 --> 00:16:22,680
 That's how they'd be talked about in the West.

167
00:16:22,680 --> 00:16:29,360
 We might have fear, phobias, anxiety, panic attacks,

168
00:16:29,360 --> 00:16:33,680
 depression, anger, addiction.

169
00:16:33,680 --> 00:16:39,180
 All of these are Sankaras and all of these bad, bad, bad

170
00:16:39,180 --> 00:16:44,680
 things are not actually, in reality, that big of a deal.

171
00:16:44,680 --> 00:16:49,680
 They're not actually that bad. They're just moments.

172
00:16:49,680 --> 00:16:54,050
 I mean, they're not good for you, but they only become

173
00:16:54,050 --> 00:16:58,370
 problems because we turn them into problems, because we re

174
00:16:58,370 --> 00:16:59,680
ify them.

175
00:16:59,680 --> 00:17:05,680
 Depression. I'm so depressed. I'm so bored. This is boring.

176
00:17:05,680 --> 00:17:12,030
 We go from saying that I'm bored, which is real. Yes, there

177
00:17:12,030 --> 00:17:13,680
 is boredom.

178
00:17:13,680 --> 00:17:20,680
 To saying this is boring. We blame the experience.

179
00:17:20,680 --> 00:17:27,680
 We accuse our experiences of our own boredom.

180
00:17:27,680 --> 00:17:32,220
 You make me so angry. We blame the other person for making

181
00:17:32,220 --> 00:17:32,680
 you angry,

182
00:17:32,680 --> 00:17:40,960
 as if they could go into your brain and flip the switch to

183
00:17:40,960 --> 00:17:44,680
 make your mind angry.

184
00:17:44,680 --> 00:17:49,990
 If we could just see, even anxiety and fear, if we could

185
00:17:49,990 --> 00:17:53,680
 just see them for what they are,

186
00:17:53,680 --> 00:17:59,680
 we'd be free from all these problems.

187
00:17:59,680 --> 00:18:06,680
 They wouldn't let any of them be problems.

188
00:18:06,680 --> 00:18:11,310
 We would no longer reify them. We would no longer feed them

189
00:18:11,310 --> 00:18:11,680
.

190
00:18:11,680 --> 00:18:17,680
 We would just see them and let them come and let them go.

191
00:18:17,680 --> 00:18:23,520
 Finally, consciousness. We make more out of consciousness

192
00:18:23,520 --> 00:18:25,680
 than it actually is.

193
00:18:25,680 --> 00:18:33,680
 Consciousness is the chief.

194
00:18:33,680 --> 00:18:37,680
 Consciousness is another word you could say for experience.

195
00:18:37,680 --> 00:18:43,100
 Maybe not exactly, but it's almost the same thing.

196
00:18:43,100 --> 00:18:50,680
 Consciousness is, for all intents and purposes, experience.

197
00:18:50,680 --> 00:18:56,680
 This is the key that our experiences are just experiences.

198
00:18:56,680 --> 00:19:00,980
 Experiences of all these things that the Buddha talked

199
00:19:00,980 --> 00:19:04,680
 about, the things I've talked about.

200
00:19:04,680 --> 00:19:09,500
 When we apply mindfulness and we become present with our

201
00:19:09,500 --> 00:19:10,680
 experiences,

202
00:19:10,680 --> 00:19:19,580
 we free ourselves from all this baggage, our perspective

203
00:19:19,580 --> 00:19:21,680
 changes.

204
00:19:21,680 --> 00:19:32,990
 We begin a long and arduous process of healing our minds,

205
00:19:32,990 --> 00:19:39,680
 cleansing our minds, purifying our minds,

206
00:19:39,680 --> 00:19:47,210
 bringing back innocence to our minds, freeing ourselves

207
00:19:47,210 --> 00:19:54,680
 from the crippling weight of defilement,

208
00:19:54,680 --> 00:20:01,220
 of unwholesomeness, of obsession, of proliferation, our

209
00:20:01,220 --> 00:20:07,680
 illusions and delusions about reality.

210
00:20:07,680 --> 00:20:11,680
 There's a lot to say, but not a lot to talk about.

211
00:20:11,680 --> 00:20:16,120
 It's an important point that reality doesn't have a lot to

212
00:20:16,120 --> 00:20:16,680
 it.

213
00:20:16,680 --> 00:20:22,670
 We try always to come back to bring ourselves back to this

214
00:20:22,670 --> 00:20:23,680
 reality,

215
00:20:23,680 --> 00:20:31,680
 to free ourselves from the entrapment of the infinite maze

216
00:20:31,680 --> 00:20:36,680
 without end of conception.

217
00:20:36,680 --> 00:20:41,680
 Being illusory, it has no end.

218
00:20:41,680 --> 00:20:46,680
 So, that's the demo for tonight.

