1
00:00:00,000 --> 00:00:21,010
 So as we come to the end of the year, we are traditionally

2
00:00:21,010 --> 00:00:29,800
 inclined to make resolutions,

3
00:00:29,800 --> 00:00:43,080
 changes we'd like to see within ourselves.

4
00:00:43,080 --> 00:00:49,560
 And many of these changes relate to bad habits that we have

5
00:00:49,560 --> 00:00:50,040
.

6
00:00:50,040 --> 00:00:54,120
 Habits, habits.

7
00:00:54,120 --> 00:01:00,250
 They relate to behaviors that we see in ourselves as a part

8
00:01:00,250 --> 00:01:02,960
 of our personality that we'd like

9
00:01:02,960 --> 00:01:09,320
 to change, which is pretty much the definition of a habit.

10
00:01:09,320 --> 00:01:14,440
 You don't call it a habit if it is something you do once.

11
00:01:14,440 --> 00:01:17,220
 And if it's something that you do once, you probably don't

12
00:01:17,220 --> 00:01:19,280
 make a resolution about it.

13
00:01:19,280 --> 00:01:23,130
 Or if you do, it's kind of cheating because it's not a very

14
00:01:23,130 --> 00:01:24,000
 hard one.

15
00:01:24,000 --> 00:01:27,040
 Still it can be a good resolution though.

16
00:01:27,040 --> 00:01:31,470
 If you did something really terrible this year and you make

17
00:01:31,470 --> 00:01:33,360
 a resolution never to do

18
00:01:33,360 --> 00:01:39,130
 that again, that's of course an incredibly wholesome thing

19
00:01:39,130 --> 00:01:39,960
 to do.

20
00:01:39,960 --> 00:01:46,410
 But the harder ones and the more therefore valuable ones

21
00:01:46,410 --> 00:01:49,760
 are when we make a resolution

22
00:01:49,760 --> 00:01:56,060
 to change our habits.

23
00:01:56,060 --> 00:02:04,370
 And that's a really important concept from a meditation

24
00:02:04,370 --> 00:02:10,260
 perspective, a Buddhist perspective.

25
00:02:10,260 --> 00:02:17,340
 Because meditation is really all about habits.

26
00:02:17,340 --> 00:02:20,740
 It is about building habits.

27
00:02:20,740 --> 00:02:24,900
 It's about counteracting habits.

28
00:02:24,900 --> 00:02:28,240
 Meditation is a mental training.

29
00:02:28,240 --> 00:02:35,040
 And so training changes something fundamental about our

30
00:02:35,040 --> 00:02:37,080
 personality.

31
00:02:37,080 --> 00:02:39,730
 It changes the quality of our experiences in some way,

32
00:02:39,730 --> 00:02:41,400
 whether it's physical training

33
00:02:41,400 --> 00:02:49,880
 or mental training.

34
00:02:49,880 --> 00:02:53,790
 And so the determination, the resolution that we take at

35
00:02:53,790 --> 00:02:56,400
 the end of the year, at the beginning

36
00:02:56,400 --> 00:03:12,200
 of the year to change is best followed up with some sort of

37
00:03:12,200 --> 00:03:14,920
 regimen of training.

38
00:03:14,920 --> 00:03:18,320
 Some sort of action.

39
00:03:18,320 --> 00:03:25,380
 So I think it is probably a common thing for someone to

40
00:03:25,380 --> 00:03:29,640
 make a resolution and then fail

41
00:03:29,640 --> 00:03:32,880
 to follow up with anything further.

42
00:03:32,880 --> 00:03:37,640
 The idea that being resolute is enough.

43
00:03:37,640 --> 00:03:40,360
 I resolve not to get angry.

44
00:03:40,360 --> 00:03:46,080
 I resolve to stop smoking.

45
00:03:46,080 --> 00:03:52,640
 And then I'll just say no, no, no, no smoking for me.

46
00:03:52,640 --> 00:03:56,440
 You see the inclination of the mind is still there.

47
00:03:56,440 --> 00:04:02,280
 Our mind is still inclined towards smoking.

48
00:04:02,280 --> 00:04:04,700
 That's a habit.

49
00:04:04,700 --> 00:04:09,500
 And so following up our New Year's resolutions with

50
00:04:09,500 --> 00:04:13,480
 training as a means of changing our habits

51
00:04:13,480 --> 00:04:21,280
 is probably one of the most important parts of this

52
00:04:21,280 --> 00:04:27,680
 tradition of New Year's resolutions.

53
00:04:27,680 --> 00:04:34,680
 If you want to lose weight, you can't just say, "Well, I'm

54
00:04:34,680 --> 00:04:38,120
 going to stop gaining weight

55
00:04:38,120 --> 00:04:39,120
 or stop--"

56
00:04:39,120 --> 00:04:45,350
 I'm going to not keep up this weight and the weight is just

57
00:04:45,350 --> 00:04:48,000
 going to disappear.

58
00:04:48,000 --> 00:04:54,560
 Obviously you have diets and then you have exercise reg

59
00:04:54,560 --> 00:04:57,600
imens that you take on.

60
00:04:57,600 --> 00:05:03,450
 But even with exercise, even with losing weight, our

61
00:05:03,450 --> 00:05:08,800
 inclination is not to diet, not to exercise.

62
00:05:08,800 --> 00:05:12,240
 If we're overweight.

63
00:05:12,240 --> 00:05:17,200
 And so even their mental training is incredibly important.

64
00:05:17,200 --> 00:05:24,880
 We were talking about jitana recently on Discord.

65
00:05:24,880 --> 00:05:30,140
 It was something that I didn't think about the fact that

66
00:05:30,140 --> 00:05:33,240
 inclination, jitana, is in every

67
00:05:33,240 --> 00:05:37,760
 single mind state.

68
00:05:37,760 --> 00:05:44,400
 We normally think of jitana as being karma.

69
00:05:44,400 --> 00:05:45,400
 What do we mean by karma?

70
00:05:45,400 --> 00:05:50,160
 We mean jitana, but not all jitana is karmically active.

71
00:05:50,160 --> 00:05:54,080
 We have every moment an inclination of mind.

72
00:05:54,080 --> 00:05:57,610
 Some of the inclinations that we have, some moments are

73
00:05:57,610 --> 00:05:59,280
 inclined in a good way.

74
00:05:59,280 --> 00:06:00,680
 Some are inclined in a bad way.

75
00:06:00,680 --> 00:06:04,590
 Some are not inclined in either a good or bad way, but just

76
00:06:04,590 --> 00:06:07,400
 have an inclination to walk,

77
00:06:07,400 --> 00:06:10,680
 an inclination to talk, for whatever reason, but not eth

78
00:06:10,680 --> 00:06:11,800
ically charged.

79
00:06:11,800 --> 00:06:17,640
 No greed, no anger, no delusion.

80
00:06:17,640 --> 00:06:19,980
 But many are.

81
00:06:19,980 --> 00:06:23,080
 Many of our habits are bad habits.

82
00:06:23,080 --> 00:06:29,340
 Associated with obesity is often greed, laziness, and then

83
00:06:29,340 --> 00:06:32,680
 of course a lot of guilt and shame

84
00:06:32,680 --> 00:06:33,680
 and self-loathing.

85
00:06:33,680 --> 00:06:40,260
 It can be trauma, it can lead to addiction, using food as a

86
00:06:40,260 --> 00:06:43,440
 means of coping with trauma,

87
00:06:43,440 --> 00:06:54,760
 coping with depression, coping with lots of things.

88
00:06:54,760 --> 00:07:03,290
 So meditation is a great way to allow us to make the

89
00:07:03,290 --> 00:07:07,400
 changes we'd like to see in our lives

90
00:07:07,400 --> 00:07:12,720
 because it trains the mind.

91
00:07:12,720 --> 00:07:17,580
 And lots of meditation types that you hear about are very

92
00:07:17,580 --> 00:07:20,360
 specific in their training.

93
00:07:20,360 --> 00:07:26,210
 Probably the most common is just a training and being more

94
00:07:26,210 --> 00:07:27,120
 calm.

95
00:07:27,120 --> 00:07:32,210
 So when you have anxiety, you train yourself to be more

96
00:07:32,210 --> 00:07:33,080
 calm.

97
00:07:33,080 --> 00:07:34,080
 And that works.

98
00:07:34,080 --> 00:07:37,840
 You can become more calm.

99
00:07:37,840 --> 00:07:46,110
 Other meditation types teach you to be more kind, more

100
00:07:46,110 --> 00:07:51,540
 compassionate, more empathic.

101
00:07:51,540 --> 00:07:55,330
 So they give you a new habit of mind, or they change your

102
00:07:55,330 --> 00:07:56,080
 habit.

103
00:07:56,080 --> 00:07:59,850
 So if you're a very angry person, there's meditations that

104
00:07:59,850 --> 00:08:01,680
 teach you a new habit of thinking

105
00:08:01,680 --> 00:08:05,740
 kind thoughts about others instead of being spiteful and

106
00:08:05,740 --> 00:08:08,120
 hateful and so on, even towards

107
00:08:08,120 --> 00:08:12,880
 yourself.

108
00:08:12,880 --> 00:08:15,800
 But mindfulness is a little different.

109
00:08:15,800 --> 00:08:21,920
 Mindfulness goes a little deeper because there are reasons

110
00:08:21,920 --> 00:08:27,120
 why our habits persist and increase,

111
00:08:27,120 --> 00:08:28,120
 get worse.

112
00:08:28,120 --> 00:08:34,360
 And bad habits get worse, get stronger.

113
00:08:34,360 --> 00:08:38,440
 And mindfulness addresses this.

114
00:08:38,440 --> 00:08:44,990
 It addresses existing habits directly without really

115
00:08:44,990 --> 00:08:48,040
 creating any new habits.

116
00:08:48,040 --> 00:08:52,490
 Because even a meditation that teaches you to be more calm

117
00:08:52,490 --> 00:08:54,920
 is only going to be a temporary

118
00:08:54,920 --> 00:08:59,250
 solution for as long as you train your mind to be calm, you

119
00:08:59,250 --> 00:09:00,440
'll be calm.

120
00:09:00,440 --> 00:09:03,820
 But anytime you go back to doing an activity where your

121
00:09:03,820 --> 00:09:06,000
 mind has to be active, that habit

122
00:09:06,000 --> 00:09:09,000
 will change back.

123
00:09:09,000 --> 00:09:17,580
 But mindfulness isn't quite like that because mindfulness

124
00:09:17,580 --> 00:09:19,600
 is about simplifying our habits,

125
00:09:19,600 --> 00:09:25,360
 about smoothing out the complexities of our interactions

126
00:09:25,360 --> 00:09:26,920
 with things.

127
00:09:26,920 --> 00:09:33,640
 So habits relate to our reactions to our experiences.

128
00:09:33,640 --> 00:09:37,240
 Think of the habit of addiction.

129
00:09:37,240 --> 00:09:39,610
 When you're addicted to something, it's almost like

130
00:09:39,610 --> 00:09:41,120
 something you can't control.

131
00:09:41,120 --> 00:09:42,960
 It's instantaneous.

132
00:09:42,960 --> 00:09:47,860
 Whenever you see the object of your addiction or even think

133
00:09:47,860 --> 00:09:50,800
 about the object of your addiction,

134
00:09:50,800 --> 00:09:51,920
 you react to it.

135
00:09:51,920 --> 00:09:56,130
 And because of the charged, excited nature of your

136
00:09:56,130 --> 00:09:59,040
 relationship to the object of your

137
00:09:59,040 --> 00:10:04,800
 addiction, your brain is constantly triggering memories of

138
00:10:04,800 --> 00:10:07,880
 the object of your addiction.

139
00:10:07,880 --> 00:10:12,340
 So you're constantly thinking about food or sex or music or

140
00:10:12,340 --> 00:10:14,440
 whatever it is that you're

141
00:10:14,440 --> 00:10:21,480
 addicted to cigarettes and immediately reacting to it.

142
00:10:21,480 --> 00:10:30,880
 Desire, wanting, needing, craving, clinging.

143
00:10:30,880 --> 00:10:43,140
 And so mindfulness addresses this activity of reacting to

144
00:10:43,140 --> 00:10:46,800
 our experiences.

145
00:10:46,800 --> 00:10:54,060
 And it helps us to change fundamentally the way we react to

146
00:10:54,060 --> 00:10:57,640
 anything, everything.

147
00:10:57,640 --> 00:11:05,110
 Through the process of cultivating mindfulness, you observe

148
00:11:05,110 --> 00:11:09,120
, become familiar with all of the

149
00:11:09,120 --> 00:11:13,720
 many bad habits that you have.

150
00:11:13,720 --> 00:11:20,770
 Mindfulness is so universal because it addresses everything

151
00:11:20,770 --> 00:11:21,200
.

152
00:11:21,200 --> 00:11:27,220
 It's not specific to one bad habit, one quality of mind

153
00:11:27,220 --> 00:11:31,800
 that it's changing because it is present

154
00:11:31,800 --> 00:11:45,720
 with all of your many mental habits.

155
00:11:45,720 --> 00:11:51,010
 It can't stop you from coming in contact with things that

156
00:11:51,010 --> 00:11:54,360
 you like or dislike or are attached

157
00:11:54,360 --> 00:11:56,360
 to.

158
00:11:56,360 --> 00:12:02,720
 But rather it stops you from attaching in the first place.

159
00:12:02,720 --> 00:12:08,160
 It inclines the mind in a very simple way.

160
00:12:08,160 --> 00:12:09,800
 It changes your inclination.

161
00:12:09,800 --> 00:12:17,640
 It intercepts that or the arising of the inclination.

162
00:12:17,640 --> 00:12:21,520
 The inclination that says, "This is good.

163
00:12:21,520 --> 00:12:25,360
 I want this."

164
00:12:25,360 --> 00:12:31,060
 That identifies it as something to be fixed, something that

165
00:12:31,060 --> 00:12:33,600
 I don't have and I can get,

166
00:12:33,600 --> 00:12:38,000
 something that I have and I can get rid of.

167
00:12:38,000 --> 00:12:46,320
 Destroy, hurt, punish.

168
00:12:46,320 --> 00:12:47,320
 It changes that.

169
00:12:47,320 --> 00:12:56,920
 It gives you the inclination towards objectivity.

170
00:12:56,920 --> 00:13:03,940
 You might say the inclination is an inclination towards

171
00:13:03,940 --> 00:13:06,080
 familiarity.

172
00:13:06,080 --> 00:13:10,280
 So when you feel pain rather than getting upset about it,

173
00:13:10,280 --> 00:13:12,600
 inclining towards fixing the

174
00:13:12,600 --> 00:13:17,420
 pain, removing the pain, you incline towards experiencing

175
00:13:17,420 --> 00:13:20,080
 the pain, just experiencing the

176
00:13:20,080 --> 00:13:21,080
 pain.

177
00:13:21,080 --> 00:13:29,380
 So when we say to ourselves, "Pain, pain," this is a new

178
00:13:29,380 --> 00:13:33,120
 relationship with pain.

179
00:13:33,120 --> 00:13:37,000
 It addresses the habit head-on as it's happening.

180
00:13:37,000 --> 00:13:40,540
 In the moment where we would have reacted to the pain, we

181
00:13:40,540 --> 00:13:42,160
 didn't react to the pain.

182
00:13:42,160 --> 00:13:44,120
 We experienced it.

183
00:13:44,120 --> 00:13:48,480
 We faced it.

184
00:13:48,480 --> 00:13:50,360
 We didn't tell ourselves, "This is bad.

185
00:13:50,360 --> 00:13:51,360
 This is a problem."

186
00:13:51,360 --> 00:13:54,720
 We told ourselves, "This is pain."

187
00:13:54,720 --> 00:14:01,550
 In that moment, we changed our habit and the continuous

188
00:14:01,550 --> 00:14:05,520
 systematic dedicated cultivation

189
00:14:05,520 --> 00:14:11,250
 of this new habit gradually loosens up our hold on all

190
00:14:11,250 --> 00:14:13,440
 sorts of things.

191
00:14:13,440 --> 00:14:17,550
 When we want something, when you think of food, thinking of

192
00:14:17,550 --> 00:14:19,280
 when you see food in your

193
00:14:19,280 --> 00:14:21,690
 mind or when you see food in front of you, seeing when you

194
00:14:21,690 --> 00:14:23,000
 smell food, smelling when

195
00:14:23,000 --> 00:14:27,500
 you taste food, tasting, even when you like food, liking

196
00:14:27,500 --> 00:14:29,320
 because it's a chain.

197
00:14:29,320 --> 00:14:30,480
 It's a feedback loop.

198
00:14:30,480 --> 00:14:33,670
 If you catch it anywhere, even at the moment where you've

199
00:14:33,670 --> 00:14:35,600
 already decided, "This is good.

200
00:14:35,600 --> 00:14:38,780
 I should get this," when you say to yourself, "Liking," you

201
00:14:38,780 --> 00:14:40,480
're breaking the chain so that

202
00:14:40,480 --> 00:14:45,610
 that moment of liking doesn't lead to another moment of

203
00:14:45,610 --> 00:14:48,920
 wanting, of needing, of striving

204
00:14:48,920 --> 00:14:52,780
 for, of obsessing about because it's not just one moment of

205
00:14:52,780 --> 00:14:53,640
 wanting.

206
00:14:53,640 --> 00:14:54,640
 We feed it.

207
00:14:54,640 --> 00:14:57,080
 We say, "We want something," and we say, "Yes."

208
00:14:57,080 --> 00:14:58,400
 We encourage the wanting.

209
00:14:58,400 --> 00:15:01,100
 We say, "Yes, this is something worth wanting, worth

210
00:15:01,100 --> 00:15:03,320
 getting," and we think about it again,

211
00:15:03,320 --> 00:15:07,600
 and we augment, and that's how it builds.

212
00:15:07,600 --> 00:15:10,440
 You might have a niggling sort of desire for something, and

213
00:15:10,440 --> 00:15:11,840
 then it builds and builds in

214
00:15:11,840 --> 00:15:16,040
 your mind until you just have to go get it.

215
00:15:16,040 --> 00:15:19,410
 You can say to yourself all you want, "I'm going to resolve

216
00:15:19,410 --> 00:15:20,800
 not to eat junk food.

217
00:15:20,800 --> 00:15:23,030
 I'm going to resolve not to have cigarettes," and then when

218
00:15:23,030 --> 00:15:24,440
 it comes up, you say, "No, no,"

219
00:15:24,440 --> 00:15:28,280
 but it grows and grows and grows without mindfulness,

220
00:15:28,280 --> 00:15:31,280
 without something to change that habit to

221
00:15:31,280 --> 00:15:33,840
 interrupt it.

222
00:15:33,840 --> 00:15:36,440
 It's really a lost cause.

223
00:15:36,440 --> 00:15:41,040
 You can do this with ... There was one of my most popular

224
00:15:41,040 --> 00:15:43,520
 videos is about pornography and

225
00:15:43,520 --> 00:15:47,770
 masturbation in regards to sexual desire, rather than

226
00:15:47,770 --> 00:15:49,960
 saying to yourself, "No, no, no,

227
00:15:49,960 --> 00:15:52,240
 I'm going to abstain.

228
00:15:52,240 --> 00:15:53,320
 Look at the desire.

229
00:15:53,320 --> 00:15:55,200
 Look at the pleasure."

230
00:15:55,200 --> 00:15:59,240
 A big problem with addictions is that they're pleasurable.

231
00:15:59,240 --> 00:16:06,030
 They're associated with pleasure, and honestly, it's a

232
00:16:06,030 --> 00:16:10,440
 strongly held argument that for that

233
00:16:10,440 --> 00:16:15,700
 reason there's nothing wrong really with most addictions,

234
00:16:15,700 --> 00:16:18,320
 except that they are habitual.

235
00:16:18,320 --> 00:16:22,080
 You can't say that you're just going to be a little bit

236
00:16:22,080 --> 00:16:25,480
 attached to something, and furthermore,

237
00:16:25,480 --> 00:16:29,850
 you can't say that you're going to have a clear mind and

238
00:16:29,850 --> 00:16:31,760
 still be addicted.

239
00:16:31,760 --> 00:16:37,720
 All of the times that you are caught up in greed and anger,

240
00:16:37,720 --> 00:16:40,320
 your mind is unclear.

241
00:16:40,320 --> 00:16:44,360
 Your mind is off balance.

242
00:16:44,360 --> 00:16:49,400
 It's not objective.

243
00:16:49,400 --> 00:16:51,800
 We get caught off guard when we lose things.

244
00:16:51,800 --> 00:16:56,640
 We are unable to cope with change.

245
00:16:56,640 --> 00:17:03,080
 We are unwise because of our addictions, because of our a

246
00:17:03,080 --> 00:17:04,520
versions.

247
00:17:04,520 --> 00:17:07,500
 That lack of wisdom has clear consequences.

248
00:17:07,500 --> 00:17:09,400
 We suffer when things change.

249
00:17:09,400 --> 00:17:10,800
 We suffer from loss.

250
00:17:10,800 --> 00:17:17,800
 We suffer from adversity, challenge, competition.

251
00:17:17,800 --> 00:17:21,560
 We suffer.

252
00:17:21,560 --> 00:17:26,500
 Mindfulness is an incredible tool, something that is so

253
00:17:26,500 --> 00:17:29,200
 simple in the theory and in its

254
00:17:29,200 --> 00:17:37,160
 application and yet so powerful because it's really not any

255
00:17:37,160 --> 00:17:38,840
 one thing.

256
00:17:38,840 --> 00:17:42,400
 The cultivation of kindness is a thing.

257
00:17:42,400 --> 00:17:44,880
 You develop kindness.

258
00:17:44,880 --> 00:17:47,320
 Cultivation of calm is a thing.

259
00:17:47,320 --> 00:17:50,180
 You cultivate calm, and they last for as long as they last,

260
00:17:50,180 --> 00:17:52,200
 but you've cultivated that thing,

261
00:17:52,200 --> 00:17:54,440
 that habit.

262
00:17:54,440 --> 00:17:57,300
 Mindfulness doesn't exactly ... I mean, you can say it

263
00:17:57,300 --> 00:17:58,520
 cultivates a habit, but it's

264
00:17:58,520 --> 00:18:05,240
 not a creation of some inclination.

265
00:18:05,240 --> 00:18:09,680
 It's rather the reduction of any inclination.

266
00:18:09,680 --> 00:18:13,120
 It's like a tree that stands tall, straight up, doesn't

267
00:18:13,120 --> 00:18:15,160
 incline in any one direction.

268
00:18:15,160 --> 00:18:17,240
 The tree is to grow.

269
00:18:17,240 --> 00:18:18,440
 It has to grow straight.

270
00:18:18,440 --> 00:18:23,880
 Otherwise it's liable to fall over in the wind.

271
00:18:23,880 --> 00:18:28,480
 That's what happens as we fall over because of our incl

272
00:18:28,480 --> 00:18:29,680
inations.

273
00:18:29,680 --> 00:18:32,840
 Not to say that good inclinations are not quite like that.

274
00:18:32,840 --> 00:18:34,280
 They're good.

275
00:18:34,280 --> 00:18:35,720
 Cultivating kindness is good.

276
00:18:35,720 --> 00:18:38,120
 Cultivating calm is good.

277
00:18:38,120 --> 00:18:46,360
 But they don't have the strength of competing with or upro

278
00:18:46,360 --> 00:18:50,920
oting our bad inclinations.

279
00:19:11,260 --> 00:19:14,460
 Without any judgments, without any inclinations towards

280
00:19:14,460 --> 00:19:17,800
 them.

281
00:19:17,800 --> 00:19:22,560
 I thought that would be interesting to think about.

282
00:19:22,560 --> 00:19:26,600
 What are your resolutions for the new year?

283
00:19:26,600 --> 00:19:29,760
 How can mindfulness help you?

284
00:19:29,760 --> 00:19:34,800
 Because most certainly mindfulness can help with any well-

285
00:19:34,800 --> 00:19:36,800
formed resolution.

286
00:19:36,800 --> 00:19:40,320
 Happy new year, everyone.

287
00:19:40,320 --> 00:19:41,160
 Wish you a happy new year.

