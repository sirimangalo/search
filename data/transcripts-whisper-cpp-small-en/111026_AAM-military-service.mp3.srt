1
00:00:00,000 --> 00:00:04,000
 How do Buddhists view military service?

2
00:00:04,000 --> 00:00:10,000
 This is interesting because it actually relates to the monk

3
00:00:10,000 --> 00:00:12,000
 question I think or the answer does.

4
00:00:12,000 --> 00:00:16,800
 Because the word military service has come to mean so many

5
00:00:16,800 --> 00:00:18,000
 different things, right?

6
00:00:18,000 --> 00:00:21,340
 You don't have to necessarily kill people to be in the

7
00:00:21,340 --> 00:00:22,000
 military.

8
00:00:22,000 --> 00:00:28,000
 Military can stop floods, for example.

9
00:00:28,000 --> 00:00:31,690
 If the military is put to use stopping floods, that's a

10
00:00:31,690 --> 00:00:33,000
 great thing.

11
00:00:33,000 --> 00:00:36,720
 You took away all the weapons from the military, took away

12
00:00:36,720 --> 00:00:48,000
 all of their weapons, gave them shovels and sandbags.

13
00:00:48,000 --> 00:00:51,000
 Then I think it would be a good thing.

14
00:00:51,000 --> 00:00:59,000
 You have a standing army that looks after people's welfare.

15
00:00:59,000 --> 00:01:07,900
 But no, obviously the military is for the purpose of doing

16
00:01:07,900 --> 00:01:12,000
 battle, causing harm,

17
00:01:12,000 --> 00:01:18,690
 hurting other beings, maybe in defense, but a person who

18
00:01:18,690 --> 00:01:22,000
 enters the military is doing it with the,

19
00:01:22,000 --> 00:01:27,110
 I believe with the understanding and the intention that

20
00:01:27,110 --> 00:01:34,000
 they are going to bring violence to other beings.

21
00:01:34,000 --> 00:01:41,860
 As I said, they can think of it as in defense, but it's a

22
00:01:41,860 --> 00:01:48,000
 violent profession by its very nature.

23
00:01:48,000 --> 00:01:52,500
 I guess my question would be, in what way would you think

24
00:01:52,500 --> 00:02:00,000
 that military service would be a positive thing?

25
00:02:00,000 --> 00:02:08,640
 Is there any way that you can think of it as not being a

26
00:02:08,640 --> 00:02:14,000
 negative thing, I suppose?

27
00:02:14,000 --> 00:02:18,890
 Okay, my job would be to help children of foreign countries

28
00:02:18,890 --> 00:02:20,000
 not hate Americans.

29
00:02:20,000 --> 00:02:23,000
 But the question is, if there were a war, would you be

30
00:02:23,000 --> 00:02:25,000
 asked to pick up a gun and fight?

31
00:02:25,000 --> 00:02:28,020
 I don't really know how to, that's what I said, the

32
00:02:28,020 --> 00:02:31,600
 military has become something quite a bit more than just

33
00:02:31,600 --> 00:02:32,000
 guys with guns.

34
00:02:32,000 --> 00:02:42,260
 But I still think you're given a duty to fight, are you not

35
00:02:42,260 --> 00:02:43,000
?

36
00:02:43,000 --> 00:02:47,880
 There are many issues here because I know that being in the

37
00:02:47,880 --> 00:02:54,110
 American military, for example, can be a great way,

38
00:02:54,110 --> 00:02:56,000
 theoretically, to help people.

39
00:02:56,000 --> 00:03:03,530
 You're given a salary, you're given a lot of equipment and

40
00:03:03,530 --> 00:03:05,000
 resources.

41
00:03:05,000 --> 00:03:08,590
 And yeah, joining other organizations might be a great

42
00:03:08,590 --> 00:03:12,320
 thing, but there's something to be said about the clout of

43
00:03:12,320 --> 00:03:15,000
 the American military in that sense.

44
00:03:15,000 --> 00:03:19,060
 The problem is that, I don't understand it fully, but as I

45
00:03:19,060 --> 00:03:25,580
 understand, when you join the military, no, I guess that's

46
00:03:25,580 --> 00:03:26,000
 not true.

47
00:03:26,000 --> 00:03:30,060
 If you're intelligence and in the Air Force, you're not

48
00:03:30,060 --> 00:03:35,000
 involved with killing. More like a desk job, right?

49
00:03:35,000 --> 00:03:39,000
 Well, then I guess there's the moral question of, is there

50
00:03:39,000 --> 00:03:43,390
 a moral question of joining an organization that, at its

51
00:03:43,390 --> 00:03:50,000
 very nature, is for the purpose of,

52
00:03:50,000 --> 00:03:56,220
 I don't want to say, the purpose is to engage in combat, no

53
00:03:56,220 --> 00:03:58,000
? Or to...

54
00:03:58,000 --> 00:04:07,680
 Hmm. Well, it's a military. Are there ethical problems

55
00:04:07,680 --> 00:04:09,000
 there?

56
00:04:09,000 --> 00:04:13,000
 I guess I don't have strong feelings about this.

57
00:04:13,000 --> 00:04:18,470
 You take your job, and if your job is for the purpose of

58
00:04:18,470 --> 00:04:22,510
 helping other people, and if, through your job, you can

59
00:04:22,510 --> 00:04:26,000
 help other people, then power to you.

60
00:04:26,000 --> 00:04:31,200
 Even if your job doesn't help other people, I'm not of the

61
00:04:31,200 --> 00:04:36,350
 bent that you have to be concerned with anything larger

62
00:04:36,350 --> 00:04:39,000
 than doing your own task.

63
00:04:39,000 --> 00:04:42,360
 And I know people are, and there's a lot of people who have

64
00:04:42,360 --> 00:04:46,170
 this social conscience, that they feel like they shouldn't

65
00:04:46,170 --> 00:04:48,000
 work for an organization.

66
00:04:48,000 --> 00:04:53,000
 This is the same debate about vegetarianism, right?

67
00:04:53,000 --> 00:04:56,460
 You can't eat meat because you're contributing to the

68
00:04:56,460 --> 00:04:58,000
 killing of animals.

69
00:04:58,000 --> 00:05:03,860
 You can't use Gillette razors because you're contributing

70
00:05:03,860 --> 00:05:07,000
 to the cruelty towards animals.

71
00:05:07,000 --> 00:05:11,210
 You can't buy certain stocks. You can't buy certain

72
00:05:11,210 --> 00:05:13,000
 products. You can't...

73
00:05:13,000 --> 00:05:18,000
 You know, don't buy Nike. Don't buy this. Don't buy that.

74
00:05:18,000 --> 00:05:22,320
 These sort of things. And I don't buy it. It seems the

75
00:05:22,320 --> 00:05:25,000
 Buddha didn't buy it.

76
00:05:25,000 --> 00:05:28,480
 And there's even... We have this wonderful example that we

77
00:05:28,480 --> 00:05:32,000
 always tell this woman whose husband was a hunter.

78
00:05:32,000 --> 00:05:35,770
 When she was young, she went to practice meditation,

79
00:05:35,770 --> 00:05:40,000
 listened to the Buddha's teaching, and became a sotapan.

80
00:05:40,000 --> 00:05:45,000
 Which means she was, in a sense, enlightened.

81
00:05:45,000 --> 00:05:48,690
 And then she got married because in India, their marriage

82
00:05:48,690 --> 00:05:51,000
 would have been decided for them.

83
00:05:51,000 --> 00:05:56,000
 So she was married off to this man who was a hunter.

84
00:05:56,000 --> 00:06:00,980
 And every day she would clean his traps, clean the blood

85
00:06:00,980 --> 00:06:04,000
 and the guts and whatever, the hair off of these traps,

86
00:06:04,000 --> 00:06:07,960
 and put them out on the table for him. And every day he

87
00:06:07,960 --> 00:06:10,000
 would go out and kill animals.

88
00:06:10,000 --> 00:06:14,060
 And so they asked, "How is this possible that she can do

89
00:06:14,060 --> 00:06:15,000
 this?"

90
00:06:15,000 --> 00:06:19,890
 And the Buddha said, "It's because..." He said, "It's like

91
00:06:19,890 --> 00:06:22,000
 if your hand has no cut.

92
00:06:22,000 --> 00:06:26,000
 If you have no wound on your hand, you can handle poison.

93
00:06:26,000 --> 00:06:31,590
 But if you have a wound on your hand, you can't handle

94
00:06:31,590 --> 00:06:33,000
 poison."

95
00:06:33,000 --> 00:06:38,400
 He says a little more poetically than that. But the point

96
00:06:38,400 --> 00:06:39,000
 being

97
00:06:39,000 --> 00:06:44,000
 that when a person has no bad intentions,

98
00:06:44,000 --> 00:06:50,750
 the act of cleaning blood and gore off of these killing

99
00:06:50,750 --> 00:06:52,000
 machines,

100
00:06:52,000 --> 00:06:56,000
 even to that extent, is not considered to be immoral.

101
00:06:56,000 --> 00:06:59,490
 Because it's not. It's the same as eating meat. The act

102
00:06:59,490 --> 00:07:02,000
 itself is never immoral.

103
00:07:02,000 --> 00:07:06,000
 The Buddha actually taught against the idea of karma.

104
00:07:06,000 --> 00:07:08,690
 He said, "Karma is not the important thing. It's the

105
00:07:08,690 --> 00:07:10,000
 intention."

106
00:07:10,000 --> 00:07:12,580
 So if the woman is intending, "Oh, here I'm helping my

107
00:07:12,580 --> 00:07:15,000
 husband, making him happy, and doing my job.

108
00:07:15,000 --> 00:07:19,850
 If I don't, he might beat me," kind of thing, that might

109
00:07:19,850 --> 00:07:22,000
 have had a part in it.

110
00:07:22,000 --> 00:07:26,000
 Because there's some horrible things that go on out there.

111
00:07:26,000 --> 00:07:30,100
 But even he's a hunter. He's not the most cultured of

112
00:07:30,100 --> 00:07:31,000
 beings.

113
00:07:31,000 --> 00:07:37,980
 Even without that, just the idea that she's doing her job

114
00:07:37,980 --> 00:07:41,000
 and doing what is her way.

115
00:07:41,000 --> 00:07:43,800
 And for a person who eats meat, they're just eating the

116
00:07:43,800 --> 00:07:47,000
 food in order to gain the benefit.

117
00:07:47,000 --> 00:07:54,470
 This is in line, I think, with my understanding of the

118
00:07:54,470 --> 00:07:56,000
 Buddha's teaching.

119
00:07:56,000 --> 00:08:00,000
 Because we're a deal in terms of experience.

120
00:08:00,000 --> 00:08:08,000
 [Silence]

121
00:08:08,000 --> 00:08:11,450
 "Don't some monks practice some military aspects like Sha

122
00:08:11,450 --> 00:08:13,000
olin? I'll never live this one down."

123
00:08:13,000 --> 00:08:17,790
 No, Buddhist monks don't practice Shaolin. Shaolin monks

124
00:08:17,790 --> 00:08:19,000
 practice Shaolin.

125
00:08:19,000 --> 00:08:21,000
 I'm sorry, they call themselves Buddhists.

126
00:08:21,000 --> 00:08:26,450
 But in what way does kung fu have to do with the Buddha's

127
00:08:26,450 --> 00:08:28,000
 teaching?

128
00:08:28,000 --> 00:08:33,770
 Well, to each their own. But no, Shaolin monks are a very

129
00:08:33,770 --> 00:08:36,000
 specific type of monk.

130
00:08:36,000 --> 00:08:40,730
 I don't know. I guess I just can't answer that because it's

131
00:08:40,730 --> 00:08:42,000
 so different from what I do.

132
00:08:42,000 --> 00:08:45,000
 I used to practice karate, but that was a long time ago.

133
00:08:45,000 --> 00:08:50,960
 Karate isn't kung fu, but it's not a part of the Buddha's

134
00:08:50,960 --> 00:08:53,000
 teaching for sure.

135
00:08:53,000 --> 00:08:57,500
 Those monks are doing it. I mean, there's a whole history

136
00:08:57,500 --> 00:08:58,000
 there of how it came about,

137
00:08:58,000 --> 00:09:00,510
 because there was war and the monks had to defend

138
00:09:00,510 --> 00:09:02,000
 themselves or whatever.

139
00:09:02,000 --> 00:09:04,000
 I'm not sure how it all came about.

140
00:09:04,000 --> 00:09:09,060
 Anyway, question on joining the military. I say, as I said,

141
00:09:09,060 --> 00:09:10,000
 take it as your job.

142
00:09:10,000 --> 00:09:13,000
 Because the American military is such a huge organization

143
00:09:13,000 --> 00:09:17,490
 and so many people aren't involved with the killing part of

144
00:09:17,490 --> 00:09:18,000
 it.

145
00:09:18,000 --> 00:09:24,460
 But you have to be fairly careful there as well, because if

146
00:09:24,460 --> 00:09:27,000
 you are put in a position

147
00:09:27,000 --> 00:09:36,560
 where you have to be involved with or encourage the use of

148
00:09:36,560 --> 00:09:37,000
 force

149
00:09:37,000 --> 00:09:42,000
 or the distribution of weapons, for example,

150
00:09:42,000 --> 00:09:44,870
 it's certainly something that you have to take into

151
00:09:44,870 --> 00:09:46,000
 consideration.

152
00:09:46,000 --> 00:09:49,560
 Be careful that you don't somehow get conscripted and, you

153
00:09:49,560 --> 00:09:52,000
 know, you're a soldier, go and fight.

154
00:09:52,000 --> 00:09:54,000
 That kind of thing.

155
00:09:54,000 --> 00:09:56,000
 But, you know, we're living in hard times.

156
00:09:56,000 --> 00:10:00,000
 If you have a chance to help people, then power to you.

157
00:10:00,000 --> 00:10:03,380
 Go out there and help. Because actually, in Canada, the

158
00:10:03,380 --> 00:10:06,000
 military is mostly involved in peacekeeping,

159
00:10:06,000 --> 00:10:09,370
 helping to bring peace. Or they were, I don't know what it

160
00:10:09,370 --> 00:10:10,000
's like now.

161
00:10:10,000 --> 00:10:12,000
 Anyway, military.

